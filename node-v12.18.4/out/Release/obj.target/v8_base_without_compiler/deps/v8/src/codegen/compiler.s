	.file	"compiler.cc"
	.text
	.section	.text._ZNKSt5ctypeIcE8do_widenEc,"axG",@progbits,_ZNKSt5ctypeIcE8do_widenEc,comdat
	.align 2
	.p2align 4
	.weak	_ZNKSt5ctypeIcE8do_widenEc
	.type	_ZNKSt5ctypeIcE8do_widenEc, @function
_ZNKSt5ctypeIcE8do_widenEc:
.LFB2349:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	ret
	.cfi_endproc
.LFE2349:
	.size	_ZNKSt5ctypeIcE8do_widenEc, .-_ZNKSt5ctypeIcE8do_widenEc
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB5104:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE5104:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB5105:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5105:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB5107:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5107:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.text._ZN2v88internal6Logger26DefaultEventLoggerSentinelEPKci,"axG",@progbits,_ZN2v88internal6Logger26DefaultEventLoggerSentinelEPKci,comdat
	.p2align 4
	.weak	_ZN2v88internal6Logger26DefaultEventLoggerSentinelEPKci
	.type	_ZN2v88internal6Logger26DefaultEventLoggerSentinelEPKci, @function
_ZN2v88internal6Logger26DefaultEventLoggerSentinelEPKci:
.LFB21376:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE21376:
	.size	_ZN2v88internal6Logger26DefaultEventLoggerSentinelEPKci, .-_ZN2v88internal6Logger26DefaultEventLoggerSentinelEPKci
	.section	.text._ZN2v88internal6Logger27is_listening_to_code_eventsEv,"axG",@progbits,_ZN2v88internal6Logger27is_listening_to_code_eventsEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6Logger27is_listening_to_code_eventsEv
	.type	_ZN2v88internal6Logger27is_listening_to_code_eventsEv, @function
_ZN2v88internal6Logger27is_listening_to_code_eventsEv:
.LFB21377:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	jne	.L7
	cmpq	$0, 80(%rbx)
	setne	%al
.L7:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21377:
	.size	_ZN2v88internal6Logger27is_listening_to_code_eventsEv, .-_ZN2v88internal6Logger27is_listening_to_code_eventsEv
	.section	.text._ZN2v88internal12StdoutStreamD1Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD1Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StdoutStreamD1Ev
	.type	_ZN2v88internal12StdoutStreamD1Ev, @function
_ZN2v88internal12StdoutStreamD1Ev:
.LFB34021:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE34021:
	.size	_ZN2v88internal12StdoutStreamD1Ev, .-_ZN2v88internal12StdoutStreamD1Ev
	.section	.text._ZN2v88internal15InterruptsScopeD2Ev,"axG",@progbits,_ZN2v88internal15InterruptsScopeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15InterruptsScopeD2Ev
	.type	_ZN2v88internal15InterruptsScopeD2Ev, @function
_ZN2v88internal15InterruptsScopeD2Ev:
.LFB13714:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal15InterruptsScopeE(%rip), %rax
	cmpl	$2, 32(%rdi)
	movq	%rax, (%rdi)
	jne	.L14
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movq	8(%rdi), %rdi
	jmp	_ZN2v88internal10StackGuard18PopInterruptsScopeEv@PLT
	.cfi_endproc
.LFE13714:
	.size	_ZN2v88internal15InterruptsScopeD2Ev, .-_ZN2v88internal15InterruptsScopeD2Ev
	.weak	_ZN2v88internal15InterruptsScopeD1Ev
	.set	_ZN2v88internal15InterruptsScopeD1Ev,_ZN2v88internal15InterruptsScopeD2Ev
	.section	.text._ZN2v88internal15InterruptsScopeD0Ev,"axG",@progbits,_ZN2v88internal15InterruptsScopeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15InterruptsScopeD0Ev
	.type	_ZN2v88internal15InterruptsScopeD0Ev, @function
_ZN2v88internal15InterruptsScopeD0Ev:
.LFB13716:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal15InterruptsScopeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	cmpl	$2, 32(%rdi)
	movq	%rax, (%rdi)
	je	.L16
	movq	8(%rdi), %rdi
	call	_ZN2v88internal10StackGuard18PopInterruptsScopeEv@PLT
.L16:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$48, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE13716:
	.size	_ZN2v88internal15InterruptsScopeD0Ev, .-_ZN2v88internal15InterruptsScopeD0Ev
	.section	.text._ZN2v88internal23PostponeInterruptsScopeD2Ev,"axG",@progbits,_ZN2v88internal23PostponeInterruptsScopeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23PostponeInterruptsScopeD2Ev
	.type	_ZN2v88internal23PostponeInterruptsScopeD2Ev, @function
_ZN2v88internal23PostponeInterruptsScopeD2Ev:
.LFB34128:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal15InterruptsScopeE(%rip), %rax
	cmpl	$2, 32(%rdi)
	movq	%rax, (%rdi)
	jne	.L20
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	movq	8(%rdi), %rdi
	jmp	_ZN2v88internal10StackGuard18PopInterruptsScopeEv@PLT
	.cfi_endproc
.LFE34128:
	.size	_ZN2v88internal23PostponeInterruptsScopeD2Ev, .-_ZN2v88internal23PostponeInterruptsScopeD2Ev
	.weak	_ZN2v88internal23PostponeInterruptsScopeD1Ev
	.set	_ZN2v88internal23PostponeInterruptsScopeD1Ev,_ZN2v88internal23PostponeInterruptsScopeD2Ev
	.section	.text._ZN2v88internal23PostponeInterruptsScopeD0Ev,"axG",@progbits,_ZN2v88internal23PostponeInterruptsScopeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23PostponeInterruptsScopeD0Ev
	.type	_ZN2v88internal23PostponeInterruptsScopeD0Ev, @function
_ZN2v88internal23PostponeInterruptsScopeD0Ev:
.LFB34130:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal15InterruptsScopeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	cmpl	$2, 32(%rdi)
	movq	%rax, (%rdi)
	je	.L22
	movq	8(%rdi), %rdi
	call	_ZN2v88internal10StackGuard18PopInterruptsScopeEv@PLT
.L22:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$48, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE34130:
	.size	_ZN2v88internal23PostponeInterruptsScopeD0Ev, .-_ZN2v88internal23PostponeInterruptsScopeD0Ev
	.section	.text._ZN2v88internal12_GLOBAL__N_125FinalizeScriptCompilationEPNS0_7IsolateEPNS0_9ParseInfoE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_125FinalizeScriptCompilationEPNS0_7IsolateEPNS0_9ParseInfoE, @function
_ZN2v88internal12_GLOBAL__N_125FinalizeScriptCompilationEPNS0_7IsolateEPNS0_9ParseInfoE:
.LFB25246:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	80(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	0(%r13), %rdx
	movslq	99(%rdx), %rax
	orl	$2, %eax
	salq	$32, %rax
	movq	%rax, 95(%rdx)
	movq	160(%rsi), %rax
	testq	%rax, %rax
	je	.L24
	movq	8(%rax), %rbx
	movq	(%rax), %r14
	testq	%rbx, %rbx
	je	.L24
	leaq	-64(%rbp), %rax
	movq	%rdi, %r12
	movq	%rax, -72(%rbp)
	.p2align 4,,10
	.p2align 3
.L28:
	movq	0(%r13), %rax
	movq	8(%rbx), %rdx
	movq	%r12, %rsi
	movq	-72(%rbp), %rdi
	movq	16(%rbx), %r15
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6Script22FindSharedFunctionInfoEPNS0_7IsolateEPKNS0_15FunctionLiteralE@PLT
	testq	%rax, %rax
	je	.L40
	movq	(%rax), %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal18CompilerDispatcher26RegisterSharedFunctionInfoEmNS0_18SharedFunctionInfoE@PLT
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L28
.L24:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L41
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal18CompilerDispatcher8AbortJobEm@PLT
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L28
	jmp	.L24
.L41:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25246:
	.size	_ZN2v88internal12_GLOBAL__N_125FinalizeScriptCompilationEPNS0_7IsolateEPNS0_9ParseInfoE, .-_ZN2v88internal12_GLOBAL__N_125FinalizeScriptCompilationEPNS0_7IsolateEPNS0_9ParseInfoE
	.section	.text._ZN2v88internal12_GLOBAL__N_132InsertCodeIntoOptimizedCodeCacheEPNS0_24OptimizedCompilationInfoE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_132InsertCodeIntoOptimizedCodeCacheEPNS0_24OptimizedCompilationInfoE, @function
_ZN2v88internal12_GLOBAL__N_132InsertCodeIntoOptimizedCodeCacheEPNS0_24OptimizedCompilationInfoE:
.LFB25218:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	40(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	0(%r13), %rax
	testb	$62, 43(%rax)
	jne	.L42
	movq	32(%rdi), %r14
	movq	%rdi, %rbx
	testb	$2, (%rdi)
	jne	.L61
	movq	(%r14), %rax
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %r12
	movq	23(%rax), %rsi
	movq	3520(%r12), %rdi
	subq	$37592, %r12
	testq	%rdi, %rdi
	je	.L50
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L51:
	movq	(%r14), %rax
	movq	%rax, %rdx
	movq	31(%rax), %rax
	andq	$-262144, %rdx
	movq	24(%rdx), %r12
	movq	39(%rax), %rsi
	movq	3520(%r12), %rdi
	subq	$37592, %r12
	testq	%rdi, %rdi
	je	.L53
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	cmpl	$-1, 56(%rbx)
	je	.L62
	.p2align 4,,10
	.p2align 3
.L42:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L63
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	.cfi_restore_state
	cmpl	$-1, 56(%rdi)
	jne	.L42
	movq	(%r14), %rax
	movq	%rax, %rdx
	movq	39(%rax), %rax
	andq	$-262144, %rdx
	movq	24(%rdx), %rbx
	movq	7(%rax), %rsi
	movq	3520(%rbx), %rdi
	subq	$37592, %rbx
	testq	%rdi, %rdi
	je	.L47
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L48:
	leaq	-48(%rbp), %rdi
	movq	%rsi, -48(%rbp)
	call	_ZN2v88internal14FeedbackVector23ClearOptimizationMarkerEv@PLT
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L53:
	movq	41088(%r12), %rax
	cmpq	%rax, 41096(%r12)
	je	.L64
.L55:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	cmpl	$-1, 56(%rbx)
	jne	.L42
.L62:
	movq	(%r14), %rax
	movq	%rax, %rdx
	movq	39(%rax), %rax
	andq	$-262144, %rdx
	movq	24(%rdx), %rbx
	movq	7(%rax), %rsi
	movq	3520(%rbx), %rdi
	subq	$37592, %rbx
	testq	%rdi, %rdi
	je	.L56
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdi
.L57:
	movq	%r13, %rsi
	call	_ZN2v88internal14FeedbackVector16SetOptimizedCodeENS0_6HandleIS1_EENS2_INS0_4CodeEEE@PLT
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L50:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L65
.L52:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L56:
	movq	41088(%rbx), %rdi
	cmpq	41096(%rbx), %rdi
	je	.L66
.L58:
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rdi)
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L47:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L67
.L49:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L65:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L64:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L66:
	movq	%rbx, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %rdi
	jmp	.L58
.L67:
	movq	%rbx, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L49
.L63:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25218:
	.size	_ZN2v88internal12_GLOBAL__N_132InsertCodeIntoOptimizedCodeCacheEPNS0_24OptimizedCompilationInfoE, .-_ZN2v88internal12_GLOBAL__N_132InsertCodeIntoOptimizedCodeCacheEPNS0_24OptimizedCompilationInfoE
	.section	.text._ZN2v88internal12StdoutStreamD0Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD0Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.type	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev, @function
_ZTv0_n24_N2v88internal12StdoutStreamD0Ev:
.LFB34513:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %rax
	addq	-24(%rax), %rdi
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	movq	%rax, 80(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE34513:
	.size	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev, .-_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StdoutStreamD0Ev
	.type	_ZN2v88internal12StdoutStreamD0Ev, @function
_ZN2v88internal12StdoutStreamD0Ev:
.LFB34022:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE34022:
	.size	_ZN2v88internal12StdoutStreamD0Ev, .-_ZN2v88internal12StdoutStreamD0Ev
	.section	.text._ZN2v88internal12StdoutStreamD1Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD1Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.type	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev, @function
_ZTv0_n24_N2v88internal12StdoutStreamD1Ev:
.LFB34514:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	-24(%rax), %rbx
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	addq	%rdi, %rbx
	movq	%rax, 80(%rbx)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	64(%rbx), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE34514:
	.size	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev, .-_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.section	.rodata._ZN2v88internal12_GLOBAL__N_122LogFunctionCompilationENS0_17CodeEventListener16LogEventsAndTagsENS0_6HandleINS0_18SharedFunctionInfoEEENS4_INS0_6ScriptEEENS4_INS0_12AbstractCodeEEEbdPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"optimize"
.LC1:
	.string	"compile"
.LC2:
	.string	"basic_string::append"
.LC3:
	.string	"-eval"
.LC4:
	.string	"-lazy"
.LC5:
	.string	"unreachable code"
	.section	.text._ZN2v88internal12_GLOBAL__N_122LogFunctionCompilationENS0_17CodeEventListener16LogEventsAndTagsENS0_6HandleINS0_18SharedFunctionInfoEEENS4_INS0_6ScriptEEENS4_INS0_12AbstractCodeEEEbdPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_122LogFunctionCompilationENS0_17CodeEventListener16LogEventsAndTagsENS0_6HandleINS0_18SharedFunctionInfoEEENS4_INS0_6ScriptEEENS4_INS0_12AbstractCodeEEEbdPNS0_7IsolateE, @function
_ZN2v88internal12_GLOBAL__N_122LogFunctionCompilationENS0_17CodeEventListener16LogEventsAndTagsENS0_6HandleINS0_18SharedFunctionInfoEEENS4_INS0_6ScriptEEENS4_INS0_12AbstractCodeEEEbdPNS0_7IsolateE:
.LFB25149:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	leaq	_ZN2v88internal6Logger27is_listening_to_code_eventsEv(%rip), %rdx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edi, %ebx
	subq	$168, %rsp
	movq	41016(%r9), %r12
	movl	%r8d, -184(%rbp)
	movq	%r9, -136(%rbp)
	movq	(%r12), %rax
	movsd	%xmm0, -200(%rbp)
	movq	%r12, %rdi
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	movq	136(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L75
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	je	.L145
.L76:
	movq	0(%r13), %rax
	movq	%rax, -104(%rbp)
	leaq	-104(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -144(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal6Script13GetLineNumberENS0_6HandleIS1_EEi@PLT
	movq	-144(%rbp), %rdi
	movl	%eax, %r12d
	movq	0(%r13), %rax
	movq	%rax, -104(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal6Script15GetColumnNumberENS0_6HandleIS1_EEi@PLT
	movq	(%r14), %rdx
	movq	15(%rdx), %rcx
	testb	$1, %cl
	jne	.L146
.L81:
	movq	-136(%rbp), %rcx
	movq	128(%rcx), %rcx
	movq	%rcx, -168(%rbp)
.L83:
	movl	51(%rdx), %ecx
	movl	%ebx, %esi
	testl	%ecx, %ecx
	jne	.L84
	movl	$20, %esi
	cmpl	$15, %ebx
	je	.L84
	movl	$21, %esi
	cmpl	$17, %ebx
	je	.L84
	cmpl	$11, %ebx
	movl	$19, %esi
	cmovne	%ebx, %esi
.L84:
	movl	%eax, -176(%rbp)
	movq	-136(%rbp), %rax
	addl	$1, %r12d
	movl	%esi, -180(%rbp)
	movq	41488(%rax), %rdx
	movq	0(%r13), %rax
	movq	%rax, -192(%rbp)
	movq	(%r15), %rax
	leaq	56(%rdx), %r15
	movq	%r15, %rdi
	movq	%rdx, -152(%rbp)
	movq	%rax, -160(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-152(%rbp), %rdx
	movl	-176(%rbp), %eax
	movl	-180(%rbp), %esi
	movq	16(%rdx), %r10
	addl	$1, %eax
	movl	%eax, -152(%rbp)
	testq	%r10, %r10
	je	.L88
	movq	%r15, -176(%rbp)
	movq	%r10, %r15
	movl	%ebx, -180(%rbp)
	movq	-168(%rbp), %rbx
	movq	%r13, -168(%rbp)
	movq	-192(%rbp), %r13
	movq	%r14, -192(%rbp)
	movl	%esi, %r14d
	.p2align 4,,10
	.p2align 3
.L87:
	movq	8(%r15), %rdi
	movl	-152(%rbp), %ecx
	movl	%r12d, %r9d
	movq	%rbx, %r8
	subq	$8, %rsp
	movq	-160(%rbp), %rdx
	movl	%r14d, %esi
	movq	(%rdi), %rax
	pushq	%rcx
	movq	%r13, %rcx
	call	*40(%rax)
	movq	(%r15), %r15
	popq	%rax
	popq	%rdx
	testq	%r15, %r15
	jne	.L87
	movq	-176(%rbp), %r15
	movl	-180(%rbp), %ebx
	movq	-168(%rbp), %r13
	movq	-192(%rbp), %r14
.L88:
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	cmpb	$0, _ZN2v88internal24FLAG_log_function_eventsE(%rip)
	je	.L74
	cmpb	$0, -184(%rbp)
	leaq	.LC1(%rip), %rax
	leaq	.LC0(%rip), %r15
	cmove	%rax, %r15
	leaq	-80(%rbp), %r12
	movq	%r12, -96(%rbp)
	movq	%r15, %rdi
	call	strlen@PLT
	movq	%r12, %rsi
	movq	%r15, %rdx
	cmpl	$8, %eax
	jnb	.L147
	xorl	%ecx, %ecx
	testb	$4, %al
	jne	.L148
.L101:
	testb	$2, %al
	jne	.L149
.L102:
	testb	$1, %al
	jne	.L150
.L103:
	movq	%rax, -88(%rbp)
	movb	$0, -80(%rbp,%rax)
	cmpl	$15, %ebx
	je	.L151
.L141:
	ja	.L90
	cmpl	$10, %ebx
	jne	.L152
	movabsq	$4611686018427387903, %rax
	subq	-88(%rbp), %rax
	cmpq	$4, %rax
	jbe	.L95
	leaq	-96(%rbp), %rdi
	movl	$5, %edx
	leaq	.LC3(%rip), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L152:
	cmpl	$11, %ebx
	jne	.L93
.L92:
	movq	-136(%rbp), %rax
	movq	41016(%rax), %r15
	movq	%r15, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	jne	.L153
	movq	-96(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L74
.L156:
	call	_ZdlPv@PLT
.L74:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L154
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	.cfi_restore_state
	cmpl	$17, %ebx
	je	.L92
.L93:
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L145:
	cmpq	$0, 80(%r12)
	jne	.L76
.L78:
	movq	-136(%rbp), %rax
	cmpb	$0, 41812(%rax)
	jne	.L76
	cmpb	$0, _ZN2v88internal24FLAG_log_function_eventsE(%rip)
	jne	.L76
	movq	41488(%rax), %rax
	movq	16(%rax), %r12
	testq	%r12, %r12
	jne	.L80
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L155:
	movq	(%r12), %r12
	testq	%r12, %r12
	je	.L74
.L80:
	movq	8(%r12), %rdi
	movq	(%rdi), %rax
	call	*136(%rax)
	testb	%al, %al
	je	.L155
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L150:
	movzbl	(%rdx,%rcx), %edx
	movb	%dl, (%rsi,%rcx)
	movq	%rax, -88(%rbp)
	movb	$0, -80(%rbp,%rax)
	cmpl	$15, %ebx
	jne	.L141
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L149:
	movzwl	(%rdx,%rcx), %edi
	movw	%di, (%rsi,%rcx)
	addq	$2, %rcx
	testb	$1, %al
	je	.L103
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L148:
	movl	(%rdx), %ecx
	movl	%ecx, (%rsi)
	movl	$4, %ecx
	testb	$2, %al
	je	.L102
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L147:
	movl	%eax, %edi
	xorl	%edx, %edx
	andl	$-8, %edi
.L99:
	movl	%edx, %ecx
	addl	$8, %edx
	movq	(%r15,%rcx), %rsi
	movq	%rsi, (%r12,%rcx)
	cmpl	%edi, %edx
	jb	.L99
	leaq	(%r12,%rdx), %rsi
	xorl	%ecx, %ecx
	addq	%r15, %rdx
	testb	$4, %al
	je	.L101
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L153:
	movq	0(%r13), %rax
	movq	-144(%rbp), %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal18SharedFunctionInfo9DebugNameEv@PLT
	leaq	-112(%rbp), %rdi
	movq	%rax, -136(%rbp)
	movq	0(%r13), %rax
	movq	%rax, -112(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo11EndPositionEv@PLT
	leaq	-120(%rbp), %rdi
	movl	%eax, %ebx
	movq	0(%r13), %rax
	movq	%rax, -120(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	movq	-96(%rbp), %rsi
	movq	%r15, %rdi
	movl	%ebx, %r8d
	movl	%eax, %ecx
	movq	(%r14), %rax
	movq	-136(%rbp), %r9
	movsd	-200(%rbp), %xmm0
	movslq	67(%rax), %rdx
	call	_ZN2v88internal6Logger13FunctionEventEPKcidiiNS0_6StringE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r12, %rdi
	jne	.L156
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L151:
	movabsq	$4611686018427387903, %rax
	subq	-88(%rbp), %rax
	cmpq	$4, %rax
	jbe	.L95
	leaq	-96(%rbp), %rdi
	movl	$5, %edx
	leaq	.LC4(%rip), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L146:
	movq	-1(%rcx), %rsi
	movq	%rcx, -168(%rbp)
	cmpw	$63, 11(%rsi)
	jbe	.L83
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L75:
	call	*%rax
	testb	%al, %al
	je	.L78
	jmp	.L76
.L154:
	call	__stack_chk_fail@PLT
.L95:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE25149:
	.size	_ZN2v88internal12_GLOBAL__N_122LogFunctionCompilationENS0_17CodeEventListener16LogEventsAndTagsENS0_6HandleINS0_18SharedFunctionInfoEEENS4_INS0_6ScriptEEENS4_INS0_12AbstractCodeEEEbdPNS0_7IsolateE, .-_ZN2v88internal12_GLOBAL__N_122LogFunctionCompilationENS0_17CodeEventListener16LogEventsAndTagsENS0_6HandleINS0_18SharedFunctionInfoEEENS4_INS0_6ScriptEEENS4_INS0_12AbstractCodeEEEbdPNS0_7IsolateE
	.section	.text._ZN2v88internal12_GLOBAL__N_129ExecuteUnoptimizedCompileJobsEPNS0_9ParseInfoEPNS0_15FunctionLiteralEPNS0_19AccountingAllocatorEPSt12forward_listISt10unique_ptrINS0_25UnoptimizedCompilationJobESt14default_deleteISA_EESaISD_EE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_129ExecuteUnoptimizedCompileJobsEPNS0_9ParseInfoEPNS0_15FunctionLiteralEPNS0_19AccountingAllocatorEPSt12forward_listISt10unique_ptrINS0_25UnoptimizedCompilationJobESt14default_deleteISA_EESaISD_EE, @function
_ZN2v88internal12_GLOBAL__N_129ExecuteUnoptimizedCompileJobsEPNS0_9ParseInfoEPNS0_15FunctionLiteralEPNS0_19AccountingAllocatorEPSt12forward_listISt10unique_ptrINS0_25UnoptimizedCompilationJobESt14default_deleteISA_EESaISD_EE:
.LFB25171:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	$16, 9(%rsi)
	jne	.L191
	cmpb	$1, _ZN2v88internal17FLAG_validate_asmE(%rip)
	jne	.L191
	cmpb	$0, _ZN2v88internal24FLAG_stress_validate_asmE(%rip)
	jne	.L164
	movq	40(%rdx), %rdi
	call	_ZNK2v88internal5Scope11IsAsmModuleEv@PLT
	testb	%al, %al
	jne	.L164
	.p2align 4,,10
	.p2align 3
.L191:
	leaq	-80(%rbp), %r8
.L160:
	pxor	%xmm0, %xmm0
	leaq	-96(%rbp), %rdi
	movq	%r12, %rdx
	movq	%r14, %rcx
	movq	%r13, %rsi
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_ZN2v88internal11interpreter11Interpreter17NewCompilationJobEPNS0_9ParseInfoEPNS0_15FunctionLiteralEPNS0_19AccountingAllocatorEPSt6vectorIS6_SaIS6_EE@PLT
	movq	-96(%rbp), %r12
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	%rax, -104(%rbp)
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testl	%eax, %eax
	je	.L193
	movl	$4, 8(%r12)
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	subq	-104(%rbp), %rax
	addq	%rax, 48(%r12)
	movq	$0, (%r15)
.L171:
	movq	-96(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L192
	movq	(%rdi), %rax
	call	*8(%rax)
.L192:
	movq	-80(%rbp), %rdi
.L174:
	testq	%rdi, %rdi
	je	.L157
	call	_ZdlPv@PLT
.L157:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L194
	addq	$88, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L193:
	.cfi_restore_state
	movl	$2, 8(%r12)
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	subq	-104(%rbp), %rax
	addq	%rax, 48(%r12)
	leaq	-88(%rbp), %rsi
	movq	-80(%rbp), %rdi
	movq	-72(%rbp), %rax
	movq	%rsi, -104(%rbp)
	movq	%rax, -112(%rbp)
	movq	%rdi, %r12
	cmpq	%rdi, %rax
	je	.L170
.L173:
	movq	(%r12), %rdx
	movq	-104(%rbp), %rdi
	movq	%rbx, %r8
	movq	%r14, %rcx
	movq	%r13, %rsi
	call	_ZN2v88internal12_GLOBAL__N_129ExecuteUnoptimizedCompileJobsEPNS0_9ParseInfoEPNS0_15FunctionLiteralEPNS0_19AccountingAllocatorEPSt12forward_listISt10unique_ptrINS0_25UnoptimizedCompilationJobESt14default_deleteISA_EESaISD_EE
	cmpq	$0, -88(%rbp)
	je	.L195
	movl	$16, %edi
	addq	$8, %r12
	call	_Znwm@PLT
	movq	(%rbx), %xmm0
	movq	%rax, (%rbx)
	movhps	-88(%rbp), %xmm0
	movups	%xmm0, (%rax)
	cmpq	%r12, -112(%rbp)
	jne	.L173
	movq	-80(%rbp), %rdi
.L170:
	movq	-96(%rbp), %rax
	movq	%rax, (%r15)
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L164:
	leaq	-80(%rbp), %r8
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rcx
	movq	%r8, %rdi
	movq	%r8, -120(%rbp)
	call	_ZN2v88internal5AsmJs17NewCompilationJobEPNS0_9ParseInfoEPNS0_15FunctionLiteralEPNS0_19AccountingAllocatorE@PLT
	movq	-80(%rbp), %rcx
	movq	%rcx, -104(%rbp)
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	-104(%rbp), %rcx
	movq	%rax, -112(%rbp)
	movq	(%rcx), %rax
	movq	%rcx, %rdi
	call	*16(%rax)
	movq	-104(%rbp), %rcx
	testl	%eax, %eax
	je	.L162
	movl	$4, 8(%rcx)
	movq	%rcx, -104(%rbp)
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	-104(%rbp), %rcx
	subq	-112(%rbp), %rax
	movq	-120(%rbp), %r8
	addq	%rax, 48(%rcx)
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L160
	movq	(%rdi), %rax
	movq	%r8, -104(%rbp)
	call	*8(%rax)
	movq	-104(%rbp), %r8
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L162:
	movl	$2, 8(%rcx)
	movq	%rcx, -104(%rbp)
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	-104(%rbp), %rcx
	subq	-112(%rbp), %rax
	addq	%rax, 48(%rcx)
	movq	-80(%rbp), %rax
	movq	%rax, (%r15)
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L195:
	movq	$0, (%r15)
	jmp	.L171
.L194:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25171:
	.size	_ZN2v88internal12_GLOBAL__N_129ExecuteUnoptimizedCompileJobsEPNS0_9ParseInfoEPNS0_15FunctionLiteralEPNS0_19AccountingAllocatorEPSt12forward_listISt10unique_ptrINS0_25UnoptimizedCompilationJobESt14default_deleteISA_EESaISD_EE, .-_ZN2v88internal12_GLOBAL__N_129ExecuteUnoptimizedCompileJobsEPNS0_9ParseInfoEPNS0_15FunctionLiteralEPNS0_19AccountingAllocatorEPSt12forward_listISt10unique_ptrINS0_25UnoptimizedCompilationJobESt14default_deleteISA_EESaISD_EE
	.section	.text._ZN2v88internal12_GLOBAL__N_123GenerateUnoptimizedCodeEPNS0_9ParseInfoEPNS0_19AccountingAllocatorEPSt12forward_listISt10unique_ptrINS0_25UnoptimizedCompilationJobESt14default_deleteIS8_EESaISB_EE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_123GenerateUnoptimizedCodeEPNS0_9ParseInfoEPNS0_19AccountingAllocatorEPSt12forward_listISt10unique_ptrINS0_25UnoptimizedCompilationJobESt14default_deleteIS8_EESaISB_EE, @function
_ZN2v88internal12_GLOBAL__N_123GenerateUnoptimizedCodeEPNS0_9ParseInfoEPNS0_19AccountingAllocatorEPSt12forward_listISt10unique_ptrINS0_25UnoptimizedCompilationJobESt14default_deleteIS8_EESaISB_EE:
.LFB25182:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	xorl	%edx, %edx
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rcx, %rbx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, (%rdi)
	testb	$32, 9(%rsi)
	movq	136(%rsi), %rdi
	movq	$0, -48(%rbp)
	setne	%dl
	movaps	%xmm0, -80(%rbp)
	addl	$116, %edx
	movaps	%xmm0, -64(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	je	.L198
	testq	%rdi, %rdi
	jne	.L220
.L198:
	movq	%r12, %rdi
	call	_ZN2v88internal8Rewriter7RewriteEPNS0_9ParseInfoE@PLT
	testb	%al, %al
	jne	.L221
.L199:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L222
.L202:
	movq	%r12, %rdi
	call	_ZN2v88internal9ParseInfo20ResetCharacterStreamEv@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L223
	addq	$48, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L221:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal16DeclarationScope7AnalyzeEPNS0_9ParseInfoE@PLT
	testb	%al, %al
	je	.L199
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L224
.L201:
	leaq	-80(%rbp), %rdi
	movq	%rbx, %r8
	movq	%r14, %rcx
	movq	%r12, %rsi
	movq	168(%r12), %rdx
	call	_ZN2v88internal12_GLOBAL__N_129ExecuteUnoptimizedCompileJobsEPNS0_9ParseInfoEPNS0_15FunctionLiteralEPNS0_19AccountingAllocatorEPSt12forward_listISt10unique_ptrINS0_25UnoptimizedCompilationJobESt14default_deleteISA_EESaISD_EE
	movq	-80(%rbp), %rax
	movq	0(%r13), %rdi
	movq	$0, -80(%rbp)
	movq	%rax, 0(%r13)
	testq	%rdi, %rdi
	je	.L202
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L202
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L222:
	leaq	-72(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L202
.L220:
	leaq	-72(%rbp), %rsi
	movq	%rdi, -80(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L224:
	leaq	-72(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L201
.L223:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25182:
	.size	_ZN2v88internal12_GLOBAL__N_123GenerateUnoptimizedCodeEPNS0_9ParseInfoEPNS0_19AccountingAllocatorEPSt12forward_listISt10unique_ptrINS0_25UnoptimizedCompilationJobESt14default_deleteIS8_EESaISB_EE, .-_ZN2v88internal12_GLOBAL__N_123GenerateUnoptimizedCodeEPNS0_9ParseInfoEPNS0_19AccountingAllocatorEPSt12forward_listISt10unique_ptrINS0_25UnoptimizedCompilationJobESt14default_deleteIS8_EESaISB_EE
	.section	.rodata._ZN2v88internal12_GLOBAL__N_19NewScriptEPNS0_7IsolateEPNS0_9ParseInfoENS0_6HandleINS0_6StringEEENS0_8Compiler13ScriptDetailsENS_19ScriptOriginOptionsENS0_11NativesFlagE.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"disabled-by-default-v8.compile"
	.section	.rodata._ZN2v88internal12_GLOBAL__N_19NewScriptEPNS0_7IsolateEPNS0_9ParseInfoENS0_6HandleINS0_6StringEEENS0_8Compiler13ScriptDetailsENS_19ScriptOriginOptionsENS0_11NativesFlagE.str1.1,"aMS",@progbits,1
.LC7:
	.string	"snapshot"
.LC8:
	.string	"Script"
	.section	.text._ZN2v88internal12_GLOBAL__N_19NewScriptEPNS0_7IsolateEPNS0_9ParseInfoENS0_6HandleINS0_6StringEEENS0_8Compiler13ScriptDetailsENS_19ScriptOriginOptionsENS0_11NativesFlagE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_19NewScriptEPNS0_7IsolateEPNS0_9ParseInfoENS0_6HandleINS0_6StringEEENS0_8Compiler13ScriptDetailsENS_19ScriptOriginOptionsENS0_11NativesFlagE, @function
_ZN2v88internal12_GLOBAL__N_19NewScriptEPNS0_7IsolateEPNS0_9ParseInfoENS0_6HandleINS0_6StringEEENS0_8Compiler13ScriptDetailsENS_19ScriptOriginOptionsENS0_11NativesFlagE:
.LFB25327:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movq	%rbx, %rsi
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal9ParseInfo12CreateScriptEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS_19ScriptOriginOptionsENS0_11NativesFlagE@PLT
	movq	%rax, %r12
	movq	24(%rbp), %rax
	testq	%rax, %rax
	je	.L226
	movq	(%r12), %r14
	movq	(%rax), %r13
	movq	%r13, 15(%r14)
	leaq	15(%r14), %rsi
	testb	$1, %r13b
	je	.L249
	movq	%r13, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L295
	testb	$24, %al
	je	.L249
.L305:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L296
	.p2align 4,,10
	.p2align 3
.L249:
	movslq	16(%rbp), %rax
	movq	(%r12), %rdx
	salq	$32, %rax
	movq	%rax, 23(%rdx)
	movslq	20(%rbp), %rax
	movq	(%r12), %rdx
	salq	$32, %rax
	movq	%rax, 31(%rdx)
.L226:
	movq	32(%rbp), %rax
	testq	%rax, %rax
	je	.L230
	movq	(%r12), %r14
	movq	(%rax), %r13
	movq	%r13, 111(%r14)
	leaq	111(%r14), %rsi
	testb	$1, %r13b
	je	.L230
	movq	%r13, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L297
	testb	$24, %al
	je	.L230
.L307:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L298
	.p2align 4,,10
	.p2align 3
.L230:
	movq	40(%rbp), %rax
	testq	%rax, %rax
	je	.L234
	movq	(%r12), %r14
	movq	(%rax), %r13
	movq	%r13, 119(%r14)
	leaq	119(%r14), %rsi
	testb	$1, %r13b
	je	.L234
	movq	%r13, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L299
	testb	$24, %al
	je	.L234
.L306:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L300
	.p2align 4,,10
	.p2align 3
.L234:
	movq	41016(%rbx), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	jne	.L301
.L238:
	movq	_ZZN2v88internal12_GLOBAL__N_19NewScriptEPNS0_7IsolateEPNS0_9ParseInfoENS0_6HandleINS0_6StringEEENS0_8Compiler13ScriptDetailsENS_19ScriptOriginOptionsENS0_11NativesFlagEE29trace_event_unique_atomic1966(%rip), %rdx
	movq	%rdx, %r13
	testq	%rdx, %rdx
	je	.L302
.L240:
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L303
.L242:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L304
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L295:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rsi, -136(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-136(%rbp), %rsi
	testb	$24, %al
	jne	.L305
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L299:
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rsi, -136(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-136(%rbp), %rsi
	testb	$24, %al
	jne	.L306
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L297:
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rsi, -136(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-136(%rbp), %rsi
	testb	$24, %al
	jne	.L307
	jmp	.L230
	.p2align 4,,10
	.p2align 3
.L303:
	movq	(%r12), %rax
	leaq	-104(%rbp), %rdi
	leaq	-112(%rbp), %rsi
	movq	_ZN2v88internal6Script11kTraceScopeE(%rip), %rbx
	movq	63(%rax), %r14
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal6Script13ToTracedValueEv@PLT
	leaq	.LC7(%rip), %rax
	movb	$8, -113(%rbp)
	movq	%rax, -96(%rbp)
	movq	-104(%rbp), %rax
	movq	$0, -72(%rbp)
	movq	$0, -104(%rbp)
	movq	%rax, -88(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L308
.L243:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L244
	movq	(%rdi), %rax
	call	*8(%rax)
.L244:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L245
	movq	(%rdi), %rax
	call	*8(%rax)
.L245:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L242
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L302:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L309
.L241:
	movq	%r13, _ZZN2v88internal12_GLOBAL__N_19NewScriptEPNS0_7IsolateEPNS0_9ParseInfoENS0_6HandleINS0_6StringEEENS0_8Compiler13ScriptDetailsENS_19ScriptOriginOptionsENS0_11NativesFlagEE29trace_event_unique_atomic1966(%rip)
	jmp	.L240
	.p2align 4,,10
	.p2align 3
.L301:
	movq	(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal6Logger13ScriptDetailsENS0_6ScriptE@PLT
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L296:
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L298:
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L230
	.p2align 4,,10
	.p2align 3
.L300:
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L309:
	leaq	.LC6(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L308:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	movq	%r14, %r9
	movq	%rbx, %r8
	pushq	$2
	sarq	$32, %r9
	movl	$79, %esi
	leaq	.LC8(%rip), %rcx
	pushq	%rdx
	leaq	-88(%rbp), %rdx
	pushq	%rdx
	leaq	-113(%rbp), %rdx
	pushq	%rdx
	leaq	-96(%rbp), %rdx
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$1
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L243
.L304:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25327:
	.size	_ZN2v88internal12_GLOBAL__N_19NewScriptEPNS0_7IsolateEPNS0_9ParseInfoENS0_6HandleINS0_6StringEEENS0_8Compiler13ScriptDetailsENS_19ScriptOriginOptionsENS0_11NativesFlagE, .-_ZN2v88internal12_GLOBAL__N_19NewScriptEPNS0_7IsolateEPNS0_9ParseInfoENS0_6HandleINS0_6StringEEENS0_8Compiler13ScriptDetailsENS_19ScriptOriginOptionsENS0_11NativesFlagE
	.section	.text._ZNK2v88internal10JSFunction19has_feedback_vectorEv,"axG",@progbits,_ZNK2v88internal10JSFunction19has_feedback_vectorEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal10JSFunction19has_feedback_vectorEv
	.type	_ZNK2v88internal10JSFunction19has_feedback_vectorEv, @function
_ZNK2v88internal10JSFunction19has_feedback_vectorEv:
.LFB12311:
	.cfi_startproc
	endbr64
	movabsq	$287762808832, %rdx
	movq	(%rdi), %rax
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	cmpq	%rdx, %rax
	je	.L314
	testb	$1, %al
	jne	.L313
.L315:
	movq	(%rdi), %rax
	movq	39(%rax), %rax
	movq	7(%rax), %rax
	movq	-1(%rax), %rax
	cmpw	$155, 11(%rax)
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L313:
	movq	-1(%rax), %rdx
	cmpw	$165, 11(%rdx)
	jne	.L316
.L314:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L316:
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L315
	jmp	.L314
	.cfi_endproc
.LFE12311:
	.size	_ZNK2v88internal10JSFunction19has_feedback_vectorEv, .-_ZNK2v88internal10JSFunction19has_feedback_vectorEv
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB21573:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L323
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L326
.L317:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L323:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L326:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L317
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE21573:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.text._ZN2v88internal25UnoptimizedCompilationJob10ExecuteJobEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal25UnoptimizedCompilationJob10ExecuteJobEv
	.type	_ZN2v88internal25UnoptimizedCompilationJob10ExecuteJobEv, @function
_ZN2v88internal25UnoptimizedCompilationJob10ExecuteJobEv:
.LFB25151:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %r13
	movq	(%rbx), %rax
	call	*16(%rax)
	cmpl	$1, %eax
	movl	%eax, %r12d
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
	movl	%eax, 8(%rbx)
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	subq	%r13, %rax
	addq	%rax, 48(%rbx)
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25151:
	.size	_ZN2v88internal25UnoptimizedCompilationJob10ExecuteJobEv, .-_ZN2v88internal25UnoptimizedCompilationJob10ExecuteJobEv
	.section	.text._ZN2v88internal25UnoptimizedCompilationJob11FinalizeJobENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal25UnoptimizedCompilationJob11FinalizeJobENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateE
	.type	_ZN2v88internal25UnoptimizedCompilationJob11FinalizeJobENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateE, @function
_ZN2v88internal25UnoptimizedCompilationJob11FinalizeJobENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateE:
.LFB25155:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r15
	movq	%rsi, %r14
	movq	%rdx, %rsi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r15, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb0EEC1EPNS0_7IsolateE@PLT
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%rax, %r13
	movq	(%rbx), %rax
	call	*24(%rax)
	testl	%eax, %eax
	movl	%eax, %r12d
	setne	%al
	movzbl	%al, %eax
	addl	$3, %eax
	movl	%eax, 8(%rbx)
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	%r15, %rdi
	subq	%r13, %rax
	addq	%rax, 56(%rbx)
	call	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb0EED1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L336
	addq	$40, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L336:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25155:
	.size	_ZN2v88internal25UnoptimizedCompilationJob11FinalizeJobENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateE, .-_ZN2v88internal25UnoptimizedCompilationJob11FinalizeJobENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateE
	.section	.text._ZNK2v88internal25UnoptimizedCompilationJob22RecordCompilationStatsEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal25UnoptimizedCompilationJob22RecordCompilationStatsEPNS0_7IsolateE
	.type	_ZNK2v88internal25UnoptimizedCompilationJob22RecordCompilationStatsEPNS0_7IsolateE, @function
_ZNK2v88internal25UnoptimizedCompilationJob22RecordCompilationStatsEPNS0_7IsolateE:
.LFB25156:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	40(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	40(%rdx), %rax
	testq	%rax, %rax
	je	.L338
	movq	(%rax), %r12
	leaq	-48(%rbp), %rdi
	movq	15(%r12), %rax
	movslq	11(%r12), %rbx
	movq	%rax, -48(%rbp)
	movq	-1(%rax), %rsi
	addl	$61, %ebx
	andl	$-8, %ebx
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	23(%r12), %rdx
	addl	%ebx, %eax
	movslq	11(%rdx), %rbx
	addl	$23, %ebx
	andl	$-8, %ebx
	addl	%eax, %ebx
	movq	31(%r12), %rax
	testb	$1, %al
	jne	.L362
.L339:
	andq	$-262144, %r12
	movq	24(%r12), %rdx
	cmpq	-37280(%rdx), %rax
	je	.L363
	movq	7(%rax), %rax
.L342:
	movslq	11(%rax), %rax
	addl	$23, %eax
	andl	$-8, %eax
	addl	%eax, %ebx
.L344:
	movq	40960(%r13), %r12
	cmpb	$0, 7744(%r12)
	je	.L345
	movq	7736(%r12), %rax
.L346:
	testq	%rax, %rax
	je	.L347
	addl	%ebx, (%rax)
.L347:
	cmpb	$0, 7776(%r12)
	je	.L348
	movq	7768(%r12), %rax
.L349:
	testq	%rax, %rax
	je	.L337
	addl	$1, (%rax)
.L337:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L364
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L362:
	.cfi_restore_state
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	leaq	-37592(%rdx), %rcx
	cmpq	-37504(%rdx), %rax
	je	.L344
	cmpq	312(%rcx), %rax
	je	.L344
	movq	-1(%rax), %rdx
	cmpw	$70, 11(%rdx)
	jne	.L339
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L348:
	movb	$1, 7776(%r12)
	leaq	7752(%r12), %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 7768(%r12)
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L345:
	movb	$1, 7744(%r12)
	leaq	7720(%r12), %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 7736(%r12)
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L338:
	movq	48(%rdx), %rax
	leaq	-48(%rbp), %rdi
	movq	(%rax), %rax
	movq	%rax, -48(%rbp)
	movq	-1(%rax), %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movl	%eax, %ebx
	jmp	.L344
	.p2align 4,,10
	.p2align 3
.L363:
	movq	-36616(%rdx), %rax
	jmp	.L342
.L364:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25156:
	.size	_ZNK2v88internal25UnoptimizedCompilationJob22RecordCompilationStatsEPNS0_7IsolateE, .-_ZNK2v88internal25UnoptimizedCompilationJob22RecordCompilationStatsEPNS0_7IsolateE
	.section	.text._ZN2v88internal12_GLOBAL__N_133FinalizeUnoptimizedCompilationJobEPNS0_25UnoptimizedCompilationJobENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_133FinalizeUnoptimizedCompilationJobEPNS0_25UnoptimizedCompilationJobENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateE, @function
_ZN2v88internal12_GLOBAL__N_133FinalizeUnoptimizedCompilationJobEPNS0_25UnoptimizedCompilationJobENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateE:
.LFB25170:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	32(%rdi), %rsi
	movq	0(%r13), %rcx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	40(%rdi), %rax
	movq	%rsi, -120(%rbp)
	movq	16(%rax), %rbx
	movq	%rax, -104(%rbp)
	movl	47(%rcx), %eax
	movl	4(%rbx), %edx
	andb	$-9, %ah
	andl	$2048, %edx
	orl	%edx, %eax
	movl	%eax, 47(%rcx)
	movq	0(%r13), %rax
	movl	4(%rbx), %edx
	movl	47(%rax), %ecx
	andb	$4, %ch
	je	.L508
.L366:
	leaq	-80(%rbp), %r15
	movq	%rbx, %rsi
	movq	%rax, -80(%rbp)
	movq	%r15, %rdi
	call	_ZN2v88internal18SharedFunctionInfo50UpdateAndFinalizeExpectedNofPropertiesFromEstimateEPNS0_15FunctionLiteralE@PLT
	testl	$1044480, 4(%rbx)
	jne	.L509
.L367:
	movq	0(%r13), %rcx
	movq	%rbx, %rdi
	movq	%rcx, -112(%rbp)
	call	_ZNK2v88internal15FunctionLiteral26SafeToSkipArgumentsAdaptorEv@PLT
	movq	-112(%rbp), %rcx
	movq	%r14, %rsi
	movq	%r15, %rdi
	sall	$30, %eax
	movl	47(%rcx), %edx
	andl	$-1073741825, %edx
	orl	%eax, %edx
	movl	%edx, 47(%rcx)
	call	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb0EEC1EPNS0_7IsolateE@PLT
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rbx
	movq	(%r12), %rax
	call	*24(%rax)
	movl	%eax, -112(%rbp)
	testl	%eax, %eax
	jne	.L368
	movl	$3, 8(%r12)
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	%r15, %rdi
	subq	%rbx, %rax
	addq	%rax, 56(%r12)
	call	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb0EED1Ev@PLT
	movq	-104(%rbp), %rdi
	call	_ZNK2v88internal26UnoptimizedCompilationInfo5scopeEv@PLT
	movq	0(%r13), %rbx
	movq	104(%rax), %rax
	movq	(%rax), %rax
	movq	%rax, -88(%rbp)
	movq	15(%rbx), %rsi
	leaq	15(%rbx), %rax
	movq	%rax, -128(%rbp)
	testb	$1, %sil
	jne	.L510
.L369:
	leaq	-88(%rbp), %r9
	movq	%r9, %rdi
	movq	%r9, -128(%rbp)
	call	_ZN2v88internal9ScopeInfo15SetFunctionNameENS0_6ObjectE@PLT
	movq	15(%rbx), %rax
	movq	-128(%rbp), %r9
	testb	$1, %al
	jne	.L511
.L372:
	movq	7(%rbx), %rax
	testb	$1, %al
	jne	.L512
.L374:
	movq	-88(%rbp), %rdx
	movq	%rdx, 15(%rbx)
	testb	$1, %dl
	je	.L437
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -128(%rbp)
	testl	$262144, %eax
	jne	.L513
	testb	$24, %al
	je	.L437
.L522:
	movq	%rbx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L437
	leaq	15(%rbx), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L437:
	movq	-104(%rbp), %rax
	cmpq	$0, 40(%rax)
	je	.L394
	movq	16(%rax), %rax
	movq	40(%rax), %rdi
	call	_ZNK2v88internal5Scope11IsAsmModuleEv@PLT
	testb	%al, %al
	jne	.L514
.L395:
	movq	-104(%rbp), %rax
	cmpb	$0, _ZN2v88internal36FLAG_interpreted_frames_native_stackE(%rip)
	movq	40(%rax), %rcx
	je	.L515
	leaq	4776(%r14), %rsi
	movq	%r14, %rdi
	movq	%rcx, -128(%rbp)
	call	_ZN2v88internal7Factory8CopyCodeENS0_6HandleINS0_4CodeEEE@PLT
	movl	$1, %edx
	movl	$91, %esi
	movq	%r14, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal7Factory9NewStructENS0_12InstanceTypeENS0_14AllocationTypeE@PLT
	movq	-128(%rbp), %rcx
	movq	(%rax), %rdi
	movq	%rax, %rbx
	movq	(%rcx), %rdx
	leaq	7(%rdi), %rsi
	movq	%rdx, 7(%rdi)
	testb	$1, %dl
	je	.L434
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -128(%rbp)
	testl	$262144, %eax
	je	.L402
	movq	%rdx, -160(%rbp)
	movq	%rsi, -152(%rbp)
	movq	%rdi, -144(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-128(%rbp), %rcx
	movq	-160(%rbp), %rdx
	movq	-152(%rbp), %rsi
	movq	-144(%rbp), %rdi
	movq	8(%rcx), %rax
.L402:
	testb	$24, %al
	je	.L434
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L516
	.p2align 4,,10
	.p2align 3
.L434:
	movq	-136(%rbp), %rax
	movq	(%rbx), %rdi
	movq	(%rax), %rdx
	leaq	15(%rdi), %rsi
	movq	%rdx, 15(%rdi)
	testb	$1, %dl
	je	.L433
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -128(%rbp)
	testl	$262144, %eax
	je	.L405
	movq	%rdx, -160(%rbp)
	movq	%rsi, -152(%rbp)
	movq	%rdi, -144(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-128(%rbp), %rcx
	movq	-160(%rbp), %rdx
	movq	-152(%rbp), %rsi
	movq	-144(%rbp), %rdi
	movq	8(%rcx), %rax
.L405:
	testb	$24, %al
	je	.L433
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L517
	.p2align 4,,10
	.p2align 3
.L433:
	movq	0(%r13), %rdi
	movq	(%rbx), %rbx
	movq	%rbx, 7(%rdi)
	leaq	7(%rdi), %rsi
	testb	$1, %bl
	je	.L432
	movq	%rbx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -128(%rbp)
	testl	$262144, %eax
	je	.L408
	movq	%rbx, %rdx
	movq	%rsi, -152(%rbp)
	movq	%rdi, -144(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-128(%rbp), %rcx
	movq	-152(%rbp), %rsi
	movq	-144(%rbp), %rdi
	movq	8(%rcx), %rax
.L408:
	testb	$24, %al
	je	.L432
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L518
	.p2align 4,,10
	.p2align 3
.L432:
	movq	-120(%rbp), %rax
	movq	%r15, %rdi
	movq	80(%rax), %rbx
	movq	0(%r13), %rax
	movq	%rax, -80(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	movq	%rbx, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal6Script13GetLineNumberENS0_6HandleIS1_EEi@PLT
	movq	%r15, %rdi
	addl	$1, %eax
	movl	%eax, -144(%rbp)
	movq	0(%r13), %rax
	movq	%rax, -80(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	movq	%rbx, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal6Script15GetColumnNumberENS0_6HandleIS1_EEi@PLT
	addl	$1, %eax
	movl	%eax, -128(%rbp)
	movq	(%rbx), %rax
	movq	15(%rax), %rbx
	testb	$1, %bl
	jne	.L519
.L410:
	movq	128(%r14), %rax
	movq	%rax, -152(%rbp)
.L411:
	movq	0(%r13), %rax
	movq	41488(%r14), %rbx
	movq	%rax, -160(%rbp)
	movq	-136(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -168(%rbp)
	leaq	56(%rbx), %rax
	movq	%rax, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L413
	movq	%r12, -176(%rbp)
	movl	-144(%rbp), %r12d
	movq	%r13, -144(%rbp)
	movq	%rbx, %r13
	movq	-168(%rbp), %rbx
	movq	%r14, -168(%rbp)
	movq	-152(%rbp), %r14
	movq	%r15, -152(%rbp)
	movq	-160(%rbp), %r15
	.p2align 4,,10
	.p2align 3
.L412:
	movq	8(%r13), %rdi
	movl	-128(%rbp), %eax
	subq	$8, %rsp
	movq	%rbx, %rdx
	movl	%r12d, %r9d
	movq	%r14, %r8
	movq	%r15, %rcx
	movl	$12, %esi
	movq	(%rdi), %r10
	pushq	%rax
	call	*40(%r10)
	movq	0(%r13), %r13
	popq	%rax
	popq	%rdx
	testq	%r13, %r13
	jne	.L412
	movq	-176(%rbp), %r12
	movq	-144(%rbp), %r13
	movq	-168(%rbp), %r14
	movq	-152(%rbp), %r15
.L413:
	movq	-136(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
.L400:
	movq	-104(%rbp), %rax
	movq	%r14, %rdi
	leaq	56(%rax), %rsi
	call	_ZN2v88internal16FeedbackMetadata3NewEPNS0_7IsolateEPKNS0_18FeedbackVectorSpecE@PLT
	movq	0(%r13), %rdi
	movq	(%rax), %rbx
	leaq	23(%rdi), %rsi
	movq	%rbx, 23(%rdi)
	testb	$1, %bl
	je	.L417
.L507:
	movq	%rbx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -128(%rbp)
	testl	$262144, %eax
	je	.L422
	movq	%rbx, %rdx
	movq	%rsi, -144(%rbp)
	movq	%rdi, -136(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-128(%rbp), %rcx
	movq	-144(%rbp), %rsi
	movq	-136(%rbp), %rdi
	movq	8(%rcx), %rax
.L422:
	testb	$24, %al
	je	.L417
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L417
	movq	%rbx, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L417:
	movq	-104(%rbp), %rax
	cmpq	$0, 32(%rax)
	je	.L425
	movq	0(%r13), %rax
	movq	%r15, %rdi
	movq	%rax, -80(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo15HasCoverageInfoEv@PLT
	testb	%al, %al
	je	.L520
.L425:
	movq	-120(%rbp), %rax
	movl	8(%rax), %eax
	testb	$1, %al
	je	.L426
	movq	-104(%rbp), %rax
	movl	(%rax), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%r15d, %r15d
	andl	$7, %r15d
	addl	$10, %r15d
.L427:
	movq	40(%r12), %rax
	movq	40(%rax), %rcx
	testq	%rcx, %rcx
	je	.L428
	movq	%rcx, %rbx
.L429:
	leaq	48(%r12), %rdi
	call	_ZNK2v84base9TimeDelta15InMillisecondsFEv@PLT
	leaq	56(%r12), %rdi
	movsd	%xmm0, -104(%rbp)
	call	_ZNK2v84base9TimeDelta15InMillisecondsFEv@PLT
	movq	32(%r12), %rax
	movq	%r13, %rsi
	movl	%r15d, %edi
	addsd	-104(%rbp), %xmm0
	movq	%r14, %r9
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movq	80(%rax), %rdx
	call	_ZN2v88internal12_GLOBAL__N_122LogFunctionCompilationENS0_17CodeEventListener16LogEventsAndTagsENS0_6HandleINS0_18SharedFunctionInfoEEENS4_INS0_6ScriptEEENS4_INS0_12AbstractCodeEEEbdPNS0_7IsolateE
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNK2v88internal25UnoptimizedCompilationJob22RecordCompilationStatsEPNS0_7IsolateE
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L368:
	movl	$4, 8(%r12)
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	%r15, %rdi
	subq	%rbx, %rax
	addq	%rax, 56(%r12)
	call	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb0EED1Ev@PLT
.L365:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L521
	movl	-112(%rbp), %eax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L509:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	%r15, %rdi
	movq	%rax, -80(%rbp)
	movl	4(%rbx), %esi
	shrl	$12, %esi
	call	_ZN2v88internal18SharedFunctionInfo19DisableOptimizationENS0_13BailoutReasonE@PLT
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L508:
	movl	47(%rax), %ecx
	sall	$7, %edx
	andl	$536870912, %edx
	andl	$-536870913, %ecx
	orl	%ecx, %edx
	movl	%edx, 47(%rax)
	movq	0(%r13), %rax
	jmp	.L366
	.p2align 4,,10
	.p2align 3
.L510:
	movq	-1(%rsi), %rax
	cmpw	$136, 11(%rax)
	jne	.L369
	movq	%r15, %rdi
	movq	%rsi, -80(%rbp)
	call	_ZNK2v88internal9ScopeInfo12FunctionNameEv@PLT
	movq	%rax, %rsi
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L513:
	leaq	15(%rbx), %rsi
	movq	%rbx, %rdi
	movq	%rdx, -136(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-128(%rbp), %rcx
	movq	-136(%rbp), %rdx
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L522
	jmp	.L437
	.p2align 4,,10
	.p2align 3
.L512:
	movq	-1(%rax), %rdx
	cmpw	$165, 11(%rdx)
	jne	.L523
.L373:
	movq	15(%rbx), %rax
	testb	$1, %al
	jne	.L524
.L375:
	movq	7(%rbx), %rax
	testb	$1, %al
	jne	.L525
.L380:
	movq	%rbx, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-37464(%rax), %rax
.L379:
	movl	11(%rax), %ecx
	testl	%ecx, %ecx
	je	.L374
	movq	15(%rbx), %rax
	testb	$1, %al
	jne	.L526
.L384:
	movq	7(%rbx), %rax
	testb	$1, %al
	jne	.L527
.L387:
	movq	%rbx, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-37464(%rax), %rsi
.L386:
	movq	%r9, %rdi
	call	_ZN2v88internal9ScopeInfo23SetInferredFunctionNameENS0_6StringE@PLT
	jmp	.L374
	.p2align 4,,10
	.p2align 3
.L515:
	movq	0(%r13), %rdi
	movq	(%rcx), %rbx
	movq	%rbx, 7(%rdi)
	leaq	7(%rdi), %rsi
	testb	$1, %bl
	je	.L400
	movq	%rbx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -128(%rbp)
	testl	$262144, %eax
	jne	.L528
.L398:
	testb	$24, %al
	je	.L400
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L400
	movq	%rbx, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L400
	.p2align 4,,10
	.p2align 3
.L514:
	movq	0(%r13), %rdx
	movl	47(%rdx), %eax
	orb	$64, %ah
	movl	%eax, 47(%rdx)
	jmp	.L395
	.p2align 4,,10
	.p2align 3
.L426:
	andl	$256, %eax
	cmpl	$1, %eax
	sbbl	%r15d, %r15d
	andl	$-4, %r15d
	addl	$15, %r15d
	jmp	.L427
	.p2align 4,,10
	.p2align 3
.L511:
	movq	-1(%rax), %rdx
	cmpw	$136, 11(%rdx)
	jne	.L372
	movq	%r15, %rdi
	movq	%rax, -80(%rbp)
	call	_ZNK2v88internal9ScopeInfo23HasInferredFunctionNameEv@PLT
	movq	-128(%rbp), %r9
	testb	%al, %al
	je	.L374
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L394:
	movq	48(%rax), %rax
	movq	0(%r13), %rdi
	movq	(%rax), %rbx
	leaq	7(%rdi), %rsi
	movq	%rbx, 7(%rdi)
	testb	$1, %bl
	je	.L436
	movq	%rbx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -128(%rbp)
	testl	$262144, %eax
	jne	.L529
.L419:
	testb	$24, %al
	je	.L436
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L436
	movq	%rbx, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L436:
	movq	0(%r13), %rdi
	movq	1040(%r14), %rbx
	movq	%rbx, 23(%rdi)
	leaq	23(%rdi), %rsi
	testb	$1, %bl
	jne	.L507
	jmp	.L417
	.p2align 4,,10
	.p2align 3
.L520:
	movq	-104(%rbp), %rax
	movq	41472(%r14), %rdi
	movq	%r13, %rsi
	movq	32(%rax), %rdx
	call	_ZN2v88internal5Debug19InstallCoverageInfoENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_12CoverageInfoEEE@PLT
	jmp	.L425
	.p2align 4,,10
	.p2align 3
.L428:
	leaq	41184(%r14), %rdi
	movl	$69, %esi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	movq	%rax, %rbx
	jmp	.L429
	.p2align 4,,10
	.p2align 3
.L519:
	movq	-1(%rbx), %rax
	cmpw	$63, 11(%rax)
	ja	.L410
	movq	%rbx, -152(%rbp)
	jmp	.L411
	.p2align 4,,10
	.p2align 3
.L523:
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L374
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L529:
	movq	%rbx, %rdx
	movq	%rsi, -144(%rbp)
	movq	%rdi, -136(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-128(%rbp), %rcx
	movq	-144(%rbp), %rsi
	movq	-136(%rbp), %rdi
	movq	8(%rcx), %rax
	jmp	.L419
	.p2align 4,,10
	.p2align 3
.L528:
	movq	%rbx, %rdx
	movq	%rsi, -144(%rbp)
	movq	%rdi, -136(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-128(%rbp), %rcx
	movq	-144(%rbp), %rsi
	movq	-136(%rbp), %rdi
	movq	8(%rcx), %rax
	jmp	.L398
	.p2align 4,,10
	.p2align 3
.L516:
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L434
	.p2align 4,,10
	.p2align 3
.L517:
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L433
	.p2align 4,,10
	.p2align 3
.L518:
	movq	%rbx, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L432
	.p2align 4,,10
	.p2align 3
.L524:
	movq	-1(%rax), %rdx
	cmpw	$136, 11(%rdx)
	jne	.L375
	movq	%r15, %rdi
	movq	%r9, -128(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZNK2v88internal9ScopeInfo23HasInferredFunctionNameEv@PLT
	movq	-128(%rbp), %r9
	testb	%al, %al
	je	.L380
	movq	%r15, %rdi
	call	_ZNK2v88internal9ScopeInfo20InferredFunctionNameEv@PLT
	movq	-128(%rbp), %r9
	testb	$1, %al
	je	.L380
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L380
	jmp	.L379
	.p2align 4,,10
	.p2align 3
.L525:
	movq	-1(%rax), %rdx
	cmpw	$165, 11(%rdx)
	je	.L383
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L380
.L383:
	movq	7(%rbx), %rax
	movq	7(%rax), %rax
	jmp	.L379
	.p2align 4,,10
	.p2align 3
.L526:
	movq	-1(%rax), %rdx
	cmpw	$136, 11(%rdx)
	jne	.L384
	movq	%r15, %rdi
	movq	%r9, -128(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZNK2v88internal9ScopeInfo23HasInferredFunctionNameEv@PLT
	movq	-128(%rbp), %r9
	testb	%al, %al
	je	.L387
	movq	%r15, %rdi
	call	_ZNK2v88internal9ScopeInfo20InferredFunctionNameEv@PLT
	movq	-128(%rbp), %r9
	testb	$1, %al
	movq	%rax, %rsi
	je	.L387
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L387
	jmp	.L386
	.p2align 4,,10
	.p2align 3
.L527:
	movq	-1(%rax), %rdx
	cmpw	$165, 11(%rdx)
	je	.L390
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L387
.L390:
	movq	7(%rbx), %rax
	movq	7(%rax), %rsi
	jmp	.L386
.L521:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25170:
	.size	_ZN2v88internal12_GLOBAL__N_133FinalizeUnoptimizedCompilationJobEPNS0_25UnoptimizedCompilationJobENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateE, .-_ZN2v88internal12_GLOBAL__N_133FinalizeUnoptimizedCompilationJobEPNS0_25UnoptimizedCompilationJobENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateE
	.section	.text._ZN2v88internal12_GLOBAL__N_123FinalizeUnoptimizedCodeEPNS0_9ParseInfoEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_25UnoptimizedCompilationJobEPSt12forward_listISt10unique_ptrIS9_St14default_deleteIS9_EESaISF_EE.part.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_123FinalizeUnoptimizedCodeEPNS0_9ParseInfoEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_25UnoptimizedCompilationJobEPSt12forward_listISt10unique_ptrIS9_St14default_deleteIS9_EESaISF_EE.part.0, @function
_ZN2v88internal12_GLOBAL__N_123FinalizeUnoptimizedCodeEPNS0_9ParseInfoEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_25UnoptimizedCompilationJobEPSt12forward_listISt10unique_ptrIS9_St14default_deleteIS9_EESaISF_EE.part.0:
.LFB34478:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -72(%rbp)
	movq	(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-64(%rbp), %rax
	movq	%rax, -80(%rbp)
	testq	%rbx, %rbx
	jne	.L531
	jmp	.L553
	.p2align 4,,10
	.p2align 3
.L573:
	testb	$1, %al
	jne	.L571
.L552:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L553
.L531:
	movq	-72(%rbp), %rax
	movq	-80(%rbp), %rdi
	movq	%r14, %rsi
	movq	80(%rax), %r15
	movq	8(%rbx), %rax
	movq	40(%rax), %rax
	movq	16(%rax), %r13
	movq	(%r15), %rax
	movq	%r13, %rdx
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6Script22FindSharedFunctionInfoEPNS0_7IsolateEPKNS0_15FunctionLiteralE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L572
	movq	(%rax), %rdx
	cmpq	$0, 80(%r13)
	leaq	7(%rdx), %rax
	je	.L535
	movq	7(%rdx), %rax
	testb	$1, %al
	jne	.L537
.L554:
	movq	(%r12), %rax
.L568:
	addq	$7, %rax
.L535:
	movabsq	$287762808832, %rcx
	movq	(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L573
.L550:
	movq	8(%rbx), %rdi
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal12_GLOBAL__N_133FinalizeUnoptimizedCompilationJobEPNS0_25UnoptimizedCompilationJobENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateE
	testl	%eax, %eax
	je	.L552
	xorl	%eax, %eax
	jmp	.L530
	.p2align 4,,10
	.p2align 3
.L571:
	movq	-1(%rax), %rdx
	cmpw	$165, 11(%rdx)
	je	.L550
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	je	.L550
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L531
	.p2align 4,,10
	.p2align 3
.L553:
	movq	-72(%rbp), %rax
	cmpq	$0, 216(%rax)
	je	.L569
	movq	80(%rax), %rdx
	leaq	176(%rax), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal30PendingCompilationErrorHandler14ReportWarningsEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEE@PLT
.L569:
	movl	$1, %eax
.L530:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L574
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L572:
	.cfi_restore_state
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal7Factory31NewSharedFunctionInfoForLiteralEPNS0_15FunctionLiteralENS0_6HandleINS0_6ScriptEEEb@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	addq	$7, %rax
	jmp	.L535
	.p2align 4,,10
	.p2align 3
.L537:
	movq	-1(%rax), %rax
	cmpw	$165, 11(%rax)
	movq	(%r12), %rax
	jne	.L568
	movq	7(%rax), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L566
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rdx
.L542:
	movq	41112(%r14), %rdi
	movq	7(%rsi), %rsi
	testq	%rdi, %rdi
	je	.L544
	movq	%rdx, -88(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-88(%rbp), %rdx
	movq	%rax, %r10
.L545:
	movq	80(%r13), %rdi
	movq	%r10, -96(%rbp)
	movq	%r14, %rsi
	movq	%rdx, -88(%rbp)
	movq	(%rdi), %rax
	call	*(%rax)
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %r10
	movq	%r14, %rdi
	movq	%rax, %r8
	movq	(%rdx), %rdx
	movq	%r10, %rsi
	movl	19(%rdx), %ecx
	movl	15(%rdx), %edx
	call	_ZN2v88internal7Factory33NewUncompiledDataWithPreparseDataENS0_6HandleINS0_6StringEEEiiNS2_INS0_12PreparseDataEEE@PLT
	movq	(%r12), %rdi
	movq	(%rax), %rdx
	leaq	7(%rdi), %rsi
	movq	%rdx, 7(%rdi)
	testb	$1, %dl
	je	.L554
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -88(%rbp)
	testl	$262144, %eax
	je	.L548
	movq	%rdx, -112(%rbp)
	movq	%rsi, -104(%rbp)
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rcx
	movq	-112(%rbp), %rdx
	movq	-104(%rbp), %rsi
	movq	-96(%rbp), %rdi
	movq	8(%rcx), %rax
.L548:
	testb	$24, %al
	je	.L554
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L554
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L554
	.p2align 4,,10
	.p2align 3
.L544:
	movq	41088(%r14), %r10
	cmpq	41096(%r14), %r10
	je	.L575
.L546:
	leaq	8(%r10), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%r10)
	jmp	.L545
	.p2align 4,,10
	.p2align 3
.L566:
	movq	41088(%r14), %rdx
	cmpq	41096(%r14), %rdx
	je	.L576
.L543:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%rdx)
	jmp	.L542
	.p2align 4,,10
	.p2align 3
.L575:
	movq	%r14, %rdi
	movq	%rsi, -96(%rbp)
	movq	%rdx, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rsi
	movq	-88(%rbp), %rdx
	movq	%rax, %r10
	jmp	.L546
	.p2align 4,,10
	.p2align 3
.L576:
	movq	%r14, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L543
.L574:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE34478:
	.size	_ZN2v88internal12_GLOBAL__N_123FinalizeUnoptimizedCodeEPNS0_9ParseInfoEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_25UnoptimizedCompilationJobEPSt12forward_listISt10unique_ptrIS9_St14default_deleteIS9_EESaISF_EE.part.0, .-_ZN2v88internal12_GLOBAL__N_123FinalizeUnoptimizedCodeEPNS0_9ParseInfoEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_25UnoptimizedCompilationJobEPSt12forward_listISt10unique_ptrIS9_St14default_deleteIS9_EESaISF_EE.part.0
	.section	.text._ZNK2v88internal25UnoptimizedCompilationJob25RecordFunctionCompilationENS0_17CodeEventListener16LogEventsAndTagsENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal25UnoptimizedCompilationJob25RecordFunctionCompilationENS0_17CodeEventListener16LogEventsAndTagsENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateE
	.type	_ZNK2v88internal25UnoptimizedCompilationJob25RecordFunctionCompilationENS0_17CodeEventListener16LogEventsAndTagsENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateE, @function
_ZNK2v88internal25UnoptimizedCompilationJob25RecordFunctionCompilationENS0_17CodeEventListener16LogEventsAndTagsENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateE:
.LFB25157:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	40(%rdi), %rax
	movq	40(%rax), %rcx
	testq	%rcx, %rcx
	je	.L578
	movq	%rcx, %r12
.L579:
	leaq	48(%rbx), %rdi
	call	_ZNK2v84base9TimeDelta15InMillisecondsFEv@PLT
	leaq	56(%rbx), %rdi
	movsd	%xmm0, -56(%rbp)
	call	_ZNK2v84base9TimeDelta15InMillisecondsFEv@PLT
	movq	32(%rbx), %rax
	movq	%r13, %r9
	movq	%r12, %rcx
	addsd	-56(%rbp), %xmm0
	movq	%r15, %rsi
	movl	%r14d, %edi
	xorl	%r8d, %r8d
	movq	80(%rax), %rdx
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12_GLOBAL__N_122LogFunctionCompilationENS0_17CodeEventListener16LogEventsAndTagsENS0_6HandleINS0_18SharedFunctionInfoEEENS4_INS0_6ScriptEEENS4_INS0_12AbstractCodeEEEbdPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L578:
	.cfi_restore_state
	leaq	41184(%r13), %rdi
	movl	$69, %esi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	movq	%rax, %r12
	jmp	.L579
	.cfi_endproc
.LFE25157:
	.size	_ZNK2v88internal25UnoptimizedCompilationJob25RecordFunctionCompilationENS0_17CodeEventListener16LogEventsAndTagsENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateE, .-_ZNK2v88internal25UnoptimizedCompilationJob25RecordFunctionCompilationENS0_17CodeEventListener16LogEventsAndTagsENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateE
	.section	.rodata._ZN2v88internal23OptimizedCompilationJob10PrepareJobEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC9:
	.string	"[compiling method "
.LC10:
	.string	" using "
.LC11:
	.string	" OSR"
.LC12:
	.string	"]"
	.section	.text._ZN2v88internal23OptimizedCompilationJob10PrepareJobEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23OptimizedCompilationJob10PrepareJobEPNS0_7IsolateE
	.type	_ZN2v88internal23OptimizedCompilationJob10PrepareJobEPNS0_7IsolateE, @function
_ZN2v88internal23OptimizedCompilationJob10PrepareJobEPNS0_7IsolateE:
.LFB25158:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-416(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$408, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb0EEC1EPNS0_7IsolateE@PLT
	cmpb	$0, _ZN2v88internal14FLAG_trace_optE(%rip)
	je	.L582
	movq	24(%rbx), %rax
	movl	8(%rax), %edx
	testl	%edx, %edx
	je	.L594
.L582:
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, %r14
	movq	(%rbx), %rax
	call	*16(%rax)
	cmpl	$1, %eax
	movl	%eax, %r12d
	sbbl	%eax, %eax
	andl	$-3, %eax
	addl	$4, %eax
	movl	%eax, 8(%rbx)
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	%r13, %rdi
	subq	%r14, %rax
	addq	%rax, 32(%rbx)
	call	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb0EED1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L595
	addq	$408, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L594:
	.cfi_restore_state
	leaq	-320(%rbp), %r15
	leaq	-400(%rbp), %r14
	movq	%r15, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	stdout(%rip), %rdx
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movq	%rax, -320(%rbp)
	movq	%r14, %rdi
	xorl	%eax, %eax
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movw	%ax, -96(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movl	$18, %edx
	movq	%r14, %rdi
	movq	%rax, -400(%rbp)
	leaq	.LC9(%rip), %rsi
	addq	$40, %rax
	movq	%rax, -320(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	24(%rbx), %rax
	leaq	-424(%rbp), %rsi
	movq	%r14, %rdi
	movq	32(%rax), %rax
	movq	(%rax), %rax
	movq	%rax, -424(%rbp)
	call	_ZN2v88internallsERSoRKNS0_5BriefE@PLT
	leaq	.LC10(%rip), %rsi
	movl	$7, %edx
	movq	%rax, %rdi
	movq	%rax, -440(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	56(%rbx), %rsi
	movq	-440(%rbp), %r8
	testq	%rsi, %rsi
	je	.L596
	movq	%rsi, %rdi
	movq	%r8, -448(%rbp)
	movq	%rsi, -440(%rbp)
	call	strlen@PLT
	movq	-448(%rbp), %r8
	movq	-440(%rbp), %rsi
	movq	%rax, %rdx
	movq	%r8, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L584:
	movq	24(%rbx), %rax
	cmpl	$-1, 56(%rax)
	je	.L585
	movl	$4, %edx
	leaq	.LC11(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L585:
	movq	%r14, %rdi
	movl	$1, %edx
	leaq	.LC12(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-400(%rbp), %rax
	movq	-24(%rax), %rax
	movq	-160(%rbp,%rax), %rdi
	testq	%rdi, %rdi
	je	.L597
	cmpb	$0, 56(%rdi)
	je	.L587
	movsbl	67(%rdi), %esi
.L588:
	movq	%r14, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	movq	%rax, -320(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-336(%rbp), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -400(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r15, %rdi
	movq	%rax, -400(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L582
	.p2align 4,,10
	.p2align 3
.L587:
	movq	%rdi, -440(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-440(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L588
	call	*%rax
	movsbl	%al, %esi
	jmp	.L588
	.p2align 4,,10
	.p2align 3
.L596:
	movq	(%r8), %rax
	movq	-24(%rax), %rdi
	addq	%r8, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L584
.L595:
	call	__stack_chk_fail@PLT
.L597:
	call	_ZSt16__throw_bad_castv@PLT
	.cfi_endproc
.LFE25158:
	.size	_ZN2v88internal23OptimizedCompilationJob10PrepareJobEPNS0_7IsolateE, .-_ZN2v88internal23OptimizedCompilationJob10PrepareJobEPNS0_7IsolateE
	.section	.text._ZN2v88internal23OptimizedCompilationJob10ExecuteJobEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23OptimizedCompilationJob10ExecuteJobEv
	.type	_ZN2v88internal23OptimizedCompilationJob10ExecuteJobEv, @function
_ZN2v88internal23OptimizedCompilationJob10ExecuteJobEv:
.LFB25159:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %r13
	movq	(%rbx), %rax
	call	*24(%rax)
	cmpl	$1, %eax
	movl	%eax, %r12d
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
	movl	%eax, 8(%rbx)
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	subq	%r13, %rax
	addq	%rax, 40(%rbx)
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25159:
	.size	_ZN2v88internal23OptimizedCompilationJob10ExecuteJobEv, .-_ZN2v88internal23OptimizedCompilationJob10ExecuteJobEv
	.section	.text._ZN2v88internal23OptimizedCompilationJob11FinalizeJobEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23OptimizedCompilationJob11FinalizeJobEPNS0_7IsolateE
	.type	_ZN2v88internal23OptimizedCompilationJob11FinalizeJobEPNS0_7IsolateE, @function
_ZN2v88internal23OptimizedCompilationJob11FinalizeJobEPNS0_7IsolateE:
.LFB25160:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-64(%rbp), %r14
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%r14, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb0EEC1EPNS0_7IsolateE@PLT
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, %r13
	movq	(%rbx), %rax
	call	*32(%rax)
	testl	%eax, %eax
	movl	%eax, %r12d
	setne	%al
	movzbl	%al, %eax
	addl	$3, %eax
	movl	%eax, 8(%rbx)
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	%r14, %rdi
	subq	%r13, %rax
	addq	%rax, 48(%rbx)
	call	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb0EED1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L607
	addq	$32, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L607:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25160:
	.size	_ZN2v88internal23OptimizedCompilationJob11FinalizeJobEPNS0_7IsolateE, .-_ZN2v88internal23OptimizedCompilationJob11FinalizeJobEPNS0_7IsolateE
	.section	.text._ZN2v88internal23OptimizedCompilationJob17RetryOptimizationENS0_13BailoutReasonE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23OptimizedCompilationJob17RetryOptimizationENS0_13BailoutReasonE
	.type	_ZN2v88internal23OptimizedCompilationJob17RetryOptimizationENS0_13BailoutReasonE, @function
_ZN2v88internal23OptimizedCompilationJob17RetryOptimizationENS0_13BailoutReasonE:
.LFB25161:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	24(%rdi), %rdi
	call	_ZN2v88internal24OptimizedCompilationInfo17RetryOptimizationENS0_13BailoutReasonE@PLT
	movl	$4, 8(%rbx)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25161:
	.size	_ZN2v88internal23OptimizedCompilationJob17RetryOptimizationENS0_13BailoutReasonE, .-_ZN2v88internal23OptimizedCompilationJob17RetryOptimizationENS0_13BailoutReasonE
	.section	.text._ZN2v88internal23OptimizedCompilationJob17AbortOptimizationENS0_13BailoutReasonE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23OptimizedCompilationJob17AbortOptimizationENS0_13BailoutReasonE
	.type	_ZN2v88internal23OptimizedCompilationJob17AbortOptimizationENS0_13BailoutReasonE, @function
_ZN2v88internal23OptimizedCompilationJob17AbortOptimizationENS0_13BailoutReasonE:
.LFB25162:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	24(%rdi), %rdi
	call	_ZN2v88internal24OptimizedCompilationInfo17AbortOptimizationENS0_13BailoutReasonE@PLT
	movl	$4, 8(%rbx)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25162:
	.size	_ZN2v88internal23OptimizedCompilationJob17AbortOptimizationENS0_13BailoutReasonE, .-_ZN2v88internal23OptimizedCompilationJob17AbortOptimizationENS0_13BailoutReasonE
	.section	.rodata._ZNK2v88internal23OptimizedCompilationJob22RecordCompilationStatsENS1_15CompilationModeEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC13:
	.string	"[optimizing "
	.section	.rodata._ZNK2v88internal23OptimizedCompilationJob22RecordCompilationStatsENS1_15CompilationModeEPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC14:
	.string	" - took %0.3f, %0.3f, %0.3f ms]\n"
	.align 8
.LC15:
	.string	"Compiled: %d functions with %d byte source size in %fms.\n"
	.section	.text._ZNK2v88internal23OptimizedCompilationJob22RecordCompilationStatsENS1_15CompilationModeEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal23OptimizedCompilationJob22RecordCompilationStatsENS1_15CompilationModeEPNS0_7IsolateE
	.type	_ZNK2v88internal23OptimizedCompilationJob22RecordCompilationStatsENS1_15CompilationModeEPNS0_7IsolateE, @function
_ZNK2v88internal23OptimizedCompilationJob22RecordCompilationStatsENS1_15CompilationModeEPNS0_7IsolateE:
.LFB25163:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	32(%rdi), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	40(%rbx), %r13
	leaq	48(%rbx), %r12
	subq	$88, %rsp
	movl	%esi, -108(%rbp)
	movq	%rdx, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	movq	%r14, %rdi
	movq	32(%rax), %r15
	call	_ZNK2v84base9TimeDelta15InMillisecondsFEv@PLT
	movq	%r13, %rdi
	movsd	%xmm0, -88(%rbp)
	call	_ZNK2v84base9TimeDelta15InMillisecondsFEv@PLT
	movq	%r12, %rdi
	movsd	%xmm0, -96(%rbp)
	call	_ZNK2v84base9TimeDelta15InMillisecondsFEv@PLT
	cmpb	$0, _ZN2v88internal14FLAG_trace_optE(%rip)
	movapd	%xmm0, %xmm2
	jne	.L626
.L613:
	cmpb	$0, _ZN2v88internal20FLAG_trace_opt_statsE(%rip)
	jne	.L627
.L614:
	call	_ZN2v84base9TimeTicks16IsHighResolutionEv@PLT
	testb	%al, %al
	je	.L612
	movq	-104(%rbp), %rax
	movq	%r14, %rdi
	movq	40960(%rax), %r15
	movq	24(%rbx), %rax
	cmpl	$-1, 56(%rax)
	je	.L616
	call	_ZNK2v84base9TimeDelta14InMicrosecondsEv@PLT
	leaq	4008(%r15), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	movq	%r13, %rdi
	call	_ZNK2v84base9TimeDelta14InMicrosecondsEv@PLT
	leaq	4056(%r15), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	movq	%r12, %rdi
	call	_ZNK2v84base9TimeDelta14InMicrosecondsEv@PLT
	leaq	4104(%r15), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	leaq	-64(%rbp), %rdi
	subq	16(%rbx), %rax
	movq	%rax, -64(%rbp)
	call	_ZNK2v84base9TimeDelta14InMicrosecondsEv@PLT
	leaq	4152(%r15), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
.L617:
	movq	24(%rbx), %rax
	leaq	2168(%r15), %rdi
	movabsq	$2361183241434822607, %rsi
	movq	160(%rax), %rdx
	shrq	$3, %rdx
	movq	%rdx, %rax
	mulq	%rsi
	shrq	$4, %rdx
	movl	%edx, %esi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
.L612:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L628
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L627:
	.cfi_restore_state
	movsd	-88(%rbp), %xmm0
	addsd	-96(%rbp), %xmm0
	leaq	-64(%rbp), %rdi
	movq	(%r15), %rax
	addl	$1, _ZZNK2v88internal23OptimizedCompilationJob22RecordCompilationStatsENS1_15CompilationModeEPNS0_7IsolateEE18compiled_functions(%rip)
	addsd	%xmm2, %xmm0
	addsd	_ZZNK2v88internal23OptimizedCompilationJob22RecordCompilationStatsENS1_15CompilationModeEPNS0_7IsolateEE16compilation_time(%rip), %xmm0
	movsd	%xmm0, _ZZNK2v88internal23OptimizedCompilationJob22RecordCompilationStatsENS1_15CompilationModeEPNS0_7IsolateEE16compilation_time(%rip)
	movq	23(%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal18SharedFunctionInfo10SourceSizeEv@PLT
	movsd	_ZZNK2v88internal23OptimizedCompilationJob22RecordCompilationStatsENS1_15CompilationModeEPNS0_7IsolateEE16compilation_time(%rip), %xmm0
	addl	_ZZNK2v88internal23OptimizedCompilationJob22RecordCompilationStatsENS1_15CompilationModeEPNS0_7IsolateEE9code_size(%rip), %eax
	leaq	.LC15(%rip), %rdi
	movl	_ZZNK2v88internal23OptimizedCompilationJob22RecordCompilationStatsENS1_15CompilationModeEPNS0_7IsolateEE18compiled_functions(%rip), %esi
	movl	%eax, %edx
	movl	%eax, _ZZNK2v88internal23OptimizedCompilationJob22RecordCompilationStatsENS1_15CompilationModeEPNS0_7IsolateEE9code_size(%rip)
	movl	$1, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L614
	.p2align 4,,10
	.p2align 3
.L626:
	leaq	.LC13(%rip), %rdi
	xorl	%eax, %eax
	movsd	%xmm0, -120(%rbp)
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	(%r15), %rax
	movq	stdout(%rip), %rsi
	leaq	-64(%rbp), %rdi
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal6Object10ShortPrintEP8_IO_FILE@PLT
	movsd	-120(%rbp), %xmm2
	movsd	-96(%rbp), %xmm1
	leaq	.LC14(%rip), %rdi
	movsd	-88(%rbp), %xmm0
	movl	$3, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movsd	-120(%rbp), %xmm2
	jmp	.L613
	.p2align 4,,10
	.p2align 3
.L616:
	call	_ZNK2v84base9TimeDelta14InMicrosecondsEv@PLT
	leaq	3624(%r15), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	movq	%r13, %rdi
	call	_ZNK2v84base9TimeDelta14InMicrosecondsEv@PLT
	leaq	3672(%r15), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	movq	%r12, %rdi
	leaq	-64(%rbp), %r12
	call	_ZNK2v84base9TimeDelta14InMicrosecondsEv@PLT
	leaq	3720(%r15), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	%r12, %rdi
	subq	16(%rbx), %rax
	movq	%rax, -64(%rbp)
	call	_ZNK2v84base9TimeDelta14InMicrosecondsEv@PLT
	leaq	3864(%r15), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	movq	48(%rbx), %rax
	addq	32(%rbx), %rax
	movq	$0, -80(%rbp)
	movq	%rax, -72(%rbp)
	movl	-108(%rbp), %eax
	testl	%eax, %eax
	je	.L618
	cmpl	$1, %eax
	jne	.L620
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	%r12, %rdi
	subq	16(%rbx), %rax
	movq	%rax, -64(%rbp)
	call	_ZNK2v84base9TimeDelta14InMicrosecondsEv@PLT
	leaq	3912(%r15), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	movq	40(%rbx), %rax
	addq	%rax, -72(%rbp)
.L620:
	leaq	-80(%rbp), %rdi
	call	_ZNK2v84base9TimeDelta14InMicrosecondsEv@PLT
	leaq	3816(%r15), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	leaq	-72(%rbp), %rdi
	call	_ZNK2v84base9TimeDelta14InMicrosecondsEv@PLT
	leaq	3768(%r15), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	jmp	.L617
	.p2align 4,,10
	.p2align 3
.L618:
	movq	40(%rbx), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	%r12, %rdi
	subq	16(%rbx), %rax
	movq	%rax, -64(%rbp)
	call	_ZNK2v84base9TimeDelta14InMicrosecondsEv@PLT
	leaq	3960(%r15), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	jmp	.L620
.L628:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25163:
	.size	_ZNK2v88internal23OptimizedCompilationJob22RecordCompilationStatsENS1_15CompilationModeEPNS0_7IsolateE, .-_ZNK2v88internal23OptimizedCompilationJob22RecordCompilationStatsENS1_15CompilationModeEPNS0_7IsolateE
	.section	.text._ZNK2v88internal23OptimizedCompilationJob25RecordFunctionCompilationENS0_17CodeEventListener16LogEventsAndTagsEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal23OptimizedCompilationJob25RecordFunctionCompilationENS0_17CodeEventListener16LogEventsAndTagsEPNS0_7IsolateE
	.type	_ZNK2v88internal23OptimizedCompilationJob25RecordFunctionCompilationENS0_17CodeEventListener16LogEventsAndTagsEPNS0_7IsolateE, @function
_ZNK2v88internal23OptimizedCompilationJob25RecordFunctionCompilationENS0_17CodeEventListener16LogEventsAndTagsEPNS0_7IsolateE:
.LFB25164:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$16, %rsp
	movq	-8(%rdi), %rax
	movq	40(%rax), %r14
	call	_ZNK2v84base9TimeDelta15InMillisecondsFEv@PLT
	leaq	40(%rbx), %rdi
	movsd	%xmm0, -40(%rbp)
	call	_ZNK2v84base9TimeDelta15InMillisecondsFEv@PLT
	movsd	-40(%rbp), %xmm1
	leaq	48(%rbx), %rdi
	addsd	%xmm0, %xmm1
	movsd	%xmm1, -40(%rbp)
	call	_ZNK2v84base9TimeDelta15InMillisecondsFEv@PLT
	movq	24(%rbx), %rax
	addsd	-40(%rbp), %xmm0
	movq	24(%rax), %rax
	movq	(%rax), %rax
	movq	31(%rax), %rsi
	testb	$1, %sil
	jne	.L635
.L630:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L631
	movsd	%xmm0, -40(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movsd	-40(%rbp), %xmm0
	movq	%rax, %rdx
.L632:
	movq	24(%rbx), %rax
	movq	%r12, %r9
	movq	%r14, %rcx
	movl	%r13d, %edi
	movl	$1, %r8d
	movq	24(%rax), %rsi
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12_GLOBAL__N_122LogFunctionCompilationENS0_17CodeEventListener16LogEventsAndTagsENS0_6HandleINS0_18SharedFunctionInfoEEENS4_INS0_6ScriptEEENS4_INS0_12AbstractCodeEEEbdPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L631:
	.cfi_restore_state
	movq	41088(%r12), %rdx
	cmpq	41096(%r12), %rdx
	je	.L636
.L633:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdx)
	jmp	.L632
	.p2align 4,,10
	.p2align 3
.L635:
	movq	-1(%rsi), %rax
	cmpw	$86, 11(%rax)
	jne	.L630
	movq	23(%rsi), %rsi
	jmp	.L630
	.p2align 4,,10
	.p2align 3
.L636:
	movq	%r12, %rdi
	movq	%rsi, -48(%rbp)
	movsd	%xmm0, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-48(%rbp), %rsi
	movsd	-40(%rbp), %xmm0
	movq	%rax, %rdx
	jmp	.L633
	.cfi_endproc
.LFE25164:
	.size	_ZNK2v88internal23OptimizedCompilationJob25RecordFunctionCompilationENS0_17CodeEventListener16LogEventsAndTagsEPNS0_7IsolateE, .-_ZNK2v88internal23OptimizedCompilationJob25RecordFunctionCompilationENS0_17CodeEventListener16LogEventsAndTagsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internal12_GLOBAL__N_116GetOptimizedCodeENS0_6HandleINS0_10JSFunctionEEENS0_15ConcurrencyModeENS0_9BailoutIdEPNS0_15JavaScriptFrameE.str1.1,"aMS",@progbits,1
.LC16:
	.string	"GetCodeFromOptimizedCodeCache"
.LC17:
	.string	"V8.OptimizeCode"
.LC18:
	.string	"[found optimized code for "
.LC19:
	.string	"]\n"
	.section	.rodata._ZN2v88internal12_GLOBAL__N_116GetOptimizedCodeENS0_6HandleINS0_10JSFunctionEEENS0_15ConcurrencyModeENS0_9BailoutIdEPNS0_15JavaScriptFrameE.str1.8,"aMS",@progbits,1
	.align 8
.LC20:
	.string	"  ** Compilation queue full, will retry optimizing "
	.section	.rodata._ZN2v88internal12_GLOBAL__N_116GetOptimizedCodeENS0_6HandleINS0_10JSFunctionEEENS0_15ConcurrencyModeENS0_9BailoutIdEPNS0_15JavaScriptFrameE.str1.1
.LC21:
	.string	" later.\n"
	.section	.rodata._ZN2v88internal12_GLOBAL__N_116GetOptimizedCodeENS0_6HandleINS0_10JSFunctionEEENS0_15ConcurrencyModeENS0_9BailoutIdEPNS0_15JavaScriptFrameE.str1.8
	.align 8
.LC22:
	.string	"  ** High memory pressure, will retry optimizing "
	.section	.rodata._ZN2v88internal12_GLOBAL__N_116GetOptimizedCodeENS0_6HandleINS0_10JSFunctionEEENS0_15ConcurrencyModeENS0_9BailoutIdEPNS0_15JavaScriptFrameE.str1.1
.LC23:
	.string	"V8.RecompileSynchronous"
.LC24:
	.string	"  ** Queued "
	.section	.rodata._ZN2v88internal12_GLOBAL__N_116GetOptimizedCodeENS0_6HandleINS0_10JSFunctionEEENS0_15ConcurrencyModeENS0_9BailoutIdEPNS0_15JavaScriptFrameE.str1.8
	.align 8
.LC25:
	.string	" for concurrent optimization.\n"
	.section	.rodata._ZN2v88internal12_GLOBAL__N_116GetOptimizedCodeENS0_6HandleINS0_10JSFunctionEEENS0_15ConcurrencyModeENS0_9BailoutIdEPNS0_15JavaScriptFrameE.str1.1
.LC26:
	.string	"[aborted optimizing "
.LC27:
	.string	" because: %s]\n"
	.section	.text._ZN2v88internal12_GLOBAL__N_116GetOptimizedCodeENS0_6HandleINS0_10JSFunctionEEENS0_15ConcurrencyModeENS0_9BailoutIdEPNS0_15JavaScriptFrameE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_116GetOptimizedCodeENS0_6HandleINS0_10JSFunctionEEENS0_15ConcurrencyModeENS0_9BailoutIdEPNS0_15JavaScriptFrameE, @function
_ZN2v88internal12_GLOBAL__N_116GetOptimizedCodeENS0_6HandleINS0_10JSFunctionEEENS0_15ConcurrencyModeENS0_9BailoutIdEPNS0_15JavaScriptFrameE:
.LFB25221:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$488, %rsp
	.cfi_offset 3, -56
	movl	%esi, -488(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rbx
	subq	$37592, %rbx
	movq	23(%rax), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L638
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L639:
	movabsq	$287762808832, %rcx
	movq	(%r12), %rdx
	movq	23(%rdx), %rax
	movq	7(%rax), %rax
	cmpq	%rcx, %rax
	je	.L649
	testb	$1, %al
	jne	.L643
.L647:
	movq	39(%rdx), %rax
	movq	7(%rax), %rax
	movq	-1(%rax), %rdx
	cmpw	$155, 11(%rdx)
	je	.L853
.L649:
	movq	(%r15), %rax
	movl	47(%rax), %edx
	andl	$15728640, %edx
	jne	.L854
.L650:
	movq	41472(%rbx), %rax
	cmpb	$0, 9(%rax)
	jne	.L652
	cmpb	$0, _ZN2v88internal27FLAG_testing_d8_test_runnerE(%rip)
	jne	.L855
.L653:
	movq	(%r12), %rax
	pxor	%xmm0, %xmm0
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rcx
	movaps	%xmm0, -192(%rbp)
	movq	$0, -160(%rbp)
	movaps	%xmm0, -176(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %edx
	testl	%edx, %edx
	jne	.L856
.L654:
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	movq	23(%rax), %rsi
	movq	3520(%rdx), %rdi
	subq	$37592, %rdx
	testq	%rdi, %rdi
	je	.L655
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L656:
	leaq	-240(%rbp), %r11
	cmpl	$-1, %r13d
	je	.L857
.L658:
	movq	-192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L672
	leaq	-184(%rbp), %rsi
	movq	%r11, -496(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	movq	-496(%rbp), %r11
.L672:
	movq	(%r12), %rax
	movq	39(%rax), %rax
	movq	7(%rax), %rax
	movl	$0, 39(%rax)
	movl	12616(%rbx), %eax
	movl	$4, 12616(%rbx)
	movl	%eax, -496(%rbp)
	movq	41632(%rbx), %rax
	testq	%rax, %rax
	je	.L675
	leaq	_ZN2v88internal6Logger26DefaultEventLoggerSentinelEPKci(%rip), %rdx
	cmpq	%rdx, %rax
	je	.L858
	movq	%r11, -504(%rbp)
	xorl	%esi, %esi
	leaq	.LC17(%rip), %rdi
	call	*%rax
	movq	-504(%rbp), %r11
.L675:
	movq	$0, -352(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -384(%rbp)
	movaps	%xmm0, -368(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L859
.L678:
	movq	_ZZN2v88internal12_GLOBAL__N_116GetOptimizedCodeENS0_6HandleINS0_10JSFunctionEEENS0_15ConcurrencyModeENS0_9BailoutIdEPNS0_15JavaScriptFrameEE28trace_event_unique_atomic839(%rip), %rdx
	testq	%rdx, %rdx
	je	.L679
.L680:
	movq	$0, -448(%rbp)
	movzbl	(%rdx), %eax
	testb	$5, %al
	jne	.L860
.L683:
	xorl	%ecx, %ecx
	movl	$255, %edx
	movq	%rbx, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal15InterruptsScopeC2EPNS0_7IsolateElNS1_4ModeE@PLT
	leaq	16+_ZTVN2v88internal23PostponeInterruptsScopeE(%rip), %rax
	movq	%rax, -240(%rbp)
	movq	(%r15), %rax
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L687
.L690:
	xorl	%ecx, %ecx
.L688:
	leaq	-472(%rbp), %rdi
	movq	%r12, %rdx
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler8Pipeline17NewCompilationJobEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEEb@PLT
	movq	-472(%rbp), %rax
	movq	24(%rax), %r11
	movq	24(%r11), %rax
	movl	%r13d, 56(%r11)
	leaq	-192(%rbp), %r13
	movq	%r14, 128(%r11)
	movq	%r13, %rdi
	movq	(%rax), %rax
	movq	%r11, -504(%rbp)
	movq	%rax, -192(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo12HasBreakInfoEv@PLT
	movq	-504(%rbp), %r11
	testb	%al, %al
	movl	%eax, %r14d
	jne	.L861
	cmpb	$0, _ZN2v88internal8FLAG_optE(%rip)
	je	.L693
	movq	(%r15), %rax
	movq	_ZN2v88internal17FLAG_turbo_filterE(%rip), %rsi
	movq	%r13, %rdi
	movq	%r11, -504(%rbp)
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal18SharedFunctionInfo12PassesFilterEPKc@PLT
	movq	-504(%rbp), %r11
	testb	%al, %al
	movb	%al, -512(%rbp)
	je	.L693
	pxor	%xmm0, %xmm0
	cmpl	$1, -488(%rbp)
	movq	$0, -304(%rbp)
	movaps	%xmm0, -336(%rbp)
	movaps	%xmm0, -320(%rbp)
	je	.L862
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%r11, -488(%rbp)
	call	_ZN2v88internal20CanonicalHandleScopeC1EPNS0_7IsolateE@PLT
	movq	-488(%rbp), %r11
	movq	%rbx, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal24OptimizedCompilationInfo29ReopenHandlesInNewHandleScopeEPNS0_7IsolateE@PLT
	movq	41632(%rbx), %rax
	movq	-472(%rbp), %r12
	movq	-488(%rbp), %r11
	testq	%rax, %rax
	je	.L721
	leaq	_ZN2v88internal6Logger26DefaultEventLoggerSentinelEPKci(%rip), %rdx
	movq	%r11, -488(%rbp)
	cmpq	%rdx, %rax
	je	.L863
	xorl	%esi, %esi
	leaq	.LC23(%rip), %rdi
	call	*%rax
	movq	-488(%rbp), %r11
.L721:
	movq	$0, -256(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -288(%rbp)
	movaps	%xmm0, -272(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L864
.L722:
	movq	24(%r12), %r15
	movq	_ZZN2v88internal12_GLOBAL__N_119GetOptimizedCodeNowEPNS0_23OptimizedCompilationJobEPNS0_7IsolateEE28trace_event_unique_atomic728(%rip), %rdx
	movq	%rdx, %r14
	testq	%rdx, %rdx
	je	.L865
.L724:
	movq	$0, -416(%rbp)
	movzbl	(%r14), %eax
	testb	$5, %al
	jne	.L866
.L726:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%r11, -488(%rbp)
	call	_ZN2v88internal23OptimizedCompilationJob10PrepareJobEPNS0_7IsolateE
	movq	-488(%rbp), %r11
	testl	%eax, %eax
	jne	.L730
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	movq	(%r12), %rax
	call	*24(%rax)
	movq	-488(%rbp), %r11
	testl	%eax, %eax
	jne	.L731
	movl	$2, 8(%r12)
	movq	%r11, -504(%rbp)
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	%rbx, %rsi
	subq	%r14, %rax
	addq	%rax, 40(%r12)
	leaq	-464(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb0EEC1EPNS0_7IsolateE@PLT
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, -488(%rbp)
	movq	(%r12), %rax
	call	*32(%rax)
	movq	-504(%rbp), %r11
	testl	%eax, %eax
	jne	.L867
	movl	$3, 8(%r12)
	movq	%r11, -504(%rbp)
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	subq	-488(%rbp), %rax
	movq	%r14, %rdi
	addq	%rax, 48(%r12)
	call	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb0EED1Ev@PLT
	movq	%rbx, %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal23OptimizedCompilationJob22RecordCompilationStatsENS1_15CompilationModeEPNS0_7IsolateE
	movq	%r15, %rdi
	call	_ZN2v88internal12_GLOBAL__N_132InsertCodeIntoOptimizedCodeCacheEPNS0_24OptimizedCompilationInfoE
	movq	%rbx, %rdx
	movl	$15, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal23OptimizedCompilationJob25RecordFunctionCompilationENS0_17CodeEventListener16LogEventsAndTagsEPNS0_7IsolateE
	movq	-504(%rbp), %r11
.L734:
	leaq	-416(%rbp), %rdi
	movq	%r11, -488(%rbp)
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-288(%rbp), %rdi
	movq	-488(%rbp), %r11
	testq	%rdi, %rdi
	jne	.L868
.L735:
	movq	41632(%rbx), %rax
	testq	%rax, %rax
	je	.L737
	leaq	_ZN2v88internal6Logger26DefaultEventLoggerSentinelEPKci(%rip), %rdx
	movq	%r11, -488(%rbp)
	cmpq	%rdx, %rax
	je	.L869
	movl	$1, %esi
	leaq	.LC23(%rip), %rdi
	call	*%rax
	movq	-488(%rbp), %r11
.L737:
	cmpb	$0, -512(%rbp)
	je	.L698
	movq	40(%r11), %r12
.L718:
	movq	%r13, %rdi
	call	_ZN2v88internal20CanonicalHandleScopeD1Ev@PLT
	cmpb	$0, -336(%rbp)
	je	.L692
	leaq	-328(%rbp), %r13
	movq	-304(%rbp), %r14
	movq	%r13, %rdi
	call	_ZN2v88internal19DeferredHandleScope6DetachEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal24OptimizedCompilationInfo20set_deferred_handlesEPNS0_15DeferredHandlesE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal19DeferredHandleScopeD1Ev@PLT
	jmp	.L692
	.p2align 4,,10
	.p2align 3
.L854:
	movl	47(%rax), %eax
	shrl	$20, %eax
	andl	$15, %eax
	cmpb	$11, %al
	jne	.L650
.L652:
	xorl	%r12d, %r12d
.L651:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L870
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L853:
	.cfi_restore_state
	movq	15(%rax), %rax
	testb	$1, %al
	jne	.L649
	sarq	$32, %rax
	cmpq	$1, %rax
	jbe	.L649
	movq	(%r12), %rax
	leaq	-192(%rbp), %rdi
	movq	39(%rax), %rax
	movq	7(%rax), %rax
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal14FeedbackVector23ClearOptimizationMarkerEv@PLT
	jmp	.L649
	.p2align 4,,10
	.p2align 3
.L643:
	movq	-1(%rax), %rcx
	cmpw	$165, 11(%rcx)
	je	.L649
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L647
	jmp	.L649
	.p2align 4,,10
	.p2align 3
.L638:
	movq	41088(%rbx), %r15
	cmpq	%r15, 41096(%rbx)
	je	.L871
.L640:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r15)
	jmp	.L639
	.p2align 4,,10
	.p2align 3
.L655:
	movq	41088(%rdx), %rax
	cmpq	41096(%rdx), %rax
	je	.L872
.L657:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%rdx)
	movq	%rsi, (%rax)
	jmp	.L656
	.p2align 4,,10
	.p2align 3
.L857:
	movabsq	$287762808832, %rcx
	movq	(%r12), %rdx
	movq	23(%rdx), %rax
	movq	7(%rax), %rax
	cmpq	%rcx, %rax
	je	.L658
	testb	$1, %al
	jne	.L660
.L663:
	movq	39(%rdx), %rax
	leaq	-240(%rbp), %r11
	movq	7(%rax), %rax
	movq	-1(%rax), %rax
	cmpw	$155, 11(%rax)
	jne	.L658
	movq	(%r12), %rax
	leaq	-240(%rbp), %r11
	movq	%r11, %rdi
	movq	39(%rax), %rdx
	movq	7(%rdx), %rdx
	movq	%rdx, -240(%rbp)
	movq	23(%rax), %rsi
	leaq	.LC16(%rip), %rdx
	movq	%r11, -496(%rbp)
	call	_ZN2v88internal14FeedbackVector41EvictOptimizedCodeMarkedForDeoptimizationENS0_18SharedFunctionInfoEPKc@PLT
	movq	-240(%rbp), %rax
	movq	-496(%rbp), %r11
	movq	15(%rax), %rsi
	testb	$1, %sil
	je	.L658
	cmpl	$3, %esi
	je	.L658
	andq	$-3, %rsi
	je	.L658
	andq	$-262144, %rax
	movq	24(%rax), %rcx
	movq	3520(%rcx), %rdi
	subq	$37592, %rcx
	testq	%rdi, %rdi
	je	.L666
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-192(%rbp), %rdi
	movq	-496(%rbp), %r11
	movq	%rax, %rdx
	testq	%rdi, %rdi
	jne	.L667
.L668:
	testq	%rdx, %rdx
	je	.L672
.L673:
	cmpb	$0, _ZN2v88internal14FLAG_trace_optE(%rip)
	jne	.L873
.L681:
	movq	%rdx, %r12
	jmp	.L651
	.p2align 4,,10
	.p2align 3
.L871:
	movq	%rbx, %rdi
	movq	%rsi, -496(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-496(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L640
	.p2align 4,,10
	.p2align 3
.L872:
	movq	%rdx, %rdi
	movq	%rsi, -504(%rbp)
	movq	%rdx, -496(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-504(%rbp), %rsi
	movq	-496(%rbp), %rdx
	jmp	.L657
	.p2align 4,,10
	.p2align 3
.L693:
	movl	$10, %esi
	movq	%r11, %rdi
	xorl	%r12d, %r12d
	call	_ZN2v88internal24OptimizedCompilationInfo17AbortOptimizationENS0_13BailoutReasonE@PLT
.L692:
	movq	-472(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L742
	movq	(%rdi), %rax
	call	*8(%rax)
.L742:
	leaq	16+_ZTVN2v88internal15InterruptsScopeE(%rip), %rax
	cmpl	$2, -208(%rbp)
	movq	%rax, -240(%rbp)
	je	.L743
	movq	-232(%rbp), %rdi
	call	_ZN2v88internal10StackGuard18PopInterruptsScopeEv@PLT
.L743:
	leaq	-448(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-384(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L874
.L744:
	movq	41632(%rbx), %rax
	testq	%rax, %rax
	je	.L746
	leaq	_ZN2v88internal6Logger26DefaultEventLoggerSentinelEPKci(%rip), %rdx
	cmpq	%rdx, %rax
	je	.L875
	movl	$1, %esi
	leaq	.LC17(%rip), %rdi
	call	*%rax
.L746:
	movl	-496(%rbp), %eax
	movl	%eax, 12616(%rbx)
	jmp	.L651
	.p2align 4,,10
	.p2align 3
.L687:
	movq	-1(%rax), %rcx
	leaq	-1(%rax), %rdx
	cmpw	$86, 11(%rcx)
	je	.L876
.L689:
	movq	(%rdx), %rax
	xorl	%ecx, %ecx
	cmpw	$96, 11(%rax)
	sete	%cl
	jmp	.L688
	.p2align 4,,10
	.p2align 3
.L660:
	movq	-1(%rax), %rcx
	cmpw	$165, 11(%rcx)
	je	.L658
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L663
	jmp	.L658
.L858:
	movq	41016(%rbx), %rdi
	movq	%r11, -512(%rbp)
	movq	%rdi, -504(%rbp)
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	movq	-504(%rbp), %rdi
	movq	-512(%rbp), %r11
	testb	%al, %al
	je	.L675
	leaq	.LC17(%rip), %rdx
	xorl	%esi, %esi
	movq	%r11, -504(%rbp)
	call	_ZN2v88internal6Logger10TimerEventENS1_8StartEndEPKc@PLT
	movq	-504(%rbp), %r11
	jmp	.L675
	.p2align 4,,10
	.p2align 3
.L873:
	leaq	.LC18(%rip), %rdi
	xorl	%eax, %eax
	movq	%rdx, -488(%rbp)
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	(%r12), %rax
	movq	stdout(%rip), %rsi
	leaq	-192(%rbp), %rdi
	movq	%rax, -192(%rbp)
	call	_ZNK2v88internal6Object10ShortPrintEP8_IO_FILE@PLT
	leaq	.LC19(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-488(%rbp), %rdx
	jmp	.L681
	.p2align 4,,10
	.p2align 3
.L855:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal24PendingOptimizationTable20FunctionWasOptimizedEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE@PLT
	jmp	.L653
	.p2align 4,,10
	.p2align 3
.L856:
	movq	3368(%rcx), %rdi
	leaq	-184(%rbp), %rsi
	movl	$132, %edx
	addq	$23240, %rdi
	movq	%rdi, -192(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	movq	(%r12), %rax
	jmp	.L654
	.p2align 4,,10
	.p2align 3
.L860:
	pxor	%xmm0, %xmm0
	movq	%r11, -520(%rbp)
	movq	%rdx, -512(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rcx
	movq	$0, -504(%rbp)
	movq	-512(%rbp), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	-520(%rbp), %r11
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L877
.L684:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L685
	movq	(%rdi), %rax
	movq	%r11, -520(%rbp)
	movq	%rdx, -512(%rbp)
	call	*8(%rax)
	movq	-520(%rbp), %r11
	movq	-512(%rbp), %rdx
.L685:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L686
	movq	(%rdi), %rax
	movq	%r11, -520(%rbp)
	movq	%rdx, -512(%rbp)
	call	*8(%rax)
	movq	-520(%rbp), %r11
	movq	-512(%rbp), %rdx
.L686:
	leaq	.LC17(%rip), %rax
	movq	%rdx, -440(%rbp)
	movq	%rax, -432(%rbp)
	movq	-504(%rbp), %rax
	movq	%rax, -424(%rbp)
	leaq	-440(%rbp), %rax
	movq	%rax, -448(%rbp)
	jmp	.L683
	.p2align 4,,10
	.p2align 3
.L679:
	movq	%r11, -504(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	-504(%rbp), %r11
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L878
.L682:
	movq	%rdx, _ZZN2v88internal12_GLOBAL__N_116GetOptimizedCodeENS0_6HandleINS0_10JSFunctionEEENS0_15ConcurrencyModeENS0_9BailoutIdEPNS0_15JavaScriptFrameEE28trace_event_unique_atomic839(%rip)
	jmp	.L680
	.p2align 4,,10
	.p2align 3
.L861:
	movl	$4, %esi
	movq	%r11, %rdi
	xorl	%r12d, %r12d
	call	_ZN2v88internal24OptimizedCompilationInfo17AbortOptimizationENS0_13BailoutReasonE@PLT
	jmp	.L692
.L875:
	movq	41016(%rbx), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	je	.L746
	leaq	.LC17(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal6Logger10TimerEventENS1_8StartEndEPKc@PLT
	jmp	.L746
.L876:
	movq	23(%rax), %rdx
	testb	$1, %dl
	je	.L690
	subq	$1, %rdx
	jmp	.L689
.L874:
	leaq	-376(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L744
.L859:
	movq	40960(%rbx), %rax
	leaq	-376(%rbp), %rsi
	movl	$178, %edx
	movq	%r11, -504(%rbp)
	leaq	23240(%rax), %rdi
	movq	%rdi, -384(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	movq	-504(%rbp), %r11
	jmp	.L678
.L867:
	movl	$4, 8(%r12)
	movq	%r11, -504(%rbp)
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	subq	-488(%rbp), %rax
	movq	%r14, %rdi
	addq	%rax, 48(%r12)
	call	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb0EED1Ev@PLT
	movq	-504(%rbp), %r11
.L730:
	movzbl	_ZN2v88internal14FLAG_trace_optE(%rip), %eax
	movb	%al, -512(%rbp)
	testb	%al, %al
	je	.L734
	leaq	.LC26(%rip), %rdi
	xorl	%eax, %eax
	movq	%r11, -488(%rbp)
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	32(%r15), %rax
	movq	stdout(%rip), %rsi
	leaq	-464(%rbp), %rdi
	movq	(%rax), %rax
	movq	%rax, -464(%rbp)
	call	_ZNK2v88internal6Object10ShortPrintEP8_IO_FILE@PLT
	movzbl	88(%r15), %edi
	call	_ZN2v88internal16GetBailoutReasonENS0_13BailoutReasonE@PLT
	leaq	.LC27(%rip), %rdi
	movq	%rax, %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movb	$0, -512(%rbp)
	movq	-488(%rbp), %r11
	jmp	.L734
.L862:
	leaq	-328(%rbp), %rdi
	movq	%rbx, %rsi
	movq	%r11, -488(%rbp)
	call	_ZN2v88internal19DeferredHandleScopeC1EPNS0_7IsolateE@PLT
	movq	-488(%rbp), %r11
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movb	$1, -336(%rbp)
	movq	%r11, -304(%rbp)
	call	_ZN2v88internal20CanonicalHandleScopeC1EPNS0_7IsolateE@PLT
	movq	-488(%rbp), %r11
	movq	%rbx, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal24OptimizedCompilationInfo29ReopenHandlesInNewHandleScopeEPNS0_7IsolateE@PLT
	movq	-472(%rbp), %rax
	movq	45416(%rbx), %r15
	movq	%rax, -528(%rbp)
	movq	24(%rax), %rax
	leaq	32(%r15), %rdi
	movq	%rdi, -504(%rbp)
	movq	%rax, -520(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movl	20(%r15), %eax
	movq	-504(%rbp), %rdi
	movl	16(%r15), %r15d
	movl	%eax, -488(%rbp)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	cmpl	%r15d, -488(%rbp)
	jl	.L696
	cmpb	$0, _ZN2v88internal35FLAG_trace_concurrent_recompilationE(%rip)
	leaq	.LC20(%rip), %rdi
	je	.L698
.L852:
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-520(%rbp), %rax
	movq	stdout(%rip), %rsi
	leaq	-288(%rbp), %rdi
	movq	32(%rax), %rax
	movq	(%rax), %rax
	movq	%rax, -288(%rbp)
	call	_ZNK2v88internal6Object10ShortPrintEP8_IO_FILE@PLT
	leaq	.LC21(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
.L698:
	movq	96(%rbx), %rax
	cmpq	12480(%rbx), %rax
	je	.L740
	movq	%rax, 12480(%rbx)
.L740:
	xorl	%r12d, %r12d
	jmp	.L718
.L666:
	movq	41088(%rcx), %rdx
	cmpq	41096(%rcx), %rdx
	je	.L879
.L669:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%rcx)
	movq	%rsi, (%rdx)
	movq	-192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L673
.L667:
	leaq	-184(%rbp), %rsi
	movq	%r11, -504(%rbp)
	movq	%rdx, -496(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	movq	-504(%rbp), %r11
	movq	-496(%rbp), %rdx
	jmp	.L668
	.p2align 4,,10
	.p2align 3
.L877:
	subq	$8, %rsp
	leaq	-80(%rbp), %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	movl	$88, %esi
	pushq	%rcx
	leaq	.LC17(%rip), %rcx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	-520(%rbp), %r11
	movq	-512(%rbp), %rdx
	movq	%rax, -504(%rbp)
	addq	$64, %rsp
	jmp	.L684
.L878:
	leaq	.LC6(%rip), %rsi
	call	*%rax
	movq	-504(%rbp), %r11
	movq	%rax, %rdx
	jmp	.L682
.L865:
	movq	%r11, -488(%rbp)
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r14
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	-488(%rbp), %r11
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L880
.L725:
	movq	%r14, _ZZN2v88internal12_GLOBAL__N_119GetOptimizedCodeNowEPNS0_23OptimizedCompilationJobEPNS0_7IsolateEE28trace_event_unique_atomic728(%rip)
	jmp	.L724
.L866:
	pxor	%xmm0, %xmm0
	movq	%r11, -504(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rcx
	movq	$0, -488(%rbp)
	movq	-504(%rbp), %r11
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L881
.L727:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L728
	movq	(%rdi), %rax
	movq	%r11, -504(%rbp)
	call	*8(%rax)
	movq	-504(%rbp), %r11
.L728:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L729
	movq	(%rdi), %rax
	movq	%r11, -504(%rbp)
	call	*8(%rax)
	movq	-504(%rbp), %r11
.L729:
	leaq	.LC23(%rip), %rax
	movq	%r14, -408(%rbp)
	movq	%rax, -400(%rbp)
	movq	-488(%rbp), %rax
	movq	%rax, -392(%rbp)
	leaq	-408(%rbp), %rax
	movq	%rax, -416(%rbp)
	jmp	.L726
.L696:
	movl	37800(%rbx), %eax
	testl	%eax, %eax
	je	.L699
	cmpb	$0, _ZN2v88internal35FLAG_trace_concurrent_recompilationE(%rip)
	je	.L698
	leaq	.LC22(%rip), %rdi
	jmp	.L852
.L731:
	movl	$4, 8(%r12)
	movq	%r11, -488(%rbp)
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	-488(%rbp), %r11
	subq	%r14, %rax
	addq	%rax, 40(%r12)
	jmp	.L730
.L879:
	movq	%rcx, %rdi
	movq	%r11, -512(%rbp)
	movq	%rsi, -504(%rbp)
	movq	%rcx, -496(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-512(%rbp), %r11
	movq	-504(%rbp), %rsi
	movq	-496(%rbp), %rcx
	movq	%rax, %rdx
	jmp	.L669
.L699:
	movq	41632(%rbx), %rax
	testq	%rax, %rax
	je	.L702
	leaq	_ZN2v88internal6Logger26DefaultEventLoggerSentinelEPKci(%rip), %rdx
	cmpq	%rdx, %rax
	je	.L882
	xorl	%esi, %esi
	leaq	.LC23(%rip), %rdi
	call	*%rax
.L702:
	movq	$0, -256(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -288(%rbp)
	movaps	%xmm0, -272(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L883
.L705:
	movq	_ZZN2v88internal12_GLOBAL__N_121GetOptimizedCodeLaterEPNS0_23OptimizedCompilationJobEPNS0_7IsolateEE28trace_event_unique_atomic774(%rip), %rax
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L884
.L707:
	movq	$0, -416(%rbp)
	movzbl	(%r15), %eax
	testb	$5, %al
	jne	.L885
.L708:
	movq	-528(%rbp), %r15
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal23OptimizedCompilationJob10PrepareJobEPNS0_7IsolateE
	testl	%eax, %eax
	jne	.L711
	movq	45416(%rbx), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal27OptimizingCompileDispatcher20QueueForOptimizationEPNS0_23OptimizedCompilationJobE@PLT
	movzbl	_ZN2v88internal35FLAG_trace_concurrent_recompilationE(%rip), %r14d
	testb	%r14b, %r14b
	jne	.L886
	movzbl	-512(%rbp), %r14d
.L711:
	leaq	-416(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-288(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L887
.L712:
	movq	41632(%rbx), %rax
	testq	%rax, %rax
	je	.L714
	leaq	_ZN2v88internal6Logger26DefaultEventLoggerSentinelEPKci(%rip), %rdx
	cmpq	%rdx, %rax
	je	.L888
	movl	$1, %esi
	leaq	.LC23(%rip), %rdi
	call	*%rax
.L714:
	testb	%r14b, %r14b
	je	.L698
	movq	(%r12), %rax
	leaq	-288(%rbp), %rdi
	movl	$4, %esi
	movq	$0, -472(%rbp)
	movq	39(%rax), %rax
	movq	7(%rax), %rax
	movq	%rax, -288(%rbp)
	call	_ZN2v88internal14FeedbackVector21SetOptimizationMarkerENS0_18OptimizationMarkerE@PLT
	leaq	41184(%rbx), %rdi
	movl	$57, %esi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	movq	%rax, %r12
	jmp	.L718
.L869:
	movq	41016(%rbx), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	movq	-488(%rbp), %r11
	testb	%al, %al
	je	.L737
	leaq	.LC23(%rip), %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal6Logger10TimerEventENS1_8StartEndEPKc@PLT
	movq	-488(%rbp), %r11
	jmp	.L737
.L863:
	movq	41016(%rbx), %r14
	movq	%r14, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	movq	-488(%rbp), %r11
	testb	%al, %al
	je	.L721
	leaq	.LC23(%rip), %rdx
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal6Logger10TimerEventENS1_8StartEndEPKc@PLT
	movq	-488(%rbp), %r11
	jmp	.L721
.L864:
	movq	40960(%rbx), %rax
	leaq	-280(%rbp), %rsi
	movl	$196, %edx
	movq	%r11, -488(%rbp)
	leaq	23240(%rax), %rdi
	movq	%rdi, -288(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	movq	-488(%rbp), %r11
	jmp	.L722
.L868:
	leaq	-280(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	movq	-488(%rbp), %r11
	jmp	.L735
.L880:
	leaq	.LC6(%rip), %rsi
	call	*%rax
	movq	-488(%rbp), %r11
	movq	%rax, %r14
	jmp	.L725
.L881:
	leaq	-80(%rbp), %rcx
	pushq	%rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	movq	%r14, %rdx
	movl	$88, %esi
	pushq	%rcx
	leaq	.LC23(%rip), %rcx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	-504(%rbp), %r11
	movq	%rax, -488(%rbp)
	addq	$64, %rsp
	jmp	.L727
.L870:
	call	__stack_chk_fail@PLT
.L885:
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	%rcx
	movq	%rax, %rdi
	movq	(%rax), %rax
	leaq	.LC23(%rip), %rcx
	pushq	$0
	movl	$88, %esi
	pushq	%rdx
	movq	%r15, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*24(%rax)
	movq	-72(%rbp), %rdi
	movq	%rax, -488(%rbp)
	addq	$64, %rsp
	testq	%rdi, %rdi
	je	.L709
	movq	(%rdi), %rax
	call	*8(%rax)
.L709:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L710
	movq	(%rdi), %rax
	call	*8(%rax)
.L710:
	leaq	.LC23(%rip), %rax
	movq	%r15, -408(%rbp)
	movq	%rax, -400(%rbp)
	movq	-488(%rbp), %rax
	movq	%rax, -392(%rbp)
	leaq	-408(%rbp), %rax
	movq	%rax, -416(%rbp)
	jmp	.L708
.L884:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	.LC6(%rip), %rsi
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*16(%rax)
	movq	%rax, _ZZN2v88internal12_GLOBAL__N_121GetOptimizedCodeLaterEPNS0_23OptimizedCompilationJobEPNS0_7IsolateEE28trace_event_unique_atomic774(%rip)
	movq	%rax, %r15
	jmp	.L707
.L886:
	xorl	%eax, %eax
	leaq	.LC24(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-520(%rbp), %rax
	movq	stdout(%rip), %rsi
	leaq	-464(%rbp), %rdi
	movq	32(%rax), %rax
	movq	(%rax), %rax
	movq	%rax, -464(%rbp)
	call	_ZNK2v88internal6Object10ShortPrintEP8_IO_FILE@PLT
	leaq	.LC25(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L711
.L883:
	movq	40960(%rbx), %rax
	leaq	-280(%rbp), %rsi
	movl	$196, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -288(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L705
.L887:
	leaq	-280(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L712
.L888:
	movq	41016(%rbx), %r15
	movq	%r15, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	je	.L714
	leaq	.LC23(%rip), %rdx
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal6Logger10TimerEventENS1_8StartEndEPKc@PLT
	jmp	.L714
.L882:
	movq	41016(%rbx), %r15
	movq	%r15, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	je	.L702
	leaq	.LC23(%rip), %rdx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal6Logger10TimerEventENS1_8StartEndEPKc@PLT
	jmp	.L702
	.cfi_endproc
.LFE25221:
	.size	_ZN2v88internal12_GLOBAL__N_116GetOptimizedCodeENS0_6HandleINS0_10JSFunctionEEENS0_15ConcurrencyModeENS0_9BailoutIdEPNS0_15JavaScriptFrameE, .-_ZN2v88internal12_GLOBAL__N_116GetOptimizedCodeENS0_6HandleINS0_10JSFunctionEEENS0_15ConcurrencyModeENS0_9BailoutIdEPNS0_15JavaScriptFrameE
	.section	.text._ZN2v88internal21BackgroundCompileTaskC2EPNS0_19ScriptStreamingDataEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21BackgroundCompileTaskC2EPNS0_19ScriptStreamingDataEPNS0_7IsolateE
	.type	_ZN2v88internal21BackgroundCompileTaskC2EPNS0_19ScriptStreamingDataEPNS0_7IsolateE, @function
_ZN2v88internal21BackgroundCompileTaskC2EPNS0_19ScriptStreamingDataEPNS0_7IsolateE:
.LFB25261:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	$224, %edi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal9ParseInfoC1EPNS0_7IsolateE@PLT
	movl	_ZN2v88internal15FLAG_stack_sizeE(%rip), %eax
	movq	%r13, (%rbx)
	movq	$0, 8(%rbx)
	movq	$0, 16(%rbx)
	movq	$0, 24(%rbx)
	movq	41016(%r12), %r15
	movl	%eax, 32(%rbx)
	movq	40960(%r12), %rax
	movq	%r15, %rdi
	leaq	50888(%rax), %rdx
	addq	$5352, %rax
	movq	%rdx, 40(%rbx)
	movq	41136(%r12), %rdx
	movq	%rax, 56(%rbx)
	movq	%rdx, 48(%rbx)
	movl	12616(%r12), %r13d
	movl	$2, 12616(%r12)
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	jne	.L906
.L890:
	movq	(%rbx), %rdi
	movl	8(%rdi), %edx
	movl	%edx, %eax
	orb	$-127, %al
	movl	%eax, 8(%rdi)
	andb	$8, %dh
	jne	.L907
.L891:
	movl	%eax, %edx
	movl	%eax, %ecx
	andl	$-9, %eax
	shrl	$3, %edx
	orl	$8, %ecx
	andl	$1, %edx
	orb	_ZN2v88internal15FLAG_use_strictE(%rip), %dl
	cmovne	%ecx, %eax
	movl	%eax, 8(%rdi)
	movl	8(%r14), %esi
	movq	(%r14), %rdi
	call	_ZN2v88internal13ScannerStream3ForEPNS_14ScriptCompiler20ExternalSourceStreamENS2_14StreamedSource8EncodingE@PLT
	movq	(%rbx), %rdi
	leaq	-64(%rbp), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal9ParseInfo20set_character_streamESt10unique_ptrINS0_20Utf16CharacterStreamESt14default_deleteIS3_EE@PLT
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L894
	movq	(%rdi), %rax
	call	*8(%rax)
.L894:
	movl	%r13d, 12616(%r12)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L908
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L906:
	.cfi_restore_state
	movq	(%rbx), %rax
	movl	$4, %esi
	movq	%r15, %rdi
	movl	52(%rax), %edx
	call	_ZN2v88internal6Logger11ScriptEventENS1_15ScriptEventTypeEi@PLT
	jmp	.L890
	.p2align 4,,10
	.p2align 3
.L907:
	call	_ZN2v88internal9ParseInfo22AllocateSourceRangeMapEv@PLT
	movq	(%rbx), %rdi
	movl	8(%rdi), %eax
	jmp	.L891
.L908:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25261:
	.size	_ZN2v88internal21BackgroundCompileTaskC2EPNS0_19ScriptStreamingDataEPNS0_7IsolateE, .-_ZN2v88internal21BackgroundCompileTaskC2EPNS0_19ScriptStreamingDataEPNS0_7IsolateE
	.globl	_ZN2v88internal21BackgroundCompileTaskC1EPNS0_19ScriptStreamingDataEPNS0_7IsolateE
	.set	_ZN2v88internal21BackgroundCompileTaskC1EPNS0_19ScriptStreamingDataEPNS0_7IsolateE,_ZN2v88internal21BackgroundCompileTaskC2EPNS0_19ScriptStreamingDataEPNS0_7IsolateE
	.section	.text._ZN2v88internal21BackgroundCompileTaskC2EPNS0_19AccountingAllocatorEPKNS0_9ParseInfoEPKNS0_12AstRawStringEPKNS0_15FunctionLiteralEPNS0_28WorkerThreadRuntimeCallStatsEPNS0_14TimedHistogramEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21BackgroundCompileTaskC2EPNS0_19AccountingAllocatorEPKNS0_9ParseInfoEPKNS0_12AstRawStringEPKNS0_15FunctionLiteralEPNS0_28WorkerThreadRuntimeCallStatsEPNS0_14TimedHistogramEi
	.type	_ZN2v88internal21BackgroundCompileTaskC2EPNS0_19AccountingAllocatorEPKNS0_9ParseInfoEPKNS0_12AstRawStringEPKNS0_15FunctionLiteralEPNS0_28WorkerThreadRuntimeCallStatsEPNS0_14TimedHistogramEi, @function
_ZN2v88internal21BackgroundCompileTaskC2EPNS0_19AccountingAllocatorEPKNS0_9ParseInfoEPKNS0_12AstRawStringEPKNS0_15FunctionLiteralEPNS0_28WorkerThreadRuntimeCallStatsEPNS0_14TimedHistogramEi:
.LFB25264:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %xmm2
	movq	%r9, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	movq	%rsi, %rdx
	pushq	%r12
	.cfi_offset 12, -40
	movq	%r8, %r12
	movq	%r13, %rsi
	movq	%rcx, %r8
	pushq	%rbx
	movq	%r12, %rcx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	16(%rbp), %r14
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal9ParseInfo10FromParentEPKS1_PNS0_19AccountingAllocatorEPKNS0_15FunctionLiteralEPKNS0_12AstRawStringE@PLT
	pxor	%xmm1, %xmm1
	movl	24(%rbp), %eax
	movq	%r14, 56(%rbx)
	movups	%xmm1, 8(%rbx)
	movdqa	-80(%rbp), %xmm0
	leaq	-56(%rbp), %rdi
	movq	$0, 24(%rbx)
	movq	96(%r13), %rsi
	movups	%xmm0, 40(%rbx)
	movl	%eax, 32(%rbx)
	movq	(%rsi), %rax
	call	*24(%rax)
	movq	-56(%rbp), %r13
	movq	%r12, %rdi
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	movq	32(%r13), %rcx
	cltq
	movq	8(%r13), %rsi
	cmpq	%rcx, %rax
	jb	.L911
	movq	24(%r13), %rdx
	subq	%rsi, %rdx
	sarq	%rdx
	addq	%rcx, %rdx
	cmpq	%rdx, %rax
	jnb	.L911
	subq	%rcx, %rax
	leaq	(%rsi,%rax,2), %rax
	movq	%rax, 16(%r13)
.L912:
	movq	-56(%rbp), %rax
	movq	(%rbx), %rdi
	leaq	-48(%rbp), %r13
	movq	$0, -56(%rbp)
	movq	%r13, %rsi
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal9ParseInfo20set_character_streamESt10unique_ptrINS0_20Utf16CharacterStreamESt14default_deleteIS3_EE@PLT
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L913
	movq	(%rdi), %rax
	call	*8(%rax)
.L913:
	movq	80(%r12), %rdi
	testq	%rdi, %rdi
	je	.L914
	movq	(%rbx), %rdx
	movq	(%rdi), %rax
	movq	(%rdx), %rsi
	call	*8(%rax)
	movq	(%rbx), %rbx
	movq	%r13, %rdi
	movq	%rax, %rdx
	movq	(%rbx), %rsi
	call	_ZN2v88internal20ConsumedPreparseData3ForEPNS0_4ZoneEPNS0_16ZonePreparseDataE@PLT
	movq	104(%rbx), %rdi
	movq	-48(%rbp), %rax
	movq	%rax, 104(%rbx)
	movq	%rdi, -48(%rbp)
	testq	%rdi, %rdi
	je	.L914
	movq	(%rdi), %rax
	call	*8(%rax)
.L914:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L909
	movq	(%rdi), %rax
	call	*8(%rax)
.L909:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L931
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L911:
	.cfi_restore_state
	cmpb	$0, 48(%r13)
	movq	%rax, 32(%r13)
	movq	%rsi, 16(%r13)
	jne	.L912
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*40(%rax)
	jmp	.L912
.L931:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25264:
	.size	_ZN2v88internal21BackgroundCompileTaskC2EPNS0_19AccountingAllocatorEPKNS0_9ParseInfoEPKNS0_12AstRawStringEPKNS0_15FunctionLiteralEPNS0_28WorkerThreadRuntimeCallStatsEPNS0_14TimedHistogramEi, .-_ZN2v88internal21BackgroundCompileTaskC2EPNS0_19AccountingAllocatorEPKNS0_9ParseInfoEPKNS0_12AstRawStringEPKNS0_15FunctionLiteralEPNS0_28WorkerThreadRuntimeCallStatsEPNS0_14TimedHistogramEi
	.globl	_ZN2v88internal21BackgroundCompileTaskC1EPNS0_19AccountingAllocatorEPKNS0_9ParseInfoEPKNS0_12AstRawStringEPKNS0_15FunctionLiteralEPNS0_28WorkerThreadRuntimeCallStatsEPNS0_14TimedHistogramEi
	.set	_ZN2v88internal21BackgroundCompileTaskC1EPNS0_19AccountingAllocatorEPKNS0_9ParseInfoEPKNS0_12AstRawStringEPKNS0_15FunctionLiteralEPNS0_28WorkerThreadRuntimeCallStatsEPNS0_14TimedHistogramEi,_ZN2v88internal21BackgroundCompileTaskC2EPNS0_19AccountingAllocatorEPKNS0_9ParseInfoEPKNS0_12AstRawStringEPKNS0_15FunctionLiteralEPNS0_28WorkerThreadRuntimeCallStatsEPNS0_14TimedHistogramEi
	.section	.text._ZN2v88internal21BackgroundCompileTaskD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21BackgroundCompileTaskD2Ev
	.type	_ZN2v88internal21BackgroundCompileTaskD2Ev, @function
_ZN2v88internal21BackgroundCompileTaskD2Ev:
.LFB25267:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	24(%rdi), %r12
	testq	%r12, %r12
	jne	.L936
	jmp	.L933
	.p2align 4,,10
	.p2align 3
.L1008:
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L933
.L935:
	movq	%rbx, %r12
.L936:
	movq	8(%r12), %rdi
	movq	(%r12), %rbx
	testq	%rdi, %rdi
	jne	.L1008
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L935
.L933:
	movq	16(%r13), %rdi
	testq	%rdi, %rdi
	je	.L937
	movq	(%rdi), %rax
	call	*8(%rax)
.L937:
	movq	8(%r13), %r12
	testq	%r12, %r12
	je	.L938
	movq	744(%r12), %r14
	testq	%r14, %r14
	je	.L939
	movq	296(%r14), %rdi
	testq	%rdi, %rdi
	je	.L940
	call	_ZdlPv@PLT
.L940:
	movq	176(%r14), %rdi
	testq	%rdi, %rdi
	je	.L941
	call	_ZdlPv@PLT
.L941:
	movq	152(%r14), %rdi
	testq	%rdi, %rdi
	je	.L942
	call	_ZdlPv@PLT
.L942:
	movl	$320, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L939:
	movq	1120(%r12), %rdi
	movq	$0, 744(%r12)
	testq	%rdi, %rdi
	je	.L943
	call	_ZdlPv@PLT
.L943:
	leaq	680(%r12), %rdi
	call	_ZN2v88internal4ZoneD1Ev@PLT
	movq	632(%r12), %rdi
	testq	%rdi, %rdi
	je	.L944
	call	_ZdaPv@PLT
.L944:
	movq	608(%r12), %rdi
	testq	%rdi, %rdi
	je	.L945
	call	_ZdaPv@PLT
.L945:
	leaq	520(%r12), %rbx
	leaq	280(%r12), %r14
.L950:
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L946
	call	_ZdaPv@PLT
.L946:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L947
	call	_ZdaPv@PLT
	subq	$80, %rbx
	cmpq	%rbx, %r14
	jne	.L950
	movq	224(%r12), %rdi
	testq	%rdi, %rdi
	je	.L951
.L1009:
	call	_ZdlPv@PLT
.L951:
	movq	200(%r12), %rdi
	testq	%rdi, %rdi
	je	.L952
	call	_ZdlPv@PLT
.L952:
	movq	64(%r12), %rdi
	testq	%rdi, %rdi
	je	.L953
	call	_ZdlPv@PLT
.L953:
	movq	40(%r12), %rdi
	testq	%rdi, %rdi
	je	.L954
	call	_ZdlPv@PLT
.L954:
	movl	$1152, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L938:
	movq	0(%r13), %r12
	testq	%r12, %r12
	je	.L932
	movq	%r12, %rdi
	call	_ZN2v88internal9ParseInfoD1Ev@PLT
	popq	%rbx
	movq	%r12, %rdi
	movl	$224, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L947:
	.cfi_restore_state
	subq	$80, %rbx
	cmpq	%rbx, %r14
	jne	.L950
	movq	224(%r12), %rdi
	testq	%rdi, %rdi
	jne	.L1009
	jmp	.L951
	.p2align 4,,10
	.p2align 3
.L932:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25267:
	.size	_ZN2v88internal21BackgroundCompileTaskD2Ev, .-_ZN2v88internal21BackgroundCompileTaskD2Ev
	.globl	_ZN2v88internal21BackgroundCompileTaskD1Ev
	.set	_ZN2v88internal21BackgroundCompileTaskD1Ev,_ZN2v88internal21BackgroundCompileTaskD2Ev
	.section	.rodata._ZN2v88internal21BackgroundCompileTask3RunEv.str1.1,"aMS",@progbits,1
.LC28:
	.string	"BackgroundCompileTask::Run"
.LC29:
	.string	"V8.CompileCodeBackground"
	.section	.text._ZN2v88internal21BackgroundCompileTask3RunEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21BackgroundCompileTask3RunEv
	.type	_ZN2v88internal21BackgroundCompileTask3RunEv, @function
_ZN2v88internal21BackgroundCompileTask3RunEv:
.LFB25275:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-248(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-304(%rbp), %r13
	pushq	%r12
	movq	%r13, %rsi
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$312, %rsp
	movq	56(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -304(%rbp)
	movq	%rdi, -296(%rbp)
	movq	$0, -288(%rbp)
	call	_ZN2v88internal14TimedHistogram5StartEPNS_4base12ElapsedTimerEPNS0_7IsolateE@PLT
	movq	(%rbx), %rax
	movq	40(%rbx), %rsi
	movq	%r14, %rdi
	movl	32(%rbx), %r12d
	movq	136(%rax), %rdx
	movq	%rax, -272(%rbp)
	movq	32(%rax), %rax
	sall	$10, %r12d
	movq	%rdx, -264(%rbp)
	movslq	%r12d, %r12
	movq	%rax, -256(%rbp)
	call	_ZN2v88internal33WorkerThreadRuntimeCallStatsScopeC1EPNS0_28WorkerThreadRuntimeCallStatsE@PLT
	movq	-272(%rbp), %r15
	orl	$8192, 8(%r15)
	movq	-248(%rbp), %rax
	movq	%rax, 136(%r15)
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	subq	%r12, %rax
	movq	%rax, 32(%r15)
	movq	_ZZN2v88internal21BackgroundCompileTask3RunEvE29trace_event_unique_atomic1132(%rip), %r12
	testq	%r12, %r12
	je	.L1126
.L1012:
	movq	$0, -240(%rbp)
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L1127
.L1014:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -144(%rbp)
	movaps	%xmm0, -176(%rbp)
	movq	136(%rax), %rdi
	movaps	%xmm0, -160(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %edx
	testq	%rdi, %rdi
	je	.L1018
	testl	%edx, %edx
	jne	.L1128
.L1018:
	movq	96(%rax), %rdx
	movq	136(%rax), %rax
	movl	$1152, %edi
	movq	%rax, 40(%rdx)
	movq	(%rbx), %r12
	call	_Znwm@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal6ParserC1EPNS0_9ParseInfoE@PLT
	movq	8(%rbx), %r12
	movq	%r15, 8(%rbx)
	testq	%r12, %r12
	je	.L1019
	movq	744(%r12), %r15
	testq	%r15, %r15
	je	.L1020
	movq	296(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1021
	call	_ZdlPv@PLT
.L1021:
	movq	176(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1022
	call	_ZdlPv@PLT
.L1022:
	movq	152(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1023
	call	_ZdlPv@PLT
.L1023:
	movl	$320, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1020:
	movq	1120(%r12), %rdi
	movq	$0, 744(%r12)
	testq	%rdi, %rdi
	je	.L1024
	call	_ZdlPv@PLT
.L1024:
	leaq	680(%r12), %rdi
	call	_ZN2v88internal4ZoneD1Ev@PLT
	movq	632(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1025
	call	_ZdaPv@PLT
.L1025:
	movq	608(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1026
	call	_ZdaPv@PLT
.L1026:
	leaq	280(%r12), %rax
	leaq	520(%r12), %r15
	movq	%rax, -328(%rbp)
.L1031:
	movq	32(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1027
	call	_ZdaPv@PLT
.L1027:
	movq	8(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1028
	call	_ZdaPv@PLT
	subq	$80, %r15
	cmpq	%r15, -328(%rbp)
	jne	.L1031
	movq	224(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1032
.L1135:
	call	_ZdlPv@PLT
.L1032:
	movq	200(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1033
	call	_ZdlPv@PLT
.L1033:
	movq	64(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1034
	call	_ZdlPv@PLT
.L1034:
	movq	40(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1035
	call	_ZdlPv@PLT
.L1035:
	movl	$1152, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	movq	8(%rbx), %r15
.L1019:
	movq	(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal6Parser25InitializeEmptyScopeChainEPNS0_9ParseInfoE@PLT
	movq	8(%rbx), %rdi
	movq	(%rbx), %rsi
	call	_ZN2v88internal6Parser17ParseOnBackgroundEPNS0_9ParseInfoE@PLT
	movq	(%rbx), %r12
	cmpq	$0, 168(%r12)
	je	.L1036
	movq	48(%rbx), %r15
	movq	_ZZN2v88internal12_GLOBAL__N_125CompileOnBackgroundThreadEPNS0_9ParseInfoEPNS0_19AccountingAllocatorEPSt12forward_listISt10unique_ptrINS0_25UnoptimizedCompilationJobESt14default_deleteIS8_EESaISB_EEE29trace_event_unique_atomic1011(%rip), %rdx
	leaq	24(%rbx), %r11
	testq	%rdx, %rdx
	je	.L1129
.L1038:
	movq	$0, -208(%rbp)
	movzbl	(%rdx), %eax
	testb	$5, %al
	jne	.L1130
	movl	8(%r12), %eax
	movl	$120, %edx
	testb	$1, %al
	je	.L1044
.L1139:
	andl	$4, %eax
	cmpl	$1, %eax
	sbbl	%edx, %edx
	andl	$5, %edx
	addl	$119, %edx
.L1044:
	pxor	%xmm0, %xmm0
	movq	$0, -96(%rbp)
	movq	136(%r12), %rdi
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	je	.L1045
	testq	%rdi, %rdi
	jne	.L1131
.L1045:
	leaq	-312(%rbp), %rdi
	movq	%r11, %rcx
	movq	%r15, %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal12_GLOBAL__N_123GenerateUnoptimizedCodeEPNS0_9ParseInfoEPNS0_19AccountingAllocatorEPSt12forward_listISt10unique_ptrINS0_25UnoptimizedCompilationJobESt14default_deleteIS8_EESaISB_EE
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1132
.L1046:
	leaq	-208(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-312(%rbp), %rax
	movq	16(%rbx), %rdi
	movq	$0, -312(%rbp)
	movq	%rax, 16(%rbx)
	testq	%rdi, %rdi
	je	.L1036
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	-312(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1036
	movq	(%rdi), %rax
	call	*8(%rax)
.L1036:
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1133
.L1050:
	leaq	-240(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-272(%rbp), %rax
	movq	-256(%rbp), %rdx
	movq	%r14, %rdi
	movq	%rdx, 32(%rax)
	movq	-264(%rbp), %rdx
	andl	$-8193, 8(%rax)
	movq	%rdx, 136(%rax)
	call	_ZN2v88internal33WorkerThreadRuntimeCallStatsScopeD1Ev@PLT
	movq	-288(%rbp), %rdx
	movq	-296(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal14TimedHistogram4StopEPNS_4base12ElapsedTimerEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1134
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1028:
	.cfi_restore_state
	subq	$80, %r15
	cmpq	%r15, -328(%rbp)
	jne	.L1031
	movq	224(%r12), %rdi
	testq	%rdi, %rdi
	jne	.L1135
	jmp	.L1032
	.p2align 4,,10
	.p2align 3
.L1127:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1136
.L1015:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1016
	movq	(%rdi), %rax
	call	*8(%rax)
.L1016:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1017
	movq	(%rdi), %rax
	call	*8(%rax)
.L1017:
	leaq	.LC28(%rip), %rax
	movq	%r12, -232(%rbp)
	movq	%rax, -224(%rbp)
	leaq	-232(%rbp), %rax
	movq	%r15, -216(%rbp)
	movq	%rax, -240(%rbp)
	jmp	.L1014
	.p2align 4,,10
	.p2align 3
.L1126:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1137
.L1013:
	movq	%r12, _ZZN2v88internal21BackgroundCompileTask3RunEvE29trace_event_unique_atomic1132(%rip)
	jmp	.L1012
	.p2align 4,,10
	.p2align 3
.L1130:
	pxor	%xmm0, %xmm0
	movq	%rdx, -344(%rbp)
	movq	%r11, -336(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rcx
	movq	$0, -328(%rbp)
	movq	-336(%rbp), %r11
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	-344(%rbp), %rdx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1138
.L1041:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1042
	movq	(%rdi), %rax
	movq	%rdx, -344(%rbp)
	movq	%r11, -336(%rbp)
	call	*8(%rax)
	movq	-344(%rbp), %rdx
	movq	-336(%rbp), %r11
.L1042:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1043
	movq	(%rdi), %rax
	movq	%rdx, -344(%rbp)
	movq	%r11, -336(%rbp)
	call	*8(%rax)
	movq	-344(%rbp), %rdx
	movq	-336(%rbp), %r11
.L1043:
	leaq	.LC29(%rip), %rax
	movq	%rdx, -200(%rbp)
	movl	$120, %edx
	movq	%rax, -192(%rbp)
	movq	-328(%rbp), %rax
	movq	%rax, -184(%rbp)
	leaq	-200(%rbp), %rax
	movq	%rax, -208(%rbp)
	movl	8(%r12), %eax
	testb	$1, %al
	je	.L1044
	jmp	.L1139
	.p2align 4,,10
	.p2align 3
.L1129:
	movq	%r11, -328(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	-328(%rbp), %r11
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1140
.L1039:
	movq	%rdx, _ZZN2v88internal12_GLOBAL__N_125CompileOnBackgroundThreadEPNS0_9ParseInfoEPNS0_19AccountingAllocatorEPSt12forward_listISt10unique_ptrINS0_25UnoptimizedCompilationJobESt14default_deleteIS8_EESaISB_EEE29trace_event_unique_atomic1011(%rip)
	jmp	.L1038
	.p2align 4,,10
	.p2align 3
.L1132:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1046
	.p2align 4,,10
	.p2align 3
.L1133:
	leaq	-168(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1050
	.p2align 4,,10
	.p2align 3
.L1137:
	leaq	.LC6(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L1013
	.p2align 4,,10
	.p2align 3
.L1136:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC28(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L1015
.L1128:
	leaq	-168(%rbp), %rsi
	movl	$118, %edx
	movq	%rdi, -176(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	movq	(%rbx), %rax
	jmp	.L1018
.L1131:
	leaq	-120(%rbp), %rsi
	movq	%r11, -328(%rbp)
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	movq	-328(%rbp), %r11
	jmp	.L1045
	.p2align 4,,10
	.p2align 3
.L1140:
	leaq	.LC6(%rip), %rsi
	call	*%rax
	movq	-328(%rbp), %r11
	movq	%rax, %rdx
	jmp	.L1039
	.p2align 4,,10
	.p2align 3
.L1138:
	subq	$8, %rsp
	leaq	-80(%rbp), %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	movl	$88, %esi
	pushq	%rcx
	leaq	.LC29(%rip), %rcx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	movq	%r11, -344(%rbp)
	movq	%rdx, -336(%rbp)
	call	*%rax
	movq	-344(%rbp), %r11
	movq	-336(%rbp), %rdx
	addq	$64, %rsp
	movq	%rax, -328(%rbp)
	jmp	.L1041
.L1134:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25275:
	.size	_ZN2v88internal21BackgroundCompileTask3RunEv, .-_ZN2v88internal21BackgroundCompileTask3RunEv
	.section	.text._ZN2v88internal8Compiler7AnalyzeEPNS0_9ParseInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Compiler7AnalyzeEPNS0_9ParseInfoE
	.type	_ZN2v88internal8Compiler7AnalyzeEPNS0_9ParseInfoE, @function
_ZN2v88internal8Compiler7AnalyzeEPNS0_9ParseInfoE:
.LFB25276:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testb	$32, 9(%rdi)
	movq	136(%rdi), %rdi
	movaps	%xmm0, -64(%rbp)
	setne	%dl
	movq	$0, -32(%rbp)
	movaps	%xmm0, -48(%rbp)
	addl	$116, %edx
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testq	%rdi, %rdi
	je	.L1143
	testl	%eax, %eax
	jne	.L1155
.L1143:
	movq	%r12, %rdi
	call	_ZN2v88internal8Rewriter7RewriteEPNS0_9ParseInfoE@PLT
	testb	%al, %al
	je	.L1144
	movq	%r12, %rdi
	call	_ZN2v88internal16DeclarationScope7AnalyzeEPNS0_9ParseInfoE@PLT
.L1144:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1156
.L1141:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1157
	addq	$72, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1156:
	.cfi_restore_state
	leaq	-56(%rbp), %rsi
	movb	%al, -65(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	movzbl	-65(%rbp), %eax
	jmp	.L1141
.L1155:
	leaq	-56(%rbp), %rsi
	movq	%rdi, -64(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1143
.L1157:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25276:
	.size	_ZN2v88internal8Compiler7AnalyzeEPNS0_9ParseInfoE, .-_ZN2v88internal8Compiler7AnalyzeEPNS0_9ParseInfoE
	.section	.text._ZN2v88internal8Compiler15ParseAndAnalyzeEPNS0_9ParseInfoENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Compiler15ParseAndAnalyzeEPNS0_9ParseInfoENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateE
	.type	_ZN2v88internal8Compiler15ParseAndAnalyzeEPNS0_9ParseInfoENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateE, @function
_ZN2v88internal8Compiler15ParseAndAnalyzeEPNS0_9ParseInfoENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateE:
.LFB25277:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal7parsing8ParseAnyEPNS0_9ParseInfoENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateENS1_29ReportErrorsAndStatisticsModeE@PLT
	testb	%al, %al
	jne	.L1161
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1161:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8Compiler7AnalyzeEPNS0_9ParseInfoE
	.cfi_endproc
.LFE25277:
	.size	_ZN2v88internal8Compiler15ParseAndAnalyzeEPNS0_9ParseInfoENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateE, .-_ZN2v88internal8Compiler15ParseAndAnalyzeEPNS0_9ParseInfoENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateE
	.section	.rodata._ZN2v88internal8Compiler22CollectSourcePositionsEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEE.str1.1,"aMS",@progbits,1
.LC30:
	.string	"V8.CollectSourcePositions"
	.section	.text._ZN2v88internal8Compiler22CollectSourcePositionsEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Compiler22CollectSourcePositionsEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEE
	.type	_ZN2v88internal8Compiler22CollectSourcePositionsEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEE, @function
_ZN2v88internal8Compiler22CollectSourcePositionsEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEE:
.LFB25278:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movq	%rdi, %rsi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$504, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-464(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -488(%rbp)
	call	_ZN2v88internal20SaveAndSwitchContextC2EPNS0_7IsolateENS0_7ContextE@PLT
	movq	0(%r13), %rax
	movq	31(%rax), %rdx
	testb	$1, %dl
	jne	.L1280
.L1163:
	movq	7(%rax), %rdx
	testb	$1, %dl
	jne	.L1281
.L1165:
	movq	7(%rax), %rax
	movq	7(%rax), %rsi
.L1164:
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1166
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	37528(%rbx), %rax
	jb	.L1282
.L1169:
	movl	12616(%rbx), %eax
	xorl	%ecx, %ecx
	movl	$255, %edx
	movq	%rbx, %rsi
	movl	$3, 12616(%rbx)
	leaq	-352(%rbp), %rdi
	movl	%eax, -496(%rbp)
	call	_ZN2v88internal15InterruptsScopeC2EPNS0_7IsolateElNS1_4ModeE@PLT
	leaq	16+_ZTVN2v88internal23PostponeInterruptsScopeE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -368(%rbp)
	movq	%rax, -352(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1283
.L1174:
	movq	_ZZN2v88internal8Compiler22CollectSourcePositionsEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEEE29trace_event_unique_atomic1213(%rip), %r15
	testq	%r15, %r15
	je	.L1284
.L1176:
	movq	$0, -432(%rbp)
	movzbl	(%r15), %eax
	testb	$5, %al
	jne	.L1285
.L1178:
	movq	40960(%rbx), %rax
	leaq	-304(%rbp), %r15
	leaq	2544(%rax), %rdi
	leaq	2592(%rax), %rsi
	movq	%rax, -520(%rbp)
	movq	2576(%rax), %rax
	movq	%rdi, -504(%rbp)
	movq	16(%rax), %rdx
	movq	%rsi, -512(%rbp)
	call	_ZN2v88internal14TimedHistogram5StartEPNS_4base12ElapsedTimerEPNS0_7IsolateE@PLT
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal9ParseInfoC1EPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEE@PLT
	movl	-296(%rbp), %eax
	movl	%eax, %edx
	orl	$268435712, %edx
	cmpb	$0, _ZN2v88internal25FLAG_allow_natives_syntaxE(%rip)
	movl	%edx, -296(%rbp)
	je	.L1182
	orl	$268960000, %eax
	movl	%eax, -296(%rbp)
.L1182:
	movl	$1, %ecx
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal7parsing8ParseAnyEPNS0_9ParseInfoENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateENS1_29ReportErrorsAndStatisticsModeE@PLT
	movl	%eax, %r12d
	testb	%al, %al
	jne	.L1183
.L1279:
	movq	(%r14), %r13
	movq	%r13, %rax
	leaq	31(%r13), %rsi
	andq	$-262144, %rax
	movq	24(%rax), %rdx
	movq	-37280(%rdx), %r14
	movq	%r14, 31(%r13)
	testb	$1, %r14b
	je	.L1216
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rdx
	movq	%rcx, -528(%rbp)
	testl	$262144, %edx
	jne	.L1286
.L1190:
	andl	$24, %edx
	je	.L1216
	testb	$24, 8(%rax)
	je	.L1287
	.p2align 4,,10
	.p2align 3
.L1216:
	movq	96(%rbx), %rax
	movq	%rax, 12480(%rbx)
.L1187:
	movq	%r15, %rdi
	call	_ZN2v88internal9ParseInfoD1Ev@PLT
	movq	-520(%rbp), %rax
	movq	-512(%rbp), %rsi
	movq	-504(%rbp), %rdi
	movq	2576(%rax), %rax
	movq	16(%rax), %rdx
	call	_ZN2v88internal14TimedHistogram4StopEPNS_4base12ElapsedTimerEPNS0_7IsolateE@PLT
	leaq	-432(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-400(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1288
.L1212:
	leaq	16+_ZTVN2v88internal15InterruptsScopeE(%rip), %rax
	cmpl	$2, -320(%rbp)
	movq	%rax, -352(%rbp)
	je	.L1213
	movq	-344(%rbp), %rdi
	call	_ZN2v88internal10StackGuard18PopInterruptsScopeEv@PLT
.L1213:
	movl	-496(%rbp), %eax
	movl	%eax, 12616(%rbx)
.L1173:
	movq	-488(%rbp), %rdi
	call	_ZN2v88internal11SaveContextD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1289
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1183:
	.cfi_restore_state
	movq	%r15, %rdi
	call	_ZN2v88internal9ParseInfo20ResetCharacterStreamEv@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8Compiler7AnalyzeEPNS0_9ParseInfoE
	movl	%eax, %r12d
	testb	%al, %al
	je	.L1279
	leaq	-480(%rbp), %rax
	movq	41136(%rbx), %r8
	movq	%r14, %rcx
	movq	%r15, %rsi
	movq	-136(%rbp), %rdx
	movq	%rax, %rdi
	movq	%rax, -528(%rbp)
	call	_ZN2v88internal11interpreter11Interpreter30NewSourcePositionCollectionJobEPNS0_9ParseInfoEPNS0_15FunctionLiteralENS0_6HandleINS0_13BytecodeArrayEEEPNS0_19AccountingAllocatorE@PLT
	movq	-480(%rbp), %r8
	testq	%r8, %r8
	je	.L1192
	movq	%r8, -536(%rbp)
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	-536(%rbp), %r8
	movq	%rax, -544(%rbp)
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*16(%rax)
	movq	-536(%rbp), %r8
	testl	%eax, %eax
	jne	.L1193
	movl	$2, 8(%r8)
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	-536(%rbp), %r8
	subq	-544(%rbp), %rax
	movq	%rbx, %rsi
	movq	-528(%rbp), %rdi
	addq	%rax, 48(%r8)
	call	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb0EEC1EPNS0_7IsolateE@PLT
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	-536(%rbp), %r8
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%rax, -544(%rbp)
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*24(%rax)
	movq	-536(%rbp), %r8
	testl	%eax, %eax
	jne	.L1290
	movl	$3, 8(%r8)
	movq	%r8, -536(%rbp)
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	-536(%rbp), %r8
	subq	-544(%rbp), %rax
	movq	-528(%rbp), %rdi
	addq	%rax, 56(%r8)
	call	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb0EED1Ev@PLT
	movq	0(%r13), %rdx
	movq	-536(%rbp), %r8
	movq	31(%rdx), %rax
	testb	$1, %al
	je	.L1201
	movq	-1(%rax), %rcx
	cmpw	$86, 11(%rcx)
	jne	.L1201
	movq	39(%rax), %rax
	testb	$1, %al
	je	.L1201
	movq	-1(%rax), %rax
	cmpw	$72, 11(%rax)
	jne	.L1201
	movq	40(%r8), %rax
	movq	40(%rax), %rax
	movq	(%rax), %rax
	movq	31(%rax), %r13
	movq	%r13, %rcx
	notq	%rcx
	testb	$1, %r13b
	jne	.L1202
.L1206:
	andq	$-262144, %rax
	movq	24(%rax), %rax
	leaq	-37592(%rax), %rcx
	cmpq	-37280(%rax), %r13
	je	.L1291
	movq	7(%r13), %r13
	movq	%r13, %rcx
	notq	%rcx
.L1205:
	movq	7(%rdx), %rax
	testb	$1, %al
	jne	.L1292
.L1207:
	movq	7(%rdx), %rax
	movq	7(%rax), %r14
.L1208:
	movq	%r13, 31(%r14)
	andl	$1, %ecx
	leaq	31(%r14), %rsi
	jne	.L1201
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -544(%rbp)
	testl	$262144, %eax
	je	.L1210
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%r8, -536(%rbp)
	movq	%rsi, -528(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-544(%rbp), %rcx
	movq	-536(%rbp), %r8
	movq	-528(%rbp), %rsi
	movq	8(%rcx), %rax
.L1210:
	testb	$24, %al
	je	.L1201
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1201
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%r8, -528(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-528(%rbp), %r8
	jmp	.L1201
	.p2align 4,,10
	.p2align 3
.L1285:
	pxor	%xmm0, %xmm0
	xorl	%r12d, %r12d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1293
.L1179:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1180
	movq	(%rdi), %rax
	call	*8(%rax)
.L1180:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1181
	movq	(%rdi), %rax
	call	*8(%rax)
.L1181:
	leaq	.LC30(%rip), %rax
	movq	%r15, -424(%rbp)
	movq	%rax, -416(%rbp)
	leaq	-424(%rbp), %rax
	movq	%r12, -408(%rbp)
	movq	%rax, -432(%rbp)
	jmp	.L1178
	.p2align 4,,10
	.p2align 3
.L1284:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r15
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1294
.L1177:
	movq	%r15, _ZZN2v88internal8Compiler22CollectSourcePositionsEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEEE29trace_event_unique_atomic1213(%rip)
	jmp	.L1176
	.p2align 4,,10
	.p2align 3
.L1166:
	movq	41088(%rbx), %r14
	cmpq	41096(%rbx), %r14
	je	.L1295
.L1168:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r14)
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	37528(%rbx), %rax
	jnb	.L1169
.L1282:
	movq	(%r14), %r12
	movq	%r12, %rbx
	leaq	31(%r12), %r15
	andq	$-262144, %rbx
	movq	24(%rbx), %rax
	movq	-37280(%rax), %r13
	movq	%r13, 31(%r12)
	testb	$1, %r13b
	je	.L1214
	movq	%r13, %r14
	andq	$-262144, %r14
	movq	8(%r14), %rax
	testl	$262144, %eax
	jne	.L1296
	testb	$24, %al
	je	.L1214
.L1297:
	testb	$24, 8(%rbx)
	jne	.L1214
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L1214:
	xorl	%r12d, %r12d
	jmp	.L1173
	.p2align 4,,10
	.p2align 3
.L1280:
	movq	-1(%rdx), %rcx
	cmpw	$86, 11(%rcx)
	jne	.L1163
	movq	39(%rdx), %rcx
	testb	$1, %cl
	je	.L1163
	movq	-1(%rcx), %rcx
	cmpw	$72, 11(%rcx)
	jne	.L1163
	movq	31(%rdx), %rsi
	jmp	.L1164
	.p2align 4,,10
	.p2align 3
.L1281:
	movq	-1(%rdx), %rdx
	cmpw	$72, 11(%rdx)
	jne	.L1165
	movq	7(%rax), %rsi
	jmp	.L1164
	.p2align 4,,10
	.p2align 3
.L1286:
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, -544(%rbp)
	movq	%rsi, -536(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-528(%rbp), %rcx
	movq	-544(%rbp), %rax
	movq	-536(%rbp), %rsi
	movq	8(%rcx), %rdx
	jmp	.L1190
	.p2align 4,,10
	.p2align 3
.L1296:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r14), %rax
	testb	$24, %al
	jne	.L1297
	jmp	.L1214
	.p2align 4,,10
	.p2align 3
.L1287:
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1216
	.p2align 4,,10
	.p2align 3
.L1290:
	movl	$4, 8(%r8)
	movq	%r8, -536(%rbp)
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	-536(%rbp), %r8
	subq	-544(%rbp), %rax
	movq	-528(%rbp), %rdi
	addq	%rax, 56(%r8)
	call	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb0EED1Ev@PLT
	movq	-536(%rbp), %r8
	.p2align 4,,10
	.p2align 3
.L1192:
	movq	(%r14), %r12
	movq	%r12, %r14
	leaq	31(%r12), %rsi
	andq	$-262144, %r14
	movq	24(%r14), %rax
	movq	-37280(%rax), %r13
	movq	%r13, 31(%r12)
	testb	$1, %r13b
	je	.L1217
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -528(%rbp)
	testl	$262144, %eax
	je	.L1199
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%r8, -544(%rbp)
	movq	%rsi, -536(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-528(%rbp), %rcx
	movq	-544(%rbp), %r8
	movq	-536(%rbp), %rsi
	movq	8(%rcx), %rax
.L1199:
	testb	$24, %al
	je	.L1217
	testb	$24, 8(%r14)
	je	.L1298
	.p2align 4,,10
	.p2align 3
.L1217:
	movq	96(%rbx), %rax
	xorl	%r12d, %r12d
	movq	%rax, 12480(%rbx)
	testq	%r8, %r8
	je	.L1187
.L1201:
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
	jmp	.L1187
	.p2align 4,,10
	.p2align 3
.L1295:
	movq	%rbx, %rdi
	movq	%rsi, -496(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-496(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L1168
	.p2align 4,,10
	.p2align 3
.L1283:
	movq	40960(%rbx), %rax
	leaq	-392(%rbp), %rsi
	movl	$125, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -400(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1174
	.p2align 4,,10
	.p2align 3
.L1288:
	leaq	-392(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1212
	.p2align 4,,10
	.p2align 3
.L1193:
	movl	$4, 8(%r8)
	movq	%r8, -528(%rbp)
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	-528(%rbp), %r8
	subq	-544(%rbp), %rax
	addq	%rax, 48(%r8)
	jmp	.L1192
	.p2align 4,,10
	.p2align 3
.L1293:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC30(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r15, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r12
	addq	$64, %rsp
	jmp	.L1179
	.p2align 4,,10
	.p2align 3
.L1294:
	leaq	.LC6(%rip), %rsi
	call	*%rax
	movq	%rax, %r15
	jmp	.L1177
	.p2align 4,,10
	.p2align 3
.L1298:
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%r8, -528(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-528(%rbp), %r8
	jmp	.L1217
.L1292:
	movq	-1(%rax), %rax
	cmpw	$72, 11(%rax)
	jne	.L1207
	movq	7(%rdx), %r14
	jmp	.L1208
.L1202:
	movq	-1(%r13), %rsi
	cmpw	$70, 11(%rsi)
	jne	.L1206
	jmp	.L1205
.L1289:
	call	__stack_chk_fail@PLT
.L1291:
	movq	976(%rcx), %r13
	movq	%r13, %rcx
	notq	%rcx
	jmp	.L1205
	.cfi_endproc
.LFE25278:
	.size	_ZN2v88internal8Compiler22CollectSourcePositionsEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEE, .-_ZN2v88internal8Compiler22CollectSourcePositionsEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEE
	.section	.rodata._ZN2v88internal8Compiler29FinalizeBackgroundCompileTaskEPNS0_21BackgroundCompileTaskENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateENS1_18ClearExceptionFlagE.str1.8,"aMS",@progbits,1
	.align 8
.LC31:
	.string	"V8.FinalizeBackgroundCompileTask"
	.section	.text._ZN2v88internal8Compiler29FinalizeBackgroundCompileTaskEPNS0_21BackgroundCompileTaskENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateENS1_18ClearExceptionFlagE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Compiler29FinalizeBackgroundCompileTaskEPNS0_21BackgroundCompileTaskENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateENS1_18ClearExceptionFlagE
	.type	_ZN2v88internal8Compiler29FinalizeBackgroundCompileTaskEPNS0_21BackgroundCompileTaskENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateENS1_18ClearExceptionFlagE, @function
_ZN2v88internal8Compiler29FinalizeBackgroundCompileTaskEPNS0_21BackgroundCompileTaskENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateENS1_18ClearExceptionFlagE:
.LFB25284:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$152, %rsp
	movl	%ecx, -180(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	_ZZN2v88internal8Compiler29FinalizeBackgroundCompileTaskEPNS0_21BackgroundCompileTaskENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateENS1_18ClearExceptionFlagEE29trace_event_unique_atomic1421(%rip), %r15
	testq	%r15, %r15
	je	.L1346
.L1301:
	movq	$0, -160(%rbp)
	movzbl	(%r15), %eax
	testb	$5, %al
	jne	.L1347
.L1303:
	pxor	%xmm0, %xmm0
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1348
.L1307:
	movq	41088(%r13), %rax
	addl	$1, 41104(%r13)
	movq	(%rbx), %r15
	movq	%rax, -176(%rbp)
	movq	41096(%r13), %rax
	movq	%rax, -168(%rbp)
	movq	(%r12), %rax
	movq	31(%rax), %rsi
	testb	$1, %sil
	jne	.L1349
.L1308:
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1309
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L1310:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal9ParseInfo10set_scriptENS0_6HandleINS0_6ScriptEEE@PLT
	movq	8(%rbx), %rdi
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	_ZN2v88internal6Parser16UpdateStatisticsEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEE@PLT
	movq	8(%rbx), %rdi
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	_ZN2v88internal6Parser23HandleSourceURLCommentsEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEE@PLT
	cmpq	$0, 168(%r15)
	je	.L1312
	cmpq	$0, 16(%rbx)
	je	.L1312
	movq	112(%r15), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal15AstValueFactory11InternalizeEPNS0_7IsolateE@PLT
	movq	16(%rbx), %r8
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%r8, -192(%rbp)
	call	_ZN2v88internal16DeclarationScope18AllocateScopeInfosEPNS0_9ParseInfoEPNS0_7IsolateE@PLT
	movq	-192(%rbp), %r8
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal12_GLOBAL__N_133FinalizeUnoptimizedCompilationJobEPNS0_25UnoptimizedCompilationJobENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateE
	testl	%eax, %eax
	jne	.L1319
	leaq	24(%rbx), %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal12_GLOBAL__N_123FinalizeUnoptimizedCodeEPNS0_9ParseInfoEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_25UnoptimizedCompilationJobEPSt12forward_listISt10unique_ptrIS9_St14default_deleteIS9_EESaISF_EE.part.0
	movl	%eax, %r12d
	testb	%al, %al
	je	.L1319
.L1315:
	movq	-176(%rbp), %rax
	subl	$1, 41104(%r13)
	movq	%rax, 41088(%r13)
	movq	-168(%rbp), %rax
	cmpq	41096(%r13), %rax
	je	.L1324
	movq	%rax, 41096(%r13)
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1324:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1350
.L1322:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1351
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1312:
	.cfi_restore_state
	cmpl	$1, -180(%rbp)
	je	.L1345
	movq	12480(%r13), %rax
	cmpq	%rax, 96(%r13)
	je	.L1352
.L1316:
	xorl	%r12d, %r12d
	jmp	.L1315
	.p2align 4,,10
	.p2align 3
.L1345:
	movq	96(%r13), %rax
	xorl	%r12d, %r12d
	movq	%rax, 12480(%r13)
	jmp	.L1315
	.p2align 4,,10
	.p2align 3
.L1309:
	movq	41088(%r13), %r14
	cmpq	41096(%r13), %r14
	je	.L1353
.L1311:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r14)
	jmp	.L1310
	.p2align 4,,10
	.p2align 3
.L1349:
	movq	-1(%rsi), %rax
	cmpw	$86, 11(%rax)
	jne	.L1308
	movq	23(%rsi), %rsi
	jmp	.L1308
	.p2align 4,,10
	.p2align 3
.L1347:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1354
.L1304:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1305
	movq	(%rdi), %rax
	call	*8(%rax)
.L1305:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1306
	movq	(%rdi), %rax
	call	*8(%rax)
.L1306:
	leaq	.LC31(%rip), %rax
	movq	%r15, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1303
	.p2align 4,,10
	.p2align 3
.L1346:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r15
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1355
.L1302:
	movq	%r15, _ZZN2v88internal8Compiler29FinalizeBackgroundCompileTaskEPNS0_21BackgroundCompileTaskENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateENS1_18ClearExceptionFlagEE29trace_event_unique_atomic1421(%rip)
	jmp	.L1301
	.p2align 4,,10
	.p2align 3
.L1319:
	cmpl	$1, -180(%rbp)
	je	.L1345
	movq	12480(%r13), %rax
	cmpq	%rax, 96(%r13)
	jne	.L1316
	movzbl	176(%r15), %r12d
	testb	%r12b, %r12b
	je	.L1321
	movq	112(%r15), %r8
	movq	80(%r15), %rdx
	movq	%r13, %rsi
	xorl	%r12d, %r12d
	leaq	176(%r15), %rdi
	movq	%r8, %rcx
	call	_ZN2v88internal30PendingCompilationErrorHandler12ReportErrorsEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEEPNS0_15AstValueFactoryE@PLT
	jmp	.L1315
	.p2align 4,,10
	.p2align 3
.L1352:
	movzbl	176(%r15), %r12d
	testb	%r12b, %r12b
	je	.L1321
	movq	112(%r15), %rcx
	movq	80(%r15), %rdx
	movq	%r13, %rsi
	xorl	%r12d, %r12d
	leaq	176(%r15), %rdi
	call	_ZN2v88internal30PendingCompilationErrorHandler12ReportErrorsEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEEPNS0_15AstValueFactoryE@PLT
	jmp	.L1315
	.p2align 4,,10
	.p2align 3
.L1348:
	movq	40960(%r13), %rax
	leaq	-120(%rbp), %rsi
	movl	$129, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1307
	.p2align 4,,10
	.p2align 3
.L1350:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1322
	.p2align 4,,10
	.p2align 3
.L1353:
	movq	%r13, %rdi
	movq	%rsi, -192(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-192(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L1311
	.p2align 4,,10
	.p2align 3
.L1321:
	movq	%r13, %rdi
	call	_ZN2v88internal7Isolate13StackOverflowEv@PLT
	jmp	.L1315
	.p2align 4,,10
	.p2align 3
.L1355:
	leaq	.LC6(%rip), %rsi
	call	*%rax
	movq	%rax, %r15
	jmp	.L1302
	.p2align 4,,10
	.p2align 3
.L1354:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC31(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r15, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1304
.L1351:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25284:
	.size	_ZN2v88internal8Compiler29FinalizeBackgroundCompileTaskEPNS0_21BackgroundCompileTaskENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateENS1_18ClearExceptionFlagE, .-_ZN2v88internal8Compiler29FinalizeBackgroundCompileTaskEPNS0_21BackgroundCompileTaskENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateENS1_18ClearExceptionFlagE
	.section	.text._ZN2v88internal8Compiler16CompileOptimizedENS0_6HandleINS0_10JSFunctionEEENS0_15ConcurrencyModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Compiler16CompileOptimizedENS0_6HandleINS0_10JSFunctionEEENS0_15ConcurrencyModeE
	.type	_ZN2v88internal8Compiler16CompileOptimizedENS0_6HandleINS0_10JSFunctionEEENS0_15ConcurrencyModeE, @function
_ZN2v88internal8Compiler16CompileOptimizedENS0_6HandleINS0_10JSFunctionEEENS0_15ConcurrencyModeE:
.LFB25285:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	movq	47(%rax), %rdx
	cmpl	$67, 59(%rdx)
	jne	.L1380
.L1362:
	andq	$-262144, %rax
	xorl	%ecx, %ecx
	movl	$-1, %edx
	movq	%rbx, %rdi
	movq	24(%rax), %r12
	call	_ZN2v88internal12_GLOBAL__N_116GetOptimizedCodeENS0_6HandleINS0_10JSFunctionEEENS0_15ConcurrencyModeENS0_9BailoutIdEPNS0_15JavaScriptFrameE
	testq	%rax, %rax
	je	.L1381
.L1366:
	movq	(%rbx), %rdi
	movq	(%rax), %rdx
	movq	%rdx, 47(%rdi)
	leaq	47(%rdi), %rsi
	testb	$1, %dl
	je	.L1364
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	jne	.L1382
.L1364:
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1380:
	.cfi_restore_state
	movabsq	$287762808832, %rcx
	movq	23(%rax), %rdx
	movq	7(%rdx), %rdx
	cmpq	%rcx, %rdx
	je	.L1379
	testb	$1, %dl
	jne	.L1383
.L1359:
	movq	47(%rax), %rdx
	testb	$62, 43(%rdx)
	je	.L1361
.L1379:
	movq	(%rbx), %rax
	jmp	.L1362
	.p2align 4,,10
	.p2align 3
.L1382:
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1381:
	.cfi_restore_state
	leaq	3592(%r12), %rdi
	movl	$57, %esi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	jmp	.L1366
	.p2align 4,,10
	.p2align 3
.L1361:
	movq	47(%rax), %rax
	movq	31(%rax), %rax
	movl	15(%rax), %eax
	testb	$1, %al
	je	.L1364
	jmp	.L1379
	.p2align 4,,10
	.p2align 3
.L1383:
	movq	-1(%rdx), %rcx
	cmpw	$165, 11(%rcx)
	je	.L1379
	movq	-1(%rdx), %rdx
	cmpw	$166, 11(%rdx)
	jne	.L1359
	jmp	.L1379
	.cfi_endproc
.LFE25285:
	.size	_ZN2v88internal8Compiler16CompileOptimizedENS0_6HandleINS0_10JSFunctionEEENS0_15ConcurrencyModeE, .-_ZN2v88internal8Compiler16CompileOptimizedENS0_6HandleINS0_10JSFunctionEEENS0_15ConcurrencyModeE
	.section	.text._ZN2v88internal32CodeGenerationFromStringsAllowedEPNS0_7IsolateENS0_6HandleINS0_7ContextEEENS3_INS0_6StringEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal32CodeGenerationFromStringsAllowedEPNS0_7IsolateENS0_6HandleINS0_7ContextEEENS3_INS0_6StringEEE
	.type	_ZN2v88internal32CodeGenerationFromStringsAllowedEPNS0_7IsolateENS0_6HandleINS0_7ContextEEENS3_INS0_6StringEEE, @function
_ZN2v88internal32CodeGenerationFromStringsAllowedEPNS0_7IsolateENS0_6HandleINS0_7ContextEEENS3_INS0_6StringEEE:
.LFB25294:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$64, %rsp
	movl	12616(%rdi), %r14d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -48(%rbp)
	movl	$6, 12616(%rdi)
	movaps	%xmm0, -80(%rbp)
	movaps	%xmm0, -64(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1395
.L1385:
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	*41640(%rbx)
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1396
.L1386:
	movl	%r14d, 12616(%rbx)
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1397
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1395:
	.cfi_restore_state
	movq	40960(%rdi), %rax
	leaq	-72(%rbp), %rsi
	movl	$115, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -80(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1385
	.p2align 4,,10
	.p2align 3
.L1396:
	leaq	-72(%rbp), %rsi
	movb	%al, -81(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	movzbl	-81(%rbp), %eax
	jmp	.L1386
.L1397:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25294:
	.size	_ZN2v88internal32CodeGenerationFromStringsAllowedEPNS0_7IsolateENS0_6HandleINS0_7ContextEEENS3_INS0_6StringEEE, .-_ZN2v88internal32CodeGenerationFromStringsAllowedEPNS0_7IsolateENS0_6HandleINS0_7ContextEEENS3_INS0_6StringEEE
	.section	.text._ZN2v88internal31ModifyCodeGenerationFromStringsEPNS0_7IsolateENS0_6HandleINS0_7ContextEEEPNS3_INS0_6ObjectEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal31ModifyCodeGenerationFromStringsEPNS0_7IsolateENS0_6HandleINS0_7ContextEEEPNS3_INS0_6ObjectEEE
	.type	_ZN2v88internal31ModifyCodeGenerationFromStringsEPNS0_7IsolateENS0_6HandleINS0_7ContextEEEPNS3_INS0_6ObjectEEE, @function
_ZN2v88internal31ModifyCodeGenerationFromStringsEPNS0_7IsolateENS0_6HandleINS0_7ContextEEEPNS3_INS0_6ObjectEEE:
.LFB25295:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movl	12616(%rdi), %r13d
	movq	41648(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -64(%rbp)
	movl	$6, 12616(%rdi)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1411
.L1399:
	movq	(%r12), %rsi
	movq	%r14, %rdi
	call	*%r15
	testq	%rax, %rax
	je	.L1403
	movq	-96(%rbp), %rdi
	movq	%rax, (%r12)
	movl	$1, %eax
	testq	%rdi, %rdi
	jne	.L1412
.L1401:
	movl	%r13d, 12616(%rbx)
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1413
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1403:
	.cfi_restore_state
	movq	-96(%rbp), %rdi
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L1401
	.p2align 4,,10
	.p2align 3
.L1412:
	leaq	-88(%rbp), %rsi
	movb	%al, -97(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	movzbl	-97(%rbp), %eax
	jmp	.L1401
	.p2align 4,,10
	.p2align 3
.L1411:
	movq	40960(%rdi), %rax
	leaq	-88(%rbp), %rsi
	movl	$115, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1399
.L1413:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25295:
	.size	_ZN2v88internal31ModifyCodeGenerationFromStringsEPNS0_7IsolateENS0_6HandleINS0_7ContextEEEPNS3_INS0_6ObjectEEE, .-_ZN2v88internal31ModifyCodeGenerationFromStringsEPNS0_7IsolateENS0_6HandleINS0_7ContextEEEPNS3_INS0_6ObjectEEE
	.section	.text._ZN2v88internal8Compiler32ValidateDynamicCompilationSourceEPNS0_7IsolateENS0_6HandleINS0_7ContextEEENS4_INS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Compiler32ValidateDynamicCompilationSourceEPNS0_7IsolateENS0_6HandleINS0_7ContextEEENS4_INS0_6ObjectEEE
	.type	_ZN2v88internal8Compiler32ValidateDynamicCompilationSourceEPNS0_7IsolateENS0_6HandleINS0_7ContextEEENS4_INS0_6ObjectEEE, @function
_ZN2v88internal8Compiler32ValidateDynamicCompilationSourceEPNS0_7IsolateENS0_6HandleINS0_7ContextEEENS4_INS0_6ObjectEEE:
.LFB25296:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	testb	$1, %al
	jne	.L1415
.L1417:
	xorl	%r13d, %r13d
.L1416:
	movq	(%r15), %rdx
	movq	71(%rdx), %rdx
	cmpq	%rdx, 120(%rbx)
	je	.L1418
.L1461:
	movl	$1, %ecx
	testb	$1, %al
	jne	.L1462
.L1426:
	xorl	%edx, %edx
	movq	%r13, %rax
	movb	%cl, %dl
.L1420:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1463
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1462:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	seta	%cl
	jmp	.L1426
	.p2align 4,,10
	.p2align 3
.L1418:
	cmpq	$0, 41640(%rbx)
	je	.L1421
	testb	$1, %al
	jne	.L1464
.L1421:
	movq	41648(%rbx), %r13
	testq	%r13, %r13
	je	.L1458
	pxor	%xmm0, %xmm0
	movl	12616(%rbx), %r14d
	movq	$0, -64(%rbp)
	movl	$6, 12616(%rbx)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1465
.L1429:
	movq	%r15, %rdi
	movq	%r12, %rsi
	call	*%r13
	movq	-96(%rbp), %rdi
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1430
	testq	%rdi, %rdi
	jne	.L1466
	movl	%r14d, 12616(%rbx)
.L1432:
	movq	0(%r13), %rdx
	movl	$1, %ecx
	testb	$1, %dl
	jne	.L1467
	.p2align 4,,10
	.p2align 3
.L1436:
	xorl	%edx, %edx
	xorl	%eax, %eax
	movb	%cl, %dl
	jmp	.L1420
	.p2align 4,,10
	.p2align 3
.L1415:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L1417
	movq	%r12, %r13
	jmp	.L1416
	.p2align 4,,10
	.p2align 3
.L1430:
	testq	%rdi, %rdi
	jne	.L1468
.L1456:
	movl	%r14d, 12616(%rbx)
	.p2align 4,,10
	.p2align 3
.L1458:
	movq	(%r12), %rdx
	movq	%rdx, %rax
	notq	%rax
	andl	$1, %eax
.L1428:
	movl	$1, %ecx
	testb	%al, %al
	jne	.L1436
	movq	-1(%rdx), %rax
	cmpw	$63, 11(%rax)
	seta	%cl
	jmp	.L1436
	.p2align 4,,10
	.p2align 3
.L1464:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1421
	pxor	%xmm0, %xmm0
	movl	12616(%rbx), %r14d
	movq	$0, -64(%rbp)
	movl	$6, 12616(%rbx)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1469
.L1424:
	movq	%r15, %rdi
	movq	%r13, %rsi
	call	*41640(%rbx)
	movq	-96(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1470
.L1425:
	movl	%r14d, 12616(%rbx)
	testb	%al, %al
	je	.L1421
	movq	(%r12), %rax
	jmp	.L1461
	.p2align 4,,10
	.p2align 3
.L1466:
	leaq	-88(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	movl	%r14d, 12616(%rbx)
	jmp	.L1432
	.p2align 4,,10
	.p2align 3
.L1467:
	movq	-1(%rdx), %rax
	cmpw	$63, 11(%rax)
	jbe	.L1435
	xorl	%eax, %eax
	jmp	.L1428
	.p2align 4,,10
	.p2align 3
.L1465:
	movq	40960(%rbx), %rax
	leaq	-88(%rbp), %rsi
	movl	$115, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1429
	.p2align 4,,10
	.p2align 3
.L1435:
	movq	%r13, %rax
	xorl	%edx, %edx
	jmp	.L1420
.L1470:
	leaq	-88(%rbp), %rsi
	movb	%al, -97(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	movzbl	-97(%rbp), %eax
	jmp	.L1425
.L1469:
	movq	40960(%rbx), %rax
	leaq	-88(%rbp), %rsi
	movl	$115, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1424
.L1463:
	call	__stack_chk_fail@PLT
.L1468:
	leaq	-88(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1456
	.cfi_endproc
.LFE25296:
	.size	_ZN2v88internal8Compiler32ValidateDynamicCompilationSourceEPNS0_7IsolateENS0_6HandleINS0_7ContextEEENS4_INS0_6ObjectEEE, .-_ZN2v88internal8Compiler32ValidateDynamicCompilationSourceEPNS0_7IsolateENS0_6HandleINS0_7ContextEEENS4_INS0_6ObjectEEE
	.section	.text._ZN2v88internal8Compiler38GetSharedFunctionInfoForStreamedScriptEPNS0_7IsolateENS0_6HandleINS0_6StringEEERKNS1_13ScriptDetailsENS_19ScriptOriginOptionsEPNS0_19ScriptStreamingDataE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Compiler38GetSharedFunctionInfoForStreamedScriptEPNS0_7IsolateENS0_6HandleINS0_6StringEEERKNS1_13ScriptDetailsENS_19ScriptOriginOptionsEPNS0_19ScriptStreamingDataE
	.type	_ZN2v88internal8Compiler38GetSharedFunctionInfoForStreamedScriptEPNS0_7IsolateENS0_6HandleINS0_6StringEEERKNS1_13ScriptDetailsENS_19ScriptOriginOptionsEPNS0_19ScriptStreamingDataE, @function
_ZN2v88internal8Compiler38GetSharedFunctionInfoForStreamedScriptEPNS0_7IsolateENS0_6HandleINS0_6StringEEERKNS1_13ScriptDetailsENS_19ScriptOriginOptionsEPNS0_19ScriptStreamingDataE:
.LFB25330:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$184, %rsp
	movl	%ecx, -176(%rbp)
	movq	%r8, -168(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -160(%rbp)
	movq	$0, -152(%rbp)
	movq	$0, -144(%rbp)
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	40960(%r15), %rsi
	movq	%rax, -152(%rbp)
	movq	2856(%rsi), %rax
	leaq	2824(%rsi), %rdi
	addq	$2872, %rsi
	movq	%rdi, -136(%rbp)
	movq	16(%rax), %rdx
	call	_ZN2v88internal14TimedHistogram5StartEPNS_4base12ElapsedTimerEPNS0_7IsolateE@PLT
	xorl	%ecx, %ecx
	leaq	-112(%rbp), %rdi
	movl	$255, %edx
	movq	%r15, %rsi
	movq	$5, -128(%rbp)
	call	_ZN2v88internal15InterruptsScopeC2EPNS0_7IsolateElNS1_4ModeE@PLT
	leaq	16+_ZTVN2v88internal23PostponeInterruptsScopeE(%rip), %rax
	movq	40960(%r15), %r14
	movq	%rax, -112(%rbp)
	movq	0(%r13), %rax
	cmpb	$0, 6432(%r14)
	movl	11(%rax), %r12d
	je	.L1472
	movq	6424(%r14), %rax
.L1473:
	testq	%rax, %rax
	je	.L1474
	addl	%r12d, (%rax)
.L1474:
	movq	40960(%r15), %r14
	cmpb	$0, 6528(%r14)
	je	.L1475
	movq	6520(%r14), %rax
.L1476:
	testq	%rax, %rax
	je	.L1477
	addl	%r12d, (%rax)
.L1477:
	movq	-168(%rbp), %rax
	movq	16(%rax), %rax
	movq	(%rax), %r12
	movq	%rax, -192(%rbp)
	movq	40952(%r15), %rax
	movl	8(%r12), %r14d
	movq	%rax, -184(%rbp)
	movq	12464(%r15), %rax
	shrl	$3, %r14d
	movq	39(%rax), %rsi
	movq	41112(%r15), %rdi
	andl	$1, %r14d
	testq	%rdi, %rdi
	je	.L1478
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L1479:
	movq	8(%rbx), %rdx
	movl	-176(%rbp), %r9d
	pushq	%r14
	movq	%r13, %rsi
	pushq	%rax
	movq	-184(%rbp), %rdi
	movl	(%rbx), %ecx
	movl	4(%rbx), %r8d
	call	_ZN2v88internal16CompilationCache12LookupScriptENS0_6HandleINS0_6StringEEENS0_11MaybeHandleINS0_6ObjectEEEiiNS_19ScriptOriginOptionsENS2_INS0_7ContextEEENS0_12LanguageModeE@PLT
	popq	%rdx
	popq	%rcx
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1481
	movb	$1, -124(%rbp)
.L1482:
	movq	-168(%rbp), %rax
	movq	16(%rax), %r13
	movq	$0, 16(%rax)
	testq	%r13, %r13
	je	.L1503
	movq	%r13, %rdi
	call	_ZN2v88internal21BackgroundCompileTaskD1Ev
	movl	$64, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1503:
	leaq	16+_ZTVN2v88internal15InterruptsScopeE(%rip), %rax
	cmpl	$2, -80(%rbp)
	movq	%rax, -112(%rbp)
	je	.L1504
	movq	-104(%rbp), %rdi
	call	_ZN2v88internal10StackGuard18PopInterruptsScopeEv@PLT
.L1504:
	cmpb	$0, -123(%rbp)
	movzbl	-124(%rbp), %eax
	je	.L1505
	movq	-160(%rbp), %rdx
	movq	40960(%rdx), %rdi
	addq	$1728, %rdi
	testb	%al, %al
	jne	.L1554
	xorl	%esi, %esi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
.L1519:
	movq	-160(%rbp), %rax
	movq	40960(%rax), %rax
	addq	$4968, %rax
	.p2align 4,,10
	.p2align 3
.L1525:
	movq	-136(%rbp), %rdi
	movq	%rax, -144(%rbp)
	movq	32(%rdi), %rax
	leaq	48(%rdi), %rsi
	movq	16(%rax), %rdx
	call	_ZN2v88internal14TimedHistogram4StopEPNS_4base12ElapsedTimerEPNS0_7IsolateE@PLT
	movq	-144(%rbp), %rdi
	xorl	%edx, %edx
	leaq	-152(%rbp), %rsi
	call	_ZN2v88internal14TimedHistogram4StopEPNS_4base12ElapsedTimerEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1555
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1505:
	.cfi_restore_state
	cmpb	$0, -122(%rbp)
	je	.L1508
	movq	-160(%rbp), %rdx
	movq	40960(%rdx), %rdi
	addq	$1728, %rdi
	testb	%al, %al
	jne	.L1509
	cmpb	$0, -121(%rbp)
	je	.L1556
	movl	$3, %esi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
.L1523:
	movq	-160(%rbp), %rax
	movq	40960(%rax), %rax
	addq	$5112, %rax
	jmp	.L1525
	.p2align 4,,10
	.p2align 3
.L1475:
	movb	$1, 6528(%r14)
	leaq	6504(%r14), %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 6520(%r14)
	jmp	.L1476
	.p2align 4,,10
	.p2align 3
.L1472:
	movb	$1, 6432(%r14)
	leaq	6408(%r14), %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 6424(%r14)
	jmp	.L1473
	.p2align 4,,10
	.p2align 3
.L1478:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L1557
.L1480:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%rsi, (%rax)
	jmp	.L1479
	.p2align 4,,10
	.p2align 3
.L1508:
	movl	-128(%rbp), %edx
	testb	%al, %al
	je	.L1512
	movq	-160(%rbp), %rax
	movq	40960(%rax), %rdi
	addq	$1728, %rdi
	cmpl	$5, %edx
	je	.L1558
	movl	$1, %esi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
.L1516:
	movq	-160(%rbp), %rax
	movq	40960(%rax), %rax
	addq	$5016, %rax
	jmp	.L1525
	.p2align 4,,10
	.p2align 3
.L1554:
	movl	$14, %esi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	jmp	.L1519
	.p2align 4,,10
	.p2align 3
.L1512:
	cmpl	$14, %edx
	ja	.L1515
	leaq	CSWTCH.453(%rip), %rax
	movl	(%rax,%rdx,4), %ebx
	movq	-160(%rbp), %rax
	movq	40960(%rax), %rdi
	movl	%ebx, %esi
	addq	$1728, %rdi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	cmpl	$20, %ebx
	ja	.L1515
	leaq	.L1517(%rip), %rdx
	movslq	(%rdx,%rbx,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8Compiler38GetSharedFunctionInfoForStreamedScriptEPNS0_7IsolateENS0_6HandleINS0_6StringEEERKNS1_13ScriptDetailsENS_19ScriptOriginOptionsEPNS0_19ScriptStreamingDataE,"a",@progbits
	.align 4
	.align 4
.L1517:
	.long	.L1519-.L1517
	.long	.L1516-.L1517
	.long	.L1524-.L1517
	.long	.L1523-.L1517
	.long	.L1522-.L1517
	.long	.L1521-.L1517
	.long	.L1520-.L1517
	.long	.L1518-.L1517
	.long	.L1518-.L1517
	.long	.L1518-.L1517
	.long	.L1518-.L1517
	.long	.L1518-.L1517
	.long	.L1518-.L1517
	.long	.L1518-.L1517
	.long	.L1519-.L1517
	.long	.L1516-.L1517
	.long	.L1518-.L1517
	.long	.L1518-.L1517
	.long	.L1518-.L1517
	.long	.L1518-.L1517
	.long	.L1516-.L1517
	.section	.text._ZN2v88internal8Compiler38GetSharedFunctionInfoForStreamedScriptEPNS0_7IsolateENS0_6HandleINS0_6StringEEERKNS1_13ScriptDetailsENS_19ScriptOriginOptionsEPNS0_19ScriptStreamingDataE
	.p2align 4,,10
	.p2align 3
.L1518:
	movq	-160(%rbp), %rax
	movq	40960(%rax), %rax
	addq	$5160, %rax
	jmp	.L1525
	.p2align 4,,10
	.p2align 3
.L1509:
	movl	$15, %esi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	jmp	.L1516
.L1520:
	movq	-160(%rbp), %rax
	movq	40960(%rax), %rax
	addq	$5304, %rax
	jmp	.L1525
.L1521:
	movq	-160(%rbp), %rax
	movq	40960(%rax), %rax
	addq	$5256, %rax
	jmp	.L1525
.L1522:
	movq	-160(%rbp), %rax
	movq	40960(%rax), %rax
	addq	$5208, %rax
	jmp	.L1525
	.p2align 4,,10
	.p2align 3
.L1556:
	movl	$2, %esi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
.L1524:
	movq	-160(%rbp), %rax
	movq	40960(%rax), %rax
	addq	$5064, %rax
	jmp	.L1525
	.p2align 4,,10
	.p2align 3
.L1481:
	pushq	24(%rbx)
	movl	-176(%rbp), %ecx
	xorl	%r8d, %r8d
	movq	%r13, %rdx
	pushq	16(%rbx)
	movq	%r12, %rsi
	movq	%r15, %rdi
	pushq	8(%rbx)
	pushq	(%rbx)
	call	_ZN2v88internal12_GLOBAL__N_19NewScriptEPNS0_7IsolateEPNS0_9ParseInfoENS0_6HandleINS0_6StringEEENS0_8Compiler13ScriptDetailsENS_19ScriptOriginOptionsENS0_11NativesFlagE
	addq	$32, %rsp
	movq	%r15, %rsi
	movq	%rax, %rbx
	movq	-192(%rbp), %rax
	movq	%rbx, %rdx
	movq	8(%rax), %rdi
	call	_ZN2v88internal6Parser16UpdateStatisticsEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEE@PLT
	movq	-192(%rbp), %rax
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	8(%rax), %rdi
	call	_ZN2v88internal6Parser23HandleSourceURLCommentsEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEE@PLT
	cmpq	$0, 168(%r12)
	je	.L1527
	movq	-192(%rbp), %rax
	movq	16(%rax), %r8
	testq	%r8, %r8
	je	.L1527
	movq	112(%r12), %rdi
	movq	%r15, %rsi
	movq	%r8, -176(%rbp)
	call	_ZN2v88internal15AstValueFactory11InternalizeEPNS0_7IsolateE@PLT
	movq	80(%r12), %rdx
	movq	-176(%rbp), %r8
	movq	(%rdx), %rax
	movq	87(%rax), %rax
	movl	11(%rax), %eax
	testl	%eax, %eax
	jle	.L1559
.L1487:
	movq	168(%r12), %rsi
	movl	$1, %ecx
	movq	%r15, %rdi
	movq	%r8, -176(%rbp)
	call	_ZN2v88internal7Factory31NewSharedFunctionInfoForLiteralEPNS0_15FunctionLiteralENS0_6HandleINS0_6ScriptEEEb@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal16DeclarationScope18AllocateScopeInfosEPNS0_9ParseInfoEPNS0_7IsolateE@PLT
	movq	-176(%rbp), %r8
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal12_GLOBAL__N_133FinalizeUnoptimizedCompilationJobEPNS0_25UnoptimizedCompilationJobENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateE
	testl	%eax, %eax
	jne	.L1492
	movq	-192(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	addq	$24, %rdx
	call	_ZN2v88internal12_GLOBAL__N_123FinalizeUnoptimizedCodeEPNS0_9ParseInfoEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_25UnoptimizedCompilationJobEPSt12forward_listISt10unique_ptrIS9_St14default_deleteIS9_EESaISF_EE.part.0
	testb	%al, %al
	je	.L1492
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal12_GLOBAL__N_125FinalizeScriptCompilationEPNS0_7IsolateEPNS0_9ParseInfoE
	testq	%rbx, %rbx
	je	.L1560
	movq	12464(%r15), %rax
	movl	8(%r12), %r14d
	movq	39(%rax), %rsi
	movq	41112(%r15), %rdi
	shrl	$3, %r14d
	andl	$1, %r14d
	testq	%rdi, %rdi
	je	.L1500
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L1501:
	movq	-184(%rbp), %rdi
	movl	%r14d, %ecx
	movq	%rbx, %r8
	movq	%r13, %rsi
	movq	%rbx, %r14
	call	_ZN2v88internal16CompilationCache9PutScriptENS0_6HandleINS0_6StringEEENS2_INS0_7ContextEEENS0_12LanguageModeENS2_INS0_18SharedFunctionInfoEEE@PLT
	jmp	.L1482
	.p2align 4,,10
	.p2align 3
.L1527:
	movq	12480(%r15), %rax
	cmpq	%rax, 96(%r15)
	jne	.L1482
.L1553:
	cmpb	$0, 176(%r12)
	je	.L1499
	movq	112(%r12), %rcx
	movq	80(%r12), %rdx
	leaq	176(%r12), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal30PendingCompilationErrorHandler12ReportErrorsEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEEPNS0_15AstValueFactoryE@PLT
	jmp	.L1482
	.p2align 4,,10
	.p2align 3
.L1557:
	movq	%r15, %rdi
	movq	%rsi, -200(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-200(%rbp), %rsi
	jmp	.L1480
	.p2align 4,,10
	.p2align 3
.L1500:
	movq	41088(%r15), %rdx
	cmpq	41096(%r15), %rdx
	je	.L1561
.L1502:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rdx)
	jmp	.L1501
	.p2align 4,,10
	.p2align 3
.L1559:
	movl	72(%r12), %eax
	xorl	%edx, %edx
	movq	%r15, %rdi
	leal	1(%rax), %esi
	call	_ZN2v88internal7Factory17NewWeakFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	80(%r12), %rdx
	movq	(%rax), %rbx
	movq	(%rdx), %rdi
	testb	$1, %bl
	movq	%rbx, 87(%rdi)
	leaq	87(%rdi), %rsi
	movq	-176(%rbp), %r8
	je	.L1526
	movq	%rbx, %rax
	andq	$-262144, %rax
	movq	8(%rax), %rdx
	movq	%rax, -176(%rbp)
	testl	$262144, %edx
	je	.L1489
	movq	%rbx, %rdx
	movq	%r8, -216(%rbp)
	movq	%rsi, -208(%rbp)
	movq	%rdi, -200(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-176(%rbp), %rax
	movq	-216(%rbp), %r8
	movq	-208(%rbp), %rsi
	movq	-200(%rbp), %rdi
	movq	8(%rax), %rdx
.L1489:
	andl	$24, %edx
	je	.L1526
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1526
	movq	%rbx, %rdx
	movq	%r8, -176(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-176(%rbp), %r8
	.p2align 4,,10
	.p2align 3
.L1526:
	movq	80(%r12), %rdx
	jmp	.L1487
	.p2align 4,,10
	.p2align 3
.L1492:
	movq	12480(%r15), %rax
	cmpq	%rax, 96(%r15)
	jne	.L1482
	cmpb	$0, 176(%r12)
	je	.L1495
	movq	80(%r12), %rdx
	movq	112(%r12), %rcx
	leaq	176(%r12), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal30PendingCompilationErrorHandler12ReportErrorsEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEEPNS0_15AstValueFactoryE@PLT
	movq	96(%r15), %rdx
	movq	12480(%r15), %rax
.L1496:
	cmpq	%rax, %rdx
	jne	.L1482
	jmp	.L1553
	.p2align 4,,10
	.p2align 3
.L1558:
	movl	$20, %esi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	jmp	.L1516
	.p2align 4,,10
	.p2align 3
.L1499:
	movq	%r15, %rdi
	call	_ZN2v88internal7Isolate13StackOverflowEv@PLT
	jmp	.L1482
	.p2align 4,,10
	.p2align 3
.L1561:
	movq	%r15, %rdi
	movq	%rsi, -176(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-176(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L1502
.L1495:
	movq	%r15, %rdi
	call	_ZN2v88internal7Isolate13StackOverflowEv@PLT
	movq	96(%r15), %rdx
	movq	12480(%r15), %rax
	jmp	.L1496
.L1515:
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1555:
	call	__stack_chk_fail@PLT
.L1560:
	movq	96(%r15), %rdx
	movq	12480(%r15), %rax
	jmp	.L1496
	.cfi_endproc
.LFE25330:
	.size	_ZN2v88internal8Compiler38GetSharedFunctionInfoForStreamedScriptEPNS0_7IsolateENS0_6HandleINS0_6StringEEERKNS1_13ScriptDetailsENS_19ScriptOriginOptionsEPNS0_19ScriptStreamingDataE, .-_ZN2v88internal8Compiler38GetSharedFunctionInfoForStreamedScriptEPNS0_7IsolateENS0_6HandleINS0_6StringEEERKNS1_13ScriptDetailsENS_19ScriptOriginOptionsEPNS0_19ScriptStreamingDataE
	.section	.text._ZN2v88internal8Compiler21GetSharedFunctionInfoEPNS0_15FunctionLiteralENS0_6HandleINS0_6ScriptEEEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Compiler21GetSharedFunctionInfoEPNS0_15FunctionLiteralENS0_6HandleINS0_6ScriptEEEPNS0_7IsolateE
	.type	_ZN2v88internal8Compiler21GetSharedFunctionInfoEPNS0_15FunctionLiteralENS0_6HandleINS0_6ScriptEEEPNS0_7IsolateE, @function
_ZN2v88internal8Compiler21GetSharedFunctionInfoEPNS0_15FunctionLiteralENS0_6HandleINS0_6ScriptEEEPNS0_7IsolateE:
.LFB25331:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	leaq	-64(%rbp), %rdi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	movq	%r14, %rdx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	%r13, %rsi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6Script22FindSharedFunctionInfoEPNS0_7IsolateEPKNS0_15FunctionLiteralE@PLT
	testq	%rax, %rax
	je	.L1587
	cmpq	$0, 80(%r14)
	movq	%rax, %rbx
	je	.L1568
	movq	(%rax), %rax
	movq	7(%rax), %rax
	testb	$1, %al
	jne	.L1588
.L1568:
	movq	%rbx, %rax
.L1564:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1589
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1588:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$165, 11(%rax)
	jne	.L1568
	movq	(%rbx), %rax
	movq	7(%rax), %rsi
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1585
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r12
.L1569:
	movq	41112(%r13), %rdi
	movq	7(%rsi), %rsi
	testq	%rdi, %rdi
	je	.L1571
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L1572:
	movq	80(%r14), %rdi
	movq	%r13, %rsi
	movq	(%rdi), %rax
	call	*(%rax)
	movq	(%r12), %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %r8
	movl	19(%rdx), %ecx
	movl	15(%rdx), %edx
	call	_ZN2v88internal7Factory33NewUncompiledDataWithPreparseDataENS0_6HandleINS0_6StringEEEiiNS2_INS0_12PreparseDataEEE@PLT
	movq	(%rbx), %r13
	movq	(%rax), %r12
	leaq	7(%r13), %r15
	movq	%r12, 7(%r13)
	testb	$1, %r12b
	je	.L1568
	movq	%r12, %r14
	andq	$-262144, %r14
	movq	8(%r14), %rax
	testl	$262144, %eax
	jne	.L1590
.L1575:
	testb	$24, %al
	je	.L1568
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1568
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1568
	.p2align 4,,10
	.p2align 3
.L1587:
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal7Factory31NewSharedFunctionInfoForLiteralEPNS0_15FunctionLiteralENS0_6HandleINS0_6ScriptEEEb@PLT
	jmp	.L1564
	.p2align 4,,10
	.p2align 3
.L1571:
	movq	41088(%r13), %r15
	cmpq	41096(%r13), %r15
	je	.L1591
.L1573:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r15)
	jmp	.L1572
	.p2align 4,,10
	.p2align 3
.L1585:
	movq	41088(%r13), %r12
	cmpq	41096(%r13), %r12
	je	.L1592
.L1570:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r12)
	jmp	.L1569
	.p2align 4,,10
	.p2align 3
.L1590:
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r14), %rax
	jmp	.L1575
	.p2align 4,,10
	.p2align 3
.L1591:
	movq	%r13, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L1573
	.p2align 4,,10
	.p2align 3
.L1592:
	movq	%r13, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L1570
.L1589:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25331:
	.size	_ZN2v88internal8Compiler21GetSharedFunctionInfoEPNS0_15FunctionLiteralENS0_6HandleINS0_6ScriptEEEPNS0_7IsolateE, .-_ZN2v88internal8Compiler21GetSharedFunctionInfoEPNS0_15FunctionLiteralENS0_6HandleINS0_6ScriptEEEPNS0_7IsolateE
	.section	.rodata._ZN2v88internal12_GLOBAL__N_115CompileToplevelEPNS0_9ParseInfoEPNS0_7IsolateEPNS0_15IsCompiledScopeE.str1.1,"aMS",@progbits,1
.LC32:
	.string	"V8.CompileEval"
.LC33:
	.string	"V8.Compile"
.LC34:
	.string	"V8.CompileCode"
	.section	.text._ZN2v88internal12_GLOBAL__N_115CompileToplevelEPNS0_9ParseInfoEPNS0_7IsolateEPNS0_15IsCompiledScopeE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_115CompileToplevelEPNS0_9ParseInfoEPNS0_7IsolateEPNS0_15IsCompiledScopeE, @function
_ZN2v88internal12_GLOBAL__N_115CompileToplevelEPNS0_9ParseInfoEPNS0_7IsolateEPNS0_15IsCompiledScopeE:
.LFB25248:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$344, %rsp
	movq	%rdx, -344(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	41632(%rsi), %rax
	testq	%rax, %rax
	je	.L1595
	leaq	_ZN2v88internal6Logger26DefaultEventLoggerSentinelEPKci(%rip), %rdx
	cmpq	%rdx, %rax
	je	.L1762
	xorl	%esi, %esi
	leaq	.LC34(%rip), %rdi
	call	*%rax
.L1595:
	movq	_ZZN2v88internal12_GLOBAL__N_115CompileToplevelEPNS0_9ParseInfoEPNS0_7IsolateEPNS0_15IsCompiledScopeEE28trace_event_unique_atomic970(%rip), %r14
	testq	%r14, %r14
	je	.L1763
.L1599:
	movq	$0, -240(%rbp)
	movzbl	(%r14), %eax
	testb	$5, %al
	jne	.L1764
.L1601:
	xorl	%ecx, %ecx
	movl	$255, %edx
	leaq	-128(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal15InterruptsScopeC2EPNS0_7IsolateElNS1_4ModeE@PLT
	leaq	16+_ZTVN2v88internal23PostponeInterruptsScopeE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -144(%rbp)
	movq	%rax, -128(%rbp)
	movl	8(%r12), %eax
	movaps	%xmm0, -176(%rbp)
	andl	$4, %eax
	movaps	%xmm0, -160(%rbp)
	cmpl	$1, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	sbbl	%edx, %edx
	andl	$9, %edx
	subl	$-128, %edx
	testl	%eax, %eax
	jne	.L1765
.L1606:
	movl	12616(%rbx), %eax
	movl	$3, 12616(%rbx)
	cmpq	$0, 168(%r12)
	movl	%eax, -348(%rbp)
	je	.L1766
.L1607:
	movq	40960(%rbx), %r14
	leaq	2656(%r14), %rax
	addq	$2600, %r14
	testb	$4, 8(%r12)
	cmovne	%rax, %r14
	movq	32(%r14), %rax
	leaq	48(%r14), %rsi
	movq	%r14, %rdi
	movq	%rsi, -360(%rbp)
	movq	16(%rax), %rdx
	call	_ZN2v88internal14TimedHistogram5StartEPNS_4base12ElapsedTimerEPNS0_7IsolateE@PLT
	movq	_ZZN2v88internal12_GLOBAL__N_115CompileToplevelEPNS0_9ParseInfoEPNS0_7IsolateEPNS0_15IsCompiledScopeEE28trace_event_unique_atomic990(%rip), %rdx
	movq	%rdx, %r15
	testq	%rdx, %rdx
	je	.L1767
.L1612:
	movq	$0, -208(%rbp)
	movzbl	(%r15), %eax
	testb	$5, %al
	je	.L1614
	testb	$4, 8(%r12)
	leaq	.LC33(%rip), %rax
	pxor	%xmm0, %xmm0
	leaq	.LC32(%rip), %r13
	movaps	%xmm0, -80(%rbp)
	cmove	%rax, %r13
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rcx
	movq	$0, -312(%rbp)
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1768
.L1616:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1617
	movq	(%rdi), %rax
	call	*8(%rax)
.L1617:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1618
	movq	(%rdi), %rax
	call	*8(%rax)
.L1618:
	testb	$4, 8(%r12)
	leaq	.LC32(%rip), %rax
	leaq	.LC33(%rip), %rcx
	movq	%r15, -200(%rbp)
	cmove	%rcx, %rax
	movq	%rax, -192(%rbp)
	movq	-312(%rbp), %rax
	movq	%rax, -184(%rbp)
	leaq	-200(%rbp), %rax
	movq	%rax, -208(%rbp)
.L1614:
	movq	41136(%rbx), %rax
	movq	%rax, -328(%rbp)
	movq	80(%r12), %rax
	movq	(%rax), %rax
	movq	87(%rax), %rax
	movl	11(%rax), %eax
	testl	%eax, %eax
	jle	.L1769
.L1620:
	movq	112(%r12), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal15AstValueFactory11InternalizeEPNS0_7IsolateE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8Compiler7AnalyzeEPNS0_9ParseInfoE
	testb	%al, %al
	jne	.L1770
.L1625:
	movq	12480(%rbx), %rax
	cmpq	%rax, 96(%rbx)
	je	.L1771
.L1672:
	xorl	%r12d, %r12d
.L1674:
	leaq	-208(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	32(%r14), %rax
	movq	-360(%rbp), %rsi
	movq	%r14, %rdi
	movq	16(%rax), %rdx
	call	_ZN2v88internal14TimedHistogram4StopEPNS_4base12ElapsedTimerEPNS0_7IsolateE@PLT
.L1608:
	movl	-348(%rbp), %eax
	movq	-176(%rbp), %rdi
	movl	%eax, 12616(%rbx)
	testq	%rdi, %rdi
	jne	.L1772
.L1675:
	leaq	16+_ZTVN2v88internal15InterruptsScopeE(%rip), %rax
	cmpl	$2, -96(%rbp)
	movq	%rax, -128(%rbp)
	je	.L1676
	movq	-120(%rbp), %rdi
	call	_ZN2v88internal10StackGuard18PopInterruptsScopeEv@PLT
.L1676:
	leaq	-240(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	41632(%rbx), %rax
	testq	%rax, %rax
	je	.L1678
	leaq	_ZN2v88internal6Logger26DefaultEventLoggerSentinelEPKci(%rip), %rdx
	cmpq	%rdx, %rax
	je	.L1773
	movl	$1, %esi
	leaq	.LC34(%rip), %rdi
	call	*%rax
.L1678:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1774
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1769:
	.cfi_restore_state
	movl	72(%r12), %eax
	xorl	%edx, %edx
	movq	%rbx, %rdi
	leal	1(%rax), %esi
	call	_ZN2v88internal7Factory17NewWeakFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	80(%r12), %rdx
	movq	(%rax), %r13
	movq	(%rdx), %r15
	movq	%r13, 87(%r15)
	leaq	87(%r15), %rsi
	testb	$1, %r13b
	je	.L1620
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -312(%rbp)
	testl	$262144, %eax
	jne	.L1775
	testb	$24, %al
	je	.L1620
.L1791:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1620
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1620
	.p2align 4,,10
	.p2align 3
.L1764:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1776
.L1602:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1603
	movq	(%rdi), %rax
	call	*8(%rax)
.L1603:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1604
	movq	(%rdi), %rax
	call	*8(%rax)
.L1604:
	leaq	.LC34(%rip), %rax
	movq	%r14, -232(%rbp)
	movq	%rax, -224(%rbp)
	leaq	-232(%rbp), %rax
	movq	%r13, -216(%rbp)
	movq	%rax, -240(%rbp)
	jmp	.L1601
	.p2align 4,,10
	.p2align 3
.L1763:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r14
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1777
.L1600:
	movq	%r14, _ZZN2v88internal12_GLOBAL__N_115CompileToplevelEPNS0_9ParseInfoEPNS0_7IsolateEPNS0_15IsCompiledScopeEE28trace_event_unique_atomic970(%rip)
	jmp	.L1599
	.p2align 4,,10
	.p2align 3
.L1767:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r15
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1778
.L1613:
	movq	%r15, _ZZN2v88internal12_GLOBAL__N_115CompileToplevelEPNS0_9ParseInfoEPNS0_7IsolateEPNS0_15IsCompiledScopeEE28trace_event_unique_atomic990(%rip)
	jmp	.L1612
	.p2align 4,,10
	.p2align 3
.L1770:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal16DeclarationScope18AllocateScopeInfosEPNS0_9ParseInfoEPNS0_7IsolateE@PLT
	movq	80(%r12), %rax
	movl	$1, %ecx
	movq	%rbx, %rdi
	movq	168(%r12), %rsi
	movq	%rax, %rdx
	movq	%rax, -368(%rbp)
	call	_ZN2v88internal7Factory31NewSharedFunctionInfoForLiteralEPNS0_15FunctionLiteralENS0_6HandleINS0_6ScriptEEEb@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	168(%r12), %r15
	movq	%rax, -312(%rbp)
	movq	$0, -256(%rbp)
	movaps	%xmm0, -272(%rbp)
	call	_Znwm@PLT
	movq	%r14, -384(%rbp)
	movq	%r15, %r14
	leaq	8(%rax), %rdx
	movq	%r15, (%rax)
	movq	-320(%rbp), %r15
	movq	%rax, -272(%rbp)
	movq	%rdx, -256(%rbp)
	jmp	.L1626
	.p2align 4,,10
	.p2align 3
.L1627:
	testb	$1, %al
	jne	.L1779
.L1632:
	movq	-264(%rbp), %rdx
	cmpq	-272(%rbp), %rdx
	je	.L1780
.L1669:
	movq	-8(%rdx), %r14
.L1626:
	subq	$8, %rdx
	movq	-368(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rdx, -264(%rbp)
	movq	%rbx, %rdx
	call	_ZN2v88internal8Compiler21GetSharedFunctionInfoEPNS0_15FunctionLiteralENS0_6HandleINS0_6ScriptEEEPNS0_7IsolateE
	movabsq	$287762808832, %rcx
	movq	%rax, %r13
	movq	(%rax), %rax
	movq	7(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1627
.L1631:
	testb	$16, 9(%r12)
	jne	.L1759
	cmpb	$1, _ZN2v88internal17FLAG_validate_asmE(%rip)
	jne	.L1759
	cmpb	$0, _ZN2v88internal24FLAG_stress_validate_asmE(%rip)
	jne	.L1637
	movq	40(%r14), %rdi
	call	_ZNK2v88internal5Scope11IsAsmModuleEv@PLT
	testb	%al, %al
	je	.L1759
.L1637:
	leaq	-280(%rbp), %r10
	movq	-328(%rbp), %rcx
	movq	%r12, %rsi
	movq	%r14, %rdx
	movq	%r10, %rdi
	movq	%r10, -376(%rbp)
	call	_ZN2v88internal5AsmJs17NewCompilationJobEPNS0_9ParseInfoEPNS0_15FunctionLiteralEPNS0_19AccountingAllocatorE@PLT
	movq	-280(%rbp), %rdx
	movq	%rdx, -336(%rbp)
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	-336(%rbp), %rdx
	movq	%rax, -320(%rbp)
	movq	(%rdx), %rax
	movq	%rdx, %rdi
	call	*16(%rax)
	movq	-336(%rbp), %rdx
	movq	-376(%rbp), %r10
	testl	%eax, %eax
	jne	.L1781
	movl	$2, 8(%rdx)
	movq	%r10, -376(%rbp)
	movq	%rdx, -336(%rbp)
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	-336(%rbp), %rdx
	subq	-320(%rbp), %rax
	movq	%r13, %rsi
	addq	%rax, 48(%rdx)
	movq	-280(%rbp), %rdi
	movq	%rbx, %rdx
	call	_ZN2v88internal12_GLOBAL__N_133FinalizeUnoptimizedCompilationJobEPNS0_25UnoptimizedCompilationJobENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateE
	movq	-376(%rbp), %r10
	testl	%eax, %eax
	je	.L1652
.L1639:
	movq	-280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1633
	movq	(%rdi), %rax
	movq	%r10, -320(%rbp)
	call	*8(%rax)
	movq	-320(%rbp), %r10
.L1633:
	movq	-328(%rbp), %rcx
	movq	%r14, %rdx
	movq	%r10, %rdi
	movq	%r12, %rsi
	leaq	-272(%rbp), %r8
	call	_ZN2v88internal11interpreter11Interpreter17NewCompilationJobEPNS0_9ParseInfoEPNS0_15FunctionLiteralEPNS0_19AccountingAllocatorEPSt6vectorIS6_SaIS6_EE@PLT
	movq	-280(%rbp), %r14
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	%rax, -320(%rbp)
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*16(%rax)
	testl	%eax, %eax
	jne	.L1643
	movl	$2, 8(%r14)
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	subq	-320(%rbp), %rax
	addq	%rax, 48(%r14)
.L1644:
	movq	-280(%rbp), %rdi
	movq	%rbx, %rdx
	movq	%r13, %rsi
	call	_ZN2v88internal12_GLOBAL__N_133FinalizeUnoptimizedCompilationJobEPNS0_25UnoptimizedCompilationJobENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateE
	cmpl	$1, %eax
	je	.L1645
	cmpb	$0, _ZN2v88internal33FLAG_stress_lazy_source_positionsE(%rip)
	jne	.L1782
	movq	-312(%rbp), %rax
	cmpq	-312(%rbp), %r13
	je	.L1783
.L1650:
	testq	%rax, %rax
	je	.L1652
	movq	(%rax), %rax
	cmpq	%rax, 0(%r13)
	jne	.L1652
.L1651:
	movq	%rax, %rdx
	leaq	7(%rax), %r13
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	movq	7(%rax), %rcx
	testb	$1, %cl
	jne	.L1653
.L1656:
	movq	0(%r13), %rcx
	testb	$1, %cl
	jne	.L1784
.L1654:
	xorl	%eax, %eax
.L1665:
	movabsq	$287762808832, %rcx
	movq	0(%r13), %rdx
	cmpq	%rcx, %rdx
	je	.L1668
	movl	$1, %ecx
	testb	$1, %dl
	jne	.L1785
.L1667:
	movq	-344(%rbp), %rsi
	movb	%cl, %r15b
	movq	%rax, -304(%rbp)
	movq	%r15, -296(%rbp)
	movq	%rax, (%rsi)
	movb	%r15b, 8(%rsi)
.L1652:
	movq	-280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1632
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	-264(%rbp), %rdx
	cmpq	-272(%rbp), %rdx
	jne	.L1669
.L1780:
	movq	%r12, %rdi
	movq	-384(%rbp), %r14
	call	_ZN2v88internal9ParseInfo20ResetCharacterStreamEv@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1786
.L1670:
	cmpq	$0, -312(%rbp)
	je	.L1625
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal12_GLOBAL__N_125FinalizeScriptCompilationEPNS0_7IsolateEPNS0_9ParseInfoE
	movq	-312(%rbp), %r12
	jmp	.L1674
	.p2align 4,,10
	.p2align 3
.L1784:
	movq	-1(%rcx), %rcx
	cmpw	$91, 11(%rcx)
	jne	.L1654
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L1787
.L1657:
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L1788
.L1661:
	movq	0(%r13), %rax
	movq	7(%rax), %rsi
.L1660:
	movq	3520(%rdx), %rdi
	leaq	-37592(%rdx), %r14
	testq	%rdi, %rdi
	je	.L1662
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	jmp	.L1665
	.p2align 4,,10
	.p2align 3
.L1759:
	leaq	-280(%rbp), %r10
	jmp	.L1633
	.p2align 4,,10
	.p2align 3
.L1643:
	movl	$4, 8(%r14)
	movl	%eax, -336(%rbp)
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movl	-336(%rbp), %ecx
	subq	-320(%rbp), %rax
	addq	%rax, 48(%r14)
	cmpl	$1, %ecx
	jne	.L1644
.L1645:
	movq	-280(%rbp), %rdi
	movq	-384(%rbp), %r14
	testq	%rdi, %rdi
	je	.L1648
	movq	(%rdi), %rax
	call	*8(%rax)
.L1648:
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1625
	call	_ZdlPv@PLT
	jmp	.L1625
	.p2align 4,,10
	.p2align 3
.L1779:
	movq	-1(%rax), %rdx
	cmpw	$165, 11(%rdx)
	je	.L1631
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L1632
	jmp	.L1631
	.p2align 4,,10
	.p2align 3
.L1782:
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal18SharedFunctionInfo30EnsureSourcePositionsAvailableEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	-312(%rbp), %rax
	cmpq	-312(%rbp), %r13
	jne	.L1650
.L1783:
	movq	(%rax), %rax
	jmp	.L1651
	.p2align 4,,10
	.p2align 3
.L1785:
	movq	-1(%rdx), %rcx
	cmpw	$165, 11(%rcx)
	jne	.L1789
.L1668:
	xorl	%ecx, %ecx
	jmp	.L1667
	.p2align 4,,10
	.p2align 3
.L1781:
	movl	$4, 8(%rdx)
	movq	%r10, -376(%rbp)
	movq	%rdx, -336(%rbp)
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	-336(%rbp), %rdx
	subq	-320(%rbp), %rax
	movq	-376(%rbp), %r10
	addq	%rax, 48(%rdx)
	jmp	.L1639
	.p2align 4,,10
	.p2align 3
.L1653:
	movq	-1(%rcx), %rcx
	cmpw	$72, 11(%rcx)
	jne	.L1656
	movq	31(%rax), %rax
	testb	$1, %al
	je	.L1657
.L1787:
	movq	-1(%rax), %rcx
	cmpw	$86, 11(%rcx)
	jne	.L1657
	movq	39(%rax), %rcx
	testb	$1, %cl
	je	.L1657
	movq	-1(%rcx), %rcx
	cmpw	$72, 11(%rcx)
	jne	.L1657
	movq	31(%rax), %rsi
	jmp	.L1660
	.p2align 4,,10
	.p2align 3
.L1662:
	movq	41088(%r14), %rax
	cmpq	41096(%r14), %rax
	je	.L1790
.L1664:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r14)
	movq	%rsi, (%rax)
	jmp	.L1665
	.p2align 4,,10
	.p2align 3
.L1789:
	movq	-1(%rdx), %rdx
	cmpw	$166, 11(%rdx)
	setne	%cl
	jmp	.L1667
	.p2align 4,,10
	.p2align 3
.L1775:
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rsi, -336(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-312(%rbp), %rcx
	movq	-336(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L1791
	jmp	.L1620
	.p2align 4,,10
	.p2align 3
.L1771:
	cmpb	$0, 176(%r12)
	je	.L1673
	movq	112(%r12), %rcx
	movq	80(%r12), %rdx
	leaq	176(%r12), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal30PendingCompilationErrorHandler12ReportErrorsEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEEPNS0_15AstValueFactoryE@PLT
	jmp	.L1672
	.p2align 4,,10
	.p2align 3
.L1766:
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7parsing12ParseProgramEPNS0_9ParseInfoEPNS0_7IsolateENS1_29ReportErrorsAndStatisticsModeE@PLT
	testb	%al, %al
	jne	.L1607
	xorl	%r12d, %r12d
	jmp	.L1608
	.p2align 4,,10
	.p2align 3
.L1773:
	movq	41016(%rbx), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	je	.L1678
	leaq	.LC34(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal6Logger10TimerEventENS1_8StartEndEPKc@PLT
	jmp	.L1678
	.p2align 4,,10
	.p2align 3
.L1762:
	movq	41016(%rsi), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	je	.L1595
	leaq	.LC34(%rip), %rdx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal6Logger10TimerEventENS1_8StartEndEPKc@PLT
	jmp	.L1595
	.p2align 4,,10
	.p2align 3
.L1772:
	leaq	-168(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1675
	.p2align 4,,10
	.p2align 3
.L1765:
	movq	40960(%rbx), %rax
	leaq	-168(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -176(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1606
	.p2align 4,,10
	.p2align 3
.L1768:
	subq	$8, %rsp
	leaq	-80(%rbp), %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	movq	%r15, %rdx
	movl	$88, %esi
	pushq	%rcx
	movq	%r13, %rcx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, -312(%rbp)
	addq	$64, %rsp
	jmp	.L1616
	.p2align 4,,10
	.p2align 3
.L1788:
	movq	-1(%rax), %rax
	cmpw	$72, 11(%rax)
	jne	.L1661
	movq	0(%r13), %rsi
	jmp	.L1660
	.p2align 4,,10
	.p2align 3
.L1777:
	leaq	.LC6(%rip), %rsi
	call	*%rax
	movq	%rax, %r14
	jmp	.L1600
	.p2align 4,,10
	.p2align 3
.L1776:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC34(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r14, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L1602
	.p2align 4,,10
	.p2align 3
.L1778:
	leaq	.LC6(%rip), %rsi
	call	*%rax
	movq	%rax, %r15
	jmp	.L1613
	.p2align 4,,10
	.p2align 3
.L1673:
	movq	%rbx, %rdi
	call	_ZN2v88internal7Isolate13StackOverflowEv@PLT
	jmp	.L1672
	.p2align 4,,10
	.p2align 3
.L1790:
	movq	%r14, %rdi
	movq	%rsi, -320(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-320(%rbp), %rsi
	jmp	.L1664
.L1774:
	call	__stack_chk_fail@PLT
.L1786:
	call	_ZdlPv@PLT
	jmp	.L1670
	.cfi_endproc
.LFE25248:
	.size	_ZN2v88internal12_GLOBAL__N_115CompileToplevelEPNS0_9ParseInfoEPNS0_7IsolateEPNS0_15IsCompiledScopeE, .-_ZN2v88internal12_GLOBAL__N_115CompileToplevelEPNS0_9ParseInfoEPNS0_7IsolateEPNS0_15IsCompiledScopeE
	.section	.text._ZN2v88internal8Compiler18CompileForLiveEditEPNS0_9ParseInfoEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Compiler18CompileForLiveEditEPNS0_9ParseInfoEPNS0_7IsolateE
	.type	_ZN2v88internal8Compiler18CompileForLiveEditEPNS0_9ParseInfoEPNS0_7IsolateE, @function
_ZN2v88internal8Compiler18CompileForLiveEditEPNS0_9ParseInfoEPNS0_7IsolateE:
.LFB25286:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-32(%rbp), %rdx
	movq	$0, -32(%rbp)
	movb	$0, -24(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_115CompileToplevelEPNS0_9ParseInfoEPNS0_7IsolateEPNS0_15IsCompiledScopeE
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1795
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1795:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25286:
	.size	_ZN2v88internal8Compiler18CompileForLiveEditEPNS0_9ParseInfoEPNS0_7IsolateE, .-_ZN2v88internal8Compiler18CompileForLiveEditEPNS0_9ParseInfoEPNS0_7IsolateE
	.section	.rodata._ZN2v88internal8Compiler30GetSharedFunctionInfoForScriptEPNS0_7IsolateENS0_6HandleINS0_6StringEEERKNS1_13ScriptDetailsENS_19ScriptOriginOptionsEPNS_9ExtensionEPNS0_10ScriptDataENS_14ScriptCompiler14CompileOptionsENSF_13NoCacheReasonENS0_11NativesFlagE.str1.1,"aMS",@progbits,1
.LC35:
	.string	"V8.CompileDeserialize"
	.section	.text._ZN2v88internal8Compiler30GetSharedFunctionInfoForScriptEPNS0_7IsolateENS0_6HandleINS0_6StringEEERKNS1_13ScriptDetailsENS_19ScriptOriginOptionsEPNS_9ExtensionEPNS0_10ScriptDataENS_14ScriptCompiler14CompileOptionsENSF_13NoCacheReasonENS0_11NativesFlagE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Compiler30GetSharedFunctionInfoForScriptEPNS0_7IsolateENS0_6HandleINS0_6StringEEERKNS1_13ScriptDetailsENS_19ScriptOriginOptionsEPNS_9ExtensionEPNS0_10ScriptDataENS_14ScriptCompiler14CompileOptionsENSF_13NoCacheReasonENS0_11NativesFlagE
	.type	_ZN2v88internal8Compiler30GetSharedFunctionInfoForScriptEPNS0_7IsolateENS0_6HandleINS0_6StringEEERKNS1_13ScriptDetailsENS_19ScriptOriginOptionsEPNS_9ExtensionEPNS0_10ScriptDataENS_14ScriptCompiler14CompileOptionsENSF_13NoCacheReasonENS0_11NativesFlagE, @function
_ZN2v88internal8Compiler30GetSharedFunctionInfoForScriptEPNS0_7IsolateENS0_6HandleINS0_6StringEEERKNS1_13ScriptDetailsENS_19ScriptOriginOptionsEPNS_9ExtensionEPNS0_10ScriptDataENS_14ScriptCompiler14CompileOptionsENSF_13NoCacheReasonENS0_11NativesFlagE:
.LFB25328:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$456, %rsp
	movq	%r8, -424(%rbp)
	movq	%r9, -448(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -352(%rbp)
	movq	$0, -344(%rbp)
	movq	$0, -336(%rbp)
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	40960(%r14), %rsi
	movq	%rax, -344(%rbp)
	movq	2856(%rsi), %rax
	leaq	2824(%rsi), %rdi
	addq	$2872, %rsi
	movq	%rdi, -328(%rbp)
	movq	16(%rax), %rdx
	call	_ZN2v88internal14TimedHistogram5StartEPNS_4base12ElapsedTimerEPNS0_7IsolateE@PLT
	movl	24(%rbp), %eax
	movq	40960(%r14), %r15
	movl	$0, -316(%rbp)
	movl	%eax, -320(%rbp)
	movq	(%r12), %rax
	cmpb	$0, 6432(%r15)
	movl	11(%rax), %edx
	je	.L1797
	movq	6424(%r15), %rax
.L1798:
	testq	%rax, %rax
	je	.L1799
	addl	%edx, (%rax)
.L1799:
	movq	40960(%r14), %r15
	cmpb	$0, 6528(%r15)
	je	.L1800
	movq	6520(%r15), %rax
.L1801:
	testq	%rax, %rax
	je	.L1802
	addl	%edx, (%rax)
.L1802:
	movzbl	_ZN2v88internal15FLAG_use_strictE(%rip), %eax
	cmpq	$0, -424(%rbp)
	movq	$0, -400(%rbp)
	movb	$0, -392(%rbp)
	movb	%al, -428(%rbp)
	movq	40952(%r14), %rax
	movq	%rax, -440(%rbp)
	je	.L1924
.L1803:
	leaq	-304(%rbp), %r15
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal9ParseInfoC1EPNS0_7IsolateE@PLT
	pushq	24(%rbx)
	movl	32(%rbp), %r8d
	movl	%r13d, %ecx
	pushq	16(%rbx)
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	pushq	8(%rbx)
	pushq	(%rbx)
	call	_ZN2v88internal12_GLOBAL__N_19NewScriptEPNS0_7IsolateEPNS0_9ParseInfoENS0_6HandleINS0_6StringEEENS0_8Compiler13ScriptDetailsENS_19ScriptOriginOptionsENS0_11NativesFlagE
	addq	$32, %rsp
	andl	$8, %r13d
	je	.L1925
.L1849:
	movl	-296(%rbp), %eax
	orl	$64, %eax
.L1850:
	movq	-424(%rbp), %rbx
	cmpl	$2, 16(%rbp)
	movq	%rbx, -288(%rbp)
	jne	.L1851
	orl	$2, %eax
.L1852:
	movl	%eax, %edx
	movl	%eax, %ecx
	andl	$-9, %eax
	movq	%r14, %rsi
	shrl	$3, %edx
	orl	$8, %ecx
	movq	%r15, %rdi
	andl	$1, %edx
	orb	-428(%rbp), %dl
	leaq	-400(%rbp), %rdx
	cmovne	%ecx, %eax
	movl	%eax, -296(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_115CompileToplevelEPNS0_9ParseInfoEPNS0_7IsolateEPNS0_15IsCompiledScopeE
	cmpq	$0, -424(%rbp)
	movq	%rax, %r8
	je	.L1926
	testq	%rax, %rax
	je	.L1856
.L1860:
	movq	%r15, %rdi
	movq	%r8, -424(%rbp)
	call	_ZN2v88internal9ParseInfoD1Ev@PLT
	movzbl	-316(%rbp), %eax
	movq	-424(%rbp), %r8
.L1882:
	cmpb	$0, -315(%rbp)
	movq	%r8, %r12
	je	.L1861
	movq	-352(%rbp), %rdx
	movq	40960(%rdx), %rdi
	addq	$1728, %rdi
	testb	%al, %al
	jne	.L1810
	xorl	%esi, %esi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
.L1874:
	movq	-352(%rbp), %rax
	movq	40960(%rax), %rdx
	addq	$4968, %rdx
	.p2align 4,,10
	.p2align 3
.L1880:
	movq	-328(%rbp), %rdi
	movq	%rdx, -336(%rbp)
	movq	32(%rdi), %rax
	leaq	48(%rdi), %rsi
	movq	16(%rax), %rdx
	call	_ZN2v88internal14TimedHistogram4StopEPNS_4base12ElapsedTimerEPNS0_7IsolateE@PLT
	movq	-336(%rbp), %rdi
	xorl	%edx, %edx
	leaq	-344(%rbp), %rsi
	call	_ZN2v88internal14TimedHistogram4StopEPNS_4base12ElapsedTimerEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1927
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1797:
	.cfi_restore_state
	movb	$1, 6432(%r15)
	leaq	6408(%r15), %rdi
	movl	%edx, -428(%rbp)
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movl	-428(%rbp), %edx
	movq	%rax, 6424(%r15)
	jmp	.L1798
	.p2align 4,,10
	.p2align 3
.L1800:
	movb	$1, 6528(%r15)
	leaq	6504(%r15), %rdi
	movl	%edx, -428(%rbp)
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movl	-428(%rbp), %edx
	movq	%rax, 6520(%r15)
	jmp	.L1801
	.p2align 4,,10
	.p2align 3
.L1924:
	cmpl	$1, 16(%rbp)
	je	.L1928
.L1804:
	movq	12464(%r14), %rax
	movq	39(%rax), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1805
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L1806:
	movzbl	-428(%rbp), %ecx
	movq	8(%rbx), %rdx
	movl	%r13d, %r9d
	movq	%r12, %rsi
	movq	-440(%rbp), %rdi
	pushq	%rcx
	pushq	%rax
	movl	4(%rbx), %r8d
	movl	(%rbx), %ecx
	call	_ZN2v88internal16CompilationCache12LookupScriptENS0_6HandleINS0_6StringEEENS0_11MaybeHandleINS0_6ObjectEEEiiNS_19ScriptOriginOptionsENS2_INS0_7ContextEEENS0_12LanguageModeE@PLT
	movq	%rax, %r15
	popq	%rax
	popq	%rdx
	testq	%r15, %r15
	je	.L1808
	cmpb	$0, -315(%rbp)
	movb	$1, -316(%rbp)
	movq	%r15, %r12
	je	.L1809
	movq	-352(%rbp), %rax
	movq	40960(%rax), %rdi
	addq	$1728, %rdi
.L1810:
	movl	$14, %esi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	jmp	.L1874
	.p2align 4,,10
	.p2align 3
.L1925:
	movl	-296(%rbp), %eax
	jmp	.L1850
	.p2align 4,,10
	.p2align 3
.L1805:
	movq	41088(%r14), %rax
	cmpq	41096(%r14), %rax
	je	.L1929
.L1807:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r14)
	movq	%rsi, (%rax)
	jmp	.L1806
	.p2align 4,,10
	.p2align 3
.L1928:
	movb	$1, -314(%rbp)
	jmp	.L1804
	.p2align 4,,10
	.p2align 3
.L1861:
	cmpb	$0, -314(%rbp)
	je	.L1863
	movq	-352(%rbp), %rdx
	movq	40960(%rdx), %rdi
	addq	$1728, %rdi
	testb	%al, %al
	jne	.L1864
	cmpb	$0, -313(%rbp)
	je	.L1930
	movl	$3, %esi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
.L1878:
	movq	-352(%rbp), %rax
	movq	40960(%rax), %rdx
	addq	$5112, %rdx
	jmp	.L1880
	.p2align 4,,10
	.p2align 3
.L1863:
	movl	-320(%rbp), %edx
	testb	%al, %al
	jne	.L1884
	cmpl	$14, %edx
	ja	.L1870
	leaq	CSWTCH.453(%rip), %rax
	movl	(%rax,%rdx,4), %ebx
	movq	-352(%rbp), %rax
	movq	40960(%rax), %rdi
	movl	%ebx, %esi
	addq	$1728, %rdi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	cmpl	$20, %ebx
	ja	.L1870
	leaq	.L1872(%rip), %rdx
	movslq	(%rdx,%rbx,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8Compiler30GetSharedFunctionInfoForScriptEPNS0_7IsolateENS0_6HandleINS0_6StringEEERKNS1_13ScriptDetailsENS_19ScriptOriginOptionsEPNS_9ExtensionEPNS0_10ScriptDataENS_14ScriptCompiler14CompileOptionsENSF_13NoCacheReasonENS0_11NativesFlagE,"a",@progbits
	.align 4
	.align 4
.L1872:
	.long	.L1874-.L1872
	.long	.L1871-.L1872
	.long	.L1879-.L1872
	.long	.L1878-.L1872
	.long	.L1877-.L1872
	.long	.L1876-.L1872
	.long	.L1875-.L1872
	.long	.L1873-.L1872
	.long	.L1873-.L1872
	.long	.L1873-.L1872
	.long	.L1873-.L1872
	.long	.L1873-.L1872
	.long	.L1873-.L1872
	.long	.L1873-.L1872
	.long	.L1874-.L1872
	.long	.L1871-.L1872
	.long	.L1873-.L1872
	.long	.L1873-.L1872
	.long	.L1873-.L1872
	.long	.L1873-.L1872
	.long	.L1871-.L1872
	.section	.text._ZN2v88internal8Compiler30GetSharedFunctionInfoForScriptEPNS0_7IsolateENS0_6HandleINS0_6StringEEERKNS1_13ScriptDetailsENS_19ScriptOriginOptionsEPNS_9ExtensionEPNS0_10ScriptDataENS_14ScriptCompiler14CompileOptionsENSF_13NoCacheReasonENS0_11NativesFlagE
	.p2align 4,,10
	.p2align 3
.L1873:
	movq	-352(%rbp), %rax
	movq	40960(%rax), %rdx
	addq	$5160, %rdx
	jmp	.L1880
	.p2align 4,,10
	.p2align 3
.L1809:
	cmpb	$0, -314(%rbp)
	jne	.L1883
	movl	-320(%rbp), %edx
.L1884:
	movq	-352(%rbp), %rax
	movq	40960(%rax), %rdi
	addq	$1728, %rdi
	cmpl	$5, %edx
	je	.L1931
	movl	$1, %esi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
.L1871:
	movq	-352(%rbp), %rax
	movq	40960(%rax), %rdx
	addq	$5016, %rdx
	jmp	.L1880
	.p2align 4,,10
	.p2align 3
.L1930:
	movl	$2, %esi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
.L1879:
	movq	-352(%rbp), %rax
	movq	40960(%rax), %rdx
	addq	$5064, %rdx
	jmp	.L1880
.L1876:
	movq	-352(%rbp), %rax
	movq	40960(%rax), %rdx
	addq	$5256, %rdx
	jmp	.L1880
.L1877:
	movq	-352(%rbp), %rax
	movq	40960(%rax), %rdx
	addq	$5208, %rdx
	jmp	.L1880
.L1875:
	movq	-352(%rbp), %rax
	movq	40960(%rax), %rdx
	addq	$5304, %rdx
	jmp	.L1880
	.p2align 4,,10
	.p2align 3
.L1808:
	cmpl	$1, 16(%rbp)
	jne	.L1803
	movq	40960(%r14), %rax
	movb	$1, -314(%rbp)
	leaq	2768(%rax), %rdi
	leaq	2816(%rax), %rsi
	movq	%rax, -472(%rbp)
	movq	2800(%rax), %rax
	movq	%rdi, -456(%rbp)
	movq	16(%rax), %rdx
	movq	%rsi, -464(%rbp)
	call	_ZN2v88internal14TimedHistogram5StartEPNS_4base12ElapsedTimerEPNS0_7IsolateE@PLT
	pxor	%xmm0, %xmm0
	movq	$0, -272(%rbp)
	movaps	%xmm0, -304(%rbp)
	movaps	%xmm0, -288(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1932
.L1811:
	movq	_ZZN2v88internal8Compiler30GetSharedFunctionInfoForScriptEPNS0_7IsolateENS0_6HandleINS0_6StringEEERKNS1_13ScriptDetailsENS_19ScriptOriginOptionsEPNS_9ExtensionEPNS0_10ScriptDataENS_14ScriptCompiler14CompileOptionsENSF_13NoCacheReasonENS0_11NativesFlagEE29trace_event_unique_atomic2021(%rip), %rdx
	testq	%rdx, %rdx
	je	.L1933
.L1813:
	movq	$0, -384(%rbp)
	movzbl	(%rdx), %eax
	testb	$5, %al
	jne	.L1934
.L1815:
	movq	-448(%rbp), %rsi
	movl	%r13d, %ecx
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal14CodeSerializer11DeserializeEPNS0_7IsolateEPNS0_10ScriptDataENS0_6HandleINS0_6StringEEENS_19ScriptOriginOptionsE@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L1822
	movabsq	$287762808832, %rdx
	movq	(%rax), %rax
	movq	7(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1822
	testb	$1, %al
	jne	.L1935
.L1823:
	movq	(%r8), %rbx
	movq	%rbx, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rdx
	movq	7(%rbx), %rax
	testb	$1, %al
	jne	.L1824
.L1827:
	movq	7(%rbx), %rax
	testb	$1, %al
	jne	.L1936
.L1836:
	movabsq	$287762808832, %rdx
	movq	7(%rbx), %rax
	cmpq	%rdx, %rax
	je	.L1839
	movl	$1, %edx
	testb	$1, %al
	jne	.L1937
.L1838:
	xorl	%eax, %eax
	movq	%r15, -416(%rbp)
	movb	%dl, %al
	movq	%r15, -400(%rbp)
	movq	%rax, -408(%rbp)
	movb	%al, -392(%rbp)
	movq	12464(%r14), %rax
	movq	39(%rax), %r13
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1840
	movq	%r13, %rsi
	movq	%r8, -424(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-424(%rbp), %r8
	movq	%rax, %rdx
.L1841:
	movzbl	-428(%rbp), %ecx
	movq	-440(%rbp), %rdi
	movq	%r12, %rsi
	movq	%r8, -424(%rbp)
	call	_ZN2v88internal16CompilationCache9PutScriptENS0_6HandleINS0_6StringEEENS2_INS0_7ContextEEENS0_12LanguageModeENS2_INS0_18SharedFunctionInfoEEE@PLT
	movq	-424(%rbp), %r8
	movq	(%r8), %rax
	movq	31(%rax), %r12
	testb	$1, %r12b
	jne	.L1938
.L1843:
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1844
	movq	%r12, %rsi
	movq	%r8, -424(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-424(%rbp), %r8
.L1845:
	leaq	-384(%rbp), %rdi
	movq	%r8, -424(%rbp)
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-304(%rbp), %rdi
	movq	-424(%rbp), %r8
	testq	%rdi, %rdi
	jne	.L1939
	movq	%r8, -424(%rbp)
.L1922:
	movq	-472(%rbp), %rax
	movq	-464(%rbp), %rsi
	movq	-456(%rbp), %rdi
	movq	2800(%rax), %rax
	movq	16(%rax), %rdx
	call	_ZN2v88internal14TimedHistogram4StopEPNS_4base12ElapsedTimerEPNS0_7IsolateE@PLT
	movzbl	-316(%rbp), %eax
	movq	-424(%rbp), %r8
	jmp	.L1882
.L1935:
	movq	-1(%rax), %rdx
	cmpw	$165, 11(%rdx)
	je	.L1822
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L1823
	.p2align 4,,10
	.p2align 3
.L1822:
	leaq	-384(%rbp), %rdi
	movb	$1, -313(%rbp)
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-304(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1820
.L1923:
	movq	-472(%rbp), %rax
	movq	-464(%rbp), %rsi
	leaq	-304(%rbp), %r15
	movq	-456(%rbp), %rdi
	movq	2800(%rax), %rax
	movq	16(%rax), %rdx
	call	_ZN2v88internal14TimedHistogram4StopEPNS_4base12ElapsedTimerEPNS0_7IsolateE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal9ParseInfoC1EPNS0_7IsolateE@PLT
	pushq	24(%rbx)
	movl	32(%rbp), %r8d
	movl	%r13d, %ecx
	pushq	16(%rbx)
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	pushq	8(%rbx)
	pushq	(%rbx)
	call	_ZN2v88internal12_GLOBAL__N_19NewScriptEPNS0_7IsolateEPNS0_9ParseInfoENS0_6HandleINS0_6StringEEENS0_8Compiler13ScriptDetailsENS_19ScriptOriginOptionsENS0_11NativesFlagE
	addq	$32, %rsp
	andl	$8, %r13d
	jne	.L1849
	movq	$0, -288(%rbp)
	movl	-296(%rbp), %eax
	.p2align 4,,10
	.p2align 3
.L1851:
	andl	$-3, %eax
	jmp	.L1852
	.p2align 4,,10
	.p2align 3
.L1883:
	movq	-352(%rbp), %rax
	movq	40960(%rax), %rdi
	addq	$1728, %rdi
.L1864:
	movl	$15, %esi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	jmp	.L1871
	.p2align 4,,10
	.p2align 3
.L1926:
	testq	%rax, %rax
	je	.L1856
	movq	12464(%r14), %rax
	movq	39(%rax), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1857
	movq	%r8, -424(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-424(%rbp), %r8
	movq	%rax, %rdx
.L1858:
	movzbl	-428(%rbp), %ecx
	movq	-440(%rbp), %rdi
	movq	%r12, %rsi
	movq	%r8, -424(%rbp)
	call	_ZN2v88internal16CompilationCache9PutScriptENS0_6HandleINS0_6StringEEENS2_INS0_7ContextEEENS0_12LanguageModeENS2_INS0_18SharedFunctionInfoEEE@PLT
	movq	-424(%rbp), %r8
	jmp	.L1860
	.p2align 4,,10
	.p2align 3
.L1856:
	cmpl	$1, 32(%rbp)
	je	.L1860
	movq	%r14, %rdi
	movq	%r8, -424(%rbp)
	call	_ZN2v88internal7Isolate21ReportPendingMessagesEv@PLT
	movq	-424(%rbp), %r8
	jmp	.L1860
	.p2align 4,,10
	.p2align 3
.L1931:
	movl	$20, %esi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	jmp	.L1871
	.p2align 4,,10
	.p2align 3
.L1929:
	movq	%r14, %rdi
	movq	%rsi, -456(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-456(%rbp), %rsi
	jmp	.L1807
	.p2align 4,,10
	.p2align 3
.L1857:
	movq	41088(%r14), %rdx
	cmpq	41096(%r14), %rdx
	je	.L1940
.L1859:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%rdx)
	jmp	.L1858
.L1939:
	leaq	-296(%rbp), %rsi
	movq	%r8, -424(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1922
.L1933:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1941
.L1814:
	movq	%rdx, _ZZN2v88internal8Compiler30GetSharedFunctionInfoForScriptEPNS0_7IsolateENS0_6HandleINS0_6StringEEERKNS1_13ScriptDetailsENS_19ScriptOriginOptionsEPNS_9ExtensionEPNS0_10ScriptDataENS_14ScriptCompiler14CompileOptionsENSF_13NoCacheReasonENS0_11NativesFlagEE29trace_event_unique_atomic2021(%rip)
	jmp	.L1813
.L1934:
	pxor	%xmm0, %xmm0
	movq	%rdx, -488(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rcx
	movq	$0, -480(%rbp)
	movq	-488(%rbp), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1942
.L1816:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1817
	movq	(%rdi), %rax
	movq	%rdx, -488(%rbp)
	call	*8(%rax)
	movq	-488(%rbp), %rdx
.L1817:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1818
	movq	(%rdi), %rax
	movq	%rdx, -488(%rbp)
	call	*8(%rax)
	movq	-488(%rbp), %rdx
.L1818:
	leaq	.LC35(%rip), %rax
	movq	%rdx, -376(%rbp)
	movq	%rax, -368(%rbp)
	movq	-480(%rbp), %rax
	movq	%rax, -360(%rbp)
	leaq	-376(%rbp), %rax
	movq	%rax, -384(%rbp)
	jmp	.L1815
.L1940:
	movq	%r14, %rdi
	movq	%rsi, -448(%rbp)
	movq	%r8, -424(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-448(%rbp), %rsi
	movq	-424(%rbp), %r8
	movq	%rax, %rdx
	jmp	.L1859
.L1844:
	movq	41088(%r14), %rax
	cmpq	41096(%r14), %rax
	je	.L1943
.L1846:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r14)
	movq	%r12, (%rax)
	jmp	.L1845
.L1840:
	movq	41088(%r14), %rdx
	cmpq	41096(%r14), %rdx
	je	.L1944
.L1842:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r14)
	movq	%r13, (%rdx)
	jmp	.L1841
.L1839:
	xorl	%edx, %edx
	jmp	.L1838
.L1932:
	movq	40960(%r14), %rax
	leaq	-296(%rbp), %rsi
	movl	$126, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -304(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1811
.L1824:
	movq	-1(%rax), %rax
	cmpw	$72, 11(%rax)
	jne	.L1827
.L1830:
	movq	31(%rbx), %rax
	testb	$1, %al
	jne	.L1945
.L1828:
	movq	7(%rbx), %rax
	testb	$1, %al
	jne	.L1946
.L1832:
	movq	7(%rbx), %rax
	movq	7(%rax), %rsi
.L1831:
	movq	3520(%rdx), %rdi
	leaq	-37592(%rdx), %r13
	testq	%rdi, %rdi
	je	.L1833
	movq	%r8, -424(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-424(%rbp), %r8
	movq	%rax, %r15
	jmp	.L1836
.L1938:
	movq	-1(%r12), %rax
	cmpw	$86, 11(%rax)
	jne	.L1843
	movq	23(%r12), %r12
	jmp	.L1843
.L1936:
	movq	-1(%rax), %rax
	cmpw	$91, 11(%rax)
	jne	.L1836
	jmp	.L1830
.L1942:
	subq	$8, %rsp
	leaq	-80(%rbp), %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	movl	$88, %esi
	pushq	%rcx
	leaq	.LC35(%rip), %rcx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	-488(%rbp), %rdx
	movq	%rax, -480(%rbp)
	addq	$64, %rsp
	jmp	.L1816
.L1941:
	leaq	.LC6(%rip), %rsi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L1814
.L1937:
	movq	-1(%rax), %rdx
	cmpw	$165, 11(%rdx)
	je	.L1839
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	setne	%dl
	jmp	.L1838
.L1870:
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1833:
	movq	41088(%r13), %r15
	cmpq	41096(%r13), %r15
	je	.L1947
.L1835:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r15)
	jmp	.L1836
.L1945:
	movq	-1(%rax), %rcx
	cmpw	$86, 11(%rcx)
	jne	.L1828
	movq	39(%rax), %rcx
	testb	$1, %cl
	je	.L1828
	movq	-1(%rcx), %rcx
	cmpw	$72, 11(%rcx)
	jne	.L1828
	movq	31(%rax), %rsi
	jmp	.L1831
.L1946:
	movq	-1(%rax), %rax
	cmpw	$72, 11(%rax)
	jne	.L1832
	movq	7(%rbx), %rsi
	jmp	.L1831
.L1944:
	movq	%r14, %rdi
	movq	%r8, -424(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-424(%rbp), %r8
	movq	%rax, %rdx
	jmp	.L1842
.L1943:
	movq	%r14, %rdi
	movq	%r8, -424(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-424(%rbp), %r8
	jmp	.L1846
.L1927:
	call	__stack_chk_fail@PLT
.L1820:
	leaq	-296(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1923
.L1947:
	movq	%r13, %rdi
	movq	%rsi, -448(%rbp)
	movq	%r8, -424(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-448(%rbp), %rsi
	movq	-424(%rbp), %r8
	movq	%rax, %r15
	jmp	.L1835
	.cfi_endproc
.LFE25328:
	.size	_ZN2v88internal8Compiler30GetSharedFunctionInfoForScriptEPNS0_7IsolateENS0_6HandleINS0_6StringEEERKNS1_13ScriptDetailsENS_19ScriptOriginOptionsEPNS_9ExtensionEPNS0_10ScriptDataENS_14ScriptCompiler14CompileOptionsENSF_13NoCacheReasonENS0_11NativesFlagE, .-_ZN2v88internal8Compiler30GetSharedFunctionInfoForScriptEPNS0_7IsolateENS0_6HandleINS0_6StringEEERKNS1_13ScriptDetailsENS_19ScriptOriginOptionsEPNS_9ExtensionEPNS0_10ScriptDataENS_14ScriptCompiler14CompileOptionsENSF_13NoCacheReasonENS0_11NativesFlagE
	.section	.text._ZN2v88internal8Compiler18GetWrappedFunctionENS0_6HandleINS0_6StringEEENS2_INS0_10FixedArrayEEENS2_INS0_7ContextEEERKNS1_13ScriptDetailsENS_19ScriptOriginOptionsEPNS0_10ScriptDataENS_14ScriptCompiler14CompileOptionsENSF_13NoCacheReasonE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Compiler18GetWrappedFunctionENS0_6HandleINS0_6StringEEENS2_INS0_10FixedArrayEEENS2_INS0_7ContextEEERKNS1_13ScriptDetailsENS_19ScriptOriginOptionsEPNS0_10ScriptDataENS_14ScriptCompiler14CompileOptionsENSF_13NoCacheReasonE
	.type	_ZN2v88internal8Compiler18GetWrappedFunctionENS0_6HandleINS0_6StringEEENS2_INS0_10FixedArrayEEENS2_INS0_7ContextEEERKNS1_13ScriptDetailsENS_19ScriptOriginOptionsEPNS0_10ScriptDataENS_14ScriptCompiler14CompileOptionsENSF_13NoCacheReasonE, @function
_ZN2v88internal8Compiler18GetWrappedFunctionENS0_6HandleINS0_6StringEEENS2_INS0_10FixedArrayEEENS2_INS0_7ContextEEERKNS1_13ScriptDetailsENS_19ScriptOriginOptionsEPNS0_10ScriptDataENS_14ScriptCompiler14CompileOptionsENSF_13NoCacheReasonE:
.LFB25329:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r9, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$440, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -440(%rbp)
	movq	%rcx, -448(%rbp)
	movl	%r8d, -424(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	andq	$-262144, %rax
	movq	24(%rax), %r12
	movq	$0, -344(%rbp)
	movq	$0, -336(%rbp)
	subq	$37592, %r12
	movq	%r12, -352(%rbp)
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	40960(%r12), %rsi
	movq	%rax, -344(%rbp)
	movq	2856(%rsi), %rax
	leaq	2824(%rsi), %rdi
	addq	$2872, %rsi
	movq	%rdi, -328(%rbp)
	movq	16(%rax), %rdx
	call	_ZN2v88internal14TimedHistogram5StartEPNS_4base12ElapsedTimerEPNS0_7IsolateE@PLT
	movl	24(%rbp), %eax
	movq	40960(%r12), %rdx
	movl	$0, -316(%rbp)
	movl	%eax, -320(%rbp)
	movq	(%r14), %rax
	cmpb	$0, 6528(%rdx)
	movl	11(%rax), %ebx
	je	.L1949
	movq	6520(%rdx), %rax
.L1950:
	testq	%rax, %rax
	je	.L1951
	addl	%ebx, (%rax)
.L1951:
	movzbl	_ZN2v88internal15FLAG_use_strictE(%rip), %eax
	cmpl	$1, 16(%rbp)
	movb	%al, -432(%rbp)
	je	.L1952
.L2067:
	leaq	-304(%rbp), %rbx
	movq	%r12, %rsi
	movq	$0, -400(%rbp)
	movq	%rbx, %rdi
	movb	$0, -392(%rbp)
	call	_ZN2v88internal9ParseInfoC1EPNS0_7IsolateE@PLT
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	-448(%rbp), %rax
	movl	-424(%rbp), %ecx
	xorl	%r8d, %r8d
	pushq	24(%rax)
	pushq	16(%rax)
	pushq	8(%rax)
	pushq	(%rax)
	call	_ZN2v88internal12_GLOBAL__N_19NewScriptEPNS0_7IsolateEPNS0_9ParseInfoENS0_6HandleINS0_6StringEEENS0_8Compiler13ScriptDetailsENS_19ScriptOriginOptionsENS0_11NativesFlagE
	addq	$32, %rsp
	movq	(%rax), %rdi
	movq	%rax, %r14
	movq	-440(%rbp), %rax
	movq	(%rax), %r15
	leaq	71(%rdi), %rsi
	movq	%r15, 71(%rdi)
	testb	$1, %r15b
	je	.L2026
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -424(%rbp)
	testl	$262144, %eax
	jne	.L2069
.L1966:
	testb	$24, %al
	je	.L2026
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2070
	.p2align 4,,10
	.p2align 3
.L2026:
	movq	0(%r13), %rax
	movb	$4, -255(%rbp)
	orl	$268435460, -296(%rbp)
	movq	-1(%rax), %rdx
	cmpw	$145, 11(%rdx)
	je	.L1969
	leaq	-384(%rbp), %rdi
	movq	%rax, -384(%rbp)
	call	_ZN2v88internal7Context10scope_infoEv@PLT
	movq	41112(%r12), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L1989
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L1990:
	movq	%rax, -216(%rbp)
.L1969:
	movl	-296(%rbp), %eax
	movzbl	-432(%rbp), %esi
	movq	%rbx, %rdi
	movl	%eax, %edx
	movl	%eax, %ecx
	andl	$-9, %eax
	shrl	$3, %edx
	orl	$8, %ecx
	andl	$1, %edx
	orb	%dl, %sil
	leaq	-400(%rbp), %rdx
	movq	%r12, %rsi
	cmovne	%ecx, %eax
	movl	%eax, -296(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_115CompileToplevelEPNS0_9ParseInfoEPNS0_7IsolateEPNS0_15IsCompiledScopeE
	testq	%rax, %rax
	je	.L2071
	movq	(%r14), %rdx
	leaq	-384(%rbp), %r14
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal18SharedFunctionInfo14ScriptIteratorC1EPNS0_7IsolateENS0_6ScriptE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal18SharedFunctionInfo14ScriptIterator4NextEv@PLT
	testq	%rax, %rax
	jne	.L2000
	jmp	.L2025
	.p2align 4,,10
	.p2align 3
.L1996:
	movq	%r14, %rdi
	call	_ZN2v88internal18SharedFunctionInfo14ScriptIterator4NextEv@PLT
	testq	%rax, %rax
	je	.L2025
.L2000:
	movl	47(%rax), %edx
	shrl	$7, %edx
	andl	$7, %edx
	cmpb	$4, %dl
	jne	.L1996
	movq	41112(%r12), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L1997
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
	jmp	.L1998
	.p2align 4,,10
	.p2align 3
.L1949:
	movb	$1, 6528(%rdx)
	leaq	6504(%rdx), %rdi
	movq	%rdx, -432(%rbp)
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	-432(%rbp), %rdx
	movq	%rax, 6520(%rdx)
	jmp	.L1950
	.p2align 4,,10
	.p2align 3
.L1952:
	movq	40960(%r12), %rax
	movb	$1, -314(%rbp)
	leaq	2768(%rax), %rdi
	leaq	2816(%rax), %rsi
	movq	%rax, -472(%rbp)
	movq	2800(%rax), %rax
	movq	%rdi, -456(%rbp)
	movq	16(%rax), %rdx
	movq	%rsi, -464(%rbp)
	call	_ZN2v88internal14TimedHistogram5StartEPNS_4base12ElapsedTimerEPNS0_7IsolateE@PLT
	pxor	%xmm0, %xmm0
	movq	$0, -272(%rbp)
	movaps	%xmm0, -304(%rbp)
	movaps	%xmm0, -288(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2072
.L1954:
	movq	_ZZN2v88internal8Compiler18GetWrappedFunctionENS0_6HandleINS0_6StringEEENS2_INS0_10FixedArrayEEENS2_INS0_7ContextEEERKNS1_13ScriptDetailsENS_19ScriptOriginOptionsEPNS0_10ScriptDataENS_14ScriptCompiler14CompileOptionsENSF_13NoCacheReasonEE29trace_event_unique_atomic2100(%rip), %rdx
	movq	%rdx, %rbx
	testq	%rdx, %rdx
	je	.L2073
.L1956:
	movq	$0, -384(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2074
.L1958:
	movl	-424(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal14CodeSerializer11DeserializeEPNS0_7IsolateEPNS0_10ScriptDataENS0_6HandleINS0_6StringEEENS_19ScriptOriginOptionsE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L2075
	leaq	-384(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-304(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2076
.L2068:
	movq	-472(%rbp), %rax
	movq	-464(%rbp), %rsi
	movq	-456(%rbp), %rdi
	movq	2800(%rax), %rax
	movq	16(%rax), %rdx
	call	_ZN2v88internal14TimedHistogram4StopEPNS_4base12ElapsedTimerEPNS0_7IsolateE@PLT
	movb	$0, -392(%rbp)
	movq	$0, -400(%rbp)
	movq	(%r15), %rbx
	movq	%rbx, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	7(%rbx), %rdx
	testb	$1, %dl
	jne	.L1970
.L1973:
	movq	7(%rbx), %rdx
	testb	$1, %dl
	jne	.L2077
.L1971:
	xorl	%eax, %eax
.L1982:
	movabsq	$287762808832, %rcx
	movq	7(%rbx), %rdx
	cmpq	%rcx, %rdx
	je	.L1985
	movl	$1, %ecx
	testb	$1, %dl
	jne	.L2078
.L1984:
	xorl	%edx, %edx
	movq	%rax, -416(%rbp)
	movb	%cl, %dl
	movq	%rax, -400(%rbp)
	movq	%rdx, -408(%rbp)
	movb	%dl, -392(%rbp)
	movq	(%r15), %rax
	movq	31(%rax), %rsi
	testb	$1, %sil
	jne	.L2079
.L1986:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2080
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L2001:
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%r15, %rsi
	call	_ZN2v88internal7Factory33NewFunctionFromSharedFunctionInfoENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_14AllocationTypeE@PLT
	movq	%rax, %r12
.L1995:
	cmpb	$0, -315(%rbp)
	movzbl	-316(%rbp), %eax
	je	.L2004
	movq	-352(%rbp), %rdx
	movq	40960(%rdx), %rdi
	addq	$1728, %rdi
	testb	%al, %al
	jne	.L2081
	xorl	%esi, %esi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
.L2018:
	movq	-352(%rbp), %rax
	movq	40960(%rax), %rax
	addq	$4968, %rax
	.p2align 4,,10
	.p2align 3
.L2024:
	movq	-328(%rbp), %rdi
	movq	%rax, -336(%rbp)
	movq	32(%rdi), %rax
	leaq	48(%rdi), %rsi
	movq	16(%rax), %rdx
	call	_ZN2v88internal14TimedHistogram4StopEPNS_4base12ElapsedTimerEPNS0_7IsolateE@PLT
	movq	-336(%rbp), %rdi
	xorl	%edx, %edx
	leaq	-344(%rbp), %rsi
	call	_ZN2v88internal14TimedHistogram4StopEPNS_4base12ElapsedTimerEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2082
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2077:
	.cfi_restore_state
	movq	-1(%rdx), %rdx
	cmpw	$91, 11(%rdx)
	jne	.L1971
.L1976:
	movq	31(%rbx), %rdx
	testb	$1, %dl
	jne	.L2083
.L1974:
	movq	7(%rbx), %rdx
	testb	$1, %dl
	jne	.L2084
.L1978:
	movq	7(%rbx), %rdx
	movq	7(%rdx), %rsi
.L1977:
	movq	3520(%rax), %rdi
	leaq	-37592(%rax), %r14
	testq	%rdi, %rdi
	je	.L1979
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	jmp	.L1982
	.p2align 4,,10
	.p2align 3
.L2074:
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rcx
	movq	$0, -480(%rbp)
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2085
.L1959:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1960
	movq	(%rdi), %rax
	call	*8(%rax)
.L1960:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1961
	movq	(%rdi), %rax
	call	*8(%rax)
.L1961:
	leaq	.LC35(%rip), %rax
	movq	%rbx, -376(%rbp)
	movq	%rax, -368(%rbp)
	movq	-480(%rbp), %rax
	movq	%rax, -360(%rbp)
	leaq	-376(%rbp), %rax
	movq	%rax, -384(%rbp)
	jmp	.L1958
	.p2align 4,,10
	.p2align 3
.L2073:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2086
.L1957:
	movq	%rbx, _ZZN2v88internal8Compiler18GetWrappedFunctionENS0_6HandleINS0_6StringEEENS2_INS0_10FixedArrayEEENS2_INS0_7ContextEEERKNS1_13ScriptDetailsENS_19ScriptOriginOptionsEPNS0_10ScriptDataENS_14ScriptCompiler14CompileOptionsENSF_13NoCacheReasonEE29trace_event_unique_atomic2100(%rip)
	jmp	.L1956
	.p2align 4,,10
	.p2align 3
.L2075:
	leaq	-384(%rbp), %rdi
	movb	$1, -313(%rbp)
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2066
	leaq	-296(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
.L2066:
	movq	-472(%rbp), %rax
	movq	-464(%rbp), %rsi
	movq	-456(%rbp), %rdi
	movq	2800(%rax), %rax
	movq	16(%rax), %rdx
	call	_ZN2v88internal14TimedHistogram4StopEPNS_4base12ElapsedTimerEPNS0_7IsolateE@PLT
	jmp	.L2067
	.p2align 4,,10
	.p2align 3
.L2069:
	movq	%r15, %rdx
	movq	%rsi, -448(%rbp)
	movq	%rdi, -440(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-424(%rbp), %rcx
	movq	-448(%rbp), %rsi
	movq	-440(%rbp), %rdi
	movq	8(%rcx), %rax
	jmp	.L1966
	.p2align 4,,10
	.p2align 3
.L2004:
	cmpb	$0, -314(%rbp)
	je	.L2007
	movq	-352(%rbp), %rdx
	movq	40960(%rdx), %rdi
	addq	$1728, %rdi
	testb	%al, %al
	jne	.L2008
	cmpb	$0, -313(%rbp)
	je	.L2087
	movl	$3, %esi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
.L2022:
	movq	-352(%rbp), %rax
	movq	40960(%rax), %rax
	addq	$5112, %rax
	jmp	.L2024
	.p2align 4,,10
	.p2align 3
.L2080:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L2088
.L2003:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L2001
	.p2align 4,,10
	.p2align 3
.L1985:
	xorl	%ecx, %ecx
	jmp	.L1984
	.p2align 4,,10
	.p2align 3
.L1970:
	movq	-1(%rdx), %rdx
	cmpw	$72, 11(%rdx)
	jne	.L1973
	jmp	.L1976
	.p2align 4,,10
	.p2align 3
.L2079:
	movq	-1(%rsi), %rax
	cmpw	$86, 11(%rax)
	jne	.L1986
	movq	23(%rsi), %rsi
	jmp	.L1986
	.p2align 4,,10
	.p2align 3
.L2007:
	movl	-320(%rbp), %edx
	testb	%al, %al
	je	.L2011
	movq	-352(%rbp), %rax
	movq	40960(%rax), %rdi
	addq	$1728, %rdi
	cmpl	$5, %edx
	je	.L2089
	movl	$1, %esi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
.L2015:
	movq	-352(%rbp), %rax
	movq	40960(%rax), %rax
	addq	$5016, %rax
	jmp	.L2024
	.p2align 4,,10
	.p2align 3
.L2078:
	movq	-1(%rdx), %rcx
	cmpw	$165, 11(%rcx)
	je	.L1985
	movq	-1(%rdx), %rdx
	cmpw	$166, 11(%rdx)
	setne	%cl
	jmp	.L1984
	.p2align 4,,10
	.p2align 3
.L2081:
	movl	$14, %esi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	jmp	.L2018
	.p2align 4,,10
	.p2align 3
.L1979:
	movq	41088(%r14), %rax
	cmpq	41096(%r14), %rax
	je	.L2090
.L1981:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r14)
	movq	%rsi, (%rax)
	jmp	.L1982
	.p2align 4,,10
	.p2align 3
.L2011:
	cmpl	$14, %edx
	ja	.L2014
	leaq	CSWTCH.453(%rip), %rax
	movl	(%rax,%rdx,4), %ebx
	movq	-352(%rbp), %rax
	movq	40960(%rax), %rdi
	movl	%ebx, %esi
	addq	$1728, %rdi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	cmpl	$20, %ebx
	ja	.L2014
	leaq	.L2016(%rip), %rdx
	movslq	(%rdx,%rbx,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8Compiler18GetWrappedFunctionENS0_6HandleINS0_6StringEEENS2_INS0_10FixedArrayEEENS2_INS0_7ContextEEERKNS1_13ScriptDetailsENS_19ScriptOriginOptionsEPNS0_10ScriptDataENS_14ScriptCompiler14CompileOptionsENSF_13NoCacheReasonE,"a",@progbits
	.align 4
	.align 4
.L2016:
	.long	.L2018-.L2016
	.long	.L2015-.L2016
	.long	.L2023-.L2016
	.long	.L2022-.L2016
	.long	.L2021-.L2016
	.long	.L2020-.L2016
	.long	.L2019-.L2016
	.long	.L2017-.L2016
	.long	.L2017-.L2016
	.long	.L2017-.L2016
	.long	.L2017-.L2016
	.long	.L2017-.L2016
	.long	.L2017-.L2016
	.long	.L2017-.L2016
	.long	.L2018-.L2016
	.long	.L2015-.L2016
	.long	.L2017-.L2016
	.long	.L2017-.L2016
	.long	.L2017-.L2016
	.long	.L2017-.L2016
	.long	.L2015-.L2016
	.section	.text._ZN2v88internal8Compiler18GetWrappedFunctionENS0_6HandleINS0_6StringEEENS2_INS0_10FixedArrayEEENS2_INS0_7ContextEEERKNS1_13ScriptDetailsENS_19ScriptOriginOptionsEPNS0_10ScriptDataENS_14ScriptCompiler14CompileOptionsENSF_13NoCacheReasonE
	.p2align 4,,10
	.p2align 3
.L2017:
	movq	-352(%rbp), %rax
	movq	40960(%rax), %rax
	addq	$5160, %rax
	jmp	.L2024
.L2019:
	movq	-352(%rbp), %rax
	movq	40960(%rax), %rax
	addq	$5304, %rax
	jmp	.L2024
.L2020:
	movq	-352(%rbp), %rax
	movq	40960(%rax), %rax
	addq	$5256, %rax
	jmp	.L2024
.L2021:
	movq	-352(%rbp), %rax
	movq	40960(%rax), %rax
	addq	$5208, %rax
	jmp	.L2024
	.p2align 4,,10
	.p2align 3
.L2087:
	movl	$2, %esi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
.L2023:
	movq	-352(%rbp), %rax
	movq	40960(%rax), %rax
	addq	$5064, %rax
	jmp	.L2024
	.p2align 4,,10
	.p2align 3
.L2083:
	movq	-1(%rdx), %rcx
	cmpw	$86, 11(%rcx)
	jne	.L1974
	movq	39(%rdx), %rcx
	testb	$1, %cl
	je	.L1974
	movq	-1(%rcx), %rcx
	cmpw	$72, 11(%rcx)
	jne	.L1974
	movq	31(%rdx), %rsi
	jmp	.L1977
	.p2align 4,,10
	.p2align 3
.L2025:
	xorl	%r15d, %r15d
.L1998:
	movq	%rbx, %rdi
	call	_ZN2v88internal9ParseInfoD1Ev@PLT
	jmp	.L2001
	.p2align 4,,10
	.p2align 3
.L2084:
	movq	-1(%rdx), %rdx
	cmpw	$72, 11(%rdx)
	jne	.L1978
	movq	7(%rbx), %rsi
	jmp	.L1977
	.p2align 4,,10
	.p2align 3
.L2088:
	movq	%r12, %rdi
	movq	%rsi, -424(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-424(%rbp), %rsi
	jmp	.L2003
	.p2align 4,,10
	.p2align 3
.L2008:
	movl	$15, %esi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	jmp	.L2015
	.p2align 4,,10
	.p2align 3
.L1989:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L2091
.L1991:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L1990
	.p2align 4,,10
	.p2align 3
.L1997:
	movq	41088(%r12), %r15
	cmpq	41096(%r12), %r15
	je	.L2092
.L1999:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r15)
	jmp	.L1998
	.p2align 4,,10
	.p2align 3
.L2070:
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2026
	.p2align 4,,10
	.p2align 3
.L2072:
	movq	40960(%r12), %rax
	leaq	-296(%rbp), %rsi
	movl	$126, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -304(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1954
	.p2align 4,,10
	.p2align 3
.L2071:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	_ZN2v88internal7Isolate21ReportPendingMessagesEv@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal9ParseInfoD1Ev@PLT
	jmp	.L1995
	.p2align 4,,10
	.p2align 3
.L2089:
	movl	$20, %esi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	jmp	.L2015
	.p2align 4,,10
	.p2align 3
.L2090:
	movq	%r14, %rdi
	movq	%rsi, -424(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-424(%rbp), %rsi
	jmp	.L1981
.L2085:
	subq	$8, %rsp
	leaq	-80(%rbp), %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	movq	%rbx, %rdx
	movl	$88, %esi
	pushq	%rcx
	leaq	.LC35(%rip), %rcx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, -480(%rbp)
	addq	$64, %rsp
	jmp	.L1959
.L2086:
	leaq	.LC6(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1957
.L2091:
	movq	%r12, %rdi
	movq	%rsi, -424(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-424(%rbp), %rsi
	jmp	.L1991
.L2092:
	movq	%r12, %rdi
	movq	%rax, -424(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-424(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L1999
.L2014:
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2082:
	call	__stack_chk_fail@PLT
.L2076:
	leaq	-296(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2068
	.cfi_endproc
.LFE25329:
	.size	_ZN2v88internal8Compiler18GetWrappedFunctionENS0_6HandleINS0_6StringEEENS2_INS0_10FixedArrayEEENS2_INS0_7ContextEEERKNS1_13ScriptDetailsENS_19ScriptOriginOptionsEPNS0_10ScriptDataENS_14ScriptCompiler14CompileOptionsENSF_13NoCacheReasonE, .-_ZN2v88internal8Compiler18GetWrappedFunctionENS0_6HandleINS0_6StringEEENS2_INS0_10FixedArrayEEENS2_INS0_7ContextEEERKNS1_13ScriptDetailsENS_19ScriptOriginOptionsEPNS0_10ScriptDataENS_14ScriptCompiler14CompileOptionsENSF_13NoCacheReasonE
	.section	.text._ZN2v88internal8Compiler19GetFunctionFromEvalENS0_6HandleINS0_6StringEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_12LanguageModeENS0_16ParseRestrictionEiii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Compiler19GetFunctionFromEvalENS0_6HandleINS0_6StringEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_12LanguageModeENS0_16ParseRestrictionEiii
	.type	_ZN2v88internal8Compiler19GetFunctionFromEvalENS0_6HandleINS0_6StringEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_12LanguageModeENS0_16ParseRestrictionEiii, @function
_ZN2v88internal8Compiler19GetFunctionFromEvalENS0_6HandleINS0_6StringEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_12LanguageModeENS0_16ParseRestrictionEiii:
.LFB25287:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%r9d, %ebx
	subq	$1976, %rsp
	movl	24(%rbp), %eax
	movq	%rsi, -1912(%rbp)
	movl	%ecx, -1928(%rbp)
	movl	%r8d, -1936(%rbp)
	movl	%eax, -1944(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	andq	$-262144, %rax
	movq	24(%rax), %r12
	movq	(%rdi), %rax
	movq	3368(%r12), %r15
	subq	$37592, %r12
	movl	11(%rax), %edx
	cmpb	$0, 6400(%r15)
	je	.L2094
	movq	6392(%r15), %rax
.L2095:
	testq	%rax, %rax
	je	.L2096
	addl	%edx, (%rax)
.L2096:
	movq	40960(%r12), %r15
	cmpb	$0, 6528(%r15)
	je	.L2097
	movq	6520(%r15), %rax
.L2098:
	testq	%rax, %rax
	je	.L2099
	addl	%edx, (%rax)
.L2099:
	cmpl	$1, -1936(%rbp)
	jne	.L2100
	cmpl	$-1, %ebx
	movl	%ebx, %eax
	setne	%dl
	negl	%eax
	testb	%dl, %dl
	cmove	16(%rbp), %eax
	movl	%eax, 16(%rbp)
.L2100:
	movl	16(%rbp), %ecx
	subq	$8, %rsp
	movq	%r13, %r8
	movq	%r14, %rdx
	movq	40952(%r12), %rax
	movzbl	-1928(%rbp), %r9d
	leaq	-1840(%rbp), %rdi
	pushq	%rcx
	movq	-1912(%rbp), %rcx
	movq	%rax, %rsi
	movq	%rax, -1960(%rbp)
	call	_ZN2v88internal16CompilationCache10LookupEvalENS0_6HandleINS0_6StringEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_12LanguageModeEi@PLT
	movzbl	-1832(%rbp), %eax
	movq	-1824(%rbp), %rcx
	movq	$0, -1976(%rbp)
	movq	-1816(%rbp), %rsi
	popq	%r8
	movb	%al, -1945(%rbp)
	popq	%r9
	movq	%rcx, -1920(%rbp)
	movq	%rsi, -1968(%rbp)
	testq	%rsi, %rsi
	je	.L2101
	testb	%al, %al
	je	.L2102
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2103
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L2104:
	cmpq	$0, -1920(%rbp)
	movq	%rax, -1976(%rbp)
	movq	$0, -1856(%rbp)
	movb	$0, -1848(%rbp)
	je	.L2181
.L2109:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2250
	movq	-1920(%rbp), %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r9
	movq	(%rax), %rax
	movq	%rax, -1920(%rbp)
.L2110:
	movq	31(%rax), %rsi
	testb	$1, %sil
	jne	.L2251
.L2112:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2113
	movq	%r9, -1920(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-1920(%rbp), %r9
.L2114:
	movq	(%r9), %rbx
	movq	%rbx, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	7(%rbx), %rdx
	testb	$1, %dl
	jne	.L2116
.L2119:
	movq	7(%rbx), %rdx
	testb	$1, %dl
	jne	.L2252
.L2117:
	xorl	%eax, %eax
.L2128:
	movabsq	$287762808832, %rcx
	movq	7(%rbx), %rdx
	cmpq	%rcx, %rdx
	je	.L2131
	movl	$1, %ecx
	testb	$1, %dl
	jne	.L2253
.L2130:
	xorl	%edx, %edx
	cmpq	$0, -1968(%rbp)
	movq	%rax, -1904(%rbp)
	movb	%cl, %dl
	movq	%rax, -1856(%rbp)
	movq	%rdx, -1896(%rbp)
	movb	%dl, -1848(%rbp)
	je	.L2254
.L2132:
	movq	-1976(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r13, %rdx
	movq	%r9, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory33NewFunctionFromSharedFunctionInfoENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS2_INS0_12FeedbackCellEEENS0_14AllocationTypeE@PLT
	movq	%rax, %r15
	.p2align 4,,10
	.p2align 3
.L2172:
	movq	%r15, %rax
.L2169:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L2255
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2101:
	.cfi_restore_state
	cmpq	$0, -1920(%rbp)
	movb	$0, -1848(%rbp)
	movq	$0, -1856(%rbp)
	je	.L2181
	cmpb	$0, -1945(%rbp)
	jne	.L2109
	.p2align 4,,10
	.p2align 3
.L2181:
	leaq	-1744(%rbp), %r15
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal9ParseInfoC1EPNS0_7IsolateE@PLT
	movq	-1912(%rbp), %rax
	movq	(%rax), %rax
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L2256
.L2134:
	xorl	%ecx, %ecx
.L2137:
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	xorl	%r8d, %r8d
	call	_ZN2v88internal9ParseInfo12CreateScriptEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS_19ScriptOriginOptionsENS0_11NativesFlagE@PLT
	movq	(%rax), %rdx
	movq	%rax, %rcx
	movslq	99(%rdx), %rax
	orl	$1, %eax
	salq	$32, %rax
	movq	%rax, 95(%rdx)
	movq	-1912(%rbp), %rax
	movq	(%rcx), %rdi
	movq	(%rax), %rdx
	leaq	71(%rdi), %rsi
	movq	%rdx, 71(%rdi)
	testb	$1, %dl
	je	.L2180
	movq	%rdx, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rax
	movq	%r8, -1984(%rbp)
	testl	$262144, %eax
	je	.L2139
	movq	%rcx, -2016(%rbp)
	movq	%rdx, -2008(%rbp)
	movq	%rsi, -2000(%rbp)
	movq	%rdi, -1992(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-1984(%rbp), %r8
	movq	-2016(%rbp), %rcx
	movq	-2008(%rbp), %rdx
	movq	-2000(%rbp), %rsi
	movq	8(%r8), %rax
	movq	-1992(%rbp), %rdi
.L2139:
	testb	$24, %al
	je	.L2180
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2257
.L2180:
	movl	-1944(%rbp), %eax
	cmpl	$-1, %eax
	je	.L2141
.L2267:
	salq	$32, %rax
.L2142:
	movq	(%rcx), %rdx
	movq	%rax, 79(%rdx)
	movq	_ZZN2v88internal8Compiler19GetFunctionFromEvalENS0_6HandleINS0_6StringEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_12LanguageModeENS0_16ParseRestrictionEiiiE29trace_event_unique_atomic1558(%rip), %rdx
	testq	%rdx, %rdx
	je	.L2258
.L2153:
	movzbl	(%rdx), %eax
	testb	$5, %al
	jne	.L2259
.L2155:
	movl	-1736(%rbp), %eax
	movl	-1936(%rbp), %edi
	movl	%ebx, -1680(%rbp)
	movl	%eax, %edx
	andl	$-9, %eax
	orl	$12, %edx
	orl	$4, %eax
	cmpb	$0, -1928(%rbp)
	cmovne	%edx, %eax
	movl	%eax, %edx
	andl	$-33, %eax
	orl	$32, %edx
	testl	%edi, %edi
	cmovne	%edx, %eax
	movl	%eax, -1736(%rbp)
	movq	0(%r13), %rax
	movq	-1(%rax), %rdx
	cmpw	$145, 11(%rdx)
	je	.L2164
	leaq	-1520(%rbp), %rdi
	movq	%rax, -1520(%rbp)
	call	_ZN2v88internal7Context10scope_infoEv@PLT
	movq	41112(%r12), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L2165
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L2166:
	movq	%rax, -1656(%rbp)
.L2164:
	leaq	-1856(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal12_GLOBAL__N_115CompileToplevelEPNS0_9ParseInfoEPNS0_7IsolateEPNS0_15IsCompiledScopeE
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L2260
	movq	%rax, -1936(%rbp)
	movl	-1736(%rbp), %eax
	movq	%r15, %rdi
	andl	$16384, %eax
	movl	%eax, -1928(%rbp)
	call	_ZN2v88internal9ParseInfoD1Ev@PLT
	cmpq	$0, -1920(%rbp)
	movq	-1936(%rbp), %r9
	jne	.L2261
.L2170:
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory33NewFunctionFromSharedFunctionInfoENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal10JSFunction22InitializeFeedbackCellENS0_6HandleIS1_EE@PLT
	movl	-1928(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L2172
	movq	(%r15), %rax
	movq	41112(%r12), %rdi
	movq	39(%rax), %rsi
	testq	%rdi, %rdi
	je	.L2176
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r9
.L2177:
	movl	16(%rbp), %eax
	subq	$8, %rsp
	movq	%rbx, %r8
	pushq	%rax
.L2249:
	movq	-1912(%rbp), %rdx
	movq	-1960(%rbp), %rdi
	movq	%r13, %rcx
	movq	%r14, %rsi
	call	_ZN2v88internal16CompilationCache7PutEvalENS0_6HandleINS0_6StringEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEES6_NS2_INS0_12FeedbackCellEEEi@PLT
	popq	%rax
	popq	%rdx
	jmp	.L2172
	.p2align 4,,10
	.p2align 3
.L2256:
	movq	-1(%rax), %rcx
	leaq	-1(%rax), %rdx
	cmpw	$86, 11(%rcx)
	je	.L2262
.L2135:
	movq	(%rdx), %rdx
	cmpw	$96, 11(%rdx)
	jne	.L2134
	movslq	99(%rax), %rcx
	sarl	$2, %ecx
	andl	$3, %ecx
	jmp	.L2137
	.p2align 4,,10
	.p2align 3
.L2097:
	movb	$1, 6528(%r15)
	leaq	6504(%r15), %rdi
	movl	%edx, -1920(%rbp)
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movl	-1920(%rbp), %edx
	movq	%rax, 6520(%r15)
	jmp	.L2098
	.p2align 4,,10
	.p2align 3
.L2094:
	movb	$1, 6400(%r15)
	leaq	6376(%r15), %rdi
	movl	%edx, -1920(%rbp)
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movl	-1920(%rbp), %edx
	movq	%rax, 6392(%r15)
	jmp	.L2095
	.p2align 4,,10
	.p2align 3
.L2102:
	movq	$0, -1856(%rbp)
	movb	$0, -1848(%rbp)
	jmp	.L2181
	.p2align 4,,10
	.p2align 3
.L2259:
	movq	(%rcx), %rax
	leaq	-1864(%rbp), %rdi
	leaq	-1872(%rbp), %rsi
	movq	%rdx, -1944(%rbp)
	movq	63(%rax), %rcx
	movq	%rax, -1872(%rbp)
	movq	%rcx, -1992(%rbp)
	movq	_ZN2v88internal6Script11kTraceScopeE(%rip), %rcx
	movq	%rcx, -1984(%rbp)
	call	_ZN2v88internal6Script13ToTracedValueEv@PLT
	leaq	.LC7(%rip), %rax
	movb	$8, -1873(%rbp)
	movq	%rax, -1808(%rbp)
	movq	-1864(%rbp), %rax
	movq	$0, -72(%rbp)
	movq	$0, -1864(%rbp)
	movq	%rax, -1520(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rcx
	movq	-1944(%rbp), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2263
.L2156:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2157
	movq	(%rdi), %rax
	call	*8(%rax)
.L2157:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2158
	movq	(%rdi), %rax
	call	*8(%rax)
.L2158:
	movq	-1864(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2155
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L2155
	.p2align 4,,10
	.p2align 3
.L2258:
	movq	%rcx, -1944(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rsi
	movq	-1944(%rbp), %rcx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2264
.L2154:
	movq	%rdx, _ZZN2v88internal8Compiler19GetFunctionFromEvalENS0_6HandleINS0_6StringEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_12LanguageModeENS0_16ParseRestrictionEiiiE29trace_event_unique_atomic1558(%rip)
	jmp	.L2153
	.p2align 4,,10
	.p2align 3
.L2261:
	cmpb	$0, -1945(%rbp)
	je	.L2170
	cmpq	$0, -1968(%rbp)
	jne	.L2132
	xorl	%ecx, %ecx
	movq	%rbx, %rsi
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rbx, -1920(%rbp)
	call	_ZN2v88internal7Factory33NewFunctionFromSharedFunctionInfoENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal10JSFunction22InitializeFeedbackCellENS0_6HandleIS1_EE@PLT
	movl	-1928(%rbp), %esi
	movq	-1920(%rbp), %r8
	testl	%esi, %esi
	je	.L2172
	jmp	.L2182
	.p2align 4,,10
	.p2align 3
.L2165:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L2265
.L2167:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L2166
	.p2align 4,,10
	.p2align 3
.L2176:
	movq	41088(%r12), %r9
	cmpq	41096(%r12), %r9
	je	.L2266
.L2178:
	leaq	8(%r9), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r9)
	jmp	.L2177
	.p2align 4,,10
	.p2align 3
.L2257:
	movq	%rcx, -1984(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movl	-1944(%rbp), %eax
	movq	-1984(%rbp), %rcx
	cmpl	$-1, %eax
	jne	.L2267
	.p2align 4,,10
	.p2align 3
.L2141:
	leaq	-1520(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rcx, -1944(%rbp)
	call	_ZN2v88internal23StackTraceFrameIteratorC1EPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rdi
	movq	-1944(%rbp), %rcx
	testq	%rdi, %rdi
	je	.L2145
	movq	(%rdi), %rax
	movq	%rcx, -1944(%rbp)
	call	*8(%rax)
	movq	-1944(%rbp), %rcx
	cmpl	$20, %eax
	ja	.L2145
	movl	$1150992, %edx
	btq	%rax, %rdx
	jnc	.L2145
	leaq	-1808(%rbp), %r8
	movq	-104(%rbp), %rsi
	movq	%rcx, -1984(%rbp)
	movq	%r8, %rdi
	movq	%r8, -1944(%rbp)
	call	_ZN2v88internal12FrameSummary6GetTopEPKNS0_13StandardFrameE@PLT
	movq	-1784(%rbp), %rax
	movq	-1984(%rbp), %rcx
	movq	(%rax), %rax
	movq	(%rcx), %rdi
	movq	23(%rax), %rdx
	leaq	71(%rdi), %rsi
	movq	%rdx, 71(%rdi)
	testb	$1, %dl
	movq	-1944(%rbp), %r8
	je	.L2179
	movq	%rdx, %r9
	andq	$-262144, %r9
	movq	8(%r9), %rax
	movq	%r9, -1944(%rbp)
	testl	$262144, %eax
	je	.L2147
	movq	%r8, -2016(%rbp)
	movq	%rcx, -2008(%rbp)
	movq	%rdx, -2000(%rbp)
	movq	%rsi, -1992(%rbp)
	movq	%rdi, -1984(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-1944(%rbp), %r9
	movq	-2016(%rbp), %r8
	movq	-2008(%rbp), %rcx
	movq	-2000(%rbp), %rdx
	movq	8(%r9), %rax
	movq	-1992(%rbp), %rsi
	movq	-1984(%rbp), %rdi
.L2147:
	testb	$24, %al
	je	.L2179
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L2179
	movq	%r8, -1984(%rbp)
	movq	%rcx, -1944(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-1984(%rbp), %r8
	movq	-1944(%rbp), %rcx
	.p2align 4,,10
	.p2align 3
.L2179:
	movq	(%rcx), %rsi
	movq	%r8, %rdi
	movq	%rcx, -1992(%rbp)
	movq	%r8, -1944(%rbp)
	movq	%rsi, -1984(%rbp)
	call	_ZNK2v88internal12FrameSummary6scriptEv@PLT
	movq	-1944(%rbp), %r8
	movq	-1984(%rbp), %rsi
	movq	(%rax), %rdx
	movq	-1992(%rbp), %rcx
	testb	$1, %dl
	jne	.L2268
.L2149:
	xorl	%edx, %edx
.L2151:
	movslq	99(%rsi), %rax
	movq	%rcx, -1992(%rbp)
	movq	%r8, %rdi
	andl	$-61, %eax
	orl	%edx, %eax
	salq	$32, %rax
	movq	%rax, 95(%rsi)
	movq	%r8, -1984(%rbp)
	call	_ZNK2v88internal12FrameSummary11code_offsetEv@PLT
	movq	-1984(%rbp), %r8
	movl	%eax, -1944(%rbp)
	movq	%r8, %rdi
	call	_ZN2v88internal12FrameSummaryD1Ev@PLT
	movl	-1944(%rbp), %eax
	movq	-1992(%rbp), %rcx
	negl	%eax
	salq	$32, %rax
	jmp	.L2142
	.p2align 4,,10
	.p2align 3
.L2250:
	movq	41088(%r12), %r9
	cmpq	41096(%r12), %r9
	je	.L2269
.L2111:
	leaq	8(%r9), %rax
	movq	%rax, 41088(%r12)
	movq	-1920(%rbp), %rax
	movq	%rax, (%r9)
	jmp	.L2110
	.p2align 4,,10
	.p2align 3
.L2103:
	movq	41088(%r12), %rax
	cmpq	%rax, 41096(%r12)
	je	.L2270
.L2105:
	movq	-1968(%rbp), %rcx
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rcx, (%rax)
	jmp	.L2104
	.p2align 4,,10
	.p2align 3
.L2262:
	movq	23(%rax), %rax
	testb	$1, %al
	je	.L2134
	leaq	-1(%rax), %rdx
	jmp	.L2135
	.p2align 4,,10
	.p2align 3
.L2252:
	movq	-1(%rdx), %rdx
	cmpw	$91, 11(%rdx)
	jne	.L2117
.L2122:
	movq	31(%rbx), %rdx
	testb	$1, %dl
	jne	.L2271
.L2120:
	movq	7(%rbx), %rdx
	testb	$1, %dl
	jne	.L2272
.L2124:
	movq	7(%rbx), %rdx
	movq	7(%rdx), %rsi
.L2123:
	movq	3520(%rax), %rdi
	leaq	-37592(%rax), %r15
	testq	%rdi, %rdi
	je	.L2125
	movq	%r9, -1920(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-1920(%rbp), %r9
	jmp	.L2128
	.p2align 4,,10
	.p2align 3
.L2145:
	xorl	%eax, %eax
	jmp	.L2142
	.p2align 4,,10
	.p2align 3
.L2260:
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%rax, -1912(%rbp)
	call	_ZN2v88internal9ParseInfoD1Ev@PLT
	movq	-1912(%rbp), %rax
	jmp	.L2169
	.p2align 4,,10
	.p2align 3
.L2113:
	movq	41088(%r12), %rax
	cmpq	%rax, 41096(%r12)
	je	.L2273
.L2115:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L2114
	.p2align 4,,10
	.p2align 3
.L2253:
	movq	-1(%rdx), %rcx
	cmpw	$165, 11(%rcx)
	jne	.L2274
.L2131:
	xorl	%ecx, %ecx
	jmp	.L2130
	.p2align 4,,10
	.p2align 3
.L2116:
	movq	-1(%rdx), %rdx
	cmpw	$72, 11(%rdx)
	jne	.L2119
	jmp	.L2122
	.p2align 4,,10
	.p2align 3
.L2251:
	movq	-1(%rsi), %rax
	cmpw	$86, 11(%rax)
	jne	.L2112
	movq	23(%rsi), %rsi
	jmp	.L2112
	.p2align 4,,10
	.p2align 3
.L2254:
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%r9, %rsi
	movq	%r12, %rdi
	movq	%r9, -1920(%rbp)
	call	_ZN2v88internal7Factory33NewFunctionFromSharedFunctionInfoENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal10JSFunction22InitializeFeedbackCellENS0_6HandleIS1_EE@PLT
	movq	-1920(%rbp), %r8
.L2182:
	movq	(%r15), %rax
	movq	41112(%r12), %rdi
	movq	39(%rax), %rsi
	testq	%rdi, %rdi
	je	.L2173
	movq	%r8, -1920(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-1920(%rbp), %r8
	movq	%rax, %r9
.L2174:
	movl	16(%rbp), %eax
	subq	$8, %rsp
	pushq	%rax
	jmp	.L2249
	.p2align 4,,10
	.p2align 3
.L2263:
	subq	$8, %rsp
	leaq	-80(%rbp), %rcx
	movq	-1992(%rbp), %r9
	movq	-1984(%rbp), %r8
	pushq	$2
	movl	$79, %esi
	pushq	%rcx
	leaq	-1520(%rbp), %rcx
	sarq	$32, %r9
	pushq	%rcx
	leaq	-1873(%rbp), %rcx
	pushq	%rcx
	leaq	-1808(%rbp), %rcx
	pushq	%rcx
	leaq	.LC8(%rip), %rcx
	pushq	$1
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L2156
	.p2align 4,,10
	.p2align 3
.L2264:
	leaq	.LC6(%rip), %rsi
	call	*%rax
	movq	-1944(%rbp), %rcx
	movq	%rax, %rdx
	jmp	.L2154
	.p2align 4,,10
	.p2align 3
.L2125:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L2275
.L2127:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%rsi, (%rax)
	jmp	.L2128
	.p2align 4,,10
	.p2align 3
.L2274:
	movq	-1(%rdx), %rdx
	cmpw	$166, 11(%rdx)
	setne	%cl
	jmp	.L2130
	.p2align 4,,10
	.p2align 3
.L2173:
	movq	41088(%r12), %r9
	cmpq	41096(%r12), %r9
	je	.L2276
.L2175:
	leaq	8(%r9), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r9)
	jmp	.L2174
.L2268:
	movq	-1(%rdx), %rax
	cmpw	$96, 11(%rax)
	jne	.L2149
	movl	$12, %eax
	andl	99(%rdx), %eax
	movl	%eax, %edx
	jmp	.L2151
	.p2align 4,,10
	.p2align 3
.L2265:
	movq	%r12, %rdi
	movq	%rsi, -1928(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-1928(%rbp), %rsi
	jmp	.L2167
	.p2align 4,,10
	.p2align 3
.L2271:
	movq	-1(%rdx), %rcx
	cmpw	$86, 11(%rcx)
	jne	.L2120
	movq	39(%rdx), %rcx
	testb	$1, %cl
	je	.L2120
	movq	-1(%rcx), %rcx
	cmpw	$72, 11(%rcx)
	jne	.L2120
	movq	31(%rdx), %rsi
	jmp	.L2123
	.p2align 4,,10
	.p2align 3
.L2266:
	movq	%r12, %rdi
	movq	%rsi, -1920(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-1920(%rbp), %rsi
	movq	%rax, %r9
	jmp	.L2178
	.p2align 4,,10
	.p2align 3
.L2272:
	movq	-1(%rdx), %rdx
	cmpw	$72, 11(%rdx)
	jne	.L2124
	movq	7(%rbx), %rsi
	jmp	.L2123
	.p2align 4,,10
	.p2align 3
.L2273:
	movq	%r12, %rdi
	movq	%rsi, -1928(%rbp)
	movq	%r9, -1920(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-1928(%rbp), %rsi
	movq	-1920(%rbp), %r9
	jmp	.L2115
	.p2align 4,,10
	.p2align 3
.L2270:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L2105
	.p2align 4,,10
	.p2align 3
.L2269:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r9
	jmp	.L2111
.L2275:
	movq	%r15, %rdi
	movq	%rsi, -1928(%rbp)
	movq	%r9, -1920(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-1928(%rbp), %rsi
	movq	-1920(%rbp), %r9
	jmp	.L2127
.L2276:
	movq	%r12, %rdi
	movq	%r8, -1928(%rbp)
	movq	%rsi, -1920(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-1928(%rbp), %r8
	movq	-1920(%rbp), %rsi
	movq	%rax, %r9
	jmp	.L2175
.L2255:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25287:
	.size	_ZN2v88internal8Compiler19GetFunctionFromEvalENS0_6HandleINS0_6StringEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_12LanguageModeENS0_16ParseRestrictionEiii, .-_ZN2v88internal8Compiler19GetFunctionFromEvalENS0_6HandleINS0_6StringEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_12LanguageModeENS0_16ParseRestrictionEiii
	.section	.text._ZN2v88internal8Compiler30GetFunctionFromValidatedStringENS0_6HandleINS0_7ContextEEENS0_11MaybeHandleINS0_6StringEEENS0_16ParseRestrictionEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Compiler30GetFunctionFromValidatedStringENS0_6HandleINS0_7ContextEEENS0_11MaybeHandleINS0_6StringEEENS0_16ParseRestrictionEi
	.type	_ZN2v88internal8Compiler30GetFunctionFromValidatedStringENS0_6HandleINS0_7ContextEEENS0_11MaybeHandleINS0_6StringEEENS0_16ParseRestrictionEi, @function
_ZN2v88internal8Compiler30GetFunctionFromValidatedStringENS0_6HandleINS0_7ContextEEENS0_11MaybeHandleINS0_6StringEEENS0_16ParseRestrictionEi:
.LFB25313:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %r12
	subq	$37592, %r12
	movq	39(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2278
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r13
	testq	%r14, %r14
	je	.L2288
.L2281:
	movq	343(%rsi), %rax
	movq	23(%rax), %r8
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2283
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L2284:
	pushq	$-1
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movl	%r15d, %r9d
	pushq	$0
	movl	%ebx, %r8d
	movq	%r14, %rdi
	call	_ZN2v88internal8Compiler19GetFunctionFromEvalENS0_6HandleINS0_6StringEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_12LanguageModeENS0_16ParseRestrictionEiii
	popq	%rdx
	popq	%rcx
.L2282:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L2289
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2283:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L2290
.L2285:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r8, (%rsi)
	jmp	.L2284
	.p2align 4,,10
	.p2align 3
.L2278:
	movq	41088(%r12), %r13
	cmpq	%r13, 41096(%r12)
	je	.L2291
.L2280:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	testq	%r14, %r14
	jne	.L2281
.L2288:
	leaq	-64(%rbp), %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal7Context40ErrorMessageForCodeGenerationFromStringsEv@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$337, %esi
	movq	%rax, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewEvalErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	xorl	%eax, %eax
	jmp	.L2282
	.p2align 4,,10
	.p2align 3
.L2291:
	movq	%r12, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L2280
	.p2align 4,,10
	.p2align 3
.L2290:
	movq	%r12, %rdi
	movq	%r8, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L2285
.L2289:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25313:
	.size	_ZN2v88internal8Compiler30GetFunctionFromValidatedStringENS0_6HandleINS0_7ContextEEENS0_11MaybeHandleINS0_6StringEEENS0_16ParseRestrictionEi, .-_ZN2v88internal8Compiler30GetFunctionFromValidatedStringENS0_6HandleINS0_7ContextEEENS0_11MaybeHandleINS0_6StringEEENS0_16ParseRestrictionEi
	.section	.text._ZN2v88internal8Compiler21GetFunctionFromStringENS0_6HandleINS0_7ContextEEENS2_INS0_6ObjectEEENS0_16ParseRestrictionEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Compiler21GetFunctionFromStringENS0_6HandleINS0_7ContextEEENS2_INS0_6ObjectEEENS0_16ParseRestrictionEi
	.type	_ZN2v88internal8Compiler21GetFunctionFromStringENS0_6HandleINS0_7ContextEEENS2_INS0_6ObjectEEENS0_16ParseRestrictionEi, @function
_ZN2v88internal8Compiler21GetFunctionFromStringENS0_6HandleINS0_7ContextEEENS2_INS0_6ObjectEEENS0_16ParseRestrictionEi:
.LFB25314:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %r12
	movq	39(%rax), %rsi
	movq	3520(%r12), %rdi
	subq	$37592, %r12
	testq	%rdi, %rdi
	je	.L2293
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L2294:
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rbx, %rdx
	call	_ZN2v88internal8Compiler32ValidateDynamicCompilationSourceEPNS0_7IsolateENS0_6HandleINS0_7ContextEEENS4_INS0_6ObjectEEE
	addq	$24, %rsp
	movl	%r15d, %ecx
	movl	%r14d, %edx
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	movq	%rax, %rsi
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8Compiler30GetFunctionFromValidatedStringENS0_6HandleINS0_7ContextEEENS0_11MaybeHandleINS0_6StringEEENS0_16ParseRestrictionEi
	.p2align 4,,10
	.p2align 3
.L2293:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L2297
.L2295:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L2294
	.p2align 4,,10
	.p2align 3
.L2297:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L2295
	.cfi_endproc
.LFE25314:
	.size	_ZN2v88internal8Compiler21GetFunctionFromStringENS0_6HandleINS0_7ContextEEENS2_INS0_6ObjectEEENS0_16ParseRestrictionEi, .-_ZN2v88internal8Compiler21GetFunctionFromStringENS0_6HandleINS0_7ContextEEENS2_INS0_6ObjectEEENS0_16ParseRestrictionEi
	.section	.text._ZN2v88internal8Compiler7CompileENS0_6HandleINS0_18SharedFunctionInfoEEENS1_18ClearExceptionFlagEPNS0_15IsCompiledScopeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Compiler7CompileENS0_6HandleINS0_18SharedFunctionInfoEEENS1_18ClearExceptionFlagEPNS0_15IsCompiledScopeE
	.type	_ZN2v88internal8Compiler7CompileENS0_6HandleINS0_18SharedFunctionInfoEEENS1_18ClearExceptionFlagEPNS0_15IsCompiledScopeE, @function
_ZN2v88internal8Compiler7CompileENS0_6HandleINS0_18SharedFunctionInfoEEENS1_18ClearExceptionFlagEPNS0_15IsCompiledScopeE:
.LFB25282:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$504, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -520(%rbp)
	movl	$255, %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	leaq	-352(%rbp), %rdi
	andq	$-262144, %rax
	movq	24(%rax), %rbx
	movl	-24976(%rbx), %eax
	subq	$37592, %rbx
	movl	$3, 12616(%rbx)
	movq	%rbx, %rsi
	movl	%eax, -484(%rbp)
	call	_ZN2v88internal15InterruptsScopeC2EPNS0_7IsolateElNS1_4ModeE@PLT
	leaq	16+_ZTVN2v88internal23PostponeInterruptsScopeE(%rip), %rax
	movq	%rax, -352(%rbp)
	movq	41632(%rbx), %rax
	testq	%rax, %rax
	je	.L2300
	leaq	_ZN2v88internal6Logger26DefaultEventLoggerSentinelEPKci(%rip), %rdx
	cmpq	%rdx, %rax
	je	.L2436
	xorl	%esi, %esi
	leaq	.LC34(%rip), %rdi
	call	*%rax
.L2300:
	movq	$0, -368(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2437
.L2303:
	movq	_ZZN2v88internal8Compiler7CompileENS0_6HandleINS0_18SharedFunctionInfoEEENS1_18ClearExceptionFlagEPNS0_15IsCompiledScopeEE29trace_event_unique_atomic1294(%rip), %r14
	testq	%r14, %r14
	je	.L2438
.L2305:
	movq	$0, -432(%rbp)
	movzbl	(%r14), %eax
	testb	$5, %al
	jne	.L2439
.L2307:
	movq	40960(%rbx), %rax
	leaq	-304(%rbp), %r14
	movq	%rax, -504(%rbp)
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	%rax, -496(%rbp)
	call	_ZN2v88internal9ParseInfoC1EPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEE@PLT
	movq	41528(%rbx), %rdi
	movq	%r12, %rsi
	orl	$256, -296(%rbp)
	movq	%rdi, -512(%rbp)
	call	_ZNK2v88internal18CompilerDispatcher10IsEnqueuedENS0_6HandleINS0_18SharedFunctionInfoEEE@PLT
	movl	%eax, %r13d
	testb	%al, %al
	je	.L2311
	movq	-512(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal18CompilerDispatcher9FinishNowENS0_6HandleINS0_18SharedFunctionInfoEEE@PLT
	movl	%eax, %r13d
	testb	%al, %al
	jne	.L2312
	cmpl	$1, %r15d
	je	.L2440
	movq	12480(%rbx), %rax
	cmpq	%rax, 96(%rbx)
	je	.L2315
.L2433:
	xorl	%r13d, %r13d
.L2314:
	movq	%r14, %rdi
	call	_ZN2v88internal9ParseInfoD1Ev@PLT
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	-504(%rbp), %rcx
	subq	-496(%rbp), %rax
	leaq	-432(%rbp), %rdi
	addq	%rax, 5488(%rcx)
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-400(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2441
.L2378:
	movq	41632(%rbx), %rax
	testq	%rax, %rax
	je	.L2380
	leaq	_ZN2v88internal6Logger26DefaultEventLoggerSentinelEPKci(%rip), %rdx
	cmpq	%rdx, %rax
	je	.L2442
	movl	$1, %esi
	leaq	.LC34(%rip), %rdi
	call	*%rax
.L2380:
	leaq	16+_ZTVN2v88internal15InterruptsScopeE(%rip), %rax
	cmpl	$2, -320(%rbp)
	movq	%rax, -352(%rbp)
	je	.L2383
	movq	-344(%rbp), %rdi
	call	_ZN2v88internal10StackGuard18PopInterruptsScopeEv@PLT
.L2383:
	movl	-484(%rbp), %eax
	movl	%eax, 12616(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2443
	leaq	-40(%rbp), %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2311:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	7(%rax), %rax
	testb	$1, %al
	jne	.L2444
.L2335:
	xorl	%ecx, %ecx
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal7parsing8ParseAnyEPNS0_9ParseInfoENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateENS1_29ReportErrorsAndStatisticsModeE@PLT
	testb	%al, %al
	jne	.L2340
	cmpl	$1, %r15d
	je	.L2445
	movq	12480(%rbx), %rax
	cmpq	%rax, 96(%rbx)
	jne	.L2433
	movzbl	-128(%rbp), %r13d
	testb	%r13b, %r13b
	je	.L2343
	movq	-192(%rbp), %rcx
	movq	-224(%rbp), %rdx
	leaq	-128(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal30PendingCompilationErrorHandler12ReportErrorsEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEEPNS0_15AstValueFactoryE@PLT
	jmp	.L2433
	.p2align 4,,10
	.p2align 3
.L2340:
	leaq	-448(%rbp), %r8
	movq	41136(%rbx), %rdx
	leaq	-440(%rbp), %rdi
	movq	%r14, %rsi
	movq	%r8, %rcx
	movq	%r8, -512(%rbp)
	movq	$0, -448(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_123GenerateUnoptimizedCodeEPNS0_9ParseInfoEPNS0_19AccountingAllocatorEPSt12forward_listISt10unique_ptrINS0_25UnoptimizedCompilationJobESt14default_deleteIS8_EESaISB_EE
	cmpq	$0, -440(%rbp)
	movq	-512(%rbp), %r8
	je	.L2446
	movq	-192(%rbp), %rdi
	movq	%rbx, %rsi
	movq	%r8, -528(%rbp)
	call	_ZN2v88internal15AstValueFactory11InternalizeEPNS0_7IsolateE@PLT
	movq	-440(%rbp), %r9
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	%r9, -512(%rbp)
	call	_ZN2v88internal16DeclarationScope18AllocateScopeInfosEPNS0_9ParseInfoEPNS0_7IsolateE@PLT
	movq	-512(%rbp), %r9
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r9, %rdi
	call	_ZN2v88internal12_GLOBAL__N_133FinalizeUnoptimizedCompilationJobEPNS0_25UnoptimizedCompilationJobENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateE
	testl	%eax, %eax
	jne	.L2350
	movq	-528(%rbp), %r8
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	%r8, %rdx
	call	_ZN2v88internal12_GLOBAL__N_123FinalizeUnoptimizedCodeEPNS0_9ParseInfoEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_25UnoptimizedCompilationJobEPSt12forward_listISt10unique_ptrIS9_St14default_deleteIS9_EESaISF_EE.part.0
	movb	%al, -512(%rbp)
	testb	%al, %al
	je	.L2350
	movq	(%r12), %r13
	movq	%r13, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	7(%r13), %rdx
	testb	$1, %dl
	jne	.L2354
.L2357:
	movq	7(%r13), %rdx
	testb	$1, %dl
	jne	.L2447
.L2355:
	xorl	%eax, %eax
.L2366:
	movabsq	$287762808832, %rcx
	movq	7(%r13), %rdx
	cmpq	%rcx, %rdx
	je	.L2369
	movl	$1, %ecx
	testb	$1, %dl
	jne	.L2448
.L2368:
	xorl	%edx, %edx
	movq	%rax, -480(%rbp)
	movb	%cl, %dl
	movq	-520(%rbp), %rcx
	movq	%rdx, -472(%rbp)
	movq	%rax, (%rcx)
	cmpb	$0, _ZN2v88internal33FLAG_stress_lazy_source_positionsE(%rip)
	movb	%dl, 8(%rcx)
	jne	.L2370
.L2371:
	movzbl	-512(%rbp), %r13d
.L2348:
	movq	-440(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2373
	movq	(%rdi), %rax
	call	*8(%rax)
.L2373:
	movq	-448(%rbp), %r12
	testq	%r12, %r12
	jne	.L2374
	jmp	.L2314
	.p2align 4,,10
	.p2align 3
.L2449:
	movq	(%rdi), %rdx
	call	*8(%rdx)
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r15, %r15
	je	.L2314
.L2377:
	movq	%r15, %r12
.L2374:
	movq	8(%r12), %rdi
	movq	(%r12), %r15
	testq	%rdi, %rdi
	jne	.L2449
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r15, %r15
	jne	.L2377
	jmp	.L2314
	.p2align 4,,10
	.p2align 3
.L2439:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2450
.L2308:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2309
	movq	(%rdi), %rax
	call	*8(%rax)
.L2309:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2310
	movq	(%rdi), %rax
	call	*8(%rax)
.L2310:
	leaq	.LC34(%rip), %rax
	movq	%r14, -424(%rbp)
	movq	%rax, -416(%rbp)
	leaq	-424(%rbp), %rax
	movq	%r13, -408(%rbp)
	movq	%rax, -432(%rbp)
	jmp	.L2307
	.p2align 4,,10
	.p2align 3
.L2438:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r14
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2451
.L2306:
	movq	%r14, _ZZN2v88internal8Compiler7CompileENS0_6HandleINS0_18SharedFunctionInfoEEENS1_18ClearExceptionFlagEPNS0_15IsCompiledScopeEE29trace_event_unique_atomic1294(%rip)
	jmp	.L2305
	.p2align 4,,10
	.p2align 3
.L2312:
	movq	(%r12), %r12
	movq	%r12, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	7(%r12), %rdx
	testb	$1, %dl
	jne	.L2317
.L2320:
	movq	7(%r12), %rdx
	testb	$1, %dl
	jne	.L2452
.L2318:
	xorl	%eax, %eax
.L2329:
	movabsq	$287762808832, %rcx
	movq	7(%r12), %rdx
	cmpq	%rcx, %rdx
	je	.L2332
	movl	$1, %ecx
	testb	$1, %dl
	jne	.L2453
.L2331:
	xorl	%edx, %edx
	movq	%rax, -464(%rbp)
	movb	%cl, %dl
	movq	-520(%rbp), %rcx
	movq	%rdx, -456(%rbp)
	movq	%rax, (%rcx)
	movb	%dl, 8(%rcx)
	jmp	.L2314
	.p2align 4,,10
	.p2align 3
.L2444:
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L2335
	movq	(%r12), %rax
	movq	7(%rax), %rax
	movq	41112(%rbx), %rdi
	movq	23(%rax), %rsi
	testq	%rdi, %rdi
	je	.L2336
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L2337:
	leaq	-440(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal20ConsumedPreparseData3ForEPNS0_7IsolateENS0_6HandleINS0_12PreparseDataEEE@PLT
	movq	-200(%rbp), %rdi
	movq	-440(%rbp), %rax
	movq	%rdi, -440(%rbp)
	movq	%rax, -200(%rbp)
	testq	%rdi, %rdi
	je	.L2335
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L2335
	.p2align 4,,10
	.p2align 3
.L2445:
	movq	96(%rbx), %rax
	xorl	%r13d, %r13d
	movq	%rax, 12480(%rbx)
	jmp	.L2314
	.p2align 4,,10
	.p2align 3
.L2446:
	cmpl	$1, %r15d
	je	.L2454
	movq	12480(%rbx), %rax
	cmpq	%rax, 96(%rbx)
	jne	.L2373
.L2435:
	cmpb	$0, -128(%rbp)
	je	.L2353
	movq	-192(%rbp), %rcx
	movq	-224(%rbp), %rdx
	leaq	-128(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal30PendingCompilationErrorHandler12ReportErrorsEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEEPNS0_15AstValueFactoryE@PLT
	jmp	.L2348
	.p2align 4,,10
	.p2align 3
.L2315:
	cmpb	$0, -128(%rbp)
	je	.L2343
	movq	-192(%rbp), %rcx
	movq	-224(%rbp), %rdx
	leaq	-128(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal30PendingCompilationErrorHandler12ReportErrorsEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEEPNS0_15AstValueFactoryE@PLT
	jmp	.L2314
	.p2align 4,,10
	.p2align 3
.L2343:
	movq	%rbx, %rdi
	call	_ZN2v88internal7Isolate13StackOverflowEv@PLT
	jmp	.L2314
	.p2align 4,,10
	.p2align 3
.L2436:
	movq	41016(%rbx), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	je	.L2300
	leaq	.LC34(%rip), %rdx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal6Logger10TimerEventENS1_8StartEndEPKc@PLT
	jmp	.L2300
	.p2align 4,,10
	.p2align 3
.L2442:
	movq	41016(%rbx), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	je	.L2380
	leaq	.LC34(%rip), %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal6Logger10TimerEventENS1_8StartEndEPKc@PLT
	jmp	.L2380
	.p2align 4,,10
	.p2align 3
.L2440:
	movq	96(%rbx), %rax
	movq	%rax, 12480(%rbx)
	jmp	.L2314
	.p2align 4,,10
	.p2align 3
.L2437:
	movq	40960(%rbx), %rax
	leaq	-392(%rbp), %rsi
	movl	$131, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -400(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2303
	.p2align 4,,10
	.p2align 3
.L2441:
	leaq	-392(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2378
	.p2align 4,,10
	.p2align 3
.L2350:
	cmpl	$1, %r15d
	je	.L2455
	movq	12480(%rbx), %rax
	cmpq	%rax, 96(%rbx)
	jne	.L2348
	jmp	.L2435
	.p2align 4,,10
	.p2align 3
.L2336:
	movq	41088(%rbx), %rdx
	cmpq	41096(%rbx), %rdx
	je	.L2456
.L2338:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rdx)
	jmp	.L2337
	.p2align 4,,10
	.p2align 3
.L2332:
	xorl	%ecx, %ecx
	jmp	.L2331
	.p2align 4,,10
	.p2align 3
.L2450:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC34(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r14, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L2308
	.p2align 4,,10
	.p2align 3
.L2451:
	leaq	.LC6(%rip), %rsi
	call	*%rax
	movq	%rax, %r14
	jmp	.L2306
	.p2align 4,,10
	.p2align 3
.L2317:
	movq	-1(%rdx), %rdx
	cmpw	$72, 11(%rdx)
	jne	.L2320
.L2323:
	movq	31(%r12), %rdx
	testb	$1, %dl
	jne	.L2457
.L2321:
	movq	7(%r12), %rdx
	testb	$1, %dl
	jne	.L2458
.L2325:
	movq	7(%r12), %rdx
	movq	7(%rdx), %rsi
.L2324:
	movq	3520(%rax), %rdi
	leaq	-37592(%rax), %rdx
	testq	%rdi, %rdi
	je	.L2326
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	jmp	.L2329
	.p2align 4,,10
	.p2align 3
.L2452:
	movq	-1(%rdx), %rdx
	cmpw	$91, 11(%rdx)
	jne	.L2318
	jmp	.L2323
	.p2align 4,,10
	.p2align 3
.L2326:
	movq	41088(%rdx), %rax
	cmpq	41096(%rdx), %rax
	je	.L2459
.L2328:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%rdx)
	movq	%rsi, (%rax)
	jmp	.L2329
	.p2align 4,,10
	.p2align 3
.L2447:
	movq	-1(%rdx), %rdx
	cmpw	$91, 11(%rdx)
	jne	.L2355
.L2360:
	movq	31(%r13), %rdx
	testb	$1, %dl
	jne	.L2460
.L2358:
	movq	7(%r13), %rdx
	testb	$1, %dl
	jne	.L2461
.L2362:
	movq	7(%r13), %rdx
	movq	7(%rdx), %rsi
.L2361:
	movq	3520(%rax), %rdi
	leaq	-37592(%rax), %rdx
	testq	%rdi, %rdi
	je	.L2363
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	jmp	.L2366
	.p2align 4,,10
	.p2align 3
.L2454:
	movq	96(%rbx), %rax
	movq	%rax, 12480(%rbx)
	jmp	.L2373
	.p2align 4,,10
	.p2align 3
.L2448:
	movq	-1(%rdx), %rcx
	cmpw	$165, 11(%rcx)
	je	.L2369
	movq	-1(%rdx), %rdx
	cmpw	$166, 11(%rdx)
	setne	%cl
	jmp	.L2368
	.p2align 4,,10
	.p2align 3
.L2457:
	movq	-1(%rdx), %rcx
	cmpw	$86, 11(%rcx)
	jne	.L2321
	movq	39(%rdx), %rcx
	testb	$1, %cl
	je	.L2321
	movq	-1(%rcx), %rcx
	cmpw	$72, 11(%rcx)
	jne	.L2321
	movq	31(%rdx), %rsi
	jmp	.L2324
	.p2align 4,,10
	.p2align 3
.L2369:
	xorl	%ecx, %ecx
	jmp	.L2368
	.p2align 4,,10
	.p2align 3
.L2453:
	movq	-1(%rdx), %rcx
	cmpw	$165, 11(%rcx)
	je	.L2332
	movq	-1(%rdx), %rdx
	cmpw	$166, 11(%rdx)
	setne	%cl
	jmp	.L2331
	.p2align 4,,10
	.p2align 3
.L2455:
	movq	96(%rbx), %rax
	movq	%rax, 12480(%rbx)
	jmp	.L2348
	.p2align 4,,10
	.p2align 3
.L2354:
	movq	-1(%rdx), %rdx
	cmpw	$72, 11(%rdx)
	jne	.L2357
	jmp	.L2360
	.p2align 4,,10
	.p2align 3
.L2370:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal18SharedFunctionInfo30EnsureSourcePositionsAvailableEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	-448(%rbp), %r12
	testq	%r12, %r12
	je	.L2371
	.p2align 4,,10
	.p2align 3
.L2372:
	movq	8(%r12), %rax
	movq	-224(%rbp), %rsi
	movq	%rbx, %rdx
	movq	40(%rax), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8Compiler21GetSharedFunctionInfoEPNS0_15FunctionLiteralENS0_6HandleINS0_6ScriptEEEPNS0_7IsolateE
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal18SharedFunctionInfo30EnsureSourcePositionsAvailableEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%r12), %r12
	testq	%r12, %r12
	jne	.L2372
	jmp	.L2371
	.p2align 4,,10
	.p2align 3
.L2353:
	movq	%rbx, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal7Isolate13StackOverflowEv@PLT
	jmp	.L2348
	.p2align 4,,10
	.p2align 3
.L2458:
	movq	-1(%rdx), %rdx
	cmpw	$72, 11(%rdx)
	jne	.L2325
	movq	7(%r12), %rsi
	jmp	.L2324
	.p2align 4,,10
	.p2align 3
.L2456:
	movq	%rbx, %rdi
	movq	%rsi, -512(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-512(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L2338
	.p2align 4,,10
	.p2align 3
.L2363:
	movq	41088(%rdx), %rax
	cmpq	41096(%rdx), %rax
	je	.L2462
.L2365:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%rdx)
	movq	%rsi, (%rax)
	jmp	.L2366
.L2460:
	movq	-1(%rdx), %rcx
	cmpw	$86, 11(%rcx)
	jne	.L2358
	movq	39(%rdx), %rcx
	testb	$1, %cl
	je	.L2358
	movq	-1(%rcx), %rcx
	cmpw	$72, 11(%rcx)
	jne	.L2358
	movq	31(%rdx), %rsi
	jmp	.L2361
	.p2align 4,,10
	.p2align 3
.L2461:
	movq	-1(%rdx), %rdx
	cmpw	$72, 11(%rdx)
	jne	.L2362
	movq	7(%r13), %rsi
	jmp	.L2361
.L2459:
	movq	%rdx, %rdi
	movq	%rsi, -528(%rbp)
	movq	%rdx, -512(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-528(%rbp), %rsi
	movq	-512(%rbp), %rdx
	jmp	.L2328
.L2462:
	movq	%rdx, %rdi
	movq	%rsi, -536(%rbp)
	movq	%rdx, -528(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-536(%rbp), %rsi
	movq	-528(%rbp), %rdx
	jmp	.L2365
.L2443:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25282:
	.size	_ZN2v88internal8Compiler7CompileENS0_6HandleINS0_18SharedFunctionInfoEEENS1_18ClearExceptionFlagEPNS0_15IsCompiledScopeE, .-_ZN2v88internal8Compiler7CompileENS0_6HandleINS0_18SharedFunctionInfoEEENS1_18ClearExceptionFlagEPNS0_15IsCompiledScopeE
	.section	.rodata._ZN2v88internal8Compiler7CompileENS0_6HandleINS0_10JSFunctionEEENS1_18ClearExceptionFlagEPNS0_15IsCompiledScopeE.str1.1,"aMS",@progbits,1
.LC36:
	.string	" because --always-opt]\n"
	.section	.text._ZN2v88internal8Compiler7CompileENS0_6HandleINS0_10JSFunctionEEENS1_18ClearExceptionFlagEPNS0_15IsCompiledScopeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Compiler7CompileENS0_6HandleINS0_10JSFunctionEEENS1_18ClearExceptionFlagEPNS0_15IsCompiledScopeE
	.type	_ZN2v88internal8Compiler7CompileENS0_6HandleINS0_10JSFunctionEEENS1_18ClearExceptionFlagEPNS0_15IsCompiledScopeE, @function
_ZN2v88internal8Compiler7CompileENS0_6HandleINS0_10JSFunctionEEENS1_18ClearExceptionFlagEPNS0_15IsCompiledScopeE:
.LFB25283:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal19FLAG_flush_bytecodeE(%rip)
	leaq	23(%rbx), %rax
	jne	.L2538
.L2464:
	andq	$-262144, %rbx
	movq	24(%rbx), %rbx
	movq	(%rax), %rsi
	movq	3520(%rbx), %rdi
	subq	$37592, %rbx
	testq	%rdi, %rdi
	je	.L2482
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r14
.L2483:
	movq	%rsi, %rax
	leaq	7(%rsi), %rdx
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	7(%rsi), %rcx
	testb	$1, %cl
	jne	.L2485
.L2488:
	movq	(%rdx), %rcx
	testb	$1, %cl
	jne	.L2539
.L2486:
	xorl	%eax, %eax
.L2497:
	movabsq	$287762808832, %rcx
	movq	(%rdx), %rdx
	cmpq	%rcx, %rdx
	je	.L2500
	movl	$1, %ecx
	testb	$1, %dl
	jne	.L2540
.L2499:
	xorl	%edx, %edx
	movq	%rax, -80(%rbp)
	movb	%cl, %dl
	movq	%rax, (%r15)
	movq	%rdx, -72(%rbp)
	movb	%dl, 8(%r15)
	testb	%dl, %dl
	jne	.L2504
	movq	%r15, %rdx
	movl	%r13d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8Compiler7CompileENS0_6HandleINS0_18SharedFunctionInfoEEENS1_18ClearExceptionFlagEPNS0_15IsCompiledScopeE
	testb	%al, %al
	je	.L2463
.L2504:
	movq	(%r14), %rax
	leaq	-64(%rbp), %r14
	movq	%r14, %rdi
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo7GetCodeEv@PLT
	movq	41112(%rbx), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L2541
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L2506:
	movq	%r12, %rdi
	call	_ZN2v88internal10JSFunction22InitializeFeedbackCellENS0_6HandleIS1_EE@PLT
	cmpb	$0, _ZN2v88internal15FLAG_always_optE(%rip)
	jne	.L2542
.L2509:
	movq	(%r12), %rdi
	movq	0(%r13), %rdx
	movq	%rdx, 47(%rdi)
	leaq	47(%rdi), %rsi
	testb	$1, %dl
	je	.L2514
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L2514
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
.L2514:
	movl	$1, %eax
.L2463:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2543
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2541:
	.cfi_restore_state
	movq	41088(%rbx), %r13
	cmpq	41096(%rbx), %r13
	je	.L2544
.L2507:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, 0(%r13)
	jmp	.L2506
	.p2align 4,,10
	.p2align 3
.L2539:
	movq	-1(%rcx), %rcx
	cmpw	$91, 11(%rcx)
	jne	.L2486
	movq	31(%rsi), %rcx
	testb	$1, %cl
	jne	.L2545
.L2489:
	movq	(%rdx), %rcx
	testb	$1, %cl
	jne	.L2546
.L2493:
	movq	(%rdx), %rcx
	movq	7(%rcx), %rsi
.L2492:
	movq	3520(%rax), %rdi
	leaq	-37592(%rax), %rcx
	testq	%rdi, %rdi
	je	.L2494
	movq	%rdx, -88(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-88(%rbp), %rdx
	jmp	.L2497
	.p2align 4,,10
	.p2align 3
.L2540:
	movq	-1(%rdx), %rcx
	cmpw	$165, 11(%rcx)
	jne	.L2547
.L2500:
	xorl	%ecx, %ecx
	jmp	.L2499
	.p2align 4,,10
	.p2align 3
.L2482:
	movq	41088(%rbx), %r14
	cmpq	%r14, 41096(%rbx)
	je	.L2548
.L2484:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r14)
	jmp	.L2483
	.p2align 4,,10
	.p2align 3
.L2538:
	movq	23(%rbx), %rax
	leaq	47(%rbx), %r14
	movq	47(%rbx), %rdx
	testb	$1, %al
	jne	.L2465
.L2537:
	movq	(%r12), %rbx
	leaq	23(%rbx), %rax
	jmp	.L2464
	.p2align 4,,10
	.p2align 3
.L2485:
	movq	-1(%rcx), %rcx
	cmpw	$72, 11(%rcx)
	jne	.L2488
	movq	31(%rsi), %rcx
	testb	$1, %cl
	je	.L2489
.L2545:
	movq	-1(%rcx), %rsi
	cmpw	$86, 11(%rsi)
	jne	.L2489
	movq	39(%rcx), %rsi
	testb	$1, %sil
	je	.L2489
	movq	-1(%rsi), %rsi
	cmpw	$72, 11(%rsi)
	jne	.L2489
	movq	31(%rcx), %rsi
	jmp	.L2492
	.p2align 4,,10
	.p2align 3
.L2542:
	movq	(%r12), %rax
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	testb	$1, %al
	jne	.L2549
.L2510:
	cmpb	$0, _ZN2v88internal14FLAG_trace_optE(%rip)
	jne	.L2550
.L2512:
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movl	$-1, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_116GetOptimizedCodeENS0_6HandleINS0_10JSFunctionEEENS0_15ConcurrencyModeENS0_9BailoutIdEPNS0_15JavaScriptFrameE
	testq	%rax, %rax
	cmovne	%rax, %r13
	jmp	.L2509
	.p2align 4,,10
	.p2align 3
.L2494:
	movq	41088(%rcx), %rax
	cmpq	41096(%rcx), %rax
	je	.L2551
.L2496:
	leaq	8(%rax), %rdi
	movq	%rdi, 41088(%rcx)
	movq	%rsi, (%rax)
	jmp	.L2497
	.p2align 4,,10
	.p2align 3
.L2547:
	movq	-1(%rdx), %rdx
	cmpw	$166, 11(%rdx)
	setne	%cl
	jmp	.L2499
	.p2align 4,,10
	.p2align 3
.L2549:
	movq	-1(%rax), %rax
	cmpw	$83, 11(%rax)
	jne	.L2510
	jmp	.L2509
	.p2align 4,,10
	.p2align 3
.L2465:
	movq	-1(%rax), %rcx
	cmpw	$160, 11(%rcx)
	jne	.L2537
	testb	$1, %dl
	je	.L2537
	movq	-1(%rdx), %rcx
	cmpw	$69, 11(%rcx)
	jne	.L2537
	movabsq	$287762808832, %rcx
	movq	7(%rax), %rax
	cmpq	%rcx, %rax
	je	.L2473
	testb	$1, %al
	je	.L2537
	movq	-1(%rax), %rcx
	cmpw	$165, 11(%rcx)
	je	.L2473
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L2537
.L2473:
	cmpl	$67, 59(%rdx)
	je	.L2537
	movq	%rbx, %rax
	movl	$67, %esi
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	addq	$3592, %rdi
	call	_ZN2v88internal8Builtins7builtinEi@PLT
	movq	%rax, (%r14)
	movq	%rax, %rdx
	testb	$1, %al
	je	.L2474
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L2474
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
.L2474:
	movq	39(%rbx), %rbx
	movq	%rbx, %rax
	leaq	7(%rbx), %rsi
	andq	$-262144, %rax
	movq	24(%rax), %rdx
	movq	-37504(%rdx), %r14
	movq	%r14, 7(%rbx)
	testb	$1, %r14b
	je	.L2516
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rdx
	movq	%rcx, -88(%rbp)
	testl	$262144, %edx
	je	.L2478
	movq	%r14, %rdx
	movq	%rbx, %rdi
	movq	%rax, -104(%rbp)
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rcx
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %rsi
	movq	8(%rcx), %rdx
.L2478:
	andl	$24, %edx
	je	.L2516
	testb	$24, 8(%rax)
	jne	.L2516
	movq	%r14, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L2516:
	cmpb	$0, _ZN2v88internal29FLAG_lazy_feedback_allocationE(%rip)
	movl	_ZN2v88internal21FLAG_interrupt_budgetE(%rip), %eax
	je	.L2481
	movl	_ZN2v88internal42FLAG_budget_for_feedback_vector_allocationE(%rip), %eax
.L2481:
	movl	%eax, 15(%rbx)
	jmp	.L2537
	.p2align 4,,10
	.p2align 3
.L2546:
	movq	-1(%rcx), %rcx
	cmpw	$72, 11(%rcx)
	jne	.L2493
	movq	(%rdx), %rsi
	jmp	.L2492
	.p2align 4,,10
	.p2align 3
.L2548:
	movq	%rbx, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L2484
	.p2align 4,,10
	.p2align 3
.L2544:
	movq	%rbx, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L2507
	.p2align 4,,10
	.p2align 3
.L2550:
	xorl	%eax, %eax
	leaq	.LC13(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	(%r12), %rax
	movq	stdout(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal6Object10ShortPrintEP8_IO_FILE@PLT
	leaq	.LC36(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L2512
	.p2align 4,,10
	.p2align 3
.L2551:
	movq	%rcx, %rdi
	movq	%rsi, -104(%rbp)
	movq	%rdx, -96(%rbp)
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rsi
	movq	-96(%rbp), %rdx
	movq	-88(%rbp), %rcx
	jmp	.L2496
.L2543:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25283:
	.size	_ZN2v88internal8Compiler7CompileENS0_6HandleINS0_10JSFunctionEEENS1_18ClearExceptionFlagEPNS0_15IsCompiledScopeE, .-_ZN2v88internal8Compiler7CompileENS0_6HandleINS0_10JSFunctionEEENS1_18ClearExceptionFlagEPNS0_15IsCompiledScopeE
	.section	.text._ZN2v88internal8Compiler22GetOptimizedCodeForOSRENS0_6HandleINS0_10JSFunctionEEENS0_9BailoutIdEPNS0_15JavaScriptFrameE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Compiler22GetOptimizedCodeForOSRENS0_6HandleINS0_10JSFunctionEEENS0_9BailoutIdEPNS0_15JavaScriptFrameE
	.type	_ZN2v88internal8Compiler22GetOptimizedCodeForOSRENS0_6HandleINS0_10JSFunctionEEENS0_9BailoutIdEPNS0_15JavaScriptFrameE, @function
_ZN2v88internal8Compiler22GetOptimizedCodeForOSRENS0_6HandleINS0_10JSFunctionEEENS0_9BailoutIdEPNS0_15JavaScriptFrameE:
.LFB25332:
	.cfi_startproc
	endbr64
	movq	%rdx, %rcx
	movl	%esi, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal12_GLOBAL__N_116GetOptimizedCodeENS0_6HandleINS0_10JSFunctionEEENS0_15ConcurrencyModeENS0_9BailoutIdEPNS0_15JavaScriptFrameE
	.cfi_endproc
.LFE25332:
	.size	_ZN2v88internal8Compiler22GetOptimizedCodeForOSRENS0_6HandleINS0_10JSFunctionEEENS0_9BailoutIdEPNS0_15JavaScriptFrameE, .-_ZN2v88internal8Compiler22GetOptimizedCodeForOSRENS0_6HandleINS0_10JSFunctionEEENS0_9BailoutIdEPNS0_15JavaScriptFrameE
	.section	.rodata._ZN2v88internal8Compiler31FinalizeOptimizedCompilationJobEPNS0_23OptimizedCompilationJobEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC37:
	.string	"[completed optimizing "
	.section	.text._ZN2v88internal8Compiler31FinalizeOptimizedCompilationJobEPNS0_23OptimizedCompilationJobEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Compiler31FinalizeOptimizedCompilationJobEPNS0_23OptimizedCompilationJobEPNS0_7IsolateE
	.type	_ZN2v88internal8Compiler31FinalizeOptimizedCompilationJobEPNS0_23OptimizedCompilationJobEPNS0_7IsolateE, @function
_ZN2v88internal8Compiler31FinalizeOptimizedCompilationJobEPNS0_23OptimizedCompilationJobEPNS0_7IsolateE:
.LFB25333:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$152, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	12616(%rsi), %eax
	movl	$4, 12616(%rsi)
	movq	24(%rdi), %r14
	movl	%eax, -180(%rbp)
	movq	41632(%rsi), %rax
	testq	%rax, %rax
	je	.L2555
	leaq	_ZN2v88internal6Logger26DefaultEventLoggerSentinelEPKci(%rip), %rdx
	cmpq	%rdx, %rax
	je	.L2624
	xorl	%esi, %esi
	leaq	.LC23(%rip), %rdi
	call	*%rax
.L2555:
	pxor	%xmm0, %xmm0
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2625
.L2558:
	movq	_ZZN2v88internal8Compiler31FinalizeOptimizedCompilationJobEPNS0_23OptimizedCompilationJobEPNS0_7IsolateEE29trace_event_unique_atomic2284(%rip), %r15
	testq	%r15, %r15
	je	.L2626
.L2560:
	movq	$0, -160(%rbp)
	movzbl	(%r15), %eax
	testb	$5, %al
	jne	.L2627
.L2562:
	movq	32(%r14), %rax
	movq	24(%r14), %rdx
	leaq	-176(%rbp), %r13
	movq	(%rax), %rax
	movq	39(%rax), %rax
	movq	7(%rax), %rax
	movl	$0, 39(%rax)
	cmpl	$2, 8(%r12)
	je	.L2628
.L2566:
	cmpb	$0, _ZN2v88internal14FLAG_trace_optE(%rip)
	jne	.L2629
.L2574:
	movq	32(%r14), %rax
	movq	%r13, %rdi
	movq	(%rax), %r15
	movq	(%rdx), %rax
	movq	%rax, -176(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo7GetCodeEv@PLT
	leaq	47(%r15), %rsi
	movq	%rax, 47(%r15)
	movq	%rax, %rdx
	testb	$1, %al
	je	.L2575
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	jne	.L2630
.L2575:
	movabsq	$287762808832, %rcx
	movq	32(%r14), %rax
	movq	(%rax), %rdx
	movq	23(%rdx), %rax
	movq	7(%rax), %rax
	cmpq	%rcx, %rax
	je	.L2623
	testb	$1, %al
	jne	.L2579
.L2583:
	movq	39(%rdx), %rax
	movq	7(%rax), %rax
	movq	-1(%rax), %rdx
	cmpw	$155, 11(%rdx)
	je	.L2631
.L2623:
	movl	$1, %r13d
.L2573:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2632
.L2586:
	movq	41632(%rbx), %rax
	testq	%rax, %rax
	je	.L2588
	leaq	_ZN2v88internal6Logger26DefaultEventLoggerSentinelEPKci(%rip), %rdx
	cmpq	%rdx, %rax
	je	.L2633
	movl	$1, %esi
	leaq	.LC23(%rip), %rdi
	call	*%rax
.L2588:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
	movl	-180(%rbp), %eax
	movl	%eax, 12616(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2634
	leaq	-40(%rbp), %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2630:
	.cfi_restore_state
	movq	%r15, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2575
	.p2align 4,,10
	.p2align 3
.L2579:
	movq	-1(%rax), %rcx
	cmpw	$165, 11(%rcx)
	je	.L2623
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L2583
	jmp	.L2623
	.p2align 4,,10
	.p2align 3
.L2631:
	movq	15(%rax), %rax
	testb	$1, %al
	jne	.L2623
	sarq	$32, %rax
	cmpq	$4, %rax
	jne	.L2623
	movq	32(%r14), %rax
	movq	%r13, %rdi
	movq	(%rax), %rax
	movq	39(%rax), %rax
	movq	7(%rax), %rax
	movq	%rax, -176(%rbp)
	call	_ZN2v88internal14FeedbackVector23ClearOptimizationMarkerEv@PLT
	jmp	.L2623
	.p2align 4,,10
	.p2align 3
.L2627:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2635
.L2563:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2564
	movq	(%rdi), %rax
	call	*8(%rax)
.L2564:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2565
	movq	(%rdi), %rax
	call	*8(%rax)
.L2565:
	leaq	.LC23(%rip), %rax
	movq	%r15, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r13, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L2562
	.p2align 4,,10
	.p2align 3
.L2626:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r15
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2636
.L2561:
	movq	%r15, _ZZN2v88internal8Compiler31FinalizeOptimizedCompilationJobEPNS0_23OptimizedCompilationJobEPNS0_7IsolateEE29trace_event_unique_atomic2284(%rip)
	jmp	.L2560
	.p2align 4,,10
	.p2align 3
.L2629:
	leaq	.LC26(%rip), %rdi
	xorl	%eax, %eax
	movq	%rdx, -192(%rbp)
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	32(%r14), %rax
	movq	stdout(%rip), %rsi
	movq	%r13, %rdi
	movq	(%rax), %rax
	movq	%rax, -176(%rbp)
	call	_ZNK2v88internal6Object10ShortPrintEP8_IO_FILE@PLT
	movzbl	88(%r14), %edi
	call	_ZN2v88internal16GetBailoutReasonENS0_13BailoutReasonE@PLT
	leaq	.LC27(%rip), %rdi
	movq	%rax, %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-192(%rbp), %rdx
	jmp	.L2574
	.p2align 4,,10
	.p2align 3
.L2628:
	movq	(%rdx), %rax
	movl	47(%rax), %eax
	movq	%rdx, -192(%rbp)
	testl	$15728640, %eax
	jne	.L2637
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb0EEC1EPNS0_7IsolateE@PLT
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %r15
	movq	(%r12), %rax
	call	*32(%rax)
	movq	-192(%rbp), %rdx
	testl	%eax, %eax
	jne	.L2568
	movl	$3, 8(%r12)
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	%r13, %rdi
	subq	%r15, %rax
	addq	%rax, 48(%r12)
	call	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb0EED1Ev@PLT
	xorl	%esi, %esi
	movq	%rbx, %rdx
	movq	%r12, %rdi
	call	_ZNK2v88internal23OptimizedCompilationJob22RecordCompilationStatsENS1_15CompilationModeEPNS0_7IsolateE
	movq	%rbx, %rdx
	movl	$15, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal23OptimizedCompilationJob25RecordFunctionCompilationENS0_17CodeEventListener16LogEventsAndTagsEPNS0_7IsolateE
	movq	%r14, %rdi
	call	_ZN2v88internal12_GLOBAL__N_132InsertCodeIntoOptimizedCodeCacheEPNS0_24OptimizedCompilationInfoE
	cmpb	$0, _ZN2v88internal14FLAG_trace_optE(%rip)
	jne	.L2638
.L2570:
	movq	32(%r14), %rax
	movq	(%rax), %rdi
	movq	40(%r14), %rax
	movq	(%rax), %rdx
	leaq	47(%rdi), %rsi
	movq	%rdx, 47(%rdi)
	testb	$1, %dl
	je	.L2571
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L2571
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
.L2571:
	xorl	%r13d, %r13d
	jmp	.L2573
	.p2align 4,,10
	.p2align 3
.L2637:
	movq	24(%r12), %rdi
	movl	$10, %esi
	call	_ZN2v88internal24OptimizedCompilationInfo17RetryOptimizationENS0_13BailoutReasonE@PLT
	movq	-192(%rbp), %rdx
	movl	$4, 8(%r12)
	jmp	.L2566
	.p2align 4,,10
	.p2align 3
.L2633:
	movq	41016(%rbx), %r15
	movq	%r15, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	je	.L2588
	leaq	.LC23(%rip), %rdx
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal6Logger10TimerEventENS1_8StartEndEPKc@PLT
	jmp	.L2588
	.p2align 4,,10
	.p2align 3
.L2624:
	movq	41016(%rsi), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	je	.L2555
	leaq	.LC23(%rip), %rdx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal6Logger10TimerEventENS1_8StartEndEPKc@PLT
	jmp	.L2555
	.p2align 4,,10
	.p2align 3
.L2625:
	movq	40960(%rbx), %rax
	leaq	-120(%rbp), %rsi
	movl	$196, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2558
	.p2align 4,,10
	.p2align 3
.L2632:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2586
	.p2align 4,,10
	.p2align 3
.L2636:
	leaq	.LC6(%rip), %rsi
	call	*%rax
	movq	%rax, %r15
	jmp	.L2561
	.p2align 4,,10
	.p2align 3
.L2635:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC23(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r15, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L2563
	.p2align 4,,10
	.p2align 3
.L2568:
	movl	$4, 8(%r12)
	movq	%rdx, -192(%rbp)
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	%r13, %rdi
	subq	%r15, %rax
	addq	%rax, 48(%r12)
	call	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb0EED1Ev@PLT
	movq	-192(%rbp), %rdx
	jmp	.L2566
	.p2align 4,,10
	.p2align 3
.L2638:
	xorl	%eax, %eax
	leaq	.LC37(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	32(%r14), %rax
	movq	stdout(%rip), %rsi
	movq	%r13, %rdi
	movq	(%rax), %rax
	movq	%rax, -176(%rbp)
	call	_ZNK2v88internal6Object10ShortPrintEP8_IO_FILE@PLT
	leaq	.LC19(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L2570
.L2634:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25333:
	.size	_ZN2v88internal8Compiler31FinalizeOptimizedCompilationJobEPNS0_23OptimizedCompilationJobEPNS0_7IsolateE, .-_ZN2v88internal8Compiler31FinalizeOptimizedCompilationJobEPNS0_23OptimizedCompilationJobEPNS0_7IsolateE
	.section	.text._ZN2v88internal8Compiler17PostInstantiationENS0_6HandleINS0_10JSFunctionEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Compiler17PostInstantiationENS0_6HandleINS0_10JSFunctionEEE
	.type	_ZN2v88internal8Compiler17PostInstantiationENS0_6HandleINS0_10JSFunctionEEE, @function
_ZN2v88internal8Compiler17PostInstantiationENS0_6HandleINS0_10JSFunctionEEE:
.LFB25334:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rbx
	subq	$37592, %rbx
	movq	23(%rax), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2640
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r12
.L2641:
	movq	%rsi, %rax
	leaq	7(%rsi), %r14
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	7(%rsi), %rdx
	testb	$1, %dl
	jne	.L2643
.L2646:
	movq	(%r14), %rdx
	testb	$1, %dl
	jne	.L2743
.L2649:
	movabsq	$287762808832, %rdx
	movq	(%r14), %rax
	cmpq	%rdx, %rax
	je	.L2742
	testb	$1, %al
	jne	.L2744
.L2657:
	movq	(%r12), %rax
	leaq	7(%rax), %rdx
	movq	7(%rax), %rax
	testb	$1, %al
	jne	.L2659
.L2662:
	movq	(%rdx), %rax
	testb	$1, %al
	jne	.L2734
.L2742:
	movq	(%r12), %rax
.L2673:
	movl	47(%rax), %edx
	andl	$268435456, %edx
	jne	.L2695
	movl	47(%rax), %edx
	shrl	$7, %edx
	andl	$7, %edx
	cmpb	$4, %dl
	je	.L2695
.L2639:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2745
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2695:
	.cfi_restore_state
	movq	31(%rax), %r12
	testb	$1, %r12b
	jne	.L2746
.L2693:
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2697
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L2698:
	movq	41472(%rbx), %rdi
	call	_ZN2v88internal5Debug14OnAfterCompileENS0_6HandleINS0_6ScriptEEE@PLT
	jmp	.L2639
	.p2align 4,,10
	.p2align 3
.L2697:
	movq	41088(%rbx), %rsi
	cmpq	41096(%rbx), %rsi
	je	.L2747
.L2699:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rbx)
	movq	%r12, (%rsi)
	jmp	.L2698
	.p2align 4,,10
	.p2align 3
.L2746:
	movq	-1(%r12), %rax
	cmpw	$86, 11(%rax)
	jne	.L2693
	movq	23(%r12), %r12
	jmp	.L2693
	.p2align 4,,10
	.p2align 3
.L2744:
	movq	-1(%rax), %rdx
	cmpw	$165, 11(%rdx)
	je	.L2742
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L2657
	jmp	.L2742
	.p2align 4,,10
	.p2align 3
.L2640:
	movq	41088(%rbx), %r12
	cmpq	%r12, 41096(%rbx)
	je	.L2748
.L2642:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r12)
	jmp	.L2641
	.p2align 4,,10
	.p2align 3
.L2643:
	movq	-1(%rdx), %rdx
	cmpw	$72, 11(%rdx)
	jne	.L2646
.L2650:
	movq	31(%rsi), %rdx
	testb	$1, %dl
	jne	.L2749
.L2647:
	movq	(%r14), %rdx
	testb	$1, %dl
	jne	.L2750
.L2652:
	movq	(%r14), %rdx
	movq	7(%rdx), %rsi
.L2651:
	movq	3520(%rax), %rdi
	leaq	-37592(%rax), %r15
	testq	%rdi, %rdi
	je	.L2653
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	jmp	.L2649
	.p2align 4,,10
	.p2align 3
.L2743:
	movq	-1(%rdx), %rdx
	cmpw	$91, 11(%rdx)
	jne	.L2649
	jmp	.L2650
	.p2align 4,,10
	.p2align 3
.L2659:
	movq	-1(%rax), %rax
	cmpw	$72, 11(%rax)
	jne	.L2662
.L2665:
	movq	%r13, %rdi
	call	_ZN2v88internal10JSFunction22InitializeFeedbackCellENS0_6HandleIS1_EE@PLT
	movq	0(%r13), %rdx
	movabsq	$287762808832, %rcx
	movq	23(%rdx), %rax
	movq	7(%rax), %rax
	cmpq	%rcx, %rax
	je	.L2672
	testb	$1, %al
	jne	.L2666
.L2669:
	movq	39(%rdx), %rax
	movq	7(%rax), %rax
	movq	-1(%rax), %rax
	cmpw	$155, 11(%rax)
	je	.L2751
.L2672:
	cmpb	$0, _ZN2v88internal15FLAG_always_optE(%rip)
	movq	(%r12), %rax
	je	.L2673
	movl	47(%rax), %edx
	andb	$16, %dh
	je	.L2673
	movl	47(%rax), %edx
	andl	$15728640, %edx
	jne	.L2673
	movq	0(%r13), %rax
	movq	47(%rax), %rdx
	cmpl	$67, 59(%rdx)
	jne	.L2752
.L2679:
	movq	%rax, -64(%rbp)
	movq	47(%rax), %rdx
	cmpl	$67, 59(%rdx)
	jne	.L2753
.L2686:
	leaq	-64(%rbp), %r14
	movq	%r14, %rdi
	call	_ZNK2v88internal10JSFunction19has_feedback_vectorEv
	testb	%al, %al
	je	.L2690
	movq	-64(%rbp), %rax
	movq	39(%rax), %rax
	movq	7(%rax), %rax
	movq	15(%rax), %rax
	testb	$1, %al
	je	.L2690
	cmpl	$3, %eax
	je	.L2690
	andq	$-3, %rax
	jne	.L2754
.L2690:
	movq	%r13, %rdi
	call	_ZN2v88internal10JSFunction20EnsureFeedbackVectorENS0_6HandleIS1_EE@PLT
	movq	0(%r13), %rax
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal10JSFunction19MarkForOptimizationENS0_15ConcurrencyModeE@PLT
	jmp	.L2742
	.p2align 4,,10
	.p2align 3
.L2653:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L2755
.L2655:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%rsi, (%rax)
	jmp	.L2649
	.p2align 4,,10
	.p2align 3
.L2734:
	movq	-1(%rax), %rax
	cmpw	$91, 11(%rax)
	jne	.L2742
	jmp	.L2665
	.p2align 4,,10
	.p2align 3
.L2751:
	movq	0(%r13), %rdi
	movq	39(%rdi), %rax
	movq	7(%rax), %rax
	movq	15(%rax), %rax
	testb	$1, %al
	je	.L2672
	cmpl	$3, %eax
	je	.L2672
	movq	%rax, %rdx
	andq	$-3, %rdx
	je	.L2672
	movq	%rdx, 47(%rdi)
	leaq	47(%rdi), %rsi
	testb	$1, %dl
	je	.L2672
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L2672
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2672
	.p2align 4,,10
	.p2align 3
.L2749:
	movq	-1(%rdx), %rcx
	cmpw	$86, 11(%rcx)
	jne	.L2647
	movq	39(%rdx), %rcx
	testb	$1, %cl
	je	.L2647
	movq	-1(%rcx), %rcx
	cmpw	$72, 11(%rcx)
	jne	.L2647
	movq	31(%rdx), %rsi
	jmp	.L2651
	.p2align 4,,10
	.p2align 3
.L2750:
	movq	-1(%rdx), %rdx
	cmpw	$72, 11(%rdx)
	jne	.L2652
	movq	(%r14), %rsi
	jmp	.L2651
	.p2align 4,,10
	.p2align 3
.L2748:
	movq	%rbx, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L2642
	.p2align 4,,10
	.p2align 3
.L2747:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L2699
	.p2align 4,,10
	.p2align 3
.L2666:
	movq	-1(%rax), %rcx
	cmpw	$165, 11(%rcx)
	je	.L2672
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L2669
	jmp	.L2672
	.p2align 4,,10
	.p2align 3
.L2755:
	movq	%r15, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	jmp	.L2655
.L2752:
	movabsq	$287762808832, %rcx
	movq	23(%rax), %rdx
	movq	7(%rdx), %rdx
	cmpq	%rcx, %rdx
	je	.L2741
	testb	$1, %dl
	jne	.L2756
.L2680:
	movq	47(%rax), %rdx
	testb	$62, 43(%rdx)
	je	.L2737
.L2741:
	movq	0(%r13), %rax
	jmp	.L2679
.L2753:
	movq	23(%rax), %rax
	movl	$67, %edx
	salq	$32, %rdx
	movq	7(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2686
	testb	$1, %al
	jne	.L2757
.L2687:
	movq	-64(%rbp), %rax
	movq	47(%rax), %rdx
	testb	$62, 43(%rdx)
	jne	.L2686
	movq	47(%rax), %rax
	movq	31(%rax), %rax
	movl	15(%rax), %eax
	testb	$1, %al
	jne	.L2686
	jmp	.L2742
.L2745:
	call	__stack_chk_fail@PLT
.L2737:
	movq	47(%rax), %rax
	movq	31(%rax), %rax
	movl	15(%rax), %eax
	testb	$1, %al
	je	.L2742
	jmp	.L2741
.L2756:
	movq	-1(%rdx), %rcx
	cmpw	$165, 11(%rcx)
	je	.L2741
	movq	-1(%rdx), %rdx
	cmpw	$166, 11(%rdx)
	jne	.L2680
	jmp	.L2741
.L2757:
	movq	-1(%rax), %rdx
	cmpw	$165, 11(%rdx)
	je	.L2686
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L2687
	jmp	.L2686
.L2754:
	movq	31(%rax), %rax
	movl	15(%rax), %eax
	testb	$1, %al
	jne	.L2690
	jmp	.L2742
	.cfi_endproc
.LFE25334:
	.size	_ZN2v88internal8Compiler17PostInstantiationENS0_6HandleINS0_10JSFunctionEEE, .-_ZN2v88internal8Compiler17PostInstantiationENS0_6HandleINS0_10JSFunctionEEE
	.section	.text._ZN2v88internal19ScriptStreamingDataC2ESt10unique_ptrINS_14ScriptCompiler20ExternalSourceStreamESt14default_deleteIS4_EENS3_14StreamedSource8EncodingE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19ScriptStreamingDataC2ESt10unique_ptrINS_14ScriptCompiler20ExternalSourceStreamESt14default_deleteIS4_EENS3_14StreamedSource8EncodingE
	.type	_ZN2v88internal19ScriptStreamingDataC2ESt10unique_ptrINS_14ScriptCompiler20ExternalSourceStreamESt14default_deleteIS4_EENS3_14StreamedSource8EncodingE, @function
_ZN2v88internal19ScriptStreamingDataC2ESt10unique_ptrINS_14ScriptCompiler20ExternalSourceStreamESt14default_deleteIS4_EENS3_14StreamedSource8EncodingE:
.LFB25337:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	movl	%edx, 8(%rdi)
	movq	$0, (%rsi)
	movq	%rax, (%rdi)
	movq	$0, 16(%rdi)
	ret
	.cfi_endproc
.LFE25337:
	.size	_ZN2v88internal19ScriptStreamingDataC2ESt10unique_ptrINS_14ScriptCompiler20ExternalSourceStreamESt14default_deleteIS4_EENS3_14StreamedSource8EncodingE, .-_ZN2v88internal19ScriptStreamingDataC2ESt10unique_ptrINS_14ScriptCompiler20ExternalSourceStreamESt14default_deleteIS4_EENS3_14StreamedSource8EncodingE
	.globl	_ZN2v88internal19ScriptStreamingDataC1ESt10unique_ptrINS_14ScriptCompiler20ExternalSourceStreamESt14default_deleteIS4_EENS3_14StreamedSource8EncodingE
	.set	_ZN2v88internal19ScriptStreamingDataC1ESt10unique_ptrINS_14ScriptCompiler20ExternalSourceStreamESt14default_deleteIS4_EENS3_14StreamedSource8EncodingE,_ZN2v88internal19ScriptStreamingDataC2ESt10unique_ptrINS_14ScriptCompiler20ExternalSourceStreamESt14default_deleteIS4_EENS3_14StreamedSource8EncodingE
	.section	.text._ZN2v88internal19ScriptStreamingDataD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19ScriptStreamingDataD2Ev
	.type	_ZN2v88internal19ScriptStreamingDataD2Ev, @function
_ZN2v88internal19ScriptStreamingDataD2Ev:
.LFB25340:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	16(%rdi), %r12
	movq	%rdi, %rbx
	testq	%r12, %r12
	je	.L2760
	movq	%r12, %rdi
	call	_ZN2v88internal21BackgroundCompileTaskD1Ev
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2760:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2759
	movq	(%rdi), %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	8(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L2759:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25340:
	.size	_ZN2v88internal19ScriptStreamingDataD2Ev, .-_ZN2v88internal19ScriptStreamingDataD2Ev
	.globl	_ZN2v88internal19ScriptStreamingDataD1Ev
	.set	_ZN2v88internal19ScriptStreamingDataD1Ev,_ZN2v88internal19ScriptStreamingDataD2Ev
	.section	.text._ZN2v88internal19ScriptStreamingData7ReleaseEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19ScriptStreamingData7ReleaseEv
	.type	_ZN2v88internal19ScriptStreamingData7ReleaseEv, @function
_ZN2v88internal19ScriptStreamingData7ReleaseEv:
.LFB25342:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	16(%rdi), %r12
	movq	$0, 16(%rdi)
	testq	%r12, %r12
	je	.L2766
	movq	%r12, %rdi
	call	_ZN2v88internal21BackgroundCompileTaskD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$64, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L2766:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25342:
	.size	_ZN2v88internal19ScriptStreamingData7ReleaseEv, .-_ZN2v88internal19ScriptStreamingData7ReleaseEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal25UnoptimizedCompilationJob10ExecuteJobEv,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal25UnoptimizedCompilationJob10ExecuteJobEv, @function
_GLOBAL__sub_I__ZN2v88internal25UnoptimizedCompilationJob10ExecuteJobEv:
.LFB34204:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE34204:
	.size	_GLOBAL__sub_I__ZN2v88internal25UnoptimizedCompilationJob10ExecuteJobEv, .-_GLOBAL__sub_I__ZN2v88internal25UnoptimizedCompilationJob10ExecuteJobEv
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal25UnoptimizedCompilationJob10ExecuteJobEv
	.section	.rodata.CSWTCH.453,"a"
	.align 32
	.type	CSWTCH.453, @object
	.size	CSWTCH.453, 60
CSWTCH.453:
	.long	7
	.long	10
	.long	8
	.long	4
	.long	11
	.long	12
	.long	9
	.long	5
	.long	6
	.long	13
	.long	16
	.long	17
	.long	18
	.long	19
	.long	0
	.weak	_ZTVN2v88internal15InterruptsScopeE
	.section	.data.rel.ro.local._ZTVN2v88internal15InterruptsScopeE,"awG",@progbits,_ZTVN2v88internal15InterruptsScopeE,comdat
	.align 8
	.type	_ZTVN2v88internal15InterruptsScopeE, @object
	.size	_ZTVN2v88internal15InterruptsScopeE, 32
_ZTVN2v88internal15InterruptsScopeE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal15InterruptsScopeD1Ev
	.quad	_ZN2v88internal15InterruptsScopeD0Ev
	.weak	_ZTVN2v88internal23PostponeInterruptsScopeE
	.section	.data.rel.ro.local._ZTVN2v88internal23PostponeInterruptsScopeE,"awG",@progbits,_ZTVN2v88internal23PostponeInterruptsScopeE,comdat
	.align 8
	.type	_ZTVN2v88internal23PostponeInterruptsScopeE, @object
	.size	_ZTVN2v88internal23PostponeInterruptsScopeE, 32
_ZTVN2v88internal23PostponeInterruptsScopeE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23PostponeInterruptsScopeD1Ev
	.quad	_ZN2v88internal23PostponeInterruptsScopeD0Ev
	.hidden	_ZTCN2v88internal12StdoutStreamE0_So
	.weak	_ZTCN2v88internal12StdoutStreamE0_So
	.section	.rodata._ZTCN2v88internal12StdoutStreamE0_So,"aG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal12StdoutStreamE0_So, @object
	.size	_ZTCN2v88internal12StdoutStreamE0_So, 80
_ZTCN2v88internal12StdoutStreamE0_So:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.hidden	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE
	.weak	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE
	.section	.rodata._ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE,"aG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE, @object
	.size	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE, 80
_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.weak	_ZTTN2v88internal12StdoutStreamE
	.section	.data.rel.ro.local._ZTTN2v88internal12StdoutStreamE,"awG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTTN2v88internal12StdoutStreamE, @object
	.size	_ZTTN2v88internal12StdoutStreamE, 48
_ZTTN2v88internal12StdoutStreamE:
	.quad	_ZTVN2v88internal12StdoutStreamE+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_So+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_So+64
	.quad	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE+64
	.quad	_ZTVN2v88internal12StdoutStreamE+64
	.weak	_ZTVN2v88internal12StdoutStreamE
	.section	.data.rel.ro.local._ZTVN2v88internal12StdoutStreamE,"awG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTVN2v88internal12StdoutStreamE, @object
	.size	_ZTVN2v88internal12StdoutStreamE, 80
_ZTVN2v88internal12StdoutStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	_ZN2v88internal12StdoutStreamD1Ev
	.quad	_ZN2v88internal12StdoutStreamD0Ev
	.quad	-80
	.quad	-80
	.quad	0
	.quad	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.quad	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.section	.bss._ZZN2v88internal8Compiler31FinalizeOptimizedCompilationJobEPNS0_23OptimizedCompilationJobEPNS0_7IsolateEE29trace_event_unique_atomic2284,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal8Compiler31FinalizeOptimizedCompilationJobEPNS0_23OptimizedCompilationJobEPNS0_7IsolateEE29trace_event_unique_atomic2284, @object
	.size	_ZZN2v88internal8Compiler31FinalizeOptimizedCompilationJobEPNS0_23OptimizedCompilationJobEPNS0_7IsolateEE29trace_event_unique_atomic2284, 8
_ZZN2v88internal8Compiler31FinalizeOptimizedCompilationJobEPNS0_23OptimizedCompilationJobEPNS0_7IsolateEE29trace_event_unique_atomic2284:
	.zero	8
	.section	.bss._ZZN2v88internal8Compiler18GetWrappedFunctionENS0_6HandleINS0_6StringEEENS2_INS0_10FixedArrayEEENS2_INS0_7ContextEEERKNS1_13ScriptDetailsENS_19ScriptOriginOptionsEPNS0_10ScriptDataENS_14ScriptCompiler14CompileOptionsENSF_13NoCacheReasonEE29trace_event_unique_atomic2100,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal8Compiler18GetWrappedFunctionENS0_6HandleINS0_6StringEEENS2_INS0_10FixedArrayEEENS2_INS0_7ContextEEERKNS1_13ScriptDetailsENS_19ScriptOriginOptionsEPNS0_10ScriptDataENS_14ScriptCompiler14CompileOptionsENSF_13NoCacheReasonEE29trace_event_unique_atomic2100, @object
	.size	_ZZN2v88internal8Compiler18GetWrappedFunctionENS0_6HandleINS0_6StringEEENS2_INS0_10FixedArrayEEENS2_INS0_7ContextEEERKNS1_13ScriptDetailsENS_19ScriptOriginOptionsEPNS0_10ScriptDataENS_14ScriptCompiler14CompileOptionsENSF_13NoCacheReasonEE29trace_event_unique_atomic2100, 8
_ZZN2v88internal8Compiler18GetWrappedFunctionENS0_6HandleINS0_6StringEEENS2_INS0_10FixedArrayEEENS2_INS0_7ContextEEERKNS1_13ScriptDetailsENS_19ScriptOriginOptionsEPNS0_10ScriptDataENS_14ScriptCompiler14CompileOptionsENSF_13NoCacheReasonEE29trace_event_unique_atomic2100:
	.zero	8
	.section	.bss._ZZN2v88internal8Compiler30GetSharedFunctionInfoForScriptEPNS0_7IsolateENS0_6HandleINS0_6StringEEERKNS1_13ScriptDetailsENS_19ScriptOriginOptionsEPNS_9ExtensionEPNS0_10ScriptDataENS_14ScriptCompiler14CompileOptionsENSF_13NoCacheReasonENS0_11NativesFlagEE29trace_event_unique_atomic2021,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal8Compiler30GetSharedFunctionInfoForScriptEPNS0_7IsolateENS0_6HandleINS0_6StringEEERKNS1_13ScriptDetailsENS_19ScriptOriginOptionsEPNS_9ExtensionEPNS0_10ScriptDataENS_14ScriptCompiler14CompileOptionsENSF_13NoCacheReasonENS0_11NativesFlagEE29trace_event_unique_atomic2021, @object
	.size	_ZZN2v88internal8Compiler30GetSharedFunctionInfoForScriptEPNS0_7IsolateENS0_6HandleINS0_6StringEEERKNS1_13ScriptDetailsENS_19ScriptOriginOptionsEPNS_9ExtensionEPNS0_10ScriptDataENS_14ScriptCompiler14CompileOptionsENSF_13NoCacheReasonENS0_11NativesFlagEE29trace_event_unique_atomic2021, 8
_ZZN2v88internal8Compiler30GetSharedFunctionInfoForScriptEPNS0_7IsolateENS0_6HandleINS0_6StringEEERKNS1_13ScriptDetailsENS_19ScriptOriginOptionsEPNS_9ExtensionEPNS0_10ScriptDataENS_14ScriptCompiler14CompileOptionsENSF_13NoCacheReasonENS0_11NativesFlagEE29trace_event_unique_atomic2021:
	.zero	8
	.section	.bss._ZZN2v88internal12_GLOBAL__N_19NewScriptEPNS0_7IsolateEPNS0_9ParseInfoENS0_6HandleINS0_6StringEEENS0_8Compiler13ScriptDetailsENS_19ScriptOriginOptionsENS0_11NativesFlagEE29trace_event_unique_atomic1966,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal12_GLOBAL__N_19NewScriptEPNS0_7IsolateEPNS0_9ParseInfoENS0_6HandleINS0_6StringEEENS0_8Compiler13ScriptDetailsENS_19ScriptOriginOptionsENS0_11NativesFlagEE29trace_event_unique_atomic1966, @object
	.size	_ZZN2v88internal12_GLOBAL__N_19NewScriptEPNS0_7IsolateEPNS0_9ParseInfoENS0_6HandleINS0_6StringEEENS0_8Compiler13ScriptDetailsENS_19ScriptOriginOptionsENS0_11NativesFlagEE29trace_event_unique_atomic1966, 8
_ZZN2v88internal12_GLOBAL__N_19NewScriptEPNS0_7IsolateEPNS0_9ParseInfoENS0_6HandleINS0_6StringEEENS0_8Compiler13ScriptDetailsENS_19ScriptOriginOptionsENS0_11NativesFlagEE29trace_event_unique_atomic1966:
	.zero	8
	.section	.bss._ZZN2v88internal8Compiler19GetFunctionFromEvalENS0_6HandleINS0_6StringEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_12LanguageModeENS0_16ParseRestrictionEiiiE29trace_event_unique_atomic1558,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal8Compiler19GetFunctionFromEvalENS0_6HandleINS0_6StringEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_12LanguageModeENS0_16ParseRestrictionEiiiE29trace_event_unique_atomic1558, @object
	.size	_ZZN2v88internal8Compiler19GetFunctionFromEvalENS0_6HandleINS0_6StringEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_12LanguageModeENS0_16ParseRestrictionEiiiE29trace_event_unique_atomic1558, 8
_ZZN2v88internal8Compiler19GetFunctionFromEvalENS0_6HandleINS0_6StringEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_12LanguageModeENS0_16ParseRestrictionEiiiE29trace_event_unique_atomic1558:
	.zero	8
	.section	.bss._ZZN2v88internal8Compiler29FinalizeBackgroundCompileTaskEPNS0_21BackgroundCompileTaskENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateENS1_18ClearExceptionFlagEE29trace_event_unique_atomic1421,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal8Compiler29FinalizeBackgroundCompileTaskEPNS0_21BackgroundCompileTaskENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateENS1_18ClearExceptionFlagEE29trace_event_unique_atomic1421, @object
	.size	_ZZN2v88internal8Compiler29FinalizeBackgroundCompileTaskEPNS0_21BackgroundCompileTaskENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateENS1_18ClearExceptionFlagEE29trace_event_unique_atomic1421, 8
_ZZN2v88internal8Compiler29FinalizeBackgroundCompileTaskEPNS0_21BackgroundCompileTaskENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateENS1_18ClearExceptionFlagEE29trace_event_unique_atomic1421:
	.zero	8
	.section	.bss._ZZN2v88internal8Compiler7CompileENS0_6HandleINS0_18SharedFunctionInfoEEENS1_18ClearExceptionFlagEPNS0_15IsCompiledScopeEE29trace_event_unique_atomic1294,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal8Compiler7CompileENS0_6HandleINS0_18SharedFunctionInfoEEENS1_18ClearExceptionFlagEPNS0_15IsCompiledScopeEE29trace_event_unique_atomic1294, @object
	.size	_ZZN2v88internal8Compiler7CompileENS0_6HandleINS0_18SharedFunctionInfoEEENS1_18ClearExceptionFlagEPNS0_15IsCompiledScopeEE29trace_event_unique_atomic1294, 8
_ZZN2v88internal8Compiler7CompileENS0_6HandleINS0_18SharedFunctionInfoEEENS1_18ClearExceptionFlagEPNS0_15IsCompiledScopeEE29trace_event_unique_atomic1294:
	.zero	8
	.section	.bss._ZZN2v88internal8Compiler22CollectSourcePositionsEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEEE29trace_event_unique_atomic1213,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal8Compiler22CollectSourcePositionsEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEEE29trace_event_unique_atomic1213, @object
	.size	_ZZN2v88internal8Compiler22CollectSourcePositionsEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEEE29trace_event_unique_atomic1213, 8
_ZZN2v88internal8Compiler22CollectSourcePositionsEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEEE29trace_event_unique_atomic1213:
	.zero	8
	.section	.bss._ZZN2v88internal21BackgroundCompileTask3RunEvE29trace_event_unique_atomic1132,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal21BackgroundCompileTask3RunEvE29trace_event_unique_atomic1132, @object
	.size	_ZZN2v88internal21BackgroundCompileTask3RunEvE29trace_event_unique_atomic1132, 8
_ZZN2v88internal21BackgroundCompileTask3RunEvE29trace_event_unique_atomic1132:
	.zero	8
	.section	.bss._ZZN2v88internal12_GLOBAL__N_125CompileOnBackgroundThreadEPNS0_9ParseInfoEPNS0_19AccountingAllocatorEPSt12forward_listISt10unique_ptrINS0_25UnoptimizedCompilationJobESt14default_deleteIS8_EESaISB_EEE29trace_event_unique_atomic1011,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal12_GLOBAL__N_125CompileOnBackgroundThreadEPNS0_9ParseInfoEPNS0_19AccountingAllocatorEPSt12forward_listISt10unique_ptrINS0_25UnoptimizedCompilationJobESt14default_deleteIS8_EESaISB_EEE29trace_event_unique_atomic1011, @object
	.size	_ZZN2v88internal12_GLOBAL__N_125CompileOnBackgroundThreadEPNS0_9ParseInfoEPNS0_19AccountingAllocatorEPSt12forward_listISt10unique_ptrINS0_25UnoptimizedCompilationJobESt14default_deleteIS8_EESaISB_EEE29trace_event_unique_atomic1011, 8
_ZZN2v88internal12_GLOBAL__N_125CompileOnBackgroundThreadEPNS0_9ParseInfoEPNS0_19AccountingAllocatorEPSt12forward_listISt10unique_ptrINS0_25UnoptimizedCompilationJobESt14default_deleteIS8_EESaISB_EEE29trace_event_unique_atomic1011:
	.zero	8
	.section	.bss._ZZN2v88internal12_GLOBAL__N_115CompileToplevelEPNS0_9ParseInfoEPNS0_7IsolateEPNS0_15IsCompiledScopeEE28trace_event_unique_atomic990,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal12_GLOBAL__N_115CompileToplevelEPNS0_9ParseInfoEPNS0_7IsolateEPNS0_15IsCompiledScopeEE28trace_event_unique_atomic990, @object
	.size	_ZZN2v88internal12_GLOBAL__N_115CompileToplevelEPNS0_9ParseInfoEPNS0_7IsolateEPNS0_15IsCompiledScopeEE28trace_event_unique_atomic990, 8
_ZZN2v88internal12_GLOBAL__N_115CompileToplevelEPNS0_9ParseInfoEPNS0_7IsolateEPNS0_15IsCompiledScopeEE28trace_event_unique_atomic990:
	.zero	8
	.section	.bss._ZZN2v88internal12_GLOBAL__N_115CompileToplevelEPNS0_9ParseInfoEPNS0_7IsolateEPNS0_15IsCompiledScopeEE28trace_event_unique_atomic970,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal12_GLOBAL__N_115CompileToplevelEPNS0_9ParseInfoEPNS0_7IsolateEPNS0_15IsCompiledScopeEE28trace_event_unique_atomic970, @object
	.size	_ZZN2v88internal12_GLOBAL__N_115CompileToplevelEPNS0_9ParseInfoEPNS0_7IsolateEPNS0_15IsCompiledScopeEE28trace_event_unique_atomic970, 8
_ZZN2v88internal12_GLOBAL__N_115CompileToplevelEPNS0_9ParseInfoEPNS0_7IsolateEPNS0_15IsCompiledScopeEE28trace_event_unique_atomic970:
	.zero	8
	.section	.bss._ZZN2v88internal12_GLOBAL__N_116GetOptimizedCodeENS0_6HandleINS0_10JSFunctionEEENS0_15ConcurrencyModeENS0_9BailoutIdEPNS0_15JavaScriptFrameEE28trace_event_unique_atomic839,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal12_GLOBAL__N_116GetOptimizedCodeENS0_6HandleINS0_10JSFunctionEEENS0_15ConcurrencyModeENS0_9BailoutIdEPNS0_15JavaScriptFrameEE28trace_event_unique_atomic839, @object
	.size	_ZZN2v88internal12_GLOBAL__N_116GetOptimizedCodeENS0_6HandleINS0_10JSFunctionEEENS0_15ConcurrencyModeENS0_9BailoutIdEPNS0_15JavaScriptFrameEE28trace_event_unique_atomic839, 8
_ZZN2v88internal12_GLOBAL__N_116GetOptimizedCodeENS0_6HandleINS0_10JSFunctionEEENS0_15ConcurrencyModeENS0_9BailoutIdEPNS0_15JavaScriptFrameEE28trace_event_unique_atomic839:
	.zero	8
	.section	.bss._ZZN2v88internal12_GLOBAL__N_121GetOptimizedCodeLaterEPNS0_23OptimizedCompilationJobEPNS0_7IsolateEE28trace_event_unique_atomic774,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal12_GLOBAL__N_121GetOptimizedCodeLaterEPNS0_23OptimizedCompilationJobEPNS0_7IsolateEE28trace_event_unique_atomic774, @object
	.size	_ZZN2v88internal12_GLOBAL__N_121GetOptimizedCodeLaterEPNS0_23OptimizedCompilationJobEPNS0_7IsolateEE28trace_event_unique_atomic774, 8
_ZZN2v88internal12_GLOBAL__N_121GetOptimizedCodeLaterEPNS0_23OptimizedCompilationJobEPNS0_7IsolateEE28trace_event_unique_atomic774:
	.zero	8
	.section	.bss._ZZN2v88internal12_GLOBAL__N_119GetOptimizedCodeNowEPNS0_23OptimizedCompilationJobEPNS0_7IsolateEE28trace_event_unique_atomic728,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal12_GLOBAL__N_119GetOptimizedCodeNowEPNS0_23OptimizedCompilationJobEPNS0_7IsolateEE28trace_event_unique_atomic728, @object
	.size	_ZZN2v88internal12_GLOBAL__N_119GetOptimizedCodeNowEPNS0_23OptimizedCompilationJobEPNS0_7IsolateEE28trace_event_unique_atomic728, 8
_ZZN2v88internal12_GLOBAL__N_119GetOptimizedCodeNowEPNS0_23OptimizedCompilationJobEPNS0_7IsolateEE28trace_event_unique_atomic728:
	.zero	8
	.section	.bss._ZZNK2v88internal23OptimizedCompilationJob22RecordCompilationStatsENS1_15CompilationModeEPNS0_7IsolateEE9code_size,"aw",@nobits
	.align 4
	.type	_ZZNK2v88internal23OptimizedCompilationJob22RecordCompilationStatsENS1_15CompilationModeEPNS0_7IsolateEE9code_size, @object
	.size	_ZZNK2v88internal23OptimizedCompilationJob22RecordCompilationStatsENS1_15CompilationModeEPNS0_7IsolateEE9code_size, 4
_ZZNK2v88internal23OptimizedCompilationJob22RecordCompilationStatsENS1_15CompilationModeEPNS0_7IsolateEE9code_size:
	.zero	4
	.section	.bss._ZZNK2v88internal23OptimizedCompilationJob22RecordCompilationStatsENS1_15CompilationModeEPNS0_7IsolateEE18compiled_functions,"aw",@nobits
	.align 4
	.type	_ZZNK2v88internal23OptimizedCompilationJob22RecordCompilationStatsENS1_15CompilationModeEPNS0_7IsolateEE18compiled_functions, @object
	.size	_ZZNK2v88internal23OptimizedCompilationJob22RecordCompilationStatsENS1_15CompilationModeEPNS0_7IsolateEE18compiled_functions, 4
_ZZNK2v88internal23OptimizedCompilationJob22RecordCompilationStatsENS1_15CompilationModeEPNS0_7IsolateEE18compiled_functions:
	.zero	4
	.section	.bss._ZZNK2v88internal23OptimizedCompilationJob22RecordCompilationStatsENS1_15CompilationModeEPNS0_7IsolateEE16compilation_time,"aw",@nobits
	.align 8
	.type	_ZZNK2v88internal23OptimizedCompilationJob22RecordCompilationStatsENS1_15CompilationModeEPNS0_7IsolateEE16compilation_time, @object
	.size	_ZZNK2v88internal23OptimizedCompilationJob22RecordCompilationStatsENS1_15CompilationModeEPNS0_7IsolateEE16compilation_time, 8
_ZZNK2v88internal23OptimizedCompilationJob22RecordCompilationStatsENS1_15CompilationModeEPNS0_7IsolateEE16compilation_time:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
