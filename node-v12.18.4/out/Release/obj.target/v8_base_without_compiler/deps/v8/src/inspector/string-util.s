	.file	"string-util.cc"
	.text
	.section	.text._ZN12v8_inspector16StringBufferImpl6stringEv,"axG",@progbits,_ZN12v8_inspector16StringBufferImpl6stringEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector16StringBufferImpl6stringEv
	.type	_ZN12v8_inspector16StringBufferImpl6stringEv, @function
_ZN12v8_inspector16StringBufferImpl6stringEv:
.LFB3675:
	.cfi_startproc
	endbr64
	leaq	48(%rdi), %rax
	ret
	.cfi_endproc
.LFE3675:
	.size	_ZN12v8_inspector16StringBufferImpl6stringEv, .-_ZN12v8_inspector16StringBufferImpl6stringEv
	.section	.text._ZN12v8_inspector16StringBufferImplD2Ev,"axG",@progbits,_ZN12v8_inspector16StringBufferImplD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector16StringBufferImplD2Ev
	.type	_ZN12v8_inspector16StringBufferImplD2Ev, @function
_ZN12v8_inspector16StringBufferImplD2Ev:
.LFB6666:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r8
	leaq	16+_ZTVN12v8_inspector16StringBufferImplE(%rip), %rax
	addq	$24, %rdi
	movq	%rax, -24(%rdi)
	cmpq	%rdi, %r8
	je	.L3
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L3:
	ret
	.cfi_endproc
.LFE6666:
	.size	_ZN12v8_inspector16StringBufferImplD2Ev, .-_ZN12v8_inspector16StringBufferImplD2Ev
	.weak	_ZN12v8_inspector16StringBufferImplD1Ev
	.set	_ZN12v8_inspector16StringBufferImplD1Ev,_ZN12v8_inspector16StringBufferImplD2Ev
	.section	.text._ZN12v8_inspector16StringBufferImplD0Ev,"axG",@progbits,_ZN12v8_inspector16StringBufferImplD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector16StringBufferImplD0Ev
	.type	_ZN12v8_inspector16StringBufferImplD0Ev, @function
_ZN12v8_inspector16StringBufferImplD0Ev:
.LFB6668:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector16StringBufferImplE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L6
	call	_ZdlPv@PLT
.L6:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$72, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE6668:
	.size	_ZN12v8_inspector16StringBufferImplD0Ev, .-_ZN12v8_inspector16StringBufferImplD0Ev
	.section	.text._ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E,"ax",@progbits
	.p2align 4
	.globl	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E
	.type	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E, @function
_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E:
.LFB5531:
	.cfi_startproc
	endbr64
	movq	8(%rsi), %rcx
	testq	%rcx, %rcx
	jne	.L9
	leaq	128(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	(%rsi), %rsi
	call	_ZN2v86String14NewFromTwoByteEPNS_7IsolateEPKtNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L15
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore_state
	movq	%rax, -8(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-8(%rbp), %rax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5531:
	.size	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E, .-_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E
	.section	.text._ZN12v8_inspector22toV8StringInternalizedEPN2v87IsolateERKNS_8String16E,"ax",@progbits
	.p2align 4
	.globl	_ZN12v8_inspector22toV8StringInternalizedEPN2v87IsolateERKNS_8String16E
	.type	_ZN12v8_inspector22toV8StringInternalizedEPN2v87IsolateERKNS_8String16E, @function
_ZN12v8_inspector22toV8StringInternalizedEPN2v87IsolateERKNS_8String16E:
.LFB5532:
	.cfi_startproc
	endbr64
	movq	8(%rsi), %rcx
	testq	%rcx, %rcx
	jne	.L17
	leaq	128(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	(%rsi), %rsi
	call	_ZN2v86String14NewFromTwoByteEPNS_7IsolateEPKtNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L23
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	.cfi_restore_state
	movq	%rax, -8(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-8(%rbp), %rax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5532:
	.size	_ZN12v8_inspector22toV8StringInternalizedEPN2v87IsolateERKNS_8String16E, .-_ZN12v8_inspector22toV8StringInternalizedEPN2v87IsolateERKNS_8String16E
	.section	.text._ZN12v8_inspector22toV8StringInternalizedEPN2v87IsolateEPKc,"ax",@progbits
	.p2align 4
	.globl	_ZN12v8_inspector22toV8StringInternalizedEPN2v87IsolateEPKc
	.type	_ZN12v8_inspector22toV8StringInternalizedEPN2v87IsolateEPKc, @function
_ZN12v8_inspector22toV8StringInternalizedEPN2v87IsolateEPKc:
.LFB5533:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L27
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	.cfi_restore_state
	movq	%rax, -8(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-8(%rbp), %rax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5533:
	.size	_ZN12v8_inspector22toV8StringInternalizedEPN2v87IsolateEPKc, .-_ZN12v8_inspector22toV8StringInternalizedEPN2v87IsolateEPKc
	.section	.text._ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_10StringViewE,"ax",@progbits
	.p2align 4
	.globl	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_10StringViewE
	.type	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_10StringViewE, @function
_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_10StringViewE:
.LFB5534:
	.cfi_startproc
	endbr64
	movq	8(%rsi), %rcx
	testq	%rcx, %rcx
	jne	.L29
	leaq	128(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	16(%rsi), %r8
	cmpb	$0, (%rsi)
	movq	%r8, %rsi
	je	.L31
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L37
.L30:
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	.cfi_restore_state
	call	_ZN2v86String14NewFromTwoByteEPNS_7IsolateEPKtNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	jne	.L30
.L37:
	movq	%rax, -8(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-8(%rbp), %rax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5534:
	.size	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_10StringViewE, .-_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_10StringViewE
	.section	.text._ZN12v8_inspector16toProtocolStringEPN2v87IsolateENS0_5LocalINS0_6StringEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN12v8_inspector16toProtocolStringEPN2v87IsolateENS0_5LocalINS0_6StringEEE
	.type	_ZN12v8_inspector16toProtocolStringEPN2v87IsolateENS0_5LocalINS0_6StringEEE, @function
_ZN12v8_inspector16toProtocolStringEPN2v87IsolateENS0_5LocalINS0_6StringEEE:
.LFB5535:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	testq	%rdx, %rdx
	je	.L39
	movq	(%rdx), %rax
	movq	%rdx, %r12
	movq	%rsi, %r15
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	je	.L48
.L40:
	movq	%r12, %rdi
	call	_ZNK2v86String6LengthEv@PLT
	movabsq	$4611686018427387900, %rdx
	cltq
	cmpq	%rdx, %rax
	leaq	(%rax,%rax), %rdi
	movq	$-1, %rax
	cmova	%rax, %rdi
	call	_Znam@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZNK2v86String6LengthEv@PLT
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movl	%eax, %r8d
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZNK2v86String5WriteEPNS_7IsolateEPtiii@PLT
	movq	%r12, %rdi
	call	_ZNK2v86String6LengthEv@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movslq	%eax, %rdx
	call	_ZN12v8_inspector8String16C1EPKtm@PLT
	movq	%r14, %rdi
	call	_ZdaPv@PLT
	popq	%r12
	movq	%r13, %rax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	jne	.L40
	movslq	43(%rax), %rax
	subl	$3, %eax
	andl	$-3, %eax
	jne	.L40
.L39:
	leaq	16(%r13), %rax
	pxor	%xmm0, %xmm0
	popq	%r12
	movq	$0, 32(%r13)
	movq	%rax, 0(%r13)
	movq	%r13, %rax
	movq	$0, 8(%r13)
	movups	%xmm0, 16(%r13)
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5535:
	.size	_ZN12v8_inspector16toProtocolStringEPN2v87IsolateENS0_5LocalINS0_6StringEEE, .-_ZN12v8_inspector16toProtocolStringEPN2v87IsolateENS0_5LocalINS0_6StringEEE
	.section	.text._ZN12v8_inspector29toProtocolStringWithTypeCheckEPN2v87IsolateENS0_5LocalINS0_5ValueEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN12v8_inspector29toProtocolStringWithTypeCheckEPN2v87IsolateENS0_5LocalINS0_5ValueEEE
	.type	_ZN12v8_inspector29toProtocolStringWithTypeCheckEPN2v87IsolateENS0_5LocalINS0_5ValueEEE, @function
_ZN12v8_inspector29toProtocolStringWithTypeCheckEPN2v87IsolateENS0_5LocalINS0_5ValueEEE:
.LFB5538:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rcx
	movq	%rcx, -8(%rbp)
	xorl	%ecx, %ecx
	testq	%rdx, %rdx
	je	.L50
	movq	(%rdx), %rcx
	movq	%rcx, %rdi
	andl	$3, %edi
	cmpq	$1, %rdi
	je	.L57
.L50:
	leaq	16(%rax), %rdx
	movq	$0, 32(%rax)
	pxor	%xmm0, %xmm0
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movups	%xmm0, 16(%rax)
.L49:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L58
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	.cfi_restore_state
	movq	-1(%rcx), %rcx
	cmpw	$63, 11(%rcx)
	ja	.L50
	movq	%rax, %rdi
	movq	%rax, -24(%rbp)
	call	_ZN12v8_inspector16toProtocolStringEPN2v87IsolateENS0_5LocalINS0_6StringEEE
	movq	-24(%rbp), %rax
	jmp	.L49
.L58:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5538:
	.size	_ZN12v8_inspector29toProtocolStringWithTypeCheckEPN2v87IsolateENS0_5LocalINS0_5ValueEEE, .-_ZN12v8_inspector29toProtocolStringWithTypeCheckEPN2v87IsolateENS0_5LocalINS0_5ValueEEE
	.section	.text._ZN12v8_inspector10toString16ERKNS_10StringViewE,"ax",@progbits
	.p2align 4
	.globl	_ZN12v8_inspector10toString16ERKNS_10StringViewE
	.type	_ZN12v8_inspector10toString16ERKNS_10StringViewE, @function
_ZN12v8_inspector10toString16ERKNS_10StringViewE:
.LFB5539:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	8(%rsi), %rdx
	testq	%rdx, %rdx
	jne	.L60
	leaq	16(%rdi), %rax
	pxor	%xmm0, %xmm0
	movq	$0, 32(%rdi)
	movq	%rax, (%rdi)
	movq	%r12, %rax
	movq	$0, 8(%rdi)
	movups	%xmm0, 16(%rdi)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	.cfi_restore_state
	movq	16(%rsi), %r8
	cmpb	$0, (%rsi)
	movq	%r8, %rsi
	je	.L62
	call	_ZN12v8_inspector8String16C1EPKcm@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore_state
	call	_ZN12v8_inspector8String16C1EPKtm@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5539:
	.size	_ZN12v8_inspector10toString16ERKNS_10StringViewE, .-_ZN12v8_inspector10toString16ERKNS_10StringViewE
	.section	.text._ZN12v8_inspector12toStringViewERKNS_8String16E,"ax",@progbits
	.p2align 4
	.globl	_ZN12v8_inspector12toStringViewERKNS_8String16E
	.type	_ZN12v8_inspector12toStringViewERKNS_8String16E, @function
_ZN12v8_inspector12toStringViewERKNS_8String16E:
.LFB5540:
	.cfi_startproc
	endbr64
	movq	8(%rsi), %rdx
	movq	%rdi, %rax
	testq	%rdx, %rdx
	jne	.L65
	movb	$1, (%rdi)
	movq	$0, 8(%rdi)
	movq	$0, 16(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	movq	(%rsi), %rcx
	movb	$0, (%rdi)
	movq	%rdx, 8(%rdi)
	movq	%rcx, 16(%rdi)
	ret
	.cfi_endproc
.LFE5540:
	.size	_ZN12v8_inspector12toStringViewERKNS_8String16E, .-_ZN12v8_inspector12toStringViewERKNS_8String16E
	.section	.text._ZN12v8_inspector20stringViewStartsWithERKNS_10StringViewEPKc,"ax",@progbits
	.p2align 4
	.globl	_ZN12v8_inspector20stringViewStartsWithERKNS_10StringViewEPKc
	.type	_ZN12v8_inspector20stringViewStartsWithERKNS_10StringViewEPKc, @function
_ZN12v8_inspector20stringViewStartsWithERKNS_10StringViewEPKc:
.LFB5541:
	.cfi_startproc
	endbr64
	movsbl	(%rsi), %eax
	movq	8(%rdi), %rcx
	testb	%al, %al
	sete	%r9b
	testq	%rcx, %rcx
	je	.L67
	movzbl	(%rdi), %r9d
	testb	%r9b, %r9b
	je	.L70
	testb	%al, %al
	je	.L67
	movq	16(%rdi), %r8
	xorl	%edx, %edx
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L89:
	addq	$1, %rdx
	movsbl	(%rsi,%rdx), %eax
	testb	%al, %al
	je	.L67
	cmpq	%rcx, %rdx
	jnb	.L67
.L71:
	movzbl	(%r8,%rdx), %edi
	cmpl	%eax, %edi
	je	.L89
	xorl	%r9d, %r9d
.L67:
	movl	%r9d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	testb	%al, %al
	je	.L76
	movq	16(%rdi), %r8
	xorl	%edx, %edx
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L90:
	addq	$1, %rdx
	movsbl	(%rsi,%rdx), %eax
	cmpq	%rcx, %rdx
	jnb	.L76
	testb	%al, %al
	je	.L76
.L73:
	movzwl	(%r8,%rdx,2), %edi
	cmpl	%eax, %edi
	je	.L90
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L76:
	movl	$1, %r9d
	movl	%r9d, %eax
	ret
	.cfi_endproc
.LFE5541:
	.size	_ZN12v8_inspector20stringViewStartsWithERKNS_10StringViewEPKc, .-_ZN12v8_inspector20stringViewStartsWithERKNS_10StringViewEPKc
	.section	.text._ZN12v8_inspector8protocol10StringUtil8toDoubleEPKcmPb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol10StringUtil8toDoubleEPKcmPb
	.type	_ZN12v8_inspector8protocol10StringUtil8toDoubleEPKcmPb, @function
_ZN12v8_inspector8protocol10StringUtil8toDoubleEPKcmPb:
.LFB5542:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movl	$11, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdx, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal14StringToDoubleEPKcid@PLT
	ucomisd	%xmm0, %xmm0
	setnp	(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5542:
	.size	_ZN12v8_inspector8protocol10StringUtil8toDoubleEPKcmPb, .-_ZN12v8_inspector8protocol10StringUtil8toDoubleEPKcmPb
	.section	.text._ZN12v8_inspector8protocol10StringUtil9parseJSONERKNS_10StringViewE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol10StringUtil9parseJSONERKNS_10StringViewE
	.type	_ZN12v8_inspector8protocol10StringUtil9parseJSONERKNS_10StringViewE, @function
_ZN12v8_inspector8protocol10StringUtil9parseJSONERKNS_10StringViewE:
.LFB5543:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	8(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	jne	.L94
	movq	$0, (%rdi)
.L93:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L99
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	.cfi_restore_state
	movq	16(%rsi), %r8
	cmpb	$0, (%rsi)
	movq	%r8, %rsi
	je	.L96
	call	_ZN12v8_inspector8protocol19parseJSONCharactersEPKhj@PLT
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L96:
	call	_ZN12v8_inspector8protocol19parseJSONCharactersEPKtj@PLT
	jmp	.L93
.L99:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5543:
	.size	_ZN12v8_inspector8protocol10StringUtil9parseJSONERKNS_10StringViewE, .-_ZN12v8_inspector8protocol10StringUtil9parseJSONERKNS_10StringViewE
	.section	.text._ZN12v8_inspector8protocol10StringUtil9parseJSONERKNS_8String16E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol10StringUtil9parseJSONERKNS_8String16E
	.type	_ZN12v8_inspector8protocol10StringUtil9parseJSONERKNS_8String16E, @function
_ZN12v8_inspector8protocol10StringUtil9parseJSONERKNS_8String16E:
.LFB5544:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	8(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	jne	.L101
	movq	$0, (%rdi)
.L100:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L105
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L101:
	.cfi_restore_state
	movq	(%rsi), %rsi
	call	_ZN12v8_inspector8protocol19parseJSONCharactersEPKtj@PLT
	jmp	.L100
.L105:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5544:
	.size	_ZN12v8_inspector8protocol10StringUtil9parseJSONERKNS_8String16E, .-_ZN12v8_inspector8protocol10StringUtil9parseJSONERKNS_8String16E
	.section	.text._ZN12v8_inspector8protocol10StringUtil13jsonToMessageENS_8String16E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol10StringUtil13jsonToMessageENS_8String16E
	.type	_ZN12v8_inspector8protocol10StringUtil13jsonToMessageENS_8String16E, @function
_ZN12v8_inspector8protocol10StringUtil13jsonToMessageENS_8String16E:
.LFB5545:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	addq	$16, %rsi
	movq	%rdi, (%r12)
	movq	$0, 8(%r12)
	movw	%r8w, 16(%r12)
	movq	$0, 32(%r12)
	movq	$0, 56(%r12)
	movups	%xmm0, 40(%r12)
	movq	-16(%rsi), %rax
	cmpq	%rsi, %rax
	je	.L118
	movq	%rax, (%r12)
	movq	8(%rbx), %rax
	movq	%rsi, (%rbx)
	movq	%rax, 8(%r12)
	movq	16(%rbx), %rax
	movq	%rax, 16(%r12)
.L110:
	xorl	%eax, %eax
	movq	$0, 8(%rbx)
	movw	%ax, (%rsi)
	movq	32(%rbx), %rax
	popq	%rbx
	movq	%rax, 32(%r12)
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L118:
	.cfi_restore_state
	movq	8(%rbx), %rax
	leaq	(%rax,%rax), %rdx
	testq	%rax, %rax
	je	.L108
	cmpq	$1, %rax
	je	.L119
	testq	%rdx, %rdx
	je	.L108
	call	memmove@PLT
	movq	8(%rbx), %rax
	movq	(%rbx), %rsi
	leaq	(%rax,%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L108:
	xorl	%ecx, %ecx
	movq	%rax, 8(%r12)
	movw	%cx, 16(%r12,%rdx)
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L119:
	movzwl	16(%rbx), %edx
	movw	%dx, 16(%r12)
	movl	$2, %edx
	jmp	.L108
	.cfi_endproc
.LFE5545:
	.size	_ZN12v8_inspector8protocol10StringUtil13jsonToMessageENS_8String16E, .-_ZN12v8_inspector8protocol10StringUtil13jsonToMessageENS_8String16E
	.section	.text._ZN12v8_inspector8protocol10StringUtil15binaryToMessageESt6vectorIhSaIhEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol10StringUtil15binaryToMessageESt6vectorIhSaIhEE
	.type	_ZN12v8_inspector8protocol10StringUtil15binaryToMessageESt6vectorIhSaIhEE, @function
_ZN12v8_inspector8protocol10StringUtil15binaryToMessageESt6vectorIhSaIhEE:
.LFB5553:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	pxor	%xmm0, %xmm0
	movq	$0, 56(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	xorl	%edx, %edx
	movups	%xmm0, 40(%rdi)
	movq	$0, 8(%rdi)
	movw	%dx, 16(%rdi)
	movq	$0, 32(%rdi)
	movdqu	(%rsi), %xmm1
	movq	16(%rsi), %rdx
	movups	%xmm0, (%rsi)
	movq	$0, 16(%rsi)
	movq	%rdx, 56(%rdi)
	movups	%xmm1, 40(%rdi)
	ret
	.cfi_endproc
.LFE5553:
	.size	_ZN12v8_inspector8protocol10StringUtil15binaryToMessageESt6vectorIhSaIhEE, .-_ZN12v8_inspector8protocol10StringUtil15binaryToMessageESt6vectorIhSaIhEE
	.section	.text._ZN12v8_inspector8protocol10StringUtil25builderAppendQuotedStringERNS_15String16BuilderERKNS_8String16E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol10StringUtil25builderAppendQuotedStringERNS_15String16BuilderERKNS_8String16E
	.type	_ZN12v8_inspector8protocol10StringUtil25builderAppendQuotedStringERNS_15String16BuilderERKNS_8String16E, @function
_ZN12v8_inspector8protocol10StringUtil25builderAppendQuotedStringERNS_15String16BuilderERKNS_8String16E:
.LFB5557:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	movl	$34, %esi
	call	_ZN12v8_inspector15String16Builder6appendEc@PLT
	movq	8(%rbx), %rsi
	testq	%rsi, %rsi
	jne	.L127
.L122:
	popq	%rbx
	movq	%r12, %rdi
	movl	$34, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN12v8_inspector15String16Builder6appendEc@PLT
	.p2align 4,,10
	.p2align 3
.L127:
	.cfi_restore_state
	movq	(%rbx), %rdi
	movq	%r12, %rdx
	call	_ZN12v8_inspector8protocol23escapeWideStringForJSONEPKtjPNS_15String16BuilderE@PLT
	jmp	.L122
	.cfi_endproc
.LFE5557:
	.size	_ZN12v8_inspector8protocol10StringUtil25builderAppendQuotedStringERNS_15String16BuilderERKNS_8String16E, .-_ZN12v8_inspector8protocol10StringUtil25builderAppendQuotedStringERNS_15String16BuilderERKNS_8String16E
	.section	.text._ZN12v8_inspector12StringBuffer6createERKNS_10StringViewE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector12StringBuffer6createERKNS_10StringViewE
	.type	_ZN12v8_inspector12StringBuffer6createERKNS_10StringViewE, @function
_ZN12v8_inspector12StringBuffer6createERKNS_10StringViewE:
.LFB5558:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -32
	movq	8(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	jne	.L129
	leaq	-48(%rbp), %rbx
	movq	$0, -56(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rbx, -64(%rbp)
	movq	$0, -32(%rbp)
	movaps	%xmm0, -48(%rbp)
.L130:
	movl	$72, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN12v8_inspector16StringBufferImplE(%rip), %rdi
	leaq	24(%rax), %rdx
	movq	%rdi, (%rax)
	movq	%rdx, 8(%rax)
	xorl	%edx, %edx
	movw	%dx, 24(%rax)
	movq	-64(%rbp), %rdx
	movq	$0, 16(%rax)
	movq	$0, 40(%rax)
	movb	$1, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	cmpq	%rbx, %rdx
	je	.L140
	movq	-48(%rbp), %rcx
	movdqu	24(%rax), %xmm1
	movq	%rdx, 8(%rax)
	movq	-56(%rbp), %rdx
	movq	%rbx, -64(%rbp)
	movq	%rcx, 24(%rax)
	movq	-32(%rbp), %rcx
	movq	$0, -56(%rbp)
	movq	%rdx, 16(%rax)
	movq	%rcx, 40(%rax)
	movq	$0, -32(%rbp)
	movaps	%xmm1, -48(%rbp)
	testq	%rdx, %rdx
	je	.L137
.L134:
	movq	8(%rax), %rsi
	xorl	%ecx, %ecx
.L135:
	movb	%cl, 48(%rax)
	movq	%rdx, 56(%rax)
	movq	%rsi, 64(%rax)
	movq	%rax, (%r12)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L141
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L129:
	.cfi_restore_state
	movq	16(%rsi), %r8
	cmpb	$0, (%rsi)
	leaq	-64(%rbp), %rdi
	movq	%r8, %rsi
	je	.L131
	call	_ZN12v8_inspector8String16C1EPKcm@PLT
	leaq	-48(%rbp), %rbx
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L137:
	xorl	%esi, %esi
	movl	$1, %ecx
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L131:
	call	_ZN12v8_inspector8String16C1EPKtm@PLT
	leaq	-48(%rbp), %rbx
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L140:
	movq	-56(%rbp), %rdx
	testq	%rdx, %rdx
	jne	.L142
	movq	-32(%rbp), %rcx
	xorl	%esi, %esi
	movq	%rcx, 40(%rax)
	movl	$1, %ecx
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L142:
	movdqa	-48(%rbp), %xmm2
	movq	-32(%rbp), %rcx
	movq	%rdx, 16(%rax)
	movq	%rcx, 40(%rax)
	movups	%xmm2, 24(%rax)
	jmp	.L134
.L141:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5558:
	.size	_ZN12v8_inspector12StringBuffer6createERKNS_10StringViewE, .-_ZN12v8_inspector12StringBuffer6createERKNS_10StringViewE
	.section	.text._ZN12v8_inspector16StringBufferImpl5adoptERNS_8String16E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector16StringBufferImpl5adoptERNS_8String16E
	.type	_ZN12v8_inspector16StringBufferImpl5adoptERNS_8String16E, @function
_ZN12v8_inspector16StringBufferImpl5adoptERNS_8String16E:
.LFB5561:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	$72, %edi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_Znwm@PLT
	leaq	16+_ZTVN12v8_inspector16StringBufferImplE(%rip), %rdi
	xorl	%esi, %esi
	leaq	24(%rax), %rcx
	leaq	8(%rax), %rdx
	movq	%rdi, (%rax)
	movq	%rcx, 8(%rax)
	movq	$0, 16(%rax)
	movw	%si, 24(%rax)
	movq	$0, 40(%rax)
	movb	$1, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	cmpq	%rdx, %rbx
	je	.L152
	movq	(%rbx), %rdi
	leaq	16(%rbx), %rsi
	movq	8(%rbx), %rdx
	cmpq	%rsi, %rdi
	je	.L153
	movq	16(%rbx), %rcx
	movdqu	24(%rax), %xmm0
	movq	%rdi, 8(%rax)
	movq	%rsi, (%rbx)
	movq	%rcx, 24(%rax)
	movq	32(%rbx), %rcx
	movq	%rdx, 16(%rax)
	movq	$0, 8(%rbx)
	movq	%rcx, 40(%rax)
	movq	$0, 32(%rbx)
	movups	%xmm0, 16(%rbx)
	testq	%rdx, %rdx
	je	.L149
.L148:
	movq	8(%rax), %rcx
	xorl	%esi, %esi
.L145:
	movb	%sil, 48(%rax)
	movq	%rdx, 56(%rax)
	movq	%rcx, 64(%rax)
	popq	%rbx
	movq	%rax, (%r12)
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L153:
	.cfi_restore_state
	testq	%rdx, %rdx
	jne	.L154
	movq	32(%rbx), %rdx
	movq	$0, 32(%rbx)
	movq	%rdx, 40(%rax)
	.p2align 4,,10
	.p2align 3
.L149:
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movl	$1, %esi
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L152:
	movq	32(%rbx), %rdx
	xorl	%ecx, %ecx
	movq	$0, 32(%rbx)
	movl	$1, %esi
	movq	%rdx, 40(%rax)
	xorl	%edx, %edx
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L154:
	xorl	%ecx, %ecx
	movdqu	16(%rbx), %xmm1
	movq	%rdx, 16(%rax)
	movw	%cx, 16(%rbx)
	movq	32(%rbx), %rcx
	movq	$0, 8(%rbx)
	movq	%rcx, 40(%rax)
	movq	$0, 32(%rbx)
	movups	%xmm1, 24(%rax)
	jmp	.L148
	.cfi_endproc
.LFE5561:
	.size	_ZN12v8_inspector16StringBufferImpl5adoptERNS_8String16E, .-_ZN12v8_inspector16StringBufferImpl5adoptERNS_8String16E
	.section	.text._ZN12v8_inspector16StringBufferImplC2ERNS_8String16E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector16StringBufferImplC2ERNS_8String16E
	.type	_ZN12v8_inspector16StringBufferImplC2ERNS_8String16E, @function
_ZN12v8_inspector16StringBufferImplC2ERNS_8String16E:
.LFB5563:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN12v8_inspector16StringBufferImplE(%rip), %rax
	leaq	24(%rdi), %rdx
	xorl	%ecx, %ecx
	movq	$0, 16(%rdi)
	movq	%rax, (%rdi)
	leaq	8(%rdi), %rax
	movq	%rdx, 8(%rdi)
	movw	%cx, 24(%rdi)
	movq	$0, 40(%rdi)
	movb	$1, 48(%rdi)
	movq	$0, 56(%rdi)
	movq	$0, 64(%rdi)
	cmpq	%rax, %rsi
	je	.L163
	movq	(%rsi), %r8
	leaq	16(%rsi), %rcx
	movq	8(%rsi), %rax
	cmpq	%rcx, %r8
	je	.L164
	movq	16(%rsi), %rdx
	movdqu	24(%rdi), %xmm0
	movq	%r8, 8(%rdi)
	movq	%rcx, (%rsi)
	movq	%rdx, 24(%rdi)
	movq	32(%rsi), %rdx
	movq	%rax, 16(%rdi)
	movq	$0, 8(%rsi)
	movq	%rdx, 40(%rdi)
	movq	$0, 32(%rsi)
	movups	%xmm0, 16(%rsi)
	testq	%rax, %rax
	je	.L161
.L160:
	movq	8(%rdi), %rdx
	xorl	%ecx, %ecx
	movq	%rax, 56(%rdi)
	movb	%cl, 48(%rdi)
	movq	%rdx, 64(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L164:
	testq	%rax, %rax
	jne	.L165
	movq	32(%rsi), %rax
	movq	$0, 32(%rsi)
	movq	%rax, 40(%rdi)
.L161:
	xorl	%eax, %eax
	xorl	%edx, %edx
	movl	$1, %ecx
	movb	%cl, 48(%rdi)
	movq	%rax, 56(%rdi)
	movq	%rdx, 64(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L163:
	movq	32(%rsi), %rax
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	$0, 32(%rsi)
	movb	%cl, 48(%rdi)
	movq	%rax, 40(%rdi)
	xorl	%eax, %eax
	movq	%rax, 56(%rdi)
	movq	%rdx, 64(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L165:
	xorl	%edx, %edx
	movdqu	16(%rsi), %xmm1
	movq	%rax, 16(%rdi)
	movw	%dx, 16(%rsi)
	movq	32(%rsi), %rdx
	movq	$0, 8(%rsi)
	movq	%rdx, 40(%rdi)
	movq	$0, 32(%rsi)
	movups	%xmm1, 24(%rdi)
	jmp	.L160
	.cfi_endproc
.LFE5563:
	.size	_ZN12v8_inspector16StringBufferImplC2ERNS_8String16E, .-_ZN12v8_inspector16StringBufferImplC2ERNS_8String16E
	.globl	_ZN12v8_inspector16StringBufferImplC1ERNS_8String16E
	.set	_ZN12v8_inspector16StringBufferImplC1ERNS_8String16E,_ZN12v8_inspector16StringBufferImplC2ERNS_8String16E
	.section	.rodata._ZN12v8_inspector18debuggerIdToStringERKSt4pairIllE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"(%08lX%08lX)"
	.section	.text._ZN12v8_inspector18debuggerIdToStringERKSt4pairIllE,"ax",@progbits
	.p2align 4
	.globl	_ZN12v8_inspector18debuggerIdToStringERKSt4pairIllE
	.type	_ZN12v8_inspector18debuggerIdToStringERKSt4pairIllE, @function
_ZN12v8_inspector18debuggerIdToStringERKSt4pairIllE:
.LFB5565:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$48, %rsp
	movq	8(%rsi), %r8
	movq	(%rsi), %rcx
	movl	$35, %esi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v84base2OS8SNPrintFEPciPKcz@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L169
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L169:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5565:
	.size	_ZN12v8_inspector18debuggerIdToStringERKSt4pairIllE, .-_ZN12v8_inspector18debuggerIdToStringERKSt4pairIllE
	.section	.text._ZN12v8_inspector20stackTraceIdToStringEm,"ax",@progbits
	.p2align 4
	.globl	_ZN12v8_inspector20stackTraceIdToStringEm
	.type	_ZN12v8_inspector20stackTraceIdToStringEm, @function
_ZN12v8_inspector20stackTraceIdToStringEm:
.LFB5566:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-64(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector15String16BuilderC1Ev@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector15String16Builder12appendNumberEm@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector15String16Builder8toStringEv@PLT
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L170
	call	_ZdlPv@PLT
.L170:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L177
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L177:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5566:
	.size	_ZN12v8_inspector20stackTraceIdToStringEm, .-_ZN12v8_inspector20stackTraceIdToStringEm
	.section	.text.startup._GLOBAL__sub_I__ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E, @function
_GLOBAL__sub_I__ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E:
.LFB7580:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE7580:
	.size	_GLOBAL__sub_I__ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E, .-_GLOBAL__sub_I__ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E
	.weak	_ZTVN12v8_inspector16StringBufferImplE
	.section	.data.rel.ro.local._ZTVN12v8_inspector16StringBufferImplE,"awG",@progbits,_ZTVN12v8_inspector16StringBufferImplE,comdat
	.align 8
	.type	_ZTVN12v8_inspector16StringBufferImplE, @object
	.size	_ZTVN12v8_inspector16StringBufferImplE, 40
_ZTVN12v8_inspector16StringBufferImplE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector16StringBufferImplD1Ev
	.quad	_ZN12v8_inspector16StringBufferImplD0Ev
	.quad	_ZN12v8_inspector16StringBufferImpl6stringEv
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
