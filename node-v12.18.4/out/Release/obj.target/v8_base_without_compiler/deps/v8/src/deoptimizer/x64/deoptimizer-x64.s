	.file	"deoptimizer-x64.cc"
	.text
	.section	.text._ZN2v88internal11Deoptimizer29GenerateDeoptimizationEntriesEPNS0_14MacroAssemblerEPNS0_7IsolateENS0_14DeoptimizeKindE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11Deoptimizer29GenerateDeoptimizationEntriesEPNS0_14MacroAssemblerEPNS0_7IsolateENS0_14DeoptimizeKindE
	.type	_ZN2v88internal11Deoptimizer29GenerateDeoptimizationEntriesEPNS0_14MacroAssemblerEPNS0_7IsolateENS0_14DeoptimizeKindE, @function
_ZN2v88internal11Deoptimizer29GenerateDeoptimizationEntriesEPNS0_14MacroAssemblerEPNS0_7IsolateENS0_14DeoptimizeKindE:
.LFB19493:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %r8d
	movabsq	$81604378752, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$216, %rsp
	movq	%rsi, -216(%rbp)
	movl	$5, %esi
	movb	%dl, -224(%rbp)
	movl	$4, %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	528(%rdi), %eax
	movb	$0, 528(%rdi)
	movb	%al, -225(%rbp)
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	call	_ZN2v88internal21RegisterConfiguration7DefaultEv@PLT
	movl	32(%rax), %edx
	movq	%rax, %r15
	testl	%edx, %edx
	jle	.L32
	movl	_ZN2v88internalL3rspE(%rip), %eax
	xorl	%r12d, %r12d
	leaq	-116(%rbp), %r13
	movq	%r15, %r14
	movl	%eax, -200(%rbp)
	.p2align 4,,10
	.p2align 3
.L2:
	movq	192(%r14), %rdx
	movl	-200(%rbp), %esi
	movq	%r13, %rdi
	movl	(%rdx,%r12,4), %r15d
	leal	0(,%r15,8), %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-116(%rbp), %r8
	movl	-108(%rbp), %edx
	movq	%r8, -104(%rbp)
	movl	%edx, -96(%rbp)
	movq	%r8, -92(%rbp)
	movl	%edx, -84(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L4
	subq	$8, %rsp
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	%edx, -72(%rbp)
	movl	%edx, %r9d
	pushq	$0
	movl	$17, %esi
	movq	%rbx, %rdi
	addq	$1, %r12
	pushq	$1
	pushq	$3
	movl	%edx, -60(%rbp)
	movl	%r15d, %edx
	movq	%r8, -80(%rbp)
	movq	%r8, -68(%rbp)
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	addq	$32, %rsp
	cmpl	%r12d, 32(%r14)
	jg	.L2
.L29:
	movq	%r14, %r15
.L6:
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L3:
	movl	%r12d, %esi
	movq	%rbx, %rdi
	addl	$1, %r12d
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	cmpl	$16, %r12d
	jne	.L3
	movq	-216(%rbp), %rsi
	movl	$1, %edi
	leaq	-80(%rbp), %r14
	call	_ZN2v88internal17ExternalReference6CreateENS0_16IsolateAddressIdEPNS0_7IsolateE@PLT
	movl	_ZN2v88internalL3rbpE(%rip), %r12d
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movl	%r12d, %edx
	call	_ZN2v88internal14MacroAssembler5StoreENS0_17ExternalReferenceENS0_8RegisterE@PLT
	movl	_ZN2v88internalL3r13E(%rip), %edx
	movl	$8, %ecx
	movq	%rbx, %rdi
	movl	_ZN2v88internalL9arg_reg_3E(%rip), %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movl	-200(%rbp), %r13d
	movq	%r14, %rdi
	movl	$256, %edx
	movq	%r14, -208(%rbp)
	movl	%r13d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-72(%rbp), %ecx
	movq	-80(%rbp), %rdx
	movq	%rbx, %rdi
	movl	_ZN2v88internalL9arg_reg_4E(%rip), %esi
	movl	$8, %r8d
	movl	%ecx, -60(%rbp)
	movq	%rdx, -68(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	%r13d, %esi
	movq	%r14, %rdi
	movl	$264, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-80(%rbp), %rdx
	movl	-72(%rbp), %ecx
	movq	%rbx, %rdi
	movl	$8, %r8d
	movl	$11, %esi
	leaq	-184(%rbp), %r13
	movq	%rdx, -68(%rbp)
	movl	%ecx, -60(%rbp)
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	movl	$8, %r8d
	movl	$5, %ecx
	movq	%rbx, %rdi
	movl	$11, %edx
	movl	$43, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movl	$8, %edx
	movl	$11, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler8emit_negENS0_8RegisterEi@PLT
	movl	$6, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal14TurboAssembler20PrepareCallCFunctionEi@PLT
	movl	_ZN2v88internalL3raxE(%rip), %esi
	movl	$8, %ecx
	movq	%rbx, %rdi
	movabsq	$81604378624, %rdx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_9ImmediateEi@PLT
	movl	%r12d, %esi
	movq	%r14, %rdi
	movl	$-8, %edx
	movq	$0, -184(%rbp)
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-80(%rbp), %rdx
	movl	-72(%rbp), %ecx
	movq	%rbx, %rdi
	movl	_ZN2v88internalL3rdiE(%rip), %esi
	movl	$8, %r8d
	movq	%rdx, -68(%rbp)
	movl	%ecx, -60(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	$1, %ecx
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movl	_ZN2v88internalL3rdiE(%rip), %esi
	call	_ZN2v88internal14TurboAssembler9JumpIfSmiENS0_8RegisterEPNS0_5LabelENS3_8DistanceE@PLT
	movl	%r12d, %esi
	movq	%r14, %rdi
	movl	$-16, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-80(%rbp), %rdx
	movl	-72(%rbp), %ecx
	movq	%rbx, %rdi
	movl	_ZN2v88internalL3raxE(%rip), %esi
	movl	$8, %r8d
	movq	%rdx, -68(%rbp)
	movl	%ecx, -60(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movl	$136, %r13d
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	_ZN2v88internalL3raxE(%rip), %edx
	movl	$8, %ecx
	movq	%rbx, %rdi
	movl	_ZN2v88internalL9arg_reg_1E(%rip), %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movzbl	-224(%rbp), %edx
	movl	_ZN2v88internalL9arg_reg_2E(%rip), %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal14TurboAssembler3SetENS0_8RegisterEl@PLT
	movl	_ZN2v88internalL2r8E(%rip), %esi
	movl	$8, %ecx
	movq	%rbx, %rdi
	movl	$11, %edx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	-216(%rbp), %rdi
	call	_ZN2v88internal17ExternalReference15isolate_addressEPNS0_7IsolateE@PLT
	movl	_ZN2v88internalL2r9E(%rip), %esi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal14TurboAssembler11LoadAddressENS0_8RegisterENS0_17ExternalReferenceE@PLT
	movzbl	536(%rbx), %r12d
	movb	$1, 536(%rbx)
	call	_ZN2v88internal17ExternalReference24new_deoptimizer_functionEv@PLT
	movl	$6, %edx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler13CallCFunctionENS0_17ExternalReferenceEi@PLT
	movb	%r12b, 536(%rbx)
	movl	_ZN2v88internalL3raxE(%rip), %esi
	movq	%r14, %rdi
	movl	$56, %edx
	leaq	-68(%rbp), %r14
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-80(%rbp), %rdx
	movl	-72(%rbp), %ecx
	movq	%rbx, %rdi
	movl	_ZN2v88internalL3rbxE(%rip), %r12d
	movl	$8, %r8d
	movq	%rdx, -68(%rbp)
	movl	%r12d, %esi
	movl	%ecx, -60(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	.p2align 4,,10
	.p2align 3
.L7:
	movl	%r13d, %edx
	movl	%r12d, %esi
	movq	%r14, %rdi
	subl	$8, %r13d
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-68(%rbp), %rsi
	movl	-60(%rbp), %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal14MacroAssembler7PopQuadENS0_7OperandE@PLT
	cmpl	$8, %r13d
	jne	.L7
	movl	$144, %r13d
	.p2align 4,,10
	.p2align 3
.L8:
	movl	%r13d, %edx
	movl	%r12d, %esi
	movq	%r14, %rdi
	addl	$8, %r13d
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-68(%rbp), %rsi
	movl	-60(%rbp), %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_7OperandE@PLT
	cmpl	$272, %r13d
	jne	.L8
	movq	-216(%rbp), %rdi
	movabsq	$81604378624, %r13
	call	_ZN2v88internal17ExternalReference25stack_is_iterable_addressEPNS0_7IsolateE@PLT
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %edx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler26ExternalReferenceAsOperandENS0_17ExternalReferenceENS0_8RegisterE@PLT
	movq	%r13, %rcx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movq	%rax, -116(%rbp)
	movl	%edx, -108(%rbp)
	call	_ZN2v88internal9Assembler4movbENS0_7OperandENS0_9ImmediateE@PLT
	movq	%r13, %rcx
	movl	$8, %r8d
	xorl	%esi, %esi
	orq	$8, %rcx
	movl	$4, %edx
	movq	%rbx, %rdi
	movq	%r13, -224(%rbp)
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movq	-208(%rbp), %r13
	xorl	%edx, %edx
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-80(%rbp), %rdx
	movl	-72(%rbp), %ecx
	movq	%rbx, %rdi
	movl	_ZN2v88internalL3rcxE(%rip), %r10d
	movl	$8, %r8d
	movq	%rdx, -68(%rbp)
	movl	%r10d, %esi
	movl	%ecx, -60(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	$8, %r8d
	movl	$4, %ecx
	movq	%rbx, %rdi
	movl	$1, %edx
	movl	$3, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	%r13, %rdi
	movl	$320, %edx
	movl	%r12d, %esi
	movq	%r13, -208(%rbp)
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-72(%rbp), %ecx
	movq	-80(%rbp), %rdx
	movq	%rbx, %rdi
	movl	_ZN2v88internalL3rdxE(%rip), %r13d
	movl	$8, %r8d
	movl	%ecx, -60(%rbp)
	movl	%r13d, %esi
	movq	%rdx, -68(%rbp)
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	leaq	-176(%rbp), %r11
	movl	$1, %edx
	movq	%rbx, %rdi
	movq	%r11, %rsi
	movq	%r11, -248(%rbp)
	movq	$0, -176(%rbp)
	call	_ZN2v88internal9Assembler3jmpEPNS0_5LabelENS2_8DistanceE@PLT
	leaq	-168(%rbp), %r9
	movq	%rbx, %rdi
	movq	$0, -168(%rbp)
	movq	%r9, %rsi
	movq	%r9, -240(%rbp)
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	xorl	%edx, %edx
	movl	%r13d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-68(%rbp), %rsi
	movl	-60(%rbp), %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal14MacroAssembler3PopENS0_7OperandE@PLT
	movl	$8, %r8d
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	-224(%rbp), %rcx
	movl	$2, %edx
	orq	$8, %rcx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movq	-248(%rbp), %r11
	movq	%rbx, %rdi
	movq	%r11, %rsi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	$8, %r8d
	movl	$4, %ecx
	movq	%rbx, %rdi
	movl	$1, %edx
	movl	$59, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	-240(%rbp), %r9
	movl	$1, %ecx
	movq	%rbx, %rdi
	movl	$5, %esi
	movq	%r9, %rdx
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	_ZN2v88internalL3raxE(%rip), %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	movl	$2, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal14TurboAssembler20PrepareCallCFunctionEi@PLT
	movl	_ZN2v88internalL3raxE(%rip), %edx
	movl	$8, %ecx
	movq	%rbx, %rdi
	movl	_ZN2v88internalL9arg_reg_1E(%rip), %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	-216(%rbp), %rdi
	call	_ZN2v88internal17ExternalReference15isolate_addressEPNS0_7IsolateE@PLT
	movl	_ZN2v88internalL9arg_reg_2E(%rip), %esi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal14TurboAssembler11LoadAddressENS0_8RegisterENS0_17ExternalReferenceE@PLT
	movzbl	536(%rbx), %ecx
	movb	$1, 536(%rbx)
	movb	%cl, -240(%rbp)
	call	_ZN2v88internal17ExternalReference30compute_output_frames_functionEv@PLT
	movl	$2, %edx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler13CallCFunctionENS0_17ExternalReferenceEi@PLT
	movzbl	-240(%rbp), %ecx
	movl	_ZN2v88internalL3raxE(%rip), %esi
	movq	%rbx, %rdi
	movb	%cl, 536(%rbx)
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	movl	_ZN2v88internalL3raxE(%rip), %esi
	movl	$80, %edx
	movq	-208(%rbp), %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-72(%rbp), %ecx
	movq	-80(%rbp), %rdx
	movq	%rbx, %rdi
	movl	-200(%rbp), %esi
	movl	$8, %r8d
	movl	%ecx, -60(%rbp)
	movq	%rdx, -68(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	_ZN2v88internalL3raxE(%rip), %esi
	movl	$64, %edx
	movq	-208(%rbp), %rdi
	movq	$0, -160(%rbp)
	movq	$0, -152(%rbp)
	movq	$0, -144(%rbp)
	movq	$0, -136(%rbp)
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-72(%rbp), %ecx
	movq	-80(%rbp), %rdx
	movl	%r13d, %esi
	movl	$4, %r8d
	movq	%rbx, %rdi
	movl	%ecx, -60(%rbp)
	movq	%rdx, -68(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	_ZN2v88internalL3raxE(%rip), %esi
	movl	$72, %edx
	movq	-208(%rbp), %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-80(%rbp), %rdx
	movl	-72(%rbp), %ecx
	movq	%rbx, %rdi
	movl	_ZN2v88internalL3raxE(%rip), %esi
	movl	$8, %r8d
	movq	%rdx, -68(%rbp)
	movl	%ecx, -60(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	xorl	%r8d, %r8d
	movl	%r13d, %edx
	movl	$3, %ecx
	movl	_ZN2v88internalL3raxE(%rip), %esi
	movq	-208(%rbp), %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movl	-72(%rbp), %ecx
	movq	-80(%rbp), %rdx
	movl	%r13d, %esi
	movl	$8, %r8d
	leaq	-144(%rbp), %r13
	movq	%rbx, %rdi
	movl	%ecx, -60(%rbp)
	movq	%rdx, -68(%rbp)
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler3jmpEPNS0_5LabelENS2_8DistanceE@PLT
	leaq	-160(%rbp), %r11
	movq	%rbx, %rdi
	movq	%r11, %rsi
	movq	%r11, -240(%rbp)
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	_ZN2v88internalL3raxE(%rip), %esi
	movq	-208(%rbp), %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-72(%rbp), %ecx
	movq	-80(%rbp), %rdx
	movl	%r12d, %esi
	movl	$8, %r8d
	movq	%rbx, %rdi
	movl	%ecx, -60(%rbp)
	movq	%rdx, -68(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movq	-208(%rbp), %rdi
	xorl	%edx, %edx
	movl	%r12d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-72(%rbp), %ecx
	movq	-80(%rbp), %rdx
	movq	%rbx, %rdi
	movl	_ZN2v88internalL3rcxE(%rip), %r10d
	movl	$8, %r8d
	movl	%ecx, -60(%rbp)
	movl	%r10d, %esi
	movq	%rdx, -68(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	leaq	-136(%rbp), %r9
	movl	$1, %edx
	movq	%rbx, %rdi
	movq	%r9, %rsi
	movq	%r9, -208(%rbp)
	call	_ZN2v88internal9Assembler3jmpEPNS0_5LabelENS2_8DistanceE@PLT
	leaq	-152(%rbp), %rax
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	-224(%rbp), %rcx
	movl	$8, %r8d
	movq	%rbx, %rdi
	movl	$1, %edx
	movl	$5, %esi
	orq	$8, %rcx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movl	$320, %r8d
	xorl	%ecx, %ecx
	movl	%r12d, %esi
	movl	_ZN2v88internalL3rcxE(%rip), %r10d
	movq	%r14, %rdi
	movl	%r10d, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movl	-60(%rbp), %edx
	movq	-68(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal14TurboAssembler4PushENS0_7OperandE@PLT
	movq	-208(%rbp), %r9
	movq	%rbx, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	_ZN2v88internalL3rcxE(%rip), %r10d
	movl	$8, %ecx
	movq	%rbx, %rdi
	movl	%r10d, %edx
	movl	%r10d, %esi
	call	_ZN2v88internal9Assembler9emit_testENS0_8RegisterES2_i@PLT
	movq	-200(%rbp), %rax
	movl	$1, %ecx
	movq	%rbx, %rdi
	movl	$5, %esi
	movq	%rax, %rdx
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	-224(%rbp), %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	$8, %r8d
	movq	%rbx, %rdi
	orq	$8, %rcx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	$8, %r8d
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movl	$2, %ecx
	movl	$59, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movl	$1, %ecx
	movq	%rbx, %rdi
	xorl	%r13d, %r13d
	movq	-240(%rbp), %r11
	movl	$2, %esi
	movq	%r11, %rdx
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	leaq	-128(%rbp), %rax
	movq	%rax, -200(%rbp)
	movl	32(%r15), %eax
	testl	%eax, %eax
	jle	.L13
	movq	%r14, -208(%rbp)
	movq	%r13, %rax
	movq	%r15, %r13
	movq	%rax, %r15
	.p2align 4,,10
	.p2align 3
.L9:
	movq	192(%r13), %rdx
	movq	-200(%rbp), %rdi
	movl	%r12d, %esi
	movl	(%rdx,%r15,4), %r14d
	leal	144(,%r14,8), %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-128(%rbp), %r8
	movl	-120(%rbp), %r9d
	movq	%r8, -104(%rbp)
	movl	%r9d, -96(%rbp)
	movq	%r8, -92(%rbp)
	movl	%r9d, -84(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L11
	subq	$8, %rsp
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	%r14d, %edx
	movq	%rbx, %rdi
	pushq	$0
	movl	$16, %esi
	addq	$1, %r15
	pushq	$1
	pushq	$3
	movq	%r8, -80(%rbp)
	movl	%r9d, -72(%rbp)
	movq	%r8, -68(%rbp)
	movl	%r9d, -60(%rbp)
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	addq	$32, %rsp
	cmpl	%r15d, 32(%r13)
	jg	.L9
.L30:
	movq	-208(%rbp), %r14
.L13:
	movl	$280, %edx
	movl	%r12d, %esi
	movq	%r14, %rdi
	movl	$16, %r13d
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-68(%rbp), %rsi
	movl	-60(%rbp), %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal14MacroAssembler8PushQuadENS0_7OperandE@PLT
	movl	$312, %edx
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-68(%rbp), %rsi
	movl	-60(%rbp), %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal14MacroAssembler8PushQuadENS0_7OperandE@PLT
	.p2align 4,,10
	.p2align 3
.L10:
	movl	%r13d, %edx
	movl	%r12d, %esi
	movq	%r14, %rdi
	addl	$8, %r13d
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-68(%rbp), %rsi
	movl	-60(%rbp), %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal14MacroAssembler8PushQuadENS0_7OperandE@PLT
	cmpl	$144, %r13d
	jne	.L10
	movl	$15, %r12d
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L33:
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	testl	%r12d, %r12d
	je	.L15
.L17:
	subl	$1, %r12d
.L16:
	cmpl	$4, %r12d
	jne	.L33
	movl	$3, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L4:
	movl	%r15d, %ecx
	movq	%r8, %rsi
	movq	%rbx, %rdi
	addq	$1, %r12
	call	_ZN2v88internal9Assembler5movsdENS0_7OperandENS0_11XMMRegisterE@PLT
	cmpl	%r12d, 32(%r14)
	jg	.L2
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L11:
	movl	-84(%rbp), %ecx
	movq	%r8, %rdx
	movl	%r14d, %esi
	movq	%rbx, %rdi
	addq	$1, %r15
	call	_ZN2v88internal9Assembler5movsdENS0_11XMMRegisterENS0_7OperandE@PLT
	cmpl	%r15d, 32(%r13)
	jg	.L9
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L15:
	movq	-216(%rbp), %rdi
	call	_ZN2v88internal17ExternalReference25stack_is_iterable_addressEPNS0_7IsolateE@PLT
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %edx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler26ExternalReferenceAsOperandENS0_17ExternalReferenceENS0_8RegisterE@PLT
	movq	%rbx, %rdi
	movabsq	$81604378625, %rcx
	movq	%rax, %rsi
	movq	%rax, -68(%rbp)
	movl	%edx, -60(%rbp)
	call	_ZN2v88internal9Assembler4movbENS0_7OperandENS0_9ImmediateE@PLT
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler3retEi@PLT
	movzbl	-225(%rbp), %eax
	movb	%al, 528(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L34
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L32:
	.cfi_restore_state
	movl	_ZN2v88internalL3rspE(%rip), %eax
	movl	%eax, -200(%rbp)
	jmp	.L6
.L34:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19493:
	.size	_ZN2v88internal11Deoptimizer29GenerateDeoptimizationEntriesEPNS0_14MacroAssemblerEPNS0_7IsolateENS0_14DeoptimizeKindE, .-_ZN2v88internal11Deoptimizer29GenerateDeoptimizationEntriesEPNS0_14MacroAssemblerEPNS0_7IsolateENS0_14DeoptimizeKindE
	.section	.text._ZNK2v88internal14RegisterValues16GetFloatRegisterEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal14RegisterValues16GetFloatRegisterEj
	.type	_ZNK2v88internal14RegisterValues16GetFloatRegisterEj, @function
_ZNK2v88internal14RegisterValues16GetFloatRegisterEj:
.LFB19501:
	.cfi_startproc
	endbr64
	movl	%esi, %esi
	movq	128(%rdi,%rsi,8), %rax
	ret
	.cfi_endproc
.LFE19501:
	.size	_ZNK2v88internal14RegisterValues16GetFloatRegisterEj, .-_ZNK2v88internal14RegisterValues16GetFloatRegisterEj
	.section	.text._ZN2v88internal16FrameDescription11SetCallerPcEjl,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16FrameDescription11SetCallerPcEjl
	.type	_ZN2v88internal16FrameDescription11SetCallerPcEjl, @function
_ZN2v88internal16FrameDescription11SetCallerPcEjl:
.LFB19502:
	.cfi_startproc
	endbr64
	movl	%esi, %esi
	movq	%rdx, 320(%rsi,%rdi)
	ret
	.cfi_endproc
.LFE19502:
	.size	_ZN2v88internal16FrameDescription11SetCallerPcEjl, .-_ZN2v88internal16FrameDescription11SetCallerPcEjl
	.section	.text._ZN2v88internal16FrameDescription11SetCallerFpEjl,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16FrameDescription11SetCallerFpEjl
	.type	_ZN2v88internal16FrameDescription11SetCallerFpEjl, @function
_ZN2v88internal16FrameDescription11SetCallerFpEjl:
.LFB23627:
	.cfi_startproc
	endbr64
	movl	%esi, %esi
	movq	%rdx, 320(%rsi,%rdi)
	ret
	.cfi_endproc
.LFE23627:
	.size	_ZN2v88internal16FrameDescription11SetCallerFpEjl, .-_ZN2v88internal16FrameDescription11SetCallerFpEjl
	.section	.rodata._ZN2v88internal16FrameDescription21SetCallerConstantPoolEjl.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal16FrameDescription21SetCallerConstantPoolEjl,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16FrameDescription21SetCallerConstantPoolEjl
	.type	_ZN2v88internal16FrameDescription21SetCallerConstantPoolEjl, @function
_ZN2v88internal16FrameDescription21SetCallerConstantPoolEjl:
.LFB19504:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19504:
	.size	_ZN2v88internal16FrameDescription21SetCallerConstantPoolEjl, .-_ZN2v88internal16FrameDescription21SetCallerConstantPoolEjl
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal11Deoptimizer27kSupportsFixedDeoptExitSizeE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal11Deoptimizer27kSupportsFixedDeoptExitSizeE, @function
_GLOBAL__sub_I__ZN2v88internal11Deoptimizer27kSupportsFixedDeoptExitSizeE:
.LFB23617:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE23617:
	.size	_GLOBAL__sub_I__ZN2v88internal11Deoptimizer27kSupportsFixedDeoptExitSizeE, .-_GLOBAL__sub_I__ZN2v88internal11Deoptimizer27kSupportsFixedDeoptExitSizeE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal11Deoptimizer27kSupportsFixedDeoptExitSizeE
	.globl	_ZN2v88internal11Deoptimizer14kDeoptExitSizeE
	.section	.rodata._ZN2v88internal11Deoptimizer14kDeoptExitSizeE,"a"
	.align 4
	.type	_ZN2v88internal11Deoptimizer14kDeoptExitSizeE, @object
	.size	_ZN2v88internal11Deoptimizer14kDeoptExitSizeE, 4
_ZN2v88internal11Deoptimizer14kDeoptExitSizeE:
	.zero	4
	.globl	_ZN2v88internal11Deoptimizer27kSupportsFixedDeoptExitSizeE
	.section	.rodata._ZN2v88internal11Deoptimizer27kSupportsFixedDeoptExitSizeE,"a"
	.type	_ZN2v88internal11Deoptimizer27kSupportsFixedDeoptExitSizeE, @object
	.size	_ZN2v88internal11Deoptimizer27kSupportsFixedDeoptExitSizeE, 1
_ZN2v88internal11Deoptimizer27kSupportsFixedDeoptExitSizeE:
	.zero	1
	.section	.rodata._ZN2v88internalL16kScratchRegisterE,"a"
	.align 4
	.type	_ZN2v88internalL16kScratchRegisterE, @object
	.size	_ZN2v88internalL16kScratchRegisterE, 4
_ZN2v88internalL16kScratchRegisterE:
	.long	10
	.section	.rodata._ZN2v88internalL4xmm0E,"a"
	.align 4
	.type	_ZN2v88internalL4xmm0E, @object
	.size	_ZN2v88internalL4xmm0E, 4
_ZN2v88internalL4xmm0E:
	.zero	4
	.section	.rodata._ZN2v88internalL9arg_reg_2E,"a"
	.align 4
	.type	_ZN2v88internalL9arg_reg_2E, @object
	.size	_ZN2v88internalL9arg_reg_2E, 4
_ZN2v88internalL9arg_reg_2E:
	.long	6
	.section	.rodata._ZN2v88internalL3r13E,"a"
	.align 4
	.type	_ZN2v88internalL3r13E, @object
	.size	_ZN2v88internalL3r13E, 4
_ZN2v88internalL3r13E:
	.long	13
	.section	.rodata._ZN2v88internalL2r9E,"a"
	.align 4
	.type	_ZN2v88internalL2r9E, @object
	.size	_ZN2v88internalL2r9E, 4
_ZN2v88internalL2r9E:
	.long	9
	.section	.rodata._ZN2v88internalL2r8E,"a"
	.align 4
	.type	_ZN2v88internalL2r8E, @object
	.size	_ZN2v88internalL2r8E, 4
_ZN2v88internalL2r8E:
	.long	8
	.section	.rodata._ZN2v88internalL3rdiE,"a"
	.align 4
	.type	_ZN2v88internalL3rdiE, @object
	.size	_ZN2v88internalL3rdiE, 4
_ZN2v88internalL3rdiE:
	.long	7
	.set	_ZN2v88internalL9arg_reg_1E,_ZN2v88internalL3rdiE
	.section	.rodata._ZN2v88internalL3rbpE,"a"
	.align 4
	.type	_ZN2v88internalL3rbpE, @object
	.size	_ZN2v88internalL3rbpE, 4
_ZN2v88internalL3rbpE:
	.long	5
	.section	.rodata._ZN2v88internalL3rspE,"a"
	.align 4
	.type	_ZN2v88internalL3rspE, @object
	.size	_ZN2v88internalL3rspE, 4
_ZN2v88internalL3rspE:
	.long	4
	.section	.rodata._ZN2v88internalL3rbxE,"a"
	.align 4
	.type	_ZN2v88internalL3rbxE, @object
	.size	_ZN2v88internalL3rbxE, 4
_ZN2v88internalL3rbxE:
	.long	3
	.section	.rodata._ZN2v88internalL3rdxE,"a"
	.align 4
	.type	_ZN2v88internalL3rdxE, @object
	.size	_ZN2v88internalL3rdxE, 4
_ZN2v88internalL3rdxE:
	.long	2
	.set	_ZN2v88internalL9arg_reg_3E,_ZN2v88internalL3rdxE
	.section	.rodata._ZN2v88internalL3rcxE,"a"
	.align 4
	.type	_ZN2v88internalL3rcxE, @object
	.size	_ZN2v88internalL3rcxE, 4
_ZN2v88internalL3rcxE:
	.long	1
	.set	_ZN2v88internalL9arg_reg_4E,_ZN2v88internalL3rcxE
	.section	.rodata._ZN2v88internalL3raxE,"a"
	.align 4
	.type	_ZN2v88internalL3raxE, @object
	.size	_ZN2v88internalL3raxE, 4
_ZN2v88internalL3raxE:
	.zero	4
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
