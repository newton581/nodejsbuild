	.file	"builtins-object.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB5450:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE5450:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB5451:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5451:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB5453:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5453:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.text._ZN2v88internalL31Builtin_Impl_ObjectDefineSetterENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL31Builtin_Impl_ObjectDefineSetterENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL31Builtin_Impl_ObjectDefineSetterENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB22569:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	-16(%rsi), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movq	41088(%rdx), %r15
	movq	41096(%rdx), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L6
.L9:
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rcx, -104(%rbp)
	call	_ZN2v88internal6Object12ToObjectImplEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movq	-104(%rbp), %rcx
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L23
	movq	(%rcx), %rax
	testb	$1, %al
	jne	.L24
.L11:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$108, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L10:
	subl	$1, 41104(%r12)
	movq	%r15, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L19
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L19:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L25
	addq	$72, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L9
	movq	(%rcx), %rax
	movq	%rsi, %r13
	testb	$1, %al
	je	.L11
	.p2align 4,,10
	.p2align 3
.L24:
	movq	-1(%rax), %rax
	testb	$2, 13(%rax)
	je	.L11
	movzbl	-96(%rbp), %edx
	movq	-8(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	%rcx, -72(%rbp)
	movq	$0, -64(%rbp)
	leaq	-8(%rbx), %r9
	andl	$-64, %edx
	movups	%xmm0, -88(%rbp)
	orl	$15, %edx
	movb	%dl, -96(%rbp)
	testb	$1, %al
	jne	.L26
.L14:
	leaq	-96(%rbp), %rcx
	movl	$1, %r8d
	movq	%r9, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10JSReceiver17DefineOwnPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEEPNS0_18PropertyDescriptorENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testb	%al, %al
	je	.L23
	shrw	$8, %ax
	je	.L27
.L17:
	movq	88(%r12), %r13
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L26:
	movq	-1(%rax), %rax
	cmpw	$64, 11(%rax)
	jbe	.L14
	movq	%r9, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object20ConvertToPropertyKeyEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	jne	.L14
	.p2align 4,,10
	.p2align 3
.L23:
	movq	312(%r12), %r13
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L27:
	movl	$34, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate10CountUsageENS_7Isolate17UseCounterFeatureE@PLT
	jmp	.L17
.L25:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22569:
	.size	_ZN2v88internalL31Builtin_Impl_ObjectDefineSetterENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL31Builtin_Impl_ObjectDefineSetterENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.text._ZN2v88internalL36Builtin_Impl_ObjectPrototypeGetProtoENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL36Builtin_Impl_ObjectPrototypeGetProtoENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL36Builtin_Impl_ObjectPrototypeGetProtoENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB22581:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	41088(%rdx), %rax
	addl	$1, 41104(%rdx)
	movq	%rax, -80(%rbp)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L29
.L32:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal6Object12ToObjectImplEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L49
.L31:
	leaq	-64(%rbp), %r14
	.p2align 4,,10
	.p2align 3
.L47:
	movq	0(%r13), %r15
	movq	-1(%r15), %rax
	cmpw	$1026, 11(%rax)
	je	.L68
	movq	-1(%r15), %rax
	movzbl	13(%rax), %eax
	shrb	$5, %al
	andl	$1, %eax
.L37:
	testb	%al, %al
	je	.L38
	movq	41112(%r12), %rdi
	movq	12464(%r12), %r8
	testq	%rdi, %rdi
	je	.L39
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal7Isolate9MayAccessENS0_6HandleINS0_7ContextEEENS2_INS0_8JSObjectEEE@PLT
	testb	%al, %al
	jne	.L38
.L71:
	leaq	104(%r12), %r13
.L50:
	movq	0(%r13), %r13
.L33:
	subl	$1, 41104(%r12)
	movq	-80(%rbp), %rax
	movq	%rax, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L52
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L52:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L69
	addq	$56, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L70
.L41:
	leaq	8(%rsi), %rax
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, 41088(%r12)
	movq	%r8, (%rsi)
	call	_ZN2v88internal7Isolate9MayAccessENS0_6HandleINS0_7ContextEEENS2_INS0_8JSObjectEEE@PLT
	testb	%al, %al
	je	.L71
	.p2align 4,,10
	.p2align 3
.L38:
	movq	0(%r13), %rax
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	jne	.L56
	movq	%r13, %rdi
	call	_ZN2v88internal7JSProxy12GetPrototypeENS0_6HandleIS1_EE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L50
.L49:
	movq	312(%r12), %r13
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L68:
	movq	%r15, %rcx
	movq	%r14, %rdi
	andq	$-262144, %rcx
	movq	24(%rcx), %rax
	movq	%rcx, -72(%rbp)
	movq	-25128(%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal7Context13global_objectEv@PLT
	movq	-72(%rbp), %rcx
	movq	24(%rcx), %rcx
	testb	$1, %r15b
	jne	.L72
.L35:
	movq	-1(%r15), %rdx
	movq	23(%rdx), %rdx
.L36:
	cmpq	%rax, %rdx
	setne	%al
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L56:
	movq	-1(%rax), %rax
	movl	$1, %edx
	movq	23(%rax), %rsi
	cmpq	104(%r12), %rsi
	je	.L43
	cmpw	$1026, 11(%rax)
	setne	%dl
.L43:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L73
	movb	%dl, -72(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movzbl	-72(%rbp), %edx
	movq	%rax, %r13
.L44:
	testb	%dl, %dl
	je	.L47
	testq	%r13, %r13
	jne	.L50
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L73:
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L74
.L45:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L29:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L32
	movq	%rsi, %r13
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L70:
	movq	%r12, %rdi
	movq	%r8, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L72:
	movq	-1(%r15), %rsi
	cmpw	$1024, 11(%rsi)
	jne	.L35
	movq	-37488(%rcx), %rdx
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L74:
	movq	%r12, %rdi
	movq	%rsi, -88(%rbp)
	movb	%dl, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movzbl	-72(%rbp), %edx
	movq	%rax, %r13
	jmp	.L45
.L69:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22581:
	.size	_ZN2v88internalL36Builtin_Impl_ObjectPrototypeGetProtoENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL36Builtin_Impl_ObjectPrototypeGetProtoENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internal12_GLOBAL__N_120ObjectLookupAccessorEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_NS0_17AccessorComponentE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal12_GLOBAL__N_120ObjectLookupAccessorEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_NS0_17AccessorComponentE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_120ObjectLookupAccessorEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_NS0_17AccessorComponentE, @function
_ZN2v88internal12_GLOBAL__N_120ObjectLookupAccessorEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_NS0_17AccessorComponentE:
.LFB22563:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L76
.L79:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal6Object12ToObjectImplEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L117
.L78:
	movq	(%r15), %rax
	testb	$1, %al
	jne	.L120
.L82:
	leaq	-144(%rbp), %r15
	movl	$2, %r9d
	movq	%r14, %rcx
	movq	%r12, %rsi
	leaq	-193(%rbp), %r8
	movq	%r15, %rdi
	movb	$0, -193(%rbp)
	call	_ZN2v88internal14LookupIterator17PropertyOrElementEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_PbNS1_13ConfigurationE@PLT
	movl	-140(%rbp), %eax
	cmpl	$4, %eax
	je	.L88
	leaq	.L87(%rip), %rbx
.L84:
	cmpl	$7, %eax
	ja	.L85
	movslq	(%rbx,%rax,4), %rax
	addq	%rbx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal12_GLOBAL__N_120ObjectLookupAccessorEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_NS0_17AccessorComponentE,"a",@progbits
	.align 4
	.align 4
.L87:
	.long	.L91-.L87
	.long	.L88-.L87
	.long	.L86-.L87
	.long	.L90-.L87
	.long	.L85-.L87
	.long	.L89-.L87
	.long	.L88-.L87
	.long	.L86-.L87
	.section	.text._ZN2v88internal12_GLOBAL__N_120ObjectLookupAccessorEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_NS0_17AccessorComponentE
	.p2align 4,,10
	.p2align 3
.L89:
	movq	%r15, %rdi
	call	_ZNK2v88internal14LookupIterator12GetAccessorsEv@PLT
	movq	%rax, %rdx
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L121
.L85:
	movq	%r15, %rdi
	call	_ZN2v88internal14LookupIterator4NextEv@PLT
	movl	-140(%rbp), %eax
	cmpl	$4, %eax
	jne	.L84
	.p2align 4,,10
	.p2align 3
.L88:
	movq	88(%r12), %rax
.L80:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L122
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L91:
	.cfi_restore_state
	movq	%r15, %rdi
	call	_ZNK2v88internal14LookupIterator9HasAccessEv@PLT
	testb	%al, %al
	jne	.L85
	movq	-88(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate23ReportFailedAccessCheckENS0_6HandleINS0_8JSObjectEEE@PLT
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	je	.L88
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L90:
	movq	-112(%rbp), %rdx
	pxor	%xmm0, %xmm0
	andb	$-64, -192(%rbp)
	movups	%xmm0, -184(%rbp)
	movups	%xmm0, -168(%rbp)
	testq	%rdx, %rdx
	je	.L123
.L93:
	movq	-88(%rbp), %rsi
	leaq	-192(%rbp), %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal7JSProxy24GetOwnPropertyDescriptorEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEEPNS0_18PropertyDescriptorE@PLT
	testb	%al, %al
	je	.L117
	shrw	$8, %ax
	je	.L101
	testl	%r13d, %r13d
	jne	.L102
	movq	-176(%rbp), %rax
	testq	%rax, %rax
	je	.L88
.L119:
	movq	(%rax), %rax
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L120:
	movq	-1(%rax), %rax
	cmpw	$64, 11(%rax)
	jbe	.L82
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rdx, -216(%rbp)
	call	_ZN2v88internal6Object20ConvertToPropertyKeyEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	-216(%rbp), %rdx
	testq	%rax, %rax
	movq	%rax, %r14
	jne	.L82
.L117:
	movq	312(%r12), %rax
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L76:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L79
	movq	%rsi, %rdx
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L121:
	movq	-1(%rax), %rax
	cmpw	$79, 11(%rax)
	jne	.L85
	movq	-88(%rbp), %rax
	leaq	-192(%rbp), %rdi
	movq	%rdx, -216(%rbp)
	movq	(%rax), %rax
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal10JSReceiver18GetCreationContextEv@PLT
	movq	-216(%rbp), %rdx
	movl	%r13d, %ecx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal12AccessorPair12GetComponentEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEENS4_IS1_EENS0_17AccessorComponentE@PLT
	jmp	.L119
.L86:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L102:
	movq	-168(%rbp), %rax
	testq	%rax, %rax
	jne	.L119
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L123:
	movl	-72(%rbp), %r15d
	movq	-120(%rbp), %rdi
	testl	%r15d, %r15d
	js	.L94
	movq	%r15, %rsi
	movl	$1, %edx
	salq	$32, %rsi
	call	_ZN2v88internal7Factory14NumberToStringENS0_3SmiEb@PLT
	movq	%rax, %rdx
.L95:
	movq	(%rdx), %rax
	movl	11(%rax), %esi
	cmpl	$10, %esi
	jle	.L124
.L98:
	movq	%rdx, -112(%rbp)
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L101:
	movq	-88(%rbp), %rdi
	call	_ZN2v88internal7JSProxy12GetPrototypeENS0_6HandleIS1_EE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L117
	movq	(%rax), %rax
	cmpq	%rax, 104(%r12)
	je	.L88
	movl	%r13d, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_120ObjectLookupAccessorEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_NS0_17AccessorComponentE
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L94:
	xorl	%edx, %edx
	movl	%r15d, %esi
	movq	%rdi, -216(%rbp)
	call	_ZN2v88internal7Factory17NewNumberFromUintEjNS0_14AllocationTypeE@PLT
	movq	-216(%rbp), %rdi
	movl	$1, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal7Factory14NumberToStringENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%rax, %rdx
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L124:
	cmpl	$3, 7(%rax)
	jne	.L98
	movl	%r15d, %edi
	movq	%rdx, -216(%rbp)
	call	_ZN2v88internal12StringHasher18MakeArrayIndexHashEji@PLT
	movq	-216(%rbp), %rdx
	movq	(%rdx), %rcx
	movl	%eax, 7(%rcx)
	jmp	.L98
.L122:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22563:
	.size	_ZN2v88internal12_GLOBAL__N_120ObjectLookupAccessorEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_NS0_17AccessorComponentE, .-_ZN2v88internal12_GLOBAL__N_120ObjectLookupAccessorEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_NS0_17AccessorComponentE
	.section	.text._ZN2v88internalL48Builtin_Impl_ObjectPrototypePropertyIsEnumerableENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL48Builtin_Impl_ObjectPrototypePropertyIsEnumerableENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL48Builtin_Impl_ObjectPrototypePropertyIsEnumerableENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB22555:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-8(%rsi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 3, -56
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	41088(%rdx), %rax
	addl	$1, 41104(%rdx)
	cmpl	$5, %edi
	movq	%rax, -248(%rbp)
	leaq	88(%rdx), %rax
	cmovle	%rax, %r13
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L128
.L131:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object13ConvertToNameEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L163
	movq	(%r15), %rax
	testb	$1, %al
	jne	.L133
.L136:
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object12ToObjectImplEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L163
.L135:
	movq	(%r14), %rax
	andq	$-262144, %rax
	movq	24(%rax), %r15
	movq	0(%r13), %rax
	movq	-1(%rax), %rcx
	subq	$37592, %r15
	cmpw	$63, 11(%rcx)
	jbe	.L164
.L138:
	movq	0(%r13), %rcx
	movl	$1, %eax
	movq	-1(%rcx), %rsi
	cmpw	$64, 11(%rsi)
	jne	.L142
	movl	11(%rcx), %eax
	notl	%eax
	andl	$1, %eax
.L142:
	movl	%eax, -224(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -212(%rbp)
	movq	%r15, -200(%rbp)
	movq	0(%r13), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L165
.L143:
	movq	%r13, -192(%rbp)
	leaq	-224(%rbp), %r13
	movq	%r13, %rdi
	movq	$0, -184(%rbp)
	movq	%r14, -176(%rbp)
	movq	$0, -168(%rbp)
	movq	%r14, -160(%rbp)
	movq	$-1, -152(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
.L141:
	movq	%r13, %rdi
	call	_ZN2v88internal10JSReceiver21GetPropertyAttributesEPNS0_14LookupIteratorE@PLT
	testb	%al, %al
	je	.L163
	shrq	$32, %rax
	cmpl	$64, %eax
	je	.L146
	testb	$2, %al
	jne	.L146
	movq	112(%r12), %r13
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L163:
	movq	312(%r12), %r13
.L132:
	subl	$1, 41104(%r12)
	movq	-248(%rbp), %rax
	movq	%rax, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L149
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L149:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L166
	addq	$216, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L164:
	.cfi_restore_state
	movq	%rax, -144(%rbp)
	movl	7(%rax), %eax
	testb	$1, %al
	jne	.L139
	testb	$2, %al
	jne	.L138
.L139:
	leaq	-144(%rbp), %rdi
	leaq	-228(%rbp), %rsi
	movq	%rdi, -256(%rbp)
	call	_ZN2v88internal6String16SlowAsArrayIndexEPj@PLT
	testb	%al, %al
	je	.L138
	movabsq	$824633720832, %rax
	movq	-256(%rbp), %rdi
	movq	%r15, -120(%rbp)
	movq	%rax, -132(%rbp)
	movl	-228(%rbp), %eax
	movl	$1, -144(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%r14, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%r14, -80(%rbp)
	movl	%eax, -72(%rbp)
	movl	$-1, -68(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	movq	%r13, -112(%rbp)
	movdqa	-128(%rbp), %xmm1
	leaq	-224(%rbp), %r13
	movdqa	-144(%rbp), %xmm0
	movdqa	-112(%rbp), %xmm2
	movdqa	-96(%rbp), %xmm3
	movdqa	-80(%rbp), %xmm4
	movaps	%xmm1, -208(%rbp)
	movaps	%xmm0, -224(%rbp)
	movaps	%xmm2, -192(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm4, -160(%rbp)
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L146:
	movq	120(%r12), %r13
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L128:
	movq	-1(%rax), %rax
	cmpw	$64, 11(%rax)
	ja	.L131
	movq	(%r15), %rax
	testb	$1, %al
	je	.L136
	.p2align 4,,10
	.p2align 3
.L133:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L136
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L165:
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %r13
	jmp	.L143
.L166:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22555:
	.size	_ZN2v88internalL48Builtin_Impl_ObjectPrototypePropertyIsEnumerableENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL48Builtin_Impl_ObjectPrototypePropertyIsEnumerableENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.text._ZN2v88internalL31Builtin_Impl_ObjectDefineGetterENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL31Builtin_Impl_ObjectDefineGetterENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL31Builtin_Impl_ObjectDefineGetterENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB22566:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	-16(%rsi), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movq	41088(%rdx), %r15
	movq	41096(%rdx), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L168
.L171:
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rcx, -104(%rbp)
	call	_ZN2v88internal6Object12ToObjectImplEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movq	-104(%rbp), %rcx
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L185
	movq	(%rcx), %rax
	testb	$1, %al
	jne	.L186
.L173:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$105, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L172:
	subl	$1, 41104(%r12)
	movq	%r15, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L181
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L181:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L187
	addq	$72, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L168:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L171
	movq	(%rcx), %rax
	movq	%rsi, %r13
	testb	$1, %al
	je	.L173
	.p2align 4,,10
	.p2align 3
.L186:
	movq	-1(%rax), %rax
	testb	$2, 13(%rax)
	je	.L173
	movzbl	-96(%rbp), %edx
	movq	-8(%rbx), %rax
	movq	%rcx, -80(%rbp)
	leaq	-8(%rbx), %r9
	movq	$0, -88(%rbp)
	andl	$-64, %edx
	movq	$0, -72(%rbp)
	orl	$15, %edx
	movq	$0, -64(%rbp)
	movb	%dl, -96(%rbp)
	testb	$1, %al
	jne	.L188
.L176:
	leaq	-96(%rbp), %rcx
	movl	$1, %r8d
	movq	%r9, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10JSReceiver17DefineOwnPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEEPNS0_18PropertyDescriptorENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testb	%al, %al
	je	.L185
	shrw	$8, %ax
	je	.L189
.L179:
	movq	88(%r12), %r13
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L188:
	movq	-1(%rax), %rax
	cmpw	$64, 11(%rax)
	jbe	.L176
	movq	%r9, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object20ConvertToPropertyKeyEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	jne	.L176
	.p2align 4,,10
	.p2align 3
.L185:
	movq	312(%r12), %r13
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L189:
	movl	$34, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate10CountUsageENS_7Isolate17UseCounterFeatureE@PLT
	jmp	.L179
.L187:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22566:
	.size	_ZN2v88internalL31Builtin_Impl_ObjectDefineGetterENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL31Builtin_Impl_ObjectDefineGetterENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL36Builtin_Impl_ObjectPrototypeSetProtoENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"set Object.prototype.__proto__"
	.section	.rodata._ZN2v88internalL36Builtin_Impl_ObjectPrototypeSetProtoENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"(location_) != nullptr"
.LC3:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internalL36Builtin_Impl_ObjectPrototypeSetProtoENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL36Builtin_Impl_ObjectPrototypeSetProtoENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL36Builtin_Impl_ObjectPrototypeSetProtoENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB22584:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -48
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	41104(%rdx), %edx
	leal	1(%rdx), %eax
	movl	%eax, 41104(%r12)
	movq	(%rsi), %rax
	cmpq	%rax, 104(%r12)
	jne	.L210
.L191:
	leaq	.LC1(%rip), %rax
	xorl	%edx, %edx
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	movq	$30, -56(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L211
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movl	$27, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	41096(%r12), %rcx
	movq	%rax, %r14
	movl	41104(%r12), %eax
	leal	-1(%rax), %edx
.L195:
	movq	%r13, 41088(%r12)
	movl	%edx, 41104(%r12)
	cmpq	%rcx, %rbx
	je	.L204
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L204:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L212
	addq	$32, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L210:
	.cfi_restore_state
	movq	88(%r12), %r14
	cmpq	%rax, %r14
	je	.L191
	movq	%rsi, %rdi
	leaq	-8(%rsi), %r8
	movq	-8(%rsi), %rsi
	cmpq	%rsi, 104(%r12)
	je	.L193
	movq	%rbx, %rcx
	testb	$1, %sil
	je	.L195
	movq	-1(%rsi), %rcx
	cmpw	$1023, 11(%rcx)
	ja	.L193
	movq	%rbx, %rcx
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L211:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L193:
	testb	$1, %al
	jne	.L213
.L209:
	movl	41104(%r12), %eax
	movq	41096(%r12), %rcx
	leal	-1(%rax), %edx
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L213:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L209
	xorl	%ecx, %ecx
	movl	$1, %edx
	movq	%r8, %rsi
	call	_ZN2v88internal10JSReceiver12SetPrototypeENS0_6HandleIS1_EENS2_INS0_6ObjectEEEbNS0_11ShouldThrowE@PLT
	testb	%al, %al
	jne	.L202
	movq	312(%r12), %r14
	jmp	.L209
.L202:
	movl	41104(%r12), %eax
	movq	88(%r12), %r14
	movq	41096(%r12), %rcx
	leal	-1(%rax), %edx
	jmp	.L195
.L212:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22584:
	.size	_ZN2v88internalL36Builtin_Impl_ObjectPrototypeSetProtoENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL36Builtin_Impl_ObjectPrototypeSetProtoENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL44Builtin_Impl_ObjectGetOwnPropertyDescriptorsENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"success.FromJust()"
	.section	.text._ZN2v88internalL44Builtin_Impl_ObjectGetOwnPropertyDescriptorsENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL44Builtin_Impl_ObjectGetOwnPropertyDescriptorsENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL44Builtin_Impl_ObjectGetOwnPropertyDescriptorsENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB22597:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	subq	$8, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	88(%rdx), %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	41088(%rdx), %rax
	addl	$1, 41104(%rdx)
	cmpl	$5, %edi
	cmovg	%rsi, %r12
	movq	%rax, -136(%rbp)
	movq	41096(%rdx), %rax
	movq	%rax, -128(%rbp)
	movq	(%r12), %rax
	testb	$1, %al
	jne	.L217
.L220:
	movq	%r12, %rsi
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal6Object12ToObjectImplEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L241
.L219:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14KeyAccumulator7GetKeysENS0_6HandleINS0_10JSReceiverEEENS0_17KeyCollectionModeENS0_14PropertyFilterENS0_17GetKeysConversionEbb@PLT
	movq	%rax, -104(%rbp)
	testq	%rax, %rax
	je	.L241
	movq	12464(%r14), %rax
	movq	39(%rax), %rax
	movq	879(%rax), %r15
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L223
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L224:
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movq	%rax, -120(%rbp)
	movq	-104(%rbp), %rax
	movq	(%rax), %rax
	movl	11(%rax), %edx
	testl	%edx, %edx
	jle	.L226
	movq	%r12, -112(%rbp)
	xorl	%ebx, %ebx
	leaq	-96(%rbp), %r15
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L244:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r12
.L228:
	movq	-112(%rbp), %rsi
	pxor	%xmm0, %xmm0
	movq	%r15, %rcx
	movq	%r12, %rdx
	movq	%r14, %rdi
	andb	$-64, -96(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	call	_ZN2v88internal10JSReceiver24GetOwnPropertyDescriptorEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEEPNS0_18PropertyDescriptorE@PLT
	testb	%al, %al
	je	.L241
	shrw	$8, %ax
	je	.L234
	movq	%r14, %rsi
	movq	%r15, %rdi
	movb	$1, %r13b
	call	_ZN2v88internal18PropertyDescriptor8ToObjectEPNS0_7IsolateE@PLT
	movl	%r13d, %r13d
	movq	-120(%rbp), %rsi
	movq	%r12, %rdx
	movq	%rax, %rcx
	movq	%r14, %rdi
	movabsq	$4294967296, %rax
	orq	%rax, %r13
	movq	%r13, %r8
	call	_ZN2v88internal10JSReceiver18CreateDataPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testb	%al, %al
	je	.L242
.L233:
	shrw	$8, %ax
	je	.L243
.L234:
	movq	-104(%rbp), %rax
	addq	$1, %rbx
	movq	(%rax), %rax
	cmpl	%ebx, 11(%rax)
	jle	.L226
.L232:
	movq	15(%rax,%rbx,8), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	jne	.L244
	movq	41088(%r14), %r12
	cmpq	41096(%r14), %r12
	je	.L245
.L229:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%r12)
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L241:
	movq	312(%r14), %r12
.L221:
	movq	-136(%rbp), %rax
	subl	$1, 41104(%r14)
	movq	%rax, 41088(%r14)
	movq	-128(%rbp), %rax
	cmpq	41096(%r14), %rax
	je	.L236
	movq	%rax, 41096(%r14)
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L236:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L246
	addq	$104, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L223:
	.cfi_restore_state
	movq	41088(%r14), %rsi
	cmpq	41096(%r14), %rsi
	je	.L247
.L225:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r14)
	movq	%r15, (%rsi)
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L245:
	movq	%r14, %rdi
	movq	%rsi, -144(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-144(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L217:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L220
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L243:
	leaq	.LC4(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L242:
	movl	%eax, -144(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-144(%rbp), %eax
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L226:
	movq	-120(%rbp), %rax
	movq	(%rax), %r12
	jmp	.L221
.L247:
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L225
.L246:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22597:
	.size	_ZN2v88internalL44Builtin_Impl_ObjectGetOwnPropertyDescriptorsENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL44Builtin_Impl_ObjectGetOwnPropertyDescriptorsENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB8771:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L254
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L257
.L248:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L254:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L257:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L248
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE8771:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.rodata._ZN2v88internalL39Builtin_Impl_Stats_ObjectDefinePropertyEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"disabled-by-default-v8.runtime"
	.align 8
.LC6:
	.string	"V8.Builtin_ObjectDefineProperty"
	.section	.text._ZN2v88internalL39Builtin_Impl_Stats_ObjectDefinePropertyEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL39Builtin_Impl_Stats_ObjectDefinePropertyEiPmPNS0_7IsolateE, @function
_ZN2v88internalL39Builtin_Impl_Stats_ObjectDefinePropertyEiPmPNS0_7IsolateE:
.LFB22559:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L288
.L259:
	movq	_ZZN2v88internalL39Builtin_Impl_Stats_ObjectDefinePropertyEiPmPNS0_7IsolateEE27trace_event_unique_atomic50(%rip), %r13
	testq	%r13, %r13
	je	.L289
.L261:
	movq	$0, -160(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L290
.L263:
	addl	$1, 41104(%r12)
	leaq	-8(%rbx), %rsi
	leaq	-16(%rbx), %rdx
	movq	%r12, %rdi
	movq	41088(%r12), %r14
	leaq	-24(%rbx), %rcx
	movq	41096(%r12), %r13
	call	_ZN2v88internal10JSReceiver14DefinePropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_@PLT
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r15
	cmpq	41096(%r12), %r13
	je	.L267
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L267:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L291
.L258:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L292
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L289:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L293
.L262:
	movq	%r13, _ZZN2v88internalL39Builtin_Impl_Stats_ObjectDefinePropertyEiPmPNS0_7IsolateEE27trace_event_unique_atomic50(%rip)
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L290:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L294
.L264:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L265
	movq	(%rdi), %rax
	call	*8(%rax)
.L265:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L266
	movq	(%rdi), %rax
	call	*8(%rax)
.L266:
	leaq	.LC6(%rip), %rax
	movq	%r13, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L291:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L288:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$795, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L294:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC6(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L293:
	leaq	.LC5(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L262
.L292:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22559:
	.size	_ZN2v88internalL39Builtin_Impl_Stats_ObjectDefinePropertyEiPmPNS0_7IsolateE, .-_ZN2v88internalL39Builtin_Impl_Stats_ObjectDefinePropertyEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL41Builtin_Impl_Stats_ObjectDefinePropertiesEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC7:
	.string	"V8.Builtin_ObjectDefineProperties"
	.section	.text._ZN2v88internalL41Builtin_Impl_Stats_ObjectDefinePropertiesEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL41Builtin_Impl_Stats_ObjectDefinePropertiesEiPmPNS0_7IsolateE, @function
_ZN2v88internalL41Builtin_Impl_Stats_ObjectDefinePropertiesEiPmPNS0_7IsolateE:
.LFB22556:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L328
.L296:
	movq	_ZZN2v88internalL41Builtin_Impl_Stats_ObjectDefinePropertiesEiPmPNS0_7IsolateEE27trace_event_unique_atomic39(%rip), %r13
	testq	%r13, %r13
	je	.L329
.L298:
	movq	$0, -160(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L330
.L300:
	addl	$1, 41104(%r12)
	leaq	-8(%rbx), %rsi
	leaq	-16(%rbx), %rdx
	movq	%r12, %rdi
	movq	41088(%r12), %r13
	movq	41096(%r12), %r14
	call	_ZN2v88internal10JSReceiver16DefinePropertiesEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_@PLT
	testq	%rax, %rax
	je	.L331
	movq	(%rax), %r15
.L305:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L308
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L308:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L332
.L295:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L333
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L330:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L334
.L301:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L302
	movq	(%rdi), %rax
	call	*8(%rax)
.L302:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L303
	movq	(%rdi), %rax
	call	*8(%rax)
.L303:
	leaq	.LC7(%rip), %rax
	movq	%r13, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L329:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L335
.L299:
	movq	%r13, _ZZN2v88internalL41Builtin_Impl_Stats_ObjectDefinePropertiesEiPmPNS0_7IsolateEE27trace_event_unique_atomic39(%rip)
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L331:
	movq	312(%r12), %r15
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L332:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L328:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$794, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L335:
	leaq	.LC5(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L334:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC7(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L301
.L333:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22556:
	.size	_ZN2v88internalL41Builtin_Impl_Stats_ObjectDefinePropertiesEiPmPNS0_7IsolateE, .-_ZN2v88internalL41Builtin_Impl_Stats_ObjectDefinePropertiesEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL31Builtin_Impl_Stats_ObjectFreezeEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC8:
	.string	"V8.Builtin_ObjectFreeze"
	.section	.text._ZN2v88internalL31Builtin_Impl_Stats_ObjectFreezeEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL31Builtin_Impl_Stats_ObjectFreezeEiPmPNS0_7IsolateE, @function
_ZN2v88internalL31Builtin_Impl_Stats_ObjectFreezeEiPmPNS0_7IsolateE:
.LFB22576:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edi, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L374
.L337:
	movq	_ZZN2v88internalL31Builtin_Impl_Stats_ObjectFreezeEiPmPNS0_7IsolateEE28trace_event_unique_atomic213(%rip), %r13
	testq	%r13, %r13
	je	.L375
.L339:
	movq	$0, -160(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L376
.L341:
	movl	41104(%r12), %eax
	subq	$8, %rbx
	leaq	88(%r12), %rsi
	cmpl	$5, %r14d
	cmovle	%rsi, %rbx
	movq	41088(%r12), %rcx
	movq	41096(%r12), %r15
	leal	1(%rax), %edx
	movl	%edx, 41104(%r12)
	movq	(%rbx), %r13
	testb	$1, %r13b
	jne	.L347
	movl	%eax, 41104(%r12)
.L348:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L377
.L336:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L378
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L375:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L379
.L340:
	movq	%r13, _ZZN2v88internalL31Builtin_Impl_Stats_ObjectFreezeEiPmPNS0_7IsolateEE28trace_event_unique_atomic213(%rip)
	jmp	.L339
	.p2align 4,,10
	.p2align 3
.L347:
	movq	-1(%r13), %rdx
	cmpw	$1023, 11(%rdx)
	ja	.L349
	movq	%r15, %rdx
.L352:
	movq	%rcx, 41088(%r12)
	movl	%eax, 41104(%r12)
	cmpq	%rdx, %r15
	je	.L348
	movq	%r15, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L376:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L380
.L342:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L343
	movq	(%rdi), %rax
	call	*8(%rax)
.L343:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L344
	movq	(%rdi), %rax
	call	*8(%rax)
.L344:
	leaq	.LC8(%rip), %rax
	movq	%r13, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L349:
	xorl	%edx, %edx
	movl	$5, %esi
	movq	%rbx, %rdi
	movq	%rcx, -168(%rbp)
	call	_ZN2v88internal10JSReceiver17SetIntegrityLevelENS0_6HandleIS1_EENS0_18PropertyAttributesENS0_11ShouldThrowE@PLT
	movq	-168(%rbp), %rcx
	testb	%al, %al
	jne	.L351
	movl	41104(%r12), %eax
	movq	312(%r12), %r13
	movq	41096(%r12), %rdx
	subl	$1, %eax
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L377:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L374:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$797, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L337
	.p2align 4,,10
	.p2align 3
.L351:
	movl	41104(%r12), %eax
	movq	(%rbx), %r13
	movq	41096(%r12), %rdx
	subl	$1, %eax
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L379:
	leaq	.LC5(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L380:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC8(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L342
.L378:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22576:
	.size	_ZN2v88internalL31Builtin_Impl_Stats_ObjectFreezeEiPmPNS0_7IsolateE, .-_ZN2v88internalL31Builtin_Impl_Stats_ObjectFreezeEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL42Builtin_Impl_Stats_ObjectPrototypeSetProtoEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC9:
	.string	"V8.Builtin_ObjectPrototypeSetProto"
	.section	.text._ZN2v88internalL42Builtin_Impl_Stats_ObjectPrototypeSetProtoEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL42Builtin_Impl_Stats_ObjectPrototypeSetProtoEiPmPNS0_7IsolateE, @function
_ZN2v88internalL42Builtin_Impl_Stats_ObjectPrototypeSetProtoEiPmPNS0_7IsolateE:
.LFB22582:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L410
.L382:
	movq	_ZZN2v88internalL42Builtin_Impl_Stats_ObjectPrototypeSetProtoEiPmPNS0_7IsolateEE28trace_event_unique_atomic238(%rip), %rbx
	testq	%rbx, %rbx
	je	.L411
.L384:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L412
.L386:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL36Builtin_Impl_ObjectPrototypeSetProtoENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L413
.L390:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L414
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L411:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L415
.L385:
	movq	%rbx, _ZZN2v88internalL42Builtin_Impl_Stats_ObjectPrototypeSetProtoEiPmPNS0_7IsolateEE28trace_event_unique_atomic238(%rip)
	jmp	.L384
	.p2align 4,,10
	.p2align 3
.L412:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L416
.L387:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L388
	movq	(%rdi), %rax
	call	*8(%rax)
.L388:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L389
	movq	(%rdi), %rax
	call	*8(%rax)
.L389:
	leaq	.LC9(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L386
	.p2align 4,,10
	.p2align 3
.L413:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L390
	.p2align 4,,10
	.p2align 3
.L410:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$806, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L382
	.p2align 4,,10
	.p2align 3
.L416:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC9(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L387
	.p2align 4,,10
	.p2align 3
.L415:
	leaq	.LC5(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L385
.L414:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22582:
	.size	_ZN2v88internalL42Builtin_Impl_Stats_ObjectPrototypeSetProtoEiPmPNS0_7IsolateE, .-_ZN2v88internalL42Builtin_Impl_Stats_ObjectPrototypeSetProtoEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL29Builtin_Impl_Stats_ObjectSealEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC10:
	.string	"V8.Builtin_ObjectSeal"
	.section	.text._ZN2v88internalL29Builtin_Impl_Stats_ObjectSealEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL29Builtin_Impl_Stats_ObjectSealEiPmPNS0_7IsolateE, @function
_ZN2v88internalL29Builtin_Impl_Stats_ObjectSealEiPmPNS0_7IsolateE:
.LFB22598:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edi, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L455
.L418:
	movq	_ZZN2v88internalL29Builtin_Impl_Stats_ObjectSealEiPmPNS0_7IsolateEE28trace_event_unique_atomic352(%rip), %r13
	testq	%r13, %r13
	je	.L456
.L420:
	movq	$0, -160(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L457
.L422:
	movl	41104(%r12), %eax
	subq	$8, %rbx
	leaq	88(%r12), %rsi
	cmpl	$5, %r14d
	cmovle	%rsi, %rbx
	movq	41088(%r12), %rcx
	movq	41096(%r12), %r15
	leal	1(%rax), %edx
	movl	%edx, 41104(%r12)
	movq	(%rbx), %r13
	testb	$1, %r13b
	jne	.L428
	movl	%eax, 41104(%r12)
.L429:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L458
.L417:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L459
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L456:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L460
.L421:
	movq	%r13, _ZZN2v88internalL29Builtin_Impl_Stats_ObjectSealEiPmPNS0_7IsolateEE28trace_event_unique_atomic352(%rip)
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L428:
	movq	-1(%r13), %rdx
	cmpw	$1023, 11(%rdx)
	ja	.L430
	movq	%r15, %rdx
.L433:
	movq	%rcx, 41088(%r12)
	movl	%eax, 41104(%r12)
	cmpq	%rdx, %r15
	je	.L429
	movq	%r15, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	jmp	.L429
	.p2align 4,,10
	.p2align 3
.L457:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L461
.L423:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L424
	movq	(%rdi), %rax
	call	*8(%rax)
.L424:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L425
	movq	(%rdi), %rax
	call	*8(%rax)
.L425:
	leaq	.LC10(%rip), %rax
	movq	%r13, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L422
	.p2align 4,,10
	.p2align 3
.L430:
	xorl	%edx, %edx
	movl	$4, %esi
	movq	%rbx, %rdi
	movq	%rcx, -168(%rbp)
	call	_ZN2v88internal10JSReceiver17SetIntegrityLevelENS0_6HandleIS1_EENS0_18PropertyAttributesENS0_11ShouldThrowE@PLT
	movq	-168(%rbp), %rcx
	testb	%al, %al
	jne	.L432
	movl	41104(%r12), %eax
	movq	312(%r12), %r13
	movq	41096(%r12), %rdx
	subl	$1, %eax
	jmp	.L433
	.p2align 4,,10
	.p2align 3
.L458:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L417
	.p2align 4,,10
	.p2align 3
.L455:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$807, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L418
	.p2align 4,,10
	.p2align 3
.L432:
	movl	41104(%r12), %eax
	movq	(%rbx), %r13
	movq	41096(%r12), %rdx
	subl	$1, %eax
	jmp	.L433
	.p2align 4,,10
	.p2align 3
.L460:
	leaq	.LC5(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L461:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC10(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L423
.L459:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22598:
	.size	_ZN2v88internalL29Builtin_Impl_Stats_ObjectSealEiPmPNS0_7IsolateE, .-_ZN2v88internalL29Builtin_Impl_Stats_ObjectSealEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL37Builtin_Impl_Stats_ObjectDefineGetterEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC11:
	.string	"V8.Builtin_ObjectDefineGetter"
	.section	.text._ZN2v88internalL37Builtin_Impl_Stats_ObjectDefineGetterEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL37Builtin_Impl_Stats_ObjectDefineGetterEiPmPNS0_7IsolateE, @function
_ZN2v88internalL37Builtin_Impl_Stats_ObjectDefineGetterEiPmPNS0_7IsolateE:
.LFB22564:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L491
.L463:
	movq	_ZZN2v88internalL37Builtin_Impl_Stats_ObjectDefineGetterEiPmPNS0_7IsolateEE28trace_event_unique_atomic176(%rip), %rbx
	testq	%rbx, %rbx
	je	.L492
.L465:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L493
.L467:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL31Builtin_Impl_ObjectDefineGetterENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L494
.L471:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L495
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L492:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L496
.L466:
	movq	%rbx, _ZZN2v88internalL37Builtin_Impl_Stats_ObjectDefineGetterEiPmPNS0_7IsolateEE28trace_event_unique_atomic176(%rip)
	jmp	.L465
	.p2align 4,,10
	.p2align 3
.L493:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L497
.L468:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L469
	movq	(%rdi), %rax
	call	*8(%rax)
.L469:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L470
	movq	(%rdi), %rax
	call	*8(%rax)
.L470:
	leaq	.LC11(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L467
	.p2align 4,,10
	.p2align 3
.L494:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L471
	.p2align 4,,10
	.p2align 3
.L491:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$793, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L463
	.p2align 4,,10
	.p2align 3
.L497:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC11(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L496:
	leaq	.LC5(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L466
.L495:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22564:
	.size	_ZN2v88internalL37Builtin_Impl_Stats_ObjectDefineGetterEiPmPNS0_7IsolateE, .-_ZN2v88internalL37Builtin_Impl_Stats_ObjectDefineGetterEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL37Builtin_Impl_Stats_ObjectDefineSetterEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC12:
	.string	"V8.Builtin_ObjectDefineSetter"
	.section	.text._ZN2v88internalL37Builtin_Impl_Stats_ObjectDefineSetterEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL37Builtin_Impl_Stats_ObjectDefineSetterEiPmPNS0_7IsolateE, @function
_ZN2v88internalL37Builtin_Impl_Stats_ObjectDefineSetterEiPmPNS0_7IsolateE:
.LFB22567:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L527
.L499:
	movq	_ZZN2v88internalL37Builtin_Impl_Stats_ObjectDefineSetterEiPmPNS0_7IsolateEE28trace_event_unique_atomic186(%rip), %rbx
	testq	%rbx, %rbx
	je	.L528
.L501:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L529
.L503:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL31Builtin_Impl_ObjectDefineSetterENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L530
.L507:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L531
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L528:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L532
.L502:
	movq	%rbx, _ZZN2v88internalL37Builtin_Impl_Stats_ObjectDefineSetterEiPmPNS0_7IsolateEE28trace_event_unique_atomic186(%rip)
	jmp	.L501
	.p2align 4,,10
	.p2align 3
.L529:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L533
.L504:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L505
	movq	(%rdi), %rax
	call	*8(%rax)
.L505:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L506
	movq	(%rdi), %rax
	call	*8(%rax)
.L506:
	leaq	.LC12(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L503
	.p2align 4,,10
	.p2align 3
.L530:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L507
	.p2align 4,,10
	.p2align 3
.L527:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$796, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L499
	.p2align 4,,10
	.p2align 3
.L533:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC12(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L504
	.p2align 4,,10
	.p2align 3
.L532:
	leaq	.LC5(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L502
.L531:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22567:
	.size	_ZN2v88internalL37Builtin_Impl_Stats_ObjectDefineSetterEiPmPNS0_7IsolateE, .-_ZN2v88internalL37Builtin_Impl_Stats_ObjectDefineSetterEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL33Builtin_Impl_Stats_ObjectIsFrozenEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC13:
	.string	"V8.Builtin_ObjectIsFrozen"
	.section	.text._ZN2v88internalL33Builtin_Impl_Stats_ObjectIsFrozenEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL33Builtin_Impl_Stats_ObjectIsFrozenEiPmPNS0_7IsolateE, @function
_ZN2v88internalL33Builtin_Impl_Stats_ObjectIsFrozenEiPmPNS0_7IsolateE:
.LFB22589:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edi, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L575
.L535:
	movq	_ZZN2v88internalL33Builtin_Impl_Stats_ObjectIsFrozenEiPmPNS0_7IsolateEE28trace_event_unique_atomic293(%rip), %r13
	testq	%r13, %r13
	je	.L576
.L537:
	movq	$0, -160(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L577
.L539:
	movl	41104(%r12), %eax
	leaq	88(%r12), %rsi
	leaq	-8(%rbx), %rdi
	cmpl	$5, %r14d
	cmovle	%rsi, %rdi
	movq	41096(%r12), %r13
	movq	41088(%r12), %r15
	leal	1(%rax), %edx
	movl	%edx, 41104(%r12)
	movq	(%rdi), %rcx
	movq	%r13, %rdx
	testb	$1, %cl
	jne	.L578
.L554:
	movq	112(%r12), %r14
.L548:
	movq	%r15, 41088(%r12)
	movl	%eax, 41104(%r12)
	cmpq	%rdx, %r13
	je	.L553
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L553:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L579
.L534:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L580
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L578:
	.cfi_restore_state
	movq	-1(%rcx), %rdx
	cmpw	$1023, 11(%rdx)
	jbe	.L581
	movl	$5, %esi
	call	_ZN2v88internal10JSReceiver18TestIntegrityLevelENS0_6HandleIS1_EENS0_18PropertyAttributesE@PLT
	movzbl	%ah, %ecx
	testb	%al, %al
	jne	.L547
	movl	41104(%r12), %eax
	movq	312(%r12), %r14
	movq	41096(%r12), %rdx
	subl	$1, %eax
	jmp	.L548
	.p2align 4,,10
	.p2align 3
.L577:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L582
.L540:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L541
	movq	(%rdi), %rax
	call	*8(%rax)
.L541:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L542
	movq	(%rdi), %rax
	call	*8(%rax)
.L542:
	leaq	.LC13(%rip), %rax
	movq	%r13, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L539
	.p2align 4,,10
	.p2align 3
.L576:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L583
.L538:
	movq	%r13, _ZZN2v88internalL33Builtin_Impl_Stats_ObjectIsFrozenEiPmPNS0_7IsolateEE28trace_event_unique_atomic293(%rip)
	jmp	.L537
.L547:
	movl	41104(%r12), %eax
	movq	41096(%r12), %rdx
	subl	$1, %eax
	testb	%cl, %cl
	jne	.L554
	movq	120(%r12), %r14
	jmp	.L548
	.p2align 4,,10
	.p2align 3
.L581:
	movq	%r13, %rdx
	jmp	.L554
	.p2align 4,,10
	.p2align 3
.L579:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L534
	.p2align 4,,10
	.p2align 3
.L575:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$800, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L535
	.p2align 4,,10
	.p2align 3
.L583:
	leaq	.LC5(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L538
	.p2align 4,,10
	.p2align 3
.L582:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC13(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L540
.L580:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22589:
	.size	_ZN2v88internalL33Builtin_Impl_Stats_ObjectIsFrozenEiPmPNS0_7IsolateE, .-_ZN2v88internalL33Builtin_Impl_Stats_ObjectIsFrozenEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL33Builtin_Impl_Stats_ObjectIsSealedEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC14:
	.string	"V8.Builtin_ObjectIsSealed"
	.section	.text._ZN2v88internalL33Builtin_Impl_Stats_ObjectIsSealedEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL33Builtin_Impl_Stats_ObjectIsSealedEiPmPNS0_7IsolateE, @function
_ZN2v88internalL33Builtin_Impl_Stats_ObjectIsSealedEiPmPNS0_7IsolateE:
.LFB22592:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edi, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L625
.L585:
	movq	_ZZN2v88internalL33Builtin_Impl_Stats_ObjectIsSealedEiPmPNS0_7IsolateEE28trace_event_unique_atomic305(%rip), %r13
	testq	%r13, %r13
	je	.L626
.L587:
	movq	$0, -160(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L627
.L589:
	movl	41104(%r12), %eax
	leaq	88(%r12), %rsi
	leaq	-8(%rbx), %rdi
	cmpl	$5, %r14d
	cmovle	%rsi, %rdi
	movq	41096(%r12), %r13
	movq	41088(%r12), %r15
	leal	1(%rax), %edx
	movl	%edx, 41104(%r12)
	movq	(%rdi), %rcx
	movq	%r13, %rdx
	testb	$1, %cl
	jne	.L628
.L604:
	movq	112(%r12), %r14
.L598:
	movq	%r15, 41088(%r12)
	movl	%eax, 41104(%r12)
	cmpq	%rdx, %r13
	je	.L603
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L603:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L629
.L584:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L630
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L628:
	.cfi_restore_state
	movq	-1(%rcx), %rdx
	cmpw	$1023, 11(%rdx)
	jbe	.L631
	movl	$4, %esi
	call	_ZN2v88internal10JSReceiver18TestIntegrityLevelENS0_6HandleIS1_EENS0_18PropertyAttributesE@PLT
	movzbl	%ah, %ecx
	testb	%al, %al
	jne	.L597
	movl	41104(%r12), %eax
	movq	312(%r12), %r14
	movq	41096(%r12), %rdx
	subl	$1, %eax
	jmp	.L598
	.p2align 4,,10
	.p2align 3
.L627:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L632
.L590:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L591
	movq	(%rdi), %rax
	call	*8(%rax)
.L591:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L592
	movq	(%rdi), %rax
	call	*8(%rax)
.L592:
	leaq	.LC14(%rip), %rax
	movq	%r13, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L589
	.p2align 4,,10
	.p2align 3
.L626:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L633
.L588:
	movq	%r13, _ZZN2v88internalL33Builtin_Impl_Stats_ObjectIsSealedEiPmPNS0_7IsolateEE28trace_event_unique_atomic305(%rip)
	jmp	.L587
.L597:
	movl	41104(%r12), %eax
	movq	41096(%r12), %rdx
	subl	$1, %eax
	testb	%cl, %cl
	jne	.L604
	movq	120(%r12), %r14
	jmp	.L598
	.p2align 4,,10
	.p2align 3
.L631:
	movq	%r13, %rdx
	jmp	.L604
	.p2align 4,,10
	.p2align 3
.L629:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L584
	.p2align 4,,10
	.p2align 3
.L625:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$801, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L585
	.p2align 4,,10
	.p2align 3
.L633:
	leaq	.LC5(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L588
	.p2align 4,,10
	.p2align 3
.L632:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC14(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L590
.L630:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22592:
	.size	_ZN2v88internalL33Builtin_Impl_Stats_ObjectIsSealedEiPmPNS0_7IsolateE, .-_ZN2v88internalL33Builtin_Impl_Stats_ObjectIsSealedEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL37Builtin_Impl_Stats_ObjectLookupGetterEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC15:
	.string	"V8.Builtin_ObjectLookupGetter"
	.section	.text._ZN2v88internalL37Builtin_Impl_Stats_ObjectLookupGetterEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL37Builtin_Impl_Stats_ObjectLookupGetterEiPmPNS0_7IsolateE, @function
_ZN2v88internalL37Builtin_Impl_Stats_ObjectLookupGetterEiPmPNS0_7IsolateE:
.LFB22570:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L664
.L635:
	movq	_ZZN2v88internalL37Builtin_Impl_Stats_ObjectLookupGetterEiPmPNS0_7IsolateEE28trace_event_unique_atomic196(%rip), %rbx
	testq	%rbx, %rbx
	je	.L665
.L637:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L666
.L639:
	movq	41088(%r12), %r14
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	addl	$1, 41104(%r12)
	leaq	-8(%r13), %rdx
	movq	41096(%r12), %rbx
	call	_ZN2v88internal12_GLOBAL__N_120ObjectLookupAccessorEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_NS0_17AccessorComponentE
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	41096(%r12), %rbx
	je	.L643
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L643:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L667
.L634:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L668
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L665:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L669
.L638:
	movq	%rbx, _ZZN2v88internalL37Builtin_Impl_Stats_ObjectLookupGetterEiPmPNS0_7IsolateEE28trace_event_unique_atomic196(%rip)
	jmp	.L637
	.p2align 4,,10
	.p2align 3
.L666:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L670
.L640:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L641
	movq	(%rdi), %rax
	call	*8(%rax)
.L641:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L642
	movq	(%rdi), %rax
	call	*8(%rax)
.L642:
	leaq	.LC15(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L639
	.p2align 4,,10
	.p2align 3
.L667:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L634
	.p2align 4,,10
	.p2align 3
.L664:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$802, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L670:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC15(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L640
	.p2align 4,,10
	.p2align 3
.L669:
	leaq	.LC5(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L638
.L668:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22570:
	.size	_ZN2v88internalL37Builtin_Impl_Stats_ObjectLookupGetterEiPmPNS0_7IsolateE, .-_ZN2v88internalL37Builtin_Impl_Stats_ObjectLookupGetterEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL37Builtin_Impl_Stats_ObjectLookupSetterEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC16:
	.string	"V8.Builtin_ObjectLookupSetter"
	.section	.text._ZN2v88internalL37Builtin_Impl_Stats_ObjectLookupSetterEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL37Builtin_Impl_Stats_ObjectLookupSetterEiPmPNS0_7IsolateE, @function
_ZN2v88internalL37Builtin_Impl_Stats_ObjectLookupSetterEiPmPNS0_7IsolateE:
.LFB22573:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L701
.L672:
	movq	_ZZN2v88internalL37Builtin_Impl_Stats_ObjectLookupSetterEiPmPNS0_7IsolateEE28trace_event_unique_atomic205(%rip), %rbx
	testq	%rbx, %rbx
	je	.L702
.L674:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L703
.L676:
	leaq	-8(%r13), %rdx
	movq	%r13, %rsi
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	call	_ZN2v88internal12_GLOBAL__N_120ObjectLookupAccessorEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_NS0_17AccessorComponentE
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	41096(%r12), %rbx
	je	.L680
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L680:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L704
.L671:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L705
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L702:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L706
.L675:
	movq	%rbx, _ZZN2v88internalL37Builtin_Impl_Stats_ObjectLookupSetterEiPmPNS0_7IsolateEE28trace_event_unique_atomic205(%rip)
	jmp	.L674
	.p2align 4,,10
	.p2align 3
.L703:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L707
.L677:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L678
	movq	(%rdi), %rax
	call	*8(%rax)
.L678:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L679
	movq	(%rdi), %rax
	call	*8(%rax)
.L679:
	leaq	.LC16(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L676
	.p2align 4,,10
	.p2align 3
.L704:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L671
	.p2align 4,,10
	.p2align 3
.L701:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$803, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L672
	.p2align 4,,10
	.p2align 3
.L707:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC16(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L677
	.p2align 4,,10
	.p2align 3
.L706:
	leaq	.LC5(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L675
.L705:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22573:
	.size	_ZN2v88internalL37Builtin_Impl_Stats_ObjectLookupSetterEiPmPNS0_7IsolateE, .-_ZN2v88internalL37Builtin_Impl_Stats_ObjectLookupSetterEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL46Builtin_Impl_Stats_ObjectGetOwnPropertySymbolsEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC17:
	.string	"V8.Builtin_ObjectGetOwnPropertySymbols"
	.section	.text._ZN2v88internalL46Builtin_Impl_Stats_ObjectGetOwnPropertySymbolsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL46Builtin_Impl_Stats_ObjectGetOwnPropertySymbolsEiPmPNS0_7IsolateE, @function
_ZN2v88internalL46Builtin_Impl_Stats_ObjectGetOwnPropertySymbolsEiPmPNS0_7IsolateE:
.LFB22586:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edi, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L748
.L709:
	movq	_ZZN2v88internalL46Builtin_Impl_Stats_ObjectGetOwnPropertySymbolsEiPmPNS0_7IsolateEE28trace_event_unique_atomic288(%rip), %r13
	testq	%r13, %r13
	je	.L749
.L711:
	movq	$0, -160(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L750
.L713:
	addl	$1, 41104(%r12)
	leaq	-8(%rbx), %rdi
	leaq	88(%r12), %rax
	cmpl	$5, %r15d
	cmovle	%rax, %rdi
	movq	41088(%r12), %r14
	movq	41096(%r12), %r13
	movq	(%rdi), %rax
	testb	$1, %al
	jne	.L719
.L722:
	movq	%rdi, %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal6Object12ToObjectImplEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L747
.L721:
	xorl	%esi, %esi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$8, %edx
	call	_ZN2v88internal14KeyAccumulator7GetKeysENS0_6HandleINS0_10JSReceiverEEENS0_17KeyCollectionModeENS0_14PropertyFilterENS0_17GetKeysConversionEbb@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L747
	movq	(%rax), %rax
	xorl	%r8d, %r8d
	movl	$3, %edx
	movq	%r12, %rdi
	movslq	11(%rax), %rcx
	call	_ZN2v88internal7Factory22NewJSArrayWithElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE@PLT
	movq	(%rax), %r15
.L723:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L727
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L727:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L751
.L708:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L752
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L719:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L722
	jmp	.L721
	.p2align 4,,10
	.p2align 3
.L750:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L753
.L714:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L715
	movq	(%rdi), %rax
	call	*8(%rax)
.L715:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L716
	movq	(%rdi), %rax
	call	*8(%rax)
.L716:
	leaq	.LC17(%rip), %rax
	movq	%r13, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L713
	.p2align 4,,10
	.p2align 3
.L749:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L754
.L712:
	movq	%r13, _ZZN2v88internalL46Builtin_Impl_Stats_ObjectGetOwnPropertySymbolsEiPmPNS0_7IsolateEE28trace_event_unique_atomic288(%rip)
	jmp	.L711
	.p2align 4,,10
	.p2align 3
.L747:
	movq	312(%r12), %r15
	jmp	.L723
	.p2align 4,,10
	.p2align 3
.L748:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$799, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L709
	.p2align 4,,10
	.p2align 3
.L751:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L708
	.p2align 4,,10
	.p2align 3
.L754:
	leaq	.LC5(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L712
	.p2align 4,,10
	.p2align 3
.L753:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC17(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L714
.L752:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22586:
	.size	_ZN2v88internalL46Builtin_Impl_Stats_ObjectGetOwnPropertySymbolsEiPmPNS0_7IsolateE, .-_ZN2v88internalL46Builtin_Impl_Stats_ObjectGetOwnPropertySymbolsEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL54Builtin_Impl_Stats_ObjectPrototypePropertyIsEnumerableEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC18:
	.string	"V8.Builtin_ObjectPrototypePropertyIsEnumerable"
	.section	.text._ZN2v88internalL54Builtin_Impl_Stats_ObjectPrototypePropertyIsEnumerableEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL54Builtin_Impl_Stats_ObjectPrototypePropertyIsEnumerableEiPmPNS0_7IsolateE, @function
_ZN2v88internalL54Builtin_Impl_Stats_ObjectPrototypePropertyIsEnumerableEiPmPNS0_7IsolateE:
.LFB22553:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L784
.L756:
	movq	_ZZN2v88internalL54Builtin_Impl_Stats_ObjectPrototypePropertyIsEnumerableEiPmPNS0_7IsolateEE27trace_event_unique_atomic23(%rip), %rbx
	testq	%rbx, %rbx
	je	.L785
.L758:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L786
.L760:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL48Builtin_Impl_ObjectPrototypePropertyIsEnumerableENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L787
.L764:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L788
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L785:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L789
.L759:
	movq	%rbx, _ZZN2v88internalL54Builtin_Impl_Stats_ObjectPrototypePropertyIsEnumerableEiPmPNS0_7IsolateEE27trace_event_unique_atomic23(%rip)
	jmp	.L758
	.p2align 4,,10
	.p2align 3
.L786:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L790
.L761:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L762
	movq	(%rdi), %rax
	call	*8(%rax)
.L762:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L763
	movq	(%rdi), %rax
	call	*8(%rax)
.L763:
	leaq	.LC18(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L760
	.p2align 4,,10
	.p2align 3
.L787:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L764
	.p2align 4,,10
	.p2align 3
.L784:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$804, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L756
	.p2align 4,,10
	.p2align 3
.L790:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC18(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L761
	.p2align 4,,10
	.p2align 3
.L789:
	leaq	.LC5(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L759
.L788:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22553:
	.size	_ZN2v88internalL54Builtin_Impl_Stats_ObjectPrototypePropertyIsEnumerableEiPmPNS0_7IsolateE, .-_ZN2v88internalL54Builtin_Impl_Stats_ObjectPrototypePropertyIsEnumerableEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL50Builtin_Impl_Stats_ObjectGetOwnPropertyDescriptorsEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC19:
	.string	"V8.Builtin_ObjectGetOwnPropertyDescriptors"
	.section	.text._ZN2v88internalL50Builtin_Impl_Stats_ObjectGetOwnPropertyDescriptorsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL50Builtin_Impl_Stats_ObjectGetOwnPropertyDescriptorsEiPmPNS0_7IsolateE, @function
_ZN2v88internalL50Builtin_Impl_Stats_ObjectGetOwnPropertyDescriptorsEiPmPNS0_7IsolateE:
.LFB22595:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L820
.L792:
	movq	_ZZN2v88internalL50Builtin_Impl_Stats_ObjectGetOwnPropertyDescriptorsEiPmPNS0_7IsolateEE28trace_event_unique_atomic316(%rip), %rbx
	testq	%rbx, %rbx
	je	.L821
.L794:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L822
.L796:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL44Builtin_Impl_ObjectGetOwnPropertyDescriptorsENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L823
.L800:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L824
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L821:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L825
.L795:
	movq	%rbx, _ZZN2v88internalL50Builtin_Impl_Stats_ObjectGetOwnPropertyDescriptorsEiPmPNS0_7IsolateEE28trace_event_unique_atomic316(%rip)
	jmp	.L794
	.p2align 4,,10
	.p2align 3
.L822:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L826
.L797:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L798
	movq	(%rdi), %rax
	call	*8(%rax)
.L798:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L799
	movq	(%rdi), %rax
	call	*8(%rax)
.L799:
	leaq	.LC19(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L796
	.p2align 4,,10
	.p2align 3
.L823:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L800
	.p2align 4,,10
	.p2align 3
.L820:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$798, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L792
	.p2align 4,,10
	.p2align 3
.L826:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC19(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L797
	.p2align 4,,10
	.p2align 3
.L825:
	leaq	.LC5(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L795
.L824:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22595:
	.size	_ZN2v88internalL50Builtin_Impl_Stats_ObjectGetOwnPropertyDescriptorsEiPmPNS0_7IsolateE, .-_ZN2v88internalL50Builtin_Impl_Stats_ObjectGetOwnPropertyDescriptorsEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL42Builtin_Impl_Stats_ObjectPrototypeGetProtoEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC20:
	.string	"V8.Builtin_ObjectPrototypeGetProto"
	.section	.text._ZN2v88internalL42Builtin_Impl_Stats_ObjectPrototypeGetProtoEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL42Builtin_Impl_Stats_ObjectPrototypeGetProtoEiPmPNS0_7IsolateE, @function
_ZN2v88internalL42Builtin_Impl_Stats_ObjectPrototypeGetProtoEiPmPNS0_7IsolateE:
.LFB22579:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L856
.L828:
	movq	_ZZN2v88internalL42Builtin_Impl_Stats_ObjectPrototypeGetProtoEiPmPNS0_7IsolateEE28trace_event_unique_atomic225(%rip), %rbx
	testq	%rbx, %rbx
	je	.L857
.L830:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L858
.L832:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL36Builtin_Impl_ObjectPrototypeGetProtoENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L859
.L836:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L860
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L857:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L861
.L831:
	movq	%rbx, _ZZN2v88internalL42Builtin_Impl_Stats_ObjectPrototypeGetProtoEiPmPNS0_7IsolateEE28trace_event_unique_atomic225(%rip)
	jmp	.L830
	.p2align 4,,10
	.p2align 3
.L858:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L862
.L833:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L834
	movq	(%rdi), %rax
	call	*8(%rax)
.L834:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L835
	movq	(%rdi), %rax
	call	*8(%rax)
.L835:
	leaq	.LC20(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L832
	.p2align 4,,10
	.p2align 3
.L859:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L836
	.p2align 4,,10
	.p2align 3
.L856:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$805, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L828
	.p2align 4,,10
	.p2align 3
.L862:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC20(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L833
	.p2align 4,,10
	.p2align 3
.L861:
	leaq	.LC5(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L831
.L860:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22579:
	.size	_ZN2v88internalL42Builtin_Impl_Stats_ObjectPrototypeGetProtoEiPmPNS0_7IsolateE, .-_ZN2v88internalL42Builtin_Impl_Stats_ObjectPrototypeGetProtoEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal43Builtin_ObjectPrototypePropertyIsEnumerableEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal43Builtin_ObjectPrototypePropertyIsEnumerableEiPmPNS0_7IsolateE
	.type	_ZN2v88internal43Builtin_ObjectPrototypePropertyIsEnumerableEiPmPNS0_7IsolateE, @function
_ZN2v88internal43Builtin_ObjectPrototypePropertyIsEnumerableEiPmPNS0_7IsolateE:
.LFB22554:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L867
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL48Builtin_Impl_ObjectPrototypePropertyIsEnumerableENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L867:
	.cfi_restore 6
	jmp	_ZN2v88internalL54Builtin_Impl_Stats_ObjectPrototypePropertyIsEnumerableEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE22554:
	.size	_ZN2v88internal43Builtin_ObjectPrototypePropertyIsEnumerableEiPmPNS0_7IsolateE, .-_ZN2v88internal43Builtin_ObjectPrototypePropertyIsEnumerableEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal30Builtin_ObjectDefinePropertiesEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal30Builtin_ObjectDefinePropertiesEiPmPNS0_7IsolateE
	.type	_ZN2v88internal30Builtin_ObjectDefinePropertiesEiPmPNS0_7IsolateE, @function
_ZN2v88internal30Builtin_ObjectDefinePropertiesEiPmPNS0_7IsolateE:
.LFB22557:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L875
	addl	$1, 41104(%rdx)
	leaq	-8(%rsi), %r8
	movq	%r12, %rdi
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r13
	leaq	-16(%rsi), %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal10JSReceiver16DefinePropertiesEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_@PLT
	testq	%rax, %rax
	je	.L876
	movq	(%rax), %r14
.L871:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L868
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L868:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L876:
	.cfi_restore_state
	movq	312(%r12), %r14
	jmp	.L871
	.p2align 4,,10
	.p2align 3
.L875:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL41Builtin_Impl_Stats_ObjectDefinePropertiesEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE22557:
	.size	_ZN2v88internal30Builtin_ObjectDefinePropertiesEiPmPNS0_7IsolateE, .-_ZN2v88internal30Builtin_ObjectDefinePropertiesEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal28Builtin_ObjectDefinePropertyEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal28Builtin_ObjectDefinePropertyEiPmPNS0_7IsolateE
	.type	_ZN2v88internal28Builtin_ObjectDefinePropertyEiPmPNS0_7IsolateE, @function
_ZN2v88internal28Builtin_ObjectDefinePropertyEiPmPNS0_7IsolateE:
.LFB22560:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L881
	addl	$1, 41104(%rdx)
	leaq	-8(%rsi), %r8
	leaq	-24(%rsi), %rcx
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%r12, %rdi
	leaq	-16(%rsi), %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal10JSReceiver14DefinePropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_@PLT
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	41096(%r12), %rbx
	je	.L879
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L879:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L881:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL39Builtin_Impl_Stats_ObjectDefinePropertyEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE22560:
	.size	_ZN2v88internal28Builtin_ObjectDefinePropertyEiPmPNS0_7IsolateE, .-_ZN2v88internal28Builtin_ObjectDefinePropertyEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal26Builtin_ObjectDefineGetterEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal26Builtin_ObjectDefineGetterEiPmPNS0_7IsolateE
	.type	_ZN2v88internal26Builtin_ObjectDefineGetterEiPmPNS0_7IsolateE, @function
_ZN2v88internal26Builtin_ObjectDefineGetterEiPmPNS0_7IsolateE:
.LFB22565:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L886
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL31Builtin_Impl_ObjectDefineGetterENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L886:
	.cfi_restore 6
	jmp	_ZN2v88internalL37Builtin_Impl_Stats_ObjectDefineGetterEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE22565:
	.size	_ZN2v88internal26Builtin_ObjectDefineGetterEiPmPNS0_7IsolateE, .-_ZN2v88internal26Builtin_ObjectDefineGetterEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal26Builtin_ObjectDefineSetterEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal26Builtin_ObjectDefineSetterEiPmPNS0_7IsolateE
	.type	_ZN2v88internal26Builtin_ObjectDefineSetterEiPmPNS0_7IsolateE, @function
_ZN2v88internal26Builtin_ObjectDefineSetterEiPmPNS0_7IsolateE:
.LFB22568:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L891
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL31Builtin_Impl_ObjectDefineSetterENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L891:
	.cfi_restore 6
	jmp	_ZN2v88internalL37Builtin_Impl_Stats_ObjectDefineSetterEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE22568:
	.size	_ZN2v88internal26Builtin_ObjectDefineSetterEiPmPNS0_7IsolateE, .-_ZN2v88internal26Builtin_ObjectDefineSetterEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal26Builtin_ObjectLookupGetterEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal26Builtin_ObjectLookupGetterEiPmPNS0_7IsolateE
	.type	_ZN2v88internal26Builtin_ObjectLookupGetterEiPmPNS0_7IsolateE, @function
_ZN2v88internal26Builtin_ObjectLookupGetterEiPmPNS0_7IsolateE:
.LFB22571:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L896
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r14
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	41096(%rdx), %rbx
	leaq	-8(%rsi), %rdx
	call	_ZN2v88internal12_GLOBAL__N_120ObjectLookupAccessorEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_NS0_17AccessorComponentE
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	41096(%r12), %rbx
	je	.L894
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L894:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L896:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL37Builtin_Impl_Stats_ObjectLookupGetterEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE22571:
	.size	_ZN2v88internal26Builtin_ObjectLookupGetterEiPmPNS0_7IsolateE, .-_ZN2v88internal26Builtin_ObjectLookupGetterEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal26Builtin_ObjectLookupSetterEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal26Builtin_ObjectLookupSetterEiPmPNS0_7IsolateE
	.type	_ZN2v88internal26Builtin_ObjectLookupSetterEiPmPNS0_7IsolateE, @function
_ZN2v88internal26Builtin_ObjectLookupSetterEiPmPNS0_7IsolateE:
.LFB22574:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L901
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r14
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	41096(%rdx), %rbx
	leaq	-8(%rsi), %rdx
	call	_ZN2v88internal12_GLOBAL__N_120ObjectLookupAccessorEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_NS0_17AccessorComponentE
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	41096(%r12), %rbx
	je	.L899
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L899:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L901:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL37Builtin_Impl_Stats_ObjectLookupSetterEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE22574:
	.size	_ZN2v88internal26Builtin_ObjectLookupSetterEiPmPNS0_7IsolateE, .-_ZN2v88internal26Builtin_ObjectLookupSetterEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal20Builtin_ObjectFreezeEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal20Builtin_ObjectFreezeEiPmPNS0_7IsolateE
	.type	_ZN2v88internal20Builtin_ObjectFreezeEiPmPNS0_7IsolateE, @function
_ZN2v88internal20Builtin_ObjectFreezeEiPmPNS0_7IsolateE:
.LFB22577:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L914
	movl	41104(%rdx), %eax
	subq	$8, %rsi
	leaq	88(%r12), %rbx
	cmpl	$5, %edi
	cmovg	%rsi, %rbx
	movq	41088(%rdx), %r15
	movq	41096(%rdx), %r14
	leal	1(%rax), %edx
	movl	%edx, 41104(%r12)
	movq	(%rbx), %r13
	testb	$1, %r13b
	jne	.L906
	movl	%eax, 41104(%r12)
.L902:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L906:
	.cfi_restore_state
	movq	-1(%r13), %rdx
	cmpw	$1023, 11(%rdx)
	ja	.L908
	movq	%r14, %rdx
.L911:
	movq	%r15, 41088(%r12)
	movl	%eax, 41104(%r12)
	cmpq	%r14, %rdx
	je	.L902
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	jmp	.L902
	.p2align 4,,10
	.p2align 3
.L908:
	xorl	%edx, %edx
	movl	$5, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal10JSReceiver17SetIntegrityLevelENS0_6HandleIS1_EENS0_18PropertyAttributesENS0_11ShouldThrowE@PLT
	testb	%al, %al
	jne	.L910
	movl	41104(%r12), %eax
	movq	312(%r12), %r13
	movq	41096(%r12), %rdx
	subl	$1, %eax
	jmp	.L911
	.p2align 4,,10
	.p2align 3
.L914:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL31Builtin_Impl_Stats_ObjectFreezeEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L910:
	.cfi_restore_state
	movl	41104(%r12), %eax
	movq	(%rbx), %r13
	movq	41096(%r12), %rdx
	subl	$1, %eax
	jmp	.L911
	.cfi_endproc
.LFE22577:
	.size	_ZN2v88internal20Builtin_ObjectFreezeEiPmPNS0_7IsolateE, .-_ZN2v88internal20Builtin_ObjectFreezeEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal31Builtin_ObjectPrototypeGetProtoEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal31Builtin_ObjectPrototypeGetProtoEiPmPNS0_7IsolateE
	.type	_ZN2v88internal31Builtin_ObjectPrototypeGetProtoEiPmPNS0_7IsolateE, @function
_ZN2v88internal31Builtin_ObjectPrototypeGetProtoEiPmPNS0_7IsolateE:
.LFB22580:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L919
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL36Builtin_Impl_ObjectPrototypeGetProtoENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L919:
	.cfi_restore 6
	jmp	_ZN2v88internalL42Builtin_Impl_Stats_ObjectPrototypeGetProtoEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE22580:
	.size	_ZN2v88internal31Builtin_ObjectPrototypeGetProtoEiPmPNS0_7IsolateE, .-_ZN2v88internal31Builtin_ObjectPrototypeGetProtoEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal31Builtin_ObjectPrototypeSetProtoEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal31Builtin_ObjectPrototypeSetProtoEiPmPNS0_7IsolateE
	.type	_ZN2v88internal31Builtin_ObjectPrototypeSetProtoEiPmPNS0_7IsolateE, @function
_ZN2v88internal31Builtin_ObjectPrototypeSetProtoEiPmPNS0_7IsolateE:
.LFB22583:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L924
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL36Builtin_Impl_ObjectPrototypeSetProtoENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L924:
	.cfi_restore 6
	jmp	_ZN2v88internalL42Builtin_Impl_Stats_ObjectPrototypeSetProtoEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE22583:
	.size	_ZN2v88internal31Builtin_ObjectPrototypeSetProtoEiPmPNS0_7IsolateE, .-_ZN2v88internal31Builtin_ObjectPrototypeSetProtoEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal35Builtin_ObjectGetOwnPropertySymbolsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal35Builtin_ObjectGetOwnPropertySymbolsEiPmPNS0_7IsolateE
	.type	_ZN2v88internal35Builtin_ObjectGetOwnPropertySymbolsEiPmPNS0_7IsolateE, @function
_ZN2v88internal35Builtin_ObjectGetOwnPropertySymbolsEiPmPNS0_7IsolateE:
.LFB22587:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L939
	addl	$1, 41104(%rdx)
	subq	$8, %rsi
	leaq	88(%rdx), %rax
	cmpl	$5, %edi
	cmovg	%rsi, %rax
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L929
.L932:
	movq	%rdi, %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal6Object12ToObjectImplEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L938
.L931:
	xorl	%esi, %esi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$8, %edx
	call	_ZN2v88internal14KeyAccumulator7GetKeysENS0_6HandleINS0_10JSReceiverEEENS0_17KeyCollectionModeENS0_14PropertyFilterENS0_17GetKeysConversionEbb@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L938
	movq	(%rax), %rax
	xorl	%r8d, %r8d
	movl	$3, %edx
	movq	%r12, %rdi
	movslq	11(%rax), %rcx
	call	_ZN2v88internal7Factory22NewJSArrayWithElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE@PLT
	movq	(%rax), %r14
.L933:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L925
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L925:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L929:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L932
	jmp	.L931
	.p2align 4,,10
	.p2align 3
.L938:
	movq	312(%r12), %r14
	jmp	.L933
	.p2align 4,,10
	.p2align 3
.L939:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL46Builtin_Impl_Stats_ObjectGetOwnPropertySymbolsEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE22587:
	.size	_ZN2v88internal35Builtin_ObjectGetOwnPropertySymbolsEiPmPNS0_7IsolateE, .-_ZN2v88internal35Builtin_ObjectGetOwnPropertySymbolsEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal22Builtin_ObjectIsFrozenEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal22Builtin_ObjectIsFrozenEiPmPNS0_7IsolateE
	.type	_ZN2v88internal22Builtin_ObjectIsFrozenEiPmPNS0_7IsolateE, @function
_ZN2v88internal22Builtin_ObjectIsFrozenEiPmPNS0_7IsolateE:
.LFB22590:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L955
	movl	41104(%rdx), %eax
	movq	41096(%rdx), %rbx
	subq	$8, %rsi
	cmpl	$5, %edi
	movq	41088(%rdx), %r13
	leal	1(%rax), %edx
	movl	%edx, 41104(%r12)
	leaq	88(%r12), %rdx
	movq	%rdx, %rdi
	movq	%rbx, %rdx
	cmovg	%rsi, %rdi
	movq	(%rdi), %rcx
	testb	$1, %cl
	jne	.L956
.L952:
	movq	112(%r12), %r14
.L947:
	movq	%r13, 41088(%r12)
	movl	%eax, 41104(%r12)
	cmpq	%rdx, %rbx
	je	.L940
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L940:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L956:
	.cfi_restore_state
	movq	-1(%rcx), %rdx
	cmpw	$1023, 11(%rdx)
	jbe	.L957
	movl	$5, %esi
	call	_ZN2v88internal10JSReceiver18TestIntegrityLevelENS0_6HandleIS1_EENS0_18PropertyAttributesE@PLT
	movzbl	%ah, %ecx
	testb	%al, %al
	jne	.L946
	movl	41104(%r12), %eax
	movq	312(%r12), %r14
	movq	41096(%r12), %rdx
	subl	$1, %eax
	jmp	.L947
.L946:
	movl	41104(%r12), %eax
	movq	41096(%r12), %rdx
	subl	$1, %eax
	testb	%cl, %cl
	jne	.L952
	movq	120(%r12), %r14
	jmp	.L947
	.p2align 4,,10
	.p2align 3
.L957:
	movq	%rbx, %rdx
	jmp	.L952
	.p2align 4,,10
	.p2align 3
.L955:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL33Builtin_Impl_Stats_ObjectIsFrozenEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE22590:
	.size	_ZN2v88internal22Builtin_ObjectIsFrozenEiPmPNS0_7IsolateE, .-_ZN2v88internal22Builtin_ObjectIsFrozenEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal22Builtin_ObjectIsSealedEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal22Builtin_ObjectIsSealedEiPmPNS0_7IsolateE
	.type	_ZN2v88internal22Builtin_ObjectIsSealedEiPmPNS0_7IsolateE, @function
_ZN2v88internal22Builtin_ObjectIsSealedEiPmPNS0_7IsolateE:
.LFB22593:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L973
	movl	41104(%rdx), %eax
	movq	41096(%rdx), %rbx
	subq	$8, %rsi
	cmpl	$5, %edi
	movq	41088(%rdx), %r13
	leal	1(%rax), %edx
	movl	%edx, 41104(%r12)
	leaq	88(%r12), %rdx
	movq	%rdx, %rdi
	movq	%rbx, %rdx
	cmovg	%rsi, %rdi
	movq	(%rdi), %rcx
	testb	$1, %cl
	jne	.L974
.L970:
	movq	112(%r12), %r14
.L965:
	movq	%r13, 41088(%r12)
	movl	%eax, 41104(%r12)
	cmpq	%rdx, %rbx
	je	.L958
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L958:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L974:
	.cfi_restore_state
	movq	-1(%rcx), %rdx
	cmpw	$1023, 11(%rdx)
	jbe	.L975
	movl	$4, %esi
	call	_ZN2v88internal10JSReceiver18TestIntegrityLevelENS0_6HandleIS1_EENS0_18PropertyAttributesE@PLT
	movzbl	%ah, %ecx
	testb	%al, %al
	jne	.L964
	movl	41104(%r12), %eax
	movq	312(%r12), %r14
	movq	41096(%r12), %rdx
	subl	$1, %eax
	jmp	.L965
.L964:
	movl	41104(%r12), %eax
	movq	41096(%r12), %rdx
	subl	$1, %eax
	testb	%cl, %cl
	jne	.L970
	movq	120(%r12), %r14
	jmp	.L965
	.p2align 4,,10
	.p2align 3
.L975:
	movq	%rbx, %rdx
	jmp	.L970
	.p2align 4,,10
	.p2align 3
.L973:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL33Builtin_Impl_Stats_ObjectIsSealedEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE22593:
	.size	_ZN2v88internal22Builtin_ObjectIsSealedEiPmPNS0_7IsolateE, .-_ZN2v88internal22Builtin_ObjectIsSealedEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal39Builtin_ObjectGetOwnPropertyDescriptorsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal39Builtin_ObjectGetOwnPropertyDescriptorsEiPmPNS0_7IsolateE
	.type	_ZN2v88internal39Builtin_ObjectGetOwnPropertyDescriptorsEiPmPNS0_7IsolateE, @function
_ZN2v88internal39Builtin_ObjectGetOwnPropertyDescriptorsEiPmPNS0_7IsolateE:
.LFB22596:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L980
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL44Builtin_Impl_ObjectGetOwnPropertyDescriptorsENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L980:
	.cfi_restore 6
	jmp	_ZN2v88internalL50Builtin_Impl_Stats_ObjectGetOwnPropertyDescriptorsEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE22596:
	.size	_ZN2v88internal39Builtin_ObjectGetOwnPropertyDescriptorsEiPmPNS0_7IsolateE, .-_ZN2v88internal39Builtin_ObjectGetOwnPropertyDescriptorsEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal18Builtin_ObjectSealEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal18Builtin_ObjectSealEiPmPNS0_7IsolateE
	.type	_ZN2v88internal18Builtin_ObjectSealEiPmPNS0_7IsolateE, @function
_ZN2v88internal18Builtin_ObjectSealEiPmPNS0_7IsolateE:
.LFB22599:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L993
	movl	41104(%rdx), %eax
	subq	$8, %rsi
	leaq	88(%r12), %rbx
	cmpl	$5, %edi
	cmovg	%rsi, %rbx
	movq	41088(%rdx), %r15
	movq	41096(%rdx), %r14
	leal	1(%rax), %edx
	movl	%edx, 41104(%r12)
	movq	(%rbx), %r13
	testb	$1, %r13b
	jne	.L985
	movl	%eax, 41104(%r12)
.L981:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L985:
	.cfi_restore_state
	movq	-1(%r13), %rdx
	cmpw	$1023, 11(%rdx)
	ja	.L987
	movq	%r14, %rdx
.L990:
	movq	%r15, 41088(%r12)
	movl	%eax, 41104(%r12)
	cmpq	%r14, %rdx
	je	.L981
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	jmp	.L981
	.p2align 4,,10
	.p2align 3
.L987:
	xorl	%edx, %edx
	movl	$4, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal10JSReceiver17SetIntegrityLevelENS0_6HandleIS1_EENS0_18PropertyAttributesENS0_11ShouldThrowE@PLT
	testb	%al, %al
	jne	.L989
	movl	41104(%r12), %eax
	movq	312(%r12), %r13
	movq	41096(%r12), %rdx
	subl	$1, %eax
	jmp	.L990
	.p2align 4,,10
	.p2align 3
.L993:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL29Builtin_Impl_Stats_ObjectSealEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L989:
	.cfi_restore_state
	movl	41104(%r12), %eax
	movq	(%rbx), %r13
	movq	41096(%r12), %rdx
	subl	$1, %eax
	jmp	.L990
	.cfi_endproc
.LFE22599:
	.size	_ZN2v88internal18Builtin_ObjectSealEiPmPNS0_7IsolateE, .-_ZN2v88internal18Builtin_ObjectSealEiPmPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal43Builtin_ObjectPrototypePropertyIsEnumerableEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal43Builtin_ObjectPrototypePropertyIsEnumerableEiPmPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal43Builtin_ObjectPrototypePropertyIsEnumerableEiPmPNS0_7IsolateE:
.LFB28343:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE28343:
	.size	_GLOBAL__sub_I__ZN2v88internal43Builtin_ObjectPrototypePropertyIsEnumerableEiPmPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal43Builtin_ObjectPrototypePropertyIsEnumerableEiPmPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal43Builtin_ObjectPrototypePropertyIsEnumerableEiPmPNS0_7IsolateE
	.section	.bss._ZZN2v88internalL29Builtin_Impl_Stats_ObjectSealEiPmPNS0_7IsolateEE28trace_event_unique_atomic352,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL29Builtin_Impl_Stats_ObjectSealEiPmPNS0_7IsolateEE28trace_event_unique_atomic352, @object
	.size	_ZZN2v88internalL29Builtin_Impl_Stats_ObjectSealEiPmPNS0_7IsolateEE28trace_event_unique_atomic352, 8
_ZZN2v88internalL29Builtin_Impl_Stats_ObjectSealEiPmPNS0_7IsolateEE28trace_event_unique_atomic352:
	.zero	8
	.section	.bss._ZZN2v88internalL50Builtin_Impl_Stats_ObjectGetOwnPropertyDescriptorsEiPmPNS0_7IsolateEE28trace_event_unique_atomic316,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL50Builtin_Impl_Stats_ObjectGetOwnPropertyDescriptorsEiPmPNS0_7IsolateEE28trace_event_unique_atomic316, @object
	.size	_ZZN2v88internalL50Builtin_Impl_Stats_ObjectGetOwnPropertyDescriptorsEiPmPNS0_7IsolateEE28trace_event_unique_atomic316, 8
_ZZN2v88internalL50Builtin_Impl_Stats_ObjectGetOwnPropertyDescriptorsEiPmPNS0_7IsolateEE28trace_event_unique_atomic316:
	.zero	8
	.section	.bss._ZZN2v88internalL33Builtin_Impl_Stats_ObjectIsSealedEiPmPNS0_7IsolateEE28trace_event_unique_atomic305,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL33Builtin_Impl_Stats_ObjectIsSealedEiPmPNS0_7IsolateEE28trace_event_unique_atomic305, @object
	.size	_ZZN2v88internalL33Builtin_Impl_Stats_ObjectIsSealedEiPmPNS0_7IsolateEE28trace_event_unique_atomic305, 8
_ZZN2v88internalL33Builtin_Impl_Stats_ObjectIsSealedEiPmPNS0_7IsolateEE28trace_event_unique_atomic305:
	.zero	8
	.section	.bss._ZZN2v88internalL33Builtin_Impl_Stats_ObjectIsFrozenEiPmPNS0_7IsolateEE28trace_event_unique_atomic293,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL33Builtin_Impl_Stats_ObjectIsFrozenEiPmPNS0_7IsolateEE28trace_event_unique_atomic293, @object
	.size	_ZZN2v88internalL33Builtin_Impl_Stats_ObjectIsFrozenEiPmPNS0_7IsolateEE28trace_event_unique_atomic293, 8
_ZZN2v88internalL33Builtin_Impl_Stats_ObjectIsFrozenEiPmPNS0_7IsolateEE28trace_event_unique_atomic293:
	.zero	8
	.section	.bss._ZZN2v88internalL46Builtin_Impl_Stats_ObjectGetOwnPropertySymbolsEiPmPNS0_7IsolateEE28trace_event_unique_atomic288,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL46Builtin_Impl_Stats_ObjectGetOwnPropertySymbolsEiPmPNS0_7IsolateEE28trace_event_unique_atomic288, @object
	.size	_ZZN2v88internalL46Builtin_Impl_Stats_ObjectGetOwnPropertySymbolsEiPmPNS0_7IsolateEE28trace_event_unique_atomic288, 8
_ZZN2v88internalL46Builtin_Impl_Stats_ObjectGetOwnPropertySymbolsEiPmPNS0_7IsolateEE28trace_event_unique_atomic288:
	.zero	8
	.section	.bss._ZZN2v88internalL42Builtin_Impl_Stats_ObjectPrototypeSetProtoEiPmPNS0_7IsolateEE28trace_event_unique_atomic238,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL42Builtin_Impl_Stats_ObjectPrototypeSetProtoEiPmPNS0_7IsolateEE28trace_event_unique_atomic238, @object
	.size	_ZZN2v88internalL42Builtin_Impl_Stats_ObjectPrototypeSetProtoEiPmPNS0_7IsolateEE28trace_event_unique_atomic238, 8
_ZZN2v88internalL42Builtin_Impl_Stats_ObjectPrototypeSetProtoEiPmPNS0_7IsolateEE28trace_event_unique_atomic238:
	.zero	8
	.section	.bss._ZZN2v88internalL42Builtin_Impl_Stats_ObjectPrototypeGetProtoEiPmPNS0_7IsolateEE28trace_event_unique_atomic225,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL42Builtin_Impl_Stats_ObjectPrototypeGetProtoEiPmPNS0_7IsolateEE28trace_event_unique_atomic225, @object
	.size	_ZZN2v88internalL42Builtin_Impl_Stats_ObjectPrototypeGetProtoEiPmPNS0_7IsolateEE28trace_event_unique_atomic225, 8
_ZZN2v88internalL42Builtin_Impl_Stats_ObjectPrototypeGetProtoEiPmPNS0_7IsolateEE28trace_event_unique_atomic225:
	.zero	8
	.section	.bss._ZZN2v88internalL31Builtin_Impl_Stats_ObjectFreezeEiPmPNS0_7IsolateEE28trace_event_unique_atomic213,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL31Builtin_Impl_Stats_ObjectFreezeEiPmPNS0_7IsolateEE28trace_event_unique_atomic213, @object
	.size	_ZZN2v88internalL31Builtin_Impl_Stats_ObjectFreezeEiPmPNS0_7IsolateEE28trace_event_unique_atomic213, 8
_ZZN2v88internalL31Builtin_Impl_Stats_ObjectFreezeEiPmPNS0_7IsolateEE28trace_event_unique_atomic213:
	.zero	8
	.section	.bss._ZZN2v88internalL37Builtin_Impl_Stats_ObjectLookupSetterEiPmPNS0_7IsolateEE28trace_event_unique_atomic205,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL37Builtin_Impl_Stats_ObjectLookupSetterEiPmPNS0_7IsolateEE28trace_event_unique_atomic205, @object
	.size	_ZZN2v88internalL37Builtin_Impl_Stats_ObjectLookupSetterEiPmPNS0_7IsolateEE28trace_event_unique_atomic205, 8
_ZZN2v88internalL37Builtin_Impl_Stats_ObjectLookupSetterEiPmPNS0_7IsolateEE28trace_event_unique_atomic205:
	.zero	8
	.section	.bss._ZZN2v88internalL37Builtin_Impl_Stats_ObjectLookupGetterEiPmPNS0_7IsolateEE28trace_event_unique_atomic196,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL37Builtin_Impl_Stats_ObjectLookupGetterEiPmPNS0_7IsolateEE28trace_event_unique_atomic196, @object
	.size	_ZZN2v88internalL37Builtin_Impl_Stats_ObjectLookupGetterEiPmPNS0_7IsolateEE28trace_event_unique_atomic196, 8
_ZZN2v88internalL37Builtin_Impl_Stats_ObjectLookupGetterEiPmPNS0_7IsolateEE28trace_event_unique_atomic196:
	.zero	8
	.section	.bss._ZZN2v88internalL37Builtin_Impl_Stats_ObjectDefineSetterEiPmPNS0_7IsolateEE28trace_event_unique_atomic186,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL37Builtin_Impl_Stats_ObjectDefineSetterEiPmPNS0_7IsolateEE28trace_event_unique_atomic186, @object
	.size	_ZZN2v88internalL37Builtin_Impl_Stats_ObjectDefineSetterEiPmPNS0_7IsolateEE28trace_event_unique_atomic186, 8
_ZZN2v88internalL37Builtin_Impl_Stats_ObjectDefineSetterEiPmPNS0_7IsolateEE28trace_event_unique_atomic186:
	.zero	8
	.section	.bss._ZZN2v88internalL37Builtin_Impl_Stats_ObjectDefineGetterEiPmPNS0_7IsolateEE28trace_event_unique_atomic176,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL37Builtin_Impl_Stats_ObjectDefineGetterEiPmPNS0_7IsolateEE28trace_event_unique_atomic176, @object
	.size	_ZZN2v88internalL37Builtin_Impl_Stats_ObjectDefineGetterEiPmPNS0_7IsolateEE28trace_event_unique_atomic176, 8
_ZZN2v88internalL37Builtin_Impl_Stats_ObjectDefineGetterEiPmPNS0_7IsolateEE28trace_event_unique_atomic176:
	.zero	8
	.section	.bss._ZZN2v88internalL39Builtin_Impl_Stats_ObjectDefinePropertyEiPmPNS0_7IsolateEE27trace_event_unique_atomic50,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL39Builtin_Impl_Stats_ObjectDefinePropertyEiPmPNS0_7IsolateEE27trace_event_unique_atomic50, @object
	.size	_ZZN2v88internalL39Builtin_Impl_Stats_ObjectDefinePropertyEiPmPNS0_7IsolateEE27trace_event_unique_atomic50, 8
_ZZN2v88internalL39Builtin_Impl_Stats_ObjectDefinePropertyEiPmPNS0_7IsolateEE27trace_event_unique_atomic50:
	.zero	8
	.section	.bss._ZZN2v88internalL41Builtin_Impl_Stats_ObjectDefinePropertiesEiPmPNS0_7IsolateEE27trace_event_unique_atomic39,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL41Builtin_Impl_Stats_ObjectDefinePropertiesEiPmPNS0_7IsolateEE27trace_event_unique_atomic39, @object
	.size	_ZZN2v88internalL41Builtin_Impl_Stats_ObjectDefinePropertiesEiPmPNS0_7IsolateEE27trace_event_unique_atomic39, 8
_ZZN2v88internalL41Builtin_Impl_Stats_ObjectDefinePropertiesEiPmPNS0_7IsolateEE27trace_event_unique_atomic39:
	.zero	8
	.section	.bss._ZZN2v88internalL54Builtin_Impl_Stats_ObjectPrototypePropertyIsEnumerableEiPmPNS0_7IsolateEE27trace_event_unique_atomic23,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL54Builtin_Impl_Stats_ObjectPrototypePropertyIsEnumerableEiPmPNS0_7IsolateEE27trace_event_unique_atomic23, @object
	.size	_ZZN2v88internalL54Builtin_Impl_Stats_ObjectPrototypePropertyIsEnumerableEiPmPNS0_7IsolateEE27trace_event_unique_atomic23, 8
_ZZN2v88internalL54Builtin_Impl_Stats_ObjectPrototypePropertyIsEnumerableEiPmPNS0_7IsolateEE27trace_event_unique_atomic23:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
