	.file	"js-break-iterator.cc"
	.text
	.section	.text._ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data,"axG",@progbits,_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data,comdat
	.p2align 4
	.weak	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data
	.type	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data, @function
_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data:
.LFB22646:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r8
	movq	(%rdi), %rax
	movq	%r8, %rdi
	jmp	*%rax
	.cfi_endproc
.LFE22646:
	.size	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data, .-_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation,"axG",@progbits,_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation,comdat
	.p2align 4
	.weak	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation
	.type	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation:
.LFB22647:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L4
	cmpl	$3, %edx
	je	.L5
	cmpl	$1, %edx
	je	.L9
.L5:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movdqu	(%rsi), %xmm0
	xorl	%eax, %eax
	movups	%xmm0, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE22647:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation
	.section	.text._ZNSt15_Sp_counted_ptrIPN6icu_6713UnicodeStringELN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN6icu_6713UnicodeStringELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN6icu_6713UnicodeStringELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt15_Sp_counted_ptrIPN6icu_6713UnicodeStringELN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt15_Sp_counted_ptrIPN6icu_6713UnicodeStringELN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB22737:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE22737:
	.size	_ZNSt15_Sp_counted_ptrIPN6icu_6713UnicodeStringELN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt15_Sp_counted_ptrIPN6icu_6713UnicodeStringELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt15_Sp_counted_ptrIPN6icu_6713UnicodeStringELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt15_Sp_counted_ptrIPN6icu_6713UnicodeStringELN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt15_Sp_counted_ptrIPN6icu_6713UnicodeStringELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt15_Sp_counted_ptrIPN6icu_6713UnicodeStringELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN6icu_6713UnicodeStringELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN6icu_6713UnicodeStringELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.type	_ZNSt15_Sp_counted_ptrIPN6icu_6713UnicodeStringELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
_ZNSt15_Sp_counted_ptrIPN6icu_6713UnicodeStringELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
.LFB22765:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L11
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L11:
	ret
	.cfi_endproc
.LFE22765:
	.size	_ZNSt15_Sp_counted_ptrIPN6icu_6713UnicodeStringELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt15_Sp_counted_ptrIPN6icu_6713UnicodeStringELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.section	.text._ZNSt15_Sp_counted_ptrIPN6icu_6713UnicodeStringELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN6icu_6713UnicodeStringELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN6icu_6713UnicodeStringELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.type	_ZNSt15_Sp_counted_ptrIPN6icu_6713UnicodeStringELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, @function
_ZNSt15_Sp_counted_ptrIPN6icu_6713UnicodeStringELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info:
.LFB22767:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE22767:
	.size	_ZNSt15_Sp_counted_ptrIPN6icu_6713UnicodeStringELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, .-_ZNSt15_Sp_counted_ptrIPN6icu_6713UnicodeStringELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.text._ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB22769:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE22769:
	.size	_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"axG",@progbits,_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.type	_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
.LFB22772:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L15
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L15:
	ret
	.cfi_endproc
.LFE22772:
	.size	_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.section	.text._ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,"axG",@progbits,_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.type	_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, @function
_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info:
.LFB22774:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE22774:
	.size	_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, .-_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.text._ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB22773:
	.cfi_startproc
	endbr64
	jmp	_ZdlPv@PLT
	.cfi_endproc
.LFE22773:
	.size	_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED0Ev,"axG",@progbits,_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.type	_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED0Ev, @function
_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED0Ev:
.LFB22771:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE22771:
	.size	_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED0Ev, .-_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.section	.text._ZNSt15_Sp_counted_ptrIPN6icu_6713UnicodeStringELN9__gnu_cxx12_Lock_policyE2EED0Ev,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN6icu_6713UnicodeStringELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN6icu_6713UnicodeStringELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.type	_ZNSt15_Sp_counted_ptrIPN6icu_6713UnicodeStringELN9__gnu_cxx12_Lock_policyE2EED0Ev, @function
_ZNSt15_Sp_counted_ptrIPN6icu_6713UnicodeStringELN9__gnu_cxx12_Lock_policyE2EED0Ev:
.LFB22739:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE22739:
	.size	_ZNSt15_Sp_counted_ptrIPN6icu_6713UnicodeStringELN9__gnu_cxx12_Lock_policyE2EED0Ev, .-_ZNSt15_Sp_counted_ptrIPN6icu_6713UnicodeStringELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.section	.text._ZNSt15_Sp_counted_ptrIPN6icu_6713UnicodeStringELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN6icu_6713UnicodeStringELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN6icu_6713UnicodeStringELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt15_Sp_counted_ptrIPN6icu_6713UnicodeStringELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt15_Sp_counted_ptrIPN6icu_6713UnicodeStringELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB22766:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE22766:
	.size	_ZNSt15_Sp_counted_ptrIPN6icu_6713UnicodeStringELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt15_Sp_counted_ptrIPN6icu_6713UnicodeStringELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZN2v88internal7ManagedIN6icu_6713UnicodeStringEE10DestructorEPv,"axG",@progbits,_ZN2v88internal7ManagedIN6icu_6713UnicodeStringEE10DestructorEPv,comdat
	.p2align 4
	.weak	_ZN2v88internal7ManagedIN6icu_6713UnicodeStringEE10DestructorEPv
	.type	_ZN2v88internal7ManagedIN6icu_6713UnicodeStringEE10DestructorEPv, @function
_ZN2v88internal7ManagedIN6icu_6713UnicodeStringEE10DestructorEPv:
.LFB22172:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L22
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	8(%rdi), %r13
	testq	%r13, %r13
	je	.L25
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L26
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
.L27:
	cmpl	$1, %eax
	je	.L34
.L25:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$16, %esi
	popq	%rbx
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L22:
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L29
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L30:
	cmpl	$1, %eax
	jne	.L25
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L26:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L29:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L30
	.cfi_endproc
.LFE22172:
	.size	_ZN2v88internal7ManagedIN6icu_6713UnicodeStringEE10DestructorEPv, .-_ZN2v88internal7ManagedIN6icu_6713UnicodeStringEE10DestructorEPv
	.section	.text._ZN2v88internal7ManagedIN6icu_6713BreakIteratorEE10DestructorEPv,"axG",@progbits,_ZN2v88internal7ManagedIN6icu_6713BreakIteratorEE10DestructorEPv,comdat
	.p2align 4
	.weak	_ZN2v88internal7ManagedIN6icu_6713BreakIteratorEE10DestructorEPv
	.type	_ZN2v88internal7ManagedIN6icu_6713BreakIteratorEE10DestructorEPv, @function
_ZN2v88internal7ManagedIN6icu_6713BreakIteratorEE10DestructorEPv:
.LFB22163:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L35
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	8(%rdi), %r13
	testq	%r13, %r13
	je	.L38
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L39
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
.L40:
	cmpl	$1, %eax
	je	.L47
.L38:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$16, %esi
	popq	%rbx
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L35:
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L42
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L43:
	cmpl	$1, %eax
	jne	.L38
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L39:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L42:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L43
	.cfi_endproc
.LFE22163:
	.size	_ZN2v88internal7ManagedIN6icu_6713BreakIteratorEE10DestructorEPv, .-_ZN2v88internal7ManagedIN6icu_6713BreakIteratorEE10DestructorEPv
	.section	.rodata._ZN2v88internal17JSV8BreakIterator9AdoptTextEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"(break_iterator) != nullptr"
.LC1:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal17JSV8BreakIterator9AdoptTextEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17JSV8BreakIterator9AdoptTextEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEE
	.type	_ZN2v88internal17JSV8BreakIterator9AdoptTextEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEE, @function
_ZN2v88internal17JSV8BreakIterator9AdoptTextEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEE:
.LFB18237:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	movq	%rdx, %rsi
	movq	(%rbx), %rax
	movq	31(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L61
	call	_ZN2v88internal4Intl22SetTextToBreakIteratorEPNS0_7IsolateENS0_6HandleINS0_6StringEEEPN6icu_6713BreakIteratorE@PLT
	movq	(%rbx), %r13
	movq	(%rax), %r12
	leaq	39(%r13), %r14
	movq	%r12, 39(%r13)
	testb	$1, %r12b
	je	.L48
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L62
	testb	$24, %al
	je	.L48
.L64:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L63
.L48:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore_state
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L64
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L63:
	popq	%rbx
	movq	%r12, %rdx
	movq	%r14, %rsi
	popq	%r12
	movq	%r13, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L61:
	.cfi_restore_state
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18237:
	.size	_ZN2v88internal17JSV8BreakIterator9AdoptTextEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEE, .-_ZN2v88internal17JSV8BreakIterator9AdoptTextEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEE
	.section	.rodata._ZNK2v88internal17JSV8BreakIterator12TypeAsStringEv.str1.1,"aMS",@progbits,1
.LC2:
	.string	"unreachable code"
	.section	.text._ZNK2v88internal17JSV8BreakIterator12TypeAsStringEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal17JSV8BreakIterator12TypeAsStringEv
	.type	_ZNK2v88internal17JSV8BreakIterator12TypeAsStringEv, @function
_ZNK2v88internal17JSV8BreakIterator12TypeAsStringEv:
.LFB18238:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movslq	91(%rdx), %rax
	cmpq	$2, %rax
	je	.L66
	cmpl	$2, %eax
	jg	.L67
	testl	%eax, %eax
	je	.L68
	cmpl	$1, %eax
	jne	.L70
	andq	$-262144, %rdx
	movq	24(%rdx), %rax
	subq	$34000, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	cmpl	$3, %eax
	jne	.L70
	andq	$-262144, %rdx
	movq	24(%rdx), %rax
	subq	$34808, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	andq	$-262144, %rdx
	movq	24(%rdx), %rax
	subq	$34336, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	andq	$-262144, %rdx
	movq	24(%rdx), %rax
	subq	$35352, %rax
	ret
.L70:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18238:
	.size	_ZNK2v88internal17JSV8BreakIterator12TypeAsStringEv, .-_ZNK2v88internal17JSV8BreakIterator12TypeAsStringEv
	.section	.text._ZN2v88internal17JSV8BreakIterator15ResolvedOptionsEPNS0_7IsolateENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17JSV8BreakIterator15ResolvedOptionsEPNS0_7IsolateENS0_6HandleIS1_EE
	.type	_ZN2v88internal17JSV8BreakIterator15ResolvedOptionsEPNS0_7IsolateENS0_6HandleIS1_EE, @function
_ZN2v88internal17JSV8BreakIterator15ResolvedOptionsEPNS0_7IsolateENS0_6HandleIS1_EE:
.LFB18236:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	12464(%rdi), %rax
	movq	39(%rax), %rax
	movq	879(%rax), %r13
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L76
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L77:
	movq	%r12, %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movq	41112(%r12), %rdi
	movq	%rax, %r13
	movq	(%rbx), %rax
	movq	23(%rax), %rsi
	testq	%rdi, %rdi
	je	.L79
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L80:
	xorl	%r8d, %r8d
	leaq	1592(%r12), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	(%rbx), %rax
	leaq	-48(%rbp), %rdi
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal17JSV8BreakIterator12TypeAsStringEv
	xorl	%r8d, %r8d
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	leaq	1912(%r12), %rdx
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L84
	addq	$40, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L85
.L78:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, (%rsi)
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L79:
	movq	41088(%r12), %rcx
	cmpq	41096(%r12), %rcx
	je	.L86
.L81:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rcx)
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L85:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L86:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L81
.L84:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18236:
	.size	_ZN2v88internal17JSV8BreakIterator15ResolvedOptionsEPNS0_7IsolateENS0_6HandleIS1_EE, .-_ZN2v88internal17JSV8BreakIterator15ResolvedOptionsEPNS0_7IsolateENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal17JSV8BreakIterator7CurrentEPNS0_7IsolateENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17JSV8BreakIterator7CurrentEPNS0_7IsolateENS0_6HandleIS1_EE
	.type	_ZN2v88internal17JSV8BreakIterator7CurrentEPNS0_7IsolateENS0_6HandleIS1_EE, @function
_ZN2v88internal17JSV8BreakIterator7CurrentEPNS0_7IsolateENS0_6HandleIS1_EE:
.LFB18239:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rsi), %rax
	movq	31(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rdi
	movq	(%rdi), %rax
	call	*112(%rax)
	addq	$8, %rsp
	movq	%r12, %rdi
	xorl	%edx, %edx
	popq	%r12
	movl	%eax, %esi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal7Factory16NewNumberFromIntEiNS0_14AllocationTypeE@PLT
	.cfi_endproc
.LFE18239:
	.size	_ZN2v88internal17JSV8BreakIterator7CurrentEPNS0_7IsolateENS0_6HandleIS1_EE, .-_ZN2v88internal17JSV8BreakIterator7CurrentEPNS0_7IsolateENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal17JSV8BreakIterator5FirstEPNS0_7IsolateENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17JSV8BreakIterator5FirstEPNS0_7IsolateENS0_6HandleIS1_EE
	.type	_ZN2v88internal17JSV8BreakIterator5FirstEPNS0_7IsolateENS0_6HandleIS1_EE, @function
_ZN2v88internal17JSV8BreakIterator5FirstEPNS0_7IsolateENS0_6HandleIS1_EE:
.LFB18240:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rsi), %rax
	movq	31(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rdi
	movq	(%rdi), %rax
	call	*80(%rax)
	addq	$8, %rsp
	movq	%r12, %rdi
	xorl	%edx, %edx
	popq	%r12
	movl	%eax, %esi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal7Factory16NewNumberFromIntEiNS0_14AllocationTypeE@PLT
	.cfi_endproc
.LFE18240:
	.size	_ZN2v88internal17JSV8BreakIterator5FirstEPNS0_7IsolateENS0_6HandleIS1_EE, .-_ZN2v88internal17JSV8BreakIterator5FirstEPNS0_7IsolateENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal17JSV8BreakIterator4NextEPNS0_7IsolateENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17JSV8BreakIterator4NextEPNS0_7IsolateENS0_6HandleIS1_EE
	.type	_ZN2v88internal17JSV8BreakIterator4NextEPNS0_7IsolateENS0_6HandleIS1_EE, @function
_ZN2v88internal17JSV8BreakIterator4NextEPNS0_7IsolateENS0_6HandleIS1_EE:
.LFB18241:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rsi), %rax
	movq	31(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rdi
	movq	(%rdi), %rax
	call	*104(%rax)
	addq	$8, %rsp
	movq	%r12, %rdi
	xorl	%edx, %edx
	popq	%r12
	movl	%eax, %esi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal7Factory16NewNumberFromIntEiNS0_14AllocationTypeE@PLT
	.cfi_endproc
.LFE18241:
	.size	_ZN2v88internal17JSV8BreakIterator4NextEPNS0_7IsolateENS0_6HandleIS1_EE, .-_ZN2v88internal17JSV8BreakIterator4NextEPNS0_7IsolateENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal17JSV8BreakIterator9BreakTypeEPNS0_7IsolateENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17JSV8BreakIterator9BreakTypeEPNS0_7IsolateENS0_6HandleIS1_EE
	.type	_ZN2v88internal17JSV8BreakIterator9BreakTypeEPNS0_7IsolateENS0_6HandleIS1_EE, @function
_ZN2v88internal17JSV8BreakIterator9BreakTypeEPNS0_7IsolateENS0_6HandleIS1_EE:
.LFB18242:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rsi), %rax
	movq	31(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rdi
	movq	(%rdi), %rax
	call	*152(%rax)
	cmpl	$99, %eax
	ja	.L94
	movq	1704(%rbx), %rax
.L95:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	.cfi_restore_state
	leal	-100(%rax), %edx
	cmpl	$99, %edx
	ja	.L96
	movq	2984(%rbx), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L96:
	.cfi_restore_state
	leal	-200(%rax), %edx
	cmpl	$99, %edx
	jbe	.L101
	leal	-300(%rax), %edx
	cmpl	$99, %edx
	ja	.L98
	movq	1552(%rbx), %rax
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L101:
	movq	1568(%rbx), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	.cfi_restore_state
	subl	$400, %eax
	cmpl	$99, %eax
	ja	.L99
	movq	1520(%rbx), %rax
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L99:
	movq	1920(%rbx), %rax
	jmp	.L95
	.cfi_endproc
.LFE18242:
	.size	_ZN2v88internal17JSV8BreakIterator9BreakTypeEPNS0_7IsolateENS0_6HandleIS1_EE, .-_ZN2v88internal17JSV8BreakIterator9BreakTypeEPNS0_7IsolateENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal17JSV8BreakIterator19GetAvailableLocalesB5cxx11Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17JSV8BreakIterator19GetAvailableLocalesB5cxx11Ev
	.type	_ZN2v88internal17JSV8BreakIterator19GetAvailableLocalesB5cxx11Ev, @function
_ZN2v88internal17JSV8BreakIterator19GetAvailableLocalesB5cxx11Ev:
.LFB18243:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$56, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movzbl	_ZZN2v88internal17JSV8BreakIterator19GetAvailableLocalesB5cxx11EvE17available_locales(%rip), %eax
	cmpb	$2, %al
	jne	.L113
.L103:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L114
	addq	$56, %rsp
	leaq	16+_ZZN2v88internal17JSV8BreakIterator19GetAvailableLocalesB5cxx11EvE17available_locales(%rip), %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L113:
	.cfi_restore_state
	leaq	_ZN2v84base16LazyInstanceImplINS_8internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS3_17SkipResourceCheckEEENS0_32StaticallyAllocatedInstanceTraitIS8_EENS0_21DefaultConstructTraitIS8_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS8_EEE12InitInstanceEPv(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rcx
	movq	%rax, -64(%rbp)
	movq	%rcx, %xmm0
	leaq	8+_ZZN2v88internal17JSV8BreakIterator19GetAvailableLocalesB5cxx11EvE17available_locales(%rip), %rax
	leaq	-64(%rbp), %r12
	movq	%rax, -56(%rbp)
	leaq	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%r12, %rsi
	leaq	_ZZN2v88internal17JSV8BreakIterator19GetAvailableLocalesB5cxx11EvE17available_locales(%rip), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-48(%rbp), %rax
	testq	%rax, %rax
	je	.L103
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	*%rax
	jmp	.L103
.L114:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18243:
	.size	_ZN2v88internal17JSV8BreakIterator19GetAvailableLocalesB5cxx11Ev, .-_ZN2v88internal17JSV8BreakIterator19GetAvailableLocalesB5cxx11Ev
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E:
.LFB21389:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L130
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
.L119:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L117
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L115
.L118:
	movq	%rbx, %r12
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L117:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L118
.L115:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L130:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE21389:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	.section	.text._ZN2v84base16LazyInstanceImplINS_8internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS3_17SkipResourceCheckEEENS0_32StaticallyAllocatedInstanceTraitIS8_EENS0_21DefaultConstructTraitIS8_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS8_EEE12InitInstanceEPv,"axG",@progbits,_ZN2v84base16LazyInstanceImplINS_8internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS3_17SkipResourceCheckEEENS0_32StaticallyAllocatedInstanceTraitIS8_EENS0_21DefaultConstructTraitIS8_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS8_EEE12InitInstanceEPv,comdat
	.p2align 4
	.weak	_ZN2v84base16LazyInstanceImplINS_8internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS3_17SkipResourceCheckEEENS0_32StaticallyAllocatedInstanceTraitIS8_EENS0_21DefaultConstructTraitIS8_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS8_EEE12InitInstanceEPv
	.type	_ZN2v84base16LazyInstanceImplINS_8internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS3_17SkipResourceCheckEEENS0_32StaticallyAllocatedInstanceTraitIS8_EENS0_21DefaultConstructTraitIS8_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS8_EEE12InitInstanceEPv, @function
_ZN2v84base16LazyInstanceImplINS_8internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS3_17SkipResourceCheckEEENS0_32StaticallyAllocatedInstanceTraitIS8_EENS0_21DefaultConstructTraitIS8_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS8_EEE12InitInstanceEPv:
.LFB22183:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	16(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN2v88internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS1_17SkipResourceCheckEEE(%rip), %rax
	movl	$0, 16(%rdi)
	movq	%rax, (%rdi)
	movq	$0, 24(%rdi)
	movq	%r13, 32(%rdi)
	movq	%r13, 40(%rdi)
	movq	$0, 48(%rdi)
	leaq	-116(%rbp), %rdi
	movl	$0, -116(%rbp)
	call	_ZN6icu_6713BreakIterator19getAvailableLocalesERi@PLT
	movl	-116(%rbp), %edx
	leaq	-112(%rbp), %rdi
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	xorl	%ecx, %ecx
	call	_ZN2v88internal4Intl14BuildLocaleSetB5cxx11EPKN6icu_676LocaleEiPKcS7_@PLT
	movq	24(%rbx), %r12
	testq	%r12, %r12
	je	.L134
	leaq	8(%rbx), %r15
.L137:
	movq	24(%r12), %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %r14
	cmpq	%rax, %rdi
	je	.L135
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r14, %r14
	je	.L134
.L136:
	movq	%r14, %r12
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L135:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r14, %r14
	jne	.L136
.L134:
	movq	-96(%rbp), %rax
	movq	$0, 24(%rbx)
	movq	%r13, 32(%rbx)
	movq	%r13, 40(%rbx)
	movq	$0, 48(%rbx)
	testq	%rax, %rax
	je	.L133
	movl	-104(%rbp), %edx
	movq	%rax, 24(%rbx)
	movl	%edx, 16(%rbx)
	movq	-88(%rbp), %rdx
	movq	%rdx, 32(%rbx)
	movq	-80(%rbp), %rdx
	movq	%rdx, 40(%rbx)
	movq	%r13, 8(%rax)
	movq	-72(%rbp), %rax
	movq	%rax, 48(%rbx)
.L133:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L153
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L153:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22183:
	.size	_ZN2v84base16LazyInstanceImplINS_8internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS3_17SkipResourceCheckEEENS0_32StaticallyAllocatedInstanceTraitIS8_EENS0_21DefaultConstructTraitIS8_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS8_EEE12InitInstanceEPv, .-_ZN2v84base16LazyInstanceImplINS_8internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS3_17SkipResourceCheckEEENS0_32StaticallyAllocatedInstanceTraitIS8_EENS0_21DefaultConstructTraitIS8_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS8_EEE12InitInstanceEPv
	.section	.text._ZN2v88internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS1_17SkipResourceCheckEED2Ev,"axG",@progbits,_ZN2v88internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS1_17SkipResourceCheckEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS1_17SkipResourceCheckEED2Ev
	.type	_ZN2v88internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS1_17SkipResourceCheckEED2Ev, @function
_ZN2v88internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS1_17SkipResourceCheckEED2Ev:
.LFB22776:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS1_17SkipResourceCheckEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	24(%rdi), %r12
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L154
	leaq	8(%rdi), %r13
.L158:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L156
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L154
.L157:
	movq	%rbx, %r12
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L156:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L157
.L154:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22776:
	.size	_ZN2v88internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS1_17SkipResourceCheckEED2Ev, .-_ZN2v88internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS1_17SkipResourceCheckEED2Ev
	.weak	_ZN2v88internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS1_17SkipResourceCheckEED1Ev
	.set	_ZN2v88internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS1_17SkipResourceCheckEED1Ev,_ZN2v88internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS1_17SkipResourceCheckEED2Ev
	.section	.text._ZN2v88internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS1_17SkipResourceCheckEED0Ev,"axG",@progbits,_ZN2v88internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS1_17SkipResourceCheckEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS1_17SkipResourceCheckEED0Ev
	.type	_ZN2v88internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS1_17SkipResourceCheckEED0Ev, @function
_ZN2v88internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS1_17SkipResourceCheckEED0Ev:
.LFB22778:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS1_17SkipResourceCheckEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	24(%rdi), %r12
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L170
	leaq	8(%rdi), %r14
.L173:
	movq	24(%r12), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L171
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L170
.L172:
	movq	%rbx, %r12
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L171:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L172
.L170:
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	movl	$56, %esi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE22778:
	.size	_ZN2v88internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS1_17SkipResourceCheckEED0Ev, .-_ZN2v88internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS1_17SkipResourceCheckEED0Ev
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E:
.LFB21391:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L200
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
.L189:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	movq	64(%r12), %rdi
	leaq	80(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L186
	call	_ZdlPv@PLT
.L186:
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	cmpq	%rax, %rdi
	je	.L187
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L184
.L188:
	movq	%rbx, %r12
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L187:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L188
.L184:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L200:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE21391:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	.section	.text._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2IN6icu_6713BreakIteratorESt14default_deleteIS5_EEEOSt10unique_ptrIT_T0_E,"axG",@progbits,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC5IN6icu_6713BreakIteratorESt14default_deleteIS5_EEEOSt10unique_ptrIT_T0_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2IN6icu_6713BreakIteratorESt14default_deleteIS5_EEEOSt10unique_ptrIT_T0_E
	.type	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2IN6icu_6713BreakIteratorESt14default_deleteIS5_EEEOSt10unique_ptrIT_T0_E, @function
_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2IN6icu_6713BreakIteratorESt14default_deleteIS5_EEEOSt10unique_ptrIT_T0_E:
.LFB22404:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rsi)
	movq	$0, (%rdi)
	je	.L206
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	$24, %edi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_Znwm@PLT
	movq	(%rbx), %rdx
	movq	$0, (%rbx)
	movabsq	$4294967297, %rcx
	movq	%rcx, 8(%rax)
	leaq	16+_ZTVSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	%rdx, 16(%rax)
	movq	%rax, (%r12)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L206:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE22404:
	.size	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2IN6icu_6713BreakIteratorESt14default_deleteIS5_EEEOSt10unique_ptrIT_T0_E, .-_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2IN6icu_6713BreakIteratorESt14default_deleteIS5_EEEOSt10unique_ptrIT_T0_E
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1IN6icu_6713BreakIteratorESt14default_deleteIS5_EEEOSt10unique_ptrIT_T0_E
	.set	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1IN6icu_6713BreakIteratorESt14default_deleteIS5_EEEOSt10unique_ptrIT_T0_E,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2IN6icu_6713BreakIteratorESt14default_deleteIS5_EEEOSt10unique_ptrIT_T0_E
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal17JSV8BreakIterator3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6ObjectEEES8_,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal17JSV8BreakIterator3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6ObjectEEES8_, @function
_GLOBAL__sub_I__ZN2v88internal17JSV8BreakIterator3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6ObjectEEES8_:
.LFB22795:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE22795:
	.size	_GLOBAL__sub_I__ZN2v88internal17JSV8BreakIterator3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6ObjectEEES8_, .-_GLOBAL__sub_I__ZN2v88internal17JSV8BreakIterator3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6ObjectEEES8_
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal17JSV8BreakIterator3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6ObjectEEES8_
	.section	.rodata._ZN2v88internal17JSV8BreakIterator3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6ObjectEEES8_.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"basic_string::_M_construct null not valid"
	.section	.rodata._ZN2v88internal17JSV8BreakIterator3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6ObjectEEES8_.str1.1,"aMS",@progbits,1
.LC4:
	.string	"Intl.JSV8BreakIterator"
.LC5:
	.string	"word"
.LC6:
	.string	"character"
.LC7:
	.string	"sentence"
.LC8:
	.string	"line"
.LC9:
	.string	"Intl.v8BreakIterator"
.LC10:
	.string	"type"
	.section	.rodata._ZN2v88internal17JSV8BreakIterator3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6ObjectEEES8_.str1.8
	.align 8
.LC11:
	.string	"Failed to create ICU break iterator, are ICU data files missing?"
	.align 8
.LC12:
	.string	"(break_iterator.get()) != nullptr"
	.section	.rodata._ZN2v88internal17JSV8BreakIterator3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6ObjectEEES8_.str1.1
.LC13:
	.string	"(location_) != nullptr"
	.section	.text._ZN2v88internal17JSV8BreakIterator3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6ObjectEEES8_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17JSV8BreakIterator3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6ObjectEEES8_
	.type	_ZN2v88internal17JSV8BreakIterator3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6ObjectEEES8_, @function
_ZN2v88internal17JSV8BreakIterator3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6ObjectEEES8_:
.LFB18181:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	-672(%rbp), %rdi
	pushq	%rbx
	subq	$744, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -744(%rbp)
	movq	%r12, %rsi
	movq	%rcx, -728(%rbp)
	xorl	%ecx, %ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal4Intl22CanonicalizeLocaleListB5cxx11EPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEb@PLT
	cmpb	$0, -672(%rbp)
	je	.L213
	movq	-656(%rbp), %rbx
	pxor	%xmm0, %xmm0
	movq	-664(%rbp), %rcx
	movq	$0, -688(%rbp)
	movaps	%xmm0, -704(%rbp)
	movq	%rbx, %r13
	subq	%rcx, %r13
	movq	%r13, %rax
	sarq	$5, %rax
	je	.L398
	movabsq	$288230376151711743, %rdx
	cmpq	%rdx, %rax
	ja	.L399
	movq	%r13, %rdi
	call	_Znwm@PLT
	movq	-656(%rbp), %rbx
	movq	-664(%rbp), %rcx
	movq	%rax, %r15
.L215:
	movq	%r15, %xmm0
	addq	%r15, %r13
	punpcklqdq	%xmm0, %xmm0
	movq	%r13, -688(%rbp)
	movaps	%xmm0, -704(%rbp)
	cmpq	%rbx, %rcx
	je	.L217
	leaq	-640(%rbp), %r13
	movq	%r12, -752(%rbp)
	movq	%rcx, %r12
	movq	%rbx, -736(%rbp)
	movq	%r13, %r14
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L219:
	cmpq	$1, %r13
	jne	.L221
	movzbl	(%rbx), %eax
	movb	%al, 16(%r15)
.L222:
	movq	%r13, 8(%r15)
	addq	$32, %r12
	addq	$32, %r15
	movb	$0, (%rdi,%r13)
	cmpq	%r12, -736(%rbp)
	je	.L400
.L223:
	leaq	16(%r15), %rdi
	movq	%rdi, (%r15)
	movq	(%r12), %rbx
	movq	8(%r12), %r13
	movq	%rbx, %rax
	addq	%r13, %rax
	je	.L218
	testq	%rbx, %rbx
	je	.L401
.L218:
	movq	%r13, -640(%rbp)
	cmpq	$15, %r13
	jbe	.L219
	movq	%r15, %rdi
	xorl	%edx, %edx
	movq	%r14, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r15)
	movq	%rax, %rdi
	movq	-640(%rbp), %rax
	movq	%rax, 16(%r15)
.L220:
	movq	%r13, %rdx
	movq	%rbx, %rsi
	call	memcpy@PLT
	movq	-640(%rbp), %r13
	movq	(%r15), %rdi
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L227:
	xorl	%r13d, %r13d
.L230:
	movq	-696(%rbp), %rbx
	movq	-704(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L299
	.p2align 4,,10
	.p2align 3
.L303:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L300
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L303
.L301:
	movq	-704(%rbp), %r12
.L299:
	testq	%r12, %r12
	je	.L213
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L213:
	movq	-656(%rbp), %rbx
	movq	-664(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L305
	.p2align 4,,10
	.p2align 3
.L309:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L306
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L309
.L307:
	movq	-664(%rbp), %r12
.L305:
	testq	%r12, %r12
	je	.L310
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L310:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L402
	addq	$744, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L306:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L309
	jmp	.L307
	.p2align 4,,10
	.p2align 3
.L221:
	testq	%r13, %r13
	je	.L222
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L400:
	movq	-752(%rbp), %r12
.L217:
	movq	-728(%rbp), %r13
	movq	%r15, -696(%rbp)
	movq	0(%r13), %rax
	cmpq	88(%r12), %rax
	je	.L403
	testb	$1, %al
	jne	.L226
.L229:
	movq	-728(%rbp), %rsi
	leaq	.LC4(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal6Object12ToObjectImplEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L227
.L225:
	leaq	.LC4(%rip), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4Intl16GetLocaleMatcherEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEEPKc@PLT
	testb	%al, %al
	je	.L227
	sarq	$32, %rax
	movl	$0, -632(%rbp)
	leaq	-368(%rbp), %r15
	movq	$0, -624(%rbp)
	movq	%rax, %rbx
	leaq	-632(%rbp), %rax
	movq	%rax, -616(%rbp)
	movq	%rax, -608(%rbp)
	movq	$0, -600(%rbp)
	movzbl	_ZZN2v88internal17JSV8BreakIterator19GetAvailableLocalesB5cxx11EvE17available_locales(%rip), %eax
	cmpb	$2, %al
	jne	.L404
.L231:
	leaq	-640(%rbp), %r14
	movq	%r15, %rdi
	movl	%ebx, %r8d
	movq	%r12, %rsi
	leaq	-704(%rbp), %rcx
	movq	%r14, %r9
	leaq	16+_ZZN2v88internal17JSV8BreakIterator19GetAvailableLocalesB5cxx11EvE17available_locales(%rip), %rdx
	call	_ZN2v88internal4Intl13ResolveLocaleEPNS0_7IsolateERKSt3setINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessISA_ESaISA_EERKSt6vectorISA_SD_ENS1_13MatcherOptionESG_@PLT
	movq	-624(%rbp), %r15
	testq	%r15, %r15
	je	.L237
.L233:
	movq	24(%r15), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r15), %rdi
	leaq	48(%r15), %rax
	movq	16(%r15), %rbx
	cmpq	%rax, %rdi
	je	.L236
	call	_ZdlPv@PLT
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L237
.L238:
	movq	%rbx, %r15
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L236:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L238
.L237:
	movl	$16, %edi
	call	_Znwm@PLT
	leaq	.LC5(%rip), %rcx
	movl	$32, %edi
	movabsq	$12884901890, %rdx
	movq	%rax, %r15
	movl	$1, %eax
	movq	%rcx, %xmm0
	movq	%rax, (%r15)
	leaq	.LC6(%rip), %rax
	leaq	.LC7(%rip), %rcx
	movq	%rax, %xmm1
	movq	%rdx, 8(%r15)
	leaq	.LC8(%rip), %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, %xmm2
	movaps	%xmm0, -592(%rbp)
	movq	%rcx, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -576(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movdqa	-592(%rbp), %xmm3
	movdqa	-576(%rbp), %xmm4
	movq	%rax, %rbx
	movaps	%xmm0, -640(%rbp)
	movups	%xmm3, (%rax)
	movups	%xmm4, 16(%rax)
	movq	$0, -712(%rbp)
	movq	$0, -624(%rbp)
	call	_Znwm@PLT
	movdqu	(%rbx), %xmm5
	movq	%r13, %rsi
	movq	%r12, %rdi
	movdqu	16(%rbx), %xmm6
	leaq	32(%rax), %rdx
	movq	%r14, %rcx
	movq	%rax, -640(%rbp)
	movups	%xmm5, (%rax)
	leaq	.LC9(%rip), %r8
	movups	%xmm6, 16(%rax)
	leaq	-712(%rbp), %rax
	movq	%rdx, -624(%rbp)
	movq	%rax, %r9
	movq	%rdx, -632(%rbp)
	leaq	.LC10(%rip), %rdx
	movq	%rax, -728(%rbp)
	call	_ZN2v88internal4Intl15GetStringOptionEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEEPKcSt6vectorIS8_SaIS8_EES8_PSt10unique_ptrIA_cSt14default_deleteISD_EE@PLT
	movq	-640(%rbp), %rdi
	movl	%eax, %r13d
	testq	%rdi, %rdi
	je	.L235
	movb	%al, -736(%rbp)
	call	_ZdlPv@PLT
	movzbl	-736(%rbp), %eax
.L235:
	movq	-712(%rbp), %rdi
	testb	%al, %al
	jne	.L405
	testq	%rdi, %rdi
	jne	.L247
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L316:
	xorl	%r13d, %r13d
	leaq	-336(%rbp), %r15
.L248:
	movq	-96(%rbp), %r12
	leaq	-112(%rbp), %r14
	testq	%r12, %r12
	je	.L297
.L292:
	movq	24(%r12), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	movq	64(%r12), %rdi
	leaq	80(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L295
	call	_ZdlPv@PLT
.L295:
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	cmpq	%rax, %rdi
	je	.L296
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L297
.L298:
	movq	%rbx, %r12
	jmp	.L292
	.p2align 4,,10
	.p2align 3
.L300:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L303
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L398:
	xorl	%r15d, %r15d
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L405:
	shrw	$8, %r13w
	je	.L240
	movq	(%rbx), %rsi
	movq	%rdi, -736(%rbp)
	call	strcmp@PLT
	movq	-736(%rbp), %rdi
	testl	%eax, %eax
	je	.L318
	movq	8(%rbx), %rsi
	call	strcmp@PLT
	movq	-736(%rbp), %rdi
	testl	%eax, %eax
	je	.L319
	movq	16(%rbx), %rsi
	call	strcmp@PLT
	movq	-736(%rbp), %rdi
	testl	%eax, %eax
	je	.L320
	movq	24(%rbx), %rsi
	call	strcmp@PLT
	movq	-736(%rbp), %rdi
	testl	%eax, %eax
	je	.L406
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L404:
	leaq	_ZN2v84base16LazyInstanceImplINS_8internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS3_17SkipResourceCheckEEENS0_32StaticallyAllocatedInstanceTraitIS8_EENS0_21DefaultConstructTraitIS8_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS8_EEE12InitInstanceEPv(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rcx
	movq	%r15, %rsi
	movq	%rax, -368(%rbp)
	leaq	8+_ZZN2v88internal17JSV8BreakIterator19GetAvailableLocalesB5cxx11EvE17available_locales(%rip), %rax
	movq	%rcx, %xmm0
	leaq	_ZZN2v88internal17JSV8BreakIterator19GetAvailableLocalesB5cxx11EvE17available_locales(%rip), %rdi
	movq	%rax, -360(%rbp)
	leaq	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%rax, %xmm7
	punpcklqdq	%xmm7, %xmm0
	movaps	%xmm0, -352(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-352(%rbp), %rax
	testq	%rax, %rax
	je	.L231
	movl	$3, %edx
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	*%rax
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L296:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L298
.L297:
	movq	%r15, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movq	-368(%rbp), %rdi
	leaq	-352(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L230
	call	_ZdlPv@PLT
	jmp	.L230
	.p2align 4,,10
	.p2align 3
.L403:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory24NewJSObjectWithNullProtoENS0_14AllocationTypeE@PLT
	movq	%rax, %r13
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L226:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L229
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L240:
	testq	%rdi, %rdi
	jne	.L245
.L397:
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
	movq	%r15, %rdi
	leaq	-336(%rbp), %r15
	call	_ZdlPv@PLT
	leaq	-592(%rbp), %rax
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -752(%rbp)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movl	$0, -716(%rbp)
	movl	$1, -736(%rbp)
.L246:
	movq	-752(%rbp), %rdi
	leaq	-716(%rbp), %rsi
	call	_ZN6icu_6713BreakIterator18createWordInstanceERKNS_6LocaleER10UErrorCode@PLT
	movq	%rax, %r13
.L250:
	movl	-716(%rbp), %eax
	testl	%eax, %eax
	jg	.L407
	testq	%r13, %r13
	je	.L408
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate10CountUsageENS_7Isolate17UseCounterFeatureE@PLT
	movq	-728(%rbp), %rsi
	movq	%r14, %rdi
	movq	%r13, -712(%rbp)
	call	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1IN6icu_6713BreakIteratorESt14default_deleteIS5_EEEOSt10unique_ptrIT_T0_E
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rax
	movq	-640(%rbp), %r8
	movq	%rax, -728(%rbp)
	testq	%r8, %r8
	je	.L253
	testq	%rax, %rax
	je	.L254
	lock addl	$1, 8(%r8)
	movq	-640(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L253
	movl	$-1, %eax
	lock xaddl	%eax, 8(%rbx)
.L257:
	cmpl	$1, %eax
	jne	.L253
	movq	(%rbx), %rax
	movq	%r8, -768(%rbp)
	movq	%rbx, %rdi
	call	*16(%rax)
	cmpq	$0, -728(%rbp)
	movq	-768(%rbp), %r8
	je	.L258
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rbx)
.L259:
	cmpl	$1, %eax
	jne	.L253
	movq	(%rbx), %rax
	movq	%r8, -768(%rbp)
	movq	%rbx, %rdi
	call	*24(%rax)
	movq	-768(%rbp), %r8
	.p2align 4,,10
	.p2align 3
.L253:
	movq	32(%r12), %rax
	subq	48(%r12), %rax
	cmpq	$33554432, %rax
	jg	.L409
.L260:
	movq	%r8, %xmm7
	movq	%r13, %xmm0
	movl	$16, %edi
	movq	%r8, -776(%rbp)
	punpcklqdq	%xmm7, %xmm0
	movaps	%xmm0, -768(%rbp)
	call	_Znwm@PLT
	movdqa	-768(%rbp), %xmm0
	movq	-776(%rbp), %r8
	movq	%rax, %r13
	movups	%xmm0, (%rax)
	testq	%r8, %r8
	je	.L261
	cmpq	$0, -728(%rbp)
	je	.L262
	lock addl	$1, 8(%r8)
.L261:
	movl	$48, %edi
	movq	%r8, -768(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rbx
	movups	%xmm0, 8(%rax)
	movq	$0, (%rax)
	movq	%rbx, %rsi
	movq	%r13, 24(%rax)
	leaq	_ZN2v88internal7ManagedIN6icu_6713BreakIteratorEE10DestructorEPv(%rip), %rax
	movq	%rax, 32(%rbx)
	movq	$0, 40(%rbx)
	call	_ZN2v88internal7Factory10NewForeignEmNS0_14AllocationTypeE@PLT
	movq	41152(%r12), %rdi
	movq	(%rax), %rsi
	movq	%rax, -776(%rbp)
	call	_ZN2v88internal13GlobalHandles6CreateENS0_6ObjectE@PLT
	movq	_ZN2v88internal22ManagedObjectFinalizerERKNS_16WeakCallbackInfoIvEE@GOTPCREL(%rip), %rdx
	xorl	%ecx, %ecx
	movq	%rbx, %rsi
	movq	%rax, 40(%rbx)
	movq	%rax, %rdi
	call	_ZN2v88internal13GlobalHandles8MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate28RegisterManagedPtrDestructorEPNS0_20ManagedPtrDestructorE@PLT
	movq	-768(%rbp), %r8
	testq	%r8, %r8
	je	.L264
	cmpq	$0, -728(%rbp)
	je	.L265
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r8)
.L266:
	cmpl	$1, %eax
	jne	.L264
	movq	(%r8), %rax
	movq	%r8, -768(%rbp)
	movq	%r8, %rdi
	call	*16(%rax)
	cmpq	$0, -728(%rbp)
	movq	-768(%rbp), %r8
	je	.L268
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r8)
.L269:
	cmpl	$1, %eax
	jne	.L264
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L264:
	movq	-712(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L270
	movq	(%rdi), %rax
	call	*8(%rax)
.L270:
	movl	$24, %edi
	call	_Znwm@PLT
	movq	%rax, %r13
	leaq	16+_ZTVSt15_Sp_counted_ptrIPN6icu_6713UnicodeStringELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rax
	movq	%rax, 0(%r13)
	movabsq	$4294967297, %rax
	movq	%rax, 8(%r13)
	movq	32(%r12), %rax
	subq	48(%r12), %rax
	movq	$0, 16(%r13)
	cmpq	$33554432, %rax
	jg	.L410
.L271:
	movl	$16, %edi
	call	_Znwm@PLT
	cmpq	$0, -728(%rbp)
	movq	$0, (%rax)
	movq	%rax, %rbx
	movq	%r13, 8(%rax)
	leaq	8(%r13), %rax
	movq	%rax, -768(%rbp)
	je	.L411
	leaq	8(%r13), %rax
	lock addl	$1, (%rax)
.L272:
	movl	$48, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %r8
	movups	%xmm0, 8(%rax)
	movq	%rbx, 24(%rax)
	movq	%r8, %rsi
	movq	$0, (%rax)
	leaq	_ZN2v88internal7ManagedIN6icu_6713UnicodeStringEE10DestructorEPv(%rip), %rax
	movq	%rax, 32(%r8)
	movq	$0, 40(%r8)
	movq	%r8, -768(%rbp)
	call	_ZN2v88internal7Factory10NewForeignEmNS0_14AllocationTypeE@PLT
	movq	41152(%r12), %rdi
	movq	(%rax), %rsi
	movq	%rax, %rbx
	call	_ZN2v88internal13GlobalHandles6CreateENS0_6ObjectE@PLT
	movq	-768(%rbp), %r8
	movq	_ZN2v88internal22ManagedObjectFinalizerERKNS_16WeakCallbackInfoIvEE@GOTPCREL(%rip), %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	movq	%rax, 40(%r8)
	movq	%r8, %rsi
	call	_ZN2v88internal13GlobalHandles8MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	movq	-768(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal7Isolate28RegisterManagedPtrDestructorEPNS0_20ManagedPtrDestructorE@PLT
	cmpq	$0, -728(%rbp)
	je	.L273
	leaq	8(%r13), %rcx
	movl	$-1, %eax
	lock xaddl	%eax, (%rcx)
.L274:
	cmpl	$1, %eax
	je	.L412
.L276:
	movq	-368(%rbp), %r13
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%r14, %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r13, -640(%rbp)
	movq	%rax, -632(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L413
	movq	-744(%rbp), %rsi
	movq	(%rsi), %rax
	movl	15(%rax), %eax
	testl	$2097152, %eax
	je	.L281
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$2, %edx
	call	_ZN2v88internal7Factory22NewSlowJSObjectFromMapENS0_6HandleINS0_3MapEEEiNS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE@PLT
	movq	%rax, %r12
.L282:
	movq	(%r12), %r13
	movq	(%r14), %r14
	movq	%r14, 23(%r13)
	leaq	23(%r13), %rsi
	testb	$1, %r14b
	je	.L313
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -728(%rbp)
	testl	$262144, %eax
	je	.L284
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rsi, -744(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-728(%rbp), %rcx
	movq	-744(%rbp), %rsi
	movq	8(%rcx), %rax
.L284:
	testb	$24, %al
	je	.L313
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L313
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L313:
	movq	-736(%rbp), %rdx
	movq	(%r12), %rax
	salq	$32, %rdx
	movq	%rdx, 87(%rax)
	movq	-776(%rbp), %rax
	movq	(%r12), %r14
	movq	(%rax), %r13
	leaq	31(%r14), %rsi
	movq	%r13, 31(%r14)
	testb	$1, %r13b
	je	.L312
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -728(%rbp)
	testl	$262144, %eax
	je	.L287
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rsi, -736(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-728(%rbp), %rcx
	movq	-736(%rbp), %rsi
	movq	8(%rcx), %rax
.L287:
	testb	$24, %al
	je	.L312
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L312
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L312:
	movq	(%r12), %r14
	movq	(%rbx), %r13
	movq	%r13, 39(%r14)
	leaq	39(%r14), %rsi
	testb	$1, %r13b
	je	.L311
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L290
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rsi, -728(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-728(%rbp), %rsi
.L290:
	testb	$24, %al
	je	.L311
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L311
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L311:
	movq	-752(%rbp), %rdi
	movq	%r12, %r13
	call	_ZN6icu_676LocaleD1Ev@PLT
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L411:
	addl	$1, 8(%r13)
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L262:
	addl	$1, 8(%r8)
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L265:
	movl	8(%r8), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r8)
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L254:
	addl	$1, 8(%r8)
	movl	8(%r8), %eax
	movq	%r8, %rbx
	leal	-1(%rax), %edx
	movl	%edx, 8(%r8)
	jmp	.L257
	.p2align 4,,10
	.p2align 3
.L245:
	call	_ZdaPv@PLT
	jmp	.L397
.L319:
	movl	$1, %eax
.L241:
	movl	(%r15,%rax,4), %r13d
	movl	%r13d, -736(%rbp)
	call	_ZdaPv@PLT
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
	movq	%r15, %rdi
	leaq	-336(%rbp), %r15
	call	_ZdlPv@PLT
	leaq	-592(%rbp), %rax
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -752(%rbp)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movl	$0, -716(%rbp)
	cmpl	$2, %r13d
	jne	.L414
	movq	-752(%rbp), %rdi
	leaq	-716(%rbp), %rsi
	call	_ZN6icu_6713BreakIterator22createSentenceInstanceERKNS_6LocaleER10UErrorCode@PLT
	movq	%rax, %r13
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L414:
	cmpl	$3, -736(%rbp)
	jne	.L415
	movq	-752(%rbp), %rdi
	leaq	-716(%rbp), %rsi
	call	_ZN6icu_6713BreakIterator18createLineInstanceERKNS_6LocaleER10UErrorCode@PLT
	movq	%rax, %r13
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L415:
	movl	-736(%rbp), %edx
	testl	%edx, %edx
	jne	.L246
	movq	-752(%rbp), %rdi
	leaq	-716(%rbp), %rsi
	call	_ZN6icu_6713BreakIterator23createCharacterInstanceERKNS_6LocaleER10UErrorCode@PLT
	movq	%rax, %r13
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L281:
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory18NewJSObjectFromMapENS0_6HandleINS0_3MapEEENS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE@PLT
	movq	%rax, %r12
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L247:
	call	_ZdaPv@PLT
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L409:
	movq	%r12, %rdi
	movq	%r8, -768(%rbp)
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	movq	-768(%rbp), %r8
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L410:
	movq	%r12, %rdi
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	jmp	.L271
	.p2align 4,,10
	.p2align 3
.L273:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L412:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	cmpq	$0, -728(%rbp)
	je	.L277
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L278:
	cmpl	$1, %eax
	jne	.L276
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L408:
	leaq	.LC12(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L413:
	leaq	.LC13(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L258:
	movl	12(%rbx), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rbx)
	jmp	.L259
.L268:
	movl	12(%r8), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r8)
	jmp	.L269
.L277:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L278
.L318:
	xorl	%eax, %eax
	jmp	.L241
.L320:
	movl	$2, %eax
	jmp	.L241
.L406:
	movl	$3, %eax
	jmp	.L241
.L401:
	leaq	.LC3(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L402:
	call	__stack_chk_fail@PLT
.L399:
	call	_ZSt17__throw_bad_allocv@PLT
.L407:
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18181:
	.size	_ZN2v88internal17JSV8BreakIterator3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6ObjectEEES8_, .-_ZN2v88internal17JSV8BreakIterator3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6ObjectEEES8_
	.weak	_ZTVN2v88internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS1_17SkipResourceCheckEEE
	.section	.data.rel.ro.local._ZTVN2v88internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS1_17SkipResourceCheckEEE,"awG",@progbits,_ZTVN2v88internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS1_17SkipResourceCheckEEE,comdat
	.align 8
	.type	_ZTVN2v88internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS1_17SkipResourceCheckEEE, @object
	.size	_ZTVN2v88internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS1_17SkipResourceCheckEEE, 32
_ZTVN2v88internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS1_17SkipResourceCheckEEE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS1_17SkipResourceCheckEED1Ev
	.quad	_ZN2v88internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS1_17SkipResourceCheckEED0Ev
	.weak	_ZTVSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE
	.section	.data.rel.ro.local._ZTVSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE,"awG",@progbits,_ZTVSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 8
	.type	_ZTVSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTVSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE, 56
_ZTVSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE:
	.quad	0
	.quad	0
	.quad	_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.quad	_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.quad	_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.quad	_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.quad	_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.weak	_ZTVSt15_Sp_counted_ptrIPN6icu_6713UnicodeStringELN9__gnu_cxx12_Lock_policyE2EE
	.section	.data.rel.ro.local._ZTVSt15_Sp_counted_ptrIPN6icu_6713UnicodeStringELN9__gnu_cxx12_Lock_policyE2EE,"awG",@progbits,_ZTVSt15_Sp_counted_ptrIPN6icu_6713UnicodeStringELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 8
	.type	_ZTVSt15_Sp_counted_ptrIPN6icu_6713UnicodeStringELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTVSt15_Sp_counted_ptrIPN6icu_6713UnicodeStringELN9__gnu_cxx12_Lock_policyE2EE, 56
_ZTVSt15_Sp_counted_ptrIPN6icu_6713UnicodeStringELN9__gnu_cxx12_Lock_policyE2EE:
	.quad	0
	.quad	0
	.quad	_ZNSt15_Sp_counted_ptrIPN6icu_6713UnicodeStringELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.quad	_ZNSt15_Sp_counted_ptrIPN6icu_6713UnicodeStringELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.quad	_ZNSt15_Sp_counted_ptrIPN6icu_6713UnicodeStringELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.quad	_ZNSt15_Sp_counted_ptrIPN6icu_6713UnicodeStringELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.quad	_ZNSt15_Sp_counted_ptrIPN6icu_6713UnicodeStringELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.bss._ZZN2v88internal17JSV8BreakIterator19GetAvailableLocalesB5cxx11EvE17available_locales,"aw",@nobits
	.align 32
	.type	_ZZN2v88internal17JSV8BreakIterator19GetAvailableLocalesB5cxx11EvE17available_locales, @object
	.size	_ZZN2v88internal17JSV8BreakIterator19GetAvailableLocalesB5cxx11EvE17available_locales, 64
_ZZN2v88internal17JSV8BreakIterator19GetAvailableLocalesB5cxx11EvE17available_locales:
	.zero	64
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
