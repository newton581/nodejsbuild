	.file	"builtins-global.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB5450:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE5450:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB5451:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5451:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB5453:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5453:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.text._ZN2v88internalL23Builtin_Impl_GlobalEvalENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL23Builtin_Impl_GlobalEvalENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL23Builtin_Impl_GlobalEvalENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB20920:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-8(%rsi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	41096(%rdx), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	41088(%rdx), %rax
	addl	$1, 41104(%rdx)
	cmpl	$5, %edi
	movq	%rax, -72(%rbp)
	leaq	88(%rdx), %rax
	cmovle	%rax, %r13
	leal	-16(,%rdi,8), %eax
	leaq	-64(%rbp), %rdi
	cltq
	subq	%rax, %rbx
	movq	(%rbx), %rax
	movq	31(%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal7Context12global_proxyEv@PLT
	movq	41112(%r12), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L8
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L9:
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8Builtins20AllowDynamicFunctionEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEENS4_INS0_8JSObjectEEE@PLT
	testb	%al, %al
	jne	.L11
	movl	$35, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate10CountUsageENS_7Isolate17UseCounterFeatureE@PLT
	movq	88(%r12), %r13
.L12:
	subl	$1, 41104(%r12)
	movq	-72(%rbp), %rax
	movq	%rax, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L23
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L23:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L28
	addq	$56, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	31(%rax), %rax
	movq	39(%rax), %r8
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L13
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L14:
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8Compiler32ValidateDynamicCompilationSourceEPNS0_7IsolateENS0_6HandleINS0_7ContextEEENS4_INS0_6ObjectEEE@PLT
	movq	%rax, %r8
	testb	%dl, %dl
	je	.L16
	movq	0(%r13), %r13
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L13:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L29
.L15:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r8, (%rsi)
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L8:
	movq	41088(%r12), %r15
	cmpq	41096(%r12), %r15
	je	.L30
.L10:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r15)
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L16:
	movq	(%rbx), %rax
	movq	31(%rax), %rax
	movq	39(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L17
	movq	%r8, -80(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-80(%rbp), %r8
	movq	%rax, %rdi
.L18:
	movq	%r8, %rsi
	movl	$-1, %ecx
	xorl	%edx, %edx
	call	_ZN2v88internal8Compiler30GetFunctionFromValidatedStringENS0_6HandleINS0_7ContextEEENS0_11MaybeHandleINS0_6StringEEENS0_16ParseRestrictionEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L27
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal9Execution4CallEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_iPS6_@PLT
	testq	%rax, %rax
	je	.L27
	movq	(%rax), %r13
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L30:
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L17:
	movq	41088(%r12), %rdi
	cmpq	41096(%r12), %rdi
	je	.L31
.L19:
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdi)
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L27:
	movq	312(%r12), %r13
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L29:
	movq	%r12, %rdi
	movq	%r8, -80(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L31:
	movq	%r12, %rdi
	movq	%r8, -88(%rbp)
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %r8
	movq	-80(%rbp), %rsi
	movq	%rax, %rdi
	jmp	.L19
.L28:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20920:
	.size	_ZN2v88internalL23Builtin_Impl_GlobalEvalENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL23Builtin_Impl_GlobalEvalENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB8771:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L38
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L41
.L32:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L32
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE8771:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.rodata._ZN2v88internalL34Builtin_Impl_Stats_GlobalDecodeURIEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"disabled-by-default-v8.runtime"
	.section	.rodata._ZN2v88internalL34Builtin_Impl_Stats_GlobalDecodeURIEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"V8.Builtin_GlobalDecodeURI"
	.section	.text._ZN2v88internalL34Builtin_Impl_Stats_GlobalDecodeURIEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL34Builtin_Impl_Stats_GlobalDecodeURIEiPmPNS0_7IsolateE, @function
_ZN2v88internalL34Builtin_Impl_Stats_GlobalDecodeURIEiPmPNS0_7IsolateE:
.LFB20900:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edi, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L82
.L43:
	movq	_ZZN2v88internalL34Builtin_Impl_Stats_GlobalDecodeURIEiPmPNS0_7IsolateEE27trace_event_unique_atomic17(%rip), %r13
	testq	%r13, %r13
	je	.L83
.L45:
	movq	$0, -160(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L84
.L47:
	addl	$1, 41104(%r12)
	leaq	-8(%rbx), %rsi
	leaq	88(%r12), %rax
	cmpl	$5, %r15d
	cmovle	%rax, %rsi
	movq	41088(%r12), %r14
	movq	41096(%r12), %r13
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L53
.L56:
	movq	%r12, %rdi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L81
.L55:
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal3Uri6DecodeEPNS0_7IsolateENS0_6HandleINS0_6StringEEEb@PLT
	testq	%rax, %rax
	je	.L81
	movq	(%rax), %r15
.L57:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L61
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L61:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L85
.L42:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L86
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L56
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L84:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L87
.L48:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L49
	movq	(%rdi), %rax
	call	*8(%rax)
.L49:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L50
	movq	(%rdi), %rax
	call	*8(%rax)
.L50:
	leaq	.LC1(%rip), %rax
	movq	%r13, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L83:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L88
.L46:
	movq	%r13, _ZZN2v88internalL34Builtin_Impl_Stats_GlobalDecodeURIEiPmPNS0_7IsolateEE27trace_event_unique_atomic17(%rip)
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L81:
	movq	312(%r12), %r15
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L82:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$778, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L85:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L88:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L87:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC1(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L48
.L86:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20900:
	.size	_ZN2v88internalL34Builtin_Impl_Stats_GlobalDecodeURIEiPmPNS0_7IsolateE, .-_ZN2v88internalL34Builtin_Impl_Stats_GlobalDecodeURIEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL43Builtin_Impl_Stats_GlobalDecodeURIComponentEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"V8.Builtin_GlobalDecodeURIComponent"
	.section	.text._ZN2v88internalL43Builtin_Impl_Stats_GlobalDecodeURIComponentEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL43Builtin_Impl_Stats_GlobalDecodeURIComponentEiPmPNS0_7IsolateE, @function
_ZN2v88internalL43Builtin_Impl_Stats_GlobalDecodeURIComponentEiPmPNS0_7IsolateE:
.LFB20903:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edi, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L129
.L90:
	movq	_ZZN2v88internalL43Builtin_Impl_Stats_GlobalDecodeURIComponentEiPmPNS0_7IsolateEE27trace_event_unique_atomic28(%rip), %r13
	testq	%r13, %r13
	je	.L130
.L92:
	movq	$0, -160(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L131
.L94:
	addl	$1, 41104(%r12)
	leaq	-8(%rbx), %rsi
	leaq	88(%r12), %rax
	cmpl	$5, %r15d
	cmovle	%rax, %rsi
	movq	41088(%r12), %r14
	movq	41096(%r12), %r13
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L100
.L103:
	movq	%r12, %rdi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L128
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal3Uri6DecodeEPNS0_7IsolateENS0_6HandleINS0_6StringEEEb@PLT
	testq	%rax, %rax
	je	.L128
.L105:
	movq	(%rax), %r15
.L104:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L108
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L108:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L132
.L89:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L133
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L103
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal3Uri6DecodeEPNS0_7IsolateENS0_6HandleINS0_6StringEEEb@PLT
	testq	%rax, %rax
	jne	.L105
	.p2align 4,,10
	.p2align 3
.L128:
	movq	312(%r12), %r15
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L131:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L134
.L95:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L96
	movq	(%rdi), %rax
	call	*8(%rax)
.L96:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L97
	movq	(%rdi), %rax
	call	*8(%rax)
.L97:
	leaq	.LC2(%rip), %rax
	movq	%r13, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L130:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L135
.L93:
	movq	%r13, _ZZN2v88internalL43Builtin_Impl_Stats_GlobalDecodeURIComponentEiPmPNS0_7IsolateEE27trace_event_unique_atomic28(%rip)
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L129:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$779, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L132:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L135:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L134:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC2(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L95
.L133:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20903:
	.size	_ZN2v88internalL43Builtin_Impl_Stats_GlobalDecodeURIComponentEiPmPNS0_7IsolateE, .-_ZN2v88internalL43Builtin_Impl_Stats_GlobalDecodeURIComponentEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL34Builtin_Impl_Stats_GlobalEncodeURIEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"V8.Builtin_GlobalEncodeURI"
	.section	.text._ZN2v88internalL34Builtin_Impl_Stats_GlobalEncodeURIEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL34Builtin_Impl_Stats_GlobalEncodeURIEiPmPNS0_7IsolateE, @function
_ZN2v88internalL34Builtin_Impl_Stats_GlobalEncodeURIEiPmPNS0_7IsolateE:
.LFB20906:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edi, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L176
.L137:
	movq	_ZZN2v88internalL34Builtin_Impl_Stats_GlobalEncodeURIEiPmPNS0_7IsolateEE27trace_event_unique_atomic40(%rip), %r13
	testq	%r13, %r13
	je	.L177
.L139:
	movq	$0, -160(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L178
.L141:
	addl	$1, 41104(%r12)
	leaq	-8(%rbx), %rsi
	leaq	88(%r12), %rax
	cmpl	$5, %r15d
	cmovle	%rax, %rsi
	movq	41088(%r12), %r14
	movq	41096(%r12), %r13
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L147
.L150:
	movq	%r12, %rdi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L175
.L149:
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal3Uri6EncodeEPNS0_7IsolateENS0_6HandleINS0_6StringEEEb@PLT
	testq	%rax, %rax
	je	.L175
	movq	(%rax), %r15
.L151:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L155
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L155:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L179
.L136:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L180
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L147:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L150
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L178:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L181
.L142:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L143
	movq	(%rdi), %rax
	call	*8(%rax)
.L143:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L144
	movq	(%rdi), %rax
	call	*8(%rax)
.L144:
	leaq	.LC3(%rip), %rax
	movq	%r13, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L177:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L182
.L140:
	movq	%r13, _ZZN2v88internalL34Builtin_Impl_Stats_GlobalEncodeURIEiPmPNS0_7IsolateEE27trace_event_unique_atomic40(%rip)
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L175:
	movq	312(%r12), %r15
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L176:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$780, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L179:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L182:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L181:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC3(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L142
.L180:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20906:
	.size	_ZN2v88internalL34Builtin_Impl_Stats_GlobalEncodeURIEiPmPNS0_7IsolateE, .-_ZN2v88internalL34Builtin_Impl_Stats_GlobalEncodeURIEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL43Builtin_Impl_Stats_GlobalEncodeURIComponentEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"V8.Builtin_GlobalEncodeURIComponent"
	.section	.text._ZN2v88internalL43Builtin_Impl_Stats_GlobalEncodeURIComponentEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL43Builtin_Impl_Stats_GlobalEncodeURIComponentEiPmPNS0_7IsolateE, @function
_ZN2v88internalL43Builtin_Impl_Stats_GlobalEncodeURIComponentEiPmPNS0_7IsolateE:
.LFB20909:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edi, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L223
.L184:
	movq	_ZZN2v88internalL43Builtin_Impl_Stats_GlobalEncodeURIComponentEiPmPNS0_7IsolateEE27trace_event_unique_atomic50(%rip), %r13
	testq	%r13, %r13
	je	.L224
.L186:
	movq	$0, -160(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L225
.L188:
	addl	$1, 41104(%r12)
	leaq	-8(%rbx), %rsi
	leaq	88(%r12), %rax
	cmpl	$5, %r15d
	cmovle	%rax, %rsi
	movq	41088(%r12), %r14
	movq	41096(%r12), %r13
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L194
.L197:
	movq	%r12, %rdi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L222
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal3Uri6EncodeEPNS0_7IsolateENS0_6HandleINS0_6StringEEEb@PLT
	testq	%rax, %rax
	je	.L222
.L199:
	movq	(%rax), %r15
.L198:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L202
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L202:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L226
.L183:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L227
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L194:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L197
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal3Uri6EncodeEPNS0_7IsolateENS0_6HandleINS0_6StringEEEb@PLT
	testq	%rax, %rax
	jne	.L199
	.p2align 4,,10
	.p2align 3
.L222:
	movq	312(%r12), %r15
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L225:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L228
.L189:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L190
	movq	(%rdi), %rax
	call	*8(%rax)
.L190:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L191
	movq	(%rdi), %rax
	call	*8(%rax)
.L191:
	leaq	.LC4(%rip), %rax
	movq	%r13, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L224:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L229
.L187:
	movq	%r13, _ZZN2v88internalL43Builtin_Impl_Stats_GlobalEncodeURIComponentEiPmPNS0_7IsolateEE27trace_event_unique_atomic50(%rip)
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L223:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$781, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L226:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L229:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L228:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC4(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L189
.L227:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20909:
	.size	_ZN2v88internalL43Builtin_Impl_Stats_GlobalEncodeURIComponentEiPmPNS0_7IsolateE, .-_ZN2v88internalL43Builtin_Impl_Stats_GlobalEncodeURIComponentEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL31Builtin_Impl_Stats_GlobalEscapeEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"V8.Builtin_GlobalEscape"
	.section	.text._ZN2v88internalL31Builtin_Impl_Stats_GlobalEscapeEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL31Builtin_Impl_Stats_GlobalEscapeEiPmPNS0_7IsolateE, @function
_ZN2v88internalL31Builtin_Impl_Stats_GlobalEscapeEiPmPNS0_7IsolateE:
.LFB20912:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edi, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L270
.L231:
	movq	_ZZN2v88internalL31Builtin_Impl_Stats_GlobalEscapeEiPmPNS0_7IsolateEE27trace_event_unique_atomic62(%rip), %r13
	testq	%r13, %r13
	je	.L271
.L233:
	movq	$0, -160(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L272
.L235:
	addl	$1, 41104(%r12)
	leaq	-8(%rbx), %rsi
	leaq	88(%r12), %rax
	cmpl	$5, %r15d
	cmovle	%rax, %rsi
	movq	41088(%r12), %r14
	movq	41096(%r12), %r13
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L241
.L244:
	movq	%r12, %rdi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L269
	movq	%r12, %rdi
	call	_ZN2v88internal3Uri6EscapeEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	testq	%rax, %rax
	je	.L269
.L246:
	movq	(%rax), %r15
.L245:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L249
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L249:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L273
.L230:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L274
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L241:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L244
	movq	%r12, %rdi
	call	_ZN2v88internal3Uri6EscapeEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	testq	%rax, %rax
	jne	.L246
	.p2align 4,,10
	.p2align 3
.L269:
	movq	312(%r12), %r15
	jmp	.L245
	.p2align 4,,10
	.p2align 3
.L272:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L275
.L236:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L237
	movq	(%rdi), %rax
	call	*8(%rax)
.L237:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L238
	movq	(%rdi), %rax
	call	*8(%rax)
.L238:
	leaq	.LC5(%rip), %rax
	movq	%r13, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L271:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L276
.L234:
	movq	%r13, _ZZN2v88internalL31Builtin_Impl_Stats_GlobalEscapeEiPmPNS0_7IsolateEE27trace_event_unique_atomic62(%rip)
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L270:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$782, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L273:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L230
	.p2align 4,,10
	.p2align 3
.L276:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L275:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC5(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L236
.L274:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20912:
	.size	_ZN2v88internalL31Builtin_Impl_Stats_GlobalEscapeEiPmPNS0_7IsolateE, .-_ZN2v88internalL31Builtin_Impl_Stats_GlobalEscapeEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL33Builtin_Impl_Stats_GlobalUnescapeEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC6:
	.string	"V8.Builtin_GlobalUnescape"
	.section	.text._ZN2v88internalL33Builtin_Impl_Stats_GlobalUnescapeEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL33Builtin_Impl_Stats_GlobalUnescapeEiPmPNS0_7IsolateE, @function
_ZN2v88internalL33Builtin_Impl_Stats_GlobalUnescapeEiPmPNS0_7IsolateE:
.LFB20915:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edi, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L317
.L278:
	movq	_ZZN2v88internalL33Builtin_Impl_Stats_GlobalUnescapeEiPmPNS0_7IsolateEE27trace_event_unique_atomic73(%rip), %r13
	testq	%r13, %r13
	je	.L318
.L280:
	movq	$0, -160(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L319
.L282:
	addl	$1, 41104(%r12)
	leaq	-8(%rbx), %rsi
	leaq	88(%r12), %rax
	cmpl	$5, %r15d
	cmovle	%rax, %rsi
	movq	41088(%r12), %r14
	movq	41096(%r12), %r13
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L288
.L291:
	movq	%r12, %rdi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L316
	movq	%r12, %rdi
	call	_ZN2v88internal3Uri8UnescapeEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	testq	%rax, %rax
	je	.L316
.L293:
	movq	(%rax), %r15
.L292:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L296
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L296:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L320
.L277:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L321
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L288:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L291
	movq	%r12, %rdi
	call	_ZN2v88internal3Uri8UnescapeEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	testq	%rax, %rax
	jne	.L293
	.p2align 4,,10
	.p2align 3
.L316:
	movq	312(%r12), %r15
	jmp	.L292
	.p2align 4,,10
	.p2align 3
.L319:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L322
.L283:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L284
	movq	(%rdi), %rax
	call	*8(%rax)
.L284:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L285
	movq	(%rdi), %rax
	call	*8(%rax)
.L285:
	leaq	.LC6(%rip), %rax
	movq	%r13, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L318:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L323
.L281:
	movq	%r13, _ZZN2v88internalL33Builtin_Impl_Stats_GlobalUnescapeEiPmPNS0_7IsolateEE27trace_event_unique_atomic73(%rip)
	jmp	.L280
	.p2align 4,,10
	.p2align 3
.L317:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$783, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L320:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L323:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L281
	.p2align 4,,10
	.p2align 3
.L322:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC6(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L283
.L321:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20915:
	.size	_ZN2v88internalL33Builtin_Impl_Stats_GlobalUnescapeEiPmPNS0_7IsolateE, .-_ZN2v88internalL33Builtin_Impl_Stats_GlobalUnescapeEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL29Builtin_Impl_Stats_GlobalEvalEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC7:
	.string	"V8.Builtin_GlobalEval"
	.section	.text._ZN2v88internalL29Builtin_Impl_Stats_GlobalEvalEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL29Builtin_Impl_Stats_GlobalEvalEiPmPNS0_7IsolateE, @function
_ZN2v88internalL29Builtin_Impl_Stats_GlobalEvalEiPmPNS0_7IsolateE:
.LFB20918:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L353
.L325:
	movq	_ZZN2v88internalL29Builtin_Impl_Stats_GlobalEvalEiPmPNS0_7IsolateEE27trace_event_unique_atomic84(%rip), %rbx
	testq	%rbx, %rbx
	je	.L354
.L327:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L355
.L329:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL23Builtin_Impl_GlobalEvalENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L356
.L333:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L357
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L354:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L358
.L328:
	movq	%rbx, _ZZN2v88internalL29Builtin_Impl_Stats_GlobalEvalEiPmPNS0_7IsolateEE27trace_event_unique_atomic84(%rip)
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L355:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L359
.L330:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L331
	movq	(%rdi), %rax
	call	*8(%rax)
.L331:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L332
	movq	(%rdi), %rax
	call	*8(%rax)
.L332:
	leaq	.LC7(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L356:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L353:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$784, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L325
	.p2align 4,,10
	.p2align 3
.L359:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC7(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L358:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L328
.L357:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20918:
	.size	_ZN2v88internalL29Builtin_Impl_Stats_GlobalEvalEiPmPNS0_7IsolateE, .-_ZN2v88internalL29Builtin_Impl_Stats_GlobalEvalEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal23Builtin_GlobalDecodeURIEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal23Builtin_GlobalDecodeURIEiPmPNS0_7IsolateE
	.type	_ZN2v88internal23Builtin_GlobalDecodeURIEiPmPNS0_7IsolateE, @function
_ZN2v88internal23Builtin_GlobalDecodeURIEiPmPNS0_7IsolateE:
.LFB20901:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L374
	addl	$1, 41104(%rdx)
	leaq	88(%rdx), %rax
	subq	$8, %rsi
	cmpl	$5, %edi
	cmovle	%rax, %rsi
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L364
.L367:
	movq	%r12, %rdi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L373
.L366:
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal3Uri6DecodeEPNS0_7IsolateENS0_6HandleINS0_6StringEEEb@PLT
	testq	%rax, %rax
	je	.L373
	movq	(%rax), %r14
.L368:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L360
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L360:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L364:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L367
	jmp	.L366
	.p2align 4,,10
	.p2align 3
.L373:
	movq	312(%r12), %r14
	jmp	.L368
	.p2align 4,,10
	.p2align 3
.L374:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL34Builtin_Impl_Stats_GlobalDecodeURIEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE20901:
	.size	_ZN2v88internal23Builtin_GlobalDecodeURIEiPmPNS0_7IsolateE, .-_ZN2v88internal23Builtin_GlobalDecodeURIEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal32Builtin_GlobalDecodeURIComponentEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal32Builtin_GlobalDecodeURIComponentEiPmPNS0_7IsolateE
	.type	_ZN2v88internal32Builtin_GlobalDecodeURIComponentEiPmPNS0_7IsolateE, @function
_ZN2v88internal32Builtin_GlobalDecodeURIComponentEiPmPNS0_7IsolateE:
.LFB20904:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L389
	addl	$1, 41104(%rdx)
	leaq	88(%rdx), %rax
	subq	$8, %rsi
	cmpl	$5, %edi
	cmovle	%rax, %rsi
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L379
.L382:
	movq	%r12, %rdi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L388
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal3Uri6DecodeEPNS0_7IsolateENS0_6HandleINS0_6StringEEEb@PLT
	testq	%rax, %rax
	je	.L388
.L384:
	movq	(%rax), %r14
.L383:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L375
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L375:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L379:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L382
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal3Uri6DecodeEPNS0_7IsolateENS0_6HandleINS0_6StringEEEb@PLT
	testq	%rax, %rax
	jne	.L384
	.p2align 4,,10
	.p2align 3
.L388:
	movq	312(%r12), %r14
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L389:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL43Builtin_Impl_Stats_GlobalDecodeURIComponentEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE20904:
	.size	_ZN2v88internal32Builtin_GlobalDecodeURIComponentEiPmPNS0_7IsolateE, .-_ZN2v88internal32Builtin_GlobalDecodeURIComponentEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal23Builtin_GlobalEncodeURIEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal23Builtin_GlobalEncodeURIEiPmPNS0_7IsolateE
	.type	_ZN2v88internal23Builtin_GlobalEncodeURIEiPmPNS0_7IsolateE, @function
_ZN2v88internal23Builtin_GlobalEncodeURIEiPmPNS0_7IsolateE:
.LFB20907:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L404
	addl	$1, 41104(%rdx)
	leaq	88(%rdx), %rax
	subq	$8, %rsi
	cmpl	$5, %edi
	cmovle	%rax, %rsi
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L394
.L397:
	movq	%r12, %rdi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L403
.L396:
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal3Uri6EncodeEPNS0_7IsolateENS0_6HandleINS0_6StringEEEb@PLT
	testq	%rax, %rax
	je	.L403
	movq	(%rax), %r14
.L398:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L390
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L390:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L394:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L397
	jmp	.L396
	.p2align 4,,10
	.p2align 3
.L403:
	movq	312(%r12), %r14
	jmp	.L398
	.p2align 4,,10
	.p2align 3
.L404:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL34Builtin_Impl_Stats_GlobalEncodeURIEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE20907:
	.size	_ZN2v88internal23Builtin_GlobalEncodeURIEiPmPNS0_7IsolateE, .-_ZN2v88internal23Builtin_GlobalEncodeURIEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal32Builtin_GlobalEncodeURIComponentEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal32Builtin_GlobalEncodeURIComponentEiPmPNS0_7IsolateE
	.type	_ZN2v88internal32Builtin_GlobalEncodeURIComponentEiPmPNS0_7IsolateE, @function
_ZN2v88internal32Builtin_GlobalEncodeURIComponentEiPmPNS0_7IsolateE:
.LFB20910:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L419
	addl	$1, 41104(%rdx)
	leaq	88(%rdx), %rax
	subq	$8, %rsi
	cmpl	$5, %edi
	cmovle	%rax, %rsi
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L409
.L412:
	movq	%r12, %rdi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L418
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal3Uri6EncodeEPNS0_7IsolateENS0_6HandleINS0_6StringEEEb@PLT
	testq	%rax, %rax
	je	.L418
.L414:
	movq	(%rax), %r14
.L413:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L405
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L405:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L409:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L412
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal3Uri6EncodeEPNS0_7IsolateENS0_6HandleINS0_6StringEEEb@PLT
	testq	%rax, %rax
	jne	.L414
	.p2align 4,,10
	.p2align 3
.L418:
	movq	312(%r12), %r14
	jmp	.L413
	.p2align 4,,10
	.p2align 3
.L419:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL43Builtin_Impl_Stats_GlobalEncodeURIComponentEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE20910:
	.size	_ZN2v88internal32Builtin_GlobalEncodeURIComponentEiPmPNS0_7IsolateE, .-_ZN2v88internal32Builtin_GlobalEncodeURIComponentEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal20Builtin_GlobalEscapeEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal20Builtin_GlobalEscapeEiPmPNS0_7IsolateE
	.type	_ZN2v88internal20Builtin_GlobalEscapeEiPmPNS0_7IsolateE, @function
_ZN2v88internal20Builtin_GlobalEscapeEiPmPNS0_7IsolateE:
.LFB20913:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L434
	addl	$1, 41104(%rdx)
	leaq	88(%rdx), %rax
	subq	$8, %rsi
	cmpl	$5, %edi
	cmovle	%rax, %rsi
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L424
.L427:
	movq	%r12, %rdi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L433
	movq	%r12, %rdi
	call	_ZN2v88internal3Uri6EscapeEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	testq	%rax, %rax
	je	.L433
.L429:
	movq	(%rax), %r14
.L428:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L420
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L420:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L424:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L427
	movq	%r12, %rdi
	call	_ZN2v88internal3Uri6EscapeEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	testq	%rax, %rax
	jne	.L429
	.p2align 4,,10
	.p2align 3
.L433:
	movq	312(%r12), %r14
	jmp	.L428
	.p2align 4,,10
	.p2align 3
.L434:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL31Builtin_Impl_Stats_GlobalEscapeEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE20913:
	.size	_ZN2v88internal20Builtin_GlobalEscapeEiPmPNS0_7IsolateE, .-_ZN2v88internal20Builtin_GlobalEscapeEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal22Builtin_GlobalUnescapeEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal22Builtin_GlobalUnescapeEiPmPNS0_7IsolateE
	.type	_ZN2v88internal22Builtin_GlobalUnescapeEiPmPNS0_7IsolateE, @function
_ZN2v88internal22Builtin_GlobalUnescapeEiPmPNS0_7IsolateE:
.LFB20916:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L449
	addl	$1, 41104(%rdx)
	leaq	88(%rdx), %rax
	subq	$8, %rsi
	cmpl	$5, %edi
	cmovle	%rax, %rsi
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L439
.L442:
	movq	%r12, %rdi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L448
	movq	%r12, %rdi
	call	_ZN2v88internal3Uri8UnescapeEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	testq	%rax, %rax
	je	.L448
.L444:
	movq	(%rax), %r14
.L443:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L435
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L435:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L439:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L442
	movq	%r12, %rdi
	call	_ZN2v88internal3Uri8UnescapeEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	testq	%rax, %rax
	jne	.L444
	.p2align 4,,10
	.p2align 3
.L448:
	movq	312(%r12), %r14
	jmp	.L443
	.p2align 4,,10
	.p2align 3
.L449:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL33Builtin_Impl_Stats_GlobalUnescapeEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE20916:
	.size	_ZN2v88internal22Builtin_GlobalUnescapeEiPmPNS0_7IsolateE, .-_ZN2v88internal22Builtin_GlobalUnescapeEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal18Builtin_GlobalEvalEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal18Builtin_GlobalEvalEiPmPNS0_7IsolateE
	.type	_ZN2v88internal18Builtin_GlobalEvalEiPmPNS0_7IsolateE, @function
_ZN2v88internal18Builtin_GlobalEvalEiPmPNS0_7IsolateE:
.LFB20919:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L454
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL23Builtin_Impl_GlobalEvalENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L454:
	.cfi_restore 6
	jmp	_ZN2v88internalL29Builtin_Impl_Stats_GlobalEvalEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE20919:
	.size	_ZN2v88internal18Builtin_GlobalEvalEiPmPNS0_7IsolateE, .-_ZN2v88internal18Builtin_GlobalEvalEiPmPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal23Builtin_GlobalDecodeURIEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal23Builtin_GlobalDecodeURIEiPmPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal23Builtin_GlobalDecodeURIEiPmPNS0_7IsolateE:
.LFB25647:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE25647:
	.size	_GLOBAL__sub_I__ZN2v88internal23Builtin_GlobalDecodeURIEiPmPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal23Builtin_GlobalDecodeURIEiPmPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal23Builtin_GlobalDecodeURIEiPmPNS0_7IsolateE
	.section	.bss._ZZN2v88internalL29Builtin_Impl_Stats_GlobalEvalEiPmPNS0_7IsolateEE27trace_event_unique_atomic84,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL29Builtin_Impl_Stats_GlobalEvalEiPmPNS0_7IsolateEE27trace_event_unique_atomic84, @object
	.size	_ZZN2v88internalL29Builtin_Impl_Stats_GlobalEvalEiPmPNS0_7IsolateEE27trace_event_unique_atomic84, 8
_ZZN2v88internalL29Builtin_Impl_Stats_GlobalEvalEiPmPNS0_7IsolateEE27trace_event_unique_atomic84:
	.zero	8
	.section	.bss._ZZN2v88internalL33Builtin_Impl_Stats_GlobalUnescapeEiPmPNS0_7IsolateEE27trace_event_unique_atomic73,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL33Builtin_Impl_Stats_GlobalUnescapeEiPmPNS0_7IsolateEE27trace_event_unique_atomic73, @object
	.size	_ZZN2v88internalL33Builtin_Impl_Stats_GlobalUnescapeEiPmPNS0_7IsolateEE27trace_event_unique_atomic73, 8
_ZZN2v88internalL33Builtin_Impl_Stats_GlobalUnescapeEiPmPNS0_7IsolateEE27trace_event_unique_atomic73:
	.zero	8
	.section	.bss._ZZN2v88internalL31Builtin_Impl_Stats_GlobalEscapeEiPmPNS0_7IsolateEE27trace_event_unique_atomic62,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL31Builtin_Impl_Stats_GlobalEscapeEiPmPNS0_7IsolateEE27trace_event_unique_atomic62, @object
	.size	_ZZN2v88internalL31Builtin_Impl_Stats_GlobalEscapeEiPmPNS0_7IsolateEE27trace_event_unique_atomic62, 8
_ZZN2v88internalL31Builtin_Impl_Stats_GlobalEscapeEiPmPNS0_7IsolateEE27trace_event_unique_atomic62:
	.zero	8
	.section	.bss._ZZN2v88internalL43Builtin_Impl_Stats_GlobalEncodeURIComponentEiPmPNS0_7IsolateEE27trace_event_unique_atomic50,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL43Builtin_Impl_Stats_GlobalEncodeURIComponentEiPmPNS0_7IsolateEE27trace_event_unique_atomic50, @object
	.size	_ZZN2v88internalL43Builtin_Impl_Stats_GlobalEncodeURIComponentEiPmPNS0_7IsolateEE27trace_event_unique_atomic50, 8
_ZZN2v88internalL43Builtin_Impl_Stats_GlobalEncodeURIComponentEiPmPNS0_7IsolateEE27trace_event_unique_atomic50:
	.zero	8
	.section	.bss._ZZN2v88internalL34Builtin_Impl_Stats_GlobalEncodeURIEiPmPNS0_7IsolateEE27trace_event_unique_atomic40,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL34Builtin_Impl_Stats_GlobalEncodeURIEiPmPNS0_7IsolateEE27trace_event_unique_atomic40, @object
	.size	_ZZN2v88internalL34Builtin_Impl_Stats_GlobalEncodeURIEiPmPNS0_7IsolateEE27trace_event_unique_atomic40, 8
_ZZN2v88internalL34Builtin_Impl_Stats_GlobalEncodeURIEiPmPNS0_7IsolateEE27trace_event_unique_atomic40:
	.zero	8
	.section	.bss._ZZN2v88internalL43Builtin_Impl_Stats_GlobalDecodeURIComponentEiPmPNS0_7IsolateEE27trace_event_unique_atomic28,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL43Builtin_Impl_Stats_GlobalDecodeURIComponentEiPmPNS0_7IsolateEE27trace_event_unique_atomic28, @object
	.size	_ZZN2v88internalL43Builtin_Impl_Stats_GlobalDecodeURIComponentEiPmPNS0_7IsolateEE27trace_event_unique_atomic28, 8
_ZZN2v88internalL43Builtin_Impl_Stats_GlobalDecodeURIComponentEiPmPNS0_7IsolateEE27trace_event_unique_atomic28:
	.zero	8
	.section	.bss._ZZN2v88internalL34Builtin_Impl_Stats_GlobalDecodeURIEiPmPNS0_7IsolateEE27trace_event_unique_atomic17,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL34Builtin_Impl_Stats_GlobalDecodeURIEiPmPNS0_7IsolateEE27trace_event_unique_atomic17, @object
	.size	_ZZN2v88internalL34Builtin_Impl_Stats_GlobalDecodeURIEiPmPNS0_7IsolateEE27trace_event_unique_atomic17, 8
_ZZN2v88internalL34Builtin_Impl_Stats_GlobalDecodeURIEiPmPNS0_7IsolateEE27trace_event_unique_atomic17:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
