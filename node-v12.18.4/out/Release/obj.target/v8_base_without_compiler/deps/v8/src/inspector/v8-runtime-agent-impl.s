	.file	"v8-runtime-agent-impl.cc"
	.text
	.section	.text._ZN12v8_inspector17V8InspectorClient23runIfWaitingForDebuggerEi,"axG",@progbits,_ZN12v8_inspector17V8InspectorClient23runIfWaitingForDebuggerEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector17V8InspectorClient23runIfWaitingForDebuggerEi
	.type	_ZN12v8_inspector17V8InspectorClient23runIfWaitingForDebuggerEi, @function
_ZN12v8_inspector17V8InspectorClient23runIfWaitingForDebuggerEi:
.LFB4055:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4055:
	.size	_ZN12v8_inspector17V8InspectorClient23runIfWaitingForDebuggerEi, .-_ZN12v8_inspector17V8InspectorClient23runIfWaitingForDebuggerEi
	.section	.text._ZN12v8_inspector17V8InspectorClient27ensureDefaultContextInGroupEi,"axG",@progbits,_ZN12v8_inspector17V8InspectorClient27ensureDefaultContextInGroupEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector17V8InspectorClient27ensureDefaultContextInGroupEi
	.type	_ZN12v8_inspector17V8InspectorClient27ensureDefaultContextInGroupEi, @function
_ZN12v8_inspector17V8InspectorClient27ensureDefaultContextInGroupEi:
.LFB4065:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4065:
	.size	_ZN12v8_inspector17V8InspectorClient27ensureDefaultContextInGroupEi, .-_ZN12v8_inspector17V8InspectorClient27ensureDefaultContextInGroupEi
	.section	.text._ZN12v8_inspector17V8InspectorClient29beginEnsureAllContextsInGroupEi,"axG",@progbits,_ZN12v8_inspector17V8InspectorClient29beginEnsureAllContextsInGroupEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector17V8InspectorClient29beginEnsureAllContextsInGroupEi
	.type	_ZN12v8_inspector17V8InspectorClient29beginEnsureAllContextsInGroupEi, @function
_ZN12v8_inspector17V8InspectorClient29beginEnsureAllContextsInGroupEi:
.LFB4066:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4066:
	.size	_ZN12v8_inspector17V8InspectorClient29beginEnsureAllContextsInGroupEi, .-_ZN12v8_inspector17V8InspectorClient29beginEnsureAllContextsInGroupEi
	.section	.text._ZN12v8_inspector17V8InspectorClient27endEnsureAllContextsInGroupEi,"axG",@progbits,_ZN12v8_inspector17V8InspectorClient27endEnsureAllContextsInGroupEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector17V8InspectorClient27endEnsureAllContextsInGroupEi
	.type	_ZN12v8_inspector17V8InspectorClient27endEnsureAllContextsInGroupEi, @function
_ZN12v8_inspector17V8InspectorClient27endEnsureAllContextsInGroupEi:
.LFB4067:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4067:
	.size	_ZN12v8_inspector17V8InspectorClient27endEnsureAllContextsInGroupEi, .-_ZN12v8_inspector17V8InspectorClient27endEnsureAllContextsInGroupEi
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB7720:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE7720:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB7721:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE7721:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB7723:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7723:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.text._ZNSt14_Function_base13_Base_managerIZN12v8_inspector18V8RuntimeAgentImpl10addBindingERKNS1_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIiEEEUlPNS1_16InspectedContextEE_E10_M_managerERSt9_Any_dataRKSF_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector18V8RuntimeAgentImpl10addBindingERKNS1_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIiEEEUlPNS1_16InspectedContextEE_E10_M_managerERSt9_Any_dataRKSF_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN12v8_inspector18V8RuntimeAgentImpl10addBindingERKNS1_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIiEEEUlPNS1_16InspectedContextEE_E10_M_managerERSt9_Any_dataRKSF_St18_Manager_operation:
.LFB10999:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L10
	cmpl	$3, %edx
	je	.L11
	cmpl	$1, %edx
	je	.L15
.L11:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movdqu	(%rsi), %xmm0
	xorl	%eax, %eax
	movups	%xmm0, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE10999:
	.size	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector18V8RuntimeAgentImpl10addBindingERKNS1_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIiEEEUlPNS1_16InspectedContextEE_E10_M_managerERSt9_Any_dataRKSF_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN12v8_inspector18V8RuntimeAgentImpl10addBindingERKNS1_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIiEEEUlPNS1_16InspectedContextEE_E10_M_managerERSt9_Any_dataRKSF_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN12v8_inspector18V8RuntimeAgentImpl7restoreEvEUlPNS1_16InspectedContextEE_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector18V8RuntimeAgentImpl7restoreEvEUlPNS1_16InspectedContextEE_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN12v8_inspector18V8RuntimeAgentImpl7restoreEvEUlPNS1_16InspectedContextEE_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation:
.LFB11009:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L17
	cmpl	$3, %edx
	je	.L18
	cmpl	$1, %edx
	je	.L22
.L18:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE11009:
	.size	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector18V8RuntimeAgentImpl7restoreEvEUlPNS1_16InspectedContextEE_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN12v8_inspector18V8RuntimeAgentImpl7restoreEvEUlPNS1_16InspectedContextEE_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN12v8_inspector18V8RuntimeAgentImpl5resetEvEUlPNS1_16InspectedContextEE_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector18V8RuntimeAgentImpl5resetEvEUlPNS1_16InspectedContextEE_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN12v8_inspector18V8RuntimeAgentImpl5resetEvEUlPNS1_16InspectedContextEE_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation:
.LFB11019:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L24
	cmpl	$3, %edx
	je	.L25
	cmpl	$1, %edx
	je	.L29
.L25:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE11019:
	.size	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector18V8RuntimeAgentImpl5resetEvEUlPNS1_16InspectedContextEE_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN12v8_inspector18V8RuntimeAgentImpl5resetEvEUlPNS1_16InspectedContextEE_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation
	.section	.text._ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend17RunScriptCallbackEED2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend17RunScriptCallbackEED2Ev, @function
_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend17RunScriptCallbackEED2Ev:
.LFB14408:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend17RunScriptCallbackEEE(%rip), %rax
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L30
	movq	(%rdi), %rax
	jmp	*32(%rax)
	.p2align 4,,10
	.p2align 3
.L30:
	ret
	.cfi_endproc
.LFE14408:
	.size	_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend17RunScriptCallbackEED2Ev, .-_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend17RunScriptCallbackEED2Ev
	.set	_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend17RunScriptCallbackEED1Ev,_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend17RunScriptCallbackEED2Ev
	.section	.text._ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend20AwaitPromiseCallbackEED2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend20AwaitPromiseCallbackEED2Ev, @function
_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend20AwaitPromiseCallbackEED2Ev:
.LFB14412:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend20AwaitPromiseCallbackEEE(%rip), %rax
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L32
	movq	(%rdi), %rax
	jmp	*32(%rax)
	.p2align 4,,10
	.p2align 3
.L32:
	ret
	.cfi_endproc
.LFE14412:
	.size	_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend20AwaitPromiseCallbackEED2Ev, .-_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend20AwaitPromiseCallbackEED2Ev
	.set	_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend20AwaitPromiseCallbackEED1Ev,_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend20AwaitPromiseCallbackEED2Ev
	.section	.text._ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend16EvaluateCallbackEED2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend16EvaluateCallbackEED2Ev, @function
_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend16EvaluateCallbackEED2Ev:
.LFB14416:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend16EvaluateCallbackEEE(%rip), %rax
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L34
	movq	(%rdi), %rax
	jmp	*32(%rax)
	.p2align 4,,10
	.p2align 3
.L34:
	ret
	.cfi_endproc
.LFE14416:
	.size	_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend16EvaluateCallbackEED2Ev, .-_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend16EvaluateCallbackEED2Ev
	.set	_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend16EvaluateCallbackEED1Ev,_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend16EvaluateCallbackEED2Ev
	.section	.text._ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend22CallFunctionOnCallbackEED2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend22CallFunctionOnCallbackEED2Ev, @function
_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend22CallFunctionOnCallbackEED2Ev:
.LFB14420:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend22CallFunctionOnCallbackEEE(%rip), %rax
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L36
	movq	(%rdi), %rax
	jmp	*32(%rax)
	.p2align 4,,10
	.p2align 3
.L36:
	ret
	.cfi_endproc
.LFE14420:
	.size	_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend22CallFunctionOnCallbackEED2Ev, .-_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend22CallFunctionOnCallbackEED2Ev
	.set	_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend22CallFunctionOnCallbackEED1Ev,_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend22CallFunctionOnCallbackEED2Ev
	.section	.text._ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend17RunScriptCallbackEE11sendFailureERKNS2_16DispatchResponseE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend17RunScriptCallbackEE11sendFailureERKNS2_16DispatchResponseE, @function
_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend17RunScriptCallbackEE11sendFailureERKNS2_16DispatchResponseE:
.LFB14436:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.cfi_endproc
.LFE14436:
	.size	_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend17RunScriptCallbackEE11sendFailureERKNS2_16DispatchResponseE, .-_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend17RunScriptCallbackEE11sendFailureERKNS2_16DispatchResponseE
	.section	.text._ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend20AwaitPromiseCallbackEE11sendFailureERKNS2_16DispatchResponseE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend20AwaitPromiseCallbackEE11sendFailureERKNS2_16DispatchResponseE, @function
_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend20AwaitPromiseCallbackEE11sendFailureERKNS2_16DispatchResponseE:
.LFB14438:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.cfi_endproc
.LFE14438:
	.size	_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend20AwaitPromiseCallbackEE11sendFailureERKNS2_16DispatchResponseE, .-_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend20AwaitPromiseCallbackEE11sendFailureERKNS2_16DispatchResponseE
	.section	.text._ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend16EvaluateCallbackEE11sendFailureERKNS2_16DispatchResponseE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend16EvaluateCallbackEE11sendFailureERKNS2_16DispatchResponseE, @function
_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend16EvaluateCallbackEE11sendFailureERKNS2_16DispatchResponseE:
.LFB14440:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.cfi_endproc
.LFE14440:
	.size	_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend16EvaluateCallbackEE11sendFailureERKNS2_16DispatchResponseE, .-_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend16EvaluateCallbackEE11sendFailureERKNS2_16DispatchResponseE
	.section	.text._ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend22CallFunctionOnCallbackEE11sendFailureERKNS2_16DispatchResponseE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend22CallFunctionOnCallbackEE11sendFailureERKNS2_16DispatchResponseE, @function
_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend22CallFunctionOnCallbackEE11sendFailureERKNS2_16DispatchResponseE:
.LFB14442:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.cfi_endproc
.LFE14442:
	.size	_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend22CallFunctionOnCallbackEE11sendFailureERKNS2_16DispatchResponseE, .-_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend22CallFunctionOnCallbackEE11sendFailureERKNS2_16DispatchResponseE
	.section	.text._ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend22CallFunctionOnCallbackEED0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend22CallFunctionOnCallbackEED0Ev, @function
_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend22CallFunctionOnCallbackEED0Ev:
.LFB14422:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend22CallFunctionOnCallbackEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L43
	movq	(%rdi), %rax
	call	*32(%rax)
.L43:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$16, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE14422:
	.size	_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend22CallFunctionOnCallbackEED0Ev, .-_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend22CallFunctionOnCallbackEED0Ev
	.section	.text._ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend16EvaluateCallbackEED0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend16EvaluateCallbackEED0Ev, @function
_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend16EvaluateCallbackEED0Ev:
.LFB14418:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend16EvaluateCallbackEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L49
	movq	(%rdi), %rax
	call	*32(%rax)
.L49:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$16, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE14418:
	.size	_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend16EvaluateCallbackEED0Ev, .-_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend16EvaluateCallbackEED0Ev
	.section	.text._ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend20AwaitPromiseCallbackEED0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend20AwaitPromiseCallbackEED0Ev, @function
_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend20AwaitPromiseCallbackEED0Ev:
.LFB14414:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend20AwaitPromiseCallbackEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L55
	movq	(%rdi), %rax
	call	*32(%rax)
.L55:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$16, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE14414:
	.size	_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend20AwaitPromiseCallbackEED0Ev, .-_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend20AwaitPromiseCallbackEED0Ev
	.section	.text._ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend17RunScriptCallbackEED0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend17RunScriptCallbackEED0Ev, @function
_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend17RunScriptCallbackEED0Ev:
.LFB14410:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend17RunScriptCallbackEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L61
	movq	(%rdi), %rax
	call	*32(%rax)
.L61:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$16, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE14410:
	.size	_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend17RunScriptCallbackEED0Ev, .-_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend17RunScriptCallbackEED0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime9CallFrameD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime9CallFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime9CallFrameD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime9CallFrameD2Ev, @function
_ZN12v8_inspector8protocol7Runtime9CallFrameD2Ev:
.LFB5119:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	88(%rdi), %rdi
	leaq	104(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L67
	call	_ZdlPv@PLT
.L67:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L68
	call	_ZdlPv@PLT
.L68:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L66
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5119:
	.size	_ZN12v8_inspector8protocol7Runtime9CallFrameD2Ev, .-_ZN12v8_inspector8protocol7Runtime9CallFrameD2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime9CallFrameD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime9CallFrameD1Ev,_ZN12v8_inspector8protocol7Runtime9CallFrameD2Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime12CallArgumentD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12CallArgumentD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12CallArgumentD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime12CallArgumentD2Ev, @function
_ZN12v8_inspector8protocol7Runtime12CallArgumentD2Ev:
.LFB4930:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12CallArgumentE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	72(%rdi), %rdi
	leaq	88(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L72
	call	_ZdlPv@PLT
.L72:
	movq	24(%rbx), %rdi
	leaq	40(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L73
	call	_ZdlPv@PLT
.L73:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L71
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L71:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4930:
	.size	_ZN12v8_inspector8protocol7Runtime12CallArgumentD2Ev, .-_ZN12v8_inspector8protocol7Runtime12CallArgumentD2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime12CallArgumentD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime12CallArgumentD1Ev,_ZN12v8_inspector8protocol7Runtime12CallArgumentD2Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime13CustomPreviewD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13CustomPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD2Ev, @function
_ZN12v8_inspector8protocol7Runtime13CustomPreviewD2Ev:
.LFB4606:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	56(%rdi), %rdi
	leaq	72(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L77
	call	_ZdlPv@PLT
.L77:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L76
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L76:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4606:
	.size	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD2Ev, .-_ZN12v8_inspector8protocol7Runtime13CustomPreviewD2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD1Ev,_ZN12v8_inspector8protocol7Runtime13CustomPreviewD2Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime27ExecutionContextDescriptionD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime27ExecutionContextDescriptionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime27ExecutionContextDescriptionD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime27ExecutionContextDescriptionD2Ev, @function
_ZN12v8_inspector8protocol7Runtime27ExecutionContextDescriptionD2Ev:
.LFB4967:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime27ExecutionContextDescriptionE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	96(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L81
	movq	(%rdi), %rax
	call	*24(%rax)
.L81:
	movq	56(%rbx), %rdi
	leaq	72(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L82
	call	_ZdlPv@PLT
.L82:
	movq	16(%rbx), %rdi
	addq	$32, %rbx
	cmpq	%rbx, %rdi
	je	.L80
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L80:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4967:
	.size	_ZN12v8_inspector8protocol7Runtime27ExecutionContextDescriptionD2Ev, .-_ZN12v8_inspector8protocol7Runtime27ExecutionContextDescriptionD2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime27ExecutionContextDescriptionD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime27ExecutionContextDescriptionD1Ev,_ZN12v8_inspector8protocol7Runtime27ExecutionContextDescriptionD2Ev
	.section	.rodata._ZN12v8_inspector18V8RuntimeAgentImpl28setMaxCallStackSizeToCaptureEi.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"maxCallStackSizeToCapture should be non-negative"
	.section	.text._ZN12v8_inspector18V8RuntimeAgentImpl28setMaxCallStackSizeToCaptureEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector18V8RuntimeAgentImpl28setMaxCallStackSizeToCaptureEi
	.type	_ZN12v8_inspector18V8RuntimeAgentImpl28setMaxCallStackSizeToCaptureEi, @function
_ZN12v8_inspector18V8RuntimeAgentImpl28setMaxCallStackSizeToCaptureEi:
.LFB8032:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	js	.L94
	movl	%edx, _ZN12v8_inspector16V8StackTraceImpl25maxCallStackSizeToCaptureE(%rip)
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
.L88:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L95
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	.cfi_restore_state
	leaq	-64(%rbp), %r13
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L88
	call	_ZdlPv@PLT
	jmp	.L88
.L95:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8032:
	.size	_ZN12v8_inspector18V8RuntimeAgentImpl28setMaxCallStackSizeToCaptureEi, .-_ZN12v8_inspector18V8RuntimeAgentImpl28setMaxCallStackSizeToCaptureEi
	.section	.text._ZN12v8_inspector8protocol7Runtime16ExceptionDetails17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime16ExceptionDetails17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime16ExceptionDetails17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol7Runtime16ExceptionDetails17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol7Runtime16ExceptionDetails17serializeToBinaryEv:
.LFB5064:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime16ExceptionDetails7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L96
	movq	(%rdi), %rax
	call	*24(%rax)
.L96:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L103
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L103:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5064:
	.size	_ZN12v8_inspector8protocol7Runtime16ExceptionDetails17serializeToBinaryEv, .-_ZN12v8_inspector8protocol7Runtime16ExceptionDetails17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol7Runtime16ExceptionDetails15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime16ExceptionDetails15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime16ExceptionDetails15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol7Runtime16ExceptionDetails15serializeToJSONEv, @function
_ZN12v8_inspector8protocol7Runtime16ExceptionDetails15serializeToJSONEv:
.LFB5063:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime16ExceptionDetails7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L104
	movq	(%rdi), %rax
	call	*24(%rax)
.L104:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L111
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L111:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5063:
	.size	_ZN12v8_inspector8protocol7Runtime16ExceptionDetails15serializeToJSONEv, .-_ZN12v8_inspector8protocol7Runtime16ExceptionDetails15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv:
.LFB5133:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime9CallFrame7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L112
	movq	(%rdi), %rax
	call	*24(%rax)
.L112:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L119
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L119:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5133:
	.size	_ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv, .-_ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv, @function
_ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv:
.LFB5132:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime9CallFrame7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L120
	movq	(%rdi), %rax
	call	*24(%rax)
.L120:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L127
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L127:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5132:
	.size	_ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv, .-_ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv:
.LFB4615:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime13CustomPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L128
	movq	(%rdi), %rax
	call	*24(%rax)
.L128:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L135
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L135:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4615:
	.size	_ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv, .-_ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv, @function
_ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv:
.LFB4614:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime13CustomPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L136
	movq	(%rdi), %rax
	call	*24(%rax)
.L136:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L143
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L143:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4614:
	.size	_ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv, .-_ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv:
.LFB4661:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime13ObjectPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L144
	movq	(%rdi), %rax
	call	*24(%rax)
.L144:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L151
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L151:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4661:
	.size	_ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv, .-_ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv, @function
_ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv:
.LFB4660:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime13ObjectPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L152
	movq	(%rdi), %rax
	call	*24(%rax)
.L152:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L159
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L159:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4660:
	.size	_ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv, .-_ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv:
.LFB4762:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime12EntryPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L160
	movq	(%rdi), %rax
	call	*24(%rax)
.L160:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L167
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L167:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4762:
	.size	_ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv, .-_ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv, @function
_ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv:
.LFB4761:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime12EntryPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L168
	movq	(%rdi), %rax
	call	*24(%rax)
.L168:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L175
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L175:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4761:
	.size	_ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv, .-_ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv:
.LFB4730:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime15PropertyPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L176
	movq	(%rdi), %rax
	call	*24(%rax)
.L176:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L183
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L183:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4730:
	.size	_ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv, .-_ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv, @function
_ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv:
.LFB4729:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime15PropertyPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L184
	movq	(%rdi), %rax
	call	*24(%rax)
.L184:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L191
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L191:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4729:
	.size	_ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv, .-_ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol7Runtime12CallArgument17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12CallArgument17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12CallArgument17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol7Runtime12CallArgument17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol7Runtime12CallArgument17serializeToBinaryEv:
.LFB4943:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime12CallArgument7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L192
	movq	(%rdi), %rax
	call	*24(%rax)
.L192:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L199
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L199:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4943:
	.size	_ZN12v8_inspector8protocol7Runtime12CallArgument17serializeToBinaryEv, .-_ZN12v8_inspector8protocol7Runtime12CallArgument17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol7Runtime12CallArgument15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12CallArgument15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12CallArgument15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol7Runtime12CallArgument15serializeToJSONEv, @function
_ZN12v8_inspector8protocol7Runtime12CallArgument15serializeToJSONEv:
.LFB4942:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime12CallArgument7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L200
	movq	(%rdi), %rax
	call	*24(%rax)
.L200:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L207
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L207:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4942:
	.size	_ZN12v8_inspector8protocol7Runtime12CallArgument15serializeToJSONEv, .-_ZN12v8_inspector8protocol7Runtime12CallArgument15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol7Runtime26InternalPropertyDescriptor17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime26InternalPropertyDescriptor17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime26InternalPropertyDescriptor17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol7Runtime26InternalPropertyDescriptor17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol7Runtime26InternalPropertyDescriptor17serializeToBinaryEv:
.LFB4882:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime26InternalPropertyDescriptor7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L208
	movq	(%rdi), %rax
	call	*24(%rax)
.L208:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L215
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L215:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4882:
	.size	_ZN12v8_inspector8protocol7Runtime26InternalPropertyDescriptor17serializeToBinaryEv, .-_ZN12v8_inspector8protocol7Runtime26InternalPropertyDescriptor17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol7Runtime26InternalPropertyDescriptor15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime26InternalPropertyDescriptor15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime26InternalPropertyDescriptor15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol7Runtime26InternalPropertyDescriptor15serializeToJSONEv, @function
_ZN12v8_inspector8protocol7Runtime26InternalPropertyDescriptor15serializeToJSONEv:
.LFB4881:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime26InternalPropertyDescriptor7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L216
	movq	(%rdi), %rax
	call	*24(%rax)
.L216:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L223
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L223:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4881:
	.size	_ZN12v8_inspector8protocol7Runtime26InternalPropertyDescriptor15serializeToJSONEv, .-_ZN12v8_inspector8protocol7Runtime26InternalPropertyDescriptor15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol7Runtime25PrivatePropertyDescriptor17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime25PrivatePropertyDescriptor17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime25PrivatePropertyDescriptor17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol7Runtime25PrivatePropertyDescriptor17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol7Runtime25PrivatePropertyDescriptor17serializeToBinaryEv:
.LFB4910:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime25PrivatePropertyDescriptor7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L224
	movq	(%rdi), %rax
	call	*24(%rax)
.L224:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L231
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L231:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4910:
	.size	_ZN12v8_inspector8protocol7Runtime25PrivatePropertyDescriptor17serializeToBinaryEv, .-_ZN12v8_inspector8protocol7Runtime25PrivatePropertyDescriptor17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol7Runtime25PrivatePropertyDescriptor15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime25PrivatePropertyDescriptor15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime25PrivatePropertyDescriptor15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol7Runtime25PrivatePropertyDescriptor15serializeToJSONEv, @function
_ZN12v8_inspector8protocol7Runtime25PrivatePropertyDescriptor15serializeToJSONEv:
.LFB4909:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime25PrivatePropertyDescriptor7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L232
	movq	(%rdi), %rax
	call	*24(%rax)
.L232:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L239
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L239:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4909:
	.size	_ZN12v8_inspector8protocol7Runtime25PrivatePropertyDescriptor15serializeToJSONEv, .-_ZN12v8_inspector8protocol7Runtime25PrivatePropertyDescriptor15serializeToJSONEv
	.section	.text._ZN12v8_inspector18V8RuntimeAgentImpl18releaseObjectGroupERKNS_8String16E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector18V8RuntimeAgentImpl18releaseObjectGroupERKNS_8String16E
	.type	_ZN12v8_inspector18V8RuntimeAgentImpl18releaseObjectGroupERKNS_8String16E, @function
_ZN12v8_inspector18V8RuntimeAgentImpl18releaseObjectGroupERKNS_8String16E:
.LFB8029:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rdx, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	8(%r8), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector22V8InspectorSessionImpl18releaseObjectGroupERKNS_8String16E@PLT
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L243
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L243:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8029:
	.size	_ZN12v8_inspector18V8RuntimeAgentImpl18releaseObjectGroupERKNS_8String16E, .-_ZN12v8_inspector18V8RuntimeAgentImpl18releaseObjectGroupERKNS_8String16E
	.section	.rodata._ZN12v8_inspector18V8RuntimeAgentImpl31setCustomObjectFormatterEnabledEb.str1.1,"aMS",@progbits,1
.LC1:
	.string	"Runtime agent is not enabled"
	.section	.text._ZN12v8_inspector18V8RuntimeAgentImpl31setCustomObjectFormatterEnabledEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector18V8RuntimeAgentImpl31setCustomObjectFormatterEnabledEb
	.type	_ZN12v8_inspector18V8RuntimeAgentImpl31setCustomObjectFormatterEnabledEb, @function
_ZN12v8_inspector18V8RuntimeAgentImpl31setCustomObjectFormatterEnabledEb:
.LFB8031:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movzbl	%dl, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	16(%rsi), %r15
	leaq	_ZN12v8_inspector23V8RuntimeAgentImplStateL28customObjectFormatterEnabledE(%rip), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r15, %rdi
	movl	%r13d, %edx
	movq	%r14, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue10setBooleanERKNS_8String16Eb@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %r15
	cmpq	%r15, %rdi
	je	.L245
	call	_ZdlPv@PLT
.L245:
	cmpb	$0, 40(%rbx)
	jne	.L246
	leaq	.LC1(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L244
	call	_ZdlPv@PLT
.L244:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L251
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L246:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	movl	%r13d, %esi
	call	_ZN12v8_inspector22V8InspectorSessionImpl31setCustomObjectFormatterEnabledEb@PLT
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	jmp	.L244
.L251:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8031:
	.size	_ZN12v8_inspector18V8RuntimeAgentImpl31setCustomObjectFormatterEnabledEb, .-_ZN12v8_inspector18V8RuntimeAgentImpl31setCustomObjectFormatterEnabledEb
	.section	.text._ZN12v8_inspector18V8RuntimeAgentImpl21discardConsoleEntriesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector18V8RuntimeAgentImpl21discardConsoleEntriesEv
	.type	_ZN12v8_inspector18V8RuntimeAgentImpl21discardConsoleEntriesEv, @function
_ZN12v8_inspector18V8RuntimeAgentImpl21discardConsoleEntriesEv:
.LFB8033:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	32(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rsi), %rax
	movl	16(%rax), %r8d
	movl	%r8d, %esi
	call	_ZN12v8_inspector15V8InspectorImpl27ensureConsoleMessageStorageEi@PLT
	movq	%rax, %rdi
	call	_ZN12v8_inspector23V8ConsoleMessageStorage5clearEv@PLT
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L255
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L255:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8033:
	.size	_ZN12v8_inspector18V8RuntimeAgentImpl21discardConsoleEntriesEv, .-_ZN12v8_inspector18V8RuntimeAgentImpl21discardConsoleEntriesEv
	.section	.text._ZN12v8_inspector18V8RuntimeAgentImpl12getHeapUsageEPdS1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector18V8RuntimeAgentImpl12getHeapUsageEPdS1_
	.type	_ZN12v8_inspector18V8RuntimeAgentImpl12getHeapUsageEPdS1_, @function
_ZN12v8_inspector18V8RuntimeAgentImpl12getHeapUsageEPdS1_:
.LFB8043:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-160(%rbp), %r15
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r15, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v814HeapStatisticsC1Ev@PLT
	movq	32(%r14), %rax
	movq	%r15, %rsi
	movq	8(%rax), %rdi
	call	_ZN2v87Isolate17GetHeapStatisticsEPNS_14HeapStatisticsE@PLT
	movq	-128(%rbp), %rsi
	testq	%rsi, %rsi
	js	.L257
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rsi, %xmm0
.L258:
	movq	-160(%rbp), %rdx
	movsd	%xmm0, 0(%r13)
	testq	%rdx, %rdx
	js	.L259
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
.L260:
	movsd	%xmm0, (%rbx)
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L263
	addq	$120, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L257:
	.cfi_restore_state
	movq	%rsi, %rax
	andl	$1, %esi
	pxor	%xmm0, %xmm0
	shrq	%rax
	orq	%rsi, %rax
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L259:
	movq	%rdx, %rax
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rax
	orq	%rdx, %rax
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L260
.L263:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8043:
	.size	_ZN12v8_inspector18V8RuntimeAgentImpl12getHeapUsageEPdS1_, .-_ZN12v8_inspector18V8RuntimeAgentImpl12getHeapUsageEPdS1_
	.section	.text._ZN12v8_inspector18V8RuntimeAgentImpl18terminateExecutionESt10unique_ptrINS_8protocol7Runtime7Backend26TerminateExecutionCallbackESt14default_deleteIS5_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector18V8RuntimeAgentImpl18terminateExecutionESt10unique_ptrINS_8protocol7Runtime7Backend26TerminateExecutionCallbackESt14default_deleteIS5_EE
	.type	_ZN12v8_inspector18V8RuntimeAgentImpl18terminateExecutionESt10unique_ptrINS_8protocol7Runtime7Backend26TerminateExecutionCallbackESt14default_deleteIS5_EE, @function
_ZN12v8_inspector18V8RuntimeAgentImpl18terminateExecutionESt10unique_ptrINS_8protocol7Runtime7Backend26TerminateExecutionCallbackESt14default_deleteIS5_EE:
.LFB8044:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rax
	movq	24(%rax), %rdi
	movq	(%rsi), %rax
	movq	$0, (%rsi)
	leaq	-16(%rbp), %rsi
	movq	%rax, -16(%rbp)
	call	_ZN12v8_inspector10V8Debugger18terminateExecutionESt10unique_ptrINS_8protocol7Runtime7Backend26TerminateExecutionCallbackESt14default_deleteIS5_EE@PLT
	movq	-16(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L264
	movq	(%rdi), %rax
	call	*32(%rax)
.L264:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L271
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L271:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8044:
	.size	_ZN12v8_inspector18V8RuntimeAgentImpl18terminateExecutionESt10unique_ptrINS_8protocol7Runtime7Backend26TerminateExecutionCallbackESt14default_deleteIS5_EE, .-_ZN12v8_inspector18V8RuntimeAgentImpl18terminateExecutionESt10unique_ptrINS_8protocol7Runtime7Backend26TerminateExecutionCallbackESt14default_deleteIS5_EE
	.section	.rodata._ZN12v8_inspector18V8RuntimeAgentImpl15bindingCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"Invalid arguments: should be exactly one string."
	.section	.text._ZN12v8_inspector18V8RuntimeAgentImpl15bindingCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector18V8RuntimeAgentImpl15bindingCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN12v8_inspector18V8RuntimeAgentImpl15bindingCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN12v8_inspector18V8RuntimeAgentImpl15bindingCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB8051:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	cmpl	$1, 16(%rdi)
	movq	8(%rax), %r13
	jne	.L273
	movq	8(%rdi), %rax
	movq	%rdi, %rbx
	movq	(%rax), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	je	.L289
.L273:
	leaq	-96(%rbp), %r12
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L272
.L288:
	call	_ZdlPv@PLT
.L272:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L290
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L289:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L273
	movq	%r13, %rdi
	leaq	-144(%rbp), %r15
	call	_ZN2v85debug12GetInspectorEPNS_7IsolateE@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rax, %rdi
	call	_ZN12v8_inspector16InspectedContext9contextIdEN2v85LocalINS1_7ContextEEE@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	movl	%eax, -180(%rbp)
	call	_ZNK12v8_inspector15V8InspectorImpl14contextGroupIdEi@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	movl	%eax, %r14d
	movq	(%rbx), %rax
	leaq	32(%rax), %rdx
	call	_ZN12v8_inspector16toProtocolStringEPN2v87IsolateENS0_5LocalINS0_6StringEEE@PLT
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L291
	movq	(%rbx), %rax
	movq	8(%rax), %rdx
	addq	$88, %rdx
.L278:
	leaq	-96(%rbp), %rdi
	movq	%r15, %xmm0
	movq	%r13, %rsi
	movq	%rdi, %xmm1
	leaq	-176(%rbp), %r13
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -208(%rbp)
	call	_ZN12v8_inspector16toProtocolStringEPN2v87IsolateENS0_5LocalINS0_6StringEEE@PLT
	movl	$24, %edi
	movq	$0, -160(%rbp)
	call	_Znwm@PLT
	movdqa	-208(%rbp), %xmm0
	movl	%r14d, %esi
	movq	%r12, %rdi
	leaq	-180(%rbp), %rdx
	movq	%rax, -176(%rbp)
	leaq	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector18V8RuntimeAgentImpl15bindingCallbackERKN2v820FunctionCallbackInfoINS3_5ValueEEEEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKSD_St18_Manager_operation(%rip), %rcx
	movq	%rdx, 16(%rax)
	movq	%r13, %rdx
	movups	%xmm0, (%rax)
	leaq	_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_18V8RuntimeAgentImpl15bindingCallbackERKN2v820FunctionCallbackInfoINS5_5ValueEEEEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_(%rip), %rax
	movq	%rcx, %xmm0
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -160(%rbp)
	call	_ZN12v8_inspector15V8InspectorImpl14forEachSessionEiRKSt8functionIFvPNS_22V8InspectorSessionImplEEE@PLT
	movq	-160(%rbp), %rax
	testq	%rax, %rax
	je	.L279
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	*%rax
.L279:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L280
	call	_ZdlPv@PLT
.L280:
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L288
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L291:
	movq	8(%rbx), %rdx
	jmp	.L278
.L290:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8051:
	.size	_ZN12v8_inspector18V8RuntimeAgentImpl15bindingCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN12v8_inspector18V8RuntimeAgentImpl15bindingCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.text._ZN12v8_inspector18V8RuntimeAgentImpl13removeBindingERKNS_8String16E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector18V8RuntimeAgentImpl13removeBindingERKNS_8String16E
	.type	_ZN12v8_inspector18V8RuntimeAgentImpl13removeBindingERKNS_8String16E, @function
_ZN12v8_inspector18V8RuntimeAgentImpl13removeBindingERKNS_8String16E:
.LFB8057:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-80(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$48, %rsp
	movq	16(%rsi), %r15
	leaq	_ZN12v8_inspector23V8RuntimeAgentImplStateL8bindingsE(%rip), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue9getObjectERKNS_8String16E@PLT
	movq	-80(%rbp), %rdi
	movq	%rax, %r13
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L293
	call	_ZdlPv@PLT
.L293:
	testq	%r13, %r13
	je	.L298
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue6removeERKNS_8String16E@PLT
.L298:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L299
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L299:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8057:
	.size	_ZN12v8_inspector18V8RuntimeAgentImpl13removeBindingERKNS_8String16E, .-_ZN12v8_inspector18V8RuntimeAgentImpl13removeBindingERKNS_8String16E
	.section	.text._ZNSt17_Function_handlerIFvPN12v8_inspector16InspectedContextEEZNS0_18V8RuntimeAgentImpl5resetEvEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvPN12v8_inspector16InspectedContextEEZNS0_18V8RuntimeAgentImpl5resetEvEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_, @function
_ZNSt17_Function_handlerIFvPN12v8_inspector16InspectedContextEEZNS0_18V8RuntimeAgentImpl5resetEvEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_:
.LFB11018:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	%rsi, %r8
	xorl	%edx, %edx
	movq	(%r8), %rdi
	movl	(%rax), %esi
	jmp	_ZN12v8_inspector16InspectedContext11setReportedEib@PLT
	.cfi_endproc
.LFE11018:
	.size	_ZNSt17_Function_handlerIFvPN12v8_inspector16InspectedContextEEZNS0_18V8RuntimeAgentImpl5resetEvEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_, .-_ZNSt17_Function_handlerIFvPN12v8_inspector16InspectedContextEEZNS0_18V8RuntimeAgentImpl5resetEvEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_
	.section	.text._ZN12v8_inspector8protocol7Runtime27ExecutionContextDescription17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime27ExecutionContextDescription17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime27ExecutionContextDescription17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol7Runtime27ExecutionContextDescription17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol7Runtime27ExecutionContextDescription17serializeToBinaryEv:
.LFB4981:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime27ExecutionContextDescription7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L301
	movq	(%rdi), %rax
	call	*24(%rax)
.L301:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L308
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L308:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4981:
	.size	_ZN12v8_inspector8protocol7Runtime27ExecutionContextDescription17serializeToBinaryEv, .-_ZN12v8_inspector8protocol7Runtime27ExecutionContextDescription17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol7Runtime27ExecutionContextDescription15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime27ExecutionContextDescription15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime27ExecutionContextDescription15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol7Runtime27ExecutionContextDescription15serializeToJSONEv, @function
_ZN12v8_inspector8protocol7Runtime27ExecutionContextDescription15serializeToJSONEv:
.LFB4980:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime27ExecutionContextDescription7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L309
	movq	(%rdi), %rax
	call	*24(%rax)
.L309:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L316
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L316:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4980:
	.size	_ZN12v8_inspector8protocol7Runtime27ExecutionContextDescription15serializeToJSONEv, .-_ZN12v8_inspector8protocol7Runtime27ExecutionContextDescription15serializeToJSONEv
	.section	.text._ZNSt14_Function_base13_Base_managerIZN12v8_inspector18V8RuntimeAgentImpl15bindingCallbackERKN2v820FunctionCallbackInfoINS3_5ValueEEEEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKSD_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector18V8RuntimeAgentImpl15bindingCallbackERKN2v820FunctionCallbackInfoINS3_5ValueEEEEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKSD_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN12v8_inspector18V8RuntimeAgentImpl15bindingCallbackERKN2v820FunctionCallbackInfoINS3_5ValueEEEEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKSD_St18_Manager_operation:
.LFB11004:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	cmpl	$2, %edx
	je	.L318
	cmpl	$3, %edx
	je	.L319
	cmpl	$1, %edx
	je	.L325
.L320:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L318:
	.cfi_restore_state
	movq	(%rsi), %r12
	movl	$24, %edi
	call	_Znwm@PLT
	movdqu	(%r12), %xmm0
	movups	%xmm0, (%rax)
	movq	16(%r12), %rdx
	movq	%rax, (%rbx)
	movq	%rdx, 16(%rax)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L325:
	.cfi_restore_state
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L319:
	.cfi_restore_state
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L320
	movl	$24, %esi
	call	_ZdlPvm@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11004:
	.size	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector18V8RuntimeAgentImpl15bindingCallbackERKN2v820FunctionCallbackInfoINS3_5ValueEEEEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKSD_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN12v8_inspector18V8RuntimeAgentImpl15bindingCallbackERKN2v820FunctionCallbackInfoINS3_5ValueEEEEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKSD_St18_Manager_operation
	.section	.text._ZN12v8_inspector18V8RuntimeAgentImpl23runIfWaitingForDebuggerEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector18V8RuntimeAgentImpl23runIfWaitingForDebuggerEv
	.type	_ZN12v8_inspector18V8RuntimeAgentImpl23runIfWaitingForDebuggerEv, @function
_ZN12v8_inspector18V8RuntimeAgentImpl23runIfWaitingForDebuggerEv:
.LFB8030:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN12v8_inspector17V8InspectorClient23runIfWaitingForDebuggerEi(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	32(%rsi), %rax
	movq	16(%rax), %rdi
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L330
.L327:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L331
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L330:
	.cfi_restore_state
	movq	8(%rsi), %rdx
	movl	16(%rdx), %esi
	call	*%rax
	jmp	.L327
.L331:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8030:
	.size	_ZN12v8_inspector18V8RuntimeAgentImpl23runIfWaitingForDebuggerEv, .-_ZN12v8_inspector18V8RuntimeAgentImpl23runIfWaitingForDebuggerEv
	.section	.rodata._ZN12v8_inspector12_GLOBAL__N_113ensureContextEPNS_15V8InspectorImplEiN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIiEEPi.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"Cannot find default execution context"
	.section	.text._ZN12v8_inspector12_GLOBAL__N_113ensureContextEPNS_15V8InspectorImplEiN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIiEEPi,"ax",@progbits
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_113ensureContextEPNS_15V8InspectorImplEiN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIiEEPi, @function
_ZN12v8_inspector12_GLOBAL__N_113ensureContextEPNS_15V8InspectorImplEiN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIiEEPi:
.LFB7925:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, (%rcx)
	je	.L333
	movl	4(%rcx), %eax
	movl	%eax, (%r8)
.L334:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
.L332:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L347
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L333:
	.cfi_restore_state
	movq	%rsi, %r13
	leaq	-128(%rbp), %r15
	movq	8(%rsi), %rsi
	movl	%edx, %r14d
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	16(%r13), %rdi
	leaq	_ZN12v8_inspector17V8InspectorClient27ensureDefaultContextInGroupEi(%rip), %rdx
	movq	(%rdi), %rax
	movq	96(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L335
.L338:
	leaq	-96(%rbp), %r13
	leaq	.LC3(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L337
	call	_ZdlPv@PLT
.L337:
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L332
	.p2align 4,,10
	.p2align 3
.L335:
	movl	%r14d, %esi
	call	*%rax
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L338
	call	_ZN12v8_inspector16InspectedContext9contextIdEN2v85LocalINS1_7ContextEEE@PLT
	movq	%r15, %rdi
	movl	%eax, (%rbx)
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L334
.L347:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7925:
	.size	_ZN12v8_inspector12_GLOBAL__N_113ensureContextEPNS_15V8InspectorImplEiN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIiEEPi, .-_ZN12v8_inspector12_GLOBAL__N_113ensureContextEPNS_15V8InspectorImplEiN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIiEEPi
	.section	.text._ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13CustomPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev, @function
_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev:
.LFB4608:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	56(%rdi), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L349
	call	_ZdlPv@PLT
.L349:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L350
	call	_ZdlPv@PLT
.L350:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$96, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4608:
	.size	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev, .-_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime9CallFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev, @function
_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev:
.LFB5121:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	88(%rdi), %rdi
	leaq	104(%r12), %rax
	cmpq	%rax, %rdi
	je	.L353
	call	_ZdlPv@PLT
.L353:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L354
	call	_ZdlPv@PLT
.L354:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L355
	call	_ZdlPv@PLT
.L355:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$136, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5121:
	.size	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev, .-_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12StackTraceIdD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev, @function
_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev:
.LFB5239:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rax
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	80(%r12), %rax
	subq	$8, %rsp
	movups	%xmm0, (%rdi)
	movq	64(%rdi), %rdi
	cmpq	%rax, %rdi
	je	.L358
	call	_ZdlPv@PLT
.L358:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L359
	call	_ZdlPv@PLT
.L359:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$104, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5239:
	.size	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev, .-_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime12CallArgumentD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12CallArgumentD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12CallArgumentD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime12CallArgumentD0Ev, @function
_ZN12v8_inspector8protocol7Runtime12CallArgumentD0Ev:
.LFB4932:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12CallArgumentE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	72(%rdi), %rdi
	leaq	88(%r12), %rax
	cmpq	%rax, %rdi
	je	.L362
	call	_ZdlPv@PLT
.L362:
	movq	24(%r12), %rdi
	leaq	40(%r12), %rax
	cmpq	%rax, %rdi
	je	.L363
	call	_ZdlPv@PLT
.L363:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L364
	movq	(%rdi), %rax
	call	*24(%rax)
.L364:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$112, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4932:
	.size	_ZN12v8_inspector8protocol7Runtime12CallArgumentD0Ev, .-_ZN12v8_inspector8protocol7Runtime12CallArgumentD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime27ExecutionContextDescriptionD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime27ExecutionContextDescriptionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime27ExecutionContextDescriptionD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime27ExecutionContextDescriptionD0Ev, @function
_ZN12v8_inspector8protocol7Runtime27ExecutionContextDescriptionD0Ev:
.LFB4969:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime27ExecutionContextDescriptionE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	96(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L370
	movq	(%rdi), %rax
	call	*24(%rax)
.L370:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L371
	call	_ZdlPv@PLT
.L371:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L372
	call	_ZdlPv@PLT
.L372:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$104, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4969:
	.size	_ZN12v8_inspector8protocol7Runtime27ExecutionContextDescriptionD0Ev, .-_ZN12v8_inspector8protocol7Runtime27ExecutionContextDescriptionD0Ev
	.section	.text._ZN12v8_inspector18V8RuntimeAgentImplD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector18V8RuntimeAgentImplD2Ev
	.type	_ZN12v8_inspector18V8RuntimeAgentImplD2Ev, @function
_ZN12v8_inspector18V8RuntimeAgentImplD2Ev:
.LFB7954:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector18V8RuntimeAgentImplE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	64(%rdi), %r12
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	testq	%r12, %r12
	jne	.L383
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L401:
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L378
.L382:
	movq	%r13, %r12
.L383:
	movq	48(%r12), %r14
	movq	(%r12), %r13
	testq	%r14, %r14
	je	.L379
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L380
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L380:
	movl	$8, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L379:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	jne	.L401
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L382
.L378:
	movq	56(%rbx), %rax
	movq	48(%rbx), %rdi
	xorl	%esi, %esi
	addq	$96, %rbx
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-48(%rbx), %rdi
	movq	$0, -24(%rbx)
	movq	$0, -32(%rbx)
	cmpq	%rbx, %rdi
	je	.L377
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L377:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7954:
	.size	_ZN12v8_inspector18V8RuntimeAgentImplD2Ev, .-_ZN12v8_inspector18V8RuntimeAgentImplD2Ev
	.globl	_ZN12v8_inspector18V8RuntimeAgentImplD1Ev
	.set	_ZN12v8_inspector18V8RuntimeAgentImplD1Ev,_ZN12v8_inspector18V8RuntimeAgentImplD2Ev
	.section	.text._ZN12v8_inspector18V8RuntimeAgentImplD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector18V8RuntimeAgentImplD0Ev
	.type	_ZN12v8_inspector18V8RuntimeAgentImplD0Ev, @function
_ZN12v8_inspector18V8RuntimeAgentImplD0Ev:
.LFB7956:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector18V8RuntimeAgentImplE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	64(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	jne	.L408
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L426:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L403
.L407:
	movq	%rbx, %r13
.L408:
	movq	48(%r13), %r14
	movq	0(%r13), %rbx
	testq	%r14, %r14
	je	.L404
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L405
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L405:
	movl	$8, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L404:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	jne	.L426
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L407
.L403:
	movq	56(%r12), %rax
	movq	48(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	48(%r12), %rdi
	leaq	96(%r12), %rax
	movq	$0, 72(%r12)
	movq	$0, 64(%r12)
	cmpq	%rax, %rdi
	je	.L409
	call	_ZdlPv@PLT
.L409:
	popq	%rbx
	movq	%r12, %rdi
	movl	$104, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE7956:
	.size	_ZN12v8_inspector18V8RuntimeAgentImplD0Ev, .-_ZN12v8_inspector18V8RuntimeAgentImplD0Ev
	.section	.text._ZN12v8_inspector18V8RuntimeAgentImpl6enableEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector18V8RuntimeAgentImpl6enableEv
	.type	_ZN12v8_inspector18V8RuntimeAgentImpl6enableEv, @function
_ZN12v8_inspector18V8RuntimeAgentImpl6enableEv:
.LFB8065:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -112(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 40(%rsi)
	jne	.L441
	movq	32(%rsi), %rax
	leaq	_ZN12v8_inspector17V8InspectorClient29beginEnsureAllContextsInGroupEi(%rip), %rdx
	movq	%rsi, %r15
	movq	16(%rax), %rdi
	movq	(%rdi), %rax
	movq	104(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L442
.L430:
	movb	$1, 40(%r15)
	movq	16(%r15), %r13
	leaq	-96(%rbp), %r12
	leaq	_ZN12v8_inspector23V8RuntimeAgentImplStateL14runtimeEnabledE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue10setBooleanERKNS_8String16Eb@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L431
	call	_ZdlPv@PLT
.L431:
	movq	32(%r15), %rdi
	leaq	24(%r15), %rbx
	call	_ZN12v8_inspector15V8InspectorImpl28enableStackCapturingIfNeededEv@PLT
	movq	8(%r15), %rdi
	movq	%r15, %rsi
	call	_ZN12v8_inspector22V8InspectorSessionImpl17reportAllContextsEPNS_18V8RuntimeAgentImplE@PLT
	movq	8(%r15), %rax
	movq	32(%r15), %rdi
	movl	16(%rax), %esi
	call	_ZN12v8_inspector15V8InspectorImpl27ensureConsoleMessageStorageEi@PLT
	movq	56(%rax), %r14
	movq	32(%rax), %r8
	movq	48(%rax), %r12
	movq	64(%rax), %r13
	leaq	8(%r14), %rax
	movq	%rax, -104(%rbp)
.L434:
	movq	%r8, %r14
.L433:
	cmpq	%r14, %r13
	je	.L432
	movq	8(%r15), %rdx
	movq	(%r14), %rdi
	xorl	%ecx, %ecx
	movq	%rbx, %rsi
	call	_ZNK12v8_inspector16V8ConsoleMessage16reportToFrontendEPNS_8protocol7Runtime8FrontendEPNS_22V8InspectorSessionImplEb@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol7Runtime8Frontend5flushEv@PLT
	movq	8(%r15), %rax
	movq	32(%r15), %rdi
	movl	16(%rax), %esi
	call	_ZN12v8_inspector15V8InspectorImpl24hasConsoleMessageStorageEi@PLT
	testb	%al, %al
	je	.L432
	addq	$8, %r14
	cmpq	%r14, %r12
	jne	.L433
	movq	-104(%rbp), %rax
	movq	(%rax), %r8
	addq	$8, %rax
	movq	%rax, -104(%rbp)
	leaq	512(%r8), %r12
	jmp	.L434
	.p2align 4,,10
	.p2align 3
.L432:
	movq	-112(%rbp), %rdi
.L441:
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L443
	movq	-112(%rbp), %rax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L442:
	.cfi_restore_state
	movq	8(%rsi), %rdx
	movl	16(%rdx), %esi
	call	*%rax
	jmp	.L430
.L443:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8065:
	.size	_ZN12v8_inspector18V8RuntimeAgentImpl6enableEv, .-_ZN12v8_inspector18V8RuntimeAgentImpl6enableEv
	.section	.text._ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_18V8RuntimeAgentImpl15bindingCallbackERKN2v820FunctionCallbackInfoINS5_5ValueEEEEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_18V8RuntimeAgentImpl15bindingCallbackERKN2v820FunctionCallbackInfoINS5_5ValueEEEEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_, @function
_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_18V8RuntimeAgentImpl15bindingCallbackERKN2v820FunctionCallbackInfoINS5_5ValueEEEEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_:
.LFB11002:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-96(%rbp), %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rdx
	leaq	_ZN12v8_inspector23V8RuntimeAgentImplStateL8bindingsE(%rip), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%r12, %rdi
	movq	184(%rdx), %rbx
	movq	8(%rax), %rcx
	movq	16(%rax), %rdx
	movq	16(%rbx), %r14
	movq	(%rax), %r13
	movq	%rcx, -104(%rbp)
	movl	(%rdx), %r15d
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue9getObjectERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r12
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L445
	call	_ZdlPv@PLT
.L445:
	testq	%r12, %r12
	je	.L444
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	testq	%rax, %rax
	je	.L444
	movq	-104(%rbp), %rdx
	leaq	24(%rbx), %rdi
	movl	%r15d, %ecx
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol7Runtime8Frontend13bindingCalledERKNS_8String16ES5_i@PLT
.L444:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L454
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L454:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11002:
	.size	_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_18V8RuntimeAgentImpl15bindingCallbackERKN2v820FunctionCallbackInfoINS5_5ValueEEEEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_, .-_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_18V8RuntimeAgentImpl15bindingCallbackERKN2v820FunctionCallbackInfoINS5_5ValueEEEEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_
	.section	.text._ZN12v8_inspector8protocol7Runtime13ObjectPreviewD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD2Ev, @function
_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD2Ev:
.LFB4640:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	160(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L456
	movq	8(%r13), %rax
	movq	0(%r13), %r12
	movq	%rax, -64(%rbp)
	cmpq	%r12, %rax
	je	.L457
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %r14
	jmp	.L464
	.p2align 4,,10
	.p2align 3
.L512:
	movq	16(%r15), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%rdi, %rdi
	je	.L460
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%r14, %rax
	jne	.L461
	movq	%rdi, -56(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-56(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L460:
	movq	8(%r15), %rdi
	testq	%rdi, %rdi
	je	.L462
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%r14, %rax
	jne	.L463
	movq	%rdi, -56(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-56(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L462:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L458:
	addq	$8, %r12
	cmpq	%r12, -64(%rbp)
	je	.L511
.L464:
	movq	(%r12), %r15
	testq	%r15, %r15
	je	.L458
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L512
	movq	%r15, %rdi
	addq	$8, %r12
	call	*%rax
	cmpq	%r12, -64(%rbp)
	jne	.L464
	.p2align 4,,10
	.p2align 3
.L511:
	movq	0(%r13), %r12
.L457:
	testq	%r12, %r12
	je	.L465
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L465:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L456:
	movq	152(%rbx), %r13
	testq	%r13, %r13
	je	.L466
	movq	8(%r13), %r14
	movq	0(%r13), %r12
	cmpq	%r12, %r14
	jne	.L476
	testq	%r12, %r12
	je	.L477
	.p2align 4,,10
	.p2align 3
.L515:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L477:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L466:
	movq	104(%rbx), %rdi
	leaq	120(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L478
	call	_ZdlPv@PLT
.L478:
	movq	56(%rbx), %rdi
	leaq	72(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L479
	call	_ZdlPv@PLT
.L479:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L455
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L514:
	.cfi_restore_state
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r15), %rdi
	movq	%rax, (%r15)
	leaq	168(%r15), %rax
	cmpq	%rax, %rdi
	je	.L470
	call	_ZdlPv@PLT
.L470:
	movq	136(%r15), %rdi
	testq	%rdi, %rdi
	je	.L471
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L472
	movq	%rdi, -56(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-56(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L471:
	movq	96(%r15), %rdi
	leaq	112(%r15), %rax
	cmpq	%rax, %rdi
	je	.L473
	call	_ZdlPv@PLT
.L473:
	movq	48(%r15), %rdi
	leaq	64(%r15), %rax
	cmpq	%rax, %rdi
	je	.L474
	call	_ZdlPv@PLT
.L474:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L475
	call	_ZdlPv@PLT
.L475:
	movl	$192, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L468:
	addq	$8, %r12
	cmpq	%r12, %r14
	je	.L513
.L476:
	movq	(%r12), %r15
	testq	%r15, %r15
	je	.L468
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L514
	addq	$8, %r12
	movq	%r15, %rdi
	call	*%rax
	cmpq	%r12, %r14
	jne	.L476
	.p2align 4,,10
	.p2align 3
.L513:
	movq	0(%r13), %r12
	testq	%r12, %r12
	jne	.L515
	jmp	.L477
	.p2align 4,,10
	.p2align 3
.L455:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L463:
	.cfi_restore_state
	call	*%rax
	jmp	.L462
	.p2align 4,,10
	.p2align 3
.L461:
	call	*%rax
	jmp	.L460
	.p2align 4,,10
	.p2align 3
.L472:
	call	*%rax
	jmp	.L471
	.cfi_endproc
.LFE4640:
	.size	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD2Ev, .-_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev,_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD2Ev
	.section	.rodata._ZN12v8_inspector18V8RuntimeAgentImpl12getIsolateIdEPNS_8String16E.str1.1,"aMS",@progbits,1
.LC4:
	.string	"%lx"
	.section	.text._ZN12v8_inspector18V8RuntimeAgentImpl12getIsolateIdEPNS_8String16E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector18V8RuntimeAgentImpl12getIsolateIdEPNS_8String16E
	.type	_ZN12v8_inspector18V8RuntimeAgentImpl12getIsolateIdEPNS_8String16E, @function
_ZN12v8_inspector18V8RuntimeAgentImpl12getIsolateIdEPNS_8String16E:
.LFB8042:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC4(%rip), %r8
	movl	$40, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-80(%rbp), %r13
	movq	%rdi, %r12
	pushq	%rbx
	movq	%r13, %rdi
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	movl	$1, %edx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	32(%rsi), %rax
	movl	$40, %esi
	movq	56(%rax), %r9
	xorl	%eax, %eax
	call	__snprintf_chk@PLT
	leaq	-128(%rbp), %rdi
	movq	%r13, %rsi
	leaq	-112(%rbp), %r13
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-128(%rbp), %rdx
	movq	(%rbx), %rdi
	movq	-120(%rbp), %rax
	cmpq	%r13, %rdx
	je	.L534
	leaq	16(%rbx), %rsi
	movq	-112(%rbp), %rcx
	cmpq	%rsi, %rdi
	je	.L535
	movq	%rax, %xmm0
	movq	%rcx, %xmm1
	movq	16(%rbx), %rsi
	movq	%rdx, (%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%rbx)
	testq	%rdi, %rdi
	je	.L522
	movq	%rdi, -128(%rbp)
	movq	%rsi, -112(%rbp)
.L520:
	xorl	%eax, %eax
	movq	$0, -120(%rbp)
	movw	%ax, (%rdi)
	movq	-96(%rbp), %rax
	movq	-128(%rbp), %rdi
	movq	%rax, 32(%rbx)
	cmpq	%r13, %rdi
	je	.L523
	call	_ZdlPv@PLT
.L523:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L536
	addq	$104, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L535:
	.cfi_restore_state
	movq	%rax, %xmm0
	movq	%rcx, %xmm2
	movq	%rdx, (%rbx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 8(%rbx)
.L522:
	movq	%r13, -128(%rbp)
	leaq	-112(%rbp), %r13
	movq	%r13, %rdi
	jmp	.L520
	.p2align 4,,10
	.p2align 3
.L534:
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.L518
	cmpq	$1, %rax
	je	.L537
	movq	%rax, %rdx
	addq	%rdx, %rdx
	je	.L518
	movq	%r13, %rsi
	call	memmove@PLT
	movq	-120(%rbp), %rax
	movq	(%rbx), %rdi
	leaq	(%rax,%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L518:
	xorl	%ecx, %ecx
	movq	%rax, 8(%rbx)
	movw	%cx, (%rdi,%rdx)
	movq	-128(%rbp), %rdi
	jmp	.L520
	.p2align 4,,10
	.p2align 3
.L537:
	movzwl	-112(%rbp), %eax
	movw	%ax, (%rdi)
	movq	-120(%rbp), %rax
	movq	(%rbx), %rdi
	leaq	(%rax,%rax), %rdx
	jmp	.L518
.L536:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8042:
	.size	_ZN12v8_inspector18V8RuntimeAgentImpl12getIsolateIdEPNS_8String16E, .-_ZN12v8_inspector18V8RuntimeAgentImpl12getIsolateIdEPNS_8String16E
	.section	.text._ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev, @function
_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev:
.LFB4715:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%rax, (%rdi)
	movq	152(%rdi), %rdi
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L539
	call	_ZdlPv@PLT
.L539:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L540
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rbx
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L541
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -64(%rbp)
	testq	%rax, %rax
	je	.L542
	movq	8(%rax), %rdx
	movq	(%rax), %r14
	movq	%rdx, -56(%rbp)
	cmpq	%r14, %rdx
	jne	.L550
	jmp	.L543
	.p2align 4,,10
	.p2align 3
.L604:
	movq	16(%r15), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%rdi, %rdi
	je	.L546
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L547
	movq	%rdi, -72(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L546:
	movq	8(%r15), %rdi
	testq	%rdi, %rdi
	je	.L548
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L549
	movq	%rdi, -72(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L548:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L544:
	addq	$8, %r14
	cmpq	%r14, -56(%rbp)
	je	.L603
.L550:
	movq	(%r14), %r15
	testq	%r15, %r15
	je	.L544
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L604
	movq	%r15, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -56(%rbp)
	jne	.L550
	.p2align 4,,10
	.p2align 3
.L603:
	movq	-64(%rbp), %rax
	movq	(%rax), %r14
.L543:
	testq	%r14, %r14
	je	.L551
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L551:
	movq	-64(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L542:
	movq	152(%r13), %rax
	movq	%rax, -64(%rbp)
	testq	%rax, %rax
	je	.L552
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	movq	%rcx, -56(%rbp)
	cmpq	%r14, %rcx
	jne	.L562
	jmp	.L553
	.p2align 4,,10
	.p2align 3
.L606:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r15), %rdi
	movq	%rax, (%r15)
	leaq	168(%r15), %rax
	cmpq	%rax, %rdi
	je	.L556
	call	_ZdlPv@PLT
.L556:
	movq	136(%r15), %rdi
	testq	%rdi, %rdi
	je	.L557
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L558
	movq	%rdi, -72(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L557:
	movq	96(%r15), %rdi
	leaq	112(%r15), %rax
	cmpq	%rax, %rdi
	je	.L559
	call	_ZdlPv@PLT
.L559:
	movq	48(%r15), %rdi
	leaq	64(%r15), %rax
	cmpq	%rax, %rdi
	je	.L560
	call	_ZdlPv@PLT
.L560:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L561
	call	_ZdlPv@PLT
.L561:
	movl	$192, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L554:
	addq	$8, %r14
	cmpq	%r14, -56(%rbp)
	je	.L605
.L562:
	movq	(%r14), %r15
	testq	%r15, %r15
	je	.L554
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L606
	movq	%r15, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -56(%rbp)
	jne	.L562
	.p2align 4,,10
	.p2align 3
.L605:
	movq	-64(%rbp), %rax
	movq	(%rax), %r14
.L553:
	testq	%r14, %r14
	je	.L563
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L563:
	movq	-64(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L552:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L564
	call	_ZdlPv@PLT
.L564:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L565
	call	_ZdlPv@PLT
.L565:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L566
	call	_ZdlPv@PLT
.L566:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L540:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L567
	call	_ZdlPv@PLT
.L567:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L568
	call	_ZdlPv@PLT
.L568:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L569
	call	_ZdlPv@PLT
.L569:
	addq	$40, %rsp
	movq	%r12, %rdi
	movl	$192, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L541:
	.cfi_restore_state
	movq	%r13, %rdi
	call	*%rax
	jmp	.L540
	.p2align 4,,10
	.p2align 3
.L558:
	call	*%rax
	jmp	.L557
	.p2align 4,,10
	.p2align 3
.L549:
	call	*%rax
	jmp	.L548
	.p2align 4,,10
	.p2align 3
.L547:
	call	*%rax
	jmp	.L546
	.cfi_endproc
.LFE4715:
	.size	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev, .-_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12EntryPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev, @function
_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev:
.LFB4755:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r12
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L608
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rbx
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L609
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	movq	160(%r12), %rax
	movq	%rax, -64(%rbp)
	testq	%rax, %rax
	je	.L610
	movq	8(%rax), %rdx
	movq	(%rax), %r14
	movq	%rdx, -56(%rbp)
	cmpq	%r14, %rdx
	jne	.L618
	jmp	.L611
	.p2align 4,,10
	.p2align 3
.L728:
	movq	16(%r15), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%rdi, %rdi
	je	.L614
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L615
	movq	%rdi, -72(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L614:
	movq	8(%r15), %rdi
	testq	%rdi, %rdi
	je	.L616
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L617
	movq	%rdi, -72(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L616:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L612:
	addq	$8, %r14
	cmpq	%r14, -56(%rbp)
	je	.L727
.L618:
	movq	(%r14), %r15
	testq	%r15, %r15
	je	.L612
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L728
	movq	%r15, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -56(%rbp)
	jne	.L618
	.p2align 4,,10
	.p2align 3
.L727:
	movq	-64(%rbp), %rax
	movq	(%rax), %r14
.L611:
	testq	%r14, %r14
	je	.L619
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L619:
	movq	-64(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L610:
	movq	152(%r12), %rax
	movq	%rax, -64(%rbp)
	testq	%rax, %rax
	je	.L620
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	movq	%rcx, -56(%rbp)
	cmpq	%r14, %rcx
	jne	.L630
	jmp	.L621
	.p2align 4,,10
	.p2align 3
.L730:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r15), %rdi
	movq	%rax, (%r15)
	leaq	168(%r15), %rax
	cmpq	%rax, %rdi
	je	.L624
	call	_ZdlPv@PLT
.L624:
	movq	136(%r15), %rdi
	testq	%rdi, %rdi
	je	.L625
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L626
	movq	%rdi, -72(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L625:
	movq	96(%r15), %rdi
	leaq	112(%r15), %rax
	cmpq	%rax, %rdi
	je	.L627
	call	_ZdlPv@PLT
.L627:
	movq	48(%r15), %rdi
	leaq	64(%r15), %rax
	cmpq	%rax, %rdi
	je	.L628
	call	_ZdlPv@PLT
.L628:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L629
	call	_ZdlPv@PLT
.L629:
	movl	$192, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L622:
	addq	$8, %r14
	cmpq	%r14, -56(%rbp)
	je	.L729
.L630:
	movq	(%r14), %r15
	testq	%r15, %r15
	je	.L622
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L730
	movq	%r15, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -56(%rbp)
	jne	.L630
	.p2align 4,,10
	.p2align 3
.L729:
	movq	-64(%rbp), %rax
	movq	(%rax), %r14
.L621:
	testq	%r14, %r14
	je	.L631
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L631:
	movq	-64(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L620:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L632
	call	_ZdlPv@PLT
.L632:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L633
	call	_ZdlPv@PLT
.L633:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L634
	call	_ZdlPv@PLT
.L634:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L608:
	movq	8(%r13), %r12
	testq	%r12, %r12
	je	.L635
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rbx
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L636
	movq	160(%r12), %r15
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r15, %r15
	je	.L637
	movq	8(%r15), %rax
	movq	(%r15), %r14
	movq	%rax, -56(%rbp)
	cmpq	%r14, %rax
	jne	.L645
	jmp	.L638
	.p2align 4,,10
	.p2align 3
.L732:
	movq	16(%r8), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r8)
	testq	%rdi, %rdi
	je	.L641
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L642
	movq	%r8, -72(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-64(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
	movq	-72(%rbp), %r8
.L641:
	movq	8(%r8), %rdi
	testq	%rdi, %rdi
	je	.L643
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L644
	movq	%r8, -72(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-64(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
	movq	-72(%rbp), %r8
.L643:
	movl	$24, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L639:
	addq	$8, %r14
	cmpq	%r14, -56(%rbp)
	je	.L731
.L645:
	movq	(%r14), %r8
	testq	%r8, %r8
	je	.L639
	movq	(%r8), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L732
	movq	%r8, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -56(%rbp)
	jne	.L645
	.p2align 4,,10
	.p2align 3
.L731:
	movq	(%r15), %r14
.L638:
	testq	%r14, %r14
	je	.L646
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L646:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L637:
	movq	152(%r12), %rax
	movq	%rax, -64(%rbp)
	testq	%rax, %rax
	je	.L647
	movq	8(%rax), %rdx
	movq	(%rax), %r14
	movq	%rdx, -56(%rbp)
	cmpq	%r14, %rdx
	jne	.L657
	jmp	.L648
	.p2align 4,,10
	.p2align 3
.L734:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r15), %rdi
	movq	%rax, (%r15)
	leaq	168(%r15), %rax
	cmpq	%rax, %rdi
	je	.L651
	call	_ZdlPv@PLT
.L651:
	movq	136(%r15), %rdi
	testq	%rdi, %rdi
	je	.L652
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L653
	movq	%rdi, -72(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L652:
	movq	96(%r15), %rdi
	leaq	112(%r15), %rax
	cmpq	%rax, %rdi
	je	.L654
	call	_ZdlPv@PLT
.L654:
	movq	48(%r15), %rdi
	leaq	64(%r15), %rax
	cmpq	%rax, %rdi
	je	.L655
	call	_ZdlPv@PLT
.L655:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L656
	call	_ZdlPv@PLT
.L656:
	movl	$192, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L649:
	addq	$8, %r14
	cmpq	%r14, -56(%rbp)
	je	.L733
.L657:
	movq	(%r14), %r15
	testq	%r15, %r15
	je	.L649
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L734
	movq	%r15, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -56(%rbp)
	jne	.L657
	.p2align 4,,10
	.p2align 3
.L733:
	movq	-64(%rbp), %rax
	movq	(%rax), %r14
.L648:
	testq	%r14, %r14
	je	.L658
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L658:
	movq	-64(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L647:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L659
	call	_ZdlPv@PLT
.L659:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L660
	call	_ZdlPv@PLT
.L660:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L661
	call	_ZdlPv@PLT
.L661:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L635:
	addq	$40, %rsp
	movq	%r13, %rdi
	movl	$24, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L636:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L609:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L608
	.p2align 4,,10
	.p2align 3
.L615:
	call	*%rax
	jmp	.L614
	.p2align 4,,10
	.p2align 3
.L626:
	call	*%rax
	jmp	.L625
	.p2align 4,,10
	.p2align 3
.L653:
	call	*%rax
	jmp	.L652
	.p2align 4,,10
	.p2align 3
.L642:
	movq	%r8, -64(%rbp)
	call	*%rax
	movq	-64(%rbp), %r8
	jmp	.L641
	.p2align 4,,10
	.p2align 3
.L617:
	call	*%rax
	jmp	.L616
	.p2align 4,,10
	.p2align 3
.L644:
	movq	%r8, -64(%rbp)
	call	*%rax
	movq	-64(%rbp), %r8
	jmp	.L643
	.cfi_endproc
.LFE4755:
	.size	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev, .-_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev, @function
_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev:
.LFB4642:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	160(%rdi), %rax
	movq	%rdi, -64(%rbp)
	movq	%rcx, (%rdi)
	movq	%rax, -96(%rbp)
	testq	%rax, %rax
	je	.L736
	movq	8(%rax), %rbx
	movq	(%rax), %rax
	movq	%rbx, -112(%rbp)
	movq	%rax, -72(%rbp)
	cmpq	%rax, %rbx
	je	.L737
	.p2align 4,,10
	.p2align 3
.L944:
	movq	-72(%rbp), %rax
	movq	(%rax), %rcx
	movq	%rcx, -88(%rbp)
	testq	%rcx, %rcx
	je	.L738
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L739
	movq	16(%rcx), %rbx
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%rcx)
	testq	%rbx, %rbx
	je	.L740
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%rcx, -56(%rbp)
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L741
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rbx)
	movq	160(%rbx), %rax
	movq	%rax, -104(%rbp)
	testq	%rax, %rax
	je	.L742
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -120(%rbp)
	movq	%rax, -80(%rbp)
	cmpq	%rax, %rdx
	je	.L743
	movq	%rbx, -128(%rbp)
	.p2align 4,,10
	.p2align 3
.L800:
	movq	-80(%rbp), %rax
	movq	(%rax), %r13
	testq	%r13, %r13
	je	.L744
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L745
	movq	16(%r13), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r12, %r12
	je	.L746
	movq	(%r12), %rax
	movq	-56(%rbp), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L747
	movq	160(%r12), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r14, %r14
	je	.L748
	movq	8(%r14), %r15
	movq	(%r14), %rbx
	cmpq	%rbx, %r15
	je	.L749
	movq	%r12, -144(%rbp)
	movq	%rbx, %r12
	movq	%rsi, %rbx
	movq	%r13, -136(%rbp)
	movq	%r14, -152(%rbp)
	jmp	.L756
	.p2align 4,,10
	.p2align 3
.L1440:
	movq	16(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L752
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L753
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L752:
	movq	8(%r13), %r14
	testq	%r14, %r14
	je	.L754
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L755
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L754:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L750:
	addq	$8, %r12
	cmpq	%r12, %r15
	je	.L1439
.L756:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L750
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L1440
	addq	$8, %r12
	movq	%r13, %rdi
	call	*%rdx
	cmpq	%r12, %r15
	jne	.L756
	.p2align 4,,10
	.p2align 3
.L1439:
	movq	-152(%rbp), %r14
	movq	-136(%rbp), %r13
	movq	-144(%rbp), %r12
	movq	(%r14), %rbx
.L749:
	testq	%rbx, %rbx
	je	.L757
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L757:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L748:
	movq	152(%r12), %r15
	testq	%r15, %r15
	je	.L758
	movq	8(%r15), %rbx
	movq	(%r15), %r14
	cmpq	%r14, %rbx
	je	.L759
	movq	%r13, -136(%rbp)
	movq	%r12, -144(%rbp)
	jmp	.L768
	.p2align 4,,10
	.p2align 3
.L1442:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L762
	call	_ZdlPv@PLT
.L762:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L763
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L764
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L763:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L765
	call	_ZdlPv@PLT
.L765:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L766
	call	_ZdlPv@PLT
.L766:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L767
	call	_ZdlPv@PLT
.L767:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L760:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L1441
.L768:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L760
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L1442
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %rbx
	jne	.L768
	.p2align 4,,10
	.p2align 3
.L1441:
	movq	-136(%rbp), %r13
	movq	-144(%rbp), %r12
	movq	(%r15), %r14
.L759:
	testq	%r14, %r14
	je	.L769
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L769:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L758:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L770
	call	_ZdlPv@PLT
.L770:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L771
	call	_ZdlPv@PLT
.L771:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L772
	call	_ZdlPv@PLT
.L772:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L746:
	movq	8(%r13), %r12
	testq	%r12, %r12
	je	.L773
	movq	(%r12), %rax
	movq	-56(%rbp), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L774
	movq	160(%r12), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r14, %r14
	je	.L775
	movq	8(%r14), %r15
	movq	(%r14), %rbx
	cmpq	%rbx, %r15
	je	.L776
	movq	%r12, -144(%rbp)
	movq	%rbx, %r12
	movq	%rcx, %rbx
	movq	%r13, -136(%rbp)
	movq	%r14, -152(%rbp)
	jmp	.L783
	.p2align 4,,10
	.p2align 3
.L1444:
	movq	16(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L779
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L780
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L779:
	movq	8(%r13), %r14
	testq	%r14, %r14
	je	.L781
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L782
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L781:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L777:
	addq	$8, %r12
	cmpq	%r12, %r15
	je	.L1443
.L783:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L777
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L1444
	addq	$8, %r12
	movq	%r13, %rdi
	call	*%rdx
	cmpq	%r12, %r15
	jne	.L783
	.p2align 4,,10
	.p2align 3
.L1443:
	movq	-152(%rbp), %r14
	movq	-136(%rbp), %r13
	movq	-144(%rbp), %r12
	movq	(%r14), %rbx
.L776:
	testq	%rbx, %rbx
	je	.L784
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L784:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L775:
	movq	152(%r12), %r15
	testq	%r15, %r15
	je	.L785
	movq	8(%r15), %rbx
	movq	(%r15), %r14
	cmpq	%r14, %rbx
	je	.L786
	movq	%r13, -136(%rbp)
	movq	%r12, -144(%rbp)
	jmp	.L795
	.p2align 4,,10
	.p2align 3
.L1446:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L789
	call	_ZdlPv@PLT
.L789:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L790
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L791
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L790:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L792
	call	_ZdlPv@PLT
.L792:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L793
	call	_ZdlPv@PLT
.L793:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L794
	call	_ZdlPv@PLT
.L794:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L787:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L1445
.L795:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L787
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1446
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %rbx
	jne	.L795
	.p2align 4,,10
	.p2align 3
.L1445:
	movq	-136(%rbp), %r13
	movq	-144(%rbp), %r12
	movq	(%r15), %r14
.L786:
	testq	%r14, %r14
	je	.L796
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L796:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L785:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L797
	call	_ZdlPv@PLT
.L797:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L798
	call	_ZdlPv@PLT
.L798:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L799
	call	_ZdlPv@PLT
.L799:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L773:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L744:
	addq	$8, -80(%rbp)
	movq	-80(%rbp), %rax
	cmpq	%rax, -120(%rbp)
	jne	.L800
	movq	-104(%rbp), %rax
	movq	-128(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, -80(%rbp)
.L743:
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L801
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L801:
	movq	-104(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L742:
	movq	152(%rbx), %rax
	movq	%rax, -80(%rbp)
	testq	%rax, %rax
	je	.L802
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -104(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rcx
	je	.L803
	movq	%rbx, -128(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L837:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L804
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L805
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L806
	call	_ZdlPv@PLT
.L806:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L807
	movq	0(%r13), %rax
	movq	-56(%rbp), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L808
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L809
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L810
	movq	%rbx, -144(%rbp)
	movq	%r14, %rbx
	movq	%rdx, %r14
	movq	%r12, -136(%rbp)
	movq	%r13, -152(%rbp)
	jmp	.L817
	.p2align 4,,10
	.p2align 3
.L1448:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L813
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%r14, %rdx
	jne	.L814
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L813:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L815
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%r14, %rdx
	jne	.L816
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L815:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L811:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L1447
.L817:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L811
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L1448
	addq	$8, %rbx
	movq	%r12, %rdi
	call	*%rdx
	cmpq	%rbx, %r15
	jne	.L817
	.p2align 4,,10
	.p2align 3
.L1447:
	movq	-120(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %rbx
	movq	-152(%rbp), %r13
	movq	(%rax), %r14
.L810:
	testq	%r14, %r14
	je	.L818
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L818:
	movq	-120(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L809:
	movq	152(%r13), %rax
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L819
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L820
	movq	%r12, -136(%rbp)
	movq	%r13, -144(%rbp)
	jmp	.L829
	.p2align 4,,10
	.p2align 3
.L1450:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L823
	call	_ZdlPv@PLT
.L823:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L824
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L825
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L824:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L826
	call	_ZdlPv@PLT
.L826:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L827
	call	_ZdlPv@PLT
.L827:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L828
	call	_ZdlPv@PLT
.L828:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L821:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L1449
.L829:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L821
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1450
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %r15
	jne	.L829
	.p2align 4,,10
	.p2align 3
.L1449:
	movq	-120(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %r13
	movq	(%rax), %r14
.L820:
	testq	%r14, %r14
	je	.L830
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L830:
	movq	-120(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L819:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L831
	call	_ZdlPv@PLT
.L831:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L832
	call	_ZdlPv@PLT
.L832:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L833
	call	_ZdlPv@PLT
.L833:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L807:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L834
	call	_ZdlPv@PLT
.L834:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L835
	call	_ZdlPv@PLT
.L835:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L836
	call	_ZdlPv@PLT
.L836:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L804:
	addq	$8, %rbx
	cmpq	%rbx, -104(%rbp)
	jne	.L837
	movq	-80(%rbp), %rax
	movq	-128(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L803:
	testq	%rdi, %rdi
	je	.L838
	call	_ZdlPv@PLT
.L838:
	movq	-80(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L802:
	movq	104(%rbx), %rdi
	leaq	120(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L839
	call	_ZdlPv@PLT
.L839:
	movq	56(%rbx), %rdi
	leaq	72(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L840
	call	_ZdlPv@PLT
.L840:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L841
	call	_ZdlPv@PLT
.L841:
	movl	$168, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L740:
	movq	-88(%rbp), %rax
	movq	8(%rax), %rbx
	testq	%rbx, %rbx
	je	.L842
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	%rsi, -56(%rbp)
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L843
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rbx)
	movq	160(%rbx), %rax
	movq	%rax, -104(%rbp)
	testq	%rax, %rax
	je	.L844
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	%rsi, -120(%rbp)
	movq	%rax, -80(%rbp)
	cmpq	%rax, %rsi
	je	.L845
	movq	%rbx, -128(%rbp)
	.p2align 4,,10
	.p2align 3
.L902:
	movq	-80(%rbp), %rax
	movq	(%rax), %r13
	testq	%r13, %r13
	je	.L846
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L847
	movq	16(%r13), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r12, %r12
	je	.L848
	movq	(%r12), %rax
	movq	-56(%rbp), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L849
	movq	160(%r12), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r14, %r14
	je	.L850
	movq	8(%r14), %r15
	movq	(%r14), %rbx
	cmpq	%rbx, %r15
	je	.L851
	movq	%r12, -144(%rbp)
	movq	%rbx, %r12
	movq	%rsi, %rbx
	movq	%r13, -136(%rbp)
	movq	%r14, -152(%rbp)
	jmp	.L858
	.p2align 4,,10
	.p2align 3
.L1452:
	movq	16(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L854
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L855
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L854:
	movq	8(%r13), %r14
	testq	%r14, %r14
	je	.L856
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L857
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L856:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L852:
	addq	$8, %r12
	cmpq	%r12, %r15
	je	.L1451
.L858:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L852
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L1452
	addq	$8, %r12
	movq	%r13, %rdi
	call	*%rdx
	cmpq	%r12, %r15
	jne	.L858
	.p2align 4,,10
	.p2align 3
.L1451:
	movq	-152(%rbp), %r14
	movq	-136(%rbp), %r13
	movq	-144(%rbp), %r12
	movq	(%r14), %rbx
.L851:
	testq	%rbx, %rbx
	je	.L859
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L859:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L850:
	movq	152(%r12), %r15
	testq	%r15, %r15
	je	.L860
	movq	8(%r15), %rbx
	movq	(%r15), %r14
	cmpq	%r14, %rbx
	je	.L861
	movq	%r13, -136(%rbp)
	movq	%r12, -144(%rbp)
	jmp	.L870
	.p2align 4,,10
	.p2align 3
.L1454:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L864
	call	_ZdlPv@PLT
.L864:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L865
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L866
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L865:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L867
	call	_ZdlPv@PLT
.L867:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L868
	call	_ZdlPv@PLT
.L868:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L869
	call	_ZdlPv@PLT
.L869:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L862:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L1453
.L870:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L862
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L1454
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %rbx
	jne	.L870
	.p2align 4,,10
	.p2align 3
.L1453:
	movq	-136(%rbp), %r13
	movq	-144(%rbp), %r12
	movq	(%r15), %r14
.L861:
	testq	%r14, %r14
	je	.L871
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L871:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L860:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L872
	call	_ZdlPv@PLT
.L872:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L873
	call	_ZdlPv@PLT
.L873:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L874
	call	_ZdlPv@PLT
.L874:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L848:
	movq	8(%r13), %r12
	testq	%r12, %r12
	je	.L875
	movq	(%r12), %rax
	movq	-56(%rbp), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L876
	movq	160(%r12), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r14, %r14
	je	.L877
	movq	8(%r14), %r15
	movq	(%r14), %rbx
	cmpq	%rbx, %r15
	je	.L878
	movq	%r12, -144(%rbp)
	movq	%rbx, %r12
	movq	%rcx, %rbx
	movq	%r13, -136(%rbp)
	movq	%r14, -152(%rbp)
	jmp	.L885
	.p2align 4,,10
	.p2align 3
.L1456:
	movq	16(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L881
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L882
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L881:
	movq	8(%r13), %r14
	testq	%r14, %r14
	je	.L883
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L884
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L883:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L879:
	addq	$8, %r12
	cmpq	%r12, %r15
	je	.L1455
.L885:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L879
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L1456
	addq	$8, %r12
	movq	%r13, %rdi
	call	*%rdx
	cmpq	%r12, %r15
	jne	.L885
	.p2align 4,,10
	.p2align 3
.L1455:
	movq	-152(%rbp), %r14
	movq	-136(%rbp), %r13
	movq	-144(%rbp), %r12
	movq	(%r14), %rbx
.L878:
	testq	%rbx, %rbx
	je	.L886
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L886:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L877:
	movq	152(%r12), %r15
	testq	%r15, %r15
	je	.L887
	movq	8(%r15), %rbx
	movq	(%r15), %r14
	cmpq	%r14, %rbx
	je	.L888
	movq	%r13, -136(%rbp)
	movq	%r12, -144(%rbp)
	jmp	.L897
	.p2align 4,,10
	.p2align 3
.L1458:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L891
	call	_ZdlPv@PLT
.L891:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L892
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L893
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L892:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L894
	call	_ZdlPv@PLT
.L894:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L895
	call	_ZdlPv@PLT
.L895:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L896
	call	_ZdlPv@PLT
.L896:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L889:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L1457
.L897:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L889
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1458
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %rbx
	jne	.L897
	.p2align 4,,10
	.p2align 3
.L1457:
	movq	-136(%rbp), %r13
	movq	-144(%rbp), %r12
	movq	(%r15), %r14
.L888:
	testq	%r14, %r14
	je	.L898
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L898:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L887:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L899
	call	_ZdlPv@PLT
.L899:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L900
	call	_ZdlPv@PLT
.L900:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L901
	call	_ZdlPv@PLT
.L901:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L875:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L846:
	addq	$8, -80(%rbp)
	movq	-80(%rbp), %rax
	cmpq	%rax, -120(%rbp)
	jne	.L902
	movq	-104(%rbp), %rax
	movq	-128(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, -80(%rbp)
.L845:
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L903
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L903:
	movq	-104(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L844:
	movq	152(%rbx), %rax
	movq	%rax, -80(%rbp)
	testq	%rax, %rax
	je	.L904
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -104(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rdx
	je	.L905
	movq	%rbx, -128(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L939:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L906
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L907
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L908
	call	_ZdlPv@PLT
.L908:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L909
	movq	0(%r13), %rax
	movq	-56(%rbp), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L910
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L911
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L912
	movq	%rbx, -144(%rbp)
	movq	%r14, %rbx
	movq	%rdx, %r14
	movq	%r12, -136(%rbp)
	movq	%r13, -152(%rbp)
	jmp	.L919
	.p2align 4,,10
	.p2align 3
.L1460:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L915
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%r14, %rdx
	jne	.L916
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L915:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L917
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%r14, %rdx
	jne	.L918
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L917:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L913:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L1459
.L919:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L913
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L1460
	addq	$8, %rbx
	movq	%r12, %rdi
	call	*%rdx
	cmpq	%rbx, %r15
	jne	.L919
	.p2align 4,,10
	.p2align 3
.L1459:
	movq	-120(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %rbx
	movq	-152(%rbp), %r13
	movq	(%rax), %r14
.L912:
	testq	%r14, %r14
	je	.L920
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L920:
	movq	-120(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L911:
	movq	152(%r13), %rax
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L921
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L922
	movq	%r12, -136(%rbp)
	movq	%r13, -144(%rbp)
	jmp	.L931
	.p2align 4,,10
	.p2align 3
.L1462:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L925
	call	_ZdlPv@PLT
.L925:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L926
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L927
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L926:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L928
	call	_ZdlPv@PLT
.L928:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L929
	call	_ZdlPv@PLT
.L929:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L930
	call	_ZdlPv@PLT
.L930:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L923:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L1461
.L931:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L923
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1462
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %r15
	jne	.L931
	.p2align 4,,10
	.p2align 3
.L1461:
	movq	-120(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %r13
	movq	(%rax), %r14
.L922:
	testq	%r14, %r14
	je	.L932
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L932:
	movq	-120(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L921:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L933
	call	_ZdlPv@PLT
.L933:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L934
	call	_ZdlPv@PLT
.L934:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L935
	call	_ZdlPv@PLT
.L935:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L909:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L936
	call	_ZdlPv@PLT
.L936:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L937
	call	_ZdlPv@PLT
.L937:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L938
	call	_ZdlPv@PLT
.L938:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L906:
	addq	$8, %rbx
	cmpq	%rbx, -104(%rbp)
	jne	.L939
	movq	-80(%rbp), %rax
	movq	-128(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L905:
	testq	%rdi, %rdi
	je	.L940
	call	_ZdlPv@PLT
.L940:
	movq	-80(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L904:
	movq	104(%rbx), %rdi
	leaq	120(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L941
	call	_ZdlPv@PLT
.L941:
	movq	56(%rbx), %rdi
	leaq	72(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L942
	call	_ZdlPv@PLT
.L942:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L943
	call	_ZdlPv@PLT
.L943:
	movl	$168, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L842:
	movq	-88(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L738:
	addq	$8, -72(%rbp)
	movq	-72(%rbp), %rax
	cmpq	%rax, -112(%rbp)
	jne	.L944
	movq	-96(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -72(%rbp)
.L737:
	movq	-72(%rbp), %rax
	testq	%rax, %rax
	je	.L945
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L945:
	movq	-96(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L736:
	movq	-64(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -96(%rbp)
	testq	%rax, %rax
	je	.L946
	movq	8(%rax), %rbx
	movq	(%rax), %rax
	movq	%rbx, -112(%rbp)
	movq	%rax, -56(%rbp)
	cmpq	%rax, %rbx
	je	.L947
	.p2align 4,,10
	.p2align 3
.L1056:
	movq	-56(%rbp), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.L948
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L949
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	168(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L950
	call	_ZdlPv@PLT
.L950:
	movq	136(%rbx), %rcx
	movq	%rcx, -72(%rbp)
	testq	%rcx, %rcx
	je	.L951
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	%rsi, -88(%rbp)
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L952
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rcx)
	movq	160(%rcx), %rax
	movq	%rax, -104(%rbp)
	testq	%rax, %rax
	je	.L953
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -120(%rbp)
	movq	%rax, -80(%rbp)
	cmpq	%rax, %rcx
	je	.L954
	movq	%rbx, -128(%rbp)
	.p2align 4,,10
	.p2align 3
.L1011:
	movq	-80(%rbp), %rax
	movq	(%rax), %r13
	testq	%r13, %r13
	je	.L955
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L956
	movq	16(%r13), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r12, %r12
	je	.L957
	movq	(%r12), %rax
	movq	-88(%rbp), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L958
	movq	160(%r12), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r14, %r14
	je	.L959
	movq	8(%r14), %r15
	movq	(%r14), %rbx
	cmpq	%rbx, %r15
	je	.L960
	movq	%r12, -144(%rbp)
	movq	%rbx, %r12
	movq	%rsi, %rbx
	movq	%r13, -136(%rbp)
	movq	%r14, -152(%rbp)
	jmp	.L967
	.p2align 4,,10
	.p2align 3
.L1464:
	movq	16(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L963
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L964
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L963:
	movq	8(%r13), %r14
	testq	%r14, %r14
	je	.L965
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L966
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L965:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L961:
	addq	$8, %r12
	cmpq	%r12, %r15
	je	.L1463
.L967:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L961
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L1464
	addq	$8, %r12
	movq	%r13, %rdi
	call	*%rdx
	cmpq	%r12, %r15
	jne	.L967
	.p2align 4,,10
	.p2align 3
.L1463:
	movq	-152(%rbp), %r14
	movq	-136(%rbp), %r13
	movq	-144(%rbp), %r12
	movq	(%r14), %rbx
.L960:
	testq	%rbx, %rbx
	je	.L968
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L968:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L959:
	movq	152(%r12), %r15
	testq	%r15, %r15
	je	.L969
	movq	8(%r15), %rbx
	movq	(%r15), %r14
	cmpq	%r14, %rbx
	je	.L970
	movq	%r13, -136(%rbp)
	movq	%r12, -144(%rbp)
	jmp	.L979
	.p2align 4,,10
	.p2align 3
.L1466:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L973
	call	_ZdlPv@PLT
.L973:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L974
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	-88(%rbp), %rax
	jne	.L975
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L974:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L976
	call	_ZdlPv@PLT
.L976:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L977
	call	_ZdlPv@PLT
.L977:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L978
	call	_ZdlPv@PLT
.L978:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L971:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L1465
.L979:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L971
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L1466
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %rbx
	jne	.L979
	.p2align 4,,10
	.p2align 3
.L1465:
	movq	-136(%rbp), %r13
	movq	-144(%rbp), %r12
	movq	(%r15), %r14
.L970:
	testq	%r14, %r14
	je	.L980
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L980:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L969:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L981
	call	_ZdlPv@PLT
.L981:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L982
	call	_ZdlPv@PLT
.L982:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L983
	call	_ZdlPv@PLT
.L983:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L957:
	movq	8(%r13), %r12
	testq	%r12, %r12
	je	.L984
	movq	(%r12), %rax
	movq	-88(%rbp), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L985
	movq	160(%r12), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r14, %r14
	je	.L986
	movq	8(%r14), %r15
	movq	(%r14), %rbx
	cmpq	%rbx, %r15
	je	.L987
	movq	%r12, -144(%rbp)
	movq	%rbx, %r12
	movq	%rcx, %rbx
	movq	%r13, -136(%rbp)
	movq	%r14, -152(%rbp)
	jmp	.L994
	.p2align 4,,10
	.p2align 3
.L1468:
	movq	16(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L990
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L991
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L990:
	movq	8(%r13), %r14
	testq	%r14, %r14
	je	.L992
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L993
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L992:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L988:
	addq	$8, %r12
	cmpq	%r12, %r15
	je	.L1467
.L994:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L988
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L1468
	addq	$8, %r12
	movq	%r13, %rdi
	call	*%rdx
	cmpq	%r12, %r15
	jne	.L994
	.p2align 4,,10
	.p2align 3
.L1467:
	movq	-152(%rbp), %r14
	movq	-136(%rbp), %r13
	movq	-144(%rbp), %r12
	movq	(%r14), %rbx
.L987:
	testq	%rbx, %rbx
	je	.L995
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L995:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L986:
	movq	152(%r12), %r15
	testq	%r15, %r15
	je	.L996
	movq	8(%r15), %rbx
	movq	(%r15), %r14
	cmpq	%r14, %rbx
	je	.L997
	movq	%r13, -136(%rbp)
	movq	%r12, -144(%rbp)
	jmp	.L1006
	.p2align 4,,10
	.p2align 3
.L1470:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1000
	call	_ZdlPv@PLT
.L1000:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L1001
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	-88(%rbp), %rax
	jne	.L1002
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1001:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1003
	call	_ZdlPv@PLT
.L1003:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1004
	call	_ZdlPv@PLT
.L1004:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1005
	call	_ZdlPv@PLT
.L1005:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L998:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L1469
.L1006:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L998
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1470
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %rbx
	jne	.L1006
	.p2align 4,,10
	.p2align 3
.L1469:
	movq	-136(%rbp), %r13
	movq	-144(%rbp), %r12
	movq	(%r15), %r14
.L997:
	testq	%r14, %r14
	je	.L1007
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1007:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L996:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1008
	call	_ZdlPv@PLT
.L1008:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1009
	call	_ZdlPv@PLT
.L1009:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1010
	call	_ZdlPv@PLT
.L1010:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L984:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L955:
	addq	$8, -80(%rbp)
	movq	-80(%rbp), %rax
	cmpq	%rax, -120(%rbp)
	jne	.L1011
	movq	-104(%rbp), %rax
	movq	-128(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, -80(%rbp)
.L954:
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L1012
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L1012:
	movq	-104(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L953:
	movq	-72(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -80(%rbp)
	testq	%rax, %rax
	je	.L1013
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	%rsi, -104(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rsi
	je	.L1014
	movq	%rbx, -128(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L1048:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L1015
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1016
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1017
	call	_ZdlPv@PLT
.L1017:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L1018
	movq	0(%r13), %rax
	movq	-88(%rbp), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1019
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L1020
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L1021
	movq	%rbx, -144(%rbp)
	movq	%r14, %rbx
	movq	%rdx, %r14
	movq	%r12, -136(%rbp)
	movq	%r13, -152(%rbp)
	jmp	.L1028
	.p2align 4,,10
	.p2align 3
.L1472:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L1024
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%r14, %rdx
	jne	.L1025
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1024:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L1026
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%r14, %rdx
	jne	.L1027
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1026:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1022:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L1471
.L1028:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L1022
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L1472
	addq	$8, %rbx
	movq	%r12, %rdi
	call	*%rdx
	cmpq	%rbx, %r15
	jne	.L1028
	.p2align 4,,10
	.p2align 3
.L1471:
	movq	-120(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %rbx
	movq	-152(%rbp), %r13
	movq	(%rax), %r14
.L1021:
	testq	%r14, %r14
	je	.L1029
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1029:
	movq	-120(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1020:
	movq	152(%r13), %rax
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L1030
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L1031
	movq	%r12, -136(%rbp)
	movq	%r13, -144(%rbp)
	jmp	.L1040
	.p2align 4,,10
	.p2align 3
.L1474:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1034
	call	_ZdlPv@PLT
.L1034:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L1035
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	-88(%rbp), %rax
	jne	.L1036
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1035:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1037
	call	_ZdlPv@PLT
.L1037:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1038
	call	_ZdlPv@PLT
.L1038:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1039
	call	_ZdlPv@PLT
.L1039:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1032:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L1473
.L1040:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L1032
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1474
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %r15
	jne	.L1040
	.p2align 4,,10
	.p2align 3
.L1473:
	movq	-120(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %r13
	movq	(%rax), %r14
.L1031:
	testq	%r14, %r14
	je	.L1041
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1041:
	movq	-120(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1030:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1042
	call	_ZdlPv@PLT
.L1042:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1043
	call	_ZdlPv@PLT
.L1043:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1044
	call	_ZdlPv@PLT
.L1044:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1018:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1045
	call	_ZdlPv@PLT
.L1045:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1046
	call	_ZdlPv@PLT
.L1046:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1047
	call	_ZdlPv@PLT
.L1047:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1015:
	addq	$8, %rbx
	cmpq	%rbx, -104(%rbp)
	jne	.L1048
	movq	-80(%rbp), %rax
	movq	-128(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L1014:
	testq	%rdi, %rdi
	je	.L1049
	call	_ZdlPv@PLT
.L1049:
	movq	-80(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1013:
	movq	-72(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L1050
	call	_ZdlPv@PLT
.L1050:
	movq	-72(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L1051
	call	_ZdlPv@PLT
.L1051:
	movq	-72(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L1052
	call	_ZdlPv@PLT
.L1052:
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L951:
	movq	96(%rbx), %rdi
	leaq	112(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1053
	call	_ZdlPv@PLT
.L1053:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1054
	call	_ZdlPv@PLT
.L1054:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1055
	call	_ZdlPv@PLT
.L1055:
	movl	$192, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L948:
	addq	$8, -56(%rbp)
	movq	-56(%rbp), %rax
	cmpq	%rax, -112(%rbp)
	jne	.L1056
	movq	-96(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -56(%rbp)
.L947:
	movq	-56(%rbp), %rax
	testq	%rax, %rax
	je	.L1057
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L1057:
	movq	-96(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L946:
	movq	-64(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L1058
	call	_ZdlPv@PLT
.L1058:
	movq	-64(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L1059
	call	_ZdlPv@PLT
.L1059:
	movq	-64(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L1060
	call	_ZdlPv@PLT
.L1060:
	movq	-64(%rbp), %rdi
	addq	$120, %rsp
	movl	$168, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L739:
	.cfi_restore_state
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L738
	.p2align 4,,10
	.p2align 3
.L949:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L948
	.p2align 4,,10
	.p2align 3
.L1016:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1015
	.p2align 4,,10
	.p2align 3
.L956:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L955
	.p2align 4,,10
	.p2align 3
.L805:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L804
	.p2align 4,,10
	.p2align 3
.L907:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L906
	.p2align 4,,10
	.p2align 3
.L847:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L846
	.p2align 4,,10
	.p2align 3
.L745:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L744
	.p2align 4,,10
	.p2align 3
.L952:
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L951
	.p2align 4,,10
	.p2align 3
.L843:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L842
	.p2align 4,,10
	.p2align 3
.L741:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L740
	.p2align 4,,10
	.p2align 3
.L985:
	movq	%r12, %rdi
	call	*%rax
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	jmp	.L955
	.p2align 4,,10
	.p2align 3
.L774:
	movq	%r12, %rdi
	call	*%rax
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	jmp	.L744
	.p2align 4,,10
	.p2align 3
.L1019:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1018
	.p2align 4,,10
	.p2align 3
.L747:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L746
	.p2align 4,,10
	.p2align 3
.L910:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L909
	.p2align 4,,10
	.p2align 3
.L849:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L848
	.p2align 4,,10
	.p2align 3
.L876:
	movq	%r12, %rdi
	call	*%rax
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	jmp	.L846
	.p2align 4,,10
	.p2align 3
.L808:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L807
	.p2align 4,,10
	.p2align 3
.L958:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L957
	.p2align 4,,10
	.p2align 3
.L991:
	call	*%rdx
	jmp	.L990
	.p2align 4,,10
	.p2align 3
.L916:
	call	*%rdx
	jmp	.L915
	.p2align 4,,10
	.p2align 3
.L882:
	call	*%rdx
	jmp	.L881
	.p2align 4,,10
	.p2align 3
.L857:
	call	*%rdx
	jmp	.L856
	.p2align 4,,10
	.p2align 3
.L753:
	call	*%rdx
	jmp	.L752
	.p2align 4,,10
	.p2align 3
.L755:
	call	*%rdx
	jmp	.L754
	.p2align 4,,10
	.p2align 3
.L816:
	call	*%rdx
	jmp	.L815
	.p2align 4,,10
	.p2align 3
.L855:
	call	*%rdx
	jmp	.L854
	.p2align 4,,10
	.p2align 3
.L814:
	call	*%rdx
	jmp	.L813
	.p2align 4,,10
	.p2align 3
.L791:
	call	*%rax
	jmp	.L790
	.p2align 4,,10
	.p2align 3
.L782:
	call	*%rdx
	jmp	.L781
	.p2align 4,,10
	.p2align 3
.L1025:
	call	*%rdx
	jmp	.L1024
	.p2align 4,,10
	.p2align 3
.L964:
	call	*%rdx
	jmp	.L963
	.p2align 4,,10
	.p2align 3
.L993:
	call	*%rdx
	jmp	.L992
	.p2align 4,,10
	.p2align 3
.L966:
	call	*%rdx
	jmp	.L965
	.p2align 4,,10
	.p2align 3
.L893:
	call	*%rax
	jmp	.L892
	.p2align 4,,10
	.p2align 3
.L1002:
	call	*%rax
	jmp	.L1001
	.p2align 4,,10
	.p2align 3
.L866:
	call	*%rax
	jmp	.L865
	.p2align 4,,10
	.p2align 3
.L825:
	call	*%rax
	jmp	.L824
	.p2align 4,,10
	.p2align 3
.L927:
	call	*%rax
	jmp	.L926
	.p2align 4,,10
	.p2align 3
.L764:
	call	*%rax
	jmp	.L763
	.p2align 4,,10
	.p2align 3
.L1036:
	call	*%rax
	jmp	.L1035
	.p2align 4,,10
	.p2align 3
.L975:
	call	*%rax
	jmp	.L974
	.p2align 4,,10
	.p2align 3
.L884:
	call	*%rdx
	jmp	.L883
	.p2align 4,,10
	.p2align 3
.L780:
	call	*%rdx
	jmp	.L779
	.p2align 4,,10
	.p2align 3
.L918:
	call	*%rdx
	jmp	.L917
	.p2align 4,,10
	.p2align 3
.L1027:
	call	*%rdx
	jmp	.L1026
	.cfi_endproc
.LFE4642:
	.size	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev, .-_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime12EntryPreviewD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12EntryPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD2Ev, @function
_ZN12v8_inspector8protocol7Runtime12EntryPreviewD2Ev:
.LFB4753:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	16(%rdi), %r12
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L1476
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %r13
	movq	24(%rax), %rax
	cmpq	%r13, %rax
	jne	.L1477
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	movq	160(%r12), %rax
	movq	%rax, -64(%rbp)
	testq	%rax, %rax
	je	.L1478
	movq	8(%rax), %rdx
	movq	(%rax), %r14
	movq	%rdx, -56(%rbp)
	cmpq	%r14, %rdx
	jne	.L1486
	jmp	.L1479
	.p2align 4,,10
	.p2align 3
.L1593:
	movq	16(%r15), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%rdi, %rdi
	je	.L1482
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%r13, %rax
	jne	.L1483
	movq	%rdi, -72(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L1482:
	movq	8(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1484
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%r13, %rax
	jne	.L1485
	movq	%rdi, -72(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L1484:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1480:
	addq	$8, %r14
	cmpq	%r14, -56(%rbp)
	je	.L1592
.L1486:
	movq	(%r14), %r15
	testq	%r15, %r15
	je	.L1480
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1593
	movq	%r15, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -56(%rbp)
	jne	.L1486
	.p2align 4,,10
	.p2align 3
.L1592:
	movq	-64(%rbp), %rax
	movq	(%rax), %r14
.L1479:
	testq	%r14, %r14
	je	.L1487
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1487:
	movq	-64(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1478:
	movq	152(%r12), %rax
	movq	%rax, -64(%rbp)
	testq	%rax, %rax
	je	.L1488
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	movq	%rcx, -56(%rbp)
	cmpq	%r14, %rcx
	jne	.L1498
	jmp	.L1489
	.p2align 4,,10
	.p2align 3
.L1595:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r15), %rdi
	movq	%rax, (%r15)
	leaq	168(%r15), %rax
	cmpq	%rax, %rdi
	je	.L1492
	call	_ZdlPv@PLT
.L1492:
	movq	136(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1493
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%r13, %rax
	jne	.L1494
	movq	%rdi, -72(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L1493:
	movq	96(%r15), %rdi
	leaq	112(%r15), %rax
	cmpq	%rax, %rdi
	je	.L1495
	call	_ZdlPv@PLT
.L1495:
	movq	48(%r15), %rdi
	leaq	64(%r15), %rax
	cmpq	%rax, %rdi
	je	.L1496
	call	_ZdlPv@PLT
.L1496:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L1497
	call	_ZdlPv@PLT
.L1497:
	movl	$192, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1490:
	addq	$8, %r14
	cmpq	%r14, -56(%rbp)
	je	.L1594
.L1498:
	movq	(%r14), %r15
	testq	%r15, %r15
	je	.L1490
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1595
	movq	%r15, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -56(%rbp)
	jne	.L1498
	.p2align 4,,10
	.p2align 3
.L1594:
	movq	-64(%rbp), %rax
	movq	(%rax), %r14
.L1489:
	testq	%r14, %r14
	je	.L1499
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1499:
	movq	-64(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1488:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1500
	call	_ZdlPv@PLT
.L1500:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1501
	call	_ZdlPv@PLT
.L1501:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1502
	call	_ZdlPv@PLT
.L1502:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1476:
	movq	8(%rbx), %r12
	testq	%r12, %r12
	je	.L1475
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %r13
	movq	24(%rax), %rax
	cmpq	%r13, %rax
	jne	.L1504
	movq	160(%r12), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r14, %r14
	je	.L1505
	movq	8(%r14), %r15
	movq	(%r14), %rbx
	cmpq	%rbx, %r15
	jne	.L1513
	jmp	.L1506
	.p2align 4,,10
	.p2align 3
.L1597:
	movq	16(%r8), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r8)
	testq	%rdi, %rdi
	je	.L1509
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%r13, %rax
	jne	.L1510
	movq	%r8, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-56(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
	movq	-64(%rbp), %r8
.L1509:
	movq	8(%r8), %rdi
	testq	%rdi, %rdi
	je	.L1511
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%r13, %rax
	jne	.L1512
	movq	%r8, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-56(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
	movq	-64(%rbp), %r8
.L1511:
	movl	$24, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L1507:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L1596
.L1513:
	movq	(%rbx), %r8
	testq	%r8, %r8
	je	.L1507
	movq	(%r8), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1597
	addq	$8, %rbx
	movq	%r8, %rdi
	call	*%rax
	cmpq	%rbx, %r15
	jne	.L1513
	.p2align 4,,10
	.p2align 3
.L1596:
	movq	(%r14), %rbx
.L1506:
	testq	%rbx, %rbx
	je	.L1514
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L1514:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1505:
	movq	152(%r12), %r15
	testq	%r15, %r15
	je	.L1515
	movq	8(%r15), %rax
	movq	(%r15), %r14
	movq	%rax, -56(%rbp)
	cmpq	%r14, %rax
	jne	.L1525
	jmp	.L1516
	.p2align 4,,10
	.p2align 3
.L1599:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	168(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1519
	call	_ZdlPv@PLT
.L1519:
	movq	136(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1520
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%r13, %rax
	jne	.L1521
	movq	%rdi, -64(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-64(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L1520:
	movq	96(%rbx), %rdi
	leaq	112(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1522
	call	_ZdlPv@PLT
.L1522:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1523
	call	_ZdlPv@PLT
.L1523:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1524
	call	_ZdlPv@PLT
.L1524:
	movl	$192, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L1517:
	addq	$8, %r14
	cmpq	%r14, -56(%rbp)
	je	.L1598
.L1525:
	movq	(%r14), %rbx
	testq	%rbx, %rbx
	je	.L1517
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1599
	movq	%rbx, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -56(%rbp)
	jne	.L1525
	.p2align 4,,10
	.p2align 3
.L1598:
	movq	(%r15), %r14
.L1516:
	testq	%r14, %r14
	je	.L1526
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1526:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1515:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1527
	call	_ZdlPv@PLT
.L1527:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1528
	call	_ZdlPv@PLT
.L1528:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1529
	call	_ZdlPv@PLT
.L1529:
	addq	$40, %rsp
	movq	%r12, %rdi
	movl	$168, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L1475:
	.cfi_restore_state
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1477:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1476
	.p2align 4,,10
	.p2align 3
.L1504:
	addq	$40, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1483:
	.cfi_restore_state
	call	*%rax
	jmp	.L1482
	.p2align 4,,10
	.p2align 3
.L1510:
	movq	%r8, -56(%rbp)
	call	*%rax
	movq	-56(%rbp), %r8
	jmp	.L1509
	.p2align 4,,10
	.p2align 3
.L1485:
	call	*%rax
	jmp	.L1484
	.p2align 4,,10
	.p2align 3
.L1494:
	call	*%rax
	jmp	.L1493
	.p2align 4,,10
	.p2align 3
.L1512:
	movq	%r8, -56(%rbp)
	call	*%rax
	movq	-56(%rbp), %r8
	jmp	.L1511
	.p2align 4,,10
	.p2align 3
.L1521:
	call	*%rax
	jmp	.L1520
	.cfi_endproc
.LFE4753:
	.size	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD2Ev, .-_ZN12v8_inspector8protocol7Runtime12EntryPreviewD2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD1Ev,_ZN12v8_inspector8protocol7Runtime12EntryPreviewD2Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime15PropertyPreviewD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD2Ev, @function
_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD2Ev:
.LFB4713:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	168(%rbx), %rax
	subq	$40, %rsp
	movq	%r15, (%rdi)
	movq	152(%rdi), %rdi
	cmpq	%rax, %rdi
	je	.L1601
	call	_ZdlPv@PLT
.L1601:
	movq	136(%rbx), %r12
	testq	%r12, %r12
	je	.L1602
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1603
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	movq	160(%r12), %rax
	movq	%rax, -64(%rbp)
	testq	%rax, %rax
	je	.L1604
	movq	8(%rax), %rsi
	movq	(%rax), %r13
	movq	%rsi, -56(%rbp)
	cmpq	%r13, %rsi
	jne	.L1612
	jmp	.L1605
	.p2align 4,,10
	.p2align 3
.L1666:
	movq	16(%r14), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r14)
	testq	%rdi, %rdi
	je	.L1608
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1609
	movq	%rdi, -72(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L1608:
	movq	8(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1610
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1611
	movq	%rdi, -72(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L1610:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1606:
	addq	$8, %r13
	cmpq	%r13, -56(%rbp)
	je	.L1665
.L1612:
	movq	0(%r13), %r14
	testq	%r14, %r14
	je	.L1606
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1666
	movq	%r14, %rdi
	addq	$8, %r13
	call	*%rax
	cmpq	%r13, -56(%rbp)
	jne	.L1612
	.p2align 4,,10
	.p2align 3
.L1665:
	movq	-64(%rbp), %rax
	movq	(%rax), %r13
.L1605:
	testq	%r13, %r13
	je	.L1613
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1613:
	movq	-64(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1604:
	movq	152(%r12), %rax
	movq	%rax, -64(%rbp)
	testq	%rax, %rax
	je	.L1614
	movq	8(%rax), %rcx
	movq	(%rax), %r13
	movq	%rcx, -56(%rbp)
	cmpq	%r13, %rcx
	jne	.L1624
	jmp	.L1615
	.p2align 4,,10
	.p2align 3
.L1668:
	movq	152(%r14), %rdi
	leaq	168(%r14), %rax
	movq	%r15, (%r14)
	cmpq	%rax, %rdi
	je	.L1618
	call	_ZdlPv@PLT
.L1618:
	movq	136(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1619
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1620
	movq	%rdi, -72(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L1619:
	movq	96(%r14), %rdi
	leaq	112(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1621
	call	_ZdlPv@PLT
.L1621:
	movq	48(%r14), %rdi
	leaq	64(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1622
	call	_ZdlPv@PLT
.L1622:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1623
	call	_ZdlPv@PLT
.L1623:
	movl	$192, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1616:
	addq	$8, %r13
	cmpq	%r13, -56(%rbp)
	je	.L1667
.L1624:
	movq	0(%r13), %r14
	testq	%r14, %r14
	je	.L1616
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1668
	movq	%r14, %rdi
	addq	$8, %r13
	call	*%rax
	cmpq	%r13, -56(%rbp)
	jne	.L1624
	.p2align 4,,10
	.p2align 3
.L1667:
	movq	-64(%rbp), %rax
	movq	(%rax), %r13
.L1615:
	testq	%r13, %r13
	je	.L1625
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1625:
	movq	-64(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1614:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1626
	call	_ZdlPv@PLT
.L1626:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1627
	call	_ZdlPv@PLT
.L1627:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1628
	call	_ZdlPv@PLT
.L1628:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1602:
	movq	96(%rbx), %rdi
	leaq	112(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1629
	call	_ZdlPv@PLT
.L1629:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1630
	call	_ZdlPv@PLT
.L1630:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L1600
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L1600:
	.cfi_restore_state
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1603:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1602
	.p2align 4,,10
	.p2align 3
.L1620:
	call	*%rax
	jmp	.L1619
	.p2align 4,,10
	.p2align 3
.L1611:
	call	*%rax
	jmp	.L1610
	.p2align 4,,10
	.p2align 3
.L1609:
	call	*%rax
	jmp	.L1608
	.cfi_endproc
.LFE4713:
	.size	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD2Ev, .-_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD1Ev,_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD2Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12RemoteObjectD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev, @function
_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev:
.LFB4459:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12RemoteObjectE(%rip), %rax
	leaq	-64(%rax), %rcx
	movq	%rax, %xmm1
	movq	%rcx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	312(%rdi), %r12
	movups	%xmm0, (%rdi)
	testq	%r12, %r12
	je	.L1670
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1671
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE(%rip), %rax
	movq	56(%r12), %rdi
	movq	%rax, (%r12)
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1672
	call	_ZdlPv@PLT
.L1672:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1673
	call	_ZdlPv@PLT
.L1673:
	movl	$96, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1670:
	movq	304(%r15), %rcx
	movq	%rcx, -72(%rbp)
	testq	%rcx, %rcx
	je	.L1674
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1675
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rcx)
	movq	160(%rcx), %rax
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L1676
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -104(%rbp)
	movq	%rax, -56(%rbp)
	cmpq	%rax, %rcx
	je	.L1677
	movq	%r15, -160(%rbp)
	.p2align 4,,10
	.p2align 3
.L1984:
	movq	-56(%rbp), %rax
	movq	(%rax), %rcx
	movq	%rcx, -88(%rbp)
	testq	%rcx, %rcx
	je	.L1678
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1679
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%rcx)
	movq	16(%rcx), %rcx
	movq	%rcx, -80(%rbp)
	testq	%rcx, %rcx
	je	.L1680
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1681
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rcx)
	movq	160(%rcx), %rax
	movq	%rax, -136(%rbp)
	testq	%rax, %rax
	je	.L1682
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -112(%rbp)
	movq	%rax, -64(%rbp)
	cmpq	%rax, %rdx
	je	.L1683
	.p2align 4,,10
	.p2align 3
.L1790:
	movq	-64(%rbp), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.L1684
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1685
	movq	16(%rbx), %rdx
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%rbx)
	movq	%rdx, -96(%rbp)
	testq	%rdx, %rdx
	je	.L1686
	movq	(%rdx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1687
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rdx)
	movq	160(%rdx), %rax
	movq	%rax, -144(%rbp)
	testq	%rax, %rax
	je	.L1688
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -128(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rdx
	je	.L1689
	movq	%rbx, -168(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L1746:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L1690
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1691
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L1692
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1693
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -152(%rbp)
	testq	%rax, %rax
	je	.L1694
	movq	8(%rax), %rsi
	movq	(%rax), %r14
	cmpq	%r14, %rsi
	je	.L1695
	movq	%rbx, -184(%rbp)
	movq	%r14, %rbx
	movq	%rsi, %r14
	movq	%r12, -176(%rbp)
	jmp	.L1702
	.p2align 4,,10
	.p2align 3
.L2774:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L1698
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1699
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1698:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L1700
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1701
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1700:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1696:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L2773
.L1702:
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.L1696
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L2774
	movq	%r15, %rdi
	call	*%rdx
	jmp	.L1696
	.p2align 4,,10
	.p2align 3
.L2809:
	movq	-96(%rbp), %rax
	movq	-128(%rbp), %r12
	movq	-136(%rbp), %r13
	movq	(%rax), %r14
.L1962:
	testq	%r14, %r14
	je	.L1972
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1972:
	movq	-96(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1961:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1973
	call	_ZdlPv@PLT
.L1973:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1974
	call	_ZdlPv@PLT
.L1974:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1975
	call	_ZdlPv@PLT
.L1975:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1949:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1976
	call	_ZdlPv@PLT
.L1976:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1977
	call	_ZdlPv@PLT
.L1977:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1978
	call	_ZdlPv@PLT
.L1978:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1946:
	addq	$8, %rbx
	cmpq	%rbx, -64(%rbp)
	jne	.L1979
	movq	-80(%rbp), %rax
	movq	-112(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L1945:
	testq	%rdi, %rdi
	je	.L1980
	call	_ZdlPv@PLT
.L1980:
	movq	-80(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1944:
	movq	104(%rbx), %rdi
	leaq	120(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1981
	call	_ZdlPv@PLT
.L1981:
	movq	56(%rbx), %rdi
	leaq	72(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1982
	call	_ZdlPv@PLT
.L1982:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1983
	call	_ZdlPv@PLT
.L1983:
	movl	$168, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L1857:
	movq	-88(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1678:
	addq	$8, -56(%rbp)
	movq	-56(%rbp), %rax
	cmpq	%rax, -104(%rbp)
	jne	.L1984
	movq	-120(%rbp), %rax
	movq	-160(%rbp), %r15
	movq	(%rax), %rax
	movq	%rax, -56(%rbp)
.L1677:
	movq	-56(%rbp), %rax
	testq	%rax, %rax
	je	.L1985
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L1985:
	movq	-120(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1676:
	movq	-72(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L1986
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	%rsi, -96(%rbp)
	movq	%rax, -56(%rbp)
	cmpq	%rax, %rsi
	je	.L1987
	movq	%r15, -152(%rbp)
	.p2align 4,,10
	.p2align 3
.L2171:
	movq	-56(%rbp), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.L1988
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1989
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	168(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1990
	call	_ZdlPv@PLT
.L1990:
	movq	136(%rbx), %rsi
	movq	%rsi, -80(%rbp)
	testq	%rsi, %rsi
	je	.L1991
	movq	(%rsi), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1992
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rsi)
	movq	160(%rsi), %rax
	movq	%rax, -128(%rbp)
	testq	%rax, %rax
	je	.L1993
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -104(%rbp)
	movq	%rax, -64(%rbp)
	cmpq	%rax, %rcx
	je	.L1994
	movq	%rbx, -160(%rbp)
	.p2align 4,,10
	.p2align 3
.L2126:
	movq	-64(%rbp), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.L1995
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1996
	movq	16(%rbx), %rdx
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%rbx)
	movq	%rdx, -88(%rbp)
	testq	%rdx, %rdx
	je	.L1997
	movq	(%rdx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1998
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rdx)
	movq	160(%rdx), %rax
	movq	%rax, -136(%rbp)
	testq	%rax, %rax
	je	.L1999
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -120(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rdx
	je	.L2000
	movq	%rbx, -168(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L2057:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L2001
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2002
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L2003
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2004
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -144(%rbp)
	testq	%rax, %rax
	je	.L2005
	movq	8(%rax), %rdx
	movq	(%rax), %r14
	cmpq	%r14, %rdx
	je	.L2006
	movq	%rbx, -184(%rbp)
	movq	%r14, %rbx
	movq	%rdx, %r14
	movq	%r12, -176(%rbp)
	jmp	.L2013
	.p2align 4,,10
	.p2align 3
.L2776:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L2009
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2010
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2009:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L2011
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2012
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2011:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L2007:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L2775
.L2013:
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.L2007
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L2776
	movq	%r15, %rdi
	call	*%rdx
	jmp	.L2007
	.p2align 4,,10
	.p2align 3
.L2805:
	movq	-104(%rbp), %rax
	movq	-128(%rbp), %r12
	movq	-136(%rbp), %r13
	movq	(%rax), %r14
.L2146:
	testq	%r14, %r14
	je	.L2156
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2156:
	movq	-104(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2145:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2157
	call	_ZdlPv@PLT
.L2157:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2158
	call	_ZdlPv@PLT
.L2158:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2159
	call	_ZdlPv@PLT
.L2159:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2133:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2160
	call	_ZdlPv@PLT
.L2160:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2161
	call	_ZdlPv@PLT
.L2161:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2162
	call	_ZdlPv@PLT
.L2162:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2130:
	addq	$8, %rbx
	cmpq	%rbx, -64(%rbp)
	jne	.L2163
	movq	-88(%rbp), %rax
	movq	-120(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L2129:
	testq	%rdi, %rdi
	je	.L2164
	call	_ZdlPv@PLT
.L2164:
	movq	-88(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2128:
	movq	-80(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L2165
	call	_ZdlPv@PLT
.L2165:
	movq	-80(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L2166
	call	_ZdlPv@PLT
.L2166:
	movq	-80(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L2167
	call	_ZdlPv@PLT
.L2167:
	movq	-80(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L1991:
	movq	96(%rbx), %rdi
	leaq	112(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2168
	call	_ZdlPv@PLT
.L2168:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2169
	call	_ZdlPv@PLT
.L2169:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2170
	call	_ZdlPv@PLT
.L2170:
	movl	$192, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L1988:
	addq	$8, -56(%rbp)
	movq	-56(%rbp), %rax
	cmpq	%rax, -96(%rbp)
	jne	.L2171
	movq	-112(%rbp), %rax
	movq	-152(%rbp), %r15
	movq	(%rax), %rax
	movq	%rax, -56(%rbp)
.L1987:
	movq	-56(%rbp), %rax
	testq	%rax, %rax
	je	.L2172
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L2172:
	movq	-112(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1986:
	movq	-72(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L2173
	call	_ZdlPv@PLT
.L2173:
	movq	-72(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L2174
	call	_ZdlPv@PLT
.L2174:
	movq	-72(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L2175
	call	_ZdlPv@PLT
.L2175:
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L1674:
	movq	264(%r15), %rdi
	leaq	280(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2176
	call	_ZdlPv@PLT
.L2176:
	movq	216(%r15), %rdi
	leaq	232(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2177
	call	_ZdlPv@PLT
.L2177:
	movq	168(%r15), %rdi
	leaq	184(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2178
	call	_ZdlPv@PLT
.L2178:
	movq	152(%r15), %rdi
	testq	%rdi, %rdi
	je	.L2179
	movq	(%rdi), %rax
	call	*24(%rax)
.L2179:
	movq	112(%r15), %rdi
	leaq	128(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2180
	call	_ZdlPv@PLT
.L2180:
	movq	64(%r15), %rdi
	leaq	80(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2181
	call	_ZdlPv@PLT
.L2181:
	movq	16(%r15), %rdi
	leaq	32(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2182
	call	_ZdlPv@PLT
.L2182:
	addq	$152, %rsp
	movq	%r15, %rdi
	movl	$320, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L1996:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	*%rax
	.p2align 4,,10
	.p2align 3
.L1995:
	addq	$8, -64(%rbp)
	movq	-64(%rbp), %rax
	cmpq	%rax, -104(%rbp)
	jne	.L2126
	movq	-128(%rbp), %rax
	movq	-160(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
.L1994:
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L2127
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L2127:
	movq	-128(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1993:
	movq	-80(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -88(%rbp)
	testq	%rax, %rax
	je	.L2128
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	%rsi, -64(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rsi
	je	.L2129
	movq	%rbx, -120(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L2163:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L2130
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2131
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2132
	call	_ZdlPv@PLT
.L2132:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2133
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2134
	movq	160(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L2135
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L2136
	movq	%rbx, -128(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r12, -104(%rbp)
	movq	%r13, -136(%rbp)
	jmp	.L2143
	.p2align 4,,10
	.p2align 3
.L2778:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L2139
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2140
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2139:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L2141
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2142
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2141:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2137:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L2777
.L2143:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L2137
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L2778
	movq	%r12, %rdi
	call	*%rdx
	jmp	.L2137
	.p2align 4,,10
	.p2align 3
.L1685:
	movq	%rbx, %rdi
	call	*%rax
	.p2align 4,,10
	.p2align 3
.L1684:
	addq	$8, -64(%rbp)
	movq	-64(%rbp), %rax
	cmpq	%rax, -112(%rbp)
	jne	.L1790
	movq	-136(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
.L1683:
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L1791
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L1791:
	movq	-136(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1682:
	movq	-80(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -136(%rbp)
	testq	%rax, %rax
	je	.L1792
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	%rcx, -112(%rbp)
	cmpq	%rbx, %rcx
	je	.L1793
	movq	%rbx, -64(%rbp)
	.p2align 4,,10
	.p2align 3
.L1852:
	movq	-64(%rbp), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.L1794
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1795
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	168(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1796
	call	_ZdlPv@PLT
.L1796:
	movq	136(%rbx), %rcx
	movq	%rcx, -96(%rbp)
	testq	%rcx, %rcx
	je	.L1797
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1798
	movq	160(%rcx), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rcx)
	testq	%r12, %r12
	je	.L1799
	movq	8(%r12), %r15
	movq	(%r12), %r14
	cmpq	%r14, %r15
	je	.L1800
	movq	%r12, -128(%rbp)
	jmp	.L1807
	.p2align 4,,10
	.p2align 3
.L2780:
	movq	16(%r13), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r12, %r12
	je	.L1803
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1804
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1803:
	movq	8(%r13), %r12
	testq	%r12, %r12
	je	.L1805
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1806
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1805:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1801:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L2779
.L1807:
	movq	(%r14), %r13
	testq	%r13, %r13
	je	.L1801
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2780
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1801
	.p2align 4,,10
	.p2align 3
.L2815:
	movq	-152(%rbp), %rax
	movq	-176(%rbp), %r12
	movq	-184(%rbp), %r13
	movq	(%rax), %r14
.L1827:
	testq	%r14, %r14
	je	.L1837
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1837:
	movq	-152(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1826:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1838
	call	_ZdlPv@PLT
.L1838:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1839
	call	_ZdlPv@PLT
.L1839:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1840
	call	_ZdlPv@PLT
.L1840:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1814:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1841
	call	_ZdlPv@PLT
.L1841:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1842
	call	_ZdlPv@PLT
.L1842:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1843
	call	_ZdlPv@PLT
.L1843:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1811:
	addq	$8, %rbx
	cmpq	%rbx, -128(%rbp)
	jne	.L1844
	movq	-144(%rbp), %rax
	movq	-168(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L1810:
	testq	%rdi, %rdi
	je	.L1845
	call	_ZdlPv@PLT
.L1845:
	movq	-144(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1809:
	movq	-96(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L1846
	call	_ZdlPv@PLT
.L1846:
	movq	-96(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L1847
	call	_ZdlPv@PLT
.L1847:
	movq	-96(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L1848
	call	_ZdlPv@PLT
.L1848:
	movq	-96(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L1797:
	movq	96(%rbx), %rdi
	leaq	112(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1849
	call	_ZdlPv@PLT
.L1849:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1850
	call	_ZdlPv@PLT
.L1850:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1851
	call	_ZdlPv@PLT
.L1851:
	movl	$192, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L1794:
	addq	$8, -64(%rbp)
	movq	-64(%rbp), %rax
	cmpq	%rax, -112(%rbp)
	jne	.L1852
	movq	-136(%rbp), %rax
	movq	(%rax), %rbx
.L1793:
	testq	%rbx, %rbx
	je	.L1853
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L1853:
	movq	-136(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1792:
	movq	-80(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L1854
	call	_ZdlPv@PLT
.L1854:
	movq	-80(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L1855
	call	_ZdlPv@PLT
.L1855:
	movq	-80(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L1856
	call	_ZdlPv@PLT
.L1856:
	movq	-80(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L1680:
	movq	-88(%rbp), %rax
	movq	8(%rax), %rbx
	testq	%rbx, %rbx
	je	.L1857
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1858
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rbx)
	movq	160(%rbx), %rax
	movq	%rax, -128(%rbp)
	testq	%rax, %rax
	je	.L1859
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	%rsi, -96(%rbp)
	movq	%rax, -64(%rbp)
	cmpq	%rax, %rsi
	je	.L1860
	movq	%rbx, -152(%rbp)
	.p2align 4,,10
	.p2align 3
.L1942:
	movq	-64(%rbp), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.L1861
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1862
	movq	16(%rbx), %rsi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%rbx)
	movq	%rsi, -80(%rbp)
	testq	%rsi, %rsi
	je	.L1863
	movq	(%rsi), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1864
	movq	160(%rsi), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rsi)
	testq	%r12, %r12
	je	.L1865
	movq	8(%r12), %r15
	movq	(%r12), %r14
	cmpq	%r14, %r15
	je	.L1866
	movq	%r12, -112(%rbp)
	jmp	.L1873
	.p2align 4,,10
	.p2align 3
.L2782:
	movq	16(%r13), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r12, %r12
	je	.L1869
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1870
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1869:
	movq	8(%r13), %r12
	testq	%r12, %r12
	je	.L1871
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1872
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1871:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1867:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L2781
.L1873:
	movq	(%r14), %r13
	testq	%r13, %r13
	je	.L1867
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L2782
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1867
	.p2align 4,,10
	.p2align 3
.L1862:
	movq	%rbx, %rdi
	call	*%rax
	.p2align 4,,10
	.p2align 3
.L1861:
	addq	$8, -64(%rbp)
	movq	-64(%rbp), %rax
	cmpq	%rax, -96(%rbp)
	jne	.L1942
	movq	-128(%rbp), %rax
	movq	-152(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
.L1860:
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L1943
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L1943:
	movq	-128(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1859:
	movq	152(%rbx), %rax
	movq	%rax, -80(%rbp)
	testq	%rax, %rax
	je	.L1944
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -64(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rdx
	je	.L1945
	movq	%rbx, -112(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L1979:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L1946
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1947
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1948
	call	_ZdlPv@PLT
.L1948:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L1949
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1950
	movq	160(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L1951
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L1952
	movq	%rbx, -128(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r12, -96(%rbp)
	movq	%r13, -136(%rbp)
	jmp	.L1959
	.p2align 4,,10
	.p2align 3
.L2784:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L1955
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1956
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1955:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L1957
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1958
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1957:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1953:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L2783
.L1959:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L1953
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L2784
	movq	%r12, %rdi
	call	*%rdx
	jmp	.L1953
	.p2align 4,,10
	.p2align 3
.L2002:
	movq	%r12, %rdi
	call	*%rax
	.p2align 4,,10
	.p2align 3
.L2001:
	addq	$8, %rbx
	cmpq	%rbx, -120(%rbp)
	jne	.L2057
	movq	-136(%rbp), %rax
	movq	-168(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L2000:
	testq	%rdi, %rdi
	je	.L2058
	call	_ZdlPv@PLT
.L2058:
	movq	-136(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1999:
	movq	-88(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -136(%rbp)
	testq	%rax, %rax
	je	.L2059
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -120(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rcx
	je	.L2060
	movq	%rbx, -168(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L2094:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L2061
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2062
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2063
	call	_ZdlPv@PLT
.L2063:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2064
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2065
	movq	160(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L2066
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L2067
	movq	%rbx, -176(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r12, -144(%rbp)
	movq	%r13, -184(%rbp)
	jmp	.L2074
	.p2align 4,,10
	.p2align 3
.L2786:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L2070
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2071
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2070:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L2072
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2073
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2072:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2068:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L2785
.L2074:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L2068
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L2786
	movq	%r12, %rdi
	call	*%rdx
	jmp	.L2068
	.p2align 4,,10
	.p2align 3
.L2821:
	movq	-144(%rbp), %rax
	movq	-176(%rbp), %r12
	movq	-184(%rbp), %r13
	movq	(%rax), %r14
.L2077:
	testq	%r14, %r14
	je	.L2087
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2087:
	movq	-144(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2076:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2088
	call	_ZdlPv@PLT
.L2088:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2089
	call	_ZdlPv@PLT
.L2089:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2090
	call	_ZdlPv@PLT
.L2090:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2064:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2091
	call	_ZdlPv@PLT
.L2091:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2092
	call	_ZdlPv@PLT
.L2092:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2093
	call	_ZdlPv@PLT
.L2093:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2061:
	addq	$8, %rbx
	cmpq	%rbx, -120(%rbp)
	jne	.L2094
	movq	-136(%rbp), %rax
	movq	-168(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L2060:
	testq	%rdi, %rdi
	je	.L2095
	call	_ZdlPv@PLT
.L2095:
	movq	-136(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2059:
	movq	-88(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L2096
	call	_ZdlPv@PLT
.L2096:
	movq	-88(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L2097
	call	_ZdlPv@PLT
.L2097:
	movq	-88(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L2098
	call	_ZdlPv@PLT
.L2098:
	movq	-88(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L1997:
	movq	8(%rbx), %r12
	testq	%r12, %r12
	je	.L2099
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2100
	movq	160(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L2101
	movq	8(%r13), %rax
	movq	0(%r13), %r15
	cmpq	%r15, %rax
	je	.L2102
	movq	%rbx, -88(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r12, -120(%rbp)
	jmp	.L2109
	.p2align 4,,10
	.p2align 3
.L2788:
	movq	16(%r14), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r14)
	testq	%r12, %r12
	je	.L2105
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2106
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2105:
	movq	8(%r14), %r12
	testq	%r12, %r12
	je	.L2107
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2108
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2107:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2103:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L2787
.L2109:
	movq	(%rbx), %r14
	testq	%r14, %r14
	je	.L2103
	movq	(%r14), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L2788
	movq	%r14, %rdi
	call	*%rdx
	jmp	.L2103
	.p2align 4,,10
	.p2align 3
.L1691:
	movq	%r12, %rdi
	call	*%rax
	.p2align 4,,10
	.p2align 3
.L1690:
	addq	$8, %rbx
	cmpq	%rbx, -128(%rbp)
	jne	.L1746
	movq	-144(%rbp), %rax
	movq	-168(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L1689:
	testq	%rdi, %rdi
	je	.L1747
	call	_ZdlPv@PLT
.L1747:
	movq	-144(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1688:
	movq	-96(%rbp), %rax
	movq	152(%rax), %r13
	testq	%r13, %r13
	je	.L1748
	movq	8(%r13), %r15
	movq	0(%r13), %r12
	cmpq	%r12, %r15
	je	.L1749
	movq	%r13, -128(%rbp)
	jmp	.L1758
	.p2align 4,,10
	.p2align 3
.L2790:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r14), %rdi
	movq	%rax, (%r14)
	leaq	168(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1752
	call	_ZdlPv@PLT
.L1752:
	movq	136(%r14), %r13
	testq	%r13, %r13
	je	.L1753
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1754
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1753:
	movq	96(%r14), %rdi
	leaq	112(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1755
	call	_ZdlPv@PLT
.L1755:
	movq	48(%r14), %rdi
	leaq	64(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1756
	call	_ZdlPv@PLT
.L1756:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1757
	call	_ZdlPv@PLT
.L1757:
	movl	$192, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1750:
	addq	$8, %r12
	cmpq	%r12, %r15
	je	.L2789
.L1758:
	movq	(%r12), %r14
	testq	%r14, %r14
	je	.L1750
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2790
	movq	%r14, %rdi
	call	*%rax
	jmp	.L1750
	.p2align 4,,10
	.p2align 3
.L2789:
	movq	-128(%rbp), %r13
	movq	0(%r13), %r12
.L1749:
	testq	%r12, %r12
	je	.L1759
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1759:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1748:
	movq	-96(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L1760
	call	_ZdlPv@PLT
.L1760:
	movq	-96(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L1761
	call	_ZdlPv@PLT
.L1761:
	movq	-96(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L1762
	call	_ZdlPv@PLT
.L1762:
	movq	-96(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L1686:
	movq	8(%rbx), %r12
	testq	%r12, %r12
	je	.L1763
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1764
	movq	160(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L1765
	movq	8(%r13), %rax
	movq	0(%r13), %r15
	cmpq	%r15, %rax
	je	.L1766
	movq	%rbx, -96(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r12, -128(%rbp)
	jmp	.L1773
	.p2align 4,,10
	.p2align 3
.L2792:
	movq	16(%r14), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r14)
	testq	%r12, %r12
	je	.L1769
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1770
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1769:
	movq	8(%r14), %r12
	testq	%r12, %r12
	je	.L1771
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1772
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1771:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1767:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L2791
.L1773:
	movq	(%rbx), %r14
	testq	%r14, %r14
	je	.L1767
	movq	(%r14), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L2792
	movq	%r14, %rdi
	call	*%rdx
	jmp	.L1767
	.p2align 4,,10
	.p2align 3
.L2799:
	movq	-128(%rbp), %r14
	movq	-96(%rbp), %r12
	movq	(%r14), %r13
.L1776:
	testq	%r13, %r13
	je	.L1786
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1786:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1775:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1787
	call	_ZdlPv@PLT
.L1787:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1788
	call	_ZdlPv@PLT
.L1788:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1789
	call	_ZdlPv@PLT
.L1789:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1763:
	movl	$24, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
	jmp	.L1684
	.p2align 4,,10
	.p2align 3
.L2803:
	movq	-120(%rbp), %r14
	movq	-88(%rbp), %r12
	movq	(%r14), %r13
.L2112:
	testq	%r13, %r13
	je	.L2122
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L2122:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2111:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2123
	call	_ZdlPv@PLT
.L2123:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2124
	call	_ZdlPv@PLT
.L2124:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2125
	call	_ZdlPv@PLT
.L2125:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2099:
	movl	$24, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
	jmp	.L1995
	.p2align 4,,10
	.p2align 3
.L2813:
	movq	-144(%rbp), %rax
	movq	-176(%rbp), %r12
	movq	-184(%rbp), %r13
	movq	(%rax), %r14
.L1893:
	testq	%r14, %r14
	je	.L1903
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1903:
	movq	-144(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1892:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1904
	call	_ZdlPv@PLT
.L1904:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1905
	call	_ZdlPv@PLT
.L1905:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1906
	call	_ZdlPv@PLT
.L1906:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1880:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1907
	call	_ZdlPv@PLT
.L1907:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1908
	call	_ZdlPv@PLT
.L1908:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1909
	call	_ZdlPv@PLT
.L1909:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1877:
	addq	$8, %rbx
	cmpq	%rbx, -112(%rbp)
	jne	.L1910
	movq	-136(%rbp), %rax
	movq	-168(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L1876:
	testq	%rdi, %rdi
	je	.L1911
	call	_ZdlPv@PLT
.L1911:
	movq	-136(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1875:
	movq	-80(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L1912
	call	_ZdlPv@PLT
.L1912:
	movq	-80(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L1913
	call	_ZdlPv@PLT
.L1913:
	movq	-80(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L1914
	call	_ZdlPv@PLT
.L1914:
	movq	-80(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L1863:
	movq	8(%rbx), %r12
	testq	%r12, %r12
	je	.L1915
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1916
	movq	160(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L1917
	movq	8(%r13), %rax
	movq	0(%r13), %r15
	cmpq	%r15, %rax
	je	.L1918
	movq	%rbx, -80(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r12, -112(%rbp)
	jmp	.L1925
	.p2align 4,,10
	.p2align 3
.L2794:
	movq	16(%r14), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r14)
	testq	%r12, %r12
	je	.L1921
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1922
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1921:
	movq	8(%r14), %r12
	testq	%r12, %r12
	je	.L1923
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1924
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1923:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1919:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L2793
.L1925:
	movq	(%rbx), %r14
	testq	%r14, %r14
	je	.L1919
	movq	(%r14), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L2794
	movq	%r14, %rdi
	call	*%rdx
	jmp	.L1919
	.p2align 4,,10
	.p2align 3
.L2807:
	movq	-112(%rbp), %r14
	movq	-80(%rbp), %r12
	movq	(%r14), %r13
.L1928:
	testq	%r13, %r13
	je	.L1938
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1938:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1927:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1939
	call	_ZdlPv@PLT
.L1939:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1940
	call	_ZdlPv@PLT
.L1940:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1941
	call	_ZdlPv@PLT
.L1941:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1915:
	movl	$24, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
	jmp	.L1861
	.p2align 4,,10
	.p2align 3
.L2823:
	movq	-144(%rbp), %rax
	movq	-176(%rbp), %r12
	movq	-184(%rbp), %r13
	movq	(%rax), %r14
.L2016:
	testq	%r14, %r14
	je	.L2026
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2026:
	movq	-144(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2015:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2027
	call	_ZdlPv@PLT
.L2027:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2028
	call	_ZdlPv@PLT
.L2028:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2029
	call	_ZdlPv@PLT
.L2029:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2003:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L2030
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2031
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -144(%rbp)
	testq	%rax, %rax
	je	.L2032
	movq	8(%rax), %rsi
	movq	(%rax), %r14
	cmpq	%r14, %rsi
	je	.L2033
	movq	%rbx, -184(%rbp)
	movq	%r14, %rbx
	movq	%rsi, %r14
	movq	%r12, -176(%rbp)
	jmp	.L2040
	.p2align 4,,10
	.p2align 3
.L2796:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L2036
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2037
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2036:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L2038
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2039
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2038:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L2034:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L2795
.L2040:
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.L2034
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L2796
	movq	%r15, %rdi
	call	*%rdx
	jmp	.L2034
	.p2align 4,,10
	.p2align 3
.L2817:
	movq	-144(%rbp), %rax
	movq	-176(%rbp), %r12
	movq	-184(%rbp), %r13
	movq	(%rax), %r14
.L2043:
	testq	%r14, %r14
	je	.L2053
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2053:
	movq	-144(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2042:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2054
	call	_ZdlPv@PLT
.L2054:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2055
	call	_ZdlPv@PLT
.L2055:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2056
	call	_ZdlPv@PLT
.L2056:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2030:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L2001
	.p2align 4,,10
	.p2align 3
.L2819:
	movq	-152(%rbp), %rax
	movq	-176(%rbp), %r12
	movq	-184(%rbp), %r13
	movq	(%rax), %r14
.L1705:
	testq	%r14, %r14
	je	.L1715
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1715:
	movq	-152(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1704:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1716
	call	_ZdlPv@PLT
.L1716:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1717
	call	_ZdlPv@PLT
.L1717:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1718
	call	_ZdlPv@PLT
.L1718:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1692:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L1719
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1720
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -152(%rbp)
	testq	%rax, %rax
	je	.L1721
	movq	8(%rax), %rsi
	movq	(%rax), %r14
	cmpq	%r14, %rsi
	je	.L1722
	movq	%rbx, -184(%rbp)
	movq	%r14, %rbx
	movq	%rsi, %r14
	movq	%r12, -176(%rbp)
	jmp	.L1729
	.p2align 4,,10
	.p2align 3
.L2798:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L1725
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1726
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1725:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L1727
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1728
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1727:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1723:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L2797
.L1729:
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.L1723
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L2798
	movq	%r15, %rdi
	call	*%rdx
	jmp	.L1723
	.p2align 4,,10
	.p2align 3
.L2825:
	movq	-152(%rbp), %rax
	movq	-176(%rbp), %r12
	movq	-184(%rbp), %r13
	movq	(%rax), %r14
.L1732:
	testq	%r14, %r14
	je	.L1742
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1742:
	movq	-152(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1731:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1743
	call	_ZdlPv@PLT
.L1743:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1744
	call	_ZdlPv@PLT
.L1744:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1745
	call	_ZdlPv@PLT
.L1745:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1719:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L1690
	.p2align 4,,10
	.p2align 3
.L2791:
	movq	-96(%rbp), %rbx
	movq	-128(%rbp), %r12
	movq	0(%r13), %r15
.L1766:
	testq	%r15, %r15
	je	.L1774
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L1774:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1765:
	movq	152(%r12), %r14
	testq	%r14, %r14
	je	.L1775
	movq	8(%r14), %r15
	movq	(%r14), %r13
	cmpq	%r13, %r15
	je	.L1776
	movq	%r12, -96(%rbp)
	movq	%r14, -128(%rbp)
	jmp	.L1785
	.p2align 4,,10
	.p2align 3
.L2800:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1779
	call	_ZdlPv@PLT
.L1779:
	movq	136(%r12), %r14
	testq	%r14, %r14
	je	.L1780
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	%r14, %rdi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1781
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1780:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1782
	call	_ZdlPv@PLT
.L1782:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1783
	call	_ZdlPv@PLT
.L1783:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1784
	call	_ZdlPv@PLT
.L1784:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1777:
	addq	$8, %r13
	cmpq	%r13, %r15
	je	.L2799
.L1785:
	movq	0(%r13), %r12
	testq	%r12, %r12
	je	.L1777
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L2800
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1777
	.p2align 4,,10
	.p2align 3
.L2779:
	movq	-128(%rbp), %r12
	movq	(%r12), %r14
.L1800:
	testq	%r14, %r14
	je	.L1808
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1808:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1799:
	movq	-96(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -144(%rbp)
	testq	%rax, %rax
	je	.L1809
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -128(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rcx
	je	.L1810
	movq	%rbx, -168(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L1844:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L1811
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1812
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1813
	call	_ZdlPv@PLT
.L1813:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L1814
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1815
	movq	160(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L1816
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L1817
	movq	%rbx, -176(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r12, -152(%rbp)
	movq	%r13, -184(%rbp)
	jmp	.L1824
	.p2align 4,,10
	.p2align 3
.L2802:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L1820
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1821
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1820:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L1822
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1823
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1822:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1818:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L2801
.L1824:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L1818
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L2802
	movq	%r12, %rdi
	call	*%rdx
	jmp	.L1818
	.p2align 4,,10
	.p2align 3
.L2787:
	movq	-88(%rbp), %rbx
	movq	-120(%rbp), %r12
	movq	0(%r13), %r15
.L2102:
	testq	%r15, %r15
	je	.L2110
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L2110:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2101:
	movq	152(%r12), %r14
	testq	%r14, %r14
	je	.L2111
	movq	8(%r14), %r15
	movq	(%r14), %r13
	cmpq	%r13, %r15
	je	.L2112
	movq	%r12, -88(%rbp)
	movq	%r14, -120(%rbp)
	jmp	.L2121
	.p2align 4,,10
	.p2align 3
.L2804:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2115
	call	_ZdlPv@PLT
.L2115:
	movq	136(%r12), %r14
	testq	%r14, %r14
	je	.L2116
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	%r14, %rdi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2117
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2116:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2118
	call	_ZdlPv@PLT
.L2118:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2119
	call	_ZdlPv@PLT
.L2119:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2120
	call	_ZdlPv@PLT
.L2120:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2113:
	addq	$8, %r13
	cmpq	%r13, %r15
	je	.L2803
.L2121:
	movq	0(%r13), %r12
	testq	%r12, %r12
	je	.L2113
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L2804
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2113
	.p2align 4,,10
	.p2align 3
.L2777:
	movq	-104(%rbp), %r12
	movq	-128(%rbp), %rbx
	movq	-136(%rbp), %r13
	movq	(%r14), %r15
.L2136:
	testq	%r15, %r15
	je	.L2144
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L2144:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2135:
	movq	152(%r13), %rax
	movq	%rax, -104(%rbp)
	testq	%rax, %rax
	je	.L2145
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L2146
	movq	%r12, -128(%rbp)
	movq	%r13, -136(%rbp)
	jmp	.L2155
	.p2align 4,,10
	.p2align 3
.L2806:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2149
	call	_ZdlPv@PLT
.L2149:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2150
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2151
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2150:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2152
	call	_ZdlPv@PLT
.L2152:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2153
	call	_ZdlPv@PLT
.L2153:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2154
	call	_ZdlPv@PLT
.L2154:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2147:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L2805
.L2155:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L2147
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2806
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2147
	.p2align 4,,10
	.p2align 3
.L2793:
	movq	-80(%rbp), %rbx
	movq	-112(%rbp), %r12
	movq	0(%r13), %r15
.L1918:
	testq	%r15, %r15
	je	.L1926
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L1926:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1917:
	movq	152(%r12), %r14
	testq	%r14, %r14
	je	.L1927
	movq	8(%r14), %r15
	movq	(%r14), %r13
	cmpq	%r13, %r15
	je	.L1928
	movq	%r12, -80(%rbp)
	movq	%r14, -112(%rbp)
	jmp	.L1937
	.p2align 4,,10
	.p2align 3
.L2808:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1931
	call	_ZdlPv@PLT
.L1931:
	movq	136(%r12), %r14
	testq	%r14, %r14
	je	.L1932
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r14, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1933
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1932:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1934
	call	_ZdlPv@PLT
.L1934:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1935
	call	_ZdlPv@PLT
.L1935:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1936
	call	_ZdlPv@PLT
.L1936:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1929:
	addq	$8, %r13
	cmpq	%r13, %r15
	je	.L2807
.L1937:
	movq	0(%r13), %r12
	testq	%r12, %r12
	je	.L1929
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L2808
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1929
	.p2align 4,,10
	.p2align 3
.L2783:
	movq	-96(%rbp), %r12
	movq	-128(%rbp), %rbx
	movq	-136(%rbp), %r13
	movq	(%r14), %r15
.L1952:
	testq	%r15, %r15
	je	.L1960
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L1960:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1951:
	movq	152(%r13), %rax
	movq	%rax, -96(%rbp)
	testq	%rax, %rax
	je	.L1961
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L1962
	movq	%r12, -128(%rbp)
	movq	%r13, -136(%rbp)
	jmp	.L1971
	.p2align 4,,10
	.p2align 3
.L2810:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1965
	call	_ZdlPv@PLT
.L1965:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L1966
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1967
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1966:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1968
	call	_ZdlPv@PLT
.L1968:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1969
	call	_ZdlPv@PLT
.L1969:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1970
	call	_ZdlPv@PLT
.L1970:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1963:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L2809
.L1971:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L1963
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L2810
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1963
	.p2align 4,,10
	.p2align 3
.L2781:
	movq	-112(%rbp), %r12
	movq	(%r12), %r14
.L1866:
	testq	%r14, %r14
	je	.L1874
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1874:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1865:
	movq	-80(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -136(%rbp)
	testq	%rax, %rax
	je	.L1875
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	%rsi, -112(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rsi
	je	.L1876
	movq	%rbx, -168(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L1910:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L1877
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1878
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1879
	call	_ZdlPv@PLT
.L1879:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L1880
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1881
	movq	160(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L1882
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L1883
	movq	%rbx, -176(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r12, -144(%rbp)
	movq	%r13, -184(%rbp)
	jmp	.L1890
	.p2align 4,,10
	.p2align 3
.L2812:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L1886
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1887
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1886:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L1888
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1889
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1888:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1884:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L2811
.L1890:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L1884
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L2812
	movq	%r12, %rdi
	call	*%rdx
	jmp	.L1884
	.p2align 4,,10
	.p2align 3
.L2811:
	movq	-144(%rbp), %r12
	movq	-176(%rbp), %rbx
	movq	-184(%rbp), %r13
	movq	(%r14), %r15
.L1883:
	testq	%r15, %r15
	je	.L1891
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L1891:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1882:
	movq	152(%r13), %rax
	movq	%rax, -144(%rbp)
	testq	%rax, %rax
	je	.L1892
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L1893
	movq	%r12, -176(%rbp)
	movq	%r13, -184(%rbp)
	jmp	.L1902
	.p2align 4,,10
	.p2align 3
.L2814:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1896
	call	_ZdlPv@PLT
.L1896:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L1897
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1898
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1897:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1899
	call	_ZdlPv@PLT
.L1899:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1900
	call	_ZdlPv@PLT
.L1900:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1901
	call	_ZdlPv@PLT
.L1901:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1894:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L2813
.L1902:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L1894
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L2814
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1894
	.p2align 4,,10
	.p2align 3
.L2801:
	movq	-152(%rbp), %r12
	movq	-176(%rbp), %rbx
	movq	-184(%rbp), %r13
	movq	(%r14), %r15
.L1817:
	testq	%r15, %r15
	je	.L1825
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L1825:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1816:
	movq	152(%r13), %rax
	movq	%rax, -152(%rbp)
	testq	%rax, %rax
	je	.L1826
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L1827
	movq	%r12, -176(%rbp)
	movq	%r13, -184(%rbp)
	jmp	.L1836
	.p2align 4,,10
	.p2align 3
.L2816:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1830
	call	_ZdlPv@PLT
.L1830:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L1831
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1832
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1831:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1833
	call	_ZdlPv@PLT
.L1833:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1834
	call	_ZdlPv@PLT
.L1834:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1835
	call	_ZdlPv@PLT
.L1835:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1828:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L2815
.L1836:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L1828
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L2816
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1828
	.p2align 4,,10
	.p2align 3
.L2795:
	movq	-144(%rbp), %rax
	movq	-176(%rbp), %r12
	movq	-184(%rbp), %rbx
	movq	(%rax), %r14
.L2033:
	testq	%r14, %r14
	je	.L2041
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2041:
	movq	-144(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2032:
	movq	152(%r13), %rax
	movq	%rax, -144(%rbp)
	testq	%rax, %rax
	je	.L2042
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L2043
	movq	%r12, -176(%rbp)
	movq	%r13, -184(%rbp)
	jmp	.L2052
	.p2align 4,,10
	.p2align 3
.L2818:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2046
	call	_ZdlPv@PLT
.L2046:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2047
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2048
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2047:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2049
	call	_ZdlPv@PLT
.L2049:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2050
	call	_ZdlPv@PLT
.L2050:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2051
	call	_ZdlPv@PLT
.L2051:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2044:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L2817
.L2052:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L2044
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L2818
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2044
	.p2align 4,,10
	.p2align 3
.L2773:
	movq	-152(%rbp), %rax
	movq	-176(%rbp), %r12
	movq	-184(%rbp), %rbx
	movq	(%rax), %r14
.L1695:
	testq	%r14, %r14
	je	.L1703
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1703:
	movq	-152(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1694:
	movq	152(%r13), %rax
	movq	%rax, -152(%rbp)
	testq	%rax, %rax
	je	.L1704
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L1705
	movq	%r12, -176(%rbp)
	movq	%r13, -184(%rbp)
	jmp	.L1714
	.p2align 4,,10
	.p2align 3
.L2820:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1708
	call	_ZdlPv@PLT
.L1708:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L1709
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1710
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1709:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1711
	call	_ZdlPv@PLT
.L1711:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1712
	call	_ZdlPv@PLT
.L1712:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1713
	call	_ZdlPv@PLT
.L1713:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1706:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L2819
.L1714:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L1706
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L2820
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1706
	.p2align 4,,10
	.p2align 3
.L2785:
	movq	-144(%rbp), %r12
	movq	-176(%rbp), %rbx
	movq	-184(%rbp), %r13
	movq	(%r14), %r15
.L2067:
	testq	%r15, %r15
	je	.L2075
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L2075:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2066:
	movq	152(%r13), %rax
	movq	%rax, -144(%rbp)
	testq	%rax, %rax
	je	.L2076
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L2077
	movq	%r12, -176(%rbp)
	movq	%r13, -184(%rbp)
	jmp	.L2086
	.p2align 4,,10
	.p2align 3
.L2822:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2080
	call	_ZdlPv@PLT
.L2080:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2081
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2082
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2081:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2083
	call	_ZdlPv@PLT
.L2083:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2084
	call	_ZdlPv@PLT
.L2084:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2085
	call	_ZdlPv@PLT
.L2085:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2078:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L2821
.L2086:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L2078
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L2822
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2078
	.p2align 4,,10
	.p2align 3
.L2775:
	movq	-144(%rbp), %rax
	movq	-176(%rbp), %r12
	movq	-184(%rbp), %rbx
	movq	(%rax), %r14
.L2006:
	testq	%r14, %r14
	je	.L2014
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2014:
	movq	-144(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2005:
	movq	152(%r13), %rax
	movq	%rax, -144(%rbp)
	testq	%rax, %rax
	je	.L2015
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L2016
	movq	%r12, -176(%rbp)
	movq	%r13, -184(%rbp)
	jmp	.L2025
	.p2align 4,,10
	.p2align 3
.L2824:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2019
	call	_ZdlPv@PLT
.L2019:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2020
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2021
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2020:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2022
	call	_ZdlPv@PLT
.L2022:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2023
	call	_ZdlPv@PLT
.L2023:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2024
	call	_ZdlPv@PLT
.L2024:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2017:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L2823
.L2025:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L2017
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2824
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2017
	.p2align 4,,10
	.p2align 3
.L2797:
	movq	-152(%rbp), %rax
	movq	-176(%rbp), %r12
	movq	-184(%rbp), %rbx
	movq	(%rax), %r14
.L1722:
	testq	%r14, %r14
	je	.L1730
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1730:
	movq	-152(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1721:
	movq	152(%r13), %rax
	movq	%rax, -152(%rbp)
	testq	%rax, %rax
	je	.L1731
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L1732
	movq	%r12, -176(%rbp)
	movq	%r13, -184(%rbp)
	jmp	.L1741
	.p2align 4,,10
	.p2align 3
.L2826:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1735
	call	_ZdlPv@PLT
.L1735:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L1736
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1737
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1736:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1738
	call	_ZdlPv@PLT
.L1738:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1739
	call	_ZdlPv@PLT
.L1739:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1740
	call	_ZdlPv@PLT
.L1740:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1733:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L2825
.L1741:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L1733
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2826
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1733
	.p2align 4,,10
	.p2align 3
.L1989:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L1988
	.p2align 4,,10
	.p2align 3
.L1679:
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L1678
	.p2align 4,,10
	.p2align 3
.L1675:
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L1674
	.p2align 4,,10
	.p2align 3
.L1671:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1670
	.p2align 4,,10
	.p2align 3
.L1795:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L1794
	.p2align 4,,10
	.p2align 3
.L1947:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1946
	.p2align 4,,10
	.p2align 3
.L2131:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2130
	.p2align 4,,10
	.p2align 3
.L1681:
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L1680
	.p2align 4,,10
	.p2align 3
.L1992:
	movq	%rsi, %rdi
	call	*%rax
	jmp	.L1991
	.p2align 4,,10
	.p2align 3
.L1858:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L1857
	.p2align 4,,10
	.p2align 3
.L2062:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2061
	.p2align 4,,10
	.p2align 3
.L1878:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1877
	.p2align 4,,10
	.p2align 3
.L1812:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1811
	.p2align 4,,10
	.p2align 3
.L1687:
	movq	%rdx, %rdi
	call	*%rax
	jmp	.L1686
	.p2align 4,,10
	.p2align 3
.L1916:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1915
	.p2align 4,,10
	.p2align 3
.L1998:
	movq	%rdx, %rdi
	call	*%rax
	jmp	.L1997
	.p2align 4,,10
	.p2align 3
.L1950:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1949
	.p2align 4,,10
	.p2align 3
.L1764:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1763
	.p2align 4,,10
	.p2align 3
.L1864:
	movq	%rsi, %rdi
	call	*%rax
	jmp	.L1863
	.p2align 4,,10
	.p2align 3
.L2100:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2099
	.p2align 4,,10
	.p2align 3
.L2134:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2133
	.p2align 4,,10
	.p2align 3
.L1798:
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L1797
	.p2align 4,,10
	.p2align 3
.L2106:
	call	*%rdx
	jmp	.L2105
	.p2align 4,,10
	.p2align 3
.L1804:
	call	*%rax
	jmp	.L1803
	.p2align 4,,10
	.p2align 3
.L1956:
	call	*%rdx
	jmp	.L1955
	.p2align 4,,10
	.p2align 3
.L2031:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2030
	.p2align 4,,10
	.p2align 3
.L1881:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1880
	.p2align 4,,10
	.p2align 3
.L1870:
	call	*%rax
	jmp	.L1869
	.p2align 4,,10
	.p2align 3
.L2140:
	call	*%rdx
	jmp	.L2139
	.p2align 4,,10
	.p2align 3
.L1967:
	call	*%rax
	jmp	.L1966
	.p2align 4,,10
	.p2align 3
.L1924:
	call	*%rdx
	jmp	.L1923
	.p2align 4,,10
	.p2align 3
.L1815:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1814
	.p2align 4,,10
	.p2align 3
.L1922:
	call	*%rdx
	jmp	.L1921
	.p2align 4,,10
	.p2align 3
.L2065:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2064
	.p2align 4,,10
	.p2align 3
.L1720:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1719
	.p2align 4,,10
	.p2align 3
.L2117:
	call	*%rax
	jmp	.L2116
	.p2align 4,,10
	.p2align 3
.L1872:
	call	*%rax
	jmp	.L1871
	.p2align 4,,10
	.p2align 3
.L2142:
	call	*%rdx
	jmp	.L2141
	.p2align 4,,10
	.p2align 3
.L1770:
	call	*%rdx
	jmp	.L1769
	.p2align 4,,10
	.p2align 3
.L1754:
	call	*%rax
	jmp	.L1753
	.p2align 4,,10
	.p2align 3
.L1772:
	call	*%rdx
	jmp	.L1771
	.p2align 4,,10
	.p2align 3
.L1958:
	call	*%rdx
	jmp	.L1957
	.p2align 4,,10
	.p2align 3
.L1806:
	call	*%rax
	jmp	.L1805
	.p2align 4,,10
	.p2align 3
.L1933:
	call	*%rax
	jmp	.L1932
	.p2align 4,,10
	.p2align 3
.L2151:
	call	*%rax
	jmp	.L2150
	.p2align 4,,10
	.p2align 3
.L2108:
	call	*%rdx
	jmp	.L2107
	.p2align 4,,10
	.p2align 3
.L1781:
	call	*%rax
	jmp	.L1780
	.p2align 4,,10
	.p2align 3
.L2004:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2003
	.p2align 4,,10
	.p2align 3
.L1693:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1692
	.p2align 4,,10
	.p2align 3
.L2071:
	call	*%rdx
	jmp	.L2070
	.p2align 4,,10
	.p2align 3
.L1701:
	call	*%rdx
	jmp	.L1700
	.p2align 4,,10
	.p2align 3
.L1699:
	call	*%rdx
	jmp	.L1698
	.p2align 4,,10
	.p2align 3
.L2012:
	call	*%rdx
	jmp	.L2011
	.p2align 4,,10
	.p2align 3
.L2010:
	call	*%rdx
	jmp	.L2009
	.p2align 4,,10
	.p2align 3
.L1737:
	call	*%rax
	jmp	.L1736
	.p2align 4,,10
	.p2align 3
.L2021:
	call	*%rax
	jmp	.L2020
	.p2align 4,,10
	.p2align 3
.L2082:
	call	*%rax
	jmp	.L2081
	.p2align 4,,10
	.p2align 3
.L1710:
	call	*%rax
	jmp	.L1709
	.p2align 4,,10
	.p2align 3
.L2037:
	call	*%rdx
	jmp	.L2036
	.p2align 4,,10
	.p2align 3
.L2073:
	call	*%rdx
	jmp	.L2072
	.p2align 4,,10
	.p2align 3
.L2048:
	call	*%rax
	jmp	.L2047
	.p2align 4,,10
	.p2align 3
.L1832:
	call	*%rax
	jmp	.L1831
	.p2align 4,,10
	.p2align 3
.L1898:
	call	*%rax
	jmp	.L1897
	.p2align 4,,10
	.p2align 3
.L1823:
	call	*%rdx
	jmp	.L1822
	.p2align 4,,10
	.p2align 3
.L1889:
	call	*%rdx
	jmp	.L1888
	.p2align 4,,10
	.p2align 3
.L1887:
	call	*%rdx
	jmp	.L1886
	.p2align 4,,10
	.p2align 3
.L1821:
	call	*%rdx
	jmp	.L1820
	.p2align 4,,10
	.p2align 3
.L1728:
	call	*%rdx
	jmp	.L1727
	.p2align 4,,10
	.p2align 3
.L1726:
	call	*%rdx
	jmp	.L1725
	.p2align 4,,10
	.p2align 3
.L2039:
	call	*%rdx
	jmp	.L2038
	.cfi_endproc
.LFE4459:
	.size	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev, .-_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime12RemoteObjectD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12RemoteObjectD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD2Ev, @function
_ZN12v8_inspector8protocol7Runtime12RemoteObjectD2Ev:
.LFB4457:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12RemoteObjectE(%rip), %rax
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	punpcklqdq	%xmm1, %xmm0
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	312(%rdi), %r12
	movups	%xmm0, (%rdi)
	testq	%r12, %r12
	je	.L2828
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2829
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE(%rip), %rax
	movq	56(%r12), %rdi
	movq	%rax, (%r12)
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2830
	call	_ZdlPv@PLT
.L2830:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2831
	call	_ZdlPv@PLT
.L2831:
	movl	$96, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2828:
	movq	304(%rbx), %rcx
	movq	%rcx, -64(%rbp)
	testq	%rcx, %rcx
	je	.L2832
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2833
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rcx)
	movq	160(%rcx), %rax
	movq	%rax, -96(%rbp)
	testq	%rax, %rax
	je	.L2834
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -80(%rbp)
	movq	%rax, -56(%rbp)
	cmpq	%rax, %rdx
	je	.L2835
	movq	%rbx, -120(%rbp)
	.p2align 4,,10
	.p2align 3
.L2942:
	movq	-56(%rbp), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.L2836
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2837
	movq	16(%rbx), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%rbx)
	testq	%r12, %r12
	je	.L2838
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2839
	movq	160(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L2840
	movq	8(%r13), %rax
	movq	0(%r13), %r15
	cmpq	%r15, %rax
	je	.L2841
	movq	%rbx, -72(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r12, -88(%rbp)
	jmp	.L2848
	.p2align 4,,10
	.p2align 3
.L3338:
	movq	16(%r14), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r14)
	testq	%r12, %r12
	je	.L2844
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2845
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2844:
	movq	8(%r14), %r12
	testq	%r12, %r12
	je	.L2846
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2847
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2846:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2842:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L3337
.L2848:
	movq	(%rbx), %r14
	testq	%r14, %r14
	je	.L2842
	movq	(%r14), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L3338
	addq	$8, %rbx
	movq	%r14, %rdi
	call	*%rdx
	cmpq	%rbx, %r15
	jne	.L2848
	.p2align 4,,10
	.p2align 3
.L3337:
	movq	-72(%rbp), %rbx
	movq	-88(%rbp), %r12
	movq	0(%r13), %r15
.L2841:
	testq	%r15, %r15
	je	.L2849
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L2849:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2840:
	movq	152(%r12), %r14
	testq	%r14, %r14
	je	.L2850
	movq	8(%r14), %r15
	movq	(%r14), %r13
	cmpq	%r13, %r15
	je	.L2851
	movq	%r12, -72(%rbp)
	movq	%r14, -88(%rbp)
	jmp	.L2860
	.p2align 4,,10
	.p2align 3
.L3340:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2854
	call	_ZdlPv@PLT
.L2854:
	movq	136(%r12), %r14
	testq	%r14, %r14
	je	.L2855
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	%r14, %rdi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2856
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2855:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2857
	call	_ZdlPv@PLT
.L2857:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2858
	call	_ZdlPv@PLT
.L2858:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2859
	call	_ZdlPv@PLT
.L2859:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2852:
	addq	$8, %r13
	cmpq	%r13, %r15
	je	.L3339
.L2860:
	movq	0(%r13), %r12
	testq	%r12, %r12
	je	.L2852
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L3340
	addq	$8, %r13
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r13, %r15
	jne	.L2860
	.p2align 4,,10
	.p2align 3
.L3339:
	movq	-88(%rbp), %r14
	movq	-72(%rbp), %r12
	movq	(%r14), %r13
.L2851:
	testq	%r13, %r13
	je	.L2861
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L2861:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2850:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2862
	call	_ZdlPv@PLT
.L2862:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2863
	call	_ZdlPv@PLT
.L2863:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2864
	call	_ZdlPv@PLT
.L2864:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2838:
	movq	8(%rbx), %rcx
	movq	%rcx, -72(%rbp)
	testq	%rcx, %rcx
	je	.L2865
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2866
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rcx)
	movq	160(%rcx), %rax
	movq	%rax, -104(%rbp)
	testq	%rax, %rax
	je	.L2867
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -88(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rcx
	je	.L2868
	movq	%rbx, -128(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L2925:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L2869
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2870
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L2871
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2872
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L2873
	movq	8(%rax), %rsi
	movq	(%rax), %r14
	cmpq	%r14, %rsi
	je	.L2874
	movq	%rbx, -144(%rbp)
	movq	%r14, %rbx
	movq	%rsi, %r14
	movq	%r12, -136(%rbp)
	jmp	.L2881
	.p2align 4,,10
	.p2align 3
.L3342:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L2877
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2878
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2877:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L2879
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2880
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2879:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L2875:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L3341
.L2881:
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.L2875
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L3342
	movq	%r15, %rdi
	call	*%rdx
	jmp	.L2875
	.p2align 4,,10
	.p2align 3
.L2837:
	movq	%rbx, %rdi
	call	*%rax
	.p2align 4,,10
	.p2align 3
.L2836:
	addq	$8, -56(%rbp)
	movq	-56(%rbp), %rax
	cmpq	%rax, -80(%rbp)
	jne	.L2942
	movq	-96(%rbp), %rax
	movq	-120(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, -56(%rbp)
.L2835:
	movq	-56(%rbp), %rax
	testq	%rax, %rax
	je	.L2943
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L2943:
	movq	-96(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2834:
	movq	-64(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -96(%rbp)
	testq	%rax, %rax
	je	.L2944
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	%rsi, -80(%rbp)
	movq	%rax, -56(%rbp)
	cmpq	%rax, %rsi
	je	.L2945
	movq	%rbx, -120(%rbp)
	.p2align 4,,10
	.p2align 3
.L3054:
	movq	-56(%rbp), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.L2946
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2947
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	168(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2948
	call	_ZdlPv@PLT
.L2948:
	movq	136(%rbx), %rsi
	movq	%rsi, -72(%rbp)
	testq	%rsi, %rsi
	je	.L2949
	movq	(%rsi), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2950
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rsi)
	movq	160(%rsi), %rax
	movq	%rax, -104(%rbp)
	testq	%rax, %rax
	je	.L2951
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -88(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rcx
	je	.L2952
	movq	%rbx, -128(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L3009:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L2953
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2954
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L2955
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2956
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L2957
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	cmpq	%r14, %rcx
	je	.L2958
	movq	%rbx, -136(%rbp)
	movq	%r14, %rbx
	movq	%rcx, %r14
	movq	%r12, -144(%rbp)
	jmp	.L2965
	.p2align 4,,10
	.p2align 3
.L3344:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L2961
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2962
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2961:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L2963
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2964
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2963:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L2959:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L3343
.L2965:
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.L2959
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L3344
	movq	%r15, %rdi
	call	*%rdx
	jmp	.L2959
	.p2align 4,,10
	.p2align 3
.L3355:
	movq	-112(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %r13
	movq	(%rax), %r14
.L3029:
	testq	%r14, %r14
	je	.L3039
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3039:
	movq	-112(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3028:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3040
	call	_ZdlPv@PLT
.L3040:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3041
	call	_ZdlPv@PLT
.L3041:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3042
	call	_ZdlPv@PLT
.L3042:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3016:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3043
	call	_ZdlPv@PLT
.L3043:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3044
	call	_ZdlPv@PLT
.L3044:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3045
	call	_ZdlPv@PLT
.L3045:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3013:
	addq	$8, %rbx
	cmpq	%rbx, -88(%rbp)
	jne	.L3046
	movq	-104(%rbp), %rax
	movq	-128(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L3012:
	testq	%rdi, %rdi
	je	.L3047
	call	_ZdlPv@PLT
.L3047:
	movq	-104(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3011:
	movq	-72(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L3048
	call	_ZdlPv@PLT
.L3048:
	movq	-72(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L3049
	call	_ZdlPv@PLT
.L3049:
	movq	-72(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L3050
	call	_ZdlPv@PLT
.L3050:
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L2949:
	movq	96(%rbx), %rdi
	leaq	112(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3051
	call	_ZdlPv@PLT
.L3051:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3052
	call	_ZdlPv@PLT
.L3052:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3053
	call	_ZdlPv@PLT
.L3053:
	movl	$192, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L2946:
	addq	$8, -56(%rbp)
	movq	-56(%rbp), %rax
	cmpq	%rax, -80(%rbp)
	jne	.L3054
	movq	-96(%rbp), %rax
	movq	-120(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, -56(%rbp)
.L2945:
	movq	-56(%rbp), %rax
	testq	%rax, %rax
	je	.L3055
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L3055:
	movq	-96(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2944:
	movq	-64(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L3056
	call	_ZdlPv@PLT
.L3056:
	movq	-64(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L3057
	call	_ZdlPv@PLT
.L3057:
	movq	-64(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L3058
	call	_ZdlPv@PLT
.L3058:
	movq	-64(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L2832:
	movq	264(%rbx), %rdi
	leaq	280(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3059
	call	_ZdlPv@PLT
.L3059:
	movq	216(%rbx), %rdi
	leaq	232(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3060
	call	_ZdlPv@PLT
.L3060:
	movq	168(%rbx), %rdi
	leaq	184(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3061
	call	_ZdlPv@PLT
.L3061:
	movq	152(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L3062
	movq	(%rdi), %rax
	call	*24(%rax)
.L3062:
	movq	112(%rbx), %rdi
	leaq	128(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3063
	call	_ZdlPv@PLT
.L3063:
	movq	64(%rbx), %rdi
	leaq	80(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3064
	call	_ZdlPv@PLT
.L3064:
	movq	16(%rbx), %r8
	leaq	32(%rbx), %rdi
	cmpq	%rdi, %r8
	je	.L2827
	addq	$104, %rsp
	movq	%r8, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L2827:
	.cfi_restore_state
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2954:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	.p2align 4,,10
	.p2align 3
.L2953:
	addq	$8, %rbx
	cmpq	%rbx, -88(%rbp)
	jne	.L3009
	movq	-104(%rbp), %rax
	movq	-128(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L2952:
	testq	%rdi, %rdi
	je	.L3010
	call	_ZdlPv@PLT
.L3010:
	movq	-104(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2951:
	movq	-72(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -104(%rbp)
	testq	%rax, %rax
	je	.L3011
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -88(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rdx
	je	.L3012
	movq	%rbx, -128(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L3046:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L3013
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3014
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3015
	call	_ZdlPv@PLT
.L3015:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L3016
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L3017
	movq	160(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L3018
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L3019
	movq	%rbx, -136(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r12, -112(%rbp)
	movq	%r13, -144(%rbp)
	jmp	.L3026
	.p2align 4,,10
	.p2align 3
.L3346:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L3022
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3023
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3022:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L3024
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3025
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3024:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3020:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L3345
.L3026:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L3020
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L3346
	movq	%r12, %rdi
	call	*%rdx
	jmp	.L3020
	.p2align 4,,10
	.p2align 3
.L2870:
	movq	%r12, %rdi
	call	*%rax
	.p2align 4,,10
	.p2align 3
.L2869:
	addq	$8, %rbx
	cmpq	%rbx, -88(%rbp)
	jne	.L2925
	movq	-104(%rbp), %rax
	movq	-128(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L2868:
	testq	%rdi, %rdi
	je	.L2926
	call	_ZdlPv@PLT
.L2926:
	movq	-104(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2867:
	movq	-72(%rbp), %rax
	movq	152(%rax), %r13
	testq	%r13, %r13
	je	.L2927
	movq	8(%r13), %r14
	movq	0(%r13), %r12
	cmpq	%r12, %r14
	je	.L2928
	movq	%r13, -88(%rbp)
	jmp	.L2937
	.p2align 4,,10
	.p2align 3
.L3348:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r15), %rdi
	movq	%rax, (%r15)
	leaq	168(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2931
	call	_ZdlPv@PLT
.L2931:
	movq	136(%r15), %r13
	testq	%r13, %r13
	je	.L2932
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2933
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2932:
	movq	96(%r15), %rdi
	leaq	112(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2934
	call	_ZdlPv@PLT
.L2934:
	movq	48(%r15), %rdi
	leaq	64(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2935
	call	_ZdlPv@PLT
.L2935:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2936
	call	_ZdlPv@PLT
.L2936:
	movl	$192, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L2929:
	addq	$8, %r12
	cmpq	%r12, %r14
	je	.L3347
.L2937:
	movq	(%r12), %r15
	testq	%r15, %r15
	je	.L2929
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L3348
	addq	$8, %r12
	movq	%r15, %rdi
	call	*%rax
	cmpq	%r12, %r14
	jne	.L2937
	.p2align 4,,10
	.p2align 3
.L3347:
	movq	-88(%rbp), %r13
	movq	0(%r13), %r12
.L2928:
	testq	%r12, %r12
	je	.L2938
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2938:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2927:
	movq	-72(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L2939
	call	_ZdlPv@PLT
.L2939:
	movq	-72(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L2940
	call	_ZdlPv@PLT
.L2940:
	movq	-72(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L2941
	call	_ZdlPv@PLT
.L2941:
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L2865:
	movl	$24, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
	jmp	.L2836
	.p2align 4,,10
	.p2align 3
.L3357:
	movq	-112(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %r13
	movq	(%rax), %r14
.L2884:
	testq	%r14, %r14
	je	.L2894
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2894:
	movq	-112(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2883:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2895
	call	_ZdlPv@PLT
.L2895:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2896
	call	_ZdlPv@PLT
.L2896:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2897
	call	_ZdlPv@PLT
.L2897:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2871:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L2898
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2899
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L2900
	movq	8(%rax), %rsi
	movq	(%rax), %r14
	cmpq	%r14, %rsi
	je	.L2901
	movq	%rbx, -144(%rbp)
	movq	%r14, %rbx
	movq	%rsi, %r14
	movq	%r12, -136(%rbp)
	jmp	.L2908
	.p2align 4,,10
	.p2align 3
.L3350:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L2904
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2905
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2904:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L2906
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2907
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2906:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L2902:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L3349
.L2908:
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.L2902
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L3350
	movq	%r15, %rdi
	call	*%rdx
	jmp	.L2902
	.p2align 4,,10
	.p2align 3
.L3359:
	movq	-112(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %r13
	movq	(%rax), %r14
.L2911:
	testq	%r14, %r14
	je	.L2921
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2921:
	movq	-112(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2910:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2922
	call	_ZdlPv@PLT
.L2922:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2923
	call	_ZdlPv@PLT
.L2923:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2924
	call	_ZdlPv@PLT
.L2924:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2898:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L2869
	.p2align 4,,10
	.p2align 3
.L3361:
	movq	-112(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %r13
	movq	(%rax), %r14
.L2968:
	testq	%r14, %r14
	je	.L2978
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2978:
	movq	-112(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2967:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2979
	call	_ZdlPv@PLT
.L2979:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2980
	call	_ZdlPv@PLT
.L2980:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2981
	call	_ZdlPv@PLT
.L2981:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2955:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L2982
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2983
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L2984
	movq	8(%rax), %rdx
	movq	(%rax), %r14
	cmpq	%r14, %rdx
	je	.L2985
	movq	%rbx, -136(%rbp)
	movq	%r14, %rbx
	movq	%rdx, %r14
	movq	%r12, -144(%rbp)
	jmp	.L2992
	.p2align 4,,10
	.p2align 3
.L3352:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L2988
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2989
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2988:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L2990
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2991
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2990:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L2986:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L3351
.L2992:
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.L2986
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L3352
	movq	%r15, %rdi
	call	*%rdx
	jmp	.L2986
	.p2align 4,,10
	.p2align 3
.L3353:
	movq	-112(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %r13
	movq	(%rax), %r14
.L2995:
	testq	%r14, %r14
	je	.L3005
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3005:
	movq	-112(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2994:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3006
	call	_ZdlPv@PLT
.L3006:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3007
	call	_ZdlPv@PLT
.L3007:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3008
	call	_ZdlPv@PLT
.L3008:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2982:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L2953
	.p2align 4,,10
	.p2align 3
.L3351:
	movq	-112(%rbp), %rax
	movq	-136(%rbp), %rbx
	movq	-144(%rbp), %r12
	movq	(%rax), %r14
.L2985:
	testq	%r14, %r14
	je	.L2993
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2993:
	movq	-112(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2984:
	movq	152(%r13), %rax
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L2994
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L2995
	movq	%r12, -136(%rbp)
	movq	%r13, -144(%rbp)
	jmp	.L3004
	.p2align 4,,10
	.p2align 3
.L3354:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2998
	call	_ZdlPv@PLT
.L2998:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2999
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L3000
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2999:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3001
	call	_ZdlPv@PLT
.L3001:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3002
	call	_ZdlPv@PLT
.L3002:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3003
	call	_ZdlPv@PLT
.L3003:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2996:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L3353
.L3004:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L2996
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L3354
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2996
	.p2align 4,,10
	.p2align 3
.L3345:
	movq	-112(%rbp), %r12
	movq	-136(%rbp), %rbx
	movq	-144(%rbp), %r13
	movq	(%r14), %r15
.L3019:
	testq	%r15, %r15
	je	.L3027
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L3027:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L3018:
	movq	152(%r13), %rax
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L3028
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L3029
	movq	%r12, -136(%rbp)
	movq	%r13, -144(%rbp)
	jmp	.L3038
	.p2align 4,,10
	.p2align 3
.L3356:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3032
	call	_ZdlPv@PLT
.L3032:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L3033
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3034
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3033:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3035
	call	_ZdlPv@PLT
.L3035:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3036
	call	_ZdlPv@PLT
.L3036:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3037
	call	_ZdlPv@PLT
.L3037:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3030:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L3355
.L3038:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L3030
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L3356
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3030
	.p2align 4,,10
	.p2align 3
.L3341:
	movq	-112(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %rbx
	movq	(%rax), %r14
.L2874:
	testq	%r14, %r14
	je	.L2882
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2882:
	movq	-112(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2873:
	movq	152(%r13), %rax
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L2883
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L2884
	movq	%r12, -136(%rbp)
	movq	%r13, -144(%rbp)
	jmp	.L2893
	.p2align 4,,10
	.p2align 3
.L3358:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2887
	call	_ZdlPv@PLT
.L2887:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2888
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2889
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2888:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2890
	call	_ZdlPv@PLT
.L2890:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2891
	call	_ZdlPv@PLT
.L2891:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2892
	call	_ZdlPv@PLT
.L2892:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2885:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L3357
.L2893:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L2885
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L3358
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2885
	.p2align 4,,10
	.p2align 3
.L3349:
	movq	-112(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %rbx
	movq	(%rax), %r14
.L2901:
	testq	%r14, %r14
	je	.L2909
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2909:
	movq	-112(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2900:
	movq	152(%r13), %rax
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L2910
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L2911
	movq	%r12, -136(%rbp)
	movq	%r13, -144(%rbp)
	jmp	.L2920
	.p2align 4,,10
	.p2align 3
.L3360:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2914
	call	_ZdlPv@PLT
.L2914:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2915
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2916
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2915:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2917
	call	_ZdlPv@PLT
.L2917:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2918
	call	_ZdlPv@PLT
.L2918:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2919
	call	_ZdlPv@PLT
.L2919:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2912:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L3359
.L2920:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L2912
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L3360
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2912
	.p2align 4,,10
	.p2align 3
.L3343:
	movq	-112(%rbp), %rax
	movq	-136(%rbp), %rbx
	movq	-144(%rbp), %r12
	movq	(%rax), %r14
.L2958:
	testq	%r14, %r14
	je	.L2966
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2966:
	movq	-112(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2957:
	movq	152(%r13), %rax
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L2967
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L2968
	movq	%r12, -136(%rbp)
	movq	%r13, -144(%rbp)
	jmp	.L2977
	.p2align 4,,10
	.p2align 3
.L3362:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2971
	call	_ZdlPv@PLT
.L2971:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2972
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2973
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2972:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2974
	call	_ZdlPv@PLT
.L2974:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2975
	call	_ZdlPv@PLT
.L2975:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2976
	call	_ZdlPv@PLT
.L2976:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2969:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L3361
.L2977:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L2969
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L3362
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2969
	.p2align 4,,10
	.p2align 3
.L2947:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L2946
	.p2align 4,,10
	.p2align 3
.L2829:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2828
	.p2align 4,,10
	.p2align 3
.L2833:
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L2832
	.p2align 4,,10
	.p2align 3
.L3014:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3013
	.p2align 4,,10
	.p2align 3
.L2839:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2838
	.p2align 4,,10
	.p2align 3
.L2866:
	movq	%rcx, %rdi
	call	*%rax
	movl	$24, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
	jmp	.L2836
	.p2align 4,,10
	.p2align 3
.L2950:
	movq	%rsi, %rdi
	call	*%rax
	jmp	.L2949
	.p2align 4,,10
	.p2align 3
.L2956:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2955
	.p2align 4,,10
	.p2align 3
.L2845:
	call	*%rdx
	jmp	.L2844
	.p2align 4,,10
	.p2align 3
.L2872:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2871
	.p2align 4,,10
	.p2align 3
.L2933:
	call	*%rax
	jmp	.L2932
	.p2align 4,,10
	.p2align 3
.L3017:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L3016
	.p2align 4,,10
	.p2align 3
.L2847:
	call	*%rdx
	jmp	.L2846
	.p2align 4,,10
	.p2align 3
.L2856:
	call	*%rax
	jmp	.L2855
	.p2align 4,,10
	.p2align 3
.L2983:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2982
	.p2align 4,,10
	.p2align 3
.L2899:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2898
	.p2align 4,,10
	.p2align 3
.L3023:
	call	*%rdx
	jmp	.L3022
	.p2align 4,,10
	.p2align 3
.L2964:
	call	*%rdx
	jmp	.L2963
	.p2align 4,,10
	.p2align 3
.L2989:
	call	*%rdx
	jmp	.L2988
	.p2align 4,,10
	.p2align 3
.L2907:
	call	*%rdx
	jmp	.L2906
	.p2align 4,,10
	.p2align 3
.L2973:
	call	*%rax
	jmp	.L2972
	.p2align 4,,10
	.p2align 3
.L2991:
	call	*%rdx
	jmp	.L2990
	.p2align 4,,10
	.p2align 3
.L3000:
	call	*%rax
	jmp	.L2999
	.p2align 4,,10
	.p2align 3
.L2905:
	call	*%rdx
	jmp	.L2904
	.p2align 4,,10
	.p2align 3
.L2962:
	call	*%rdx
	jmp	.L2961
	.p2align 4,,10
	.p2align 3
.L2880:
	call	*%rdx
	jmp	.L2879
	.p2align 4,,10
	.p2align 3
.L3025:
	call	*%rdx
	jmp	.L3024
	.p2align 4,,10
	.p2align 3
.L3034:
	call	*%rax
	jmp	.L3033
	.p2align 4,,10
	.p2align 3
.L2878:
	call	*%rdx
	jmp	.L2877
	.p2align 4,,10
	.p2align 3
.L2889:
	call	*%rax
	jmp	.L2888
	.p2align 4,,10
	.p2align 3
.L2916:
	call	*%rax
	jmp	.L2915
	.cfi_endproc
.LFE4457:
	.size	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD2Ev, .-_ZN12v8_inspector8protocol7Runtime12RemoteObjectD2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev,_ZN12v8_inspector8protocol7Runtime12RemoteObjectD2Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime25PrivatePropertyDescriptorD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime25PrivatePropertyDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime25PrivatePropertyDescriptorD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime25PrivatePropertyDescriptorD0Ev, @function
_ZN12v8_inspector8protocol7Runtime25PrivatePropertyDescriptorD0Ev:
.LFB4904:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime25PrivatePropertyDescriptorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	48(%rdi), %r13
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L3364
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3365
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3364:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3366
	call	_ZdlPv@PLT
.L3366:
	movq	%r12, %rdi
	movl	$56, %esi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L3365:
	.cfi_restore_state
	call	*%rax
	jmp	.L3364
	.cfi_endproc
.LFE4904:
	.size	_ZN12v8_inspector8protocol7Runtime25PrivatePropertyDescriptorD0Ev, .-_ZN12v8_inspector8protocol7Runtime25PrivatePropertyDescriptorD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime26InternalPropertyDescriptorD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime26InternalPropertyDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime26InternalPropertyDescriptorD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime26InternalPropertyDescriptorD0Ev, @function
_ZN12v8_inspector8protocol7Runtime26InternalPropertyDescriptorD0Ev:
.LFB4875:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime26InternalPropertyDescriptorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	48(%rdi), %r13
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L3372
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3373
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3372:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3374
	call	_ZdlPv@PLT
.L3374:
	movq	%r12, %rdi
	movl	$56, %esi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L3373:
	.cfi_restore_state
	call	*%rax
	jmp	.L3372
	.cfi_endproc
.LFE4875:
	.size	_ZN12v8_inspector8protocol7Runtime26InternalPropertyDescriptorD0Ev, .-_ZN12v8_inspector8protocol7Runtime26InternalPropertyDescriptorD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime25PrivatePropertyDescriptorD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime25PrivatePropertyDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime25PrivatePropertyDescriptorD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime25PrivatePropertyDescriptorD2Ev, @function
_ZN12v8_inspector8protocol7Runtime25PrivatePropertyDescriptorD2Ev:
.LFB4902:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime25PrivatePropertyDescriptorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	48(%rdi), %r12
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L3380
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3381
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3380:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L3379
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L3379:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3381:
	.cfi_restore_state
	call	*%rax
	jmp	.L3380
	.cfi_endproc
.LFE4902:
	.size	_ZN12v8_inspector8protocol7Runtime25PrivatePropertyDescriptorD2Ev, .-_ZN12v8_inspector8protocol7Runtime25PrivatePropertyDescriptorD2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime25PrivatePropertyDescriptorD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime25PrivatePropertyDescriptorD1Ev,_ZN12v8_inspector8protocol7Runtime25PrivatePropertyDescriptorD2Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime26InternalPropertyDescriptorD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime26InternalPropertyDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime26InternalPropertyDescriptorD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime26InternalPropertyDescriptorD2Ev, @function
_ZN12v8_inspector8protocol7Runtime26InternalPropertyDescriptorD2Ev:
.LFB4873:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime26InternalPropertyDescriptorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	48(%rdi), %r12
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L3388
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3389
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3388:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L3387
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L3387:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3389:
	.cfi_restore_state
	call	*%rax
	jmp	.L3388
	.cfi_endproc
.LFE4873:
	.size	_ZN12v8_inspector8protocol7Runtime26InternalPropertyDescriptorD2Ev, .-_ZN12v8_inspector8protocol7Runtime26InternalPropertyDescriptorD2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime26InternalPropertyDescriptorD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime26InternalPropertyDescriptorD1Ev,_ZN12v8_inspector8protocol7Runtime26InternalPropertyDescriptorD2Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime12RemoteObjectD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12RemoteObjectD5Ev,comdat
	.p2align 4
	.weak	_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	.type	_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD1Ev, @function
_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD1Ev:
.LFB14668:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12RemoteObjectE(%rip), %rdx
	leaq	-64(%rdx), %rax
	movq	%rdx, %xmm1
	movq	%rax, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	punpcklqdq	%xmm1, %xmm0
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	304(%rdi), %r12
	movups	%xmm0, -8(%rdi)
	testq	%r12, %r12
	je	.L3396
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev(%rip), %rcx
	movq	24(%rdx), %rdx
	cmpq	%rcx, %rdx
	je	.L3471
	movq	%r12, %rdi
	call	*%rdx
.L3396:
	movq	296(%r14), %r15
	testq	%r15, %r15
	je	.L3400
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %r13
	movq	24(%rdx), %rdx
	cmpq	%r13, %rdx
	je	.L3472
	movq	%r15, %rdi
	call	*%rdx
.L3400:
	movq	256(%r14), %rdi
	leaq	272(%r14), %rdx
	cmpq	%rdx, %rdi
	je	.L3427
	call	_ZdlPv@PLT
.L3427:
	movq	208(%r14), %rdi
	leaq	224(%r14), %rdx
	cmpq	%rdx, %rdi
	je	.L3428
	call	_ZdlPv@PLT
.L3428:
	movq	160(%r14), %rdi
	leaq	176(%r14), %rdx
	cmpq	%rdx, %rdi
	je	.L3429
	call	_ZdlPv@PLT
.L3429:
	movq	144(%r14), %rdi
	testq	%rdi, %rdi
	je	.L3430
	movq	(%rdi), %rdx
	call	*24(%rdx)
.L3430:
	movq	104(%r14), %rdi
	leaq	120(%r14), %rdx
	cmpq	%rdx, %rdi
	je	.L3431
	call	_ZdlPv@PLT
.L3431:
	movq	56(%r14), %rdi
	leaq	72(%r14), %rdx
	cmpq	%rdx, %rdi
	je	.L3432
	call	_ZdlPv@PLT
.L3432:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L3395
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L3395:
	.cfi_restore_state
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3472:
	.cfi_restore_state
	movq	160(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L3402
	movq	8(%r12), %rax
	movq	%rax, -64(%rbp)
	movq	(%r12), %rax
	movq	%rax, -56(%rbp)
.L3410:
	movq	-56(%rbp), %rax
	cmpq	%rax, -64(%rbp)
	je	.L3403
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.L3404
	movq	(%rbx), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3405
	movq	16(%rbx), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%rbx)
	testq	%rdi, %rdi
	je	.L3406
	movq	(%rdi), %rdx
	movq	24(%rdx), %rdx
	cmpq	%r13, %rdx
	jne	.L3407
	movq	%rdi, -72(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L3406:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L3408
	movq	(%rdi), %rdx
	movq	24(%rdx), %rdx
	cmpq	%r13, %rdx
	jne	.L3409
	movq	%rdi, -72(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L3408:
	movl	$24, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L3404:
	addq	$8, -56(%rbp)
	jmp	.L3410
.L3405:
	movq	%rbx, %rdi
	call	*%rdx
	jmp	.L3404
.L3409:
	call	*%rdx
	jmp	.L3408
.L3403:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3411
	call	_ZdlPv@PLT
.L3411:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3402:
	movq	152(%r15), %r12
	testq	%r12, %r12
	je	.L3412
	movq	8(%r12), %rax
	movq	%rax, -64(%rbp)
	movq	(%r12), %rax
	movq	%rax, -56(%rbp)
.L3422:
	movq	-56(%rbp), %rax
	cmpq	%rax, -64(%rbp)
	je	.L3413
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.L3414
	movq	(%rbx), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3415
	movq	152(%rbx), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	leaq	168(%rbx), %rdx
	movq	%rax, (%rbx)
	cmpq	%rdx, %rdi
	je	.L3416
	call	_ZdlPv@PLT
.L3416:
	movq	136(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L3417
	movq	(%rdi), %rdx
	movq	24(%rdx), %rdx
	cmpq	%r13, %rdx
	jne	.L3418
	movq	%rdi, -72(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L3417:
	movq	96(%rbx), %rdi
	leaq	112(%rbx), %rdx
	cmpq	%rdx, %rdi
	je	.L3419
	call	_ZdlPv@PLT
.L3419:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rdx
	cmpq	%rdx, %rdi
	je	.L3420
	call	_ZdlPv@PLT
.L3420:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rdx
	cmpq	%rdx, %rdi
	je	.L3421
	call	_ZdlPv@PLT
.L3421:
	movl	$192, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L3414:
	addq	$8, -56(%rbp)
	jmp	.L3422
.L3415:
	movq	%rbx, %rdi
	call	*%rdx
	jmp	.L3414
.L3418:
	call	*%rdx
	jmp	.L3417
.L3413:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3423
	call	_ZdlPv@PLT
.L3423:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3412:
	movq	104(%r15), %rdi
	leaq	120(%r15), %rdx
	cmpq	%rdx, %rdi
	je	.L3424
	call	_ZdlPv@PLT
.L3424:
	movq	56(%r15), %rdi
	leaq	72(%r15), %rdx
	cmpq	%rdx, %rdi
	je	.L3425
	call	_ZdlPv@PLT
.L3425:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rdx
	cmpq	%rdx, %rdi
	je	.L3426
	call	_ZdlPv@PLT
.L3426:
	movl	$168, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
	jmp	.L3400
.L3471:
	movq	56(%r12), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE(%rip), %rax
	leaq	72(%r12), %rdx
	movq	%rax, (%r12)
	cmpq	%rdx, %rdi
	je	.L3398
	call	_ZdlPv@PLT
.L3398:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rdx
	cmpq	%rdx, %rdi
	je	.L3399
	call	_ZdlPv@PLT
.L3399:
	movl	$96, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L3396
.L3407:
	call	*%rdx
	jmp	.L3406
	.cfi_endproc
.LFE14668:
	.size	_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD1Ev, .-_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12RemoteObjectD5Ev,comdat
	.p2align 4
	.weak	_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD0Ev
	.type	_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD0Ev, @function
_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD0Ev:
.LFB14667:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12RemoteObjectE(%rip), %rdx
	leaq	-64(%rdx), %rax
	movq	%rdx, %xmm1
	movq	%rax, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-8(%rdi), %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	304(%rdi), %r13
	movups	%xmm0, -8(%rdi)
	testq	%r13, %r13
	je	.L3474
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev(%rip), %rcx
	movq	24(%rdx), %rdx
	cmpq	%rcx, %rdx
	je	.L3549
	movq	%r13, %rdi
	call	*%rdx
.L3474:
	movq	296(%r15), %r14
	testq	%r14, %r14
	je	.L3478
	movq	(%r14), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rbx
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	je	.L3550
	movq	%r14, %rdi
	call	*%rdx
.L3478:
	movq	256(%r15), %rdi
	leaq	272(%r15), %rdx
	cmpq	%rdx, %rdi
	je	.L3505
	call	_ZdlPv@PLT
.L3505:
	movq	208(%r15), %rdi
	leaq	224(%r15), %rdx
	cmpq	%rdx, %rdi
	je	.L3506
	call	_ZdlPv@PLT
.L3506:
	movq	160(%r15), %rdi
	leaq	176(%r15), %rdx
	cmpq	%rdx, %rdi
	je	.L3507
	call	_ZdlPv@PLT
.L3507:
	movq	144(%r15), %rdi
	testq	%rdi, %rdi
	je	.L3508
	movq	(%rdi), %rdx
	call	*24(%rdx)
.L3508:
	movq	104(%r15), %rdi
	leaq	120(%r15), %rdx
	cmpq	%rdx, %rdi
	je	.L3509
	call	_ZdlPv@PLT
.L3509:
	movq	56(%r15), %rdi
	leaq	72(%r15), %rdx
	cmpq	%rdx, %rdi
	je	.L3510
	call	_ZdlPv@PLT
.L3510:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L3511
	call	_ZdlPv@PLT
.L3511:
	addq	$40, %rsp
	movq	%r12, %rdi
	movl	$320, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
.L3549:
	.cfi_restore_state
	movq	56(%r13), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE(%rip), %rax
	leaq	72(%r13), %rdx
	movq	%rax, 0(%r13)
	cmpq	%rdx, %rdi
	je	.L3476
	call	_ZdlPv@PLT
.L3476:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rdx
	cmpq	%rdx, %rdi
	je	.L3477
	call	_ZdlPv@PLT
.L3477:
	movl	$96, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	jmp	.L3474
.L3550:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r14)
	movq	160(%r14), %rax
	movq	%rax, -56(%rbp)
	testq	%rax, %rax
	je	.L3480
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	%rsi, -72(%rbp)
	movq	%rax, -64(%rbp)
.L3488:
	movq	-64(%rbp), %rax
	cmpq	%rax, -72(%rbp)
	je	.L3481
	movq	(%rax), %r13
	testq	%r13, %r13
	je	.L3482
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3483
	movq	16(%r13), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%rdi, %rdi
	je	.L3484
	movq	(%rdi), %rdx
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L3485
	movq	%rdi, -80(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-80(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L3484:
	movq	8(%r13), %rdi
	testq	%rdi, %rdi
	je	.L3486
	movq	(%rdi), %rdx
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L3487
	movq	%rdi, -80(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-80(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L3486:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3482:
	addq	$8, -64(%rbp)
	jmp	.L3488
.L3481:
	movq	-56(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L3489
	call	_ZdlPv@PLT
.L3489:
	movq	-56(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3480:
	movq	152(%r14), %rax
	movq	%rax, -56(%rbp)
	testq	%rax, %rax
	je	.L3490
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	%rsi, -72(%rbp)
	movq	%rax, -64(%rbp)
.L3500:
	movq	-64(%rbp), %rax
	cmpq	%rax, -72(%rbp)
	je	.L3491
	movq	(%rax), %r13
	testq	%r13, %r13
	je	.L3492
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3493
	movq	152(%r13), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	leaq	168(%r13), %rdx
	movq	%rax, 0(%r13)
	cmpq	%rdx, %rdi
	je	.L3494
	call	_ZdlPv@PLT
.L3494:
	movq	136(%r13), %rdi
	testq	%rdi, %rdi
	je	.L3495
	movq	(%rdi), %rdx
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L3496
	movq	%rdi, -80(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-80(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L3495:
	movq	96(%r13), %rdi
	leaq	112(%r13), %rdx
	cmpq	%rdx, %rdi
	je	.L3497
	call	_ZdlPv@PLT
.L3497:
	movq	48(%r13), %rdi
	leaq	64(%r13), %rdx
	cmpq	%rdx, %rdi
	je	.L3498
	call	_ZdlPv@PLT
.L3498:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rdx
	cmpq	%rdx, %rdi
	je	.L3499
	call	_ZdlPv@PLT
.L3499:
	movl	$192, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3492:
	addq	$8, -64(%rbp)
	jmp	.L3500
.L3491:
	movq	-56(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L3501
	call	_ZdlPv@PLT
.L3501:
	movq	-56(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3490:
	movq	104(%r14), %rdi
	leaq	120(%r14), %rdx
	cmpq	%rdx, %rdi
	je	.L3502
	call	_ZdlPv@PLT
.L3502:
	movq	56(%r14), %rdi
	leaq	72(%r14), %rdx
	cmpq	%rdx, %rdi
	je	.L3503
	call	_ZdlPv@PLT
.L3503:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rdx
	cmpq	%rdx, %rdi
	je	.L3504
	call	_ZdlPv@PLT
.L3504:
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
	jmp	.L3478
.L3485:
	call	*%rdx
	jmp	.L3484
.L3493:
	movq	%r13, %rdi
	call	*%rdx
	jmp	.L3492
.L3496:
	call	*%rdx
	jmp	.L3495
.L3483:
	movq	%r13, %rdi
	call	*%rdx
	jmp	.L3482
.L3487:
	call	*%rdx
	jmp	.L3486
	.cfi_endproc
.LFE14667:
	.size	_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD0Ev, .-_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime10StackTraceD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime10StackTraceD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime10StackTraceD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime10StackTraceD2Ev, @function
_ZN12v8_inspector8protocol7Runtime10StackTraceD2Ev:
.LFB5165:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime10StackTraceE(%rip), %rax
	leaq	-64(%rax), %rcx
	movq	%rax, %xmm1
	movq	%rcx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	punpcklqdq	%xmm1, %xmm0
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	80(%rdi), %r12
	movups	%xmm0, (%rdi)
	testq	%r12, %r12
	je	.L3552
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3553
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rax
	movq	64(%r12), %rdi
	leaq	-64(%rax), %rsi
	movq	%rax, %xmm2
	leaq	80(%r12), %rax
	movq	%rsi, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, (%r12)
	cmpq	%rax, %rdi
	je	.L3554
	call	_ZdlPv@PLT
.L3554:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3555
	call	_ZdlPv@PLT
.L3555:
	movl	$104, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3552:
	movq	72(%rbx), %r12
	testq	%r12, %r12
	je	.L3556
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3557
	call	_ZN12v8_inspector8protocol7Runtime10StackTraceD1Ev
	movl	$88, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3556:
	movq	64(%rbx), %r13
	testq	%r13, %r13
	je	.L3558
	movq	8(%r13), %r14
	movq	0(%r13), %r12
	cmpq	%r12, %r14
	jne	.L3565
	testq	%r12, %r12
	je	.L3566
	.p2align 4,,10
	.p2align 3
.L3587:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L3566:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3558:
	movq	24(%rbx), %rdi
	addq	$40, %rbx
	cmpq	%rbx, %rdi
	je	.L3551
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L3586:
	.cfi_restore_state
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r15), %rdi
	movq	%rax, (%r15)
	leaq	104(%r15), %rax
	cmpq	%rax, %rdi
	je	.L3562
	call	_ZdlPv@PLT
.L3562:
	movq	48(%r15), %rdi
	leaq	64(%r15), %rax
	cmpq	%rax, %rdi
	je	.L3563
	call	_ZdlPv@PLT
.L3563:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L3564
	call	_ZdlPv@PLT
.L3564:
	movl	$136, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L3560:
	addq	$8, %r12
	cmpq	%r12, %r14
	je	.L3585
.L3565:
	movq	(%r12), %r15
	testq	%r15, %r15
	je	.L3560
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L3586
	addq	$8, %r12
	movq	%r15, %rdi
	call	*%rax
	cmpq	%r12, %r14
	jne	.L3565
	.p2align 4,,10
	.p2align 3
.L3585:
	movq	0(%r13), %r12
	testq	%r12, %r12
	jne	.L3587
	jmp	.L3566
	.p2align 4,,10
	.p2align 3
.L3551:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3557:
	.cfi_restore_state
	call	*%rax
	jmp	.L3556
	.p2align 4,,10
	.p2align 3
.L3553:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3552
	.cfi_endproc
.LFE5165:
	.size	_ZN12v8_inspector8protocol7Runtime10StackTraceD2Ev, .-_ZN12v8_inspector8protocol7Runtime10StackTraceD2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime10StackTraceD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime10StackTraceD1Ev,_ZN12v8_inspector8protocol7Runtime10StackTraceD2Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime10StackTraceD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev, @function
_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev:
.LFB5167:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN12v8_inspector8protocol7Runtime10StackTraceD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$88, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5167:
	.size	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev, .-_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD2Ev, @function
_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD2Ev:
.LFB5036:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime16ExceptionDetailsE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	168(%rdi), %r12
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L3591
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3592
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3591:
	movq	160(%rbx), %r12
	testq	%r12, %r12
	je	.L3593
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3594
	call	_ZN12v8_inspector8protocol7Runtime10StackTraceD1Ev
	movl	$88, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3593:
	movq	120(%rbx), %rdi
	leaq	136(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3595
	call	_ZdlPv@PLT
.L3595:
	movq	72(%rbx), %rdi
	leaq	88(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3596
	call	_ZdlPv@PLT
.L3596:
	movq	16(%rbx), %rdi
	addq	$32, %rbx
	cmpq	%rbx, %rdi
	je	.L3590
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L3590:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3594:
	.cfi_restore_state
	call	*%rax
	jmp	.L3593
	.p2align 4,,10
	.p2align 3
.L3592:
	call	*%rax
	jmp	.L3591
	.cfi_endproc
.LFE5036:
	.size	_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD2Ev, .-_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD1Ev,_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD2Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD0Ev, @function
_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD0Ev:
.LFB5038:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime16ExceptionDetailsE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	168(%rdi), %r13
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L3606
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3607
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3606:
	movq	160(%r12), %r13
	testq	%r13, %r13
	je	.L3608
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3609
	call	_ZN12v8_inspector8protocol7Runtime10StackTraceD1Ev
	movl	$88, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3608:
	movq	120(%r12), %rdi
	leaq	136(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3610
	call	_ZdlPv@PLT
.L3610:
	movq	72(%r12), %rdi
	leaq	88(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3611
	call	_ZdlPv@PLT
.L3611:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3612
	call	_ZdlPv@PLT
.L3612:
	movq	%r12, %rdi
	movl	$184, %esi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L3607:
	.cfi_restore_state
	call	*%rax
	jmp	.L3606
	.p2align 4,,10
	.p2align 3
.L3609:
	call	*%rax
	jmp	.L3608
	.cfi_endproc
.LFE5038:
	.size	_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD0Ev, .-_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD0Ev
	.section	.text._ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend20AwaitPromiseCallbackEE11sendSuccessESt10unique_ptrINS3_12RemoteObjectESt14default_deleteIS8_EEN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeINS3_16ExceptionDetailsEEE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend20AwaitPromiseCallbackEE11sendSuccessESt10unique_ptrINS3_12RemoteObjectESt14default_deleteIS8_EEN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeINS3_16ExceptionDetailsEEE, @function
_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend20AwaitPromiseCallbackEE11sendSuccessESt10unique_ptrINS3_12RemoteObjectESt14default_deleteIS8_EEN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeINS3_16ExceptionDetailsEEE:
.LFB14437:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$32, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	8(%rdi), %rdi
	movq	(%rdx), %rcx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	(%rax), %rax
	movq	$0, (%rdx)
	movq	(%rsi), %rdx
	movq	%rcx, -32(%rbp)
	movq	$0, (%rsi)
	leaq	-40(%rbp), %rsi
	movq	%rdx, -40(%rbp)
	leaq	-32(%rbp), %rdx
	call	*%rax
	movq	-40(%rbp), %r12
	testq	%r12, %r12
	je	.L3621
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3622
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3621:
	movq	-32(%rbp), %r12
	testq	%r12, %r12
	je	.L3620
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3624
	movq	168(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime16ExceptionDetailsE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L3625
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3626
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3625:
	movq	160(%r12), %r13
	testq	%r13, %r13
	je	.L3627
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3628
	call	_ZN12v8_inspector8protocol7Runtime10StackTraceD1Ev
	movl	$88, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3627:
	movq	120(%r12), %rdi
	leaq	136(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3629
	call	_ZdlPv@PLT
.L3629:
	movq	72(%r12), %rdi
	leaq	88(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3630
	call	_ZdlPv@PLT
.L3630:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3631
	call	_ZdlPv@PLT
.L3631:
	movl	$184, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3620:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3646
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3624:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3620
	.p2align 4,,10
	.p2align 3
.L3622:
	call	*%rax
	jmp	.L3621
	.p2align 4,,10
	.p2align 3
.L3626:
	call	*%rax
	jmp	.L3625
	.p2align 4,,10
	.p2align 3
.L3628:
	call	*%rax
	jmp	.L3627
.L3646:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE14437:
	.size	_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend20AwaitPromiseCallbackEE11sendSuccessESt10unique_ptrINS3_12RemoteObjectESt14default_deleteIS8_EEN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeINS3_16ExceptionDetailsEEE, .-_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend20AwaitPromiseCallbackEE11sendSuccessESt10unique_ptrINS3_12RemoteObjectESt14default_deleteIS8_EEN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeINS3_16ExceptionDetailsEEE
	.section	.text._ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend22CallFunctionOnCallbackEE11sendSuccessESt10unique_ptrINS3_12RemoteObjectESt14default_deleteIS8_EEN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeINS3_16ExceptionDetailsEEE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend22CallFunctionOnCallbackEE11sendSuccessESt10unique_ptrINS3_12RemoteObjectESt14default_deleteIS8_EEN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeINS3_16ExceptionDetailsEEE, @function
_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend22CallFunctionOnCallbackEE11sendSuccessESt10unique_ptrINS3_12RemoteObjectESt14default_deleteIS8_EEN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeINS3_16ExceptionDetailsEEE:
.LFB14441:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$32, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	8(%rdi), %rdi
	movq	(%rdx), %rcx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	(%rax), %rax
	movq	$0, (%rdx)
	movq	(%rsi), %rdx
	movq	%rcx, -32(%rbp)
	movq	$0, (%rsi)
	leaq	-40(%rbp), %rsi
	movq	%rdx, -40(%rbp)
	leaq	-32(%rbp), %rdx
	call	*%rax
	movq	-40(%rbp), %r12
	testq	%r12, %r12
	je	.L3648
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3649
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3648:
	movq	-32(%rbp), %r12
	testq	%r12, %r12
	je	.L3647
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3651
	movq	168(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime16ExceptionDetailsE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L3652
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3653
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3652:
	movq	160(%r12), %r13
	testq	%r13, %r13
	je	.L3654
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3655
	call	_ZN12v8_inspector8protocol7Runtime10StackTraceD1Ev
	movl	$88, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3654:
	movq	120(%r12), %rdi
	leaq	136(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3656
	call	_ZdlPv@PLT
.L3656:
	movq	72(%r12), %rdi
	leaq	88(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3657
	call	_ZdlPv@PLT
.L3657:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3658
	call	_ZdlPv@PLT
.L3658:
	movl	$184, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3647:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3673
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3651:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3647
	.p2align 4,,10
	.p2align 3
.L3649:
	call	*%rax
	jmp	.L3648
	.p2align 4,,10
	.p2align 3
.L3653:
	call	*%rax
	jmp	.L3652
	.p2align 4,,10
	.p2align 3
.L3655:
	call	*%rax
	jmp	.L3654
.L3673:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE14441:
	.size	_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend22CallFunctionOnCallbackEE11sendSuccessESt10unique_ptrINS3_12RemoteObjectESt14default_deleteIS8_EEN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeINS3_16ExceptionDetailsEEE, .-_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend22CallFunctionOnCallbackEE11sendSuccessESt10unique_ptrINS3_12RemoteObjectESt14default_deleteIS8_EEN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeINS3_16ExceptionDetailsEEE
	.section	.text._ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend17RunScriptCallbackEE11sendSuccessESt10unique_ptrINS3_12RemoteObjectESt14default_deleteIS8_EEN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeINS3_16ExceptionDetailsEEE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend17RunScriptCallbackEE11sendSuccessESt10unique_ptrINS3_12RemoteObjectESt14default_deleteIS8_EEN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeINS3_16ExceptionDetailsEEE, @function
_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend17RunScriptCallbackEE11sendSuccessESt10unique_ptrINS3_12RemoteObjectESt14default_deleteIS8_EEN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeINS3_16ExceptionDetailsEEE:
.LFB14435:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$32, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	8(%rdi), %rdi
	movq	(%rdx), %rcx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	(%rax), %rax
	movq	$0, (%rdx)
	movq	(%rsi), %rdx
	movq	%rcx, -32(%rbp)
	movq	$0, (%rsi)
	leaq	-40(%rbp), %rsi
	movq	%rdx, -40(%rbp)
	leaq	-32(%rbp), %rdx
	call	*%rax
	movq	-40(%rbp), %r12
	testq	%r12, %r12
	je	.L3675
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3676
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3675:
	movq	-32(%rbp), %r12
	testq	%r12, %r12
	je	.L3674
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3678
	movq	168(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime16ExceptionDetailsE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L3679
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3680
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3679:
	movq	160(%r12), %r13
	testq	%r13, %r13
	je	.L3681
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3682
	call	_ZN12v8_inspector8protocol7Runtime10StackTraceD1Ev
	movl	$88, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3681:
	movq	120(%r12), %rdi
	leaq	136(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3683
	call	_ZdlPv@PLT
.L3683:
	movq	72(%r12), %rdi
	leaq	88(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3684
	call	_ZdlPv@PLT
.L3684:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3685
	call	_ZdlPv@PLT
.L3685:
	movl	$184, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3674:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3700
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3678:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3674
	.p2align 4,,10
	.p2align 3
.L3676:
	call	*%rax
	jmp	.L3675
	.p2align 4,,10
	.p2align 3
.L3680:
	call	*%rax
	jmp	.L3679
	.p2align 4,,10
	.p2align 3
.L3682:
	call	*%rax
	jmp	.L3681
.L3700:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE14435:
	.size	_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend17RunScriptCallbackEE11sendSuccessESt10unique_ptrINS3_12RemoteObjectESt14default_deleteIS8_EEN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeINS3_16ExceptionDetailsEEE, .-_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend17RunScriptCallbackEE11sendSuccessESt10unique_ptrINS3_12RemoteObjectESt14default_deleteIS8_EEN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeINS3_16ExceptionDetailsEEE
	.section	.text._ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend16EvaluateCallbackEE11sendSuccessESt10unique_ptrINS3_12RemoteObjectESt14default_deleteIS8_EEN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeINS3_16ExceptionDetailsEEE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend16EvaluateCallbackEE11sendSuccessESt10unique_ptrINS3_12RemoteObjectESt14default_deleteIS8_EEN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeINS3_16ExceptionDetailsEEE, @function
_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend16EvaluateCallbackEE11sendSuccessESt10unique_ptrINS3_12RemoteObjectESt14default_deleteIS8_EEN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeINS3_16ExceptionDetailsEEE:
.LFB14439:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$32, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	8(%rdi), %rdi
	movq	(%rdx), %rcx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	(%rax), %rax
	movq	$0, (%rdx)
	movq	(%rsi), %rdx
	movq	%rcx, -32(%rbp)
	movq	$0, (%rsi)
	leaq	-40(%rbp), %rsi
	movq	%rdx, -40(%rbp)
	leaq	-32(%rbp), %rdx
	call	*%rax
	movq	-40(%rbp), %r12
	testq	%r12, %r12
	je	.L3702
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3703
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3702:
	movq	-32(%rbp), %r12
	testq	%r12, %r12
	je	.L3701
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3705
	movq	168(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime16ExceptionDetailsE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L3706
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3707
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3706:
	movq	160(%r12), %r13
	testq	%r13, %r13
	je	.L3708
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3709
	call	_ZN12v8_inspector8protocol7Runtime10StackTraceD1Ev
	movl	$88, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3708:
	movq	120(%r12), %rdi
	leaq	136(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3710
	call	_ZdlPv@PLT
.L3710:
	movq	72(%r12), %rdi
	leaq	88(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3711
	call	_ZdlPv@PLT
.L3711:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3712
	call	_ZdlPv@PLT
.L3712:
	movl	$184, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3701:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3727
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3705:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3701
	.p2align 4,,10
	.p2align 3
.L3703:
	call	*%rax
	jmp	.L3702
	.p2align 4,,10
	.p2align 3
.L3707:
	call	*%rax
	jmp	.L3706
	.p2align 4,,10
	.p2align 3
.L3709:
	call	*%rax
	jmp	.L3708
.L3727:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE14439:
	.size	_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend16EvaluateCallbackEE11sendSuccessESt10unique_ptrINS3_12RemoteObjectESt14default_deleteIS8_EEN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeINS3_16ExceptionDetailsEEE, .-_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend16EvaluateCallbackEE11sendSuccessESt10unique_ptrINS3_12RemoteObjectESt14default_deleteIS8_EEN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeINS3_16ExceptionDetailsEEE
	.section	.text._ZN12v8_inspector12_GLOBAL__N_123wrapEvaluateResultAsyncINS_8protocol7Runtime7Backend22CallFunctionOnCallbackEEEbPNS_14InjectedScriptEN2v810MaybeLocalINS8_5ValueEEERKNS8_8TryCatchERKNS_8String16ENS_8WrapModeEPT_,"ax",@progbits
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_123wrapEvaluateResultAsyncINS_8protocol7Runtime7Backend22CallFunctionOnCallbackEEEbPNS_14InjectedScriptEN2v810MaybeLocalINS8_5ValueEEERKNS8_8TryCatchERKNS_8String16ENS_8WrapModeEPT_, @function
_ZN12v8_inspector12_GLOBAL__N_123wrapEvaluateResultAsyncINS_8protocol7Runtime7Backend22CallFunctionOnCallbackEEEbPNS_14InjectedScriptEN2v810MaybeLocalINS8_5ValueEEERKNS8_8TryCatchERKNS_8String16ENS_8WrapModeEPT_:
.LFB9529:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-96(%rbp), %r14
	pushq	%r12
	.cfi_offset 12, -40
	movq	%r9, %r12
	movl	%r8d, %r9d
	movq	%rcx, %r8
	movq	%rdx, %rcx
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movq	%r14, %rdi
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	-120(%rbp), %rax
	movq	$0, -128(%rbp)
	pushq	%rax
	leaq	-128(%rbp), %rax
	pushq	%rax
	movq	$0, -120(%rbp)
	call	_ZN12v8_inspector14InjectedScript18wrapEvaluateResultEN2v810MaybeLocalINS1_5ValueEEERKNS1_8TryCatchERKNS_8String16ENS_8WrapModeEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISF_EEPN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeINSE_16ExceptionDetailsEEE@PLT
	movl	-96(%rbp), %ecx
	popq	%rax
	movq	(%r12), %rax
	popq	%rdx
	testl	%ecx, %ecx
	je	.L3780
	movq	%r14, %rsi
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	call	*8(%rax)
.L3741:
	movq	-88(%rbp), %rdi
	leaq	-72(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3742
	call	_ZdlPv@PLT
.L3742:
	movq	-120(%rbp), %r12
	testq	%r12, %r12
	je	.L3743
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3744
	movq	168(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime16ExceptionDetailsE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L3745
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3746
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3745:
	movq	160(%r12), %r13
	testq	%r13, %r13
	je	.L3747
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3748
	call	_ZN12v8_inspector8protocol7Runtime10StackTraceD1Ev
	movl	$88, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3747:
	movq	120(%r12), %rdi
	leaq	136(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3749
	call	_ZdlPv@PLT
.L3749:
	movq	72(%r12), %rdi
	leaq	88(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3750
	call	_ZdlPv@PLT
.L3750:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3751
	call	_ZdlPv@PLT
.L3751:
	movl	$184, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3743:
	movq	-128(%rbp), %r12
	testq	%r12, %r12
	je	.L3728
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3753
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3728:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3781
	leaq	-24(%rbp), %rsp
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3780:
	.cfi_restore_state
	movq	-120(%rbp), %rdx
	movq	(%rax), %rax
	movq	%r12, %rdi
	leaq	-112(%rbp), %rsi
	movq	$0, -120(%rbp)
	movq	%rdx, -104(%rbp)
	movq	-128(%rbp), %rdx
	movq	$0, -128(%rbp)
	movq	%rdx, -112(%rbp)
	leaq	-104(%rbp), %rdx
	call	*%rax
	movq	-112(%rbp), %r12
	testq	%r12, %r12
	je	.L3730
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3731
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3730:
	movq	-104(%rbp), %r12
	testq	%r12, %r12
	je	.L3732
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3733
	movq	168(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime16ExceptionDetailsE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L3734
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3735
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3734:
	movq	160(%r12), %r13
	testq	%r13, %r13
	je	.L3736
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3737
	call	_ZN12v8_inspector8protocol7Runtime10StackTraceD1Ev
	movl	$88, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3736:
	movq	120(%r12), %rdi
	leaq	136(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3738
	call	_ZdlPv@PLT
.L3738:
	movq	72(%r12), %rdi
	leaq	88(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3739
	call	_ZdlPv@PLT
.L3739:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3740
	call	_ZdlPv@PLT
.L3740:
	movl	$184, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3732:
	movl	$1, %r14d
	jmp	.L3741
	.p2align 4,,10
	.p2align 3
.L3744:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3743
	.p2align 4,,10
	.p2align 3
.L3753:
	call	*%rax
	jmp	.L3728
	.p2align 4,,10
	.p2align 3
.L3746:
	call	*%rax
	jmp	.L3745
	.p2align 4,,10
	.p2align 3
.L3748:
	call	*%rax
	jmp	.L3747
	.p2align 4,,10
	.p2align 3
.L3731:
	call	*%rax
	jmp	.L3730
	.p2align 4,,10
	.p2align 3
.L3733:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3732
	.p2align 4,,10
	.p2align 3
.L3737:
	call	*%rax
	jmp	.L3736
	.p2align 4,,10
	.p2align 3
.L3735:
	call	*%rax
	jmp	.L3734
.L3781:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9529:
	.size	_ZN12v8_inspector12_GLOBAL__N_123wrapEvaluateResultAsyncINS_8protocol7Runtime7Backend22CallFunctionOnCallbackEEEbPNS_14InjectedScriptEN2v810MaybeLocalINS8_5ValueEEERKNS8_8TryCatchERKNS_8String16ENS_8WrapModeEPT_, .-_ZN12v8_inspector12_GLOBAL__N_123wrapEvaluateResultAsyncINS_8protocol7Runtime7Backend22CallFunctionOnCallbackEEEbPNS_14InjectedScriptEN2v810MaybeLocalINS8_5ValueEEERKNS8_8TryCatchERKNS_8String16ENS_8WrapModeEPT_
	.section	.text._ZN12v8_inspector8protocol7Runtime10StackTraceD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime10StackTraceD5Ev,comdat
	.p2align 4
	.weak	_ZThn8_N12v8_inspector8protocol7Runtime10StackTraceD1Ev
	.type	_ZThn8_N12v8_inspector8protocol7Runtime10StackTraceD1Ev, @function
_ZThn8_N12v8_inspector8protocol7Runtime10StackTraceD1Ev:
.LFB14669:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime10StackTraceE(%rip), %rax
	leaq	-64(%rax), %rcx
	movq	%rax, %xmm1
	movq	%rcx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	punpcklqdq	%xmm1, %xmm0
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	72(%rdi), %r12
	movups	%xmm0, -8(%rdi)
	testq	%r12, %r12
	je	.L3783
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L3816
	movq	%r12, %rdi
	call	*%rax
.L3783:
	movq	64(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L3787
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L3817
	call	*%rax
.L3787:
	movq	56(%rbx), %r15
	testq	%r15, %r15
	je	.L3789
	movq	8(%r15), %r13
	movq	(%r15), %r12
	cmpq	%r12, %r13
	je	.L3790
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %r14
	.p2align 4,,10
	.p2align 3
.L3796:
	movq	(%r12), %r8
	testq	%r8, %r8
	je	.L3791
	movq	(%r8), %rax
	movq	24(%rax), %rax
	cmpq	%r14, %rax
	je	.L3818
	movq	%r8, %rdi
	call	*%rax
.L3791:
	addq	$8, %r12
	cmpq	%r12, %r13
	jne	.L3796
	movq	(%r15), %r12
.L3790:
	testq	%r12, %r12
	je	.L3797
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L3797:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L3789:
	movq	16(%rbx), %rdi
	addq	$32, %rbx
	cmpq	%rbx, %rdi
	je	.L3782
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L3782:
	.cfi_restore_state
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3818:
	.cfi_restore_state
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r8), %rdi
	movq	%rax, (%r8)
	leaq	104(%r8), %rax
	cmpq	%rax, %rdi
	je	.L3793
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L3793:
	movq	48(%r8), %rdi
	leaq	64(%r8), %rax
	cmpq	%rax, %rdi
	je	.L3794
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L3794:
	movq	8(%r8), %rdi
	leaq	24(%r8), %rax
	cmpq	%rax, %rdi
	je	.L3795
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L3795:
	movl	$136, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
	jmp	.L3791
.L3817:
	movq	%rdi, -56(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime10StackTraceD1Ev
	movq	-56(%rbp), %rdi
	movl	$88, %esi
	call	_ZdlPvm@PLT
	jmp	.L3787
.L3816:
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rax
	movq	64(%r12), %rdi
	leaq	-64(%rax), %rsi
	movq	%rax, %xmm2
	leaq	80(%r12), %rax
	movq	%rsi, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, (%r12)
	cmpq	%rax, %rdi
	je	.L3785
	call	_ZdlPv@PLT
.L3785:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3786
	call	_ZdlPv@PLT
.L3786:
	movl	$104, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L3783
	.cfi_endproc
.LFE14669:
	.size	_ZThn8_N12v8_inspector8protocol7Runtime10StackTraceD1Ev, .-_ZThn8_N12v8_inspector8protocol7Runtime10StackTraceD1Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime10StackTraceD5Ev,comdat
	.p2align 4
	.weak	_ZThn8_N12v8_inspector8protocol7Runtime10StackTraceD0Ev
	.type	_ZThn8_N12v8_inspector8protocol7Runtime10StackTraceD0Ev, @function
_ZThn8_N12v8_inspector8protocol7Runtime10StackTraceD0Ev:
.LFB14665:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime10StackTraceE(%rip), %rax
	leaq	-64(%rax), %rsi
	movq	%rax, %xmm1
	movq	%rsi, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	punpcklqdq	%xmm1, %xmm0
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-8(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	72(%rdi), %r12
	movups	%xmm0, -8(%rdi)
	testq	%r12, %r12
	je	.L3820
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L3853
	movq	%r12, %rdi
	call	*%rax
.L3820:
	movq	64(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L3824
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L3854
	call	*%rax
.L3824:
	movq	56(%rbx), %r15
	testq	%r15, %r15
	je	.L3826
	movq	8(%r15), %r12
	movq	(%r15), %r14
	cmpq	%r14, %r12
	je	.L3827
	.p2align 4,,10
	.p2align 3
.L3833:
	movq	(%r14), %r9
	testq	%r9, %r9
	je	.L3828
	movq	(%r9), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L3855
	movq	%r9, %rdi
	call	*%rax
.L3828:
	addq	$8, %r14
	cmpq	%r14, %r12
	jne	.L3833
	movq	(%r15), %r14
.L3827:
	testq	%r14, %r14
	je	.L3834
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3834:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L3826:
	movq	16(%rbx), %rdi
	addq	$32, %rbx
	cmpq	%rbx, %rdi
	je	.L3835
	call	_ZdlPv@PLT
.L3835:
	addq	$24, %rsp
	movq	%r13, %rdi
	movl	$88, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
.L3853:
	.cfi_restore_state
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rax
	movq	64(%r12), %rdi
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm2
	leaq	80(%r12), %rax
	movq	%rdx, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, (%r12)
	cmpq	%rax, %rdi
	je	.L3822
	call	_ZdlPv@PLT
.L3822:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3823
	call	_ZdlPv@PLT
.L3823:
	movl	$104, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L3820
.L3855:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r9), %rdi
	movq	%rax, (%r9)
	leaq	104(%r9), %rax
	cmpq	%rax, %rdi
	je	.L3830
	movq	%r9, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r9
.L3830:
	movq	48(%r9), %rdi
	leaq	64(%r9), %rax
	cmpq	%rax, %rdi
	je	.L3831
	movq	%r9, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r9
.L3831:
	movq	8(%r9), %rdi
	leaq	24(%r9), %rax
	cmpq	%rax, %rdi
	je	.L3832
	movq	%r9, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r9
.L3832:
	movl	$136, %esi
	movq	%r9, %rdi
	call	_ZdlPvm@PLT
	jmp	.L3828
.L3854:
	movq	%rdi, -56(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime10StackTraceD1Ev
	movq	-56(%rbp), %rdi
	movl	$88, %esi
	call	_ZdlPvm@PLT
	jmp	.L3824
	.cfi_endproc
.LFE14665:
	.size	_ZThn8_N12v8_inspector8protocol7Runtime10StackTraceD0Ev, .-_ZThn8_N12v8_inspector8protocol7Runtime10StackTraceD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime12StackTraceIdD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12StackTraceIdD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD2Ev, @function
_ZN12v8_inspector8protocol7Runtime12StackTraceIdD2Ev:
.LFB5237:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rax
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	80(%rbx), %rax
	subq	$8, %rsp
	movups	%xmm0, (%rdi)
	movq	64(%rdi), %rdi
	cmpq	%rax, %rdi
	je	.L3857
	call	_ZdlPv@PLT
.L3857:
	movq	16(%rbx), %rdi
	addq	$32, %rbx
	cmpq	%rbx, %rdi
	je	.L3856
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L3856:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5237:
	.size	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD2Ev, .-_ZN12v8_inspector8protocol7Runtime12StackTraceIdD2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD1Ev,_ZN12v8_inspector8protocol7Runtime12StackTraceIdD2Ev
	.p2align 4
	.weak	_ZThn8_N12v8_inspector8protocol7Runtime12StackTraceIdD1Ev
	.type	_ZThn8_N12v8_inspector8protocol7Runtime12StackTraceIdD1Ev, @function
_ZThn8_N12v8_inspector8protocol7Runtime12StackTraceIdD1Ev:
.LFB14666:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rax
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	72(%rbx), %rax
	subq	$8, %rsp
	movups	%xmm0, -8(%rdi)
	movq	56(%rdi), %rdi
	cmpq	%rax, %rdi
	je	.L3861
	call	_ZdlPv@PLT
.L3861:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L3860
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L3860:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE14666:
	.size	_ZThn8_N12v8_inspector8protocol7Runtime12StackTraceIdD1Ev, .-_ZThn8_N12v8_inspector8protocol7Runtime12StackTraceIdD1Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12StackTraceIdD5Ev,comdat
	.p2align 4
	.weak	_ZThn8_N12v8_inspector8protocol7Runtime12StackTraceIdD0Ev
	.type	_ZThn8_N12v8_inspector8protocol7Runtime12StackTraceIdD0Ev, @function
_ZThn8_N12v8_inspector8protocol7Runtime12StackTraceIdD0Ev:
.LFB14664:
	.cfi_startproc
	endbr64
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-8(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movups	%xmm0, -8(%rdi)
	movq	56(%rdi), %rdi
	leaq	72(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3865
	call	_ZdlPv@PLT
.L3865:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L3866
	call	_ZdlPv@PLT
.L3866:
	popq	%rbx
	movq	%r12, %rdi
	movl	$104, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE14664:
	.size	_ZThn8_N12v8_inspector8protocol7Runtime12StackTraceIdD0Ev, .-_ZThn8_N12v8_inspector8protocol7Runtime12StackTraceIdD0Ev
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB7896:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L3874
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L3877
.L3868:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3874:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L3877:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L3868
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE7896:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.text._ZN12v8_inspector18V8RuntimeAgentImplC2EPNS_22V8InspectorSessionImplEPNS_8protocol15FrontendChannelEPNS3_15DictionaryValueE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector18V8RuntimeAgentImplC2EPNS_22V8InspectorSessionImplEPNS_8protocol15FrontendChannelEPNS3_15DictionaryValueE
	.type	_ZN12v8_inspector18V8RuntimeAgentImplC2EPNS_22V8InspectorSessionImplEPNS_8protocol15FrontendChannelEPNS3_15DictionaryValueE, @function
_ZN12v8_inspector18V8RuntimeAgentImplC2EPNS_22V8InspectorSessionImplEPNS_8protocol15FrontendChannelEPNS3_15DictionaryValueE:
.LFB7951:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN12v8_inspector18V8RuntimeAgentImplE(%rip), %rax
	movq	%rdx, 24(%rdi)
	movq	%rax, (%rdi)
	movq	24(%rsi), %rax
	movq	%rsi, 8(%rdi)
	movq	%rax, 32(%rdi)
	leaq	96(%rdi), %rax
	movq	%rcx, 16(%rdi)
	movb	$0, 40(%rdi)
	movq	%rax, 48(%rdi)
	movq	$1, 56(%rdi)
	movq	$0, 64(%rdi)
	movq	$0, 72(%rdi)
	movl	$0x3f800000, 80(%rdi)
	movq	$0, 88(%rdi)
	movq	$0, 96(%rdi)
	ret
	.cfi_endproc
.LFE7951:
	.size	_ZN12v8_inspector18V8RuntimeAgentImplC2EPNS_22V8InspectorSessionImplEPNS_8protocol15FrontendChannelEPNS3_15DictionaryValueE, .-_ZN12v8_inspector18V8RuntimeAgentImplC2EPNS_22V8InspectorSessionImplEPNS_8protocol15FrontendChannelEPNS3_15DictionaryValueE
	.globl	_ZN12v8_inspector18V8RuntimeAgentImplC1EPNS_22V8InspectorSessionImplEPNS_8protocol15FrontendChannelEPNS3_15DictionaryValueE
	.set	_ZN12v8_inspector18V8RuntimeAgentImplC1EPNS_22V8InspectorSessionImplEPNS_8protocol15FrontendChannelEPNS3_15DictionaryValueE,_ZN12v8_inspector18V8RuntimeAgentImplC2EPNS_22V8InspectorSessionImplEPNS_8protocol15FrontendChannelEPNS3_15DictionaryValueE
	.section	.text._ZN12v8_inspector18V8RuntimeAgentImpl10addBindingEPNS_16InspectedContextERKNS_8String16E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector18V8RuntimeAgentImpl10addBindingEPNS_16InspectedContextERKNS_8String16E
	.type	_ZN12v8_inspector18V8RuntimeAgentImpl10addBindingEPNS_16InspectedContextERKNS_8String16E, @function
_ZN12v8_inspector18V8RuntimeAgentImpl10addBindingEPNS_16InspectedContextERKNS_8String16E:
.LFB8056:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-112(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rax
	movq	%r15, %rdi
	movq	8(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r12, %rdi
	call	_ZNK12v8_inspector16InspectedContext7contextEv@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v87Context6GlobalEv@PLT
	movq	%r13, %rsi
	movq	%rax, %r14
	movq	32(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movl	$1, %edx
	movq	%rax, %r13
	movq	32(%rbx), %rax
	leaq	-80(%rbp), %rbx
	movq	%rbx, %rdi
	movq	8(%rax), %rsi
	call	_ZN2v815MicrotasksScopeC1EPNS_7IsolateENS0_4TypeE@PLT
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%r13, %rdx
	leaq	_ZN12v8_inspector18V8RuntimeAgentImpl15bindingCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88Function3NewENS_5LocalINS_7ContextEEEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS1_IS5_EEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	testq	%rax, %rax
	je	.L3882
	movq	%rax, %rcx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
.L3882:
	movq	%rbx, %rdi
	call	_ZN2v815MicrotasksScopeD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3890
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3890:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8056:
	.size	_ZN12v8_inspector18V8RuntimeAgentImpl10addBindingEPNS_16InspectedContextERKNS_8String16E, .-_ZN12v8_inspector18V8RuntimeAgentImpl10addBindingEPNS_16InspectedContextERKNS_8String16E
	.section	.text._ZNSt17_Function_handlerIFvPN12v8_inspector16InspectedContextEEZNS0_18V8RuntimeAgentImpl10addBindingERKNS0_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIiEEEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvPN12v8_inspector16InspectedContextEEZNS0_18V8RuntimeAgentImpl10addBindingERKNS0_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIiEEEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_, @function
_ZNSt17_Function_handlerIFvPN12v8_inspector16InspectedContextEEZNS0_18V8RuntimeAgentImpl10addBindingERKNS0_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIiEEEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_:
.LFB10997:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r8
	movq	(%rdi), %rdx
	movq	(%rsi), %rsi
	movq	%r8, %rdi
	jmp	_ZN12v8_inspector18V8RuntimeAgentImpl10addBindingEPNS_16InspectedContextERKNS_8String16E
	.cfi_endproc
.LFE10997:
	.size	_ZNSt17_Function_handlerIFvPN12v8_inspector16InspectedContextEEZNS0_18V8RuntimeAgentImpl10addBindingERKNS0_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIiEEEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_, .-_ZNSt17_Function_handlerIFvPN12v8_inspector16InspectedContextEEZNS0_18V8RuntimeAgentImpl10addBindingERKNS0_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIiEEEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_
	.section	.rodata._ZN12v8_inspector18V8RuntimeAgentImpl10addBindingERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIiEE.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"Cannot find execution context with given executionContextId"
	.section	.text._ZN12v8_inspector18V8RuntimeAgentImpl10addBindingERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIiEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector18V8RuntimeAgentImpl10addBindingERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIiEE
	.type	_ZN12v8_inspector18V8RuntimeAgentImpl10addBindingERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIiEE, @function
_ZN12v8_inspector18V8RuntimeAgentImpl10addBindingERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIiEE:
.LFB8046:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-96(%rbp), %r13
	pushq	%r12
	movq	%r13, %rdi
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	%rcx, -144(%rbp)
	movq	16(%rsi), %rbx
	leaq	_ZN12v8_inspector23V8RuntimeAgentImplStateL8bindingsE(%rip), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%rbx, %rdi
	movq	%r13, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue9getObjectERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %rbx
	leaq	-80(%rbp), %rax
	movq	%rax, -128(%rbp)
	cmpq	%rax, %rdi
	je	.L3893
	call	_ZdlPv@PLT
.L3893:
	movq	16(%r12), %r8
	testq	%rbx, %rbx
	je	.L3912
.L3894:
	leaq	_ZN12v8_inspector23V8RuntimeAgentImplStateL8bindingsE(%rip), %rsi
	movq	%r13, %rdi
	movq	%r8, -136(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-136(%rbp), %r8
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue9getObjectERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %rbx
	cmpq	-128(%rbp), %rdi
	je	.L3897
	call	_ZdlPv@PLT
.L3897:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue15booleanPropertyERKNS_8String16Eb@PLT
	testb	%al, %al
	jne	.L3903
	movq	-144(%rbp), %rax
	cmpb	$0, (%rax)
	je	.L3900
	movl	4(%rax), %edx
	movq	8(%r12), %rax
	movq	32(%r12), %rdi
	movl	16(%rax), %esi
	call	_ZNK12v8_inspector15V8InspectorImpl10getContextEii@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L3913
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	_ZN12v8_inspector18V8RuntimeAgentImpl10addBindingEPNS_16InspectedContextERKNS_8String16E
	movq	%rbx, %rdi
	xorl	%edx, %edx
	movq	%r14, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue10setBooleanERKNS_8String16Eb@PLT
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	jmp	.L3892
	.p2align 4,,10
	.p2align 3
.L3900:
	movq	%r12, %xmm1
	movq	%r14, %xmm0
	movq	%r14, %rsi
	movq	%rbx, %rdi
	punpcklqdq	%xmm1, %xmm0
	movl	$1, %edx
	movaps	%xmm0, -128(%rbp)
	call	_ZN12v8_inspector8protocol15DictionaryValue10setBooleanERKNS_8String16Eb@PLT
	movdqa	-128(%rbp), %xmm0
	movq	32(%r12), %rdi
	movq	%r13, %rdx
	leaq	_ZNSt17_Function_handlerIFvPN12v8_inspector16InspectedContextEEZNS0_18V8RuntimeAgentImpl10addBindingERKNS0_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIiEEEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector18V8RuntimeAgentImpl10addBindingERKNS1_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIiEEEUlPNS1_16InspectedContextEE_E10_M_managerERSt9_Any_dataRKSF_St18_Manager_operation(%rip), %rcx
	movaps	%xmm0, -96(%rbp)
	movq	%rax, %xmm2
	movq	%rcx, %xmm0
	movq	8(%r12), %rax
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -80(%rbp)
	movl	16(%rax), %esi
	call	_ZN12v8_inspector15V8InspectorImpl14forEachContextEiRKSt8functionIFvPNS_16InspectedContextEEE@PLT
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L3903
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	*%rax
.L3903:
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
.L3892:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3914
	addq	$104, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3912:
	.cfi_restore_state
	movl	$96, %edi
	movq	%r8, -136(%rbp)
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	leaq	_ZN12v8_inspector23V8RuntimeAgentImplStateL8bindingsE(%rip), %rsi
	movq	%r13, %rdi
	movq	%rbx, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-136(%rbp), %r8
	leaq	-104(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue9setObjectERKNS_8String16ESt10unique_ptrIS1_St14default_deleteIS1_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	-128(%rbp), %rdi
	je	.L3895
	call	_ZdlPv@PLT
.L3895:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3896
	movq	(%rdi), %rax
	call	*24(%rax)
.L3896:
	movq	16(%r12), %r8
	jmp	.L3894
	.p2align 4,,10
	.p2align 3
.L3913:
	leaq	.LC6(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	-128(%rbp), %rdi
	je	.L3892
	call	_ZdlPv@PLT
	jmp	.L3892
.L3914:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8046:
	.size	_ZN12v8_inspector18V8RuntimeAgentImpl10addBindingERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIiEE, .-_ZN12v8_inspector18V8RuntimeAgentImpl10addBindingERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIiEE
	.section	.text._ZN12v8_inspector18V8RuntimeAgentImpl11addBindingsEPNS_16InspectedContextE.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector18V8RuntimeAgentImpl11addBindingsEPNS_16InspectedContextE.part.0, @function
_ZN12v8_inspector18V8RuntimeAgentImpl11addBindingsEPNS_16InspectedContextE.part.0:
.LFB14571:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-112(%rbp), %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -128(%rbp)
	movq	16(%rdi), %r13
	leaq	_ZN12v8_inspector23V8RuntimeAgentImplStateL8bindingsE(%rip), %rsi
	movq	%r12, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	leaq	-96(%rbp), %r13
	call	_ZNK12v8_inspector8protocol15DictionaryValue9getObjectERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %rbx
	cmpq	%r13, %rdi
	je	.L3916
	call	_ZdlPv@PLT
.L3916:
	testq	%rbx, %rbx
	je	.L3915
	cmpq	$0, 40(%rbx)
	je	.L3915
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L3921:
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue2atEm@PLT
	movq	-112(%rbp), %rdi
	movq	-72(%rbp), %rdx
	cmpq	%r13, %rdi
	je	.L3918
	movq	%rdx, -120(%rbp)
	call	_ZdlPv@PLT
	movq	-120(%rbp), %rdx
.L3918:
	testq	%rdx, %rdx
	je	.L3919
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue2atEm@PLT
	movq	-128(%rbp), %rsi
	movq	%r14, %rdi
	movq	%r12, %rdx
	call	_ZN12v8_inspector18V8RuntimeAgentImpl10addBindingEPNS_16InspectedContextERKNS_8String16E
	movq	-112(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L3919
	call	_ZdlPv@PLT
.L3919:
	addq	$1, %r15
	cmpq	%r15, 40(%rbx)
	ja	.L3921
.L3915:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3931
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3931:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE14571:
	.size	_ZN12v8_inspector18V8RuntimeAgentImpl11addBindingsEPNS_16InspectedContextE.part.0, .-_ZN12v8_inspector18V8RuntimeAgentImpl11addBindingsEPNS_16InspectedContextE.part.0
	.section	.text._ZNSt17_Function_handlerIFvPN12v8_inspector16InspectedContextEEZNS0_18V8RuntimeAgentImpl7restoreEvEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvPN12v8_inspector16InspectedContextEEZNS0_18V8RuntimeAgentImpl7restoreEvEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_, @function
_ZNSt17_Function_handlerIFvPN12v8_inspector16InspectedContextEEZNS0_18V8RuntimeAgentImpl7restoreEvEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_:
.LFB11008:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	cmpb	$0, 40(%rdi)
	jne	.L3934
	ret
	.p2align 4,,10
	.p2align 3
.L3934:
	movq	(%rsi), %rsi
	jmp	_ZN12v8_inspector18V8RuntimeAgentImpl11addBindingsEPNS_16InspectedContextE.part.0
	.cfi_endproc
.LFE11008:
	.size	_ZNSt17_Function_handlerIFvPN12v8_inspector16InspectedContextEEZNS0_18V8RuntimeAgentImpl7restoreEvEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_, .-_ZNSt17_Function_handlerIFvPN12v8_inspector16InspectedContextEEZNS0_18V8RuntimeAgentImpl7restoreEvEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_
	.section	.text._ZN12v8_inspector18V8RuntimeAgentImpl13bindingCalledERKNS_8String16ES3_i,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector18V8RuntimeAgentImpl13bindingCalledERKNS_8String16ES3_i
	.type	_ZN12v8_inspector18V8RuntimeAgentImpl13bindingCalledERKNS_8String16ES3_i, @function
_ZN12v8_inspector18V8RuntimeAgentImpl13bindingCalledERKNS_8String16ES3_i:
.LFB8058:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	leaq	_ZN12v8_inspector23V8RuntimeAgentImplStateL8bindingsE(%rip), %rsi
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-96(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%rdx, -104(%rbp)
	movq	16(%rdi), %r14
	movq	%r12, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue9getObjectERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r12
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3936
	call	_ZdlPv@PLT
.L3936:
	testq	%r12, %r12
	je	.L3935
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	testq	%rax, %rax
	je	.L3935
	movq	-104(%rbp), %rdx
	leaq	24(%rbx), %rdi
	movl	%r15d, %ecx
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol7Runtime8Frontend13bindingCalledERKNS_8String16ES5_i@PLT
.L3935:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3945
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3945:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8058:
	.size	_ZN12v8_inspector18V8RuntimeAgentImpl13bindingCalledERKNS_8String16ES3_i, .-_ZN12v8_inspector18V8RuntimeAgentImpl13bindingCalledERKNS_8String16ES3_i
	.section	.text._ZN12v8_inspector18V8RuntimeAgentImpl11addBindingsEPNS_16InspectedContextE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector18V8RuntimeAgentImpl11addBindingsEPNS_16InspectedContextE
	.type	_ZN12v8_inspector18V8RuntimeAgentImpl11addBindingsEPNS_16InspectedContextE, @function
_ZN12v8_inspector18V8RuntimeAgentImpl11addBindingsEPNS_16InspectedContextE:
.LFB8059:
	.cfi_startproc
	endbr64
	cmpb	$0, 40(%rdi)
	jne	.L3948
	ret
	.p2align 4,,10
	.p2align 3
.L3948:
	jmp	_ZN12v8_inspector18V8RuntimeAgentImpl11addBindingsEPNS_16InspectedContextE.part.0
	.cfi_endproc
.LFE8059:
	.size	_ZN12v8_inspector18V8RuntimeAgentImpl11addBindingsEPNS_16InspectedContextE, .-_ZN12v8_inspector18V8RuntimeAgentImpl11addBindingsEPNS_16InspectedContextE
	.section	.text._ZN12v8_inspector18V8RuntimeAgentImpl7restoreEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector18V8RuntimeAgentImpl7restoreEv
	.type	_ZN12v8_inspector18V8RuntimeAgentImpl7restoreEv, @function
_ZN12v8_inspector18V8RuntimeAgentImpl7restoreEv:
.LFB8063:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN12v8_inspector23V8RuntimeAgentImplStateL14runtimeEnabledE(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-80(%rbp), %r14
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-96(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$64, %rsp
	movq	16(%rdi), %r13
	movq	%r12, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	xorl	%edx, %edx
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue15booleanPropertyERKNS_8String16Eb@PLT
	movq	-96(%rbp), %rdi
	movl	%eax, %r13d
	cmpq	%r14, %rdi
	je	.L3950
	call	_ZdlPv@PLT
.L3950:
	testb	%r13b, %r13b
	jne	.L3967
.L3949:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3968
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3967:
	.cfi_restore_state
	leaq	24(%rbx), %rdi
	call	_ZN12v8_inspector8protocol7Runtime8Frontend24executionContextsClearedEv@PLT
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	*56(%rax)
	movq	-88(%rbp), %rdi
	leaq	-72(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3952
	call	_ZdlPv@PLT
.L3952:
	movq	16(%rbx), %r13
	leaq	_ZN12v8_inspector23V8RuntimeAgentImplStateL28customObjectFormatterEnabledE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue15booleanPropertyERKNS_8String16Eb@PLT
	movq	-96(%rbp), %rdi
	movl	%eax, %r13d
	cmpq	%r14, %rdi
	je	.L3953
	call	_ZdlPv@PLT
.L3953:
	movq	8(%rbx), %rdi
	testb	%r13b, %r13b
	jne	.L3969
.L3954:
	leaq	_ZNSt17_Function_handlerIFvPN12v8_inspector16InspectedContextEEZNS0_18V8RuntimeAgentImpl7restoreEvEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_(%rip), %rax
	movq	32(%rbx), %r8
	movq	%rbx, -96(%rbp)
	movq	%r12, %rdx
	leaq	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector18V8RuntimeAgentImpl7restoreEvEUlPNS1_16InspectedContextEE_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation(%rip), %rcx
	movq	%rax, %xmm1
	movq	%rcx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	movl	16(%rdi), %esi
	movq	%r8, %rdi
	call	_ZN12v8_inspector15V8InspectorImpl14forEachContextEiRKSt8functionIFvPNS_16InspectedContextEEE@PLT
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L3949
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3949
	.p2align 4,,10
	.p2align 3
.L3969:
	movl	$1, %esi
	call	_ZN12v8_inspector22V8InspectorSessionImpl31setCustomObjectFormatterEnabledEb@PLT
	movq	8(%rbx), %rdi
	jmp	.L3954
.L3968:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8063:
	.size	_ZN12v8_inspector18V8RuntimeAgentImpl7restoreEv, .-_ZN12v8_inspector18V8RuntimeAgentImpl7restoreEv
	.section	.text._ZN12v8_inspector18V8RuntimeAgentImpl5resetEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector18V8RuntimeAgentImpl5resetEv
	.type	_ZN12v8_inspector18V8RuntimeAgentImpl5resetEv, @function
_ZN12v8_inspector18V8RuntimeAgentImpl5resetEv:
.LFB8069:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$64, %rsp
	movq	64(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%r12, %r12
	jne	.L3976
	jmp	.L3971
	.p2align 4,,10
	.p2align 3
.L3999:
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L3971
.L3975:
	movq	%r13, %r12
.L3976:
	movq	48(%r12), %r14
	movq	(%r12), %r13
	testq	%r14, %r14
	je	.L3972
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L3973
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L3973:
	movl	$8, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L3972:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	jne	.L3999
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L3975
.L3971:
	movq	56(%rbx), %rax
	movq	48(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	cmpb	$0, 40(%rbx)
	movq	$0, 72(%rbx)
	movq	$0, 64(%rbx)
	jne	.L4000
.L3970:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4001
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4000:
	.cfi_restore_state
	movq	8(%rbx), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector18V8RuntimeAgentImpl5resetEvEUlPNS1_16InspectedContextEE_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation(%rip), %rcx
	movq	32(%rbx), %rdi
	leaq	-80(%rbp), %r12
	movq	%rcx, %xmm0
	movl	20(%rax), %edx
	movl	%edx, -84(%rbp)
	leaq	-84(%rbp), %rdx
	movq	%rdx, -80(%rbp)
	leaq	_ZNSt17_Function_handlerIFvPN12v8_inspector16InspectedContextEEZNS0_18V8RuntimeAgentImpl5resetEvEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_(%rip), %rdx
	movq	%rdx, %xmm1
	movq	%r12, %rdx
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -64(%rbp)
	movl	16(%rax), %esi
	call	_ZN12v8_inspector15V8InspectorImpl14forEachContextEiRKSt8functionIFvPNS_16InspectedContextEEE@PLT
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L3978
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	*%rax
.L3978:
	leaq	24(%rbx), %rdi
	call	_ZN12v8_inspector8protocol7Runtime8Frontend24executionContextsClearedEv@PLT
	jmp	.L3970
.L4001:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8069:
	.size	_ZN12v8_inspector18V8RuntimeAgentImpl5resetEv, .-_ZN12v8_inspector18V8RuntimeAgentImpl5resetEv
	.section	.text._ZN12v8_inspector18V8RuntimeAgentImpl7disableEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector18V8RuntimeAgentImpl7disableEv
	.type	_ZN12v8_inspector18V8RuntimeAgentImpl7disableEv, @function
_ZN12v8_inspector18V8RuntimeAgentImpl7disableEv:
.LFB8068:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 40(%rsi)
	je	.L4007
	movb	$0, 40(%rsi)
	movq	16(%rsi), %r14
	leaq	-112(%rbp), %r13
	movq	%rsi, %rbx
	movq	%r13, %rdi
	leaq	_ZN12v8_inspector23V8RuntimeAgentImplStateL14runtimeEnabledE(%rip), %rsi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue10setBooleanERKNS_8String16Eb@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %r14
	cmpq	%r14, %rdi
	je	.L4004
	call	_ZdlPv@PLT
.L4004:
	movq	16(%rbx), %r15
	leaq	_ZN12v8_inspector23V8RuntimeAgentImplStateL8bindingsE(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue6removeERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L4005
	call	_ZdlPv@PLT
.L4005:
	movq	32(%rbx), %rdi
	call	_ZN12v8_inspector15V8InspectorImpl29disableStackCapturingIfNeededEv@PLT
	movq	8(%rbx), %rdi
	xorl	%esi, %esi
	call	_ZN12v8_inspector22V8InspectorSessionImpl31setCustomObjectFormatterEnabledEb@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector18V8RuntimeAgentImpl5resetEv
	movq	32(%rbx), %rax
	leaq	_ZN12v8_inspector17V8InspectorClient27endEnsureAllContextsInGroupEi(%rip), %rcx
	movq	16(%rax), %rdi
	movq	(%rdi), %rax
	movq	112(%rax), %rdx
	movq	8(%rbx), %rax
	cmpq	%rcx, %rdx
	jne	.L4015
	movq	192(%rax), %rsi
	testq	%rsi, %rsi
	je	.L4007
.L4017:
	cmpb	$0, 32(%rsi)
	jne	.L4007
	movq	(%rsi), %rax
	movq	%r13, %rdi
	xorl	%edx, %edx
	call	*120(%rax)
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4007
	call	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L4007:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4016
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4015:
	.cfi_restore_state
	movl	16(%rax), %esi
	call	*%rdx
	movq	8(%rbx), %rax
	movq	192(%rax), %rsi
	testq	%rsi, %rsi
	jne	.L4017
	jmp	.L4007
.L4016:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8068:
	.size	_ZN12v8_inspector18V8RuntimeAgentImpl7disableEv, .-_ZN12v8_inspector18V8RuntimeAgentImpl7disableEv
	.section	.text._ZN12v8_inspector18V8RuntimeAgentImpl31reportExecutionContextDestroyedEPNS_16InspectedContextE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector18V8RuntimeAgentImpl31reportExecutionContextDestroyedEPNS_16InspectedContextE
	.type	_ZN12v8_inspector18V8RuntimeAgentImpl31reportExecutionContextDestroyedEPNS_16InspectedContextE, @function
_ZN12v8_inspector18V8RuntimeAgentImpl31reportExecutionContextDestroyedEPNS_16InspectedContextE:
.LFB8072:
	.cfi_startproc
	endbr64
	cmpb	$0, 40(%rdi)
	jne	.L4029
	ret
	.p2align 4,,10
	.p2align 3
.L4029:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	8(%rdi), %rax
	movq	%rdi, %rbx
	movq	%r12, %rdi
	movl	20(%rax), %esi
	call	_ZNK12v8_inspector16InspectedContext10isReportedEi@PLT
	testb	%al, %al
	jne	.L4030
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4030:
	.cfi_restore_state
	movq	8(%rbx), %rax
	movq	%r12, %rdi
	xorl	%edx, %edx
	movl	20(%rax), %esi
	call	_ZN12v8_inspector16InspectedContext11setReportedEib@PLT
	movl	16(%r12), %esi
	leaq	24(%rbx), %rdi
	popq	%rbx
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN12v8_inspector8protocol7Runtime8Frontend25executionContextDestroyedEi@PLT
	.cfi_endproc
.LFE8072:
	.size	_ZN12v8_inspector18V8RuntimeAgentImpl31reportExecutionContextDestroyedEPNS_16InspectedContextE, .-_ZN12v8_inspector18V8RuntimeAgentImpl31reportExecutionContextDestroyedEPNS_16InspectedContextE
	.section	.text._ZN12v8_inspector18V8RuntimeAgentImpl7inspectESt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteIS4_EES1_INS2_15DictionaryValueES5_IS8_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector18V8RuntimeAgentImpl7inspectESt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteIS4_EES1_INS2_15DictionaryValueES5_IS8_EE
	.type	_ZN12v8_inspector18V8RuntimeAgentImpl7inspectESt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteIS4_EES1_INS2_15DictionaryValueES5_IS8_EE, @function
_ZN12v8_inspector18V8RuntimeAgentImpl7inspectESt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteIS4_EES1_INS2_15DictionaryValueES5_IS8_EE:
.LFB8073:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$40, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 40(%rdi)
	jne	.L4044
.L4031:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4045
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4044:
	.cfi_restore_state
	movq	(%rdx), %rax
	movq	$0, (%rdx)
	addq	$24, %rdi
	leaq	-32(%rbp), %rdx
	movq	%rax, -32(%rbp)
	movq	(%rsi), %rax
	movq	$0, (%rsi)
	leaq	-40(%rbp), %rsi
	movq	%rax, -40(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime8Frontend16inspectRequestedESt10unique_ptrINS1_12RemoteObjectESt14default_deleteIS4_EES3_INS0_15DictionaryValueES5_IS8_EE@PLT
	movq	-40(%rbp), %r12
	testq	%r12, %r12
	je	.L4033
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4034
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4033:
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4031
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L4031
	.p2align 4,,10
	.p2align 3
.L4034:
	call	*%rax
	jmp	.L4033
.L4045:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8073:
	.size	_ZN12v8_inspector18V8RuntimeAgentImpl7inspectESt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteIS4_EES1_INS2_15DictionaryValueES5_IS8_EE, .-_ZN12v8_inspector18V8RuntimeAgentImpl7inspectESt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteIS4_EES1_INS2_15DictionaryValueES5_IS8_EE
	.section	.text._ZN12v8_inspector18V8RuntimeAgentImpl12messageAddedEPNS_16V8ConsoleMessageE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector18V8RuntimeAgentImpl12messageAddedEPNS_16V8ConsoleMessageE
	.type	_ZN12v8_inspector18V8RuntimeAgentImpl12messageAddedEPNS_16V8ConsoleMessageE, @function
_ZN12v8_inspector18V8RuntimeAgentImpl12messageAddedEPNS_16V8ConsoleMessageE:
.LFB8074:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	cmpb	$0, 40(%rdi)
	movq	%rdi, %rbx
	jne	.L4049
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4049:
	.cfi_restore_state
	leaq	24(%rbx), %r12
	movq	8(%rbx), %rdx
	movq	%rsi, %rdi
	movl	$1, %ecx
	movq	%r12, %rsi
	call	_ZNK12v8_inspector16V8ConsoleMessage16reportToFrontendEPNS_8protocol7Runtime8FrontendEPNS_22V8InspectorSessionImplEb@PLT
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol7Runtime8Frontend5flushEv@PLT
	movq	8(%rbx), %rax
	movq	32(%rbx), %rdi
	popq	%rbx
	popq	%r12
	movl	16(%rax), %esi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN12v8_inspector15V8InspectorImpl24hasConsoleMessageStorageEi@PLT
	.cfi_endproc
.LFE8074:
	.size	_ZN12v8_inspector18V8RuntimeAgentImpl12messageAddedEPNS_16V8ConsoleMessageE, .-_ZN12v8_inspector18V8RuntimeAgentImpl12messageAddedEPNS_16V8ConsoleMessageE
	.section	.text._ZN12v8_inspector18V8RuntimeAgentImpl13reportMessageEPNS_16V8ConsoleMessageEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector18V8RuntimeAgentImpl13reportMessageEPNS_16V8ConsoleMessageEb
	.type	_ZN12v8_inspector18V8RuntimeAgentImpl13reportMessageEPNS_16V8ConsoleMessageEb, @function
_ZN12v8_inspector18V8RuntimeAgentImpl13reportMessageEPNS_16V8ConsoleMessageEb:
.LFB8075:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzbl	%dl, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	leaq	24(%rbx), %r12
	movq	8(%rbx), %rdx
	movq	%r12, %rsi
	call	_ZNK12v8_inspector16V8ConsoleMessage16reportToFrontendEPNS_8protocol7Runtime8FrontendEPNS_22V8InspectorSessionImplEb@PLT
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol7Runtime8Frontend5flushEv@PLT
	movq	8(%rbx), %rax
	movq	32(%rbx), %rdi
	popq	%rbx
	popq	%r12
	movl	16(%rax), %esi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN12v8_inspector15V8InspectorImpl24hasConsoleMessageStorageEi@PLT
	.cfi_endproc
.LFE8075:
	.size	_ZN12v8_inspector18V8RuntimeAgentImpl13reportMessageEPNS_16V8ConsoleMessageEb, .-_ZN12v8_inspector18V8RuntimeAgentImpl13reportMessageEPNS_16V8ConsoleMessageEb
	.section	.text._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_,"axG",@progbits,_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	.type	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_, @function
_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_:
.LFB9542:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rsi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	addq	$16, %rsi
	movq	(%rdi), %rdi
	cmpq	%rax, %rsi
	je	.L4068
	leaq	16(%r12), %rdx
	cmpq	%rdx, %rdi
	je	.L4069
	movq	%rax, (%r12)
	movq	8(%rbx), %rax
	movq	16(%r12), %rdx
	movq	%rax, 8(%r12)
	movq	16(%rbx), %rax
	movq	%rax, 16(%r12)
	testq	%rdi, %rdi
	je	.L4058
	movq	%rdi, (%rbx)
	movq	%rdx, 16(%rbx)
.L4056:
	xorl	%eax, %eax
	movq	$0, 8(%rbx)
	movw	%ax, (%rdi)
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4069:
	.cfi_restore_state
	movq	%rax, (%r12)
	movq	8(%rbx), %rax
	movq	%rax, 8(%r12)
	movq	16(%rbx), %rax
	movq	%rax, 16(%r12)
.L4058:
	movq	%rsi, (%rbx)
	movq	%rsi, %rdi
	jmp	.L4056
	.p2align 4,,10
	.p2align 3
.L4068:
	movq	8(%rbx), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.L4054
	cmpq	$1, %rax
	je	.L4070
	movq	%rax, %rdx
	addq	%rdx, %rdx
	je	.L4054
	call	memmove@PLT
	movq	8(%rbx), %rax
	movq	(%r12), %rdi
	leaq	(%rax,%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L4054:
	xorl	%ecx, %ecx
	movq	%rax, 8(%r12)
	movw	%cx, (%rdi,%rdx)
	movq	(%rbx), %rdi
	jmp	.L4056
	.p2align 4,,10
	.p2align 3
.L4070:
	movzwl	16(%rbx), %eax
	movw	%ax, (%rdi)
	movq	8(%rbx), %rax
	movq	(%r12), %rdi
	leaq	(%rax,%rax), %rdx
	jmp	.L4054
	.cfi_endproc
.LFE9542:
	.size	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_, .-_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	.section	.rodata._ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_.str1.1,"aMS",@progbits,1
.LC7:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	.type	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_, @function
_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_:
.LFB10982:
	.cfi_startproc
	endbr64
	movabsq	$-3689348814741910323, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rbx
	movq	(%rdi), %r14
	movq	%rdi, -80(%rbp)
	movabsq	$230584300921369395, %rdi
	movq	%rsi, -56(%rbp)
	movq	%rbx, %rax
	subq	%r14, %rax
	sarq	$3, %rax
	imulq	%rcx, %rax
	cmpq	%rdi, %rax
	je	.L4099
	movq	%rsi, %rcx
	subq	%r14, %rcx
	testq	%rax, %rax
	je	.L4091
	movabsq	$9223372036854775800, %r12
	leaq	(%rax,%rax), %r8
	cmpq	%r8, %rax
	jbe	.L4100
.L4073:
	movq	%r12, %rdi
	movq	%rdx, -96(%rbp)
	movq	%rcx, -88(%rbp)
	call	_Znwm@PLT
	movq	-88(%rbp), %rcx
	movq	-96(%rbp), %rdx
	movq	%rax, -64(%rbp)
	addq	%rax, %r12
	movq	%r12, -72(%rbp)
	leaq	40(%rax), %r12
.L4090:
	movq	-64(%rbp), %rax
	movq	(%rdx), %rdi
	addq	%rcx, %rax
	leaq	16(%rax), %rcx
	movq	%rcx, (%rax)
	leaq	16(%rdx), %rcx
	cmpq	%rcx, %rdi
	je	.L4101
	movq	%rdi, (%rax)
	movq	16(%rdx), %rdi
	movq	%rdi, 16(%rax)
.L4076:
	movq	%rcx, (%rdx)
	xorl	%ecx, %ecx
	movq	8(%rdx), %rdi
	movw	%cx, 16(%rdx)
	movq	$0, 8(%rdx)
	movq	32(%rdx), %rdx
	movq	%rdi, 8(%rax)
	movq	%rdx, 32(%rax)
	movq	-56(%rbp), %rax
	cmpq	%r14, %rax
	je	.L4077
	leaq	-40(%rax), %rdx
	movq	-64(%rbp), %r15
	leaq	16(%r14), %r13
	movabsq	$922337203685477581, %rcx
	subq	%r14, %rdx
	shrq	$3, %rdx
	imulq	%rcx, %rdx
	movabsq	$2305843009213693951, %rcx
	andq	%rcx, %rdx
	movq	%rdx, -88(%rbp)
	leaq	(%rdx,%rdx,4), %rdx
	leaq	56(%r14,%rdx,8), %r12
	.p2align 4,,10
	.p2align 3
.L4083:
	leaq	16(%r15), %rcx
	movq	%rcx, (%r15)
	movq	-16(%r13), %rcx
	cmpq	%r13, %rcx
	je	.L4102
.L4078:
	movq	%rcx, (%r15)
	movq	0(%r13), %rcx
	movq	%rcx, 16(%r15)
.L4079:
	movq	-8(%r13), %rcx
	xorl	%eax, %eax
	movq	%rcx, 8(%r15)
	movq	16(%r13), %rcx
	movq	%r13, -16(%r13)
	movq	$0, -8(%r13)
	movw	%ax, 0(%r13)
	movq	%rcx, 32(%r15)
	movq	-16(%r13), %rdi
	cmpq	%r13, %rdi
	je	.L4080
	call	_ZdlPv@PLT
	addq	$40, %r13
	addq	$40, %r15
	cmpq	%r13, %r12
	jne	.L4083
.L4081:
	movq	-88(%rbp), %rax
	movq	-64(%rbp), %rsi
	leaq	10(%rax,%rax,4), %rax
	leaq	(%rsi,%rax,8), %r12
.L4077:
	movq	-56(%rbp), %rax
	cmpq	%rbx, %rax
	je	.L4084
	movq	%r12, %rdx
	.p2align 4,,10
	.p2align 3
.L4088:
	leaq	16(%rdx), %rcx
	leaq	16(%rax), %rdi
	movq	%rcx, (%rdx)
	movq	(%rax), %rcx
	cmpq	%rdi, %rcx
	je	.L4103
	movq	%rcx, (%rdx)
	movq	16(%rax), %rcx
	addq	$40, %rax
	addq	$40, %rdx
	movq	%rcx, -24(%rdx)
	movq	-32(%rax), %rcx
	movq	%rcx, -32(%rdx)
	movq	-8(%rax), %rcx
	movq	%rcx, -8(%rdx)
	cmpq	%rbx, %rax
	jne	.L4088
.L4086:
	movq	%rbx, %rsi
	subq	-56(%rbp), %rsi
	leaq	-40(%rsi), %rax
	shrq	$3, %rax
	leaq	40(%r12,%rax,8), %r12
.L4084:
	testq	%r14, %r14
	je	.L4089
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L4089:
	movq	-64(%rbp), %xmm0
	movq	-80(%rbp), %rax
	movq	%r12, %xmm3
	movq	-72(%rbp), %rsi
	punpcklqdq	%xmm3, %xmm0
	movq	%rsi, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4080:
	.cfi_restore_state
	addq	$40, %r13
	addq	$40, %r15
	cmpq	%r12, %r13
	je	.L4081
	leaq	16(%r15), %rcx
	movq	%rcx, (%r15)
	movq	-16(%r13), %rcx
	cmpq	%r13, %rcx
	jne	.L4078
.L4102:
	movdqu	0(%r13), %xmm1
	movups	%xmm1, 16(%r15)
	jmp	.L4079
	.p2align 4,,10
	.p2align 3
.L4103:
	movq	8(%rax), %rcx
	movdqu	16(%rax), %xmm2
	addq	$40, %rax
	addq	$40, %rdx
	movq	%rcx, -32(%rdx)
	movq	-8(%rax), %rcx
	movups	%xmm2, -24(%rdx)
	movq	%rcx, -8(%rdx)
	cmpq	%rax, %rbx
	jne	.L4088
	jmp	.L4086
	.p2align 4,,10
	.p2align 3
.L4100:
	testq	%r8, %r8
	jne	.L4074
	movq	$0, -72(%rbp)
	movl	$40, %r12d
	movq	$0, -64(%rbp)
	jmp	.L4090
	.p2align 4,,10
	.p2align 3
.L4091:
	movl	$40, %r12d
	jmp	.L4073
	.p2align 4,,10
	.p2align 3
.L4101:
	movdqu	16(%rdx), %xmm4
	movups	%xmm4, 16(%rax)
	jmp	.L4076
.L4074:
	cmpq	%rdi, %r8
	cmovbe	%r8, %rdi
	imulq	$40, %rdi, %r12
	jmp	.L4073
.L4099:
	leaq	.LC7(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE10982:
	.size	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_, .-_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	.section	.rodata._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_.str1.1,"aMS",@progbits,1
.LC8:
	.string	"basic_string::_M_create"
	.section	.text._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_,"axG",@progbits,_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	.type	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_, @function
_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_:
.LFB11478:
	.cfi_startproc
	endbr64
	cmpq	%rsi, %rdi
	je	.L4123
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16(%rdi), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %r13
	movq	8(%rsi), %r15
	cmpq	%r13, %rdx
	je	.L4114
	movq	16(%rdi), %rax
.L4106:
	cmpq	%r15, %rax
	jb	.L4126
	leaq	(%r15,%r15), %rdx
	testq	%r15, %r15
	je	.L4110
.L4129:
	movq	(%r12), %rsi
	cmpq	$1, %r15
	je	.L4127
	testq	%rdx, %rdx
	je	.L4110
	movq	%r13, %rdi
	call	memmove@PLT
	movq	(%rbx), %r13
.L4110:
	xorl	%eax, %eax
	movq	%r15, 8(%rbx)
	movw	%ax, 0(%r13,%r15,2)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4126:
	.cfi_restore_state
	movabsq	$2305843009213693951, %rcx
	cmpq	%rcx, %r15
	ja	.L4128
	addq	%rax, %rax
	movq	%r15, %r14
	cmpq	%rax, %r15
	jnb	.L4109
	cmpq	%rcx, %rax
	cmovbe	%rax, %rcx
	movq	%rcx, %r14
.L4109:
	leaq	2(%r14,%r14), %rdi
	movq	%rdx, -56(%rbp)
	call	_Znwm@PLT
	movq	(%rbx), %rdi
	movq	-56(%rbp), %rdx
	movq	%rax, %r13
	cmpq	%rdi, %rdx
	je	.L4113
	call	_ZdlPv@PLT
.L4113:
	movq	%r13, (%rbx)
	leaq	(%r15,%r15), %rdx
	movq	%r14, 16(%rbx)
	testq	%r15, %r15
	je	.L4110
	jmp	.L4129
	.p2align 4,,10
	.p2align 3
.L4123:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L4114:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	$7, %eax
	jmp	.L4106
	.p2align 4,,10
	.p2align 3
.L4127:
	movzwl	(%rsi), %eax
	movw	%ax, 0(%r13)
	movq	(%rbx), %r13
	jmp	.L4110
.L4128:
	leaq	.LC8(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE11478:
	.size	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_, .-_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	.section	.text._ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrIN2v86GlobalINS5_6ScriptEEESt14default_deleteIS8_EEESaISC_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSE_18_Mod_range_hashingENSE_20_Default_ranged_hashENSE_20_Prime_rehash_policyENSE_17_Hashtable_traitsILb1ELb0ELb1EEEE5eraseENSE_20_Node_const_iteratorISC_Lb0ELb1EEE,"axG",@progbits,_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrIN2v86GlobalINS5_6ScriptEEESt14default_deleteIS8_EEESaISC_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSE_18_Mod_range_hashingENSE_20_Default_ranged_hashENSE_20_Prime_rehash_policyENSE_17_Hashtable_traitsILb1ELb0ELb1EEEE5eraseENSE_20_Node_const_iteratorISC_Lb0ELb1EEE,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrIN2v86GlobalINS5_6ScriptEEESt14default_deleteIS8_EEESaISC_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSE_18_Mod_range_hashingENSE_20_Default_ranged_hashENSE_20_Prime_rehash_policyENSE_17_Hashtable_traitsILb1ELb0ELb1EEEE5eraseENSE_20_Node_const_iteratorISC_Lb0ELb1EEE
	.type	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrIN2v86GlobalINS5_6ScriptEEESt14default_deleteIS8_EEESaISC_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSE_18_Mod_range_hashingENSE_20_Default_ranged_hashENSE_20_Prime_rehash_policyENSE_17_Hashtable_traitsILb1ELb0ELb1EEEE5eraseENSE_20_Node_const_iteratorISC_Lb0ELb1EEE, @function
_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrIN2v86GlobalINS5_6ScriptEEESt14default_deleteIS8_EEESaISC_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSE_18_Mod_range_hashingENSE_20_Default_ranged_hashENSE_20_Prime_rehash_policyENSE_17_Hashtable_traitsILb1ELb0ELb1EEEE5eraseENSE_20_Node_const_iteratorISC_Lb0ELb1EEE:
.LFB12839:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	56(%r12), %rax
	movq	%rdi, %rbx
	movq	8(%rdi), %rsi
	movq	(%rbx), %r8
	divq	%rsi
	leaq	0(,%rdx,8), %r10
	movq	%rdx, %rdi
	leaq	(%r8,%r10), %r9
	movq	(%r9), %rax
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L4131:
	movq	%rdx, %rcx
	movq	(%rdx), %rdx
	cmpq	%rdx, %r12
	jne	.L4131
	movq	(%r12), %r13
	cmpq	%rcx, %rax
	je	.L4151
	testq	%r13, %r13
	je	.L4134
	movq	56(%r13), %rax
	xorl	%edx, %edx
	divq	%rsi
	cmpq	%rdx, %rdi
	je	.L4134
	movq	%rcx, (%r8,%rdx,8)
	movq	(%r12), %r13
.L4134:
	movq	%r13, (%rcx)
	movq	48(%r12), %r14
	testq	%r14, %r14
	je	.L4136
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L4137
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L4137:
	movl	$8, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L4136:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4138
	call	_ZdlPv@PLT
.L4138:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	movq	%r13, %rax
	subq	$1, 24(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4151:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L4139
	movq	56(%r13), %rax
	xorl	%edx, %edx
	divq	%rsi
	cmpq	%rdx, %rdi
	je	.L4134
	movq	%rcx, (%r8,%rdx,8)
	addq	(%rbx), %r10
	movq	(%r10), %rax
	movq	%r10, %r9
.L4133:
	leaq	16(%rbx), %rdx
	cmpq	%rdx, %rax
	je	.L4152
.L4135:
	movq	$0, (%r9)
	movq	(%r12), %r13
	jmp	.L4134
.L4139:
	movq	%rcx, %rax
	jmp	.L4133
.L4152:
	movq	%r13, 16(%rbx)
	jmp	.L4135
	.cfi_endproc
.LFE12839:
	.size	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrIN2v86GlobalINS5_6ScriptEEESt14default_deleteIS8_EEESaISC_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSE_18_Mod_range_hashingENSE_20_Default_ranged_hashENSE_20_Prime_rehash_policyENSE_17_Hashtable_traitsILb1ELb0ELb1EEEE5eraseENSE_20_Node_const_iteratorISC_Lb0ELb1EEE, .-_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrIN2v86GlobalINS5_6ScriptEEESt14default_deleteIS8_EEESaISC_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSE_18_Mod_range_hashingENSE_20_Default_ranged_hashENSE_20_Prime_rehash_policyENSE_17_Hashtable_traitsILb1ELb0ELb1EEEE5eraseENSE_20_Node_const_iteratorISC_Lb0ELb1EEE
	.section	.text._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm,"axG",@progbits,_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm
	.type	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm, @function
_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm:
.LFB13007:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%r8, %r9
	subq	%rdx, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	16(%rdi), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	addq	%rdx, %rsi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	8(%rdi), %rax
	movq	%rsi, -72(%rbp)
	movq	%rax, %r13
	leaq	(%r9,%rax), %r15
	subq	%rsi, %r13
	cmpq	(%rdi), %r14
	je	.L4166
	movq	16(%rdi), %rax
.L4154:
	movabsq	$2305843009213693951, %rdx
	cmpq	%rdx, %r15
	ja	.L4187
	cmpq	%rax, %r15
	jbe	.L4156
	addq	%rax, %rax
	cmpq	%rax, %r15
	jnb	.L4156
	movabsq	$4611686018427387904, %rdi
	movq	%rdx, %r15
	cmpq	%rdx, %rax
	ja	.L4157
	movq	%rax, %r15
	.p2align 4,,10
	.p2align 3
.L4156:
	leaq	2(%r15,%r15), %rdi
.L4157:
	movq	%r8, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_Znwm@PLT
	testq	%r12, %r12
	movq	(%rbx), %r11
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %r8
	movq	%rax, %r10
	je	.L4159
	cmpq	$1, %r12
	je	.L4188
	movq	%r12, %rdx
	addq	%rdx, %rdx
	je	.L4159
	movq	%r11, %rsi
	movq	%rax, %rdi
	movq	%r8, -80(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%r11, -56(%rbp)
	call	memmove@PLT
	movq	-80(%rbp), %r8
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %r11
	movq	%rax, %r10
	.p2align 4,,10
	.p2align 3
.L4159:
	testq	%rcx, %rcx
	je	.L4161
	testq	%r8, %r8
	je	.L4161
	leaq	(%r10,%r12,2), %rdi
	cmpq	$1, %r8
	je	.L4189
	movq	%r8, %rdx
	addq	%rdx, %rdx
	je	.L4161
	movq	%rcx, %rsi
	movq	%r8, -80(%rbp)
	movq	%r10, -64(%rbp)
	movq	%r11, -56(%rbp)
	call	memcpy@PLT
	movq	-80(%rbp), %r8
	movq	-64(%rbp), %r10
	movq	-56(%rbp), %r11
.L4161:
	testq	%r13, %r13
	jne	.L4190
.L4163:
	cmpq	%r11, %r14
	je	.L4165
	movq	%r11, %rdi
	movq	%r10, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r10
.L4165:
	movq	%r15, 16(%rbx)
	movq	%r10, (%rbx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4190:
	.cfi_restore_state
	movq	-72(%rbp), %rax
	addq	%r12, %r8
	leaq	(%r10,%r8,2), %rdi
	leaq	(%r11,%rax,2), %rsi
	cmpq	$1, %r13
	je	.L4191
	addq	%r13, %r13
	je	.L4163
	movq	%r13, %rdx
	movq	%r10, -64(%rbp)
	movq	%r11, -56(%rbp)
	call	memmove@PLT
	movq	-64(%rbp), %r10
	movq	-56(%rbp), %r11
	jmp	.L4163
	.p2align 4,,10
	.p2align 3
.L4166:
	movl	$7, %eax
	jmp	.L4154
	.p2align 4,,10
	.p2align 3
.L4191:
	movzwl	(%rsi), %eax
	movw	%ax, (%rdi)
	jmp	.L4163
	.p2align 4,,10
	.p2align 3
.L4188:
	movzwl	(%r11), %eax
	movw	%ax, (%r10)
	jmp	.L4159
	.p2align 4,,10
	.p2align 3
.L4189:
	movzwl	(%rcx), %eax
	movw	%ax, (%rdi)
	testq	%r13, %r13
	je	.L4163
	jmp	.L4190
.L4187:
	leaq	.LC8(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE13007:
	.size	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm, .-_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm
	.section	.rodata._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.str1.8,"aMS",@progbits,1
	.align 8
.LC9:
	.string	"basic_string::_M_construct null not valid"
	.section	.text._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag,"axG",@progbits,_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	.type	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag, @function
_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag:
.LFB13037:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	testq	%rsi, %rsi
	jne	.L4193
	testq	%rdx, %rdx
	jne	.L4209
.L4193:
	subq	%r13, %rbx
	movq	%rbx, %r14
	sarq	%r14
	cmpq	$15, %rbx
	ja	.L4194
	movq	(%r12), %rdi
.L4195:
	cmpq	$2, %rbx
	je	.L4210
	testq	%rbx, %rbx
	je	.L4198
	movq	%rbx, %rdx
	movq	%r13, %rsi
	call	memmove@PLT
	movq	(%r12), %rdi
.L4198:
	xorl	%eax, %eax
	movq	%r14, 8(%r12)
	movw	%ax, (%rdi,%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4210:
	.cfi_restore_state
	movzwl	0(%r13), %eax
	movw	%ax, (%rdi)
	movq	(%r12), %rdi
	jmp	.L4198
	.p2align 4,,10
	.p2align 3
.L4194:
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %r14
	ja	.L4211
	leaq	2(%rbx), %rdi
	call	_Znwm@PLT
	movq	%r14, 16(%r12)
	movq	%rax, (%r12)
	movq	%rax, %rdi
	jmp	.L4195
.L4209:
	leaq	.LC9(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L4211:
	leaq	.LC8(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE13037:
	.size	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag, .-_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	.section	.text._ZN12v8_inspector18V8RuntimeAgentImpl29reportExecutionContextCreatedEPNS_16InspectedContextE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector18V8RuntimeAgentImpl29reportExecutionContextCreatedEPNS_16InspectedContextE
	.type	_ZN12v8_inspector18V8RuntimeAgentImpl29reportExecutionContextCreatedEPNS_16InspectedContextE, @function
_ZN12v8_inspector18V8RuntimeAgentImpl29reportExecutionContextCreatedEPNS_16InspectedContextE:
.LFB8071:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 40(%rdi)
	jne	.L4246
.L4212:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4247
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4246:
	.cfi_restore_state
	movq	8(%rdi), %rax
	movq	%rsi, %r14
	movl	$1, %edx
	movq	%rdi, %rbx
	movq	%r14, %rdi
	leaq	-144(%rbp), %r12
	movl	20(%rax), %esi
	call	_ZN12v8_inspector16InspectedContext11setReportedEib@PLT
	movl	$104, %edi
	call	_Znwm@PLT
	movq	64(%r14), %rsi
	movq	%r12, %rdi
	movq	%rax, %r15
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime27ExecutionContextDescriptionE(%rip), %rax
	leaq	32(%r15), %rdx
	movq	%rax, (%r15)
	xorl	%eax, %eax
	leaq	16(%r15), %r8
	movq	%rdx, 16(%r15)
	leaq	72(%r15), %rdx
	leaq	56(%r15), %r13
	movq	%rdx, 56(%r15)
	xorl	%edx, %edx
	movw	%dx, 72(%r15)
	movl	16(%r14), %edx
	movw	%ax, 32(%r15)
	leaq	-128(%rbp), %rax
	movl	%edx, 8(%r15)
	movq	72(%r14), %rdx
	movq	%r8, -176(%rbp)
	leaq	(%rsi,%rdx,2), %rdx
	movq	%rax, -168(%rbp)
	movq	%rax, -144(%rbp)
	movq	$0, 24(%r15)
	movq	$0, 48(%r15)
	movq	$0, 64(%r15)
	movq	$0, 88(%r15)
	movq	$0, 96(%r15)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	96(%r14), %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	leaq	-96(%rbp), %r13
	leaq	-80(%rbp), %r12
	movq	%rdx, -112(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-112(%rbp), %rdx
	movq	24(%r14), %rsi
	movq	%r13, %rdi
	movq	%r12, -96(%rbp)
	movq	%rdx, 88(%r15)
	movq	32(%r14), %rdx
	leaq	(%rsi,%rdx,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	-176(%rbp), %r8
	movq	56(%r14), %rdx
	movq	%r13, %rsi
	movq	%r8, %rdi
	movq	%rdx, -64(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-64(%rbp), %rdx
	movq	-96(%rbp), %rdi
	movq	%rdx, 48(%r15)
	cmpq	%r12, %rdi
	je	.L4214
	call	_ZdlPv@PLT
.L4214:
	movq	-144(%rbp), %rdi
	cmpq	-168(%rbp), %rdi
	je	.L4215
	call	_ZdlPv@PLT
.L4215:
	movq	104(%r14), %rsi
	movq	112(%r14), %rdx
	movq	%r13, %rdi
	movq	%r12, -96(%rbp)
	leaq	(%rsi,%rdx,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	136(%r14), %rdx
	movq	-96(%rbp), %rdi
	movq	%rdx, -64(%rbp)
	movq	-88(%rbp), %rdx
	cmpq	%r12, %rdi
	je	.L4216
	movq	%rdx, -168(%rbp)
	call	_ZdlPv@PLT
	movq	-168(%rbp), %rdx
.L4216:
	leaq	-152(%rbp), %r8
	testq	%rdx, %rdx
	jne	.L4248
.L4217:
	leaq	24(%rbx), %rdi
	movq	%r8, %rsi
	movq	%r15, -152(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime8Frontend23executionContextCreatedESt10unique_ptrINS1_27ExecutionContextDescriptionESt14default_deleteIS4_EE@PLT
	movq	-152(%rbp), %r12
	testq	%r12, %r12
	je	.L4212
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime27ExecutionContextDescriptionD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4224
	movq	96(%r12), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime27ExecutionContextDescriptionE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L4225
	movq	(%rdi), %rax
	call	*24(%rax)
.L4225:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4226
	call	_ZdlPv@PLT
.L4226:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4227
	call	_ZdlPv@PLT
.L4227:
	movl	$104, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L4212
	.p2align 4,,10
	.p2align 3
.L4248:
	movq	104(%r14), %rsi
	movq	112(%r14), %rdx
	movq	%r13, %rdi
	movq	%r12, -96(%rbp)
	movq	%r8, -168(%rbp)
	leaq	(%rsi,%rdx,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	-168(%rbp), %r8
	movq	136(%r14), %rdx
	movq	%r13, %rsi
	movq	%r8, %rdi
	movq	%rdx, -64(%rbp)
	call	_ZN12v8_inspector8protocol10StringUtil9parseJSONERKNS_8String16E@PLT
	movq	-152(%rbp), %rdx
	movq	-168(%rbp), %r8
	movq	$0, -152(%rbp)
	testq	%rdx, %rdx
	je	.L4218
	cmpl	$6, 8(%rdx)
	movl	$0, %ecx
	cmovne	%rcx, %rdx
.L4218:
	movq	96(%r15), %rdi
	movq	%rdx, 96(%r15)
	testq	%rdi, %rdi
	je	.L4220
	movq	(%rdi), %rdx
	movq	%r8, -168(%rbp)
	call	*24(%rdx)
	movq	-152(%rbp), %rdi
	movq	-168(%rbp), %r8
	testq	%rdi, %rdi
	je	.L4220
	movq	(%rdi), %rdx
	call	*24(%rdx)
	movq	-168(%rbp), %r8
.L4220:
	movq	-96(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L4217
	movq	%r8, -168(%rbp)
	call	_ZdlPv@PLT
	movq	-168(%rbp), %r8
	jmp	.L4217
	.p2align 4,,10
	.p2align 3
.L4224:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L4212
.L4247:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8071:
	.size	_ZN12v8_inspector18V8RuntimeAgentImpl29reportExecutionContextCreatedEPNS_16InspectedContextE, .-_ZN12v8_inspector18V8RuntimeAgentImpl29reportExecutionContextCreatedEPNS_16InspectedContextE
	.section	.rodata._ZN12v8_inspector12_GLOBAL__N_119innerCallFunctionOnEPNS_22V8InspectorSessionImplERNS_14InjectedScript5ScopeEN2v85LocalINS6_5ValueEEERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeISt6vectorISt10unique_ptrINS_8protocol7Runtime12CallArgumentESt14default_deleteISL_EESaISO_EEEEbNS_8WrapModeEbbSC_SI_INSK_7Backend22CallFunctionOnCallbackESM_ISU_EE.str1.1,"aMS",@progbits,1
.LC10:
	.string	")"
.LC11:
	.string	"("
	.section	.rodata._ZN12v8_inspector12_GLOBAL__N_119innerCallFunctionOnEPNS_22V8InspectorSessionImplERNS_14InjectedScript5ScopeEN2v85LocalINS6_5ValueEEERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeISt6vectorISt10unique_ptrINS_8protocol7Runtime12CallArgumentESt14default_deleteISL_EESaISO_EEEEbNS_8WrapModeEbbSC_SI_INSK_7Backend22CallFunctionOnCallbackESM_ISU_EE.str1.8,"aMS",@progbits,1
	.align 8
.LC12:
	.string	"Given expression does not evaluate to a function"
	.section	.text._ZN12v8_inspector12_GLOBAL__N_119innerCallFunctionOnEPNS_22V8InspectorSessionImplERNS_14InjectedScript5ScopeEN2v85LocalINS6_5ValueEEERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeISt6vectorISt10unique_ptrINS_8protocol7Runtime12CallArgumentESt14default_deleteISL_EESaISO_EEEEbNS_8WrapModeEbbSC_SI_INSK_7Backend22CallFunctionOnCallbackESM_ISU_EE,"ax",@progbits
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_119innerCallFunctionOnEPNS_22V8InspectorSessionImplERNS_14InjectedScript5ScopeEN2v85LocalINS6_5ValueEEERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeISt6vectorISt10unique_ptrINS_8protocol7Runtime12CallArgumentESt14default_deleteISL_EESaISO_EEEEbNS_8WrapModeEbbSC_SI_INSK_7Backend22CallFunctionOnCallbackESM_ISU_EE, @function
_ZN12v8_inspector12_GLOBAL__N_119innerCallFunctionOnEPNS_22V8InspectorSessionImplERNS_14InjectedScript5ScopeEN2v85LocalINS6_5ValueEEERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeISt6vectorISt10unique_ptrINS_8protocol7Runtime12CallArgumentESt14default_deleteISL_EESaISO_EEEEbNS_8WrapModeEbbSC_SI_INSK_7Backend22CallFunctionOnCallbackESM_ISU_EE:
.LFB7916:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$392, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	24(%rbp), %eax
	movq	%rdi, -432(%rbp)
	movq	%rdx, -416(%rbp)
	movq	(%r8), %rbx
	movl	%eax, -396(%rbp)
	movl	32(%rbp), %eax
	movq	%rcx, -384(%rbp)
	movl	%eax, -420(%rbp)
	movq	40(%rbp), %rax
	movl	%r9d, -392(%rbp)
	movq	%rax, -408(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -376(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	movq	%rax, -368(%rbp)
	testq	%rbx, %rbx
	je	.L4304
	movabsq	$1152921504606846975, %rax
	movq	8(%rbx), %r12
	subq	(%rbx), %r12
	sarq	$3, %r12
	movslq	%r12d, %r13
	movl	%r12d, -400(%rbp)
	cmpq	%rax, %r13
	leaq	0(,%r13,8), %rdi
	movq	$-1, %rax
	cmova	%rax, %rdi
	call	_Znam@PLT
	movq	%rax, -344(%rbp)
	movq	%r13, %rax
	subq	$1, %rax
	js	.L4257
	leaq	-2(%r13), %rax
	movl	$1, %edx
	cmpq	$-1, %rax
	cmovl	%rdx, %r13
	cmpl	$1, %r12d
	je	.L4306
	cmpq	$-1, %rax
	jl	.L4306
	movq	%r13, %rdx
	movq	-344(%rbp), %rcx
	pxor	%xmm0, %xmm0
	shrq	%rdx
	salq	$4, %rdx
	movq	%rcx, %rax
	addq	%rcx, %rdx
	.p2align 4,,10
	.p2align 3
.L4256:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L4256
	movq	-344(%rbp), %rax
	movq	%r13, %rdx
	andq	$-2, %rdx
	leaq	(%rax,%rdx,8), %rax
	cmpq	%r13, %rdx
	je	.L4257
.L4254:
	movq	$0, (%rax)
.L4257:
	testl	%r12d, %r12d
	jle	.L4330
	leal	-1(%r12), %eax
	xorl	%r14d, %r14d
	leaq	-112(%rbp), %r12
	leaq	8(,%rax,8), %rax
	leaq	-336(%rbp), %r13
	movq	%rax, -360(%rbp)
	leaq	-88(%rbp), %rax
	movq	%rax, -352(%rbp)
	.p2align 4,,10
	.p2align 3
.L4264:
	movq	(%rbx), %rax
	movq	16(%r15), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	$0, -336(%rbp)
	movq	(%rax,%r14), %rdx
	call	_ZN12v8_inspector14InjectedScript19resolveCallArgumentEPNS_8protocol7Runtime12CallArgumentEPN2v85LocalINS5_5ValueEEE@PLT
	movl	-112(%rbp), %r8d
	testl	%r8d, %r8d
	jne	.L4332
	movq	-336(%rbp), %rax
	movq	-344(%rbp), %rcx
	movq	-104(%rbp), %rdi
	movq	%rax, (%rcx,%r14)
	cmpq	-352(%rbp), %rdi
	je	.L4262
	call	_ZdlPv@PLT
	addq	$8, %r14
	cmpq	%r14, -360(%rbp)
	jne	.L4264
.L4250:
	cmpb	$0, -392(%rbp)
	jne	.L4333
.L4265:
	cmpb	$0, -396(%rbp)
	jne	.L4334
.L4266:
	movq	%r15, %rdi
	leaq	-304(%rbp), %r13
	leaq	-288(%rbp), %rbx
	call	_ZN12v8_inspector14InjectedScript5Scope30allowCodeGenerationFromStringsEv@PLT
	leaq	-160(%rbp), %rax
	xorl	%edi, %edi
	leaq	.LC10(%rip), %rsi
	movq	%rax, -352(%rbp)
	movq	%rax, -176(%rbp)
	leaq	-224(%rbp), %rax
	movw	%di, -160(%rbp)
	movq	%rax, %rdi
	movq	%rax, -392(%rbp)
	movq	$0, -168(%rbp)
	movq	$0, -144(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	leaq	.LC11(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-112(%rbp), %rsi
	movq	-104(%rbp), %rax
	movq	%r13, %rdi
	movq	%rbx, -304(%rbp)
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	-384(%rbp), %rax
	movq	-304(%rbp), %rdx
	movq	-296(%rbp), %rsi
	movq	8(%rax), %r8
	movq	(%rax), %rcx
	cmpq	%rbx, %rdx
	movl	$7, %eax
	cmovne	-288(%rbp), %rax
	leaq	(%r8,%rsi), %r14
	cmpq	%rax, %r14
	ja	.L4268
	testq	%r8, %r8
	je	.L4269
	leaq	(%rdx,%rsi,2), %rdi
	cmpq	$1, %r8
	je	.L4335
	addq	%r8, %r8
	je	.L4269
	movq	%r8, %rdx
	movq	%rcx, %rsi
	call	memmove@PLT
	movq	-304(%rbp), %rdx
	jmp	.L4269
	.p2align 4,,10
	.p2align 3
.L4262:
	addq	$8, %r14
	cmpq	%r14, -360(%rbp)
	jne	.L4264
	jmp	.L4250
	.p2align 4,,10
	.p2align 3
.L4334:
	movq	%r15, %rdi
	call	_ZN12v8_inspector14InjectedScript5Scope18pretendUserGestureEv@PLT
	jmp	.L4266
	.p2align 4,,10
	.p2align 3
.L4333:
	movq	%r15, %rdi
	call	_ZN12v8_inspector14InjectedScript5Scope30ignoreExceptionsAndMuteConsoleEv@PLT
	cmpb	$0, -396(%rbp)
	je	.L4266
	jmp	.L4334
	.p2align 4,,10
	.p2align 3
.L4268:
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm
	movq	-304(%rbp), %rdx
.L4269:
	xorl	%esi, %esi
	movq	%r14, -296(%rbp)
	leaq	-272(%rbp), %rdi
	movw	%si, (%rdx,%r14,2)
	movq	%r13, %rsi
	call	_ZN12v8_inspector8String16C1EONSt7__cxx1112basic_stringItSt11char_traitsItESaItEEE@PLT
	movq	-304(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L4271
	call	_ZdlPv@PLT
.L4271:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	movq	%rax, -360(%rbp)
	cmpq	%rax, %rdi
	je	.L4272
	call	_ZdlPv@PLT
.L4272:
	movq	-272(%rbp), %rsi
	movq	-264(%rbp), %rax
	movq	%r13, %rdi
	movq	%rbx, -304(%rbp)
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	-304(%rbp), %rdx
	movl	$7, %eax
	movq	-216(%rbp), %r8
	movq	-296(%rbp), %rsi
	movq	-224(%rbp), %rcx
	cmpq	%rbx, %rdx
	cmovne	-288(%rbp), %rax
	leaq	(%r8,%rsi), %r14
	cmpq	%rax, %r14
	ja	.L4274
	testq	%r8, %r8
	je	.L4275
	leaq	(%rdx,%rsi,2), %rdi
	cmpq	$1, %r8
	je	.L4336
	addq	%r8, %r8
	je	.L4275
	movq	%r8, %rdx
	movq	%rcx, %rsi
	call	memmove@PLT
	movq	-304(%rbp), %rdx
	.p2align 4,,10
	.p2align 3
.L4275:
	xorl	%ecx, %ecx
	movq	%r14, -296(%rbp)
	movq	%r12, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rdx,%r14,2)
	call	_ZN12v8_inspector8String16C1EONSt7__cxx1112basic_stringItSt11char_traitsItESaItEEE@PLT
	movq	-304(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L4277
	call	_ZdlPv@PLT
.L4277:
	movq	-368(%rbp), %rdi
	movq	96(%r15), %rsi
	leaq	-176(%rbp), %rbx
	movq	%r12, %rdx
	movq	%rbx, %rcx
	call	_ZN12v8_inspector15V8InspectorImpl13compileScriptEN2v85LocalINS1_7ContextEEERKNS_8String16ES7_@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %r14
	cmpq	-360(%rbp), %rdi
	je	.L4278
	call	_ZdlPv@PLT
.L4278:
	movq	-272(%rbp), %rdi
	leaq	-256(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4279
	call	_ZdlPv@PLT
.L4279:
	movq	-224(%rbp), %rdi
	leaq	-208(%rbp), %r13
	cmpq	%r13, %rdi
	je	.L4280
	call	_ZdlPv@PLT
.L4280:
	movq	-176(%rbp), %rdi
	cmpq	-352(%rbp), %rdi
	je	.L4281
	call	_ZdlPv@PLT
.L4281:
	testq	%r14, %r14
	je	.L4282
	movq	-368(%rbp), %rax
	leaq	-336(%rbp), %r8
	xorl	%edx, %edx
	movq	%r8, %rdi
	movq	%r8, -352(%rbp)
	movq	8(%rax), %rsi
	call	_ZN2v815MicrotasksScopeC1EPNS_7IsolateENS0_4TypeE@PLT
	movq	96(%r15), %rsi
	movq	%r14, %rdi
	call	_ZN2v86Script3RunENS_5LocalINS_7ContextEEE@PLT
	movq	-352(%rbp), %r8
	movq	%rax, %r14
	movq	%r8, %rdi
	call	_ZN2v815MicrotasksScopeD1Ev@PLT
.L4282:
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector14InjectedScript5Scope10initializeEv@PLT
	movl	-176(%rbp), %edx
	testl	%edx, %edx
	jne	.L4331
	leaq	48(%r15), %rax
	movq	%rax, %rdi
	movq	%rax, -352(%rbp)
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L4337
	testq	%r14, %r14
	je	.L4302
	movq	%r14, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L4338
.L4302:
	movq	-376(%rbp), %rax
	movq	-392(%rbp), %r15
	leaq	.LC12(%rip), %rsi
	movq	(%rax), %r14
	movq	%r15, %rdi
	movq	(%r14), %rax
	movq	8(%rax), %rbx
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	*%rbx
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4288
	call	_ZdlPv@PLT
.L4288:
	movq	-224(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L4284
	call	_ZdlPv@PLT
	jmp	.L4284
	.p2align 4,,10
	.p2align 3
.L4331:
	movq	-376(%rbp), %rax
	movq	%rbx, %rsi
	movq	(%rax), %rdi
	movq	(%rdi), %rax
	call	*8(%rax)
.L4284:
	movq	-168(%rbp), %rdi
	leaq	-152(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4299
	call	_ZdlPv@PLT
.L4299:
	cmpq	$0, -344(%rbp)
	jne	.L4261
.L4249:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4339
	addq	$392, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4332:
	.cfi_restore_state
	movq	-376(%rbp), %rax
	movq	%r12, %rsi
	movq	(%rax), %rdi
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	-104(%rbp), %rdi
	cmpq	-352(%rbp), %rdi
	je	.L4261
	call	_ZdlPv@PLT
.L4261:
	movq	-344(%rbp), %rdi
	call	_ZdaPv@PLT
	jmp	.L4249
	.p2align 4,,10
	.p2align 3
.L4304:
	movq	$0, -344(%rbp)
	movl	$0, -400(%rbp)
.L4330:
	cmpb	$0, -392(%rbp)
	leaq	-112(%rbp), %r12
	je	.L4265
	jmp	.L4333
	.p2align 4,,10
	.p2align 3
.L4274:
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm
	movq	-304(%rbp), %rdx
	jmp	.L4275
	.p2align 4,,10
	.p2align 3
.L4338:
	movq	-368(%rbp), %rax
	leaq	-336(%rbp), %r13
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	8(%rax), %rsi
	call	_ZN2v815MicrotasksScopeC1EPNS_7IsolateENS0_4TypeE@PLT
	movq	-344(%rbp), %r8
	movq	96(%r15), %rsi
	movq	%r14, %rdi
	movl	-400(%rbp), %ecx
	movq	-416(%rbp), %rdx
	call	_ZN2v88Function4CallENS_5LocalINS_7ContextEEENS1_INS_5ValueEEEiPS5_@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v815MicrotasksScopeD1Ev@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector14InjectedScript5Scope10initializeEv@PLT
	movl	-112(%rbp), %eax
	leaq	-168(%rbp), %rdi
	leaq	-104(%rbp), %rsi
	movl	%eax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	movq	-72(%rbp), %rax
	movq	-104(%rbp), %rdi
	movq	%rax, -136(%rbp)
	movl	-64(%rbp), %eax
	movl	%eax, -128(%rbp)
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4290
	call	_ZdlPv@PLT
.L4290:
	movl	-176(%rbp), %eax
	testl	%eax, %eax
	jne	.L4331
	cmpb	$0, -420(%rbp)
	je	.L4293
	movq	-352(%rbp), %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L4293
	movq	-376(%rbp), %rax
	movq	16(%r15), %r12
	movl	$16, %edi
	movq	(%rax), %rbx
	movq	$0, (%rax)
	call	_Znwm@PLT
	movl	16(%rbp), %r8d
	movq	%r12, %rdi
	movq	%r13, %r9
	leaq	16+_ZTVN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend22CallFunctionOnCallbackEEE(%rip), %rcx
	movq	%rbx, 8(%rax)
	movq	%r14, %rdx
	movq	-432(%rbp), %rsi
	movq	%rcx, (%rax)
	movq	-408(%rbp), %rcx
	movq	%rax, -336(%rbp)
	call	_ZN12v8_inspector14InjectedScript18addPromiseCallbackEPNS_22V8InspectorSessionImplEN2v810MaybeLocalINS3_5ValueEEERKNS_8String16ENS_8WrapModeESt10unique_ptrINS_16EvaluateCallbackESt14default_deleteISC_EE@PLT
	movq	-336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4295
	movq	(%rdi), %rax
	call	*24(%rax)
.L4295:
	movq	-168(%rbp), %rdi
	leaq	-152(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4296
	call	_ZdlPv@PLT
.L4296:
	movq	-344(%rbp), %rax
	testq	%rax, %rax
	je	.L4249
	movq	%rax, %rdi
	call	_ZdaPv@PLT
	jmp	.L4249
	.p2align 4,,10
	.p2align 3
.L4336:
	movzwl	(%rcx), %eax
	movw	%ax, (%rdi)
	movq	-304(%rbp), %rdx
	jmp	.L4275
	.p2align 4,,10
	.p2align 3
.L4335:
	movzwl	(%rcx), %eax
	movw	%ax, (%rdi)
	movq	-304(%rbp), %rdx
	jmp	.L4269
.L4337:
	movq	-376(%rbp), %rax
	movq	16(%r15), %rdi
	movq	%r14, %rsi
	movl	$1, %r8d
	movq	-408(%rbp), %rcx
	movq	-352(%rbp), %rdx
	movq	(%rax), %r9
	call	_ZN12v8_inspector12_GLOBAL__N_123wrapEvaluateResultAsyncINS_8protocol7Runtime7Backend22CallFunctionOnCallbackEEEbPNS_14InjectedScriptEN2v810MaybeLocalINS8_5ValueEEERKNS8_8TryCatchERKNS_8String16ENS_8WrapModeEPT_
	jmp	.L4284
.L4306:
	movq	-344(%rbp), %rax
	jmp	.L4254
.L4293:
	movq	-376(%rbp), %rax
	movq	16(%r15), %rdi
	movq	%r14, %rsi
	movl	16(%rbp), %r8d
	movq	-408(%rbp), %rcx
	movq	(%rax), %r9
	movq	-352(%rbp), %rdx
	call	_ZN12v8_inspector12_GLOBAL__N_123wrapEvaluateResultAsyncINS_8protocol7Runtime7Backend22CallFunctionOnCallbackEEEbPNS_14InjectedScriptEN2v810MaybeLocalINS8_5ValueEEERKNS8_8TryCatchERKNS_8String16ENS_8WrapModeEPT_
	jmp	.L4284
.L4339:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7916:
	.size	_ZN12v8_inspector12_GLOBAL__N_119innerCallFunctionOnEPNS_22V8InspectorSessionImplERNS_14InjectedScript5ScopeEN2v85LocalINS6_5ValueEEERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeISt6vectorISt10unique_ptrINS_8protocol7Runtime12CallArgumentESt14default_deleteISL_EESaISO_EEEEbNS_8WrapModeEbbSC_SI_INSK_7Backend22CallFunctionOnCallbackESM_ISU_EE, .-_ZN12v8_inspector12_GLOBAL__N_119innerCallFunctionOnEPNS_22V8InspectorSessionImplERNS_14InjectedScript5ScopeEN2v85LocalINS6_5ValueEEERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeISt6vectorISt10unique_ptrINS_8protocol7Runtime12CallArgumentESt14default_deleteISL_EESaISO_EEEEbNS_8WrapModeEbbSC_SI_INSK_7Backend22CallFunctionOnCallbackESM_ISU_EE
	.section	.text._ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrIN2v86GlobalINS5_6ScriptEEESt14default_deleteIS8_EEESaISC_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSE_18_Mod_range_hashingENSE_20_Default_ranged_hashENSE_20_Prime_rehash_policyENSE_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmRS3_m,"axG",@progbits,_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrIN2v86GlobalINS5_6ScriptEEESt14default_deleteIS8_EEESaISC_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSE_18_Mod_range_hashingENSE_20_Default_ranged_hashENSE_20_Prime_rehash_policyENSE_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmRS3_m,comdat
	.align 2
	.p2align 4
	.weak	_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrIN2v86GlobalINS5_6ScriptEEESt14default_deleteIS8_EEESaISC_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSE_18_Mod_range_hashingENSE_20_Default_ranged_hashENSE_20_Prime_rehash_policyENSE_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmRS3_m
	.type	_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrIN2v86GlobalINS5_6ScriptEEESt14default_deleteIS8_EEESaISC_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSE_18_Mod_range_hashingENSE_20_Default_ranged_hashENSE_20_Prime_rehash_policyENSE_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmRS3_m, @function
_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrIN2v86GlobalINS5_6ScriptEEESt14default_deleteIS8_EEESaISC_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSE_18_Mod_range_hashingENSE_20_Default_ranged_hashENSE_20_Prime_rehash_policyENSE_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmRS3_m:
.LFB13865:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	(%rax,%rsi,8), %r10
	testq	%r10, %r10
	je	.L4357
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%r10), %rdx
	movl	$2147483648, %ebx
	movq	56(%rdx), %r8
	cmpq	%r8, %rcx
	je	.L4360
	.p2align 4,,10
	.p2align 3
.L4342:
	movq	(%rdx), %r9
	testq	%r9, %r9
	je	.L4347
	movq	56(%r9), %r8
	movq	%rdx, %r10
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	8(%rdi)
	cmpq	%rdx, %rsi
	jne	.L4347
	movq	%r9, %rdx
	cmpq	%r8, %rcx
	jne	.L4342
.L4360:
	movq	8(%r11), %r8
	movq	16(%rdx), %r15
	movq	8(%rdx), %r13
	movq	(%r11), %r14
	cmpq	%r15, %r8
	movq	%r15, %r9
	cmovbe	%r8, %r9
	testq	%r9, %r9
	je	.L4343
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L4344:
	movzwl	0(%r13,%rax,2), %r12d
	cmpw	%r12w, (%r14,%rax,2)
	jne	.L4342
	addq	$1, %rax
	cmpq	%rax, %r9
	jne	.L4344
.L4343:
	subq	%r15, %r8
	cmpq	%rbx, %r8
	jge	.L4342
	movabsq	$-2147483649, %rax
	cmpq	%rax, %r8
	jle	.L4342
	testl	%r8d, %r8d
	jne	.L4342
	popq	%rbx
	movq	%r10, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4347:
	.cfi_restore_state
	xorl	%r10d, %r10d
	popq	%rbx
	popq	%r12
	movq	%r10, %rax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4357:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	movq	%r10, %rax
	ret
	.cfi_endproc
.LFE13865:
	.size	_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrIN2v86GlobalINS5_6ScriptEEESt14default_deleteIS8_EEESaISC_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSE_18_Mod_range_hashingENSE_20_Default_ranged_hashENSE_20_Prime_rehash_policyENSE_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmRS3_m, .-_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrIN2v86GlobalINS5_6ScriptEEESt14default_deleteIS8_EEESaISC_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSE_18_Mod_range_hashingENSE_20_Default_ranged_hashENSE_20_Prime_rehash_policyENSE_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmRS3_m
	.section	.text._ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_St10unique_ptrIN2v86GlobalINS6_6ScriptEEESt14default_deleteIS9_EEESaISD_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_,"axG",@progbits,_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_St10unique_ptrIN2v86GlobalINS6_6ScriptEEESt14default_deleteIS9_EEESaISD_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_St10unique_ptrIN2v86GlobalINS6_6ScriptEEESt14default_deleteIS9_EEESaISD_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_
	.type	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_St10unique_ptrIN2v86GlobalINS6_6ScriptEEESt14default_deleteIS9_EEESaISD_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_, @function
_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_St10unique_ptrIN2v86GlobalINS6_6ScriptEEESt14default_deleteIS9_EEESaISD_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_:
.LFB10913:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	32(%rsi), %r12
	testq	%r12, %r12
	jne	.L4362
	movq	(%rsi), %rax
	movq	8(%rsi), %rdx
	leaq	(%rax,%rdx,2), %rcx
	cmpq	%rcx, %rax
	je	.L4365
	.p2align 4,,10
	.p2align 3
.L4364:
	movq	%r12, %rdx
	addq	$2, %rax
	salq	$5, %rdx
	subq	%r12, %rdx
	movq	%rdx, %r12
	movsbq	-2(%rax), %rdx
	addq	%rdx, %r12
	movq	%r12, 32(%r13)
	cmpq	%rax, %rcx
	jne	.L4364
	testq	%r12, %r12
	je	.L4365
.L4362:
	xorl	%edx, %edx
	movq	%r12, %rax
	movq	%r12, %rcx
	movq	%r14, %rdi
	divq	8(%r14)
	movq	%rdx, %r15
	movq	%r13, %rdx
	movq	%r15, %rsi
	call	_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrIN2v86GlobalINS5_6ScriptEEESt14default_deleteIS8_EEESaISC_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSE_18_Mod_range_hashingENSE_20_Default_ranged_hashENSE_20_Prime_rehash_policyENSE_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmRS3_m
	testq	%rax, %rax
	je	.L4366
	movq	(%rax), %rdx
	leaq	48(%rdx), %rax
	testq	%rdx, %rdx
	je	.L4366
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4365:
	.cfi_restore_state
	movq	$1, 32(%r13)
	movl	$1, %r12d
	jmp	.L4362
	.p2align 4,,10
	.p2align 3
.L4366:
	movl	$64, %edi
	call	_Znwm@PLT
	movq	0(%r13), %rsi
	movq	$0, (%rax)
	movq	%rax, %rbx
	leaq	8(%rax), %rdi
	leaq	24(%rax), %rax
	movq	%rax, 8(%rbx)
	movq	8(%r13), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	32(%r13), %rax
	movq	24(%r14), %rdx
	leaq	32(%r14), %rdi
	movq	8(%r14), %rsi
	movl	$1, %ecx
	movq	$0, 48(%rbx)
	movq	%rax, 40(%rbx)
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %r13
	testb	%al, %al
	jne	.L4368
	movq	(%r14), %r8
.L4369:
	salq	$3, %r15
	movq	%r12, 56(%rbx)
	leaq	(%r8,%r15), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L4378
	movq	(%rdx), %rdx
	movq	%rdx, (%rbx)
	movq	(%rax), %rax
	movq	%rbx, (%rax)
.L4379:
	addq	$1, 24(%r14)
	addq	$24, %rsp
	leaq	48(%rbx), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4368:
	.cfi_restore_state
	cmpq	$1, %rdx
	je	.L4401
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L4402
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%r14), %r10
	movq	%rax, %r8
.L4371:
	movq	16(%r14), %rsi
	movq	$0, 16(%r14)
	testq	%rsi, %rsi
	je	.L4373
	xorl	%edi, %edi
	leaq	16(%r14), %r9
	jmp	.L4374
	.p2align 4,,10
	.p2align 3
.L4375:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L4376:
	testq	%rsi, %rsi
	je	.L4373
.L4374:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	56(%rcx), %rax
	divq	%r13
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L4375
	movq	16(%r14), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%r14)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L4382
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L4374
	.p2align 4,,10
	.p2align 3
.L4373:
	movq	(%r14), %rdi
	cmpq	%r10, %rdi
	je	.L4377
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L4377:
	movq	%r12, %rax
	xorl	%edx, %edx
	movq	%r13, 8(%r14)
	divq	%r13
	movq	%r8, (%r14)
	movq	%rdx, %r15
	jmp	.L4369
	.p2align 4,,10
	.p2align 3
.L4378:
	movq	16(%r14), %rdx
	movq	%rbx, 16(%r14)
	movq	%rdx, (%rbx)
	testq	%rdx, %rdx
	je	.L4380
	movq	56(%rdx), %rax
	xorl	%edx, %edx
	divq	8(%r14)
	movq	%rbx, (%r8,%rdx,8)
	movq	(%r14), %rax
	addq	%r15, %rax
.L4380:
	leaq	16(%r14), %rdx
	movq	%rdx, (%rax)
	jmp	.L4379
	.p2align 4,,10
	.p2align 3
.L4382:
	movq	%rdx, %rdi
	jmp	.L4376
.L4401:
	leaq	48(%r14), %r8
	movq	$0, 48(%r14)
	movq	%r8, %r10
	jmp	.L4371
.L4402:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE10913:
	.size	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_St10unique_ptrIN2v86GlobalINS6_6ScriptEEESt14default_deleteIS9_EEESaISD_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_, .-_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_St10unique_ptrIN2v86GlobalINS6_6ScriptEEESt14default_deleteIS9_EEESaISD_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_
	.section	.rodata._ZN12v8_inspector18V8RuntimeAgentImpl8evaluateERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS1_EENS7_IbEES9_NS7_IiEES9_S9_S9_S9_S9_NS7_IdEESt10unique_ptrINS_8protocol7Runtime7Backend16EvaluateCallbackESt14default_deleteISG_EE.str1.8,"aMS",@progbits,1
	.align 8
.LC13:
	.string	"disabled-by-default-devtools.timeline"
	.section	.rodata._ZN12v8_inspector18V8RuntimeAgentImpl8evaluateERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS1_EENS7_IbEES9_NS7_IiEES9_S9_S9_S9_S9_NS7_IdEESt10unique_ptrINS_8protocol7Runtime7Backend16EvaluateCallbackESt14default_deleteISG_EE.str1.1,"aMS",@progbits,1
.LC14:
	.string	"EvaluateScript"
.LC16:
	.string	""
	.section	.text._ZN12v8_inspector18V8RuntimeAgentImpl8evaluateERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS1_EENS7_IbEES9_NS7_IiEES9_S9_S9_S9_S9_NS7_IdEESt10unique_ptrINS_8protocol7Runtime7Backend16EvaluateCallbackESt14default_deleteISG_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector18V8RuntimeAgentImpl8evaluateERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS1_EENS7_IbEES9_NS7_IiEES9_S9_S9_S9_S9_NS7_IdEESt10unique_ptrINS_8protocol7Runtime7Backend16EvaluateCallbackESt14default_deleteISG_EE
	.type	_ZN12v8_inspector18V8RuntimeAgentImpl8evaluateERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS1_EENS7_IbEES9_NS7_IiEES9_S9_S9_S9_S9_NS7_IdEESt10unique_ptrINS_8protocol7Runtime7Backend16EvaluateCallbackESt14default_deleteISG_EE, @function
_ZN12v8_inspector18V8RuntimeAgentImpl8evaluateERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS1_EENS7_IbEES9_NS7_IiEES9_S9_S9_S9_S9_NS7_IdEESt10unique_ptrINS_8protocol7Runtime7Backend16EvaluateCallbackESt14default_deleteISG_EE:
.LFB7959:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%r9, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$584, %rsp
	movq	16(%rbp), %rax
	movq	%rsi, -544(%rbp)
	movq	%rdx, -576(%rbp)
	movq	32(%rbp), %r15
	movq	%rax, -584(%rbp)
	movq	24(%rbp), %rax
	movq	%rcx, -528(%rbp)
	movq	%rax, -592(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -568(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -552(%rbp)
	movq	56(%rbp), %rax
	movq	%rax, -536(%rbp)
	movq	64(%rbp), %rax
	movq	%rax, -520(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	_ZZN12v8_inspector18V8RuntimeAgentImpl8evaluateERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS1_EENS7_IbEES9_NS7_IiEES9_S9_S9_S9_S9_NS7_IdEESt10unique_ptrINS_8protocol7Runtime7Backend16EvaluateCallbackESt14default_deleteISG_EEE28trace_event_unique_atomic239(%rip), %r13
	testq	%r13, %r13
	je	.L4488
.L4405:
	movq	$0, -448(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L4489
.L4407:
	movzbl	(%r12), %eax
	movq	32(%rbx), %rsi
	leaq	-176(%rbp), %r13
	leaq	-500(%rbp), %r8
	movl	$0, -500(%rbp)
	movq	%r13, %rdi
	movb	%al, -368(%rbp)
	movl	4(%r12), %eax
	leaq	-368(%rbp), %r12
	movq	%r12, %rcx
	movl	%eax, -364(%rbp)
	movq	8(%rbx), %rax
	movl	16(%rax), %edx
	call	_ZN12v8_inspector12_GLOBAL__N_113ensureContextEPNS_15V8InspectorImplEiN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIiEEPi
	movl	-176(%rbp), %r9d
	testl	%r9d, %r9d
	je	.L4411
	movq	-520(%rbp), %rax
	movq	%r13, %rsi
	movq	(%rax), %rdi
	movq	(%rdi), %rax
	call	*8(%rax)
.L4487:
	movq	-168(%rbp), %rdi
	leaq	-152(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4445
	call	_ZdlPv@PLT
.L4445:
	leaq	-448(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4490
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4411:
	.cfi_restore_state
	movl	-500(%rbp), %edx
	movq	8(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector14InjectedScript12ContextScopeC1EPNS_22V8InspectorSessionImplEi@PLT
	leaq	-112(%rbp), %rax
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -560(%rbp)
	call	_ZN12v8_inspector14InjectedScript5Scope10initializeEv@PLT
	movl	-112(%rbp), %eax
	leaq	-104(%rbp), %rsi
	movq	%rsi, -608(%rbp)
	movl	%eax, -176(%rbp)
	leaq	-168(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -616(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	movq	-72(%rbp), %rax
	movq	-104(%rbp), %rdi
	movq	%rax, -136(%rbp)
	movl	-64(%rbp), %eax
	movl	%eax, -128(%rbp)
	leaq	-88(%rbp), %rax
	movq	%rax, -600(%rbp)
	cmpq	%rax, %rdi
	je	.L4413
	call	_ZdlPv@PLT
.L4413:
	movl	-176(%rbp), %r8d
	testl	%r8d, %r8d
	jne	.L4485
	cmpb	$0, (%r14)
	je	.L4416
	cmpb	$0, 1(%r14)
	je	.L4416
	movq	%r12, %rdi
	call	_ZN12v8_inspector14InjectedScript5Scope30ignoreExceptionsAndMuteConsoleEv@PLT
.L4416:
	cmpb	$0, (%r15)
	je	.L4417
	cmpb	$0, 1(%r15)
	je	.L4417
	movq	%r12, %rdi
	call	_ZN12v8_inspector14InjectedScript5Scope18pretendUserGestureEv@PLT
.L4417:
	movq	-528(%rbp), %rax
	cmpb	$0, (%rax)
	je	.L4418
	cmpb	$0, 1(%rax)
	je	.L4418
	movq	%r12, %rdi
	call	_ZN12v8_inspector14InjectedScript5Scope21installCommandLineAPIEv@PLT
.L4418:
	movq	%r12, %rdi
	leaq	-416(%rbp), %r15
	call	_ZN12v8_inspector14InjectedScript5Scope30allowCodeGenerationFromStringsEv@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector15V8InspectorImpl13EvaluateScopeC1ERKNS_14InjectedScript5ScopeE@PLT
	movq	-536(%rbp), %rax
	cmpb	$0, (%rax)
	je	.L4419
	movq	-560(%rbp), %rdi
	movq	%r15, %rsi
	movsd	8(%rax), %xmm0
	divsd	.LC15(%rip), %xmm0
	call	_ZN12v8_inspector15V8InspectorImpl13EvaluateScope10setTimeoutEd@PLT
	movl	-112(%rbp), %eax
	movq	-616(%rbp), %rdi
	movq	-608(%rbp), %rsi
	movl	%eax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	movq	-72(%rbp), %rax
	movq	-104(%rbp), %rdi
	movq	%rax, -136(%rbp)
	movl	-64(%rbp), %eax
	movl	%eax, -128(%rbp)
	cmpq	-600(%rbp), %rdi
	je	.L4420
	call	_ZdlPv@PLT
.L4420:
	movl	-176(%rbp), %edi
	testl	%edi, %edi
	je	.L4419
	movq	-520(%rbp), %rax
	movq	%r13, %rsi
	movq	(%rax), %rdi
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	%r15, %rdi
	call	_ZN12v8_inspector15V8InspectorImpl13EvaluateScopeD1Ev@PLT
	jmp	.L4443
	.p2align 4,,10
	.p2align 3
.L4419:
	movq	32(%rbx), %rax
	xorl	%edx, %edx
	xorl	%r14d, %r14d
	movq	8(%rax), %rsi
	leaq	-480(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -528(%rbp)
	call	_ZN2v815MicrotasksScopeC1EPNS_7IsolateENS0_4TypeE@PLT
	movq	-552(%rbp), %rax
	cmpb	$0, (%rax)
	je	.L4421
	movzbl	1(%rax), %r14d
.L4421:
	movq	32(%rbx), %rax
	movq	-544(%rbp), %rsi
	movq	8(%rax), %rdi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movl	%r14d, %edx
	movq	%rax, %rsi
	movq	32(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v85debug14EvaluateGlobalEPNS_7IsolateENS_5LocalINS_6StringEEEb@PLT
	movq	-528(%rbp), %rdi
	movq	%rax, %r14
	call	_ZN2v815MicrotasksScopeD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN12v8_inspector15V8InspectorImpl13EvaluateScopeD1Ev@PLT
	movq	-560(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN12v8_inspector14InjectedScript5Scope10initializeEv@PLT
	movl	-112(%rbp), %eax
	movq	-616(%rbp), %rdi
	movq	-608(%rbp), %rsi
	movl	%eax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	movq	-72(%rbp), %rax
	movq	-104(%rbp), %rdi
	movq	%rax, -136(%rbp)
	movl	-64(%rbp), %eax
	movl	%eax, -128(%rbp)
	cmpq	-600(%rbp), %rdi
	je	.L4422
	call	_ZdlPv@PLT
.L4422:
	movl	-176(%rbp), %esi
	testl	%esi, %esi
	jne	.L4485
	movq	-592(%rbp), %rax
	movl	$1, %r13d
	cmpb	$0, (%rax)
	je	.L4424
	xorl	%r13d, %r13d
	cmpb	$0, 1(%rax)
	setne	%r13b
	addl	$1, %r13d
.L4424:
	movq	-584(%rbp), %rax
	cmpb	$0, (%rax)
	je	.L4425
	cmpb	$0, 1(%rax)
	movl	$0, %eax
	cmovne	%eax, %r13d
.L4425:
	movq	-568(%rbp), %rax
	cmpb	$0, (%rax)
	je	.L4483
	cmpb	$0, 1(%rax)
	je	.L4483
	leaq	-320(%rbp), %rcx
	movq	%rcx, %rdi
	movq	%rcx, -536(%rbp)
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	movq	-536(%rbp), %rcx
	testb	%al, %al
	jne	.L4429
	movq	-520(%rbp), %rax
	movq	-352(%rbp), %r10
	movl	$16, %edi
	movq	(%rax), %rdx
	movq	$0, (%rax)
	movq	%r10, -528(%rbp)
	movq	%rdx, -520(%rbp)
	call	_Znwm@PLT
	movq	-520(%rbp), %rdx
	leaq	16+_ZTVN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend16EvaluateCallbackEEE(%rip), %rcx
	movq	-560(%rbp), %rdi
	movq	%rcx, (%rax)
	leaq	.LC16(%rip), %rsi
	movq	%rdx, 8(%rax)
	movq	%rax, -416(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	8(%rbx), %rsi
	movq	%r15, %r9
	movl	%r13d, %r8d
	movq	-576(%rbp), %rax
	movq	-528(%rbp), %r10
	movq	%r14, %rdx
	leaq	8(%rax), %rcx
	cmpb	$0, (%rax)
	cmove	-560(%rbp), %rcx
	movq	%r10, %rdi
	call	_ZN12v8_inspector14InjectedScript18addPromiseCallbackEPNS_22V8InspectorSessionImplEN2v810MaybeLocalINS3_5ValueEEERKNS_8String16ENS_8WrapModeESt10unique_ptrINS_16EvaluateCallbackESt14default_deleteISC_EE@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4442
	call	_ZdlPv@PLT
.L4442:
	movq	-416(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4443
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L4443
	.p2align 4,,10
	.p2align 3
.L4485:
	movq	-520(%rbp), %rax
	movq	%r13, %rsi
	movq	(%rax), %rdi
	movq	(%rdi), %rax
	call	*8(%rax)
.L4443:
	movq	%r12, %rdi
	call	_ZN12v8_inspector14InjectedScript12ContextScopeD1Ev@PLT
	jmp	.L4487
	.p2align 4,,10
	.p2align 3
.L4489:
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	$0, -560(%rbp)
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %r11
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rax
	cmpq	%rax, %r11
	jne	.L4491
.L4408:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4409
	movq	(%rdi), %rax
	call	*8(%rax)
.L4409:
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4410
	movq	(%rdi), %rax
	call	*8(%rax)
.L4410:
	leaq	.LC14(%rip), %rax
	movq	%r13, -440(%rbp)
	movq	%rax, -432(%rbp)
	movq	-560(%rbp), %rax
	movq	%rax, -424(%rbp)
	leaq	-440(%rbp), %rax
	movq	%rax, -448(%rbp)
	jmp	.L4407
	.p2align 4,,10
	.p2align 3
.L4488:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4492
.L4406:
	movq	%r13, _ZZN12v8_inspector18V8RuntimeAgentImpl8evaluateERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS1_EENS7_IbEES9_NS7_IiEES9_S9_S9_S9_S9_NS7_IdEESt10unique_ptrINS_8protocol7Runtime7Backend16EvaluateCallbackESt14default_deleteISG_EEE28trace_event_unique_atomic239(%rip)
	jmp	.L4405
	.p2align 4,,10
	.p2align 3
.L4483:
	leaq	-320(%rbp), %rcx
.L4429:
	movq	-520(%rbp), %rax
	leaq	.LC16(%rip), %rsi
	movq	%rcx, -536(%rbp)
	movq	(%rax), %rbx
	leaq	-224(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -520(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-576(%rbp), %rax
	movq	%r14, %rdx
	movq	-536(%rbp), %rcx
	movq	-352(%rbp), %rsi
	movl	%r13d, %r9d
	movq	-560(%rbp), %rdi
	movq	$0, -496(%rbp)
	cmpb	$0, (%rax)
	leaq	8(%rax), %r8
	movq	-520(%rbp), %rax
	movq	$0, -488(%rbp)
	cmove	%rax, %r8
	leaq	-488(%rbp), %rax
	pushq	%rax
	leaq	-496(%rbp), %rax
	pushq	%rax
	call	_ZN12v8_inspector14InjectedScript18wrapEvaluateResultEN2v810MaybeLocalINS1_5ValueEEERKNS1_8TryCatchERKNS_8String16ENS_8WrapModeEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISF_EEPN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeINSE_16ExceptionDetailsEEE@PLT
	movl	-112(%rbp), %ecx
	popq	%rax
	popq	%rdx
	testl	%ecx, %ecx
	je	.L4493
	movq	(%rbx), %rax
	movq	-560(%rbp), %rsi
	movq	%rbx, %rdi
	call	*8(%rax)
.L4435:
	movq	-104(%rbp), %rdi
	cmpq	-600(%rbp), %rdi
	je	.L4436
	call	_ZdlPv@PLT
.L4436:
	movq	-488(%rbp), %r13
	testq	%r13, %r13
	je	.L4437
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4438
	call	_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD1Ev
	movl	$184, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4437:
	movq	-496(%rbp), %r13
	testq	%r13, %r13
	je	.L4439
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4440
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4439:
	movq	-224(%rbp), %rdi
	leaq	-208(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4443
	call	_ZdlPv@PLT
	jmp	.L4443
	.p2align 4,,10
	.p2align 3
.L4492:
	leaq	.LC13(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L4406
	.p2align 4,,10
	.p2align 3
.L4491:
	subq	$8, %rsp
	leaq	-112(%rbp), %rax
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC14(%rip), %rcx
	movq	%r13, %rdx
	movl	$88, %esi
	pushq	%rax
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%r11
	movq	%rax, -560(%rbp)
	addq	$64, %rsp
	jmp	.L4408
	.p2align 4,,10
	.p2align 3
.L4493:
	movq	-488(%rbp), %rdx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	-528(%rbp), %rsi
	movq	(%rax), %rax
	movq	%rdx, -416(%rbp)
	movq	-496(%rbp), %rdx
	movq	$0, -488(%rbp)
	movq	$0, -496(%rbp)
	movq	%rdx, -480(%rbp)
	movq	%r15, %rdx
	call	*%rax
	movq	-480(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4433
	movq	(%rdi), %rax
	call	*24(%rax)
.L4433:
	movq	-416(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4435
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L4435
	.p2align 4,,10
	.p2align 3
.L4440:
	call	*%rax
	jmp	.L4439
	.p2align 4,,10
	.p2align 3
.L4438:
	call	*%rax
	jmp	.L4437
.L4490:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7959:
	.size	_ZN12v8_inspector18V8RuntimeAgentImpl8evaluateERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS1_EENS7_IbEES9_NS7_IiEES9_S9_S9_S9_S9_NS7_IdEESt10unique_ptrINS_8protocol7Runtime7Backend16EvaluateCallbackESt14default_deleteISG_EE, .-_ZN12v8_inspector18V8RuntimeAgentImpl8evaluateERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS1_EENS7_IbEES9_NS7_IiEES9_S9_S9_S9_S9_NS7_IdEESt10unique_ptrINS_8protocol7Runtime7Backend16EvaluateCallbackESt14default_deleteISG_EE
	.section	.rodata._ZN12v8_inspector18V8RuntimeAgentImpl13compileScriptERKNS_8String16ES3_bN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIiEEPNS7_IS1_EEPNS6_8PtrMaybeINS_8protocol7Runtime16ExceptionDetailsEEE.str1.1,"aMS",@progbits,1
.LC17:
	.string	"Script compilation failed"
	.section	.text._ZN12v8_inspector18V8RuntimeAgentImpl13compileScriptERKNS_8String16ES3_bN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIiEEPNS7_IS1_EEPNS6_8PtrMaybeINS_8protocol7Runtime16ExceptionDetailsEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector18V8RuntimeAgentImpl13compileScriptERKNS_8String16ES3_bN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIiEEPNS7_IS1_EEPNS6_8PtrMaybeINS_8protocol7Runtime16ExceptionDetailsEEE
	.type	_ZN12v8_inspector18V8RuntimeAgentImpl13compileScriptERKNS_8String16ES3_bN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIiEEPNS7_IS1_EEPNS6_8PtrMaybeINS_8protocol7Runtime16ExceptionDetailsEEE, @function
_ZN12v8_inspector18V8RuntimeAgentImpl13compileScriptERKNS_8String16ES3_bN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIiEEPNS7_IS1_EEPNS6_8PtrMaybeINS_8protocol7Runtime16ExceptionDetailsEEE:
.LFB8034:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$408, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %rax
	movl	%r8d, -392(%rbp)
	movq	%rax, -400(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -408(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 40(%rsi)
	jne	.L4495
	leaq	-112(%rbp), %r12
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r15, %rdi
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4494
.L4538:
	call	_ZdlPv@PLT
.L4494:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4539
	addq	$408, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4495:
	.cfi_restore_state
	movzbl	(%r9), %eax
	movq	%rsi, %rbx
	movq	%rdx, %r12
	movq	%rcx, %r13
	leaq	-368(%rbp), %r14
	leaq	-176(%rbp), %rdi
	movl	$0, -372(%rbp)
	movb	%al, -368(%rbp)
	movl	4(%r9), %eax
	leaq	-372(%rbp), %r8
	movq	%r14, %rcx
	movl	%eax, -364(%rbp)
	movq	8(%rsi), %rax
	movq	32(%rsi), %rsi
	movl	16(%rax), %edx
	call	_ZN12v8_inspector12_GLOBAL__N_113ensureContextEPNS_15V8InspectorImplEiN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIiEEPi
	movl	-176(%rbp), %eax
	testl	%eax, %eax
	je	.L4498
	movl	%eax, (%r15)
	leaq	24(%r15), %rax
	leaq	-152(%rbp), %rdx
	movq	%rax, 8(%r15)
	movq	-168(%rbp), %rax
	cmpq	%rdx, %rax
	je	.L4540
	movq	%rax, 8(%r15)
	movq	-152(%rbp), %rax
	movq	%rax, 24(%r15)
.L4500:
	movq	-160(%rbp), %rax
	movq	%rax, 16(%r15)
	movq	-136(%rbp), %rax
	movq	%rax, 40(%r15)
	movl	-128(%rbp), %eax
	movl	%eax, 48(%r15)
	jmp	.L4494
	.p2align 4,,10
	.p2align 3
.L4498:
	movl	-372(%rbp), %edx
	movq	8(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector14InjectedScript12ContextScopeC1EPNS_22V8InspectorSessionImplEi@PLT
	leaq	-112(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -416(%rbp)
	call	_ZN12v8_inspector14InjectedScript5Scope10initializeEv@PLT
	movl	-112(%rbp), %eax
	leaq	-104(%rbp), %rsi
	movq	%rsi, -424(%rbp)
	movl	%eax, -176(%rbp)
	leaq	-168(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -440(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	movq	-72(%rbp), %rax
	movq	-104(%rbp), %rdi
	movq	%rax, -136(%rbp)
	movl	-64(%rbp), %eax
	movl	%eax, -128(%rbp)
	leaq	-88(%rbp), %rax
	movq	%rax, -432(%rbp)
	cmpq	%rax, %rdi
	je	.L4501
	call	_ZdlPv@PLT
.L4501:
	movl	-176(%rbp), %eax
	testl	%eax, %eax
	je	.L4502
	movl	%eax, (%r15)
	leaq	24(%r15), %rax
	leaq	-152(%rbp), %rbx
	movq	%rax, 8(%r15)
	movq	-168(%rbp), %rax
	cmpq	%rbx, %rax
	je	.L4541
.L4511:
	movq	%rax, 8(%r15)
	movq	-152(%rbp), %rax
	movq	%rax, 24(%r15)
.L4512:
	movq	-160(%rbp), %rax
	movq	%rbx, -168(%rbp)
	movq	$0, -160(%rbp)
	movq	%rax, 16(%r15)
	xorl	%eax, %eax
	movw	%ax, -152(%rbp)
	movq	-136(%rbp), %rax
	movq	%rax, 40(%r15)
	movl	-128(%rbp), %eax
	movl	%eax, 48(%r15)
.L4505:
	movq	%r14, %rdi
	call	_ZN12v8_inspector14InjectedScript12ContextScopeD1Ev@PLT
	movq	-168(%rbp), %rdi
	cmpq	%rbx, %rdi
	jne	.L4538
	jmp	.L4494
	.p2align 4,,10
	.p2align 3
.L4540:
	movdqu	-152(%rbp), %xmm0
	movups	%xmm0, 24(%r15)
	jmp	.L4500
	.p2align 4,,10
	.p2align 3
.L4502:
	cmpb	$0, -392(%rbp)
	movq	32(%rbx), %rdi
	je	.L4542
	movq	-272(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r13, %rcx
	call	_ZN12v8_inspector15V8InspectorImpl13compileScriptEN2v85LocalINS1_7ContextEEERKNS_8String16ES7_@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L4518
	movq	%r12, %rdi
	leaq	-224(%rbp), %r13
	call	_ZN2v86Script16GetUnboundScriptEv@PLT
	movq	%rax, %rdi
	call	_ZN2v813UnboundScript5GetIdEv@PLT
	movq	%r13, %rdi
	movl	%eax, %esi
	call	_ZN12v8_inspector8String1611fromIntegerEi@PLT
	movq	32(%rbx), %rax
	movl	$8, %edi
	movq	8(%rax), %r8
	movq	%r8, -408(%rbp)
	call	_Znwm@PLT
	movq	-408(%rbp), %r8
	movq	%r12, %rsi
	movq	%rax, -392(%rbp)
	movq	%r8, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	-392(%rbp), %rcx
	leaq	48(%rbx), %rdi
	movq	%r13, %rsi
	movq	%rax, (%rcx)
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_St10unique_ptrIN2v86GlobalINS6_6ScriptEEESt14default_deleteIS9_EEESaISD_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_
	movq	-392(%rbp), %rcx
	movq	(%rax), %r12
	movq	%rcx, (%rax)
	testq	%r12, %r12
	je	.L4514
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L4515
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L4515:
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4514:
	movq	-224(%rbp), %rsi
	movq	-216(%rbp), %rax
	leaq	-96(%rbp), %rbx
	movq	-416(%rbp), %r12
	movq	%rbx, -112(%rbp)
	leaq	(%rsi,%rax,2), %rdx
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	-400(%rbp), %r13
	movq	-192(%rbp), %rax
	movq	%r12, %rsi
	leaq	8(%r13), %rdi
	movq	%rax, -80(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-80(%rbp), %rax
	movq	-112(%rbp), %rdi
	movb	$1, 0(%r13)
	movq	%rax, 40(%r13)
	cmpq	%rbx, %rdi
	je	.L4516
	call	_ZdlPv@PLT
.L4516:
	movq	%r15, %rdi
	leaq	-152(%rbp), %rbx
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	movq	-224(%rbp), %rdi
	leaq	-208(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4505
.L4537:
	call	_ZdlPv@PLT
	jmp	.L4505
	.p2align 4,,10
	.p2align 3
.L4542:
	movq	24(%rdi), %rdi
	call	_ZN12v8_inspector10V8Debugger22muteScriptParsedEventsEv@PLT
	movq	32(%rbx), %rdi
	movq	%r12, %rdx
	movq	%r13, %rcx
	movq	-272(%rbp), %rsi
	call	_ZN12v8_inspector15V8InspectorImpl13compileScriptEN2v85LocalINS1_7ContextEEERKNS_8String16ES7_@PLT
	movq	%rax, %r12
	movq	32(%rbx), %rax
	movq	24(%rax), %rdi
	call	_ZN12v8_inspector10V8Debugger24unmuteScriptParsedEventsEv@PLT
	testq	%r12, %r12
	jne	.L4507
	.p2align 4,,10
	.p2align 3
.L4518:
	leaq	-320(%rbp), %r12
	movq	%r12, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	je	.L4508
	movq	-408(%rbp), %r8
	movq	-352(%rbp), %rsi
	xorl	%edx, %edx
	leaq	-224(%rbp), %rcx
	movq	-416(%rbp), %rdi
	movw	%dx, -208(%rbp)
	movq	%r12, %rdx
	leaq	-208(%rbp), %rbx
	movq	%rbx, -224(%rbp)
	movq	$0, -216(%rbp)
	movq	$0, -192(%rbp)
	call	_ZN12v8_inspector14InjectedScript22createExceptionDetailsERKN2v88TryCatchERKNS_8String16EPN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeINS_8protocol7Runtime16ExceptionDetailsEEE@PLT
	movl	-112(%rbp), %eax
	movq	-440(%rbp), %rdi
	movq	-424(%rbp), %rsi
	movl	%eax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	movq	-72(%rbp), %rax
	movq	-104(%rbp), %rdi
	movq	%rax, -136(%rbp)
	movl	-64(%rbp), %eax
	movl	%eax, -128(%rbp)
	cmpq	-432(%rbp), %rdi
	je	.L4509
	call	_ZdlPv@PLT
.L4509:
	movq	-224(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L4510
	call	_ZdlPv@PLT
.L4510:
	movl	-176(%rbp), %eax
	testl	%eax, %eax
	je	.L4507
	movl	%eax, (%r15)
	leaq	24(%r15), %rax
	leaq	-152(%rbp), %rbx
	movq	%rax, 8(%r15)
	movq	-168(%rbp), %rax
	cmpq	%rbx, %rax
	jne	.L4511
	movdqu	-152(%rbp), %xmm2
	movups	%xmm2, 24(%r15)
	jmp	.L4512
	.p2align 4,,10
	.p2align 3
.L4507:
	movq	%r15, %rdi
	leaq	-152(%rbp), %rbx
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	jmp	.L4505
	.p2align 4,,10
	.p2align 3
.L4541:
	movdqu	-152(%rbp), %xmm1
	movups	%xmm1, 24(%r15)
	jmp	.L4512
	.p2align 4,,10
	.p2align 3
.L4508:
	movq	-416(%rbp), %rbx
	leaq	.LC17(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	leaq	-152(%rbp), %rbx
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L4537
	jmp	.L4505
.L4539:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8034:
	.size	_ZN12v8_inspector18V8RuntimeAgentImpl13compileScriptERKNS_8String16ES3_bN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIiEEPNS7_IS1_EEPNS6_8PtrMaybeINS_8protocol7Runtime16ExceptionDetailsEEE, .-_ZN12v8_inspector18V8RuntimeAgentImpl13compileScriptERKNS_8String16ES3_bN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIiEEPNS7_IS1_EEPNS6_8PtrMaybeINS_8protocol7Runtime16ExceptionDetailsEEE
	.section	.rodata._ZN12v8_inspector18V8RuntimeAgentImpl9runScriptERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIiEENS7_IS1_EENS7_IbEESA_SA_SA_SA_St10unique_ptrINS_8protocol7Runtime7Backend17RunScriptCallbackESt14default_deleteISF_EE.str1.1,"aMS",@progbits,1
.LC18:
	.string	"No script with given id"
.LC19:
	.string	"Script execution failed"
	.section	.text._ZN12v8_inspector18V8RuntimeAgentImpl9runScriptERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIiEENS7_IS1_EENS7_IbEESA_SA_SA_SA_St10unique_ptrINS_8protocol7Runtime7Backend17RunScriptCallbackESt14default_deleteISF_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector18V8RuntimeAgentImpl9runScriptERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIiEENS7_IS1_EENS7_IbEESA_SA_SA_SA_St10unique_ptrINS_8protocol7Runtime7Backend17RunScriptCallbackESt14default_deleteISF_EE
	.type	_ZN12v8_inspector18V8RuntimeAgentImpl9runScriptERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIiEENS7_IS1_EENS7_IbEESA_SA_SA_SA_St10unique_ptrINS_8protocol7Runtime7Backend17RunScriptCallbackESt14default_deleteISF_EE, @function
_ZN12v8_inspector18V8RuntimeAgentImpl9runScriptERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIiEENS7_IS1_EENS7_IbEESA_SA_SA_SA_St10unique_ptrINS_8protocol7Runtime7Backend17RunScriptCallbackESt14default_deleteISF_EE:
.LFB8038:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$488, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %rax
	movq	%rcx, -464(%rbp)
	movq	%r8, -440(%rbp)
	movq	40(%rbp), %r12
	movq	%rax, -472(%rbp)
	movq	24(%rbp), %rax
	movq	%r9, -448(%rbp)
	movq	%rax, -480(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -456(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 40(%rdi)
	je	.L4657
	movq	32(%rsi), %rcx
	movq	%rdi, %rbx
	movq	%rsi, %r11
	movq	%rdx, %r13
	leaq	48(%rdi), %r15
	testq	%rcx, %rcx
	jne	.L4548
	movq	(%rsi), %rax
	movq	8(%rsi), %rdx
	leaq	(%rax,%rdx,2), %rsi
	cmpq	%rsi, %rax
	je	.L4551
	.p2align 4,,10
	.p2align 3
.L4550:
	movq	%rcx, %rdx
	addq	$2, %rax
	salq	$5, %rdx
	subq	%rcx, %rdx
	movsbq	-2(%rax), %rcx
	addq	%rdx, %rcx
	movq	%rcx, 32(%r11)
	cmpq	%rax, %rsi
	jne	.L4550
	testq	%rcx, %rcx
	je	.L4551
.L4548:
	xorl	%edx, %edx
	movq	%rcx, %rax
	movq	%r15, %rdi
	divq	56(%rbx)
	movq	%rdx, %rsi
	movq	%r11, %rdx
	call	_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrIN2v86GlobalINS5_6ScriptEEESt14default_deleteIS8_EEESaISC_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSE_18_Mod_range_hashingENSE_20_Default_ranged_hashENSE_20_Prime_rehash_policyENSE_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmRS3_m
	testq	%rax, %rax
	je	.L4552
	movq	(%rax), %r14
	testq	%r14, %r14
	je	.L4552
	movzbl	0(%r13), %eax
	movq	32(%rbx), %rsi
	leaq	-176(%rbp), %rdi
	leaq	-428(%rbp), %r8
	movl	$0, -428(%rbp)
	movb	%al, -368(%rbp)
	movl	4(%r13), %eax
	leaq	-368(%rbp), %r13
	movq	%r13, %rcx
	movq	%rdi, -488(%rbp)
	movl	%eax, -364(%rbp)
	movq	8(%rbx), %rax
	movl	16(%rax), %edx
	call	_ZN12v8_inspector12_GLOBAL__N_113ensureContextEPNS_15V8InspectorImplEiN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIiEEPi
	movl	-176(%rbp), %r8d
	testl	%r8d, %r8d
	je	.L4658
	movq	(%r12), %rdi
	movq	-488(%rbp), %rsi
	movq	(%rdi), %rax
	call	*8(%rax)
.L4557:
	movq	-168(%rbp), %rdi
	leaq	-152(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4543
.L4649:
	call	_ZdlPv@PLT
.L4543:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4659
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4552:
	.cfi_restore_state
	movq	(%r12), %r12
	leaq	-176(%rbp), %r14
	leaq	.LC18(%rip), %rsi
	movq	(%r12), %rax
	movq	8(%rax), %rbx
.L4655:
	movq	%r14, %rdi
	leaq	-112(%rbp), %r13
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	*%rbx
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4555
	call	_ZdlPv@PLT
.L4555:
	movq	-176(%rbp), %rdi
	leaq	-160(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L4649
	jmp	.L4543
	.p2align 4,,10
	.p2align 3
.L4657:
	movq	(%r12), %r12
	leaq	-176(%rbp), %r14
	leaq	.LC1(%rip), %rsi
	movq	(%r12), %rax
	movq	8(%rax), %rbx
	jmp	.L4655
	.p2align 4,,10
	.p2align 3
.L4658:
	movl	-428(%rbp), %edx
	movq	8(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector14InjectedScript12ContextScopeC1EPNS_22V8InspectorSessionImplEi@PLT
	leaq	-112(%rbp), %rax
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -496(%rbp)
	call	_ZN12v8_inspector14InjectedScript5Scope10initializeEv@PLT
	movl	-112(%rbp), %eax
	leaq	-104(%rbp), %rsi
	movq	%rsi, -512(%rbp)
	movl	%eax, -176(%rbp)
	leaq	-168(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -520(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	movq	-72(%rbp), %rax
	movq	-104(%rbp), %rdi
	movq	%rax, -136(%rbp)
	movl	-64(%rbp), %eax
	movl	%eax, -128(%rbp)
	leaq	-88(%rbp), %rax
	movq	%rax, -504(%rbp)
	cmpq	%rax, %rdi
	je	.L4558
	call	_ZdlPv@PLT
.L4558:
	movl	-176(%rbp), %edi
	testl	%edi, %edi
	jne	.L4660
	movq	-440(%rbp), %rax
	cmpb	$0, (%rax)
	je	.L4561
	cmpb	$0, 1(%rax)
	je	.L4561
	movq	%r13, %rdi
	call	_ZN12v8_inspector14InjectedScript5Scope30ignoreExceptionsAndMuteConsoleEv@PLT
.L4561:
	movq	48(%r14), %rax
	movq	$0, 48(%r14)
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, -440(%rbp)
	call	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrIN2v86GlobalINS5_6ScriptEEESt14default_deleteIS8_EEESaISC_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSE_18_Mod_range_hashingENSE_20_Default_ranged_hashENSE_20_Prime_rehash_policyENSE_17_Hashtable_traitsILb1ELb0ELb1EEEE5eraseENSE_20_Node_const_iteratorISC_Lb0ELb1EEE
	movq	-440(%rbp), %rax
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L4565
	movq	32(%rbx), %rdx
	movq	(%rax), %rsi
	movq	8(%rdx), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L4565
	movq	-448(%rbp), %rax
	cmpb	$0, (%rax)
	je	.L4569
	cmpb	$0, 1(%rax)
	je	.L4569
	movq	%r13, %rdi
	call	_ZN12v8_inspector14InjectedScript5Scope21installCommandLineAPIEv@PLT
.L4569:
	movq	32(%rbx), %rax
	leaq	-400(%rbp), %r15
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	8(%rax), %rsi
	call	_ZN2v815MicrotasksScopeC1EPNS_7IsolateENS0_4TypeE@PLT
	movq	-272(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v86Script3RunENS_5LocalINS_7ContextEEE@PLT
	movq	%r15, %rdi
	movq	%rax, %r14
	call	_ZN2v815MicrotasksScopeD1Ev@PLT
	movq	-496(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector14InjectedScript5Scope10initializeEv@PLT
	movl	-112(%rbp), %eax
	movq	-520(%rbp), %rdi
	movq	-512(%rbp), %rsi
	movl	%eax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	movq	-72(%rbp), %rax
	movq	-104(%rbp), %rdi
	movq	%rax, -136(%rbp)
	movl	-64(%rbp), %eax
	movl	%eax, -128(%rbp)
	cmpq	-504(%rbp), %rdi
	je	.L4570
	call	_ZdlPv@PLT
.L4570:
	movl	-176(%rbp), %esi
	testl	%esi, %esi
	jne	.L4661
	movq	-480(%rbp), %rax
	movl	$1, %edx
	cmpb	$0, (%rax)
	je	.L4572
	xorl	%edx, %edx
	cmpb	$0, 1(%rax)
	setne	%dl
	addl	$1, %edx
.L4572:
	movq	-472(%rbp), %rax
	cmpb	$0, (%rax)
	je	.L4573
	cmpb	$0, 1(%rax)
	movl	$0, %eax
	cmovne	%eax, %edx
.L4573:
	movq	-456(%rbp), %rax
	cmpb	$0, (%rax)
	je	.L4646
	cmpb	$0, 1(%rax)
	je	.L4646
	leaq	-320(%rbp), %rcx
	movl	%edx, -456(%rbp)
	movq	%rcx, %rdi
	movq	%rcx, -448(%rbp)
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	movq	-448(%rbp), %rcx
	movl	-456(%rbp), %edx
	testb	%al, %al
	je	.L4662
.L4577:
	leaq	-224(%rbp), %rbx
	movl	%edx, -448(%rbp)
	movq	(%r12), %r12
	leaq	.LC16(%rip), %rsi
	movq	%rbx, %rdi
	movq	%rcx, -456(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-464(%rbp), %rax
	movl	-448(%rbp), %edx
	movq	$0, -424(%rbp)
	movq	-456(%rbp), %rcx
	movq	-352(%rbp), %rsi
	movq	$0, -416(%rbp)
	cmpb	$0, (%rax)
	leaq	8(%rax), %r8
	leaq	-416(%rbp), %rax
	movl	%edx, %r9d
	pushq	%rax
	leaq	-424(%rbp), %rax
	cmove	%rbx, %r8
	movq	%r14, %rdx
	pushq	%rax
	movq	-496(%rbp), %rdi
	call	_ZN12v8_inspector14InjectedScript18wrapEvaluateResultEN2v810MaybeLocalINS1_5ValueEEERKNS1_8TryCatchERKNS_8String16ENS_8WrapModeEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISF_EEPN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeINSE_16ExceptionDetailsEEE@PLT
	movl	-112(%rbp), %ecx
	popq	%rax
	popq	%rdx
	testl	%ecx, %ecx
	je	.L4663
	movq	(%r12), %rax
	movq	-496(%rbp), %rsi
	movq	%r12, %rdi
	call	*8(%rax)
.L4583:
	movq	-104(%rbp), %rdi
	cmpq	-504(%rbp), %rdi
	je	.L4584
	call	_ZdlPv@PLT
.L4584:
	movq	-416(%rbp), %r12
	testq	%r12, %r12
	je	.L4585
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4586
	call	_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD1Ev
	movl	$184, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4585:
	movq	-424(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4587
	movq	(%rdi), %rax
	call	*24(%rax)
.L4587:
	movq	-224(%rbp), %rdi
	leaq	-208(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4591
	call	_ZdlPv@PLT
.L4591:
	movq	-440(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L4594
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L4594:
	movq	-440(%rbp), %rdi
	movl	$8, %esi
	call	_ZdlPvm@PLT
	jmp	.L4560
	.p2align 4,,10
	.p2align 3
.L4551:
	movq	$1, 32(%r11)
	movl	$1, %ecx
	jmp	.L4548
	.p2align 4,,10
	.p2align 3
.L4660:
	movq	(%r12), %rdi
	movq	-488(%rbp), %rsi
	movq	(%rdi), %rax
	call	*8(%rax)
.L4560:
	movq	%r13, %rdi
	call	_ZN12v8_inspector14InjectedScript12ContextScopeD1Ev@PLT
	jmp	.L4557
	.p2align 4,,10
	.p2align 3
.L4646:
	leaq	-320(%rbp), %rcx
	jmp	.L4577
	.p2align 4,,10
	.p2align 3
.L4565:
	movq	(%r12), %r12
	leaq	-224(%rbp), %r14
	leaq	.LC19(%rip), %rsi
	movq	%r14, %rdi
	movq	(%r12), %rax
	movq	8(%rax), %r15
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-496(%rbp), %rbx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	*%r15
	movq	-104(%rbp), %rdi
	cmpq	-504(%rbp), %rdi
	je	.L4587
	call	_ZdlPv@PLT
	jmp	.L4587
	.p2align 4,,10
	.p2align 3
.L4661:
	movq	(%r12), %rdi
	movq	-488(%rbp), %rsi
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L4591
	.p2align 4,,10
	.p2align 3
.L4662:
	movq	(%r12), %rcx
	movq	$0, (%r12)
	movl	$16, %edi
	movq	-352(%rbp), %r11
	movl	%edx, -472(%rbp)
	movq	%rcx, -448(%rbp)
	movq	%r11, -456(%rbp)
	call	_Znwm@PLT
	leaq	16+_ZTVN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend17RunScriptCallbackEEE(%rip), %rcx
	movq	-496(%rbp), %r12
	leaq	.LC16(%rip), %rsi
	movq	%rcx, (%rax)
	movq	-448(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, -400(%rbp)
	movq	%rcx, 8(%rax)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-464(%rbp), %rax
	movq	-456(%rbp), %r11
	movl	-472(%rbp), %edx
	cmpb	$0, (%rax)
	leaq	8(%rax), %rcx
	cmove	%r12, %rcx
	testq	%r14, %r14
	je	.L4664
.L4589:
	movq	8(%rbx), %rsi
	movl	%edx, %r8d
	movq	%r11, %rdi
	movq	%r15, %r9
	movq	%r14, %rdx
	call	_ZN12v8_inspector14InjectedScript18addPromiseCallbackEPNS_22V8InspectorSessionImplEN2v810MaybeLocalINS3_5ValueEEERKNS_8String16ENS_8WrapModeESt10unique_ptrINS_16EvaluateCallbackESt14default_deleteISC_EE@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4590
	call	_ZdlPv@PLT
.L4590:
	movq	-400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4591
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L4591
	.p2align 4,,10
	.p2align 3
.L4663:
	movq	-416(%rbp), %rdx
	movq	(%r12), %rax
	movq	%r12, %rdi
	leaq	-408(%rbp), %rsi
	movq	(%rax), %rax
	movq	%rdx, -400(%rbp)
	movq	-424(%rbp), %rdx
	movq	$0, -416(%rbp)
	movq	$0, -424(%rbp)
	movq	%rdx, -408(%rbp)
	movq	%r15, %rdx
	call	*%rax
	movq	-408(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4581
	movq	(%rdi), %rax
	call	*24(%rax)
.L4581:
	movq	-400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4583
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L4583
.L4586:
	call	*%rax
	jmp	.L4585
.L4664:
	movq	%rcx, -464(%rbp)
	movq	%r11, -456(%rbp)
	movl	%edx, -448(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-464(%rbp), %rcx
	movq	-456(%rbp), %r11
	movl	-448(%rbp), %edx
	jmp	.L4589
.L4659:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8038:
	.size	_ZN12v8_inspector18V8RuntimeAgentImpl9runScriptERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIiEENS7_IS1_EENS7_IbEESA_SA_SA_SA_St10unique_ptrINS_8protocol7Runtime7Backend17RunScriptCallbackESt14default_deleteISF_EE, .-_ZN12v8_inspector18V8RuntimeAgentImpl9runScriptERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIiEENS7_IS1_EENS7_IbEESA_SA_SA_SA_St10unique_ptrINS_8protocol7Runtime7Backend17RunScriptCallbackESt14default_deleteISF_EE
	.section	.text._ZN12v8_inspector18V8RuntimeAgentImpl23globalLexicalScopeNamesEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIiEEPSt10unique_ptrISt6vectorINS_8String16ESaIS8_EESt14default_deleteISA_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector18V8RuntimeAgentImpl23globalLexicalScopeNamesEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIiEEPSt10unique_ptrISt6vectorINS_8String16ESaIS8_EESt14default_deleteISA_EE
	.type	_ZN12v8_inspector18V8RuntimeAgentImpl23globalLexicalScopeNamesEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIiEEPSt10unique_ptrISt6vectorINS_8String16ESaIS8_EESt14default_deleteISA_EE, @function
_ZN12v8_inspector18V8RuntimeAgentImpl23globalLexicalScopeNamesEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIiEEPSt10unique_ptrISt6vectorINS_8String16ESaIS8_EESt14default_deleteISA_EE:
.LFB8041:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-320(%rbp), %r14
	leaq	-356(%rbp), %r8
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	-176(%rbp), %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$360, %rsp
	movq	%rcx, -376(%rbp)
	movq	%r14, %rcx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	(%rdx), %eax
	movl	$0, -356(%rbp)
	movb	%al, -320(%rbp)
	movl	4(%rdx), %eax
	movl	%eax, -316(%rbp)
	movq	8(%rsi), %rax
	movq	32(%rsi), %rsi
	movl	16(%rax), %edx
	call	_ZN12v8_inspector12_GLOBAL__N_113ensureContextEPNS_15V8InspectorImplEiN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIiEEPi
	movl	-176(%rbp), %eax
	testl	%eax, %eax
	je	.L4666
	movl	%eax, (%r12)
	leaq	24(%r12), %rax
	leaq	-152(%rbp), %rdx
	movq	%rax, 8(%r12)
	movq	-168(%rbp), %rax
	cmpq	%rdx, %rax
	je	.L4717
	movq	%rax, 8(%r12)
	movq	-152(%rbp), %rax
	movq	%rax, 24(%r12)
.L4668:
	movq	-160(%rbp), %rax
	movq	%rax, 16(%r12)
	movq	-136(%rbp), %rax
	movq	%rax, 40(%r12)
	movl	-128(%rbp), %eax
	movl	%eax, 48(%r12)
.L4665:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4718
	addq	$360, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4717:
	.cfi_restore_state
	movdqu	-152(%rbp), %xmm2
	movups	%xmm2, 24(%r12)
	jmp	.L4668
	.p2align 4,,10
	.p2align 3
.L4666:
	movl	-356(%rbp), %edx
	movq	8(%rbx), %rsi
	movq	%r14, %rdi
	leaq	-112(%rbp), %r15
	call	_ZN12v8_inspector14InjectedScript12ContextScopeC1EPNS_22V8InspectorSessionImplEi@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector14InjectedScript5Scope10initializeEv@PLT
	movl	-112(%rbp), %eax
	leaq	-168(%rbp), %rdi
	leaq	-104(%rbp), %rsi
	movl	%eax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	movq	-72(%rbp), %rax
	movq	-104(%rbp), %rdi
	movq	%rax, -136(%rbp)
	movl	-64(%rbp), %eax
	movl	%eax, -128(%rbp)
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4670
	call	_ZdlPv@PLT
.L4670:
	movl	-176(%rbp), %eax
	testl	%eax, %eax
	je	.L4671
	movl	%eax, (%r12)
	leaq	24(%r12), %rax
	leaq	-152(%rbp), %rbx
	movq	%rax, 8(%r12)
	movq	-168(%rbp), %rax
	cmpq	%rbx, %rax
	je	.L4719
	movq	%rax, 8(%r12)
	movq	-152(%rbp), %rax
	movq	%rax, 24(%r12)
.L4673:
	movq	-160(%rbp), %rax
	xorl	%edx, %edx
	movq	%rbx, -168(%rbp)
	movq	$0, -160(%rbp)
	movq	%rax, 16(%r12)
	movq	-136(%rbp), %rax
	movw	%dx, -152(%rbp)
	movq	%rax, 40(%r12)
	movl	-128(%rbp), %eax
	movl	%eax, 48(%r12)
.L4674:
	movq	%r14, %rdi
	call	_ZN12v8_inspector14InjectedScript12ContextScopeD1Ev@PLT
	movq	-168(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L4665
	call	_ZdlPv@PLT
	jmp	.L4665
	.p2align 4,,10
	.p2align 3
.L4719:
	movdqu	-152(%rbp), %xmm3
	movups	%xmm3, 24(%r12)
	jmp	.L4673
	.p2align 4,,10
	.p2align 3
.L4671:
	movq	32(%rbx), %rax
	movq	-224(%rbp), %rdi
	leaq	-352(%rbp), %rsi
	movq	8(%rax), %rax
	movq	$0, -344(%rbp)
	movq	$0, -336(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -328(%rbp)
	call	_ZN2v85debug23GlobalLexicalScopeNamesENS_5LocalINS_7ContextEEEPNS_21PersistentValueVectorINS_6StringENS_34DefaultPersistentValueVectorTraitsEEE@PLT
	movl	$24, %edi
	call	_Znwm@PLT
	movq	-376(%rbp), %rsi
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rax)
	movq	(%rsi), %rcx
	movups	%xmm0, (%rax)
	movq	%rax, (%rsi)
	movq	%rcx, -392(%rbp)
	movq	%rcx, %rax
	testq	%rcx, %rcx
	je	.L4675
	movq	8(%rcx), %rcx
	movq	(%rax), %r13
	movq	%rcx, -384(%rbp)
	cmpq	%r13, %rcx
	je	.L4676
	.p2align 4,,10
	.p2align 3
.L4680:
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L4677
	call	_ZdlPv@PLT
	addq	$40, %r13
	cmpq	%r13, -384(%rbp)
	jne	.L4680
.L4678:
	movq	-392(%rbp), %rax
	movq	(%rax), %r13
.L4676:
	testq	%r13, %r13
	je	.L4681
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L4681:
	movq	-392(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L4675:
	leaq	-96(%rbp), %rax
	movq	-344(%rbp), %rdx
	xorl	%r13d, %r13d
	movq	%rax, -392(%rbp)
	cmpq	%rdx, -336(%rbp)
	jne	.L4682
	jmp	.L4690
	.p2align 4,,10
	.p2align 3
.L4687:
	movq	%rax, (%rsi)
	movq	-96(%rbp), %rax
	movq	%rax, 16(%rsi)
.L4688:
	movq	-104(%rbp), %rax
	movq	%rax, 8(%rsi)
	xorl	%eax, %eax
	movw	%ax, -96(%rbp)
	movq	-80(%rbp), %rax
	movq	$0, -104(%rbp)
	movq	%rax, 32(%rsi)
	addq	$40, 8(%r8)
.L4689:
	movq	-344(%rbp), %rdx
	movq	-336(%rbp), %rax
	addq	$1, %r13
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rax, %r13
	jnb	.L4690
.L4682:
	movq	-376(%rbp), %rax
	movq	(%rax), %r8
	movq	(%rdx,%r13,8), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.L4685
	movq	(%rax), %rsi
	movq	-352(%rbp), %rdi
	movq	%r8, -384(%rbp)
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	-384(%rbp), %r8
	movq	%rax, %rdx
.L4685:
	movq	32(%rbx), %rax
	movq	%r15, %rdi
	movq	%r8, -384(%rbp)
	movq	8(%rax), %rsi
	call	_ZN12v8_inspector16toProtocolStringEPN2v87IsolateENS0_5LocalINS0_6StringEEE@PLT
	movq	-384(%rbp), %r8
	movq	8(%r8), %rsi
	cmpq	16(%r8), %rsi
	je	.L4686
	leaq	16(%rsi), %rax
	movq	%rax, (%rsi)
	movq	-112(%rbp), %rax
	cmpq	-392(%rbp), %rax
	jne	.L4687
	movdqa	-96(%rbp), %xmm1
	movups	%xmm1, 16(%rsi)
	jmp	.L4688
	.p2align 4,,10
	.p2align 3
.L4686:
	movq	%r8, %rdi
	movq	%r15, %rdx
	call	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	movq	-112(%rbp), %rdi
	cmpq	-392(%rbp), %rdi
	je	.L4689
	call	_ZdlPv@PLT
	jmp	.L4689
	.p2align 4,,10
	.p2align 3
.L4677:
	addq	$40, %r13
	cmpq	%r13, -384(%rbp)
	jne	.L4680
	jmp	.L4678
	.p2align 4,,10
	.p2align 3
.L4690:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	movq	-336(%rbp), %rdx
	movq	-344(%rbp), %rdi
	movq	%rdx, %r13
	subq	%rdi, %r13
	sarq	$3, %r13
	je	.L4683
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L4684:
	movq	%rdx, %rax
	subq	%rdi, %rax
	sarq	$3, %rax
	cmpq	%rbx, %rax
	jbe	.L4691
	movq	(%rdi,%rbx,8), %r8
	testq	%r8, %r8
	je	.L4691
	movq	%r8, %rdi
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	-344(%rbp), %rdi
	movq	-336(%rbp), %rdx
.L4691:
	addq	$1, %rbx
	cmpq	%rbx, %r13
	jne	.L4684
.L4683:
	cmpq	%rdx, %rdi
	je	.L4692
	movq	%rdi, -336(%rbp)
.L4692:
	leaq	-152(%rbp), %rbx
	testq	%rdi, %rdi
	je	.L4674
	call	_ZdlPv@PLT
	jmp	.L4674
.L4718:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8041:
	.size	_ZN12v8_inspector18V8RuntimeAgentImpl23globalLexicalScopeNamesEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIiEEPSt10unique_ptrISt6vectorINS_8String16ESaIS8_EESt14default_deleteISA_EE, .-_ZN12v8_inspector18V8RuntimeAgentImpl23globalLexicalScopeNamesEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIiEEPSt10unique_ptrISt6vectorINS_8String16ESaIS8_EESt14default_deleteISA_EE
	.section	.rodata._ZN12v8_inspector18V8RuntimeAgentImpl12awaitPromiseERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEES8_St10unique_ptrINS_8protocol7Runtime7Backend20AwaitPromiseCallbackESt14default_deleteISD_EE.str1.8,"aMS",@progbits,1
	.align 8
.LC20:
	.string	"Could not find promise with given id"
	.section	.text._ZN12v8_inspector18V8RuntimeAgentImpl12awaitPromiseERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEES8_St10unique_ptrINS_8protocol7Runtime7Backend20AwaitPromiseCallbackESt14default_deleteISD_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector18V8RuntimeAgentImpl12awaitPromiseERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEES8_St10unique_ptrINS_8protocol7Runtime7Backend20AwaitPromiseCallbackESt14default_deleteISD_EE
	.type	_ZN12v8_inspector18V8RuntimeAgentImpl12awaitPromiseERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEES8_St10unique_ptrINS_8protocol7Runtime7Backend20AwaitPromiseCallbackESt14default_deleteISD_EE, @function
_ZN12v8_inspector18V8RuntimeAgentImpl12awaitPromiseERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEES8_St10unique_ptrINS_8protocol7Runtime7Backend20AwaitPromiseCallbackESt14default_deleteISD_EE:
.LFB7964:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-416(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-288(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$456, %rsp
	movq	%rdx, -488(%rbp)
	movq	8(%rdi), %rsi
	movq	%r9, %rdx
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector14InjectedScript11ObjectScopeC1EPNS_22V8InspectorSessionImplERKNS_8String16E@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector14InjectedScript5Scope10initializeEv@PLT
	movl	-416(%rbp), %eax
	testl	%eax, %eax
	je	.L4721
	movq	(%r12), %rdi
	movq	%r14, %rsi
	movq	(%rdi), %rax
	call	*8(%rax)
.L4728:
	movq	-408(%rbp), %rdi
	leaq	-392(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4730
	call	_ZdlPv@PLT
.L4730:
	movq	%r13, %rdi
	call	_ZN12v8_inspector14InjectedScript11ObjectScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4742
	addq	$456, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4721:
	.cfi_restore_state
	movq	-72(%rbp), %rdi
	call	_ZNK2v85Value9IsPromiseEv@PLT
	testb	%al, %al
	je	.L4743
	cmpb	$0, (%r15)
	movl	$1, %r8d
	je	.L4726
	xorl	%r8d, %r8d
	cmpb	$0, 1(%r15)
	setne	%r8b
	addl	$1, %r8d
.L4726:
	movq	-488(%rbp), %rax
	cmpb	$0, (%rax)
	jne	.L4744
.L4727:
	movq	(%r12), %r15
	movq	$0, (%r12)
	movl	$16, %edi
	movl	%r8d, -488(%rbp)
	movq	-272(%rbp), %r14
	call	_Znwm@PLT
	movq	8(%rbx), %rsi
	movq	-72(%rbp), %rdx
	movq	%r14, %rdi
	leaq	16+_ZTVN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend20AwaitPromiseCallbackEEE(%rip), %rcx
	movq	%r15, 8(%rax)
	movl	-488(%rbp), %r8d
	leaq	-472(%rbp), %r9
	movq	%rcx, (%rax)
	leaq	-112(%rbp), %rcx
	movq	%rax, -472(%rbp)
	call	_ZN12v8_inspector14InjectedScript18addPromiseCallbackEPNS_22V8InspectorSessionImplEN2v810MaybeLocalINS3_5ValueEEERKNS_8String16ENS_8WrapModeESt10unique_ptrINS_16EvaluateCallbackESt14default_deleteISC_EE@PLT
	movq	-472(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4728
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L4728
	.p2align 4,,10
	.p2align 3
.L4744:
	cmpb	$0, 1(%rax)
	movl	$0, %eax
	cmovne	%eax, %r8d
	jmp	.L4727
	.p2align 4,,10
	.p2align 3
.L4743:
	movq	(%r12), %r12
	leaq	-464(%rbp), %r15
	leaq	.LC20(%rip), %rsi
	movq	%r15, %rdi
	leaq	-352(%rbp), %r14
	movq	(%r12), %rax
	movq	8(%rax), %rbx
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	*%rbx
	movq	-344(%rbp), %rdi
	leaq	-328(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4724
	call	_ZdlPv@PLT
.L4724:
	movq	-464(%rbp), %rdi
	leaq	-448(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4728
	call	_ZdlPv@PLT
	jmp	.L4728
.L4742:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7964:
	.size	_ZN12v8_inspector18V8RuntimeAgentImpl12awaitPromiseERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEES8_St10unique_ptrINS_8protocol7Runtime7Backend20AwaitPromiseCallbackESt14default_deleteISD_EE, .-_ZN12v8_inspector18V8RuntimeAgentImpl12awaitPromiseERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEES8_St10unique_ptrINS_8protocol7Runtime7Backend20AwaitPromiseCallbackESt14default_deleteISD_EE
	.section	.rodata._ZN12v8_inspector18V8RuntimeAgentImpl14callFunctionOnERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS1_EENS6_8PtrMaybeISt6vectorISt10unique_ptrINS_8protocol7Runtime12CallArgumentESt14default_deleteISE_EESaISH_EEEENS7_IbEESL_SL_SL_SL_NS7_IiEES8_SB_INSD_7Backend22CallFunctionOnCallbackESF_ISO_EE.str1.8,"aMS",@progbits,1
	.align 8
.LC21:
	.string	"ObjectId must not be specified together with executionContextId"
	.align 8
.LC22:
	.string	"Either ObjectId or executionContextId must be specified"
	.align 8
.LC23:
	.string	"const T& v8_inspector_protocol_bindings::glue::detail::ValueMaybe<T>::fromJust() const [with T = int]"
	.align 8
.LC24:
	.string	"../deps/v8/../../deps/v8/third_party/inspector_protocol/bindings/bindings.h"
	.section	.rodata._ZN12v8_inspector18V8RuntimeAgentImpl14callFunctionOnERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS1_EENS6_8PtrMaybeISt6vectorISt10unique_ptrINS_8protocol7Runtime12CallArgumentESt14default_deleteISE_EESaISH_EEEENS7_IbEESL_SL_SL_SL_NS7_IiEES8_SB_INSD_7Backend22CallFunctionOnCallbackESF_ISO_EE.str1.1,"aMS",@progbits,1
.LC25:
	.string	"is_just_"
	.section	.text._ZN12v8_inspector18V8RuntimeAgentImpl14callFunctionOnERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS1_EENS6_8PtrMaybeISt6vectorISt10unique_ptrINS_8protocol7Runtime12CallArgumentESt14default_deleteISE_EESaISH_EEEENS7_IbEESL_SL_SL_SL_NS7_IiEES8_SB_INSD_7Backend22CallFunctionOnCallbackESF_ISO_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector18V8RuntimeAgentImpl14callFunctionOnERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS1_EENS6_8PtrMaybeISt6vectorISt10unique_ptrINS_8protocol7Runtime12CallArgumentESt14default_deleteISE_EESaISH_EEEENS7_IbEESL_SL_SL_SL_NS7_IiEES8_SB_INSD_7Backend22CallFunctionOnCallbackESF_ISO_EE
	.type	_ZN12v8_inspector18V8RuntimeAgentImpl14callFunctionOnERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS1_EENS6_8PtrMaybeISt6vectorISt10unique_ptrINS_8protocol7Runtime12CallArgumentESt14default_deleteISE_EESaISH_EEEENS7_IbEESL_SL_SL_SL_NS7_IiEES8_SB_INSD_7Backend22CallFunctionOnCallbackESF_ISO_EE, @function
_ZN12v8_inspector18V8RuntimeAgentImpl14callFunctionOnERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS1_EENS6_8PtrMaybeISt6vectorISt10unique_ptrINS_8protocol7Runtime12CallArgumentESt14default_deleteISE_EESaISH_EEEENS7_IbEESL_SL_SL_SL_NS7_IiEES8_SB_INSD_7Backend22CallFunctionOnCallbackESF_ISO_EE:
.LFB7966:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$600, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rbp), %rax
	movq	%rsi, -584(%rbp)
	movq	16(%rbp), %rdi
	movq	56(%rbp), %r13
	movq	%rcx, -592(%rbp)
	movq	%rax, -608(%rbp)
	movq	32(%rbp), %rax
	movq	%r8, -600(%rbp)
	movq	40(%rbp), %rcx
	movq	%rax, -616(%rbp)
	movq	48(%rbp), %rax
	movzbl	(%rcx), %esi
	movq	%rax, -624(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	(%rdx), %eax
	testb	%al, %al
	jne	.L4858
	testb	%sil, %sil
	je	.L4751
.L4747:
	cmpb	$0, (%rdi)
	movl	$1, %r15d
	jne	.L4859
	cmpb	$0, (%r9)
	jne	.L4860
.L4756:
	testb	%al, %al
	jne	.L4861
.L4757:
	movl	$0, -564(%rbp)
	testb	%sil, %sil
	je	.L4862
	movl	4(%rcx), %eax
	leaq	-352(%rbp), %r9
	movq	32(%r14), %rsi
	movb	$1, -544(%rbp)
	leaq	-544(%rbp), %rbx
	movq	%r9, %rdi
	leaq	-564(%rbp), %r8
	movq	%r9, -632(%rbp)
	movl	%eax, -540(%rbp)
	movq	8(%r14), %rax
	movq	%rbx, %rcx
	movl	16(%rax), %edx
	call	_ZN12v8_inspector12_GLOBAL__N_113ensureContextEPNS_15V8InspectorImplEiN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIiEEPi
	movl	-352(%rbp), %edx
	movq	-632(%rbp), %r9
	testl	%edx, %edx
	je	.L4778
	movq	0(%r13), %rdi
	movq	%r9, %rsi
	movq	(%rdi), %rax
	call	*8(%rax)
.L4779:
	movq	-344(%rbp), %rdi
	leaq	-328(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4745
.L4851:
	call	_ZdlPv@PLT
.L4745:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4863
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4858:
	.cfi_restore_state
	testb	%sil, %sil
	je	.L4747
	movq	0(%r13), %r12
	leaq	-352(%rbp), %r14
	leaq	.LC21(%rip), %rsi
	movq	(%r12), %rax
	movq	8(%rax), %rbx
.L4857:
	movq	%r14, %rdi
	leaq	-288(%rbp), %r13
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	*%rbx
	movq	-280(%rbp), %rdi
	leaq	-264(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4754
	call	_ZdlPv@PLT
.L4754:
	movq	-352(%rbp), %rdi
	leaq	-336(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L4851
	jmp	.L4745
	.p2align 4,,10
	.p2align 3
.L4860:
	cmpb	$0, 1(%r9)
	movl	$0, %edi
	cmovne	%edi, %r15d
	testb	%al, %al
	je	.L4757
.L4861:
	movq	8(%r14), %rsi
	leaq	-288(%rbp), %r12
	addq	$8, %rdx
	movq	%r12, %rdi
	leaq	-352(%rbp), %rbx
	call	_ZN12v8_inspector14InjectedScript11ObjectScopeC1EPNS_22V8InspectorSessionImplERKNS_8String16E@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector14InjectedScript5Scope10initializeEv@PLT
	movl	-352(%rbp), %esi
	testl	%esi, %esi
	jne	.L4864
	movq	-624(%rbp), %rdx
	movq	0(%r13), %rax
	leaq	-112(%rbp), %rcx
	movq	$0, 0(%r13)
	movzbl	(%rdx), %ebx
	movq	%rax, -544(%rbp)
	testb	%bl, %bl
	je	.L4761
	leaq	-384(%rbp), %rax
	xorl	%ecx, %ecx
	movq	$0, -392(%rbp)
	movq	%rax, -400(%rbp)
	movq	$0, -368(%rbp)
	movw	%cx, -384(%rbp)
	movq	%rdx, %rcx
	addq	$8, %rcx
.L4761:
	movq	-616(%rbp), %rax
	xorl	%edx, %edx
	cmpb	$0, (%rax)
	je	.L4762
	movzbl	1(%rax), %edx
.L4762:
	movq	-608(%rbp), %rsi
	xorl	%eax, %eax
	cmpb	$0, (%rsi)
	je	.L4763
	movzbl	1(%rsi), %eax
.L4763:
	movq	-600(%rbp), %rsi
	xorl	%r9d, %r9d
	cmpb	$0, (%rsi)
	je	.L4764
	movzbl	1(%rsi), %r9d
.L4764:
	movq	-592(%rbp), %rdi
	subq	$8, %rsp
	leaq	-552(%rbp), %r8
	movq	(%rdi), %rsi
	movq	$0, (%rdi)
	movq	8(%r14), %rdi
	movq	%rsi, -552(%rbp)
	leaq	-544(%rbp), %rsi
	pushq	%rsi
	movq	%r12, %rsi
	pushq	%rcx
	movq	-584(%rbp), %rcx
	pushq	%rdx
	movq	-72(%rbp), %rdx
	pushq	%rax
	pushq	%r15
	call	_ZN12v8_inspector12_GLOBAL__N_119innerCallFunctionOnEPNS_22V8InspectorSessionImplERNS_14InjectedScript5ScopeEN2v85LocalINS6_5ValueEEERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeISt6vectorISt10unique_ptrINS_8protocol7Runtime12CallArgumentESt14default_deleteISL_EESaISO_EEEEbNS_8WrapModeEbbSC_SI_INSK_7Backend22CallFunctionOnCallbackESM_ISU_EE
	movq	-552(%rbp), %r13
	addq	$48, %rsp
	testq	%r13, %r13
	je	.L4765
	movq	8(%r13), %rax
	movq	0(%r13), %r15
	cmpq	%r15, %rax
	je	.L4766
	movb	%bl, -584(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	jmp	.L4772
	.p2align 4,,10
	.p2align 3
.L4866:
	movq	72(%r14), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12CallArgumentE(%rip), %rax
	leaq	88(%r14), %rdx
	movq	%rax, (%r14)
	cmpq	%rdx, %rdi
	je	.L4769
	call	_ZdlPv@PLT
.L4769:
	movq	24(%r14), %rdi
	leaq	40(%r14), %rdx
	cmpq	%rdx, %rdi
	je	.L4770
	call	_ZdlPv@PLT
.L4770:
	movq	8(%r14), %rdi
	testq	%rdi, %rdi
	je	.L4771
	movq	(%rdi), %rdx
	call	*24(%rdx)
.L4771:
	movl	$112, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L4767:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L4865
.L4772:
	movq	(%rbx), %r14
	testq	%r14, %r14
	je	.L4767
	movq	(%r14), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12CallArgumentD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L4866
	movq	%r14, %rdi
	call	*%rdx
	jmp	.L4767
	.p2align 4,,10
	.p2align 3
.L4859:
	xorl	%r15d, %r15d
	cmpb	$0, 1(%rdi)
	setne	%r15b
	addl	$1, %r15d
	cmpb	$0, (%r9)
	je	.L4756
	jmp	.L4860
	.p2align 4,,10
	.p2align 3
.L4778:
	movl	-564(%rbp), %edx
	movq	8(%r14), %rsi
	movq	%rbx, %rdi
	leaq	-288(%rbp), %r12
	movq	%r9, -632(%rbp)
	call	_ZN12v8_inspector14InjectedScript12ContextScopeC1EPNS_22V8InspectorSessionImplEi@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector14InjectedScript5Scope10initializeEv@PLT
	movl	-288(%rbp), %eax
	leaq	-344(%rbp), %rdi
	leaq	-280(%rbp), %rsi
	movl	%eax, -352(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	movq	-248(%rbp), %rax
	movq	-280(%rbp), %rdi
	movq	-632(%rbp), %r9
	movq	%rax, -312(%rbp)
	movl	-240(%rbp), %eax
	movl	%eax, -304(%rbp)
	leaq	-264(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4780
	call	_ZdlPv@PLT
	movq	-632(%rbp), %r9
.L4780:
	movl	-352(%rbp), %eax
	movq	0(%r13), %rdi
	testl	%eax, %eax
	je	.L4781
	movq	(%rdi), %rax
	movq	%r9, %rsi
	call	*8(%rax)
	movq	%rbx, %rdi
	call	_ZN12v8_inspector14InjectedScript12ContextScopeD1Ev@PLT
	jmp	.L4779
	.p2align 4,,10
	.p2align 3
.L4781:
	movq	$0, 0(%r13)
	leaq	.LC16(%rip), %rsi
	movq	%rdi, -552(%rbp)
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-624(%rbp), %rcx
	cmpb	$0, (%rcx)
	leaq	8(%rcx), %rax
	cmovne	%rax, %r12
	movq	-616(%rbp), %rax
	xorl	%esi, %esi
	cmpb	$0, (%rax)
	je	.L4783
	movzbl	1(%rax), %esi
.L4783:
	movq	-608(%rbp), %rax
	xorl	%ecx, %ecx
	cmpb	$0, (%rax)
	je	.L4784
	movzbl	1(%rax), %ecx
.L4784:
	movq	-600(%rbp), %rax
	xorl	%r9d, %r9d
	cmpb	$0, (%rax)
	je	.L4785
	movzbl	1(%rax), %r9d
.L4785:
	movl	%ecx, -608(%rbp)
	movq	-592(%rbp), %rcx
	movq	-448(%rbp), %rdi
	movl	%r9d, -616(%rbp)
	movq	(%rcx), %rax
	movq	$0, (%rcx)
	movl	%esi, -600(%rbp)
	movq	%rax, -560(%rbp)
	call	_ZN2v87Context6GlobalEv@PLT
	movl	-600(%rbp), %esi
	movl	-608(%rbp), %ecx
	subq	$8, %rsp
	movq	%rax, %rdx
	leaq	-552(%rbp), %rax
	movq	8(%r14), %rdi
	movl	-616(%rbp), %r9d
	pushq	%rax
	leaq	-560(%rbp), %r8
	pushq	%r12
	pushq	%rsi
	movq	%rbx, %rsi
	pushq	%rcx
	movq	-584(%rbp), %rcx
	pushq	%r15
	call	_ZN12v8_inspector12_GLOBAL__N_119innerCallFunctionOnEPNS_22V8InspectorSessionImplERNS_14InjectedScript5ScopeEN2v85LocalINS6_5ValueEEERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeISt6vectorISt10unique_ptrINS_8protocol7Runtime12CallArgumentESt14default_deleteISL_EESaISO_EEEEbNS_8WrapModeEbbSC_SI_INSK_7Backend22CallFunctionOnCallbackESM_ISU_EE
	movq	-560(%rbp), %r12
	addq	$48, %rsp
	testq	%r12, %r12
	je	.L4786
	movq	8(%r12), %r13
	movq	(%r12), %r15
	cmpq	%r15, %r13
	jne	.L4793
	.p2align 4,,10
	.p2align 3
.L4787:
	testq	%r15, %r15
	je	.L4794
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L4794:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4786:
	movq	-288(%rbp), %rdi
	leaq	-272(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4795
	call	_ZdlPv@PLT
.L4795:
	movq	-552(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4796
	movq	(%rdi), %rax
	call	*32(%rax)
.L4796:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector14InjectedScript12ContextScopeD1Ev@PLT
	movq	-344(%rbp), %rdi
	leaq	-328(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L4851
	jmp	.L4745
	.p2align 4,,10
	.p2align 3
.L4868:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12CallArgumentE(%rip), %rax
	movq	72(%r14), %rdi
	movq	%rax, (%r14)
	leaq	88(%r14), %rax
	cmpq	%rax, %rdi
	je	.L4790
	call	_ZdlPv@PLT
.L4790:
	movq	24(%r14), %rdi
	leaq	40(%r14), %rax
	cmpq	%rax, %rdi
	je	.L4791
	call	_ZdlPv@PLT
.L4791:
	movq	8(%r14), %rdi
	testq	%rdi, %rdi
	je	.L4792
	movq	(%rdi), %rax
	call	*24(%rax)
.L4792:
	movl	$112, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L4788:
	addq	$8, %r15
	cmpq	%r15, %r13
	je	.L4867
.L4793:
	movq	(%r15), %r14
	testq	%r14, %r14
	je	.L4788
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12CallArgumentD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L4868
	movq	%r14, %rdi
	call	*%rax
	jmp	.L4788
	.p2align 4,,10
	.p2align 3
.L4751:
	movq	0(%r13), %r12
	leaq	-352(%rbp), %r14
	leaq	.LC22(%rip), %rsi
	movq	(%r12), %rax
	movq	8(%rax), %rbx
	jmp	.L4857
	.p2align 4,,10
	.p2align 3
.L4864:
	movq	0(%r13), %rdi
	movq	%rbx, %rsi
	movq	(%rdi), %rax
	call	*8(%rax)
.L4853:
	movq	-344(%rbp), %rdi
	leaq	-328(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4760
	call	_ZdlPv@PLT
.L4760:
	movq	%r12, %rdi
	call	_ZN12v8_inspector14InjectedScript11ObjectScopeD1Ev@PLT
	jmp	.L4745
	.p2align 4,,10
	.p2align 3
.L4865:
	movzbl	-584(%rbp), %ebx
	movq	0(%r13), %r15
.L4766:
	testq	%r15, %r15
	je	.L4773
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L4773:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4765:
	testb	%bl, %bl
	je	.L4774
	movq	-400(%rbp), %rdi
	leaq	-384(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4774
	call	_ZdlPv@PLT
.L4774:
	movq	-544(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4853
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L4853
	.p2align 4,,10
	.p2align 3
.L4867:
	movq	(%r12), %r15
	jmp	.L4787
.L4863:
	call	__stack_chk_fail@PLT
.L4862:
	leaq	.LC23(%rip), %rcx
	movl	$54, %edx
	leaq	.LC24(%rip), %rsi
	leaq	.LC25(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE7966:
	.size	_ZN12v8_inspector18V8RuntimeAgentImpl14callFunctionOnERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS1_EENS6_8PtrMaybeISt6vectorISt10unique_ptrINS_8protocol7Runtime12CallArgumentESt14default_deleteISE_EESaISH_EEEENS7_IbEESL_SL_SL_SL_NS7_IiEES8_SB_INSD_7Backend22CallFunctionOnCallbackESF_ISO_EE, .-_ZN12v8_inspector18V8RuntimeAgentImpl14callFunctionOnERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS1_EENS6_8PtrMaybeISt6vectorISt10unique_ptrINS_8protocol7Runtime12CallArgumentESt14default_deleteISE_EESaISH_EEEENS7_IbEESL_SL_SL_SL_NS7_IiEES8_SB_INSD_7Backend22CallFunctionOnCallbackESF_ISO_EE
	.section	.rodata._ZN12v8_inspector18V8RuntimeAgentImpl13getPropertiesERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEES8_S8_PSt10unique_ptrISt6vectorIS9_INS_8protocol7Runtime18PropertyDescriptorESt14default_deleteISD_EESaISG_EESE_ISI_EEPNS6_8PtrMaybeISA_IS9_INSC_26InternalPropertyDescriptorESE_ISN_EESaISP_EEEEPNSM_ISA_IS9_INSC_25PrivatePropertyDescriptorESE_ISU_EESaISW_EEEEPNSM_INSC_16ExceptionDetailsEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC26:
	.string	"Value with given id is not an object"
	.section	.text._ZN12v8_inspector18V8RuntimeAgentImpl13getPropertiesERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEES8_S8_PSt10unique_ptrISt6vectorIS9_INS_8protocol7Runtime18PropertyDescriptorESt14default_deleteISD_EESaISG_EESE_ISI_EEPNS6_8PtrMaybeISA_IS9_INSC_26InternalPropertyDescriptorESE_ISN_EESaISP_EEEEPNSM_ISA_IS9_INSC_25PrivatePropertyDescriptorESE_ISU_EESaISW_EEEEPNSM_INSC_16ExceptionDetailsEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector18V8RuntimeAgentImpl13getPropertiesERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEES8_S8_PSt10unique_ptrISt6vectorIS9_INS_8protocol7Runtime18PropertyDescriptorESt14default_deleteISD_EESaISG_EESE_ISI_EEPNS6_8PtrMaybeISA_IS9_INSC_26InternalPropertyDescriptorESE_ISN_EESaISP_EEEEPNSM_ISA_IS9_INSC_25PrivatePropertyDescriptorESE_ISU_EESaISW_EEEEPNSM_INSC_16ExceptionDetailsEEE
	.type	_ZN12v8_inspector18V8RuntimeAgentImpl13getPropertiesERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEES8_S8_PSt10unique_ptrISt6vectorIS9_INS_8protocol7Runtime18PropertyDescriptorESt14default_deleteISD_EESaISG_EESE_ISI_EEPNS6_8PtrMaybeISA_IS9_INSC_26InternalPropertyDescriptorESE_ISN_EESaISP_EEEEPNSM_ISA_IS9_INSC_25PrivatePropertyDescriptorESE_ISU_EESaISW_EEEEPNSM_INSC_16ExceptionDetailsEEE, @function
_ZN12v8_inspector18V8RuntimeAgentImpl13getPropertiesERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEES8_S8_PSt10unique_ptrISt6vectorIS9_INS_8protocol7Runtime18PropertyDescriptorESt14default_deleteISD_EESaISG_EESE_ISI_EEPNS6_8PtrMaybeISA_IS9_INSC_26InternalPropertyDescriptorESE_ISN_EESaISP_EEEEPNSM_ISA_IS9_INSC_25PrivatePropertyDescriptorESE_ISU_EESaISW_EEEEPNSM_INSC_16ExceptionDetailsEEE:
.LFB7971:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r9, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-288(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$488, %rsp
	movq	16(%rbp), %rax
	movq	8(%rsi), %rsi
	movq	%r8, -480(%rbp)
	movq	%rax, -488(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -496(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -504(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -472(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector14InjectedScript11ObjectScopeC1EPNS_22V8InspectorSessionImplERKNS_8String16E@PLT
	leaq	-416(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN12v8_inspector14InjectedScript5Scope10initializeEv@PLT
	movl	-416(%rbp), %eax
	testl	%eax, %eax
	je	.L4870
	movl	%eax, (%r14)
	leaq	24(%r14), %rax
	leaq	-392(%rbp), %rdx
	movq	%rax, 8(%r14)
	movq	-408(%rbp), %rax
	cmpq	%rdx, %rax
	je	.L4986
	movq	%rax, 8(%r14)
	movq	-392(%rbp), %rax
	movq	%rax, 24(%r14)
.L4872:
	movq	-400(%rbp), %rax
	movq	%rax, 16(%r14)
	movq	-376(%rbp), %rax
	movq	%rax, 40(%r14)
	movl	-368(%rbp), %eax
	movl	%eax, 48(%r14)
.L4873:
	movq	%r12, %rdi
	call	_ZN12v8_inspector14InjectedScript11ObjectScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4987
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4986:
	.cfi_restore_state
	movdqu	-392(%rbp), %xmm0
	movups	%xmm0, 24(%r14)
	jmp	.L4872
	.p2align 4,,10
	.p2align 3
.L4870:
	movq	%r12, %rdi
	call	_ZN12v8_inspector14InjectedScript5Scope30ignoreExceptionsAndMuteConsoleEv@PLT
	movq	32(%r15), %rax
	leaq	-448(%rbp), %r15
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	8(%rax), %rsi
	call	_ZN2v815MicrotasksScopeC1EPNS_7IsolateENS0_4TypeE@PLT
	movq	-72(%rbp), %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L4988
	movq	-72(%rbp), %rax
	cmpb	$0, 0(%r13)
	movq	-272(%rbp), %rsi
	movq	%rax, -512(%rbp)
	movl	$1, %eax
	je	.L4877
	xorl	%eax, %eax
	cmpb	$0, 1(%r13)
	setne	%al
	addl	$1, %eax
.L4877:
	movq	-480(%rbp), %rcx
	xorl	%r9d, %r9d
	cmpb	$0, (%rcx)
	je	.L4878
	movzbl	1(%rcx), %r9d
.L4878:
	xorl	%r8d, %r8d
	cmpb	$0, (%rbx)
	je	.L4879
	movzbl	1(%rbx), %r8d
.L4879:
	subq	$8, %rsp
	pushq	-472(%rbp)
	leaq	-352(%rbp), %rcx
	movq	-512(%rbp), %rdx
	pushq	-488(%rbp)
	leaq	-112(%rbp), %rbx
	movq	%rcx, %rdi
	leaq	-408(%rbp), %r13
	pushq	%rax
	movq	%rcx, -520(%rbp)
	movq	%rbx, %rcx
	call	_ZN12v8_inspector14InjectedScript13getPropertiesEN2v85LocalINS1_6ObjectEEERKNS_8String16EbbNS_8WrapModeEPSt10unique_ptrISt6vectorIS9_INS_8protocol7Runtime18PropertyDescriptorESt14default_deleteISD_EESaISG_EESE_ISI_EEPN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeINSC_16ExceptionDetailsEEE@PLT
	movl	-352(%rbp), %eax
	movq	%r13, %rdi
	addq	$32, %rsp
	movl	%eax, -416(%rbp)
	leaq	-344(%rbp), %rax
	movq	%rax, %rsi
	movq	%rax, -488(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	movq	-312(%rbp), %rax
	movq	-344(%rbp), %rdi
	leaq	-328(%rbp), %r11
	movq	%rax, -376(%rbp)
	movl	-304(%rbp), %eax
	movl	%eax, -368(%rbp)
	cmpq	%r11, %rdi
	je	.L4880
	movq	%r11, -528(%rbp)
	call	_ZdlPv@PLT
	movq	-528(%rbp), %r11
.L4880:
	movl	-416(%rbp), %eax
	testl	%eax, %eax
	jne	.L4989
	movq	-472(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L4990
.L4884:
	movq	%r14, %rdi
	leaq	-392(%rbp), %rbx
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
.L4876:
	movq	%r15, %rdi
	call	_ZN2v815MicrotasksScopeD1Ev@PLT
	movq	-408(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L4873
	call	_ZdlPv@PLT
	jmp	.L4873
	.p2align 4,,10
	.p2align 3
.L4989:
	movl	%eax, (%r14)
	leaq	24(%r14), %rax
	leaq	-392(%rbp), %rbx
	movq	%rax, 8(%r14)
	movq	-408(%rbp), %rax
	cmpq	%rbx, %rax
	je	.L4991
	movq	%rax, 8(%r14)
	movq	-392(%rbp), %rax
	movq	%rax, 24(%r14)
.L4883:
	movq	-400(%rbp), %rax
	xorl	%edx, %edx
	movq	%rbx, -408(%rbp)
	movq	$0, -400(%rbp)
	movq	%rax, 16(%r14)
	movq	-376(%rbp), %rax
	movw	%dx, -392(%rbp)
	movq	%rax, 40(%r14)
	movl	-368(%rbp), %eax
	movl	%eax, 48(%r14)
	jmp	.L4876
	.p2align 4,,10
	.p2align 3
.L4988:
	leaq	-352(%rbp), %r13
	leaq	.LC26(%rip), %rsi
	movq	%r13, %rdi
	leaq	-392(%rbp), %rbx
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-352(%rbp), %rdi
	leaq	-336(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4876
	call	_ZdlPv@PLT
	jmp	.L4876
	.p2align 4,,10
	.p2align 3
.L4990:
	movq	-480(%rbp), %rax
	cmpb	$0, (%rax)
	je	.L4885
	cmpb	$0, 1(%rax)
	jne	.L4884
	.p2align 4,,10
	.p2align 3
.L4885:
	movq	-512(%rbp), %rdx
	movq	-272(%rbp), %rsi
	leaq	-456(%rbp), %r9
	leaq	-464(%rbp), %r8
	movq	-520(%rbp), %rdi
	movq	%rbx, %rcx
	movq	%r11, -472(%rbp)
	movq	$0, -464(%rbp)
	movq	$0, -456(%rbp)
	call	_ZN12v8_inspector14InjectedScript31getInternalAndPrivatePropertiesEN2v85LocalINS1_5ValueEEERKNS_8String16EPSt10unique_ptrISt6vectorIS8_INS_8protocol7Runtime26InternalPropertyDescriptorESt14default_deleteISC_EESaISF_EESD_ISH_EEPS8_IS9_IS8_INSB_25PrivatePropertyDescriptorESD_ISL_EESaISN_EESD_ISP_EE@PLT
	movl	-352(%rbp), %eax
	movq	-488(%rbp), %rsi
	movq	%r13, %rdi
	movl	%eax, -416(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	movq	-312(%rbp), %rax
	movq	-344(%rbp), %rdi
	movq	-472(%rbp), %r11
	movq	%rax, -376(%rbp)
	movl	-304(%rbp), %eax
	movl	%eax, -368(%rbp)
	cmpq	%r11, %rdi
	je	.L4886
	call	_ZdlPv@PLT
.L4886:
	movl	-416(%rbp), %eax
	testl	%eax, %eax
	je	.L4887
	movl	%eax, (%r14)
	leaq	24(%r14), %rax
	leaq	-392(%rbp), %rbx
	movq	%rax, 8(%r14)
	movq	-408(%rbp), %rax
	cmpq	%rbx, %rax
	je	.L4992
	movq	%rax, 8(%r14)
	movq	-392(%rbp), %rax
	movq	%rax, 24(%r14)
.L4889:
	movq	-400(%rbp), %rax
	movq	%rbx, -408(%rbp)
	movq	$0, -400(%rbp)
	movq	%rax, 16(%r14)
	xorl	%eax, %eax
	movw	%ax, -392(%rbp)
	movq	-376(%rbp), %rax
	movq	%rax, 40(%r14)
	movl	-368(%rbp), %eax
	movl	%eax, 48(%r14)
.L4890:
	movq	-456(%rbp), %rax
	movq	%rax, -472(%rbp)
	testq	%rax, %rax
	je	.L4915
	movq	8(%rax), %rcx
	movq	(%rax), %r13
	cmpq	%r13, %rcx
	je	.L4924
	movq	%rbx, -496(%rbp)
	movq	%r13, %rbx
	movq	%rcx, %r13
	movq	%r14, -480(%rbp)
	movq	%r12, -488(%rbp)
	jmp	.L4916
	.p2align 4,,10
	.p2align 3
.L4994:
	movq	48(%r12), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime25PrivatePropertyDescriptorE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r14, %r14
	je	.L4921
	movq	(%r14), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rax
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L4922
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L4921:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rdx
	cmpq	%rdx, %rdi
	je	.L4923
	call	_ZdlPv@PLT
.L4923:
	movl	$56, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4919:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	je	.L4993
.L4916:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L4919
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime25PrivatePropertyDescriptorD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L4994
	movq	%r12, %rdi
	call	*%rdx
	jmp	.L4919
	.p2align 4,,10
	.p2align 3
.L4991:
	movdqu	-392(%rbp), %xmm1
	movups	%xmm1, 24(%r14)
	jmp	.L4883
.L4993:
	movq	-480(%rbp), %r14
	movq	-488(%rbp), %r12
	movq	-496(%rbp), %rbx
.L4924:
	movq	-472(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L4918
	call	_ZdlPv@PLT
.L4918:
	movq	-472(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L4915:
	movq	-464(%rbp), %rax
	movq	%rax, -472(%rbp)
	testq	%rax, %rax
	je	.L4876
	movq	8(%rax), %rcx
	movq	(%rax), %r13
	cmpq	%r13, %rcx
	je	.L4934
	movq	%rbx, -496(%rbp)
	movq	%r13, %rbx
	movq	%rcx, %r13
	movq	%r14, -480(%rbp)
	movq	%r12, -488(%rbp)
	jmp	.L4926
	.p2align 4,,10
	.p2align 3
.L4996:
	movq	48(%r12), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime26InternalPropertyDescriptorE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r14, %r14
	je	.L4931
	movq	(%r14), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rax
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L4932
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L4931:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rdx
	cmpq	%rdx, %rdi
	je	.L4933
	call	_ZdlPv@PLT
.L4933:
	movl	$56, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4929:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	je	.L4995
.L4926:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L4929
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime26InternalPropertyDescriptorD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L4996
	movq	%r12, %rdi
	call	*%rdx
	jmp	.L4929
.L4995:
	movq	-480(%rbp), %r14
	movq	-488(%rbp), %r12
	movq	-496(%rbp), %rbx
.L4934:
	movq	-472(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L4928
	call	_ZdlPv@PLT
.L4928:
	movq	-472(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
	jmp	.L4876
.L4887:
	movq	-464(%rbp), %rax
	movq	(%rax), %rbx
	cmpq	%rbx, 8(%rax)
	je	.L4892
	movq	-496(%rbp), %rcx
	movq	$0, -464(%rbp)
	movq	(%rcx), %rbx
	movq	%rax, (%rcx)
	movq	%rbx, -472(%rbp)
	movq	%rbx, %rax
	testq	%rbx, %rbx
	je	.L4892
	movq	8(%rbx), %rbx
	movq	(%rax), %r13
	cmpq	%r13, %rbx
	je	.L4902
	movq	%r12, -488(%rbp)
	movq	%r13, %r12
	movq	%r14, -480(%rbp)
	jmp	.L4894
	.p2align 4,,10
	.p2align 3
.L4998:
	movq	48(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime26InternalPropertyDescriptorE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L4899
	movq	(%r14), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rax
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L4900
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L4899:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rdx
	cmpq	%rdx, %rdi
	je	.L4901
	call	_ZdlPv@PLT
.L4901:
	movl	$56, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4897:
	addq	$8, %r12
	cmpq	%r12, %rbx
	je	.L4997
.L4894:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L4897
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime26InternalPropertyDescriptorD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L4998
	movq	%r13, %rdi
	call	*%rdx
	jmp	.L4897
.L4997:
	movq	-480(%rbp), %r14
	movq	-488(%rbp), %r12
.L4902:
	movq	-472(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L4896
	call	_ZdlPv@PLT
.L4896:
	movq	-472(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L4892:
	movq	-456(%rbp), %rax
	movq	(%rax), %rbx
	cmpq	%rbx, 8(%rax)
	je	.L4904
	movq	-504(%rbp), %rcx
	movq	$0, -456(%rbp)
	movq	(%rcx), %rbx
	movq	%rax, (%rcx)
	movq	%rbx, -472(%rbp)
	movq	%rbx, %rax
	testq	%rbx, %rbx
	je	.L4904
	movq	8(%rbx), %rbx
	movq	(%rax), %r13
	cmpq	%r13, %rbx
	je	.L4914
	movq	%r12, -488(%rbp)
	movq	%r13, %r12
	movq	%r14, -480(%rbp)
	jmp	.L4906
	.p2align 4,,10
	.p2align 3
.L5000:
	movq	48(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime25PrivatePropertyDescriptorE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L4911
	movq	(%r14), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rax
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L4912
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L4911:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rdx
	cmpq	%rdx, %rdi
	je	.L4913
	call	_ZdlPv@PLT
.L4913:
	movl	$56, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4909:
	addq	$8, %r12
	cmpq	%r12, %rbx
	je	.L4999
.L4906:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L4909
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime25PrivatePropertyDescriptorD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L5000
	movq	%r13, %rdi
	call	*%rdx
	jmp	.L4909
.L4999:
	movq	-480(%rbp), %r14
	movq	-488(%rbp), %r12
.L4914:
	movq	-472(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L4908
	call	_ZdlPv@PLT
.L4908:
	movq	-472(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L4904:
	movq	%r14, %rdi
	leaq	-392(%rbp), %rbx
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	jmp	.L4890
.L4932:
	call	*%rdx
	jmp	.L4931
.L4922:
	call	*%rdx
	jmp	.L4921
.L4992:
	movdqu	-392(%rbp), %xmm2
	movups	%xmm2, 24(%r14)
	jmp	.L4889
.L4900:
	call	*%rdx
	jmp	.L4899
.L4912:
	call	*%rdx
	jmp	.L4911
.L4987:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7971:
	.size	_ZN12v8_inspector18V8RuntimeAgentImpl13getPropertiesERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEES8_S8_PSt10unique_ptrISt6vectorIS9_INS_8protocol7Runtime18PropertyDescriptorESt14default_deleteISD_EESaISG_EESE_ISI_EEPNS6_8PtrMaybeISA_IS9_INSC_26InternalPropertyDescriptorESE_ISN_EESaISP_EEEEPNSM_ISA_IS9_INSC_25PrivatePropertyDescriptorESE_ISU_EESaISW_EEEEPNSM_INSC_16ExceptionDetailsEEE, .-_ZN12v8_inspector18V8RuntimeAgentImpl13getPropertiesERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEES8_S8_PSt10unique_ptrISt6vectorIS9_INS_8protocol7Runtime18PropertyDescriptorESt14default_deleteISD_EESaISG_EESE_ISI_EEPNS6_8PtrMaybeISA_IS9_INSC_26InternalPropertyDescriptorESE_ISN_EESaISP_EEEEPNSM_ISA_IS9_INSC_25PrivatePropertyDescriptorESE_ISU_EESaISW_EEEEPNSM_INSC_16ExceptionDetailsEEE
	.section	.text._ZN12v8_inspector18V8RuntimeAgentImpl13releaseObjectERKNS_8String16E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector18V8RuntimeAgentImpl13releaseObjectERKNS_8String16E
	.type	_ZN12v8_inspector18V8RuntimeAgentImpl13releaseObjectERKNS_8String16E, @function
_ZN12v8_inspector18V8RuntimeAgentImpl13releaseObjectERKNS_8String16E:
.LFB8028:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-272(%rbp), %r14
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r14, %rdi
	subq	$312, %rsp
	movq	8(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector14InjectedScript11ObjectScopeC1EPNS_22V8InspectorSessionImplERKNS_8String16E@PLT
	leaq	-336(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN12v8_inspector14InjectedScript5Scope10initializeEv@PLT
	movl	-336(%rbp), %eax
	testl	%eax, %eax
	je	.L5002
	movl	%eax, (%r12)
	leaq	24(%r12), %rax
	leaq	-312(%rbp), %rdx
	movq	%rax, 8(%r12)
	movq	-328(%rbp), %rax
	cmpq	%rdx, %rax
	je	.L5008
	movq	%rax, 8(%r12)
	movq	-312(%rbp), %rax
	movq	%rax, 24(%r12)
.L5004:
	movq	-320(%rbp), %rax
	movq	%rax, 16(%r12)
	movq	-296(%rbp), %rax
	movq	%rax, 40(%r12)
	movl	-288(%rbp), %eax
	movl	%eax, 48(%r12)
	jmp	.L5005
	.p2align 4,,10
	.p2align 3
.L5002:
	movq	-256(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector14InjectedScript13releaseObjectERKNS_8String16E@PLT
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	movq	-328(%rbp), %rdi
	leaq	-312(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5005
	call	_ZdlPv@PLT
.L5005:
	movq	%r14, %rdi
	call	_ZN12v8_inspector14InjectedScript11ObjectScopeD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5009
	addq	$312, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5008:
	.cfi_restore_state
	movdqu	-312(%rbp), %xmm0
	movups	%xmm0, 24(%r12)
	jmp	.L5004
.L5009:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8028:
	.size	_ZN12v8_inspector18V8RuntimeAgentImpl13releaseObjectERKNS_8String16E, .-_ZN12v8_inspector18V8RuntimeAgentImpl13releaseObjectERKNS_8String16E
	.section	.rodata._ZN12v8_inspector18V8RuntimeAgentImpl12queryObjectsERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS1_EEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISC_EE.str1.8,"aMS",@progbits,1
	.align 8
.LC27:
	.string	"Prototype should be instance of Object"
	.section	.text._ZN12v8_inspector18V8RuntimeAgentImpl12queryObjectsERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS1_EEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISC_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector18V8RuntimeAgentImpl12queryObjectsERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS1_EEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISC_EE
	.type	_ZN12v8_inspector18V8RuntimeAgentImpl12queryObjectsERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS1_EEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISC_EE, @function
_ZN12v8_inspector18V8RuntimeAgentImpl12queryObjectsERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS1_EEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISC_EE:
.LFB8040:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-288(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$360, %rsp
	movq	8(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector14InjectedScript11ObjectScopeC1EPNS_22V8InspectorSessionImplERKNS_8String16E@PLT
	leaq	-352(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector14InjectedScript5Scope10initializeEv@PLT
	movl	-352(%rbp), %eax
	testl	%eax, %eax
	je	.L5011
	movl	%eax, (%r12)
	leaq	24(%r12), %rax
	leaq	-328(%rbp), %rdx
	movq	%rax, 8(%r12)
	movq	-344(%rbp), %rax
	cmpq	%rdx, %rax
	je	.L5022
	movq	%rax, 8(%r12)
	movq	-328(%rbp), %rax
	movq	%rax, 24(%r12)
.L5013:
	movq	-336(%rbp), %rax
	movq	%rax, 16(%r12)
	movq	-312(%rbp), %rax
	movq	%rax, 40(%r12)
	movl	-304(%rbp), %eax
	movl	%eax, 48(%r12)
.L5014:
	movq	%r13, %rdi
	call	_ZN12v8_inspector14InjectedScript11ObjectScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5023
	addq	$360, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5022:
	.cfi_restore_state
	movdqu	-328(%rbp), %xmm0
	movups	%xmm0, 24(%r12)
	jmp	.L5013
	.p2align 4,,10
	.p2align 3
.L5011:
	movq	-72(%rbp), %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L5024
	movq	32(%rbx), %rax
	movq	-72(%rbp), %rdx
	movq	-192(%rbp), %rsi
	movq	24(%rax), %rdi
	call	_ZN12v8_inspector10V8Debugger12queryObjectsEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEE@PLT
	leaq	-112(%rbp), %rcx
	cmpb	$0, (%r14)
	movq	%r15, %r9
	movq	%rax, %rdx
	leaq	8(%r14), %rax
	movl	$1, %r8d
	movq	%r12, %rdi
	cmovne	%rax, %rcx
	movq	-272(%rbp), %rsi
	call	_ZN12v8_inspector14InjectedScript10wrapObjectEN2v85LocalINS1_5ValueEEERKNS_8String16ENS_8WrapModeEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISC_EE@PLT
.L5017:
	movq	-344(%rbp), %rdi
	leaq	-328(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5014
	call	_ZdlPv@PLT
	jmp	.L5014
	.p2align 4,,10
	.p2align 3
.L5024:
	leaq	-400(%rbp), %r14
	leaq	.LC27(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-400(%rbp), %rdi
	leaq	-384(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5017
	call	_ZdlPv@PLT
	jmp	.L5017
.L5023:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8040:
	.size	_ZN12v8_inspector18V8RuntimeAgentImpl12queryObjectsERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS1_EEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISC_EE, .-_ZN12v8_inspector18V8RuntimeAgentImpl12queryObjectsERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS1_EEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISC_EE
	.weak	_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE,"awG",@progbits,_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE, @object
	.size	_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE, 48
_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD1Ev
	.quad	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev
	.weak	_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE,"awG",@progbits,_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE, @object
	.size	_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE, 48
_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	.quad	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev
	.weak	_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE,"awG",@progbits,_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE, @object
	.size	_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE, 48
_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD1Ev
	.quad	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev
	.weak	_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE,"awG",@progbits,_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE, @object
	.size	_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE, 48
_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD1Ev
	.quad	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev
	.weak	_ZTVN12v8_inspector8protocol7Runtime26InternalPropertyDescriptorE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol7Runtime26InternalPropertyDescriptorE,"awG",@progbits,_ZTVN12v8_inspector8protocol7Runtime26InternalPropertyDescriptorE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol7Runtime26InternalPropertyDescriptorE, @object
	.size	_ZTVN12v8_inspector8protocol7Runtime26InternalPropertyDescriptorE, 48
_ZTVN12v8_inspector8protocol7Runtime26InternalPropertyDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol7Runtime26InternalPropertyDescriptor15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol7Runtime26InternalPropertyDescriptor17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol7Runtime26InternalPropertyDescriptorD1Ev
	.quad	_ZN12v8_inspector8protocol7Runtime26InternalPropertyDescriptorD0Ev
	.weak	_ZTVN12v8_inspector8protocol7Runtime25PrivatePropertyDescriptorE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol7Runtime25PrivatePropertyDescriptorE,"awG",@progbits,_ZTVN12v8_inspector8protocol7Runtime25PrivatePropertyDescriptorE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol7Runtime25PrivatePropertyDescriptorE, @object
	.size	_ZTVN12v8_inspector8protocol7Runtime25PrivatePropertyDescriptorE, 48
_ZTVN12v8_inspector8protocol7Runtime25PrivatePropertyDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol7Runtime25PrivatePropertyDescriptor15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol7Runtime25PrivatePropertyDescriptor17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol7Runtime25PrivatePropertyDescriptorD1Ev
	.quad	_ZN12v8_inspector8protocol7Runtime25PrivatePropertyDescriptorD0Ev
	.weak	_ZTVN12v8_inspector8protocol7Runtime12CallArgumentE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol7Runtime12CallArgumentE,"awG",@progbits,_ZTVN12v8_inspector8protocol7Runtime12CallArgumentE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol7Runtime12CallArgumentE, @object
	.size	_ZTVN12v8_inspector8protocol7Runtime12CallArgumentE, 48
_ZTVN12v8_inspector8protocol7Runtime12CallArgumentE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol7Runtime12CallArgument15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol7Runtime12CallArgument17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol7Runtime12CallArgumentD1Ev
	.quad	_ZN12v8_inspector8protocol7Runtime12CallArgumentD0Ev
	.weak	_ZTVN12v8_inspector8protocol7Runtime27ExecutionContextDescriptionE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol7Runtime27ExecutionContextDescriptionE,"awG",@progbits,_ZTVN12v8_inspector8protocol7Runtime27ExecutionContextDescriptionE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol7Runtime27ExecutionContextDescriptionE, @object
	.size	_ZTVN12v8_inspector8protocol7Runtime27ExecutionContextDescriptionE, 48
_ZTVN12v8_inspector8protocol7Runtime27ExecutionContextDescriptionE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol7Runtime27ExecutionContextDescription15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol7Runtime27ExecutionContextDescription17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol7Runtime27ExecutionContextDescriptionD1Ev
	.quad	_ZN12v8_inspector8protocol7Runtime27ExecutionContextDescriptionD0Ev
	.weak	_ZTVN12v8_inspector8protocol7Runtime16ExceptionDetailsE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol7Runtime16ExceptionDetailsE,"awG",@progbits,_ZTVN12v8_inspector8protocol7Runtime16ExceptionDetailsE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol7Runtime16ExceptionDetailsE, @object
	.size	_ZTVN12v8_inspector8protocol7Runtime16ExceptionDetailsE, 48
_ZTVN12v8_inspector8protocol7Runtime16ExceptionDetailsE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol7Runtime16ExceptionDetails15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol7Runtime16ExceptionDetails17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD1Ev
	.quad	_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD0Ev
	.weak	_ZTVN12v8_inspector8protocol7Runtime9CallFrameE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol7Runtime9CallFrameE,"awG",@progbits,_ZTVN12v8_inspector8protocol7Runtime9CallFrameE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol7Runtime9CallFrameE, @object
	.size	_ZTVN12v8_inspector8protocol7Runtime9CallFrameE, 48
_ZTVN12v8_inspector8protocol7Runtime9CallFrameE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol7Runtime9CallFrameD1Ev
	.quad	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev
	.section	.data.rel.ro.local._ZTVN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend22CallFunctionOnCallbackEEE,"aw"
	.align 8
	.type	_ZTVN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend22CallFunctionOnCallbackEEE, @object
	.size	_ZTVN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend22CallFunctionOnCallbackEEE, 48
_ZTVN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend22CallFunctionOnCallbackEEE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend22CallFunctionOnCallbackEE11sendSuccessESt10unique_ptrINS3_12RemoteObjectESt14default_deleteIS8_EEN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeINS3_16ExceptionDetailsEEE
	.quad	_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend22CallFunctionOnCallbackEE11sendFailureERKNS2_16DispatchResponseE
	.quad	_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend22CallFunctionOnCallbackEED1Ev
	.quad	_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend22CallFunctionOnCallbackEED0Ev
	.weak	_ZTVN12v8_inspector18V8RuntimeAgentImplE
	.section	.data.rel.ro.local._ZTVN12v8_inspector18V8RuntimeAgentImplE,"awG",@progbits,_ZTVN12v8_inspector18V8RuntimeAgentImplE,comdat
	.align 8
	.type	_ZTVN12v8_inspector18V8RuntimeAgentImplE, @object
	.size	_ZTVN12v8_inspector18V8RuntimeAgentImplE, 200
_ZTVN12v8_inspector18V8RuntimeAgentImplE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector18V8RuntimeAgentImplD1Ev
	.quad	_ZN12v8_inspector18V8RuntimeAgentImplD0Ev
	.quad	_ZN12v8_inspector18V8RuntimeAgentImpl12awaitPromiseERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEES8_St10unique_ptrINS_8protocol7Runtime7Backend20AwaitPromiseCallbackESt14default_deleteISD_EE
	.quad	_ZN12v8_inspector18V8RuntimeAgentImpl14callFunctionOnERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS1_EENS6_8PtrMaybeISt6vectorISt10unique_ptrINS_8protocol7Runtime12CallArgumentESt14default_deleteISE_EESaISH_EEEENS7_IbEESL_SL_SL_SL_NS7_IiEES8_SB_INSD_7Backend22CallFunctionOnCallbackESF_ISO_EE
	.quad	_ZN12v8_inspector18V8RuntimeAgentImpl13compileScriptERKNS_8String16ES3_bN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIiEEPNS7_IS1_EEPNS6_8PtrMaybeINS_8protocol7Runtime16ExceptionDetailsEEE
	.quad	_ZN12v8_inspector18V8RuntimeAgentImpl7disableEv
	.quad	_ZN12v8_inspector18V8RuntimeAgentImpl21discardConsoleEntriesEv
	.quad	_ZN12v8_inspector18V8RuntimeAgentImpl6enableEv
	.quad	_ZN12v8_inspector18V8RuntimeAgentImpl8evaluateERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS1_EENS7_IbEES9_NS7_IiEES9_S9_S9_S9_S9_NS7_IdEESt10unique_ptrINS_8protocol7Runtime7Backend16EvaluateCallbackESt14default_deleteISG_EE
	.quad	_ZN12v8_inspector18V8RuntimeAgentImpl12getIsolateIdEPNS_8String16E
	.quad	_ZN12v8_inspector18V8RuntimeAgentImpl12getHeapUsageEPdS1_
	.quad	_ZN12v8_inspector18V8RuntimeAgentImpl13getPropertiesERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEES8_S8_PSt10unique_ptrISt6vectorIS9_INS_8protocol7Runtime18PropertyDescriptorESt14default_deleteISD_EESaISG_EESE_ISI_EEPNS6_8PtrMaybeISA_IS9_INSC_26InternalPropertyDescriptorESE_ISN_EESaISP_EEEEPNSM_ISA_IS9_INSC_25PrivatePropertyDescriptorESE_ISU_EESaISW_EEEEPNSM_INSC_16ExceptionDetailsEEE
	.quad	_ZN12v8_inspector18V8RuntimeAgentImpl23globalLexicalScopeNamesEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIiEEPSt10unique_ptrISt6vectorINS_8String16ESaIS8_EESt14default_deleteISA_EE
	.quad	_ZN12v8_inspector18V8RuntimeAgentImpl12queryObjectsERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS1_EEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISC_EE
	.quad	_ZN12v8_inspector18V8RuntimeAgentImpl13releaseObjectERKNS_8String16E
	.quad	_ZN12v8_inspector18V8RuntimeAgentImpl18releaseObjectGroupERKNS_8String16E
	.quad	_ZN12v8_inspector18V8RuntimeAgentImpl23runIfWaitingForDebuggerEv
	.quad	_ZN12v8_inspector18V8RuntimeAgentImpl9runScriptERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIiEENS7_IS1_EENS7_IbEESA_SA_SA_SA_St10unique_ptrINS_8protocol7Runtime7Backend17RunScriptCallbackESt14default_deleteISF_EE
	.quad	_ZN12v8_inspector18V8RuntimeAgentImpl31setCustomObjectFormatterEnabledEb
	.quad	_ZN12v8_inspector18V8RuntimeAgentImpl28setMaxCallStackSizeToCaptureEi
	.quad	_ZN12v8_inspector18V8RuntimeAgentImpl18terminateExecutionESt10unique_ptrINS_8protocol7Runtime7Backend26TerminateExecutionCallbackESt14default_deleteIS5_EE
	.quad	_ZN12v8_inspector18V8RuntimeAgentImpl10addBindingERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIiEE
	.quad	_ZN12v8_inspector18V8RuntimeAgentImpl13removeBindingERKNS_8String16E
	.section	.data.rel.ro.local._ZTVN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend16EvaluateCallbackEEE,"aw"
	.align 8
	.type	_ZTVN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend16EvaluateCallbackEEE, @object
	.size	_ZTVN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend16EvaluateCallbackEEE, 48
_ZTVN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend16EvaluateCallbackEEE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend16EvaluateCallbackEE11sendSuccessESt10unique_ptrINS3_12RemoteObjectESt14default_deleteIS8_EEN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeINS3_16ExceptionDetailsEEE
	.quad	_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend16EvaluateCallbackEE11sendFailureERKNS2_16DispatchResponseE
	.quad	_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend16EvaluateCallbackEED1Ev
	.quad	_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend16EvaluateCallbackEED0Ev
	.section	.data.rel.ro.local._ZTVN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend20AwaitPromiseCallbackEEE,"aw"
	.align 8
	.type	_ZTVN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend20AwaitPromiseCallbackEEE, @object
	.size	_ZTVN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend20AwaitPromiseCallbackEEE, 48
_ZTVN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend20AwaitPromiseCallbackEEE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend20AwaitPromiseCallbackEE11sendSuccessESt10unique_ptrINS3_12RemoteObjectESt14default_deleteIS8_EEN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeINS3_16ExceptionDetailsEEE
	.quad	_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend20AwaitPromiseCallbackEE11sendFailureERKNS2_16DispatchResponseE
	.quad	_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend20AwaitPromiseCallbackEED1Ev
	.quad	_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend20AwaitPromiseCallbackEED0Ev
	.section	.data.rel.ro.local._ZTVN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend17RunScriptCallbackEEE,"aw"
	.align 8
	.type	_ZTVN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend17RunScriptCallbackEEE, @object
	.size	_ZTVN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend17RunScriptCallbackEEE, 48
_ZTVN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend17RunScriptCallbackEEE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend17RunScriptCallbackEE11sendSuccessESt10unique_ptrINS3_12RemoteObjectESt14default_deleteIS8_EEN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeINS3_16ExceptionDetailsEEE
	.quad	_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend17RunScriptCallbackEE11sendFailureERKNS2_16DispatchResponseE
	.quad	_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend17RunScriptCallbackEED1Ev
	.quad	_ZN12v8_inspector12_GLOBAL__N_123EvaluateCallbackWrapperINS_8protocol7Runtime7Backend17RunScriptCallbackEED0Ev
	.section	.bss._ZZN12v8_inspector18V8RuntimeAgentImpl8evaluateERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS1_EENS7_IbEES9_NS7_IiEES9_S9_S9_S9_S9_NS7_IdEESt10unique_ptrINS_8protocol7Runtime7Backend16EvaluateCallbackESt14default_deleteISG_EEE28trace_event_unique_atomic239,"aw",@nobits
	.align 8
	.type	_ZZN12v8_inspector18V8RuntimeAgentImpl8evaluateERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS1_EENS7_IbEES9_NS7_IiEES9_S9_S9_S9_S9_NS7_IdEESt10unique_ptrINS_8protocol7Runtime7Backend16EvaluateCallbackESt14default_deleteISG_EEE28trace_event_unique_atomic239, @object
	.size	_ZZN12v8_inspector18V8RuntimeAgentImpl8evaluateERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS1_EENS7_IbEES9_NS7_IiEES9_S9_S9_S9_S9_NS7_IdEESt10unique_ptrINS_8protocol7Runtime7Backend16EvaluateCallbackESt14default_deleteISG_EEE28trace_event_unique_atomic239, 8
_ZZN12v8_inspector18V8RuntimeAgentImpl8evaluateERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS1_EENS7_IbEES9_NS7_IiEES9_S9_S9_S9_S9_NS7_IdEESt10unique_ptrINS_8protocol7Runtime7Backend16EvaluateCallbackESt14default_deleteISG_EEE28trace_event_unique_atomic239:
	.zero	8
	.section	.rodata._ZN12v8_inspector23V8RuntimeAgentImplStateL8bindingsE,"a"
	.align 8
	.type	_ZN12v8_inspector23V8RuntimeAgentImplStateL8bindingsE, @object
	.size	_ZN12v8_inspector23V8RuntimeAgentImplStateL8bindingsE, 9
_ZN12v8_inspector23V8RuntimeAgentImplStateL8bindingsE:
	.string	"bindings"
	.section	.rodata._ZN12v8_inspector23V8RuntimeAgentImplStateL14runtimeEnabledE,"a"
	.align 8
	.type	_ZN12v8_inspector23V8RuntimeAgentImplStateL14runtimeEnabledE, @object
	.size	_ZN12v8_inspector23V8RuntimeAgentImplStateL14runtimeEnabledE, 15
_ZN12v8_inspector23V8RuntimeAgentImplStateL14runtimeEnabledE:
	.string	"runtimeEnabled"
	.section	.rodata._ZN12v8_inspector23V8RuntimeAgentImplStateL28customObjectFormatterEnabledE,"a"
	.align 16
	.type	_ZN12v8_inspector23V8RuntimeAgentImplStateL28customObjectFormatterEnabledE, @object
	.size	_ZN12v8_inspector23V8RuntimeAgentImplStateL28customObjectFormatterEnabledE, 29
_ZN12v8_inspector23V8RuntimeAgentImplStateL28customObjectFormatterEnabledE:
	.string	"customObjectFormatterEnabled"
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC15:
	.long	0
	.long	1083129856
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
