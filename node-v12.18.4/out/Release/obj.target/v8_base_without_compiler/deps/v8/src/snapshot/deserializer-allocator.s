	.file	"deserializer-allocator.cc"
	.text
	.section	.rodata._ZN2v88internal21DeserializerAllocator11AllocateRawENS0_13SnapshotSpaceEi.part.0.str1.1,"aMS",@progbits,1
.LC0:
	.string	"!IsRetry()"
.LC1:
	.string	"Check failed: %s."
.LC2:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZN2v88internal21DeserializerAllocator11AllocateRawENS0_13SnapshotSpaceEi.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal21DeserializerAllocator11AllocateRawENS0_13SnapshotSpaceEi.part.0, @function
_ZN2v88internal21DeserializerAllocator11AllocateRawENS0_13SnapshotSpaceEi.part.0:
.LFB24619:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	256(%rdi), %rax
	leaq	200(%rax), %r15
	lock addq	$1, (%r15)
	movq	256(%rdi), %rax
	movq	280(%rax), %rdi
	call	_ZN2v88internal16LargeObjectSpace11AllocateRawEi@PLT
	movq	%rax, %r13
	notq	%r13
	andl	$1, %r13d
	jne	.L31
	movq	240(%rbx), %r14
	movq	%rax, %r12
	cmpq	248(%rbx), %r14
	je	.L3
	movq	%rax, (%r14)
	addq	$8, 240(%rbx)
.L4:
	lock subq	$1, (%r15)
	addq	$40, %rsp
	leaq	-1(%r12), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	.cfi_restore_state
	movq	232(%rbx), %r8
	movq	%r14, %rcx
	movabsq	$1152921504606846975, %rdi
	subq	%r8, %rcx
	movq	%rcx, %rax
	sarq	$3, %rax
	cmpq	%rdi, %rax
	je	.L32
	testq	%rax, %rax
	je	.L14
	movabsq	$9223372036854775800, %rsi
	leaq	(%rax,%rax), %r9
	cmpq	%r9, %rax
	jbe	.L33
.L6:
	movq	%rsi, %rdi
	movq	%rcx, -72(%rbp)
	movq	%r8, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %r8
	movq	-72(%rbp), %rcx
	movq	%rax, %rdx
	addq	%rax, %rsi
	leaq	8(%rax), %rax
.L7:
	movq	%r12, (%rdx,%rcx)
	cmpq	%r8, %r14
	je	.L8
	leaq	-8(%r14), %rdi
	leaq	15(%rdx), %rax
	subq	%r8, %rdi
	subq	%r8, %rax
	movq	%rdi, %r9
	shrq	$3, %r9
	cmpq	$30, %rax
	jbe	.L17
	movabsq	$2305843009213693948, %rax
	testq	%rax, %r9
	je	.L17
	addq	$1, %r9
	movq	%r9, %rax
	shrq	%rax
	salq	$4, %rax
	.p2align 4,,10
	.p2align 3
.L10:
	movdqu	(%r8,%r13), %xmm1
	movups	%xmm1, (%rdx,%r13)
	addq	$16, %r13
	cmpq	%rax, %r13
	jne	.L10
	movq	%r9, %r10
	andq	$-2, %r10
	leaq	0(,%r10,8), %rcx
	leaq	(%r8,%rcx), %rax
	addq	%rdx, %rcx
	cmpq	%r10, %r9
	je	.L12
	movq	(%rax), %rax
	movq	%rax, (%rcx)
.L12:
	leaq	16(%rdx,%rdi), %rax
.L8:
	testq	%r8, %r8
	je	.L13
	movq	%r8, %rdi
	movq	%rdx, -72(%rbp)
	movq	%rax, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-72(%rbp), %rdx
	movq	-64(%rbp), %rax
	movq	-56(%rbp), %rsi
.L13:
	movq	%rdx, %xmm0
	movq	%rax, %xmm2
	movq	%rsi, 248(%rbx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 232(%rbx)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L31:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L33:
	testq	%r9, %r9
	jne	.L34
	movl	$8, %eax
	xorl	%esi, %esi
	xorl	%edx, %edx
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L14:
	movl	$8, %esi
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L17:
	movq	%rdx, %rcx
	movq	%r8, %rax
	.p2align 4,,10
	.p2align 3
.L9:
	movq	(%rax), %r9
	addq	$8, %rax
	addq	$8, %rcx
	movq	%r9, -8(%rcx)
	cmpq	%rax, %r14
	jne	.L9
	jmp	.L12
.L32:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L34:
	cmpq	%rdi, %r9
	movq	%rdi, %rsi
	cmovbe	%r9, %rsi
	salq	$3, %rsi
	jmp	.L6
	.cfi_endproc
.LFE24619:
	.size	_ZN2v88internal21DeserializerAllocator11AllocateRawENS0_13SnapshotSpaceEi.part.0, .-_ZN2v88internal21DeserializerAllocator11AllocateRawENS0_13SnapshotSpaceEi.part.0
	.section	.text._ZN2v88internal21DeserializerAllocator11AllocateRawENS0_13SnapshotSpaceEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21DeserializerAllocator11AllocateRawENS0_13SnapshotSpaceEi
	.type	_ZN2v88internal21DeserializerAllocator11AllocateRawENS0_13SnapshotSpaceEi, @function
_ZN2v88internal21DeserializerAllocator11AllocateRawENS0_13SnapshotSpaceEi:
.LFB19575:
	.cfi_startproc
	endbr64
	cmpl	$5, %esi
	je	.L43
	cmpl	$4, %esi
	je	.L44
	movslq	%esi, %rax
	movslq	%edx, %rdx
	leaq	(%rdi,%rax,8), %rcx
	movq	160(%rcx), %rax
	addq	%rax, %rdx
	movq	%rdx, 160(%rcx)
	cmpl	$3, %esi
	je	.L45
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	movl	200(%rdi), %eax
	leal	1(%rax), %edx
	movl	%edx, 200(%rdi)
	movq	208(%rdi), %rdx
	movq	(%rdx,%rax,8), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	movl	%edx, %esi
	jmp	_ZN2v88internal21DeserializerAllocator11AllocateRawENS0_13SnapshotSpaceEi.part.0
	.p2align 4,,10
	.p2align 3
.L45:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rax, %rdx
	movq	%rax, %rsi
	andq	$-262144, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	272(%rdx), %rdi
	movq	%rax, -8(%rbp)
	call	_ZN2v88internal18CodeObjectRegistry32RegisterNewlyAllocatedCodeObjectEm@PLT
	movq	-8(%rbp), %rax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19575:
	.size	_ZN2v88internal21DeserializerAllocator11AllocateRawENS0_13SnapshotSpaceEi, .-_ZN2v88internal21DeserializerAllocator11AllocateRawENS0_13SnapshotSpaceEi
	.section	.text._ZN2v88internal21DeserializerAllocator8AllocateENS0_13SnapshotSpaceEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21DeserializerAllocator8AllocateENS0_13SnapshotSpaceEi
	.type	_ZN2v88internal21DeserializerAllocator8AllocateENS0_13SnapshotSpaceEi, @function
_ZN2v88internal21DeserializerAllocator8AllocateENS0_13SnapshotSpaceEi:
.LFB19576:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$24, %rsp
	movl	192(%rdi), %edi
	testl	%edi, %edi
	jne	.L55
	cmpl	$5, %esi
	je	.L56
	cmpl	$4, %esi
	je	.L57
	movslq	%esi, %rax
	movslq	%edx, %rdx
	leaq	(%r12,%rax,8), %rcx
	movq	160(%rcx), %rax
	addq	%rax, %rdx
	movq	%rdx, 160(%rcx)
	cmpl	$3, %esi
	je	.L58
.L46:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	.cfi_restore_state
	movl	200(%r12), %eax
	leal	1(%rax), %edx
	movl	%edx, 200(%r12)
	movq	208(%r12), %rdx
	movq	(%rdx,%rax,8), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	.cfi_restore_state
	call	_ZN2v88internal4Heap21GetMaximumFillToAlignENS0_19AllocationAlignmentE@PLT
	leal	(%rax,%r13), %r15d
	cmpl	$5, %ebx
	je	.L59
	cmpl	$4, %ebx
	je	.L60
	movslq	%ebx, %rax
	leaq	(%r12,%rax,8), %rdx
	movslq	%r15d, %rax
	movq	160(%rdx), %r14
	addq	%r14, %rax
	movq	%rax, 160(%rdx)
	cmpl	$3, %ebx
	je	.L61
.L49:
	movq	256(%r12), %rdi
	leaq	1(%r14), %rsi
	movl	%r15d, %ecx
	movl	%r13d, %edx
	movl	192(%r12), %r8d
	call	_ZN2v88internal4Heap15AlignWithFillerENS0_10HeapObjectEiiNS0_19AllocationAlignmentE@PLT
	movl	$0, 192(%r12)
	addq	$24, %rsp
	popq	%rbx
	subq	$1, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	.cfi_restore_state
	movl	200(%r12), %eax
	leal	1(%rax), %edx
	movl	%edx, 200(%r12)
	movq	208(%r12), %rdx
	movq	(%rdx,%rax,8), %r14
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L56:
	addq	$24, %rsp
	movq	%r12, %rdi
	movl	%edx, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal21DeserializerAllocator11AllocateRawENS0_13SnapshotSpaceEi.part.0
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal21DeserializerAllocator11AllocateRawENS0_13SnapshotSpaceEi.part.0
	movq	%rax, %r14
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L58:
	movq	%rax, %rdx
	movq	%rax, %rsi
	movq	%rax, -56(%rbp)
	andq	$-262144, %rdx
	movq	272(%rdx), %rdi
	call	_ZN2v88internal18CodeObjectRegistry32RegisterNewlyAllocatedCodeObjectEm@PLT
	movq	-56(%rbp), %rax
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L61:
	movq	%r14, %rax
	movq	%r14, %rsi
	andq	$-262144, %rax
	movq	272(%rax), %rdi
	call	_ZN2v88internal18CodeObjectRegistry32RegisterNewlyAllocatedCodeObjectEm@PLT
	jmp	.L49
	.cfi_endproc
.LFE19576:
	.size	_ZN2v88internal21DeserializerAllocator8AllocateENS0_13SnapshotSpaceEi, .-_ZN2v88internal21DeserializerAllocator8AllocateENS0_13SnapshotSpaceEi
	.section	.rodata._ZN2v88internal21DeserializerAllocator15MoveToNextChunkENS0_13SnapshotSpaceE.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"reservation[chunk_index].end == high_water_[space_number]"
	.align 8
.LC4:
	.string	"chunk_index < reservation.size()"
	.section	.text._ZN2v88internal21DeserializerAllocator15MoveToNextChunkENS0_13SnapshotSpaceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21DeserializerAllocator15MoveToNextChunkENS0_13SnapshotSpaceE
	.type	_ZN2v88internal21DeserializerAllocator15MoveToNextChunkENS0_13SnapshotSpaceE, @function
_ZN2v88internal21DeserializerAllocator15MoveToNextChunkENS0_13SnapshotSpaceE:
.LFB19577:
	.cfi_startproc
	endbr64
	movslq	%esi, %rsi
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	(%rdi,%rsi,4), %r8
	leaq	(%rsi,%rsi,2), %rdx
	movl	144(%r8), %eax
	leaq	(%rdi,%rdx,8), %rcx
	leaq	(%rdi,%rsi,8), %rsi
	movq	(%rcx), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	%eax, %edx
	leaq	(%rdx,%rdx,2), %rdx
	leaq	(%rdi,%rdx,8), %rdx
	movq	16(%rdx), %rdi
	cmpq	%rdi, 160(%rsi)
	jne	.L66
	addl	$1, %eax
	movl	%eax, 144(%r8)
	movq	(%rcx), %rdi
	movq	8(%rcx), %rdx
	movabsq	$-6148914691236517205, %rcx
	subq	%rdi, %rdx
	sarq	$3, %rdx
	imulq	%rcx, %rdx
	cmpq	%rdx, %rax
	jnb	.L67
	leaq	(%rax,%rax,2), %rax
	movq	8(%rdi,%rax,8), %rax
	movq	%rax, 160(%rsi)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_restore_state
	leaq	.LC3(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L67:
	leaq	.LC4(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19577:
	.size	_ZN2v88internal21DeserializerAllocator15MoveToNextChunkENS0_13SnapshotSpaceE, .-_ZN2v88internal21DeserializerAllocator15MoveToNextChunkENS0_13SnapshotSpaceE
	.section	.text._ZN2v88internal21DeserializerAllocator6GetMapEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21DeserializerAllocator6GetMapEj
	.type	_ZN2v88internal21DeserializerAllocator6GetMapEj, @function
_ZN2v88internal21DeserializerAllocator6GetMapEj:
.LFB19578:
	.cfi_startproc
	endbr64
	movq	208(%rdi), %rax
	movl	%esi, %esi
	movq	(%rax,%rsi,8), %rax
	addq	$1, %rax
	ret
	.cfi_endproc
.LFE19578:
	.size	_ZN2v88internal21DeserializerAllocator6GetMapEj, .-_ZN2v88internal21DeserializerAllocator6GetMapEj
	.section	.text._ZN2v88internal21DeserializerAllocator14GetLargeObjectEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21DeserializerAllocator14GetLargeObjectEj
	.type	_ZN2v88internal21DeserializerAllocator14GetLargeObjectEj, @function
_ZN2v88internal21DeserializerAllocator14GetLargeObjectEj:
.LFB19579:
	.cfi_startproc
	endbr64
	movq	232(%rdi), %rax
	movl	%esi, %esi
	movq	(%rax,%rsi,8), %rax
	ret
	.cfi_endproc
.LFE19579:
	.size	_ZN2v88internal21DeserializerAllocator14GetLargeObjectEj, .-_ZN2v88internal21DeserializerAllocator14GetLargeObjectEj
	.section	.text._ZN2v88internal21DeserializerAllocator9GetObjectENS0_13SnapshotSpaceEjj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21DeserializerAllocator9GetObjectENS0_13SnapshotSpaceEjj
	.type	_ZN2v88internal21DeserializerAllocator9GetObjectENS0_13SnapshotSpaceEjj, @function
_ZN2v88internal21DeserializerAllocator9GetObjectENS0_13SnapshotSpaceEjj:
.LFB19580:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rsi
	movl	%edx, %edx
	movl	%ecx, %ecx
	leaq	(%rsi,%rsi,2), %rsi
	leaq	(%rdx,%rdx,2), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi,%rsi,8), %rdx
	movl	192(%rdi), %esi
	leaq	(%rdx,%rax,8), %rax
	movq	8(%rax), %rbx
	addq	%rcx, %rbx
	testl	%esi, %esi
	jne	.L76
	leaq	1(%rbx), %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	.cfi_restore_state
	movq	%rdi, %r12
	movq	%rbx, %rdi
	call	_ZN2v88internal4Heap14GetFillToAlignEmNS0_19AllocationAlignmentE@PLT
	movl	$0, 192(%r12)
	cltq
	addq	%rax, %rbx
	leaq	1(%rbx), %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19580:
	.size	_ZN2v88internal21DeserializerAllocator9GetObjectENS0_13SnapshotSpaceEjj, .-_ZN2v88internal21DeserializerAllocator9GetObjectENS0_13SnapshotSpaceEjj
	.section	.text._ZN2v88internal21DeserializerAllocator12ReserveSpaceEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21DeserializerAllocator12ReserveSpaceEv
	.type	_ZN2v88internal21DeserializerAllocator12ReserveSpaceEv, @function
_ZN2v88internal21DeserializerAllocator12ReserveSpaceEv:
.LFB19582:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	208(%rdi), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rbx, %rsi
	subq	$8, %rsp
	movq	256(%rdi), %rdi
	call	_ZN2v88internal4Heap12ReserveSpaceEPSt6vectorINS1_5ChunkESaIS3_EEPS2_ImSaImEE@PLT
	testb	%al, %al
	je	.L77
	movq	(%rbx), %rdx
	movq	8(%rdx), %rdx
	movq	%rdx, 160(%rbx)
	movq	24(%rbx), %rdx
	movq	8(%rdx), %rdx
	movq	%rdx, 168(%rbx)
	movq	48(%rbx), %rdx
	movq	8(%rdx), %rdx
	movq	%rdx, 176(%rbx)
	movq	72(%rbx), %rdx
	movq	8(%rdx), %rdx
	movq	%rdx, 184(%rbx)
.L77:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19582:
	.size	_ZN2v88internal21DeserializerAllocator12ReserveSpaceEv, .-_ZN2v88internal21DeserializerAllocator12ReserveSpaceEv
	.section	.text._ZNK2v88internal21DeserializerAllocator24ReservationsAreFullyUsedEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal21DeserializerAllocator24ReservationsAreFullyUsedEv
	.type	_ZNK2v88internal21DeserializerAllocator24ReservationsAreFullyUsedEv, @function
_ZNK2v88internal21DeserializerAllocator24ReservationsAreFullyUsedEv:
.LFB19583:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rsi
	movq	8(%rdi), %rax
	movabsq	$-6148914691236517205, %rcx
	movl	144(%rdi), %edx
	subq	%rsi, %rax
	sarq	$3, %rax
	leal	1(%rdx), %r8d
	imulq	%rcx, %rax
	cmpq	%rax, %r8
	jne	.L92
	leaq	(%rdx,%rdx,2), %rax
	movq	16(%rsi,%rax,8), %rax
	cmpq	%rax, 160(%rdi)
	jne	.L92
	movq	24(%rdi), %rsi
	movq	32(%rdi), %rax
	movl	148(%rdi), %edx
	subq	%rsi, %rax
	sarq	$3, %rax
	leal	1(%rdx), %r8d
	imulq	%rcx, %rax
	cmpq	%rax, %r8
	jne	.L92
	leaq	(%rdx,%rdx,2), %rax
	movq	168(%rdi), %rdx
	cmpq	%rdx, 16(%rsi,%rax,8)
	jne	.L92
	movq	48(%rdi), %rsi
	movq	56(%rdi), %rax
	movl	152(%rdi), %edx
	subq	%rsi, %rax
	sarq	$3, %rax
	leal	1(%rdx), %r8d
	imulq	%rcx, %rax
	cmpq	%rax, %r8
	jne	.L92
	leaq	(%rdx,%rdx,2), %rax
	movq	176(%rdi), %rdx
	cmpq	%rdx, 16(%rsi,%rax,8)
	jne	.L92
	movq	72(%rdi), %rsi
	movq	80(%rdi), %rax
	movl	156(%rdi), %edx
	subq	%rsi, %rax
	sarq	$3, %rax
	leal	1(%rdx), %r8d
	imulq	%rcx, %rax
	cmpq	%rax, %r8
	jne	.L92
	leaq	(%rdx,%rdx,2), %rax
	movq	184(%rdi), %rcx
	cmpq	%rcx, 16(%rsi,%rax,8)
	jne	.L92
	movq	216(%rdi), %rax
	movl	200(%rdi), %edx
	subq	208(%rdi), %rax
	sarq	$3, %rax
	cmpq	%rdx, %rax
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE19583:
	.size	_ZNK2v88internal21DeserializerAllocator24ReservationsAreFullyUsedEv, .-_ZNK2v88internal21DeserializerAllocator24ReservationsAreFullyUsedEv
	.section	.text._ZN2v88internal21DeserializerAllocator45RegisterDeserializedObjectsForBlackAllocationEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21DeserializerAllocator45RegisterDeserializedObjectsForBlackAllocationEv
	.type	_ZN2v88internal21DeserializerAllocator45RegisterDeserializedObjectsForBlackAllocationEv, @function
_ZN2v88internal21DeserializerAllocator45RegisterDeserializedObjectsForBlackAllocationEv:
.LFB19584:
	.cfi_startproc
	endbr64
	movq	%rdi, %rsi
	leaq	208(%rdi), %rcx
	leaq	232(%rdi), %rdx
	movq	256(%rdi), %rdi
	jmp	_ZN2v88internal4Heap45RegisterDeserializedObjectsForBlackAllocationEPSt6vectorINS1_5ChunkESaIS3_EERKS2_INS0_10HeapObjectESaIS7_EERKS2_ImSaImEE@PLT
	.cfi_endproc
.LFE19584:
	.size	_ZN2v88internal21DeserializerAllocator45RegisterDeserializedObjectsForBlackAllocationEv, .-_ZN2v88internal21DeserializerAllocator45RegisterDeserializedObjectsForBlackAllocationEv
	.section	.text._ZNSt6vectorIN2v88internal4Heap5ChunkESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal4Heap5ChunkESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal4Heap5ChunkESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal4Heap5ChunkESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, @function
_ZNSt6vectorIN2v88internal4Heap5ChunkESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_:
.LFB23689:
	.cfi_startproc
	endbr64
	movabsq	$384307168202282325, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	movabsq	$-6148914691236517205, %rdx
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r9
	movq	(%rdi), %r8
	movq	%r9, %rax
	subq	%r8, %rax
	sarq	$3, %rax
	imulq	%rdx, %rax
	cmpq	%rcx, %rax
	je	.L108
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r13
	subq	%r8, %rdx
	testq	%rax, %rax
	je	.L104
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L109
.L96:
	movq	%r14, %rdi
	movq	%rdx, -72(%rbp)
	movq	%r9, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %rdx
	movq	%rax, %rbx
	addq	%rax, %r14
.L103:
	movq	16(%r15), %rax
	movdqu	(%r15), %xmm1
	subq	%r13, %r9
	leaq	24(%rbx,%rdx), %r10
	movq	%r9, %r15
	movq	%rax, 16(%rbx,%rdx)
	leaq	(%r10,%r9), %rax
	movq	%rax, -56(%rbp)
	movups	%xmm1, (%rbx,%rdx)
	testq	%rdx, %rdx
	jg	.L110
	testq	%r9, %r9
	jg	.L99
	testq	%r8, %r8
	jne	.L102
.L100:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L110:
	.cfi_restore_state
	movq	%r8, %rsi
	movq	%rbx, %rdi
	movq	%r10, -72(%rbp)
	movq	%r8, -64(%rbp)
	call	memmove@PLT
	testq	%r15, %r15
	movq	-64(%rbp), %r8
	movq	-72(%rbp), %r10
	jg	.L99
.L102:
	movq	%r8, %rdi
	call	_ZdlPv@PLT
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L109:
	testq	%rsi, %rsi
	jne	.L97
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L99:
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r10, %rdi
	movq	%r8, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r8
	testq	%r8, %r8
	je	.L100
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L104:
	movl	$24, %r14d
	jmp	.L96
.L108:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L97:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	imulq	$24, %rcx, %r14
	jmp	.L96
	.cfi_endproc
.LFE23689:
	.size	_ZNSt6vectorIN2v88internal4Heap5ChunkESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, .-_ZNSt6vectorIN2v88internal4Heap5ChunkESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.section	.text._ZN2v88internal21DeserializerAllocator17DecodeReservationERKSt6vectorINS0_14SerializedData11ReservationESaIS4_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21DeserializerAllocator17DecodeReservationERKSt6vectorINS0_14SerializedData11ReservationESaIS4_EE
	.type	_ZN2v88internal21DeserializerAllocator17DecodeReservationERKSt6vectorINS0_14SerializedData11ReservationESaIS4_EE, @function
_ZN2v88internal21DeserializerAllocator17DecodeReservationERKSt6vectorINS0_14SerializedData11ReservationESaIS4_EE:
.LFB19581:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -48
	movq	8(%rsi), %r14
	movq	(%rsi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	%r14, %rbx
	jne	.L116
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L124:
	movdqa	-64(%rbp), %xmm1
	movups	%xmm1, (%rsi)
	movq	-48(%rbp), %rax
	movq	%rax, 16(%rsi)
	addq	$24, 8(%rdi)
.L114:
	cmpl	$-2147483648, (%rbx)
	sbbl	$-1, %r12d
	addq	$4, %rbx
	cmpq	%rbx, %r14
	je	.L117
.L116:
	movl	(%rbx), %eax
	movups	%xmm0, -56(%rbp)
	andl	$2147483647, %eax
	movl	%eax, -64(%rbp)
	movslq	%r12d, %rax
	leaq	(%rax,%rax,2), %rax
	leaq	0(%r13,%rax,8), %rdi
	movq	8(%rdi), %rsi
	cmpq	16(%rdi), %rsi
	jne	.L124
	leaq	-64(%rbp), %rdx
	call	_ZNSt6vectorIN2v88internal4Heap5ChunkESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	pxor	%xmm0, %xmm0
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L117:
	pxor	%xmm0, %xmm0
	movups	%xmm0, 144(%r13)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L125
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L125:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19581:
	.size	_ZN2v88internal21DeserializerAllocator17DecodeReservationERKSt6vectorINS0_14SerializedData11ReservationESaIS4_EE, .-_ZN2v88internal21DeserializerAllocator17DecodeReservationERKSt6vectorINS0_14SerializedData11ReservationESaIS4_EE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal21DeserializerAllocator11AllocateRawENS0_13SnapshotSpaceEi,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal21DeserializerAllocator11AllocateRawENS0_13SnapshotSpaceEi, @function
_GLOBAL__sub_I__ZN2v88internal21DeserializerAllocator11AllocateRawENS0_13SnapshotSpaceEi:
.LFB24562:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE24562:
	.size	_GLOBAL__sub_I__ZN2v88internal21DeserializerAllocator11AllocateRawENS0_13SnapshotSpaceEi, .-_GLOBAL__sub_I__ZN2v88internal21DeserializerAllocator11AllocateRawENS0_13SnapshotSpaceEi
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal21DeserializerAllocator11AllocateRawENS0_13SnapshotSpaceEi
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
