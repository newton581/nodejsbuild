	.file	"signature-map.cc"
	.text
	.section	.text._ZNKSt10_HashtableIN2v88internal9SignatureINS1_4wasm9ValueTypeEEESt4pairIKS5_jESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ENS0_4base4hashIS5_EENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmRS7_m,"axG",@progbits,_ZNKSt10_HashtableIN2v88internal9SignatureINS1_4wasm9ValueTypeEEESt4pairIKS5_jESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ENS0_4base4hashIS5_EENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmRS7_m,comdat
	.align 2
	.p2align 4
	.weak	_ZNKSt10_HashtableIN2v88internal9SignatureINS1_4wasm9ValueTypeEEESt4pairIKS5_jESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ENS0_4base4hashIS5_EENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmRS7_m
	.type	_ZNKSt10_HashtableIN2v88internal9SignatureINS1_4wasm9ValueTypeEEESt4pairIKS5_jESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ENS0_4base4hashIS5_EENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmRS7_m, @function
_ZNKSt10_HashtableIN2v88internal9SignatureINS1_4wasm9ValueTypeEEESt4pairIKS5_jESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ENS0_4base4hashIS5_EENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmRS7_m:
.LFB5073:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	(%rax,%rsi,8), %r10
	testq	%r10, %r10
	je	.L12
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	(%r10), %rax
	movq	40(%rax), %r8
	cmpq	%rcx, %r8
	je	.L16
	.p2align 4,,10
	.p2align 3
.L3:
	movq	(%rax), %r9
	testq	%r9, %r9
	je	.L6
	movq	40(%r9), %r8
	movq	%rax, %r10
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	8(%rdi)
	cmpq	%rdx, %rsi
	jne	.L6
	movq	%r9, %rax
	cmpq	%rcx, %r8
	jne	.L3
.L16:
	leaq	8(%rax), %rdx
	cmpq	%rdx, %r11
	je	.L1
	movq	8(%r11), %r9
	cmpq	16(%rax), %r9
	jne	.L3
	movq	(%r11), %rbx
	cmpq	8(%rax), %rbx
	jne	.L3
	movq	16(%r11), %rdx
	addq	%rbx, %r9
	movq	24(%rax), %r8
	addq	%rdx, %r9
	cmpq	%r9, %rdx
	je	.L1
	.p2align 4,,10
	.p2align 3
.L4:
	movzbl	(%r8), %ebx
	cmpb	%bl, (%rdx)
	jne	.L3
	addq	$1, %rdx
	addq	$1, %r8
	cmpq	%rdx, %r9
	jne	.L4
.L1:
	movq	%r10, %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	xorl	%r10d, %r10d
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	%r10, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore 3
	.cfi_restore 6
	movq	%r10, %rax
	ret
	.cfi_endproc
.LFE5073:
	.size	_ZNKSt10_HashtableIN2v88internal9SignatureINS1_4wasm9ValueTypeEEESt4pairIKS5_jESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ENS0_4base4hashIS5_EENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmRS7_m, .-_ZNKSt10_HashtableIN2v88internal9SignatureINS1_4wasm9ValueTypeEEESt4pairIKS5_jESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ENS0_4base4hashIS5_EENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmRS7_m
	.section	.text._ZNK2v88internal4wasm12SignatureMap4FindERKNS0_9SignatureINS1_9ValueTypeEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4wasm12SignatureMap4FindERKNS0_9SignatureINS1_9ValueTypeEEE
	.type	_ZNK2v88internal4wasm12SignatureMap4FindERKNS0_9SignatureINS1_9ValueTypeEEE, @function
_ZNK2v88internal4wasm12SignatureMap4FindERKNS0_9SignatureINS1_9ValueTypeEEE:
.LFB4551:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	8(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	8(%rsi), %rdi
	movq	(%rsi), %rsi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	16(%r13), %r14
	movq	8(%r13), %r15
	movq	%rax, %rcx
	addq	%r14, %r15
	addq	0(%r13), %r15
	cmpq	%r15, %r14
	je	.L18
	.p2align 4,,10
	.p2align 3
.L19:
	movq	%rcx, %rdi
	addq	$1, %r14
	call	_ZN2v84base10hash_valueEm@PLT
	movzbl	-1(%r14), %esi
	xorl	%edi, %edi
	movq	%rax, -56(%rbp)
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	-56(%rbp), %r8
	movq	%rax, %rdi
	movq	%r8, %rsi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%rax, %rcx
	cmpq	%r15, %r14
	jne	.L19
.L18:
	movq	%rcx, %rax
	xorl	%edx, %edx
	movq	%r12, %rdi
	divq	16(%rbx)
	movq	%rdx, %rsi
	movq	%r13, %rdx
	call	_ZNKSt10_HashtableIN2v88internal9SignatureINS1_4wasm9ValueTypeEEESt4pairIKS5_jESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ENS0_4base4hashIS5_EENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmRS7_m
	movl	$-1, %r8d
	testq	%rax, %rax
	je	.L17
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L17
	movl	32(%rax), %r8d
.L17:
	addq	$24, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4551:
	.size	_ZNK2v88internal4wasm12SignatureMap4FindERKNS0_9SignatureINS1_9ValueTypeEEE, .-_ZNK2v88internal4wasm12SignatureMap4FindERKNS0_9SignatureINS1_9ValueTypeEEE
	.section	.text._ZNSt10_HashtableIN2v88internal9SignatureINS1_4wasm9ValueTypeEEESt4pairIKS5_jESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ENS0_4base4hashIS5_EENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJS6_IS5_jEEEES6_INSA_14_Node_iteratorIS8_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_,"axG",@progbits,_ZNSt10_HashtableIN2v88internal9SignatureINS1_4wasm9ValueTypeEEESt4pairIKS5_jESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ENS0_4base4hashIS5_EENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJS6_IS5_jEEEES6_INSA_14_Node_iteratorIS8_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIN2v88internal9SignatureINS1_4wasm9ValueTypeEEESt4pairIKS5_jESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ENS0_4base4hashIS5_EENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJS6_IS5_jEEEES6_INSA_14_Node_iteratorIS8_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_
	.type	_ZNSt10_HashtableIN2v88internal9SignatureINS1_4wasm9ValueTypeEEESt4pairIKS5_jESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ENS0_4base4hashIS5_EENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJS6_IS5_jEEEES6_INSA_14_Node_iteratorIS8_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_, @function
_ZNSt10_HashtableIN2v88internal9SignatureINS1_4wasm9ValueTypeEEESt4pairIKS5_jESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ENS0_4base4hashIS5_EENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJS6_IS5_jEEEES6_INSA_14_Node_iteratorIS8_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_:
.LFB5028:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	$48, %edi
	subq	$24, %rsp
	call	_Znwm@PLT
	movdqu	0(%r13), %xmm0
	movq	0(%r13), %rsi
	movq	%rax, %r12
	movq	$0, (%rax)
	movups	%xmm0, 8(%rax)
	movq	16(%r13), %rax
	movq	16(%r12), %rdi
	leaq	8(%r12), %r15
	movq	%rax, 24(%r12)
	movl	24(%r13), %eax
	movl	%eax, 32(%r12)
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	24(%r12), %r14
	movq	16(%r12), %rdx
	movq	%rax, %r13
	addq	%r14, %rdx
	addq	8(%r12), %rdx
	cmpq	%rdx, %r14
	je	.L28
	.p2align 4,,10
	.p2align 3
.L29:
	movq	%r13, %rdi
	movq	%rdx, -56(%rbp)
	addq	$1, %r14
	call	_ZN2v84base10hash_valueEm@PLT
	movzbl	-1(%r14), %esi
	xorl	%edi, %edi
	movq	%rax, %r13
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %r13
	cmpq	%rdx, %r14
	jne	.L29
.L28:
	movq	8(%rbx), %r8
	xorl	%edx, %edx
	movq	%r13, %rax
	movq	%r13, %rcx
	movq	%rbx, %rdi
	divq	%r8
	movq	%r8, -56(%rbp)
	movq	%rdx, %r14
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	_ZNKSt10_HashtableIN2v88internal9SignatureINS1_4wasm9ValueTypeEEESt4pairIKS5_jESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ENS0_4base4hashIS5_EENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmRS7_m
	movq	-56(%rbp), %r8
	testq	%rax, %rax
	je	.L30
	movq	(%rax), %r15
	testq	%r15, %r15
	je	.L30
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	addq	$24, %rsp
	movq	%r15, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	movq	24(%rbx), %rdx
	leaq	32(%rbx), %rdi
	movl	$1, %ecx
	movq	%r8, %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %r15
	testb	%al, %al
	jne	.L32
	movq	(%rbx), %r8
.L33:
	leaq	0(,%r14,8), %rcx
	movq	%r13, 40(%r12)
	leaq	(%r8,%rcx), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L42
	movq	(%rdx), %rdx
	movq	%rdx, (%r12)
	movq	(%rax), %rax
	movq	%r12, (%rax)
.L43:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r12, %rax
	movl	$1, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	cmpq	$1, %rdx
	je	.L63
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L64
	leaq	0(,%rdx,8), %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%rbx), %r10
	movq	%rax, %r8
.L35:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L37
	xorl	%edi, %edi
	leaq	16(%rbx), %r9
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L39:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L40:
	testq	%rsi, %rsi
	je	.L37
.L38:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	40(%rcx), %rax
	divq	%r15
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L39
	movq	16(%rbx), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L45
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L38
	.p2align 4,,10
	.p2align 3
.L37:
	movq	(%rbx), %rdi
	cmpq	%r10, %rdi
	je	.L41
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L41:
	movq	%r13, %rax
	xorl	%edx, %edx
	movq	%r15, 8(%rbx)
	divq	%r15
	movq	%r8, (%rbx)
	movq	%rdx, %r14
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L42:
	movq	16(%rbx), %rdx
	movq	%r12, 16(%rbx)
	movq	%rdx, (%r12)
	testq	%rdx, %rdx
	je	.L44
	movq	40(%rdx), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	%r12, (%r8,%rdx,8)
	movq	(%rbx), %rax
	addq	%rcx, %rax
.L44:
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax)
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L45:
	movq	%rdx, %rdi
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L63:
	leaq	48(%rbx), %r8
	movq	$0, 48(%rbx)
	movq	%r8, %r10
	jmp	.L35
.L64:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE5028:
	.size	_ZNSt10_HashtableIN2v88internal9SignatureINS1_4wasm9ValueTypeEEESt4pairIKS5_jESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ENS0_4base4hashIS5_EENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJS6_IS5_jEEEES6_INSA_14_Node_iteratorIS8_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_, .-_ZNSt10_HashtableIN2v88internal9SignatureINS1_4wasm9ValueTypeEEESt4pairIKS5_jESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ENS0_4base4hashIS5_EENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJS6_IS5_jEEEES6_INSA_14_Node_iteratorIS8_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_
	.section	.rodata._ZN2v88internal4wasm12SignatureMap12FindOrInsertERKNS0_9SignatureINS1_9ValueTypeEEE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"!frozen_"
.LC1:
	.string	"Check failed: %s."
.LC2:
	.string	"kMaxInt >= map_.size()"
	.section	.text._ZN2v88internal4wasm12SignatureMap12FindOrInsertERKNS0_9SignatureINS1_9ValueTypeEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm12SignatureMap12FindOrInsertERKNS0_9SignatureINS1_9ValueTypeEEE
	.type	_ZN2v88internal4wasm12SignatureMap12FindOrInsertERKNS0_9SignatureINS1_9ValueTypeEEE, @function
_ZN2v88internal4wasm12SignatureMap12FindOrInsertERKNS0_9SignatureINS1_9ValueTypeEEE:
.LFB4528:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, (%rdi)
	jne	.L81
	movq	%rsi, %r13
	movq	%rdi, %rbx
	leaq	8(%rdi), %r12
	movq	8(%rsi), %rdi
	movq	(%rsi), %rsi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	16(%r13), %r14
	movq	8(%r13), %r15
	movq	%rax, %rdi
	addq	%r14, %r15
	addq	0(%r13), %r15
	cmpq	%r15, %r14
	je	.L67
	.p2align 4,,10
	.p2align 3
.L68:
	call	_ZN2v84base10hash_valueEm@PLT
	movzbl	(%r14), %esi
	xorl	%edi, %edi
	addq	$1, %r14
	movq	%rax, -104(%rbp)
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	-104(%rbp), %r8
	movq	%rax, %rdi
	movq	%r8, %rsi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%rax, %rdi
	cmpq	%r14, %r15
	jne	.L68
.L67:
	movq	%rdi, %rax
	xorl	%edx, %edx
	movq	%rdi, %rcx
	movq	%r12, %rdi
	divq	16(%rbx)
	movq	%rdx, %rsi
	movq	%r13, %rdx
	call	_ZNKSt10_HashtableIN2v88internal9SignatureINS1_4wasm9ValueTypeEEESt4pairIKS5_jESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ENS0_4base4hashIS5_EENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmRS7_m
	testq	%rax, %rax
	je	.L69
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L69
	movl	32(%rax), %r14d
.L65:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L82
	addq	$72, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	.cfi_restore_state
	movq	32(%rbx), %rax
	cmpq	$2147483647, %rax
	jbe	.L83
	leaq	.LC2(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L83:
	movdqu	0(%r13), %xmm0
	movq	16(%r13), %rdx
	movq	%r12, %rdi
	movl	%eax, -72(%rbp)
	leaq	-96(%rbp), %rsi
	movl	%eax, %r14d
	movq	%rdx, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZNSt10_HashtableIN2v88internal9SignatureINS1_4wasm9ValueTypeEEESt4pairIKS5_jESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ENS0_4base4hashIS5_EENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJS6_IS5_jEEEES6_INSA_14_Node_iteratorIS8_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L81:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L82:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4528:
	.size	_ZN2v88internal4wasm12SignatureMap12FindOrInsertERKNS0_9SignatureINS1_9ValueTypeEEE, .-_ZN2v88internal4wasm12SignatureMap12FindOrInsertERKNS0_9SignatureINS1_9ValueTypeEEE
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
