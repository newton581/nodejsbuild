	.file	"serializer-allocator.cc"
	.text
	.section	.text._ZN2v88internal19SerializerAllocatorC2EPNS0_10SerializerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19SerializerAllocatorC2EPNS0_10SerializerE
	.type	_ZN2v88internal19SerializerAllocatorC2EPNS0_10SerializerE, @function
_ZN2v88internal19SerializerAllocatorC2EPNS0_10SerializerE:
.LFB19785:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movq	%rsi, 136(%rdi)
	movups	%xmm0, 16(%rdi)
	movups	%xmm0, 32(%rdi)
	movups	%xmm0, 48(%rdi)
	movups	%xmm0, 64(%rdi)
	movups	%xmm0, 80(%rdi)
	movups	%xmm0, 96(%rdi)
	movdqa	.LC0(%rip), %xmm0
	movl	$0, 128(%rdi)
	movups	%xmm0, 112(%rdi)
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%rdi)
	ret
	.cfi_endproc
.LFE19785:
	.size	_ZN2v88internal19SerializerAllocatorC2EPNS0_10SerializerE, .-_ZN2v88internal19SerializerAllocatorC2EPNS0_10SerializerE
	.globl	_ZN2v88internal19SerializerAllocatorC1EPNS0_10SerializerE
	.set	_ZN2v88internal19SerializerAllocatorC1EPNS0_10SerializerE,_ZN2v88internal19SerializerAllocatorC2EPNS0_10SerializerE
	.section	.text._ZN2v88internal19SerializerAllocator18UseCustomChunkSizeEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19SerializerAllocator18UseCustomChunkSizeEj
	.type	_ZN2v88internal19SerializerAllocator18UseCustomChunkSizeEj, @function
_ZN2v88internal19SerializerAllocator18UseCustomChunkSizeEj:
.LFB19787:
	.cfi_startproc
	endbr64
	movl	%esi, 128(%rdi)
	ret
	.cfi_endproc
.LFE19787:
	.size	_ZN2v88internal19SerializerAllocator18UseCustomChunkSizeEj, .-_ZN2v88internal19SerializerAllocator18UseCustomChunkSizeEj
	.section	.text._ZN2v88internal19SerializerAllocator15TargetChunkSizeENS0_13SnapshotSpaceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19SerializerAllocator15TargetChunkSizeENS0_13SnapshotSpaceE
	.type	_ZN2v88internal19SerializerAllocator15TargetChunkSizeENS0_13SnapshotSpaceE, @function
_ZN2v88internal19SerializerAllocator15TargetChunkSizeENS0_13SnapshotSpaceE:
.LFB19789:
	.cfi_startproc
	endbr64
	movl	128(%rdi), %eax
	testl	%eax, %eax
	je	.L10
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal17MemoryChunkLayout30AllocatableMemoryInMemoryChunkENS0_15AllocationSpaceE@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19789:
	.size	_ZN2v88internal19SerializerAllocator15TargetChunkSizeENS0_13SnapshotSpaceE, .-_ZN2v88internal19SerializerAllocator15TargetChunkSizeENS0_13SnapshotSpaceE
	.section	.text._ZN2v88internal19SerializerAllocator11AllocateMapEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19SerializerAllocator11AllocateMapEv
	.type	_ZN2v88internal19SerializerAllocator11AllocateMapEv, @function
_ZN2v88internal19SerializerAllocator11AllocateMapEv:
.LFB19791:
	.cfi_startproc
	endbr64
	movl	112(%rdi), %eax
	leal	1(%rax), %edx
	salq	$32, %rax
	movl	%edx, 112(%rdi)
	orq	$4, %rax
	ret
	.cfi_endproc
.LFE19791:
	.size	_ZN2v88internal19SerializerAllocator11AllocateMapEv, .-_ZN2v88internal19SerializerAllocator11AllocateMapEv
	.section	.text._ZN2v88internal19SerializerAllocator19AllocateLargeObjectEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19SerializerAllocator19AllocateLargeObjectEj
	.type	_ZN2v88internal19SerializerAllocator19AllocateLargeObjectEj, @function
_ZN2v88internal19SerializerAllocator19AllocateLargeObjectEj:
.LFB19792:
	.cfi_startproc
	endbr64
	movl	120(%rdi), %eax
	addl	%esi, 116(%rdi)
	leal	1(%rax), %edx
	salq	$32, %rax
	movl	%edx, 120(%rdi)
	orq	$5, %rax
	ret
	.cfi_endproc
.LFE19792:
	.size	_ZN2v88internal19SerializerAllocator19AllocateLargeObjectEj, .-_ZN2v88internal19SerializerAllocator19AllocateLargeObjectEj
	.section	.text._ZN2v88internal19SerializerAllocator27AllocateOffHeapBackingStoreEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19SerializerAllocator27AllocateOffHeapBackingStoreEv
	.type	_ZN2v88internal19SerializerAllocator27AllocateOffHeapBackingStoreEv, @function
_ZN2v88internal19SerializerAllocator27AllocateOffHeapBackingStoreEv:
.LFB19793:
	.cfi_startproc
	endbr64
	movl	124(%rdi), %eax
	leal	1(%rax), %edx
	salq	$32, %rax
	movl	%edx, 124(%rdi)
	orq	$38, %rax
	ret
	.cfi_endproc
.LFE19793:
	.size	_ZN2v88internal19SerializerAllocator27AllocateOffHeapBackingStoreEv, .-_ZN2v88internal19SerializerAllocator27AllocateOffHeapBackingStoreEv
	.section	.rodata._ZN2v88internal19SerializerAllocator16OutputStatisticsEv.str1.1,"aMS",@progbits,1
.LC1:
	.string	"  Spaces (bytes):\n"
.LC2:
	.string	"%16s"
.LC3:
	.string	"\n"
.LC4:
	.string	"%16zu"
.LC5:
	.string	"%16d"
.LC6:
	.string	"%16d\n"
	.section	.text._ZN2v88internal19SerializerAllocator16OutputStatisticsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19SerializerAllocator16OutputStatisticsEv
	.type	_ZN2v88internal19SerializerAllocator16OutputStatisticsEv, @function
_ZN2v88internal19SerializerAllocator16OutputStatisticsEv:
.LFB19801:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	.LC2(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -40
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	leaq	.LC1(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L15:
	movl	%r12d, %edi
	addl	$1, %r12d
	call	_ZN2v88internal4Heap12GetSpaceNameENS0_15AllocationSpaceE@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	cmpl	$6, %r12d
	jne	.L15
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	leaq	16(%rbx), %r13
	movq	%rbx, %r14
	call	_ZN2v88internal6PrintFEPKcz@PLT
	leaq	112(%rbx), %r12
	pxor	%xmm2, %xmm2
.L21:
	movq	0(%r13), %rdx
	movq	8(%r13), %r8
	movl	(%r14), %esi
	cmpq	%r8, %rdx
	je	.L16
	leaq	-4(%r8), %rcx
	movq	%rdx, %rax
	subq	%rdx, %rcx
	movq	%rcx, %rdi
	shrq	$2, %rdi
	addq	$1, %rdi
	cmpq	$8, %rcx
	jbe	.L17
	movq	%rdi, %rcx
	pxor	%xmm1, %xmm1
	shrq	$2, %rcx
	salq	$4, %rcx
	addq	%rdx, %rcx
	.p2align 4,,10
	.p2align 3
.L18:
	movdqu	(%rax), %xmm0
	addq	$16, %rax
	movdqa	%xmm0, %xmm3
	punpckhdq	%xmm2, %xmm0
	punpckldq	%xmm2, %xmm3
	paddq	%xmm3, %xmm0
	paddq	%xmm0, %xmm1
	cmpq	%rcx, %rax
	jne	.L18
	movdqa	%xmm1, %xmm0
	psrldq	$8, %xmm0
	paddq	%xmm0, %xmm1
	movq	%xmm1, %rax
	addq	%rax, %rsi
	movq	%rdi, %rax
	andq	$-4, %rax
	leaq	(%rdx,%rax,4), %rdx
	cmpq	%rax, %rdi
	je	.L16
.L17:
	movl	(%rdx), %eax
	addq	%rax, %rsi
	leaq	4(%rdx), %rax
	cmpq	%rax, %r8
	je	.L16
	movl	4(%rdx), %eax
	addq	%rax, %rsi
	leaq	8(%rdx), %rax
	cmpq	%rax, %r8
	je	.L16
	movl	8(%rdx), %eax
	addq	%rax, %rsi
.L16:
	xorl	%eax, %eax
	leaq	.LC4(%rip), %rdi
	addq	$24, %r13
	addq	$4, %r14
	call	_ZN2v88internal6PrintFEPKcz@PLT
	cmpq	%r12, %r13
	pxor	%xmm2, %xmm2
	jne	.L21
	movl	112(%rbx), %eax
	leaq	.LC5(%rip), %rdi
	leal	(%rax,%rax,4), %esi
	xorl	%eax, %eax
	sall	$4, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movl	116(%rbx), %esi
	leaq	.LC6(%rip), %rdi
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6PrintFEPKcz@PLT
	.cfi_endproc
.LFE19801:
	.size	_ZN2v88internal19SerializerAllocator16OutputStatisticsEv, .-_ZN2v88internal19SerializerAllocator16OutputStatisticsEv
	.section	.rodata._ZNSt6vectorIjSaIjEE17_M_realloc_insertIJRKjEEEvN9__gnu_cxx17__normal_iteratorIPjS1_EEDpOT_.str1.1,"aMS",@progbits,1
.LC7:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIjSaIjEE17_M_realloc_insertIJRKjEEEvN9__gnu_cxx17__normal_iteratorIPjS1_EEDpOT_,"axG",@progbits,_ZNSt6vectorIjSaIjEE17_M_realloc_insertIJRKjEEEvN9__gnu_cxx17__normal_iteratorIPjS1_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIjSaIjEE17_M_realloc_insertIJRKjEEEvN9__gnu_cxx17__normal_iteratorIPjS1_EEDpOT_
	.type	_ZNSt6vectorIjSaIjEE17_M_realloc_insertIJRKjEEEvN9__gnu_cxx17__normal_iteratorIPjS1_EEDpOT_, @function
_ZNSt6vectorIjSaIjEE17_M_realloc_insertIJRKjEEEvN9__gnu_cxx17__normal_iteratorIPjS1_EEDpOT_:
.LFB23240:
	.cfi_startproc
	endbr64
	movabsq	$2305843009213693951, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$2, %rax
	cmpq	%rcx, %rax
	je	.L40
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L36
	movabsq	$9223372036854775804, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L41
.L28:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L35:
	movl	(%r15), %eax
	subq	%r8, %r13
	leaq	4(%rbx,%rdx), %r15
	movl	%eax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L42
	testq	%r13, %r13
	jg	.L31
	testq	%r9, %r9
	jne	.L34
.L32:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L31
.L34:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L41:
	testq	%rsi, %rsi
	jne	.L29
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L31:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L32
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L36:
	movl	$4, %r14d
	jmp	.L28
.L40:
	leaq	.LC7(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L29:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$2, %r14
	jmp	.L28
	.cfi_endproc
.LFE23240:
	.size	_ZNSt6vectorIjSaIjEE17_M_realloc_insertIJRKjEEEvN9__gnu_cxx17__normal_iteratorIPjS1_EEDpOT_, .-_ZNSt6vectorIjSaIjEE17_M_realloc_insertIJRKjEEEvN9__gnu_cxx17__normal_iteratorIPjS1_EEDpOT_
	.section	.text._ZN2v88internal19SerializerAllocator8AllocateENS0_13SnapshotSpaceEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19SerializerAllocator8AllocateENS0_13SnapshotSpaceEj
	.type	_ZN2v88internal19SerializerAllocator8AllocateENS0_13SnapshotSpaceEj, @function
_ZN2v88internal19SerializerAllocator8AllocateENS0_13SnapshotSpaceEj:
.LFB19790:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%esi, %r14
	pushq	%r13
	leaq	(%rdi,%r14,4), %r15
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r14, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	(%r15), %esi
	movl	128(%rdi), %eax
	addl	%esi, %edx
	testl	%eax, %eax
	je	.L55
.L44:
	leaq	(%r14,%r14,2), %r14
	salq	$3, %r14
	leaq	16(%rbx,%r14), %rcx
	testl	%esi, %esi
	je	.L50
	cmpl	%eax, %edx
	ja	.L45
.L50:
	movl	(%r15), %esi
.L47:
	movl	%edx, (%r15)
	movq	8(%rcx), %rax
	salq	$32, %rsi
	subq	(%rcx), %rax
	addq	$24, %rsp
	sarq	$2, %rax
	popq	%rbx
	sall	$4, %eax
	orl	%r12d, %eax
	popq	%r12
	popq	%r13
	orq	%rsi, %rax
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	.cfi_restore_state
	movl	%r14d, %edi
	movl	%esi, -60(%rbp)
	movl	%edx, -56(%rbp)
	call	_ZN2v88internal17MemoryChunkLayout30AllocatableMemoryInMemoryChunkENS0_15AllocationSpaceE@PLT
	movl	-60(%rbp), %esi
	movl	-56(%rbp), %edx
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L45:
	movq	136(%rbx), %rdi
	movl	%r12d, %esi
	addq	%r14, %rbx
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal10Serializer12PutNextChunkENS0_13SnapshotSpaceE@PLT
	movq	24(%rbx), %rsi
	cmpq	32(%rbx), %rsi
	movq	-56(%rbp), %rcx
	je	.L48
	movl	(%r15), %eax
	movl	%eax, (%rsi)
	addq	$4, 24(%rbx)
.L49:
	movl	%r13d, %edx
	xorl	%esi, %esi
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L48:
	movq	%rcx, %rdi
	movq	%r15, %rdx
	movq	%rcx, -56(%rbp)
	call	_ZNSt6vectorIjSaIjEE17_M_realloc_insertIJRKjEEEvN9__gnu_cxx17__normal_iteratorIPjS1_EEDpOT_
	movq	-56(%rbp), %rcx
	jmp	.L49
	.cfi_endproc
.LFE19790:
	.size	_ZN2v88internal19SerializerAllocator8AllocateENS0_13SnapshotSpaceEj, .-_ZN2v88internal19SerializerAllocator8AllocateENS0_13SnapshotSpaceEj
	.section	.text._ZNSt6vectorIN2v88internal14SerializedData11ReservationESaIS3_EE17_M_realloc_insertIJRKjEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal14SerializedData11ReservationESaIS3_EE17_M_realloc_insertIJRKjEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal14SerializedData11ReservationESaIS3_EE17_M_realloc_insertIJRKjEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal14SerializedData11ReservationESaIS3_EE17_M_realloc_insertIJRKjEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, @function
_ZNSt6vectorIN2v88internal14SerializedData11ReservationESaIS3_EE17_M_realloc_insertIJRKjEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_:
.LFB23251:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movabsq	$2305843009213693951, %rsi
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rcx
	movq	(%rdi), %r12
	movq	%rcx, %rax
	subq	%r12, %rax
	sarq	$2, %rax
	cmpq	%rsi, %rax
	je	.L83
	movq	%r13, %r9
	movq	%rdi, %r15
	subq	%r12, %r9
	testq	%rax, %rax
	je	.L68
	movabsq	$9223372036854775804, %r14
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L84
.L58:
	movq	%r14, %rdi
	movq	%rdx, -80(%rbp)
	movq	%r9, -72(%rbp)
	movq	%rcx, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %r9
	movq	%rax, %rbx
	leaq	(%rax,%r14), %rax
	movq	-80(%rbp), %rdx
	movq	%rax, -56(%rbp)
	leaq	4(%rbx), %r14
.L67:
	movl	(%rdx), %eax
	movl	%eax, (%rbx,%r9)
	cmpq	%r12, %r13
	je	.L60
	leaq	-4(%r13), %rdi
	leaq	15(%rbx), %rax
	subq	%r12, %rdi
	subq	%r12, %rax
	movq	%rdi, %rsi
	shrq	$2, %rsi
	cmpq	$30, %rax
	jbe	.L70
	movabsq	$4611686018427387900, %rax
	testq	%rax, %rsi
	je	.L70
	addq	$1, %rsi
	xorl	%eax, %eax
	movq	%rsi, %rdx
	shrq	$2, %rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L62:
	movdqu	(%r12,%rax), %xmm1
	movups	%xmm1, (%rbx,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L62
	movq	%rsi, %rdx
	andq	$-4, %rdx
	leaq	0(,%rdx,4), %rax
	leaq	(%r12,%rax), %r8
	addq	%rbx, %rax
	cmpq	%rdx, %rsi
	je	.L64
	movl	(%r8), %edx
	movl	%edx, (%rax)
	leaq	4(%r8), %rdx
	cmpq	%rdx, %r13
	je	.L64
	movl	4(%r8), %edx
	movl	%edx, 4(%rax)
	leaq	8(%r8), %rdx
	cmpq	%rdx, %r13
	je	.L64
	movl	8(%r8), %edx
	movl	%edx, 8(%rax)
.L64:
	leaq	8(%rbx,%rdi), %r14
.L60:
	cmpq	%rcx, %r13
	je	.L65
	subq	%r13, %rcx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movq	%rcx, %rdx
	movq	%rcx, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %rcx
	addq	%rcx, %r14
.L65:
	testq	%r12, %r12
	je	.L66
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L66:
	movq	-56(%rbp), %rax
	movq	%rbx, %xmm0
	movq	%r14, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movq	%rax, 16(%r15)
	movups	%xmm0, (%r15)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L59
	movq	$0, -56(%rbp)
	movl	$4, %r14d
	xorl	%ebx, %ebx
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L68:
	movl	$4, %r14d
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L70:
	movq	%rbx, %rdx
	movq	%r12, %rax
	.p2align 4,,10
	.p2align 3
.L61:
	movl	(%rax), %esi
	addq	$4, %rax
	addq	$4, %rdx
	movl	%esi, -4(%rdx)
	cmpq	%rax, %r13
	jne	.L61
	jmp	.L64
.L59:
	cmpq	%rsi, %rdi
	cmovbe	%rdi, %rsi
	movq	%rsi, %r14
	salq	$2, %r14
	jmp	.L58
.L83:
	leaq	.LC7(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE23251:
	.size	_ZNSt6vectorIN2v88internal14SerializedData11ReservationESaIS3_EE17_M_realloc_insertIJRKjEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, .-_ZNSt6vectorIN2v88internal14SerializedData11ReservationESaIS3_EE17_M_realloc_insertIJRKjEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.section	.text._ZNK2v88internal19SerializerAllocator18EncodeReservationsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal19SerializerAllocator18EncodeReservationsEv
	.type	_ZNK2v88internal19SerializerAllocator18EncodeReservationsEv, @function
_ZNK2v88internal19SerializerAllocator18EncodeReservationsEv:
.LFB19794:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16(%rsi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	xorl	%esi, %esi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	$0, 16(%rdi)
	movups	%xmm0, (%rdi)
	movq	(%r15), %rax
	movq	%r15, -56(%rbp)
	cmpq	%rax, 8(%r15)
	je	.L86
.L129:
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L90:
	leaq	(%rax,%rbx,4), %rdx
	cmpq	%rsi, 16(%r12)
	je	.L87
	movl	(%rdx), %eax
	addq	$1, %rbx
	movl	%eax, (%rsi)
	movq	8(%r12), %rax
	leaq	4(%rax), %rsi
	movq	%rsi, 8(%r12)
	movq	8(%r15), %rcx
	movq	(%r15), %rax
	movq	%rcx, %rdx
	subq	%rax, %rdx
	sarq	$2, %rdx
	cmpq	%rdx, %rbx
	jb	.L90
.L89:
	movl	(%r14), %edx
	testl	%edx, %edx
	jne	.L109
	cmpq	%rax, %rcx
	je	.L109
.L91:
	orl	$-2147483648, -4(%rsi)
	addq	$24, %r15
	addq	$4, %r14
	cmpq	-56(%rbp), %r14
	je	.L94
	movq	8(%r12), %rsi
	movq	(%r15), %rax
	cmpq	%rax, 8(%r15)
	jne	.L129
.L86:
	movl	(%r14), %edx
.L109:
	cmpq	%rsi, 16(%r12)
	je	.L93
	movl	%edx, (%rsi)
	movq	8(%r12), %rax
	leaq	4(%rax), %rsi
	movq	%rsi, 8(%r12)
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L87:
	movq	%r12, %rdi
	addq	$1, %rbx
	call	_ZNSt6vectorIN2v88internal14SerializedData11ReservationESaIS3_EE17_M_realloc_insertIJRKjEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	movq	8(%r15), %rcx
	movq	(%r15), %rax
	movq	8(%r12), %rsi
	movq	%rcx, %rdx
	subq	%rax, %rdx
	sarq	$2, %rdx
	cmpq	%rbx, %rdx
	ja	.L90
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L94:
	movl	112(%r13), %eax
	movq	8(%r12), %r15
	leal	(%rax,%rax,4), %eax
	sall	$4, %eax
	movl	%eax, %r14d
	cmpq	16(%r12), %r15
	je	.L96
	movl	%eax, (%r15)
	movq	8(%r12), %rax
	leaq	4(%rax), %rbx
	movq	%rbx, 8(%r12)
.L97:
	orl	$-2147483648, -4(%rbx)
	movq	8(%r12), %rsi
	cmpq	16(%r12), %rsi
	je	.L107
	movl	116(%r13), %eax
	movl	%eax, (%rsi)
	movq	8(%r12), %rax
	addq	$4, %rax
	movq	%rax, 8(%r12)
.L108:
	orl	$-2147483648, -4(%rax)
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L93:
	.cfi_restore_state
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIN2v88internal14SerializedData11ReservationESaIS3_EE17_M_realloc_insertIJRKjEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	movq	8(%r12), %rsi
	jmp	.L91
.L107:
	leaq	116(%r13), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIN2v88internal14SerializedData11ReservationESaIS3_EE17_M_realloc_insertIJRKjEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	movq	8(%r12), %rax
	jmp	.L108
.L96:
	movq	(%r12), %r8
	movq	%r15, %rcx
	movabsq	$2305843009213693951, %rsi
	subq	%r8, %rcx
	movq	%rcx, %rax
	sarq	$2, %rax
	cmpq	%rsi, %rax
	je	.L130
	testq	%rax, %rax
	je	.L110
	movabsq	$9223372036854775804, %rbx
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L131
.L99:
	movq	%rbx, %rdi
	movq	%rcx, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %rcx
	leaq	(%rax,%rbx), %rsi
	movq	%rax, %rdx
	leaq	4(%rax), %rbx
.L100:
	movl	%r14d, (%rdx,%rcx)
	cmpq	%r8, %r15
	je	.L101
	leaq	-4(%r15), %r9
	leaq	15(%rdx), %rax
	subq	%r8, %r9
	subq	%r8, %rax
	movq	%r9, %rdi
	shrq	$2, %rdi
	cmpq	$30, %rax
	jbe	.L113
	movabsq	$4611686018427387900, %rax
	testq	%rax, %rdi
	je	.L113
	addq	$1, %rdi
	xorl	%eax, %eax
	movq	%rdi, %rcx
	shrq	$2, %rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L103:
	movdqu	(%r8,%rax), %xmm1
	movups	%xmm1, (%rdx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rcx
	jne	.L103
	movq	%rdi, %rcx
	andq	$-4, %rcx
	leaq	0(,%rcx,4), %rax
	leaq	(%r8,%rax), %r10
	addq	%rdx, %rax
	cmpq	%rdi, %rcx
	je	.L105
	movl	(%r10), %ecx
	movl	%ecx, (%rax)
	leaq	4(%r10), %rcx
	cmpq	%rcx, %r15
	je	.L105
	movl	4(%r10), %ecx
	movl	%ecx, 4(%rax)
	leaq	8(%r10), %rcx
	cmpq	%rcx, %r15
	je	.L105
	movl	8(%r10), %ecx
	movl	%ecx, 8(%rax)
.L105:
	leaq	8(%rdx,%r9), %rbx
.L101:
	testq	%r8, %r8
	je	.L106
	movq	%r8, %rdi
	movq	%rsi, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdx
.L106:
	movq	%rdx, %xmm0
	movq	%rbx, %xmm2
	movq	%rsi, 16(%r12)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, (%r12)
	jmp	.L97
.L131:
	testq	%rdx, %rdx
	jne	.L132
	movl	$4, %ebx
	xorl	%esi, %esi
	xorl	%edx, %edx
	jmp	.L100
.L110:
	movl	$4, %ebx
	jmp	.L99
.L113:
	movq	%rdx, %rcx
	movq	%r8, %rax
.L102:
	movl	(%rax), %edi
	addq	$4, %rax
	addq	$4, %rcx
	movl	%edi, -4(%rcx)
	cmpq	%rax, %r15
	jne	.L102
	jmp	.L105
.L130:
	leaq	.LC7(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L132:
	cmpq	%rsi, %rdx
	cmova	%rsi, %rdx
	leaq	0(,%rdx,4), %rbx
	jmp	.L99
	.cfi_endproc
.LFE19794:
	.size	_ZNK2v88internal19SerializerAllocator18EncodeReservationsEv, .-_ZNK2v88internal19SerializerAllocator18EncodeReservationsEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal19SerializerAllocatorC2EPNS0_10SerializerE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal19SerializerAllocatorC2EPNS0_10SerializerE, @function
_GLOBAL__sub_I__ZN2v88internal19SerializerAllocatorC2EPNS0_10SerializerE:
.LFB25138:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE25138:
	.size	_GLOBAL__sub_I__ZN2v88internal19SerializerAllocatorC2EPNS0_10SerializerE, .-_GLOBAL__sub_I__ZN2v88internal19SerializerAllocatorC2EPNS0_10SerializerE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal19SerializerAllocatorC2EPNS0_10SerializerE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	0
	.long	0
	.long	0
	.long	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
