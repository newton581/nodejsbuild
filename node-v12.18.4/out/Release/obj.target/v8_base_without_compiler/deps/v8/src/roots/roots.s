	.file	"roots.cc"
	.text
	.section	.text._ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE,"axG",@progbits,_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE
	.type	_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE, @function
_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE:
.LFB7690:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7690:
	.size	_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE, .-_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE
	.section	.text._ZN2v88internal13ReadOnlyRoots7IterateEPNS0_11RootVisitorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13ReadOnlyRoots7IterateEPNS0_11RootVisitorE
	.type	_ZN2v88internal13ReadOnlyRoots7IterateEPNS0_11RootVisitorE, @function
_ZN2v88internal13ReadOnlyRoots7IterateEPNS0_11RootVisitorE:
.LFB17782:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$8, %rsp
	movq	(%rsi), %rax
	movq	(%rdi), %rcx
	movl	$2, %esi
	movq	%r12, %rdi
	leaq	4312(%rcx), %r8
	call	*16(%rax)
	movq	(%r12), %rax
	leaq	_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE(%rip), %rdx
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L6
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$2, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE17782:
	.size	_ZN2v88internal13ReadOnlyRoots7IterateEPNS0_11RootVisitorE, .-_ZN2v88internal13ReadOnlyRoots7IterateEPNS0_11RootVisitorE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal10RootsTable11root_names_E,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal10RootsTable11root_names_E, @function
_GLOBAL__sub_I__ZN2v88internal10RootsTable11root_names_E:
.LFB21439:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE21439:
	.size	_GLOBAL__sub_I__ZN2v88internal10RootsTable11root_names_E, .-_GLOBAL__sub_I__ZN2v88internal10RootsTable11root_names_E
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal10RootsTable11root_names_E
	.globl	_ZN2v88internal10RootsTable11root_names_E
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"free_space_map"
.LC1:
	.string	"one_pointer_filler_map"
.LC2:
	.string	"two_pointer_filler_map"
.LC3:
	.string	"uninitialized_value"
.LC4:
	.string	"undefined_value"
.LC5:
	.string	"the_hole_value"
.LC6:
	.string	"null_value"
.LC7:
	.string	"true_value"
.LC8:
	.string	"false_value"
.LC9:
	.string	"empty_string"
.LC10:
	.string	"meta_map"
.LC11:
	.string	"byte_array_map"
.LC12:
	.string	"fixed_array_map"
.LC13:
	.string	"fixed_cow_array_map"
.LC14:
	.string	"hash_table_map"
.LC15:
	.string	"symbol_map"
.LC16:
	.string	"one_byte_string_map"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC17:
	.string	"one_byte_internalized_string_map"
	.section	.rodata.str1.1
.LC18:
	.string	"scope_info_map"
.LC19:
	.string	"shared_function_info_map"
.LC20:
	.string	"code_map"
.LC21:
	.string	"function_context_map"
.LC22:
	.string	"cell_map"
.LC23:
	.string	"global_property_cell_map"
.LC24:
	.string	"foreign_map"
.LC25:
	.string	"heap_number_map"
.LC26:
	.string	"transition_array_map"
.LC27:
	.string	"feedback_vector_map"
.LC28:
	.string	"empty_scope_info"
.LC29:
	.string	"empty_fixed_array"
.LC30:
	.string	"empty_descriptor_array"
.LC31:
	.string	"arguments_marker"
.LC32:
	.string	"exception"
.LC33:
	.string	"termination_exception"
.LC34:
	.string	"optimized_out"
.LC35:
	.string	"stale_register"
.LC36:
	.string	"native_context_map"
.LC37:
	.string	"module_context_map"
.LC38:
	.string	"eval_context_map"
.LC39:
	.string	"script_context_map"
.LC40:
	.string	"await_context_map"
.LC41:
	.string	"block_context_map"
.LC42:
	.string	"catch_context_map"
.LC43:
	.string	"with_context_map"
.LC44:
	.string	"debug_evaluate_context_map"
.LC45:
	.string	"script_context_table_map"
	.section	.rodata.str1.8
	.align 8
.LC46:
	.string	"closure_feedback_cell_array_map"
	.section	.rodata.str1.1
.LC47:
	.string	"feedback_metadata_map"
.LC48:
	.string	"array_list_map"
.LC49:
	.string	"bigint_map"
	.section	.rodata.str1.8
	.align 8
.LC50:
	.string	"object_boilerplate_description_map"
	.section	.rodata.str1.1
.LC51:
	.string	"bytecode_array_map"
.LC52:
	.string	"code_data_container_map"
.LC53:
	.string	"descriptor_array_map"
.LC54:
	.string	"fixed_double_array_map"
.LC55:
	.string	"global_dictionary_map"
.LC56:
	.string	"many_closures_cell_map"
.LC57:
	.string	"module_info_map"
.LC58:
	.string	"name_dictionary_map"
.LC59:
	.string	"no_closures_cell_map"
.LC60:
	.string	"number_dictionary_map"
.LC61:
	.string	"one_closure_cell_map"
.LC62:
	.string	"ordered_hash_map_map"
.LC63:
	.string	"ordered_hash_set_map"
.LC64:
	.string	"ordered_name_dictionary_map"
.LC65:
	.string	"preparse_data_map"
.LC66:
	.string	"property_array_map"
	.section	.rodata.str1.8
	.align 8
.LC67:
	.string	"side_effect_call_handler_info_map"
	.align 8
.LC68:
	.string	"side_effect_free_call_handler_info_map"
	.align 8
.LC69:
	.string	"next_call_side_effect_free_call_handler_info_map"
	.section	.rodata.str1.1
.LC70:
	.string	"simple_number_dictionary_map"
.LC71:
	.string	"sloppy_arguments_elements_map"
.LC72:
	.string	"small_ordered_hash_map_map"
.LC73:
	.string	"small_ordered_hash_set_map"
	.section	.rodata.str1.8
	.align 8
.LC74:
	.string	"small_ordered_name_dictionary_map"
	.section	.rodata.str1.1
.LC75:
	.string	"source_text_module_map"
.LC76:
	.string	"string_table_map"
.LC77:
	.string	"synthetic_module_map"
	.section	.rodata.str1.8
	.align 8
.LC78:
	.string	"uncompiled_data_without_preparse_data_map"
	.align 8
.LC79:
	.string	"uncompiled_data_with_preparse_data_map"
	.section	.rodata.str1.1
.LC80:
	.string	"weak_fixed_array_map"
.LC81:
	.string	"weak_array_list_map"
.LC82:
	.string	"ephemeron_hash_table_map"
.LC83:
	.string	"embedder_data_array_map"
.LC84:
	.string	"weak_cell_map"
.LC85:
	.string	"native_source_string_map"
.LC86:
	.string	"string_map"
.LC87:
	.string	"cons_one_byte_string_map"
.LC88:
	.string	"cons_string_map"
.LC89:
	.string	"thin_one_byte_string_map"
.LC90:
	.string	"thin_string_map"
.LC91:
	.string	"sliced_string_map"
.LC92:
	.string	"sliced_one_byte_string_map"
.LC93:
	.string	"external_string_map"
.LC94:
	.string	"external_one_byte_string_map"
.LC95:
	.string	"uncached_external_string_map"
.LC96:
	.string	"internalized_string_map"
	.section	.rodata.str1.8
	.align 8
.LC97:
	.string	"external_internalized_string_map"
	.align 8
.LC98:
	.string	"external_one_byte_internalized_string_map"
	.align 8
.LC99:
	.string	"uncached_external_internalized_string_map"
	.align 8
.LC100:
	.string	"uncached_external_one_byte_internalized_string_map"
	.align 8
.LC101:
	.string	"uncached_external_one_byte_string_map"
	.section	.rodata.str1.1
.LC102:
	.string	"undefined_map"
.LC103:
	.string	"the_hole_map"
.LC104:
	.string	"null_map"
.LC105:
	.string	"boolean_map"
.LC106:
	.string	"uninitialized_map"
.LC107:
	.string	"arguments_marker_map"
.LC108:
	.string	"exception_map"
.LC109:
	.string	"termination_exception_map"
.LC110:
	.string	"optimized_out_map"
.LC111:
	.string	"stale_register_map"
.LC112:
	.string	"self_reference_marker_map"
.LC113:
	.string	"empty_enum_cache"
.LC114:
	.string	"empty_property_array"
.LC115:
	.string	"empty_byte_array"
	.section	.rodata.str1.8
	.align 8
.LC116:
	.string	"empty_object_boilerplate_description"
	.align 8
.LC117:
	.string	"empty_array_boilerplate_description"
	.align 8
.LC118:
	.string	"empty_closure_feedback_cell_array"
	.align 8
.LC119:
	.string	"empty_sloppy_arguments_elements"
	.section	.rodata.str1.1
.LC120:
	.string	"empty_slow_element_dictionary"
.LC121:
	.string	"empty_ordered_hash_map"
.LC122:
	.string	"empty_ordered_hash_set"
.LC123:
	.string	"empty_feedback_metadata"
.LC124:
	.string	"empty_property_cell"
.LC125:
	.string	"empty_property_dictionary"
.LC126:
	.string	"noop_interceptor_info"
.LC127:
	.string	"empty_weak_fixed_array"
.LC128:
	.string	"empty_weak_array_list"
.LC129:
	.string	"nan_value"
.LC130:
	.string	"hole_nan_value"
.LC131:
	.string	"infinity_value"
.LC132:
	.string	"minus_zero_value"
.LC133:
	.string	"minus_infinity_value"
.LC134:
	.string	"self_reference_marker"
	.section	.rodata.str1.8
	.align 8
.LC135:
	.string	"off_heap_trampoline_relocation_info"
	.align 8
.LC136:
	.string	"trampoline_trivial_code_data_container"
	.align 8
.LC137:
	.string	"trampoline_promise_rejection_code_data_container"
	.align 8
.LC138:
	.string	"global_this_binding_scope_info"
	.section	.rodata.str1.1
.LC139:
	.string	"empty_function_scope_info"
.LC140:
	.string	"hash_seed"
.LC141:
	.string	"adoptText_string"
.LC142:
	.string	"baseName_string"
.LC143:
	.string	"accounting_string"
.LC144:
	.string	"breakType_string"
.LC145:
	.string	"calendar_string"
.LC146:
	.string	"cardinal_string"
.LC147:
	.string	"caseFirst_string"
.LC148:
	.string	"compare_string"
.LC149:
	.string	"current_string"
.LC150:
	.string	"collation_string"
.LC151:
	.string	"compact_string"
.LC152:
	.string	"compactDisplay_string"
.LC153:
	.string	"currency_string"
.LC154:
	.string	"currencyDisplay_string"
.LC155:
	.string	"currencySign_string"
.LC156:
	.string	"dateStyle_string"
.LC157:
	.string	"day_string"
.LC158:
	.string	"dayPeriod_string"
.LC159:
	.string	"decimal_string"
.LC160:
	.string	"endRange_string"
.LC161:
	.string	"engineering_string"
.LC162:
	.string	"era_string"
.LC163:
	.string	"exceptZero_string"
.LC164:
	.string	"exponentInteger_string"
.LC165:
	.string	"exponentMinusSign_string"
.LC166:
	.string	"exponentSeparator_string"
.LC167:
	.string	"first_string"
.LC168:
	.string	"format_string"
.LC169:
	.string	"fraction_string"
.LC170:
	.string	"fractionalSecond_string"
.LC171:
	.string	"fractionalSecondDigits_string"
.LC172:
	.string	"full_string"
.LC173:
	.string	"granularity_string"
.LC174:
	.string	"grapheme_string"
.LC175:
	.string	"group_string"
.LC176:
	.string	"h11_string"
.LC177:
	.string	"h12_string"
.LC178:
	.string	"h23_string"
.LC179:
	.string	"h24_string"
.LC180:
	.string	"hour_string"
.LC181:
	.string	"hour12_string"
.LC182:
	.string	"hourCycle_string"
.LC183:
	.string	"ideo_string"
.LC184:
	.string	"ignorePunctuation_string"
.LC185:
	.string	"Invalid_Date_string"
.LC186:
	.string	"integer_string"
.LC187:
	.string	"kana_string"
.LC188:
	.string	"language_string"
.LC189:
	.string	"letter_string"
.LC190:
	.string	"list_string"
.LC191:
	.string	"literal_string"
.LC192:
	.string	"locale_string"
.LC193:
	.string	"loose_string"
.LC194:
	.string	"lower_string"
.LC195:
	.string	"maximumFractionDigits_string"
	.section	.rodata.str1.8
	.align 8
.LC196:
	.string	"maximumSignificantDigits_string"
	.section	.rodata.str1.1
.LC197:
	.string	"minimumFractionDigits_string"
.LC198:
	.string	"minimumIntegerDigits_string"
	.section	.rodata.str1.8
	.align 8
.LC199:
	.string	"minimumSignificantDigits_string"
	.section	.rodata.str1.1
.LC200:
	.string	"minusSign_string"
.LC201:
	.string	"minute_string"
.LC202:
	.string	"month_string"
.LC203:
	.string	"nan_string"
.LC204:
	.string	"narrowSymbol_string"
.LC205:
	.string	"never_string"
.LC206:
	.string	"none_string"
.LC207:
	.string	"notation_string"
.LC208:
	.string	"normal_string"
.LC209:
	.string	"numberingSystem_string"
.LC210:
	.string	"numeric_string"
.LC211:
	.string	"ordinal_string"
.LC212:
	.string	"percentSign_string"
.LC213:
	.string	"plusSign_string"
.LC214:
	.string	"quarter_string"
.LC215:
	.string	"region_string"
.LC216:
	.string	"scientific_string"
.LC217:
	.string	"second_string"
.LC218:
	.string	"segment_string"
.LC219:
	.string	"SegmentIterator_string"
.LC220:
	.string	"sensitivity_string"
.LC221:
	.string	"sep_string"
.LC222:
	.string	"shared_string"
.LC223:
	.string	"signDisplay_string"
.LC224:
	.string	"standard_string"
.LC225:
	.string	"startRange_string"
.LC226:
	.string	"strict_string"
.LC227:
	.string	"style_string"
.LC228:
	.string	"term_string"
.LC229:
	.string	"timeStyle_string"
.LC230:
	.string	"timeZone_string"
.LC231:
	.string	"timeZoneName_string"
.LC232:
	.string	"type_string"
.LC233:
	.string	"unknown_string"
.LC234:
	.string	"upper_string"
.LC235:
	.string	"usage_string"
.LC236:
	.string	"useGrouping_string"
.LC237:
	.string	"UTC_string"
.LC238:
	.string	"unit_string"
.LC239:
	.string	"unitDisplay_string"
.LC240:
	.string	"weekday_string"
.LC241:
	.string	"year_string"
.LC242:
	.string	"add_string"
.LC243:
	.string	"always_string"
.LC244:
	.string	"anonymous_function_string"
.LC245:
	.string	"anonymous_string"
.LC246:
	.string	"apply_string"
.LC247:
	.string	"Arguments_string"
.LC248:
	.string	"arguments_string"
.LC249:
	.string	"arguments_to_string"
.LC250:
	.string	"Array_string"
.LC251:
	.string	"array_to_string"
.LC252:
	.string	"ArrayBuffer_string"
.LC253:
	.string	"ArrayIterator_string"
.LC254:
	.string	"as_string"
.LC255:
	.string	"async_string"
.LC256:
	.string	"auto_string"
.LC257:
	.string	"await_string"
.LC258:
	.string	"BigInt_string"
.LC259:
	.string	"bigint_string"
.LC260:
	.string	"BigInt64Array_string"
.LC261:
	.string	"BigUint64Array_string"
.LC262:
	.string	"bind_string"
.LC263:
	.string	"Boolean_string"
.LC264:
	.string	"boolean_string"
.LC265:
	.string	"boolean_to_string"
.LC266:
	.string	"bound__string"
.LC267:
	.string	"buffer_string"
.LC268:
	.string	"byte_length_string"
.LC269:
	.string	"byte_offset_string"
.LC270:
	.string	"CompileError_string"
.LC271:
	.string	"callee_string"
.LC272:
	.string	"caller_string"
.LC273:
	.string	"character_string"
.LC274:
	.string	"closure_string"
.LC275:
	.string	"code_string"
.LC276:
	.string	"column_string"
.LC277:
	.string	"computed_string"
.LC278:
	.string	"configurable_string"
.LC279:
	.string	"conjunction_string"
.LC280:
	.string	"construct_string"
.LC281:
	.string	"constructor_string"
.LC282:
	.string	"Date_string"
.LC283:
	.string	"date_to_string"
.LC284:
	.string	"default_string"
.LC285:
	.string	"defineProperty_string"
.LC286:
	.string	"deleteProperty_string"
.LC287:
	.string	"disjunction_string"
.LC288:
	.string	"display_name_string"
.LC289:
	.string	"done_string"
.LC290:
	.string	"dot_brand_string"
.LC291:
	.string	"dot_catch_string"
.LC292:
	.string	"dot_default_string"
.LC293:
	.string	"dot_for_string"
.LC294:
	.string	"dot_generator_object_string"
.LC295:
	.string	"dot_iterator_string"
.LC296:
	.string	"dot_promise_string"
.LC297:
	.string	"dot_result_string"
.LC298:
	.string	"dot_string"
.LC299:
	.string	"dot_switch_tag_string"
.LC300:
	.string	"dotAll_string"
.LC301:
	.string	"enumerable_string"
.LC302:
	.string	"element_string"
.LC303:
	.string	"Error_string"
.LC304:
	.string	"error_to_string"
.LC305:
	.string	"eval_string"
.LC306:
	.string	"EvalError_string"
.LC307:
	.string	"exec_string"
.LC308:
	.string	"false_string"
.LC309:
	.string	"flags_string"
.LC310:
	.string	"Float32Array_string"
.LC311:
	.string	"Float64Array_string"
.LC312:
	.string	"from_string"
.LC313:
	.string	"Function_string"
.LC314:
	.string	"function_native_code_string"
.LC315:
	.string	"function_string"
.LC316:
	.string	"function_to_string"
.LC317:
	.string	"Generator_string"
.LC318:
	.string	"get_space_string"
.LC319:
	.string	"get_string"
	.section	.rodata.str1.8
	.align 8
.LC320:
	.string	"getOwnPropertyDescriptor_string"
	.section	.rodata.str1.1
.LC321:
	.string	"getPrototypeOf_string"
.LC322:
	.string	"global_string"
.LC323:
	.string	"globalThis_string"
.LC324:
	.string	"groups_string"
.LC325:
	.string	"has_string"
.LC326:
	.string	"ignoreCase_string"
.LC327:
	.string	"illegal_access_string"
.LC328:
	.string	"illegal_argument_string"
.LC329:
	.string	"index_string"
.LC330:
	.string	"Infinity_string"
.LC331:
	.string	"infinity_string"
.LC332:
	.string	"input_string"
.LC333:
	.string	"Int16Array_string"
.LC334:
	.string	"Int32Array_string"
.LC335:
	.string	"Int8Array_string"
.LC336:
	.string	"isExtensible_string"
.LC337:
	.string	"keys_string"
.LC338:
	.string	"lastIndex_string"
.LC339:
	.string	"length_string"
.LC340:
	.string	"let_string"
.LC341:
	.string	"line_string"
.LC342:
	.string	"LinkError_string"
.LC343:
	.string	"long_string"
.LC344:
	.string	"Map_string"
.LC345:
	.string	"MapIterator_string"
.LC346:
	.string	"medium_string"
.LC347:
	.string	"message_string"
.LC348:
	.string	"meta_string"
.LC349:
	.string	"minus_Infinity_string"
.LC350:
	.string	"Module_string"
.LC351:
	.string	"multiline_string"
.LC352:
	.string	"name_string"
.LC353:
	.string	"NaN_string"
.LC354:
	.string	"narrow_string"
.LC355:
	.string	"native_string"
.LC356:
	.string	"new_target_string"
.LC357:
	.string	"next_string"
.LC358:
	.string	"NFC_string"
.LC359:
	.string	"NFD_string"
.LC360:
	.string	"NFKC_string"
.LC361:
	.string	"NFKD_string"
.LC362:
	.string	"not_equal"
.LC363:
	.string	"null_string"
.LC364:
	.string	"null_to_string"
.LC365:
	.string	"Number_string"
.LC366:
	.string	"number_string"
.LC367:
	.string	"number_to_string"
.LC368:
	.string	"Object_string"
.LC369:
	.string	"object_string"
.LC370:
	.string	"object_to_string"
.LC371:
	.string	"of_string"
.LC372:
	.string	"ok"
.LC373:
	.string	"one_string"
.LC374:
	.string	"ownKeys_string"
.LC375:
	.string	"percent_string"
.LC376:
	.string	"position_string"
.LC377:
	.string	"preventExtensions_string"
.LC378:
	.string	"private_constructor_string"
.LC379:
	.string	"Promise_string"
.LC380:
	.string	"proto_string"
.LC381:
	.string	"prototype_string"
.LC382:
	.string	"proxy_string"
.LC383:
	.string	"Proxy_string"
.LC384:
	.string	"query_colon_string"
.LC385:
	.string	"RangeError_string"
.LC386:
	.string	"raw_string"
.LC387:
	.string	"ReferenceError_string"
.LC388:
	.string	"ReflectGet_string"
.LC389:
	.string	"ReflectHas_string"
.LC390:
	.string	"RegExp_string"
.LC391:
	.string	"regexp_to_string"
.LC392:
	.string	"resolve_string"
.LC393:
	.string	"return_string"
.LC394:
	.string	"revoke_string"
.LC395:
	.string	"RuntimeError_string"
.LC396:
	.string	"Script_string"
.LC397:
	.string	"script_string"
.LC398:
	.string	"short_string"
.LC399:
	.string	"Set_string"
.LC400:
	.string	"sentence_string"
.LC401:
	.string	"set_space_string"
.LC402:
	.string	"set_string"
.LC403:
	.string	"SetIterator_string"
.LC404:
	.string	"setPrototypeOf_string"
.LC405:
	.string	"SharedArrayBuffer_string"
.LC406:
	.string	"source_string"
.LC407:
	.string	"sourceText_string"
.LC408:
	.string	"stack_string"
.LC409:
	.string	"stackTraceLimit_string"
.LC410:
	.string	"sticky_string"
.LC411:
	.string	"String_string"
.LC412:
	.string	"string_string"
.LC413:
	.string	"string_to_string"
.LC414:
	.string	"symbol_species_string"
.LC415:
	.string	"Symbol_string"
.LC416:
	.string	"symbol_string"
.LC417:
	.string	"SyntaxError_string"
.LC418:
	.string	"target_string"
.LC419:
	.string	"then_string"
.LC420:
	.string	"this_function_string"
.LC421:
	.string	"this_string"
.LC422:
	.string	"throw_string"
.LC423:
	.string	"timed_out"
.LC424:
	.string	"toJSON_string"
.LC425:
	.string	"toString_string"
.LC426:
	.string	"true_string"
.LC427:
	.string	"TypeError_string"
.LC428:
	.string	"Uint16Array_string"
.LC429:
	.string	"Uint32Array_string"
.LC430:
	.string	"Uint8Array_string"
.LC431:
	.string	"Uint8ClampedArray_string"
.LC432:
	.string	"undefined_string"
.LC433:
	.string	"undefined_to_string"
.LC434:
	.string	"unicode_string"
.LC435:
	.string	"URIError_string"
.LC436:
	.string	"value_string"
.LC437:
	.string	"valueOf_string"
.LC438:
	.string	"WeakMap_string"
.LC439:
	.string	"WeakRef_string"
.LC440:
	.string	"WeakSet_string"
.LC441:
	.string	"week_string"
.LC442:
	.string	"word_string"
.LC443:
	.string	"writable_string"
.LC444:
	.string	"zero_string"
.LC445:
	.string	"call_site_frame_array_symbol"
.LC446:
	.string	"call_site_frame_index_symbol"
.LC447:
	.string	"console_context_id_symbol"
.LC448:
	.string	"console_context_name_symbol"
.LC449:
	.string	"class_fields_symbol"
.LC450:
	.string	"class_positions_symbol"
.LC451:
	.string	"detailed_stack_trace_symbol"
.LC452:
	.string	"elements_transition_symbol"
.LC453:
	.string	"error_end_pos_symbol"
.LC454:
	.string	"error_script_symbol"
.LC455:
	.string	"error_start_pos_symbol"
.LC456:
	.string	"frozen_symbol"
.LC457:
	.string	"generic_symbol"
.LC458:
	.string	"home_object_symbol"
.LC459:
	.string	"interpreter_trampoline_symbol"
.LC460:
	.string	"megamorphic_symbol"
.LC461:
	.string	"native_context_index_symbol"
.LC462:
	.string	"nonextensible_symbol"
.LC463:
	.string	"not_mapped_symbol"
.LC464:
	.string	"premonomorphic_symbol"
.LC465:
	.string	"promise_debug_marker_symbol"
	.section	.rodata.str1.8
	.align 8
.LC466:
	.string	"promise_forwarding_handler_symbol"
	.section	.rodata.str1.1
.LC467:
	.string	"promise_handled_by_symbol"
.LC468:
	.string	"sealed_symbol"
.LC469:
	.string	"stack_trace_symbol"
	.section	.rodata.str1.8
	.align 8
.LC470:
	.string	"strict_function_transition_symbol"
	.section	.rodata.str1.1
.LC471:
	.string	"wasm_exception_tag_symbol"
.LC472:
	.string	"wasm_exception_values_symbol"
.LC473:
	.string	"uninitialized_symbol"
.LC474:
	.string	"async_iterator_symbol"
.LC475:
	.string	"iterator_symbol"
.LC476:
	.string	"intl_fallback_symbol"
.LC477:
	.string	"match_all_symbol"
.LC478:
	.string	"match_symbol"
.LC479:
	.string	"replace_symbol"
.LC480:
	.string	"search_symbol"
.LC481:
	.string	"species_symbol"
.LC482:
	.string	"split_symbol"
.LC483:
	.string	"to_primitive_symbol"
.LC484:
	.string	"unscopables_symbol"
.LC485:
	.string	"has_instance_symbol"
.LC486:
	.string	"is_concat_spreadable_symbol"
.LC487:
	.string	"to_string_tag_symbol"
.LC488:
	.string	"access_check_info_map"
.LC489:
	.string	"accessor_info_map"
.LC490:
	.string	"accessor_pair_map"
.LC491:
	.string	"aliased_arguments_entry_map"
.LC492:
	.string	"allocation_memento_map"
	.section	.rodata.str1.8
	.align 8
.LC493:
	.string	"array_boilerplate_description_map"
	.section	.rodata.str1.1
.LC494:
	.string	"asm_wasm_data_map"
.LC495:
	.string	"async_generator_request_map"
.LC496:
	.string	"class_positions_map"
.LC497:
	.string	"debug_info_map"
.LC498:
	.string	"enum_cache_map"
.LC499:
	.string	"function_template_info_map"
	.section	.rodata.str1.8
	.align 8
.LC500:
	.string	"function_template_rare_data_map"
	.section	.rodata.str1.1
.LC501:
	.string	"interceptor_info_map"
.LC502:
	.string	"interpreter_data_map"
.LC503:
	.string	"object_template_info_map"
.LC504:
	.string	"promise_capability_map"
.LC505:
	.string	"promise_reaction_map"
.LC506:
	.string	"prototype_info_map"
.LC507:
	.string	"script_map"
	.section	.rodata.str1.8
	.align 8
.LC508:
	.string	"source_position_table_with_frame_cache_map"
	.section	.rodata.str1.1
.LC509:
	.string	"module_info_entry_map"
.LC510:
	.string	"stack_frame_info_map"
.LC511:
	.string	"stack_trace_frame_map"
	.section	.rodata.str1.8
	.align 8
.LC512:
	.string	"template_object_description_map"
	.section	.rodata.str1.1
.LC513:
	.string	"tuple2_map"
.LC514:
	.string	"tuple3_map"
.LC515:
	.string	"wasm_capi_function_data_map"
.LC516:
	.string	"wasm_debug_info_map"
.LC517:
	.string	"wasm_exception_tag_map"
	.section	.rodata.str1.8
	.align 8
.LC518:
	.string	"wasm_exported_function_data_map"
	.align 8
.LC519:
	.string	"wasm_indirect_function_table_map"
	.section	.rodata.str1.1
.LC520:
	.string	"wasm_js_function_data_map"
.LC521:
	.string	"callable_task_map"
.LC522:
	.string	"callback_task_map"
	.section	.rodata.str1.8
	.align 8
.LC523:
	.string	"promise_fulfill_reaction_job_task_map"
	.align 8
.LC524:
	.string	"promise_reject_reaction_job_task_map"
	.align 8
.LC525:
	.string	"promise_resolve_thenable_job_task_map"
	.section	.rodata.str1.1
.LC526:
	.string	"sort_state_map"
.LC527:
	.string	"internal_class_map"
.LC528:
	.string	"smi_pair_map"
.LC529:
	.string	"smi_box_map"
.LC530:
	.string	"allocation_site_map"
	.section	.rodata.str1.8
	.align 8
.LC531:
	.string	"allocation_site_without_weaknext_map"
	.section	.rodata.str1.1
.LC532:
	.string	"load_handler1_map"
.LC533:
	.string	"load_handler2_map"
.LC534:
	.string	"load_handler3_map"
.LC535:
	.string	"store_handler0_map"
.LC536:
	.string	"store_handler1_map"
.LC537:
	.string	"store_handler2_map"
.LC538:
	.string	"store_handler3_map"
.LC539:
	.string	"arguments_iterator_accessor"
.LC540:
	.string	"array_length_accessor"
	.section	.rodata.str1.8
	.align 8
.LC541:
	.string	"bound_function_length_accessor"
	.section	.rodata.str1.1
.LC542:
	.string	"bound_function_name_accessor"
.LC543:
	.string	"error_stack_accessor"
.LC544:
	.string	"function_arguments_accessor"
.LC545:
	.string	"function_caller_accessor"
.LC546:
	.string	"function_name_accessor"
.LC547:
	.string	"function_length_accessor"
.LC548:
	.string	"function_prototype_accessor"
.LC549:
	.string	"string_length_accessor"
.LC550:
	.string	"external_map"
.LC551:
	.string	"message_object_map"
.LC552:
	.string	"empty_script"
.LC553:
	.string	"many_closures_cell"
	.section	.rodata.str1.8
	.align 8
.LC554:
	.string	"invalid_prototype_validity_cell"
	.section	.rodata.str1.1
.LC555:
	.string	"array_constructor_protector"
.LC556:
	.string	"no_elements_protector"
	.section	.rodata.str1.8
	.align 8
.LC557:
	.string	"is_concat_spreadable_protector"
	.section	.rodata.str1.1
.LC558:
	.string	"array_species_protector"
.LC559:
	.string	"typed_array_species_protector"
.LC560:
	.string	"promise_species_protector"
.LC561:
	.string	"string_length_protector"
.LC562:
	.string	"array_iterator_protector"
	.section	.rodata.str1.8
	.align 8
.LC563:
	.string	"array_buffer_detaching_protector"
	.section	.rodata.str1.1
.LC564:
	.string	"promise_hook_protector"
.LC565:
	.string	"promise_resolve_protector"
.LC566:
	.string	"map_iterator_protector"
.LC567:
	.string	"promise_then_protector"
.LC568:
	.string	"set_iterator_protector"
.LC569:
	.string	"string_iterator_protector"
.LC570:
	.string	"single_character_string_cache"
.LC571:
	.string	"string_split_cache"
.LC572:
	.string	"regexp_multiple_cache"
.LC573:
	.string	"builtins_constants_table"
.LC574:
	.string	"number_string_cache"
.LC575:
	.string	"public_symbol_table"
.LC576:
	.string	"api_symbol_table"
.LC577:
	.string	"api_private_symbol_table"
.LC578:
	.string	"script_list"
.LC579:
	.string	"materialized_objects"
.LC580:
	.string	"detached_contexts"
.LC581:
	.string	"retaining_path_targets"
.LC582:
	.string	"retained_maps"
	.section	.rodata.str1.8
	.align 8
.LC583:
	.string	"feedback_vectors_for_profiling_tools"
	.section	.rodata.str1.1
.LC584:
	.string	"serialized_objects"
.LC585:
	.string	"serialized_global_proxy_sizes"
.LC586:
	.string	"message_listeners"
.LC587:
	.string	"current_microtask"
.LC588:
	.string	"dirty_js_finalization_groups"
.LC589:
	.string	"weak_refs_keep_during_job"
	.section	.rodata.str1.8
	.align 8
.LC590:
	.string	"interpreter_entry_trampoline_for_profiling"
	.align 8
.LC591:
	.string	"pending_optimize_for_test_bytecode"
	.section	.rodata.str1.1
.LC592:
	.string	"string_table"
.LC593:
	.string	"last_script_id"
.LC594:
	.string	"last_debugging_id"
.LC595:
	.string	"next_template_serial_number"
	.section	.rodata.str1.8
	.align 8
.LC596:
	.string	"arguments_adaptor_deopt_pc_offset"
	.align 8
.LC597:
	.string	"construct_stub_create_deopt_pc_offset"
	.align 8
.LC598:
	.string	"construct_stub_invoke_deopt_pc_offset"
	.align 8
.LC599:
	.string	"interpreter_entry_return_pc_offset"
	.section	.data.rel.local._ZN2v88internal10RootsTable11root_names_E,"aw"
	.align 32
	.type	_ZN2v88internal10RootsTable11root_names_E, @object
	.size	_ZN2v88internal10RootsTable11root_names_E, 4800
_ZN2v88internal10RootsTable11root_names_E:
	.quad	.LC0
	.quad	.LC1
	.quad	.LC2
	.quad	.LC3
	.quad	.LC4
	.quad	.LC5
	.quad	.LC6
	.quad	.LC7
	.quad	.LC8
	.quad	.LC9
	.quad	.LC10
	.quad	.LC11
	.quad	.LC12
	.quad	.LC13
	.quad	.LC14
	.quad	.LC15
	.quad	.LC16
	.quad	.LC17
	.quad	.LC18
	.quad	.LC19
	.quad	.LC20
	.quad	.LC21
	.quad	.LC22
	.quad	.LC23
	.quad	.LC24
	.quad	.LC25
	.quad	.LC26
	.quad	.LC27
	.quad	.LC28
	.quad	.LC29
	.quad	.LC30
	.quad	.LC31
	.quad	.LC32
	.quad	.LC33
	.quad	.LC34
	.quad	.LC35
	.quad	.LC36
	.quad	.LC37
	.quad	.LC38
	.quad	.LC39
	.quad	.LC40
	.quad	.LC41
	.quad	.LC42
	.quad	.LC43
	.quad	.LC44
	.quad	.LC45
	.quad	.LC46
	.quad	.LC47
	.quad	.LC48
	.quad	.LC49
	.quad	.LC50
	.quad	.LC51
	.quad	.LC52
	.quad	.LC53
	.quad	.LC54
	.quad	.LC55
	.quad	.LC56
	.quad	.LC57
	.quad	.LC58
	.quad	.LC59
	.quad	.LC60
	.quad	.LC61
	.quad	.LC62
	.quad	.LC63
	.quad	.LC64
	.quad	.LC65
	.quad	.LC66
	.quad	.LC67
	.quad	.LC68
	.quad	.LC69
	.quad	.LC70
	.quad	.LC71
	.quad	.LC72
	.quad	.LC73
	.quad	.LC74
	.quad	.LC75
	.quad	.LC76
	.quad	.LC77
	.quad	.LC78
	.quad	.LC79
	.quad	.LC80
	.quad	.LC81
	.quad	.LC82
	.quad	.LC83
	.quad	.LC84
	.quad	.LC85
	.quad	.LC86
	.quad	.LC87
	.quad	.LC88
	.quad	.LC89
	.quad	.LC90
	.quad	.LC91
	.quad	.LC92
	.quad	.LC93
	.quad	.LC94
	.quad	.LC95
	.quad	.LC96
	.quad	.LC97
	.quad	.LC98
	.quad	.LC99
	.quad	.LC100
	.quad	.LC101
	.quad	.LC102
	.quad	.LC103
	.quad	.LC104
	.quad	.LC105
	.quad	.LC106
	.quad	.LC107
	.quad	.LC108
	.quad	.LC109
	.quad	.LC110
	.quad	.LC111
	.quad	.LC112
	.quad	.LC113
	.quad	.LC114
	.quad	.LC115
	.quad	.LC116
	.quad	.LC117
	.quad	.LC118
	.quad	.LC119
	.quad	.LC120
	.quad	.LC121
	.quad	.LC122
	.quad	.LC123
	.quad	.LC124
	.quad	.LC125
	.quad	.LC126
	.quad	.LC127
	.quad	.LC128
	.quad	.LC129
	.quad	.LC130
	.quad	.LC131
	.quad	.LC132
	.quad	.LC133
	.quad	.LC134
	.quad	.LC135
	.quad	.LC136
	.quad	.LC137
	.quad	.LC138
	.quad	.LC139
	.quad	.LC140
	.quad	.LC141
	.quad	.LC142
	.quad	.LC143
	.quad	.LC144
	.quad	.LC145
	.quad	.LC146
	.quad	.LC147
	.quad	.LC148
	.quad	.LC149
	.quad	.LC150
	.quad	.LC151
	.quad	.LC152
	.quad	.LC153
	.quad	.LC154
	.quad	.LC155
	.quad	.LC156
	.quad	.LC157
	.quad	.LC158
	.quad	.LC159
	.quad	.LC160
	.quad	.LC161
	.quad	.LC162
	.quad	.LC163
	.quad	.LC164
	.quad	.LC165
	.quad	.LC166
	.quad	.LC167
	.quad	.LC168
	.quad	.LC169
	.quad	.LC170
	.quad	.LC171
	.quad	.LC172
	.quad	.LC173
	.quad	.LC174
	.quad	.LC175
	.quad	.LC176
	.quad	.LC177
	.quad	.LC178
	.quad	.LC179
	.quad	.LC180
	.quad	.LC181
	.quad	.LC182
	.quad	.LC183
	.quad	.LC184
	.quad	.LC185
	.quad	.LC186
	.quad	.LC187
	.quad	.LC188
	.quad	.LC189
	.quad	.LC190
	.quad	.LC191
	.quad	.LC192
	.quad	.LC193
	.quad	.LC194
	.quad	.LC195
	.quad	.LC196
	.quad	.LC197
	.quad	.LC198
	.quad	.LC199
	.quad	.LC200
	.quad	.LC201
	.quad	.LC202
	.quad	.LC203
	.quad	.LC204
	.quad	.LC205
	.quad	.LC206
	.quad	.LC207
	.quad	.LC208
	.quad	.LC209
	.quad	.LC210
	.quad	.LC211
	.quad	.LC212
	.quad	.LC213
	.quad	.LC214
	.quad	.LC215
	.quad	.LC216
	.quad	.LC217
	.quad	.LC218
	.quad	.LC219
	.quad	.LC220
	.quad	.LC221
	.quad	.LC222
	.quad	.LC223
	.quad	.LC224
	.quad	.LC225
	.quad	.LC226
	.quad	.LC227
	.quad	.LC228
	.quad	.LC229
	.quad	.LC230
	.quad	.LC231
	.quad	.LC232
	.quad	.LC233
	.quad	.LC234
	.quad	.LC235
	.quad	.LC236
	.quad	.LC237
	.quad	.LC238
	.quad	.LC239
	.quad	.LC240
	.quad	.LC241
	.quad	.LC242
	.quad	.LC243
	.quad	.LC244
	.quad	.LC245
	.quad	.LC246
	.quad	.LC247
	.quad	.LC248
	.quad	.LC249
	.quad	.LC250
	.quad	.LC251
	.quad	.LC252
	.quad	.LC253
	.quad	.LC254
	.quad	.LC255
	.quad	.LC256
	.quad	.LC257
	.quad	.LC258
	.quad	.LC259
	.quad	.LC260
	.quad	.LC261
	.quad	.LC262
	.quad	.LC263
	.quad	.LC264
	.quad	.LC265
	.quad	.LC266
	.quad	.LC267
	.quad	.LC268
	.quad	.LC269
	.quad	.LC270
	.quad	.LC271
	.quad	.LC272
	.quad	.LC273
	.quad	.LC274
	.quad	.LC275
	.quad	.LC276
	.quad	.LC277
	.quad	.LC278
	.quad	.LC279
	.quad	.LC280
	.quad	.LC281
	.quad	.LC282
	.quad	.LC283
	.quad	.LC284
	.quad	.LC285
	.quad	.LC286
	.quad	.LC287
	.quad	.LC288
	.quad	.LC289
	.quad	.LC290
	.quad	.LC291
	.quad	.LC292
	.quad	.LC293
	.quad	.LC294
	.quad	.LC295
	.quad	.LC296
	.quad	.LC297
	.quad	.LC298
	.quad	.LC299
	.quad	.LC300
	.quad	.LC301
	.quad	.LC302
	.quad	.LC303
	.quad	.LC304
	.quad	.LC305
	.quad	.LC306
	.quad	.LC307
	.quad	.LC308
	.quad	.LC309
	.quad	.LC310
	.quad	.LC311
	.quad	.LC312
	.quad	.LC313
	.quad	.LC314
	.quad	.LC315
	.quad	.LC316
	.quad	.LC317
	.quad	.LC318
	.quad	.LC319
	.quad	.LC320
	.quad	.LC321
	.quad	.LC322
	.quad	.LC323
	.quad	.LC324
	.quad	.LC325
	.quad	.LC326
	.quad	.LC327
	.quad	.LC328
	.quad	.LC329
	.quad	.LC330
	.quad	.LC331
	.quad	.LC332
	.quad	.LC333
	.quad	.LC334
	.quad	.LC335
	.quad	.LC336
	.quad	.LC337
	.quad	.LC338
	.quad	.LC339
	.quad	.LC340
	.quad	.LC341
	.quad	.LC342
	.quad	.LC343
	.quad	.LC344
	.quad	.LC345
	.quad	.LC346
	.quad	.LC347
	.quad	.LC348
	.quad	.LC349
	.quad	.LC350
	.quad	.LC351
	.quad	.LC352
	.quad	.LC353
	.quad	.LC354
	.quad	.LC355
	.quad	.LC356
	.quad	.LC357
	.quad	.LC358
	.quad	.LC359
	.quad	.LC360
	.quad	.LC361
	.quad	.LC362
	.quad	.LC363
	.quad	.LC364
	.quad	.LC365
	.quad	.LC366
	.quad	.LC367
	.quad	.LC368
	.quad	.LC369
	.quad	.LC370
	.quad	.LC371
	.quad	.LC372
	.quad	.LC373
	.quad	.LC374
	.quad	.LC375
	.quad	.LC376
	.quad	.LC377
	.quad	.LC378
	.quad	.LC379
	.quad	.LC380
	.quad	.LC381
	.quad	.LC382
	.quad	.LC383
	.quad	.LC384
	.quad	.LC385
	.quad	.LC386
	.quad	.LC387
	.quad	.LC388
	.quad	.LC389
	.quad	.LC390
	.quad	.LC391
	.quad	.LC392
	.quad	.LC393
	.quad	.LC394
	.quad	.LC395
	.quad	.LC396
	.quad	.LC397
	.quad	.LC398
	.quad	.LC399
	.quad	.LC400
	.quad	.LC401
	.quad	.LC402
	.quad	.LC403
	.quad	.LC404
	.quad	.LC405
	.quad	.LC406
	.quad	.LC407
	.quad	.LC408
	.quad	.LC409
	.quad	.LC410
	.quad	.LC411
	.quad	.LC412
	.quad	.LC413
	.quad	.LC414
	.quad	.LC415
	.quad	.LC416
	.quad	.LC417
	.quad	.LC418
	.quad	.LC419
	.quad	.LC420
	.quad	.LC421
	.quad	.LC422
	.quad	.LC423
	.quad	.LC424
	.quad	.LC425
	.quad	.LC426
	.quad	.LC427
	.quad	.LC428
	.quad	.LC429
	.quad	.LC430
	.quad	.LC431
	.quad	.LC432
	.quad	.LC433
	.quad	.LC434
	.quad	.LC435
	.quad	.LC436
	.quad	.LC437
	.quad	.LC438
	.quad	.LC439
	.quad	.LC440
	.quad	.LC441
	.quad	.LC442
	.quad	.LC443
	.quad	.LC444
	.quad	.LC445
	.quad	.LC446
	.quad	.LC447
	.quad	.LC448
	.quad	.LC449
	.quad	.LC450
	.quad	.LC451
	.quad	.LC452
	.quad	.LC453
	.quad	.LC454
	.quad	.LC455
	.quad	.LC456
	.quad	.LC457
	.quad	.LC458
	.quad	.LC459
	.quad	.LC460
	.quad	.LC461
	.quad	.LC462
	.quad	.LC463
	.quad	.LC464
	.quad	.LC465
	.quad	.LC466
	.quad	.LC467
	.quad	.LC468
	.quad	.LC469
	.quad	.LC470
	.quad	.LC471
	.quad	.LC472
	.quad	.LC473
	.quad	.LC474
	.quad	.LC475
	.quad	.LC476
	.quad	.LC477
	.quad	.LC478
	.quad	.LC479
	.quad	.LC480
	.quad	.LC481
	.quad	.LC482
	.quad	.LC483
	.quad	.LC484
	.quad	.LC485
	.quad	.LC486
	.quad	.LC487
	.quad	.LC488
	.quad	.LC489
	.quad	.LC490
	.quad	.LC491
	.quad	.LC492
	.quad	.LC493
	.quad	.LC494
	.quad	.LC495
	.quad	.LC496
	.quad	.LC497
	.quad	.LC498
	.quad	.LC499
	.quad	.LC500
	.quad	.LC501
	.quad	.LC502
	.quad	.LC503
	.quad	.LC504
	.quad	.LC505
	.quad	.LC506
	.quad	.LC507
	.quad	.LC508
	.quad	.LC509
	.quad	.LC510
	.quad	.LC511
	.quad	.LC512
	.quad	.LC513
	.quad	.LC514
	.quad	.LC515
	.quad	.LC516
	.quad	.LC517
	.quad	.LC518
	.quad	.LC519
	.quad	.LC520
	.quad	.LC521
	.quad	.LC522
	.quad	.LC523
	.quad	.LC524
	.quad	.LC525
	.quad	.LC526
	.quad	.LC527
	.quad	.LC528
	.quad	.LC529
	.quad	.LC530
	.quad	.LC531
	.quad	.LC532
	.quad	.LC533
	.quad	.LC534
	.quad	.LC535
	.quad	.LC536
	.quad	.LC537
	.quad	.LC538
	.quad	.LC539
	.quad	.LC540
	.quad	.LC541
	.quad	.LC542
	.quad	.LC543
	.quad	.LC544
	.quad	.LC545
	.quad	.LC546
	.quad	.LC547
	.quad	.LC548
	.quad	.LC549
	.quad	.LC550
	.quad	.LC551
	.quad	.LC552
	.quad	.LC553
	.quad	.LC554
	.quad	.LC555
	.quad	.LC556
	.quad	.LC557
	.quad	.LC558
	.quad	.LC559
	.quad	.LC560
	.quad	.LC561
	.quad	.LC562
	.quad	.LC563
	.quad	.LC564
	.quad	.LC565
	.quad	.LC566
	.quad	.LC567
	.quad	.LC568
	.quad	.LC569
	.quad	.LC570
	.quad	.LC571
	.quad	.LC572
	.quad	.LC573
	.quad	.LC574
	.quad	.LC575
	.quad	.LC576
	.quad	.LC577
	.quad	.LC578
	.quad	.LC579
	.quad	.LC580
	.quad	.LC581
	.quad	.LC582
	.quad	.LC583
	.quad	.LC584
	.quad	.LC585
	.quad	.LC586
	.quad	.LC587
	.quad	.LC588
	.quad	.LC589
	.quad	.LC590
	.quad	.LC591
	.quad	.LC592
	.quad	.LC593
	.quad	.LC594
	.quad	.LC595
	.quad	.LC596
	.quad	.LC597
	.quad	.LC598
	.quad	.LC599
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
