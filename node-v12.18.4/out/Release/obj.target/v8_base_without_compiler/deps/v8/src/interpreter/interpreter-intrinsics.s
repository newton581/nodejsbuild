	.file	"interpreter-intrinsics.cc"
	.text
	.section	.text._ZN2v88internal11interpreter16IntrinsicsHelper11IsSupportedENS0_7Runtime10FunctionIdE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter16IntrinsicsHelper11IsSupportedENS0_7Runtime10FunctionIdE
	.type	_ZN2v88internal11interpreter16IntrinsicsHelper11IsSupportedENS0_7Runtime10FunctionIdE, @function
_ZN2v88internal11interpreter16IntrinsicsHelper11IsSupportedENS0_7Runtime10FunctionIdE:
.LFB5249:
	.cfi_startproc
	endbr64
	leal	-467(%rdi), %ecx
	cmpl	$27, %ecx
	ja	.L3
	movl	$1, %eax
	salq	%cl, %rax
	testl	$233832445, %eax
	setne	%al
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5249:
	.size	_ZN2v88internal11interpreter16IntrinsicsHelper11IsSupportedENS0_7Runtime10FunctionIdE, .-_ZN2v88internal11interpreter16IntrinsicsHelper11IsSupportedENS0_7Runtime10FunctionIdE
	.section	.rodata._ZN2v88internal11interpreter16IntrinsicsHelper13FromRuntimeIdENS0_7Runtime10FunctionIdE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal11interpreter16IntrinsicsHelper13FromRuntimeIdENS0_7Runtime10FunctionIdE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter16IntrinsicsHelper13FromRuntimeIdENS0_7Runtime10FunctionIdE
	.type	_ZN2v88internal11interpreter16IntrinsicsHelper13FromRuntimeIdENS0_7Runtime10FunctionIdE, @function
_ZN2v88internal11interpreter16IntrinsicsHelper13FromRuntimeIdENS0_7Runtime10FunctionIdE:
.LFB5250:
	.cfi_startproc
	endbr64
	subl	$467, %edi
	cmpl	$27, %edi
	ja	.L6
	leaq	.L8(%rip), %rdx
	movslq	(%rdx,%rdi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal11interpreter16IntrinsicsHelper13FromRuntimeIdENS0_7Runtime10FunctionIdE,"a",@progbits
	.align 4
	.align 4
.L8:
	.long	.L32-.L8
	.long	.L6-.L8
	.long	.L31-.L8
	.long	.L30-.L8
	.long	.L29-.L8
	.long	.L33-.L8
	.long	.L27-.L8
	.long	.L26-.L8
	.long	.L25-.L8
	.long	.L24-.L8
	.long	.L23-.L8
	.long	.L22-.L8
	.long	.L21-.L8
	.long	.L20-.L8
	.long	.L19-.L8
	.long	.L18-.L8
	.long	.L17-.L8
	.long	.L16-.L8
	.long	.L15-.L8
	.long	.L14-.L8
	.long	.L6-.L8
	.long	.L13-.L8
	.long	.L12-.L8
	.long	.L11-.L8
	.long	.L10-.L8
	.long	.L6-.L8
	.long	.L9-.L8
	.long	.L7-.L8
	.section	.text._ZN2v88internal11interpreter16IntrinsicsHelper13FromRuntimeIdENS0_7Runtime10FunctionIdE
.L6:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movl	$22, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movl	$24, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movl	$23, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movl	$20, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movl	$18, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movl	$16, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movl	$15, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	movl	$21, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	movl	$13, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	movl	$17, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	movl	$11, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	movl	$12, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	movl	$10, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	movl	$9, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	movl	$8, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	movl	$7, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	movl	$6, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	movl	$5, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	movl	$4, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	movl	$3, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	movl	$14, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	movl	$19, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	movl	$2, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE5250:
	.size	_ZN2v88internal11interpreter16IntrinsicsHelper13FromRuntimeIdENS0_7Runtime10FunctionIdE, .-_ZN2v88internal11interpreter16IntrinsicsHelper13FromRuntimeIdENS0_7Runtime10FunctionIdE
	.section	.text._ZN2v88internal11interpreter16IntrinsicsHelper11ToRuntimeIdENS2_11IntrinsicIdE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter16IntrinsicsHelper11ToRuntimeIdENS2_11IntrinsicIdE
	.type	_ZN2v88internal11interpreter16IntrinsicsHelper11ToRuntimeIdENS2_11IntrinsicIdE, @function
_ZN2v88internal11interpreter16IntrinsicsHelper11ToRuntimeIdENS2_11IntrinsicIdE:
.LFB5251:
	.cfi_startproc
	endbr64
	cmpl	$24, %edi
	ja	.L37
	movl	%edi, %edi
	leaq	CSWTCH.4(%rip), %rax
	movl	(%rax,%rdi,4), %eax
	ret
.L37:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE5251:
	.size	_ZN2v88internal11interpreter16IntrinsicsHelper11ToRuntimeIdENS2_11IntrinsicIdE, .-_ZN2v88internal11interpreter16IntrinsicsHelper11ToRuntimeIdENS2_11IntrinsicIdE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal11interpreter16IntrinsicsHelper11IsSupportedENS0_7Runtime10FunctionIdE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal11interpreter16IntrinsicsHelper11IsSupportedENS0_7Runtime10FunctionIdE, @function
_GLOBAL__sub_I__ZN2v88internal11interpreter16IntrinsicsHelper11IsSupportedENS0_7Runtime10FunctionIdE:
.LFB6047:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE6047:
	.size	_GLOBAL__sub_I__ZN2v88internal11interpreter16IntrinsicsHelper11IsSupportedENS0_7Runtime10FunctionIdE, .-_GLOBAL__sub_I__ZN2v88internal11interpreter16IntrinsicsHelper11IsSupportedENS0_7Runtime10FunctionIdE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal11interpreter16IntrinsicsHelper11IsSupportedENS0_7Runtime10FunctionIdE
	.section	.rodata.CSWTCH.4,"a"
	.align 32
	.type	CSWTCH.4, @object
	.size	CSWTCH.4, 100
CSWTCH.4:
	.long	470
	.long	471
	.long	472
	.long	473
	.long	474
	.long	475
	.long	476
	.long	477
	.long	478
	.long	479
	.long	480
	.long	482
	.long	481
	.long	484
	.long	469
	.long	486
	.long	488
	.long	483
	.long	489
	.long	467
	.long	490
	.long	485
	.long	494
	.long	491
	.long	493
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
