	.file	"bytecode-array-writer.cc"
	.text
	.section	.rodata._ZN2v88internal11interpreter19BytecodeArrayWriter22MaybeElideLastBytecodeENS1_8BytecodeEb.part.0.str1.1,"aMS",@progbits,1
.LC0:
	.string	"vector::_M_default_append"
	.section	.text._ZN2v88internal11interpreter19BytecodeArrayWriter22MaybeElideLastBytecodeENS1_8BytecodeEb.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter19BytecodeArrayWriter22MaybeElideLastBytecodeENS1_8BytecodeEb.part.0, @function
_ZN2v88internal11interpreter19BytecodeArrayWriter22MaybeElideLastBytecodeENS1_8BytecodeEb.part.0:
.LFB22755:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movzbl	120(%rdi), %ecx
	cmpb	$37, %cl
	jbe	.L2
	movq	32(%rdi), %r14
	subq	24(%rdi), %r14
.L3:
	movb	%r12b, 120(%rbx)
	movq	%r14, 128(%rbx)
	movb	%al, 136(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	movabsq	$137691133952, %r8
	movzbl	%sil, %edx
	movq	24(%rdi), %rsi
	movq	32(%rdi), %rdi
	movq	%rdi, %r14
	subq	%rsi, %r14
	btq	%rcx, %r8
	jnc	.L3
	leaq	_ZN2v88internal11interpreter9Bytecodes15kAccumulatorUseE(%rip), %rcx
	cmpb	$2, (%rcx,%rdx)
	jne	.L3
	movzbl	136(%rbx), %eax
	movl	%eax, %edx
	andb	%r13b, %dl
	jne	.L22
	movq	128(%rbx), %r15
	cmpq	%r14, %r15
	ja	.L36
	jnb	.L5
	addq	%r15, %rsi
	cmpq	%rdi, %rsi
	je	.L5
	movq	%rsi, 32(%rbx)
	movq	%r15, %r14
.L5:
	orl	%r13d, %eax
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L22:
	movl	%edx, %eax
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L36:
	movq	%r15, %rdx
	subq	%r14, %rdx
	je	.L5
	movq	40(%rbx), %rax
	movl	$2147483647, %esi
	movq	%rsi, %rcx
	subq	%rdi, %rax
	subq	%r14, %rcx
	cmpq	%rax, %rdx
	ja	.L6
	xorl	%esi, %esi
	movq	%rdx, -56(%rbp)
	call	memset@PLT
	movq	-56(%rbp), %rdx
	leaq	(%rax,%rdx), %r14
	movzbl	136(%rbx), %eax
	movq	%r14, 32(%rbx)
	subq	24(%rbx), %r14
	jmp	.L5
.L6:
	cmpq	%rcx, %rdx
	ja	.L37
	cmpq	%r14, %rdx
	movq	%r14, %rcx
	cmovnb	%rdx, %rcx
	addq	%r14, %rcx
	jc	.L9
	cmpq	$2147483647, %rcx
	cmova	%rsi, %rcx
	leaq	7(%rcx), %rsi
	andq	$-8, %rsi
.L21:
	movq	16(%rbx), %rdi
	movq	16(%rdi), %r8
	movq	24(%rdi), %rax
	subq	%r8, %rax
	cmpq	%rsi, %rax
	jb	.L38
	addq	%r8, %rsi
	movq	%rsi, 16(%rdi)
.L12:
	xorl	%esi, %esi
	leaq	(%r8,%r14), %rdi
	movq	%rcx, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	memset@PLT
	movq	32(%rbx), %rdx
	movq	24(%rbx), %rsi
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %rcx
	cmpq	%rsi, %rdx
	je	.L17
	leaq	-1(%rdx), %rax
	subq	%rsi, %rax
	cmpq	$14, %rax
	jbe	.L14
	leaq	15(%rsi), %rax
	subq	%r8, %rax
	cmpq	$30, %rax
	jbe	.L14
	movq	%rdx, %r9
	movq	%rsi, %r10
	movq	%r8, %rax
	subq	%rsi, %r9
	subq	%r8, %r10
	movq	%r9, %rdi
	andq	$-16, %rdi
	addq	%r8, %rdi
	.p2align 4,,10
	.p2align 3
.L15:
	movdqu	(%rax,%r10), %xmm0
	addq	$16, %rax
	movups	%xmm0, -16(%rax)
	cmpq	%rdi, %rax
	jne	.L15
	movq	%r9, %rdi
	andq	$-16, %rdi
	leaq	(%rsi,%rdi), %rax
	leaq	(%r8,%rdi), %rsi
	cmpq	%r9, %rdi
	je	.L17
	movzbl	(%rax), %edi
	movb	%dil, (%rsi)
	leaq	1(%rax), %rdi
	cmpq	%rdi, %rdx
	je	.L17
	movzbl	1(%rax), %edi
	movb	%dil, 1(%rsi)
	leaq	2(%rax), %rdi
	cmpq	%rdi, %rdx
	je	.L17
	movzbl	2(%rax), %edi
	movb	%dil, 2(%rsi)
	leaq	3(%rax), %rdi
	cmpq	%rdi, %rdx
	je	.L17
	movzbl	3(%rax), %edi
	movb	%dil, 3(%rsi)
	leaq	4(%rax), %rdi
	cmpq	%rdi, %rdx
	je	.L17
	movzbl	4(%rax), %edi
	movb	%dil, 4(%rsi)
	leaq	5(%rax), %rdi
	cmpq	%rdi, %rdx
	je	.L17
	movzbl	5(%rax), %edi
	movb	%dil, 5(%rsi)
	leaq	6(%rax), %rdi
	cmpq	%rdi, %rdx
	je	.L17
	movzbl	6(%rax), %edi
	movb	%dil, 6(%rsi)
	leaq	7(%rax), %rdi
	cmpq	%rdi, %rdx
	je	.L17
	movzbl	7(%rax), %edi
	movb	%dil, 7(%rsi)
	leaq	8(%rax), %rdi
	cmpq	%rdi, %rdx
	je	.L17
	movzbl	8(%rax), %edi
	movb	%dil, 8(%rsi)
	leaq	9(%rax), %rdi
	cmpq	%rdi, %rdx
	je	.L17
	movzbl	9(%rax), %edi
	movb	%dil, 9(%rsi)
	leaq	10(%rax), %rdi
	cmpq	%rdi, %rdx
	je	.L17
	movzbl	10(%rax), %edi
	movb	%dil, 10(%rsi)
	leaq	11(%rax), %rdi
	cmpq	%rdi, %rdx
	je	.L17
	movzbl	11(%rax), %edi
	movb	%dil, 11(%rsi)
	leaq	12(%rax), %rdi
	cmpq	%rdi, %rdx
	je	.L17
	movzbl	12(%rax), %edi
	movb	%dil, 12(%rsi)
	leaq	13(%rax), %rdi
	cmpq	%rdi, %rdx
	je	.L17
	movzbl	13(%rax), %edi
	movb	%dil, 13(%rsi)
	leaq	14(%rax), %rdi
	cmpq	%rdi, %rdx
	je	.L17
	movzbl	14(%rax), %eax
	movb	%al, 14(%rsi)
.L17:
	leaq	(%r8,%r15), %rax
	addq	%r8, %rcx
	movq	%r8, 24(%rbx)
	movq	%r15, %r14
	movq	%rax, 32(%rbx)
	movzbl	136(%rbx), %eax
	movq	%rcx, 40(%rbx)
	jmp	.L5
.L14:
	movq	%rdx, %rax
	movq	%r8, %rdx
	subq	%rsi, %rax
	subq	%r8, %rsi
	addq	%r8, %rax
.L19:
	movzbl	(%rdx,%rsi), %edi
	addq	$1, %rdx
	movb	%dil, -1(%rdx)
	cmpq	%rax, %rdx
	jne	.L19
	jmp	.L17
.L38:
	movq	%rcx, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rcx
	movq	%rax, %r8
	jmp	.L12
.L9:
	movl	$2147483648, %esi
	movl	$2147483647, %ecx
	jmp	.L21
.L37:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE22755:
	.size	_ZN2v88internal11interpreter19BytecodeArrayWriter22MaybeElideLastBytecodeENS1_8BytecodeEb.part.0, .-_ZN2v88internal11interpreter19BytecodeArrayWriter22MaybeElideLastBytecodeENS1_8BytecodeEb.part.0
	.section	.text._ZN2v88internal11interpreter19BytecodeArrayWriterC2EPNS0_4ZoneEPNS1_20ConstantArrayBuilderENS0_26SourcePositionTableBuilder13RecordingModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19BytecodeArrayWriterC2EPNS0_4ZoneEPNS1_20ConstantArrayBuilderENS0_26SourcePositionTableBuilder13RecordingModeE
	.type	_ZN2v88internal11interpreter19BytecodeArrayWriterC2EPNS0_4ZoneEPNS1_20ConstantArrayBuilderENS0_26SourcePositionTableBuilder13RecordingModeE, @function
_ZN2v88internal11interpreter19BytecodeArrayWriterC2EPNS0_4ZoneEPNS1_20ConstantArrayBuilderENS0_26SourcePositionTableBuilder13RecordingModeE:
.LFB18580:
	.cfi_startproc
	endbr64
	movabsq	$140183437574271, %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movl	%ecx, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	addq	$56, %rdi
	movq	%rax, -56(%rdi)
	movl	$2139062143, -48(%rdi)
	movq	%r8, -40(%rdi)
	movq	$0, -32(%rdi)
	movq	$0, -24(%rdi)
	movq	$0, -16(%rdi)
	movl	$0, -8(%rdi)
	call	_ZN2v88internal26SourcePositionTableBuilderC1ENS1_13RecordingModeE@PLT
	movzbl	_ZN2v88internal42FLAG_ignition_elide_noneffectful_bytecodesE(%rip), %eax
	movq	%r12, 112(%rbx)
	movq	24(%rbx), %r12
	movb	$-74, 120(%rbx)
	movb	%al, 137(%rbx)
	movq	40(%rbx), %rax
	movq	$0, 128(%rbx)
	subq	%r12, %rax
	movb	$0, 136(%rbx)
	movb	$0, 138(%rbx)
	cmpq	$511, %rax
	jbe	.L58
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	.cfi_restore_state
	movq	16(%rbx), %rdi
	movq	32(%rbx), %r13
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	movq	%r13, %r14
	subq	%r12, %r14
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L59
	leaq	512(%rax), %rcx
	movq	%rcx, 16(%rdi)
.L42:
	cmpq	%r13, %r12
	je	.L43
	leaq	-1(%r13), %rdx
	subq	%r12, %rdx
	cmpq	$14, %rdx
	jbe	.L44
	leaq	15(%r12), %rdx
	subq	%rax, %rdx
	cmpq	$30, %rdx
	jbe	.L44
	movq	%r13, %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	subq	%r12, %r8
	subq	%rax, %rdi
	movq	%r8, %rsi
	andq	$-16, %rsi
	addq	%rax, %rsi
	.p2align 4,,10
	.p2align 3
.L45:
	movdqu	(%rdx,%rdi), %xmm0
	addq	$16, %rdx
	movups	%xmm0, -16(%rdx)
	cmpq	%rsi, %rdx
	jne	.L45
	movq	%r8, %rsi
	andq	$-16, %rsi
	addq	%rsi, %r12
	leaq	(%rax,%rsi), %rdx
	cmpq	%rsi, %r8
	je	.L43
	movzbl	(%r12), %esi
	movb	%sil, (%rdx)
	leaq	1(%r12), %rsi
	cmpq	%rsi, %r13
	je	.L43
	movzbl	1(%r12), %esi
	movb	%sil, 1(%rdx)
	leaq	2(%r12), %rsi
	cmpq	%rsi, %r13
	je	.L43
	movzbl	2(%r12), %esi
	movb	%sil, 2(%rdx)
	leaq	3(%r12), %rsi
	cmpq	%rsi, %r13
	je	.L43
	movzbl	3(%r12), %esi
	movb	%sil, 3(%rdx)
	leaq	4(%r12), %rsi
	cmpq	%rsi, %r13
	je	.L43
	movzbl	4(%r12), %esi
	movb	%sil, 4(%rdx)
	leaq	5(%r12), %rsi
	cmpq	%rsi, %r13
	je	.L43
	movzbl	5(%r12), %esi
	movb	%sil, 5(%rdx)
	leaq	6(%r12), %rsi
	cmpq	%rsi, %r13
	je	.L43
	movzbl	6(%r12), %esi
	movb	%sil, 6(%rdx)
	leaq	7(%r12), %rsi
	cmpq	%rsi, %r13
	je	.L43
	movzbl	7(%r12), %esi
	movb	%sil, 7(%rdx)
	leaq	8(%r12), %rsi
	cmpq	%rsi, %r13
	je	.L43
	movzbl	8(%r12), %esi
	movb	%sil, 8(%rdx)
	leaq	9(%r12), %rsi
	cmpq	%rsi, %r13
	je	.L43
	movzbl	9(%r12), %esi
	movb	%sil, 9(%rdx)
	leaq	10(%r12), %rsi
	cmpq	%rsi, %r13
	je	.L43
	movzbl	10(%r12), %esi
	movb	%sil, 10(%rdx)
	leaq	11(%r12), %rsi
	cmpq	%rsi, %r13
	je	.L43
	movzbl	11(%r12), %esi
	movb	%sil, 11(%rdx)
	leaq	12(%r12), %rsi
	cmpq	%rsi, %r13
	je	.L43
	movzbl	12(%r12), %esi
	movb	%sil, 12(%rdx)
	leaq	13(%r12), %rsi
	cmpq	%rsi, %r13
	je	.L43
	movzbl	13(%r12), %esi
	movb	%sil, 13(%rdx)
	leaq	14(%r12), %rsi
	cmpq	%rsi, %r13
	je	.L43
	movzbl	14(%r12), %esi
	movb	%sil, 14(%rdx)
.L43:
	movq	%rax, 24(%rbx)
	addq	%r14, %rax
	movq	%rax, 32(%rbx)
	movq	%rcx, 40(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	subq	%r12, %r13
	movq	%rax, %rdx
	subq	%rax, %r12
	addq	%rax, %r13
	.p2align 4,,10
	.p2align 3
.L47:
	movzbl	(%rdx,%r12), %esi
	addq	$1, %rdx
	movb	%sil, -1(%rdx)
	cmpq	%r13, %rdx
	jne	.L47
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L59:
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	leaq	512(%rax), %rcx
	jmp	.L42
	.cfi_endproc
.LFE18580:
	.size	_ZN2v88internal11interpreter19BytecodeArrayWriterC2EPNS0_4ZoneEPNS1_20ConstantArrayBuilderENS0_26SourcePositionTableBuilder13RecordingModeE, .-_ZN2v88internal11interpreter19BytecodeArrayWriterC2EPNS0_4ZoneEPNS1_20ConstantArrayBuilderENS0_26SourcePositionTableBuilder13RecordingModeE
	.globl	_ZN2v88internal11interpreter19BytecodeArrayWriterC1EPNS0_4ZoneEPNS1_20ConstantArrayBuilderENS0_26SourcePositionTableBuilder13RecordingModeE
	.set	_ZN2v88internal11interpreter19BytecodeArrayWriterC1EPNS0_4ZoneEPNS1_20ConstantArrayBuilderENS0_26SourcePositionTableBuilder13RecordingModeE,_ZN2v88internal11interpreter19BytecodeArrayWriterC2EPNS0_4ZoneEPNS1_20ConstantArrayBuilderENS0_26SourcePositionTableBuilder13RecordingModeE
	.section	.text._ZN2v88internal11interpreter19BytecodeArrayWriter15ToBytecodeArrayEPNS0_7IsolateEiiNS0_6HandleINS0_9ByteArrayEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19BytecodeArrayWriter15ToBytecodeArrayEPNS0_7IsolateEiiNS0_6HandleINS0_9ByteArrayEEE
	.type	_ZN2v88internal11interpreter19BytecodeArrayWriter15ToBytecodeArrayEPNS0_7IsolateEiiNS0_6HandleINS0_9ByteArrayEEE, @function
_ZN2v88internal11interpreter19BytecodeArrayWriter15ToBytecodeArrayEPNS0_7IsolateEiiNS0_6HandleINS0_9ByteArrayEEE:
.LFB18582:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	32(%rdi), %r15
	subq	24(%rdi), %r15
	movl	%edx, -52(%rbp)
	movq	112(%rdi), %rdi
	call	_ZN2v88internal11interpreter20ConstantArrayBuilder12ToFixedArrayEPNS0_7IsolateE@PLT
	movl	-52(%rbp), %ecx
	movq	24(%rbx), %r10
	movl	%r12d, %r8d
	movl	%r15d, %esi
	movq	%r13, %rdi
	movq	%rax, %r9
	sall	$3, %ecx
	movq	%r10, %rdx
	call	_ZN2v88internal7Factory16NewBytecodeArrayEiPKhiiNS0_6HandleINS0_10FixedArrayEEE@PLT
	movq	(%r14), %r13
	movq	(%rax), %r15
	movq	%rax, %r12
	movq	%r13, 23(%r15)
	testb	$1, %r13b
	je	.L64
	movq	%r13, %rbx
	leaq	23(%r15), %r14
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L72
	testb	$24, %al
	je	.L64
.L74:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L73
.L64:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L74
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L73:
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L64
	.cfi_endproc
.LFE18582:
	.size	_ZN2v88internal11interpreter19BytecodeArrayWriter15ToBytecodeArrayEPNS0_7IsolateEiiNS0_6HandleINS0_9ByteArrayEEE, .-_ZN2v88internal11interpreter19BytecodeArrayWriter15ToBytecodeArrayEPNS0_7IsolateEiiNS0_6HandleINS0_9ByteArrayEEE
	.section	.text._ZN2v88internal11interpreter19BytecodeArrayWriter21ToSourcePositionTableEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19BytecodeArrayWriter21ToSourcePositionTableEPNS0_7IsolateE
	.type	_ZN2v88internal11interpreter19BytecodeArrayWriter21ToSourcePositionTableEPNS0_7IsolateE, @function
_ZN2v88internal11interpreter19BytecodeArrayWriter21ToSourcePositionTableEPNS0_7IsolateE:
.LFB18583:
	.cfi_startproc
	endbr64
	cmpl	$2, 56(%rdi)
	je	.L76
	leaq	976(%rsi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$56, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal26SourcePositionTableBuilder21ToSourcePositionTableEPNS0_7IsolateE@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18583:
	.size	_ZN2v88internal11interpreter19BytecodeArrayWriter21ToSourcePositionTableEPNS0_7IsolateE, .-_ZN2v88internal11interpreter19BytecodeArrayWriter21ToSourcePositionTableEPNS0_7IsolateE
	.section	.text._ZN2v88internal11interpreter19BytecodeArrayWriter14BindLoopHeaderEPNS1_18BytecodeLoopHeaderE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19BytecodeArrayWriter14BindLoopHeaderEPNS1_18BytecodeLoopHeaderE
	.type	_ZN2v88internal11interpreter19BytecodeArrayWriter14BindLoopHeaderEPNS1_18BytecodeLoopHeaderE, @function
_ZN2v88internal11interpreter19BytecodeArrayWriter14BindLoopHeaderEPNS1_18BytecodeLoopHeaderE:
.LFB18589:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	subq	24(%rdi), %rax
	movq	%rax, (%rsi)
	movb	$-74, 120(%rdi)
	movb	$0, 138(%rdi)
	ret
	.cfi_endproc
.LFE18589:
	.size	_ZN2v88internal11interpreter19BytecodeArrayWriter14BindLoopHeaderEPNS1_18BytecodeLoopHeaderE, .-_ZN2v88internal11interpreter19BytecodeArrayWriter14BindLoopHeaderEPNS1_18BytecodeLoopHeaderE
	.section	.text._ZN2v88internal11interpreter19BytecodeArrayWriter18BindJumpTableEntryEPNS1_17BytecodeJumpTableEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19BytecodeArrayWriter18BindJumpTableEntryEPNS1_17BytecodeJumpTableEi
	.type	_ZN2v88internal11interpreter19BytecodeArrayWriter18BindJumpTableEntryEPNS1_17BytecodeJumpTableEi, @function
_ZN2v88internal11interpreter19BytecodeArrayWriter18BindJumpTableEntryEPNS1_17BytecodeJumpTableEi:
.LFB18590:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	32(%rbx), %r8
	subq	24(%rbx), %r8
	subq	8(%rsi), %r8
	movslq	%edx, %rsi
	addq	(%rax), %rsi
	movslq	20(%rax), %rax
	salq	$32, %r8
	movq	112(%rdi), %rdi
	movq	%r8, %rdx
	subq	%rax, %rsi
	call	_ZN2v88internal11interpreter20ConstantArrayBuilder15SetJumpTableSmiEmNS0_3SmiE@PLT
	movb	$-74, 120(%rbx)
	movb	$0, 138(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18590:
	.size	_ZN2v88internal11interpreter19BytecodeArrayWriter18BindJumpTableEntryEPNS1_17BytecodeJumpTableEi, .-_ZN2v88internal11interpreter19BytecodeArrayWriter18BindJumpTableEntryEPNS1_17BytecodeJumpTableEi
	.section	.text._ZN2v88internal11interpreter19BytecodeArrayWriter17BindHandlerTargetEPNS1_19HandlerTableBuilderEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19BytecodeArrayWriter17BindHandlerTargetEPNS1_19HandlerTableBuilderEi
	.type	_ZN2v88internal11interpreter19BytecodeArrayWriter17BindHandlerTargetEPNS1_19HandlerTableBuilderEi, @function
_ZN2v88internal11interpreter19BytecodeArrayWriter17BindHandlerTargetEPNS1_19HandlerTableBuilderEi:
.LFB18591:
	.cfi_startproc
	endbr64
	movb	$-74, 120(%rdi)
	movq	%rsi, %r8
	movl	%edx, %esi
	movq	32(%rdi), %rdx
	movb	$0, 138(%rdi)
	subq	24(%rdi), %rdx
	movq	%r8, %rdi
	jmp	_ZN2v88internal11interpreter19HandlerTableBuilder16SetHandlerTargetEim@PLT
	.cfi_endproc
.LFE18591:
	.size	_ZN2v88internal11interpreter19BytecodeArrayWriter17BindHandlerTargetEPNS1_19HandlerTableBuilderEi, .-_ZN2v88internal11interpreter19BytecodeArrayWriter17BindHandlerTargetEPNS1_19HandlerTableBuilderEi
	.section	.text._ZN2v88internal11interpreter19BytecodeArrayWriter18BindTryRegionStartEPNS1_19HandlerTableBuilderEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19BytecodeArrayWriter18BindTryRegionStartEPNS1_19HandlerTableBuilderEi
	.type	_ZN2v88internal11interpreter19BytecodeArrayWriter18BindTryRegionStartEPNS1_19HandlerTableBuilderEi, @function
_ZN2v88internal11interpreter19BytecodeArrayWriter18BindTryRegionStartEPNS1_19HandlerTableBuilderEi:
.LFB18592:
	.cfi_startproc
	endbr64
	movb	$-74, 120(%rdi)
	movq	%rsi, %r8
	movl	%edx, %esi
	movq	32(%rdi), %rdx
	subq	24(%rdi), %rdx
	movq	%r8, %rdi
	jmp	_ZN2v88internal11interpreter19HandlerTableBuilder17SetTryRegionStartEim@PLT
	.cfi_endproc
.LFE18592:
	.size	_ZN2v88internal11interpreter19BytecodeArrayWriter18BindTryRegionStartEPNS1_19HandlerTableBuilderEi, .-_ZN2v88internal11interpreter19BytecodeArrayWriter18BindTryRegionStartEPNS1_19HandlerTableBuilderEi
	.section	.text._ZN2v88internal11interpreter19BytecodeArrayWriter16BindTryRegionEndEPNS1_19HandlerTableBuilderEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19BytecodeArrayWriter16BindTryRegionEndEPNS1_19HandlerTableBuilderEi
	.type	_ZN2v88internal11interpreter19BytecodeArrayWriter16BindTryRegionEndEPNS1_19HandlerTableBuilderEi, @function
_ZN2v88internal11interpreter19BytecodeArrayWriter16BindTryRegionEndEPNS1_19HandlerTableBuilderEi:
.LFB18593:
	.cfi_startproc
	endbr64
	movb	$-74, 120(%rdi)
	movq	%rsi, %r8
	movl	%edx, %esi
	movq	32(%rdi), %rdx
	subq	24(%rdi), %rdx
	movq	%r8, %rdi
	jmp	_ZN2v88internal11interpreter19HandlerTableBuilder15SetTryRegionEndEim@PLT
	.cfi_endproc
.LFE18593:
	.size	_ZN2v88internal11interpreter19BytecodeArrayWriter16BindTryRegionEndEPNS1_19HandlerTableBuilderEi, .-_ZN2v88internal11interpreter19BytecodeArrayWriter16BindTryRegionEndEPNS1_19HandlerTableBuilderEi
	.section	.text._ZN2v88internal11interpreter19BytecodeArrayWriter15StartBasicBlockEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19BytecodeArrayWriter15StartBasicBlockEv
	.type	_ZN2v88internal11interpreter19BytecodeArrayWriter15StartBasicBlockEv, @function
_ZN2v88internal11interpreter19BytecodeArrayWriter15StartBasicBlockEv:
.LFB18594:
	.cfi_startproc
	endbr64
	movb	$-74, 120(%rdi)
	movb	$0, 138(%rdi)
	ret
	.cfi_endproc
.LFE18594:
	.size	_ZN2v88internal11interpreter19BytecodeArrayWriter15StartBasicBlockEv, .-_ZN2v88internal11interpreter19BytecodeArrayWriter15StartBasicBlockEv
	.section	.text._ZN2v88internal11interpreter19BytecodeArrayWriter25UpdateSourcePositionTableEPKNS1_12BytecodeNodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19BytecodeArrayWriter25UpdateSourcePositionTableEPKNS1_12BytecodeNodeE
	.type	_ZN2v88internal11interpreter19BytecodeArrayWriter25UpdateSourcePositionTableEPKNS1_12BytecodeNodeE, @function
_ZN2v88internal11interpreter19BytecodeArrayWriter25UpdateSourcePositionTableEPKNS1_12BytecodeNodeE:
.LFB18595:
	.cfi_startproc
	endbr64
	movzbl	32(%rsi), %eax
	testb	%al, %al
	jne	.L90
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	movl	36(%rsi), %edx
	movq	32(%rdi), %r8
	xorl	%ecx, %ecx
	movabsq	$-140735340871681, %rsi
	addl	$1, %edx
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	andq	%rsi, %rdx
	cmpb	$2, %al
	movq	%r8, %rsi
	sete	%cl
	subq	24(%rdi), %rsi
	addq	$56, %rdi
	movslq	%esi, %rsi
	jmp	_ZN2v88internal26SourcePositionTableBuilder11AddPositionEmNS0_14SourcePositionEb@PLT
	.cfi_endproc
.LFE18595:
	.size	_ZN2v88internal11interpreter19BytecodeArrayWriter25UpdateSourcePositionTableEPKNS1_12BytecodeNodeE, .-_ZN2v88internal11interpreter19BytecodeArrayWriter25UpdateSourcePositionTableEPKNS1_12BytecodeNodeE
	.section	.text._ZN2v88internal11interpreter19BytecodeArrayWriter21UpdateExitSeenInBlockENS1_8BytecodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19BytecodeArrayWriter21UpdateExitSeenInBlockENS1_8BytecodeE
	.type	_ZN2v88internal11interpreter19BytecodeArrayWriter21UpdateExitSeenInBlockENS1_8BytecodeE, @function
_ZN2v88internal11interpreter19BytecodeArrayWriter21UpdateExitSeenInBlockENS1_8BytecodeE:
.LFB18596:
	.cfi_startproc
	endbr64
	addl	$117, %esi
	cmpb	$42, %sil
	ja	.L91
	movabsq	$4543001657347, %rax
	btq	%rsi, %rax
	jc	.L96
.L91:
	ret
.L96:
	movb	$1, 138(%rdi)
	ret
	.cfi_endproc
.LFE18596:
	.size	_ZN2v88internal11interpreter19BytecodeArrayWriter21UpdateExitSeenInBlockENS1_8BytecodeE, .-_ZN2v88internal11interpreter19BytecodeArrayWriter21UpdateExitSeenInBlockENS1_8BytecodeE
	.section	.text._ZN2v88internal11interpreter19BytecodeArrayWriter22MaybeElideLastBytecodeENS1_8BytecodeEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19BytecodeArrayWriter22MaybeElideLastBytecodeENS1_8BytecodeEb
	.type	_ZN2v88internal11interpreter19BytecodeArrayWriter22MaybeElideLastBytecodeENS1_8BytecodeEb, @function
_ZN2v88internal11interpreter19BytecodeArrayWriter22MaybeElideLastBytecodeENS1_8BytecodeEb:
.LFB18597:
	.cfi_startproc
	endbr64
	cmpb	$0, 137(%rdi)
	je	.L132
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movzbl	120(%rdi), %ecx
	cmpb	$37, %cl
	ja	.L135
	movabsq	$137691133952, %r8
	movzbl	%sil, %edx
	movq	24(%rdi), %rsi
	movq	32(%rdi), %rdi
	movq	%rdi, %r14
	subq	%rsi, %r14
	btq	%rcx, %r8
	jnc	.L100
	leaq	_ZN2v88internal11interpreter9Bytecodes15kAccumulatorUseE(%rip), %rcx
	cmpb	$2, (%rcx,%rdx)
	jne	.L100
	movzbl	136(%rbx), %eax
	movl	%eax, %edx
	andb	%r13b, %dl
	jne	.L119
	movq	128(%rbx), %r15
	cmpq	%r14, %r15
	ja	.L136
	jnb	.L102
	addq	%r15, %rsi
	cmpq	%rdi, %rsi
	je	.L102
	movq	%rsi, 32(%rbx)
	movq	%r15, %r14
.L102:
	orl	%r13d, %eax
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L135:
	movq	32(%rdi), %r14
	subq	24(%rdi), %r14
.L100:
	movb	%r12b, 120(%rbx)
	movb	%al, 136(%rbx)
	movq	%r14, 128(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L132:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L136:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	%r15, %rdx
	subq	%r14, %rdx
	je	.L102
	movl	$2147483647, %eax
	subq	%r14, %rax
	movq	%rax, %rcx
	movq	40(%rbx), %rax
	subq	%rdi, %rax
	cmpq	%rax, %rdx
	ja	.L103
	xorl	%esi, %esi
	movq	%rdx, -56(%rbp)
	call	memset@PLT
	movq	-56(%rbp), %rdx
	leaq	(%rax,%rdx), %r14
	movzbl	136(%rbx), %eax
	movq	%r14, 32(%rbx)
	subq	24(%rbx), %r14
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L119:
	movl	%edx, %eax
	jmp	.L100
.L103:
	cmpq	%rcx, %rdx
	ja	.L137
	cmpq	%r14, %rdx
	movq	%r14, %rcx
	cmovnb	%rdx, %rcx
	addq	%r14, %rcx
	jc	.L106
	cmpq	$2147483647, %rcx
	movl	$2147483647, %eax
	cmova	%rax, %rcx
	leaq	7(%rcx), %rsi
	andq	$-8, %rsi
.L118:
	movq	16(%rbx), %rdi
	movq	16(%rdi), %r8
	movq	24(%rdi), %rax
	subq	%r8, %rax
	cmpq	%rsi, %rax
	jb	.L138
	addq	%r8, %rsi
	movq	%rsi, 16(%rdi)
.L109:
	xorl	%esi, %esi
	leaq	(%r8,%r14), %rdi
	movq	%rcx, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	memset@PLT
	movq	32(%rbx), %rdx
	movq	24(%rbx), %rsi
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %rcx
	cmpq	%rsi, %rdx
	je	.L114
	leaq	-1(%rdx), %rax
	subq	%rsi, %rax
	cmpq	$14, %rax
	jbe	.L111
	leaq	15(%rsi), %rax
	subq	%r8, %rax
	cmpq	$30, %rax
	jbe	.L111
	movq	%rdx, %r9
	movq	%rsi, %r10
	movq	%r8, %rax
	subq	%rsi, %r9
	subq	%r8, %r10
	movq	%r9, %rdi
	andq	$-16, %rdi
	addq	%r8, %rdi
	.p2align 4,,10
	.p2align 3
.L112:
	movdqu	(%rax,%r10), %xmm0
	addq	$16, %rax
	movups	%xmm0, -16(%rax)
	cmpq	%rdi, %rax
	jne	.L112
	movq	%r9, %rdi
	andq	$-16, %rdi
	leaq	(%rsi,%rdi), %rax
	leaq	(%r8,%rdi), %rsi
	cmpq	%r9, %rdi
	je	.L114
	movzbl	(%rax), %edi
	movb	%dil, (%rsi)
	leaq	1(%rax), %rdi
	cmpq	%rdi, %rdx
	je	.L114
	movzbl	1(%rax), %edi
	movb	%dil, 1(%rsi)
	leaq	2(%rax), %rdi
	cmpq	%rdi, %rdx
	je	.L114
	movzbl	2(%rax), %edi
	movb	%dil, 2(%rsi)
	leaq	3(%rax), %rdi
	cmpq	%rdi, %rdx
	je	.L114
	movzbl	3(%rax), %edi
	movb	%dil, 3(%rsi)
	leaq	4(%rax), %rdi
	cmpq	%rdi, %rdx
	je	.L114
	movzbl	4(%rax), %edi
	movb	%dil, 4(%rsi)
	leaq	5(%rax), %rdi
	cmpq	%rdi, %rdx
	je	.L114
	movzbl	5(%rax), %edi
	movb	%dil, 5(%rsi)
	leaq	6(%rax), %rdi
	cmpq	%rdi, %rdx
	je	.L114
	movzbl	6(%rax), %edi
	movb	%dil, 6(%rsi)
	leaq	7(%rax), %rdi
	cmpq	%rdi, %rdx
	je	.L114
	movzbl	7(%rax), %edi
	movb	%dil, 7(%rsi)
	leaq	8(%rax), %rdi
	cmpq	%rdi, %rdx
	je	.L114
	movzbl	8(%rax), %edi
	movb	%dil, 8(%rsi)
	leaq	9(%rax), %rdi
	cmpq	%rdi, %rdx
	je	.L114
	movzbl	9(%rax), %edi
	movb	%dil, 9(%rsi)
	leaq	10(%rax), %rdi
	cmpq	%rdi, %rdx
	je	.L114
	movzbl	10(%rax), %edi
	movb	%dil, 10(%rsi)
	leaq	11(%rax), %rdi
	cmpq	%rdi, %rdx
	je	.L114
	movzbl	11(%rax), %edi
	movb	%dil, 11(%rsi)
	leaq	12(%rax), %rdi
	cmpq	%rdi, %rdx
	je	.L114
	movzbl	12(%rax), %edi
	movb	%dil, 12(%rsi)
	leaq	13(%rax), %rdi
	cmpq	%rdi, %rdx
	je	.L114
	movzbl	13(%rax), %edi
	movb	%dil, 13(%rsi)
	leaq	14(%rax), %rdi
	cmpq	%rdi, %rdx
	je	.L114
	movzbl	14(%rax), %eax
	movb	%al, 14(%rsi)
.L114:
	leaq	(%r8,%r15), %rax
	addq	%r8, %rcx
	movq	%r8, 24(%rbx)
	movq	%r15, %r14
	movq	%rax, 32(%rbx)
	movzbl	136(%rbx), %eax
	movq	%rcx, 40(%rbx)
	jmp	.L102
.L111:
	movq	%rdx, %rax
	movq	%r8, %rdx
	subq	%rsi, %rax
	subq	%r8, %rsi
	addq	%r8, %rax
.L116:
	movzbl	(%rdx,%rsi), %edi
	addq	$1, %rdx
	movb	%dil, -1(%rdx)
	cmpq	%rax, %rdx
	jne	.L116
	jmp	.L114
.L138:
	movq	%rcx, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rcx
	movq	%rax, %r8
	jmp	.L109
.L106:
	movl	$2147483648, %esi
	movl	$2147483647, %ecx
	jmp	.L118
.L137:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE18597:
	.size	_ZN2v88internal11interpreter19BytecodeArrayWriter22MaybeElideLastBytecodeENS1_8BytecodeEb, .-_ZN2v88internal11interpreter19BytecodeArrayWriter22MaybeElideLastBytecodeENS1_8BytecodeEb
	.section	.text._ZN2v88internal11interpreter19BytecodeArrayWriter22InvalidateLastBytecodeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19BytecodeArrayWriter22InvalidateLastBytecodeEv
	.type	_ZN2v88internal11interpreter19BytecodeArrayWriter22InvalidateLastBytecodeEv, @function
_ZN2v88internal11interpreter19BytecodeArrayWriter22InvalidateLastBytecodeEv:
.LFB18598:
	.cfi_startproc
	endbr64
	movb	$-74, 120(%rdi)
	ret
	.cfi_endproc
.LFE18598:
	.size	_ZN2v88internal11interpreter19BytecodeArrayWriter22InvalidateLastBytecodeEv, .-_ZN2v88internal11interpreter19BytecodeArrayWriter22InvalidateLastBytecodeEv
	.section	.rodata._ZN2v88internal11interpreter26GetJumpWithConstantOperandENS1_8BytecodeE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"unreachable code"
	.section	.text._ZN2v88internal11interpreter26GetJumpWithConstantOperandENS1_8BytecodeE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal11interpreter26GetJumpWithConstantOperandENS1_8BytecodeE
	.type	_ZN2v88internal11interpreter26GetJumpWithConstantOperandENS1_8BytecodeE, @function
_ZN2v88internal11interpreter26GetJumpWithConstantOperandENS1_8BytecodeE:
.LFB18600:
	.cfi_startproc
	endbr64
	addl	$117, %edi
	cmpb	$21, %dil
	ja	.L141
	leaq	.L143(%rip), %rdx
	movzbl	%dil, %edi
	movslq	(%rdx,%rdi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal11interpreter26GetJumpWithConstantOperandENS1_8BytecodeE,"a",@progbits
	.align 4
	.align 4
.L143:
	.long	.L153-.L143
	.long	.L141-.L143
	.long	.L141-.L143
	.long	.L141-.L143
	.long	.L141-.L143
	.long	.L141-.L143
	.long	.L141-.L143
	.long	.L141-.L143
	.long	.L141-.L143
	.long	.L141-.L143
	.long	.L141-.L143
	.long	.L141-.L143
	.long	.L152-.L143
	.long	.L151-.L143
	.long	.L150-.L143
	.long	.L154-.L143
	.long	.L148-.L143
	.long	.L147-.L143
	.long	.L146-.L143
	.long	.L145-.L143
	.long	.L144-.L143
	.long	.L142-.L143
	.section	.text._ZN2v88internal11interpreter26GetJumpWithConstantOperandENS1_8BytecodeE
	.p2align 4,,10
	.p2align 3
.L153:
	movl	$-116, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L152:
	movl	$-107, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L151:
	movl	$-106, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L148:
	movl	$-115, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L147:
	movl	$-114, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L146:
	movl	$-113, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L145:
	movl	$-112, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L144:
	movl	$-111, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L142:
	movl	$-108, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L154:
	movl	$-109, %eax
	ret
.L141:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L150:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movl	$-110, %eax
	ret
	.cfi_endproc
.LFE18600:
	.size	_ZN2v88internal11interpreter26GetJumpWithConstantOperandENS1_8BytecodeE, .-_ZN2v88internal11interpreter26GetJumpWithConstantOperandENS1_8BytecodeE
	.section	.rodata._ZN2v88internal11interpreter19BytecodeArrayWriter24PatchJumpWith8BitOperandEmi.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"vector::_M_range_check: __n (which is %zu) >= this->size() (which is %zu)"
	.section	.text._ZN2v88internal11interpreter19BytecodeArrayWriter24PatchJumpWith8BitOperandEmi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19BytecodeArrayWriter24PatchJumpWith8BitOperandEmi
	.type	_ZN2v88internal11interpreter19BytecodeArrayWriter24PatchJumpWith8BitOperandEmi, @function
_ZN2v88internal11interpreter19BytecodeArrayWriter24PatchJumpWith8BitOperandEmi:
.LFB18601:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	24(%rdi), %rax
	movq	32(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	jnb	.L179
	movq	%rdi, %rbx
	leaq	1(%rsi), %r15
	movq	112(%rdi), %rdi
	cmpl	$255, %r13d
	jbe	.L181
	movzbl	(%rax,%rsi), %r14d
	movq	%r13, %rdx
	movl	$1, %esi
	salq	$32, %rdx
	call	_ZN2v88internal11interpreter20ConstantArrayBuilder19CommitReservedEntryENS1_11OperandSizeENS0_3SmiE@PLT
	addl	$117, %r14d
	cmpb	$21, %r14b
	ja	.L162
	leaq	.L164(%rip), %rcx
	movzbl	%r14b, %r14d
	movslq	(%rcx,%r14,4), %rdx
	addq	%rcx, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal11interpreter19BytecodeArrayWriter24PatchJumpWith8BitOperandEmi,"a",@progbits
	.align 4
	.align 4
.L164:
	.long	.L174-.L164
	.long	.L162-.L164
	.long	.L162-.L164
	.long	.L162-.L164
	.long	.L162-.L164
	.long	.L162-.L164
	.long	.L162-.L164
	.long	.L162-.L164
	.long	.L162-.L164
	.long	.L162-.L164
	.long	.L162-.L164
	.long	.L162-.L164
	.long	.L173-.L164
	.long	.L172-.L164
	.long	.L171-.L164
	.long	.L177-.L164
	.long	.L169-.L164
	.long	.L168-.L164
	.long	.L167-.L164
	.long	.L166-.L164
	.long	.L165-.L164
	.long	.L163-.L164
	.section	.text._ZN2v88internal11interpreter19BytecodeArrayWriter24PatchJumpWith8BitOperandEmi
	.p2align 4,,10
	.p2align 3
.L181:
	movl	$1, %esi
	call	_ZN2v88internal11interpreter20ConstantArrayBuilder20DiscardReservedEntryENS1_11OperandSizeE@PLT
	movq	24(%rbx), %rax
	movq	32(%rbx), %rdx
	subq	%rax, %rdx
	cmpq	%r15, %rdx
	jbe	.L180
	movb	%r13b, 1(%rax,%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L163:
	.cfi_restore_state
	movl	$-108, %esi
	.p2align 4,,10
	.p2align 3
.L170:
	movq	24(%rbx), %rcx
	movq	32(%rbx), %rdx
	subq	%rcx, %rdx
	cmpq	%rdx, %r12
	jnb	.L179
	movb	%sil, (%rcx,%r12)
	movq	24(%rbx), %rcx
	movq	32(%rbx), %rdx
	subq	%rcx, %rdx
	cmpq	%rdx, %r15
	jnb	.L180
	movb	%al, 1(%rcx,%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L165:
	.cfi_restore_state
	movl	$-111, %esi
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L166:
	movl	$-112, %esi
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L167:
	movl	$-113, %esi
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L168:
	movl	$-114, %esi
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L169:
	movl	$-115, %esi
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L172:
	movl	$-106, %esi
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L173:
	movl	$-107, %esi
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L174:
	movl	$-116, %esi
	jmp	.L170
.L162:
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L177:
	movl	$-109, %esi
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L171:
	movl	$-110, %esi
	jmp	.L170
.L179:
	movq	%r12, %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L180:
	movq	%r15, %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE18601:
	.size	_ZN2v88internal11interpreter19BytecodeArrayWriter24PatchJumpWith8BitOperandEmi, .-_ZN2v88internal11interpreter19BytecodeArrayWriter24PatchJumpWith8BitOperandEmi
	.section	.text._ZN2v88internal11interpreter19BytecodeArrayWriter25PatchJumpWith16BitOperandEmi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19BytecodeArrayWriter25PatchJumpWith16BitOperandEmi
	.type	_ZN2v88internal11interpreter19BytecodeArrayWriter25PatchJumpWith16BitOperandEmi, @function
_ZN2v88internal11interpreter19BytecodeArrayWriter25PatchJumpWith16BitOperandEmi:
.LFB18602:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$24, %rsp
	movq	32(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	jnb	.L198
	movq	%rdi, %r15
	leaq	1(%rsi), %r12
	movq	112(%rdi), %rdi
	cmpl	$65535, %ebx
	ja	.L207
	movl	$2, %esi
	call	_ZN2v88internal11interpreter20ConstantArrayBuilder20DiscardReservedEntryENS1_11OperandSizeE@PLT
.L200:
	movq	24(%r15), %rax
	movq	32(%r15), %rdx
	leaq	2(%r14), %rsi
	subq	%rax, %rdx
	cmpq	%rdx, %r12
	jnb	.L208
	movb	%bl, 1(%rax,%r14)
	movq	24(%r15), %rax
	movq	32(%r15), %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	jnb	.L209
	movzbl	%bh, %ebx
	movb	%bl, 2(%rax,%r14)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L210
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L207:
	.cfi_restore_state
	movzbl	(%rax,%rsi), %r13d
	movq	%rbx, %rdx
	movl	$2, %esi
	salq	$32, %rdx
	call	_ZN2v88internal11interpreter20ConstantArrayBuilder19CommitReservedEntryENS1_11OperandSizeENS0_3SmiE@PLT
	addl	$117, %r13d
	movq	%rax, %rbx
	cmpb	$21, %r13b
	ja	.L185
	leaq	.L187(%rip), %rdx
	movzbl	%r13b, %r13d
	movslq	(%rdx,%r13,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal11interpreter19BytecodeArrayWriter25PatchJumpWith16BitOperandEmi,"a",@progbits
	.align 4
	.align 4
.L187:
	.long	.L197-.L187
	.long	.L185-.L187
	.long	.L185-.L187
	.long	.L185-.L187
	.long	.L185-.L187
	.long	.L185-.L187
	.long	.L185-.L187
	.long	.L185-.L187
	.long	.L185-.L187
	.long	.L185-.L187
	.long	.L185-.L187
	.long	.L185-.L187
	.long	.L196-.L187
	.long	.L195-.L187
	.long	.L194-.L187
	.long	.L204-.L187
	.long	.L192-.L187
	.long	.L191-.L187
	.long	.L190-.L187
	.long	.L189-.L187
	.long	.L188-.L187
	.long	.L186-.L187
	.section	.text._ZN2v88internal11interpreter19BytecodeArrayWriter25PatchJumpWith16BitOperandEmi
.L185:
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L197:
	movl	$-116, %ecx
	.p2align 4,,10
	.p2align 3
.L193:
	movq	24(%r15), %rax
	movq	32(%r15), %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %r14
	jnb	.L198
	movb	%cl, (%rax,%r14)
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L196:
	movl	$-107, %ecx
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L195:
	movl	$-106, %ecx
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L192:
	movl	$-115, %ecx
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L191:
	movl	$-114, %ecx
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L190:
	movl	$-113, %ecx
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L189:
	movl	$-112, %ecx
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L188:
	movl	$-111, %ecx
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L186:
	movl	$-108, %ecx
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L204:
	movl	$-109, %ecx
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L194:
	movl	$-110, %ecx
	jmp	.L193
.L198:
	movq	%r14, %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L210:
	call	__stack_chk_fail@PLT
.L209:
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L208:
	movq	%r12, %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE18602:
	.size	_ZN2v88internal11interpreter19BytecodeArrayWriter25PatchJumpWith16BitOperandEmi, .-_ZN2v88internal11interpreter19BytecodeArrayWriter25PatchJumpWith16BitOperandEmi
	.section	.text._ZN2v88internal11interpreter19BytecodeArrayWriter25PatchJumpWith32BitOperandEmi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19BytecodeArrayWriter25PatchJumpWith32BitOperandEmi
	.type	_ZN2v88internal11interpreter19BytecodeArrayWriter25PatchJumpWith32BitOperandEmi, @function
_ZN2v88internal11interpreter19BytecodeArrayWriter25PatchJumpWith32BitOperandEmi:
.LFB18603:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	movl	$4, %esi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	112(%rdi), %rdi
	call	_ZN2v88internal11interpreter20ConstantArrayBuilder20DiscardReservedEntryENS1_11OperandSizeE@PLT
	movq	24(%rbx), %rax
	movq	32(%rbx), %rdx
	leaq	1(%r12), %rsi
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	jnb	.L217
	movb	%r13b, 1(%rax,%r12)
	movq	24(%rbx), %rax
	leaq	2(%r12), %r8
	leaq	3(%r12), %rsi
	movq	32(%rbx), %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %r8
	jnb	.L218
	movl	%r13d, %ecx
	leaq	4(%r12), %r8
	movzbl	%ch, %ecx
	movb	%cl, 2(%rax,%r12)
	movq	24(%rbx), %rax
	movq	32(%rbx), %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	jnb	.L217
	movl	%r13d, %edx
	shrl	$16, %edx
	movb	%dl, 3(%rax,%r12)
	movq	24(%rbx), %rax
	movq	32(%rbx), %rdx
	subq	%rax, %rdx
	cmpq	%r8, %rdx
	jbe	.L218
	shrl	$24, %r13d
	movb	%r13b, 4(%rax,%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L218:
	.cfi_restore_state
	movq	%r8, %rsi
.L217:
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE18603:
	.size	_ZN2v88internal11interpreter19BytecodeArrayWriter25PatchJumpWith32BitOperandEmi, .-_ZN2v88internal11interpreter19BytecodeArrayWriter25PatchJumpWith32BitOperandEmi
	.section	.text._ZN2v88internal11interpreter19BytecodeArrayWriter9BindLabelEPNS1_13BytecodeLabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19BytecodeArrayWriter9BindLabelEPNS1_13BytecodeLabelE
	.type	_ZN2v88internal11interpreter19BytecodeArrayWriter9BindLabelEPNS1_13BytecodeLabelE, @function
_ZN2v88internal11interpreter19BytecodeArrayWriter9BindLabelEPNS1_13BytecodeLabelE:
.LFB18588:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	24(%rdi), %rax
	movq	32(%rdi), %r8
	movq	8(%rsi), %rsi
	subq	%rax, %r8
	cmpq	%r8, %rsi
	jnb	.L224
	movzbl	(%rax,%rsi), %eax
	movl	%r8d, %edx
	movq	%rdi, %rbx
	subl	%esi, %edx
	cmpb	$3, %al
	ja	.L221
	subl	$1, %edx
	cmpb	$2, %al
	je	.L222
	cmpb	$3, %al
	je	.L223
	testb	%al, %al
	je	.L222
	cmpb	$1, %al
	jne	.L239
.L223:
	addq	$1, %rsi
	cmpq	%rsi, %r8
	jbe	.L224
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter25PatchJumpWith32BitOperandEmi
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L222:
	addq	$1, %rsi
	cmpq	%rsi, %r8
	jbe	.L224
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter25PatchJumpWith16BitOperandEmi
.L225:
	subl	$1, 48(%rbx)
	movb	$1, (%r12)
	movb	$-74, 120(%rbx)
	movb	$0, 138(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L221:
	.cfi_restore_state
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter24PatchJumpWith8BitOperandEmi
	jmp	.L225
.L224:
	movq	%r8, %rdx
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L239:
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18588:
	.size	_ZN2v88internal11interpreter19BytecodeArrayWriter9BindLabelEPNS1_13BytecodeLabelE, .-_ZN2v88internal11interpreter19BytecodeArrayWriter9BindLabelEPNS1_13BytecodeLabelE
	.section	.text._ZN2v88internal11interpreter19BytecodeArrayWriter9PatchJumpEmm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19BytecodeArrayWriter9PatchJumpEmm
	.type	_ZN2v88internal11interpreter19BytecodeArrayWriter9PatchJumpEmm, @function
_ZN2v88internal11interpreter19BytecodeArrayWriter9PatchJumpEmm:
.LFB18604:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	movq	%rdx, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	24(%rdi), %rdx
	movq	32(%rdi), %r8
	subq	%rdx, %r8
	cmpq	%r8, %rsi
	jnb	.L245
	movzbl	(%rdx,%rsi), %ecx
	movq	%rdi, %rbx
	subl	%esi, %eax
	cmpb	$3, %cl
	ja	.L242
	leal	-1(%rax), %edx
	cmpb	$2, %cl
	je	.L243
	cmpb	$3, %cl
	je	.L244
	testb	%cl, %cl
	je	.L243
	cmpb	$1, %cl
	jne	.L260
.L244:
	addq	$1, %rsi
	cmpq	%rsi, %r8
	jbe	.L245
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter25PatchJumpWith32BitOperandEmi
	subl	$1, 48(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L243:
	.cfi_restore_state
	addq	$1, %rsi
	cmpq	%rsi, %r8
	jbe	.L245
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter25PatchJumpWith16BitOperandEmi
	subl	$1, 48(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L242:
	.cfi_restore_state
	movl	%eax, %edx
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter24PatchJumpWith8BitOperandEmi
	subl	$1, 48(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L245:
	.cfi_restore_state
	movq	%r8, %rdx
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L260:
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18604:
	.size	_ZN2v88internal11interpreter19BytecodeArrayWriter9PatchJumpEmm, .-_ZN2v88internal11interpreter19BytecodeArrayWriter9PatchJumpEmm
	.section	.rodata._ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE12emplace_backIJhEEEvDpOT_.str1.1,"aMS",@progbits,1
.LC3:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE12emplace_backIJhEEEvDpOT_,"axG",@progbits,_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE12emplace_backIJhEEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE12emplace_backIJhEEEvDpOT_
	.type	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE12emplace_backIJhEEEvDpOT_, @function
_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE12emplace_backIJhEEEvDpOT_:
.LFB20944:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	16(%rdi), %r12
	cmpq	24(%rdi), %r12
	je	.L262
	movzbl	(%rsi), %eax
	movb	%al, (%r12)
	addq	$1, 16(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L262:
	.cfi_restore_state
	movq	8(%rdi), %r14
	movq	%r12, %r15
	subq	%r14, %r15
	cmpq	$2147483647, %r15
	je	.L285
	testq	%r15, %r15
	je	.L274
	leaq	(%r15,%r15), %rax
	movl	$2147483648, %esi
	movl	$2147483647, %ecx
	cmpq	%rax, %r15
	jbe	.L286
.L265:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L287
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L267:
	leaq	(%rax,%rcx), %rsi
	movzbl	0(%r13), %ecx
	leaq	1(%rax), %rdx
	movb	%cl, (%rax,%r15)
	cmpq	%r14, %r12
	je	.L268
	leaq	15(%rax), %rdx
	subq	%r14, %rdx
	cmpq	$30, %rdx
	jbe	.L269
	leaq	-1(%r12), %rdx
	subq	%r14, %rdx
	cmpq	$14, %rdx
	jbe	.L269
	movq	%r12, %r8
	xorl	%edx, %edx
	subq	%r14, %r8
	movq	%r8, %rcx
	andq	$-16, %rcx
	.p2align 4,,10
	.p2align 3
.L270:
	movdqu	(%r14,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L270
	movq	%r8, %rdi
	andq	$-16, %rdi
	leaq	(%r14,%rdi), %rdx
	leaq	(%rax,%rdi), %rcx
	cmpq	%rdi, %r8
	je	.L273
	movzbl	(%rdx), %edi
	movb	%dil, (%rcx)
	leaq	1(%rdx), %rdi
	cmpq	%rdi, %r12
	je	.L273
	movzbl	1(%rdx), %edi
	movb	%dil, 1(%rcx)
	leaq	2(%rdx), %rdi
	cmpq	%rdi, %r12
	je	.L273
	movzbl	2(%rdx), %edi
	movb	%dil, 2(%rcx)
	leaq	3(%rdx), %rdi
	cmpq	%rdi, %r12
	je	.L273
	movzbl	3(%rdx), %edi
	movb	%dil, 3(%rcx)
	leaq	4(%rdx), %rdi
	cmpq	%rdi, %r12
	je	.L273
	movzbl	4(%rdx), %edi
	movb	%dil, 4(%rcx)
	leaq	5(%rdx), %rdi
	cmpq	%rdi, %r12
	je	.L273
	movzbl	5(%rdx), %edi
	movb	%dil, 5(%rcx)
	leaq	6(%rdx), %rdi
	cmpq	%rdi, %r12
	je	.L273
	movzbl	6(%rdx), %edi
	movb	%dil, 6(%rcx)
	leaq	7(%rdx), %rdi
	cmpq	%rdi, %r12
	je	.L273
	movzbl	7(%rdx), %edi
	movb	%dil, 7(%rcx)
	leaq	8(%rdx), %rdi
	cmpq	%rdi, %r12
	je	.L273
	movzbl	8(%rdx), %edi
	movb	%dil, 8(%rcx)
	leaq	9(%rdx), %rdi
	cmpq	%rdi, %r12
	je	.L273
	movzbl	9(%rdx), %edi
	movb	%dil, 9(%rcx)
	leaq	10(%rdx), %rdi
	cmpq	%rdi, %r12
	je	.L273
	movzbl	10(%rdx), %edi
	movb	%dil, 10(%rcx)
	leaq	11(%rdx), %rdi
	cmpq	%rdi, %r12
	je	.L273
	movzbl	11(%rdx), %edi
	movb	%dil, 11(%rcx)
	leaq	12(%rdx), %rdi
	cmpq	%rdi, %r12
	je	.L273
	movzbl	12(%rdx), %edi
	movb	%dil, 12(%rcx)
	leaq	13(%rdx), %rdi
	cmpq	%rdi, %r12
	je	.L273
	movzbl	13(%rdx), %edi
	movb	%dil, 13(%rcx)
	leaq	14(%rdx), %rdi
	cmpq	%rdi, %r12
	je	.L273
	movzbl	14(%rdx), %edx
	movb	%dl, 14(%rcx)
	.p2align 4,,10
	.p2align 3
.L273:
	subq	%r14, %r12
	leaq	1(%rax,%r12), %rdx
.L268:
	movq	%rax, %xmm0
	movq	%rdx, %xmm2
	movq	%rsi, 24(%rbx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 8(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L274:
	.cfi_restore_state
	movl	$8, %esi
	movl	$1, %ecx
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L269:
	movq	%r12, %rdi
	xorl	%edx, %edx
	subq	%r14, %rdi
	.p2align 4,,10
	.p2align 3
.L272:
	movzbl	(%r14,%rdx), %ecx
	movb	%cl, (%rax,%rdx)
	addq	$1, %rdx
	cmpq	%rdi, %rdx
	jne	.L272
	jmp	.L273
.L287:
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	jmp	.L267
.L285:
	leaq	.LC3(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L286:
	cmpq	%rsi, %rax
	cmovb	%rax, %rcx
	leaq	7(%rcx), %rsi
	andq	$-8, %rsi
	jmp	.L265
	.cfi_endproc
.LFE20944:
	.size	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE12emplace_backIJhEEEvDpOT_, .-_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE12emplace_backIJhEEEvDpOT_
	.section	.text._ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_,"axG",@progbits,_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_
	.type	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_, @function
_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_:
.LFB21814:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r13
	movq	8(%rdi), %r12
	movq	%r13, %rax
	subq	%r12, %rax
	cmpq	$2147483647, %rax
	je	.L326
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r14
	movq	%rsi, %rbx
	subq	%r12, %rdx
	testq	%rax, %rax
	je	.L306
	leaq	(%rax,%rax), %rdi
	movl	$2147483648, %esi
	movl	$2147483647, %ecx
	cmpq	%rdi, %rax
	jbe	.L327
.L290:
	movq	(%r14), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %r8
	subq	%rax, %r8
	cmpq	%rsi, %r8
	jb	.L328
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L293:
	leaq	(%rax,%rcx), %rsi
	leaq	1(%rax), %rcx
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L327:
	testq	%rdi, %rdi
	jne	.L329
	movl	$1, %ecx
	xorl	%esi, %esi
	xorl	%eax, %eax
.L291:
	movzbl	(%r15), %edi
	movb	%dil, (%rax,%rdx)
	cmpq	%r12, %rbx
	je	.L294
	leaq	-1(%rbx), %rdx
	subq	%r12, %rdx
	cmpq	$14, %rdx
	jbe	.L295
	leaq	15(%rax), %rdx
	subq	%r12, %rdx
	cmpq	$30, %rdx
	jbe	.L295
	movq	%rbx, %r8
	xorl	%edx, %edx
	subq	%r12, %r8
	movq	%r8, %rcx
	andq	$-16, %rcx
	.p2align 4,,10
	.p2align 3
.L296:
	movdqu	(%r12,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L296
	movq	%r8, %rdi
	andq	$-16, %rdi
	leaq	(%r12,%rdi), %rdx
	leaq	(%rax,%rdi), %rcx
	cmpq	%rdi, %r8
	je	.L299
	movzbl	(%rdx), %edi
	movb	%dil, (%rcx)
	leaq	1(%rdx), %rdi
	cmpq	%rdi, %rbx
	je	.L299
	movzbl	1(%rdx), %edi
	movb	%dil, 1(%rcx)
	leaq	2(%rdx), %rdi
	cmpq	%rdi, %rbx
	je	.L299
	movzbl	2(%rdx), %edi
	movb	%dil, 2(%rcx)
	leaq	3(%rdx), %rdi
	cmpq	%rdi, %rbx
	je	.L299
	movzbl	3(%rdx), %edi
	movb	%dil, 3(%rcx)
	leaq	4(%rdx), %rdi
	cmpq	%rdi, %rbx
	je	.L299
	movzbl	4(%rdx), %edi
	movb	%dil, 4(%rcx)
	leaq	5(%rdx), %rdi
	cmpq	%rdi, %rbx
	je	.L299
	movzbl	5(%rdx), %edi
	movb	%dil, 5(%rcx)
	leaq	6(%rdx), %rdi
	cmpq	%rdi, %rbx
	je	.L299
	movzbl	6(%rdx), %edi
	movb	%dil, 6(%rcx)
	leaq	7(%rdx), %rdi
	cmpq	%rdi, %rbx
	je	.L299
	movzbl	7(%rdx), %edi
	movb	%dil, 7(%rcx)
	leaq	8(%rdx), %rdi
	cmpq	%rdi, %rbx
	je	.L299
	movzbl	8(%rdx), %edi
	movb	%dil, 8(%rcx)
	leaq	9(%rdx), %rdi
	cmpq	%rdi, %rbx
	je	.L299
	movzbl	9(%rdx), %edi
	movb	%dil, 9(%rcx)
	leaq	10(%rdx), %rdi
	cmpq	%rdi, %rbx
	je	.L299
	movzbl	10(%rdx), %edi
	movb	%dil, 10(%rcx)
	leaq	11(%rdx), %rdi
	cmpq	%rdi, %rbx
	je	.L299
	movzbl	11(%rdx), %edi
	movb	%dil, 11(%rcx)
	leaq	12(%rdx), %rdi
	cmpq	%rdi, %rbx
	je	.L299
	movzbl	12(%rdx), %edi
	movb	%dil, 12(%rcx)
	leaq	13(%rdx), %rdi
	cmpq	%rdi, %rbx
	je	.L299
	movzbl	13(%rdx), %edi
	movb	%dil, 13(%rcx)
	leaq	14(%rdx), %rdi
	cmpq	%rdi, %rbx
	je	.L299
	movzbl	14(%rdx), %edx
	movb	%dl, 14(%rcx)
	.p2align 4,,10
	.p2align 3
.L299:
	movq	%rbx, %rdx
	subq	%r12, %rdx
	leaq	1(%rax,%rdx), %rcx
.L294:
	cmpq	%r13, %rbx
	je	.L300
	leaq	15(%rbx), %rdx
	subq	%rcx, %rdx
	cmpq	$30, %rdx
	jbe	.L301
	movq	%rbx, %rdx
	notq	%rdx
	addq	%r13, %rdx
	cmpq	$14, %rdx
	jbe	.L301
	movq	%r13, %r9
	xorl	%edx, %edx
	subq	%rbx, %r9
	movq	%r9, %rdi
	andq	$-16, %rdi
	.p2align 4,,10
	.p2align 3
.L302:
	movdqu	(%rbx,%rdx), %xmm2
	movups	%xmm2, (%rcx,%rdx)
	addq	$16, %rdx
	cmpq	%rdi, %rdx
	jne	.L302
	movq	%r9, %r8
	andq	$-16, %r8
	leaq	(%rcx,%r8), %rdi
	leaq	(%rbx,%r8), %rdx
	cmpq	%r8, %r9
	je	.L305
	movzbl	(%rdx), %r8d
	movb	%r8b, (%rdi)
	leaq	1(%rdx), %r8
	cmpq	%r8, %r13
	je	.L305
	movzbl	1(%rdx), %r8d
	movb	%r8b, 1(%rdi)
	leaq	2(%rdx), %r8
	cmpq	%r8, %r13
	je	.L305
	movzbl	2(%rdx), %r8d
	movb	%r8b, 2(%rdi)
	leaq	3(%rdx), %r8
	cmpq	%r8, %r13
	je	.L305
	movzbl	3(%rdx), %r8d
	movb	%r8b, 3(%rdi)
	leaq	4(%rdx), %r8
	cmpq	%r8, %r13
	je	.L305
	movzbl	4(%rdx), %r8d
	movb	%r8b, 4(%rdi)
	leaq	5(%rdx), %r8
	cmpq	%r8, %r13
	je	.L305
	movzbl	5(%rdx), %r8d
	movb	%r8b, 5(%rdi)
	leaq	6(%rdx), %r8
	cmpq	%r8, %r13
	je	.L305
	movzbl	6(%rdx), %r8d
	movb	%r8b, 6(%rdi)
	leaq	7(%rdx), %r8
	cmpq	%r8, %r13
	je	.L305
	movzbl	7(%rdx), %r8d
	movb	%r8b, 7(%rdi)
	leaq	8(%rdx), %r8
	cmpq	%r8, %r13
	je	.L305
	movzbl	8(%rdx), %r8d
	movb	%r8b, 8(%rdi)
	leaq	9(%rdx), %r8
	cmpq	%r8, %r13
	je	.L305
	movzbl	9(%rdx), %r8d
	movb	%r8b, 9(%rdi)
	leaq	10(%rdx), %r8
	cmpq	%r8, %r13
	je	.L305
	movzbl	10(%rdx), %r8d
	movb	%r8b, 10(%rdi)
	leaq	11(%rdx), %r8
	cmpq	%r8, %r13
	je	.L305
	movzbl	11(%rdx), %r8d
	movb	%r8b, 11(%rdi)
	leaq	12(%rdx), %r8
	cmpq	%r8, %r13
	je	.L305
	movzbl	12(%rdx), %r8d
	movb	%r8b, 12(%rdi)
	leaq	13(%rdx), %r8
	cmpq	%r8, %r13
	je	.L305
	movzbl	13(%rdx), %r8d
	movb	%r8b, 13(%rdi)
	leaq	14(%rdx), %r8
	cmpq	%r8, %r13
	je	.L305
	movzbl	14(%rdx), %edx
	movb	%dl, 14(%rdi)
	.p2align 4,,10
	.p2align 3
.L305:
	subq	%rbx, %r13
	addq	%r13, %rcx
.L300:
	movq	%rax, %xmm0
	movq	%rcx, %xmm3
	movq	%rsi, 24(%r14)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 8(%r14)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L306:
	.cfi_restore_state
	movl	$8, %esi
	movl	$1, %ecx
	jmp	.L290
.L328:
	movq	%rdx, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rdx
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L301:
	movq	%r13, %r8
	xorl	%edx, %edx
	subq	%rbx, %r8
	.p2align 4,,10
	.p2align 3
.L304:
	movzbl	(%rbx,%rdx), %edi
	movb	%dil, (%rcx,%rdx)
	addq	$1, %rdx
	cmpq	%r8, %rdx
	jne	.L304
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L295:
	movq	%rbx, %rdi
	xorl	%edx, %edx
	subq	%r12, %rdi
	.p2align 4,,10
	.p2align 3
.L298:
	movzbl	(%r12,%rdx), %ecx
	movb	%cl, (%rax,%rdx)
	addq	$1, %rdx
	cmpq	%rdi, %rdx
	jne	.L298
	jmp	.L299
.L329:
	cmpq	%rsi, %rdi
	cmovb	%rdi, %rcx
	leaq	7(%rcx), %rsi
	andq	$-8, %rsi
	jmp	.L290
.L326:
	leaq	.LC3(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE21814:
	.size	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_, .-_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_
	.section	.text._ZN2v88internal11interpreter19BytecodeArrayWriter12EmitBytecodeEPKNS1_12BytecodeNodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19BytecodeArrayWriter12EmitBytecodeEPKNS1_12BytecodeNodeE
	.type	_ZN2v88internal11interpreter19BytecodeArrayWriter12EmitBytecodeEPKNS1_12BytecodeNodeE, @function
_ZN2v88internal11interpreter19BytecodeArrayWriter12EmitBytecodeEPKNS1_12BytecodeNodeE:
.LFB18599:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movzbl	28(%rsi), %r12d
	movzbl	(%rsi), %r15d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$1, %r12b
	je	.L362
	cmpb	$2, %r12b
	je	.L355
	cmpb	$4, %r12b
	jne	.L337
	movl	$1, %eax
.L333:
	movb	%al, -58(%rbp)
	leaq	16(%rbx), %r13
	leaq	-58(%rbp), %rax
	movq	%rax, %rsi
	movq	%r13, %rdi
	movq	%rdx, -80(%rbp)
	movq	%rax, -72(%rbp)
	call	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE12emplace_backIJhEEEvDpOT_
	movq	-80(%rbp), %rdx
.L332:
	movq	-72(%rbp), %rsi
	shrq	%r12
	movq	%r13, %rdi
	movq	%rdx, -80(%rbp)
	andl	$127, %r12d
	movb	%r15b, -58(%rbp)
	call	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE12emplace_backIJhEEEvDpOT_
	imulq	$183, %r12, %rax
	movq	-80(%rbp), %rdx
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandSizesE(%rip), %rcx
	movl	24(%rdx), %esi
	addq	%r15, %rax
	movq	(%rcx,%rax,8), %rcx
	testl	%esi, %esi
	jle	.L330
	subl	$1, %esi
	leaq	-57(%rbp), %rax
	leaq	4(%rdx), %r15
	movq	%rcx, %r14
	movq	%rax, -80(%rbp)
	leaq	8(%rdx,%rsi,4), %r12
	jmp	.L353
	.p2align 4,,10
	.p2align 3
.L363:
	testb	%dl, %dl
	je	.L337
	movl	(%r15), %edx
	movq	-72(%rbp), %rsi
	movq	%r13, %rdi
	movb	%dl, -58(%rbp)
	call	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE12emplace_backIJhEEEvDpOT_
.L340:
	addq	$4, %r15
	addq	$1, %r14
	cmpq	%r12, %r15
	je	.L330
.L353:
	movzbl	(%r14), %edx
	cmpb	$2, %dl
	je	.L335
	jbe	.L363
	cmpb	$4, %dl
	jne	.L340
	movq	32(%rbx), %rsi
	cmpq	40(%rbx), %rsi
	je	.L345
	movzbl	(%r15), %edx
	movb	%dl, (%rsi)
	movq	32(%rbx), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 32(%rbx)
.L346:
	cmpq	%rsi, 40(%rbx)
	je	.L347
	movzbl	1(%r15), %edx
	movb	%dl, (%rsi)
	movq	32(%rbx), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 32(%rbx)
.L348:
	cmpq	%rsi, 40(%rbx)
	je	.L349
	movzbl	2(%r15), %edx
	movb	%dl, (%rsi)
	movq	32(%rbx), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 32(%rbx)
.L350:
	cmpq	40(%rbx), %rsi
	je	.L351
	movzbl	3(%r15), %edx
	addq	$4, %r15
	addq	$1, %r14
	movb	%dl, (%rsi)
	addq	$1, 32(%rbx)
	cmpq	%r12, %r15
	jne	.L353
	.p2align 4,,10
	.p2align 3
.L330:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L364
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L335:
	.cfi_restore_state
	movl	(%r15), %edx
	movq	32(%rbx), %rsi
	movw	%dx, -58(%rbp)
	cmpq	40(%rbx), %rsi
	je	.L341
	movb	%dl, (%rsi)
	movq	32(%rbx), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 32(%rbx)
.L342:
	cmpq	%rsi, 40(%rbx)
	je	.L343
	movzbl	-57(%rbp), %edx
	movb	%dl, (%rsi)
	addq	$1, 32(%rbx)
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L343:
	movq	-80(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L341:
	movq	-72(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_
	movq	32(%rbx), %rsi
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L351:
	leaq	3(%r15), %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L349:
	leaq	2(%r15), %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_
	movq	32(%rbx), %rsi
	jmp	.L350
	.p2align 4,,10
	.p2align 3
.L347:
	leaq	1(%r15), %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_
	movq	32(%rbx), %rsi
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L345:
	movq	%r15, %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIhN2v88internal13ZoneAllocatorIhEEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS4_EEDpOT_
	movq	32(%rbx), %rsi
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L355:
	xorl	%eax, %eax
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L362:
	leaq	-58(%rbp), %rax
	leaq	16(%rdi), %r13
	movq	%rax, -72(%rbp)
	jmp	.L332
.L337:
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L364:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18599:
	.size	_ZN2v88internal11interpreter19BytecodeArrayWriter12EmitBytecodeEPKNS1_12BytecodeNodeE, .-_ZN2v88internal11interpreter19BytecodeArrayWriter12EmitBytecodeEPKNS1_12BytecodeNodeE
	.section	.rodata._ZN2v88internal11interpreter19BytecodeArrayWriter12EmitJumpLoopEPNS1_12BytecodeNodeEPNS1_18BytecodeLoopHeaderE.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"current_offset >= loop_header->offset()"
	.section	.rodata._ZN2v88internal11interpreter19BytecodeArrayWriter12EmitJumpLoopEPNS1_12BytecodeNodeEPNS1_18BytecodeLoopHeaderE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"Check failed: %s."
	.section	.rodata._ZN2v88internal11interpreter19BytecodeArrayWriter12EmitJumpLoopEPNS1_12BytecodeNodeEPNS1_18BytecodeLoopHeaderE.str1.8
	.align 8
.LC6:
	.string	"current_offset <= static_cast<size_t>(kMaxUInt32)"
	.section	.text._ZN2v88internal11interpreter19BytecodeArrayWriter12EmitJumpLoopEPNS1_12BytecodeNodeEPNS1_18BytecodeLoopHeaderE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19BytecodeArrayWriter12EmitJumpLoopEPNS1_12BytecodeNodeEPNS1_18BytecodeLoopHeaderE
	.type	_ZN2v88internal11interpreter19BytecodeArrayWriter12EmitJumpLoopEPNS1_12BytecodeNodeEPNS1_18BytecodeLoopHeaderE, @function
_ZN2v88internal11interpreter19BytecodeArrayWriter12EmitJumpLoopEPNS1_12BytecodeNodeEPNS1_18BytecodeLoopHeaderE:
.LFB18605:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdx), %rdx
	movq	32(%rdi), %rax
	subq	24(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpq	%rdx, %rax
	jb	.L376
	movl	$4294967295, %ecx
	cmpq	%rcx, %rax
	ja	.L377
	subl	%edx, %eax
	movzbl	(%rsi), %ecx
	leaq	_ZN2v88internal11interpreter9Bytecodes17kOperandTypeInfosE(%rip), %rdx
	cmpl	$256, %eax
	sbbl	$-1, %eax
	movl	%eax, 4(%rsi)
	movq	(%rdx,%rcx,8), %rdx
	movzbl	(%rdx), %edx
	cmpb	$1, %dl
	je	.L378
	cmpb	$2, %dl
	je	.L379
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter19BytecodeArrayWriter12EmitBytecodeEPKNS1_12BytecodeNodeE
	.p2align 4,,10
	.p2align 3
.L378:
	.cfi_restore_state
	leal	128(%rax), %ecx
	cmpl	$255, %ecx
	jbe	.L372
	addl	$32768, %eax
	cmpl	$65536, %eax
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
.L372:
	movzbl	28(%rsi), %eax
	cmpb	%dl, %al
	cmovnb	%eax, %edx
	movb	%dl, 28(%rsi)
.L380:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter19BytecodeArrayWriter12EmitBytecodeEPKNS1_12BytecodeNodeE
	.p2align 4,,10
	.p2align 3
.L379:
	.cfi_restore_state
	cmpl	$255, %eax
	jbe	.L374
	cmpl	$65536, %eax
	movl	$4, %eax
	cmovnb	%eax, %edx
	movzbl	28(%rsi), %eax
	cmpb	%dl, %al
	cmovnb	%eax, %edx
	movb	%dl, 28(%rsi)
	jmp	.L380
	.p2align 4,,10
	.p2align 3
.L376:
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L377:
	leaq	.LC6(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L374:
	movl	$1, %edx
	jmp	.L372
	.cfi_endproc
.LFE18605:
	.size	_ZN2v88internal11interpreter19BytecodeArrayWriter12EmitJumpLoopEPNS1_12BytecodeNodeEPNS1_18BytecodeLoopHeaderE, .-_ZN2v88internal11interpreter19BytecodeArrayWriter12EmitJumpLoopEPNS1_12BytecodeNodeEPNS1_18BytecodeLoopHeaderE
	.section	.text._ZN2v88internal11interpreter19BytecodeArrayWriter8EmitJumpEPNS1_12BytecodeNodeEPNS1_13BytecodeLabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19BytecodeArrayWriter8EmitJumpEPNS1_12BytecodeNodeEPNS1_13BytecodeLabelE
	.type	_ZN2v88internal11interpreter19BytecodeArrayWriter8EmitJumpEPNS1_12BytecodeNodeEPNS1_13BytecodeLabelE, @function
_ZN2v88internal11interpreter19BytecodeArrayWriter8EmitJumpEPNS1_12BytecodeNodeEPNS1_13BytecodeLabelE:
.LFB18606:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	32(%rdi), %rax
	movq	%rdi, %r12
	addl	$1, 48(%rdi)
	subq	24(%rdi), %rax
	movq	%rax, 8(%rdx)
	movq	112(%rdi), %rdi
	call	_ZN2v88internal11interpreter20ConstantArrayBuilder19CreateReservedEntryEv@PLT
	cmpb	$2, %al
	je	.L382
	ja	.L383
	testb	%al, %al
	je	.L415
	movl	(%r12), %edx
.L414:
	movzbl	0(%r13), %ecx
	leaq	_ZN2v88internal11interpreter9Bytecodes17kOperandTypeInfosE(%rip), %rax
	movl	%edx, 4(%r13)
	movq	(%rax,%rcx,8), %rax
	movzbl	(%rax), %eax
	cmpb	$1, %al
	je	.L416
	cmpb	$2, %al
	je	.L417
.L387:
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter19BytecodeArrayWriter12EmitBytecodeEPKNS1_12BytecodeNodeE
	.p2align 4,,10
	.p2align 3
.L383:
	.cfi_restore_state
	cmpb	$4, %al
	jne	.L387
	movl	8(%r12), %ecx
	movzbl	0(%r13), %esi
	leaq	_ZN2v88internal11interpreter9Bytecodes17kOperandTypeInfosE(%rip), %rdx
	movl	%ecx, 4(%r13)
	movq	(%rdx,%rsi,8), %rdx
	movzbl	(%rdx), %edx
	cmpb	$1, %dl
	je	.L418
	cmpb	$2, %dl
	jne	.L387
	cmpl	$255, %ecx
	jbe	.L404
	cmpl	$65536, %ecx
	cmovnb	%eax, %edx
	jmp	.L398
	.p2align 4,,10
	.p2align 3
.L382:
	movl	4(%r12), %edx
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L417:
	cmpl	$255, %edx
	jbe	.L402
	cmpl	$65536, %edx
	movl	$4, %edx
	cmovnb	%edx, %eax
.L395:
	movzbl	28(%r13), %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	cmpb	%al, %dl
	cmovnb	%edx, %eax
	movb	%al, 28(%r13)
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter19BytecodeArrayWriter12EmitBytecodeEPKNS1_12BytecodeNodeE
	.p2align 4,,10
	.p2align 3
.L418:
	.cfi_restore_state
	leal	128(%rcx), %eax
	cmpl	$255, %eax
	jbe	.L398
	addl	$32768, %ecx
	cmpl	$65536, %ecx
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
.L398:
	movzbl	28(%r13), %eax
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	cmpb	%dl, %al
	cmovnb	%eax, %edx
	movb	%dl, 28(%r13)
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter19BytecodeArrayWriter12EmitBytecodeEPKNS1_12BytecodeNodeE
	.p2align 4,,10
	.p2align 3
.L416:
	.cfi_restore_state
	leal	128(%rdx), %ecx
	cmpl	$255, %ecx
	jbe	.L395
	addl	$32768, %edx
	cmpl	$65536, %edx
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
	jmp	.L395
	.p2align 4,,10
	.p2align 3
.L404:
	movl	$1, %edx
	jmp	.L398
	.p2align 4,,10
	.p2align 3
.L402:
	movl	$1, %eax
	jmp	.L395
.L415:
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18606:
	.size	_ZN2v88internal11interpreter19BytecodeArrayWriter8EmitJumpEPNS1_12BytecodeNodeEPNS1_13BytecodeLabelE, .-_ZN2v88internal11interpreter19BytecodeArrayWriter8EmitJumpEPNS1_12BytecodeNodeEPNS1_13BytecodeLabelE
	.section	.text._ZN2v88internal11interpreter19BytecodeArrayWriter10EmitSwitchEPNS1_12BytecodeNodeEPNS1_17BytecodeJumpTableE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19BytecodeArrayWriter10EmitSwitchEPNS1_12BytecodeNodeEPNS1_17BytecodeJumpTableE
	.type	_ZN2v88internal11interpreter19BytecodeArrayWriter10EmitSwitchEPNS1_12BytecodeNodeEPNS1_17BytecodeJumpTableE, @function
_ZN2v88internal11interpreter19BytecodeArrayWriter10EmitSwitchEPNS1_12BytecodeNodeEPNS1_17BytecodeJumpTableE:
.LFB18607:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	subq	24(%rdi), %rax
	cmpb	$2, 28(%rsi)
	sbbq	$-1, %rax
	movq	%rax, 8(%rdx)
	jmp	_ZN2v88internal11interpreter19BytecodeArrayWriter12EmitBytecodeEPKNS1_12BytecodeNodeE
	.cfi_endproc
.LFE18607:
	.size	_ZN2v88internal11interpreter19BytecodeArrayWriter10EmitSwitchEPNS1_12BytecodeNodeEPNS1_17BytecodeJumpTableE, .-_ZN2v88internal11interpreter19BytecodeArrayWriter10EmitSwitchEPNS1_12BytecodeNodeEPNS1_17BytecodeJumpTableE
	.section	.text._ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE
	.type	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE, @function
_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE:
.LFB18584:
	.cfi_startproc
	endbr64
	cmpb	$0, 138(%rdi)
	jne	.L421
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movzbl	(%rsi), %ebx
	leal	117(%rbx), %eax
	cmpb	$42, %al
	ja	.L423
	movabsq	$4543001657347, %rdx
	btq	%rax, %rdx
	jc	.L467
.L423:
	movq	32(%r12), %rcx
	movq	24(%r12), %rdx
	movzbl	32(%r13), %eax
	movq	%rcx, %r14
	subq	%rdx, %r14
	cmpb	$0, 137(%r12)
	je	.L424
	movzbl	120(%r12), %esi
	testb	%al, %al
	movq	%r14, %rax
	setne	%r15b
	cmpb	$37, %sil
	ja	.L426
	movabsq	$137691133952, %rdi
	btq	%rsi, %rdi
	jnc	.L426
	movzbl	%bl, %esi
	leaq	_ZN2v88internal11interpreter9Bytecodes15kAccumulatorUseE(%rip), %rdi
	cmpb	$2, (%rdi,%rsi)
	je	.L468
.L426:
	movb	%bl, 120(%r12)
	movb	%r15b, 136(%r12)
	movq	%rax, 128(%r12)
	movzbl	32(%r13), %eax
.L424:
	testb	%al, %al
	jne	.L469
.L444:
	addq	$40, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter19BytecodeArrayWriter12EmitBytecodeEPKNS1_12BytecodeNodeE
	.p2align 4,,10
	.p2align 3
.L469:
	.cfi_restore_state
	movl	36(%r13), %ebx
	movslq	%r14d, %rsi
	leaq	56(%r12), %rdi
	movabsq	$-140735340871681, %rcx
	leal	1(%rbx), %edx
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	andq	%rcx, %rdx
	xorl	%ecx, %ecx
	cmpb	$2, %al
	sete	%cl
	call	_ZN2v88internal26SourcePositionTableBuilder11AddPositionEmNS0_14SourcePositionEb@PLT
	jmp	.L444
	.p2align 4,,10
	.p2align 3
.L421:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
.L467:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movb	$1, 138(%rdi)
	movzbl	(%rsi), %ebx
	jmp	.L423
	.p2align 4,,10
	.p2align 3
.L468:
	movzbl	136(%r12), %esi
	movl	%r15d, %edi
	andb	%sil, %dil
	jne	.L446
	movq	128(%r12), %r8
	cmpq	%r14, %r8
	ja	.L470
	jnb	.L428
	addq	%r8, %rdx
	cmpq	%rcx, %rdx
	je	.L428
	movq	%rdx, 32(%r12)
	movq	%r8, %r14
	movq	%r8, %rax
.L428:
	orl	%esi, %r15d
	jmp	.L426
	.p2align 4,,10
	.p2align 3
.L446:
	movl	%edi, %r15d
	jmp	.L426
	.p2align 4,,10
	.p2align 3
.L470:
	movq	%r8, %rdx
	subq	%r14, %rdx
	je	.L428
	movl	$2147483647, %eax
	subq	%r14, %rax
	movq	%rax, %rsi
	movq	40(%r12), %rax
	subq	%rcx, %rax
	cmpq	%rax, %rdx
	ja	.L429
	xorl	%esi, %esi
	movq	%rcx, %rdi
	movq	%rdx, -56(%rbp)
	call	memset@PLT
	movq	-56(%rbp), %rdx
	movzbl	136(%r12), %esi
	leaq	(%rax,%rdx), %r14
	movq	%r14, 32(%r12)
	subq	24(%r12), %r14
	movq	%r14, %rax
	jmp	.L428
.L429:
	cmpq	%rsi, %rdx
	ja	.L471
	cmpq	%r14, %rdx
	movq	%r14, %rcx
	cmovnb	%rdx, %rcx
	addq	%r14, %rcx
	jc	.L432
	cmpq	$2147483647, %rcx
	movl	$2147483647, %eax
	cmova	%rax, %rcx
	leaq	7(%rcx), %rsi
	andq	$-8, %rsi
.L445:
	movq	16(%r12), %rdi
	movq	16(%rdi), %r9
	movq	24(%rdi), %rax
	subq	%r9, %rax
	cmpq	%rsi, %rax
	jb	.L472
	addq	%r9, %rsi
	movq	%rsi, 16(%rdi)
.L435:
	xorl	%esi, %esi
	leaq	(%r9,%r14), %rdi
	movq	%rcx, -72(%rbp)
	movq	%r8, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	memset@PLT
	movq	32(%r12), %rsi
	movq	24(%r12), %rdx
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %r8
	cmpq	%rdx, %rsi
	movq	-72(%rbp), %rcx
	je	.L440
	leaq	-1(%rsi), %rax
	subq	%rdx, %rax
	cmpq	$14, %rax
	jbe	.L437
	leaq	15(%rdx), %rax
	subq	%r9, %rax
	cmpq	$30, %rax
	jbe	.L437
	movq	%rsi, %r10
	movq	%rdx, %r11
	movq	%r9, %rax
	subq	%rdx, %r10
	subq	%r9, %r11
	movq	%r10, %rdi
	andq	$-16, %rdi
	addq	%r9, %rdi
	.p2align 4,,10
	.p2align 3
.L438:
	movdqu	(%rax,%r11), %xmm0
	addq	$16, %rax
	movups	%xmm0, -16(%rax)
	cmpq	%rdi, %rax
	jne	.L438
	movq	%r10, %rdi
	andq	$-16, %rdi
	leaq	(%rdx,%rdi), %rax
	leaq	(%r9,%rdi), %rdx
	cmpq	%r10, %rdi
	je	.L440
	movzbl	(%rax), %edi
	movb	%dil, (%rdx)
	leaq	1(%rax), %rdi
	cmpq	%rdi, %rsi
	je	.L440
	movzbl	1(%rax), %edi
	movb	%dil, 1(%rdx)
	leaq	2(%rax), %rdi
	cmpq	%rdi, %rsi
	je	.L440
	movzbl	2(%rax), %edi
	movb	%dil, 2(%rdx)
	leaq	3(%rax), %rdi
	cmpq	%rdi, %rsi
	je	.L440
	movzbl	3(%rax), %edi
	movb	%dil, 3(%rdx)
	leaq	4(%rax), %rdi
	cmpq	%rdi, %rsi
	je	.L440
	movzbl	4(%rax), %edi
	movb	%dil, 4(%rdx)
	leaq	5(%rax), %rdi
	cmpq	%rdi, %rsi
	je	.L440
	movzbl	5(%rax), %edi
	movb	%dil, 5(%rdx)
	leaq	6(%rax), %rdi
	cmpq	%rdi, %rsi
	je	.L440
	movzbl	6(%rax), %edi
	movb	%dil, 6(%rdx)
	leaq	7(%rax), %rdi
	cmpq	%rdi, %rsi
	je	.L440
	movzbl	7(%rax), %edi
	movb	%dil, 7(%rdx)
	leaq	8(%rax), %rdi
	cmpq	%rdi, %rsi
	je	.L440
	movzbl	8(%rax), %edi
	movb	%dil, 8(%rdx)
	leaq	9(%rax), %rdi
	cmpq	%rdi, %rsi
	je	.L440
	movzbl	9(%rax), %edi
	movb	%dil, 9(%rdx)
	leaq	10(%rax), %rdi
	cmpq	%rdi, %rsi
	je	.L440
	movzbl	10(%rax), %edi
	movb	%dil, 10(%rdx)
	leaq	11(%rax), %rdi
	cmpq	%rdi, %rsi
	je	.L440
	movzbl	11(%rax), %edi
	movb	%dil, 11(%rdx)
	leaq	12(%rax), %rdi
	cmpq	%rdi, %rsi
	je	.L440
	movzbl	12(%rax), %edi
	movb	%dil, 12(%rdx)
	leaq	13(%rax), %rdi
	cmpq	%rdi, %rsi
	je	.L440
	movzbl	13(%rax), %edi
	movb	%dil, 13(%rdx)
	leaq	14(%rax), %rdi
	cmpq	%rdi, %rsi
	je	.L440
	movzbl	14(%rax), %eax
	movb	%al, 14(%rdx)
.L440:
	leaq	(%r9,%r8), %rax
	addq	%r9, %rcx
	movq	%r9, 24(%r12)
	movq	%r8, %r14
	movzbl	136(%r12), %esi
	movq	%rax, 32(%r12)
	movq	%r8, %rax
	movq	%rcx, 40(%r12)
	jmp	.L428
.L437:
	subq	%rdx, %rsi
	movq	%rdx, %rax
	movq	%r9, %rdi
	addq	%r9, %rsi
	subq	%r9, %rax
.L442:
	movzbl	(%rdi,%rax), %edx
	addq	$1, %rdi
	movb	%dl, -1(%rdi)
	cmpq	%rsi, %rdi
	jne	.L442
	jmp	.L440
.L472:
	movq	%rcx, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %rcx
	movq	%rax, %r9
	jmp	.L435
.L432:
	movl	$2147483648, %esi
	movl	$2147483647, %ecx
	jmp	.L445
.L471:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE18584:
	.size	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE, .-_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE
	.section	.text._ZN2v88internal11interpreter19BytecodeArrayWriter11WriteSwitchEPNS1_12BytecodeNodeEPNS1_17BytecodeJumpTableE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19BytecodeArrayWriter11WriteSwitchEPNS1_12BytecodeNodeEPNS1_17BytecodeJumpTableE
	.type	_ZN2v88internal11interpreter19BytecodeArrayWriter11WriteSwitchEPNS1_12BytecodeNodeEPNS1_17BytecodeJumpTableE, @function
_ZN2v88internal11interpreter19BytecodeArrayWriter11WriteSwitchEPNS1_12BytecodeNodeEPNS1_17BytecodeJumpTableE:
.LFB18587:
	.cfi_startproc
	endbr64
	cmpb	$0, 138(%rdi)
	jne	.L473
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movzbl	(%rsi), %r15d
	leal	117(%r15), %eax
	cmpb	$42, %al
	ja	.L475
	movabsq	$4543001657347, %rdx
	btq	%rax, %rdx
	jc	.L520
.L475:
	movq	32(%r12), %rcx
	movq	24(%r12), %rdx
	movzbl	32(%r13), %eax
	movq	%rcx, %rbx
	subq	%rdx, %rbx
	cmpb	$0, 137(%r12)
	je	.L476
	movzbl	120(%r12), %esi
	testb	%al, %al
	movq	%rbx, %rax
	setne	%r8b
	cmpb	$37, %sil
	ja	.L478
	movabsq	$137691133952, %rdi
	btq	%rsi, %rdi
	jnc	.L478
	movzbl	%r15b, %esi
	leaq	_ZN2v88internal11interpreter9Bytecodes15kAccumulatorUseE(%rip), %rdi
	cmpb	$2, (%rdi,%rsi)
	je	.L521
.L478:
	movb	%r15b, 120(%r12)
	movb	%r8b, 136(%r12)
	movq	%rax, 128(%r12)
	movzbl	32(%r13), %eax
.L476:
	testb	%al, %al
	jne	.L522
.L496:
	cmpb	$2, 28(%r13)
	movq	%r13, %rsi
	movq	%r12, %rdi
	sbbq	$-1, %rbx
	movq	%rbx, 8(%r14)
	addq	$40, %rsp
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter19BytecodeArrayWriter12EmitBytecodeEPKNS1_12BytecodeNodeE
	.p2align 4,,10
	.p2align 3
.L522:
	.cfi_restore_state
	movl	36(%r13), %ecx
	movslq	%ebx, %rsi
	leaq	56(%r12), %rdi
	leal	1(%rcx), %edx
	movabsq	$-140735340871681, %rcx
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	andq	%rcx, %rdx
	xorl	%ecx, %ecx
	cmpb	$2, %al
	sete	%cl
	call	_ZN2v88internal26SourcePositionTableBuilder11AddPositionEmNS0_14SourcePositionEb@PLT
	movq	32(%r12), %rbx
	subq	24(%r12), %rbx
	jmp	.L496
	.p2align 4,,10
	.p2align 3
.L473:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
.L520:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movb	$1, 138(%rdi)
	movzbl	(%rsi), %r15d
	jmp	.L475
	.p2align 4,,10
	.p2align 3
.L521:
	movzbl	136(%r12), %esi
	movl	%r8d, %edi
	andb	%sil, %dil
	jne	.L499
	movq	128(%r12), %r9
	cmpq	%rbx, %r9
	ja	.L523
	jnb	.L480
	addq	%r9, %rdx
	cmpq	%rcx, %rdx
	je	.L480
	movq	%rdx, 32(%r12)
	movq	%r9, %rbx
	movq	%r9, %rax
.L480:
	orl	%esi, %r8d
	jmp	.L478
	.p2align 4,,10
	.p2align 3
.L499:
	movl	%edi, %r8d
	jmp	.L478
	.p2align 4,,10
	.p2align 3
.L523:
	movq	%r9, %rdx
	subq	%rbx, %rdx
	je	.L480
	movl	$2147483647, %eax
	subq	%rbx, %rax
	movq	%rax, %rsi
	movq	40(%r12), %rax
	subq	%rcx, %rax
	cmpq	%rax, %rdx
	ja	.L481
	xorl	%esi, %esi
	movq	%rcx, %rdi
	movb	%r8b, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	memset@PLT
	movq	-56(%rbp), %rdx
	movzbl	-64(%rbp), %r8d
	movzbl	136(%r12), %esi
	leaq	(%rax,%rdx), %rbx
	movq	%rbx, 32(%r12)
	subq	24(%r12), %rbx
	movq	%rbx, %rax
	jmp	.L480
.L481:
	cmpq	%rsi, %rdx
	ja	.L524
	cmpq	%rbx, %rdx
	movq	%rbx, %rcx
	cmovnb	%rdx, %rcx
	addq	%rbx, %rcx
	jc	.L484
	cmpq	$2147483647, %rcx
	movl	$2147483647, %eax
	cmova	%rax, %rcx
	leaq	7(%rcx), %rsi
	andq	$-8, %rsi
.L498:
	movq	16(%r12), %rdi
	movq	16(%rdi), %r10
	movq	24(%rdi), %rax
	subq	%r10, %rax
	cmpq	%rsi, %rax
	jb	.L525
	addq	%r10, %rsi
	movq	%rsi, 16(%rdi)
.L487:
	xorl	%esi, %esi
	leaq	(%r10,%rbx), %rdi
	movq	%rcx, -80(%rbp)
	movq	%r9, -72(%rbp)
	movb	%r8b, -64(%rbp)
	movq	%r10, -56(%rbp)
	call	memset@PLT
	movq	32(%r12), %rsi
	movq	24(%r12), %rdx
	movq	-56(%rbp), %r10
	movzbl	-64(%rbp), %r8d
	cmpq	%rdx, %rsi
	movq	-72(%rbp), %r9
	movq	-80(%rbp), %rcx
	je	.L492
	leaq	-1(%rsi), %rax
	subq	%rdx, %rax
	cmpq	$14, %rax
	jbe	.L489
	leaq	15(%rdx), %rax
	subq	%r10, %rax
	cmpq	$30, %rax
	jbe	.L489
	movq	%rsi, %rdi
	movq	%rdx, %rbx
	movq	%r10, %rax
	subq	%rdx, %rdi
	subq	%r10, %rbx
	movq	%rdi, %r11
	andq	$-16, %r11
	addq	%r10, %r11
	.p2align 4,,10
	.p2align 3
.L490:
	movdqu	(%rax,%rbx), %xmm0
	addq	$16, %rax
	movups	%xmm0, -16(%rax)
	cmpq	%r11, %rax
	jne	.L490
	movq	%rdi, %r11
	andq	$-16, %r11
	leaq	(%rdx,%r11), %rax
	leaq	(%r10,%r11), %rdx
	cmpq	%rdi, %r11
	je	.L492
	movzbl	(%rax), %edi
	movb	%dil, (%rdx)
	leaq	1(%rax), %rdi
	cmpq	%rdi, %rsi
	je	.L492
	movzbl	1(%rax), %edi
	movb	%dil, 1(%rdx)
	leaq	2(%rax), %rdi
	cmpq	%rdi, %rsi
	je	.L492
	movzbl	2(%rax), %edi
	movb	%dil, 2(%rdx)
	leaq	3(%rax), %rdi
	cmpq	%rdi, %rsi
	je	.L492
	movzbl	3(%rax), %edi
	movb	%dil, 3(%rdx)
	leaq	4(%rax), %rdi
	cmpq	%rdi, %rsi
	je	.L492
	movzbl	4(%rax), %edi
	movb	%dil, 4(%rdx)
	leaq	5(%rax), %rdi
	cmpq	%rdi, %rsi
	je	.L492
	movzbl	5(%rax), %edi
	movb	%dil, 5(%rdx)
	leaq	6(%rax), %rdi
	cmpq	%rdi, %rsi
	je	.L492
	movzbl	6(%rax), %edi
	movb	%dil, 6(%rdx)
	leaq	7(%rax), %rdi
	cmpq	%rdi, %rsi
	je	.L492
	movzbl	7(%rax), %edi
	movb	%dil, 7(%rdx)
	leaq	8(%rax), %rdi
	cmpq	%rdi, %rsi
	je	.L492
	movzbl	8(%rax), %edi
	movb	%dil, 8(%rdx)
	leaq	9(%rax), %rdi
	cmpq	%rdi, %rsi
	je	.L492
	movzbl	9(%rax), %edi
	movb	%dil, 9(%rdx)
	leaq	10(%rax), %rdi
	cmpq	%rdi, %rsi
	je	.L492
	movzbl	10(%rax), %edi
	movb	%dil, 10(%rdx)
	leaq	11(%rax), %rdi
	cmpq	%rdi, %rsi
	je	.L492
	movzbl	11(%rax), %edi
	movb	%dil, 11(%rdx)
	leaq	12(%rax), %rdi
	cmpq	%rdi, %rsi
	je	.L492
	movzbl	12(%rax), %edi
	movb	%dil, 12(%rdx)
	leaq	13(%rax), %rdi
	cmpq	%rdi, %rsi
	je	.L492
	movzbl	13(%rax), %edi
	movb	%dil, 13(%rdx)
	leaq	14(%rax), %rdi
	cmpq	%rdi, %rsi
	je	.L492
	movzbl	14(%rax), %eax
	movb	%al, 14(%rdx)
.L492:
	leaq	(%r10,%r9), %rax
	addq	%r10, %rcx
	movq	%r10, 24(%r12)
	movq	%r9, %rbx
	movzbl	136(%r12), %esi
	movq	%rax, 32(%r12)
	movq	%r9, %rax
	movq	%rcx, 40(%r12)
	jmp	.L480
.L489:
	subq	%rdx, %rsi
	movq	%rdx, %rax
	movq	%r10, %rdi
	addq	%r10, %rsi
	subq	%r10, %rax
.L494:
	movzbl	(%rdi,%rax), %edx
	addq	$1, %rdi
	movb	%dl, -1(%rdi)
	cmpq	%rsi, %rdi
	jne	.L494
	jmp	.L492
.L525:
	movq	%rcx, -80(%rbp)
	movq	%rdx, -72(%rbp)
	movq	%r9, -64(%rbp)
	movb	%r8b, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movzbl	-56(%rbp), %r8d
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %rdx
	movq	-80(%rbp), %rcx
	movq	%rax, %r10
	jmp	.L487
.L484:
	movl	$2147483648, %esi
	movl	$2147483647, %ecx
	jmp	.L498
.L524:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE18587:
	.size	_ZN2v88internal11interpreter19BytecodeArrayWriter11WriteSwitchEPNS1_12BytecodeNodeEPNS1_17BytecodeJumpTableE, .-_ZN2v88internal11interpreter19BytecodeArrayWriter11WriteSwitchEPNS1_12BytecodeNodeEPNS1_17BytecodeJumpTableE
	.section	.text._ZN2v88internal11interpreter19BytecodeArrayWriter13WriteJumpLoopEPNS1_12BytecodeNodeEPNS1_18BytecodeLoopHeaderE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19BytecodeArrayWriter13WriteJumpLoopEPNS1_12BytecodeNodeEPNS1_18BytecodeLoopHeaderE
	.type	_ZN2v88internal11interpreter19BytecodeArrayWriter13WriteJumpLoopEPNS1_12BytecodeNodeEPNS1_18BytecodeLoopHeaderE, @function
_ZN2v88internal11interpreter19BytecodeArrayWriter13WriteJumpLoopEPNS1_12BytecodeNodeEPNS1_18BytecodeLoopHeaderE:
.LFB18586:
	.cfi_startproc
	endbr64
	cmpb	$0, 138(%rdi)
	jne	.L526
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$8, %rsp
	movzbl	(%rsi), %esi
	leal	117(%rsi), %eax
	cmpb	$42, %al
	ja	.L528
	movabsq	$4543001657347, %rdx
	btq	%rax, %rdx
	jc	.L549
.L528:
	cmpb	$0, 137(%r12)
	movzbl	32(%r13), %eax
	je	.L529
	xorl	%edx, %edx
	testb	%al, %al
	movq	%r12, %rdi
	setne	%dl
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter22MaybeElideLastBytecodeENS1_8BytecodeEb.part.0
	movzbl	32(%r13), %eax
.L529:
	movq	32(%r12), %rsi
	subq	24(%r12), %rsi
	testb	%al, %al
	jne	.L550
.L530:
	movq	(%rbx), %rax
	cmpq	%rax, %rsi
	jb	.L551
	movl	$4294967295, %edx
	cmpq	%rdx, %rsi
	ja	.L552
	subl	%eax, %esi
	movzbl	0(%r13), %edx
	leaq	_ZN2v88internal11interpreter9Bytecodes17kOperandTypeInfosE(%rip), %rax
	cmpl	$256, %esi
	sbbl	$-1, %esi
	movl	%esi, 4(%r13)
	movq	(%rax,%rdx,8), %rax
	movzbl	(%rax), %eax
	cmpb	$1, %al
	je	.L553
	cmpb	$2, %al
	je	.L554
.L536:
	addq	$8, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter19BytecodeArrayWriter12EmitBytecodeEPKNS1_12BytecodeNodeE
	.p2align 4,,10
	.p2align 3
.L550:
	.cfi_restore_state
	movl	36(%r13), %ecx
	movslq	%esi, %rsi
	leaq	56(%r12), %rdi
	leal	1(%rcx), %edx
	movabsq	$-140735340871681, %rcx
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	andq	%rcx, %rdx
	xorl	%ecx, %ecx
	cmpb	$2, %al
	sete	%cl
	call	_ZN2v88internal26SourcePositionTableBuilder11AddPositionEmNS0_14SourcePositionEb@PLT
	movq	32(%r12), %rsi
	subq	24(%r12), %rsi
	jmp	.L530
	.p2align 4,,10
	.p2align 3
.L526:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
.L549:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movb	$1, 138(%rdi)
	movzbl	0(%r13), %esi
	jmp	.L528
	.p2align 4,,10
	.p2align 3
.L553:
	leal	128(%rsi), %edx
	cmpl	$255, %edx
	jbe	.L537
	addl	$32768, %esi
	cmpl	$65536, %esi
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L537:
	movzbl	28(%r13), %edx
	cmpb	%al, %dl
	cmovnb	%edx, %eax
	movb	%al, 28(%r13)
	jmp	.L536
	.p2align 4,,10
	.p2align 3
.L554:
	cmpl	$255, %esi
	jbe	.L539
	cmpl	$65536, %esi
	movl	$4, %edx
	cmovnb	%edx, %eax
	jmp	.L537
	.p2align 4,,10
	.p2align 3
.L551:
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L552:
	leaq	.LC6(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L539:
	movl	$1, %eax
	jmp	.L537
	.cfi_endproc
.LFE18586:
	.size	_ZN2v88internal11interpreter19BytecodeArrayWriter13WriteJumpLoopEPNS1_12BytecodeNodeEPNS1_18BytecodeLoopHeaderE, .-_ZN2v88internal11interpreter19BytecodeArrayWriter13WriteJumpLoopEPNS1_12BytecodeNodeEPNS1_18BytecodeLoopHeaderE
	.section	.text._ZN2v88internal11interpreter19BytecodeArrayWriter9WriteJumpEPNS1_12BytecodeNodeEPNS1_13BytecodeLabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19BytecodeArrayWriter9WriteJumpEPNS1_12BytecodeNodeEPNS1_13BytecodeLabelE
	.type	_ZN2v88internal11interpreter19BytecodeArrayWriter9WriteJumpEPNS1_12BytecodeNodeEPNS1_13BytecodeLabelE, @function
_ZN2v88internal11interpreter19BytecodeArrayWriter9WriteJumpEPNS1_12BytecodeNodeEPNS1_13BytecodeLabelE:
.LFB18585:
	.cfi_startproc
	endbr64
	cmpb	$0, 138(%rdi)
	jne	.L555
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$8, %rsp
	movzbl	(%rsi), %esi
	leal	117(%rsi), %eax
	cmpb	$42, %al
	ja	.L557
	movabsq	$4543001657347, %rdx
	btq	%rax, %rdx
	jc	.L601
.L557:
	cmpb	$0, 137(%r12)
	movzbl	32(%r13), %eax
	je	.L558
	xorl	%edx, %edx
	testb	%al, %al
	movq	%r12, %rdi
	setne	%dl
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter22MaybeElideLastBytecodeENS1_8BytecodeEb.part.0
	movzbl	32(%r13), %eax
.L558:
	movq	32(%r12), %rsi
	subq	24(%r12), %rsi
	testb	%al, %al
	jne	.L602
.L559:
	addl	$1, 48(%r12)
	movq	%rsi, 8(%rbx)
	movq	112(%r12), %rdi
	call	_ZN2v88internal11interpreter20ConstantArrayBuilder19CreateReservedEntryEv@PLT
	cmpb	$2, %al
	je	.L560
	ja	.L561
	testb	%al, %al
	je	.L603
	movl	(%r12), %edx
.L600:
	movzbl	0(%r13), %ecx
	leaq	_ZN2v88internal11interpreter9Bytecodes17kOperandTypeInfosE(%rip), %rax
	movl	%edx, 4(%r13)
	movq	(%rax,%rcx,8), %rax
	movzbl	(%rax), %eax
	cmpb	$1, %al
	je	.L604
	cmpb	$2, %al
	je	.L605
.L565:
	addq	$8, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter19BytecodeArrayWriter12EmitBytecodeEPKNS1_12BytecodeNodeE
	.p2align 4,,10
	.p2align 3
.L561:
	.cfi_restore_state
	cmpb	$4, %al
	jne	.L565
	movl	8(%r12), %ecx
	movzbl	0(%r13), %esi
	leaq	_ZN2v88internal11interpreter9Bytecodes17kOperandTypeInfosE(%rip), %rdx
	movl	%ecx, 4(%r13)
	movq	(%rdx,%rsi,8), %rdx
	movzbl	(%rdx), %edx
	cmpb	$1, %dl
	je	.L606
	cmpb	$2, %dl
	jne	.L565
	cmpl	$255, %ecx
	jbe	.L582
	cmpl	$65536, %ecx
	cmovnb	%eax, %edx
.L576:
	movzbl	28(%r13), %eax
	cmpb	%dl, %al
	cmovnb	%eax, %edx
	movb	%dl, 28(%r13)
	jmp	.L565
	.p2align 4,,10
	.p2align 3
.L602:
	movl	36(%r13), %ecx
	movslq	%esi, %rsi
	leaq	56(%r12), %rdi
	leal	1(%rcx), %edx
	movabsq	$-140735340871681, %rcx
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	andq	%rcx, %rdx
	xorl	%ecx, %ecx
	cmpb	$2, %al
	sete	%cl
	call	_ZN2v88internal26SourcePositionTableBuilder11AddPositionEmNS0_14SourcePositionEb@PLT
	movq	32(%r12), %rsi
	subq	24(%r12), %rsi
	jmp	.L559
	.p2align 4,,10
	.p2align 3
.L555:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
.L601:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movb	$1, 138(%rdi)
	movzbl	0(%r13), %esi
	jmp	.L557
	.p2align 4,,10
	.p2align 3
.L560:
	movl	4(%r12), %edx
	jmp	.L600
	.p2align 4,,10
	.p2align 3
.L605:
	cmpl	$255, %edx
	jbe	.L580
	cmpl	$65536, %edx
	movl	$4, %edx
	cmovnb	%edx, %eax
	jmp	.L573
	.p2align 4,,10
	.p2align 3
.L604:
	leal	128(%rdx), %ecx
	cmpl	$255, %ecx
	jbe	.L573
	addl	$32768, %edx
	cmpl	$65536, %edx
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L573:
	movzbl	28(%r13), %edx
	cmpb	%al, %dl
	cmovnb	%edx, %eax
	movb	%al, 28(%r13)
	jmp	.L565
	.p2align 4,,10
	.p2align 3
.L606:
	leal	128(%rcx), %eax
	cmpl	$255, %eax
	jbe	.L576
	addl	$32768, %ecx
	cmpl	$65536, %ecx
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	jmp	.L576
	.p2align 4,,10
	.p2align 3
.L580:
	movl	$1, %eax
	jmp	.L573
	.p2align 4,,10
	.p2align 3
.L582:
	movl	$1, %edx
	jmp	.L576
.L603:
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18585:
	.size	_ZN2v88internal11interpreter19BytecodeArrayWriter9WriteJumpEPNS1_12BytecodeNodeEPNS1_13BytecodeLabelE, .-_ZN2v88internal11interpreter19BytecodeArrayWriter9WriteJumpEPNS1_12BytecodeNodeEPNS1_13BytecodeLabelE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal11interpreter19BytecodeArrayWriter24kMaxSizeOfPackedBytecodeE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal11interpreter19BytecodeArrayWriter24kMaxSizeOfPackedBytecodeE, @function
_GLOBAL__sub_I__ZN2v88internal11interpreter19BytecodeArrayWriter24kMaxSizeOfPackedBytecodeE:
.LFB22696:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE22696:
	.size	_GLOBAL__sub_I__ZN2v88internal11interpreter19BytecodeArrayWriter24kMaxSizeOfPackedBytecodeE, .-_GLOBAL__sub_I__ZN2v88internal11interpreter19BytecodeArrayWriter24kMaxSizeOfPackedBytecodeE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal11interpreter19BytecodeArrayWriter24kMaxSizeOfPackedBytecodeE
	.globl	_ZN2v88internal11interpreter19BytecodeArrayWriter24kMaxSizeOfPackedBytecodeE
	.section	.rodata._ZN2v88internal11interpreter19BytecodeArrayWriter24kMaxSizeOfPackedBytecodeE,"a"
	.align 8
	.type	_ZN2v88internal11interpreter19BytecodeArrayWriter24kMaxSizeOfPackedBytecodeE, @object
	.size	_ZN2v88internal11interpreter19BytecodeArrayWriter24kMaxSizeOfPackedBytecodeE, 8
_ZN2v88internal11interpreter19BytecodeArrayWriter24kMaxSizeOfPackedBytecodeE:
	.quad	22
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
