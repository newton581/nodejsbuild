	.file	"variables.cc"
	.text
	.section	.text._ZN2v88internal8VariableC2EPS1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8VariableC2EPS1_
	.type	_ZN2v88internal8VariableC2EPS1_, @function
_ZN2v88internal8VariableC2EPS1_:
.LFB19197:
	.cfi_startproc
	endbr64
	movdqu	(%rsi), %xmm1
	movzwl	40(%rsi), %eax
	pxor	%xmm0, %xmm0
	movq	32(%rsi), %rdx
	movups	%xmm0, 16(%rdi)
	movw	%ax, 40(%rdi)
	movq	%rdx, 32(%rdi)
	movups	%xmm1, (%rdi)
	ret
	.cfi_endproc
.LFE19197:
	.size	_ZN2v88internal8VariableC2EPS1_, .-_ZN2v88internal8VariableC2EPS1_
	.globl	_ZN2v88internal8VariableC1EPS1_
	.set	_ZN2v88internal8VariableC1EPS1_,_ZN2v88internal8VariableC2EPS1_
	.section	.text._ZNK2v88internal8Variable22IsGlobalObjectPropertyEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8Variable22IsGlobalObjectPropertyEv
	.type	_ZNK2v88internal8Variable22IsGlobalObjectPropertyEv, @function
_ZNK2v88internal8Variable22IsGlobalObjectPropertyEv:
.LFB19199:
	.cfi_startproc
	endbr64
	movzbl	40(%rdi), %edx
	andl	$15, %edx
	leal	-4(%rdx), %eax
	cmpb	$2, %al
	setbe	%al
	cmpb	$2, %dl
	sete	%dl
	orb	%dl, %al
	je	.L3
	movq	(%rdi), %rdx
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L3
	cmpb	$4, 128(%rdx)
	sete	%al
.L3:
	ret
	.cfi_endproc
.LFE19199:
	.size	_ZNK2v88internal8Variable22IsGlobalObjectPropertyEv, .-_ZNK2v88internal8Variable22IsGlobalObjectPropertyEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8VariableC2EPS1_,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8VariableC2EPS1_, @function
_GLOBAL__sub_I__ZN2v88internal8VariableC2EPS1_:
.LFB23426:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE23426:
	.size	_GLOBAL__sub_I__ZN2v88internal8VariableC2EPS1_, .-_GLOBAL__sub_I__ZN2v88internal8VariableC2EPS1_
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8VariableC2EPS1_
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
