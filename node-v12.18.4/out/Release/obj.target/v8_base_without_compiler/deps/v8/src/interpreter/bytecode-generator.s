	.file	"bytecode-generator.cc"
	.text
	.section	.text._ZN2v89Extension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE,"axG",@progbits,_ZN2v89Extension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v89Extension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE
	.type	_ZN2v89Extension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE, @function
_ZN2v89Extension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE:
.LFB4612:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4612:
	.size	_ZN2v89Extension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE, .-_ZN2v89Extension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator24ControlScopeForIterationD2Ev,"axG",@progbits,_ZN2v88internal11interpreter17BytecodeGenerator24ControlScopeForIterationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11interpreter17BytecodeGenerator24ControlScopeForIterationD2Ev
	.type	_ZN2v88internal11interpreter17BytecodeGenerator24ControlScopeForIterationD2Ev, @function
_ZN2v88internal11interpreter17BytecodeGenerator24ControlScopeForIterationD2Ev:
.LFB20413:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal11interpreter17BytecodeGenerator24ControlScopeForIterationE(%rip), %rax
	movq	%rax, (%rdi)
	movq	8(%rdi), %rax
	subl	$1, 868(%rax)
	movq	8(%rdi), %rax
	movq	16(%rdi), %rdx
	movq	%rdx, 800(%rax)
	ret
	.cfi_endproc
.LFE20413:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator24ControlScopeForIterationD2Ev, .-_ZN2v88internal11interpreter17BytecodeGenerator24ControlScopeForIterationD2Ev
	.weak	_ZN2v88internal11interpreter17BytecodeGenerator24ControlScopeForIterationD1Ev
	.set	_ZN2v88internal11interpreter17BytecodeGenerator24ControlScopeForIterationD1Ev,_ZN2v88internal11interpreter17BytecodeGenerator24ControlScopeForIterationD2Ev
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator25ControlScopeForTryFinallyD2Ev,"axG",@progbits,_ZN2v88internal11interpreter17BytecodeGenerator25ControlScopeForTryFinallyD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11interpreter17BytecodeGenerator25ControlScopeForTryFinallyD2Ev
	.type	_ZN2v88internal11interpreter17BytecodeGenerator25ControlScopeForTryFinallyD2Ev, @function
_ZN2v88internal11interpreter17BytecodeGenerator25ControlScopeForTryFinallyD2Ev:
.LFB23639:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	16(%rdi), %rdx
	movq	%rdx, 800(%rax)
	ret
	.cfi_endproc
.LFE23639:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator25ControlScopeForTryFinallyD2Ev, .-_ZN2v88internal11interpreter17BytecodeGenerator25ControlScopeForTryFinallyD2Ev
	.weak	_ZN2v88internal11interpreter17BytecodeGenerator25ControlScopeForTryFinallyD1Ev
	.set	_ZN2v88internal11interpreter17BytecodeGenerator25ControlScopeForTryFinallyD1Ev,_ZN2v88internal11interpreter17BytecodeGenerator25ControlScopeForTryFinallyD2Ev
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTryCatchD2Ev,"axG",@progbits,_ZN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTryCatchD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTryCatchD2Ev
	.type	_ZN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTryCatchD2Ev, @function
_ZN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTryCatchD2Ev:
.LFB23644:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	16(%rdi), %rdx
	movq	%rdx, 800(%rax)
	ret
	.cfi_endproc
.LFE23644:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTryCatchD2Ev, .-_ZN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTryCatchD2Ev
	.weak	_ZN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTryCatchD1Ev
	.set	_ZN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTryCatchD1Ev,_ZN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTryCatchD2Ev
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator24ControlScopeForBreakableD2Ev,"axG",@progbits,_ZN2v88internal11interpreter17BytecodeGenerator24ControlScopeForBreakableD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11interpreter17BytecodeGenerator24ControlScopeForBreakableD2Ev
	.type	_ZN2v88internal11interpreter17BytecodeGenerator24ControlScopeForBreakableD2Ev, @function
_ZN2v88internal11interpreter17BytecodeGenerator24ControlScopeForBreakableD2Ev:
.LFB28023:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	16(%rdi), %rdx
	movq	%rdx, 800(%rax)
	ret
	.cfi_endproc
.LFE28023:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator24ControlScopeForBreakableD2Ev, .-_ZN2v88internal11interpreter17BytecodeGenerator24ControlScopeForBreakableD2Ev
	.weak	_ZN2v88internal11interpreter17BytecodeGenerator24ControlScopeForBreakableD1Ev
	.set	_ZN2v88internal11interpreter17BytecodeGenerator24ControlScopeForBreakableD1Ev,_ZN2v88internal11interpreter17BytecodeGenerator24ControlScopeForBreakableD2Ev
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTopLevelD2Ev,"axG",@progbits,_ZN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTopLevelD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTopLevelD2Ev
	.type	_ZN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTopLevelD2Ev, @function
_ZN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTopLevelD2Ev:
.LFB28027:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	16(%rdi), %rdx
	movq	%rdx, 800(%rax)
	ret
	.cfi_endproc
.LFE28027:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTopLevelD2Ev, .-_ZN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTopLevelD2Ev
	.weak	_ZN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTopLevelD1Ev
	.set	_ZN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTopLevelD1Ev,_ZN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTopLevelD2Ev
	.section	.text._ZN2v88internal6Logger27is_listening_to_code_eventsEv,"axG",@progbits,_ZN2v88internal6Logger27is_listening_to_code_eventsEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6Logger27is_listening_to_code_eventsEv
	.type	_ZN2v88internal6Logger27is_listening_to_code_eventsEv, @function
_ZN2v88internal6Logger27is_listening_to_code_eventsEv:
.LFB19984:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	jne	.L8
	cmpq	$0, 80(%rbx)
	setne	%al
.L8:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19984:
	.size	_ZN2v88internal6Logger27is_listening_to_code_eventsEv, .-_ZN2v88internal6Logger27is_listening_to_code_eventsEv
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTopLevelD0Ev,"axG",@progbits,_ZN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTopLevelD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTopLevelD0Ev
	.type	_ZN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTopLevelD0Ev, @function
_ZN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTopLevelD0Ev:
.LFB28029:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	16(%rdi), %rdx
	movl	$32, %esi
	movq	%rdx, 800(%rax)
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE28029:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTopLevelD0Ev, .-_ZN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTopLevelD0Ev
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator24ControlScopeForBreakableD0Ev,"axG",@progbits,_ZN2v88internal11interpreter17BytecodeGenerator24ControlScopeForBreakableD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11interpreter17BytecodeGenerator24ControlScopeForBreakableD0Ev
	.type	_ZN2v88internal11interpreter17BytecodeGenerator24ControlScopeForBreakableD0Ev, @function
_ZN2v88internal11interpreter17BytecodeGenerator24ControlScopeForBreakableD0Ev:
.LFB28025:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	16(%rdi), %rdx
	movl	$48, %esi
	movq	%rdx, 800(%rax)
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE28025:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator24ControlScopeForBreakableD0Ev, .-_ZN2v88internal11interpreter17BytecodeGenerator24ControlScopeForBreakableD0Ev
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator24ControlScopeForIterationD0Ev,"axG",@progbits,_ZN2v88internal11interpreter17BytecodeGenerator24ControlScopeForIterationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11interpreter17BytecodeGenerator24ControlScopeForIterationD0Ev
	.type	_ZN2v88internal11interpreter17BytecodeGenerator24ControlScopeForIterationD0Ev, @function
_ZN2v88internal11interpreter17BytecodeGenerator24ControlScopeForIterationD0Ev:
.LFB20415:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal11interpreter17BytecodeGenerator24ControlScopeForIterationE(%rip), %rax
	movl	$48, %esi
	movq	%rax, (%rdi)
	movq	8(%rdi), %rax
	subl	$1, 868(%rax)
	movq	8(%rdi), %rax
	movq	16(%rdi), %rdx
	movq	%rdx, 800(%rax)
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE20415:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator24ControlScopeForIterationD0Ev, .-_ZN2v88internal11interpreter17BytecodeGenerator24ControlScopeForIterationD0Ev
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator25ControlScopeForTryFinallyD0Ev,"axG",@progbits,_ZN2v88internal11interpreter17BytecodeGenerator25ControlScopeForTryFinallyD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11interpreter17BytecodeGenerator25ControlScopeForTryFinallyD0Ev
	.type	_ZN2v88internal11interpreter17BytecodeGenerator25ControlScopeForTryFinallyD0Ev, @function
_ZN2v88internal11interpreter17BytecodeGenerator25ControlScopeForTryFinallyD0Ev:
.LFB23641:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	16(%rdi), %rdx
	movl	$48, %esi
	movq	%rdx, 800(%rax)
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE23641:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator25ControlScopeForTryFinallyD0Ev, .-_ZN2v88internal11interpreter17BytecodeGenerator25ControlScopeForTryFinallyD0Ev
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTryCatchD0Ev,"axG",@progbits,_ZN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTryCatchD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTryCatchD0Ev
	.type	_ZN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTryCatchD0Ev, @function
_ZN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTryCatchD0Ev:
.LFB23646:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	16(%rdi), %rdx
	movl	$32, %esi
	movq	%rdx, 800(%rax)
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE23646:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTryCatchD0Ev, .-_ZN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTryCatchD0Ev
	.section	.text._ZN2v88internal11interpreter12BlockBuilderD2Ev,"axG",@progbits,_ZN2v88internal11interpreter12BlockBuilderD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11interpreter12BlockBuilderD2Ev
	.type	_ZN2v88internal11interpreter12BlockBuilderD2Ev, @function
_ZN2v88internal11interpreter12BlockBuilderD2Ev:
.LFB28031:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal11interpreter12BlockBuilderE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN2v88internal11interpreter27BreakableControlFlowBuilderD2Ev@PLT
	.cfi_endproc
.LFE28031:
	.size	_ZN2v88internal11interpreter12BlockBuilderD2Ev, .-_ZN2v88internal11interpreter12BlockBuilderD2Ev
	.weak	_ZN2v88internal11interpreter12BlockBuilderD1Ev
	.set	_ZN2v88internal11interpreter12BlockBuilderD1Ev,_ZN2v88internal11interpreter12BlockBuilderD2Ev
	.section	.text._ZN2v88internal11interpreter12BlockBuilderD0Ev,"axG",@progbits,_ZN2v88internal11interpreter12BlockBuilderD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11interpreter12BlockBuilderD0Ev
	.type	_ZN2v88internal11interpreter12BlockBuilderD0Ev, @function
_ZN2v88internal11interpreter12BlockBuilderD0Ev:
.LFB28033:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal11interpreter12BlockBuilderE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN2v88internal11interpreter27BreakableControlFlowBuilderD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$72, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE28033:
	.size	_ZN2v88internal11interpreter12BlockBuilderD0Ev, .-_ZN2v88internal11interpreter12BlockBuilderD0Ev
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTryCatch7ExecuteENS2_12ControlScope7CommandEPNS0_9StatementEi,"axG",@progbits,_ZN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTryCatch7ExecuteENS2_12ControlScope7CommandEPNS0_9StatementEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTryCatch7ExecuteENS2_12ControlScope7CommandEPNS0_9StatementEi
	.type	_ZN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTryCatch7ExecuteENS2_12ControlScope7CommandEPNS0_9StatementEi, @function
_ZN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTryCatch7ExecuteENS2_12ControlScope7CommandEPNS0_9StatementEi:
.LFB20420:
	.cfi_startproc
	endbr64
	cmpl	$4, %esi
	je	.L27
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	8(%rdi), %rdi
	addq	$24, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder7ReThrowEv@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20420:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTryCatch7ExecuteENS2_12ControlScope7CommandEPNS0_9StatementEi, .-_ZN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTryCatch7ExecuteENS2_12ControlScope7CommandEPNS0_9StatementEi
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator24ControlScopeForIteration7ExecuteENS2_12ControlScope7CommandEPNS0_9StatementEi,"axG",@progbits,_ZN2v88internal11interpreter17BytecodeGenerator24ControlScopeForIteration7ExecuteENS2_12ControlScope7CommandEPNS0_9StatementEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11interpreter17BytecodeGenerator24ControlScopeForIteration7ExecuteENS2_12ControlScope7CommandEPNS0_9StatementEi
	.type	_ZN2v88internal11interpreter17BytecodeGenerator24ControlScopeForIteration7ExecuteENS2_12ControlScope7CommandEPNS0_9StatementEi, @function
_ZN2v88internal11interpreter17BytecodeGenerator24ControlScopeForIteration7ExecuteENS2_12ControlScope7CommandEPNS0_9StatementEi:
.LFB20416:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpq	%rdx, 32(%rdi)
	je	.L39
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	testl	%esi, %esi
	je	.L30
	cmpl	$1, %esi
	jne	.L28
	movq	8(%rdi), %rdi
	movq	24(%rbx), %rax
	cmpq	%rax, 808(%rdi)
	je	.L33
	movl	24(%rax), %esi
	addq	$24, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder10PopContextENS1_8RegisterE@PLT
.L33:
	movq	40(%rbx), %rdi
	leaq	80(%rdi), %rsi
	call	_ZN2v88internal11interpreter27BreakableControlFlowBuilder8EmitJumpEPNS1_14BytecodeLabelsE@PLT
	movl	$1, %eax
.L28:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	movq	24(%rbx), %rax
	cmpq	%rax, 808(%rdi)
	je	.L32
	movl	24(%rax), %esi
	addq	$24, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder10PopContextENS1_8RegisterE@PLT
.L32:
	movq	40(%rbx), %rdi
	leaq	16(%rdi), %rsi
	call	_ZN2v88internal11interpreter27BreakableControlFlowBuilder8EmitJumpEPNS1_14BytecodeLabelsE@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20416:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator24ControlScopeForIteration7ExecuteENS2_12ControlScope7CommandEPNS0_9StatementEi, .-_ZN2v88internal11interpreter17BytecodeGenerator24ControlScopeForIteration7ExecuteENS2_12ControlScope7CommandEPNS0_9StatementEi
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator24ControlScopeForBreakable7ExecuteENS2_12ControlScope7CommandEPNS0_9StatementEi,"axG",@progbits,_ZN2v88internal11interpreter17BytecodeGenerator24ControlScopeForBreakable7ExecuteENS2_12ControlScope7CommandEPNS0_9StatementEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11interpreter17BytecodeGenerator24ControlScopeForBreakable7ExecuteENS2_12ControlScope7CommandEPNS0_9StatementEi
	.type	_ZN2v88internal11interpreter17BytecodeGenerator24ControlScopeForBreakable7ExecuteENS2_12ControlScope7CommandEPNS0_9StatementEi, @function
_ZN2v88internal11interpreter17BytecodeGenerator24ControlScopeForBreakable7ExecuteENS2_12ControlScope7CommandEPNS0_9StatementEi:
.LFB20408:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	cmpq	%rdx, 32(%rdi)
	sete	%al
	testl	%esi, %esi
	sete	%r12b
	andb	%al, %r12b
	jne	.L47
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore_state
	movq	%rdi, %rbx
	movq	8(%rdi), %rdi
	movq	24(%rbx), %rax
	cmpq	%rax, 808(%rdi)
	je	.L42
	movl	24(%rax), %esi
	addq	$24, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder10PopContextENS1_8RegisterE@PLT
.L42:
	movq	40(%rbx), %rdi
	leaq	16(%rdi), %rsi
	call	_ZN2v88internal11interpreter27BreakableControlFlowBuilder8EmitJumpEPNS1_14BytecodeLabelsE@PLT
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20408:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator24ControlScopeForBreakable7ExecuteENS2_12ControlScope7CommandEPNS0_9StatementEi, .-_ZN2v88internal11interpreter17BytecodeGenerator24ControlScopeForBreakable7ExecuteENS2_12ControlScope7CommandEPNS0_9StatementEi
	.section	.text._ZN2v88internal11interpreter25BytecodeRegisterAllocator11NewRegisterEv,"axG",@progbits,_ZN2v88internal11interpreter25BytecodeRegisterAllocator11NewRegisterEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11interpreter25BytecodeRegisterAllocator11NewRegisterEv
	.type	_ZN2v88internal11interpreter25BytecodeRegisterAllocator11NewRegisterEv, @function
_ZN2v88internal11interpreter25BytecodeRegisterAllocator11NewRegisterEv:
.LFB10377:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movl	(%rdi), %r12d
	leal	1(%r12), %eax
	cmpl	%eax, 4(%rdi)
	movl	%eax, (%rdi)
	cmovge	4(%rdi), %eax
	movl	%eax, 4(%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L49
	movq	(%rdi), %rax
	movl	%r12d, %esi
	call	*16(%rax)
.L49:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10377:
	.size	_ZN2v88internal11interpreter25BytecodeRegisterAllocator11NewRegisterEv, .-_ZN2v88internal11interpreter25BytecodeRegisterAllocator11NewRegisterEv
	.section	.text._ZN2v88internal11interpreter25BytecodeRegisterAllocator15NewRegisterListEi,"axG",@progbits,_ZN2v88internal11interpreter25BytecodeRegisterAllocator15NewRegisterListEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11interpreter25BytecodeRegisterAllocator15NewRegisterListEi
	.type	_ZN2v88internal11interpreter25BytecodeRegisterAllocator15NewRegisterListEi, @function
_ZN2v88internal11interpreter25BytecodeRegisterAllocator15NewRegisterListEi:
.LFB10378:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	(%rdi), %r12d
	movl	%esi, %ebx
	leal	(%r12,%rsi), %eax
	cmpl	%eax, 4(%rdi)
	movl	%eax, (%rdi)
	cmovge	4(%rdi), %eax
	movl	%eax, 4(%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L55
	movq	%rbx, %rsi
	movq	(%rdi), %rax
	salq	$32, %rsi
	orq	%r12, %rsi
	call	*24(%rax)
.L55:
	salq	$32, %rbx
	movq	%r12, %rax
	orq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10378:
	.size	_ZN2v88internal11interpreter25BytecodeRegisterAllocator15NewRegisterListEi, .-_ZN2v88internal11interpreter25BytecodeRegisterAllocator15NewRegisterListEi
	.section	.rodata._ZN2v88internal11interpreter17BytecodeGenerator12ControlScope14PerformCommandENS3_7CommandEPNS0_9StatementEi.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator12ControlScope14PerformCommandENS3_7CommandEPNS0_9StatementEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator12ControlScope14PerformCommandENS3_7CommandEPNS0_9StatementEi
	.type	_ZN2v88internal11interpreter17BytecodeGenerator12ControlScope14PerformCommandENS3_7CommandEPNS0_9StatementEi, @function
_ZN2v88internal11interpreter17BytecodeGenerator12ControlScope14PerformCommandENS3_7CommandEPNS0_9StatementEi:
.LFB20438:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L67:
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L66
.L62:
	movq	(%rbx), %rax
	movl	%r12d, %ecx
	movq	%r13, %rdx
	movl	%r14d, %esi
	movq	%rbx, %rdi
	call	*16(%rax)
	testb	%al, %al
	je	.L67
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L66:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20438:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator12ControlScope14PerformCommandENS3_7CommandEPNS0_9StatementEi, .-_ZN2v88internal11interpreter17BytecodeGenerator12ControlScope14PerformCommandENS3_7CommandEPNS0_9StatementEi
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator12ControlScope25PopContextToExpectedDepthEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator12ControlScope25PopContextToExpectedDepthEv
	.type	_ZN2v88internal11interpreter17BytecodeGenerator12ControlScope25PopContextToExpectedDepthEv, @function
_ZN2v88internal11interpreter17BytecodeGenerator12ControlScope25PopContextToExpectedDepthEv:
.LFB20439:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	24(%rdi), %rdx
	cmpq	%rdx, 808(%rax)
	je	.L68
	movl	24(%rdx), %esi
	leaq	24(%rax), %rdi
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder10PopContextENS1_8RegisterE@PLT
	.p2align 4,,10
	.p2align 3
.L68:
	ret
	.cfi_endproc
.LFE20439:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator12ControlScope25PopContextToExpectedDepthEv, .-_ZN2v88internal11interpreter17BytecodeGenerator12ControlScope25PopContextToExpectedDepthEv
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator21ExpressionResultScopeD2Ev,"axG",@progbits,_ZN2v88internal11interpreter17BytecodeGenerator21ExpressionResultScopeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11interpreter17BytecodeGenerator21ExpressionResultScopeD2Ev
	.type	_ZN2v88internal11interpreter17BytecodeGenerator21ExpressionResultScopeD2Ev, @function
_ZN2v88internal11interpreter17BytecodeGenerator21ExpressionResultScopeD2Ev:
.LFB20457:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	(%rdi), %rdx
	movq	%rdx, 816(%rax)
	movq	8(%rdi), %rdx
	movl	16(%rdi), %eax
	movq	336(%rdx), %rdi
	movl	328(%rdx), %esi
	movl	%eax, 328(%rdx)
	testq	%rdi, %rdi
	je	.L70
	movq	(%rdi), %rdx
	subl	%eax, %esi
	salq	$32, %rsi
	movq	32(%rdx), %rcx
	movq	%rsi, %rdx
	movl	%eax, %esi
	orq	%rdx, %rsi
	jmp	*%rcx
	.p2align 4,,10
	.p2align 3
.L70:
	ret
	.cfi_endproc
.LFE20457:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator21ExpressionResultScopeD2Ev, .-_ZN2v88internal11interpreter17BytecodeGenerator21ExpressionResultScopeD2Ev
	.weak	_ZN2v88internal11interpreter17BytecodeGenerator21ExpressionResultScopeD1Ev
	.set	_ZN2v88internal11interpreter17BytecodeGenerator21ExpressionResultScopeD1Ev,_ZN2v88internal11interpreter17BytecodeGenerator21ExpressionResultScopeD2Ev
	.section	.text._ZN2v88internal11interpreter17BytecodeGeneratorC2EPNS0_26UnoptimizedCompilationInfoEPKNS0_18AstStringConstantsEPSt6vectorIPNS0_15FunctionLiteralESaISA_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGeneratorC2EPNS0_26UnoptimizedCompilationInfoEPKNS0_18AstStringConstantsEPSt6vectorIPNS0_15FunctionLiteralESaISA_EE
	.type	_ZN2v88internal11interpreter17BytecodeGeneratorC2EPNS0_26UnoptimizedCompilationInfoEPKNS0_18AstStringConstantsEPSt6vectorIPNS0_15FunctionLiteralESaISA_EE, @function
_ZN2v88internal11interpreter17BytecodeGeneratorC2EPNS0_26UnoptimizedCompilationInfoEPKNS0_18AstStringConstantsEPSt6vectorIPNS0_15FunctionLiteralESaISA_EE:
.LFB20715:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	24(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	8(%rsi), %rax
	movaps	%xmm0, -80(%rbp)
	movq	%rcx, -56(%rbp)
	movq	%rax, 16(%rdi)
	movq	%rsi, %rdi
	call	_ZNK2v88internal26UnoptimizedCompilationInfo27SourcePositionRecordingModeEv@PLT
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZNK2v88internal26UnoptimizedCompilationInfo5scopeEv@PLT
	movq	%r12, %rdi
	movl	120(%rax), %r15d
	call	_ZNK2v88internal26UnoptimizedCompilationInfo29num_parameters_including_thisEv@PLT
	movq	16(%rbx), %rsi
	movl	%r14d, %r9d
	movq	%r13, %rdi
	movl	%eax, %edx
	leaq	56(%r12), %r8
	movl	%r15d, %ecx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilderC1EPNS0_4ZoneEiiPNS0_18FeedbackVectorSpecENS0_26SourcePositionTableBuilder13RecordingModeE@PLT
	movdqa	-80(%rbp), %xmm0
	movq	%r12, %rdi
	movups	%xmm0, 512(%rbx)
	call	_ZNK2v88internal26UnoptimizedCompilationInfo5scopeEv@PLT
	movq	%r12, %rdi
	movq	%rax, 528(%rbx)
	call	_ZNK2v88internal26UnoptimizedCompilationInfo5scopeEv@PLT
	movq	16(%rbx), %r14
	movq	%rax, %xmm0
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, 536(%rbx)
	movq	16(%r14), %rax
	movq	24(%r14), %rdx
	subq	%rax, %rdx
	cmpq	$55, %rdx
	jbe	.L84
	leaq	56(%rax), %rdx
	movq	%rdx, 16(%r14)
.L74:
	leaq	16(%rax), %rdx
	movq	%r14, (%rax)
	movl	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	%rdx, 32(%rax)
	movq	%rdx, 40(%rax)
	movq	$0, 48(%rax)
	movq	16(%rbx), %r14
	movq	%rax, 552(%rbx)
	movq	16(%r14), %rax
	movq	24(%r14), %rdx
	subq	%rax, %rdx
	cmpq	$47, %rdx
	jbe	.L85
	leaq	48(%rax), %rdx
	movq	%rdx, 16(%r14)
.L76:
	movq	%r14, (%rax)
	movabsq	$-4294967280, %rcx
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movb	$0, 40(%rax)
	movq	16(%rbx), %r14
	movq	%rax, 560(%rbx)
	movq	$0, 568(%rbx)
	movq	%r14, 576(%rbx)
	movq	$0, 584(%rbx)
	movq	$0, 592(%rbx)
	movq	$0, 600(%rbx)
	movq	%r14, 608(%rbx)
	movq	$0, 616(%rbx)
	movq	$0, 624(%rbx)
	movq	$0, 632(%rbx)
	movq	%r14, 640(%rbx)
	movq	$0, 648(%rbx)
	movq	$0, 656(%rbx)
	movq	$0, 664(%rbx)
	movq	%r14, 672(%rbx)
	movq	$0, 680(%rbx)
	movq	$0, 688(%rbx)
	movq	$0, 696(%rbx)
	movq	%r14, 704(%rbx)
	movq	$0, 712(%rbx)
	movq	$0, 720(%rbx)
	movq	$0, 728(%rbx)
	movq	%r14, 736(%rbx)
	movq	$0, 744(%rbx)
	movq	$0, 752(%rbx)
	movq	$0, 760(%rbx)
	movq	512(%rbx), %rax
	movq	$0, 776(%rbx)
	addq	$56, %rax
	movq	$0, 784(%rbx)
	movq	$0, 792(%rbx)
	movl	$2147483647, 824(%rbx)
	movq	%rcx, 840(%rbx)
	movq	%rax, 848(%rbx)
	movq	24(%r12), %r12
	movq	%r14, 768(%rbx)
	movq	$0, 800(%rbx)
	movq	$0, 808(%rbx)
	movq	$0, 816(%rbx)
	movq	$0, 832(%rbx)
	movq	$0, 856(%rbx)
	movq	$0, 864(%rbx)
	movl	$0, 872(%rbx)
	testq	%r12, %r12
	je	.L72
	movq	16(%r14), %rax
	movq	24(%r14), %rdx
	subq	%rax, %rdx
	cmpq	$47, %rdx
	jbe	.L86
	leaq	48(%rax), %rdx
	movq	%rdx, 16(%r14)
.L79:
	movq	%r14, (%rax)
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	%r13, 32(%rax)
	movq	%r12, 40(%rax)
	movq	%rax, 568(%rbx)
.L72:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L86:
	.cfi_restore_state
	movl	$48, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L84:
	movl	$56, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L85:
	movl	$48, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L76
	.cfi_endproc
.LFE20715:
	.size	_ZN2v88internal11interpreter17BytecodeGeneratorC2EPNS0_26UnoptimizedCompilationInfoEPKNS0_18AstStringConstantsEPSt6vectorIPNS0_15FunctionLiteralESaISA_EE, .-_ZN2v88internal11interpreter17BytecodeGeneratorC2EPNS0_26UnoptimizedCompilationInfoEPKNS0_18AstStringConstantsEPSt6vectorIPNS0_15FunctionLiteralESaISA_EE
	.globl	_ZN2v88internal11interpreter17BytecodeGeneratorC1EPNS0_26UnoptimizedCompilationInfoEPKNS0_18AstStringConstantsEPSt6vectorIPNS0_15FunctionLiteralESaISA_EE
	.set	_ZN2v88internal11interpreter17BytecodeGeneratorC1EPNS0_26UnoptimizedCompilationInfoEPKNS0_18AstStringConstantsEPSt6vectorIPNS0_15FunctionLiteralESaISA_EE,_ZN2v88internal11interpreter17BytecodeGeneratorC2EPNS0_26UnoptimizedCompilationInfoEPKNS0_18AstStringConstantsEPSt6vectorIPNS0_15FunctionLiteralESaISA_EE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator27FinalizeSourcePositionTableEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator27FinalizeSourcePositionTableEPNS0_7IsolateE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator27FinalizeSourcePositionTableEPNS0_7IsolateE, @function
_ZN2v88internal11interpreter17BytecodeGenerator27FinalizeSourcePositionTableEPNS0_7IsolateE:
.LFB20720:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	addq	$24, %rdi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder21ToSourcePositionTableEPNS0_7IsolateE@PLT
	movq	41016(%rbx), %r14
	leaq	_ZN2v88internal6Logger27is_listening_to_code_eventsEv(%rip), %rdx
	movq	%rax, %r12
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	136(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L88
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	jne	.L89
	cmpq	$0, 80(%r14)
	je	.L90
.L89:
	movq	512(%r13), %rax
	movq	(%r12), %rdx
	movq	%r14, %rdi
	movq	40(%rax), %rax
	movq	(%rax), %rsi
	addq	$53, %rsi
	call	_ZN2v88internal6Logger26CodeLinePosInfoRecordEventEmNS0_9ByteArrayE@PLT
.L90:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	.cfi_restore_state
	call	*%rax
	testb	%al, %al
	jne	.L89
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20720:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator27FinalizeSourcePositionTableEPNS0_7IsolateE, .-_ZN2v88internal11interpreter17BytecodeGenerator27FinalizeSourcePositionTableEPNS0_7IsolateE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator25AllocateDeferredConstantsEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator25AllocateDeferredConstantsEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator25AllocateDeferredConstantsEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEE, @function
_ZN2v88internal11interpreter17BytecodeGenerator25AllocateDeferredConstantsEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEE:
.LFB20721:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rax
	leaq	88(%rsi), %rcx
	addq	$24, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	560(%rax), %rbx
	movq	%rdi, -144(%rbp)
	movq	592(%rdi), %rdi
	movq	%rsi, -64(%rbp)
	movq	%rdx, -112(%rbp)
	movq	%rdi, -128(%rbp)
	movq	%rbx, -56(%rbp)
	movq	%rcx, -120(%rbp)
	movq	%rax, -136(%rbp)
	cmpq	%rdi, %rbx
	je	.L121
	.p2align 4,,10
	.p2align 3
.L120:
	movq	-56(%rbp), %rax
	movq	-64(%rbp), %rdi
	movl	$1, %edx
	movl	$40, %r13d
	movq	(%rax), %r15
	movabsq	$-6148914691236517205, %rax
	movq	16(%r15), %rsi
	subq	8(%r15), %rsi
	sarq	$3, %rsi
	imulq	%rax, %rsi
	sall	$2, %esi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	8(%r15), %rbx
	movq	%rax, %r12
	movq	16(%r15), %rax
	movq	%rax, -96(%rbp)
	cmpq	%rax, %rbx
	je	.L195
	movq	%r15, -104(%rbp)
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L104:
	movq	-64(%rbp), %rdx
	movq	-112(%rbp), %rsi
	call	_ZN2v88internal8Compiler21GetSharedFunctionInfoEPNS0_15FunctionLiteralENS0_6HandleINS0_6ScriptEEEPNS0_7IsolateE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L194
.L105:
	movq	(%rbx), %rax
	movq	(%r12), %r15
	movq	(%rax), %rax
	leaq	-25(%r15,%r13), %rsi
	movq	(%rax), %rdx
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L147
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -88(%rbp)
	testl	$262144, %eax
	je	.L107
	movq	%r15, %rdi
	movq	%rdx, -80(%rbp)
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rcx
	movq	-80(%rbp), %rdx
	movq	-72(%rbp), %rsi
	movq	8(%rcx), %rax
.L107:
	testb	$24, %al
	je	.L147
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L147
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L147:
	movslq	8(%rbx), %rax
	movq	(%r12), %rdx
	salq	$32, %rax
	movq	%rax, -17(%r13,%rdx)
	movl	12(%rbx), %eax
	movq	%rax, %rdx
	salq	$32, %rdx
	cmpl	$-1, %eax
	jne	.L110
	movq	-64(%rbp), %rax
	movq	88(%rax), %rdx
.L110:
	movq	(%r12), %r15
	leaq	-9(%r15,%r13), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L146
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -88(%rbp)
	testl	$262144, %eax
	je	.L112
	movq	%r15, %rdi
	movq	%rdx, -80(%rbp)
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rcx
	movq	-80(%rbp), %rdx
	movq	-72(%rbp), %rsi
	movq	8(%rcx), %rax
.L112:
	testb	$24, %al
	je	.L146
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L146
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L146:
	movq	(%r12), %r15
	movq	(%r14), %r14
	leaq	-1(%r15,%r13), %rsi
	movq	%r14, (%rsi)
	testb	$1, %r14b
	je	.L145
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -80(%rbp)
	testl	$262144, %eax
	je	.L115
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %rcx
	movq	-72(%rbp), %rsi
	movq	8(%rcx), %rax
.L115:
	testb	$24, %al
	je	.L145
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L145
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L145:
	addq	$24, %rbx
	addq	$32, %r13
	cmpq	%rbx, -96(%rbp)
	je	.L196
.L117:
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L104
	movq	-120(%rbp), %r14
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L195:
	testq	%r12, %r12
	je	.L194
.L103:
	movq	32(%r15), %rsi
	movq	-136(%rbp), %rdi
	movq	%r12, %rdx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder28SetDeferredConstantPoolEntryEmNS0_6HandleINS0_6ObjectEEE@PLT
	addq	$8, -56(%rbp)
	movq	-56(%rbp), %rax
	cmpq	%rax, -128(%rbp)
	jne	.L120
.L121:
	movq	-144(%rbp), %rax
	movq	624(%rax), %rbx
	movq	616(%rax), %r13
	leaq	24(%rax), %r12
	cmpq	%r13, %rbx
	je	.L100
	movq	-64(%rbp), %r14
	movq	-112(%rbp), %r15
	movq	%rbx, -56(%rbp)
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L124:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	addq	$16, %r13
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder28SetDeferredConstantPoolEntryEmNS0_6HandleINS0_6ObjectEEE@PLT
	cmpq	%r13, -56(%rbp)
	je	.L100
.L125:
	movq	0(%r13), %rdi
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	8(%r13), %rbx
	call	_ZN2v88internal8Compiler21GetSharedFunctionInfoEPNS0_15FunctionLiteralENS0_6HandleINS0_6ScriptEEEPNS0_7IsolateE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	jne	.L124
	.p2align 4,,10
	.p2align 3
.L194:
	movq	-144(%rbp), %rax
	movb	$1, 8(%rax)
.L97:
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L196:
	.cfi_restore_state
	movq	-104(%rbp), %r15
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L100:
	movq	-144(%rbp), %rax
	leaq	_ZN2v89Extension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE(%rip), %r12
	movq	656(%rax), %rbx
	movq	648(%rax), %r14
	leaq	24(%rax), %r13
	cmpq	%rbx, %r14
	jne	.L130
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L192:
	movq	-64(%rbp), %rdi
	addq	$16, %r14
	call	_ZN2v88internal20FunctionTemplateInfo29GetOrCreateSharedFunctionInfoEPNS0_7IsolateENS0_6HandleIS1_EENS0_11MaybeHandleINS0_4NameEEE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder28SetDeferredConstantPoolEntryEmNS0_6HandleINS0_6ObjectEEE@PLT
	cmpq	%r14, %rbx
	je	.L123
.L130:
	movq	(%r14), %rcx
	movq	8(%r14), %r15
	xorl	%esi, %esi
	movq	16(%rcx), %rdi
	movq	8(%rcx), %rdx
	movq	(%rdi), %rax
	movq	(%rdx), %rdx
	movq	16(%rax), %rax
	cmpq	%r12, %rax
	je	.L192
	movq	%rcx, -56(%rbp)
	movq	-64(%rbp), %rsi
	call	*%rax
	movq	-56(%rbp), %rcx
	movq	%rax, %rsi
	movq	8(%rcx), %rax
	movq	(%rax), %rdx
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L123:
	movq	-144(%rbp), %rax
	movq	688(%rax), %r12
	movq	680(%rax), %rbx
	leaq	24(%rax), %r13
	cmpq	%rbx, %r12
	je	.L127
	.p2align 4,,10
	.p2align 3
.L135:
	movq	(%rbx), %r14
	movl	12(%r14), %eax
	testl	%eax, %eax
	jle	.L133
	movq	16(%r14), %rdx
	movq	8(%rbx), %r15
	testq	%rdx, %rdx
	je	.L197
.L134:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder28SetDeferredConstantPoolEntryEmNS0_6HandleINS0_6ObjectEEE@PLT
.L133:
	addq	$16, %rbx
	cmpq	%rbx, %r12
	jne	.L135
.L127:
	movq	-144(%rbp), %rax
	movq	720(%rax), %r12
	movq	712(%rax), %rbx
	leaq	24(%rax), %r13
	cmpq	%rbx, %r12
	jne	.L140
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L138:
	movq	%r15, %rsi
	movq	%r13, %rdi
	addq	$16, %rbx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder28SetDeferredConstantPoolEntryEmNS0_6HandleINS0_6ObjectEEE@PLT
	cmpq	%rbx, %r12
	je	.L132
.L140:
	movq	(%rbx), %r14
	movq	8(%rbx), %r15
	movq	16(%r14), %rdx
	testq	%rdx, %rdx
	jne	.L138
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal12ArrayLiteral27BuildBoilerplateDescriptionEPNS0_7IsolateE@PLT
	movq	16(%r14), %rdx
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L132:
	movq	-144(%rbp), %rax
	movq	-64(%rbp), %r14
	movq	752(%rax), %r12
	movq	744(%rax), %rbx
	leaq	24(%rax), %r13
	cmpq	%rbx, %r12
	je	.L137
	.p2align 4,,10
	.p2align 3
.L143:
	movq	(%rbx), %rsi
	movq	8(%rbx), %r15
	movq	%r14, %rdi
	addq	$16, %rbx
	call	_ZN2v88internal16ClassBoilerplate21BuildClassBoilerplateEPNS0_7IsolateEPNS0_12ClassLiteralE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder28SetDeferredConstantPoolEntryEmNS0_6HandleINS0_6ObjectEEE@PLT
	cmpq	%rbx, %r12
	jne	.L143
.L137:
	movq	-144(%rbp), %rax
	movq	-64(%rbp), %r14
	movq	784(%rax), %r12
	movq	776(%rax), %rbx
	leaq	24(%rax), %r13
	cmpq	%rbx, %r12
	je	.L97
	.p2align 4,,10
	.p2align 3
.L144:
	movq	(%rbx), %rdi
	movq	8(%rbx), %r15
	movq	%r14, %rsi
	addq	$16, %rbx
	call	_ZN2v88internal17GetTemplateObject21GetOrBuildDescriptionEPNS0_7IsolateE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder28SetDeferredConstantPoolEntryEmNS0_6HandleINS0_6ObjectEEE@PLT
	cmpq	%rbx, %r12
	jne	.L144
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L197:
	.cfi_restore_state
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal13ObjectLiteral27BuildBoilerplateDescriptionEPNS0_7IsolateE@PLT
	movq	16(%r14), %rdx
	jmp	.L134
	.cfi_endproc
.LFE20721:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator25AllocateDeferredConstantsEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEE, .-_ZN2v88internal11interpreter17BytecodeGenerator25AllocateDeferredConstantsEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator16FinalizeBytecodeEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator16FinalizeBytecodeEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator16FinalizeBytecodeEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEE, @function
_ZN2v88internal11interpreter17BytecodeGenerator16FinalizeBytecodeEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEE:
.LFB20717:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter17BytecodeGenerator25AllocateDeferredConstantsEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEE
	movq	568(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L200
	movq	512(%rbx), %r13
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory15NewCoverageInfoERKNS0_10ZoneVectorINS0_11SourceRangeEEE@PLT
	cmpb	$0, _ZN2v88internal25FLAG_trace_block_coverageE(%rip)
	movq	%rax, 32(%r13)
	jne	.L214
.L200:
	xorl	%eax, %eax
	cmpb	$0, 8(%rbx)
	je	.L215
.L204:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L216
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L215:
	.cfi_restore_state
	leaq	24(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder15ToBytecodeArrayEPNS0_7IsolateE@PLT
	movl	824(%rbx), %edx
	cmpl	$2147483647, %edx
	je	.L204
	movq	(%rax), %rsi
	movl	$-5, %ecx
	subl	%edx, %ecx
	movl	%ecx, 47(%rsi)
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L214:
	movq	512(%rbx), %rax
	leaq	-48(%rbp), %r13
	movq	%r13, %rdi
	movq	32(%rax), %rdx
	movq	16(%rax), %rsi
	movq	(%rdx), %rdx
	movq	%rdx, -56(%rbp)
	call	_ZNK2v88internal15FunctionLiteral12GetDebugNameEv@PLT
	leaq	-56(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal12CoverageInfo5PrintESt10unique_ptrIA_cSt14default_deleteIS3_EE@PLT
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L200
	call	_ZdaPv@PLT
	jmp	.L200
.L216:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20717:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator16FinalizeBytecodeEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEE, .-_ZN2v88internal11interpreter17BytecodeGenerator16FinalizeBytecodeEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEE
	.section	.text.unlikely._ZN2v88internal11interpreter17BytecodeGenerator25AllocateTopLevelRegistersEv,"ax",@progbits
	.align 2
.LCOLDB1:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator25AllocateTopLevelRegistersEv,"ax",@progbits
.LHOTB1:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator25AllocateTopLevelRegistersEv
	.type	_ZN2v88internal11interpreter17BytecodeGenerator25AllocateTopLevelRegistersEv, @function
_ZN2v88internal11interpreter17BytecodeGenerator25AllocateTopLevelRegistersEv:
.LFB20748:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	512(%rdi), %rax
	movq	%rdi, %rbx
	movq	16(%rax), %rdi
	call	_ZNK2v88internal15FunctionLiteral4kindEv@PLT
	leal	-9(%rax), %edx
	cmpb	$6, %dl
	jbe	.L218
	cmpb	$1, %al
	jne	.L240
.L218:
	movq	528(%rbx), %rax
	movq	216(%rax), %rax
	testq	%rax, %rax
	je	.L222
	movq	8(%rax), %rdx
.L239:
	movzwl	40(%rdx), %eax
	sarl	$7, %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L241
	movl	328(%rbx), %r12d
	movq	336(%rbx), %rdi
	leal	1(%r12), %eax
	cmpl	%eax, 332(%rbx)
	movl	%eax, 328(%rbx)
	cmovge	332(%rbx), %eax
	movl	%eax, 332(%rbx)
	testq	%rdi, %rdi
	je	.L224
	movq	(%rdi), %rax
	movl	%r12d, %esi
	call	*16(%rax)
.L224:
	movl	%r12d, 824(%rbx)
.L217:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L241:
	.cfi_restore_state
	movl	32(%rdx), %esi
	leaq	24(%rbx), %rdi
	call	_ZNK2v88internal11interpreter20BytecodeArrayBuilder5LocalEi@PLT
	movl	%eax, 824(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L240:
	.cfi_restore_state
	movq	528(%rbx), %rax
	movq	192(%rax), %rdx
	testq	%rdx, %rdx
	jne	.L239
	jmp	.L217
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal11interpreter17BytecodeGenerator25AllocateTopLevelRegistersEv
	.cfi_startproc
	.type	_ZN2v88internal11interpreter17BytecodeGenerator25AllocateTopLevelRegistersEv.cold, @function
_ZN2v88internal11interpreter17BytecodeGenerator25AllocateTopLevelRegistersEv.cold:
.LFSB20748:
.L222:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movzwl	40, %eax
	ud2
	.cfi_endproc
.LFE20748:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator25AllocateTopLevelRegistersEv
	.size	_ZN2v88internal11interpreter17BytecodeGenerator25AllocateTopLevelRegistersEv, .-_ZN2v88internal11interpreter17BytecodeGenerator25AllocateTopLevelRegistersEv
	.section	.text.unlikely._ZN2v88internal11interpreter17BytecodeGenerator25AllocateTopLevelRegistersEv
	.size	_ZN2v88internal11interpreter17BytecodeGenerator25AllocateTopLevelRegistersEv.cold, .-_ZN2v88internal11interpreter17BytecodeGenerator25AllocateTopLevelRegistersEv.cold
.LCOLDE1:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator25AllocateTopLevelRegistersEv
.LHOTE1:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator22BuildGeneratorPrologueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator22BuildGeneratorPrologueEv
	.type	_ZN2v88internal11interpreter17BytecodeGenerator22BuildGeneratorPrologueEv, @function
_ZN2v88internal11interpreter17BytecodeGenerator22BuildGeneratorPrologueEv:
.LFB20749:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	24(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	512(%rdi), %rax
	movq	%rdi, %rbx
	movq	%r12, %rdi
	movq	16(%rax), %rax
	movl	24(%rax), %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder17AllocateJumpTableEii@PLT
	movl	824(%rbx), %esi
	movq	%r12, %rdi
	movq	%rax, 856(%rbx)
	movq	%rax, %rdx
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder22SwitchOnGeneratorStateENS1_8RegisterEPNS1_17BytecodeJumpTableE@PLT
	.cfi_endproc
.LFE20749:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator22BuildGeneratorPrologueEv, .-_ZN2v88internal11interpreter17BytecodeGenerator22BuildGeneratorPrologueEv
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator19VisitEmptyStatementEPNS0_14EmptyStatementE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator19VisitEmptyStatementEPNS0_14EmptyStatementE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator19VisitEmptyStatementEPNS0_14EmptyStatementE, @function
_ZN2v88internal11interpreter17BytecodeGenerator19VisitEmptyStatementEPNS0_14EmptyStatementE:
.LFB20758:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE20758:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator19VisitEmptyStatementEPNS0_14EmptyStatementE, .-_ZN2v88internal11interpreter17BytecodeGenerator19VisitEmptyStatementEPNS0_14EmptyStatementE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator22VisitDebuggerStatementEPNS0_17DebuggerStatementE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator22VisitDebuggerStatementEPNS0_17DebuggerStatementE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator22VisitDebuggerStatementEPNS0_17DebuggerStatementE, @function
_ZN2v88internal11interpreter17BytecodeGenerator22VisitDebuggerStatementEPNS0_17DebuggerStatementE:
.LFB20785:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	leaq	24(%rdi), %r8
	cmpl	$-1, %eax
	je	.L246
	movb	$2, 496(%rdi)
	movl	%eax, 500(%rdi)
.L246:
	movq	%r8, %rdi
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder8DebuggerEv@PLT
	.cfi_endproc
.LFE20785:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator22VisitDebuggerStatementEPNS0_17DebuggerStatementE, .-_ZN2v88internal11interpreter17BytecodeGenerator22VisitDebuggerStatementEPNS0_17DebuggerStatementE
	.section	.text.unlikely._ZN2v88internal11interpreter17BytecodeGenerator26BuildInvalidPropertyAccessENS0_15MessageTemplateEPNS0_8PropertyE,"ax",@progbits
	.align 2
.LCOLDB2:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator26BuildInvalidPropertyAccessENS0_15MessageTemplateEPNS0_8PropertyE,"ax",@progbits
.LHOTB2:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator26BuildInvalidPropertyAccessENS0_15MessageTemplateEPNS0_8PropertyE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator26BuildInvalidPropertyAccessENS0_15MessageTemplateEPNS0_8PropertyE, @function
_ZN2v88internal11interpreter17BytecodeGenerator26BuildInvalidPropertyAccessENS0_15MessageTemplateEPNS0_8PropertyE:
.LFB20828:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdx), %rax
	movl	328(%rdi), %r12d
	movl	4(%rax), %edx
	movl	%edx, %ecx
	andl	$63, %ecx
	cmpb	$54, %cl
	jne	.L251
	movq	%rdi, %rbx
	movl	%esi, %r13d
	andb	$1, %dh
	jne	.L260
	movq	8(%rax), %r14
.L253:
	leal	2(%r12), %eax
	movq	336(%rbx), %rdi
	cmpl	%eax, 332(%rbx)
	movl	%r12d, %r15d
	movl	%eax, 328(%rbx)
	cmovge	332(%rbx), %eax
	movl	%eax, 332(%rbx)
	testq	%rdi, %rdi
	je	.L254
	movq	(%rdi), %rax
	movq	%r15, %rsi
	btsq	$33, %rsi
	call	*24(%rax)
.L254:
	movq	%r15, %rdx
	movq	%r13, %rsi
	leaq	24(%rbx), %rdi
	btsq	$33, %rdx
	salq	$32, %rsi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralENS0_3SmiE@PLT
	movl	%r12d, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralEPKNS0_12AstRawStringE@PLT
	leal	1(%r12), %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-56(%rbp), %rdx
	movl	$152, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_12RegisterListE@PLT
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder5ThrowEv@PLT
	movq	336(%rbx), %rdi
	movl	328(%rbx), %esi
	movl	%r12d, 328(%rbx)
	testq	%rdi, %rdi
	je	.L250
	movq	(%rdi), %rax
	subl	%r12d, %esi
	salq	$32, %rsi
	movq	32(%rax), %rax
	addq	$24, %rsp
	orq	%r15, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L260:
	.cfi_restore_state
	movq	8(%rax), %rax
	movq	8(%rax), %r14
	jmp	.L253
	.p2align 4,,10
	.p2align 3
.L250:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal11interpreter17BytecodeGenerator26BuildInvalidPropertyAccessENS0_15MessageTemplateEPNS0_8PropertyE
	.cfi_startproc
	.type	_ZN2v88internal11interpreter17BytecodeGenerator26BuildInvalidPropertyAccessENS0_15MessageTemplateEPNS0_8PropertyE.cold, @function
_ZN2v88internal11interpreter17BytecodeGenerator26BuildInvalidPropertyAccessENS0_15MessageTemplateEPNS0_8PropertyE.cold:
.LFSB20828:
.L251:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	4, %eax
	ud2
	.cfi_endproc
.LFE20828:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator26BuildInvalidPropertyAccessENS0_15MessageTemplateEPNS0_8PropertyE
	.size	_ZN2v88internal11interpreter17BytecodeGenerator26BuildInvalidPropertyAccessENS0_15MessageTemplateEPNS0_8PropertyE, .-_ZN2v88internal11interpreter17BytecodeGenerator26BuildInvalidPropertyAccessENS0_15MessageTemplateEPNS0_8PropertyE
	.section	.text.unlikely._ZN2v88internal11interpreter17BytecodeGenerator26BuildInvalidPropertyAccessENS0_15MessageTemplateEPNS0_8PropertyE
	.size	_ZN2v88internal11interpreter17BytecodeGenerator26BuildInvalidPropertyAccessENS0_15MessageTemplateEPNS0_8PropertyE.cold, .-_ZN2v88internal11interpreter17BytecodeGenerator26BuildInvalidPropertyAccessENS0_15MessageTemplateEPNS0_8PropertyE.cold
.LCOLDE2:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator26BuildInvalidPropertyAccessENS0_15MessageTemplateEPNS0_8PropertyE
.LHOTE2:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator33BuildInstanceMemberInitializationENS1_8RegisterES3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator33BuildInstanceMemberInitializationENS1_8RegisterES3_
	.type	_ZN2v88internal11interpreter17BytecodeGenerator33BuildInstanceMemberInitializationENS1_8RegisterES3_, @function
_ZN2v88internal11interpreter17BytecodeGenerator33BuildInstanceMemberInitializationENS1_8RegisterES3_:
.LFB20830:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movl	%esi, -88(%rbp)
	movl	328(%rdi), %r14d
	leal	1(%r14), %r12d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	%r12d, 332(%rdi)
	movl	%r12d, %eax
	cmovge	332(%rdi), %eax
	movl	%r12d, 328(%rdi)
	movl	%eax, 332(%rdi)
	movq	336(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L262
	movq	(%rdi), %rax
	movl	%r14d, %esi
	movl	%r14d, %r13d
	movabsq	$4294967296, %rdx
	orq	%rdx, %rsi
	call	*24(%rax)
	movl	328(%rbx), %r12d
	movl	332(%rbx), %eax
	movabsq	$4294967296, %rdx
	orq	%rdx, %r13
	movq	336(%rbx), %rdi
	leal	1(%r12), %edx
	cmpl	%edx, %eax
	movl	%edx, 328(%rbx)
	cmovl	%edx, %eax
	movl	%eax, 332(%rbx)
	testq	%rdi, %rdi
	je	.L263
	movq	(%rdi), %rax
	movl	%r12d, %esi
	call	*16(%rax)
.L263:
	movq	512(%rbx), %rax
	movl	$5, %esi
	leaq	56(%rax), %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movl	-88(%rbp), %esi
	leaq	24(%rbx), %rdi
	movb	$0, -80(%rbp)
	movl	%eax, %edx
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26LoadClassFieldsInitializerENS1_8RegisterEi@PLT
	leaq	-80(%rbp), %r9
	movq	%r9, %rsi
	movq	%rax, %rdi
	movq	%r9, -88(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder15JumpIfUndefinedEPNS1_13BytecodeLabelE@PLT
	movl	%r12d, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movl	%r14d, %edx
	movl	%r15d, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder12MoveRegisterENS1_8RegisterES3_@PLT
	movq	512(%rbx), %rdi
	movl	$4, %esi
	movq	%rax, %r14
	addq	$56, %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movq	%r13, %rdx
	movl	%r12d, %esi
	movq	%r14, %rdi
	movl	%eax, %ecx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder12CallPropertyENS1_8RegisterENS1_12RegisterListEi@PLT
	movq	-88(%rbp), %r9
	movq	%rax, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4BindEPNS1_13BytecodeLabelE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L270
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L262:
	.cfi_restore_state
	leal	2(%r14), %edx
	movl	%r14d, %r13d
	btsq	$32, %r13
	cmpl	%eax, %edx
	movl	%edx, 328(%rbx)
	cmovge	%edx, %eax
	movl	%eax, 332(%rbx)
	jmp	.L263
.L270:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20830:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator33BuildInstanceMemberInitializationENS1_8RegisterES3_, .-_ZN2v88internal11interpreter17BytecodeGenerator33BuildInstanceMemberInitializationENS1_8RegisterES3_
	.section	.rodata._ZN2v88internal11interpreter17BytecodeGenerator26VisitNativeFunctionLiteralEPNS0_21NativeFunctionLiteralE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator26VisitNativeFunctionLiteralEPNS0_21NativeFunctionLiteralE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator26VisitNativeFunctionLiteralEPNS0_21NativeFunctionLiteralE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator26VisitNativeFunctionLiteralEPNS0_21NativeFunctionLiteralE, @function
_ZN2v88internal11interpreter17BytecodeGenerator26VisitNativeFunctionLiteralEPNS0_21NativeFunctionLiteralE:
.LFB20831:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	24(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$24, %rsp
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder33AllocateDeferredConstantPoolEntryEv@PLT
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%rax, %r13
	movq	512(%rbx), %rax
	movl	88(%rax), %r15d
	leal	1(%r15), %edx
	movl	%edx, 88(%rax)
	xorl	%edx, %edx
	call	_ZN2v88internal11interpreter18CreateClosureFlags6EncodeEbbb@PLT
	movq	%r12, %rdi
	movl	%r15d, %edx
	movq	%r13, %rsi
	movzbl	%al, %ecx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder13CreateClosureEmii@PLT
	movq	656(%rbx), %r12
	cmpq	664(%rbx), %r12
	je	.L272
	movq	%r14, (%r12)
	movq	%r13, 8(%r12)
	addq	$16, 656(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L272:
	.cfi_restore_state
	movq	648(%rbx), %r8
	movq	%r12, %r15
	subq	%r8, %r15
	movq	%r15, %rax
	sarq	$4, %rax
	cmpq	$134217727, %rax
	je	.L286
	testq	%rax, %rax
	je	.L281
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L287
	movl	$2147483632, %esi
	movl	$2147483632, %r9d
.L275:
	movq	640(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L288
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L278:
	addq	%rax, %r9
	leaq	16(%rax), %rdx
.L276:
	addq	%rax, %r15
	movq	%r14, (%r15)
	movq	%r13, 8(%r15)
	cmpq	%r8, %r12
	je	.L279
	movq	%r8, %rdx
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L280:
	movq	(%rdx), %rdi
	movq	8(%rdx), %rsi
	addq	$16, %rdx
	addq	$16, %rcx
	movq	%rdi, -16(%rcx)
	movq	%rsi, -8(%rcx)
	cmpq	%rdx, %r12
	jne	.L280
	subq	%r8, %r12
	leaq	16(%rax,%r12), %rdx
.L279:
	movq	%rax, %xmm0
	movq	%rdx, %xmm1
	movq	%r9, 664(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 648(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L287:
	.cfi_restore_state
	testq	%rdx, %rdx
	jne	.L289
	movl	$16, %edx
	xorl	%r9d, %r9d
	xorl	%eax, %eax
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L281:
	movl	$16, %esi
	movl	$16, %r9d
	jmp	.L275
.L288:
	movq	%r9, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %r9
	jmp	.L278
.L286:
	leaq	.LC3(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L289:
	cmpq	$134217727, %rdx
	movl	$134217727, %r9d
	cmovbe	%rdx, %r9
	salq	$4, %r9
	movq	%r9, %rsi
	jmp	.L275
	.cfi_endproc
.LFE20831:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator26VisitNativeFunctionLiteralEPNS0_21NativeFunctionLiteralE, .-_ZN2v88internal11interpreter17BytecodeGenerator26VisitNativeFunctionLiteralEPNS0_21NativeFunctionLiteralE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator12VisitLiteralEPNS0_7LiteralE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator12VisitLiteralEPNS0_7LiteralE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator12VisitLiteralEPNS0_7LiteralE, @function
_ZN2v88internal11interpreter17BytecodeGenerator12VisitLiteralEPNS0_7LiteralE:
.LFB20847:
	.cfi_startproc
	endbr64
	movq	816(%rdi), %rax
	cmpl	$1, 24(%rax)
	je	.L303
	movl	4(%rsi), %eax
	shrl	$7, %eax
	andl	$15, %eax
	cmpl	$8, %eax
	ja	.L303
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.L293(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal11interpreter17BytecodeGenerator12VisitLiteralEPNS0_7LiteralE,"a",@progbits
	.align 4
	.align 4
.L293:
	.long	.L301-.L293
	.long	.L300-.L293
	.long	.L299-.L293
	.long	.L298-.L293
	.long	.L297-.L293
	.long	.L296-.L293
	.long	.L295-.L293
	.long	.L294-.L293
	.long	.L292-.L293
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator12VisitLiteralEPNS0_7LiteralE
	.p2align 4,,10
	.p2align 3
.L294:
	addq	$8, %rsp
	leaq	24(%rdi), %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder8LoadNullEv@PLT
	.p2align 4,,10
	.p2align 3
.L292:
	.cfi_restore_state
	addq	$8, %rsp
	leaq	24(%rdi), %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadTheHoleEv@PLT
	.p2align 4,,10
	.p2align 3
.L301:
	.cfi_restore_state
	movslq	8(%rsi), %rsi
	addq	$8, %rsp
	leaq	24(%rdi), %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	salq	$32, %rsi
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralENS0_3SmiE@PLT
	.p2align 4,,10
	.p2align 3
.L300:
	.cfi_restore_state
	movsd	8(%rsi), %xmm0
	addq	$8, %rsp
	leaq	24(%rdi), %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralEd@PLT
	.p2align 4,,10
	.p2align 3
.L299:
	.cfi_restore_state
	movq	8(%rsi), %rsi
	addq	$8, %rsp
	leaq	24(%rdi), %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralENS0_9AstBigIntE@PLT
	.p2align 4,,10
	.p2align 3
.L298:
	.cfi_restore_state
	movq	8(%rsi), %rsi
	leaq	24(%rdi), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralEPKNS0_12AstRawStringE@PLT
	movq	816(%rbx), %rax
	movl	$2, 28(%rax)
.L290:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L297:
	.cfi_restore_state
	movzbl	8(%rsi), %esi
	addq	$8, %rsp
	leaq	24(%rdi), %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralENS0_9AstSymbolE@PLT
	.p2align 4,,10
	.p2align 3
.L296:
	.cfi_restore_state
	movq	%rsi, %rdi
	call	_ZNK2v88internal7Literal15ToBooleanIsTrueEv@PLT
	leaq	24(%rbx), %rdi
	movzbl	%al, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadBooleanEb@PLT
	movq	816(%rbx), %rax
	movl	$1, 28(%rax)
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L295:
	addq	$8, %rsp
	leaq	24(%rdi), %rdi
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder13LoadUndefinedEv@PLT
.L303:
	ret
	.cfi_endproc
.LFE20847:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator12VisitLiteralEPNS0_7LiteralE, .-_ZN2v88internal11interpreter17BytecodeGenerator12VisitLiteralEPNS0_7LiteralE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator18VisitRegExpLiteralEPNS0_13RegExpLiteralE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator18VisitRegExpLiteralEPNS0_13RegExpLiteralE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator18VisitRegExpLiteralEPNS0_13RegExpLiteralE, @function
_ZN2v88internal11interpreter17BytecodeGenerator18VisitRegExpLiteralEPNS0_13RegExpLiteralE:
.LFB20848:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	512(%rdi), %rax
	movl	8(%rsi), %r13d
	movl	$19, %esi
	leaq	56(%rax), %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movq	16(%r12), %rsi
	addq	$8, %rsp
	leaq	24(%rbx), %rdi
	movl	%r13d, %ecx
	popq	%rbx
	movl	%eax, %edx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder19CreateRegExpLiteralEPKNS0_12AstRawStringEii@PLT
	.cfi_endproc
.LFE20848:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator18VisitRegExpLiteralEPNS0_13RegExpLiteralE, .-_ZN2v88internal11interpreter17BytecodeGenerator18VisitRegExpLiteralEPNS0_13RegExpLiteralE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator24BuildCreateObjectLiteralENS1_8RegisterEhm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator24BuildCreateObjectLiteralENS1_8RegisterEhm
	.type	_ZN2v88internal11interpreter17BytecodeGenerator24BuildCreateObjectLiteralENS1_8RegisterEhm, @function
_ZN2v88internal11interpreter17BytecodeGenerator24BuildCreateObjectLiteralENS1_8RegisterEhm:
.LFB20849:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, _ZN2v88internal33FLAG_enable_one_shot_optimizationE(%rip)
	movq	512(%rdi), %rdi
	je	.L309
	movl	868(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L309
	movq	16(%rdi), %rax
	movl	%edx, %r15d
	movl	28(%rax), %edx
	testl	%edx, %edx
	je	.L310
	testb	$64, 6(%rax)
	je	.L309
.L310:
	movl	328(%rbx), %edx
	movq	336(%rbx), %rdi
	leal	2(%rdx), %eax
	cmpl	%eax, 332(%rbx)
	movq	%rdx, %r12
	movl	%eax, 328(%rbx)
	cmovge	332(%rbx), %eax
	movl	%eax, 332(%rbx)
	testq	%rdi, %rdi
	je	.L311
	movq	(%rdi), %rax
	movq	%rdx, %rsi
	movq	%rdx, -56(%rbp)
	btsq	$33, %rsi
	call	*24(%rax)
	movq	-56(%rbp), %rdx
.L311:
	btsq	$33, %rdx
	leaq	24(%rbx), %rdi
	movq	%r14, %rsi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder21LoadConstantPoolEntryEm@PLT
	movl	%r12d, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movzbl	%r15b, %esi
	salq	$32, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralENS0_3SmiE@PLT
	leal	1(%r12), %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-56(%rbp), %rdx
	movl	$189, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_12RegisterListE@PLT
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L309:
	addq	$56, %rdi
	movl	$19, %esi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movzbl	%r12b, %ecx
	leaq	24(%rbx), %rdi
	movq	%r14, %rsi
	movl	%eax, %edx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder19CreateObjectLiteralEmii@PLT
.L319:
	addq	$24, %rsp
	movl	%r13d, %esi
	movq	%rax, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	.cfi_endproc
.LFE20849:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator24BuildCreateObjectLiteralENS1_8RegisterEhm, .-_ZN2v88internal11interpreter17BytecodeGenerator24BuildCreateObjectLiteralENS1_8RegisterEhm
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator11BuildReturnEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator11BuildReturnEi
	.type	_ZN2v88internal11interpreter17BytecodeGenerator11BuildReturnEi, @function
_ZN2v88internal11interpreter17BytecodeGenerator11BuildReturnEi:
.LFB20895:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	24(%rdi), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpb	$0, _ZN2v88internal10FLAG_traceE(%rip)
	jne	.L340
.L321:
	movq	512(%rbx), %rax
	movq	16(%rax), %r13
	testb	$2, (%rax)
	jne	.L341
.L324:
	cmpl	$-1, %r12d
	je	.L325
.L342:
	movb	$2, 496(%rbx)
	movl	%r12d, 500(%rbx)
.L326:
	addq	$8, %rsp
	movq	%r14, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder6ReturnEv@PLT
	.p2align 4,,10
	.p2align 3
.L340:
	.cfi_restore_state
	movl	328(%rdi), %r13d
	leal	1(%r13), %eax
	cmpl	%eax, 332(%rdi)
	movl	%eax, 328(%rdi)
	cmovge	332(%rdi), %eax
	movl	%eax, 332(%rdi)
	movq	336(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L322
	movq	(%rdi), %rax
	movl	%r13d, %esi
	call	*16(%rax)
.L322:
	movl	%r13d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movl	$431, %esi
	movl	%r13d, %edx
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_8RegisterE@PLT
	movq	336(%rbx), %rdi
	movl	328(%rbx), %esi
	movl	%r13d, 328(%rbx)
	testq	%rdi, %rdi
	je	.L321
	subl	%r13d, %esi
	movq	(%rdi), %rdx
	movq	%rsi, %rax
	movl	%r13d, %esi
	salq	$32, %rax
	orq	%rax, %rsi
	call	*32(%rdx)
	movq	512(%rbx), %rax
	movq	16(%rax), %r13
	testb	$2, (%rax)
	je	.L324
.L341:
	movq	%r13, %rdi
	call	_ZNK2v88internal15FunctionLiteral12end_positionEv@PLT
	movl	4(%r13), %edx
	movq	%r13, %rdi
	shrl	$21, %edx
	andl	$1, %edx
	subl	%edx, %eax
	movl	%eax, %r15d
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	movq	%r14, %rdi
	cmpl	%eax, %r15d
	cmovge	%r15d, %eax
	movl	%eax, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder18CollectTypeProfileEi@PLT
	movq	512(%rbx), %rax
	movq	16(%rax), %r13
	cmpl	$-1, %r12d
	jne	.L342
.L325:
	movq	%r13, %rdi
	call	_ZNK2v88internal15FunctionLiteral12end_positionEv@PLT
	movl	4(%r13), %edx
	movq	%r13, %rdi
	shrl	$21, %edx
	andl	$1, %edx
	subl	%edx, %eax
	movl	%eax, %r12d
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	cmpl	%eax, %r12d
	cmovl	%eax, %r12d
	cmpl	$-1, %r12d
	je	.L326
	movq	%r13, %rdi
	call	_ZNK2v88internal15FunctionLiteral12end_positionEv@PLT
	movl	4(%r13), %edx
	movq	%r13, %rdi
	shrl	$21, %edx
	andl	$1, %edx
	subl	%edx, %eax
	movl	%eax, %r12d
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	movb	$2, 496(%rbx)
	cmpl	%eax, %r12d
	cmovl	%eax, %r12d
	movl	%r12d, 500(%rbx)
	jmp	.L326
	.cfi_endproc
.LFE20895:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator11BuildReturnEi, .-_ZN2v88internal11interpreter17BytecodeGenerator11BuildReturnEi
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator16BuildAsyncReturnEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator16BuildAsyncReturnEi
	.type	_ZN2v88internal11interpreter17BytecodeGenerator16BuildAsyncReturnEi, @function
_ZN2v88internal11interpreter17BytecodeGenerator16BuildAsyncReturnEi:
.LFB20896:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	512(%rdi), %rax
	movl	328(%rdi), %r12d
	movq	16(%rax), %rdi
	call	_ZNK2v88internal15FunctionLiteral4kindEv@PLT
	movl	328(%rbx), %r15d
	subl	$12, %eax
	cmpb	$1, %al
	leal	3(%r15), %eax
	movq	%r15, %r13
	movl	%eax, 328(%rbx)
	jbe	.L356
	cmpl	%eax, 332(%rbx)
	movq	336(%rbx), %rdi
	cmovge	332(%rbx), %eax
	movl	%eax, 332(%rbx)
	testq	%rdi, %rdi
	je	.L347
	movabsq	$12884901888, %rsi
	movq	(%rdi), %rax
	orq	%r15, %rsi
	call	*24(%rax)
.L347:
	movl	824(%rbx), %esi
	movl	%r13d, %edx
	leaq	24(%rbx), %rdi
	movabsq	$12884901888, %rax
	orq	%rax, %r15
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder12MoveRegisterENS1_8RegisterES3_@PLT
	leal	1(%r13), %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	512(%rbx), %rax
	movq	16(%rax), %rax
	movl	24(%rax), %eax
	testl	%eax, %eax
	setg	%sil
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadBooleanEb@PLT
	leal	2(%r13), %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	%r15, %rdx
	movl	$474, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_12RegisterListE@PLT
.L346:
	movl	%r14d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator11BuildReturnEi
	movq	336(%rbx), %rdi
	movl	328(%rbx), %esi
	movl	%r12d, 328(%rbx)
	testq	%rdi, %rdi
	je	.L343
	movq	(%rdi), %rax
	subl	%r12d, %esi
	movq	32(%rax), %rdx
	movq	%rsi, %rax
	addq	$8, %rsp
	movl	%r12d, %esi
	salq	$32, %rax
	popq	%rbx
	popq	%r12
	orq	%rax, %rsi
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rdx
	.p2align 4,,10
	.p2align 3
.L356:
	.cfi_restore_state
	cmpl	%eax, 332(%rbx)
	movq	336(%rbx), %rdi
	cmovge	332(%rbx), %eax
	movl	%eax, 332(%rbx)
	testq	%rdi, %rdi
	je	.L345
	movabsq	$12884901888, %rsi
	movq	(%rdi), %rax
	orq	%r15, %rsi
	call	*24(%rax)
.L345:
	movl	824(%rbx), %esi
	movl	%r13d, %edx
	leaq	24(%rbx), %rdi
	movabsq	$12884901888, %rax
	orq	%rax, %r15
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder12MoveRegisterENS1_8RegisterES3_@PLT
	leal	1(%r13), %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder8LoadTrueEv@PLT
	leal	2(%r13), %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	%r15, %rdx
	movl	$478, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_12RegisterListE@PLT
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L343:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20896:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator16BuildAsyncReturnEi, .-_ZN2v88internal11interpreter17BytecodeGenerator16BuildAsyncReturnEi
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTopLevel7ExecuteENS2_12ControlScope7CommandEPNS0_9StatementEi,"axG",@progbits,_ZN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTopLevel7ExecuteENS2_12ControlScope7CommandEPNS0_9StatementEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTopLevel7ExecuteENS2_12ControlScope7CommandEPNS0_9StatementEi
	.type	_ZN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTopLevel7ExecuteENS2_12ControlScope7CommandEPNS0_9StatementEi, @function
_ZN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTopLevel7ExecuteENS2_12ControlScope7CommandEPNS0_9StatementEi:
.LFB20404:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpl	$3, %esi
	je	.L358
	ja	.L359
	cmpl	$2, %esi
	jne	.L366
	movq	8(%rdi), %rdi
	movl	%ecx, %esi
	call	_ZN2v88internal11interpreter17BytecodeGenerator11BuildReturnEi
	movl	$1, %eax
.L357:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L359:
	.cfi_restore_state
	xorl	%eax, %eax
	cmpl	$4, %esi
	jne	.L357
	movq	8(%rdi), %rdi
	addq	$24, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder7ReThrowEv@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L358:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	movl	%ecx, %esi
	call	_ZN2v88internal11interpreter17BytecodeGenerator16BuildAsyncReturnEi
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L366:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20404:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTopLevel7ExecuteENS2_12ControlScope7CommandEPNS0_9StatementEi, .-_ZN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTopLevel7ExecuteENS2_12ControlScope7CommandEPNS0_9StatementEi
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator12BuildReThrowEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator12BuildReThrowEv
	.type	_ZN2v88internal11interpreter17BytecodeGenerator12BuildReThrowEv, @function
_ZN2v88internal11interpreter17BytecodeGenerator12BuildReThrowEv:
.LFB20897:
	.cfi_startproc
	endbr64
	addq	$24, %rdi
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder7ReThrowEv@PLT
	.cfi_endproc
.LFE20897:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator12BuildReThrowEv, .-_ZN2v88internal11interpreter17BytecodeGenerator12BuildReThrowEv
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator16BuildThrowIfHoleEPNS0_8VariableE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator16BuildThrowIfHoleEPNS0_8VariableE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator16BuildThrowIfHoleEPNS0_8VariableE, @function
_ZN2v88internal11interpreter17BytecodeGenerator16BuildThrowIfHoleEPNS0_8VariableE:
.LFB20898:
	.cfi_startproc
	endbr64
	movzwl	40(%rsi), %eax
	addq	$24, %rdi
	sarl	$4, %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L370
	movq	8(%rsi), %rsi
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder25ThrowReferenceErrorIfHoleEPKNS0_12AstRawStringE@PLT
	.p2align 4,,10
	.p2align 3
.L370:
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder25ThrowSuperNotCalledIfHoleEv@PLT
	.cfi_endproc
.LFE20898:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator16BuildThrowIfHoleEPNS0_8VariableE, .-_ZN2v88internal11interpreter17BytecodeGenerator16BuildThrowIfHoleEPNS0_8VariableE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator35BuildHoleCheckForVariableAssignmentEPNS0_8VariableENS0_5Token5ValueE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator35BuildHoleCheckForVariableAssignmentEPNS0_8VariableENS0_5Token5ValueE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator35BuildHoleCheckForVariableAssignmentEPNS0_8VariableENS0_5Token5ValueE, @function
_ZN2v88internal11interpreter17BytecodeGenerator35BuildHoleCheckForVariableAssignmentEPNS0_8VariableENS0_5Token5ValueE:
.LFB20899:
	.cfi_startproc
	endbr64
	movzwl	40(%rsi), %ecx
	addq	$24, %rdi
	movl	%ecx, %eax
	shrb	$4, %al
	andl	$7, %eax
	cmpb	$2, %al
	jne	.L372
	andl	$15, %ecx
	cmpb	$1, %cl
	jne	.L373
	cmpb	$16, %dl
	je	.L380
.L373:
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder25ThrowSuperNotCalledIfHoleEv@PLT
	.p2align 4,,10
	.p2align 3
.L372:
	movq	8(%rsi), %rsi
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder25ThrowReferenceErrorIfHoleEPKNS0_12AstRawStringE@PLT
	.p2align 4,,10
	.p2align 3
.L380:
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder32ThrowSuperAlreadyCalledIfNotHoleEv@PLT
	.cfi_endproc
.LFE20899:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator35BuildHoleCheckForVariableAssignmentEPNS0_8VariableENS0_5Token5ValueE, .-_ZN2v88internal11interpreter17BytecodeGenerator35BuildHoleCheckForVariableAssignmentEPNS0_8VariableENS0_5Token5ValueE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator17AssignmentLhsData11NonPropertyEPNS0_10ExpressionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator17AssignmentLhsData11NonPropertyEPNS0_10ExpressionE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator17AssignmentLhsData11NonPropertyEPNS0_10ExpressionE, @function
_ZN2v88internal11interpreter17BytecodeGenerator17AssignmentLhsData11NonPropertyEPNS0_10ExpressionE:
.LFB20903:
	.cfi_startproc
	endbr64
	movdqa	.LC4(%rip), %xmm0
	movl	$0, (%rdi)
	movq	%rdi, %rax
	movq	%rsi, 8(%rdi)
	movups	%xmm0, 16(%rdi)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 32(%rdi)
	ret
	.cfi_endproc
.LFE20903:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator17AssignmentLhsData11NonPropertyEPNS0_10ExpressionE, .-_ZN2v88internal11interpreter17BytecodeGenerator17AssignmentLhsData11NonPropertyEPNS0_10ExpressionE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator17AssignmentLhsData13NamedPropertyEPNS0_10ExpressionENS1_8RegisterEPKNS0_12AstRawStringE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator17AssignmentLhsData13NamedPropertyEPNS0_10ExpressionENS1_8RegisterEPKNS0_12AstRawStringE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator17AssignmentLhsData13NamedPropertyEPNS0_10ExpressionENS1_8RegisterEPKNS0_12AstRawStringE, @function
_ZN2v88internal11interpreter17BytecodeGenerator17AssignmentLhsData13NamedPropertyEPNS0_10ExpressionENS1_8RegisterEPKNS0_12AstRawStringE:
.LFB20904:
	.cfi_startproc
	endbr64
	movq	%rsi, %xmm0
	movq	%rcx, %xmm1
	movl	%edx, 24(%rdi)
	movq	%rdi, %rax
	punpcklqdq	%xmm1, %xmm0
	movl	$1, (%rdi)
	movq	$0, 8(%rdi)
	movq	$2147483647, 16(%rdi)
	movl	$2147483647, 28(%rdi)
	movups	%xmm0, 32(%rdi)
	ret
	.cfi_endproc
.LFE20904:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator17AssignmentLhsData13NamedPropertyEPNS0_10ExpressionENS1_8RegisterEPKNS0_12AstRawStringE, .-_ZN2v88internal11interpreter17BytecodeGenerator17AssignmentLhsData13NamedPropertyEPNS0_10ExpressionENS1_8RegisterEPKNS0_12AstRawStringE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator17AssignmentLhsData13KeyedPropertyENS1_8RegisterES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator17AssignmentLhsData13KeyedPropertyENS1_8RegisterES4_
	.type	_ZN2v88internal11interpreter17BytecodeGenerator17AssignmentLhsData13KeyedPropertyENS1_8RegisterES4_, @function
_ZN2v88internal11interpreter17BytecodeGenerator17AssignmentLhsData13KeyedPropertyENS1_8RegisterES4_:
.LFB20905:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movl	%esi, 24(%rdi)
	movq	%rdi, %rax
	movl	$2, (%rdi)
	movq	$0, 8(%rdi)
	movq	$2147483647, 16(%rdi)
	movl	%edx, 28(%rdi)
	movups	%xmm0, 32(%rdi)
	ret
	.cfi_endproc
.LFE20905:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator17AssignmentLhsData13KeyedPropertyENS1_8RegisterES4_, .-_ZN2v88internal11interpreter17BytecodeGenerator17AssignmentLhsData13KeyedPropertyENS1_8RegisterES4_
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator17AssignmentLhsData18NamedSuperPropertyENS1_12RegisterListE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator17AssignmentLhsData18NamedSuperPropertyENS1_12RegisterListE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator17AssignmentLhsData18NamedSuperPropertyENS1_12RegisterListE, @function
_ZN2v88internal11interpreter17BytecodeGenerator17AssignmentLhsData18NamedSuperPropertyENS1_12RegisterListE:
.LFB20906:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movq	%rsi, 16(%rdi)
	movq	%rdi, %rax
	movabsq	$9223372034707292159, %rdx
	movl	$3, (%rdi)
	movq	$0, 8(%rdi)
	movq	%rdx, 24(%rdi)
	movups	%xmm0, 32(%rdi)
	ret
	.cfi_endproc
.LFE20906:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator17AssignmentLhsData18NamedSuperPropertyENS1_12RegisterListE, .-_ZN2v88internal11interpreter17BytecodeGenerator17AssignmentLhsData18NamedSuperPropertyENS1_12RegisterListE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator17AssignmentLhsData23PrivateMethodOrAccessorENS0_10AssignTypeEPNS0_8PropertyE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator17AssignmentLhsData23PrivateMethodOrAccessorENS0_10AssignTypeEPNS0_8PropertyE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator17AssignmentLhsData23PrivateMethodOrAccessorENS0_10AssignTypeEPNS0_8PropertyE, @function
_ZN2v88internal11interpreter17BytecodeGenerator17AssignmentLhsData23PrivateMethodOrAccessorENS0_10AssignTypeEPNS0_8PropertyE:
.LFB20907:
	.cfi_startproc
	endbr64
	movdqa	.LC4(%rip), %xmm0
	movl	%esi, (%rdi)
	movq	%rdi, %rax
	movq	%rdx, 8(%rdi)
	movups	%xmm0, 16(%rdi)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 32(%rdi)
	ret
	.cfi_endproc
.LFE20907:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator17AssignmentLhsData23PrivateMethodOrAccessorENS0_10AssignTypeEPNS0_8PropertyE, .-_ZN2v88internal11interpreter17BytecodeGenerator17AssignmentLhsData23PrivateMethodOrAccessorENS0_10AssignTypeEPNS0_8PropertyE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator17AssignmentLhsData18KeyedSuperPropertyENS1_12RegisterListE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator17AssignmentLhsData18KeyedSuperPropertyENS1_12RegisterListE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator17AssignmentLhsData18KeyedSuperPropertyENS1_12RegisterListE, @function
_ZN2v88internal11interpreter17BytecodeGenerator17AssignmentLhsData18KeyedSuperPropertyENS1_12RegisterListE:
.LFB20908:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movq	%rsi, 16(%rdi)
	movq	%rdi, %rax
	movabsq	$9223372034707292159, %rdx
	movl	$4, (%rdi)
	movq	$0, 8(%rdi)
	movq	%rdx, 24(%rdi)
	movups	%xmm0, 32(%rdi)
	ret
	.cfi_endproc
.LFE20908:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator17AssignmentLhsData18KeyedSuperPropertyENS1_12RegisterListE, .-_ZN2v88internal11interpreter17BytecodeGenerator17AssignmentLhsData18KeyedSuperPropertyENS1_12RegisterListE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator28GetDestructuringDefaultValueEPPNS0_10ExpressionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator28GetDestructuringDefaultValueEPPNS0_10ExpressionE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator28GetDestructuringDefaultValueEPPNS0_10ExpressionE, @function
_ZN2v88internal11interpreter17BytecodeGenerator28GetDestructuringDefaultValueEPPNS0_10ExpressionE:
.LFB20913:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rdx
	xorl	%r8d, %r8d
	movzbl	4(%rdx), %eax
	andl	$63, %eax
	cmpb	$24, %al
	jne	.L387
	movq	8(%rdx), %rax
	movq	16(%rdx), %r8
	movq	%rax, (%rsi)
.L387:
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE20913:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator28GetDestructuringDefaultValueEPPNS0_10ExpressionE, .-_ZN2v88internal11interpreter17BytecodeGenerator28GetDestructuringDefaultValueEPPNS0_10ExpressionE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator17BuildSuspendPointEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator17BuildSuspendPointEi
	.type	_ZN2v88internal11interpreter17BytecodeGenerator17BuildSuspendPointEi, @function
_ZN2v88internal11interpreter17BytecodeGenerator17BuildSuspendPointEi:
.LFB20921:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	24(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	864(%rdi), %r14d
	movq	%rdi, %rbx
	movl	328(%rdi), %r12d
	leal	1(%r14), %eax
	movl	%eax, 864(%rdi)
	salq	$32, %r12
	cmpl	$-1, %esi
	je	.L391
	cmpb	$2, 496(%rdi)
	je	.L391
	movb	$1, 496(%rdi)
	movl	%esi, 500(%rdi)
.L391:
	movl	824(%rbx), %esi
	movl	%r14d, %ecx
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder16SuspendGeneratorENS1_8RegisterENS1_12RegisterListEi@PLT
	movq	856(%rbx), %rsi
	movl	%r14d, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4BindEPNS1_17BytecodeJumpTableEi@PLT
	movl	824(%rbx), %esi
	movq	%r12, %rdx
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder15ResumeGeneratorENS1_8RegisterENS1_12RegisterListE@PLT
	.cfi_endproc
.LFE20921:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator17BuildSuspendPointEi, .-_ZN2v88internal11interpreter17BytecodeGenerator17BuildSuspendPointEi
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator10BuildAwaitEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator10BuildAwaitEi
	.type	_ZN2v88internal11interpreter17BytecodeGenerator10BuildAwaitEi, @function
_ZN2v88internal11interpreter17BytecodeGenerator10BuildAwaitEi:
.LFB20924:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movl	%esi, -84(%rbp)
	movl	328(%rdi), %r12d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	512(%rdi), %rax
	movq	16(%rax), %rdi
	call	_ZNK2v88internal15FunctionLiteral4kindEv@PLT
	subl	$12, %eax
	cmpl	$4, 872(%rbx)
	sete	%r14b
	cmpb	$1, %al
	jbe	.L421
	addl	$470, %r14d
.L398:
	movl	328(%rbx), %r13d
	movq	336(%rbx), %rdi
	leal	2(%r13), %eax
	cmpl	%eax, 332(%rbx)
	movq	%r13, %r15
	movl	%eax, 328(%rbx)
	cmovge	332(%rbx), %eax
	movl	%eax, 332(%rbx)
	testq	%rdi, %rdi
	je	.L399
	movq	(%rdi), %rax
	movq	%r13, %rsi
	btsq	$33, %rsi
	call	*24(%rax)
.L399:
	movl	824(%rbx), %esi
	movq	%r13, %r8
	leaq	24(%rbx), %r13
	movl	%r15d, %edx
	btsq	$33, %r8
	movq	%r13, %rdi
	movq	%r8, -96(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder12MoveRegisterENS1_8RegisterES3_@PLT
	leal	1(%r15), %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-96(%rbp), %r8
	movl	%r14d, %esi
	movq	%rax, %rdi
	movq	%r8, %rdx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_12RegisterListE@PLT
	movq	336(%rbx), %rdi
	movl	328(%rbx), %esi
	movl	%r12d, 328(%rbx)
	testq	%rdi, %rdi
	je	.L400
	subl	%r12d, %esi
	movq	(%rdi), %rax
	salq	$32, %rsi
	orq	%r12, %rsi
	call	*32(%rax)
	movl	328(%rbx), %r12d
.L400:
	movl	864(%rbx), %r15d
	salq	$32, %r12
	leal	1(%r15), %eax
	movl	%eax, 864(%rbx)
	movl	-84(%rbp), %eax
	cmpl	$-1, %eax
	je	.L401
	cmpb	$2, 496(%rbx)
	je	.L401
	movb	$1, 496(%rbx)
	movl	%eax, 500(%rbx)
.L401:
	movl	824(%rbx), %esi
	movl	%r15d, %ecx
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder16SuspendGeneratorENS1_8RegisterENS1_12RegisterListEi@PLT
	movq	856(%rbx), %rsi
	movl	%r15d, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4BindEPNS1_17BytecodeJumpTableEi@PLT
	movl	824(%rbx), %esi
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder15ResumeGeneratorENS1_8RegisterENS1_12RegisterListE@PLT
	movl	328(%rbx), %r14d
	movq	336(%rbx), %rdi
	leal	1(%r14), %r12d
	cmpl	%r12d, 332(%rbx)
	movl	%r12d, %eax
	cmovge	332(%rbx), %eax
	movl	%r12d, 328(%rbx)
	movl	%eax, 332(%rbx)
	testq	%rdi, %rdi
	je	.L402
	movq	(%rdi), %rax
	movl	%r14d, %esi
	call	*16(%rax)
	movl	328(%rbx), %r12d
	movl	332(%rbx), %edx
	movq	336(%rbx), %rdi
	leal	1(%r12), %eax
	cmpl	%edx, %eax
	movl	%eax, 328(%rbx)
	cmovl	%edx, %eax
	movl	%eax, 332(%rbx)
	testq	%rdi, %rdi
	je	.L403
	movq	(%rdi), %rax
	movl	%r12d, %esi
	call	*16(%rax)
.L403:
	movl	%r14d, %esi
	movq	%r13, %rdi
	movb	$0, -80(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movl	824(%rbx), %edx
	movl	$482, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_8RegisterE@PLT
	movl	%r12d, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralENS0_3SmiE@PLT
	movl	%r12d, %esi
	leaq	-80(%rbp), %r12
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder16CompareReferenceENS1_8RegisterE@PLT
	movq	%r12, %rdx
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder10JumpIfTrueENS2_13ToBooleanModeEPNS1_13BytecodeLabelE@PLT
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder7ReThrowEv@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4BindEPNS1_13BytecodeLabelE@PLT
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L422
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L421:
	.cfi_restore_state
	addl	$475, %r14d
	jmp	.L398
	.p2align 4,,10
	.p2align 3
.L402:
	leal	2(%r14), %edx
	cmpl	%edx, %eax
	movl	%edx, 328(%rbx)
	cmovl	%edx, %eax
	movl	%eax, 332(%rbx)
	jmp	.L403
.L422:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20924:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator10BuildAwaitEi, .-_ZN2v88internal11interpreter17BytecodeGenerator10BuildAwaitEi
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator22BuildFinalizeIterationENS2_14IteratorRecordENS1_8RegisterES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator22BuildFinalizeIterationENS2_14IteratorRecordENS1_8RegisterES4_
	.type	_ZN2v88internal11interpreter17BytecodeGenerator22BuildFinalizeIterationENS2_14IteratorRecordENS1_8RegisterES4_, @function
_ZN2v88internal11interpreter17BytecodeGenerator22BuildFinalizeIterationENS2_14IteratorRecordENS1_8RegisterES4_:
.LFB20910:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r10
	movl	%ecx, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-160(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	24(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-152(%rbp), %rbx
	subq	$264, %rsp
	movl	%r8d, -268(%rbp)
	movq	%r10, -224(%rbp)
	movl	%edx, -216(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	328(%rdi), %eax
	movq	%r14, -232(%rbp)
	movq	%rbx, -144(%rbp)
	movl	%eax, -264(%rbp)
	movq	16(%rdi), %rax
	movq	%r12, %rdi
	movq	%rbx, -152(%rbp)
	movq	%rax, -160(%rbp)
	movq	$0, -136(%rbp)
	movb	$0, -128(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter14BytecodeLabels3NewEv@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder10JumpIfTrueENS2_13ToBooleanModeEPNS1_13BytecodeLabelE@PLT
	movl	328(%r15), %esi
	movq	336(%r15), %rdi
	leal	1(%rsi), %eax
	cmpl	%eax, 332(%r15)
	movl	%esi, -236(%rbp)
	movl	%eax, 328(%r15)
	cmovge	332(%r15), %eax
	movl	%eax, 332(%r15)
	testq	%rdi, %rdi
	je	.L424
	movq	(%rdi), %rax
	call	*16(%rax)
.L424:
	movq	512(%r15), %rax
	movl	$5, %esi
	leaq	56(%rax), %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movq	%r12, %rdi
	movl	%eax, %ecx
	movq	520(%r15), %rax
	movq	424(%rax), %rdx
	movl	-220(%rbp), %eax
	movl	%eax, %esi
	movl	%eax, -240(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder17LoadNamedPropertyENS1_8RegisterEPKNS0_12AstRawStringEi@PLT
	movl	-236(%rbp), %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-232(%rbp), %rdi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter14BytecodeLabels3NewEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder21JumpIfUndefinedOrNullEPNS1_13BytecodeLabelE@PLT
	movl	328(%r15), %eax
	leaq	288(%r15), %rdi
	movq	%r12, -104(%rbp)
	movl	%eax, -260(%rbp)
	leaq	16+_ZTVN2v88internal11interpreter15TryCatchBuilderE(%rip), %rax
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal11interpreter19HandlerTableBuilder15NewHandlerEntryEv@PLT
	movl	328(%r15), %r13d
	pxor	%xmm0, %xmm0
	movq	336(%r15), %rdi
	movl	%eax, -96(%rbp)
	leal	1(%r13), %eax
	cmpl	%eax, 332(%r15)
	movl	$0, -92(%rbp)
	movl	%eax, 328(%r15)
	cmovge	332(%r15), %eax
	movb	$0, -88(%rbp)
	movq	$-1, -80(%rbp)
	movl	%eax, 332(%r15)
	movups	%xmm0, -72(%rbp)
	testq	%rdi, %rdi
	je	.L425
	movq	(%rdi), %rax
	movl	%r13d, %esi
	call	*16(%rax)
.L425:
	call	_ZN2v88internal11interpreter8Register15current_contextEv@PLT
	movl	%r13d, %edx
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder12MoveRegisterENS1_8RegisterES3_@PLT
	leaq	-112(%rbp), %rax
	movl	%r13d, %esi
	movq	%rax, %rdi
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal11interpreter15TryCatchBuilder8BeginTryENS1_8RegisterE@PLT
	movq	800(%r15), %rax
	movq	%r12, %rdi
	movl	$6, %esi
	movq	%r15, -184(%rbp)
	movq	%rax, -176(%rbp)
	movq	808(%r15), %rax
	movb	$0, -208(%rbp)
	movq	%rax, -168(%rbp)
	leaq	-192(%rbp), %rax
	movq	%rax, 800(%r15)
	movq	%rax, -248(%rbp)
	leaq	16+_ZTVN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTryCatchE(%rip), %rax
	movq	%rax, -192(%rbp)
	movq	$-1, -200(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder13CompareTypeOfENS1_15TestTypeOfFlags11LiteralFlagE@PLT
	movl	$1, %esi
	movq	%rax, %rdi
	leaq	-208(%rbp), %rax
	movq	%rax, %rdx
	movq	%rax, -288(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder10JumpIfTrueENS2_13ToBooleanModeEPNS1_13BytecodeLabelE@PLT
	movl	328(%r15), %r14d
	movq	336(%r15), %rdi
	leal	2(%r14), %eax
	cmpl	%eax, 332(%r15)
	movl	%eax, 328(%r15)
	cmovge	332(%r15), %eax
	movl	%eax, 332(%r15)
	movl	%r14d, %eax
	movq	%rax, -256(%rbp)
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L426
	movq	(%rdi), %rax
	btsq	$33, %rsi
	call	*24(%rax)
.L426:
	movq	-256(%rbp), %rdx
	movq	%r12, %rdi
	movabsq	$682899800064, %rsi
	btsq	$33, %rdx
	movq	%rdx, -296(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralENS0_3SmiE@PLT
	movl	%r14d, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	%rax, %rdi
	movq	520(%r15), %rax
	movq	272(%rax), %rsi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralEPKNS0_12AstRawStringE@PLT
	leal	1(%r14), %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-296(%rbp), %rdx
	movl	$152, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_12RegisterListE@PLT
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder5ThrowEv@PLT
	movq	336(%r15), %rdi
	movl	328(%r15), %esi
	movl	%r14d, 328(%r15)
	testq	%rdi, %rdi
	je	.L427
	movq	(%rdi), %rax
	subl	%r14d, %esi
	salq	$32, %rsi
	orq	-256(%rbp), %rsi
	call	*32(%rax)
.L427:
	movq	-288(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4BindEPNS1_13BytecodeLabelE@PLT
	movq	512(%r15), %rax
	movl	$4, %esi
	movl	-240(%rbp), %r14d
	leaq	56(%rax), %rdi
	btsq	$32, %r14
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movl	-236(%rbp), %esi
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	%eax, %ecx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder12CallPropertyENS1_8RegisterENS1_12RegisterListEi@PLT
	cmpl	$1, -224(%rbp)
	je	.L462
.L428:
	movq	-232(%rbp), %rdi
	call	_ZN2v88internal11interpreter14BytecodeLabels3NewEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder16JumpIfJSReceiverEPNS1_13BytecodeLabelE@PLT
	movl	328(%r15), %r14d
	movq	336(%r15), %rdi
	leal	1(%r14), %eax
	cmpl	%eax, 332(%r15)
	movl	%eax, 328(%r15)
	cmovge	332(%r15), %eax
	movl	%eax, 332(%r15)
	testq	%rdi, %rdi
	je	.L429
	movq	(%rdi), %rax
	movl	%r14d, %esi
	call	*16(%rax)
.L429:
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movl	$168, %esi
	movl	%r14d, %edx
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_8RegisterE@PLT
	movq	336(%r15), %rdi
	movl	328(%r15), %esi
	movl	%r14d, 328(%r15)
	testq	%rdi, %rdi
	je	.L430
	subl	%r14d, %esi
	movq	(%rdi), %rdx
	movq	%rsi, %rax
	movl	%r14d, %esi
	salq	$32, %rax
	orq	%rax, %rsi
	call	*32(%rdx)
.L430:
	movq	-176(%rbp), %rdx
	movq	-184(%rbp), %rax
	movq	-280(%rbp), %r14
	movq	%rdx, 800(%rax)
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter15TryCatchBuilder6EndTryEv@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movb	$0, -192(%rbp)
	movq	$-1, -184(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralENS0_3SmiE@PLT
	movl	-268(%rbp), %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder16CompareReferenceENS1_8RegisterE@PLT
	movq	-248(%rbp), %rdx
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder10JumpIfTrueENS2_13ToBooleanModeEPNS1_13BytecodeLabelE@PLT
	movl	%r13d, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder7ReThrowEv@PLT
	movq	-248(%rbp), %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4BindEPNS1_13BytecodeLabelE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter15TryCatchBuilder8EndCatchEv@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter15TryCatchBuilderD1Ev@PLT
	movl	-260(%rbp), %ecx
	movq	336(%r15), %rdi
	movl	328(%r15), %esi
	movl	%ecx, 328(%r15)
	testq	%rdi, %rdi
	je	.L431
	subl	%ecx, %esi
	movq	(%rdi), %rdx
	movq	%rsi, %rax
	movl	%ecx, %esi
	salq	$32, %rax
	orq	%rax, %rsi
	call	*32(%rdx)
.L431:
	movq	-232(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal11interpreter14BytecodeLabels4BindEPNS1_20BytecodeArrayBuilderE@PLT
	movq	-152(%rbp), %rax
	cmpq	%rbx, %rax
	je	.L432
	.p2align 4,,10
	.p2align 3
.L433:
	movq	(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L433
.L432:
	movl	-264(%rbp), %ebx
	movq	336(%r15), %rdi
	movl	328(%r15), %esi
	movl	%ebx, 328(%r15)
	testq	%rdi, %rdi
	je	.L423
	subl	%ebx, %esi
	movq	(%rdi), %rdx
	movq	%rsi, %rax
	movl	%ebx, %esi
	salq	$32, %rax
	orq	%rax, %rsi
	call	*32(%rdx)
.L423:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L463
	addq	$264, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L462:
	.cfi_restore_state
	movl	$-1, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator10BuildAwaitEi
	jmp	.L428
.L463:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20910:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator22BuildFinalizeIterationENS2_14IteratorRecordENS1_8RegisterES4_, .-_ZN2v88internal11interpreter17BytecodeGenerator22BuildFinalizeIterationENS2_14IteratorRecordENS1_8RegisterES4_
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator24BuildPrivateGetterAccessENS1_8RegisterES3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator24BuildPrivateGetterAccessENS1_8RegisterES3_
	.type	_ZN2v88internal11interpreter17BytecodeGenerator24BuildPrivateGetterAccessENS1_8RegisterES3_, @function
_ZN2v88internal11interpreter17BytecodeGenerator24BuildPrivateGetterAccessENS1_8RegisterES3_:
.LFB20928:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	328(%rdi), %r12d
	leal	1(%r12), %r13d
	cmpl	%r13d, 332(%rdi)
	movl	%r13d, %eax
	cmovge	332(%rdi), %eax
	movl	%r13d, 328(%rdi)
	movl	%eax, 332(%rdi)
	movq	336(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L465
	movq	(%rdi), %rax
	movl	%r12d, %esi
	call	*16(%rax)
	movl	328(%rbx), %r13d
	movl	332(%rbx), %edx
	movq	336(%rbx), %rdi
	leal	1(%r13), %eax
	cmpl	%edx, %eax
	movl	%eax, 328(%rbx)
	cmovl	%edx, %eax
	movl	%eax, 332(%rbx)
	testq	%rdi, %rdi
	je	.L472
	movq	(%rdi), %rdx
	movl	%r13d, %r8d
	movl	%r13d, %esi
	movq	%r8, -56(%rbp)
	btsq	$32, %rsi
	call	*24(%rdx)
	movq	-56(%rbp), %r8
.L466:
	btsq	$32, %r8
	movl	%r15d, %edx
	leaq	24(%rbx), %rdi
	movl	$235, %esi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_8RegisterE@PLT
	movl	%r12d, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movl	%r13d, %edx
	movl	%r14d, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder12MoveRegisterENS1_8RegisterES3_@PLT
	movl	$4, %esi
	movq	%rax, %r13
	movq	512(%rbx), %rax
	leaq	56(%rax), %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movq	-56(%rbp), %r8
	movl	%r12d, %esi
	movq	%r13, %rdi
	movl	%eax, %ecx
	movq	%r8, %rdx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder12CallPropertyENS1_8RegisterENS1_12RegisterListEi@PLT
	movq	336(%rbx), %rdi
	movl	328(%rbx), %esi
	movl	%r12d, 328(%rbx)
	testq	%rdi, %rdi
	je	.L464
	movq	(%rdi), %rax
	subl	%r12d, %esi
	salq	$32, %rsi
	movq	32(%rax), %rax
	addq	$24, %rsp
	orq	%r12, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L465:
	.cfi_restore_state
	leal	2(%r12), %edx
	cmpl	%eax, %edx
	movl	%edx, 328(%rbx)
	cmovge	%edx, %eax
	movl	%eax, 332(%rbx)
.L472:
	movl	%r13d, %r8d
	jmp	.L466
	.p2align 4,,10
	.p2align 3
.L464:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20928:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator24BuildPrivateGetterAccessENS1_8RegisterES3_, .-_ZN2v88internal11interpreter17BytecodeGenerator24BuildPrivateGetterAccessENS1_8RegisterES3_
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator24BuildPrivateSetterAccessENS1_8RegisterES3_S3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator24BuildPrivateSetterAccessENS1_8RegisterES3_S3_
	.type	_ZN2v88internal11interpreter17BytecodeGenerator24BuildPrivateSetterAccessENS1_8RegisterES3_S3_, @function
_ZN2v88internal11interpreter17BytecodeGenerator24BuildPrivateSetterAccessENS1_8RegisterES3_S3_:
.LFB20929:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	328(%rdi), %r13d
	leal	1(%r13), %r12d
	cmpl	%r12d, 332(%rdi)
	movl	%r12d, %eax
	cmovge	332(%rdi), %eax
	movl	%r12d, 328(%rdi)
	movl	%eax, 332(%rdi)
	movq	336(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L474
	movq	(%rdi), %rax
	movl	%edx, -56(%rbp)
	movl	%r13d, %esi
	call	*16(%rax)
	movl	328(%rbx), %r12d
	movl	332(%rbx), %ecx
	movq	336(%rbx), %rdi
	movl	-56(%rbp), %edx
	leal	2(%r12), %eax
	cmpl	%ecx, %eax
	movl	%eax, 328(%rbx)
	cmovl	%ecx, %eax
	testq	%rdi, %rdi
	movl	%eax, 332(%rbx)
	je	.L481
	movq	(%rdi), %rcx
	movl	%r12d, %r8d
	movl	%r12d, %esi
	movl	%edx, -60(%rbp)
	movq	%r8, -56(%rbp)
	btsq	$33, %rsi
	call	*24(%rcx)
	movl	-60(%rbp), %edx
	movq	-56(%rbp), %r8
.L475:
	btsq	$33, %r8
	leaq	24(%rbx), %rdi
	movl	$236, %esi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_8RegisterE@PLT
	movl	%r13d, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movl	%r12d, %edx
	movl	%r14d, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder12MoveRegisterENS1_8RegisterES3_@PLT
	leal	1(%r12), %edx
	movl	%r15d, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder12MoveRegisterENS1_8RegisterES3_@PLT
	movl	$4, %esi
	movq	%rax, %r12
	movq	512(%rbx), %rax
	leaq	56(%rax), %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movq	-56(%rbp), %r8
	movl	%r13d, %esi
	movq	%r12, %rdi
	movl	%eax, %ecx
	movq	%r8, %rdx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder12CallPropertyENS1_8RegisterENS1_12RegisterListEi@PLT
	movq	336(%rbx), %rdi
	movl	328(%rbx), %esi
	movl	%r13d, 328(%rbx)
	testq	%rdi, %rdi
	je	.L473
	movq	(%rdi), %rax
	subl	%r13d, %esi
	salq	$32, %rsi
	movq	32(%rax), %rax
	addq	$24, %rsp
	orq	%r13, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L474:
	.cfi_restore_state
	leal	3(%r13), %ecx
	cmpl	%eax, %ecx
	movl	%ecx, 328(%rbx)
	cmovge	%ecx, %eax
	movl	%eax, 332(%rbx)
.L481:
	movl	%r12d, %r8d
	jmp	.L475
	.p2align 4,,10
	.p2align 3
.L473:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20929:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator24BuildPrivateSetterAccessENS1_8RegisterES3_S3_, .-_ZN2v88internal11interpreter17BytecodeGenerator24BuildPrivateSetterAccessENS1_8RegisterES3_S3_
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator21VisitResolvedPropertyEPNS0_16ResolvedPropertyE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator21VisitResolvedPropertyEPNS0_16ResolvedPropertyE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator21VisitResolvedPropertyEPNS0_16ResolvedPropertyE, @function
_ZN2v88internal11interpreter17BytecodeGenerator21VisitResolvedPropertyEPNS0_16ResolvedPropertyE:
.LFB28919:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE28919:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator21VisitResolvedPropertyEPNS0_16ResolvedPropertyE, .-_ZN2v88internal11interpreter17BytecodeGenerator21VisitResolvedPropertyEPNS0_16ResolvedPropertyE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator22BuildLiteralCompareNilENS0_5Token5ValueENS1_20BytecodeArrayBuilder8NilValueE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator22BuildLiteralCompareNilENS0_5Token5ValueENS1_20BytecodeArrayBuilder8NilValueE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator22BuildLiteralCompareNilENS0_5Token5ValueENS1_20BytecodeArrayBuilder8NilValueE, @function
_ZN2v88internal11interpreter17BytecodeGenerator22BuildLiteralCompareNilENS0_5Token5ValueENS1_20BytecodeArrayBuilder8NilValueE:
.LFB20951:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	816(%rdi), %r14
	movq	%rdi, %rbx
	cmpl	$3, 24(%r14)
	jne	.L485
	movl	36(%r14), %eax
	cmpl	$1, %eax
	je	.L486
	cmpl	$2, %eax
	je	.L487
	testl	%eax, %eax
	je	.L492
	movb	$1, 32(%r14)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L485:
	.cfi_restore_state
	popq	%rbx
	movzbl	%sil, %esi
	leaq	24(%rdi), %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder10CompareNilENS0_5Token5ValueENS2_8NilValueE@PLT
	.p2align 4,,10
	.p2align 3
.L492:
	.cfi_restore_state
	movq	48(%r14), %rdi
	call	_ZN2v88internal11interpreter14BytecodeLabels3NewEv@PLT
	movzbl	%r13b, %edx
	leaq	24(%rbx), %rdi
	movl	%r12d, %ecx
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder12JumpIfNotNilEPNS1_13BytecodeLabelENS0_5Token5ValueENS2_8NilValueE@PLT
	movb	$1, 32(%r14)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L487:
	.cfi_restore_state
	movq	40(%r14), %rdi
	call	_ZN2v88internal11interpreter14BytecodeLabels3NewEv@PLT
	movzbl	%r13b, %edx
	movl	%r12d, %ecx
	leaq	24(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder9JumpIfNilEPNS1_13BytecodeLabelENS0_5Token5ValueENS2_8NilValueE@PLT
	movq	48(%r14), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal11interpreter14BytecodeLabels3NewEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4JumpEPNS1_13BytecodeLabelE@PLT
	movb	$1, 32(%r14)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L486:
	.cfi_restore_state
	movq	40(%r14), %rdi
	call	_ZN2v88internal11interpreter14BytecodeLabels3NewEv@PLT
	movzbl	%r13b, %edx
	leaq	24(%rbx), %rdi
	movl	%r12d, %ecx
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder9JumpIfNilEPNS1_13BytecodeLabelENS0_5Token5ValueENS2_8NilValueE@PLT
	movb	$1, 32(%r14)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20951:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator22BuildLiteralCompareNilENS0_5Token5ValueENS1_20BytecodeArrayBuilder8NilValueE, .-_ZN2v88internal11interpreter17BytecodeGenerator22BuildLiteralCompareNilENS0_5Token5ValueENS1_20BytecodeArrayBuilder8NilValueE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator21VisitEmptyParenthesesEPNS0_16EmptyParenthesesE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator21VisitEmptyParenthesesEPNS0_16EmptyParenthesesE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator21VisitEmptyParenthesesEPNS0_16EmptyParenthesesE, @function
_ZN2v88internal11interpreter17BytecodeGenerator21VisitEmptyParenthesesEPNS0_16EmptyParenthesesE:
.LFB20956:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20956:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator21VisitEmptyParenthesesEPNS0_16EmptyParenthesesE, .-_ZN2v88internal11interpreter17BytecodeGenerator21VisitEmptyParenthesesEPNS0_16EmptyParenthesesE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator16BuildGetIteratorENS0_12IteratorTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator16BuildGetIteratorENS0_12IteratorTypeE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator16BuildGetIteratorENS0_12IteratorTypeE, @function
_ZN2v88internal11interpreter17BytecodeGenerator16BuildGetIteratorENS0_12IteratorTypeE:
.LFB20958:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movl	328(%rdi), %r13d
	movq	336(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leal	1(%r13), %r12d
	cmpl	%r12d, 332(%rbx)
	movl	%r12d, %eax
	cmovge	332(%rbx), %eax
	movl	%r12d, 328(%rbx)
	movl	%eax, 332(%rbx)
	cmpl	$1, %esi
	je	.L518
	testq	%rdi, %rdi
	je	.L501
	movq	(%rdi), %rax
	movl	%r13d, %esi
	call	*16(%rax)
	movl	328(%rbx), %r12d
	movl	332(%rbx), %edx
	movq	336(%rbx), %rdi
	leal	1(%r12), %eax
	cmpl	%edx, %eax
	movl	%eax, 328(%rbx)
	cmovl	%edx, %eax
	movl	%eax, 332(%rbx)
	testq	%rdi, %rdi
	je	.L502
	movq	(%rdi), %rax
	movl	%r12d, %esi
	call	*16(%rax)
.L502:
	leaq	24(%rbx), %r14
	movl	%r13d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movl	$5, %esi
	movq	%rax, %r15
	movq	512(%rbx), %rax
	leaq	56(%rax), %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movq	%r15, %rdi
	movl	%r13d, %esi
	movl	%r13d, %r15d
	movl	%eax, %edx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11GetIteratorENS1_8RegisterEi@PLT
	movl	%r12d, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	512(%rbx), %rax
	movl	$4, %esi
	leaq	56(%rax), %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movl	%r13d, %edx
	movl	%r12d, %esi
	movq	%r14, %rdi
	movl	%eax, %ecx
	btsq	$32, %rdx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder12CallPropertyENS1_8RegisterENS1_12RegisterListEi@PLT
	movq	336(%rbx), %rdi
	movl	328(%rbx), %esi
	movl	%r13d, 328(%rbx)
	testq	%rdi, %rdi
	je	.L503
	subl	%r13d, %esi
	movq	(%rdi), %rax
	salq	$32, %rsi
	orq	%r15, %rsi
	call	*32(%rax)
.L503:
	leaq	-80(%rbp), %r12
	movq	%r14, %rdi
	movb	$0, -80(%rbp)
	movq	%r12, %rsi
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder16JumpIfJSReceiverEPNS1_13BytecodeLabelE@PLT
	movl	$176, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdE@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4BindEPNS1_13BytecodeLabelE@PLT
.L495:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L519
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L518:
	.cfi_restore_state
	testq	%rdi, %rdi
	je	.L497
	movq	(%rdi), %rax
	movl	%r13d, %esi
	call	*16(%rax)
	movl	328(%rbx), %r12d
	movl	332(%rbx), %edx
	movq	336(%rbx), %rdi
	leal	1(%r12), %eax
	cmpl	%edx, %eax
	movl	%eax, 328(%rbx)
	cmovl	%edx, %eax
	movl	%eax, 332(%rbx)
	testq	%rdi, %rdi
	je	.L498
	movq	(%rdi), %rax
	movl	%r12d, %esi
	call	*16(%rax)
.L498:
	leaq	24(%rbx), %r14
	movl	%r13d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movl	$5, %esi
	movq	%rax, %r15
	movq	512(%rbx), %rax
	leaq	56(%rax), %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movq	%r15, %rdi
	movl	%r13d, %esi
	movl	%r13d, %r15d
	movl	%eax, %edx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder25LoadAsyncIteratorPropertyENS1_8RegisterEi@PLT
	leaq	-96(%rbp), %r10
	movq	%r14, %rdi
	movb	$0, -96(%rbp)
	movq	%r10, %rsi
	movq	%r10, -112(%rbp)
	movq	$-1, -88(%rbp)
	movb	$0, -80(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder21JumpIfUndefinedOrNullEPNS1_13BytecodeLabelE@PLT
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movl	$4, %esi
	movq	%rax, -104(%rbp)
	movq	512(%rbx), %rax
	leaq	56(%rax), %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movq	-104(%rbp), %r9
	movl	%r13d, %edx
	movl	%r12d, %esi
	movabsq	$4294967296, %r8
	movl	%eax, %ecx
	orq	%r8, %rdx
	movq	%r9, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder12CallPropertyENS1_8RegisterENS1_12RegisterListEi@PLT
	leaq	-80(%rbp), %r9
	movq	%r14, %rdi
	movq	%r9, %rsi
	movq	%r9, -104(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder16JumpIfJSReceiverEPNS1_13BytecodeLabelE@PLT
	movl	$175, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdE@PLT
	movq	-112(%rbp), %r10
	movq	%r14, %rdi
	movq	%r10, %rsi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4BindEPNS1_13BytecodeLabelE@PLT
	movq	512(%rbx), %rax
	movl	$5, %esi
	leaq	56(%rax), %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movl	%r13d, %esi
	movq	%r14, %rdi
	movl	%eax, %edx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11GetIteratorENS1_8RegisterEi@PLT
	movl	%r12d, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	512(%rbx), %rax
	movl	$4, %esi
	leaq	56(%rax), %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movl	%r12d, %esi
	movq	%r14, %rdi
	movabsq	$4294967296, %r8
	movq	%r8, %rdx
	movl	%eax, %ecx
	orq	%r15, %rdx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder12CallPropertyENS1_8RegisterENS1_12RegisterListEi@PLT
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movl	%r12d, %edx
	movl	$483, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_8RegisterE@PLT
	movq	-104(%rbp), %r9
	movq	%r14, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4BindEPNS1_13BytecodeLabelE@PLT
	movq	336(%rbx), %rdi
	movl	328(%rbx), %esi
	movl	%r13d, 328(%rbx)
	testq	%rdi, %rdi
	je	.L495
	subl	%r13d, %esi
	movq	(%rdi), %rax
	salq	$32, %rsi
	orq	%r15, %rsi
	call	*32(%rax)
	jmp	.L495
	.p2align 4,,10
	.p2align 3
.L501:
	leal	2(%r13), %edx
	cmpl	%eax, %edx
	movl	%edx, 328(%rbx)
	cmovge	%edx, %eax
	movl	%eax, 332(%rbx)
	jmp	.L502
	.p2align 4,,10
	.p2align 3
.L497:
	leal	2(%r13), %edx
	cmpl	%eax, %edx
	movl	%edx, 328(%rbx)
	cmovge	%edx, %eax
	movl	%eax, 332(%rbx)
	jmp	.L498
.L519:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20958:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator16BuildGetIteratorENS0_12IteratorTypeE, .-_ZN2v88internal11interpreter17BytecodeGenerator16BuildGetIteratorENS0_12IteratorTypeE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator22BuildGetIteratorRecordENS1_8RegisterES3_NS0_12IteratorTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator22BuildGetIteratorRecordENS1_8RegisterES3_NS0_12IteratorTypeE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator22BuildGetIteratorRecordENS1_8RegisterES3_NS0_12IteratorTypeE, @function
_ZN2v88internal11interpreter17BytecodeGenerator22BuildGetIteratorRecordENS1_8RegisterES3_NS0_12IteratorTypeE:
.LFB20959:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	movl	%ecx, %esi
	subq	$40, %rsp
	call	_ZN2v88internal11interpreter17BytecodeGenerator16BuildGetIteratorENS0_12IteratorTypeE
	leaq	24(%r12), %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movl	$5, %esi
	movq	%rax, %r15
	movq	512(%r12), %rax
	leaq	56(%rax), %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movl	%r13d, %esi
	movq	%r15, %rdi
	movl	%eax, %ecx
	movq	520(%r12), %rax
	movq	368(%rax), %rdx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder17LoadNamedPropertyENS1_8RegisterEPKNS0_12AstRawStringEi@PLT
	movl	%ebx, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movl	%r14d, -60(%rbp)
	movl	%ebx, %edx
	movl	%r13d, -56(%rbp)
	movq	-60(%rbp), %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20959:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator22BuildGetIteratorRecordENS1_8RegisterES3_NS0_12IteratorTypeE, .-_ZN2v88internal11interpreter17BytecodeGenerator22BuildGetIteratorRecordENS1_8RegisterES3_NS0_12IteratorTypeE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator22BuildGetIteratorRecordENS0_12IteratorTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator22BuildGetIteratorRecordENS0_12IteratorTypeE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator22BuildGetIteratorRecordENS0_12IteratorTypeE, @function
_ZN2v88internal11interpreter17BytecodeGenerator22BuildGetIteratorRecordENS0_12IteratorTypeE:
.LFB20960:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movl	328(%rdi), %r13d
	leal	1(%r13), %r12d
	cmpl	%r12d, 332(%rdi)
	movl	%r12d, %eax
	cmovge	332(%rdi), %eax
	movl	%r12d, 328(%rdi)
	movl	%eax, 332(%rdi)
	movq	336(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L523
	movq	(%rdi), %rax
	movl	%r13d, %esi
	call	*16(%rax)
	movl	328(%rbx), %r12d
	movl	332(%rbx), %eax
	movq	336(%rbx), %rdi
	leal	1(%r12), %edx
	cmpl	%edx, %eax
	movl	%edx, 328(%rbx)
	cmovl	%edx, %eax
	movl	%eax, 332(%rbx)
	testq	%rdi, %rdi
	je	.L524
	movq	(%rdi), %rax
	movl	%r12d, %esi
	call	*16(%rax)
.L524:
	movl	%r14d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator16BuildGetIteratorENS0_12IteratorTypeE
	leaq	24(%rbx), %rdi
	movl	%r12d, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movl	$5, %esi
	movq	%rax, %r15
	movq	512(%rbx), %rax
	leaq	56(%rax), %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movl	%r12d, %esi
	movq	%r15, %rdi
	movl	%eax, %ecx
	movq	520(%rbx), %rax
	movq	368(%rax), %rdx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder17LoadNamedPropertyENS1_8RegisterEPKNS0_12AstRawStringEi@PLT
	movl	%r13d, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movl	%r14d, -60(%rbp)
	movl	%r13d, %edx
	movl	%r12d, -56(%rbp)
	movq	-60(%rbp), %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L523:
	.cfi_restore_state
	leal	2(%r13), %edx
	cmpl	%eax, %edx
	movl	%edx, 328(%rbx)
	cmovge	%edx, %eax
	movl	%eax, 332(%rbx)
	jmp	.L524
	.cfi_endproc
.LFE20960:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator22BuildGetIteratorRecordENS0_12IteratorTypeE, .-_ZN2v88internal11interpreter17BytecodeGenerator22BuildGetIteratorRecordENS0_12IteratorTypeE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator17BuildIteratorNextERKNS2_14IteratorRecordENS1_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator17BuildIteratorNextERKNS2_14IteratorRecordENS1_8RegisterE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator17BuildIteratorNextERKNS2_14IteratorRecordENS1_8RegisterE, @function
_ZN2v88internal11interpreter17BytecodeGenerator17BuildIteratorNextERKNS2_14IteratorRecordENS1_8RegisterE:
.LFB20961:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	24(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	movl	$4, %esi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	512(%rdi), %rax
	leaq	56(%rax), %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movl	4(%rbx), %edx
	movl	8(%rbx), %esi
	movq	%r14, %rdi
	movl	%eax, %ecx
	btsq	$32, %rdx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder12CallPropertyENS1_8RegisterENS1_12RegisterListEi@PLT
	cmpl	$1, (%rbx)
	je	.L534
.L531:
	movl	%r13d, %esi
	movq	%r14, %rdi
	leaq	-64(%rbp), %r12
	movb	$0, -64(%rbp)
	movq	$-1, -56(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder16JumpIfJSReceiverEPNS1_13BytecodeLabelE@PLT
	movl	%r13d, %edx
	movl	$168, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_8RegisterE@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4BindEPNS1_13BytecodeLabelE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L535
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L534:
	.cfi_restore_state
	movl	$-1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator10BuildAwaitEi
	jmp	.L531
.L535:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20961:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator17BuildIteratorNextERKNS2_14IteratorRecordENS1_8RegisterE, .-_ZN2v88internal11interpreter17BytecodeGenerator17BuildIteratorNextERKNS2_14IteratorRecordENS1_8RegisterE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator26BuildFillArrayWithIteratorENS2_14IteratorRecordENS1_8RegisterES4_S4_NS0_12FeedbackSlotES5_S5_S5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator26BuildFillArrayWithIteratorENS2_14IteratorRecordENS1_8RegisterES4_S4_NS0_12FeedbackSlotES5_S5_S5_
	.type	_ZN2v88internal11interpreter17BytecodeGenerator26BuildFillArrayWithIteratorENS2_14IteratorRecordENS1_8RegisterES4_S4_NS0_12FeedbackSlotES5_S5_S5_, @function
_ZN2v88internal11interpreter17BytecodeGenerator26BuildFillArrayWithIteratorENS2_14IteratorRecordENS1_8RegisterES4_S4_NS0_12FeedbackSlotES5_S5_S5_:
.LFB20872:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	24(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%r9d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%r8d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-192(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$184, %rsp
	movl	%ecx, -212(%rbp)
	leaq	16+_ZTVN2v88internal11interpreter11LoopBuilderE(%rip), %rcx
	movq	%rsi, -208(%rbp)
	movl	%edx, -200(%rbp)
	leaq	-168(%rbp), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	movq	%r12, %rdi
	movb	$0, -80(%rbp)
	movups	%xmm0, -136(%rbp)
	movq	%rax, -176(%rbp)
	movq	%rax, -112(%rbp)
	leaq	-104(%rbp), %rax
	movq	%rcx, -192(%rbp)
	movq	%rax, -96(%rbp)
	movq	%rax, -104(%rbp)
	movq	%rdx, -160(%rbp)
	movq	%rdx, -168(%rbp)
	movq	%r15, -184(%rbp)
	movq	$0, -152(%rbp)
	movb	$0, -144(%rbp)
	movq	$-1, -120(%rbp)
	movq	$0, -88(%rbp)
	call	_ZN2v88internal11interpreter11LoopBuilder10LoopHeaderEv@PLT
	movl	%r14d, %edx
	leaq	-208(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator17BuildIteratorNextERKNS2_14IteratorRecordENS1_8RegisterE
	movq	512(%rbx), %rax
	movl	$5, %esi
	leaq	56(%rax), %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movl	%r14d, %esi
	movq	%r15, %rdi
	movl	%eax, %ecx
	movq	520(%rbx), %rax
	movq	192(%rax), %rdx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder17LoadNamedPropertyENS1_8RegisterEPKNS0_12AstRawStringEi@PLT
	xorl	%esi, %esi
	leaq	-176(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter27BreakableControlFlowBuilder14EmitJumpIfTrueENS1_20BytecodeArrayBuilder13ToBooleanModeEPNS1_14BytecodeLabelsE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter11LoopBuilder8LoopBodyEv@PLT
	movl	16(%rbp), %ecx
	movl	%r14d, %esi
	movq	%r15, %rdi
	movq	520(%rbx), %rax
	movq	504(%rax), %rdx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder17LoadNamedPropertyENS1_8RegisterEPKNS0_12AstRawStringEi@PLT
	movl	-212(%rbp), %r10d
	movl	40(%rbp), %ecx
	movl	%r13d, %edx
	movq	%rax, %rdi
	movl	%r10d, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder19StoreInArrayLiteralENS1_8RegisterES3_i@PLT
	movl	%r13d, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	movl	32(%rbp), %edx
	movl	$51, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder14UnaryOperationENS0_5Token5ValueEi@PLT
	movl	%r13d, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter11LoopBuilder18BindContinueTargetEv@PLT
	movl	868(%rbx), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter11LoopBuilder12JumpToHeaderEi@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter11LoopBuilderD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L539
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L539:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20872:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator26BuildFillArrayWithIteratorENS2_14IteratorRecordENS1_8RegisterES4_S4_NS0_12FeedbackSlotES5_S5_S5_, .-_ZN2v88internal11interpreter17BytecodeGenerator26BuildFillArrayWithIteratorENS2_14IteratorRecordENS1_8RegisterES4_S4_NS0_12FeedbackSlotES5_S5_S5_
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator23BuildCallIteratorMethodENS1_8RegisterEPKNS0_12AstRawStringENS1_12RegisterListEPNS1_13BytecodeLabelEPNS1_14BytecodeLabelsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator23BuildCallIteratorMethodENS1_8RegisterEPKNS0_12AstRawStringENS1_12RegisterListEPNS1_13BytecodeLabelEPNS1_14BytecodeLabelsE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator23BuildCallIteratorMethodENS1_8RegisterEPKNS0_12AstRawStringENS1_12RegisterListEPNS1_13BytecodeLabelEPNS1_14BytecodeLabelsE, @function
_ZN2v88internal11interpreter17BytecodeGenerator23BuildCallIteratorMethodENS1_8RegisterEPKNS0_12AstRawStringENS1_12RegisterListEPNS1_13BytecodeLabelEPNS1_14BytecodeLabelsE:
.LFB20962:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r9, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	328(%rdi), %ebx
	movq	%rdx, -56(%rbp)
	movq	%r8, -64(%rbp)
	leal	1(%rbx), %eax
	cmpl	%eax, 332(%rdi)
	movl	%eax, 328(%rdi)
	cmovge	332(%rdi), %eax
	movl	%eax, 332(%rdi)
	movq	336(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L541
	movq	(%rdi), %rax
	movl	%ebx, %esi
	call	*16(%rax)
.L541:
	movq	512(%r12), %rax
	movl	$5, %esi
	leaq	56(%rax), %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movq	-56(%rbp), %rdx
	movl	%r15d, %esi
	leaq	24(%r12), %rdi
	movl	%eax, %ecx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder17LoadNamedPropertyENS1_8RegisterEPKNS0_12AstRawStringEi@PLT
	movq	%r14, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal11interpreter14BytecodeLabels3NewEv@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder21JumpIfUndefinedOrNullEPNS1_13BytecodeLabelE@PLT
	movl	%ebx, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movl	$4, %esi
	movq	%rax, %r14
	movq	512(%r12), %rax
	leaq	56(%rax), %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movq	%r13, %rdx
	movl	%ebx, %esi
	movq	%r14, %rdi
	movl	%eax, %ecx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder12CallPropertyENS1_8RegisterENS1_12RegisterListEi@PLT
	movq	-64(%rbp), %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4JumpEPNS1_13BytecodeLabelE@PLT
	movq	336(%r12), %rdi
	movl	328(%r12), %esi
	movl	%ebx, 328(%r12)
	testq	%rdi, %rdi
	je	.L540
	movq	(%rdi), %rax
	subl	%ebx, %esi
	movq	32(%rax), %rdx
	movq	%rsi, %rax
	addq	$24, %rsp
	movl	%ebx, %esi
	salq	$32, %rax
	popq	%rbx
	popq	%r12
	orq	%rax, %rsi
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rdx
	.p2align 4,,10
	.p2align 3
.L540:
	.cfi_restore_state
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20962:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator23BuildCallIteratorMethodENS1_8RegisterEPKNS0_12AstRawStringENS1_12RegisterListEPNS1_13BytecodeLabelEPNS1_14BytecodeLabelsE, .-_ZN2v88internal11interpreter17BytecodeGenerator23BuildCallIteratorMethodENS1_8RegisterEPKNS0_12AstRawStringENS1_12RegisterListEPNS1_13BytecodeLabelEPNS1_14BytecodeLabelsE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator18BuildIteratorCloseERKNS2_14IteratorRecordEPNS0_10ExpressionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator18BuildIteratorCloseERKNS2_14IteratorRecordEPNS0_10ExpressionE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator18BuildIteratorCloseERKNS2_14IteratorRecordEPNS0_10ExpressionE, @function
_ZN2v88internal11interpreter17BytecodeGenerator18BuildIteratorCloseERKNS2_14IteratorRecordEPNS0_10ExpressionE:
.LFB20963:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r14
	leaq	-112(%rbp), %r8
	pushq	%r13
	movq	%r14, %r9
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-88(%rbp), %rbx
	leaq	24(%r12), %r13
	subq	$104, %rsp
	movq	%rdx, -136(%rbp)
	movl	4(%rsi), %ecx
	movq	%rcx, %rsi
	btsq	$32, %rcx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	328(%rdi), %eax
	movq	%r8, -128(%rbp)
	movq	%rbx, -80(%rbp)
	movl	%eax, -116(%rbp)
	movq	16(%rdi), %rax
	movq	%rbx, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	520(%rdi), %rax
	movq	$0, -72(%rbp)
	movq	424(%rax), %rdx
	movb	$0, -64(%rbp)
	movb	$0, -112(%rbp)
	movq	$-1, -104(%rbp)
	call	_ZN2v88internal11interpreter17BytecodeGenerator23BuildCallIteratorMethodENS1_8RegisterEPKNS0_12AstRawStringENS1_12RegisterListEPNS1_13BytecodeLabelEPNS1_14BytecodeLabelsE
	movq	-128(%rbp), %r8
	movq	%r13, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4BindEPNS1_13BytecodeLabelE@PLT
	cmpl	$1, (%r15)
	je	.L566
.L548:
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter14BytecodeLabels3NewEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder16JumpIfJSReceiverEPNS1_13BytecodeLabelE@PLT
	movl	328(%r12), %r15d
	movq	336(%r12), %rdi
	leal	1(%r15), %eax
	cmpl	%eax, 332(%r12)
	movl	%eax, 328(%r12)
	cmovge	332(%r12), %eax
	movl	%eax, 332(%r12)
	testq	%rdi, %rdi
	je	.L549
	movq	(%rdi), %rax
	movl	%r15d, %esi
	call	*16(%rax)
.L549:
	movl	%r15d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movl	$168, %esi
	movl	%r15d, %edx
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_8RegisterE@PLT
	movq	336(%r12), %rdi
	movl	328(%r12), %esi
	movl	%r15d, 328(%r12)
	testq	%rdi, %rdi
	je	.L550
	subl	%r15d, %esi
	movq	(%rdi), %rdx
	movq	%rsi, %rax
	movl	%r15d, %esi
	salq	$32, %rax
	orq	%rax, %rsi
	call	*32(%rdx)
.L550:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter14BytecodeLabels4BindEPNS1_20BytecodeArrayBuilderE@PLT
	movq	-88(%rbp), %rax
	cmpq	%rbx, %rax
	je	.L551
	.p2align 4,,10
	.p2align 3
.L552:
	movq	(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L552
.L551:
	movl	-116(%rbp), %ecx
	movq	336(%r12), %rdi
	movl	328(%r12), %esi
	movl	%ecx, 328(%r12)
	testq	%rdi, %rdi
	je	.L547
	subl	%ecx, %esi
	movq	(%rdi), %rdx
	movq	%rsi, %rax
	movl	%ecx, %esi
	salq	$32, %rax
	orq	%rax, %rsi
	call	*32(%rdx)
.L547:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L567
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L566:
	.cfi_restore_state
	movq	-136(%rbp), %rax
	movq	%r12, %rdi
	movl	(%rax), %esi
	call	_ZN2v88internal11interpreter17BytecodeGenerator10BuildAwaitEi
	jmp	.L548
.L567:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20963:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator18BuildIteratorCloseERKNS2_14IteratorRecordEPNS0_10ExpressionE, .-_ZN2v88internal11interpreter17BytecodeGenerator18BuildIteratorCloseERKNS2_14IteratorRecordEPNS0_10ExpressionE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator22VisitGetTemplateObjectEPNS0_17GetTemplateObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator22VisitGetTemplateObjectEPNS0_17GetTemplateObjectE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator22VisitGetTemplateObjectEPNS0_17GetTemplateObjectE, @function
_ZN2v88internal11interpreter17BytecodeGenerator22VisitGetTemplateObjectEPNS0_17GetTemplateObjectE:
.LFB20964:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	24(%rdi), %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movl	(%rsi), %eax
	cmpl	$-1, %eax
	je	.L569
	cmpb	$2, 496(%rdi)
	je	.L569
	movb	$1, 496(%rdi)
	movl	%eax, 500(%rdi)
.L569:
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder33AllocateDeferredConstantPoolEntryEv@PLT
	movq	784(%rbx), %r12
	movq	%rax, %r14
	cmpq	792(%rbx), %r12
	je	.L570
	movq	%r13, (%r12)
	movq	%rax, 8(%r12)
	addq	$16, 784(%rbx)
.L571:
	movq	512(%rbx), %rdi
	movl	$19, %esi
	addq	$56, %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	addq	$40, %rsp
	movq	%r14, %rsi
	movq	%r15, %rdi
	popq	%rbx
	movl	%eax, %edx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder17GetTemplateObjectEmi@PLT
	.p2align 4,,10
	.p2align 3
.L570:
	.cfi_restore_state
	movq	776(%rbx), %r8
	movq	%r12, %rdx
	subq	%r8, %rdx
	movq	%rdx, %rax
	sarq	$4, %rax
	cmpq	$134217727, %rax
	je	.L587
	testq	%rax, %rax
	je	.L579
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L588
	movl	$2147483632, %esi
	movl	$2147483632, %r9d
.L573:
	movq	768(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	%rcx, %rsi
	ja	.L589
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L576:
	addq	%rax, %r9
	leaq	16(%rax), %rcx
.L574:
	addq	%rax, %rdx
	movq	%r13, (%rdx)
	movq	%r14, 8(%rdx)
	cmpq	%r8, %r12
	je	.L577
	movq	%r8, %rdx
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L578:
	movq	(%rdx), %rdi
	movq	8(%rdx), %rsi
	addq	$16, %rdx
	addq	$16, %rcx
	movq	%rdi, -16(%rcx)
	movq	%rsi, -8(%rcx)
	cmpq	%rdx, %r12
	jne	.L578
	subq	%r8, %r12
	leaq	16(%rax,%r12), %rcx
.L577:
	movq	%rax, %xmm0
	movq	%rcx, %xmm1
	movq	%r9, 792(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 776(%rbx)
	jmp	.L571
	.p2align 4,,10
	.p2align 3
.L588:
	testq	%rcx, %rcx
	jne	.L590
	movl	$16, %ecx
	xorl	%r9d, %r9d
	xorl	%eax, %eax
	jmp	.L574
	.p2align 4,,10
	.p2align 3
.L579:
	movl	$16, %esi
	movl	$16, %r9d
	jmp	.L573
.L589:
	movq	%rdx, -72(%rbp)
	movq	%r8, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %r8
	movq	-72(%rbp), %rdx
	jmp	.L576
.L587:
	leaq	.LC3(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L590:
	cmpq	$134217727, %rcx
	movl	$134217727, %r9d
	cmovbe	%rcx, %r9
	salq	$4, %r9
	movq	%r9, %rsi
	jmp	.L573
	.cfi_endproc
.LFE20964:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator22VisitGetTemplateObjectEPNS0_17GetTemplateObjectE, .-_ZN2v88internal11interpreter17BytecodeGenerator22VisitGetTemplateObjectEPNS0_17GetTemplateObjectE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator23VisitSuperCallReferenceEPNS0_18SuperCallReferenceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator23VisitSuperCallReferenceEPNS0_18SuperCallReferenceE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator23VisitSuperCallReferenceEPNS0_18SuperCallReferenceE, @function
_ZN2v88internal11interpreter17BytecodeGenerator23VisitSuperCallReferenceEPNS0_18SuperCallReferenceE:
.LFB28921:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE28921:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator23VisitSuperCallReferenceEPNS0_18SuperCallReferenceE, .-_ZN2v88internal11interpreter17BytecodeGenerator23VisitSuperCallReferenceEPNS0_18SuperCallReferenceE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator27VisitSuperPropertyReferenceEPNS0_22SuperPropertyReferenceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator27VisitSuperPropertyReferenceEPNS0_22SuperPropertyReferenceE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator27VisitSuperPropertyReferenceEPNS0_22SuperPropertyReferenceE, @function
_ZN2v88internal11interpreter17BytecodeGenerator27VisitSuperPropertyReferenceEPNS0_22SuperPropertyReferenceE:
.LFB20982:
	.cfi_startproc
	endbr64
	addq	$24, %rdi
	movl	$45, %esi
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdE@PLT
	.cfi_endproc
.LFE20982:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator27VisitSuperPropertyReferenceEPNS0_22SuperPropertyReferenceE, .-_ZN2v88internal11interpreter17BytecodeGenerator27VisitSuperPropertyReferenceEPNS0_22SuperPropertyReferenceE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator30BuildNewLocalActivationContextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator30BuildNewLocalActivationContextEv
	.type	_ZN2v88internal11interpreter17BytecodeGenerator30BuildNewLocalActivationContextEv, @function
_ZN2v88internal11interpreter17BytecodeGenerator30BuildNewLocalActivationContextEv:
.LFB21000:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$48, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	528(%rdi), %r13
	movl	328(%rdi), %r12d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	816(%rdi), %rax
	movq	%rdi, -72(%rbp)
	movl	%r12d, -64(%rbp)
	movq	%rax, -80(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rax, 816(%rdi)
	movzbl	128(%r13), %edx
	movq	$2, -56(%rbp)
	cmpb	$4, %dl
	je	.L623
	cmpb	$3, %dl
	je	.L624
	movl	124(%r13), %eax
	cmpb	$1, _ZN2v88internal46FLAG_test_small_max_function_context_stub_sizeE(%rip)
	leal	-4(%rax), %r8d
	sbbl	%eax, %eax
	andl	$16367, %eax
	addl	$10, %eax
	cmpl	%eax, %r8d
	jg	.L601
	cmpb	$1, %dl
	jne	.L625
	leaq	24(%rdi), %rdi
	movl	%r8d, %edx
	movq	%r13, %rsi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder17CreateEvalContextEPKNS0_5ScopeEi@PLT
.L597:
	movq	-72(%rbp), %rdx
	movq	-80(%rbp), %rax
	movq	336(%rdx), %rdi
	movq	%rax, 816(%rdx)
	movl	-64(%rbp), %eax
	movl	328(%rdx), %esi
	movl	%eax, 328(%rdx)
	testq	%rdi, %rdi
	je	.L594
	subl	%eax, %esi
	movq	(%rdi), %rcx
	salq	$32, %rsi
	movq	%rsi, %rdx
	movl	%eax, %esi
	orq	%rdx, %rsi
	call	*32(%rcx)
.L594:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L626
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L601:
	.cfi_restore_state
	leal	1(%r12), %eax
	cmpl	%eax, 332(%rdi)
	movl	%eax, 328(%rdi)
	cmovge	332(%rdi), %eax
	movl	%eax, 332(%rdi)
	movq	336(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L605
	movq	(%rdi), %rax
	movl	%r12d, %esi
	call	*16(%rax)
.L605:
	leaq	24(%r15), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralEPKNS0_5ScopeE@PLT
	movl	%r12d, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movl	%r12d, %edx
	movl	$308, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_8RegisterE@PLT
	jmp	.L597
	.p2align 4,,10
	.p2align 3
.L625:
	cmpb	$2, %dl
	jne	.L627
	leaq	24(%rdi), %rdi
	movl	%r8d, %edx
	movq	%r13, %rsi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder21CreateFunctionContextEPKNS0_5ScopeEi@PLT
	jmp	.L597
	.p2align 4,,10
	.p2align 3
.L623:
	leal	1(%r12), %eax
	cmpl	%eax, 332(%rdi)
	movl	%eax, 328(%rdi)
	cmovge	332(%rdi), %eax
	movl	%eax, 332(%rdi)
	movq	336(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L596
	movq	(%rdi), %rax
	movl	%r12d, %esi
	call	*16(%rax)
.L596:
	leaq	24(%r15), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralEPKNS0_5ScopeE@PLT
	movl	%r12d, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movl	%r12d, %edx
	movl	$310, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_8RegisterE@PLT
	jmp	.L597
	.p2align 4,,10
	.p2align 3
.L624:
	leal	2(%r12), %eax
	cmpl	%eax, 332(%rdi)
	movl	%r12d, %r14d
	movl	%eax, 328(%rdi)
	cmovge	332(%rdi), %eax
	movl	%eax, 332(%rdi)
	movq	336(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L599
	movq	(%rdi), %rax
	movq	%r14, %rsi
	btsq	$33, %rsi
	call	*24(%rax)
.L599:
	addq	$24, %r15
	xorl	%esi, %esi
	btsq	$33, %r14
	movq	%r15, %rdi
	call	_ZNK2v88internal11interpreter20BytecodeArrayBuilder9ParameterEi@PLT
	movl	%r12d, %edx
	movq	%r15, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder12MoveRegisterENS1_8RegisterES3_@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralEPKNS0_5ScopeE@PLT
	leal	1(%r12), %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	%r14, %rdx
	movl	$316, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_12RegisterListE@PLT
	jmp	.L597
.L626:
	call	__stack_chk_fail@PLT
.L627:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21000:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator30BuildNewLocalActivationContextEv, .-_ZN2v88internal11interpreter17BytecodeGenerator30BuildNewLocalActivationContextEv
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator41BuildLocalActivationContextInitializationEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator41BuildLocalActivationContextInitializationEv
	.type	_ZN2v88internal11interpreter17BytecodeGenerator41BuildLocalActivationContextInitializationEv, @function
_ZN2v88internal11interpreter17BytecodeGenerator41BuildLocalActivationContextInitializationEv:
.LFB21001:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	528(%rdi), %r15
	testb	$16, 132(%r15)
	je	.L629
	movq	176(%r15), %r12
	movzwl	40(%r12), %eax
	sarl	$7, %eax
	andl	$7, %eax
	cmpb	$3, %al
	je	.L639
.L629:
	movl	136(%r15), %eax
	testl	%eax, %eax
	jle	.L628
	leal	-1(%rax), %r8d
	xorl	%r13d, %r13d
	leaq	24(%rbx), %r12
	jmp	.L633
	.p2align 4,,10
	.p2align 3
.L634:
	movq	%rax, %r13
.L633:
	movq	144(%r15), %rax
	movq	(%rax,%r13,8), %r14
	movzwl	40(%r14), %eax
	sarl	$7, %eax
	andl	$7, %eax
	cmpb	$3, %al
	jne	.L632
	movl	%r13d, %esi
	movq	%r12, %rdi
	movq	%r8, -56(%rbp)
	call	_ZNK2v88internal11interpreter20BytecodeArrayBuilder9ParameterEi@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	movl	32(%r14), %edx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	movq	808(%rbx), %rax
	movl	24(%rax), %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder16StoreContextSlotENS1_8RegisterEii@PLT
	movq	-56(%rbp), %r8
.L632:
	leaq	1(%r13), %rax
	cmpq	%r8, %r13
	jne	.L634
.L628:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L639:
	.cfi_restore_state
	leaq	24(%rdi), %r13
	movq	%r13, %rdi
	call	_ZNK2v88internal11interpreter20BytecodeArrayBuilder8ReceiverEv@PLT
	movq	%r13, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	movl	32(%r12), %edx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	movq	808(%rbx), %rax
	movl	24(%rax), %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder16StoreContextSlotENS1_8RegisterEii@PLT
	jmp	.L629
	.cfi_endproc
.LFE21001:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator41BuildLocalActivationContextInitializationEv, .-_ZN2v88internal11interpreter17BytecodeGenerator41BuildLocalActivationContextInitializationEv
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator25BuildNewLocalBlockContextEPNS0_5ScopeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator25BuildNewLocalBlockContextEPNS0_5ScopeE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator25BuildNewLocalBlockContextEPNS0_5ScopeE, @function
_ZN2v88internal11interpreter17BytecodeGenerator25BuildNewLocalBlockContextEPNS0_5ScopeE:
.LFB21002:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	816(%rdi), %rax
	movq	%rdi, -40(%rbp)
	addq	$24, %rdi
	movq	$2, -24(%rbp)
	movq	%rax, -48(%rbp)
	movl	304(%rdi), %eax
	movl	%eax, -32(%rbp)
	leaq	-48(%rbp), %rax
	movq	%rax, 792(%rdi)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder18CreateBlockContextEPKNS0_5ScopeE@PLT
	movq	-40(%rbp), %rdx
	movq	-48(%rbp), %rax
	movq	336(%rdx), %rdi
	movq	%rax, 816(%rdx)
	movl	-32(%rbp), %eax
	movl	328(%rdx), %esi
	movl	%eax, 328(%rdx)
	testq	%rdi, %rdi
	je	.L640
	subl	%eax, %esi
	movq	(%rdi), %rcx
	movq	%rsi, %rdx
	movl	%eax, %esi
	salq	$32, %rdx
	orq	%rdx, %rsi
	call	*32(%rcx)
.L640:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L647
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L647:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21002:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator25BuildNewLocalBlockContextEPNS0_5ScopeE, .-_ZN2v88internal11interpreter17BytecodeGenerator25BuildNewLocalBlockContextEPNS0_5ScopeE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator24BuildNewLocalWithContextEPNS0_5ScopeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator24BuildNewLocalWithContextEPNS0_5ScopeE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator24BuildNewLocalWithContextEPNS0_5ScopeE, @function
_ZN2v88internal11interpreter17BytecodeGenerator24BuildNewLocalWithContextEPNS0_5ScopeE:
.LFB21003:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$56, %rsp
	movl	328(%rdi), %r14d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	816(%rdi), %rax
	movq	%rdi, -72(%rbp)
	movl	%r14d, -64(%rbp)
	movq	%rax, -80(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rax, 816(%rdi)
	leal	1(%r14), %eax
	cmpl	%eax, 332(%rdi)
	movl	%eax, 328(%rdi)
	cmovge	332(%rdi), %eax
	movq	$2, -56(%rbp)
	movl	%eax, 332(%rdi)
	movq	336(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L649
	movq	(%rdi), %rax
	movl	%r14d, %esi
	call	*16(%rax)
.L649:
	addq	$24, %r12
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder8ToObjectENS1_8RegisterE@PLT
	movq	%r13, %rdx
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder17CreateWithContextENS1_8RegisterEPKNS0_5ScopeE@PLT
	movq	-72(%rbp), %rdx
	movq	-80(%rbp), %rax
	movq	336(%rdx), %rdi
	movq	%rax, 816(%rdx)
	movl	-64(%rbp), %eax
	movl	328(%rdx), %esi
	movl	%eax, 328(%rdx)
	testq	%rdi, %rdi
	je	.L648
	subl	%eax, %esi
	movq	(%rdi), %rcx
	movq	%rsi, %rdx
	movl	%eax, %esi
	salq	$32, %rdx
	orq	%rdx, %rsi
	call	*32(%rcx)
.L648:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L659
	addq	$56, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L659:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21003:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator24BuildNewLocalWithContextEPNS0_5ScopeE, .-_ZN2v88internal11interpreter17BytecodeGenerator24BuildNewLocalWithContextEPNS0_5ScopeE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator25BuildNewLocalCatchContextEPNS0_5ScopeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator25BuildNewLocalCatchContextEPNS0_5ScopeE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator25BuildNewLocalCatchContextEPNS0_5ScopeE, @function
_ZN2v88internal11interpreter17BytecodeGenerator25BuildNewLocalCatchContextEPNS0_5ScopeE:
.LFB21004:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$56, %rsp
	movl	328(%rdi), %r14d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	816(%rdi), %rax
	movq	%rdi, -72(%rbp)
	movl	%r14d, -64(%rbp)
	movq	%rax, -80(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rax, 816(%rdi)
	leal	1(%r14), %eax
	cmpl	%eax, 332(%rdi)
	movl	%eax, 328(%rdi)
	cmovge	332(%rdi), %eax
	movq	$2, -56(%rbp)
	movl	%eax, 332(%rdi)
	movq	336(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L661
	movq	(%rdi), %rax
	movl	%r14d, %esi
	call	*16(%rax)
.L661:
	addq	$24, %r12
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	%r13, %rdx
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder18CreateCatchContextENS1_8RegisterEPKNS0_5ScopeE@PLT
	movq	-72(%rbp), %rdx
	movq	-80(%rbp), %rax
	movq	336(%rdx), %rdi
	movq	%rax, 816(%rdx)
	movl	-64(%rbp), %eax
	movl	328(%rdx), %esi
	movl	%eax, 328(%rdx)
	testq	%rdi, %rdi
	je	.L660
	subl	%eax, %esi
	movq	(%rdi), %rcx
	movq	%rsi, %rdx
	movl	%eax, %esi
	salq	$32, %rdx
	orq	%rdx, %rsi
	call	*32(%rcx)
.L660:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L671
	addq	$56, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L671:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21004:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator25BuildNewLocalCatchContextEPNS0_5ScopeE, .-_ZN2v88internal11interpreter17BytecodeGenerator25BuildNewLocalCatchContextEPNS0_5ScopeE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator18VisitSetHomeObjectENS1_8RegisterES3_PNS0_15LiteralPropertyE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator18VisitSetHomeObjectENS1_8RegisterES3_PNS0_15LiteralPropertyE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator18VisitSetHomeObjectENS1_8RegisterES3_PNS0_15LiteralPropertyE, @function
_ZN2v88internal11interpreter17BytecodeGenerator18VisitSetHomeObjectENS1_8RegisterES3_PNS0_15LiteralPropertyE:
.LFB21006:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	8(%rcx), %rdi
	call	_ZN2v88internal15FunctionLiteral15NeedsHomeObjectEPNS0_10ExpressionE@PLT
	testb	%al, %al
	je	.L672
	movq	512(%rbx), %rax
	leaq	56(%rax), %rdi
	movq	536(%rbx), %rax
	movzbl	129(%rax), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%esi, %esi
	andl	$-9, %esi
	addl	$11, %esi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	leaq	24(%rbx), %rdi
	movl	%r13d, %esi
	movl	%eax, %r14d
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	movl	%r14d, %edx
	movl	%r12d, %esi
	movq	%rax, %rdi
	movq	536(%rbx), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	movzbl	129(%rax), %ecx
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	andl	$1, %ecx
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder23StoreHomeObjectPropertyENS1_8RegisterEiNS0_12LanguageModeE@PLT
	.p2align 4,,10
	.p2align 3
.L672:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21006:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator18VisitSetHomeObjectENS1_8RegisterES3_PNS0_15LiteralPropertyE, .-_ZN2v88internal11interpreter17BytecodeGenerator18VisitSetHomeObjectENS1_8RegisterES3_PNS0_15LiteralPropertyE
	.section	.rodata._ZN2v88internal11interpreter17BytecodeGenerator34BuildPushUndefinedIntoRegisterListEPNS1_12RegisterListE.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"reg.index() == reg_list->last_register().index()"
	.section	.rodata._ZN2v88internal11interpreter17BytecodeGenerator34BuildPushUndefinedIntoRegisterListEPNS1_12RegisterListE.str1.1,"aMS",@progbits,1
.LC6:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator34BuildPushUndefinedIntoRegisterListEPNS1_12RegisterListE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator34BuildPushUndefinedIntoRegisterListEPNS1_12RegisterListE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator34BuildPushUndefinedIntoRegisterListEPNS1_12RegisterListE, @function
_ZN2v88internal11interpreter17BytecodeGenerator34BuildPushUndefinedIntoRegisterListEPNS1_12RegisterListE:
.LFB21012:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	328(%rdi), %r14d
	movq	%rdi, %rbx
	leal	1(%r14), %eax
	cmpl	%eax, 332(%rdi)
	movl	%eax, 328(%rdi)
	cmovge	332(%rdi), %eax
	movl	%eax, 332(%rdi)
	movq	336(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L678
	movq	(%rdi), %rax
	movl	%r14d, %esi
	call	*16(%rax)
.L678:
	movl	4(%r12), %eax
	leal	1(%rax), %r13d
	movl	%r13d, 4(%r12)
	testl	%r13d, %r13d
	je	.L679
	addl	(%r12), %eax
	movl	%eax, %r13d
.L679:
	cmpl	%r14d, %r13d
	jne	.L688
	leaq	24(%rbx), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder13LoadUndefinedEv@PLT
	popq	%rbx
	movl	%r13d, %esi
	popq	%r12
	movq	%rax, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	.p2align 4,,10
	.p2align 3
.L688:
	.cfi_restore_state
	leaq	.LC5(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21012:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator34BuildPushUndefinedIntoRegisterListEPNS1_12RegisterListE, .-_ZN2v88internal11interpreter17BytecodeGenerator34BuildPushUndefinedIntoRegisterListEPNS1_12RegisterListE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator43BuildIncrementBlockCoverageCounterIfEnabledEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator43BuildIncrementBlockCoverageCounterIfEnabledEi
	.type	_ZN2v88internal11interpreter17BytecodeGenerator43BuildIncrementBlockCoverageCounterIfEnabledEi, @function
_ZN2v88internal11interpreter17BytecodeGenerator43BuildIncrementBlockCoverageCounterIfEnabledEi:
.LFB21017:
	.cfi_startproc
	endbr64
	movq	568(%rdi), %rax
	testq	%rax, %rax
	je	.L689
	cmpl	$-1, %esi
	je	.L689
	movq	32(%rax), %rdi
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder15IncBlockCounterEi@PLT
	.p2align 4,,10
	.p2align 3
.L689:
	ret
	.cfi_endproc
.LFE21017:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator43BuildIncrementBlockCoverageCounterIfEnabledEi, .-_ZN2v88internal11interpreter17BytecodeGenerator43BuildIncrementBlockCoverageCounterIfEnabledEi
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator9BuildTestENS1_20BytecodeArrayBuilder13ToBooleanModeEPNS1_14BytecodeLabelsES6_NS2_15TestFallthroughE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator9BuildTestENS1_20BytecodeArrayBuilder13ToBooleanModeEPNS1_14BytecodeLabelsES6_NS2_15TestFallthroughE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator9BuildTestENS1_20BytecodeArrayBuilder13ToBooleanModeEPNS1_14BytecodeLabelsES6_NS2_15TestFallthroughE, @function
_ZN2v88internal11interpreter17BytecodeGenerator9BuildTestENS1_20BytecodeArrayBuilder13ToBooleanModeEPNS1_14BytecodeLabelsES6_NS2_15TestFallthroughE:
.LFB21024:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movq	%rdx, %rdi
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -40
	cmpl	$1, %r8d
	je	.L692
	movq	%rcx, %r12
	cmpl	$2, %r8d
	je	.L693
	testl	%r8d, %r8d
	je	.L696
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L692:
	.cfi_restore_state
	call	_ZN2v88internal11interpreter14BytecodeLabels3NewEv@PLT
	addq	$8, %rsp
	leaq	24(%r13), %rdi
	movl	%r14d, %esi
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder10JumpIfTrueENS2_13ToBooleanModeEPNS1_13BytecodeLabelE@PLT
	.p2align 4,,10
	.p2align 3
.L696:
	.cfi_restore_state
	movq	%rcx, %rdi
	call	_ZN2v88internal11interpreter14BytecodeLabels3NewEv@PLT
	addq	$8, %rsp
	leaq	24(%r13), %rdi
	movl	%r14d, %esi
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder11JumpIfFalseENS2_13ToBooleanModeEPNS1_13BytecodeLabelE@PLT
	.p2align 4,,10
	.p2align 3
.L693:
	.cfi_restore_state
	call	_ZN2v88internal11interpreter14BytecodeLabels3NewEv@PLT
	addq	$24, %r13
	movl	%r14d, %esi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder10JumpIfTrueENS2_13ToBooleanModeEPNS1_13BytecodeLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter14BytecodeLabels3NewEv@PLT
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%r12
	movq	%rax, %rsi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder4JumpEPNS1_13BytecodeLabelE@PLT
	.cfi_endproc
.LFE21024:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator9BuildTestENS1_20BytecodeArrayBuilder13ToBooleanModeEPNS1_14BytecodeLabelsES6_NS2_15TestFallthroughE, .-_ZN2v88internal11interpreter17BytecodeGenerator9BuildTestENS1_20BytecodeArrayBuilder13ToBooleanModeEPNS1_14BytecodeLabelsES6_NS2_15TestFallthroughE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator27GetRegisterForLocalVariableEPNS0_8VariableE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator27GetRegisterForLocalVariableEPNS0_8VariableE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator27GetRegisterForLocalVariableEPNS0_8VariableE, @function
_ZN2v88internal11interpreter17BytecodeGenerator27GetRegisterForLocalVariableEPNS0_8VariableE:
.LFB21032:
	.cfi_startproc
	endbr64
	movl	32(%rsi), %esi
	addq	$24, %rdi
	jmp	_ZNK2v88internal11interpreter20BytecodeArrayBuilder5LocalEi@PLT
	.cfi_endproc
.LFE21032:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator27GetRegisterForLocalVariableEPNS0_8VariableE, .-_ZN2v88internal11interpreter17BytecodeGenerator27GetRegisterForLocalVariableEPNS0_8VariableE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator21GetDummyCompareICSlotEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator21GetDummyCompareICSlotEv
	.type	_ZN2v88internal11interpreter17BytecodeGenerator21GetDummyCompareICSlotEv, @function
_ZN2v88internal11interpreter17BytecodeGenerator21GetDummyCompareICSlotEv:
.LFB21043:
	.cfi_startproc
	endbr64
	movl	844(%rdi), %eax
	cmpl	$-1, %eax
	je	.L704
	ret
	.p2align 4,,10
	.p2align 3
.L704:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	840(%rdi), %esi
	movq	848(%rdi), %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movl	%eax, 844(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21043:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator21GetDummyCompareICSlotEv, .-_ZN2v88internal11interpreter17BytecodeGenerator21GetDummyCompareICSlotEv
	.section	.text._ZNSt6vectorIN2v88internal11SourceRangeENS1_13ZoneAllocatorIS2_EEE12emplace_backIJRS2_EEEvDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal11SourceRangeENS1_13ZoneAllocatorIS2_EEE12emplace_backIJRS2_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal11SourceRangeENS1_13ZoneAllocatorIS2_EEE12emplace_backIJRS2_EEEvDpOT_
	.type	_ZNSt6vectorIN2v88internal11SourceRangeENS1_13ZoneAllocatorIS2_EEE12emplace_backIJRS2_EEEvDpOT_, @function
_ZNSt6vectorIN2v88internal11SourceRangeENS1_13ZoneAllocatorIS2_EEE12emplace_backIJRS2_EEEvDpOT_:
.LFB23344:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	16(%rdi), %r13
	cmpq	24(%rdi), %r13
	je	.L706
	movq	(%rsi), %rax
	movq	%rax, 0(%r13)
	addq	$8, 16(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L706:
	.cfi_restore_state
	movq	8(%rdi), %r14
	movq	%r13, %rcx
	subq	%r14, %rcx
	movq	%rcx, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L731
	testq	%rax, %rax
	je	.L718
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L732
	movl	$2147483640, %esi
	movl	$2147483640, %r15d
.L709:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rsi, %rdx
	jb	.L733
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L712:
	leaq	(%rax,%r15), %rsi
	leaq	8(%rax), %rdx
.L710:
	movq	(%r12), %rdi
	movq	%rdi, (%rax,%rcx)
	cmpq	%r14, %r13
	je	.L713
	leaq	-8(%r13), %rdi
	leaq	15(%rax), %rcx
	movq	%r14, %rdx
	subq	%r14, %rdi
	subq	%r14, %rcx
	movq	%rdi, %r8
	shrq	$3, %r8
	cmpq	$30, %rcx
	jbe	.L721
	movabsq	$2305843009213693950, %rcx
	testq	%rcx, %r8
	je	.L721
	addq	$1, %r8
	xorl	%edx, %edx
	movq	%r8, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L715:
	movdqu	(%r14,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L715
	movq	%r8, %r9
	andq	$-2, %r9
	leaq	0(,%r9,8), %rcx
	leaq	(%r14,%rcx), %rdx
	addq	%rax, %rcx
	cmpq	%r9, %r8
	je	.L717
	movq	(%rdx), %rdx
	movq	%rdx, (%rcx)
.L717:
	leaq	16(%rax,%rdi), %rdx
.L713:
	movq	%rax, %xmm0
	movq	%rdx, %xmm2
	movq	%rsi, 24(%rbx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 8(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L732:
	.cfi_restore_state
	testq	%rdx, %rdx
	jne	.L734
	movl	$8, %edx
	xorl	%esi, %esi
	xorl	%eax, %eax
	jmp	.L710
	.p2align 4,,10
	.p2align 3
.L718:
	movl	$8, %esi
	movl	$8, %r15d
	jmp	.L709
	.p2align 4,,10
	.p2align 3
.L721:
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L714:
	movl	(%rdx), %r9d
	movl	4(%rdx), %r8d
	addq	$8, %rdx
	addq	$8, %rcx
	movl	%r9d, -8(%rcx)
	movl	%r8d, -4(%rcx)
	cmpq	%rdx, %r13
	jne	.L714
	jmp	.L717
.L733:
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	jmp	.L712
.L731:
	leaq	.LC3(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L734:
	movl	$268435455, %r15d
	cmpq	$268435455, %rdx
	cmova	%r15, %rdx
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rsi
	jmp	.L709
	.cfi_endproc
.LFE23344:
	.size	_ZNSt6vectorIN2v88internal11SourceRangeENS1_13ZoneAllocatorIS2_EEE12emplace_backIJRS2_EEEvDpOT_, .-_ZNSt6vectorIN2v88internal11SourceRangeENS1_13ZoneAllocatorIS2_EEE12emplace_backIJRS2_EEEvDpOT_
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator38AllocateNaryBlockCoverageSlotIfEnabledEPNS0_13NaryOperationEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator38AllocateNaryBlockCoverageSlotIfEnabledEPNS0_13NaryOperationEm
	.type	_ZN2v88internal11interpreter17BytecodeGenerator38AllocateNaryBlockCoverageSlotIfEnabledEPNS0_13NaryOperationEm, @function
_ZN2v88internal11interpreter17BytecodeGenerator38AllocateNaryBlockCoverageSlotIfEnabledEPNS0_13NaryOperationEm:
.LFB21015:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 12, -24
	movq	568(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L745
	movq	40(%rdi), %rax
	leaq	16(%rax), %r8
	movq	24(%rax), %rax
	movq	%r8, %rcx
	testq	%rax, %rax
	jne	.L739
	jmp	.L745
	.p2align 4,,10
	.p2align 3
.L754:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L740
.L739:
	cmpq	%rsi, 32(%rax)
	jnb	.L754
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L739
.L740:
	cmpq	%rcx, %r8
	je	.L745
	cmpq	%rsi, 32(%rcx)
	ja	.L745
	movq	40(%rcx), %rax
	testq	%rax, %rax
	je	.L745
	movq	16(%rax), %rax
	leaq	(%rax,%rdx,8), %rax
	movl	(%rax), %edx
	movq	(%rax), %rax
	movq	%rax, -32(%rbp)
	cmpl	$-1, %edx
	je	.L745
	movq	16(%rdi), %r12
	leaq	-32(%rbp), %rsi
	subq	8(%rdi), %r12
	sarq	$3, %r12
	call	_ZNSt6vectorIN2v88internal11SourceRangeENS1_13ZoneAllocatorIS2_EEE12emplace_backIJRS2_EEEvDpOT_
.L735:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L755
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L745:
	.cfi_restore_state
	movl	$-1, %r12d
	jmp	.L735
.L755:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21015:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator38AllocateNaryBlockCoverageSlotIfEnabledEPNS0_13NaryOperationEm, .-_ZN2v88internal11interpreter17BytecodeGenerator38AllocateNaryBlockCoverageSlotIfEnabledEPNS0_13NaryOperationEm
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator34AllocateBlockCoverageSlotIfEnabledEPNS0_7AstNodeENS0_15SourceRangeKindE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator34AllocateBlockCoverageSlotIfEnabledEPNS0_7AstNodeENS0_15SourceRangeKindE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator34AllocateBlockCoverageSlotIfEnabledEPNS0_7AstNodeENS0_15SourceRangeKindE, @function
_ZN2v88internal11interpreter17BytecodeGenerator34AllocateBlockCoverageSlotIfEnabledEPNS0_7AstNodeENS0_15SourceRangeKindE:
.LFB21014:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	568(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%r13, %r13
	je	.L766
	movq	40(%r13), %rax
	leaq	16(%rax), %rdi
	movq	24(%rax), %rax
	movq	%rdi, %rcx
	testq	%rax, %rax
	jne	.L760
	jmp	.L766
	.p2align 4,,10
	.p2align 3
.L772:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L761
.L760:
	cmpq	%rsi, 32(%rax)
	jnb	.L772
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L760
.L761:
	cmpq	%rcx, %rdi
	je	.L766
	cmpq	%rsi, 32(%rcx)
	ja	.L766
	movq	40(%rcx), %rdi
	testq	%rdi, %rdi
	je	.L766
	movq	(%rdi), %rax
	movl	%edx, %esi
	call	*16(%rax)
	movq	%rax, -32(%rbp)
	cmpl	$-1, %eax
	je	.L766
	movq	16(%r13), %r12
	leaq	-32(%rbp), %rsi
	subq	8(%r13), %r12
	movq	%r13, %rdi
	sarq	$3, %r12
	call	_ZNSt6vectorIN2v88internal11SourceRangeENS1_13ZoneAllocatorIS2_EEE12emplace_backIJRS2_EEEvDpOT_
.L756:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L773
	addq	$16, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L766:
	.cfi_restore_state
	movl	$-1, %r12d
	jmp	.L756
.L773:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21014:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator34AllocateBlockCoverageSlotIfEnabledEPNS0_7AstNodeENS0_15SourceRangeKindE, .-_ZN2v88internal11interpreter17BytecodeGenerator34AllocateBlockCoverageSlotIfEnabledEPNS0_7AstNodeENS0_15SourceRangeKindE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator43BuildIncrementBlockCoverageCounterIfEnabledEPNS0_7AstNodeENS0_15SourceRangeKindE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator43BuildIncrementBlockCoverageCounterIfEnabledEPNS0_7AstNodeENS0_15SourceRangeKindE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator43BuildIncrementBlockCoverageCounterIfEnabledEPNS0_7AstNodeENS0_15SourceRangeKindE, @function
_ZN2v88internal11interpreter17BytecodeGenerator43BuildIncrementBlockCoverageCounterIfEnabledEPNS0_7AstNodeENS0_15SourceRangeKindE:
.LFB21016:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	568(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.L774
	movq	40(%rbx), %rax
	leaq	16(%rax), %rdi
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L774
	movq	%rdi, %rcx
	jmp	.L778
	.p2align 4,,10
	.p2align 3
.L792:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L779
.L778:
	cmpq	%rsi, 32(%rax)
	jnb	.L792
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L778
.L779:
	cmpq	%rcx, %rdi
	je	.L774
	cmpq	%rsi, 32(%rcx)
	ja	.L774
	movq	40(%rcx), %rdi
	testq	%rdi, %rdi
	je	.L774
	movq	(%rdi), %rax
	movl	%edx, %esi
	call	*16(%rax)
	movq	%rax, -32(%rbp)
	cmpl	$-1, %eax
	je	.L774
	movq	16(%rbx), %r12
	subq	8(%rbx), %r12
	leaq	-32(%rbp), %rsi
	movq	%rbx, %rdi
	sarq	$3, %r12
	call	_ZNSt6vectorIN2v88internal11SourceRangeENS1_13ZoneAllocatorIS2_EEE12emplace_backIJRS2_EEEvDpOT_
	cmpl	$-1, %r12d
	je	.L774
	movq	32(%rbx), %rdi
	movl	%r12d, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder15IncBlockCounterEi@PLT
.L774:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L793
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L793:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21016:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator43BuildIncrementBlockCoverageCounterIfEnabledEPNS0_7AstNodeENS0_15SourceRangeKindE, .-_ZN2v88internal11interpreter17BytecodeGenerator43BuildIncrementBlockCoverageCounterIfEnabledEPNS0_7AstNodeENS0_15SourceRangeKindE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator19VisitBreakStatementEPNS0_14BreakStatementE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator19VisitBreakStatementEPNS0_14BreakStatementE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator19VisitBreakStatementEPNS0_14BreakStatementE, @function
_ZN2v88internal11interpreter17BytecodeGenerator19VisitBreakStatementEPNS0_14BreakStatementE:
.LFB20762:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	568(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%r13, %r13
	je	.L795
	movq	40(%r13), %rax
	leaq	16(%rax), %rcx
	movq	24(%rax), %rax
	movq	%rcx, %rdx
	testq	%rax, %rax
	jne	.L798
	jmp	.L795
	.p2align 4,,10
	.p2align 3
.L819:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L799
.L798:
	cmpq	%rbx, 32(%rax)
	jnb	.L819
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L798
.L799:
	cmpq	%rdx, %rcx
	je	.L795
	cmpq	%rbx, 32(%rdx)
	jbe	.L820
.L795:
	movl	(%rbx), %eax
	cmpl	$-1, %eax
	je	.L803
	movb	$2, 496(%r12)
	movl	%eax, 500(%r12)
.L803:
	movq	800(%r12), %r12
	movq	8(%rbx), %rbx
	jmp	.L805
	.p2align 4,,10
	.p2align 3
.L822:
	movq	16(%r12), %r12
	testq	%r12, %r12
	je	.L821
.L805:
	movq	(%r12), %rax
	xorl	%esi, %esi
	movl	$-1, %ecx
	movq	%rbx, %rdx
	movq	%r12, %rdi
	call	*16(%rax)
	testb	%al, %al
	je	.L822
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L823
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L820:
	.cfi_restore_state
	movq	40(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L795
	movq	(%rdi), %rax
	movl	$2, %esi
	call	*16(%rax)
	movq	%rax, -48(%rbp)
	cmpl	$-1, %eax
	je	.L795
	leaq	-48(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt6vectorIN2v88internal11SourceRangeENS1_13ZoneAllocatorIS2_EEE12emplace_backIJRS2_EEEvDpOT_
	jmp	.L795
.L821:
	leaq	.LC0(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L823:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20762:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator19VisitBreakStatementEPNS0_14BreakStatementE, .-_ZN2v88internal11interpreter17BytecodeGenerator19VisitBreakStatementEPNS0_14BreakStatementE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator22VisitContinueStatementEPNS0_17ContinueStatementE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator22VisitContinueStatementEPNS0_17ContinueStatementE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator22VisitContinueStatementEPNS0_17ContinueStatementE, @function
_ZN2v88internal11interpreter17BytecodeGenerator22VisitContinueStatementEPNS0_17ContinueStatementE:
.LFB20761:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	568(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%r13, %r13
	je	.L825
	movq	40(%r13), %rax
	leaq	16(%rax), %rcx
	movq	24(%rax), %rax
	movq	%rcx, %rdx
	testq	%rax, %rax
	jne	.L828
	jmp	.L825
	.p2align 4,,10
	.p2align 3
.L849:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L829
.L828:
	cmpq	%rbx, 32(%rax)
	jnb	.L849
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L828
.L829:
	cmpq	%rdx, %rcx
	je	.L825
	cmpq	%rbx, 32(%rdx)
	jbe	.L850
.L825:
	movl	(%rbx), %eax
	cmpl	$-1, %eax
	je	.L833
	movb	$2, 496(%r12)
	movl	%eax, 500(%r12)
.L833:
	movq	800(%r12), %r12
	movq	8(%rbx), %rbx
	jmp	.L835
	.p2align 4,,10
	.p2align 3
.L852:
	movq	16(%r12), %r12
	testq	%r12, %r12
	je	.L851
.L835:
	movq	(%r12), %rax
	movl	$-1, %ecx
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movl	$1, %esi
	call	*16(%rax)
	testb	%al, %al
	je	.L852
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L853
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L850:
	.cfi_restore_state
	movq	40(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L825
	movq	(%rdi), %rax
	movl	$2, %esi
	call	*16(%rax)
	movq	%rax, -48(%rbp)
	cmpl	$-1, %eax
	je	.L825
	leaq	-48(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt6vectorIN2v88internal11SourceRangeENS1_13ZoneAllocatorIS2_EEE12emplace_backIJRS2_EEEvDpOT_
	jmp	.L825
.L851:
	leaq	.LC0(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L853:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20761:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator22VisitContinueStatementEPNS0_17ContinueStatementE, .-_ZN2v88internal11interpreter17BytecodeGenerator22VisitContinueStatementEPNS0_17ContinueStatementE
	.section	.text._ZN2v88internal11interpreter29ConditionalControlFlowBuilderC2EPNS1_20BytecodeArrayBuilderEPNS1_20BlockCoverageBuilderEPNS0_7AstNodeE,"axG",@progbits,_ZN2v88internal11interpreter29ConditionalControlFlowBuilderC5EPNS1_20BytecodeArrayBuilderEPNS1_20BlockCoverageBuilderEPNS0_7AstNodeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11interpreter29ConditionalControlFlowBuilderC2EPNS1_20BytecodeArrayBuilderEPNS1_20BlockCoverageBuilderEPNS0_7AstNodeE
	.type	_ZN2v88internal11interpreter29ConditionalControlFlowBuilderC2EPNS1_20BytecodeArrayBuilderEPNS1_20BlockCoverageBuilderEPNS0_7AstNodeE, @function
_ZN2v88internal11interpreter29ConditionalControlFlowBuilderC2EPNS1_20BytecodeArrayBuilderEPNS1_20BlockCoverageBuilderEPNS0_7AstNodeE:
.LFB19963:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN2v88internal11interpreter29ConditionalControlFlowBuilderE(%rip), %rax
	movq	%rsi, 8(%rdi)
	movq	%rax, (%rdi)
	movq	(%rsi), %rax
	movq	$0, 40(%rdi)
	movq	%rax, 16(%rdi)
	leaq	24(%rdi), %rax
	movq	%rax, 32(%rdi)
	movq	%rax, 24(%rdi)
	movb	$0, 48(%rdi)
	movq	(%rsi), %rax
	movq	$0, 80(%rdi)
	movq	%rax, 56(%rdi)
	leaq	64(%rdi), %rax
	movq	%rax, 72(%rdi)
	movq	%rax, 64(%rdi)
	movb	$0, 88(%rdi)
	movq	(%rsi), %rax
	movq	%rcx, 136(%rdi)
	movq	%rax, 96(%rdi)
	leaq	104(%rdi), %rax
	movq	%rax, 112(%rdi)
	movq	%rax, 104(%rdi)
	movq	$0, 120(%rdi)
	movb	$0, 128(%rdi)
	movq	%rdx, 152(%rdi)
	testq	%rdx, %rdx
	je	.L854
	movq	40(%rdx), %rax
	movq	%rcx, %r13
	movq	%rdx, %r12
	leaq	16(%rax), %rcx
	movq	24(%rax), %rax
	movq	%rcx, %rdx
	testq	%rax, %rax
	jne	.L858
	.p2align 4,,10
	.p2align 3
.L862:
	movl	$-1, %r14d
.L857:
	movl	%r14d, 144(%rbx)
	movq	40(%r12), %rax
	leaq	16(%rax), %rcx
	movq	24(%rax), %rax
	movq	%rcx, %rdx
	testq	%rax, %rax
	jne	.L865
	jmp	.L869
	.p2align 4,,10
	.p2align 3
.L883:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L866
.L865:
	cmpq	%r13, 32(%rax)
	jnb	.L883
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L865
.L866:
	cmpq	%rdx, %rcx
	je	.L869
	cmpq	%r13, 32(%rdx)
	ja	.L869
	movq	40(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L869
	movq	(%rdi), %rax
	movl	$3, %esi
	call	*16(%rax)
	movq	%rax, -48(%rbp)
	cmpl	$-1, %eax
	je	.L869
	movq	16(%r12), %r13
	leaq	-48(%rbp), %rsi
	subq	8(%r12), %r13
	movq	%r12, %rdi
	sarq	$3, %r13
	call	_ZNSt6vectorIN2v88internal11SourceRangeENS1_13ZoneAllocatorIS2_EEE12emplace_backIJRS2_EEEvDpOT_
.L864:
	movl	%r13d, 148(%rbx)
.L854:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L884
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L885:
	.cfi_restore_state
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L859
.L858:
	cmpq	%r13, 32(%rax)
	jnb	.L885
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L858
.L859:
	cmpq	%rdx, %rcx
	je	.L862
	cmpq	%r13, 32(%rdx)
	ja	.L862
	movq	40(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L862
	movq	(%rdi), %rax
	movl	$6, %esi
	call	*16(%rax)
	movq	%rax, -48(%rbp)
	cmpl	$-1, %eax
	je	.L862
	movq	16(%r12), %r14
	leaq	-48(%rbp), %rsi
	subq	8(%r12), %r14
	movq	%r12, %rdi
	sarq	$3, %r14
	call	_ZNSt6vectorIN2v88internal11SourceRangeENS1_13ZoneAllocatorIS2_EEE12emplace_backIJRS2_EEEvDpOT_
	jmp	.L857
	.p2align 4,,10
	.p2align 3
.L869:
	movl	$-1, %r13d
	jmp	.L864
.L884:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19963:
	.size	_ZN2v88internal11interpreter29ConditionalControlFlowBuilderC2EPNS1_20BytecodeArrayBuilderEPNS1_20BlockCoverageBuilderEPNS0_7AstNodeE, .-_ZN2v88internal11interpreter29ConditionalControlFlowBuilderC2EPNS1_20BytecodeArrayBuilderEPNS1_20BlockCoverageBuilderEPNS0_7AstNodeE
	.weak	_ZN2v88internal11interpreter29ConditionalControlFlowBuilderC1EPNS1_20BytecodeArrayBuilderEPNS1_20BlockCoverageBuilderEPNS0_7AstNodeE
	.set	_ZN2v88internal11interpreter29ConditionalControlFlowBuilderC1EPNS1_20BytecodeArrayBuilderEPNS1_20BlockCoverageBuilderEPNS0_7AstNodeE,_ZN2v88internal11interpreter29ConditionalControlFlowBuilderC2EPNS1_20BytecodeArrayBuilderEPNS1_20BlockCoverageBuilderEPNS0_7AstNodeE
	.section	.rodata._ZNSt6vectorIN2v88internal11interpreter13BytecodeLabelENS1_13ZoneAllocatorIS3_EEE17_M_default_appendEm.str1.1,"aMS",@progbits,1
.LC7:
	.string	"vector::_M_default_append"
	.section	.text._ZNSt6vectorIN2v88internal11interpreter13BytecodeLabelENS1_13ZoneAllocatorIS3_EEE17_M_default_appendEm,"axG",@progbits,_ZNSt6vectorIN2v88internal11interpreter13BytecodeLabelENS1_13ZoneAllocatorIS3_EEE17_M_default_appendEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal11interpreter13BytecodeLabelENS1_13ZoneAllocatorIS3_EEE17_M_default_appendEm
	.type	_ZNSt6vectorIN2v88internal11interpreter13BytecodeLabelENS1_13ZoneAllocatorIS3_EEE17_M_default_appendEm, @function
_ZNSt6vectorIN2v88internal11interpreter13BytecodeLabelENS1_13ZoneAllocatorIS3_EEE17_M_default_appendEm:
.LFB25079:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L907
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$134217727, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movq	%rdx, %rsi
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	movq	%rcx, %rbx
	subq	8(%rdi), %rbx
	subq	%rcx, %rax
	movq	%rbx, %r14
	sarq	$4, %rax
	sarq	$4, %r14
	subq	%r14, %rsi
	cmpq	%r12, %rax
	jb	.L888
	movq	%rcx, %rax
	movq	%r12, %rdx
	.p2align 4,,10
	.p2align 3
.L889:
	movb	$0, (%rax)
	addq	$16, %rax
	movq	$-1, -8(%rax)
	subq	$1, %rdx
	jne	.L889
	salq	$4, %r12
	addq	%r12, %rcx
	movq	%rcx, 16(%r13)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L907:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L888:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	cmpq	%r12, %rsi
	jb	.L910
	cmpq	%r14, %r12
	movq	%r14, %r15
	movq	(%rdi), %rdi
	cmovnb	%r12, %r15
	movq	16(%rdi), %r8
	movq	24(%rdi), %rax
	addq	%r14, %r15
	cmpq	$134217727, %r15
	cmova	%rdx, %r15
	subq	%r8, %rax
	salq	$4, %r15
	movq	%r15, %rsi
	cmpq	%r15, %rax
	jb	.L911
	addq	%r8, %rsi
	movq	%rsi, 16(%rdi)
.L893:
	leaq	(%r8,%rbx), %rax
	movq	%r12, %rdx
	.p2align 4,,10
	.p2align 3
.L894:
	movb	$0, (%rax)
	addq	$16, %rax
	movq	$-1, -8(%rax)
	subq	$1, %rdx
	jne	.L894
	movq	8(%r13), %rax
	movq	16(%r13), %rdi
	movq	%r8, %r9
	subq	%rax, %r9
	cmpq	%rax, %rdi
	je	.L896
	.p2align 4,,10
	.p2align 3
.L895:
	movzbl	(%rax), %esi
	movq	8(%rax), %rcx
	leaq	(%rax,%r9), %rdx
	addq	$16, %rax
	movb	%sil, (%rdx)
	movq	%rcx, 8(%rdx)
	cmpq	%rax, %rdi
	jne	.L895
.L896:
	addq	%r14, %r12
	addq	%r8, %r15
	movq	%r8, 8(%r13)
	salq	$4, %r12
	movq	%r15, 24(%r13)
	leaq	(%r8,%r12), %rax
	movq	%rax, 16(%r13)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L911:
	.cfi_restore_state
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r8
	jmp	.L893
.L910:
	leaq	.LC7(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE25079:
	.size	_ZNSt6vectorIN2v88internal11interpreter13BytecodeLabelENS1_13ZoneAllocatorIS3_EEE17_M_default_appendEm, .-_ZNSt6vectorIN2v88internal11interpreter13BytecodeLabelENS1_13ZoneAllocatorIS3_EEE17_M_default_appendEm
	.section	.text._ZNSt8_Rb_treeISt5tupleIJN2v88internal11interpreter17BytecodeGenerator17FeedbackSlotCache8SlotKindEiPKvEESt4pairIKS9_iESt10_Select1stISC_ESt4lessIS9_ENS2_13ZoneAllocatorISC_EEE17_M_emplace_uniqueIJRSA_IS9_iEEEESA_ISt17_Rb_tree_iteratorISC_EbEDpOT_,"axG",@progbits,_ZNSt8_Rb_treeISt5tupleIJN2v88internal11interpreter17BytecodeGenerator17FeedbackSlotCache8SlotKindEiPKvEESt4pairIKS9_iESt10_Select1stISC_ESt4lessIS9_ENS2_13ZoneAllocatorISC_EEE17_M_emplace_uniqueIJRSA_IS9_iEEEESA_ISt17_Rb_tree_iteratorISC_EbEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeISt5tupleIJN2v88internal11interpreter17BytecodeGenerator17FeedbackSlotCache8SlotKindEiPKvEESt4pairIKS9_iESt10_Select1stISC_ESt4lessIS9_ENS2_13ZoneAllocatorISC_EEE17_M_emplace_uniqueIJRSA_IS9_iEEEESA_ISt17_Rb_tree_iteratorISC_EbEDpOT_
	.type	_ZNSt8_Rb_treeISt5tupleIJN2v88internal11interpreter17BytecodeGenerator17FeedbackSlotCache8SlotKindEiPKvEESt4pairIKS9_iESt10_Select1stISC_ESt4lessIS9_ENS2_13ZoneAllocatorISC_EEE17_M_emplace_uniqueIJRSA_IS9_iEEEESA_ISt17_Rb_tree_iteratorISC_EbEDpOT_, @function
_ZNSt8_Rb_treeISt5tupleIJN2v88internal11interpreter17BytecodeGenerator17FeedbackSlotCache8SlotKindEiPKvEESt4pairIKS9_iESt10_Select1stISC_ESt4lessIS9_ENS2_13ZoneAllocatorISC_EEE17_M_emplace_uniqueIJRSA_IS9_iEEEESA_ISt17_Rb_tree_iteratorISC_EbEDpOT_:
.LFB25177:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$55, %rax
	jbe	.L941
	leaq	56(%rbx), %rax
	movq	%rax, 16(%rdi)
.L914:
	movdqu	(%r14), %xmm0
	movl	16(%r14), %eax
	leaq	16(%r13), %r15
	movl	%eax, 48(%rbx)
	movups	%xmm0, 32(%rbx)
	movq	24(%r13), %r12
	testq	%r12, %r12
	je	.L915
	movl	44(%rbx), %r14d
	movl	44(%r12), %edx
	cmpl	%edx, %r14d
	jl	.L918
	.p2align 4,,10
	.p2align 3
.L942:
	jne	.L919
	movl	40(%r12), %eax
	cmpl	%eax, 40(%rbx)
	jl	.L918
	jne	.L919
	movq	32(%r12), %rax
	cmpq	%rax, 32(%rbx)
	jb	.L918
	.p2align 4,,10
	.p2align 3
.L919:
	movq	24(%r12), %rax
	xorl	%ecx, %ecx
	testq	%rax, %rax
	je	.L917
.L943:
	movq	%rax, %r12
	movl	44(%r12), %edx
	cmpl	%edx, %r14d
	jge	.L942
.L918:
	movq	16(%r12), %rax
	movl	$1, %ecx
	testq	%rax, %rax
	jne	.L943
.L917:
	testb	%cl, %cl
	jne	.L944
	movq	%r12, %rcx
	cmpl	%edx, %r14d
	jg	.L927
.L928:
	cmpl	%r14d, %edx
	jne	.L923
	movl	40(%rbx), %eax
	cmpl	%eax, 40(%r12)
	jge	.L945
.L924:
	testq	%rcx, %rcx
	jne	.L946
	xorl	%r12d, %r12d
.L923:
	addq	$8, %rsp
	movq	%r12, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L946:
	.cfi_restore_state
	movq	%rcx, %r12
	.p2align 4,,10
	.p2align 3
.L927:
	movl	$1, %edi
	cmpq	%r12, %r15
	jne	.L947
.L925:
	movq	%r12, %rdx
	movq	%r15, %rcx
	movq	%rbx, %rsi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 48(%r13)
	addq	$8, %rsp
	movq	%rbx, %rax
	movl	$1, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L944:
	.cfi_restore_state
	cmpq	%r12, 32(%r13)
	je	.L927
.L929:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movl	44(%rax), %edx
	cmpl	%edx, %r14d
	jg	.L927
	movq	%r12, %rcx
	movq	%rax, %r12
	jmp	.L928
	.p2align 4,,10
	.p2align 3
.L947:
	cmpl	44(%r12), %r14d
	jl	.L925
	movl	$0, %edi
	jne	.L925
	movl	40(%r12), %eax
	movl	$1, %edi
	cmpl	%eax, 40(%rbx)
	jl	.L925
	movl	$0, %edi
	jne	.L925
	xorl	%edi, %edi
	movq	32(%r12), %rax
	cmpq	%rax, 32(%rbx)
	setb	%dil
	jmp	.L925
	.p2align 4,,10
	.p2align 3
.L945:
	jne	.L923
	movq	32(%rbx), %rax
	cmpq	%rax, 32(%r12)
	jnb	.L923
	jmp	.L924
	.p2align 4,,10
	.p2align 3
.L915:
	movq	%r15, %r12
	cmpq	32(%r13), %r15
	je	.L937
	movl	44(%rbx), %r14d
	jmp	.L929
	.p2align 4,,10
	.p2align 3
.L941:
	movl	$56, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rbx
	jmp	.L914
	.p2align 4,,10
	.p2align 3
.L937:
	movl	$1, %edi
	jmp	.L925
	.cfi_endproc
.LFE25177:
	.size	_ZNSt8_Rb_treeISt5tupleIJN2v88internal11interpreter17BytecodeGenerator17FeedbackSlotCache8SlotKindEiPKvEESt4pairIKS9_iESt10_Select1stISC_ESt4lessIS9_ENS2_13ZoneAllocatorISC_EEE17_M_emplace_uniqueIJRSA_IS9_iEEEESA_ISt17_Rb_tree_iteratorISC_EbEDpOT_, .-_ZNSt8_Rb_treeISt5tupleIJN2v88internal11interpreter17BytecodeGenerator17FeedbackSlotCache8SlotKindEiPKvEESt4pairIKS9_iESt10_Select1stISC_ESt4lessIS9_ENS2_13ZoneAllocatorISC_EEE17_M_emplace_uniqueIJRSA_IS9_iEEEESA_ISt17_Rb_tree_iteratorISC_EbEDpOT_
	.section	.text._ZNKSt8_Rb_treeISt5tupleIJN2v88internal11interpreter17BytecodeGenerator17FeedbackSlotCache8SlotKindEiPKvEESt4pairIKS9_iESt10_Select1stISC_ESt4lessIS9_ENS2_13ZoneAllocatorISC_EEE4findERSB_,"axG",@progbits,_ZNKSt8_Rb_treeISt5tupleIJN2v88internal11interpreter17BytecodeGenerator17FeedbackSlotCache8SlotKindEiPKvEESt4pairIKS9_iESt10_Select1stISC_ESt4lessIS9_ENS2_13ZoneAllocatorISC_EEE4findERSB_,comdat
	.align 2
	.p2align 4
	.weak	_ZNKSt8_Rb_treeISt5tupleIJN2v88internal11interpreter17BytecodeGenerator17FeedbackSlotCache8SlotKindEiPKvEESt4pairIKS9_iESt10_Select1stISC_ESt4lessIS9_ENS2_13ZoneAllocatorISC_EEE4findERSB_
	.type	_ZNKSt8_Rb_treeISt5tupleIJN2v88internal11interpreter17BytecodeGenerator17FeedbackSlotCache8SlotKindEiPKvEESt4pairIKS9_iESt10_Select1stISC_ESt4lessIS9_ENS2_13ZoneAllocatorISC_EEE4findERSB_, @function
_ZNKSt8_Rb_treeISt5tupleIJN2v88internal11interpreter17BytecodeGenerator17FeedbackSlotCache8SlotKindEiPKvEESt4pairIKS9_iESt10_Select1stISC_ESt4lessIS9_ENS2_13ZoneAllocatorISC_EEE4findERSB_:
.LFB25186:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	leaq	16(%rdi), %r8
	testq	%rax, %rax
	je	.L949
	movl	12(%rsi), %ecx
	movq	%r8, %rdx
	.p2align 4,,10
	.p2align 3
.L950:
	cmpl	%ecx, 44(%rax)
	jl	.L952
	jne	.L953
	movl	8(%rsi), %edi
	cmpl	%edi, 40(%rax)
	jl	.L952
	jne	.L953
	movq	(%rsi), %rdi
	cmpq	%rdi, 32(%rax)
	jb	.L952
	.p2align 4,,10
	.p2align 3
.L953:
	movq	%rax, %rdx
	movq	16(%rax), %rax
.L954:
	testq	%rax, %rax
	jne	.L950
	cmpq	%rdx, %r8
	je	.L949
	cmpl	%ecx, 44(%rdx)
	jle	.L962
.L949:
	movq	%r8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L952:
	movq	24(%rax), %rax
	jmp	.L954
	.p2align 4,,10
	.p2align 3
.L962:
	jne	.L957
	movl	40(%rdx), %eax
	cmpl	%eax, 8(%rsi)
	jl	.L949
	jne	.L957
	movq	32(%rdx), %rax
	cmpq	%rax, (%rsi)
	cmovnb	%rdx, %r8
	jmp	.L949
	.p2align 4,,10
	.p2align 3
.L957:
	movq	%rdx, %r8
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE25186:
	.size	_ZNKSt8_Rb_treeISt5tupleIJN2v88internal11interpreter17BytecodeGenerator17FeedbackSlotCache8SlotKindEiPKvEESt4pairIKS9_iESt10_Select1stISC_ESt4lessIS9_ENS2_13ZoneAllocatorISC_EEE4findERSB_, .-_ZNKSt8_Rb_treeISt5tupleIJN2v88internal11interpreter17BytecodeGenerator17FeedbackSlotCache8SlotKindEiPKvEESt4pairIKS9_iESt10_Select1stISC_ESt4lessIS9_ENS2_13ZoneAllocatorISC_EEE4findERSB_
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator25GetCachedLoadGlobalICSlotENS0_10TypeofModeEPNS0_8VariableE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator25GetCachedLoadGlobalICSlotENS0_10TypeofModeEPNS0_8VariableE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator25GetCachedLoadGlobalICSlotENS0_10TypeofModeEPNS0_8VariableE, @function
_ZN2v88internal11interpreter17BytecodeGenerator25GetCachedLoadGlobalICSlotENS0_10TypeofModeEPNS0_8VariableE:
.LFB21038:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	552(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%esi, %esi
	movl	%esi, -84(%rbp)
	movq	%r15, %rsi
	sete	%r14b
	movq	%rbx, %rdi
	movq	%rdx, -80(%rbp)
	addq	$16, %rbx
	movzbl	%r14b, %r14d
	movl	$0, -72(%rbp)
	addl	$5, %r14d
	movl	%r14d, -68(%rbp)
	call	_ZNKSt8_Rb_treeISt5tupleIJN2v88internal11interpreter17BytecodeGenerator17FeedbackSlotCache8SlotKindEiPKvEESt4pairIKS9_iESt10_Select1stISC_ESt4lessIS9_ENS2_13ZoneAllocatorISC_EEE4findERSB_
	movl	-84(%rbp), %ecx
	cmpq	%rbx, %rax
	je	.L966
	movl	48(%rax), %eax
	cmpl	$-1, %eax
	je	.L966
.L967:
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L976
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L966:
	.cfi_restore_state
	movq	512(%r13), %rax
	xorl	%esi, %esi
	testl	%ecx, %ecx
	sete	%sil
	leaq	56(%rax), %rdi
	addl	$6, %esi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movq	552(%r13), %rdi
	movq	%r15, %rsi
	movq	%r12, -80(%rbp)
	movl	%eax, %ebx
	movl	$0, -72(%rbp)
	movl	%r14d, -68(%rbp)
	movl	%eax, -64(%rbp)
	call	_ZNSt8_Rb_treeISt5tupleIJN2v88internal11interpreter17BytecodeGenerator17FeedbackSlotCache8SlotKindEiPKvEESt4pairIKS9_iESt10_Select1stISC_ESt4lessIS9_ENS2_13ZoneAllocatorISC_EEE17_M_emplace_uniqueIJRSA_IS9_iEEEESA_ISt17_Rb_tree_iteratorISC_EbEDpOT_
	movl	%ebx, %eax
	jmp	.L967
.L976:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21038:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator25GetCachedLoadGlobalICSlotENS0_10TypeofModeEPNS0_8VariableE, .-_ZN2v88internal11interpreter17BytecodeGenerator25GetCachedLoadGlobalICSlotENS0_10TypeofModeEPNS0_8VariableE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator17BuildVariableLoadEPNS0_8VariableENS0_13HoleCheckModeENS0_10TypeofModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator17BuildVariableLoadEPNS0_8VariableENS0_13HoleCheckModeENS0_10TypeofModeE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator17BuildVariableLoadEPNS0_8VariableENS0_13HoleCheckModeENS0_10TypeofModeE, @function
_ZN2v88internal11interpreter17BytecodeGenerator17BuildVariableLoadEPNS0_8VariableENS0_13HoleCheckModeENS0_10TypeofModeE:
.LFB20890:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movzwl	40(%rsi), %ecx
	movl	%ecx, %eax
	shrl	$7, %eax
	andl	$7, %eax
	cmpb	$5, %al
	ja	.L977
	movl	%edx, %ebx
	movzbl	%al, %eax
	leaq	.L980(%rip), %rdx
	movq	%rdi, %r12
	movslq	(%rdx,%rax,4), %rax
	movq	%rsi, %r13
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal11interpreter17BytecodeGenerator17BuildVariableLoadEPNS0_8VariableENS0_13HoleCheckModeENS0_10TypeofModeE,"a",@progbits
	.align 4
	.align 4
.L980:
	.long	.L985-.L980
	.long	.L984-.L980
	.long	.L983-.L980
	.long	.L982-.L980
	.long	.L981-.L980
	.long	.L979-.L980
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator17BuildVariableLoadEPNS0_8VariableENS0_13HoleCheckModeENS0_10TypeofModeE
	.p2align 4,,10
	.p2align 3
.L981:
	andl	$15, %ecx
	leaq	24(%rdi), %r15
	cmpb	$5, %cl
	je	.L1001
	cmpb	$6, %cl
	jne	.L1002
	movq	16(%rsi), %rdx
	movq	808(%rdi), %rax
	movq	(%rdx), %rsi
	movq	8(%rax), %rdi
	movq	%rdx, -56(%rbp)
	call	_ZNK2v88internal5Scope18ContextChainLengthEPS1_@PLT
	movq	-56(%rbp), %rdx
	movq	8(%r13), %rsi
	movq	%r15, %rdi
	movl	%eax, %r8d
	movl	32(%rdx), %ecx
	movl	%r14d, %edx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder21LoadLookupContextSlotEPKNS0_12AstRawStringENS0_10TypeofModeEii@PLT
	testl	%ebx, %ebx
	je	.L1024
.L977:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L979:
	.cfi_restore_state
	movq	808(%rdi), %rax
	movq	(%rsi), %rsi
	addq	$24, %r12
	movq	8(%rax), %rdi
	call	_ZNK2v88internal5Scope18ContextChainLengthEPS1_@PLT
	movl	32(%r13), %esi
	movq	%r12, %rdi
	movl	%eax, %edx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder18LoadModuleVariableEii@PLT
	testl	%ebx, %ebx
	jne	.L977
	jmp	.L1019
	.p2align 4,,10
	.p2align 3
.L985:
	movq	520(%rdi), %rax
	movq	8(%rsi), %rbx
	leaq	24(%rdi), %r15
	cmpq	%rbx, 496(%rax)
	je	.L1025
	movq	%rsi, %rdx
	movl	%r14d, %esi
	call	_ZN2v88internal11interpreter17BytecodeGenerator25GetCachedLoadGlobalICSlotENS0_10TypeofModeEPNS0_8VariableE
	movq	8(%r13), %rsi
	addq	$24, %rsp
	movl	%r14d, %ecx
	popq	%rbx
	movq	%r15, %rdi
	popq	%r12
	movl	%eax, %edx
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder10LoadGlobalEPKNS0_12AstRawStringEiNS0_10TypeofModeE@PLT
	.p2align 4,,10
	.p2align 3
.L983:
	.cfi_restore_state
	addq	$24, %r12
	movl	32(%rsi), %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal11interpreter20BytecodeArrayBuilder5LocalEi@PLT
.L1023:
	movl	%eax, %esi
.L989:
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	testl	%ebx, %ebx
	jne	.L977
.L1019:
	movzwl	40(%r13), %eax
	sarl	$4, %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L1026
	movq	8(%r13), %rsi
	movq	%r12, %rdi
.L1016:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder25ThrowReferenceErrorIfHoleEPKNS0_12AstRawStringE@PLT
	.p2align 4,,10
	.p2align 3
.L982:
	.cfi_restore_state
	movq	808(%rdi), %rax
	movq	(%rsi), %rsi
	movq	8(%rax), %rdi
	call	_ZNK2v88internal5Scope18ContextChainLengthEPS1_@PLT
	movl	%eax, %ecx
	movq	808(%r12), %rax
	cmpl	%ecx, 28(%rax)
	jl	.L997
	testl	%ecx, %ecx
	jle	.L994
	movl	%ecx, %edx
	movq	%rax, %rsi
	.p2align 4,,10
	.p2align 3
.L996:
	movq	16(%rsi), %rsi
	subl	$1, %edx
	jne	.L996
	testq	%rsi, %rsi
	je	.L997
.L1008:
	movl	24(%rsi), %esi
	xorl	%ecx, %ecx
.L998:
	movzwl	40(%r13), %r8d
	addq	$24, %r12
	movl	32(%r13), %edx
	movq	%r12, %rdi
	sarl	$14, %r8d
	andl	$1, %r8d
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder15LoadContextSlotENS1_8RegisterEiiNS2_21ContextSlotMutabilityE@PLT
	testl	%ebx, %ebx
	jne	.L977
	jmp	.L1019
	.p2align 4,,10
	.p2align 3
.L984:
	movl	32(%rsi), %esi
	addq	$24, %r12
	movq	%r12, %rdi
	cmpl	$-1, %esi
	je	.L1027
	call	_ZNK2v88internal11interpreter20BytecodeArrayBuilder9ParameterEi@PLT
	jmp	.L1023
	.p2align 4,,10
	.p2align 3
.L1026:
	movq	%r12, %rdi
.L1017:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder25ThrowSuperNotCalledIfHoleEv@PLT
	.p2align 4,,10
	.p2align 3
.L997:
	.cfi_restore_state
	movl	24(%rax), %esi
	jmp	.L998
	.p2align 4,,10
	.p2align 3
.L1024:
	movzwl	40(%r13), %eax
	movq	%r15, %rdi
	sarl	$4, %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L1017
	movq	8(%r13), %rsi
	jmp	.L1016
	.p2align 4,,10
	.p2align 3
.L1027:
	call	_ZNK2v88internal11interpreter20BytecodeArrayBuilder8ReceiverEv@PLT
	movl	%eax, %esi
	jmp	.L989
	.p2align 4,,10
	.p2align 3
.L1001:
	movq	536(%rdi), %rdi
	call	_ZNK2v88internal5Scope42ContextChainLengthUntilOutermostSloppyEvalEv@PLT
	movq	512(%r12), %rdi
	xorl	%esi, %esi
	movl	%eax, %ebx
	addq	$56, %rdi
	testl	%r14d, %r14d
	sete	%sil
	addl	$6, %esi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movq	8(%r13), %rsi
	addq	$24, %rsp
	movl	%ebx, %r8d
	movl	%r14d, %edx
	popq	%rbx
	movq	%r15, %rdi
	popq	%r12
	movl	%eax, %ecx
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder20LoadLookupGlobalSlotEPKNS0_12AstRawStringENS0_10TypeofModeEii@PLT
	.p2align 4,,10
	.p2align 3
.L1002:
	.cfi_restore_state
	movq	8(%rsi), %rsi
	addq	$24, %rsp
	movl	%r14d, %edx
	movq	%r15, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder14LoadLookupSlotEPKNS0_12AstRawStringENS0_10TypeofModeE@PLT
	.p2align 4,,10
	.p2align 3
.L1025:
	.cfi_restore_state
	addq	$24, %rsp
	movq	%r15, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder13LoadUndefinedEv@PLT
	.p2align 4,,10
	.p2align 3
.L994:
	.cfi_restore_state
	movq	%rax, %rsi
	jmp	.L1008
	.cfi_endproc
.LFE20890:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator17BuildVariableLoadEPNS0_8VariableENS0_13HoleCheckModeENS0_10TypeofModeE, .-_ZN2v88internal11interpreter17BytecodeGenerator17BuildVariableLoadEPNS0_8VariableENS0_13HoleCheckModeENS0_10TypeofModeE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator31BuildPrivateBrandInitializationENS1_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator31BuildPrivateBrandInitializationENS1_8RegisterE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator31BuildPrivateBrandInitializationENS1_8RegisterE, @function
_ZN2v88internal11interpreter17BytecodeGenerator31BuildPrivateBrandInitializationENS1_8RegisterE:
.LFB20829:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	328(%rdi), %r14d
	movq	%rdi, %rbx
	leal	2(%r14), %eax
	cmpl	%eax, 332(%rdi)
	movq	%r14, %r12
	movl	%eax, 328(%rdi)
	cmovge	332(%rdi), %eax
	movl	%eax, 332(%rdi)
	movq	336(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1029
	movq	(%rdi), %rax
	movq	%r14, %rsi
	btsq	$33, %rsi
	call	*24(%rax)
.L1029:
	movq	512(%rbx), %rdi
	btsq	$33, %r14
	call	_ZNK2v88internal26UnoptimizedCompilationInfo5scopeEv@PLT
	movq	8(%rax), %rdi
	call	_ZN2v88internal5Scope12AsClassScopeEv@PLT
	movq	136(%rax), %rsi
	testq	%rsi, %rsi
	je	.L1030
	movq	40(%rsi), %rsi
.L1030:
	movl	$1, %ecx
	movl	$1, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator17BuildVariableLoadEPNS0_8VariableENS0_13HoleCheckModeENS0_10TypeofModeE
	leal	1(%r12), %esi
	leaq	24(%rbx), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movl	%r12d, %edx
	movl	%r13d, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder12MoveRegisterENS1_8RegisterES3_@PLT
	popq	%rbx
	movq	%r14, %rdx
	popq	%r12
	movq	%rax, %rdi
	popq	%r13
	movl	$205, %esi
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_12RegisterListE@PLT
	.cfi_endproc
.LFE20829:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator31BuildPrivateBrandInitializationENS1_8RegisterE, .-_ZN2v88internal11interpreter17BytecodeGenerator31BuildPrivateBrandInitializationENS1_8RegisterE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator18VisitVariableProxyEPNS0_13VariableProxyE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator18VisitVariableProxyEPNS0_13VariableProxyE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator18VisitVariableProxyEPNS0_13VariableProxyE, @function
_ZN2v88internal11interpreter17BytecodeGenerator18VisitVariableProxyEPNS0_13VariableProxyE:
.LFB20889:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	cmpl	$-1, %eax
	je	.L1039
	cmpb	$2, 496(%rdi)
	je	.L1039
	movb	$1, 496(%rdi)
	movl	%eax, 500(%rdi)
.L1039:
	movl	4(%rsi), %edx
	movq	8(%rsi), %rsi
	movl	$1, %ecx
	shrl	$11, %edx
	andl	$1, %edx
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator17BuildVariableLoadEPNS0_8VariableENS0_13HoleCheckModeENS0_10TypeofModeE
	.cfi_endproc
.LFE20889:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator18VisitVariableProxyEPNS0_13VariableProxyE, .-_ZN2v88internal11interpreter17BytecodeGenerator18VisitVariableProxyEPNS0_13VariableProxyE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator36BuildVariableLoadForAccumulatorValueEPNS0_8VariableENS0_13HoleCheckModeENS0_10TypeofModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator36BuildVariableLoadForAccumulatorValueEPNS0_8VariableENS0_13HoleCheckModeENS0_10TypeofModeE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator36BuildVariableLoadForAccumulatorValueEPNS0_8VariableENS0_13HoleCheckModeENS0_10TypeofModeE, @function
_ZN2v88internal11interpreter17BytecodeGenerator36BuildVariableLoadForAccumulatorValueEPNS0_8VariableENS0_13HoleCheckModeENS0_10TypeofModeE:
.LFB20891:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	816(%rdi), %rax
	movq	%rdi, -40(%rbp)
	movq	$2, -24(%rbp)
	movq	%rax, -48(%rbp)
	movl	328(%rdi), %eax
	movl	%eax, -32(%rbp)
	leaq	-48(%rbp), %rax
	movq	%rax, 816(%rdi)
	call	_ZN2v88internal11interpreter17BytecodeGenerator17BuildVariableLoadEPNS0_8VariableENS0_13HoleCheckModeENS0_10TypeofModeE
	movq	-40(%rbp), %rdx
	movq	-48(%rbp), %rax
	movq	336(%rdx), %rdi
	movq	%rax, 816(%rdx)
	movl	-32(%rbp), %eax
	movl	328(%rdx), %esi
	movl	%eax, 328(%rdx)
	testq	%rdi, %rdi
	je	.L1043
	subl	%eax, %esi
	movq	(%rdi), %rcx
	movq	%rsi, %rdx
	movl	%eax, %esi
	salq	$32, %rdx
	orq	%rdx, %rsi
	call	*32(%rcx)
.L1043:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1050
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1050:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20891:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator36BuildVariableLoadForAccumulatorValueEPNS0_8VariableENS0_13HoleCheckModeENS0_10TypeofModeE, .-_ZN2v88internal11interpreter17BytecodeGenerator36BuildVariableLoadForAccumulatorValueEPNS0_8VariableENS0_13HoleCheckModeENS0_10TypeofModeE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator21BuildThisVariableLoadEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator21BuildThisVariableLoadEv
	.type	_ZN2v88internal11interpreter17BytecodeGenerator21BuildThisVariableLoadEv, @function
_ZN2v88internal11interpreter17BytecodeGenerator21BuildThisVariableLoadEv:
.LFB20979:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	528(%rdi), %rdi
	call	_ZN2v88internal5Scope16GetReceiverScopeEv@PLT
	movq	%r12, %rdi
	movzbl	133(%rax), %ecx
	movq	176(%rax), %rsi
	leal	-4(%rcx), %edx
	movl	$1, %ecx
	cmpb	$1, %dl
	seta	%dl
	addq	$8, %rsp
	popq	%r12
	movzbl	%dl, %edx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator17BuildVariableLoadEPNS0_8VariableENS0_13HoleCheckModeENS0_10TypeofModeE
	.cfi_endproc
.LFE20979:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator21BuildThisVariableLoadEv, .-_ZN2v88internal11interpreter17BytecodeGenerator21BuildThisVariableLoadEv
	.section	.text.unlikely._ZN2v88internal11interpreter17BytecodeGenerator22BuildPrivateBrandCheckEPNS0_8PropertyENS1_8RegisterE,"ax",@progbits
	.align 2
.LCOLDB8:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator22BuildPrivateBrandCheckEPNS0_8PropertyENS1_8RegisterE,"ax",@progbits
.LHOTB8:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator22BuildPrivateBrandCheckEPNS0_8PropertyENS1_8RegisterE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator22BuildPrivateBrandCheckEPNS0_8PropertyENS1_8RegisterE, @function
_ZN2v88internal11interpreter17BytecodeGenerator22BuildPrivateBrandCheckEPNS0_8PropertyENS1_8RegisterE:
.LFB20930:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	16(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	4(%rdx), %eax
	andl	$63, %eax
	cmpb	$54, %al
	jne	.L1054
	movq	8(%rdx), %rax
	movq	%rdi, %rbx
	movq	%rsi, %r12
	movq	(%rax), %rdi
	call	_ZN2v88internal5Scope12AsClassScopeEv@PLT
	movq	136(%rax), %rsi
	testq	%rsi, %rsi
	je	.L1055
	movq	40(%rsi), %rsi
.L1055:
	movq	816(%rbx), %rax
	movl	$1, %edx
	movq	%rbx, %rdi
	movq	%rbx, -72(%rbp)
	movl	$1, %ecx
	movq	$2, -56(%rbp)
	movq	%rax, -80(%rbp)
	movl	328(%rbx), %eax
	movl	%eax, -64(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rax, 816(%rbx)
	call	_ZN2v88internal11interpreter17BytecodeGenerator17BuildVariableLoadEPNS0_8VariableENS0_13HoleCheckModeENS0_10TypeofModeE
	movq	-72(%rbp), %rdx
	movq	-80(%rbp), %rax
	movq	336(%rdx), %rdi
	movq	%rax, 816(%rdx)
	movl	-64(%rbp), %eax
	movl	328(%rdx), %esi
	movl	%eax, 328(%rdx)
	testq	%rdi, %rdi
	je	.L1056
	subl	%eax, %esi
	movq	(%rdi), %rcx
	salq	$32, %rsi
	movq	%rsi, %rdx
	movl	%eax, %esi
	orq	%rdx, %rsi
	call	*32(%rcx)
.L1056:
	movl	(%r12), %eax
	leaq	24(%rbx), %r14
	cmpl	$-1, %eax
	je	.L1057
	cmpb	$2, 496(%rbx)
	je	.L1057
	movb	$1, 496(%rbx)
	movl	%eax, 500(%rbx)
.L1057:
	movq	512(%rbx), %rdi
	movl	$8, %esi
	addq	$56, %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movl	%r13d, %esi
	movq	%r14, %rdi
	movl	%eax, %edx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder17LoadKeyedPropertyENS1_8RegisterEi@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1070
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1070:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal11interpreter17BytecodeGenerator22BuildPrivateBrandCheckEPNS0_8PropertyENS1_8RegisterE
	.cfi_startproc
	.type	_ZN2v88internal11interpreter17BytecodeGenerator22BuildPrivateBrandCheckEPNS0_8PropertyENS1_8RegisterE.cold, @function
_ZN2v88internal11interpreter17BytecodeGenerator22BuildPrivateBrandCheckEPNS0_8PropertyENS1_8RegisterE.cold:
.LFSB20930:
.L1054:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movq	8, %rax
	ud2
	.cfi_endproc
.LFE20930:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator22BuildPrivateBrandCheckEPNS0_8PropertyENS1_8RegisterE
	.size	_ZN2v88internal11interpreter17BytecodeGenerator22BuildPrivateBrandCheckEPNS0_8PropertyENS1_8RegisterE, .-_ZN2v88internal11interpreter17BytecodeGenerator22BuildPrivateBrandCheckEPNS0_8PropertyENS1_8RegisterE
	.section	.text.unlikely._ZN2v88internal11interpreter17BytecodeGenerator22BuildPrivateBrandCheckEPNS0_8PropertyENS1_8RegisterE
	.size	_ZN2v88internal11interpreter17BytecodeGenerator22BuildPrivateBrandCheckEPNS0_8PropertyENS1_8RegisterE.cold, .-_ZN2v88internal11interpreter17BytecodeGenerator22BuildPrivateBrandCheckEPNS0_8PropertyENS1_8RegisterE.cold
.LCOLDE8:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator22BuildPrivateBrandCheckEPNS0_8PropertyENS1_8RegisterE
.LHOTE8:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator19VisitThisExpressionEPNS0_14ThisExpressionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator19VisitThisExpressionEPNS0_14ThisExpressionE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator19VisitThisExpressionEPNS0_14ThisExpressionE, @function
_ZN2v88internal11interpreter17BytecodeGenerator19VisitThisExpressionEPNS0_14ThisExpressionE:
.LFB20980:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	528(%rdi), %rdi
	call	_ZN2v88internal5Scope16GetReceiverScopeEv@PLT
	movq	%r12, %rdi
	movzbl	133(%rax), %ecx
	movq	176(%rax), %rsi
	leal	-4(%rcx), %edx
	movl	$1, %ecx
	cmpb	$1, %dl
	seta	%dl
	addq	$8, %rsp
	popq	%r12
	movzbl	%dl, %edx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator17BuildVariableLoadEPNS0_8VariableENS0_13HoleCheckModeENS0_10TypeofModeE
	.cfi_endproc
.LFE20980:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator19VisitThisExpressionEPNS0_14ThisExpressionE, .-_ZN2v88internal11interpreter17BytecodeGenerator19VisitThisExpressionEPNS0_14ThisExpressionE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator26GetCachedStoreGlobalICSlotENS0_12LanguageModeEPNS0_8VariableE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator26GetCachedStoreGlobalICSlotENS0_12LanguageModeEPNS0_8VariableE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator26GetCachedStoreGlobalICSlotENS0_12LanguageModeEPNS0_8VariableE, @function
_ZN2v88internal11interpreter17BytecodeGenerator26GetCachedStoreGlobalICSlotENS0_12LanguageModeEPNS0_8VariableE:
.LFB21039:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r15
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movzbl	%sil, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	552(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%esi, -84(%rbp)
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%rdx, -80(%rbp)
	addq	$16, %rbx
	movl	$0, -72(%rbp)
	movl	%r13d, -68(%rbp)
	call	_ZNKSt8_Rb_treeISt5tupleIJN2v88internal11interpreter17BytecodeGenerator17FeedbackSlotCache8SlotKindEiPKvEESt4pairIKS9_iESt10_Select1stISC_ESt4lessIS9_ENS2_13ZoneAllocatorISC_EEE4findERSB_
	movl	-84(%rbp), %ecx
	cmpq	%rbx, %rax
	je	.L1075
	movl	48(%rax), %eax
	cmpl	$-1, %eax
	je	.L1075
.L1076:
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L1084
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1075:
	.cfi_restore_state
	cmpb	$1, %cl
	movq	512(%r14), %rax
	sbbl	%esi, %esi
	andl	$-9, %esi
	leaq	56(%rax), %rdi
	addl	$10, %esi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movq	552(%r14), %rdi
	movq	%r15, %rsi
	movq	%r12, -80(%rbp)
	movl	%eax, %ebx
	movl	$0, -72(%rbp)
	movl	%r13d, -68(%rbp)
	movl	%eax, -64(%rbp)
	call	_ZNSt8_Rb_treeISt5tupleIJN2v88internal11interpreter17BytecodeGenerator17FeedbackSlotCache8SlotKindEiPKvEESt4pairIKS9_iESt10_Select1stISC_ESt4lessIS9_ENS2_13ZoneAllocatorISC_EEE17_M_emplace_uniqueIJRSA_IS9_iEEEESA_ISt17_Rb_tree_iteratorISC_EbEDpOT_
	movl	%ebx, %eax
	jmp	.L1076
.L1084:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21039:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator26GetCachedStoreGlobalICSlotENS0_12LanguageModeEPNS0_8VariableE, .-_ZN2v88internal11interpreter17BytecodeGenerator26GetCachedStoreGlobalICSlotENS0_12LanguageModeEPNS0_8VariableE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator19GetCachedLoadICSlotEPKNS0_10ExpressionEPKNS0_12AstRawStringE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator19GetCachedLoadICSlotEPKNS0_10ExpressionEPKNS0_12AstRawStringE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator19GetCachedLoadICSlotEPKNS0_10ExpressionEPKNS0_12AstRawStringE, @function
_ZN2v88internal11interpreter17BytecodeGenerator19GetCachedLoadICSlotEPKNS0_10ExpressionEPKNS0_12AstRawStringE:
.LFB21040:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal43FLAG_ignition_share_named_property_feedbackE(%rip)
	je	.L1094
	movzbl	4(%rsi), %eax
	movq	%rsi, %r14
	andl	$63, %eax
	cmpb	$54, %al
	je	.L1088
.L1094:
	movq	512(%r12), %rdi
	movl	$5, %esi
	addq	$56, %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
.L1087:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1095
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1088:
	.cfi_restore_state
	movq	8(%rsi), %rax
	movq	552(%rdi), %rbx
	leaq	-80(%rbp), %r15
	movq	%rdx, %r13
	movq	%r15, %rsi
	movl	32(%rax), %eax
	movq	%rbx, %rdi
	movq	%rdx, -80(%rbp)
	addq	$16, %rbx
	movl	$4, -68(%rbp)
	movl	%eax, -72(%rbp)
	call	_ZNKSt8_Rb_treeISt5tupleIJN2v88internal11interpreter17BytecodeGenerator17FeedbackSlotCache8SlotKindEiPKvEESt4pairIKS9_iESt10_Select1stISC_ESt4lessIS9_ENS2_13ZoneAllocatorISC_EEE4findERSB_
	cmpq	%rbx, %rax
	je	.L1090
	movl	48(%rax), %ebx
	cmpl	$-1, %ebx
	je	.L1090
	movl	%ebx, %eax
	jmp	.L1087
	.p2align 4,,10
	.p2align 3
.L1090:
	movq	512(%r12), %rax
	movl	$5, %esi
	leaq	56(%rax), %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movq	552(%r12), %rdi
	movq	%r15, %rsi
	movl	%eax, %ebx
	movq	8(%r14), %rax
	movl	32(%rax), %eax
	movq	%r13, -80(%rbp)
	movl	$4, -68(%rbp)
	movl	%eax, -72(%rbp)
	movl	%ebx, -64(%rbp)
	call	_ZNSt8_Rb_treeISt5tupleIJN2v88internal11interpreter17BytecodeGenerator17FeedbackSlotCache8SlotKindEiPKvEESt4pairIKS9_iESt10_Select1stISC_ESt4lessIS9_ENS2_13ZoneAllocatorISC_EEE17_M_emplace_uniqueIJRSA_IS9_iEEEESA_ISt17_Rb_tree_iteratorISC_EbEDpOT_
	movl	%ebx, %eax
	jmp	.L1087
.L1095:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21040:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator19GetCachedLoadICSlotEPKNS0_10ExpressionEPKNS0_12AstRawStringE, .-_ZN2v88internal11interpreter17BytecodeGenerator19GetCachedLoadICSlotEPKNS0_10ExpressionEPKNS0_12AstRawStringE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator23BuildStoreNamedPropertyEPKNS0_10ExpressionENS1_8RegisterEPKNS0_12AstRawStringE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator23BuildStoreNamedPropertyEPKNS0_10ExpressionENS1_8RegisterEPKNS0_12AstRawStringE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator23BuildStoreNamedPropertyEPKNS0_10ExpressionENS1_8RegisterEPKNS0_12AstRawStringE, @function
_ZN2v88internal11interpreter17BytecodeGenerator23BuildStoreNamedPropertyEPKNS0_10ExpressionENS1_8RegisterEPKNS0_12AstRawStringE:
.LFB20902:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	24(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	$2147483647, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	816(%rdi), %rax
	cmpl	$1, 24(%rax)
	jne	.L1128
.L1097:
	cmpb	$0, _ZN2v88internal33FLAG_enable_one_shot_optimizationE(%rip)
	je	.L1099
	movl	868(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L1099
	movq	512(%rbx), %rax
	movq	16(%rax), %rax
	movl	28(%rax), %edx
	testl	%edx, %edx
	je	.L1100
	testb	$64, 6(%rax)
	je	.L1099
.L1100:
	movq	536(%rbx), %rax
	movq	%r13, %rdx
	movl	%r14d, %esi
	movq	%r15, %rdi
	movzbl	129(%rax), %ecx
	andl	$1, %ecx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder28StoreNamedPropertyNoFeedbackENS1_8RegisterEPKNS0_12AstRawStringENS0_12LanguageModeE@PLT
	jmp	.L1101
	.p2align 4,,10
	.p2align 3
.L1099:
	movq	536(%rbx), %rax
	movzbl	129(%rax), %eax
	andl	$1, %eax
	cmpb	$0, _ZN2v88internal43FLAG_ignition_share_named_property_feedbackE(%rip)
	jne	.L1102
	cmpb	$1, %al
	movq	512(%rbx), %rcx
	sbbl	%esi, %esi
	andl	$-9, %esi
	leaq	56(%rcx), %rdi
	addl	$11, %esi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movl	%eax, %ecx
.L1104:
	movq	536(%rbx), %rax
	movq	%r13, %rdx
	movl	%r14d, %esi
	movq	%r15, %rdi
	movzbl	129(%rax), %r8d
	andl	$1, %r8d
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder18StoreNamedPropertyENS1_8RegisterEPKNS0_12AstRawStringEiNS0_12LanguageModeE@PLT
.L1101:
	movq	816(%rbx), %rax
	cmpl	$1, 24(%rax)
	je	.L1096
	movl	%r12d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
.L1096:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1129
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1102:
	.cfi_restore_state
	movzbl	4(%r8), %edx
	andl	$63, %edx
	testb	%al, %al
	je	.L1130
	cmpb	$54, %dl
	je	.L1131
	movq	512(%rbx), %rax
	movl	$11, %esi
	leaq	56(%rax), %rdi
.L1112:
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movl	%eax, %ecx
	jmp	.L1104
	.p2align 4,,10
	.p2align 3
.L1128:
	movl	328(%rdi), %r12d
	leal	1(%r12), %eax
	cmpl	%eax, 332(%rdi)
	movl	%eax, 328(%rdi)
	cmovge	332(%rdi), %eax
	movl	%eax, 332(%rdi)
	movq	336(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1098
	movq	(%rdi), %rax
	movq	%rsi, -88(%rbp)
	movl	%r12d, %esi
	call	*16(%rax)
	movq	-88(%rbp), %r8
.L1098:
	movl	%r12d, %esi
	movq	%r15, %rdi
	movq	%r8, -88(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-88(%rbp), %r8
	jmp	.L1097
	.p2align 4,,10
	.p2align 3
.L1130:
	cmpb	$54, %dl
	je	.L1132
	movq	512(%rbx), %rax
	movl	$2, %esi
	leaq	56(%rax), %rdi
	jmp	.L1112
	.p2align 4,,10
	.p2align 3
.L1132:
	movl	$3, -88(%rbp)
.L1114:
	movq	8(%r8), %rax
	movq	552(%rbx), %rdx
	leaq	-80(%rbp), %r10
	movq	%r8, -112(%rbp)
	movq	%r10, %rsi
	movq	%r10, -104(%rbp)
	movl	32(%rax), %eax
	movq	%rdx, %rdi
	movq	%rdx, -96(%rbp)
	movq	%r13, -80(%rbp)
	movl	%eax, -72(%rbp)
	movl	-88(%rbp), %eax
	movl	%eax, -68(%rbp)
	call	_ZNKSt8_Rb_treeISt5tupleIJN2v88internal11interpreter17BytecodeGenerator17FeedbackSlotCache8SlotKindEiPKvEESt4pairIKS9_iESt10_Select1stISC_ESt4lessIS9_ENS2_13ZoneAllocatorISC_EEE4findERSB_
	movq	-96(%rbp), %rdx
	movq	-104(%rbp), %r10
	movq	-112(%rbp), %r8
	addq	$16, %rdx
	cmpq	%rdx, %rax
	je	.L1109
	movl	48(%rax), %ecx
	cmpl	$-1, %ecx
	jne	.L1104
.L1109:
	movq	512(%rbx), %rax
	movq	%r10, -104(%rbp)
	movq	%r8, -96(%rbp)
	leaq	56(%rax), %rdi
	movq	536(%rbx), %rax
	movzbl	129(%rax), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%esi, %esi
	andl	$-9, %esi
	addl	$11, %esi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movq	-96(%rbp), %r8
	movq	-104(%rbp), %r10
	movl	%eax, %ecx
	movq	552(%rbx), %rdi
	movq	8(%r8), %rax
	movq	%r10, %rsi
	movl	32(%rax), %eax
	movl	%ecx, -64(%rbp)
	movq	%r13, -80(%rbp)
	movl	%eax, -72(%rbp)
	movl	-88(%rbp), %eax
	movl	%ecx, -88(%rbp)
	movl	%eax, -68(%rbp)
	call	_ZNSt8_Rb_treeISt5tupleIJN2v88internal11interpreter17BytecodeGenerator17FeedbackSlotCache8SlotKindEiPKvEESt4pairIKS9_iESt10_Select1stISC_ESt4lessIS9_ENS2_13ZoneAllocatorISC_EEE17_M_emplace_uniqueIJRSA_IS9_iEEEESA_ISt17_Rb_tree_iteratorISC_EbEDpOT_
	movl	-88(%rbp), %ecx
	jmp	.L1104
	.p2align 4,,10
	.p2align 3
.L1131:
	movl	$2, -88(%rbp)
	jmp	.L1114
.L1129:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20902:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator23BuildStoreNamedPropertyEPKNS0_10ExpressionENS1_8RegisterEPKNS0_12AstRawStringE, .-_ZN2v88internal11interpreter17BytecodeGenerator23BuildStoreNamedPropertyEPKNS0_10ExpressionENS1_8RegisterEPKNS0_12AstRawStringE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator26GetCachedCreateClosureSlotEPNS0_15FunctionLiteralE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator26GetCachedCreateClosureSlotEPNS0_15FunctionLiteralE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator26GetCachedCreateClosureSlotEPNS0_15FunctionLiteralE, @function
_ZN2v88internal11interpreter17BytecodeGenerator26GetCachedCreateClosureSlotEPNS0_15FunctionLiteralE:
.LFB21042:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-64(%rbp), %r14
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	552(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -64(%rbp)
	movq	%r14, %rsi
	movabsq	$30064771072, %rax
	movq	%r12, %rdi
	movq	%rax, -56(%rbp)
	addq	$16, %r12
	call	_ZNKSt8_Rb_treeISt5tupleIJN2v88internal11interpreter17BytecodeGenerator17FeedbackSlotCache8SlotKindEiPKvEESt4pairIKS9_iESt10_Select1stISC_ESt4lessIS9_ENS2_13ZoneAllocatorISC_EEE4findERSB_
	cmpq	%r12, %rax
	je	.L1135
	movl	48(%rax), %r12d
	cmpl	$-1, %r12d
	je	.L1135
.L1133:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1139
	addq	$32, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1135:
	.cfi_restore_state
	movq	512(%rbx), %rax
	movq	%r14, %rsi
	movl	88(%rax), %r12d
	leal	1(%r12), %edx
	movl	%edx, 88(%rax)
	movq	552(%rbx), %rdi
	movabsq	$30064771072, %rax
	movq	%r13, -64(%rbp)
	movq	%rax, -56(%rbp)
	movl	%r12d, -48(%rbp)
	call	_ZNSt8_Rb_treeISt5tupleIJN2v88internal11interpreter17BytecodeGenerator17FeedbackSlotCache8SlotKindEiPKvEESt4pairIKS9_iESt10_Select1stISC_ESt4lessIS9_ENS2_13ZoneAllocatorISC_EEE17_M_emplace_uniqueIJRSA_IS9_iEEEESA_ISt17_Rb_tree_iteratorISC_EbEDpOT_
	jmp	.L1133
.L1139:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21042:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator26GetCachedCreateClosureSlotEPNS0_15FunctionLiteralE, .-_ZN2v88internal11interpreter17BytecodeGenerator26GetCachedCreateClosureSlotEPNS0_15FunctionLiteralE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator20GetCachedStoreICSlotEPKNS0_10ExpressionEPKNS0_12AstRawStringE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator20GetCachedStoreICSlotEPKNS0_10ExpressionEPKNS0_12AstRawStringE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator20GetCachedStoreICSlotEPKNS0_10ExpressionEPKNS0_12AstRawStringE, @function
_ZN2v88internal11interpreter17BytecodeGenerator20GetCachedStoreICSlotEPKNS0_10ExpressionEPKNS0_12AstRawStringE:
.LFB21041:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	536(%rdi), %rax
	movzbl	129(%rax), %eax
	andl	$1, %eax
	cmpb	$0, _ZN2v88internal43FLAG_ignition_share_named_property_feedbackE(%rip)
	jne	.L1141
	movq	512(%rdi), %rdi
	addq	$56, %rdi
	cmpb	$1, %al
	sbbl	%esi, %esi
	andl	$-9, %esi
	addl	$11, %esi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
.L1143:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1160
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1141:
	.cfi_restore_state
	movzbl	4(%rsi), %ecx
	movq	%rsi, %r13
	andl	$63, %ecx
	testb	%al, %al
	je	.L1161
	movl	$2, %r14d
	cmpb	$54, %cl
	je	.L1152
	movq	512(%rbx), %rdi
	movl	$11, %esi
	addq	$56, %rdi
.L1150:
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movl	%eax, %r12d
.L1146:
	movl	%r12d, %eax
	jmp	.L1143
	.p2align 4,,10
	.p2align 3
.L1161:
	movl	$3, %r14d
	cmpb	$54, %cl
	je	.L1152
	movq	512(%rbx), %rdi
	movl	$2, %esi
	addq	$56, %rdi
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1152:
	movq	8(%r13), %rax
	movq	552(%rbx), %r12
	leaq	-80(%rbp), %r15
	movq	%rdx, -88(%rbp)
	movq	%r15, %rsi
	movl	32(%rax), %eax
	movq	%r12, %rdi
	movq	%rdx, -80(%rbp)
	addq	$16, %r12
	movl	%r14d, -68(%rbp)
	movl	%eax, -72(%rbp)
	call	_ZNKSt8_Rb_treeISt5tupleIJN2v88internal11interpreter17BytecodeGenerator17FeedbackSlotCache8SlotKindEiPKvEESt4pairIKS9_iESt10_Select1stISC_ESt4lessIS9_ENS2_13ZoneAllocatorISC_EEE4findERSB_
	movq	-88(%rbp), %rdx
	cmpq	%r12, %rax
	je	.L1148
	movl	48(%rax), %r12d
	cmpl	$-1, %r12d
	jne	.L1146
.L1148:
	movq	512(%rbx), %rax
	movq	%rdx, -88(%rbp)
	leaq	56(%rax), %rdi
	movq	536(%rbx), %rax
	movzbl	129(%rax), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%esi, %esi
	andl	$-9, %esi
	addl	$11, %esi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movq	-88(%rbp), %rdx
	movq	552(%rbx), %rdi
	movq	%r15, %rsi
	movl	%eax, %r12d
	movq	8(%r13), %rax
	movl	32(%rax), %eax
	movq	%rdx, -80(%rbp)
	movl	%r14d, -68(%rbp)
	movl	%eax, -72(%rbp)
	movl	%r12d, -64(%rbp)
	call	_ZNSt8_Rb_treeISt5tupleIJN2v88internal11interpreter17BytecodeGenerator17FeedbackSlotCache8SlotKindEiPKvEESt4pairIKS9_iESt10_Select1stISC_ESt4lessIS9_ENS2_13ZoneAllocatorISC_EEE17_M_emplace_uniqueIJRSA_IS9_iEEEESA_ISt17_Rb_tree_iteratorISC_EbEDpOT_
	jmp	.L1146
.L1160:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21041:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator20GetCachedStoreICSlotEPKNS0_10ExpressionEPKNS0_12AstRawStringE, .-_ZN2v88internal11interpreter17BytecodeGenerator20GetCachedStoreICSlotEPKNS0_10ExpressionEPKNS0_12AstRawStringE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator22BuildLoadNamedPropertyEPKNS0_10ExpressionENS1_8RegisterEPKNS0_12AstRawStringE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator22BuildLoadNamedPropertyEPKNS0_10ExpressionENS1_8RegisterEPKNS0_12AstRawStringE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator22BuildLoadNamedPropertyEPKNS0_10ExpressionENS1_8RegisterEPKNS0_12AstRawStringE, @function
_ZN2v88internal11interpreter17BytecodeGenerator22BuildLoadNamedPropertyEPKNS0_10ExpressionENS1_8RegisterEPKNS0_12AstRawStringE:
.LFB20901:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal33FLAG_enable_one_shot_optimizationE(%rip)
	je	.L1163
	movl	868(%rdi), %ecx
	testl	%ecx, %ecx
	jg	.L1163
	movq	512(%rdi), %rax
	movq	16(%rax), %rax
	movl	28(%rax), %edx
	testl	%edx, %edx
	je	.L1164
	testb	$64, 6(%rax)
	je	.L1163
.L1164:
	leaq	24(%rbx), %rdi
	movq	%r12, %rdx
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadNamedPropertyNoFeedbackENS1_8RegisterEPKNS0_12AstRawStringE@PLT
	jmp	.L1162
	.p2align 4,,10
	.p2align 3
.L1163:
	cmpb	$0, _ZN2v88internal43FLAG_ignition_share_named_property_feedbackE(%rip)
	je	.L1177
	movzbl	4(%r14), %eax
	andl	$63, %eax
	cmpb	$54, %al
	je	.L1168
.L1177:
	movq	512(%rbx), %rax
	movl	$5, %esi
	leaq	56(%rax), %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movl	%eax, %ecx
.L1167:
	leaq	24(%rbx), %rdi
	movq	%r12, %rdx
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder17LoadNamedPropertyENS1_8RegisterEPKNS0_12AstRawStringEi@PLT
.L1162:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1178
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1168:
	.cfi_restore_state
	movq	8(%r14), %rax
	movq	552(%rbx), %r15
	leaq	-80(%rbp), %r8
	movq	%r8, %rsi
	movq	%r8, -88(%rbp)
	movl	32(%rax), %eax
	movq	%r15, %rdi
	movq	%r12, -80(%rbp)
	addq	$16, %r15
	movl	$4, -68(%rbp)
	movl	%eax, -72(%rbp)
	call	_ZNKSt8_Rb_treeISt5tupleIJN2v88internal11interpreter17BytecodeGenerator17FeedbackSlotCache8SlotKindEiPKvEESt4pairIKS9_iESt10_Select1stISC_ESt4lessIS9_ENS2_13ZoneAllocatorISC_EEE4findERSB_
	movq	-88(%rbp), %r8
	cmpq	%r15, %rax
	je	.L1170
	movl	48(%rax), %ecx
	cmpl	$-1, %ecx
	jne	.L1167
.L1170:
	movq	512(%rbx), %rax
	movl	$5, %esi
	movq	%r8, -96(%rbp)
	leaq	56(%rax), %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movq	-96(%rbp), %r8
	movq	552(%rbx), %rdi
	movl	%eax, %ecx
	movq	8(%r14), %rax
	movq	%r8, %rsi
	movl	%ecx, -88(%rbp)
	movl	32(%rax), %eax
	movl	%ecx, -64(%rbp)
	movq	%r12, -80(%rbp)
	movl	%eax, -72(%rbp)
	movl	$4, -68(%rbp)
	call	_ZNSt8_Rb_treeISt5tupleIJN2v88internal11interpreter17BytecodeGenerator17FeedbackSlotCache8SlotKindEiPKvEESt4pairIKS9_iESt10_Select1stISC_ESt4lessIS9_ENS2_13ZoneAllocatorISC_EEE17_M_emplace_uniqueIJRSA_IS9_iEEEESA_ISt17_Rb_tree_iteratorISC_EbEDpOT_
	movl	-88(%rbp), %ecx
	jmp	.L1167
.L1178:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20901:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator22BuildLoadNamedPropertyEPKNS0_10ExpressionENS1_8RegisterEPKNS0_12AstRawStringE, .-_ZN2v88internal11interpreter17BytecodeGenerator22BuildLoadNamedPropertyEPKNS0_10ExpressionENS1_8RegisterEPKNS0_12AstRawStringE
	.section	.text.unlikely._ZN2v88internal11interpreter17BytecodeGenerator23BuildVariableAssignmentEPNS0_8VariableENS0_5Token5ValueENS0_13HoleCheckModeENS0_18LookupHoistingModeE,"ax",@progbits
	.align 2
.LCOLDB9:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator23BuildVariableAssignmentEPNS0_8VariableENS0_5Token5ValueENS0_13HoleCheckModeENS0_18LookupHoistingModeE,"ax",@progbits
.LHOTB9:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator23BuildVariableAssignmentEPNS0_8VariableENS0_5Token5ValueENS0_13HoleCheckModeENS0_18LookupHoistingModeE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator23BuildVariableAssignmentEPNS0_8VariableENS0_5Token5ValueENS0_13HoleCheckModeENS0_18LookupHoistingModeE, @function
_ZN2v88internal11interpreter17BytecodeGenerator23BuildVariableAssignmentEPNS0_8VariableENS0_5Token5ValueENS0_13HoleCheckModeENS0_18LookupHoistingModeE:
.LFB20900:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movl	328(%rdi), %r12d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzwl	40(%rsi), %eax
	movl	%eax, %r13d
	shrl	$7, %eax
	andl	$7, %eax
	andl	$15, %r13d
	cmpb	$5, %al
	ja	.L1225
	movq	%rsi, %r14
	movl	%ecx, %r11d
	movzbl	%al, %esi
	movl	%edx, %r15d
	leaq	.L1182(%rip), %rcx
	movslq	(%rcx,%rsi,4), %rdx
	addq	%rcx, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal11interpreter17BytecodeGenerator23BuildVariableAssignmentEPNS0_8VariableENS0_5Token5ValueENS0_13HoleCheckModeENS0_18LookupHoistingModeE,"a",@progbits
	.align 4
	.align 4
.L1182:
	.long	.L1186-.L1182
	.long	.L1185-.L1182
	.long	.L1185-.L1182
	.long	.L1184-.L1182
	.long	.L1183-.L1182
	.long	.L1181-.L1182
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator23BuildVariableAssignmentEPNS0_8VariableENS0_5Token5ValueENS0_13HoleCheckModeENS0_18LookupHoistingModeE
	.p2align 4,,10
	.p2align 3
.L1185:
	leaq	24(%rdi), %r8
	movl	%r11d, -100(%rbp)
	movl	32(%r14), %esi
	movq	%r8, -96(%rbp)
	movq	%r8, %rdi
	cmpb	$1, %al
	jne	.L1187
	cmpl	$-1, %esi
	je	.L1267
	call	_ZNK2v88internal11interpreter20BytecodeArrayBuilder9ParameterEi@PLT
	movq	-96(%rbp), %r8
	movl	-100(%rbp), %r11d
	movl	%eax, -88(%rbp)
.L1189:
	cmpb	$16, %r15b
	sete	%r15b
	testl	%r11d, %r11d
	je	.L1268
.L1190:
	cmpb	$1, %r13b
	jne	.L1228
	testb	%r15b, %r15b
	jne	.L1228
	movq	536(%rbx), %rax
	testb	$1, 129(%rax)
	jne	.L1197
	movzwl	40(%r14), %eax
	sarl	$4, %eax
	andl	$7, %eax
	cmpb	$4, %al
	je	.L1196
.L1197:
	movl	$321, %esi
	movq	%r8, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdE@PLT
.L1196:
	movl	328(%rbx), %esi
	subl	%r12d, %esi
	jmp	.L1180
	.p2align 4,,10
	.p2align 3
.L1184:
	movq	808(%rdi), %rax
	movq	(%r14), %rsi
	movl	%r11d, -88(%rbp)
	movq	8(%rax), %rdi
	call	_ZNK2v88internal5Scope18ContextChainLengthEPS1_@PLT
	movq	808(%rbx), %rsi
	movl	-88(%rbp), %r11d
	movl	%eax, %ecx
	cmpl	%eax, 28(%rsi)
	jl	.L1207
	testl	%eax, %eax
	jle	.L1204
	movq	%rsi, %rdx
	.p2align 4,,10
	.p2align 3
.L1206:
	movq	16(%rdx), %rdx
	subl	$1, %eax
	jne	.L1206
	testq	%rdx, %rdx
	je	.L1207
.L1223:
	movl	24(%rdx), %eax
	xorl	%ecx, %ecx
	movl	%eax, -96(%rbp)
.L1208:
	cmpb	$16, %r15b
	sete	-88(%rbp)
	testl	%r11d, %r11d
	je	.L1269
.L1209:
	cmpb	$1, %r13b
	jne	.L1229
	cmpb	$0, -88(%rbp)
	jne	.L1229
	movq	536(%rbx), %rax
	testb	$1, 129(%rax)
	jne	.L1216
	movzwl	40(%r14), %eax
	sarl	$4, %eax
	andl	$7, %eax
	cmpb	$4, %al
	je	.L1215
.L1216:
	leaq	24(%rbx), %rdi
	movl	$321, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdE@PLT
.L1215:
	movl	328(%rbx), %esi
	subl	%r12d, %esi
	jmp	.L1180
	.p2align 4,,10
	.p2align 3
.L1181:
	leaq	24(%rdi), %r8
	cmpb	$1, %r13b
	jne	.L1217
	cmpb	$16, %r15b
	je	.L1217
	movl	$321, %esi
	movq	%r8, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdE@PLT
	movl	328(%rbx), %esi
	subl	%r12d, %esi
	.p2align 4,,10
	.p2align 3
.L1180:
	movq	336(%rbx), %rdi
	movl	%r12d, 328(%rbx)
	testq	%rdi, %rdi
	je	.L1179
	movq	(%rdi), %rax
	salq	$32, %rsi
	orq	%r12, %rsi
	call	*32(%rax)
.L1179:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1270
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1186:
	.cfi_restore_state
	movq	536(%rdi), %rax
	movq	552(%rdi), %r15
	movzbl	129(%rax), %r13d
	movq	%r15, %rdi
	movq	%r14, -80(%rbp)
	movl	$0, -72(%rbp)
	andl	$1, %r13d
	setne	%al
	movb	%r13b, -88(%rbp)
	leaq	-80(%rbp), %r13
	addq	$16, %r15
	movzbl	%al, %eax
	movq	%r13, %rsi
	movl	%eax, -96(%rbp)
	movl	%eax, -68(%rbp)
	call	_ZNKSt8_Rb_treeISt5tupleIJN2v88internal11interpreter17BytecodeGenerator17FeedbackSlotCache8SlotKindEiPKvEESt4pairIKS9_iESt10_Select1stISC_ESt4lessIS9_ENS2_13ZoneAllocatorISC_EEE4findERSB_
	cmpq	%r15, %rax
	je	.L1200
	movl	48(%rax), %r15d
	cmpl	$-1, %r15d
	je	.L1200
.L1201:
	movq	8(%r14), %rsi
	leaq	24(%rbx), %rdi
	movl	%r15d, %edx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11StoreGlobalEPKNS0_12AstRawStringEi@PLT
	movl	328(%rbx), %esi
	subl	%r12d, %esi
	jmp	.L1180
	.p2align 4,,10
	.p2align 3
.L1183:
	movq	536(%rdi), %rax
	movq	8(%r14), %rsi
	leaq	24(%rdi), %rdi
	movl	%r8d, %ecx
	movzbl	129(%rax), %edx
	andl	$1, %edx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder15StoreLookupSlotEPKNS0_12AstRawStringENS0_12LanguageModeENS0_18LookupHoistingModeE@PLT
	movl	328(%rbx), %esi
	subl	%r12d, %esi
	jmp	.L1180
	.p2align 4,,10
	.p2align 3
.L1217:
	movq	808(%rbx), %rax
	movq	(%r14), %rsi
	movl	%r11d, -96(%rbp)
	movq	%r8, -88(%rbp)
	movq	8(%rax), %rdi
	call	_ZNK2v88internal5Scope18ContextChainLengthEPS1_@PLT
	movl	-96(%rbp), %r11d
	movq	-88(%rbp), %r8
	movl	%eax, %r13d
	testl	%r11d, %r11d
	je	.L1271
.L1218:
	movl	32(%r14), %esi
	movl	%r13d, %edx
	movq	%r8, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder19StoreModuleVariableEii@PLT
	movl	328(%rbx), %esi
	subl	%r12d, %esi
	jmp	.L1180
	.p2align 4,,10
	.p2align 3
.L1187:
	call	_ZNK2v88internal11interpreter20BytecodeArrayBuilder5LocalEi@PLT
	movl	-100(%rbp), %r11d
	movq	-96(%rbp), %r8
	movl	%eax, -88(%rbp)
	jmp	.L1189
	.p2align 4,,10
	.p2align 3
.L1207:
	movl	24(%rsi), %eax
	movl	%eax, -96(%rbp)
	jmp	.L1208
	.p2align 4,,10
	.p2align 3
.L1200:
	cmpb	$1, -88(%rbp)
	movq	512(%rbx), %rax
	sbbl	%esi, %esi
	andl	$-9, %esi
	leaq	56(%rax), %rdi
	addl	$10, %esi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movq	552(%rbx), %rdi
	movq	%r13, %rsi
	movq	%r14, -80(%rbp)
	movl	%eax, %r15d
	movl	-96(%rbp), %eax
	movl	$0, -72(%rbp)
	movl	%r15d, -64(%rbp)
	movl	%eax, -68(%rbp)
	call	_ZNSt8_Rb_treeISt5tupleIJN2v88internal11interpreter17BytecodeGenerator17FeedbackSlotCache8SlotKindEiPKvEESt4pairIKS9_iESt10_Select1stISC_ESt4lessIS9_ENS2_13ZoneAllocatorISC_EEE17_M_emplace_uniqueIJRSA_IS9_iEEEESA_ISt17_Rb_tree_iteratorISC_EbEDpOT_
	jmp	.L1201
	.p2align 4,,10
	.p2align 3
.L1268:
	leaq	328(%rbx), %rdi
	movq	%r8, -96(%rbp)
	call	_ZN2v88internal11interpreter25BytecodeRegisterAllocator11NewRegisterEv
	movq	-96(%rbp), %r8
	movl	%eax, %esi
	movl	%eax, -100(%rbp)
	movq	%r8, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movl	-88(%rbp), %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	movzwl	40(%r14), %edx
	movq	-96(%rbp), %r8
	movl	-100(%rbp), %r10d
	movl	%edx, %eax
	shrb	$4, %al
	andl	$7, %eax
	cmpb	$2, %al
	jne	.L1191
	andl	$15, %edx
	cmpb	$1, %dl
	jne	.L1192
	testb	%r15b, %r15b
	jne	.L1272
.L1192:
	movq	%r8, %rdi
	movl	%r10d, -100(%rbp)
	movq	%r8, -96(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder25ThrowSuperNotCalledIfHoleEv@PLT
	movq	-96(%rbp), %r8
	movl	-100(%rbp), %r10d
.L1193:
	movq	%r8, %rdi
	movl	%r10d, %esi
	movq	%r8, -96(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	movq	-96(%rbp), %r8
	jmp	.L1190
	.p2align 4,,10
	.p2align 3
.L1228:
	movl	-88(%rbp), %esi
	movq	%r8, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	jmp	.L1196
	.p2align 4,,10
	.p2align 3
.L1229:
	movl	32(%r14), %edx
	movl	-96(%rbp), %esi
	leaq	24(%rbx), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder16StoreContextSlotENS1_8RegisterEii@PLT
	jmp	.L1215
	.p2align 4,,10
	.p2align 3
.L1269:
	leaq	328(%rbx), %rdi
	movl	%ecx, -100(%rbp)
	leaq	24(%rbx), %r15
	call	_ZN2v88internal11interpreter25BytecodeRegisterAllocator11NewRegisterEv
	movq	%r15, %rdi
	movl	%eax, %esi
	movl	%eax, -104(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movl	32(%r14), %edx
	movl	-100(%rbp), %ecx
	movl	$1, %r8d
	movl	-96(%rbp), %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder15LoadContextSlotENS1_8RegisterEiiNS2_21ContextSlotMutabilityE@PLT
	movzwl	40(%r14), %edx
	movl	-100(%rbp), %ecx
	movl	-104(%rbp), %r10d
	movl	%edx, %eax
	shrb	$4, %al
	andl	$7, %eax
	cmpb	$2, %al
	jne	.L1210
	andl	$15, %edx
	cmpb	$1, %dl
	jne	.L1211
	cmpb	$0, -88(%rbp)
	jne	.L1273
.L1211:
	movq	%r15, %rdi
	movl	%r10d, -104(%rbp)
	movl	%ecx, -100(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder25ThrowSuperNotCalledIfHoleEv@PLT
	movl	-100(%rbp), %ecx
	movl	-104(%rbp), %r10d
.L1212:
	movl	%r10d, %esi
	movq	%r15, %rdi
	movl	%ecx, -100(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	movl	-100(%rbp), %ecx
	jmp	.L1209
	.p2align 4,,10
	.p2align 3
.L1191:
	movq	8(%r14), %rsi
	movq	%r8, %rdi
	movl	%r10d, -100(%rbp)
	movq	%r8, -96(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder25ThrowReferenceErrorIfHoleEPKNS0_12AstRawStringE@PLT
	movl	-100(%rbp), %r10d
	movq	-96(%rbp), %r8
	jmp	.L1193
	.p2align 4,,10
	.p2align 3
.L1210:
	movq	8(%r14), %rsi
	movq	%r15, %rdi
	movl	%r10d, -104(%rbp)
	movl	%ecx, -100(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder25ThrowReferenceErrorIfHoleEPKNS0_12AstRawStringE@PLT
	movl	-104(%rbp), %r10d
	movl	-100(%rbp), %ecx
	jmp	.L1212
	.p2align 4,,10
	.p2align 3
.L1267:
	call	_ZNK2v88internal11interpreter20BytecodeArrayBuilder8ReceiverEv@PLT
	movq	-96(%rbp), %r8
	movl	-100(%rbp), %r11d
	movl	%eax, -88(%rbp)
	jmp	.L1189
	.p2align 4,,10
	.p2align 3
.L1271:
	leaq	328(%rbx), %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterAllocator11NewRegisterEv
	movq	-88(%rbp), %r8
	movl	%eax, %esi
	movl	%eax, -96(%rbp)
	movq	%r8, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movl	32(%r14), %esi
	movl	%r13d, %edx
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder18LoadModuleVariableEii@PLT
	movzwl	40(%r14), %edx
	movq	-88(%rbp), %r8
	movl	-96(%rbp), %r9d
	movl	%edx, %eax
	shrb	$4, %al
	andl	$7, %eax
	cmpb	$2, %al
	jne	.L1219
	andl	$15, %edx
	cmpb	$1, %dl
	jne	.L1220
	cmpb	$16, %r15b
	je	.L1274
.L1220:
	movq	%r8, %rdi
	movl	%r9d, -96(%rbp)
	movq	%r8, -88(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder25ThrowSuperNotCalledIfHoleEv@PLT
	movq	-88(%rbp), %r8
	movl	-96(%rbp), %r9d
.L1221:
	movq	%r8, %rdi
	movl	%r9d, %esi
	movq	%r8, -88(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	movq	-88(%rbp), %r8
	jmp	.L1218
	.p2align 4,,10
	.p2align 3
.L1219:
	movq	8(%r14), %rsi
	movq	%r8, %rdi
	movl	%r9d, -96(%rbp)
	movq	%r8, -88(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder25ThrowReferenceErrorIfHoleEPKNS0_12AstRawStringE@PLT
	movl	-96(%rbp), %r9d
	movq	-88(%rbp), %r8
	jmp	.L1221
	.p2align 4,,10
	.p2align 3
.L1204:
	movq	%rsi, %rdx
	jmp	.L1223
.L1272:
	movq	%r8, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder32ThrowSuperAlreadyCalledIfNotHoleEv@PLT
	movq	-96(%rbp), %r8
	movl	-100(%rbp), %r10d
	jmp	.L1193
.L1273:
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder32ThrowSuperAlreadyCalledIfNotHoleEv@PLT
	movl	-100(%rbp), %ecx
	movl	-104(%rbp), %r10d
	jmp	.L1212
.L1274:
	movq	%r8, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder32ThrowSuperAlreadyCalledIfNotHoleEv@PLT
	movq	-88(%rbp), %r8
	movl	-96(%rbp), %r9d
	jmp	.L1221
.L1270:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal11interpreter17BytecodeGenerator23BuildVariableAssignmentEPNS0_8VariableENS0_5Token5ValueENS0_13HoleCheckModeENS0_18LookupHoistingModeE
	.cfi_startproc
	.type	_ZN2v88internal11interpreter17BytecodeGenerator23BuildVariableAssignmentEPNS0_8VariableENS0_5Token5ValueENS0_13HoleCheckModeENS0_18LookupHoistingModeE.cold, @function
_ZN2v88internal11interpreter17BytecodeGenerator23BuildVariableAssignmentEPNS0_8VariableENS0_5Token5ValueENS0_13HoleCheckModeENS0_18LookupHoistingModeE.cold:
.LFSB20900:
.L1225:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	xorl	%esi, %esi
	jmp	.L1180
	.cfi_endproc
.LFE20900:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator23BuildVariableAssignmentEPNS0_8VariableENS0_5Token5ValueENS0_13HoleCheckModeENS0_18LookupHoistingModeE
	.size	_ZN2v88internal11interpreter17BytecodeGenerator23BuildVariableAssignmentEPNS0_8VariableENS0_5Token5ValueENS0_13HoleCheckModeENS0_18LookupHoistingModeE, .-_ZN2v88internal11interpreter17BytecodeGenerator23BuildVariableAssignmentEPNS0_8VariableENS0_5Token5ValueENS0_13HoleCheckModeENS0_18LookupHoistingModeE
	.section	.text.unlikely._ZN2v88internal11interpreter17BytecodeGenerator23BuildVariableAssignmentEPNS0_8VariableENS0_5Token5ValueENS0_13HoleCheckModeENS0_18LookupHoistingModeE
	.size	_ZN2v88internal11interpreter17BytecodeGenerator23BuildVariableAssignmentEPNS0_8VariableENS0_5Token5ValueENS0_13HoleCheckModeENS0_18LookupHoistingModeE.cold, .-_ZN2v88internal11interpreter17BytecodeGenerator23BuildVariableAssignmentEPNS0_8VariableENS0_5Token5ValueENS0_13HoleCheckModeENS0_18LookupHoistingModeE.cold
.LCOLDE9:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator23BuildVariableAssignmentEPNS0_8VariableENS0_5Token5ValueENS0_13HoleCheckModeENS0_18LookupHoistingModeE
.LHOTE9:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator27VisitModuleNamespaceImportsEv.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter17BytecodeGenerator27VisitModuleNamespaceImportsEv.part.0, @function
_ZN2v88internal11interpreter17BytecodeGenerator27VisitModuleNamespaceImportsEv.part.0:
.LFB28872:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	328(%rdi), %r13d
	leal	1(%r13), %eax
	cmpl	%eax, 332(%rdi)
	movl	%eax, 328(%rdi)
	cmovge	332(%rdi), %eax
	movl	%eax, 332(%rdi)
	movq	336(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1276
	movq	(%rdi), %rax
	movl	%r13d, %esi
	call	*16(%rax)
.L1276:
	movq	528(%r12), %rdi
	leaq	24(%r12), %r15
	call	_ZN2v88internal5Scope13AsModuleScopeEv@PLT
	movq	224(%rax), %rax
	movq	104(%rax), %rcx
	movq	96(%rax), %rbx
	movq	%rcx, -56(%rbp)
	cmpq	%rcx, %rbx
	je	.L1281
	.p2align 4,,10
	.p2align 3
.L1280:
	movq	(%rbx), %r14
	movq	%r15, %rdi
	addq	$8, %rbx
	movslq	32(%r14), %rsi
	salq	$32, %rsi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralENS0_3SmiE@PLT
	movl	%r13d, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movl	%r13d, %edx
	movl	$193, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_8RegisterE@PLT
	movq	528(%r12), %rax
	movq	16(%r14), %rsi
	leaq	32(%rax), %rdi
	call	_ZN2v88internal11VariableMap6LookupEPKNS0_12AstRawStringE@PLT
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movl	$16, %edx
	call	_ZN2v88internal11interpreter17BytecodeGenerator23BuildVariableAssignmentEPNS0_8VariableENS0_5Token5ValueENS0_13HoleCheckModeENS0_18LookupHoistingModeE
	cmpq	%rbx, -56(%rbp)
	jne	.L1280
.L1281:
	movq	336(%r12), %rdi
	movl	328(%r12), %esi
	movl	%r13d, 328(%r12)
	testq	%rdi, %rdi
	je	.L1289
	movq	(%rdi), %rax
	subl	%r13d, %esi
	movq	32(%rax), %rdx
	movq	%rsi, %rax
	addq	$24, %rsp
	movl	%r13d, %esi
	salq	$32, %rax
	popq	%rbx
	popq	%r12
	orq	%rax, %rsi
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rdx
	.p2align 4,,10
	.p2align 3
.L1289:
	.cfi_restore_state
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE28872:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator27VisitModuleNamespaceImportsEv.part.0, .-_ZN2v88internal11interpreter17BytecodeGenerator27VisitModuleNamespaceImportsEv.part.0
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator27VisitModuleNamespaceImportsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator27VisitModuleNamespaceImportsEv
	.type	_ZN2v88internal11interpreter17BytecodeGenerator27VisitModuleNamespaceImportsEv, @function
_ZN2v88internal11interpreter17BytecodeGenerator27VisitModuleNamespaceImportsEv:
.LFB20754:
	.cfi_startproc
	endbr64
	movq	528(%rdi), %rax
	cmpb	$3, 128(%rax)
	je	.L1292
	ret
	.p2align 4,,10
	.p2align 3
.L1292:
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator27VisitModuleNamespaceImportsEv.part.0
	.cfi_endproc
.LFE20754:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator27VisitModuleNamespaceImportsEv, .-_ZN2v88internal11interpreter17BytecodeGenerator27VisitModuleNamespaceImportsEv
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator20VisitArgumentsObjectEPNS0_8VariableE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator20VisitArgumentsObjectEPNS0_8VariableE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator20VisitArgumentsObjectEPNS0_8VariableE, @function
_ZN2v88internal11interpreter17BytecodeGenerator20VisitArgumentsObjectEPNS0_8VariableE:
.LFB21007:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L1293
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	movl	$1, %esi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	leaq	24(%rdi), %rdi
	movq	528(%r12), %rax
	testb	$1, 129(%rax)
	je	.L1300
.L1295:
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder15CreateArgumentsENS0_19CreateArgumentsTypeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	movl	$1, %ecx
	popq	%r13
	.cfi_restore 13
	movl	$17, %edx
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator23BuildVariableAssignmentEPNS0_8VariableENS0_5Token5ValueENS0_13HoleCheckModeENS0_18LookupHoistingModeE
	.p2align 4,,10
	.p2align 3
.L1300:
	.cfi_restore_state
	movzbl	131(%rax), %esi
	andl	$1, %esi
	xorl	$1, %esi
	jmp	.L1295
	.p2align 4,,10
	.p2align 3
.L1293:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE21007:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator20VisitArgumentsObjectEPNS0_8VariableE, .-_ZN2v88internal11interpreter17BytecodeGenerator20VisitArgumentsObjectEPNS0_8VariableE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator22VisitNewTargetVariableEPNS0_8VariableE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator22VisitNewTargetVariableEPNS0_8VariableE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator22VisitNewTargetVariableEPNS0_8VariableE, @function
_ZN2v88internal11interpreter17BytecodeGenerator22VisitNewTargetVariableEPNS0_8VariableE:
.LFB21010:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L1309
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	512(%rdi), %rax
	movq	%rsi, %r12
	movq	16(%rax), %rdi
	call	_ZNK2v88internal15FunctionLiteral4kindEv@PLT
	leal	-9(%rax), %edx
	cmpb	$6, %dl
	jbe	.L1301
	cmpb	$1, %al
	je	.L1301
	movzwl	40(%r12), %eax
	sarl	$7, %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L1301
	movl	824(%r13), %esi
	leaq	24(%r13), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	xorl	%r8d, %r8d
	popq	%r13
	.cfi_restore 13
	movl	$1, %ecx
	movl	$16, %edx
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator23BuildVariableAssignmentEPNS0_8VariableENS0_5Token5ValueENS0_13HoleCheckModeENS0_18LookupHoistingModeE
	.p2align 4,,10
	.p2align 3
.L1301:
	.cfi_restore_state
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1309:
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE21010:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator22VisitNewTargetVariableEPNS0_8VariableE, .-_ZN2v88internal11interpreter17BytecodeGenerator22VisitNewTargetVariableEPNS0_8VariableE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator42BuildGeneratorObjectVariableInitializationEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator42BuildGeneratorObjectVariableInitializationEv
	.type	_ZN2v88internal11interpreter17BytecodeGenerator42BuildGeneratorObjectVariableInitializationEv, @function
_ZN2v88internal11interpreter17BytecodeGenerator42BuildGeneratorObjectVariableInitializationEv:
.LFB21011:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	528(%rdi), %rax
	movq	216(%rax), %r13
	testq	%r13, %r13
	je	.L1313
	movq	8(%r13), %r13
.L1313:
	movl	328(%rbx), %r14d
	movq	336(%rbx), %rdi
	leal	2(%r14), %eax
	cmpl	%eax, 332(%rbx)
	movq	%r14, %r12
	movl	%eax, 328(%rbx)
	cmovge	332(%rbx), %eax
	movl	%eax, 332(%rbx)
	testq	%rdi, %rdi
	je	.L1314
	movq	(%rdi), %rax
	movq	%r14, %rsi
	btsq	$33, %rsi
	call	*24(%rax)
.L1314:
	movq	%r14, %rax
	btsq	$33, %rax
	movq	%rax, -56(%rbp)
	movq	512(%rbx), %rax
	movq	16(%rax), %rdi
	call	_ZNK2v88internal15FunctionLiteral4kindEv@PLT
	subl	$9, %eax
	cmpb	$4, %al
	jbe	.L1330
.L1315:
	movl	$480, %r9d
.L1316:
	movl	%r9d, -64(%rbp)
	leaq	24(%rbx), %r15
	call	_ZN2v88internal11interpreter8Register16function_closureEv@PLT
	movl	%r12d, %edx
	movq	%r15, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder12MoveRegisterENS1_8RegisterES3_@PLT
	leal	1(%r12), %edx
	movq	%r15, %rdi
	movl	%edx, -60(%rbp)
	movq	%rax, -72(%rbp)
	call	_ZNK2v88internal11interpreter20BytecodeArrayBuilder8ReceiverEv@PLT
	movq	-72(%rbp), %r8
	movl	-60(%rbp), %edx
	movl	%eax, %esi
	movq	%r8, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder12MoveRegisterENS1_8RegisterES3_@PLT
	movl	-64(%rbp), %r9d
	movq	-56(%rbp), %rdx
	movq	%rax, %rdi
	movl	%r9d, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_12RegisterListE@PLT
	movl	824(%rbx), %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movzwl	40(%r13), %eax
	sarl	$7, %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L1317
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$16, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator23BuildVariableAssignmentEPNS0_8VariableENS0_5Token5ValueENS0_13HoleCheckModeENS0_18LookupHoistingModeE
.L1317:
	movq	336(%rbx), %rdi
	movl	328(%rbx), %esi
	movl	%r12d, 328(%rbx)
	testq	%rdi, %rdi
	je	.L1312
	movq	(%rdi), %rax
	subl	%r12d, %esi
	salq	$32, %rsi
	movq	32(%rax), %rax
	addq	$40, %rsp
	orq	%r14, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1330:
	.cfi_restore_state
	movq	512(%rbx), %rax
	movq	16(%rax), %rdi
	call	_ZNK2v88internal15FunctionLiteral4kindEv@PLT
	movl	$472, %r9d
	subl	$12, %eax
	cmpb	$1, %al
	ja	.L1316
	jmp	.L1315
	.p2align 4,,10
	.p2align 3
.L1312:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21011:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator42BuildGeneratorObjectVariableInitializationEv, .-_ZN2v88internal11interpreter17BytecodeGenerator42BuildGeneratorObjectVariableInitializationEv
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator23VisitRestArgumentsArrayEPNS0_8VariableE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator23VisitRestArgumentsArrayEPNS0_8VariableE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator23VisitRestArgumentsArrayEPNS0_8VariableE, @function
_ZN2v88internal11interpreter17BytecodeGenerator23VisitRestArgumentsArrayEPNS0_8VariableE:
.LFB21008:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L1331
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	leaq	24(%rdi), %rdi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	movl	$2, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder15CreateArgumentsENS0_19CreateArgumentsTypeE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	.cfi_restore 12
	xorl	%r8d, %r8d
	popq	%r13
	.cfi_restore 13
	movl	$1, %ecx
	movl	$17, %edx
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator23BuildVariableAssignmentEPNS0_8VariableENS0_5Token5ValueENS0_13HoleCheckModeENS0_18LookupHoistingModeE
	.p2align 4,,10
	.p2align 3
.L1331:
	ret
	.cfi_endproc
.LFE21008:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator23VisitRestArgumentsArrayEPNS0_8VariableE, .-_ZN2v88internal11interpreter17BytecodeGenerator23VisitRestArgumentsArrayEPNS0_8VariableE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator25VisitThisFunctionVariableEPNS0_8VariableE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator25VisitThisFunctionVariableEPNS0_8VariableE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator25VisitThisFunctionVariableEPNS0_8VariableE, @function
_ZN2v88internal11interpreter17BytecodeGenerator25VisitThisFunctionVariableEPNS0_8VariableE:
.LFB21009:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L1336
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	call	_ZN2v88internal11interpreter8Register16function_closureEv@PLT
	leaq	24(%r13), %rdi
	movl	%eax, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	.cfi_restore 12
	xorl	%r8d, %r8d
	popq	%r13
	.cfi_restore 13
	movl	$1, %ecx
	movl	$16, %edx
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator23BuildVariableAssignmentEPNS0_8VariableENS0_5Token5ValueENS0_13HoleCheckModeENS0_18LookupHoistingModeE
	.p2align 4,,10
	.p2align 3
.L1336:
	ret
	.cfi_endproc
.LFE21009:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator25VisitThisFunctionVariableEPNS0_8VariableE, .-_ZN2v88internal11interpreter17BytecodeGenerator25VisitThisFunctionVariableEPNS0_8VariableE
	.section	.text._ZNSt6vectorIPN2v88internal15FunctionLiteralESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal15FunctionLiteralESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal15FunctionLiteralESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal15FunctionLiteralESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal15FunctionLiteralESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_:
.LFB25379:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L1355
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L1351
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L1356
.L1343:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L1350:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L1357
	testq	%r13, %r13
	jg	.L1346
	testq	%r9, %r9
	jne	.L1349
.L1347:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1357:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L1346
.L1349:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L1347
	.p2align 4,,10
	.p2align 3
.L1356:
	testq	%rsi, %rsi
	jne	.L1344
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L1350
	.p2align 4,,10
	.p2align 3
.L1346:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L1347
	jmp	.L1349
	.p2align 4,,10
	.p2align 3
.L1351:
	movl	$8, %r14d
	jmp	.L1343
.L1355:
	leaq	.LC3(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1344:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L1343
	.cfi_endproc
.LFE25379:
	.size	_ZNSt6vectorIPN2v88internal15FunctionLiteralESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, .-_ZNSt6vectorIPN2v88internal15FunctionLiteralESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator25AddToEagerLiteralsIfEagerEPNS0_15FunctionLiteralE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator25AddToEagerLiteralsIfEagerEPNS0_15FunctionLiteralE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator25AddToEagerLiteralsIfEagerEPNS0_15FunctionLiteralE, @function
_ZN2v88internal11interpreter17BytecodeGenerator25AddToEagerLiteralsIfEagerEPNS0_15FunctionLiteralE:
.LFB20801:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	cmpq	$0, 544(%rdi)
	movq	%rsi, -24(%rbp)
	je	.L1358
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	call	_ZNK2v88internal15FunctionLiteral18ShouldEagerCompileEv@PLT
	testb	%al, %al
	jne	.L1367
.L1358:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1367:
	.cfi_restore_state
	movq	544(%rbx), %rdi
	movq	8(%rdi), %rsi
	cmpq	16(%rdi), %rsi
	je	.L1362
	movq	-24(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 8(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1362:
	.cfi_restore_state
	leaq	-24(%rbp), %rdx
	call	_ZNSt6vectorIPN2v88internal15FunctionLiteralESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L1358
	.cfi_endproc
.LFE20801:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator25AddToEagerLiteralsIfEagerEPNS0_15FunctionLiteralE, .-_ZN2v88internal11interpreter17BytecodeGenerator25AddToEagerLiteralsIfEagerEPNS0_15FunctionLiteralE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator20VisitFunctionLiteralEPNS0_15FunctionLiteralE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator20VisitFunctionLiteralEPNS0_15FunctionLiteralE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator20VisitFunctionLiteralEPNS0_15FunctionLiteralE, @function
_ZN2v88internal11interpreter17BytecodeGenerator20VisitFunctionLiteralEPNS0_15FunctionLiteralE:
.LFB20786:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	512(%rdi), %rax
	movl	(%rax), %edx
	movq	528(%rdi), %rax
	movl	4(%r12), %edi
	shrl	$2, %edx
	andl	$1, %edx
	cmpb	$2, 128(%rax)
	sete	%sil
	shrl	$10, %edi
	andl	$1, %edi
	call	_ZN2v88internal11interpreter18CreateClosureFlags6EncodeEbbb@PLT
	leaq	24(%rbx), %r9
	movq	%r9, %rdi
	movq	%r9, -96(%rbp)
	movzbl	%al, %r13d
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder33AllocateDeferredConstantPoolEntryEv@PLT
	movq	552(%rbx), %rdx
	movq	%r15, %rsi
	movq	%r12, -80(%rbp)
	movq	%rax, %r14
	movabsq	$30064771072, %rax
	movq	%rdx, %rdi
	movq	%rdx, -88(%rbp)
	movq	%rax, -72(%rbp)
	call	_ZNKSt8_Rb_treeISt5tupleIJN2v88internal11interpreter17BytecodeGenerator17FeedbackSlotCache8SlotKindEiPKvEESt4pairIKS9_iESt10_Select1stISC_ESt4lessIS9_ENS2_13ZoneAllocatorISC_EEE4findERSB_
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %r9
	addq	$16, %rdx
	cmpq	%rdx, %rax
	je	.L1370
	movl	48(%rax), %r8d
	cmpl	$-1, %r8d
	je	.L1370
.L1371:
	movl	%r13d, %ecx
	movl	%r8d, %edx
	movq	%r14, %rsi
	movq	%r9, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder13CreateClosureEmii@PLT
	movq	624(%rbx), %r13
	cmpq	632(%rbx), %r13
	je	.L1372
	movq	%r12, 0(%r13)
	movq	%r14, 8(%r13)
	addq	$16, 624(%rbx)
.L1373:
	cmpq	$0, 544(%rbx)
	movq	%r12, -80(%rbp)
	je	.L1368
	movq	%r12, %rdi
	call	_ZNK2v88internal15FunctionLiteral18ShouldEagerCompileEv@PLT
	testb	%al, %al
	jne	.L1394
.L1368:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1395
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1370:
	.cfi_restore_state
	movq	512(%rbx), %rax
	movq	%r15, %rsi
	movq	%r9, -96(%rbp)
	movl	88(%rax), %r8d
	leal	1(%r8), %edx
	movl	%r8d, -88(%rbp)
	movl	%edx, 88(%rax)
	movq	552(%rbx), %rdi
	movabsq	$30064771072, %rax
	movl	%r8d, -64(%rbp)
	movq	%r12, -80(%rbp)
	movq	%rax, -72(%rbp)
	call	_ZNSt8_Rb_treeISt5tupleIJN2v88internal11interpreter17BytecodeGenerator17FeedbackSlotCache8SlotKindEiPKvEESt4pairIKS9_iESt10_Select1stISC_ESt4lessIS9_ENS2_13ZoneAllocatorISC_EEE17_M_emplace_uniqueIJRSA_IS9_iEEEESA_ISt17_Rb_tree_iteratorISC_EbEDpOT_
	movq	-96(%rbp), %r9
	movl	-88(%rbp), %r8d
	jmp	.L1371
	.p2align 4,,10
	.p2align 3
.L1394:
	movq	544(%rbx), %rdi
	movq	8(%rdi), %rsi
	cmpq	16(%rdi), %rsi
	je	.L1384
	movq	-80(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 8(%rdi)
	jmp	.L1368
	.p2align 4,,10
	.p2align 3
.L1372:
	movq	616(%rbx), %r8
	movq	%r13, %rdx
	subq	%r8, %rdx
	movq	%rdx, %rax
	sarq	$4, %rax
	cmpq	$134217727, %rax
	je	.L1396
	testq	%rax, %rax
	je	.L1386
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L1397
	movl	$2147483632, %esi
	movl	$2147483632, %r9d
.L1375:
	movq	608(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	%rcx, %rsi
	ja	.L1398
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1378:
	addq	%rax, %r9
	leaq	16(%rax), %rcx
.L1376:
	addq	%rax, %rdx
	movq	%r12, (%rdx)
	movq	%r14, 8(%rdx)
	cmpq	%r8, %r13
	je	.L1379
	movq	%r8, %rdx
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L1380:
	movq	(%rdx), %rdi
	movq	8(%rdx), %rsi
	addq	$16, %rdx
	addq	$16, %rcx
	movq	%rdi, -16(%rcx)
	movq	%rsi, -8(%rcx)
	cmpq	%rdx, %r13
	jne	.L1380
	subq	%r8, %r13
	leaq	16(%rax,%r13), %rcx
.L1379:
	movq	%rax, %xmm0
	movq	%rcx, %xmm1
	movq	%r9, 632(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 616(%rbx)
	jmp	.L1373
	.p2align 4,,10
	.p2align 3
.L1397:
	testq	%rcx, %rcx
	jne	.L1399
	movl	$16, %ecx
	xorl	%r9d, %r9d
	xorl	%eax, %eax
	jmp	.L1376
	.p2align 4,,10
	.p2align 3
.L1386:
	movl	$16, %esi
	movl	$16, %r9d
	jmp	.L1375
	.p2align 4,,10
	.p2align 3
.L1384:
	movq	%r15, %rdx
	call	_ZNSt6vectorIPN2v88internal15FunctionLiteralESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L1368
.L1398:
	movq	%r9, -104(%rbp)
	movq	%rdx, -96(%rbp)
	movq	%r8, -88(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-88(%rbp), %r8
	movq	-96(%rbp), %rdx
	movq	-104(%rbp), %r9
	jmp	.L1378
.L1395:
	call	__stack_chk_fail@PLT
.L1396:
	leaq	.LC3(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1399:
	cmpq	$134217727, %rcx
	movl	$134217727, %r9d
	cmovbe	%rcx, %r9
	salq	$4, %r9
	movq	%r9, %rsi
	jmp	.L1375
	.cfi_endproc
.LFE20786:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator20VisitFunctionLiteralEPNS0_15FunctionLiteralE, .-_ZN2v88internal11interpreter17BytecodeGenerator20VisitFunctionLiteralEPNS0_15FunctionLiteralE
	.section	.text._ZNSt6vectorISt4pairIPN2v88internal13ObjectLiteralEmENS2_13ZoneAllocatorIS5_EEE12emplace_backIJS5_EEEvDpOT_,"axG",@progbits,_ZNSt6vectorISt4pairIPN2v88internal13ObjectLiteralEmENS2_13ZoneAllocatorIS5_EEE12emplace_backIJS5_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt4pairIPN2v88internal13ObjectLiteralEmENS2_13ZoneAllocatorIS5_EEE12emplace_backIJS5_EEEvDpOT_
	.type	_ZNSt6vectorISt4pairIPN2v88internal13ObjectLiteralEmENS2_13ZoneAllocatorIS5_EEE12emplace_backIJS5_EEEvDpOT_, @function
_ZNSt6vectorISt4pairIPN2v88internal13ObjectLiteralEmENS2_13ZoneAllocatorIS5_EEE12emplace_backIJS5_EEEvDpOT_:
.LFB25417:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	16(%rdi), %r12
	cmpq	24(%rdi), %r12
	je	.L1401
	movq	(%rsi), %rdx
	movq	8(%rsi), %rax
	movq	%rdx, (%r12)
	movq	%rax, 8(%r12)
	addq	$16, 16(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1401:
	.cfi_restore_state
	movq	8(%rdi), %r14
	movq	%r12, %r15
	subq	%r14, %r15
	movq	%r15, %rax
	sarq	$4, %rax
	cmpq	$134217727, %rax
	je	.L1415
	testq	%rax, %rax
	je	.L1410
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L1416
	movl	$2147483632, %esi
	movl	$2147483632, %r8d
.L1404:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rsi, %rdx
	jb	.L1417
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1407:
	addq	%rax, %r8
	leaq	16(%rax), %rcx
.L1405:
	movq	0(%r13), %rdi
	movq	8(%r13), %rsi
	leaq	(%rax,%r15), %rdx
	movq	%rdi, (%rdx)
	movq	%rsi, 8(%rdx)
	cmpq	%r14, %r12
	je	.L1408
	movq	%r14, %rdx
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L1409:
	movq	(%rdx), %rdi
	movq	8(%rdx), %rsi
	addq	$16, %rdx
	addq	$16, %rcx
	movq	%rdi, -16(%rcx)
	movq	%rsi, -8(%rcx)
	cmpq	%rdx, %r12
	jne	.L1409
	subq	%r14, %r12
	leaq	16(%rax,%r12), %rcx
.L1408:
	movq	%rax, %xmm0
	movq	%rcx, %xmm1
	movq	%r8, 24(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1416:
	.cfi_restore_state
	testq	%rdx, %rdx
	jne	.L1418
	movl	$16, %ecx
	xorl	%r8d, %r8d
	xorl	%eax, %eax
	jmp	.L1405
	.p2align 4,,10
	.p2align 3
.L1410:
	movl	$16, %esi
	movl	$16, %r8d
	jmp	.L1404
.L1417:
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r8
	jmp	.L1407
.L1415:
	leaq	.LC3(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1418:
	cmpq	$134217727, %rdx
	movl	$134217727, %r8d
	cmovbe	%rdx, %r8
	salq	$4, %r8
	movq	%r8, %rsi
	jmp	.L1404
	.cfi_endproc
.LFE25417:
	.size	_ZNSt6vectorISt4pairIPN2v88internal13ObjectLiteralEmENS2_13ZoneAllocatorIS5_EEE12emplace_backIJS5_EEEvDpOT_, .-_ZNSt6vectorISt4pairIPN2v88internal13ObjectLiteralEmENS2_13ZoneAllocatorIS5_EEE12emplace_backIJS5_EEEvDpOT_
	.section	.text._ZNSt6vectorISt4pairIPN2v88internal12ArrayLiteralEmENS2_13ZoneAllocatorIS5_EEE12emplace_backIJS5_EEEvDpOT_,"axG",@progbits,_ZNSt6vectorISt4pairIPN2v88internal12ArrayLiteralEmENS2_13ZoneAllocatorIS5_EEE12emplace_backIJS5_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt4pairIPN2v88internal12ArrayLiteralEmENS2_13ZoneAllocatorIS5_EEE12emplace_backIJS5_EEEvDpOT_
	.type	_ZNSt6vectorISt4pairIPN2v88internal12ArrayLiteralEmENS2_13ZoneAllocatorIS5_EEE12emplace_backIJS5_EEEvDpOT_, @function
_ZNSt6vectorISt4pairIPN2v88internal12ArrayLiteralEmENS2_13ZoneAllocatorIS5_EEE12emplace_backIJS5_EEEvDpOT_:
.LFB25450:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	16(%rdi), %r12
	cmpq	24(%rdi), %r12
	je	.L1420
	movq	(%rsi), %rdx
	movq	8(%rsi), %rax
	movq	%rdx, (%r12)
	movq	%rax, 8(%r12)
	addq	$16, 16(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1420:
	.cfi_restore_state
	movq	8(%rdi), %r14
	movq	%r12, %r15
	subq	%r14, %r15
	movq	%r15, %rax
	sarq	$4, %rax
	cmpq	$134217727, %rax
	je	.L1434
	testq	%rax, %rax
	je	.L1429
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L1435
	movl	$2147483632, %esi
	movl	$2147483632, %r8d
.L1423:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rsi, %rdx
	jb	.L1436
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1426:
	addq	%rax, %r8
	leaq	16(%rax), %rcx
.L1424:
	movq	0(%r13), %rdi
	movq	8(%r13), %rsi
	leaq	(%rax,%r15), %rdx
	movq	%rdi, (%rdx)
	movq	%rsi, 8(%rdx)
	cmpq	%r14, %r12
	je	.L1427
	movq	%r14, %rdx
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L1428:
	movq	(%rdx), %rdi
	movq	8(%rdx), %rsi
	addq	$16, %rdx
	addq	$16, %rcx
	movq	%rdi, -16(%rcx)
	movq	%rsi, -8(%rcx)
	cmpq	%rdx, %r12
	jne	.L1428
	subq	%r14, %r12
	leaq	16(%rax,%r12), %rcx
.L1427:
	movq	%rax, %xmm0
	movq	%rcx, %xmm1
	movq	%r8, 24(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1435:
	.cfi_restore_state
	testq	%rdx, %rdx
	jne	.L1437
	movl	$16, %ecx
	xorl	%r8d, %r8d
	xorl	%eax, %eax
	jmp	.L1424
	.p2align 4,,10
	.p2align 3
.L1429:
	movl	$16, %esi
	movl	$16, %r8d
	jmp	.L1423
.L1436:
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r8
	jmp	.L1426
.L1434:
	leaq	.LC3(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1437:
	cmpq	$134217727, %rdx
	movl	$134217727, %r8d
	cmovbe	%rdx, %r8
	salq	$4, %r8
	movq	%r8, %rsi
	jmp	.L1423
	.cfi_endproc
.LFE25450:
	.size	_ZNSt6vectorISt4pairIPN2v88internal12ArrayLiteralEmENS2_13ZoneAllocatorIS5_EEE12emplace_backIJS5_EEEvDpOT_, .-_ZNSt6vectorISt4pairIPN2v88internal12ArrayLiteralEmENS2_13ZoneAllocatorIS5_EEE12emplace_backIJS5_EEEvDpOT_
	.section	.text._ZNSt6vectorIN2v88internal11interpreter17BytecodeGenerator12ControlScope16DeferredCommands5EntryENS1_13ZoneAllocatorIS6_EEE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S9_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal11interpreter17BytecodeGenerator12ControlScope16DeferredCommands5EntryENS1_13ZoneAllocatorIS6_EEE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S9_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal11interpreter17BytecodeGenerator12ControlScope16DeferredCommands5EntryENS1_13ZoneAllocatorIS6_EEE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S9_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal11interpreter17BytecodeGenerator12ControlScope16DeferredCommands5EntryENS1_13ZoneAllocatorIS6_EEE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S9_EEDpOT_, @function
_ZNSt6vectorIN2v88internal11interpreter17BytecodeGenerator12ControlScope16DeferredCommands5EntryENS1_13ZoneAllocatorIS6_EEE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S9_EEDpOT_:
.LFB26231:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	movabsq	$-6148914691236517205, %rdx
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r12
	movq	8(%rdi), %r14
	movq	%r12, %rax
	subq	%r14, %rax
	sarq	$3, %rax
	imulq	%rdx, %rax
	cmpq	$89478485, %rax
	je	.L1454
	movq	%rsi, %rdx
	movq	%rdi, %r13
	movq	%rsi, %rbx
	subq	%r14, %rdx
	testq	%rax, %rax
	je	.L1448
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L1455
	movl	$2147483640, %esi
	movl	$2147483640, %r8d
.L1440:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	%rsi, %rcx
	jb	.L1456
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1443:
	addq	%rax, %r8
	leaq	24(%rax), %rdi
	jmp	.L1441
	.p2align 4,,10
	.p2align 3
.L1455:
	testq	%rcx, %rcx
	jne	.L1457
	movl	$24, %edi
	xorl	%r8d, %r8d
	xorl	%eax, %eax
.L1441:
	movdqu	(%r15), %xmm3
	movups	%xmm3, (%rax,%rdx)
	movq	16(%r15), %rcx
	movq	%rcx, 16(%rax,%rdx)
	cmpq	%r14, %rbx
	je	.L1444
	movq	%r14, %rdx
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L1445:
	movdqu	(%rdx), %xmm1
	addq	$24, %rdx
	addq	$24, %rcx
	movups	%xmm1, -24(%rcx)
	movq	-8(%rdx), %rsi
	movq	%rsi, -8(%rcx)
	cmpq	%rdx, %rbx
	jne	.L1445
	leaq	-24(%rbx), %rdx
	subq	%r14, %rdx
	shrq	$3, %rdx
	leaq	48(%rax,%rdx,8), %rdi
.L1444:
	cmpq	%r12, %rbx
	je	.L1446
	movq	%rbx, %rdx
	movq	%rdi, %rcx
	.p2align 4,,10
	.p2align 3
.L1447:
	movdqu	(%rdx), %xmm2
	addq	$24, %rdx
	addq	$24, %rcx
	movups	%xmm2, -24(%rcx)
	movq	-8(%rdx), %rsi
	movq	%rsi, -8(%rcx)
	cmpq	%rdx, %r12
	jne	.L1447
	subq	%rbx, %r12
	leaq	-24(%r12), %rdx
	shrq	$3, %rdx
	leaq	24(%rdi,%rdx,8), %rdi
.L1446:
	movq	%rax, %xmm0
	movq	%rdi, %xmm4
	movq	%r8, 24(%r13)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, 8(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1448:
	.cfi_restore_state
	movl	$24, %esi
	movl	$24, %r8d
	jmp	.L1440
.L1456:
	movq	%rdx, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %rdx
	jmp	.L1443
.L1457:
	cmpq	$89478485, %rcx
	movl	$89478485, %r8d
	cmovbe	%rcx, %r8
	imulq	$24, %r8, %r8
	movq	%r8, %rsi
	jmp	.L1440
.L1454:
	leaq	.LC3(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE26231:
	.size	_ZNSt6vectorIN2v88internal11interpreter17BytecodeGenerator12ControlScope16DeferredCommands5EntryENS1_13ZoneAllocatorIS6_EEE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S9_EEDpOT_, .-_ZNSt6vectorIN2v88internal11interpreter17BytecodeGenerator12ControlScope16DeferredCommands5EntryENS1_13ZoneAllocatorIS6_EEE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S9_EEDpOT_
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator25ControlScopeForTryFinally7ExecuteENS2_12ControlScope7CommandEPNS0_9StatementEi,"axG",@progbits,_ZN2v88internal11interpreter17BytecodeGenerator25ControlScopeForTryFinally7ExecuteENS2_12ControlScope7CommandEPNS0_9StatementEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11interpreter17BytecodeGenerator25ControlScopeForTryFinally7ExecuteENS2_12ControlScope7CommandEPNS0_9StatementEi
	.type	_ZN2v88internal11interpreter17BytecodeGenerator25ControlScopeForTryFinally7ExecuteENS2_12ControlScope7CommandEPNS0_9StatementEi, @function
_ZN2v88internal11interpreter17BytecodeGenerator25ControlScopeForTryFinally7ExecuteENS2_12ControlScope7CommandEPNS0_9StatementEi:
.LFB20424:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$4, %esi
	ja	.L1458
	movq	%rdi, %r14
	movq	8(%rdi), %rdi
	movl	%esi, %ebx
	movq	%rdx, %r13
	movq	24(%r14), %rax
	cmpq	%rax, 808(%rdi)
	je	.L1460
	movl	24(%rax), %esi
	addq	$24, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder10PopContextENS1_8RegisterE@PLT
.L1460:
	movq	40(%r14), %r12
	cmpl	$3, %ebx
	je	.L1461
	cmpl	$4, %ebx
	je	.L1475
	cmpl	$2, %ebx
	je	.L1477
	movabsq	$-6148914691236517205, %rax
	movq	24(%r12), %rsi
	movq	%rsi, %r15
	subq	16(%r12), %r15
	movl	%ebx, -80(%rbp)
	sarq	$3, %r15
	movq	%r13, -72(%rbp)
	imulq	%rax, %r15
	movl	%r15d, -64(%rbp)
	cmpq	32(%r12), %rsi
	je	.L1470
	movdqa	-80(%rbp), %xmm0
	movups	%xmm0, (%rsi)
	movq	-64(%rbp), %rax
	movq	%rax, 16(%rsi)
	addq	$24, 24(%r12)
.L1471:
	movq	(%r12), %rax
	movq	%r15, %rsi
	salq	$32, %rsi
	leaq	24(%rax), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralENS0_3SmiE@PLT
	movq	(%r12), %rax
	movl	40(%r12), %esi
	leaq	24(%rax), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	(%r12), %rdi
	movl	44(%r12), %esi
	addq	$24, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
.L1472:
	movq	32(%r14), %rdi
	call	_ZN2v88internal11interpreter17TryFinallyBuilder8LeaveTryEv@PLT
	movl	$1, %eax
.L1458:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1478
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1475:
	.cfi_restore_state
	xorl	%r13d, %r13d
.L1462:
	movq	(%r12), %rax
	movl	44(%r12), %esi
	leaq	24(%rax), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	(%r12), %rax
	movq	%r13, %rsi
	leaq	24(%rax), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralENS0_3SmiE@PLT
	movq	(%r12), %rdi
	movl	40(%r12), %esi
	addq	$24, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	jmp	.L1472
	.p2align 4,,10
	.p2align 3
.L1477:
	movl	48(%r12), %r13d
	cmpl	$-1, %r13d
	je	.L1479
.L1467:
	salq	$32, %r13
	jmp	.L1462
	.p2align 4,,10
	.p2align 3
.L1461:
	movl	52(%r12), %r13d
	cmpl	$-1, %r13d
	jne	.L1467
	movabsq	$-6148914691236517205, %rax
	movq	24(%r12), %rsi
	movq	%rsi, %rbx
	subq	16(%r12), %rbx
	movl	$3, -80(%rbp)
	sarq	$3, %rbx
	movq	$0, -72(%rbp)
	imulq	%rax, %rbx
	movl	%ebx, -64(%rbp)
	movl	%ebx, %r13d
	cmpq	32(%r12), %rsi
	je	.L1468
	movdqa	-80(%rbp), %xmm2
	movups	%xmm2, (%rsi)
	movq	-64(%rbp), %rax
	movq	%rax, 16(%rsi)
	addq	$24, 24(%r12)
.L1469:
	movl	%ebx, 52(%r12)
	jmp	.L1467
	.p2align 4,,10
	.p2align 3
.L1479:
	movabsq	$-6148914691236517205, %rax
	movq	24(%r12), %rsi
	movq	%rsi, %rbx
	subq	16(%r12), %rbx
	movl	$2, -80(%rbp)
	sarq	$3, %rbx
	movq	$0, -72(%rbp)
	imulq	%rax, %rbx
	movl	%ebx, -64(%rbp)
	movl	%ebx, %r13d
	cmpq	32(%r12), %rsi
	je	.L1465
	movdqa	-80(%rbp), %xmm1
	movups	%xmm1, (%rsi)
	movq	-64(%rbp), %rax
	movq	%rax, 16(%rsi)
	addq	$24, 24(%r12)
.L1466:
	movl	%ebx, 48(%r12)
	jmp	.L1467
	.p2align 4,,10
	.p2align 3
.L1470:
	leaq	-80(%rbp), %rdx
	leaq	8(%r12), %rdi
	call	_ZNSt6vectorIN2v88internal11interpreter17BytecodeGenerator12ControlScope16DeferredCommands5EntryENS1_13ZoneAllocatorIS6_EEE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S9_EEDpOT_
	jmp	.L1471
.L1468:
	leaq	-80(%rbp), %rdx
	leaq	8(%r12), %rdi
	call	_ZNSt6vectorIN2v88internal11interpreter17BytecodeGenerator12ControlScope16DeferredCommands5EntryENS1_13ZoneAllocatorIS6_EEE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S9_EEDpOT_
	jmp	.L1469
.L1465:
	leaq	-80(%rbp), %rdx
	leaq	8(%r12), %rdi
	call	_ZNSt6vectorIN2v88internal11interpreter17BytecodeGenerator12ControlScope16DeferredCommands5EntryENS1_13ZoneAllocatorIS6_EEE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S9_EEDpOT_
	jmp	.L1466
.L1478:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20424:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator25ControlScopeForTryFinally7ExecuteENS2_12ControlScope7CommandEPNS0_9StatementEi, .-_ZN2v88internal11interpreter17BytecodeGenerator25ControlScopeForTryFinally7ExecuteENS2_12ControlScope7CommandEPNS0_9StatementEi
	.section	.text._ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_,"axG",@progbits,_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	.type	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_, @function
_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_:
.LFB26241:
	.cfi_startproc
	endbr64
	movabsq	$2305843009213693951, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$2, %rax
	cmpq	%rcx, %rax
	je	.L1494
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L1490
	movabsq	$9223372036854775804, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L1495
.L1482:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L1489:
	movl	(%r15), %eax
	subq	%r8, %r13
	leaq	4(%rbx,%rdx), %r15
	movl	%eax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L1496
	testq	%r13, %r13
	jg	.L1485
	testq	%r9, %r9
	jne	.L1488
.L1486:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1496:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L1485
.L1488:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L1486
	.p2align 4,,10
	.p2align 3
.L1495:
	testq	%rsi, %rsi
	jne	.L1483
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L1489
	.p2align 4,,10
	.p2align 3
.L1485:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L1486
	jmp	.L1488
	.p2align 4,,10
	.p2align 3
.L1490:
	movl	$4, %r14d
	jmp	.L1482
.L1494:
	leaq	.LC3(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1483:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$2, %r14
	jmp	.L1482
	.cfi_endproc
.LFE26241:
	.size	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_, .-_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	.section	.text._ZNSt6vectorIN2v88internal11interpreter17BytecodeGenerator25GlobalDeclarationsBuilder11DeclarationENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal11interpreter17BytecodeGenerator25GlobalDeclarationsBuilder11DeclarationENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal11interpreter17BytecodeGenerator25GlobalDeclarationsBuilder11DeclarationENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal11interpreter17BytecodeGenerator25GlobalDeclarationsBuilder11DeclarationENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_, @function
_ZNSt6vectorIN2v88internal11interpreter17BytecodeGenerator25GlobalDeclarationsBuilder11DeclarationENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_:
.LFB26252:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	movabsq	$-6148914691236517205, %rdx
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r12
	movq	8(%rdi), %r14
	movq	%r12, %rax
	subq	%r14, %rax
	sarq	$3, %rax
	imulq	%rdx, %rax
	cmpq	$89478485, %rax
	je	.L1513
	movq	%rsi, %rdx
	movq	%rdi, %r13
	movq	%rsi, %rbx
	subq	%r14, %rdx
	testq	%rax, %rax
	je	.L1507
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L1514
	movl	$2147483640, %esi
	movl	$2147483640, %r8d
.L1499:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	%rsi, %rcx
	jb	.L1515
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1502:
	addq	%rax, %r8
	leaq	24(%rax), %rdi
	jmp	.L1500
	.p2align 4,,10
	.p2align 3
.L1514:
	testq	%rcx, %rcx
	jne	.L1516
	movl	$24, %edi
	xorl	%r8d, %r8d
	xorl	%eax, %eax
.L1500:
	movdqu	(%r15), %xmm3
	movups	%xmm3, (%rax,%rdx)
	movq	16(%r15), %rcx
	movq	%rcx, 16(%rax,%rdx)
	cmpq	%r14, %rbx
	je	.L1503
	movq	%r14, %rdx
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L1504:
	movdqu	(%rdx), %xmm1
	addq	$24, %rdx
	addq	$24, %rcx
	movups	%xmm1, -24(%rcx)
	movq	-8(%rdx), %rsi
	movq	%rsi, -8(%rcx)
	cmpq	%rdx, %rbx
	jne	.L1504
	leaq	-24(%rbx), %rdx
	subq	%r14, %rdx
	shrq	$3, %rdx
	leaq	48(%rax,%rdx,8), %rdi
.L1503:
	cmpq	%r12, %rbx
	je	.L1505
	movq	%rbx, %rdx
	movq	%rdi, %rcx
	.p2align 4,,10
	.p2align 3
.L1506:
	movdqu	(%rdx), %xmm2
	addq	$24, %rdx
	addq	$24, %rcx
	movups	%xmm2, -24(%rcx)
	movq	-8(%rdx), %rsi
	movq	%rsi, -8(%rcx)
	cmpq	%rdx, %r12
	jne	.L1506
	subq	%rbx, %r12
	leaq	-24(%r12), %rdx
	shrq	$3, %rdx
	leaq	24(%rdi,%rdx,8), %rdi
.L1505:
	movq	%rax, %xmm0
	movq	%rdi, %xmm4
	movq	%r8, 24(%r13)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, 8(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1507:
	.cfi_restore_state
	movl	$24, %esi
	movl	$24, %r8d
	jmp	.L1499
.L1515:
	movq	%rdx, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %rdx
	jmp	.L1502
.L1516:
	cmpq	$89478485, %rcx
	movl	$89478485, %r8d
	cmovbe	%rcx, %r8
	imulq	$24, %r8, %r8
	movq	%r8, %rsi
	jmp	.L1499
.L1513:
	leaq	.LC3(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE26252:
	.size	_ZNSt6vectorIN2v88internal11interpreter17BytecodeGenerator25GlobalDeclarationsBuilder11DeclarationENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_, .-_ZNSt6vectorIN2v88internal11interpreter17BytecodeGenerator25GlobalDeclarationsBuilder11DeclarationENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator24VisitVariableDeclarationEPNS0_19VariableDeclarationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator24VisitVariableDeclarationEPNS0_19VariableDeclarationE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator24VisitVariableDeclarationEPNS0_19VariableDeclarationE, @function
_ZN2v88internal11interpreter17BytecodeGenerator24VisitVariableDeclarationEPNS0_19VariableDeclarationE:
.LFB20752:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	8(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzwl	40(%r13), %edx
	testb	$8, %dh
	je	.L1517
	movzwl	%dx, %esi
	movl	%esi, %eax
	sarl	$7, %eax
	andl	$7, %eax
	cmpb	$5, %al
	ja	.L1517
	leaq	.L1520(%rip), %rcx
	movzbl	%al, %eax
	movq	%rdi, %r12
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal11interpreter17BytecodeGenerator24VisitVariableDeclarationEPNS0_19VariableDeclarationE,"a",@progbits
	.align 4
	.align 4
.L1520:
	.long	.L1525-.L1520
	.long	.L1524-.L1520
	.long	.L1523-.L1520
	.long	.L1522-.L1520
	.long	.L1521-.L1520
	.long	.L1519-.L1520
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator24VisitVariableDeclarationEPNS0_19VariableDeclarationE
	.p2align 4,,10
	.p2align 3
.L1525:
	movq	552(%rdi), %rbx
	leaq	-64(%rbp), %r14
	movq	%r13, -64(%rbp)
	movabsq	$21474836480, %rax
	movq	%r14, %rsi
	movq	%rax, -56(%rbp)
	movq	%rbx, %rdi
	addq	$16, %rbx
	call	_ZNKSt8_Rb_treeISt5tupleIJN2v88internal11interpreter17BytecodeGenerator17FeedbackSlotCache8SlotKindEiPKvEESt4pairIKS9_iESt10_Select1stISC_ESt4lessIS9_ENS2_13ZoneAllocatorISC_EEE4findERSB_
	cmpq	%rbx, %rax
	je	.L1527
	movl	48(%rax), %ebx
	cmpl	$-1, %ebx
	je	.L1527
.L1528:
	movq	8(%r13), %rax
	movq	560(%r12), %rdi
	movl	%ebx, -56(%rbp)
	movl	$-1, -52(%rbp)
	movq	%rax, -64(%rbp)
	movq	$0, -48(%rbp)
	movq	16(%rdi), %rsi
	cmpq	24(%rdi), %rsi
	je	.L1529
	movdqa	-64(%rbp), %xmm0
	movups	%xmm0, (%rsi)
	movq	-48(%rbp), %rax
	movq	%rax, 16(%rsi)
	addq	$24, 16(%rdi)
	.p2align 4,,10
	.p2align 3
.L1517:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1555
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1521:
	.cfi_restore_state
	movl	328(%rdi), %r14d
	leal	1(%r14), %eax
	cmpl	%eax, 332(%rdi)
	movl	%eax, 328(%rdi)
	cmovge	332(%rdi), %eax
	movl	%eax, 332(%rdi)
	movq	336(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1532
	movq	(%rdi), %rax
	movl	%r14d, %esi
	call	*16(%rax)
.L1532:
	movq	8(%r13), %rsi
	leaq	24(%r12), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralEPKNS0_12AstRawStringE@PLT
	movl	%r14d, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movl	%r14d, %edx
	movl	$300, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_8RegisterE@PLT
	jmp	.L1517
	.p2align 4,,10
	.p2align 3
.L1519:
	movl	32(%r13), %eax
	testl	%eax, %eax
	jle	.L1517
	andb	$32, %dh
	jne	.L1533
	andl	$4096, %esi
	jne	.L1517
.L1533:
	leaq	24(%r12), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadTheHoleEv@PLT
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movq	%r13, %rsi
	movl	$16, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator23BuildVariableAssignmentEPNS0_8VariableENS0_5Token5ValueENS0_13HoleCheckModeENS0_18LookupHoistingModeE
	jmp	.L1517
	.p2align 4,,10
	.p2align 3
.L1524:
	andb	$32, %dh
	je	.L1517
	addq	$24, %r12
	movl	32(%r13), %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal11interpreter20BytecodeArrayBuilder9ParameterEi@PLT
	jmp	.L1554
	.p2align 4,,10
	.p2align 3
.L1523:
	andb	$32, %dh
	je	.L1517
	addq	$24, %r12
	movl	32(%r13), %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal11interpreter20BytecodeArrayBuilder5LocalEi@PLT
.L1554:
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadTheHoleEv@PLT
	movl	%r13d, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	jmp	.L1517
	.p2align 4,,10
	.p2align 3
.L1522:
	andb	$32, %dh
	jne	.L1531
	andl	$4096, %esi
	jne	.L1517
.L1531:
	leaq	24(%r12), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadTheHoleEv@PLT
	movl	32(%r13), %edx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	movq	808(%r12), %rax
	movl	24(%rax), %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder16StoreContextSlotENS1_8RegisterEii@PLT
	jmp	.L1517
	.p2align 4,,10
	.p2align 3
.L1527:
	movq	512(%r12), %rax
	movl	$6, %esi
	leaq	56(%rax), %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movq	552(%r12), %rdi
	movq	%r14, %rsi
	movq	%r13, -64(%rbp)
	movl	%eax, %ebx
	movabsq	$21474836480, %rax
	movq	%rax, -56(%rbp)
	movl	%ebx, -48(%rbp)
	call	_ZNSt8_Rb_treeISt5tupleIJN2v88internal11interpreter17BytecodeGenerator17FeedbackSlotCache8SlotKindEiPKvEESt4pairIKS9_iESt10_Select1stISC_ESt4lessIS9_ENS2_13ZoneAllocatorISC_EEE17_M_emplace_uniqueIJRSA_IS9_iEEEESA_ISt17_Rb_tree_iteratorISC_EbEDpOT_
	jmp	.L1528
	.p2align 4,,10
	.p2align 3
.L1529:
	movq	%r14, %rdx
	call	_ZNSt6vectorIN2v88internal11interpreter17BytecodeGenerator25GlobalDeclarationsBuilder11DeclarationENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_
	jmp	.L1517
.L1555:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20752:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator24VisitVariableDeclarationEPNS0_19VariableDeclarationE, .-_ZN2v88internal11interpreter17BytecodeGenerator24VisitVariableDeclarationEPNS0_19VariableDeclarationE
	.section	.text._ZNK2v84base19TemplateHashMapImplIPvS2_NS0_26HashEqualityThenKeyMatcherIS2_PFbS2_S2_EEENS_8internal20ZoneAllocationPolicyEE5ProbeERKS2_j,"axG",@progbits,_ZNK2v84base19TemplateHashMapImplIPvS2_NS0_26HashEqualityThenKeyMatcherIS2_PFbS2_S2_EEENS_8internal20ZoneAllocationPolicyEE5ProbeERKS2_j,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v84base19TemplateHashMapImplIPvS2_NS0_26HashEqualityThenKeyMatcherIS2_PFbS2_S2_EEENS_8internal20ZoneAllocationPolicyEE5ProbeERKS2_j
	.type	_ZNK2v84base19TemplateHashMapImplIPvS2_NS0_26HashEqualityThenKeyMatcherIS2_PFbS2_S2_EEENS_8internal20ZoneAllocationPolicyEE5ProbeERKS2_j, @function
_ZNK2v84base19TemplateHashMapImplIPvS2_NS0_26HashEqualityThenKeyMatcherIS2_PFbS2_S2_EEENS_8internal20ZoneAllocationPolicyEE5ProbeERKS2_j:
.LFB26970:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	8(%rdi), %ecx
	subl	$1, %ecx
	movl	%ecx, %ebx
	andl	%edx, %ebx
	movq	(%rdi), %rdx
	leaq	(%rbx,%rbx,2), %r12
	salq	$3, %r12
	leaq	(%rdx,%r12), %rax
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	je	.L1556
	movq	%rdi, %r14
.L1560:
	cmpl	%r15d, 16(%rax)
	je	.L1566
.L1558:
	addq	$1, %rbx
	andl	%ecx, %ebx
	leaq	(%rbx,%rbx,2), %r12
	salq	$3, %r12
	leaq	(%rdx,%r12), %rax
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	jne	.L1560
.L1556:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1566:
	.cfi_restore_state
	movq	0(%r13), %rdi
	call	*16(%r14)
	testb	%al, %al
	jne	.L1559
	movl	8(%r14), %ecx
	movq	(%r14), %rdx
	subl	$1, %ecx
	jmp	.L1558
	.p2align 4,,10
	.p2align 3
.L1559:
	movq	(%r14), %rax
	addq	$8, %rsp
	popq	%rbx
	addq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26970:
	.size	_ZNK2v84base19TemplateHashMapImplIPvS2_NS0_26HashEqualityThenKeyMatcherIS2_PFbS2_S2_EEENS_8internal20ZoneAllocationPolicyEE5ProbeERKS2_j, .-_ZNK2v84base19TemplateHashMapImplIPvS2_NS0_26HashEqualityThenKeyMatcherIS2_PFbS2_S2_EEENS_8internal20ZoneAllocationPolicyEE5ProbeERKS2_j
	.section	.rodata._ZN2v84base19TemplateHashMapImplIPvS2_NS0_26HashEqualityThenKeyMatcherIS2_PFbS2_S2_EEENS_8internal20ZoneAllocationPolicyEE6ResizeES8_.str1.8,"aMS",@progbits,1
	.align 8
.LC10:
	.string	"Out of memory: HashMap::Initialize"
	.section	.text._ZN2v84base19TemplateHashMapImplIPvS2_NS0_26HashEqualityThenKeyMatcherIS2_PFbS2_S2_EEENS_8internal20ZoneAllocationPolicyEE6ResizeES8_,"axG",@progbits,_ZN2v84base19TemplateHashMapImplIPvS2_NS0_26HashEqualityThenKeyMatcherIS2_PFbS2_S2_EEENS_8internal20ZoneAllocationPolicyEE6ResizeES8_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_26HashEqualityThenKeyMatcherIS2_PFbS2_S2_EEENS_8internal20ZoneAllocationPolicyEE6ResizeES8_
	.type	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_26HashEqualityThenKeyMatcherIS2_PFbS2_S2_EEENS_8internal20ZoneAllocationPolicyEE6ResizeES8_, @function
_ZN2v84base19TemplateHashMapImplIPvS2_NS0_26HashEqualityThenKeyMatcherIS2_PFbS2_S2_EEENS_8internal20ZoneAllocationPolicyEE6ResizeES8_:
.LFB27633:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	12(%rdi), %eax
	movq	(%rdi), %r13
	movq	%rsi, -64(%rbp)
	movl	%eax, -52(%rbp)
	movl	8(%rdi), %eax
	addl	%eax, %eax
	leaq	(%rax,%rax,2), %rsi
	movq	%rax, %rbx
	movq	16(%rdx), %rax
	movq	24(%rdx), %rdx
	salq	$3, %rsi
	movq	%rdx, %rcx
	movq	%rdx, -72(%rbp)
	subq	%rax, %rcx
	cmpq	%rcx, %rsi
	ja	.L1603
	movq	-64(%rbp), %rdx
	addq	%rax, %rsi
	movq	%rsi, 16(%rdx)
.L1569:
	movq	%rax, (%r14)
	testq	%rax, %rax
	je	.L1604
	movl	%ebx, 8(%r14)
	testl	%ebx, %ebx
	je	.L1571
	movq	$0, (%rax)
	cmpl	$1, 8(%r14)
	jbe	.L1571
	movl	$24, %ecx
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L1572:
	movq	(%r14), %rsi
	addq	$1, %rax
	movq	$0, (%rsi,%rcx)
	movl	8(%r14), %esi
	addq	$24, %rcx
	cmpq	%rax, %rsi
	ja	.L1572
.L1571:
	movl	-52(%rbp), %eax
	movl	$0, 12(%r14)
	testl	%eax, %eax
	je	.L1567
	.p2align 4,,10
	.p2align 3
.L1573:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1574
	movl	8(%r14), %esi
	movl	16(%r13), %r12d
	movq	(%r14), %r9
	subl	$1, %esi
	movl	%esi, %r15d
	andl	%r12d, %r15d
	leaq	(%r15,%r15,2), %rbx
	salq	$3, %rbx
	leaq	(%r9,%rbx), %rax
	movq	(%rax), %r8
	testq	%r8, %r8
	je	.L1575
.L1581:
	cmpl	16(%rax), %r12d
	je	.L1605
.L1576:
	addq	$1, %r15
	andl	%esi, %r15d
	leaq	(%r15,%r15,2), %rbx
	salq	$3, %rbx
	leaq	(%r9,%rbx), %rax
	movq	(%rax), %r8
	testq	%r8, %r8
	jne	.L1581
	movl	16(%r13), %r12d
.L1575:
	movq	%rdi, %xmm0
	movhps	8(%r13), %xmm0
	movl	%r12d, 16(%rax)
	movups	%xmm0, (%rax)
	movl	12(%r14), %eax
	addl	$1, %eax
	movl	%eax, %esi
	movl	%eax, 12(%r14)
	shrl	$2, %esi
	addl	%esi, %eax
	cmpl	8(%r14), %eax
	jnb	.L1578
.L1582:
	addq	$24, %r13
	subl	$1, -52(%rbp)
	jne	.L1573
.L1567:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1605:
	.cfi_restore_state
	movq	%r8, %rsi
	call	*16(%r14)
	testb	%al, %al
	jne	.L1577
	movl	8(%r14), %esi
	movq	(%r14), %r9
	movq	0(%r13), %rdi
	subl	$1, %esi
	jmp	.L1576
	.p2align 4,,10
	.p2align 3
.L1574:
	addq	$24, %r13
	jmp	.L1573
	.p2align 4,,10
	.p2align 3
.L1577:
	movq	(%r14), %rax
	movl	16(%r13), %r12d
	movq	0(%r13), %rdi
	addq	%rbx, %rax
	jmp	.L1575
	.p2align 4,,10
	.p2align 3
.L1578:
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_26HashEqualityThenKeyMatcherIS2_PFbS2_S2_EEENS_8internal20ZoneAllocationPolicyEE6ResizeES8_
	movl	8(%r14), %edi
	movq	(%r14), %r8
	subl	$1, %edi
	movl	%edi, %ebx
	andl	%r12d, %ebx
	leaq	(%rbx,%rbx,2), %rax
	leaq	(%r8,%rax,8), %rax
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	je	.L1582
	cmpl	%r12d, 16(%rax)
	je	.L1606
	.p2align 4,,10
	.p2align 3
.L1583:
	addq	$1, %rbx
	andl	%edi, %ebx
	leaq	(%rbx,%rbx,2), %rax
	leaq	(%r8,%rax,8), %rax
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	je	.L1582
	cmpl	%r12d, 16(%rax)
	jne	.L1583
.L1606:
	movq	0(%r13), %rdi
	call	*16(%r14)
	testb	%al, %al
	jne	.L1582
	movl	8(%r14), %edi
	movq	(%r14), %r8
	subl	$1, %edi
	jmp	.L1583
	.p2align 4,,10
	.p2align 3
.L1603:
	movq	-64(%rbp), %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1569
.L1604:
	leaq	.LC10(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE27633:
	.size	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_26HashEqualityThenKeyMatcherIS2_PFbS2_S2_EEENS_8internal20ZoneAllocationPolicyEE6ResizeES8_, .-_ZN2v84base19TemplateHashMapImplIPvS2_NS0_26HashEqualityThenKeyMatcherIS2_PFbS2_S2_EEENS_8internal20ZoneAllocationPolicyEE6ResizeES8_
	.section	.text.unlikely._ZN2v88internal11interpreter17BytecodeGenerator17BuildClassLiteralEPNS0_12ClassLiteralENS1_8RegisterE,"ax",@progbits
	.align 2
.LCOLDB11:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator17BuildClassLiteralEPNS0_12ClassLiteralENS1_8RegisterE,"ax",@progbits
.LHOTB11:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator17BuildClassLiteralEPNS0_12ClassLiteralENS1_8RegisterE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator17BuildClassLiteralEPNS0_12ClassLiteralENS1_8RegisterE, @function
_ZN2v88internal11interpreter17BytecodeGenerator17BuildClassLiteralEPNS0_12ClassLiteralENS1_8RegisterE:
.LFB20803:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -152(%rbp)
	movl	%edx, -192(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	24(%rdi), %rax
	movq	%rax, %rdi
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder33AllocateDeferredConstantPoolEntryEv@PLT
	movq	752(%r15), %rbx
	movq	%rax, %r12
	cmpq	760(%r15), %rbx
	je	.L1608
	movq	%r14, (%rbx)
	movq	%rax, 8(%rbx)
	addq	$16, 752(%r15)
.L1609:
	movq	-152(%rbp), %rax
	movq	%r15, %rdi
	movq	16(%rax), %rax
	leaq	88(%rax), %rsi
	movq	%rax, -160(%rbp)
	call	_ZN2v88internal11interpreter17BytecodeGenerator17VisitDeclarationsEPNS_4base16ThreadedListBaseINS0_11DeclarationENS3_9EmptyBaseENS3_18ThreadedListTraitsIS5_EEEE
	movl	328(%r15), %esi
	movq	336(%r15), %rdi
	leal	1(%rsi), %eax
	cmpl	%eax, 332(%r15)
	movl	%esi, -188(%rbp)
	movl	%eax, 328(%r15)
	cmovge	332(%r15), %eax
	movl	%eax, 332(%r15)
	testq	%rdi, %rdi
	je	.L1617
	movq	(%rdi), %rax
	call	*16(%rax)
.L1617:
	movq	16(%r15), %rbx
	movq	_ZN2v88internal7Literal5MatchEPvS2_@GOTPCREL(%rip), %rax
	movq	24(%rbx), %rdx
	movq	%rax, -96(%rbp)
	movq	16(%rbx), %rax
	subq	%rax, %rdx
	cmpq	$191, %rdx
	jbe	.L1858
	leaq	192(%rax), %rdx
	movq	%rdx, 16(%rbx)
.L1619:
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L1859
	movl	$8, -104(%rbp)
	movq	$0, (%rax)
	cmpl	$1, -104(%rbp)
	jbe	.L1621
	movl	$24, %edx
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L1622:
	movq	-112(%rbp), %rcx
	addq	$1, %rax
	movq	$0, (%rcx,%rdx)
	movl	-104(%rbp), %ecx
	addq	$24, %rdx
	cmpq	%rax, %rcx
	ja	.L1622
.L1621:
	movl	328(%r15), %eax
	movq	%rbx, -64(%rbp)
	pxor	%xmm0, %xmm0
	movq	336(%r15), %rdi
	movl	$0, -100(%rbp)
	leal	1(%rax), %ebx
	movl	%eax, -160(%rbp)
	movq	%rax, %rsi
	cmpl	%ebx, 332(%r15)
	movq	%rax, -216(%rbp)
	movq	%rax, -208(%rbp)
	movl	%ebx, %eax
	cmovge	332(%r15), %eax
	movq	$0, -72(%rbp)
	movl	%ebx, 328(%r15)
	movl	%eax, 332(%r15)
	movups	%xmm0, -88(%rbp)
	testq	%rdi, %rdi
	je	.L1623
	movq	(%rdi), %rax
	call	*16(%rax)
	movl	328(%r15), %esi
	movl	332(%r15), %edx
	movq	336(%r15), %rdi
	leal	1(%rsi), %eax
	movl	%esi, -200(%rbp)
	cmpl	%edx, %eax
	movl	%eax, 328(%r15)
	cmovl	%edx, %eax
	movl	%eax, 332(%r15)
	testq	%rdi, %rdi
	je	.L1624
	movq	(%rdi), %rax
	call	*16(%rax)
.L1624:
	cmpl	-200(%rbp), %ebx
	jne	.L1627
	movl	328(%r15), %r13d
	movq	336(%r15), %rdi
	leal	1(%r13), %eax
	cmpl	%eax, 332(%r15)
	movl	%eax, 328(%r15)
	cmovge	332(%r15), %eax
	movl	%eax, 332(%r15)
	testq	%rdi, %rdi
	je	.L1626
	movq	(%rdi), %rax
	movl	%r13d, %esi
	call	*16(%rax)
.L1626:
	movl	-160(%rbp), %eax
	addl	$2, %eax
	cmpl	%r13d, %eax
	jne	.L1627
	movq	-152(%rbp), %rax
	movq	32(%rax), %rsi
	testq	%rsi, %rsi
	je	.L1860
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
.L1629:
	movl	%r13d, %esi
	movq	-168(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-152(%rbp), %rbx
	movq	%r15, %rdi
	movq	40(%rbx), %rsi
	call	_ZN2v88internal11interpreter17BytecodeGenerator20VisitFunctionLiteralEPNS0_15FunctionLiteralE
	movl	-188(%rbp), %r14d
	movq	%r13, %rdi
	movl	%r14d, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movl	-200(%rbp), %edx
	movl	%r14d, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder12MoveRegisterENS1_8RegisterES3_@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder21LoadConstantPoolEntryEm@PLT
	movl	-160(%rbp), %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	48(%rbx), %rax
	movl	12(%rax), %ecx
	testl	%ecx, %ecx
	jle	.L1738
	leaq	-144(%rbp), %rcx
	movl	$3, %r11d
	xorl	%ebx, %ebx
	movq	%rcx, -184(%rbp)
	movl	%r11d, %r14d
	.p2align 4,,10
	.p2align 3
.L1694:
	movq	(%rax), %rax
	movq	(%rax,%rbx,8), %r12
	testb	$3, (%r12)
	jne	.L1631
	cmpb	$0, 18(%r12)
	movzbl	16(%r12), %eax
	jne	.L1861
.L1638:
	cmpb	$3, %al
	je	.L1693
	movl	328(%r15), %r13d
	movq	336(%r15), %rdi
	leal	1(%r13), %eax
	cmpl	%eax, 332(%r15)
	movl	%eax, 328(%r15)
	cmovge	332(%r15), %eax
	movl	%eax, 332(%r15)
	testq	%rdi, %rdi
	je	.L1689
	movq	(%rdi), %rax
	movl	%r13d, %esi
	call	*16(%rax)
.L1689:
	leal	1(%r14), %eax
	movl	%eax, -176(%rbp)
	movl	-160(%rbp), %eax
	leal	(%rax,%r14), %r11d
	cmpl	%r13d, %r11d
	jne	.L1627
	movq	816(%r15), %rax
	movq	8(%r12), %r12
	movq	%r15, -136(%rbp)
	cmpb	$0, 8(%r15)
	movq	$2, -120(%rbp)
	movq	%rax, -144(%rbp)
	movl	328(%r15), %eax
	movl	%eax, -128(%rbp)
	movq	-184(%rbp), %rax
	movq	%rax, 816(%r15)
	jne	.L1690
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r15), %rax
	jb	.L1862
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator25VisitNoStackOverflowCheckEPNS0_7AstNodeE
.L1690:
	movq	-168(%rbp), %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-136(%rbp), %rdx
	movq	-144(%rbp), %rax
	movq	336(%rdx), %rdi
	movq	%rax, 816(%rdx)
	movl	-128(%rbp), %eax
	movl	328(%rdx), %esi
	movl	%eax, 328(%rdx)
	testq	%rdi, %rdi
	je	.L1692
	subl	%eax, %esi
	movq	(%rdi), %rdx
	movq	%rsi, %rcx
	movl	%eax, %esi
	salq	$32, %rcx
	orq	%rcx, %rsi
	call	*32(%rdx)
.L1692:
	movl	-176(%rbp), %r14d
.L1693:
	movq	-152(%rbp), %rax
	addq	$1, %rbx
	movq	48(%rax), %rax
	cmpl	%ebx, 12(%rax)
	jg	.L1694
	movl	%r14d, %r11d
.L1630:
	movq	-168(%rbp), %rdi
	movq	%r11, %rdx
	movl	$34, %esi
	salq	$32, %rdx
	orq	-208(%rbp), %rdx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_12RegisterListE@PLT
	movl	-160(%rbp), %ebx
	movq	336(%r15), %rdi
	movl	328(%r15), %esi
	movl	%ebx, 328(%r15)
	testq	%rdi, %rdi
	je	.L1695
	movq	(%rdi), %rax
	subl	%ebx, %esi
	salq	$32, %rsi
	orq	-216(%rbp), %rsi
	call	*32(%rax)
	movl	328(%r15), %esi
	movq	336(%r15), %rdi
	leal	1(%rsi), %eax
	cmpl	%eax, 332(%r15)
	movl	%esi, -160(%rbp)
	movl	%eax, 328(%r15)
	cmovge	332(%r15), %eax
	movl	%eax, 332(%r15)
	testq	%rdi, %rdi
	je	.L1696
	movq	(%rdi), %rax
	call	*16(%rax)
.L1696:
	movq	-168(%rbp), %r14
	movl	-160(%rbp), %esi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-152(%rbp), %rbx
	cmpq	$0, 24(%rbx)
	je	.L1697
	movl	-188(%rbp), %esi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	movq	24(%rbx), %rsi
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movl	$1, %ecx
	movl	$16, %edx
	call	_ZN2v88internal11interpreter17BytecodeGenerator23BuildVariableAssignmentEPNS0_8VariableENS0_5Token5ValueENS0_13HoleCheckModeENS0_18LookupHoistingModeE
.L1697:
	movq	-152(%rbp), %rax
	movq	16(%rax), %rax
	movq	136(%rax), %rax
	testq	%rax, %rax
	je	.L1698
	cmpq	$0, 40(%rax)
	je	.L1698
	movl	328(%r15), %r12d
	movq	336(%r15), %rdi
	leal	1(%r12), %eax
	cmpl	%eax, 332(%r15)
	movl	%eax, 328(%r15)
	cmovge	332(%r15), %eax
	movl	%eax, 332(%r15)
	testq	%rdi, %rdi
	je	.L1699
	movq	(%rdi), %rax
	movl	%r12d, %esi
	call	*16(%rax)
.L1699:
	movq	-152(%rbp), %rax
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L1700
	movq	8(%rax), %rsi
.L1701:
	movq	-168(%rbp), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralEPKNS0_12AstRawStringE@PLT
	movl	%r12d, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movl	$344, %esi
	movl	%r12d, %edx
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_8RegisterE@PLT
	movq	-152(%rbp), %rax
	movq	16(%rax), %rax
	movq	136(%rax), %rsi
	testq	%rsi, %rsi
	je	.L1702
	movq	40(%rsi), %rsi
.L1702:
	movl	$16, %edx
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator23BuildVariableAssignmentEPNS0_8VariableENS0_5Token5ValueENS0_13HoleCheckModeENS0_18LookupHoistingModeE
	movq	-152(%rbp), %rax
	xorl	%r12d, %r12d
	movq	48(%rax), %rax
	movl	12(%rax), %edx
	testl	%edx, %edx
	jg	.L1703
	jmp	.L1711
	.p2align 4,,10
	.p2align 3
.L1706:
	movq	336(%r15), %rdi
	movl	%ebx, 328(%r15)
	testq	%rdi, %rdi
	je	.L1709
	movq	%rsi, %rax
	movq	(%rdi), %rdx
	movl	%ebx, %esi
	salq	$32, %rax
	orq	%rax, %rsi
	call	*32(%rdx)
.L1709:
	movq	-152(%rbp), %rax
	addq	$1, %r12
	movq	48(%rax), %rax
	cmpl	%r12d, 12(%rax)
	jle	.L1711
.L1703:
	movq	(%rax), %rax
	xorl	%esi, %esi
	movl	328(%r15), %ebx
	movq	(%rax,%r12,8), %r13
	cmpb	$0, 18(%r13)
	je	.L1706
	cmpb	$0, 16(%r13)
	jne	.L1706
	movq	8(%r13), %rdi
	call	_ZN2v88internal15FunctionLiteral15NeedsHomeObjectEPNS0_10ExpressionE@PLT
	testb	%al, %al
	je	.L1857
	movl	328(%r15), %r14d
	movq	336(%r15), %rdi
	leal	1(%r14), %eax
	cmpl	%eax, 332(%r15)
	movl	%eax, 328(%r15)
	cmovge	332(%r15), %eax
	movl	%eax, 332(%r15)
	testq	%rdi, %rdi
	je	.L1708
	movq	(%rdi), %rax
	movl	%r14d, %esi
	call	*16(%rax)
.L1708:
	movq	24(%r13), %rsi
	movl	$1, %ecx
	movl	$1, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator17BuildVariableLoadEPNS0_8VariableENS0_13HoleCheckModeENS0_10TypeofModeE
	movq	-168(%rbp), %rdi
	movl	%r14d, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	%r13, %rcx
	movl	%r14d, %esi
	movq	%r15, %rdi
	movl	-160(%rbp), %edx
	call	_ZN2v88internal11interpreter17BytecodeGenerator18VisitSetHomeObjectENS1_8RegisterES3_PNS0_15LiteralPropertyE
.L1857:
	movl	328(%r15), %esi
	subl	%ebx, %esi
	jmp	.L1706
	.p2align 4,,10
	.p2align 3
.L1631:
	movl	328(%r15), %r13d
	movq	336(%r15), %rdi
	leal	1(%r13), %eax
	cmpl	%eax, 332(%r15)
	movl	%eax, 328(%r15)
	cmovge	332(%r15), %eax
	movl	%eax, 332(%r15)
	testq	%rdi, %rdi
	je	.L1633
	movq	(%rdi), %rax
	movl	%r13d, %esi
	call	*16(%rax)
.L1633:
	leal	1(%r14), %eax
	movl	%eax, -176(%rbp)
	movl	-160(%rbp), %eax
	leal	(%rax,%r14), %r11d
	cmpl	%r13d, %r11d
	jne	.L1627
	movq	(%r12), %rax
	andq	$-4, %rax
	movl	(%rax), %eax
	cmpl	$-1, %eax
	je	.L1634
	movb	$2, 496(%r15)
	movl	%eax, 500(%r15)
.L1634:
	movl	%r13d, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator20BuildLoadPropertyKeyEPNS0_15LiteralPropertyENS1_8RegisterE
	cmpb	$0, 17(%r12)
	je	.L1635
	movl	844(%r15), %r14d
	cmpl	$-1, %r14d
	je	.L1863
.L1636:
	movq	520(%r15), %rax
	movb	$0, -144(%rbp)
	movq	$-1, -136(%rbp)
	movq	-168(%rbp), %rdi
	movq	416(%rax), %rsi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralEPKNS0_12AstRawStringE@PLT
	movl	%r14d, %ecx
	movl	%r13d, %edx
	movl	$54, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder16CompareOperationENS0_5Token5ValueENS1_8RegisterEi@PLT
	movq	-184(%rbp), %r14
	movl	$1, %esi
	movq	%rax, %rdi
	movq	%r14, %rdx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11JumpIfFalseENS2_13ToBooleanModeEPNS1_13BytecodeLabelE@PLT
	movl	$42, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4BindEPNS1_13BytecodeLabelE@PLT
.L1635:
	movzbl	16(%r12), %eax
	cmpb	$3, %al
	je	.L1864
.L1637:
	cmpb	$0, 18(%r12)
	movl	-176(%rbp), %r14d
	je	.L1638
.L1861:
	cmpb	$2, %al
	je	.L1639
	ja	.L1640
	testb	%al, %al
	je	.L1865
	movq	(%r12), %r13
	andq	$-4, %r13
	movzbl	4(%r13), %eax
	andl	$63, %eax
	cmpb	$41, %al
	movl	$0, %eax
	cmovne	%rax, %r13
	movq	-64(%rbp), %rax
	movq	%r13, %rdi
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal7Literal4HashEv@PLT
	movq	-112(%rbp), %r10
	movq	%r13, -144(%rbp)
	movq	%r13, %rdi
	movl	%eax, %r9d
	movl	-104(%rbp), %eax
	subl	$1, %eax
	movl	%eax, %edx
	andl	%r9d, %edx
	leaq	(%rdx,%rdx,2), %rcx
	salq	$3, %rcx
	leaq	(%r10,%rcx), %r8
	movq	(%r8), %rsi
	testq	%rsi, %rsi
	je	.L1730
	movq	%r12, -224(%rbp)
	movq	%rcx, %r12
	movq	%rbx, -232(%rbp)
	movl	%r9d, %ebx
.L1654:
	cmpl	16(%r8), %ebx
	je	.L1866
.L1650:
	addq	$1, %rdx
	andl	%eax, %edx
	leaq	(%rdx,%rdx,2), %rcx
	leaq	0(,%rcx,8), %r12
	leaq	(%r10,%r12), %r8
	movq	(%r8), %rsi
	testq	%rsi, %rsi
	jne	.L1654
	movl	%ebx, %r9d
	movq	-224(%rbp), %r12
	movq	-232(%rbp), %rbx
.L1730:
	movq	%rdi, (%r8)
	movq	$0, 8(%r8)
	movl	-100(%rbp), %eax
	movl	%r9d, 16(%r8)
	addl	$1, %eax
	movl	%eax, %edx
	movl	%eax, -100(%rbp)
	shrl	$2, %edx
	addl	%edx, %eax
	cmpl	-104(%rbp), %eax
	jb	.L1652
	movq	-240(%rbp), %rsi
	leaq	-112(%rbp), %rdi
	movl	%r9d, -224(%rbp)
	movq	%rdi, -176(%rbp)
	call	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_26HashEqualityThenKeyMatcherIS2_PFbS2_S2_EEENS_8internal20ZoneAllocationPolicyEE6ResizeES8_
	movl	-224(%rbp), %r9d
	movq	-184(%rbp), %rsi
	movq	-176(%rbp), %rdi
	movl	%r9d, %edx
	call	_ZNK2v84base19TemplateHashMapImplIPvS2_NS0_26HashEqualityThenKeyMatcherIS2_PFbS2_S2_EEENS_8internal20ZoneAllocationPolicyEE5ProbeERKS2_j
	movq	%rax, %r8
	jmp	.L1652
	.p2align 4,,10
	.p2align 3
.L1640:
	cmpb	$3, %al
	jne	.L1693
	movl	328(%r15), %r13d
	movq	336(%r15), %rdi
	leal	1(%r13), %eax
	cmpl	%eax, 332(%r15)
	movl	%eax, %edx
	cmovge	332(%r15), %edx
	movl	%eax, 328(%r15)
	movl	%edx, 332(%r15)
	testq	%rdi, %rdi
	je	.L1644
	movq	(%rdi), %rax
	movl	%r13d, %esi
	call	*16(%rax)
	movl	328(%r15), %eax
.L1644:
	movq	(%r12), %rsi
	movl	%eax, -128(%rbp)
	movq	%r15, %rdi
	movq	-184(%rbp), %rax
	movq	816(%r15), %rdx
	movq	%r15, -136(%rbp)
	andq	$-4, %rsi
	movq	$2, -120(%rbp)
	movq	%rax, 816(%r15)
	movq	%rdx, -144(%rbp)
	call	_ZN2v88internal11interpreter17BytecodeGenerator5VisitEPNS0_7AstNodeE
	movq	-168(%rbp), %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-136(%rbp), %rdx
	movq	-144(%rbp), %rax
	movq	336(%rdx), %rdi
	movq	%rax, 816(%rdx)
	movl	-128(%rbp), %eax
	movl	328(%rdx), %esi
	movl	%eax, 328(%rdx)
	testq	%rdi, %rdi
	je	.L1645
	subl	%eax, %esi
	movq	(%rdi), %rdx
	movq	%rsi, %rcx
	movl	%eax, %esi
	salq	$32, %rcx
	orq	%rcx, %rsi
	call	*32(%rdx)
.L1645:
	movq	(%r12), %rax
	andq	$-4, %rax
	movzbl	4(%rax), %edx
	andl	$63, %edx
	cmpb	$41, %dl
	jne	.L1646
	movq	8(%rax), %rsi
	movq	-168(%rbp), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralEPKNS0_12AstRawStringE@PLT
	movl	%r13d, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movl	%r13d, %edx
	movl	$344, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_8RegisterE@PLT
	movq	24(%r12), %rsi
	movq	%r15, %rdi
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$16, %edx
	call	_ZN2v88internal11interpreter17BytecodeGenerator23BuildVariableAssignmentEPNS0_8VariableENS0_5Token5ValueENS0_13HoleCheckModeENS0_18LookupHoistingModeE
	movl	328(%r15), %esi
	movq	336(%r15), %rdi
	movl	%r13d, 328(%r15)
	subl	%r13d, %esi
	testq	%rdi, %rdi
	je	.L1693
	movq	%rsi, %rax
	movq	(%rdi), %rdx
	movl	%r13d, %esi
	salq	$32, %rax
	orq	%rax, %rsi
	call	*32(%rdx)
	jmp	.L1693
	.p2align 4,,10
	.p2align 3
.L1862:
	movb	$1, 8(%r15)
	jmp	.L1690
	.p2align 4,,10
	.p2align 3
.L1711:
	movq	-80(%rbp), %rax
	movq	-88(%rbp), %r13
	movq	%rax, -200(%rbp)
	cmpq	%rax, %r13
	je	.L1698
	.p2align 4,,10
	.p2align 3
.L1704:
	movl	328(%r15), %r12d
	movq	8(%r13), %r14
	movq	336(%r15), %rdi
	leal	2(%r12), %eax
	cmpl	%eax, 332(%r15)
	movq	%r12, %rbx
	movl	%eax, 328(%r15)
	cmovge	332(%r15), %eax
	movl	%eax, 332(%r15)
	testq	%rdi, %rdi
	je	.L1712
	movabsq	$8589934592, %rsi
	movq	(%rdi), %rax
	orq	%r12, %rsi
	call	*24(%rax)
.L1712:
	movq	(%r14), %r8
	movl	-160(%rbp), %esi
	movl	%ebx, %ecx
	movq	%r15, %rdi
	movabsq	$8589934592, %r10
	movq	8(%r14), %r14
	movq	%r8, %rdx
	orq	%r12, %r10
	movq	%r8, -176(%rbp)
	movq	%r10, -184(%rbp)
	call	_ZN2v88internal11interpreter17BytecodeGenerator20VisitLiteralAccessorENS1_8RegisterEPNS0_15LiteralPropertyES3_
	leal	1(%rbx), %ecx
	movq	%r14, %rdx
	movq	%r15, %rdi
	movl	-160(%rbp), %esi
	call	_ZN2v88internal11interpreter17BytecodeGenerator20VisitLiteralAccessorENS1_8RegisterEPNS0_15LiteralPropertyES3_
	movq	-184(%rbp), %r10
	movl	$214, %esi
	movq	-168(%rbp), %rdi
	movq	%r10, %rdx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_12RegisterListE@PLT
	movq	-176(%rbp), %r8
	testq	%r8, %r8
	je	.L1713
	movq	24(%r8), %rsi
.L1714:
	movq	%r15, %rdi
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$16, %edx
	call	_ZN2v88internal11interpreter17BytecodeGenerator23BuildVariableAssignmentEPNS0_8VariableENS0_5Token5ValueENS0_13HoleCheckModeENS0_18LookupHoistingModeE
	movq	336(%r15), %rdi
	movl	328(%r15), %esi
	movl	%ebx, 328(%r15)
	testq	%rdi, %rdi
	je	.L1715
	movq	(%rdi), %rax
	subl	%ebx, %esi
	addq	$16, %r13
	salq	$32, %rsi
	orq	%r12, %rsi
	call	*32(%rax)
	cmpq	%r13, -200(%rbp)
	jne	.L1704
.L1698:
	movq	-152(%rbp), %rbx
	movq	64(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L1717
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator21VisitForRegisterValueEPNS0_10ExpressionE
	movq	64(%rbx), %rdi
	movl	%eax, %r12d
	call	_ZN2v88internal15FunctionLiteral15NeedsHomeObjectEPNS0_10ExpressionE@PLT
	testb	%al, %al
	jne	.L1867
.L1718:
	movq	512(%r15), %rax
	leaq	56(%rax), %rdi
	movq	536(%r15), %rax
	movzbl	129(%rax), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%esi, %esi
	andl	$-9, %esi
	addl	$11, %esi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movq	-168(%rbp), %rdi
	movl	%r12d, %esi
	movl	%eax, %r13d
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	movl	-188(%rbp), %ebx
	movl	%r13d, %edx
	movq	%rax, %rdi
	movl	%ebx, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27StoreClassFieldsInitializerENS1_8RegisterEi@PLT
	movl	%ebx, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
.L1717:
	movq	-152(%rbp), %rax
	cmpq	$0, 56(%rax)
	je	.L1721
	movl	328(%r15), %r14d
	movq	336(%r15), %rdi
	leal	1(%r14), %eax
	cmpl	%eax, 332(%r15)
	movl	%eax, %edx
	cmovge	332(%r15), %edx
	cmpl	$2147483647, -192(%rbp)
	jne	.L1868
.L1722:
	movl	%eax, 328(%r15)
	movl	%r14d, %r12d
	movl	%edx, 332(%r15)
	testq	%rdi, %rdi
	je	.L1724
	movq	(%rdi), %rax
	movq	%r12, %rsi
	btsq	$32, %rsi
	call	*24(%rax)
.L1724:
	movq	-152(%rbp), %rbx
	movq	%r15, %rdi
	btsq	$32, %r12
	movq	56(%rbx), %rsi
	call	_ZN2v88internal11interpreter17BytecodeGenerator21VisitForRegisterValueEPNS0_10ExpressionE
	movq	56(%rbx), %rdi
	movl	%eax, %r13d
	call	_ZN2v88internal15FunctionLiteral15NeedsHomeObjectEPNS0_10ExpressionE@PLT
	testb	%al, %al
	jne	.L1869
.L1725:
	movl	-188(%rbp), %esi
	movq	-168(%rbp), %rdi
	movl	%r14d, %edx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder12MoveRegisterENS1_8RegisterES3_@PLT
	movq	512(%r15), %rdi
	movl	$4, %esi
	movq	%rax, %r14
	addq	$56, %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movq	%r12, %rdx
	movl	%r13d, %esi
	movq	%r14, %rdi
	movl	%eax, %ecx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder12CallPropertyENS1_8RegisterENS1_12RegisterListEi@PLT
.L1721:
	movq	-168(%rbp), %rdi
	movl	-188(%rbp), %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1607
	call	_ZdlPv@PLT
.L1607:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1870
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1627:
	.cfi_restore_state
	leaq	.LC5(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1866:
	movq	%rdx, -176(%rbp)
	call	*-96(%rbp)
	testb	%al, %al
	je	.L1871
	movq	%r12, %rcx
	addq	-112(%rbp), %rcx
	movl	%ebx, %r9d
	movq	-224(%rbp), %r12
	cmpq	$0, (%rcx)
	movq	-232(%rbp), %rbx
	movq	%rcx, %r8
	je	.L1872
.L1652:
	movq	8(%r8), %rdx
	testq	%rdx, %rdx
	je	.L1873
.L1656:
	movq	%r12, (%rdx)
	jmp	.L1693
	.p2align 4,,10
	.p2align 3
.L1623:
	leal	2(%rsi), %edx
	movl	%ebx, -200(%rbp)
	cmpl	%edx, %eax
	movl	%edx, 328(%r15)
	cmovl	%edx, %eax
	movl	%eax, 332(%r15)
	jmp	.L1624
	.p2align 4,,10
	.p2align 3
.L1639:
	movq	(%r12), %r13
	andq	$-4, %r13
	movzbl	4(%r13), %eax
	andl	$63, %eax
	cmpb	$41, %al
	movl	$0, %eax
	cmovne	%rax, %r13
	movq	-64(%rbp), %rax
	movq	%r13, %rdi
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal7Literal4HashEv@PLT
	movq	-112(%rbp), %r10
	movq	%r13, -144(%rbp)
	movq	%r13, %rdi
	movl	%eax, %r9d
	movl	-104(%rbp), %eax
	subl	$1, %eax
	movl	%eax, %edx
	andl	%r9d, %edx
	leaq	(%rdx,%rdx,2), %rcx
	salq	$3, %rcx
	leaq	(%r10,%rcx), %r8
	movq	(%r8), %rsi
	testq	%rsi, %rsi
	je	.L1731
	movq	%r12, -224(%rbp)
	movq	%rcx, %r12
	movq	%rbx, -232(%rbp)
	movl	%r9d, %ebx
.L1675:
	cmpl	16(%r8), %ebx
	je	.L1874
.L1671:
	addq	$1, %rdx
	andl	%eax, %edx
	leaq	(%rdx,%rdx,2), %rcx
	leaq	0(,%rcx,8), %r12
	leaq	(%r10,%r12), %r8
	movq	(%r8), %rsi
	testq	%rsi, %rsi
	jne	.L1675
	movl	%ebx, %r9d
	movq	-224(%rbp), %r12
	movq	-232(%rbp), %rbx
.L1731:
	movq	%rdi, (%r8)
	movq	$0, 8(%r8)
	movl	-100(%rbp), %eax
	movl	%r9d, 16(%r8)
	addl	$1, %eax
	movl	%eax, %edx
	movl	%eax, -100(%rbp)
	shrl	$2, %edx
	addl	%edx, %eax
	cmpl	-104(%rbp), %eax
	jb	.L1673
	movq	-240(%rbp), %rsi
	leaq	-112(%rbp), %rdi
	movl	%r9d, -224(%rbp)
	movq	%rdi, -176(%rbp)
	call	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_26HashEqualityThenKeyMatcherIS2_PFbS2_S2_EEENS_8internal20ZoneAllocationPolicyEE6ResizeES8_
	movl	-224(%rbp), %r9d
	movq	-184(%rbp), %rsi
	movq	-176(%rbp), %rdi
	movl	%r9d, %edx
	call	_ZNK2v84base19TemplateHashMapImplIPvS2_NS0_26HashEqualityThenKeyMatcherIS2_PFbS2_S2_EEENS_8internal20ZoneAllocationPolicyEE5ProbeERKS2_j
	movq	%rax, %r8
	jmp	.L1673
	.p2align 4,,10
	.p2align 3
.L1874:
	movq	%rdx, -176(%rbp)
	call	*-96(%rbp)
	testb	%al, %al
	je	.L1875
	movq	%r12, %rcx
	addq	-112(%rbp), %rcx
	movl	%ebx, %r9d
	movq	-224(%rbp), %r12
	cmpq	$0, (%rcx)
	movq	-232(%rbp), %rbx
	movq	%rcx, %r8
	je	.L1876
.L1673:
	movq	8(%r8), %rdx
	testq	%rdx, %rdx
	je	.L1877
.L1677:
	movq	%r12, 8(%rdx)
	jmp	.L1693
	.p2align 4,,10
	.p2align 3
.L1865:
	movq	8(%r12), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	movq	24(%r12), %rsi
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movl	$1, %ecx
	movl	$16, %edx
	call	_ZN2v88internal11interpreter17BytecodeGenerator23BuildVariableAssignmentEPNS0_8VariableENS0_5Token5ValueENS0_13HoleCheckModeENS0_18LookupHoistingModeE
	jmp	.L1693
	.p2align 4,,10
	.p2align 3
.L1864:
	movq	-168(%rbp), %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	movq	24(%r12), %rsi
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movl	$1, %ecx
	movl	$16, %edx
	call	_ZN2v88internal11interpreter17BytecodeGenerator23BuildVariableAssignmentEPNS0_8VariableENS0_5Token5ValueENS0_13HoleCheckModeENS0_18LookupHoistingModeE
	movzbl	16(%r12), %eax
	jmp	.L1637
	.p2align 4,,10
	.p2align 3
.L1860:
	movq	-168(%rbp), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadTheHoleEv@PLT
	jmp	.L1629
	.p2align 4,,10
	.p2align 3
.L1875:
	movl	-104(%rbp), %eax
	movq	-112(%rbp), %r10
	movq	-144(%rbp), %rdi
	movq	-176(%rbp), %rdx
	subl	$1, %eax
	jmp	.L1671
	.p2align 4,,10
	.p2align 3
.L1871:
	movl	-104(%rbp), %eax
	movq	-112(%rbp), %r10
	movq	-144(%rbp), %rdi
	movq	-176(%rbp), %rdx
	subl	$1, %eax
	jmp	.L1650
	.p2align 4,,10
	.p2align 3
.L1869:
	movq	512(%r15), %rax
	leaq	56(%rax), %rdi
	movq	536(%r15), %rax
	movzbl	129(%rax), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%esi, %esi
	andl	$-9, %esi
	addl	$11, %esi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movl	-188(%rbp), %esi
	movq	-168(%rbp), %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	movl	%ebx, %edx
	movl	%r13d, %esi
	movq	%rax, %rdi
	movq	536(%r15), %rax
	movzbl	129(%rax), %ecx
	andl	$1, %ecx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder23StoreHomeObjectPropertyENS1_8RegisterEiNS0_12LanguageModeE@PLT
	jmp	.L1725
	.p2align 4,,10
	.p2align 3
.L1868:
	movl	%eax, 328(%r15)
	movl	%edx, 332(%r15)
	testq	%rdi, %rdi
	je	.L1723
	movq	(%rdi), %rax
	movl	%r14d, %esi
	call	*16(%rax)
.L1723:
	movq	520(%r15), %rax
	movq	-168(%rbp), %r13
	movq	344(%rax), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralEPKNS0_12AstRawStringE@PLT
	movl	%r14d, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	512(%r15), %rax
	movl	$17, %esi
	leaq	56(%rax), %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movl	-192(%rbp), %esi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	movl	-188(%rbp), %esi
	movl	%r14d, %edx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	movl	%ebx, %r8d
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreDataPropertyInLiteralENS1_8RegisterES3_NS_4base5FlagsINS0_25DataPropertyInLiteralFlagEiEEi@PLT
	movl	328(%r15), %r14d
	movq	336(%r15), %rdi
	leal	1(%r14), %eax
	cmpl	%eax, 332(%r15)
	movl	%eax, %edx
	cmovge	332(%r15), %edx
	jmp	.L1722
	.p2align 4,,10
	.p2align 3
.L1867:
	movq	512(%r15), %rax
	leaq	56(%rax), %rdi
	movq	536(%r15), %rax
	movzbl	129(%rax), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%esi, %esi
	andl	$-9, %esi
	addl	$11, %esi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movl	-160(%rbp), %esi
	movq	-168(%rbp), %rdi
	movl	%eax, %r13d
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	movl	%r13d, %edx
	movl	%r12d, %esi
	movq	%rax, %rdi
	movq	536(%r15), %rax
	movzbl	129(%rax), %ecx
	andl	$1, %ecx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder23StoreHomeObjectPropertyENS1_8RegisterEiNS0_12LanguageModeE@PLT
	jmp	.L1718
	.p2align 4,,10
	.p2align 3
.L1863:
	movl	840(%r15), %esi
	movq	848(%r15), %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movl	%eax, 844(%r15)
	movl	%eax, %r14d
	jmp	.L1636
.L1700:
	movq	520(%r15), %rax
	movq	272(%rax), %rsi
	jmp	.L1701
	.p2align 4,,10
	.p2align 3
.L1713:
	movq	24(%r14), %rsi
	jmp	.L1714
	.p2align 4,,10
	.p2align 3
.L1715:
	addq	$16, %r13
	cmpq	%r13, -200(%rbp)
	jne	.L1704
	jmp	.L1698
	.p2align 4,,10
	.p2align 3
.L1608:
	movq	744(%r15), %r13
	movq	%rbx, %r14
	subq	%r13, %r14
	movq	%r14, %rax
	sarq	$4, %rax
	cmpq	$134217727, %rax
	je	.L1661
	testq	%rax, %rax
	je	.L1735
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L1878
	movl	$2147483632, %esi
	movl	$2147483632, %edx
.L1611:
	movq	736(%r15), %r8
	movq	16(%r8), %rdi
	movq	24(%r8), %rax
	subq	%rdi, %rax
	cmpq	%rsi, %rax
	jb	.L1879
	addq	%rdi, %rsi
	movq	%rsi, 16(%r8)
.L1614:
	leaq	(%rdi,%rdx), %r8
	leaq	16(%rdi), %rax
.L1612:
	movq	-152(%rbp), %rcx
	addq	%rdi, %r14
	movq	%r12, 8(%r14)
	movq	%rcx, (%r14)
	cmpq	%r13, %rbx
	je	.L1615
	movq	%r13, %rax
	movq	%rdi, %rdx
	.p2align 4,,10
	.p2align 3
.L1616:
	movq	(%rax), %rsi
	movq	8(%rax), %rcx
	addq	$16, %rax
	addq	$16, %rdx
	movq	%rsi, -16(%rdx)
	movq	%rcx, -8(%rdx)
	cmpq	%rax, %rbx
	jne	.L1616
	subq	%r13, %rbx
	leaq	16(%rdi,%rbx), %rax
.L1615:
	movq	%rdi, %xmm0
	movq	%rax, %xmm1
	movq	%r8, 760(%r15)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 744(%r15)
	jmp	.L1609
	.p2align 4,,10
	.p2align 3
.L1695:
	movl	-200(%rbp), %eax
	cmpl	%eax, 332(%r15)
	movl	%eax, 328(%r15)
	cmovge	332(%r15), %eax
	movl	%eax, 332(%r15)
	jmp	.L1696
.L1878:
	testq	%rdx, %rdx
	jne	.L1880
	movl	$16, %eax
	xorl	%r8d, %r8d
	xorl	%edi, %edi
	jmp	.L1612
	.p2align 4,,10
	.p2align 3
.L1858:
	movl	$192, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1619
.L1738:
	movl	$3, %r11d
	jmp	.L1630
.L1735:
	movl	$16, %esi
	movl	$16, %edx
	jmp	.L1611
.L1873:
	movq	-64(%rbp), %rdi
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	subq	%rcx, %rax
	cmpq	$15, %rax
	jbe	.L1881
	leaq	16(%rcx), %rax
	movq	%rax, 16(%rdi)
.L1658:
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%rcx)
	movq	%rcx, 8(%r8)
	movq	-80(%rbp), %r10
	cmpq	-72(%rbp), %r10
	je	.L1659
	movq	%r13, %xmm0
	movq	%rcx, %xmm4
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, (%r10)
	addq	$16, -80(%rbp)
.L1660:
	movq	8(%r8), %rdx
	jmp	.L1656
.L1877:
	movq	-64(%rbp), %rdi
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	subq	%rcx, %rax
	cmpq	$15, %rax
	jbe	.L1882
	leaq	16(%rcx), %rax
	movq	%rax, 16(%rdi)
.L1679:
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%rcx)
	movq	%rcx, 8(%r8)
	movq	-80(%rbp), %r10
	cmpq	-72(%rbp), %r10
	je	.L1680
	movq	%r13, %xmm0
	movq	%rcx, %xmm5
	punpcklqdq	%xmm5, %xmm0
	movups	%xmm0, (%r10)
	addq	$16, -80(%rbp)
.L1681:
	movq	8(%r8), %rdx
	jmp	.L1677
.L1879:
	movq	%r8, %rdi
	movq	%rdx, -160(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-160(%rbp), %rdx
	movq	%rax, %rdi
	jmp	.L1614
.L1872:
	movq	-144(%rbp), %rdi
	jmp	.L1730
.L1876:
	movq	-144(%rbp), %rdi
	jmp	.L1731
.L1659:
	movq	-88(%rbp), %r9
	movq	%r10, %rax
	subq	%r9, %rax
	movq	%rax, -176(%rbp)
	sarq	$4, %rax
	movq	%rax, %rsi
	movabsq	$576460752303423487, %rax
	cmpq	%rax, %rsi
	je	.L1661
	testq	%rsi, %rsi
	je	.L1883
	leaq	(%rsi,%rsi), %rax
	cmpq	%rax, %rsi
	ja	.L1884
	xorl	%r11d, %r11d
	movl	$16, %esi
	testq	%rax, %rax
	jne	.L1885
.L1664:
	movq	-176(%rbp), %rax
	movq	%r13, %xmm0
	movq	%rcx, %xmm6
	punpcklqdq	%xmm6, %xmm0
	movups	%xmm0, (%rdx,%rax)
	cmpq	%r9, %r10
	je	.L1665
	leaq	-16(%r10), %rdi
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	subq	%r9, %rdi
	movq	%rdi, %rsi
	shrq	$4, %rsi
	addq	$1, %rsi
.L1666:
	movdqu	(%r9,%rax), %xmm2
	addq	$1, %rcx
	movups	%xmm2, (%rdx,%rax)
	addq	$16, %rax
	cmpq	%rcx, %rsi
	ja	.L1666
	leaq	32(%rdx,%rdi), %rsi
.L1667:
	movq	%r9, %rdi
	movq	%rsi, -240(%rbp)
	movq	%r11, -232(%rbp)
	movq	%r8, -224(%rbp)
	movq	%rdx, -176(%rbp)
	call	_ZdlPv@PLT
	movq	-240(%rbp), %rsi
	movq	-232(%rbp), %r11
	movq	-224(%rbp), %r8
	movq	-176(%rbp), %rdx
.L1668:
	movq	%rdx, %xmm0
	movq	%rsi, %xmm7
	movq	%r11, -72(%rbp)
	punpcklqdq	%xmm7, %xmm0
	movups	%xmm0, -88(%rbp)
	jmp	.L1660
.L1680:
	movq	-88(%rbp), %r9
	movq	%r10, %rax
	subq	%r9, %rax
	movq	%rax, -176(%rbp)
	sarq	$4, %rax
	movq	%rax, %rsi
	movabsq	$576460752303423487, %rax
	cmpq	%rax, %rsi
	je	.L1661
	testq	%rsi, %rsi
	je	.L1886
	leaq	(%rsi,%rsi), %rax
	cmpq	%rsi, %rax
	jb	.L1887
	xorl	%r11d, %r11d
	movl	$16, %esi
	testq	%rax, %rax
	jne	.L1888
.L1684:
	movq	-176(%rbp), %rax
	movq	%r13, %xmm0
	movq	%rcx, %xmm6
	punpcklqdq	%xmm6, %xmm0
	movups	%xmm0, (%rdx,%rax)
	cmpq	%r9, %r10
	je	.L1685
	leaq	-16(%r10), %rdi
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	subq	%r9, %rdi
	movq	%rdi, %rsi
	shrq	$4, %rsi
	addq	$1, %rsi
.L1686:
	movdqu	(%r9,%rax), %xmm3
	addq	$1, %rcx
	movups	%xmm3, (%rdx,%rax)
	addq	$16, %rax
	cmpq	%rsi, %rcx
	jb	.L1686
	leaq	32(%rdx,%rdi), %rsi
.L1687:
	movq	%r9, %rdi
	movq	%r8, -240(%rbp)
	movq	%rsi, -232(%rbp)
	movq	%rdx, -224(%rbp)
	movq	%r11, -176(%rbp)
	call	_ZdlPv@PLT
	movq	-240(%rbp), %r8
	movq	-232(%rbp), %rsi
	movq	-224(%rbp), %rdx
	movq	-176(%rbp), %r11
.L1688:
	movq	%rdx, %xmm0
	movq	%rsi, %xmm7
	movq	%r11, -72(%rbp)
	punpcklqdq	%xmm7, %xmm0
	movups	%xmm0, -88(%rbp)
	jmp	.L1681
.L1685:
	testq	%r9, %r9
	je	.L1688
	jmp	.L1687
	.p2align 4,,10
	.p2align 3
.L1882:
	movl	$16, %esi
	movq	%r8, -224(%rbp)
	movq	%rdx, -176(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-176(%rbp), %rdx
	movq	-224(%rbp), %r8
	movq	%rax, %rcx
	jmp	.L1679
.L1665:
	testq	%r9, %r9
	je	.L1668
	jmp	.L1667
	.p2align 4,,10
	.p2align 3
.L1881:
	movl	$16, %esi
	movq	%r8, -224(%rbp)
	movq	%rdx, -176(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-176(%rbp), %rdx
	movq	-224(%rbp), %r8
	movq	%rax, %rcx
	jmp	.L1658
.L1886:
	movl	$16, %r11d
.L1682:
	movq	%r11, %rdi
	movq	%r9, -256(%rbp)
	movq	%rcx, -248(%rbp)
	movq	%r10, -240(%rbp)
	movq	%r8, -232(%rbp)
	movq	%r11, -224(%rbp)
	call	_Znwm@PLT
	movq	-224(%rbp), %r11
	movq	-256(%rbp), %r9
	movq	-248(%rbp), %rcx
	movq	-240(%rbp), %r10
	movq	%rax, %rdx
	leaq	16(%rax), %rsi
	movq	-232(%rbp), %r8
	addq	%rax, %r11
	jmp	.L1684
.L1883:
	movl	$16, %r11d
.L1662:
	movq	%r11, %rdi
	movq	%r9, -256(%rbp)
	movq	%rcx, -248(%rbp)
	movq	%r10, -240(%rbp)
	movq	%r8, -232(%rbp)
	movq	%r11, -224(%rbp)
	call	_Znwm@PLT
	movq	-224(%rbp), %r11
	movq	-256(%rbp), %r9
	movq	-248(%rbp), %rcx
	movq	-240(%rbp), %r10
	movq	%rax, %rdx
	leaq	16(%rax), %rsi
	movq	-232(%rbp), %r8
	addq	%rax, %r11
	jmp	.L1664
.L1884:
	movabsq	$9223372036854775792, %r11
	jmp	.L1662
.L1887:
	movabsq	$9223372036854775792, %r11
	jmp	.L1682
.L1888:
	movabsq	$576460752303423487, %rdx
	cmpq	%rdx, %rax
	cmova	%rdx, %rax
	salq	$4, %rax
	movq	%rax, %r11
	jmp	.L1682
.L1885:
	movabsq	$576460752303423487, %rdx
	cmpq	%rdx, %rax
	cmova	%rdx, %rax
	salq	$4, %rax
	movq	%rax, %r11
	jmp	.L1662
.L1870:
	call	__stack_chk_fail@PLT
.L1859:
	leaq	.LC10(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L1661:
	leaq	.LC3(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1880:
	cmpq	$134217727, %rdx
	movl	$134217727, %eax
	cmova	%rax, %rdx
	salq	$4, %rdx
	movq	%rdx, %rsi
	jmp	.L1611
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal11interpreter17BytecodeGenerator17BuildClassLiteralEPNS0_12ClassLiteralENS1_8RegisterE
	.cfi_startproc
	.type	_ZN2v88internal11interpreter17BytecodeGenerator17BuildClassLiteralEPNS0_12ClassLiteralENS1_8RegisterE.cold, @function
_ZN2v88internal11interpreter17BytecodeGenerator17BuildClassLiteralEPNS0_12ClassLiteralENS1_8RegisterE.cold:
.LFSB20803:
.L1646:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	8, %rax
	ud2
	.cfi_endproc
.LFE20803:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator17BuildClassLiteralEPNS0_12ClassLiteralENS1_8RegisterE
	.size	_ZN2v88internal11interpreter17BytecodeGenerator17BuildClassLiteralEPNS0_12ClassLiteralENS1_8RegisterE, .-_ZN2v88internal11interpreter17BytecodeGenerator17BuildClassLiteralEPNS0_12ClassLiteralENS1_8RegisterE
	.section	.text.unlikely._ZN2v88internal11interpreter17BytecodeGenerator17BuildClassLiteralEPNS0_12ClassLiteralENS1_8RegisterE
	.size	_ZN2v88internal11interpreter17BytecodeGenerator17BuildClassLiteralEPNS0_12ClassLiteralENS1_8RegisterE.cold, .-_ZN2v88internal11interpreter17BytecodeGenerator17BuildClassLiteralEPNS0_12ClassLiteralENS1_8RegisterE.cold
.LCOLDE11:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator17BuildClassLiteralEPNS0_12ClassLiteralENS1_8RegisterE
.LHOTE11:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator17VisitClassLiteralEPNS0_12ClassLiteralENS1_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator17VisitClassLiteralEPNS0_12ClassLiteralENS1_8RegisterE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator17VisitClassLiteralEPNS0_12ClassLiteralENS1_8RegisterE, @function
_ZN2v88internal11interpreter17BytecodeGenerator17VisitClassLiteralEPNS0_12ClassLiteralENS1_8RegisterE:
.LFB20826:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	16(%rsi), %rsi
	movq	536(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L1890
	movq	%rsi, 536(%rdi)
	movq	16(%r12), %rsi
.L1890:
	movl	124(%rsi), %eax
	testl	%eax, %eax
	jle	.L1891
	movq	816(%rbx), %rax
	leaq	-96(%rbp), %r15
	leaq	24(%rbx), %rdi
	movq	%rbx, -88(%rbp)
	movq	%r15, 816(%rbx)
	movq	%rax, -96(%rbp)
	movl	328(%rbx), %eax
	movq	$2, -72(%rbp)
	movl	%eax, -80(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder18CreateBlockContextEPKNS0_5ScopeE@PLT
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rax
	movq	336(%rdx), %rdi
	movq	%rax, 816(%rdx)
	movl	-80(%rbp), %eax
	movl	328(%rdx), %esi
	movl	%eax, 328(%rdx)
	testq	%rdi, %rdi
	je	.L1892
	subl	%eax, %esi
	movq	(%rdi), %rcx
	movq	%rsi, %rdx
	movl	%eax, %esi
	salq	$32, %rdx
	orq	%rdx, %rsi
	call	*32(%rcx)
.L1892:
	movq	16(%r12), %rax
	movq	%rbx, -96(%rbp)
	movq	%rax, -88(%rbp)
	movq	808(%rbx), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal11interpreter8Register15current_contextEv@PLT
	movl	$0, -68(%rbp)
	movl	%eax, -72(%rbp)
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L1893
	movl	28(%rax), %ecx
	movq	-96(%rbp), %rdi
	leal	1(%rcx), %edx
	movl	%edx, -68(%rbp)
	movl	328(%rdi), %esi
	movq	336(%rdi), %r8
	leal	1(%rsi), %edx
	cmpl	%edx, 332(%rdi)
	movl	%edx, 328(%rdi)
	cmovge	332(%rdi), %edx
	movl	%edx, 332(%rdi)
	testq	%r8, %r8
	je	.L1894
	movq	(%r8), %rax
	movl	%esi, -100(%rbp)
	movq	%r8, %rdi
	call	*16(%rax)
	movq	-80(%rbp), %rax
	movq	-96(%rbp), %rdi
	movl	-100(%rbp), %esi
.L1894:
	movl	%esi, 24(%rax)
	addq	$24, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11PushContextENS1_8RegisterE@PLT
.L1893:
	movq	-96(%rbp), %rax
	movl	%r13d, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%r15, 808(%rax)
	call	_ZN2v88internal11interpreter17BytecodeGenerator17BuildClassLiteralEPNS0_12ClassLiteralENS1_8RegisterE
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L1895
	movq	-96(%rbp), %rcx
	movl	24(%rax), %esi
	leaq	24(%rcx), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder10PopContextENS1_8RegisterE@PLT
	movq	-80(%rbp), %rax
	movl	-72(%rbp), %edx
	movl	%edx, 24(%rax)
.L1895:
	movq	-96(%rbp), %rdx
	movq	%rax, 808(%rdx)
.L1896:
	cmpq	536(%rbx), %r14
	je	.L1889
	movq	%r14, 536(%rbx)
.L1889:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1915
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1891:
	.cfi_restore_state
	movl	%r13d, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator17BuildClassLiteralEPNS0_12ClassLiteralENS1_8RegisterE
	jmp	.L1896
.L1915:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20826:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator17VisitClassLiteralEPNS0_12ClassLiteralENS1_8RegisterE, .-_ZN2v88internal11interpreter17BytecodeGenerator17VisitClassLiteralEPNS0_12ClassLiteralENS1_8RegisterE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator25VisitNoStackOverflowCheckEPNS0_7AstNodeE,"axG",@progbits,_ZN2v88internal11interpreter17BytecodeGenerator25VisitNoStackOverflowCheckEPNS0_7AstNodeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11interpreter17BytecodeGenerator25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator25VisitNoStackOverflowCheckEPNS0_7AstNodeE, @function
_ZN2v88internal11interpreter17BytecodeGenerator25VisitNoStackOverflowCheckEPNS0_7AstNodeE:
.LFB10646:
	.cfi_startproc
	endbr64
	movl	4(%rsi), %ecx
	movl	%ecx, %eax
	andl	$63, %eax
	cmpb	$57, %al
	ja	.L1990
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.L1919(%rip), %rdx
	movzbl	%al, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	subq	$8, %rsp
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal11interpreter17BytecodeGenerator25VisitNoStackOverflowCheckEPNS0_7AstNodeE,"aG",@progbits,_ZN2v88internal11interpreter17BytecodeGenerator25VisitNoStackOverflowCheckEPNS0_7AstNodeE,comdat
	.align 4
	.align 4
.L1919:
	.long	.L1972-.L1919
	.long	.L1971-.L1919
	.long	.L1970-.L1919
	.long	.L1969-.L1919
	.long	.L1968-.L1919
	.long	.L1967-.L1919
	.long	.L1966-.L1919
	.long	.L1965-.L1919
	.long	.L1964-.L1919
	.long	.L1963-.L1919
	.long	.L1916-.L1919
	.long	.L1929-.L1919
	.long	.L1961-.L1919
	.long	.L1960-.L1919
	.long	.L1959-.L1919
	.long	.L1958-.L1919
	.long	.L1957-.L1919
	.long	.L1956-.L1919
	.long	.L1955-.L1919
	.long	.L1954-.L1919
	.long	.L1953-.L1919
	.long	.L1952-.L1919
	.long	.L1951-.L1919
	.long	.L1950-.L1919
	.long	.L1949-.L1919
	.long	.L1948-.L1919
	.long	.L1947-.L1919
	.long	.L1946-.L1919
	.long	.L1945-.L1919
	.long	.L1944-.L1919
	.long	.L1943-.L1919
	.long	.L1942-.L1919
	.long	.L1941-.L1919
	.long	.L1940-.L1919
	.long	.L1939-.L1919
	.long	.L1938-.L1919
	.long	.L1937-.L1919
	.long	.L1918-.L1919
	.long	.L1936-.L1919
	.long	.L1935-.L1919
	.long	.L1934-.L1919
	.long	.L1933-.L1919
	.long	.L1932-.L1919
	.long	.L1931-.L1919
	.long	.L1930-.L1919
	.long	.L1918-.L1919
	.long	.L1929-.L1919
	.long	.L1928-.L1919
	.long	.L1918-.L1919
	.long	.L1927-.L1919
	.long	.L1926-.L1919
	.long	.L1925-.L1919
	.long	.L1924-.L1919
	.long	.L1923-.L1919
	.long	.L1922-.L1919
	.long	.L1921-.L1919
	.long	.L1920-.L1919
	.long	.L1918-.L1919
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator25VisitNoStackOverflowCheckEPNS0_7AstNodeE,"axG",@progbits,_ZN2v88internal11interpreter17BytecodeGenerator25VisitNoStackOverflowCheckEPNS0_7AstNodeE,comdat
	.p2align 4,,10
	.p2align 3
.L1918:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1929:
	movq	16(%rsi), %rsi
	addq	$8, %rsp
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator5VisitEPNS0_7AstNodeE
	.p2align 4,,10
	.p2align 3
.L1930:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator13VisitPropertyEPNS0_8PropertyE
	.p2align 4,,10
	.p2align 3
.L1953:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator36VisitInitializeClassMembersStatementEPNS0_31InitializeClassMembersStatementE
	.p2align 4,,10
	.p2align 3
.L1954:
	.cfi_restore_state
	movl	(%rsi), %eax
	leaq	24(%rdi), %rdi
	cmpl	$-1, %eax
	je	.L1974
	movb	$2, 496(%r13)
	movl	%eax, 500(%r13)
.L1974:
	addq	$8, %rsp
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder8DebuggerEv@PLT
	.p2align 4,,10
	.p2align 3
.L1955:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator24VisitTryFinallyStatementEPNS0_19TryFinallyStatementE
	.p2align 4,,10
	.p2align 3
.L1956:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator22VisitTryCatchStatementEPNS0_17TryCatchStatementE
	.p2align 4,,10
	.p2align 3
.L1957:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator18VisitWithStatementEPNS0_13WithStatementE
	.p2align 4,,10
	.p2align 3
.L1958:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator20VisitReturnStatementEPNS0_15ReturnStatementE
	.p2align 4,,10
	.p2align 3
.L1937:
	.cfi_restore_state
	movq	8(%rsi), %rsi
	call	_ZN2v88internal11interpreter17BytecodeGenerator10VisitBlockEPNS0_5BlockE
	movq	16(%r12), %rax
	movl	(%rax), %edx
	cmpl	$-1, %edx
	je	.L1975
	cmpb	$2, 496(%r13)
	je	.L1975
	movb	$1, 496(%r13)
	movl	%edx, 500(%r13)
.L1975:
	movl	4(%rax), %edx
	movq	8(%rax), %rsi
	shrl	$11, %edx
	andl	$1, %edx
.L1993:
	addq	$8, %rsp
	movq	%r13, %rdi
	movl	$1, %ecx
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator17BuildVariableLoadEPNS0_8VariableENS0_13HoleCheckModeENS0_10TypeofModeE
	.p2align 4,,10
	.p2align 3
.L1938:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator19VisitCountOperationEPNS0_14CountOperationE
	.p2align 4,,10
	.p2align 3
.L1931:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator18VisitOptionalChainEPNS0_13OptionalChainE
	.p2align 4,,10
	.p2align 3
.L1932:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator26VisitNativeFunctionLiteralEPNS0_21NativeFunctionLiteralE
	.p2align 4,,10
	.p2align 3
.L1933:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator12VisitLiteralEPNS0_7LiteralE
	.p2align 4,,10
	.p2align 3
.L1934:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator25VisitImportCallExpressionEPNS0_20ImportCallExpressionE
	.p2align 4,,10
	.p2align 3
.L1935:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator22VisitGetTemplateObjectEPNS0_17GetTemplateObjectE
	.p2align 4,,10
	.p2align 3
.L1936:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator20VisitFunctionLiteralEPNS0_15FunctionLiteralE
	.p2align 4,,10
	.p2align 3
.L1921:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator10VisitYieldEPNS0_5YieldE
	.p2align 4,,10
	.p2align 3
.L1922:
	.cfi_restore_state
	movl	(%rsi), %eax
	cmpl	$-1, %eax
	je	.L1976
	cmpb	$2, 496(%rdi)
	je	.L1976
	movb	$1, 496(%rdi)
	movl	%eax, 500(%rdi)
	movl	4(%rsi), %ecx
.L1976:
	shrl	$11, %ecx
	movq	8(%r12), %rsi
	movl	%ecx, %edx
	andl	$1, %edx
	jmp	.L1993
	.p2align 4,,10
	.p2align 3
.L1945:
	addq	$8, %rsp
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator9VisitCallEPNS0_4CallE
	.p2align 4,,10
	.p2align 3
.L1946:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator18VisitNaryOperationEPNS0_13NaryOperationE
	.p2align 4,,10
	.p2align 3
.L1947:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator20VisitBinaryOperationEPNS0_15BinaryOperationE
	.p2align 4,,10
	.p2align 3
.L1948:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator10VisitAwaitEPNS0_5AwaitE
	.p2align 4,,10
	.p2align 3
.L1971:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator24VisitFunctionDeclarationEPNS0_19FunctionDeclarationE
	.p2align 4,,10
	.p2align 3
.L1972:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator24VisitVariableDeclarationEPNS0_19VariableDeclarationE
	.p2align 4,,10
	.p2align 3
.L1967:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator19VisitForInStatementEPNS0_14ForInStatementE
	.p2align 4,,10
	.p2align 3
.L1968:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator17VisitForStatementEPNS0_12ForStatementE
	.p2align 4,,10
	.p2align 3
.L1969:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator19VisitWhileStatementEPNS0_14WhileStatementE
	.p2align 4,,10
	.p2align 3
.L1970:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator21VisitDoWhileStatementEPNS0_16DoWhileStatementE
	.p2align 4,,10
	.p2align 3
.L1959:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator19VisitBreakStatementEPNS0_14BreakStatementE
	.p2align 4,,10
	.p2align 3
.L1960:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator22VisitContinueStatementEPNS0_17ContinueStatementE
	.p2align 4,,10
	.p2align 3
.L1961:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator16VisitIfStatementEPNS0_11IfStatementE
	.p2align 4,,10
	.p2align 3
.L1920:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator14VisitYieldStarEPNS0_9YieldStarE
	.p2align 4,,10
	.p2align 3
.L1963:
	.cfi_restore_state
	movl	(%rsi), %eax
	cmpl	$-1, %eax
	je	.L1973
	movb	$2, 496(%rdi)
	movl	%eax, 500(%rdi)
.L1973:
	movq	8(%r12), %rsi
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator14VisitForEffectEPNS0_10ExpressionE
	.p2align 4,,10
	.p2align 3
.L1964:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator20VisitSwitchStatementEPNS0_15SwitchStatementE
	.p2align 4,,10
	.p2align 3
.L1965:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator10VisitBlockEPNS0_5BlockE
	.p2align 4,,10
	.p2align 3
.L1966:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator19VisitForOfStatementEPNS0_14ForOfStatementE
	.p2align 4,,10
	.p2align 3
.L1949:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator15VisitAssignmentEPNS0_10AssignmentE
	.p2align 4,,10
	.p2align 3
.L1950:
	.cfi_restore_state
	movq	%rsi, %rdi
	call	_ZN2v88internal12ArrayLiteral17InitDepthAndFlagsEv@PLT
	addq	$8, %rsp
	movq	%r12, %rdx
	movq	%r13, %rdi
	leaq	24(%r12), %rsi
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator23BuildCreateArrayLiteralEPKNS0_8ZoneListIPNS0_10ExpressionEEEPNS0_12ArrayLiteralE
	.p2align 4,,10
	.p2align 3
.L1951:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator18VisitObjectLiteralEPNS0_13ObjectLiteralE
	.p2align 4,,10
	.p2align 3
.L1952:
	.cfi_restore_state
	movq	512(%rdi), %rax
	movl	8(%rsi), %r14d
	movl	$19, %esi
	leaq	56(%rax), %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movq	16(%r12), %rsi
	addq	$8, %rsp
	leaq	24(%r13), %rdi
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	movl	%r14d, %ecx
	popq	%r13
	.cfi_restore 13
	movl	%eax, %edx
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder19CreateRegExpLiteralEPKNS0_12AstRawStringEii@PLT
	.p2align 4,,10
	.p2align 3
.L1941:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator21VisitCompareOperationEPNS0_16CompareOperationE
	.p2align 4,,10
	.p2align 3
.L1942:
	.cfi_restore_state
	addq	$8, %rsp
	movl	$2147483647, %edx
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator17VisitClassLiteralEPNS0_12ClassLiteralENS1_8RegisterE
	.p2align 4,,10
	.p2align 3
.L1943:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator16VisitCallRuntimeEPNS0_11CallRuntimeE
	.p2align 4,,10
	.p2align 3
.L1944:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator12VisitCallNewEPNS0_7CallNewE
	.p2align 4,,10
	.p2align 3
.L1939:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator16VisitConditionalEPNS0_11ConditionalE
	.p2align 4,,10
	.p2align 3
.L1940:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator23VisitCompoundAssignmentEPNS0_18CompoundAssignmentE
	.p2align 4,,10
	.p2align 3
.L1923:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator19VisitUnaryOperationEPNS0_14UnaryOperationE
	.p2align 4,,10
	.p2align 3
.L1924:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator10VisitThrowEPNS0_5ThrowE
	.p2align 4,,10
	.p2align 3
.L1925:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator21BuildThisVariableLoadEv
	.p2align 4,,10
	.p2align 3
.L1926:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator20VisitTemplateLiteralEPNS0_15TemplateLiteralE
	.p2align 4,,10
	.p2align 3
.L1927:
	.cfi_restore_state
	addq	$8, %rsp
	leaq	24(%rdi), %rdi
	movl	$45, %esi
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdE@PLT
	.p2align 4,,10
	.p2align 3
.L1928:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator24VisitStoreInArrayLiteralEPNS0_19StoreInArrayLiteralE
.L1916:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
.L1990:
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.cfi_endproc
.LFE10646:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator25VisitNoStackOverflowCheckEPNS0_7AstNodeE, .-_ZN2v88internal11interpreter17BytecodeGenerator25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator5VisitEPNS0_7AstNodeE,"axG",@progbits,_ZN2v88internal11interpreter17BytecodeGenerator5VisitEPNS0_7AstNodeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11interpreter17BytecodeGenerator5VisitEPNS0_7AstNodeE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator5VisitEPNS0_7AstNodeE, @function
_ZN2v88internal11interpreter17BytecodeGenerator5VisitEPNS0_7AstNodeE:
.LFB10647:
	.cfi_startproc
	endbr64
	cmpb	$0, 8(%rdi)
	je	.L2001
	ret
	.p2align 4,,10
	.p2align 3
.L2001:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L2002
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	.p2align 4,,10
	.p2align 3
.L2002:
	.cfi_restore_state
	movb	$1, 8(%r12)
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10647:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator5VisitEPNS0_7AstNodeE, .-_ZN2v88internal11interpreter17BytecodeGenerator5VisitEPNS0_7AstNodeE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator15VisitStatementsEPKNS0_8ZoneListIPNS0_9StatementEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator15VisitStatementsEPKNS0_8ZoneListIPNS0_9StatementEEE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator15VisitStatementsEPKNS0_8ZoneListIPNS0_9StatementEEE, @function
_ZN2v88internal11interpreter17BytecodeGenerator15VisitStatementsEPKNS0_8ZoneListIPNS0_9StatementEEE:
.LFB20756:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	12(%rsi), %eax
	testl	%eax, %eax
	jle	.L2003
	.p2align 4,,10
	.p2align 3
.L2004:
	xorl	%esi, %esi
	cmpb	$0, 8(%rbx)
	movl	328(%rbx), %r12d
	jne	.L2006
	movq	0(%r13), %rax
	movq	(%rax,%r14,8), %r15
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%rbx), %rax
	jb	.L2019
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movl	328(%rbx), %esi
	subl	%r12d, %esi
.L2006:
	cmpb	$0, 482(%rbx)
	movl	%r12d, 328(%rbx)
	movq	336(%rbx), %rdi
	jne	.L2008
	testq	%rdi, %rdi
	je	.L2009
	movq	(%rdi), %rax
	salq	$32, %rsi
	addq	$1, %r14
	orq	%r12, %rsi
	call	*32(%rax)
	cmpl	%r14d, 12(%r13)
	jg	.L2004
.L2003:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2009:
	.cfi_restore_state
	addq	$1, %r14
	cmpl	%r14d, 12(%r13)
	jg	.L2004
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2019:
	.cfi_restore_state
	movl	328(%rbx), %esi
	movb	$1, 8(%rbx)
	subl	%r12d, %esi
	jmp	.L2006
	.p2align 4,,10
	.p2align 3
.L2008:
	testq	%rdi, %rdi
	je	.L2003
	movq	(%rdi), %rax
	salq	$32, %rsi
	orq	%r12, %rsi
	movq	32(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE20756:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator15VisitStatementsEPKNS0_8ZoneListIPNS0_9StatementEEE, .-_ZN2v88internal11interpreter17BytecodeGenerator15VisitStatementsEPKNS0_8ZoneListIPNS0_9StatementEEE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator11VisitSpreadEPNS0_6SpreadE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator11VisitSpreadEPNS0_6SpreadE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator11VisitSpreadEPNS0_6SpreadE, @function
_ZN2v88internal11interpreter17BytecodeGenerator11VisitSpreadEPNS0_6SpreadE:
.LFB20955:
	.cfi_startproc
	endbr64
	cmpb	$0, 8(%rdi)
	je	.L2027
	ret
	.p2align 4,,10
	.p2align 3
.L2027:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	16(%rsi), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L2028
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	.p2align 4,,10
	.p2align 3
.L2028:
	.cfi_restore_state
	movb	$1, 8(%r12)
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20955:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator11VisitSpreadEPNS0_6SpreadE, .-_ZN2v88internal11interpreter17BytecodeGenerator11VisitSpreadEPNS0_6SpreadE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator33VisitSloppyBlockFunctionStatementEPNS0_28SloppyBlockFunctionStatementE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator33VisitSloppyBlockFunctionStatementEPNS0_28SloppyBlockFunctionStatementE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator33VisitSloppyBlockFunctionStatementEPNS0_28SloppyBlockFunctionStatementE, @function
_ZN2v88internal11interpreter17BytecodeGenerator33VisitSloppyBlockFunctionStatementEPNS0_28SloppyBlockFunctionStatementE:
.LFB20760:
	.cfi_startproc
	endbr64
	cmpb	$0, 8(%rdi)
	je	.L2036
	ret
	.p2align 4,,10
	.p2align 3
.L2036:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	16(%rsi), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L2037
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	.p2align 4,,10
	.p2align 3
.L2037:
	.cfi_restore_state
	movb	$1, 8(%r12)
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20760:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator33VisitSloppyBlockFunctionStatementEPNS0_28SloppyBlockFunctionStatementE, .-_ZN2v88internal11interpreter17BytecodeGenerator33VisitSloppyBlockFunctionStatementEPNS0_28SloppyBlockFunctionStatementE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator17VisitDeclarationsEPNS_4base16ThreadedListBaseINS0_11DeclarationENS3_9EmptyBaseENS3_18ThreadedListTraitsIS5_EEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator17VisitDeclarationsEPNS_4base16ThreadedListBaseINS0_11DeclarationENS3_9EmptyBaseENS3_18ThreadedListTraitsIS5_EEEE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator17VisitDeclarationsEPNS_4base16ThreadedListBaseINS0_11DeclarationENS3_9EmptyBaseENS3_18ThreadedListTraitsIS5_EEEE, @function
_ZN2v88internal11interpreter17BytecodeGenerator17VisitDeclarationsEPNS_4base16ThreadedListBaseINS0_11DeclarationENS3_9EmptyBaseENS3_18ThreadedListTraitsIS5_EEEE:
.LFB20755:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movl	328(%rdi), %r13d
	movq	8(%rsi), %r14
	movl	%r13d, -52(%rbp)
	cmpq	%r14, %rsi
	je	.L2039
	movq	%rsi, %r12
	jmp	.L2045
	.p2align 4,,10
	.p2align 3
.L2087:
	movq	%rsi, %rax
	movq	(%rdi), %rdx
	movl	%r13d, %esi
	salq	$32, %rax
	orq	%rax, %rsi
	call	*32(%rdx)
	movq	(%r12), %r12
	addq	$16, %r12
	cmpq	%r14, %r12
	je	.L2039
.L2043:
	movl	328(%rbx), %r13d
.L2045:
	xorl	%esi, %esi
	cmpb	$0, 8(%rbx)
	jne	.L2040
	movq	(%r12), %r15
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%rbx), %rax
	jb	.L2086
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movl	328(%rbx), %esi
	subl	%r13d, %esi
.L2040:
	movq	336(%rbx), %rdi
	movl	%r13d, 328(%rbx)
	testq	%rdi, %rdi
	jne	.L2087
	movq	(%r12), %r12
	addq	$16, %r12
	cmpq	%r14, %r12
	jne	.L2043
	movq	560(%rbx), %r12
	movq	8(%r12), %rax
	cmpq	%rax, 16(%r12)
	jne	.L2046
	movl	-52(%rbp), %eax
	movl	%eax, 328(%rbx)
.L2038:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2086:
	.cfi_restore_state
	movl	328(%rbx), %esi
	movb	$1, 8(%rbx)
	subl	%r13d, %esi
	jmp	.L2040
	.p2align 4,,10
	.p2align 3
.L2039:
	movq	560(%rbx), %r12
	movq	8(%r12), %rax
	cmpq	%rax, 16(%r12)
	je	.L2088
.L2046:
	leaq	24(%rbx), %r8
	movq	%r8, %rdi
	movq	%r8, -64(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder33AllocateDeferredConstantPoolEntryEv@PLT
	movb	$1, 40(%r12)
	movq	-64(%rbp), %r8
	movq	%rax, 32(%r12)
	movq	512(%rbx), %rax
	movl	328(%rbx), %r14d
	movq	336(%rbx), %rdi
	movl	(%rax), %r13d
	leal	3(%r14), %eax
	movq	%r14, %r12
	movl	%eax, 328(%rbx)
	andl	$1, %r13d
	cmpl	%eax, 332(%rbx)
	cmovge	332(%rbx), %eax
	testq	%rdi, %rdi
	movl	%eax, 332(%rbx)
	je	.L2049
	movabsq	$12884901888, %rsi
	movq	(%rdi), %rax
	orq	%r14, %rsi
	call	*24(%rax)
	movq	-64(%rbp), %r8
.L2049:
	movabsq	$12884901888, %rax
	movq	%r8, %rdi
	orq	%rax, %r14
	movq	560(%rbx), %rax
	movq	32(%rax), %rsi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder21LoadConstantPoolEntryEm@PLT
	movl	%r12d, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movl	%r13d, %esi
	movq	%rax, %rdi
	salq	$32, %rsi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralENS0_3SmiE@PLT
	leal	1(%r12), %esi
	addl	$2, %r12d
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter8Register16function_closureEv@PLT
	movl	%r12d, %edx
	movq	%r13, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder12MoveRegisterENS1_8RegisterES3_@PLT
	movq	%r14, %rdx
	movl	$301, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_12RegisterListE@PLT
	movq	560(%rbx), %r14
	movq	592(%rbx), %r12
	cmpq	600(%rbx), %r12
	je	.L2050
	movq	%r14, (%r12)
	addq	$8, 592(%rbx)
.L2051:
	movq	16(%rbx), %r12
	movq	16(%r12), %rax
	movq	24(%r12), %rdx
	subq	%rax, %rdx
	cmpq	$47, %rdx
	jbe	.L2089
	leaq	48(%rax), %rdx
	movq	%rdx, 16(%r12)
.L2063:
	movq	%r12, (%rax)
	movl	-52(%rbp), %ecx
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movb	$0, 40(%rax)
	movq	336(%rbx), %rdi
	movl	328(%rbx), %esi
	movq	%rax, 560(%rbx)
	movl	%ecx, 328(%rbx)
	testq	%rdi, %rdi
	je	.L2038
	movq	(%rdi), %rax
	subl	%ecx, %esi
	movl	%ecx, %r15d
	salq	$32, %rsi
	movq	32(%rax), %rax
	addq	$40, %rsp
	orq	%r15, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L2088:
	.cfi_restore_state
	movl	-52(%rbp), %ecx
	movq	336(%rbx), %rdi
	movl	328(%rbx), %eax
	movl	%ecx, 328(%rbx)
	testq	%rdi, %rdi
	je	.L2038
	movq	(%rdi), %rdx
	subl	%ecx, %eax
	movl	%ecx, %esi
	salq	$32, %rax
	movq	32(%rdx), %rdx
	addq	$40, %rsp
	orq	%rax, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rdx
	.p2align 4,,10
	.p2align 3
.L2050:
	.cfi_restore_state
	movq	584(%rbx), %r13
	movq	%r12, %r8
	subq	%r13, %r8
	movq	%r8, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L2090
	testq	%rax, %rax
	je	.L2067
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L2091
	movl	$2147483640, %esi
	movl	$2147483640, %ecx
.L2053:
	movq	576(%rbx), %rdi
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	subq	%rdx, %rax
	cmpq	%rax, %rsi
	ja	.L2092
	addq	%rdx, %rsi
	movq	%rsi, 16(%rdi)
.L2056:
	leaq	(%rdx,%rcx), %rsi
	leaq	8(%rdx), %rax
	jmp	.L2054
.L2091:
	testq	%rdx, %rdx
	jne	.L2093
	movl	$8, %eax
	xorl	%esi, %esi
	xorl	%edx, %edx
.L2054:
	movq	%r14, (%rdx,%r8)
	cmpq	%r13, %r12
	je	.L2057
	subq	$8, %r12
	leaq	15(%rdx), %rax
	subq	%r13, %r12
	subq	%r13, %rax
	movq	%r12, %rcx
	shrq	$3, %rcx
	cmpq	$30, %rax
	jbe	.L2070
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rcx
	je	.L2070
	leaq	1(%rcx), %rdi
	xorl	%eax, %eax
	movq	%rdi, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L2059:
	movdqu	0(%r13,%rax), %xmm1
	movups	%xmm1, (%rdx,%rax)
	addq	$16, %rax
	cmpq	%rcx, %rax
	jne	.L2059
	movq	%rdi, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rcx
	addq	%rcx, %r13
	addq	%rdx, %rcx
	cmpq	%rax, %rdi
	je	.L2061
	movq	0(%r13), %rax
	movq	%rax, (%rcx)
.L2061:
	leaq	16(%rdx,%r12), %rax
.L2057:
	movq	%rdx, %xmm0
	movq	%rax, %xmm2
	movq	%rsi, 600(%rbx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 584(%rbx)
	jmp	.L2051
.L2089:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L2063
.L2067:
	movl	$8, %esi
	movl	$8, %ecx
	jmp	.L2053
.L2070:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L2058:
	movq	0(%r13,%rax,8), %rdi
	movq	%rdi, (%rdx,%rax,8)
	movq	%rax, %rdi
	addq	$1, %rax
	cmpq	%rdi, %rcx
	jne	.L2058
	jmp	.L2061
.L2092:
	movq	%r8, -72(%rbp)
	movq	%rcx, -64(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %r8
	movq	%rax, %rdx
	jmp	.L2056
.L2093:
	movl	$268435455, %ecx
	cmpq	$268435455, %rdx
	cmova	%rcx, %rdx
	leaq	0(,%rdx,8), %rcx
	movq	%rcx, %rsi
	jmp	.L2053
.L2090:
	leaq	.LC3(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE20755:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator17VisitDeclarationsEPNS_4base16ThreadedListBaseINS0_11DeclarationENS3_9EmptyBaseENS3_18ThreadedListTraitsIS5_EEEE, .-_ZN2v88internal11interpreter17BytecodeGenerator17VisitDeclarationsEPNS_4base16ThreadedListBaseINS0_11DeclarationENS3_9EmptyBaseENS3_18ThreadedListTraitsIS5_EEEE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator20GenerateBytecodeBodyEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator20GenerateBytecodeBodyEv
	.type	_ZN2v88internal11interpreter17BytecodeGenerator20GenerateBytecodeBodyEv, @function
_ZN2v88internal11interpreter17BytecodeGenerator20GenerateBytecodeBodyEv:
.LFB20747:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	24(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	528(%rdi), %rax
	movq	200(%rax), %r14
	testq	%r14, %r14
	je	.L2095
	movl	$1, %esi
	testb	$1, 129(%rax)
	je	.L2171
.L2096:
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder15CreateArgumentsENS0_19CreateArgumentsTypeE@PLT
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movq	%r14, %rsi
	movl	$17, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator23BuildVariableAssignmentEPNS0_8VariableENS0_5Token5ValueENS0_13HoleCheckModeENS0_18LookupHoistingModeE
	movq	528(%r12), %rax
.L2095:
	testb	$8, 131(%rax)
	je	.L2097
	movl	156(%rax), %ecx
	leal	-1(%rcx), %edx
	movq	144(%rax), %rcx
	movslq	%edx, %rdx
	movq	(%rcx,%rdx,8), %r14
	testq	%r14, %r14
	je	.L2097
	movl	$2, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder15CreateArgumentsENS0_19CreateArgumentsTypeE@PLT
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movq	%r14, %rsi
	movl	$17, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator23BuildVariableAssignmentEPNS0_8VariableENS0_5Token5ValueENS0_13HoleCheckModeENS0_18LookupHoistingModeE
	movq	528(%r12), %rax
.L2097:
	movq	184(%rax), %r14
	testq	%r14, %r14
	je	.L2098
	call	_ZN2v88internal11interpreter8Register16function_closureEv@PLT
	movq	%r13, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movq	%r14, %rsi
	movl	$16, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator23BuildVariableAssignmentEPNS0_8VariableENS0_5Token5ValueENS0_13HoleCheckModeENS0_18LookupHoistingModeE
	movq	528(%r12), %rax
.L2098:
	movq	216(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2099
	movq	(%rdx), %r14
	testq	%r14, %r14
	je	.L2099
	call	_ZN2v88internal11interpreter8Register16function_closureEv@PLT
	movq	%r13, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movq	%r14, %rsi
	movl	$16, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator23BuildVariableAssignmentEPNS0_8VariableENS0_5Token5ValueENS0_13HoleCheckModeENS0_18LookupHoistingModeE
	movq	528(%r12), %rax
.L2099:
	movq	192(%rax), %r15
	movq	512(%r12), %rax
	movq	16(%rax), %r14
	testq	%r15, %r15
	je	.L2100
	movq	%r14, %rdi
	call	_ZNK2v88internal15FunctionLiteral4kindEv@PLT
	leal	-9(%rax), %edx
	cmpb	$6, %dl
	jbe	.L2170
	cmpb	$1, %al
	je	.L2170
	movzwl	40(%r15), %eax
	sarl	$7, %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L2170
	movl	824(%r12), %esi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movq	%r15, %rsi
	movl	$16, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator23BuildVariableAssignmentEPNS0_8VariableENS0_5Token5ValueENS0_13HoleCheckModeENS0_18LookupHoistingModeE
	.p2align 4,,10
	.p2align 3
.L2170:
	movq	512(%r12), %rax
	movq	16(%rax), %r14
.L2100:
	movq	%r14, %rdi
	call	_ZNK2v88internal15FunctionLiteral4kindEv@PLT
	leal	-9(%rax), %edx
	cmpb	$6, %dl
	jbe	.L2128
	cmpb	$1, %al
	jne	.L2104
.L2128:
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator42BuildGeneratorObjectVariableInitializationEv
.L2104:
	cmpb	$0, _ZN2v88internal10FLAG_traceE(%rip)
	jne	.L2172
.L2106:
	movq	512(%r12), %rdi
	testb	$2, (%rdi)
	jne	.L2107
.L2110:
	movq	568(%r12), %rbx
	testq	%rbx, %rbx
	je	.L2112
	movq	40(%rbx), %rax
	leaq	16(%rax), %rcx
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L2112
	movq	%rcx, %rdx
	jmp	.L2114
	.p2align 4,,10
	.p2align 3
.L2173:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L2115
.L2114:
	cmpq	%r14, 32(%rax)
	jnb	.L2173
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L2114
.L2115:
	cmpq	%rdx, %rcx
	je	.L2112
	cmpq	%r14, 32(%rdx)
	ja	.L2112
	movq	40(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L2112
	movq	(%rdi), %rax
	xorl	%esi, %esi
	call	*16(%rax)
	movq	%rax, -64(%rbp)
	cmpl	$-1, %eax
	je	.L2112
	movq	16(%rbx), %r15
	subq	8(%rbx), %r15
	leaq	-64(%rbp), %rsi
	movq	%rbx, %rdi
	sarq	$3, %r15
	call	_ZNSt6vectorIN2v88internal11SourceRangeENS1_13ZoneAllocatorIS2_EEE12emplace_backIJRS2_EEEvDpOT_
	cmpl	$-1, %r15d
	je	.L2112
	movq	32(%rbx), %rdi
	movl	%r15d, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder15IncBlockCounterEi@PLT
.L2112:
	movq	528(%r12), %rax
	movq	%r12, %rdi
	leaq	88(%rax), %rsi
	call	_ZN2v88internal11interpreter17BytecodeGenerator17VisitDeclarationsEPNS_4base16ThreadedListBaseINS0_11DeclarationENS3_9EmptyBaseENS3_18ThreadedListTraitsIS5_EEEE
	movq	528(%r12), %rax
	cmpb	$3, 128(%rax)
	je	.L2174
.L2119:
	movq	%r14, %rdi
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	movq	%r13, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder10StackCheckEi@PLT
	movq	512(%r12), %rax
	movq	16(%rax), %rdi
	call	_ZNK2v88internal15FunctionLiteral4kindEv@PLT
	subl	$2, %eax
	cmpb	$1, %al
	jbe	.L2175
.L2121:
	leaq	48(%r14), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator15VisitStatementsEPKNS0_8ZoneListIPNS0_9StatementEEE
	cmpb	$0, 482(%r12)
	je	.L2176
.L2094:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2177
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2171:
	.cfi_restore_state
	movzbl	131(%rax), %esi
	andl	$1, %esi
	xorl	$1, %esi
	jmp	.L2096
	.p2align 4,,10
	.p2align 3
.L2174:
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator27VisitModuleNamespaceImportsEv.part.0
	jmp	.L2119
	.p2align 4,,10
	.p2align 3
.L2107:
	addq	$56, %rdi
	call	_ZN2v88internal18FeedbackVectorSpec18AddTypeProfileSlotEv@PLT
	movq	528(%r12), %rax
	movl	136(%rax), %eax
	testl	%eax, %eax
	jle	.L2110
	leal	-1(%rax), %r15d
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L2111:
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal11interpreter20BytecodeArrayBuilder9ParameterEi@PLT
	movq	%r13, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	movq	%rax, %rdi
	movq	528(%r12), %rax
	movq	144(%rax), %rax
	movq	(%rax,%rbx,8), %rax
	movl	36(%rax), %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder18CollectTypeProfileEi@PLT
	movq	%rbx, %rax
	addq	$1, %rbx
	cmpq	%r15, %rax
	jne	.L2111
	jmp	.L2110
	.p2align 4,,10
	.p2align 3
.L2172:
	movl	$430, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdE@PLT
	jmp	.L2106
	.p2align 4,,10
	.p2align 3
.L2176:
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder13LoadUndefinedEv@PLT
	movl	$-1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator11BuildReturnEi
	jmp	.L2094
	.p2align 4,,10
	.p2align 3
.L2175:
	movq	%r14, %rdi
	call	_ZNK2v88internal15FunctionLiteral29requires_brand_initializationEv@PLT
	testb	%al, %al
	jne	.L2178
.L2122:
	testb	$16, 6(%r14)
	je	.L2121
	movq	%r13, %rdi
	call	_ZNK2v88internal11interpreter20BytecodeArrayBuilder8ReceiverEv@PLT
	movl	%eax, %r15d
	call	_ZN2v88internal11interpreter8Register16function_closureEv@PLT
	movl	%r15d, %edx
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal11interpreter17BytecodeGenerator33BuildInstanceMemberInitializationENS1_8RegisterES3_
	jmp	.L2121
	.p2align 4,,10
	.p2align 3
.L2178:
	movq	%r13, %rdi
	call	_ZNK2v88internal11interpreter20BytecodeArrayBuilder8ReceiverEv@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal11interpreter17BytecodeGenerator31BuildPrivateBrandInitializationENS1_8RegisterE
	jmp	.L2122
.L2177:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20747:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator20GenerateBytecodeBodyEv, .-_ZN2v88internal11interpreter17BytecodeGenerator20GenerateBytecodeBodyEv
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator16GenerateBytecodeEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator16GenerateBytecodeEm
	.type	_ZN2v88internal11interpreter17BytecodeGenerator16GenerateBytecodeEm, @function
_ZN2v88internal11interpreter17BytecodeGenerator16GenerateBytecodeEm:
.LFB20746:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	528(%rdi), %rax
	movq	%rsi, (%rdi)
	movb	$0, 8(%rdi)
	movq	%rax, -136(%rbp)
	movq	808(%rdi), %rax
	movq	%rdi, -144(%rbp)
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal11interpreter8Register15current_contextEv@PLT
	movl	$0, -116(%rbp)
	movl	%eax, -120(%rbp)
	movq	-128(%rbp), %rax
	testq	%rax, %rax
	je	.L2180
	movq	-144(%rbp), %rdi
	movl	28(%rax), %ecx
	movl	328(%rdi), %r12d
	leal	1(%rcx), %edx
	movq	336(%rdi), %r8
	movl	%edx, -116(%rbp)
	leal	1(%r12), %edx
	cmpl	%edx, 332(%rdi)
	movl	%edx, 328(%rdi)
	cmovge	332(%rdi), %edx
	movl	%edx, 332(%rdi)
	testq	%r8, %r8
	je	.L2181
	movq	(%r8), %rax
	movq	%r8, %rdi
	movl	%r12d, %esi
	call	*16(%rax)
	movq	-128(%rbp), %rax
	movq	-144(%rbp), %rdi
.L2181:
	movl	%r12d, 24(%rax)
	addq	$24, %rdi
	movl	%r12d, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11PushContextENS1_8RegisterE@PLT
.L2180:
	movq	-144(%rbp), %rax
	leaq	-144(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%rbx, -104(%rbp)
	movq	%rdx, 808(%rax)
	movq	800(%rbx), %rax
	movl	328(%rbx), %r12d
	movq	%rax, -96(%rbp)
	movq	808(%rbx), %rax
	movq	%rax, -88(%rbp)
	leaq	-112(%rbp), %rax
	movq	%rax, 800(%rbx)
	leaq	16+_ZTVN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTopLevelE(%rip), %rax
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal11interpreter17BytecodeGenerator25AllocateTopLevelRegistersEv
	movq	512(%rbx), %rax
	movq	16(%rax), %rax
	movl	24(%rax), %esi
	testl	%esi, %esi
	jle	.L2182
	leaq	24(%rbx), %r13
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder17AllocateJumpTableEii@PLT
	movl	824(%rbx), %esi
	movq	%r13, %rdi
	movq	%rax, 856(%rbx)
	movq	%rax, %rdx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder22SwitchOnGeneratorStateENS1_8RegisterEPNS1_17BytecodeJumpTableE@PLT
.L2182:
	movq	528(%rbx), %rax
	movq	%rbx, %rdi
	movl	124(%rax), %eax
	testl	%eax, %eax
	jle	.L2183
	call	_ZN2v88internal11interpreter17BytecodeGenerator30BuildNewLocalActivationContextEv
	movq	528(%rbx), %rax
	movq	%rbx, -80(%rbp)
	movq	%rax, -72(%rbp)
	movq	808(%rbx), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal11interpreter8Register15current_contextEv@PLT
	movl	$0, -52(%rbp)
	movl	%eax, -56(%rbp)
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L2184
	movq	-80(%rbp), %rdi
	movl	28(%rax), %ecx
	movl	328(%rdi), %r13d
	leal	1(%rcx), %edx
	movq	336(%rdi), %r8
	movl	%edx, -52(%rbp)
	leal	1(%r13), %edx
	cmpl	%edx, 332(%rdi)
	movl	%edx, 328(%rdi)
	cmovge	332(%rdi), %edx
	movl	%edx, 332(%rdi)
	testq	%r8, %r8
	je	.L2185
	movq	(%r8), %rax
	movq	%r8, %rdi
	movl	%r13d, %esi
	call	*16(%rax)
	movq	-64(%rbp), %rax
	movq	-80(%rbp), %rdi
.L2185:
	movl	%r13d, 24(%rax)
	addq	$24, %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11PushContextENS1_8RegisterE@PLT
.L2184:
	movq	-80(%rbp), %rax
	leaq	-80(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%rdx, 808(%rax)
	call	_ZN2v88internal11interpreter17BytecodeGenerator41BuildLocalActivationContextInitializationEv
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator20GenerateBytecodeBodyEv
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L2186
	movq	-80(%rbp), %rcx
	movl	24(%rax), %esi
	leaq	24(%rcx), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder10PopContextENS1_8RegisterE@PLT
	movq	-64(%rbp), %rax
	movl	-56(%rbp), %edx
	movl	%edx, 24(%rax)
.L2186:
	movq	-80(%rbp), %rdx
	movq	%rax, 808(%rdx)
.L2187:
	movq	336(%rbx), %rdi
	movl	328(%rbx), %esi
	movl	%r12d, 328(%rbx)
	testq	%rdi, %rdi
	je	.L2188
	subl	%r12d, %esi
	movq	(%rdi), %rdx
	movq	%rsi, %rax
	movl	%r12d, %esi
	salq	$32, %rax
	orq	%rax, %rsi
	call	*32(%rdx)
.L2188:
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	-144(%rbp), %rdi
	movq	%rdx, 800(%rax)
	movq	-128(%rbp), %rax
	testq	%rax, %rax
	je	.L2189
	movl	24(%rax), %esi
	addq	$24, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder10PopContextENS1_8RegisterE@PLT
	movq	-128(%rbp), %rax
	movl	-120(%rbp), %edx
	movq	-144(%rbp), %rdi
	movl	%edx, 24(%rax)
.L2189:
	movq	%rax, 808(%rdi)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2213
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2183:
	.cfi_restore_state
	call	_ZN2v88internal11interpreter17BytecodeGenerator20GenerateBytecodeBodyEv
	jmp	.L2187
.L2213:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20746:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator16GenerateBytecodeEm, .-_ZN2v88internal11interpreter17BytecodeGenerator16GenerateBytecodeEm
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator35VisitBlockDeclarationsAndStatementsEPNS0_5BlockE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator35VisitBlockDeclarationsAndStatementsEPNS0_5BlockE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator35VisitBlockDeclarationsAndStatementsEPNS0_5BlockE, @function
_ZN2v88internal11interpreter17BytecodeGenerator35VisitBlockDeclarationsAndStatementsEPNS0_5BlockE:
.LFB20751:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-112(%rbp), %r14
	leaq	16+_ZTVN2v88internal11interpreter12BlockBuilderE(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	addq	$-128, %rsp
	movq	568(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	24(%rdi), %rax
	movq	%rsi, -56(%rbp)
	movq	%rax, -104(%rbp)
	movq	24(%rdi), %rax
	movq	%rsi, -128(%rbp)
	movq	%rax, -96(%rbp)
	leaq	-88(%rbp), %rax
	movq	%rax, -80(%rbp)
	movq	%rax, -88(%rbp)
	movq	800(%rdi), %rax
	movq	$0, -72(%rbp)
	movq	%rax, -144(%rbp)
	movq	808(%rdi), %rax
	movb	$0, -64(%rbp)
	movq	%rax, -136(%rbp)
	leaq	-160(%rbp), %rax
	movq	%rax, 800(%rdi)
	movq	24(%rsi), %rsi
	leaq	16+_ZTVN2v88internal11interpreter17BytecodeGenerator24ControlScopeForBreakableE(%rip), %rax
	movq	%rdx, -48(%rbp)
	movq	%r13, -112(%rbp)
	movq	%rdi, -152(%rbp)
	movq	%rax, -160(%rbp)
	movq	%r14, -120(%rbp)
	testq	%rsi, %rsi
	je	.L2215
	addq	$88, %rsi
	call	_ZN2v88internal11interpreter17BytecodeGenerator17VisitDeclarationsEPNS_4base16ThreadedListBaseINS0_11DeclarationENS3_9EmptyBaseENS3_18ThreadedListTraitsIS5_EEEE
.L2215:
	leaq	8(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator15VisitStatementsEPKNS0_8ZoneListIPNS0_9StatementEEE
	movq	-152(%rbp), %rax
	movq	%r14, %rdi
	movq	%r13, -112(%rbp)
	movq	-144(%rbp), %rdx
	movq	%rdx, 800(%rax)
	call	_ZN2v88internal11interpreter27BreakableControlFlowBuilderD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2221
	subq	$-128, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2221:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20751:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator35VisitBlockDeclarationsAndStatementsEPNS0_5BlockE, .-_ZN2v88internal11interpreter17BytecodeGenerator35VisitBlockDeclarationsAndStatementsEPNS0_5BlockE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator10VisitBlockEPNS0_5BlockE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator10VisitBlockEPNS0_5BlockE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator10VisitBlockEPNS0_5BlockE, @function
_ZN2v88internal11interpreter17BytecodeGenerator10VisitBlockEPNS0_5BlockE:
.LFB20750:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	24(%rdi), %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$184, %rsp
	movq	536(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rsi), %rax
	testq	%rax, %rax
	je	.L2223
	movq	%rax, 536(%rdi)
	movq	24(%rsi), %rsi
	testq	%rsi, %rsi
	je	.L2223
	movl	124(%rsi), %eax
	testl	%eax, %eax
	jg	.L2258
.L2223:
	movq	24(%rbx), %rax
	movq	568(%rbx), %rdx
	movq	%r15, -120(%rbp)
	leaq	-128(%rbp), %r13
	leaq	16+_ZTVN2v88internal11interpreter12BlockBuilderE(%rip), %r15
	movq	$0, -88(%rbp)
	movq	%rax, -112(%rbp)
	leaq	-104(%rbp), %rax
	movq	%rax, -96(%rbp)
	movq	%rax, -104(%rbp)
	movq	800(%rbx), %rax
	movb	$0, -80(%rbp)
	movq	%rax, -160(%rbp)
	movq	808(%rbx), %rax
	movq	%r12, -72(%rbp)
	movq	%rax, -152(%rbp)
	leaq	-176(%rbp), %rax
	movq	%rax, 800(%rbx)
	movq	24(%r12), %rsi
	leaq	16+_ZTVN2v88internal11interpreter17BytecodeGenerator24ControlScopeForBreakableE(%rip), %rax
	movq	%rdx, -64(%rbp)
	movq	%r15, -128(%rbp)
	movq	%rbx, -168(%rbp)
	movq	%rax, -176(%rbp)
	movq	%r12, -144(%rbp)
	movq	%r13, -136(%rbp)
	testq	%rsi, %rsi
	je	.L2230
	addq	$88, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator17VisitDeclarationsEPNS_4base16ThreadedListBaseINS0_11DeclarationENS3_9EmptyBaseENS3_18ThreadedListTraitsIS5_EEEE
.L2230:
	movq	%rbx, %rdi
	leaq	8(%r12), %rsi
	call	_ZN2v88internal11interpreter17BytecodeGenerator15VisitStatementsEPKNS0_8ZoneListIPNS0_9StatementEEE
	movq	-168(%rbp), %rax
	movq	-160(%rbp), %rdx
	movq	%r13, %rdi
	movq	%rdx, 800(%rax)
	movq	%r15, -128(%rbp)
	call	_ZN2v88internal11interpreter27BreakableControlFlowBuilderD2Ev@PLT
.L2229:
	cmpq	536(%rbx), %r14
	je	.L2222
	movq	%r14, 536(%rbx)
.L2222:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2259
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2258:
	.cfi_restore_state
	movq	816(%rdi), %rax
	leaq	-128(%rbp), %r13
	movq	%rdi, -120(%rbp)
	movq	%r13, 816(%rdi)
	movq	%rax, -128(%rbp)
	movl	328(%rdi), %eax
	movq	%r15, %rdi
	movq	$2, -104(%rbp)
	movl	%eax, -112(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder18CreateBlockContextEPKNS0_5ScopeE@PLT
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %rax
	movq	336(%rdx), %rdi
	movq	%rax, 816(%rdx)
	movl	-112(%rbp), %eax
	movl	328(%rdx), %esi
	movl	%eax, 328(%rdx)
	testq	%rdi, %rdi
	je	.L2224
	subl	%eax, %esi
	movq	(%rdi), %rcx
	movq	%rsi, %rdx
	movl	%eax, %esi
	salq	$32, %rdx
	orq	%rdx, %rsi
	call	*32(%rcx)
.L2224:
	movq	24(%r12), %rax
	movq	%rbx, -208(%rbp)
	movq	%rax, -200(%rbp)
	movq	808(%rbx), %rax
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal11interpreter8Register15current_contextEv@PLT
	movl	$0, -180(%rbp)
	movl	%eax, -184(%rbp)
	movq	-192(%rbp), %rax
	testq	%rax, %rax
	je	.L2225
	movq	-208(%rbp), %rdi
	movl	28(%rax), %ecx
	movl	328(%rdi), %esi
	leal	1(%rcx), %edx
	movq	336(%rdi), %r8
	movl	%edx, -180(%rbp)
	leal	1(%rsi), %edx
	cmpl	%edx, 332(%rdi)
	movl	%edx, 328(%rdi)
	cmovge	332(%rdi), %edx
	movl	%edx, 332(%rdi)
	testq	%r8, %r8
	je	.L2226
	movq	(%r8), %rax
	movl	%esi, -212(%rbp)
	movq	%r8, %rdi
	call	*16(%rax)
	movq	-192(%rbp), %rax
	movq	-208(%rbp), %rdi
	movl	-212(%rbp), %esi
.L2226:
	movl	%esi, 24(%rax)
	addq	$24, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11PushContextENS1_8RegisterE@PLT
.L2225:
	movq	-208(%rbp), %rax
	leaq	-208(%rbp), %rdx
	movq	%rdx, 808(%rax)
	movq	24(%rbx), %rax
	movq	568(%rbx), %rdx
	movq	%r15, -120(%rbp)
	leaq	16+_ZTVN2v88internal11interpreter12BlockBuilderE(%rip), %r15
	movq	%rax, -112(%rbp)
	leaq	-104(%rbp), %rax
	movq	%rax, -96(%rbp)
	movq	%rax, -104(%rbp)
	movq	800(%rbx), %rax
	movq	$0, -88(%rbp)
	movq	%rax, -160(%rbp)
	movq	808(%rbx), %rax
	movb	$0, -80(%rbp)
	movq	%rax, -152(%rbp)
	leaq	-176(%rbp), %rax
	movq	%rax, 800(%rbx)
	movq	24(%r12), %rsi
	leaq	16+_ZTVN2v88internal11interpreter17BytecodeGenerator24ControlScopeForBreakableE(%rip), %rax
	movq	%r12, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r15, -128(%rbp)
	movq	%rbx, -168(%rbp)
	movq	%rax, -176(%rbp)
	movq	%r12, -144(%rbp)
	movq	%r13, -136(%rbp)
	testq	%rsi, %rsi
	je	.L2227
	addq	$88, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator17VisitDeclarationsEPNS_4base16ThreadedListBaseINS0_11DeclarationENS3_9EmptyBaseENS3_18ThreadedListTraitsIS5_EEEE
.L2227:
	leaq	8(%r12), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator15VisitStatementsEPKNS0_8ZoneListIPNS0_9StatementEEE
	movq	-168(%rbp), %rax
	movq	-160(%rbp), %rdx
	movq	%r13, %rdi
	movq	%rdx, 800(%rax)
	movq	%r15, -128(%rbp)
	call	_ZN2v88internal11interpreter27BreakableControlFlowBuilderD2Ev@PLT
	movq	-192(%rbp), %rax
	testq	%rax, %rax
	je	.L2228
	movq	-208(%rbp), %rcx
	movl	24(%rax), %esi
	leaq	24(%rcx), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder10PopContextENS1_8RegisterE@PLT
	movq	-192(%rbp), %rax
	movl	-184(%rbp), %edx
	movl	%edx, 24(%rax)
.L2228:
	movq	-208(%rbp), %rdx
	movq	%rax, 808(%rdx)
	jmp	.L2229
.L2259:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20750:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator10VisitBlockEPNS0_5BlockE, .-_ZN2v88internal11interpreter17BytecodeGenerator10VisitBlockEPNS0_5BlockE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator17VisitDoExpressionEPNS0_12DoExpressionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator17VisitDoExpressionEPNS0_12DoExpressionE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator17VisitDoExpressionEPNS0_12DoExpressionE, @function
_ZN2v88internal11interpreter17BytecodeGenerator17VisitDoExpressionEPNS0_12DoExpressionE:
.LFB20845:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	movq	8(%rsi), %rsi
	call	_ZN2v88internal11interpreter17BytecodeGenerator10VisitBlockEPNS0_5BlockE
	movq	16(%rbx), %rax
	movl	(%rax), %edx
	cmpl	$-1, %edx
	je	.L2261
	cmpb	$2, 496(%r12)
	je	.L2261
	movb	$1, 496(%r12)
	movl	%edx, 500(%r12)
.L2261:
	movl	4(%rax), %edx
	movq	8(%rax), %rsi
	movq	%r12, %rdi
	movl	$1, %ecx
	popq	%rbx
	popq	%r12
	shrl	$11, %edx
	popq	%rbp
	.cfi_def_cfa 7, 8
	andl	$1, %edx
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator17BuildVariableLoadEPNS0_8VariableENS0_13HoleCheckModeENS0_10TypeofModeE
	.cfi_endproc
.LFE20845:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator17VisitDoExpressionEPNS0_12DoExpressionE, .-_ZN2v88internal11interpreter17BytecodeGenerator17VisitDoExpressionEPNS0_12DoExpressionE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator24VisitTryFinallyStatementEPNS0_19TryFinallyStatementE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator24VisitTryFinallyStatementEPNS0_19TryFinallyStatementE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator24VisitTryFinallyStatementEPNS0_19TryFinallyStatementE, @function
_ZN2v88internal11interpreter17BytecodeGenerator24VisitTryFinallyStatementEPNS0_19TryFinallyStatementE:
.LFB20782:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$264, %rsp
	movq	%rsi, -280(%rbp)
	movl	872(%rdi), %r13d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L2267
	movq	568(%rdi), %r12
.L2267:
	leaq	16+_ZTVN2v88internal11interpreter17TryFinallyBuilderE(%rip), %rax
	leaq	288(%rbx), %rdi
	leaq	24(%rbx), %r14
	movq	%rax, -160(%rbp)
	movq	%r14, -152(%rbp)
	call	_ZN2v88internal11interpreter19HandlerTableBuilder15NewHandlerEntryEv@PLT
	movq	%r12, %xmm0
	movb	$0, -88(%rbp)
	movq	336(%rbx), %rdi
	movl	%eax, -144(%rbp)
	movq	24(%rbx), %rax
	movhps	-280(%rbp), %xmm0
	movl	%r13d, -140(%rbp)
	movl	328(%rbx), %r13d
	movq	%rax, -120(%rbp)
	leaq	-112(%rbp), %rax
	leal	1(%r13), %r12d
	movq	%rax, -104(%rbp)
	cmpl	%r12d, 332(%rbx)
	movq	%rax, -112(%rbp)
	movl	%r12d, %eax
	cmovge	332(%rbx), %eax
	movb	$0, -136(%rbp)
	movq	$-1, -128(%rbp)
	movq	$0, -96(%rbp)
	movl	%r12d, 328(%rbx)
	movl	%eax, 332(%rbx)
	movaps	%xmm0, -80(%rbp)
	testq	%rdi, %rdi
	je	.L2268
	movq	(%rdi), %rax
	movl	%r13d, %esi
	call	*16(%rax)
	movl	328(%rbx), %r12d
	movl	332(%rbx), %edx
	movq	336(%rbx), %rdi
	leal	1(%r12), %eax
	cmpl	%edx, %eax
	movl	%eax, 328(%rbx)
	cmovl	%edx, %eax
	movl	%eax, 332(%rbx)
	testq	%rdi, %rdi
	je	.L2269
	movq	(%rdi), %rax
	movl	%r12d, %esi
	call	*16(%rax)
.L2269:
	movq	16(%rbx), %rax
	leaq	-272(%rbp), %r15
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	leaq	-216(%rbp), %rdi
	movq	%r15, %rdx
	movl	%r13d, -184(%rbp)
	movq	%rax, -216(%rbp)
	movq	%rbx, -224(%rbp)
	movq	$0, -192(%rbp)
	movl	%r12d, -180(%rbp)
	movq	$-1, -176(%rbp)
	movl	$4, -272(%rbp)
	movq	$0, -264(%rbp)
	movl	$0, -256(%rbp)
	movaps	%xmm0, -208(%rbp)
	call	_ZNSt6vectorIN2v88internal11interpreter17BytecodeGenerator12ControlScope16DeferredCommands5EntryENS1_13ZoneAllocatorIS6_EEE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S9_EEDpOT_
	movl	328(%rbx), %r13d
	movq	336(%rbx), %rdi
	leal	1(%r13), %eax
	cmpl	%eax, 332(%rbx)
	movl	%eax, 328(%rbx)
	cmovge	332(%rbx), %eax
	movl	%eax, 332(%rbx)
	testq	%rdi, %rdi
	je	.L2270
	movq	(%rdi), %rax
	movl	%r13d, %esi
	call	*16(%rax)
.L2270:
	call	_ZN2v88internal11interpreter8Register15current_contextEv@PLT
	leaq	-160(%rbp), %r12
	movl	%r13d, %edx
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder12MoveRegisterENS1_8RegisterES3_@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter17TryFinallyBuilder8BeginTryENS1_8RegisterE@PLT
	movq	808(%rbx), %rdx
	movq	800(%rbx), %rax
	leaq	16+_ZTVN2v88internal11interpreter17BytecodeGenerator25ControlScopeForTryFinallyE(%rip), %rcx
	cmpb	$0, 8(%rbx)
	movq	%rbx, -264(%rbp)
	movq	%rdx, -248(%rbp)
	leaq	-224(%rbp), %rdx
	movq	%rdx, -232(%rbp)
	movq	%rbx, %rdx
	movq	%rax, -256(%rbp)
	movq	%r15, 800(%rbx)
	movq	%rcx, -272(%rbp)
	movq	%r12, -240(%rbp)
	je	.L2305
.L2271:
	movq	%rax, 800(%rdx)
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter17TryFinallyBuilder6EndTryEv@PLT
	movq	-224(%rbp), %rax
	movabsq	$-4294967296, %rsi
	leaq	24(%rax), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralENS0_3SmiE@PLT
	movq	-224(%rbp), %rax
	movl	-184(%rbp), %esi
	leaq	24(%rax), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-224(%rbp), %rax
	movl	-180(%rbp), %esi
	leaq	24(%rax), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter17TryFinallyBuilder8LeaveTryEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter17TryFinallyBuilder12BeginHandlerEv@PLT
	movq	-224(%rbp), %rax
	movl	-180(%rbp), %esi
	leaq	24(%rax), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-224(%rbp), %rax
	xorl	%esi, %esi
	leaq	24(%rax), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralENS0_3SmiE@PLT
	movq	-224(%rbp), %rax
	movl	-184(%rbp), %esi
	leaq	24(%rax), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter17TryFinallyBuilder12BeginFinallyEv@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadTheHoleEv@PLT
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder17SetPendingMessageEv@PLT
	movl	%r13d, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	cmpb	$0, 8(%rbx)
	je	.L2306
.L2273:
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter17TryFinallyBuilder10EndFinallyEv@PLT
	movl	%r13d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder17SetPendingMessageEv@PLT
	movq	-200(%rbp), %rax
	movq	-208(%rbp), %r13
	movq	%rax, %rsi
	subq	%r13, %rsi
	cmpq	%rax, %r13
	je	.L2276
	movq	-224(%rbp), %rax
	movb	$0, -272(%rbp)
	movq	$-1, -264(%rbp)
	leaq	24(%rax), %rdi
	cmpq	$24, %rsi
	je	.L2307
	movabsq	$-6148914691236517205, %rax
	sarq	$3, %rsi
	xorl	%edx, %edx
	imulq	%rax, %rsi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder17AllocateJumpTableEii@PLT
	movl	-184(%rbp), %esi
	movq	%rax, %r13
	movq	-224(%rbp), %rax
	leaq	24(%rax), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder21SwitchOnSmiNoFeedbackEPNS1_17BytecodeJumpTableE@PLT
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4JumpEPNS1_13BytecodeLabelE@PLT
	movq	-200(%rbp), %rax
	movq	-208(%rbp), %rbx
	movq	%rax, -296(%rbp)
	cmpq	%rax, %rbx
	je	.L2281
	.p2align 4,,10
	.p2align 3
.L2286:
	movq	-224(%rbp), %rax
	movl	16(%rbx), %edx
	movq	%r13, %rsi
	leaq	24(%rax), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4BindEPNS1_17BytecodeJumpTableEi@PLT
	movl	(%rbx), %esi
	cmpl	$1, %esi
	jbe	.L2282
	movq	-224(%rbp), %rax
	movl	-180(%rbp), %esi
	leaq	24(%rax), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	movl	(%rbx), %esi
.L2282:
	movq	-224(%rbp), %rax
	movq	8(%rbx), %r14
	movq	800(%rax), %rdi
	jmp	.L2285
	.p2align 4,,10
	.p2align 3
.L2308:
	movq	-280(%rbp), %rdi
	movl	-288(%rbp), %esi
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2284
.L2285:
	movq	(%rdi), %rax
	movl	%esi, -288(%rbp)
	movl	$-1, %ecx
	movq	%r14, %rdx
	movq	%rdi, -280(%rbp)
	call	*16(%rax)
	testb	%al, %al
	je	.L2308
	addq	$24, %rbx
	cmpq	%rbx, -296(%rbp)
	jne	.L2286
.L2281:
	movq	-224(%rbp), %rax
	movq	%r15, %rsi
	leaq	24(%rax), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4BindEPNS1_13BytecodeLabelE@PLT
.L2276:
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter17TryFinallyBuilderD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2309
	addq	$264, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2305:
	.cfi_restore_state
	movq	-280(%rbp), %rax
	movq	8(%rax), %rsi
	movq	%rsi, -288(%rbp)
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%rbx), %rax
	movq	-288(%rbp), %rsi
	jb	.L2310
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movq	-264(%rbp), %rdx
	movq	-256(%rbp), %rax
	jmp	.L2271
	.p2align 4,,10
	.p2align 3
.L2306:
	movq	-280(%rbp), %rax
	movq	16(%rax), %rsi
	movq	%rsi, -280(%rbp)
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%rbx), %rax
	movq	-280(%rbp), %rsi
	jb	.L2311
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	jmp	.L2273
	.p2align 4,,10
	.p2align 3
.L2268:
	leal	2(%r13), %edx
	cmpl	%edx, %eax
	movl	%edx, 328(%rbx)
	cmovl	%edx, %eax
	movl	%eax, 332(%rbx)
	jmp	.L2269
	.p2align 4,,10
	.p2align 3
.L2307:
	movslq	16(%r13), %rsi
	salq	$32, %rsi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralENS0_3SmiE@PLT
	movl	-184(%rbp), %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder16CompareReferenceENS1_8RegisterE@PLT
	movq	%r15, %rdx
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11JumpIfFalseENS2_13ToBooleanModeEPNS1_13BytecodeLabelE@PLT
	movl	0(%r13), %r14d
	cmpl	$1, %r14d
	ja	.L2312
.L2278:
	movq	-224(%rbp), %rax
	movq	8(%r13), %r13
	movq	800(%rax), %rbx
	jmp	.L2280
	.p2align 4,,10
	.p2align 3
.L2279:
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L2284
.L2280:
	movq	(%rbx), %rax
	movl	$-1, %ecx
	movq	%r13, %rdx
	movl	%r14d, %esi
	movq	%rbx, %rdi
	call	*16(%rax)
	testb	%al, %al
	je	.L2279
	jmp	.L2281
	.p2align 4,,10
	.p2align 3
.L2312:
	movq	-224(%rbp), %rax
	movl	-180(%rbp), %esi
	leaq	24(%rax), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	movl	0(%r13), %r14d
	jmp	.L2278
	.p2align 4,,10
	.p2align 3
.L2311:
	movb	$1, 8(%rbx)
	jmp	.L2273
	.p2align 4,,10
	.p2align 3
.L2310:
	movb	$1, 8(%rbx)
	movq	-264(%rbp), %rdx
	movq	-256(%rbp), %rax
	jmp	.L2271
	.p2align 4,,10
	.p2align 3
.L2284:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2309:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20782:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator24VisitTryFinallyStatementEPNS0_19TryFinallyStatementE, .-_ZN2v88internal11interpreter17BytecodeGenerator24VisitTryFinallyStatementEPNS0_19TryFinallyStatementE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator14VisitForEffectEPNS0_10ExpressionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator14VisitForEffectEPNS0_10ExpressionE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator14VisitForEffectEPNS0_10ExpressionE, @function
_ZN2v88internal11interpreter17BytecodeGenerator14VisitForEffectEPNS0_10ExpressionE:
.LFB21020:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	xorl	%esi, %esi
	pushq	%r12
	leaq	-64(%rbp), %rcx
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$48, %rsp
	movq	816(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	328(%rdi), %eax
	cmpb	$0, 8(%rdi)
	movq	%rdi, -56(%rbp)
	movq	%rdx, -64(%rbp)
	movl	%eax, -48(%rbp)
	movq	$1, -40(%rbp)
	movq	%rcx, 816(%rdi)
	je	.L2324
.L2314:
	movq	336(%r12), %rdi
	movq	%rdx, 816(%r12)
	movl	%eax, 328(%r12)
	testq	%rdi, %rdi
	je	.L2313
	movq	%rsi, %rdx
	movq	(%rdi), %rcx
	movl	%eax, %esi
	salq	$32, %rdx
	orq	%rdx, %rsi
	call	*32(%rcx)
.L2313:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2325
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2324:
	.cfi_restore_state
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L2326
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator25VisitNoStackOverflowCheckEPNS0_7AstNodeE
.L2323:
	movq	-56(%rbp), %r12
	movl	-48(%rbp), %eax
	movq	-64(%rbp), %rdx
	movl	328(%r12), %esi
	subl	%eax, %esi
	jmp	.L2314
	.p2align 4,,10
	.p2align 3
.L2326:
	movb	$1, 8(%r12)
	jmp	.L2323
.L2325:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21020:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator14VisitForEffectEPNS0_10ExpressionE, .-_ZN2v88internal11interpreter17BytecodeGenerator14VisitForEffectEPNS0_10ExpressionE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE, @function
_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE:
.LFB21018:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	leaq	-64(%rbp), %rcx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$48, %rsp
	movq	816(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	328(%rdi), %eax
	cmpb	$0, 8(%rdi)
	movq	%rdi, -56(%rbp)
	movq	%rdx, -64(%rbp)
	movl	%eax, -48(%rbp)
	movq	$2, -40(%rbp)
	movq	%rcx, 816(%rdi)
	je	.L2338
	xorl	%esi, %esi
	xorl	%r13d, %r13d
.L2328:
	movq	336(%r12), %rdi
	movq	%rdx, 816(%r12)
	movl	%eax, 328(%r12)
	testq	%rdi, %rdi
	je	.L2327
	movq	%rsi, %rdx
	movq	(%rdi), %rcx
	movl	%eax, %esi
	salq	$32, %rdx
	orq	%rdx, %rsi
	call	*32(%rcx)
.L2327:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2339
	addq	$48, %rsp
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2338:
	.cfi_restore_state
	movq	%rsi, %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L2340
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator25VisitNoStackOverflowCheckEPNS0_7AstNodeE
.L2337:
	movq	-56(%rbp), %r12
	movl	-48(%rbp), %eax
	movl	-36(%rbp), %r13d
	movq	-64(%rbp), %rdx
	movl	328(%r12), %esi
	subl	%eax, %esi
	jmp	.L2328
	.p2align 4,,10
	.p2align 3
.L2340:
	movb	$1, 8(%r12)
	jmp	.L2337
.L2339:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21018:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE, .-_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator24VisitFunctionDeclarationEPNS0_19FunctionDeclarationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator24VisitFunctionDeclarationEPNS0_19FunctionDeclarationE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator24VisitFunctionDeclarationEPNS0_19FunctionDeclarationE, @function
_ZN2v88internal11interpreter17BytecodeGenerator24VisitFunctionDeclarationEPNS0_19FunctionDeclarationE:
.LFB20753:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzwl	40(%r13), %eax
	testb	$8, %ah
	je	.L2341
	shrl	$7, %eax
	andl	$7, %eax
	cmpb	$5, %al
	ja	.L2341
	leaq	.L2344(%rip), %rdx
	movzbl	%al, %eax
	movq	%rdi, %r12
	movq	%rsi, %rbx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal11interpreter17BytecodeGenerator24VisitFunctionDeclarationEPNS0_19FunctionDeclarationE,"a",@progbits
	.align 4
	.align 4
.L2344:
	.long	.L2348-.L2344
	.long	.L2347-.L2344
	.long	.L2347-.L2344
	.long	.L2346-.L2344
	.long	.L2345-.L2344
	.long	.L2343-.L2344
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator24VisitFunctionDeclarationEPNS0_19FunctionDeclarationE
	.p2align 4,,10
	.p2align 3
.L2345:
	movl	328(%rdi), %r15d
	leal	2(%r15), %eax
	cmpl	%eax, 332(%rdi)
	movq	%r15, %r14
	movl	%eax, 328(%rdi)
	cmovge	332(%rdi), %eax
	movl	%eax, 332(%rdi)
	movq	336(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2361
	movq	(%rdi), %rax
	movq	%r15, %rsi
	btsq	$33, %rsi
	call	*24(%rax)
.L2361:
	movq	8(%r13), %rsi
	movq	%r15, %rdx
	leaq	24(%r12), %r15
	btsq	$33, %rdx
	movq	%r15, %rdi
	movq	%rdx, -88(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralEPKNS0_12AstRawStringE@PLT
	movl	%r14d, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator20VisitFunctionLiteralEPNS0_15FunctionLiteralE
	leal	1(%r14), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-88(%rbp), %rdx
	movl	$299, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_12RegisterListE@PLT
	.p2align 4,,10
	.p2align 3
.L2341:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2374
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2347:
	.cfi_restore_state
	movq	24(%rsi), %rsi
	call	_ZN2v88internal11interpreter17BytecodeGenerator20VisitFunctionLiteralEPNS0_15FunctionLiteralE
.L2373:
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$16, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator23BuildVariableAssignmentEPNS0_8VariableENS0_5Token5ValueENS0_13HoleCheckModeENS0_18LookupHoistingModeE
	jmp	.L2341
	.p2align 4,,10
	.p2align 3
.L2346:
	movq	24(%rsi), %rsi
	call	_ZN2v88internal11interpreter17BytecodeGenerator20VisitFunctionLiteralEPNS0_15FunctionLiteralE
	movl	32(%r13), %edx
	leaq	24(%r12), %rdi
	xorl	%ecx, %ecx
	movq	808(%r12), %rax
	movl	24(%rax), %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder16StoreContextSlotENS1_8RegisterEii@PLT
	jmp	.L2341
	.p2align 4,,10
	.p2align 3
.L2343:
	movq	24(%rsi), %rsi
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	jmp	.L2373
	.p2align 4,,10
	.p2align 3
.L2348:
	movq	552(%rdi), %r14
	leaq	-80(%rbp), %r15
	movq	%r13, -80(%rbp)
	movabsq	$21474836480, %rax
	movq	%r15, %rsi
	movq	%rax, -72(%rbp)
	movq	%r14, %rdi
	addq	$16, %r14
	call	_ZNKSt8_Rb_treeISt5tupleIJN2v88internal11interpreter17BytecodeGenerator17FeedbackSlotCache8SlotKindEiPKvEESt4pairIKS9_iESt10_Select1stISC_ESt4lessIS9_ENS2_13ZoneAllocatorISC_EEE4findERSB_
	cmpq	%r14, %rax
	je	.L2350
	movl	48(%rax), %r14d
	cmpl	$-1, %r14d
	je	.L2350
.L2351:
	movq	552(%r12), %rdx
	movq	24(%rbx), %r8
	movq	%r15, %rsi
	movabsq	$30064771072, %rax
	movq	%rax, -72(%rbp)
	movq	%rdx, %rdi
	movq	%r8, -80(%rbp)
	movq	%r8, -96(%rbp)
	movq	%rdx, -88(%rbp)
	call	_ZNKSt8_Rb_treeISt5tupleIJN2v88internal11interpreter17BytecodeGenerator17FeedbackSlotCache8SlotKindEiPKvEESt4pairIKS9_iESt10_Select1stISC_ESt4lessIS9_ENS2_13ZoneAllocatorISC_EEE4findERSB_
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %r8
	addq	$16, %rdx
	cmpq	%rdx, %rax
	je	.L2353
	movl	48(%rax), %ecx
	cmpl	$-1, %ecx
	jne	.L2354
.L2353:
	movq	512(%r12), %rax
	movq	%r15, %rsi
	movl	88(%rax), %ecx
	leal	1(%rcx), %edx
	movl	%ecx, -88(%rbp)
	movl	%edx, 88(%rax)
	movq	552(%r12), %rdi
	movabsq	$30064771072, %rax
	movl	%ecx, -64(%rbp)
	movq	%r8, -80(%rbp)
	movq	%rax, -72(%rbp)
	call	_ZNSt8_Rb_treeISt5tupleIJN2v88internal11interpreter17BytecodeGenerator17FeedbackSlotCache8SlotKindEiPKvEESt4pairIKS9_iESt10_Select1stISC_ESt4lessIS9_ENS2_13ZoneAllocatorISC_EEE17_M_emplace_uniqueIJRSA_IS9_iEEEESA_ISt17_Rb_tree_iteratorISC_EbEDpOT_
	movl	-88(%rbp), %ecx
.L2354:
	movq	8(%r13), %rdx
	movq	560(%r12), %rdi
	movl	%r14d, -72(%rbp)
	movq	24(%rbx), %rax
	movl	%ecx, -68(%rbp)
	movq	%rdx, -80(%rbp)
	movq	%rax, -64(%rbp)
	movq	16(%rdi), %rsi
	cmpq	24(%rdi), %rsi
	je	.L2355
	movdqa	-80(%rbp), %xmm0
	movups	%xmm0, (%rsi)
	movq	-64(%rbp), %rax
	movq	%rax, 16(%rsi)
	addq	$24, 16(%rdi)
.L2356:
	cmpq	$0, 544(%r12)
	movq	24(%rbx), %rdi
	movq	%rdi, -80(%rbp)
	je	.L2341
	call	_ZNK2v88internal15FunctionLiteral18ShouldEagerCompileEv@PLT
	testb	%al, %al
	je	.L2341
	movq	544(%r12), %rdi
	movq	8(%rdi), %rsi
	cmpq	16(%rdi), %rsi
	je	.L2360
	movq	-80(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 8(%rdi)
	jmp	.L2341
	.p2align 4,,10
	.p2align 3
.L2350:
	movq	512(%r12), %rax
	movl	$6, %esi
	leaq	56(%rax), %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movq	552(%r12), %rdi
	movq	%r15, %rsi
	movq	%r13, -80(%rbp)
	movl	%eax, %r14d
	movabsq	$21474836480, %rax
	movq	%rax, -72(%rbp)
	movl	%r14d, -64(%rbp)
	call	_ZNSt8_Rb_treeISt5tupleIJN2v88internal11interpreter17BytecodeGenerator17FeedbackSlotCache8SlotKindEiPKvEESt4pairIKS9_iESt10_Select1stISC_ESt4lessIS9_ENS2_13ZoneAllocatorISC_EEE17_M_emplace_uniqueIJRSA_IS9_iEEEESA_ISt17_Rb_tree_iteratorISC_EbEDpOT_
	jmp	.L2351
	.p2align 4,,10
	.p2align 3
.L2355:
	movq	%r15, %rdx
	call	_ZNSt6vectorIN2v88internal11interpreter17BytecodeGenerator25GlobalDeclarationsBuilder11DeclarationENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_
	jmp	.L2356
.L2360:
	movq	%r15, %rdx
	call	_ZNSt6vectorIPN2v88internal15FunctionLiteralESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L2341
.L2374:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20753:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator24VisitFunctionDeclarationEPNS0_19FunctionDeclarationE, .-_ZN2v88internal11interpreter17BytecodeGenerator24VisitFunctionDeclarationEPNS0_19FunctionDeclarationE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator20VisitReturnStatementEPNS0_15ReturnStatementE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator20VisitReturnStatementEPNS0_15ReturnStatementE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator20VisitReturnStatementEPNS0_15ReturnStatementE, @function
_ZN2v88internal11interpreter17BytecodeGenerator20VisitReturnStatementEPNS0_15ReturnStatementE:
.LFB20763:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	568(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%r13, %r13
	je	.L2376
	movq	40(%r13), %rax
	leaq	16(%rax), %rcx
	movq	24(%rax), %rax
	movq	%rcx, %rdx
	testq	%rax, %rax
	jne	.L2379
	jmp	.L2376
	.p2align 4,,10
	.p2align 3
.L2406:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L2380
.L2379:
	cmpq	%rbx, 32(%rax)
	jnb	.L2406
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L2379
.L2380:
	cmpq	%rdx, %rcx
	je	.L2376
	cmpq	%rbx, 32(%rdx)
	jbe	.L2407
.L2376:
	movl	(%rbx), %eax
	cmpl	$-1, %eax
	je	.L2384
	movb	$2, 496(%r12)
	movl	%eax, 500(%r12)
.L2384:
	movq	8(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	testb	$64, 4(%rbx)
	movl	16(%rbx), %ebx
	movq	800(%r12), %r12
	je	.L2389
	jmp	.L2387
	.p2align 4,,10
	.p2align 3
.L2408:
	movq	16(%r12), %r12
	testq	%r12, %r12
	je	.L2388
.L2387:
	movq	(%r12), %rax
	xorl	%edx, %edx
	movl	%ebx, %ecx
	movl	$3, %esi
	movq	%r12, %rdi
	call	*16(%rax)
	testb	%al, %al
	je	.L2408
.L2375:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2409
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2410:
	.cfi_restore_state
	movq	16(%r12), %r12
	testq	%r12, %r12
	je	.L2388
.L2389:
	movq	(%r12), %rax
	xorl	%edx, %edx
	movl	%ebx, %ecx
	movl	$2, %esi
	movq	%r12, %rdi
	call	*16(%rax)
	testb	%al, %al
	je	.L2410
	jmp	.L2375
	.p2align 4,,10
	.p2align 3
.L2407:
	movq	40(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L2376
	movq	(%rdi), %rax
	movl	$2, %esi
	call	*16(%rax)
	movq	%rax, -48(%rbp)
	cmpl	$-1, %eax
	je	.L2376
	leaq	-48(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt6vectorIN2v88internal11SourceRangeENS1_13ZoneAllocatorIS2_EEE12emplace_backIJRS2_EEEvDpOT_
	jmp	.L2376
.L2388:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2409:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20763:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator20VisitReturnStatementEPNS0_15ReturnStatementE, .-_ZN2v88internal11interpreter17BytecodeGenerator20VisitReturnStatementEPNS0_15ReturnStatementE
	.section	.text.unlikely._ZN2v88internal11interpreter17BytecodeGenerator23BuildCreateArrayLiteralEPKNS0_8ZoneListIPNS0_10ExpressionEEEPNS0_12ArrayLiteralE,"ax",@progbits
	.align 2
.LCOLDB12:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator23BuildCreateArrayLiteralEPKNS0_8ZoneListIPNS0_10ExpressionEEEPNS0_12ArrayLiteralE,"ax",@progbits
.LHOTB12:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator23BuildCreateArrayLiteralEPKNS0_8ZoneListIPNS0_10ExpressionEEEPNS0_12ArrayLiteralE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator23BuildCreateArrayLiteralEPKNS0_8ZoneListIPNS0_10ExpressionEEEPNS0_12ArrayLiteralE, @function
_ZN2v88internal11interpreter17BytecodeGenerator23BuildCreateArrayLiteralEPKNS0_8ZoneListIPNS0_10ExpressionEEEPNS0_12ArrayLiteralE:
.LFB20873:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$152, %rsp
	movl	328(%rdi), %r12d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leal	1(%r12), %eax
	cmpl	%eax, 332(%rdi)
	movl	%eax, 328(%rdi)
	movl	%eax, -100(%rbp)
	cmovge	332(%rdi), %eax
	movl	%eax, 332(%rdi)
	movq	336(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2412
	movq	(%rdi), %rax
	movl	%r12d, %esi
	call	*16(%rax)
	movl	328(%rbx), %esi
	movl	332(%rbx), %edx
	movq	336(%rbx), %rdi
	leal	1(%rsi), %eax
	movl	%esi, -100(%rbp)
	cmpl	%edx, %eax
	movl	%eax, 328(%rbx)
	cmovl	%edx, %eax
	movl	%eax, 332(%rbx)
	testq	%rdi, %rdi
	je	.L2413
	movq	(%rdi), %rax
	call	*16(%rax)
.L2413:
	movq	512(%rbx), %rax
	movq	(%r14), %rdx
	addq	$56, %rax
	movq	%rdx, -112(%rbp)
	movq	%rax, -160(%rbp)
	movslq	12(%r14), %rax
	leaq	24(%rbx), %r14
	movq	%rax, %r15
	leaq	(%rdx,%rax,8), %rax
	movq	%rax, -120(%rbp)
	testl	%r15d, %r15d
	je	.L2414
	movq	(%rdx), %rsi
	movzbl	4(%rsi), %eax
	andl	$63, %eax
	cmpb	$46, %al
	je	.L2496
.L2414:
	testq	%r13, %r13
	je	.L2497
	movl	8(%r13), %eax
	xorl	%esi, %esi
	movq	%r13, %rdi
	addl	%eax, %eax
	cmpl	$2, %eax
	sete	%sil
	movl	%esi, %eax
	orl	$4, %eax
	testb	$-128, 4(%r13)
	cmovne	%eax, %esi
	movl	%esi, -88(%rbp)
	call	_ZNK2v88internal12ArrayLiteral22IsFastCloningSupportedEv@PLT
	movl	-88(%rbp), %esi
	movzbl	%al, %edi
	call	_ZN2v88internal11interpreter23CreateArrayLiteralFlags6EncodeEbi@PLT
	cmpb	$0, _ZN2v88internal33FLAG_enable_one_shot_optimizationE(%rip)
	movb	%al, -88(%rbp)
	je	.L2422
	movl	868(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L2422
	movq	512(%rbx), %rdi
	movq	16(%rdi), %rax
	movl	28(%rax), %edx
	testl	%edx, %edx
	jne	.L2498
.L2423:
	movq	%r14, %rdi
	testl	%r15d, %r15d
	jne	.L2425
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder49EmptyArrayBoilerplateDescriptionConstantPoolEntryEv@PLT
	movq	%rax, %r8
.L2426:
	movl	328(%rbx), %r15d
	movq	336(%rbx), %rdi
	leal	2(%r15), %edx
	cmpl	%edx, 332(%rbx)
	movl	%edx, 328(%rbx)
	cmovge	332(%rbx), %edx
	movl	%edx, 332(%rbx)
	movl	%r15d, %edx
	testq	%rdi, %rdi
	je	.L2430
	movq	(%rdi), %rax
	movq	%rdx, %rsi
	movq	%r8, -128(%rbp)
	movq	%rdx, -96(%rbp)
	btsq	$33, %rsi
	call	*24(%rax)
	movq	-128(%rbp), %r8
	movq	-96(%rbp), %rdx
.L2430:
	btsq	$33, %rdx
	movq	%r8, %rsi
	movq	%r14, %rdi
	movq	%rdx, -96(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder21LoadConstantPoolEntryEm@PLT
	movl	%r15d, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movzbl	-88(%rbp), %esi
	movq	%rax, %rdi
	salq	$32, %rsi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralENS0_3SmiE@PLT
	leal	1(%r15), %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-96(%rbp), %rdx
	movl	$187, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_12RegisterListE@PLT
	jmp	.L2431
	.p2align 4,,10
	.p2align 3
.L2422:
	testl	%r15d, %r15d
	jne	.L2427
	movq	512(%rbx), %rdi
.L2429:
	addq	$56, %rdi
	movl	$19, %esi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder23CreateEmptyArrayLiteralEi@PLT
.L2431:
	movl	-100(%rbp), %esi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-120(%rbp), %rdx
	movslq	12(%r13), %rax
	movq	%rdx, -96(%rbp)
	testl	%eax, %eax
	js	.L2432
	movq	-112(%rbp), %rcx
	leaq	(%rcx,%rax,8), %rax
	movq	%rax, -96(%rbp)
.L2432:
	movq	-112(%rbp), %r13
	cmpq	%r13, -96(%rbp)
	je	.L2462
	movq	%r13, %rax
	movl	$-1, -88(%rbp)
	negq	%rax
	salq	$29, %rax
	movq	%rax, -128(%rbp)
	.p2align 4,,10
	.p2align 3
.L2436:
	movq	0(%r13), %r15
	movq	%r15, %rdi
	call	_ZN2v88internal10Expression18IsCompileTimeValueEv@PLT
	testb	%al, %al
	jne	.L2434
	movq	%r13, %rsi
	movq	%r14, %rdi
	salq	$29, %rsi
	addq	-128(%rbp), %rsi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralENS0_3SmiE@PLT
	movl	%r12d, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	cmpl	$-1, -88(%rbp)
	je	.L2499
.L2435:
	movl	-88(%rbp), %ecx
	movl	-100(%rbp), %esi
	movl	%r12d, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder19StoreInArrayLiteralENS1_8RegisterES3_i@PLT
.L2434:
	addq	$8, %r13
	cmpq	%r13, -96(%rbp)
	jne	.L2436
	movq	-112(%rbp), %rcx
	movq	-96(%rbp), %r15
	subq	%rcx, %r15
	addq	%r15, %rcx
	leaq	-8(%r15), %rax
	movq	%rcx, -112(%rbp)
	shrq	$3, %rax
	addl	$1, %eax
.L2433:
	cmpq	%rcx, -120(%rbp)
	je	.L2458
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	cvtsi2sdl	%eax, %xmm0
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralEd@PLT
	movl	%r12d, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
.L2419:
	movq	512(%rbx), %rax
	addq	$56, %rax
	movq	%rax, -176(%rbp)
.L2459:
	movq	-112(%rbp), %r13
	movl	$-1, -96(%rbp)
	movq	%r14, -112(%rbp)
	movq	%rbx, %r14
	movl	$-1, -144(%rbp)
	addq	$8, %r13
	jmp	.L2456
	.p2align 4,,10
	.p2align 3
.L2503:
	movl	328(%r14), %eax
	movq	16(%r15), %rsi
	movl	%eax, -128(%rbp)
	movl	(%rsi), %eax
	cmpl	$-1, %eax
	je	.L2442
	movb	$2, 496(%r14)
	movl	%eax, 500(%r14)
	movzbl	4(%r15), %eax
	andl	$63, %eax
	cmpb	$46, %al
	jne	.L2489
	movq	16(%r15), %rsi
.L2442:
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	movl	328(%r14), %ebx
	movq	336(%r14), %rdi
	leal	1(%rbx), %r15d
	cmpl	%r15d, 332(%r14)
	movl	%r15d, %eax
	cmovge	332(%r14), %eax
	movl	%r15d, 328(%r14)
	movl	%eax, 332(%r14)
	testq	%rdi, %rdi
	je	.L2500
	movq	(%rdi), %rax
	movl	%ebx, %esi
	call	*16(%rax)
	movl	328(%r14), %r15d
	movl	332(%r14), %edx
	movq	336(%r14), %rdi
	leal	1(%r15), %eax
	cmpl	%edx, %eax
	movl	%eax, 328(%r14)
	cmovl	%edx, %eax
	movl	%eax, 332(%r14)
	testq	%rdi, %rdi
	je	.L2444
	movq	(%rdi), %rax
	movl	%r15d, %esi
	call	*16(%rax)
.L2444:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator16BuildGetIteratorENS0_12IteratorTypeE
	movq	-112(%rbp), %rdi
	movl	%r15d, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movl	$5, %esi
	movq	%rax, -136(%rbp)
	movq	512(%r14), %rax
	leaq	56(%rax), %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movq	-136(%rbp), %r9
	movl	%r15d, %esi
	movl	%eax, %ecx
	movq	520(%r14), %rax
	movq	%r9, %rdi
	movq	368(%rax), %rdx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder17LoadNamedPropertyENS1_8RegisterEPKNS0_12AstRawStringEi@PLT
	movl	%ebx, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movl	%r15d, -76(%rbp)
	movl	328(%r14), %r15d
	movq	336(%r14), %rdi
	movl	$0, -80(%rbp)
	leal	1(%r15), %eax
	cmpl	%eax, 332(%r14)
	movl	%ebx, -72(%rbp)
	movl	%eax, 328(%r14)
	cmovge	332(%r14), %eax
	movl	%eax, 332(%r14)
	testq	%rdi, %rdi
	je	.L2445
	movq	(%rdi), %rax
	movl	%r15d, %esi
	call	*16(%rax)
.L2445:
	movq	512(%r14), %rax
	movl	$5, %esi
	leaq	56(%rax), %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movl	$5, %esi
	movl	%eax, %ebx
	movq	512(%r14), %rax
	leaq	56(%rax), %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	cmpl	$-1, -96(%rbp)
	movl	%eax, %edx
	je	.L2501
.L2446:
	cmpl	$-1, -88(%rbp)
	je	.L2502
.L2447:
	movl	-88(%rbp), %eax
	movl	-100(%rbp), %ecx
	movq	%r14, %rdi
	movl	%r15d, %r9d
	movq	-80(%rbp), %rsi
	movl	%r12d, %r8d
	pushq	%rax
	movl	-96(%rbp), %eax
	pushq	%rax
	pushq	%rdx
	movl	-72(%rbp), %edx
	pushq	%rbx
	call	_ZN2v88internal11interpreter17BytecodeGenerator26BuildFillArrayWithIteratorENS2_14IteratorRecordENS1_8RegisterES4_S4_NS0_12FeedbackSlotES5_S5_S5_
	movl	-128(%rbp), %ecx
	movq	336(%r14), %rdi
	addq	$32, %rsp
	movl	328(%r14), %esi
	movl	%ecx, 328(%r14)
	testq	%rdi, %rdi
	je	.L2449
	subl	%ecx, %esi
	movq	(%rdi), %rax
	movl	%ecx, %r15d
	salq	$32, %rsi
	orq	%r15, %rsi
	call	*32(%rax)
.L2449:
	cmpq	%r13, -120(%rbp)
	je	.L2494
.L2453:
	addq	$8, %r13
.L2456:
	movq	-8(%r13), %r15
	movzbl	4(%r15), %eax
	andl	$63, %eax
	cmpb	$46, %al
	je	.L2503
	movq	%r15, %rdi
	call	_ZNK2v88internal10Expression16IsTheHoleLiteralEv@PLT
	testb	%al, %al
	jne	.L2450
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	cmpl	$-1, -88(%rbp)
	je	.L2504
.L2451:
	movl	-100(%rbp), %esi
	movq	-112(%rbp), %rdi
	movl	%r12d, %edx
	movl	-88(%rbp), %ecx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder19StoreInArrayLiteralENS1_8RegisterES3_i@PLT
	movl	%r12d, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	cmpq	%r13, -120(%rbp)
	je	.L2494
	cmpl	$-1, -96(%rbp)
	je	.L2505
.L2452:
	movq	-112(%rbp), %rdi
	movl	-96(%rbp), %edx
	movl	$51, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder14UnaryOperationENS0_5Token5ValueEi@PLT
	movl	%r12d, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	jmp	.L2453
	.p2align 4,,10
	.p2align 3
.L2450:
	movq	520(%r14), %rax
	movq	-112(%rbp), %rdi
	movl	%r12d, %esi
	movq	320(%rax), %r15
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	cmpl	$-1, -96(%rbp)
	movq	%rax, %rbx
	je	.L2506
.L2454:
	movl	-96(%rbp), %edx
	movq	%rbx, %rdi
	movl	$51, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder14UnaryOperationENS0_5Token5ValueEi@PLT
	movl	%r12d, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	cmpl	$-1, -144(%rbp)
	movq	%rax, %rbx
	je	.L2507
.L2455:
	movl	-144(%rbp), %ecx
	movl	-100(%rbp), %esi
	movq	%r15, %rdx
	movq	%rbx, %rdi
	movl	$1, %r8d
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder18StoreNamedPropertyENS1_8RegisterEPKNS0_12AstRawStringEiNS0_12LanguageModeE@PLT
	cmpq	%r13, -120(%rbp)
	jne	.L2453
	.p2align 4,,10
	.p2align 3
.L2494:
	movq	%r14, %rbx
	movq	-112(%rbp), %r14
.L2458:
	movl	-100(%rbp), %esi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	movq	336(%rbx), %rdi
	movl	328(%rbx), %esi
	movl	%r12d, 328(%rbx)
	testq	%rdi, %rdi
	je	.L2457
	subl	%r12d, %esi
	movq	(%rdi), %rax
	salq	$32, %rsi
	orq	%r12, %rsi
	call	*32(%rax)
.L2457:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2508
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2500:
	.cfi_restore_state
	leal	2(%rbx), %edx
	cmpl	%eax, %edx
	movl	%edx, 328(%r14)
	cmovge	%edx, %eax
	movl	%eax, 332(%r14)
	jmp	.L2444
	.p2align 4,,10
	.p2align 3
.L2502:
	movq	-160(%rbp), %rdi
	movl	$14, %esi
	movl	%edx, -136(%rbp)
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movl	-152(%rbp), %ecx
	movl	-136(%rbp), %edx
	salq	$32, %rax
	orq	%rax, %rcx
	shrq	$32, %rax
	movq	%rcx, -152(%rbp)
	movq	%rax, -88(%rbp)
	jmp	.L2447
	.p2align 4,,10
	.p2align 3
.L2501:
	movq	-176(%rbp), %rdi
	movl	$15, %esi
	movl	%eax, -136(%rbp)
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movl	-168(%rbp), %ecx
	movl	-136(%rbp), %edx
	salq	$32, %rax
	orq	%rax, %rcx
	shrq	$32, %rax
	movq	%rcx, -168(%rbp)
	movq	%rax, -96(%rbp)
	jmp	.L2446
	.p2align 4,,10
	.p2align 3
.L2499:
	movq	-160(%rbp), %rdi
	movl	$14, %esi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movq	%rax, %r15
	movl	-152(%rbp), %eax
	salq	$32, %r15
	orq	%r15, %rax
	shrq	$32, %r15
	movq	%rax, -152(%rbp)
	movq	%r15, -88(%rbp)
	jmp	.L2435
	.p2align 4,,10
	.p2align 3
.L2497:
	movq	-160(%rbp), %rdi
	movl	$19, %esi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder23CreateEmptyArrayLiteralEi@PLT
	movl	-100(%rbp), %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralENS0_3SmiE@PLT
	movl	%r12d, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	512(%rbx), %rax
	movq	-112(%rbp), %rcx
	addq	$56, %rax
	movq	%rax, -176(%rbp)
	cmpq	%rcx, -120(%rbp)
	je	.L2458
	movl	$-1, -88(%rbp)
	jmp	.L2459
	.p2align 4,,10
	.p2align 3
.L2507:
	movq	-176(%rbp), %rdi
	movl	$11, %esi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movl	-184(%rbp), %edx
	salq	$32, %rax
	orq	%rax, %rdx
	shrq	$32, %rax
	movq	%rdx, -184(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L2455
	.p2align 4,,10
	.p2align 3
.L2506:
	movq	-176(%rbp), %rdi
	movl	$15, %esi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movl	-168(%rbp), %edx
	salq	$32, %rax
	orq	%rax, %rdx
	shrq	$32, %rax
	movq	%rdx, -168(%rbp)
	movq	%rax, -96(%rbp)
	jmp	.L2454
	.p2align 4,,10
	.p2align 3
.L2504:
	movq	-160(%rbp), %rdi
	movl	$14, %esi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movl	-152(%rbp), %edx
	salq	$32, %rax
	orq	%rax, %rdx
	shrq	$32, %rax
	movq	%rdx, -152(%rbp)
	movq	%rax, -88(%rbp)
	jmp	.L2451
	.p2align 4,,10
	.p2align 3
.L2412:
	leal	2(%r12), %edx
	cmpl	%eax, %edx
	movl	%edx, 328(%rbx)
	cmovge	%edx, %eax
	movl	%eax, 332(%rbx)
	jmp	.L2413
	.p2align 4,,10
	.p2align 3
.L2505:
	movq	-176(%rbp), %rdi
	movl	$15, %esi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movl	-168(%rbp), %edx
	salq	$32, %rax
	orq	%rax, %rdx
	shrq	$32, %rax
	movq	%rdx, -168(%rbp)
	movq	%rax, -96(%rbp)
	jmp	.L2452
	.p2align 4,,10
	.p2align 3
.L2498:
	testb	$64, 6(%rax)
	jne	.L2423
	testl	%r15d, %r15d
	je	.L2429
	.p2align 4,,10
	.p2align 3
.L2427:
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder33AllocateDeferredConstantPoolEntryEv@PLT
	leaq	-80(%rbp), %rsi
	leaq	704(%rbx), %rdi
	movq	%r13, -80(%rbp)
	movq	%rax, %r15
	movq	%rax, -72(%rbp)
	call	_ZNSt6vectorISt4pairIPN2v88internal12ArrayLiteralEmENS2_13ZoneAllocatorIS5_EEE12emplace_backIJS5_EEEvDpOT_
	movq	512(%rbx), %rax
	movl	$19, %esi
	leaq	56(%rax), %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movzbl	-88(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movl	%eax, %edx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder18CreateArrayLiteralEmii@PLT
	jmp	.L2431
	.p2align 4,,10
	.p2align 3
.L2496:
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder23CreateArrayFromIterableEv@PLT
	movl	-100(%rbp), %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	addq	$8, -112(%rbp)
	movq	-112(%rbp), %rax
	cmpq	-120(%rbp), %rax
	je	.L2458
	movq	520(%rbx), %rax
	movl	$5, %esi
	movq	320(%rax), %r13
	movq	512(%rbx), %rax
	leaq	56(%rax), %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movl	-100(%rbp), %esi
	movq	%r13, %rdx
	movq	%r14, %rdi
	movl	%eax, %ecx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder17LoadNamedPropertyENS1_8RegisterEPKNS0_12AstRawStringEi@PLT
	movl	%r12d, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movl	$-1, -88(%rbp)
	jmp	.L2419
.L2462:
	movl	$-1, -88(%rbp)
	xorl	%eax, %eax
	movq	%r13, %rcx
	jmp	.L2433
.L2508:
	call	__stack_chk_fail@PLT
.L2425:
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder33AllocateDeferredConstantPoolEntryEv@PLT
	leaq	-80(%rbp), %rsi
	leaq	704(%rbx), %rdi
	movq	%r13, -80(%rbp)
	movq	%rax, -72(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZNSt6vectorISt4pairIPN2v88internal12ArrayLiteralEmENS2_13ZoneAllocatorIS5_EEE12emplace_backIJS5_EEEvDpOT_
	movq	-96(%rbp), %r8
	jmp	.L2426
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal11interpreter17BytecodeGenerator23BuildCreateArrayLiteralEPKNS0_8ZoneListIPNS0_10ExpressionEEEPNS0_12ArrayLiteralE
	.cfi_startproc
	.type	_ZN2v88internal11interpreter17BytecodeGenerator23BuildCreateArrayLiteralEPKNS0_8ZoneListIPNS0_10ExpressionEEEPNS0_12ArrayLiteralE.cold, @function
_ZN2v88internal11interpreter17BytecodeGenerator23BuildCreateArrayLiteralEPKNS0_8ZoneListIPNS0_10ExpressionEEEPNS0_12ArrayLiteralE.cold:
.LFSB20873:
.L2489:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	16, %rax
	ud2
	.cfi_endproc
.LFE20873:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator23BuildCreateArrayLiteralEPKNS0_8ZoneListIPNS0_10ExpressionEEEPNS0_12ArrayLiteralE
	.size	_ZN2v88internal11interpreter17BytecodeGenerator23BuildCreateArrayLiteralEPKNS0_8ZoneListIPNS0_10ExpressionEEEPNS0_12ArrayLiteralE, .-_ZN2v88internal11interpreter17BytecodeGenerator23BuildCreateArrayLiteralEPKNS0_8ZoneListIPNS0_10ExpressionEEEPNS0_12ArrayLiteralE
	.section	.text.unlikely._ZN2v88internal11interpreter17BytecodeGenerator23BuildCreateArrayLiteralEPKNS0_8ZoneListIPNS0_10ExpressionEEEPNS0_12ArrayLiteralE
	.size	_ZN2v88internal11interpreter17BytecodeGenerator23BuildCreateArrayLiteralEPKNS0_8ZoneListIPNS0_10ExpressionEEEPNS0_12ArrayLiteralE.cold, .-_ZN2v88internal11interpreter17BytecodeGenerator23BuildCreateArrayLiteralEPKNS0_8ZoneListIPNS0_10ExpressionEEEPNS0_12ArrayLiteralE.cold
.LCOLDE12:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator23BuildCreateArrayLiteralEPKNS0_8ZoneListIPNS0_10ExpressionEEEPNS0_12ArrayLiteralE
.LHOTE12:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator17VisitArrayLiteralEPNS0_12ArrayLiteralE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator17VisitArrayLiteralEPNS0_12ArrayLiteralE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator17VisitArrayLiteralEPNS0_12ArrayLiteralE, @function
_ZN2v88internal11interpreter17BytecodeGenerator17VisitArrayLiteralEPNS0_12ArrayLiteralE:
.LFB20887:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	call	_ZN2v88internal12ArrayLiteral17InitDepthAndFlagsEv@PLT
	leaq	24(%r12), %rsi
	movq	%r12, %rdx
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator23BuildCreateArrayLiteralEPKNS0_8ZoneListIPNS0_10ExpressionEEEPNS0_12ArrayLiteralE
	.cfi_endproc
.LFE20887:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator17VisitArrayLiteralEPNS0_12ArrayLiteralE, .-_ZN2v88internal11interpreter17BytecodeGenerator17VisitArrayLiteralEPNS0_12ArrayLiteralE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator10VisitYieldEPNS0_5YieldE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator10VisitYieldEPNS0_5YieldE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator10VisitYieldEPNS0_5YieldE, @function
_ZN2v88internal11interpreter17BytecodeGenerator10VisitYieldEPNS0_5YieldE:
.LFB20922:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	24(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%rsi), %eax
	cmpl	$-1, %eax
	je	.L2512
	cmpb	$2, 496(%rdi)
	je	.L2512
	movb	$1, 496(%rdi)
	movl	%eax, 500(%rdi)
.L2512:
	movq	8(%r13), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	movl	864(%rbx), %r15d
	testl	%r15d, %r15d
	jg	.L2513
	movl	328(%rbx), %r14d
.L2514:
	movl	0(%r13), %eax
	leal	1(%r15), %edx
	salq	$32, %r14
	movl	%edx, 864(%rbx)
	cmpl	$-1, %eax
	je	.L2520
	cmpb	$2, 496(%rbx)
	je	.L2520
	movb	$1, 496(%rbx)
	movl	%eax, 500(%rbx)
.L2520:
	movl	824(%rbx), %esi
	movl	%r15d, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder16SuspendGeneratorENS1_8RegisterENS1_12RegisterListEi@PLT
	movq	856(%rbx), %rsi
	movl	%r15d, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4BindEPNS1_17BytecodeJumpTableEi@PLT
	movl	824(%rbx), %esi
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder15ResumeGeneratorENS1_8RegisterENS1_12RegisterListE@PLT
	testb	$-128, 4(%r13)
	je	.L2578
.L2511:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2579
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2578:
	.cfi_restore_state
	movl	328(%rbx), %r14d
	movq	336(%rbx), %rdi
	leal	1(%r14), %eax
	cmpl	%eax, 332(%rbx)
	movl	%eax, 328(%rbx)
	cmovge	332(%rbx), %eax
	movl	%eax, 332(%rbx)
	testq	%rdi, %rdi
	je	.L2523
	movq	(%rdi), %rax
	movl	%r14d, %esi
	call	*16(%rax)
.L2523:
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movl	824(%rbx), %edx
	movl	$482, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_8RegisterE@PLT
	xorl	%edx, %edx
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder17AllocateJumpTableEii@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r15
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder21SwitchOnSmiNoFeedbackEPNS1_17BytecodeJumpTableE@PLT
	movl	0(%r13), %eax
	cmpl	$-1, %eax
	je	.L2524
	cmpb	$2, 496(%rbx)
	je	.L2524
	movb	$1, 496(%rbx)
	movl	%eax, 500(%rbx)
.L2524:
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder5ThrowEv@PLT
	movl	$1, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4BindEPNS1_17BytecodeJumpTableEi@PLT
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	movq	512(%rbx), %rax
	movq	16(%rax), %rdi
	call	_ZNK2v88internal15FunctionLiteral4kindEv@PLT
	movq	800(%rbx), %rdi
	subl	$12, %eax
	cmpb	$1, %al
	ja	.L2529
	jmp	.L2527
	.p2align 4,,10
	.p2align 3
.L2580:
	movq	-72(%rbp), %rdi
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2528
.L2529:
	movq	(%rdi), %rax
	xorl	%edx, %edx
	movq	%rdi, -72(%rbp)
	movl	$-1, %ecx
	movl	$2, %esi
	call	*16(%rax)
	testb	%al, %al
	je	.L2580
.L2526:
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4BindEPNS1_17BytecodeJumpTableEi@PLT
	movq	568(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L2531
	movq	40(%rbx), %rax
	leaq	16(%rax), %rcx
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L2531
	movq	%rcx, %rdx
	jmp	.L2533
	.p2align 4,,10
	.p2align 3
.L2581:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L2534
.L2533:
	cmpq	%r13, 32(%rax)
	jnb	.L2581
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L2533
.L2534:
	cmpq	%rcx, %rdx
	je	.L2531
	cmpq	%r13, 32(%rdx)
	ja	.L2531
	movq	40(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L2531
	movq	(%rdi), %rax
	movl	$2, %esi
	call	*16(%rax)
	movq	%rax, -64(%rbp)
	cmpl	$-1, %eax
	je	.L2531
	movq	16(%rbx), %r13
	subq	8(%rbx), %r13
	leaq	-64(%rbp), %rsi
	movq	%rbx, %rdi
	sarq	$3, %r13
	call	_ZNSt6vectorIN2v88internal11SourceRangeENS1_13ZoneAllocatorIS2_EEE12emplace_backIJRS2_EEEvDpOT_
	cmpl	$-1, %r13d
	je	.L2531
	movq	32(%rbx), %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder15IncBlockCounterEi@PLT
.L2531:
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	jmp	.L2511
	.p2align 4,,10
	.p2align 3
.L2582:
	movq	-72(%rbp), %rdi
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2528
.L2527:
	movq	(%rdi), %rax
	xorl	%edx, %edx
	movq	%rdi, -72(%rbp)
	movl	$-1, %ecx
	movl	$3, %esi
	call	*16(%rax)
	testb	%al, %al
	je	.L2582
	jmp	.L2526
	.p2align 4,,10
	.p2align 3
.L2513:
	movq	512(%rbx), %rax
	movq	16(%rax), %rdi
	call	_ZNK2v88internal15FunctionLiteral4kindEv@PLT
	movl	328(%rbx), %r14d
	subl	$12, %eax
	movq	%r14, %r15
	cmpb	$1, %al
	jbe	.L2583
	leal	2(%r14), %eax
	movq	336(%rbx), %rdi
	cmpl	%eax, 332(%rbx)
	movl	%eax, 328(%rbx)
	cmovge	332(%rbx), %eax
	movl	%eax, 332(%rbx)
	testq	%rdi, %rdi
	je	.L2518
	movq	(%rdi), %rax
	movq	%r14, %rsi
	btsq	$33, %rsi
	call	*24(%rax)
.L2518:
	movq	%r14, %rdx
	movl	%r15d, %esi
	movq	%r12, %rdi
	btsq	$33, %rdx
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder9LoadFalseEv@PLT
	leal	1(%r15), %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-72(%rbp), %rdx
	movl	$488, %esi
	movq	%rax, %rdi
.L2577:
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_12RegisterListE@PLT
	movq	336(%rbx), %rdi
	movl	328(%rbx), %esi
	movl	%r15d, 328(%rbx)
	testq	%rdi, %rdi
	je	.L2519
	movq	(%rdi), %rax
	subl	%r15d, %esi
	salq	$32, %rsi
	orq	%r14, %rsi
	call	*32(%rax)
	movl	328(%rbx), %r14d
.L2519:
	movl	864(%rbx), %r15d
	jmp	.L2514
	.p2align 4,,10
	.p2align 3
.L2583:
	leal	3(%r14), %eax
	movq	336(%rbx), %rdi
	cmpl	%eax, 332(%rbx)
	movl	%eax, 328(%rbx)
	cmovge	332(%rbx), %eax
	movl	%eax, 332(%rbx)
	testq	%rdi, %rdi
	je	.L2516
	movabsq	$12884901888, %rsi
	movq	(%rdi), %rax
	orq	%r14, %rsi
	call	*24(%rax)
.L2516:
	movl	824(%rbx), %esi
	movl	%r15d, %edx
	movq	%r12, %rdi
	movabsq	$12884901888, %r8
	orq	%r14, %r8
	movq	%r8, -72(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder12MoveRegisterENS1_8RegisterES3_@PLT
	leal	1(%r15), %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	xorl	%esi, %esi
	cmpl	$4, 872(%rbx)
	setne	%sil
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadBooleanEb@PLT
	leal	2(%r15), %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-72(%rbp), %r8
	movl	$479, %esi
	movq	%rax, %rdi
	movq	%r8, %rdx
	jmp	.L2577
.L2528:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2579:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20922:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator10VisitYieldEPNS0_5YieldE, .-_ZN2v88internal11interpreter17BytecodeGenerator10VisitYieldEPNS0_5YieldE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator14VisitYieldStarEPNS0_9YieldStarE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator14VisitYieldStarEPNS0_9YieldStarE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator14VisitYieldStarEPNS0_9YieldStarE, @function
_ZN2v88internal11interpreter17BytecodeGenerator14VisitYieldStarEPNS0_9YieldStarE:
.LFB20923:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$360, %rsp
	movq	%rsi, -328(%rbp)
	movl	328(%rdi), %esi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leal	1(%rsi), %eax
	cmpl	%eax, 332(%rdi)
	movl	%esi, -376(%rbp)
	movl	%eax, 328(%rdi)
	movl	%eax, -332(%rbp)
	cmovge	332(%rdi), %eax
	movl	%eax, 332(%rdi)
	movq	336(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2585
	movq	(%rdi), %rax
	call	*16(%rax)
	movl	328(%rbx), %esi
	movl	332(%rbx), %edx
	movq	336(%rbx), %rdi
	leal	1(%rsi), %eax
	movl	%esi, -332(%rbp)
	cmpl	%edx, %eax
	movl	%eax, 328(%rbx)
	cmovl	%edx, %eax
	movl	%eax, 332(%rbx)
	testq	%rdi, %rdi
	je	.L2586
	movq	(%rdi), %rax
	call	*16(%rax)
.L2586:
	movq	512(%rbx), %rax
	xorl	%r13d, %r13d
	movq	16(%rax), %rdi
	call	_ZNK2v88internal15FunctionLiteral4kindEv@PLT
	movl	328(%rbx), %r14d
	movq	336(%rbx), %rdi
	subl	$12, %eax
	cmpb	$1, %al
	movb	%al, -333(%rbp)
	leal	2(%r14), %eax
	movl	%eax, 328(%rbx)
	setbe	%r13b
	cmpl	%eax, 332(%rbx)
	cmovge	332(%rbx), %eax
	movl	%eax, 332(%rbx)
	movl	%r14d, %eax
	movq	%rax, -384(%rbp)
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L2587
	movq	(%rdi), %rax
	btsq	$33, %rsi
	call	*24(%rax)
.L2587:
	movq	-384(%rbp), %rax
	movq	%rbx, %rdi
	btsq	$33, %rax
	movq	%rax, -352(%rbp)
	movq	-328(%rbp), %rax
	movq	8(%rax), %rsi
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	movl	328(%rbx), %r15d
	movq	336(%rbx), %rdi
	leal	1(%r15), %eax
	cmpl	%eax, 332(%rbx)
	movl	%eax, 328(%rbx)
	cmovge	332(%rbx), %eax
	movl	%eax, 332(%rbx)
	testq	%rdi, %rdi
	je	.L2588
	movq	(%rdi), %rax
	movl	%r15d, %esi
	call	*16(%rax)
.L2588:
	leaq	24(%rbx), %r12
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator16BuildGetIteratorENS0_12IteratorTypeE
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movl	$5, %esi
	movq	%rax, -344(%rbp)
	movq	512(%rbx), %rax
	leaq	56(%rax), %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movq	-344(%rbp), %r8
	movl	%r14d, %esi
	movl	%eax, %ecx
	movq	520(%rbx), %rax
	movq	%r8, %rdi
	movq	368(%rax), %rdx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder17LoadNamedPropertyENS1_8RegisterEPKNS0_12AstRawStringEi@PLT
	movl	%r15d, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	%r12, %rdi
	movl	%r13d, -316(%rbp)
	leaq	-288(%rbp), %r13
	movl	%r15d, -308(%rbp)
	leal	1(%r14), %r15d
	movl	%r14d, -312(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder13LoadUndefinedEv@PLT
	movl	%r15d, %esi
	movl	%r15d, -372(%rbp)
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralENS0_3SmiE@PLT
	movl	-332(%rbp), %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	24(%rbx), %rax
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVN2v88internal11interpreter11LoopBuilderE(%rip), %rcx
	leaq	-168(%rbp), %rdx
	leaq	-192(%rbp), %rdi
	movups	%xmm0, -136(%rbp)
	movq	%rax, -176(%rbp)
	movq	%rax, -112(%rbp)
	leaq	-104(%rbp), %rax
	movq	%rcx, -192(%rbp)
	movq	%rdi, -368(%rbp)
	movq	%rdx, -160(%rbp)
	movq	%rdx, -168(%rbp)
	movq	%rax, -96(%rbp)
	movq	%rax, -104(%rbp)
	movq	%r12, -184(%rbp)
	movq	$0, -152(%rbp)
	movb	$0, -144(%rbp)
	movq	$-1, -120(%rbp)
	movq	$0, -88(%rbp)
	movb	$0, -80(%rbp)
	call	_ZN2v88internal11interpreter11LoopBuilder10LoopHeaderEv@PLT
	movq	16(%rbx), %rax
	movl	$1, %edx
	movq	%r12, %rdi
	movl	$2, %esi
	movb	$0, -256(%rbp)
	movq	%rax, -288(%rbp)
	leaq	-280(%rbp), %rax
	movq	%rax, -400(%rbp)
	movq	%rax, -272(%rbp)
	movq	%rax, -280(%rbp)
	movq	$0, -264(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder17AllocateJumpTableEii@PLT
	movl	-332(%rbp), %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	movq	%rax, -392(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder21SwitchOnSmiNoFeedbackEPNS1_17BytecodeJumpTableE@PLT
	movq	512(%rbx), %rax
	movl	$4, %esi
	leaq	56(%rax), %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movq	-352(%rbp), %rdx
	movl	-308(%rbp), %esi
	movq	%r12, %rdi
	movl	%eax, %ecx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder12CallPropertyENS1_8RegisterENS1_12RegisterListEi@PLT
	movq	%r13, %rdi
	movq	%r13, -360(%rbp)
	leaq	-232(%rbp), %r13
	call	_ZN2v88internal11interpreter14BytecodeLabels3NewEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4JumpEPNS1_13BytecodeLabelE@PLT
	movq	%r15, %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4BindEPNS1_17BytecodeJumpTableEi@PLT
	movq	520(%rbx), %rax
	movq	-360(%rbp), %rdi
	movq	424(%rax), %r15
	movq	16(%rbx), %rax
	movq	%r13, -224(%rbp)
	movq	%r13, -232(%rbp)
	movq	%rax, -240(%rbp)
	leaq	-240(%rbp), %rax
	movq	%rax, -344(%rbp)
	movq	$0, -216(%rbp)
	movb	$0, -208(%rbp)
	call	_ZN2v88internal11interpreter14BytecodeLabels3NewEv@PLT
	movq	-344(%rbp), %r9
	movq	%r15, %rdx
	movq	%rbx, %rdi
	movq	-352(%rbp), %rcx
	movl	-312(%rbp), %esi
	movq	%rax, %r8
	call	_ZN2v88internal11interpreter17BytecodeGenerator23BuildCallIteratorMethodENS1_8RegisterEPKNS0_12AstRawStringENS1_12RegisterListEPNS1_13BytecodeLabelEPNS1_14BytecodeLabelsE
	movq	-344(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal11interpreter14BytecodeLabels4BindEPNS1_20BytecodeArrayBuilderE@PLT
	movl	-372(%rbp), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	cmpb	$1, -333(%rbp)
	jbe	.L2667
	movq	800(%rbx), %r15
	jmp	.L2593
	.p2align 4,,10
	.p2align 3
.L2668:
	movq	16(%r15), %r15
	testq	%r15, %r15
	je	.L2592
.L2593:
	movq	(%r15), %rax
	xorl	%edx, %edx
	movl	$-1, %ecx
	movl	$2, %esi
	movq	%r15, %rdi
	call	*16(%rax)
	testb	%al, %al
	je	.L2668
.L2590:
	movq	-232(%rbp), %rax
	cmpq	%r13, %rax
	je	.L2594
	.p2align 4,,10
	.p2align 3
.L2595:
	movq	(%rax), %rax
	cmpq	%r13, %rax
	jne	.L2595
.L2594:
	movq	-392(%rbp), %rsi
	movl	$2, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4BindEPNS1_17BytecodeJumpTableEi@PLT
	movq	520(%rbx), %rax
	movq	-360(%rbp), %rdi
	movq	488(%rax), %rdx
	movq	16(%rbx), %rax
	movq	%r13, -224(%rbp)
	movq	%r13, -232(%rbp)
	movq	%rdx, -392(%rbp)
	movq	%rax, -240(%rbp)
	movq	$0, -216(%rbp)
	movb	$0, -208(%rbp)
	call	_ZN2v88internal11interpreter14BytecodeLabels3NewEv@PLT
	movq	-344(%rbp), %r15
	movq	%rbx, %rdi
	movq	-352(%rbp), %rcx
	movq	-392(%rbp), %rdx
	movl	-312(%rbp), %esi
	movq	%rax, %r8
	movq	%r15, %r9
	call	_ZN2v88internal11interpreter17BytecodeGenerator23BuildCallIteratorMethodENS1_8RegisterEPKNS0_12AstRawStringENS1_12RegisterListEPNS1_13BytecodeLabelEPNS1_14BytecodeLabelsE
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter14BytecodeLabels4BindEPNS1_20BytecodeArrayBuilderE@PLT
	movq	-328(%rbp), %rdx
	leaq	-316(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator18BuildIteratorCloseERKNS2_14IteratorRecordEPNS0_10ExpressionE
	movl	$177, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdE@PLT
	movq	-232(%rbp), %rax
	cmpq	%r13, %rax
	je	.L2596
	.p2align 4,,10
	.p2align 3
.L2597:
	movq	(%rax), %rax
	cmpq	%r13, %rax
	jne	.L2597
.L2596:
	movq	-360(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal11interpreter14BytecodeLabels4BindEPNS1_20BytecodeArrayBuilderE@PLT
	movq	-280(%rbp), %rax
	movq	-400(%rbp), %rdx
	cmpq	%rdx, %rax
	je	.L2598
	.p2align 4,,10
	.p2align 3
.L2599:
	movq	(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2599
.L2598:
	cmpb	$1, -333(%rbp)
	jbe	.L2669
	movl	-376(%rbp), %r15d
	movq	%r12, %rdi
	movb	$0, -240(%rbp)
	movq	$-1, -232(%rbp)
	movl	%r15d, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-344(%rbp), %r13
	movq	%rax, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder16JumpIfJSReceiverEPNS1_13BytecodeLabelE@PLT
	movl	%r15d, %edx
	movl	$168, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_8RegisterE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4BindEPNS1_13BytecodeLabelE@PLT
	movq	512(%rbx), %rax
	movl	$5, %esi
	leaq	56(%rax), %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movl	%r15d, %esi
	movq	%r12, %rdi
	movl	%eax, %ecx
	movq	520(%rbx), %rax
	movq	192(%rax), %rdx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder17LoadNamedPropertyENS1_8RegisterEPKNS0_12AstRawStringEi@PLT
	movq	-368(%rbp), %rdi
	leaq	-176(%rbp), %rdx
	xorl	%esi, %esi
	call	_ZN2v88internal11interpreter27BreakableControlFlowBuilder14EmitJumpIfTrueENS1_20BytecodeArrayBuilder13ToBooleanModeEPNS1_14BytecodeLabelsE@PLT
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	movl	328(%rbx), %r13d
.L2619:
	movq	-328(%rbp), %rax
	movl	864(%rbx), %r15d
	salq	$32, %r13
	movl	(%rax), %eax
	leal	1(%r15), %edx
	movl	%edx, 864(%rbx)
	cmpl	$-1, %eax
	je	.L2603
	cmpb	$2, 496(%rbx)
	je	.L2603
	movb	$1, 496(%rbx)
	movl	%eax, 500(%rbx)
.L2603:
	movl	824(%rbx), %esi
	movl	%r15d, %ecx
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder16SuspendGeneratorENS1_8RegisterENS1_12RegisterListEi@PLT
	movq	856(%rbx), %rsi
	movl	%r15d, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4BindEPNS1_17BytecodeJumpTableEi@PLT
	movl	824(%rbx), %esi
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder15ResumeGeneratorENS1_8RegisterENS1_12RegisterListE@PLT
	movl	-372(%rbp), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movl	824(%rbx), %edx
	movl	$482, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_8RegisterE@PLT
	movl	-332(%rbp), %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-368(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter11LoopBuilder18BindContinueTargetEv@PLT
	movl	868(%rbx), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter11LoopBuilder12JumpToHeaderEi@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter11LoopBuilderD1Ev@PLT
	movq	336(%rbx), %rdi
	movl	328(%rbx), %esi
	movl	%r14d, 328(%rbx)
	testq	%rdi, %rdi
	je	.L2604
	movq	(%rdi), %rax
	subl	%r14d, %esi
	salq	$32, %rsi
	orq	-384(%rbp), %rsi
	call	*32(%rax)
	movl	328(%rbx), %r14d
	movq	336(%rbx), %rdi
	movb	$0, -304(%rbp)
	movq	$-1, -296(%rbp)
	leal	1(%r14), %eax
	cmpl	%eax, 332(%rbx)
	movl	%eax, 328(%rbx)
	cmovge	332(%rbx), %eax
	movl	%eax, 332(%rbx)
	testq	%rdi, %rdi
	je	.L2605
	movq	(%rdi), %rax
	movl	%r14d, %esi
	call	*16(%rax)
.L2605:
	movq	512(%rbx), %rax
	movl	$5, %esi
	leaq	-304(%rbp), %r13
	leaq	56(%rax), %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movl	-376(%rbp), %esi
	movq	%r12, %rdi
	movl	%eax, %ecx
	movq	520(%rbx), %rax
	movq	504(%rax), %rdx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder17LoadNamedPropertyENS1_8RegisterEPKNS0_12AstRawStringEi@PLT
	movl	%r14d, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movabsq	$4294967296, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralENS0_3SmiE@PLT
	movl	-332(%rbp), %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder16CompareReferenceENS1_8RegisterE@PLT
	movq	%r13, %rdx
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11JumpIfFalseENS2_13ToBooleanModeEPNS1_13BytecodeLabelE@PLT
	movl	%r14d, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	cmpb	$1, -333(%rbp)
	movq	800(%rbx), %r15
	ja	.L2609
	jmp	.L2608
	.p2align 4,,10
	.p2align 3
.L2670:
	movq	16(%r15), %r15
	testq	%r15, %r15
	je	.L2592
.L2609:
	movq	(%r15), %rax
	xorl	%edx, %edx
	movl	$-1, %ecx
	movl	$2, %esi
	movq	%r15, %rdi
	call	*16(%rax)
	testb	%al, %al
	je	.L2670
.L2607:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4BindEPNS1_13BytecodeLabelE@PLT
	movq	568(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L2611
	movq	40(%rbx), %rax
	leaq	16(%rax), %rcx
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L2611
	movq	-328(%rbp), %rsi
	movq	%rcx, %rdx
	jmp	.L2613
	.p2align 4,,10
	.p2align 3
.L2671:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L2614
.L2613:
	cmpq	%rsi, 32(%rax)
	jnb	.L2671
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L2613
.L2614:
	cmpq	%rcx, %rdx
	je	.L2611
	movq	-328(%rbp), %rax
	cmpq	%rax, 32(%rdx)
	ja	.L2611
	movq	40(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L2611
	movq	(%rdi), %rax
	movl	$2, %esi
	call	*16(%rax)
	movq	%rax, -192(%rbp)
	cmpl	$-1, %eax
	je	.L2611
	movq	16(%rbx), %r13
	movq	-368(%rbp), %rsi
	movq	%rbx, %rdi
	subq	8(%rbx), %r13
	sarq	$3, %r13
	call	_ZNSt6vectorIN2v88internal11SourceRangeENS1_13ZoneAllocatorIS2_EEE12emplace_backIJRS2_EEEvDpOT_
	cmpl	$-1, %r13d
	je	.L2611
	movq	32(%rbx), %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder15IncBlockCounterEi@PLT
.L2611:
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2672
	addq	$360, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2673:
	.cfi_restore_state
	movq	16(%r15), %r15
	testq	%r15, %r15
	je	.L2592
.L2608:
	movq	(%r15), %rax
	xorl	%edx, %edx
	movl	$-1, %ecx
	movl	$3, %esi
	movq	%r15, %rdi
	call	*16(%rax)
	testb	%al, %al
	je	.L2673
	jmp	.L2607
	.p2align 4,,10
	.p2align 3
.L2669:
	movq	-328(%rbp), %rax
	movq	%rbx, %rdi
	movl	(%rax), %esi
	call	_ZN2v88internal11interpreter17BytecodeGenerator10BuildAwaitEi
	movl	-376(%rbp), %r15d
	movq	%r12, %rdi
	movb	$0, -240(%rbp)
	movq	$-1, -232(%rbp)
	movl	%r15d, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-344(%rbp), %r13
	movq	%rax, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder16JumpIfJSReceiverEPNS1_13BytecodeLabelE@PLT
	movl	%r15d, %edx
	movl	$168, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_8RegisterE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4BindEPNS1_13BytecodeLabelE@PLT
	movq	512(%rbx), %rax
	movl	$5, %esi
	leaq	56(%rax), %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movl	%r15d, %esi
	movq	%r12, %rdi
	movl	%eax, %ecx
	movq	520(%rbx), %rax
	movq	192(%rax), %rdx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder17LoadNamedPropertyENS1_8RegisterEPKNS0_12AstRawStringEi@PLT
	movq	-368(%rbp), %rdi
	leaq	-176(%rbp), %rdx
	xorl	%esi, %esi
	call	_ZN2v88internal11interpreter27BreakableControlFlowBuilder14EmitJumpIfTrueENS1_20BytecodeArrayBuilder13ToBooleanModeEPNS1_14BytecodeLabelsE@PLT
	movq	512(%rbx), %rax
	movl	$5, %esi
	movl	328(%rbx), %r13d
	leaq	56(%rax), %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movl	%r15d, %esi
	movq	%r12, %rdi
	movl	%eax, %ecx
	movq	520(%rbx), %rax
	movq	504(%rax), %rdx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder17LoadNamedPropertyENS1_8RegisterEPKNS0_12AstRawStringEi@PLT
	movl	328(%rbx), %r8d
	movq	336(%rbx), %rdi
	leal	3(%r8), %eax
	cmpl	%eax, 332(%rbx)
	movq	%r8, %r15
	movl	%eax, 328(%rbx)
	cmovge	332(%rbx), %eax
	movl	%eax, 332(%rbx)
	testq	%rdi, %rdi
	je	.L2601
	movq	(%rdi), %rax
	movq	%r8, -344(%rbp)
	movabsq	$12884901888, %rsi
	orq	%r8, %rsi
	call	*24(%rax)
	movq	-344(%rbp), %r8
.L2601:
	movl	824(%rbx), %esi
	movl	%r15d, %edx
	movq	%r12, %rdi
	movabsq	$12884901888, %rax
	orq	%rax, %r8
	movq	%r8, -344(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder12MoveRegisterENS1_8RegisterES3_@PLT
	leal	1(%r15), %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	xorl	%esi, %esi
	cmpl	$4, 872(%rbx)
	setne	%sil
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadBooleanEb@PLT
	leal	2(%r15), %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-344(%rbp), %r8
	movl	$479, %esi
	movq	%rax, %rdi
	movq	%r8, %rdx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_12RegisterListE@PLT
	movq	336(%rbx), %rdi
	movl	328(%rbx), %esi
	movl	%r13d, 328(%rbx)
	testq	%rdi, %rdi
	je	.L2619
	subl	%r13d, %esi
	movq	(%rdi), %rax
	salq	$32, %rsi
	orq	%r13, %rsi
	call	*32(%rax)
	movl	328(%rbx), %r13d
	jmp	.L2619
	.p2align 4,,10
	.p2align 3
.L2667:
	movq	-328(%rbp), %rax
	movq	%rbx, %rdi
	movl	(%rax), %esi
	call	_ZN2v88internal11interpreter17BytecodeGenerator10BuildAwaitEi
	movq	800(%rbx), %r15
	jmp	.L2591
	.p2align 4,,10
	.p2align 3
.L2674:
	movq	16(%r15), %r15
	testq	%r15, %r15
	je	.L2592
.L2591:
	movq	(%r15), %rax
	xorl	%edx, %edx
	movl	$-1, %ecx
	movl	$3, %esi
	movq	%r15, %rdi
	call	*16(%rax)
	testb	%al, %al
	je	.L2674
	jmp	.L2590
	.p2align 4,,10
	.p2align 3
.L2585:
	leal	2(%rsi), %edx
	cmpl	%eax, %edx
	movl	%edx, 328(%rbx)
	cmovge	%edx, %eax
	movl	%eax, 332(%rbx)
	jmp	.L2586
	.p2align 4,,10
	.p2align 3
.L2604:
	movl	-372(%rbp), %eax
	cmpl	%eax, 332(%rbx)
	movb	$0, -304(%rbp)
	movq	$-1, -296(%rbp)
	movl	%eax, 328(%rbx)
	cmovge	332(%rbx), %eax
	movl	%eax, 332(%rbx)
	jmp	.L2605
.L2592:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2672:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20923:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator14VisitYieldStarEPNS0_9YieldStarE, .-_ZN2v88internal11interpreter17BytecodeGenerator14VisitYieldStarEPNS0_9YieldStarE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator10VisitAwaitEPNS0_5AwaitE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator10VisitAwaitEPNS0_5AwaitE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator10VisitAwaitEPNS0_5AwaitE, @function
_ZN2v88internal11interpreter17BytecodeGenerator10VisitAwaitEPNS0_5AwaitE:
.LFB20925:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	(%rsi), %eax
	cmpl	$-1, %eax
	je	.L2676
	cmpb	$2, 496(%rdi)
	je	.L2676
	movb	$1, 496(%rdi)
	movl	%eax, 500(%rdi)
.L2676:
	movq	8(%r12), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	movl	(%r12), %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator10BuildAwaitEi
	movq	568(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L2675
	movq	40(%rbx), %rax
	leaq	16(%rax), %rcx
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L2675
	movq	%rcx, %rdx
	jmp	.L2680
	.p2align 4,,10
	.p2align 3
.L2697:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L2681
.L2680:
	cmpq	%r12, 32(%rax)
	jnb	.L2697
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L2680
.L2681:
	cmpq	%rdx, %rcx
	je	.L2675
	cmpq	%r12, 32(%rdx)
	ja	.L2675
	movq	40(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L2675
	movq	(%rdi), %rax
	movl	$2, %esi
	call	*16(%rax)
	movq	%rax, -32(%rbp)
	cmpl	$-1, %eax
	je	.L2675
	movq	16(%rbx), %r12
	subq	8(%rbx), %r12
	leaq	-32(%rbp), %rsi
	movq	%rbx, %rdi
	sarq	$3, %r12
	call	_ZNSt6vectorIN2v88internal11SourceRangeENS1_13ZoneAllocatorIS2_EEE12emplace_backIJRS2_EEEvDpOT_
	cmpl	$-1, %r12d
	je	.L2675
	movq	32(%rbx), %rdi
	movl	%r12d, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder15IncBlockCounterEi@PLT
.L2675:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2698
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2698:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20925:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator10VisitAwaitEPNS0_5AwaitE, .-_ZN2v88internal11interpreter17BytecodeGenerator10VisitAwaitEPNS0_5AwaitE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator10VisitThrowEPNS0_5ThrowE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator10VisitThrowEPNS0_5ThrowE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator10VisitThrowEPNS0_5ThrowE, @function
_ZN2v88internal11interpreter17BytecodeGenerator10VisitThrowEPNS0_5ThrowE:
.LFB20926:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	568(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%r13, %r13
	je	.L2700
	movq	40(%r13), %rax
	leaq	16(%rax), %rcx
	movq	24(%rax), %rax
	movq	%rcx, %rdx
	testq	%rax, %rax
	jne	.L2703
	jmp	.L2700
	.p2align 4,,10
	.p2align 3
.L2721:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L2704
.L2703:
	cmpq	%r12, 32(%rax)
	jnb	.L2721
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L2703
.L2704:
	cmpq	%rdx, %rcx
	je	.L2700
	cmpq	%r12, 32(%rdx)
	jbe	.L2722
.L2700:
	movq	8(%r12), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	movl	(%r12), %eax
	leaq	24(%rbx), %rdi
	cmpl	$-1, %eax
	je	.L2708
	cmpb	$2, 496(%rbx)
	je	.L2708
	movb	$1, 496(%rbx)
	movl	%eax, 500(%rbx)
.L2708:
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder5ThrowEv@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2723
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2722:
	.cfi_restore_state
	movq	40(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L2700
	movq	(%rdi), %rax
	movl	$2, %esi
	call	*16(%rax)
	movq	%rax, -48(%rbp)
	cmpl	$-1, %eax
	je	.L2700
	leaq	-48(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt6vectorIN2v88internal11SourceRangeENS1_13ZoneAllocatorIS2_EEE12emplace_backIJRS2_EEEvDpOT_
	jmp	.L2700
.L2723:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20926:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator10VisitThrowEPNS0_5ThrowE, .-_ZN2v88internal11interpreter17BytecodeGenerator10VisitThrowEPNS0_5ThrowE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator18VisitOptionalChainEPNS0_13OptionalChainE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator18VisitOptionalChainEPNS0_13OptionalChainE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator18VisitOptionalChainEPNS0_13OptionalChainE, @function
_ZN2v88internal11interpreter17BytecodeGenerator18VisitOptionalChainEPNS0_13OptionalChainE:
.LFB20934:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-88(%rbp), %r15
	leaq	-112(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-80(%rbp), %r13
	pushq	%r12
	movq	%r13, %xmm0
	.cfi_offset 12, -48
	movq	%rdi, %r12
	punpcklqdq	%xmm0, %xmm0
	addq	$24, %r12
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movaps	%xmm0, -80(%rbp)
	movq	%rdi, -96(%rbp)
	movq	%rax, -88(%rbp)
	movq	832(%rdi), %rax
	movq	%r15, 832(%rdi)
	movq	8(%rsi), %rsi
	movb	$0, -112(%rbp)
	movq	$-1, -104(%rbp)
	movq	$0, -64(%rbp)
	movb	$0, -56(%rbp)
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4JumpEPNS1_13BytecodeLabelE@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter14BytecodeLabels4BindEPNS1_20BytecodeArrayBuilderE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder13LoadUndefinedEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4BindEPNS1_13BytecodeLabelE@PLT
	movq	-96(%rbp), %rax
	movq	-48(%rbp), %rdx
	movq	%rdx, 832(%rax)
	movq	-80(%rbp), %rax
	cmpq	%r13, %rax
	je	.L2724
	.p2align 4,,10
	.p2align 3
.L2726:
	movq	(%rax), %rax
	cmpq	%r13, %rax
	jne	.L2726
.L2724:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2730
	addq	$80, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2730:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20934:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator18VisitOptionalChainEPNS0_13OptionalChainE, .-_ZN2v88internal11interpreter17BytecodeGenerator18VisitOptionalChainEPNS0_13OptionalChainE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator19VisitForTypeOfValueEPNS0_10ExpressionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator19VisitForTypeOfValueEPNS0_10ExpressionE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator19VisitForTypeOfValueEPNS0_10ExpressionE, @function
_ZN2v88internal11interpreter17BytecodeGenerator19VisitForTypeOfValueEPNS0_10ExpressionE:
.LFB20943:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movl	4(%rsi), %edx
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	%edx, %eax
	andl	$63, %eax
	cmpb	$54, %al
	je	.L2740
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
.L2731:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2741
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2740:
	.cfi_restore_state
	movq	816(%rdi), %rax
	movq	8(%rsi), %rsi
	shrl	$11, %edx
	xorl	%ecx, %ecx
	andl	$1, %edx
	movq	%rdi, -40(%rbp)
	movq	%rax, -48(%rbp)
	movl	328(%rdi), %eax
	movq	$2, -24(%rbp)
	movl	%eax, -32(%rbp)
	leaq	-48(%rbp), %rax
	movq	%rax, 816(%rdi)
	call	_ZN2v88internal11interpreter17BytecodeGenerator17BuildVariableLoadEPNS0_8VariableENS0_13HoleCheckModeENS0_10TypeofModeE
	movq	-40(%rbp), %rdx
	movq	-48(%rbp), %rax
	movq	336(%rdx), %rdi
	movq	%rax, 816(%rdx)
	movl	-32(%rbp), %eax
	movl	328(%rdx), %esi
	movl	%eax, 328(%rdx)
	testq	%rdi, %rdi
	je	.L2731
	subl	%eax, %esi
	movq	(%rdi), %rcx
	movq	%rsi, %rdx
	movl	%eax, %esi
	salq	$32, %rdx
	orq	%rdx, %rsi
	call	*32(%rcx)
	jmp	.L2731
.L2741:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20943:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator19VisitForTypeOfValueEPNS0_10ExpressionE, .-_ZN2v88internal11interpreter17BytecodeGenerator19VisitForTypeOfValueEPNS0_10ExpressionE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator25VisitArithmeticExpressionEPNS0_15BinaryOperationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator25VisitArithmeticExpressionEPNS0_15BinaryOperationE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator25VisitArithmeticExpressionEPNS0_15BinaryOperationE, @function
_ZN2v88internal11interpreter17BytecodeGenerator25VisitArithmeticExpressionEPNS0_15BinaryOperationE:
.LFB20953:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	$15, %esi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	24(%rbx), %r14
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	512(%rdi), %rax
	leaq	56(%rax), %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	leaq	-64(%rbp), %rdx
	leaq	-72(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -64(%rbp)
	movl	%eax, %r13d
	call	_ZN2v88internal15BinaryOperation21IsSmiLiteralOperationEPPNS0_10ExpressionEPNS0_3SmiE@PLT
	testb	%al, %al
	je	.L2743
	movq	-72(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	movl	%eax, %r15d
	movl	(%r12), %eax
	cmpl	$-1, %eax
	je	.L2744
	cmpb	$2, 496(%rbx)
	je	.L2744
	movb	$1, 496(%rbx)
	movl	%eax, 500(%rbx)
.L2744:
	movl	4(%r12), %esi
	movq	-64(%rbp), %rdx
	movl	%r13d, %ecx
	movq	%r14, %rdi
	shrl	$7, %esi
	andl	$127, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder25BinaryOperationSmiLiteralENS0_5Token5ValueENS0_3SmiEi@PLT
	movl	4(%r12), %eax
	shrl	$7, %eax
	andl	$127, %eax
	cmpb	$44, %al
	jne	.L2742
	cmpl	$2, %r15d
	jne	.L2742
	movq	816(%rbx), %rax
	movl	$2, 28(%rax)
	jmp	.L2742
	.p2align 4,,10
	.p2align 3
.L2743:
	movq	8(%r12), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	movl	328(%rbx), %r15d
	movq	336(%rbx), %rdi
	movl	%eax, -84(%rbp)
	leal	1(%r15), %eax
	cmpl	%eax, 332(%rbx)
	movl	%eax, 328(%rbx)
	cmovge	332(%rbx), %eax
	movl	%eax, 332(%rbx)
	testq	%rdi, %rdi
	je	.L2746
	movq	(%rdi), %rax
	movl	%r15d, %esi
	call	*16(%rax)
.L2746:
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	16(%r12), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	movl	4(%r12), %edx
	shrl	$7, %edx
	andl	$127, %edx
	cmpb	$44, %dl
	je	.L2771
.L2747:
	movl	(%r12), %eax
	cmpl	$-1, %eax
	je	.L2749
	cmpb	$2, 496(%rbx)
	je	.L2749
	movb	$1, 496(%rbx)
	movl	%eax, 500(%rbx)
.L2749:
	movl	4(%r12), %esi
	movl	%r13d, %ecx
	movl	%r15d, %edx
	movq	%r14, %rdi
	shrl	$7, %esi
	andl	$127, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder15BinaryOperationENS0_5Token5ValueENS1_8RegisterEi@PLT
.L2742:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2772
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2771:
	.cfi_restore_state
	cmpl	$2, -84(%rbp)
	je	.L2751
	cmpl	$2, %eax
	jne	.L2747
.L2751:
	movq	816(%rbx), %rax
	movl	$2, 28(%rax)
	jmp	.L2747
.L2772:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20953:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator25VisitArithmeticExpressionEPNS0_15BinaryOperationE, .-_ZN2v88internal11interpreter17BytecodeGenerator25VisitArithmeticExpressionEPNS0_15BinaryOperationE
	.section	.text.unlikely._ZN2v88internal11interpreter17BytecodeGenerator29VisitNaryArithmeticExpressionEPNS0_13NaryOperationE,"ax",@progbits
	.align 2
.LCOLDB13:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator29VisitNaryArithmeticExpressionEPNS0_13NaryOperationE,"ax",@progbits
.LHOTB13:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator29VisitNaryArithmeticExpressionEPNS0_13NaryOperationE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator29VisitNaryArithmeticExpressionEPNS0_13NaryOperationE, @function
_ZN2v88internal11interpreter17BytecodeGenerator29VisitNaryArithmeticExpressionEPNS0_13NaryOperationE:
.LFB20954:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rsi), %rsi
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	movq	24(%r15), %rdx
	movl	%eax, -60(%rbp)
	cmpq	32(%r15), %rdx
	je	.L2774
	leaq	24(%r14), %rax
	xorl	%r12d, %r12d
	movq	%rax, -56(%rbp)
	jmp	.L2785
	.p2align 4,,10
	.p2align 3
.L2797:
	movq	24(%r15), %rax
	movl	8(%rax,%r13), %eax
	cmpl	$-1, %eax
	je	.L2776
	cmpb	$2, 496(%r14)
	je	.L2776
	movb	$1, 496(%r14)
	movl	%eax, 500(%r14)
.L2776:
	movq	512(%r14), %rax
	movl	$15, %esi
	leaq	56(%rax), %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movl	%eax, %ecx
	movq	24(%r15), %rax
	movq	(%rax,%r13), %rax
	movzbl	4(%rax), %edx
	andl	$63, %edx
	cmpb	$41, %dl
	jne	.L2777
	movl	4(%r15), %esi
	movslq	8(%rax), %rdx
	movq	-56(%rbp), %rdi
	shrl	$7, %esi
	salq	$32, %rdx
	andl	$127, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder25BinaryOperationSmiLiteralENS0_5Token5ValueENS0_3SmiEi@PLT
.L2778:
	movq	336(%r14), %rdi
	movl	328(%r14), %esi
	movl	%ebx, 328(%r14)
	testq	%rdi, %rdi
	je	.L2782
	subl	%ebx, %esi
	movq	(%rdi), %rdx
	movq	%rsi, %rax
	movl	%ebx, %esi
	salq	$32, %rax
	orq	%rax, %rsi
	call	*32(%rdx)
.L2782:
	movq	24(%r15), %rdx
	movq	32(%r15), %rax
	addq	$1, %r12
	subq	%rdx, %rax
	sarq	$4, %rax
	cmpq	%r12, %rax
	jbe	.L2774
.L2785:
	movq	%r12, %r13
	movl	328(%r14), %ebx
	salq	$4, %r13
	movq	(%rdx,%r13), %rdi
	call	_ZNK2v88internal10Expression12IsSmiLiteralEv@PLT
	testb	%al, %al
	jne	.L2797
	movl	328(%r14), %edx
	movq	336(%r14), %rdi
	leal	1(%rdx), %eax
	cmpl	%eax, 332(%r14)
	movl	%eax, 328(%r14)
	cmovge	332(%r14), %eax
	movl	%eax, 332(%r14)
	testq	%rdi, %rdi
	je	.L2779
	movq	(%rdi), %rax
	movl	%edx, -64(%rbp)
	movl	%edx, %esi
	call	*16(%rax)
	movl	-64(%rbp), %edx
.L2779:
	movq	-56(%rbp), %rdi
	movl	%edx, %esi
	movl	%edx, -64(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	24(%r15), %rax
	movq	%r14, %rdi
	movq	(%rax,%r13), %rsi
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	movl	-64(%rbp), %edx
	cmpl	$2, %eax
	movl	$2, %eax
	cmovne	-60(%rbp), %eax
	movl	%eax, -60(%rbp)
	movq	24(%r15), %rax
	movl	8(%rax,%r13), %eax
	cmpl	$-1, %eax
	je	.L2781
	cmpb	$2, 496(%r14)
	je	.L2781
	movb	$1, 496(%r14)
	movl	%eax, 500(%r14)
.L2781:
	movq	512(%r14), %rax
	movl	$15, %esi
	movl	%edx, -64(%rbp)
	leaq	56(%rax), %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movl	4(%r15), %esi
	movl	-64(%rbp), %edx
	movq	-56(%rbp), %rdi
	movl	%eax, %ecx
	shrl	$7, %esi
	andl	$127, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder15BinaryOperationENS0_5Token5ValueENS1_8RegisterEi@PLT
	jmp	.L2778
	.p2align 4,,10
	.p2align 3
.L2774:
	cmpl	$2, -60(%rbp)
	je	.L2798
.L2773:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2798:
	.cfi_restore_state
	movl	4(%r15), %eax
	shrl	$7, %eax
	andl	$127, %eax
	cmpb	$44, %al
	jne	.L2773
	movq	816(%r14), %rax
	movl	$2, 28(%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal11interpreter17BytecodeGenerator29VisitNaryArithmeticExpressionEPNS0_13NaryOperationE
	.cfi_startproc
	.type	_ZN2v88internal11interpreter17BytecodeGenerator29VisitNaryArithmeticExpressionEPNS0_13NaryOperationE.cold, @function
_ZN2v88internal11interpreter17BytecodeGenerator29VisitNaryArithmeticExpressionEPNS0_13NaryOperationE.cold:
.LFSB20954:
.L2777:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	8, %eax
	ud2
	.cfi_endproc
.LFE20954:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator29VisitNaryArithmeticExpressionEPNS0_13NaryOperationE
	.size	_ZN2v88internal11interpreter17BytecodeGenerator29VisitNaryArithmeticExpressionEPNS0_13NaryOperationE, .-_ZN2v88internal11interpreter17BytecodeGenerator29VisitNaryArithmeticExpressionEPNS0_13NaryOperationE
	.section	.text.unlikely._ZN2v88internal11interpreter17BytecodeGenerator29VisitNaryArithmeticExpressionEPNS0_13NaryOperationE
	.size	_ZN2v88internal11interpreter17BytecodeGenerator29VisitNaryArithmeticExpressionEPNS0_13NaryOperationE.cold, .-_ZN2v88internal11interpreter17BytecodeGenerator29VisitNaryArithmeticExpressionEPNS0_13NaryOperationE.cold
.LCOLDE13:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator29VisitNaryArithmeticExpressionEPNS0_13NaryOperationE
.LHOTE13:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator20VisitTemplateLiteralEPNS0_15TemplateLiteralE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator20VisitTemplateLiteralEPNS0_15TemplateLiteralE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator20VisitTemplateLiteralEPNS0_15TemplateLiteralE, @function
_ZN2v88internal11interpreter17BytecodeGenerator20VisitTemplateLiteralEPNS0_15TemplateLiteralE:
.LFB20978:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	512(%rdi), %rax
	movq	8(%rsi), %r15
	movq	16(%rsi), %r13
	movl	$15, %esi
	leaq	56(%rax), %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movl	328(%r12), %esi
	movq	336(%r12), %rdi
	movl	%eax, -56(%rbp)
	leal	1(%rsi), %eax
	cmpl	%eax, 332(%r12)
	movl	%esi, -52(%rbp)
	movl	%eax, 328(%r12)
	cmovge	332(%r12), %eax
	movl	%eax, 332(%r12)
	testq	%rdi, %rdi
	je	.L2800
	movq	(%rdi), %rax
	call	*16(%rax)
.L2800:
	movl	(%rbx), %eax
	leaq	24(%r12), %r14
	cmpl	$-1, %eax
	je	.L2801
	cmpb	$2, 496(%r12)
	je	.L2801
	movb	$1, 496(%r12)
	movl	%eax, 500(%r12)
.L2801:
	movl	12(%r13), %esi
	testl	%esi, %esi
	jle	.L2802
	xorl	%ebx, %ebx
	jmp	.L2810
	.p2align 4,,10
	.p2align 3
.L2803:
	movq	(%r15), %rax
	movq	(%rax), %rsi
	movl	16(%rsi), %eax
	testl	%eax, %eax
	jne	.L2828
	movq	0(%r13), %rax
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	cmpl	$2, %eax
	jne	.L2829
.L2809:
	addq	$1, %rbx
	cmpl	%ebx, 12(%r13)
	jle	.L2802
.L2810:
	testq	%rbx, %rbx
	je	.L2803
	movl	-52(%rbp), %esi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	(%r15), %rax
	leaq	0(,%rbx,8), %r8
	movq	(%rax,%rbx,8), %rsi
	movl	16(%rsi), %ecx
	testl	%ecx, %ecx
	jne	.L2830
	movq	0(%r13), %rax
	movq	%r12, %rdi
	movq	(%rax,%rbx,8), %rsi
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	cmpl	$2, %eax
	je	.L2807
.L2815:
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder8ToStringEv@PLT
.L2807:
	movl	-56(%rbp), %ecx
	movl	-52(%rbp), %edx
	movl	$44, %esi
	movq	%r14, %rdi
	addq	$1, %rbx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder15BinaryOperationENS0_5Token5ValueENS1_8RegisterEi@PLT
	cmpl	%ebx, 12(%r13)
	jg	.L2810
.L2802:
	movl	12(%r15), %eax
	movq	(%r15), %rdx
	subl	$1, %eax
	cltq
	movq	(%rdx,%rax,8), %rax
	movl	16(%rax), %edx
	testl	%edx, %edx
	jne	.L2831
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2828:
	.cfi_restore_state
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralEPKNS0_12AstRawStringE@PLT
	xorl	%r8d, %r8d
.L2812:
	movl	-52(%rbp), %esi
	movq	%r14, %rdi
	movq	%r8, -64(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	0(%r13), %rax
	movq	-64(%rbp), %r8
	movq	%r12, %rdi
	movq	(%rax,%r8), %rsi
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	cmpl	$2, %eax
	je	.L2807
	jmp	.L2815
	.p2align 4,,10
	.p2align 3
.L2829:
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder8ToStringEv@PLT
	jmp	.L2809
	.p2align 4,,10
	.p2align 3
.L2830:
	movq	%r14, %rdi
	movq	%r8, -64(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralEPKNS0_12AstRawStringE@PLT
	movl	-56(%rbp), %ecx
	movl	-52(%rbp), %edx
	movl	$44, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder15BinaryOperationENS0_5Token5ValueENS1_8RegisterEi@PLT
	movq	-64(%rbp), %r8
	jmp	.L2812
	.p2align 4,,10
	.p2align 3
.L2831:
	movl	-52(%rbp), %ebx
	movq	%r14, %rdi
	movl	%ebx, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movl	12(%r15), %eax
	movq	(%r15), %rdx
	movq	%r14, %rdi
	subl	$1, %eax
	cltq
	movq	(%rdx,%rax,8), %rsi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralEPKNS0_12AstRawStringE@PLT
	movl	-56(%rbp), %ecx
	addq	$24, %rsp
	movl	%ebx, %edx
	movq	%r14, %rdi
	popq	%rbx
	movl	$44, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder15BinaryOperationENS0_5Token5ValueENS1_8RegisterEi@PLT
	.cfi_endproc
.LFE20978:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator20VisitTemplateLiteralEPNS0_15TemplateLiteralE, .-_ZN2v88internal11interpreter17BytecodeGenerator20VisitTemplateLiteralEPNS0_15TemplateLiteralE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator28VisitLogicalAndSubExpressionEPNS0_10ExpressionEPNS1_14BytecodeLabelsEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator28VisitLogicalAndSubExpressionEPNS0_10ExpressionEPNS1_14BytecodeLabelsEi
	.type	_ZN2v88internal11interpreter17BytecodeGenerator28VisitLogicalAndSubExpressionEPNS0_10ExpressionEPNS1_14BytecodeLabelsEi, @function
_ZN2v88internal11interpreter17BytecodeGenerator28VisitLogicalAndSubExpressionEPNS0_10ExpressionEPNS1_14BytecodeLabelsEi:
.LFB20989:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$8, %rsp
	call	_ZNK2v88internal10Expression16ToBooleanIsFalseEv@PLT
	movl	%eax, %r12d
	testb	%al, %al
	jne	.L2837
	movq	%r13, %rdi
	call	_ZNK2v88internal10Expression15ToBooleanIsTrueEv@PLT
	testb	%al, %al
	je	.L2838
.L2835:
	movq	568(%rbx), %rax
	testq	%rax, %rax
	sete	%cl
	cmpl	$-1, %r14d
	sete	%dl
	orb	%dl, %cl
	je	.L2839
.L2832:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2839:
	.cfi_restore_state
	movq	32(%rax), %rdi
	movl	%r14d, %esi
	xorl	%r12d, %r12d
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder15IncBlockCounterEi@PLT
	jmp	.L2832
	.p2align 4,,10
	.p2align 3
.L2838:
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	movq	%r15, %rdi
	movl	%eax, %r13d
	call	_ZN2v88internal11interpreter14BytecodeLabels3NewEv@PLT
	xorl	%esi, %esi
	cmpl	$1, %r13d
	leaq	24(%rbx), %rdi
	movq	%rax, %rdx
	sete	%sil
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11JumpIfFalseENS2_13ToBooleanModeEPNS1_13BytecodeLabelE@PLT
	jmp	.L2835
	.p2align 4,,10
	.p2align 3
.L2837:
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	leaq	24(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter14BytecodeLabels4BindEPNS1_20BytecodeArrayBuilderE@PLT
	jmp	.L2832
	.cfi_endproc
.LFE20989:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator28VisitLogicalAndSubExpressionEPNS0_10ExpressionEPNS1_14BytecodeLabelsEi, .-_ZN2v88internal11interpreter17BytecodeGenerator28VisitLogicalAndSubExpressionEPNS0_10ExpressionEPNS1_14BytecodeLabelsEi
	.section	.text.unlikely._ZN2v88internal11interpreter17BytecodeGenerator20BuildLoadPropertyKeyEPNS0_15LiteralPropertyENS1_8RegisterE,"ax",@progbits
	.align 2
.LCOLDB14:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator20BuildLoadPropertyKeyEPNS0_15LiteralPropertyENS1_8RegisterE,"ax",@progbits
.LHOTB14:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator20BuildLoadPropertyKeyEPNS0_15LiteralPropertyENS1_8RegisterE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator20BuildLoadPropertyKeyEPNS0_15LiteralPropertyENS1_8RegisterE, @function
_ZN2v88internal11interpreter17BytecodeGenerator20BuildLoadPropertyKeyEPNS0_15LiteralPropertyENS1_8RegisterE:
.LFB21013:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rsi), %rdi
	movq	%rsi, %rbx
	leaq	24(%r12), %r14
	andq	$-4, %rdi
	call	_ZNK2v88internal10Expression15IsStringLiteralEv@PLT
	testb	%al, %al
	je	.L2841
	movq	(%rbx), %rax
	andq	$-4, %rax
	movzbl	4(%rax), %edx
	andl	$63, %edx
	cmpb	$41, %dl
	jne	.L2842
	movq	8(%rax), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralEPKNS0_12AstRawStringE@PLT
	popq	%rbx
	movl	%r13d, %esi
	popq	%r12
	movq	%rax, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	.p2align 4,,10
	.p2align 3
.L2841:
	.cfi_restore_state
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	andq	$-4, %rsi
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	popq	%rbx
	movl	%r13d, %esi
	popq	%r12
	movq	%r14, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder6ToNameENS1_8RegisterE@PLT
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal11interpreter17BytecodeGenerator20BuildLoadPropertyKeyEPNS0_15LiteralPropertyENS1_8RegisterE
	.cfi_startproc
	.type	_ZN2v88internal11interpreter17BytecodeGenerator20BuildLoadPropertyKeyEPNS0_15LiteralPropertyENS1_8RegisterE.cold, @function
_ZN2v88internal11interpreter17BytecodeGenerator20BuildLoadPropertyKeyEPNS0_15LiteralPropertyENS1_8RegisterE.cold:
.LFSB21013:
.L2842:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movq	8, %rax
	ud2
	.cfi_endproc
.LFE21013:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator20BuildLoadPropertyKeyEPNS0_15LiteralPropertyENS1_8RegisterE
	.size	_ZN2v88internal11interpreter17BytecodeGenerator20BuildLoadPropertyKeyEPNS0_15LiteralPropertyENS1_8RegisterE, .-_ZN2v88internal11interpreter17BytecodeGenerator20BuildLoadPropertyKeyEPNS0_15LiteralPropertyENS1_8RegisterE
	.section	.text.unlikely._ZN2v88internal11interpreter17BytecodeGenerator20BuildLoadPropertyKeyEPNS0_15LiteralPropertyENS1_8RegisterE
	.size	_ZN2v88internal11interpreter17BytecodeGenerator20BuildLoadPropertyKeyEPNS0_15LiteralPropertyENS1_8RegisterE.cold, .-_ZN2v88internal11interpreter17BytecodeGenerator20BuildLoadPropertyKeyEPNS0_15LiteralPropertyENS1_8RegisterE.cold
.LCOLDE14:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator20BuildLoadPropertyKeyEPNS0_15LiteralPropertyENS1_8RegisterE
.LHOTE14:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator33VisitForAccumulatorValueOrTheHoleEPNS0_10ExpressionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator33VisitForAccumulatorValueOrTheHoleEPNS0_10ExpressionE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator33VisitForAccumulatorValueOrTheHoleEPNS0_10ExpressionE, @function
_ZN2v88internal11interpreter17BytecodeGenerator33VisitForAccumulatorValueOrTheHoleEPNS0_10ExpressionE:
.LFB21019:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L2846
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	.p2align 4,,10
	.p2align 3
.L2846:
	addq	$24, %rdi
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadTheHoleEv@PLT
	.cfi_endproc
.LFE21019:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator33VisitForAccumulatorValueOrTheHoleEPNS0_10ExpressionE, .-_ZN2v88internal11interpreter17BytecodeGenerator33VisitForAccumulatorValueOrTheHoleEPNS0_10ExpressionE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator21VisitForRegisterValueEPNS0_10ExpressionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator21VisitForRegisterValueEPNS0_10ExpressionE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator21VisitForRegisterValueEPNS0_10ExpressionE, @function
_ZN2v88internal11interpreter17BytecodeGenerator21VisitForRegisterValueEPNS0_10ExpressionE:
.LFB21021:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	movl	328(%rbx), %r12d
	movq	336(%rbx), %rdi
	leal	1(%r12), %eax
	cmpl	%eax, 332(%rbx)
	movl	%eax, 328(%rbx)
	cmovge	332(%rbx), %eax
	movl	%eax, 332(%rbx)
	testq	%rdi, %rdi
	je	.L2848
	movq	(%rdi), %rax
	movl	%r12d, %esi
	call	*16(%rax)
.L2848:
	leaq	24(%rbx), %rdi
	movl	%r12d, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21021:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator21VisitForRegisterValueEPNS0_10ExpressionE, .-_ZN2v88internal11interpreter17BytecodeGenerator21VisitForRegisterValueEPNS0_10ExpressionE
	.section	.text.unlikely._ZN2v88internal11interpreter17BytecodeGenerator20PrepareAssignmentLhsEPNS0_10ExpressionENS2_25AccumulatorPreservingModeE,"ax",@progbits
	.align 2
.LCOLDB15:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator20PrepareAssignmentLhsEPNS0_10ExpressionENS2_25AccumulatorPreservingModeE,"ax",@progbits
.LHOTB15:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator20PrepareAssignmentLhsEPNS0_10ExpressionENS2_25AccumulatorPreservingModeE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator20PrepareAssignmentLhsEPNS0_10ExpressionENS2_25AccumulatorPreservingModeE, @function
_ZN2v88internal11interpreter17BytecodeGenerator20PrepareAssignmentLhsEPNS0_10ExpressionENS2_25AccumulatorPreservingModeE:
.LFB20909:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	4(%rdx), %eax
	andl	$63, %eax
	cmpb	$44, %al
	jne	.L2854
	testq	%rdx, %rdx
	je	.L2854
	movq	16(%rdx), %rdi
	movq	%rsi, %r12
	movl	%ecx, %ebx
	call	_ZNK2v88internal10Expression13IsPrivateNameEv@PLT
	testb	%al, %al
	je	.L2855
	movq	16(%r14), %rdx
	movzbl	4(%rdx), %eax
	andl	$63, %eax
	cmpb	$54, %al
	jne	.L2920
	movq	8(%rdx), %rax
	movzwl	40(%rax), %eax
	andl	$15, %eax
	cmpb	$10, %al
	ja	.L2857
	leaq	.L2859(%rip), %rdx
	movzbl	%al, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal11interpreter17BytecodeGenerator20PrepareAssignmentLhsEPNS0_10ExpressionENS2_25AccumulatorPreservingModeE,"a",@progbits
	.align 4
	.align 4
.L2859:
	.long	.L2857-.L2859
	.long	.L2863-.L2859
	.long	.L2857-.L2859
	.long	.L2857-.L2859
	.long	.L2857-.L2859
	.long	.L2857-.L2859
	.long	.L2857-.L2859
	.long	.L2862-.L2859
	.long	.L2889-.L2859
	.long	.L2860-.L2859
	.long	.L2858-.L2859
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator20PrepareAssignmentLhsEPNS0_10ExpressionENS2_25AccumulatorPreservingModeE
	.p2align 4,,10
	.p2align 3
.L2855:
	movq	8(%r14), %rax
	movq	16(%r14), %rdi
	movl	4(%rax), %r13d
	andl	$63, %r13d
	call	_ZNK2v88internal10Expression14IsPropertyNameEv@PLT
	testb	%al, %al
	je	.L2864
	cmpb	$49, %r13b
	je	.L2922
	cmpl	$1, %ebx
	je	.L2869
	movl	$2147483647, %r13d
.L2870:
	movq	8(%r14), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator21VisitForRegisterValueEPNS0_10ExpressionE
	movq	16(%r14), %rcx
	movzbl	4(%rcx), %edx
	andl	$63, %edx
	cmpb	$41, %dl
	jne	.L2920
	movq	8(%r14), %xmm0
	movl	$1, (%r15)
	movq	$0, 8(%r15)
	movhps	8(%rcx), %xmm0
	movq	$2147483647, 16(%r15)
	movl	%eax, 24(%r15)
	movl	$2147483647, 28(%r15)
	movups	%xmm0, 32(%r15)
	cmpl	$2147483647, %r13d
	je	.L2853
	leaq	24(%r12), %rdi
	movl	%r13d, %esi
	movl	%r13d, -88(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	jmp	.L2853
	.p2align 4,,10
	.p2align 3
.L2864:
	cmpb	$49, %r13b
	je	.L2923
.L2863:
	cmpl	$1, %ebx
	je	.L2924
	movq	8(%r14), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator21VisitForRegisterValueEPNS0_10ExpressionE
	movq	16(%r14), %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal11interpreter17BytecodeGenerator21VisitForRegisterValueEPNS0_10ExpressionE
	movl	$2, (%r15)
	pxor	%xmm0, %xmm0
	movq	$0, 8(%r15)
	movq	$2147483647, 16(%r15)
	movl	%ebx, 24(%r15)
	movl	%eax, 28(%r15)
	movups	%xmm0, 32(%r15)
	.p2align 4,,10
	.p2align 3
.L2853:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2925
	addq	$88, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2858:
	.cfi_restore_state
	movl	$8, %eax
.L2861:
	movl	%eax, (%r15)
.L2919:
	movdqa	.LC4(%rip), %xmm0
	movq	%r14, 8(%r15)
	movups	%xmm0, 16(%r15)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 32(%r15)
	jmp	.L2853
	.p2align 4,,10
	.p2align 3
.L2923:
	leaq	24(%r12), %rax
	movq	%rax, -120(%rbp)
	cmpl	$1, %ebx
	je	.L2867
	movl	$2147483647, -124(%rbp)
.L2868:
	movl	328(%r12), %r13d
	movq	336(%r12), %rdi
	leal	4(%r13), %eax
	cmpl	%eax, 332(%r12)
	movl	%eax, 328(%r12)
	cmovge	332(%r12), %eax
	movl	%eax, 332(%r12)
	testq	%rdi, %rdi
	je	.L2883
	movq	(%rdi), %rax
	movl	%r13d, %esi
	btsq	$34, %rsi
	call	*24(%rax)
.L2883:
	movq	8(%r14), %rbx
	movq	%r12, %rdi
	movzbl	4(%rbx), %eax
	andl	$63, %eax
	cmpb	$49, %al
	jne	.L2884
	call	_ZN2v88internal11interpreter17BytecodeGenerator21BuildThisVariableLoadEv
	movl	%r13d, %esi
	leaq	24(%r12), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	8(%rbx), %rsi
	leaq	-96(%rbp), %rbx
	movq	%r12, %rdi
	movq	816(%r12), %rax
	leal	1(%r13), %r10d
	movq	%rbx, 816(%r12)
	movl	%r10d, -128(%rbp)
	movq	%rax, -96(%rbp)
	movl	328(%r12), %eax
	movq	%r12, -88(%rbp)
	movl	%eax, -80(%rbp)
	movq	$2, -72(%rbp)
	call	_ZN2v88internal11interpreter17BytecodeGenerator5VisitEPNS0_7AstNodeE
	movl	-128(%rbp), %r10d
	leaq	24(%r12), %rdi
	movl	%r10d, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rax
	movl	328(%rdx), %esi
	movq	%rax, 816(%rdx)
	movq	336(%rdx), %rdi
	movl	-80(%rbp), %eax
	movl	%eax, 328(%rdx)
	subl	%eax, %esi
	testq	%rdi, %rdi
	je	.L2885
	movq	%rsi, %rdx
	movq	(%rdi), %rcx
	movl	%eax, %esi
	salq	$32, %rdx
	orq	%rdx, %rsi
	call	*32(%rcx)
.L2885:
	movq	816(%r12), %rax
	movq	16(%r14), %rsi
	movq	%r12, %rdi
	movq	%r12, -88(%rbp)
	movq	%rbx, 816(%r12)
	leal	2(%r13), %r14d
	movq	%rax, -96(%rbp)
	movl	328(%r12), %eax
	movq	$2, -72(%rbp)
	movl	%eax, -80(%rbp)
	call	_ZN2v88internal11interpreter17BytecodeGenerator5VisitEPNS0_7AstNodeE
	movq	-120(%rbp), %rdi
	movl	%r14d, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rax
	movq	336(%rdx), %rdi
	movq	%rax, 816(%rdx)
	movl	-80(%rbp), %eax
	movl	328(%rdx), %esi
	movl	%eax, 328(%rdx)
	testq	%rdi, %rdi
	je	.L2886
	subl	%eax, %esi
	movq	(%rdi), %rcx
	movq	%rsi, %rdx
	movl	%eax, %esi
	salq	$32, %rdx
	orq	%rdx, %rsi
	call	*32(%rcx)
.L2886:
	movabsq	$9223372034707292159, %rax
	pxor	%xmm0, %xmm0
	movl	%r13d, 16(%r15)
	movq	%rax, 24(%r15)
	movl	-124(%rbp), %eax
	movl	$4, (%r15)
	movq	$0, 8(%r15)
	movl	$4, 20(%r15)
	movups	%xmm0, 32(%r15)
	cmpl	$2147483647, %eax
	je	.L2853
	movq	-120(%rbp), %rdi
	movl	%eax, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	jmp	.L2853
	.p2align 4,,10
	.p2align 3
.L2854:
	movl	$0, (%r15)
	jmp	.L2919
	.p2align 4,,10
	.p2align 3
.L2922:
	leaq	24(%r12), %rax
	movq	%rax, -120(%rbp)
	cmpl	$1, %ebx
	je	.L2871
	movl	$2147483647, -124(%rbp)
.L2872:
	movl	328(%r12), %r13d
	movq	336(%r12), %rdi
	leal	4(%r13), %eax
	cmpl	%eax, 332(%r12)
	movl	%eax, 328(%r12)
	cmovge	332(%r12), %eax
	movl	%eax, 332(%r12)
	testq	%rdi, %rdi
	je	.L2878
	movq	(%rdi), %rax
	movl	%r13d, %esi
	btsq	$34, %rsi
	call	*24(%rax)
.L2878:
	movq	8(%r14), %rbx
	movq	528(%r12), %rdi
	movzbl	4(%rbx), %eax
	andl	$63, %eax
	cmpb	$49, %al
	jne	.L2879
	call	_ZN2v88internal5Scope16GetReceiverScopeEv@PLT
	movq	%r12, %rdi
	movzbl	133(%rax), %ecx
	movq	176(%rax), %rsi
	leal	-4(%rcx), %edx
	movl	$1, %ecx
	cmpb	$1, %dl
	seta	%dl
	movzbl	%dl, %edx
	call	_ZN2v88internal11interpreter17BytecodeGenerator17BuildVariableLoadEPNS0_8VariableENS0_13HoleCheckModeENS0_10TypeofModeE
	movl	%r13d, %esi
	leaq	24(%r12), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	816(%r12), %rax
	movq	8(%rbx), %rsi
	movq	%r12, %rdi
	movq	%r12, -88(%rbp)
	leal	1(%r13), %ebx
	movq	%rax, -96(%rbp)
	movl	328(%r12), %eax
	movq	$2, -72(%rbp)
	movl	%eax, -80(%rbp)
	leaq	-96(%rbp), %rax
	movq	%rax, 816(%r12)
	call	_ZN2v88internal11interpreter17BytecodeGenerator5VisitEPNS0_7AstNodeE
	movq	-120(%rbp), %rdi
	movl	%ebx, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rax
	movl	328(%rdx), %esi
	movq	%rax, 816(%rdx)
	movq	336(%rdx), %rdi
	movl	-80(%rbp), %eax
	movl	%eax, 328(%rdx)
	subl	%eax, %esi
	testq	%rdi, %rdi
	je	.L2880
	movq	%rsi, %rdx
	movq	(%rdi), %rcx
	movl	%eax, %esi
	salq	$32, %rdx
	orq	%rdx, %rsi
	call	*32(%rcx)
.L2880:
	movq	16(%r14), %rdx
	movzbl	4(%rdx), %eax
	andl	$63, %eax
	cmpb	$41, %al
	jne	.L2920
	movq	-120(%rbp), %rbx
	movq	8(%rdx), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralEPKNS0_12AstRawStringE@PLT
	leal	2(%r13), %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, (%r15)
	movabsq	$9223372034707292159, %rax
	movq	%rax, 24(%r15)
	movl	-124(%rbp), %eax
	movq	$0, 8(%r15)
	movl	%r13d, 16(%r15)
	movl	$4, 20(%r15)
	movups	%xmm0, 32(%r15)
	cmpl	$2147483647, %eax
	je	.L2853
	movl	%eax, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	jmp	.L2853
	.p2align 4,,10
	.p2align 3
.L2924:
	leaq	24(%r12), %r8
	leaq	328(%r12), %rdi
	movq	%r8, -120(%rbp)
	call	_ZN2v88internal11interpreter25BytecodeRegisterAllocator11NewRegisterEv
	movq	-120(%rbp), %r8
	movl	%eax, %esi
	movl	%eax, -88(%rbp)
	movl	%eax, %r13d
	movq	%r8, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	8(%r14), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator21VisitForRegisterValueEPNS0_10ExpressionE
	movq	16(%r14), %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal11interpreter17BytecodeGenerator21VisitForRegisterValueEPNS0_10ExpressionE
	pxor	%xmm0, %xmm0
	movl	$2, (%r15)
	movq	$0, 8(%r15)
	movq	$2147483647, 16(%r15)
	movl	%ebx, 24(%r15)
	movl	%eax, 28(%r15)
	movups	%xmm0, 32(%r15)
	cmpl	$2147483647, %r13d
	je	.L2853
	movq	-120(%rbp), %r8
	movl	%r13d, %esi
	movl	%r13d, -88(%rbp)
	movq	%r8, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	jmp	.L2853
.L2857:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2862:
	movl	$5, %eax
	jmp	.L2861
	.p2align 4,,10
	.p2align 3
.L2889:
	movl	$7, %eax
	jmp	.L2861
	.p2align 4,,10
	.p2align 3
.L2860:
	movl	$6, %eax
	jmp	.L2861
	.p2align 4,,10
	.p2align 3
.L2869:
	leaq	328(%r12), %rdi
	leaq	24(%r12), %rbx
	call	_ZN2v88internal11interpreter25BytecodeRegisterAllocator11NewRegisterEv
	movq	%rbx, %rdi
	movl	%eax, %esi
	movl	%eax, -88(%rbp)
	movl	%eax, %r13d
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	jmp	.L2870
	.p2align 4,,10
	.p2align 3
.L2871:
	leaq	328(%r12), %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterAllocator11NewRegisterEv
	leaq	24(%r12), %rdi
	movl	%eax, %esi
	movl	%eax, -104(%rbp)
	movl	%eax, -124(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	jmp	.L2872
	.p2align 4,,10
	.p2align 3
.L2867:
	leaq	328(%r12), %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterAllocator11NewRegisterEv
	leaq	24(%r12), %rdi
	movl	%eax, %esi
	movl	%eax, -104(%rbp)
	movl	%eax, -124(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	jmp	.L2868
.L2925:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal11interpreter17BytecodeGenerator20PrepareAssignmentLhsEPNS0_10ExpressionENS2_25AccumulatorPreservingModeE
	.cfi_startproc
	.type	_ZN2v88internal11interpreter17BytecodeGenerator20PrepareAssignmentLhsEPNS0_10ExpressionENS2_25AccumulatorPreservingModeE.cold, @function
_ZN2v88internal11interpreter17BytecodeGenerator20PrepareAssignmentLhsEPNS0_10ExpressionENS2_25AccumulatorPreservingModeE.cold:
.LFSB20909:
.L2879:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	_ZN2v88internal5Scope16GetReceiverScopeEv@PLT
	movl	$1, %ecx
	movq	%r12, %rdi
	movzbl	133(%rax), %ebx
	movq	176(%rax), %rsi
	leal	-4(%rbx), %edx
	cmpb	$1, %dl
	seta	%dl
	movzbl	%dl, %edx
	call	_ZN2v88internal11interpreter17BytecodeGenerator17BuildVariableLoadEPNS0_8VariableENS0_13HoleCheckModeENS0_10TypeofModeE
.L2921:
	movq	-120(%rbp), %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
.L2920:
	movq	8, %rax
	ud2
.L2884:
	call	_ZN2v88internal11interpreter17BytecodeGenerator21BuildThisVariableLoadEv
	jmp	.L2921
	.cfi_endproc
.LFE20909:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator20PrepareAssignmentLhsEPNS0_10ExpressionENS2_25AccumulatorPreservingModeE
	.size	_ZN2v88internal11interpreter17BytecodeGenerator20PrepareAssignmentLhsEPNS0_10ExpressionENS2_25AccumulatorPreservingModeE, .-_ZN2v88internal11interpreter17BytecodeGenerator20PrepareAssignmentLhsEPNS0_10ExpressionENS2_25AccumulatorPreservingModeE
	.section	.text.unlikely._ZN2v88internal11interpreter17BytecodeGenerator20PrepareAssignmentLhsEPNS0_10ExpressionENS2_25AccumulatorPreservingModeE
	.size	_ZN2v88internal11interpreter17BytecodeGenerator20PrepareAssignmentLhsEPNS0_10ExpressionENS2_25AccumulatorPreservingModeE.cold, .-_ZN2v88internal11interpreter17BytecodeGenerator20PrepareAssignmentLhsEPNS0_10ExpressionENS2_25AccumulatorPreservingModeE.cold
.LCOLDE15:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator20PrepareAssignmentLhsEPNS0_10ExpressionENS2_25AccumulatorPreservingModeE
.LHOTE15:
	.section	.text.unlikely._ZN2v88internal11interpreter17BytecodeGenerator20PrepareAssignmentLhsEPNS0_10ExpressionENS2_25AccumulatorPreservingModeE.constprop.0,"ax",@progbits
	.align 2
.LCOLDB16:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator20PrepareAssignmentLhsEPNS0_10ExpressionENS2_25AccumulatorPreservingModeE.constprop.0,"ax",@progbits
.LHOTB16:
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter17BytecodeGenerator20PrepareAssignmentLhsEPNS0_10ExpressionENS2_25AccumulatorPreservingModeE.constprop.0, @function
_ZN2v88internal11interpreter17BytecodeGenerator20PrepareAssignmentLhsEPNS0_10ExpressionENS2_25AccumulatorPreservingModeE.constprop.0:
.LFB28935:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	4(%rdx), %eax
	andl	$63, %eax
	cmpb	$44, %al
	jne	.L2927
	testq	%rdx, %rdx
	je	.L2927
	movq	16(%rdx), %rdi
	movq	%rsi, %r14
	call	_ZNK2v88internal10Expression13IsPrivateNameEv@PLT
	testb	%al, %al
	je	.L2928
	movq	16(%rbx), %rdx
	movzbl	4(%rdx), %eax
	andl	$63, %eax
	cmpb	$54, %al
	jne	.L2969
	movq	8(%rdx), %rax
	movzwl	40(%rax), %eax
	andl	$15, %eax
	cmpb	$10, %al
	ja	.L2930
	leaq	.L2932(%rip), %rdx
	movzbl	%al, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal11interpreter17BytecodeGenerator20PrepareAssignmentLhsEPNS0_10ExpressionENS2_25AccumulatorPreservingModeE.constprop.0,"a",@progbits
	.align 4
	.align 4
.L2932:
	.long	.L2930-.L2932
	.long	.L2936-.L2932
	.long	.L2930-.L2932
	.long	.L2930-.L2932
	.long	.L2930-.L2932
	.long	.L2930-.L2932
	.long	.L2930-.L2932
	.long	.L2935-.L2932
	.long	.L2954-.L2932
	.long	.L2933-.L2932
	.long	.L2931-.L2932
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator20PrepareAssignmentLhsEPNS0_10ExpressionENS2_25AccumulatorPreservingModeE.constprop.0
	.p2align 4,,10
	.p2align 3
.L2928:
	movq	8(%rbx), %rax
	movq	16(%rbx), %rdi
	movl	4(%rax), %r13d
	andl	$63, %r13d
	call	_ZNK2v88internal10Expression14IsPropertyNameEv@PLT
	testb	%al, %al
	jne	.L2971
	cmpb	$49, %r13b
	je	.L2972
.L2936:
	movq	8(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator21VisitForRegisterValueEPNS0_10ExpressionE
	movq	16(%rbx), %rsi
	movq	%r14, %rdi
	movl	%eax, %r13d
	call	_ZN2v88internal11interpreter17BytecodeGenerator21VisitForRegisterValueEPNS0_10ExpressionE
	movl	%r13d, 24(%r12)
	pxor	%xmm0, %xmm0
	movl	$2, (%r12)
	movq	$0, 8(%r12)
	movq	$2147483647, 16(%r12)
	movl	%eax, 28(%r12)
	movups	%xmm0, 32(%r12)
	.p2align 4,,10
	.p2align 3
.L2926:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2973
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2971:
	.cfi_restore_state
	cmpb	$49, %r13b
	je	.L2974
	movq	8(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator21VisitForRegisterValueEPNS0_10ExpressionE
	movq	16(%rbx), %rcx
	movzbl	4(%rcx), %edx
	andl	$63, %edx
	cmpb	$41, %dl
	jne	.L2975
	movq	8(%rbx), %xmm0
	movl	%eax, 24(%r12)
	movl	$1, (%r12)
	movq	$0, 8(%r12)
	movhps	8(%rcx), %xmm0
	movq	$2147483647, 16(%r12)
	movl	$2147483647, 28(%r12)
	movups	%xmm0, 32(%r12)
	jmp	.L2926
	.p2align 4,,10
	.p2align 3
.L2931:
	movl	$8, %eax
.L2934:
	movl	%eax, (%r12)
.L2968:
	movdqa	.LC4(%rip), %xmm0
	movq	%rbx, 8(%r12)
	movups	%xmm0, 16(%r12)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 32(%r12)
	jmp	.L2926
	.p2align 4,,10
	.p2align 3
.L2972:
	movl	328(%r14), %r13d
	movq	336(%r14), %rdi
	leal	4(%r13), %eax
	cmpl	%eax, 332(%r14)
	movl	%eax, 328(%r14)
	cmovge	332(%r14), %eax
	movl	%eax, 332(%r14)
	testq	%rdi, %rdi
	je	.L2941
	movq	(%rdi), %rax
	movl	%r13d, %esi
	btsq	$34, %rsi
	call	*24(%rax)
.L2941:
	movq	8(%rbx), %r15
	leaq	24(%r14), %rax
	movq	%r14, %rdi
	movq	%rax, -104(%rbp)
	movzbl	4(%r15), %eax
	andl	$63, %eax
	cmpb	$49, %al
	jne	.L2950
	call	_ZN2v88internal11interpreter17BytecodeGenerator21BuildThisVariableLoadEv
	movl	%r13d, %esi
	leaq	24(%r14), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	8(%r15), %rsi
	leaq	-96(%rbp), %r15
	movq	%r14, %rdi
	movq	816(%r14), %rax
	leal	1(%r13), %r9d
	movq	%r15, 816(%r14)
	movl	%r9d, -108(%rbp)
	movq	%rax, -96(%rbp)
	movl	328(%r14), %eax
	movq	%r14, -88(%rbp)
	movl	%eax, -80(%rbp)
	movq	$2, -72(%rbp)
	call	_ZN2v88internal11interpreter17BytecodeGenerator5VisitEPNS0_7AstNodeE
	movl	-108(%rbp), %r9d
	leaq	24(%r14), %rdi
	movl	%r9d, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rax
	movl	328(%rdx), %esi
	movq	%rax, 816(%rdx)
	movq	336(%rdx), %rdi
	movl	-80(%rbp), %eax
	movl	%eax, 328(%rdx)
	subl	%eax, %esi
	testq	%rdi, %rdi
	je	.L2951
	movq	%rsi, %rdx
	movq	(%rdi), %rcx
	movl	%eax, %esi
	salq	$32, %rdx
	orq	%rdx, %rsi
	call	*32(%rcx)
.L2951:
	movq	816(%r14), %rax
	movq	16(%rbx), %rsi
	movq	%r14, %rdi
	movq	%r14, -88(%rbp)
	movq	%r15, 816(%r14)
	leal	2(%r13), %ebx
	movq	%rax, -96(%rbp)
	movl	328(%r14), %eax
	movq	$2, -72(%rbp)
	movl	%eax, -80(%rbp)
	call	_ZN2v88internal11interpreter17BytecodeGenerator5VisitEPNS0_7AstNodeE
	movq	-104(%rbp), %rdi
	movl	%ebx, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rax
	movq	336(%rdx), %rdi
	movq	%rax, 816(%rdx)
	movl	-80(%rbp), %eax
	movl	328(%rdx), %esi
	movl	%eax, 328(%rdx)
	testq	%rdi, %rdi
	je	.L2952
	subl	%eax, %esi
	movq	(%rdi), %rcx
	movq	%rsi, %rdx
	movl	%eax, %esi
	salq	$32, %rdx
	orq	%rdx, %rsi
	call	*32(%rcx)
.L2952:
	movl	$4, (%r12)
.L2967:
	movabsq	$9223372034707292159, %rax
	pxor	%xmm0, %xmm0
	movl	%r13d, 16(%r12)
	movq	$0, 8(%r12)
	movl	$4, 20(%r12)
	movq	%rax, 24(%r12)
	movups	%xmm0, 32(%r12)
	jmp	.L2926
	.p2align 4,,10
	.p2align 3
.L2927:
	movl	$0, (%r12)
	jmp	.L2968
	.p2align 4,,10
	.p2align 3
.L2974:
	movl	328(%r14), %r13d
	movq	336(%r14), %rdi
	leal	4(%r13), %eax
	cmpl	%eax, 332(%r14)
	movl	%eax, 328(%r14)
	cmovge	332(%r14), %eax
	movl	%eax, 332(%r14)
	testq	%rdi, %rdi
	je	.L2945
	movq	(%rdi), %rax
	movl	%r13d, %esi
	btsq	$34, %rsi
	call	*24(%rax)
.L2945:
	movq	8(%rbx), %r15
	leaq	24(%r14), %rax
	movq	528(%r14), %rdi
	movq	%rax, -104(%rbp)
	movzbl	4(%r15), %eax
	andl	$63, %eax
	cmpb	$49, %al
	jne	.L2947
	call	_ZN2v88internal5Scope16GetReceiverScopeEv@PLT
	movq	%r14, %rdi
	movzbl	133(%rax), %ecx
	movq	176(%rax), %rsi
	leal	-4(%rcx), %edx
	movl	$1, %ecx
	cmpb	$1, %dl
	seta	%dl
	movzbl	%dl, %edx
	call	_ZN2v88internal11interpreter17BytecodeGenerator17BuildVariableLoadEPNS0_8VariableENS0_13HoleCheckModeENS0_10TypeofModeE
	movl	%r13d, %esi
	leaq	24(%r14), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	816(%r14), %rax
	movq	8(%r15), %rsi
	movq	%r14, %rdi
	movq	%r14, -88(%rbp)
	leal	1(%r13), %r15d
	movq	%rax, -96(%rbp)
	movl	328(%r14), %eax
	movq	$2, -72(%rbp)
	movl	%eax, -80(%rbp)
	leaq	-96(%rbp), %rax
	movq	%rax, 816(%r14)
	call	_ZN2v88internal11interpreter17BytecodeGenerator5VisitEPNS0_7AstNodeE
	movq	-104(%rbp), %rdi
	movl	%r15d, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rax
	movl	328(%rdx), %esi
	movq	%rax, 816(%rdx)
	movq	336(%rdx), %rdi
	movl	-80(%rbp), %eax
	movl	%eax, 328(%rdx)
	subl	%eax, %esi
	testq	%rdi, %rdi
	je	.L2948
	movq	%rsi, %rdx
	movq	(%rdi), %rcx
	movl	%eax, %esi
	salq	$32, %rdx
	orq	%rdx, %rsi
	call	*32(%rcx)
.L2948:
	movq	16(%rbx), %rdx
	movzbl	4(%rdx), %eax
	andl	$63, %eax
	cmpb	$41, %al
	jne	.L2969
	movq	8(%rdx), %rsi
	movq	-104(%rbp), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralEPKNS0_12AstRawStringE@PLT
	leal	2(%r13), %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movl	$3, (%r12)
	jmp	.L2967
.L2930:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2935:
	movl	$5, %eax
	jmp	.L2934
	.p2align 4,,10
	.p2align 3
.L2954:
	movl	$7, %eax
	jmp	.L2934
	.p2align 4,,10
	.p2align 3
.L2933:
	movl	$6, %eax
	jmp	.L2934
.L2973:
	call	__stack_chk_fail@PLT
.L2975:
	jmp	.L2969
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal11interpreter17BytecodeGenerator20PrepareAssignmentLhsEPNS0_10ExpressionENS2_25AccumulatorPreservingModeE.constprop.0
	.cfi_startproc
	.type	_ZN2v88internal11interpreter17BytecodeGenerator20PrepareAssignmentLhsEPNS0_10ExpressionENS2_25AccumulatorPreservingModeE.constprop.0.cold, @function
_ZN2v88internal11interpreter17BytecodeGenerator20PrepareAssignmentLhsEPNS0_10ExpressionENS2_25AccumulatorPreservingModeE.constprop.0.cold:
.LFSB28935:
.L2947:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	_ZN2v88internal5Scope16GetReceiverScopeEv@PLT
	movq	%r14, %rdi
	movzbl	133(%rax), %ecx
	movq	176(%rax), %rsi
	leal	-4(%rcx), %edx
	movl	$1, %ecx
	cmpb	$1, %dl
	seta	%dl
	movzbl	%dl, %edx
	call	_ZN2v88internal11interpreter17BytecodeGenerator17BuildVariableLoadEPNS0_8VariableENS0_13HoleCheckModeENS0_10TypeofModeE
.L2970:
	movq	-104(%rbp), %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
.L2969:
	movq	8, %rax
	ud2
.L2950:
	call	_ZN2v88internal11interpreter17BytecodeGenerator21BuildThisVariableLoadEv
	jmp	.L2970
	.cfi_endproc
.LFE28935:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator20PrepareAssignmentLhsEPNS0_10ExpressionENS2_25AccumulatorPreservingModeE.constprop.0
	.size	_ZN2v88internal11interpreter17BytecodeGenerator20PrepareAssignmentLhsEPNS0_10ExpressionENS2_25AccumulatorPreservingModeE.constprop.0, .-_ZN2v88internal11interpreter17BytecodeGenerator20PrepareAssignmentLhsEPNS0_10ExpressionENS2_25AccumulatorPreservingModeE.constprop.0
	.section	.text.unlikely._ZN2v88internal11interpreter17BytecodeGenerator20PrepareAssignmentLhsEPNS0_10ExpressionENS2_25AccumulatorPreservingModeE.constprop.0
	.size	_ZN2v88internal11interpreter17BytecodeGenerator20PrepareAssignmentLhsEPNS0_10ExpressionENS2_25AccumulatorPreservingModeE.constprop.0.cold, .-_ZN2v88internal11interpreter17BytecodeGenerator20PrepareAssignmentLhsEPNS0_10ExpressionENS2_25AccumulatorPreservingModeE.constprop.0.cold
.LCOLDE16:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator20PrepareAssignmentLhsEPNS0_10ExpressionENS2_25AccumulatorPreservingModeE.constprop.0
.LHOTE16:
	.section	.rodata._ZN2v88internal11interpreter17BytecodeGenerator20VisitSwitchStatementEPNS0_15SwitchStatementE.str1.8,"aMS",@progbits,1
	.align 8
.LC17:
	.string	"vector::_M_range_check: __n (which is %zu) >= this->size() (which is %zu)"
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator20VisitSwitchStatementEPNS0_15SwitchStatementE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator20VisitSwitchStatementEPNS0_15SwitchStatementE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator20VisitSwitchStatementEPNS0_15SwitchStatementE, @function
_ZN2v88internal11interpreter17BytecodeGenerator20VisitSwitchStatementEPNS0_15SwitchStatementE:
.LFB20765:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	leaq	-136(%rbp), %rdx
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	24(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$184, %rsp
	movq	568(%rdi), %rcx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	movq	%rsi, -104(%rbp)
	movslq	36(%rsi), %rsi
	movq	%rcx, -96(%rbp)
	leaq	16+_ZTVN2v88internal11interpreter13SwitchBuilderE(%rip), %rcx
	movq	%r13, -152(%rbp)
	movq	%rax, -144(%rbp)
	movq	%rdx, -128(%rbp)
	movq	%rdx, -136(%rbp)
	movq	$0, -120(%rbp)
	movb	$0, -112(%rbp)
	movq	%rcx, -160(%rbp)
	movq	%rax, -88(%rbp)
	movq	$0, -80(%rbp)
	movups	%xmm0, -72(%rbp)
	testq	%rsi, %rsi
	jne	.L3007
.L2977:
	movq	800(%rbx), %rax
	leaq	-160(%rbp), %r12
	movq	%rbx, -200(%rbp)
	movq	%r15, -176(%rbp)
	movq	%rax, -192(%rbp)
	movq	808(%rbx), %rax
	movq	%r12, -168(%rbp)
	movq	%rax, -184(%rbp)
	leaq	-208(%rbp), %rax
	movq	%rax, 800(%rbx)
	leaq	16+_ZTVN2v88internal11interpreter17BytecodeGenerator24ControlScopeForBreakableE(%rip), %rax
	movq	%rax, -208(%rbp)
	movl	(%r15), %eax
	cmpl	$-1, %eax
	je	.L2978
	movb	$2, 496(%rbx)
	movl	%eax, 500(%rbx)
.L2978:
	movq	16(%r15), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator21VisitForRegisterValueEPNS0_10ExpressionE
	movl	36(%r15), %edx
	movl	%eax, -212(%rbp)
	testl	%edx, %edx
	jg	.L3008
.L2985:
	leaq	-144(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter27BreakableControlFlowBuilder8EmitJumpEPNS1_14BytecodeLabelsE@PLT
.L2984:
	movl	36(%r15), %eax
	xorl	%r13d, %r13d
	testl	%eax, %eax
	jle	.L2988
	.p2align 4,,10
	.p2align 3
.L2987:
	movq	24(%r15), %rax
	movl	%r13d, %esi
	movq	%r12, %rdi
	movq	(%rax,%r13,8), %r14
	addq	$1, %r13
	movq	%r14, %rdx
	call	_ZN2v88internal11interpreter13SwitchBuilder13SetCaseTargetEiPNS0_10CaseClauseE@PLT
	leaq	8(%r14), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator15VisitStatementsEPKNS0_8ZoneListIPNS0_9StatementEEE
	cmpl	%r13d, 36(%r15)
	jg	.L2987
.L2988:
	movq	-200(%rbp), %rax
	movq	-192(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rdx, 800(%rax)
	call	_ZN2v88internal11interpreter13SwitchBuilderD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3009
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3008:
	.cfi_restore_state
	movq	512(%rbx), %rax
	movl	$16, %esi
	leaq	56(%rax), %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movl	36(%r15), %edx
	movl	%eax, -216(%rbp)
	testl	%edx, %edx
	jle	.L2985
	movl	$-1, -220(%rbp)
	xorl	%r14d, %r14d
	jmp	.L2982
	.p2align 4,,10
	.p2align 3
.L3012:
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	movl	-212(%rbp), %edx
	movl	-216(%rbp), %ecx
	movq	%r13, %rdi
	movl	$54, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder16CompareOperationENS0_5Token5ValueENS1_8RegisterEi@PLT
	movq	-80(%rbp), %rax
	movq	-72(%rbp), %rdx
	movq	-152(%rbp), %rdi
	subq	%rax, %rdx
	sarq	$4, %rdx
	cmpq	%r14, %rdx
	jbe	.L3010
	movq	%r14, %rdx
	movl	$1, %esi
	addq	$1, %r14
	salq	$4, %rdx
	addq	%rax, %rdx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder10JumpIfTrueENS2_13ToBooleanModeEPNS1_13BytecodeLabelE@PLT
	movl	36(%r15), %edx
	cmpl	%r14d, %edx
	jle	.L3011
.L2982:
	movq	24(%r15), %rax
	movq	(%rax,%r14,8), %rax
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	jne	.L3012
	movl	%r14d, -220(%rbp)
	addq	$1, %r14
	cmpl	%r14d, %edx
	jg	.L2982
.L3011:
	movl	-220(%rbp), %eax
	cmpl	$-1, %eax
	je	.L2985
	movq	-72(%rbp), %rdx
	movslq	%eax, %rsi
	movq	-80(%rbp), %rax
	movq	-152(%rbp), %rdi
	subq	%rax, %rdx
	sarq	$4, %rdx
	cmpq	%rdx, %rsi
	jnb	.L3013
	salq	$4, %rsi
	addq	%rax, %rsi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4JumpEPNS1_13BytecodeLabelE@PLT
	jmp	.L2984
	.p2align 4,,10
	.p2align 3
.L3007:
	leaq	-88(%rbp), %rdi
	call	_ZNSt6vectorIN2v88internal11interpreter13BytecodeLabelENS1_13ZoneAllocatorIS3_EEE17_M_default_appendEm
	jmp	.L2977
.L3010:
	movq	%r14, %rsi
	leaq	.LC17(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L3013:
	leaq	.LC17(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L3009:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20765:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator20VisitSwitchStatementEPNS0_15SwitchStatementE, .-_ZN2v88internal11interpreter17BytecodeGenerator20VisitSwitchStatementEPNS0_15SwitchStatementE
	.section	.text.unlikely._ZN2v88internal11interpreter17BytecodeGenerator19VisitCountOperationEPNS0_14CountOperationE,"ax",@progbits
	.align 2
.LCOLDB18:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator19VisitCountOperationEPNS0_14CountOperationE,"ax",@progbits
.LHOTB18:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator19VisitCountOperationEPNS0_14CountOperationE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator19VisitCountOperationEPNS0_14CountOperationE, @function
_ZN2v88internal11interpreter17BytecodeGenerator19VisitCountOperationEPNS0_14CountOperationE:
.LFB20948:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$120, %rsp
	movq	8(%rsi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	4(%r15), %eax
	andl	$63, %eax
	cmpb	$44, %al
	jne	.L3063
	testq	%r15, %r15
	je	.L3063
	movq	16(%r15), %rdi
	call	_ZNK2v88internal10Expression13IsPrivateNameEv@PLT
	testb	%al, %al
	je	.L3016
	movq	16(%r15), %rdx
	movzbl	4(%rdx), %eax
	andl	$63, %eax
	cmpb	$54, %al
	jne	.L3087
	movq	8(%rdx), %rax
	movzwl	40(%rax), %eax
	andl	$15, %eax
	cmpb	$10, %al
	ja	.L3018
	leaq	.L3020(%rip), %rdx
	movzbl	%al, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal11interpreter17BytecodeGenerator19VisitCountOperationEPNS0_14CountOperationE,"a",@progbits
	.align 4
	.align 4
.L3020:
	.long	.L3018-.L3020
	.long	.L3023-.L3020
	.long	.L3018-.L3020
	.long	.L3018-.L3020
	.long	.L3018-.L3020
	.long	.L3018-.L3020
	.long	.L3018-.L3020
	.long	.L3064-.L3020
	.long	.L3022-.L3020
	.long	.L3021-.L3020
	.long	.L3019-.L3020
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator19VisitCountOperationEPNS0_14CountOperationE
	.p2align 4,,10
	.p2align 3
.L3016:
	movq	8(%r15), %rax
	movq	16(%r15), %rdi
	movl	4(%rax), %r12d
	call	_ZNK2v88internal10Expression14IsPropertyNameEv@PLT
	andl	$63, %r12d
	cmpb	$49, %r12b
	sete	%r12b
	movzbl	%r12b, %r12d
	testb	%al, %al
	je	.L3024
	leal	1(%r12,%r12), %r12d
	jmp	.L3015
	.p2align 4,,10
	.p2align 3
.L3063:
	xorl	%r15d, %r15d
	xorl	%r12d, %r12d
.L3015:
	movb	$0, -97(%rbp)
	testb	$-128, 4(%rbx)
	jne	.L3025
	movq	816(%r14), %rax
	cmpl	$1, 24(%rax)
	setne	-97(%rbp)
.L3025:
	movl	$2147483647, -104(%rbp)
	movl	$2147483647, -120(%rbp)
	movl	$2147483647, -116(%rbp)
	movq	$0, -112(%rbp)
	cmpl	$8, %r12d
	ja	.L3026
	leaq	.L3028(%rip), %rcx
	movl	%r12d, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal11interpreter17BytecodeGenerator19VisitCountOperationEPNS0_14CountOperationE
	.align 4
	.align 4
.L3028:
	.long	.L3026-.L3028
	.long	.L3035-.L3028
	.long	.L3034-.L3028
	.long	.L3033-.L3028
	.long	.L3032-.L3028
	.long	.L3031-.L3028
	.long	.L3030-.L3028
	.long	.L3029-.L3028
	.long	.L3027-.L3028
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator19VisitCountOperationEPNS0_14CountOperationE
	.p2align 4,,10
	.p2align 3
.L3024:
	leal	2(%r12,%r12), %r12d
	jmp	.L3015
	.p2align 4,,10
	.p2align 3
.L3027:
	movq	8(%r15), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator21VisitForRegisterValueEPNS0_10ExpressionE
	movq	16(%r15), %rsi
	movq	%r14, %rdi
	movl	%eax, %r13d
	movl	%eax, -104(%rbp)
	call	_ZN2v88internal11interpreter17BytecodeGenerator21VisitForRegisterValueEPNS0_10ExpressionE
	movl	%r13d, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movl	%eax, -120(%rbp)
	call	_ZN2v88internal11interpreter17BytecodeGenerator22BuildPrivateBrandCheckEPNS0_8PropertyENS1_8RegisterE
	movl	-120(%rbp), %edx
	movl	%r13d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator24BuildPrivateGetterAccessENS1_8RegisterES3_
.L3085:
	movl	$2147483647, -128(%rbp)
	leaq	24(%r14), %r13
.L3038:
	movq	512(%r14), %rax
	movl	$15, %esi
	leaq	56(%rax), %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	cmpb	$0, -97(%rbp)
	movl	%eax, %r8d
	jne	.L3088
.L3044:
	movl	4(%rbx), %esi
	movl	%r8d, %edx
	movq	%r13, %rdi
	shrl	$8, %esi
	andl	$127, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder14UnaryOperationENS0_5Token5ValueEi@PLT
	movl	(%rbx), %eax
	cmpl	$-1, %eax
	je	.L3046
	cmpb	$2, 496(%r14)
	je	.L3046
	movb	$1, 496(%r14)
	movl	%eax, 500(%r14)
.L3046:
	cmpl	$8, %r12d
	ja	.L3047
	leaq	.L3049(%rip), %rdx
	movslq	(%rdx,%r12,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal11interpreter17BytecodeGenerator19VisitCountOperationEPNS0_14CountOperationE
	.align 4
	.align 4
.L3049:
	.long	.L3047-.L3049
	.long	.L3053-.L3049
	.long	.L3052-.L3049
	.long	.L3051-.L3049
	.long	.L3050-.L3049
	.long	.L3018-.L3049
	.long	.L3018-.L3049
	.long	.L3018-.L3049
	.long	.L3048-.L3049
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator19VisitCountOperationEPNS0_14CountOperationE
	.p2align 4,,10
	.p2align 3
.L3030:
	movq	%r15, %rdx
	movl	$263, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator26BuildInvalidPropertyAccessENS0_15MessageTemplateEPNS0_8PropertyE
	.p2align 4,,10
	.p2align 3
.L3014:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3089
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3031:
	.cfi_restore_state
	movq	%r15, %rdx
	movl	$261, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator26BuildInvalidPropertyAccessENS0_15MessageTemplateEPNS0_8PropertyE
	jmp	.L3014
	.p2align 4,,10
	.p2align 3
.L3032:
	leaq	328(%r14), %rdi
	leaq	24(%r14), %r13
	movl	$4, %esi
	movq	%r13, -160(%rbp)
	call	_ZN2v88internal11interpreter25BytecodeRegisterAllocator15NewRegisterListEi
	movq	8(%r15), %rcx
	movq	%r14, %rdi
	movabsq	$12884901888, %rdx
	movl	%eax, -128(%rbp)
	movq	%rax, -112(%rbp)
	movl	-112(%rbp), %eax
	movq	%rcx, -136(%rbp)
	orq	%rax, %rdx
	movzbl	4(%rcx), %eax
	movq	%rdx, -152(%rbp)
	andl	$63, %eax
	cmpb	$49, %al
	jne	.L3040
	call	_ZN2v88internal11interpreter17BytecodeGenerator21BuildThisVariableLoadEv
	movl	-112(%rbp), %esi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-136(%rbp), %rcx
	leaq	-96(%rbp), %r8
	movq	%r14, %rdi
	movq	816(%r14), %rax
	movq	%r8, -136(%rbp)
	movq	8(%rcx), %rsi
	movl	-112(%rbp), %ecx
	movq	%r14, -88(%rbp)
	movq	%r8, 816(%r14)
	movq	%rax, -96(%rbp)
	movl	328(%r14), %eax
	leal	1(%rcx), %r10d
	movl	%r10d, -160(%rbp)
	movl	%eax, -80(%rbp)
	movq	$2, -72(%rbp)
	call	_ZN2v88internal11interpreter17BytecodeGenerator5VisitEPNS0_7AstNodeE
	movl	-160(%rbp), %r10d
	movq	%r13, %rdi
	movl	%r10d, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-136(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator21ExpressionResultScopeD2Ev
	movl	-112(%rbp), %eax
	movq	16(%r15), %rsi
	movq	%r14, %rdi
	movq	-136(%rbp), %r8
	movq	%r14, -88(%rbp)
	leal	2(%rax), %r10d
	movq	816(%r14), %rax
	movq	$2, -72(%rbp)
	movq	%r8, 816(%r14)
	movq	%rax, -96(%rbp)
	movl	328(%r14), %eax
	movl	%r10d, -160(%rbp)
	movl	%eax, -80(%rbp)
	call	_ZN2v88internal11interpreter17BytecodeGenerator5VisitEPNS0_7AstNodeE
	movl	-160(%rbp), %r10d
	movq	%r13, %rdi
	movl	%r10d, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-136(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator21ExpressionResultScopeD2Ev
	movq	-152(%rbp), %rdx
	movl	$37, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_12RegisterListE@PLT
	jmp	.L3038
	.p2align 4,,10
	.p2align 3
.L3033:
	leaq	328(%r14), %rdi
	leaq	24(%r14), %r13
	movl	$4, %esi
	movq	%r13, -160(%rbp)
	call	_ZN2v88internal11interpreter25BytecodeRegisterAllocator15NewRegisterListEi
	movq	%r14, %rdi
	movq	%rax, -112(%rbp)
	movl	-112(%rbp), %edx
	movl	%eax, -128(%rbp)
	movabsq	$12884901888, %rax
	orq	%rax, %rdx
	movq	%rdx, -136(%rbp)
	movq	8(%r15), %rdx
	movzbl	4(%rdx), %eax
	movq	%rdx, -152(%rbp)
	andl	$63, %eax
	cmpb	$49, %al
	jne	.L3040
	call	_ZN2v88internal11interpreter17BytecodeGenerator21BuildThisVariableLoadEv
	movl	-112(%rbp), %esi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movl	-112(%rbp), %eax
	leaq	-96(%rbp), %r8
	movq	%r14, %rdi
	movq	-152(%rbp), %rdx
	movq	%r8, -152(%rbp)
	leal	1(%rax), %r10d
	movq	816(%r14), %rax
	movq	8(%rdx), %rsi
	movq	%r8, 816(%r14)
	movq	%rax, -96(%rbp)
	movl	328(%r14), %eax
	movl	%r10d, -160(%rbp)
	movl	%eax, -80(%rbp)
	movq	%r14, -88(%rbp)
	movq	$2, -72(%rbp)
	call	_ZN2v88internal11interpreter17BytecodeGenerator5VisitEPNS0_7AstNodeE
	movl	-160(%rbp), %r10d
	movq	%r13, %rdi
	movl	%r10d, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-152(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator21ExpressionResultScopeD2Ev
	movq	16(%r15), %rdx
	movzbl	4(%rdx), %eax
	andl	$63, %eax
	cmpb	$41, %al
	jne	.L3087
	movq	8(%rdx), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralEPKNS0_12AstRawStringE@PLT
	movl	-128(%rbp), %ecx
	movq	%rax, %rdi
	leal	2(%rcx), %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-136(%rbp), %rdx
	movl	$36, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_12RegisterListE@PLT
	jmp	.L3038
	.p2align 4,,10
	.p2align 3
.L3034:
	movq	8(%r15), %rsi
	movq	%r14, %rdi
	leaq	24(%r14), %r13
	call	_ZN2v88internal11interpreter17BytecodeGenerator21VisitForRegisterValueEPNS0_10ExpressionE
	leaq	328(%r14), %rdi
	movl	%eax, -104(%rbp)
	call	_ZN2v88internal11interpreter25BytecodeRegisterAllocator11NewRegisterEv
	movq	16(%r15), %rsi
	movq	%r14, %rdi
	movl	%eax, -120(%rbp)
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	movl	-120(%rbp), %esi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	512(%r14), %rcx
	movl	$8, %esi
	movq	%rax, -128(%rbp)
	leaq	56(%rcx), %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movq	-128(%rbp), %r8
	movl	-104(%rbp), %esi
	movl	%eax, %edx
	movq	%r8, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder17LoadKeyedPropertyENS1_8RegisterEi@PLT
	movl	$2147483647, -128(%rbp)
	jmp	.L3038
	.p2align 4,,10
	.p2align 3
.L3035:
	movq	8(%r15), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator21VisitForRegisterValueEPNS0_10ExpressionE
	movq	16(%r15), %rdx
	movl	%eax, -104(%rbp)
	movzbl	4(%rdx), %eax
	andl	$63, %eax
	cmpb	$41, %al
	jne	.L3087
	movq	8(%rdx), %rcx
	movq	8(%r15), %rsi
	movq	%r14, %rdi
	leaq	24(%r14), %r13
	movq	%rcx, %rdx
	movq	%rcx, -144(%rbp)
	call	_ZN2v88internal11interpreter17BytecodeGenerator19GetCachedLoadICSlotEPKNS0_10ExpressionEPKNS0_12AstRawStringE
	movq	-144(%rbp), %rdx
	movl	-104(%rbp), %esi
	movq	%r13, %rdi
	movl	%eax, %ecx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder17LoadNamedPropertyENS1_8RegisterEPKNS0_12AstRawStringEi@PLT
	movl	$2147483647, -128(%rbp)
	jmp	.L3038
	.p2align 4,,10
	.p2align 3
.L3029:
	movq	%r15, %rdx
	movl	$262, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator26BuildInvalidPropertyAccessENS0_15MessageTemplateEPNS0_8PropertyE
	jmp	.L3014
	.p2align 4,,10
	.p2align 3
.L3048:
	leaq	328(%r14), %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterAllocator11NewRegisterEv
	movq	%r13, %rdi
	movl	%eax, %esi
	movl	%eax, %r12d
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movl	-120(%rbp), %edx
	movl	-104(%rbp), %esi
	movl	%r12d, %ecx
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator24BuildPrivateSetterAccessENS1_8RegisterES3_S3_
	movq	816(%r14), %rax
	cmpl	$1, 24(%rax)
	je	.L3055
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	.p2align 4,,10
	.p2align 3
.L3055:
	cmpb	$0, -97(%rbp)
	je	.L3014
	movl	-116(%rbp), %esi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	jmp	.L3014
	.p2align 4,,10
	.p2align 3
.L3050:
	movl	-128(%rbp), %ebx
	movq	%r13, %rdi
	leal	3(%rbx), %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movl	%ebx, %edx
	movl	$38, %esi
	movq	%rax, %rdi
	movabsq	$-4294967296, %rax
	andq	-112(%rbp), %rax
	orq	%rax, %rdx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_12RegisterListE@PLT
	jmp	.L3055
	.p2align 4,,10
	.p2align 3
.L3051:
	movl	-128(%rbp), %ebx
	movq	%r13, %rdi
	leal	3(%rbx), %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movl	$39, %esi
	movabsq	$-4294967296, %rdx
	andq	-112(%rbp), %rdx
	movq	%rax, %rdi
	movl	%ebx, %eax
	orq	%rax, %rdx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_12RegisterListE@PLT
	jmp	.L3055
	.p2align 4,,10
	.p2align 3
.L3052:
	movq	512(%r14), %rax
	movl	$2147483647, %r15d
	leaq	56(%rax), %rdi
	movq	536(%r14), %rax
	movzbl	129(%rax), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%esi, %esi
	andl	$-10, %esi
	addl	$13, %esi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movl	%eax, %r12d
	movq	816(%r14), %rax
	cmpl	$1, 24(%rax)
	je	.L3059
	leaq	328(%r14), %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterAllocator11NewRegisterEv
	movq	%r13, %rdi
	movl	%eax, %esi
	movl	%eax, %r15d
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
.L3059:
	movq	536(%r14), %rax
	movl	-120(%rbp), %edx
	movl	%r12d, %ecx
	movq	%r13, %rdi
	movl	-104(%rbp), %esi
	movzbl	129(%rax), %r8d
	andl	$1, %r8d
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder18StoreKeyedPropertyENS1_8RegisterES3_iNS0_12LanguageModeE@PLT
	movq	816(%r14), %rax
	cmpl	$1, 24(%rax)
	je	.L3055
.L3086:
	movl	%r15d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	jmp	.L3055
	.p2align 4,,10
	.p2align 3
.L3053:
	movq	8(%r15), %rsi
	movq	-144(%rbp), %rdx
	movq	%r14, %rdi
	movl	$2147483647, %r15d
	call	_ZN2v88internal11interpreter17BytecodeGenerator20GetCachedStoreICSlotEPKNS0_10ExpressionEPKNS0_12AstRawStringE
	movl	%eax, %r12d
	movq	816(%r14), %rax
	cmpl	$1, 24(%rax)
	je	.L3056
	leaq	328(%r14), %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterAllocator11NewRegisterEv
	movq	%r13, %rdi
	movl	%eax, %esi
	movl	%eax, %r15d
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
.L3056:
	movq	536(%r14), %rax
	movl	-104(%rbp), %esi
	movl	%r12d, %ecx
	movq	%r13, %rdi
	movq	-144(%rbp), %rdx
	movzbl	129(%rax), %r8d
	andl	$1, %r8d
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder18StoreNamedPropertyENS1_8RegisterEPKNS0_12AstRawStringEiNS0_12LanguageModeE@PLT
	movq	816(%r14), %rax
	cmpl	$1, 24(%rax)
	je	.L3055
	jmp	.L3086
	.p2align 4,,10
	.p2align 3
.L3019:
	movl	$8, %r12d
	jmp	.L3015
	.p2align 4,,10
	.p2align 3
.L3021:
	movl	$6, %r12d
	jmp	.L3015
	.p2align 4,,10
	.p2align 3
.L3022:
	movl	$7, %r12d
	jmp	.L3015
	.p2align 4,,10
	.p2align 3
.L3088:
	movl	328(%r14), %edx
	movq	336(%r14), %rdi
	leal	1(%rdx), %eax
	cmpl	%eax, 332(%r14)
	movl	%eax, 328(%r14)
	cmovge	332(%r14), %eax
	movl	%eax, 332(%r14)
	testq	%rdi, %rdi
	je	.L3045
	movq	(%rdi), %rax
	movl	%r8d, -136(%rbp)
	movl	%edx, %esi
	movl	%edx, -116(%rbp)
	call	*16(%rax)
	movl	-136(%rbp), %r8d
	movl	-116(%rbp), %edx
.L3045:
	movl	%r8d, %esi
	movq	%r13, %rdi
	movl	%r8d, -136(%rbp)
	movl	%edx, -116(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder9ToNumericEi@PLT
	movl	-116(%rbp), %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movl	-136(%rbp), %r8d
	jmp	.L3044
.L3026:
	movq	8(%rbx), %rax
	movl	4(%rax), %edx
	movl	%edx, %ecx
	andl	$63, %ecx
	cmpb	$54, %cl
	jne	.L3054
	movq	8(%rax), %rsi
	shrl	$11, %edx
	movq	%r14, %rdi
	movl	$1, %ecx
	movq	816(%r14), %rax
	andl	$1, %edx
	movq	%r14, -88(%rbp)
	movq	$2, -72(%rbp)
	movq	%rax, -96(%rbp)
	movl	328(%r14), %eax
	movl	%eax, -80(%rbp)
	leaq	-96(%rbp), %rax
	movq	%rax, 816(%r14)
	call	_ZN2v88internal11interpreter17BytecodeGenerator17BuildVariableLoadEPNS0_8VariableENS0_13HoleCheckModeENS0_10TypeofModeE
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rax
	movl	328(%rdx), %esi
	movq	%rax, 816(%rdx)
	movq	336(%rdx), %rdi
	movl	-80(%rbp), %eax
	movl	%eax, 328(%rdx)
	subl	%eax, %esi
	testq	%rdi, %rdi
	je	.L3085
	movq	%rsi, %rdx
	movq	(%rdi), %rcx
	movl	%eax, %esi
	salq	$32, %rdx
	orq	%rdx, %rsi
	call	*32(%rcx)
	jmp	.L3085
.L3047:
	movq	8(%rbx), %rax
	movl	4(%rax), %ecx
	movl	%ecx, %edx
	andl	$63, %edx
	cmpb	$54, %dl
	jne	.L3054
	movl	4(%rbx), %edx
	shrl	$11, %ecx
	movq	8(%rax), %rsi
	xorl	%r8d, %r8d
	andl	$1, %ecx
	movq	%r14, %rdi
	shrl	$8, %edx
	andl	$127, %edx
	call	_ZN2v88internal11interpreter17BytecodeGenerator23BuildVariableAssignmentEPNS0_8VariableENS0_5Token5ValueENS0_13HoleCheckModeENS0_18LookupHoistingModeE
	jmp	.L3055
.L3018:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3064:
	movl	$5, %r12d
	jmp	.L3015
	.p2align 4,,10
	.p2align 3
.L3023:
	movl	$2, %r12d
	jmp	.L3015
.L3089:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal11interpreter17BytecodeGenerator19VisitCountOperationEPNS0_14CountOperationE
	.cfi_startproc
	.type	_ZN2v88internal11interpreter17BytecodeGenerator19VisitCountOperationEPNS0_14CountOperationE.cold, @function
_ZN2v88internal11interpreter17BytecodeGenerator19VisitCountOperationEPNS0_14CountOperationE.cold:
.LFSB20948:
.L3040:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	_ZN2v88internal11interpreter17BytecodeGenerator21BuildThisVariableLoadEv
	movl	-112(%rbp), %esi
	movq	-160(%rbp), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
.L3087:
	movq	8, %rax
	ud2
.L3054:
	movl	4, %eax
	ud2
	.cfi_endproc
.LFE20948:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator19VisitCountOperationEPNS0_14CountOperationE
	.size	_ZN2v88internal11interpreter17BytecodeGenerator19VisitCountOperationEPNS0_14CountOperationE, .-_ZN2v88internal11interpreter17BytecodeGenerator19VisitCountOperationEPNS0_14CountOperationE
	.section	.text.unlikely._ZN2v88internal11interpreter17BytecodeGenerator19VisitCountOperationEPNS0_14CountOperationE
	.size	_ZN2v88internal11interpreter17BytecodeGenerator19VisitCountOperationEPNS0_14CountOperationE.cold, .-_ZN2v88internal11interpreter17BytecodeGenerator19VisitCountOperationEPNS0_14CountOperationE.cold
.LCOLDE18:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator19VisitCountOperationEPNS0_14CountOperationE
.LHOTE18:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator21VisitCompareOperationEPNS0_16CompareOperationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator21VisitCompareOperationEPNS0_16CompareOperationE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator21VisitCompareOperationEPNS0_16CompareOperationE, @function
_ZN2v88internal11interpreter17BytecodeGenerator21VisitCompareOperationEPNS0_16CompareOperationE:
.LFB20952:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-56(%rbp), %r13
	leaq	-48(%rbp), %rdx
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	movq	%r13, %rsi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal16CompareOperation22IsLiteralCompareTypeofEPPNS0_10ExpressionEPPNS0_7LiteralE@PLT
	testb	%al, %al
	je	.L3091
	movq	-56(%rbp), %rsi
	movq	%rbx, %rdi
	leaq	24(%rbx), %r13
	call	_ZN2v88internal11interpreter17BytecodeGenerator19VisitForTypeOfValueEPNS0_10ExpressionE
	movl	(%r12), %eax
	cmpl	$-1, %eax
	je	.L3092
	cmpb	$2, 496(%rbx)
	je	.L3092
	movb	$1, 496(%rbx)
	movl	%eax, 500(%rbx)
.L3092:
	movq	520(%rbx), %rdi
	movq	-48(%rbp), %rsi
	call	_ZN2v88internal11interpreter15TestTypeOfFlags17GetFlagForLiteralEPKNS0_18AstStringConstantsEPNS0_7LiteralE@PLT
	movq	%r13, %rdi
	movl	%eax, %esi
	cmpb	$8, %al
	je	.L3117
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder13CompareTypeOfENS1_15TestTypeOfFlags11LiteralFlagE@PLT
.L3094:
	movq	816(%rbx), %rax
	movl	$1, 28(%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3118
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3091:
	.cfi_restore_state
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal16CompareOperation25IsLiteralCompareUndefinedEPPNS0_10ExpressionE@PLT
	testb	%al, %al
	je	.L3095
	movq	-56(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	movl	(%r12), %eax
	cmpl	$-1, %eax
	je	.L3096
	cmpb	$2, 496(%rbx)
	je	.L3096
	movb	$1, 496(%rbx)
	movl	%eax, 500(%rbx)
.L3096:
	movl	4(%r12), %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	shrl	$7, %esi
	andl	$127, %esi
	call	_ZN2v88internal11interpreter17BytecodeGenerator22BuildLiteralCompareNilENS0_5Token5ValueENS1_20BytecodeArrayBuilder8NilValueE
	jmp	.L3094
	.p2align 4,,10
	.p2align 3
.L3095:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal16CompareOperation20IsLiteralCompareNullEPPNS0_10ExpressionE@PLT
	testb	%al, %al
	jne	.L3119
	movq	8(%r12), %rsi
	movq	%rbx, %rdi
	leaq	24(%rbx), %r14
	call	_ZN2v88internal11interpreter17BytecodeGenerator21VisitForRegisterValueEPNS0_10ExpressionE
	movq	16(%r12), %rsi
	movq	%rbx, %rdi
	movl	%eax, %r13d
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	movl	(%r12), %eax
	cmpl	$-1, %eax
	je	.L3099
	cmpb	$2, 496(%rbx)
	je	.L3099
	movb	$1, 496(%rbx)
	movl	%eax, 500(%rbx)
.L3099:
	movl	4(%r12), %eax
	movq	512(%rbx), %rcx
	shrl	$7, %eax
	leaq	56(%rcx), %rdi
	andl	$127, %eax
	cmpb	$62, %al
	je	.L3120
	cmpb	$61, %al
	je	.L3121
	movl	$16, %esi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movl	%eax, %ecx
.L3101:
	movl	4(%r12), %esi
	movl	%r13d, %edx
	movq	%r14, %rdi
	shrl	$7, %esi
	andl	$127, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder16CompareOperationENS0_5Token5ValueENS1_8RegisterEi@PLT
	jmp	.L3094
	.p2align 4,,10
	.p2align 3
.L3119:
	movq	-56(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	movl	(%r12), %eax
	cmpl	$-1, %eax
	je	.L3098
	cmpb	$2, 496(%rbx)
	je	.L3098
	movb	$1, 496(%rbx)
	movl	%eax, 500(%rbx)
.L3098:
	movl	4(%r12), %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	shrl	$7, %esi
	andl	$127, %esi
	call	_ZN2v88internal11interpreter17BytecodeGenerator22BuildLiteralCompareNilENS0_5Token5ValueENS1_20BytecodeArrayBuilder8NilValueE
	jmp	.L3094
	.p2align 4,,10
	.p2align 3
.L3117:
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder9LoadFalseEv@PLT
	jmp	.L3094
	.p2align 4,,10
	.p2align 3
.L3121:
	movl	$21, %esi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movl	%eax, %ecx
	jmp	.L3101
	.p2align 4,,10
	.p2align 3
.L3120:
	movl	$9, %esi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movl	%eax, %ecx
	jmp	.L3101
.L3118:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20952:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator21VisitCompareOperationEPNS0_16CompareOperationE, .-_ZN2v88internal11interpreter17BytecodeGenerator21VisitCompareOperationEPNS0_16CompareOperationE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator19VisitForNullishTestEPNS0_10ExpressionEPNS1_14BytecodeLabelsES6_S6_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator19VisitForNullishTestEPNS0_10ExpressionEPNS1_14BytecodeLabelsES6_S6_
	.type	_ZN2v88internal11interpreter17BytecodeGenerator19VisitForNullishTestEPNS0_10ExpressionEPNS1_14BytecodeLabelsES6_S6_, @function
_ZN2v88internal11interpreter17BytecodeGenerator19VisitForNullishTestEPNS0_10ExpressionEPNS1_14BytecodeLabelsES6_S6_:
.LFB21029:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	addq	$24, %r12
	subq	$8, %rsp
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	movl	%eax, %r13d
	cmpl	$1, %eax
	je	.L3123
	movq	%rbx, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal11interpreter14BytecodeLabels3NewEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder21JumpIfUndefinedOrNullEPNS1_13BytecodeLabelE@PLT
.L3123:
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter14BytecodeLabels3NewEv@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder10JumpIfTrueENS2_13ToBooleanModeEPNS1_13BytecodeLabelE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter14BytecodeLabels3NewEv@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	movq	%rax, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder4JumpEPNS1_13BytecodeLabelE@PLT
	.cfi_endproc
.LFE21029:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator19VisitForNullishTestEPNS0_10ExpressionEPNS1_14BytecodeLabelsES6_S6_, .-_ZN2v88internal11interpreter17BytecodeGenerator19VisitForNullishTestEPNS0_10ExpressionEPNS1_14BytecodeLabelsES6_S6_
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator11VisitDeleteEPNS0_14UnaryOperationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator11VisitDeleteEPNS0_14UnaryOperationE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator11VisitDeleteEPNS0_14UnaryOperationE, @function
_ZN2v88internal11interpreter17BytecodeGenerator11VisitDeleteEPNS0_14UnaryOperationE:
.LFB20947:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	8(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	4(%r13), %edx
	movl	%edx, %eax
	andl	$63, %eax
	cmpb	$44, %al
	je	.L3160
	cmpb	$43, %al
	je	.L3161
	cmpb	$54, %al
	jne	.L3138
	andb	$4, %dh
	jne	.L3138
	movq	8(%r13), %rbx
	movzwl	40(%rbx), %eax
	sarl	$7, %eax
	movl	%eax, %edx
	andl	$7, %edx
	testb	$4, %al
	jne	.L3139
	testb	%dl, %dl
	jne	.L3162
.L3141:
	movl	328(%r12), %r13d
	movq	336(%r12), %rdi
	leal	1(%r13), %eax
	cmpl	%eax, 332(%r12)
	movl	%eax, 328(%r12)
	cmovge	332(%r12), %eax
	movl	%eax, 332(%r12)
	testq	%rdi, %rdi
	je	.L3143
	movq	(%rdi), %rax
	movl	%r13d, %esi
	call	*16(%rax)
.L3143:
	movq	8(%rbx), %rsi
	leaq	24(%r12), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralEPKNS0_12AstRawStringE@PLT
	movl	%r13d, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movl	%r13d, %edx
	movl	$302, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_8RegisterE@PLT
	jmp	.L3129
	.p2align 4,,10
	.p2align 3
.L3138:
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal11interpreter17BytecodeGenerator14VisitForEffectEPNS0_10ExpressionE
	leaq	24(%r12), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder8LoadTrueEv@PLT
.L3129:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3163
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3161:
	.cfi_restore_state
	movq	8(%r13), %rbx
	leaq	24(%rdi), %r14
	movzbl	4(%rbx), %eax
	andl	$63, %eax
	cmpb	$44, %al
	je	.L3164
	movq	%r13, %rsi
	call	_ZN2v88internal11interpreter17BytecodeGenerator14VisitForEffectEPNS0_10ExpressionE
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder8LoadTrueEv@PLT
	jmp	.L3129
	.p2align 4,,10
	.p2align 3
.L3160:
	movq	8(%r13), %rsi
	call	_ZN2v88internal11interpreter17BytecodeGenerator21VisitForRegisterValueEPNS0_10ExpressionE
	movq	16(%r13), %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	movq	536(%r12), %rax
	leaq	24(%r12), %rdi
	movl	%r14d, %esi
	movzbl	129(%rax), %edx
	andl	$1, %edx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder6DeleteENS1_8RegisterENS0_12LanguageModeE@PLT
	jmp	.L3129
	.p2align 4,,10
	.p2align 3
.L3164:
	movq	16(%rdi), %rax
	leaq	-96(%rbp), %r13
	leaq	-104(%rbp), %r15
	movb	$0, -72(%rbp)
	movq	%r13, %xmm0
	movq	$-1, -120(%rbp)
	movq	%rax, -104(%rbp)
	movq	832(%rdi), %rax
	punpcklqdq	%xmm0, %xmm0
	movq	%r15, 832(%rdi)
	movq	$0, -80(%rbp)
	movq	%rax, -64(%rbp)
	movaps	%xmm0, -96(%rbp)
	movq	8(%rbx), %rsi
	movb	$0, -128(%rbp)
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	testb	$-128, 4(%rbx)
	jne	.L3165
.L3134:
	movl	328(%r12), %r8d
	movq	336(%r12), %rdi
	leal	1(%r8), %eax
	cmpl	%eax, 332(%r12)
	movl	%eax, 328(%r12)
	cmovge	332(%r12), %eax
	movl	%eax, 332(%r12)
	testq	%rdi, %rdi
	je	.L3135
	movq	(%rdi), %rax
	movl	%r8d, -132(%rbp)
	movl	%r8d, %esi
	call	*16(%rax)
	movl	-132(%rbp), %r8d
.L3135:
	movl	%r8d, %esi
	movq	%r14, %rdi
	movl	%r8d, -132(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	16(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	movq	536(%r12), %rax
	movq	%r14, %rdi
	leaq	-128(%rbp), %r12
	movl	-132(%rbp), %r8d
	movzbl	129(%rax), %edx
	movl	%r8d, %esi
	andl	$1, %edx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder6DeleteENS1_8RegisterENS0_12LanguageModeE@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4JumpEPNS1_13BytecodeLabelE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter14BytecodeLabels4BindEPNS1_20BytecodeArrayBuilderE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder8LoadTrueEv@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4BindEPNS1_13BytecodeLabelE@PLT
	movq	-112(%rbp), %rax
	movq	-64(%rbp), %rdx
	movq	%rdx, 832(%rax)
	movq	-96(%rbp), %rax
	cmpq	%r13, %rax
	je	.L3129
	.p2align 4,,10
	.p2align 3
.L3137:
	movq	(%rax), %rax
	cmpq	%r13, %rax
	jne	.L3137
	jmp	.L3129
	.p2align 4,,10
	.p2align 3
.L3139:
	cmpb	$4, %dl
	je	.L3141
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3162:
	leaq	24(%rdi), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder9LoadFalseEv@PLT
	jmp	.L3129
	.p2align 4,,10
	.p2align 3
.L3165:
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter14BytecodeLabels3NewEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder21JumpIfUndefinedOrNullEPNS1_13BytecodeLabelE@PLT
	jmp	.L3134
.L3163:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20947:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator11VisitDeleteEPNS0_14UnaryOperationE, .-_ZN2v88internal11interpreter17BytecodeGenerator11VisitDeleteEPNS0_14UnaryOperationE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator27VisitLogicalOrSubExpressionEPNS0_10ExpressionEPNS1_14BytecodeLabelsEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator27VisitLogicalOrSubExpressionEPNS0_10ExpressionEPNS1_14BytecodeLabelsEi
	.type	_ZN2v88internal11interpreter17BytecodeGenerator27VisitLogicalOrSubExpressionEPNS0_10ExpressionEPNS1_14BytecodeLabelsEi, @function
_ZN2v88internal11interpreter17BytecodeGenerator27VisitLogicalOrSubExpressionEPNS0_10ExpressionEPNS1_14BytecodeLabelsEi:
.LFB20988:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$8, %rsp
	call	_ZNK2v88internal10Expression15ToBooleanIsTrueEv@PLT
	movl	%eax, %r12d
	testb	%al, %al
	jne	.L3171
	movq	%r13, %rdi
	call	_ZNK2v88internal10Expression16ToBooleanIsFalseEv@PLT
	testb	%al, %al
	je	.L3172
.L3169:
	movq	568(%rbx), %rax
	testq	%rax, %rax
	sete	%cl
	cmpl	$-1, %r14d
	sete	%dl
	orb	%dl, %cl
	je	.L3173
.L3166:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3173:
	.cfi_restore_state
	movq	32(%rax), %rdi
	movl	%r14d, %esi
	xorl	%r12d, %r12d
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder15IncBlockCounterEi@PLT
	jmp	.L3166
	.p2align 4,,10
	.p2align 3
.L3172:
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	movq	%r15, %rdi
	movl	%eax, %r13d
	call	_ZN2v88internal11interpreter14BytecodeLabels3NewEv@PLT
	xorl	%esi, %esi
	cmpl	$1, %r13d
	leaq	24(%rbx), %rdi
	movq	%rax, %rdx
	sete	%sil
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder10JumpIfTrueENS2_13ToBooleanModeEPNS1_13BytecodeLabelE@PLT
	jmp	.L3169
	.p2align 4,,10
	.p2align 3
.L3171:
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	leaq	24(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter14BytecodeLabels4BindEPNS1_20BytecodeArrayBuilderE@PLT
	jmp	.L3166
	.cfi_endproc
.LFE20988:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator27VisitLogicalOrSubExpressionEPNS0_10ExpressionEPNS1_14BytecodeLabelsEi, .-_ZN2v88internal11interpreter17BytecodeGenerator27VisitLogicalOrSubExpressionEPNS0_10ExpressionEPNS1_14BytecodeLabelsEi
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator11VisitTypeOfEPNS0_14UnaryOperationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator11VisitTypeOfEPNS0_14UnaryOperationE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator11VisitTypeOfEPNS0_14UnaryOperationE, @function
_ZN2v88internal11interpreter17BytecodeGenerator11VisitTypeOfEPNS0_14UnaryOperationE:
.LFB20944:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	8(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	4(%rsi), %edx
	movl	%edx, %eax
	andl	$63, %eax
	cmpb	$54, %al
	je	.L3183
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
.L3177:
	leaq	24(%rbx), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder6TypeOfEv@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3184
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3183:
	.cfi_restore_state
	movq	816(%rdi), %rax
	movq	8(%rsi), %rsi
	shrl	$11, %edx
	xorl	%ecx, %ecx
	andl	$1, %edx
	movq	%rdi, -56(%rbp)
	movq	%rax, -64(%rbp)
	movl	328(%rdi), %eax
	movq	$2, -40(%rbp)
	movl	%eax, -48(%rbp)
	leaq	-64(%rbp), %rax
	movq	%rax, 816(%rdi)
	call	_ZN2v88internal11interpreter17BytecodeGenerator17BuildVariableLoadEPNS0_8VariableENS0_13HoleCheckModeENS0_10TypeofModeE
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rax
	movq	336(%rdx), %rdi
	movq	%rax, 816(%rdx)
	movl	-48(%rbp), %eax
	movl	328(%rdx), %esi
	movl	%eax, 328(%rdx)
	testq	%rdi, %rdi
	je	.L3177
	subl	%eax, %esi
	movq	(%rdi), %rcx
	movq	%rsi, %rdx
	movl	%eax, %esi
	salq	$32, %rdx
	orq	%rdx, %rsi
	call	*32(%rcx)
	jmp	.L3177
.L3184:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20944:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator11VisitTypeOfEPNS0_14UnaryOperationE, .-_ZN2v88internal11interpreter17BytecodeGenerator11VisitTypeOfEPNS0_14UnaryOperationE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator25VisitNullishSubExpressionEPNS0_10ExpressionEPNS1_14BytecodeLabelsEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator25VisitNullishSubExpressionEPNS0_10ExpressionEPNS1_14BytecodeLabelsEi
	.type	_ZN2v88internal11interpreter17BytecodeGenerator25VisitNullishSubExpressionEPNS0_10ExpressionEPNS1_14BytecodeLabelsEi, @function
_ZN2v88internal11interpreter17BytecodeGenerator25VisitNullishSubExpressionEPNS0_10ExpressionEPNS1_14BytecodeLabelsEi:
.LFB20990:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal10Expression30IsLiteralButNotNullOrUndefinedEv@PLT
	movl	%eax, %r13d
	testb	%al, %al
	jne	.L3193
	movq	%r12, %rdi
	call	_ZNK2v88internal10Expression13IsNullLiteralEv@PLT
	testb	%al, %al
	je	.L3194
.L3189:
	movq	568(%rbx), %rax
	testq	%rax, %rax
	sete	%cl
	cmpl	$-1, %r14d
	sete	%dl
	orb	%dl, %cl
	je	.L3195
.L3185:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3196
	addq	$56, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3195:
	.cfi_restore_state
	movq	32(%rax), %rdi
	movl	%r14d, %esi
	xorl	%r13d, %r13d
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder15IncBlockCounterEi@PLT
	jmp	.L3185
	.p2align 4,,10
	.p2align 3
.L3194:
	movq	%r12, %rdi
	call	_ZNK2v88internal10Expression18IsUndefinedLiteralEv@PLT
	testb	%al, %al
	jne	.L3189
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	leaq	24(%rbx), %r8
	leaq	-80(%rbp), %r9
	movb	$0, -80(%rbp)
	movq	%r9, %rsi
	movq	%r8, %rdi
	movq	%r9, -96(%rbp)
	movq	%r8, -88(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder21JumpIfUndefinedOrNullEPNS1_13BytecodeLabelE@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal11interpreter14BytecodeLabels3NewEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4JumpEPNS1_13BytecodeLabelE@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %r8
	movq	%r9, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4BindEPNS1_13BytecodeLabelE@PLT
	jmp	.L3189
	.p2align 4,,10
	.p2align 3
.L3193:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	leaq	24(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter14BytecodeLabels4BindEPNS1_20BytecodeArrayBuilderE@PLT
	jmp	.L3185
.L3196:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20990:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator25VisitNullishSubExpressionEPNS0_10ExpressionEPNS1_14BytecodeLabelsEi, .-_ZN2v88internal11interpreter17BytecodeGenerator25VisitNullishSubExpressionEPNS0_10ExpressionEPNS1_14BytecodeLabelsEi
	.section	.text.unlikely._ZN2v88internal11interpreter17BytecodeGenerator36VisitInitializeClassMembersStatementEPNS0_31InitializeClassMembersStatementE,"ax",@progbits
	.align 2
.LCOLDB19:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator36VisitInitializeClassMembersStatementEPNS0_31InitializeClassMembersStatementE,"ax",@progbits
.LHOTB19:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator36VisitInitializeClassMembersStatementEPNS0_31InitializeClassMembersStatementE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator36VisitInitializeClassMembersStatementEPNS0_31InitializeClassMembersStatementE, @function
_ZN2v88internal11interpreter17BytecodeGenerator36VisitInitializeClassMembersStatementEPNS0_31InitializeClassMembersStatementE:
.LFB20827:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -120(%rbp)
	movl	328(%rdi), %ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leal	3(%rcx), %eax
	cmpl	%eax, 332(%rdi)
	movl	%ecx, -140(%rbp)
	movl	%eax, 328(%rdi)
	cmovge	332(%rdi), %eax
	movl	%eax, 332(%rdi)
	movq	336(%rdi), %rdi
	movl	%ecx, %eax
	movq	%rax, -128(%rbp)
	testq	%rdi, %rdi
	je	.L3198
	movabsq	$12884901888, %rsi
	movq	%rax, %rcx
	movq	(%rdi), %rax
	orq	%rcx, %rsi
	call	*24(%rax)
.L3198:
	movl	-140(%rbp), %ebx
	leaq	24(%r15), %r14
	xorl	%r13d, %r13d
	movabsq	$12884901888, %r12
	movq	%r14, %rdi
	orq	-128(%rbp), %r12
	leal	1(%rbx), %eax
	movl	%eax, -104(%rbp)
	leal	2(%rbx), %eax
	movl	%eax, -100(%rbp)
	call	_ZNK2v88internal11interpreter20BytecodeArrayBuilder8ReceiverEv@PLT
	movl	%ebx, %edx
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder12MoveRegisterENS1_8RegisterES3_@PLT
	movq	-120(%rbp), %rax
	movabsq	$-4294967296, %rcx
	movq	%rcx, -136(%rbp)
	movq	8(%rax), %rax
	movl	12(%rax), %edx
	testl	%edx, %edx
	jg	.L3199
	jmp	.L3197
	.p2align 4,,10
	.p2align 3
.L3202:
	andq	$-4, %rdi
	call	_ZNK2v88internal10Expression15IsStringLiteralEv@PLT
	testb	%al, %al
	je	.L3203
	movq	(%rbx), %rax
	andq	$-4, %rax
	movzbl	4(%rax), %edx
	andl	$63, %edx
	cmpb	$41, %dl
	jne	.L3204
	movq	8(%rax), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralEPKNS0_12AstRawStringE@PLT
	movl	-104(%rbp), %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	.p2align 4,,10
	.p2align 3
.L3201:
	movq	8(%rbx), %rsi
	movl	(%rsi), %eax
	cmpl	$-1, %eax
	je	.L3206
	movb	$2, 496(%r15)
	movl	%eax, 500(%r15)
	movq	8(%rbx), %rsi
.L3206:
	movq	816(%r15), %rax
	cmpb	$0, 8(%r15)
	movq	%rsi, -112(%rbp)
	movq	%r15, -88(%rbp)
	movq	%rax, -96(%rbp)
	movl	328(%r15), %eax
	movq	$2, -72(%rbp)
	movl	%eax, -80(%rbp)
	leaq	-96(%rbp), %rax
	movq	%rax, 816(%r15)
	jne	.L3207
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r15), %rax
	movq	-112(%rbp), %rsi
	jb	.L3235
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator25VisitNoStackOverflowCheckEPNS0_7AstNodeE
.L3207:
	movl	-100(%rbp), %esi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rax
	movq	336(%rdx), %rdi
	movq	%rax, 816(%rdx)
	movl	-80(%rbp), %eax
	movl	328(%rdx), %esi
	movl	%eax, 328(%rdx)
	testq	%rdi, %rdi
	je	.L3209
	subl	%eax, %esi
	movq	(%rdi), %rdx
	movq	%rsi, %rcx
	movl	%eax, %esi
	salq	$32, %rcx
	orq	%rcx, %rsi
	call	*32(%rdx)
.L3209:
	movq	8(%rbx), %rdi
	call	_ZN2v88internal15FunctionLiteral15NeedsHomeObjectEPNS0_10ExpressionE@PLT
	testb	%al, %al
	je	.L3210
	movq	512(%r15), %rax
	leaq	56(%rax), %rdi
	movq	536(%r15), %rax
	movzbl	129(%rax), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%esi, %esi
	andl	$-9, %esi
	addl	$11, %esi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movl	-140(%rbp), %esi
	movq	%r14, %rdi
	movl	%eax, -112(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	movl	-112(%rbp), %edx
	movl	-100(%rbp), %esi
	movq	%rax, %rdi
	movq	536(%r15), %rax
	movzbl	129(%rax), %ecx
	andl	$1, %ecx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder23StoreHomeObjectPropertyENS1_8RegisterEiNS0_12LanguageModeE@PLT
.L3210:
	cmpb	$3, 16(%rbx)
	movl	$204, %esi
	jne	.L3212
	cmpb	$1, 18(%rbx)
	sbbl	%esi, %esi
	andl	$8, %esi
	addl	$204, %esi
.L3212:
	andq	-136(%rbp), %r12
	orq	-128(%rbp), %r12
	movq	%r14, %rdi
	addq	$1, %r13
	movq	%r12, %rdx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_12RegisterListE@PLT
	movq	-120(%rbp), %rax
	movq	8(%rax), %rax
	cmpl	%r13d, 12(%rax)
	jle	.L3197
.L3199:
	movq	(%rax), %rax
	movq	(%rax,%r13,8), %rbx
	movq	(%rbx), %rdi
	testb	$3, %dil
	jne	.L3234
	cmpb	$0, 18(%rbx)
	je	.L3202
.L3234:
	movq	24(%rbx), %rsi
	movq	%r15, %rdi
	movl	$1, %ecx
	movl	$1, %edx
	call	_ZN2v88internal11interpreter17BytecodeGenerator17BuildVariableLoadEPNS0_8VariableENS0_13HoleCheckModeENS0_10TypeofModeE
	movl	-104(%rbp), %esi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	jmp	.L3201
	.p2align 4,,10
	.p2align 3
.L3235:
	movb	$1, 8(%r15)
	jmp	.L3207
	.p2align 4,,10
	.p2align 3
.L3197:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3236
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3203:
	.cfi_restore_state
	movq	(%rbx), %rsi
	movq	%r15, %rdi
	andq	$-4, %rsi
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	movl	-104(%rbp), %esi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder6ToNameENS1_8RegisterE@PLT
	jmp	.L3201
.L3236:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal11interpreter17BytecodeGenerator36VisitInitializeClassMembersStatementEPNS0_31InitializeClassMembersStatementE
	.cfi_startproc
	.type	_ZN2v88internal11interpreter17BytecodeGenerator36VisitInitializeClassMembersStatementEPNS0_31InitializeClassMembersStatementE.cold, @function
_ZN2v88internal11interpreter17BytecodeGenerator36VisitInitializeClassMembersStatementEPNS0_31InitializeClassMembersStatementE.cold:
.LFSB20827:
.L3204:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	8, %rax
	ud2
	.cfi_endproc
.LFE20827:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator36VisitInitializeClassMembersStatementEPNS0_31InitializeClassMembersStatementE
	.size	_ZN2v88internal11interpreter17BytecodeGenerator36VisitInitializeClassMembersStatementEPNS0_31InitializeClassMembersStatementE, .-_ZN2v88internal11interpreter17BytecodeGenerator36VisitInitializeClassMembersStatementEPNS0_31InitializeClassMembersStatementE
	.section	.text.unlikely._ZN2v88internal11interpreter17BytecodeGenerator36VisitInitializeClassMembersStatementEPNS0_31InitializeClassMembersStatementE
	.size	_ZN2v88internal11interpreter17BytecodeGenerator36VisitInitializeClassMembersStatementEPNS0_31InitializeClassMembersStatementE.cold, .-_ZN2v88internal11interpreter17BytecodeGenerator36VisitInitializeClassMembersStatementEPNS0_31InitializeClassMembersStatementE.cold
.LCOLDE19:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator36VisitInitializeClassMembersStatementEPNS0_31InitializeClassMembersStatementE
.LHOTE19:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator12VisitInScopeEPNS0_9StatementEPNS0_5ScopeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator12VisitInScopeEPNS0_9StatementEPNS0_5ScopeE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator12VisitInScopeEPNS0_9StatementEPNS0_5ScopeE, @function
_ZN2v88internal11interpreter17BytecodeGenerator12VisitInScopeEPNS0_9StatementEPNS0_5ScopeE:
.LFB21031:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	536(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L3238
	movq	%rdx, 536(%rdi)
.L3238:
	movq	808(%rbx), %rax
	movq	%rdx, %xmm1
	movq	%rbx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal11interpreter8Register15current_contextEv@PLT
	movl	$0, -52(%rbp)
	movl	%eax, -56(%rbp)
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L3239
	movq	-80(%rbp), %rdi
	movl	28(%rax), %ecx
	movl	328(%rdi), %r14d
	leal	1(%rcx), %edx
	movq	336(%rdi), %r8
	movl	%edx, -52(%rbp)
	leal	1(%r14), %edx
	cmpl	%edx, 332(%rdi)
	movl	%edx, 328(%rdi)
	cmovge	332(%rdi), %edx
	movl	%edx, 332(%rdi)
	testq	%r8, %r8
	je	.L3240
	movq	(%r8), %rax
	movq	%r8, %rdi
	movl	%r14d, %esi
	call	*16(%rax)
	movq	-64(%rbp), %rax
	movq	-80(%rbp), %rdi
.L3240:
	movl	%r14d, 24(%rax)
	addq	$24, %rdi
	movl	%r14d, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11PushContextENS1_8RegisterE@PLT
.L3239:
	movq	-80(%rbp), %rdi
	leaq	-80(%rbp), %rax
	movq	%rax, 808(%rdi)
	cmpb	$0, 8(%rbx)
	je	.L3259
.L3241:
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L3243
	movl	24(%rax), %esi
	addq	$24, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder10PopContextENS1_8RegisterE@PLT
	movq	-64(%rbp), %rax
	movl	-56(%rbp), %edx
	movq	-80(%rbp), %rdi
	movl	%edx, 24(%rax)
.L3243:
	movq	%rax, 808(%rdi)
	cmpq	%r13, 536(%rbx)
	je	.L3237
	movq	%r13, 536(%rbx)
.L3237:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3260
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3259:
	.cfi_restore_state
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%rbx), %rax
	jb	.L3261
	movq	%rbx, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal11interpreter17BytecodeGenerator25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movq	-80(%rbp), %rdi
	jmp	.L3241
	.p2align 4,,10
	.p2align 3
.L3261:
	movb	$1, 8(%rbx)
	movq	-80(%rbp), %rdi
	jmp	.L3241
.L3260:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21031:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator12VisitInScopeEPNS0_9StatementEPNS0_5ScopeE, .-_ZN2v88internal11interpreter17BytecodeGenerator12VisitInScopeEPNS0_9StatementEPNS0_5ScopeE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator18VisitWithStatementEPNS0_13WithStatementE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator18VisitWithStatementEPNS0_13WithStatementE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator18VisitWithStatementEPNS0_13WithStatementE, @function
_ZN2v88internal11interpreter17BytecodeGenerator18VisitWithStatementEPNS0_13WithStatementE:
.LFB20764:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%rsi), %eax
	cmpl	$-1, %eax
	je	.L3263
	movb	$2, 496(%rdi)
	movl	%eax, 500(%rdi)
.L3263:
	movq	16(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	movq	8(%rbx), %r13
	movq	816(%r12), %rax
	movq	%r12, -88(%rbp)
	movl	328(%r12), %r14d
	movq	336(%r12), %rdi
	movq	$2, -72(%rbp)
	movq	%rax, -96(%rbp)
	leaq	-96(%rbp), %rax
	movq	%rax, 816(%r12)
	leal	1(%r14), %eax
	cmpl	%eax, 332(%r12)
	movl	%eax, 328(%r12)
	cmovge	332(%r12), %eax
	movl	%r14d, -80(%rbp)
	movl	%eax, 332(%r12)
	testq	%rdi, %rdi
	je	.L3264
	movq	(%rdi), %rax
	movl	%r14d, %esi
	call	*16(%rax)
.L3264:
	leaq	24(%r12), %r15
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder8ToObjectENS1_8RegisterE@PLT
	movq	%r13, %rdx
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder17CreateWithContextENS1_8RegisterEPKNS0_5ScopeE@PLT
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rax
	movq	336(%rdx), %rdi
	movq	%rax, 816(%rdx)
	movl	-80(%rbp), %eax
	movl	328(%rdx), %esi
	movl	%eax, 328(%rdx)
	testq	%rdi, %rdi
	je	.L3265
	subl	%eax, %esi
	movq	(%rdi), %rcx
	movq	%rsi, %rdx
	movl	%eax, %esi
	salq	$32, %rdx
	orq	%rdx, %rsi
	call	*32(%rcx)
.L3265:
	movq	8(%rbx), %rdx
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator12VisitInScopeEPNS0_9StatementEPNS0_5ScopeE
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3277
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3277:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20764:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator18VisitWithStatementEPNS0_13WithStatementE, .-_ZN2v88internal11interpreter17BytecodeGenerator18VisitWithStatementEPNS0_13WithStatementE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator28VisitAndPushIntoRegisterListEPNS0_10ExpressionEPNS1_12RegisterListE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator28VisitAndPushIntoRegisterListEPNS0_10ExpressionEPNS1_12RegisterListE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator28VisitAndPushIntoRegisterListEPNS0_10ExpressionEPNS1_12RegisterListE, @function
_ZN2v88internal11interpreter17BytecodeGenerator28VisitAndPushIntoRegisterListEPNS0_10ExpressionEPNS1_12RegisterListE:
.LFB21023:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdx, %r12
	leaq	-80(%rbp), %rdx
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	816(%rdi), %rcx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	328(%rdi), %eax
	cmpb	$0, 8(%rdi)
	movq	%rdi, -72(%rbp)
	movq	%rcx, -80(%rbp)
	movl	%eax, -64(%rbp)
	movq	$2, -56(%rbp)
	movq	%rdx, 816(%rdi)
	je	.L3298
	movq	%rdi, %rdx
	xorl	%esi, %esi
.L3279:
	movq	336(%rdx), %rdi
	movq	%rcx, 816(%rdx)
	movl	%eax, 328(%rdx)
	testq	%rdi, %rdi
	je	.L3281
	movq	%rsi, %rdx
	movq	(%rdi), %rcx
	movl	%eax, %esi
	salq	$32, %rdx
	orq	%rdx, %rsi
	call	*32(%rcx)
.L3281:
	movl	328(%rbx), %r13d
	movq	336(%rbx), %rdi
	leal	1(%r13), %eax
	cmpl	%eax, 332(%rbx)
	movl	%eax, 328(%rbx)
	cmovge	332(%rbx), %eax
	movl	%eax, 332(%rbx)
	testq	%rdi, %rdi
	je	.L3282
	movq	(%rdi), %rax
	movl	%r13d, %esi
	call	*16(%rax)
.L3282:
	movl	4(%r12), %eax
	leal	1(%rax), %esi
	movl	%esi, 4(%r12)
	testl	%esi, %esi
	jne	.L3299
	cmpl	%r13d, %esi
	jne	.L3300
.L3284:
	leaq	24(%rbx), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3301
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3299:
	.cfi_restore_state
	addl	(%r12), %eax
	movl	%eax, %esi
	cmpl	%r13d, %esi
	je	.L3284
.L3300:
	leaq	.LC5(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3298:
	movq	%rsi, %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%rbx), %rax
	jb	.L3302
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator25VisitNoStackOverflowCheckEPNS0_7AstNodeE
.L3297:
	movq	-72(%rbp), %rdx
	movl	-64(%rbp), %eax
	movq	-80(%rbp), %rcx
	movl	328(%rdx), %esi
	subl	%eax, %esi
	jmp	.L3279
	.p2align 4,,10
	.p2align 3
.L3302:
	movb	$1, 8(%rbx)
	jmp	.L3297
.L3301:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21023:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator28VisitAndPushIntoRegisterListEPNS0_10ExpressionEPNS1_12RegisterListE, .-_ZN2v88internal11interpreter17BytecodeGenerator28VisitAndPushIntoRegisterListEPNS0_10ExpressionEPNS1_12RegisterListE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator14VisitArgumentsEPKNS0_8ZoneListIPNS0_10ExpressionEEEPNS1_12RegisterListE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator14VisitArgumentsEPKNS0_8ZoneListIPNS0_10ExpressionEEEPNS1_12RegisterListE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator14VisitArgumentsEPKNS0_8ZoneListIPNS0_10ExpressionEEEPNS1_12RegisterListE, @function
_ZN2v88internal11interpreter17BytecodeGenerator14VisitArgumentsEPKNS0_8ZoneListIPNS0_10ExpressionEEEPNS1_12RegisterListE:
.LFB20937:
	.cfi_startproc
	endbr64
	movl	12(%rsi), %eax
	testl	%eax, %eax
	jle	.L3308
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L3305:
	movq	(%r12), %rax
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	(%rax,%rbx,8), %rsi
	addq	$1, %rbx
	call	_ZN2v88internal11interpreter17BytecodeGenerator28VisitAndPushIntoRegisterListEPNS0_10ExpressionEPNS1_12RegisterListE
	cmpl	%ebx, 12(%r12)
	jg	.L3305
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3308:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.cfi_endproc
.LFE20937:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator14VisitArgumentsEPKNS0_8ZoneListIPNS0_10ExpressionEEEPNS1_12RegisterListE, .-_ZN2v88internal11interpreter17BytecodeGenerator14VisitArgumentsEPKNS0_8ZoneListIPNS0_10ExpressionEEEPNS1_12RegisterListE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator14VisitCallSuperEPNS0_4CallE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator14VisitCallSuperEPNS0_4CallE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator14VisitCallSuperEPNS0_4CallE, @function
_ZN2v88internal11interpreter17BytecodeGenerator14VisitCallSuperEPNS0_4CallE:
.LFB20939:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	328(%rdi), %eax
	movl	%eax, -124(%rbp)
	movzbl	4(%rdx), %eax
	andl	$63, %eax
	cmpb	$48, %al
	movl	$0, %eax
	cmove	%rdx, %rax
	movq	%rax, -112(%rbp)
	movl	28(%rsi), %eax
	testl	%eax, %eax
	jle	.L3334
	movq	16(%rsi), %rcx
	leal	-1(%rax), %esi
	xorl	%eax, %eax
	jmp	.L3314
	.p2align 4,,10
	.p2align 3
.L3362:
	leal	1(%rax), %ebx
	leaq	1(%rax), %rdx
	cmpq	%rax, %rsi
	je	.L3313
	movq	%rdx, %rax
.L3314:
	movq	(%rcx,%rax,8), %rdx
	movl	%eax, %ebx
	movl	4(%rdx), %edx
	andl	$63, %edx
	cmpb	$46, %dl
	jne	.L3362
.L3313:
	movq	-112(%rbp), %rax
	movq	%r15, %rdi
	movq	16(%rax), %rsi
	call	_ZN2v88internal11interpreter17BytecodeGenerator21VisitForRegisterValueEPNS0_10ExpressionE
	movl	328(%r15), %esi
	movq	336(%r15), %rdi
	movl	%eax, -120(%rbp)
	leal	1(%rsi), %eax
	cmpl	%eax, 332(%r15)
	movl	%esi, -116(%rbp)
	movl	%eax, 328(%r15)
	cmovge	332(%r15), %eax
	movl	%eax, 332(%r15)
	testq	%rdi, %rdi
	je	.L3315
	movq	(%rdi), %rax
	call	*16(%rax)
.L3315:
	leaq	24(%r15), %rax
	movl	-120(%rbp), %esi
	movq	%rax, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	movl	-116(%rbp), %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder19GetSuperConstructorENS1_8RegisterE@PLT
	movl	28(%r13), %eax
	leal	-1(%rax), %edx
	cmpl	%ebx, %edx
	jg	.L3363
	movl	328(%r15), %edx
	movl	$0, -92(%rbp)
	movl	%edx, -96(%rbp)
	testl	%eax, %eax
	jle	.L3320
	xorl	%r14d, %r14d
	leaq	-96(%rbp), %r12
	.p2align 4,,10
	.p2align 3
.L3321:
	movq	16(%r13), %rdx
	movq	%r15, %rdi
	movq	(%rdx,%r14,8), %rsi
	movq	%r12, %rdx
	addq	$1, %r14
	call	_ZN2v88internal11interpreter17BytecodeGenerator28VisitAndPushIntoRegisterListEPNS0_10ExpressionEPNS1_12RegisterListE
	cmpl	%r14d, 28(%r13)
	jg	.L3321
.L3320:
	movq	-112(%rbp), %rax
	movq	%r15, %rdi
	movq	8(%rax), %rsi
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	movl	0(%r13), %eax
	cmpl	$-1, %eax
	je	.L3322
	cmpb	$2, 496(%r15)
	je	.L3322
	movb	$1, 496(%r15)
	movl	%eax, 500(%r15)
.L3322:
	movq	512(%r15), %rax
	movl	$4, %esi
	leaq	56(%rax), %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movq	-96(%rbp), %rdx
	movl	-116(%rbp), %esi
	movl	%eax, %ecx
	movl	28(%r13), %eax
	movq	-104(%rbp), %rdi
	subl	$1, %eax
	cmpl	%ebx, %eax
	je	.L3364
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder9ConstructENS1_8RegisterENS1_12RegisterListEi@PLT
.L3319:
	movq	512(%r15), %rax
	movq	16(%rax), %rdi
	call	_ZNK2v88internal15FunctionLiteral4kindEv@PLT
	subl	$3, %eax
	cmpb	$1, %al
	jbe	.L3325
	movq	528(%r15), %rdi
	call	_ZN2v88internal5Scope16GetReceiverScopeEv@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$16, %edx
	movq	176(%rax), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator23BuildVariableAssignmentEPNS0_8VariableENS0_5Token5ValueENS0_13HoleCheckModeENS0_18LookupHoistingModeE
.L3325:
	movl	328(%r15), %r12d
	movq	336(%r15), %rdi
	leal	1(%r12), %eax
	cmpl	%eax, 332(%r15)
	movl	%eax, 328(%r15)
	cmovge	332(%r15), %eax
	movl	%eax, 332(%r15)
	testq	%rdi, %rdi
	je	.L3326
	movq	(%rdi), %rax
	movl	%r12d, %esi
	call	*16(%rax)
.L3326:
	movq	-104(%rbp), %rdi
	movl	%r12d, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	512(%r15), %rax
	movq	16(%rax), %rdi
	call	_ZNK2v88internal15FunctionLiteral29requires_brand_initializationEv@PLT
	testb	%al, %al
	jne	.L3365
.L3327:
	movq	512(%r15), %rax
	movq	16(%rax), %rdi
	testb	$16, 6(%rdi)
	jne	.L3330
	call	_ZNK2v88internal15FunctionLiteral4kindEv@PLT
	subl	$4, %eax
	cmpb	$1, %al
	jbe	.L3329
.L3330:
	movl	-120(%rbp), %esi
	movl	%r12d, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator33BuildInstanceMemberInitializationENS1_8RegisterES3_
.L3329:
	movq	-104(%rbp), %rdi
	movl	%r12d, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	movl	-124(%rbp), %ecx
	movq	336(%r15), %rdi
	movl	328(%r15), %esi
	movl	%ecx, 328(%r15)
	testq	%rdi, %rdi
	je	.L3311
	subl	%ecx, %esi
	movq	(%rdi), %rdx
	movq	%rsi, %rax
	movl	%ecx, %esi
	salq	$32, %rax
	orq	%rax, %rsi
	call	*32(%rdx)
.L3311:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3366
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3364:
	.cfi_restore_state
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder19ConstructWithSpreadENS1_8RegisterENS1_12RegisterListEi@PLT
	jmp	.L3319
	.p2align 4,,10
	.p2align 3
.L3365:
	movl	%r12d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator31BuildPrivateBrandInitializationENS1_8RegisterE
	jmp	.L3327
	.p2align 4,,10
	.p2align 3
.L3363:
	leaq	16(%r13), %rsi
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator23BuildCreateArrayLiteralEPKNS0_8ZoneListIPNS0_10ExpressionEEEPNS0_12ArrayLiteralE
	movl	328(%r15), %r12d
	movq	336(%r15), %rdi
	leal	3(%r12), %eax
	cmpl	%eax, 332(%r15)
	movq	%r12, %r13
	movl	%eax, 328(%r15)
	cmovge	332(%r15), %eax
	movl	%eax, 332(%r15)
	testq	%rdi, %rdi
	je	.L3317
	movabsq	$12884901888, %rsi
	movq	(%rdi), %rax
	orq	%r12, %rsi
	call	*24(%rax)
.L3317:
	movq	-104(%rbp), %rbx
	leal	1(%r13), %esi
	movabsq	$12884901888, %rax
	orq	%rax, %r12
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movl	-116(%rbp), %esi
	movl	%r13d, %edx
	movq	%rbx, %rdi
	addl	$2, %r13d
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder12MoveRegisterENS1_8RegisterES3_@PLT
	movq	-112(%rbp), %rax
	movq	%r15, %rdi
	movq	8(%rax), %rsi
	movq	816(%r15), %rax
	movq	%r15, -88(%rbp)
	movq	$2, -72(%rbp)
	movq	%rax, -96(%rbp)
	movl	328(%r15), %eax
	movl	%eax, -80(%rbp)
	leaq	-96(%rbp), %rax
	movq	%rax, 816(%r15)
	call	_ZN2v88internal11interpreter17BytecodeGenerator5VisitEPNS0_7AstNodeE
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rax
	movq	336(%rdx), %rdi
	movq	%rax, 816(%rdx)
	movl	-80(%rbp), %eax
	movl	328(%rdx), %esi
	movl	%eax, 328(%rdx)
	testq	%rdi, %rdi
	je	.L3318
	subl	%eax, %esi
	movq	(%rdi), %rcx
	salq	$32, %rsi
	movq	%rsi, %rdx
	movl	%eax, %esi
	orq	%rdx, %rsi
	call	*32(%rcx)
.L3318:
	movq	-104(%rbp), %rdi
	movq	%r12, %rdx
	movl	$235, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder13CallJSRuntimeEiNS1_12RegisterListE@PLT
	jmp	.L3319
.L3334:
	xorl	%ebx, %ebx
	jmp	.L3313
.L3366:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20939:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator14VisitCallSuperEPNS0_4CallE, .-_ZN2v88internal11interpreter17BytecodeGenerator14VisitCallSuperEPNS0_4CallE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator12VisitCallNewEPNS0_7CallNewE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator12VisitCallNewEPNS0_7CallNewE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator12VisitCallNewEPNS0_7CallNewE, @function
_ZN2v88internal11interpreter17BytecodeGenerator12VisitCallNewEPNS0_7CallNewE:
.LFB20940:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	8(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter17BytecodeGenerator21VisitForRegisterValueEPNS0_10ExpressionE
	movl	$0, -60(%rbp)
	movl	%eax, %r15d
	movl	328(%r13), %eax
	movl	%eax, -64(%rbp)
	movl	28(%r12), %eax
	testl	%eax, %eax
	jle	.L3368
	xorl	%ebx, %ebx
	leaq	-64(%rbp), %r14
	.p2align 4,,10
	.p2align 3
.L3369:
	movq	16(%r12), %rax
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	(%rax,%rbx,8), %rsi
	addq	$1, %rbx
	call	_ZN2v88internal11interpreter17BytecodeGenerator28VisitAndPushIntoRegisterListEPNS0_10ExpressionEPNS1_12RegisterListE
	cmpl	%ebx, 28(%r12)
	jg	.L3369
.L3368:
	movl	(%r12), %eax
	leaq	24(%r13), %r14
	cmpl	$-1, %eax
	je	.L3370
	cmpb	$2, 496(%r13)
	je	.L3370
	movb	$1, 496(%r13)
	movl	%eax, 500(%r13)
.L3370:
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	movq	512(%r13), %rdi
	movl	$4, %esi
	addq	$56, %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movl	28(%r12), %edx
	movl	%eax, %ecx
	testl	%edx, %edx
	je	.L3371
	movq	16(%r12), %rax
	subl	$1, %edx
	movslq	%edx, %rdx
	movq	(%rax,%rdx,8), %rax
	movl	4(%rax), %eax
	andl	$63, %eax
	cmpb	$46, %al
	je	.L3382
.L3371:
	movq	-64(%rbp), %rdx
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder9ConstructENS1_8RegisterENS1_12RegisterListEi@PLT
.L3367:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3383
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3382:
	.cfi_restore_state
	movq	-64(%rbp), %rdx
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder19ConstructWithSpreadENS1_8RegisterENS1_12RegisterListEi@PLT
	jmp	.L3367
.L3383:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20940:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator12VisitCallNewEPNS0_7CallNewE, .-_ZN2v88internal11interpreter17BytecodeGenerator12VisitCallNewEPNS0_7CallNewE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator16VisitCallRuntimeEPNS0_11CallRuntimeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator16VisitCallRuntimeEPNS0_11CallRuntimeE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator16VisitCallRuntimeEPNS0_11CallRuntimeE, @function
_ZN2v88internal11interpreter17BytecodeGenerator16VisitCallRuntimeEPNS0_11CallRuntimeE:
.LFB20941:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movl	328(%rdi), %ecx
	movl	36(%rsi), %edx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rsi), %rax
	movl	$0, -44(%rbp)
	movl	%ecx, -48(%rbp)
	testq	%rax, %rax
	je	.L3397
	testl	%edx, %edx
	jle	.L3389
	xorl	%ebx, %ebx
	leaq	-48(%rbp), %r14
	.p2align 4,,10
	.p2align 3
.L3390:
	movq	24(%r12), %rax
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	(%rax,%rbx,8), %rsi
	addq	$1, %rbx
	call	_ZN2v88internal11interpreter17BytecodeGenerator28VisitAndPushIntoRegisterListEPNS0_10ExpressionEPNS1_12RegisterListE
	cmpl	%ebx, 36(%r12)
	jg	.L3390
	movq	16(%r12), %rax
.L3389:
	movq	-48(%rbp), %rdx
	movl	(%rax), %esi
	leaq	24(%r13), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_12RegisterListE@PLT
.L3384:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3398
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3397:
	.cfi_restore_state
	xorl	%ebx, %ebx
	leaq	-48(%rbp), %r14
	testl	%edx, %edx
	jle	.L3388
	.p2align 4,,10
	.p2align 3
.L3386:
	movq	24(%r12), %rax
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	(%rax,%rbx,8), %rsi
	addq	$1, %rbx
	call	_ZN2v88internal11interpreter17BytecodeGenerator28VisitAndPushIntoRegisterListEPNS0_10ExpressionEPNS1_12RegisterListE
	cmpl	%ebx, 36(%r12)
	jg	.L3386
.L3388:
	movl	8(%r12), %esi
	movq	-48(%rbp), %rdx
	leaq	24(%r13), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder13CallJSRuntimeEiNS1_12RegisterListE@PLT
	jmp	.L3384
.L3398:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20941:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator16VisitCallRuntimeEPNS0_11CallRuntimeE, .-_ZN2v88internal11interpreter17BytecodeGenerator16VisitCallRuntimeEPNS0_11CallRuntimeE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator22VisitTryCatchStatementEPNS0_17TryCatchStatementE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator22VisitTryCatchStatementEPNS0_17TryCatchStatementE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator22VisitTryCatchStatementEPNS0_17TryCatchStatementE, @function
_ZN2v88internal11interpreter17BytecodeGenerator22VisitTryCatchStatementEPNS0_17TryCatchStatementE:
.LFB20779:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	24(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$136, %rsp
	movl	32(%rsi), %r14d
	movq	568(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	872(%rdi), %eax
	movq	%r12, -104(%rbp)
	testl	%r14d, %r14d
	cmove	%eax, %r14d
	movl	%eax, -148(%rbp)
	addq	$288, %rdi
	leaq	16+_ZTVN2v88internal11interpreter15TryCatchBuilderE(%rip), %rax
	movq	%rax, -112(%rbp)
	movl	%r14d, 584(%rdi)
	call	_ZN2v88internal11interpreter19HandlerTableBuilder15NewHandlerEntryEv@PLT
	movq	%r13, %xmm0
	movl	328(%r15), %r13d
	movq	%rbx, %xmm1
	movl	%eax, -96(%rbp)
	movq	336(%r15), %rdi
	punpcklqdq	%xmm1, %xmm0
	leal	1(%r13), %eax
	cmpl	%eax, 332(%r15)
	movl	%r14d, -92(%rbp)
	movl	%eax, 328(%r15)
	cmovge	332(%r15), %eax
	movb	$0, -88(%rbp)
	movq	$-1, -80(%rbp)
	movl	%eax, 332(%r15)
	movups	%xmm0, -72(%rbp)
	testq	%rdi, %rdi
	je	.L3411
	movq	(%rdi), %rax
	movl	%r13d, %esi
	call	*16(%rax)
.L3411:
	call	_ZN2v88internal11interpreter8Register15current_contextEv@PLT
	leaq	-112(%rbp), %r14
	movl	%r13d, %edx
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder12MoveRegisterENS1_8RegisterES3_@PLT
	movl	%r13d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter15TryCatchBuilder8BeginTryENS1_8RegisterE@PLT
	leaq	-144(%rbp), %rcx
	cmpb	$0, 8(%r15)
	movq	800(%r15), %rax
	movq	808(%r15), %rdx
	movq	%rcx, -160(%rbp)
	movq	%rcx, 800(%r15)
	leaq	16+_ZTVN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTryCatchE(%rip), %rcx
	movq	%rdx, -120(%rbp)
	movq	%r15, %rdx
	movq	%r15, -136(%rbp)
	movq	%rax, -128(%rbp)
	movq	%rcx, -144(%rbp)
	je	.L3427
.L3402:
	movl	-148(%rbp), %ecx
	movq	%r14, %rdi
	movl	%ecx, 872(%r15)
	movq	%rax, 800(%rdx)
	call	_ZN2v88internal11interpreter15TryCatchBuilder6EndTryEv@PLT
	movq	16(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L3404
	movq	816(%r15), %rax
	movl	328(%r15), %esi
	movq	%r15, -136(%rbp)
	movq	336(%r15), %rdi
	movq	$2, -120(%rbp)
	movq	%rax, -144(%rbp)
	movq	-160(%rbp), %rax
	movl	%esi, -128(%rbp)
	movq	%rax, 816(%r15)
	leal	1(%rsi), %eax
	cmpl	%eax, 332(%r15)
	movl	%eax, 328(%r15)
	cmovge	332(%r15), %eax
	movl	%eax, 332(%r15)
	testq	%rdi, %rdi
	je	.L3405
	movq	(%rdi), %rax
	movq	%rdx, -168(%rbp)
	movl	%esi, -160(%rbp)
	call	*16(%rax)
	movq	-168(%rbp), %rdx
	movl	-160(%rbp), %esi
.L3405:
	movq	%r12, %rdi
	movq	%rdx, -168(%rbp)
	movl	%esi, -160(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-168(%rbp), %rdx
	movl	-160(%rbp), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder18CreateCatchContextENS1_8RegisterEPKNS0_5ScopeE@PLT
	movq	-136(%rbp), %rdx
	movq	-144(%rbp), %rax
	movq	336(%rdx), %rdi
	movq	%rax, 816(%rdx)
	movl	-128(%rbp), %eax
	movl	328(%rdx), %esi
	movl	%eax, 328(%rdx)
	testq	%rdi, %rdi
	je	.L3406
	subl	%eax, %esi
	movq	(%rdi), %rdx
	movq	%rsi, %rcx
	movl	%eax, %esi
	salq	$32, %rcx
	orq	%rcx, %rsi
	call	*32(%rdx)
.L3406:
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
.L3404:
	movl	-148(%rbp), %eax
	orl	32(%rbx), %eax
	je	.L3407
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadTheHoleEv@PLT
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder17SetPendingMessageEv@PLT
.L3407:
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	movq	16(%rbx), %rdx
	movq	24(%rbx), %rsi
	movq	%r15, %rdi
	testq	%rdx, %rdx
	je	.L3408
	call	_ZN2v88internal11interpreter17BytecodeGenerator12VisitInScopeEPNS0_9StatementEPNS0_5ScopeE
.L3409:
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter15TryCatchBuilder8EndCatchEv@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter15TryCatchBuilderD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3428
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3427:
	.cfi_restore_state
	movq	8(%rbx), %rsi
	movq	%rsi, -168(%rbp)
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r15), %rax
	movq	-168(%rbp), %rsi
	jb	.L3429
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movq	-136(%rbp), %rdx
	movq	-128(%rbp), %rax
	jmp	.L3402
	.p2align 4,,10
	.p2align 3
.L3408:
	call	_ZN2v88internal11interpreter17BytecodeGenerator10VisitBlockEPNS0_5BlockE
	jmp	.L3409
	.p2align 4,,10
	.p2align 3
.L3429:
	movq	-136(%rbp), %rdx
	movq	-128(%rbp), %rax
	movb	$1, 8(%r15)
	jmp	.L3402
.L3428:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20779:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator22VisitTryCatchStatementEPNS0_17TryCatchStatementE, .-_ZN2v88internal11interpreter17BytecodeGenerator22VisitTryCatchStatementEPNS0_17TryCatchStatementE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator24VisitNaryCommaExpressionEPNS0_13NaryOperationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator24VisitNaryCommaExpressionEPNS0_13NaryOperationE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator24VisitNaryCommaExpressionEPNS0_13NaryOperationE, @function
_ZN2v88internal11interpreter17BytecodeGenerator24VisitNaryCommaExpressionEPNS0_13NaryOperationE:
.LFB20984:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	8(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter17BytecodeGenerator14VisitForEffectEPNS0_10ExpressionE
	movq	24(%r14), %rdx
	movq	32(%r14), %rax
	subq	%rdx, %rax
	cmpq	$16, %rax
	je	.L3431
	xorl	%r13d, %r13d
	leaq	-96(%rbp), %rbx
	jmp	.L3437
	.p2align 4,,10
	.p2align 3
.L3448:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L3447
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator25VisitNoStackOverflowCheckEPNS0_7AstNodeE
.L3446:
	movq	-88(%rbp), %rdx
	movl	-80(%rbp), %eax
	movq	-96(%rbp), %rcx
	movl	328(%rdx), %esi
	subl	%eax, %esi
.L3432:
	movq	336(%rdx), %rdi
	movq	%rcx, 816(%rdx)
	movl	%eax, 328(%rdx)
	testq	%rdi, %rdi
	je	.L3434
	movq	%rsi, %rdx
	movq	(%rdi), %rcx
	movl	%eax, %esi
	salq	$32, %rdx
	orq	%rdx, %rsi
	call	*32(%rcx)
.L3434:
	movq	24(%r14), %rdx
	movq	32(%r14), %rax
	addq	$1, %r13
	subq	%rdx, %rax
	movq	%rax, %rcx
	sarq	$4, %rcx
	subq	$1, %rcx
	cmpq	%r13, %rcx
	jbe	.L3431
.L3437:
	movq	%r13, %rax
	movq	816(%r12), %rcx
	salq	$4, %rax
	cmpb	$0, 8(%r12)
	movq	(%rdx,%rax), %r15
	movl	328(%r12), %eax
	movq	%rcx, -96(%rbp)
	movq	%r12, -88(%rbp)
	movl	%eax, -80(%rbp)
	movq	$1, -72(%rbp)
	movq	%rbx, 816(%r12)
	je	.L3448
	movq	%r12, %rdx
	xorl	%esi, %esi
	jmp	.L3432
	.p2align 4,,10
	.p2align 3
.L3447:
	movb	$1, 8(%r12)
	jmp	.L3446
	.p2align 4,,10
	.p2align 3
.L3431:
	cmpb	$0, 8(%r12)
	je	.L3449
.L3430:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3450
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3449:
	.cfi_restore_state
	movq	-16(%rdx,%rax), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L3451
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	jmp	.L3430
	.p2align 4,,10
	.p2align 3
.L3451:
	movb	$1, 8(%r12)
	jmp	.L3430
.L3450:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20984:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator24VisitNaryCommaExpressionEPNS0_13NaryOperationE, .-_ZN2v88internal11interpreter17BytecodeGenerator24VisitNaryCommaExpressionEPNS0_13NaryOperationE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator18VisitIterationBodyEPNS0_18IterationStatementEPNS1_11LoopBuilderE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator18VisitIterationBodyEPNS0_18IterationStatementEPNS1_11LoopBuilderE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator18VisitIterationBodyEPNS0_18IterationStatementEPNS1_11LoopBuilderE, @function
_ZN2v88internal11interpreter17BytecodeGenerator18VisitIterationBodyEPNS0_18IterationStatementEPNS1_11LoopBuilderE:
.LFB20768:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%rdx, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter11LoopBuilder8LoopBodyEv@PLT
	leaq	24(%r12), %rdi
	movq	%r12, -88(%rbp)
	movq	800(%r12), %rax
	addl	$1, 868(%r12)
	movq	%rax, -80(%rbp)
	movq	808(%r12), %rax
	movq	%rbx, -64(%rbp)
	movq	%rax, -72(%rbp)
	leaq	-96(%rbp), %rax
	movq	%rax, 800(%r12)
	movl	(%rbx), %esi
	leaq	16+_ZTVN2v88internal11interpreter17BytecodeGenerator24ControlScopeForIterationE(%rip), %rax
	movq	%rax, -96(%rbp)
	movq	%r13, -56(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder10StackCheckEi@PLT
	cmpb	$0, 8(%r12)
	je	.L3457
.L3453:
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter11LoopBuilder18BindContinueTargetEv@PLT
	movq	-88(%rbp), %rax
	movq	-80(%rbp), %rdx
	subl	$1, 868(%rax)
	movq	%rdx, 800(%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3458
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3457:
	.cfi_restore_state
	movq	24(%rbx), %r14
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L3459
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	jmp	.L3453
	.p2align 4,,10
	.p2align 3
.L3459:
	movb	$1, 8(%r12)
	jmp	.L3453
.L3458:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20768:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator18VisitIterationBodyEPNS0_18IterationStatementEPNS1_11LoopBuilderE, .-_ZN2v88internal11interpreter17BytecodeGenerator18VisitIterationBodyEPNS0_18IterationStatementEPNS1_11LoopBuilderE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator12VisitForTestEPNS0_10ExpressionEPNS1_14BytecodeLabelsES6_NS2_15TestFallthroughE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator12VisitForTestEPNS0_10ExpressionEPNS1_14BytecodeLabelsES6_NS2_15TestFallthroughE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator12VisitForTestEPNS0_10ExpressionEPNS1_14BytecodeLabelsES6_NS2_15TestFallthroughE, @function
_ZN2v88internal11interpreter17BytecodeGenerator12VisitForTestEPNS0_10ExpressionEPNS1_14BytecodeLabelsES6_NS2_15TestFallthroughE:
.LFB21025:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	leaq	-112(%rbp), %rdx
	pushq	%r13
	movq	%r14, %xmm0
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	%rcx, -120(%rbp)
	movq	816(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	328(%rbx), %eax
	movhps	-120(%rbp), %xmm0
	cmpb	$0, 8(%rbx)
	movq	%rdi, -112(%rbp)
	movq	%rbx, -104(%rbp)
	movl	%eax, -96(%rbp)
	movq	%rdx, 816(%rbx)
	movq	$3, -88(%rbp)
	movb	$0, -80(%rbp)
	movl	%r8d, -76(%rbp)
	movups	%xmm0, -72(%rbp)
	je	.L3474
	movl	%r8d, %r12d
	movq	%rbx, %rdx
	xorl	%esi, %esi
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
.L3461:
	movq	%rdi, 816(%rdx)
	movq	336(%rdx), %rdi
	movl	%eax, 328(%rdx)
	testq	%rdi, %rdi
	je	.L3463
	movq	%rsi, %r8
	movq	(%rdi), %rdx
	movl	%eax, %esi
	salq	$32, %r8
	orq	%r8, %rsi
	call	*32(%rdx)
.L3463:
	testb	%r15b, %r15b
	jne	.L3460
	xorl	%r15d, %r15d
	cmpl	$1, %r13d
	sete	%r15b
	cmpl	$1, %r12d
	je	.L3465
	cmpl	$2, %r12d
	je	.L3466
	testl	%r12d, %r12d
	je	.L3475
.L3460:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3476
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3474:
	.cfi_restore_state
	movq	%rsi, %r15
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%rbx), %rax
	jb	.L3477
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator25VisitNoStackOverflowCheckEPNS0_7AstNodeE
.L3473:
	movq	-64(%rbp), %rax
	movq	-104(%rbp), %rdx
	movl	-84(%rbp), %r13d
	movzbl	-80(%rbp), %r15d
	movq	-72(%rbp), %r14
	movl	-76(%rbp), %r12d
	movq	%rax, -120(%rbp)
	movl	-96(%rbp), %eax
	movl	328(%rdx), %esi
	movq	-112(%rbp), %rdi
	subl	%eax, %esi
	jmp	.L3461
	.p2align 4,,10
	.p2align 3
.L3475:
	movq	-120(%rbp), %rdi
	call	_ZN2v88internal11interpreter14BytecodeLabels3NewEv@PLT
	leaq	24(%rbx), %rdi
	movl	%r15d, %esi
	movq	%rax, %rdx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11JumpIfFalseENS2_13ToBooleanModeEPNS1_13BytecodeLabelE@PLT
	jmp	.L3460
	.p2align 4,,10
	.p2align 3
.L3465:
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter14BytecodeLabels3NewEv@PLT
	leaq	24(%rbx), %rdi
	movl	%r15d, %esi
	movq	%rax, %rdx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder10JumpIfTrueENS2_13ToBooleanModeEPNS1_13BytecodeLabelE@PLT
	jmp	.L3460
	.p2align 4,,10
	.p2align 3
.L3477:
	movb	$1, 8(%rbx)
	jmp	.L3473
	.p2align 4,,10
	.p2align 3
.L3466:
	movq	%r14, %rdi
	leaq	24(%rbx), %r12
	call	_ZN2v88internal11interpreter14BytecodeLabels3NewEv@PLT
	movl	%r15d, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder10JumpIfTrueENS2_13ToBooleanModeEPNS1_13BytecodeLabelE@PLT
	movq	-120(%rbp), %rdi
	call	_ZN2v88internal11interpreter14BytecodeLabels3NewEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4JumpEPNS1_13BytecodeLabelE@PLT
	jmp	.L3460
.L3476:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21025:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator12VisitForTestEPNS0_10ExpressionEPNS1_14BytecodeLabelsES6_NS2_15TestFallthroughE, .-_ZN2v88internal11interpreter17BytecodeGenerator12VisitForTestEPNS0_10ExpressionEPNS1_14BytecodeLabelsES6_NS2_15TestFallthroughE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator21VisitDoWhileStatementEPNS0_16DoWhileStatementE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator21VisitDoWhileStatementEPNS0_16DoWhileStatementE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator21VisitDoWhileStatementEPNS0_16DoWhileStatementE, @function
_ZN2v88internal11interpreter17BytecodeGenerator21VisitDoWhileStatementEPNS0_16DoWhileStatementE:
.LFB20769:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal11interpreter11LoopBuilderE(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	24(%rdi), %r15
	pushq	%r14
	leaq	-168(%rbp), %rdx
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 3, -56
	movq	568(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	movq	%r15, -184(%rbp)
	movq	%rdx, -160(%rbp)
	movq	%rax, -176(%rbp)
	movq	%rax, -112(%rbp)
	leaq	-104(%rbp), %rax
	movq	%rdx, -168(%rbp)
	movq	$0, -152(%rbp)
	movb	$0, -144(%rbp)
	movq	%rsi, -136(%rbp)
	movq	%r14, -128(%rbp)
	movq	%rcx, -192(%rbp)
	movq	$-1, -120(%rbp)
	movq	%rax, -96(%rbp)
	movq	%rax, -104(%rbp)
	movq	$0, -88(%rbp)
	movb	$0, -80(%rbp)
	testq	%r14, %r14
	je	.L3479
	movq	40(%r14), %rax
	leaq	16(%rax), %rcx
	movq	24(%rax), %rax
	movq	%rcx, %rdx
	testq	%rax, %rax
	jne	.L3482
	.p2align 4,,10
	.p2align 3
.L3486:
	movl	$-1, %ebx
.L3481:
	movl	%ebx, -72(%rbp)
.L3479:
	movq	32(%r12), %rdi
	call	_ZNK2v88internal10Expression16ToBooleanIsFalseEv@PLT
	testb	%al, %al
	je	.L3487
	leaq	-192(%rbp), %r14
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%r14, %rdx
	call	_ZN2v88internal11interpreter17BytecodeGenerator18VisitIterationBodyEPNS0_18IterationStatementEPNS1_11LoopBuilderE
.L3488:
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter11LoopBuilderD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3506
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3507:
	.cfi_restore_state
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L3483
.L3482:
	cmpq	%r12, 32(%rax)
	jnb	.L3507
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L3482
.L3483:
	cmpq	%rdx, %rcx
	je	.L3486
	cmpq	%r12, 32(%rdx)
	ja	.L3486
	movq	40(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L3486
	movq	(%rdi), %rax
	xorl	%esi, %esi
	call	*16(%rax)
	movq	%rax, -240(%rbp)
	cmpl	$-1, %eax
	je	.L3486
	movq	16(%r14), %rbx
	leaq	-240(%rbp), %rsi
	subq	8(%r14), %rbx
	movq	%r14, %rdi
	sarq	$3, %rbx
	call	_ZNSt6vectorIN2v88internal11SourceRangeENS1_13ZoneAllocatorIS2_EEE12emplace_backIJRS2_EEEvDpOT_
	jmp	.L3481
	.p2align 4,,10
	.p2align 3
.L3487:
	movq	32(%r12), %rdi
	leaq	-192(%rbp), %r14
	call	_ZNK2v88internal10Expression15ToBooleanIsTrueEv@PLT
	movq	%r14, %rdi
	testb	%al, %al
	je	.L3489
	call	_ZN2v88internal11interpreter11LoopBuilder10LoopHeaderEv@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%r14, %rdx
	call	_ZN2v88internal11interpreter17BytecodeGenerator18VisitIterationBodyEPNS0_18IterationStatementEPNS1_11LoopBuilderE
	movl	868(%r13), %esi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter11LoopBuilder12JumpToHeaderEi@PLT
	jmp	.L3488
	.p2align 4,,10
	.p2align 3
.L3489:
	call	_ZN2v88internal11interpreter11LoopBuilder10LoopHeaderEv@PLT
	movq	%r12, %rsi
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator18VisitIterationBodyEPNS0_18IterationStatementEPNS1_11LoopBuilderE
	movq	32(%r12), %rsi
	movl	(%rsi), %eax
	cmpl	$-1, %eax
	je	.L3490
	movb	$2, 496(%r13)
	movl	%eax, 500(%r13)
	movq	32(%r12), %rsi
.L3490:
	movq	16(%r13), %rax
	leaq	-240(%rbp), %r12
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	leaq	-176(%rbp), %rcx
	movq	%r12, %rdx
	leaq	-232(%rbp), %rbx
	movq	$0, -216(%rbp)
	movq	%rax, -240(%rbp)
	movq	%rbx, -224(%rbp)
	movq	%rbx, -232(%rbp)
	movb	$0, -208(%rbp)
	call	_ZN2v88internal11interpreter17BytecodeGenerator12VisitForTestEPNS0_10ExpressionEPNS1_14BytecodeLabelsES6_NS2_15TestFallthroughE
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter14BytecodeLabels4BindEPNS1_20BytecodeArrayBuilderE@PLT
	movl	868(%r13), %esi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter11LoopBuilder12JumpToHeaderEi@PLT
	movq	-232(%rbp), %rax
	cmpq	%rbx, %rax
	je	.L3488
	.p2align 4,,10
	.p2align 3
.L3492:
	movq	(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L3492
	jmp	.L3488
.L3506:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20769:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator21VisitDoWhileStatementEPNS0_16DoWhileStatementE, .-_ZN2v88internal11interpreter17BytecodeGenerator21VisitDoWhileStatementEPNS0_16DoWhileStatementE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator19VisitWhileStatementEPNS0_14WhileStatementE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator19VisitWhileStatementEPNS0_14WhileStatementE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator19VisitWhileStatementEPNS0_14WhileStatementE, @function
_ZN2v88internal11interpreter17BytecodeGenerator19VisitWhileStatementEPNS0_14WhileStatementE:
.LFB20770:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal11interpreter11LoopBuilderE(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	leaq	-168(%rbp), %rdx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	24(%rdi), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$216, %rsp
	movq	568(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	movq	%r14, -184(%rbp)
	movq	%rdx, -160(%rbp)
	movq	%rax, -176(%rbp)
	movq	%rax, -112(%rbp)
	leaq	-104(%rbp), %rax
	movq	%rdx, -168(%rbp)
	movq	$0, -152(%rbp)
	movb	$0, -144(%rbp)
	movq	%rsi, -136(%rbp)
	movq	%r13, -128(%rbp)
	movq	%rcx, -192(%rbp)
	movq	$-1, -120(%rbp)
	movq	%rax, -96(%rbp)
	movq	%rax, -104(%rbp)
	movq	$0, -88(%rbp)
	movb	$0, -80(%rbp)
	testq	%r13, %r13
	je	.L3509
	movq	40(%r13), %rax
	leaq	16(%rax), %rcx
	movq	24(%rax), %rax
	movq	%rcx, %rdx
	testq	%rax, %rax
	jne	.L3512
	.p2align 4,,10
	.p2align 3
.L3516:
	movl	$-1, %r15d
.L3511:
	movl	%r15d, -72(%rbp)
.L3509:
	movq	32(%r12), %rdi
	call	_ZNK2v88internal10Expression16ToBooleanIsFalseEv@PLT
	testb	%al, %al
	jne	.L3536
	leaq	-192(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter11LoopBuilder10LoopHeaderEv@PLT
	movq	32(%r12), %rdi
	call	_ZNK2v88internal10Expression15ToBooleanIsTrueEv@PLT
	testb	%al, %al
	jne	.L3519
	movq	32(%r12), %rsi
	movl	(%rsi), %eax
	cmpl	$-1, %eax
	je	.L3520
	movb	$2, 496(%rbx)
	movl	%eax, 500(%rbx)
	movq	32(%r12), %rsi
.L3520:
	movq	16(%rbx), %rax
	xorl	%r8d, %r8d
	movq	%rbx, %rdi
	leaq	-240(%rbp), %r9
	movq	%r9, %rdx
	leaq	-232(%rbp), %r13
	leaq	-176(%rbp), %rcx
	movq	%r9, -248(%rbp)
	movq	%rax, -240(%rbp)
	movq	%r13, -224(%rbp)
	movq	%r13, -232(%rbp)
	movq	$0, -216(%rbp)
	movb	$0, -208(%rbp)
	call	_ZN2v88internal11interpreter17BytecodeGenerator12VisitForTestEPNS0_10ExpressionEPNS1_14BytecodeLabelsES6_NS2_15TestFallthroughE
	movq	-248(%rbp), %r9
	movq	%r14, %rsi
	movq	%r9, %rdi
	call	_ZN2v88internal11interpreter14BytecodeLabels4BindEPNS1_20BytecodeArrayBuilderE@PLT
	movq	-232(%rbp), %rax
	cmpq	%r13, %rax
	je	.L3519
	.p2align 4,,10
	.p2align 3
.L3522:
	movq	(%rax), %rax
	cmpq	%r13, %rax
	jne	.L3522
.L3519:
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator18VisitIterationBodyEPNS0_18IterationStatementEPNS1_11LoopBuilderE
	movl	868(%rbx), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter11LoopBuilder12JumpToHeaderEi@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter11LoopBuilderD1Ev@PLT
.L3508:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3537
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3538:
	.cfi_restore_state
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L3513
.L3512:
	cmpq	%r12, 32(%rax)
	jnb	.L3538
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L3512
.L3513:
	cmpq	%rdx, %rcx
	je	.L3516
	cmpq	%r12, 32(%rdx)
	ja	.L3516
	movq	40(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L3516
	movq	(%rdi), %rax
	xorl	%esi, %esi
	call	*16(%rax)
	movq	%rax, -240(%rbp)
	cmpl	$-1, %eax
	je	.L3516
	movq	16(%r13), %r15
	leaq	-240(%rbp), %rsi
	subq	8(%r13), %r15
	movq	%r13, %rdi
	sarq	$3, %r15
	call	_ZNSt6vectorIN2v88internal11SourceRangeENS1_13ZoneAllocatorIS2_EEE12emplace_backIJRS2_EEEvDpOT_
	jmp	.L3511
	.p2align 4,,10
	.p2align 3
.L3536:
	leaq	-192(%rbp), %rdi
	call	_ZN2v88internal11interpreter11LoopBuilderD1Ev@PLT
	jmp	.L3508
.L3537:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20770:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator19VisitWhileStatementEPNS0_14WhileStatementE, .-_ZN2v88internal11interpreter17BytecodeGenerator19VisitWhileStatementEPNS0_14WhileStatementE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator16VisitConditionalEPNS0_11ConditionalE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator16VisitConditionalEPNS0_11ConditionalE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator16VisitConditionalEPNS0_11ConditionalE, @function
_ZN2v88internal11interpreter17BytecodeGenerator16VisitConditionalEPNS0_11ConditionalE:
.LFB20846:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-208(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	leaq	24(%rdi), %rsi
	movq	%rbx, %rcx
	subq	$184, %rsp
	movq	568(%rdi), %rdx
	movq	%r12, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter29ConditionalControlFlowBuilderC1EPNS1_20BytecodeArrayBuilderEPNS1_20BlockCoverageBuilderEPNS0_7AstNodeE
	movq	8(%rbx), %rdi
	call	_ZNK2v88internal10Expression15ToBooleanIsTrueEv@PLT
	testb	%al, %al
	je	.L3540
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter29ConditionalControlFlowBuilder4ThenEv@PLT
	movq	16(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
.L3541:
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter29ConditionalControlFlowBuilderD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3546
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3540:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	call	_ZNK2v88internal10Expression16ToBooleanIsFalseEv@PLT
	testb	%al, %al
	je	.L3542
.L3545:
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter29ConditionalControlFlowBuilder4ElseEv@PLT
	movq	24(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	jmp	.L3541
	.p2align 4,,10
	.p2align 3
.L3542:
	movq	8(%rbx), %rsi
	leaq	-112(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	leaq	-152(%rbp), %rdx
	call	_ZN2v88internal11interpreter17BytecodeGenerator12VisitForTestEPNS0_10ExpressionEPNS1_14BytecodeLabelsES6_NS2_15TestFallthroughE
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter29ConditionalControlFlowBuilder4ThenEv@PLT
	movq	16(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter29ConditionalControlFlowBuilder9JumpToEndEv@PLT
	jmp	.L3545
.L3546:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20846:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator16VisitConditionalEPNS0_11ConditionalE, .-_ZN2v88internal11interpreter17BytecodeGenerator16VisitConditionalEPNS0_11ConditionalE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator29VisitLogicalTestSubExpressionENS0_5Token5ValueEPNS0_10ExpressionEPNS1_14BytecodeLabelsES8_i,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator29VisitLogicalTestSubExpressionENS0_5Token5ValueEPNS0_10ExpressionEPNS1_14BytecodeLabelsES8_i
	.type	_ZN2v88internal11interpreter17BytecodeGenerator29VisitLogicalTestSubExpressionENS0_5Token5ValueEPNS0_10ExpressionEPNS1_14BytecodeLabelsES8_i, @function
_ZN2v88internal11interpreter17BytecodeGenerator29VisitLogicalTestSubExpressionENS0_5Token5ValueEPNS0_10ExpressionEPNS1_14BytecodeLabelsES8_i:
.LFB20985:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r11
	movq	%rcx, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	24(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%r9d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-88(%rbp), %rbx
	subq	$72, %rsp
	movq	%r8, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	%rbx, -80(%rbp)
	movq	%rbx, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	$0, -72(%rbp)
	movb	$0, -64(%rbp)
	cmpb	$32, %sil
	je	.L3561
	cmpb	$33, %sil
	je	.L3562
	movq	%rdx, %rsi
	movq	%rcx, -112(%rbp)
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	movq	-112(%rbp), %r10
	cmpl	$1, %eax
	movl	%eax, %esi
	je	.L3551
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter14BytecodeLabels3NewEv@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder21JumpIfUndefinedOrNullEPNS1_13BytecodeLabelE@PLT
	movq	-112(%rbp), %r10
	xorl	%esi, %esi
.L3551:
	movq	%r10, %rdi
	movl	%esi, -112(%rbp)
	call	_ZN2v88internal11interpreter14BytecodeLabels3NewEv@PLT
	movl	-112(%rbp), %esi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder10JumpIfTrueENS2_13ToBooleanModeEPNS1_13BytecodeLabelE@PLT
	movq	-104(%rbp), %rdi
	call	_ZN2v88internal11interpreter14BytecodeLabels3NewEv@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4JumpEPNS1_13BytecodeLabelE@PLT
.L3549:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter14BytecodeLabels4BindEPNS1_20BytecodeArrayBuilderE@PLT
	movq	568(%r12), %rax
	testq	%rax, %rax
	je	.L3552
	cmpl	$-1, %r13d
	je	.L3552
	movq	32(%rax), %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder15IncBlockCounterEi@PLT
.L3552:
	movq	-88(%rbp), %rax
	cmpq	%rbx, %rax
	je	.L3547
	.p2align 4,,10
	.p2align 3
.L3554:
	movq	(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L3554
.L3547:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3563
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3562:
	.cfi_restore_state
	movq	%r8, %rcx
	movq	%r14, %rdx
	xorl	%r8d, %r8d
	movq	%r11, %rsi
	call	_ZN2v88internal11interpreter17BytecodeGenerator12VisitForTestEPNS0_10ExpressionEPNS1_14BytecodeLabelsES6_NS2_15TestFallthroughE
	jmp	.L3549
	.p2align 4,,10
	.p2align 3
.L3561:
	movl	$1, %r8d
	movq	%r14, %rcx
	movq	%r10, %rdx
	movq	%r11, %rsi
	call	_ZN2v88internal11interpreter17BytecodeGenerator12VisitForTestEPNS0_10ExpressionEPNS1_14BytecodeLabelsES6_NS2_15TestFallthroughE
	jmp	.L3549
.L3563:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20985:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator29VisitLogicalTestSubExpressionENS0_5Token5ValueEPNS0_10ExpressionEPNS1_14BytecodeLabelsES8_i, .-_ZN2v88internal11interpreter17BytecodeGenerator29VisitLogicalTestSubExpressionENS0_5Token5ValueEPNS0_10ExpressionEPNS1_14BytecodeLabelsES8_i
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator16VisitLogicalTestENS0_5Token5ValueEPNS0_10ExpressionES6_i,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator16VisitLogicalTestENS0_5Token5ValueEPNS0_10ExpressionES6_i
	.type	_ZN2v88internal11interpreter17BytecodeGenerator16VisitLogicalTestENS0_5Token5ValueEPNS0_10ExpressionES6_i, @function
_ZN2v88internal11interpreter17BytecodeGenerator16VisitLogicalTestENS0_5Token5ValueEPNS0_10ExpressionES6_i:
.LFB20986:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzbl	%sil, %esi
	movl	%r8d, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	816(%rdi), %rax
	movq	40(%rax), %r14
	movq	48(%rax), %r15
	movl	36(%rax), %ebx
	movq	%r15, %r8
	movq	%r14, %rcx
	call	_ZN2v88internal11interpreter17BytecodeGenerator29VisitLogicalTestSubExpressionENS0_5Token5ValueEPNS0_10ExpressionEPNS1_14BytecodeLabelsES8_i
	addq	$8, %rsp
	movl	%ebx, %r8d
	movq	%r15, %rcx
	popq	%rbx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator12VisitForTestEPNS0_10ExpressionEPNS1_14BytecodeLabelsES6_NS2_15TestFallthroughE
	.cfi_endproc
.LFE20986:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator16VisitLogicalTestENS0_5Token5ValueEPNS0_10ExpressionES6_i, .-_ZN2v88internal11interpreter17BytecodeGenerator16VisitLogicalTestENS0_5Token5ValueEPNS0_10ExpressionES6_i
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator20VisitNaryLogicalTestENS0_5Token5ValueEPNS0_13NaryOperationEPKNS2_21NaryCodeCoverageSlotsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator20VisitNaryLogicalTestENS0_5Token5ValueEPNS0_13NaryOperationEPKNS2_21NaryCodeCoverageSlotsE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator20VisitNaryLogicalTestENS0_5Token5ValueEPNS0_13NaryOperationEPKNS2_21NaryCodeCoverageSlotsE, @function
_ZN2v88internal11interpreter17BytecodeGenerator20VisitNaryLogicalTestENS0_5Token5ValueEPNS0_13NaryOperationEPKNS2_21NaryCodeCoverageSlotsE:
.LFB20987:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	816(%rdi), %rax
	movq	48(%rax), %rcx
	movq	40(%rax), %r12
	movl	36(%rax), %eax
	movq	%rcx, -56(%rbp)
	movl	%eax, -64(%rbp)
	movq	0(%r13), %rax
	cmpq	$0, 568(%rax)
	je	.L3573
	movq	8(%r13), %rax
	movq	%rcx, %r8
	movl	(%rax), %r9d
.L3567:
	movzbl	%sil, %eax
	movq	8(%r15), %rdx
	movq	%r12, %rcx
	movq	%r14, %rdi
	movl	%eax, %esi
	movl	%eax, -60(%rbp)
	call	_ZN2v88internal11interpreter17BytecodeGenerator29VisitLogicalTestSubExpressionENS0_5Token5ValueEPNS0_10ExpressionEPNS1_14BytecodeLabelsES8_i
	movq	24(%r15), %rsi
	movq	32(%r15), %rax
	subq	%rsi, %rax
	cmpq	$16, %rax
	je	.L3568
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L3572:
	movq	0(%r13), %rax
	addq	$1, %rbx
	cmpq	$0, 568(%rax)
	je	.L3569
	movq	8(%r13), %rdx
	movq	%rbx, %rax
	movq	-56(%rbp), %r8
	movq	%r12, %rcx
	salq	$4, %rax
	movq	%r14, %rdi
	movl	(%rdx,%rbx,4), %r9d
	movq	-16(%rsi,%rax), %rdx
	movl	-60(%rbp), %esi
	call	_ZN2v88internal11interpreter17BytecodeGenerator29VisitLogicalTestSubExpressionENS0_5Token5ValueEPNS0_10ExpressionEPNS1_14BytecodeLabelsES8_i
	movq	24(%r15), %rsi
	movq	32(%r15), %rax
	subq	%rsi, %rax
	movq	%rax, %rdx
	sarq	$4, %rdx
	subq	$1, %rdx
	cmpq	%rdx, %rbx
	jb	.L3572
.L3568:
	movl	-64(%rbp), %r8d
	movq	-56(%rbp), %rcx
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	-16(%rsi,%rax), %rsi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator12VisitForTestEPNS0_10ExpressionEPNS1_14BytecodeLabelsES6_NS2_15TestFallthroughE
.L3569:
	.cfi_restore_state
	movq	%rbx, %rax
	movq	-56(%rbp), %r8
	movl	$-1, %r9d
	movq	%r12, %rcx
	salq	$4, %rax
	movq	%r14, %rdi
	movq	-16(%rsi,%rax), %rdx
	movl	-60(%rbp), %esi
	call	_ZN2v88internal11interpreter17BytecodeGenerator29VisitLogicalTestSubExpressionENS0_5Token5ValueEPNS0_10ExpressionEPNS1_14BytecodeLabelsES8_i
	movq	24(%r15), %rsi
	movq	32(%r15), %rax
	subq	%rsi, %rax
	movq	%rax, %rdx
	sarq	$4, %rdx
	subq	$1, %rdx
	cmpq	%rbx, %rdx
	ja	.L3572
	jmp	.L3568
.L3573:
	movl	$-1, %r9d
	movq	%rcx, %r8
	jmp	.L3567
	.cfi_endproc
.LFE20987:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator20VisitNaryLogicalTestENS0_5Token5ValueEPNS0_13NaryOperationEPKNS2_21NaryCodeCoverageSlotsE, .-_ZN2v88internal11interpreter17BytecodeGenerator20VisitNaryLogicalTestENS0_5Token5ValueEPNS0_13NaryOperationEPKNS2_21NaryCodeCoverageSlotsE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator28VisitNaryLogicalOrExpressionEPNS0_13NaryOperationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator28VisitNaryLogicalOrExpressionEPNS0_13NaryOperationE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator28VisitNaryLogicalOrExpressionEPNS0_13NaryOperationE, @function
_ZN2v88internal11interpreter17BytecodeGenerator28VisitNaryLogicalOrExpressionEPNS0_13NaryOperationE:
.LFB20992:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -128(%rbp)
	movq	8(%rsi), %rax
	movq	568(%rdi), %rdi
	movq	$0, -120(%rbp)
	movq	%rax, -136(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	testq	%rdi, %rdi
	je	.L3579
	movq	24(%rsi), %rax
	cmpq	%rax, 32(%rsi)
	je	.L3579
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L3590:
	testq	%rdi, %rdi
	je	.L3609
	movq	40(%rdi), %rax
	leaq	16(%rax), %rcx
	movq	24(%rax), %rax
	movq	%rcx, %rdx
	testq	%rax, %rax
	jne	.L3583
	jmp	.L3609
	.p2align 4,,10
	.p2align 3
.L3633:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L3584
.L3583:
	cmpq	%r14, 32(%rax)
	jnb	.L3633
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L3583
.L3584:
	cmpq	%rdx, %rcx
	je	.L3609
	cmpq	%r14, 32(%rdx)
	jbe	.L3634
.L3609:
	movl	$-1, %ebx
	movq	-112(%rbp), %rsi
	movl	%ebx, -96(%rbp)
	cmpq	-104(%rbp), %rsi
	je	.L3588
.L3635:
	movl	%ebx, (%rsi)
	addq	$4, -112(%rbp)
.L3627:
	movq	32(%r14), %rax
	subq	24(%r14), %rax
	addq	$1, %r15
	sarq	$4, %rax
	cmpq	%r15, %rax
	jbe	.L3579
	movq	-128(%rbp), %rax
	movq	568(%rax), %rdi
	jmp	.L3590
	.p2align 4,,10
	.p2align 3
.L3634:
	movq	40(%rdx), %rax
	testq	%rax, %rax
	je	.L3609
	movq	16(%rax), %rax
	leaq	(%rax,%r15,8), %rax
	movl	(%rax), %edx
	movq	(%rax), %rax
	movq	%rax, -96(%rbp)
	cmpl	$-1, %edx
	je	.L3609
	movq	16(%rdi), %rbx
	subq	8(%rdi), %rbx
	leaq	-96(%rbp), %rsi
	call	_ZNSt6vectorIN2v88internal11SourceRangeENS1_13ZoneAllocatorIS2_EEE12emplace_backIJRS2_EEEvDpOT_
	sarq	$3, %rbx
	movq	-112(%rbp), %rsi
	movl	%ebx, -96(%rbp)
	cmpq	-104(%rbp), %rsi
	jne	.L3635
.L3588:
	leaq	-96(%rbp), %rdx
	leaq	-120(%rbp), %rdi
	call	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	jmp	.L3627
	.p2align 4,,10
	.p2align 3
.L3579:
	movq	816(%r12), %rbx
	cmpl	$3, 24(%rbx)
	je	.L3636
	movq	16(%r12), %rax
	leaq	-88(%rbp), %r15
	movq	$0, -72(%rbp)
	movq	%r15, -80(%rbp)
	movq	%rax, -96(%rbp)
	movq	-128(%rbp), %rax
	movq	%r15, -88(%rbp)
	movb	$0, -64(%rbp)
	cmpq	$0, 568(%rax)
	je	.L3610
	movq	-120(%rbp), %rax
	movl	(%rax), %ecx
.L3595:
	leaq	-96(%rbp), %r13
	movq	-136(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r13, %rdx
	call	_ZN2v88internal11interpreter17BytecodeGenerator27VisitLogicalOrSubExpressionEPNS0_10ExpressionEPNS1_14BytecodeLabelsEi
	testb	%al, %al
	jne	.L3596
	xorl	%ebx, %ebx
	jmp	.L3600
	.p2align 4,,10
	.p2align 3
.L3637:
	movq	-128(%rbp), %rax
	addq	$1, %rbx
	cmpq	$0, 568(%rax)
	je	.L3598
	movq	-120(%rbp), %rax
	movl	(%rax,%rbx,4), %ecx
	movq	%rbx, %rax
	salq	$4, %rax
.L3629:
	movq	-16(%rsi,%rax), %rsi
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator27VisitLogicalOrSubExpressionEPNS0_10ExpressionEPNS1_14BytecodeLabelsEi
	testb	%al, %al
	jne	.L3596
.L3600:
	movq	24(%r14), %rsi
	movq	32(%r14), %rax
	subq	%rsi, %rax
	movq	%rax, %rdx
	sarq	$4, %rdx
	subq	$1, %rdx
	cmpq	%rbx, %rdx
	ja	.L3637
	movq	-16(%rsi,%rax), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	leaq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter14BytecodeLabels4BindEPNS1_20BytecodeArrayBuilderE@PLT
	movq	-88(%rbp), %rax
	cmpq	%r15, %rax
	je	.L3594
	.p2align 4,,10
	.p2align 3
.L3602:
	movq	(%rax), %rax
	cmpq	%r15, %rax
	jne	.L3602
.L3594:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3578
	call	_ZdlPv@PLT
.L3578:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3638
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3596:
	.cfi_restore_state
	movq	-88(%rbp), %rax
	cmpq	%r15, %rax
	je	.L3594
	.p2align 4,,10
	.p2align 3
.L3604:
	movq	(%rax), %rax
	cmpq	%r15, %rax
	je	.L3594
	movq	(%rax), %rax
	cmpq	%r15, %rax
	jne	.L3604
	jmp	.L3594
	.p2align 4,,10
	.p2align 3
.L3636:
	movq	-136(%rbp), %rdi
	call	_ZNK2v88internal10Expression15ToBooleanIsTrueEv@PLT
	testb	%al, %al
	jne	.L3639
	leaq	-128(%rbp), %rcx
	movq	%r14, %rdx
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator20VisitNaryLogicalTestENS0_5Token5ValueEPNS0_13NaryOperationEPKNS2_21NaryCodeCoverageSlotsE
.L3593:
	movb	$1, 32(%rbx)
	jmp	.L3594
	.p2align 4,,10
	.p2align 3
.L3639:
	movq	40(%rbx), %rdi
	call	_ZN2v88internal11interpreter14BytecodeLabels3NewEv@PLT
	leaq	24(%r12), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4JumpEPNS1_13BytecodeLabelE@PLT
	jmp	.L3593
.L3598:
	movq	%rbx, %rax
	movl	$-1, %ecx
	salq	$4, %rax
	jmp	.L3629
.L3610:
	movl	$-1, %ecx
	jmp	.L3595
.L3638:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20992:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator28VisitNaryLogicalOrExpressionEPNS0_13NaryOperationE, .-_ZN2v88internal11interpreter17BytecodeGenerator28VisitNaryLogicalOrExpressionEPNS0_13NaryOperationE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator29VisitNaryLogicalAndExpressionEPNS0_13NaryOperationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator29VisitNaryLogicalAndExpressionEPNS0_13NaryOperationE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator29VisitNaryLogicalAndExpressionEPNS0_13NaryOperationE, @function
_ZN2v88internal11interpreter17BytecodeGenerator29VisitNaryLogicalAndExpressionEPNS0_13NaryOperationE:
.LFB20997:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -128(%rbp)
	movq	8(%rsi), %rax
	movq	568(%rdi), %rdi
	movq	$0, -120(%rbp)
	movq	%rax, -136(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	testq	%rdi, %rdi
	je	.L3641
	movq	24(%rsi), %rax
	cmpq	%rax, 32(%rsi)
	je	.L3641
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L3652:
	testq	%rdi, %rdi
	je	.L3671
	movq	40(%rdi), %rax
	leaq	16(%rax), %rcx
	movq	24(%rax), %rax
	movq	%rcx, %rdx
	testq	%rax, %rax
	jne	.L3645
	jmp	.L3671
	.p2align 4,,10
	.p2align 3
.L3695:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L3646
.L3645:
	cmpq	%r14, 32(%rax)
	jnb	.L3695
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L3645
.L3646:
	cmpq	%rdx, %rcx
	je	.L3671
	cmpq	%r14, 32(%rdx)
	jbe	.L3696
.L3671:
	movl	$-1, %ebx
	movq	-112(%rbp), %rsi
	movl	%ebx, -96(%rbp)
	cmpq	-104(%rbp), %rsi
	je	.L3650
.L3697:
	movl	%ebx, (%rsi)
	addq	$4, -112(%rbp)
.L3689:
	movq	32(%r14), %rax
	subq	24(%r14), %rax
	addq	$1, %r15
	sarq	$4, %rax
	cmpq	%r15, %rax
	jbe	.L3641
	movq	-128(%rbp), %rax
	movq	568(%rax), %rdi
	jmp	.L3652
	.p2align 4,,10
	.p2align 3
.L3696:
	movq	40(%rdx), %rax
	testq	%rax, %rax
	je	.L3671
	movq	16(%rax), %rax
	leaq	(%rax,%r15,8), %rax
	movl	(%rax), %edx
	movq	(%rax), %rax
	movq	%rax, -96(%rbp)
	cmpl	$-1, %edx
	je	.L3671
	movq	16(%rdi), %rbx
	subq	8(%rdi), %rbx
	leaq	-96(%rbp), %rsi
	call	_ZNSt6vectorIN2v88internal11SourceRangeENS1_13ZoneAllocatorIS2_EEE12emplace_backIJRS2_EEEvDpOT_
	sarq	$3, %rbx
	movq	-112(%rbp), %rsi
	movl	%ebx, -96(%rbp)
	cmpq	-104(%rbp), %rsi
	jne	.L3697
.L3650:
	leaq	-96(%rbp), %rdx
	leaq	-120(%rbp), %rdi
	call	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	jmp	.L3689
	.p2align 4,,10
	.p2align 3
.L3641:
	movq	816(%r12), %rbx
	cmpl	$3, 24(%rbx)
	je	.L3698
	movq	16(%r12), %rax
	leaq	-88(%rbp), %r15
	movq	$0, -72(%rbp)
	movq	%r15, -80(%rbp)
	movq	%rax, -96(%rbp)
	movq	-128(%rbp), %rax
	movq	%r15, -88(%rbp)
	movb	$0, -64(%rbp)
	cmpq	$0, 568(%rax)
	je	.L3672
	movq	-120(%rbp), %rax
	movl	(%rax), %ecx
.L3657:
	leaq	-96(%rbp), %r13
	movq	-136(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r13, %rdx
	call	_ZN2v88internal11interpreter17BytecodeGenerator28VisitLogicalAndSubExpressionEPNS0_10ExpressionEPNS1_14BytecodeLabelsEi
	testb	%al, %al
	jne	.L3658
	xorl	%ebx, %ebx
	jmp	.L3662
	.p2align 4,,10
	.p2align 3
.L3699:
	movq	-128(%rbp), %rax
	addq	$1, %rbx
	cmpq	$0, 568(%rax)
	je	.L3660
	movq	-120(%rbp), %rax
	movl	(%rax,%rbx,4), %ecx
	movq	%rbx, %rax
	salq	$4, %rax
.L3691:
	movq	-16(%rsi,%rax), %rsi
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator28VisitLogicalAndSubExpressionEPNS0_10ExpressionEPNS1_14BytecodeLabelsEi
	testb	%al, %al
	jne	.L3658
.L3662:
	movq	24(%r14), %rsi
	movq	32(%r14), %rax
	subq	%rsi, %rax
	movq	%rax, %rdx
	sarq	$4, %rdx
	subq	$1, %rdx
	cmpq	%rbx, %rdx
	ja	.L3699
	movq	-16(%rsi,%rax), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	leaq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter14BytecodeLabels4BindEPNS1_20BytecodeArrayBuilderE@PLT
	movq	-88(%rbp), %rax
	cmpq	%r15, %rax
	je	.L3656
	.p2align 4,,10
	.p2align 3
.L3664:
	movq	(%rax), %rax
	cmpq	%r15, %rax
	jne	.L3664
.L3656:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3640
	call	_ZdlPv@PLT
.L3640:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3700
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3658:
	.cfi_restore_state
	movq	-88(%rbp), %rax
	cmpq	%r15, %rax
	je	.L3656
	.p2align 4,,10
	.p2align 3
.L3666:
	movq	(%rax), %rax
	cmpq	%r15, %rax
	je	.L3656
	movq	(%rax), %rax
	cmpq	%r15, %rax
	jne	.L3666
	jmp	.L3656
	.p2align 4,,10
	.p2align 3
.L3698:
	movq	-136(%rbp), %rdi
	call	_ZNK2v88internal10Expression16ToBooleanIsFalseEv@PLT
	testb	%al, %al
	jne	.L3701
	leaq	-128(%rbp), %rcx
	movq	%r14, %rdx
	movl	$33, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator20VisitNaryLogicalTestENS0_5Token5ValueEPNS0_13NaryOperationEPKNS2_21NaryCodeCoverageSlotsE
.L3655:
	movb	$1, 32(%rbx)
	jmp	.L3656
	.p2align 4,,10
	.p2align 3
.L3701:
	movq	48(%rbx), %rdi
	call	_ZN2v88internal11interpreter14BytecodeLabels3NewEv@PLT
	leaq	24(%r12), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4JumpEPNS1_13BytecodeLabelE@PLT
	jmp	.L3655
.L3660:
	movq	%rbx, %rax
	movl	$-1, %ecx
	salq	$4, %rax
	jmp	.L3691
.L3672:
	movl	$-1, %ecx
	jmp	.L3657
.L3700:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20997:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator29VisitNaryLogicalAndExpressionEPNS0_13NaryOperationE, .-_ZN2v88internal11interpreter17BytecodeGenerator29VisitNaryLogicalAndExpressionEPNS0_13NaryOperationE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator26VisitNaryNullishExpressionEPNS0_13NaryOperationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator26VisitNaryNullishExpressionEPNS0_13NaryOperationE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator26VisitNaryNullishExpressionEPNS0_13NaryOperationE, @function
_ZN2v88internal11interpreter17BytecodeGenerator26VisitNaryNullishExpressionEPNS0_13NaryOperationE:
.LFB20999:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	8(%rsi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -128(%rbp)
	movq	568(%rdi), %rdi
	movq	$0, -120(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	testq	%rdi, %rdi
	je	.L3703
	movq	24(%rsi), %rax
	cmpq	%rax, 32(%rsi)
	je	.L3703
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L3714:
	testq	%rdi, %rdi
	je	.L3739
	movq	40(%rdi), %rax
	leaq	16(%rax), %rcx
	movq	24(%rax), %rax
	movq	%rcx, %rdx
	testq	%rax, %rax
	jne	.L3707
	jmp	.L3739
	.p2align 4,,10
	.p2align 3
.L3770:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L3708
.L3707:
	cmpq	%r14, 32(%rax)
	jnb	.L3770
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L3707
.L3708:
	cmpq	%rdx, %rcx
	je	.L3739
	cmpq	%r14, 32(%rdx)
	jbe	.L3771
.L3739:
	movl	$-1, %ebx
	movq	-112(%rbp), %rsi
	movl	%ebx, -96(%rbp)
	cmpq	-104(%rbp), %rsi
	je	.L3712
.L3772:
	movl	%ebx, (%rsi)
	addq	$4, -112(%rbp)
.L3766:
	movq	32(%r14), %rax
	subq	24(%r14), %rax
	addq	$1, %r13
	sarq	$4, %rax
	cmpq	%r13, %rax
	jbe	.L3703
	movq	-128(%rbp), %rax
	movq	568(%rax), %rdi
	jmp	.L3714
	.p2align 4,,10
	.p2align 3
.L3771:
	movq	40(%rdx), %rax
	testq	%rax, %rax
	je	.L3739
	movq	16(%rax), %rax
	leaq	(%rax,%r13,8), %rax
	movl	(%rax), %edx
	movq	(%rax), %rax
	movq	%rax, -96(%rbp)
	cmpl	$-1, %edx
	je	.L3739
	movq	16(%rdi), %rbx
	subq	8(%rdi), %rbx
	leaq	-96(%rbp), %rsi
	call	_ZNSt6vectorIN2v88internal11SourceRangeENS1_13ZoneAllocatorIS2_EEE12emplace_backIJRS2_EEEvDpOT_
	sarq	$3, %rbx
	movq	-112(%rbp), %rsi
	movl	%ebx, -96(%rbp)
	cmpq	-104(%rbp), %rsi
	jne	.L3772
.L3712:
	leaq	-96(%rbp), %rdx
	leaq	-120(%rbp), %rdi
	call	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	jmp	.L3766
	.p2align 4,,10
	.p2align 3
.L3703:
	movq	816(%r12), %rbx
	cmpl	$3, 24(%rbx)
	je	.L3773
	movq	16(%r12), %rax
	movq	$0, -72(%rbp)
	movb	$0, -64(%rbp)
	movq	%rax, -96(%rbp)
	leaq	-88(%rbp), %rax
	movq	%rax, -80(%rbp)
	movq	%rax, -88(%rbp)
	movq	%rax, -152(%rbp)
	movq	-128(%rbp), %rax
	cmpq	$0, 568(%rax)
	je	.L3740
	movq	-120(%rbp), %rax
	movl	(%rax), %ecx
.L3719:
	leaq	-96(%rbp), %rax
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal11interpreter17BytecodeGenerator25VisitNullishSubExpressionEPNS0_10ExpressionEPNS1_14BytecodeLabelsEi
	testb	%al, %al
	jne	.L3720
	movq	24(%r14), %rdx
	movq	32(%r14), %rax
	leaq	24(%r12), %rsi
	xorl	%ebx, %ebx
	movq	%rsi, -160(%rbp)
	subq	%rdx, %rax
	cmpq	$16, %rax
	je	.L3722
	.p2align 4,,10
	.p2align 3
.L3721:
	movq	-128(%rbp), %rax
	addq	$1, %rbx
	cmpq	$0, 568(%rax)
	je	.L3742
	movq	-120(%rbp), %rax
	movl	(%rax,%rbx,4), %r13d
.L3724:
	movq	%rbx, %rax
	salq	$4, %rax
	movq	-16(%rdx,%rax), %r15
	movq	%r15, %rdi
	call	_ZNK2v88internal10Expression30IsLiteralButNotNullOrUndefinedEv@PLT
	testb	%al, %al
	jne	.L3774
	movq	%r15, %rdi
	call	_ZNK2v88internal10Expression13IsNullLiteralEv@PLT
	testb	%al, %al
	je	.L3775
.L3727:
	movq	568(%r12), %rax
	testq	%rax, %rax
	je	.L3729
	cmpl	$-1, %r13d
	je	.L3729
	movq	32(%rax), %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder15IncBlockCounterEi@PLT
	movq	24(%r14), %rdx
	movq	32(%r14), %rax
	subq	%rdx, %rax
	movq	%rax, %rcx
	sarq	$4, %rcx
	subq	$1, %rcx
	cmpq	%rcx, %rbx
	jb	.L3721
.L3722:
	movq	-16(%rdx,%rax), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	movq	-160(%rbp), %rsi
	movq	-168(%rbp), %rdi
	call	_ZN2v88internal11interpreter14BytecodeLabels4BindEPNS1_20BytecodeArrayBuilderE@PLT
	movq	-88(%rbp), %rax
	cmpq	-152(%rbp), %rax
	je	.L3718
	.p2align 4,,10
	.p2align 3
.L3732:
	movq	(%rax), %rax
	cmpq	-152(%rbp), %rax
	jne	.L3732
.L3718:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3702
	call	_ZdlPv@PLT
.L3702:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3776
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3729:
	.cfi_restore_state
	movq	24(%r14), %rdx
	movq	32(%r14), %rax
	subq	%rdx, %rax
	movq	%rax, %rcx
	sarq	$4, %rcx
	subq	$1, %rcx
	cmpq	%rbx, %rcx
	ja	.L3721
	jmp	.L3722
	.p2align 4,,10
	.p2align 3
.L3774:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	movq	-160(%rbp), %rsi
	movq	-168(%rbp), %rdi
	call	_ZN2v88internal11interpreter14BytecodeLabels4BindEPNS1_20BytecodeArrayBuilderE@PLT
.L3720:
	movq	-88(%rbp), %rax
	cmpq	-152(%rbp), %rax
	je	.L3718
	.p2align 4,,10
	.p2align 3
.L3734:
	movq	(%rax), %rax
	cmpq	-152(%rbp), %rax
	je	.L3718
	movq	(%rax), %rax
	cmpq	-152(%rbp), %rax
	jne	.L3734
	jmp	.L3718
	.p2align 4,,10
	.p2align 3
.L3775:
	movq	%r15, %rdi
	call	_ZNK2v88internal10Expression18IsUndefinedLiteralEv@PLT
	testb	%al, %al
	jne	.L3727
	movq	%r15, %rsi
	movq	%r12, %rdi
	leaq	-144(%rbp), %r15
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	movq	-160(%rbp), %rdi
	movq	%r15, %rsi
	movb	$0, -144(%rbp)
	movq	$-1, -136(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder21JumpIfUndefinedOrNullEPNS1_13BytecodeLabelE@PLT
	movq	-168(%rbp), %rdi
	movq	%rax, -176(%rbp)
	call	_ZN2v88internal11interpreter14BytecodeLabels3NewEv@PLT
	movq	-176(%rbp), %r9
	movq	%rax, %rsi
	movq	%r9, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4JumpEPNS1_13BytecodeLabelE@PLT
	movq	-160(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4BindEPNS1_13BytecodeLabelE@PLT
	jmp	.L3727
	.p2align 4,,10
	.p2align 3
.L3773:
	movq	%r15, %rdi
	call	_ZNK2v88internal10Expression30IsLiteralButNotNullOrUndefinedEv@PLT
	testb	%al, %al
	jne	.L3777
.L3716:
	leaq	-128(%rbp), %rcx
	movq	%r14, %rdx
	movl	$31, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator20VisitNaryLogicalTestENS0_5Token5ValueEPNS0_13NaryOperationEPKNS2_21NaryCodeCoverageSlotsE
.L3717:
	movb	$1, 32(%rbx)
	jmp	.L3718
	.p2align 4,,10
	.p2align 3
.L3777:
	movq	%r15, %rdi
	call	_ZNK2v88internal10Expression15ToBooleanIsTrueEv@PLT
	testb	%al, %al
	je	.L3716
	movq	40(%rbx), %rdi
	call	_ZN2v88internal11interpreter14BytecodeLabels3NewEv@PLT
	leaq	24(%r12), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4JumpEPNS1_13BytecodeLabelE@PLT
	jmp	.L3717
.L3742:
	movl	$-1, %r13d
	jmp	.L3724
.L3740:
	movl	$-1, %ecx
	jmp	.L3719
.L3776:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20999:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator26VisitNaryNullishExpressionEPNS0_13NaryOperationE, .-_ZN2v88internal11interpreter17BytecodeGenerator26VisitNaryNullishExpressionEPNS0_13NaryOperationE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator18VisitNaryOperationEPNS0_13NaryOperationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator18VisitNaryOperationEPNS0_13NaryOperationE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator18VisitNaryOperationEPNS0_13NaryOperationE, @function
_ZN2v88internal11interpreter17BytecodeGenerator18VisitNaryOperationEPNS0_13NaryOperationE:
.LFB20950:
	.cfi_startproc
	endbr64
	movl	4(%rsi), %eax
	shrl	$7, %eax
	andl	$127, %eax
	cmpb	$32, %al
	je	.L3779
	ja	.L3780
	cmpb	$30, %al
	je	.L3781
	cmpb	$31, %al
	jne	.L3783
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator26VisitNaryNullishExpressionEPNS0_13NaryOperationE
	.p2align 4,,10
	.p2align 3
.L3780:
	cmpb	$33, %al
	jne	.L3783
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator29VisitNaryLogicalAndExpressionEPNS0_13NaryOperationE
	.p2align 4,,10
	.p2align 3
.L3779:
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator28VisitNaryLogicalOrExpressionEPNS0_13NaryOperationE
	.p2align 4,,10
	.p2align 3
.L3781:
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator24VisitNaryCommaExpressionEPNS0_13NaryOperationE
	.p2align 4,,10
	.p2align 3
.L3783:
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator29VisitNaryArithmeticExpressionEPNS0_13NaryOperationE
	.cfi_endproc
.LFE20950:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator18VisitNaryOperationEPNS0_13NaryOperationE, .-_ZN2v88internal11interpreter17BytecodeGenerator18VisitNaryOperationEPNS0_13NaryOperationE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator22VisitNullishExpressionEPNS0_15BinaryOperationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator22VisitNullishExpressionEPNS0_15BinaryOperationE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator22VisitNullishExpressionEPNS0_15BinaryOperationE, @function
_ZN2v88internal11interpreter17BytecodeGenerator22VisitNullishExpressionEPNS0_15BinaryOperationE:
.LFB20998:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	568(%rdi), %rbx
	movq	8(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rsi), %r14
	testq	%rbx, %rbx
	je	.L3809
	movq	40(%rbx), %rax
	leaq	16(%rax), %rcx
	movq	24(%rax), %rax
	movq	%rcx, %rdx
	testq	%rax, %rax
	jne	.L3789
	jmp	.L3809
	.p2align 4,,10
	.p2align 3
.L3826:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L3790
.L3789:
	cmpq	%rsi, 32(%rax)
	jnb	.L3826
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L3789
.L3790:
	cmpq	%rdx, %rcx
	je	.L3809
	cmpq	%rsi, 32(%rdx)
	ja	.L3809
	movq	40(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L3809
	movq	(%rdi), %rax
	movl	$5, %esi
	call	*16(%rax)
	movq	%rax, -96(%rbp)
	cmpl	$-1, %eax
	je	.L3809
	movq	%rbx, %rdi
	leaq	-96(%rbp), %rsi
	movq	16(%rbx), %r15
	subq	8(%rbx), %r15
	call	_ZNSt6vectorIN2v88internal11SourceRangeENS1_13ZoneAllocatorIS2_EEE12emplace_backIJRS2_EEEvDpOT_
	movq	816(%r12), %rbx
	sarq	$3, %r15
	cmpl	$3, 24(%rbx)
	je	.L3827
.L3794:
	leaq	-96(%rbp), %r8
	movq	16(%r12), %rax
	leaq	-88(%rbp), %rbx
	movl	%r15d, %ecx
	movq	%r8, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r8, -104(%rbp)
	movq	%rax, -96(%rbp)
	movq	%rbx, -80(%rbp)
	movq	%rbx, -88(%rbp)
	movq	$0, -72(%rbp)
	movb	$0, -64(%rbp)
	call	_ZN2v88internal11interpreter17BytecodeGenerator25VisitNullishSubExpressionEPNS0_10ExpressionEPNS1_14BytecodeLabelsEi
	movq	-104(%rbp), %r8
	testb	%al, %al
	je	.L3804
	movq	-88(%rbp), %rax
	cmpq	%rbx, %rax
	je	.L3785
	.p2align 4,,10
	.p2align 3
.L3805:
	movq	(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L3805
.L3785:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3828
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3809:
	.cfi_restore_state
	movq	816(%r12), %rbx
	movl	$-1, %r15d
	cmpl	$3, 24(%rbx)
	jne	.L3794
.L3827:
	movq	%r13, %rdi
	call	_ZNK2v88internal10Expression30IsLiteralButNotNullOrUndefinedEv@PLT
	testb	%al, %al
	je	.L3795
	movq	%r13, %rdi
	call	_ZNK2v88internal10Expression15ToBooleanIsTrueEv@PLT
	testb	%al, %al
	je	.L3795
	movq	40(%rbx), %rdi
	call	_ZN2v88internal11interpreter14BytecodeLabels3NewEv@PLT
	leaq	24(%r12), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4JumpEPNS1_13BytecodeLabelE@PLT
	.p2align 4,,10
	.p2align 3
.L3796:
	movb	$1, 32(%rbx)
	jmp	.L3785
	.p2align 4,,10
	.p2align 3
.L3804:
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	movq	-104(%rbp), %r8
	leaq	24(%r12), %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal11interpreter14BytecodeLabels4BindEPNS1_20BytecodeArrayBuilderE@PLT
	movq	-88(%rbp), %rax
	cmpq	%rbx, %rax
	je	.L3785
	.p2align 4,,10
	.p2align 3
.L3807:
	movq	(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L3807
	jmp	.L3785
	.p2align 4,,10
	.p2align 3
.L3795:
	movq	%r13, %rdi
	call	_ZNK2v88internal10Expression13IsNullLiteralEv@PLT
	testb	%al, %al
	je	.L3797
.L3800:
	movq	%r14, %rdi
	call	_ZNK2v88internal10Expression13IsNullLiteralEv@PLT
	testb	%al, %al
	je	.L3829
.L3798:
	movq	568(%r12), %rax
	testq	%rax, %rax
	je	.L3802
	cmpl	$-1, %r15d
	je	.L3802
	movq	32(%rax), %rdi
	movl	%r15d, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder15IncBlockCounterEi@PLT
.L3802:
	movq	48(%rbx), %rdi
	call	_ZN2v88internal11interpreter14BytecodeLabels3NewEv@PLT
	leaq	24(%r12), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4JumpEPNS1_13BytecodeLabelE@PLT
	jmp	.L3796
	.p2align 4,,10
	.p2align 3
.L3797:
	movq	%r13, %rdi
	call	_ZNK2v88internal10Expression18IsUndefinedLiteralEv@PLT
	testb	%al, %al
	jne	.L3800
.L3801:
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	%r15d, %r9d
	movl	$31, %esi
	movq	816(%r12), %rax
	movq	40(%rax), %r10
	movq	48(%rax), %r11
	movl	36(%rax), %eax
	movq	%r11, %r8
	movq	%r10, %rcx
	movq	%r11, -112(%rbp)
	movl	%eax, -116(%rbp)
	movq	%r10, -104(%rbp)
	call	_ZN2v88internal11interpreter17BytecodeGenerator29VisitLogicalTestSubExpressionENS0_5Token5ValueEPNS0_10ExpressionEPNS1_14BytecodeLabelsES8_i
	movl	-116(%rbp), %eax
	movq	-112(%rbp), %r11
	movq	%r14, %rsi
	movq	-104(%rbp), %r10
	movq	%r12, %rdi
	movl	%eax, %r8d
	movq	%r11, %rcx
	movq	%r10, %rdx
	call	_ZN2v88internal11interpreter17BytecodeGenerator12VisitForTestEPNS0_10ExpressionEPNS1_14BytecodeLabelsES6_NS2_15TestFallthroughE
	jmp	.L3796
	.p2align 4,,10
	.p2align 3
.L3829:
	movq	%r14, %rdi
	call	_ZNK2v88internal10Expression18IsUndefinedLiteralEv@PLT
	testb	%al, %al
	je	.L3801
	jmp	.L3798
.L3828:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20998:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator22VisitNullishExpressionEPNS0_15BinaryOperationE, .-_ZN2v88internal11interpreter17BytecodeGenerator22VisitNullishExpressionEPNS0_15BinaryOperationE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator24VisitLogicalOrExpressionEPNS0_15BinaryOperationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator24VisitLogicalOrExpressionEPNS0_15BinaryOperationE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator24VisitLogicalOrExpressionEPNS0_15BinaryOperationE, @function
_ZN2v88internal11interpreter17BytecodeGenerator24VisitLogicalOrExpressionEPNS0_15BinaryOperationE:
.LFB20991:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	568(%rdi), %r14
	movq	8(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rsi), %rax
	movq	%rax, -104(%rbp)
	testq	%r14, %r14
	je	.L3851
	movq	40(%r14), %rax
	leaq	16(%rax), %rcx
	movq	24(%rax), %rax
	movq	%rcx, %rdx
	testq	%rax, %rax
	jne	.L3834
	jmp	.L3851
	.p2align 4,,10
	.p2align 3
.L3865:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L3835
.L3834:
	cmpq	%rsi, 32(%rax)
	jnb	.L3865
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L3834
.L3835:
	cmpq	%rdx, %rcx
	je	.L3851
	cmpq	%rsi, 32(%rdx)
	ja	.L3851
	movq	40(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L3851
	movq	(%rdi), %rax
	movl	$5, %esi
	call	*16(%rax)
	movq	%rax, -96(%rbp)
	cmpl	$-1, %eax
	je	.L3851
	leaq	-96(%rbp), %rsi
	movq	%r14, %rdi
	movq	16(%r14), %r15
	subq	8(%r14), %r15
	call	_ZNSt6vectorIN2v88internal11SourceRangeENS1_13ZoneAllocatorIS2_EEE12emplace_backIJRS2_EEEvDpOT_
	movq	816(%r12), %rbx
	sarq	$3, %r15
	cmpl	$3, 24(%rbx)
	je	.L3866
.L3839:
	movq	16(%r12), %rax
	leaq	-96(%rbp), %r14
	leaq	-88(%rbp), %rbx
	movl	%r15d, %ecx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rbx, -80(%rbp)
	movq	%rax, -96(%rbp)
	movq	%rbx, -88(%rbp)
	movq	$0, -72(%rbp)
	movb	$0, -64(%rbp)
	call	_ZN2v88internal11interpreter17BytecodeGenerator27VisitLogicalOrSubExpressionEPNS0_10ExpressionEPNS1_14BytecodeLabelsEi
	testb	%al, %al
	je	.L3846
	movq	-88(%rbp), %rax
	cmpq	%rbx, %rax
	je	.L3830
	.p2align 4,,10
	.p2align 3
.L3847:
	movq	(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L3847
.L3830:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3867
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3851:
	.cfi_restore_state
	movq	816(%r12), %rbx
	movl	$-1, %r15d
	cmpl	$3, 24(%rbx)
	jne	.L3839
.L3866:
	movq	%r13, %rdi
	call	_ZNK2v88internal10Expression15ToBooleanIsTrueEv@PLT
	testb	%al, %al
	je	.L3840
	movq	40(%rbx), %rdi
	call	_ZN2v88internal11interpreter14BytecodeLabels3NewEv@PLT
	leaq	24(%r12), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4JumpEPNS1_13BytecodeLabelE@PLT
.L3841:
	movb	$1, 32(%rbx)
	jmp	.L3830
	.p2align 4,,10
	.p2align 3
.L3846:
	movq	-104(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	leaq	24(%r12), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter14BytecodeLabels4BindEPNS1_20BytecodeArrayBuilderE@PLT
	movq	-88(%rbp), %rax
	cmpq	%rbx, %rax
	je	.L3830
	.p2align 4,,10
	.p2align 3
.L3849:
	movq	(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L3849
	jmp	.L3830
	.p2align 4,,10
	.p2align 3
.L3840:
	movq	%r13, %rdi
	call	_ZNK2v88internal10Expression16ToBooleanIsFalseEv@PLT
	testb	%al, %al
	je	.L3842
	movq	-104(%rbp), %rdi
	call	_ZNK2v88internal10Expression16ToBooleanIsFalseEv@PLT
	testb	%al, %al
	je	.L3842
	movq	568(%r12), %rax
	testq	%rax, %rax
	je	.L3843
	cmpl	$-1, %r15d
	je	.L3843
	movq	32(%rax), %rdi
	movl	%r15d, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder15IncBlockCounterEi@PLT
.L3843:
	movq	48(%rbx), %rdi
	call	_ZN2v88internal11interpreter14BytecodeLabels3NewEv@PLT
	leaq	24(%r12), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4JumpEPNS1_13BytecodeLabelE@PLT
	jmp	.L3841
	.p2align 4,,10
	.p2align 3
.L3842:
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	%r15d, %r9d
	movl	$32, %esi
	movq	816(%r12), %rax
	movq	40(%rax), %r14
	movq	48(%rax), %r11
	movl	36(%rax), %eax
	movq	%r11, %r8
	movq	%r14, %rcx
	movq	%r11, -112(%rbp)
	movl	%eax, -116(%rbp)
	call	_ZN2v88internal11interpreter17BytecodeGenerator29VisitLogicalTestSubExpressionENS0_5Token5ValueEPNS0_10ExpressionEPNS1_14BytecodeLabelsES8_i
	movl	-116(%rbp), %eax
	movq	-112(%rbp), %r11
	movq	%r14, %rdx
	movq	-104(%rbp), %rsi
	movq	%r12, %rdi
	movl	%eax, %r8d
	movq	%r11, %rcx
	call	_ZN2v88internal11interpreter17BytecodeGenerator12VisitForTestEPNS0_10ExpressionEPNS1_14BytecodeLabelsES6_NS2_15TestFallthroughE
	jmp	.L3841
.L3867:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20991:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator24VisitLogicalOrExpressionEPNS0_15BinaryOperationE, .-_ZN2v88internal11interpreter17BytecodeGenerator24VisitLogicalOrExpressionEPNS0_15BinaryOperationE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator25VisitLogicalAndExpressionEPNS0_15BinaryOperationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator25VisitLogicalAndExpressionEPNS0_15BinaryOperationE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator25VisitLogicalAndExpressionEPNS0_15BinaryOperationE, @function
_ZN2v88internal11interpreter17BytecodeGenerator25VisitLogicalAndExpressionEPNS0_15BinaryOperationE:
.LFB20996:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	568(%rdi), %r14
	movq	8(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rsi), %rax
	movq	%rax, -104(%rbp)
	testq	%r14, %r14
	je	.L3889
	movq	40(%r14), %rax
	leaq	16(%rax), %rcx
	movq	24(%rax), %rax
	movq	%rcx, %rdx
	testq	%rax, %rax
	jne	.L3872
	jmp	.L3889
	.p2align 4,,10
	.p2align 3
.L3903:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L3873
.L3872:
	cmpq	%rsi, 32(%rax)
	jnb	.L3903
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L3872
.L3873:
	cmpq	%rdx, %rcx
	je	.L3889
	cmpq	%rsi, 32(%rdx)
	ja	.L3889
	movq	40(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L3889
	movq	(%rdi), %rax
	movl	$5, %esi
	call	*16(%rax)
	movq	%rax, -96(%rbp)
	cmpl	$-1, %eax
	je	.L3889
	leaq	-96(%rbp), %rsi
	movq	%r14, %rdi
	movq	16(%r14), %r15
	subq	8(%r14), %r15
	call	_ZNSt6vectorIN2v88internal11SourceRangeENS1_13ZoneAllocatorIS2_EEE12emplace_backIJRS2_EEEvDpOT_
	movq	816(%r12), %rbx
	sarq	$3, %r15
	cmpl	$3, 24(%rbx)
	je	.L3904
.L3877:
	movq	16(%r12), %rax
	leaq	-96(%rbp), %r14
	leaq	-88(%rbp), %rbx
	movl	%r15d, %ecx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rbx, -80(%rbp)
	movq	%rax, -96(%rbp)
	movq	%rbx, -88(%rbp)
	movq	$0, -72(%rbp)
	movb	$0, -64(%rbp)
	call	_ZN2v88internal11interpreter17BytecodeGenerator28VisitLogicalAndSubExpressionEPNS0_10ExpressionEPNS1_14BytecodeLabelsEi
	testb	%al, %al
	je	.L3884
	movq	-88(%rbp), %rax
	cmpq	%rbx, %rax
	je	.L3868
	.p2align 4,,10
	.p2align 3
.L3885:
	movq	(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L3885
.L3868:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3905
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3889:
	.cfi_restore_state
	movq	816(%r12), %rbx
	movl	$-1, %r15d
	cmpl	$3, 24(%rbx)
	jne	.L3877
.L3904:
	movq	%r13, %rdi
	call	_ZNK2v88internal10Expression16ToBooleanIsFalseEv@PLT
	testb	%al, %al
	je	.L3878
	movq	48(%rbx), %rdi
	call	_ZN2v88internal11interpreter14BytecodeLabels3NewEv@PLT
	leaq	24(%r12), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4JumpEPNS1_13BytecodeLabelE@PLT
.L3879:
	movb	$1, 32(%rbx)
	jmp	.L3868
	.p2align 4,,10
	.p2align 3
.L3884:
	movq	-104(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	leaq	24(%r12), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter14BytecodeLabels4BindEPNS1_20BytecodeArrayBuilderE@PLT
	movq	-88(%rbp), %rax
	cmpq	%rbx, %rax
	je	.L3868
	.p2align 4,,10
	.p2align 3
.L3887:
	movq	(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L3887
	jmp	.L3868
	.p2align 4,,10
	.p2align 3
.L3878:
	movq	%r13, %rdi
	call	_ZNK2v88internal10Expression15ToBooleanIsTrueEv@PLT
	testb	%al, %al
	je	.L3880
	movq	-104(%rbp), %rdi
	call	_ZNK2v88internal10Expression15ToBooleanIsTrueEv@PLT
	testb	%al, %al
	je	.L3880
	movq	568(%r12), %rax
	testq	%rax, %rax
	je	.L3881
	cmpl	$-1, %r15d
	je	.L3881
	movq	32(%rax), %rdi
	movl	%r15d, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder15IncBlockCounterEi@PLT
.L3881:
	movq	40(%rbx), %rdi
	call	_ZN2v88internal11interpreter14BytecodeLabels3NewEv@PLT
	leaq	24(%r12), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4JumpEPNS1_13BytecodeLabelE@PLT
	jmp	.L3879
	.p2align 4,,10
	.p2align 3
.L3880:
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	%r15d, %r9d
	movl	$33, %esi
	movq	816(%r12), %rax
	movq	40(%rax), %r14
	movq	48(%rax), %r11
	movl	36(%rax), %eax
	movq	%r11, %r8
	movq	%r14, %rcx
	movq	%r11, -112(%rbp)
	movl	%eax, -116(%rbp)
	call	_ZN2v88internal11interpreter17BytecodeGenerator29VisitLogicalTestSubExpressionENS0_5Token5ValueEPNS0_10ExpressionEPNS1_14BytecodeLabelsES8_i
	movl	-116(%rbp), %eax
	movq	-112(%rbp), %r11
	movq	%r14, %rdx
	movq	-104(%rbp), %rsi
	movq	%r12, %rdi
	movl	%eax, %r8d
	movq	%r11, %rcx
	call	_ZN2v88internal11interpreter17BytecodeGenerator12VisitForTestEPNS0_10ExpressionEPNS1_14BytecodeLabelsES6_NS2_15TestFallthroughE
	jmp	.L3879
.L3905:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20996:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator25VisitLogicalAndExpressionEPNS0_15BinaryOperationE, .-_ZN2v88internal11interpreter17BytecodeGenerator25VisitLogicalAndExpressionEPNS0_15BinaryOperationE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator29VisitInSameTestExecutionScopeEPNS0_10ExpressionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator29VisitInSameTestExecutionScopeEPNS0_10ExpressionE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator29VisitInSameTestExecutionScopeEPNS0_10ExpressionE, @function
_ZN2v88internal11interpreter17BytecodeGenerator29VisitInSameTestExecutionScopeEPNS0_10ExpressionE:
.LFB21030:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	xorl	%esi, %esi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpb	$0, 8(%rdi)
	movl	328(%rdi), %r12d
	je	.L3919
.L3907:
	movq	336(%rbx), %rdi
	movl	%r12d, 328(%rbx)
	testq	%rdi, %rdi
	je	.L3909
	movq	%rsi, %rax
	movq	(%rdi), %rdx
	movl	%r12d, %esi
	salq	$32, %rax
	orq	%rax, %rsi
	call	*32(%rdx)
.L3909:
	movq	816(%rbx), %r13
	cmpb	$0, 32(%r13)
	jne	.L3906
	movl	36(%r13), %eax
	xorl	%r14d, %r14d
	cmpl	$1, 28(%r13)
	movq	48(%r13), %r15
	movq	40(%r13), %rdi
	sete	%r14b
	cmpl	$1, %eax
	je	.L3911
	cmpl	$2, %eax
	je	.L3912
	testl	%eax, %eax
	je	.L3920
.L3913:
	movb	$1, 32(%r13)
.L3906:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3919:
	.cfi_restore_state
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%rbx), %rax
	jb	.L3921
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movl	328(%rbx), %esi
	subl	%r12d, %esi
	jmp	.L3907
	.p2align 4,,10
	.p2align 3
.L3911:
	call	_ZN2v88internal11interpreter14BytecodeLabels3NewEv@PLT
	leaq	24(%rbx), %rdi
	movl	%r14d, %esi
	movq	%rax, %rdx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder10JumpIfTrueENS2_13ToBooleanModeEPNS1_13BytecodeLabelE@PLT
	jmp	.L3913
	.p2align 4,,10
	.p2align 3
.L3921:
	movl	328(%rbx), %esi
	movb	$1, 8(%rbx)
	subl	%r12d, %esi
	jmp	.L3907
	.p2align 4,,10
	.p2align 3
.L3920:
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter14BytecodeLabels3NewEv@PLT
	leaq	24(%rbx), %rdi
	movl	%r14d, %esi
	movq	%rax, %rdx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11JumpIfFalseENS2_13ToBooleanModeEPNS1_13BytecodeLabelE@PLT
	jmp	.L3913
	.p2align 4,,10
	.p2align 3
.L3912:
	call	_ZN2v88internal11interpreter14BytecodeLabels3NewEv@PLT
	leaq	24(%rbx), %r12
	movl	%r14d, %esi
	movq	%rax, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder10JumpIfTrueENS2_13ToBooleanModeEPNS1_13BytecodeLabelE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter14BytecodeLabels3NewEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4JumpEPNS1_13BytecodeLabelE@PLT
	jmp	.L3913
	.cfi_endproc
.LFE21030:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator29VisitInSameTestExecutionScopeEPNS0_10ExpressionE, .-_ZN2v88internal11interpreter17BytecodeGenerator29VisitInSameTestExecutionScopeEPNS0_10ExpressionE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator8VisitNotEPNS0_14UnaryOperationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator8VisitNotEPNS0_14UnaryOperationE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator8VisitNotEPNS0_14UnaryOperationE, @function
_ZN2v88internal11interpreter17BytecodeGenerator8VisitNotEPNS0_14UnaryOperationE:
.LFB20945:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	816(%rdi), %rax
	movl	24(%rax), %edx
	cmpl	$1, %edx
	je	.L3930
	cmpl	$3, %edx
	jne	.L3924
	movdqu	40(%rax), %xmm0
	movl	36(%rax), %ecx
	movl	$1, %edx
	shufpd	$1, %xmm0, %xmm0
	movups	%xmm0, 40(%rax)
	testl	%ecx, %ecx
	je	.L3925
	xorl	%edx, %edx
	cmpl	$1, %ecx
	setne	%dl
	addl	%edx, %edx
.L3925:
	movl	%edx, 36(%rax)
	movq	8(%rsi), %rsi
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator29VisitInSameTestExecutionScopeEPNS0_10ExpressionE
	.p2align 4,,10
	.p2align 3
.L3924:
	.cfi_restore_state
	movq	8(%rsi), %rsi
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	xorl	%esi, %esi
	leaq	24(%r12), %rdi
	cmpl	$1, %eax
	sete	%sil
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder10LogicalNotENS2_13ToBooleanModeE@PLT
	movq	816(%r12), %rax
	movl	$1, 28(%rax)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3930:
	.cfi_restore_state
	movq	8(%rsi), %rsi
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator14VisitForEffectEPNS0_10ExpressionE
	.cfi_endproc
.LFE20945:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator8VisitNotEPNS0_14UnaryOperationE, .-_ZN2v88internal11interpreter17BytecodeGenerator8VisitNotEPNS0_14UnaryOperationE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator19VisitUnaryOperationEPNS0_14UnaryOperationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator19VisitUnaryOperationEPNS0_14UnaryOperationE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator19VisitUnaryOperationEPNS0_14UnaryOperationE, @function
_ZN2v88internal11interpreter17BytecodeGenerator19VisitUnaryOperationEPNS0_14UnaryOperationE:
.LFB20946:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movl	4(%rsi), %eax
	shrl	$7, %eax
	andl	$127, %eax
	subl	$44, %eax
	cmpb	$6, %al
	ja	.L3932
	leaq	.L3934(%rip), %rdx
	movzbl	%al, %eax
	movq	%rdi, %r12
	movq	%rsi, %r13
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal11interpreter17BytecodeGenerator19VisitUnaryOperationEPNS0_14UnaryOperationE,"a",@progbits
	.align 4
	.align 4
.L3934:
	.long	.L3937-.L3934
	.long	.L3937-.L3934
	.long	.L3938-.L3934
	.long	.L3937-.L3934
	.long	.L3936-.L3934
	.long	.L3935-.L3934
	.long	.L3933-.L3934
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator19VisitUnaryOperationEPNS0_14UnaryOperationE
	.p2align 4,,10
	.p2align 3
.L3937:
	movq	8(%rsi), %rsi
	leaq	24(%r12), %r14
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	movl	0(%r13), %eax
	cmpl	$-1, %eax
	je	.L3939
	cmpb	$2, 496(%r12)
	je	.L3939
	movb	$1, 496(%r12)
	movl	%eax, 500(%r12)
.L3939:
	movq	512(%r12), %rdi
	movl	$15, %esi
	addq	$56, %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movl	4(%r13), %esi
	addq	$8, %rsp
	movq	%r14, %rdi
	popq	%r12
	movl	%eax, %edx
	popq	%r13
	shrl	$7, %esi
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	andl	$127, %esi
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder14UnaryOperationENS0_5Token5ValueEi@PLT
	.p2align 4,,10
	.p2align 3
.L3938:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator8VisitNotEPNS0_14UnaryOperationE
	.p2align 4,,10
	.p2align 3
.L3936:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator11VisitDeleteEPNS0_14UnaryOperationE
	.p2align 4,,10
	.p2align 3
.L3935:
	.cfi_restore_state
	movq	8(%rsi), %rsi
	call	_ZN2v88internal11interpreter17BytecodeGenerator19VisitForTypeOfValueEPNS0_10ExpressionE
	addq	$8, %rsp
	leaq	24(%r12), %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder6TypeOfEv@PLT
	.p2align 4,,10
	.p2align 3
.L3933:
	.cfi_restore_state
	movq	8(%rsi), %rsi
	call	_ZN2v88internal11interpreter17BytecodeGenerator14VisitForEffectEPNS0_10ExpressionE
	addq	$8, %rsp
	leaq	24(%r12), %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder13LoadUndefinedEv@PLT
.L3932:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20946:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator19VisitUnaryOperationEPNS0_14UnaryOperationE, .-_ZN2v88internal11interpreter17BytecodeGenerator19VisitUnaryOperationEPNS0_14UnaryOperationE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator25VisitImportCallExpressionEPNS0_20ImportCallExpressionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator25VisitImportCallExpressionEPNS0_20ImportCallExpressionE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator25VisitImportCallExpressionEPNS0_20ImportCallExpressionE, @function
_ZN2v88internal11interpreter17BytecodeGenerator25VisitImportCallExpressionEPNS0_20ImportCallExpressionE:
.LFB20957:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movl	328(%rdi), %r14d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leal	2(%r14), %eax
	cmpl	%eax, 332(%rdi)
	movq	%r14, %r13
	movl	%eax, %edx
	cmovge	332(%rdi), %edx
	movl	%eax, 328(%rdi)
	movl	%edx, 332(%rdi)
	movq	336(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L3945
	movq	(%rdi), %rax
	movq	%r14, %rsi
	btsq	$33, %rsi
	call	*24(%rax)
	movl	328(%rbx), %eax
.L3945:
	movq	816(%rbx), %rdx
	movl	%eax, -80(%rbp)
	btsq	$33, %r14
	leaq	-96(%rbp), %rax
	cmpb	$0, 8(%rbx)
	movq	%rbx, -88(%rbp)
	leal	1(%r13), %r15d
	movq	8(%r12), %r12
	movq	%rdx, -96(%rbp)
	movq	$2, -72(%rbp)
	movq	%rax, 816(%rbx)
	je	.L3957
.L3946:
	leaq	24(%rbx), %r12
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rax
	movq	336(%rdx), %rdi
	movq	%rax, 816(%rdx)
	movl	-80(%rbp), %eax
	movl	328(%rdx), %esi
	movl	%eax, 328(%rdx)
	testq	%rdi, %rdi
	je	.L3948
	subl	%eax, %esi
	movq	(%rdi), %rcx
	movq	%rsi, %rdx
	movl	%eax, %esi
	salq	$32, %rdx
	orq	%rdx, %rsi
	call	*32(%rcx)
.L3948:
	call	_ZN2v88internal11interpreter8Register16function_closureEv@PLT
	movl	%r13d, %edx
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder12MoveRegisterENS1_8RegisterES3_@PLT
	movq	%r14, %rdx
	movl	$191, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_12RegisterListE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3958
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3957:
	.cfi_restore_state
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%rbx), %rax
	jb	.L3959
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	jmp	.L3946
	.p2align 4,,10
	.p2align 3
.L3959:
	movb	$1, 8(%rbx)
	jmp	.L3946
.L3958:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20957:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator25VisitImportCallExpressionEPNS0_20ImportCallExpressionE, .-_ZN2v88internal11interpreter17BytecodeGenerator25VisitImportCallExpressionEPNS0_20ImportCallExpressionE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator20VisitLiteralAccessorENS1_8RegisterEPNS0_15LiteralPropertyES3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator20VisitLiteralAccessorENS1_8RegisterEPNS0_15LiteralPropertyES3_
	.type	_ZN2v88internal11interpreter17BytecodeGenerator20VisitLiteralAccessorENS1_8RegisterEPNS0_15LiteralPropertyES3_, @function
_ZN2v88internal11interpreter17BytecodeGenerator20VisitLiteralAccessorENS1_8RegisterEPNS0_15LiteralPropertyES3_:
.LFB21005:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	24(%rdi), %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L3977
	movq	816(%rdi), %rax
	movl	%esi, %r14d
	movq	%rdi, %rbx
	movq	%rdx, %r12
	movq	8(%rdx), %rsi
	cmpb	$0, 8(%rdi)
	movq	%rdi, -88(%rbp)
	movq	%rax, -96(%rbp)
	movl	328(%rdi), %eax
	movq	%rsi, -104(%rbp)
	movl	%eax, -80(%rbp)
	leaq	-96(%rbp), %rax
	movq	$2, -72(%rbp)
	movq	%rax, 816(%rdi)
	je	.L3978
.L3963:
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rax
	movq	336(%rdx), %rdi
	movq	%rax, 816(%rdx)
	movl	-80(%rbp), %eax
	movl	328(%rdx), %esi
	movl	%eax, 328(%rdx)
	testq	%rdi, %rdi
	je	.L3965
	subl	%eax, %esi
	movq	(%rdi), %rcx
	movq	%rsi, %rdx
	movl	%eax, %esi
	salq	$32, %rdx
	orq	%rdx, %rsi
	call	*32(%rcx)
.L3965:
	movq	8(%r12), %rdi
	call	_ZN2v88internal15FunctionLiteral15NeedsHomeObjectEPNS0_10ExpressionE@PLT
	testb	%al, %al
	jne	.L3979
.L3960:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3980
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3979:
	.cfi_restore_state
	movq	512(%rbx), %rax
	leaq	56(%rax), %rdi
	movq	536(%rbx), %rax
	movzbl	129(%rax), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%esi, %esi
	andl	$-9, %esi
	addl	$11, %esi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movl	%r14d, %esi
	movq	%r15, %rdi
	movl	%eax, %r12d
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	movl	%r12d, %edx
	movl	%r13d, %esi
	movq	%rax, %rdi
	movq	536(%rbx), %rax
	movzbl	129(%rax), %ecx
	andl	$1, %ecx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder23StoreHomeObjectPropertyENS1_8RegisterEiNS0_12LanguageModeE@PLT
	jmp	.L3960
	.p2align 4,,10
	.p2align 3
.L3978:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%rbx), %rax
	movq	-104(%rbp), %rsi
	jb	.L3981
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	jmp	.L3963
	.p2align 4,,10
	.p2align 3
.L3977:
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder8LoadNullEv@PLT
	movl	%r13d, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	jmp	.L3960
	.p2align 4,,10
	.p2align 3
.L3981:
	movb	$1, 8(%rbx)
	jmp	.L3963
.L3980:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21005:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator20VisitLiteralAccessorENS1_8RegisterEPNS0_15LiteralPropertyES3_, .-_ZN2v88internal11interpreter17BytecodeGenerator20VisitLiteralAccessorENS1_8RegisterEPNS0_15LiteralPropertyES3_
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator24VisitStoreInArrayLiteralEPNS0_19StoreInArrayLiteralE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator24VisitStoreInArrayLiteralEPNS0_19StoreInArrayLiteralE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator24VisitStoreInArrayLiteralEPNS0_19StoreInArrayLiteralE, @function
_ZN2v88internal11interpreter17BytecodeGenerator24VisitStoreInArrayLiteralEPNS0_19StoreInArrayLiteralE:
.LFB20888:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	24(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%rsi), %eax
	cmpl	$-1, %eax
	je	.L3983
	movb	$2, 496(%rdi)
	movl	%eax, 500(%rdi)
.L3983:
	movl	328(%r15), %ebx
	movq	336(%r15), %rdi
	leal	1(%rbx), %r12d
	cmpl	%r12d, 332(%r15)
	movl	%r12d, %edx
	cmovge	332(%r15), %edx
	movl	%r12d, 328(%r15)
	movl	%edx, 332(%r15)
	testq	%rdi, %rdi
	je	.L3984
	movq	(%rdi), %rax
	movl	%ebx, %esi
	call	*16(%rax)
	movl	328(%r15), %r12d
	movl	332(%r15), %edx
	movq	336(%r15), %rdi
	leal	1(%r12), %eax
	cmpl	%eax, %edx
	movl	%eax, 328(%r15)
	cmovl	%eax, %edx
	movl	%edx, 332(%r15)
	testq	%rdi, %rdi
	je	.L3985
	movq	(%rdi), %rax
	movl	%r12d, %esi
	call	*16(%rax)
	movl	328(%r15), %eax
.L3985:
	movq	8(%r13), %rsi
	movq	816(%r15), %rdx
	movl	%eax, -80(%rbp)
	leaq	-96(%rbp), %rax
	cmpb	$0, 8(%r15)
	movq	%r15, -88(%rbp)
	movq	%rsi, -112(%rbp)
	movq	%rdx, -96(%rbp)
	movq	$2, -72(%rbp)
	movq	%rax, -104(%rbp)
	movq	%rax, 816(%r15)
	je	.L4010
.L3986:
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rax
	movq	336(%rdx), %rdi
	movq	%rax, 816(%rdx)
	movl	-80(%rbp), %eax
	movl	328(%rdx), %esi
	movl	%eax, 328(%rdx)
	testq	%rdi, %rdi
	je	.L3988
	subl	%eax, %esi
	movq	(%rdi), %rdx
	movq	%rsi, %rcx
	movl	%eax, %esi
	salq	$32, %rcx
	orq	%rcx, %rsi
	call	*32(%rdx)
.L3988:
	movq	816(%r15), %rax
	movq	16(%r13), %rsi
	movq	%r15, -88(%rbp)
	cmpb	$0, 8(%r15)
	movq	$2, -72(%rbp)
	movq	%rax, -96(%rbp)
	movl	328(%r15), %eax
	movq	%rsi, -112(%rbp)
	movl	%eax, -80(%rbp)
	movq	-104(%rbp), %rax
	movq	%rax, 816(%r15)
	je	.L4011
.L3989:
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rax
	movq	336(%rdx), %rdi
	movq	%rax, 816(%rdx)
	movl	-80(%rbp), %eax
	movl	328(%rdx), %esi
	movl	%eax, 328(%rdx)
	testq	%rdi, %rdi
	je	.L3991
	subl	%eax, %esi
	movq	(%rdi), %rcx
	movq	%rsi, %rdx
	movl	%eax, %esi
	salq	$32, %rdx
	orq	%rdx, %rsi
	call	*32(%rcx)
.L3991:
	movq	24(%r13), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	movq	512(%r15), %rax
	movl	$14, %esi
	leaq	56(%rax), %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movl	%ebx, %esi
	movq	%r14, %rdi
	movl	%r12d, %edx
	movl	%eax, %ecx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder19StoreInArrayLiteralENS1_8RegisterES3_i@PLT
	movq	336(%r15), %rdi
	movl	328(%r15), %esi
	movl	%ebx, 328(%r15)
	testq	%rdi, %rdi
	je	.L3982
	subl	%ebx, %esi
	movq	(%rdi), %rax
	salq	$32, %rsi
	orq	%rbx, %rsi
	call	*32(%rax)
.L3982:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4012
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4010:
	.cfi_restore_state
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r15), %rax
	movq	-112(%rbp), %rsi
	jb	.L4013
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	jmp	.L3986
	.p2align 4,,10
	.p2align 3
.L4011:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r15), %rax
	movq	-112(%rbp), %rsi
	jb	.L4014
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	jmp	.L3989
	.p2align 4,,10
	.p2align 3
.L3984:
	leal	2(%rbx), %eax
	cmpl	%edx, %eax
	movl	%eax, 328(%r15)
	cmovge	%eax, %edx
	movl	%edx, 332(%r15)
	jmp	.L3985
	.p2align 4,,10
	.p2align 3
.L4013:
	movb	$1, 8(%r15)
	jmp	.L3986
	.p2align 4,,10
	.p2align 3
.L4014:
	movb	$1, 8(%r15)
	jmp	.L3989
.L4012:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20888:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator24VisitStoreInArrayLiteralEPNS0_19StoreInArrayLiteralE, .-_ZN2v88internal11interpreter17BytecodeGenerator24VisitStoreInArrayLiteralEPNS0_19StoreInArrayLiteralE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator9VisitVoidEPNS0_14UnaryOperationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator9VisitVoidEPNS0_14UnaryOperationE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator9VisitVoidEPNS0_14UnaryOperationE, @function
_ZN2v88internal11interpreter17BytecodeGenerator9VisitVoidEPNS0_14UnaryOperationE:
.LFB20942:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	leaq	-64(%rbp), %rdx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	816(%rdi), %rcx
	movq	8(%rsi), %r12
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	328(%rdi), %eax
	cmpb	$0, 8(%rdi)
	movq	%rdi, -56(%rbp)
	movq	%rcx, -64(%rbp)
	movl	%eax, -48(%rbp)
	movq	$1, -40(%rbp)
	movq	%rdx, 816(%rdi)
	je	.L4026
	movq	%rdi, %rdx
	xorl	%esi, %esi
.L4016:
	movq	336(%rdx), %rdi
	movq	%rcx, 816(%rdx)
	movl	%eax, 328(%rdx)
	testq	%rdi, %rdi
	je	.L4018
	movq	%rsi, %rdx
	movq	(%rdi), %rcx
	movl	%eax, %esi
	salq	$32, %rdx
	orq	%rdx, %rsi
	call	*32(%rcx)
.L4018:
	leaq	24(%rbx), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder13LoadUndefinedEv@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4027
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4026:
	.cfi_restore_state
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%rbx), %rax
	jb	.L4028
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator25VisitNoStackOverflowCheckEPNS0_7AstNodeE
.L4025:
	movq	-56(%rbp), %rdx
	movl	-48(%rbp), %eax
	movq	-64(%rbp), %rcx
	movl	328(%rdx), %esi
	subl	%eax, %esi
	jmp	.L4016
	.p2align 4,,10
	.p2align 3
.L4028:
	movb	$1, 8(%rbx)
	jmp	.L4025
.L4027:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20942:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator9VisitVoidEPNS0_14UnaryOperationE, .-_ZN2v88internal11interpreter17BytecodeGenerator9VisitVoidEPNS0_14UnaryOperationE
	.section	.text.unlikely._ZN2v88internal11interpreter17BytecodeGenerator27VisitNamedSuperPropertyLoadEPNS0_8PropertyENS1_8RegisterE,"ax",@progbits
	.align 2
.LCOLDB20:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator27VisitNamedSuperPropertyLoadEPNS0_8PropertyENS1_8RegisterE,"ax",@progbits
.LHOTB20:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator27VisitNamedSuperPropertyLoadEPNS0_8PropertyENS1_8RegisterE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator27VisitNamedSuperPropertyLoadEPNS0_8PropertyENS1_8RegisterE, @function
_ZN2v88internal11interpreter17BytecodeGenerator27VisitNamedSuperPropertyLoadEPNS0_8PropertyENS1_8RegisterE:
.LFB20932:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movl	%edx, -108(%rbp)
	movq	8(%rsi), %rdx
	movl	328(%rdi), %r13d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	4(%rdx), %eax
	movq	%r13, %rbx
	andl	$63, %eax
	cmpb	$49, %al
	movl	$0, %eax
	cmove	%rdx, %rax
	movq	%rax, -104(%rbp)
	leal	3(%r13), %eax
	cmpl	%eax, 332(%rdi)
	movl	%eax, 328(%rdi)
	cmovge	332(%rdi), %eax
	movl	%eax, 332(%rdi)
	movq	336(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L4031
	movabsq	$12884901888, %rsi
	movq	(%rdi), %rax
	orq	%r13, %rsi
	call	*24(%rax)
.L4031:
	movq	528(%r15), %rdi
	leaq	24(%r15), %r14
	movabsq	$12884901888, %rax
	orq	%r13, %rax
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal5Scope16GetReceiverScopeEv@PLT
	movq	%r15, %rdi
	movzbl	133(%rax), %ecx
	movq	176(%rax), %rsi
	leal	-4(%rcx), %edx
	movl	$1, %ecx
	cmpb	$1, %dl
	seta	%dl
	movzbl	%dl, %edx
	call	_ZN2v88internal11interpreter17BytecodeGenerator17BuildVariableLoadEPNS0_8VariableENS0_13HoleCheckModeENS0_10TypeofModeE
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-104(%rbp), %rax
	cmpb	$0, 8(%r15)
	leal	1(%rbx), %r9d
	movq	8(%rax), %rsi
	movq	816(%r15), %rax
	movq	%r15, -88(%rbp)
	movq	$2, -72(%rbp)
	movq	%rax, -96(%rbp)
	movl	328(%r15), %eax
	movq	%rsi, -104(%rbp)
	movl	%eax, -80(%rbp)
	leaq	-96(%rbp), %rax
	movq	%rax, 816(%r15)
	je	.L4057
.L4032:
	movl	%r9d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rax
	movq	336(%rdx), %rdi
	movq	%rax, 816(%rdx)
	movl	-80(%rbp), %eax
	movl	328(%rdx), %esi
	movl	%eax, 328(%rdx)
	testq	%rdi, %rdi
	je	.L4034
	subl	%eax, %esi
	movq	(%rdi), %rdx
	movq	%rsi, %rcx
	movl	%eax, %esi
	salq	$32, %rcx
	orq	%rcx, %rsi
	call	*32(%rdx)
.L4034:
	movl	(%r12), %eax
	cmpl	$-1, %eax
	je	.L4035
	cmpb	$2, 496(%r15)
	je	.L4035
	movb	$1, 496(%r15)
	movl	%eax, 500(%r15)
.L4035:
	movq	16(%r12), %rdx
	movzbl	4(%rdx), %eax
	andl	$63, %eax
	cmpb	$41, %al
	jne	.L4036
	movq	8(%rdx), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralEPKNS0_12AstRawStringE@PLT
	leal	2(%rbx), %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-120(%rbp), %rdx
	movl	$36, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_12RegisterListE@PLT
	movl	-108(%rbp), %eax
	cmpl	$2147483647, %eax
	jne	.L4058
.L4037:
	movq	336(%r15), %rdi
	movl	328(%r15), %esi
	movl	%ebx, 328(%r15)
	testq	%rdi, %rdi
	je	.L4029
	subl	%ebx, %esi
	movq	(%rdi), %rax
	salq	$32, %rsi
	orq	%r13, %rsi
	call	*32(%rax)
.L4029:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4059
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4058:
	.cfi_restore_state
	movl	%eax, %edx
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder12MoveRegisterENS1_8RegisterES3_@PLT
	jmp	.L4037
	.p2align 4,,10
	.p2align 3
.L4057:
	movl	%r9d, -112(%rbp)
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r15), %rax
	movl	-112(%rbp), %r9d
	movq	-104(%rbp), %rsi
	jb	.L4060
	movq	%r15, %rdi
	movl	%r9d, -104(%rbp)
	call	_ZN2v88internal11interpreter17BytecodeGenerator25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movl	-104(%rbp), %r9d
	jmp	.L4032
	.p2align 4,,10
	.p2align 3
.L4060:
	movb	$1, 8(%r15)
	jmp	.L4032
.L4059:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal11interpreter17BytecodeGenerator27VisitNamedSuperPropertyLoadEPNS0_8PropertyENS1_8RegisterE
	.cfi_startproc
	.type	_ZN2v88internal11interpreter17BytecodeGenerator27VisitNamedSuperPropertyLoadEPNS0_8PropertyENS1_8RegisterE.cold, @function
_ZN2v88internal11interpreter17BytecodeGenerator27VisitNamedSuperPropertyLoadEPNS0_8PropertyENS1_8RegisterE.cold:
.LFSB20932:
.L4036:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	8, %rax
	ud2
	.cfi_endproc
.LFE20932:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator27VisitNamedSuperPropertyLoadEPNS0_8PropertyENS1_8RegisterE
	.size	_ZN2v88internal11interpreter17BytecodeGenerator27VisitNamedSuperPropertyLoadEPNS0_8PropertyENS1_8RegisterE, .-_ZN2v88internal11interpreter17BytecodeGenerator27VisitNamedSuperPropertyLoadEPNS0_8PropertyENS1_8RegisterE
	.section	.text.unlikely._ZN2v88internal11interpreter17BytecodeGenerator27VisitNamedSuperPropertyLoadEPNS0_8PropertyENS1_8RegisterE
	.size	_ZN2v88internal11interpreter17BytecodeGenerator27VisitNamedSuperPropertyLoadEPNS0_8PropertyENS1_8RegisterE.cold, .-_ZN2v88internal11interpreter17BytecodeGenerator27VisitNamedSuperPropertyLoadEPNS0_8PropertyENS1_8RegisterE.cold
.LCOLDE20:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator27VisitNamedSuperPropertyLoadEPNS0_8PropertyENS1_8RegisterE
.LHOTE20:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator20VisitCommaExpressionEPNS0_15BinaryOperationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator20VisitCommaExpressionEPNS0_15BinaryOperationE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator20VisitCommaExpressionEPNS0_15BinaryOperationE, @function
_ZN2v88internal11interpreter17BytecodeGenerator20VisitCommaExpressionEPNS0_15BinaryOperationE:
.LFB20983:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	leaq	-80(%rbp), %rdx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	816(%rdi), %rcx
	movq	8(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	328(%rdi), %eax
	cmpb	$0, 8(%rdi)
	movq	%rdi, -72(%rbp)
	movq	%rcx, -80(%rbp)
	movl	%eax, -64(%rbp)
	movq	$1, -56(%rbp)
	movq	%rdx, 816(%rdi)
	je	.L4074
	movq	%rdi, %rdx
	xorl	%esi, %esi
.L4062:
	movq	336(%rdx), %rdi
	movq	%rcx, 816(%rdx)
	movl	%eax, 328(%rdx)
	testq	%rdi, %rdi
	je	.L4064
	movq	%rsi, %rdx
	movq	(%rdi), %rcx
	movl	%eax, %esi
	salq	$32, %rdx
	orq	%rdx, %rsi
	call	*32(%rcx)
.L4064:
	cmpb	$0, 8(%r12)
	je	.L4075
.L4061:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4076
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4074:
	.cfi_restore_state
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L4077
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator25VisitNoStackOverflowCheckEPNS0_7AstNodeE
.L4073:
	movq	-72(%rbp), %rdx
	movl	-64(%rbp), %eax
	movq	-80(%rbp), %rcx
	movl	328(%rdx), %esi
	subl	%eax, %esi
	jmp	.L4062
	.p2align 4,,10
	.p2align 3
.L4075:
	movq	16(%rbx), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L4078
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	jmp	.L4061
	.p2align 4,,10
	.p2align 3
.L4077:
	movb	$1, 8(%r12)
	jmp	.L4073
	.p2align 4,,10
	.p2align 3
.L4078:
	movb	$1, 8(%r12)
	jmp	.L4061
.L4076:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20983:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator20VisitCommaExpressionEPNS0_15BinaryOperationE, .-_ZN2v88internal11interpreter17BytecodeGenerator20VisitCommaExpressionEPNS0_15BinaryOperationE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator27VisitKeyedSuperPropertyLoadEPNS0_8PropertyENS1_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator27VisitKeyedSuperPropertyLoadEPNS0_8PropertyENS1_8RegisterE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator27VisitKeyedSuperPropertyLoadEPNS0_8PropertyENS1_8RegisterE, @function
_ZN2v88internal11interpreter17BytecodeGenerator27VisitKeyedSuperPropertyLoadEPNS0_8PropertyENS1_8RegisterE:
.LFB20933:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movl	%edx, -108(%rbp)
	movq	8(%rsi), %r15
	movl	328(%rdi), %ebx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	4(%r15), %eax
	andl	$63, %eax
	cmpb	$49, %al
	movl	$0, %eax
	cmovne	%rax, %r15
	leal	3(%rbx), %eax
	cmpl	%eax, 332(%rdi)
	movl	%eax, 328(%rdi)
	cmovge	332(%rdi), %eax
	movl	%eax, 332(%rdi)
	movq	336(%rdi), %rdi
	movl	%ebx, %eax
	movq	%rax, -104(%rbp)
	testq	%rdi, %rdi
	je	.L4081
	movabsq	$12884901888, %rsi
	movq	%rax, %rcx
	movq	(%rdi), %rax
	orq	%rcx, %rsi
	call	*24(%rax)
.L4081:
	movq	528(%r14), %rdi
	leaq	24(%r14), %r13
	movabsq	$12884901888, %rax
	orq	-104(%rbp), %rax
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal5Scope16GetReceiverScopeEv@PLT
	movq	%r14, %rdi
	movzbl	133(%rax), %ecx
	movq	176(%rax), %rsi
	leal	-4(%rcx), %edx
	movl	$1, %ecx
	cmpb	$1, %dl
	seta	%dl
	movzbl	%dl, %edx
	call	_ZN2v88internal11interpreter17BytecodeGenerator17BuildVariableLoadEPNS0_8VariableENS0_13HoleCheckModeENS0_10TypeofModeE
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	816(%r14), %rax
	movq	8(%r15), %rsi
	leaq	-96(%rbp), %r15
	cmpb	$0, 8(%r14)
	movq	%r14, -88(%rbp)
	leal	1(%rbx), %r9d
	movq	%rax, -96(%rbp)
	movl	328(%r14), %eax
	movq	%rsi, -128(%rbp)
	movl	%eax, -80(%rbp)
	movq	$2, -72(%rbp)
	movq	%r15, 816(%r14)
	je	.L4112
.L4082:
	movl	%r9d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rax
	movq	336(%rdx), %rdi
	movq	%rax, 816(%rdx)
	movl	-80(%rbp), %eax
	movl	328(%rdx), %esi
	movl	%eax, 328(%rdx)
	testq	%rdi, %rdi
	je	.L4084
	subl	%eax, %esi
	movq	(%rdi), %rdx
	movq	%rsi, %rcx
	movl	%eax, %esi
	salq	$32, %rcx
	orq	%rcx, %rsi
	call	*32(%rdx)
.L4084:
	movq	816(%r14), %rax
	movq	16(%r12), %rsi
	movq	%r14, -88(%rbp)
	leal	2(%rbx), %r9d
	cmpb	$0, 8(%r14)
	movq	$2, -72(%rbp)
	movq	%rax, -96(%rbp)
	movl	328(%r14), %eax
	movq	%rsi, -128(%rbp)
	movl	%eax, -80(%rbp)
	movq	%r15, 816(%r14)
	je	.L4113
.L4085:
	movl	%r9d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rax
	movq	336(%rdx), %rdi
	movq	%rax, 816(%rdx)
	movl	-80(%rbp), %eax
	movl	328(%rdx), %esi
	movl	%eax, 328(%rdx)
	testq	%rdi, %rdi
	je	.L4087
	subl	%eax, %esi
	movq	(%rdi), %rdx
	movq	%rsi, %rcx
	movl	%eax, %esi
	salq	$32, %rcx
	orq	%rcx, %rsi
	call	*32(%rdx)
.L4087:
	movl	(%r12), %eax
	cmpl	$-1, %eax
	je	.L4088
	cmpb	$2, 496(%r14)
	je	.L4088
	movb	$1, 496(%r14)
	movl	%eax, 500(%r14)
.L4088:
	movq	-120(%rbp), %rdx
	movl	$37, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_12RegisterListE@PLT
	movl	-108(%rbp), %eax
	cmpl	$2147483647, %eax
	je	.L4089
	movl	%eax, %edx
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder12MoveRegisterENS1_8RegisterES3_@PLT
.L4089:
	movq	336(%r14), %rdi
	movl	328(%r14), %esi
	movl	%ebx, 328(%r14)
	testq	%rdi, %rdi
	je	.L4079
	movq	(%rdi), %rax
	subl	%ebx, %esi
	salq	$32, %rsi
	orq	-104(%rbp), %rsi
	call	*32(%rax)
.L4079:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4114
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4113:
	.cfi_restore_state
	movl	%r9d, -112(%rbp)
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r14), %rax
	movl	-112(%rbp), %r9d
	movq	-128(%rbp), %rsi
	jb	.L4115
	movq	%r14, %rdi
	movl	%r9d, -128(%rbp)
	call	_ZN2v88internal11interpreter17BytecodeGenerator25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movl	-128(%rbp), %r9d
	jmp	.L4085
	.p2align 4,,10
	.p2align 3
.L4112:
	movl	%r9d, -112(%rbp)
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r14), %rax
	movl	-112(%rbp), %r9d
	movq	-128(%rbp), %rsi
	jb	.L4116
	movq	%r14, %rdi
	movl	%r9d, -128(%rbp)
	call	_ZN2v88internal11interpreter17BytecodeGenerator25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movl	-128(%rbp), %r9d
	jmp	.L4082
	.p2align 4,,10
	.p2align 3
.L4115:
	movb	$1, 8(%r14)
	jmp	.L4085
	.p2align 4,,10
	.p2align 3
.L4116:
	movb	$1, 8(%r14)
	jmp	.L4082
.L4114:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20933:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator27VisitKeyedSuperPropertyLoadEPNS0_8PropertyENS1_8RegisterE, .-_ZN2v88internal11interpreter17BytecodeGenerator27VisitKeyedSuperPropertyLoadEPNS0_8PropertyENS1_8RegisterE
	.section	.text.unlikely._ZN2v88internal11interpreter17BytecodeGenerator17VisitPropertyLoadENS1_8RegisterEPNS0_8PropertyE,"ax",@progbits
	.align 2
.LCOLDB21:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator17VisitPropertyLoadENS1_8RegisterEPNS0_8PropertyE,"ax",@progbits
.LHOTB21:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator17VisitPropertyLoadENS1_8RegisterEPNS0_8PropertyE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator17VisitPropertyLoadENS1_8RegisterEPNS0_8PropertyE, @function
_ZN2v88internal11interpreter17BytecodeGenerator17VisitPropertyLoadENS1_8RegisterEPNS0_8PropertyE:
.LFB20927:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	testb	$-128, 4(%rdx)
	jne	.L4153
.L4118:
	movq	16(%r12), %rdi
	call	_ZNK2v88internal10Expression13IsPrivateNameEv@PLT
	testb	%al, %al
	je	.L4154
	movq	16(%r12), %rsi
	movzbl	4(%rsi), %eax
	andl	$63, %eax
	cmpb	$54, %al
	jne	.L4132
	movq	8(%rsi), %rax
	movzwl	40(%rax), %eax
	andl	$15, %eax
	cmpb	$8, %al
	je	.L4120
	ja	.L4121
	cmpb	$1, %al
	je	.L4122
	cmpb	$7, %al
	jne	.L4124
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	%r14d, %edx
	call	_ZN2v88internal11interpreter17BytecodeGenerator22BuildPrivateBrandCheckEPNS0_8PropertyENS1_8RegisterE
	movq	16(%r12), %rsi
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	.p2align 4,,10
	.p2align 3
.L4154:
	.cfi_restore_state
	movq	8(%r12), %rax
	movq	16(%r12), %rdi
	movl	4(%rax), %ebx
	andl	$63, %ebx
	call	_ZNK2v88internal10Expression14IsPropertyNameEv@PLT
	testb	%al, %al
	je	.L4126
	cmpb	$49, %bl
	je	.L4155
	movl	(%r12), %eax
	cmpl	$-1, %eax
	je	.L4130
	cmpb	$2, 496(%r13)
	je	.L4130
	movb	$1, 496(%r13)
	movl	%eax, 500(%r13)
.L4130:
	movq	16(%r12), %rdx
	movzbl	4(%rdx), %eax
	andl	$63, %eax
	cmpb	$41, %al
	jne	.L4132
	movq	8(%rdx), %rcx
	movq	8(%r12), %rsi
	addq	$8, %rsp
	movl	%r14d, %edx
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator22BuildLoadNamedPropertyEPKNS0_10ExpressionENS1_8RegisterEPKNS0_12AstRawStringE
	.p2align 4,,10
	.p2align 3
.L4153:
	.cfi_restore_state
	leaq	24(%rdi), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	movq	832(%r13), %rdi
	movq	%rax, %r15
	call	_ZN2v88internal11interpreter14BytecodeLabels3NewEv@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder21JumpIfUndefinedOrNullEPNS1_13BytecodeLabelE@PLT
	jmp	.L4118
	.p2align 4,,10
	.p2align 3
.L4126:
	cmpb	$49, %bl
	je	.L4129
	movq	16(%r12), %rsi
.L4122:
	movq	%r13, %rdi
	leaq	24(%r13), %r15
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	movl	(%r12), %eax
	cmpl	$-1, %eax
	je	.L4133
	cmpb	$2, 496(%r13)
	je	.L4133
	movb	$1, 496(%r13)
	movl	%eax, 500(%r13)
.L4133:
	movq	512(%r13), %rdi
	movl	$8, %esi
	addq	$56, %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	addq	$8, %rsp
	movl	%r14d, %esi
	movq	%r15, %rdi
	popq	%rbx
	movl	%eax, %edx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder17LoadKeyedPropertyENS1_8RegisterEi@PLT
	.p2align 4,,10
	.p2align 3
.L4120:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdx
	movq	%r13, %rdi
	movl	$262, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator26BuildInvalidPropertyAccessENS0_15MessageTemplateEPNS0_8PropertyE
	.p2align 4,,10
	.p2align 3
.L4121:
	.cfi_restore_state
	subl	$9, %eax
	cmpb	$1, %al
	ja	.L4124
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator21VisitForRegisterValueEPNS0_10ExpressionE
	movl	%r14d, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	%eax, %r15d
	call	_ZN2v88internal11interpreter17BytecodeGenerator22BuildPrivateBrandCheckEPNS0_8PropertyENS1_8RegisterE
	addq	$8, %rsp
	movl	%r15d, %edx
	movl	%r14d, %esi
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator24BuildPrivateGetterAccessENS1_8RegisterES3_
	.p2align 4,,10
	.p2align 3
.L4129:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	$2147483647, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator27VisitKeyedSuperPropertyLoadEPNS0_8PropertyENS1_8RegisterE
	.p2align 4,,10
	.p2align 3
.L4155:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	$2147483647, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator27VisitNamedSuperPropertyLoadEPNS0_8PropertyENS1_8RegisterE
.L4124:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal11interpreter17BytecodeGenerator17VisitPropertyLoadENS1_8RegisterEPNS0_8PropertyE
	.cfi_startproc
	.type	_ZN2v88internal11interpreter17BytecodeGenerator17VisitPropertyLoadENS1_8RegisterEPNS0_8PropertyE.cold, @function
_ZN2v88internal11interpreter17BytecodeGenerator17VisitPropertyLoadENS1_8RegisterEPNS0_8PropertyE.cold:
.LFSB20927:
.L4132:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	8, %rax
	ud2
	.cfi_endproc
.LFE20927:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator17VisitPropertyLoadENS1_8RegisterEPNS0_8PropertyE
	.size	_ZN2v88internal11interpreter17BytecodeGenerator17VisitPropertyLoadENS1_8RegisterEPNS0_8PropertyE, .-_ZN2v88internal11interpreter17BytecodeGenerator17VisitPropertyLoadENS1_8RegisterEPNS0_8PropertyE
	.section	.text.unlikely._ZN2v88internal11interpreter17BytecodeGenerator17VisitPropertyLoadENS1_8RegisterEPNS0_8PropertyE
	.size	_ZN2v88internal11interpreter17BytecodeGenerator17VisitPropertyLoadENS1_8RegisterEPNS0_8PropertyE.cold, .-_ZN2v88internal11interpreter17BytecodeGenerator17VisitPropertyLoadENS1_8RegisterEPNS0_8PropertyE.cold
.LCOLDE21:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator17VisitPropertyLoadENS1_8RegisterEPNS0_8PropertyE
.LHOTE21:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator28VisitPropertyLoadForRegisterENS1_8RegisterEPNS0_8PropertyES3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator28VisitPropertyLoadForRegisterENS1_8RegisterEPNS0_8PropertyES3_
	.type	_ZN2v88internal11interpreter17BytecodeGenerator28VisitPropertyLoadForRegisterENS1_8RegisterEPNS0_8PropertyES3_, @function
_ZN2v88internal11interpreter17BytecodeGenerator28VisitPropertyLoadForRegisterENS1_8RegisterEPNS0_8PropertyES3_:
.LFB20931:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	816(%rdi), %rax
	movq	%rdi, -56(%rbp)
	movq	$2, -40(%rbp)
	movq	%rax, -64(%rbp)
	movl	328(%rdi), %eax
	movl	%eax, -48(%rbp)
	leaq	-64(%rbp), %rax
	movq	%rax, 816(%rdi)
	call	_ZN2v88internal11interpreter17BytecodeGenerator17VisitPropertyLoadENS1_8RegisterEPNS0_8PropertyE
	leaq	24(%rbx), %rdi
	movl	%r12d, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rax
	movq	336(%rdx), %rdi
	movq	%rax, 816(%rdx)
	movl	-48(%rbp), %eax
	movl	328(%rdx), %esi
	movl	%eax, 328(%rdx)
	testq	%rdi, %rdi
	je	.L4156
	subl	%eax, %esi
	movq	(%rdi), %rcx
	movq	%rsi, %rdx
	movl	%eax, %esi
	salq	$32, %rdx
	orq	%rdx, %rsi
	call	*32(%rcx)
.L4156:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4163
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4163:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20931:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator28VisitPropertyLoadForRegisterENS1_8RegisterEPNS0_8PropertyES3_, .-_ZN2v88internal11interpreter17BytecodeGenerator28VisitPropertyLoadForRegisterENS1_8RegisterEPNS0_8PropertyES3_
	.section	.text.unlikely._ZN2v88internal11interpreter17BytecodeGenerator13VisitPropertyEPNS0_8PropertyE,"ax",@progbits
	.align 2
.LCOLDB22:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator13VisitPropertyEPNS0_8PropertyE,"ax",@progbits
.LHOTB22:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator13VisitPropertyEPNS0_8PropertyE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator13VisitPropertyEPNS0_8PropertyE, @function
_ZN2v88internal11interpreter17BytecodeGenerator13VisitPropertyEPNS0_8PropertyE:
.LFB20935:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	testq	%rsi, %rsi
	je	.L4166
	movq	16(%rsi), %rdi
	call	_ZNK2v88internal10Expression13IsPrivateNameEv@PLT
	testb	%al, %al
	je	.L4167
	movq	16(%r12), %rdx
	movzbl	4(%rdx), %eax
	andl	$63, %eax
	cmpb	$54, %al
	jne	.L4179
	movq	8(%rdx), %rax
	movzwl	40(%rax), %eax
	andl	$15, %eax
	cmpb	$1, %al
	je	.L4166
	subl	$7, %eax
	cmpb	$3, %al
	ja	.L4180
.L4166:
	movq	8(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator21VisitForRegisterValueEPNS0_10ExpressionE
	movq	%r12, %rdx
	movl	%eax, %esi
.L4178:
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator17VisitPropertyLoadENS1_8RegisterEPNS0_8PropertyE
	.p2align 4,,10
	.p2align 3
.L4167:
	.cfi_restore_state
	movq	8(%r12), %rax
	movq	16(%r12), %rdi
	movl	4(%rax), %ebx
	andl	$63, %ebx
	call	_ZNK2v88internal10Expression14IsPropertyNameEv@PLT
	cmpb	$49, %bl
	jne	.L4166
	movq	%r12, %rdx
	movl	$2147483647, %esi
	jmp	.L4178
.L4180:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L4179:
	jmp	.L4176
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal11interpreter17BytecodeGenerator13VisitPropertyEPNS0_8PropertyE
	.cfi_startproc
	.type	_ZN2v88internal11interpreter17BytecodeGenerator13VisitPropertyEPNS0_8PropertyE.cold, @function
_ZN2v88internal11interpreter17BytecodeGenerator13VisitPropertyEPNS0_8PropertyE.cold:
.LFSB20935:
.L4176:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	8, %rax
	ud2
	.cfi_endproc
.LFE20935:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator13VisitPropertyEPNS0_8PropertyE
	.size	_ZN2v88internal11interpreter17BytecodeGenerator13VisitPropertyEPNS0_8PropertyE, .-_ZN2v88internal11interpreter17BytecodeGenerator13VisitPropertyEPNS0_8PropertyE
	.section	.text.unlikely._ZN2v88internal11interpreter17BytecodeGenerator13VisitPropertyEPNS0_8PropertyE
	.size	_ZN2v88internal11interpreter17BytecodeGenerator13VisitPropertyEPNS0_8PropertyE.cold, .-_ZN2v88internal11interpreter17BytecodeGenerator13VisitPropertyEPNS0_8PropertyE.cold
.LCOLDE22:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator13VisitPropertyEPNS0_8PropertyE
.LHOTE22:
	.section	.rodata._ZN2v88internal11interpreter17BytecodeGenerator9VisitCallEPNS0_4CallE.str1.8,"aMS",@progbits,1
	.align 8
.LC23:
	.string	"reciever_arg_count + expr->arguments()->length() == args.register_count()"
	.section	.text.unlikely._ZN2v88internal11interpreter17BytecodeGenerator9VisitCallEPNS0_4CallE,"ax",@progbits
	.align 2
.LCOLDB24:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator9VisitCallEPNS0_4CallE,"ax",@progbits
.LHOTB24:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator9VisitCallEPNS0_4CallE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator9VisitCallEPNS0_4CallE, @function
_ZN2v88internal11interpreter17BytecodeGenerator9VisitCallEPNS0_4CallE:
.LFB20938:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rsi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal4Call11GetCallTypeEv@PLT
	cmpl	$7, %eax
	je	.L4302
	movl	328(%r13), %esi
	movl	%eax, %r12d
	leaq	328(%r13), %rax
	movq	336(%r13), %rdi
	movq	%rax, -152(%rbp)
	leal	1(%rsi), %ecx
	cmpl	%ecx, 332(%r13)
	movl	%esi, -116(%rbp)
	movl	%ecx, %eax
	cmovge	332(%r13), %eax
	movl	%ecx, 328(%r13)
	movl	%eax, 332(%r13)
	testq	%rdi, %rdi
	je	.L4184
	movq	(%rdi), %rax
	call	*16(%rax)
	movl	328(%r13), %ecx
.L4184:
	movl	28(%r14), %eax
	movl	%ecx, -104(%rbp)
	movl	$0, -100(%rbp)
	movb	$0, -117(%rbp)
	testl	%eax, %eax
	jne	.L4303
.L4185:
	movzbl	_ZN2v88internal33FLAG_enable_one_shot_optimizationE(%rip), %ebx
	testb	%bl, %bl
	je	.L4186
	movl	868(%r13), %esi
	testl	%esi, %esi
	jg	.L4186
	movq	512(%r13), %rax
	movq	16(%rax), %rax
	movl	28(%rax), %edx
	testl	%edx, %edx
	jne	.L4304
.L4187:
	cmpl	$9, %r12d
	ja	.L4250
	leaq	.L4233(%rip), %rsi
	movl	%r12d, %edx
	movslq	(%rsi,%rdx,4), %rax
	addq	%rsi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal11interpreter17BytecodeGenerator9VisitCallEPNS0_4CallE,"a",@progbits
	.align 4
	.align 4
.L4233:
	.long	.L4202-.L4233
	.long	.L4195-.L4233
	.long	.L4192-.L4233
	.long	.L4192-.L4233
	.long	.L4194-.L4233
	.long	.L4193-.L4233
	.long	.L4192-.L4233
	.long	.L4250-.L4233
	.long	.L4191-.L4233
	.long	.L4212-.L4233
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator9VisitCallEPNS0_4CallE
	.p2align 4,,10
	.p2align 3
.L4304:
	testb	$64, 6(%rax)
	jne	.L4187
	.p2align 4,,10
	.p2align 3
.L4186:
	cmpl	$9, %r12d
	ja	.L4240
	leaq	.L4190(%rip), %rsi
	movl	%r12d, %edx
	movslq	(%rsi,%rdx,4), %rax
	addq	%rsi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal11interpreter17BytecodeGenerator9VisitCallEPNS0_4CallE
	.align 4
	.align 4
.L4190:
	.long	.L4196-.L4190
	.long	.L4241-.L4190
	.long	.L4242-.L4190
	.long	.L4242-.L4190
	.long	.L4243-.L4190
	.long	.L4244-.L4190
	.long	.L4242-.L4190
	.long	.L4240-.L4190
	.long	.L4245-.L4190
	.long	.L4189-.L4190
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator9VisitCallEPNS0_4CallE
.L4245:
	xorl	%ebx, %ebx
.L4191:
	movzbl	4(%r15), %eax
	andl	$63, %eax
	cmpb	$45, %al
	jne	.L4209
	movq	8(%r15), %rsi
	movq	%r13, %rdi
	leaq	-104(%rbp), %rdx
	call	_ZN2v88internal11interpreter17BytecodeGenerator28VisitAndPushIntoRegisterListEPNS0_10ExpressionEPNS1_12RegisterListE
	movq	16(%r15), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
.L4301:
	movl	-116(%rbp), %esi
	leaq	24(%r13), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movl	$1, %r9d
	xorl	%r10d, %r10d
	testb	$2, 5(%r14)
	je	.L4220
.L4309:
	movl	-116(%rbp), %esi
	leaq	24(%r13), %rdi
	movl	%r9d, -132(%rbp)
	movb	%r10b, -128(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	movq	832(%r13), %rdi
	movq	%rax, %r15
	call	_ZN2v88internal11interpreter14BytecodeLabels3NewEv@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder21JumpIfUndefinedOrNullEPNS1_13BytecodeLabelE@PLT
	movl	-132(%rbp), %r9d
	movzbl	-128(%rbp), %r10d
	jmp	.L4220
.L4244:
	xorl	%ebx, %ebx
.L4193:
	leal	1(%rcx), %eax
	movq	336(%r13), %rdi
	cmpl	%eax, 332(%r13)
	movl	%eax, 328(%r13)
	cmovge	332(%r13), %eax
	movl	%eax, 332(%r13)
	testq	%rdi, %rdi
	je	.L4217
	movq	(%rdi), %rax
	movl	%ecx, -128(%rbp)
	movl	%ecx, %esi
	call	*16(%rax)
	movl	-100(%rbp), %eax
	movl	-128(%rbp), %ecx
	leal	1(%rax), %edx
	testl	%edx, %edx
	movl	%edx, -100(%rbp)
	jne	.L4305
.L4218:
	cmpl	%ecx, %edx
	jne	.L4215
	movzbl	4(%r15), %eax
	movl	$0, %esi
	movq	%r13, %rdi
	andl	$63, %eax
	cmpb	$44, %al
	cmove	%r15, %rsi
	call	_ZN2v88internal11interpreter17BytecodeGenerator27VisitKeyedSuperPropertyLoadEPNS0_8PropertyENS1_8RegisterE
	jmp	.L4301
.L4243:
	xorl	%ebx, %ebx
.L4194:
	leal	1(%rcx), %eax
	movq	336(%r13), %rdi
	cmpl	%eax, 332(%r13)
	movl	%eax, 328(%r13)
	cmovge	332(%r13), %eax
	movl	%eax, 332(%r13)
	testq	%rdi, %rdi
	je	.L4213
	movq	(%rdi), %rax
	movl	%ecx, -128(%rbp)
	movl	%ecx, %esi
	call	*16(%rax)
	movl	-100(%rbp), %eax
	movl	-128(%rbp), %ecx
	leal	1(%rax), %edx
	testl	%edx, %edx
	movl	%edx, -100(%rbp)
	jne	.L4306
.L4214:
	cmpl	%ecx, %edx
	jne	.L4215
	movzbl	4(%r15), %eax
	movl	$0, %esi
	movq	%r13, %rdi
	andl	$63, %eax
	cmpb	$44, %al
	cmove	%r15, %rsi
	call	_ZN2v88internal11interpreter17BytecodeGenerator27VisitNamedSuperPropertyLoadEPNS0_8PropertyENS1_8RegisterE
	jmp	.L4301
.L4242:
	xorl	%ebx, %ebx
.L4192:
	movzbl	4(%r15), %eax
	andl	$63, %eax
	cmpb	$44, %al
	jne	.L4209
	movq	8(%r15), %rsi
	leaq	-104(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator28VisitAndPushIntoRegisterListEPNS0_10ExpressionEPNS1_12RegisterListE
	movl	-100(%rbp), %esi
	testl	%esi, %esi
	jne	.L4307
.L4198:
	movq	816(%r13), %rax
	movq	%r15, %rdx
	movq	%r13, %rdi
	movq	%r13, -88(%rbp)
	movq	$2, -72(%rbp)
	movq	%rax, -96(%rbp)
	movl	328(%r13), %eax
	movl	%eax, -80(%rbp)
	leaq	-96(%rbp), %rax
	movq	%rax, 816(%r13)
	call	_ZN2v88internal11interpreter17BytecodeGenerator17VisitPropertyLoadENS1_8RegisterEPNS0_8PropertyE
	movl	-116(%rbp), %esi
	leaq	24(%r13), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rax
	movq	336(%rdx), %rdi
	movq	%rax, 816(%rdx)
	movl	-80(%rbp), %eax
	movl	328(%rdx), %esi
	movl	%eax, 328(%rdx)
	testq	%rdi, %rdi
	je	.L4250
	subl	%eax, %esi
	movq	(%rdi), %rdx
	movq	%rsi, %rcx
	movl	%eax, %esi
	salq	$32, %rcx
	orq	%rcx, %rsi
	call	*32(%rdx)
.L4250:
	movl	$1, %r9d
	xorl	%r10d, %r10d
	jmp	.L4188
.L4241:
	xorl	%ebx, %ebx
.L4195:
	leal	1(%rcx), %eax
	movq	336(%r13), %rdi
	cmpl	%eax, 332(%r13)
	movl	%eax, 328(%r13)
	cmovge	332(%r13), %eax
	movl	%eax, 332(%r13)
	testq	%rdi, %rdi
	je	.L4205
	movq	(%rdi), %rax
	movl	%ecx, -128(%rbp)
	movl	%ecx, %esi
	call	*16(%rax)
	movl	-100(%rbp), %eax
	movl	-128(%rbp), %ecx
	leal	1(%rax), %r9d
	testl	%r9d, %r9d
	movl	%r9d, -100(%rbp)
	jne	.L4308
.L4206:
	cmpl	%ecx, %r9d
	jne	.L4215
	movl	328(%r13), %eax
	leaq	328(%r13), %rdi
	movl	%r9d, -128(%rbp)
	movl	%eax, -132(%rbp)
	call	_ZN2v88internal11interpreter25BytecodeRegisterAllocator11NewRegisterEv
	movl	328(%r13), %ecx
	movq	336(%r13), %rdi
	movl	%eax, -144(%rbp)
	movl	-128(%rbp), %r9d
	leal	2(%rcx), %eax
	cmpl	%eax, 332(%r13)
	movq	%rcx, %r10
	movl	%eax, 328(%r13)
	cmovge	332(%r13), %eax
	testq	%rdi, %rdi
	movl	%eax, 332(%r13)
	je	.L4208
	movq	(%rdi), %rax
	movq	%rcx, %rsi
	movl	%ecx, -136(%rbp)
	movl	%r9d, -156(%rbp)
	btsq	$33, %rsi
	movq	%rcx, -128(%rbp)
	call	*24(%rax)
	movl	-156(%rbp), %r9d
	movl	-136(%rbp), %r10d
	movq	-128(%rbp), %rcx
.L4208:
	movzbl	4(%r15), %eax
	btsq	$33, %rcx
	movl	%r9d, -156(%rbp)
	movl	%r10d, -136(%rbp)
	andl	$63, %eax
	movq	%rcx, -128(%rbp)
	cmpb	$54, %al
	jne	.L4209
	movq	8(%r15), %rax
	leaq	24(%r13), %rdi
	movq	8(%rax), %rsi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralEPKNS0_12AstRawStringE@PLT
	movl	-144(%rbp), %r15d
	movq	%rax, %rdi
	movl	%r15d, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-128(%rbp), %rcx
	movl	%r15d, %edx
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder18CallRuntimeForPairENS0_7Runtime10FunctionIdENS1_8RegisterENS1_12RegisterListE@PLT
	movl	-136(%rbp), %r10d
	movl	-116(%rbp), %edx
	movq	%rax, %rdi
	movl	%r10d, %esi
	movl	%r10d, -128(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder12MoveRegisterENS1_8RegisterES3_@PLT
	movl	-128(%rbp), %r10d
	movl	-156(%rbp), %r9d
	movq	%rax, %rdi
	leal	1(%r10), %esi
	movl	%r9d, %edx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder12MoveRegisterENS1_8RegisterES3_@PLT
	movl	328(%r13), %esi
	movl	-132(%rbp), %ecx
	movq	336(%r13), %rdi
	movl	%ecx, 328(%r13)
	subl	%ecx, %esi
	testq	%rdi, %rdi
	je	.L4250
	movq	%rsi, %rax
	movq	(%rdi), %rdx
	movl	%ecx, %esi
	salq	$32, %rax
	orq	%rax, %rsi
	call	*32(%rdx)
	jmp	.L4250
.L4240:
	movl	$1, %r9d
	xorl	%ebx, %ebx
	xorl	%r10d, %r10d
.L4188:
	testb	$2, 5(%r14)
	jne	.L4309
.L4220:
	movl	28(%r14), %eax
	testl	%eax, %eax
	jle	.L4221
	movb	%r10b, -128(%rbp)
	xorl	%r15d, %r15d
	leaq	-104(%rbp), %rdx
	movl	%r12d, -132(%rbp)
	movq	%r14, %r12
	movl	%r9d, %r14d
	movb	%bl, -144(%rbp)
	movq	%r15, %rbx
	movq	%rdx, %r15
	.p2align 4,,10
	.p2align 3
.L4222:
	movq	16(%r12), %rax
	movq	%r15, %rdx
	movq	%r13, %rdi
	movq	(%rax,%rbx,8), %rsi
	addq	$1, %rbx
	call	_ZN2v88internal11interpreter17BytecodeGenerator28VisitAndPushIntoRegisterListEPNS0_10ExpressionEPNS1_12RegisterListE
	movl	28(%r12), %eax
	cmpl	%ebx, %eax
	jg	.L4222
	movl	%r14d, %r9d
	movzbl	-128(%rbp), %r10d
	movq	%r12, %r14
	movzbl	-144(%rbp), %ebx
	movl	-132(%rbp), %r12d
	leal	(%rax,%r9), %edx
	cmpl	-100(%rbp), %edx
	jne	.L4234
	cmpb	$0, 4(%r14)
	leaq	24(%r13), %r15
	jns	.L4224
	testl	%eax, %eax
	jg	.L4310
.L4224:
	movl	(%r14), %eax
	cmpl	$-1, %eax
	je	.L4226
	cmpb	$2, 496(%r13)
	je	.L4226
	movb	$1, 496(%r13)
	movl	%eax, 500(%r13)
.L4226:
	cmpb	$0, -117(%rbp)
	jne	.L4311
	testb	%bl, %bl
	jne	.L4312
	movq	512(%r13), %rdi
	leal	-2(%r12), %eax
	movl	$4, %esi
	addq	$56, %rdi
	cmpl	$1, %eax
	jbe	.L4251
	cmpl	$8, %r12d
	je	.L4251
	testb	%r10b, %r10b
	je	.L4232
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movq	-104(%rbp), %rdx
	movl	-116(%rbp), %esi
	movq	%r15, %rdi
	movl	%eax, %ecx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder21CallUndefinedReceiverENS1_8RegisterENS1_12RegisterListEi@PLT
	jmp	.L4181
	.p2align 4,,10
	.p2align 3
.L4303:
	subl	$1, %eax
	movq	16(%r14), %rdx
	cltq
	movq	(%rdx,%rax,8), %rax
	movl	4(%rax), %eax
	andl	$63, %eax
	cmpb	$46, %al
	sete	-117(%rbp)
	jmp	.L4185
	.p2align 4,,10
	.p2align 3
.L4311:
	movq	512(%r13), %rdi
	movl	$4, %esi
	addq	$56, %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movq	-104(%rbp), %rdx
	movl	-116(%rbp), %esi
	movq	%r15, %rdi
	movl	%eax, %ecx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder14CallWithSpreadENS1_8RegisterENS1_12RegisterListEi@PLT
.L4181:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4313
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4302:
	.cfi_restore_state
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator14VisitCallSuperEPNS0_4CallE
	jmp	.L4181
.L4189:
	xorl	%ebx, %ebx
	xorl	%r9d, %r9d
	cmpb	$0, -117(%rbp)
	movl	$1, %r10d
	jne	.L4212
.L4211:
	leaq	-96(%rbp), %r11
	movq	%r15, %rsi
	movq	%r13, %rdi
	movl	%ecx, -80(%rbp)
	movq	816(%r13), %rax
	movq	%r11, 816(%r13)
	movl	%r9d, -144(%rbp)
	movb	%r10b, -132(%rbp)
	movq	%r11, -128(%rbp)
	movq	%rax, -96(%rbp)
	movq	%r13, -88(%rbp)
	movq	$2, -72(%rbp)
	call	_ZN2v88internal11interpreter17BytecodeGenerator5VisitEPNS0_7AstNodeE
	movl	-116(%rbp), %esi
	leaq	24(%r13), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-128(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator21ExpressionResultScopeD2Ev
	movzbl	-132(%rbp), %r10d
	movl	-144(%rbp), %r9d
	jmp	.L4188
.L4196:
	xorl	%ebx, %ebx
	xorl	%r9d, %r9d
	cmpb	$0, -117(%rbp)
	movl	$1, %r10d
	jne	.L4202
.L4201:
	movl	4(%r15), %eax
	movl	%r9d, -132(%rbp)
	movb	%r10b, -128(%rbp)
	movl	%eax, %edx
	andl	$63, %edx
	cmpb	$54, %dl
	jne	.L4203
	movq	8(%r15), %rsi
	shrl	$11, %eax
	movq	%r13, %rdi
	movl	$1, %ecx
	movq	816(%r13), %rdx
	andl	$1, %eax
	movq	%r13, -88(%rbp)
	movq	$2, -72(%rbp)
	movq	%rdx, -96(%rbp)
	movl	328(%r13), %edx
	movl	%edx, -80(%rbp)
	leaq	-96(%rbp), %rdx
	movq	%rdx, 816(%r13)
	movl	%eax, %edx
	call	_ZN2v88internal11interpreter17BytecodeGenerator17BuildVariableLoadEPNS0_8VariableENS0_13HoleCheckModeENS0_10TypeofModeE
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rax
	movzbl	-128(%rbp), %r10d
	movl	-132(%rbp), %r9d
	movl	328(%rdx), %esi
	movq	%rax, 816(%rdx)
	movq	336(%rdx), %rdi
	movl	-80(%rbp), %eax
	subl	%eax, %esi
	testq	%rdi, %rdi
	movl	%eax, 328(%rdx)
	je	.L4204
	movq	%rsi, %rcx
	movq	(%rdi), %rdx
	movl	%eax, %esi
	salq	$32, %rcx
	orq	%rcx, %rsi
	call	*32(%rdx)
	movl	-132(%rbp), %r9d
	movzbl	-128(%rbp), %r10d
.L4204:
	movl	-116(%rbp), %esi
	leaq	24(%r13), %rdi
	movl	%r9d, -132(%rbp)
	movb	%r10b, -128(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movzbl	-128(%rbp), %r10d
	movl	-132(%rbp), %r9d
	jmp	.L4188
	.p2align 4,,10
	.p2align 3
.L4310:
	movl	328(%r13), %ecx
	addl	-104(%rbp), %r9d
	movl	$6, %esi
	movb	%r10b, -156(%rbp)
	movq	-152(%rbp), %rdi
	movl	%r9d, -132(%rbp)
	movl	%ecx, -136(%rbp)
	call	_ZN2v88internal11interpreter25BytecodeRegisterAllocator15NewRegisterListEi
	movl	-116(%rbp), %esi
	movq	%r15, %rdi
	movl	%eax, %edx
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder12MoveRegisterENS1_8RegisterES3_@PLT
	movl	-132(%rbp), %r9d
	movl	-128(%rbp), %ecx
	movq	%rax, %rdi
	movl	%r9d, %esi
	leal	1(%rcx), %edx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder12MoveRegisterENS1_8RegisterES3_@PLT
	movl	-128(%rbp), %ecx
	movq	%rax, -144(%rbp)
	leal	2(%rcx), %edx
	movl	%edx, -132(%rbp)
	call	_ZN2v88internal11interpreter8Register16function_closureEv@PLT
	movl	-132(%rbp), %edx
	movq	-144(%rbp), %rdi
	movl	%eax, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder12MoveRegisterENS1_8RegisterES3_@PLT
	movq	%rax, %rdi
	movq	536(%r13), %rax
	movzbl	129(%rax), %esi
	andl	$1, %esi
	salq	$32, %rsi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralENS0_3SmiE@PLT
	movl	-128(%rbp), %ecx
	movq	%rax, %rdi
	leal	3(%rcx), %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	%rax, %rdi
	movq	536(%r13), %rax
	movslq	112(%rax), %rsi
	salq	$32, %rsi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralENS0_3SmiE@PLT
	movl	-128(%rbp), %ecx
	movq	%rax, %rdi
	leal	4(%rcx), %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movslq	(%r14), %rsi
	movq	%rax, %rdi
	salq	$32, %rsi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralENS0_3SmiE@PLT
	movl	-128(%rbp), %ecx
	movq	%rax, %rdi
	leal	5(%rcx), %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-128(%rbp), %rdx
	movl	$61, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_12RegisterListE@PLT
	movl	-116(%rbp), %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	336(%r13), %rdi
	movl	-136(%rbp), %ecx
	movl	328(%r13), %esi
	movzbl	-156(%rbp), %r10d
	testq	%rdi, %rdi
	movl	%ecx, 328(%r13)
	je	.L4224
	subl	%ecx, %esi
	movq	(%rdi), %rax
	movb	%r10b, -128(%rbp)
	movq	%rsi, %rdx
	movl	%ecx, %esi
	salq	$32, %rdx
	orq	%rdx, %rsi
	call	*32(%rax)
	movzbl	-128(%rbp), %r10d
	jmp	.L4224
	.p2align 4,,10
	.p2align 3
.L4232:
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movq	-104(%rbp), %rdx
	movl	-116(%rbp), %esi
	movq	%r15, %rdi
	movl	%eax, %ecx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder15CallAnyReceiverENS1_8RegisterENS1_12RegisterListEi@PLT
	jmp	.L4181
	.p2align 4,,10
	.p2align 3
.L4312:
	movq	-104(%rbp), %rdx
	movl	-116(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder14CallNoFeedbackENS1_8RegisterENS1_12RegisterListE@PLT
	jmp	.L4181
	.p2align 4,,10
	.p2align 3
.L4251:
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movq	-104(%rbp), %rdx
	movl	-116(%rbp), %esi
	movq	%r15, %rdi
	movl	%eax, %ecx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder12CallPropertyENS1_8RegisterENS1_12RegisterListEi@PLT
	jmp	.L4181
	.p2align 4,,10
	.p2align 3
.L4221:
	addl	%eax, %r9d
	cmpl	-100(%rbp), %r9d
	jne	.L4234
	leaq	24(%r13), %r15
	jmp	.L4224
	.p2align 4,,10
	.p2align 3
.L4234:
	leaq	.LC23(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L4212:
	leaq	-104(%rbp), %rdx
	movq	%r13, %rdi
	movq	%rdx, %rsi
	call	_ZN2v88internal11interpreter17BytecodeGenerator34BuildPushUndefinedIntoRegisterListEPNS1_12RegisterListE
	movl	328(%r13), %ecx
	movl	$1, %r9d
	xorl	%r10d, %r10d
	jmp	.L4211
.L4202:
	leaq	-104(%rbp), %rdx
	movq	%r13, %rdi
	movq	%rdx, %rsi
	call	_ZN2v88internal11interpreter17BytecodeGenerator34BuildPushUndefinedIntoRegisterListEPNS1_12RegisterListE
	movl	$1, %r9d
	xorl	%r10d, %r10d
	jmp	.L4201
.L4217:
	movl	%ecx, %edx
	xorl	%eax, %eax
	movl	$1, -100(%rbp)
	addl	%eax, %edx
	jmp	.L4218
.L4205:
	movl	$1, -100(%rbp)
	movl	%ecx, %r9d
	xorl	%eax, %eax
.L4235:
	addl	%eax, %r9d
	jmp	.L4206
.L4213:
	movl	%ecx, %edx
	xorl	%eax, %eax
	movl	$1, -100(%rbp)
	addl	%eax, %edx
	jmp	.L4214
.L4307:
	movl	-104(%rbp), %eax
	leal	-1(%rsi,%rax), %esi
	jmp	.L4198
.L4308:
	movl	-104(%rbp), %r9d
	jmp	.L4235
.L4306:
	movl	-104(%rbp), %edx
	addl	%eax, %edx
	jmp	.L4214
.L4305:
	movl	-104(%rbp), %edx
	addl	%eax, %edx
	jmp	.L4218
.L4215:
	leaq	.LC5(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L4313:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal11interpreter17BytecodeGenerator9VisitCallEPNS0_4CallE
	.cfi_startproc
	.type	_ZN2v88internal11interpreter17BytecodeGenerator9VisitCallEPNS0_4CallE.cold, @function
_ZN2v88internal11interpreter17BytecodeGenerator9VisitCallEPNS0_4CallE.cold:
.LFSB20938:
.L4203:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	4, %eax
	ud2
.L4209:
	movq	8, %rax
	ud2
	.cfi_endproc
.LFE20938:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator9VisitCallEPNS0_4CallE
	.size	_ZN2v88internal11interpreter17BytecodeGenerator9VisitCallEPNS0_4CallE, .-_ZN2v88internal11interpreter17BytecodeGenerator9VisitCallEPNS0_4CallE
	.section	.text.unlikely._ZN2v88internal11interpreter17BytecodeGenerator9VisitCallEPNS0_4CallE
	.size	_ZN2v88internal11interpreter17BytecodeGenerator9VisitCallEPNS0_4CallE.cold, .-_ZN2v88internal11interpreter17BytecodeGenerator9VisitCallEPNS0_4CallE.cold
.LCOLDE24:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator9VisitCallEPNS0_4CallE
.LHOTE24:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator17VisitForStatementEPNS0_12ForStatementE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator17VisitForStatementEPNS0_12ForStatementE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator17VisitForStatementEPNS0_12ForStatementE, @function
_ZN2v88internal11interpreter17BytecodeGenerator17VisitForStatementEPNS0_12ForStatementE:
.LFB20771:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal11interpreter11LoopBuilderE(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	leaq	-168(%rbp), %rdx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	24(%rdi), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$216, %rsp
	movq	568(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	movq	%r14, -184(%rbp)
	movq	%rdx, -160(%rbp)
	movq	%rax, -176(%rbp)
	movq	%rax, -112(%rbp)
	leaq	-104(%rbp), %rax
	movq	%rdx, -168(%rbp)
	movq	$0, -152(%rbp)
	movb	$0, -144(%rbp)
	movq	%rsi, -136(%rbp)
	movq	%r13, -128(%rbp)
	movq	%rcx, -192(%rbp)
	movq	$-1, -120(%rbp)
	movq	%rax, -96(%rbp)
	movq	%rax, -104(%rbp)
	movq	$0, -88(%rbp)
	movb	$0, -80(%rbp)
	testq	%r13, %r13
	je	.L4315
	movq	40(%r13), %rax
	leaq	16(%rax), %rcx
	movq	24(%rax), %rax
	movq	%rcx, %rdx
	testq	%rax, %rax
	jne	.L4318
	.p2align 4,,10
	.p2align 3
.L4322:
	movl	$-1, %r15d
.L4317:
	movl	%r15d, -72(%rbp)
.L4315:
	movq	32(%rbx), %r13
	testq	%r13, %r13
	je	.L4323
	cmpb	$0, 8(%r12)
	je	.L4363
.L4323:
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L4325
	call	_ZNK2v88internal10Expression16ToBooleanIsFalseEv@PLT
	testb	%al, %al
	jne	.L4364
.L4325:
	leaq	-192(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter11LoopBuilder10LoopHeaderEv@PLT
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L4328
	call	_ZNK2v88internal10Expression15ToBooleanIsTrueEv@PLT
	testb	%al, %al
	jne	.L4328
	movq	40(%rbx), %rsi
	movl	(%rsi), %eax
	cmpl	$-1, %eax
	je	.L4330
	movb	$2, 496(%r12)
	movl	%eax, 500(%r12)
	movq	40(%rbx), %rsi
.L4330:
	movq	16(%r12), %rax
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	-240(%rbp), %r9
	movq	%r9, %rdx
	leaq	-232(%rbp), %r15
	leaq	-176(%rbp), %rcx
	movq	%r9, -248(%rbp)
	movq	%rax, -240(%rbp)
	movq	%r15, -224(%rbp)
	movq	%r15, -232(%rbp)
	movq	$0, -216(%rbp)
	movb	$0, -208(%rbp)
	call	_ZN2v88internal11interpreter17BytecodeGenerator12VisitForTestEPNS0_10ExpressionEPNS1_14BytecodeLabelsES6_NS2_15TestFallthroughE
	movq	-248(%rbp), %r9
	movq	%r14, %rsi
	movq	%r9, %rdi
	call	_ZN2v88internal11interpreter14BytecodeLabels4BindEPNS1_20BytecodeArrayBuilderE@PLT
	movq	-232(%rbp), %rax
	cmpq	%r15, %rax
	je	.L4328
	.p2align 4,,10
	.p2align 3
.L4332:
	movq	(%rax), %rax
	cmpq	%r15, %rax
	jne	.L4332
.L4328:
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator18VisitIterationBodyEPNS0_18IterationStatementEPNS1_11LoopBuilderE
	movq	48(%rbx), %r14
	testq	%r14, %r14
	je	.L4334
	movl	(%r14), %eax
	cmpl	$-1, %eax
	je	.L4335
	movb	$2, 496(%r12)
	movl	%eax, 500(%r12)
	movq	48(%rbx), %r14
.L4335:
	cmpb	$0, 8(%r12)
	je	.L4365
.L4334:
	movl	868(%r12), %esi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter11LoopBuilder12JumpToHeaderEi@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter11LoopBuilderD1Ev@PLT
.L4314:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4366
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4367:
	.cfi_restore_state
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L4319
.L4318:
	cmpq	%rbx, 32(%rax)
	jnb	.L4367
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L4318
.L4319:
	cmpq	%rdx, %rcx
	je	.L4322
	cmpq	%rbx, 32(%rdx)
	ja	.L4322
	movq	40(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L4322
	movq	(%rdi), %rax
	xorl	%esi, %esi
	call	*16(%rax)
	movq	%rax, -240(%rbp)
	cmpl	$-1, %eax
	je	.L4322
	movq	16(%r13), %r15
	leaq	-240(%rbp), %rsi
	subq	8(%r13), %r15
	movq	%r13, %rdi
	sarq	$3, %r15
	call	_ZNSt6vectorIN2v88internal11SourceRangeENS1_13ZoneAllocatorIS2_EEE12emplace_backIJRS2_EEEvDpOT_
	jmp	.L4317
	.p2align 4,,10
	.p2align 3
.L4365:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L4368
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	jmp	.L4334
	.p2align 4,,10
	.p2align 3
.L4363:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L4369
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	jmp	.L4323
	.p2align 4,,10
	.p2align 3
.L4368:
	movb	$1, 8(%r12)
	jmp	.L4334
	.p2align 4,,10
	.p2align 3
.L4369:
	movb	$1, 8(%r12)
	jmp	.L4323
	.p2align 4,,10
	.p2align 3
.L4364:
	leaq	-192(%rbp), %rdi
	call	_ZN2v88internal11interpreter11LoopBuilderD1Ev@PLT
	jmp	.L4314
.L4366:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20771:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator17VisitForStatementEPNS0_12ForStatementE, .-_ZN2v88internal11interpreter17BytecodeGenerator17VisitForStatementEPNS0_12ForStatementE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator20VisitBinaryOperationEPNS0_15BinaryOperationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator20VisitBinaryOperationEPNS0_15BinaryOperationE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator20VisitBinaryOperationEPNS0_15BinaryOperationE, @function
_ZN2v88internal11interpreter17BytecodeGenerator20VisitBinaryOperationEPNS0_15BinaryOperationE:
.LFB20949:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$24, %rsp
	movl	4(%rsi), %eax
	shrl	$7, %eax
	andl	$127, %eax
	cmpb	$32, %al
	je	.L4371
	ja	.L4372
	cmpb	$30, %al
	je	.L4373
	cmpb	$31, %al
	jne	.L4375
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator22VisitNullishExpressionEPNS0_15BinaryOperationE
	.p2align 4,,10
	.p2align 3
.L4372:
	.cfi_restore_state
	cmpb	$33, %al
	jne	.L4375
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator25VisitLogicalAndExpressionEPNS0_15BinaryOperationE
	.p2align 4,,10
	.p2align 3
.L4371:
	.cfi_restore_state
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator24VisitLogicalOrExpressionEPNS0_15BinaryOperationE
	.p2align 4,,10
	.p2align 3
.L4373:
	.cfi_restore_state
	movq	8(%rsi), %rsi
	movq	%rdi, -24(%rbp)
	call	_ZN2v88internal11interpreter17BytecodeGenerator14VisitForEffectEPNS0_10ExpressionE
	movq	-24(%rbp), %rdi
	cmpb	$0, 8(%rdi)
	jne	.L4370
	movq	16(%r12), %r12
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	movq	-24(%rbp), %rdi
	cmpq	(%rdi), %rax
	jb	.L4380
	addq	$24, %rsp
	movq	%r12, %rsi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	.p2align 4,,10
	.p2align 3
.L4375:
	.cfi_restore_state
	addq	$24, %rsp
	movq	%r12, %rsi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator25VisitArithmeticExpressionEPNS0_15BinaryOperationE
	.p2align 4,,10
	.p2align 3
.L4380:
	.cfi_restore_state
	movb	$1, 8(%rdi)
.L4370:
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20949:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator20VisitBinaryOperationEPNS0_15BinaryOperationE, .-_ZN2v88internal11interpreter17BytecodeGenerator20VisitBinaryOperationEPNS0_15BinaryOperationE
	.section	.text.unlikely._ZN2v88internal11interpreter17BytecodeGenerator34BuildDestructuringObjectAssignmentEPNS0_13ObjectLiteralENS0_5Token5ValueENS0_18LookupHoistingModeE,"ax",@progbits
	.align 2
.LCOLDB25:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator34BuildDestructuringObjectAssignmentEPNS0_13ObjectLiteralENS0_5Token5ValueENS0_18LookupHoistingModeE,"ax",@progbits
.LHOTB25:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator34BuildDestructuringObjectAssignmentEPNS0_13ObjectLiteralENS0_5Token5ValueENS0_18LookupHoistingModeE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator34BuildDestructuringObjectAssignmentEPNS0_13ObjectLiteralENS0_5Token5ValueENS0_18LookupHoistingModeE, @function
_ZN2v88internal11interpreter17BytecodeGenerator34BuildDestructuringObjectAssignmentEPNS0_13ObjectLiteralENS0_5Token5ValueENS0_18LookupHoistingModeE:
.LFB20917:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$184, %rsp
	movl	%ecx, -172(%rbp)
	movl	328(%rdi), %ecx
	movb	%dl, -157(%rbp)
	movl	332(%rdi), %edx
	movq	%rsi, -192(%rbp)
	movq	336(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%ecx, -144(%rbp)
	movq	$0, -200(%rbp)
	testb	$4, 5(%rsi)
	je	.L4382
	movl	36(%rsi), %r13d
	movl	%ecx, %r12d
	leal	(%r12,%r13), %ecx
	cmpl	%edx, %ecx
	movl	%ecx, 328(%rbx)
	cmovge	%ecx, %edx
	movl	%edx, 332(%rbx)
	testq	%rdi, %rdi
	je	.L4383
	movq	%r13, %rsi
	movq	(%rdi), %rax
	salq	$32, %rsi
	orq	%r12, %rsi
	call	*24(%rax)
.L4383:
	movq	%r13, %rax
	salq	$32, %rax
	orq	%rax, %r12
	movl	-144(%rbp), %eax
	movq	%r12, -200(%rbp)
	movl	%eax, -176(%rbp)
.L4384:
	movl	-144(%rbp), %esi
	leaq	24(%rbx), %r15
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-192(%rbp), %rcx
	movslq	36(%rcx), %rax
	testl	%eax, %eax
	je	.L4386
	movq	24(%rcx), %rdx
	movq	(%rdx), %rcx
	testb	$3, (%rcx)
	jne	.L4463
.L4387:
	leaq	(%rdx,%rax,8), %rax
	movq	%rdx, %r13
	movl	$1, -156(%rbp)
	movq	%rax, -168(%rbp)
	cmpq	%rdx, %rax
	je	.L4411
	.p2align 4,,10
	.p2align 3
.L4414:
	movq	0(%r13), %r14
	movl	328(%rbx), %eax
	movq	$0, -136(%rbp)
	movq	8(%r14), %rdx
	movq	(%r14), %r9
	movl	%eax, -140(%rbp)
	movzbl	4(%rdx), %eax
	andl	$63, %eax
	cmpb	$24, %al
	jne	.L4392
	movq	16(%rdx), %rax
	movq	8(%rdx), %rdx
	movq	%rax, -136(%rbp)
	movzbl	4(%rdx), %eax
	andl	$63, %eax
.L4392:
	subl	$22, %eax
	cmpb	$1, %al
	jbe	.L4393
	movl	(%rdx), %eax
	cmpl	$-1, %eax
	je	.L4393
	movb	$2, 496(%rbx)
	movl	%eax, 500(%rbx)
.L4393:
	cmpb	$6, 16(%r14)
	je	.L4394
	andq	$-4, %r9
	movq	%rdx, -184(%rbp)
	movq	%r9, %rdi
	movq	%r9, -152(%rbp)
	call	_ZNK2v88internal10Expression14IsPropertyNameEv@PLT
	movq	-152(%rbp), %r9
	movq	-184(%rbp), %rdx
	testb	%al, %al
	jne	.L4395
	movq	-192(%rbp), %rax
	testb	$4, 5(%rax)
	jne	.L4464
.L4397:
	movl	328(%rbx), %esi
	leal	1(%rsi), %eax
	cmpl	%eax, 332(%rbx)
	movl	%esi, -184(%rbp)
	movl	%eax, 328(%rbx)
	cmovge	332(%rbx), %eax
	movl	%eax, 332(%rbx)
	movq	336(%rbx), %rax
	movq	%rax, -152(%rbp)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L4400
	movq	(%rax), %rax
	movq	%r9, -216(%rbp)
	movq	%rdx, -208(%rbp)
	call	*16(%rax)
	movq	-216(%rbp), %r9
	movq	$0, -152(%rbp)
	movq	-208(%rbp), %rdx
.L4400:
	testb	$3, (%r14)
	je	.L4401
.L4467:
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%rdx, -208(%rbp)
	leaq	-112(%rbp), %r12
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	movl	-184(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder6ToNameENS1_8RegisterE@PLT
	movq	-208(%rbp), %rdx
.L4402:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator20PrepareAssignmentLhsEPNS0_10ExpressionENS2_25AccumulatorPreservingModeE.constprop.0
	cmpb	$6, 16(%r14)
	je	.L4416
	cmpq	$0, -152(%rbp)
	je	.L4408
.L4417:
	movq	512(%rbx), %rax
	movl	$5, %esi
	leaq	56(%rax), %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movq	-152(%rbp), %rdx
	movl	-144(%rbp), %esi
	movq	%r15, %rdi
	movl	%eax, %ecx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder17LoadNamedPropertyENS1_8RegisterEPKNS0_12AstRawStringEi@PLT
.L4407:
	cmpq	$0, -136(%rbp)
	je	.L4409
	leaq	-128(%rbp), %r14
	movq	%r15, %rdi
	movb	$0, -128(%rbp)
	movq	%r14, %rsi
	movq	$-1, -120(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder18JumpIfNotUndefinedEPNS1_13BytecodeLabelE@PLT
	movq	-136(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4BindEPNS1_13BytecodeLabelE@PLT
.L4409:
	movl	-172(%rbp), %ecx
	movzbl	-157(%rbp), %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator15BuildAssignmentERKNS2_17AssignmentLhsDataENS0_5Token5ValueENS0_18LookupHoistingModeE
	movl	-140(%rbp), %ecx
	movq	336(%rbx), %rdi
	movl	328(%rbx), %esi
	movl	%ecx, 328(%rbx)
	testq	%rdi, %rdi
	je	.L4410
	subl	%ecx, %esi
	movq	(%rdi), %rax
	movl	%ecx, %edx
	addq	$8, %r13
	salq	$32, %rsi
	orq	%rdx, %rsi
	call	*32(%rax)
	addl	$1, -156(%rbp)
	cmpq	%r13, -168(%rbp)
	jne	.L4414
.L4411:
	movq	816(%rbx), %rax
	cmpl	$1, 24(%rax)
	jne	.L4390
.L4391:
	movl	-144(%rbp), %eax
	movq	336(%rbx), %rdi
	movl	328(%rbx), %esi
	movl	%eax, 328(%rbx)
	testq	%rdi, %rdi
	je	.L4381
	subl	%eax, %esi
	movq	(%rdi), %rdx
	salq	$32, %rsi
	orq	%rax, %rsi
	call	*32(%rdx)
.L4381:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4465
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4394:
	.cfi_restore_state
	leaq	-112(%rbp), %r12
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator20PrepareAssignmentLhsEPNS0_10ExpressionENS2_25AccumulatorPreservingModeE.constprop.0
	cmpb	$6, 16(%r14)
	jne	.L4466
.L4416:
	movl	-176(%rbp), %eax
	movl	$211, %esi
	movq	%r15, %rdi
	movabsq	$-4294967296, %rdx
	andq	-200(%rbp), %rdx
	orq	%rax, %rdx
	movq	%rdx, -200(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_12RegisterListE@PLT
	jmp	.L4407
	.p2align 4,,10
	.p2align 3
.L4410:
	addl	$1, -156(%rbp)
	addq	$8, %r13
	cmpq	%r13, -168(%rbp)
	jne	.L4414
	movq	816(%rbx), %rax
	cmpl	$1, 24(%rax)
	jne	.L4390
	movl	-144(%rbp), %eax
	movl	%eax, 328(%rbx)
	jmp	.L4381
	.p2align 4,,10
	.p2align 3
.L4395:
	movzbl	4(%r9), %eax
	andl	$63, %eax
	cmpb	$41, %al
	jne	.L4398
	movq	8(%r9), %rax
	movq	-192(%rbp), %rcx
	movq	%rax, -152(%rbp)
	testb	$4, 5(%rcx)
	jne	.L4396
	testq	%rax, %rax
	je	.L4397
	leaq	-112(%rbp), %r12
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator20PrepareAssignmentLhsEPNS0_10ExpressionENS2_25AccumulatorPreservingModeE.constprop.0
	cmpb	$6, 16(%r14)
	jne	.L4417
	jmp	.L4416
	.p2align 4,,10
	.p2align 3
.L4466:
	movl	$2147483647, -184(%rbp)
.L4408:
	movl	-184(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	movl	$8, %esi
	movq	%rax, %r14
	movq	512(%rbx), %rax
	leaq	56(%rax), %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movl	-144(%rbp), %esi
	movq	%r14, %rdi
	movl	%eax, %edx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder17LoadKeyedPropertyENS1_8RegisterEi@PLT
	jmp	.L4407
	.p2align 4,,10
	.p2align 3
.L4464:
	movq	$0, -152(%rbp)
.L4396:
	movl	-176(%rbp), %eax
	addl	-156(%rbp), %eax
	movl	%eax, -184(%rbp)
	testb	$3, (%r14)
	jne	.L4467
.L4401:
	movq	816(%rbx), %rax
	leaq	-112(%rbp), %r12
	cmpb	$0, 8(%rbx)
	movq	%r9, -208(%rbp)
	movq	%rbx, -104(%rbp)
	movq	%rax, -112(%rbp)
	movl	328(%rbx), %eax
	movq	$2, -88(%rbp)
	movl	%eax, -96(%rbp)
	movq	%r12, 816(%rbx)
	jne	.L4403
	movq	%rdx, -216(%rbp)
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%rbx), %rax
	movq	-216(%rbp), %rdx
	movq	-208(%rbp), %r9
	jb	.L4468
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%rdx, -208(%rbp)
	call	_ZN2v88internal11interpreter17BytecodeGenerator25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movq	-208(%rbp), %rdx
.L4403:
	movl	-184(%rbp), %esi
	movq	%r15, %rdi
	movq	%rdx, -208(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-104(%rbp), %r9
	movq	-112(%rbp), %rax
	movq	-208(%rbp), %rdx
	movq	336(%r9), %rdi
	movq	%rax, 816(%r9)
	movl	-96(%rbp), %eax
	movl	328(%r9), %esi
	testq	%rdi, %rdi
	movl	%eax, 328(%r9)
	je	.L4402
	subl	%eax, %esi
	movq	(%rdi), %r9
	salq	$32, %rsi
	movq	%rsi, %r10
	movl	%eax, %esi
	orq	%r10, %rsi
	call	*32(%r9)
	movq	-208(%rbp), %rdx
	jmp	.L4402
	.p2align 4,,10
	.p2align 3
.L4463:
	cmpb	$6, 16(%rcx)
	je	.L4387
.L4386:
	leaq	-128(%rbp), %r13
	movq	%r15, %rdi
	movb	$0, -128(%rbp)
	leaq	-112(%rbp), %r12
	movq	%r13, %rsi
	movq	$-1, -120(%rbp)
	movb	$0, -112(%rbp)
	movq	$-1, -104(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder21JumpIfUndefinedOrNullEPNS1_13BytecodeLabelE@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4JumpEPNS1_13BytecodeLabelE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4BindEPNS1_13BytecodeLabelE@PLT
	movq	-192(%rbp), %rax
	movl	(%rax), %eax
	cmpl	$-1, %eax
	je	.L4388
	cmpb	$2, 496(%rbx)
	je	.L4388
	movb	$1, 496(%rbx)
	movl	%eax, 500(%rbx)
.L4388:
	movl	-144(%rbp), %edx
	movl	$170, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_8RegisterE@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4BindEPNS1_13BytecodeLabelE@PLT
	movq	-192(%rbp), %rax
	movq	24(%rax), %rdx
	movslq	36(%rax), %rax
	jmp	.L4387
	.p2align 4,,10
	.p2align 3
.L4382:
	leal	1(%rcx), %eax
	movl	%ecx, %esi
	cmpl	%edx, %eax
	movl	%eax, 328(%rbx)
	cmovl	%edx, %eax
	movl	%eax, 332(%rbx)
	testq	%rdi, %rdi
	je	.L4385
	movq	(%rdi), %rax
	call	*16(%rax)
.L4385:
	movl	$2147483647, -176(%rbp)
	jmp	.L4384
	.p2align 4,,10
	.p2align 3
.L4390:
	movl	-144(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	jmp	.L4391
	.p2align 4,,10
	.p2align 3
.L4468:
	movb	$1, 8(%rbx)
	jmp	.L4403
.L4465:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal11interpreter17BytecodeGenerator34BuildDestructuringObjectAssignmentEPNS0_13ObjectLiteralENS0_5Token5ValueENS0_18LookupHoistingModeE
	.cfi_startproc
	.type	_ZN2v88internal11interpreter17BytecodeGenerator34BuildDestructuringObjectAssignmentEPNS0_13ObjectLiteralENS0_5Token5ValueENS0_18LookupHoistingModeE.cold, @function
_ZN2v88internal11interpreter17BytecodeGenerator34BuildDestructuringObjectAssignmentEPNS0_13ObjectLiteralENS0_5Token5ValueENS0_18LookupHoistingModeE.cold:
.LFSB20917:
.L4398:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	8, %rax
	ud2
	.cfi_endproc
.LFE20917:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator34BuildDestructuringObjectAssignmentEPNS0_13ObjectLiteralENS0_5Token5ValueENS0_18LookupHoistingModeE
	.size	_ZN2v88internal11interpreter17BytecodeGenerator34BuildDestructuringObjectAssignmentEPNS0_13ObjectLiteralENS0_5Token5ValueENS0_18LookupHoistingModeE, .-_ZN2v88internal11interpreter17BytecodeGenerator34BuildDestructuringObjectAssignmentEPNS0_13ObjectLiteralENS0_5Token5ValueENS0_18LookupHoistingModeE
	.section	.text.unlikely._ZN2v88internal11interpreter17BytecodeGenerator34BuildDestructuringObjectAssignmentEPNS0_13ObjectLiteralENS0_5Token5ValueENS0_18LookupHoistingModeE
	.size	_ZN2v88internal11interpreter17BytecodeGenerator34BuildDestructuringObjectAssignmentEPNS0_13ObjectLiteralENS0_5Token5ValueENS0_18LookupHoistingModeE.cold, .-_ZN2v88internal11interpreter17BytecodeGenerator34BuildDestructuringObjectAssignmentEPNS0_13ObjectLiteralENS0_5Token5ValueENS0_18LookupHoistingModeE.cold
.LCOLDE25:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator34BuildDestructuringObjectAssignmentEPNS0_13ObjectLiteralENS0_5Token5ValueENS0_18LookupHoistingModeE
.LHOTE25:
	.section	.text.unlikely._ZN2v88internal11interpreter17BytecodeGenerator15BuildAssignmentERKNS2_17AssignmentLhsDataENS0_5Token5ValueENS0_18LookupHoistingModeE,"ax",@progbits
	.align 2
.LCOLDB26:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator15BuildAssignmentERKNS2_17AssignmentLhsDataENS0_5Token5ValueENS0_18LookupHoistingModeE,"ax",@progbits
.LHOTB26:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator15BuildAssignmentERKNS2_17AssignmentLhsDataENS0_5Token5ValueENS0_18LookupHoistingModeE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator15BuildAssignmentERKNS2_17AssignmentLhsDataENS0_5Token5ValueENS0_18LookupHoistingModeE, @function
_ZN2v88internal11interpreter17BytecodeGenerator15BuildAssignmentERKNS2_17AssignmentLhsDataENS0_5Token5ValueENS0_18LookupHoistingModeE:
.LFB20918:
	.cfi_startproc
	endbr64
	cmpl	$8, (%rsi)
	ja	.L4504
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movl	(%rsi), %eax
	leaq	.L4472(%rip), %rsi
	movslq	(%rsi,%rax,4), %rax
	addq	%rsi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal11interpreter17BytecodeGenerator15BuildAssignmentERKNS2_17AssignmentLhsDataENS0_5Token5ValueENS0_18LookupHoistingModeE,"a",@progbits
	.align 4
	.align 4
.L4472:
	.long	.L4479-.L4472
	.long	.L4478-.L4472
	.long	.L4477-.L4472
	.long	.L4476-.L4472
	.long	.L4475-.L4472
	.long	.L4474-.L4472
	.long	.L4473-.L4472
	.long	.L4471-.L4472
	.long	.L4471-.L4472
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator15BuildAssignmentERKNS2_17AssignmentLhsDataENS0_5Token5ValueENS0_18LookupHoistingModeE
	.p2align 4,,10
	.p2align 3
.L4471:
	movl	328(%rdi), %r13d
	leal	1(%r13), %eax
	cmpl	%eax, 332(%rdi)
	movl	%eax, 328(%rdi)
	cmovge	332(%rdi), %eax
	movl	%eax, 332(%rdi)
	movq	336(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L4488
	movq	(%rdi), %rax
	movl	%r13d, %esi
	call	*16(%rax)
.L4488:
	leaq	24(%r12), %r15
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	8(%rbx), %rbx
	movzbl	4(%rbx), %eax
	andl	$63, %eax
	cmpb	$44, %al
	jne	.L4489
	movq	8(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator21VisitForRegisterValueEPNS0_10ExpressionE
	movq	16(%rbx), %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal11interpreter17BytecodeGenerator21VisitForRegisterValueEPNS0_10ExpressionE
	movl	%r14d, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	%eax, -52(%rbp)
	call	_ZN2v88internal11interpreter17BytecodeGenerator22BuildPrivateBrandCheckEPNS0_8PropertyENS1_8RegisterE
	movl	-52(%rbp), %r8d
	movl	%r13d, %ecx
	movl	%r14d, %esi
	movq	%r12, %rdi
	movl	%r8d, %edx
	call	_ZN2v88internal11interpreter17BytecodeGenerator24BuildPrivateSetterAccessENS1_8RegisterES3_S3_
	movq	816(%r12), %rax
	cmpl	$1, 24(%rax)
	jne	.L4510
.L4469:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4479:
	.cfi_restore_state
	movq	8(%rbx), %rsi
	movl	4(%rsi), %edi
	movl	%edi, %eax
	andl	$63, %eax
	cmpb	$22, %al
	jne	.L4480
	testq	%rsi, %rsi
	je	.L4481
	addq	$24, %rsp
	movq	%r12, %rdi
	movzbl	%dl, %edx
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator34BuildDestructuringObjectAssignmentEPNS0_13ObjectLiteralENS0_5Token5ValueENS0_18LookupHoistingModeE
	.p2align 4,,10
	.p2align 3
.L4476:
	.cfi_restore_state
	movl	16(%rbx), %eax
	leaq	24(%rdi), %rdi
	leal	3(%rax), %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	16(%rbx), %rdx
	movl	$39, %esi
	movq	%rax, %rdi
.L4508:
	addq	$24, %rsp
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_12RegisterListE@PLT
	.p2align 4,,10
	.p2align 3
.L4475:
	.cfi_restore_state
	movl	16(%rbx), %eax
	leaq	24(%rdi), %rdi
	leal	3(%rax), %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	16(%rbx), %rdx
	movl	$38, %esi
	movq	%rax, %rdi
	jmp	.L4508
	.p2align 4,,10
	.p2align 3
.L4474:
	movq	8(%rbx), %rdx
	movl	$261, %esi
	movzbl	4(%rdx), %eax
	andl	$63, %eax
	cmpb	$44, %al
	movl	$0, %eax
	cmovne	%rax, %rdx
.L4509:
	addq	$24, %rsp
	movq	%r12, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator26BuildInvalidPropertyAccessENS0_15MessageTemplateEPNS0_8PropertyE
	.p2align 4,,10
	.p2align 3
.L4473:
	.cfi_restore_state
	movq	8(%rbx), %rdx
	movl	$263, %esi
	movzbl	4(%rdx), %eax
	andl	$63, %eax
	cmpb	$44, %al
	movl	$0, %eax
	cmovne	%rax, %rdx
	jmp	.L4509
	.p2align 4,,10
	.p2align 3
.L4478:
	movq	40(%rbx), %rcx
	movl	24(%rbx), %edx
	movq	32(%rbx), %rsi
	addq	$24, %rsp
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator23BuildStoreNamedPropertyEPKNS0_10ExpressionENS1_8RegisterEPKNS0_12AstRawStringE
	.p2align 4,,10
	.p2align 3
.L4477:
	.cfi_restore_state
	movq	512(%rdi), %rax
	movl	$2147483647, %r14d
	leaq	24(%r12), %r15
	leaq	56(%rax), %rdi
	movq	536(%r12), %rax
	movzbl	129(%rax), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%esi, %esi
	andl	$-10, %esi
	addl	$13, %esi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movl	%eax, %r13d
	movq	816(%r12), %rax
	cmpl	$1, 24(%rax)
	je	.L4484
	leaq	328(%r12), %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterAllocator11NewRegisterEv
	movq	%r15, %rdi
	movl	%eax, %esi
	movl	%eax, %r14d
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
.L4484:
	movl	24(%rbx), %esi
	movl	28(%rbx), %edx
	movl	%r13d, %ecx
	movq	%r15, %rdi
	movq	536(%r12), %rax
	movzbl	129(%rax), %r8d
	andl	$1, %r8d
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder18StoreKeyedPropertyENS1_8RegisterES3_iNS0_12LanguageModeE@PLT
	movq	816(%r12), %rax
	movl	%r14d, %esi
	cmpl	$1, 24(%rax)
	je	.L4469
.L4507:
	addq	$24, %rsp
	movq	%r15, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	.p2align 4,,10
	.p2align 3
.L4510:
	.cfi_restore_state
	movl	%r13d, %esi
	jmp	.L4507
	.p2align 4,,10
	.p2align 3
.L4480:
	cmpb	$23, %al
	jne	.L4482
	testq	%rsi, %rsi
	je	.L4481
	addq	$24, %rsp
	movq	%r12, %rdi
	movzbl	%dl, %edx
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator33BuildDestructuringArrayAssignmentEPNS0_12ArrayLiteralENS0_5Token5ValueENS0_18LookupHoistingModeE
.L4482:
	.cfi_restore_state
	cmpb	$54, %al
	jne	.L4481
	shrl	$11, %edi
	movq	8(%rsi), %rsi
	addq	$24, %rsp
	movl	%ecx, %r8d
	andl	$1, %edi
	popq	%rbx
	.cfi_restore 3
	movzbl	%dl, %edx
	movl	%edi, %ecx
	movq	%r12, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter17BytecodeGenerator23BuildVariableAssignmentEPNS0_8VariableENS0_5Token5ValueENS0_13HoleCheckModeENS0_18LookupHoistingModeE
.L4504:
	ret
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal11interpreter17BytecodeGenerator15BuildAssignmentERKNS2_17AssignmentLhsDataENS0_5Token5ValueENS0_18LookupHoistingModeE
	.cfi_startproc
	.type	_ZN2v88internal11interpreter17BytecodeGenerator15BuildAssignmentERKNS2_17AssignmentLhsDataENS0_5Token5ValueENS0_18LookupHoistingModeE.cold, @function
_ZN2v88internal11interpreter17BytecodeGenerator15BuildAssignmentERKNS2_17AssignmentLhsDataENS0_5Token5ValueENS0_18LookupHoistingModeE.cold:
.LFSB20918:
.L4489:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	8, %rax
	ud2
.L4481:
	movl	4, %eax
	ud2
	.cfi_endproc
.LFE20918:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator15BuildAssignmentERKNS2_17AssignmentLhsDataENS0_5Token5ValueENS0_18LookupHoistingModeE
	.size	_ZN2v88internal11interpreter17BytecodeGenerator15BuildAssignmentERKNS2_17AssignmentLhsDataENS0_5Token5ValueENS0_18LookupHoistingModeE, .-_ZN2v88internal11interpreter17BytecodeGenerator15BuildAssignmentERKNS2_17AssignmentLhsDataENS0_5Token5ValueENS0_18LookupHoistingModeE
	.section	.text.unlikely._ZN2v88internal11interpreter17BytecodeGenerator15BuildAssignmentERKNS2_17AssignmentLhsDataENS0_5Token5ValueENS0_18LookupHoistingModeE
	.size	_ZN2v88internal11interpreter17BytecodeGenerator15BuildAssignmentERKNS2_17AssignmentLhsDataENS0_5Token5ValueENS0_18LookupHoistingModeE.cold, .-_ZN2v88internal11interpreter17BytecodeGenerator15BuildAssignmentERKNS2_17AssignmentLhsDataENS0_5Token5ValueENS0_18LookupHoistingModeE.cold
.LCOLDE26:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator15BuildAssignmentERKNS2_17AssignmentLhsDataENS0_5Token5ValueENS0_18LookupHoistingModeE
.LHOTE26:
	.section	.text.unlikely._ZN2v88internal11interpreter17BytecodeGenerator23VisitCompoundAssignmentEPNS0_18CompoundAssignmentE,"ax",@progbits
	.align 2
.LCOLDB27:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator23VisitCompoundAssignmentEPNS0_18CompoundAssignmentE,"ax",@progbits
.LHOTB27:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator23VisitCompoundAssignmentEPNS0_18CompoundAssignmentE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator23VisitCompoundAssignmentEPNS0_18CompoundAssignmentE, @function
_ZN2v88internal11interpreter17BytecodeGenerator23VisitCompoundAssignmentEPNS0_18CompoundAssignmentE:
.LFB20920:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-112(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$88, %rsp
	movq	8(%rsi), %rdx
	movq	%rdi, %rsi
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter17BytecodeGenerator20PrepareAssignmentLhsEPNS0_10ExpressionENS2_25AccumulatorPreservingModeE.constprop.0
	cmpl	$8, -112(%rbp)
	ja	.L4512
	movl	-112(%rbp), %eax
	leaq	.L4514(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal11interpreter17BytecodeGenerator23VisitCompoundAssignmentEPNS0_18CompoundAssignmentE,"a",@progbits
	.align 4
	.align 4
.L4514:
	.long	.L4519-.L4514
	.long	.L4518-.L4514
	.long	.L4517-.L4514
	.long	.L4516-.L4514
	.long	.L4515-.L4514
	.long	.L4513-.L4514
	.long	.L4513-.L4514
	.long	.L4513-.L4514
	.long	.L4513-.L4514
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator23VisitCompoundAssignmentEPNS0_18CompoundAssignmentE
	.p2align 4,,10
	.p2align 3
.L4519:
	movq	8(%rbx), %rax
	movl	4(%rax), %edx
	movl	%edx, %ecx
	andl	$63, %ecx
	cmpb	$54, %cl
	jne	.L4520
	shrl	$11, %edx
	movq	8(%rax), %rsi
	movl	$1, %ecx
	movq	%r12, %rdi
	andl	$1, %edx
	call	_ZN2v88internal11interpreter17BytecodeGenerator17BuildVariableLoadEPNS0_8VariableENS0_13HoleCheckModeENS0_10TypeofModeE
	.p2align 4,,10
	.p2align 3
.L4512:
	movzbl	4(%rbx), %eax
	andl	$63, %eax
	cmpb	$33, %al
	jne	.L4521
	movq	512(%r12), %rax
	movl	$15, %esi
	movq	24(%rbx), %r15
	leaq	56(%rax), %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movq	16(%rbx), %rdi
	movl	%eax, %r14d
	call	_ZNK2v88internal10Expression12IsSmiLiteralEv@PLT
	leaq	24(%r12), %r8
	testb	%al, %al
	je	.L4522
	movq	16(%rbx), %rdx
	movzbl	4(%rdx), %eax
	andl	$63, %eax
	cmpb	$41, %al
	jne	.L4523
	movl	4(%r15), %esi
	movslq	8(%rdx), %rdx
	movl	%r14d, %ecx
	movq	%r8, %rdi
	shrl	$7, %esi
	salq	$32, %rdx
	andl	$127, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder25BinaryOperationSmiLiteralENS0_5Token5ValueENS0_3SmiEi@PLT
.L4524:
	movl	(%rbx), %eax
	cmpl	$-1, %eax
	je	.L4526
	cmpb	$2, 496(%r12)
	je	.L4526
	movb	$1, 496(%r12)
	movl	%eax, 500(%r12)
.L4526:
	movl	4(%rbx), %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%edx, %ecx
	shrl	$7, %edx
	shrl	$14, %ecx
	andl	$127, %edx
	andl	$1, %ecx
	call	_ZN2v88internal11interpreter17BytecodeGenerator15BuildAssignmentERKNS2_17AssignmentLhsDataENS0_5Token5ValueENS0_18LookupHoistingModeE
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4536
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4522:
	.cfi_restore_state
	movl	328(%r12), %edx
	movq	336(%r12), %rdi
	leal	1(%rdx), %eax
	cmpl	%eax, 332(%r12)
	movl	%eax, 328(%r12)
	cmovge	332(%r12), %eax
	movl	%eax, 332(%r12)
	testq	%rdi, %rdi
	je	.L4525
	movq	(%rdi), %rax
	movq	%r8, -128(%rbp)
	movl	%edx, %esi
	movl	%edx, -120(%rbp)
	call	*16(%rax)
	movq	-128(%rbp), %r8
	movl	-120(%rbp), %edx
.L4525:
	movl	%edx, %esi
	movq	%r8, %rdi
	movl	%edx, -128(%rbp)
	movq	%r8, -120(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	16(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	movl	4(%r15), %esi
	movq	-120(%rbp), %r8
	movl	%r14d, %ecx
	movl	-128(%rbp), %edx
	shrl	$7, %esi
	movq	%r8, %rdi
	andl	$127, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder15BinaryOperationENS0_5Token5ValueENS1_8RegisterEi@PLT
	jmp	.L4524
	.p2align 4,,10
	.p2align 3
.L4518:
	movq	-72(%rbp), %rcx
	movl	-88(%rbp), %edx
	movq	%r12, %rdi
	movq	-80(%rbp), %rsi
	call	_ZN2v88internal11interpreter17BytecodeGenerator22BuildLoadNamedPropertyEPKNS0_10ExpressionENS1_8RegisterEPKNS0_12AstRawStringE
	jmp	.L4512
	.p2align 4,,10
	.p2align 3
.L4517:
	movq	512(%r12), %rax
	movl	$8, %esi
	leaq	56(%rax), %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movl	-84(%rbp), %esi
	leaq	24(%r12), %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	movl	-88(%rbp), %esi
	movl	%r14d, %edx
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder17LoadKeyedPropertyENS1_8RegisterEi@PLT
	jmp	.L4512
	.p2align 4,,10
	.p2align 3
.L4516:
	movl	-96(%rbp), %edx
	leaq	24(%r12), %rdi
	movl	$36, %esi
	movabsq	$12884901888, %rax
	orq	%rax, %rdx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_12RegisterListE@PLT
	jmp	.L4512
	.p2align 4,,10
	.p2align 3
.L4515:
	movl	-96(%rbp), %edx
	leaq	24(%r12), %rdi
	movl	$37, %esi
	movabsq	$12884901888, %rax
	orq	%rax, %rdx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_12RegisterListE@PLT
	jmp	.L4512
.L4513:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L4536:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal11interpreter17BytecodeGenerator23VisitCompoundAssignmentEPNS0_18CompoundAssignmentE
	.cfi_startproc
	.type	_ZN2v88internal11interpreter17BytecodeGenerator23VisitCompoundAssignmentEPNS0_18CompoundAssignmentE.cold, @function
_ZN2v88internal11interpreter17BytecodeGenerator23VisitCompoundAssignmentEPNS0_18CompoundAssignmentE.cold:
.LFSB20920:
.L4520:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	4, %eax
	ud2
.L4521:
	movq	24, %rax
	ud2
.L4523:
	movl	8, %eax
	ud2
	.cfi_endproc
.LFE20920:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator23VisitCompoundAssignmentEPNS0_18CompoundAssignmentE
	.size	_ZN2v88internal11interpreter17BytecodeGenerator23VisitCompoundAssignmentEPNS0_18CompoundAssignmentE, .-_ZN2v88internal11interpreter17BytecodeGenerator23VisitCompoundAssignmentEPNS0_18CompoundAssignmentE
	.section	.text.unlikely._ZN2v88internal11interpreter17BytecodeGenerator23VisitCompoundAssignmentEPNS0_18CompoundAssignmentE
	.size	_ZN2v88internal11interpreter17BytecodeGenerator23VisitCompoundAssignmentEPNS0_18CompoundAssignmentE.cold, .-_ZN2v88internal11interpreter17BytecodeGenerator23VisitCompoundAssignmentEPNS0_18CompoundAssignmentE.cold
.LCOLDE27:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator23VisitCompoundAssignmentEPNS0_18CompoundAssignmentE
.LHOTE27:
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator19VisitForInStatementEPNS0_14ForInStatementE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator19VisitForInStatementEPNS0_14ForInStatementE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator19VisitForInStatementEPNS0_14ForInStatementE, @function
_ZN2v88internal11interpreter17BytecodeGenerator19VisitForInStatementEPNS0_14ForInStatementE:
.LFB20772:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$296, %rsp
	movq	40(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal10Expression13IsNullLiteralEv@PLT
	testb	%al, %al
	je	.L4586
.L4537:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4587
	addq	$296, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4586:
	.cfi_restore_state
	movq	40(%r12), %rdi
	call	_ZNK2v88internal10Expression18IsUndefinedLiteralEv@PLT
	testb	%al, %al
	jne	.L4537
	movq	512(%rbx), %rax
	movl	$20, %esi
	movb	$0, -288(%rbp)
	leaq	24(%rbx), %r13
	movq	$-1, -280(%rbp)
	leaq	56(%rax), %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movq	40(%r12), %rsi
	movl	%eax, -296(%rbp)
	movl	(%rsi), %eax
	cmpl	$-1, %eax
	je	.L4541
	movb	$2, 496(%rbx)
	movl	%eax, 500(%rbx)
	movq	40(%r12), %rsi
.L4541:
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	leaq	-288(%rbp), %rax
	movq	%r13, %rdi
	movq	%rax, %rsi
	movq	%rax, -328(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder21JumpIfUndefinedOrNullEPNS1_13BytecodeLabelE@PLT
	movl	328(%rbx), %esi
	movq	336(%rbx), %rdi
	leal	1(%rsi), %eax
	cmpl	%eax, 332(%rbx)
	movl	%esi, -292(%rbp)
	movl	%eax, 328(%rbx)
	cmovge	332(%rbx), %eax
	movl	%eax, 332(%rbx)
	testq	%rdi, %rdi
	je	.L4542
	movq	(%rdi), %rax
	call	*16(%rax)
.L4542:
	movl	-292(%rbp), %esi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder8ToObjectENS1_8RegisterE@PLT
	movl	328(%rbx), %r15d
	movq	336(%rbx), %rdi
	leal	3(%r15), %eax
	cmpl	%eax, 332(%rbx)
	movl	%eax, 328(%rbx)
	cmovge	332(%rbx), %eax
	movl	%eax, 332(%rbx)
	movl	%r15d, %eax
	movq	%rax, -312(%rbp)
	movq	%rax, %rcx
	testq	%rdi, %rdi
	je	.L4543
	movabsq	$12884901888, %rsi
	movq	(%rdi), %rax
	orq	%rcx, %rsi
	call	*24(%rax)
.L4543:
	movl	-292(%rbp), %esi
	leal	2(%r15), %eax
	movq	%r13, %rdi
	movabsq	$12884901888, %r14
	movl	%eax, -316(%rbp)
	orq	-312(%rbp), %r14
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder14ForInEnumerateENS1_8RegisterE@PLT
	movl	-296(%rbp), %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder12ForInPrepareENS1_12RegisterListEi@PLT
	movl	328(%rbx), %r14d
	movq	336(%rbx), %rdi
	leal	1(%r14), %eax
	cmpl	%eax, 332(%rbx)
	movl	%eax, 328(%rbx)
	cmovge	332(%rbx), %eax
	movl	%eax, 332(%rbx)
	testq	%rdi, %rdi
	je	.L4544
	movq	(%rdi), %rax
	movl	%r14d, %esi
	call	*16(%rax)
.L4544:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralENS0_3SmiE@PLT
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	24(%rbx), %rax
	movb	$0, -80(%rbp)
	leaq	-168(%rbp), %rdx
	movq	568(%rbx), %r15
	leaq	16+_ZTVN2v88internal11interpreter11LoopBuilderE(%rip), %rcx
	movq	%r13, -184(%rbp)
	movq	%rax, -176(%rbp)
	movq	%rax, -112(%rbp)
	leaq	-104(%rbp), %rax
	movq	%rax, -96(%rbp)
	movq	%rax, -104(%rbp)
	leaq	-240(%rbp), %rax
	movq	%rdx, -160(%rbp)
	movq	%rdx, -168(%rbp)
	movq	$0, -152(%rbp)
	movb	$0, -144(%rbp)
	movq	%r12, -136(%rbp)
	movq	%r15, -128(%rbp)
	movq	%rcx, -192(%rbp)
	movq	$-1, -120(%rbp)
	movq	$0, -88(%rbp)
	movq	%rax, -304(%rbp)
	testq	%r15, %r15
	je	.L4545
	movq	40(%r15), %rax
	leaq	16(%rax), %rcx
	movq	24(%rax), %rax
	movq	%rcx, %rdx
	testq	%rax, %rax
	jne	.L4548
	jmp	.L4552
	.p2align 4,,10
	.p2align 3
.L4588:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L4549
.L4548:
	cmpq	%r12, 32(%rax)
	jnb	.L4588
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L4548
.L4549:
	cmpq	%rcx, %rdx
	je	.L4552
	cmpq	%r12, 32(%rdx)
	ja	.L4552
	movq	40(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L4552
	movq	(%rdi), %rax
	xorl	%esi, %esi
	call	*16(%rax)
	movq	%rax, -240(%rbp)
	cmpl	$-1, %eax
	je	.L4552
	movq	16(%r15), %rax
	subq	8(%r15), %rax
	movq	%r15, %rdi
	sarq	$3, %rax
	movl	%eax, -320(%rbp)
	leaq	-240(%rbp), %rax
	movq	%rax, %rsi
	movq	%rax, -304(%rbp)
	call	_ZNSt6vectorIN2v88internal11SourceRangeENS1_13ZoneAllocatorIS2_EEE12emplace_backIJRS2_EEEvDpOT_
	movl	-320(%rbp), %eax
.L4547:
	movl	%eax, -72(%rbp)
.L4545:
	leaq	-192(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter11LoopBuilder10LoopHeaderEv@PLT
	movq	32(%r12), %rax
	movl	(%rax), %eax
	cmpl	$-1, %eax
	je	.L4553
	movb	$2, 496(%rbx)
	movl	%eax, 500(%rbx)
.L4553:
	movl	-316(%rbp), %edx
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder13ForInContinueENS1_8RegisterES3_@PLT
	leaq	-176(%rbp), %rdx
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter27BreakableControlFlowBuilder15EmitJumpIfFalseENS1_20BytecodeArrayBuilder13ToBooleanModeEPNS1_14BytecodeLabelsE@PLT
	movq	-312(%rbp), %rcx
	movl	%r14d, %edx
	movq	%r13, %rdi
	movl	-296(%rbp), %r8d
	movl	-292(%rbp), %esi
	btsq	$33, %rcx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder9ForInNextENS1_8RegisterES3_NS1_12RegisterListEi@PLT
	leaq	-112(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter27BreakableControlFlowBuilder19EmitJumpIfUndefinedEPNS1_14BytecodeLabelsE@PLT
	movq	816(%rbx), %rax
	movl	$1, %ecx
	movq	%rbx, %rsi
	movq	-304(%rbp), %rdi
	movq	%rbx, -264(%rbp)
	movq	%rax, -272(%rbp)
	movl	328(%rbx), %eax
	movq	$1, -248(%rbp)
	movl	%eax, -256(%rbp)
	leaq	-272(%rbp), %rax
	movq	%rax, 816(%rbx)
	movq	32(%r12), %rdx
	call	_ZN2v88internal11interpreter17BytecodeGenerator20PrepareAssignmentLhsEPNS0_10ExpressionENS2_25AccumulatorPreservingModeE
	movq	32(%r12), %rax
	movl	(%rax), %eax
	cmpl	$-1, %eax
	je	.L4554
	cmpb	$2, 496(%rbx)
	je	.L4554
	movb	$1, 496(%rbx)
	movl	%eax, 500(%rbx)
.L4554:
	movq	-304(%rbp), %rsi
	movl	$17, %edx
	movq	%rbx, %rdi
	xorl	%ecx, %ecx
	call	_ZN2v88internal11interpreter17BytecodeGenerator15BuildAssignmentERKNS2_17AssignmentLhsDataENS0_5Token5ValueENS0_18LookupHoistingModeE
	movq	-264(%rbp), %rdx
	movq	-272(%rbp), %rax
	movq	336(%rdx), %rdi
	movq	%rax, 816(%rdx)
	movl	-256(%rbp), %eax
	movl	328(%rdx), %esi
	movl	%eax, 328(%rdx)
	testq	%rdi, %rdi
	je	.L4555
	subl	%eax, %esi
	movq	(%rdi), %rdx
	movq	%rsi, %rcx
	movl	%eax, %esi
	salq	$32, %rcx
	orq	%rcx, %rsi
	call	*32(%rdx)
.L4555:
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator18VisitIterationBodyEPNS0_18IterationStatementEPNS1_11LoopBuilderE
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder9ForInStepENS1_8RegisterE@PLT
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movl	868(%rbx), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter11LoopBuilder12JumpToHeaderEi@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter11LoopBuilderD1Ev@PLT
	movq	-328(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4BindEPNS1_13BytecodeLabelE@PLT
	jmp	.L4537
	.p2align 4,,10
	.p2align 3
.L4552:
	leaq	-240(%rbp), %rcx
	movl	$-1, %eax
	movq	%rcx, -304(%rbp)
	jmp	.L4547
.L4587:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20772:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator19VisitForInStatementEPNS0_14ForInStatementE, .-_ZN2v88internal11interpreter17BytecodeGenerator19VisitForInStatementEPNS0_14ForInStatementE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator19VisitForOfStatementEPNS0_14ForOfStatementE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator19VisitForOfStatementEPNS0_14ForOfStatementE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator19VisitForOfStatementEPNS0_14ForOfStatementE, @function
_ZN2v88internal11interpreter17BytecodeGenerator19VisitForOfStatementEPNS0_14ForOfStatementE:
.LFB20776:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	24(%rdi), %r12
	pushq	%rbx
	subq	$504, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -504(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	816(%rdi), %rax
	movq	%rdi, -472(%rbp)
	movq	$1, -456(%rbp)
	movq	%rax, -480(%rbp)
	movl	328(%rdi), %eax
	movl	%eax, -464(%rbp)
	leaq	-480(%rbp), %rax
	movq	%rax, 816(%rdi)
	movq	40(%rsi), %rsi
	movl	(%rsi), %eax
	cmpl	$-1, %eax
	je	.L4590
	movb	$2, 496(%rdi)
	movl	%eax, 500(%rdi)
	movq	40(%rcx), %rsi
.L4590:
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	movl	328(%r13), %ebx
	movq	-504(%rbp), %rax
	movq	336(%r13), %rdi
	leal	1(%rbx), %r15d
	movl	48(%rax), %r14d
	cmpl	%r15d, 332(%r13)
	movl	%r15d, %eax
	cmovge	332(%r13), %eax
	movl	%r15d, 328(%r13)
	movl	%eax, 332(%r13)
	testq	%rdi, %rdi
	je	.L4591
	movq	(%rdi), %rax
	movl	%ebx, %esi
	call	*16(%rax)
	movl	328(%r13), %r15d
	movl	332(%r13), %edx
	movq	336(%r13), %rdi
	leal	1(%r15), %eax
	cmpl	%edx, %eax
	movl	%eax, 328(%r13)
	cmovl	%edx, %eax
	movl	%eax, 332(%r13)
	testq	%rdi, %rdi
	je	.L4592
	movq	(%rdi), %rax
	movl	%r15d, %esi
	call	*16(%rax)
.L4592:
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator16BuildGetIteratorENS0_12IteratorTypeE
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movl	$5, %esi
	movq	%rax, -512(%rbp)
	movq	512(%r13), %rax
	leaq	56(%rax), %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movq	-512(%rbp), %r8
	movl	%r15d, %esi
	movl	%eax, %ecx
	movq	520(%r13), %rax
	movq	%r8, %rdi
	movq	368(%rax), %rdx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder17LoadNamedPropertyENS1_8RegisterEPKNS0_12AstRawStringEi@PLT
	movl	%ebx, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movl	328(%r13), %esi
	movq	336(%r13), %rdi
	movl	%r14d, -492(%rbp)
	movl	%r15d, -488(%rbp)
	leal	1(%rsi), %eax
	cmpl	%eax, 332(%r13)
	movl	%ebx, -484(%rbp)
	movl	%eax, 328(%r13)
	cmovge	332(%r13), %eax
	movl	%esi, -512(%rbp)
	movl	%eax, 332(%r13)
	testq	%rdi, %rdi
	je	.L4593
	movq	(%rdi), %rax
	call	*16(%rax)
.L4593:
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder9LoadFalseEv@PLT
	movl	-512(%rbp), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	leaq	16+_ZTVN2v88internal11interpreter17TryFinallyBuilderE(%rip), %rax
	leaq	288(%r13), %rdi
	movq	%r12, -280(%rbp)
	movq	%rax, -288(%rbp)
	call	_ZN2v88internal11interpreter19HandlerTableBuilder15NewHandlerEntryEv@PLT
	movl	328(%r13), %esi
	pxor	%xmm0, %xmm0
	movq	336(%r13), %rdi
	movl	%eax, -272(%rbp)
	movq	24(%r13), %rax
	leal	1(%rsi), %ebx
	cmpl	%ebx, 332(%r13)
	movl	$0, -268(%rbp)
	movq	%rax, -248(%rbp)
	leaq	-240(%rbp), %rax
	movq	%rax, -232(%rbp)
	movq	%rax, -240(%rbp)
	movl	%ebx, %eax
	cmovge	332(%r13), %eax
	movb	$0, -264(%rbp)
	movq	$-1, -256(%rbp)
	movq	$0, -224(%rbp)
	movb	$0, -216(%rbp)
	movl	%esi, -524(%rbp)
	movl	%ebx, 328(%r13)
	movl	%eax, 332(%r13)
	movaps	%xmm0, -208(%rbp)
	testq	%rdi, %rdi
	je	.L4594
	movq	(%rdi), %rax
	call	*16(%rax)
	movl	328(%r13), %ebx
	movl	332(%r13), %edx
	movq	336(%r13), %rdi
	leal	1(%rbx), %eax
	cmpl	%edx, %eax
	movl	%eax, 328(%r13)
	cmovl	%edx, %eax
	movl	%eax, 332(%r13)
	testq	%rdi, %rdi
	je	.L4595
	movq	(%rdi), %rax
	movl	%ebx, %esi
	call	*16(%rax)
.L4595:
	movq	16(%r13), %rax
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movl	%ebx, -308(%rbp)
	leaq	-192(%rbp), %r15
	leaq	-344(%rbp), %rdi
	movq	%r13, -352(%rbp)
	movq	%rax, -344(%rbp)
	movq	%r15, %rdx
	movl	-524(%rbp), %eax
	movq	$0, -320(%rbp)
	movl	%eax, -312(%rbp)
	movq	$-1, -304(%rbp)
	movl	$4, -192(%rbp)
	movq	$0, -184(%rbp)
	movl	$0, -176(%rbp)
	movaps	%xmm0, -336(%rbp)
	call	_ZNSt6vectorIN2v88internal11interpreter17BytecodeGenerator12ControlScope16DeferredCommands5EntryENS1_13ZoneAllocatorIS6_EEE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S9_EEDpOT_
	movl	328(%r13), %esi
	movq	336(%r13), %rdi
	leal	1(%rsi), %eax
	cmpl	%eax, 332(%r13)
	movl	%esi, -528(%rbp)
	movl	%eax, 328(%r13)
	cmovge	332(%r13), %eax
	movl	%eax, 332(%r13)
	testq	%rdi, %rdi
	je	.L4596
	movq	(%rdi), %rax
	call	*16(%rax)
.L4596:
	call	_ZN2v88internal11interpreter8Register15current_contextEv@PLT
	movl	-528(%rbp), %ebx
	movq	%r12, %rdi
	leaq	-288(%rbp), %r14
	movl	%eax, %esi
	movl	%ebx, %edx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder12MoveRegisterENS1_8RegisterES3_@PLT
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter17TryFinallyBuilder8BeginTryENS1_8RegisterE@PLT
	movq	800(%r13), %rax
	movl	328(%r13), %ebx
	movq	%r13, -440(%rbp)
	movq	336(%r13), %rdi
	movq	%r14, -416(%rbp)
	movq	%rax, -432(%rbp)
	movq	808(%r13), %rax
	movq	%rax, -424(%rbp)
	leaq	-448(%rbp), %rax
	movq	%rax, 800(%r13)
	leaq	16+_ZTVN2v88internal11interpreter17BytecodeGenerator25ControlScopeForTryFinallyE(%rip), %rax
	movq	%rax, -448(%rbp)
	leaq	-352(%rbp), %rax
	movq	%rax, -408(%rbp)
	leal	1(%rbx), %eax
	cmpl	%eax, 332(%r13)
	movl	%eax, 328(%r13)
	cmovge	332(%r13), %eax
	movl	%eax, 332(%r13)
	testq	%rdi, %rdi
	je	.L4597
	movq	(%rdi), %rax
	movl	%ebx, %esi
	call	*16(%rax)
.L4597:
	movq	24(%r13), %rax
	movq	-504(%rbp), %rcx
	leaq	-168(%rbp), %rdx
	movq	%r12, -184(%rbp)
	movq	568(%r13), %r8
	movb	$0, -80(%rbp)
	movq	%rax, -176(%rbp)
	movq	%rax, -112(%rbp)
	leaq	-104(%rbp), %rax
	movq	%rcx, -136(%rbp)
	leaq	16+_ZTVN2v88internal11interpreter11LoopBuilderE(%rip), %rcx
	movq	%rax, -96(%rbp)
	movq	%rax, -104(%rbp)
	leaq	-400(%rbp), %rax
	movq	%rdx, -160(%rbp)
	movq	%rdx, -168(%rbp)
	movq	$0, -152(%rbp)
	movb	$0, -144(%rbp)
	movq	%r8, -128(%rbp)
	movq	%rcx, -192(%rbp)
	movq	$-1, -120(%rbp)
	movq	$0, -88(%rbp)
	movq	%rax, -520(%rbp)
	testq	%r8, %r8
	je	.L4598
	movq	40(%r8), %rax
	leaq	16(%rax), %rcx
	movq	24(%rax), %rax
	movq	%rcx, %rdx
	testq	%rax, %rax
	jne	.L4601
	.p2align 4,,10
	.p2align 3
.L4605:
	leaq	-400(%rbp), %rcx
	movl	$-1, %eax
	movq	%rcx, -520(%rbp)
.L4600:
	movl	%eax, -72(%rbp)
.L4598:
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter11LoopBuilder10LoopHeaderEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder8LoadTrueEv@PLT
	movl	-512(%rbp), %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-504(%rbp), %rax
	movq	32(%rax), %rax
	movl	(%rax), %eax
	cmpl	$-1, %eax
	je	.L4606
	movb	$2, 496(%r13)
	movl	%eax, 500(%r13)
.L4606:
	movl	%ebx, %edx
	leaq	-492(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator17BuildIteratorNextERKNS2_14IteratorRecordENS1_8RegisterE
	movq	512(%r13), %rax
	movl	$5, %esi
	leaq	56(%rax), %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movl	%ebx, %esi
	movq	%r12, %rdi
	movl	%eax, %ecx
	movq	520(%r13), %rax
	movq	192(%rax), %rdx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder17LoadNamedPropertyENS1_8RegisterEPKNS0_12AstRawStringEi@PLT
	leaq	-176(%rbp), %rdx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter27BreakableControlFlowBuilder14EmitJumpIfTrueENS1_20BytecodeArrayBuilder13ToBooleanModeEPNS1_14BytecodeLabelsE@PLT
	movq	512(%r13), %rax
	movl	$5, %esi
	leaq	56(%rax), %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movl	%ebx, %esi
	movq	%r12, %rdi
	movl	%eax, %ecx
	movq	520(%r13), %rax
	movq	504(%rax), %rdx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder17LoadNamedPropertyENS1_8RegisterEPKNS0_12AstRawStringEi@PLT
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder9LoadFalseEv@PLT
	movl	-512(%rbp), %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-504(%rbp), %rax
	movq	-520(%rbp), %rdi
	movq	%r13, %rsi
	movq	32(%rax), %rdx
	call	_ZN2v88internal11interpreter17BytecodeGenerator20PrepareAssignmentLhsEPNS0_10ExpressionENS2_25AccumulatorPreservingModeE.constprop.0
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	xorl	%ecx, %ecx
	movl	$17, %edx
	movq	%r13, %rdi
	movq	-520(%rbp), %rsi
	call	_ZN2v88internal11interpreter17BytecodeGenerator15BuildAssignmentERKNS2_17AssignmentLhsDataENS0_5Token5ValueENS0_18LookupHoistingModeE
	movq	-504(%rbp), %rsi
	movq	%r15, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator18VisitIterationBodyEPNS0_18IterationStatementEPNS1_11LoopBuilderE
	movl	868(%r13), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter11LoopBuilder12JumpToHeaderEi@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter11LoopBuilderD1Ev@PLT
	movq	-432(%rbp), %rdx
	movq	-440(%rbp), %rax
	movq	%r14, %rdi
	movq	%rdx, 800(%rax)
	call	_ZN2v88internal11interpreter17TryFinallyBuilder6EndTryEv@PLT
	movq	-352(%rbp), %rax
	movabsq	$-4294967296, %rsi
	leaq	24(%rax), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralENS0_3SmiE@PLT
	movq	-352(%rbp), %rax
	movl	-312(%rbp), %esi
	leaq	24(%rax), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-352(%rbp), %rax
	movl	-308(%rbp), %esi
	leaq	24(%rax), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter17TryFinallyBuilder8LeaveTryEv@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter17TryFinallyBuilder12BeginHandlerEv@PLT
	movq	-352(%rbp), %rax
	movl	-308(%rbp), %esi
	leaq	24(%rax), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-352(%rbp), %rax
	xorl	%esi, %esi
	leaq	24(%rax), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralENS0_3SmiE@PLT
	movq	-352(%rbp), %rax
	movl	-312(%rbp), %esi
	leaq	24(%rax), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter17TryFinallyBuilder12BeginFinallyEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadTheHoleEv@PLT
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder17SetPendingMessageEv@PLT
	movl	-528(%rbp), %ebx
	movq	%rax, %rdi
	movl	%ebx, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movl	-524(%rbp), %r8d
	movq	%r13, %rdi
	movl	-512(%rbp), %ecx
	movl	-484(%rbp), %edx
	movq	-492(%rbp), %rsi
	call	_ZN2v88internal11interpreter17BytecodeGenerator22BuildFinalizeIterationENS2_14IteratorRecordENS1_8RegisterES4_
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter17TryFinallyBuilder10EndFinallyEv@PLT
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder17SetPendingMessageEv@PLT
	movq	-328(%rbp), %rax
	movq	-336(%rbp), %r12
	movq	%rax, %rsi
	subq	%r12, %rsi
	cmpq	%rax, %r12
	je	.L4608
	movq	-352(%rbp), %rax
	movb	$0, -192(%rbp)
	movq	$-1, -184(%rbp)
	leaq	24(%rax), %rdi
	cmpq	$24, %rsi
	je	.L4658
	movabsq	$-6148914691236517205, %rax
	sarq	$3, %rsi
	xorl	%edx, %edx
	imulq	%rax, %rsi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder17AllocateJumpTableEii@PLT
	movl	-312(%rbp), %esi
	movq	%rax, %r12
	movq	-352(%rbp), %rax
	leaq	24(%rax), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder21SwitchOnSmiNoFeedbackEPNS1_17BytecodeJumpTableE@PLT
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4JumpEPNS1_13BytecodeLabelE@PLT
	movq	-328(%rbp), %rax
	movq	-336(%rbp), %rbx
	movq	%rax, -520(%rbp)
	cmpq	%rax, %rbx
	je	.L4613
	.p2align 4,,10
	.p2align 3
.L4618:
	movq	-352(%rbp), %rax
	movl	16(%rbx), %edx
	movq	%r12, %rsi
	leaq	24(%rax), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4BindEPNS1_17BytecodeJumpTableEi@PLT
	movl	(%rbx), %esi
	cmpl	$1, %esi
	jbe	.L4614
	movq	-352(%rbp), %rax
	movl	-308(%rbp), %esi
	leaq	24(%rax), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	movl	(%rbx), %esi
.L4614:
	movq	-352(%rbp), %rax
	movq	8(%rbx), %r13
	movq	800(%rax), %rdi
	jmp	.L4617
	.p2align 4,,10
	.p2align 3
.L4659:
	movq	-504(%rbp), %rdi
	movl	-512(%rbp), %esi
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L4616
.L4617:
	movq	(%rdi), %rax
	movl	%esi, -512(%rbp)
	movl	$-1, %ecx
	movq	%r13, %rdx
	movq	%rdi, -504(%rbp)
	call	*16(%rax)
	testb	%al, %al
	je	.L4659
	addq	$24, %rbx
	cmpq	%rbx, -520(%rbp)
	jne	.L4618
.L4613:
	movq	-352(%rbp), %rax
	movq	%r15, %rsi
	leaq	24(%rax), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4BindEPNS1_13BytecodeLabelE@PLT
.L4608:
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter17TryFinallyBuilderD1Ev@PLT
	movq	-472(%rbp), %rdx
	movq	-480(%rbp), %rax
	movq	336(%rdx), %rdi
	movq	%rax, 816(%rdx)
	movl	-464(%rbp), %eax
	movl	328(%rdx), %esi
	movl	%eax, 328(%rdx)
	testq	%rdi, %rdi
	je	.L4589
	subl	%eax, %esi
	movq	(%rdi), %rcx
	movq	%rsi, %rdx
	movl	%eax, %esi
	salq	$32, %rdx
	orq	%rdx, %rsi
	call	*32(%rcx)
.L4589:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4660
	addq	$504, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4661:
	.cfi_restore_state
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L4602
.L4601:
	movq	-504(%rbp), %rsi
	cmpq	%rsi, 32(%rax)
	jnb	.L4661
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L4601
.L4602:
	movq	%r8, -520(%rbp)
	cmpq	%rcx, %rdx
	je	.L4605
	movq	-504(%rbp), %rax
	cmpq	%rax, 32(%rdx)
	ja	.L4605
	movq	40(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L4605
	movq	(%rdi), %rax
	xorl	%esi, %esi
	call	*16(%rax)
	movq	%rax, -400(%rbp)
	cmpl	$-1, %eax
	je	.L4605
	movq	-520(%rbp), %r8
	movq	16(%r8), %rax
	subq	8(%r8), %rax
	movq	%r8, %rdi
	sarq	$3, %rax
	movl	%eax, -532(%rbp)
	leaq	-400(%rbp), %rax
	movq	%rax, %rsi
	movq	%rax, -520(%rbp)
	call	_ZNSt6vectorIN2v88internal11SourceRangeENS1_13ZoneAllocatorIS2_EEE12emplace_backIJRS2_EEEvDpOT_
	movl	-532(%rbp), %eax
	jmp	.L4600
	.p2align 4,,10
	.p2align 3
.L4594:
	leal	2(%rsi), %edx
	cmpl	%edx, %eax
	movl	%edx, 328(%r13)
	cmovl	%edx, %eax
	movl	%eax, 332(%r13)
	jmp	.L4595
	.p2align 4,,10
	.p2align 3
.L4591:
	leal	2(%rbx), %edx
	cmpl	%edx, %eax
	movl	%edx, 328(%r13)
	cmovl	%edx, %eax
	movl	%eax, 332(%r13)
	jmp	.L4592
	.p2align 4,,10
	.p2align 3
.L4658:
	movslq	16(%r12), %rsi
	salq	$32, %rsi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralENS0_3SmiE@PLT
	movl	-312(%rbp), %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder16CompareReferenceENS1_8RegisterE@PLT
	movq	%r15, %rdx
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11JumpIfFalseENS2_13ToBooleanModeEPNS1_13BytecodeLabelE@PLT
	movl	(%r12), %r13d
	cmpl	$1, %r13d
	ja	.L4662
.L4610:
	movq	-352(%rbp), %rax
	movq	8(%r12), %r12
	movq	800(%rax), %rbx
	jmp	.L4612
	.p2align 4,,10
	.p2align 3
.L4611:
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L4616
.L4612:
	movq	(%rbx), %rax
	movl	$-1, %ecx
	movq	%r12, %rdx
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	*16(%rax)
	testb	%al, %al
	je	.L4611
	jmp	.L4613
	.p2align 4,,10
	.p2align 3
.L4662:
	movq	-352(%rbp), %rax
	movl	-308(%rbp), %esi
	leaq	24(%rax), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	movl	(%r12), %r13d
	jmp	.L4610
	.p2align 4,,10
	.p2align 3
.L4616:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L4660:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20776:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator19VisitForOfStatementEPNS0_14ForOfStatementE, .-_ZN2v88internal11interpreter17BytecodeGenerator19VisitForOfStatementEPNS0_14ForOfStatementE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator33BuildDestructuringArrayAssignmentEPNS0_12ArrayLiteralENS0_5Token5ValueENS0_18LookupHoistingModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator33BuildDestructuringArrayAssignmentEPNS0_12ArrayLiteralENS0_5Token5ValueENS0_18LookupHoistingModeE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator33BuildDestructuringArrayAssignmentEPNS0_12ArrayLiteralENS0_5Token5ValueENS0_18LookupHoistingModeE, @function
_ZN2v88internal11interpreter17BytecodeGenerator33BuildDestructuringArrayAssignmentEPNS0_12ArrayLiteralENS0_5Token5ValueENS0_18LookupHoistingModeE:
.LFB20914:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$472, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, -480(%rbp)
	movl	328(%rdi), %esi
	movl	%ecx, -464(%rbp)
	movb	%dl, -473(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leal	1(%rsi), %eax
	cmpl	%eax, 332(%rdi)
	movl	%esi, -428(%rbp)
	movl	%eax, 328(%rdi)
	cmovge	332(%rdi), %eax
	movl	%eax, 332(%rdi)
	movq	336(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L4664
	movq	(%rdi), %rax
	call	*16(%rax)
.L4664:
	movl	-428(%rbp), %esi
	leaq	24(%r15), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movl	328(%r15), %ebx
	movq	336(%r15), %rdi
	leal	1(%rbx), %r13d
	cmpl	%r13d, 332(%r15)
	movl	%r13d, %eax
	cmovge	332(%r15), %eax
	movl	%r13d, 328(%r15)
	movl	%eax, 332(%r15)
	testq	%rdi, %rdi
	je	.L4665
	movq	(%rdi), %rax
	movl	%ebx, %esi
	call	*16(%rax)
	movl	328(%r15), %r13d
	movl	332(%r15), %edx
	movq	336(%r15), %rdi
	leal	1(%r13), %eax
	cmpl	%edx, %eax
	movl	%eax, 328(%r15)
	cmovl	%edx, %eax
	movl	%eax, 332(%r15)
	testq	%rdi, %rdi
	je	.L4666
	movq	(%rdi), %rax
	movl	%r13d, %esi
	call	*16(%rax)
.L4666:
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator16BuildGetIteratorENS0_12IteratorTypeE
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movl	$5, %esi
	movq	%rax, -408(%rbp)
	movq	512(%r15), %rax
	leaq	56(%rax), %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movq	-408(%rbp), %r8
	movl	%r13d, %esi
	movl	%eax, %ecx
	movq	520(%r15), %rax
	movq	%r8, %rdi
	movq	368(%rax), %rdx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder17LoadNamedPropertyENS1_8RegisterEPKNS0_12AstRawStringEi@PLT
	movl	%ebx, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movl	%r13d, -392(%rbp)
	movl	328(%r15), %r13d
	movq	336(%r15), %rdi
	movl	%ebx, -388(%rbp)
	movl	$0, -396(%rbp)
	leal	1(%r13), %eax
	cmpl	%eax, 332(%r15)
	movl	%eax, 328(%r15)
	cmovge	332(%r15), %eax
	movl	%eax, 332(%r15)
	testq	%rdi, %rdi
	je	.L4667
	movq	(%rdi), %rax
	movl	%r13d, %esi
	call	*16(%rax)
.L4667:
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder9LoadFalseEv@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	leaq	16+_ZTVN2v88internal11interpreter17TryFinallyBuilderE(%rip), %rax
	leaq	288(%r15), %rdi
	movq	%r12, -152(%rbp)
	movq	%rax, -160(%rbp)
	call	_ZN2v88internal11interpreter19HandlerTableBuilder15NewHandlerEntryEv@PLT
	movl	328(%r15), %esi
	pxor	%xmm0, %xmm0
	movq	336(%r15), %rdi
	movl	%eax, -144(%rbp)
	movq	24(%r15), %rax
	leal	1(%rsi), %ebx
	cmpl	%ebx, 332(%r15)
	movl	$0, -140(%rbp)
	movq	%rax, -120(%rbp)
	leaq	-112(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	%rax, -112(%rbp)
	movl	%ebx, %eax
	cmovge	332(%r15), %eax
	movb	$0, -136(%rbp)
	movq	$-1, -128(%rbp)
	movq	$0, -96(%rbp)
	movb	$0, -88(%rbp)
	movl	%esi, -440(%rbp)
	movl	%ebx, 328(%r15)
	movl	%eax, 332(%r15)
	movaps	%xmm0, -80(%rbp)
	testq	%rdi, %rdi
	je	.L4668
	movq	(%rdi), %rax
	call	*16(%rax)
	movl	328(%r15), %ebx
	movl	332(%r15), %edx
	movq	336(%r15), %rdi
	leal	1(%rbx), %eax
	cmpl	%edx, %eax
	movl	%eax, 328(%r15)
	cmovl	%edx, %eax
	movl	%eax, 332(%r15)
	testq	%rdi, %rdi
	je	.L4669
	movq	(%rdi), %rax
	movl	%ebx, %esi
	call	*16(%rax)
.L4669:
	movq	16(%r15), %rax
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movl	%ebx, -180(%rbp)
	leaq	-216(%rbp), %rdi
	movq	%r15, -224(%rbp)
	movq	%rax, -216(%rbp)
	movl	-440(%rbp), %eax
	movq	$0, -192(%rbp)
	movl	%eax, -184(%rbp)
	leaq	-272(%rbp), %rax
	movq	%rax, %rdx
	movq	%rax, -408(%rbp)
	movq	$-1, -176(%rbp)
	movl	$4, -272(%rbp)
	movq	$0, -264(%rbp)
	movl	$0, -256(%rbp)
	movaps	%xmm0, -208(%rbp)
	call	_ZNSt6vectorIN2v88internal11interpreter17BytecodeGenerator12ControlScope16DeferredCommands5EntryENS1_13ZoneAllocatorIS6_EEE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S9_EEDpOT_
	movl	328(%r15), %esi
	movq	336(%r15), %rdi
	leal	1(%rsi), %eax
	cmpl	%eax, 332(%r15)
	movl	%esi, -424(%rbp)
	movl	%eax, 328(%r15)
	cmovge	332(%r15), %eax
	movl	%eax, 332(%r15)
	testq	%rdi, %rdi
	je	.L4670
	movq	(%rdi), %rax
	call	*16(%rax)
.L4670:
	call	_ZN2v88internal11interpreter8Register15current_contextEv@PLT
	movl	-424(%rbp), %edx
	movq	%r12, %rdi
	leaq	-160(%rbp), %rbx
	movl	%eax, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder12MoveRegisterENS1_8RegisterES3_@PLT
	movl	-424(%rbp), %esi
	movq	%rbx, %rdi
	movq	%rbx, -456(%rbp)
	call	_ZN2v88internal11interpreter17TryFinallyBuilder8BeginTryENS1_8RegisterE@PLT
	movq	800(%r15), %rax
	movq	%rbx, -288(%rbp)
	movl	328(%r15), %ebx
	movq	336(%r15), %rdi
	movq	%r15, -312(%rbp)
	movq	%rax, -304(%rbp)
	movq	808(%r15), %rax
	movq	%rax, -296(%rbp)
	leaq	-320(%rbp), %rax
	movq	%rax, 800(%r15)
	leaq	16+_ZTVN2v88internal11interpreter17BytecodeGenerator25ControlScopeForTryFinallyE(%rip), %rax
	movq	%rax, -320(%rbp)
	leaq	-224(%rbp), %rax
	movq	%rax, -280(%rbp)
	leal	1(%rbx), %eax
	cmpl	%eax, 332(%r15)
	movl	%eax, 328(%r15)
	cmovge	332(%r15), %eax
	movl	%eax, 332(%r15)
	testq	%rdi, %rdi
	je	.L4671
	movq	(%rdi), %rax
	movl	%ebx, %esi
	call	*16(%rax)
.L4671:
	movq	512(%r15), %rax
	movl	$5, %esi
	leaq	56(%rax), %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movl	$5, %esi
	movl	%eax, -432(%rbp)
	movq	512(%r15), %rax
	leaq	56(%rax), %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movslq	36(%r14), %rdx
	movl	%eax, -460(%rbp)
	movq	24(%r14), %rax
	leaq	(%rax,%rdx,8), %rcx
	movq	%rax, -416(%rbp)
	movq	%rcx, -472(%rbp)
	cmpq	%rcx, %rax
	je	.L4673
	.p2align 4,,10
	.p2align 3
.L4686:
	movq	-416(%rbp), %rax
	movq	(%rax), %r8
	movzbl	4(%r8), %eax
	andl	$63, %eax
	cmpb	$46, %al
	je	.L4750
	xorl	%r9d, %r9d
	cmpb	$24, %al
	jne	.L4677
	movq	16(%r8), %r9
	movq	8(%r8), %r8
	movzbl	4(%r8), %eax
	andl	$63, %eax
.L4677:
	subl	$22, %eax
	cmpb	$1, %al
	jbe	.L4678
	movl	(%r8), %eax
	cmpl	$-1, %eax
	je	.L4678
	movb	$2, 496(%r15)
	movl	%eax, 500(%r15)
.L4678:
	movq	-408(%rbp), %rdi
	movq	%r8, %rdx
	movq	%r15, %rsi
	movq	%r8, -496(%rbp)
	movq	%r9, -504(%rbp)
	leaq	-368(%rbp), %r14
	call	_ZN2v88internal11interpreter17BytecodeGenerator20PrepareAssignmentLhsEPNS0_10ExpressionENS2_25AccumulatorPreservingModeE.constprop.0
	movq	16(%r15), %rax
	movl	%r13d, %esi
	movq	%r12, %rdi
	movq	$0, -344(%rbp)
	movq	%rax, -368(%rbp)
	leaq	-360(%rbp), %rax
	movq	%rax, -448(%rbp)
	movq	%rax, -352(%rbp)
	movq	%rax, -360(%rbp)
	movb	$0, -336(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter14BytecodeLabels3NewEv@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder10JumpIfTrueENS2_13ToBooleanModeEPNS1_13BytecodeLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder8LoadTrueEv@PLT
	movl	%r13d, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	leaq	-396(%rbp), %rsi
	movl	%ebx, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator17BuildIteratorNextERKNS2_14IteratorRecordENS1_8RegisterE
	movl	-460(%rbp), %ecx
	movl	%ebx, %esi
	movq	%r12, %rdi
	movq	520(%r15), %rax
	movq	192(%rax), %rdx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder17LoadNamedPropertyENS1_8RegisterEPKNS0_12AstRawStringEi@PLT
	movq	%r14, %rdi
	movq	%rax, -488(%rbp)
	call	_ZN2v88internal11interpreter14BytecodeLabels3NewEv@PLT
	movq	-488(%rbp), %r10
	xorl	%esi, %esi
	movq	%rax, %rdx
	movq	%r10, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder10JumpIfTrueENS2_13ToBooleanModeEPNS1_13BytecodeLabelE@PLT
	movl	-432(%rbp), %ecx
	movl	%ebx, %esi
	movq	%rax, %rdi
	movq	520(%r15), %rax
	movq	504(%rax), %rdx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder17LoadNamedPropertyENS1_8RegisterEPKNS0_12AstRawStringEi@PLT
	movl	%ebx, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder9LoadFalseEv@PLT
	movl	%r13d, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movl	%ebx, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	movq	-496(%rbp), %r8
	movq	%r8, %rdi
	call	_ZNK2v88internal10Expression16IsTheHoleLiteralEv@PLT
	testb	%al, %al
	jne	.L4679
	movq	-504(%rbp), %r9
	movq	%r12, %rdi
	leaq	-384(%rbp), %r8
	movb	$0, -384(%rbp)
	movq	$-1, -376(%rbp)
	movq	%r8, %rsi
	testq	%r9, %r9
	movq	%r9, -496(%rbp)
	movq	%r8, -488(%rbp)
	je	.L4680
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder18JumpIfNotUndefinedEPNS1_13BytecodeLabelE@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter14BytecodeLabels4BindEPNS1_20BytecodeArrayBuilderE@PLT
	movq	-496(%rbp), %r9
	movq	%r15, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	movq	-488(%rbp), %r8
.L4681:
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4BindEPNS1_13BytecodeLabelE@PLT
	movzbl	-473(%rbp), %edx
	movl	-464(%rbp), %ecx
	movq	%r15, %rdi
	movq	-408(%rbp), %rsi
	call	_ZN2v88internal11interpreter17BytecodeGenerator15BuildAssignmentERKNS2_17AssignmentLhsDataENS0_5Token5ValueENS0_18LookupHoistingModeE
.L4682:
	movq	-360(%rbp), %rax
	cmpq	-448(%rbp), %rax
	je	.L4683
	.p2align 4,,10
	.p2align 3
.L4684:
	movq	(%rax), %rax
	cmpq	-448(%rbp), %rax
	jne	.L4684
.L4683:
	addq	$8, -416(%rbp)
	movq	-416(%rbp), %rax
	cmpq	%rax, -472(%rbp)
	jne	.L4686
.L4673:
	movq	-304(%rbp), %rdx
	movq	-312(%rbp), %rax
	movq	-456(%rbp), %rbx
	movq	%rdx, 800(%rax)
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter17TryFinallyBuilder6EndTryEv@PLT
	movq	-224(%rbp), %rax
	movabsq	$-4294967296, %rsi
	leaq	24(%rax), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralENS0_3SmiE@PLT
	movq	-224(%rbp), %rax
	movl	-184(%rbp), %esi
	leaq	24(%rax), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-224(%rbp), %rax
	movl	-180(%rbp), %esi
	leaq	24(%rax), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter17TryFinallyBuilder8LeaveTryEv@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter17TryFinallyBuilder12BeginHandlerEv@PLT
	movq	-224(%rbp), %rax
	movl	-180(%rbp), %esi
	leaq	24(%rax), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-224(%rbp), %rax
	xorl	%esi, %esi
	leaq	24(%rax), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralENS0_3SmiE@PLT
	movq	-224(%rbp), %rax
	movl	-184(%rbp), %esi
	leaq	24(%rax), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter17TryFinallyBuilder12BeginFinallyEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadTheHoleEv@PLT
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder17SetPendingMessageEv@PLT
	movl	-424(%rbp), %r14d
	movq	%rax, %rdi
	movl	%r14d, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movl	-440(%rbp), %r8d
	movl	%r13d, %ecx
	movq	%r15, %rdi
	movl	-388(%rbp), %edx
	movq	-396(%rbp), %rsi
	call	_ZN2v88internal11interpreter17BytecodeGenerator22BuildFinalizeIterationENS2_14IteratorRecordENS1_8RegisterES4_
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter17TryFinallyBuilder10EndFinallyEv@PLT
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder17SetPendingMessageEv@PLT
	movq	-200(%rbp), %rax
	movq	-208(%rbp), %rbx
	movq	%rax, %rsi
	subq	%rbx, %rsi
	cmpq	%rax, %rbx
	je	.L4691
	movq	-224(%rbp), %rax
	movb	$0, -272(%rbp)
	movq	$-1, -264(%rbp)
	leaq	24(%rax), %rdi
	cmpq	$24, %rsi
	je	.L4751
	movabsq	$-6148914691236517205, %rax
	sarq	$3, %rsi
	xorl	%edx, %edx
	imulq	%rax, %rsi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder17AllocateJumpTableEii@PLT
	movl	-184(%rbp), %esi
	movq	%rax, %rbx
	movq	-224(%rbp), %rax
	leaq	24(%rax), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	movq	%rbx, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder21SwitchOnSmiNoFeedbackEPNS1_17BytecodeJumpTableE@PLT
	movq	-408(%rbp), %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4JumpEPNS1_13BytecodeLabelE@PLT
	movq	-200(%rbp), %rax
	movq	-208(%rbp), %r14
	movq	%rax, -416(%rbp)
	cmpq	%rax, %r14
	je	.L4696
	movq	%r12, -424(%rbp)
	movq	%r15, -440(%rbp)
	.p2align 4,,10
	.p2align 3
.L4701:
	movq	-224(%rbp), %rax
	movl	16(%r14), %edx
	movq	%rbx, %rsi
	leaq	24(%rax), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4BindEPNS1_17BytecodeJumpTableEi@PLT
	movl	(%r14), %r15d
	cmpl	$1, %r15d
	jbe	.L4697
	movq	-224(%rbp), %rax
	movl	-180(%rbp), %esi
	leaq	24(%rax), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	movl	(%r14), %r15d
.L4697:
	movq	-224(%rbp), %rax
	movq	8(%r14), %r13
	movq	800(%rax), %r12
	jmp	.L4700
	.p2align 4,,10
	.p2align 3
.L4752:
	movq	16(%r12), %r12
	testq	%r12, %r12
	je	.L4699
.L4700:
	movq	(%r12), %rax
	movl	$-1, %ecx
	movq	%r13, %rdx
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	*16(%rax)
	testb	%al, %al
	je	.L4752
	addq	$24, %r14
	cmpq	%r14, -416(%rbp)
	jne	.L4701
	movq	-424(%rbp), %r12
	movq	-440(%rbp), %r15
.L4696:
	movq	-224(%rbp), %rax
	movq	-408(%rbp), %rsi
	leaq	24(%rax), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4BindEPNS1_13BytecodeLabelE@PLT
.L4691:
	movq	-456(%rbp), %rdi
	call	_ZN2v88internal11interpreter17TryFinallyBuilderD1Ev@PLT
	movq	816(%r15), %rax
	cmpl	$1, 24(%rax)
	jne	.L4753
.L4702:
	movl	-428(%rbp), %eax
	movq	336(%r15), %rdi
	movl	328(%r15), %esi
	movl	%eax, 328(%r15)
	testq	%rdi, %rdi
	je	.L4663
	subl	%eax, %esi
	movq	(%rdi), %rdx
	salq	$32, %rsi
	orq	%rax, %rsi
	call	*32(%rdx)
.L4663:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4754
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4680:
	.cfi_restore_state
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4JumpEPNS1_13BytecodeLabelE@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter14BytecodeLabels4BindEPNS1_20BytecodeArrayBuilderE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder13LoadUndefinedEv@PLT
	movq	-488(%rbp), %r8
	jmp	.L4681
	.p2align 4,,10
	.p2align 3
.L4679:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter14BytecodeLabels4BindEPNS1_20BytecodeArrayBuilderE@PLT
	jmp	.L4682
	.p2align 4,,10
	.p2align 3
.L4750:
	movl	328(%r15), %eax
	movq	16(%r8), %rdx
	movl	%eax, -416(%rbp)
	movzbl	4(%rdx), %eax
	andl	$63, %eax
	subl	$22, %eax
	cmpb	$1, %al
	jbe	.L4676
	movl	(%r8), %eax
	cmpl	$-1, %eax
	je	.L4676
	movb	$2, 496(%r15)
	movl	%eax, 500(%r15)
.L4676:
	movq	-408(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal11interpreter17BytecodeGenerator20PrepareAssignmentLhsEPNS0_10ExpressionENS2_25AccumulatorPreservingModeE.constprop.0
	movl	328(%r15), %r14d
	movq	336(%r15), %rdi
	leal	1(%r14), %eax
	cmpl	%eax, 332(%r15)
	movl	%eax, 328(%r15)
	cmovge	332(%r15), %eax
	movl	%eax, 332(%r15)
	testq	%rdi, %rdi
	je	.L4687
	movq	(%rdi), %rax
	movl	%r14d, %esi
	call	*16(%rax)
.L4687:
	movq	512(%r15), %rax
	movl	$19, %esi
	leaq	56(%rax), %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder23CreateEmptyArrayLiteralEi@PLT
	movq	%r12, %rdi
	movl	%r14d, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movl	328(%r15), %r8d
	movq	336(%r15), %rdi
	leal	1(%r8), %eax
	cmpl	%eax, 332(%r15)
	movl	%eax, 328(%r15)
	cmovge	332(%r15), %eax
	movl	%eax, 332(%r15)
	testq	%rdi, %rdi
	je	.L4688
	movq	(%rdi), %rax
	movl	%r8d, -448(%rbp)
	movl	%r8d, %esi
	call	*16(%rax)
	movl	-448(%rbp), %r8d
.L4688:
	xorl	%esi, %esi
	movq	%r12, %rdi
	movl	%r8d, -448(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralENS0_3SmiE@PLT
	movl	-448(%rbp), %r8d
	movq	%r12, %rdi
	movl	%r8d, %esi
	movl	%r8d, -472(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder8LoadTrueEv@PLT
	movl	%r13d, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	512(%r15), %rax
	movl	$14, %esi
	leaq	56(%rax), %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movl	$15, %esi
	movl	%eax, -448(%rbp)
	movq	512(%r15), %rax
	leaq	56(%rax), %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movl	%ebx, %r9d
	movl	%r14d, %ecx
	movq	%r15, %rdi
	movl	-448(%rbp), %edx
	movl	-472(%rbp), %r8d
	movq	-396(%rbp), %rsi
	pushq	%rdx
	movl	-388(%rbp), %edx
	pushq	%rax
	movl	-460(%rbp), %eax
	pushq	%rax
	movl	-432(%rbp), %eax
	pushq	%rax
	call	_ZN2v88internal11interpreter17BytecodeGenerator26BuildFillArrayWithIteratorENS2_14IteratorRecordENS1_8RegisterES4_S4_NS0_12FeedbackSlotES5_S5_S5_
	addq	$32, %rsp
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	movl	-464(%rbp), %ecx
	movq	-408(%rbp), %rsi
	movq	%r15, %rdi
	movzbl	-480(%rbp), %edx
	call	_ZN2v88internal11interpreter17BytecodeGenerator15BuildAssignmentERKNS2_17AssignmentLhsDataENS0_5Token5ValueENS0_18LookupHoistingModeE
	movl	-416(%rbp), %ecx
	movq	336(%r15), %rdi
	movl	328(%r15), %esi
	movl	%ecx, 328(%r15)
	testq	%rdi, %rdi
	je	.L4673
	subl	%ecx, %esi
	movq	(%rdi), %rdx
	movq	%rsi, %rax
	movl	%ecx, %esi
	salq	$32, %rax
	orq	%rax, %rsi
	call	*32(%rdx)
	jmp	.L4673
	.p2align 4,,10
	.p2align 3
.L4753:
	movl	-428(%rbp), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	jmp	.L4702
	.p2align 4,,10
	.p2align 3
.L4668:
	leal	2(%rsi), %edx
	cmpl	%edx, %eax
	movl	%edx, 328(%r15)
	cmovl	%edx, %eax
	movl	%eax, 332(%r15)
	jmp	.L4669
	.p2align 4,,10
	.p2align 3
.L4665:
	leal	2(%rbx), %edx
	cmpl	%edx, %eax
	movl	%edx, 328(%r15)
	cmovl	%edx, %eax
	movl	%eax, 332(%r15)
	jmp	.L4666
	.p2align 4,,10
	.p2align 3
.L4751:
	movslq	16(%rbx), %rsi
	salq	$32, %rsi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralENS0_3SmiE@PLT
	movl	-184(%rbp), %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder16CompareReferenceENS1_8RegisterE@PLT
	movq	-408(%rbp), %rdx
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11JumpIfFalseENS2_13ToBooleanModeEPNS1_13BytecodeLabelE@PLT
	movl	(%rbx), %r14d
	cmpl	$1, %r14d
	ja	.L4755
.L4693:
	movq	-224(%rbp), %rax
	movq	8(%rbx), %rbx
	movq	800(%rax), %r13
	jmp	.L4695
	.p2align 4,,10
	.p2align 3
.L4694:
	movq	16(%r13), %r13
	testq	%r13, %r13
	je	.L4699
.L4695:
	movq	0(%r13), %rax
	movl	$-1, %ecx
	movq	%rbx, %rdx
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	*16(%rax)
	testb	%al, %al
	je	.L4694
	jmp	.L4696
	.p2align 4,,10
	.p2align 3
.L4755:
	movq	-224(%rbp), %rax
	movl	-180(%rbp), %esi
	leaq	24(%rax), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	movl	(%rbx), %r14d
	jmp	.L4693
	.p2align 4,,10
	.p2align 3
.L4699:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L4754:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20914:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator33BuildDestructuringArrayAssignmentEPNS0_12ArrayLiteralENS0_5Token5ValueENS0_18LookupHoistingModeE, .-_ZN2v88internal11interpreter17BytecodeGenerator33BuildDestructuringArrayAssignmentEPNS0_12ArrayLiteralENS0_5Token5ValueENS0_18LookupHoistingModeE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator15VisitAssignmentEPNS0_10AssignmentE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator15VisitAssignmentEPNS0_10AssignmentE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator15VisitAssignmentEPNS0_10AssignmentE, @function
_ZN2v88internal11interpreter17BytecodeGenerator15VisitAssignmentEPNS0_10AssignmentE:
.LFB20919:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-96(%rbp), %r13
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$72, %rsp
	movq	8(%rsi), %rdx
	movq	%rdi, %rsi
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter17BytecodeGenerator20PrepareAssignmentLhsEPNS0_10ExpressionENS2_25AccumulatorPreservingModeE.constprop.0
	movq	16(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	movl	(%rbx), %eax
	cmpl	$-1, %eax
	je	.L4757
	cmpb	$2, 496(%r12)
	je	.L4757
	movb	$1, 496(%r12)
	movl	%eax, 500(%r12)
.L4757:
	movl	4(%rbx), %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%edx, %ecx
	shrl	$7, %edx
	shrl	$14, %ecx
	andl	$127, %edx
	andl	$1, %ecx
	call	_ZN2v88internal11interpreter17BytecodeGenerator15BuildAssignmentERKNS2_17AssignmentLhsDataENS0_5Token5ValueENS0_18LookupHoistingModeE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4763
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4763:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20919:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator15VisitAssignmentEPNS0_10AssignmentE, .-_ZN2v88internal11interpreter17BytecodeGenerator15VisitAssignmentEPNS0_10AssignmentE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator24VisitExpressionStatementEPNS0_19ExpressionStatementE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator24VisitExpressionStatementEPNS0_19ExpressionStatementE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator24VisitExpressionStatementEPNS0_19ExpressionStatementE, @function
_ZN2v88internal11interpreter17BytecodeGenerator24VisitExpressionStatementEPNS0_19ExpressionStatementE:
.LFB20757:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	(%rsi), %eax
	cmpl	$-1, %eax
	je	.L4765
	movb	$2, 496(%rdi)
	movl	%eax, 500(%rdi)
.L4765:
	movq	816(%r12), %rdx
	movq	8(%rsi), %r13
	leaq	-64(%rbp), %rcx
	xorl	%esi, %esi
	movl	328(%r12), %eax
	cmpb	$0, 8(%r12)
	movq	%r12, -56(%rbp)
	movq	%rdx, -64(%rbp)
	movl	%eax, -48(%rbp)
	movq	$1, -40(%rbp)
	movq	%rcx, 816(%r12)
	je	.L4779
.L4766:
	movq	336(%r12), %rdi
	movq	%rdx, 816(%r12)
	movl	%eax, 328(%r12)
	testq	%rdi, %rdi
	je	.L4764
	movq	%rsi, %rdx
	movq	(%rdi), %rcx
	movl	%eax, %esi
	salq	$32, %rdx
	orq	%rdx, %rsi
	call	*32(%rcx)
.L4764:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4780
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4779:
	.cfi_restore_state
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L4781
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator25VisitNoStackOverflowCheckEPNS0_7AstNodeE
.L4778:
	movq	-56(%rbp), %r12
	movl	-48(%rbp), %eax
	movq	-64(%rbp), %rdx
	movl	328(%r12), %esi
	subl	%eax, %esi
	jmp	.L4766
	.p2align 4,,10
	.p2align 3
.L4781:
	movb	$1, 8(%r12)
	jmp	.L4778
.L4780:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20757:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator24VisitExpressionStatementEPNS0_19ExpressionStatementE, .-_ZN2v88internal11interpreter17BytecodeGenerator24VisitExpressionStatementEPNS0_19ExpressionStatementE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator16VisitIfStatementEPNS0_11IfStatementE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator16VisitIfStatementEPNS0_11IfStatementE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator16VisitIfStatementEPNS0_11IfStatementE, @function
_ZN2v88internal11interpreter17BytecodeGenerator16VisitIfStatementEPNS0_11IfStatementE:
.LFB20759:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-208(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	leaq	24(%rdi), %rsi
	movq	%rbx, %rcx
	subq	$176, %rsp
	movq	568(%rdi), %rdx
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter29ConditionalControlFlowBuilderC1EPNS1_20BytecodeArrayBuilderEPNS1_20BlockCoverageBuilderEPNS0_7AstNodeE
	movl	(%rbx), %eax
	cmpl	$-1, %eax
	je	.L4783
	movb	$2, 496(%r12)
	movl	%eax, 500(%r12)
.L4783:
	movq	8(%rbx), %rdi
	call	_ZNK2v88internal10Expression15ToBooleanIsTrueEv@PLT
	testb	%al, %al
	je	.L4784
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter29ConditionalControlFlowBuilder4ThenEv@PLT
	cmpb	$0, 8(%r12)
	je	.L4804
.L4786:
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter29ConditionalControlFlowBuilderD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4805
	addq	$176, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4804:
	.cfi_restore_state
	movq	16(%rbx), %r14
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L4806
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	jmp	.L4786
	.p2align 4,,10
	.p2align 3
.L4784:
	movq	8(%rbx), %rdi
	call	_ZNK2v88internal10Expression16ToBooleanIsFalseEv@PLT
	testb	%al, %al
	je	.L4788
	movq	24(%rbx), %rax
	movl	4(%rax), %eax
	andl	$63, %eax
	cmpb	$10, %al
	je	.L4786
.L4803:
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter29ConditionalControlFlowBuilder4ElseEv@PLT
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator5VisitEPNS0_7AstNodeE
	jmp	.L4786
	.p2align 4,,10
	.p2align 3
.L4788:
	movq	8(%rbx), %rsi
	xorl	%r8d, %r8d
	leaq	-112(%rbp), %rcx
	movq	%r12, %rdi
	leaq	-152(%rbp), %rdx
	call	_ZN2v88internal11interpreter17BytecodeGenerator12VisitForTestEPNS0_10ExpressionEPNS1_14BytecodeLabelsES6_NS2_15TestFallthroughE
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter29ConditionalControlFlowBuilder4ThenEv@PLT
	cmpb	$0, 8(%r12)
	je	.L4807
.L4790:
	movq	24(%rbx), %rax
	movl	4(%rax), %eax
	andl	$63, %eax
	cmpb	$10, %al
	je	.L4786
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter29ConditionalControlFlowBuilder9JumpToEndEv@PLT
	jmp	.L4803
	.p2align 4,,10
	.p2align 3
.L4807:
	movq	16(%rbx), %r14
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L4808
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	jmp	.L4790
	.p2align 4,,10
	.p2align 3
.L4806:
	movb	$1, 8(%r12)
	jmp	.L4786
	.p2align 4,,10
	.p2align 3
.L4808:
	movb	$1, 8(%r12)
	jmp	.L4790
.L4805:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20759:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator16VisitIfStatementEPNS0_11IfStatementE, .-_ZN2v88internal11interpreter17BytecodeGenerator16VisitIfStatementEPNS0_11IfStatementE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator18VisitObjectLiteralEPNS0_13ObjectLiteralE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator18VisitObjectLiteralEPNS0_13ObjectLiteralE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator18VisitObjectLiteralEPNS0_13ObjectLiteralE, @function
_ZN2v88internal11interpreter17BytecodeGenerator18VisitObjectLiteralEPNS0_13ObjectLiteralE:
.LFB20850:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	leaq	24(%r13), %r14
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$216, %rsp
	movq	%rsi, -168(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal13ObjectLiteral17InitDepthAndFlagsEv@PLT
	movl	4(%rbx), %eax
	testb	$2, %ah
	jne	.L4810
	movl	12(%rbx), %edx
	testl	%edx, %edx
	je	.L5094
.L4810:
	movq	-168(%rbp), %rbx
	movq	%rbx, %rdi
	call	_ZNK2v88internal13ObjectLiteral22IsFastCloningSupportedEv@PLT
	xorl	%edi, %edi
	movzbl	%al, %esi
	movl	8(%rbx), %eax
	movl	%eax, -148(%rbp)
	addl	%eax, %eax
	cmpl	$2, %eax
	movl	4(%rbx), %eax
	sete	%dil
	movl	%edi, %edx
	orl	$4, %edx
	testb	$-128, %al
	cmovne	%edx, %edi
	movl	%edi, %edx
	orl	$8, %edx
	testb	$8, %ah
	cmovne	%edx, %edi
	movl	%edi, %edx
	orl	$16, %edx
	testb	$16, %ah
	cmovne	%edx, %edi
	call	_ZN2v88internal11interpreter24CreateObjectLiteralFlags6EncodeEib@PLT
	movl	328(%r13), %esi
	movq	336(%r13), %rdi
	movzbl	%al, %r12d
	leaq	328(%r13), %rax
	leal	1(%rsi), %ecx
	cmpl	%ecx, 332(%r13)
	movq	%rax, -224(%rbp)
	movl	%ecx, 328(%r13)
	cmovge	332(%r13), %ecx
	movl	%esi, -152(%rbp)
	movl	%ecx, 332(%r13)
	testq	%rdi, %rdi
	je	.L4815
	movq	(%rdi), %rax
	call	*16(%rax)
.L4815:
	movq	-168(%rbp), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rax
	movzbl	16(%rax), %ecx
	movb	%cl, -176(%rbp)
	cmpb	$6, %cl
	je	.L5095
	movq	-168(%rbp), %rax
	movq	%r14, %rdi
	movl	12(%rax), %eax
	testl	%eax, %eax
	je	.L5096
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder33AllocateDeferredConstantPoolEntryEv@PLT
	leaq	-112(%rbp), %rsi
	leaq	672(%r13), %rdi
	movq	%rax, %r15
	movq	-168(%rbp), %rax
	movq	%r15, -104(%rbp)
	movq	%rax, -112(%rbp)
	call	_ZNSt6vectorISt4pairIPN2v88internal13ObjectLiteralEmENS2_13ZoneAllocatorIS5_EEE12emplace_backIJS5_EEEvDpOT_
.L4820:
	movl	-152(%rbp), %esi
	movq	%r15, %rcx
	movl	%r12d, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator24BuildCreateObjectLiteralENS1_8RegisterEhm
	movl	$0, -148(%rbp)
.L4818:
	movq	16(%r13), %rbx
	movq	_ZN2v88internal7Literal5MatchEPvS2_@GOTPCREL(%rip), %rax
	movq	%rax, -96(%rbp)
	movq	16(%rbx), %rax
	movq	24(%rbx), %rdx
	subq	%rax, %rdx
	cmpq	$191, %rdx
	jbe	.L5097
	leaq	192(%rax), %rdx
	movq	%rdx, 16(%rbx)
.L4822:
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L5098
	movl	$8, -104(%rbp)
	movq	$0, (%rax)
	cmpl	$1, -104(%rbp)
	jbe	.L4824
	movl	$24, %edx
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L4825:
	movq	-112(%rbp), %rcx
	addq	$1, %rax
	movq	$0, (%rcx,%rdx)
	movl	-104(%rbp), %ecx
	addq	$24, %rdx
	cmpq	%rax, %rcx
	ja	.L4825
.L4824:
	movq	-168(%rbp), %r15
	pxor	%xmm0, %xmm0
	movslq	-148(%rbp), %rax
	movl	$0, -100(%rbp)
	movq	$0, -72(%rbp)
	movl	36(%r15), %edx
	movq	%rbx, -64(%rbp)
	movups	%xmm0, -88(%rbp)
	cmpl	%edx, %eax
	jge	.L4826
	movq	%r14, -192(%rbp)
	leaq	0(,%rax,8), %r12
	.p2align 4,,10
	.p2align 3
.L4899:
	movq	24(%r15), %rax
	movq	(%rax,%r12), %r14
	movq	(%r14), %rbx
	movq	%rbx, %rax
	andl	$3, %eax
	movq	%rax, -184(%rbp)
	jne	.L4827
	cmpb	$6, -176(%rbp)
	je	.L4828
	movq	%r14, %rdi
	call	_ZNK2v88internal21ObjectLiteralProperty18IsCompileTimeValueEv@PLT
	testb	%al, %al
	jne	.L4830
	movq	(%r14), %rbx
.L4828:
	movl	328(%r13), %eax
	andq	$-4, %rbx
	movl	%eax, -160(%rbp)
	movzbl	4(%rbx), %eax
	andl	$63, %eax
	cmpb	$41, %al
	movl	$0, %eax
	cmovne	%rax, %rbx
	cmpb	$6, 16(%r14)
	ja	.L4832
	movzbl	16(%r14), %eax
	leaq	.L4834(%rip), %rcx
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal11interpreter17BytecodeGenerator18VisitObjectLiteralEPNS0_13ObjectLiteralE,"a",@progbits
	.align 4
	.align 4
.L4834:
	.long	.L4838-.L4834
	.long	.L4838-.L4834
	.long	.L4838-.L4834
	.long	.L4837-.L4834
	.long	.L4836-.L4834
	.long	.L4835-.L4834
	.long	.L4833-.L4834
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator18VisitObjectLiteralEPNS0_13ObjectLiteralE
	.p2align 4,,10
	.p2align 3
.L4838:
	movq	%rbx, %rdi
	call	_ZNK2v88internal10Expression15IsStringLiteralEv@PLT
	testb	%al, %al
	je	.L4840
	movq	%r14, %rdi
	call	_ZNK2v88internal21ObjectLiteralProperty10emit_storeEv@PLT
	movq	8(%r14), %rsi
	testb	%al, %al
	movl	(%rsi), %eax
	je	.L4841
	cmpl	$-1, %eax
	je	.L4842
	cmpb	$2, 496(%r13)
	je	.L4842
	movb	$1, 496(%r13)
	movl	%eax, 500(%r13)
	movq	8(%r14), %rsi
.L4842:
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator24VisitForAccumulatorValueEPNS0_10ExpressionE
	movq	512(%r13), %rax
	movl	$12, %esi
	leaq	56(%rax), %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movq	8(%r14), %rdi
	movl	%eax, -184(%rbp)
	call	_ZN2v88internal15FunctionLiteral15NeedsHomeObjectEPNS0_10ExpressionE@PLT
	movl	-184(%rbp), %ecx
	testb	%al, %al
	je	.L4843
	movl	328(%r13), %r9d
	movq	-224(%rbp), %rdi
	movl	%ecx, -208(%rbp)
	movl	%r9d, -200(%rbp)
	call	_ZN2v88internal11interpreter25BytecodeRegisterAllocator11NewRegisterEv
	movq	-192(%rbp), %rdi
	movl	%eax, %esi
	movl	%eax, -184(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	8(%rbx), %rdx
	movl	-152(%rbp), %ebx
	movl	-208(%rbp), %ecx
	movq	-192(%rbp), %rdi
	movl	%ebx, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder21StoreNamedOwnPropertyENS1_8RegisterEPKNS0_12AstRawStringEi@PLT
	movq	%r13, %rdi
	movq	%r14, %rcx
	movl	%ebx, %edx
	movl	-184(%rbp), %r10d
	movl	%r10d, %esi
	call	_ZN2v88internal11interpreter17BytecodeGenerator18VisitSetHomeObjectENS1_8RegisterES3_PNS0_15LiteralPropertyE
	movl	-200(%rbp), %r9d
	movq	336(%r13), %rdi
	movl	328(%r13), %esi
	movl	%r9d, 328(%r13)
	testq	%rdi, %rdi
	je	.L5099
	subl	%r9d, %esi
	movq	(%rdi), %rdx
	movq	%rsi, %rax
	movl	%r9d, %esi
	salq	$32, %rax
	orq	%rax, %rsi
	call	*32(%rdx)
	movl	328(%r13), %esi
	subl	-160(%rbp), %esi
	movq	336(%r13), %rdi
	jmp	.L4839
	.p2align 4,,10
	.p2align 3
.L4836:
	movq	%r14, %rdi
	call	_ZNK2v88internal21ObjectLiteralProperty10emit_storeEv@PLT
	testb	%al, %al
	jne	.L4879
.L5091:
	movq	336(%r13), %rdi
	movl	328(%r13), %esi
	subl	-160(%rbp), %esi
.L4839:
	movl	-160(%rbp), %ecx
	movl	%ecx, 328(%r13)
	testq	%rdi, %rdi
	je	.L4830
	movq	(%rdi), %rax
	salq	$32, %rsi
	movl	%ecx, %r14d
	orq	%r14, %rsi
	call	*32(%rax)
.L4830:
	addl	$1, -148(%rbp)
	movl	36(%r15), %edx
	addq	$8, %r12
	movl	-148(%rbp), %eax
	cmpl	%edx, %eax
	jl	.L4899
	movq	-80(%rbp), %rbx
	movq	-88(%rbp), %rax
	movq	-192(%rbp), %r14
	movq	%rbx, -200(%rbp)
	cmpq	%rbx, %rax
	je	.L4826
.L4901:
	movq	%rax, %r15
	leaq	-144(%rbp), %rax
	movq	%r13, %r12
	movl	-152(%rbp), %r13d
	movq	%rax, -208(%rbp)
	.p2align 4,,10
	.p2align 3
.L4912:
	movq	(%r15), %rax
	movl	328(%r12), %ebx
	movq	336(%r12), %rdi
	movq	%rax, -192(%rbp)
	movq	8(%r15), %rax
	movq	%rax, -160(%rbp)
	leal	5(%rbx), %eax
	cmpl	%eax, 332(%r12)
	movl	%eax, 328(%r12)
	cmovge	332(%r12), %eax
	movl	%eax, 332(%r12)
	movl	%ebx, %eax
	movq	%rax, -176(%rbp)
	movq	%rax, %rcx
	testq	%rdi, %rdi
	je	.L4905
	movabsq	$21474836480, %rsi
	movq	(%rdi), %rax
	orq	%rcx, %rsi
	call	*24(%rax)
.L4905:
	movabsq	$21474836480, %rax
	movl	%ebx, %edx
	orq	-176(%rbp), %rax
	movl	%r13d, %esi
	movq	%r14, %rdi
	movq	%rax, -184(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder12MoveRegisterENS1_8RegisterES3_@PLT
	cmpb	$0, 8(%r12)
	movq	816(%r12), %rax
	movq	%r12, -136(%rbp)
	movq	$2, -120(%rbp)
	leal	1(%rbx), %r9d
	movq	%rax, -144(%rbp)
	movl	328(%r12), %eax
	movl	%eax, -128(%rbp)
	movq	-208(%rbp), %rax
	movq	%rax, 816(%r12)
	jne	.L4906
	movl	%r9d, -216(%rbp)
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	movl	-216(%rbp), %r9d
	jb	.L5100
	movq	-192(%rbp), %rsi
	movq	%r12, %rdi
	movl	%r9d, -216(%rbp)
	call	_ZN2v88internal11interpreter17BytecodeGenerator25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movl	-216(%rbp), %r9d
.L4906:
	movl	%r9d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-136(%rbp), %rdx
	movq	-144(%rbp), %rax
	movq	336(%rdx), %rdi
	movq	%rax, 816(%rdx)
	movl	-128(%rbp), %eax
	movl	328(%rdx), %esi
	movl	%eax, 328(%rdx)
	testq	%rdi, %rdi
	je	.L4908
	subl	%eax, %esi
	movq	(%rdi), %rdx
	movq	%rsi, %rcx
	movl	%eax, %esi
	salq	$32, %rcx
	orq	%rcx, %rsi
	call	*32(%rdx)
.L4908:
	movq	-160(%rbp), %rax
	leal	2(%rbx), %ecx
	movl	%r13d, %esi
	movq	%r12, %rdi
	movq	(%rax), %rdx
	call	_ZN2v88internal11interpreter17BytecodeGenerator20VisitLiteralAccessorENS1_8RegisterEPNS0_15LiteralPropertyES3_
	leal	3(%rbx), %ecx
	movl	%r13d, %esi
	movq	%r12, %rdi
	movq	-160(%rbp), %rax
	movq	8(%rax), %rdx
	call	_ZN2v88internal11interpreter17BytecodeGenerator20VisitLiteralAccessorENS1_8RegisterEPNS0_15LiteralPropertyES3_
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralENS0_3SmiE@PLT
	leal	4(%rbx), %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-184(%rbp), %rdx
	movl	$215, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_12RegisterListE@PLT
	movq	336(%r12), %rdi
	movl	328(%r12), %esi
	movl	%ebx, 328(%r12)
	testq	%rdi, %rdi
	je	.L4909
	movq	(%rdi), %rax
	subl	%ebx, %esi
	addq	$16, %r15
	salq	$32, %rsi
	orq	-176(%rbp), %rsi
	call	*32(%rax)
	cmpq	%r15, -200(%rbp)
	jne	.L4912
.L5092:
	movq	-168(%rbp), %rax
	movq	%r12, %r13
	movl	36(%rax), %edx
.L4913:
	movslq	-148(%rbp), %rax
	movq	%r13, %r15
	movq	%rax, %rbx
	salq	$3, %rax
	movq	%rax, -160(%rbp)
	leaq	-144(%rbp), %rax
	movq	%rax, -176(%rbp)
	cmpl	%edx, %ebx
	jl	.L4950
	jmp	.L4826
	.p2align 4,,10
	.p2align 3
.L5103:
	cmpb	$2, %al
	ja	.L5101
	leal	1(%rbx), %eax
	cmpl	%eax, 332(%r15)
	movl	%eax, 328(%r15)
	cmovge	332(%r15), %eax
	movl	%eax, 332(%r15)
	testq	%rdi, %rdi
	je	.L4926
	movq	(%rdi), %rax
	movl	%ebx, %esi
	call	*16(%rax)
.L4926:
	movq	%r12, %rsi
	movl	%ebx, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator20BuildLoadPropertyKeyEPNS0_15LiteralPropertyENS1_8RegisterE
	movq	8(%r12), %rsi
	movl	(%rsi), %eax
	cmpl	$-1, %eax
	je	.L4927
	cmpb	$2, 496(%r15)
	je	.L4927
	movb	$1, 496(%r15)
	movl	%eax, 500(%r15)
	movq	8(%r12), %rsi
.L4927:
	movzbl	4(%rsi), %eax
	andl	$63, %eax
	cmpb	$31, %al
	jne	.L4928
	cmpq	$0, 56(%rsi)
	je	.L4928
	movl	328(%r15), %r13d
	movq	336(%r15), %rdi
	leal	1(%r13), %eax
	cmpl	%eax, 332(%r15)
	movl	%eax, 328(%r15)
	cmovge	332(%r15), %eax
	movl	%eax, 332(%r15)
	testq	%rdi, %rdi
	je	.L4931
	movq	(%rdi), %rax
	movl	%r13d, %esi
	call	*16(%rax)
.L4931:
	movq	8(%r12), %rsi
	movq	%r15, %rdi
	movl	%ebx, %edx
	movzbl	4(%rsi), %eax
	andl	$63, %eax
	cmpb	$31, %al
	movl	$0, %eax
	cmovne	%rax, %rsi
	call	_ZN2v88internal11interpreter17BytecodeGenerator17VisitClassLiteralEPNS0_12ClassLiteralENS1_8RegisterE
	movl	%r13d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
.L4930:
	movq	8(%r12), %rdi
	call	_ZN2v88internal15FunctionLiteral15NeedsHomeObjectEPNS0_10ExpressionE@PLT
	testb	%al, %al
	je	.L4933
	movq	512(%r15), %rax
	leaq	56(%rax), %rdi
	movq	536(%r15), %rax
	movzbl	129(%rax), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%esi, %esi
	andl	$-9, %esi
	addl	$11, %esi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movl	-152(%rbp), %esi
	movq	%r14, %rdi
	movl	%eax, -184(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	movl	-184(%rbp), %edx
	movl	%r13d, %esi
	movq	%rax, %rdi
	movq	536(%r15), %rax
	movzbl	129(%rax), %ecx
	andl	$1, %ecx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder23StoreHomeObjectPropertyENS1_8RegisterEiNS0_12LanguageModeE@PLT
.L4933:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	_ZNK2v88internal15LiteralProperty20NeedsSetFunctionNameEv@PLT
	movl	$17, %esi
	testb	%al, %al
	movq	512(%r15), %rax
	setne	%r12b
	leaq	56(%rax), %rdi
	addl	%r12d, %r12d
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movl	%r13d, %esi
	movq	%r14, %rdi
	movl	%eax, -184(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	movl	-152(%rbp), %esi
	movl	%r12d, %ecx
	movl	%ebx, %edx
	movl	-184(%rbp), %r8d
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreDataPropertyInLiteralENS1_8RegisterES3_NS_4base5FlagsINS0_25DataPropertyInLiteralFlagEiEEi@PLT
	movl	328(%r15), %esi
	movq	336(%r15), %rdi
	subl	%ebx, %esi
.L4925:
	movl	%ebx, 328(%r15)
	testq	%rdi, %rdi
	je	.L4920
	movq	%rsi, %rax
	movq	(%rdi), %rdx
	movl	%ebx, %esi
	salq	$32, %rax
	orq	%rax, %rsi
	call	*32(%rdx)
.L4920:
	movq	-168(%rbp), %rbx
	addl	$1, -148(%rbp)
	addq	$8, -160(%rbp)
	movl	-148(%rbp), %eax
	cmpl	%eax, 36(%rbx)
	jle	.L4826
.L4950:
	movq	-168(%rbp), %rax
	movq	-160(%rbp), %rbx
	movq	24(%rax), %rax
	movq	(%rax,%rbx), %r12
	movl	328(%r15), %ebx
	movzbl	16(%r12), %eax
	cmpb	$5, %al
	je	.L5102
	movq	336(%r15), %rdi
	cmpb	$4, %al
	jbe	.L5103
	xorl	%esi, %esi
	cmpb	$6, %al
	jne	.L4925
	leal	2(%rbx), %eax
	cmpl	%eax, 332(%r15)
	movl	%ebx, %r13d
	movl	%eax, 328(%r15)
	cmovge	332(%r15), %eax
	movl	%eax, 332(%r15)
	testq	%rdi, %rdi
	je	.L4944
	movabsq	$8589934592, %rsi
	movq	(%rdi), %rax
	orq	%r13, %rsi
	call	*24(%rax)
.L4944:
	movl	-152(%rbp), %esi
	movl	%ebx, %edx
	movq	%r14, %rdi
	movabsq	$8589934592, %rax
	orq	%rax, %r13
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder12MoveRegisterENS1_8RegisterES3_@PLT
	movq	8(%r12), %rsi
	movl	(%rsi), %eax
	cmpl	$-1, %eax
	je	.L4945
	cmpb	$2, 496(%r15)
	je	.L4945
	movb	$1, 496(%r15)
	movl	%eax, 500(%r15)
	movq	8(%r12), %rsi
.L4945:
	movq	816(%r15), %rax
	cmpb	$0, 8(%r15)
	movq	%rsi, -184(%rbp)
	leal	1(%rbx), %r12d
	movq	%r15, -136(%rbp)
	movq	%rax, -144(%rbp)
	movl	328(%r15), %eax
	movq	$2, -120(%rbp)
	movl	%eax, -128(%rbp)
	movq	-176(%rbp), %rax
	movq	%rax, 816(%r15)
	jne	.L4946
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r15), %rax
	movq	-184(%rbp), %rsi
	jb	.L5104
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator25VisitNoStackOverflowCheckEPNS0_7AstNodeE
.L4946:
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-136(%rbp), %rdx
	movq	-144(%rbp), %rax
	movq	336(%rdx), %rdi
	movq	%rax, 816(%rdx)
	movl	-128(%rbp), %eax
	movl	328(%rdx), %esi
	movl	%eax, 328(%rdx)
	testq	%rdi, %rdi
	je	.L4948
	subl	%eax, %esi
	movq	(%rdi), %rdx
	movq	%rsi, %rcx
	movl	%eax, %esi
	salq	$32, %rcx
	orq	%rcx, %rsi
	call	*32(%rdx)
.L4948:
	movq	%r13, %rdx
	movl	$486, %esi
.L5093:
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_12RegisterListE@PLT
	movl	328(%r15), %esi
	movq	336(%r15), %rdi
	subl	%ebx, %esi
	jmp	.L4925
	.p2align 4,,10
	.p2align 3
.L4837:
	movq	%r14, %rdi
	call	_ZNK2v88internal21ObjectLiteralProperty10emit_storeEv@PLT
	testb	%al, %al
	je	.L5091
	movq	-64(%rbp), %rax
	movq	%rbx, %rdi
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal7Literal4HashEv@PLT
	movq	-112(%rbp), %r11
	movq	%rbx, -144(%rbp)
	movq	%rbx, %rdi
	movl	%eax, %r10d
	movl	-104(%rbp), %eax
	subl	$1, %eax
	movl	%eax, %edx
	andl	%r10d, %edx
	leaq	(%rdx,%rdx,2), %rcx
	salq	$3, %rcx
	leaq	(%r11,%rcx), %r9
	movq	(%r9), %rsi
	testq	%rsi, %rsi
	je	.L4951
	movq	%rbx, -208(%rbp)
	movl	%r10d, %ebx
	movq	%r12, -216(%rbp)
	movq	%rcx, %r12
.L4865:
	cmpl	16(%r9), %ebx
	je	.L5105
.L4861:
	addq	$1, %rdx
	andl	%eax, %edx
	leaq	(%rdx,%rdx,2), %rcx
	leaq	0(,%rcx,8), %r12
	leaq	(%r11,%r12), %r9
	movq	(%r9), %rsi
	testq	%rsi, %rsi
	jne	.L4865
	movl	%ebx, %r10d
	movq	-216(%rbp), %r12
	movq	-208(%rbp), %rbx
.L4951:
	movq	%rdi, (%r9)
	movq	$0, 8(%r9)
	movl	%r10d, 16(%r9)
	movl	-100(%rbp), %eax
	addl	$1, %eax
	movl	%eax, %edx
	movl	%eax, -100(%rbp)
	shrl	$2, %edx
	addl	%edx, %eax
	cmpl	-104(%rbp), %eax
	jb	.L4863
	movq	-232(%rbp), %rsi
	leaq	-112(%rbp), %rdi
	movl	%r10d, -208(%rbp)
	movq	%rdi, -200(%rbp)
	call	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_26HashEqualityThenKeyMatcherIS2_PFbS2_S2_EEENS_8internal20ZoneAllocationPolicyEE6ResizeES8_
	movl	-208(%rbp), %r10d
	leaq	-144(%rbp), %r9
	movq	-200(%rbp), %rdi
	movq	%r9, %rsi
	movl	%r10d, %edx
	call	_ZNK2v84base19TemplateHashMapImplIPvS2_NS0_26HashEqualityThenKeyMatcherIS2_PFbS2_S2_EEENS_8internal20ZoneAllocationPolicyEE5ProbeERKS2_j
	movq	%rax, %r9
	jmp	.L4863
	.p2align 4,,10
	.p2align 3
.L4835:
	movq	8(%r14), %rdi
	call	_ZNK2v88internal10Expression13IsNullLiteralEv@PLT
	testb	%al, %al
	jne	.L5091
	movl	328(%r13), %ebx
	movq	336(%r13), %rdi
	leal	2(%rbx), %eax
	cmpl	%eax, 332(%r13)
	movq	%rbx, %rdx
	movl	%eax, 328(%r13)
	cmovge	332(%r13), %eax
	movl	%eax, 332(%r13)
	testq	%rdi, %rdi
	je	.L4854
	movq	(%rdi), %rax
	movq	%rbx, %rsi
	movl	%ebx, -184(%rbp)
	btsq	$33, %rsi
	call	*24(%rax)
	movl	-184(%rbp), %edx
.L4854:
	movl	-152(%rbp), %esi
	movl	%edx, -184(%rbp)
	btsq	$33, %rbx
	movq	-192(%rbp), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder12MoveRegisterENS1_8RegisterES3_@PLT
	movq	8(%r14), %rsi
	movl	-184(%rbp), %edx
	movl	(%rsi), %eax
	cmpl	$-1, %eax
	je	.L4855
	cmpb	$2, 496(%r13)
	je	.L4855
	movb	$1, 496(%r13)
	movl	%eax, 500(%r13)
	movq	8(%r14), %rsi
.L4855:
	movq	816(%r13), %rax
	cmpb	$0, 8(%r13)
	leaq	-144(%rbp), %r9
	movq	%rsi, -184(%rbp)
	movq	%r13, -136(%rbp)
	leal	1(%rdx), %r14d
	movq	%rax, -144(%rbp)
	movl	328(%r13), %eax
	movq	$2, -120(%rbp)
	movl	%eax, -128(%rbp)
	movq	%r9, 816(%r13)
	jne	.L4856
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	0(%r13), %rax
	movq	-184(%rbp), %rsi
	jb	.L5106
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator25VisitNoStackOverflowCheckEPNS0_7AstNodeE
.L4856:
	movq	-192(%rbp), %rdi
	movl	%r14d, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-136(%rbp), %rdx
	movq	-144(%rbp), %rax
	movq	336(%rdx), %rdi
	movq	%rax, 816(%rdx)
	movl	-128(%rbp), %eax
	movl	328(%rdx), %esi
	movl	%eax, 328(%rdx)
	testq	%rdi, %rdi
	je	.L4858
	subl	%eax, %esi
	movq	(%rdi), %rdx
	movq	%rsi, %rcx
	movl	%eax, %esi
	salq	$32, %rcx
	orq	%rcx, %rsi
	call	*32(%rdx)
.L4858:
	movq	-192(%rbp), %rdi
	movq	%rbx, %rdx
	movl	$228, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_12RegisterListE@PLT
	jmp	.L5091
	.p2align 4,,10
	.p2align 3
.L5096:
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder50EmptyObjectBoilerplateDescriptionConstantPoolEntryEv@PLT
	movq	%rax, %r15
	jmp	.L4820
	.p2align 4,,10
	.p2align 3
.L5094:
	andl	$4096, %eax
	orl	36(%rbx), %eax
	jne	.L4810
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder24CreateEmptyObjectLiteralEv@PLT
	jmp	.L4809
	.p2align 4,,10
	.p2align 3
.L4909:
	addq	$16, %r15
	cmpq	%r15, -200(%rbp)
	jne	.L4912
	jmp	.L5092
.L4832:
	movq	336(%r13), %rdi
	xorl	%esi, %esi
	jmp	.L4839
	.p2align 4,,10
	.p2align 3
.L5102:
	movq	8(%r12), %rdi
	call	_ZNK2v88internal10Expression13IsNullLiteralEv@PLT
	testb	%al, %al
	jne	.L4915
	movl	328(%r15), %r13d
	movq	336(%r15), %rdi
	leal	2(%r13), %eax
	cmpl	%eax, 332(%r15)
	movq	%r13, %rdx
	movl	%eax, 328(%r15)
	cmovge	332(%r15), %eax
	movl	%eax, 332(%r15)
	testq	%rdi, %rdi
	je	.L4916
	movq	(%rdi), %rax
	movl	%r13d, -184(%rbp)
	movabsq	$8589934592, %rsi
	orq	%r13, %rsi
	call	*24(%rax)
	movl	-184(%rbp), %edx
.L4916:
	movl	-152(%rbp), %esi
	movq	%r14, %rdi
	movl	%edx, -184(%rbp)
	movabsq	$8589934592, %rax
	orq	%rax, %r13
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder12MoveRegisterENS1_8RegisterES3_@PLT
	movq	8(%r12), %rsi
	movl	-184(%rbp), %edx
	movl	(%rsi), %eax
	cmpl	$-1, %eax
	je	.L4917
	cmpb	$2, 496(%r15)
	je	.L4917
	movb	$1, 496(%r15)
	movl	%eax, 500(%r15)
	movq	8(%r12), %rsi
.L4917:
	movq	816(%r15), %rax
	leal	1(%rdx), %r12d
	movq	%r15, %rdi
	movq	%r15, -136(%rbp)
	movq	$2, -120(%rbp)
	movq	%rax, -144(%rbp)
	movl	328(%r15), %eax
	movl	%eax, -128(%rbp)
	movq	-176(%rbp), %rax
	movq	%rax, 816(%r15)
	call	_ZN2v88internal11interpreter17BytecodeGenerator5VisitEPNS0_7AstNodeE
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-136(%rbp), %rdx
	movq	-144(%rbp), %rax
	movq	336(%rdx), %rdi
	movq	%rax, 816(%rdx)
	movl	-128(%rbp), %eax
	movl	328(%rdx), %esi
	movl	%eax, 328(%rdx)
	testq	%rdi, %rdi
	je	.L4918
	subl	%eax, %esi
	movq	(%rdi), %rdx
	movq	%rsi, %rcx
	movl	%eax, %esi
	salq	$32, %rcx
	orq	%rcx, %rsi
	call	*32(%rdx)
.L4918:
	movq	%r13, %rdx
	movl	$228, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_12RegisterListE@PLT
.L4915:
	movq	336(%r15), %rdi
	movl	328(%r15), %esi
	movl	%ebx, 328(%r15)
	testq	%rdi, %rdi
	je	.L4920
	subl	%ebx, %esi
	movq	(%rdi), %rax
	salq	$32, %rsi
	orq	%rbx, %rsi
	call	*32(%rax)
	jmp	.L4920
	.p2align 4,,10
	.p2align 3
.L5101:
	leal	4(%rbx), %eax
	cmpl	%eax, 332(%r15)
	movl	%ebx, %r13d
	movl	%eax, 328(%r15)
	cmovge	332(%r15), %eax
	movl	%eax, 332(%r15)
	testq	%rdi, %rdi
	je	.L4936
	movabsq	$17179869184, %rsi
	movq	(%rdi), %rax
	orq	%r13, %rsi
	call	*24(%rax)
.L4936:
	movl	-152(%rbp), %esi
	movl	%ebx, %edx
	movq	%r14, %rdi
	movabsq	$17179869184, %rax
	orq	%rax, %r13
	movq	%r13, -184(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder12MoveRegisterENS1_8RegisterES3_@PLT
	movq	%r12, %rsi
	leal	1(%rbx), %edx
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator20BuildLoadPropertyKeyEPNS0_15LiteralPropertyENS1_8RegisterE
	movq	8(%r12), %rsi
	movl	(%rsi), %eax
	cmpl	$-1, %eax
	je	.L4937
	cmpb	$2, 496(%r15)
	je	.L4937
	movb	$1, 496(%r15)
	movl	%eax, 500(%r15)
	movq	8(%r12), %rsi
.L4937:
	movq	816(%r15), %rax
	cmpb	$0, 8(%r15)
	movq	%rsi, -192(%rbp)
	leal	2(%rbx), %r13d
	movq	%r15, -136(%rbp)
	movq	%rax, -144(%rbp)
	movl	328(%r15), %eax
	movq	$2, -120(%rbp)
	movl	%eax, -128(%rbp)
	movq	-176(%rbp), %rax
	movq	%rax, 816(%r15)
	jne	.L4938
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r15), %rax
	movq	-192(%rbp), %rsi
	jb	.L5107
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator25VisitNoStackOverflowCheckEPNS0_7AstNodeE
.L4938:
	movl	%r13d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-136(%rbp), %rdx
	movq	-144(%rbp), %rax
	movq	336(%rdx), %rdi
	movq	%rax, 816(%rdx)
	movl	-128(%rbp), %eax
	movl	328(%rdx), %esi
	movl	%eax, 328(%rdx)
	testq	%rdi, %rdi
	je	.L4940
	subl	%eax, %esi
	movq	(%rdi), %rdx
	movq	%rsi, %rcx
	movl	%eax, %esi
	salq	$32, %rcx
	orq	%rcx, %rsi
	call	*32(%rdx)
.L4940:
	movq	8(%r12), %rdi
	call	_ZN2v88internal15FunctionLiteral15NeedsHomeObjectEPNS0_10ExpressionE@PLT
	testb	%al, %al
	je	.L4941
	movq	512(%r15), %rax
	leaq	56(%rax), %rdi
	movq	536(%r15), %rax
	movzbl	129(%rax), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%esi, %esi
	andl	$-9, %esi
	addl	$11, %esi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movl	-152(%rbp), %esi
	movq	%r14, %rdi
	movl	%eax, -192(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	movl	-192(%rbp), %edx
	movl	%r13d, %esi
	movq	%rax, %rdi
	movq	536(%r15), %rax
	movzbl	129(%rax), %ecx
	andl	$1, %ecx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder23StoreHomeObjectPropertyENS1_8RegisterEiNS0_12LanguageModeE@PLT
.L4941:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralENS0_3SmiE@PLT
	leal	3(%rbx), %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	xorl	%esi, %esi
	cmpb	$3, 16(%r12)
	movq	-184(%rbp), %rdx
	setne	%sil
	addl	$217, %esi
	jmp	.L5093
	.p2align 4,,10
	.p2align 3
.L5100:
	movb	$1, 8(%r12)
	jmp	.L4906
	.p2align 4,,10
	.p2align 3
.L4928:
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator21VisitForRegisterValueEPNS0_10ExpressionE
	movl	%eax, %r13d
	jmp	.L4930
	.p2align 4,,10
	.p2align 3
.L4840:
	movl	328(%r13), %ebx
	movq	336(%r13), %rdi
	leal	3(%rbx), %eax
	cmpl	%eax, 332(%r13)
	movl	%eax, 328(%r13)
	cmovge	332(%r13), %eax
	movl	%eax, 332(%r13)
	movl	%ebx, %eax
	testq	%rdi, %rdi
	je	.L4847
	movq	(%rdi), %rdx
	movq	%rax, -184(%rbp)
	movabsq	$12884901888, %rsi
	orq	%rax, %rsi
	call	*24(%rdx)
	movq	-184(%rbp), %rax
.L4847:
	movl	-152(%rbp), %esi
	movq	-192(%rbp), %rdi
	movabsq	$12884901888, %rdx
	orq	%rdx, %rax
	movl	%ebx, %edx
	movq	%rax, -208(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder12MoveRegisterENS1_8RegisterES3_@PLT
	movq	(%r14), %rsi
	andq	$-4, %rsi
	movl	(%rsi), %eax
	cmpl	$-1, %eax
	je	.L4848
	cmpb	$2, 496(%r13)
	je	.L4848
	movb	$1, 496(%r13)
	movl	%eax, 500(%r13)
	movq	(%r14), %rsi
	andq	$-4, %rsi
.L4848:
	leaq	-144(%rbp), %r9
	movq	816(%r13), %rax
	leal	1(%rbx), %r10d
	movq	%r13, %rdi
	movq	%r9, 816(%r13)
	movq	%rax, -144(%rbp)
	movl	328(%r13), %eax
	movq	%r9, -184(%rbp)
	movl	%r10d, -200(%rbp)
	movl	%eax, -128(%rbp)
	movq	%r13, -136(%rbp)
	movq	$2, -120(%rbp)
	call	_ZN2v88internal11interpreter17BytecodeGenerator5VisitEPNS0_7AstNodeE
	movl	-200(%rbp), %r10d
	movq	-192(%rbp), %rdi
	movl	%r10d, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-136(%rbp), %rdx
	movq	-144(%rbp), %rax
	movq	-184(%rbp), %r9
	movq	336(%rdx), %rdi
	movq	%rax, 816(%rdx)
	movl	-128(%rbp), %eax
	movl	328(%rdx), %esi
	testq	%rdi, %rdi
	movl	%eax, 328(%rdx)
	je	.L4849
	subl	%eax, %esi
	movq	(%rdi), %rdx
	movq	%rsi, %rcx
	movl	%eax, %esi
	salq	$32, %rcx
	orq	%rcx, %rsi
	call	*32(%rdx)
	movq	-184(%rbp), %r9
.L4849:
	movq	8(%r14), %rsi
	movl	(%rsi), %eax
	cmpl	$-1, %eax
	je	.L4850
	cmpb	$2, 496(%r13)
	je	.L4850
	movb	$1, 496(%r13)
	movl	%eax, 500(%r13)
	movq	8(%r14), %rsi
.L4850:
	movq	816(%r13), %rax
	movq	%r13, %rdi
	movq	%r9, 816(%r13)
	addl	$2, %ebx
	movq	%r13, -136(%rbp)
	movq	%rax, -144(%rbp)
	movl	328(%r13), %eax
	movq	$2, -120(%rbp)
	movl	%eax, -128(%rbp)
	call	_ZN2v88internal11interpreter17BytecodeGenerator5VisitEPNS0_7AstNodeE
	movq	-192(%rbp), %rdi
	movl	%ebx, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	-136(%rbp), %rdx
	movq	-144(%rbp), %rax
	movq	336(%rdx), %rdi
	movq	%rax, 816(%rdx)
	movl	-128(%rbp), %eax
	movl	328(%rdx), %esi
	movl	%eax, 328(%rdx)
	testq	%rdi, %rdi
	je	.L4851
	subl	%eax, %esi
	movq	(%rdi), %rdx
	movq	%rsi, %rcx
	movl	%eax, %esi
	salq	$32, %rcx
	orq	%rcx, %rsi
	call	*32(%rdx)
.L4851:
	movq	%r14, %rdi
	call	_ZNK2v88internal21ObjectLiteralProperty10emit_storeEv@PLT
	testb	%al, %al
	je	.L5091
	movq	-208(%rbp), %rdx
	movq	-192(%rbp), %rdi
	movl	$251, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_12RegisterListE@PLT
	movl	-152(%rbp), %edx
	movq	%r14, %rcx
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator18VisitSetHomeObjectENS1_8RegisterES3_PNS0_15LiteralPropertyE
	jmp	.L5091
	.p2align 4,,10
	.p2align 3
.L5105:
	movq	%rdx, -200(%rbp)
	call	*-96(%rbp)
	testb	%al, %al
	jne	.L4862
	movl	-104(%rbp), %eax
	movq	-112(%rbp), %r11
	movq	-144(%rbp), %rdi
	movq	-200(%rbp), %rdx
	subl	$1, %eax
	jmp	.L4861
	.p2align 4,,10
	.p2align 3
.L4826:
	movl	-152(%rbp), %esi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE@PLT
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4809
	call	_ZdlPv@PLT
.L4809:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5108
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4879:
	.cfi_restore_state
	movq	-64(%rbp), %rax
	movq	%rbx, %rdi
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal7Literal4HashEv@PLT
	movq	-112(%rbp), %r11
	movq	%rbx, -144(%rbp)
	movq	%rbx, %rdi
	movl	%eax, %r10d
	movl	-104(%rbp), %eax
	subl	$1, %eax
	movl	%eax, %edx
	andl	%r10d, %edx
	leaq	(%rdx,%rdx,2), %rcx
	salq	$3, %rcx
	leaq	(%r11,%rcx), %r9
	movq	(%r9), %rsi
	testq	%rsi, %rsi
	je	.L4952
	movq	%rbx, -208(%rbp)
	movl	%r10d, %ebx
	movq	%r12, -216(%rbp)
	movq	%rcx, %r12
.L4885:
	cmpl	16(%r9), %ebx
	je	.L5109
.L4881:
	addq	$1, %rdx
	andl	%eax, %edx
	leaq	(%rdx,%rdx,2), %rcx
	leaq	0(,%rcx,8), %r12
	leaq	(%r11,%r12), %r9
	movq	(%r9), %rsi
	testq	%rsi, %rsi
	jne	.L4885
	movl	%ebx, %r10d
	movq	-216(%rbp), %r12
	movq	-208(%rbp), %rbx
.L4952:
	movq	%rdi, (%r9)
	movq	$0, 8(%r9)
	movl	%r10d, 16(%r9)
	movl	-100(%rbp), %eax
	addl	$1, %eax
	movl	%eax, %edx
	movl	%eax, -100(%rbp)
	shrl	$2, %edx
	addl	%edx, %eax
	cmpl	-104(%rbp), %eax
	jb	.L4883
	movq	-232(%rbp), %rsi
	leaq	-112(%rbp), %rdi
	movl	%r10d, -208(%rbp)
	movq	%rdi, -200(%rbp)
	call	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_26HashEqualityThenKeyMatcherIS2_PFbS2_S2_EEENS_8internal20ZoneAllocationPolicyEE6ResizeES8_
	movl	-208(%rbp), %r10d
	leaq	-144(%rbp), %r9
	movq	-200(%rbp), %rdi
	movq	%r9, %rsi
	movl	%r10d, %edx
	call	_ZNK2v84base19TemplateHashMapImplIPvS2_NS0_26HashEqualityThenKeyMatcherIS2_PFbS2_S2_EEENS_8internal20ZoneAllocationPolicyEE5ProbeERKS2_j
	movq	%rax, %r9
	jmp	.L4883
	.p2align 4,,10
	.p2align 3
.L5109:
	movq	%rdx, -200(%rbp)
	call	*-96(%rbp)
	testb	%al, %al
	jne	.L4882
	movl	-104(%rbp), %eax
	movq	-112(%rbp), %r11
	movq	-144(%rbp), %rdi
	movq	-200(%rbp), %rdx
	subl	$1, %eax
	jmp	.L4881
	.p2align 4,,10
	.p2align 3
.L4841:
	cmpl	$-1, %eax
	je	.L4846
	cmpb	$2, 496(%r13)
	je	.L4846
	movb	$1, 496(%r13)
	movl	%eax, 500(%r13)
	movq	8(%r14), %rsi
.L4846:
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator14VisitForEffectEPNS0_10ExpressionE
	movl	328(%r13), %esi
	movq	336(%r13), %rdi
	subl	-160(%rbp), %esi
	jmp	.L4839
	.p2align 4,,10
	.p2align 3
.L4862:
	movq	%r12, %rcx
	addq	-112(%rbp), %rcx
	movl	%ebx, %r10d
	movq	-216(%rbp), %r12
	cmpq	$0, (%rcx)
	movq	-208(%rbp), %rbx
	movq	%rcx, %r9
	je	.L5110
.L4863:
	movq	8(%r9), %rdx
	testq	%rdx, %rdx
	je	.L5111
.L4867:
	movq	%r14, (%rdx)
	movl	328(%r13), %esi
	movq	336(%r13), %rdi
	subl	-160(%rbp), %esi
	jmp	.L4839
	.p2align 4,,10
	.p2align 3
.L4882:
	movq	%r12, %rcx
	addq	-112(%rbp), %rcx
	movl	%ebx, %r10d
	movq	-216(%rbp), %r12
	cmpq	$0, (%rcx)
	movq	-208(%rbp), %rbx
	movq	%rcx, %r9
	je	.L5112
.L4883:
	movq	8(%r9), %rdx
	testq	%rdx, %rdx
	je	.L5113
.L4887:
	movq	%r14, 8(%rdx)
	jmp	.L5091
	.p2align 4,,10
	.p2align 3
.L4827:
	movq	-80(%rbp), %rbx
	movq	-88(%rbp), %rax
	movq	-192(%rbp), %r14
	movq	%rbx, -200(%rbp)
	cmpq	%rbx, %rax
	jne	.L4901
	jmp	.L4913
	.p2align 4,,10
	.p2align 3
.L5106:
	movb	$1, 8(%r13)
	jmp	.L4856
	.p2align 4,,10
	.p2align 3
.L4843:
	movl	-152(%rbp), %esi
	movq	-192(%rbp), %rdi
	movq	8(%rbx), %rdx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder21StoreNamedOwnPropertyENS1_8RegisterEPKNS0_12AstRawStringEi@PLT
	movl	328(%r13), %esi
	movq	336(%r13), %rdi
	subl	-160(%rbp), %esi
	jmp	.L4839
	.p2align 4,,10
	.p2align 3
.L5107:
	movb	$1, 8(%r15)
	jmp	.L4938
	.p2align 4,,10
	.p2align 3
.L5104:
	movb	$1, 8(%r15)
	jmp	.L4946
.L5099:
	movl	-160(%rbp), %eax
	movl	%eax, 328(%r13)
	jmp	.L4830
	.p2align 4,,10
	.p2align 3
.L5095:
	movq	8(%rax), %rsi
	movq	%r13, %rdi
	movl	328(%r13), %ebx
	call	_ZN2v88internal11interpreter17BytecodeGenerator21VisitForRegisterValueEPNS0_10ExpressionE
	movl	$22, %esi
	movl	%eax, %r15d
	movq	512(%r13), %rax
	leaq	56(%rax), %rdi
	call	_ZN2v88internal18FeedbackVectorSpec7AddSlotENS0_16FeedbackSlotKindE@PLT
	movl	%r12d, %edx
	movl	%r15d, %esi
	movq	%r14, %rdi
	movl	%eax, %ecx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CloneObjectENS1_8RegisterEii@PLT
	movl	-152(%rbp), %esi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE@PLT
	movq	336(%r13), %rdi
	movl	328(%r13), %esi
	movl	%ebx, 328(%r13)
	testq	%rdi, %rdi
	je	.L4817
	subl	%ebx, %esi
	movq	(%rdi), %rdx
	movq	%rsi, %rax
	movl	%ebx, %esi
	salq	$32, %rax
	orq	%rax, %rsi
	call	*32(%rdx)
.L4817:
	movl	$1, -148(%rbp)
	jmp	.L4818
.L5113:
	movq	-64(%rbp), %rdi
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	subq	%rcx, %rax
	cmpq	$15, %rax
	jbe	.L5114
	leaq	16(%rcx), %rax
	movq	%rax, 16(%rdi)
.L4889:
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%rcx)
	movq	%rcx, 8(%r9)
	movq	-80(%rbp), %rsi
	cmpq	-72(%rbp), %rsi
	je	.L4890
	movq	%rbx, %xmm0
	movq	%rcx, %xmm4
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, (%rsi)
	addq	$16, -80(%rbp)
.L4891:
	movq	8(%r9), %rdx
	jmp	.L4887
.L5111:
	movq	-64(%rbp), %rdi
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	subq	%rcx, %rax
	cmpq	$15, %rax
	jbe	.L5115
	leaq	16(%rcx), %rax
	movq	%rax, 16(%rdi)
.L4869:
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%rcx)
	movq	%rcx, 8(%r9)
	movq	-80(%rbp), %rsi
	cmpq	-72(%rbp), %rsi
	je	.L4870
	movq	%rbx, %xmm0
	movq	%rcx, %xmm3
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, (%rsi)
	addq	$16, -80(%rbp)
.L4871:
	movq	8(%r9), %rdx
	jmp	.L4867
.L5097:
	movl	$192, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L4822
	.p2align 4,,10
	.p2align 3
.L4833:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L5112:
	movq	-144(%rbp), %rdi
	jmp	.L4952
.L5110:
	movq	-144(%rbp), %rdi
	jmp	.L4951
.L4870:
	movq	-88(%rbp), %r10
	movq	%rsi, %r8
	movabsq	$576460752303423487, %rax
	subq	%r10, %r8
	movq	%r8, %r11
	sarq	$4, %r11
	cmpq	%rax, %r11
	je	.L4892
	testq	%r11, %r11
	je	.L4956
	movabsq	$9223372036854775792, %rdi
	leaq	(%r11,%r11), %rax
	cmpq	%rax, %r11
	jbe	.L5116
.L4873:
	movq	%r8, -248(%rbp)
	movq	%r10, -240(%rbp)
	movq	%rcx, -232(%rbp)
	movq	%rsi, -216(%rbp)
	movq	%r9, -208(%rbp)
	movq	%rdi, -200(%rbp)
	call	_Znwm@PLT
	movq	-200(%rbp), %rdi
	movq	-208(%rbp), %r9
	movq	-216(%rbp), %rsi
	movq	-232(%rbp), %rcx
	movq	%rax, %rdx
	movq	-240(%rbp), %r10
	movq	-248(%rbp), %r8
	leaq	(%rax,%rdi), %r11
	leaq	16(%rax), %rax
.L4874:
	movq	%rbx, %xmm0
	movq	%rcx, %xmm5
	punpcklqdq	%xmm5, %xmm0
	movups	%xmm0, (%rdx,%r8)
	cmpq	%r10, %rsi
	je	.L4875
	subq	$16, %rsi
	movq	-184(%rbp), %rdi
	xorl	%eax, %eax
	subq	%r10, %rsi
	movq	%rsi, %rcx
	shrq	$4, %rcx
	addq	$1, %rcx
	.p2align 4,,10
	.p2align 3
.L4876:
	movdqu	(%r10,%rax), %xmm1
	addq	$1, %rdi
	movups	%xmm1, (%rdx,%rax)
	addq	$16, %rax
	cmpq	%rdi, %rcx
	ja	.L4876
	leaq	32(%rdx,%rsi), %rax
.L4877:
	movq	%r10, %rdi
	movq	%r11, -216(%rbp)
	movq	%rax, -208(%rbp)
	movq	%r9, -200(%rbp)
	movq	%rdx, -184(%rbp)
	call	_ZdlPv@PLT
	movq	-216(%rbp), %r11
	movq	-208(%rbp), %rax
	movq	-200(%rbp), %r9
	movq	-184(%rbp), %rdx
.L4878:
	movq	%rdx, %xmm0
	movq	%rax, %xmm6
	movq	%r11, -72(%rbp)
	punpcklqdq	%xmm6, %xmm0
	movups	%xmm0, -88(%rbp)
	jmp	.L4871
.L4890:
	movq	-88(%rbp), %r10
	movq	%rsi, %r8
	movabsq	$576460752303423487, %rax
	subq	%r10, %r8
	movq	%r8, %r11
	sarq	$4, %r11
	cmpq	%rax, %r11
	je	.L4892
	testq	%r11, %r11
	je	.L4960
	movabsq	$9223372036854775792, %rdi
	leaq	(%r11,%r11), %rax
	cmpq	%rax, %r11
	jbe	.L5117
.L4893:
	movq	%r8, -248(%rbp)
	movq	%r10, -240(%rbp)
	movq	%rcx, -232(%rbp)
	movq	%rsi, -216(%rbp)
	movq	%r9, -208(%rbp)
	movq	%rdi, -200(%rbp)
	call	_Znwm@PLT
	movq	-200(%rbp), %rdi
	movq	-208(%rbp), %r9
	movq	-216(%rbp), %rsi
	movq	-232(%rbp), %rcx
	movq	%rax, %rdx
	movq	-240(%rbp), %r10
	movq	-248(%rbp), %r8
	leaq	(%rax,%rdi), %r11
	leaq	16(%rax), %rax
.L4894:
	movq	%rbx, %xmm0
	movq	%rcx, %xmm7
	punpcklqdq	%xmm7, %xmm0
	movups	%xmm0, (%rdx,%r8)
	cmpq	%r10, %rsi
	je	.L4895
	subq	$16, %rsi
	movq	-184(%rbp), %rdi
	xorl	%eax, %eax
	subq	%r10, %rsi
	movq	%rsi, %rcx
	shrq	$4, %rcx
	addq	$1, %rcx
	.p2align 4,,10
	.p2align 3
.L4896:
	movdqu	(%r10,%rax), %xmm2
	addq	$1, %rdi
	movups	%xmm2, (%rdx,%rax)
	addq	$16, %rax
	cmpq	%rcx, %rdi
	jb	.L4896
	leaq	32(%rdx,%rsi), %rax
.L4897:
	movq	%r10, %rdi
	movq	%r11, -216(%rbp)
	movq	%rdx, -208(%rbp)
	movq	%rax, -200(%rbp)
	movq	%r9, -184(%rbp)
	call	_ZdlPv@PLT
	movq	-216(%rbp), %r11
	movq	-208(%rbp), %rdx
	movq	-200(%rbp), %rax
	movq	-184(%rbp), %r9
.L4898:
	movq	%rdx, %xmm0
	movq	%rax, %xmm5
	movq	%r11, -72(%rbp)
	punpcklqdq	%xmm5, %xmm0
	movups	%xmm0, -88(%rbp)
	jmp	.L4891
.L5114:
	movl	$16, %esi
	movq	%rdx, -208(%rbp)
	movq	%r9, -200(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-200(%rbp), %r9
	movq	-208(%rbp), %rdx
	movq	%rax, %rcx
	jmp	.L4889
.L5115:
	movl	$16, %esi
	movq	%r9, -208(%rbp)
	movq	%rdx, -200(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-200(%rbp), %rdx
	movq	-208(%rbp), %r9
	movq	%rax, %rcx
	jmp	.L4869
.L4895:
	testq	%r10, %r10
	je	.L4898
	jmp	.L4897
	.p2align 4,,10
	.p2align 3
.L4875:
	testq	%r10, %r10
	je	.L4878
	jmp	.L4877
	.p2align 4,,10
	.p2align 3
.L5117:
	testq	%rax, %rax
	jne	.L5118
	xorl	%r11d, %r11d
	movl	$16, %eax
	jmp	.L4894
.L5116:
	testq	%rax, %rax
	jne	.L5119
	xorl	%r11d, %r11d
	movl	$16, %eax
	jmp	.L4874
.L4960:
	movl	$16, %edi
	jmp	.L4893
.L4956:
	movl	$16, %edi
	jmp	.L4873
.L5118:
	movabsq	$576460752303423487, %rdx
	cmpq	%rdx, %rax
	cmova	%rdx, %rax
	salq	$4, %rax
	movq	%rax, %rdi
	jmp	.L4893
.L5098:
	leaq	.LC10(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L5108:
	call	__stack_chk_fail@PLT
.L4892:
	leaq	.LC3(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L5119:
	movabsq	$576460752303423487, %rdx
	cmpq	%rdx, %rax
	cmova	%rdx, %rax
	salq	$4, %rax
	movq	%rax, %rdi
	jmp	.L4873
	.cfi_endproc
.LFE20850:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator18VisitObjectLiteralEPNS0_13ObjectLiteralE, .-_ZN2v88internal11interpreter17BytecodeGenerator18VisitObjectLiteralEPNS0_13ObjectLiteralE
	.section	.text._ZN2v88internal11interpreter17BytecodeGenerator17VisitClassLiteralEPNS0_12ClassLiteralE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17BytecodeGenerator17VisitClassLiteralEPNS0_12ClassLiteralE
	.type	_ZN2v88internal11interpreter17BytecodeGenerator17VisitClassLiteralEPNS0_12ClassLiteralE, @function
_ZN2v88internal11interpreter17BytecodeGenerator17VisitClassLiteralEPNS0_12ClassLiteralE:
.LFB20825:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	16(%rsi), %rsi
	movq	536(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L5121
	movq	%rsi, 536(%rdi)
	movq	16(%r12), %rsi
.L5121:
	movl	124(%rsi), %eax
	testl	%eax, %eax
	jle	.L5122
	movq	816(%rbx), %rax
	leaq	-96(%rbp), %r14
	leaq	24(%rbx), %rdi
	movq	%rbx, -88(%rbp)
	movq	%r14, 816(%rbx)
	movq	%rax, -96(%rbp)
	movl	328(%rbx), %eax
	movq	$2, -72(%rbp)
	movl	%eax, -80(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder18CreateBlockContextEPKNS0_5ScopeE@PLT
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rax
	movq	336(%rdx), %rdi
	movq	%rax, 816(%rdx)
	movl	-80(%rbp), %eax
	movl	328(%rdx), %esi
	movl	%eax, 328(%rdx)
	testq	%rdi, %rdi
	je	.L5123
	subl	%eax, %esi
	movq	(%rdi), %rcx
	movq	%rsi, %rdx
	movl	%eax, %esi
	salq	$32, %rdx
	orq	%rdx, %rsi
	call	*32(%rcx)
.L5123:
	movq	16(%r12), %rax
	movq	%rbx, -96(%rbp)
	movq	%rax, -88(%rbp)
	movq	808(%rbx), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal11interpreter8Register15current_contextEv@PLT
	movl	$0, -68(%rbp)
	movl	%eax, -72(%rbp)
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L5124
	movl	28(%rax), %ecx
	movq	-96(%rbp), %rdi
	leal	1(%rcx), %edx
	movl	%edx, -68(%rbp)
	movl	328(%rdi), %r15d
	movq	336(%rdi), %r8
	leal	1(%r15), %edx
	cmpl	%edx, 332(%rdi)
	movl	%edx, 328(%rdi)
	cmovge	332(%rdi), %edx
	movl	%edx, 332(%rdi)
	testq	%r8, %r8
	je	.L5125
	movq	(%r8), %rax
	movq	%r8, %rdi
	movl	%r15d, %esi
	call	*16(%rax)
	movq	-80(%rbp), %rax
	movq	-96(%rbp), %rdi
.L5125:
	movl	%r15d, 24(%rax)
	addq	$24, %rdi
	movl	%r15d, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11PushContextENS1_8RegisterE@PLT
.L5124:
	movq	-96(%rbp), %rax
	movl	$2147483647, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%r14, 808(%rax)
	call	_ZN2v88internal11interpreter17BytecodeGenerator17BuildClassLiteralEPNS0_12ClassLiteralENS1_8RegisterE
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L5126
	movq	-96(%rbp), %rcx
	movl	24(%rax), %esi
	leaq	24(%rcx), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder10PopContextENS1_8RegisterE@PLT
	movq	-80(%rbp), %rax
	movl	-72(%rbp), %edx
	movl	%edx, 24(%rax)
.L5126:
	movq	-96(%rbp), %rdx
	movq	%rax, 808(%rdx)
.L5127:
	cmpq	536(%rbx), %r13
	je	.L5120
	movq	%r13, 536(%rbx)
.L5120:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5146
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5122:
	.cfi_restore_state
	movl	$2147483647, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter17BytecodeGenerator17BuildClassLiteralEPNS0_12ClassLiteralENS1_8RegisterE
	jmp	.L5127
.L5146:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20825:
	.size	_ZN2v88internal11interpreter17BytecodeGenerator17VisitClassLiteralEPNS0_12ClassLiteralE, .-_ZN2v88internal11interpreter17BytecodeGenerator17VisitClassLiteralEPNS0_12ClassLiteralE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal11interpreter17BytecodeGenerator12ControlScope14PerformCommandENS3_7CommandEPNS0_9StatementEi,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal11interpreter17BytecodeGenerator12ControlScope14PerformCommandENS3_7CommandEPNS0_9StatementEi, @function
_GLOBAL__sub_I__ZN2v88internal11interpreter17BytecodeGenerator12ControlScope14PerformCommandENS3_7CommandEPNS0_9StatementEi:
.LFB28115:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE28115:
	.size	_GLOBAL__sub_I__ZN2v88internal11interpreter17BytecodeGenerator12ControlScope14PerformCommandENS3_7CommandEPNS0_9StatementEi, .-_GLOBAL__sub_I__ZN2v88internal11interpreter17BytecodeGenerator12ControlScope14PerformCommandENS3_7CommandEPNS0_9StatementEi
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal11interpreter17BytecodeGenerator12ControlScope14PerformCommandENS3_7CommandEPNS0_9StatementEi
	.weak	_ZTVN2v88internal11interpreter12BlockBuilderE
	.section	.data.rel.ro.local._ZTVN2v88internal11interpreter12BlockBuilderE,"awG",@progbits,_ZTVN2v88internal11interpreter12BlockBuilderE,comdat
	.align 8
	.type	_ZTVN2v88internal11interpreter12BlockBuilderE, @object
	.size	_ZTVN2v88internal11interpreter12BlockBuilderE, 32
_ZTVN2v88internal11interpreter12BlockBuilderE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal11interpreter12BlockBuilderD1Ev
	.quad	_ZN2v88internal11interpreter12BlockBuilderD0Ev
	.weak	_ZTVN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTopLevelE
	.section	.data.rel.ro.local._ZTVN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTopLevelE,"awG",@progbits,_ZTVN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTopLevelE,comdat
	.align 8
	.type	_ZTVN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTopLevelE, @object
	.size	_ZTVN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTopLevelE, 40
_ZTVN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTopLevelE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTopLevelD1Ev
	.quad	_ZN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTopLevelD0Ev
	.quad	_ZN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTopLevel7ExecuteENS2_12ControlScope7CommandEPNS0_9StatementEi
	.weak	_ZTVN2v88internal11interpreter17BytecodeGenerator24ControlScopeForBreakableE
	.section	.data.rel.ro.local._ZTVN2v88internal11interpreter17BytecodeGenerator24ControlScopeForBreakableE,"awG",@progbits,_ZTVN2v88internal11interpreter17BytecodeGenerator24ControlScopeForBreakableE,comdat
	.align 8
	.type	_ZTVN2v88internal11interpreter17BytecodeGenerator24ControlScopeForBreakableE, @object
	.size	_ZTVN2v88internal11interpreter17BytecodeGenerator24ControlScopeForBreakableE, 40
_ZTVN2v88internal11interpreter17BytecodeGenerator24ControlScopeForBreakableE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal11interpreter17BytecodeGenerator24ControlScopeForBreakableD1Ev
	.quad	_ZN2v88internal11interpreter17BytecodeGenerator24ControlScopeForBreakableD0Ev
	.quad	_ZN2v88internal11interpreter17BytecodeGenerator24ControlScopeForBreakable7ExecuteENS2_12ControlScope7CommandEPNS0_9StatementEi
	.weak	_ZTVN2v88internal11interpreter17BytecodeGenerator24ControlScopeForIterationE
	.section	.data.rel.ro.local._ZTVN2v88internal11interpreter17BytecodeGenerator24ControlScopeForIterationE,"awG",@progbits,_ZTVN2v88internal11interpreter17BytecodeGenerator24ControlScopeForIterationE,comdat
	.align 8
	.type	_ZTVN2v88internal11interpreter17BytecodeGenerator24ControlScopeForIterationE, @object
	.size	_ZTVN2v88internal11interpreter17BytecodeGenerator24ControlScopeForIterationE, 40
_ZTVN2v88internal11interpreter17BytecodeGenerator24ControlScopeForIterationE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal11interpreter17BytecodeGenerator24ControlScopeForIterationD1Ev
	.quad	_ZN2v88internal11interpreter17BytecodeGenerator24ControlScopeForIterationD0Ev
	.quad	_ZN2v88internal11interpreter17BytecodeGenerator24ControlScopeForIteration7ExecuteENS2_12ControlScope7CommandEPNS0_9StatementEi
	.weak	_ZTVN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTryCatchE
	.section	.data.rel.ro.local._ZTVN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTryCatchE,"awG",@progbits,_ZTVN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTryCatchE,comdat
	.align 8
	.type	_ZTVN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTryCatchE, @object
	.size	_ZTVN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTryCatchE, 40
_ZTVN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTryCatchE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTryCatchD1Ev
	.quad	_ZN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTryCatchD0Ev
	.quad	_ZN2v88internal11interpreter17BytecodeGenerator23ControlScopeForTryCatch7ExecuteENS2_12ControlScope7CommandEPNS0_9StatementEi
	.weak	_ZTVN2v88internal11interpreter17BytecodeGenerator25ControlScopeForTryFinallyE
	.section	.data.rel.ro.local._ZTVN2v88internal11interpreter17BytecodeGenerator25ControlScopeForTryFinallyE,"awG",@progbits,_ZTVN2v88internal11interpreter17BytecodeGenerator25ControlScopeForTryFinallyE,comdat
	.align 8
	.type	_ZTVN2v88internal11interpreter17BytecodeGenerator25ControlScopeForTryFinallyE, @object
	.size	_ZTVN2v88internal11interpreter17BytecodeGenerator25ControlScopeForTryFinallyE, 40
_ZTVN2v88internal11interpreter17BytecodeGenerator25ControlScopeForTryFinallyE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal11interpreter17BytecodeGenerator25ControlScopeForTryFinallyD1Ev
	.quad	_ZN2v88internal11interpreter17BytecodeGenerator25ControlScopeForTryFinallyD0Ev
	.quad	_ZN2v88internal11interpreter17BytecodeGenerator25ControlScopeForTryFinally7ExecuteENS2_12ControlScope7CommandEPNS0_9StatementEi
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC4:
	.long	2147483647
	.long	0
	.long	2147483647
	.long	2147483647
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
