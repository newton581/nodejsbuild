	.file	"js-array-buffer.cc"
	.text
	.section	.rodata._ZN2v88internal13JSArrayBuffer6DetachEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"is_detachable()"
.LC1:
	.string	"Check failed: %s."
.LC2:
	.string	"!was_detached()"
.LC3:
	.string	"is_external()"
	.section	.text._ZN2v88internal13JSArrayBuffer6DetachEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13JSArrayBuffer6DetachEv
	.type	_ZN2v88internal13JSArrayBuffer6DetachEv, @function
_ZN2v88internal13JSArrayBuffer6DetachEv:
.LFB18358:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rax
	movl	39(%rax), %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	andl	$2, %edx
	je	.L8
	movl	39(%rax), %edx
	andl	$4, %edx
	jne	.L9
	movl	39(%rax), %edx
	andl	$1, %edx
	je	.L10
	movq	$0, 31(%rax)
	movq	(%rdi), %rax
	movq	$0, 23(%rax)
	movq	(%rdi), %rdx
	movl	39(%rdx), %eax
	orl	$4, %eax
	movl	%eax, 39(%rdx)
	movq	(%rdi), %rdx
	movl	39(%rdx), %eax
	andl	$-3, %eax
	movl	%eax, 39(%rdx)
	movq	(%rdi), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	movabsq	$4294967296, %rax
	movq	-33032(%rdi), %rdx
	subq	$37592, %rdi
	cmpq	%rax, 23(%rdx)
	je	.L11
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal7Isolate39InvalidateArrayBufferDetachingProtectorEv@PLT
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L9:
	leaq	.LC2(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L10:
	leaq	.LC3(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18358:
	.size	_ZN2v88internal13JSArrayBuffer6DetachEv, .-_ZN2v88internal13JSArrayBuffer6DetachEv
	.section	.text._ZN2v88internal13JSArrayBuffer30FreeBackingStoreFromMainThreadEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13JSArrayBuffer30FreeBackingStoreFromMainThreadEv
	.type	_ZN2v88internal13JSArrayBuffer30FreeBackingStoreFromMainThreadEv, @function
_ZN2v88internal13JSArrayBuffer30FreeBackingStoreFromMainThreadEv:
.LFB18359:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	cmpq	$0, 31(%rax)
	je	.L32
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	39(%rax), %edx
	movq	%rdi, %rbx
	andl	$16, %edx
	jne	.L35
	movq	31(%rax), %rax
.L16:
	testq	%rax, %rax
	je	.L12
	movq	(%rbx), %rax
	movq	31(%rax), %rdx
	leaq	39(%rax), %rcx
	testq	%rdx, %rdx
	je	.L24
	movl	39(%rax), %ecx
	movq	%rdx, %r12
	andl	$16, %ecx
	jne	.L36
.L20:
	leaq	39(%rax), %rcx
	xorl	%r8d, %r8d
	testq	%rdx, %rdx
	je	.L18
	movl	39(%rax), %edx
	andl	$16, %edx
	jne	.L37
	movq	23(%rax), %rdx
	movq	31(%rax), %r8
.L18:
	movl	(%rcx), %ecx
	movq	(%rbx), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rsi
	subq	$37592, %rsi
	andl	$16, %ecx
	jne	.L38
	movq	45544(%rsi), %rdi
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
.L23:
	movq	(%rbx), %rax
	movq	$0, 31(%rax)
.L12:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	xorl	%r8d, %r8d
	xorl	%r12d, %r12d
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movq	45752(%rsi), %rdi
	movq	%r8, %rdx
	call	_ZN2v88internal4wasm17WasmMemoryTracker14FreeWasmMemoryEPNS0_7IsolateEPKv@PLT
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L35:
	movq	31(%rax), %rsi
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	8160(%rax), %rdi
	call	_ZN2v88internal4wasm17WasmMemoryTracker18FindAllocationDataEPKv@PLT
	movq	(%rax), %rax
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L36:
	andq	$-262144, %rax
	movq	%rdx, %rsi
	movq	24(%rax), %rax
	movq	8160(%rax), %rdi
	call	_ZN2v88internal4wasm17WasmMemoryTracker18FindAllocationDataEPKv@PLT
	movq	(%rax), %r12
	movq	(%rbx), %rax
	movq	31(%rax), %rdx
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L37:
	movq	(%rbx), %rax
	movq	31(%rax), %rsi
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	8160(%rax), %rdi
	call	_ZN2v88internal4wasm17WasmMemoryTracker18FindAllocationDataEPKv@PLT
	movq	(%rbx), %rcx
	movq	8(%rax), %rdx
	movq	31(%rcx), %r8
	addq	$39, %rcx
	jmp	.L18
	.cfi_endproc
.LFE18359:
	.size	_ZN2v88internal13JSArrayBuffer30FreeBackingStoreFromMainThreadEv, .-_ZN2v88internal13JSArrayBuffer30FreeBackingStoreFromMainThreadEv
	.section	.text._ZN2v88internal13JSArrayBuffer16FreeBackingStoreEPNS0_7IsolateENS1_10AllocationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13JSArrayBuffer16FreeBackingStoreEPNS0_7IsolateENS1_10AllocationE
	.type	_ZN2v88internal13JSArrayBuffer16FreeBackingStoreEPNS0_7IsolateENS1_10AllocationE, @function
_ZN2v88internal13JSArrayBuffer16FreeBackingStoreEPNS0_7IsolateENS1_10AllocationE:
.LFB18360:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpb	$0, 40(%rbp)
	jne	.L42
	movq	45544(%rdi), %rdi
	movq	24(%rbp), %rdx
	movq	16(%rbp), %rsi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L42:
	.cfi_restore_state
	movq	32(%rbp), %rdx
	movq	45752(%rdi), %rdi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal4wasm17WasmMemoryTracker14FreeWasmMemoryEPNS0_7IsolateEPKv@PLT
	.cfi_endproc
.LFE18360:
	.size	_ZN2v88internal13JSArrayBuffer16FreeBackingStoreEPNS0_7IsolateENS1_10AllocationE, .-_ZN2v88internal13JSArrayBuffer16FreeBackingStoreEPNS0_7IsolateENS1_10AllocationE
	.section	.text._ZN2v88internal13JSArrayBuffer5SetupENS0_6HandleIS1_EEPNS0_7IsolateEbPvmNS0_10SharedFlagEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13JSArrayBuffer5SetupENS0_6HandleIS1_EEPNS0_7IsolateEbPvmNS0_10SharedFlagEb
	.type	_ZN2v88internal13JSArrayBuffer5SetupENS0_6HandleIS1_EEPNS0_7IsolateEbPvmNS0_10SharedFlagEb, @function
_ZN2v88internal13JSArrayBuffer5SetupENS0_6HandleIS1_EEPNS0_7IsolateEbPvmNS0_10SharedFlagEb:
.LFB18361:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	16(%rbp), %eax
	movq	%rsi, -96(%rbp)
	movl	%edx, -60(%rbp)
	movq	%r8, -72(%rbp)
	movl	%r9d, -64(%rbp)
	movl	%eax, -76(%rbp)
	movq	%rcx, -88(%rbp)
.L48:
	movq	(%r14), %rbx
	leaq	_ZN2v88internal3Smi5kZeroE(%rip), %rax
	movq	(%rax), %r15
	movl	$24, %eax
	movq	-1(%rbx), %rcx
	leaq	-1(%rbx), %rdx
	movzwl	11(%rcx), %edi
	cmpw	$1057, %di
	je	.L44
	movq	%rdx, -56(%rbp)
	movsbl	13(%rcx), %esi
	shrl	$31, %esi
	call	_ZN2v88internal8JSObject13GetHeaderSizeENS0_12InstanceTypeEb@PLT
	movq	-56(%rbp), %rdx
.L44:
	leal	0(%r13,%rax), %r12d
	movslq	%r12d, %r12
	addq	%rdx, %r12
	movq	%r15, (%r12)
	testb	$1, %r15b
	je	.L50
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	je	.L46
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	8(%rcx), %rax
.L46:
	testb	$24, %al
	je	.L50
	movq	%rbx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L64
	.p2align 4,,10
	.p2align 3
.L50:
	addl	$8, %r13d
	cmpl	$16, %r13d
	jne	.L48
	movq	(%r14), %rax
	movq	-72(%rbp), %rsi
	movq	-88(%rbp), %rbx
	movq	%rsi, 23(%rax)
	movq	(%r14), %rax
	movl	$0, 39(%rax)
	movq	(%r14), %rax
	movl	$0, 43(%rax)
	movq	(%r14), %rdx
	movl	39(%rdx), %eax
	movl	-60(%rbp), %r10d
	andl	$-2, %eax
	movzbl	%r10b, %ecx
	orl	%ecx, %eax
	movl	%eax, 39(%rdx)
	xorl	%edx, %edx
	movq	(%r14), %rsi
	movl	39(%rsi), %ecx
	movl	-64(%rbp), %edi
	testl	%edi, %edi
	sete	%dl
	leal	(%rdx,%rdx), %eax
	movl	%ecx, %edx
	andl	$-3, %edx
	orl	%edx, %eax
	xorl	%edx, %edx
	cmpl	$1, %edi
	movl	%eax, 39(%rsi)
	sete	%dl
	leal	0(,%rdx,8), %eax
	movq	(%r14), %rsi
	movl	39(%rsi), %ecx
	andl	$-9, %ecx
	orl	%ecx, %eax
	movl	%eax, 39(%rsi)
	movq	(%r14), %rcx
	movl	39(%rcx), %eax
	movzbl	-76(%rbp), %edx
	andl	$-17, %eax
	sall	$4, %edx
	orl	%edx, %eax
	movl	%eax, 39(%rcx)
	movq	(%r14), %rax
	movq	%rbx, 31(%rax)
	testq	%rbx, %rbx
	je	.L43
	cmpb	$1, %r10b
	jne	.L65
.L43:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_restore_state
	movq	-96(%rbp), %rdi
	movq	(%r14), %rsi
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	addq	$37592, %rdi
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal4Heap22RegisterNewArrayBufferENS0_13JSArrayBufferE@PLT
	.p2align 4,,10
	.p2align 3
.L64:
	.cfi_restore_state
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L50
	.cfi_endproc
.LFE18361:
	.size	_ZN2v88internal13JSArrayBuffer5SetupENS0_6HandleIS1_EEPNS0_7IsolateEbPvmNS0_10SharedFlagEb, .-_ZN2v88internal13JSArrayBuffer5SetupENS0_6HandleIS1_EEPNS0_7IsolateEbPvmNS0_10SharedFlagEb
	.section	.text._ZN2v88internal13JSArrayBuffer12SetupAsEmptyENS0_6HandleIS1_EEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13JSArrayBuffer12SetupAsEmptyENS0_6HandleIS1_EEPNS0_7IsolateE
	.type	_ZN2v88internal13JSArrayBuffer12SetupAsEmptyENS0_6HandleIS1_EEPNS0_7IsolateE, @function
_ZN2v88internal13JSArrayBuffer12SetupAsEmptyENS0_6HandleIS1_EEPNS0_7IsolateE:
.LFB18362:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	$0
	call	_ZN2v88internal13JSArrayBuffer5SetupENS0_6HandleIS1_EEPNS0_7IsolateEbPvmNS0_10SharedFlagEb
	popq	%rax
	popq	%rdx
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18362:
	.size	_ZN2v88internal13JSArrayBuffer12SetupAsEmptyENS0_6HandleIS1_EEPNS0_7IsolateE, .-_ZN2v88internal13JSArrayBuffer12SetupAsEmptyENS0_6HandleIS1_EEPNS0_7IsolateE
	.section	.rodata._ZN2v88internal13JSArrayBuffer19SetupAllocatingDataENS0_6HandleIS1_EEPNS0_7IsolateEmbNS0_10SharedFlagE.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"(isolate->array_buffer_allocator()) != nullptr"
	.section	.text._ZN2v88internal13JSArrayBuffer19SetupAllocatingDataENS0_6HandleIS1_EEPNS0_7IsolateEmbNS0_10SharedFlagE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13JSArrayBuffer19SetupAllocatingDataENS0_6HandleIS1_EEPNS0_7IsolateEmbNS0_10SharedFlagE
	.type	_ZN2v88internal13JSArrayBuffer19SetupAllocatingDataENS0_6HandleIS1_EEPNS0_7IsolateEmbNS0_10SharedFlagE, @function
_ZN2v88internal13JSArrayBuffer19SetupAllocatingDataENS0_6HandleIS1_EEPNS0_7IsolateEmbNS0_10SharedFlagE:
.LFB18363:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpq	$0, 45544(%rsi)
	je	.L78
	movq	%rdi, %r14
	movq	%rsi, %r12
	movq	%rdx, %rbx
	movl	%r8d, %r13d
	testq	%rdx, %rdx
	je	.L76
	movl	%ecx, %r15d
	cmpq	$1048575, %rdx
	ja	.L79
	cmpl	$1, %r13d
	je	.L80
.L72:
	movq	45544(%r12), %rdi
	movq	%rbx, %rsi
	movq	(%rdi), %rax
	testb	%r15b, %r15b
	jne	.L81
	call	*24(%rax)
	movq	%rax, %rcx
.L74:
	testq	%rcx, %rcx
	je	.L82
.L70:
	subq	$8, %rsp
	xorl	%edx, %edx
	movl	%r13d, %r9d
	movq	%rbx, %r8
	pushq	$0
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal13JSArrayBuffer5SetupENS0_6HandleIS1_EEPNS0_7IsolateEbPvmNS0_10SharedFlagEb
	popq	%rax
	movl	$1, %eax
	popq	%rdx
.L68:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	.cfi_restore_state
	call	*16(%rax)
	movq	%rax, %rcx
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L76:
	xorl	%ecx, %ecx
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L79:
	movq	40960(%r12), %rax
	movq	%rdx, %rsi
	shrq	$20, %rsi
	leaq	1048(%rax), %rdi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	cmpl	$1, %r13d
	jne	.L72
.L80:
	movq	40960(%r12), %rax
	movq	%rbx, %rsi
	shrq	$20, %rsi
	leaq	1128(%rax), %rdi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L78:
	leaq	.LC4(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L82:
	movq	40960(%r12), %rax
	shrq	$20, %rbx
	movq	%rbx, %rsi
	leaq	1088(%rax), %rdi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	pushq	$0
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal13JSArrayBuffer5SetupENS0_6HandleIS1_EEPNS0_7IsolateEbPvmNS0_10SharedFlagEb
	popq	%rcx
	xorl	%eax, %eax
	popq	%rsi
	jmp	.L68
	.cfi_endproc
.LFE18363:
	.size	_ZN2v88internal13JSArrayBuffer19SetupAllocatingDataENS0_6HandleIS1_EEPNS0_7IsolateEmbNS0_10SharedFlagE, .-_ZN2v88internal13JSArrayBuffer19SetupAllocatingDataENS0_6HandleIS1_EEPNS0_7IsolateEmbNS0_10SharedFlagE
	.section	.rodata._ZN2v88internal12JSTypedArray22MaterializeArrayBufferENS0_6HandleIS1_EE.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"JSTypedArray::MaterializeArrayBuffer"
	.section	.text._ZN2v88internal12JSTypedArray22MaterializeArrayBufferENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12JSTypedArray22MaterializeArrayBufferENS0_6HandleIS1_EE
	.type	_ZN2v88internal12JSTypedArray22MaterializeArrayBufferENS0_6HandleIS1_EE, @function
_ZN2v88internal12JSTypedArray22MaterializeArrayBufferENS0_6HandleIS1_EE:
.LFB18364:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %rax
	movq	%rax, %rdx
	movq	23(%rax), %rsi
	andq	$-262144, %rdx
	movq	24(%rdx), %r13
	movq	3520(%r13), %rdi
	subq	$37592, %r13
	testq	%rdi, %rdi
	je	.L84
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r12
.L85:
	movq	45544(%r13), %rdi
	movq	(%rbx), %rax
	movq	39(%rax), %rsi
	movq	(%rdi), %rax
	call	*24(%rax)
	leaq	37592(%r13), %rdi
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L109
.L87:
	movq	(%r12), %rdx
	movl	39(%rdx), %eax
	andl	$-2, %eax
	movl	%eax, 39(%rdx)
	movq	(%r12), %rax
	movq	%r14, 31(%rax)
	movq	(%r12), %rsi
	call	_ZN2v88internal4Heap22RegisterNewArrayBufferENS0_13JSArrayBufferE@PLT
	movq	(%rbx), %rax
	movq	(%r12), %rdx
	movq	31(%rdx), %rdi
	movq	39(%rax), %rdx
	movq	55(%rax), %rsi
	addq	63(%rax), %rsi
	call	memcpy@PLT
	movq	(%rbx), %r15
	movq	976(%r13), %r13
	movq	%r13, 15(%r15)
	leaq	15(%r15), %rsi
	testb	$1, %r13b
	je	.L95
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	jne	.L110
	testb	$24, %al
	je	.L95
.L115:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L111
	.p2align 4,,10
	.p2align 3
.L95:
	movq	(%rbx), %rax
	movq	%r14, 55(%rax)
	movq	(%rbx), %r14
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %r13
	leaq	63(%r14), %r15
	movq	%r13, 63(%r14)
	testb	$1, %r13b
	je	.L94
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L112
	testb	$24, %al
	je	.L94
.L114:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L113
.L94:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L114
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L110:
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L115
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L84:
	movq	41088(%r13), %r12
	cmpq	%r12, 41096(%r13)
	je	.L116
.L86:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r12)
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L111:
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L113:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L109:
	leaq	.LC5(%rip), %rsi
	movq	%rdi, -56(%rbp)
	call	_ZN2v88internal4Heap23FatalProcessOutOfMemoryEPKc@PLT
	movq	-56(%rbp), %rdi
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L116:
	movq	%r13, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L86
	.cfi_endproc
.LFE18364:
	.size	_ZN2v88internal12JSTypedArray22MaterializeArrayBufferENS0_6HandleIS1_EE, .-_ZN2v88internal12JSTypedArray22MaterializeArrayBufferENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal12JSTypedArray9GetBufferEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12JSTypedArray9GetBufferEv
	.type	_ZN2v88internal12JSTypedArray9GetBufferEv, @function
_ZN2v88internal12JSTypedArray9GetBufferEv:
.LFB18365:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rsi
	movq	%rsi, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rbx
	movq	15(%rsi), %rax
	subq	$37592, %rbx
	movq	41112(%rbx), %rdi
	cmpq	%rax, 63(%rsi)
	je	.L118
	movq	23(%rsi), %rsi
	testq	%rdi, %rdi
	je	.L119
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L119:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L128
.L121:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L118:
	.cfi_restore_state
	testq	%rdi, %rdi
	je	.L123
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$24, %rsp
	popq	%rbx
	movq	%rax, %rdi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12JSTypedArray22MaterializeArrayBufferENS0_6HandleIS1_EE
	.p2align 4,,10
	.p2align 3
.L123:
	.cfi_restore_state
	movq	41088(%rbx), %rdi
	cmpq	41096(%rbx), %rdi
	je	.L129
.L125:
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12JSTypedArray22MaterializeArrayBufferENS0_6HandleIS1_EE
	.p2align 4,,10
	.p2align 3
.L128:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L129:
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	movq	%rax, %rdi
	jmp	.L125
	.cfi_endproc
.LFE18365:
	.size	_ZN2v88internal12JSTypedArray9GetBufferEv, .-_ZN2v88internal12JSTypedArray9GetBufferEv
	.section	.rodata._ZN2v88internal12JSTypedArray17DefineOwnPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEEPNS0_18PropertyDescriptorENS_5MaybeINS0_11ShouldThrowEEE.str1.1,"aMS",@progbits,1
.LC6:
	.string	"(location_) != nullptr"
	.section	.text._ZN2v88internal12JSTypedArray17DefineOwnPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEEPNS0_18PropertyDescriptorENS_5MaybeINS0_11ShouldThrowEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12JSTypedArray17DefineOwnPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEEPNS0_18PropertyDescriptorENS_5MaybeINS0_11ShouldThrowEEE
	.type	_ZN2v88internal12JSTypedArray17DefineOwnPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEEPNS0_18PropertyDescriptorENS_5MaybeINS0_11ShouldThrowEEE, @function
_ZN2v88internal12JSTypedArray17DefineOwnPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEEPNS0_18PropertyDescriptorENS_5MaybeINS0_11ShouldThrowEEE:
.LFB18366:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	(%rdx), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	$1, %sil
	jne	.L217
.L178:
	sarq	$32, %rsi
	js	.L150
.L148:
	movq	(%r15), %rax
	movq	47(%rax), %rdx
	movq	23(%rax), %rax
	movl	39(%rax), %eax
	testb	$4, %al
	jne	.L150
	movl	%esi, %eax
	cmpq	%rdx, %rax
	jnb	.L150
	cmpq	$0, 16(%rcx)
	je	.L218
.L164:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal14GetShouldThrowEPNS0_7IsolateENS_5MaybeINS0_11ShouldThrowEEE@PLT
	cmpl	$1, %eax
	je	.L158
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movl	$151, %esi
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L217:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	ja	.L135
	movq	%rdx, %rsi
	movq	%rcx, -72(%rbp)
	call	_ZN2v88internal6String8ToNumberEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	-72(%rbp), %rcx
	movq	%rax, %rbx
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L219
.L137:
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rcx, -72(%rbp)
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	-72(%rbp), %rcx
	testq	%rax, %rax
	je	.L220
.L144:
	movq	(%rax), %rax
	movq	(%r12), %rsi
	leaq	-64(%rbp), %rdi
	movq	%rcx, -72(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6Object9SameValueES1_@PLT
	movq	-72(%rbp), %rcx
	testb	%al, %al
	je	.L135
	movq	(%rbx), %rax
.L141:
	movq	%rax, %rsi
	testb	$1, %al
	je	.L178
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	je	.L221
.L145:
	movq	%rax, %rsi
	testb	$1, %al
	je	.L178
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	je	.L222
.L150:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal14GetShouldThrowEPNS0_7IsolateENS_5MaybeINS0_11ShouldThrowEEE@PLT
	cmpl	$1, %eax
	jne	.L210
.L158:
	movl	$1, %eax
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L135:
	movq	%r14, %r8
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal10JSReceiver25OrdinaryDefineOwnPropertyEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6ObjectEEEPNS0_18PropertyDescriptorENS_5MaybeINS0_11ShouldThrowEEE@PLT
.L177:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L223
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L210:
	.cfi_restore_state
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$205, %esi
.L214:
	movq	%r13, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	xorl	%eax, %eax
.L215:
	movb	$0, %ah
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L219:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	je	.L224
.L139:
	testb	$1, %al
	je	.L137
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L137
	movq	%rbx, %rax
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L222:
	movsd	7(%rax), %xmm1
	movsd	.LC7(%rip), %xmm2
	addsd	%xmm1, %xmm2
	movq	%xmm2, %rdx
	movq	%xmm2, %rax
	shrq	$32, %rdx
	cmpq	$1127219200, %rdx
	jne	.L150
	movl	%eax, %eax
	pxor	%xmm0, %xmm0
	movd	%xmm2, %esi
	cvtsi2sdq	%rax, %xmm0
	ucomisd	%xmm0, %xmm1
	jp	.L150
	je	.L148
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L224:
	movabsq	$-9223372036854775808, %rdx
	cmpq	%rdx, 7(%rax)
	je	.L141
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L221:
	movabsq	$-9223372036854775808, %rdx
	cmpq	%rdx, 7(%rax)
	je	.L150
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L220:
	leaq	.LC6(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L218:
	cmpq	$0, 24(%rcx)
	jne	.L164
	movzbl	(%rcx), %eax
	movl	%eax, %edx
	shrb	$3, %dl
	andl	$1, %edx
	je	.L163
	testb	$4, %al
	jne	.L164
.L163:
	movl	%eax, %r9d
	shrb	%r9b
	andl	$1, %r9d
	je	.L165
	testb	$1, %al
	je	.L164
.L165:
	movl	%eax, %edi
	shrb	$5, %dil
	andl	$1, %edi
	je	.L166
	testb	$16, %al
	je	.L164
.L166:
	movq	8(%rcx), %r8
	testq	%r8, %r8
	je	.L169
	testb	%dl, %dl
	jne	.L170
	movzbl	(%rcx), %eax
	andl	$-13, %eax
	orl	$8, %eax
	movb	%al, (%rcx)
.L170:
	testb	%r9b, %r9b
	jne	.L171
	orb	$3, (%rcx)
.L171:
	testb	%dil, %dil
	jne	.L172
	orb	$48, (%rcx)
.L172:
	movzbl	(%rcx), %eax
	xorl	%edx, %edx
	testb	$2, %al
	je	.L173
	movl	%eax, %edx
	andl	$1, %edx
	cmpb	$1, %dl
	sbbl	%edx, %edx
	andl	$2, %edx
.L173:
	xorl	%ecx, %ecx
	testb	$8, %al
	je	.L174
	movl	%eax, %ecx
	shrb	$2, %cl
	andl	$1, %ecx
	cmpb	$1, %cl
	sbbl	%ecx, %ecx
	andl	$4, %ecx
.L174:
	orl	%ecx, %edx
	xorl	%ecx, %ecx
	testb	$32, %al
	je	.L175
	shrb	$4, %al
	andl	$1, %eax
	xorl	$1, %eax
	movzbl	%al, %ecx
.L175:
	orl	%edx, %ecx
	movq	%r15, %rdi
	movq	%r8, %rdx
	call	_ZN2v88internal8JSObject29SetOwnElementIgnoreAttributesENS0_6HandleIS1_EEjNS2_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	testq	%rax, %rax
	je	.L215
.L169:
	movl	$257, %eax
	jmp	.L177
.L223:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18366:
	.size	_ZN2v88internal12JSTypedArray17DefineOwnPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEEPNS0_18PropertyDescriptorENS_5MaybeINS0_11ShouldThrowEEE, .-_ZN2v88internal12JSTypedArray17DefineOwnPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEEPNS0_18PropertyDescriptorENS_5MaybeINS0_11ShouldThrowEEE
	.section	.rodata._ZN2v88internal12JSTypedArray4typeEv.str1.1,"aMS",@progbits,1
.LC8:
	.string	"unreachable code"
	.section	.text._ZN2v88internal12JSTypedArray4typeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12JSTypedArray4typeEv
	.type	_ZN2v88internal12JSTypedArray4typeEv, @function
_ZN2v88internal12JSTypedArray4typeEv:
.LFB18367:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	-1(%rax), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	subl	$17, %eax
	cmpl	$10, %eax
	ja	.L226
	leaq	CSWTCH.494(%rip), %rdx
	movl	(%rdx,%rax,4), %eax
	ret
.L226:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC8(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18367:
	.size	_ZN2v88internal12JSTypedArray4typeEv, .-_ZN2v88internal12JSTypedArray4typeEv
	.section	.text._ZN2v88internal12JSTypedArray12element_sizeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12JSTypedArray12element_sizeEv
	.type	_ZN2v88internal12JSTypedArray12element_sizeEv, @function
_ZN2v88internal12JSTypedArray12element_sizeEv:
.LFB18368:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	-1(%rax), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	subl	$17, %eax
	cmpl	$10, %eax
	ja	.L231
	leaq	CSWTCH.499(%rip), %rdx
	movq	(%rdx,%rax,8), %rax
	ret
.L231:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC8(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18368:
	.size	_ZN2v88internal12JSTypedArray12element_sizeEv, .-_ZN2v88internal12JSTypedArray12element_sizeEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal13JSArrayBuffer6DetachEv,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal13JSArrayBuffer6DetachEv, @function
_GLOBAL__sub_I__ZN2v88internal13JSArrayBuffer6DetachEv:
.LFB22092:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE22092:
	.size	_GLOBAL__sub_I__ZN2v88internal13JSArrayBuffer6DetachEv, .-_GLOBAL__sub_I__ZN2v88internal13JSArrayBuffer6DetachEv
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal13JSArrayBuffer6DetachEv
	.section	.rodata.CSWTCH.499,"a"
	.align 32
	.type	CSWTCH.499, @object
	.size	CSWTCH.499, 88
CSWTCH.499:
	.quad	1
	.quad	1
	.quad	2
	.quad	2
	.quad	4
	.quad	4
	.quad	4
	.quad	8
	.quad	1
	.quad	8
	.quad	8
	.section	.rodata.CSWTCH.494,"a"
	.align 32
	.type	CSWTCH.494, @object
	.size	CSWTCH.494, 44
CSWTCH.494:
	.long	2
	.long	1
	.long	4
	.long	3
	.long	6
	.long	5
	.long	7
	.long	8
	.long	9
	.long	11
	.long	10
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC7:
	.long	0
	.long	1127219200
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
