	.file	"js-collator.cc"
	.text
	.section	.text._ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data,"axG",@progbits,_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data,comdat
	.p2align 4
	.weak	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data
	.type	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data, @function
_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data:
.LFB22962:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r8
	movq	(%rdi), %rax
	movq	%r8, %rdi
	jmp	*%rax
	.cfi_endproc
.LFE22962:
	.size	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data, .-_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation,"axG",@progbits,_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation,comdat
	.p2align 4
	.weak	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation
	.type	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation:
.LFB22963:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L4
	cmpl	$3, %edx
	je	.L5
	cmpl	$1, %edx
	je	.L9
.L5:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movdqu	(%rsi), %xmm0
	xorl	%eax, %eax
	movups	%xmm0, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE22963:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation
	.section	.text._ZNSt19_Sp_counted_deleterIPN6icu_678CollatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt19_Sp_counted_deleterIPN6icu_678CollatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt19_Sp_counted_deleterIPN6icu_678CollatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt19_Sp_counted_deleterIPN6icu_678CollatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt19_Sp_counted_deleterIPN6icu_678CollatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB23103:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE23103:
	.size	_ZNSt19_Sp_counted_deleterIPN6icu_678CollatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt19_Sp_counted_deleterIPN6icu_678CollatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt19_Sp_counted_deleterIPN6icu_678CollatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt19_Sp_counted_deleterIPN6icu_678CollatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt19_Sp_counted_deleterIPN6icu_678CollatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt19_Sp_counted_deleterIPN6icu_678CollatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"axG",@progbits,_ZNSt19_Sp_counted_deleterIPN6icu_678CollatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt19_Sp_counted_deleterIPN6icu_678CollatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.type	_ZNSt19_Sp_counted_deleterIPN6icu_678CollatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
_ZNSt19_Sp_counted_deleterIPN6icu_678CollatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
.LFB23106:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L11
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L11:
	ret
	.cfi_endproc
.LFE23106:
	.size	_ZNSt19_Sp_counted_deleterIPN6icu_678CollatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt19_Sp_counted_deleterIPN6icu_678CollatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.section	.text._ZNSt19_Sp_counted_deleterIPN6icu_678CollatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,"axG",@progbits,_ZNSt19_Sp_counted_deleterIPN6icu_678CollatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt19_Sp_counted_deleterIPN6icu_678CollatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.type	_ZNSt19_Sp_counted_deleterIPN6icu_678CollatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, @function
_ZNSt19_Sp_counted_deleterIPN6icu_678CollatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info:
.LFB23108:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE23108:
	.size	_ZNSt19_Sp_counted_deleterIPN6icu_678CollatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, .-_ZNSt19_Sp_counted_deleterIPN6icu_678CollatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.rodata._ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE6AppendEPKci.str1.1,"aMS",@progbits,1
.LC0:
	.string	"basic_string::append"
	.section	.text._ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE6AppendEPKci,"axG",@progbits,_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE6AppendEPKci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE6AppendEPKci
	.type	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE6AppendEPKci, @function
_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE6AppendEPKci:
.LFB23109:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movslq	%edx, %rdx
	movabsq	$4611686018427387903, %rax
	subq	8(%rdi), %rax
	cmpq	%rax, %rdx
	ja	.L19
	jmp	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.L19:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE23109:
	.size	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE6AppendEPKci, .-_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE6AppendEPKci
	.section	.text._ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED2Ev,"axG",@progbits,_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED2Ev
	.type	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED2Ev, @function
_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED2Ev:
.LFB20243:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_678ByteSinkD2Ev@PLT
	.cfi_endproc
.LFE20243:
	.size	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED2Ev, .-_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED2Ev
	.weak	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED1Ev
	.set	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED1Ev,_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED2Ev
	.section	.text._ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED0Ev,"axG",@progbits,_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED0Ev
	.type	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED0Ev, @function
_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED0Ev:
.LFB20245:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_678ByteSinkD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE20245:
	.size	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED0Ev, .-_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED0Ev
	.section	.text._ZNSt19_Sp_counted_deleterIPN6icu_678CollatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt19_Sp_counted_deleterIPN6icu_678CollatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt19_Sp_counted_deleterIPN6icu_678CollatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt19_Sp_counted_deleterIPN6icu_678CollatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt19_Sp_counted_deleterIPN6icu_678CollatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB23107:
	.cfi_startproc
	endbr64
	jmp	_ZdlPv@PLT
	.cfi_endproc
.LFE23107:
	.size	_ZNSt19_Sp_counted_deleterIPN6icu_678CollatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt19_Sp_counted_deleterIPN6icu_678CollatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZNSt19_Sp_counted_deleterIPN6icu_678CollatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED0Ev,"axG",@progbits,_ZNSt19_Sp_counted_deleterIPN6icu_678CollatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt19_Sp_counted_deleterIPN6icu_678CollatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.type	_ZNSt19_Sp_counted_deleterIPN6icu_678CollatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED0Ev, @function
_ZNSt19_Sp_counted_deleterIPN6icu_678CollatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED0Ev:
.LFB23105:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE23105:
	.size	_ZNSt19_Sp_counted_deleterIPN6icu_678CollatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED0Ev, .-_ZNSt19_Sp_counted_deleterIPN6icu_678CollatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.section	.rodata._ZN2v88internal12_GLOBAL__N_128CreateDataPropertyForOptionsEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEEPKc.str1.1,"aMS",@progbits,1
.LC1:
	.string	"(value) != nullptr"
.LC2:
	.string	"Check failed: %s."
.LC3:
	.string	"(location_) != nullptr"
	.section	.rodata._ZN2v88internal12_GLOBAL__N_128CreateDataPropertyForOptionsEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEEPKc.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"JSReceiver::CreateDataProperty(isolate, options, key, value_str, Just(kDontThrow)) .FromJust()"
	.section	.text._ZN2v88internal12_GLOBAL__N_128CreateDataPropertyForOptionsEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEEPKc,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_128CreateDataPropertyForOptionsEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEEPKc, @function
_ZN2v88internal12_GLOBAL__N_128CreateDataPropertyForOptionsEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEEPKc:
.LFB18206:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.L32
	movq	%rdi, %r12
	movq	%rcx, %rdi
	movq	%rcx, %rbx
	movq	%rsi, %r14
	movq	%rdx, %r13
	call	strlen@PLT
	leaq	-64(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rbx, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L33
	movabsq	$4294967297, %r8
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10JSReceiver18CreateDataPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testb	%al, %al
	je	.L34
.L28:
	shrw	$8, %ax
	je	.L35
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L36
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L33:
	leaq	.LC3(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L34:
	movl	%eax, -68(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-68(%rbp), %eax
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L35:
	leaq	.LC4(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L36:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18206:
	.size	_ZN2v88internal12_GLOBAL__N_128CreateDataPropertyForOptionsEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEEPKc, .-_ZN2v88internal12_GLOBAL__N_128CreateDataPropertyForOptionsEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEEPKc
	.section	.text._ZN2v88internal7ManagedIN6icu_678CollatorEE10DestructorEPv,"axG",@progbits,_ZN2v88internal7ManagedIN6icu_678CollatorEE10DestructorEPv,comdat
	.p2align 4
	.weak	_ZN2v88internal7ManagedIN6icu_678CollatorEE10DestructorEPv
	.type	_ZN2v88internal7ManagedIN6icu_678CollatorEE10DestructorEPv, @function
_ZN2v88internal7ManagedIN6icu_678CollatorEE10DestructorEPv:
.LFB22375:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L37
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	8(%rdi), %r13
	testq	%r13, %r13
	je	.L40
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L41
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
.L42:
	cmpl	$1, %eax
	je	.L49
.L40:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$16, %esi
	popq	%rbx
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L37:
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L44
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L45:
	cmpl	$1, %eax
	jne	.L40
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L41:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L44:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L45
	.cfi_endproc
.LFE22375:
	.size	_ZN2v88internal7ManagedIN6icu_678CollatorEE10DestructorEPv, .-_ZN2v88internal7ManagedIN6icu_678CollatorEE10DestructorEPv
	.section	.text._ZN2v88internal10JSCollator19GetAvailableLocalesB5cxx11Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSCollator19GetAvailableLocalesB5cxx11Ev
	.type	_ZN2v88internal10JSCollator19GetAvailableLocalesB5cxx11Ev, @function
_ZN2v88internal10JSCollator19GetAvailableLocalesB5cxx11Ev:
.LFB18267:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$56, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movzbl	_ZZN2v88internal10JSCollator19GetAvailableLocalesB5cxx11EvE17available_locales(%rip), %eax
	cmpb	$2, %al
	jne	.L61
.L51:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L62
	addq	$56, %rsp
	leaq	16+_ZZN2v88internal10JSCollator19GetAvailableLocalesB5cxx11EvE17available_locales(%rip), %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	.cfi_restore_state
	leaq	_ZN2v84base16LazyInstanceImplINS_8internal4Intl16AvailableLocalesIN6icu_678CollatorENS2_12_GLOBAL__N_19CheckCollEEENS0_32StaticallyAllocatedInstanceTraitIS9_EENS0_21DefaultConstructTraitIS9_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS9_EEE12InitInstanceEPv(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rcx
	movq	%rax, -64(%rbp)
	movq	%rcx, %xmm0
	leaq	8+_ZZN2v88internal10JSCollator19GetAvailableLocalesB5cxx11EvE17available_locales(%rip), %rax
	leaq	-64(%rbp), %r12
	movq	%rax, -56(%rbp)
	leaq	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%r12, %rsi
	leaq	_ZZN2v88internal10JSCollator19GetAvailableLocalesB5cxx11EvE17available_locales(%rip), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-48(%rbp), %rax
	testq	%rax, %rax
	je	.L51
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	*%rax
	jmp	.L51
.L62:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18267:
	.size	_ZN2v88internal10JSCollator19GetAvailableLocalesB5cxx11Ev, .-_ZN2v88internal10JSCollator19GetAvailableLocalesB5cxx11Ev
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E:
.LFB21554:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L78
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
.L67:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L65
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L63
.L66:
	movq	%rbx, %r12
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L65:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L66
.L63:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L78:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE21554:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	.section	.rodata._ZN2v84base16LazyInstanceImplINS_8internal4Intl16AvailableLocalesIN6icu_678CollatorENS2_12_GLOBAL__N_19CheckCollEEENS0_32StaticallyAllocatedInstanceTraitIS9_EENS0_21DefaultConstructTraitIS9_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS9_EEE12InitInstanceEPv.str1.1,"aMS",@progbits,1
.LC5:
	.string	"icudt67l-coll"
	.section	.text._ZN2v84base16LazyInstanceImplINS_8internal4Intl16AvailableLocalesIN6icu_678CollatorENS2_12_GLOBAL__N_19CheckCollEEENS0_32StaticallyAllocatedInstanceTraitIS9_EENS0_21DefaultConstructTraitIS9_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS9_EEE12InitInstanceEPv,"ax",@progbits
	.p2align 4
	.type	_ZN2v84base16LazyInstanceImplINS_8internal4Intl16AvailableLocalesIN6icu_678CollatorENS2_12_GLOBAL__N_19CheckCollEEENS0_32StaticallyAllocatedInstanceTraitIS9_EENS0_21DefaultConstructTraitIS9_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS9_EEE12InitInstanceEPv, @function
_ZN2v84base16LazyInstanceImplINS_8internal4Intl16AvailableLocalesIN6icu_678CollatorENS2_12_GLOBAL__N_19CheckCollEEENS0_32StaticallyAllocatedInstanceTraitIS9_EENS0_21DefaultConstructTraitIS9_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS9_EEE12InitInstanceEPv:
.LFB22401:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	16(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN2v88internal4Intl16AvailableLocalesIN6icu_678CollatorENS0_12_GLOBAL__N_19CheckCollEEE(%rip), %rax
	movl	$0, 16(%rdi)
	movq	%rax, (%rdi)
	movq	$0, 24(%rdi)
	movq	%r13, 32(%rdi)
	movq	%r13, 40(%rdi)
	movq	$0, 48(%rdi)
	leaq	-116(%rbp), %rdi
	movl	$0, -116(%rbp)
	call	_ZN6icu_678Collator19getAvailableLocalesERi@PLT
	movl	-116(%rbp), %edx
	leaq	-112(%rbp), %rdi
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	leaq	.LC5(%rip), %rcx
	call	_ZN2v88internal4Intl14BuildLocaleSetB5cxx11EPKN6icu_676LocaleEiPKcS7_@PLT
	movq	24(%rbx), %r12
	testq	%r12, %r12
	je	.L82
	leaq	8(%rbx), %r15
.L85:
	movq	24(%r12), %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %r14
	cmpq	%rax, %rdi
	je	.L83
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r14, %r14
	je	.L82
.L84:
	movq	%r14, %r12
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L83:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r14, %r14
	jne	.L84
.L82:
	movq	-96(%rbp), %rax
	movq	$0, 24(%rbx)
	movq	%r13, 32(%rbx)
	movq	%r13, 40(%rbx)
	movq	$0, 48(%rbx)
	testq	%rax, %rax
	je	.L81
	movl	-104(%rbp), %edx
	movq	%rax, 24(%rbx)
	movl	%edx, 16(%rbx)
	movq	-88(%rbp), %rdx
	movq	%rdx, 32(%rbx)
	movq	-80(%rbp), %rdx
	movq	%rdx, 40(%rbx)
	movq	%r13, 8(%rax)
	movq	-72(%rbp), %rax
	movq	%rax, 48(%rbx)
.L81:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L101
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L101:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22401:
	.size	_ZN2v84base16LazyInstanceImplINS_8internal4Intl16AvailableLocalesIN6icu_678CollatorENS2_12_GLOBAL__N_19CheckCollEEENS0_32StaticallyAllocatedInstanceTraitIS9_EENS0_21DefaultConstructTraitIS9_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS9_EEE12InitInstanceEPv, .-_ZN2v84base16LazyInstanceImplINS_8internal4Intl16AvailableLocalesIN6icu_678CollatorENS2_12_GLOBAL__N_19CheckCollEEENS0_32StaticallyAllocatedInstanceTraitIS9_EENS0_21DefaultConstructTraitIS9_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS9_EEE12InitInstanceEPv
	.section	.text._ZN2v88internal4Intl16AvailableLocalesIN6icu_678CollatorENS0_12_GLOBAL__N_19CheckCollEED2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4Intl16AvailableLocalesIN6icu_678CollatorENS0_12_GLOBAL__N_19CheckCollEED2Ev, @function
_ZN2v88internal4Intl16AvailableLocalesIN6icu_678CollatorENS0_12_GLOBAL__N_19CheckCollEED2Ev:
.LFB23111:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal4Intl16AvailableLocalesIN6icu_678CollatorENS0_12_GLOBAL__N_19CheckCollEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	24(%rdi), %r12
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L102
	leaq	8(%rdi), %r13
.L106:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L104
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L102
.L105:
	movq	%rbx, %r12
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L104:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L105
.L102:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23111:
	.size	_ZN2v88internal4Intl16AvailableLocalesIN6icu_678CollatorENS0_12_GLOBAL__N_19CheckCollEED2Ev, .-_ZN2v88internal4Intl16AvailableLocalesIN6icu_678CollatorENS0_12_GLOBAL__N_19CheckCollEED2Ev
	.set	_ZN2v88internal4Intl16AvailableLocalesIN6icu_678CollatorENS0_12_GLOBAL__N_19CheckCollEED1Ev,_ZN2v88internal4Intl16AvailableLocalesIN6icu_678CollatorENS0_12_GLOBAL__N_19CheckCollEED2Ev
	.section	.text._ZN2v88internal4Intl16AvailableLocalesIN6icu_678CollatorENS0_12_GLOBAL__N_19CheckCollEED0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4Intl16AvailableLocalesIN6icu_678CollatorENS0_12_GLOBAL__N_19CheckCollEED0Ev, @function
_ZN2v88internal4Intl16AvailableLocalesIN6icu_678CollatorENS0_12_GLOBAL__N_19CheckCollEED0Ev:
.LFB23113:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal4Intl16AvailableLocalesIN6icu_678CollatorENS0_12_GLOBAL__N_19CheckCollEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	24(%rdi), %r12
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L118
	leaq	8(%rdi), %r14
.L121:
	movq	24(%r12), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L119
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L118
.L120:
	movq	%rbx, %r12
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L119:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L120
.L118:
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	movl	$56, %esi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE23113:
	.size	_ZN2v88internal4Intl16AvailableLocalesIN6icu_678CollatorENS0_12_GLOBAL__N_19CheckCollEED0Ev, .-_ZN2v88internal4Intl16AvailableLocalesIN6icu_678CollatorENS0_12_GLOBAL__N_19CheckCollEED0Ev
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E:
.LFB21556:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L148
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
.L137:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	movq	64(%r12), %rdi
	leaq	80(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L134
	call	_ZdlPv@PLT
.L134:
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	cmpq	%rax, %rdi
	je	.L135
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L132
.L136:
	movq	%rbx, %r12
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L135:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L136
.L132:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L148:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE21556:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE4findERS7_,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE4findERS7_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE4findERS7_
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE4findERS7_, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE4findERS7_:
.LFB21572:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	8(%rdi), %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %r13
	movq	%r12, -56(%rbp)
	testq	%r13, %r13
	je	.L152
	movq	8(%rsi), %r15
	movq	(%rsi), %r14
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L158:
	movq	24(%r13), %r13
	testq	%r13, %r13
	je	.L154
.L153:
	movq	40(%r13), %rbx
	movq	%r15, %rdx
	cmpq	%r15, %rbx
	cmovbe	%rbx, %rdx
	testq	%rdx, %rdx
	je	.L155
	movq	32(%r13), %rdi
	movq	%r14, %rsi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L156
.L155:
	subq	%r15, %rbx
	movl	$2147483648, %eax
	cmpq	%rax, %rbx
	jge	.L157
	movabsq	$-2147483649, %rax
	cmpq	%rax, %rbx
	jle	.L158
	movl	%ebx, %eax
.L156:
	testl	%eax, %eax
	js	.L158
.L157:
	movq	%r13, %r12
	movq	16(%r13), %r13
	testq	%r13, %r13
	jne	.L153
.L154:
	cmpq	%r12, -56(%rbp)
	je	.L152
	movq	40(%r12), %rbx
	cmpq	%rbx, %r15
	movq	%rbx, %rdx
	cmovbe	%r15, %rdx
	testq	%rdx, %rdx
	je	.L160
	movq	32(%r12), %rsi
	movq	%r14, %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L161
.L160:
	movq	%r15, %r8
	subq	%rbx, %r8
	cmpq	$2147483647, %r8
	jg	.L152
	cmpq	$-2147483648, %r8
	jl	.L163
	movl	%r8d, %eax
.L161:
	testl	%eax, %eax
	cmovs	-56(%rbp), %r12
.L152:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L163:
	.cfi_restore_state
	movq	-56(%rbp), %r12
	jmp	.L152
	.cfi_endproc
.LFE21572:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE4findERS7_, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE4findERS7_
	.section	.text._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2IN6icu_678CollatorESt14default_deleteIS5_EEEOSt10unique_ptrIT_T0_E,"axG",@progbits,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC5IN6icu_678CollatorESt14default_deleteIS5_EEEOSt10unique_ptrIT_T0_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2IN6icu_678CollatorESt14default_deleteIS5_EEEOSt10unique_ptrIT_T0_E
	.type	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2IN6icu_678CollatorESt14default_deleteIS5_EEEOSt10unique_ptrIT_T0_E, @function
_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2IN6icu_678CollatorESt14default_deleteIS5_EEEOSt10unique_ptrIT_T0_E:
.LFB22686:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rsi)
	movq	$0, (%rdi)
	je	.L174
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	$24, %edi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_Znwm@PLT
	movq	(%rbx), %rdx
	movq	$0, (%rbx)
	movabsq	$4294967297, %rcx
	movq	%rcx, 8(%rax)
	leaq	16+_ZTVSt19_Sp_counted_deleterIPN6icu_678CollatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	%rdx, 16(%rax)
	movq	%rax, (%r12)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L174:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE22686:
	.size	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2IN6icu_678CollatorESt14default_deleteIS5_EEEOSt10unique_ptrIT_T0_E, .-_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2IN6icu_678CollatorESt14default_deleteIS5_EEEOSt10unique_ptrIT_T0_E
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1IN6icu_678CollatorESt14default_deleteIS5_EEEOSt10unique_ptrIT_T0_E
	.set	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1IN6icu_678CollatorESt14default_deleteIS5_EEEOSt10unique_ptrIT_T0_E,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2IN6icu_678CollatorESt14default_deleteIS5_EEEOSt10unique_ptrIT_T0_E
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE24_M_get_insert_unique_posERKS5_,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE24_M_get_insert_unique_posERKS5_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE24_M_get_insert_unique_posERKS5_
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE24_M_get_insert_unique_posERKS5_, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE24_M_get_insert_unique_posERKS5_:
.LFB22820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r15
	movq	%rdi, -72(%rbp)
	movq	%rsi, -64(%rbp)
	testq	%r15, %r15
	je	.L203
	movq	8(%rsi), %r14
	movq	(%rsi), %rbx
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L185:
	movq	16(%r15), %rax
	movl	$1, %esi
	testq	%rax, %rax
	je	.L181
.L204:
	movq	%rax, %r15
.L180:
	movq	40(%r15), %r12
	movq	32(%r15), %r13
	cmpq	%r12, %r14
	movq	%r12, %rdx
	cmovbe	%r14, %rdx
	testq	%rdx, %rdx
	je	.L182
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%rdx, -56(%rbp)
	call	memcmp@PLT
	movq	-56(%rbp), %rdx
	testl	%eax, %eax
	jne	.L183
.L182:
	movq	%r14, %rax
	movl	$2147483648, %ecx
	subq	%r12, %rax
	cmpq	%rcx, %rax
	jge	.L184
	movabsq	$-2147483649, %rdi
	cmpq	%rdi, %rax
	jle	.L185
.L183:
	testl	%eax, %eax
	js	.L185
.L184:
	movq	24(%r15), %rax
	xorl	%esi, %esi
	testq	%rax, %rax
	jne	.L204
.L181:
	movq	%r15, %r8
	testb	%sil, %sil
	jne	.L179
.L187:
	testq	%rdx, %rdx
	je	.L190
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%r8, -56(%rbp)
	call	memcmp@PLT
	movq	-56(%rbp), %r8
	testl	%eax, %eax
	jne	.L191
.L190:
	movq	%r12, %rcx
	subq	%r14, %rcx
	cmpq	$2147483647, %rcx
	jg	.L192
	cmpq	$-2147483648, %rcx
	jl	.L193
	movl	%ecx, %eax
.L191:
	testl	%eax, %eax
	js	.L193
.L192:
	addq	$40, %rsp
	movq	%r15, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L193:
	.cfi_restore_state
	addq	$40, %rsp
	xorl	%eax, %eax
	movq	%r8, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L203:
	.cfi_restore_state
	leaq	8(%rdi), %r15
.L179:
	movq	-72(%rbp), %rax
	cmpq	24(%rax), %r15
	je	.L205
	movq	%r15, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	-64(%rbp), %rbx
	movq	%r15, %r8
	movq	40(%rax), %r12
	movq	32(%rax), %r13
	movq	%rax, %r15
	movq	8(%rbx), %r14
	movq	(%rbx), %rbx
	cmpq	%r14, %r12
	movq	%r14, %rdx
	cmovbe	%r12, %rdx
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L205:
	addq	$40, %rsp
	movq	%r15, %rdx
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22820:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE24_M_get_insert_unique_posERKS5_, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE24_M_get_insert_unique_posERKS5_
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal10JSCollator15ResolvedOptionsEPNS0_7IsolateENS0_6HandleIS1_EE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal10JSCollator15ResolvedOptionsEPNS0_7IsolateENS0_6HandleIS1_EE, @function
_GLOBAL__sub_I__ZN2v88internal10JSCollator15ResolvedOptionsEPNS0_7IsolateENS0_6HandleIS1_EE:
.LFB23131:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE23131:
	.size	_GLOBAL__sub_I__ZN2v88internal10JSCollator15ResolvedOptionsEPNS0_7IsolateENS0_6HandleIS1_EE, .-_GLOBAL__sub_I__ZN2v88internal10JSCollator15ResolvedOptionsEPNS0_7IsolateENS0_6HandleIS1_EE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal10JSCollator15ResolvedOptionsEPNS0_7IsolateENS0_6HandleIS1_EE
	.section	.rodata._ZN2v88internal10JSCollator15ResolvedOptionsEPNS0_7IsolateENS0_6HandleIS1_EE.str1.1,"aMS",@progbits,1
.LC6:
	.string	"false"
.LC7:
	.string	"lower"
.LC8:
	.string	"upper"
.LC9:
	.string	"case"
.LC10:
	.string	"base"
.LC11:
	.string	"variant"
.LC12:
	.string	"accent"
.LC13:
	.string	"sort"
.LC14:
	.string	"default"
.LC15:
	.string	"search"
.LC16:
	.string	"(icu_collator) != nullptr"
.LC17:
	.string	"U_SUCCESS(status)"
.LC18:
	.string	"co"
	.section	.rodata._ZN2v88internal10JSCollator15ResolvedOptionsEPNS0_7IsolateENS0_6HandleIS1_EE.str1.8,"aMS",@progbits,1
	.align 8
.LC19:
	.string	"basic_string::_M_construct null not valid"
	.align 8
.LC20:
	.string	"JSReceiver::CreateDataProperty(isolate, options, key, value_obj, Just(kDontThrow)) .FromJust()"
	.section	.text._ZN2v88internal10JSCollator15ResolvedOptionsEPNS0_7IsolateENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSCollator15ResolvedOptionsEPNS0_7IsolateENS0_6HandleIS1_EE
	.type	_ZN2v88internal10JSCollator15ResolvedOptionsEPNS0_7IsolateENS0_6HandleIS1_EE, @function
_ZN2v88internal10JSCollator15ResolvedOptionsEPNS0_7IsolateENS0_6HandleIS1_EE:
.LFB18208:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$904, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	12464(%rdi), %rax
	movq	39(%rax), %rax
	movq	879(%rax), %r13
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L209
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L210:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movq	%rax, %r13
	movq	(%rbx), %rax
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %r14
	testq	%r14, %r14
	je	.L317
	leaq	-852(%rbp), %rbx
	movl	$7, %esi
	movq	%r14, %rdi
	movl	$0, -852(%rbp)
	movq	(%r14), %rax
	movq	%rbx, %rdx
	call	*192(%rax)
	movl	-852(%rbp), %r11d
	movl	%eax, -892(%rbp)
	testl	%r11d, %r11d
	jg	.L215
	movl	$0, -852(%rbp)
	movq	(%r14), %rax
	movq	%rbx, %rdx
	movl	$2, %esi
	movq	%r14, %rdi
	call	*192(%rax)
	leaq	.LC7(%rip), %rcx
	movq	%rcx, -872(%rbp)
	cmpl	$24, %eax
	je	.L214
	cmpl	$25, %eax
	leaq	.LC6(%rip), %rdx
	leaq	.LC8(%rip), %rax
	cmovne	%rdx, %rax
	movq	%rax, -872(%rbp)
.L214:
	movl	-852(%rbp), %r10d
	testl	%r10d, %r10d
	jg	.L215
	movl	$0, -852(%rbp)
	movq	(%r14), %rax
	movq	%rbx, %rdx
	movl	$5, %esi
	movq	%r14, %rdi
	call	*192(%rax)
	testl	%eax, %eax
	je	.L216
	cmpl	$1, %eax
	leaq	.LC12(%rip), %rdx
	leaq	.LC11(%rip), %rax
	movl	-852(%rbp), %r9d
	cmove	%rdx, %rax
	movq	%rax, -888(%rbp)
	testl	%r9d, %r9d
	jg	.L215
.L220:
	movl	$0, -852(%rbp)
	movq	(%r14), %rax
	movl	$1, %esi
	movq	%rbx, %rdx
	movq	%r14, %rdi
	call	*192(%rax)
	movl	-852(%rbp), %esi
	cmpl	$20, %eax
	sete	-893(%rbp)
	testl	%esi, %esi
	jg	.L215
	leaq	-512(%rbp), %rax
	movq	%rbx, %rcx
	movl	$1, %edx
	movq	%r14, %rsi
	movl	$0, -852(%rbp)
	movq	%rax, %rdi
	movq	(%r14), %rax
	movq	%rdi, -880(%rbp)
	call	*136(%rax)
	movl	-852(%rbp), %ecx
	testl	%ecx, %ecx
	jg	.L215
	leaq	-816(%rbp), %rcx
	leaq	16+_ZTVN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE(%rip), %r15
	movl	$0, -852(%rbp)
	movq	%rcx, -920(%rbp)
	movq	%r15, %xmm0
	leaq	-848(%rbp), %rdi
	leaq	.LC18(%rip), %rsi
	leaq	-832(%rbp), %r14
	movhps	-920(%rbp), %xmm0
	movaps	%xmm0, -912(%rbp)
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movdqa	-912(%rbp), %xmm0
	movq	%rbx, %r8
	movq	-840(%rbp), %rdx
	leaq	-800(%rbp), %rcx
	movq	-848(%rbp), %rsi
	movq	-880(%rbp), %rdi
	movq	$0, -808(%rbp)
	movq	%rcx, -928(%rbp)
	movq	%rcx, -816(%rbp)
	movq	%r14, %rcx
	movaps	%xmm0, -832(%rbp)
	movb	$0, -800(%rbp)
	call	_ZNK6icu_676Locale22getUnicodeKeywordValueENS_11StringPieceERNS_8ByteSinkER10UErrorCode@PLT
	movq	%r14, %rdi
	movq	%r15, -832(%rbp)
	call	_ZN6icu_678ByteSinkD2Ev@PLT
	movl	-852(%rbp), %edx
	leaq	-768(%rbp), %rcx
	movq	$0, -776(%rbp)
	movq	%rcx, -912(%rbp)
	movq	%rcx, -784(%rbp)
	movb	$0, -768(%rbp)
	testl	%edx, %edx
	jle	.L318
	movq	-880(%rbp), %rsi
	leaq	-560(%rbp), %rdi
	call	_ZN2v88internal4Intl13ToLanguageTagB5cxx11ERKN6icu_676LocaleE@PLT
	cmpb	$0, -560(%rbp)
	je	.L319
.L253:
	movq	-552(%rbp), %r8
	movq	-544(%rbp), %r15
	leaq	-672(%rbp), %rbx
	movq	%rbx, -688(%rbp)
	movq	%r8, %rax
	addq	%r15, %rax
	je	.L281
	testq	%r8, %r8
	je	.L239
.L281:
	movq	%r15, -832(%rbp)
	cmpq	$15, %r15
	ja	.L320
	cmpq	$1, %r15
	jne	.L257
	movzbl	(%r8), %eax
	movb	%al, -672(%rbp)
	movq	%rbx, %rax
.L258:
	movq	%r15, -680(%rbp)
	movb	$0, (%rax,%r15)
	movq	-688(%rbp), %rdx
	movq	-784(%rbp), %rdi
	cmpq	%rbx, %rdx
	je	.L321
	movq	-672(%rbp), %rcx
	movq	-680(%rbp), %rax
	cmpq	-912(%rbp), %rdi
	je	.L322
	movq	%rax, %xmm0
	movq	%rcx, %xmm1
	movq	-768(%rbp), %rsi
	movq	%rdx, -784(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -776(%rbp)
	testq	%rdi, %rdi
	je	.L264
	movq	%rdi, -688(%rbp)
	movq	%rsi, -672(%rbp)
.L262:
	movq	$0, -680(%rbp)
	movb	$0, (%rdi)
	movq	-688(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L265
	call	_ZdlPv@PLT
.L265:
	movq	-552(%rbp), %rdi
	leaq	-536(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L266
	call	_ZdlPv@PLT
.L266:
	leaq	.LC13(%rip), %r14
	leaq	.LC14(%rip), %r15
.L237:
	movq	-784(%rbp), %rcx
	leaq	1592(%r12), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_128CreateDataPropertyForOptionsEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEEPKc
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	1936(%r12), %rdx
	call	_ZN2v88internal12_GLOBAL__N_128CreateDataPropertyForOptionsEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEEPKc
	movq	-888(%rbp), %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	1816(%r12), %rdx
	call	_ZN2v88internal12_GLOBAL__N_128CreateDataPropertyForOptionsEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEEPKc
	movzbl	-893(%rbp), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory9ToBooleanEb@PLT
	leaq	1528(%r12), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movabsq	$4294967297, %r8
	movq	%rax, %rcx
	call	_ZN2v88internal10JSReceiver18CreateDataPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testb	%al, %al
	je	.L323
.L267:
	shrw	$8, %ax
	je	.L270
	leaq	1256(%r12), %rdx
	movq	%r15, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_128CreateDataPropertyForOptionsEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEEPKc
	xorl	%esi, %esi
	cmpl	$17, -892(%rbp)
	movq	%r12, %rdi
	sete	%sil
	call	_ZN2v88internal7Factory9ToBooleanEb@PLT
	leaq	1736(%r12), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movabsq	$4294967297, %r8
	movq	%rax, %rcx
	call	_ZN2v88internal10JSReceiver18CreateDataPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testb	%al, %al
	je	.L324
.L269:
	shrw	$8, %ax
	je	.L270
	movq	-872(%rbp), %rcx
	movq	%r12, %rdi
	leaq	1232(%r12), %rdx
	movq	%r13, %rsi
	call	_ZN2v88internal12_GLOBAL__N_128CreateDataPropertyForOptionsEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6StringEEEPKc
	movq	-784(%rbp), %rdi
	cmpq	-912(%rbp), %rdi
	je	.L271
	call	_ZdlPv@PLT
.L271:
	movq	-816(%rbp), %rdi
	cmpq	-928(%rbp), %rdi
	je	.L272
	call	_ZdlPv@PLT
.L272:
	movq	-880(%rbp), %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L325
	addq	$904, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L318:
	.cfi_restore_state
	movq	-920(%rbp), %rdi
	leaq	.LC15(%rip), %rsi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	je	.L326
	movq	-880(%rbp), %rsi
	leaq	-608(%rbp), %rdi
	movq	-816(%rbp), %r15
	call	_ZN2v88internal4Intl13ToLanguageTagB5cxx11ERKN6icu_676LocaleE@PLT
	cmpb	$0, -608(%rbp)
	je	.L327
.L238:
	movq	-600(%rbp), %r8
	movq	-592(%rbp), %rbx
	leaq	-704(%rbp), %rcx
	movq	%rcx, -720(%rbp)
	movq	%r8, %rax
	addq	%rbx, %rax
	je	.L280
	testq	%r8, %r8
	je	.L239
.L280:
	movq	%rbx, -832(%rbp)
	cmpq	$15, %rbx
	ja	.L328
	cmpq	$1, %rbx
	jne	.L243
	movzbl	(%r8), %eax
	movb	%al, -704(%rbp)
	movq	%rcx, %rax
.L244:
	movq	%rbx, -712(%rbp)
	movb	$0, (%rax,%rbx)
	movq	-720(%rbp), %rdx
	movq	-784(%rbp), %rdi
	cmpq	%rcx, %rdx
	je	.L329
	movq	-712(%rbp), %rax
	movq	-704(%rbp), %rsi
	cmpq	-912(%rbp), %rdi
	je	.L330
	movq	%rax, %xmm0
	movq	%rsi, %xmm3
	movq	-768(%rbp), %r8
	movq	%rdx, -784(%rbp)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, -776(%rbp)
	testq	%rdi, %rdi
	je	.L250
	movq	%rdi, -720(%rbp)
	movq	%r8, -704(%rbp)
.L248:
	movq	$0, -712(%rbp)
	movb	$0, (%rdi)
	movq	-720(%rbp), %rdi
	cmpq	%rcx, %rdi
	je	.L251
	call	_ZdlPv@PLT
.L251:
	movq	-600(%rbp), %rdi
	leaq	-584(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L252
	call	_ZdlPv@PLT
.L252:
	leaq	.LC13(%rip), %r14
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L209:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L331
.L211:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, (%rsi)
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L216:
	movl	-852(%rbp), %r8d
	testl	%r8d, %r8d
	jg	.L215
	movl	$0, -852(%rbp)
	movq	(%r14), %rax
	movq	%rbx, %rdx
	movq	%r14, %rdi
	movl	$3, %esi
	call	*192(%rax)
	leaq	.LC9(%rip), %rdx
	movl	-852(%rbp), %edi
	cmpl	$17, %eax
	leaq	.LC10(%rip), %rax
	cmove	%rdx, %rax
	movq	%rax, -888(%rbp)
	testl	%edi, %edi
	jle	.L220
	.p2align 4,,10
	.p2align 3
.L215:
	leaq	.LC17(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L326:
	movq	-880(%rbp), %rsi
	leaq	-288(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	leaq	.LC18(%rip), %rsi
	movq	%r14, %rdi
	movl	$0, -852(%rbp)
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movq	%rbx, %r9
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	-832(%rbp), %rsi
	movq	-824(%rbp), %rdx
	movq	%r15, %rdi
	call	_ZN6icu_676Locale22setUnicodeKeywordValueENS_11StringPieceES1_R10UErrorCode@PLT
	movl	-852(%rbp), %eax
	testl	%eax, %eax
	jg	.L215
	leaq	-656(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal4Intl13ToLanguageTagB5cxx11ERKN6icu_676LocaleE@PLT
	cmpb	$0, -656(%rbp)
	je	.L332
.L223:
	movq	-648(%rbp), %r8
	movq	-640(%rbp), %rbx
	leaq	-736(%rbp), %rcx
	movq	%rcx, -752(%rbp)
	movq	%r8, %rax
	addq	%rbx, %rax
	je	.L224
	testq	%r8, %r8
	jne	.L224
.L239:
	leaq	.LC19(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L257:
	testq	%r15, %r15
	jne	.L333
	movq	%rbx, %rax
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L224:
	movq	%rbx, -832(%rbp)
	cmpq	$15, %rbx
	ja	.L334
	cmpq	$1, %rbx
	jne	.L227
	movzbl	(%r8), %eax
	movb	%al, -736(%rbp)
	movq	%rcx, %rax
.L228:
	movq	%rbx, -744(%rbp)
	movb	$0, (%rax,%rbx)
	movq	-752(%rbp), %rdx
	movq	-784(%rbp), %rdi
	cmpq	%rcx, %rdx
	je	.L335
	movq	-736(%rbp), %rsi
	movq	-744(%rbp), %rax
	cmpq	-912(%rbp), %rdi
	je	.L336
	movq	%rax, %xmm0
	movq	%rsi, %xmm4
	movq	-768(%rbp), %r8
	movq	%rdx, -784(%rbp)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, -776(%rbp)
	testq	%rdi, %rdi
	je	.L234
	movq	%rdi, -752(%rbp)
	movq	%r8, -736(%rbp)
.L232:
	movq	$0, -744(%rbp)
	movb	$0, (%rdi)
	movq	-752(%rbp), %rdi
	cmpq	%rcx, %rdi
	je	.L235
	call	_ZdlPv@PLT
.L235:
	movq	-648(%rbp), %rdi
	leaq	-632(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L236
	call	_ZdlPv@PLT
.L236:
	movq	%r15, %rdi
	leaq	.LC15(%rip), %r14
	leaq	.LC14(%rip), %r15
	call	_ZN6icu_676LocaleD1Ev@PLT
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L317:
	leaq	.LC16(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L320:
	leaq	-688(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r8, -920(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-920(%rbp), %r8
	movq	%rax, -688(%rbp)
	movq	%rax, %rdi
	movq	-832(%rbp), %rax
	movq	%rax, -672(%rbp)
.L256:
	movq	%r15, %rdx
	movq	%r8, %rsi
	call	memcpy@PLT
	movq	-832(%rbp), %r15
	movq	-688(%rbp), %rax
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L270:
	leaq	.LC20(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L321:
	movq	-680(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L260
	cmpq	$1, %rdx
	je	.L337
	movq	%rbx, %rsi
	call	memcpy@PLT
	movq	-680(%rbp), %rdx
	movq	-784(%rbp), %rdi
.L260:
	movq	%rdx, -776(%rbp)
	movb	$0, (%rdi,%rdx)
	movq	-688(%rbp), %rdi
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L331:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L322:
	movq	%rax, %xmm0
	movq	%rcx, %xmm2
	movq	%rdx, -784(%rbp)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, -776(%rbp)
.L264:
	movq	%rbx, -688(%rbp)
	leaq	-672(%rbp), %rbx
	movq	%rbx, %rdi
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L323:
	movl	%eax, -888(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-888(%rbp), %eax
	jmp	.L267
	.p2align 4,,10
	.p2align 3
.L243:
	testq	%rbx, %rbx
	jne	.L338
	movq	%rcx, %rax
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L227:
	testq	%rbx, %rbx
	jne	.L339
	movq	%rcx, %rax
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L324:
	movl	%eax, -888(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-888(%rbp), %eax
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L328:
	leaq	-720(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%rcx, -936(%rbp)
	movq	%r8, -920(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-920(%rbp), %r8
	movq	-936(%rbp), %rcx
	movq	%rax, -720(%rbp)
	movq	%rax, %rdi
	movq	-832(%rbp), %rax
	movq	%rax, -704(%rbp)
.L242:
	movq	%rbx, %rdx
	movq	%r8, %rsi
	movq	%rcx, -920(%rbp)
	call	memcpy@PLT
	movq	-832(%rbp), %rbx
	movq	-720(%rbp), %rax
	movq	-920(%rbp), %rcx
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L329:
	movq	-712(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L246
	cmpq	$1, %rdx
	je	.L340
	movq	%rcx, %rsi
	movq	%rcx, -920(%rbp)
	call	memcpy@PLT
	movq	-712(%rbp), %rdx
	movq	-784(%rbp), %rdi
	movq	-920(%rbp), %rcx
.L246:
	movq	%rdx, -776(%rbp)
	movb	$0, (%rdi,%rdx)
	movq	-720(%rbp), %rdi
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L334:
	leaq	-752(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%rcx, -936(%rbp)
	movq	%r8, -920(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-920(%rbp), %r8
	movq	-936(%rbp), %rcx
	movq	%rax, -752(%rbp)
	movq	%rax, %rdi
	movq	-832(%rbp), %rax
	movq	%rax, -736(%rbp)
.L226:
	movq	%rbx, %rdx
	movq	%r8, %rsi
	movq	%rcx, -920(%rbp)
	call	memcpy@PLT
	movq	-832(%rbp), %rbx
	movq	-752(%rbp), %rax
	movq	-920(%rbp), %rcx
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L319:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L253
	.p2align 4,,10
	.p2align 3
.L335:
	movq	-744(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L230
	cmpq	$1, %rdx
	je	.L341
	movq	%rcx, %rsi
	movq	%rcx, -920(%rbp)
	call	memcpy@PLT
	movq	-744(%rbp), %rdx
	movq	-784(%rbp), %rdi
	movq	-920(%rbp), %rcx
.L230:
	movq	%rdx, -776(%rbp)
	movb	$0, (%rdi,%rdx)
	movq	-752(%rbp), %rdi
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L330:
	movq	%rax, %xmm0
	movq	%rsi, %xmm5
	movq	%rdx, -784(%rbp)
	punpcklqdq	%xmm5, %xmm0
	movups	%xmm0, -776(%rbp)
.L250:
	movq	%rcx, -720(%rbp)
	leaq	-704(%rbp), %rcx
	movq	%rcx, %rdi
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L336:
	movq	%rax, %xmm0
	movq	%rsi, %xmm6
	movq	%rdx, -784(%rbp)
	punpcklqdq	%xmm6, %xmm0
	movups	%xmm0, -776(%rbp)
.L234:
	movq	%rcx, -752(%rbp)
	leaq	-736(%rbp), %rcx
	movq	%rcx, %rdi
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L337:
	movzbl	-672(%rbp), %eax
	movb	%al, (%rdi)
	movq	-680(%rbp), %rdx
	movq	-784(%rbp), %rdi
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L327:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L238
.L332:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L223
.L340:
	movzbl	-704(%rbp), %eax
	movb	%al, (%rdi)
	movq	-712(%rbp), %rdx
	movq	-784(%rbp), %rdi
	jmp	.L246
.L341:
	movzbl	-736(%rbp), %eax
	movb	%al, (%rdi)
	movq	-744(%rbp), %rdx
	movq	-784(%rbp), %rdi
	jmp	.L230
.L325:
	call	__stack_chk_fail@PLT
.L339:
	movq	%rcx, %rdi
	jmp	.L226
.L333:
	movq	%rbx, %rdi
	jmp	.L256
.L338:
	movq	%rcx, %rdi
	jmp	.L242
	.cfi_endproc
.LFE18208:
	.size	_ZN2v88internal10JSCollator15ResolvedOptionsEPNS0_7IsolateENS0_6HandleIS1_EE, .-_ZN2v88internal10JSCollator15ResolvedOptionsEPNS0_7IsolateENS0_6HandleIS1_EE
	.section	.rodata._ZN2v88internal10JSCollator3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6ObjectEEES8_.str1.1,"aMS",@progbits,1
.LC21:
	.string	"Intl.Collator"
.LC22:
	.string	"usage"
.LC23:
	.string	"unreachable code"
.LC24:
	.string	"numeric"
	.section	.rodata._ZN2v88internal10JSCollator3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6ObjectEEES8_.str1.8,"aMS",@progbits,1
	.align 8
.LC25:
	.string	"Failed to create ICU collator, are ICU data files missing?"
	.section	.rodata._ZN2v88internal10JSCollator3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6ObjectEEES8_.str1.1
.LC26:
	.string	"sensitivity"
.LC27:
	.string	"ignorePunctuation"
.LC28:
	.string	"true"
	.section	.text._ZN2v88internal10JSCollator3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6ObjectEEES8_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSCollator3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6ObjectEEES8_
	.type	_ZN2v88internal10JSCollator3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6ObjectEEES8_, @function
_ZN2v88internal10JSCollator3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6ObjectEEES8_:
.LFB18219:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	xorl	%ecx, %ecx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	-896(%rbp), %rdi
	pushq	%rbx
	subq	$1096, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -1032(%rbp)
	movq	%r12, %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal4Intl22CanonicalizeLocaleListB5cxx11EPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEb@PLT
	cmpb	$0, -896(%rbp)
	jne	.L343
	xorl	%r12d, %r12d
.L344:
	movq	-880(%rbp), %rbx
	movq	-888(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L462
	.p2align 4,,10
	.p2align 3
.L466:
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L463
	call	_ZdlPv@PLT
	addq	$32, %r13
	cmpq	%rbx, %r13
	jne	.L466
.L464:
	movq	-888(%rbp), %r13
.L462:
	testq	%r13, %r13
	je	.L467
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L467:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L602
	addq	$1096, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L463:
	.cfi_restore_state
	addq	$32, %r13
	cmpq	%r13, %rbx
	jne	.L466
	jmp	.L464
	.p2align 4,,10
	.p2align 3
.L343:
	movq	-880(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	-888(%rbp), %r9
	movq	$0, -944(%rbp)
	movaps	%xmm0, -960(%rbp)
	movq	%rax, %r15
	movq	%rax, -1024(%rbp)
	subq	%r9, %r15
	movq	%r15, %rax
	sarq	$5, %rax
	je	.L603
	movabsq	$288230376151711743, %rdx
	cmpq	%rdx, %rax
	ja	.L604
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	-888(%rbp), %r9
	movq	%rax, %rbx
	movq	-880(%rbp), %rax
	movq	%rax, -1024(%rbp)
.L346:
	movq	%rbx, %xmm0
	addq	%rbx, %r15
	punpcklqdq	%xmm0, %xmm0
	movq	%r15, -944(%rbp)
	movaps	%xmm0, -960(%rbp)
	cmpq	%r9, -1024(%rbp)
	je	.L348
	leaq	-864(%rbp), %rax
	movq	%r12, -1048(%rbp)
	movq	%r9, %r15
	movq	%rbx, %r12
	movq	%r13, -1040(%rbp)
	movq	%rax, %rbx
	movq	%r14, -1056(%rbp)
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L350:
	cmpq	$1, %r13
	jne	.L352
	movzbl	(%r14), %eax
	movb	%al, 16(%r12)
.L353:
	movq	%r13, 8(%r12)
	addq	$32, %r15
	addq	$32, %r12
	movb	$0, (%rdi,%r13)
	cmpq	%r15, -1024(%rbp)
	je	.L605
.L354:
	leaq	16(%r12), %rdi
	movq	%rdi, (%r12)
	movq	(%r15), %r14
	movq	8(%r15), %r13
	movq	%r14, %rax
	addq	%r13, %rax
	je	.L349
	testq	%r14, %r14
	je	.L376
.L349:
	movq	%r13, -864(%rbp)
	cmpq	$15, %r13
	jbe	.L350
	movq	%r12, %rdi
	xorl	%edx, %edx
	movq	%rbx, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-864(%rbp), %rax
	movq	%rax, 16(%r12)
.L351:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-864(%rbp), %r13
	movq	(%r12), %rdi
	jmp	.L353
	.p2align 4,,10
	.p2align 3
.L352:
	testq	%r13, %r13
	je	.L353
	jmp	.L351
	.p2align 4,,10
	.p2align 3
.L605:
	movq	%r12, %rbx
	movq	-1040(%rbp), %r13
	movq	-1048(%rbp), %r12
	movq	-1056(%rbp), %r14
.L348:
	movq	%rbx, -952(%rbp)
	movq	(%r14), %rax
	cmpq	88(%r12), %rax
	je	.L606
	testb	$1, %al
	jne	.L357
.L359:
	leaq	.LC21(%rip), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object12ToObjectImplEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L358
.L356:
	movl	$8, %edi
	leaq	-864(%rbp), %rbx
	call	_Znwm@PLT
	movl	$16, %edi
	movq	%rax, %r15
	movabsq	$4294967296, %rax
	movq	%rax, (%r15)
	call	_Znwm@PLT
	leaq	.LC13(%rip), %rcx
	movl	$16, %edi
	movq	$0, -928(%rbp)
	movq	%rax, %r14
	movq	%rcx, %xmm0
	leaq	.LC15(%rip), %rax
	movq	$0, -848(%rbp)
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%r14)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -864(%rbp)
	call	_Znwm@PLT
	movdqu	(%r14), %xmm2
	movq	%r12, %rdi
	movq	%rbx, %rcx
	leaq	16(%rax), %rdx
	movq	%rax, -864(%rbp)
	leaq	.LC21(%rip), %r8
	movq	%r13, %rsi
	movups	%xmm2, (%rax)
	leaq	-928(%rbp), %rax
	movq	%rdx, -848(%rbp)
	movq	%rax, %r9
	movq	%rdx, -856(%rbp)
	leaq	.LC22(%rip), %rdx
	movq	%rax, -1024(%rbp)
	call	_ZN2v88internal4Intl15GetStringOptionEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEEPKcSt6vectorIS8_SaIS8_EES8_PSt10unique_ptrIA_cSt14default_deleteISD_EE@PLT
	movq	-864(%rbp), %rdi
	movl	%eax, %edx
	testq	%rdi, %rdi
	je	.L361
	movl	%eax, -1048(%rbp)
	movb	%al, -1040(%rbp)
	call	_ZdlPv@PLT
	movl	-1048(%rbp), %eax
	movzbl	-1040(%rbp), %edx
.L361:
	movq	-928(%rbp), %rdi
	testb	%dl, %dl
	je	.L362
	shrw	$8, %ax
	jne	.L607
	testq	%rdi, %rdi
	je	.L595
	call	_ZdaPv@PLT
.L595:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	movl	$0, -1040(%rbp)
.L366:
	leaq	.LC21(%rip), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4Intl16GetLocaleMatcherEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEEPKc@PLT
	testb	%al, %al
	je	.L358
	sarq	$32, %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	-994(%rbp), %r8
	leaq	.LC21(%rip), %rcx
	movq	%rax, -1056(%rbp)
	leaq	.LC24(%rip), %rdx
	call	_ZN2v88internal4Intl13GetBoolOptionEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEEPKcS8_Pb@PLT
	movw	%ax, -1064(%rbp)
	testb	%al, %al
	jne	.L608
	.p2align 4,,10
	.p2align 3
.L358:
	xorl	%r12d, %r12d
.L360:
	movq	-952(%rbp), %rbx
	movq	-960(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L456
	.p2align 4,,10
	.p2align 3
.L460:
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L457
	call	_ZdlPv@PLT
	addq	$32, %r13
	cmpq	%r13, %rbx
	jne	.L460
.L458:
	movq	-960(%rbp), %r13
.L456:
	testq	%r13, %r13
	je	.L344
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	jmp	.L344
	.p2align 4,,10
	.p2align 3
.L457:
	addq	$32, %r13
	cmpq	%r13, %rbx
	jne	.L460
	jmp	.L458
	.p2align 4,,10
	.p2align 3
.L362:
	testq	%rdi, %rdi
	jne	.L609
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	jmp	.L358
	.p2align 4,,10
	.p2align 3
.L603:
	xorl	%ebx, %ebx
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L607:
	movq	(%r14), %rsi
	movq	%rdi, -1040(%rbp)
	call	strcmp@PLT
	movq	-1040(%rbp), %rdi
	testl	%eax, %eax
	je	.L482
	movq	8(%r14), %rsi
	call	strcmp@PLT
	movq	-1040(%rbp), %rdi
	testl	%eax, %eax
	je	.L610
.L365:
	leaq	.LC23(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L609:
	call	_ZdaPv@PLT
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	jmp	.L358
	.p2align 4,,10
	.p2align 3
.L606:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory24NewJSObjectWithNullProtoENS0_14AllocationTypeE@PLT
	movq	%rax, %r13
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L357:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L359
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L608:
	leaq	.LC21(%rip), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4Intl12GetCaseFirstEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEEPKc@PLT
	movb	%al, -1097(%rbp)
	testb	%al, %al
	je	.L358
	sarq	$32, %rax
	leaq	-368(%rbp), %r15
	movq	$2, -360(%rbp)
	movq	%rax, -1096(%rbp)
	movl	%eax, -1104(%rbp)
	leaq	-352(%rbp), %rax
	movq	%rax, -1128(%rbp)
	movq	%rax, -368(%rbp)
	movl	$28515, %eax
	movw	%ax, -352(%rbp)
	leaq	-320(%rbp), %rax
	movq	%rax, -336(%rbp)
	movl	$28267, %eax
	movw	%ax, -320(%rbp)
	leaq	-288(%rbp), %rax
	movq	%rax, -304(%rbp)
	movl	$26219, %eax
	movq	%r15, -1072(%rbp)
	movb	$0, -350(%rbp)
	movq	$2, -328(%rbp)
	movb	$0, -318(%rbp)
	movq	$2, -296(%rbp)
	movb	$0, -286(%rbp)
	movl	$0, -856(%rbp)
	movq	$0, -848(%rbp)
	movq	$0, -824(%rbp)
	movw	%ax, -288(%rbp)
	leaq	-856(%rbp), %rax
	movq	%rax, -1048(%rbp)
	movq	%rax, -840(%rbp)
	movq	%rax, -832(%rbp)
	xorl	%eax, %eax
	movq	%r13, -1112(%rbp)
	movq	%r15, %r13
	movq	%r12, -1120(%rbp)
	movq	%rbx, %r12
	.p2align 4,,10
	.p2align 3
.L383:
	testq	%rax, %rax
	je	.L367
	movq	-832(%rbp), %rbx
	movq	8(%r13), %r15
	movq	40(%rbx), %r14
	movq	%r15, %rdx
	cmpq	%r15, %r14
	cmovbe	%r14, %rdx
	testq	%rdx, %rdx
	je	.L368
	movq	32(%rbx), %rdi
	movq	0(%r13), %rsi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L369
.L368:
	subq	%r15, %r14
	movl	$2147483648, %eax
	cmpq	%rax, %r14
	jge	.L367
	movabsq	$-2147483649, %rax
	cmpq	%rax, %r14
	jle	.L370
	movl	%r14d, %eax
.L369:
	testl	%eax, %eax
	js	.L370
	.p2align 4,,10
	.p2align 3
.L367:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE24_M_get_insert_unique_posERKS5_
	movq	%rdx, %rbx
	testq	%rdx, %rdx
	je	.L372
	testq	%rax, %rax
	setne	%dil
.L371:
	cmpq	-1048(%rbp), %rbx
	sete	%r14b
	orb	%dil, %r14b
	je	.L611
.L373:
	movl	$64, %edi
	call	_Znwm@PLT
	movq	0(%r13), %r8
	movq	8(%r13), %r11
	leaq	48(%rax), %rdi
	movq	%rax, %r15
	movq	%rdi, 32(%rax)
	movq	%r8, %rax
	addq	%r11, %rax
	je	.L492
	testq	%r8, %r8
	je	.L376
.L492:
	movq	%r11, -928(%rbp)
	cmpq	$15, %r11
	ja	.L612
	cmpq	$1, %r11
	jne	.L380
	movzbl	(%r8), %eax
	movb	%al, 48(%r15)
.L381:
	movq	%r11, 40(%r15)
	movq	-1048(%rbp), %rcx
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movb	$0, (%rdi,%r11)
	movzbl	%r14b, %edi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, -824(%rbp)
.L372:
	addq	$32, %r13
	leaq	-272(%rbp), %rax
	cmpq	%rax, %r13
	je	.L382
	movq	-824(%rbp), %rax
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L370:
	xorl	%edi, %edi
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L380:
	testq	%r11, %r11
	je	.L381
	.p2align 4,,10
	.p2align 3
.L379:
	movq	%r11, %rdx
	movq	%r8, %rsi
	call	memcpy@PLT
	movq	-928(%rbp), %r11
	movq	32(%r15), %rdi
	jmp	.L381
	.p2align 4,,10
	.p2align 3
.L612:
	movq	-1024(%rbp), %rsi
	leaq	32(%r15), %rdi
	xorl	%edx, %edx
	movq	%r11, -1088(%rbp)
	movq	%r8, -1080(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-1080(%rbp), %r8
	movq	-1088(%rbp), %r11
	movq	%rax, 32(%r15)
	movq	%rax, %rdi
	movq	-928(%rbp), %rax
	movq	%rax, 48(%r15)
	jmp	.L379
	.p2align 4,,10
	.p2align 3
.L382:
	movq	%r13, %r15
	movq	%r12, %rbx
	movq	-1112(%rbp), %r13
	movq	-1120(%rbp), %r12
	movq	%r15, %r14
.L387:
	subq	$32, %r14
	movq	(%r14), %rdi
	leaq	16(%r14), %rax
	cmpq	%rax, %rdi
	je	.L384
	call	_ZdlPv@PLT
	cmpq	-1072(%rbp), %r14
	jne	.L387
.L385:
	movzbl	_ZZN2v88internal10JSCollator19GetAvailableLocalesB5cxx11EvE17available_locales(%rip), %eax
	cmpb	$2, %al
	je	.L388
	leaq	_ZN2v84base16LazyInstanceImplINS_8internal4Intl16AvailableLocalesIN6icu_678CollatorENS2_12_GLOBAL__N_19CheckCollEEENS0_32StaticallyAllocatedInstanceTraitIS9_EENS0_21DefaultConstructTraitIS9_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS9_EEE12InitInstanceEPv(%rip), %rax
	movq	-1072(%rbp), %r14
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rcx
	movq	%rax, -368(%rbp)
	leaq	8+_ZZN2v88internal10JSCollator19GetAvailableLocalesB5cxx11EvE17available_locales(%rip), %rax
	movq	%rcx, %xmm0
	leaq	_ZZN2v88internal10JSCollator19GetAvailableLocalesB5cxx11EvE17available_locales(%rip), %rdi
	movq	%rax, -360(%rbp)
	leaq	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%r14, %rsi
	movq	%rax, %xmm4
	punpcklqdq	%xmm4, %xmm0
	movaps	%xmm0, -352(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-352(%rbp), %rax
	testq	%rax, %rax
	je	.L388
	movl	$3, %edx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	*%rax
.L388:
	movl	-1056(%rbp), %r8d
	movq	-1072(%rbp), %rdi
	movq	%rbx, %r9
	movq	%r12, %rsi
	leaq	-960(%rbp), %rcx
	leaq	16+_ZZN2v88internal10JSCollator19GetAvailableLocalesB5cxx11EvE17available_locales(%rip), %rdx
	call	_ZN2v88internal4Intl13ResolveLocaleEPNS0_7IsolateERKSt3setINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessISA_ESaISA_EERKSt6vectorISA_SD_ENS1_13MatcherOptionESG_@PLT
	leaq	-816(%rbp), %rax
	leaq	-336(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rsi, -1072(%rbp)
	movq	%rax, -1048(%rbp)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	cmpl	$1, -1040(%rbp)
	je	.L613
.L390:
	leaq	-988(%rbp), %rax
	movq	-1048(%rbp), %rdi
	movl	$0, -988(%rbp)
	movq	%rax, %rsi
	movq	%rax, -1056(%rbp)
	call	_ZN6icu_678Collator14createInstanceERKNS_6LocaleER10UErrorCode@PLT
	movl	-988(%rbp), %r14d
	movq	%rax, %r15
	testl	%r14d, %r14d
	jg	.L392
	testq	%rax, %rax
	je	.L614
.L394:
	movzwl	-1064(%rbp), %eax
	movl	$0, -988(%rbp)
	shrw	$8, %ax
	je	.L615
	movl	$0, -984(%rbp)
	xorl	%edx, %edx
	movq	(%r15), %rax
	movl	$7, %esi
	cmpb	$0, -994(%rbp)
	leaq	-984(%rbp), %rcx
	movq	%r15, %rdi
	setne	%dl
	addl	$16, %edx
	call	*184(%rax)
	movl	-984(%rbp), %r10d
	testl	%r10d, %r10d
	jg	.L398
.L399:
	cmpl	$3, -1096(%rbp)
	je	.L616
	movl	-1096(%rbp), %edx
	movl	$2, %esi
	movq	%r15, %rdi
	leaq	CSWTCH.331(%rip), %rax
	movl	$0, -976(%rbp)
	leaq	-976(%rbp), %rcx
	movl	(%rax,%rdx,4), %edx
	movq	(%r15), %rax
	call	*184(%rax)
	movl	-976(%rbp), %r8d
	testl	%r8d, %r8d
	jg	.L398
.L401:
	movl	$4, %esi
	movq	-1056(%rbp), %rcx
	movl	$17, %edx
	movq	%r15, %rdi
	movl	$0, -988(%rbp)
	movq	(%r15), %rax
	call	*184(%rax)
	movl	-988(%rbp), %esi
	testl	%esi, %esi
	jg	.L398
	movl	$16, %edi
	call	_Znwm@PLT
	movl	$32, %edi
	movabsq	$12884901890, %rdx
	movq	%rax, %rcx
	movq	%rax, -1064(%rbp)
	movabsq	$4294967296, %rax
	movq	%rdx, 8(%rcx)
	movq	%rax, (%rcx)
	leaq	.LC12(%rip), %rax
	leaq	.LC10(%rip), %rcx
	movq	%rax, %xmm3
	movq	%rcx, %xmm0
	leaq	.LC11(%rip), %rax
	punpcklqdq	%xmm3, %xmm0
	leaq	.LC9(%rip), %rcx
	movq	%rax, %xmm4
	movaps	%xmm0, -592(%rbp)
	movq	%rcx, %xmm0
	punpcklqdq	%xmm4, %xmm0
	movaps	%xmm0, -576(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movdqa	-592(%rbp), %xmm5
	movdqa	-576(%rbp), %xmm6
	movq	%rax, %r14
	movaps	%xmm0, -928(%rbp)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movq	$0, -976(%rbp)
	movq	$0, -912(%rbp)
	call	_Znwm@PLT
	movdqu	(%r14), %xmm7
	movq	%r12, %rdi
	movq	%r13, %rsi
	movdqu	16(%r14), %xmm3
	leaq	32(%rax), %rdx
	movq	%rax, -928(%rbp)
	leaq	.LC21(%rip), %r8
	movups	%xmm7, (%rax)
	movq	-1024(%rbp), %rcx
	movups	%xmm3, 16(%rax)
	leaq	-976(%rbp), %rax
	movq	%rdx, -912(%rbp)
	movq	%rax, %r9
	movq	%rdx, -920(%rbp)
	leaq	.LC26(%rip), %rdx
	movq	%rax, -1080(%rbp)
	call	_ZN2v88internal4Intl15GetStringOptionEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEEPKcSt6vectorIS8_SaIS8_EES8_PSt10unique_ptrIA_cSt14default_deleteISD_EE@PLT
	movq	-928(%rbp), %rdi
	movl	%eax, %edx
	testq	%rdi, %rdi
	je	.L404
	movl	%eax, -1096(%rbp)
	movb	%al, -1088(%rbp)
	call	_ZdlPv@PLT
	movl	-1096(%rbp), %eax
	movzbl	-1088(%rbp), %edx
.L404:
	movq	-976(%rbp), %rdi
	testb	%dl, %dl
	je	.L405
	shrw	$8, %ax
	je	.L406
	movq	(%r14), %rsi
	movq	%rdi, -1088(%rbp)
	call	strcmp@PLT
	movq	-1088(%rbp), %rdi
	testl	%eax, %eax
	je	.L488
	movq	8(%r14), %rsi
	call	strcmp@PLT
	movq	-1088(%rbp), %rdi
	testl	%eax, %eax
	je	.L489
	movq	16(%r14), %rsi
	call	strcmp@PLT
	movq	-1088(%rbp), %rdi
	testl	%eax, %eax
	je	.L490
	movq	24(%r14), %rsi
	call	strcmp@PLT
	movq	-1088(%rbp), %rdi
	testl	%eax, %eax
	jne	.L365
	movl	$3, %eax
.L407:
	movq	-1064(%rbp), %rcx
	movl	(%rcx,%rax,4), %eax
	movl	%eax, -1088(%rbp)
	call	_ZdaPv@PLT
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	movq	-1064(%rbp), %rdi
	call	_ZdlPv@PLT
	movl	-1088(%rbp), %eax
	cmpl	$4, %eax
	je	.L408
	cmpl	$1, %eax
	je	.L415
	jle	.L617
	cmpl	$2, %eax
	jne	.L618
	movq	(%r15), %rax
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	*152(%rax)
	movq	-1056(%rbp), %rcx
	movl	$17, %edx
	movq	%r15, %rdi
	movl	$0, -988(%rbp)
	movq	(%r15), %rax
	movl	$3, %esi
	call	*184(%rax)
	cmpl	$0, -988(%rbp)
	jg	.L398
.L414:
	leaq	-993(%rbp), %r8
	leaq	.LC21(%rip), %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	.LC27(%rip), %rdx
	call	_ZN2v88internal4Intl13GetBoolOptionEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEEPKcS8_Pb@PLT
	testb	%al, %al
	je	.L420
	shrw	$8, %ax
	je	.L421
	cmpb	$0, -993(%rbp)
	je	.L421
	movq	-1056(%rbp), %rcx
	movl	$20, %edx
	movl	$1, %esi
	movq	%r15, %rdi
	movl	$0, -988(%rbp)
	movq	(%r15), %rax
	call	*184(%rax)
	cmpl	$0, -988(%rbp)
	jg	.L398
.L421:
	movq	-1080(%rbp), %rsi
	movq	-1024(%rbp), %rdi
	movq	%r15, -976(%rbp)
	call	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1IN6icu_678CollatorESt14default_deleteIS5_EEEOSt10unique_ptrIT_T0_E
	movq	-928(%rbp), %r14
	testq	%r14, %r14
	je	.L422
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rax
	testq	%rax, %rax
	je	.L423
	lock addl	$1, 8(%r14)
	movq	-928(%rbp), %r13
	testq	%r13, %r13
	je	.L422
	movl	$-1, %edx
	lock xaddl	%edx, 8(%r13)
.L426:
	cmpl	$1, %edx
	je	.L619
.L422:
	movq	32(%r12), %rax
	subq	48(%r12), %rax
	cmpq	$33554432, %rax
	jg	.L620
.L429:
	movq	%r14, %xmm5
	movq	%r15, %xmm0
	movl	$16, %edi
	punpcklqdq	%xmm5, %xmm0
	movaps	%xmm0, -1024(%rbp)
	call	_Znwm@PLT
	movdqa	-1024(%rbp), %xmm0
	movq	%rax, %r13
	movups	%xmm0, (%rax)
	testq	%r14, %r14
	je	.L430
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L431
	lock addl	$1, 8(%r14)
.L430:
	movl	$48, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %r15
	movups	%xmm0, 8(%rax)
	movq	%r13, 24(%rax)
	movq	%r15, %rsi
	movq	$0, (%rax)
	leaq	_ZN2v88internal7ManagedIN6icu_678CollatorEE10DestructorEPv(%rip), %rax
	movq	%rax, 32(%r15)
	movq	$0, 40(%r15)
	call	_ZN2v88internal7Factory10NewForeignEmNS0_14AllocationTypeE@PLT
	movq	41152(%r12), %rdi
	movq	(%rax), %rsi
	movq	%rax, %r13
	call	_ZN2v88internal13GlobalHandles6CreateENS0_6ObjectE@PLT
	movq	_ZN2v88internal22ManagedObjectFinalizerERKNS_16WeakCallbackInfoIvEE@GOTPCREL(%rip), %rdx
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movq	%rax, 40(%r15)
	movq	%rax, %rdi
	call	_ZN2v88internal13GlobalHandles8MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate28RegisterManagedPtrDestructorEPNS0_20ManagedPtrDestructorE@PLT
	testq	%r14, %r14
	je	.L433
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rax
	testq	%rax, %rax
	je	.L434
	orl	$-1, %edx
	lock xaddl	%edx, 8(%r14)
.L435:
	cmpl	$1, %edx
	je	.L621
.L433:
	movq	-976(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L439
	movq	(%rdi), %rax
	call	*8(%rax)
.L439:
	movq	-1032(%rbp), %rsi
	movq	(%rsi), %rax
	movl	15(%rax), %eax
	testl	$2097152, %eax
	je	.L440
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$2, %edx
	call	_ZN2v88internal7Factory22NewSlowJSObjectFromMapENS0_6HandleINS0_3MapEEEiNS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE@PLT
	movq	%rax, %r12
.L441:
	movq	(%r12), %r14
	movq	0(%r13), %r13
	movq	%r13, 23(%r14)
	leaq	23(%r14), %r15
	testb	$1, %r13b
	je	.L412
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -1024(%rbp)
	testl	$262144, %eax
	je	.L443
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-1024(%rbp), %rcx
	movq	8(%rcx), %rax
.L443:
	testb	$24, %al
	je	.L412
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L412
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
.L412:
	movq	-1048(%rbp), %rdi
	leaq	-112(%rbp), %r14
	call	_ZN6icu_676LocaleD1Ev@PLT
	movq	-96(%rbp), %r15
	testq	%r15, %r15
	je	.L450
.L445:
	movq	24(%r15), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	movq	64(%r15), %rdi
	leaq	80(%r15), %rax
	movq	16(%r15), %r13
	cmpq	%rax, %rdi
	je	.L448
	call	_ZdlPv@PLT
.L448:
	movq	32(%r15), %rdi
	leaq	48(%r15), %rax
	cmpq	%rax, %rdi
	je	.L449
	call	_ZdlPv@PLT
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L450
.L451:
	movq	%r13, %r15
	jmp	.L445
	.p2align 4,,10
	.p2align 3
.L384:
	cmpq	-1072(%rbp), %r14
	jne	.L387
	jmp	.L385
	.p2align 4,,10
	.p2align 3
.L611:
	movq	8(%r13), %rcx
	movq	40(%rbx), %r15
	cmpq	%r15, %rcx
	movq	%r15, %rdx
	cmovbe	%rcx, %rdx
	testq	%rdx, %rdx
	je	.L374
	movq	32(%rbx), %rsi
	movq	0(%r13), %rdi
	movq	%rcx, -1080(%rbp)
	call	memcmp@PLT
	movq	-1080(%rbp), %rcx
	testl	%eax, %eax
	jne	.L375
.L374:
	subq	%r15, %rcx
	movl	$2147483648, %eax
	cmpq	%rax, %rcx
	jge	.L373
	movabsq	$-2147483649, %rax
	cmpq	%rax, %rcx
	jle	.L483
	movl	%ecx, %eax
.L375:
	shrl	$31, %eax
	movl	%eax, %r14d
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L449:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L451
.L450:
	movq	-1072(%rbp), %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movq	-368(%rbp), %rdi
	cmpq	-1128(%rbp), %rdi
	je	.L447
	call	_ZdlPv@PLT
.L447:
	movq	-848(%rbp), %r13
	testq	%r13, %r13
	je	.L360
.L452:
	movq	24(%r13), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r13), %rdi
	leaq	48(%r13), %rax
	movq	16(%r13), %r14
	cmpq	%rax, %rdi
	je	.L453
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%r14, %r14
	je	.L360
.L455:
	movq	%r14, %r13
	jmp	.L452
	.p2align 4,,10
	.p2align 3
.L453:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%r14, %r14
	jne	.L455
	jmp	.L360
.L615:
	leaq	-112(%rbp), %r14
	leaq	-576(%rbp), %rdx
	movl	$28267, %r9d
	movq	$2, -584(%rbp)
	movq	%r14, %rdi
	leaq	-592(%rbp), %rsi
	movq	%rdx, -592(%rbp)
	movq	%rdx, -1064(%rbp)
	movw	%r9w, -576(%rbp)
	movb	$0, -574(%rbp)
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE4findERS7_
	movq	-592(%rbp), %rdi
	movq	-1064(%rbp), %rdx
	movq	%rax, %r14
	cmpq	%rdx, %rdi
	je	.L400
	call	_ZdlPv@PLT
.L400:
	leaq	-104(%rbp), %rax
	cmpq	%rax, %r14
	je	.L399
	leaq	64(%r14), %rdi
	leaq	.LC28(%rip), %rsi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	xorl	%edx, %edx
	movl	$7, %esi
	movq	%r15, %rdi
	testl	%eax, %eax
	leaq	-980(%rbp), %rcx
	movl	$0, -980(%rbp)
	movq	(%r15), %r8
	sete	%dl
	addl	$16, %edx
	call	*184(%r8)
	movl	-980(%rbp), %eax
	testl	%eax, %eax
	jle	.L399
.L398:
	leaq	.LC17(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L405:
	testq	%rdi, %rdi
	jne	.L411
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	movq	-1064(%rbp), %rdi
	call	_ZdlPv@PLT
.L420:
	movq	(%r15), %rax
	xorl	%r12d, %r12d
	movq	%r15, %rdi
	call	*8(%rax)
	jmp	.L412
.L392:
	movq	-1048(%rbp), %rdi
	leaq	-592(%rbp), %r14
	movl	$0, -988(%rbp)
	call	_ZNK6icu_676Locale11getBaseNameEv@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movq	-1056(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN6icu_678Collator14createInstanceERKNS_6LocaleER10UErrorCode@PLT
	movq	%rax, -1080(%rbp)
	testq	%r15, %r15
	je	.L484
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*8(%rax)
	movq	-1080(%rbp), %r15
.L395:
	movl	-988(%rbp), %r11d
	testl	%r11d, %r11d
	jg	.L396
	testq	%r15, %r15
	je	.L396
	movq	%r14, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	jmp	.L394
.L613:
	movq	-1024(%rbp), %rdi
	leaq	.LC15(%rip), %rsi
	movl	$0, -992(%rbp)
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	leaq	-976(%rbp), %rdi
	leaq	.LC18(%rip), %rsi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movq	-928(%rbp), %rcx
	movq	-920(%rbp), %r8
	leaq	-992(%rbp), %r9
	movq	-976(%rbp), %rsi
	movq	-968(%rbp), %rdx
	movq	-1048(%rbp), %rdi
	call	_ZN6icu_676Locale22setUnicodeKeywordValueENS_11StringPieceES1_R10UErrorCode@PLT
	movl	-992(%rbp), %r15d
	testl	%r15d, %r15d
	jle	.L390
	jmp	.L398
.L482:
	xorl	%eax, %eax
.L364:
	movl	(%r15,%rax,4), %eax
	movl	%eax, -1040(%rbp)
	call	_ZdaPv@PLT
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	jmp	.L366
.L610:
	movl	$1, %eax
	jmp	.L364
.L616:
	movl	$26219, %edi
	leaq	-112(%rbp), %r14
	leaq	-576(%rbp), %rdx
	movq	$2, -584(%rbp)
	movw	%di, -576(%rbp)
	leaq	-592(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rdx, -592(%rbp)
	movq	%rdx, -1064(%rbp)
	movb	$0, -574(%rbp)
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE4findERS7_
	movq	-592(%rbp), %rdi
	movq	-1064(%rbp), %rdx
	movq	%rax, %r14
	cmpq	%rdx, %rdi
	je	.L402
	call	_ZdlPv@PLT
.L402:
	leaq	-104(%rbp), %rax
	cmpq	%rax, %r14
	je	.L401
	movq	64(%r14), %rdx
	movl	$6, %ecx
	leaq	.LC8(%rip), %rdi
	movq	%rdx, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L486
	movl	$6, %ecx
	leaq	.LC7(%rip), %rdi
	movq	%rdx, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L487
	movl	$6, %ecx
	leaq	.LC6(%rip), %rdi
	movq	%rdx, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	movl	$2, %eax
	cmovne	-1104(%rbp), %eax
	movl	%eax, -1104(%rbp)
.L403:
	movl	-1104(%rbp), %edx
	movl	$2, %esi
	movq	%r15, %rdi
	leaq	CSWTCH.331(%rip), %rax
	movl	$0, -928(%rbp)
	movq	-1024(%rbp), %rcx
	movl	(%rax,%rdx,4), %edx
	movq	(%r15), %rax
	call	*184(%rax)
	movl	-928(%rbp), %edx
	testl	%edx, %edx
	jle	.L401
	jmp	.L398
	.p2align 4,,10
	.p2align 3
.L618:
	cmpl	$3, %eax
	jne	.L414
.L413:
	movq	(%r15), %rax
	movl	$2, %esi
	movq	%r15, %rdi
	call	*152(%rax)
	jmp	.L414
.L617:
	testl	%eax, %eax
	jne	.L414
	movq	(%r15), %rax
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	*152(%rax)
	jmp	.L414
.L614:
	movq	-1048(%rbp), %rdi
	leaq	-592(%rbp), %r14
	movl	$0, -988(%rbp)
	call	_ZNK6icu_676Locale11getBaseNameEv@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movq	-1056(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN6icu_678Collator14createInstanceERKNS_6LocaleER10UErrorCode@PLT
	movq	%rax, %r15
	jmp	.L395
.L406:
	testq	%rdi, %rdi
	jne	.L410
.L601:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	movq	-1064(%rbp), %rdi
	call	_ZdlPv@PLT
.L408:
	movl	-1040(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L413
	jmp	.L414
.L411:
	call	_ZdaPv@PLT
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	movq	-1064(%rbp), %rdi
	call	_ZdlPv@PLT
	jmp	.L420
.L440:
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory18NewJSObjectFromMapENS0_6HandleINS0_3MapEEENS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE@PLT
	movq	%rax, %r12
	jmp	.L441
.L620:
	movq	%r12, %rdi
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	jmp	.L429
.L431:
	addl	$1, 8(%r14)
	jmp	.L430
.L415:
	movq	(%r15), %rax
	movl	$1, %esi
	movq	%r15, %rdi
	call	*152(%rax)
	jmp	.L414
.L486:
	movl	$0, -1104(%rbp)
	jmp	.L403
.L423:
	addl	$1, 8(%r14)
	movq	-928(%rbp), %r13
	movl	8(%r13), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 8(%r13)
	jmp	.L426
.L434:
	movl	8(%r14), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 8(%r14)
	jmp	.L435
.L487:
	movl	$1, -1104(%rbp)
	jmp	.L403
.L376:
	leaq	.LC19(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L602:
	call	__stack_chk_fail@PLT
.L621:
	movq	(%r14), %rdx
	movq	%rax, -1024(%rbp)
	movq	%r14, %rdi
	call	*16(%rdx)
	movq	-1024(%rbp), %rax
	testq	%rax, %rax
	je	.L437
	orl	$-1, %eax
	lock xaddl	%eax, 12(%r14)
.L438:
	subl	$1, %eax
	jne	.L433
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	jmp	.L433
.L619:
	movq	0(%r13), %rdx
	movq	%rax, -1024(%rbp)
	movq	%r13, %rdi
	call	*16(%rdx)
	movq	-1024(%rbp), %rax
	testq	%rax, %rax
	je	.L427
	orl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L428:
	subl	$1, %eax
	jne	.L422
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L422
.L437:
	movl	12(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r14)
	jmp	.L438
.L427:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L428
.L410:
	call	_ZdaPv@PLT
	jmp	.L601
.L483:
	movzbl	-1097(%rbp), %r14d
	jmp	.L373
.L604:
	call	_ZSt17__throw_bad_allocv@PLT
.L488:
	xorl	%eax, %eax
	jmp	.L407
.L490:
	movl	$2, %eax
	jmp	.L407
.L489:
	movl	$1, %eax
	jmp	.L407
.L396:
	leaq	.LC25(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L484:
	movq	-1080(%rbp), %r15
	jmp	.L395
	.cfi_endproc
.LFE18219:
	.size	_ZN2v88internal10JSCollator3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6ObjectEEES8_, .-_ZN2v88internal10JSCollator3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6ObjectEEES8_
	.section	.rodata.CSWTCH.331,"a"
	.align 16
	.type	CSWTCH.331, @object
	.size	CSWTCH.331, 16
CSWTCH.331:
	.long	25
	.long	24
	.long	16
	.long	16
	.section	.data.rel.ro.local._ZTVN2v88internal4Intl16AvailableLocalesIN6icu_678CollatorENS0_12_GLOBAL__N_19CheckCollEEE,"aw"
	.align 8
	.type	_ZTVN2v88internal4Intl16AvailableLocalesIN6icu_678CollatorENS0_12_GLOBAL__N_19CheckCollEEE, @object
	.size	_ZTVN2v88internal4Intl16AvailableLocalesIN6icu_678CollatorENS0_12_GLOBAL__N_19CheckCollEEE, 32
_ZTVN2v88internal4Intl16AvailableLocalesIN6icu_678CollatorENS0_12_GLOBAL__N_19CheckCollEEE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal4Intl16AvailableLocalesIN6icu_678CollatorENS0_12_GLOBAL__N_19CheckCollEED1Ev
	.quad	_ZN2v88internal4Intl16AvailableLocalesIN6icu_678CollatorENS0_12_GLOBAL__N_19CheckCollEED0Ev
	.weak	_ZTVN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.section	.data.rel.ro._ZTVN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"awG",@progbits,_ZTVN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,comdat
	.align 8
	.type	_ZTVN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, @object
	.size	_ZTVN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, 56
_ZTVN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE:
	.quad	0
	.quad	0
	.quad	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED1Ev
	.quad	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED0Ev
	.quad	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE6AppendEPKci
	.quad	_ZN6icu_678ByteSink15GetAppendBufferEiiPciPi
	.quad	_ZN6icu_678ByteSink5FlushEv
	.weak	_ZTVSt19_Sp_counted_deleterIPN6icu_678CollatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE
	.section	.data.rel.ro.local._ZTVSt19_Sp_counted_deleterIPN6icu_678CollatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE,"awG",@progbits,_ZTVSt19_Sp_counted_deleterIPN6icu_678CollatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 8
	.type	_ZTVSt19_Sp_counted_deleterIPN6icu_678CollatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTVSt19_Sp_counted_deleterIPN6icu_678CollatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE, 56
_ZTVSt19_Sp_counted_deleterIPN6icu_678CollatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE:
	.quad	0
	.quad	0
	.quad	_ZNSt19_Sp_counted_deleterIPN6icu_678CollatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.quad	_ZNSt19_Sp_counted_deleterIPN6icu_678CollatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.quad	_ZNSt19_Sp_counted_deleterIPN6icu_678CollatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.quad	_ZNSt19_Sp_counted_deleterIPN6icu_678CollatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.quad	_ZNSt19_Sp_counted_deleterIPN6icu_678CollatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.bss._ZZN2v88internal10JSCollator19GetAvailableLocalesB5cxx11EvE17available_locales,"aw",@nobits
	.align 32
	.type	_ZZN2v88internal10JSCollator19GetAvailableLocalesB5cxx11EvE17available_locales, @object
	.size	_ZZN2v88internal10JSCollator19GetAvailableLocalesB5cxx11EvE17available_locales, 64
_ZZN2v88internal10JSCollator19GetAvailableLocalesB5cxx11EvE17available_locales:
	.zero	64
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
