	.file	"sampling-heap-profiler.cc"
	.text
	.section	.text._ZN2v88internal17AllocationProfile11GetRootNodeEv,"axG",@progbits,_ZN2v88internal17AllocationProfile11GetRootNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal17AllocationProfile11GetRootNodeEv
	.type	_ZN2v88internal17AllocationProfile11GetRootNodeEv, @function
_ZN2v88internal17AllocationProfile11GetRootNodeEv:
.LFB7490:
	.cfi_startproc
	endbr64
	movq	80(%rdi), %rax
	subq	48(%rdi), %rax
	movabsq	$3353953467947191203, %rsi
	sarq	$3, %rax
	movq	24(%rdi), %r8
	movq	56(%rdi), %rdx
	leaq	-5(%rax,%rax,4), %rcx
	movq	40(%rdi), %rax
	subq	64(%rdi), %rdx
	sarq	$3, %rdx
	subq	%r8, %rax
	imulq	%rsi, %rdx
	sarq	$3, %rax
	imulq	%rsi, %rax
	addq	%rcx, %rdx
	addq	%rax, %rdx
	movl	$0, %eax
	cmove	%rax, %r8
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE7490:
	.size	_ZN2v88internal17AllocationProfile11GetRootNodeEv, .-_ZN2v88internal17AllocationProfile11GetRootNodeEv
	.section	.text._ZN2v88internal17AllocationProfile10GetSamplesEv,"axG",@progbits,_ZN2v88internal17AllocationProfile10GetSamplesEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal17AllocationProfile10GetSamplesEv
	.type	_ZN2v88internal17AllocationProfile10GetSamplesEv, @function
_ZN2v88internal17AllocationProfile10GetSamplesEv:
.LFB7491:
	.cfi_startproc
	endbr64
	leaq	88(%rdi), %rax
	ret
	.cfi_endproc
.LFE7491:
	.size	_ZN2v88internal17AllocationProfile10GetSamplesEv, .-_ZN2v88internal17AllocationProfile10GetSamplesEv
	.section	.text._ZN2v88internal20SamplingHeapProfiler8ObserverD2Ev,"axG",@progbits,_ZN2v88internal20SamplingHeapProfiler8ObserverD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20SamplingHeapProfiler8ObserverD2Ev
	.type	_ZN2v88internal20SamplingHeapProfiler8ObserverD2Ev, @function
_ZN2v88internal20SamplingHeapProfiler8ObserverD2Ev:
.LFB24055:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE24055:
	.size	_ZN2v88internal20SamplingHeapProfiler8ObserverD2Ev, .-_ZN2v88internal20SamplingHeapProfiler8ObserverD2Ev
	.weak	_ZN2v88internal20SamplingHeapProfiler8ObserverD1Ev
	.set	_ZN2v88internal20SamplingHeapProfiler8ObserverD1Ev,_ZN2v88internal20SamplingHeapProfiler8ObserverD2Ev
	.section	.text._ZN2v88internal20SamplingHeapProfiler8ObserverD0Ev,"axG",@progbits,_ZN2v88internal20SamplingHeapProfiler8ObserverD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20SamplingHeapProfiler8ObserverD0Ev
	.type	_ZN2v88internal20SamplingHeapProfiler8ObserverD0Ev, @function
_ZN2v88internal20SamplingHeapProfiler8ObserverD0Ev:
.LFB24057:
	.cfi_startproc
	endbr64
	movl	$56, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE24057:
	.size	_ZN2v88internal20SamplingHeapProfiler8ObserverD0Ev, .-_ZN2v88internal20SamplingHeapProfiler8ObserverD0Ev
	.section	.text._ZN2v88internal20SamplingHeapProfiler8Observer15GetNextStepSizeEv,"axG",@progbits,_ZN2v88internal20SamplingHeapProfiler8Observer15GetNextStepSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20SamplingHeapProfiler8Observer15GetNextStepSizeEv
	.type	_ZN2v88internal20SamplingHeapProfiler8Observer15GetNextStepSizeEv, @function
_ZN2v88internal20SamplingHeapProfiler8Observer15GetNextStepSizeEv:
.LFB7540:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	48(%rdi), %rbx
	cmpb	$0, _ZN2v88internal47FLAG_sampling_heap_profiler_suppress_randomnessE(%rip)
	movq	%rbx, %rax
	je	.L16
.L8:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	movq	40(%rdi), %rdi
	call	_ZN2v84base21RandomNumberGenerator10NextDoubleEv@PLT
	call	_ZN2v84base7ieee7543logEd@PLT
	movapd	%xmm0, %xmm1
	xorpd	.LC0(%rip), %xmm1
	testq	%rbx, %rbx
	js	.L11
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rbx, %xmm0
.L12:
	mulsd	%xmm1, %xmm0
	movsd	.LC1(%rip), %xmm1
	movl	$8, %eax
	comisd	%xmm0, %xmm1
	ja	.L8
	comisd	.LC2(%rip), %xmm0
	movl	$2147483647, %eax
	ja	.L8
	addq	$8, %rsp
	cvttsd2siq	%xmm0, %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	movq	%rbx, %rax
	andl	$1, %ebx
	pxor	%xmm0, %xmm0
	shrq	%rax
	orq	%rbx, %rax
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L12
	.cfi_endproc
.LFE7540:
	.size	_ZN2v88internal20SamplingHeapProfiler8Observer15GetNextStepSizeEv, .-_ZN2v88internal20SamplingHeapProfiler8Observer15GetNextStepSizeEv
	.section	.text._ZN2v88internal17AllocationProfileD2Ev,"axG",@progbits,_ZN2v88internal17AllocationProfileD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal17AllocationProfileD2Ev
	.type	_ZN2v88internal17AllocationProfileD2Ev, @function
_ZN2v88internal17AllocationProfileD2Ev:
.LFB24059:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal17AllocationProfileE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%rax, (%rdi)
	movq	88(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L18
	call	_ZdlPv@PLT
.L18:
	movq	48(%rbx), %rcx
	movq	80(%rbx), %rax
	movq	40(%rbx), %rsi
	movq	56(%rbx), %r13
	leaq	8(%rcx), %r14
	movq	64(%rbx), %r12
	movq	24(%rbx), %r15
	movq	%rax, -64(%rbp)
	movq	%rsi, -56(%rbp)
	movq	%rcx, -80(%rbp)
	cmpq	%r14, %rax
	jbe	.L27
	.p2align 4,,10
	.p2align 3
.L19:
	movq	(%r14), %rdx
	leaq	440(%rdx), %rax
	movq	%rax, -72(%rbp)
	.p2align 4,,10
	.p2align 3
.L26:
	movq	64(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L22
	movq	%rdx, -88(%rbp)
	call	_ZdlPv@PLT
	movq	-88(%rbp), %rdx
.L22:
	movq	40(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L23
	movq	%rdx, -88(%rbp)
	call	_ZdlPv@PLT
	movq	-88(%rbp), %rdx
	addq	$88, %rdx
	cmpq	-72(%rbp), %rdx
	jne	.L26
	addq	$8, %r14
	cmpq	%r14, -64(%rbp)
	ja	.L19
.L27:
	movq	-80(%rbp), %rsi
	cmpq	%rsi, -64(%rbp)
	je	.L68
	cmpq	-56(%rbp), %r15
	je	.L34
	.p2align 4,,10
	.p2align 3
.L28:
	movq	64(%r15), %rdi
	testq	%rdi, %rdi
	je	.L32
	call	_ZdlPv@PLT
.L32:
	movq	40(%r15), %rdi
	testq	%rdi, %rdi
	je	.L33
	call	_ZdlPv@PLT
	addq	$88, %r15
	cmpq	%r15, -56(%rbp)
	jne	.L28
.L34:
	cmpq	%r12, %r13
	je	.L38
	.p2align 4,,10
	.p2align 3
.L29:
	movq	64(%r12), %rdi
	testq	%rdi, %rdi
	je	.L36
	call	_ZdlPv@PLT
.L36:
	movq	40(%r12), %rdi
	testq	%rdi, %rdi
	je	.L37
	call	_ZdlPv@PLT
	addq	$88, %r12
	cmpq	%r12, %r13
	jne	.L29
.L38:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L71
.L30:
	movq	80(%rbx), %rax
	movq	48(%rbx), %r12
	leaq	8(%rax), %r13
	cmpq	%r12, %r13
	jbe	.L44
	.p2align 4,,10
	.p2align 3
.L45:
	movq	(%r12), %rdi
	addq	$8, %r12
	call	_ZdlPv@PLT
	cmpq	%r12, %r13
	ja	.L45
	movq	8(%rbx), %rdi
.L44:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
	movq	64(%r15), %rdi
	testq	%rdi, %rdi
	je	.L40
	call	_ZdlPv@PLT
.L40:
	movq	40(%r15), %rdi
	testq	%rdi, %rdi
	je	.L70
	call	_ZdlPv@PLT
.L70:
	addq	$88, %r15
.L68:
	cmpq	%r15, %r13
	jne	.L43
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L30
.L71:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	.cfi_restore_state
	addq	$88, %r12
	cmpq	%r12, %r13
	jne	.L29
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L23:
	addq	$88, %rdx
	cmpq	%rdx, -72(%rbp)
	jne	.L26
	addq	$8, %r14
	cmpq	%r14, -64(%rbp)
	ja	.L19
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L33:
	addq	$88, %r15
	cmpq	%r15, -56(%rbp)
	jne	.L28
	jmp	.L34
	.cfi_endproc
.LFE24059:
	.size	_ZN2v88internal17AllocationProfileD2Ev, .-_ZN2v88internal17AllocationProfileD2Ev
	.weak	_ZN2v88internal17AllocationProfileD1Ev
	.set	_ZN2v88internal17AllocationProfileD1Ev,_ZN2v88internal17AllocationProfileD2Ev
	.section	.text._ZN2v88internal17AllocationProfileD0Ev,"axG",@progbits,_ZN2v88internal17AllocationProfileD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal17AllocationProfileD0Ev
	.type	_ZN2v88internal17AllocationProfileD0Ev, @function
_ZN2v88internal17AllocationProfileD0Ev:
.LFB24061:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal17AllocationProfileE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%rax, (%rdi)
	movq	88(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L73
	call	_ZdlPv@PLT
.L73:
	movq	48(%r12), %rcx
	movq	80(%r12), %rax
	movq	40(%r12), %rsi
	movq	56(%r12), %r13
	leaq	8(%rcx), %r14
	movq	%rax, -64(%rbp)
	movq	64(%r12), %rbx
	movq	%rsi, -56(%rbp)
	movq	24(%r12), %r15
	movq	%rcx, -80(%rbp)
	cmpq	%r14, %rax
	jbe	.L82
	.p2align 4,,10
	.p2align 3
.L74:
	movq	(%r14), %rdx
	leaq	440(%rdx), %rax
	movq	%rax, -72(%rbp)
	.p2align 4,,10
	.p2align 3
.L81:
	movq	64(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L77
	movq	%rdx, -88(%rbp)
	call	_ZdlPv@PLT
	movq	-88(%rbp), %rdx
.L77:
	movq	40(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L78
	movq	%rdx, -88(%rbp)
	call	_ZdlPv@PLT
	movq	-88(%rbp), %rdx
	addq	$88, %rdx
	cmpq	-72(%rbp), %rdx
	jne	.L81
	addq	$8, %r14
	cmpq	%r14, -64(%rbp)
	ja	.L74
.L82:
	movq	-80(%rbp), %rsi
	cmpq	%rsi, -64(%rbp)
	je	.L122
	cmpq	-56(%rbp), %r15
	je	.L89
	.p2align 4,,10
	.p2align 3
.L83:
	movq	64(%r15), %rdi
	testq	%rdi, %rdi
	je	.L87
	call	_ZdlPv@PLT
.L87:
	movq	40(%r15), %rdi
	testq	%rdi, %rdi
	je	.L88
	call	_ZdlPv@PLT
	addq	$88, %r15
	cmpq	%r15, -56(%rbp)
	jne	.L83
.L89:
	cmpq	%rbx, %r13
	je	.L93
	.p2align 4,,10
	.p2align 3
.L84:
	movq	64(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L91
	call	_ZdlPv@PLT
.L91:
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L92
	call	_ZdlPv@PLT
	addq	$88, %rbx
	cmpq	%rbx, %r13
	jne	.L84
.L93:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L86
	movq	80(%r12), %rax
	movq	48(%r12), %rbx
	leaq	8(%rax), %r13
	cmpq	%rbx, %r13
	jbe	.L99
	.p2align 4,,10
	.p2align 3
.L100:
	movq	(%rbx), %rdi
	addq	$8, %rbx
	call	_ZdlPv@PLT
	cmpq	%rbx, %r13
	ja	.L100
	movq	8(%r12), %rdi
.L99:
	call	_ZdlPv@PLT
.L86:
	addq	$56, %rsp
	movq	%r12, %rdi
	movl	$112, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L98:
	.cfi_restore_state
	movq	64(%r15), %rdi
	testq	%rdi, %rdi
	je	.L95
	call	_ZdlPv@PLT
.L95:
	movq	40(%r15), %rdi
	testq	%rdi, %rdi
	je	.L124
	call	_ZdlPv@PLT
.L124:
	addq	$88, %r15
.L122:
	cmpq	%r15, %r13
	jne	.L98
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L92:
	addq	$88, %rbx
	cmpq	%rbx, %r13
	jne	.L84
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L78:
	addq	$88, %rdx
	cmpq	%rdx, -72(%rbp)
	jne	.L81
	addq	$8, %r14
	cmpq	%r14, -64(%rbp)
	ja	.L74
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L88:
	addq	$88, %r15
	cmpq	%r15, -56(%rbp)
	jne	.L83
	jmp	.L89
	.cfi_endproc
.LFE24061:
	.size	_ZN2v88internal17AllocationProfileD0Ev, .-_ZN2v88internal17AllocationProfileD0Ev
	.section	.text._ZN2v88internal20SamplingHeapProfiler8Observer21GetNextSampleIntervalEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20SamplingHeapProfiler8Observer21GetNextSampleIntervalEm
	.type	_ZN2v88internal20SamplingHeapProfiler8Observer21GetNextSampleIntervalEm, @function
_ZN2v88internal20SamplingHeapProfiler8Observer21GetNextSampleIntervalEm:
.LFB18461:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	subq	$8, %rsp
	cmpb	$0, _ZN2v88internal47FLAG_sampling_heap_profiler_suppress_randomnessE(%rip)
	je	.L133
.L125:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L133:
	.cfi_restore_state
	movq	40(%rdi), %rdi
	call	_ZN2v84base21RandomNumberGenerator10NextDoubleEv@PLT
	call	_ZN2v84base7ieee7543logEd@PLT
	movapd	%xmm0, %xmm1
	xorpd	.LC0(%rip), %xmm1
	testq	%rbx, %rbx
	js	.L128
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rbx, %xmm0
.L129:
	mulsd	%xmm1, %xmm0
	movsd	.LC1(%rip), %xmm1
	movl	$8, %eax
	comisd	%xmm0, %xmm1
	ja	.L125
	comisd	.LC2(%rip), %xmm0
	movl	$2147483647, %eax
	ja	.L125
	addq	$8, %rsp
	cvttsd2siq	%xmm0, %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L128:
	.cfi_restore_state
	movq	%rbx, %rax
	andl	$1, %ebx
	pxor	%xmm0, %xmm0
	shrq	%rax
	orq	%rbx, %rax
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L129
	.cfi_endproc
.LFE18461:
	.size	_ZN2v88internal20SamplingHeapProfiler8Observer21GetNextSampleIntervalEm, .-_ZN2v88internal20SamplingHeapProfiler8Observer21GetNextSampleIntervalEm
	.section	.text._ZNK2v88internal20SamplingHeapProfiler11ScaleSampleEmj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal20SamplingHeapProfiler11ScaleSampleEmj
	.type	_ZNK2v88internal20SamplingHeapProfiler11ScaleSampleEmj, @function
_ZNK2v88internal20SamplingHeapProfiler11ScaleSampleEmj:
.LFB18462:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%edx, %ebx
	testq	%rsi, %rsi
	js	.L135
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rsi, %xmm0
.L136:
	movq	288(%rdi), %rdx
	xorpd	.LC0(%rip), %xmm0
	testq	%rdx, %rdx
	js	.L137
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rdx, %xmm1
.L138:
	divsd	%xmm1, %xmm0
	call	exp@PLT
	movq	%r12, %rax
	movapd	%xmm0, %xmm1
	movsd	.LC3(%rip), %xmm0
	movapd	%xmm0, %xmm2
	subsd	%xmm1, %xmm2
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rbx, %xmm1
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	divsd	%xmm2, %xmm0
	mulsd	%xmm1, %xmm0
	addsd	.LC4(%rip), %xmm0
	cvttsd2siq	%xmm0, %rdx
	movl	%edx, %edx
	ret
	.p2align 4,,10
	.p2align 3
.L135:
	.cfi_restore_state
	movq	%rsi, %rdx
	movq	%rsi, %rax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	andl	$1, %eax
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L137:
	movq	%rdx, %rax
	andl	$1, %edx
	pxor	%xmm1, %xmm1
	shrq	%rax
	orq	%rdx, %rax
	cvtsi2sdq	%rax, %xmm1
	addsd	%xmm1, %xmm1
	jmp	.L138
	.cfi_endproc
.LFE18462:
	.size	_ZNK2v88internal20SamplingHeapProfiler11ScaleSampleEmj, .-_ZNK2v88internal20SamplingHeapProfiler11ScaleSampleEmj
	.section	.rodata._ZN2v88internal20SamplingHeapProfilerC2EPNS0_4HeapEPNS0_14StringsStorageEmiNS_12HeapProfiler13SamplingFlagsE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"(root)"
.LC7:
	.string	"rate_ > 0u"
.LC8:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal20SamplingHeapProfilerC2EPNS0_4HeapEPNS0_14StringsStorageEmiNS_12HeapProfiler13SamplingFlagsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20SamplingHeapProfilerC2EPNS0_4HeapEPNS0_14StringsStorageEmiNS_12HeapProfiler13SamplingFlagsE
	.type	_ZN2v88internal20SamplingHeapProfilerC2EPNS0_4HeapEPNS0_14StringsStorageEmiNS_12HeapProfiler13SamplingFlagsE, @function
_ZN2v88internal20SamplingHeapProfilerC2EPNS0_4HeapEPNS0_14StringsStorageEmiNS_12HeapProfiler13SamplingFlagsE:
.LFB18488:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%r8d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%r9d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	-37592(%rsi), %rdi
	subq	$8, %rsp
	movq	%rdi, (%rbx)
	movq	%rsi, 8(%rbx)
	movq	$0, 16(%rbx)
	movl	$0, 24(%rbx)
	call	_ZN2v88internal7Isolate23random_number_generatorEv@PLT
	leaq	104(%rbx), %rdx
	movq	8(%rbx), %rdi
	leaq	16+_ZTVN2v88internal20SamplingHeapProfiler8ObserverE(%rip), %rcx
	movq	%rax, 72(%rbx)
	movl	24(%rbx), %eax
	movq	%rcx, 32(%rbx)
	leaq	.LC5(%rip), %rcx
	addl	$1, %eax
	movq	%rdx, 120(%rbx)
	movl	%eax, 24(%rbx)
	movq	%rdx, 128(%rbx)
	leaq	152(%rbx), %rdx
	movl	%eax, 216(%rbx)
	leaq	272(%rbx), %rax
	movq	%r12, 40(%rbx)
	movq	%r12, 48(%rbx)
	movq	%rbx, 56(%rbx)
	movq	%rdi, 64(%rbx)
	movq	%r12, 80(%rbx)
	movq	%r15, 88(%rbx)
	movl	$0, 104(%rbx)
	movq	$0, 112(%rbx)
	movq	$0, 136(%rbx)
	movl	$0, 152(%rbx)
	movq	$0, 160(%rbx)
	movq	%rdx, 168(%rbx)
	movq	%rdx, 176(%rbx)
	movq	$0, 184(%rbx)
	movq	$0, 192(%rbx)
	movq	$0, 200(%rbx)
	movq	%rcx, 208(%rbx)
	movb	$0, 220(%rbx)
	movq	%rax, 224(%rbx)
	movq	$1, 232(%rbx)
	movq	$0, 240(%rbx)
	movq	$0, 248(%rbx)
	movl	$0x3f800000, 256(%rbx)
	movq	$0, 264(%rbx)
	movq	$0, 272(%rbx)
	movl	%r14d, 280(%rbx)
	movq	%r12, 288(%rbx)
	movl	%r13d, 296(%rbx)
	testq	%r12, %r12
	je	.L145
	addq	$8, %rsp
	leaq	32(%rbx), %rsi
	movq	%rsi, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal4Heap33AddAllocationObserversToAllSpacesEPNS0_18AllocationObserverES3_@PLT
	.p2align 4,,10
	.p2align 3
.L145:
	.cfi_restore_state
	leaq	.LC7(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18488:
	.size	_ZN2v88internal20SamplingHeapProfilerC2EPNS0_4HeapEPNS0_14StringsStorageEmiNS_12HeapProfiler13SamplingFlagsE, .-_ZN2v88internal20SamplingHeapProfilerC2EPNS0_4HeapEPNS0_14StringsStorageEmiNS_12HeapProfiler13SamplingFlagsE
	.globl	_ZN2v88internal20SamplingHeapProfilerC1EPNS0_4HeapEPNS0_14StringsStorageEmiNS_12HeapProfiler13SamplingFlagsE
	.set	_ZN2v88internal20SamplingHeapProfilerC1EPNS0_4HeapEPNS0_14StringsStorageEmiNS_12HeapProfiler13SamplingFlagsE,_ZN2v88internal20SamplingHeapProfilerC2EPNS0_4HeapEPNS0_14StringsStorageEmiNS_12HeapProfiler13SamplingFlagsE
	.section	.text._ZNSt6vectorIN2v817AllocationProfile6SampleESaIS2_EEaSERKS4_,"axG",@progbits,_ZNSt6vectorIN2v817AllocationProfile6SampleESaIS2_EEaSERKS4_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v817AllocationProfile6SampleESaIS2_EEaSERKS4_
	.type	_ZNSt6vectorIN2v817AllocationProfile6SampleESaIS2_EEaSERKS4_, @function
_ZNSt6vectorIN2v817AllocationProfile6SampleESaIS2_EEaSERKS4_:
.LFB20835:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	cmpq	%rdi, %rsi
	je	.L147
	movq	8(%rsi), %rdx
	movq	(%rsi), %r14
	movq	%rsi, %rbx
	movq	(%rdi), %r15
	movq	16(%rdi), %rax
	movq	%rdx, %r13
	subq	%r14, %r13
	subq	%r15, %rax
	movq	%r13, %rcx
	sarq	$5, %rax
	sarq	$5, %rcx
	cmpq	%rax, %rcx
	ja	.L167
	movq	8(%rdi), %rdi
	movq	%rdi, %rsi
	subq	%r15, %rsi
	movq	%rsi, %rax
	sarq	$5, %rax
	cmpq	%rax, %rcx
	ja	.L155
	cmpq	%rdx, %r14
	je	.L166
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	memmove@PLT
	addq	(%r12), %r13
	movq	%r13, 8(%r12)
.L147:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L167:
	.cfi_restore_state
	xorl	%ebx, %ebx
	testq	%rcx, %rcx
	je	.L150
	movabsq	$288230376151711743, %rax
	cmpq	%rax, %rcx
	ja	.L168
	movq	%r13, %rdi
	movq	%rdx, -56(%rbp)
	call	_Znwm@PLT
	movq	(%r12), %r15
	movq	-56(%rbp), %rdx
	movq	%rax, %rbx
.L150:
	cmpq	%rdx, %r14
	je	.L152
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	memcpy@PLT
.L152:
	testq	%r15, %r15
	je	.L153
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L153:
	addq	%rbx, %r13
	movq	%rbx, (%r12)
	movq	%r13, 16(%r12)
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L155:
	testq	%rsi, %rsi
	je	.L157
	movq	%rsi, %rdx
	movq	%r15, %rdi
	movq	%r14, %rsi
	call	memmove@PLT
	movq	8(%r12), %rdi
	movq	(%r12), %r15
	movq	8(%rbx), %rdx
	movq	(%rbx), %r14
	movq	%rdi, %rsi
	subq	%r15, %rsi
.L157:
	addq	%r14, %rsi
	cmpq	%rdx, %rsi
	jne	.L158
.L166:
	addq	%r15, %r13
.L154:
	movq	%r13, 8(%r12)
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L158:
	subq	%rsi, %rdx
	call	memmove@PLT
	addq	(%r12), %r13
	jmp	.L154
.L168:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE20835:
	.size	_ZNSt6vectorIN2v817AllocationProfile6SampleESaIS2_EEaSERKS4_, .-_ZNSt6vectorIN2v817AllocationProfile6SampleESaIS2_EEaSERKS4_
	.section	.text._ZNSt8_Rb_treeImSt4pairIKmjESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E,"axG",@progbits,_ZNSt8_Rb_treeImSt4pairIKmjESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeImSt4pairIKmjESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	.type	_ZNSt8_Rb_treeImSt4pairIKmjESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E, @function
_ZNSt8_Rb_treeImSt4pairIKmjESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E:
.LFB21051:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L177
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L171:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmjESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L171
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L177:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE21051:
	.size	_ZNSt8_Rb_treeImSt4pairIKmjESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E, .-_ZNSt8_Rb_treeImSt4pairIKmjESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	.section	.text._ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal20SamplingHeapProfiler14AllocationNodeESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E,"axG",@progbits,_ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal20SamplingHeapProfiler14AllocationNodeESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal20SamplingHeapProfiler14AllocationNodeESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E
	.type	_ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal20SamplingHeapProfiler14AllocationNodeESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E, @function
_ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal20SamplingHeapProfiler14AllocationNodeESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E:
.LFB21059:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L198
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
.L185:
	movq	24(%rbx), %rsi
	movq	%rbx, %r14
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal20SamplingHeapProfiler14AllocationNodeESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E
	movq	40(%r14), %r13
	movq	16(%rbx), %rbx
	testq	%r13, %r13
	je	.L182
	movq	64(%r13), %rsi
	leaq	48(%r13), %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal20SamplingHeapProfiler14AllocationNodeESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E
	movq	16(%r13), %r12
	testq	%r12, %r12
	je	.L183
.L184:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmjESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%r12, %rdi
	movq	16(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L184
.L183:
	movl	$128, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L182:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L185
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L198:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE21059:
	.size	_ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal20SamplingHeapProfiler14AllocationNodeESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E, .-_ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal20SamplingHeapProfiler14AllocationNodeESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E
	.section	.text._ZN2v88internal20SamplingHeapProfilerD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20SamplingHeapProfilerD2Ev
	.type	_ZN2v88internal20SamplingHeapProfilerD2Ev, @function
_ZN2v88internal20SamplingHeapProfilerD2Ev:
.LFB18491:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	32(%rdi), %rsi
	movq	%rsi, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %rdi
	call	_ZN2v88internal4Heap38RemoveAllocationObserversFromAllSpacesEPNS0_18AllocationObserverES3_@PLT
	movq	240(%rbx), %r12
	testq	%r12, %r12
	je	.L202
	.p2align 4,,10
	.p2align 3
.L205:
	movq	%r12, %r14
	movq	(%r12), %r12
	movq	16(%r14), %r13
	testq	%r13, %r13
	je	.L203
	movq	16(%r13), %rdi
	testq	%rdi, %rdi
	je	.L204
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L204:
	movl	$40, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L203:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L205
.L202:
	movq	232(%rbx), %rax
	movq	224(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	224(%rbx), %rdi
	leaq	272(%rbx), %rax
	movq	$0, 248(%rbx)
	movq	$0, 240(%rbx)
	cmpq	%rax, %rdi
	je	.L206
	call	_ZdlPv@PLT
.L206:
	leaq	96(%rbx), %rax
	movq	160(%rbx), %r12
	movq	%rax, -56(%rbp)
	leaq	144(%rbx), %rax
	movq	%rax, -64(%rbp)
	testq	%r12, %r12
	je	.L207
.L211:
	movq	24(%r12), %rsi
	movq	-64(%rbp), %rdi
	movq	%r12, %r13
	call	_ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal20SamplingHeapProfiler14AllocationNodeESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E
	movq	40(%r13), %r14
	movq	16(%r12), %r12
	testq	%r14, %r14
	je	.L208
	movq	64(%r14), %rsi
	leaq	48(%r14), %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal20SamplingHeapProfiler14AllocationNodeESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E
	movq	16(%r14), %r15
	testq	%r15, %r15
	je	.L209
.L210:
	movq	24(%r15), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmjESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%r15, %rdi
	movq	16(%r15), %r15
	call	_ZdlPv@PLT
	testq	%r15, %r15
	jne	.L210
.L209:
	movl	$128, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L208:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L211
.L207:
	movq	112(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L201
.L213:
	movq	24(%rbx), %rsi
	movq	-56(%rbp), %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmjESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L213
.L201:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18491:
	.size	_ZN2v88internal20SamplingHeapProfilerD2Ev, .-_ZN2v88internal20SamplingHeapProfilerD2Ev
	.globl	_ZN2v88internal20SamplingHeapProfilerD1Ev
	.set	_ZN2v88internal20SamplingHeapProfilerD1Ev,_ZN2v88internal20SamplingHeapProfilerD2Ev
	.section	.text._ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal20SamplingHeapProfiler14AllocationNodeESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE17_M_emplace_uniqueIJRmS9_EEES0_ISt17_Rb_tree_iteratorISA_EbEDpOT_,"axG",@progbits,_ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal20SamplingHeapProfiler14AllocationNodeESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE17_M_emplace_uniqueIJRmS9_EEES0_ISt17_Rb_tree_iteratorISA_EbEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal20SamplingHeapProfiler14AllocationNodeESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE17_M_emplace_uniqueIJRmS9_EEES0_ISt17_Rb_tree_iteratorISA_EbEDpOT_
	.type	_ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal20SamplingHeapProfiler14AllocationNodeESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE17_M_emplace_uniqueIJRmS9_EEES0_ISt17_Rb_tree_iteratorISA_EbEDpOT_, @function
_ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal20SamplingHeapProfiler14AllocationNodeESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE17_M_emplace_uniqueIJRmS9_EEES0_ISt17_Rb_tree_iteratorISA_EbEDpOT_:
.LFB21065:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	$48, %edi
	subq	$40, %rsp
	call	_Znwm@PLT
	movq	(%r15), %rdx
	movq	(%r14), %r15
	movq	$0, (%r14)
	movq	16(%rbx), %r12
	movq	%rax, %r13
	leaq	8(%rbx), %r14
	movq	%rdx, 32(%rax)
	movq	%r15, 40(%rax)
	testq	%r12, %r12
	jne	.L242
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L291:
	movq	16(%r12), %rax
	movl	$1, %ecx
	testq	%rax, %rax
	je	.L243
.L292:
	movq	%rax, %r12
.L242:
	movq	32(%r12), %rsi
	cmpq	%rsi, %rdx
	jb	.L291
	movq	24(%r12), %rax
	xorl	%ecx, %ecx
	testq	%rax, %rax
	jne	.L292
.L243:
	testb	%cl, %cl
	jne	.L293
	cmpq	%rsi, %rdx
	jbe	.L248
.L259:
	movl	$1, %edi
	cmpq	%r12, %r14
	jne	.L294
.L249:
	movq	%r12, %rdx
	movq	%r14, %rcx
	movq	%r13, %rsi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 40(%rbx)
	addq	$40, %rsp
	movq	%r13, %rax
	popq	%rbx
	movl	$1, %edx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L293:
	.cfi_restore_state
	cmpq	24(%rbx), %r12
	je	.L259
.L260:
	movq	%r12, %rdi
	movq	%rdx, -56(%rbp)
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	-56(%rbp), %rdx
	cmpq	32(%rax), %rdx
	ja	.L295
	movq	%rax, %r12
.L248:
	testq	%r15, %r15
	je	.L251
	movq	64(%r15), %rbx
	leaq	48(%r15), %rax
	movq	%rax, -72(%rbp)
	testq	%rbx, %rbx
	je	.L258
.L252:
	movq	24(%rbx), %rsi
	movq	-72(%rbp), %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal20SamplingHeapProfiler14AllocationNodeESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E
	movq	%rbx, %r9
	movq	16(%rbx), %rbx
	movq	40(%r9), %r8
	testq	%r8, %r8
	je	.L255
	movq	64(%r8), %rsi
	leaq	48(%r8), %rdi
	movq	%r9, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal20SamplingHeapProfiler14AllocationNodeESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %r9
	movq	16(%r8), %r14
	testq	%r14, %r14
	je	.L257
.L256:
	movq	24(%r14), %rsi
	movq	%r8, %rdi
	movq	%r9, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZNSt8_Rb_treeImSt4pairIKmjESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%r14, %rdi
	movq	16(%r14), %r14
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %r9
	testq	%r14, %r14
	jne	.L256
.L257:
	movl	$128, %esi
	movq	%r8, %rdi
	movq	%r9, -56(%rbp)
	call	_ZdlPvm@PLT
	movq	-56(%rbp), %r9
.L255:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L252
.L258:
	movq	16(%r15), %rbx
	testq	%rbx, %rbx
	je	.L253
.L254:
	movq	24(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmjESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L254
.L253:
	movl	$128, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L251:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	addq	$40, %rsp
	movq	%r12, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L295:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L248
	movl	$1, %edi
	cmpq	%r12, %r14
	je	.L249
.L294:
	xorl	%edi, %edi
	cmpq	32(%r12), %rdx
	setb	%dil
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L290:
	movq	%r14, %r12
	cmpq	24(%rbx), %r14
	jne	.L260
	movl	$1, %edi
	jmp	.L249
	.cfi_endproc
.LFE21065:
	.size	_ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal20SamplingHeapProfiler14AllocationNodeESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE17_M_emplace_uniqueIJRmS9_EEES0_ISt17_Rb_tree_iteratorISA_EbEDpOT_, .-_ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal20SamplingHeapProfiler14AllocationNodeESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE17_M_emplace_uniqueIJRmS9_EEES0_ISt17_Rb_tree_iteratorISA_EbEDpOT_
	.section	.text._ZN2v88internal20SamplingHeapProfiler18FindOrAddChildNodeEPNS1_14AllocationNodeEPKcii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20SamplingHeapProfiler18FindOrAddChildNodeEPNS1_14AllocationNodeEPKcii
	.type	_ZN2v88internal20SamplingHeapProfiler18FindOrAddChildNodeEPNS1_14AllocationNodeEPKcii, @function
_ZN2v88internal20SamplingHeapProfiler18FindOrAddChildNodeEPNS1_14AllocationNodeEPKcii:
.LFB18496:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%r8d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	orq	$1, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	je	.L298
	movq	%r13, %rbx
	leal	(%r8,%r8), %eax
	salq	$32, %rbx
	cltq
	addq	%rax, %rbx
.L298:
	movq	64(%r12), %rax
	leaq	56(%r12), %rcx
	testq	%rax, %rax
	je	.L299
	movq	%rcx, %r9
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L336:
	movq	%rax, %r9
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L301
.L300:
	cmpq	%rbx, 32(%rax)
	jnb	.L336
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L300
.L301:
	cmpq	%r9, %rcx
	je	.L299
	cmpq	%rbx, 32(%r9)
	ja	.L299
	movq	40(%r9), %r15
	testq	%r15, %r15
	je	.L299
.L296:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L337
	addq	$56, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L299:
	.cfi_restore_state
	movl	24(%r10), %eax
	movl	$128, %edi
	movq	%rdx, -88(%rbp)
	leal	1(%rax), %r15d
	movl	%r15d, 24(%r10)
	call	_Znwm@PLT
	movq	-88(%rbp), %rdx
	leaq	-64(%rbp), %rsi
	leaq	48(%r12), %rdi
	leaq	8(%rax), %rcx
	movl	%r13d, 104(%rax)
	movq	%rcx, 24(%rax)
	movq	%rcx, 32(%rax)
	leaq	56(%rax), %rcx
	movq	%rdx, 112(%rax)
	leaq	-72(%rbp), %rdx
	movl	%r15d, 120(%rax)
	movl	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 40(%rax)
	movl	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rcx, 72(%rax)
	movq	%rcx, 80(%rax)
	movq	$0, 88(%rax)
	movq	%r12, 96(%rax)
	movl	%r14d, 108(%rax)
	movb	$0, 124(%rax)
	movq	%rax, -72(%rbp)
	movq	%rbx, -64(%rbp)
	call	_ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal20SamplingHeapProfiler14AllocationNodeESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE17_M_emplace_uniqueIJRmS9_EEES0_ISt17_Rb_tree_iteratorISA_EbEDpOT_
	movq	-72(%rbp), %r13
	movq	40(%rax), %r15
	testq	%r13, %r13
	je	.L296
	movq	64(%r13), %r12
	leaq	48(%r13), %rax
	movq	%rax, -96(%rbp)
	testq	%r12, %r12
	je	.L306
.L310:
	movq	24(%r12), %rsi
	movq	-96(%rbp), %rdi
	movq	%r12, %r14
	call	_ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal20SamplingHeapProfiler14AllocationNodeESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E
	movq	40(%r14), %r8
	movq	16(%r12), %r12
	testq	%r8, %r8
	je	.L307
	movq	64(%r8), %rsi
	leaq	48(%r8), %rdi
	movq	%r8, -88(%rbp)
	call	_ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal20SamplingHeapProfiler14AllocationNodeESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E
	movq	-88(%rbp), %r8
	movq	16(%r8), %rbx
	testq	%rbx, %rbx
	je	.L308
.L309:
	movq	24(%rbx), %rsi
	movq	%r8, %rdi
	movq	%r8, -88(%rbp)
	call	_ZNSt8_Rb_treeImSt4pairIKmjESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	movq	-88(%rbp), %r8
	testq	%rbx, %rbx
	jne	.L309
.L308:
	movl	$128, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L307:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L310
.L306:
	movq	16(%r13), %rbx
	testq	%rbx, %rbx
	je	.L311
.L312:
	movq	24(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmjESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L312
.L311:
	movl	$128, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	jmp	.L296
.L337:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18496:
	.size	_ZN2v88internal20SamplingHeapProfiler18FindOrAddChildNodeEPNS1_14AllocationNodeEPKcii, .-_ZN2v88internal20SamplingHeapProfiler18FindOrAddChildNodeEPNS1_14AllocationNodeEPKcii
	.section	.rodata._ZN2v88internal20SamplingHeapProfiler8AddStackEv.str1.1,"aMS",@progbits,1
.LC9:
	.string	"(JS)"
.LC10:
	.string	"(PARSER)"
.LC11:
	.string	"(COMPILER)"
.LC12:
	.string	"(BYTECODE_COMPILER)"
.LC13:
	.string	"(V8 API)"
.LC14:
	.string	"(EXTERNAL)"
.LC15:
	.string	"(IDLE)"
.LC16:
	.string	"(GC)"
.LC17:
	.string	"vector::_M_realloc_insert"
.LC18:
	.string	"(deopt)"
	.section	.text.unlikely._ZN2v88internal20SamplingHeapProfiler8AddStackEv,"ax",@progbits
	.align 2
.LCOLDB19:
	.section	.text._ZN2v88internal20SamplingHeapProfiler8AddStackEv,"ax",@progbits
.LHOTB19:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20SamplingHeapProfiler8AddStackEv
	.type	_ZN2v88internal20SamplingHeapProfiler8AddStackEv, @function
_ZN2v88internal20SamplingHeapProfiler8AddStackEv:
.LFB18497:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-1504(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	96(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$1528, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rsi
	movq	%r15, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateE@PLT
	cmpq	$0, -88(%rbp)
	je	.L339
	movq	%r15, %rdi
	call	_ZN2v88internal23JavaScriptFrameIterator7AdvanceEv@PLT
	movq	-88(%rbp), %r14
	testq	%r14, %r14
	je	.L339
	xorl	%ebx, %ebx
	xorl	%esi, %esi
	xorl	%eax, %eax
	movq	%r13, -1552(%rbp)
	movq	%rbx, -1528(%rbp)
	movl	%eax, %r13d
	movl	%esi, %ebx
	movq	$0, -1544(%rbp)
	movq	$0, -1536(%rbp)
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L377:
	movl	%edx, %ebx
.L341:
	movq	%r15, %rdi
	call	_ZN2v88internal23JavaScriptFrameIterator7AdvanceEv@PLT
	movq	-88(%rbp), %r14
	testq	%r14, %r14
	je	.L411
.L356:
	cmpl	%r13d, 280(%r12)
	jle	.L411
	movq	%r14, %rdi
	call	_ZNK2v88internal15JavaScriptFrame18unchecked_functionEv@PLT
	movq	%rax, %rdx
	notq	%rdx
	andl	$1, %edx
	jne	.L377
	movq	-1(%rax), %rax
	cmpw	$1105, 11(%rax)
	je	.L413
	movl	$1, %ebx
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L411:
	movl	%r13d, %eax
	movb	%bl, -1544(%rbp)
	movq	-1552(%rbp), %r13
	movq	-1528(%rbp), %rbx
	testl	%eax, %eax
	je	.L343
	cmpq	-1536(%rbp), %rbx
	je	.L368
	leaq	-1512(%rbp), %r14
	.p2align 4,,10
	.p2align 3
.L374:
	movq	-8(%rbx), %rax
	movq	88(%r12), %r15
	movq	%r14, %rdi
	movq	%rax, -1512(%rbp)
	call	_ZN2v88internal18SharedFunctionInfo9DebugNameEv@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14StringsStorage7GetNameENS0_4NameE@PLT
	movq	-1512(%rbp), %rcx
	movq	%rax, %r15
	movq	31(%rcx), %rax
	testb	$1, %al
	jne	.L369
.L372:
	xorl	%ecx, %ecx
.L370:
	movq	%r14, %rdi
	movl	%ecx, -1528(%rbp)
	subq	$8, %rbx
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	movq	%r13, %rsi
	movq	%r15, %rdx
	movq	%r12, %rdi
	movl	-1528(%rbp), %ecx
	movl	%eax, %r8d
	call	_ZN2v88internal20SamplingHeapProfiler18FindOrAddChildNodeEPNS1_14AllocationNodeEPKcii
	movq	%rax, %r13
	cmpq	-1536(%rbp), %rbx
	jne	.L374
.L368:
	cmpb	$0, -1544(%rbp)
	jne	.L414
.L367:
	movq	-1536(%rbp), %rax
	testq	%rax, %rax
	je	.L338
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L338:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L415
	addq	$1528, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L413:
	.cfi_restore_state
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*152(%rax)
	movq	23(%rax), %rdx
	movq	-1528(%rbp), %rax
	cmpq	%rax, -1544(%rbp)
	je	.L345
	movq	%rdx, (%rax)
	addq	$8, %rax
	movq	%rax, -1528(%rbp)
.L346:
	addl	$1, %r13d
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L369:
	movq	-1(%rax), %rdi
	leaq	-1(%rax), %rsi
	cmpw	$86, 11(%rdi)
	je	.L416
.L371:
	movq	(%rsi), %rax
	cmpw	$96, 11(%rax)
	jne	.L372
	movq	31(%rcx), %rax
	testb	$1, %al
	jne	.L417
.L373:
	movl	67(%rax), %ecx
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L339:
	movq	$0, -1536(%rbp)
.L343:
	movq	(%r12), %rax
	cmpl	$7, 12616(%rax)
	ja	.L384
	movl	12616(%rax), %eax
	leaq	.L359(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal20SamplingHeapProfiler8AddStackEv,"a",@progbits
	.align 4
	.align 4
.L359:
	.long	.L366-.L359
	.long	.L365-.L359
	.long	.L364-.L359
	.long	.L363-.L359
	.long	.L362-.L359
	.long	.L361-.L359
	.long	.L360-.L359
	.long	.L358-.L359
	.section	.text._ZN2v88internal20SamplingHeapProfiler8AddStackEv
	.p2align 4,,10
	.p2align 3
.L416:
	movq	23(%rax), %rsi
	testb	$1, %sil
	je	.L372
	subq	$1, %rsi
	jmp	.L371
.L365:
	leaq	.LC16(%rip), %rdx
	.p2align 4,,10
	.p2align 3
.L357:
	movq	%r13, %rsi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal20SamplingHeapProfiler18FindOrAddChildNodeEPNS1_14AllocationNodeEPKcii
	movq	%rax, %r13
	jmp	.L367
.L366:
	leaq	.LC9(%rip), %rdx
	jmp	.L357
.L358:
	leaq	.LC15(%rip), %rdx
	jmp	.L357
.L360:
	leaq	.LC14(%rip), %rdx
	jmp	.L357
.L361:
	leaq	.LC13(%rip), %rdx
	jmp	.L357
.L362:
	leaq	.LC11(%rip), %rdx
	jmp	.L357
.L363:
	leaq	.LC12(%rip), %rdx
	jmp	.L357
.L364:
	leaq	.LC10(%rip), %rdx
	jmp	.L357
	.p2align 4,,10
	.p2align 3
.L414:
	movq	%r13, %rsi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	.LC18(%rip), %rdx
	call	_ZN2v88internal20SamplingHeapProfiler18FindOrAddChildNodeEPNS1_14AllocationNodeEPKcii
	movq	%rax, %r13
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L345:
	movq	-1544(%rbp), %rcx
	subq	-1536(%rbp), %rcx
	movabsq	$1152921504606846975, %rsi
	movq	%rcx, %rax
	sarq	$3, %rax
	cmpq	%rsi, %rax
	je	.L418
	testq	%rax, %rax
	je	.L379
	movabsq	$9223372036854775800, %rsi
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L419
.L348:
	movq	%rsi, %rdi
	movq	%rcx, -1568(%rbp)
	movq	%rdx, -1560(%rbp)
	movq	%rsi, -1544(%rbp)
	call	_Znwm@PLT
	movq	-1544(%rbp), %rsi
	movq	-1560(%rbp), %rdx
	movq	%rax, %r14
	movq	-1568(%rbp), %rcx
	leaq	(%rax,%rsi), %rax
	leaq	8(%r14), %rsi
	movq	%rax, -1544(%rbp)
.L349:
	movq	%rdx, (%r14,%rcx)
	movq	-1536(%rbp), %rax
	movq	-1528(%rbp), %rcx
	cmpq	%rax, %rcx
	je	.L382
	leaq	-8(%rcx), %rsi
	leaq	15(%r14), %rdx
	subq	%rax, %rsi
	subq	%rax, %rdx
	movq	%rsi, %rdi
	shrq	$3, %rdi
	cmpq	$30, %rdx
	jbe	.L383
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rdi
	je	.L383
	addq	$1, %rdi
	xorl	%edx, %edx
	movq	%rdi, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L352:
	movdqu	(%rax,%rdx), %xmm0
	movups	%xmm0, (%r14,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rcx
	jne	.L352
	movq	%rdi, %r8
	movq	-1536(%rbp), %rax
	andq	$-2, %r8
	leaq	0(,%r8,8), %rcx
	leaq	(%rax,%rcx), %rdx
	addq	%r14, %rcx
	cmpq	%rdi, %r8
	je	.L354
	movq	(%rdx), %rdx
	movq	%rdx, (%rcx)
.L354:
	leaq	16(%r14,%rsi), %rax
	movq	%rax, -1528(%rbp)
.L350:
	movq	-1536(%rbp), %rax
	testq	%rax, %rax
	je	.L355
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L355:
	movq	%r14, -1536(%rbp)
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L417:
	movq	-1(%rax), %rcx
	cmpw	$86, 11(%rcx)
	jne	.L373
	movq	23(%rax), %rax
	jmp	.L373
.L419:
	testq	%rdi, %rdi
	jne	.L420
	movq	$0, -1544(%rbp)
	movl	$8, %esi
	xorl	%r14d, %r14d
	jmp	.L349
.L379:
	movl	$8, %esi
	jmp	.L348
.L383:
	movq	-1536(%rbp), %rdx
	movq	-1528(%rbp), %rdi
	movq	%r14, %rcx
	.p2align 4,,10
	.p2align 3
.L351:
	movq	(%rdx), %rax
	addq	$8, %rdx
	addq	$8, %rcx
	movq	%rax, -8(%rcx)
	cmpq	%rdi, %rdx
	jne	.L351
	jmp	.L354
.L382:
	movq	%rsi, -1528(%rbp)
	jmp	.L350
.L418:
	leaq	.LC17(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L415:
	call	__stack_chk_fail@PLT
.L420:
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdi
	movq	%rax, %rsi
	cmovbe	%rdi, %rsi
	salq	$3, %rsi
	jmp	.L348
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal20SamplingHeapProfiler8AddStackEv
	.cfi_startproc
	.type	_ZN2v88internal20SamplingHeapProfiler8AddStackEv.cold, @function
_ZN2v88internal20SamplingHeapProfiler8AddStackEv.cold:
.LFSB18497:
.L384:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	xorl	%edx, %edx
	jmp	.L357
	.cfi_endproc
.LFE18497:
	.section	.text._ZN2v88internal20SamplingHeapProfiler8AddStackEv
	.size	_ZN2v88internal20SamplingHeapProfiler8AddStackEv, .-_ZN2v88internal20SamplingHeapProfiler8AddStackEv
	.section	.text.unlikely._ZN2v88internal20SamplingHeapProfiler8AddStackEv
	.size	_ZN2v88internal20SamplingHeapProfiler8AddStackEv.cold, .-_ZN2v88internal20SamplingHeapProfiler8AddStackEv.cold
.LCOLDE19:
	.section	.text._ZN2v88internal20SamplingHeapProfiler8AddStackEv
.LHOTE19:
	.section	.text._ZNSt8_Rb_treeImSt4pairIKmjESt10_Select1stIS2_ESt4lessImESaIS2_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS1_EESD_IJEEEEESt17_Rb_tree_iteratorIS2_ESt23_Rb_tree_const_iteratorIS2_EDpOT_,"axG",@progbits,_ZNSt8_Rb_treeImSt4pairIKmjESt10_Select1stIS2_ESt4lessImESaIS2_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS1_EESD_IJEEEEESt17_Rb_tree_iteratorIS2_ESt23_Rb_tree_const_iteratorIS2_EDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeImSt4pairIKmjESt10_Select1stIS2_ESt4lessImESaIS2_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS1_EESD_IJEEEEESt17_Rb_tree_iteratorIS2_ESt23_Rb_tree_const_iteratorIS2_EDpOT_
	.type	_ZNSt8_Rb_treeImSt4pairIKmjESt10_Select1stIS2_ESt4lessImESaIS2_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS1_EESD_IJEEEEESt17_Rb_tree_iteratorIS2_ESt23_Rb_tree_const_iteratorIS2_EDpOT_, @function
_ZNSt8_Rb_treeImSt4pairIKmjESt10_Select1stIS2_ESt4lessImESaIS2_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS1_EESD_IJEEEEESt17_Rb_tree_iteratorIS2_ESt23_Rb_tree_const_iteratorIS2_EDpOT_:
.LFB22058:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	$48, %edi
	subq	$24, %rsp
	call	_Znwm@PLT
	leaq	8(%rbx), %rcx
	movq	%rax, %r13
	movq	(%r14), %rax
	movl	$0, 40(%r13)
	movq	(%rax), %r14
	movq	%r14, 32(%r13)
	cmpq	%r15, %rcx
	je	.L481
	movq	%r15, %r12
	movq	32(%r15), %r15
	cmpq	%r15, %r14
	jnb	.L434
	movq	24(%rbx), %r15
	cmpq	%r12, %r15
	je	.L461
	movq	%r12, %rdi
	movq	%rcx, -56(%rbp)
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	-56(%rbp), %rcx
	cmpq	32(%rax), %r14
	jbe	.L436
	cmpq	$0, 24(%rax)
	je	.L424
.L461:
	movq	%r12, %rax
.L456:
	testq	%rax, %rax
	setne	%al
.L455:
	cmpq	%r12, %rcx
	je	.L469
	testb	%al, %al
	je	.L482
.L469:
	movl	$1, %edi
.L446:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 40(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L434:
	.cfi_restore_state
	jbe	.L432
	cmpq	%r12, 32(%rbx)
	je	.L478
	movq	%r12, %rdi
	movq	%rcx, -56(%rbp)
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	-56(%rbp), %rcx
	cmpq	32(%rax), %r14
	jnb	.L444
	cmpq	$0, 24(%r12)
	je	.L445
	movq	%rax, %r12
	movl	$1, %edi
	jmp	.L446
	.p2align 4,,10
	.p2align 3
.L427:
	movq	%rdx, %r12
	testb	%dil, %dil
	jne	.L425
.L430:
	cmpq	%rsi, %r14
	ja	.L433
	movq	%rdx, %r12
.L432:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L481:
	.cfi_restore_state
	cmpq	$0, 40(%rbx)
	je	.L423
	movq	32(%rbx), %rax
	cmpq	32(%rax), %r14
	ja	.L424
.L423:
	movq	16(%rbx), %rdx
	testq	%rdx, %rdx
	jne	.L426
	jmp	.L483
	.p2align 4,,10
	.p2align 3
.L484:
	movq	16(%rdx), %rax
	movl	$1, %edi
.L429:
	testq	%rax, %rax
	je	.L427
	movq	%rax, %rdx
.L426:
	movq	32(%rdx), %rsi
	cmpq	%rsi, %r14
	jb	.L484
	movq	24(%rdx), %rax
	xorl	%edi, %edi
	jmp	.L429
	.p2align 4,,10
	.p2align 3
.L424:
	movq	%rax, %r12
	xorl	%eax, %eax
	jmp	.L455
	.p2align 4,,10
	.p2align 3
.L436:
	movq	16(%rbx), %r12
	testq	%r12, %r12
	jne	.L438
	jmp	.L485
	.p2align 4,,10
	.p2align 3
.L486:
	movq	16(%r12), %rax
	movl	$1, %esi
.L441:
	testq	%rax, %rax
	je	.L439
	movq	%rax, %r12
.L438:
	movq	32(%r12), %rdx
	cmpq	%rdx, %r14
	jb	.L486
	movq	24(%r12), %rax
	xorl	%esi, %esi
	jmp	.L441
.L485:
	movq	%rcx, %r12
.L437:
	cmpq	%r12, %r15
	je	.L464
.L480:
	movq	%r12, %rdi
	movq	%rcx, -56(%rbp)
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	-56(%rbp), %rcx
	movq	%r12, %rdi
	movq	32(%rax), %rdx
	movq	%rax, %r12
.L452:
	cmpq	%rdx, %r14
	jbe	.L432
.L453:
	movq	%rdi, %r12
.L433:
	testq	%r12, %r12
	je	.L432
.L478:
	xorl	%eax, %eax
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L482:
	movq	32(%r12), %r15
.L445:
	xorl	%edi, %edi
	cmpq	%r15, %r14
	setb	%dil
	jmp	.L446
	.p2align 4,,10
	.p2align 3
.L483:
	movq	%r15, %rdx
.L425:
	cmpq	%rdx, 24(%rbx)
	je	.L459
	movq	%rdx, %rdi
	movq	%rcx, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rcx
	movq	32(%rax), %rsi
	movq	%rdx, %r12
	movq	%rax, %rdx
	jmp	.L430
	.p2align 4,,10
	.p2align 3
.L439:
	movq	%r12, %rdi
	testb	%sil, %sil
	je	.L452
	jmp	.L437
	.p2align 4,,10
	.p2align 3
.L444:
	movq	16(%rbx), %r12
	testq	%r12, %r12
	jne	.L448
	jmp	.L487
	.p2align 4,,10
	.p2align 3
.L488:
	movq	16(%r12), %rax
	movl	$1, %esi
.L451:
	testq	%rax, %rax
	je	.L449
	movq	%rax, %r12
.L448:
	movq	32(%r12), %rdx
	cmpq	%rdx, %r14
	jb	.L488
	movq	24(%r12), %rax
	xorl	%esi, %esi
	jmp	.L451
	.p2align 4,,10
	.p2align 3
.L449:
	movq	%r12, %rdi
	testb	%sil, %sil
	je	.L452
.L447:
	cmpq	%r12, 24(%rbx)
	jne	.L480
	movq	%r12, %rdi
	jmp	.L453
	.p2align 4,,10
	.p2align 3
.L459:
	movq	%rdx, %r12
	jmp	.L433
	.p2align 4,,10
	.p2align 3
.L464:
	movq	%r15, %rdi
	jmp	.L453
.L487:
	movq	%rcx, %r12
	jmp	.L447
	.cfi_endproc
.LFE22058:
	.size	_ZNSt8_Rb_treeImSt4pairIKmjESt10_Select1stIS2_ESt4lessImESaIS2_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS1_EESD_IJEEEEESt17_Rb_tree_iteratorIS2_ESt23_Rb_tree_const_iteratorIS2_EDpOT_, .-_ZNSt8_Rb_treeImSt4pairIKmjESt10_Select1stIS2_ESt4lessImESaIS2_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS1_EESD_IJEEEEESt17_Rb_tree_iteratorIS2_ESt23_Rb_tree_const_iteratorIS2_EDpOT_
	.section	.text._ZNSt8_Rb_treeImSt4pairIKmjESt10_Select1stIS2_ESt4lessImESaIS2_EE5eraseERS1_,"axG",@progbits,_ZNSt8_Rb_treeImSt4pairIKmjESt10_Select1stIS2_ESt4lessImESaIS2_EE5eraseERS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeImSt4pairIKmjESt10_Select1stIS2_ESt4lessImESaIS2_EE5eraseERS1_
	.type	_ZNSt8_Rb_treeImSt4pairIKmjESt10_Select1stIS2_ESt4lessImESaIS2_EE5eraseERS1_, @function
_ZNSt8_Rb_treeImSt4pairIKmjESt10_Select1stIS2_ESt4lessImESaIS2_EE5eraseERS1_:
.LFB22081:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	8(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %r15
	testq	%r15, %r15
	je	.L490
	movq	(%rsi), %rcx
	movq	%r13, %r14
	movq	%r15, %rbx
	jmp	.L491
	.p2align 4,,10
	.p2align 3
.L534:
	movq	24(%rbx), %rax
	testq	%rax, %rax
	je	.L533
.L511:
	movq	%rax, %rbx
.L491:
	cmpq	%rcx, 32(%rbx)
	jb	.L534
	movq	16(%rbx), %rax
	jbe	.L535
	movq	%rbx, %r14
	testq	%rax, %rax
	jne	.L511
.L533:
	movq	40(%r12), %r8
	cmpq	%r14, 24(%r12)
	jne	.L494
	cmpq	%r14, %r13
	je	.L508
.L494:
	xorl	%r8d, %r8d
.L489:
	addq	$24, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L535:
	.cfi_restore_state
	movq	24(%rbx), %rdx
	testq	%rdx, %rdx
	jne	.L498
	jmp	.L504
	.p2align 4,,10
	.p2align 3
.L536:
	movq	%rdx, %r14
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L504
.L498:
	cmpq	32(%rdx), %rcx
	jb	.L536
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L498
	.p2align 4,,10
	.p2align 3
.L504:
	testq	%rax, %rax
	je	.L499
.L537:
	cmpq	32(%rax), %rcx
	ja	.L503
	movq	%rax, %rbx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L537
.L499:
	movq	40(%r12), %r8
	cmpq	%rbx, 24(%r12)
	jne	.L506
	cmpq	%r14, %r13
	je	.L508
.L506:
	cmpq	%rbx, %r14
	je	.L494
	.p2align 4,,10
	.p2align 3
.L510:
	movq	%rbx, %rdi
	movq	%rbx, %r15
	movq	%r8, -56(%rbp)
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZSt28_Rb_tree_rebalance_for_erasePSt18_Rb_tree_node_baseRS_@PLT
	movq	%rax, %rdi
	call	_ZdlPv@PLT
	movq	40(%r12), %rax
	movq	-56(%rbp), %r8
	subq	$1, %rax
	cmpq	%r14, %rbx
	movq	%rax, 40(%r12)
	jne	.L510
	subq	%rax, %r8
	jmp	.L489
	.p2align 4,,10
	.p2align 3
.L503:
	movq	24(%rax), %rax
	jmp	.L504
	.p2align 4,,10
	.p2align 3
.L508:
	movq	24(%r15), %rsi
	movq	%r12, %rdi
	movq	%r8, -56(%rbp)
	call	_ZNSt8_Rb_treeImSt4pairIKmjESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%r15, %rdi
	movq	16(%r15), %r15
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
	testq	%r15, %r15
	jne	.L508
.L507:
	movq	$0, 16(%r12)
	movq	%r13, 24(%r12)
	movq	%r13, 32(%r12)
	movq	$0, 40(%r12)
	jmp	.L489
	.p2align 4,,10
	.p2align 3
.L490:
	movq	40(%rdi), %r8
	cmpq	%r13, 24(%rdi)
	jne	.L494
	jmp	.L507
	.cfi_endproc
.LFE22081:
	.size	_ZNSt8_Rb_treeImSt4pairIKmjESt10_Select1stIS2_ESt4lessImESaIS2_EE5eraseERS1_, .-_ZNSt8_Rb_treeImSt4pairIKmjESt10_Select1stIS2_ESt4lessImESaIS2_EE5eraseERS1_
	.section	.text._ZN2v88internal20SamplingHeapProfiler14OnWeakCallbackERKNS_16WeakCallbackInfoINS1_6SampleEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20SamplingHeapProfiler14OnWeakCallbackERKNS_16WeakCallbackInfoINS1_6SampleEEE
	.type	_ZN2v88internal20SamplingHeapProfiler14OnWeakCallbackERKNS_16WeakCallbackInfoINS1_6SampleEEE, @function
_ZN2v88internal20SamplingHeapProfiler14OnWeakCallbackERKNS_16WeakCallbackInfoINS1_6SampleEEE:
.LFB18495:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	8(%rax), %rbx
	movq	%rax, -96(%rbp)
	movq	16(%rbx), %rdx
	leaq	8(%rbx), %r12
	testq	%rdx, %rdx
	je	.L600
	movq	(%rax), %rax
	movq	%r12, %rsi
	jmp	.L540
	.p2align 4,,10
	.p2align 3
.L679:
	movq	%rdx, %rsi
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L541
.L540:
	cmpq	%rax, 32(%rdx)
	jnb	.L679
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L540
.L541:
	cmpq	%rsi, %r12
	je	.L539
	cmpq	32(%rsi), %rax
	jb	.L539
	subl	$1, 40(%rsi)
	movq	16(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L601
.L684:
	movq	-96(%rbp), %rax
	movq	%r12, %rsi
	movq	(%rax), %rax
	jmp	.L546
	.p2align 4,,10
	.p2align 3
.L680:
	movq	%rdx, %rsi
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L547
.L546:
	cmpq	%rax, 32(%rdx)
	jnb	.L680
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L546
.L547:
	cmpq	%r12, %rsi
	je	.L545
	cmpq	32(%rsi), %rax
	jb	.L545
	movl	40(%rsi), %eax
	testl	%eax, %eax
	je	.L551
.L554:
	movq	-96(%rbp), %rax
	xorl	%edx, %edx
	movq	24(%rax), %rbx
	movq	232(%rbx), %rdi
	movq	224(%rbx), %r11
	divq	%rdi
	leaq	0(,%rdx,8), %r14
	movq	%rdx, %r9
	leaq	(%r11,%r14), %r13
	movq	0(%r13), %r10
	testq	%r10, %r10
	je	.L538
	movq	(%r10), %r12
	movq	%r10, %r8
	movq	8(%r12), %rcx
	jmp	.L591
	.p2align 4,,10
	.p2align 3
.L681:
	movq	(%r12), %rsi
	testq	%rsi, %rsi
	je	.L538
	movq	8(%rsi), %rcx
	xorl	%edx, %edx
	movq	%r12, %r8
	movq	%rcx, %rax
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L538
	movq	%rsi, %r12
.L591:
	cmpq	%rcx, -96(%rbp)
	jne	.L681
	movq	(%r12), %rcx
	cmpq	%r8, %r10
	je	.L682
	testq	%rcx, %rcx
	je	.L593
	movq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	%rdi
	cmpq	%rdx, %r9
	je	.L593
	movq	%r8, (%r11,%rdx,8)
	movq	(%r12), %rcx
.L593:
	movq	%rcx, (%r8)
	movq	16(%r12), %r13
	testq	%r13, %r13
	je	.L595
	movq	16(%r13), %rdi
	testq	%rdi, %rdi
	je	.L596
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L596:
	movl	$40, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L595:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	subq	$1, 248(%rbx)
.L538:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L683
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L600:
	.cfi_restore_state
	movq	%r12, %rsi
	.p2align 4,,10
	.p2align 3
.L539:
	movq	-96(%rbp), %rax
	leaq	-64(%rbp), %rcx
	leaq	-65(%rbp), %r8
	movq	%rbx, %rdi
	leaq	_ZStL19piecewise_construct(%rip), %rdx
	movq	%rax, -64(%rbp)
	call	_ZNSt8_Rb_treeImSt4pairIKmjESt10_Select1stIS2_ESt4lessImESaIS2_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS1_EESD_IJEEEEESt17_Rb_tree_iteratorIS2_ESt23_Rb_tree_const_iteratorIS2_EDpOT_
	movq	%rax, %rsi
	subl	$1, 40(%rsi)
	movq	16(%rbx), %rdx
	testq	%rdx, %rdx
	jne	.L684
.L601:
	movq	%r12, %rsi
	.p2align 4,,10
	.p2align 3
.L545:
	movq	-96(%rbp), %rax
	leaq	-64(%rbp), %rcx
	leaq	-65(%rbp), %r8
	movq	%rbx, %rdi
	leaq	_ZStL19piecewise_construct(%rip), %rdx
	movq	%rax, -64(%rbp)
	call	_ZNSt8_Rb_treeImSt4pairIKmjESt10_Select1stIS2_ESt4lessImESaIS2_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS1_EESD_IJEEEEESt17_Rb_tree_iteratorIS2_ESt23_Rb_tree_const_iteratorIS2_EDpOT_
	movq	%rax, %rsi
	movl	40(%rsi), %eax
	testl	%eax, %eax
	jne	.L554
.L551:
	movq	-96(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmjESt10_Select1stIS2_ESt4lessImESaIS2_EE5eraseERS1_
	cmpq	$0, 40(%rbx)
	jne	.L554
	.p2align 4,,10
	.p2align 3
.L589:
	cmpq	$0, 88(%rbx)
	jne	.L554
	movq	96(%rbx), %r12
	testq	%r12, %r12
	je	.L554
	cmpb	$0, 124(%r12)
	jne	.L554
	movq	112(%rbx), %rcx
	movl	104(%rbx), %edx
	movl	108(%rbx), %eax
	orq	$1, %rcx
	testl	%edx, %edx
	je	.L556
	leal	(%rax,%rax), %ecx
	salq	$32, %rdx
	movslq	%ecx, %rcx
	addq	%rdx, %rcx
.L556:
	movq	64(%r12), %r14
	leaq	56(%r12), %r15
	movq	%r15, -88(%rbp)
	testq	%r14, %r14
	je	.L557
	movq	%r14, %rbx
	jmp	.L558
	.p2align 4,,10
	.p2align 3
.L686:
	movq	24(%rbx), %rax
	testq	%rax, %rax
	je	.L685
.L602:
	movq	%rax, %rbx
.L558:
	cmpq	32(%rbx), %rcx
	ja	.L686
	movq	16(%rbx), %rax
	jnb	.L687
	movq	%rbx, %r15
	testq	%rax, %rax
	jne	.L602
.L685:
	cmpq	%r15, 72(%r12)
	jne	.L574
	cmpq	%r15, -88(%rbp)
	je	.L559
	.p2align 4,,10
	.p2align 3
.L574:
	cmpq	$0, 40(%r12)
	jne	.L554
	movq	%r12, %rbx
	jmp	.L589
	.p2align 4,,10
	.p2align 3
.L687:
	movq	24(%rbx), %rdx
	testq	%rdx, %rdx
	jne	.L565
	jmp	.L571
	.p2align 4,,10
	.p2align 3
.L688:
	movq	%rdx, %r15
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L571
.L565:
	cmpq	32(%rdx), %rcx
	jb	.L688
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L565
	.p2align 4,,10
	.p2align 3
.L571:
	testq	%rax, %rax
	je	.L566
.L689:
	cmpq	32(%rax), %rcx
	ja	.L570
	movq	%rax, %rbx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L689
.L566:
	cmpq	%rbx, 72(%r12)
	jne	.L678
	cmpq	%r15, -88(%rbp)
	je	.L559
	.p2align 4,,10
	.p2align 3
.L678:
	cmpq	%r15, %rbx
	je	.L574
	movq	%rbx, %rdi
	movq	%rbx, %r13
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	-88(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZSt28_Rb_tree_rebalance_for_erasePSt18_Rb_tree_node_baseRS_@PLT
	movq	40(%rax), %r13
	movq	%rax, -104(%rbp)
	testq	%r13, %r13
	je	.L580
	movq	64(%r13), %r14
	leaq	48(%r13), %rax
	movq	%rax, -112(%rbp)
	testq	%r14, %r14
	je	.L587
.L581:
	movq	24(%r14), %rsi
	movq	-112(%rbp), %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal20SamplingHeapProfiler14AllocationNodeESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E
	movq	%r14, %rax
	movq	%r14, -120(%rbp)
	movq	16(%r14), %r14
	movq	40(%rax), %r8
	testq	%r8, %r8
	je	.L584
	movq	64(%r8), %rsi
	leaq	48(%r8), %rdi
	movq	%r8, -128(%rbp)
	call	_ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal20SamplingHeapProfiler14AllocationNodeESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E
	movq	-128(%rbp), %r8
	movq	16(%r8), %rax
	testq	%rax, %rax
	je	.L586
.L585:
	movq	24(%rax), %rsi
	movq	%r8, %rdi
	movq	%r8, -136(%rbp)
	movq	%rax, -128(%rbp)
	call	_ZNSt8_Rb_treeImSt4pairIKmjESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	-128(%rbp), %rax
	movq	%rax, %rdi
	movq	16(%rax), %rax
	movq	%rax, -128(%rbp)
	call	_ZdlPv@PLT
	movq	-128(%rbp), %rax
	movq	-136(%rbp), %r8
	testq	%rax, %rax
	jne	.L585
.L586:
	movl	$128, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L584:
	movq	-120(%rbp), %rdi
	call	_ZdlPv@PLT
	testq	%r14, %r14
	jne	.L581
.L587:
	movq	16(%r13), %r14
	testq	%r14, %r14
	je	.L582
.L583:
	movq	24(%r14), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmjESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%r14, %rdi
	movq	16(%r14), %r14
	call	_ZdlPv@PLT
	testq	%r14, %r14
	jne	.L583
.L582:
	movl	$128, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L580:
	movq	-104(%rbp), %rdi
	call	_ZdlPv@PLT
	subq	$1, 88(%r12)
	jmp	.L678
	.p2align 4,,10
	.p2align 3
.L570:
	movq	24(%rax), %rax
	jmp	.L571
	.p2align 4,,10
	.p2align 3
.L682:
	testq	%rcx, %rcx
	je	.L605
	movq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	%rdi
	cmpq	%rdx, %r9
	je	.L593
	movq	%r8, (%r11,%rdx,8)
	addq	224(%rbx), %r14
	movq	(%r14), %rax
	movq	%r14, %r13
.L592:
	leaq	240(%rbx), %rdx
	cmpq	%rdx, %rax
	je	.L690
.L594:
	movq	$0, 0(%r13)
	movq	(%r12), %rcx
	jmp	.L593
	.p2align 4,,10
	.p2align 3
.L557:
	cmpq	72(%r12), %r15
	jne	.L574
.L578:
	movq	$0, 64(%r12)
	movq	-88(%rbp), %rax
	movq	$0, 88(%r12)
	movq	%rax, 72(%r12)
	movq	%rax, 80(%r12)
	jmp	.L574
	.p2align 4,,10
	.p2align 3
.L559:
	leaq	48(%r12), %rax
	movq	%rax, -104(%rbp)
.L573:
	movq	24(%r14), %rsi
	movq	-104(%rbp), %rdi
	movq	%r14, %r13
	call	_ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal20SamplingHeapProfiler14AllocationNodeESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E
	movq	40(%r13), %rbx
	movq	16(%r14), %r14
	testq	%rbx, %rbx
	je	.L575
	movq	64(%rbx), %rsi
	leaq	48(%rbx), %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmSt10unique_ptrIN2v88internal20SamplingHeapProfiler14AllocationNodeESt14default_deleteIS6_EEESt10_Select1stISA_ESt4lessImESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E
	movq	16(%rbx), %r15
	testq	%r15, %r15
	je	.L577
.L576:
	movq	24(%r15), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmjESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%r15, %rdi
	movq	16(%r15), %r15
	call	_ZdlPv@PLT
	testq	%r15, %r15
	jne	.L576
.L577:
	movl	$128, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L575:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%r14, %r14
	jne	.L573
	jmp	.L578
	.p2align 4,,10
	.p2align 3
.L605:
	movq	%r8, %rax
	jmp	.L592
.L690:
	movq	%rcx, 240(%rbx)
	jmp	.L594
.L683:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18495:
	.size	_ZN2v88internal20SamplingHeapProfiler14OnWeakCallbackERKNS_16WeakCallbackInfoINS1_6SampleEEE, .-_ZN2v88internal20SamplingHeapProfiler14OnWeakCallbackERKNS_16WeakCallbackInfoINS1_6SampleEEE
	.section	.text._ZNSt8_Rb_treeIiSt4pairIKiN2v88internal6HandleINS3_6ScriptEEEESt10_Select1stIS7_ESt4lessIiESaIS7_EE8_M_eraseEPSt13_Rb_tree_nodeIS7_E,"axG",@progbits,_ZNSt8_Rb_treeIiSt4pairIKiN2v88internal6HandleINS3_6ScriptEEEESt10_Select1stIS7_ESt4lessIiESaIS7_EE8_M_eraseEPSt13_Rb_tree_nodeIS7_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIiSt4pairIKiN2v88internal6HandleINS3_6ScriptEEEESt10_Select1stIS7_ESt4lessIiESaIS7_EE8_M_eraseEPSt13_Rb_tree_nodeIS7_E
	.type	_ZNSt8_Rb_treeIiSt4pairIKiN2v88internal6HandleINS3_6ScriptEEEESt10_Select1stIS7_ESt4lessIiESaIS7_EE8_M_eraseEPSt13_Rb_tree_nodeIS7_E, @function
_ZNSt8_Rb_treeIiSt4pairIKiN2v88internal6HandleINS3_6ScriptEEEESt10_Select1stIS7_ESt4lessIiESaIS7_EE8_M_eraseEPSt13_Rb_tree_nodeIS7_E:
.LFB22161:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L699
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L693:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIiSt4pairIKiN2v88internal6HandleINS3_6ScriptEEEESt10_Select1stIS7_ESt4lessIiESaIS7_EE8_M_eraseEPSt13_Rb_tree_nodeIS7_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L693
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L699:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE22161:
	.size	_ZNSt8_Rb_treeIiSt4pairIKiN2v88internal6HandleINS3_6ScriptEEEESt10_Select1stIS7_ESt4lessIiESaIS7_EE8_M_eraseEPSt13_Rb_tree_nodeIS7_E, .-_ZNSt8_Rb_treeIiSt4pairIKiN2v88internal6HandleINS3_6ScriptEEEESt10_Select1stIS7_ESt4lessIiESaIS7_EE8_M_eraseEPSt13_Rb_tree_nodeIS7_E
	.section	.text._ZNSt6vectorIN2v817AllocationProfile6SampleESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v817AllocationProfile6SampleESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v817AllocationProfile6SampleESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.type	_ZNSt6vectorIN2v817AllocationProfile6SampleESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, @function
_ZNSt6vectorIN2v817AllocationProfile6SampleESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_:
.LFB22318:
	.cfi_startproc
	endbr64
	movabsq	$288230376151711743, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r9
	movq	(%rdi), %r15
	movq	%r9, %rax
	subq	%r15, %rax
	sarq	$5, %rax
	cmpq	%rcx, %rax
	je	.L716
	movq	%rdx, %r13
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r15, %rdx
	testq	%rax, %rax
	je	.L712
	movabsq	$9223372036854775776, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L717
.L704:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L711:
	movdqu	0(%r13), %xmm1
	movdqu	16(%r13), %xmm2
	subq	%r8, %r9
	leaq	32(%rbx,%rdx), %r10
	leaq	(%r10,%r9), %rax
	movq	%r9, %r13
	movq	%rax, -56(%rbp)
	movups	%xmm1, (%rbx,%rdx)
	movups	%xmm2, 16(%rbx,%rdx)
	testq	%rdx, %rdx
	jg	.L718
	testq	%r9, %r9
	jg	.L707
	testq	%r15, %r15
	jne	.L710
.L708:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L718:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r10, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r10
	movq	-72(%rbp), %r8
	jg	.L707
.L710:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	jmp	.L708
	.p2align 4,,10
	.p2align 3
.L717:
	testq	%rsi, %rsi
	jne	.L705
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L711
	.p2align 4,,10
	.p2align 3
.L707:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r10, %rdi
	call	memcpy@PLT
	testq	%r15, %r15
	je	.L708
	jmp	.L710
	.p2align 4,,10
	.p2align 3
.L712:
	movl	$32, %r14d
	jmp	.L704
.L716:
	leaq	.LC17(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L705:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$5, %r14
	jmp	.L704
	.cfi_endproc
.LFE22318:
	.size	_ZNSt6vectorIN2v817AllocationProfile6SampleESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, .-_ZNSt6vectorIN2v817AllocationProfile6SampleESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.section	.rodata._ZNK2v88internal20SamplingHeapProfiler12BuildSamplesEv.str1.1,"aMS",@progbits,1
.LC20:
	.string	"vector::reserve"
	.section	.text._ZNK2v88internal20SamplingHeapProfiler12BuildSamplesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal20SamplingHeapProfiler12BuildSamplesEv
	.type	_ZNK2v88internal20SamplingHeapProfiler12BuildSamplesEv, @function
_ZNK2v88internal20SamplingHeapProfiler12BuildSamplesEv:
.LFB18577:
	.cfi_startproc
	endbr64
	movabsq	$288230376151711743, %rdx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, 16(%rdi)
	movups	%xmm0, (%rdi)
	movq	248(%rsi), %rax
	cmpq	%rdx, %rax
	ja	.L747
	movq	%rdi, %r13
	movq	%rsi, %rbx
	testq	%rax, %rax
	jne	.L748
.L721:
	movq	240(%rbx), %r14
	testq	%r14, %r14
	je	.L719
	leaq	-96(%rbp), %r12
	.p2align 4,,10
	.p2align 3
.L732:
	movq	16(%r14), %r15
	movq	8(%r15), %rax
	movl	120(%rax), %eax
	movl	%eax, -96(%rbp)
	movq	(%r15), %rax
	movq	%rax, -88(%rbp)
	testq	%rax, %rax
	js	.L726
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.L727:
	movq	288(%rbx), %rax
	xorpd	.LC0(%rip), %xmm0
	testq	%rax, %rax
	js	.L728
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rax, %xmm1
.L729:
	divsd	%xmm1, %xmm0
	call	exp@PLT
	movsd	.LC3(%rip), %xmm2
	movq	8(%r13), %rsi
	movsd	.LC3(%rip), %xmm3
	subsd	%xmm0, %xmm2
	movsd	.LC4(%rip), %xmm0
	divsd	%xmm2, %xmm3
	addsd	%xmm3, %xmm0
	cvttsd2siq	%xmm0, %rax
	movl	%eax, -80(%rbp)
	movq	32(%r15), %rax
	movq	%rax, -72(%rbp)
	cmpq	16(%r13), %rsi
	je	.L730
	movdqa	-96(%rbp), %xmm4
	movups	%xmm4, (%rsi)
	movdqa	-80(%rbp), %xmm5
	movups	%xmm5, 16(%rsi)
	addq	$32, 8(%r13)
	movq	(%r14), %r14
	testq	%r14, %r14
	jne	.L732
.L719:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L749
	addq	$56, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L728:
	.cfi_restore_state
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm1, %xmm1
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm1
	addsd	%xmm1, %xmm1
	jmp	.L729
	.p2align 4,,10
	.p2align 3
.L726:
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L727
	.p2align 4,,10
	.p2align 3
.L730:
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIN2v817AllocationProfile6SampleESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	movq	(%r14), %r14
	testq	%r14, %r14
	jne	.L732
	jmp	.L719
	.p2align 4,,10
	.p2align 3
.L748:
	salq	$5, %rax
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_Znwm@PLT
	movq	0(%r13), %r15
	movq	8(%r13), %rdx
	movq	%rax, %r14
	subq	%r15, %rdx
	testq	%rdx, %rdx
	jg	.L750
	testq	%r15, %r15
	jne	.L723
.L724:
	movq	%r14, %xmm0
	leaq	(%r14,%r12), %rax
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, 16(%r13)
	movups	%xmm0, 0(%r13)
	jmp	.L721
	.p2align 4,,10
	.p2align 3
.L750:
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	memmove@PLT
.L723:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	jmp	.L724
.L749:
	call	__stack_chk_fail@PLT
.L747:
	leaq	.LC20(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE18577:
	.size	_ZNK2v88internal20SamplingHeapProfiler12BuildSamplesEv, .-_ZNK2v88internal20SamplingHeapProfiler12BuildSamplesEv
	.section	.text._ZNSt8_Rb_treeIiSt4pairIKiN2v88internal6HandleINS3_6ScriptEEEESt10_Select1stIS7_ESt4lessIiESaIS7_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS7_ERS1_,"axG",@progbits,_ZNSt8_Rb_treeIiSt4pairIKiN2v88internal6HandleINS3_6ScriptEEEESt10_Select1stIS7_ESt4lessIiESaIS7_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS7_ERS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIiSt4pairIKiN2v88internal6HandleINS3_6ScriptEEEESt10_Select1stIS7_ESt4lessIiESaIS7_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS7_ERS1_
	.type	_ZNSt8_Rb_treeIiSt4pairIKiN2v88internal6HandleINS3_6ScriptEEEESt10_Select1stIS7_ESt4lessIiESaIS7_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS7_ERS1_, @function
_ZNSt8_Rb_treeIiSt4pairIKiN2v88internal6HandleINS3_6ScriptEEEESt10_Select1stIS7_ESt4lessIiESaIS7_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS7_ERS1_:
.LFB23045:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	8(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	cmpq	%rsi, %r15
	je	.L801
	movl	(%rdx), %r14d
	cmpl	32(%rsi), %r14d
	jge	.L762
	movq	24(%rdi), %rbx
	movq	%rbx, %rax
	movq	%rbx, %rdx
	cmpq	%rsi, %rbx
	je	.L754
	movq	%rsi, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %rdx
	cmpl	32(%rax), %r14d
	jle	.L764
	movl	$0, %ebx
	cmpq	$0, 24(%rax)
	movq	%rbx, %rax
	cmovne	%r12, %rdx
	cmovne	%r12, %rax
.L754:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L762:
	.cfi_restore_state
	jle	.L773
	movq	32(%rdi), %rdx
	cmpq	%rsi, %rdx
	je	.L800
	movq	%rsi, %rdi
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %rdx
	cmpl	32(%rax), %r14d
	jge	.L775
	movl	$0, %ebx
	cmpq	$0, 24(%r12)
	movq	%rbx, %rax
	cmovne	%rdx, %rax
	cmove	%r12, %rdx
	jmp	.L754
	.p2align 4,,10
	.p2align 3
.L801:
	cmpq	$0, 40(%rdi)
	je	.L753
	movq	32(%rdi), %rdx
	movl	(%r14), %eax
	cmpl	%eax, 32(%rdx)
	jl	.L800
.L753:
	movq	16(%r13), %rbx
	testq	%rbx, %rbx
	je	.L784
	movl	(%r14), %esi
	jmp	.L756
	.p2align 4,,10
	.p2align 3
.L802:
	movq	16(%rbx), %rax
	movl	$1, %ecx
	testq	%rax, %rax
	je	.L757
.L803:
	movq	%rax, %rbx
.L756:
	movl	32(%rbx), %edx
	cmpl	%edx, %esi
	jl	.L802
	movq	24(%rbx), %rax
	xorl	%ecx, %ecx
	testq	%rax, %rax
	jne	.L803
.L757:
	movq	%rbx, %r12
	testb	%cl, %cl
	jne	.L755
.L760:
	xorl	%eax, %eax
	cmpl	%esi, %edx
	cmovl	%rax, %rbx
	cmovge	%rax, %r12
.L761:
	addq	$8, %rsp
	movq	%rbx, %rax
	movq	%r12, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L773:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%rsi, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L800:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L764:
	.cfi_restore_state
	movq	16(%r13), %r12
	testq	%r12, %r12
	jne	.L767
	jmp	.L804
	.p2align 4,,10
	.p2align 3
.L805:
	movq	16(%r12), %rax
	movl	$1, %esi
.L770:
	testq	%rax, %rax
	je	.L768
	movq	%rax, %r12
.L767:
	movl	32(%r12), %ecx
	cmpl	%ecx, %r14d
	jl	.L805
	movq	24(%r12), %rax
	xorl	%esi, %esi
	jmp	.L770
	.p2align 4,,10
	.p2align 3
.L784:
	movq	%r12, %rbx
.L755:
	cmpq	%rbx, 24(%r13)
	je	.L786
	movq	%rbx, %rdi
	movq	%rbx, %r12
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movl	(%r14), %esi
	movl	32(%rax), %edx
	movq	%rax, %rbx
	jmp	.L760
	.p2align 4,,10
	.p2align 3
.L768:
	movq	%r12, %rdx
	testb	%sil, %sil
	jne	.L766
.L771:
	xorl	%eax, %eax
	cmpl	%ecx, %r14d
	cmovg	%rax, %r12
	cmovle	%rax, %rdx
.L772:
	movq	%r12, %rax
	jmp	.L754
.L804:
	movq	%r15, %r12
.L766:
	cmpq	%r12, %rbx
	je	.L790
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%r12, %rdx
	movl	32(%rax), %ecx
	movq	%rax, %r12
	jmp	.L771
	.p2align 4,,10
	.p2align 3
.L775:
	movq	16(%r13), %rbx
	testq	%rbx, %rbx
	jne	.L778
	jmp	.L806
	.p2align 4,,10
	.p2align 3
.L807:
	movq	16(%rbx), %rax
	movl	$1, %esi
.L781:
	testq	%rax, %rax
	je	.L779
	movq	%rax, %rbx
.L778:
	movl	32(%rbx), %ecx
	cmpl	%ecx, %r14d
	jl	.L807
	movq	24(%rbx), %rax
	xorl	%esi, %esi
	jmp	.L781
	.p2align 4,,10
	.p2align 3
.L779:
	movq	%rbx, %rdx
	testb	%sil, %sil
	jne	.L777
.L782:
	xorl	%eax, %eax
	cmpl	%ecx, %r14d
	cmovg	%rax, %rbx
	cmovle	%rax, %rdx
.L783:
	movq	%rbx, %rax
	jmp	.L754
	.p2align 4,,10
	.p2align 3
.L786:
	movq	%rbx, %r12
	xorl	%ebx, %ebx
	jmp	.L761
.L806:
	movq	%r15, %rbx
.L777:
	cmpq	%rbx, 24(%r13)
	je	.L794
	movq	%rbx, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%rbx, %rdx
	movl	32(%rax), %ecx
	movq	%rax, %rbx
	jmp	.L782
	.p2align 4,,10
	.p2align 3
.L790:
	movq	%r12, %rdx
	xorl	%r12d, %r12d
	jmp	.L772
.L794:
	movq	%rbx, %rdx
	xorl	%ebx, %ebx
	jmp	.L783
	.cfi_endproc
.LFE23045:
	.size	_ZNSt8_Rb_treeIiSt4pairIKiN2v88internal6HandleINS3_6ScriptEEEESt10_Select1stIS7_ESt4lessIiESaIS7_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS7_ERS1_, .-_ZNSt8_Rb_treeIiSt4pairIKiN2v88internal6HandleINS3_6ScriptEEEESt10_Select1stIS7_ESt4lessIiESaIS7_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS7_ERS1_
	.section	.rodata._ZNSt5dequeIN2v817AllocationProfile4NodeESaIS2_EE16_M_push_back_auxIJS2_EEEvDpOT_.str1.8,"aMS",@progbits,1
	.align 8
.LC21:
	.string	"cannot create std::deque larger than max_size()"
	.section	.text._ZNSt5dequeIN2v817AllocationProfile4NodeESaIS2_EE16_M_push_back_auxIJS2_EEEvDpOT_,"axG",@progbits,_ZNSt5dequeIN2v817AllocationProfile4NodeESaIS2_EE16_M_push_back_auxIJS2_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeIN2v817AllocationProfile4NodeESaIS2_EE16_M_push_back_auxIJS2_EEEvDpOT_
	.type	_ZNSt5dequeIN2v817AllocationProfile4NodeESaIS2_EE16_M_push_back_auxIJS2_EEEvDpOT_, @function
_ZNSt5dequeIN2v817AllocationProfile4NodeESaIS2_EE16_M_push_back_auxIJS2_EEEvDpOT_:
.LFB23076:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	72(%rdi), %r14
	movq	40(%rdi), %rsi
	movq	48(%rdi), %rdx
	subq	56(%rdi), %rdx
	movabsq	$3353953467947191203, %rdi
	movq	%r14, %r13
	sarq	$3, %rdx
	subq	%rsi, %r13
	imulq	%rdi, %rdx
	movq	%r13, %rcx
	sarq	$3, %rcx
	leaq	-5(%rcx,%rcx,4), %rax
	addq	%rax, %rdx
	movq	32(%rbx), %rax
	subq	16(%rbx), %rax
	sarq	$3, %rax
	imulq	%rdi, %rax
	addq	%rdx, %rax
	movabsq	$104811045873349725, %rdx
	cmpq	%rdx, %rax
	je	.L817
	movq	(%rbx), %rdi
	movq	8(%rbx), %rdx
	movq	%r14, %rax
	subq	%rdi, %rax
	movq	%rdx, %r8
	sarq	$3, %rax
	subq	%rax, %r8
	cmpq	$1, %r8
	jbe	.L818
.L810:
	movl	$440, %edi
	call	_Znwm@PLT
	movq	%rax, 8(%r14)
	movq	48(%rbx), %rax
	movq	(%r12), %rdx
	movdqu	16(%r12), %xmm1
	movq	%rdx, (%rax)
	movq	8(%r12), %rdx
	movups	%xmm1, 16(%rax)
	movq	%rdx, 8(%rax)
	movl	32(%r12), %edx
	movl	%edx, 32(%rax)
	movq	40(%r12), %rdx
	movq	$0, 40(%r12)
	movq	%rdx, 40(%rax)
	movq	48(%r12), %rdx
	movq	$0, 48(%r12)
	movq	%rdx, 48(%rax)
	movq	56(%r12), %rdx
	movq	$0, 56(%r12)
	movq	%rdx, 56(%rax)
	movq	64(%r12), %rdx
	movq	$0, 64(%r12)
	movq	%rdx, 64(%rax)
	movq	72(%r12), %rdx
	movq	$0, 72(%r12)
	movq	%rdx, 72(%rax)
	movq	80(%r12), %rdx
	movq	$0, 80(%r12)
	movq	%rdx, 80(%rax)
	movq	72(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 72(%rbx)
	movq	8(%rax), %rax
	leaq	440(%rax), %rdx
	movq	%rax, 56(%rbx)
	movq	%rdx, 64(%rbx)
	movq	%rax, 48(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L818:
	.cfi_restore_state
	leaq	2(%rcx), %r15
	leaq	(%r15,%r15), %rax
	cmpq	%rax, %rdx
	ja	.L819
	testq	%rdx, %rdx
	movl	$1, %eax
	cmovne	%rdx, %rax
	leaq	2(%rdx,%rax), %r14
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %r14
	ja	.L820
	leaq	0(,%r14,8), %rdi
	call	_Znwm@PLT
	movq	40(%rbx), %rsi
	movq	%rax, %rcx
	movq	%rax, -56(%rbp)
	movq	%r14, %rax
	subq	%r15, %rax
	shrq	%rax
	leaq	(%rcx,%rax,8), %r15
	movq	72(%rbx), %rax
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L815
	subq	%rsi, %rdx
	movq	%r15, %rdi
	call	memmove@PLT
.L815:
	movq	(%rbx), %rdi
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
	movq	%r14, 8(%rbx)
	movq	%rax, (%rbx)
.L813:
	movq	%r15, 40(%rbx)
	movq	(%r15), %rax
	leaq	(%r15,%r13), %r14
	movq	(%r15), %xmm0
	movq	%r14, 72(%rbx)
	addq	$440, %rax
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 24(%rbx)
	movq	(%r14), %rax
	movq	%rax, 56(%rbx)
	addq	$440, %rax
	movq	%rax, 64(%rbx)
	jmp	.L810
	.p2align 4,,10
	.p2align 3
.L819:
	subq	%r15, %rdx
	addq	$8, %r14
	shrq	%rdx
	leaq	(%rdi,%rdx,8), %r15
	movq	%r14, %rdx
	subq	%rsi, %rdx
	cmpq	%r15, %rsi
	jbe	.L812
	cmpq	%r14, %rsi
	je	.L813
	movq	%r15, %rdi
	call	memmove@PLT
	jmp	.L813
	.p2align 4,,10
	.p2align 3
.L812:
	cmpq	%r14, %rsi
	je	.L813
	leaq	8(%r13), %rdi
	subq	%rdx, %rdi
	addq	%r15, %rdi
	call	memmove@PLT
	jmp	.L813
.L817:
	leaq	.LC21(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L820:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE23076:
	.size	_ZNSt5dequeIN2v817AllocationProfile4NodeESaIS2_EE16_M_push_back_auxIJS2_EEEvDpOT_, .-_ZNSt5dequeIN2v817AllocationProfile4NodeESaIS2_EE16_M_push_back_auxIJS2_EEEvDpOT_
	.section	.text._ZNSt6vectorIPN2v817AllocationProfile4NodeESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v817AllocationProfile4NodeESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v817AllocationProfile4NodeESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.type	_ZNSt6vectorIPN2v817AllocationProfile4NodeESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, @function
_ZNSt6vectorIPN2v817AllocationProfile4NodeESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_:
.LFB23080:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L835
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L831
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L836
.L823:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L830:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L837
	testq	%r13, %r13
	jg	.L826
	testq	%r9, %r9
	jne	.L829
.L827:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L837:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L826
.L829:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L827
	.p2align 4,,10
	.p2align 3
.L836:
	testq	%rsi, %rsi
	jne	.L824
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L830
	.p2align 4,,10
	.p2align 3
.L826:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L827
	jmp	.L829
	.p2align 4,,10
	.p2align 3
.L831:
	movl	$8, %r14d
	jmp	.L823
.L835:
	leaq	.LC17(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L824:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L823
	.cfi_endproc
.LFE23080:
	.size	_ZNSt6vectorIPN2v817AllocationProfile4NodeESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, .-_ZNSt6vectorIPN2v817AllocationProfile4NodeESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.section	.text._ZNSt10_HashtableIPN2v88internal20SamplingHeapProfiler6SampleESt4pairIKS4_St10unique_ptrIS3_St14default_deleteIS3_EEESaISB_ENSt8__detail10_Select1stESt8equal_toIS4_ESt4hashIS4_ENSD_18_Mod_range_hashingENSD_20_Default_ranged_hashENSD_20_Prime_rehash_policyENSD_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSD_10_Hash_nodeISB_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIPN2v88internal20SamplingHeapProfiler6SampleESt4pairIKS4_St10unique_ptrIS3_St14default_deleteIS3_EEESaISB_ENSt8__detail10_Select1stESt8equal_toIS4_ESt4hashIS4_ENSD_18_Mod_range_hashingENSD_20_Default_ranged_hashENSD_20_Prime_rehash_policyENSD_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSD_10_Hash_nodeISB_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPN2v88internal20SamplingHeapProfiler6SampleESt4pairIKS4_St10unique_ptrIS3_St14default_deleteIS3_EEESaISB_ENSt8__detail10_Select1stESt8equal_toIS4_ESt4hashIS4_ENSD_18_Mod_range_hashingENSD_20_Default_ranged_hashENSD_20_Prime_rehash_policyENSD_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSD_10_Hash_nodeISB_Lb0EEEm
	.type	_ZNSt10_HashtableIPN2v88internal20SamplingHeapProfiler6SampleESt4pairIKS4_St10unique_ptrIS3_St14default_deleteIS3_EEESaISB_ENSt8__detail10_Select1stESt8equal_toIS4_ESt4hashIS4_ENSD_18_Mod_range_hashingENSD_20_Default_ranged_hashENSD_20_Prime_rehash_policyENSD_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSD_10_Hash_nodeISB_Lb0EEEm, @function
_ZNSt10_HashtableIPN2v88internal20SamplingHeapProfiler6SampleESt4pairIKS4_St10unique_ptrIS3_St14default_deleteIS3_EEESaISB_ENSt8__detail10_Select1stESt8equal_toIS4_ESt4hashIS4_ENSD_18_Mod_range_hashingENSD_20_Default_ranged_hashENSD_20_Prime_rehash_policyENSD_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSD_10_Hash_nodeISB_Lb0EEEm:
.LFB23398:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L839
	movq	(%rbx), %r8
.L840:
	movq	(%r8,%r15,8), %rax
	leaq	0(,%r15,8), %rcx
	testq	%rax, %rax
	je	.L849
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r13, (%rax)
.L850:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L839:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L863
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L864
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%rbx), %r10
	movq	%rax, %r8
.L842:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L844
	xorl	%edi, %edi
	leaq	16(%rbx), %r9
	jmp	.L845
	.p2align 4,,10
	.p2align 3
.L846:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L847:
	testq	%rsi, %rsi
	je	.L844
.L845:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	8(%rcx), %rax
	divq	%r12
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L846
	movq	16(%rbx), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L852
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L845
	.p2align 4,,10
	.p2align 3
.L844:
	movq	(%rbx), %rdi
	cmpq	%r10, %rdi
	je	.L848
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L848:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	divq	%r12
	movq	%r8, (%rbx)
	movq	%rdx, %r15
	jmp	.L840
	.p2align 4,,10
	.p2align 3
.L849:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L851
	movq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r13, (%rax,%rdx,8)
.L851:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L850
	.p2align 4,,10
	.p2align 3
.L852:
	movq	%rdx, %rdi
	jmp	.L847
	.p2align 4,,10
	.p2align 3
.L863:
	leaq	48(%rbx), %r8
	movq	$0, 48(%rbx)
	movq	%r8, %r10
	jmp	.L842
.L864:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE23398:
	.size	_ZNSt10_HashtableIPN2v88internal20SamplingHeapProfiler6SampleESt4pairIKS4_St10unique_ptrIS3_St14default_deleteIS3_EEESaISB_ENSt8__detail10_Select1stESt8equal_toIS4_ESt4hashIS4_ENSD_18_Mod_range_hashingENSD_20_Default_ranged_hashENSD_20_Prime_rehash_policyENSD_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSD_10_Hash_nodeISB_Lb0EEEm, .-_ZNSt10_HashtableIPN2v88internal20SamplingHeapProfiler6SampleESt4pairIKS4_St10unique_ptrIS3_St14default_deleteIS3_EEESaISB_ENSt8__detail10_Select1stESt8equal_toIS4_ESt4hashIS4_ENSD_18_Mod_range_hashingENSD_20_Default_ranged_hashENSD_20_Prime_rehash_policyENSD_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSD_10_Hash_nodeISB_Lb0EEEm
	.section	.text._ZN2v88internal20SamplingHeapProfiler12SampleObjectEmm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20SamplingHeapProfiler12SampleObjectEmm
	.type	_ZN2v88internal20SamplingHeapProfiler12SampleObjectEmm, @function
_ZN2v88internal20SamplingHeapProfiler12SampleObjectEmm:
.LFB18493:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	addq	$1, %rsi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	%rdx, -88(%rbp)
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	41088(%r12), %rax
	addl	$1, 41104(%r12)
	movq	(%rdi), %r15
	movq	%rax, -120(%rbp)
	movq	41096(%r12), %rax
	movq	41112(%r15), %rdi
	movq	%rax, -96(%rbp)
	testq	%rdi, %rdi
	je	.L866
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L867:
	movl	-88(%rbp), %edx
	movq	8(%rbx), %rdi
	movq	%r13, %rsi
	movl	$1, %r8d
	movl	$1, %ecx
	call	_ZN2v88internal4Heap20CreateFillerObjectAtEmiNS0_18ClearRecordedSlotsENS0_20ClearFreedMemoryModeE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal20SamplingHeapProfiler8AddStackEv
	movq	%rax, %r13
	leaq	8(%rax), %rax
	movq	16(%r13), %rdx
	testq	%rdx, %rdx
	je	.L886
	movq	-88(%rbp), %rcx
	movq	%rax, %rsi
	jmp	.L870
	.p2align 4,,10
	.p2align 3
.L904:
	movq	%rdx, %rsi
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L871
.L870:
	cmpq	%rcx, 32(%rdx)
	jnb	.L904
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L870
.L871:
	cmpq	%rsi, %rax
	je	.L869
	cmpq	32(%rsi), %rcx
	jb	.L869
.L874:
	addl	$1, 40(%rsi)
	movq	16(%rbx), %rax
	movl	$40, %edi
	movq	%rcx, -112(%rbp)
	addq	$1, %rax
	movq	%rax, 16(%rbx)
	movq	%rax, -104(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %rcx
	movq	(%rbx), %rdi
	movq	%r13, 8(%rax)
	movq	%rax, %r15
	movq	%rcx, (%rax)
	testq	%r14, %r14
	je	.L875
	movq	%r14, %rsi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, %r14
.L875:
	movq	-104(%rbp), %rax
	movq	%rbx, %xmm1
	movq	%r14, %xmm0
	xorl	%ecx, %ecx
	punpcklqdq	%xmm1, %xmm0
	leaq	_ZN2v88internal20SamplingHeapProfiler14OnWeakCallbackERKNS_16WeakCallbackInfoINS1_6SampleEEE(%rip), %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, 32(%r15)
	movups	%xmm0, 16(%r15)
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	movq	16(%r15), %rax
	testq	%rax, %rax
	je	.L876
	orb	$8, 11(%rax)
.L876:
	movl	$24, %edi
	call	_Znwm@PLT
	movq	232(%rbx), %rsi
	xorl	%edx, %edx
	movq	$0, (%rax)
	movq	%rax, %r13
	movq	%r15, 8(%rax)
	movq	%r15, 16(%rax)
	movq	%r15, %rax
	divq	%rsi
	movq	224(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	testq	%rax, %rax
	je	.L877
	movq	(%rax), %rcx
	movq	8(%rcx), %rdi
	jmp	.L879
	.p2align 4,,10
	.p2align 3
.L905:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L877
	movq	8(%rcx), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rsi
	cmpq	%rdx, %r10
	jne	.L877
.L879:
	cmpq	%rdi, %r15
	jne	.L905
	movq	16(%r15), %rdi
	testq	%rdi, %rdi
	je	.L883
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L883:
	movq	%r15, %rdi
	movl	$40, %esi
	call	_ZdlPvm@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L880:
	subl	$1, 41104(%r12)
	movq	-120(%rbp), %rax
	movq	%rax, 41088(%r12)
	movq	-96(%rbp), %rax
	cmpq	41096(%r12), %rax
	je	.L865
	movq	%rax, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L865:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L906
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L886:
	.cfi_restore_state
	movq	%rax, %rsi
	.p2align 4,,10
	.p2align 3
.L869:
	leaq	-88(%rbp), %rax
	leaq	-64(%rbp), %rcx
	movq	%r13, %rdi
	leaq	-65(%rbp), %r8
	leaq	_ZStL19piecewise_construct(%rip), %rdx
	movq	%rax, -64(%rbp)
	call	_ZNSt8_Rb_treeImSt4pairIKmjESt10_Select1stIS2_ESt4lessImESaIS2_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS1_EESD_IJEEEEESt17_Rb_tree_iteratorIS2_ESt23_Rb_tree_const_iteratorIS2_EDpOT_
	movq	-88(%rbp), %rcx
	movq	%rax, %rsi
	jmp	.L874
	.p2align 4,,10
	.p2align 3
.L866:
	movq	41088(%r15), %r14
	cmpq	41096(%r15), %r14
	je	.L907
.L868:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r14)
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L877:
	leaq	224(%rbx), %rdi
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r10, %rsi
	movl	$1, %r8d
	call	_ZNSt10_HashtableIPN2v88internal20SamplingHeapProfiler6SampleESt4pairIKS4_St10unique_ptrIS3_St14default_deleteIS3_EEESaISB_ENSt8__detail10_Select1stESt8equal_toIS4_ESt4hashIS4_ENSD_18_Mod_range_hashingENSD_20_Default_ranged_hashENSD_20_Prime_rehash_policyENSD_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSD_10_Hash_nodeISB_Lb0EEEm
	jmp	.L880
	.p2align 4,,10
	.p2align 3
.L907:
	movq	%r15, %rdi
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L868
.L906:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18493:
	.size	_ZN2v88internal20SamplingHeapProfiler12SampleObjectEmm, .-_ZN2v88internal20SamplingHeapProfiler12SampleObjectEmm
	.section	.text._ZN2v88internal20SamplingHeapProfiler8Observer4StepEimm,"axG",@progbits,_ZN2v88internal20SamplingHeapProfiler8Observer4StepEimm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20SamplingHeapProfiler8Observer4StepEimm
	.type	_ZN2v88internal20SamplingHeapProfiler8Observer4StepEimm, @function
_ZN2v88internal20SamplingHeapProfiler8Observer4StepEimm:
.LFB7539:
	.cfi_startproc
	endbr64
	movq	%rdx, %rsi
	testq	%rdx, %rdx
	jne	.L910
	ret
	.p2align 4,,10
	.p2align 3
.L910:
	movq	24(%rdi), %rdi
	movq	%rcx, %rdx
	jmp	_ZN2v88internal20SamplingHeapProfiler12SampleObjectEmm
	.cfi_endproc
.LFE7539:
	.size	_ZN2v88internal20SamplingHeapProfiler8Observer4StepEimm, .-_ZN2v88internal20SamplingHeapProfiler8Observer4StepEimm
	.section	.text._ZNSt8_Rb_treeIiSt4pairIKiN2v88internal6HandleINS3_6ScriptEEEESt10_Select1stIS7_ESt4lessIiESaIS7_EE7_M_copyINSD_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS7_EPKSH_PSt18_Rb_tree_node_baseRT_,"axG",@progbits,_ZNSt8_Rb_treeIiSt4pairIKiN2v88internal6HandleINS3_6ScriptEEEESt10_Select1stIS7_ESt4lessIiESaIS7_EE7_M_copyINSD_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS7_EPKSH_PSt18_Rb_tree_node_baseRT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIiSt4pairIKiN2v88internal6HandleINS3_6ScriptEEEESt10_Select1stIS7_ESt4lessIiESaIS7_EE7_M_copyINSD_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS7_EPKSH_PSt18_Rb_tree_node_baseRT_
	.type	_ZNSt8_Rb_treeIiSt4pairIKiN2v88internal6HandleINS3_6ScriptEEEESt10_Select1stIS7_ESt4lessIiESaIS7_EE7_M_copyINSD_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS7_EPKSH_PSt18_Rb_tree_node_baseRT_, @function
_ZNSt8_Rb_treeIiSt4pairIKiN2v88internal6HandleINS3_6ScriptEEEESt10_Select1stIS7_ESt4lessIiESaIS7_EE7_M_copyINSD_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS7_EPKSH_PSt18_Rb_tree_node_baseRT_:
.LFB23461:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	%rdi, -56(%rbp)
	movl	$48, %edi
	call	_Znwm@PLT
	movl	32(%rbx), %edx
	movq	24(%rbx), %rsi
	movq	%rax, %r13
	movq	40(%rbx), %rax
	movl	%edx, 32(%r13)
	movq	%rax, 40(%r13)
	movl	(%rbx), %eax
	movq	$0, 16(%r13)
	movl	%eax, 0(%r13)
	movq	$0, 24(%r13)
	movq	%r12, 8(%r13)
	testq	%rsi, %rsi
	je	.L912
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIiSt4pairIKiN2v88internal6HandleINS3_6ScriptEEEESt10_Select1stIS7_ESt4lessIiESaIS7_EE7_M_copyINSD_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS7_EPKSH_PSt18_Rb_tree_node_baseRT_
	movq	%rax, 24(%r13)
.L912:
	movq	16(%rbx), %r12
	testq	%r12, %r12
	je	.L911
	movq	%r13, %r14
.L916:
	movl	$48, %edi
	call	_Znwm@PLT
	movl	32(%r12), %ecx
	movq	%rax, %rbx
	movq	40(%r12), %rax
	movl	%ecx, 32(%rbx)
	movq	%rax, 40(%rbx)
	movl	(%r12), %eax
	movq	$0, 16(%rbx)
	movl	%eax, (%rbx)
	movq	$0, 24(%rbx)
	movq	%rbx, 16(%r14)
	movq	%r14, 8(%rbx)
	movq	24(%r12), %rsi
	testq	%rsi, %rsi
	je	.L914
	movq	-56(%rbp), %rdi
	movq	%r15, %rcx
	movq	%rbx, %rdx
	call	_ZNSt8_Rb_treeIiSt4pairIKiN2v88internal6HandleINS3_6ScriptEEEESt10_Select1stIS7_ESt4lessIiESaIS7_EE7_M_copyINSD_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS7_EPKSH_PSt18_Rb_tree_node_baseRT_
	movq	%rax, 24(%rbx)
	movq	16(%r12), %r12
	testq	%r12, %r12
	je	.L911
.L915:
	movq	%rbx, %r14
	jmp	.L916
	.p2align 4,,10
	.p2align 3
.L914:
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L915
.L911:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23461:
	.size	_ZNSt8_Rb_treeIiSt4pairIKiN2v88internal6HandleINS3_6ScriptEEEESt10_Select1stIS7_ESt4lessIiESaIS7_EE7_M_copyINSD_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS7_EPKSH_PSt18_Rb_tree_node_baseRT_, .-_ZNSt8_Rb_treeIiSt4pairIKiN2v88internal6HandleINS3_6ScriptEEEESt10_Select1stIS7_ESt4lessIiESaIS7_EE7_M_copyINSD_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS7_EPKSH_PSt18_Rb_tree_node_baseRT_
	.section	.rodata._ZN2v88internal20SamplingHeapProfiler23TranslateAllocationNodeEPNS0_17AllocationProfileEPNS1_14AllocationNodeERKSt3mapIiNS0_6HandleINS0_6ScriptEEESt4lessIiESaISt4pairIKiS9_EEE.str1.1,"aMS",@progbits,1
.LC22:
	.string	""
	.section	.text._ZN2v88internal20SamplingHeapProfiler23TranslateAllocationNodeEPNS0_17AllocationProfileEPNS1_14AllocationNodeERKSt3mapIiNS0_6HandleINS0_6ScriptEEESt4lessIiESaISt4pairIKiS9_EEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20SamplingHeapProfiler23TranslateAllocationNodeEPNS0_17AllocationProfileEPNS1_14AllocationNodeERKSt3mapIiNS0_6HandleINS0_6ScriptEEESt4lessIiESaISt4pairIKiS9_EEE
	.type	_ZN2v88internal20SamplingHeapProfiler23TranslateAllocationNodeEPNS0_17AllocationProfileEPNS1_14AllocationNodeERKSt3mapIiNS0_6HandleINS0_6ScriptEEESt4lessIiESaISt4pairIKiS9_EEE, @function
_ZN2v88internal20SamplingHeapProfiler23TranslateAllocationNodeEPNS0_17AllocationProfileEPNS1_14AllocationNodeERKSt3mapIiNS0_6HandleINS0_6ScriptEEESt4lessIiESaISt4pairIKiS9_EEE:
.LFB18510:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$200, %rsp
	movq	%rsi, -192(%rbp)
	movq	%rdx, -200(%rbp)
	movq	%rcx, -208(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movb	$1, 124(%rdx)
	leaq	.LC22(%rip), %rax
	movq	(%rdi), %rdi
	movq	%rax, -144(%rbp)
	leaq	-144(%rbp), %rax
	movq	%rax, %rsi
	movq	%rax, -216(%rbp)
	movq	$0, -136(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movabsq	$576460752303423487, %rdx
	movq	%rax, -232(%rbp)
	movq	40(%r14), %rax
	cmpq	%rdx, %rax
	ja	.L1024
	movq	$0, -168(%rbp)
	xorl	%r14d, %r14d
	testq	%rax, %rax
	jne	.L1025
.L932:
	movq	-200(%rbp), %rax
	movl	$0, -224(%rbp)
	movl	104(%rax), %edi
	movl	%edi, -220(%rbp)
	testl	%edi, %edi
	jne	.L1026
.L933:
	movq	-200(%rbp), %rax
	movq	%r14, -176(%rbp)
	movq	24(%rax), %r15
	addq	$8, %rax
	movq	%rax, -184(%rbp)
	cmpq	%r15, %rax
	je	.L998
	movq	-168(%rbp), %r13
	movq	%rbx, -168(%rbp)
	jmp	.L969
	.p2align 4,,10
	.p2align 3
.L1028:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rbx, %xmm0
.L956:
	movq	-168(%rbp), %rax
	xorpd	.LC0(%rip), %xmm0
	movq	288(%rax), %rax
	testq	%rax, %rax
	js	.L957
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rax, %xmm1
.L958:
	divsd	%xmm1, %xmm0
	call	exp@PLT
	movsd	.LC3(%rip), %xmm2
	pxor	%xmm1, %xmm1
	movsd	.LC3(%rip), %xmm3
	cvtsi2sdq	%r12, %xmm1
	subsd	%xmm0, %xmm2
	divsd	%xmm2, %xmm3
	movapd	%xmm3, %xmm0
	mulsd	%xmm1, %xmm0
	addsd	.LC4(%rip), %xmm0
	cvttsd2siq	%xmm0, %r12
	cmpq	%r14, %r13
	je	.L961
	movq	%rbx, (%r14)
	addq	$16, %r14
	movl	%r12d, -8(%r14)
.L962:
	movq	%r15, %rdi
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %r15
	cmpq	%rax, -184(%rbp)
	je	.L1027
.L969:
	movq	32(%r15), %rbx
	movl	40(%r15), %r12d
	testq	%rbx, %rbx
	jns	.L1028
	movq	%rbx, %rax
	movq	%rbx, %rcx
	pxor	%xmm0, %xmm0
	shrq	%rax
	andl	$1, %ecx
	orq	%rcx, %rax
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L956
	.p2align 4,,10
	.p2align 3
.L957:
	movq	%rax, %rcx
	andl	$1, %eax
	pxor	%xmm1, %xmm1
	shrq	%rcx
	orq	%rax, %rcx
	cvtsi2sdq	%rcx, %xmm1
	addsd	%xmm1, %xmm1
	jmp	.L958
	.p2align 4,,10
	.p2align 3
.L961:
	movabsq	$576460752303423487, %rsi
	movq	%r13, %rdx
	subq	-176(%rbp), %rdx
	movq	%rdx, %rax
	sarq	$4, %rax
	cmpq	%rsi, %rax
	je	.L1029
	testq	%rax, %rax
	je	.L999
	movabsq	$9223372036854775792, %r13
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L1030
.L964:
	movq	%r13, %rdi
	movq	%rdx, -240(%rbp)
	call	_Znwm@PLT
	movq	-240(%rbp), %rdx
	movq	%rax, %rcx
	addq	%rax, %r13
.L965:
	leaq	(%rcx,%rdx), %rax
	leaq	16(%rcx,%rdx), %r14
	movq	%rbx, (%rax)
	movl	%r12d, 8(%rax)
	testq	%rdx, %rdx
	jg	.L1031
	cmpq	$0, -176(%rbp)
	jne	.L967
.L968:
	movq	%rcx, -176(%rbp)
	jmp	.L962
	.p2align 4,,10
	.p2align 3
.L1030:
	testq	%rcx, %rcx
	jne	.L1032
	xorl	%r13d, %r13d
	xorl	%ecx, %ecx
	jmp	.L965
	.p2align 4,,10
	.p2align 3
.L1027:
	movq	%r14, %r13
	subq	-176(%rbp), %r13
	movq	-168(%rbp), %rbx
	movq	%r13, %rdx
	sarq	$4, %rdx
.L954:
	movq	-200(%rbp), %r12
	movq	(%rbx), %r8
	movq	%rdx, -184(%rbp)
	movq	112(%r12), %r15
	movq	%r8, -168(%rbp)
	movq	%r15, %rdi
	call	strlen@PLT
	movq	-168(%rbp), %r8
	movq	-216(%rbp), %rsi
	movq	%r15, -144(%rbp)
	movq	%rax, -136(%rbp)
	movq	%r8, %rdi
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	-184(%rbp), %rdx
	pxor	%xmm0, %xmm0
	xorl	%edi, %edi
	movq	%rax, -144(%rbp)
	movq	-232(%rbp), %rax
	movups	%xmm0, -104(%rbp)
	movq	%rax, -136(%rbp)
	movq	104(%r12), %rax
	movups	%xmm0, -88(%rbp)
	movq	%rax, -128(%rbp)
	movl	-220(%rbp), %eax
	movups	%xmm0, -72(%rbp)
	movl	%eax, -120(%rbp)
	movl	-224(%rbp), %eax
	movl	%eax, -116(%rbp)
	movl	120(%r12), %eax
	movl	%eax, -112(%rbp)
	testq	%rdx, %rdx
	je	.L971
	movabsq	$576460752303423487, %rax
	cmpq	%rax, %rdx
	ja	.L1033
	movq	%r13, %rdi
	call	_Znwm@PLT
	movq	%rax, %rdi
.L971:
	movq	-176(%rbp), %rax
	leaq	(%rdi,%r13), %r15
	movq	%rdi, -80(%rbp)
	movq	%r15, -64(%rbp)
	cmpq	%rax, %r14
	je	.L973
	movq	%r13, %rdx
	movq	%rax, %rsi
	call	memcpy@PLT
.L973:
	movq	-192(%rbp), %rsi
	movq	%r15, -72(%rbp)
	movq	72(%rsi), %rdi
	movq	56(%rsi), %rax
	leaq	-88(%rdi), %rdx
	movq	%rdi, -168(%rbp)
	cmpq	%rdx, %rax
	je	.L974
	movq	-144(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	-136(%rbp), %rdx
	movq	%rdx, 8(%rax)
	movl	-128(%rbp), %edx
	movl	%edx, 16(%rax)
	movl	-124(%rbp), %edx
	movl	%edx, 20(%rax)
	movl	-120(%rbp), %edx
	movl	%edx, 24(%rax)
	movl	-116(%rbp), %edx
	movl	%edx, 28(%rax)
	movl	-112(%rbp), %edx
	movl	%edx, 32(%rax)
	movq	-104(%rbp), %rdx
	movq	%rdx, 40(%rax)
	movq	-96(%rbp), %rdx
	movq	%rdx, 48(%rax)
	movq	-88(%rbp), %rdx
	movq	%rdx, 56(%rax)
	movq	-80(%rbp), %rdx
	movq	%rdx, 64(%rax)
	movq	-72(%rbp), %rdx
	movq	%rdx, 72(%rax)
	movq	-64(%rbp), %rdx
	movq	%rdx, 80(%rax)
	addq	$88, 56(%rsi)
.L975:
	movq	-192(%rbp), %rax
	movq	56(%rax), %r14
	cmpq	64(%rax), %r14
	je	.L1034
.L978:
	leaq	-88(%r14), %rax
	leaq	-48(%r14), %r12
	movq	%rax, -168(%rbp)
	movq	-200(%rbp), %rax
	movq	72(%rax), %r15
	leaq	56(%rax), %r13
	cmpq	%r15, %r13
	je	.L984
	movq	%r15, %rax
	movq	%r12, %r15
	movq	%r14, %r12
	movq	%r13, %r14
	movq	%rax, %r13
.L985:
	movq	-192(%rbp), %rsi
	movq	40(%r13), %rdx
	movq	%rbx, %rdi
	movq	-208(%rbp), %rcx
	call	_ZN2v88internal20SamplingHeapProfiler23TranslateAllocationNodeEPNS0_17AllocationProfileEPNS1_14AllocationNodeERKSt3mapIiNS0_6HandleINS0_6ScriptEEESt4lessIiESaISt4pairIKiS9_EEE
	movq	%rax, -144(%rbp)
	movq	-40(%r12), %rsi
	cmpq	-32(%r12), %rsi
	je	.L982
	movq	%rax, (%rsi)
	addq	$8, -40(%r12)
.L1023:
	movq	%r13, %rdi
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %r13
	cmpq	%rax, %r14
	jne	.L985
.L984:
	movq	-200(%rbp), %rax
	cmpq	$0, -176(%rbp)
	movb	$0, 124(%rax)
	je	.L930
	movq	-176(%rbp), %rdi
	call	_ZdlPv@PLT
.L930:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1035
	movq	-168(%rbp), %rax
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1031:
	.cfi_restore_state
	movq	-176(%rbp), %rsi
	movq	%rcx, %rdi
	call	memmove@PLT
	movq	%rax, %rcx
.L967:
	movq	-176(%rbp), %rdi
	movq	%rcx, -240(%rbp)
	call	_ZdlPv@PLT
	movq	-240(%rbp), %rcx
	jmp	.L968
	.p2align 4,,10
	.p2align 3
.L982:
	movq	-216(%rbp), %rdx
	movq	%r15, %rdi
	call	_ZNSt6vectorIPN2v817AllocationProfile4NodeESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L1023
	.p2align 4,,10
	.p2align 3
.L974:
	leaq	8(%rsi), %rdi
	movq	-216(%rbp), %rsi
	call	_ZNSt5dequeIN2v817AllocationProfile4NodeESaIS2_EE16_M_push_back_auxIJS2_EEEvDpOT_
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1021
	call	_ZdlPv@PLT
.L1021:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L975
	call	_ZdlPv@PLT
	jmp	.L975
	.p2align 4,,10
	.p2align 3
.L1026:
	movq	-208(%rbp), %rax
	movq	16(%rax), %rsi
	leaq	8(%rax), %rcx
	testq	%rsi, %rsi
	je	.L1020
	movq	%rcx, %rdx
	movq	%rsi, %rax
	jmp	.L934
	.p2align 4,,10
	.p2align 3
.L1036:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L935
.L934:
	cmpl	32(%rax), %edi
	jle	.L1036
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L934
.L935:
	cmpq	%rdx, %rcx
	je	.L993
	movl	-220(%rbp), %eax
	cmpl	32(%rdx), %eax
	jl	.L993
	leaq	-136(%rbp), %r13
	movq	-216(%rbp), %rdi
	leaq	-160(%rbp), %r12
	movl	$0, -136(%rbp)
	movq	%r13, %rdx
	movq	%r12, %rcx
	movq	$0, -128(%rbp)
	movq	%r13, -120(%rbp)
	movq	%r13, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%rdi, -160(%rbp)
	call	_ZNSt8_Rb_treeIiSt4pairIKiN2v88internal6HandleINS3_6ScriptEEEESt10_Select1stIS7_ESt4lessIiESaIS7_EE7_M_copyINSD_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS7_EPKSH_PSt18_Rb_tree_node_baseRT_
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L938:
	movq	%rdx, %rcx
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L938
	movq	%rcx, -120(%rbp)
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L939:
	movq	%rdx, %rcx
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L939
	movq	-208(%rbp), %rsi
	movq	%rcx, -112(%rbp)
	movq	%rax, -128(%rbp)
	movq	40(%rsi), %rdx
	movq	%rdx, -104(%rbp)
	testq	%rax, %rax
	je	.L994
	movq	-200(%rbp), %rsi
	movl	104(%rsi), %edx
	movq	%r13, %rsi
	jmp	.L941
	.p2align 4,,10
	.p2align 3
.L1037:
	movq	%rax, %rsi
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L942
.L941:
	cmpl	%edx, 32(%rax)
	jge	.L1037
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L941
.L942:
	cmpq	%r13, %rsi
	je	.L940
	cmpl	32(%rsi), %edx
	jl	.L940
	movq	40(%rsi), %r8
	testq	%r8, %r8
	je	.L1038
.L986:
	movq	(%r8), %rax
	movq	15(%rax), %rsi
	testb	$1, %sil
	jne	.L1039
.L950:
	movq	-200(%rbp), %r15
	movq	%r8, %rdi
	movq	%r8, -176(%rbp)
	movl	108(%r15), %esi
	call	_ZN2v88internal6Script13GetLineNumberENS0_6HandleIS1_EEi@PLT
	movq	-176(%rbp), %r8
	movl	108(%r15), %esi
	addl	$1, %eax
	movq	%r8, %rdi
	movl	%eax, -220(%rbp)
	call	_ZN2v88internal6Script15GetColumnNumberENS0_6HandleIS1_EEi@PLT
	addl	$1, %eax
	movl	%eax, -224(%rbp)
.L949:
	movq	-128(%rbp), %r13
	testq	%r13, %r13
	je	.L933
	movq	-216(%rbp), %r12
	.p2align 4,,10
	.p2align 3
.L953:
	movq	24(%r13), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIiSt4pairIKiN2v88internal6HandleINS3_6ScriptEEEESt10_Select1stIS7_ESt4lessIiESaIS7_EE8_M_eraseEPSt13_Rb_tree_nodeIS7_E
	movq	%r13, %rdi
	movq	16(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L953
	jmp	.L933
	.p2align 4,,10
	.p2align 3
.L999:
	movl	$16, %r13d
	jmp	.L964
	.p2align 4,,10
	.p2align 3
.L993:
	movl	$0, -224(%rbp)
.L1020:
	movl	$0, -220(%rbp)
	jmp	.L933
	.p2align 4,,10
	.p2align 3
.L1025:
	salq	$4, %rax
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_Znwm@PLT
	movq	%rax, %r14
	leaq	(%rax,%r15), %rax
	movq	%rax, -168(%rbp)
	jmp	.L932
	.p2align 4,,10
	.p2align 3
.L1034:
	movq	80(%rax), %rax
	movq	-8(%rax), %r9
	leaq	440(%r9), %r14
	jmp	.L978
.L994:
	movq	%r13, %rsi
	.p2align 4,,10
	.p2align 3
.L940:
	movl	$48, %edi
	movq	%rsi, -176(%rbp)
	call	_Znwm@PLT
	movq	-176(%rbp), %rsi
	movq	-216(%rbp), %rdi
	movq	%rax, %r15
	movq	-200(%rbp), %rax
	movq	$0, 40(%r15)
	leaq	32(%r15), %rdx
	movl	104(%rax), %eax
	movl	%eax, 32(%r15)
	call	_ZNSt8_Rb_treeIiSt4pairIKiN2v88internal6HandleINS3_6ScriptEEEESt10_Select1stIS7_ESt4lessIiESaIS7_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS7_ERS1_
	testq	%rdx, %rdx
	je	.L946
	testq	%rax, %rax
	jne	.L996
	cmpq	%rdx, %r13
	jne	.L1040
.L996:
	movl	$1, %edi
.L947:
	movq	%r13, %rcx
	movq	%r15, %rsi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, -104(%rbp)
.L948:
	movq	40(%r15), %r8
	testq	%r8, %r8
	jne	.L986
	movl	$0, -224(%rbp)
	movl	$0, -220(%rbp)
	jmp	.L949
	.p2align 4,,10
	.p2align 3
.L998:
	xorl	%edx, %edx
	xorl	%r13d, %r13d
	jmp	.L954
.L1039:
	movq	-1(%rsi), %rax
	cmpw	$64, 11(%rax)
	ja	.L950
	movq	88(%rbx), %rdi
	movq	(%rbx), %r15
	movq	%r8, -184(%rbp)
	call	_ZN2v88internal14StringsStorage7GetNameENS0_4NameE@PLT
	movq	%rax, %rdi
	movq	%rax, -176(%rbp)
	call	strlen@PLT
	movq	-176(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, -152(%rbp)
	movq	%rdx, -160(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	-184(%rbp), %r8
	movq	%rax, -232(%rbp)
	jmp	.L950
.L1040:
	xorl	%edi, %edi
	movl	32(%rdx), %eax
	cmpl	%eax, 32(%r15)
	setl	%dil
	jmp	.L947
.L946:
	movq	%r15, %rdi
	movq	%rax, -176(%rbp)
	call	_ZdlPv@PLT
	movq	-176(%rbp), %rax
	movq	%rax, %r15
	jmp	.L948
.L1038:
	movl	$0, -224(%rbp)
	movq	-128(%rbp), %r13
	movl	$0, -220(%rbp)
	movq	-216(%rbp), %r12
	jmp	.L953
.L1029:
	leaq	.LC17(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1024:
	leaq	.LC20(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1035:
	call	__stack_chk_fail@PLT
.L1032:
	movabsq	$576460752303423487, %rax
	cmpq	%rax, %rcx
	movq	%rax, %r13
	cmovbe	%rcx, %r13
	salq	$4, %r13
	jmp	.L964
.L1033:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE18510:
	.size	_ZN2v88internal20SamplingHeapProfiler23TranslateAllocationNodeEPNS0_17AllocationProfileEPNS1_14AllocationNodeERKSt3mapIiNS0_6HandleINS0_6ScriptEEESt4lessIiESaISt4pairIKiS9_EEE, .-_ZN2v88internal20SamplingHeapProfiler23TranslateAllocationNodeEPNS0_17AllocationProfileEPNS1_14AllocationNodeERKSt3mapIiNS0_6HandleINS0_6ScriptEEESt4lessIiESaISt4pairIKiS9_EEE
	.section	.text._ZN2v88internal20SamplingHeapProfiler20GetAllocationProfileEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20SamplingHeapProfiler20GetAllocationProfileEv
	.type	_ZN2v88internal20SamplingHeapProfiler20GetAllocationProfileEv, @function
_ZN2v88internal20SamplingHeapProfiler20GetAllocationProfileEv:
.LFB18548:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -152(%rbp)
	movq	(%rdi), %rsi
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	testb	$1, 296(%rdi)
	jne	.L1076
.L1042:
	leaq	-144(%rbp), %r13
	leaq	-104(%rbp), %rbx
	movl	$0, -104(%rbp)
	movq	%r13, %rdi
	movq	$0, -96(%rbp)
	movq	%rbx, -88(%rbp)
	movq	%rbx, -80(%rbp)
	movq	$0, -72(%rbp)
	call	_ZN2v88internal6Script8IteratorC1EPNS0_7IsolateE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal6Script8Iterator4NextEv@PLT
	movq	%rax, %r12
	leaq	-112(%rbp), %rax
	movq	%rax, -160(%rbp)
	testq	%r12, %r12
	je	.L1058
	.p2align 4,,10
	.p2align 3
.L1043:
	movq	-152(%rbp), %rax
	movslq	67(%r12), %rcx
	movq	%rbx, %r15
	movq	-96(%rbp), %rdx
	movq	(%rax), %r14
	movl	%ecx, %eax
	testq	%rdx, %rdx
	jne	.L1047
	jmp	.L1046
	.p2align 4,,10
	.p2align 3
.L1077:
	movq	%rdx, %r15
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L1048
.L1047:
	cmpl	32(%rdx), %eax
	jle	.L1077
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L1047
.L1048:
	cmpq	%rbx, %r15
	je	.L1046
	cmpl	32(%r15), %ecx
	jge	.L1051
.L1046:
	movl	$48, %edi
	movq	%rcx, -176(%rbp)
	movq	%r15, -168(%rbp)
	call	_Znwm@PLT
	movq	-176(%rbp), %rcx
	movq	-168(%rbp), %rsi
	movq	$0, 40(%rax)
	movq	-160(%rbp), %rdi
	leaq	32(%rax), %rdx
	movq	%rax, %r15
	movl	%ecx, 32(%rax)
	call	_ZNSt8_Rb_treeIiSt4pairIKiN2v88internal6HandleINS3_6ScriptEEEESt10_Select1stIS7_ESt4lessIiESaIS7_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS7_ERS1_
	testq	%rdx, %rdx
	je	.L1052
	testq	%rax, %rax
	jne	.L1064
	cmpq	%rdx, %rbx
	jne	.L1078
.L1064:
	movl	$1, %edi
.L1053:
	movq	%rbx, %rcx
	movq	%r15, %rsi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, -72(%rbp)
.L1051:
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1055
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L1056:
	movq	%rax, 40(%r15)
	movq	%r13, %rdi
	call	_ZN2v88internal6Script8Iterator4NextEv@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L1043
.L1058:
	movl	$112, %edi
	call	_Znwm@PLT
	movl	$14, %ecx
	movq	%rax, %r15
	xorl	%eax, %eax
	movq	%r15, %rdi
	rep stosq
	leaq	16+_ZTVN2v88internal17AllocationProfileE(%rip), %rax
	movq	$8, 16(%r15)
	movl	$64, %edi
	movq	%rax, (%r15)
	call	_Znwm@PLT
	movq	16(%r15), %rdx
	movl	$440, %edi
	movq	%rax, 8(%r15)
	leaq	-4(,%rdx,4), %rbx
	andq	$-8, %rbx
	addq	%rax, %rbx
	call	_Znwm@PLT
	movq	%rbx, 48(%r15)
	movq	%r15, %rsi
	movq	-160(%rbp), %rcx
	leaq	440(%rax), %rdx
	movq	%rax, %xmm0
	movq	%rax, (%rbx)
	movq	%rdx, 40(%r15)
	punpcklqdq	%xmm0, %xmm0
	movq	%rbx, 80(%r15)
	movq	-152(%rbp), %rbx
	movq	%rdx, 72(%r15)
	movq	%rax, 64(%r15)
	leaq	96(%rbx), %rdx
	movq	%rbx, %rdi
	movq	%rax, 56(%r15)
	movq	$0, 104(%r15)
	movups	%xmm0, 24(%r15)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 88(%r15)
	call	_ZN2v88internal20SamplingHeapProfiler23TranslateAllocationNodeEPNS0_17AllocationProfileEPNS1_14AllocationNodeERKSt3mapIiNS0_6HandleINS0_6ScriptEEESt4lessIiESaISt4pairIKiS9_EEE
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZNK2v88internal20SamplingHeapProfiler12BuildSamplesEv
	leaq	88(%r15), %rdi
	movq	%r13, %rsi
	call	_ZNSt6vectorIN2v817AllocationProfile6SampleESaIS2_EEaSERKS4_
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1045
	call	_ZdlPv@PLT
.L1045:
	movq	-96(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L1041
.L1059:
	movq	24(%rbx), %rsi
	movq	-160(%rbp), %rdi
	call	_ZNSt8_Rb_treeIiSt4pairIKiN2v88internal6HandleINS3_6ScriptEEEESt10_Select1stIS7_ESt4lessIiESaIS7_EE8_M_eraseEPSt13_Rb_tree_nodeIS7_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1059
.L1041:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1079
	addq	$136, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1055:
	.cfi_restore_state
	movq	41088(%r14), %rax
	cmpq	41096(%r14), %rax
	je	.L1080
.L1057:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r14)
	movq	%r12, (%rax)
	jmp	.L1056
	.p2align 4,,10
	.p2align 3
.L1052:
	movq	%r15, %rdi
	movq	%rax, -168(%rbp)
	call	_ZdlPv@PLT
	movq	-168(%rbp), %rax
	movq	%rax, %r15
	jmp	.L1051
	.p2align 4,,10
	.p2align 3
.L1078:
	xorl	%edi, %edi
	movl	32(%rdx), %eax
	cmpl	%eax, 32(%r15)
	setl	%dil
	jmp	.L1053
	.p2align 4,,10
	.p2align 3
.L1080:
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L1057
	.p2align 4,,10
	.p2align 3
.L1076:
	leaq	37592(%rsi), %rdi
	movl	$19, %edx
	xorl	%esi, %esi
	call	_ZN2v88internal4Heap17CollectAllGarbageEiNS0_23GarbageCollectionReasonENS_15GCCallbackFlagsE@PLT
	movq	-152(%rbp), %rax
	movq	(%rax), %rsi
	jmp	.L1042
.L1079:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18548:
	.size	_ZN2v88internal20SamplingHeapProfiler20GetAllocationProfileEv, .-_ZN2v88internal20SamplingHeapProfiler20GetAllocationProfileEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal20SamplingHeapProfiler8Observer21GetNextSampleIntervalEm,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal20SamplingHeapProfiler8Observer21GetNextSampleIntervalEm, @function
_GLOBAL__sub_I__ZN2v88internal20SamplingHeapProfiler8Observer21GetNextSampleIntervalEm:
.LFB24115:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE24115:
	.size	_GLOBAL__sub_I__ZN2v88internal20SamplingHeapProfiler8Observer21GetNextSampleIntervalEm, .-_GLOBAL__sub_I__ZN2v88internal20SamplingHeapProfiler8Observer21GetNextSampleIntervalEm
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal20SamplingHeapProfiler8Observer21GetNextSampleIntervalEm
	.weak	_ZTVN2v88internal17AllocationProfileE
	.section	.data.rel.ro.local._ZTVN2v88internal17AllocationProfileE,"awG",@progbits,_ZTVN2v88internal17AllocationProfileE,comdat
	.align 8
	.type	_ZTVN2v88internal17AllocationProfileE, @object
	.size	_ZTVN2v88internal17AllocationProfileE, 48
_ZTVN2v88internal17AllocationProfileE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal17AllocationProfile11GetRootNodeEv
	.quad	_ZN2v88internal17AllocationProfile10GetSamplesEv
	.quad	_ZN2v88internal17AllocationProfileD1Ev
	.quad	_ZN2v88internal17AllocationProfileD0Ev
	.weak	_ZTVN2v88internal20SamplingHeapProfiler8ObserverE
	.section	.data.rel.ro.local._ZTVN2v88internal20SamplingHeapProfiler8ObserverE,"awG",@progbits,_ZTVN2v88internal20SamplingHeapProfiler8ObserverE,comdat
	.align 8
	.type	_ZTVN2v88internal20SamplingHeapProfiler8ObserverE, @object
	.size	_ZTVN2v88internal20SamplingHeapProfiler8ObserverE, 48
_ZTVN2v88internal20SamplingHeapProfiler8ObserverE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal20SamplingHeapProfiler8ObserverD1Ev
	.quad	_ZN2v88internal20SamplingHeapProfiler8ObserverD0Ev
	.quad	_ZN2v88internal20SamplingHeapProfiler8Observer4StepEimm
	.quad	_ZN2v88internal20SamplingHeapProfiler8Observer15GetNextStepSizeEv
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata._ZStL19piecewise_construct,"a"
	.type	_ZStL19piecewise_construct, @object
	.size	_ZStL19piecewise_construct, 1
_ZStL19piecewise_construct:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	0
	.long	1075838976
	.align 8
.LC2:
	.long	4290772992
	.long	1105199103
	.align 8
.LC3:
	.long	0
	.long	1072693248
	.align 8
.LC4:
	.long	0
	.long	1071644672
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
