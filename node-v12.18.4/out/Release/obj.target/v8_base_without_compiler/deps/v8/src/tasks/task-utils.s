	.file	"task-utils.cc"
	.text
	.section	.text._ZN2v88internal12_GLOBAL__N_118CancelableFuncTask11RunInternalEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_118CancelableFuncTask11RunInternalEv, @function
_ZN2v88internal12_GLOBAL__N_118CancelableFuncTask11RunInternalEv:
.LFB3746:
	.cfi_startproc
	endbr64
	cmpq	$0, 56(%rdi)
	je	.L7
	leaq	40(%rdi), %r8
	movq	64(%rdi), %rax
	movq	%r8, %rdi
	jmp	*%rax
.L7:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZSt25__throw_bad_function_callv@PLT
	.cfi_endproc
.LFE3746:
	.size	_ZN2v88internal12_GLOBAL__N_118CancelableFuncTask11RunInternalEv, .-_ZN2v88internal12_GLOBAL__N_118CancelableFuncTask11RunInternalEv
	.section	.text._ZN2v88internal12_GLOBAL__N_122CancelableIdleFuncTask11RunInternalEd,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_122CancelableIdleFuncTask11RunInternalEd, @function
_ZN2v88internal12_GLOBAL__N_122CancelableIdleFuncTask11RunInternalEd:
.LFB3765:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rdx
	movq	%rdx, -8(%rbp)
	xorl	%edx, %edx
	cmpq	$0, 56(%rdi)
	movsd	%xmm0, -16(%rbp)
	je	.L12
	movq	%rdi, %rax
	leaq	-16(%rbp), %rsi
	leaq	40(%rdi), %rdi
	call	*64(%rax)
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L13
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L12:
	.cfi_restore_state
	call	_ZSt25__throw_bad_function_callv@PLT
.L13:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3765:
	.size	_ZN2v88internal12_GLOBAL__N_122CancelableIdleFuncTask11RunInternalEd, .-_ZN2v88internal12_GLOBAL__N_122CancelableIdleFuncTask11RunInternalEd
	.section	.text._ZN2v88internal12_GLOBAL__N_118CancelableFuncTaskD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_118CancelableFuncTaskD2Ev, @function
_ZN2v88internal12_GLOBAL__N_118CancelableFuncTaskD2Ev:
.LFB4275:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal12_GLOBAL__N_118CancelableFuncTaskE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	addq	$48, %rax
	movq	%rax, 32(%rdi)
	movq	56(%rdi), %rax
	testq	%rax, %rax
	je	.L15
	leaq	40(%rdi), %rdi
	movl	$3, %edx
	movq	%rdi, %rsi
	call	*%rax
.L15:
	leaq	16+_ZTVN2v88internal14CancelableTaskE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal10CancelableD2Ev@PLT
	.cfi_endproc
.LFE4275:
	.size	_ZN2v88internal12_GLOBAL__N_118CancelableFuncTaskD2Ev, .-_ZN2v88internal12_GLOBAL__N_118CancelableFuncTaskD2Ev
	.set	_ZN2v88internal12_GLOBAL__N_118CancelableFuncTaskD1Ev,_ZN2v88internal12_GLOBAL__N_118CancelableFuncTaskD2Ev
	.section	.text._ZN2v88internal12_GLOBAL__N_122CancelableIdleFuncTaskD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_122CancelableIdleFuncTaskD2Ev, @function
_ZN2v88internal12_GLOBAL__N_122CancelableIdleFuncTaskD2Ev:
.LFB4309:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal12_GLOBAL__N_122CancelableIdleFuncTaskE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	addq	$48, %rax
	movq	%rax, 32(%rdi)
	movq	56(%rdi), %rax
	testq	%rax, %rax
	je	.L21
	leaq	40(%rdi), %rdi
	movl	$3, %edx
	movq	%rdi, %rsi
	call	*%rax
.L21:
	leaq	16+_ZTVN2v88internal18CancelableIdleTaskE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal10CancelableD2Ev@PLT
	.cfi_endproc
.LFE4309:
	.size	_ZN2v88internal12_GLOBAL__N_122CancelableIdleFuncTaskD2Ev, .-_ZN2v88internal12_GLOBAL__N_122CancelableIdleFuncTaskD2Ev
	.set	_ZN2v88internal12_GLOBAL__N_122CancelableIdleFuncTaskD1Ev,_ZN2v88internal12_GLOBAL__N_122CancelableIdleFuncTaskD2Ev
	.section	.text._ZN2v88internal14CancelableTask3RunEv,"axG",@progbits,_ZN2v88internal14CancelableTask3RunEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14CancelableTask3RunEv
	.type	_ZN2v88internal14CancelableTask3RunEv, @function
_ZN2v88internal14CancelableTask3RunEv:
.LFB3729:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	movl	$2, %edx
	lock cmpxchgl	%edx, 16(%rdi)
	jne	.L26
	movq	(%rdi), %rax
	leaq	_ZN2v88internal12_GLOBAL__N_118CancelableFuncTask11RunInternalEv(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L28
	cmpq	$0, 56(%rdi)
	je	.L33
	leaq	40(%rdi), %r8
	movq	64(%rdi), %rax
	movq	%r8, %rdi
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L28:
	jmp	*%rax
.L26:
	ret
.L33:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZSt25__throw_bad_function_callv@PLT
	.cfi_endproc
.LFE3729:
	.size	_ZN2v88internal14CancelableTask3RunEv, .-_ZN2v88internal14CancelableTask3RunEv
	.section	.text._ZN2v88internal18CancelableIdleTask3RunEd,"axG",@progbits,_ZN2v88internal18CancelableIdleTask3RunEd,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal18CancelableIdleTask3RunEd
	.type	_ZN2v88internal18CancelableIdleTask3RunEd, @function
_ZN2v88internal18CancelableIdleTask3RunEd:
.LFB3730:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	lock cmpxchgl	%edx, 16(%rdi)
	jne	.L34
	movq	(%rdi), %rax
	leaq	_ZN2v88internal12_GLOBAL__N_122CancelableIdleFuncTask11RunInternalEd(%rip), %rdx
	movq	%rdi, %r8
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L36
	cmpq	$0, 56(%rdi)
	movsd	%xmm0, -16(%rbp)
	je	.L43
	leaq	-16(%rbp), %rsi
	leaq	40(%rdi), %rdi
	call	*64(%r8)
.L34:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L44
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	.cfi_restore_state
	call	*%rax
	jmp	.L34
.L44:
	call	__stack_chk_fail@PLT
.L43:
	call	_ZSt25__throw_bad_function_callv@PLT
	.cfi_endproc
.LFE3730:
	.size	_ZN2v88internal18CancelableIdleTask3RunEd, .-_ZN2v88internal18CancelableIdleTask3RunEd
	.section	.text._ZN2v88internal14CancelableTask3RunEv,"axG",@progbits,_ZN2v88internal14CancelableTask3RunEv,comdat
	.p2align 4
	.weak	_ZThn32_N2v88internal14CancelableTask3RunEv
	.type	_ZThn32_N2v88internal14CancelableTask3RunEv, @function
_ZThn32_N2v88internal14CancelableTask3RunEv:
.LFB4730:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	movl	$2, %edx
	lock cmpxchgl	%edx, -16(%rdi)
	jne	.L45
	movq	-32(%rdi), %rax
	leaq	_ZN2v88internal12_GLOBAL__N_118CancelableFuncTask11RunInternalEv(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L52
	leaq	-32(%rdi), %r8
	movq	%r8, %rdi
	jmp	*%rax
.L45:
	ret
.L52:
	cmpq	$0, 24(%rdi)
	je	.L53
	leaq	8(%rdi), %r8
	movq	32(%rdi), %rax
	movq	%r8, %rdi
	jmp	*%rax
.L53:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZSt25__throw_bad_function_callv@PLT
	.cfi_endproc
.LFE4730:
	.size	_ZThn32_N2v88internal14CancelableTask3RunEv, .-_ZThn32_N2v88internal14CancelableTask3RunEv
	.section	.text._ZN2v88internal18CancelableIdleTask3RunEd,"axG",@progbits,_ZN2v88internal18CancelableIdleTask3RunEd,comdat
	.p2align 4
	.weak	_ZThn32_N2v88internal18CancelableIdleTask3RunEd
	.type	_ZThn32_N2v88internal18CancelableIdleTask3RunEd, @function
_ZThn32_N2v88internal18CancelableIdleTask3RunEd:
.LFB4731:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rdx
	movl	$2, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	lock cmpxchgl	%ecx, -16(%rdi)
	jne	.L54
	movq	-32(%rdi), %rax
	leaq	_ZN2v88internal12_GLOBAL__N_122CancelableIdleFuncTask11RunInternalEd(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L63
	subq	$32, %rdi
	call	*%rax
.L54:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L64
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L64:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
.L63:
	cmpq	$0, 24(%rdi)
	movsd	%xmm0, -16(%rbp)
	je	.L65
	leaq	-16(%rbp), %rsi
	leaq	8(%rdi), %rdi
	call	*32(%rdx)
	jmp	.L54
.L65:
	call	_ZSt25__throw_bad_function_callv@PLT
	.cfi_endproc
.LFE4731:
	.size	_ZThn32_N2v88internal18CancelableIdleTask3RunEd, .-_ZThn32_N2v88internal18CancelableIdleTask3RunEd
	.section	.text._ZThn32_N2v88internal12_GLOBAL__N_118CancelableFuncTaskD0Ev,"ax",@progbits
	.p2align 4
	.type	_ZThn32_N2v88internal12_GLOBAL__N_118CancelableFuncTaskD0Ev, @function
_ZThn32_N2v88internal12_GLOBAL__N_118CancelableFuncTaskD0Ev:
.LFB4729:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal12_GLOBAL__N_118CancelableFuncTaskE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-32(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rax, -32(%rdi)
	addq	$48, %rax
	movq	%rax, (%rdi)
	movq	24(%rdi), %rax
	testq	%rax, %rax
	je	.L67
	leaq	8(%rdi), %rdi
	movl	$3, %edx
	movq	%rdi, %rsi
	call	*%rax
.L67:
	leaq	16+_ZTVN2v88internal14CancelableTaskE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, -32(%rbx)
	call	_ZN2v88internal10CancelableD2Ev@PLT
	popq	%rbx
	movq	%r12, %rdi
	movl	$72, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4729:
	.size	_ZThn32_N2v88internal12_GLOBAL__N_118CancelableFuncTaskD0Ev, .-_ZThn32_N2v88internal12_GLOBAL__N_118CancelableFuncTaskD0Ev
	.section	.text._ZThn32_N2v88internal12_GLOBAL__N_122CancelableIdleFuncTaskD0Ev,"ax",@progbits
	.p2align 4
	.type	_ZThn32_N2v88internal12_GLOBAL__N_122CancelableIdleFuncTaskD0Ev, @function
_ZThn32_N2v88internal12_GLOBAL__N_122CancelableIdleFuncTaskD0Ev:
.LFB4728:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal12_GLOBAL__N_122CancelableIdleFuncTaskE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-32(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rax, -32(%rdi)
	addq	$48, %rax
	movq	%rax, (%rdi)
	movq	24(%rdi), %rax
	testq	%rax, %rax
	je	.L73
	leaq	8(%rdi), %rdi
	movl	$3, %edx
	movq	%rdi, %rsi
	call	*%rax
.L73:
	leaq	16+_ZTVN2v88internal18CancelableIdleTaskE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, -32(%rbx)
	call	_ZN2v88internal10CancelableD2Ev@PLT
	popq	%rbx
	movq	%r12, %rdi
	movl	$72, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4728:
	.size	_ZThn32_N2v88internal12_GLOBAL__N_122CancelableIdleFuncTaskD0Ev, .-_ZThn32_N2v88internal12_GLOBAL__N_122CancelableIdleFuncTaskD0Ev
	.section	.text._ZN2v88internal12_GLOBAL__N_118CancelableFuncTaskD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_118CancelableFuncTaskD0Ev, @function
_ZN2v88internal12_GLOBAL__N_118CancelableFuncTaskD0Ev:
.LFB4277:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal12_GLOBAL__N_118CancelableFuncTaskE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	addq	$48, %rax
	movq	%rax, 32(%rdi)
	movq	56(%rdi), %rax
	testq	%rax, %rax
	je	.L79
	leaq	40(%rdi), %rdi
	movl	$3, %edx
	movq	%rdi, %rsi
	call	*%rax
.L79:
	leaq	16+_ZTVN2v88internal14CancelableTaskE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	call	_ZN2v88internal10CancelableD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$72, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4277:
	.size	_ZN2v88internal12_GLOBAL__N_118CancelableFuncTaskD0Ev, .-_ZN2v88internal12_GLOBAL__N_118CancelableFuncTaskD0Ev
	.section	.text._ZN2v88internal12_GLOBAL__N_122CancelableIdleFuncTaskD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_122CancelableIdleFuncTaskD0Ev, @function
_ZN2v88internal12_GLOBAL__N_122CancelableIdleFuncTaskD0Ev:
.LFB4311:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal12_GLOBAL__N_122CancelableIdleFuncTaskE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	addq	$48, %rax
	movq	%rax, 32(%rdi)
	movq	56(%rdi), %rax
	testq	%rax, %rax
	je	.L85
	leaq	40(%rdi), %rdi
	movl	$3, %edx
	movq	%rdi, %rsi
	call	*%rax
.L85:
	leaq	16+_ZTVN2v88internal18CancelableIdleTaskE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	call	_ZN2v88internal10CancelableD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$72, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4311:
	.size	_ZN2v88internal12_GLOBAL__N_122CancelableIdleFuncTaskD0Ev, .-_ZN2v88internal12_GLOBAL__N_122CancelableIdleFuncTaskD0Ev
	.section	.text._ZThn32_N2v88internal12_GLOBAL__N_118CancelableFuncTaskD1Ev,"ax",@progbits
	.p2align 4
	.type	_ZThn32_N2v88internal12_GLOBAL__N_118CancelableFuncTaskD1Ev, @function
_ZThn32_N2v88internal12_GLOBAL__N_118CancelableFuncTaskD1Ev:
.LFB4732:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal12_GLOBAL__N_118CancelableFuncTaskE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, -32(%rdi)
	addq	$48, %rax
	movq	%rax, (%rdi)
	movq	24(%rdi), %rax
	testq	%rax, %rax
	je	.L91
	leaq	8(%rdi), %rdi
	movl	$3, %edx
	movq	%rdi, %rsi
	call	*%rax
.L91:
	leaq	16+_ZTVN2v88internal14CancelableTaskE(%rip), %rax
	leaq	-32(%rbx), %rdi
	movq	%rax, -32(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal10CancelableD2Ev@PLT
	.cfi_endproc
.LFE4732:
	.size	_ZThn32_N2v88internal12_GLOBAL__N_118CancelableFuncTaskD1Ev, .-_ZThn32_N2v88internal12_GLOBAL__N_118CancelableFuncTaskD1Ev
	.section	.text._ZThn32_N2v88internal12_GLOBAL__N_122CancelableIdleFuncTaskD1Ev,"ax",@progbits
	.p2align 4
	.type	_ZThn32_N2v88internal12_GLOBAL__N_122CancelableIdleFuncTaskD1Ev, @function
_ZThn32_N2v88internal12_GLOBAL__N_122CancelableIdleFuncTaskD1Ev:
.LFB4733:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal12_GLOBAL__N_122CancelableIdleFuncTaskE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, -32(%rdi)
	addq	$48, %rax
	movq	%rax, (%rdi)
	movq	24(%rdi), %rax
	testq	%rax, %rax
	je	.L97
	leaq	8(%rdi), %rdi
	movl	$3, %edx
	movq	%rdi, %rsi
	call	*%rax
.L97:
	leaq	16+_ZTVN2v88internal18CancelableIdleTaskE(%rip), %rax
	leaq	-32(%rbx), %rdi
	movq	%rax, -32(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal10CancelableD2Ev@PLT
	.cfi_endproc
.LFE4733:
	.size	_ZThn32_N2v88internal12_GLOBAL__N_122CancelableIdleFuncTaskD1Ev, .-_ZThn32_N2v88internal12_GLOBAL__N_122CancelableIdleFuncTaskD1Ev
	.section	.text._ZN2v88internal18MakeCancelableTaskEPNS0_7IsolateESt8functionIFvvEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal18MakeCancelableTaskEPNS0_7IsolateESt8functionIFvvEE
	.type	_ZN2v88internal18MakeCancelableTaskEPNS0_7IsolateESt8functionIFvvEE, @function
_ZN2v88internal18MakeCancelableTaskEPNS0_7IsolateESt8functionIFvvEE:
.LFB3768:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movl	$72, %edi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movdqa	-96(%rbp), %xmm1
	movq	16(%rdx), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rdx), %r13
	movq	$0, 16(%rdx)
	movq	$0, 24(%rdx)
	movdqu	(%rdx), %xmm0
	movups	%xmm1, (%rdx)
	movaps	%xmm0, -96(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal14CancelableTaskC2EPNS0_7IsolateE@PLT
	leaq	16+_ZTVN2v88internal12_GLOBAL__N_118CancelableFuncTaskE(%rip), %rax
	movq	%rbx, (%r14)
	movdqa	-96(%rbp), %xmm2
	movq	%rax, (%rbx)
	addq	$48, %rax
	movq	%r12, %xmm0
	movq	%r13, %xmm3
	movq	%rax, 32(%rbx)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm2, 40(%rbx)
	movups	%xmm0, 56(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L105
	addq	$56, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L105:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3768:
	.size	_ZN2v88internal18MakeCancelableTaskEPNS0_7IsolateESt8functionIFvvEE, .-_ZN2v88internal18MakeCancelableTaskEPNS0_7IsolateESt8functionIFvvEE
	.section	.text._ZN2v88internal18MakeCancelableTaskEPNS0_21CancelableTaskManagerESt8functionIFvvEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal18MakeCancelableTaskEPNS0_21CancelableTaskManagerESt8functionIFvvEE
	.type	_ZN2v88internal18MakeCancelableTaskEPNS0_21CancelableTaskManagerESt8functionIFvvEE, @function
_ZN2v88internal18MakeCancelableTaskEPNS0_21CancelableTaskManagerESt8functionIFvvEE:
.LFB3771:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movl	$72, %edi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movdqa	-96(%rbp), %xmm1
	movq	16(%rdx), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rdx), %r13
	movq	$0, 16(%rdx)
	movq	$0, 24(%rdx)
	movdqu	(%rdx), %xmm0
	movups	%xmm1, (%rdx)
	movaps	%xmm0, -96(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal14CancelableTaskC2EPNS0_21CancelableTaskManagerE@PLT
	leaq	16+_ZTVN2v88internal12_GLOBAL__N_118CancelableFuncTaskE(%rip), %rax
	movq	%rbx, (%r14)
	movdqa	-96(%rbp), %xmm2
	movq	%rax, (%rbx)
	addq	$48, %rax
	movq	%r12, %xmm0
	movq	%r13, %xmm3
	movq	%rax, 32(%rbx)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm2, 40(%rbx)
	movups	%xmm0, 56(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L109
	addq	$56, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L109:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3771:
	.size	_ZN2v88internal18MakeCancelableTaskEPNS0_21CancelableTaskManagerESt8functionIFvvEE, .-_ZN2v88internal18MakeCancelableTaskEPNS0_21CancelableTaskManagerESt8functionIFvvEE
	.section	.text._ZN2v88internal22MakeCancelableIdleTaskEPNS0_7IsolateESt8functionIFvdEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal22MakeCancelableIdleTaskEPNS0_7IsolateESt8functionIFvdEE
	.type	_ZN2v88internal22MakeCancelableIdleTaskEPNS0_7IsolateESt8functionIFvdEE, @function
_ZN2v88internal22MakeCancelableIdleTaskEPNS0_7IsolateESt8functionIFvdEE:
.LFB3774:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movl	$72, %edi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movdqa	-96(%rbp), %xmm1
	movq	16(%rdx), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rdx), %r13
	movq	$0, 16(%rdx)
	movq	$0, 24(%rdx)
	movdqu	(%rdx), %xmm0
	movups	%xmm1, (%rdx)
	movaps	%xmm0, -96(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal18CancelableIdleTaskC2EPNS0_7IsolateE@PLT
	leaq	16+_ZTVN2v88internal12_GLOBAL__N_122CancelableIdleFuncTaskE(%rip), %rax
	movq	%rbx, (%r14)
	movdqa	-96(%rbp), %xmm2
	movq	%rax, (%rbx)
	addq	$48, %rax
	movq	%r12, %xmm0
	movq	%r13, %xmm3
	movq	%rax, 32(%rbx)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm2, 40(%rbx)
	movups	%xmm0, 56(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L113
	addq	$56, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L113:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3774:
	.size	_ZN2v88internal22MakeCancelableIdleTaskEPNS0_7IsolateESt8functionIFvdEE, .-_ZN2v88internal22MakeCancelableIdleTaskEPNS0_7IsolateESt8functionIFvdEE
	.section	.text._ZN2v88internal22MakeCancelableIdleTaskEPNS0_21CancelableTaskManagerESt8functionIFvdEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal22MakeCancelableIdleTaskEPNS0_21CancelableTaskManagerESt8functionIFvdEE
	.type	_ZN2v88internal22MakeCancelableIdleTaskEPNS0_21CancelableTaskManagerESt8functionIFvdEE, @function
_ZN2v88internal22MakeCancelableIdleTaskEPNS0_21CancelableTaskManagerESt8functionIFvdEE:
.LFB3777:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movl	$72, %edi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movdqa	-96(%rbp), %xmm1
	movq	16(%rdx), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rdx), %r13
	movq	$0, 16(%rdx)
	movq	$0, 24(%rdx)
	movdqu	(%rdx), %xmm0
	movups	%xmm1, (%rdx)
	movaps	%xmm0, -96(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal18CancelableIdleTaskC2EPNS0_21CancelableTaskManagerE@PLT
	leaq	16+_ZTVN2v88internal12_GLOBAL__N_122CancelableIdleFuncTaskE(%rip), %rax
	movq	%rbx, (%r14)
	movdqa	-96(%rbp), %xmm2
	movq	%rax, (%rbx)
	addq	$48, %rax
	movq	%r12, %xmm0
	movq	%r13, %xmm3
	movq	%rax, 32(%rbx)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm2, 40(%rbx)
	movups	%xmm0, 56(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L117
	addq	$56, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L117:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3777:
	.size	_ZN2v88internal22MakeCancelableIdleTaskEPNS0_21CancelableTaskManagerESt8functionIFvdEE, .-_ZN2v88internal22MakeCancelableIdleTaskEPNS0_21CancelableTaskManagerESt8functionIFvdEE
	.weak	_ZTVN2v88internal14CancelableTaskE
	.section	.data.rel.ro._ZTVN2v88internal14CancelableTaskE,"awG",@progbits,_ZTVN2v88internal14CancelableTaskE,comdat
	.align 8
	.type	_ZTVN2v88internal14CancelableTaskE, @object
	.size	_ZTVN2v88internal14CancelableTaskE, 88
_ZTVN2v88internal14CancelableTaskE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZN2v88internal14CancelableTask3RunEv
	.quad	__cxa_pure_virtual
	.quad	-32
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZThn32_N2v88internal14CancelableTask3RunEv
	.weak	_ZTVN2v88internal18CancelableIdleTaskE
	.section	.data.rel.ro._ZTVN2v88internal18CancelableIdleTaskE,"awG",@progbits,_ZTVN2v88internal18CancelableIdleTaskE,comdat
	.align 8
	.type	_ZTVN2v88internal18CancelableIdleTaskE, @object
	.size	_ZTVN2v88internal18CancelableIdleTaskE, 88
_ZTVN2v88internal18CancelableIdleTaskE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZN2v88internal18CancelableIdleTask3RunEd
	.quad	__cxa_pure_virtual
	.quad	-32
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZThn32_N2v88internal18CancelableIdleTask3RunEd
	.section	.data.rel.ro.local._ZTVN2v88internal12_GLOBAL__N_118CancelableFuncTaskE,"aw"
	.align 8
	.type	_ZTVN2v88internal12_GLOBAL__N_118CancelableFuncTaskE, @object
	.size	_ZTVN2v88internal12_GLOBAL__N_118CancelableFuncTaskE, 88
_ZTVN2v88internal12_GLOBAL__N_118CancelableFuncTaskE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal12_GLOBAL__N_118CancelableFuncTaskD1Ev
	.quad	_ZN2v88internal12_GLOBAL__N_118CancelableFuncTaskD0Ev
	.quad	_ZN2v88internal14CancelableTask3RunEv
	.quad	_ZN2v88internal12_GLOBAL__N_118CancelableFuncTask11RunInternalEv
	.quad	-32
	.quad	0
	.quad	_ZThn32_N2v88internal12_GLOBAL__N_118CancelableFuncTaskD1Ev
	.quad	_ZThn32_N2v88internal12_GLOBAL__N_118CancelableFuncTaskD0Ev
	.quad	_ZThn32_N2v88internal14CancelableTask3RunEv
	.section	.data.rel.ro.local._ZTVN2v88internal12_GLOBAL__N_122CancelableIdleFuncTaskE,"aw"
	.align 8
	.type	_ZTVN2v88internal12_GLOBAL__N_122CancelableIdleFuncTaskE, @object
	.size	_ZTVN2v88internal12_GLOBAL__N_122CancelableIdleFuncTaskE, 88
_ZTVN2v88internal12_GLOBAL__N_122CancelableIdleFuncTaskE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal12_GLOBAL__N_122CancelableIdleFuncTaskD1Ev
	.quad	_ZN2v88internal12_GLOBAL__N_122CancelableIdleFuncTaskD0Ev
	.quad	_ZN2v88internal18CancelableIdleTask3RunEd
	.quad	_ZN2v88internal12_GLOBAL__N_122CancelableIdleFuncTask11RunInternalEd
	.quad	-32
	.quad	0
	.quad	_ZThn32_N2v88internal12_GLOBAL__N_122CancelableIdleFuncTaskD1Ev
	.quad	_ZThn32_N2v88internal12_GLOBAL__N_122CancelableIdleFuncTaskD0Ev
	.quad	_ZThn32_N2v88internal18CancelableIdleTask3RunEd
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
