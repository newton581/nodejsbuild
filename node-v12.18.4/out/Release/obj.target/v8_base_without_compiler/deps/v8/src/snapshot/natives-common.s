	.file	"natives-common.cc"
	.text
	.section	.text._ZNK2v86String26ExternalStringResourceBase11IsCacheableEv,"axG",@progbits,_ZNK2v86String26ExternalStringResourceBase11IsCacheableEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v86String26ExternalStringResourceBase11IsCacheableEv
	.type	_ZNK2v86String26ExternalStringResourceBase11IsCacheableEv, @function
_ZNK2v86String26ExternalStringResourceBase11IsCacheableEv:
.LFB3687:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3687:
	.size	_ZNK2v86String26ExternalStringResourceBase11IsCacheableEv, .-_ZNK2v86String26ExternalStringResourceBase11IsCacheableEv
	.section	.text._ZN2v86String26ExternalStringResourceBase7DisposeEv,"axG",@progbits,_ZN2v86String26ExternalStringResourceBase7DisposeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v86String26ExternalStringResourceBase7DisposeEv
	.type	_ZN2v86String26ExternalStringResourceBase7DisposeEv, @function
_ZN2v86String26ExternalStringResourceBase7DisposeEv:
.LFB3688:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.cfi_endproc
.LFE3688:
	.size	_ZN2v86String26ExternalStringResourceBase7DisposeEv, .-_ZN2v86String26ExternalStringResourceBase7DisposeEv
	.section	.text._ZNK2v86String26ExternalStringResourceBase4LockEv,"axG",@progbits,_ZNK2v86String26ExternalStringResourceBase4LockEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v86String26ExternalStringResourceBase4LockEv
	.type	_ZNK2v86String26ExternalStringResourceBase4LockEv, @function
_ZNK2v86String26ExternalStringResourceBase4LockEv:
.LFB3689:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3689:
	.size	_ZNK2v86String26ExternalStringResourceBase4LockEv, .-_ZNK2v86String26ExternalStringResourceBase4LockEv
	.section	.text._ZNK2v86String26ExternalStringResourceBase6UnlockEv,"axG",@progbits,_ZNK2v86String26ExternalStringResourceBase6UnlockEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v86String26ExternalStringResourceBase6UnlockEv
	.type	_ZNK2v86String26ExternalStringResourceBase6UnlockEv, @function
_ZNK2v86String26ExternalStringResourceBase6UnlockEv:
.LFB3690:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3690:
	.size	_ZNK2v86String26ExternalStringResourceBase6UnlockEv, .-_ZNK2v86String26ExternalStringResourceBase6UnlockEv
	.section	.text._ZNK2v88internal29NativesExternalStringResource4dataEv,"axG",@progbits,_ZNK2v88internal29NativesExternalStringResource4dataEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal29NativesExternalStringResource4dataEv
	.type	_ZNK2v88internal29NativesExternalStringResource4dataEv, @function
_ZNK2v88internal29NativesExternalStringResource4dataEv:
.LFB17782:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE17782:
	.size	_ZNK2v88internal29NativesExternalStringResource4dataEv, .-_ZNK2v88internal29NativesExternalStringResource4dataEv
	.section	.text._ZNK2v88internal29NativesExternalStringResource6lengthEv,"axG",@progbits,_ZNK2v88internal29NativesExternalStringResource6lengthEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal29NativesExternalStringResource6lengthEv
	.type	_ZNK2v88internal29NativesExternalStringResource6lengthEv, @function
_ZNK2v88internal29NativesExternalStringResource6lengthEv:
.LFB17783:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE17783:
	.size	_ZNK2v88internal29NativesExternalStringResource6lengthEv, .-_ZNK2v88internal29NativesExternalStringResource6lengthEv
	.section	.text._ZN2v88internal29NativesExternalStringResourceD2Ev,"axG",@progbits,_ZN2v88internal29NativesExternalStringResourceD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal29NativesExternalStringResourceD2Ev
	.type	_ZN2v88internal29NativesExternalStringResourceD2Ev, @function
_ZN2v88internal29NativesExternalStringResourceD2Ev:
.LFB21439:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE21439:
	.size	_ZN2v88internal29NativesExternalStringResourceD2Ev, .-_ZN2v88internal29NativesExternalStringResourceD2Ev
	.weak	_ZN2v88internal29NativesExternalStringResourceD1Ev
	.set	_ZN2v88internal29NativesExternalStringResourceD1Ev,_ZN2v88internal29NativesExternalStringResourceD2Ev
	.section	.text._ZN2v88internal29NativesExternalStringResourceD0Ev,"axG",@progbits,_ZN2v88internal29NativesExternalStringResourceD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal29NativesExternalStringResourceD0Ev
	.type	_ZN2v88internal29NativesExternalStringResourceD0Ev, @function
_ZN2v88internal29NativesExternalStringResourceD0Ev:
.LFB21441:
	.cfi_startproc
	endbr64
	movl	$32, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE21441:
	.size	_ZN2v88internal29NativesExternalStringResourceD0Ev, .-_ZN2v88internal29NativesExternalStringResourceD0Ev
	.section	.rodata._ZN2v88internal29NativesExternalStringResourceC2ENS0_10NativeTypeEi.str1.1,"aMS",@progbits,1
.LC0:
	.string	"EXTRAS == type_"
.LC1:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal29NativesExternalStringResourceC2ENS0_10NativeTypeEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29NativesExternalStringResourceC2ENS0_10NativeTypeEi
	.type	_ZN2v88internal29NativesExternalStringResourceC2ENS0_10NativeTypeEi, @function
_ZN2v88internal29NativesExternalStringResourceC2ENS0_10NativeTypeEi:
.LFB17804:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal29NativesExternalStringResourceE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movl	%esi, 24(%rdi)
	movl	%edx, 28(%rdi)
	testl	%esi, %esi
	jne	.L13
	movl	%edx, %edi
	call	_ZN2v88internal17NativesCollectionILNS0_10NativeTypeE0EE15GetScriptSourceEi@PLT
	movslq	%edx, %rdx
	movq	%rax, 8(%rbx)
	movq	%rdx, 16(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	.cfi_restore_state
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE17804:
	.size	_ZN2v88internal29NativesExternalStringResourceC2ENS0_10NativeTypeEi, .-_ZN2v88internal29NativesExternalStringResourceC2ENS0_10NativeTypeEi
	.globl	_ZN2v88internal29NativesExternalStringResourceC1ENS0_10NativeTypeEi
	.set	_ZN2v88internal29NativesExternalStringResourceC1ENS0_10NativeTypeEi,_ZN2v88internal29NativesExternalStringResourceC2ENS0_10NativeTypeEi
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal29NativesExternalStringResourceC2ENS0_10NativeTypeEi,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal29NativesExternalStringResourceC2ENS0_10NativeTypeEi, @function
_GLOBAL__sub_I__ZN2v88internal29NativesExternalStringResourceC2ENS0_10NativeTypeEi:
.LFB21459:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE21459:
	.size	_GLOBAL__sub_I__ZN2v88internal29NativesExternalStringResourceC2ENS0_10NativeTypeEi, .-_GLOBAL__sub_I__ZN2v88internal29NativesExternalStringResourceC2ENS0_10NativeTypeEi
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal29NativesExternalStringResourceC2ENS0_10NativeTypeEi
	.weak	_ZTVN2v88internal29NativesExternalStringResourceE
	.section	.data.rel.ro.local._ZTVN2v88internal29NativesExternalStringResourceE,"awG",@progbits,_ZTVN2v88internal29NativesExternalStringResourceE,comdat
	.align 8
	.type	_ZTVN2v88internal29NativesExternalStringResourceE, @object
	.size	_ZTVN2v88internal29NativesExternalStringResourceE, 80
_ZTVN2v88internal29NativesExternalStringResourceE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal29NativesExternalStringResourceD1Ev
	.quad	_ZN2v88internal29NativesExternalStringResourceD0Ev
	.quad	_ZNK2v86String26ExternalStringResourceBase11IsCacheableEv
	.quad	_ZN2v86String26ExternalStringResourceBase7DisposeEv
	.quad	_ZNK2v86String26ExternalStringResourceBase4LockEv
	.quad	_ZNK2v86String26ExternalStringResourceBase6UnlockEv
	.quad	_ZNK2v88internal29NativesExternalStringResource4dataEv
	.quad	_ZNK2v88internal29NativesExternalStringResource6lengthEv
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
