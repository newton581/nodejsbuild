	.file	"function-body-decoder.cc"
	.text
	.section	.text._ZNKSt5ctypeIcE8do_widenEc,"axG",@progbits,_ZNKSt5ctypeIcE8do_widenEc,comdat
	.align 2
	.p2align 4
	.weak	_ZNKSt5ctypeIcE8do_widenEc
	.type	_ZNKSt5ctypeIcE8do_widenEc, @function
_ZNKSt5ctypeIcE8do_widenEc:
.LFB1338:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	ret
	.cfi_endproc
.LFE1338:
	.size	_ZNKSt5ctypeIcE8do_widenEc, .-_ZNKSt5ctypeIcE8do_widenEc
	.section	.text._ZN2v88internal4wasm7Decoder12onFirstErrorEv,"axG",@progbits,_ZN2v88internal4wasm7Decoder12onFirstErrorEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm7Decoder12onFirstErrorEv
	.type	_ZN2v88internal4wasm7Decoder12onFirstErrorEv, @function
_ZN2v88internal4wasm7Decoder12onFirstErrorEv:
.LFB6912:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6912:
	.size	_ZN2v88internal4wasm7Decoder12onFirstErrorEv, .-_ZN2v88internal4wasm7Decoder12onFirstErrorEv
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_113RawOpcodeNameENS1_10WasmOpcodeE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"Unknown"
.LC1:
	.string	"kExprUnreachable"
.LC2:
	.string	"kExprBlock"
.LC3:
	.string	"kExprLoop"
.LC4:
	.string	"kExprIf"
.LC5:
	.string	"kExprElse"
.LC6:
	.string	"kExprTry"
.LC7:
	.string	"kExprCatch"
.LC8:
	.string	"kExprThrow"
.LC9:
	.string	"kExprRethrow"
.LC10:
	.string	"kExprBrOnExn"
.LC11:
	.string	"kExprEnd"
.LC12:
	.string	"kExprBr"
.LC13:
	.string	"kExprBrIf"
.LC14:
	.string	"kExprBrTable"
.LC15:
	.string	"kExprReturn"
.LC16:
	.string	"kExprCallFunction"
.LC17:
	.string	"kExprCallIndirect"
.LC18:
	.string	"kExprReturnCall"
.LC19:
	.string	"kExprReturnCallIndirect"
.LC20:
	.string	"kExprDrop"
.LC21:
	.string	"kExprSelect"
.LC22:
	.string	"kExprSelectWithType"
.LC23:
	.string	"kExprGetLocal"
.LC24:
	.string	"kExprSetLocal"
.LC25:
	.string	"kExprTeeLocal"
.LC26:
	.string	"kExprGetGlobal"
.LC27:
	.string	"kExprSetGlobal"
.LC28:
	.string	"kExprTableGet"
.LC29:
	.string	"kExprTableSet"
.LC30:
	.string	"kExprI32Const"
.LC31:
	.string	"kExprI64Const"
.LC32:
	.string	"kExprF32Const"
.LC33:
	.string	"kExprF64Const"
.LC34:
	.string	"kExprRefNull"
.LC35:
	.string	"kExprRefFunc"
.LC36:
	.string	"kExprI32Eqz"
.LC37:
	.string	"kExprI32Eq"
.LC38:
	.string	"kExprI32Ne"
.LC39:
	.string	"kExprI32LtS"
.LC40:
	.string	"kExprI32LtU"
.LC41:
	.string	"kExprI32GtS"
.LC42:
	.string	"kExprI32GtU"
.LC43:
	.string	"kExprI32LeS"
.LC44:
	.string	"kExprI32LeU"
.LC45:
	.string	"kExprI32GeS"
.LC46:
	.string	"kExprI32GeU"
.LC47:
	.string	"kExprI64Eqz"
.LC48:
	.string	"kExprI64Eq"
.LC49:
	.string	"kExprI64Ne"
.LC50:
	.string	"kExprI64LtS"
.LC51:
	.string	"kExprI64LtU"
.LC52:
	.string	"kExprI64GtS"
.LC53:
	.string	"kExprI64GtU"
.LC54:
	.string	"kExprI64LeS"
.LC55:
	.string	"kExprI64LeU"
.LC56:
	.string	"kExprI64GeS"
.LC57:
	.string	"kExprI64GeU"
.LC58:
	.string	"kExprF32Eq"
.LC59:
	.string	"kExprF32Ne"
.LC60:
	.string	"kExprF32Lt"
.LC61:
	.string	"kExprF32Gt"
.LC62:
	.string	"kExprF32Le"
.LC63:
	.string	"kExprF32Ge"
.LC64:
	.string	"kExprF64Eq"
.LC65:
	.string	"kExprF64Ne"
.LC66:
	.string	"kExprF64Lt"
.LC67:
	.string	"kExprF64Gt"
.LC68:
	.string	"kExprF64Le"
.LC69:
	.string	"kExprF64Ge"
.LC70:
	.string	"kExprI32Clz"
.LC71:
	.string	"kExprI32Ctz"
.LC72:
	.string	"kExprI32Popcnt"
.LC73:
	.string	"kExprI32Add"
.LC74:
	.string	"kExprI32Sub"
.LC75:
	.string	"kExprI32Mul"
.LC76:
	.string	"kExprI32DivS"
.LC77:
	.string	"kExprI32DivU"
.LC78:
	.string	"kExprI32RemS"
.LC79:
	.string	"kExprI32RemU"
.LC80:
	.string	"kExprI32And"
.LC81:
	.string	"kExprI32Ior"
.LC82:
	.string	"kExprI32Xor"
.LC83:
	.string	"kExprI32Shl"
.LC84:
	.string	"kExprI32ShrS"
.LC85:
	.string	"kExprI32ShrU"
.LC86:
	.string	"kExprI32Rol"
.LC87:
	.string	"kExprI32Ror"
.LC88:
	.string	"kExprI64Clz"
.LC89:
	.string	"kExprI64Ctz"
.LC90:
	.string	"kExprI64Popcnt"
.LC91:
	.string	"kExprI64Add"
.LC92:
	.string	"kExprI64Sub"
.LC93:
	.string	"kExprI64Mul"
.LC94:
	.string	"kExprI64DivS"
.LC95:
	.string	"kExprI64DivU"
.LC96:
	.string	"kExprI64RemS"
.LC97:
	.string	"kExprI64RemU"
.LC98:
	.string	"kExprI64And"
.LC99:
	.string	"kExprI64Ior"
.LC100:
	.string	"kExprI64Xor"
.LC101:
	.string	"kExprI64Shl"
.LC102:
	.string	"kExprI64ShrS"
.LC103:
	.string	"kExprI64ShrU"
.LC104:
	.string	"kExprI64Rol"
.LC105:
	.string	"kExprI64Ror"
.LC106:
	.string	"kExprF32Abs"
.LC107:
	.string	"kExprF32Neg"
.LC108:
	.string	"kExprF32Ceil"
.LC109:
	.string	"kExprF32Floor"
.LC110:
	.string	"kExprF32Trunc"
.LC111:
	.string	"kExprF32NearestInt"
.LC112:
	.string	"kExprF32Sqrt"
.LC113:
	.string	"kExprF32Add"
.LC114:
	.string	"kExprF32Sub"
.LC115:
	.string	"kExprF32Mul"
.LC116:
	.string	"kExprF32Div"
.LC117:
	.string	"kExprF32Min"
.LC118:
	.string	"kExprF32Max"
.LC119:
	.string	"kExprF32CopySign"
.LC120:
	.string	"kExprF64Abs"
.LC121:
	.string	"kExprF64Neg"
.LC122:
	.string	"kExprF64Ceil"
.LC123:
	.string	"kExprF64Floor"
.LC124:
	.string	"kExprF64Trunc"
.LC125:
	.string	"kExprF64NearestInt"
.LC126:
	.string	"kExprF64Sqrt"
.LC127:
	.string	"kExprF64Add"
.LC128:
	.string	"kExprF64Sub"
.LC129:
	.string	"kExprF64Mul"
.LC130:
	.string	"kExprF64Div"
.LC131:
	.string	"kExprF64Min"
.LC132:
	.string	"kExprF64Max"
.LC133:
	.string	"kExprF64CopySign"
.LC134:
	.string	"kExprI32ConvertI64"
.LC135:
	.string	"kExprI32SConvertF32"
.LC136:
	.string	"kExprI32UConvertF32"
.LC137:
	.string	"kExprI32SConvertF64"
.LC138:
	.string	"kExprI32UConvertF64"
.LC139:
	.string	"kExprI64SConvertI32"
.LC140:
	.string	"kExprI64UConvertI32"
.LC141:
	.string	"kExprI64SConvertF32"
.LC142:
	.string	"kExprI64UConvertF32"
.LC143:
	.string	"kExprI64SConvertF64"
.LC144:
	.string	"kExprI64UConvertF64"
.LC145:
	.string	"kExprF32SConvertI32"
.LC146:
	.string	"kExprF32UConvertI32"
.LC147:
	.string	"kExprF32SConvertI64"
.LC148:
	.string	"kExprF32UConvertI64"
.LC149:
	.string	"kExprF32ConvertF64"
.LC150:
	.string	"kExprF64SConvertI32"
.LC151:
	.string	"kExprF64UConvertI32"
.LC152:
	.string	"kExprF64SConvertI64"
.LC153:
	.string	"kExprF64UConvertI64"
.LC154:
	.string	"kExprF64ConvertF32"
.LC155:
	.string	"kExprI32ReinterpretF32"
.LC156:
	.string	"kExprI64ReinterpretF64"
.LC157:
	.string	"kExprF32ReinterpretI32"
.LC158:
	.string	"kExprF64ReinterpretI64"
.LC159:
	.string	"kExprI32SExtendI8"
.LC160:
	.string	"kExprI32SExtendI16"
.LC161:
	.string	"kExprI64SExtendI8"
.LC162:
	.string	"kExprI64SExtendI16"
.LC163:
	.string	"kExprI64SExtendI32"
.LC164:
	.string	"kExprRefIsNull"
.LC165:
	.string	"kExprI32StoreMem"
.LC166:
	.string	"kExprI64StoreMem"
.LC167:
	.string	"kExprF32StoreMem"
.LC168:
	.string	"kExprF64StoreMem"
.LC169:
	.string	"kExprI32StoreMem8"
.LC170:
	.string	"kExprI32StoreMem16"
.LC171:
	.string	"kExprI64StoreMem8"
.LC172:
	.string	"kExprI64StoreMem16"
.LC173:
	.string	"kExprI64StoreMem32"
.LC174:
	.string	"kExprI32LoadMem"
.LC175:
	.string	"kExprI64LoadMem"
.LC176:
	.string	"kExprF32LoadMem"
.LC177:
	.string	"kExprF64LoadMem"
.LC178:
	.string	"kExprI32LoadMem8S"
.LC179:
	.string	"kExprI32LoadMem8U"
.LC180:
	.string	"kExprI32LoadMem16S"
.LC181:
	.string	"kExprI32LoadMem16U"
.LC182:
	.string	"kExprI64LoadMem8S"
.LC183:
	.string	"kExprI64LoadMem8U"
.LC184:
	.string	"kExprI64LoadMem16S"
.LC185:
	.string	"kExprI64LoadMem16U"
.LC186:
	.string	"kExprI64LoadMem32S"
.LC187:
	.string	"kExprI64LoadMem32U"
.LC188:
	.string	"kExprMemorySize"
.LC189:
	.string	"kExprMemoryGrow"
.LC190:
	.string	"kExprF64Acos"
.LC191:
	.string	"kExprF64Asin"
.LC192:
	.string	"kExprF64Atan"
.LC193:
	.string	"kExprF64Cos"
.LC194:
	.string	"kExprF64Sin"
.LC195:
	.string	"kExprF64Tan"
.LC196:
	.string	"kExprF64Exp"
.LC197:
	.string	"kExprF64Log"
.LC198:
	.string	"kExprF64Atan2"
.LC199:
	.string	"kExprF64Pow"
.LC200:
	.string	"kExprF64Mod"
.LC201:
	.string	"kExprI32AsmjsDivS"
.LC202:
	.string	"kExprI32AsmjsDivU"
.LC203:
	.string	"kExprI32AsmjsRemS"
.LC204:
	.string	"kExprI32AsmjsRemU"
.LC205:
	.string	"kExprI32AsmjsLoadMem8S"
.LC206:
	.string	"kExprI32AsmjsLoadMem8U"
.LC207:
	.string	"kExprI32AsmjsLoadMem16S"
.LC208:
	.string	"kExprI32AsmjsLoadMem16U"
.LC209:
	.string	"kExprI32AsmjsLoadMem"
.LC210:
	.string	"kExprF32AsmjsLoadMem"
.LC211:
	.string	"kExprF64AsmjsLoadMem"
.LC212:
	.string	"kExprI32AsmjsStoreMem8"
.LC213:
	.string	"kExprI32AsmjsStoreMem16"
.LC214:
	.string	"kExprI32AsmjsStoreMem"
.LC215:
	.string	"kExprF32AsmjsStoreMem"
.LC216:
	.string	"kExprF64AsmjsStoreMem"
.LC217:
	.string	"kExprI32AsmjsSConvertF32"
.LC218:
	.string	"kExprI32AsmjsUConvertF32"
.LC219:
	.string	"kExprI32AsmjsSConvertF64"
.LC220:
	.string	"kExprI32AsmjsUConvertF64"
.LC221:
	.string	"kExprI8x16Splat"
.LC222:
	.string	"kExprI16x8Splat"
.LC223:
	.string	"kExprI32x4Splat"
.LC224:
	.string	"kExprI64x2Splat"
.LC225:
	.string	"kExprF32x4Splat"
.LC226:
	.string	"kExprF64x2Splat"
.LC227:
	.string	"kExprI8x16Eq"
.LC228:
	.string	"kExprI8x16Ne"
.LC229:
	.string	"kExprI8x16LtS"
.LC230:
	.string	"kExprI8x16LtU"
.LC231:
	.string	"kExprI8x16GtS"
.LC232:
	.string	"kExprI8x16GtU"
.LC233:
	.string	"kExprI8x16LeS"
.LC234:
	.string	"kExprI8x16LeU"
.LC235:
	.string	"kExprI8x16GeS"
.LC236:
	.string	"kExprI8x16GeU"
.LC237:
	.string	"kExprI16x8Eq"
.LC238:
	.string	"kExprI16x8Ne"
.LC239:
	.string	"kExprI16x8LtS"
.LC240:
	.string	"kExprI16x8LtU"
.LC241:
	.string	"kExprI16x8GtS"
.LC242:
	.string	"kExprI16x8GtU"
.LC243:
	.string	"kExprI16x8LeS"
.LC244:
	.string	"kExprI16x8LeU"
.LC245:
	.string	"kExprI16x8GeS"
.LC246:
	.string	"kExprI16x8GeU"
.LC247:
	.string	"kExprI32x4Eq"
.LC248:
	.string	"kExprI32x4Ne"
.LC249:
	.string	"kExprI32x4LtS"
.LC250:
	.string	"kExprI32x4LtU"
.LC251:
	.string	"kExprI32x4GtS"
.LC252:
	.string	"kExprI32x4GtU"
.LC253:
	.string	"kExprI32x4LeS"
.LC254:
	.string	"kExprI32x4LeU"
.LC255:
	.string	"kExprI32x4GeS"
.LC256:
	.string	"kExprI32x4GeU"
.LC257:
	.string	"kExprI64x2Eq"
.LC258:
	.string	"kExprI64x2Ne"
.LC259:
	.string	"kExprI64x2LtS"
.LC260:
	.string	"kExprI64x2LtU"
.LC261:
	.string	"kExprI64x2GtS"
.LC262:
	.string	"kExprI64x2GtU"
.LC263:
	.string	"kExprI64x2LeS"
.LC264:
	.string	"kExprI64x2LeU"
.LC265:
	.string	"kExprI64x2GeS"
.LC266:
	.string	"kExprI64x2GeU"
.LC267:
	.string	"kExprF32x4Eq"
.LC268:
	.string	"kExprF32x4Ne"
.LC269:
	.string	"kExprF32x4Lt"
.LC270:
	.string	"kExprF32x4Gt"
.LC271:
	.string	"kExprF32x4Le"
.LC272:
	.string	"kExprF32x4Ge"
.LC273:
	.string	"kExprF64x2Eq"
.LC274:
	.string	"kExprF64x2Ne"
.LC275:
	.string	"kExprF64x2Lt"
.LC276:
	.string	"kExprF64x2Gt"
.LC277:
	.string	"kExprF64x2Le"
.LC278:
	.string	"kExprF64x2Ge"
.LC279:
	.string	"kExprS128Not"
.LC280:
	.string	"kExprS128And"
.LC281:
	.string	"kExprS128Or"
.LC282:
	.string	"kExprS128Xor"
.LC283:
	.string	"kExprS128Select"
.LC284:
	.string	"kExprI8x16Neg"
.LC285:
	.string	"kExprS1x16AnyTrue"
.LC286:
	.string	"kExprS1x16AllTrue"
.LC287:
	.string	"kExprI8x16Shl"
.LC288:
	.string	"kExprI8x16ShrS"
.LC289:
	.string	"kExprI8x16ShrU"
.LC290:
	.string	"kExprI8x16Add"
.LC291:
	.string	"kExprI8x16AddSaturateS"
.LC292:
	.string	"kExprI8x16AddSaturateU"
.LC293:
	.string	"kExprI8x16Sub"
.LC294:
	.string	"kExprI8x16SubSaturateS"
.LC295:
	.string	"kExprI8x16SubSaturateU"
.LC296:
	.string	"kExprI8x16Mul"
.LC297:
	.string	"kExprI8x16MinS"
.LC298:
	.string	"kExprI8x16MinU"
.LC299:
	.string	"kExprI8x16MaxS"
.LC300:
	.string	"kExprI8x16MaxU"
.LC301:
	.string	"kExprI16x8Neg"
.LC302:
	.string	"kExprS1x8AnyTrue"
.LC303:
	.string	"kExprS1x8AllTrue"
.LC304:
	.string	"kExprI16x8Shl"
.LC305:
	.string	"kExprI16x8ShrS"
.LC306:
	.string	"kExprI16x8ShrU"
.LC307:
	.string	"kExprI16x8Add"
.LC308:
	.string	"kExprI16x8AddSaturateS"
.LC309:
	.string	"kExprI16x8AddSaturateU"
.LC310:
	.string	"kExprI16x8Sub"
.LC311:
	.string	"kExprI16x8SubSaturateS"
.LC312:
	.string	"kExprI16x8SubSaturateU"
.LC313:
	.string	"kExprI16x8Mul"
.LC314:
	.string	"kExprI16x8MinS"
.LC315:
	.string	"kExprI16x8MinU"
.LC316:
	.string	"kExprI16x8MaxS"
.LC317:
	.string	"kExprI16x8MaxU"
.LC318:
	.string	"kExprI32x4Neg"
.LC319:
	.string	"kExprS1x4AnyTrue"
.LC320:
	.string	"kExprS1x4AllTrue"
.LC321:
	.string	"kExprI32x4Shl"
.LC322:
	.string	"kExprI32x4ShrS"
.LC323:
	.string	"kExprI32x4ShrU"
.LC324:
	.string	"kExprI32x4Add"
.LC325:
	.string	"kExprI32x4Sub"
.LC326:
	.string	"kExprI32x4Mul"
.LC327:
	.string	"kExprI32x4MinS"
.LC328:
	.string	"kExprI32x4MinU"
.LC329:
	.string	"kExprI32x4MaxS"
.LC330:
	.string	"kExprI32x4MaxU"
.LC331:
	.string	"kExprI64x2Neg"
.LC332:
	.string	"kExprS1x2AnyTrue"
.LC333:
	.string	"kExprS1x2AllTrue"
.LC334:
	.string	"kExprI64x2Shl"
.LC335:
	.string	"kExprI64x2ShrS"
.LC336:
	.string	"kExprI64x2ShrU"
.LC337:
	.string	"kExprI64x2Add"
.LC338:
	.string	"kExprI64x2Sub"
.LC339:
	.string	"kExprI64x2Mul"
.LC340:
	.string	"kExprI64x2MinS"
.LC341:
	.string	"kExprI64x2MinU"
.LC342:
	.string	"kExprI64x2MaxS"
.LC343:
	.string	"kExprI64x2MaxU"
.LC344:
	.string	"kExprF32x4Abs"
.LC345:
	.string	"kExprF32x4Neg"
.LC346:
	.string	"kExprF32x4RecipApprox"
.LC347:
	.string	"kExprF32x4RecipSqrtApprox"
.LC348:
	.string	"kExprF32x4Add"
.LC349:
	.string	"kExprF32x4Sub"
.LC350:
	.string	"kExprF32x4Mul"
.LC351:
	.string	"kExprF32x4Div"
.LC352:
	.string	"kExprF32x4Min"
.LC353:
	.string	"kExprF32x4Max"
.LC354:
	.string	"kExprF64x2Abs"
.LC355:
	.string	"kExprF64x2Neg"
.LC356:
	.string	"kExprF64x2Add"
.LC357:
	.string	"kExprF64x2Sub"
.LC358:
	.string	"kExprF64x2Mul"
.LC359:
	.string	"kExprF64x2Div"
.LC360:
	.string	"kExprF64x2Min"
.LC361:
	.string	"kExprF64x2Max"
.LC362:
	.string	"kExprI32x4SConvertF32x4"
.LC363:
	.string	"kExprI32x4UConvertF32x4"
.LC364:
	.string	"kExprF32x4SConvertI32x4"
.LC365:
	.string	"kExprF32x4UConvertI32x4"
.LC366:
	.string	"kExprI8x16SConvertI16x8"
.LC367:
	.string	"kExprI8x16UConvertI16x8"
.LC368:
	.string	"kExprI16x8SConvertI32x4"
.LC369:
	.string	"kExprI16x8UConvertI32x4"
.LC370:
	.string	"kExprI16x8SConvertI8x16Low"
.LC371:
	.string	"kExprI16x8SConvertI8x16High"
.LC372:
	.string	"kExprI16x8UConvertI8x16Low"
.LC373:
	.string	"kExprI16x8UConvertI8x16High"
.LC374:
	.string	"kExprI32x4SConvertI16x8Low"
.LC375:
	.string	"kExprI32x4SConvertI16x8High"
.LC376:
	.string	"kExprI32x4UConvertI16x8Low"
.LC377:
	.string	"kExprI32x4UConvertI16x8High"
.LC378:
	.string	"kExprI16x8AddHoriz"
.LC379:
	.string	"kExprI32x4AddHoriz"
.LC380:
	.string	"kExprF32x4AddHoriz"
.LC381:
	.string	"kExprI8x16ExtractLane"
.LC382:
	.string	"kExprI16x8ExtractLane"
.LC383:
	.string	"kExprI32x4ExtractLane"
.LC384:
	.string	"kExprI64x2ExtractLane"
.LC385:
	.string	"kExprF32x4ExtractLane"
.LC386:
	.string	"kExprF64x2ExtractLane"
.LC387:
	.string	"kExprI8x16ReplaceLane"
.LC388:
	.string	"kExprI16x8ReplaceLane"
.LC389:
	.string	"kExprI32x4ReplaceLane"
.LC390:
	.string	"kExprI64x2ReplaceLane"
.LC391:
	.string	"kExprF32x4ReplaceLane"
.LC392:
	.string	"kExprF64x2ReplaceLane"
.LC393:
	.string	"kExprS8x16Shuffle"
.LC394:
	.string	"kExprS128LoadMem"
.LC395:
	.string	"kExprS128StoreMem"
.LC396:
	.string	"kExprAtomicNotify"
.LC397:
	.string	"kExprI32AtomicWait"
.LC398:
	.string	"kExprI64AtomicWait"
.LC399:
	.string	"kExprI32AtomicLoad"
.LC400:
	.string	"kExprI64AtomicLoad"
.LC401:
	.string	"kExprI32AtomicLoad8U"
.LC402:
	.string	"kExprI32AtomicLoad16U"
.LC403:
	.string	"kExprI64AtomicLoad8U"
.LC404:
	.string	"kExprI64AtomicLoad16U"
.LC405:
	.string	"kExprI64AtomicLoad32U"
.LC406:
	.string	"kExprI32AtomicStore"
.LC407:
	.string	"kExprI64AtomicStore"
.LC408:
	.string	"kExprI32AtomicStore8U"
.LC409:
	.string	"kExprI32AtomicStore16U"
.LC410:
	.string	"kExprI64AtomicStore8U"
.LC411:
	.string	"kExprI64AtomicStore16U"
.LC412:
	.string	"kExprI64AtomicStore32U"
.LC413:
	.string	"kExprI32AtomicAdd"
.LC414:
	.string	"kExprI64AtomicAdd"
.LC415:
	.string	"kExprI32AtomicAdd8U"
.LC416:
	.string	"kExprI32AtomicAdd16U"
.LC417:
	.string	"kExprI64AtomicAdd8U"
.LC418:
	.string	"kExprI64AtomicAdd16U"
.LC419:
	.string	"kExprI64AtomicAdd32U"
.LC420:
	.string	"kExprI32AtomicSub"
.LC421:
	.string	"kExprI64AtomicSub"
.LC422:
	.string	"kExprI32AtomicSub8U"
.LC423:
	.string	"kExprI32AtomicSub16U"
.LC424:
	.string	"kExprI64AtomicSub8U"
.LC425:
	.string	"kExprI64AtomicSub16U"
.LC426:
	.string	"kExprI64AtomicSub32U"
.LC427:
	.string	"kExprI32AtomicAnd"
.LC428:
	.string	"kExprI64AtomicAnd"
.LC429:
	.string	"kExprI32AtomicAnd8U"
.LC430:
	.string	"kExprI32AtomicAnd16U"
.LC431:
	.string	"kExprI64AtomicAnd8U"
.LC432:
	.string	"kExprI64AtomicAnd16U"
.LC433:
	.string	"kExprI64AtomicAnd32U"
.LC434:
	.string	"kExprI32AtomicOr"
.LC435:
	.string	"kExprI64AtomicOr"
.LC436:
	.string	"kExprI32AtomicOr8U"
.LC437:
	.string	"kExprI32AtomicOr16U"
.LC438:
	.string	"kExprI64AtomicOr8U"
.LC439:
	.string	"kExprI64AtomicOr16U"
.LC440:
	.string	"kExprI64AtomicOr32U"
.LC441:
	.string	"kExprI32AtomicXor"
.LC442:
	.string	"kExprI64AtomicXor"
.LC443:
	.string	"kExprI32AtomicXor8U"
.LC444:
	.string	"kExprI32AtomicXor16U"
.LC445:
	.string	"kExprI64AtomicXor8U"
.LC446:
	.string	"kExprI64AtomicXor16U"
.LC447:
	.string	"kExprI64AtomicXor32U"
.LC448:
	.string	"kExprI32AtomicExchange"
.LC449:
	.string	"kExprI64AtomicExchange"
.LC450:
	.string	"kExprI32AtomicExchange8U"
.LC451:
	.string	"kExprI32AtomicExchange16U"
.LC452:
	.string	"kExprI64AtomicExchange8U"
.LC453:
	.string	"kExprI64AtomicExchange16U"
.LC454:
	.string	"kExprI64AtomicExchange32U"
.LC455:
	.string	"kExprI32AtomicCompareExchange"
.LC456:
	.string	"kExprI64AtomicCompareExchange"
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_113RawOpcodeNameENS1_10WasmOpcodeE.str1.8,"aMS",@progbits,1
	.align 8
.LC457:
	.string	"kExprI32AtomicCompareExchange8U"
	.align 8
.LC458:
	.string	"kExprI32AtomicCompareExchange16U"
	.align 8
.LC459:
	.string	"kExprI64AtomicCompareExchange8U"
	.align 8
.LC460:
	.string	"kExprI64AtomicCompareExchange16U"
	.align 8
.LC461:
	.string	"kExprI64AtomicCompareExchange32U"
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_113RawOpcodeNameENS1_10WasmOpcodeE.str1.1
.LC462:
	.string	"kExprAtomicFence"
.LC463:
	.string	"kExprI32SConvertSatF32"
.LC464:
	.string	"kExprI32UConvertSatF32"
.LC465:
	.string	"kExprI32SConvertSatF64"
.LC466:
	.string	"kExprI32UConvertSatF64"
.LC467:
	.string	"kExprI64SConvertSatF32"
.LC468:
	.string	"kExprI64UConvertSatF32"
.LC469:
	.string	"kExprI64SConvertSatF64"
.LC470:
	.string	"kExprI64UConvertSatF64"
.LC471:
	.string	"kExprMemoryInit"
.LC472:
	.string	"kExprDataDrop"
.LC473:
	.string	"kExprMemoryCopy"
.LC474:
	.string	"kExprMemoryFill"
.LC475:
	.string	"kExprTableInit"
.LC476:
	.string	"kExprElemDrop"
.LC477:
	.string	"kExprTableCopy"
.LC478:
	.string	"kExprTableGrow"
.LC479:
	.string	"kExprTableSize"
.LC480:
	.string	"kExprTableFill"
.LC481:
	.string	"kExprNop"
	.section	.text._ZN2v88internal4wasm12_GLOBAL__N_113RawOpcodeNameENS1_10WasmOpcodeE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal4wasm12_GLOBAL__N_113RawOpcodeNameENS1_10WasmOpcodeE, @function
_ZN2v88internal4wasm12_GLOBAL__N_113RawOpcodeNameENS1_10WasmOpcodeE:
.LFB19615:
	.cfi_startproc
	cmpl	$230, %edi
	jbe	.L495
	subl	$64512, %edi
	cmpl	$590, %edi
	ja	.L496
	leaq	.L9(%rip), %rdx
	movslq	(%rdx,%rdi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_113RawOpcodeNameENS1_10WasmOpcodeE,"a",@progbits
	.align 4
	.align 4
.L9:
	.long	.L268-.L9
	.long	.L267-.L9
	.long	.L266-.L9
	.long	.L265-.L9
	.long	.L264-.L9
	.long	.L263-.L9
	.long	.L262-.L9
	.long	.L261-.L9
	.long	.L260-.L9
	.long	.L259-.L9
	.long	.L258-.L9
	.long	.L257-.L9
	.long	.L256-.L9
	.long	.L255-.L9
	.long	.L254-.L9
	.long	.L253-.L9
	.long	.L252-.L9
	.long	.L251-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L250-.L9
	.long	.L249-.L9
	.long	.L7-.L9
	.long	.L248-.L9
	.long	.L247-.L9
	.long	.L246-.L9
	.long	.L7-.L9
	.long	.L245-.L9
	.long	.L492-.L9
	.long	.L243-.L9
	.long	.L7-.L9
	.long	.L242-.L9
	.long	.L241-.L9
	.long	.L240-.L9
	.long	.L239-.L9
	.long	.L238-.L9
	.long	.L237-.L9
	.long	.L236-.L9
	.long	.L235-.L9
	.long	.L234-.L9
	.long	.L233-.L9
	.long	.L232-.L9
	.long	.L231-.L9
	.long	.L230-.L9
	.long	.L229-.L9
	.long	.L228-.L9
	.long	.L227-.L9
	.long	.L226-.L9
	.long	.L225-.L9
	.long	.L224-.L9
	.long	.L223-.L9
	.long	.L222-.L9
	.long	.L221-.L9
	.long	.L220-.L9
	.long	.L219-.L9
	.long	.L218-.L9
	.long	.L217-.L9
	.long	.L216-.L9
	.long	.L215-.L9
	.long	.L214-.L9
	.long	.L213-.L9
	.long	.L212-.L9
	.long	.L211-.L9
	.long	.L210-.L9
	.long	.L209-.L9
	.long	.L208-.L9
	.long	.L207-.L9
	.long	.L206-.L9
	.long	.L205-.L9
	.long	.L204-.L9
	.long	.L203-.L9
	.long	.L202-.L9
	.long	.L201-.L9
	.long	.L200-.L9
	.long	.L199-.L9
	.long	.L198-.L9
	.long	.L197-.L9
	.long	.L196-.L9
	.long	.L195-.L9
	.long	.L194-.L9
	.long	.L193-.L9
	.long	.L192-.L9
	.long	.L191-.L9
	.long	.L190-.L9
	.long	.L189-.L9
	.long	.L188-.L9
	.long	.L187-.L9
	.long	.L186-.L9
	.long	.L185-.L9
	.long	.L184-.L9
	.long	.L183-.L9
	.long	.L182-.L9
	.long	.L181-.L9
	.long	.L180-.L9
	.long	.L179-.L9
	.long	.L178-.L9
	.long	.L177-.L9
	.long	.L176-.L9
	.long	.L175-.L9
	.long	.L174-.L9
	.long	.L173-.L9
	.long	.L172-.L9
	.long	.L171-.L9
	.long	.L170-.L9
	.long	.L169-.L9
	.long	.L168-.L9
	.long	.L167-.L9
	.long	.L166-.L9
	.long	.L165-.L9
	.long	.L164-.L9
	.long	.L163-.L9
	.long	.L162-.L9
	.long	.L161-.L9
	.long	.L160-.L9
	.long	.L159-.L9
	.long	.L158-.L9
	.long	.L157-.L9
	.long	.L156-.L9
	.long	.L155-.L9
	.long	.L154-.L9
	.long	.L153-.L9
	.long	.L152-.L9
	.long	.L151-.L9
	.long	.L150-.L9
	.long	.L149-.L9
	.long	.L148-.L9
	.long	.L147-.L9
	.long	.L146-.L9
	.long	.L145-.L9
	.long	.L144-.L9
	.long	.L143-.L9
	.long	.L142-.L9
	.long	.L141-.L9
	.long	.L140-.L9
	.long	.L139-.L9
	.long	.L138-.L9
	.long	.L137-.L9
	.long	.L136-.L9
	.long	.L135-.L9
	.long	.L134-.L9
	.long	.L133-.L9
	.long	.L132-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L131-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L130-.L9
	.long	.L129-.L9
	.long	.L128-.L9
	.long	.L127-.L9
	.long	.L126-.L9
	.long	.L125-.L9
	.long	.L124-.L9
	.long	.L123-.L9
	.long	.L122-.L9
	.long	.L121-.L9
	.long	.L120-.L9
	.long	.L119-.L9
	.long	.L7-.L9
	.long	.L118-.L9
	.long	.L117-.L9
	.long	.L116-.L9
	.long	.L115-.L9
	.long	.L114-.L9
	.long	.L113-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L112-.L9
	.long	.L111-.L9
	.long	.L7-.L9
	.long	.L110-.L9
	.long	.L109-.L9
	.long	.L108-.L9
	.long	.L107-.L9
	.long	.L106-.L9
	.long	.L105-.L9
	.long	.L104-.L9
	.long	.L103-.L9
	.long	.L102-.L9
	.long	.L101-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L100-.L9
	.long	.L99-.L9
	.long	.L98-.L9
	.long	.L97-.L9
	.long	.L96-.L9
	.long	.L95-.L9
	.long	.L94-.L9
	.long	.L93-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L92-.L9
	.long	.L91-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L90-.L9
	.long	.L89-.L9
	.long	.L88-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L87-.L9
	.long	.L86-.L9
	.long	.L85-.L9
	.long	.L84-.L9
	.long	.L83-.L9
	.long	.L82-.L9
	.long	.L81-.L9
	.long	.L80-.L9
	.long	.L79-.L9
	.long	.L78-.L9
	.long	.L77-.L9
	.long	.L76-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L75-.L9
	.long	.L74-.L9
	.long	.L73-.L9
	.long	.L72-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L71-.L9
	.long	.L70-.L9
	.long	.L69-.L9
	.long	.L68-.L9
	.long	.L67-.L9
	.long	.L66-.L9
	.long	.L65-.L9
	.long	.L64-.L9
	.long	.L63-.L9
	.long	.L62-.L9
	.long	.L61-.L9
	.long	.L60-.L9
	.long	.L59-.L9
	.long	.L58-.L9
	.long	.L57-.L9
	.long	.L56-.L9
	.long	.L55-.L9
	.long	.L54-.L9
	.long	.L53-.L9
	.long	.L52-.L9
	.long	.L51-.L9
	.long	.L50-.L9
	.long	.L49-.L9
	.long	.L48-.L9
	.long	.L47-.L9
	.long	.L46-.L9
	.long	.L45-.L9
	.long	.L44-.L9
	.long	.L43-.L9
	.long	.L42-.L9
	.long	.L41-.L9
	.long	.L40-.L9
	.long	.L39-.L9
	.long	.L38-.L9
	.long	.L37-.L9
	.long	.L36-.L9
	.long	.L35-.L9
	.long	.L34-.L9
	.long	.L33-.L9
	.long	.L32-.L9
	.long	.L31-.L9
	.long	.L30-.L9
	.long	.L29-.L9
	.long	.L28-.L9
	.long	.L27-.L9
	.long	.L26-.L9
	.long	.L25-.L9
	.long	.L24-.L9
	.long	.L23-.L9
	.long	.L22-.L9
	.long	.L21-.L9
	.long	.L20-.L9
	.long	.L19-.L9
	.long	.L18-.L9
	.long	.L17-.L9
	.long	.L16-.L9
	.long	.L15-.L9
	.long	.L14-.L9
	.long	.L13-.L9
	.long	.L12-.L9
	.long	.L11-.L9
	.long	.L10-.L9
	.long	.L8-.L9
	.section	.text._ZN2v88internal4wasm12_GLOBAL__N_113RawOpcodeNameENS1_10WasmOpcodeE
.L492:
	leaq	.LC222(%rip), %rax
	ret
.L247:
	leaq	.LC221(%rip), %rax
	ret
.L7:
	leaq	.LC0(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L495:
	leaq	.L271(%rip), %rdx
	movl	%edi, %edi
	movslq	(%rdx,%rdi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_113RawOpcodeNameENS1_10WasmOpcodeE
	.align 4
	.align 4
.L271:
	.long	.L493-.L271
	.long	.L490-.L271
	.long	.L489-.L271
	.long	.L488-.L271
	.long	.L487-.L271
	.long	.L486-.L271
	.long	.L485-.L271
	.long	.L484-.L271
	.long	.L483-.L271
	.long	.L482-.L271
	.long	.L481-.L271
	.long	.L480-.L271
	.long	.L479-.L271
	.long	.L478-.L271
	.long	.L477-.L271
	.long	.L476-.L271
	.long	.L475-.L271
	.long	.L474-.L271
	.long	.L473-.L271
	.long	.L472-.L271
	.long	.L269-.L271
	.long	.L269-.L271
	.long	.L269-.L271
	.long	.L269-.L271
	.long	.L269-.L271
	.long	.L269-.L271
	.long	.L471-.L271
	.long	.L470-.L271
	.long	.L469-.L271
	.long	.L269-.L271
	.long	.L269-.L271
	.long	.L269-.L271
	.long	.L468-.L271
	.long	.L467-.L271
	.long	.L466-.L271
	.long	.L465-.L271
	.long	.L464-.L271
	.long	.L463-.L271
	.long	.L462-.L271
	.long	.L269-.L271
	.long	.L461-.L271
	.long	.L460-.L271
	.long	.L459-.L271
	.long	.L458-.L271
	.long	.L457-.L271
	.long	.L456-.L271
	.long	.L455-.L271
	.long	.L454-.L271
	.long	.L453-.L271
	.long	.L452-.L271
	.long	.L451-.L271
	.long	.L450-.L271
	.long	.L449-.L271
	.long	.L448-.L271
	.long	.L447-.L271
	.long	.L446-.L271
	.long	.L445-.L271
	.long	.L444-.L271
	.long	.L443-.L271
	.long	.L442-.L271
	.long	.L441-.L271
	.long	.L440-.L271
	.long	.L439-.L271
	.long	.L438-.L271
	.long	.L437-.L271
	.long	.L436-.L271
	.long	.L435-.L271
	.long	.L434-.L271
	.long	.L433-.L271
	.long	.L432-.L271
	.long	.L431-.L271
	.long	.L430-.L271
	.long	.L429-.L271
	.long	.L428-.L271
	.long	.L427-.L271
	.long	.L426-.L271
	.long	.L425-.L271
	.long	.L424-.L271
	.long	.L423-.L271
	.long	.L422-.L271
	.long	.L421-.L271
	.long	.L420-.L271
	.long	.L419-.L271
	.long	.L418-.L271
	.long	.L417-.L271
	.long	.L416-.L271
	.long	.L415-.L271
	.long	.L414-.L271
	.long	.L413-.L271
	.long	.L412-.L271
	.long	.L411-.L271
	.long	.L410-.L271
	.long	.L409-.L271
	.long	.L408-.L271
	.long	.L407-.L271
	.long	.L406-.L271
	.long	.L405-.L271
	.long	.L404-.L271
	.long	.L403-.L271
	.long	.L402-.L271
	.long	.L401-.L271
	.long	.L400-.L271
	.long	.L399-.L271
	.long	.L398-.L271
	.long	.L397-.L271
	.long	.L396-.L271
	.long	.L395-.L271
	.long	.L394-.L271
	.long	.L393-.L271
	.long	.L392-.L271
	.long	.L391-.L271
	.long	.L390-.L271
	.long	.L389-.L271
	.long	.L388-.L271
	.long	.L387-.L271
	.long	.L386-.L271
	.long	.L385-.L271
	.long	.L384-.L271
	.long	.L383-.L271
	.long	.L382-.L271
	.long	.L381-.L271
	.long	.L380-.L271
	.long	.L379-.L271
	.long	.L378-.L271
	.long	.L377-.L271
	.long	.L376-.L271
	.long	.L375-.L271
	.long	.L374-.L271
	.long	.L373-.L271
	.long	.L372-.L271
	.long	.L371-.L271
	.long	.L370-.L271
	.long	.L369-.L271
	.long	.L368-.L271
	.long	.L367-.L271
	.long	.L366-.L271
	.long	.L365-.L271
	.long	.L364-.L271
	.long	.L363-.L271
	.long	.L362-.L271
	.long	.L361-.L271
	.long	.L360-.L271
	.long	.L359-.L271
	.long	.L358-.L271
	.long	.L357-.L271
	.long	.L356-.L271
	.long	.L355-.L271
	.long	.L354-.L271
	.long	.L353-.L271
	.long	.L352-.L271
	.long	.L351-.L271
	.long	.L350-.L271
	.long	.L349-.L271
	.long	.L348-.L271
	.long	.L347-.L271
	.long	.L346-.L271
	.long	.L345-.L271
	.long	.L344-.L271
	.long	.L343-.L271
	.long	.L342-.L271
	.long	.L341-.L271
	.long	.L340-.L271
	.long	.L339-.L271
	.long	.L338-.L271
	.long	.L337-.L271
	.long	.L336-.L271
	.long	.L335-.L271
	.long	.L334-.L271
	.long	.L333-.L271
	.long	.L332-.L271
	.long	.L331-.L271
	.long	.L330-.L271
	.long	.L329-.L271
	.long	.L328-.L271
	.long	.L327-.L271
	.long	.L326-.L271
	.long	.L325-.L271
	.long	.L324-.L271
	.long	.L323-.L271
	.long	.L322-.L271
	.long	.L321-.L271
	.long	.L320-.L271
	.long	.L319-.L271
	.long	.L318-.L271
	.long	.L317-.L271
	.long	.L316-.L271
	.long	.L315-.L271
	.long	.L314-.L271
	.long	.L313-.L271
	.long	.L312-.L271
	.long	.L311-.L271
	.long	.L310-.L271
	.long	.L309-.L271
	.long	.L308-.L271
	.long	.L307-.L271
	.long	.L306-.L271
	.long	.L305-.L271
	.long	.L304-.L271
	.long	.L303-.L271
	.long	.L302-.L271
	.long	.L301-.L271
	.long	.L300-.L271
	.long	.L299-.L271
	.long	.L298-.L271
	.long	.L297-.L271
	.long	.L296-.L271
	.long	.L295-.L271
	.long	.L294-.L271
	.long	.L293-.L271
	.long	.L292-.L271
	.long	.L291-.L271
	.long	.L290-.L271
	.long	.L289-.L271
	.long	.L288-.L271
	.long	.L287-.L271
	.long	.L286-.L271
	.long	.L285-.L271
	.long	.L284-.L271
	.long	.L283-.L271
	.long	.L282-.L271
	.long	.L281-.L271
	.long	.L280-.L271
	.long	.L279-.L271
	.long	.L278-.L271
	.long	.L277-.L271
	.long	.L276-.L271
	.long	.L275-.L271
	.long	.L274-.L271
	.long	.L273-.L271
	.long	.L272-.L271
	.long	.L270-.L271
	.section	.text._ZN2v88internal4wasm12_GLOBAL__N_113RawOpcodeNameENS1_10WasmOpcodeE
.L490:
	leaq	.LC481(%rip), %rax
	ret
.L493:
	leaq	.LC1(%rip), %rax
	ret
.L269:
	leaq	.LC0(%rip), %rax
	ret
.L270:
	leaq	.LC220(%rip), %rax
	ret
.L272:
	leaq	.LC219(%rip), %rax
	ret
.L273:
	leaq	.LC218(%rip), %rax
	ret
.L274:
	leaq	.LC217(%rip), %rax
	ret
.L275:
	leaq	.LC216(%rip), %rax
	ret
.L276:
	leaq	.LC215(%rip), %rax
	ret
.L277:
	leaq	.LC214(%rip), %rax
	ret
.L278:
	leaq	.LC213(%rip), %rax
	ret
.L279:
	leaq	.LC212(%rip), %rax
	ret
.L280:
	leaq	.LC211(%rip), %rax
	ret
.L281:
	leaq	.LC210(%rip), %rax
	ret
.L282:
	leaq	.LC209(%rip), %rax
	ret
.L283:
	leaq	.LC208(%rip), %rax
	ret
.L284:
	leaq	.LC207(%rip), %rax
	ret
.L285:
	leaq	.LC206(%rip), %rax
	ret
.L286:
	leaq	.LC205(%rip), %rax
	ret
.L287:
	leaq	.LC204(%rip), %rax
	ret
.L288:
	leaq	.LC203(%rip), %rax
	ret
.L289:
	leaq	.LC202(%rip), %rax
	ret
.L290:
	leaq	.LC201(%rip), %rax
	ret
.L291:
	leaq	.LC35(%rip), %rax
	ret
.L292:
	leaq	.LC164(%rip), %rax
	ret
.L293:
	leaq	.LC34(%rip), %rax
	ret
.L294:
	leaq	.LC200(%rip), %rax
	ret
.L295:
	leaq	.LC199(%rip), %rax
	ret
.L296:
	leaq	.LC198(%rip), %rax
	ret
.L297:
	leaq	.LC197(%rip), %rax
	ret
.L298:
	leaq	.LC196(%rip), %rax
	ret
.L299:
	leaq	.LC195(%rip), %rax
	ret
.L300:
	leaq	.LC194(%rip), %rax
	ret
.L301:
	leaq	.LC193(%rip), %rax
	ret
.L302:
	leaq	.LC192(%rip), %rax
	ret
.L303:
	leaq	.LC191(%rip), %rax
	ret
.L304:
	leaq	.LC190(%rip), %rax
	ret
.L305:
	leaq	.LC163(%rip), %rax
	ret
.L306:
	leaq	.LC162(%rip), %rax
	ret
.L307:
	leaq	.LC161(%rip), %rax
	ret
.L308:
	leaq	.LC160(%rip), %rax
	ret
.L309:
	leaq	.LC159(%rip), %rax
	ret
.L310:
	leaq	.LC158(%rip), %rax
	ret
.L311:
	leaq	.LC157(%rip), %rax
	ret
.L312:
	leaq	.LC156(%rip), %rax
	ret
.L313:
	leaq	.LC155(%rip), %rax
	ret
.L314:
	leaq	.LC154(%rip), %rax
	ret
.L315:
	leaq	.LC153(%rip), %rax
	ret
.L316:
	leaq	.LC152(%rip), %rax
	ret
.L317:
	leaq	.LC151(%rip), %rax
	ret
.L318:
	leaq	.LC150(%rip), %rax
	ret
.L319:
	leaq	.LC149(%rip), %rax
	ret
.L320:
	leaq	.LC148(%rip), %rax
	ret
.L321:
	leaq	.LC147(%rip), %rax
	ret
.L322:
	leaq	.LC146(%rip), %rax
	ret
.L323:
	leaq	.LC145(%rip), %rax
	ret
.L324:
	leaq	.LC144(%rip), %rax
	ret
.L325:
	leaq	.LC143(%rip), %rax
	ret
.L326:
	leaq	.LC142(%rip), %rax
	ret
.L327:
	leaq	.LC141(%rip), %rax
	ret
.L328:
	leaq	.LC140(%rip), %rax
	ret
.L329:
	leaq	.LC139(%rip), %rax
	ret
.L330:
	leaq	.LC138(%rip), %rax
	ret
.L331:
	leaq	.LC137(%rip), %rax
	ret
.L332:
	leaq	.LC136(%rip), %rax
	ret
.L333:
	leaq	.LC135(%rip), %rax
	ret
.L334:
	leaq	.LC134(%rip), %rax
	ret
.L335:
	leaq	.LC133(%rip), %rax
	ret
.L336:
	leaq	.LC132(%rip), %rax
	ret
.L337:
	leaq	.LC131(%rip), %rax
	ret
.L338:
	leaq	.LC130(%rip), %rax
	ret
.L339:
	leaq	.LC129(%rip), %rax
	ret
.L340:
	leaq	.LC128(%rip), %rax
	ret
.L341:
	leaq	.LC127(%rip), %rax
	ret
.L342:
	leaq	.LC126(%rip), %rax
	ret
.L343:
	leaq	.LC125(%rip), %rax
	ret
.L344:
	leaq	.LC124(%rip), %rax
	ret
.L345:
	leaq	.LC123(%rip), %rax
	ret
.L346:
	leaq	.LC122(%rip), %rax
	ret
.L347:
	leaq	.LC121(%rip), %rax
	ret
.L348:
	leaq	.LC120(%rip), %rax
	ret
.L349:
	leaq	.LC119(%rip), %rax
	ret
.L350:
	leaq	.LC118(%rip), %rax
	ret
.L351:
	leaq	.LC117(%rip), %rax
	ret
.L352:
	leaq	.LC116(%rip), %rax
	ret
.L353:
	leaq	.LC115(%rip), %rax
	ret
.L354:
	leaq	.LC114(%rip), %rax
	ret
.L355:
	leaq	.LC113(%rip), %rax
	ret
.L356:
	leaq	.LC112(%rip), %rax
	ret
.L357:
	leaq	.LC111(%rip), %rax
	ret
.L358:
	leaq	.LC110(%rip), %rax
	ret
.L359:
	leaq	.LC109(%rip), %rax
	ret
.L360:
	leaq	.LC108(%rip), %rax
	ret
.L361:
	leaq	.LC107(%rip), %rax
	ret
.L362:
	leaq	.LC106(%rip), %rax
	ret
.L363:
	leaq	.LC105(%rip), %rax
	ret
.L364:
	leaq	.LC104(%rip), %rax
	ret
.L365:
	leaq	.LC103(%rip), %rax
	ret
.L366:
	leaq	.LC102(%rip), %rax
	ret
.L367:
	leaq	.LC101(%rip), %rax
	ret
.L368:
	leaq	.LC100(%rip), %rax
	ret
.L369:
	leaq	.LC99(%rip), %rax
	ret
.L370:
	leaq	.LC98(%rip), %rax
	ret
.L371:
	leaq	.LC97(%rip), %rax
	ret
.L372:
	leaq	.LC96(%rip), %rax
	ret
.L373:
	leaq	.LC95(%rip), %rax
	ret
.L374:
	leaq	.LC94(%rip), %rax
	ret
.L375:
	leaq	.LC93(%rip), %rax
	ret
.L376:
	leaq	.LC92(%rip), %rax
	ret
.L377:
	leaq	.LC91(%rip), %rax
	ret
.L378:
	leaq	.LC90(%rip), %rax
	ret
.L379:
	leaq	.LC89(%rip), %rax
	ret
.L380:
	leaq	.LC88(%rip), %rax
	ret
.L381:
	leaq	.LC87(%rip), %rax
	ret
.L382:
	leaq	.LC86(%rip), %rax
	ret
.L383:
	leaq	.LC85(%rip), %rax
	ret
.L384:
	leaq	.LC84(%rip), %rax
	ret
.L385:
	leaq	.LC83(%rip), %rax
	ret
.L386:
	leaq	.LC82(%rip), %rax
	ret
.L387:
	leaq	.LC81(%rip), %rax
	ret
.L388:
	leaq	.LC80(%rip), %rax
	ret
.L389:
	leaq	.LC79(%rip), %rax
	ret
.L390:
	leaq	.LC78(%rip), %rax
	ret
.L391:
	leaq	.LC77(%rip), %rax
	ret
.L392:
	leaq	.LC76(%rip), %rax
	ret
.L393:
	leaq	.LC75(%rip), %rax
	ret
.L394:
	leaq	.LC74(%rip), %rax
	ret
.L395:
	leaq	.LC73(%rip), %rax
	ret
.L396:
	leaq	.LC72(%rip), %rax
	ret
.L397:
	leaq	.LC71(%rip), %rax
	ret
.L398:
	leaq	.LC70(%rip), %rax
	ret
.L399:
	leaq	.LC69(%rip), %rax
	ret
.L400:
	leaq	.LC68(%rip), %rax
	ret
.L401:
	leaq	.LC67(%rip), %rax
	ret
.L402:
	leaq	.LC66(%rip), %rax
	ret
.L403:
	leaq	.LC65(%rip), %rax
	ret
.L404:
	leaq	.LC64(%rip), %rax
	ret
.L405:
	leaq	.LC63(%rip), %rax
	ret
.L406:
	leaq	.LC62(%rip), %rax
	ret
.L407:
	leaq	.LC61(%rip), %rax
	ret
.L408:
	leaq	.LC60(%rip), %rax
	ret
.L409:
	leaq	.LC59(%rip), %rax
	ret
.L410:
	leaq	.LC58(%rip), %rax
	ret
.L411:
	leaq	.LC57(%rip), %rax
	ret
.L412:
	leaq	.LC56(%rip), %rax
	ret
.L413:
	leaq	.LC55(%rip), %rax
	ret
.L414:
	leaq	.LC54(%rip), %rax
	ret
.L415:
	leaq	.LC53(%rip), %rax
	ret
.L416:
	leaq	.LC52(%rip), %rax
	ret
.L417:
	leaq	.LC51(%rip), %rax
	ret
.L418:
	leaq	.LC50(%rip), %rax
	ret
.L419:
	leaq	.LC49(%rip), %rax
	ret
.L420:
	leaq	.LC48(%rip), %rax
	ret
.L421:
	leaq	.LC47(%rip), %rax
	ret
.L422:
	leaq	.LC46(%rip), %rax
	ret
.L423:
	leaq	.LC45(%rip), %rax
	ret
.L424:
	leaq	.LC44(%rip), %rax
	ret
.L425:
	leaq	.LC43(%rip), %rax
	ret
.L426:
	leaq	.LC42(%rip), %rax
	ret
.L427:
	leaq	.LC41(%rip), %rax
	ret
.L428:
	leaq	.LC40(%rip), %rax
	ret
.L429:
	leaq	.LC39(%rip), %rax
	ret
.L430:
	leaq	.LC38(%rip), %rax
	ret
.L431:
	leaq	.LC37(%rip), %rax
	ret
.L432:
	leaq	.LC36(%rip), %rax
	ret
.L433:
	leaq	.LC33(%rip), %rax
	ret
.L434:
	leaq	.LC32(%rip), %rax
	ret
.L435:
	leaq	.LC31(%rip), %rax
	ret
.L436:
	leaq	.LC30(%rip), %rax
	ret
.L437:
	leaq	.LC189(%rip), %rax
	ret
.L438:
	leaq	.LC188(%rip), %rax
	ret
.L439:
	leaq	.LC173(%rip), %rax
	ret
.L440:
	leaq	.LC172(%rip), %rax
	ret
.L441:
	leaq	.LC171(%rip), %rax
	ret
.L442:
	leaq	.LC170(%rip), %rax
	ret
.L443:
	leaq	.LC169(%rip), %rax
	ret
.L444:
	leaq	.LC168(%rip), %rax
	ret
.L445:
	leaq	.LC167(%rip), %rax
	ret
.L446:
	leaq	.LC166(%rip), %rax
	ret
.L447:
	leaq	.LC165(%rip), %rax
	ret
.L448:
	leaq	.LC187(%rip), %rax
	ret
.L449:
	leaq	.LC186(%rip), %rax
	ret
.L450:
	leaq	.LC185(%rip), %rax
	ret
.L451:
	leaq	.LC184(%rip), %rax
	ret
.L452:
	leaq	.LC183(%rip), %rax
	ret
.L453:
	leaq	.LC182(%rip), %rax
	ret
.L454:
	leaq	.LC181(%rip), %rax
	ret
.L455:
	leaq	.LC180(%rip), %rax
	ret
.L456:
	leaq	.LC179(%rip), %rax
	ret
.L457:
	leaq	.LC178(%rip), %rax
	ret
.L458:
	leaq	.LC177(%rip), %rax
	ret
.L459:
	leaq	.LC176(%rip), %rax
	ret
.L460:
	leaq	.LC175(%rip), %rax
	ret
.L461:
	leaq	.LC174(%rip), %rax
	ret
.L462:
	leaq	.LC29(%rip), %rax
	ret
.L463:
	leaq	.LC28(%rip), %rax
	ret
.L464:
	leaq	.LC27(%rip), %rax
	ret
.L465:
	leaq	.LC26(%rip), %rax
	ret
.L466:
	leaq	.LC25(%rip), %rax
	ret
.L467:
	leaq	.LC24(%rip), %rax
	ret
.L468:
	leaq	.LC23(%rip), %rax
	ret
.L469:
	leaq	.LC22(%rip), %rax
	ret
.L470:
	leaq	.LC21(%rip), %rax
	ret
.L471:
	leaq	.LC20(%rip), %rax
	ret
.L472:
	leaq	.LC19(%rip), %rax
	ret
.L473:
	leaq	.LC18(%rip), %rax
	ret
.L474:
	leaq	.LC17(%rip), %rax
	ret
.L475:
	leaq	.LC16(%rip), %rax
	ret
.L476:
	leaq	.LC15(%rip), %rax
	ret
.L477:
	leaq	.LC14(%rip), %rax
	ret
.L478:
	leaq	.LC13(%rip), %rax
	ret
.L479:
	leaq	.LC12(%rip), %rax
	ret
.L480:
	leaq	.LC11(%rip), %rax
	ret
.L481:
	leaq	.LC10(%rip), %rax
	ret
.L482:
	leaq	.LC9(%rip), %rax
	ret
.L483:
	leaq	.LC8(%rip), %rax
	ret
.L484:
	leaq	.LC7(%rip), %rax
	ret
.L485:
	leaq	.LC6(%rip), %rax
	ret
.L486:
	leaq	.LC5(%rip), %rax
	ret
.L487:
	leaq	.LC4(%rip), %rax
	ret
.L488:
	leaq	.LC3(%rip), %rax
	ret
.L489:
	leaq	.LC2(%rip), %rax
	ret
.L8:
	leaq	.LC461(%rip), %rax
	ret
.L10:
	leaq	.LC460(%rip), %rax
	ret
.L11:
	leaq	.LC459(%rip), %rax
	ret
.L12:
	leaq	.LC458(%rip), %rax
	ret
.L13:
	leaq	.LC457(%rip), %rax
	ret
.L14:
	leaq	.LC456(%rip), %rax
	ret
.L15:
	leaq	.LC455(%rip), %rax
	ret
.L16:
	leaq	.LC454(%rip), %rax
	ret
.L17:
	leaq	.LC453(%rip), %rax
	ret
.L18:
	leaq	.LC452(%rip), %rax
	ret
.L19:
	leaq	.LC451(%rip), %rax
	ret
.L20:
	leaq	.LC450(%rip), %rax
	ret
.L21:
	leaq	.LC449(%rip), %rax
	ret
.L22:
	leaq	.LC448(%rip), %rax
	ret
.L23:
	leaq	.LC447(%rip), %rax
	ret
.L24:
	leaq	.LC446(%rip), %rax
	ret
.L25:
	leaq	.LC445(%rip), %rax
	ret
.L26:
	leaq	.LC444(%rip), %rax
	ret
.L27:
	leaq	.LC443(%rip), %rax
	ret
.L28:
	leaq	.LC442(%rip), %rax
	ret
.L29:
	leaq	.LC441(%rip), %rax
	ret
.L30:
	leaq	.LC440(%rip), %rax
	ret
.L31:
	leaq	.LC439(%rip), %rax
	ret
.L32:
	leaq	.LC438(%rip), %rax
	ret
.L33:
	leaq	.LC437(%rip), %rax
	ret
.L34:
	leaq	.LC436(%rip), %rax
	ret
.L35:
	leaq	.LC435(%rip), %rax
	ret
.L36:
	leaq	.LC434(%rip), %rax
	ret
.L37:
	leaq	.LC433(%rip), %rax
	ret
.L38:
	leaq	.LC432(%rip), %rax
	ret
.L39:
	leaq	.LC431(%rip), %rax
	ret
.L40:
	leaq	.LC430(%rip), %rax
	ret
.L41:
	leaq	.LC429(%rip), %rax
	ret
.L42:
	leaq	.LC428(%rip), %rax
	ret
.L43:
	leaq	.LC427(%rip), %rax
	ret
.L44:
	leaq	.LC426(%rip), %rax
	ret
.L45:
	leaq	.LC425(%rip), %rax
	ret
.L46:
	leaq	.LC424(%rip), %rax
	ret
.L47:
	leaq	.LC423(%rip), %rax
	ret
.L48:
	leaq	.LC422(%rip), %rax
	ret
.L49:
	leaq	.LC421(%rip), %rax
	ret
.L50:
	leaq	.LC420(%rip), %rax
	ret
.L51:
	leaq	.LC419(%rip), %rax
	ret
.L52:
	leaq	.LC418(%rip), %rax
	ret
.L53:
	leaq	.LC417(%rip), %rax
	ret
.L54:
	leaq	.LC416(%rip), %rax
	ret
.L55:
	leaq	.LC415(%rip), %rax
	ret
.L56:
	leaq	.LC414(%rip), %rax
	ret
.L57:
	leaq	.LC413(%rip), %rax
	ret
.L58:
	leaq	.LC412(%rip), %rax
	ret
.L59:
	leaq	.LC411(%rip), %rax
	ret
.L60:
	leaq	.LC410(%rip), %rax
	ret
.L61:
	leaq	.LC409(%rip), %rax
	ret
.L62:
	leaq	.LC408(%rip), %rax
	ret
.L63:
	leaq	.LC407(%rip), %rax
	ret
.L64:
	leaq	.LC406(%rip), %rax
	ret
.L65:
	leaq	.LC405(%rip), %rax
	ret
.L66:
	leaq	.LC404(%rip), %rax
	ret
.L67:
	leaq	.LC403(%rip), %rax
	ret
.L68:
	leaq	.LC402(%rip), %rax
	ret
.L69:
	leaq	.LC401(%rip), %rax
	ret
.L70:
	leaq	.LC400(%rip), %rax
	ret
.L71:
	leaq	.LC399(%rip), %rax
	ret
.L72:
	leaq	.LC462(%rip), %rax
	ret
.L73:
	leaq	.LC398(%rip), %rax
	ret
.L74:
	leaq	.LC397(%rip), %rax
	ret
.L75:
	leaq	.LC396(%rip), %rax
	ret
.L76:
	leaq	.LC377(%rip), %rax
	ret
.L77:
	leaq	.LC376(%rip), %rax
	ret
.L78:
	leaq	.LC375(%rip), %rax
	ret
.L79:
	leaq	.LC374(%rip), %rax
	ret
.L80:
	leaq	.LC373(%rip), %rax
	ret
.L81:
	leaq	.LC372(%rip), %rax
	ret
.L82:
	leaq	.LC371(%rip), %rax
	ret
.L83:
	leaq	.LC370(%rip), %rax
	ret
.L84:
	leaq	.LC369(%rip), %rax
	ret
.L85:
	leaq	.LC368(%rip), %rax
	ret
.L86:
	leaq	.LC367(%rip), %rax
	ret
.L87:
	leaq	.LC366(%rip), %rax
	ret
.L88:
	leaq	.LC380(%rip), %rax
	ret
.L89:
	leaq	.LC379(%rip), %rax
	ret
.L90:
	leaq	.LC378(%rip), %rax
	ret
.L91:
	leaq	.LC365(%rip), %rax
	ret
.L92:
	leaq	.LC364(%rip), %rax
	ret
.L93:
	leaq	.LC363(%rip), %rax
	ret
.L94:
	leaq	.LC362(%rip), %rax
	ret
.L95:
	leaq	.LC361(%rip), %rax
	ret
.L96:
	leaq	.LC360(%rip), %rax
	ret
.L97:
	leaq	.LC359(%rip), %rax
	ret
.L98:
	leaq	.LC358(%rip), %rax
	ret
.L99:
	leaq	.LC357(%rip), %rax
	ret
.L100:
	leaq	.LC356(%rip), %rax
	ret
.L101:
	leaq	.LC355(%rip), %rax
	ret
.L102:
	leaq	.LC354(%rip), %rax
	ret
.L103:
	leaq	.LC353(%rip), %rax
	ret
.L104:
	leaq	.LC352(%rip), %rax
	ret
.L105:
	leaq	.LC351(%rip), %rax
	ret
.L106:
	leaq	.LC350(%rip), %rax
	ret
.L107:
	leaq	.LC349(%rip), %rax
	ret
.L108:
	leaq	.LC348(%rip), %rax
	ret
.L109:
	leaq	.LC347(%rip), %rax
	ret
.L110:
	leaq	.LC346(%rip), %rax
	ret
.L111:
	leaq	.LC345(%rip), %rax
	ret
.L112:
	leaq	.LC344(%rip), %rax
	ret
.L113:
	leaq	.LC343(%rip), %rax
	ret
.L114:
	leaq	.LC342(%rip), %rax
	ret
.L115:
	leaq	.LC341(%rip), %rax
	ret
.L116:
	leaq	.LC340(%rip), %rax
	ret
.L117:
	leaq	.LC338(%rip), %rax
	ret
.L118:
	leaq	.LC339(%rip), %rax
	ret
.L119:
	leaq	.LC337(%rip), %rax
	ret
.L120:
	leaq	.LC336(%rip), %rax
	ret
.L121:
	leaq	.LC335(%rip), %rax
	ret
.L122:
	leaq	.LC334(%rip), %rax
	ret
.L123:
	leaq	.LC333(%rip), %rax
	ret
.L124:
	leaq	.LC332(%rip), %rax
	ret
.L125:
	leaq	.LC331(%rip), %rax
	ret
.L126:
	leaq	.LC330(%rip), %rax
	ret
.L127:
	leaq	.LC329(%rip), %rax
	ret
.L128:
	leaq	.LC328(%rip), %rax
	ret
.L129:
	leaq	.LC327(%rip), %rax
	ret
.L130:
	leaq	.LC326(%rip), %rax
	ret
.L131:
	leaq	.LC325(%rip), %rax
	ret
.L132:
	leaq	.LC324(%rip), %rax
	ret
.L133:
	leaq	.LC323(%rip), %rax
	ret
.L134:
	leaq	.LC322(%rip), %rax
	ret
.L135:
	leaq	.LC321(%rip), %rax
	ret
.L136:
	leaq	.LC320(%rip), %rax
	ret
.L137:
	leaq	.LC319(%rip), %rax
	ret
.L138:
	leaq	.LC318(%rip), %rax
	ret
.L139:
	leaq	.LC317(%rip), %rax
	ret
.L140:
	leaq	.LC316(%rip), %rax
	ret
.L141:
	leaq	.LC315(%rip), %rax
	ret
.L142:
	leaq	.LC314(%rip), %rax
	ret
.L143:
	leaq	.LC313(%rip), %rax
	ret
.L144:
	leaq	.LC312(%rip), %rax
	ret
.L145:
	leaq	.LC311(%rip), %rax
	ret
.L146:
	leaq	.LC310(%rip), %rax
	ret
.L147:
	leaq	.LC309(%rip), %rax
	ret
.L148:
	leaq	.LC308(%rip), %rax
	ret
.L149:
	leaq	.LC307(%rip), %rax
	ret
.L150:
	leaq	.LC306(%rip), %rax
	ret
.L151:
	leaq	.LC305(%rip), %rax
	ret
.L152:
	leaq	.LC304(%rip), %rax
	ret
.L153:
	leaq	.LC303(%rip), %rax
	ret
.L154:
	leaq	.LC302(%rip), %rax
	ret
.L155:
	leaq	.LC301(%rip), %rax
	ret
.L156:
	leaq	.LC300(%rip), %rax
	ret
.L157:
	leaq	.LC299(%rip), %rax
	ret
.L158:
	leaq	.LC298(%rip), %rax
	ret
.L159:
	leaq	.LC297(%rip), %rax
	ret
.L160:
	leaq	.LC296(%rip), %rax
	ret
.L161:
	leaq	.LC295(%rip), %rax
	ret
.L162:
	leaq	.LC294(%rip), %rax
	ret
.L163:
	leaq	.LC293(%rip), %rax
	ret
.L164:
	leaq	.LC292(%rip), %rax
	ret
.L165:
	leaq	.LC291(%rip), %rax
	ret
.L166:
	leaq	.LC290(%rip), %rax
	ret
.L167:
	leaq	.LC289(%rip), %rax
	ret
.L168:
	leaq	.LC288(%rip), %rax
	ret
.L169:
	leaq	.LC287(%rip), %rax
	ret
.L170:
	leaq	.LC286(%rip), %rax
	ret
.L171:
	leaq	.LC285(%rip), %rax
	ret
.L172:
	leaq	.LC284(%rip), %rax
	ret
.L173:
	leaq	.LC283(%rip), %rax
	ret
.L174:
	leaq	.LC282(%rip), %rax
	ret
.L175:
	leaq	.LC281(%rip), %rax
	ret
.L176:
	leaq	.LC280(%rip), %rax
	ret
.L177:
	leaq	.LC279(%rip), %rax
	ret
.L178:
	leaq	.LC278(%rip), %rax
	ret
.L179:
	leaq	.LC277(%rip), %rax
	ret
.L180:
	leaq	.LC276(%rip), %rax
	ret
.L181:
	leaq	.LC275(%rip), %rax
	ret
.L182:
	leaq	.LC274(%rip), %rax
	ret
.L183:
	leaq	.LC273(%rip), %rax
	ret
.L184:
	leaq	.LC272(%rip), %rax
	ret
.L185:
	leaq	.LC271(%rip), %rax
	ret
.L186:
	leaq	.LC270(%rip), %rax
	ret
.L187:
	leaq	.LC269(%rip), %rax
	ret
.L188:
	leaq	.LC268(%rip), %rax
	ret
.L189:
	leaq	.LC267(%rip), %rax
	ret
.L190:
	leaq	.LC266(%rip), %rax
	ret
.L191:
	leaq	.LC265(%rip), %rax
	ret
.L192:
	leaq	.LC264(%rip), %rax
	ret
.L193:
	leaq	.LC263(%rip), %rax
	ret
.L194:
	leaq	.LC262(%rip), %rax
	ret
.L195:
	leaq	.LC261(%rip), %rax
	ret
.L196:
	leaq	.LC260(%rip), %rax
	ret
.L197:
	leaq	.LC259(%rip), %rax
	ret
.L198:
	leaq	.LC258(%rip), %rax
	ret
.L199:
	leaq	.LC257(%rip), %rax
	ret
.L200:
	leaq	.LC256(%rip), %rax
	ret
.L201:
	leaq	.LC255(%rip), %rax
	ret
.L202:
	leaq	.LC254(%rip), %rax
	ret
.L203:
	leaq	.LC253(%rip), %rax
	ret
.L204:
	leaq	.LC252(%rip), %rax
	ret
.L205:
	leaq	.LC251(%rip), %rax
	ret
.L206:
	leaq	.LC250(%rip), %rax
	ret
.L207:
	leaq	.LC249(%rip), %rax
	ret
.L208:
	leaq	.LC248(%rip), %rax
	ret
.L209:
	leaq	.LC247(%rip), %rax
	ret
.L210:
	leaq	.LC246(%rip), %rax
	ret
.L211:
	leaq	.LC245(%rip), %rax
	ret
.L212:
	leaq	.LC244(%rip), %rax
	ret
.L213:
	leaq	.LC243(%rip), %rax
	ret
.L214:
	leaq	.LC242(%rip), %rax
	ret
.L215:
	leaq	.LC241(%rip), %rax
	ret
.L216:
	leaq	.LC240(%rip), %rax
	ret
.L217:
	leaq	.LC239(%rip), %rax
	ret
.L218:
	leaq	.LC238(%rip), %rax
	ret
.L219:
	leaq	.LC237(%rip), %rax
	ret
.L220:
	leaq	.LC236(%rip), %rax
	ret
.L221:
	leaq	.LC235(%rip), %rax
	ret
.L222:
	leaq	.LC234(%rip), %rax
	ret
.L223:
	leaq	.LC233(%rip), %rax
	ret
.L224:
	leaq	.LC232(%rip), %rax
	ret
.L225:
	leaq	.LC231(%rip), %rax
	ret
.L226:
	leaq	.LC230(%rip), %rax
	ret
.L227:
	leaq	.LC229(%rip), %rax
	ret
.L228:
	leaq	.LC228(%rip), %rax
	ret
.L229:
	leaq	.LC227(%rip), %rax
	ret
.L230:
	leaq	.LC392(%rip), %rax
	ret
.L231:
	leaq	.LC386(%rip), %rax
	ret
.L232:
	leaq	.LC226(%rip), %rax
	ret
.L233:
	leaq	.LC391(%rip), %rax
	ret
.L234:
	leaq	.LC385(%rip), %rax
	ret
.L235:
	leaq	.LC225(%rip), %rax
	ret
.L236:
	leaq	.LC390(%rip), %rax
	ret
.L237:
	leaq	.LC384(%rip), %rax
	ret
.L238:
	leaq	.LC224(%rip), %rax
	ret
.L239:
	leaq	.LC389(%rip), %rax
	ret
.L240:
	leaq	.LC383(%rip), %rax
	ret
.L241:
	leaq	.LC223(%rip), %rax
	ret
.L242:
	leaq	.LC388(%rip), %rax
	ret
.L243:
	leaq	.LC382(%rip), %rax
	ret
.L245:
	leaq	.LC387(%rip), %rax
	ret
.L246:
	leaq	.LC381(%rip), %rax
	ret
.L248:
	leaq	.LC393(%rip), %rax
	ret
.L249:
	leaq	.LC395(%rip), %rax
	ret
.L250:
	leaq	.LC394(%rip), %rax
	ret
.L251:
	leaq	.LC480(%rip), %rax
	ret
.L252:
	leaq	.LC479(%rip), %rax
	ret
.L253:
	leaq	.LC478(%rip), %rax
	ret
.L254:
	leaq	.LC477(%rip), %rax
	ret
.L255:
	leaq	.LC476(%rip), %rax
	ret
.L256:
	leaq	.LC475(%rip), %rax
	ret
.L257:
	leaq	.LC474(%rip), %rax
	ret
.L258:
	leaq	.LC473(%rip), %rax
	ret
.L259:
	leaq	.LC472(%rip), %rax
	ret
.L260:
	leaq	.LC471(%rip), %rax
	ret
.L261:
	leaq	.LC470(%rip), %rax
	ret
.L262:
	leaq	.LC469(%rip), %rax
	ret
.L263:
	leaq	.LC468(%rip), %rax
	ret
.L264:
	leaq	.LC467(%rip), %rax
	ret
.L265:
	leaq	.LC466(%rip), %rax
	ret
.L266:
	leaq	.LC465(%rip), %rax
	ret
.L267:
	leaq	.LC464(%rip), %rax
	ret
.L268:
	leaq	.LC463(%rip), %rax
	ret
.L496:
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE19615:
	.size	_ZN2v88internal4wasm12_GLOBAL__N_113RawOpcodeNameENS1_10WasmOpcodeE, .-_ZN2v88internal4wasm12_GLOBAL__N_113RawOpcodeNameENS1_10WasmOpcodeE
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE12onFirstErrorEv,"axG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE12onFirstErrorEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE12onFirstErrorEv
	.type	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE12onFirstErrorEv, @function
_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE12onFirstErrorEv:
.LFB24558:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	%rax, 24(%rdi)
	ret
	.cfi_endproc
.LFE24558:
	.size	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE12onFirstErrorEv, .-_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE12onFirstErrorEv
	.section	.text._ZN2v88internal4wasm7DecoderD2Ev,"axG",@progbits,_ZN2v88internal4wasm7DecoderD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm7DecoderD2Ev
	.type	_ZN2v88internal4wasm7DecoderD2Ev, @function
_ZN2v88internal4wasm7DecoderD2Ev:
.LFB19593:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %r8
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %rax
	addq	$64, %rdi
	movq	%rax, -64(%rdi)
	cmpq	%rdi, %r8
	je	.L498
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L498:
	ret
	.cfi_endproc
.LFE19593:
	.size	_ZN2v88internal4wasm7DecoderD2Ev, .-_ZN2v88internal4wasm7DecoderD2Ev
	.weak	_ZN2v88internal4wasm7DecoderD1Ev
	.set	_ZN2v88internal4wasm7DecoderD1Ev,_ZN2v88internal4wasm7DecoderD2Ev
	.section	.text._ZN2v88internal4wasm7DecoderD0Ev,"axG",@progbits,_ZN2v88internal4wasm7DecoderD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm7DecoderD0Ev
	.type	_ZN2v88internal4wasm7DecoderD0Ev, @function
_ZN2v88internal4wasm7DecoderD0Ev:
.LFB19595:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	48(%rdi), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L501
	call	_ZdlPv@PLT
.L501:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$80, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE19595:
	.size	_ZN2v88internal4wasm7DecoderD0Ev, .-_ZN2v88internal4wasm7DecoderD0Ev
	.section	.text._ZN2v88internal4wasm16BytecodeIteratorD2Ev,"axG",@progbits,_ZN2v88internal4wasm16BytecodeIteratorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm16BytecodeIteratorD2Ev
	.type	_ZN2v88internal4wasm16BytecodeIteratorD2Ev, @function
_ZN2v88internal4wasm16BytecodeIteratorD2Ev:
.LFB24534:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %r8
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %rax
	addq	$64, %rdi
	movq	%rax, -64(%rdi)
	cmpq	%rdi, %r8
	je	.L503
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L503:
	ret
	.cfi_endproc
.LFE24534:
	.size	_ZN2v88internal4wasm16BytecodeIteratorD2Ev, .-_ZN2v88internal4wasm16BytecodeIteratorD2Ev
	.weak	_ZN2v88internal4wasm16BytecodeIteratorD1Ev
	.set	_ZN2v88internal4wasm16BytecodeIteratorD1Ev,_ZN2v88internal4wasm16BytecodeIteratorD2Ev
	.section	.text._ZN2v88internal4wasm16BytecodeIteratorD0Ev,"axG",@progbits,_ZN2v88internal4wasm16BytecodeIteratorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm16BytecodeIteratorD0Ev
	.type	_ZN2v88internal4wasm16BytecodeIteratorD0Ev, @function
_ZN2v88internal4wasm16BytecodeIteratorD0Ev:
.LFB24536:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	48(%rdi), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L506
	call	_ZdlPv@PLT
.L506:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$80, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE24536:
	.size	_ZN2v88internal4wasm16BytecodeIteratorD0Ev, .-_ZN2v88internal4wasm16BytecodeIteratorD0Ev
	.section	.text._ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE0EED2Ev,"axG",@progbits,_ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE0EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE0EED2Ev
	.type	_ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE0EED2Ev, @function
_ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE0EED2Ev:
.LFB24514:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %r8
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %rax
	addq	$64, %rdi
	movq	%rax, -64(%rdi)
	cmpq	%rdi, %r8
	je	.L508
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L508:
	ret
	.cfi_endproc
.LFE24514:
	.size	_ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE0EED2Ev, .-_ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE0EED2Ev
	.weak	_ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE0EED1Ev
	.set	_ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE0EED1Ev,_ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE0EED2Ev
	.section	.text._ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE0EED0Ev,"axG",@progbits,_ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE0EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE0EED0Ev
	.type	_ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE0EED0Ev, @function
_ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE0EED0Ev:
.LFB24516:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	48(%rdi), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L511
	call	_ZdlPv@PLT
.L511:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$128, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE24516:
	.size	_ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE0EED0Ev, .-_ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE0EED0Ev
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEED2Ev,"axG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEED2Ev
	.type	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEED2Ev, @function
_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEED2Ev:
.LFB24518:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %r8
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %rax
	addq	$64, %rdi
	movq	%rax, -64(%rdi)
	cmpq	%rdi, %r8
	je	.L513
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L513:
	ret
	.cfi_endproc
.LFE24518:
	.size	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEED2Ev, .-_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEED2Ev
	.weak	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEED1Ev
	.set	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEED1Ev,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEED2Ev
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEED0Ev,"axG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEED0Ev
	.type	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEED0Ev, @function
_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEED0Ev:
.LFB24520:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	48(%rdi), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L516
	call	_ZdlPv@PLT
.L516:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$240, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE24520:
	.size	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEED0Ev, .-_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEED0Ev
	.section	.text._ZN2v88internal12StdoutStreamD1Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD1Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StdoutStreamD1Ev
	.type	_ZN2v88internal12StdoutStreamD1Ev, @function
_ZN2v88internal12StdoutStreamD1Ev:
.LFB24523:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE24523:
	.size	_ZN2v88internal12StdoutStreamD1Ev, .-_ZN2v88internal12StdoutStreamD1Ev
	.section	.text._ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0, @function
_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0:
.LFB24590:
	.cfi_startproc
	leal	-7(%rdi), %eax
	cmpb	$2, %al
	setbe	%al
	cmpb	$6, %sil
	sete	%dl
	andb	%dl, %al
	je	.L522
	ret
	.p2align 4,,10
	.p2align 3
.L522:
	subl	$7, %esi
	andl	$253, %esi
	sete	%al
	cmpb	$8, %dil
	sete	%dl
	andl	%edx, %eax
	ret
	.cfi_endproc
.LFE24590:
	.size	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0, .-_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0.str1.1,"aMS",@progbits,1
.LC482:
	.string	"<end>"
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0, @function
_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0:
.LFB24744:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movzbl	(%rsi), %r12d
	movl	%r12d, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	%r12d, %edi
	testb	%al, %al
	je	.L527
	leaq	1(%rbx), %rax
	cmpq	%rax, 0(%r13)
	ja	.L528
	addq	$8, %rsp
	leaq	.LC482(%rip), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L528:
	.cfi_restore_state
	movzbl	1(%rbx), %r12d
	sall	$8, %edi
	orl	%r12d, %edi
.L527:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	.cfi_endproc
.LFE24744:
	.size	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0, .-_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	.section	.text._ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE0ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi1EEET_PKhPjPKcS7_.isra.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE0ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi1EEET_PKhPjPKcS7_.isra.0, @function
_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE0ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi1EEET_PKhPjPKcS7_.isra.0:
.LFB24890:
	.cfi_startproc
	movzbl	(%rdi), %ecx
	movl	%ecx, %eax
	sall	$7, %eax
	andl	$16256, %eax
	orl	%edx, %eax
	testb	%cl, %cl
	js	.L534
	movl	$2, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L534:
	movzbl	1(%rdi), %ecx
	movl	%ecx, %edx
	sall	$14, %edx
	andl	$2080768, %edx
	orl	%edx, %eax
	testb	%cl, %cl
	js	.L535
	movl	$3, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L535:
	movzbl	2(%rdi), %ecx
	movl	%ecx, %edx
	sall	$21, %edx
	andl	$266338304, %edx
	orl	%edx, %eax
	testb	%cl, %cl
	js	.L536
	movl	$4, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L536:
	movzbl	3(%rdi), %edx
	movl	$5, (%rsi)
	sall	$28, %edx
	orl	%edx, %eax
	ret
	.cfi_endproc
.LFE24890:
	.size	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE0ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi1EEET_PKhPjPKcS7_.isra.0, .-_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE0ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi1EEET_PKhPjPKcS7_.isra.0
	.section	.text._ZN2v88internal12StdoutStreamD0Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD0Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.type	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev, @function
_ZTv0_n24_N2v88internal12StdoutStreamD0Ev:
.LFB24941:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %rax
	addq	-24(%rax), %rdi
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	movq	%rax, 80(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE24941:
	.size	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev, .-_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StdoutStreamD0Ev
	.type	_ZN2v88internal12StdoutStreamD0Ev, @function
_ZN2v88internal12StdoutStreamD0Ev:
.LFB24524:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE24524:
	.size	_ZN2v88internal12StdoutStreamD0Ev, .-_ZN2v88internal12StdoutStreamD0Ev
	.section	.text._ZN2v88internal12StdoutStreamD1Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD1Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.type	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev, @function
_ZTv0_n24_N2v88internal12StdoutStreamD1Ev:
.LFB24942:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	-24(%rax), %rbx
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	addq	%rdi, %rbx
	movq	%rax, 80(%rbx)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	64(%rbx), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE24942:
	.size	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev, .-_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.section	.rodata._ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag.str1.1,"aMS",@progbits,1
.LC483:
	.string	"0 < len"
.LC484:
	.string	"Check failed: %s."
	.section	.rodata._ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag.str1.8,"aMS",@progbits,1
	.align 8
.LC485:
	.string	"basic_string::_M_construct null not valid"
	.section	.text._ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag,"axG",@progbits,_ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag
	.type	_ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag, @function
_ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag:
.LFB6932:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$392, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 56(%rdi)
	je	.L570
.L543:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L571
	addq	$392, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L570:
	.cfi_restore_state
	movq	$256, -328(%rbp)
	movq	%rdi, %r12
	movl	%esi, %ebx
	leaq	-320(%rbp), %rdi
	movl	$256, %esi
	movq	%rdi, -336(%rbp)
	call	_ZN2v88internal9VSNPrintFENS0_6VectorIcEEPKcP13__va_list_tag@PLT
	testl	%eax, %eax
	jle	.L572
	movq	-336(%rbp), %r14
	leaq	-400(%rbp), %r13
	movslq	%eax, %r15
	leaq	-416(%rbp), %rdi
	movq	%r13, -416(%rbp)
	testq	%r14, %r14
	je	.L573
	movq	%r15, -424(%rbp)
	cmpl	$15, %eax
	jg	.L574
	movq	%r13, %rdi
	cmpl	$1, %eax
	jne	.L549
	movzbl	(%r14), %eax
	movq	%r13, %rdx
	movb	%al, -400(%rbp)
	movl	$1, %eax
.L550:
	movq	%rax, -408(%rbp)
	leaq	-360(%rbp), %r14
	movb	$0, (%rdx,%rax)
	movq	-416(%rbp), %rax
	movl	%ebx, -384(%rbp)
	movq	%r14, -376(%rbp)
	cmpq	%r13, %rax
	je	.L575
	movq	-400(%rbp), %rcx
	movq	-408(%rbp), %rdx
	movq	%rax, -376(%rbp)
	movq	%r13, -416(%rbp)
	movq	48(%r12), %rdi
	movq	%rcx, -360(%rbp)
	movq	%rdx, -368(%rbp)
	movq	$0, -408(%rbp)
	movb	$0, -400(%rbp)
	movl	%ebx, 40(%r12)
	cmpq	%r14, %rax
	je	.L552
	leaq	64(%r12), %rsi
	cmpq	%rsi, %rdi
	je	.L576
	movq	%rdx, %xmm0
	movq	%rcx, %xmm1
	movq	64(%r12), %rsi
	movq	%rax, 48(%r12)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 56(%r12)
	testq	%rdi, %rdi
	je	.L558
	movq	%rdi, -376(%rbp)
	movq	%rsi, -360(%rbp)
.L556:
	movq	$0, -368(%rbp)
	movb	$0, (%rdi)
	movq	-376(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L559
	call	_ZdlPv@PLT
.L559:
	movq	-416(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L560
	call	_ZdlPv@PLT
.L560:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	jmp	.L543
	.p2align 4,,10
	.p2align 3
.L574:
	leaq	-424(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -416(%rbp)
	movq	%rax, %rdi
	movq	-424(%rbp), %rax
	movq	%rax, -400(%rbp)
.L549:
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-424(%rbp), %rax
	movq	-416(%rbp), %rdx
	jmp	.L550
	.p2align 4,,10
	.p2align 3
.L575:
	movq	-408(%rbp), %rdx
	movq	48(%r12), %rdi
	movl	%ebx, 40(%r12)
	movdqa	-400(%rbp), %xmm2
	movq	$0, -408(%rbp)
	movq	%rdx, -368(%rbp)
	movb	$0, -400(%rbp)
	movups	%xmm2, -360(%rbp)
.L552:
	testq	%rdx, %rdx
	je	.L554
	cmpq	$1, %rdx
	je	.L577
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-368(%rbp), %rdx
	movq	48(%r12), %rdi
.L554:
	movq	%rdx, 56(%r12)
	movb	$0, (%rdi,%rdx)
	movq	-376(%rbp), %rdi
	jmp	.L556
	.p2align 4,,10
	.p2align 3
.L576:
	movq	%rdx, %xmm0
	movq	%rcx, %xmm3
	movq	%rax, 48(%r12)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 56(%r12)
.L558:
	movq	%r14, -376(%rbp)
	leaq	-360(%rbp), %r14
	movq	%r14, %rdi
	jmp	.L556
	.p2align 4,,10
	.p2align 3
.L577:
	movzbl	-360(%rbp), %eax
	movb	%al, (%rdi)
	movq	-368(%rbp), %rdx
	movq	48(%r12), %rdi
	jmp	.L554
	.p2align 4,,10
	.p2align 3
.L572:
	leaq	.LC483(%rip), %rsi
	leaq	.LC484(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L571:
	call	__stack_chk_fail@PLT
.L573:
	leaq	.LC485(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.cfi_endproc
.LFE6932:
	.size	_ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag, .-_ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag
	.section	.text._ZN2v88internal4wasm7Decoder6errorfEPKhPKcz,"axG",@progbits,_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	.type	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz, @function
_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz:
.LFB6911:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$208, %rsp
	movq	%rcx, -152(%rbp)
	movq	%r8, -144(%rbp)
	movq	%r9, -136(%rbp)
	testb	%al, %al
	je	.L579
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm3, -80(%rbp)
	movaps	%xmm4, -64(%rbp)
	movaps	%xmm5, -48(%rbp)
	movaps	%xmm6, -32(%rbp)
	movaps	%xmm7, -16(%rbp)
.L579:
	movq	%fs:40, %rax
	movq	%rax, -184(%rbp)
	xorl	%eax, %eax
	leaq	16(%rbp), %rax
	subq	8(%rdi), %rsi
	leaq	-208(%rbp), %rcx
	movq	%rax, -200(%rbp)
	addl	32(%rdi), %esi
	leaq	-176(%rbp), %rax
	movq	%rax, -192(%rbp)
	movl	$24, -208(%rbp)
	movl	$48, -204(%rbp)
	call	_ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag
	movq	-184(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L582
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L582:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6911:
	.size	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz, .-_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	.section	.text._ZN2v88internal4wasm7Decoder6errorfEjPKcz,"axG",@progbits,_ZN2v88internal4wasm7Decoder6errorfEjPKcz,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm7Decoder6errorfEjPKcz
	.type	_ZN2v88internal4wasm7Decoder6errorfEjPKcz, @function
_ZN2v88internal4wasm7Decoder6errorfEjPKcz:
.LFB6910:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$208, %rsp
	movq	%rcx, -152(%rbp)
	movq	%r8, -144(%rbp)
	movq	%r9, -136(%rbp)
	testb	%al, %al
	je	.L584
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm3, -80(%rbp)
	movaps	%xmm4, -64(%rbp)
	movaps	%xmm5, -48(%rbp)
	movaps	%xmm6, -32(%rbp)
	movaps	%xmm7, -16(%rbp)
.L584:
	movq	%fs:40, %rax
	movq	%rax, -184(%rbp)
	xorl	%eax, %eax
	leaq	16(%rbp), %rax
	leaq	-208(%rbp), %rcx
	movl	$24, -208(%rbp)
	movq	%rax, -200(%rbp)
	leaq	-176(%rbp), %rax
	movq	%rax, -192(%rbp)
	movl	$48, -204(%rbp)
	call	_ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag
	movq	-184(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L587
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L587:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6910:
	.size	_ZN2v88internal4wasm7Decoder6errorfEjPKcz, .-_ZN2v88internal4wasm7Decoder6errorfEjPKcz
	.section	.rodata._ZN2v88internal4wasm7Decoder5errorEPKhPKc.str1.1,"aMS",@progbits,1
.LC486:
	.string	"%s"
	.section	.text._ZN2v88internal4wasm7Decoder5errorEPKhPKc,"axG",@progbits,_ZN2v88internal4wasm7Decoder5errorEPKhPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	.type	_ZN2v88internal4wasm7Decoder5errorEPKhPKc, @function
_ZN2v88internal4wasm7Decoder5errorEPKhPKc:
.LFB6908:
	.cfi_startproc
	endbr64
	movq	%rdx, %rcx
	subq	8(%rdi), %rsi
	leaq	.LC486(%rip), %rdx
	xorl	%eax, %eax
	addl	32(%rdi), %esi
	jmp	_ZN2v88internal4wasm7Decoder6errorfEjPKcz
	.cfi_endproc
.LFE6908:
	.size	_ZN2v88internal4wasm7Decoder5errorEPKhPKc, .-_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	.section	.rodata._ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_.constprop.0.str1.1,"aMS",@progbits,1
.LC487:
	.string	"expected %s"
.LC488:
	.string	"extra bits in varint"
	.section	.text._ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_.constprop.0, @function
_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_.constprop.0:
.LFB24945:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	24(%rdi), %r8
	cmpq	%rsi, %r8
	ja	.L607
	movl	$0, (%rdx)
	xorl	%eax, %eax
	leaq	.LC487(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	xorl	%eax, %eax
.L589:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L607:
	.cfi_restore_state
	movzbl	(%rsi), %r9d
	movl	%r9d, %eax
	andl	$127, %eax
	testb	%r9b, %r9b
	js	.L608
	movl	$1, (%rdx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L608:
	.cfi_restore_state
	leaq	1(%rsi), %r9
	cmpq	%r9, %r8
	jbe	.L592
	movzbl	1(%rsi), %r10d
	movl	%r10d, %r9d
	sall	$7, %r9d
	andl	$16256, %r9d
	orl	%r9d, %eax
	testb	%r10b, %r10b
	js	.L609
	movl	$2, (%rdx)
	jmp	.L589
	.p2align 4,,10
	.p2align 3
.L592:
	movl	$1, (%rdx)
.L606:
	xorl	%eax, %eax
	leaq	.LC487(%rip), %rdx
	movq	%r9, %rsi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L609:
	.cfi_restore_state
	leaq	2(%rsi), %r9
	cmpq	%r9, %r8
	jbe	.L594
	movzbl	2(%rsi), %r10d
	movl	%r10d, %r9d
	sall	$14, %r9d
	andl	$2080768, %r9d
	orl	%r9d, %eax
	testb	%r10b, %r10b
	js	.L610
	movl	$3, (%rdx)
	jmp	.L589
	.p2align 4,,10
	.p2align 3
.L594:
	movl	$2, (%rdx)
	jmp	.L606
	.p2align 4,,10
	.p2align 3
.L610:
	leaq	3(%rsi), %r9
	cmpq	%r9, %r8
	ja	.L611
	movl	$3, (%rdx)
	jmp	.L606
.L611:
	movzbl	3(%rsi), %r10d
	movl	%r10d, %r9d
	sall	$21, %r9d
	andl	$266338304, %r9d
	orl	%r9d, %eax
	testb	%r10b, %r10b
	js	.L612
	movl	$4, (%rdx)
	jmp	.L589
.L612:
	leaq	4(%rsi), %r12
	cmpq	%r12, %r8
	jbe	.L598
	movzbl	4(%rsi), %ebx
	movl	$5, (%rdx)
	testb	%bl, %bl
	js	.L599
	movl	%ebx, %edx
	sall	$28, %edx
	orl	%edx, %eax
.L600:
	andl	$240, %ebx
	je	.L589
	leaq	.LC488(%rip), %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	xorl	%eax, %eax
	jmp	.L589
.L598:
	movl	$4, (%rdx)
	xorl	%ebx, %ebx
.L599:
	xorl	%eax, %eax
	leaq	.LC487(%rip), %rdx
	movq	%r12, %rsi
	movq	%rdi, -24(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-24(%rbp), %rdi
	xorl	%eax, %eax
	jmp	.L600
	.cfi_endproc
.LFE24945:
	.size	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_.constprop.0, .-_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_.constprop.0
	.section	.text._ZN2v88internal4wasm7Decoder5errorEPKc,"axG",@progbits,_ZN2v88internal4wasm7Decoder5errorEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm7Decoder5errorEPKc
	.type	_ZN2v88internal4wasm7Decoder5errorEPKc, @function
_ZN2v88internal4wasm7Decoder5errorEPKc:
.LFB6907:
	.cfi_startproc
	endbr64
	movq	%rsi, %rcx
	leaq	.LC486(%rip), %rdx
	movq	16(%rdi), %rsi
	xorl	%eax, %eax
	subq	8(%rdi), %rsi
	addl	32(%rdi), %esi
	jmp	_ZN2v88internal4wasm7Decoder6errorfEjPKcz
	.cfi_endproc
.LFE6907:
	.size	_ZN2v88internal4wasm7Decoder5errorEPKc, .-_ZN2v88internal4wasm7Decoder5errorEPKc
	.section	.rodata._ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE.str1.1,"aMS",@progbits,1
.LC489:
	.string	"<unknown>"
.LC490:
	.string	"i32"
.LC491:
	.string	"f32"
.LC492:
	.string	"f64"
.LC493:
	.string	"anyref"
.LC494:
	.string	"funcref"
.LC495:
	.string	"nullref"
.LC496:
	.string	"exn"
.LC497:
	.string	"s128"
.LC498:
	.string	"<stmt>"
.LC499:
	.string	"<bot>"
.LC500:
	.string	"i64"
	.section	.text._ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE,"axG",@progbits,_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE,comdat
	.p2align 4
	.weak	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	.type	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE, @function
_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE:
.LFB6970:
	.cfi_startproc
	endbr64
	movzbl	%dil, %edx
	cmpb	$10, %dil
	ja	.L615
	leaq	.L617(%rip), %rcx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE,"aG",@progbits,_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE,comdat
	.align 4
	.align 4
.L617:
	.long	.L627-.L617
	.long	.L628-.L617
	.long	.L625-.L617
	.long	.L624-.L617
	.long	.L623-.L617
	.long	.L622-.L617
	.long	.L621-.L617
	.long	.L620-.L617
	.long	.L619-.L617
	.long	.L618-.L617
	.long	.L616-.L617
	.section	.text._ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE,"axG",@progbits,_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE,comdat
	.p2align 4,,10
	.p2align 3
.L625:
	leaq	.LC500(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L628:
	leaq	.LC490(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L624:
	leaq	.LC491(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L623:
	leaq	.LC492(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L627:
	leaq	.LC498(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L618:
	leaq	.LC496(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L616:
	leaq	.LC499(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L622:
	leaq	.LC497(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L621:
	leaq	.LC493(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L620:
	leaq	.LC494(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L619:
	leaq	.LC495(%rip), %rax
	ret
.L615:
	leaq	.LC489(%rip), %rax
	ret
	.cfi_endproc
.LFE6970:
	.size	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE, .-_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	.section	.rodata._ZN2v88internal4wasm11StackEffectEPKNS1_10WasmModuleEPNS0_9SignatureINS1_9ValueTypeEEEPKhSA_.str1.1,"aMS",@progbits,1
.LC501:
	.string	"unimplemented opcode: %x (%s)"
	.section	.text._ZN2v88internal4wasm11StackEffectEPKNS1_10WasmModuleEPNS0_9SignatureINS1_9ValueTypeEEEPKhSA_,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm11StackEffectEPKNS1_10WasmModuleEPNS0_9SignatureINS1_9ValueTypeEEEPKhSA_
	.type	_ZN2v88internal4wasm11StackEffectEPKNS1_10WasmModuleEPNS0_9SignatureINS1_9ValueTypeEEEPKhSA_, @function
_ZN2v88internal4wasm11StackEffectEPKNS1_10WasmModuleEPNS0_9SignatureINS1_9ValueTypeEEEPKhSA_:
.LFB19603:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-128(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 3, -56
	movzbl	(%rdx), %r15d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE0EEE(%rip), %rax
	movq	%rdi, -112(%rbp)
	movq	%rax, -192(%rbp)
	movl	%r15d, %edi
	movq	_ZN2v88internal4wasmL16kAllWasmFeaturesE(%rip), %rax
	movq	$0, -237(%rbp)
	movq	%rax, -104(%rbp)
	movl	8+_ZN2v88internal4wasmL16kAllWasmFeaturesE(%rip), %eax
	movl	$0, -229(%rbp)
	movl	%eax, -96(%rbp)
	movzbl	12+_ZN2v88internal4wasmL16kAllWasmFeaturesE(%rip), %eax
	movb	$0, -225(%rbp)
	movb	%al, -92(%rbp)
	leaq	-237(%rbp), %rax
	movq	%rdx, -184(%rbp)
	movq	%rdx, -176(%rbp)
	movq	%rcx, -168(%rbp)
	movl	$0, -160(%rbp)
	movl	$0, -152(%rbp)
	movq	%r14, -144(%rbp)
	movq	$0, -136(%rbp)
	movb	$0, -128(%rbp)
	movq	%rax, -88(%rbp)
	movq	%rsi, -80(%rbp)
	movq	$0, -72(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes9SignatureENS1_10WasmOpcodeE@PLT
	testq	%rax, %rax
	je	.L663
.L662:
	movl	8(%rax), %edx
	movl	(%rax), %eax
.L632:
	movq	%rax, %r12
	movq	-144(%rbp), %rdi
	movl	%edx, %eax
	salq	$32, %r12
	orq	%rax, %r12
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %rax
	movq	%rax, -192(%rbp)
	cmpq	%r14, %rdi
	je	.L654
	call	_ZdlPv@PLT
.L654:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L664
	addq	$216, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L663:
	.cfi_restore_state
	movl	%r15d, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes14AsmjsSignatureENS1_10WasmOpcodeE@PLT
	testq	%rax, %rax
	jne	.L662
	movl	%r15d, %r13d
	cmpb	$-2, %r15b
	ja	.L633
	leaq	.L635(%rip), %rdx
	movslq	(%rdx,%r15,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm11StackEffectEPKNS1_10WasmModuleEPNS0_9SignatureINS1_9ValueTypeEEEPKhSA_,"a",@progbits
	.align 4
	.align 4
.L635:
	.long	.L640-.L635
	.long	.L640-.L635
	.long	.L640-.L635
	.long	.L640-.L635
	.long	.L638-.L635
	.long	.L640-.L635
	.long	.L640-.L635
	.long	.L640-.L635
	.long	.L643-.L635
	.long	.L638-.L635
	.long	.L640-.L635
	.long	.L640-.L635
	.long	.L640-.L635
	.long	.L638-.L635
	.long	.L638-.L635
	.long	.L640-.L635
	.long	.L642-.L635
	.long	.L641-.L635
	.long	.L640-.L635
	.long	.L640-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L638-.L635
	.long	.L639-.L635
	.long	.L639-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L636-.L635
	.long	.L638-.L635
	.long	.L657-.L635
	.long	.L636-.L635
	.long	.L638-.L635
	.long	.L657-.L635
	.long	.L656-.L635
	.long	.L633-.L635
	.long	.L657-.L635
	.long	.L657-.L635
	.long	.L657-.L635
	.long	.L657-.L635
	.long	.L657-.L635
	.long	.L657-.L635
	.long	.L657-.L635
	.long	.L657-.L635
	.long	.L657-.L635
	.long	.L657-.L635
	.long	.L657-.L635
	.long	.L657-.L635
	.long	.L657-.L635
	.long	.L657-.L635
	.long	.L656-.L635
	.long	.L656-.L635
	.long	.L656-.L635
	.long	.L656-.L635
	.long	.L656-.L635
	.long	.L656-.L635
	.long	.L656-.L635
	.long	.L656-.L635
	.long	.L656-.L635
	.long	.L636-.L635
	.long	.L657-.L635
	.long	.L636-.L635
	.long	.L636-.L635
	.long	.L636-.L635
	.long	.L636-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L636-.L635
	.long	.L633-.L635
	.long	.L636-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L633-.L635
	.long	.L634-.L635
	.long	.L634-.L635
	.long	.L634-.L635
	.section	.text._ZN2v88internal4wasm11StackEffectEPKNS1_10WasmModuleEPNS0_9SignatureINS1_9ValueTypeEEEPKhSA_
.L656:
	xorl	%eax, %eax
	movl	$2, %edx
	jmp	.L632
.L638:
	xorl	%eax, %eax
	movl	$1, %edx
	jmp	.L632
.L657:
	movl	$1, %eax
	movl	$1, %edx
	jmp	.L632
.L634:
	sall	$8, %r15d
	movl	%r15d, %r13d
	movzbl	1(%r12), %r15d
	orl	%r15d, %r13d
	leal	-64771(%r13), %eax
	cmpl	$20, %eax
	ja	.L651
	leaq	.L653(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm11StackEffectEPKNS1_10WasmModuleEPNS0_9SignatureINS1_9ValueTypeEEEPKhSA_
	.align 4
	.align 4
.L653:
	.long	.L652-.L653
	.long	.L651-.L653
	.long	.L657-.L653
	.long	.L651-.L653
	.long	.L652-.L653
	.long	.L651-.L653
	.long	.L657-.L653
	.long	.L651-.L653
	.long	.L652-.L653
	.long	.L651-.L653
	.long	.L657-.L653
	.long	.L652-.L653
	.long	.L651-.L653
	.long	.L657-.L653
	.long	.L652-.L653
	.long	.L651-.L653
	.long	.L657-.L653
	.long	.L652-.L653
	.long	.L651-.L653
	.long	.L657-.L653
	.long	.L652-.L653
	.section	.text._ZN2v88internal4wasm11StackEffectEPKNS1_10WasmModuleEPNS0_9SignatureINS1_9ValueTypeEEEPKhSA_
.L640:
	xorl	%eax, %eax
	xorl	%edx, %edx
	jmp	.L632
.L643:
	movzbl	1(%r12), %eax
	movq	$0, -216(%rbp)
	movl	%eax, %edx
	andl	$127, %edx
	testb	%al, %al
	js	.L665
	movl	$1, -208(%rbp)
.L650:
	movq	-112(%rbp), %rax
	movl	%edx, -224(%rbp)
	movq	256(%rax), %rax
	leaq	(%rax,%rdx,8), %rax
	movq	%rax, -216(%rbp)
	movq	(%rax), %rax
	movl	8(%rax), %edx
	xorl	%eax, %eax
	jmp	.L632
.L642:
	movzbl	1(%r12), %eax
	movq	$0, -216(%rbp)
	movl	%eax, %edx
	andl	$127, %edx
	testb	%al, %al
	js	.L666
	movl	$1, -208(%rbp)
.L645:
	movq	-112(%rbp), %rax
	movl	%edx, -224(%rbp)
	salq	$5, %rdx
	addq	136(%rax), %rdx
	movq	(%rdx), %rax
	movq	%rax, -216(%rbp)
	movl	8(%rax), %edx
	movl	(%rax), %eax
	jmp	.L632
.L639:
	movl	$1, %eax
	movl	$3, %edx
	jmp	.L632
.L641:
	movl	$0, -244(%rbp)
	movzbl	1(%r12), %eax
	movl	%eax, %r8d
	andl	$127, %r8d
	testb	%al, %al
	js	.L667
	movl	$1, -244(%rbp)
	movl	$1, %eax
.L647:
	addq	%rax, %r12
	movabsq	$4294967296, %rax
	movzbl	1(%r12), %edx
	movq	%rax, -224(%rbp)
	testb	%dl, %dl
	js	.L668
.L648:
	movq	-112(%rbp), %rax
	movq	88(%rax), %rax
	movq	(%rax,%r8,8), %rax
	movq	8(%rax), %rdx
	movl	(%rax), %eax
	addl	$1, %edx
	jmp	.L632
.L636:
	movl	$1, %eax
	xorl	%edx, %edx
	jmp	.L632
	.p2align 4,,10
	.p2align 3
.L651:
	movl	%r13d, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes9SignatureENS1_10WasmOpcodeE@PLT
	testq	%rax, %rax
	jne	.L662
.L633:
	movl	%r13d, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movl	%r13d, %esi
	leaq	.LC501(%rip), %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L652:
	movl	$1, %eax
	movl	$2, %edx
	jmp	.L632
.L665:
	leaq	-208(%rbp), %rsi
	leaq	2(%r12), %rdi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE0ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi1EEET_PKhPjPKcS7_.isra.0
	movl	%eax, %edx
	jmp	.L650
.L666:
	leaq	-208(%rbp), %rsi
	leaq	2(%r12), %rdi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE0ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi1EEET_PKhPjPKcS7_.isra.0
	movl	%eax, %edx
	jmp	.L645
.L668:
	andl	$127, %edx
	leaq	-220(%rbp), %rsi
	leaq	2(%r12), %rdi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE0ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi1EEET_PKhPjPKcS7_.isra.0
	jmp	.L648
.L667:
	movl	%r8d, %edx
	leaq	-244(%rbp), %rsi
	leaq	2(%r12), %rdi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE0ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi1EEET_PKhPjPKcS7_.isra.0
	movl	%eax, %r8d
	movl	-244(%rbp), %eax
	jmp	.L647
.L664:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19603:
	.size	_ZN2v88internal4wasm11StackEffectEPKNS1_10WasmModuleEPNS0_9SignatureINS1_9ValueTypeEEEPKhSA_, .-_ZN2v88internal4wasm11StackEffectEPKNS1_10WasmModuleEPNS0_9SignatureINS1_9ValueTypeEEEPKhSA_
	.section	.text._ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE0EEC2ERKNS1_12WasmFeaturesEPS3_PKh,"axG",@progbits,_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE0EEC5ERKNS1_12WasmFeaturesEPS3_PKh,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE0EEC2ERKNS1_12WasmFeaturesEPS3_PKh
	.type	_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE0EEC2ERKNS1_12WasmFeaturesEPS3_PKh, @function
_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE0EEC2ERKNS1_12WasmFeaturesEPS3_PKh:
.LFB21574:
	.cfi_startproc
	endbr64
	movzbl	1(%rcx), %edx
	movl	$1, (%rdi)
	movb	$0, 4(%rdi)
	leal	-64(%rdx), %eax
	movl	$0, 8(%rdi)
	movq	$0, 16(%rdi)
	cmpb	$63, %al
	ja	.L670
	leaq	.L672(%rip), %rsi
	movzbl	%al, %eax
	movslq	(%rsi,%rax,4), %rax
	addq	%rsi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE0EEC2ERKNS1_12WasmFeaturesEPS3_PKh,"aG",@progbits,_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE0EEC5ERKNS1_12WasmFeaturesEPS3_PKh,comdat
	.align 4
	.align 4
.L672:
	.long	.L669-.L672
	.long	.L670-.L672
	.long	.L670-.L672
	.long	.L670-.L672
	.long	.L670-.L672
	.long	.L670-.L672
	.long	.L670-.L672
	.long	.L670-.L672
	.long	.L670-.L672
	.long	.L670-.L672
	.long	.L670-.L672
	.long	.L670-.L672
	.long	.L670-.L672
	.long	.L670-.L672
	.long	.L670-.L672
	.long	.L670-.L672
	.long	.L670-.L672
	.long	.L670-.L672
	.long	.L670-.L672
	.long	.L670-.L672
	.long	.L670-.L672
	.long	.L670-.L672
	.long	.L670-.L672
	.long	.L670-.L672
	.long	.L670-.L672
	.long	.L670-.L672
	.long	.L670-.L672
	.long	.L670-.L672
	.long	.L670-.L672
	.long	.L670-.L672
	.long	.L670-.L672
	.long	.L670-.L672
	.long	.L670-.L672
	.long	.L670-.L672
	.long	.L670-.L672
	.long	.L670-.L672
	.long	.L670-.L672
	.long	.L670-.L672
	.long	.L670-.L672
	.long	.L670-.L672
	.long	.L679-.L672
	.long	.L670-.L672
	.long	.L670-.L672
	.long	.L670-.L672
	.long	.L670-.L672
	.long	.L670-.L672
	.long	.L670-.L672
	.long	.L678-.L672
	.long	.L677-.L672
	.long	.L670-.L672
	.long	.L670-.L672
	.long	.L670-.L672
	.long	.L670-.L672
	.long	.L670-.L672
	.long	.L670-.L672
	.long	.L670-.L672
	.long	.L670-.L672
	.long	.L670-.L672
	.long	.L670-.L672
	.long	.L676-.L672
	.long	.L675-.L672
	.long	.L674-.L672
	.long	.L673-.L672
	.long	.L671-.L672
	.section	.text._ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE0EEC2ERKNS1_12WasmFeaturesEPS3_PKh,"axG",@progbits,_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE0EEC5ERKNS1_12WasmFeaturesEPS3_PKh,comdat
	.p2align 4,,10
	.p2align 3
.L670:
	movl	%edx, %esi
	movb	$10, 4(%rdi)
	andl	$127, %esi
	movl	%esi, %eax
	sall	$25, %eax
	sarl	$25, %eax
	testb	%dl, %dl
	js	.L686
.L685:
	movl	%eax, 8(%rdi)
.L669:
	ret
	.p2align 4,,10
	.p2align 3
.L671:
	movb	$1, 4(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L679:
	movb	$9, 4(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L678:
	movb	$6, 4(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L677:
	movb	$7, 4(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L676:
	movb	$5, 4(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L675:
	movb	$4, 4(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L674:
	movb	$3, 4(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L673:
	movb	$2, 4(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L686:
	movzbl	2(%rcx), %edx
	movzbl	%sil, %esi
	movl	%edx, %eax
	sall	$7, %eax
	andl	$16256, %eax
	orl	%esi, %eax
	testb	%dl, %dl
	js	.L687
	sall	$18, %eax
	movl	$2, (%rdi)
	sarl	$18, %eax
	jmp	.L685
	.p2align 4,,10
	.p2align 3
.L687:
	movzbl	3(%rcx), %esi
	movl	%esi, %edx
	sall	$14, %edx
	andl	$2080768, %edx
	orl	%edx, %eax
	testb	%sil, %sil
	js	.L688
	sall	$11, %eax
	movl	$3, (%rdi)
	sarl	$11, %eax
	jmp	.L685
	.p2align 4,,10
	.p2align 3
.L688:
	movzbl	4(%rcx), %esi
	movl	%esi, %edx
	sall	$21, %edx
	andl	$266338304, %edx
	orl	%edx, %eax
	testb	%sil, %sil
	js	.L689
	sall	$4, %eax
	movl	$4, (%rdi)
	sarl	$4, %eax
	jmp	.L685
.L689:
	movzbl	5(%rcx), %edx
	movl	$5, (%rdi)
	sall	$28, %edx
	orl	%edx, %eax
	jmp	.L685
	.cfi_endproc
.LFE21574:
	.size	_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE0EEC2ERKNS1_12WasmFeaturesEPS3_PKh, .-_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE0EEC2ERKNS1_12WasmFeaturesEPS3_PKh
	.weak	_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE0EEC1ERKNS1_12WasmFeaturesEPS3_PKh
	.set	_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE0EEC1ERKNS1_12WasmFeaturesEPS3_PKh,_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE0EEC2ERKNS1_12WasmFeaturesEPS3_PKh
	.section	.rodata._ZN2v88internal4wasm12OpcodeLengthEPKhS3_.str1.1,"aMS",@progbits,1
.LC502:
	.string	"invalid select type"
.LC503:
	.string	"invalid numeric opcode"
.LC504:
	.string	"invalid SIMD opcode"
.LC505:
	.string	"invalid Atomics opcode"
	.section	.text._ZN2v88internal4wasm12OpcodeLengthEPKhS3_,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm12OpcodeLengthEPKhS3_
	.type	_ZN2v88internal4wasm12OpcodeLengthEPKhS3_, @function
_ZN2v88internal4wasm12OpcodeLengthEPKhS3_:
.LFB19602:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %r13
	pushq	%r12
	leaq	-128(%rbp), %r8
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	leaq	-64(%rbp), %rbx
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	(%rdi), %eax
	movq	%r13, -128(%rbp)
	movq	%rdi, -120(%rbp)
	movq	%rdi, -112(%rbp)
	movq	%rsi, -104(%rbp)
	movl	$0, -96(%rbp)
	movl	$0, -88(%rbp)
	movq	%rbx, -80(%rbp)
	movq	$0, -72(%rbp)
	movb	$0, -64(%rbp)
	cmpb	$-46, %al
	ja	.L691
	cmpb	$1, %al
	jbe	.L774
	cmpb	$-46, %al
	ja	.L774
	leaq	.L695(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm12OpcodeLengthEPKhS3_,"a",@progbits
	.align 4
	.align 4
.L695:
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L711-.L695
	.long	.L711-.L695
	.long	.L711-.L695
	.long	.L774-.L695
	.long	.L711-.L695
	.long	.L774-.L695
	.long	.L694-.L695
	.long	.L774-.L695
	.long	.L709-.L695
	.long	.L774-.L695
	.long	.L694-.L695
	.long	.L694-.L695
	.long	.L707-.L695
	.long	.L774-.L695
	.long	.L694-.L695
	.long	.L705-.L695
	.long	.L694-.L695
	.long	.L705-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L704-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L694-.L695
	.long	.L694-.L695
	.long	.L694-.L695
	.long	.L694-.L695
	.long	.L694-.L695
	.long	.L694-.L695
	.long	.L694-.L695
	.long	.L774-.L695
	.long	.L700-.L695
	.long	.L700-.L695
	.long	.L700-.L695
	.long	.L700-.L695
	.long	.L700-.L695
	.long	.L700-.L695
	.long	.L700-.L695
	.long	.L700-.L695
	.long	.L700-.L695
	.long	.L700-.L695
	.long	.L700-.L695
	.long	.L700-.L695
	.long	.L700-.L695
	.long	.L700-.L695
	.long	.L700-.L695
	.long	.L700-.L695
	.long	.L700-.L695
	.long	.L700-.L695
	.long	.L700-.L695
	.long	.L700-.L695
	.long	.L700-.L695
	.long	.L700-.L695
	.long	.L700-.L695
	.long	.L874-.L695
	.long	.L874-.L695
	.long	.L694-.L695
	.long	.L697-.L695
	.long	.L775-.L695
	.long	.L776-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L774-.L695
	.long	.L694-.L695
	.section	.text._ZN2v88internal4wasm12OpcodeLengthEPKhS3_
.L775:
	movq	%rbx, %r9
	movl	$5, %r12d
.L696:
	movq	%r13, -128(%rbp)
	cmpq	%rbx, %r9
	je	.L690
	movq	%r9, %rdi
	call	_ZdlPv@PLT
.L690:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L962
	addq	$136, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L776:
	.cfi_restore_state
	movl	$9, %r12d
	jmp	.L690
.L774:
	movl	$1, %r12d
	jmp	.L690
	.p2align 4,,10
	.p2align 3
.L691:
	cmpb	$-3, %al
	je	.L712
	cmpb	$-2, %al
	jne	.L963
	movzbl	1(%rdi), %eax
	movl	$3, %r12d
	orb	$-2, %ah
	cmpl	$65027, %eax
	je	.L690
	jg	.L767
	jne	.L769
.L768:
	leaq	.LC505(%rip), %rdx
.L896:
	movq	%rdi, %rsi
	movq	%r8, %rdi
	movl	$2, %r12d
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	-80(%rbp), %r9
	jmp	.L696
	.p2align 4,,10
	.p2align 3
.L963:
	cmpb	$-4, %al
	jne	.L774
	movzbl	1(%rdi), %eax
	movl	%eax, %ecx
	orb	$-4, %ah
	subl	$64512, %eax
	cmpl	$17, %eax
	ja	.L744
	leaq	.L746(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm12OpcodeLengthEPKhS3_
	.align 4
	.align 4
.L746:
	.long	.L874-.L746
	.long	.L874-.L746
	.long	.L874-.L746
	.long	.L874-.L746
	.long	.L874-.L746
	.long	.L874-.L746
	.long	.L874-.L746
	.long	.L874-.L746
	.long	.L752-.L746
	.long	.L748-.L746
	.long	.L845-.L746
	.long	.L763-.L746
	.long	.L747-.L746
	.long	.L748-.L746
	.long	.L747-.L746
	.long	.L745-.L746
	.long	.L745-.L746
	.long	.L745-.L746
	.section	.text._ZN2v88internal4wasm12OpcodeLengthEPKhS3_
.L694:
	cmpb	$0, 1(%rdi)
	movl	$2, %r12d
	jns	.L690
	cmpb	$0, 2(%rdi)
	movl	$3, %r12d
	jns	.L690
	cmpb	$0, 3(%rdi)
	movl	$4, %r12d
	jns	.L690
	movsbl	4(%rdi), %r12d
	shrl	$31, %r12d
	addl	$5, %r12d
	jmp	.L690
.L874:
	movl	$2, %r12d
	jmp	.L690
.L709:
	cmpb	$0, 1(%rdi)
	movzbl	2(%rdi), %eax
	js	.L728
	leaq	1(%rdi), %rdx
	movl	$4, %ecx
	movl	$6, %edi
	movl	$5, %esi
	movl	$3, %r12d
	movl	$7, %r8d
.L729:
	testb	%al, %al
	jns	.L690
	cmpb	$0, 2(%rdx)
	movl	%ecx, %r12d
	jns	.L690
	cmpb	$0, 3(%rdx)
	movl	%esi, %r12d
	jns	.L690
	cmpb	$0, 4(%rdx)
	cmovs	%r8d, %edi
	movl	%edi, %r12d
	jmp	.L690
.L711:
	leaq	-160(%rbp), %r9
	movq	%rdi, %rcx
	movq	%r8, %rdx
	movq	%r9, %rdi
	leaq	_ZN2v88internal4wasmL16kAllWasmFeaturesE(%rip), %rsi
	call	_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE0EEC1ERKNS1_12WasmFeaturesEPS3_PKh
	movl	-160(%rbp), %eax
	movq	-80(%rbp), %r9
	leal	1(%rax), %r12d
	jmp	.L696
.L700:
	cmpb	$0, 1(%rdi)
	js	.L964
	movl	$5, %esi
	movl	$4, %ecx
	movl	$6, %edx
	movl	$7, %r8d
	movl	$3, %r12d
	movl	$2, %eax
.L715:
	addq	%rax, %rdi
	cmpb	$0, (%rdi)
	jns	.L690
	cmpb	$0, 1(%rdi)
	movl	%ecx, %r12d
	jns	.L690
	cmpb	$0, 2(%rdi)
	movl	%esi, %r12d
	jns	.L690
	cmpb	$0, 3(%rdi)
	cmovns	%edx, %r8d
	movl	%r8d, %r12d
	jmp	.L690
.L704:
	cmpb	$0, 1(%rdi)
	movl	$2, %eax
	movl	$3, %r12d
	js	.L965
.L735:
	movzbl	(%rdi,%rax), %ecx
	subl	$104, %ecx
	cmpb	$23, %cl
	ja	.L736
	movl	$1, %eax
	movq	%rbx, %r9
	salq	%cl, %rax
	testl	$16253313, %eax
	jne	.L696
.L736:
	leaq	1(%rdi), %rsi
	leaq	.LC502(%rip), %rdx
	movq	%r8, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	-80(%rbp), %r9
	jmp	.L696
.L705:
	cmpb	$0, 1(%rdi)
	movzbl	2(%rdi), %eax
	js	.L721
	leaq	1(%rdi), %rdx
	movl	$4, %ecx
	movl	$6, %edi
	movl	$5, %esi
	movl	$3, %r12d
	movl	$7, %r8d
	jmp	.L729
.L707:
	movzbl	1(%rdi), %eax
	leaq	1(%rdi), %rsi
	movl	$2, %r12d
	movl	%eax, %edx
	andl	$127, %edx
	testb	%al, %al
	js	.L966
.L738:
	addq	%rdi, %r12
	xorl	%eax, %eax
.L740:
	cmpb	$0, (%r12)
	movl	$1, %ecx
	js	.L967
.L739:
	addl	$1, %eax
	addq	%rcx, %r12
	cmpl	%edx, %eax
	jbe	.L740
	subq	%rsi, %r12
	addl	$1, %r12d
	jmp	.L690
.L697:
	cmpb	$0, 1(%rdi)
	movl	$2, %r12d
	jns	.L690
	cmpb	$0, 2(%rdi)
	movl	$3, %r12d
	jns	.L690
	cmpb	$0, 3(%rdi)
	movl	$4, %r12d
	jns	.L690
	cmpb	$0, 4(%rdi)
	movl	$5, %r12d
	jns	.L690
	cmpb	$0, 5(%rdi)
	movl	$6, %r12d
	jns	.L690
	cmpb	$0, 6(%rdi)
	movl	$7, %r12d
	jns	.L690
	cmpb	$0, 7(%rdi)
	movl	$8, %r12d
	jns	.L690
	cmpb	$0, 8(%rdi)
	movl	$9, %r12d
	jns	.L690
	movsbl	9(%rdi), %r12d
	sarl	$31, %r12d
	notl	%r12d
	addl	$11, %r12d
	jmp	.L690
	.p2align 4,,10
	.p2align 3
.L745:
	movabsq	$4294967296, %rax
	movq	%rbx, %r9
	movl	$3, %r12d
	movq	%rax, -160(%rbp)
	testb	%cl, %cl
	jns	.L696
	movl	%ecx, %edx
	leaq	-156(%rbp), %rsi
	addq	$2, %rdi
	andl	$127, %edx
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE0ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi1EEET_PKhPjPKcS7_.isra.0
	movl	-156(%rbp), %eax
	movq	-80(%rbp), %r9
	leal	2(%rax), %r12d
	jmp	.L696
.L748:
	cmpb	$0, 2(%rdi)
	movl	$3, %r12d
	jns	.L690
	cmpb	$0, 3(%rdi)
	movl	$4, %r12d
	jns	.L690
	cmpb	$0, 4(%rdi)
	movl	$5, %r12d
	jns	.L690
	movsbl	5(%rdi), %r12d
	sarl	$31, %r12d
	notl	%r12d
	addl	$7, %r12d
	jmp	.L690
	.p2align 4,,10
	.p2align 3
.L747:
	cmpb	$0, 2(%rdi)
	js	.L968
	movl	$5, %r8d
	movl	$7, %ecx
	movl	$6, %esi
	movl	$4, %r12d
	movl	$8, %edx
	movl	$2, %eax
.L758:
	addq	%rdi, %rax
	cmpb	$0, 1(%rax)
	jns	.L690
	cmpb	$0, 2(%rax)
	movl	%r8d, %r12d
	jns	.L690
	cmpb	$0, 3(%rax)
	movl	%esi, %r12d
	jns	.L690
	cmpb	$0, 4(%rax)
	cmovs	%edx, %ecx
	movl	%ecx, %r12d
	jmp	.L690
	.p2align 4,,10
	.p2align 3
.L763:
	movl	$3, %r12d
	jmp	.L690
.L752:
	cmpb	$0, 2(%rdi)
	movl	$4, %r12d
	jns	.L690
	cmpb	$0, 3(%rdi)
	movl	$5, %r12d
	jns	.L690
	cmpb	$0, 4(%rdi)
	movl	$6, %r12d
	jns	.L690
	movsbl	5(%rdi), %r12d
	sarl	$31, %r12d
	notl	%r12d
	addl	$8, %r12d
	jmp	.L690
	.p2align 4,,10
	.p2align 3
.L845:
	movq	%rbx, %r9
	movl	$4, %r12d
	jmp	.L696
	.p2align 4,,10
	.p2align 3
.L767:
	subl	$65040, %eax
	cmpl	$62, %eax
	ja	.L768
.L769:
	cmpb	$0, 2(%rdi)
	js	.L969
	movl	$6, %r8d
	movl	$5, %ecx
	movl	$8, %esi
	movl	$4, %r12d
	movl	$7, %edx
	movl	$3, %eax
.L770:
	addq	%rax, %rdi
	cmpb	$0, (%rdi)
	jns	.L690
	cmpb	$0, 1(%rdi)
	movl	%ecx, %r12d
	jns	.L690
	cmpb	$0, 2(%rdi)
	movl	%r8d, %r12d
	jns	.L690
	cmpb	$0, 3(%rdi)
	cmovns	%edx, %esi
	movl	%esi, %r12d
	jmp	.L690
	.p2align 4,,10
	.p2align 3
.L712:
	movzbl	1(%rdi), %eax
	orb	$-3, %ah
	subl	$64768, %eax
	cmpl	$209, %eax
	ja	.L761
	leaq	.L762(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm12OpcodeLengthEPKhS3_
	.align 4
	.align 4
.L762:
	.long	.L769-.L762
	.long	.L769-.L762
	.long	.L761-.L762
	.long	.L873-.L762
	.long	.L874-.L762
	.long	.L763-.L762
	.long	.L761-.L762
	.long	.L763-.L762
	.long	.L874-.L762
	.long	.L763-.L762
	.long	.L761-.L762
	.long	.L763-.L762
	.long	.L874-.L762
	.long	.L763-.L762
	.long	.L763-.L762
	.long	.L874-.L762
	.long	.L763-.L762
	.long	.L763-.L762
	.long	.L874-.L762
	.long	.L763-.L762
	.long	.L763-.L762
	.long	.L874-.L762
	.long	.L763-.L762
	.long	.L763-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L761-.L762
	.long	.L761-.L762
	.long	.L874-.L762
	.long	.L761-.L762
	.long	.L761-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L761-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L761-.L762
	.long	.L761-.L762
	.long	.L761-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L761-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L761-.L762
	.long	.L761-.L762
	.long	.L761-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L761-.L762
	.long	.L761-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L761-.L762
	.long	.L761-.L762
	.long	.L761-.L762
	.long	.L761-.L762
	.long	.L761-.L762
	.long	.L761-.L762
	.long	.L761-.L762
	.long	.L761-.L762
	.long	.L761-.L762
	.long	.L761-.L762
	.long	.L761-.L762
	.long	.L761-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L761-.L762
	.long	.L761-.L762
	.long	.L761-.L762
	.long	.L761-.L762
	.long	.L761-.L762
	.long	.L761-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.long	.L874-.L762
	.section	.text._ZN2v88internal4wasm12OpcodeLengthEPKhS3_
.L761:
	leaq	.LC504(%rip), %rdx
	jmp	.L896
.L873:
	movq	%rbx, %r9
	movl	$18, %r12d
	jmp	.L696
	.p2align 4,,10
	.p2align 3
.L967:
	cmpb	$0, 1(%r12)
	movl	$2, %ecx
	jns	.L739
	cmpb	$0, 2(%r12)
	movl	$3, %ecx
	jns	.L739
	movsbq	3(%r12), %rcx
	shrq	$63, %rcx
	addq	$4, %rcx
	jmp	.L739
	.p2align 4,,10
	.p2align 3
.L966:
	movzbl	2(%rdi), %ecx
	movl	$3, %r12d
	movl	%ecx, %eax
	sall	$7, %eax
	andl	$16256, %eax
	orl	%eax, %edx
	testb	%cl, %cl
	jns	.L738
	movzbl	3(%rdi), %ecx
	movl	$4, %r12d
	movl	%ecx, %eax
	sall	$14, %eax
	andl	$2080768, %eax
	orl	%eax, %edx
	testb	%cl, %cl
	jns	.L738
	movzbl	4(%rdi), %ecx
	movl	$5, %r12d
	movl	%ecx, %eax
	sall	$21, %eax
	andl	$266338304, %eax
	orl	%eax, %edx
	testb	%cl, %cl
	jns	.L738
	movzbl	5(%rdi), %eax
	movl	$6, %r12d
	sall	$28, %eax
	orl	%eax, %edx
	jmp	.L738
	.p2align 4,,10
	.p2align 3
.L965:
	cmpb	$0, 2(%rdi)
	movl	$3, %eax
	movl	$4, %r12d
	jns	.L735
	cmpb	$0, 3(%rdi)
	movl	$4, %eax
	movl	$5, %r12d
	jns	.L735
	movsbq	4(%rdi), %rax
	movsbl	4(%rdi), %r12d
	sarq	$63, %rax
	sarl	$31, %r12d
	notq	%rax
	notl	%r12d
	addq	$6, %rax
	addl	$7, %r12d
	jmp	.L735
	.p2align 4,,10
	.p2align 3
.L964:
	cmpb	$0, 2(%rdi)
	js	.L970
	movl	$6, %esi
	movl	$5, %ecx
	movl	$7, %edx
	movl	$8, %r8d
	movl	$4, %r12d
	movl	$3, %eax
	jmp	.L715
	.p2align 4,,10
	.p2align 3
.L721:
	testb	%al, %al
	js	.L723
	movzbl	3(%rdi), %eax
	leaq	2(%rdi), %rdx
	movl	$5, %ecx
	movl	$7, %edi
	movl	$6, %esi
	movl	$4, %r12d
	movl	$8, %r8d
	jmp	.L729
	.p2align 4,,10
	.p2align 3
.L728:
	testb	%al, %al
	js	.L730
	movzbl	3(%rdi), %eax
	leaq	2(%rdi), %rdx
	movl	$5, %ecx
	movl	$7, %edi
	movl	$6, %esi
	movl	$4, %r12d
	movl	$8, %r8d
	jmp	.L729
.L969:
	cmpb	$0, 3(%rdi)
	js	.L971
	movl	$7, %r8d
	movl	$6, %ecx
	movl	$9, %esi
	movl	$5, %r12d
	movl	$8, %edx
	movl	$4, %eax
	jmp	.L770
.L970:
	cmpb	$0, 3(%rdi)
	js	.L972
	movl	$7, %esi
	movl	$6, %ecx
	movl	$8, %edx
	movl	$9, %r8d
	movl	$5, %r12d
	movl	$4, %eax
	jmp	.L715
.L723:
	cmpb	$0, 3(%rdi)
	js	.L724
	movzbl	4(%rdi), %eax
	leaq	3(%rdi), %rdx
	movl	$6, %ecx
	movl	$8, %edi
	movl	$7, %esi
	movl	$5, %r12d
	movl	$9, %r8d
	jmp	.L729
.L730:
	cmpb	$0, 3(%rdi)
	js	.L731
	movzbl	4(%rdi), %eax
	leaq	3(%rdi), %rdx
	movl	$6, %ecx
	movl	$8, %edi
	movl	$7, %esi
	movl	$5, %r12d
	movl	$9, %r8d
	jmp	.L729
.L744:
	leaq	.LC503(%rip), %rdx
	jmp	.L896
.L971:
	cmpb	$0, 4(%rdi)
	js	.L973
	movl	$8, %r8d
	movl	$7, %ecx
	movl	$10, %esi
	movl	$6, %r12d
	movl	$9, %edx
	movl	$5, %eax
	jmp	.L770
.L968:
	cmpb	$0, 3(%rdi)
	js	.L974
	movl	$6, %r8d
	movl	$8, %ecx
	movl	$7, %esi
	movl	$5, %r12d
	movl	$9, %edx
	movl	$3, %eax
	jmp	.L758
.L972:
	movsbl	4(%rdi), %edx
	movsbq	4(%rdi), %rax
	movl	%edx, %esi
	movl	%edx, %ecx
	movl	%edx, %r8d
	movl	%edx, %r12d
	sarl	$31, %esi
	sarl	$31, %ecx
	sarl	$31, %r8d
	sarl	$31, %r12d
	notl	%esi
	notl	%ecx
	sarl	$31, %edx
	sarq	$63, %rax
	notl	%r8d
	notl	%r12d
	notl	%edx
	notq	%rax
	addl	$9, %esi
	addl	$8, %ecx
	addl	$11, %r8d
	addl	$7, %r12d
	addl	$10, %edx
	addq	$6, %rax
	jmp	.L715
.L731:
	cmpb	$0, 4(%rdi)
	js	.L975
	movzbl	5(%rdi), %eax
	leaq	4(%rdi), %rdx
	movl	$7, %ecx
	movl	$9, %edi
	movl	$8, %esi
	movl	$6, %r12d
	movl	$10, %r8d
	jmp	.L729
.L724:
	cmpb	$0, 4(%rdi)
	js	.L725
	movzbl	5(%rdi), %eax
	leaq	4(%rdi), %rdx
	movl	$7, %ecx
	movl	$9, %edi
	movl	$8, %esi
	movl	$6, %r12d
	movl	$10, %r8d
	jmp	.L729
.L974:
	cmpb	$0, 4(%rdi)
	js	.L976
	movl	$7, %r8d
	movl	$9, %ecx
	movl	$8, %esi
	movl	$6, %r12d
	movl	$10, %edx
	movl	$4, %eax
	jmp	.L758
.L973:
	movsbl	5(%rdi), %edx
	movsbq	5(%rdi), %rax
	movl	%edx, %r8d
	movl	%edx, %ecx
	movl	%edx, %esi
	movl	%edx, %r12d
	sarl	$31, %r8d
	sarl	$31, %ecx
	sarl	$31, %esi
	sarl	$31, %r12d
	notl	%r8d
	notl	%ecx
	sarl	$31, %edx
	sarq	$63, %rax
	notl	%esi
	notl	%r12d
	notl	%edx
	notq	%rax
	addl	$10, %r8d
	addl	$9, %ecx
	addl	$12, %esi
	addl	$8, %r12d
	addl	$11, %edx
	addq	$7, %rax
	jmp	.L770
.L725:
	movzbl	6(%rdi), %eax
	leaq	5(%rdi), %rdx
	movl	$8, %ecx
	movl	$10, %edi
	movl	$9, %esi
	movl	$7, %r12d
	movl	$11, %r8d
	jmp	.L729
.L975:
	movzbl	6(%rdi), %eax
	leaq	5(%rdi), %rdx
	movl	$8, %ecx
	movl	$10, %edi
	movl	$9, %esi
	movl	$7, %r12d
	movl	$11, %r8d
	jmp	.L729
.L962:
	call	__stack_chk_fail@PLT
.L976:
	movsbl	5(%rdi), %edx
	movsbq	5(%rdi), %rax
	movl	%edx, %r8d
	movl	%edx, %ecx
	movl	%edx, %esi
	movl	%edx, %r12d
	sarl	$31, %r8d
	sarl	$31, %ecx
	sarl	$31, %esi
	sarl	$31, %r12d
	notl	%r8d
	notl	%ecx
	sarl	$31, %edx
	sarq	$63, %rax
	notl	%esi
	notl	%r12d
	notl	%edx
	notq	%rax
	addl	$9, %r8d
	addl	$11, %ecx
	addl	$10, %esi
	addl	$8, %r12d
	addl	$12, %edx
	addq	$6, %rax
	jmp	.L758
	.cfi_endproc
.LFE19602:
	.size	_ZN2v88internal4wasm12OpcodeLengthEPKhS3_, .-_ZN2v88internal4wasm12OpcodeLengthEPKhS3_
	.section	.text._ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE0EEC2EPS3_PKhj,"axG",@progbits,_ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE0EEC5EPS3_PKhj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE0EEC2EPS3_PKhj
	.type	_ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE0EEC2EPS3_PKhj, @function
_ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE0EEC2EPS3_PKhj:
.LFB22727:
	.cfi_startproc
	endbr64
	movzbl	1(%rdx), %eax
	movl	%eax, %ecx
	andl	$127, %ecx
	testb	%al, %al
	js	.L991
	movl	$4, %r9d
	movl	$3, %r8d
	movl	$2, %esi
	movl	$6, %r11d
	movl	$5, %r10d
	movl	$2, %eax
.L978:
	addq	%rax, %rdx
	movl	%ecx, (%rdi)
	movzbl	(%rdx), %eax
	movl	%eax, %ecx
	andl	$127, %ecx
	testb	%al, %al
	js	.L992
	movl	%ecx, 4(%rdi)
	movl	%esi, 8(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L991:
	movzbl	2(%rdx), %esi
	movl	%esi, %eax
	sall	$7, %eax
	andl	$16256, %eax
	orl	%eax, %ecx
	testb	%sil, %sil
	js	.L993
	movl	$5, %r9d
	movl	$4, %r8d
	movl	$3, %esi
	movl	$7, %r11d
	movl	$6, %r10d
	movl	$3, %eax
	jmp	.L978
	.p2align 4,,10
	.p2align 3
.L992:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%r8d, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movzbl	1(%rdx), %ebx
	movl	%ebx, %eax
	sall	$7, %eax
	andl	$16256, %eax
	orl	%eax, %ecx
	testb	%bl, %bl
	js	.L994
.L979:
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	%ecx, 4(%rdi)
	movl	%esi, 8(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L993:
	.cfi_restore 3
	.cfi_restore 6
	movzbl	3(%rdx), %esi
	movl	%esi, %eax
	sall	$14, %eax
	andl	$2080768, %eax
	orl	%eax, %ecx
	testb	%sil, %sil
	js	.L995
	movl	$6, %r9d
	movl	$5, %r8d
	movl	$4, %esi
	movl	$8, %r11d
	movl	$7, %r10d
	movl	$4, %eax
	jmp	.L978
	.p2align 4,,10
	.p2align 3
.L994:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movzbl	2(%rdx), %r8d
	movl	%r9d, %esi
	movl	%r8d, %eax
	sall	$14, %eax
	andl	$2080768, %eax
	orl	%eax, %ecx
	testb	%r8b, %r8b
	jns	.L979
	movzbl	3(%rdx), %r8d
	movl	%r10d, %esi
	movl	%r8d, %eax
	sall	$21, %eax
	andl	$266338304, %eax
	orl	%eax, %ecx
	testb	%r8b, %r8b
	jns	.L979
	movzbl	4(%rdx), %eax
	movl	%r11d, %esi
	sall	$28, %eax
	orl	%eax, %ecx
	jmp	.L979
	.p2align 4,,10
	.p2align 3
.L995:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	movzbl	4(%rdx), %esi
	movl	%esi, %eax
	sall	$21, %eax
	andl	$266338304, %eax
	orl	%eax, %ecx
	testb	%sil, %sil
	js	.L996
	movl	$7, %r9d
	movl	$6, %r8d
	movl	$5, %esi
	movl	$9, %r11d
	movl	$8, %r10d
	movl	$5, %eax
	jmp	.L978
	.p2align 4,,10
	.p2align 3
.L996:
	movzbl	5(%rdx), %eax
	movl	$8, %r9d
	movl	$6, %esi
	movl	$7, %r8d
	movl	$10, %r11d
	movl	$9, %r10d
	sall	$28, %eax
	orl	%eax, %ecx
	movl	$6, %eax
	jmp	.L978
	.cfi_endproc
.LFE22727:
	.size	_ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE0EEC2EPS3_PKhj, .-_ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE0EEC2EPS3_PKhj
	.weak	_ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE0EEC1EPS3_PKhj
	.set	_ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE0EEC1EPS3_PKhj,_ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE0EEC2EPS3_PKhj
	.section	.rodata._ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_.str1.1,"aMS",@progbits,1
.LC506:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_,"axG",@progbits,_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	.type	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_, @function
_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_:
.LFB22807:
	.cfi_startproc
	endbr64
	movabsq	$2305843009213693951, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$2, %rax
	cmpq	%rcx, %rax
	je	.L1011
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L1007
	movabsq	$9223372036854775804, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L1012
.L999:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L1006:
	movl	(%r15), %eax
	subq	%r8, %r13
	leaq	4(%rbx,%rdx), %r15
	movl	%eax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L1013
	testq	%r13, %r13
	jg	.L1002
	testq	%r9, %r9
	jne	.L1005
.L1003:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1013:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L1002
.L1005:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L1003
	.p2align 4,,10
	.p2align 3
.L1012:
	testq	%rsi, %rsi
	jne	.L1000
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L1006
	.p2align 4,,10
	.p2align 3
.L1002:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L1003
	jmp	.L1005
	.p2align 4,,10
	.p2align 3
.L1007:
	movl	$4, %r14d
	jmp	.L999
.L1011:
	leaq	.LC506(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1000:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$2, %r14
	jmp	.L999
	.cfi_endproc
.LFE22807:
	.size	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_, .-_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	.section	.rodata._ZNSt6vectorIN2v88internal4wasm9ValueTypeENS1_13ZoneAllocatorIS3_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS3_S6_EEmRKS3_.str1.1,"aMS",@progbits,1
.LC507:
	.string	"vector::_M_fill_insert"
	.section	.text._ZNSt6vectorIN2v88internal4wasm9ValueTypeENS1_13ZoneAllocatorIS3_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS3_S6_EEmRKS3_,"axG",@progbits,_ZNSt6vectorIN2v88internal4wasm9ValueTypeENS1_13ZoneAllocatorIS3_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS3_S6_EEmRKS3_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal4wasm9ValueTypeENS1_13ZoneAllocatorIS3_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS3_S6_EEmRKS3_
	.type	_ZNSt6vectorIN2v88internal4wasm9ValueTypeENS1_13ZoneAllocatorIS3_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS3_S6_EEmRKS3_, @function
_ZNSt6vectorIN2v88internal4wasm9ValueTypeENS1_13ZoneAllocatorIS3_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS3_S6_EEmRKS3_:
.LFB23364:
	.cfi_startproc
	endbr64
	testq	%rdx, %rdx
	je	.L1146
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	16(%rdi), %rdi
	movq	24(%rbx), %rax
	subq	%rdi, %rax
	cmpq	%rdx, %rax
	jb	.L1017
	movq	%rdi, %r15
	movzbl	(%rcx), %r14d
	subq	%rsi, %r15
	cmpq	%r15, %rdx
	jb	.L1150
	subq	%r15, %r12
	je	.L1057
	movq	%r12, %rdx
	movzbl	%r14b, %esi
	call	memset@PLT
	movq	%rax, %rdi
	addq	%rax, %r12
.L1025:
	movq	%r12, 16(%rbx)
	cmpq	%r13, %rdi
	je	.L1026
	movq	%r13, %rax
	movq	%rdi, %rcx
	notq	%rax
	subq	%r13, %rcx
	addq	%rdi, %rax
	cmpq	$14, %rax
	jbe	.L1027
	leaq	15(%r13), %rax
	subq	%r12, %rax
	cmpq	$30, %rax
	jbe	.L1027
	movq	%rcx, %rdx
	xorl	%eax, %eax
	andq	$-16, %rdx
	.p2align 4,,10
	.p2align 3
.L1028:
	movdqu	0(%r13,%rax), %xmm2
	movups	%xmm2, (%r12,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L1028
	movq	%rcx, %rdx
	andq	$-16, %rdx
	leaq	0(%r13,%rdx), %rax
	addq	%rdx, %r12
	cmpq	%rdx, %rcx
	je	.L1031
	movzbl	(%rax), %edx
	movb	%dl, (%r12)
	leaq	1(%rax), %rdx
	cmpq	%rdx, %rdi
	je	.L1031
	movzbl	1(%rax), %edx
	movb	%dl, 1(%r12)
	leaq	2(%rax), %rdx
	cmpq	%rdx, %rdi
	je	.L1031
	movzbl	2(%rax), %edx
	movb	%dl, 2(%r12)
	leaq	3(%rax), %rdx
	cmpq	%rdx, %rdi
	je	.L1031
	movzbl	3(%rax), %edx
	movb	%dl, 3(%r12)
	leaq	4(%rax), %rdx
	cmpq	%rdx, %rdi
	je	.L1031
	movzbl	4(%rax), %edx
	movb	%dl, 4(%r12)
	leaq	5(%rax), %rdx
	cmpq	%rdx, %rdi
	je	.L1031
	movzbl	5(%rax), %edx
	movb	%dl, 5(%r12)
	leaq	6(%rax), %rdx
	cmpq	%rdx, %rdi
	je	.L1031
	movzbl	6(%rax), %edx
	movb	%dl, 6(%r12)
	leaq	7(%rax), %rdx
	cmpq	%rdx, %rdi
	je	.L1031
	movzbl	7(%rax), %edx
	movb	%dl, 7(%r12)
	leaq	8(%rax), %rdx
	cmpq	%rdx, %rdi
	je	.L1031
	movzbl	8(%rax), %edx
	movb	%dl, 8(%r12)
	leaq	9(%rax), %rdx
	cmpq	%rdx, %rdi
	je	.L1031
	movzbl	9(%rax), %edx
	movb	%dl, 9(%r12)
	leaq	10(%rax), %rdx
	cmpq	%rdx, %rdi
	je	.L1031
	movzbl	10(%rax), %edx
	movb	%dl, 10(%r12)
	leaq	11(%rax), %rdx
	cmpq	%rdx, %rdi
	je	.L1031
	movzbl	11(%rax), %edx
	movb	%dl, 11(%r12)
	leaq	12(%rax), %rdx
	cmpq	%rdx, %rdi
	je	.L1031
	movzbl	12(%rax), %edx
	movb	%dl, 12(%r12)
	leaq	13(%rax), %rdx
	cmpq	%rdx, %rdi
	je	.L1031
	movzbl	13(%rax), %edx
	movb	%dl, 13(%r12)
	leaq	14(%rax), %rdx
	cmpq	%rdx, %rdi
	je	.L1031
	movzbl	14(%rax), %eax
	movb	%al, 14(%r12)
	.p2align 4,,10
	.p2align 3
.L1031:
	movq	%rdi, %rdx
	addq	%r15, 16(%rbx)
	movzbl	%r14b, %esi
	subq	%r13, %rdx
	jmp	.L1149
	.p2align 4,,10
	.p2align 3
.L1146:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L1150:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	%rdi, %rdx
	leaq	16(%rdi), %rax
	subq	%r12, %rdx
	cmpq	%rax, %rdx
	movl	$16, %eax
	setnb	%cl
	subq	%r12, %rax
	testq	%rax, %rax
	setle	%al
	orb	%al, %cl
	je	.L1056
	leaq	-1(%rdi), %rax
	subq	%rdx, %rax
	cmpq	$14, %rax
	jbe	.L1056
	movq	%r12, %rcx
	xorl	%eax, %eax
	andq	$-16, %rcx
	.p2align 4,,10
	.p2align 3
.L1020:
	movdqu	(%rdx,%rax), %xmm1
	movups	%xmm1, (%rdi,%rax)
	addq	$16, %rax
	cmpq	%rcx, %rax
	jne	.L1020
	movq	%r12, %rsi
	andq	$-16, %rsi
	leaq	(%rdx,%rsi), %rax
	leaq	(%rdi,%rsi), %rcx
	cmpq	%rsi, %r12
	je	.L1022
	movzbl	(%rax), %esi
	movb	%sil, (%rcx)
	leaq	1(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L1022
	movzbl	1(%rax), %esi
	movb	%sil, 1(%rcx)
	leaq	2(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L1022
	movzbl	2(%rax), %esi
	movb	%sil, 2(%rcx)
	leaq	3(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L1022
	movzbl	3(%rax), %esi
	movb	%sil, 3(%rcx)
	leaq	4(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L1022
	movzbl	4(%rax), %esi
	movb	%sil, 4(%rcx)
	leaq	5(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L1022
	movzbl	5(%rax), %esi
	movb	%sil, 5(%rcx)
	leaq	6(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L1022
	movzbl	6(%rax), %esi
	movb	%sil, 6(%rcx)
	leaq	7(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L1022
	movzbl	7(%rax), %esi
	movb	%sil, 7(%rcx)
	leaq	8(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L1022
	movzbl	8(%rax), %esi
	movb	%sil, 8(%rcx)
	leaq	9(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L1022
	movzbl	9(%rax), %esi
	movb	%sil, 9(%rcx)
	leaq	10(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L1022
	movzbl	10(%rax), %esi
	movb	%sil, 10(%rcx)
	leaq	11(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L1022
	movzbl	11(%rax), %esi
	movb	%sil, 11(%rcx)
	leaq	12(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L1022
	movzbl	12(%rax), %esi
	movb	%sil, 12(%rcx)
	leaq	13(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L1022
	movzbl	13(%rax), %esi
	movb	%sil, 13(%rcx)
	leaq	14(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L1022
	movzbl	14(%rax), %eax
	movb	%al, 14(%rcx)
	.p2align 4,,10
	.p2align 3
.L1022:
	addq	%r12, 16(%rbx)
	subq	%r13, %rdx
	jne	.L1151
.L1023:
	leaq	0(%r13,%r12), %rax
	cmpq	%rax, %r13
	je	.L1014
	movzbl	%r14b, %esi
	movq	%r12, %rdx
.L1149:
	addq	$24, %rsp
	movq	%r13, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	memset@PLT
	.p2align 4,,10
	.p2align 3
.L1017:
	.cfi_restore_state
	movq	8(%rbx), %rax
	movl	$2147483647, %r14d
	movq	%r14, %rdx
	subq	%rax, %rdi
	subq	%rdi, %rdx
	cmpq	%rdx, %r12
	ja	.L1152
	cmpq	%rdi, %r12
	movq	%rdi, %rdx
	movq	%rsi, %r15
	cmovnb	%r12, %rdx
	addq	%rdx, %rdi
	setc	%dl
	subq	%rax, %r15
	movzbl	%dl, %edx
	testq	%rdx, %rdx
	jne	.L1058
	testq	%rdi, %rdi
	jne	.L1153
	xorl	%edx, %edx
	xorl	%eax, %eax
.L1036:
	leaq	(%rax,%r15), %rsi
	addq	%r12, %r15
	leaq	1(%rcx), %rdi
	addq	%rax, %r15
	cmpq	%r15, %rcx
	setnb	%r8b
	cmpq	%rdi, %rsi
	setnb	%dil
	orb	%dil, %r8b
	je	.L1039
	leaq	-1(%r12), %rdi
	cmpq	$14, %rdi
	jbe	.L1039
	movzbl	(%rcx), %edi
	leaq	-16(%r12), %r8
	shrq	$4, %r8
	movd	%edi, %xmm0
	addq	$1, %r8
	xorl	%edi, %edi
	punpcklbw	%xmm0, %xmm0
	punpcklwd	%xmm0, %xmm0
	pshufd	$0, %xmm0, %xmm0
	.p2align 4,,10
	.p2align 3
.L1040:
	movq	%rdi, %r9
	addq	$1, %rdi
	salq	$4, %r9
	movups	%xmm0, (%rsi,%r9)
	cmpq	%rdi, %r8
	ja	.L1040
	salq	$4, %r8
	movq	%r12, %rdi
	subq	%r8, %rdi
	addq	%r8, %rsi
	cmpq	%r8, %r12
	je	.L1042
	movzbl	(%rcx), %r8d
	movb	%r8b, (%rsi)
	cmpq	$1, %rdi
	je	.L1042
	movzbl	(%rcx), %r8d
	movb	%r8b, 1(%rsi)
	cmpq	$2, %rdi
	je	.L1042
	movzbl	(%rcx), %r8d
	movb	%r8b, 2(%rsi)
	cmpq	$3, %rdi
	je	.L1042
	movzbl	(%rcx), %r8d
	movb	%r8b, 3(%rsi)
	cmpq	$4, %rdi
	je	.L1042
	movzbl	(%rcx), %r8d
	movb	%r8b, 4(%rsi)
	cmpq	$5, %rdi
	je	.L1042
	movzbl	(%rcx), %r8d
	movb	%r8b, 5(%rsi)
	cmpq	$6, %rdi
	je	.L1042
	movzbl	(%rcx), %r8d
	movb	%r8b, 6(%rsi)
	cmpq	$7, %rdi
	je	.L1042
	movzbl	(%rcx), %r8d
	movb	%r8b, 7(%rsi)
	cmpq	$8, %rdi
	je	.L1042
	movzbl	(%rcx), %r8d
	movb	%r8b, 8(%rsi)
	cmpq	$9, %rdi
	je	.L1042
	movzbl	(%rcx), %r8d
	movb	%r8b, 9(%rsi)
	cmpq	$10, %rdi
	je	.L1042
	movzbl	(%rcx), %r8d
	movb	%r8b, 10(%rsi)
	cmpq	$11, %rdi
	je	.L1042
	movzbl	(%rcx), %r8d
	movb	%r8b, 11(%rsi)
	cmpq	$12, %rdi
	je	.L1042
	movzbl	(%rcx), %r8d
	movb	%r8b, 12(%rsi)
	cmpq	$13, %rdi
	je	.L1042
	movzbl	(%rcx), %r8d
	movb	%r8b, 13(%rsi)
	cmpq	$14, %rdi
	je	.L1042
	movzbl	(%rcx), %ecx
	movb	%cl, 14(%rsi)
	.p2align 4,,10
	.p2align 3
.L1042:
	movq	8(%rbx), %rsi
	cmpq	%rsi, %r13
	je	.L1060
	leaq	-1(%r13), %rcx
	subq	%rsi, %rcx
	cmpq	$14, %rcx
	jbe	.L1045
	leaq	15(%rsi), %rcx
	subq	%rax, %rcx
	cmpq	$30, %rcx
	jbe	.L1045
	movq	%r13, %r9
	xorl	%ecx, %ecx
	subq	%rsi, %r9
	movq	%r9, %rdi
	andq	$-16, %rdi
	.p2align 4,,10
	.p2align 3
.L1046:
	movdqu	(%rsi,%rcx), %xmm3
	movups	%xmm3, (%rax,%rcx)
	addq	$16, %rcx
	cmpq	%rdi, %rcx
	jne	.L1046
	movq	%r9, %r8
	andq	$-16, %r8
	leaq	(%rsi,%r8), %rcx
	leaq	(%rax,%r8), %rdi
	cmpq	%r8, %r9
	je	.L1049
	movzbl	(%rcx), %r8d
	movb	%r8b, (%rdi)
	leaq	1(%rcx), %r8
	cmpq	%r8, %r13
	je	.L1049
	movzbl	1(%rcx), %r8d
	movb	%r8b, 1(%rdi)
	leaq	2(%rcx), %r8
	cmpq	%r8, %r13
	je	.L1049
	movzbl	2(%rcx), %r8d
	movb	%r8b, 2(%rdi)
	leaq	3(%rcx), %r8
	cmpq	%r8, %r13
	je	.L1049
	movzbl	3(%rcx), %r8d
	movb	%r8b, 3(%rdi)
	leaq	4(%rcx), %r8
	cmpq	%r8, %r13
	je	.L1049
	movzbl	4(%rcx), %r8d
	movb	%r8b, 4(%rdi)
	leaq	5(%rcx), %r8
	cmpq	%r8, %r13
	je	.L1049
	movzbl	5(%rcx), %r8d
	movb	%r8b, 5(%rdi)
	leaq	6(%rcx), %r8
	cmpq	%r8, %r13
	je	.L1049
	movzbl	6(%rcx), %r8d
	movb	%r8b, 6(%rdi)
	leaq	7(%rcx), %r8
	cmpq	%r8, %r13
	je	.L1049
	movzbl	7(%rcx), %r8d
	movb	%r8b, 7(%rdi)
	leaq	8(%rcx), %r8
	cmpq	%r8, %r13
	je	.L1049
	movzbl	8(%rcx), %r8d
	movb	%r8b, 8(%rdi)
	leaq	9(%rcx), %r8
	cmpq	%r8, %r13
	je	.L1049
	movzbl	9(%rcx), %r8d
	movb	%r8b, 9(%rdi)
	leaq	10(%rcx), %r8
	cmpq	%r8, %r13
	je	.L1049
	movzbl	10(%rcx), %r8d
	movb	%r8b, 10(%rdi)
	leaq	11(%rcx), %r8
	cmpq	%r8, %r13
	je	.L1049
	movzbl	11(%rcx), %r8d
	movb	%r8b, 11(%rdi)
	leaq	12(%rcx), %r8
	cmpq	%r8, %r13
	je	.L1049
	movzbl	12(%rcx), %r8d
	movb	%r8b, 12(%rdi)
	leaq	13(%rcx), %r8
	cmpq	%r8, %r13
	je	.L1049
	movzbl	13(%rcx), %r8d
	movb	%r8b, 13(%rdi)
	leaq	14(%rcx), %r8
	cmpq	%r8, %r13
	je	.L1049
	movzbl	14(%rcx), %ecx
	movb	%cl, 14(%rdi)
	.p2align 4,,10
	.p2align 3
.L1049:
	movq	%r13, %rcx
	subq	%rsi, %rcx
	addq	%rax, %rcx
.L1044:
	movq	16(%rbx), %rdi
	leaq	(%rcx,%r12), %rsi
	cmpq	%rdi, %r13
	je	.L1050
	leaq	16(%rcx,%r12), %rcx
	cmpq	%rcx, %r13
	leaq	16(%r13), %rcx
	setnb	%r8b
	cmpq	%rcx, %rsi
	setnb	%cl
	orb	%cl, %r8b
	je	.L1051
	movq	%r13, %rcx
	notq	%rcx
	addq	%rdi, %rcx
	cmpq	$14, %rcx
	jbe	.L1051
	movq	%rdi, %r10
	xorl	%ecx, %ecx
	subq	%r13, %r10
	movq	%r10, %r8
	andq	$-16, %r8
	.p2align 4,,10
	.p2align 3
.L1052:
	movdqu	0(%r13,%rcx), %xmm4
	movups	%xmm4, (%rsi,%rcx)
	addq	$16, %rcx
	cmpq	%r8, %rcx
	jne	.L1052
	movq	%r10, %r9
	andq	$-16, %r9
	leaq	0(%r13,%r9), %rcx
	leaq	(%rsi,%r9), %r8
	cmpq	%r9, %r10
	je	.L1055
	movzbl	(%rcx), %r9d
	movb	%r9b, (%r8)
	leaq	1(%rcx), %r9
	cmpq	%r9, %rdi
	je	.L1055
	movzbl	1(%rcx), %r9d
	movb	%r9b, 1(%r8)
	leaq	2(%rcx), %r9
	cmpq	%r9, %rdi
	je	.L1055
	movzbl	2(%rcx), %r9d
	movb	%r9b, 2(%r8)
	leaq	3(%rcx), %r9
	cmpq	%r9, %rdi
	je	.L1055
	movzbl	3(%rcx), %r9d
	movb	%r9b, 3(%r8)
	leaq	4(%rcx), %r9
	cmpq	%r9, %rdi
	je	.L1055
	movzbl	4(%rcx), %r9d
	movb	%r9b, 4(%r8)
	leaq	5(%rcx), %r9
	cmpq	%r9, %rdi
	je	.L1055
	movzbl	5(%rcx), %r9d
	movb	%r9b, 5(%r8)
	leaq	6(%rcx), %r9
	cmpq	%r9, %rdi
	je	.L1055
	movzbl	6(%rcx), %r9d
	movb	%r9b, 6(%r8)
	leaq	7(%rcx), %r9
	cmpq	%r9, %rdi
	je	.L1055
	movzbl	7(%rcx), %r9d
	movb	%r9b, 7(%r8)
	leaq	8(%rcx), %r9
	cmpq	%r9, %rdi
	je	.L1055
	movzbl	8(%rcx), %r9d
	movb	%r9b, 8(%r8)
	leaq	9(%rcx), %r9
	cmpq	%r9, %rdi
	je	.L1055
	movzbl	9(%rcx), %r9d
	movb	%r9b, 9(%r8)
	leaq	10(%rcx), %r9
	cmpq	%r9, %rdi
	je	.L1055
	movzbl	10(%rcx), %r9d
	movb	%r9b, 10(%r8)
	leaq	11(%rcx), %r9
	cmpq	%r9, %rdi
	je	.L1055
	movzbl	11(%rcx), %r9d
	movb	%r9b, 11(%r8)
	leaq	12(%rcx), %r9
	cmpq	%r9, %rdi
	je	.L1055
	movzbl	12(%rcx), %r9d
	movb	%r9b, 12(%r8)
	leaq	13(%rcx), %r9
	cmpq	%r9, %rdi
	je	.L1055
	movzbl	13(%rcx), %r9d
	movb	%r9b, 13(%r8)
	leaq	14(%rcx), %r9
	cmpq	%r9, %rdi
	je	.L1055
	movzbl	14(%rcx), %ecx
	movb	%cl, 14(%r8)
	.p2align 4,,10
	.p2align 3
.L1055:
	subq	%r13, %rdi
	addq	%rdi, %rsi
.L1050:
	movq	%rax, %xmm0
	movq	%rsi, %xmm5
	movq	%rdx, 24(%rbx)
	punpcklqdq	%xmm5, %xmm0
	movups	%xmm0, 8(%rbx)
.L1014:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1058:
	.cfi_restore_state
	movl	$2147483648, %esi
	movl	$2147483647, %r14d
.L1035:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L1154
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1038:
	leaq	(%rax,%r14), %rdx
	jmp	.L1036
	.p2align 4,,10
	.p2align 3
.L1151:
	subq	%rdx, %rdi
	movq	%r13, %rsi
	call	memmove@PLT
	jmp	.L1023
	.p2align 4,,10
	.p2align 3
.L1056:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1019:
	movzbl	(%rdx,%rax), %ecx
	movb	%cl, (%rdi,%rax)
	addq	$1, %rax
	cmpq	%rax, %r12
	jne	.L1019
	jmp	.L1022
	.p2align 4,,10
	.p2align 3
.L1039:
	leaq	(%rsi,%r12), %r8
	.p2align 4,,10
	.p2align 3
.L1043:
	movzbl	(%rcx), %edi
	addq	$1, %rsi
	movb	%dil, -1(%rsi)
	cmpq	%r8, %rsi
	jne	.L1043
	jmp	.L1042
	.p2align 4,,10
	.p2align 3
.L1027:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1030:
	movzbl	0(%r13,%rax), %edx
	movb	%dl, (%r12,%rax)
	addq	$1, %rax
	cmpq	%rcx, %rax
	jne	.L1030
	jmp	.L1031
	.p2align 4,,10
	.p2align 3
.L1051:
	movq	%rdi, %r9
	xorl	%ecx, %ecx
	subq	%r13, %r9
	.p2align 4,,10
	.p2align 3
.L1054:
	movzbl	0(%r13,%rcx), %r8d
	movb	%r8b, (%rsi,%rcx)
	addq	$1, %rcx
	cmpq	%r9, %rcx
	jne	.L1054
	jmp	.L1055
	.p2align 4,,10
	.p2align 3
.L1045:
	movq	%r13, %r8
	xorl	%ecx, %ecx
	subq	%rsi, %r8
	.p2align 4,,10
	.p2align 3
.L1048:
	movzbl	(%rsi,%rcx), %edi
	movb	%dil, (%rax,%rcx)
	addq	$1, %rcx
	cmpq	%r8, %rcx
	jne	.L1048
	jmp	.L1049
	.p2align 4,,10
	.p2align 3
.L1026:
	addq	%r15, %r12
	movq	%r12, 16(%rbx)
	jmp	.L1014
	.p2align 4,,10
	.p2align 3
.L1057:
	movq	%rdi, %r12
	jmp	.L1025
	.p2align 4,,10
	.p2align 3
.L1060:
	movq	%rax, %rcx
	jmp	.L1044
.L1154:
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	jmp	.L1038
.L1153:
	cmpq	$2147483647, %rdi
	cmovbe	%rdi, %r14
	leaq	7(%r14), %rsi
	andq	$-8, %rsi
	jmp	.L1035
.L1152:
	leaq	.LC507(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE23364:
	.size	_ZNSt6vectorIN2v88internal4wasm9ValueTypeENS1_13ZoneAllocatorIS3_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS3_S6_EEmRKS3_, .-_ZNSt6vectorIN2v88internal4wasm9ValueTypeENS1_13ZoneAllocatorIS3_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS3_S6_EEmRKS3_
	.section	.rodata._ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12DecodeLocalsERKNS1_12WasmFeaturesEPS3_PKNS0_9SignatureINS1_9ValueTypeEEEPNS0_10ZoneVectorISB_EE.str1.8,"aMS",@progbits,1
	.align 8
.LC508:
	.string	"cannot create std::vector larger than max_size()"
	.section	.rodata._ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12DecodeLocalsERKNS1_12WasmFeaturesEPS3_PKNS0_9SignatureINS1_9ValueTypeEEEPNS0_10ZoneVectorISB_EE.str1.1,"aMS",@progbits,1
.LC509:
	.string	"local decls count"
.LC510:
	.string	"local count"
.LC511:
	.string	"local count too large"
	.section	.rodata._ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12DecodeLocalsERKNS1_12WasmFeaturesEPS3_PKNS0_9SignatureINS1_9ValueTypeEEEPNS0_10ZoneVectorISB_EE.str1.8
	.align 8
.LC512:
	.string	"expected %u bytes, fell off end"
	.align 8
.LC513:
	.string	"invalid local type 'anyref', enable with --experimental-wasm-anyref"
	.align 8
.LC514:
	.string	"invalid local type 'funcref', enable with --experimental-wasm-anyref"
	.align 8
.LC515:
	.string	"invalid local type 'exception ref', enable with --experimental-wasm-eh"
	.align 8
.LC516:
	.string	"invalid local type 'Simd128', enable with --experimental-wasm-simd"
	.section	.rodata._ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12DecodeLocalsERKNS1_12WasmFeaturesEPS3_PKNS0_9SignatureINS1_9ValueTypeEEEPNS0_10ZoneVectorISB_EE.str1.1
.LC517:
	.string	"invalid local type"
	.section	.text._ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12DecodeLocalsERKNS1_12WasmFeaturesEPS3_PKNS0_9SignatureINS1_9ValueTypeEEEPNS0_10ZoneVectorISB_EE,"axG",@progbits,_ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12DecodeLocalsERKNS1_12WasmFeaturesEPS3_PKNS0_9SignatureINS1_9ValueTypeEEEPNS0_10ZoneVectorISB_EE,comdat
	.p2align 4
	.weak	_ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12DecodeLocalsERKNS1_12WasmFeaturesEPS3_PKNS0_9SignatureINS1_9ValueTypeEEEPNS0_10ZoneVectorISB_EE
	.type	_ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12DecodeLocalsERKNS1_12WasmFeaturesEPS3_PKNS0_9SignatureINS1_9ValueTypeEEEPNS0_10ZoneVectorISB_EE, @function
_ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12DecodeLocalsERKNS1_12WasmFeaturesEPS3_PKNS0_9SignatureINS1_9ValueTypeEEEPNS0_10ZoneVectorISB_EE:
.LFB21525:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L1156
	movq	(%rdx), %r8
	movq	8(%rdx), %rcx
	movq	8(%rbx), %rdi
	movq	24(%rbx), %rax
	movq	16(%rdx), %r9
	leaq	(%rcx,%r8), %r15
	subq	%rdi, %rax
	leaq	(%r9,%r8), %r14
	addq	%r9, %r15
	cmpq	%rax, %rcx
	jbe	.L1157
	cmpq	$2147483647, %rcx
	ja	.L1291
	movq	(%rbx), %rdi
	leaq	7(%rcx), %rsi
	andq	$-8, %rsi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L1292
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1160:
	cmpq	%r15, %r14
	je	.L1287
	leaq	16(%r9,%r8), %rdx
	cmpq	%rdx, %rax
	leaq	16(%rax), %rdx
	setnb	%sil
	cmpq	%rdx, %r14
	setnb	%dl
	orb	%dl, %sil
	je	.L1163
	leaq	-1(%rcx), %rdx
	cmpq	$14, %rdx
	jbe	.L1163
	movq	%rcx, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	andq	$-16, %rsi
	subq	%rax, %rdi
	addq	%rax, %rsi
	.p2align 4,,10
	.p2align 3
.L1165:
	movdqu	(%rdx,%rdi), %xmm0
	addq	$16, %rdx
	movups	%xmm0, -16(%rdx)
	cmpq	%rdx, %rsi
	jne	.L1165
	movq	%rcx, %rsi
	andq	$-16, %rsi
	addq	%rsi, %r14
	leaq	(%rax,%rsi), %rdx
	cmpq	%rsi, %rcx
	je	.L1166
	movzbl	(%r14), %esi
	movb	%sil, (%rdx)
	leaq	1(%r14), %rsi
	cmpq	%rsi, %r15
	je	.L1287
	movzbl	1(%r14), %esi
	movb	%sil, 1(%rdx)
	leaq	2(%r14), %rsi
	cmpq	%rsi, %r15
	je	.L1287
	movzbl	2(%r14), %esi
	movb	%sil, 2(%rdx)
	leaq	3(%r14), %rsi
	cmpq	%rsi, %r15
	je	.L1287
	movzbl	3(%r14), %esi
	movb	%sil, 3(%rdx)
	leaq	4(%r14), %rsi
	cmpq	%rsi, %r15
	je	.L1287
	movzbl	4(%r14), %esi
	movb	%sil, 4(%rdx)
	leaq	5(%r14), %rsi
	cmpq	%rsi, %r15
	je	.L1287
	movzbl	5(%r14), %esi
	movb	%sil, 5(%rdx)
	leaq	6(%r14), %rsi
	cmpq	%rsi, %r15
	je	.L1287
	movzbl	6(%r14), %esi
	movb	%sil, 6(%rdx)
	leaq	7(%r14), %rsi
	cmpq	%rsi, %r15
	je	.L1287
	movzbl	7(%r14), %esi
	movb	%sil, 7(%rdx)
	leaq	8(%r14), %rsi
	cmpq	%rsi, %r15
	je	.L1287
	movzbl	8(%r14), %esi
	movb	%sil, 8(%rdx)
	leaq	9(%r14), %rsi
	cmpq	%rsi, %r15
	je	.L1287
	movzbl	9(%r14), %esi
	movb	%sil, 9(%rdx)
	leaq	10(%r14), %rsi
	cmpq	%rsi, %r15
	je	.L1287
	movzbl	10(%r14), %esi
	movb	%sil, 10(%rdx)
	leaq	11(%r14), %rsi
	cmpq	%rsi, %r15
	je	.L1287
	movzbl	11(%r14), %esi
	movb	%sil, 11(%rdx)
	leaq	12(%r14), %rsi
	cmpq	%rsi, %r15
	je	.L1287
	movzbl	12(%r14), %esi
	movb	%sil, 12(%rdx)
	leaq	13(%r14), %rsi
	cmpq	%rsi, %r15
	je	.L1287
	movzbl	13(%r14), %esi
	movb	%sil, 13(%rdx)
	leaq	14(%r14), %rsi
	cmpq	%rsi, %r15
	je	.L1287
	movzbl	14(%r14), %esi
	movb	%sil, 14(%rdx)
	leaq	(%rax,%rcx), %rdx
	jmp	.L1166
	.p2align 4,,10
	.p2align 3
.L1157:
	movq	16(%rbx), %rax
	movq	%rax, %rdx
	subq	%rdi, %rdx
	cmpq	%rdx, %rcx
	ja	.L1170
	testq	%rcx, %rcx
	jne	.L1293
.L1171:
	addq	%rdi, %rcx
	cmpq	%rax, %rcx
	je	.L1156
	movq	%rcx, 16(%rbx)
	.p2align 4,,10
	.p2align 3
.L1156:
	movq	16(%r12), %rsi
	movq	24(%r12), %rax
	cmpq	%rax, %rsi
	jnb	.L1179
.L1302:
	movzbl	(%rsi), %edx
	leaq	1(%rsi), %r8
	movl	%edx, %ecx
	andl	$127, %ecx
	testb	%dl, %dl
	js	.L1294
.L1180:
	movq	%r8, 16(%r12)
.L1190:
	xorl	%eax, %eax
	cmpq	$0, 56(%r12)
	jne	.L1155
	leal	-1(%rcx), %r15d
	testl	%ecx, %ecx
	je	.L1193
	leaq	.L1211(%rip), %r14
	.p2align 4,,10
	.p2align 3
.L1192:
	movq	16(%r12), %rax
	movq	24(%r12), %rsi
	cmpq	%rsi, %rax
	jnb	.L1193
	movzbl	(%rax), %ecx
	leaq	1(%rax), %r8
	movl	%ecx, %edx
	andl	$127, %edx
	testb	%cl, %cl
	js	.L1295
	movq	%r8, 16(%r12)
.L1203:
	cmpq	$0, 56(%r12)
	jne	.L1207
	movq	16(%rbx), %rcx
	movl	$50000, %eax
	subq	8(%rbx), %rcx
	subq	%rcx, %rax
	cmpq	%rax, %rdx
	ja	.L1296
.L1206:
	movq	16(%r12), %rsi
	cmpl	%esi, 24(%r12)
	je	.L1208
	movzbl	(%rsi), %eax
	leaq	1(%rsi), %rcx
	movq	%rcx, 16(%r12)
	subl	$104, %eax
	cmpb	$23, %al
	ja	.L1209
	movzbl	%al, %eax
	movslq	(%r14,%rax,4), %rax
	addq	%r14, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12DecodeLocalsERKNS1_12WasmFeaturesEPS3_PKNS0_9SignatureINS1_9ValueTypeEEEPNS0_10ZoneVectorISB_EE,"aG",@progbits,_ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12DecodeLocalsERKNS1_12WasmFeaturesEPS3_PKNS0_9SignatureINS1_9ValueTypeEEEPNS0_10ZoneVectorISB_EE,comdat
	.align 4
	.align 4
.L1211:
	.long	.L1218-.L1211
	.long	.L1209-.L1211
	.long	.L1209-.L1211
	.long	.L1209-.L1211
	.long	.L1209-.L1211
	.long	.L1209-.L1211
	.long	.L1209-.L1211
	.long	.L1217-.L1211
	.long	.L1216-.L1211
	.long	.L1209-.L1211
	.long	.L1209-.L1211
	.long	.L1209-.L1211
	.long	.L1209-.L1211
	.long	.L1209-.L1211
	.long	.L1209-.L1211
	.long	.L1209-.L1211
	.long	.L1209-.L1211
	.long	.L1209-.L1211
	.long	.L1209-.L1211
	.long	.L1215-.L1211
	.long	.L1214-.L1211
	.long	.L1213-.L1211
	.long	.L1212-.L1211
	.long	.L1210-.L1211
	.section	.text._ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12DecodeLocalsERKNS1_12WasmFeaturesEPS3_PKNS0_9SignatureINS1_9ValueTypeEEEPNS0_10ZoneVectorISB_EE,"axG",@progbits,_ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12DecodeLocalsERKNS1_12WasmFeaturesEPS3_PKNS0_9SignatureINS1_9ValueTypeEEEPNS0_10ZoneVectorISB_EE,comdat
	.p2align 4,,10
	.p2align 3
.L1208:
	movl	$1, %ecx
	xorl	%eax, %eax
	leaq	.LC512(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%r12), %rcx
	cmpq	$0, 56(%r12)
	movq	%rcx, 16(%r12)
	jne	.L1207
	.p2align 4,,10
	.p2align 3
.L1209:
	leaq	-1(%rcx), %rsi
	leaq	.LC517(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
.L1207:
	xorl	%eax, %eax
	jmp	.L1155
	.p2align 4,,10
	.p2align 3
.L1179:
	leaq	.LC509(%rip), %rcx
	leaq	.LC487(%rip), %rdx
.L1290:
	xorl	%eax, %eax
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	cmpq	$0, 56(%r12)
	sete	%al
.L1155:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1297
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1210:
	.cfi_restore_state
	movb	$1, -57(%rbp)
	.p2align 4,,10
	.p2align 3
.L1219:
	movq	16(%rbx), %rsi
	leaq	-57(%rbp), %rcx
	movq	%rbx, %rdi
	subl	$1, %r15d
	call	_ZNSt6vectorIN2v88internal4wasm9ValueTypeENS1_13ZoneAllocatorIS3_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS3_S6_EEmRKS3_
	cmpl	$-1, %r15d
	jne	.L1192
.L1193:
	movl	$1, %eax
	jmp	.L1155
	.p2align 4,,10
	.p2align 3
.L1212:
	movb	$2, -57(%rbp)
	jmp	.L1219
	.p2align 4,,10
	.p2align 3
.L1213:
	movb	$3, -57(%rbp)
	jmp	.L1219
	.p2align 4,,10
	.p2align 3
.L1214:
	movb	$4, -57(%rbp)
	jmp	.L1219
	.p2align 4,,10
	.p2align 3
.L1215:
	cmpb	$0, 3(%r13)
	je	.L1223
	movb	$5, -57(%rbp)
	jmp	.L1219
	.p2align 4,,10
	.p2align 3
.L1216:
	cmpb	$0, 7(%r13)
	je	.L1221
	movb	$7, -57(%rbp)
	jmp	.L1219
	.p2align 4,,10
	.p2align 3
.L1217:
	cmpb	$0, 7(%r13)
	je	.L1220
	movb	$6, -57(%rbp)
	jmp	.L1219
	.p2align 4,,10
	.p2align 3
.L1218:
	cmpb	$0, 1(%r13)
	je	.L1222
	movb	$9, -57(%rbp)
	jmp	.L1219
	.p2align 4,,10
	.p2align 3
.L1295:
	cmpq	%r8, %rsi
	ja	.L1298
.L1201:
	movq	%r8, 16(%r12)
	movq	%r8, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC510(%rip), %rcx
	leaq	.LC487(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
.L1289:
	movq	56(%r12), %rdx
	testq	%rdx, %rdx
	je	.L1206
	xorl	%eax, %eax
	jmp	.L1155
	.p2align 4,,10
	.p2align 3
.L1298:
	movzbl	1(%rax), %edi
	movl	%edi, %ecx
	sall	$7, %ecx
	andl	$16256, %ecx
	orl	%ecx, %edx
	testb	%dil, %dil
	jns	.L1196
	leaq	2(%rax), %r8
	cmpq	%r8, %rsi
	jbe	.L1201
	movzbl	2(%rax), %edi
	movl	%edi, %ecx
	sall	$14, %ecx
	andl	$2080768, %ecx
	orl	%ecx, %edx
	testb	%dil, %dil
	jns	.L1198
	leaq	3(%rax), %r8
	cmpq	%r8, %rsi
	jbe	.L1201
	movzbl	3(%rax), %edi
	movl	%edi, %ecx
	sall	$21, %ecx
	andl	$266338304, %ecx
	orl	%ecx, %edx
	testb	%dil, %dil
	jns	.L1200
	leaq	4(%rax), %r8
	cmpq	%r8, %rsi
	jbe	.L1201
	movzbl	4(%rax), %ecx
	addq	$5, %rax
	movq	%rax, 16(%r12)
	movl	%ecx, %esi
	movl	%ecx, %r9d
	sall	$28, %esi
	andl	$-16, %r9d
	orl	%esi, %edx
	testb	%cl, %cl
	js	.L1299
.L1202:
	testb	%r9b, %r9b
	je	.L1203
	leaq	.LC488(%rip), %rdx
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	jmp	.L1289
	.p2align 4,,10
	.p2align 3
.L1294:
	cmpq	%r8, %rax
	jbe	.L1181
	movzbl	1(%rsi), %edi
	leaq	2(%rsi), %r8
	movl	%edi, %edx
	sall	$7, %edx
	andl	$16256, %edx
	orl	%edx, %ecx
	testb	%dil, %dil
	jns	.L1180
	cmpq	%r8, %rax
	jbe	.L1181
	movzbl	2(%rsi), %edi
	leaq	3(%rsi), %r8
	movl	%edi, %edx
	sall	$14, %edx
	andl	$2080768, %edx
	orl	%edx, %ecx
	testb	%dil, %dil
	jns	.L1180
	cmpq	%r8, %rax
	jbe	.L1181
	movzbl	3(%rsi), %edi
	leaq	4(%rsi), %r14
	movl	%edi, %edx
	sall	$21, %edx
	andl	$266338304, %edx
	orl	%edx, %ecx
	testb	%dil, %dil
	js	.L1300
	movq	%r14, 16(%r12)
	jmp	.L1190
	.p2align 4,,10
	.p2align 3
.L1170:
	leaq	(%r14,%rdx), %rcx
	testq	%rdx, %rdx
	jne	.L1301
.L1172:
	cmpq	%rcx, %r15
	je	.L1173
	addq	%rdx, %r8
	leaq	16(%r9,%r8), %rdx
	leaq	(%r9,%r8), %rsi
	cmpq	%rdx, %rax
	leaq	16(%rax), %rdx
	setnb	%dil
	cmpq	%rdx, %rsi
	setnb	%dl
	orb	%dl, %dil
	je	.L1174
	leaq	-1(%r15), %rdx
	subq	%rcx, %rdx
	cmpq	$14, %rdx
	jbe	.L1174
	movq	%r15, %r8
	xorl	%edx, %edx
	subq	%rcx, %r8
	movq	%r8, %rdi
	andq	$-16, %rdi
	.p2align 4,,10
	.p2align 3
.L1175:
	movdqu	(%rsi,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rdi, %rdx
	jne	.L1175
	movq	%r8, %rdi
	andq	$-16, %rdi
	leaq	(%rcx,%rdi), %rdx
	leaq	(%rax,%rdi), %rsi
	cmpq	%r8, %rdi
	je	.L1177
	movzbl	(%rdx), %edi
	movb	%dil, (%rsi)
	leaq	1(%rdx), %rdi
	cmpq	%rdi, %r15
	je	.L1177
	movzbl	1(%rdx), %edi
	movb	%dil, 1(%rsi)
	leaq	2(%rdx), %rdi
	cmpq	%rdi, %r15
	je	.L1177
	movzbl	2(%rdx), %edi
	movb	%dil, 2(%rsi)
	leaq	3(%rdx), %rdi
	cmpq	%rdi, %r15
	je	.L1177
	movzbl	3(%rdx), %edi
	movb	%dil, 3(%rsi)
	leaq	4(%rdx), %rdi
	cmpq	%rdi, %r15
	je	.L1177
	movzbl	4(%rdx), %edi
	movb	%dil, 4(%rsi)
	leaq	5(%rdx), %rdi
	cmpq	%rdi, %r15
	je	.L1177
	movzbl	5(%rdx), %edi
	movb	%dil, 5(%rsi)
	leaq	6(%rdx), %rdi
	cmpq	%rdi, %r15
	je	.L1177
	movzbl	6(%rdx), %edi
	movb	%dil, 6(%rsi)
	leaq	7(%rdx), %rdi
	cmpq	%rdi, %r15
	je	.L1177
	movzbl	7(%rdx), %edi
	movb	%dil, 7(%rsi)
	leaq	8(%rdx), %rdi
	cmpq	%rdi, %r15
	je	.L1177
	movzbl	8(%rdx), %edi
	movb	%dil, 8(%rsi)
	leaq	9(%rdx), %rdi
	cmpq	%rdi, %r15
	je	.L1177
	movzbl	9(%rdx), %edi
	movb	%dil, 9(%rsi)
	leaq	10(%rdx), %rdi
	cmpq	%rdi, %r15
	je	.L1177
	movzbl	10(%rdx), %edi
	movb	%dil, 10(%rsi)
	leaq	11(%rdx), %rdi
	cmpq	%rdi, %r15
	je	.L1177
	movzbl	11(%rdx), %edi
	movb	%dil, 11(%rsi)
	leaq	12(%rdx), %rdi
	cmpq	%rdi, %r15
	je	.L1177
	movzbl	12(%rdx), %edi
	movb	%dil, 12(%rsi)
	leaq	13(%rdx), %rdi
	cmpq	%rdi, %r15
	je	.L1177
	movzbl	13(%rdx), %edi
	movb	%dil, 13(%rsi)
	leaq	14(%rdx), %rdi
	cmpq	%rdi, %r15
	je	.L1177
	movzbl	14(%rdx), %edx
	movb	%dl, 14(%rsi)
	.p2align 4,,10
	.p2align 3
.L1177:
	subq	%rcx, %r15
	addq	%r15, %rax
.L1173:
	movq	%rax, 16(%rbx)
	movq	16(%r12), %rsi
	movq	24(%r12), %rax
	cmpq	%rax, %rsi
	jnb	.L1179
	jmp	.L1302
	.p2align 4,,10
	.p2align 3
.L1181:
	movq	%r8, 16(%r12)
	leaq	.LC509(%rip), %rcx
	leaq	.LC487(%rip), %rdx
	movq	%r8, %rsi
	jmp	.L1290
	.p2align 4,,10
	.p2align 3
.L1287:
	leaq	(%rax,%rcx), %rdx
.L1166:
	movq	%rax, 8(%rbx)
	movq	%rdx, 16(%rbx)
	movq	%rdx, 24(%rbx)
	movq	16(%r12), %rsi
	movq	24(%r12), %rax
	cmpq	%rax, %rsi
	jnb	.L1179
	jmp	.L1302
	.p2align 4,,10
	.p2align 3
.L1196:
	addq	$2, %rax
	movq	%rax, 16(%r12)
	jmp	.L1203
	.p2align 4,,10
	.p2align 3
.L1296:
	movq	16(%r12), %rax
	leaq	.LC511(%rip), %rdx
	movq	%r12, %rdi
	leaq	-1(%rax), %rsi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	xorl	%eax, %eax
	jmp	.L1155
	.p2align 4,,10
	.p2align 3
.L1163:
	leaq	(%rcx,%rax), %rdx
	subq	%rax, %r14
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L1169:
	movzbl	(%rcx,%r14), %esi
	addq	$1, %rcx
	movb	%sil, -1(%rcx)
	cmpq	%rcx, %rdx
	jne	.L1169
	jmp	.L1166
	.p2align 4,,10
	.p2align 3
.L1198:
	addq	$3, %rax
	movq	%rax, 16(%r12)
	jmp	.L1203
	.p2align 4,,10
	.p2align 3
.L1293:
	movq	%rcx, %rdx
	movq	%r14, %rsi
	movq	%rcx, -72(%rbp)
	call	memmove@PLT
	movq	-72(%rbp), %rcx
	movq	%rax, %rdi
	movq	16(%rbx), %rax
	jmp	.L1171
.L1301:
	movq	%r14, %rsi
	movq	%rcx, -96(%rbp)
	movq	%r9, -88(%rbp)
	movq	%r8, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	memmove@PLT
	movq	16(%rbx), %rax
	movq	-96(%rbp), %rcx
	movq	-88(%rbp), %r9
	movq	-80(%rbp), %r8
	movq	-72(%rbp), %rdx
	jmp	.L1172
.L1292:
	movq	%r9, -88(%rbp)
	movq	%r8, -80(%rbp)
	movq	%rcx, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %r8
	movq	-88(%rbp), %r9
	jmp	.L1160
.L1174:
	movq	%r15, %rdi
	xorl	%edx, %edx
	subq	%rcx, %rdi
	.p2align 4,,10
	.p2align 3
.L1178:
	movzbl	(%rcx,%rdx), %esi
	movb	%sil, (%rax,%rdx)
	addq	$1, %rdx
	cmpq	%rdi, %rdx
	jne	.L1178
	jmp	.L1177
.L1200:
	addq	$4, %rax
	movq	%rax, 16(%r12)
	jmp	.L1203
.L1220:
	leaq	.LC513(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	xorl	%eax, %eax
	jmp	.L1155
.L1222:
	leaq	.LC515(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	xorl	%eax, %eax
	jmp	.L1155
.L1223:
	leaq	.LC516(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	xorl	%eax, %eax
	jmp	.L1155
.L1221:
	leaq	.LC514(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	xorl	%eax, %eax
	jmp	.L1155
.L1300:
	cmpq	%r14, %rax
	jbe	.L1187
	movzbl	4(%rsi), %r15d
	addq	$5, %rsi
	movq	%rsi, 16(%r12)
	testb	%r15b, %r15b
	js	.L1188
	movl	%r15d, %eax
	sall	$28, %eax
	orl	%eax, %ecx
.L1189:
	andl	$240, %r15d
	je	.L1190
	leaq	.LC488(%rip), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	cmpq	$0, 56(%r12)
	sete	%al
	jmp	.L1155
.L1187:
	movq	%r14, 16(%r12)
	xorl	%r15d, %r15d
.L1188:
	leaq	.LC509(%rip), %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC487(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	xorl	%ecx, %ecx
	jmp	.L1189
.L1297:
	call	__stack_chk_fail@PLT
.L1299:
	leaq	.LC487(%rip), %rdx
	movq	%r8, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC510(%rip), %rcx
	movb	%r9b, -80(%rbp)
	movq	%r8, -72(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movzbl	-80(%rbp), %r9d
	movq	-72(%rbp), %r8
	xorl	%edx, %edx
	jmp	.L1202
.L1291:
	leaq	.LC508(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE21525:
	.size	_ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12DecodeLocalsERKNS1_12WasmFeaturesEPS3_PKNS0_9SignatureINS1_9ValueTypeEEEPNS0_10ZoneVectorISB_EE, .-_ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12DecodeLocalsERKNS1_12WasmFeaturesEPS3_PKNS0_9SignatureINS1_9ValueTypeEEEPNS0_10ZoneVectorISB_EE
	.section	.text._ZN2v88internal4wasm16DecodeLocalDeclsERKNS1_12WasmFeaturesEPNS1_14BodyLocalDeclsEPKhS8_,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm16DecodeLocalDeclsERKNS1_12WasmFeaturesEPNS1_14BodyLocalDeclsEPKhS8_
	.type	_ZN2v88internal4wasm16DecodeLocalDeclsERKNS1_12WasmFeaturesEPNS1_14BodyLocalDeclsEPKhS8_, @function
_ZN2v88internal4wasm16DecodeLocalDeclsERKNS1_12WasmFeaturesEPNS1_14BodyLocalDeclsEPKhS8_:
.LFB19590:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-64(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	leaq	-128(%rbp), %rsi
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rdx, -120(%rbp)
	movq	%rdx, -112(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -104(%rbp)
	leaq	8(%rbx), %rcx
	movq	%r14, -128(%rbp)
	movl	$0, -96(%rbp)
	movl	$0, -88(%rbp)
	movq	%r13, -80(%rbp)
	movq	$0, -72(%rbp)
	movb	$0, -64(%rbp)
	call	_ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12DecodeLocalsERKNS1_12WasmFeaturesEPS3_PKNS0_9SignatureINS1_9ValueTypeEEEPNS0_10ZoneVectorISB_EE
	movl	%eax, %r12d
	testb	%al, %al
	je	.L1304
	movq	-112(%rbp), %rax
	subq	-120(%rbp), %rax
	addl	-96(%rbp), %eax
	movl	%eax, (%rbx)
.L1304:
	movq	-80(%rbp), %rdi
	movq	%r14, -128(%rbp)
	cmpq	%r13, %rdi
	je	.L1303
	call	_ZdlPv@PLT
.L1303:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1311
	addq	$96, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1311:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19590:
	.size	_ZN2v88internal4wasm16DecodeLocalDeclsERKNS1_12WasmFeaturesEPNS1_14BodyLocalDeclsEPKhS8_, .-_ZN2v88internal4wasm16DecodeLocalDeclsERKNS1_12WasmFeaturesEPNS1_14BodyLocalDeclsEPKhS8_
	.section	.text._ZN2v88internal4wasm16BytecodeIteratorC2EPKhS4_PNS1_14BodyLocalDeclsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm16BytecodeIteratorC2EPKhS4_PNS1_14BodyLocalDeclsE
	.type	_ZN2v88internal4wasm16BytecodeIteratorC2EPKhS4_PNS1_14BodyLocalDeclsE, @function
_ZN2v88internal4wasm16BytecodeIteratorC2EPKhS4_PNS1_14BodyLocalDeclsE:
.LFB19596:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$96, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	64(%rdi), %rax
	movq	%rsi, 8(%rdi)
	movq	%rax, 48(%rdi)
	leaq	16+_ZTVN2v88internal4wasm16BytecodeIteratorE(%rip), %rax
	movq	%rsi, 16(%rdi)
	movq	%rdx, 24(%rdi)
	movl	$0, 32(%rdi)
	movl	$0, 40(%rdi)
	movq	$0, 56(%rdi)
	movb	$0, 64(%rdi)
	movq	%rax, (%rdi)
	testq	%rcx, %rcx
	je	.L1312
	movq	%rsi, -120(%rbp)
	movq	%rdi, %rbx
	movq	%rcx, %r12
	leaq	-64(%rbp), %r13
	movq	%rsi, -112(%rbp)
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %r14
	leaq	-128(%rbp), %rsi
	movq	%rdx, -104(%rbp)
	leaq	8(%rcx), %rcx
	xorl	%edx, %edx
	leaq	_ZN2v88internal4wasmL16kAllWasmFeaturesE(%rip), %rdi
	movq	%r14, -128(%rbp)
	movl	$0, -96(%rbp)
	movl	$0, -88(%rbp)
	movq	%r13, -80(%rbp)
	movq	$0, -72(%rbp)
	movb	$0, -64(%rbp)
	call	_ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12DecodeLocalsERKNS1_12WasmFeaturesEPS3_PKNS0_9SignatureINS1_9ValueTypeEEEPNS0_10ZoneVectorISB_EE
	testb	%al, %al
	jne	.L1322
	movq	-80(%rbp), %rdi
	movq	%r14, -128(%rbp)
	cmpq	%r13, %rdi
	je	.L1312
	call	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L1312:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1323
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1322:
	.cfi_restore_state
	movq	-80(%rbp), %rdi
	movq	-112(%rbp), %rax
	movq	%r14, -128(%rbp)
	subq	-120(%rbp), %rax
	addl	-96(%rbp), %eax
	movl	%eax, (%r12)
	cmpq	%r13, %rdi
	je	.L1318
	call	_ZdlPv@PLT
	movl	(%r12), %eax
.L1318:
	movq	24(%rbx), %rdx
	addq	16(%rbx), %rax
	cmpq	%rdx, %rax
	cmova	%rdx, %rax
	movq	%rax, 16(%rbx)
	jmp	.L1312
.L1323:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19596:
	.size	_ZN2v88internal4wasm16BytecodeIteratorC2EPKhS4_PNS1_14BodyLocalDeclsE, .-_ZN2v88internal4wasm16BytecodeIteratorC2EPKhS4_PNS1_14BodyLocalDeclsE
	.globl	_ZN2v88internal4wasm16BytecodeIteratorC1EPKhS4_PNS1_14BodyLocalDeclsE
	.set	_ZN2v88internal4wasm16BytecodeIteratorC1EPKhS4_PNS1_14BodyLocalDeclsE,_ZN2v88internal4wasm16BytecodeIteratorC2EPKhS4_PNS1_14BodyLocalDeclsE
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE11PushControlENS1_11ControlKindE,"axG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE11PushControlENS1_11ControlKindE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE11PushControlENS1_11ControlKindE
	.type	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE11PushControlENS1_11ControlKindE, @function
_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE11PushControlENS1_11ControlKindE:
.LFB23391:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	224(%rdi), %rbx
	movq	216(%rdi), %r13
	cmpq	%rbx, %r13
	je	.L1325
	cmpb	$0, -72(%rbx)
	setne	%dl
.L1325:
	movq	192(%r12), %rax
	subq	184(%r12), %rax
	sarq	$4, %rax
	movq	%rax, %r14
	cmpq	232(%r12), %rbx
	je	.L1326
	movq	16(%r12), %rax
	pxor	%xmm0, %xmm0
	movb	%dl, 16(%rbx)
	xorl	$1, %edx
	movb	%dl, 48(%rbx)
	movq	%rax, 8(%rbx)
	andb	$1, 48(%rbx)
	movb	%r15b, (%rbx)
	movl	%r14d, 4(%rbx)
	movl	$0, 24(%rbx)
	movl	$0, 56(%rbx)
	movb	$0, 80(%rbx)
	movups	%xmm0, 32(%rbx)
	movups	%xmm0, 64(%rbx)
	movq	224(%r12), %r8
	leaq	88(%r8), %rax
	movq	%rax, 224(%r12)
.L1324:
	addq	$40, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1326:
	.cfi_restore_state
	movabsq	$3353953467947191203, %rsi
	movq	%rbx, %rcx
	subq	%r13, %rcx
	movq	%rcx, %rax
	sarq	$3, %rax
	imulq	%rsi, %rax
	cmpq	$24403223, %rax
	je	.L1343
	testq	%rax, %rax
	je	.L1336
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L1344
	movl	$2147483624, %esi
	movl	$2147483624, %r8d
.L1329:
	movq	208(%r12), %r9
	movq	16(%r9), %rax
	movq	24(%r9), %rdi
	subq	%rax, %rdi
	cmpq	%rdi, %rsi
	ja	.L1345
	addq	%rax, %rsi
	movq	%rsi, 16(%r9)
.L1332:
	leaq	(%rax,%r8), %rsi
	leaq	88(%rax), %rdi
.L1330:
	movq	16(%r12), %r8
	addq	%rax, %rcx
	pxor	%xmm0, %xmm0
	movb	%dl, 16(%rcx)
	xorl	$1, %edx
	movb	%dl, 48(%rcx)
	movb	%r15b, (%rcx)
	andb	$1, 48(%rcx)
	movl	%r14d, 4(%rcx)
	movq	%r8, 8(%rcx)
	movl	$0, 24(%rcx)
	movl	$0, 56(%rcx)
	movb	$0, 80(%rcx)
	movups	%xmm0, 32(%rcx)
	movups	%xmm0, 64(%rcx)
	cmpq	%rbx, %r13
	je	.L1339
	movq	%r13, %rdx
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L1334:
	movdqu	(%rdx), %xmm1
	addq	$88, %rdx
	addq	$88, %rcx
	movups	%xmm1, -88(%rcx)
	movdqu	-72(%rdx), %xmm2
	movups	%xmm2, -72(%rcx)
	movdqu	-56(%rdx), %xmm3
	movups	%xmm3, -56(%rcx)
	movdqu	-40(%rdx), %xmm4
	movups	%xmm4, -40(%rcx)
	movdqu	-24(%rdx), %xmm5
	movups	%xmm5, -24(%rcx)
	movq	-8(%rdx), %rdi
	movq	%rdi, -8(%rcx)
	cmpq	%rdx, %rbx
	jne	.L1334
	leaq	-88(%rbx), %rdx
	movabsq	$1048110458733497251, %rbx
	subq	%r13, %rdx
	shrq	$3, %rdx
	imulq	%rbx, %rdx
	movabsq	$2305843009213693951, %rbx
	andq	%rbx, %rdx
	addq	$1, %rdx
	leaq	(%rdx,%rdx,4), %rcx
	leaq	(%rdx,%rcx,2), %rdx
	leaq	(%rax,%rdx,8), %r8
	leaq	88(%r8), %rdi
.L1333:
	movq	%rax, %xmm0
	movq	%rdi, %xmm6
	movq	%rsi, 232(%r12)
	punpcklqdq	%xmm6, %xmm0
	movups	%xmm0, 216(%r12)
	jmp	.L1324
	.p2align 4,,10
	.p2align 3
.L1344:
	testq	%rsi, %rsi
	jne	.L1346
	movl	$88, %edi
	xorl	%esi, %esi
	xorl	%eax, %eax
	jmp	.L1330
	.p2align 4,,10
	.p2align 3
.L1336:
	movl	$88, %esi
	movl	$88, %r8d
	jmp	.L1329
	.p2align 4,,10
	.p2align 3
.L1339:
	movq	%rax, %r8
	jmp	.L1333
.L1345:
	movq	%r9, %rdi
	movq	%rcx, -72(%rbp)
	movq	%r8, -64(%rbp)
	movb	%dl, -49(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movzbl	-49(%rbp), %edx
	movq	-64(%rbp), %r8
	movq	-72(%rbp), %rcx
	jmp	.L1332
.L1343:
	leaq	.LC506(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1346:
	cmpq	$24403223, %rsi
	movl	$24403223, %r8d
	cmova	%r8, %rsi
	imulq	$88, %rsi, %r8
	movq	%r8, %rsi
	jmp	.L1329
	.cfi_endproc
.LFE23391:
	.size	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE11PushControlENS1_11ControlKindE, .-_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE11PushControlENS1_11ControlKindE
	.section	.rodata._ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE1EEC2ERKNS1_12WasmFeaturesEPS3_PKh.str1.1,"aMS",@progbits,1
.LC518:
	.string	"block type"
.LC519:
	.string	"invalid block type"
.LC520:
	.string	"block arity"
.LC521:
	.string	"invalid block type index"
	.section	.text._ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE1EEC2ERKNS1_12WasmFeaturesEPS3_PKh,"axG",@progbits,_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE1EEC5ERKNS1_12WasmFeaturesEPS3_PKh,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE1EEC2ERKNS1_12WasmFeaturesEPS3_PKh
	.type	_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE1EEC2ERKNS1_12WasmFeaturesEPS3_PKh, @function
_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE1EEC2ERKNS1_12WasmFeaturesEPS3_PKh:
.LFB23422:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	1(%rcx), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	24(%rdx), %rax
	movb	$0, 4(%rdi)
	movl	$1, (%rdi)
	movl	$0, 8(%rdi)
	movq	$0, 16(%rdi)
	cmpq	%rax, %r14
	ja	.L1348
	cmpl	%r14d, %eax
	je	.L1348
	movzbl	1(%rcx), %eax
	subl	$64, %eax
	cmpb	$63, %al
	ja	.L1349
	leaq	.L1351(%rip), %rdx
	movzbl	%al, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE1EEC2ERKNS1_12WasmFeaturesEPS3_PKh,"aG",@progbits,_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE1EEC5ERKNS1_12WasmFeaturesEPS3_PKh,comdat
	.align 4
	.align 4
.L1351:
	.long	.L1347-.L1351
	.long	.L1349-.L1351
	.long	.L1349-.L1351
	.long	.L1349-.L1351
	.long	.L1349-.L1351
	.long	.L1349-.L1351
	.long	.L1349-.L1351
	.long	.L1349-.L1351
	.long	.L1349-.L1351
	.long	.L1349-.L1351
	.long	.L1349-.L1351
	.long	.L1349-.L1351
	.long	.L1349-.L1351
	.long	.L1349-.L1351
	.long	.L1349-.L1351
	.long	.L1349-.L1351
	.long	.L1349-.L1351
	.long	.L1349-.L1351
	.long	.L1349-.L1351
	.long	.L1349-.L1351
	.long	.L1349-.L1351
	.long	.L1349-.L1351
	.long	.L1349-.L1351
	.long	.L1349-.L1351
	.long	.L1349-.L1351
	.long	.L1349-.L1351
	.long	.L1349-.L1351
	.long	.L1349-.L1351
	.long	.L1349-.L1351
	.long	.L1349-.L1351
	.long	.L1349-.L1351
	.long	.L1349-.L1351
	.long	.L1349-.L1351
	.long	.L1349-.L1351
	.long	.L1349-.L1351
	.long	.L1349-.L1351
	.long	.L1349-.L1351
	.long	.L1349-.L1351
	.long	.L1349-.L1351
	.long	.L1349-.L1351
	.long	.L1358-.L1351
	.long	.L1349-.L1351
	.long	.L1349-.L1351
	.long	.L1349-.L1351
	.long	.L1349-.L1351
	.long	.L1349-.L1351
	.long	.L1349-.L1351
	.long	.L1357-.L1351
	.long	.L1356-.L1351
	.long	.L1349-.L1351
	.long	.L1349-.L1351
	.long	.L1349-.L1351
	.long	.L1349-.L1351
	.long	.L1349-.L1351
	.long	.L1349-.L1351
	.long	.L1349-.L1351
	.long	.L1349-.L1351
	.long	.L1349-.L1351
	.long	.L1349-.L1351
	.long	.L1355-.L1351
	.long	.L1354-.L1351
	.long	.L1353-.L1351
	.long	.L1352-.L1351
	.long	.L1350-.L1351
	.section	.text._ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE1EEC2ERKNS1_12WasmFeaturesEPS3_PKh,"axG",@progbits,_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE1EEC5ERKNS1_12WasmFeaturesEPS3_PKh,comdat
	.p2align 4,,10
	.p2align 3
.L1348:
	leaq	.LC518(%rip), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
.L1349:
	cmpb	$0, (%r15)
	movb	$10, 4(%rbx)
	leaq	.LC519(%rip), %rdx
	je	.L1386
	cmpq	$0, 56(%r12)
	je	.L1387
.L1347:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1350:
	.cfi_restore_state
	movb	$1, 4(%rdi)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1352:
	.cfi_restore_state
	movb	$2, 4(%rdi)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1353:
	.cfi_restore_state
	movb	$3, 4(%rdi)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1354:
	.cfi_restore_state
	movb	$4, 4(%rdi)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1355:
	.cfi_restore_state
	movb	$5, 4(%rdi)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1356:
	.cfi_restore_state
	movb	$7, 4(%rdi)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1357:
	.cfi_restore_state
	movb	$6, 4(%rdi)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1358:
	.cfi_restore_state
	movb	$9, 4(%rdi)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1387:
	.cfi_restore_state
	movq	24(%r12), %rcx
	cmpq	%rcx, %r14
	jb	.L1388
	movl	$0, (%rbx)
	leaq	.LC520(%rip), %rcx
	leaq	.LC487(%rip), %rdx
	movq	%r14, %rsi
.L1385:
	xorl	%eax, %eax
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	(%rbx), %eax
	testl	%eax, %eax
	sete	%al
	xorl	%ecx, %ecx
.L1374:
	testb	%al, %al
	jne	.L1376
.L1375:
	movl	%ecx, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1395:
	.cfi_restore_state
	leaq	5(%r13), %r15
	cmpq	%r15, %rcx
	jbe	.L1369
	movzbl	5(%r13), %r13d
	movl	$5, (%rbx)
	testb	%r13b, %r13b
	js	.L1370
	movl	%r13d, %ecx
	sall	$28, %ecx
	orl	%eax, %ecx
.L1371:
	andb	$-8, %r13b
	je	.L1382
	cmpb	$120, %r13b
	je	.L1382
	leaq	.LC488(%rip), %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	xorl	%ecx, %ecx
	cmpl	$0, (%rbx)
	jne	.L1375
	.p2align 4,,10
	.p2align 3
.L1376:
	leaq	.LC521(%rip), %rdx
.L1386:
	addq	$8, %rsp
	movq	%r14, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	.p2align 4,,10
	.p2align 3
.L1388:
	.cfi_restore_state
	movzbl	1(%r13), %edx
	movl	%edx, %eax
	andl	$127, %eax
	testb	%dl, %dl
	js	.L1389
	sall	$25, %eax
	movl	$1, (%rbx)
	movl	%eax, %ecx
	shrl	$31, %eax
	sarl	$25, %ecx
	jmp	.L1374
	.p2align 4,,10
	.p2align 3
.L1389:
	leaq	2(%r13), %rsi
	cmpq	%rsi, %rcx
	ja	.L1390
	movl	$1, (%rbx)
.L1384:
	leaq	.LC520(%rip), %rcx
	leaq	.LC487(%rip), %rdx
	jmp	.L1385
	.p2align 4,,10
	.p2align 3
.L1390:
	movzbl	2(%r13), %esi
	movzbl	%al, %eax
	movl	%esi, %edx
	sall	$7, %edx
	andl	$16256, %edx
	orl	%edx, %eax
	testb	%sil, %sil
	js	.L1391
	sall	$18, %eax
	movl	$2, (%rbx)
	movl	%eax, %ecx
	shrl	$31, %eax
	sarl	$18, %ecx
	jmp	.L1374
	.p2align 4,,10
	.p2align 3
.L1391:
	leaq	3(%r13), %rsi
	cmpq	%rsi, %rcx
	ja	.L1392
	movl	$2, (%rbx)
	jmp	.L1384
.L1392:
	movzbl	3(%r13), %esi
	movl	%esi, %edx
	sall	$14, %edx
	andl	$2080768, %edx
	orl	%edx, %eax
	testb	%sil, %sil
	js	.L1393
	sall	$11, %eax
	movl	$3, (%rbx)
	movl	%eax, %ecx
	shrl	$31, %eax
	sarl	$11, %ecx
	jmp	.L1374
.L1393:
	leaq	4(%r13), %rsi
	cmpq	%rsi, %rcx
	ja	.L1394
	movl	$3, (%rbx)
	jmp	.L1384
.L1394:
	movzbl	4(%r13), %esi
	movl	%esi, %edx
	sall	$21, %edx
	andl	$266338304, %edx
	orl	%edx, %eax
	testb	%sil, %sil
	js	.L1395
	sall	$4, %eax
	movl	$4, (%rbx)
	movl	%eax, %ecx
	shrl	$31, %eax
	sarl	$4, %ecx
	jmp	.L1374
.L1369:
	movl	$4, (%rbx)
	xorl	%r13d, %r13d
.L1370:
	leaq	.LC520(%rip), %rcx
	movq	%r15, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC487(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	xorl	%ecx, %ecx
	jmp	.L1371
.L1382:
	cmpl	$0, (%rbx)
	movl	%ecx, %edx
	sete	%al
	shrl	$31, %edx
	orl	%edx, %eax
	jmp	.L1374
	.cfi_endproc
.LFE23422:
	.size	_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE1EEC2ERKNS1_12WasmFeaturesEPS3_PKh, .-_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE1EEC2ERKNS1_12WasmFeaturesEPS3_PKh
	.weak	_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE1EEC1ERKNS1_12WasmFeaturesEPS3_PKh
	.set	_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE1EEC1ERKNS1_12WasmFeaturesEPS3_PKh,_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE1EEC2ERKNS1_12WasmFeaturesEPS3_PKh
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE12SetBlockTypeEPNS1_11ControlBaseINS1_9ValueBaseEEERNS1_18BlockTypeImmediateILS4_1EEEPS8_,"axG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE12SetBlockTypeEPNS1_11ControlBaseINS1_9ValueBaseEEERNS1_18BlockTypeImmediateILS4_1EEEPS8_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE12SetBlockTypeEPNS1_11ControlBaseINS1_9ValueBaseEEERNS1_18BlockTypeImmediateILS4_1EEEPS8_
	.type	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE12SetBlockTypeEPNS1_11ControlBaseINS1_9ValueBaseEEERNS1_18BlockTypeImmediateILS4_1EEEPS8_, @function
_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE12SetBlockTypeEPNS1_11ControlBaseINS1_9ValueBaseEEERNS1_18BlockTypeImmediateILS4_1EEEPS8_:
.LFB23433:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movzbl	4(%rdx), %eax
	testb	%al, %al
	je	.L1418
	movq	16(%rdi), %r14
	cmpb	$10, %al
	jne	.L1419
	movq	16(%rdx), %rax
	movq	(%rax), %r15
	movl	%r15d, 56(%rsi)
	cmpl	$1, %r15d
	jne	.L1401
.L1400:
	movzbl	4(%rdx), %eax
	cmpb	$10, %al
	jne	.L1402
	movq	16(%rdx), %rax
	movq	16(%rax), %rax
	movzbl	(%rax), %eax
.L1402:
	movq	%r14, 64(%rbx)
	movb	%al, 72(%rbx)
.L1398:
	cmpb	$10, 4(%rdx)
	je	.L1409
	movl	$0, 24(%rbx)
.L1396:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1419:
	.cfi_restore_state
	movl	$1, 56(%rsi)
	jmp	.L1400
	.p2align 4,,10
	.p2align 3
.L1409:
	movq	16(%rdx), %rax
	movq	8(%rax), %r14
	movl	%r14d, 24(%rbx)
	cmpl	$1, %r14d
	jne	.L1411
	movq	(%r12), %rdx
	movzbl	8(%r12), %eax
	movq	%rdx, 32(%rbx)
	movb	%al, 40(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1418:
	.cfi_restore_state
	movl	$0, 56(%rsi)
	jmp	.L1398
	.p2align 4,,10
	.p2align 3
.L1401:
	jbe	.L1398
	movq	128(%rdi), %rdi
	movl	%r15d, %esi
	salq	$4, %rsi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	%rcx, %rsi
	ja	.L1420
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1405:
	movq	%rax, 64(%rbx)
	movq	%rax, %rcx
	leal	-1(%r15), %r8d
	xorl	%esi, %esi
	jmp	.L1408
	.p2align 4,,10
	.p2align 3
.L1421:
	movq	64(%rbx), %rcx
	movq	%rax, %rsi
.L1408:
	movzbl	4(%rdx), %edi
	cmpb	$10, %dil
	jne	.L1406
	movq	16(%rdx), %rax
	movq	16(%rax), %rax
	movzbl	(%rax,%rsi), %edi
.L1406:
	movq	%rsi, %rax
	salq	$4, %rax
	addq	%rcx, %rax
	movq	%r14, (%rax)
	movb	%dil, 8(%rax)
	leaq	1(%rsi), %rax
	cmpq	%rsi, %r8
	jne	.L1421
	jmp	.L1398
	.p2align 4,,10
	.p2align 3
.L1411:
	jbe	.L1396
	movq	128(%r13), %rdi
	movl	%r14d, %esi
	salq	$4, %rsi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L1422
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1414:
	movq	%rax, 32(%rbx)
	movq	(%r12), %rcx
	movzbl	8(%r12), %edx
	movq	%rcx, (%rax)
	leal	-2(%r14), %ecx
	addq	$2, %rcx
	movb	%dl, 8(%rax)
	movl	$16, %eax
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L1415:
	movq	32(%rbx), %rdx
	movq	(%r12,%rax), %rdi
	movzbl	8(%r12,%rax), %esi
	addq	%rax, %rdx
	addq	$16, %rax
	movq	%rdi, (%rdx)
	movb	%sil, 8(%rdx)
	cmpq	%rax, %rcx
	jne	.L1415
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1420:
	.cfi_restore_state
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	jmp	.L1405
	.p2align 4,,10
	.p2align 3
.L1422:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1414
	.cfi_endproc
.LFE23433:
	.size	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE12SetBlockTypeEPNS1_11ControlBaseINS1_9ValueBaseEEERNS1_18BlockTypeImmediateILS4_1EEEPS8_, .-_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE12SetBlockTypeEPNS1_11ControlBaseINS1_9ValueBaseEEERNS1_18BlockTypeImmediateILS4_1EEEPS8_
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE10EndControlEv,"axG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE10EndControlEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE10EndControlEv
	.type	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE10EndControlEv, @function
_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE10EndControlEv:
.LFB23440:
	.cfi_startproc
	endbr64
	movq	224(%rdi), %rdx
	movl	-84(%rdx), %eax
	salq	$4, %rax
	addq	184(%rdi), %rax
	cmpq	192(%rdi), %rax
	je	.L1424
	movq	%rax, 192(%rdi)
.L1424:
	movb	$2, -72(%rdx)
	ret
	.cfi_endproc
.LFE23440:
	.size	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE10EndControlEv, .-_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE10EndControlEv
	.section	.rodata._ZN2v88internal4wasm19SelectTypeImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKh.str1.1,"aMS",@progbits,1
.LC522:
	.string	"number of select types"
	.section	.rodata._ZN2v88internal4wasm19SelectTypeImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKh.str1.8,"aMS",@progbits,1
	.align 8
.LC523:
	.string	"Invalid number of types. Select accepts exactly one type"
	.section	.rodata._ZN2v88internal4wasm19SelectTypeImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKh.str1.1
.LC524:
	.string	"select type"
	.section	.text._ZN2v88internal4wasm19SelectTypeImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKh,"axG",@progbits,_ZN2v88internal4wasm19SelectTypeImmediateILNS1_7Decoder12ValidateFlagE1EEC5EPS3_PKh,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm19SelectTypeImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKh
	.type	_ZN2v88internal4wasm19SelectTypeImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKh, @function
_ZN2v88internal4wasm19SelectTypeImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKh:
.LFB23485:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	1(%rdx), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	24(%rsi), %rax
	cmpq	%rax, %r13
	jb	.L1459
	movl	$0, (%rdi)
	leaq	.LC522(%rip), %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	.LC487(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
.L1437:
	leaq	.LC523(%rip), %rdx
.L1458:
	addq	$8, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	.p2align 4,,10
	.p2align 3
.L1459:
	.cfi_restore_state
	movzbl	1(%rdx), %esi
	movl	%esi, %ecx
	movl	%esi, %edi
	andl	$127, %ecx
	andl	$127, %edi
	testb	%sil, %sil
	js	.L1460
	movl	$1, (%rbx)
	movl	$2, %edi
	movl	$2, %esi
.L1454:
	cmpb	$1, %cl
	jne	.L1437
	addq	%rdx, %rsi
	cmpq	%rax, %rsi
	ja	.L1440
	cmpl	%esi, %eax
	je	.L1440
	movzbl	(%rsi), %eax
	movl	%edi, (%rbx)
	subl	$64, %eax
	cmpb	$63, %al
	ja	.L1441
	leaq	.L1443(%rip), %rdx
	movzbl	%al, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm19SelectTypeImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKh,"aG",@progbits,_ZN2v88internal4wasm19SelectTypeImmediateILNS1_7Decoder12ValidateFlagE1EEC5EPS3_PKh,comdat
	.align 4
	.align 4
.L1443:
	.long	.L1451-.L1443
	.long	.L1441-.L1443
	.long	.L1441-.L1443
	.long	.L1441-.L1443
	.long	.L1441-.L1443
	.long	.L1441-.L1443
	.long	.L1441-.L1443
	.long	.L1441-.L1443
	.long	.L1441-.L1443
	.long	.L1441-.L1443
	.long	.L1441-.L1443
	.long	.L1441-.L1443
	.long	.L1441-.L1443
	.long	.L1441-.L1443
	.long	.L1441-.L1443
	.long	.L1441-.L1443
	.long	.L1441-.L1443
	.long	.L1441-.L1443
	.long	.L1441-.L1443
	.long	.L1441-.L1443
	.long	.L1441-.L1443
	.long	.L1441-.L1443
	.long	.L1441-.L1443
	.long	.L1441-.L1443
	.long	.L1441-.L1443
	.long	.L1441-.L1443
	.long	.L1441-.L1443
	.long	.L1441-.L1443
	.long	.L1441-.L1443
	.long	.L1441-.L1443
	.long	.L1441-.L1443
	.long	.L1441-.L1443
	.long	.L1441-.L1443
	.long	.L1441-.L1443
	.long	.L1441-.L1443
	.long	.L1441-.L1443
	.long	.L1441-.L1443
	.long	.L1441-.L1443
	.long	.L1441-.L1443
	.long	.L1441-.L1443
	.long	.L1450-.L1443
	.long	.L1441-.L1443
	.long	.L1441-.L1443
	.long	.L1441-.L1443
	.long	.L1441-.L1443
	.long	.L1441-.L1443
	.long	.L1441-.L1443
	.long	.L1449-.L1443
	.long	.L1448-.L1443
	.long	.L1441-.L1443
	.long	.L1441-.L1443
	.long	.L1441-.L1443
	.long	.L1441-.L1443
	.long	.L1441-.L1443
	.long	.L1441-.L1443
	.long	.L1441-.L1443
	.long	.L1441-.L1443
	.long	.L1441-.L1443
	.long	.L1441-.L1443
	.long	.L1447-.L1443
	.long	.L1446-.L1443
	.long	.L1445-.L1443
	.long	.L1444-.L1443
	.long	.L1442-.L1443
	.section	.text._ZN2v88internal4wasm19SelectTypeImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKh,"axG",@progbits,_ZN2v88internal4wasm19SelectTypeImmediateILNS1_7Decoder12ValidateFlagE1EEC5EPS3_PKh,comdat
	.p2align 4,,10
	.p2align 3
.L1460:
	leaq	2(%rdx), %rsi
	cmpq	%rsi, %rax
	jbe	.L1428
	movzbl	2(%rdx), %esi
	movl	%esi, %ecx
	sall	$7, %ecx
	andl	$16256, %ecx
	orl	%edi, %ecx
	testb	%sil, %sil
	js	.L1461
	movl	$2, (%rbx)
	movl	$3, %edi
	movl	$3, %esi
	jmp	.L1454
	.p2align 4,,10
	.p2align 3
.L1440:
	leaq	.LC524(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	addl	$1, (%rbx)
.L1441:
	movb	$10, 4(%rbx)
.L1452:
	leaq	.LC502(%rip), %rdx
	jmp	.L1458
	.p2align 4,,10
	.p2align 3
.L1428:
	movl	$1, (%rbx)
.L1457:
	leaq	.LC522(%rip), %rcx
	leaq	.LC487(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L1437
.L1451:
	movb	$0, 4(%rbx)
	jmp	.L1452
.L1442:
	movb	$1, 4(%rbx)
.L1425:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1444:
	.cfi_restore_state
	movb	$2, 4(%rbx)
	jmp	.L1425
.L1445:
	movb	$3, 4(%rbx)
	jmp	.L1425
.L1446:
	movb	$4, 4(%rbx)
	jmp	.L1425
.L1447:
	movb	$5, 4(%rbx)
	jmp	.L1425
.L1448:
	movb	$7, 4(%rbx)
	jmp	.L1425
.L1449:
	movb	$6, 4(%rbx)
	jmp	.L1425
.L1450:
	movb	$9, 4(%rbx)
	jmp	.L1425
	.p2align 4,,10
	.p2align 3
.L1461:
	leaq	3(%rdx), %rsi
	cmpq	%rsi, %rax
	jbe	.L1430
	movzbl	3(%rdx), %edi
	movl	%edi, %esi
	sall	$14, %esi
	andl	$2080768, %esi
	orl	%esi, %ecx
	testb	%dil, %dil
	js	.L1462
	movl	$3, (%rbx)
	movl	$4, %edi
	movl	$4, %esi
	jmp	.L1454
	.p2align 4,,10
	.p2align 3
.L1430:
	movl	$2, (%rbx)
	jmp	.L1457
	.p2align 4,,10
	.p2align 3
.L1462:
	leaq	4(%rdx), %rsi
	cmpq	%rsi, %rax
	ja	.L1463
	movl	$3, (%rbx)
	jmp	.L1457
.L1463:
	movzbl	4(%rdx), %edi
	movl	%edi, %esi
	sall	$21, %esi
	andl	$266338304, %esi
	orl	%esi, %ecx
	testb	%dil, %dil
	js	.L1464
	movl	$4, (%rbx)
	movl	$5, %edi
	movl	$5, %esi
	jmp	.L1454
.L1464:
	leaq	5(%rdx), %r15
	cmpq	%r15, %rax
	jbe	.L1434
	movzbl	5(%rdx), %edi
	movl	$5, (%rbx)
	movl	%edi, %r14d
	andl	$-16, %r14d
	testb	%dil, %dil
	jns	.L1435
	xorl	%eax, %eax
	leaq	.LC522(%rip), %rcx
	movq	%r15, %rsi
	movq	%r12, %rdi
	leaq	.LC487(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	testb	%r14b, %r14b
	je	.L1437
.L1436:
	leaq	.LC488(%rip), %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	jmp	.L1437
.L1434:
	movl	$4, (%rbx)
	movq	%r15, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC522(%rip), %rcx
	leaq	.LC487(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L1437
.L1435:
	sall	$28, %edi
	orl	%edi, %ecx
	testb	%r14b, %r14b
	jne	.L1436
	movl	$6, %edi
	movl	$6, %esi
	jmp	.L1454
	.cfi_endproc
.LFE23485:
	.size	_ZN2v88internal4wasm19SelectTypeImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKh, .-_ZN2v88internal4wasm19SelectTypeImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKh
	.weak	_ZN2v88internal4wasm19SelectTypeImmediateILNS1_7Decoder12ValidateFlagE1EEC1EPS3_PKh
	.set	_ZN2v88internal4wasm19SelectTypeImmediateILNS1_7Decoder12ValidateFlagE1EEC1EPS3_PKh,_ZN2v88internal4wasm19SelectTypeImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKh
	.section	.rodata._ZN2v88internal4wasm19BranchTableIteratorILNS1_7Decoder12ValidateFlagE1EE4nextEv.str1.1,"aMS",@progbits,1
.LC525:
	.string	"branch table entry"
	.section	.text._ZN2v88internal4wasm19BranchTableIteratorILNS1_7Decoder12ValidateFlagE1EE4nextEv,"axG",@progbits,_ZN2v88internal4wasm19BranchTableIteratorILNS1_7Decoder12ValidateFlagE1EE4nextEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm19BranchTableIteratorILNS1_7Decoder12ValidateFlagE1EE4nextEv
	.type	_ZN2v88internal4wasm19BranchTableIteratorILNS1_7Decoder12ValidateFlagE1EE4nextEv, @function
_ZN2v88internal4wasm19BranchTableIteratorILNS1_7Decoder12ValidateFlagE1EE4nextEv:
.LFB23526:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	addl	$1, 24(%rdi)
	movq	(%rdi), %rdi
	movq	16(%rbx), %rsi
	movq	24(%rdi), %rax
	cmpq	%rax, %rsi
	jb	.L1481
	leaq	.LC525(%rip), %rcx
	leaq	.LC487(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	16(%rbx), %r12
	xorl	%r9d, %r9d
.L1467:
	movq	%r12, 16(%rbx)
	addq	$16, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1481:
	.cfi_restore_state
	movzbl	(%rsi), %edx
	leaq	1(%rsi), %r12
	movl	%edx, %r9d
	andl	$127, %r9d
	testb	%dl, %dl
	jns	.L1467
	cmpq	%r12, %rax
	jbe	.L1468
	movzbl	1(%rsi), %ecx
	leaq	2(%rsi), %r12
	movl	%ecx, %edx
	sall	$7, %edx
	andl	$16256, %edx
	orl	%edx, %r9d
	testb	%cl, %cl
	jns	.L1467
	leaq	2(%rsi), %r8
	cmpq	%r8, %rax
	jbe	.L1470
	movzbl	2(%rsi), %ecx
	leaq	3(%rsi), %r12
	movl	%ecx, %edx
	sall	$14, %edx
	andl	$2080768, %edx
	orl	%edx, %r9d
	testb	%cl, %cl
	jns	.L1467
	leaq	3(%rsi), %r8
	cmpq	%r8, %rax
	ja	.L1482
	xorl	%eax, %eax
	leaq	.LC525(%rip), %rcx
	leaq	.LC487(%rip), %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	16(%rbx), %rax
	xorl	%r9d, %r9d
	leaq	3(%rax), %r12
	jmp	.L1467
	.p2align 4,,10
	.p2align 3
.L1468:
	movq	%r12, %rsi
	xorl	%eax, %eax
	leaq	.LC525(%rip), %rcx
	leaq	.LC487(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	16(%rbx), %rax
	xorl	%r9d, %r9d
	leaq	1(%rax), %r12
	jmp	.L1467
	.p2align 4,,10
	.p2align 3
.L1470:
	xorl	%eax, %eax
	leaq	.LC525(%rip), %rcx
	leaq	.LC487(%rip), %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	16(%rbx), %rax
	xorl	%r9d, %r9d
	leaq	2(%rax), %r12
	jmp	.L1467
.L1482:
	movzbl	3(%rsi), %ecx
	leaq	4(%rsi), %r12
	movl	%ecx, %edx
	sall	$21, %edx
	andl	$266338304, %edx
	orl	%edx, %r9d
	testb	%cl, %cl
	jns	.L1467
	leaq	4(%rsi), %r13
	cmpq	%r13, %rax
	jbe	.L1477
	movzbl	4(%rsi), %r14d
	testb	%r14b, %r14b
	js	.L1478
	movl	%r14d, %eax
	movl	$5, %r12d
	sall	$28, %eax
	orl	%eax, %r9d
.L1475:
	andl	$240, %r14d
	jne	.L1476
	addq	16(%rbx), %r12
	jmp	.L1467
.L1477:
	xorl	%r14d, %r14d
	movl	$4, %r12d
.L1474:
	leaq	.LC525(%rip), %rcx
	movq	%r13, %rsi
	xorl	%eax, %eax
	movq	%rdi, -40(%rbp)
	leaq	.LC487(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-40(%rbp), %rdi
	xorl	%r9d, %r9d
	jmp	.L1475
.L1476:
	leaq	.LC488(%rip), %rdx
	movq	%r13, %rsi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	addq	16(%rbx), %r12
	xorl	%r9d, %r9d
	jmp	.L1467
.L1478:
	movl	$5, %r12d
	jmp	.L1474
	.cfi_endproc
.LFE23526:
	.size	_ZN2v88internal4wasm19BranchTableIteratorILNS1_7Decoder12ValidateFlagE1EE4nextEv, .-_ZN2v88internal4wasm19BranchTableIteratorILNS1_7Decoder12ValidateFlagE1EE4nextEv
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE28InitializeBrTableResultTypesEj,"axG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE28InitializeBrTableResultTypesEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE28InitializeBrTableResultTypesEj
	.type	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE28InitializeBrTableResultTypesEj, @function
_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE28InitializeBrTableResultTypesEj:
.LFB23530:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %edx
	leaq	(%rdx,%rdx,4), %rax
	leaq	(%rdx,%rax,2), %rdx
	movq	$-88, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	salq	$3, %rdx
	pushq	%r14
	subq	%rdx, %rax
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	addq	224(%rsi), %rax
	leaq	24(%rax), %rdx
	leaq	56(%rax), %rbx
	cmpb	$3, (%rax)
	cmove	%rdx, %rbx
	movslq	(%rbx), %r14
	testl	%r14d, %r14d
	js	.L1496
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rdi)
	movq	%rdi, %r12
	movups	%xmm0, (%rdi)
	testq	%r14, %r14
	je	.L1483
	movq	%r14, %rdi
	movq	%r14, %r13
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	leaq	(%rax,%r14), %r15
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	%r15, 16(%r12)
	call	memset@PLT
	movq	%r15, 8(%r12)
	movq	%rax, %rcx
	xorl	%eax, %eax
	jmp	.L1488
	.p2align 4,,10
	.p2align 3
.L1489:
	movq	%rax, %rdx
	salq	$4, %rdx
	addq	8(%rbx), %rdx
	movzbl	8(%rdx), %edx
	movb	%dl, (%rcx,%rax)
	addq	$1, %rax
	cmpl	%eax, %r13d
	jle	.L1483
.L1491:
	movq	(%r12), %rcx
.L1488:
	cmpl	$1, (%rbx)
	jne	.L1489
	movzbl	16(%rbx), %edx
	movb	%dl, (%rcx,%rax)
	addq	$1, %rax
	cmpl	%eax, %r13d
	jg	.L1491
.L1483:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1496:
	.cfi_restore_state
	leaq	.LC508(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE23530:
	.size	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE28InitializeBrTableResultTypesEj, .-_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE28InitializeBrTableResultTypesEj
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE24UpdateBrTableResultTypesEPSt6vectorINS1_9ValueTypeESaIS8_EEjPKhi.str1.8,"aMS",@progbits,1
	.align 8
.LC526:
	.string	"inconsistent arity in br_table target %u (previous was %zu, this one is %u)"
	.align 8
.LC527:
	.string	"inconsistent type in br_table target %u (previous was %s, this one is %s)"
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE24UpdateBrTableResultTypesEPSt6vectorINS1_9ValueTypeESaIS8_EEjPKhi,"axG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE24UpdateBrTableResultTypesEPSt6vectorINS1_9ValueTypeESaIS8_EEjPKhi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE24UpdateBrTableResultTypesEPSt6vectorINS1_9ValueTypeESaIS8_EEjPKhi
	.type	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE24UpdateBrTableResultTypesEPSt6vectorINS1_9ValueTypeESaIS8_EEjPKhi, @function
_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE24UpdateBrTableResultTypesEPSt6vectorINS1_9ValueTypeESaIS8_EEjPKhi:
.LFB23539:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %edx
	leaq	(%rdx,%rdx,4), %rax
	leaq	(%rdx,%rax,2), %rdx
	movq	$-88, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	salq	$3, %rdx
	pushq	%r14
	subq	%rdx, %rax
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%r8d, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	addq	224(%rdi), %rax
	movq	8(%rsi), %r8
	movq	%rcx, -56(%rbp)
	cmpb	$3, (%rax)
	leaq	24(%rax), %rdx
	leaq	56(%rax), %r11
	movq	(%rsi), %rax
	cmove	%rdx, %r11
	subq	%rax, %r8
	movl	(%r11), %r9d
	cmpl	%r8d, %r9d
	jne	.L1588
	testl	%r9d, %r9d
	jle	.L1502
	leal	-1(%r9), %ecx
	xorl	%edx, %edx
	leaq	8(%r11), %r14
	jmp	.L1545
	.p2align 4,,10
	.p2align 3
.L1589:
	movq	%r14, %r8
	cmpl	$1, %r9d
	je	.L1505
	movq	%rdx, %r8
	salq	$4, %r8
	addq	8(%r11), %r8
.L1505:
	movzbl	8(%r8), %r8d
	cmpb	%r10b, %r8b
	je	.L1506
	cmpb	$9, %r10b
	leal	-6(%r10), %r9d
	sete	%bl
	cmpb	$1, %r9b
	jbe	.L1557
	testb	%bl, %bl
	je	.L1547
.L1557:
	cmpb	$9, %r8b
	leal	-6(%r8), %r9d
	sete	%r12b
	cmpb	$1, %r9b
	jbe	.L1558
	testb	%r12b, %r12b
	je	.L1547
.L1558:
	cmpb	$6, %r8b
	sete	%r9b
	cmpb	$8, %r10b
	sete	%r15b
	testb	%r9b, %r9b
	je	.L1559
	testb	%r15b, %r15b
	je	.L1559
.L1551:
	movl	$8, %r10d
.L1506:
	movb	%r10b, (%rax)
.L1516:
	leaq	1(%rdx), %r8
	cmpq	%rdx, %rcx
	je	.L1502
	movq	(%rsi), %rax
	movl	(%r11), %r9d
	movq	%r8, %rdx
.L1545:
	movzbl	95(%rdi), %r12d
	addq	%rdx, %rax
	movzbl	(%rax), %r10d
	testb	%r12b, %r12b
	jne	.L1589
	movq	%r14, %rax
	cmpl	$1, %r9d
	je	.L1518
	movq	%rdx, %rax
	salq	$4, %rax
	addq	8(%r11), %rax
.L1518:
	movzbl	8(%rax), %eax
	cmpb	%r10b, %al
	je	.L1516
	cmpb	$10, %al
	ja	.L1519
	leaq	.L1521(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE24UpdateBrTableResultTypesEPSt6vectorINS1_9ValueTypeESaIS8_EEjPKhi,"aG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE24UpdateBrTableResultTypesEPSt6vectorINS1_9ValueTypeESaIS8_EEjPKhi,comdat
	.align 4
	.align 4
.L1521:
	.long	.L1531-.L1521
	.long	.L1555-.L1521
	.long	.L1529-.L1521
	.long	.L1528-.L1521
	.long	.L1527-.L1521
	.long	.L1526-.L1521
	.long	.L1525-.L1521
	.long	.L1524-.L1521
	.long	.L1523-.L1521
	.long	.L1522-.L1521
	.long	.L1520-.L1521
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE24UpdateBrTableResultTypesEPSt6vectorINS1_9ValueTypeESaIS8_EEjPKhi,"axG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE24UpdateBrTableResultTypesEPSt6vectorINS1_9ValueTypeESaIS8_EEjPKhi,comdat
	.p2align 4,,10
	.p2align 3
.L1547:
	movl	$10, %r10d
	jmp	.L1506
	.p2align 4,,10
	.p2align 3
.L1559:
	cmpb	$7, %r10b
	jne	.L1560
	testb	%r9b, %r9b
	je	.L1560
	movl	$7, %r10d
	jmp	.L1506
	.p2align 4,,10
	.p2align 3
.L1560:
	testb	%bl, %bl
	je	.L1561
	testb	%r9b, %r9b
	je	.L1561
	movl	$9, %r10d
	jmp	.L1506
	.p2align 4,,10
	.p2align 3
.L1502:
	movl	$1, %r12d
.L1497:
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1588:
	.cfi_restore_state
	movq	%rcx, %rsi
	leaq	.LC526(%rip), %rdx
	movl	%r13d, %ecx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	xorl	%r12d, %r12d
	jmp	.L1497
	.p2align 4,,10
	.p2align 3
.L1561:
	cmpb	$7, %r8b
	leal	-7(%r8), %r9d
	sete	%bl
	andl	$253, %r9d
	jne	.L1562
	testb	%r15b, %r15b
	jne	.L1551
.L1562:
	cmpb	$6, %r10b
	sete	%r9b
	cmpb	$8, %r8b
	jne	.L1563
	movl	$8, %r10d
	testb	%r9b, %r9b
	jne	.L1506
.L1563:
	testb	%bl, %bl
	je	.L1564
	movl	$7, %r10d
	testb	%r9b, %r9b
	jne	.L1506
.L1564:
	testb	%r12b, %r12b
	je	.L1551
	movl	$9, %r10d
	testb	%r9b, %r9b
	jne	.L1506
	jmp	.L1551
.L1529:
	leaq	.LC500(%rip), %r9
	.p2align 4,,10
	.p2align 3
.L1530:
	cmpb	$10, %r10b
	ja	.L1532
	leaq	.L1534(%rip), %rdx
	movslq	(%rdx,%r10,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE24UpdateBrTableResultTypesEPSt6vectorINS1_9ValueTypeESaIS8_EEjPKhi,"aG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE24UpdateBrTableResultTypesEPSt6vectorINS1_9ValueTypeESaIS8_EEjPKhi,comdat
	.align 4
	.align 4
.L1534:
	.long	.L1544-.L1534
	.long	.L1556-.L1534
	.long	.L1542-.L1534
	.long	.L1541-.L1534
	.long	.L1540-.L1534
	.long	.L1539-.L1534
	.long	.L1538-.L1534
	.long	.L1537-.L1534
	.long	.L1536-.L1534
	.long	.L1535-.L1534
	.long	.L1533-.L1534
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE24UpdateBrTableResultTypesEPSt6vectorINS1_9ValueTypeESaIS8_EEjPKhi,"axG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE24UpdateBrTableResultTypesEPSt6vectorINS1_9ValueTypeESaIS8_EEjPKhi,comdat
.L1542:
	leaq	.LC500(%rip), %r8
	.p2align 4,,10
	.p2align 3
.L1543:
	movq	-56(%rbp), %rsi
	movl	%r13d, %ecx
	leaq	.LC527(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L1497
.L1556:
	leaq	.LC490(%rip), %r8
	jmp	.L1543
.L1555:
	leaq	.LC490(%rip), %r9
	jmp	.L1530
.L1533:
	leaq	.LC499(%rip), %r8
	jmp	.L1543
.L1528:
	leaq	.LC491(%rip), %r9
	jmp	.L1530
.L1527:
	leaq	.LC492(%rip), %r9
	jmp	.L1530
.L1544:
	leaq	.LC498(%rip), %r8
	jmp	.L1543
.L1531:
	leaq	.LC498(%rip), %r9
	jmp	.L1530
.L1522:
	leaq	.LC496(%rip), %r9
	jmp	.L1530
.L1520:
	leaq	.LC499(%rip), %r9
	jmp	.L1530
.L1526:
	leaq	.LC497(%rip), %r9
	jmp	.L1530
.L1525:
	leaq	.LC493(%rip), %r9
	jmp	.L1530
.L1524:
	leaq	.LC494(%rip), %r9
	jmp	.L1530
.L1523:
	leaq	.LC495(%rip), %r9
	jmp	.L1530
.L1535:
	leaq	.LC496(%rip), %r8
	jmp	.L1543
.L1536:
	leaq	.LC495(%rip), %r8
	jmp	.L1543
.L1537:
	leaq	.LC494(%rip), %r8
	jmp	.L1543
.L1538:
	leaq	.LC493(%rip), %r8
	jmp	.L1543
.L1539:
	leaq	.LC497(%rip), %r8
	jmp	.L1543
.L1540:
	leaq	.LC492(%rip), %r8
	jmp	.L1543
.L1541:
	leaq	.LC491(%rip), %r8
	jmp	.L1543
.L1532:
	leaq	.LC489(%rip), %r8
	jmp	.L1543
.L1519:
	leaq	.LC489(%rip), %r9
	jmp	.L1530
	.cfi_endproc
.LFE23539:
	.size	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE24UpdateBrTableResultTypesEPSt6vectorINS1_9ValueTypeESaIS8_EEjPKhi, .-_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE24UpdateBrTableResultTypesEPSt6vectorINS1_9ValueTypeESaIS8_EEjPKhi
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16TypeCheckBrTableERKSt6vectorINS1_9ValueTypeESaIS8_EE.str1.8,"aMS",@progbits,1
	.align 8
.LC528:
	.string	"expected %u elements on the stack for branch to @%d, found %u"
	.align 8
.LC529:
	.string	"type error in merge[%u] (expected %s, got %s)"
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16TypeCheckBrTableERKSt6vectorINS1_9ValueTypeESaIS8_EE.str1.1,"aMS",@progbits,1
.LC530:
	.string	"%s found empty stack"
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16TypeCheckBrTableERKSt6vectorINS1_9ValueTypeESaIS8_EE.str1.8
	.align 8
.LC531:
	.string	"%s[%d] expected type %s, found %s of type %s"
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16TypeCheckBrTableERKSt6vectorINS1_9ValueTypeESaIS8_EE,"axG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16TypeCheckBrTableERKSt6vectorINS1_9ValueTypeESaIS8_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16TypeCheckBrTableERKSt6vectorINS1_9ValueTypeESaIS8_EE
	.type	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16TypeCheckBrTableERKSt6vectorINS1_9ValueTypeESaIS8_EE, @function
_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16TypeCheckBrTableERKSt6vectorINS1_9ValueTypeESaIS8_EE:
.LFB23540:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rsi
	movq	8(%r13), %rcx
	movq	224(%rdi), %rax
	subq	%rsi, %rcx
	cmpb	$0, -72(%rax)
	jne	.L1693
	movq	192(%rdi), %rdi
	movq	%rdi, %r9
	subq	184(%r12), %r9
	sarq	$4, %r9
	subl	-84(%rax), %r9d
	cmpl	%r9d, %ecx
	jg	.L1694
	movslq	%ecx, %rax
	testl	%ecx, %ecx
	jle	.L1592
	salq	$4, %rax
	leal	-1(%rcx), %r9d
	subq	%rax, %rdi
	xorl	%eax, %eax
	jmp	.L1624
	.p2align 4,,10
	.p2align 3
.L1695:
	movq	%rdx, %rax
.L1624:
	movq	%rax, %rcx
	movzbl	(%rsi,%rax), %edx
	movl	%eax, %r10d
	salq	$4, %rcx
	movzbl	8(%rdi,%rcx), %ecx
	cmpb	%cl, %dl
	je	.L1596
	leal	-7(%rcx), %r8d
	cmpb	$2, %r8b
	ja	.L1671
	cmpb	$6, %dl
	jne	.L1671
.L1596:
	leaq	1(%rax), %rdx
	cmpq	%r9, %rax
	jne	.L1695
.L1592:
	cmpq	$0, 56(%r12)
	sete	%r13b
.L1590:
	leaq	-40(%rbp), %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1671:
	.cfi_restore_state
	leal	-7(%rdx), %r8d
	andl	$253, %r8d
	sete	%r8b
	cmpb	$8, %cl
	sete	%r11b
	andb	%r11b, %r8b
	jne	.L1596
	movl	%r8d, %r13d
	cmpb	$10, %cl
	ja	.L1598
	leaq	.L1600(%rip), %rsi
	movslq	(%rsi,%rcx,4), %rax
	addq	%rsi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16TypeCheckBrTableERKSt6vectorINS1_9ValueTypeESaIS8_EE,"aG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16TypeCheckBrTableERKSt6vectorINS1_9ValueTypeESaIS8_EE,comdat
	.align 4
	.align 4
.L1600:
	.long	.L1610-.L1600
	.long	.L1664-.L1600
	.long	.L1608-.L1600
	.long	.L1607-.L1600
	.long	.L1606-.L1600
	.long	.L1605-.L1600
	.long	.L1604-.L1600
	.long	.L1603-.L1600
	.long	.L1602-.L1600
	.long	.L1601-.L1600
	.long	.L1599-.L1600
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16TypeCheckBrTableERKSt6vectorINS1_9ValueTypeESaIS8_EE,"axG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16TypeCheckBrTableERKSt6vectorINS1_9ValueTypeESaIS8_EE,comdat
	.p2align 4,,10
	.p2align 3
.L1694:
	movq	16(%r12), %rsi
	movq	-80(%rax), %r8
	movq	%r12, %rdi
	xorl	%eax, %eax
	subl	8(%r12), %r8d
	leaq	.LC528(%rip), %rdx
	xorl	%r13d, %r13d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L1590
	.p2align 4,,10
	.p2align 3
.L1693:
	testl	%ecx, %ecx
	jle	.L1592
	leal	-1(%rcx), %ebx
	xorl	%r14d, %r14d
	jmp	.L1593
	.p2align 4,,10
	.p2align 3
.L1697:
	cmpb	$2, -72(%rax)
	movq	16(%r12), %r15
	jne	.L1696
.L1663:
	leaq	1(%r14), %rdx
	cmpq	%r14, %rbx
	je	.L1592
	movq	0(%r13), %rsi
	movq	224(%r12), %rax
	movq	%rdx, %r14
.L1593:
	movq	192(%r12), %rdx
	movzbl	(%rsi,%r14), %r9d
	leal	1(%r14), %r8d
	movl	-84(%rax), %esi
	movq	%rdx, %rcx
	subq	184(%r12), %rcx
	sarq	$4, %rcx
	cmpq	%rcx, %rsi
	jnb	.L1697
	movzbl	-8(%rdx), %eax
	movq	-16(%rdx), %r15
	subq	$16, %rdx
	movq	%rdx, 192(%r12)
	cmpb	%al, %r9b
	je	.L1663
	cmpb	$6, %r9b
	sete	%dl
	cmpb	$8, %al
	sete	%cl
	testb	%dl, %dl
	je	.L1672
	testb	%cl, %cl
	jne	.L1663
.L1672:
	leal	-7(%rax), %edi
	andl	$253, %edi
	jne	.L1673
	testb	%dl, %dl
	jne	.L1663
.L1673:
	leal	-7(%r9), %edx
	andb	$-3, %dl
	sete	%dl
	testb	%cl, %dl
	jne	.L1663
	cmpb	$10, %al
	setne	%cl
	cmpb	$10, %r9b
	setne	%dl
	testb	%dl, %cl
	je	.L1663
	cmpb	$9, %al
	ja	.L1633
	leaq	.L1635(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16TypeCheckBrTableERKSt6vectorINS1_9ValueTypeESaIS8_EE,"aG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16TypeCheckBrTableERKSt6vectorINS1_9ValueTypeESaIS8_EE,comdat
	.align 4
	.align 4
.L1635:
	.long	.L1644-.L1635
	.long	.L1667-.L1635
	.long	.L1642-.L1635
	.long	.L1641-.L1635
	.long	.L1640-.L1635
	.long	.L1639-.L1635
	.long	.L1638-.L1635
	.long	.L1637-.L1635
	.long	.L1636-.L1635
	.long	.L1634-.L1635
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16TypeCheckBrTableERKSt6vectorINS1_9ValueTypeESaIS8_EE,"axG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16TypeCheckBrTableERKSt6vectorINS1_9ValueTypeESaIS8_EE,comdat
	.p2align 4,,10
	.p2align 3
.L1696:
	leaq	.LC482(%rip), %rcx
	cmpq	%r15, 24(%r12)
	jbe	.L1627
	movzbl	(%r15), %edi
	movl	%edi, -56(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	-56(%rbp), %edi
	testb	%al, %al
	je	.L1692
	leaq	1(%r15), %rax
	cmpq	%rax, 24(%r12)
	ja	.L1629
	movq	16(%r12), %r15
	leaq	.LC482(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L1627:
	leaq	.LC530(%rip), %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L1663
.L1629:
	movzbl	1(%r15), %eax
	sall	$8, %edi
	orl	%eax, %edi
.L1692:
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	16(%r12), %r15
	movq	%rax, %rcx
	jmp	.L1627
.L1601:
	leaq	.LC496(%rip), %r9
	.p2align 4,,10
	.p2align 3
.L1609:
	cmpb	$10, %dl
	ja	.L1611
	leaq	.L1613(%rip), %rcx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16TypeCheckBrTableERKSt6vectorINS1_9ValueTypeESaIS8_EE,"aG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16TypeCheckBrTableERKSt6vectorINS1_9ValueTypeESaIS8_EE,comdat
	.align 4
	.align 4
.L1613:
	.long	.L1623-.L1613
	.long	.L1665-.L1613
	.long	.L1621-.L1613
	.long	.L1620-.L1613
	.long	.L1619-.L1613
	.long	.L1618-.L1613
	.long	.L1617-.L1613
	.long	.L1616-.L1613
	.long	.L1615-.L1613
	.long	.L1614-.L1613
	.long	.L1612-.L1613
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16TypeCheckBrTableERKSt6vectorINS1_9ValueTypeESaIS8_EE,"axG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16TypeCheckBrTableERKSt6vectorINS1_9ValueTypeESaIS8_EE,comdat
.L1621:
	leaq	.LC500(%rip), %r8
	.p2align 4,,10
	.p2align 3
.L1622:
	movq	16(%r12), %rsi
	movl	%r10d, %ecx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC529(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L1590
.L1665:
	leaq	.LC490(%rip), %r8
	jmp	.L1622
.L1623:
	leaq	.LC498(%rip), %r8
	jmp	.L1622
.L1612:
	leaq	.LC499(%rip), %r8
	jmp	.L1622
.L1614:
	leaq	.LC496(%rip), %r8
	jmp	.L1622
.L1615:
	leaq	.LC495(%rip), %r8
	jmp	.L1622
.L1616:
	leaq	.LC494(%rip), %r8
	jmp	.L1622
.L1617:
	leaq	.LC493(%rip), %r8
	jmp	.L1622
.L1618:
	leaq	.LC497(%rip), %r8
	jmp	.L1622
.L1619:
	leaq	.LC492(%rip), %r8
	jmp	.L1622
.L1620:
	leaq	.LC491(%rip), %r8
	jmp	.L1622
.L1602:
	leaq	.LC495(%rip), %r9
	jmp	.L1609
.L1603:
	leaq	.LC494(%rip), %r9
	jmp	.L1609
.L1604:
	leaq	.LC493(%rip), %r9
	jmp	.L1609
.L1605:
	leaq	.LC497(%rip), %r9
	jmp	.L1609
.L1606:
	leaq	.LC492(%rip), %r9
	jmp	.L1609
.L1607:
	leaq	.LC491(%rip), %r9
	jmp	.L1609
.L1610:
	leaq	.LC498(%rip), %r9
	jmp	.L1609
.L1599:
	leaq	.LC499(%rip), %r9
	jmp	.L1609
.L1664:
	leaq	.LC490(%rip), %r9
	jmp	.L1609
.L1608:
	leaq	.LC500(%rip), %r9
	jmp	.L1609
.L1598:
	leaq	.LC489(%rip), %r9
	jmp	.L1609
.L1611:
	leaq	.LC489(%rip), %r8
	jmp	.L1622
.L1642:
	leaq	.LC500(%rip), %rax
	movq	%rax, -56(%rbp)
	.p2align 4,,10
	.p2align 3
.L1643:
	movq	24(%r12), %rcx
	leaq	.LC482(%rip), %rdx
	cmpq	%rcx, %r15
	jnb	.L1645
	movzbl	(%r15), %edi
	movl	%r8d, -80(%rbp)
	movb	%r9b, -72(%rbp)
	movl	%edi, -64(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	-64(%rbp), %edi
	movzbl	-72(%rbp), %r9d
	testb	%al, %al
	movl	-80(%rbp), %r8d
	je	.L1698
	movq	24(%r12), %rcx
	leaq	1(%r15), %rax
	leaq	.LC482(%rip), %rdx
	cmpq	%rax, %rcx
	jbe	.L1645
	movzbl	1(%r15), %eax
	sall	$8, %edi
	movl	%r8d, -72(%rbp)
	movb	%r9b, -64(%rbp)
	orl	%eax, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	24(%r12), %rcx
	movl	-72(%rbp), %r8d
	movzbl	-64(%rbp), %r9d
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L1645:
	cmpb	$9, %r9b
	ja	.L1647
	leaq	.L1649(%rip), %rdi
	movslq	(%rdi,%r9,4), %rax
	addq	%rdi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16TypeCheckBrTableERKSt6vectorINS1_9ValueTypeESaIS8_EE,"aG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16TypeCheckBrTableERKSt6vectorINS1_9ValueTypeESaIS8_EE,comdat
	.align 4
	.align 4
.L1649:
	.long	.L1658-.L1649
	.long	.L1670-.L1649
	.long	.L1656-.L1649
	.long	.L1655-.L1649
	.long	.L1654-.L1649
	.long	.L1653-.L1649
	.long	.L1652-.L1649
	.long	.L1651-.L1649
	.long	.L1650-.L1649
	.long	.L1648-.L1649
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16TypeCheckBrTableERKSt6vectorINS1_9ValueTypeESaIS8_EE,"axG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16TypeCheckBrTableERKSt6vectorINS1_9ValueTypeESaIS8_EE,comdat
.L1656:
	leaq	.LC500(%rip), %r9
	.p2align 4,,10
	.p2align 3
.L1657:
	movq	16(%r12), %r10
	cmpq	%rcx, %r10
	jnb	.L1662
	movzbl	(%r10), %edi
	movl	%r8d, -92(%rbp)
	movq	%r9, -88(%rbp)
	movq	%rdx, -80(%rbp)
	movq	%r10, -72(%rbp)
	movl	%edi, -64(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	-64(%rbp), %edi
	movq	-72(%rbp), %r10
	testb	%al, %al
	movq	-80(%rbp), %rdx
	movq	-88(%rbp), %r9
	movl	-92(%rbp), %r8d
	je	.L1699
	leaq	1(%r10), %rax
	cmpq	%rax, 24(%r12)
	jbe	.L1662
	movzbl	1(%r10), %eax
	sall	$8, %edi
	movl	%r8d, -80(%rbp)
	movq	%r9, -72(%rbp)
	orl	%eax, %edi
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movl	-80(%rbp), %r8d
	movq	-72(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L1660:
	pushq	-56(%rbp)
	xorl	%eax, %eax
	movq	%r15, %rsi
	movq	%r12, %rdi
	pushq	%rdx
	leaq	.LC531(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	popq	%rax
	popq	%rdx
	jmp	.L1663
.L1670:
	leaq	.LC490(%rip), %r9
	jmp	.L1657
.L1667:
	leaq	.LC490(%rip), %rax
	movq	%rax, -56(%rbp)
	jmp	.L1643
.L1644:
	leaq	.LC498(%rip), %rax
	movq	%rax, -56(%rbp)
	jmp	.L1643
.L1658:
	leaq	.LC498(%rip), %r9
	jmp	.L1657
.L1648:
	leaq	.LC496(%rip), %r9
	jmp	.L1657
.L1650:
	leaq	.LC495(%rip), %r9
	jmp	.L1657
.L1651:
	leaq	.LC494(%rip), %r9
	jmp	.L1657
.L1652:
	leaq	.LC493(%rip), %r9
	jmp	.L1657
.L1653:
	leaq	.LC497(%rip), %r9
	jmp	.L1657
.L1654:
	leaq	.LC492(%rip), %r9
	jmp	.L1657
.L1655:
	leaq	.LC491(%rip), %r9
	jmp	.L1657
.L1634:
	leaq	.LC496(%rip), %rax
	movq	%rax, -56(%rbp)
	jmp	.L1643
.L1636:
	leaq	.LC495(%rip), %rax
	movq	%rax, -56(%rbp)
	jmp	.L1643
.L1637:
	leaq	.LC494(%rip), %rax
	movq	%rax, -56(%rbp)
	jmp	.L1643
.L1638:
	leaq	.LC493(%rip), %rax
	movq	%rax, -56(%rbp)
	jmp	.L1643
.L1639:
	leaq	.LC497(%rip), %rax
	movq	%rax, -56(%rbp)
	jmp	.L1643
.L1640:
	leaq	.LC492(%rip), %rax
	movq	%rax, -56(%rbp)
	jmp	.L1643
.L1641:
	leaq	.LC491(%rip), %rax
	movq	%rax, -56(%rbp)
	jmp	.L1643
.L1662:
	leaq	.LC482(%rip), %rcx
	jmp	.L1660
.L1699:
	movl	%r8d, -80(%rbp)
	movq	%r9, -72(%rbp)
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r9
	movl	-80(%rbp), %r8d
	movq	%rax, %rcx
	jmp	.L1660
.L1698:
	movl	%r8d, -72(%rbp)
	movb	%r9b, -64(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	24(%r12), %rcx
	movzbl	-64(%rbp), %r9d
	movl	-72(%rbp), %r8d
	movq	%rax, %rdx
	jmp	.L1645
.L1633:
	leaq	.LC489(%rip), %rax
	movq	%rax, -56(%rbp)
	jmp	.L1643
.L1647:
	leaq	.LC489(%rip), %r9
	jmp	.L1657
	.cfi_endproc
.LFE23540:
	.size	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16TypeCheckBrTableERKSt6vectorINS1_9ValueTypeESaIS8_EE, .-_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16TypeCheckBrTableERKSt6vectorINS1_9ValueTypeESaIS8_EE
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE14DecodeStoreMemENS1_9StoreTypeEi.str1.1,"aMS",@progbits,1
.LC532:
	.string	"alignment"
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE14DecodeStoreMemENS1_9StoreTypeEi.str1.8,"aMS",@progbits,1
	.align 8
.LC533:
	.string	"invalid alignment; expected maximum alignment is %u, actual alignment is %u"
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE14DecodeStoreMemENS1_9StoreTypeEi.str1.1
.LC534:
	.string	"offset"
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE14DecodeStoreMemENS1_9StoreTypeEi.str1.8
	.align 8
.LC535:
	.string	"memory instruction with no memory"
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE14DecodeStoreMemENS1_9StoreTypeEi,"axG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE14DecodeStoreMemENS1_9StoreTypeEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE14DecodeStoreMemENS1_9StoreTypeEi
	.type	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE14DecodeStoreMemENS1_9StoreTypeEi, @function
_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE14DecodeStoreMemENS1_9StoreTypeEi:
.LFB23576:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	80(%rdi), %rcx
	movq	16(%rdi), %rax
	cmpb	$0, 18(%rcx)
	je	.L1701
	movslq	%edx, %rdx
	movzbl	%sil, %ebx
	leaq	_ZN2v88internal4wasm9StoreType14kStoreSizeLog2E(%rip), %rcx
	addq	%rax, %rdx
	movq	24(%rdi), %rax
	movzbl	(%rcx,%rbx), %ecx
	leaq	1(%rdx), %r15
	cmpq	%rax, %r15
	jnb	.L1702
	movzbl	1(%rdx), %esi
	leaq	2(%rdx), %r14
	movl	$1, %r13d
	movl	%esi, %r8d
	andl	$127, %r8d
	testb	%sil, %sil
	js	.L1836
.L1706:
	cmpl	%r8d, %ecx
	jnb	.L1714
	xorl	%eax, %eax
	leaq	.LC533(%rip), %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%r12), %rax
.L1714:
	cmpq	%rax, %r14
	jb	.L1837
.L1715:
	leaq	.LC534(%rip), %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC487(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
.L1717:
	leaq	_ZN2v88internal4wasm9StoreType10kValueTypeE(%rip), %rax
	movq	224(%r12), %rcx
	movzbl	(%rax,%rbx), %ebx
	movq	192(%r12), %rax
	movl	-84(%rcx), %esi
	movq	%rax, %rdx
	subq	184(%r12), %rdx
	sarq	$4, %rdx
	cmpq	%rdx, %rsi
	jb	.L1838
	cmpb	$2, -72(%rcx)
	movq	16(%r12), %r14
	jne	.L1839
.L1733:
	movq	224(%r12), %rcx
	movq	%rax, %rdx
	subq	184(%r12), %rdx
	sarq	$4, %rdx
	movl	-84(%rcx), %esi
	cmpq	%rdx, %rsi
	jb	.L1765
	cmpb	$2, -72(%rcx)
	jne	.L1840
.L1767:
	leaq	-40(%rbp), %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1702:
	.cfi_restore_state
	xorl	%eax, %eax
	leaq	.LC532(%rip), %rcx
	movq	%r15, %rsi
	movq	%r15, %r14
	leaq	.LC487(%rip), %rdx
	xorl	%r13d, %r13d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%r12), %rax
	cmpq	%rax, %r14
	jnb	.L1715
.L1837:
	cmpb	$0, (%r14)
	js	.L1716
.L1827:
	addl	$1, %r13d
	jmp	.L1717
	.p2align 4,,10
	.p2align 3
.L1765:
	movzbl	-8(%rax), %edx
	movq	-16(%rax), %r14
	subq	$16, %rax
	movq	%rax, 192(%r12)
	cmpb	$1, %dl
	je	.L1767
	cmpb	$10, %dl
	je	.L1767
	cmpb	$9, %dl
	ja	.L1772
	leaq	.L1774(%rip), %rcx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE14DecodeStoreMemENS1_9StoreTypeEi,"aG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE14DecodeStoreMemENS1_9StoreTypeEi,comdat
	.align 4
	.align 4
.L1774:
	.long	.L1783-.L1774
	.long	.L1799-.L1774
	.long	.L1781-.L1774
	.long	.L1780-.L1774
	.long	.L1779-.L1774
	.long	.L1778-.L1774
	.long	.L1777-.L1774
	.long	.L1776-.L1774
	.long	.L1775-.L1774
	.long	.L1773-.L1774
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE14DecodeStoreMemENS1_9StoreTypeEi,"axG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE14DecodeStoreMemENS1_9StoreTypeEi,comdat
.L1781:
	leaq	.LC500(%rip), %rbx
.L1782:
	movq	24(%r12), %rax
	leaq	.LC482(%rip), %r15
	cmpq	%rax, %r14
	jb	.L1841
.L1784:
	movq	16(%r12), %rdx
	cmpq	%rax, %rdx
	jnb	.L1789
	movzbl	(%rdx), %edi
	movq	%rdx, -64(%rbp)
	movl	%edi, -56(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	-56(%rbp), %edi
	movq	-64(%rbp), %rdx
	testb	%al, %al
	je	.L1835
	leaq	1(%rdx), %rax
	cmpq	%rax, 24(%r12)
	ja	.L1842
.L1789:
	leaq	.LC482(%rip), %rcx
.L1787:
	pushq	%rbx
	leaq	.LC531(%rip), %rdx
	xorl	%eax, %eax
	xorl	%r8d, %r8d
	pushq	%r15
	leaq	.LC490(%rip), %r9
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	popq	%rax
	popq	%rdx
	jmp	.L1767
.L1799:
	leaq	.LC490(%rip), %rbx
	jmp	.L1782
	.p2align 4,,10
	.p2align 3
.L1838:
	movzbl	-8(%rax), %edx
	movq	-16(%rax), %r14
	subq	$16, %rax
	movq	%rax, 192(%r12)
	cmpb	%bl, %dl
	je	.L1733
	leal	-7(%rdx), %ecx
	cmpb	$2, %cl
	ja	.L1802
	cmpb	$6, %bl
	je	.L1733
.L1802:
	leal	-7(%rbx), %eax
	testb	$-3, %al
	sete	%al
	cmpb	$8, %dl
	sete	%cl
	andl	%ecx, %eax
	xorl	$1, %eax
	cmpb	$10, %dl
	setne	%cl
	testb	%cl, %al
	je	.L1832
	cmpb	$10, %bl
	je	.L1832
	cmpb	$9, %dl
	ja	.L1735
	leaq	.L1737(%rip), %rcx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE14DecodeStoreMemENS1_9StoreTypeEi,"aG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE14DecodeStoreMemENS1_9StoreTypeEi,comdat
	.align 4
	.align 4
.L1737:
	.long	.L1746-.L1737
	.long	.L1794-.L1737
	.long	.L1744-.L1737
	.long	.L1743-.L1737
	.long	.L1742-.L1737
	.long	.L1741-.L1737
	.long	.L1740-.L1737
	.long	.L1739-.L1737
	.long	.L1738-.L1737
	.long	.L1736-.L1737
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE14DecodeStoreMemENS1_9StoreTypeEi,"axG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE14DecodeStoreMemENS1_9StoreTypeEi,comdat
	.p2align 4,,10
	.p2align 3
.L1836:
	cmpq	%r14, %rax
	jbe	.L1704
	movzbl	2(%rdx), %edi
	leaq	3(%rdx), %r14
	movl	$2, %r13d
	movl	%edi, %esi
	sall	$7, %esi
	andl	$16256, %esi
	orl	%esi, %r8d
	testb	%dil, %dil
	jns	.L1706
	cmpq	%r14, %rax
	jbe	.L1707
	movzbl	3(%rdx), %edi
	leaq	4(%rdx), %r14
	movl	$3, %r13d
	movl	%edi, %esi
	sall	$14, %esi
	andl	$2080768, %esi
	orl	%esi, %r8d
	testb	%dil, %dil
	jns	.L1706
	cmpq	%r14, %rax
	ja	.L1843
	xorl	%eax, %eax
	leaq	.LC532(%rip), %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	.LC487(%rip), %rdx
	movl	$3, %r13d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%r12), %rax
	jmp	.L1714
	.p2align 4,,10
	.p2align 3
.L1716:
	leaq	1(%r14), %rsi
	cmpq	%rax, %rsi
	jnb	.L1718
	cmpb	$0, 1(%r14)
	js	.L1719
.L1828:
	addl	$2, %r13d
	jmp	.L1717
	.p2align 4,,10
	.p2align 3
.L1701:
	leaq	-1(%rax), %rsi
	leaq	.LC535(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	leaq	-40(%rbp), %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1704:
	.cfi_restore_state
	xorl	%eax, %eax
	leaq	.LC532(%rip), %rcx
	leaq	.LC487(%rip), %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%r12), %rax
	jmp	.L1714
	.p2align 4,,10
	.p2align 3
.L1718:
	leaq	.LC534(%rip), %rcx
	leaq	.LC487(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L1827
	.p2align 4,,10
	.p2align 3
.L1841:
	movzbl	(%r14), %edi
	movl	%edi, -56(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	-56(%rbp), %edi
	testb	%al, %al
	je	.L1834
	movq	24(%r12), %rax
	leaq	1(%r14), %rdx
	cmpq	%rdx, %rax
	jbe	.L1784
	movzbl	1(%r14), %eax
	sall	$8, %edi
	orl	%eax, %edi
.L1834:
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	%rax, %r15
	movq	24(%r12), %rax
	jmp	.L1784
	.p2align 4,,10
	.p2align 3
.L1840:
	movq	16(%r12), %r14
	leaq	.LC482(%rip), %r15
	cmpq	24(%r12), %r14
	jnb	.L1768
	movzbl	(%r14), %ebx
	movl	%ebx, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	testb	%al, %al
	je	.L1844
	leaq	1(%r14), %rax
	cmpq	%rax, 24(%r12)
	jbe	.L1833
	movzbl	1(%r14), %edi
	sall	$8, %ebx
	orl	%ebx, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	%rax, %r15
.L1833:
	movq	16(%r12), %r14
.L1768:
	movq	%r15, %rcx
	leaq	.LC530(%rip), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L1767
	.p2align 4,,10
	.p2align 3
.L1839:
	leaq	.LC482(%rip), %r15
	cmpq	%r14, 24(%r12)
	jbe	.L1730
	movzbl	(%r14), %ebx
	movl	%ebx, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	testb	%al, %al
	je	.L1845
	leaq	1(%r14), %rax
	cmpq	%rax, 24(%r12)
	jbe	.L1830
	movzbl	1(%r14), %edi
	sall	$8, %ebx
	orl	%ebx, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	%rax, %r15
.L1830:
	movq	16(%r12), %r14
.L1730:
	xorl	%eax, %eax
	movq	%r15, %rcx
	leaq	.LC530(%rip), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	192(%r12), %rax
	jmp	.L1733
	.p2align 4,,10
	.p2align 3
.L1842:
	movzbl	1(%rdx), %eax
	sall	$8, %edi
	orl	%eax, %edi
.L1835:
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	%rax, %rcx
	jmp	.L1787
	.p2align 4,,10
	.p2align 3
.L1719:
	leaq	2(%r14), %rsi
	cmpq	%rax, %rsi
	jnb	.L1720
	cmpb	$0, 2(%r14)
	js	.L1721
.L1829:
	addl	$3, %r13d
	jmp	.L1717
.L1775:
	leaq	.LC495(%rip), %rbx
	jmp	.L1782
.L1773:
	leaq	.LC496(%rip), %rbx
	jmp	.L1782
.L1783:
	leaq	.LC498(%rip), %rbx
	jmp	.L1782
.L1780:
	leaq	.LC491(%rip), %rbx
	jmp	.L1782
.L1779:
	leaq	.LC492(%rip), %rbx
	jmp	.L1782
.L1778:
	leaq	.LC497(%rip), %rbx
	jmp	.L1782
.L1777:
	leaq	.LC493(%rip), %rbx
	jmp	.L1782
.L1776:
	leaq	.LC494(%rip), %rbx
	jmp	.L1782
	.p2align 4,,10
	.p2align 3
.L1707:
	xorl	%eax, %eax
	leaq	.LC532(%rip), %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	.LC487(%rip), %rdx
	movl	$2, %r13d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%r12), %rax
	jmp	.L1714
	.p2align 4,,10
	.p2align 3
.L1720:
	leaq	.LC534(%rip), %rcx
	leaq	.LC487(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L1828
	.p2align 4,,10
	.p2align 3
.L1844:
	movl	%ebx, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	16(%r12), %r14
	movq	%rax, %r15
	jmp	.L1768
	.p2align 4,,10
	.p2align 3
.L1845:
	movl	%ebx, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	16(%r12), %r14
	movq	%rax, %r15
	jmp	.L1730
.L1772:
	leaq	.LC489(%rip), %rbx
	jmp	.L1782
	.p2align 4,,10
	.p2align 3
.L1721:
	leaq	3(%r14), %rsi
	cmpq	%rax, %rsi
	jb	.L1846
	leaq	.LC534(%rip), %rcx
	leaq	.LC487(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L1829
.L1843:
	movzbl	4(%rdx), %edi
	movl	%edi, %esi
	sall	$21, %esi
	andl	$266338304, %esi
	orl	%esi, %r8d
	testb	%dil, %dil
	js	.L1710
	leaq	5(%rdx), %r14
	movl	$4, %r13d
	jmp	.L1706
.L1846:
	cmpb	$0, 3(%r14)
	js	.L1723
	addl	$4, %r13d
	jmp	.L1717
.L1744:
	leaq	.LC500(%rip), %r15
.L1745:
	movq	24(%r12), %rcx
	leaq	.LC482(%rip), %rax
	movq	%rax, -56(%rbp)
	cmpq	%rcx, %r14
	jnb	.L1747
	movzbl	(%r14), %edi
	movl	%edi, -64(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	-64(%rbp), %edi
	testb	%al, %al
	je	.L1831
	movq	24(%r12), %rcx
	leaq	1(%r14), %rax
	cmpq	%rax, %rcx
	jbe	.L1747
	movzbl	1(%r14), %eax
	sall	$8, %edi
	orl	%eax, %edi
.L1831:
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	24(%r12), %rcx
	movq	%rax, -56(%rbp)
.L1747:
	cmpb	$9, %bl
	ja	.L1749
	leaq	.L1751(%rip), %rsi
	movslq	(%rsi,%rbx,4), %rax
	addq	%rsi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE14DecodeStoreMemENS1_9StoreTypeEi,"aG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE14DecodeStoreMemENS1_9StoreTypeEi,comdat
	.align 4
	.align 4
.L1751:
	.long	.L1760-.L1751
	.long	.L1797-.L1751
	.long	.L1758-.L1751
	.long	.L1757-.L1751
	.long	.L1756-.L1751
	.long	.L1755-.L1751
	.long	.L1754-.L1751
	.long	.L1753-.L1751
	.long	.L1752-.L1751
	.long	.L1750-.L1751
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE14DecodeStoreMemENS1_9StoreTypeEi,"axG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE14DecodeStoreMemENS1_9StoreTypeEi,comdat
.L1794:
	leaq	.LC490(%rip), %r15
	jmp	.L1745
.L1746:
	leaq	.LC498(%rip), %r15
	jmp	.L1745
.L1736:
	leaq	.LC496(%rip), %r15
	jmp	.L1745
.L1738:
	leaq	.LC495(%rip), %r15
	jmp	.L1745
.L1739:
	leaq	.LC494(%rip), %r15
	jmp	.L1745
.L1740:
	leaq	.LC493(%rip), %r15
	jmp	.L1745
.L1741:
	leaq	.LC497(%rip), %r15
	jmp	.L1745
.L1742:
	leaq	.LC492(%rip), %r15
	jmp	.L1745
.L1743:
	leaq	.LC491(%rip), %r15
	jmp	.L1745
.L1758:
	leaq	.LC500(%rip), %r9
	.p2align 4,,10
	.p2align 3
.L1759:
	movq	16(%r12), %rbx
	cmpq	%rcx, %rbx
	jnb	.L1764
	movzbl	(%rbx), %edi
	movq	%r9, -72(%rbp)
	movl	%edi, -64(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	-64(%rbp), %edi
	movq	-72(%rbp), %r9
	testb	%al, %al
	je	.L1847
	leaq	1(%rbx), %rax
	cmpq	%rax, 24(%r12)
	jbe	.L1764
	movzbl	1(%rbx), %eax
	sall	$8, %edi
	movq	%r9, -64(%rbp)
	orl	%eax, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	-64(%rbp), %r9
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L1762:
	pushq	%r15
	movq	%r14, %rsi
	movl	$1, %r8d
	movq	%r12, %rdi
	pushq	-56(%rbp)
	leaq	.LC531(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	popq	%rcx
	popq	%rsi
.L1832:
	movq	192(%r12), %rax
	jmp	.L1733
.L1797:
	leaq	.LC490(%rip), %r9
	jmp	.L1759
.L1760:
	leaq	.LC498(%rip), %r9
	jmp	.L1759
.L1753:
	leaq	.LC494(%rip), %r9
	jmp	.L1759
.L1754:
	leaq	.LC493(%rip), %r9
	jmp	.L1759
.L1750:
	leaq	.LC496(%rip), %r9
	jmp	.L1759
.L1752:
	leaq	.LC495(%rip), %r9
	jmp	.L1759
.L1757:
	leaq	.LC491(%rip), %r9
	jmp	.L1759
.L1755:
	leaq	.LC497(%rip), %r9
	jmp	.L1759
.L1756:
	leaq	.LC492(%rip), %r9
	jmp	.L1759
	.p2align 4,,10
	.p2align 3
.L1764:
	leaq	.LC482(%rip), %rcx
	jmp	.L1762
	.p2align 4,,10
	.p2align 3
.L1847:
	movq	%r9, -64(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	-64(%rbp), %r9
	movq	%rax, %rcx
	jmp	.L1762
.L1735:
	leaq	.LC489(%rip), %r15
	jmp	.L1745
.L1749:
	leaq	.LC489(%rip), %r9
	jmp	.L1759
.L1710:
	leaq	5(%rdx), %rsi
	cmpq	%rsi, %rax
	jbe	.L1711
	movzbl	5(%rdx), %edi
	leaq	6(%rdx), %r14
	movl	%edi, %r13d
	andl	$-16, %r13d
	testb	%dil, %dil
	js	.L1848
	sall	$28, %edi
	orl	%edi, %r8d
	testb	%r13b, %r13b
	je	.L1790
.L1713:
	leaq	.LC488(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
.L1826:
	movq	24(%r12), %rax
	movl	$5, %r13d
	jmp	.L1714
.L1723:
	leaq	4(%r14), %r15
	cmpq	%rax, %r15
	jnb	.L1791
	movzbl	4(%r14), %r14d
	movl	$5, %r8d
	testb	%r14b, %r14b
	js	.L1724
.L1725:
	addl	%r8d, %r13d
	andl	$240, %r14d
	je	.L1717
	leaq	.LC488(%rip), %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	jmp	.L1717
.L1711:
	xorl	%eax, %eax
	leaq	.LC532(%rip), %rcx
	movq	%r12, %rdi
	movq	%rsi, %r14
	leaq	.LC487(%rip), %rdx
	movl	$4, %r13d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%r12), %rax
	jmp	.L1714
.L1791:
	xorl	%r14d, %r14d
	movl	$4, %r8d
.L1724:
	leaq	.LC534(%rip), %rcx
	movq	%r15, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC487(%rip), %rdx
	movl	%r8d, -56(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	-56(%rbp), %r8d
	jmp	.L1725
.L1848:
	xorl	%eax, %eax
	leaq	.LC532(%rip), %rcx
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	leaq	.LC487(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	testb	%r13b, %r13b
	movq	-56(%rbp), %rsi
	jne	.L1713
	jmp	.L1826
.L1790:
	movl	$5, %r13d
	jmp	.L1706
	.cfi_endproc
.LFE23576:
	.size	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE14DecodeStoreMemENS1_9StoreTypeEi, .-_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE14DecodeStoreMemENS1_9StoreTypeEi
	.section	.rodata._ZN2v88internal4wasm20MemoryIndexImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKh.str1.1,"aMS",@progbits,1
.LC536:
	.string	"memory index"
	.section	.rodata._ZN2v88internal4wasm20MemoryIndexImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKh.str1.8,"aMS",@progbits,1
	.align 8
.LC537:
	.string	"expected memory index 0, found %u"
	.section	.text._ZN2v88internal4wasm20MemoryIndexImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKh,"axG",@progbits,_ZN2v88internal4wasm20MemoryIndexImmediateILNS1_7Decoder12ValidateFlagE1EEC5EPS3_PKh,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm20MemoryIndexImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKh
	.type	_ZN2v88internal4wasm20MemoryIndexImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKh, @function
_ZN2v88internal4wasm20MemoryIndexImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKh:
.LFB23582:
	.cfi_startproc
	endbr64
	movabsq	$4294967296, %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	leaq	1(%rdx), %rsi
	subq	$8, %rsp
	movq	%rax, (%rbx)
	movq	24(%rdi), %rax
	cmpq	%rax, %rsi
	ja	.L1850
	cmpl	%esi, %eax
	je	.L1850
	movzbl	1(%rdx), %ecx
	movl	%ecx, (%rbx)
	testl	%ecx, %ecx
	jne	.L1854
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1854:
	.cfi_restore_state
	addq	$8, %rsp
	leaq	.LC537(%rip), %rdx
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	.p2align 4,,10
	.p2align 3
.L1850:
	.cfi_restore_state
	leaq	.LC536(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movl	$0, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23582:
	.size	_ZN2v88internal4wasm20MemoryIndexImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKh, .-_ZN2v88internal4wasm20MemoryIndexImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKh
	.weak	_ZN2v88internal4wasm20MemoryIndexImmediateILNS1_7Decoder12ValidateFlagE1EEC1EPS3_PKh
	.set	_ZN2v88internal4wasm20MemoryIndexImmediateILNS1_7Decoder12ValidateFlagE1EEC1EPS3_PKh,_ZN2v88internal4wasm20MemoryIndexImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKh
	.section	.rodata._ZN2v88internal4wasm21CallIndirectImmediateILNS1_7Decoder12ValidateFlagE1EEC2ENS1_12WasmFeaturesEPS3_PKh.str1.1,"aMS",@progbits,1
.LC538:
	.string	"signature index"
.LC539:
	.string	"table index"
	.section	.rodata._ZN2v88internal4wasm21CallIndirectImmediateILNS1_7Decoder12ValidateFlagE1EEC2ENS1_12WasmFeaturesEPS3_PKh.str1.8,"aMS",@progbits,1
	.align 8
.LC540:
	.string	"expected table index 0, found %u"
	.section	.text._ZN2v88internal4wasm21CallIndirectImmediateILNS1_7Decoder12ValidateFlagE1EEC2ENS1_12WasmFeaturesEPS3_PKh,"axG",@progbits,_ZN2v88internal4wasm21CallIndirectImmediateILNS1_7Decoder12ValidateFlagE1EEC5ENS1_12WasmFeaturesEPS3_PKh,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm21CallIndirectImmediateILNS1_7Decoder12ValidateFlagE1EEC2ENS1_12WasmFeaturesEPS3_PKh
	.type	_ZN2v88internal4wasm21CallIndirectImmediateILNS1_7Decoder12ValidateFlagE1EEC2ENS1_12WasmFeaturesEPS3_PKh, @function
_ZN2v88internal4wasm21CallIndirectImmediateILNS1_7Decoder12ValidateFlagE1EEC2ENS1_12WasmFeaturesEPS3_PKh:
.LFB23591:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	1(%r8), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	24(%rcx), %rax
	movq	%rsi, -64(%rbp)
	movq	$0, 8(%rdi)
	movl	$0, 16(%rdi)
	cmpq	%rax, %r12
	jb	.L1902
	leaq	.LC487(%rip), %rdx
	xorl	%eax, %eax
	movq	%r12, %rsi
	movq	%r13, %rdi
	leaq	.LC538(%rip), %rcx
	xorl	%r15d, %r15d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%r13), %rax
	xorl	%edx, %edx
.L1858:
	movl	%edx, 4(%rbx)
	cmpq	%r12, %rax
	ja	.L1903
	leaq	.LC539(%rip), %rcx
	movq	%r12, %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	leaq	.LC487(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
.L1901:
	xorl	%r14d, %r14d
.L1886:
	cmpb	$0, -57(%rbp)
	je	.L1904
.L1887:
	movl	%r14d, (%rbx)
	movl	%r15d, 16(%rbx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1903:
	.cfi_restore_state
	movzbl	(%r12), %edx
	movl	%edx, %ecx
	movl	%edx, %r14d
	andl	$127, %ecx
	andl	$127, %r14d
	testb	%dl, %dl
	js	.L1905
	addl	$1, %r15d
	testb	%cl, %cl
	jne	.L1886
	xorl	%r14d, %r14d
	jmp	.L1887
	.p2align 4,,10
	.p2align 3
.L1902:
	movzbl	1(%r8), %ecx
	leaq	2(%r8), %r12
	movl	$1, %r15d
	movl	%ecx, %edx
	andl	$127, %edx
	testb	%cl, %cl
	jns	.L1858
	cmpq	%r12, %rax
	jbe	.L1859
	movzbl	2(%r8), %esi
	leaq	3(%r8), %r12
	movl	$2, %r15d
	movl	%esi, %ecx
	sall	$7, %ecx
	andl	$16256, %ecx
	orl	%ecx, %edx
	testb	%sil, %sil
	jns	.L1858
	cmpq	%r12, %rax
	jbe	.L1861
	movzbl	3(%r8), %esi
	leaq	4(%r8), %r12
	movl	$3, %r15d
	movl	%esi, %ecx
	sall	$14, %ecx
	andl	$2080768, %ecx
	orl	%ecx, %edx
	testb	%sil, %sil
	jns	.L1858
	cmpq	%r12, %rax
	ja	.L1906
	leaq	.LC487(%rip), %rdx
	xorl	%eax, %eax
	movq	%r12, %rsi
	movq	%r13, %rdi
	leaq	.LC538(%rip), %rcx
	movl	$3, %r15d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%r13), %rax
	xorl	%edx, %edx
	jmp	.L1858
	.p2align 4,,10
	.p2align 3
.L1905:
	leaq	1(%r12), %rsi
	cmpq	%rax, %rsi
	jnb	.L1870
	movzbl	1(%r12), %ecx
	movl	%ecx, %edx
	sall	$7, %edx
	andl	$16256, %edx
	orl	%edx, %r14d
	testb	%cl, %cl
	js	.L1871
	movl	$2, %eax
	testl	%r14d, %r14d
	je	.L1873
.L1872:
	addl	%eax, %r15d
	cmpb	$0, -57(%rbp)
	jne	.L1887
.L1904:
	movl	%r14d, %ecx
	leaq	.LC540(%rip), %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L1887
	.p2align 4,,10
	.p2align 3
.L1870:
	leaq	.LC539(%rip), %rcx
	movq	%r13, %rdi
	xorl	%eax, %eax
	addl	$1, %r15d
	leaq	.LC487(%rip), %rdx
	xorl	%r14d, %r14d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L1887
	.p2align 4,,10
	.p2align 3
.L1859:
	leaq	.LC487(%rip), %rdx
	xorl	%eax, %eax
	movq	%r12, %rsi
	movq	%r13, %rdi
	leaq	.LC538(%rip), %rcx
	movl	$1, %r15d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%r13), %rax
	xorl	%edx, %edx
	jmp	.L1858
	.p2align 4,,10
	.p2align 3
.L1871:
	leaq	2(%r12), %rsi
	cmpq	%rax, %rsi
	jnb	.L1874
	movzbl	2(%r12), %ecx
	movl	%ecx, %edx
	sall	$14, %edx
	andl	$2080768, %edx
	orl	%edx, %r14d
	testb	%cl, %cl
	js	.L1875
	movl	$3, %eax
	testl	%r14d, %r14d
	jne	.L1872
.L1873:
	addl	%eax, %r15d
	jmp	.L1901
	.p2align 4,,10
	.p2align 3
.L1861:
	leaq	.LC487(%rip), %rdx
	xorl	%eax, %eax
	movq	%r12, %rsi
	movq	%r13, %rdi
	leaq	.LC538(%rip), %rcx
	movl	$2, %r15d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%r13), %rax
	xorl	%edx, %edx
	jmp	.L1858
	.p2align 4,,10
	.p2align 3
.L1874:
	leaq	.LC539(%rip), %rcx
	movq	%r13, %rdi
	xorl	%eax, %eax
	addl	$2, %r15d
	leaq	.LC487(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L1901
	.p2align 4,,10
	.p2align 3
.L1875:
	leaq	3(%r12), %rsi
	cmpq	%rax, %rsi
	jb	.L1907
	leaq	.LC539(%rip), %rcx
	movq	%r13, %rdi
	xorl	%eax, %eax
	addl	$3, %r15d
	leaq	.LC487(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L1901
.L1906:
	movzbl	4(%r8), %esi
	movl	%esi, %ecx
	sall	$21, %ecx
	andl	$266338304, %ecx
	orl	%ecx, %edx
	testb	%sil, %sil
	js	.L1864
	leaq	5(%r8), %r12
	movl	$4, %r15d
	jmp	.L1858
.L1907:
	movzbl	3(%r12), %ecx
	movl	%ecx, %edx
	sall	$21, %edx
	andl	$266338304, %edx
	orl	%edx, %r14d
	testb	%cl, %cl
	js	.L1877
	movl	$4, %eax
	testl	%r14d, %r14d
	jne	.L1872
	jmp	.L1873
.L1864:
	leaq	5(%r8), %r14
	cmpq	%r14, %rax
	jbe	.L1888
	movzbl	5(%r8), %r9d
	testb	%r9b, %r9b
	js	.L1889
	movl	%r9d, %eax
	movl	$6, %r12d
	movl	$5, %r15d
	sall	$28, %eax
	orl	%eax, %edx
.L1866:
	addq	%r8, %r12
	andl	$240, %r9d
	jne	.L1867
	movq	24(%r13), %rax
	jmp	.L1858
.L1877:
	leaq	4(%r12), %rsi
	cmpq	%rax, %rsi
	setnb	%dl
	jb	.L1908
	xorl	%eax, %eax
	leaq	.LC539(%rip), %rcx
	leaq	.LC487(%rip), %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	$4, %eax
	jmp	.L1873
.L1888:
	xorl	%r9d, %r9d
	movl	$4, %r15d
.L1865:
	leaq	.LC487(%rip), %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	leaq	.LC538(%rip), %rcx
	movl	%r15d, %r12d
	movq	%r8, -80(%rbp)
	movb	%r9b, -72(%rbp)
	addq	$1, %r12
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-80(%rbp), %r8
	movzbl	-72(%rbp), %r9d
	xorl	%edx, %edx
	jmp	.L1866
.L1908:
	movzbl	4(%r12), %eax
	movl	%eax, %ecx
	movl	%eax, %r8d
	sall	$28, %ecx
	andl	$-16, %r8d
	orl	%ecx, %r14d
	testb	%al, %al
	js	.L1894
	testb	%dl, %dl
	jne	.L1894
	testb	%r8b, %r8b
	je	.L1909
.L1885:
	leaq	.LC488(%rip), %rdx
	movq	%r13, %rdi
	addl	$5, %r15d
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	jmp	.L1901
.L1867:
	leaq	.LC488(%rip), %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	24(%r13), %rax
	xorl	%edx, %edx
	jmp	.L1858
.L1889:
	movl	$5, %r15d
	jmp	.L1865
.L1894:
	xorl	%eax, %eax
	leaq	.LC539(%rip), %rcx
	movq	%r13, %rdi
	movb	%r8b, -80(%rbp)
	leaq	.LC487(%rip), %rdx
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movzbl	-80(%rbp), %r8d
	movq	-72(%rbp), %rsi
	testb	%r8b, %r8b
	jne	.L1885
	movl	$5, %eax
	jmp	.L1873
.L1909:
	movl	$5, %eax
	testl	%r14d, %r14d
	jne	.L1872
	jmp	.L1873
	.cfi_endproc
.LFE23591:
	.size	_ZN2v88internal4wasm21CallIndirectImmediateILNS1_7Decoder12ValidateFlagE1EEC2ENS1_12WasmFeaturesEPS3_PKh, .-_ZN2v88internal4wasm21CallIndirectImmediateILNS1_7Decoder12ValidateFlagE1EEC2ENS1_12WasmFeaturesEPS3_PKh
	.weak	_ZN2v88internal4wasm21CallIndirectImmediateILNS1_7Decoder12ValidateFlagE1EEC1ENS1_12WasmFeaturesEPS3_PKh
	.set	_ZN2v88internal4wasm21CallIndirectImmediateILNS1_7Decoder12ValidateFlagE1EEC1ENS1_12WasmFeaturesEPS3_PKh,_ZN2v88internal4wasm21CallIndirectImmediateILNS1_7Decoder12ValidateFlagE1EEC2ENS1_12WasmFeaturesEPS3_PKh
	.section	.text._ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_,"axG",@progbits,_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	.type	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_, @function
_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_:
.LFB23654:
	.cfi_startproc
	endbr64
	movabsq	$2305843009213693951, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$2, %rax
	cmpq	%rcx, %rax
	je	.L1924
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L1920
	movabsq	$9223372036854775804, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L1925
.L1912:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L1919:
	movl	(%r15), %eax
	subq	%r8, %r13
	leaq	4(%rbx,%rdx), %r15
	movl	%eax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L1926
	testq	%r13, %r13
	jg	.L1915
	testq	%r9, %r9
	jne	.L1918
.L1916:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1926:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L1915
.L1918:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L1916
	.p2align 4,,10
	.p2align 3
.L1925:
	testq	%rsi, %rsi
	jne	.L1913
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L1919
	.p2align 4,,10
	.p2align 3
.L1915:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L1916
	jmp	.L1918
	.p2align 4,,10
	.p2align 3
.L1920:
	movl	$4, %r14d
	jmp	.L1912
.L1924:
	leaq	.LC506(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1913:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$2, %r14
	jmp	.L1912
	.cfi_endproc
.LFE23654:
	.size	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_, .-_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	.section	.text._ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKhj,"axG",@progbits,_ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE1EEC5EPS3_PKhj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKhj
	.type	_ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKhj, @function
_ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKhj:
.LFB23659:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	leaq	1(%rdx), %rsi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%r14), %rax
	movl	$0, 8(%rdi)
	cmpq	%rax, %rsi
	jb	.L1962
	leaq	.LC532(%rip), %rcx
	movq	%r14, %rdi
	xorl	%eax, %eax
	movq	%rsi, -56(%rbp)
	leaq	.LC487(%rip), %rdx
	xorl	%r15d, %r15d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-56(%rbp), %rsi
.L1937:
	movl	$0, 0(%r13)
	movq	24(%r14), %rax
	movq	%rsi, %r12
.L1938:
	cmpq	%r12, %rax
	ja	.L1963
.L1939:
	leaq	.LC487(%rip), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	leaq	.LC534(%rip), %rcx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	xorl	%edx, %edx
.L1941:
	movl	%r15d, 8(%r13)
	movl	%edx, 4(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1962:
	.cfi_restore_state
	movzbl	1(%rdx), %edi
	movq	%rdx, %rbx
	movl	$2, %r12d
	movl	$1, %r15d
	movl	%edi, %r8d
	andl	$127, %r8d
	testb	%dil, %dil
	js	.L1964
.L1929:
	movl	%r8d, 0(%r13)
	addq	%rbx, %r12
	cmpl	%r8d, %ecx
	jnb	.L1938
	xorl	%eax, %eax
	leaq	.LC533(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%r14), %rax
	cmpq	%r12, %rax
	jbe	.L1939
.L1963:
	movzbl	(%r12), %ecx
	movl	%ecx, %edx
	andl	$127, %edx
	testb	%cl, %cl
	js	.L1940
	addl	$1, %r15d
	jmp	.L1941
	.p2align 4,,10
	.p2align 3
.L1964:
	leaq	2(%rdx), %r12
	cmpq	%r12, %rax
	jbe	.L1930
	movzbl	2(%rdx), %edi
	movl	$3, %r12d
	movl	$2, %r15d
	movl	%edi, %edx
	sall	$7, %edx
	andl	$16256, %edx
	orl	%edx, %r8d
	testb	%dil, %dil
	jns	.L1929
	leaq	3(%rbx), %r12
	cmpq	%r12, %rax
	jbe	.L1930
	movzbl	3(%rbx), %edi
	movl	$4, %r12d
	movl	$3, %r15d
	movl	%edi, %edx
	sall	$14, %edx
	andl	$2080768, %edx
	orl	%edx, %r8d
	testb	%dil, %dil
	jns	.L1929
	leaq	4(%rbx), %r12
	cmpq	%r12, %rax
	jbe	.L1930
	movzbl	4(%rbx), %edi
	movl	$5, %r12d
	movl	$4, %r15d
	movl	%edi, %edx
	sall	$21, %edx
	andl	$266338304, %edx
	orl	%edx, %r8d
	testb	%dil, %dil
	jns	.L1929
	leaq	5(%rbx), %r12
	cmpq	%r12, %rax
	ja	.L1965
	leaq	.LC532(%rip), %rcx
	movq	%r12, %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	leaq	.LC487(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
.L1936:
	movl	%r15d, %eax
	movl	$0, 0(%r13)
	leaq	1(%rbx,%rax), %r12
	movq	24(%r14), %rax
	jmp	.L1938
	.p2align 4,,10
	.p2align 3
.L1940:
	leaq	1(%r12), %rsi
	cmpq	%rax, %rsi
	jnb	.L1942
	movzbl	1(%r12), %esi
	movl	%esi, %ecx
	sall	$7, %ecx
	andl	$16256, %ecx
	orl	%ecx, %edx
	testb	%sil, %sil
	js	.L1943
	addl	$2, %r15d
	jmp	.L1941
	.p2align 4,,10
	.p2align 3
.L1930:
	movq	%r12, %rsi
	xorl	%eax, %eax
	leaq	.LC532(%rip), %rcx
	movq	%r14, %rdi
	leaq	.LC487(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	%r12, %rsi
	movq	24(%r14), %rax
	movl	$0, 0(%r13)
	movq	%rsi, %r12
	jmp	.L1938
	.p2align 4,,10
	.p2align 3
.L1942:
	leaq	.LC487(%rip), %rdx
	movq	%r14, %rdi
	xorl	%eax, %eax
	addl	$1, %r15d
	leaq	.LC534(%rip), %rcx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	xorl	%edx, %edx
	jmp	.L1941
	.p2align 4,,10
	.p2align 3
.L1943:
	leaq	2(%r12), %rsi
	cmpq	%rax, %rsi
	jnb	.L1944
	movzbl	2(%r12), %esi
	movl	%esi, %ecx
	sall	$14, %ecx
	andl	$2080768, %ecx
	orl	%ecx, %edx
	testb	%sil, %sil
	js	.L1945
	addl	$3, %r15d
	jmp	.L1941
	.p2align 4,,10
	.p2align 3
.L1944:
	leaq	.LC487(%rip), %rdx
	movq	%r14, %rdi
	xorl	%eax, %eax
	addl	$2, %r15d
	leaq	.LC534(%rip), %rcx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	xorl	%edx, %edx
	jmp	.L1941
	.p2align 4,,10
	.p2align 3
.L1945:
	leaq	3(%r12), %rsi
	cmpq	%rax, %rsi
	jb	.L1966
	leaq	.LC487(%rip), %rdx
	movq	%r14, %rdi
	xorl	%eax, %eax
	addl	$3, %r15d
	leaq	.LC534(%rip), %rcx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	xorl	%edx, %edx
	jmp	.L1941
.L1966:
	movzbl	3(%r12), %esi
	movl	%esi, %ecx
	sall	$21, %ecx
	andl	$266338304, %ecx
	orl	%ecx, %edx
	testb	%sil, %sil
	js	.L1947
	addl	$4, %r15d
	jmp	.L1941
.L1947:
	leaq	4(%r12), %rbx
	cmpq	%rax, %rbx
	jnb	.L1955
	movzbl	4(%r12), %r12d
	testb	%r12b, %r12b
	js	.L1956
	movl	%r12d, %eax
	movl	$5, %r8d
	sall	$28, %eax
	orl	%eax, %edx
.L1949:
	addl	%r8d, %r15d
	andl	$240, %r12d
	je	.L1941
	leaq	.LC488(%rip), %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	xorl	%edx, %edx
	jmp	.L1941
.L1965:
	movzbl	5(%rbx), %edx
	movl	%edx, %r15d
	andl	$-16, %r15d
	testb	%dl, %dl
	js	.L1967
	sall	$28, %edx
	orl	%edx, %r8d
	testb	%r15b, %r15b
	je	.L1954
.L1935:
	movq	%r12, %rsi
	leaq	.LC488(%rip), %rdx
	movq	%r14, %rdi
	movl	$5, %r15d
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	leaq	6(%rbx), %rsi
	jmp	.L1937
.L1955:
	xorl	%r12d, %r12d
	movl	$4, %r8d
.L1948:
	leaq	.LC487(%rip), %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	leaq	.LC534(%rip), %rcx
	movl	%r8d, -56(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	-56(%rbp), %r8d
	xorl	%edx, %edx
	jmp	.L1949
.L1956:
	movl	$5, %r8d
	jmp	.L1948
.L1967:
	xorl	%eax, %eax
	leaq	.LC532(%rip), %rcx
	movq	%r12, %rsi
	movq	%r14, %rdi
	leaq	.LC487(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	testb	%r15b, %r15b
	jne	.L1935
	movl	$5, %r15d
	jmp	.L1936
.L1954:
	movl	$6, %r12d
	movl	$5, %r15d
	jmp	.L1929
	.cfi_endproc
.LFE23659:
	.size	_ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKhj, .-_ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKhj
	.weak	_ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE1EEC1EPS3_PKhj
	.set	_ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE1EEC1EPS3_PKhj,_ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKhj
	.section	.rodata._ZN2v88internal4wasm18TableInitImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKh.str1.1,"aMS",@progbits,1
.LC542:
	.string	"elem segment index"
	.section	.text._ZN2v88internal4wasm18TableInitImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKh,"axG",@progbits,_ZN2v88internal4wasm18TableInitImmediateILNS1_7Decoder12ValidateFlagE1EEC5EPS3_PKh,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm18TableInitImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKh
	.type	_ZN2v88internal4wasm18TableInitImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKh, @function
_ZN2v88internal4wasm18TableInitImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKh:
.LFB23677:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	2(%rdx), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movdqa	.LC541(%rip), %xmm0
	movq	24(%rsi), %rax
	movups	%xmm0, (%rdi)
	cmpq	%rax, %r12
	jb	.L2007
	leaq	.LC487(%rip), %rdx
	xorl	%eax, %eax
	movq	%r12, %rsi
	movq	%r14, %rdi
	leaq	.LC542(%rip), %rcx
	xorl	%r15d, %r15d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%r14), %rax
	xorl	%edx, %edx
.L1981:
	movl	%edx, (%rbx)
	cmpq	%rax, %r12
	jb	.L2008
	leaq	.LC487(%rip), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	leaq	.LC539(%rip), %rcx
	xorl	%r12d, %r12d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	xorl	%edx, %edx
.L1984:
	movl	%r12d, 8(%rbx)
	movl	%r15d, 12(%rbx)
	movl	%edx, 4(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2008:
	.cfi_restore_state
	movzbl	(%r12), %ecx
	movl	%ecx, %edx
	andl	$127, %edx
	testb	%cl, %cl
	js	.L1983
	addl	$1, %r15d
	movl	$1, %r12d
	jmp	.L1984
	.p2align 4,,10
	.p2align 3
.L1983:
	leaq	1(%r12), %rsi
	cmpq	%rax, %rsi
	jnb	.L1985
	movzbl	1(%r12), %esi
	movl	%esi, %ecx
	sall	$7, %ecx
	andl	$16256, %ecx
	orl	%ecx, %edx
	testb	%sil, %sil
	js	.L1986
	addl	$2, %r15d
	movl	$2, %r12d
	jmp	.L1984
	.p2align 4,,10
	.p2align 3
.L2007:
	movzbl	2(%rdx), %ecx
	movq	%rdx, %r13
	movl	%ecx, %edx
	andl	$127, %edx
	testb	%cl, %cl
	js	.L2009
	sall	$25, %edx
	leaq	3(%r13), %r12
	movl	$1, %r15d
	sarl	$25, %edx
	jmp	.L1981
	.p2align 4,,10
	.p2align 3
.L2009:
	leaq	3(%r13), %rsi
	cmpq	%rsi, %rax
	jbe	.L1971
	movzbl	3(%r13), %esi
	movzbl	%dl, %edx
	movl	%esi, %ecx
	sall	$7, %ecx
	andl	$16256, %ecx
	orl	%ecx, %edx
	testb	%sil, %sil
	js	.L2010
	sall	$18, %edx
	movl	$4, %r12d
	movl	$2, %r15d
	sarl	$18, %edx
.L1995:
	addq	%r13, %r12
	jmp	.L1981
	.p2align 4,,10
	.p2align 3
.L1985:
	leaq	.LC487(%rip), %rdx
	movq	%r14, %rdi
	xorl	%eax, %eax
	addl	$1, %r15d
	leaq	.LC539(%rip), %rcx
	movl	$1, %r12d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	xorl	%edx, %edx
	jmp	.L1984
	.p2align 4,,10
	.p2align 3
.L1971:
	leaq	.LC487(%rip), %rdx
	xorl	%eax, %eax
	leaq	.LC542(%rip), %rcx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	$3, %r12d
	movq	24(%r14), %rax
	xorl	%edx, %edx
	movl	$1, %r15d
	jmp	.L1995
	.p2align 4,,10
	.p2align 3
.L1986:
	leaq	2(%r12), %rsi
	cmpq	%rax, %rsi
	jnb	.L1987
	movzbl	2(%r12), %esi
	movl	%esi, %ecx
	sall	$14, %ecx
	andl	$2080768, %ecx
	orl	%ecx, %edx
	testb	%sil, %sil
	js	.L1988
	addl	$3, %r15d
	movl	$3, %r12d
	jmp	.L1984
	.p2align 4,,10
	.p2align 3
.L2010:
	leaq	4(%r13), %rsi
	cmpq	%rsi, %rax
	jbe	.L1973
	movzbl	4(%r13), %esi
	movl	%esi, %ecx
	sall	$14, %ecx
	andl	$2080768, %ecx
	orl	%ecx, %edx
	testb	%sil, %sil
	js	.L2011
	sall	$11, %edx
	movl	$5, %r12d
	movl	$3, %r15d
	sarl	$11, %edx
	jmp	.L1995
	.p2align 4,,10
	.p2align 3
.L1987:
	leaq	.LC487(%rip), %rdx
	movq	%r14, %rdi
	xorl	%eax, %eax
	addl	$2, %r15d
	leaq	.LC539(%rip), %rcx
	movl	$2, %r12d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	xorl	%edx, %edx
	jmp	.L1984
	.p2align 4,,10
	.p2align 3
.L1973:
	leaq	.LC487(%rip), %rdx
	xorl	%eax, %eax
	leaq	.LC542(%rip), %rcx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	$4, %r12d
	movq	24(%r14), %rax
	xorl	%edx, %edx
	movl	$2, %r15d
	jmp	.L1995
	.p2align 4,,10
	.p2align 3
.L1988:
	leaq	3(%r12), %rsi
	cmpq	%rax, %rsi
	jb	.L2012
	leaq	.LC487(%rip), %rdx
	movq	%r14, %rdi
	xorl	%eax, %eax
	addl	$3, %r15d
	leaq	.LC539(%rip), %rcx
	movl	$3, %r12d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	xorl	%edx, %edx
	jmp	.L1984
	.p2align 4,,10
	.p2align 3
.L2011:
	leaq	5(%r13), %rsi
	cmpq	%rsi, %rax
	ja	.L2013
	leaq	.LC487(%rip), %rdx
	xorl	%eax, %eax
	leaq	.LC542(%rip), %rcx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	$5, %r12d
	movq	24(%r14), %rax
	xorl	%edx, %edx
	movl	$3, %r15d
	jmp	.L1995
.L2012:
	movzbl	3(%r12), %esi
	movl	%esi, %ecx
	sall	$21, %ecx
	andl	$266338304, %ecx
	orl	%ecx, %edx
	testb	%sil, %sil
	js	.L1990
	addl	$4, %r15d
	movl	$4, %r12d
	jmp	.L1984
.L2013:
	movzbl	5(%r13), %esi
	movl	%esi, %ecx
	sall	$21, %ecx
	andl	$266338304, %ecx
	orl	%ecx, %edx
	testb	%sil, %sil
	js	.L2014
	sall	$4, %edx
	movl	$6, %r12d
	movl	$4, %r15d
	sarl	$4, %edx
	jmp	.L1995
.L1990:
	leaq	4(%r12), %r13
	cmpq	%rax, %r13
	jnb	.L1999
	movzbl	4(%r12), %r8d
	testb	%r8b, %r8b
	js	.L2000
	movl	%r8d, %eax
	movl	$5, %r12d
	sall	$28, %eax
	orl	%eax, %edx
.L1992:
	addl	%r12d, %r15d
	andl	$240, %r8d
	je	.L1984
	leaq	.LC488(%rip), %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	xorl	%edx, %edx
	jmp	.L1984
.L2014:
	leaq	6(%r13), %rsi
	cmpq	%rsi, %rax
	jbe	.L1997
	movzbl	6(%r13), %r8d
	testb	%r8b, %r8b
	js	.L1998
	movl	%r8d, %eax
	movl	$7, %r12d
	movl	$5, %r15d
	sall	$28, %eax
	orl	%eax, %edx
.L1978:
	addq	%r13, %r12
	andl	$248, %r8d
	je	.L2001
	cmpb	$120, %r8b
	je	.L2001
	leaq	.LC488(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	24(%r14), %rax
	xorl	%edx, %edx
	jmp	.L1981
.L1999:
	xorl	%r8d, %r8d
	movl	$4, %r12d
.L1991:
	leaq	.LC487(%rip), %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	leaq	.LC539(%rip), %rcx
	movb	%r8b, -56(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movzbl	-56(%rbp), %r8d
	xorl	%edx, %edx
	jmp	.L1992
.L1997:
	xorl	%r8d, %r8d
	movl	$4, %r15d
.L1977:
	leaq	.LC487(%rip), %rdx
	movq	%r14, %rdi
	xorl	%eax, %eax
	movl	%r15d, %r12d
	leaq	.LC542(%rip), %rcx
	movb	%r8b, -57(%rbp)
	addq	$2, %r12
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movzbl	-57(%rbp), %r8d
	movq	-56(%rbp), %rsi
	xorl	%edx, %edx
	jmp	.L1978
.L2001:
	movq	24(%r14), %rax
	jmp	.L1981
.L1998:
	movl	$5, %r15d
	jmp	.L1977
.L2000:
	movl	$5, %r12d
	jmp	.L1991
	.cfi_endproc
.LFE23677:
	.size	_ZN2v88internal4wasm18TableInitImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKh, .-_ZN2v88internal4wasm18TableInitImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKh
	.weak	_ZN2v88internal4wasm18TableInitImmediateILNS1_7Decoder12ValidateFlagE1EEC1EPS3_PKh
	.set	_ZN2v88internal4wasm18TableInitImmediateILNS1_7Decoder12ValidateFlagE1EEC1EPS3_PKh,_ZN2v88internal4wasm18TableInitImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKh
	.section	.text._ZN2v88internal4wasm18TableCopyImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKh,"axG",@progbits,_ZN2v88internal4wasm18TableCopyImmediateILNS1_7Decoder12ValidateFlagE1EEC5EPS3_PKh,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm18TableCopyImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKh
	.type	_ZN2v88internal4wasm18TableCopyImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKh, @function
_ZN2v88internal4wasm18TableCopyImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKh:
.LFB23683:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	leaq	2(%rdx), %rsi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movdqa	.LC543(%rip), %xmm0
	movl	$0, 16(%rdi)
	movups	%xmm0, (%rdi)
	movq	24(%r14), %rax
	cmpq	%rax, %rsi
	jb	.L2046
	leaq	.LC487(%rip), %rdx
	xorl	%eax, %eax
	movq	%r14, %rdi
	xorl	%r15d, %r15d
	leaq	.LC539(%rip), %rcx
	movl	$2, %r13d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%r14), %rax
	xorl	%edx, %edx
.L2017:
	leaq	(%r12,%r13), %rsi
	movl	%edx, (%rbx)
	movl	%r15d, 4(%rbx)
	cmpq	%rax, %rsi
	jb	.L2047
	leaq	.LC487(%rip), %rdx
	xorl	%eax, %eax
	movq	%r14, %rdi
	xorl	%r12d, %r12d
	leaq	.LC539(%rip), %rcx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	4(%rbx), %eax
	xorl	%edx, %edx
.L2026:
	movl	%r12d, 12(%rbx)
	movl	%edx, 8(%rbx)
	movl	%eax, 16(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2047:
	.cfi_restore_state
	movzbl	(%rsi), %ecx
	movl	%ecx, %edx
	andl	$127, %edx
	testb	%cl, %cl
	js	.L2025
	leal	1(%r15), %eax
	movl	$1, %r12d
	jmp	.L2026
	.p2align 4,,10
	.p2align 3
.L2046:
	movzbl	2(%rdx), %ecx
	movl	$3, %r13d
	movl	$1, %r15d
	movl	%ecx, %edx
	andl	$127, %edx
	testb	%cl, %cl
	jns	.L2017
	leaq	3(%r12), %rsi
	cmpq	%rsi, %rax
	jbe	.L2018
	movzbl	3(%r12), %esi
	movl	$4, %r13d
	movl	$2, %r15d
	movl	%esi, %ecx
	sall	$7, %ecx
	andl	$16256, %ecx
	orl	%ecx, %edx
	testb	%sil, %sil
	jns	.L2017
	leaq	4(%r12), %rsi
	cmpq	%rsi, %rax
	jbe	.L2018
	movzbl	4(%r12), %esi
	movl	$5, %r13d
	movl	$3, %r15d
	movl	%esi, %ecx
	sall	$14, %ecx
	andl	$2080768, %ecx
	orl	%ecx, %edx
	testb	%sil, %sil
	jns	.L2017
	leaq	5(%r12), %rsi
	cmpq	%rsi, %rax
	jbe	.L2018
	movzbl	5(%r12), %esi
	movl	$6, %r13d
	movl	$4, %r15d
	movl	%esi, %ecx
	sall	$21, %ecx
	andl	$266338304, %ecx
	orl	%ecx, %edx
	testb	%sil, %sil
	jns	.L2017
	leaq	6(%r12), %rsi
	cmpq	%rsi, %rax
	jbe	.L2040
	movzbl	6(%r12), %r8d
	testb	%r8b, %r8b
	js	.L2041
	movl	%r8d, %eax
	movl	$7, %r13d
	movl	$5, %r15d
	sall	$28, %eax
	orl	%eax, %edx
.L2022:
	andl	$240, %r8d
	jne	.L2023
	movq	24(%r14), %rax
	jmp	.L2017
	.p2align 4,,10
	.p2align 3
.L2025:
	leaq	1(%rsi), %r8
	cmpq	%rax, %r8
	jnb	.L2027
	movzbl	1(%rsi), %edi
	movl	%edi, %ecx
	sall	$7, %ecx
	andl	$16256, %ecx
	orl	%ecx, %edx
	testb	%dil, %dil
	js	.L2028
	leal	2(%r15), %eax
	movl	$2, %r12d
	jmp	.L2026
	.p2align 4,,10
	.p2align 3
.L2018:
	leaq	.LC487(%rip), %rdx
	xorl	%eax, %eax
	leaq	.LC539(%rip), %rcx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%r14), %rax
	xorl	%edx, %edx
	jmp	.L2017
	.p2align 4,,10
	.p2align 3
.L2027:
	leaq	.LC487(%rip), %rdx
	xorl	%eax, %eax
	movq	%r8, %rsi
	movq	%r14, %rdi
	leaq	.LC539(%rip), %rcx
	movl	$1, %r12d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	4(%rbx), %eax
	xorl	%edx, %edx
	addl	$1, %eax
	jmp	.L2026
	.p2align 4,,10
	.p2align 3
.L2028:
	leaq	2(%rsi), %r8
	cmpq	%rax, %r8
	jnb	.L2029
	movzbl	2(%rsi), %edi
	movl	%edi, %ecx
	sall	$14, %ecx
	andl	$2080768, %ecx
	orl	%ecx, %edx
	testb	%dil, %dil
	js	.L2030
	leal	3(%r15), %eax
	movl	$3, %r12d
	jmp	.L2026
	.p2align 4,,10
	.p2align 3
.L2029:
	leaq	.LC487(%rip), %rdx
	xorl	%eax, %eax
	movq	%r8, %rsi
	movq	%r14, %rdi
	leaq	.LC539(%rip), %rcx
	movl	$2, %r12d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	4(%rbx), %eax
	xorl	%edx, %edx
	addl	$2, %eax
	jmp	.L2026
	.p2align 4,,10
	.p2align 3
.L2030:
	leaq	3(%rsi), %r8
	cmpq	%rax, %r8
	jb	.L2048
	leaq	.LC487(%rip), %rdx
	xorl	%eax, %eax
	movq	%r8, %rsi
	movq	%r14, %rdi
	leaq	.LC539(%rip), %rcx
	movl	$3, %r12d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	4(%rbx), %eax
	xorl	%edx, %edx
	addl	$3, %eax
	jmp	.L2026
.L2048:
	movzbl	3(%rsi), %edi
	movl	%edi, %ecx
	sall	$21, %ecx
	andl	$266338304, %ecx
	orl	%ecx, %edx
	testb	%dil, %dil
	js	.L2032
	leal	4(%r15), %eax
	movl	$4, %r12d
	jmp	.L2026
.L2032:
	leaq	4(%rsi), %r13
	cmpq	%rax, %r13
	jnb	.L2042
	movzbl	4(%rsi), %r15d
	testb	%r15b, %r15b
	js	.L2043
	movl	%r15d, %eax
	movl	$5, %r12d
	sall	$28, %eax
	orl	%eax, %edx
.L2034:
	andl	$240, %r15d
	jne	.L2035
	movl	4(%rbx), %eax
	addl	%r12d, %eax
	jmp	.L2026
.L2040:
	xorl	%r8d, %r8d
.L2021:
	leaq	.LC487(%rip), %rdx
	movq	%r14, %rdi
	xorl	%eax, %eax
	movl	%r15d, %r13d
	leaq	.LC539(%rip), %rcx
	movb	%r8b, -57(%rbp)
	addq	$2, %r13
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movzbl	-57(%rbp), %r8d
	movq	-56(%rbp), %rsi
	xorl	%edx, %edx
	jmp	.L2022
.L2042:
	xorl	%r15d, %r15d
	movl	$4, %r12d
.L2033:
	leaq	.LC487(%rip), %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	leaq	.LC539(%rip), %rcx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	xorl	%edx, %edx
	jmp	.L2034
.L2023:
	leaq	.LC488(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	24(%r14), %rax
	xorl	%edx, %edx
	jmp	.L2017
.L2035:
	leaq	.LC488(%rip), %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movl	4(%rbx), %eax
	xorl	%edx, %edx
	addl	%r12d, %eax
	jmp	.L2026
.L2043:
	movl	$5, %r12d
	jmp	.L2033
.L2041:
	movl	$5, %r15d
	jmp	.L2021
	.cfi_endproc
.LFE23683:
	.size	_ZN2v88internal4wasm18TableCopyImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKh, .-_ZN2v88internal4wasm18TableCopyImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKh
	.weak	_ZN2v88internal4wasm18TableCopyImmediateILNS1_7Decoder12ValidateFlagE1EEC1EPS3_PKh
	.set	_ZN2v88internal4wasm18TableCopyImmediateILNS1_7Decoder12ValidateFlagE1EEC1EPS3_PKh,_ZN2v88internal4wasm18TableCopyImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKh
	.section	.text._ZNSt6vectorIN2v88internal4wasm9ValueBaseENS1_13ZoneAllocatorIS3_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal4wasm9ValueBaseENS1_13ZoneAllocatorIS3_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal4wasm9ValueBaseENS1_13ZoneAllocatorIS3_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_
	.type	_ZNSt6vectorIN2v88internal4wasm9ValueBaseENS1_13ZoneAllocatorIS3_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_, @function
_ZNSt6vectorIN2v88internal4wasm9ValueBaseENS1_13ZoneAllocatorIS3_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_:
.LFB23900:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	16(%rdi), %r12
	cmpq	24(%rdi), %r12
	je	.L2050
	movq	(%rsi), %rcx
	movzbl	(%rdx), %eax
	movq	%rcx, (%r12)
	movb	%al, 8(%r12)
	addq	$16, 16(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2050:
	.cfi_restore_state
	movq	8(%rdi), %r15
	movq	%r12, %r14
	subq	%r15, %r14
	movq	%r14, %rax
	sarq	$4, %rax
	cmpq	$134217727, %rax
	je	.L2064
	testq	%rax, %rax
	je	.L2059
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L2065
	movl	$2147483632, %esi
	movl	$2147483632, %r8d
.L2053:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	%rcx, %rsi
	ja	.L2066
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L2056:
	addq	%rax, %r8
	leaq	16(%rax), %rcx
.L2054:
	movq	0(%r13), %rsi
	movzbl	(%rdx), %edx
	addq	%rax, %r14
	movq	%rsi, (%r14)
	movb	%dl, 8(%r14)
	cmpq	%r15, %r12
	je	.L2057
	movq	%r15, %rdx
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L2058:
	movq	(%rdx), %rdi
	movzbl	8(%rdx), %esi
	addq	$16, %rdx
	addq	$16, %rcx
	movq	%rdi, -16(%rcx)
	movb	%sil, -8(%rcx)
	cmpq	%rdx, %r12
	jne	.L2058
	subq	%r15, %r12
	leaq	16(%rax,%r12), %rcx
.L2057:
	movq	%rax, %xmm0
	movq	%rcx, %xmm1
	movq	%r8, 24(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2065:
	.cfi_restore_state
	testq	%rcx, %rcx
	jne	.L2067
	movl	$16, %ecx
	xorl	%r8d, %r8d
	xorl	%eax, %eax
	jmp	.L2054
	.p2align 4,,10
	.p2align 3
.L2059:
	movl	$16, %esi
	movl	$16, %r8d
	jmp	.L2053
.L2066:
	movq	%rdx, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %rdx
	jmp	.L2056
.L2064:
	leaq	.LC506(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L2067:
	cmpq	$134217727, %rcx
	movl	$134217727, %r8d
	cmovbe	%rcx, %r8
	salq	$4, %r8
	movq	%r8, %rsi
	jmp	.L2053
	.cfi_endproc
.LFE23900:
	.size	_ZNSt6vectorIN2v88internal4wasm9ValueBaseENS1_13ZoneAllocatorIS3_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_, .-_ZNSt6vectorIN2v88internal4wasm9ValueBaseENS1_13ZoneAllocatorIS3_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_,"axG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	.type	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_, @function
_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_:
.LFB23867:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movzbl	%cl, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	224(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	192(%rdi), %rax
	movl	-84(%rsi), %edi
	movq	%rax, %rdx
	subq	184(%rbx), %rdx
	sarq	$4, %rdx
	cmpq	%rdx, %rdi
	jb	.L2069
	cmpb	$2, -72(%rsi)
	movq	16(%rbx), %r14
	jne	.L2190
.L2074:
	movq	224(%rbx), %rcx
	movq	%rax, %rdx
	subq	184(%rbx), %rdx
	sarq	$4, %rdx
	movl	-84(%rcx), %esi
	cmpq	%rdx, %rsi
	jnb	.L2191
	movzbl	-8(%rax), %edx
	movq	-16(%rax), %r14
	subq	$16, %rax
	movq	%rax, 192(%rbx)
	cmpb	%dl, %r12b
	je	.L2111
	cmpb	$6, %r12b
	sete	%al
	cmpb	$8, %dl
	sete	%cl
	testb	%al, %al
	je	.L2158
	testb	%cl, %cl
	je	.L2158
.L2111:
	testb	%r15b, %r15b
	jne	.L2192
.L2068:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2193
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2069:
	.cfi_restore_state
	movzbl	-8(%rax), %edx
	movq	-16(%rax), %r14
	subq	$16, %rax
	movq	%rax, 192(%rbx)
	cmpb	%dl, %r8b
	je	.L2074
	leal	-7(%rdx), %edi
	movzbl	%r8b, %ecx
	cmpb	$2, %dil
	ja	.L2157
	cmpb	$6, %r8b
	je	.L2074
.L2157:
	leal	-7(%r8), %eax
	testb	$-3, %al
	sete	%al
	cmpb	$8, %dl
	sete	%dil
	andl	%edi, %eax
	xorl	$1, %eax
	cmpb	$10, %dl
	setne	%dil
	testb	%dil, %al
	je	.L2187
	cmpb	$10, %r8b
	je	.L2187
	cmpb	$9, %dl
	ja	.L2076
	leaq	.L2078(%rip), %rdi
	movslq	(%rdi,%rdx,4), %rax
	addq	%rdi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_,"aG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_,comdat
	.align 4
	.align 4
.L2078:
	.long	.L2087-.L2078
	.long	.L2148-.L2078
	.long	.L2085-.L2078
	.long	.L2084-.L2078
	.long	.L2083-.L2078
	.long	.L2082-.L2078
	.long	.L2081-.L2078
	.long	.L2080-.L2078
	.long	.L2079-.L2078
	.long	.L2077-.L2078
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_,"axG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_,comdat
	.p2align 4,,10
	.p2align 3
.L2191:
	cmpb	$2, -72(%rcx)
	movq	16(%rbx), %r12
	je	.L2111
	leaq	.LC482(%rip), %r13
	cmpq	%r12, 24(%rbx)
	jbe	.L2108
	movzbl	(%r12), %r14d
	movl	%r14d, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	testb	%al, %al
	je	.L2194
	leaq	1(%r12), %rax
	cmpq	%rax, 24(%rbx)
	ja	.L2110
.L2188:
	movq	16(%rbx), %r12
.L2108:
	xorl	%eax, %eax
	movq	%r13, %rcx
	leaq	.LC530(%rip), %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	testb	%r15b, %r15b
	je	.L2068
	.p2align 4,,10
	.p2align 3
.L2192:
	leaq	-57(%rbp), %rdx
	leaq	16(%rbx), %rsi
	movb	%r15b, -57(%rbp)
	leaq	176(%rbx), %rdi
	call	_ZNSt6vectorIN2v88internal4wasm9ValueBaseENS1_13ZoneAllocatorIS3_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_
	jmp	.L2068
.L2158:
	leal	-7(%rdx), %edi
	andl	$253, %edi
	jne	.L2159
	testb	%al, %al
	jne	.L2111
.L2159:
	leal	-7(%r12), %eax
	testb	$-3, %al
	sete	%al
	testb	%cl, %al
	jne	.L2111
	cmpb	$10, %dl
	setne	%cl
	cmpb	$10, %r12b
	setne	%al
	testb	%al, %cl
	je	.L2111
	cmpb	$9, %dl
	ja	.L2115
	leaq	.L2117(%rip), %rcx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_,"aG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_,comdat
	.align 4
	.align 4
.L2117:
	.long	.L2126-.L2117
	.long	.L2153-.L2117
	.long	.L2124-.L2117
	.long	.L2123-.L2117
	.long	.L2122-.L2117
	.long	.L2121-.L2117
	.long	.L2120-.L2117
	.long	.L2119-.L2117
	.long	.L2118-.L2117
	.long	.L2116-.L2117
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_,"axG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_,comdat
	.p2align 4,,10
	.p2align 3
.L2190:
	leaq	.LC482(%rip), %rcx
	cmpq	%r14, 24(%rbx)
	jbe	.L2071
	movzbl	(%r14), %edi
	movq	%rcx, -80(%rbp)
	movl	%edi, -72(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	-72(%rbp), %edi
	movq	-80(%rbp), %rcx
	testb	%al, %al
	je	.L2185
	leaq	1(%r14), %rax
	cmpq	%rax, 24(%rbx)
	ja	.L2073
.L2186:
	movq	16(%rbx), %r14
.L2071:
	xorl	%eax, %eax
	leaq	.LC530(%rip), %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	192(%rbx), %rax
	jmp	.L2074
	.p2align 4,,10
	.p2align 3
.L2073:
	movzbl	1(%r14), %eax
	sall	$8, %edi
	orl	%eax, %edi
.L2185:
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	%rax, %rcx
	jmp	.L2186
	.p2align 4,,10
	.p2align 3
.L2110:
	movzbl	1(%r12), %edi
	sall	$8, %r14d
	orl	%r14d, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	%rax, %r13
	jmp	.L2188
	.p2align 4,,10
	.p2align 3
.L2194:
	movl	%r14d, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	16(%rbx), %r12
	movq	%rax, %r13
	jmp	.L2108
	.p2align 4,,10
	.p2align 3
.L2085:
	leaq	.LC500(%rip), %rax
	movq	%rax, -80(%rbp)
	.p2align 4,,10
	.p2align 3
.L2086:
	movq	24(%rbx), %rdx
	leaq	.LC482(%rip), %rax
	movq	%rax, -72(%rbp)
	cmpq	%rdx, %r14
	jnb	.L2088
	movzbl	(%r14), %edi
	movl	%r8d, -104(%rbp)
	movb	%cl, -96(%rbp)
	movl	%edi, -88(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	-88(%rbp), %edi
	movzbl	-96(%rbp), %ecx
	testb	%al, %al
	movl	-104(%rbp), %r8d
	je	.L2195
	movq	24(%rbx), %rdx
	leaq	1(%r14), %rax
	cmpq	%rax, %rdx
	jbe	.L2088
	movzbl	1(%r14), %eax
	sall	$8, %edi
	movl	%r8d, -96(%rbp)
	movb	%cl, -88(%rbp)
	orl	%eax, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	24(%rbx), %rdx
	movl	-96(%rbp), %r8d
	movq	%rax, -72(%rbp)
	movzbl	-88(%rbp), %ecx
	.p2align 4,,10
	.p2align 3
.L2088:
	cmpb	$9, %r8b
	ja	.L2090
	leaq	.L2092(%rip), %rdi
	movslq	(%rdi,%rcx,4), %rax
	addq	%rdi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_,"aG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_,comdat
	.align 4
	.align 4
.L2092:
	.long	.L2101-.L2092
	.long	.L2151-.L2092
	.long	.L2099-.L2092
	.long	.L2098-.L2092
	.long	.L2097-.L2092
	.long	.L2096-.L2092
	.long	.L2095-.L2092
	.long	.L2094-.L2092
	.long	.L2093-.L2092
	.long	.L2091-.L2092
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_,"axG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_,comdat
	.p2align 4,,10
	.p2align 3
.L2099:
	leaq	.LC500(%rip), %r9
	.p2align 4,,10
	.p2align 3
.L2100:
	movq	16(%rbx), %rcx
	cmpq	%rdx, %rcx
	jnb	.L2105
	movzbl	(%rcx), %edi
	movq	%r9, -104(%rbp)
	movq	%rcx, -96(%rbp)
	movl	%edi, -88(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	-88(%rbp), %edi
	movq	-96(%rbp), %rcx
	testb	%al, %al
	movq	-104(%rbp), %r9
	je	.L2196
	leaq	1(%rcx), %rax
	cmpq	%rax, 24(%rbx)
	jbe	.L2105
	movzbl	1(%rcx), %eax
	sall	$8, %edi
	movq	%r9, -88(%rbp)
	orl	%eax, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	-88(%rbp), %r9
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L2103:
	pushq	-80(%rbp)
	movq	%r14, %rsi
	movl	$1, %r8d
	movq	%rbx, %rdi
	pushq	-72(%rbp)
	leaq	.LC531(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	popq	%rcx
	popq	%rsi
.L2187:
	movq	192(%rbx), %rax
	jmp	.L2074
	.p2align 4,,10
	.p2align 3
.L2151:
	leaq	.LC490(%rip), %r9
	jmp	.L2100
	.p2align 4,,10
	.p2align 3
.L2148:
	leaq	.LC490(%rip), %rax
	movq	%rax, -80(%rbp)
	jmp	.L2086
	.p2align 4,,10
	.p2align 3
.L2093:
	leaq	.LC495(%rip), %r9
	jmp	.L2100
	.p2align 4,,10
	.p2align 3
.L2094:
	leaq	.LC494(%rip), %r9
	jmp	.L2100
	.p2align 4,,10
	.p2align 3
.L2095:
	leaq	.LC493(%rip), %r9
	jmp	.L2100
	.p2align 4,,10
	.p2align 3
.L2096:
	leaq	.LC497(%rip), %r9
	jmp	.L2100
	.p2align 4,,10
	.p2align 3
.L2097:
	leaq	.LC492(%rip), %r9
	jmp	.L2100
	.p2align 4,,10
	.p2align 3
.L2098:
	leaq	.LC491(%rip), %r9
	jmp	.L2100
	.p2align 4,,10
	.p2align 3
.L2101:
	leaq	.LC498(%rip), %r9
	jmp	.L2100
	.p2align 4,,10
	.p2align 3
.L2087:
	leaq	.LC498(%rip), %rax
	movq	%rax, -80(%rbp)
	jmp	.L2086
	.p2align 4,,10
	.p2align 3
.L2077:
	leaq	.LC496(%rip), %rax
	movq	%rax, -80(%rbp)
	jmp	.L2086
	.p2align 4,,10
	.p2align 3
.L2091:
	leaq	.LC496(%rip), %r9
	jmp	.L2100
	.p2align 4,,10
	.p2align 3
.L2079:
	leaq	.LC495(%rip), %rax
	movq	%rax, -80(%rbp)
	jmp	.L2086
	.p2align 4,,10
	.p2align 3
.L2080:
	leaq	.LC494(%rip), %rax
	movq	%rax, -80(%rbp)
	jmp	.L2086
	.p2align 4,,10
	.p2align 3
.L2081:
	leaq	.LC493(%rip), %rax
	movq	%rax, -80(%rbp)
	jmp	.L2086
	.p2align 4,,10
	.p2align 3
.L2082:
	leaq	.LC497(%rip), %rax
	movq	%rax, -80(%rbp)
	jmp	.L2086
	.p2align 4,,10
	.p2align 3
.L2083:
	leaq	.LC492(%rip), %rax
	movq	%rax, -80(%rbp)
	jmp	.L2086
	.p2align 4,,10
	.p2align 3
.L2084:
	leaq	.LC491(%rip), %rax
	movq	%rax, -80(%rbp)
	jmp	.L2086
	.p2align 4,,10
	.p2align 3
.L2105:
	leaq	.LC482(%rip), %rcx
	jmp	.L2103
	.p2align 4,,10
	.p2align 3
.L2195:
	movl	%r8d, -96(%rbp)
	movb	%cl, -88(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	24(%rbx), %rdx
	movzbl	-88(%rbp), %ecx
	movq	%rax, -72(%rbp)
	movl	-96(%rbp), %r8d
	jmp	.L2088
	.p2align 4,,10
	.p2align 3
.L2196:
	movq	%r9, -88(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	-88(%rbp), %r9
	movq	%rax, %rcx
	jmp	.L2103
.L2090:
	leaq	.LC489(%rip), %r9
	jmp	.L2100
.L2076:
	leaq	.LC489(%rip), %rax
	movq	%rax, -80(%rbp)
	jmp	.L2086
.L2193:
	call	__stack_chk_fail@PLT
	.p2align 4,,10
	.p2align 3
.L2124:
	leaq	.LC500(%rip), %rax
	movq	%rax, -80(%rbp)
	.p2align 4,,10
	.p2align 3
.L2125:
	movq	24(%rbx), %rdx
	leaq	.LC482(%rip), %rax
	movq	%rax, -72(%rbp)
	cmpq	%rdx, %r14
	jnb	.L2127
	movzbl	(%r14), %edi
	movl	%edi, -88(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	-88(%rbp), %edi
	testb	%al, %al
	je	.L2189
	movq	24(%rbx), %rdx
	leaq	1(%r14), %rax
	cmpq	%rax, %rdx
	jbe	.L2127
	movzbl	1(%r14), %eax
	sall	$8, %edi
	orl	%eax, %edi
.L2189:
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	24(%rbx), %rdx
	movq	%rax, -72(%rbp)
.L2127:
	cmpb	$9, %r12b
	ja	.L2129
	leaq	.L2131(%rip), %rcx
	movslq	(%rcx,%r13,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_,"aG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_,comdat
	.align 4
	.align 4
.L2131:
	.long	.L2140-.L2131
	.long	.L2156-.L2131
	.long	.L2138-.L2131
	.long	.L2137-.L2131
	.long	.L2136-.L2131
	.long	.L2135-.L2131
	.long	.L2134-.L2131
	.long	.L2133-.L2131
	.long	.L2132-.L2131
	.long	.L2130-.L2131
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_,"axG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_,comdat
	.p2align 4,,10
	.p2align 3
.L2138:
	leaq	.LC500(%rip), %r9
	.p2align 4,,10
	.p2align 3
.L2139:
	movq	16(%rbx), %r12
	cmpq	%rdx, %r12
	jnb	.L2144
	movzbl	(%r12), %r13d
	movq	%r9, -88(%rbp)
	movl	%r13d, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movq	-88(%rbp), %r9
	testb	%al, %al
	je	.L2197
	leaq	1(%r12), %rax
	cmpq	%rax, 24(%rbx)
	jbe	.L2144
	movl	%r13d, %edi
	movzbl	1(%r12), %r13d
	movq	%r9, -88(%rbp)
	sall	$8, %edi
	orl	%r13d, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	-88(%rbp), %r9
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L2142:
	pushq	-80(%rbp)
	leaq	.LC531(%rip), %rdx
	xorl	%eax, %eax
	xorl	%r8d, %r8d
	pushq	-72(%rbp)
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	popq	%rax
	popq	%rdx
	jmp	.L2111
	.p2align 4,,10
	.p2align 3
.L2156:
	leaq	.LC490(%rip), %r9
	jmp	.L2139
	.p2align 4,,10
	.p2align 3
.L2153:
	leaq	.LC490(%rip), %rax
	movq	%rax, -80(%rbp)
	jmp	.L2125
	.p2align 4,,10
	.p2align 3
.L2130:
	leaq	.LC496(%rip), %r9
	jmp	.L2139
	.p2align 4,,10
	.p2align 3
.L2140:
	leaq	.LC498(%rip), %r9
	jmp	.L2139
	.p2align 4,,10
	.p2align 3
.L2126:
	leaq	.LC498(%rip), %rax
	movq	%rax, -80(%rbp)
	jmp	.L2125
	.p2align 4,,10
	.p2align 3
.L2132:
	leaq	.LC495(%rip), %r9
	jmp	.L2139
	.p2align 4,,10
	.p2align 3
.L2133:
	leaq	.LC494(%rip), %r9
	jmp	.L2139
	.p2align 4,,10
	.p2align 3
.L2134:
	leaq	.LC493(%rip), %r9
	jmp	.L2139
	.p2align 4,,10
	.p2align 3
.L2135:
	leaq	.LC497(%rip), %r9
	jmp	.L2139
	.p2align 4,,10
	.p2align 3
.L2136:
	leaq	.LC492(%rip), %r9
	jmp	.L2139
	.p2align 4,,10
	.p2align 3
.L2137:
	leaq	.LC491(%rip), %r9
	jmp	.L2139
	.p2align 4,,10
	.p2align 3
.L2116:
	leaq	.LC496(%rip), %rax
	movq	%rax, -80(%rbp)
	jmp	.L2125
	.p2align 4,,10
	.p2align 3
.L2118:
	leaq	.LC495(%rip), %rax
	movq	%rax, -80(%rbp)
	jmp	.L2125
	.p2align 4,,10
	.p2align 3
.L2119:
	leaq	.LC494(%rip), %rax
	movq	%rax, -80(%rbp)
	jmp	.L2125
	.p2align 4,,10
	.p2align 3
.L2120:
	leaq	.LC493(%rip), %rax
	movq	%rax, -80(%rbp)
	jmp	.L2125
	.p2align 4,,10
	.p2align 3
.L2121:
	leaq	.LC497(%rip), %rax
	movq	%rax, -80(%rbp)
	jmp	.L2125
	.p2align 4,,10
	.p2align 3
.L2122:
	leaq	.LC492(%rip), %rax
	movq	%rax, -80(%rbp)
	jmp	.L2125
	.p2align 4,,10
	.p2align 3
.L2123:
	leaq	.LC491(%rip), %rax
	movq	%rax, -80(%rbp)
	jmp	.L2125
	.p2align 4,,10
	.p2align 3
.L2144:
	leaq	.LC482(%rip), %rcx
	jmp	.L2142
	.p2align 4,,10
	.p2align 3
.L2197:
	movl	%r13d, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	-88(%rbp), %r9
	movq	%rax, %rcx
	jmp	.L2142
.L2129:
	leaq	.LC489(%rip), %r9
	jmp	.L2139
.L2115:
	leaq	.LC489(%rip), %rax
	movq	%rax, -80(%rbp)
	jmp	.L2125
	.cfi_endproc
.LFE23867:
	.size	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_, .-_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_,"axG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_
	.type	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_, @function
_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_:
.LFB23865:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzbl	%cl, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	224(%rdi), %rcx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	192(%rdi), %rax
	movl	-84(%rcx), %esi
	movq	%rax, %rdx
	subq	184(%rdi), %rdx
	sarq	$4, %rdx
	cmpq	%rdx, %rsi
	jb	.L2199
	cmpb	$2, -72(%rcx)
	movq	16(%rdi), %r12
	jne	.L2261
.L2204:
	testb	%r13b, %r13b
	jne	.L2262
.L2198:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2263
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2199:
	.cfi_restore_state
	movzbl	-8(%rax), %edx
	movq	-16(%rax), %r15
	subq	$16, %rax
	movq	%rax, 192(%rdi)
	cmpb	%dl, %r12b
	je	.L2204
	cmpb	$6, %r12b
	sete	%al
	cmpb	$8, %dl
	sete	%cl
	testb	%al, %al
	je	.L2245
	testb	%cl, %cl
	jne	.L2204
.L2245:
	leal	-7(%rdx), %edi
	andl	$253, %edi
	jne	.L2246
	testb	%al, %al
	jne	.L2204
.L2246:
	leal	-7(%r12), %eax
	testb	$-3, %al
	sete	%al
	testb	%cl, %al
	jne	.L2204
	cmpb	$10, %r12b
	setne	%cl
	cmpb	$10, %dl
	setne	%al
	testb	%al, %cl
	je	.L2204
	cmpb	$9, %dl
	ja	.L2208
	leaq	.L2210(%rip), %rcx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_,"aG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_,comdat
	.align 4
	.align 4
.L2210:
	.long	.L2219-.L2210
	.long	.L2241-.L2210
	.long	.L2217-.L2210
	.long	.L2216-.L2210
	.long	.L2215-.L2210
	.long	.L2214-.L2210
	.long	.L2213-.L2210
	.long	.L2212-.L2210
	.long	.L2211-.L2210
	.long	.L2209-.L2210
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_,"axG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_,comdat
	.p2align 4,,10
	.p2align 3
.L2262:
	leaq	-57(%rbp), %rdx
	leaq	16(%rbx), %rsi
	movb	%r13b, -57(%rbp)
	leaq	176(%rbx), %rdi
	call	_ZNSt6vectorIN2v88internal4wasm9ValueBaseENS1_13ZoneAllocatorIS3_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_
	jmp	.L2198
	.p2align 4,,10
	.p2align 3
.L2261:
	leaq	.LC482(%rip), %r14
	cmpq	%r12, 24(%rdi)
	jbe	.L2201
	movzbl	(%r12), %r15d
	movl	%r15d, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	testb	%al, %al
	je	.L2264
	leaq	1(%r12), %rax
	cmpq	%rax, 24(%rbx)
	ja	.L2203
.L2260:
	movq	16(%rbx), %r12
.L2201:
	movq	%r14, %rcx
	leaq	.LC530(%rip), %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L2204
	.p2align 4,,10
	.p2align 3
.L2203:
	movzbl	1(%r12), %edi
	sall	$8, %r15d
	orl	%r15d, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	%rax, %r14
	jmp	.L2260
	.p2align 4,,10
	.p2align 3
.L2264:
	movl	%r15d, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	16(%rbx), %r12
	movq	%rax, %r14
	jmp	.L2201
.L2263:
	call	__stack_chk_fail@PLT
	.p2align 4,,10
	.p2align 3
.L2217:
	leaq	.LC500(%rip), %rax
	movq	%rax, -72(%rbp)
	.p2align 4,,10
	.p2align 3
.L2218:
	movq	24(%rbx), %rdx
	leaq	.LC482(%rip), %r14
	cmpq	%rdx, %r15
	jnb	.L2220
	movzbl	(%r15), %edi
	movb	%r8b, -88(%rbp)
	movl	%edi, -80(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	-80(%rbp), %edi
	movzbl	-88(%rbp), %r8d
	testb	%al, %al
	je	.L2265
	movq	24(%rbx), %rdx
	leaq	1(%r15), %rax
	cmpq	%rax, %rdx
	jbe	.L2220
	movzbl	1(%r15), %eax
	sall	$8, %edi
	movb	%r8b, -80(%rbp)
	orl	%eax, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	24(%rbx), %rdx
	movzbl	-80(%rbp), %r8d
	movq	%rax, %r14
	.p2align 4,,10
	.p2align 3
.L2220:
	cmpb	$9, %r12b
	ja	.L2222
	leaq	.L2224(%rip), %rcx
	movslq	(%rcx,%r8,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_,"aG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_,comdat
	.align 4
	.align 4
.L2224:
	.long	.L2233-.L2224
	.long	.L2244-.L2224
	.long	.L2231-.L2224
	.long	.L2230-.L2224
	.long	.L2229-.L2224
	.long	.L2228-.L2224
	.long	.L2227-.L2224
	.long	.L2226-.L2224
	.long	.L2225-.L2224
	.long	.L2223-.L2224
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_,"axG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_,comdat
	.p2align 4,,10
	.p2align 3
.L2231:
	leaq	.LC500(%rip), %r9
	.p2align 4,,10
	.p2align 3
.L2232:
	movq	16(%rbx), %r12
	cmpq	%rdx, %r12
	jnb	.L2237
	movzbl	(%r12), %edi
	movq	%r9, -88(%rbp)
	movl	%edi, -80(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	-80(%rbp), %edi
	movq	-88(%rbp), %r9
	testb	%al, %al
	je	.L2266
	leaq	1(%r12), %rax
	cmpq	%rax, 24(%rbx)
	jbe	.L2237
	movzbl	1(%r12), %eax
	sall	$8, %edi
	movq	%r9, -80(%rbp)
	orl	%eax, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	-80(%rbp), %r9
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L2235:
	pushq	-72(%rbp)
	leaq	.LC531(%rip), %rdx
	xorl	%eax, %eax
	xorl	%r8d, %r8d
	pushq	%r14
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	popq	%rax
	popq	%rdx
	jmp	.L2204
	.p2align 4,,10
	.p2align 3
.L2244:
	leaq	.LC490(%rip), %r9
	jmp	.L2232
	.p2align 4,,10
	.p2align 3
.L2241:
	leaq	.LC490(%rip), %rax
	movq	%rax, -72(%rbp)
	jmp	.L2218
	.p2align 4,,10
	.p2align 3
.L2219:
	leaq	.LC498(%rip), %rax
	movq	%rax, -72(%rbp)
	jmp	.L2218
	.p2align 4,,10
	.p2align 3
.L2233:
	leaq	.LC498(%rip), %r9
	jmp	.L2232
	.p2align 4,,10
	.p2align 3
.L2223:
	leaq	.LC496(%rip), %r9
	jmp	.L2232
	.p2align 4,,10
	.p2align 3
.L2225:
	leaq	.LC495(%rip), %r9
	jmp	.L2232
	.p2align 4,,10
	.p2align 3
.L2226:
	leaq	.LC494(%rip), %r9
	jmp	.L2232
	.p2align 4,,10
	.p2align 3
.L2227:
	leaq	.LC493(%rip), %r9
	jmp	.L2232
	.p2align 4,,10
	.p2align 3
.L2228:
	leaq	.LC497(%rip), %r9
	jmp	.L2232
	.p2align 4,,10
	.p2align 3
.L2229:
	leaq	.LC492(%rip), %r9
	jmp	.L2232
	.p2align 4,,10
	.p2align 3
.L2230:
	leaq	.LC491(%rip), %r9
	jmp	.L2232
	.p2align 4,,10
	.p2align 3
.L2209:
	leaq	.LC496(%rip), %rax
	movq	%rax, -72(%rbp)
	jmp	.L2218
	.p2align 4,,10
	.p2align 3
.L2211:
	leaq	.LC495(%rip), %rax
	movq	%rax, -72(%rbp)
	jmp	.L2218
	.p2align 4,,10
	.p2align 3
.L2212:
	leaq	.LC494(%rip), %rax
	movq	%rax, -72(%rbp)
	jmp	.L2218
	.p2align 4,,10
	.p2align 3
.L2213:
	leaq	.LC493(%rip), %rax
	movq	%rax, -72(%rbp)
	jmp	.L2218
	.p2align 4,,10
	.p2align 3
.L2214:
	leaq	.LC497(%rip), %rax
	movq	%rax, -72(%rbp)
	jmp	.L2218
	.p2align 4,,10
	.p2align 3
.L2215:
	leaq	.LC492(%rip), %rax
	movq	%rax, -72(%rbp)
	jmp	.L2218
	.p2align 4,,10
	.p2align 3
.L2216:
	leaq	.LC491(%rip), %rax
	movq	%rax, -72(%rbp)
	jmp	.L2218
	.p2align 4,,10
	.p2align 3
.L2237:
	leaq	.LC482(%rip), %rcx
	jmp	.L2235
	.p2align 4,,10
	.p2align 3
.L2266:
	movq	%r9, -80(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	-80(%rbp), %r9
	movq	%rax, %rcx
	jmp	.L2235
	.p2align 4,,10
	.p2align 3
.L2265:
	movb	%r8b, -80(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	24(%rbx), %rdx
	movzbl	-80(%rbp), %r8d
	movq	%rax, %r14
	jmp	.L2220
.L2208:
	leaq	.LC489(%rip), %rax
	movq	%rax, -72(%rbp)
	jmp	.L2218
.L2222:
	leaq	.LC489(%rip), %r9
	jmp	.L2232
	.cfi_endproc
.LFE23865:
	.size	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_, .-_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE13DecodeLoadMemENS1_8LoadTypeEi,"axG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE13DecodeLoadMemENS1_8LoadTypeEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE13DecodeLoadMemENS1_8LoadTypeEi
	.type	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE13DecodeLoadMemENS1_8LoadTypeEi, @function
_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE13DecodeLoadMemENS1_8LoadTypeEi:
.LFB23574:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	80(%rdi), %rcx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	cmpb	$0, 18(%rcx)
	je	.L2268
	movslq	%edx, %rdx
	movzbl	%sil, %ebx
	leaq	_ZN2v88internal4wasm8LoadType13kLoadSizeLog2E(%rip), %rcx
	addq	%rax, %rdx
	movq	24(%rdi), %rax
	movzbl	(%rcx,%rbx), %ecx
	leaq	1(%rdx), %r15
	cmpq	%rax, %r15
	jnb	.L2269
	movzbl	1(%rdx), %esi
	leaq	2(%rdx), %r14
	movl	$1, %r13d
	movl	%esi, %r8d
	andl	$127, %r8d
	testb	%sil, %sil
	js	.L2346
.L2273:
	cmpl	%r8d, %ecx
	jnb	.L2281
	xorl	%eax, %eax
	leaq	.LC533(%rip), %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%r12), %rax
.L2281:
	cmpq	%r14, %rax
	ja	.L2347
.L2282:
	leaq	.LC534(%rip), %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC487(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
.L2284:
	movq	224(%r12), %rcx
	movq	192(%r12), %rdx
	movl	-84(%rcx), %esi
	movq	%rdx, %rax
	subq	184(%r12), %rax
	sarq	$4, %rax
	cmpq	%rax, %rsi
	jb	.L2348
	cmpb	$2, -72(%rcx)
	jne	.L2349
.L2297:
	leaq	_ZN2v88internal4wasm8LoadType10kValueTypeE(%rip), %rax
	leaq	-57(%rbp), %rdx
	movzbl	(%rax,%rbx), %eax
	leaq	16(%r12), %rsi
	leaq	176(%r12), %rdi
	movb	%al, -57(%rbp)
	call	_ZNSt6vectorIN2v88internal4wasm9ValueBaseENS1_13ZoneAllocatorIS3_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_
	movl	%r13d, %eax
.L2267:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L2350
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2269:
	.cfi_restore_state
	xorl	%eax, %eax
	leaq	.LC532(%rip), %rcx
	movq	%r15, %rsi
	movq	%r15, %r14
	leaq	.LC487(%rip), %rdx
	xorl	%r13d, %r13d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%r12), %rax
	cmpq	%r14, %rax
	jbe	.L2282
.L2347:
	cmpb	$0, (%r14)
	js	.L2283
.L2339:
	addl	$1, %r13d
	jmp	.L2284
	.p2align 4,,10
	.p2align 3
.L2348:
	movzbl	-8(%rdx), %eax
	movq	-16(%rdx), %r14
	subq	$16, %rdx
	movq	%rdx, 192(%r12)
	cmpb	$1, %al
	je	.L2297
	cmpb	$10, %al
	je	.L2297
	cmpb	$9, %al
	ja	.L2302
	leaq	.L2304(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE13DecodeLoadMemENS1_8LoadTypeEi,"aG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE13DecodeLoadMemENS1_8LoadTypeEi,comdat
	.align 4
	.align 4
.L2304:
	.long	.L2312-.L2304
	.long	.L2302-.L2304
	.long	.L2311-.L2304
	.long	.L2324-.L2304
	.long	.L2309-.L2304
	.long	.L2308-.L2304
	.long	.L2307-.L2304
	.long	.L2306-.L2304
	.long	.L2305-.L2304
	.long	.L2303-.L2304
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE13DecodeLoadMemENS1_8LoadTypeEi,"axG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE13DecodeLoadMemENS1_8LoadTypeEi,comdat
.L2324:
	leaq	.LC491(%rip), %r15
.L2310:
	movq	24(%r12), %rax
	leaq	.LC482(%rip), %rdi
	movq	%rdi, -72(%rbp)
	cmpq	%rax, %r14
	jb	.L2351
.L2313:
	movq	16(%r12), %rcx
	cmpq	%rax, %rcx
	jnb	.L2318
	movzbl	(%rcx), %edi
	movq	%rcx, -88(%rbp)
	movl	%edi, -76(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	-76(%rbp), %edi
	movq	-88(%rbp), %rcx
	testb	%al, %al
	je	.L2345
	leaq	1(%rcx), %rax
	cmpq	%rax, 24(%r12)
	ja	.L2352
.L2318:
	leaq	.LC482(%rip), %rcx
.L2316:
	pushq	%r15
	leaq	.LC531(%rip), %rdx
	xorl	%eax, %eax
	xorl	%r8d, %r8d
	pushq	-72(%rbp)
	leaq	.LC490(%rip), %r9
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	popq	%rax
	popq	%rdx
	jmp	.L2297
.L2311:
	leaq	.LC500(%rip), %r15
	jmp	.L2310
	.p2align 4,,10
	.p2align 3
.L2346:
	cmpq	%r14, %rax
	jbe	.L2271
	movzbl	2(%rdx), %edi
	leaq	3(%rdx), %r14
	movl	$2, %r13d
	movl	%edi, %esi
	sall	$7, %esi
	andl	$16256, %esi
	orl	%esi, %r8d
	testb	%dil, %dil
	jns	.L2273
	cmpq	%r14, %rax
	jbe	.L2274
	movzbl	3(%rdx), %edi
	leaq	4(%rdx), %r14
	movl	$3, %r13d
	movl	%edi, %esi
	sall	$14, %esi
	andl	$2080768, %esi
	orl	%esi, %r8d
	testb	%dil, %dil
	jns	.L2273
	cmpq	%r14, %rax
	ja	.L2353
	xorl	%eax, %eax
	leaq	.LC532(%rip), %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	.LC487(%rip), %rdx
	movl	$3, %r13d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%r12), %rax
	jmp	.L2281
	.p2align 4,,10
	.p2align 3
.L2283:
	leaq	1(%r14), %rsi
	cmpq	%rax, %rsi
	jnb	.L2285
	cmpb	$0, 1(%r14)
	js	.L2286
.L2340:
	addl	$2, %r13d
	jmp	.L2284
	.p2align 4,,10
	.p2align 3
.L2268:
	leaq	-1(%rax), %rsi
	leaq	.LC535(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	xorl	%eax, %eax
	jmp	.L2267
	.p2align 4,,10
	.p2align 3
.L2285:
	leaq	.LC534(%rip), %rcx
	leaq	.LC487(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L2339
	.p2align 4,,10
	.p2align 3
.L2271:
	xorl	%eax, %eax
	leaq	.LC532(%rip), %rcx
	leaq	.LC487(%rip), %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%r12), %rax
	jmp	.L2281
	.p2align 4,,10
	.p2align 3
.L2351:
	movzbl	(%r14), %edi
	movl	%edi, -76(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	-76(%rbp), %edi
	testb	%al, %al
	je	.L2344
	movq	24(%r12), %rax
	leaq	1(%r14), %rcx
	cmpq	%rcx, %rax
	jbe	.L2313
	movzbl	1(%r14), %eax
	sall	$8, %edi
	orl	%eax, %edi
.L2344:
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	%rax, -72(%rbp)
	movq	24(%r12), %rax
	jmp	.L2313
	.p2align 4,,10
	.p2align 3
.L2349:
	movq	16(%r12), %r14
	leaq	.LC482(%rip), %r15
	cmpq	24(%r12), %r14
	jnb	.L2298
	movzbl	(%r14), %edi
	movl	%edi, -72(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	-72(%rbp), %edi
	testb	%al, %al
	je	.L2342
	leaq	1(%r14), %rax
	cmpq	%rax, 24(%r12)
	jbe	.L2343
	movzbl	1(%r14), %eax
	sall	$8, %edi
	orl	%eax, %edi
.L2342:
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	%rax, %r15
.L2343:
	movq	16(%r12), %r14
.L2298:
	movq	%r15, %rcx
	leaq	.LC530(%rip), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L2297
	.p2align 4,,10
	.p2align 3
.L2352:
	movzbl	1(%rcx), %eax
	sall	$8, %edi
	orl	%eax, %edi
.L2345:
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	%rax, %rcx
	jmp	.L2316
	.p2align 4,,10
	.p2align 3
.L2286:
	leaq	2(%r14), %rsi
	cmpq	%rax, %rsi
	jnb	.L2287
	cmpb	$0, 2(%r14)
	js	.L2288
.L2341:
	addl	$3, %r13d
	jmp	.L2284
.L2305:
	leaq	.LC495(%rip), %r15
	jmp	.L2310
.L2302:
	leaq	.LC489(%rip), %r15
	jmp	.L2310
.L2312:
	leaq	.LC498(%rip), %r15
	jmp	.L2310
.L2303:
	leaq	.LC496(%rip), %r15
	jmp	.L2310
.L2309:
	leaq	.LC492(%rip), %r15
	jmp	.L2310
.L2308:
	leaq	.LC497(%rip), %r15
	jmp	.L2310
.L2307:
	leaq	.LC493(%rip), %r15
	jmp	.L2310
.L2306:
	leaq	.LC494(%rip), %r15
	jmp	.L2310
	.p2align 4,,10
	.p2align 3
.L2287:
	leaq	.LC534(%rip), %rcx
	leaq	.LC487(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L2340
	.p2align 4,,10
	.p2align 3
.L2274:
	xorl	%eax, %eax
	leaq	.LC532(%rip), %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	.LC487(%rip), %rdx
	movl	$2, %r13d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%r12), %rax
	jmp	.L2281
	.p2align 4,,10
	.p2align 3
.L2288:
	leaq	3(%r14), %rsi
	cmpq	%rax, %rsi
	jb	.L2354
	leaq	.LC534(%rip), %rcx
	leaq	.LC487(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L2341
.L2353:
	movzbl	4(%rdx), %edi
	movl	%edi, %esi
	sall	$21, %esi
	andl	$266338304, %esi
	orl	%esi, %r8d
	testb	%dil, %dil
	js	.L2277
	leaq	5(%rdx), %r14
	movl	$4, %r13d
	jmp	.L2273
.L2354:
	cmpb	$0, 3(%r14)
	js	.L2290
	addl	$4, %r13d
	jmp	.L2284
.L2277:
	leaq	5(%rdx), %rsi
	cmpq	%rsi, %rax
	jbe	.L2278
	movzbl	5(%rdx), %edi
	leaq	6(%rdx), %r14
	movl	%edi, %r13d
	andl	$-16, %r13d
	testb	%dil, %dil
	js	.L2355
	sall	$28, %edi
	orl	%edi, %r8d
	testb	%r13b, %r13b
	je	.L2320
.L2280:
	leaq	.LC488(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
.L2338:
	movq	24(%r12), %rax
	movl	$5, %r13d
	jmp	.L2281
.L2290:
	leaq	4(%r14), %r15
	cmpq	%rax, %r15
	jnb	.L2321
	movzbl	4(%r14), %r14d
	movl	$5, %r8d
	testb	%r14b, %r14b
	js	.L2291
.L2292:
	addl	%r8d, %r13d
	andl	$240, %r14d
	je	.L2284
	leaq	.LC488(%rip), %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	jmp	.L2284
.L2278:
	xorl	%eax, %eax
	leaq	.LC532(%rip), %rcx
	movq	%r12, %rdi
	movq	%rsi, %r14
	leaq	.LC487(%rip), %rdx
	movl	$4, %r13d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%r12), %rax
	jmp	.L2281
.L2321:
	xorl	%r14d, %r14d
	movl	$4, %r8d
.L2291:
	leaq	.LC534(%rip), %rcx
	movq	%r15, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC487(%rip), %rdx
	movl	%r8d, -72(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	-72(%rbp), %r8d
	jmp	.L2292
.L2355:
	xorl	%eax, %eax
	leaq	.LC532(%rip), %rcx
	movq	%r12, %rdi
	movq	%rsi, -72(%rbp)
	leaq	.LC487(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	testb	%r13b, %r13b
	movq	-72(%rbp), %rsi
	jne	.L2280
	jmp	.L2338
.L2350:
	call	__stack_chk_fail@PLT
.L2320:
	movl	$5, %r13d
	jmp	.L2273
	.cfi_endproc
.LFE23574:
	.size	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE13DecodeLoadMemENS1_8LoadTypeEi, .-_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE13DecodeLoadMemENS1_8LoadTypeEi
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeEPNS0_9SignatureINS1_9ValueTypeEEE.str1.1,"aMS",@progbits,1
.LC544:
	.string	"unreachable code"
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeEPNS0_9SignatureINS1_9ValueTypeEEE,"axG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeEPNS0_9SignatureINS1_9ValueTypeEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeEPNS0_9SignatureINS1_9ValueTypeEEE
	.type	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeEPNS0_9SignatureINS1_9ValueTypeEEE, @function
_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeEPNS0_9SignatureINS1_9ValueTypeEEE:
.LFB23618:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdx), %rax
	cmpq	$1, %rax
	je	.L2375
	cmpq	$2, %rax
	jne	.L2455
	movq	16(%rdx), %rdx
	movq	(%r12), %rax
	movq	224(%rdi), %rcx
	movzbl	1(%rdx,%rax), %r14d
	movq	192(%rdi), %rax
	movl	-84(%rcx), %esi
	movq	%rax, %rdx
	subq	184(%rdi), %rdx
	sarq	$4, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2456
	movzbl	-8(%rax), %edi
	movq	-16(%rax), %r13
	subq	$16, %rax
	movq	%rax, 192(%rbx)
	movl	%edi, %edx
.L2374:
	cmpb	%r14b, %dl
	je	.L2375
	leal	-7(%rdx), %eax
	cmpb	$2, %al
	ja	.L2400
	cmpb	$6, %r14b
	jne	.L2400
.L2375:
	movq	16(%r12), %rdx
	movq	(%r12), %rax
	movq	224(%rbx), %rsi
	movzbl	(%rdx,%rax), %r14d
	movq	192(%rbx), %rax
	movl	-84(%rsi), %edi
	movq	%rax, %rdx
	subq	184(%rbx), %rdx
	sarq	$4, %rdx
	cmpq	%rdx, %rdi
	jnb	.L2457
	movzbl	-8(%rax), %edx
	movq	-16(%rax), %r13
	subq	$16, %rax
	movq	%rax, 192(%rbx)
	cmpb	%r14b, %dl
	je	.L2382
	cmpb	$6, %r14b
	sete	%al
	cmpb	$8, %dl
	sete	%sil
	testb	%al, %al
	je	.L2401
	testb	%sil, %sil
	je	.L2401
.L2382:
	cmpq	$0, (%r12)
	jne	.L2458
.L2356:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2459
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2457:
	.cfi_restore_state
	cmpb	$2, -72(%rsi)
	movq	16(%rbx), %r8
	je	.L2382
	leaq	.LC482(%rip), %rcx
	cmpq	%r8, 24(%rbx)
	jbe	.L2381
	movq	%r8, %rsi
	leaq	24(%rbx), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%rbx), %r8
	movq	%rax, %rcx
.L2381:
	xorl	%eax, %eax
	leaq	.LC530(%rip), %rdx
	movq	%r8, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	cmpq	$0, (%r12)
	je	.L2356
	.p2align 4,,10
	.p2align 3
.L2458:
	movq	16(%r12), %rax
	leaq	-57(%rbp), %rdx
	leaq	16(%rbx), %rsi
	leaq	176(%rbx), %rdi
	movzbl	(%rax), %eax
	movb	%al, -57(%rbp)
	call	_ZNSt6vectorIN2v88internal4wasm9ValueBaseENS1_13ZoneAllocatorIS3_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_
	jmp	.L2356
	.p2align 4,,10
	.p2align 3
.L2456:
	cmpb	$2, -72(%rcx)
	movq	16(%rdi), %r13
	jne	.L2460
.L2372:
	movl	$10, %edi
	movl	$10, %edx
	jmp	.L2374
.L2401:
	leal	-7(%rdx), %edi
	andl	$253, %edi
	jne	.L2402
	testb	%al, %al
	jne	.L2382
.L2402:
	leal	-7(%r14), %eax
	movzbl	%dl, %edi
	testb	$-3, %al
	sete	%al
	testb	%sil, %al
	jne	.L2382
	cmpb	$10, %r14b
	setne	%sil
	cmpb	$10, %dl
	setne	%al
	testb	%al, %sil
	je	.L2382
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	24(%rbx), %r15
	movq	%rax, -80(%rbp)
	leaq	.LC482(%rip), %rax
	movq	%rax, -72(%rbp)
	cmpq	24(%rbx), %r13
	jnb	.L2386
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	%rax, -72(%rbp)
.L2386:
	movzbl	%r14b, %edi
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	16(%rbx), %rsi
	leaq	.LC482(%rip), %rcx
	movq	%rax, %r9
	cmpq	24(%rbx), %rsi
	jnb	.L2387
	movq	%r15, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-88(%rbp), %r9
	movq	%rax, %rcx
.L2387:
	pushq	-80(%rbp)
	leaq	.LC531(%rip), %rdx
	xorl	%eax, %eax
	xorl	%r8d, %r8d
	pushq	-72(%rbp)
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	popq	%rax
	popq	%rdx
	jmp	.L2382
	.p2align 4,,10
	.p2align 3
.L2460:
	leaq	.LC482(%rip), %rcx
	cmpq	%r13, 24(%rdi)
	ja	.L2461
.L2373:
	movq	%r13, %rsi
	leaq	.LC530(%rip), %rdx
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	16(%rbx), %r13
	jmp	.L2372
	.p2align 4,,10
	.p2align 3
.L2461:
	movq	%r13, %rsi
	leaq	24(%rdi), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%rbx), %r13
	movq	%rax, %rcx
	jmp	.L2373
	.p2align 4,,10
	.p2align 3
.L2400:
	leal	-7(%r14), %eax
	testb	$-3, %al
	sete	%al
	cmpb	$8, %dl
	sete	%cl
	andl	%ecx, %eax
	xorl	$1, %eax
	cmpb	$10, %dl
	setne	%dl
	testb	%dl, %al
	je	.L2375
	cmpb	$10, %r14b
	je	.L2375
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	24(%rbx), %r15
	movq	%rax, -80(%rbp)
	leaq	.LC482(%rip), %rax
	movq	%rax, -72(%rbp)
	cmpq	24(%rbx), %r13
	jnb	.L2377
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	%rax, -72(%rbp)
.L2377:
	movzbl	%r14b, %edi
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	16(%rbx), %rsi
	leaq	.LC482(%rip), %rcx
	movq	%rax, %r9
	cmpq	24(%rbx), %rsi
	jnb	.L2378
	movq	%r15, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-88(%rbp), %r9
	movq	%rax, %rcx
.L2378:
	pushq	-80(%rbp)
	movq	%r13, %rsi
	movl	$1, %r8d
	movq	%rbx, %rdi
	pushq	-72(%rbp)
	leaq	.LC531(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	popq	%rcx
	popq	%rsi
	jmp	.L2375
.L2455:
	leaq	.LC544(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2459:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23618:
	.size	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeEPNS0_9SignatureINS1_9ValueTypeEEE, .-_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeEPNS0_9SignatureINS1_9ValueTypeEEE
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE25TypeCheckUnreachableMergeERNS1_5MergeINS1_9ValueBaseEEEb,"axG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE25TypeCheckUnreachableMergeERNS1_5MergeINS1_9ValueBaseEEEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE25TypeCheckUnreachableMergeERNS1_5MergeINS1_9ValueBaseEEEb
	.type	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE25TypeCheckUnreachableMergeERNS1_5MergeINS1_9ValueBaseEEEb, @function
_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE25TypeCheckUnreachableMergeERNS1_5MergeINS1_9ValueBaseEEEb:
.LFB23907:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movb	%dl, -64(%rbp)
	movl	(%rsi), %edx
	leal	-1(%rdx), %r14d
	testl	%edx, %edx
	jle	.L2463
	movl	%r14d, %eax
	movl	%r14d, -72(%rbp)
	movq	192(%rdi), %r15
	xorl	%r13d, %r13d
	movq	%rax, -56(%rbp)
	movq	%rsi, %r14
	jmp	.L2508
	.p2align 4,,10
	.p2align 3
.L2557:
	cmpb	$2, -72(%rcx)
	movq	16(%rbx), %r15
	jne	.L2556
.L2471:
	movq	192(%rbx), %r15
	movl	%edx, %eax
	leaq	1(%r13), %rcx
	cmpq	%r13, -56(%rbp)
	je	.L2550
.L2558:
	movq	%rcx, %r13
.L2508:
	leaq	8(%r14), %rax
	cmpl	$1, %edx
	je	.L2465
	movq	%r13, %rax
	salq	$4, %rax
	addq	8(%r14), %rax
.L2465:
	movq	224(%rbx), %rcx
	movzbl	8(%rax), %r12d
	movq	%r15, %rax
	subq	184(%rbx), %rax
	movl	-84(%rcx), %esi
	sarq	$4, %rax
	cmpq	%rax, %rsi
	jnb	.L2557
	movzbl	-8(%r15), %eax
	movq	-16(%r15), %rsi
	subq	$16, %r15
	movq	%r15, 192(%rbx)
	cmpb	%al, %r12b
	je	.L2555
	cmpb	$6, %r12b
	sete	%dl
	cmpb	$8, %al
	sete	%cl
	testb	%dl, %dl
	je	.L2531
	testb	%cl, %cl
	je	.L2531
.L2555:
	movl	(%r14), %edx
	leaq	1(%r13), %rcx
	movl	%edx, %eax
	cmpq	%r13, -56(%rbp)
	jne	.L2558
.L2550:
	movq	%r14, %r12
	movl	-72(%rbp), %r14d
.L2507:
	movslq	%r14d, %r13
	leaq	8(%r12), %r11
	salq	$4, %r13
	jmp	.L2521
	.p2align 4,,10
	.p2align 3
.L2510:
	movq	8(%r12), %rax
	addq	%r13, %rax
	movzbl	8(%rax), %edx
	cmpq	%r15, 200(%rbx)
	je	.L2512
.L2559:
	movq	16(%rbx), %rax
	movb	%dl, 8(%r15)
	subl	$1, %r14d
	subq	$16, %r13
	movq	%rax, (%r15)
	movq	192(%rbx), %rax
	leaq	16(%rax), %r15
	movq	%r15, 192(%rbx)
	cmpl	$-1, %r14d
	je	.L2509
.L2564:
	movl	(%r12), %eax
.L2521:
	cmpl	$1, %eax
	jne	.L2510
	movq	%r11, %rax
	movzbl	8(%rax), %edx
	cmpq	%r15, 200(%rbx)
	jne	.L2559
.L2512:
	movq	184(%rbx), %r8
	movq	%r15, %rcx
	subq	%r8, %rcx
	movq	%rcx, %rax
	sarq	$4, %rax
	cmpq	$134217727, %rax
	je	.L2560
	testq	%rax, %rax
	je	.L2527
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L2561
	movl	$2147483632, %esi
	movl	$2147483632, %r9d
.L2515:
	movq	176(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %r10
	subq	%rax, %r10
	cmpq	%rsi, %r10
	jb	.L2562
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L2518:
	addq	%rax, %r9
	leaq	16(%rax), %rdi
	jmp	.L2516
.L2531:
	leal	-7(%rax), %edi
	andl	$253, %edi
	jne	.L2532
	testb	%dl, %dl
	jne	.L2555
.L2532:
	leal	-7(%r12), %edx
	andb	$-3, %dl
	sete	%dl
	testb	%cl, %dl
	jne	.L2554
	cmpb	$10, %r12b
	setne	%cl
	cmpb	$10, %al
	setne	%dl
	testb	%dl, %cl
	jne	.L2475
.L2554:
	movl	(%r14), %edx
	jmp	.L2471
	.p2align 4,,10
	.p2align 3
.L2556:
	leaq	.LC482(%rip), %rcx
	cmpq	%r15, 24(%rbx)
	jbe	.L2468
	movzbl	(%r15), %edi
	movl	%edi, -80(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	-80(%rbp), %edi
	testb	%al, %al
	je	.L2553
	leaq	1(%r15), %rax
	cmpq	%rax, 24(%rbx)
	ja	.L2470
	movq	16(%rbx), %r15
	leaq	.LC482(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L2468:
	leaq	.LC530(%rip), %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	(%r14), %edx
	jmp	.L2471
	.p2align 4,,10
	.p2align 3
.L2561:
	testq	%rsi, %rsi
	jne	.L2563
	movl	$16, %edi
	xorl	%r9d, %r9d
	xorl	%eax, %eax
.L2516:
	movq	16(%rbx), %rsi
	addq	%rax, %rcx
	movb	%dl, 8(%rcx)
	movq	%rsi, (%rcx)
	cmpq	%r15, %r8
	je	.L2530
	movq	%r8, %rdx
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L2520:
	movq	(%rdx), %rdi
	movzbl	8(%rdx), %esi
	addq	$16, %rdx
	addq	$16, %rcx
	movq	%rdi, -16(%rcx)
	movb	%sil, -8(%rcx)
	cmpq	%rdx, %r15
	jne	.L2520
	subq	%r8, %r15
	leaq	16(%rax,%r15), %r15
.L2519:
	movq	%rax, %xmm0
	movq	%r15, %xmm1
	subl	$1, %r14d
	movq	%r9, 200(%rbx)
	punpcklqdq	%xmm1, %xmm0
	subq	$16, %r13
	movups	%xmm0, 184(%rbx)
	cmpl	$-1, %r14d
	jne	.L2564
.L2509:
	cmpq	$0, 56(%rbx)
	sete	%al
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2470:
	.cfi_restore_state
	movzbl	1(%r15), %eax
	sall	$8, %edi
	orl	%eax, %edi
.L2553:
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	16(%rbx), %r15
	movq	%rax, %rcx
	jmp	.L2468
	.p2align 4,,10
	.p2align 3
.L2527:
	movl	$16, %esi
	movl	$16, %r9d
	jmp	.L2515
	.p2align 4,,10
	.p2align 3
.L2530:
	movq	%rdi, %r15
	jmp	.L2519
	.p2align 4,,10
	.p2align 3
.L2463:
	testl	%r14d, %r14d
	js	.L2509
	movq	192(%rdi), %r15
	movl	%edx, %eax
	jmp	.L2507
	.p2align 4,,10
	.p2align 3
.L2562:
	movq	%r11, -88(%rbp)
	movq	%r9, -80(%rbp)
	movq	%rcx, -72(%rbp)
	movq	%r8, -64(%rbp)
	movb	%dl, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movzbl	-56(%rbp), %edx
	movq	-64(%rbp), %r8
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %r9
	movq	-88(%rbp), %r11
	jmp	.L2518
.L2560:
	leaq	.LC506(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L2475:
	cmpb	$9, %al
	ja	.L2477
	leaq	.L2479(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE25TypeCheckUnreachableMergeERNS1_5MergeINS1_9ValueBaseEEEb,"aG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE25TypeCheckUnreachableMergeERNS1_5MergeINS1_9ValueBaseEEEb,comdat
	.align 4
	.align 4
.L2479:
	.long	.L2488-.L2479
	.long	.L2523-.L2479
	.long	.L2486-.L2479
	.long	.L2485-.L2479
	.long	.L2484-.L2479
	.long	.L2483-.L2479
	.long	.L2482-.L2479
	.long	.L2481-.L2479
	.long	.L2480-.L2479
	.long	.L2478-.L2479
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE25TypeCheckUnreachableMergeERNS1_5MergeINS1_9ValueBaseEEEb,"axG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE25TypeCheckUnreachableMergeERNS1_5MergeINS1_9ValueBaseEEEb,comdat
.L2486:
	leaq	.LC500(%rip), %r15
	.p2align 4,,10
	.p2align 3
.L2487:
	movq	24(%rbx), %rdx
	leaq	.LC482(%rip), %r11
	cmpq	%rdx, %rsi
	jnb	.L2489
	movzbl	(%rsi), %edi
	movq	%rsi, -88(%rbp)
	movl	%edi, -80(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	-80(%rbp), %edi
	movq	-88(%rbp), %rsi
	testb	%al, %al
	je	.L2565
	movq	24(%rbx), %rdx
	leaq	1(%rsi), %rax
	leaq	.LC482(%rip), %r11
	cmpq	%rax, %rdx
	jbe	.L2489
	movzbl	1(%rsi), %eax
	sall	$8, %edi
	movq	%rsi, -80(%rbp)
	orl	%eax, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	24(%rbx), %rdx
	movq	-80(%rbp), %rsi
	movq	%rax, %r11
	.p2align 4,,10
	.p2align 3
.L2489:
	cmpb	$9, %r12b
	ja	.L2491
	leaq	.L2493(%rip), %rdi
	movzbl	%r12b, %r8d
	movslq	(%rdi,%r8,4), %rax
	addq	%rdi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE25TypeCheckUnreachableMergeERNS1_5MergeINS1_9ValueBaseEEEb,"aG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE25TypeCheckUnreachableMergeERNS1_5MergeINS1_9ValueBaseEEEb,comdat
	.align 4
	.align 4
.L2493:
	.long	.L2502-.L2493
	.long	.L2526-.L2493
	.long	.L2500-.L2493
	.long	.L2499-.L2493
	.long	.L2498-.L2493
	.long	.L2497-.L2493
	.long	.L2496-.L2493
	.long	.L2495-.L2493
	.long	.L2494-.L2493
	.long	.L2492-.L2493
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE25TypeCheckUnreachableMergeERNS1_5MergeINS1_9ValueBaseEEEb,"axG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE25TypeCheckUnreachableMergeERNS1_5MergeINS1_9ValueBaseEEEb,comdat
.L2500:
	leaq	.LC500(%rip), %r9
	.p2align 4,,10
	.p2align 3
.L2501:
	movq	16(%rbx), %rcx
	cmpq	%rdx, %rcx
	jnb	.L2506
	movzbl	(%rcx), %edi
	movq	%r9, -112(%rbp)
	movq	%r11, -104(%rbp)
	movq	%rsi, -96(%rbp)
	movq	%rcx, -88(%rbp)
	movl	%edi, -80(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	-80(%rbp), %edi
	movq	-88(%rbp), %rcx
	testb	%al, %al
	movq	-96(%rbp), %rsi
	movq	-104(%rbp), %r11
	movq	-112(%rbp), %r9
	je	.L2566
	leaq	1(%rcx), %rax
	cmpq	%rax, 24(%rbx)
	jbe	.L2506
	movzbl	1(%rcx), %eax
	sall	$8, %edi
	movq	%r9, -96(%rbp)
	movq	%r11, -88(%rbp)
	orl	%eax, %edi
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %r11
	movq	-80(%rbp), %rsi
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L2504:
	pushq	%r15
	movzbl	-64(%rbp), %edx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	pushq	%r11
	leal	(%rdx,%r13), %r8d
	leaq	.LC531(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	popq	%rax
	movq	192(%rbx), %r15
	popq	%rdx
	jmp	.L2555
.L2526:
	leaq	.LC490(%rip), %r9
	jmp	.L2501
.L2523:
	leaq	.LC490(%rip), %r15
	jmp	.L2487
.L2492:
	leaq	.LC496(%rip), %r9
	jmp	.L2501
.L2494:
	leaq	.LC495(%rip), %r9
	jmp	.L2501
.L2495:
	leaq	.LC494(%rip), %r9
	jmp	.L2501
.L2502:
	leaq	.LC498(%rip), %r9
	jmp	.L2501
.L2488:
	leaq	.LC498(%rip), %r15
	jmp	.L2487
.L2478:
	leaq	.LC496(%rip), %r15
	jmp	.L2487
.L2496:
	leaq	.LC493(%rip), %r9
	jmp	.L2501
.L2497:
	leaq	.LC497(%rip), %r9
	jmp	.L2501
.L2498:
	leaq	.LC492(%rip), %r9
	jmp	.L2501
.L2499:
	leaq	.LC491(%rip), %r9
	jmp	.L2501
.L2480:
	leaq	.LC495(%rip), %r15
	jmp	.L2487
.L2481:
	leaq	.LC494(%rip), %r15
	jmp	.L2487
.L2482:
	leaq	.LC493(%rip), %r15
	jmp	.L2487
.L2483:
	leaq	.LC497(%rip), %r15
	jmp	.L2487
.L2484:
	leaq	.LC492(%rip), %r15
	jmp	.L2487
.L2485:
	leaq	.LC491(%rip), %r15
	jmp	.L2487
	.p2align 4,,10
	.p2align 3
.L2506:
	leaq	.LC482(%rip), %rcx
	jmp	.L2504
	.p2align 4,,10
	.p2align 3
.L2566:
	movq	%r9, -96(%rbp)
	movq	%r11, -88(%rbp)
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	-80(%rbp), %rsi
	movq	-88(%rbp), %r11
	movq	-96(%rbp), %r9
	movq	%rax, %rcx
	jmp	.L2504
	.p2align 4,,10
	.p2align 3
.L2565:
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	24(%rbx), %rdx
	movq	-80(%rbp), %rsi
	movq	%rax, %r11
	jmp	.L2489
.L2491:
	leaq	.LC489(%rip), %r9
	jmp	.L2501
.L2477:
	leaq	.LC489(%rip), %r15
	jmp	.L2487
.L2563:
	cmpq	$134217727, %rsi
	movl	$134217727, %r9d
	cmovbe	%rsi, %r9
	salq	$4, %r9
	movq	%r9, %rsi
	jmp	.L2515
	.cfi_endproc
.LFE23907:
	.size	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE25TypeCheckUnreachableMergeERNS1_5MergeINS1_9ValueBaseEEEb, .-_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE25TypeCheckUnreachableMergeERNS1_5MergeINS1_9ValueBaseEEEb
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE17TypeCheckFallThruEv.str1.8,"aMS",@progbits,1
	.align 8
.LC545:
	.string	"expected %u elements on the stack for fallthru to @%d, found %u"
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE17TypeCheckFallThruEv,"axG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE17TypeCheckFallThruEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE17TypeCheckFallThruEv
	.type	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE17TypeCheckFallThruEv, @function
_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE17TypeCheckFallThruEv:
.LFB23476:
	.cfi_startproc
	endbr64
	movq	192(%rdi), %r8
	movq	224(%rdi), %rsi
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%r8, %r9
	subq	184(%rdi), %r9
	movl	-32(%rsi), %ecx
	sarq	$4, %r9
	subl	-84(%rsi), %r9d
	cmpb	$0, -72(%rsi)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	jne	.L2568
	cmpl	%r9d, %ecx
	jne	.L2620
	movl	$1, %eax
	testl	%ecx, %ecx
	jne	.L2621
.L2567:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2621:
	.cfi_restore_state
	movl	%ecx, %r9d
	salq	$4, %r9
	cmpl	$1, %ecx
	je	.L2571
	movq	-24(%rsi), %r11
	xorl	%eax, %eax
	xorl	%r10d, %r10d
	subq	%r9, %r8
	.p2align 4,,10
	.p2align 3
.L2575:
	movzbl	8(%r11,%rax), %edx
	movzbl	8(%r8,%rax), %esi
	cmpb	%dl, %sil
	je	.L2572
	leal	-7(%rsi), %r9d
	cmpb	$2, %r9b
	ja	.L2608
	cmpb	$6, %dl
	jne	.L2608
.L2572:
	addl	$1, %r10d
	addq	$16, %rax
	cmpl	%ecx, %r10d
	jne	.L2575
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2620:
	.cfi_restore_state
	movq	-80(%rsi), %r8
	movq	16(%rdi), %rsi
	xorl	%eax, %eax
	leaq	.LC545(%rip), %rdx
	subl	8(%rdi), %r8d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2571:
	.cfi_restore_state
	subq	%r9, %r8
	movzbl	-16(%rsi), %edx
	movzbl	8(%r8), %esi
	cmpb	%dl, %sil
	je	.L2567
	leal	-7(%rsi), %eax
	cmpb	$2, %al
	setbe	%al
	cmpb	$6, %dl
	sete	%cl
	andb	%cl, %al
	jne	.L2567
	leal	-7(%rdx), %eax
	testb	$-3, %al
	sete	%al
	cmpb	$8, %sil
	sete	%cl
	andb	%cl, %al
	jne	.L2567
	xorl	%r10d, %r10d
.L2574:
	cmpb	$10, %sil
	ja	.L2576
	leaq	.L2578(%rip), %rcx
	movslq	(%rcx,%rsi,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE17TypeCheckFallThruEv,"aG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE17TypeCheckFallThruEv,comdat
	.align 4
	.align 4
.L2578:
	.long	.L2588-.L2578
	.long	.L2606-.L2578
	.long	.L2586-.L2578
	.long	.L2585-.L2578
	.long	.L2584-.L2578
	.long	.L2583-.L2578
	.long	.L2582-.L2578
	.long	.L2581-.L2578
	.long	.L2580-.L2578
	.long	.L2579-.L2578
	.long	.L2577-.L2578
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE17TypeCheckFallThruEv,"axG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE17TypeCheckFallThruEv,comdat
	.p2align 4,,10
	.p2align 3
.L2608:
	leal	-7(%rdx), %r9d
	andl	$253, %r9d
	jne	.L2574
	cmpb	$8, %sil
	je	.L2572
	jmp	.L2574
	.p2align 4,,10
	.p2align 3
.L2568:
	cmpl	%r9d, %ecx
	jl	.L2620
	subq	$32, %rsi
	xorl	%edx, %edx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE25TypeCheckUnreachableMergeERNS1_5MergeINS1_9ValueBaseEEEb
.L2579:
	.cfi_restore_state
	leaq	.LC496(%rip), %r9
	.p2align 4,,10
	.p2align 3
.L2587:
	cmpb	$10, %dl
	ja	.L2589
	leaq	.L2591(%rip), %rcx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE17TypeCheckFallThruEv,"aG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE17TypeCheckFallThruEv,comdat
	.align 4
	.align 4
.L2591:
	.long	.L2601-.L2591
	.long	.L2607-.L2591
	.long	.L2599-.L2591
	.long	.L2598-.L2591
	.long	.L2597-.L2591
	.long	.L2596-.L2591
	.long	.L2595-.L2591
	.long	.L2594-.L2591
	.long	.L2593-.L2591
	.long	.L2592-.L2591
	.long	.L2590-.L2591
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE17TypeCheckFallThruEv,"axG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE17TypeCheckFallThruEv,comdat
	.p2align 4,,10
	.p2align 3
.L2599:
	leaq	.LC500(%rip), %r8
.L2600:
	movq	16(%rdi), %rsi
	xorl	%eax, %eax
	movl	%r10d, %ecx
	leaq	.LC529(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2607:
	.cfi_restore_state
	leaq	.LC490(%rip), %r8
	jmp	.L2600
	.p2align 4,,10
	.p2align 3
.L2601:
	leaq	.LC498(%rip), %r8
	jmp	.L2600
	.p2align 4,,10
	.p2align 3
.L2590:
	leaq	.LC499(%rip), %r8
	jmp	.L2600
	.p2align 4,,10
	.p2align 3
.L2592:
	leaq	.LC496(%rip), %r8
	jmp	.L2600
	.p2align 4,,10
	.p2align 3
.L2593:
	leaq	.LC495(%rip), %r8
	jmp	.L2600
	.p2align 4,,10
	.p2align 3
.L2594:
	leaq	.LC494(%rip), %r8
	jmp	.L2600
	.p2align 4,,10
	.p2align 3
.L2595:
	leaq	.LC493(%rip), %r8
	jmp	.L2600
	.p2align 4,,10
	.p2align 3
.L2596:
	leaq	.LC497(%rip), %r8
	jmp	.L2600
	.p2align 4,,10
	.p2align 3
.L2597:
	leaq	.LC492(%rip), %r8
	jmp	.L2600
	.p2align 4,,10
	.p2align 3
.L2598:
	leaq	.LC491(%rip), %r8
	jmp	.L2600
.L2581:
	leaq	.LC494(%rip), %r9
	jmp	.L2587
.L2582:
	leaq	.LC493(%rip), %r9
	jmp	.L2587
.L2580:
	leaq	.LC495(%rip), %r9
	jmp	.L2587
.L2583:
	leaq	.LC497(%rip), %r9
	jmp	.L2587
.L2584:
	leaq	.LC492(%rip), %r9
	jmp	.L2587
.L2585:
	leaq	.LC491(%rip), %r9
	jmp	.L2587
.L2588:
	leaq	.LC498(%rip), %r9
	jmp	.L2587
.L2577:
	leaq	.LC499(%rip), %r9
	jmp	.L2587
.L2606:
	leaq	.LC490(%rip), %r9
	jmp	.L2587
.L2586:
	leaq	.LC500(%rip), %r9
	jmp	.L2587
.L2576:
	leaq	.LC489(%rip), %r9
	jmp	.L2587
.L2589:
	leaq	.LC489(%rip), %r8
	jmp	.L2600
	.cfi_endproc
.LFE23476:
	.size	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE17TypeCheckFallThruEv, .-_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE17TypeCheckFallThruEv
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15TypeCheckBranchEPNS1_11ControlBaseINS1_9ValueBaseEEEb.str1.8,"aMS",@progbits,1
	.align 8
.LC546:
	.string	"expected %u elements on the stack for br to @%d, found %u"
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15TypeCheckBranchEPNS1_11ControlBaseINS1_9ValueBaseEEEb,"axG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15TypeCheckBranchEPNS1_11ControlBaseINS1_9ValueBaseEEEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15TypeCheckBranchEPNS1_11ControlBaseINS1_9ValueBaseEEEb
	.type	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15TypeCheckBranchEPNS1_11ControlBaseINS1_9ValueBaseEEEb, @function
_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15TypeCheckBranchEPNS1_11ControlBaseINS1_9ValueBaseEEEb:
.LFB23470:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	224(%rdi), %rcx
	movzbl	(%rsi), %r8d
	cmpb	$0, -72(%rcx)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	jne	.L2623
	leaq	24(%rsi), %rdx
	leaq	56(%rsi), %rax
	cmpb	$3, %r8b
	cmove	%rdx, %rax
	movl	(%rax), %r10d
	testl	%r10d, %r10d
	je	.L2622
	movq	192(%rdi), %rdx
	movq	%rdx, %r9
	subq	184(%rdi), %r9
	sarq	$4, %r9
	subl	-84(%rcx), %r9d
	cmpl	%r9d, %r10d
	ja	.L2692
	movl	%r10d, %ecx
	salq	$4, %rcx
	movq	%rcx, %rsi
	cmpl	$1, %r10d
	je	.L2628
	subq	%rsi, %rdx
	movq	8(%rax), %r9
	xorl	%ecx, %ecx
	xorl	%r11d, %r11d
	movq	%rdx, %r8
	.p2align 4,,10
	.p2align 3
.L2632:
	movzbl	8(%r9,%rcx), %eax
	movzbl	8(%r8,%rcx), %edx
	cmpb	%al, %dl
	je	.L2629
	leal	-7(%rdx), %esi
	cmpb	$2, %sil
	ja	.L2670
	cmpb	$6, %al
	jne	.L2670
.L2629:
	addl	$1, %r11d
	addq	$16, %rcx
	cmpl	%r11d, %r10d
	jne	.L2632
	xorl	%r10d, %r10d
.L2622:
	movl	%r10d, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2670:
	.cfi_restore_state
	leal	-7(%rax), %esi
	andl	$253, %esi
	jne	.L2631
	cmpb	$8, %dl
	je	.L2629
.L2631:
	cmpb	$10, %dl
	ja	.L2635
	leaq	.L2637(%rip), %rcx
	movslq	(%rcx,%rdx,4), %rdx
	addq	%rcx, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15TypeCheckBranchEPNS1_11ControlBaseINS1_9ValueBaseEEEb,"aG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15TypeCheckBranchEPNS1_11ControlBaseINS1_9ValueBaseEEEb,comdat
	.align 4
	.align 4
.L2637:
	.long	.L2647-.L2637
	.long	.L2667-.L2637
	.long	.L2645-.L2637
	.long	.L2644-.L2637
	.long	.L2643-.L2637
	.long	.L2642-.L2637
	.long	.L2641-.L2637
	.long	.L2640-.L2637
	.long	.L2639-.L2637
	.long	.L2638-.L2637
	.long	.L2636-.L2637
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15TypeCheckBranchEPNS1_11ControlBaseINS1_9ValueBaseEEEb,"axG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15TypeCheckBranchEPNS1_11ControlBaseINS1_9ValueBaseEEEb,comdat
	.p2align 4,,10
	.p2align 3
.L2692:
	movq	8(%rsi), %r8
	movq	16(%rdi), %rsi
	movl	%r10d, %ecx
	xorl	%eax, %eax
	subl	8(%rdi), %r8d
	leaq	.LC546(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	$2, %r10d
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	%r10d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2628:
	.cfi_restore_state
	subq	%rcx, %rdx
	movzbl	16(%rax), %eax
	xorl	%r10d, %r10d
	movzbl	8(%rdx), %edx
	cmpb	%al, %dl
	je	.L2622
	leal	-7(%rdx), %ecx
	cmpb	$2, %cl
	ja	.L2671
	cmpb	$6, %al
	je	.L2622
.L2671:
	leal	-7(%rax), %ecx
	andl	$253, %ecx
	jne	.L2672
	xorl	%r10d, %r10d
	cmpb	$8, %dl
	je	.L2622
.L2672:
	xorl	%r11d, %r11d
	jmp	.L2631
	.p2align 4,,10
	.p2align 3
.L2623:
	leaq	24(%rsi), %rax
	addq	$56, %rsi
	cmpb	$3, %r8b
	movzbl	%dl, %edx
	cmove	%rax, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE25TypeCheckUnreachableMergeERNS1_5MergeINS1_9ValueBaseEEEb
	xorl	%r10d, %r10d
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	testb	%al, %al
	sete	%r10b
	addl	$1, %r10d
	movl	%r10d, %eax
	ret
.L2638:
	.cfi_restore_state
	leaq	.LC496(%rip), %r9
	.p2align 4,,10
	.p2align 3
.L2646:
	cmpb	$10, %al
	ja	.L2648
	leaq	.L2650(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15TypeCheckBranchEPNS1_11ControlBaseINS1_9ValueBaseEEEb,"aG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15TypeCheckBranchEPNS1_11ControlBaseINS1_9ValueBaseEEEb,comdat
	.align 4
	.align 4
.L2650:
	.long	.L2660-.L2650
	.long	.L2668-.L2650
	.long	.L2658-.L2650
	.long	.L2657-.L2650
	.long	.L2656-.L2650
	.long	.L2655-.L2650
	.long	.L2654-.L2650
	.long	.L2653-.L2650
	.long	.L2652-.L2650
	.long	.L2651-.L2650
	.long	.L2649-.L2650
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15TypeCheckBranchEPNS1_11ControlBaseINS1_9ValueBaseEEEb,"axG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15TypeCheckBranchEPNS1_11ControlBaseINS1_9ValueBaseEEEb,comdat
	.p2align 4,,10
	.p2align 3
.L2658:
	leaq	.LC500(%rip), %r8
.L2659:
	movq	16(%rdi), %rsi
	xorl	%eax, %eax
	movl	%r11d, %ecx
	leaq	.LC529(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	$2, %r10d
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	%r10d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2668:
	.cfi_restore_state
	leaq	.LC490(%rip), %r8
	jmp	.L2659
	.p2align 4,,10
	.p2align 3
.L2660:
	leaq	.LC498(%rip), %r8
	jmp	.L2659
	.p2align 4,,10
	.p2align 3
.L2649:
	leaq	.LC499(%rip), %r8
	jmp	.L2659
	.p2align 4,,10
	.p2align 3
.L2651:
	leaq	.LC496(%rip), %r8
	jmp	.L2659
	.p2align 4,,10
	.p2align 3
.L2652:
	leaq	.LC495(%rip), %r8
	jmp	.L2659
	.p2align 4,,10
	.p2align 3
.L2653:
	leaq	.LC494(%rip), %r8
	jmp	.L2659
	.p2align 4,,10
	.p2align 3
.L2654:
	leaq	.LC493(%rip), %r8
	jmp	.L2659
	.p2align 4,,10
	.p2align 3
.L2655:
	leaq	.LC497(%rip), %r8
	jmp	.L2659
	.p2align 4,,10
	.p2align 3
.L2656:
	leaq	.LC492(%rip), %r8
	jmp	.L2659
	.p2align 4,,10
	.p2align 3
.L2657:
	leaq	.LC491(%rip), %r8
	jmp	.L2659
.L2640:
	leaq	.LC494(%rip), %r9
	jmp	.L2646
.L2641:
	leaq	.LC493(%rip), %r9
	jmp	.L2646
.L2639:
	leaq	.LC495(%rip), %r9
	jmp	.L2646
.L2642:
	leaq	.LC497(%rip), %r9
	jmp	.L2646
.L2643:
	leaq	.LC492(%rip), %r9
	jmp	.L2646
.L2644:
	leaq	.LC491(%rip), %r9
	jmp	.L2646
.L2647:
	leaq	.LC498(%rip), %r9
	jmp	.L2646
.L2636:
	leaq	.LC499(%rip), %r9
	jmp	.L2646
.L2667:
	leaq	.LC490(%rip), %r9
	jmp	.L2646
.L2645:
	leaq	.LC500(%rip), %r9
	jmp	.L2646
.L2635:
	leaq	.LC489(%rip), %r9
	jmp	.L2646
.L2648:
	leaq	.LC489(%rip), %r8
	jmp	.L2659
	.cfi_endproc
.LFE23470:
	.size	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15TypeCheckBranchEPNS1_11ControlBaseINS1_9ValueBaseEEEb, .-_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15TypeCheckBranchEPNS1_11ControlBaseINS1_9ValueBaseEEEb
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15SimdExtractLaneENS1_10WasmOpcodeENS1_9ValueTypeE.str1.1,"aMS",@progbits,1
.LC547:
	.string	"lane"
.LC548:
	.string	"invalid lane index"
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15SimdExtractLaneENS1_10WasmOpcodeENS1_9ValueTypeE,"axG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15SimdExtractLaneENS1_10WasmOpcodeENS1_9ValueTypeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15SimdExtractLaneENS1_10WasmOpcodeENS1_9ValueTypeE
	.type	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15SimdExtractLaneENS1_10WasmOpcodeENS1_9ValueTypeE, @function
_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15SimdExtractLaneENS1_10WasmOpcodeENS1_9ValueTypeE:
.LFB23982:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$40, %rsp
	movq	16(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	leaq	2(%rdx), %r8
	cmpq	%rax, %r8
	ja	.L2694
	cmpl	%r8d, %eax
	je	.L2694
	leal	-64773(%rsi), %esi
	movzbl	2(%rdx), %ecx
	cmpl	$18, %esi
	ja	.L2695
	leaq	.L2697(%rip), %rdx
	movslq	(%rdx,%rsi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15SimdExtractLaneENS1_10WasmOpcodeENS1_9ValueTypeE,"aG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15SimdExtractLaneENS1_10WasmOpcodeENS1_9ValueTypeE,comdat
	.align 4
	.align 4
.L2697:
	.long	.L2700-.L2697
	.long	.L2695-.L2697
	.long	.L2700-.L2697
	.long	.L2695-.L2697
	.long	.L2732-.L2697
	.long	.L2695-.L2697
	.long	.L2732-.L2697
	.long	.L2695-.L2697
	.long	.L2698-.L2697
	.long	.L2698-.L2697
	.long	.L2695-.L2697
	.long	.L2696-.L2697
	.long	.L2696-.L2697
	.long	.L2695-.L2697
	.long	.L2698-.L2697
	.long	.L2698-.L2697
	.long	.L2695-.L2697
	.long	.L2696-.L2697
	.long	.L2696-.L2697
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15SimdExtractLaneENS1_10WasmOpcodeENS1_9ValueTypeE,"axG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15SimdExtractLaneENS1_10WasmOpcodeENS1_9ValueTypeE,comdat
	.p2align 4,,10
	.p2align 3
.L2696:
	movl	$2, %eax
.L2699:
	cmpb	%cl, %al
	jbe	.L2703
.L2701:
	movq	224(%r12), %rcx
	movq	192(%r12), %rdx
	movl	-84(%rcx), %esi
	movq	%rdx, %rax
	subq	184(%r12), %rax
	sarq	$4, %rax
	cmpq	%rax, %rsi
	jb	.L2748
	cmpb	$2, -72(%rcx)
	jne	.L2749
.L2708:
	leaq	-57(%rbp), %rdx
	leaq	16(%r12), %rsi
	movb	%r13b, -57(%rbp)
	leaq	176(%r12), %rdi
	call	_ZNSt6vectorIN2v88internal4wasm9ValueBaseENS1_13ZoneAllocatorIS3_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_
.L2706:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2750
	leaq	-40(%rbp), %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2700:
	.cfi_restore_state
	movl	$16, %eax
	cmpb	%cl, %al
	ja	.L2701
.L2703:
	leaq	.LC548(%rip), %rdx
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	jmp	.L2706
	.p2align 4,,10
	.p2align 3
.L2748:
	movzbl	-8(%rdx), %eax
	movq	-16(%rdx), %r14
	subq	$16, %rdx
	movq	%rdx, 192(%r12)
	cmpb	$5, %al
	je	.L2708
	cmpb	$10, %al
	je	.L2708
	cmpb	$9, %al
	ja	.L2713
	leaq	.L2715(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15SimdExtractLaneENS1_10WasmOpcodeENS1_9ValueTypeE,"aG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15SimdExtractLaneENS1_10WasmOpcodeENS1_9ValueTypeE,comdat
	.align 4
	.align 4
.L2715:
	.long	.L2724-.L2715
	.long	.L2723-.L2715
	.long	.L2734-.L2715
	.long	.L2721-.L2715
	.long	.L2720-.L2715
	.long	.L2719-.L2715
	.long	.L2718-.L2715
	.long	.L2717-.L2715
	.long	.L2716-.L2715
	.long	.L2714-.L2715
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15SimdExtractLaneENS1_10WasmOpcodeENS1_9ValueTypeE,"axG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15SimdExtractLaneENS1_10WasmOpcodeENS1_9ValueTypeE,comdat
.L2734:
	leaq	.LC500(%rip), %rbx
.L2722:
	movq	24(%r12), %rax
	leaq	.LC482(%rip), %r15
	cmpq	%rax, %r14
	jnb	.L2725
	movzbl	(%r14), %edi
	movl	%edi, -68(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	-68(%rbp), %edi
	testb	%al, %al
	je	.L2746
	movq	24(%r12), %rax
	leaq	1(%r14), %rdx
	cmpq	%rdx, %rax
	jbe	.L2725
	movzbl	1(%r14), %eax
	sall	$8, %edi
	orl	%eax, %edi
.L2746:
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	%rax, %r15
	movq	24(%r12), %rax
.L2725:
	movq	16(%r12), %rdx
	cmpq	%rax, %rdx
	jnb	.L2730
	movzbl	(%rdx), %edi
	movq	%rdx, -80(%rbp)
	movl	%edi, -68(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	-68(%rbp), %edi
	movq	-80(%rbp), %rdx
	testb	%al, %al
	je	.L2747
	leaq	1(%rdx), %rax
	cmpq	%rax, 24(%r12)
	jbe	.L2730
	movzbl	1(%rdx), %eax
	sall	$8, %edi
	orl	%eax, %edi
.L2747:
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	%rax, %rcx
	jmp	.L2728
.L2723:
	leaq	.LC490(%rip), %rbx
	jmp	.L2722
	.p2align 4,,10
	.p2align 3
.L2698:
	movl	$4, %eax
	jmp	.L2699
	.p2align 4,,10
	.p2align 3
.L2732:
	movl	$8, %eax
	jmp	.L2699
	.p2align 4,,10
	.p2align 3
.L2749:
	movq	16(%r12), %r14
	leaq	.LC482(%rip), %r15
	cmpq	24(%r12), %r14
	jb	.L2751
.L2709:
	movq	%r15, %rcx
	leaq	.LC530(%rip), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L2708
	.p2align 4,,10
	.p2align 3
.L2694:
	movq	%r8, %rsi
	leaq	.LC547(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	leal	-64773(%rbx), %esi
	cmpl	$18, %esi
	ja	.L2695
	leaq	.L2702(%rip), %rdx
	movslq	(%rdx,%rsi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15SimdExtractLaneENS1_10WasmOpcodeENS1_9ValueTypeE,"aG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15SimdExtractLaneENS1_10WasmOpcodeENS1_9ValueTypeE,comdat
	.align 4
	.align 4
.L2702:
	.long	.L2701-.L2702
	.long	.L2695-.L2702
	.long	.L2701-.L2702
	.long	.L2695-.L2702
	.long	.L2701-.L2702
	.long	.L2695-.L2702
	.long	.L2701-.L2702
	.long	.L2695-.L2702
	.long	.L2701-.L2702
	.long	.L2701-.L2702
	.long	.L2695-.L2702
	.long	.L2701-.L2702
	.long	.L2701-.L2702
	.long	.L2695-.L2702
	.long	.L2701-.L2702
	.long	.L2701-.L2702
	.long	.L2695-.L2702
	.long	.L2701-.L2702
	.long	.L2701-.L2702
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15SimdExtractLaneENS1_10WasmOpcodeENS1_9ValueTypeE,"axG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15SimdExtractLaneENS1_10WasmOpcodeENS1_9ValueTypeE,comdat
	.p2align 4,,10
	.p2align 3
.L2730:
	leaq	.LC482(%rip), %rcx
.L2728:
	pushq	%rbx
	leaq	.LC531(%rip), %rdx
	xorl	%eax, %eax
	xorl	%r8d, %r8d
	pushq	%r15
	leaq	.LC497(%rip), %r9
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	popq	%rax
	popq	%rdx
	jmp	.L2708
.L2716:
	leaq	.LC495(%rip), %rbx
	jmp	.L2722
.L2717:
	leaq	.LC494(%rip), %rbx
	jmp	.L2722
.L2718:
	leaq	.LC493(%rip), %rbx
	jmp	.L2722
.L2719:
	leaq	.LC497(%rip), %rbx
	jmp	.L2722
.L2720:
	leaq	.LC492(%rip), %rbx
	jmp	.L2722
.L2721:
	leaq	.LC491(%rip), %rbx
	jmp	.L2722
.L2724:
	leaq	.LC498(%rip), %rbx
	jmp	.L2722
.L2714:
	leaq	.LC496(%rip), %rbx
	jmp	.L2722
.L2695:
	leaq	.LC544(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2751:
	movzbl	(%r14), %ebx
	movl	%ebx, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	testb	%al, %al
	je	.L2752
	leaq	1(%r14), %rax
	cmpq	%rax, 24(%r12)
	jbe	.L2745
	movzbl	1(%r14), %edi
	sall	$8, %ebx
	orl	%ebx, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	%rax, %r15
.L2745:
	movq	16(%r12), %r14
	jmp	.L2709
	.p2align 4,,10
	.p2align 3
.L2752:
	movl	%ebx, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	16(%r12), %r14
	movq	%rax, %r15
	jmp	.L2709
.L2713:
	leaq	.LC489(%rip), %rbx
	jmp	.L2722
.L2750:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23982:
	.size	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15SimdExtractLaneENS1_10WasmOpcodeENS1_9ValueTypeE, .-_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15SimdExtractLaneENS1_10WasmOpcodeENS1_9ValueTypeE
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15SimdReplaceLaneENS1_10WasmOpcodeENS1_9ValueTypeE,"axG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15SimdReplaceLaneENS1_10WasmOpcodeENS1_9ValueTypeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15SimdReplaceLaneENS1_10WasmOpcodeENS1_9ValueTypeE
	.type	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15SimdReplaceLaneENS1_10WasmOpcodeENS1_9ValueTypeE, @function
_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15SimdReplaceLaneENS1_10WasmOpcodeENS1_9ValueTypeE:
.LFB23985:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$40, %rsp
	movq	16(%rdi), %r8
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	leaq	2(%r8), %rsi
	cmpq	%rax, %rsi
	ja	.L2754
	cmpl	%esi, %eax
	je	.L2754
	leal	-64773(%rbx), %esi
	movzbl	2(%r8), %ecx
	cmpl	$18, %esi
	ja	.L2755
	leaq	.L2757(%rip), %rdx
	movq	%r8, %r9
	movslq	(%rdx,%rsi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15SimdReplaceLaneENS1_10WasmOpcodeENS1_9ValueTypeE,"aG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15SimdReplaceLaneENS1_10WasmOpcodeENS1_9ValueTypeE,comdat
	.align 4
	.align 4
.L2757:
	.long	.L2832-.L2757
	.long	.L2755-.L2757
	.long	.L2832-.L2757
	.long	.L2755-.L2757
	.long	.L2804-.L2757
	.long	.L2755-.L2757
	.long	.L2804-.L2757
	.long	.L2755-.L2757
	.long	.L2758-.L2757
	.long	.L2758-.L2757
	.long	.L2755-.L2757
	.long	.L2756-.L2757
	.long	.L2756-.L2757
	.long	.L2755-.L2757
	.long	.L2758-.L2757
	.long	.L2758-.L2757
	.long	.L2755-.L2757
	.long	.L2756-.L2757
	.long	.L2756-.L2757
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15SimdReplaceLaneENS1_10WasmOpcodeENS1_9ValueTypeE,"axG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15SimdReplaceLaneENS1_10WasmOpcodeENS1_9ValueTypeE,comdat
	.p2align 4,,10
	.p2align 3
.L2756:
	movl	$2, %eax
.L2759:
	cmpb	%cl, %al
	jbe	.L2763
.L2761:
	movq	192(%r12), %rdx
	movq	184(%r12), %rsi
	movq	224(%r12), %rcx
	movq	%rdx, %rax
	movq	%rsi, %rdi
	subq	%rsi, %rax
	movl	-84(%rcx), %r10d
	sarq	$4, %rax
	cmpq	%rax, %r10
	jb	.L2837
	cmpb	$2, -72(%rcx)
	jne	.L2838
.L2771:
	movq	224(%r12), %rcx
	movq	%rsi, %rdi
.L2772:
	movq	%rdx, %rax
	movl	-84(%rcx), %esi
	subq	%rdi, %rax
	sarq	$4, %rax
	cmpq	%rax, %rsi
	jb	.L2779
	cmpb	$2, -72(%rcx)
	jne	.L2839
.L2781:
	leaq	-57(%rbp), %rdx
	leaq	16(%r12), %rsi
	movb	$5, -57(%rbp)
	leaq	176(%r12), %rdi
	call	_ZNSt6vectorIN2v88internal4wasm9ValueBaseENS1_13ZoneAllocatorIS3_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_
	jmp	.L2766
	.p2align 4,,10
	.p2align 3
.L2805:
	xorl	%ecx, %ecx
.L2832:
	movl	$16, %eax
	cmpb	%cl, %al
	ja	.L2761
.L2763:
	leaq	2(%r8), %rsi
	leaq	.LC548(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
.L2766:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2840
	leaq	-40(%rbp), %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2837:
	.cfi_restore_state
	movzbl	-8(%rdx), %eax
	movq	-16(%rdx), %r14
	subq	$16, %rdx
	movq	%rdx, 192(%r12)
	cmpb	%al, %r13b
	je	.L2772
	cmpb	$6, %r13b
	sete	%r9b
	cmpb	$8, %al
	sete	%r11b
	testb	%r9b, %r9b
	je	.L2813
	testb	%r11b, %r11b
	jne	.L2772
.L2813:
	leal	-7(%rax), %ebx
	andl	$253, %ebx
	jne	.L2814
	testb	%r9b, %r9b
	jne	.L2772
.L2814:
	leal	-7(%r13), %edx
	movzbl	%al, %edi
	andb	$-3, %dl
	sete	%dl
	testb	%r11b, %dl
	jne	.L2834
	cmpb	$10, %r13b
	setne	%dl
	cmpb	$10, %al
	setne	%al
	testb	%al, %dl
	jne	.L2775
.L2834:
	movq	192(%r12), %rdx
	jmp	.L2771
	.p2align 4,,10
	.p2align 3
.L2779:
	movzbl	-8(%rdx), %eax
	movq	-16(%rdx), %r13
	subq	$16, %rdx
	movq	%rdx, 192(%r12)
	cmpb	$5, %al
	je	.L2781
	cmpb	$10, %al
	je	.L2781
	cmpb	$9, %al
	ja	.L2786
	leaq	.L2788(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15SimdReplaceLaneENS1_10WasmOpcodeENS1_9ValueTypeE,"aG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15SimdReplaceLaneENS1_10WasmOpcodeENS1_9ValueTypeE,comdat
	.align 4
	.align 4
.L2788:
	.long	.L2796-.L2788
	.long	.L2795-.L2788
	.long	.L2810-.L2788
	.long	.L2793-.L2788
	.long	.L2792-.L2788
	.long	.L2786-.L2788
	.long	.L2791-.L2788
	.long	.L2790-.L2788
	.long	.L2789-.L2788
	.long	.L2787-.L2788
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15SimdReplaceLaneENS1_10WasmOpcodeENS1_9ValueTypeE,"axG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15SimdReplaceLaneENS1_10WasmOpcodeENS1_9ValueTypeE,comdat
.L2810:
	leaq	.LC500(%rip), %rbx
.L2794:
	movq	24(%r12), %rax
	leaq	.LC482(%rip), %r14
	cmpq	%rax, %r13
	jnb	.L2797
	movzbl	0(%r13), %r15d
	movl	%r15d, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	testb	%al, %al
	je	.L2841
	movq	24(%r12), %rax
	leaq	1(%r13), %rdx
	cmpq	%rdx, %rax
	jbe	.L2797
	movl	%r15d, %edi
	movzbl	1(%r13), %r15d
	sall	$8, %edi
	orl	%r15d, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	%rax, %r14
	movq	24(%r12), %rax
	.p2align 4,,10
	.p2align 3
.L2797:
	movq	16(%r12), %r15
	cmpq	%rax, %r15
	jnb	.L2802
	movzbl	(%r15), %edi
	movl	%edi, -72(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	-72(%rbp), %edi
	testb	%al, %al
	je	.L2836
	leaq	1(%r15), %rax
	cmpq	%rax, 24(%r12)
	jbe	.L2802
	movzbl	1(%r15), %eax
	sall	$8, %edi
	orl	%eax, %edi
.L2836:
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	%rax, %rcx
	jmp	.L2800
.L2795:
	leaq	.LC490(%rip), %rbx
	jmp	.L2794
	.p2align 4,,10
	.p2align 3
.L2758:
	movl	$4, %eax
	jmp	.L2759
	.p2align 4,,10
	.p2align 3
.L2804:
	movl	$8, %eax
	jmp	.L2759
	.p2align 4,,10
	.p2align 3
.L2839:
	movq	16(%r12), %r13
	leaq	.LC482(%rip), %r14
	cmpq	24(%r12), %r13
	jb	.L2842
.L2782:
	movq	%r14, %rcx
	leaq	.LC530(%rip), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L2781
	.p2align 4,,10
	.p2align 3
.L2838:
	leaq	.LC482(%rip), %r14
	cmpq	%r8, 24(%r12)
	ja	.L2843
.L2768:
	leaq	.LC530(%rip), %rdx
	movq	%r9, %rsi
	movq	%r14, %rcx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	184(%r12), %rsi
	movq	192(%r12), %rdx
	jmp	.L2771
	.p2align 4,,10
	.p2align 3
.L2754:
	leaq	.LC547(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	16(%r12), %r8
	leal	-64773(%rbx), %esi
	movq	%r8, %r9
	cmpl	$18, %esi
	ja	.L2755
	leaq	.L2762(%rip), %rdx
	movslq	(%rdx,%rsi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15SimdReplaceLaneENS1_10WasmOpcodeENS1_9ValueTypeE,"aG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15SimdReplaceLaneENS1_10WasmOpcodeENS1_9ValueTypeE,comdat
	.align 4
	.align 4
.L2762:
	.long	.L2805-.L2762
	.long	.L2755-.L2762
	.long	.L2805-.L2762
	.long	.L2755-.L2762
	.long	.L2761-.L2762
	.long	.L2755-.L2762
	.long	.L2761-.L2762
	.long	.L2755-.L2762
	.long	.L2761-.L2762
	.long	.L2761-.L2762
	.long	.L2755-.L2762
	.long	.L2761-.L2762
	.long	.L2761-.L2762
	.long	.L2755-.L2762
	.long	.L2761-.L2762
	.long	.L2761-.L2762
	.long	.L2755-.L2762
	.long	.L2761-.L2762
	.long	.L2761-.L2762
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15SimdReplaceLaneENS1_10WasmOpcodeENS1_9ValueTypeE,"axG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15SimdReplaceLaneENS1_10WasmOpcodeENS1_9ValueTypeE,comdat
	.p2align 4,,10
	.p2align 3
.L2802:
	leaq	.LC482(%rip), %rcx
.L2800:
	pushq	%rbx
	leaq	.LC531(%rip), %rdx
	xorl	%eax, %eax
	xorl	%r8d, %r8d
	pushq	%r14
	leaq	.LC497(%rip), %r9
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	popq	%rax
	popq	%rdx
	jmp	.L2781
.L2789:
	leaq	.LC495(%rip), %rbx
	jmp	.L2794
.L2790:
	leaq	.LC494(%rip), %rbx
	jmp	.L2794
.L2791:
	leaq	.LC493(%rip), %rbx
	jmp	.L2794
.L2787:
	leaq	.LC496(%rip), %rbx
	jmp	.L2794
.L2792:
	leaq	.LC492(%rip), %rbx
	jmp	.L2794
.L2793:
	leaq	.LC491(%rip), %rbx
	jmp	.L2794
.L2796:
	leaq	.LC498(%rip), %rbx
	jmp	.L2794
.L2786:
	leaq	.LC489(%rip), %rbx
	jmp	.L2794
.L2755:
	leaq	.LC544(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2842:
	movzbl	0(%r13), %r15d
	movl	%r15d, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	testb	%al, %al
	je	.L2844
	leaq	1(%r13), %rax
	cmpq	%rax, 24(%r12)
	jbe	.L2835
	movzbl	1(%r13), %edi
	sall	$8, %r15d
	orl	%r15d, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	%rax, %r14
.L2835:
	movq	16(%r12), %r13
	jmp	.L2782
	.p2align 4,,10
	.p2align 3
.L2843:
	movzbl	(%r8), %r15d
	movq	%r8, -72(%rbp)
	movl	%r15d, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movq	-72(%rbp), %r8
	testb	%al, %al
	je	.L2845
	leaq	1(%r8), %rax
	cmpq	%rax, 24(%r12)
	jbe	.L2833
	movzbl	1(%r8), %edi
	sall	$8, %r15d
	orl	%r15d, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	%rax, %r14
.L2833:
	movq	16(%r12), %r9
	jmp	.L2768
	.p2align 4,,10
	.p2align 3
.L2841:
	movl	%r15d, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	%rax, %r14
	movq	24(%r12), %rax
	jmp	.L2797
	.p2align 4,,10
	.p2align 3
.L2844:
	movl	%r15d, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	16(%r12), %r13
	movq	%rax, %r14
	jmp	.L2782
	.p2align 4,,10
	.p2align 3
.L2845:
	movl	%r15d, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	16(%r12), %r9
	movq	%rax, %r14
	jmp	.L2768
.L2840:
	call	__stack_chk_fail@PLT
	.p2align 4,,10
	.p2align 3
.L2775:
	movq	%r8, -80(%rbp)
	leaq	24(%r12), %r15
	leaq	.LC482(%rip), %rbx
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	cmpq	24(%r12), %r14
	movq	-80(%rbp), %r8
	movq	%rax, -72(%rbp)
	jnb	.L2777
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %r8
	movq	%rax, %rbx
.L2777:
	movzbl	%r13b, %edi
	movq	%r8, -80(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	-80(%rbp), %r8
	leaq	.LC482(%rip), %rcx
	movq	%rax, %r13
	cmpq	24(%r12), %r8
	jnb	.L2778
	movq	%r8, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	%rax, %rcx
.L2778:
	pushq	-72(%rbp)
	movl	$1, %r8d
	movq	%r14, %rsi
	movq	%r12, %rdi
	pushq	%rbx
	leaq	.LC531(%rip), %rdx
	movq	%r13, %r9
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	popq	%rsi
	popq	%r8
	movq	184(%r12), %rdi
	movq	192(%r12), %rdx
	movq	224(%r12), %rcx
	jmp	.L2772
	.cfi_endproc
.LFE23985:
	.size	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15SimdReplaceLaneENS1_10WasmOpcodeENS1_9ValueTypeE, .-_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15SimdReplaceLaneENS1_10WasmOpcodeENS1_9ValueTypeE
	.section	.text._ZN2v88internal4wasm7Decoder13read_leb_tailIiLNS2_12ValidateFlagE0ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_,"axG",@progbits,_ZN2v88internal4wasm7Decoder13read_leb_tailIiLNS2_12ValidateFlagE0ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm7Decoder13read_leb_tailIiLNS2_12ValidateFlagE0ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	.type	_ZN2v88internal4wasm7Decoder13read_leb_tailIiLNS2_12ValidateFlagE0ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_, @function
_ZN2v88internal4wasm7Decoder13read_leb_tailIiLNS2_12ValidateFlagE0ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_:
.LFB24007:
	.cfi_startproc
	endbr64
	movzbl	(%rsi), %ecx
	movl	%ecx, %eax
	andl	$127, %eax
	orl	%r8d, %eax
	testb	%cl, %cl
	js	.L2852
	sall	$25, %eax
	movl	$1, (%rdx)
	sarl	$25, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2852:
	movzbl	1(%rsi), %edi
	movl	%edi, %ecx
	sall	$7, %ecx
	andl	$16256, %ecx
	orl	%ecx, %eax
	testb	%dil, %dil
	js	.L2853
	sall	$18, %eax
	movl	$2, (%rdx)
	sarl	$18, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2853:
	movzbl	2(%rsi), %edi
	movl	%edi, %ecx
	sall	$14, %ecx
	andl	$2080768, %ecx
	orl	%ecx, %eax
	testb	%dil, %dil
	js	.L2854
	sall	$11, %eax
	movl	$3, (%rdx)
	sarl	$11, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2854:
	movzbl	3(%rsi), %edi
	movl	%edi, %ecx
	sall	$21, %ecx
	andl	$266338304, %ecx
	orl	%ecx, %eax
	testb	%dil, %dil
	js	.L2855
	sall	$4, %eax
	movl	$4, (%rdx)
	sarl	$4, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2855:
	movzbl	4(%rsi), %esi
	movl	$5, (%rdx)
	sall	$28, %esi
	orl	%esi, %eax
	ret
	.cfi_endproc
.LFE24007:
	.size	_ZN2v88internal4wasm7Decoder13read_leb_tailIiLNS2_12ValidateFlagE0ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_, .-_ZN2v88internal4wasm7Decoder13read_leb_tailIiLNS2_12ValidateFlagE0ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	.section	.rodata._ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE0EE12OpcodeLengthEPS3_PKh.str1.1,"aMS",@progbits,1
.LC549:
	.string	"data segment index"
	.section	.text._ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE0EE12OpcodeLengthEPS3_PKh,"axG",@progbits,_ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE0EE12OpcodeLengthEPS3_PKh,comdat
	.p2align 4
	.weak	_ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE0EE12OpcodeLengthEPS3_PKh
	.type	_ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE0EE12OpcodeLengthEPS3_PKh, @function
_ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE0EE12OpcodeLengthEPS3_PKh:
.LFB21554:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r9
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movzbl	(%rsi), %eax
	cmpb	$-46, %al
	ja	.L2857
	cmpb	$1, %al
	jbe	.L2940
	cmpb	$-46, %al
	ja	.L2940
	leaq	.L2861(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE0EE12OpcodeLengthEPS3_PKh,"aG",@progbits,_ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE0EE12OpcodeLengthEPS3_PKh,comdat
	.align 4
	.align 4
.L2861:
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2877-.L2861
	.long	.L2877-.L2861
	.long	.L2877-.L2861
	.long	.L2940-.L2861
	.long	.L2877-.L2861
	.long	.L2940-.L2861
	.long	.L2876-.L2861
	.long	.L2940-.L2861
	.long	.L2875-.L2861
	.long	.L2940-.L2861
	.long	.L3001-.L2861
	.long	.L3001-.L2861
	.long	.L2873-.L2861
	.long	.L2940-.L2861
	.long	.L2876-.L2861
	.long	.L2871-.L2861
	.long	.L2876-.L2861
	.long	.L2871-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2870-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2869-.L2861
	.long	.L2869-.L2861
	.long	.L2869-.L2861
	.long	.L2868-.L2861
	.long	.L2868-.L2861
	.long	.L2860-.L2861
	.long	.L2860-.L2861
	.long	.L2940-.L2861
	.long	.L2866-.L2861
	.long	.L2866-.L2861
	.long	.L2866-.L2861
	.long	.L2866-.L2861
	.long	.L2866-.L2861
	.long	.L2866-.L2861
	.long	.L2866-.L2861
	.long	.L2866-.L2861
	.long	.L2866-.L2861
	.long	.L2866-.L2861
	.long	.L2866-.L2861
	.long	.L2866-.L2861
	.long	.L2866-.L2861
	.long	.L2866-.L2861
	.long	.L2866-.L2861
	.long	.L2866-.L2861
	.long	.L2866-.L2861
	.long	.L2866-.L2861
	.long	.L2866-.L2861
	.long	.L2866-.L2861
	.long	.L2866-.L2861
	.long	.L2866-.L2861
	.long	.L2866-.L2861
	.long	.L2995-.L2861
	.long	.L2995-.L2861
	.long	.L2864-.L2861
	.long	.L2863-.L2861
	.long	.L2862-.L2861
	.long	.L2941-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2940-.L2861
	.long	.L2860-.L2861
	.section	.text._ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE0EE12OpcodeLengthEPS3_PKh,"axG",@progbits,_ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE0EE12OpcodeLengthEPS3_PKh,comdat
.L2862:
	movl	$5, %eax
.L2856:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L3015
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2941:
	.cfi_restore_state
	movl	$9, %eax
	jmp	.L2856
.L2940:
	movl	$1, %eax
	jmp	.L2856
	.p2align 4,,10
	.p2align 3
.L2857:
	cmpb	$-3, %al
	je	.L2878
	cmpb	$-2, %al
	jne	.L3016
	movzbl	1(%rsi), %edx
	movl	$3, %eax
	orb	$-2, %dh
	cmpl	$65027, %edx
	je	.L2856
	jg	.L2936
	jne	.L2938
.L2937:
	leaq	.LC505(%rip), %rdx
	movq	%r8, %rsi
	movq	%r9, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
.L2995:
	movl	$2, %eax
	jmp	.L2856
	.p2align 4,,10
	.p2align 3
.L3016:
	cmpb	$-4, %al
	jne	.L2940
	movzbl	1(%rsi), %eax
	movl	%eax, %ecx
	orb	$-4, %ah
	subl	$64512, %eax
	cmpl	$17, %eax
	ja	.L2909
	leaq	.L2911(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE0EE12OpcodeLengthEPS3_PKh,"aG",@progbits,_ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE0EE12OpcodeLengthEPS3_PKh,comdat
	.align 4
	.align 4
.L2911:
	.long	.L2995-.L2911
	.long	.L2995-.L2911
	.long	.L2995-.L2911
	.long	.L2995-.L2911
	.long	.L2995-.L2911
	.long	.L2995-.L2911
	.long	.L2995-.L2911
	.long	.L2995-.L2911
	.long	.L2917-.L2911
	.long	.L2916-.L2911
	.long	.L2915-.L2911
	.long	.L2982-.L2911
	.long	.L2912-.L2911
	.long	.L2913-.L2911
	.long	.L2912-.L2911
	.long	.L2910-.L2911
	.long	.L2910-.L2911
	.long	.L2910-.L2911
	.section	.text._ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE0EE12OpcodeLengthEPS3_PKh,"axG",@progbits,_ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE0EE12OpcodeLengthEPS3_PKh,comdat
.L2860:
	movabsq	$4294967296, %rax
	movq	%rax, -48(%rbp)
.L3001:
	movzbl	1(%r8), %edx
	movl	$2, %eax
	testb	%dl, %dl
	jns	.L2856
	andl	$127, %edx
	leaq	-44(%rbp), %rsi
	leaq	2(%r8), %rdi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE0ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi1EEET_PKhPjPKcS7_.isra.0
	movl	-44(%rbp), %eax
	jmp	.L2994
.L2868:
	movb	$0, -44(%rbp)
.L2876:
	movq	$0, -40(%rbp)
	movzbl	1(%r8), %edx
	movl	$2, %eax
	testb	%dl, %dl
	jns	.L2856
	andl	$127, %edx
	leaq	-32(%rbp), %rsi
	leaq	2(%r8), %rdi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE0ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi1EEET_PKhPjPKcS7_.isra.0
	movl	-32(%rbp), %eax
	addl	$1, %eax
	jmp	.L2856
.L2866:
	movq	%rsi, %rdx
	leaq	-48(%rbp), %rdi
	movl	$-1, %ecx
	movq	%r9, %rsi
	call	_ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE0EEC1EPS3_PKhj
	movl	-40(%rbp), %eax
	addl	$1, %eax
	jmp	.L2856
.L2870:
	movzbl	1(%rsi), %edx
	movl	$2, %eax
	movl	$2, %ecx
	testb	%dl, %dl
	js	.L3017
.L2896:
	movzbl	(%r8,%rcx), %ecx
	movl	%eax, -48(%rbp)
	cmpb	$64, %cl
	je	.L2897
	subl	$104, %ecx
	cmpb	$23, %cl
	ja	.L2898
	movl	$1, %edx
	salq	%cl, %rdx
	testl	$16253313, %edx
	jne	.L2994
.L2898:
	movb	$10, -44(%rbp)
.L2900:
	leaq	1(%r8), %rsi
	leaq	.LC502(%rip), %rdx
	movq	%r9, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movl	-48(%rbp), %eax
.L2994:
	addl	$1, %eax
	jmp	.L2856
.L2871:
	cmpb	$0, 1(%rsi)
	movzbl	2(%rsi), %edx
	js	.L2885
	leaq	1(%rsi), %rcx
	movl	$6, %r8d
	movl	$4, %esi
	movl	$5, %edi
	movl	$3, %eax
	movl	$7, %r9d
.L2886:
	testb	%dl, %dl
	jns	.L2856
	cmpb	$0, 2(%rcx)
	movl	%esi, %eax
	jns	.L2856
	cmpb	$0, 3(%rcx)
	movl	%edi, %eax
	jns	.L2856
	cmpb	$0, 4(%rcx)
	movl	%r8d, %eax
	cmovs	%r9d, %eax
	jmp	.L2856
.L2877:
	movq	%rsi, %rcx
	leaq	-48(%rbp), %rdi
	movq	%r9, %rdx
	leaq	_ZN2v88internal4wasmL16kAllWasmFeaturesE(%rip), %rsi
	call	_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE0EEC1ERKNS1_12WasmFeaturesEPS3_PKh
	movl	-48(%rbp), %eax
	addl	$1, %eax
	jmp	.L2856
.L2863:
	cmpb	$0, 1(%rsi)
	movl	$2, %eax
	jns	.L2856
	cmpb	$0, 2(%rsi)
	movl	$3, %eax
	jns	.L2856
	cmpb	$0, 3(%rsi)
	movl	$4, %eax
	jns	.L2856
	cmpb	$0, 4(%rsi)
	movl	$5, %eax
	jns	.L2856
	cmpb	$0, 5(%rsi)
	movl	$6, %eax
	jns	.L2856
	cmpb	$0, 6(%rsi)
	movl	$7, %eax
	jns	.L2856
	cmpb	$0, 7(%rsi)
	movl	$8, %eax
	jns	.L2856
	cmpb	$0, 8(%rsi)
	movl	$9, %eax
	jns	.L2856
	movsbl	9(%rsi), %eax
	sarl	$31, %eax
	notl	%eax
	addl	$11, %eax
	jmp	.L2856
.L2864:
	cmpb	$0, 1(%rsi)
	movl	$2, %eax
	jns	.L2856
	cmpb	$0, 2(%rsi)
	movl	$3, %eax
	jns	.L2856
	cmpb	$0, 3(%rsi)
	movl	$4, %eax
	jns	.L2856
	movsbl	4(%rsi), %eax
	shrl	$31, %eax
	addl	$5, %eax
	jmp	.L2856
.L2869:
	movzbl	1(%rsi), %edx
	movb	$0, -44(%rbp)
	movl	$2, %eax
	testb	%dl, %dl
	jns	.L2856
	andl	$127, %edx
	leaq	-40(%rbp), %rsi
	leaq	2(%r8), %rdi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE0ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi1EEET_PKhPjPKcS7_.isra.0
	movl	-40(%rbp), %eax
	addl	$1, %eax
	jmp	.L2856
.L2875:
	movzbl	1(%rsi), %edx
	movl	%edx, %r9d
	andl	$127, %r9d
	testb	%dl, %dl
	js	.L3018
	movl	$1, -44(%rbp)
	movl	$1, %edx
	movl	$1, %eax
.L2893:
	addq	%rdx, %r8
	movl	%r9d, -48(%rbp)
	movl	$1, %ecx
	movzbl	1(%r8), %edx
	movq	$0, -32(%rbp)
	testb	%dl, %dl
	js	.L3019
.L2894:
	leal	1(%rcx,%rax), %eax
	jmp	.L2856
.L2873:
	movzbl	1(%rsi), %edx
	movl	$0, -48(%rbp)
	leaq	1(%rsi), %r11
	movl	$2, %eax
	movl	%edx, %r10d
	andl	$127, %r10d
	testb	%dl, %dl
	js	.L3020
.L2901:
	addq	%rax, %r8
	xorl	%r9d, %r9d
	leaq	-48(%rbp), %rsi
	movzbl	(%r8), %edx
	leaq	1(%r8), %rdi
	testb	%dl, %dl
	js	.L3021
.L2902:
	addl	$1, %r9d
	cmpl	%r10d, %r9d
	ja	.L2904
.L2903:
	movq	%rdi, %r8
	movzbl	(%r8), %edx
	leaq	1(%r8), %rdi
	testb	%dl, %dl
	jns	.L2902
.L3021:
	andl	$127, %edx
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE0ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi1EEET_PKhPjPKcS7_.isra.0
	movl	-48(%rbp), %edi
	addl	$1, %r9d
	addq	%r8, %rdi
	cmpl	%r9d, %r10d
	jnb	.L2903
.L2904:
	subq	%r11, %rdi
	leal	1(%rdi), %eax
	jmp	.L2856
.L2910:
	movabsq	$4294967296, %rax
	movq	%rax, -48(%rbp)
	movl	$3, %eax
	testb	%cl, %cl
	jns	.L2856
	movl	%ecx, %edx
	leaq	-44(%rbp), %rsi
	leaq	2(%r8), %rdi
	andl	$127, %edx
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE0ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi1EEET_PKhPjPKcS7_.isra.0
	movl	-44(%rbp), %eax
	addl	$2, %eax
	jmp	.L2856
.L2912:
	cmpb	$0, 2(%rsi)
	movzbl	3(%rsi), %edx
	js	.L3022
	movl	$5, %r10d
	movl	$7, %esi
	movl	$6, %r9d
	movl	$4, %eax
	movl	$8, %edi
	movl	$2, %ecx
.L2925:
	testb	%dl, %dl
	jns	.L2856
	addq	%r8, %rcx
	movl	%r10d, %eax
	cmpb	$0, 2(%rcx)
	jns	.L2856
	cmpb	$0, 3(%rcx)
	movl	%r9d, %eax
	jns	.L2856
	cmpb	$0, 4(%rcx)
	movl	%esi, %eax
	cmovs	%edi, %eax
	jmp	.L2856
	.p2align 4,,10
	.p2align 3
.L2982:
	movl	$3, %eax
	jmp	.L2856
.L2917:
	cmpb	$0, 2(%rsi)
	movl	$4, %eax
	jns	.L2856
	cmpb	$0, 3(%rsi)
	movl	$5, %eax
	jns	.L2856
	cmpb	$0, 4(%rsi)
	movl	$6, %eax
	jns	.L2856
	movsbl	5(%rsi), %eax
	sarl	$31, %eax
	notl	%eax
	addl	$8, %eax
	jmp	.L2856
	.p2align 4,,10
	.p2align 3
.L2913:
	leaq	-44(%rbp), %rdx
	leaq	2(%rsi), %rsi
	xorl	%r8d, %r8d
	leaq	.LC542(%rip), %rcx
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIiLNS2_12ValidateFlagE0ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	movl	-44(%rbp), %eax
	addl	$2, %eax
	jmp	.L2856
.L2916:
	leaq	-44(%rbp), %rdx
	leaq	2(%rsi), %rsi
	xorl	%r8d, %r8d
	leaq	.LC549(%rip), %rcx
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIiLNS2_12ValidateFlagE0ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	movl	-44(%rbp), %eax
	addl	$2, %eax
	jmp	.L2856
.L2915:
	movl	$4, %eax
	jmp	.L2856
	.p2align 4,,10
	.p2align 3
.L2878:
	movzbl	1(%rsi), %eax
	orb	$-3, %ah
	subl	$64768, %eax
	cmpl	$209, %eax
	ja	.L2931
	leaq	.L2933(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE0EE12OpcodeLengthEPS3_PKh,"aG",@progbits,_ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE0EE12OpcodeLengthEPS3_PKh,comdat
	.align 4
	.align 4
.L2933:
	.long	.L2938-.L2933
	.long	.L2938-.L2933
	.long	.L2931-.L2933
	.long	.L2934-.L2933
	.long	.L2995-.L2933
	.long	.L2982-.L2933
	.long	.L2931-.L2933
	.long	.L2982-.L2933
	.long	.L2995-.L2933
	.long	.L2982-.L2933
	.long	.L2931-.L2933
	.long	.L2982-.L2933
	.long	.L2995-.L2933
	.long	.L2982-.L2933
	.long	.L2982-.L2933
	.long	.L2995-.L2933
	.long	.L2982-.L2933
	.long	.L2982-.L2933
	.long	.L2995-.L2933
	.long	.L2982-.L2933
	.long	.L2982-.L2933
	.long	.L2995-.L2933
	.long	.L2982-.L2933
	.long	.L2982-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2931-.L2933
	.long	.L2931-.L2933
	.long	.L2995-.L2933
	.long	.L2931-.L2933
	.long	.L2931-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2931-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2931-.L2933
	.long	.L2931-.L2933
	.long	.L2931-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2931-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2931-.L2933
	.long	.L2931-.L2933
	.long	.L2931-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2931-.L2933
	.long	.L2931-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2931-.L2933
	.long	.L2931-.L2933
	.long	.L2931-.L2933
	.long	.L2931-.L2933
	.long	.L2931-.L2933
	.long	.L2931-.L2933
	.long	.L2931-.L2933
	.long	.L2931-.L2933
	.long	.L2931-.L2933
	.long	.L2931-.L2933
	.long	.L2931-.L2933
	.long	.L2931-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2931-.L2933
	.long	.L2931-.L2933
	.long	.L2931-.L2933
	.long	.L2931-.L2933
	.long	.L2931-.L2933
	.long	.L2931-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.long	.L2995-.L2933
	.section	.text._ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE0EE12OpcodeLengthEPS3_PKh,"axG",@progbits,_ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE0EE12OpcodeLengthEPS3_PKh,comdat
.L2931:
	leaq	.LC504(%rip), %rdx
	movq	%r8, %rsi
	movq	%r9, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movl	$2, %eax
	jmp	.L2856
	.p2align 4,,10
	.p2align 3
.L2936:
	subl	$65040, %edx
	cmpl	$62, %edx
	ja	.L2937
.L2938:
	leaq	1(%r8), %rdx
	leaq	-48(%rbp), %rdi
	movl	$-1, %ecx
	movq	%r9, %rsi
	call	_ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE0EEC1EPS3_PKhj
	movl	-40(%rbp), %eax
	addl	$2, %eax
	jmp	.L2856
.L2934:
	movl	$18, %eax
	jmp	.L2856
	.p2align 4,,10
	.p2align 3
.L3020:
	movl	%r10d, %edx
	leaq	-48(%rbp), %rsi
	leaq	2(%r8), %rdi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE0ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi1EEET_PKhPjPKcS7_.isra.0
	movl	%eax, %r10d
	movl	-48(%rbp), %eax
	addq	$1, %rax
	jmp	.L2901
	.p2align 4,,10
	.p2align 3
.L2885:
	movzbl	3(%rsi), %eax
	testb	%dl, %dl
	js	.L2887
	leaq	2(%rsi), %rcx
	movl	%eax, %edx
	movl	$5, %esi
	movl	$7, %r8d
	movl	$6, %edi
	movl	$4, %eax
	movl	$8, %r9d
	jmp	.L2886
	.p2align 4,,10
	.p2align 3
.L3017:
	andl	$127, %edx
	leaq	-48(%rbp), %rsi
	leaq	2(%r8), %rdi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE0ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi1EEET_PKhPjPKcS7_.isra.0
	movl	-48(%rbp), %ecx
	movq	%rcx, %rax
	addq	$1, %rcx
	addl	$1, %eax
	jmp	.L2896
	.p2align 4,,10
	.p2align 3
.L3019:
	andl	$127, %edx
	leaq	-24(%rbp), %rsi
	leaq	2(%r8), %rdi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE0ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi1EEET_PKhPjPKcS7_.isra.0
	movl	-44(%rbp), %eax
	movl	-24(%rbp), %ecx
	jmp	.L2894
	.p2align 4,,10
	.p2align 3
.L3018:
	movl	%r9d, %edx
	leaq	-44(%rbp), %rsi
	leaq	2(%r8), %rdi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE0ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi1EEET_PKhPjPKcS7_.isra.0
	movl	-44(%rbp), %edx
	movl	%eax, %r9d
	movq	%rdx, %rax
	jmp	.L2893
.L2887:
	testb	%al, %al
	js	.L2888
	movzbl	4(%rsi), %edx
	leaq	3(%rsi), %rcx
	movl	$8, %r8d
	movl	$6, %esi
	movl	$7, %edi
	movl	$5, %eax
	movl	$9, %r9d
	jmp	.L2886
.L2897:
	movb	$0, -44(%rbp)
	jmp	.L2900
.L2909:
	leaq	.LC503(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	jmp	.L2995
.L3022:
	testb	%dl, %dl
	js	.L2926
	movzbl	4(%rsi), %edx
	movl	$6, %r10d
	movl	$8, %esi
	movl	$7, %r9d
	movl	$5, %eax
	movl	$9, %edi
	movl	$3, %ecx
	jmp	.L2925
.L2888:
	cmpb	$0, 4(%rsi)
	js	.L2889
	movzbl	5(%rsi), %edx
	leaq	4(%rsi), %rcx
	movl	$9, %r8d
	movl	$7, %esi
	movl	$8, %edi
	movl	$6, %eax
	movl	$10, %r9d
	jmp	.L2886
.L2926:
	cmpb	$0, 4(%rsi)
	js	.L2927
	movzbl	5(%rsi), %edx
	movl	$7, %r10d
	movl	$9, %esi
	movl	$8, %r9d
	movl	$6, %eax
	movl	$10, %edi
	movl	$4, %ecx
	jmp	.L2925
.L2889:
	movzbl	6(%rsi), %edx
	leaq	5(%rsi), %rcx
	movl	$10, %r8d
	movl	$8, %esi
	movl	$9, %edi
	movl	$7, %eax
	movl	$11, %r9d
	jmp	.L2886
.L3015:
	call	__stack_chk_fail@PLT
.L2927:
	cmpb	$0, 5(%rsi)
	js	.L3023
	movzbl	6(%rsi), %edx
	movl	$8, %r10d
	movl	$10, %esi
	movl	$9, %r9d
	movl	$7, %eax
	movl	$11, %edi
	movl	$5, %ecx
	jmp	.L2925
.L3023:
	movzbl	7(%rsi), %edx
	movl	$9, %r10d
	movl	$11, %esi
	movl	$10, %r9d
	movl	$8, %eax
	movl	$12, %edi
	movl	$6, %ecx
	jmp	.L2925
	.cfi_endproc
.LFE21554:
	.size	_ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE0EE12OpcodeLengthEPS3_PKh, .-_ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE0EE12OpcodeLengthEPS3_PKh
	.section	.rodata._ZN2v88internal4wasm16PrintRawWasmCodeEPNS0_19AccountingAllocatorERKNS1_12FunctionBodyEPKNS1_10WasmModuleENS1_11PrintLocalsERSoPSt6vectorIiSaIiEE.str1.8,"aMS",@progbits,1
	.align 8
.LC550:
	.string	"../deps/v8/src/wasm/function-body-decoder.cc:100"
	.section	.rodata._ZN2v88internal4wasm16PrintRawWasmCodeEPNS0_19AccountingAllocatorERKNS1_12FunctionBodyEPKNS1_10WasmModuleENS1_11PrintLocalsERSoPSt6vectorIiSaIiEE.str1.1,"aMS",@progbits,1
.LC551:
	.string	"// signature: "
.LC552:
	.string	"// locals:"
.LC553:
	.string	" "
.LC554:
	.string	"// body: "
	.section	.rodata._ZN2v88internal4wasm16PrintRawWasmCodeEPNS0_19AccountingAllocatorERKNS1_12FunctionBodyEPKNS1_10WasmModuleENS1_11PrintLocalsERSoPSt6vectorIiSaIiEE.str1.8
	.align 8
.LC555:
	.string	"                                                                "
	.section	.rodata._ZN2v88internal4wasm16PrintRawWasmCodeEPNS0_19AccountingAllocatorERKNS1_12FunctionBodyEPKNS1_10WasmModuleENS1_11PrintLocalsERSoPSt6vectorIiSaIiEE.str1.1
.LC556:
	.string	","
.LC557:
	.string	" kWasmI32,"
.LC558:
	.string	" kWasmI64,"
.LC559:
	.string	" kWasmF32,"
.LC560:
	.string	" kWasmF64,"
.LC561:
	.string	" kWasmS128,"
.LC562:
	.string	" kWasmStmt,"
.LC563:
	.string	" 0x"
.LC564:
	.string	"   // @"
.LC565:
	.string	"   // depth="
.LC566:
	.string	" // entries="
.LC567:
	.string	"   // sig #"
.LC568:
	.string	": "
.LC569:
	.string	" // function #"
.LC570:
	.string	"0x"
	.section	.text._ZN2v88internal4wasm16PrintRawWasmCodeEPNS0_19AccountingAllocatorERKNS1_12FunctionBodyEPKNS1_10WasmModuleENS1_11PrintLocalsERSoPSt6vectorIiSaIiEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm16PrintRawWasmCodeEPNS0_19AccountingAllocatorERKNS1_12FunctionBodyEPKNS1_10WasmModuleENS1_11PrintLocalsERSoPSt6vectorIiSaIiEE
	.type	_ZN2v88internal4wasm16PrintRawWasmCodeEPNS0_19AccountingAllocatorERKNS1_12FunctionBodyEPKNS1_10WasmModuleENS1_11PrintLocalsERSoPSt6vectorIiSaIiEE, @function
_ZN2v88internal4wasm16PrintRawWasmCodeEPNS0_19AccountingAllocatorERKNS1_12FunctionBodyEPKNS1_10WasmModuleENS1_11PrintLocalsERSoPSt6vectorIiSaIiEE:
.LFB19617:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	leaq	.LC550(%rip), %rdx
	pushq	%r12
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movq	%rdi, %rsi
	subq	$568, %rsp
	movq	%r9, -560(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-416(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -584(%rbp)
	call	_ZN2v88internal4ZoneC1EPNS0_19AccountingAllocatorEPKc@PLT
	leaq	-128(%rbp), %rsi
	movq	16(%rbx), %rax
	movq	(%rbx), %rcx
	movq	24(%rbx), %rdx
	movq	%rsi, -592(%rbp)
	leaq	16+_ZTVN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE0EEE(%rip), %rdi
	movq	%rsi, -144(%rbp)
	movq	_ZN2v88internal4wasmL16kAllWasmFeaturesE(%rip), %rsi
	movq	$0, -525(%rbp)
	movq	%rsi, -104(%rbp)
	movl	8+_ZN2v88internal4wasmL16kAllWasmFeaturesE(%rip), %esi
	movl	$0, -517(%rbp)
	movl	%esi, -96(%rbp)
	movzbl	12+_ZN2v88internal4wasmL16kAllWasmFeaturesE(%rip), %esi
	movb	$0, -513(%rbp)
	movb	%sil, -92(%rbp)
	leaq	-525(%rbp), %rsi
	movq	%rax, -184(%rbp)
	movq	%rax, -176(%rbp)
	movq	%rdx, -168(%rbp)
	movl	$0, -160(%rbp)
	movl	$0, -152(%rbp)
	movq	$0, -136(%rbp)
	movb	$0, -128(%rbp)
	movq	%rdi, -192(%rbp)
	movq	%r13, -112(%rbp)
	movq	%rsi, -88(%rbp)
	movq	%rcx, -80(%rbp)
	movq	$0, -72(%rbp)
	movl	$-1, -532(%rbp)
	testq	%rcx, %rcx
	je	.L3025
	movl	$14, %edx
	leaq	.LC551(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal4wasmlsERSoRKNS0_9SignatureINS1_9ValueTypeEEE@PLT
	movq	%rax, %r13
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%r13,%rax), %r14
	testq	%r14, %r14
	je	.L3068
	cmpb	$0, 56(%r14)
	je	.L3027
	movsbl	67(%r14), %esi
.L3028:
	movq	%r13, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	cmpq	$0, -560(%rbp)
	je	.L3342
	movq	-560(%rbp), %rcx
	movq	8(%rcx), %rsi
	cmpq	16(%rcx), %rsi
	je	.L3030
	movl	-532(%rbp), %eax
	movl	%eax, (%rsi)
	addq	$4, 8(%rcx)
	movq	24(%rbx), %rdx
	movq	16(%rbx), %rax
.L3025:
	movq	-584(%rbp), %rcx
	movq	%rax, -344(%rbp)
	leaq	-272(%rbp), %rsi
	leaq	_ZN2v88internal4wasmL16kAllWasmFeaturesE(%rip), %rdi
	movq	%rax, -336(%rbp)
	movq	%rcx, -504(%rbp)
	leaq	-288(%rbp), %rcx
	movq	%rcx, -600(%rbp)
	movq	%rcx, -304(%rbp)
	leaq	16+_ZTVN2v88internal4wasm16BytecodeIteratorE(%rip), %rcx
	movq	%rcx, -352(%rbp)
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %rcx
	movq	%rdx, -328(%rbp)
	movq	%rcx, -272(%rbp)
	leaq	-504(%rbp), %rcx
	movq	%rax, -264(%rbp)
	movq	%rax, -256(%rbp)
	leaq	-208(%rbp), %rax
	movq	%rdx, -248(%rbp)
	xorl	%edx, %edx
	movl	$0, -512(%rbp)
	movq	$0, -496(%rbp)
	movq	$0, -488(%rbp)
	movq	$0, -480(%rbp)
	movl	$0, -320(%rbp)
	movl	$0, -312(%rbp)
	movq	$0, -296(%rbp)
	movb	$0, -288(%rbp)
	movl	$0, -240(%rbp)
	movl	$0, -232(%rbp)
	movq	%rsi, -576(%rbp)
	movq	%rax, -568(%rbp)
	movq	%rax, -224(%rbp)
	movq	$0, -216(%rbp)
	movb	$0, -208(%rbp)
	call	_ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12DecodeLocalsERKNS1_12WasmFeaturesEPS3_PKNS0_9SignatureINS1_9ValueTypeEEEPNS0_10ZoneVectorISB_EE
	testb	%al, %al
	jne	.L3377
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %rax
	movq	-224(%rbp), %rdi
	movq	%rax, -272(%rbp)
	cmpq	-568(%rbp), %rdi
	je	.L3343
	call	_ZdlPv@PLT
.L3343:
	movq	-336(%rbp), %rax
.L3033:
	cmpq	%rax, 16(%rbx)
	je	.L3036
	testl	%r12d, %r12d
	je	.L3378
.L3036:
	movl	$9, %edx
	leaq	.LC554(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r15), %rax
	movq	-24(%rax), %rax
	movq	240(%r15,%rax), %r12
	testq	%r12, %r12
	je	.L3068
	cmpb	$0, 56(%r12)
	je	.L3080
	movsbl	67(%r12), %esi
.L3081:
	movq	%r15, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movq	-560(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L3082
	movq	8(%rbx), %rsi
	cmpq	16(%rbx), %rsi
	je	.L3083
	movl	-532(%rbp), %eax
	movl	%eax, (%rsi)
	addq	$4, 8(%rbx)
.L3082:
	movl	$0, -548(%rbp)
	movq	-336(%rbp), %r8
	cmpq	-328(%rbp), %r8
	jnb	.L3085
.L3084:
	movzbl	(%r8), %eax
	cmpb	$-46, %al
	ja	.L3086
	cmpb	$1, %al
	jbe	.L3248
	cmpb	$-46, %al
	ja	.L3248
	leaq	.L3090(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm16PrintRawWasmCodeEPNS0_19AccountingAllocatorERKNS1_12FunctionBodyEPKNS1_10WasmModuleENS1_11PrintLocalsERSoPSt6vectorIiSaIiEE,"a",@progbits
	.align 4
	.align 4
.L3090:
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3106-.L3090
	.long	.L3106-.L3090
	.long	.L3106-.L3090
	.long	.L3248-.L3090
	.long	.L3106-.L3090
	.long	.L3248-.L3090
	.long	.L3105-.L3090
	.long	.L3248-.L3090
	.long	.L3104-.L3090
	.long	.L3248-.L3090
	.long	.L3355-.L3090
	.long	.L3355-.L3090
	.long	.L3102-.L3090
	.long	.L3248-.L3090
	.long	.L3105-.L3090
	.long	.L3100-.L3090
	.long	.L3105-.L3090
	.long	.L3100-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3099-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3098-.L3090
	.long	.L3098-.L3090
	.long	.L3098-.L3090
	.long	.L3097-.L3090
	.long	.L3097-.L3090
	.long	.L3089-.L3090
	.long	.L3089-.L3090
	.long	.L3248-.L3090
	.long	.L3095-.L3090
	.long	.L3095-.L3090
	.long	.L3095-.L3090
	.long	.L3095-.L3090
	.long	.L3095-.L3090
	.long	.L3095-.L3090
	.long	.L3095-.L3090
	.long	.L3095-.L3090
	.long	.L3095-.L3090
	.long	.L3095-.L3090
	.long	.L3095-.L3090
	.long	.L3095-.L3090
	.long	.L3095-.L3090
	.long	.L3095-.L3090
	.long	.L3095-.L3090
	.long	.L3095-.L3090
	.long	.L3095-.L3090
	.long	.L3095-.L3090
	.long	.L3095-.L3090
	.long	.L3095-.L3090
	.long	.L3095-.L3090
	.long	.L3095-.L3090
	.long	.L3095-.L3090
	.long	.L3287-.L3090
	.long	.L3287-.L3090
	.long	.L3093-.L3090
	.long	.L3092-.L3090
	.long	.L3249-.L3090
	.long	.L3091-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3248-.L3090
	.long	.L3089-.L3090
	.section	.text._ZN2v88internal4wasm16PrintRawWasmCodeEPNS0_19AccountingAllocatorERKNS1_12FunctionBodyEPKNS1_10WasmModuleENS1_11PrintLocalsERSoPSt6vectorIiSaIiEE
.L3249:
	movl	$5, %ebx
.L3087:
	movzbl	(%r8), %r12d
	movl	%r12d, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	$1, -552(%rbp)
	testb	%al, %al
	je	.L3172
	movq	-336(%rbp), %rax
	movl	$2, -552(%rbp)
	movzwl	(%rax), %r12d
	rolw	$8, %r12w
	movzwl	%r12w, %r12d
.L3172:
	movq	-560(%rbp), %rcx
	testq	%rcx, %rcx
	je	.L3173
	movq	-336(%rbp), %rax
	subq	-344(%rbp), %rax
	movl	%eax, -464(%rbp)
	movq	8(%rcx), %rsi
	cmpq	16(%rcx), %rsi
	je	.L3174
	movl	%eax, (%rsi)
	addq	$4, 8(%rcx)
.L3173:
	movl	%r12d, %eax
	leaq	.LC555(%rip), %rsi
	movq	%r15, %rdi
	andl	$-3, %eax
	cmpl	$5, %eax
	sete	%al
	movzbl	%al, %eax
	subl	%eax, -548(%rbp)
	movl	-548(%rbp), %eax
	cmpl	$32, %eax
	leal	(%rax,%rax), %edx
	movl	$64, %eax
	cmovnb	%rax, %rdx
	call	_ZNSo5writeEPKcl@PLT
	movl	%r12d, %edi
	call	_ZN2v88internal4wasm12_GLOBAL__N_113RawOpcodeNameENS1_10WasmOpcodeE
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L3379
	movq	%rax, %rdi
	call	strlen@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L3179:
	movl	$1, %edx
	leaq	.LC556(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leal	-2(%r12), %eax
	cmpl	$2, %eax
	jbe	.L3294
	cmpl	$6, %r12d
	jne	.L3180
.L3294:
	movq	-336(%rbp), %rax
	movzbl	1(%rax), %eax
	cmpb	$64, %al
	je	.L3182
	subl	$123, %eax
	cmpb	$4, %al
	ja	.L3183
	leaq	.L3185(%rip), %rdx
	movzbl	%al, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm16PrintRawWasmCodeEPNS0_19AccountingAllocatorERKNS1_12FunctionBodyEPKNS1_10WasmModuleENS1_11PrintLocalsERSoPSt6vectorIiSaIiEE
	.align 4
	.align 4
.L3185:
	.long	.L3189-.L3185
	.long	.L3188-.L3185
	.long	.L3187-.L3185
	.long	.L3186-.L3185
	.long	.L3184-.L3185
	.section	.text._ZN2v88internal4wasm16PrintRawWasmCodeEPNS0_19AccountingAllocatorERKNS1_12FunctionBodyEPKNS1_10WasmModuleENS1_11PrintLocalsERSoPSt6vectorIiSaIiEE
.L3248:
	movl	$1, %ebx
	jmp	.L3087
.L3287:
	movl	$2, %ebx
	jmp	.L3087
.L3089:
	movabsq	$4294967296, %rax
	movq	%rax, -464(%rbp)
.L3355:
	movzbl	1(%r8), %edx
	movl	$2, %ebx
	testb	%dl, %dl
	jns	.L3087
	leaq	2(%r8), %rdi
	andl	$127, %edx
	leaq	-460(%rbp), %rsi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE0ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi1EEET_PKhPjPKcS7_.isra.0
	movl	-460(%rbp), %eax
	movq	-336(%rbp), %r8
	leal	1(%rax), %ebx
	jmp	.L3087
.L3097:
	movb	$0, -460(%rbp)
.L3105:
	movq	$0, -456(%rbp)
	movzbl	1(%r8), %edx
	movl	$2, %ebx
	testb	%dl, %dl
	jns	.L3087
	leaq	2(%r8), %rdi
	andl	$127, %edx
	leaq	-448(%rbp), %rsi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE0ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi1EEET_PKhPjPKcS7_.isra.0
	movl	-448(%rbp), %eax
	movq	-336(%rbp), %r8
	leal	1(%rax), %ebx
	jmp	.L3087
.L3186:
	movl	$10, %edx
	leaq	.LC558(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	.p2align 4,,10
	.p2align 3
.L3190:
	cmpl	$17, %r12d
	ja	.L3194
.L3380:
	leaq	.L3196(%rip), %rdx
	movslq	(%rdx,%r12,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm16PrintRawWasmCodeEPNS0_19AccountingAllocatorERKNS1_12FunctionBodyEPKNS1_10WasmModuleENS1_11PrintLocalsERSoPSt6vectorIiSaIiEE
	.align 4
	.align 4
.L3196:
	.long	.L3194-.L3196
	.long	.L3194-.L3196
	.long	.L3203-.L3196
	.long	.L3203-.L3196
	.long	.L3203-.L3196
	.long	.L3202-.L3196
	.long	.L3203-.L3196
	.long	.L3202-.L3196
	.long	.L3194-.L3196
	.long	.L3194-.L3196
	.long	.L3194-.L3196
	.long	.L3201-.L3196
	.long	.L3199-.L3196
	.long	.L3199-.L3196
	.long	.L3198-.L3196
	.long	.L3194-.L3196
	.long	.L3197-.L3196
	.long	.L3195-.L3196
	.section	.text._ZN2v88internal4wasm16PrintRawWasmCodeEPNS0_19AccountingAllocatorERKNS1_12FunctionBodyEPKNS1_10WasmModuleENS1_11PrintLocalsERSoPSt6vectorIiSaIiEE
.L3187:
	movl	$10, %edx
	leaq	.LC559(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	$17, %r12d
	jbe	.L3380
	.p2align 4,,10
	.p2align 3
.L3194:
	movq	(%r15), %rax
	movq	-24(%rax), %rax
	movq	240(%r15,%rax), %r12
	testq	%r12, %r12
	je	.L3068
	cmpb	$0, 56(%r12)
	je	.L3222
	movsbl	67(%r12), %esi
.L3223:
	movq	%r15, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movq	-336(%rbp), %rsi
	movq	-328(%rbp), %rax
	cmpq	%rax, %rsi
	jb	.L3381
.L3085:
	cmpq	$0, -136(%rbp)
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %rax
	movq	-304(%rbp), %rdi
	movq	%rax, -352(%rbp)
	sete	%r12b
	cmpq	-600(%rbp), %rdi
	je	.L3239
	call	_ZdlPv@PLT
.L3239:
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %rax
	movq	-144(%rbp), %rdi
	movq	%rax, -192(%rbp)
	cmpq	-592(%rbp), %rdi
	je	.L3227
	call	_ZdlPv@PLT
.L3227:
	movq	-584(%rbp), %rdi
	call	_ZN2v88internal4ZoneD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3382
	addq	$568, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3188:
	.cfi_restore_state
	movl	$10, %edx
	leaq	.LC560(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L3190
.L3189:
	movl	$11, %edx
	leaq	.LC561(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L3190
.L3184:
	movl	$10, %edx
	leaq	.LC557(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L3190
	.p2align 4,,10
	.p2align 3
.L3203:
	movq	-336(%rbp), %rcx
	leaq	-464(%rbp), %r14
	leaq	-352(%rbp), %rdx
	leaq	_ZN2v88internal4wasmL16kAllWasmFeaturesE(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE0EEC1ERKNS1_12WasmFeaturesEPS3_PKh
	movl	$7, %edx
	leaq	.LC564(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-336(%rbp), %rsi
	movq	%r15, %rdi
	subq	-344(%rbp), %rsi
	addl	-320(%rbp), %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movzbl	-460(%rbp), %eax
	cmpb	$10, %al
	jne	.L3204
	movq	-112(%rbp), %rdx
	movl	-456(%rbp), %ecx
	movq	88(%rdx), %rdx
	movq	(%rdx,%rcx,8), %rdx
	movq	%rdx, -448(%rbp)
.L3205:
	xorl	%ebx, %ebx
	leaq	.LC490(%rip), %r14
	leaq	.LC491(%rip), %r12
	leaq	.LC492(%rip), %r13
	.p2align 4,,10
	.p2align 3
.L3216:
	movl	$1, %edx
	cmpb	$10, %al
	jne	.L3206
	movq	-448(%rbp), %rax
	movl	(%rax), %edx
.L3206:
	cmpl	%ebx, %edx
	jbe	.L3207
	movl	$1, %edx
	leaq	.LC553(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movzbl	-460(%rbp), %eax
	cmpb	$10, %al
	jne	.L3208
	movq	-448(%rbp), %rdx
	movl	%ebx, %eax
	movq	16(%rdx), %rdx
	movzbl	(%rdx,%rax), %eax
.L3208:
	cmpb	$10, %al
	ja	.L3215
	leaq	.L3231(%rip), %rcx
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm16PrintRawWasmCodeEPNS0_19AccountingAllocatorERKNS1_12FunctionBodyEPKNS1_10WasmModuleENS1_11PrintLocalsERSoPSt6vectorIiSaIiEE
	.align 4
	.align 4
.L3231:
	.long	.L3213-.L3231
	.long	.L3235-.L3231
	.long	.L3230-.L3231
	.long	.L3234-.L3231
	.long	.L3233-.L3231
	.long	.L3212-.L3231
	.long	.L3232-.L3231
	.long	.L3293-.L3231
	.long	.L3209-.L3231
	.long	.L3211-.L3231
	.long	.L3214-.L3231
	.section	.text._ZN2v88internal4wasm16PrintRawWasmCodeEPNS0_19AccountingAllocatorERKNS1_12FunctionBodyEPKNS1_10WasmModuleENS1_11PrintLocalsERSoPSt6vectorIiSaIiEE
	.p2align 4,,10
	.p2align 3
.L3199:
	movq	-336(%rbp), %rdx
	movzbl	1(%rdx), %eax
	movl	%eax, %ebx
	andl	$127, %ebx
	testb	%al, %al
	js	.L3383
.L3218:
	movl	$12, %edx
	leaq	.LC565(%rip), %rsi
.L3351:
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	jmp	.L3194
	.p2align 4,,10
	.p2align 3
.L3202:
	movl	$7, %edx
	leaq	.LC564(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-336(%rbp), %rsi
	movq	%r15, %rdi
	subq	-344(%rbp), %rsi
	addl	-320(%rbp), %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	addl	$1, -548(%rbp)
	jmp	.L3194
	.p2align 4,,10
	.p2align 3
.L3201:
	movl	$7, %edx
	leaq	.LC564(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-336(%rbp), %rsi
	movq	%r15, %rdi
	subq	-344(%rbp), %rsi
	addl	-320(%rbp), %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	subl	$1, -548(%rbp)
	jmp	.L3194
	.p2align 4,,10
	.p2align 3
.L3195:
	movq	-336(%rbp), %rdx
	movzbl	1(%rdx), %eax
	movl	%eax, %ebx
	andl	$127, %ebx
	testb	%al, %al
	js	.L3384
.L3220:
	movl	$11, %edx
	leaq	.LC567(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	-112(%rbp), %rax
	movq	88(%rax), %rax
	movq	(%rax,%rbx,8), %r12
.L3350:
	movq	%r15, %rdi
	movl	$2, %edx
	leaq	.LC568(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal4wasmlsERSoRKNS0_9SignatureINS1_9ValueTypeEEE@PLT
	jmp	.L3194
	.p2align 4,,10
	.p2align 3
.L3197:
	movq	-336(%rbp), %rdx
	movzbl	1(%rdx), %eax
	movl	%eax, %ebx
	andl	$127, %ebx
	testb	%al, %al
	js	.L3385
.L3221:
	movl	$14, %edx
	leaq	.LC569(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	salq	$5, %rbx
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	-112(%rbp), %rax
	addq	136(%rax), %rbx
	movq	(%rbx), %r12
	jmp	.L3350
	.p2align 4,,10
	.p2align 3
.L3198:
	movq	-336(%rbp), %rdx
	movzbl	1(%rdx), %eax
	movl	%eax, %ebx
	andl	$127, %ebx
	testb	%al, %al
	js	.L3386
.L3219:
	movl	$12, %edx
	leaq	.LC566(%rip), %rsi
	jmp	.L3351
	.p2align 4,,10
	.p2align 3
.L3086:
	cmpb	$-3, %al
	je	.L3107
	cmpb	$-2, %al
	jne	.L3387
	movzbl	1(%r8), %eax
	movl	$3, %ebx
	orb	$-2, %ah
	cmpl	$65027, %eax
	je	.L3087
	jle	.L3388
	subl	$65040, %eax
	cmpl	$62, %eax
	ja	.L3170
.L3171:
	leaq	-464(%rbp), %r14
	leaq	1(%r8), %rdx
	movl	$-1, %ecx
	leaq	-192(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE0EEC1EPS3_PKhj
	movl	-456(%rbp), %eax
	movq	-336(%rbp), %r8
	leal	2(%rax), %ebx
	jmp	.L3087
	.p2align 4,,10
	.p2align 3
.L3387:
	cmpb	$-4, %al
	jne	.L3248
	movzbl	1(%r8), %eax
	movl	%eax, %ecx
	orb	$-4, %ah
	subl	$64512, %eax
	cmpl	$17, %eax
	ja	.L3140
	leaq	.L3142(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm16PrintRawWasmCodeEPNS0_19AccountingAllocatorERKNS1_12FunctionBodyEPKNS1_10WasmModuleENS1_11PrintLocalsERSoPSt6vectorIiSaIiEE
	.align 4
	.align 4
.L3142:
	.long	.L3287-.L3142
	.long	.L3287-.L3142
	.long	.L3287-.L3142
	.long	.L3287-.L3142
	.long	.L3287-.L3142
	.long	.L3287-.L3142
	.long	.L3287-.L3142
	.long	.L3287-.L3142
	.long	.L3149-.L3142
	.long	.L3148-.L3142
	.long	.L3147-.L3142
	.long	.L3166-.L3142
	.long	.L3143-.L3142
	.long	.L3144-.L3142
	.long	.L3143-.L3142
	.long	.L3141-.L3142
	.long	.L3141-.L3142
	.long	.L3141-.L3142
	.section	.text._ZN2v88internal4wasm16PrintRawWasmCodeEPNS0_19AccountingAllocatorERKNS1_12FunctionBodyEPKNS1_10WasmModuleENS1_11PrintLocalsERSoPSt6vectorIiSaIiEE
	.p2align 4,,10
	.p2align 3
.L3222:
	movq	%r12, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r12), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L3223
	movq	%r12, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L3223
	.p2align 4,,10
	.p2align 3
.L3381:
	movq	-568(%rbp), %r13
	movq	-576(%rbp), %rdi
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %r14
	movq	%rsi, -264(%rbp)
	movq	%r14, -272(%rbp)
	movq	%rsi, -256(%rbp)
	movq	%rax, -248(%rbp)
	movl	$0, -240(%rbp)
	movl	$0, -232(%rbp)
	movq	%r13, -224(%rbp)
	movq	$0, -216(%rbp)
	movb	$0, -208(%rbp)
	call	_ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE0EE12OpcodeLengthEPS3_PKh
	movq	-224(%rbp), %rdi
	movq	%r14, -272(%rbp)
	movl	%eax, %ebx
	cmpq	%r13, %rdi
	je	.L3224
	call	_ZdlPv@PLT
	movq	-328(%rbp), %rax
	movl	%ebx, %r8d
	addq	-336(%rbp), %r8
	movq	%r8, -336(%rbp)
	cmpq	%r8, %rax
	ja	.L3084
.L3225:
	movq	%rax, -336(%rbp)
	jmp	.L3085
	.p2align 4,,10
	.p2align 3
.L3379:
	movq	(%r15), %rax
	movq	-24(%rax), %rdi
	addq	%r15, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L3179
	.p2align 4,,10
	.p2align 3
.L3180:
	movl	-552(%rbp), %r13d
	leaq	-464(%rbp), %r14
	cmpl	%ebx, %r13d
	jnb	.L3190
	.p2align 4,,10
	.p2align 3
.L3193:
	movl	$3, %edx
	leaq	.LC563(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$2, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	-336(%rbp), %rax
	movzbl	(%rax,%r13), %eax
	movw	%dx, -456(%rbp)
	addq	$1, %r13
	movq	%rax, -464(%rbp)
	call	_ZN2v88internallsERSoRKNS0_5AsHexE@PLT
	movl	$1, %edx
	leaq	.LC556(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r13d, %ebx
	ja	.L3193
	jmp	.L3190
	.p2align 4,,10
	.p2align 3
.L3224:
	movl	%eax, %r8d
	movq	-328(%rbp), %rax
	addq	-336(%rbp), %r8
	movq	%r8, -336(%rbp)
	cmpq	%rax, %r8
	jb	.L3084
	jmp	.L3225
	.p2align 4,,10
	.p2align 3
.L3183:
	movl	$3, %edx
	movq	%r15, %rdi
	leaq	.LC563(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-336(%rbp), %rax
	movl	$2, %ecx
	movq	%r15, %rdi
	leaq	-464(%rbp), %r14
	movzbl	1(%rax), %eax
	movq	%r14, %rsi
	movw	%cx, -456(%rbp)
	movq	%rax, -464(%rbp)
	call	_ZN2v88internallsERSoRKNS0_5AsHexE@PLT
	movl	$1, %edx
	leaq	.LC556(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L3190
	.p2align 4,,10
	.p2align 3
.L3182:
	movl	$11, %edx
	leaq	.LC562(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L3190
	.p2align 4,,10
	.p2align 3
.L3174:
	leaq	-464(%rbp), %r14
	movq	%rcx, %rdi
	movq	%r14, %rdx
	call	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	jmp	.L3173
.L3092:
	cmpb	$0, 1(%r8)
	movl	$2, %ebx
	jns	.L3087
	cmpb	$0, 2(%r8)
	movl	$3, %ebx
	jns	.L3087
	cmpb	$0, 3(%r8)
	movl	$4, %ebx
	jns	.L3087
	cmpb	$0, 4(%r8)
	movl	$5, %ebx
	jns	.L3087
	cmpb	$0, 5(%r8)
	movl	$6, %ebx
	jns	.L3087
	cmpb	$0, 6(%r8)
	movl	$7, %ebx
	jns	.L3087
	cmpb	$0, 7(%r8)
	movl	$8, %ebx
	jns	.L3087
	cmpb	$0, 8(%r8)
	movl	$9, %ebx
	jns	.L3087
	movsbl	9(%r8), %ebx
	sarl	$31, %ebx
	notl	%ebx
	addl	$11, %ebx
	jmp	.L3087
.L3093:
	cmpb	$0, 1(%r8)
	movl	$2, %ebx
	jns	.L3087
	cmpb	$0, 2(%r8)
	movl	$3, %ebx
	jns	.L3087
	cmpb	$0, 3(%r8)
	movl	$4, %ebx
	jns	.L3087
	movsbl	4(%r8), %ebx
	shrl	$31, %ebx
	addl	$5, %ebx
	jmp	.L3087
.L3095:
	leaq	-464(%rbp), %r14
	movq	%r8, %rdx
	movl	$-1, %ecx
	leaq	-192(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE0EEC1EPS3_PKhj
	movl	-456(%rbp), %eax
	movq	-336(%rbp), %r8
	leal	1(%rax), %ebx
	jmp	.L3087
.L3091:
	movl	$9, %ebx
	jmp	.L3087
.L3098:
	movb	$0, -460(%rbp)
	movzbl	1(%r8), %edx
	movl	$2, %ebx
	testb	%dl, %dl
	jns	.L3087
	leaq	2(%r8), %rdi
	andl	$127, %edx
	leaq	-456(%rbp), %rsi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE0ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi1EEET_PKhPjPKcS7_.isra.0
	movl	-456(%rbp), %eax
	movq	-336(%rbp), %r8
	leal	1(%rax), %ebx
	jmp	.L3087
.L3099:
	movzbl	1(%r8), %edx
	testb	%dl, %dl
	js	.L3389
	movl	$1, -464(%rbp)
	movl	$2, %ebx
	movl	$2, %eax
.L3126:
	movzbl	(%r8,%rax), %eax
	movl	%ebx, -464(%rbp)
	subl	$64, %eax
	cmpb	$63, %al
	ja	.L3127
	leaq	.L3129(%rip), %rdx
	movzbl	%al, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm16PrintRawWasmCodeEPNS0_19AccountingAllocatorERKNS1_12FunctionBodyEPKNS1_10WasmModuleENS1_11PrintLocalsERSoPSt6vectorIiSaIiEE
	.align 4
	.align 4
.L3129:
	.long	.L3130-.L3129
	.long	.L3127-.L3129
	.long	.L3127-.L3129
	.long	.L3127-.L3129
	.long	.L3127-.L3129
	.long	.L3127-.L3129
	.long	.L3127-.L3129
	.long	.L3127-.L3129
	.long	.L3127-.L3129
	.long	.L3127-.L3129
	.long	.L3127-.L3129
	.long	.L3127-.L3129
	.long	.L3127-.L3129
	.long	.L3127-.L3129
	.long	.L3127-.L3129
	.long	.L3127-.L3129
	.long	.L3127-.L3129
	.long	.L3127-.L3129
	.long	.L3127-.L3129
	.long	.L3127-.L3129
	.long	.L3127-.L3129
	.long	.L3127-.L3129
	.long	.L3127-.L3129
	.long	.L3127-.L3129
	.long	.L3127-.L3129
	.long	.L3127-.L3129
	.long	.L3127-.L3129
	.long	.L3127-.L3129
	.long	.L3127-.L3129
	.long	.L3127-.L3129
	.long	.L3127-.L3129
	.long	.L3127-.L3129
	.long	.L3127-.L3129
	.long	.L3127-.L3129
	.long	.L3127-.L3129
	.long	.L3127-.L3129
	.long	.L3127-.L3129
	.long	.L3127-.L3129
	.long	.L3127-.L3129
	.long	.L3127-.L3129
	.long	.L3128-.L3129
	.long	.L3127-.L3129
	.long	.L3127-.L3129
	.long	.L3127-.L3129
	.long	.L3127-.L3129
	.long	.L3127-.L3129
	.long	.L3127-.L3129
	.long	.L3128-.L3129
	.long	.L3128-.L3129
	.long	.L3127-.L3129
	.long	.L3127-.L3129
	.long	.L3127-.L3129
	.long	.L3127-.L3129
	.long	.L3127-.L3129
	.long	.L3127-.L3129
	.long	.L3127-.L3129
	.long	.L3127-.L3129
	.long	.L3127-.L3129
	.long	.L3127-.L3129
	.long	.L3128-.L3129
	.long	.L3128-.L3129
	.long	.L3128-.L3129
	.long	.L3128-.L3129
	.long	.L3128-.L3129
	.section	.text._ZN2v88internal4wasm16PrintRawWasmCodeEPNS0_19AccountingAllocatorERKNS1_12FunctionBodyEPKNS1_10WasmModuleENS1_11PrintLocalsERSoPSt6vectorIiSaIiEE
.L3100:
	cmpb	$0, 1(%r8)
	movzbl	2(%r8), %eax
	js	.L3114
	leaq	1(%r8), %rdx
	movl	$4, %ecx
	movl	$6, %esi
	movl	$5, %r9d
	movl	$3, %ebx
	movl	$7, %edi
.L3115:
	testb	%al, %al
	jns	.L3087
	cmpb	$0, 2(%rdx)
	movl	%ecx, %ebx
	jns	.L3087
	cmpb	$0, 3(%rdx)
	movl	%r9d, %ebx
	jns	.L3087
	cmpb	$0, 4(%rdx)
	movl	%esi, %ebx
	cmovs	%edi, %ebx
	jmp	.L3087
.L3102:
	movl	$0, -464(%rbp)
	movzbl	1(%r8), %eax
	leaq	1(%r8), %r10
	movl	$2, %edx
	movl	%eax, %r9d
	andl	$127, %r9d
	testb	%al, %al
	js	.L3390
.L3132:
	addq	%rdx, %r8
	xorl	%r11d, %r11d
	leaq	-464(%rbp), %rsi
.L3136:
	movzbl	(%r8), %edx
	leaq	1(%r8), %rdi
	testb	%dl, %dl
	js	.L3391
	addl	$1, %r11d
	cmpl	%r11d, %r9d
	jb	.L3135
.L3134:
	movq	%rdi, %r8
	jmp	.L3136
.L3104:
	movzbl	1(%r8), %eax
	movl	%eax, %edx
	andl	$127, %edx
	testb	%al, %al
	js	.L3392
	movl	$1, -460(%rbp)
	movl	$1, %eax
	movl	$1, %ecx
.L3122:
	addq	%rax, %r8
	movl	$1, %eax
	movl	%edx, -464(%rbp)
	movq	$0, -448(%rbp)
	movzbl	1(%r8), %edx
	testb	%dl, %dl
	js	.L3393
.L3123:
	movq	-336(%rbp), %r8
	leal	1(%rax,%rcx), %ebx
	jmp	.L3087
.L3106:
	leaq	-464(%rbp), %r14
	movq	%r8, %rcx
	leaq	-192(%rbp), %rdx
	leaq	_ZN2v88internal4wasmL16kAllWasmFeaturesE(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE0EEC1ERKNS1_12WasmFeaturesEPS3_PKh
	movl	-464(%rbp), %eax
	movq	-336(%rbp), %r8
	leal	1(%rax), %ebx
	jmp	.L3087
	.p2align 4,,10
	.p2align 3
.L3293:
	movl	$7, %edx
	leaq	.LC494(%rip), %rsi
	.p2align 4,,10
	.p2align 3
.L3210:
	movq	%r15, %rdi
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movzbl	-460(%rbp), %eax
	testb	%al, %al
	jne	.L3216
.L3207:
	addl	$1, -548(%rbp)
	jmp	.L3194
	.p2align 4,,10
	.p2align 3
.L3232:
	movl	$6, %edx
	leaq	.LC493(%rip), %rsi
	jmp	.L3210
	.p2align 4,,10
	.p2align 3
.L3233:
	movl	$3, %edx
	movq	%r13, %rsi
	jmp	.L3210
	.p2align 4,,10
	.p2align 3
.L3234:
	movl	$3, %edx
	movq	%r12, %rsi
	jmp	.L3210
	.p2align 4,,10
	.p2align 3
.L3230:
	movl	$3, %edx
	leaq	.LC500(%rip), %rsi
	jmp	.L3210
	.p2align 4,,10
	.p2align 3
.L3212:
	movl	$4, %edx
	leaq	.LC497(%rip), %rsi
	jmp	.L3210
	.p2align 4,,10
	.p2align 3
.L3213:
	movl	$6, %edx
	leaq	.LC498(%rip), %rsi
	jmp	.L3210
	.p2align 4,,10
	.p2align 3
.L3214:
	movl	$5, %edx
	leaq	.LC499(%rip), %rsi
	jmp	.L3210
	.p2align 4,,10
	.p2align 3
.L3211:
	movl	$3, %edx
	leaq	.LC496(%rip), %rsi
	jmp	.L3210
	.p2align 4,,10
	.p2align 3
.L3209:
	movl	$7, %edx
	leaq	.LC495(%rip), %rsi
	jmp	.L3210
	.p2align 4,,10
	.p2align 3
.L3204:
	testb	%al, %al
	je	.L3207
	jmp	.L3205
	.p2align 4,,10
	.p2align 3
.L3027:
	movq	%r14, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r14), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L3028
	movq	%r14, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L3028
	.p2align 4,,10
	.p2align 3
.L3378:
	leaq	.LC552(%rip), %rsi
	movl	$10, %edx
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-488(%rbp), %rsi
	movq	-496(%rbp), %rax
	cmpq	%rsi, %rax
	je	.L3037
	movzbl	(%rax), %r13d
	xorl	%r12d, %r12d
	xorl	%r14d, %r14d
	movl	%r13d, %edx
	jmp	.L3067
	.p2align 4,,10
	.p2align 3
.L3395:
	movq	%rsi, %rdx
	addq	$1, %r12
	addl	$1, %r14d
	subq	%rax, %rdx
	cmpq	%rdx, %r12
	jnb	.L3394
.L3053:
	movzbl	(%rax,%r12), %edx
.L3067:
	cmpb	%dl, %r13b
	je	.L3395
	movl	$1, %edx
	leaq	.LC553(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC553(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$10, %r13b
	ja	.L3040
	leaq	.L3042(%rip), %rcx
	movslq	(%rcx,%r13,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm16PrintRawWasmCodeEPNS0_19AccountingAllocatorERKNS1_12FunctionBodyEPKNS1_10WasmModuleENS1_11PrintLocalsERSoPSt6vectorIiSaIiEE
	.align 4
	.align 4
.L3042:
	.long	.L3052-.L3042
	.long	.L3242-.L3042
	.long	.L3050-.L3042
	.long	.L3049-.L3042
	.long	.L3048-.L3042
	.long	.L3047-.L3042
	.long	.L3046-.L3042
	.long	.L3045-.L3042
	.long	.L3044-.L3042
	.long	.L3043-.L3042
	.long	.L3041-.L3042
	.section	.text._ZN2v88internal4wasm16PrintRawWasmCodeEPNS0_19AccountingAllocatorERKNS1_12FunctionBodyEPKNS1_10WasmModuleENS1_11PrintLocalsERSoPSt6vectorIiSaIiEE
.L3388:
	jne	.L3171
.L3170:
	movq	%r8, %rsi
	leaq	-192(%rbp), %rdi
	movl	$2, %ebx
	leaq	.LC505(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	-336(%rbp), %r8
	jmp	.L3087
.L3391:
	andl	$127, %edx
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE0ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi1EEET_PKhPjPKcS7_.isra.0
	movl	-464(%rbp), %edi
	addl	$1, %r11d
	addq	%r8, %rdi
	cmpl	%r9d, %r11d
	jbe	.L3134
.L3135:
	subq	%r10, %rdi
	movq	-336(%rbp), %r8
	leal	1(%rdi), %ebx
	jmp	.L3087
.L3064:
	movl	$3, %edx
	leaq	.LC500(%rip), %rsi
.L3065:
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L3037:
	movq	(%r15), %rax
	movq	-24(%rax), %rax
	movq	240(%r15,%rax), %r12
	testq	%r12, %r12
	je	.L3068
	cmpb	$0, 56(%r12)
	je	.L3069
	movsbl	67(%r12), %esi
.L3070:
	movq	%r15, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movq	-560(%rbp), %rcx
	testq	%rcx, %rcx
	je	.L3071
	movq	8(%rcx), %rsi
	cmpq	16(%rcx), %rsi
	je	.L3072
	movl	-532(%rbp), %eax
	movl	%eax, (%rsi)
	addq	$4, 8(%rcx)
.L3071:
	movq	16(%rbx), %r14
	cmpq	-336(%rbp), %r14
	jnb	.L3073
	movq	%r14, %rax
	leaq	-464(%rbp), %r12
	leaq	.LC570(%rip), %r13
	jmp	.L3229
	.p2align 4,,10
	.p2align 3
.L3345:
	movq	%r15, %rdi
	addq	$1, %r14
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movzbl	-1(%r14), %eax
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, -464(%rbp)
	movl	$2, %eax
	movw	%ax, -456(%rbp)
	call	_ZN2v88internallsERSoRKNS0_5AsHexE@PLT
	movl	$1, %edx
	leaq	.LC556(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpq	-336(%rbp), %r14
	jnb	.L3073
	movq	16(%rbx), %rax
.L3229:
	movl	$3, %edx
	leaq	.LC563(%rip), %rsi
	cmpq	%rax, %r14
	jne	.L3345
	movl	$2, %edx
	movq	%r13, %rsi
	jmp	.L3345
	.p2align 4,,10
	.p2align 3
.L3127:
	movb	$10, -460(%rbp)
.L3131:
	leaq	1(%r8), %rsi
	leaq	-192(%rbp), %rdi
	leaq	.LC502(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movl	-464(%rbp), %ebx
.L3128:
	movq	-336(%rbp), %r8
	addl	$1, %ebx
	jmp	.L3087
.L3215:
	movl	$9, %edx
	leaq	.LC489(%rip), %rsi
	jmp	.L3210
	.p2align 4,,10
	.p2align 3
.L3080:
	movq	%r12, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r12), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L3081
	movq	%r12, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L3081
	.p2align 4,,10
	.p2align 3
.L3383:
	movzbl	2(%rdx), %ecx
	movl	%ecx, %eax
	sall	$7, %eax
	andl	$16256, %eax
	orl	%eax, %ebx
	testb	%cl, %cl
	jns	.L3218
	movzbl	3(%rdx), %ecx
	movl	%ecx, %eax
	sall	$14, %eax
	andl	$2080768, %eax
	orl	%eax, %ebx
	testb	%cl, %cl
	jns	.L3218
	movzbl	4(%rdx), %ecx
	movl	%ecx, %eax
	sall	$21, %eax
	andl	$266338304, %eax
	orl	%eax, %ebx
	testb	%cl, %cl
	jns	.L3218
	movzbl	5(%rdx), %eax
	sall	$28, %eax
	orl	%eax, %ebx
	jmp	.L3218
	.p2align 4,,10
	.p2align 3
.L3107:
	movzbl	1(%r8), %eax
	orb	$-3, %ah
	subl	$64768, %eax
	cmpl	$209, %eax
	ja	.L3164
	leaq	.L3165(%rip), %rbx
	movslq	(%rbx,%rax,4), %rax
	addq	%rbx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm16PrintRawWasmCodeEPNS0_19AccountingAllocatorERKNS1_12FunctionBodyEPKNS1_10WasmModuleENS1_11PrintLocalsERSoPSt6vectorIiSaIiEE
	.align 4
	.align 4
.L3165:
	.long	.L3171-.L3165
	.long	.L3171-.L3165
	.long	.L3164-.L3165
	.long	.L3167-.L3165
	.long	.L3287-.L3165
	.long	.L3166-.L3165
	.long	.L3164-.L3165
	.long	.L3166-.L3165
	.long	.L3287-.L3165
	.long	.L3166-.L3165
	.long	.L3164-.L3165
	.long	.L3166-.L3165
	.long	.L3287-.L3165
	.long	.L3166-.L3165
	.long	.L3166-.L3165
	.long	.L3287-.L3165
	.long	.L3166-.L3165
	.long	.L3166-.L3165
	.long	.L3287-.L3165
	.long	.L3166-.L3165
	.long	.L3166-.L3165
	.long	.L3287-.L3165
	.long	.L3166-.L3165
	.long	.L3166-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3164-.L3165
	.long	.L3164-.L3165
	.long	.L3287-.L3165
	.long	.L3164-.L3165
	.long	.L3164-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3164-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3164-.L3165
	.long	.L3164-.L3165
	.long	.L3164-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3164-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3164-.L3165
	.long	.L3164-.L3165
	.long	.L3164-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3164-.L3165
	.long	.L3164-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3164-.L3165
	.long	.L3164-.L3165
	.long	.L3164-.L3165
	.long	.L3164-.L3165
	.long	.L3164-.L3165
	.long	.L3164-.L3165
	.long	.L3164-.L3165
	.long	.L3164-.L3165
	.long	.L3164-.L3165
	.long	.L3164-.L3165
	.long	.L3164-.L3165
	.long	.L3164-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3164-.L3165
	.long	.L3164-.L3165
	.long	.L3164-.L3165
	.long	.L3164-.L3165
	.long	.L3164-.L3165
	.long	.L3164-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.long	.L3287-.L3165
	.section	.text._ZN2v88internal4wasm16PrintRawWasmCodeEPNS0_19AccountingAllocatorERKNS1_12FunctionBodyEPKNS1_10WasmModuleENS1_11PrintLocalsERSoPSt6vectorIiSaIiEE
	.p2align 4,,10
	.p2align 3
.L3377:
	movq	-256(%rbp), %rax
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %rcx
	subq	-264(%rbp), %rax
	addl	-240(%rbp), %eax
	movq	%rcx, -272(%rbp)
	movl	%eax, -512(%rbp)
	movq	-224(%rbp), %rdi
	cmpq	-568(%rbp), %rdi
	je	.L3236
	call	_ZdlPv@PLT
	movl	-512(%rbp), %eax
.L3236:
	movq	-328(%rbp), %rdx
	addq	-336(%rbp), %rax
	movq	%rax, -336(%rbp)
	cmpq	%rdx, %rax
	jbe	.L3033
	movq	%rdx, -336(%rbp)
	movq	%rdx, %rax
	jmp	.L3033
.L3030:
	movq	%rcx, %rdi
	leaq	-532(%rbp), %rdx
	call	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	.p2align 4,,10
	.p2align 3
.L3342:
	movq	24(%rbx), %rdx
	movq	16(%rbx), %rax
	jmp	.L3025
	.p2align 4,,10
	.p2align 3
.L3385:
	movzbl	2(%rdx), %ecx
	movl	%ecx, %eax
	sall	$7, %eax
	andl	$16256, %eax
	orl	%eax, %ebx
	testb	%cl, %cl
	jns	.L3221
	movzbl	3(%rdx), %ecx
	movl	%ecx, %eax
	sall	$14, %eax
	andl	$2080768, %eax
	orl	%eax, %ebx
	testb	%cl, %cl
	jns	.L3221
	movzbl	4(%rdx), %ecx
	movl	%ecx, %eax
	sall	$21, %eax
	andl	$266338304, %eax
	orl	%eax, %ebx
	testb	%cl, %cl
	jns	.L3221
	movzbl	5(%rdx), %eax
	sall	$28, %eax
	orl	%eax, %ebx
	jmp	.L3221
	.p2align 4,,10
	.p2align 3
.L3384:
	movzbl	2(%rdx), %ecx
	movl	%ecx, %eax
	sall	$7, %eax
	andl	$16256, %eax
	orl	%eax, %ebx
	testb	%cl, %cl
	jns	.L3220
	movzbl	3(%rdx), %ecx
	movl	%ecx, %eax
	sall	$14, %eax
	andl	$2080768, %eax
	orl	%eax, %ebx
	testb	%cl, %cl
	jns	.L3220
	movzbl	4(%rdx), %ecx
	movl	%ecx, %eax
	sall	$21, %eax
	andl	$266338304, %eax
	orl	%eax, %ebx
	testb	%cl, %cl
	jns	.L3220
	movzbl	5(%rdx), %eax
	sall	$28, %eax
	orl	%eax, %ebx
	jmp	.L3220
	.p2align 4,,10
	.p2align 3
.L3386:
	movzbl	2(%rdx), %ecx
	movl	%ecx, %eax
	sall	$7, %eax
	andl	$16256, %eax
	orl	%eax, %ebx
	testb	%cl, %cl
	jns	.L3219
	movzbl	3(%rdx), %ecx
	movl	%ecx, %eax
	sall	$14, %eax
	andl	$2080768, %eax
	orl	%eax, %ebx
	testb	%cl, %cl
	jns	.L3219
	movzbl	4(%rdx), %ecx
	movl	%ecx, %eax
	sall	$21, %eax
	andl	$266338304, %eax
	orl	%eax, %ebx
	testb	%cl, %cl
	jns	.L3219
	movzbl	5(%rdx), %eax
	sall	$28, %eax
	orl	%eax, %ebx
	jmp	.L3219
.L3069:
	movq	%r12, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r12), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L3070
	movq	%r12, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L3070
.L3083:
	movq	%rbx, %rdi
	leaq	-532(%rbp), %rdx
	call	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	jmp	.L3082
.L3049:
	movl	$3, %edx
	leaq	.LC491(%rip), %rsi
	.p2align 4,,10
	.p2align 3
.L3051:
	movq	%r14, %rdi
	movl	$1, %r14d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-488(%rbp), %rsi
	movq	-496(%rbp), %rax
	movq	%rsi, %rdx
	movzbl	(%rax,%r12), %r13d
	addq	$1, %r12
	subq	%rax, %rdx
	cmpq	%rdx, %r12
	jb	.L3053
.L3394:
	movl	$1, %edx
	leaq	.LC553(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC553(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$10, %r13b
	ja	.L3054
	leaq	.L3056(%rip), %rdx
	movzbl	%r13b, %r8d
	movslq	(%rdx,%r8,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm16PrintRawWasmCodeEPNS0_19AccountingAllocatorERKNS1_12FunctionBodyEPKNS1_10WasmModuleENS1_11PrintLocalsERSoPSt6vectorIiSaIiEE
	.align 4
	.align 4
.L3056:
	.long	.L3066-.L3056
	.long	.L3243-.L3056
	.long	.L3064-.L3056
	.long	.L3063-.L3056
	.long	.L3062-.L3056
	.long	.L3061-.L3056
	.long	.L3060-.L3056
	.long	.L3059-.L3056
	.long	.L3058-.L3056
	.long	.L3057-.L3056
	.long	.L3055-.L3056
	.section	.text._ZN2v88internal4wasm16PrintRawWasmCodeEPNS0_19AccountingAllocatorERKNS1_12FunctionBodyEPKNS1_10WasmModuleENS1_11PrintLocalsERSoPSt6vectorIiSaIiEE
.L3242:
	movl	$3, %edx
	leaq	.LC490(%rip), %rsi
	jmp	.L3051
.L3050:
	movl	$3, %edx
	leaq	.LC500(%rip), %rsi
	jmp	.L3051
.L3052:
	movl	$6, %edx
	leaq	.LC498(%rip), %rsi
	jmp	.L3051
.L3041:
	movl	$5, %edx
	leaq	.LC499(%rip), %rsi
	jmp	.L3051
.L3047:
	movl	$4, %edx
	leaq	.LC497(%rip), %rsi
	jmp	.L3051
.L3048:
	movl	$3, %edx
	leaq	.LC492(%rip), %rsi
	jmp	.L3051
.L3043:
	movl	$3, %edx
	leaq	.LC496(%rip), %rsi
	jmp	.L3051
.L3044:
	movl	$7, %edx
	leaq	.LC495(%rip), %rsi
	jmp	.L3051
.L3045:
	movl	$7, %edx
	leaq	.LC494(%rip), %rsi
	jmp	.L3051
.L3046:
	movl	$6, %edx
	leaq	.LC493(%rip), %rsi
	jmp	.L3051
.L3143:
	cmpb	$0, 2(%r8)
	movzbl	3(%r8), %edx
	js	.L3157
	leaq	2(%r8), %rax
	movl	$5, %r9d
	movl	$6, %edi
	movl	$8, %ecx
	movl	$4, %ebx
	movl	$7, %esi
.L3158:
	testb	%dl, %dl
	jns	.L3087
	cmpb	$0, 2(%rax)
	movl	%r9d, %ebx
	jns	.L3087
	cmpb	$0, 3(%rax)
	movl	%edi, %ebx
	jns	.L3087
	cmpb	$0, 4(%rax)
	movl	%ecx, %ebx
	cmovns	%esi, %ebx
	jmp	.L3087
	.p2align 4,,10
	.p2align 3
.L3164:
	movq	%r8, %rsi
	leaq	-192(%rbp), %rdi
	movl	$2, %ebx
	leaq	.LC504(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	-336(%rbp), %r8
	jmp	.L3087
.L3166:
	movl	$3, %ebx
	jmp	.L3087
.L3167:
	movl	$18, %ebx
	jmp	.L3087
	.p2align 4,,10
	.p2align 3
.L3073:
	movq	(%r15), %rax
	movq	-24(%rax), %rax
	movq	240(%r15,%rax), %r12
	testq	%r12, %r12
	je	.L3068
	cmpb	$0, 56(%r12)
	je	.L3076
	movsbl	67(%r12), %esi
.L3077:
	movq	%r15, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movq	-560(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L3036
	movq	8(%rbx), %rsi
	cmpq	16(%rbx), %rsi
	je	.L3079
	movl	-532(%rbp), %eax
	movl	%eax, (%rsi)
	addq	$4, 8(%rbx)
	jmp	.L3036
.L3076:
	movq	%r12, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r12), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L3077
	movq	%r12, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L3077
.L3389:
	leaq	-464(%rbp), %r14
	andl	$127, %edx
	leaq	2(%r8), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE0ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi1EEET_PKhPjPKcS7_.isra.0
	movl	-464(%rbp), %eax
	movq	%rax, %rbx
	addq	$1, %rax
	addl	$1, %ebx
	jmp	.L3126
.L3114:
	movzbl	3(%r8), %ecx
	testb	%al, %al
	js	.L3116
	movl	%ecx, %eax
	leaq	2(%r8), %rdx
	movl	$5, %ecx
	movl	$7, %esi
	movl	$6, %r9d
	movl	$4, %ebx
	movl	$8, %edi
	jmp	.L3115
.L3393:
	andl	$127, %edx
	leaq	-440(%rbp), %rsi
	leaq	2(%r8), %rdi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE0ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi1EEET_PKhPjPKcS7_.isra.0
	movl	-460(%rbp), %ecx
	movl	-440(%rbp), %eax
	jmp	.L3123
.L3392:
	leaq	-460(%rbp), %rsi
	leaq	2(%r8), %rdi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE0ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi1EEET_PKhPjPKcS7_.isra.0
	movl	%eax, %edx
	movl	-460(%rbp), %eax
	movq	%rax, %rcx
	jmp	.L3122
.L3390:
	leaq	-464(%rbp), %r14
	movl	%r9d, %edx
	leaq	2(%r8), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE0ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi1EEET_PKhPjPKcS7_.isra.0
	movl	-464(%rbp), %edx
	movl	%eax, %r9d
	addq	$1, %rdx
	jmp	.L3132
.L3040:
	movl	$9, %edx
	leaq	.LC489(%rip), %rsi
	jmp	.L3051
.L3235:
	movl	$3, %edx
	movq	%r14, %rsi
	jmp	.L3210
	.p2align 4,,10
	.p2align 3
.L3141:
	movabsq	$4294967296, %rax
	movl	$3, %ebx
	movq	%rax, -464(%rbp)
	testb	%cl, %cl
	jns	.L3087
	movl	%ecx, %edx
	leaq	2(%r8), %rdi
	leaq	-460(%rbp), %rsi
	andl	$127, %edx
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE0ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi1EEET_PKhPjPKcS7_.isra.0
	movl	-460(%rbp), %eax
	movq	-336(%rbp), %r8
	leal	2(%rax), %ebx
	jmp	.L3087
.L3148:
	leaq	2(%r8), %rsi
	leaq	-460(%rbp), %rdx
	xorl	%r8d, %r8d
	leaq	-192(%rbp), %rdi
	leaq	.LC549(%rip), %rcx
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIiLNS2_12ValidateFlagE0ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	movl	-460(%rbp), %eax
	movq	-336(%rbp), %r8
	leal	2(%rax), %ebx
	jmp	.L3087
.L3149:
	cmpb	$0, 2(%r8)
	movl	$4, %ebx
	jns	.L3087
	cmpb	$0, 3(%r8)
	movl	$5, %ebx
	jns	.L3087
	cmpb	$0, 4(%r8)
	movl	$6, %ebx
	jns	.L3087
	movsbl	5(%r8), %ebx
	shrl	$31, %ebx
	addl	$7, %ebx
	jmp	.L3087
	.p2align 4,,10
	.p2align 3
.L3144:
	leaq	2(%r8), %rsi
	leaq	-460(%rbp), %rdx
	xorl	%r8d, %r8d
	leaq	-192(%rbp), %rdi
	leaq	.LC542(%rip), %rcx
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIiLNS2_12ValidateFlagE0ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	movl	-460(%rbp), %eax
	movq	-336(%rbp), %r8
	leal	2(%rax), %ebx
	jmp	.L3087
.L3147:
	movl	$4, %ebx
	jmp	.L3087
.L3243:
	movl	$3, %edx
	leaq	.LC490(%rip), %rsi
	jmp	.L3065
.L3066:
	movl	$6, %edx
	leaq	.LC498(%rip), %rsi
	jmp	.L3065
.L3055:
	movl	$5, %edx
	leaq	.LC499(%rip), %rsi
	jmp	.L3065
.L3057:
	movl	$3, %edx
	leaq	.LC496(%rip), %rsi
	jmp	.L3065
.L3058:
	movl	$7, %edx
	leaq	.LC495(%rip), %rsi
	jmp	.L3065
.L3059:
	movl	$7, %edx
	leaq	.LC494(%rip), %rsi
	jmp	.L3065
.L3060:
	movl	$6, %edx
	leaq	.LC493(%rip), %rsi
	jmp	.L3065
.L3061:
	movl	$4, %edx
	leaq	.LC497(%rip), %rsi
	jmp	.L3065
.L3062:
	movl	$3, %edx
	leaq	.LC492(%rip), %rsi
	jmp	.L3065
.L3063:
	movl	$3, %edx
	leaq	.LC491(%rip), %rsi
	jmp	.L3065
.L3130:
	movb	$0, -460(%rbp)
	jmp	.L3131
.L3116:
	testb	%cl, %cl
	js	.L3117
	movzbl	4(%r8), %eax
	leaq	3(%r8), %rdx
	movl	$6, %ecx
	movl	$8, %esi
	movl	$7, %r9d
	movl	$5, %ebx
	movl	$9, %edi
	jmp	.L3115
.L3140:
	movq	%r8, %rsi
	leaq	-192(%rbp), %rdi
	movl	$2, %ebx
	leaq	.LC503(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	-336(%rbp), %r8
	jmp	.L3087
.L3072:
	movq	%rcx, %rdi
	leaq	-532(%rbp), %rdx
	call	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	jmp	.L3071
.L3079:
	movq	%rbx, %rdi
	leaq	-532(%rbp), %rdx
	call	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	jmp	.L3036
.L3157:
	testb	%dl, %dl
	js	.L3159
	movzbl	4(%r8), %edx
	leaq	3(%r8), %rax
	movl	$6, %r9d
	movl	$7, %edi
	movl	$9, %ecx
	movl	$5, %ebx
	movl	$8, %esi
	jmp	.L3158
.L3117:
	cmpb	$0, 4(%r8)
	js	.L3118
	movzbl	5(%r8), %eax
	leaq	4(%r8), %rdx
	movl	$7, %ecx
	movl	$9, %esi
	movl	$8, %r9d
	movl	$6, %ebx
	movl	$10, %edi
	jmp	.L3115
.L3054:
	movl	$9, %edx
	leaq	.LC489(%rip), %rsi
	jmp	.L3065
.L3159:
	cmpb	$0, 4(%r8)
	js	.L3160
	movzbl	5(%r8), %edx
	leaq	4(%r8), %rax
	movl	$7, %r9d
	movl	$8, %edi
	movl	$10, %ecx
	movl	$6, %ebx
	movl	$9, %esi
	jmp	.L3158
.L3118:
	movzbl	6(%r8), %eax
	leaq	5(%r8), %rdx
	movl	$8, %ecx
	movl	$10, %esi
	movl	$9, %r9d
	movl	$7, %ebx
	movl	$11, %edi
	jmp	.L3115
.L3068:
	call	_ZSt16__throw_bad_castv@PLT
.L3160:
	cmpb	$0, 5(%r8)
	js	.L3161
	movzbl	6(%r8), %edx
	leaq	5(%r8), %rax
	movl	$8, %r9d
	movl	$9, %edi
	movl	$11, %ecx
	movl	$7, %ebx
	movl	$10, %esi
	jmp	.L3158
.L3161:
	movzbl	7(%r8), %edx
	leaq	6(%r8), %rax
	movl	$9, %r9d
	movl	$10, %edi
	movl	$12, %ecx
	movl	$8, %ebx
	movl	$11, %esi
	jmp	.L3158
.L3382:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19617:
	.size	_ZN2v88internal4wasm16PrintRawWasmCodeEPNS0_19AccountingAllocatorERKNS1_12FunctionBodyEPKNS1_10WasmModuleENS1_11PrintLocalsERSoPSt6vectorIiSaIiEE, .-_ZN2v88internal4wasm16PrintRawWasmCodeEPNS0_19AccountingAllocatorERKNS1_12FunctionBodyEPKNS1_10WasmModuleENS1_11PrintLocalsERSoPSt6vectorIiSaIiEE
	.section	.text._ZN2v88internal4wasm16PrintRawWasmCodeEPNS0_19AccountingAllocatorERKNS1_12FunctionBodyEPKNS1_10WasmModuleENS1_11PrintLocalsE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm16PrintRawWasmCodeEPNS0_19AccountingAllocatorERKNS1_12FunctionBodyEPKNS1_10WasmModuleENS1_11PrintLocalsE
	.type	_ZN2v88internal4wasm16PrintRawWasmCodeEPNS0_19AccountingAllocatorERKNS1_12FunctionBodyEPKNS1_10WasmModuleENS1_11PrintLocalsE, @function
_ZN2v88internal4wasm16PrintRawWasmCodeEPNS0_19AccountingAllocatorERKNS1_12FunctionBodyEPKNS1_10WasmModuleENS1_11PrintLocalsE:
.LFB19616:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-400(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-320(%rbp), %rbx
	subq	$376, %rsp
	movq	%rdi, -408(%rbp)
	movq	%rbx, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNSt8ios_baseC2Ev@PLT
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	stdout(%rip), %rdx
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %r10
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movups	%xmm0, -88(%rbp)
	movq	%r10, -320(%rbp)
	movups	%xmm0, -72(%rbp)
	movw	%ax, -96(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	xorl	%r9d, %r9d
	movq	%r12, %r8
	movq	%r14, %rdx
	movq	-408(%rbp), %r11
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movq	%r13, %rsi
	movl	%r15d, %ecx
	movq	%rax, -400(%rbp)
	addq	$40, %rax
	movq	%r11, %rdi
	movq	%rax, -320(%rbp)
	call	_ZN2v88internal4wasm16PrintRawWasmCodeEPNS0_19AccountingAllocatorERKNS1_12FunctionBodyEPKNS1_10WasmModuleENS1_11PrintLocalsERSoPSt6vectorIiSaIiEE
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	leaq	-336(%rbp), %rdi
	movl	%eax, %r12d
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	movq	%rcx, %xmm0
	movq	%rax, -320(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -400(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %r10
	movq	%rbx, %rdi
	movq	%rax, -400(%rbp)
	movq	%r10, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3399
	addq	$376, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3399:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19616:
	.size	_ZN2v88internal4wasm16PrintRawWasmCodeEPNS0_19AccountingAllocatorERKNS1_12FunctionBodyEPKNS1_10WasmModuleENS1_11PrintLocalsE, .-_ZN2v88internal4wasm16PrintRawWasmCodeEPNS0_19AccountingAllocatorERKNS1_12FunctionBodyEPKNS1_10WasmModuleENS1_11PrintLocalsE
	.section	.text._ZN2v88internal4wasm16PrintRawWasmCodeEPKhS3_,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm16PrintRawWasmCodeEPKhS3_
	.type	_ZN2v88internal4wasm16PrintRawWasmCodeEPKhS3_, @function
_ZN2v88internal4wasm16PrintRawWasmCodeEPKhS3_:
.LFB19611:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsi, %xmm1
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-96(%rbp), %r12
	leaq	-64(%rbp), %rsi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movups	%xmm0, -88(%rbp)
	movq	%rdi, %xmm0
	leaq	16+_ZTVN2v88internal19AccountingAllocatorE(%rip), %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%r12, %rdi
	movq	%rax, -96(%rbp)
	movaps	%xmm0, -48(%rbp)
	movq	$0, -64(%rbp)
	movl	$0, -56(%rbp)
	call	_ZN2v88internal4wasm16PrintRawWasmCodeEPNS0_19AccountingAllocatorERKNS1_12FunctionBodyEPKNS1_10WasmModuleENS1_11PrintLocalsE
	movq	%r12, %rdi
	call	_ZN2v88internal19AccountingAllocatorD1Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3403
	addq	$88, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3403:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19611:
	.size	_ZN2v88internal4wasm16PrintRawWasmCodeEPKhS3_, .-_ZN2v88internal4wasm16PrintRawWasmCodeEPKhS3_
	.section	.text._ZNSt6vectorIN2v88internal4wasm9ValueBaseENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal4wasm9ValueBaseENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal4wasm9ValueBaseENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal4wasm9ValueBaseENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_, @function
_ZNSt6vectorIN2v88internal4wasm9ValueBaseENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_:
.LFB24148:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r13
	movq	8(%rdi), %r14
	movq	%r13, %rax
	subq	%r14, %rax
	sarq	$4, %rax
	cmpq	$134217727, %rax
	je	.L3420
	movq	%rsi, %rcx
	movq	%rdi, %r12
	movq	%rsi, %rbx
	movq	%rdx, %r15
	subq	%r14, %rcx
	testq	%rax, %rax
	je	.L3414
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L3421
	movl	$2147483632, %esi
	movl	$2147483632, %r8d
.L3406:
	movq	(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rsi, %rdx
	jb	.L3422
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L3409:
	addq	%rax, %r8
	leaq	16(%rax), %r9
	jmp	.L3407
	.p2align 4,,10
	.p2align 3
.L3421:
	testq	%rdx, %rdx
	jne	.L3423
	movl	$16, %r9d
	xorl	%r8d, %r8d
	xorl	%eax, %eax
.L3407:
	movq	(%r15), %rsi
	movzbl	8(%r15), %edx
	addq	%rax, %rcx
	movq	%rsi, (%rcx)
	movb	%dl, 8(%rcx)
	cmpq	%r14, %rbx
	je	.L3410
	movq	%r14, %rdx
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L3411:
	movq	(%rdx), %rdi
	movzbl	8(%rdx), %esi
	addq	$16, %rdx
	addq	$16, %rcx
	movq	%rdi, -16(%rcx)
	movb	%sil, -8(%rcx)
	cmpq	%rdx, %rbx
	jne	.L3411
	movq	%rbx, %rdx
	subq	%r14, %rdx
	leaq	16(%rax,%rdx), %r9
.L3410:
	cmpq	%r13, %rbx
	je	.L3412
	movq	%rbx, %rdx
	movq	%r9, %rcx
	.p2align 4,,10
	.p2align 3
.L3413:
	movq	(%rdx), %rdi
	movzbl	8(%rdx), %esi
	addq	$16, %rdx
	addq	$16, %rcx
	movq	%rdi, -16(%rcx)
	movb	%sil, -8(%rcx)
	cmpq	%rdx, %r13
	jne	.L3413
	subq	%rbx, %r13
	addq	%r13, %r9
.L3412:
	movq	%rax, %xmm0
	movq	%r9, %xmm1
	movq	%r8, 24(%r12)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3414:
	.cfi_restore_state
	movl	$16, %esi
	movl	$16, %r8d
	jmp	.L3406
.L3422:
	movq	%rcx, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %rcx
	jmp	.L3409
.L3423:
	cmpq	$134217727, %rdx
	movl	$134217727, %r8d
	cmovbe	%rdx, %r8
	salq	$4, %r8
	movq	%r8, %rsi
	jmp	.L3406
.L3420:
	leaq	.LC506(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE24148:
	.size	_ZNSt6vectorIN2v88internal4wasm9ValueBaseENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_, .-_ZNSt6vectorIN2v88internal4wasm9ValueBaseENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15PushMergeValuesEPNS1_11ControlBaseINS1_9ValueBaseEEEPNS1_5MergeIS8_EE,"axG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15PushMergeValuesEPNS1_11ControlBaseINS1_9ValueBaseEEEPNS1_5MergeIS8_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15PushMergeValuesEPNS1_11ControlBaseINS1_9ValueBaseEEEPNS1_5MergeIS8_EE
	.type	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15PushMergeValuesEPNS1_11ControlBaseINS1_9ValueBaseEEEPNS1_5MergeIS8_EE, @function
_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15PushMergeValuesEPNS1_11ControlBaseINS1_9ValueBaseEEEPNS1_5MergeIS8_EE:
.LFB23437:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	176(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	4(%rsi), %esi
	salq	$4, %rsi
	addq	184(%rdi), %rsi
	cmpq	192(%rdi), %rsi
	je	.L3425
	movq	%rsi, 192(%rdi)
.L3425:
	movl	0(%r13), %eax
	cmpl	$1, %eax
	je	.L3437
	testl	%eax, %eax
	je	.L3424
	xorl	%ebx, %ebx
	jmp	.L3432
	.p2align 4,,10
	.p2align 3
.L3438:
	movq	(%rdx), %rcx
	movzbl	8(%rdx), %eax
	addl	$1, %ebx
	movq	%rcx, (%rsi)
	movb	%al, 8(%rsi)
	addq	$16, 192(%r12)
	cmpl	%ebx, 0(%r13)
	jbe	.L3424
.L3431:
	movq	192(%r12), %rsi
.L3432:
	movl	%ebx, %edx
	salq	$4, %rdx
	addq	8(%r13), %rdx
	cmpq	%rsi, 200(%r12)
	jne	.L3438
	movq	%r14, %rdi
	addl	$1, %ebx
	call	_ZNSt6vectorIN2v88internal4wasm9ValueBaseENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	cmpl	%ebx, 0(%r13)
	ja	.L3431
.L3424:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3437:
	.cfi_restore_state
	cmpq	200(%r12), %rsi
	je	.L3427
	movq	8(%r13), %rdx
	movzbl	16(%r13), %eax
	movq	%rdx, (%rsi)
	movb	%al, 8(%rsi)
	popq	%rbx
	addq	$16, 192(%r12)
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3427:
	.cfi_restore_state
	popq	%rbx
	leaq	8(%r13), %rdx
	popq	%r12
	movq	%r14, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt6vectorIN2v88internal4wasm9ValueBaseENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	.cfi_endproc
.LFE23437:
	.size	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15PushMergeValuesEPNS1_11ControlBaseINS1_9ValueBaseEEEPNS1_5MergeIS8_EE, .-_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15PushMergeValuesEPNS1_11ControlBaseINS1_9ValueBaseEEEPNS1_5MergeIS8_EE
	.section	.text._ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_,"axG",@progbits,_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	.type	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_, @function
_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_:
.LFB24225:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	24(%rdi), %r10
	cmpq	%rsi, %r10
	ja	.L3457
	movl	$0, (%rdx)
	xorl	%eax, %eax
	leaq	.LC487(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	xorl	%eax, %eax
.L3439:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3457:
	.cfi_restore_state
	movzbl	(%rsi), %r9d
	movl	%r9d, %eax
	andl	$127, %eax
	orl	%r8d, %eax
	testb	%r9b, %r9b
	js	.L3458
	movl	$1, (%rdx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3458:
	.cfi_restore_state
	leaq	1(%rsi), %r8
	cmpq	%r8, %r10
	jbe	.L3442
	movzbl	1(%rsi), %r9d
	movl	%r9d, %r8d
	sall	$7, %r8d
	andl	$16256, %r8d
	orl	%r8d, %eax
	testb	%r9b, %r9b
	js	.L3459
	movl	$2, (%rdx)
	jmp	.L3439
	.p2align 4,,10
	.p2align 3
.L3442:
	movl	$1, (%rdx)
.L3456:
	xorl	%eax, %eax
	leaq	.LC487(%rip), %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3459:
	.cfi_restore_state
	leaq	2(%rsi), %r8
	cmpq	%r8, %r10
	jbe	.L3444
	movzbl	2(%rsi), %r9d
	movl	%r9d, %r8d
	sall	$14, %r8d
	andl	$2080768, %r8d
	orl	%r8d, %eax
	testb	%r9b, %r9b
	js	.L3460
	movl	$3, (%rdx)
	jmp	.L3439
	.p2align 4,,10
	.p2align 3
.L3444:
	movl	$2, (%rdx)
	jmp	.L3456
	.p2align 4,,10
	.p2align 3
.L3460:
	leaq	3(%rsi), %r8
	cmpq	%r8, %r10
	ja	.L3461
	movl	$3, (%rdx)
	jmp	.L3456
.L3461:
	movzbl	3(%rsi), %r9d
	movl	%r9d, %r8d
	sall	$21, %r8d
	andl	$266338304, %r8d
	orl	%r8d, %eax
	testb	%r9b, %r9b
	js	.L3462
	movl	$4, (%rdx)
	jmp	.L3439
.L3462:
	leaq	4(%rsi), %r12
	cmpq	%r12, %r10
	jbe	.L3448
	movzbl	4(%rsi), %ebx
	movl	$5, (%rdx)
	testb	%bl, %bl
	js	.L3449
	movl	%ebx, %edx
	sall	$28, %edx
	orl	%edx, %eax
.L3450:
	andl	$240, %ebx
	je	.L3439
	leaq	.LC488(%rip), %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	xorl	%eax, %eax
	jmp	.L3439
.L3448:
	movl	$4, (%rdx)
	xorl	%ebx, %ebx
.L3449:
	xorl	%eax, %eax
	leaq	.LC487(%rip), %rdx
	movq	%r12, %rsi
	movq	%rdi, -24(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-24(%rbp), %rdi
	xorl	%eax, %eax
	jmp	.L3450
	.cfi_endproc
.LFE24225:
	.size	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_, .-_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	.section	.text._ZN2v88internal4wasm7Decoder13read_leb_tailIiLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_,"axG",@progbits,_ZN2v88internal4wasm7Decoder13read_leb_tailIiLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm7Decoder13read_leb_tailIiLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	.type	_ZN2v88internal4wasm7Decoder13read_leb_tailIiLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_, @function
_ZN2v88internal4wasm7Decoder13read_leb_tailIiLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_:
.LFB24305:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	24(%rdi), %r10
	cmpq	%rsi, %r10
	ja	.L3482
	movl	$0, (%rdx)
	xorl	%eax, %eax
	leaq	.LC487(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	xorl	%eax, %eax
.L3463:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3482:
	.cfi_restore_state
	movzbl	(%rsi), %r9d
	movl	%r9d, %eax
	andl	$127, %eax
	orl	%r8d, %eax
	testb	%r9b, %r9b
	js	.L3483
	movl	$1, (%rdx)
	sall	$25, %eax
	addq	$16, %rsp
	popq	%rbx
	sarl	$25, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3483:
	.cfi_restore_state
	leaq	1(%rsi), %r8
	cmpq	%r8, %r10
	jbe	.L3466
	movzbl	1(%rsi), %r9d
	movl	%r9d, %r8d
	sall	$7, %r8d
	andl	$16256, %r8d
	orl	%r8d, %eax
	testb	%r9b, %r9b
	js	.L3484
	sall	$18, %eax
	movl	$2, (%rdx)
	sarl	$18, %eax
	jmp	.L3463
	.p2align 4,,10
	.p2align 3
.L3466:
	movl	$1, (%rdx)
.L3481:
	xorl	%eax, %eax
	leaq	.LC487(%rip), %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3484:
	.cfi_restore_state
	leaq	2(%rsi), %r8
	cmpq	%r8, %r10
	jbe	.L3468
	movzbl	2(%rsi), %r8d
	movl	%r8d, %r9d
	sall	$14, %r9d
	andl	$2080768, %r9d
	orl	%r9d, %eax
	testb	%r8b, %r8b
	js	.L3485
	sall	$11, %eax
	movl	$3, (%rdx)
	sarl	$11, %eax
	jmp	.L3463
	.p2align 4,,10
	.p2align 3
.L3468:
	movl	$2, (%rdx)
	jmp	.L3481
	.p2align 4,,10
	.p2align 3
.L3485:
	leaq	3(%rsi), %r8
	cmpq	%r8, %r10
	ja	.L3486
	movl	$3, (%rdx)
	jmp	.L3481
.L3486:
	movzbl	3(%rsi), %r9d
	movl	%r9d, %r8d
	sall	$21, %r8d
	andl	$266338304, %r8d
	orl	%r8d, %eax
	testb	%r9b, %r9b
	js	.L3487
	sall	$4, %eax
	movl	$4, (%rdx)
	sarl	$4, %eax
	jmp	.L3463
.L3487:
	leaq	4(%rsi), %r12
	cmpq	%r12, %r10
	jbe	.L3472
	movzbl	4(%rsi), %ebx
	movl	$5, (%rdx)
	testb	%bl, %bl
	js	.L3473
	movl	%ebx, %edx
	sall	$28, %edx
	orl	%edx, %eax
.L3474:
	andl	$248, %ebx
	je	.L3463
	cmpb	$120, %bl
	je	.L3463
	leaq	.LC488(%rip), %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	xorl	%eax, %eax
	jmp	.L3463
.L3472:
	movl	$4, (%rdx)
	xorl	%ebx, %ebx
.L3473:
	xorl	%eax, %eax
	leaq	.LC487(%rip), %rdx
	movq	%r12, %rsi
	movq	%rdi, -24(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-24(%rbp), %rdi
	xorl	%eax, %eax
	jmp	.L3474
	.cfi_endproc
.LFE24305:
	.size	_ZN2v88internal4wasm7Decoder13read_leb_tailIiLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_, .-_ZN2v88internal4wasm7Decoder13read_leb_tailIiLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	.section	.text._ZN2v84base11SmallVectorINS_8internal4wasm9ValueBaseELm8EE4GrowEm,"axG",@progbits,_ZN2v84base11SmallVectorINS_8internal4wasm9ValueBaseELm8EE4GrowEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base11SmallVectorINS_8internal4wasm9ValueBaseELm8EE4GrowEm
	.type	_ZN2v84base11SmallVectorINS_8internal4wasm9ValueBaseELm8EE4GrowEm, @function
_ZN2v84base11SmallVectorINS_8internal4wasm9ValueBaseELm8EE4GrowEm:
.LFB24307:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	movq	8(%rdi), %r12
	movq	16(%rdi), %rdi
	subq	%rax, %r12
	subq	%rax, %rdi
	sarq	$4, %rdi
	addq	%rdi, %rdi
	cmpq	%rsi, %rdi
	cmovb	%rsi, %rdi
	call	_ZN2v84base4bits21RoundUpToPowerOfTwo64Em@PLT
	salq	$4, %rax
	movq	%rax, %rdi
	movq	%rax, %r14
	call	malloc@PLT
	movq	(%rbx), %r15
	movq	%r12, %rdx
	movq	%rax, %rdi
	movq	%rax, %r13
	movq	%r15, %rsi
	call	memcpy@PLT
	leaq	24(%rbx), %rax
	cmpq	%rax, %r15
	je	.L3489
	movq	%r15, %rdi
	call	free@PLT
.L3489:
	leaq	0(%r13,%r12), %rax
	movq	%r13, (%rbx)
	addq	%r14, %r13
	movq	%r13, 16(%rbx)
	movq	%rax, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24307:
	.size	_ZN2v84base11SmallVectorINS_8internal4wasm9ValueBaseELm8EE4GrowEm, .-_ZN2v84base11SmallVectorINS_8internal4wasm9ValueBaseELm8EE4GrowEm
	.section	.text._ZN2v84base11SmallVectorINS_8internal4wasm9ValueBaseELm8EE14resize_no_initEm,"axG",@progbits,_ZN2v84base11SmallVectorINS_8internal4wasm9ValueBaseELm8EE14resize_no_initEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base11SmallVectorINS_8internal4wasm9ValueBaseELm8EE14resize_no_initEm
	.type	_ZN2v84base11SmallVectorINS_8internal4wasm9ValueBaseELm8EE14resize_no_initEm, @function
_ZN2v84base11SmallVectorINS_8internal4wasm9ValueBaseELm8EE14resize_no_initEm:
.LFB24143:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	16(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	movq	(%rdi), %rsi
	subq	%rsi, %rax
	sarq	$4, %rax
	cmpq	%rax, %rbx
	ja	.L3494
.L3492:
	salq	$4, %rbx
	addq	%rsi, %rbx
	movq	%rbx, 8(%r12)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3494:
	.cfi_restore_state
	movq	%rbx, %rsi
	call	_ZN2v84base11SmallVectorINS_8internal4wasm9ValueBaseELm8EE4GrowEm
	movq	(%r12), %rsi
	jmp	.L3492
	.cfi_endproc
.LFE24143:
	.size	_ZN2v84base11SmallVectorINS_8internal4wasm9ValueBaseELm8EE14resize_no_initEm, .-_ZN2v84base11SmallVectorINS_8internal4wasm9ValueBaseELm8EE14resize_no_initEm
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE18DecodeAtomicOpcodeENS1_10WasmOpcodeE.str1.1,"aMS",@progbits,1
.LC571:
	.string	"invalid atomic opcode"
.LC572:
	.string	"zero"
.LC573:
	.string	"invalid atomic operand"
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE18DecodeAtomicOpcodeENS1_10WasmOpcodeE.str1.8,"aMS",@progbits,1
	.align 8
.LC574:
	.string	"Atomic opcodes used without shared memory"
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE18DecodeAtomicOpcodeENS1_10WasmOpcodeE,"axG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE18DecodeAtomicOpcodeENS1_10WasmOpcodeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE18DecodeAtomicOpcodeENS1_10WasmOpcodeE
	.type	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE18DecodeAtomicOpcodeENS1_10WasmOpcodeE, @function
_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE18DecodeAtomicOpcodeENS1_10WasmOpcodeE:
.LFB23612:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	%esi, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$280, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm11WasmOpcodes9SignatureENS1_10WasmOpcodeE@PLT
	testq	%rax, %rax
	je	.L3498
	leal	-65024(%rbx), %esi
	cmpl	$78, %esi
	ja	.L3498
	leaq	.L3500(%rip), %rdx
	movq	%rax, %r15
	movslq	(%rdx,%rsi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE18DecodeAtomicOpcodeENS1_10WasmOpcodeE,"aG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE18DecodeAtomicOpcodeENS1_10WasmOpcodeE,comdat
	.align 4
	.align 4
.L3500:
	.long	.L3563-.L3500
	.long	.L3562-.L3500
	.long	.L3561-.L3500
	.long	.L3560-.L3500
	.long	.L3498-.L3500
	.long	.L3498-.L3500
	.long	.L3498-.L3500
	.long	.L3498-.L3500
	.long	.L3498-.L3500
	.long	.L3498-.L3500
	.long	.L3498-.L3500
	.long	.L3498-.L3500
	.long	.L3498-.L3500
	.long	.L3498-.L3500
	.long	.L3498-.L3500
	.long	.L3498-.L3500
	.long	.L3559-.L3500
	.long	.L3558-.L3500
	.long	.L3557-.L3500
	.long	.L3556-.L3500
	.long	.L3555-.L3500
	.long	.L3554-.L3500
	.long	.L3553-.L3500
	.long	.L3549-.L3500
	.long	.L3552-.L3500
	.long	.L3615-.L3500
	.long	.L3550-.L3500
	.long	.L3615-.L3500
	.long	.L3550-.L3500
	.long	.L3549-.L3500
	.long	.L3548-.L3500
	.long	.L3547-.L3500
	.long	.L3546-.L3500
	.long	.L3545-.L3500
	.long	.L3544-.L3500
	.long	.L3543-.L3500
	.long	.L3542-.L3500
	.long	.L3541-.L3500
	.long	.L3540-.L3500
	.long	.L3539-.L3500
	.long	.L3538-.L3500
	.long	.L3537-.L3500
	.long	.L3536-.L3500
	.long	.L3535-.L3500
	.long	.L3534-.L3500
	.long	.L3533-.L3500
	.long	.L3532-.L3500
	.long	.L3531-.L3500
	.long	.L3530-.L3500
	.long	.L3529-.L3500
	.long	.L3528-.L3500
	.long	.L3527-.L3500
	.long	.L3526-.L3500
	.long	.L3525-.L3500
	.long	.L3524-.L3500
	.long	.L3523-.L3500
	.long	.L3522-.L3500
	.long	.L3521-.L3500
	.long	.L3520-.L3500
	.long	.L3519-.L3500
	.long	.L3518-.L3500
	.long	.L3517-.L3500
	.long	.L3516-.L3500
	.long	.L3515-.L3500
	.long	.L3514-.L3500
	.long	.L3513-.L3500
	.long	.L3512-.L3500
	.long	.L3511-.L3500
	.long	.L3510-.L3500
	.long	.L3509-.L3500
	.long	.L3508-.L3500
	.long	.L3507-.L3500
	.long	.L3506-.L3500
	.long	.L3505-.L3500
	.long	.L3504-.L3500
	.long	.L3503-.L3500
	.long	.L3502-.L3500
	.long	.L3501-.L3500
	.long	.L3499-.L3500
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE18DecodeAtomicOpcodeENS1_10WasmOpcodeE,"axG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE18DecodeAtomicOpcodeENS1_10WasmOpcodeE,comdat
.L3615:
	xorl	%r13d, %r13d
	xorl	%r11d, %r11d
.L3551:
	movq	80(%r12), %rax
	movq	16(%r12), %rbx
	cmpb	$0, 16(%rax)
	je	.L3705
.L3567:
	leaq	2(%rbx), %r14
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movb	%r11b, -232(%rbp)
	leaq	-216(%rbp), %rdx
	leaq	.LC532(%rip), %rcx
	movq	%r14, %rsi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	movzbl	-232(%rbp), %r11d
	cmpl	%r13d, %eax
	ja	.L3706
.L3568:
	leaq	-184(%rbp), %rax
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movb	%r11b, -264(%rbp)
	movq	%rax, -232(%rbp)
	movq	%rax, %xmm0
	movl	-216(%rbp), %eax
	leaq	-212(%rbp), %rdx
	punpcklqdq	%xmm0, %xmm0
	leaq	.LC534(%rip), %rcx
	movq	%rdx, -272(%rbp)
	leaq	-208(%rbp), %r13
	leaq	2(%rbx,%rax), %rsi
	movaps	%xmm0, -256(%rbp)
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	movq	8(%r15), %r14
	movl	-212(%rbp), %eax
	movl	-216(%rbp), %ecx
	movdqa	-256(%rbp), %xmm0
	movslq	%r14d, %rbx
	movzbl	-264(%rbp), %r11d
	addl	%eax, %ecx
	leaq	-56(%rbp), %rax
	cmpq	$8, %rbx
	movaps	%xmm0, -208(%rbp)
	movq	%rax, -192(%rbp)
	movq	-232(%rbp), %rax
	movl	%ecx, -236(%rbp)
	ja	.L3707
.L3569:
	salq	$4, %rbx
	addq	%rax, %rbx
	subl	$1, %r14d
	movq	%rbx, -200(%rbp)
	movslq	%r14d, %r13
	js	.L3611
	movl	%r14d, %eax
	movb	%r11b, -256(%rbp)
	movq	%r15, %r14
	movl	%eax, %r15d
	jmp	.L3612
	.p2align 4,,10
	.p2align 3
.L3710:
	cmpb	$2, -72(%rdi)
	movq	16(%r12), %rsi
	jne	.L3708
.L3574:
	movl	$10, %ebx
.L3578:
	movq	%r13, %rax
	subl	$1, %r15d
	subq	$1, %r13
	salq	$4, %rax
	addq	-208(%rbp), %rax
	movq	%rsi, (%rax)
	movb	%bl, 8(%rax)
	cmpl	$-1, %r15d
	je	.L3709
.L3612:
	movq	16(%r14), %rax
	movq	224(%r12), %rdi
	addq	%r13, %rax
	addq	(%r14), %rax
	movl	-84(%rdi), %esi
	movzbl	(%rax), %ecx
	movq	192(%r12), %rax
	movq	%rax, %rdx
	subq	184(%r12), %rdx
	sarq	$4, %rdx
	cmpq	%rdx, %rsi
	jnb	.L3710
	movzbl	-8(%rax), %ebx
	movq	-16(%rax), %rsi
	subq	$16, %rax
	movq	%rax, 192(%r12)
	cmpb	%cl, %bl
	je	.L3578
	cmpb	$6, %cl
	sete	%al
	cmpb	$8, %bl
	sete	%dl
	testb	%al, %al
	je	.L3685
	testb	%dl, %dl
	je	.L3685
	movl	$8, %ebx
	jmp	.L3578
.L3552:
	movq	80(%r12), %rax
	xorl	%r11d, %r11d
	movq	16(%r12), %rbx
	movl	$3, %r13d
	cmpb	$0, 16(%rax)
	jne	.L3567
.L3705:
	leaq	-1(%rbx), %rsi
	leaq	.LC574(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movl	$0, -236(%rbp)
	.p2align 4,,10
	.p2align 3
.L3495:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3711
	movl	-236(%rbp), %eax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3685:
	.cfi_restore_state
	leal	-7(%rbx), %edi
	andl	$253, %edi
	jne	.L3686
	testb	%al, %al
	jne	.L3578
.L3686:
	leal	-7(%rcx), %eax
	testb	$-3, %al
	sete	%al
	testb	%dl, %al
	jne	.L3578
	cmpb	$10, %bl
	setne	%dl
	cmpb	$10, %cl
	setne	%al
	testb	%al, %dl
	je	.L3578
	cmpb	$9, %bl
	ja	.L3581
	leaq	.L3583(%rip), %rdi
	movzbl	%bl, %edx
	movslq	(%rdi,%rdx,4), %rax
	addq	%rdi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE18DecodeAtomicOpcodeENS1_10WasmOpcodeE,"aG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE18DecodeAtomicOpcodeENS1_10WasmOpcodeE,comdat
	.align 4
	.align 4
.L3583:
	.long	.L3592-.L3583
	.long	.L3680-.L3583
	.long	.L3590-.L3583
	.long	.L3589-.L3583
	.long	.L3588-.L3583
	.long	.L3587-.L3583
	.long	.L3586-.L3583
	.long	.L3585-.L3583
	.long	.L3584-.L3583
	.long	.L3582-.L3583
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE18DecodeAtomicOpcodeENS1_10WasmOpcodeE,"axG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE18DecodeAtomicOpcodeENS1_10WasmOpcodeE,comdat
	.p2align 4,,10
	.p2align 3
.L3708:
	leaq	.LC482(%rip), %rcx
	cmpq	%rsi, 24(%r12)
	ja	.L3712
.L3575:
	leaq	.LC530(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	16(%r12), %rsi
	jmp	.L3574
	.p2align 4,,10
	.p2align 3
.L3709:
	movzbl	-256(%rbp), %r11d
	movq	%r14, %r15
.L3611:
	testb	%r11b, %r11b
	je	.L3572
	xorl	%eax, %eax
	cmpq	$0, (%r15)
	je	.L3613
	movq	16(%r15), %rax
	movzbl	(%rax), %eax
.L3613:
	movq	-272(%rbp), %rdx
	leaq	16(%r12), %rsi
	leaq	176(%r12), %rdi
	movb	%al, -212(%rbp)
	call	_ZNSt6vectorIN2v88internal4wasm9ValueBaseENS1_13ZoneAllocatorIS3_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_
.L3572:
	movq	-208(%rbp), %rdi
	cmpq	-232(%rbp), %rdi
	je	.L3495
	call	free@PLT
	jmp	.L3495
	.p2align 4,,10
	.p2align 3
.L3712:
	movzbl	(%rsi), %ebx
	movq	%rsi, -264(%rbp)
	movl	%ebx, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movq	-264(%rbp), %rsi
	testb	%al, %al
	je	.L3713
	leaq	1(%rsi), %rax
	cmpq	%rax, 24(%r12)
	ja	.L3577
	movq	16(%r12), %rsi
	leaq	.LC482(%rip), %rcx
	jmp	.L3575
	.p2align 4,,10
	.p2align 3
.L3707:
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movb	%r11b, -256(%rbp)
	call	_ZN2v84base11SmallVectorINS_8internal4wasm9ValueBaseELm8EE4GrowEm
	movq	-208(%rbp), %rax
	movzbl	-256(%rbp), %r11d
	jmp	.L3569
	.p2align 4,,10
	.p2align 3
.L3706:
	movl	%eax, %r8d
	movl	%r13d, %ecx
	leaq	.LC533(%rip), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movzbl	-232(%rbp), %r11d
	jmp	.L3568
.L3498:
	leaq	.LC571(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc
	movl	$0, -236(%rbp)
	jmp	.L3495
	.p2align 4,,10
	.p2align 3
.L3577:
	movzbl	1(%rsi), %edi
	sall	$8, %ebx
	orl	%ebx, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	16(%r12), %rsi
	movq	%rax, %rcx
	jmp	.L3575
	.p2align 4,,10
	.p2align 3
.L3713:
	movl	%ebx, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	16(%r12), %rsi
	movq	%rax, %rcx
	jmp	.L3575
.L3550:
	movl	$1, %r13d
	xorl	%r11d, %r11d
	jmp	.L3551
.L3499:
	cmpq	$0, (%r15)
	je	.L3674
	movq	16(%r15), %rax
	movl	$2, %r13d
	movzbl	(%rax), %r11d
	jmp	.L3551
.L3501:
	cmpq	$0, (%r15)
	je	.L3673
	movq	16(%r15), %rax
	movl	$1, %r13d
	movzbl	(%rax), %r11d
	jmp	.L3551
.L3503:
	cmpq	$0, (%r15)
	je	.L3671
	movq	16(%r15), %rax
	movl	$1, %r13d
	movzbl	(%rax), %r11d
	jmp	.L3551
.L3502:
	cmpq	$0, (%r15)
	je	.L3672
	movq	16(%r15), %rax
	xorl	%r13d, %r13d
	movzbl	(%rax), %r11d
	jmp	.L3551
.L3504:
	cmpq	$0, (%r15)
	je	.L3670
	movq	16(%r15), %rax
	xorl	%r13d, %r13d
	movzbl	(%rax), %r11d
	jmp	.L3551
.L3505:
	cmpq	$0, (%r15)
	je	.L3669
	movq	16(%r15), %rax
	movl	$3, %r13d
	movzbl	(%rax), %r11d
	jmp	.L3551
.L3509:
	cmpq	$0, (%r15)
	je	.L3665
	movq	16(%r15), %rax
	xorl	%r13d, %r13d
	movzbl	(%rax), %r11d
	jmp	.L3551
.L3507:
	cmpq	$0, (%r15)
	je	.L3667
	movq	16(%r15), %rax
	movl	$2, %r13d
	movzbl	(%rax), %r11d
	jmp	.L3551
.L3511:
	cmpq	$0, (%r15)
	je	.L3663
	movq	16(%r15), %rax
	xorl	%r13d, %r13d
	movzbl	(%rax), %r11d
	jmp	.L3551
.L3506:
	cmpq	$0, (%r15)
	je	.L3668
	movq	16(%r15), %rax
	movl	$2, %r13d
	movzbl	(%rax), %r11d
	jmp	.L3551
.L3510:
	cmpq	$0, (%r15)
	je	.L3664
	movq	16(%r15), %rax
	movl	$1, %r13d
	movzbl	(%rax), %r11d
	jmp	.L3551
.L3508:
	cmpq	$0, (%r15)
	je	.L3666
	movq	16(%r15), %rax
	movl	$1, %r13d
	movzbl	(%rax), %r11d
	jmp	.L3551
.L3512:
	cmpq	$0, (%r15)
	je	.L3662
	movq	16(%r15), %rax
	movl	$3, %r13d
	movzbl	(%rax), %r11d
	jmp	.L3551
.L3513:
	cmpq	$0, (%r15)
	je	.L3661
	movq	16(%r15), %rax
	movl	$2, %r13d
	movzbl	(%rax), %r11d
	jmp	.L3551
.L3521:
	cmpq	$0, (%r15)
	je	.L3653
	movq	16(%r15), %rax
	movl	$2, %r13d
	movzbl	(%rax), %r11d
	jmp	.L3551
.L3517:
	cmpq	$0, (%r15)
	je	.L3657
	movq	16(%r15), %rax
	movl	$1, %r13d
	movzbl	(%rax), %r11d
	jmp	.L3551
.L3525:
	cmpq	$0, (%r15)
	je	.L3649
	movq	16(%r15), %rax
	xorl	%r13d, %r13d
	movzbl	(%rax), %r11d
	jmp	.L3551
.L3515:
	cmpq	$0, (%r15)
	je	.L3659
	movq	16(%r15), %rax
	movl	$1, %r13d
	movzbl	(%rax), %r11d
	jmp	.L3551
.L3523:
	cmpq	$0, (%r15)
	je	.L3651
	movq	16(%r15), %rax
	xorl	%r13d, %r13d
	movzbl	(%rax), %r11d
	jmp	.L3551
.L3519:
	cmpq	$0, (%r15)
	je	.L3655
	movq	16(%r15), %rax
	movl	$3, %r13d
	movzbl	(%rax), %r11d
	jmp	.L3551
.L3527:
	cmpq	$0, (%r15)
	je	.L3647
	movq	16(%r15), %rax
	movl	$2, %r13d
	movzbl	(%rax), %r11d
	jmp	.L3551
.L3514:
	cmpq	$0, (%r15)
	je	.L3660
	movq	16(%r15), %rax
	movl	$2, %r13d
	movzbl	(%rax), %r11d
	jmp	.L3551
.L3522:
	cmpq	$0, (%r15)
	je	.L3652
	movq	16(%r15), %rax
	movl	$1, %r13d
	movzbl	(%rax), %r11d
	jmp	.L3551
.L3518:
	cmpq	$0, (%r15)
	je	.L3656
	movq	16(%r15), %rax
	xorl	%r13d, %r13d
	movzbl	(%rax), %r11d
	jmp	.L3551
.L3526:
	cmpq	$0, (%r15)
	je	.L3648
	movq	16(%r15), %rax
	movl	$3, %r13d
	movzbl	(%rax), %r11d
	jmp	.L3551
.L3516:
	cmpq	$0, (%r15)
	je	.L3658
	movq	16(%r15), %rax
	xorl	%r13d, %r13d
	movzbl	(%rax), %r11d
	jmp	.L3551
.L3524:
	cmpq	$0, (%r15)
	je	.L3650
	movq	16(%r15), %rax
	movl	$1, %r13d
	movzbl	(%rax), %r11d
	jmp	.L3551
.L3520:
	cmpq	$0, (%r15)
	je	.L3654
	movq	16(%r15), %rax
	movl	$2, %r13d
	movzbl	(%rax), %r11d
	jmp	.L3551
.L3528:
	cmpq	$0, (%r15)
	je	.L3646
	movq	16(%r15), %rax
	movl	$2, %r13d
	movzbl	(%rax), %r11d
	jmp	.L3551
.L3529:
	cmpq	$0, (%r15)
	je	.L3645
	movq	16(%r15), %rax
	movl	$1, %r13d
	movzbl	(%rax), %r11d
	jmp	.L3551
.L3545:
	cmpq	$0, (%r15)
	je	.L3628
	movq	16(%r15), %rax
	movl	$1, %r13d
	movzbl	(%rax), %r11d
	jmp	.L3551
.L3537:
	cmpq	$0, (%r15)
	je	.L3637
	movq	16(%r15), %rax
	xorl	%r13d, %r13d
	movzbl	(%rax), %r11d
	jmp	.L3551
.L3563:
	cmpq	$0, (%r15)
	je	.L3616
	movq	16(%r15), %rax
	movl	$2, %r13d
	movzbl	(%rax), %r11d
	jmp	.L3551
.L3533:
	cmpq	$0, (%r15)
	je	.L3641
	movq	16(%r15), %rax
	movl	$3, %r13d
	movzbl	(%rax), %r11d
	jmp	.L3551
.L3559:
	cmpq	$0, (%r15)
	je	.L3619
	movq	16(%r15), %rax
	movl	$2, %r13d
	movzbl	(%rax), %r11d
	jmp	.L3551
.L3541:
	cmpq	$0, (%r15)
	je	.L3633
	movq	16(%r15), %rax
	movl	$2, %r13d
	movzbl	(%rax), %r11d
	jmp	.L3551
.L3555:
	cmpq	$0, (%r15)
	je	.L3623
	movq	16(%r15), %rax
	xorl	%r13d, %r13d
	movzbl	(%rax), %r11d
	jmp	.L3551
.L3531:
	cmpq	$0, (%r15)
	je	.L3643
	movq	16(%r15), %rax
	movl	$1, %r13d
	movzbl	(%rax), %r11d
	jmp	.L3551
.L3557:
	cmpq	$0, (%r15)
	je	.L3621
	movq	16(%r15), %rax
	xorl	%r13d, %r13d
	movzbl	(%rax), %r11d
	jmp	.L3551
.L3539:
	cmpq	$0, (%r15)
	je	.L3635
	movq	16(%r15), %rax
	xorl	%r13d, %r13d
	movzbl	(%rax), %r11d
	jmp	.L3551
.L3553:
	cmpq	$0, (%r15)
	je	.L3625
	movq	16(%r15), %rax
	movl	$2, %r13d
	movzbl	(%rax), %r11d
	jmp	.L3551
.L3535:
	cmpq	$0, (%r15)
	je	.L3639
	movq	16(%r15), %rax
	movl	$2, %r13d
	movzbl	(%rax), %r11d
	jmp	.L3551
.L3561:
	cmpq	$0, (%r15)
	je	.L3618
	movq	16(%r15), %rax
	movl	$3, %r13d
	movzbl	(%rax), %r11d
	jmp	.L3551
.L3543:
	cmpq	$0, (%r15)
	je	.L3631
	movq	16(%r15), %rax
	movl	$1, %r13d
	movzbl	(%rax), %r11d
	jmp	.L3551
.L3547:
	cmpq	$0, (%r15)
	je	.L3629
	movq	16(%r15), %rax
	movl	$3, %r13d
	movzbl	(%rax), %r11d
	jmp	.L3551
.L3530:
	cmpq	$0, (%r15)
	je	.L3644
	movq	16(%r15), %rax
	xorl	%r13d, %r13d
	movzbl	(%rax), %r11d
	jmp	.L3551
.L3556:
	cmpq	$0, (%r15)
	je	.L3622
	movq	16(%r15), %rax
	movl	$1, %r13d
	movzbl	(%rax), %r11d
	jmp	.L3551
.L3538:
	cmpq	$0, (%r15)
	je	.L3636
	movq	16(%r15), %rax
	movl	$1, %r13d
	movzbl	(%rax), %r11d
	jmp	.L3551
.L3549:
	movl	$2, %r13d
	xorl	%r11d, %r11d
	jmp	.L3551
.L3534:
	cmpq	$0, (%r15)
	je	.L3640
	movq	16(%r15), %rax
	movl	$2, %r13d
	movzbl	(%rax), %r11d
	jmp	.L3551
.L3536:
	cmpq	$0, (%r15)
	je	.L3638
	movq	16(%r15), %rax
	movl	$1, %r13d
	movzbl	(%rax), %r11d
	jmp	.L3551
.L3560:
	movq	16(%r12), %rax
	movq	24(%r12), %rdx
	leaq	2(%rax), %rsi
	cmpq	%rdx, %rsi
	ja	.L3564
	cmpl	%esi, %edx
	je	.L3564
	cmpb	$0, 2(%rax)
	jne	.L3714
.L3566:
	movl	$1, -236(%rbp)
	jmp	.L3495
.L3542:
	cmpq	$0, (%r15)
	je	.L3632
	movq	16(%r15), %rax
	movl	$2, %r13d
	movzbl	(%rax), %r11d
	jmp	.L3551
.L3546:
	cmpq	$0, (%r15)
	je	.L3627
	movq	16(%r15), %rax
	xorl	%r13d, %r13d
	movzbl	(%rax), %r11d
	jmp	.L3551
.L3532:
	cmpq	$0, (%r15)
	je	.L3642
	movq	16(%r15), %rax
	xorl	%r13d, %r13d
	movzbl	(%rax), %r11d
	jmp	.L3551
.L3558:
	cmpq	$0, (%r15)
	je	.L3620
	movq	16(%r15), %rax
	movl	$3, %r13d
	movzbl	(%rax), %r11d
	jmp	.L3551
.L3540:
	cmpq	$0, (%r15)
	je	.L3634
	movq	16(%r15), %rax
	movl	$3, %r13d
	movzbl	(%rax), %r11d
	jmp	.L3551
.L3554:
	cmpq	$0, (%r15)
	je	.L3624
	movq	16(%r15), %rax
	movl	$1, %r13d
	movzbl	(%rax), %r11d
	jmp	.L3551
.L3544:
	cmpq	$0, (%r15)
	je	.L3630
	movq	16(%r15), %rax
	xorl	%r13d, %r13d
	movzbl	(%rax), %r11d
	jmp	.L3551
.L3562:
	cmpq	$0, (%r15)
	je	.L3617
	movq	16(%r15), %rax
	movl	$2, %r13d
	movzbl	(%rax), %r11d
	jmp	.L3551
.L3548:
	cmpq	$0, (%r15)
	je	.L3626
	movq	16(%r15), %rax
	movl	$2, %r13d
	movzbl	(%rax), %r11d
	jmp	.L3551
.L3616:
	movl	$2, %r13d
	xorl	%r11d, %r11d
	jmp	.L3551
.L3619:
	movl	$2, %r13d
	xorl	%r11d, %r11d
	jmp	.L3551
.L3626:
	movl	$2, %r13d
	xorl	%r11d, %r11d
	jmp	.L3551
.L3674:
	movl	$2, %r13d
	xorl	%r11d, %r11d
	jmp	.L3551
.L3671:
	movl	$1, %r13d
	xorl	%r11d, %r11d
	jmp	.L3551
.L3673:
	movl	$1, %r13d
	xorl	%r11d, %r11d
	jmp	.L3551
.L3672:
	xorl	%r13d, %r13d
	xorl	%r11d, %r11d
	jmp	.L3551
.L3670:
	xorl	%r13d, %r13d
	xorl	%r11d, %r11d
	jmp	.L3551
.L3663:
	xorl	%r13d, %r13d
	xorl	%r11d, %r11d
	jmp	.L3551
.L3665:
	xorl	%r13d, %r13d
	xorl	%r11d, %r11d
	jmp	.L3551
.L3664:
	movl	$1, %r13d
	xorl	%r11d, %r11d
	jmp	.L3551
.L3669:
	movl	$3, %r13d
	xorl	%r11d, %r11d
	jmp	.L3551
.L3668:
	movl	$2, %r13d
	xorl	%r11d, %r11d
	jmp	.L3551
.L3667:
	movl	$2, %r13d
	xorl	%r11d, %r11d
	jmp	.L3551
.L3666:
	movl	$1, %r13d
	xorl	%r11d, %r11d
	jmp	.L3551
.L3662:
	movl	$3, %r13d
	xorl	%r11d, %r11d
	jmp	.L3551
.L3647:
	movl	$2, %r13d
	xorl	%r11d, %r11d
	jmp	.L3551
.L3649:
	xorl	%r13d, %r13d
	xorl	%r11d, %r11d
	jmp	.L3551
.L3648:
	movl	$3, %r13d
	xorl	%r11d, %r11d
	jmp	.L3551
.L3653:
	movl	$2, %r13d
	xorl	%r11d, %r11d
	jmp	.L3551
.L3652:
	movl	$1, %r13d
	xorl	%r11d, %r11d
	jmp	.L3551
.L3651:
	xorl	%r13d, %r13d
	xorl	%r11d, %r11d
	jmp	.L3551
.L3650:
	movl	$1, %r13d
	xorl	%r11d, %r11d
	jmp	.L3551
.L3661:
	movl	$2, %r13d
	xorl	%r11d, %r11d
	jmp	.L3551
.L3660:
	movl	$2, %r13d
	xorl	%r11d, %r11d
	jmp	.L3551
.L3659:
	movl	$1, %r13d
	xorl	%r11d, %r11d
	jmp	.L3551
.L3658:
	xorl	%r13d, %r13d
	xorl	%r11d, %r11d
	jmp	.L3551
.L3657:
	movl	$1, %r13d
	xorl	%r11d, %r11d
	jmp	.L3551
.L3656:
	xorl	%r13d, %r13d
	xorl	%r11d, %r11d
	jmp	.L3551
.L3655:
	movl	$3, %r13d
	xorl	%r11d, %r11d
	jmp	.L3551
.L3654:
	movl	$2, %r13d
	xorl	%r11d, %r11d
	jmp	.L3551
.L3625:
	movl	$2, %r13d
	xorl	%r11d, %r11d
	jmp	.L3551
.L3640:
	movl	$2, %r13d
	xorl	%r11d, %r11d
	jmp	.L3551
.L3646:
	movl	$2, %r13d
	xorl	%r11d, %r11d
	jmp	.L3551
.L3629:
	movl	$3, %r13d
	xorl	%r11d, %r11d
	jmp	.L3551
.L3623:
	xorl	%r13d, %r13d
	xorl	%r11d, %r11d
	jmp	.L3551
.L3627:
	xorl	%r13d, %r13d
	xorl	%r11d, %r11d
	jmp	.L3551
.L3628:
	movl	$1, %r13d
	xorl	%r11d, %r11d
	jmp	.L3551
.L3622:
	movl	$1, %r13d
	xorl	%r11d, %r11d
	jmp	.L3551
.L3621:
	xorl	%r13d, %r13d
	xorl	%r11d, %r11d
	jmp	.L3551
.L3620:
	movl	$3, %r13d
	xorl	%r11d, %r11d
	jmp	.L3551
.L3618:
	movl	$3, %r13d
	xorl	%r11d, %r11d
	jmp	.L3551
.L3624:
	movl	$1, %r13d
	xorl	%r11d, %r11d
	jmp	.L3551
.L3645:
	movl	$1, %r13d
	xorl	%r11d, %r11d
	jmp	.L3551
.L3644:
	xorl	%r13d, %r13d
	xorl	%r11d, %r11d
	jmp	.L3551
.L3643:
	movl	$1, %r13d
	xorl	%r11d, %r11d
	jmp	.L3551
.L3642:
	xorl	%r13d, %r13d
	xorl	%r11d, %r11d
	jmp	.L3551
.L3641:
	movl	$3, %r13d
	xorl	%r11d, %r11d
	jmp	.L3551
.L3638:
	movl	$1, %r13d
	xorl	%r11d, %r11d
	jmp	.L3551
.L3639:
	movl	$2, %r13d
	xorl	%r11d, %r11d
	jmp	.L3551
.L3630:
	xorl	%r13d, %r13d
	xorl	%r11d, %r11d
	jmp	.L3551
.L3637:
	xorl	%r13d, %r13d
	xorl	%r11d, %r11d
	jmp	.L3551
.L3636:
	movl	$1, %r13d
	xorl	%r11d, %r11d
	jmp	.L3551
.L3635:
	xorl	%r13d, %r13d
	xorl	%r11d, %r11d
	jmp	.L3551
.L3634:
	movl	$3, %r13d
	xorl	%r11d, %r11d
	jmp	.L3551
.L3633:
	movl	$2, %r13d
	xorl	%r11d, %r11d
	jmp	.L3551
.L3632:
	movl	$2, %r13d
	xorl	%r11d, %r11d
	jmp	.L3551
.L3631:
	movl	$1, %r13d
	xorl	%r11d, %r11d
	jmp	.L3551
.L3617:
	movl	$2, %r13d
	xorl	%r11d, %r11d
	jmp	.L3551
.L3564:
	leaq	.LC572(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	jmp	.L3566
.L3714:
	leaq	.LC573(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movl	$0, -236(%rbp)
	jmp	.L3495
.L3711:
	call	__stack_chk_fail@PLT
.L3590:
	leaq	.LC500(%rip), %rdx
.L3591:
	movq	24(%r12), %rdi
	leaq	.LC482(%rip), %r8
	cmpq	%rdi, %rsi
	jnb	.L3593
	movzbl	(%rsi), %r9d
	movq	%rdx, -296(%rbp)
	movb	%cl, -288(%rbp)
	movl	%r9d, %edi
	movq	%rsi, -280(%rbp)
	movl	%r9d, -264(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	-264(%rbp), %r9d
	movq	-280(%rbp), %rsi
	testb	%al, %al
	movzbl	-288(%rbp), %ecx
	movq	-296(%rbp), %rdx
	je	.L3715
	movq	24(%r12), %rdi
	leaq	1(%rsi), %rax
	leaq	.LC482(%rip), %r8
	cmpq	%rax, %rdi
	jbe	.L3593
	movl	%r9d, %edi
	movzbl	1(%rsi), %r9d
	movq	%rdx, -288(%rbp)
	sall	$8, %edi
	movb	%cl, -280(%rbp)
	orl	%r9d, %edi
	movq	%rsi, -264(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	24(%r12), %rdi
	movq	-288(%rbp), %rdx
	movzbl	-280(%rbp), %ecx
	movq	-264(%rbp), %rsi
	movq	%rax, %r8
	.p2align 4,,10
	.p2align 3
.L3593:
	cmpb	$9, %cl
	ja	.L3595
	leaq	.L3597(%rip), %r10
	movslq	(%r10,%rcx,4), %rax
	addq	%r10, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE18DecodeAtomicOpcodeENS1_10WasmOpcodeE,"aG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE18DecodeAtomicOpcodeENS1_10WasmOpcodeE,comdat
	.align 4
	.align 4
.L3597:
	.long	.L3606-.L3597
	.long	.L3605-.L3597
	.long	.L3683-.L3597
	.long	.L3603-.L3597
	.long	.L3602-.L3597
	.long	.L3601-.L3597
	.long	.L3600-.L3597
	.long	.L3599-.L3597
	.long	.L3598-.L3597
	.long	.L3596-.L3597
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE18DecodeAtomicOpcodeENS1_10WasmOpcodeE,"axG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE18DecodeAtomicOpcodeENS1_10WasmOpcodeE,comdat
.L3680:
	leaq	.LC490(%rip), %rdx
	jmp	.L3591
.L3592:
	leaq	.LC498(%rip), %rdx
	jmp	.L3591
.L3582:
	leaq	.LC496(%rip), %rdx
	jmp	.L3591
.L3584:
	leaq	.LC495(%rip), %rdx
	jmp	.L3591
.L3585:
	leaq	.LC494(%rip), %rdx
	jmp	.L3591
.L3586:
	leaq	.LC493(%rip), %rdx
	jmp	.L3591
.L3587:
	leaq	.LC497(%rip), %rdx
	jmp	.L3591
.L3588:
	leaq	.LC492(%rip), %rdx
	jmp	.L3591
.L3589:
	leaq	.LC491(%rip), %rdx
	jmp	.L3591
.L3683:
	leaq	.LC500(%rip), %r9
	.p2align 4,,10
	.p2align 3
.L3604:
	movq	16(%r12), %rcx
	cmpq	%rdi, %rcx
	jnb	.L3610
	movzbl	(%rcx), %edi
	movq	%r9, -312(%rbp)
	movq	%rdx, -304(%rbp)
	movq	%r8, -296(%rbp)
	movq	%rsi, -288(%rbp)
	movq	%rcx, -280(%rbp)
	movl	%edi, -264(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	-264(%rbp), %edi
	movq	-280(%rbp), %rcx
	testb	%al, %al
	movq	-288(%rbp), %rsi
	movq	-296(%rbp), %r8
	movq	-304(%rbp), %rdx
	movq	-312(%rbp), %r9
	je	.L3716
	leaq	1(%rcx), %rax
	cmpq	%rax, 24(%r12)
	jbe	.L3610
	movzbl	1(%rcx), %eax
	sall	$8, %edi
	movq	%r9, -296(%rbp)
	movq	%rdx, -288(%rbp)
	orl	%eax, %edi
	movq	%r8, -280(%rbp)
	movq	%rsi, -264(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	-296(%rbp), %r9
	movq	-288(%rbp), %rdx
	movq	-280(%rbp), %r8
	movq	-264(%rbp), %rsi
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L3608:
	pushq	%rdx
	xorl	%eax, %eax
	leaq	.LC531(%rip), %rdx
	movq	%r12, %rdi
	pushq	%r8
	movl	%r15d, %r8d
	movq	%rsi, -264(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	popq	%rax
	movq	-264(%rbp), %rsi
	popq	%rdx
	jmp	.L3578
.L3605:
	leaq	.LC490(%rip), %r9
	jmp	.L3604
.L3606:
	leaq	.LC498(%rip), %r9
	jmp	.L3604
.L3598:
	leaq	.LC495(%rip), %r9
	jmp	.L3604
.L3599:
	leaq	.LC494(%rip), %r9
	jmp	.L3604
.L3600:
	leaq	.LC493(%rip), %r9
	jmp	.L3604
.L3603:
	leaq	.LC491(%rip), %r9
	jmp	.L3604
.L3601:
	leaq	.LC497(%rip), %r9
	jmp	.L3604
.L3602:
	leaq	.LC492(%rip), %r9
	jmp	.L3604
.L3596:
	leaq	.LC496(%rip), %r9
	jmp	.L3604
	.p2align 4,,10
	.p2align 3
.L3610:
	leaq	.LC482(%rip), %rcx
	jmp	.L3608
	.p2align 4,,10
	.p2align 3
.L3716:
	movq	%r9, -296(%rbp)
	movq	%rdx, -288(%rbp)
	movq	%r8, -280(%rbp)
	movq	%rsi, -264(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	-264(%rbp), %rsi
	movq	-280(%rbp), %r8
	movq	-288(%rbp), %rdx
	movq	-296(%rbp), %r9
	movq	%rax, %rcx
	jmp	.L3608
	.p2align 4,,10
	.p2align 3
.L3715:
	movl	%r9d, %edi
	movq	%rdx, -288(%rbp)
	movb	%cl, -280(%rbp)
	movq	%rsi, -264(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	24(%r12), %rdi
	movq	-264(%rbp), %rsi
	movzbl	-280(%rbp), %ecx
	movq	-288(%rbp), %rdx
	movq	%rax, %r8
	jmp	.L3593
.L3595:
	leaq	.LC489(%rip), %r9
	jmp	.L3604
.L3581:
	leaq	.LC489(%rip), %rdx
	jmp	.L3591
	.cfi_endproc
.LFE23612:
	.size	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE18DecodeAtomicOpcodeENS1_10WasmOpcodeE, .-_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE18DecodeAtomicOpcodeENS1_10WasmOpcodeE
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16DecodeSimdOpcodeENS1_10WasmOpcodeE.str1.1,"aMS",@progbits,1
.LC575:
	.string	"shuffle"
.LC576:
	.string	"invalid shuffle mask"
.LC577:
	.string	"invalid simd opcode"
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16DecodeSimdOpcodeENS1_10WasmOpcodeE,"axG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16DecodeSimdOpcodeENS1_10WasmOpcodeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16DecodeSimdOpcodeENS1_10WasmOpcodeE
	.type	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16DecodeSimdOpcodeENS1_10WasmOpcodeE, @function
_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16DecodeSimdOpcodeENS1_10WasmOpcodeE:
.LFB23609:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leal	-64768(%rsi), %eax
	cmpl	$23, %eax
	ja	.L3718
	leaq	.L3720(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16DecodeSimdOpcodeENS1_10WasmOpcodeE,"aG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16DecodeSimdOpcodeENS1_10WasmOpcodeE,comdat
	.align 4
	.align 4
.L3720:
	.long	.L3730-.L3720
	.long	.L3729-.L3720
	.long	.L3718-.L3720
	.long	.L3728-.L3720
	.long	.L3718-.L3720
	.long	.L3727-.L3720
	.long	.L3718-.L3720
	.long	.L3726-.L3720
	.long	.L3718-.L3720
	.long	.L3727-.L3720
	.long	.L3718-.L3720
	.long	.L3726-.L3720
	.long	.L3718-.L3720
	.long	.L3727-.L3720
	.long	.L3726-.L3720
	.long	.L3718-.L3720
	.long	.L3725-.L3720
	.long	.L3724-.L3720
	.long	.L3718-.L3720
	.long	.L3723-.L3720
	.long	.L3722-.L3720
	.long	.L3718-.L3720
	.long	.L3721-.L3720
	.long	.L3719-.L3720
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16DecodeSimdOpcodeENS1_10WasmOpcodeE,"axG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16DecodeSimdOpcodeENS1_10WasmOpcodeE,comdat
	.p2align 4,,10
	.p2align 3
.L3718:
	movl	%esi, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes9SignatureENS1_10WasmOpcodeE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L3863
	movq	8(%rax), %r14
	leaq	-184(%rbp), %rcx
	leaq	-56(%rbp), %rax
	movq	%rcx, %xmm0
	movq	%rax, -192(%rbp)
	leaq	-208(%rbp), %rdi
	movq	%rcx, %rax
	punpcklqdq	%xmm0, %xmm0
	movslq	%r14d, %rbx
	movq	%rcx, -256(%rbp)
	movaps	%xmm0, -208(%rbp)
	cmpq	$8, %rbx
	ja	.L3864
.L3799:
	salq	$4, %rbx
	addq	%rax, %rbx
	subl	$1, %r14d
	movq	%rbx, -200(%rbp)
	movslq	%r14d, %rbx
	js	.L3813
	movl	%r14d, %eax
	movq	%r15, %r14
	movl	%eax, %r15d
	jmp	.L3814
	.p2align 4,,10
	.p2align 3
.L3867:
	cmpb	$2, -72(%rsi)
	movq	16(%r12), %r10
	jne	.L3865
.L3804:
	movl	$10, %r13d
.L3808:
	movq	%rbx, %rax
	subl	$1, %r15d
	subq	$1, %rbx
	salq	$4, %rax
	addq	-208(%rbp), %rax
	movq	%r10, (%rax)
	movb	%r13b, 8(%rax)
	cmpl	$-1, %r15d
	je	.L3866
.L3814:
	movq	16(%r14), %rax
	movq	224(%r12), %rsi
	addq	%rbx, %rax
	addq	(%r14), %rax
	movl	-84(%rsi), %edi
	movzbl	(%rax), %edx
	movq	192(%r12), %rax
	movq	%rax, %rcx
	subq	184(%r12), %rcx
	sarq	$4, %rcx
	cmpq	%rcx, %rdi
	jnb	.L3867
	movzbl	-8(%rax), %r13d
	movq	-16(%rax), %r10
	subq	$16, %rax
	movq	%rax, 192(%r12)
	cmpb	%dl, %r13b
	je	.L3808
	cmpb	$6, %dl
	sete	%al
	cmpb	$8, %r13b
	sete	%cl
	testb	%al, %al
	je	.L3830
	testb	%cl, %cl
	je	.L3830
	movl	$8, %r13d
	jmp	.L3808
	.p2align 4,,10
	.p2align 3
.L3727:
	movl	$1, %edx
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15SimdExtractLaneENS1_10WasmOpcodeENS1_9ValueTypeE
	.p2align 4,,10
	.p2align 3
.L3717:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L3868
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3726:
	.cfi_restore_state
	movl	$1, %edx
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15SimdReplaceLaneENS1_10WasmOpcodeENS1_9ValueTypeE
	jmp	.L3717
	.p2align 4,,10
	.p2align 3
.L3723:
	movl	$3, %edx
	movl	$64787, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15SimdExtractLaneENS1_10WasmOpcodeENS1_9ValueTypeE
	jmp	.L3717
	.p2align 4,,10
	.p2align 3
.L3725:
	movl	$2, %edx
	movl	$64784, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15SimdExtractLaneENS1_10WasmOpcodeENS1_9ValueTypeE
	jmp	.L3717
	.p2align 4,,10
	.p2align 3
.L3724:
	movl	$2, %edx
	movl	$64785, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15SimdReplaceLaneENS1_10WasmOpcodeENS1_9ValueTypeE
	jmp	.L3717
	.p2align 4,,10
	.p2align 3
.L3721:
	movl	$4, %edx
	movl	$64790, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15SimdExtractLaneENS1_10WasmOpcodeENS1_9ValueTypeE
	jmp	.L3717
	.p2align 4,,10
	.p2align 3
.L3728:
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	leaq	2(%rbx), %rsi
	cmpq	%rax, %rsi
	ja	.L3734
	cmpl	%esi, %eax
	je	.L3734
	movzbl	2(%rbx), %edx
.L3733:
	leaq	3(%rbx), %rsi
	movb	%dl, -208(%rbp)
	cmpq	%rsi, %rax
	jb	.L3737
	cmpl	%esi, %eax
	je	.L3737
	movzbl	3(%rbx), %edx
.L3736:
	leaq	4(%rbx), %rsi
	movb	%dl, -207(%rbp)
	cmpq	%rsi, %rax
	jb	.L3740
	cmpl	%esi, %eax
	je	.L3740
	movzbl	4(%rbx), %edx
.L3739:
	leaq	5(%rbx), %rsi
	movb	%dl, -206(%rbp)
	cmpq	%rsi, %rax
	jb	.L3743
	cmpl	%esi, %eax
	je	.L3743
	movzbl	5(%rbx), %edx
.L3742:
	leaq	6(%rbx), %rsi
	movb	%dl, -205(%rbp)
	cmpq	%rsi, %rax
	jb	.L3746
	cmpl	%esi, %eax
	je	.L3746
	movzbl	6(%rbx), %edx
.L3745:
	leaq	7(%rbx), %rsi
	movb	%dl, -204(%rbp)
	cmpq	%rsi, %rax
	jb	.L3749
	cmpl	%esi, %eax
	je	.L3749
	movzbl	7(%rbx), %edx
.L3748:
	leaq	8(%rbx), %rsi
	movb	%dl, -203(%rbp)
	cmpq	%rsi, %rax
	jb	.L3752
	cmpl	%esi, %eax
	je	.L3752
	movzbl	8(%rbx), %edx
.L3751:
	leaq	9(%rbx), %rsi
	movb	%dl, -202(%rbp)
	cmpq	%rax, %rsi
	ja	.L3755
	cmpl	%esi, %eax
	je	.L3755
	movzbl	9(%rbx), %edx
.L3754:
	leaq	10(%rbx), %rsi
	movb	%dl, -201(%rbp)
	cmpq	%rax, %rsi
	ja	.L3758
	cmpl	%esi, %eax
	je	.L3758
	movzbl	10(%rbx), %edx
.L3757:
	leaq	11(%rbx), %rsi
	movb	%dl, -200(%rbp)
	cmpq	%rax, %rsi
	ja	.L3761
	cmpl	%esi, %eax
	je	.L3761
	movzbl	11(%rbx), %edx
.L3760:
	leaq	12(%rbx), %rsi
	movb	%dl, -199(%rbp)
	cmpq	%rax, %rsi
	ja	.L3764
	cmpl	%esi, %eax
	je	.L3764
	movzbl	12(%rbx), %edx
.L3763:
	leaq	13(%rbx), %rsi
	movb	%dl, -198(%rbp)
	cmpq	%rax, %rsi
	ja	.L3767
	cmpl	%esi, %eax
	je	.L3767
	movzbl	13(%rbx), %edx
.L3766:
	leaq	14(%rbx), %rsi
	movb	%dl, -197(%rbp)
	cmpq	%rax, %rsi
	ja	.L3770
	cmpl	%esi, %eax
	je	.L3770
	movzbl	14(%rbx), %edx
.L3769:
	leaq	15(%rbx), %rsi
	movb	%dl, -196(%rbp)
	cmpq	%rax, %rsi
	ja	.L3773
	cmpl	%esi, %eax
	je	.L3773
	movzbl	15(%rbx), %edx
.L3772:
	leaq	16(%rbx), %rsi
	movb	%dl, -195(%rbp)
	cmpq	%rax, %rsi
	ja	.L3776
	cmpl	%esi, %eax
	je	.L3776
	movzbl	16(%rbx), %edx
.L3775:
	leaq	17(%rbx), %rsi
	movb	%dl, -194(%rbp)
	cmpq	%rax, %rsi
	ja	.L3777
	cmpl	%esi, %eax
	je	.L3777
	movzbl	17(%rbx), %eax
.L3778:
	movb	%al, -193(%rbp)
	pxor	%xmm0, %xmm0
	pmaxub	-208(%rbp), %xmm0
	movdqa	%xmm0, %xmm1
	psrldq	$8, %xmm1
	pmaxub	%xmm1, %xmm0
	movdqa	%xmm0, %xmm1
	psrldq	$4, %xmm1
	pmaxub	%xmm1, %xmm0
	movdqa	%xmm0, %xmm1
	psrldq	$2, %xmm1
	pmaxub	%xmm1, %xmm0
	movdqa	%xmm0, %xmm1
	psrldq	$1, %xmm1
	pmaxub	%xmm1, %xmm0
	movaps	%xmm0, -240(%rbp)
	movzbl	-240(%rbp), %eax
	cmpb	$32, %al
	ja	.L3779
	movq	192(%r12), %rdx
	movq	224(%r12), %r8
	movq	184(%r12), %r9
	movq	%rdx, %rax
	movl	-84(%r8), %ecx
	subq	%r9, %rax
	sarq	$4, %rax
	cmpq	%rcx, %rax
	jbe	.L3780
	movzbl	-8(%rdx), %r10d
	leaq	-16(%rdx), %rcx
	movq	-16(%rdx), %r13
	movq	%rcx, 192(%r12)
	cmpb	$5, %r10b
	jne	.L3786
.L3829:
	movq	%rcx, %rax
	movl	-84(%r8), %edx
	subq	%r9, %rax
	sarq	$4, %rax
.L3785:
	cmpq	%rdx, %rax
	jbe	.L3783
	movzbl	-8(%rcx), %r8d
	leaq	-16(%rcx), %rdx
	movq	-16(%rcx), %r13
	movq	%rdx, 192(%r12)
	cmpb	$5, %r8b
	jne	.L3869
.L3793:
	leaq	-209(%rbp), %rdx
	leaq	16(%r12), %rsi
	movb	$5, -209(%rbp)
	leaq	176(%r12), %rdi
	call	_ZNSt6vectorIN2v88internal4wasm9ValueBaseENS1_13ZoneAllocatorIS3_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_
.L3782:
	movl	$16, %eax
	jmp	.L3717
	.p2align 4,,10
	.p2align 3
.L3719:
	movl	$4, %edx
	movl	$64791, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15SimdReplaceLaneENS1_10WasmOpcodeENS1_9ValueTypeE
	jmp	.L3717
	.p2align 4,,10
	.p2align 3
.L3730:
	movl	$1, %edx
	movl	$14, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE13DecodeLoadMemENS1_8LoadTypeEi
	jmp	.L3717
	.p2align 4,,10
	.p2align 3
.L3729:
	movl	$1, %edx
	movl	$9, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE14DecodeStoreMemENS1_9StoreTypeEi
	jmp	.L3717
	.p2align 4,,10
	.p2align 3
.L3722:
	movl	$3, %edx
	movl	$64788, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15SimdReplaceLaneENS1_10WasmOpcodeENS1_9ValueTypeE
	jmp	.L3717
.L3830:
	leal	-7(%r13), %esi
	andl	$253, %esi
	jne	.L3831
	testb	%al, %al
	jne	.L3808
.L3831:
	leal	-7(%rdx), %eax
	movzbl	%r13b, %edi
	testb	$-3, %al
	sete	%al
	testb	%cl, %al
	jne	.L3808
	cmpb	$10, %dl
	setne	%cl
	cmpb	$10, %r13b
	setne	%al
	testb	%al, %cl
	je	.L3808
	movq	%r10, -280(%rbp)
	movb	%dl, -272(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	-280(%rbp), %r10
	leaq	24(%r12), %rdi
	cmpq	24(%r12), %r10
	movq	%rax, -264(%rbp)
	leaq	.LC482(%rip), %rax
	movzbl	-272(%rbp), %edx
	movq	%rdi, -288(%rbp)
	movq	%rax, -248(%rbp)
	jnb	.L3811
	movq	%r10, %rsi
	movb	%dl, -280(%rbp)
	movq	%r10, -272(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movzbl	-280(%rbp), %edx
	movq	-272(%rbp), %r10
	movq	%rax, -248(%rbp)
.L3811:
	movzbl	%dl, %edi
	movq	%r10, -272(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	16(%r12), %rsi
	cmpq	24(%r12), %rsi
	leaq	.LC482(%rip), %rcx
	movq	-272(%rbp), %r10
	movq	%rax, %r9
	jnb	.L3812
	movq	-288(%rbp), %rdi
	movq	%r10, -280(%rbp)
	movq	%rax, -272(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-280(%rbp), %r10
	movq	-272(%rbp), %r9
	movq	%rax, %rcx
.L3812:
	pushq	-264(%rbp)
	movq	%r10, %rsi
	xorl	%eax, %eax
	movl	%r15d, %r8d
	pushq	-248(%rbp)
	leaq	.LC531(%rip), %rdx
	movq	%r12, %rdi
	movq	%r10, -248(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	popq	%rax
	movq	-248(%rbp), %r10
	popq	%rdx
	jmp	.L3808
	.p2align 4,,10
	.p2align 3
.L3865:
	leaq	.LC482(%rip), %rcx
	cmpq	%r10, 24(%r12)
	ja	.L3870
.L3805:
	movq	%r10, %rsi
	leaq	.LC530(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	16(%r12), %r10
	jmp	.L3804
	.p2align 4,,10
	.p2align 3
.L3866:
	movq	%r14, %r15
.L3813:
	cmpq	$0, (%r15)
	jne	.L3871
.L3802:
	movq	-208(%rbp), %rdi
	cmpq	-256(%rbp), %rdi
	je	.L3815
	call	free@PLT
.L3815:
	xorl	%eax, %eax
	jmp	.L3717
	.p2align 4,,10
	.p2align 3
.L3870:
	movzbl	(%r10), %r13d
	movq	%r10, -248(%rbp)
	movl	%r13d, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movq	-248(%rbp), %r10
	testb	%al, %al
	je	.L3872
	leaq	1(%r10), %rax
	cmpq	%rax, 24(%r12)
	ja	.L3807
	movq	16(%r12), %r10
	leaq	.LC482(%rip), %rcx
	jmp	.L3805
	.p2align 4,,10
	.p2align 3
.L3780:
	cmpb	$2, -72(%r8)
	jne	.L3873
.L3783:
	cmpb	$2, -72(%r8)
	je	.L3793
	movq	16(%r12), %rsi
	leaq	.LC482(%rip), %rcx
	cmpq	24(%r12), %rsi
	jnb	.L3794
	leaq	24(%r12), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %rsi
	movq	%rax, %rcx
.L3794:
	leaq	.LC530(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L3793
	.p2align 4,,10
	.p2align 3
.L3871:
	movq	16(%r15), %rax
	leaq	-209(%rbp), %rdx
	leaq	16(%r12), %rsi
	leaq	176(%r12), %rdi
	movzbl	(%rax), %eax
	movb	%al, -209(%rbp)
	call	_ZNSt6vectorIN2v88internal4wasm9ValueBaseENS1_13ZoneAllocatorIS3_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_
	jmp	.L3802
	.p2align 4,,10
	.p2align 3
.L3864:
	movq	%rbx, %rsi
	call	_ZN2v84base11SmallVectorINS_8internal4wasm9ValueBaseELm8EE4GrowEm
	movq	-208(%rbp), %rax
	jmp	.L3799
	.p2align 4,,10
	.p2align 3
.L3863:
	leaq	.LC577(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc
	xorl	%eax, %eax
	jmp	.L3717
	.p2align 4,,10
	.p2align 3
.L3779:
	movq	16(%r12), %rax
	leaq	.LC576(%rip), %rdx
	movq	%r12, %rdi
	leaq	2(%rax), %rsi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	jmp	.L3782
	.p2align 4,,10
	.p2align 3
.L3777:
	leaq	.LC575(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	xorl	%eax, %eax
	jmp	.L3778
	.p2align 4,,10
	.p2align 3
.L3776:
	leaq	.LC575(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	24(%r12), %rax
	xorl	%edx, %edx
	jmp	.L3775
	.p2align 4,,10
	.p2align 3
.L3773:
	leaq	.LC575(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	24(%r12), %rax
	xorl	%edx, %edx
	jmp	.L3772
	.p2align 4,,10
	.p2align 3
.L3770:
	leaq	.LC575(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	24(%r12), %rax
	xorl	%edx, %edx
	jmp	.L3769
	.p2align 4,,10
	.p2align 3
.L3767:
	leaq	.LC575(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	24(%r12), %rax
	xorl	%edx, %edx
	jmp	.L3766
	.p2align 4,,10
	.p2align 3
.L3740:
	leaq	.LC575(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	24(%r12), %rax
	xorl	%edx, %edx
	jmp	.L3739
	.p2align 4,,10
	.p2align 3
.L3737:
	leaq	.LC575(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	24(%r12), %rax
	xorl	%edx, %edx
	jmp	.L3736
	.p2align 4,,10
	.p2align 3
.L3734:
	leaq	.LC575(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	24(%r12), %rax
	xorl	%edx, %edx
	jmp	.L3733
	.p2align 4,,10
	.p2align 3
.L3764:
	leaq	.LC575(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	24(%r12), %rax
	xorl	%edx, %edx
	jmp	.L3763
	.p2align 4,,10
	.p2align 3
.L3761:
	leaq	.LC575(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	24(%r12), %rax
	xorl	%edx, %edx
	jmp	.L3760
	.p2align 4,,10
	.p2align 3
.L3758:
	leaq	.LC575(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	24(%r12), %rax
	xorl	%edx, %edx
	jmp	.L3757
	.p2align 4,,10
	.p2align 3
.L3755:
	leaq	.LC575(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	24(%r12), %rax
	xorl	%edx, %edx
	jmp	.L3754
	.p2align 4,,10
	.p2align 3
.L3752:
	leaq	.LC575(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	24(%r12), %rax
	xorl	%edx, %edx
	jmp	.L3751
	.p2align 4,,10
	.p2align 3
.L3749:
	leaq	.LC575(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	24(%r12), %rax
	xorl	%edx, %edx
	jmp	.L3748
	.p2align 4,,10
	.p2align 3
.L3746:
	leaq	.LC575(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	24(%r12), %rax
	xorl	%edx, %edx
	jmp	.L3745
	.p2align 4,,10
	.p2align 3
.L3743:
	leaq	.LC575(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	24(%r12), %rax
	xorl	%edx, %edx
	jmp	.L3742
	.p2align 4,,10
	.p2align 3
.L3872:
	movl	%r13d, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	16(%r12), %r10
	movq	%rax, %rcx
	jmp	.L3805
	.p2align 4,,10
	.p2align 3
.L3807:
	movl	%r13d, %edi
	movzbl	1(%r10), %r13d
	sall	$8, %edi
	orl	%r13d, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	16(%r12), %r10
	movq	%rax, %rcx
	jmp	.L3805
.L3873:
	movq	16(%r12), %rsi
	leaq	.LC482(%rip), %rcx
	cmpq	24(%r12), %rsi
	jnb	.L3784
	leaq	24(%r12), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %rsi
	movq	%rax, %rcx
.L3784:
	leaq	.LC530(%rip), %rdx
	xorl	%eax, %eax
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	192(%r12), %rcx
	movq	224(%r12), %r8
	movq	%rcx, %rax
	subq	184(%r12), %rax
	movl	-84(%r8), %edx
	sarq	$4, %rax
	jmp	.L3785
.L3786:
	movzbl	%r10b, %edi
	movl	$5, %esi
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$1, %al
	je	.L3829
	cmpb	$10, %r10b
	je	.L3829
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	24(%r12), %r15
	leaq	.LC482(%rip), %r14
	movq	%rax, %rbx
	movq	24(%r12), %rax
	cmpq	%rax, %r13
	jnb	.L3789
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	%rax, %r14
	movq	24(%r12), %rax
.L3789:
	movq	16(%r12), %rsi
	leaq	.LC482(%rip), %rcx
	cmpq	%rax, %rsi
	jnb	.L3790
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	%rax, %rcx
.L3790:
	pushq	%rbx
	leaq	.LC497(%rip), %r9
	movl	$1, %r8d
	xorl	%eax, %eax
	pushq	%r14
	leaq	.LC531(%rip), %rdx
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	192(%r12), %rcx
	movq	224(%r12), %r8
	popq	%rdi
	popq	%r9
	movq	%rcx, %rax
	subq	184(%r12), %rax
	movl	-84(%r8), %edx
	sarq	$4, %rax
	jmp	.L3785
.L3869:
	movzbl	%r8b, %edi
	movl	$5, %esi
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$10, %r8b
	je	.L3793
	cmpb	$1, %al
	je	.L3793
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	24(%r12), %r15
	leaq	.LC482(%rip), %r14
	movq	%rax, %rbx
	movq	24(%r12), %rax
	cmpq	%rax, %r13
	jnb	.L3796
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	%rax, %r14
	movq	24(%r12), %rax
.L3796:
	movq	16(%r12), %rsi
	leaq	.LC482(%rip), %rcx
	cmpq	%rax, %rsi
	jnb	.L3797
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	%rax, %rcx
.L3797:
	pushq	%rbx
	movq	%r13, %rsi
	leaq	.LC497(%rip), %r9
	xorl	%r8d, %r8d
	pushq	%r14
	leaq	.LC531(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	popq	%rcx
	popq	%rsi
	jmp	.L3793
.L3868:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23609:
	.size	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16DecodeSimdOpcodeENS1_10WasmOpcodeE, .-_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16DecodeSimdOpcodeENS1_10WasmOpcodeE
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19DecodeNumericOpcodeENS1_10WasmOpcodeE.str1.8,"aMS",@progbits,1
	.align 8
.LC578:
	.string	"invalid data segment index: %u"
	.align 8
.LC579:
	.string	"invalid element segment index: %u"
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19DecodeNumericOpcodeENS1_10WasmOpcodeE.str1.1,"aMS",@progbits,1
.LC580:
	.string	"invalid table index: %u"
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19DecodeNumericOpcodeENS1_10WasmOpcodeE,"axG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19DecodeNumericOpcodeENS1_10WasmOpcodeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19DecodeNumericOpcodeENS1_10WasmOpcodeE
	.type	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19DecodeNumericOpcodeENS1_10WasmOpcodeE, @function
_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19DecodeNumericOpcodeENS1_10WasmOpcodeE:
.LFB23598:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	%esi, %edi
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm11WasmOpcodes9SignatureENS1_10WasmOpcodeE@PLT
	testq	%rax, %rax
	je	.L3875
	movq	%rax, %r13
	leal	-64512(%r14), %eax
	cmpl	$17, %eax
	ja	.L3875
	leaq	.L3878(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19DecodeNumericOpcodeENS1_10WasmOpcodeE,"aG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19DecodeNumericOpcodeENS1_10WasmOpcodeE,comdat
	.align 4
	.align 4
.L3878:
	.long	.L3888-.L3878
	.long	.L3888-.L3878
	.long	.L3888-.L3878
	.long	.L3888-.L3878
	.long	.L3888-.L3878
	.long	.L3888-.L3878
	.long	.L3888-.L3878
	.long	.L3888-.L3878
	.long	.L3887-.L3878
	.long	.L3886-.L3878
	.long	.L3885-.L3878
	.long	.L3884-.L3878
	.long	.L3883-.L3878
	.long	.L3882-.L3878
	.long	.L3881-.L3878
	.long	.L3880-.L3878
	.long	.L3879-.L3878
	.long	.L3877-.L3878
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19DecodeNumericOpcodeENS1_10WasmOpcodeE,"axG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19DecodeNumericOpcodeENS1_10WasmOpcodeE,comdat
.L3875:
	leaq	.LC503(%rip), %rsi
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	call	_ZN2v88internal4wasm7Decoder5errorEPKc
.L3874:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4247
	leaq	-40(%rbp), %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3879:
	.cfi_restore_state
	movabsq	$4294967296, %rax
	leaq	.LC539(%rip), %rcx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movq	%rax, -216(%rbp)
	movq	16(%r12), %rax
	leaq	-212(%rbp), %rdx
	leaq	2(%rax), %rsi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	movl	%eax, -216(%rbp)
	movl	%eax, %ecx
	movq	80(%r12), %rax
	testq	%rax, %rax
	je	.L4017
	movq	192(%rax), %rdx
	subq	184(%rax), %rdx
	movl	%ecx, %esi
	sarq	$4, %rdx
	movl	-212(%rbp), %r14d
	cmpq	%rdx, %rsi
	jnb	.L4017
.L4222:
	leaq	-220(%rbp), %rdx
	leaq	16(%r12), %rsi
	movb	$1, -220(%rbp)
	leaq	176(%r12), %rdi
	call	_ZNSt6vectorIN2v88internal4wasm9ValueBaseENS1_13ZoneAllocatorIS3_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_
	jmp	.L3874
.L3877:
	movabsq	$4294967296, %rax
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movq	%rax, -216(%rbp)
	movq	16(%r12), %rax
	leaq	.LC539(%rip), %rcx
	leaq	-212(%rbp), %rdx
	leaq	2(%rax), %rsi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	movq	16(%r12), %r15
	movl	%eax, -216(%rbp)
	movl	%eax, %ecx
	movq	80(%r12), %rax
	movq	%r15, %r8
	testq	%rax, %rax
	je	.L4020
	movq	192(%rax), %rdx
	subq	184(%rax), %rdx
	movl	%ecx, %esi
	sarq	$4, %rdx
	cmpq	%rdx, %rsi
	jnb	.L4020
	movq	16(%r13), %rdx
	movq	0(%r13), %rax
	movq	224(%r12), %rcx
	movl	-212(%rbp), %r14d
	movzbl	2(%rdx,%rax), %ebx
	movq	192(%r12), %rax
	movl	-84(%rcx), %esi
	movq	%rax, %rdx
	subq	184(%r12), %rdx
	sarq	$4, %rdx
	cmpq	%rdx, %rsi
	jb	.L4023
	cmpb	$2, -72(%rcx)
	jne	.L4248
.L4024:
	movl	$10, %edi
	movl	$10, %ecx
.L4026:
	movq	%r8, -232(%rbp)
	cmpb	%bl, %cl
	je	.L4027
	movzbl	%bl, %r9d
	movl	%r9d, %esi
	movl	%r9d, -240(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$10, %bl
	setne	%sil
	cmpb	$10, %cl
	setne	%dl
	testb	%dl, %sil
	je	.L4027
	cmpb	$1, %al
	movq	-232(%rbp), %r8
	jne	.L4249
.L4027:
	movq	80(%r12), %rdx
	movl	-216(%rbp), %eax
	movq	224(%r12), %rcx
	salq	$4, %rax
	addq	184(%rdx), %rax
	movq	192(%r12), %rdx
	movzbl	(%rax), %r9d
	movl	-84(%rcx), %esi
	movq	%rdx, %rax
	subq	184(%r12), %rax
	sarq	$4, %rax
	cmpq	%rax, %rsi
	jnb	.L4242
	movzbl	-8(%rdx), %edi
	leaq	-16(%rdx), %rcx
	movq	-16(%rdx), %r15
	movq	%rcx, 192(%r12)
	movq	%rcx, %rdx
	cmpb	%dil, %r9b
	je	.L4033
	movzbl	%r9b, %ebx
	movl	%ebx, %esi
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$10, %r9b
	movq	%rcx, %rdx
	setne	%r9b
	cmpb	$10, %dil
	setne	%sil
	testb	%sil, %r9b
	jne	.L4246
	jmp	.L4033
.L3888:
	movl	%r14d, %esi
	movq	%r13, %rdx
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeEPNS0_9SignatureINS1_9ValueTypeEEE
	jmp	.L3874
.L3887:
	movq	16(%r12), %rbx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	-216(%rbp), %rdx
	leaq	.LC549(%rip), %rcx
	movl	$0, -216(%rbp)
	leaq	2(%rbx), %rsi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIiLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	movl	%eax, %r15d
	movl	-216(%rbp), %eax
	leaq	1(%rbx,%rax), %rdx
	movq	%rax, %r14
	movq	24(%r12), %rax
	leaq	1(%rdx), %rsi
	cmpq	%rax, %rsi
	ja	.L3890
	cmpl	%esi, %eax
	je	.L3890
	movzbl	1(%rdx), %ecx
	testl	%ecx, %ecx
	jne	.L4250
.L3892:
	movq	80(%r12), %rax
	movq	16(%r12), %r10
	testq	%rax, %rax
	je	.L3893
	cmpl	%r15d, 76(%rax)
	jbe	.L3893
	addl	$1, %r14d
	cmpb	$0, 18(%rax)
	leaq	-1(%r10,%r14), %rsi
	je	.L4218
	movq	16(%r13), %rdx
	movq	0(%r13), %rax
	movq	224(%r12), %rcx
	movzbl	2(%rdx,%rax), %ebx
	movq	192(%r12), %rax
	movl	-84(%rcx), %esi
	movq	%rax, %rdx
	subq	184(%r12), %rdx
	sarq	$4, %rdx
	cmpq	%rdx, %rsi
	jb	.L3897
	cmpb	$2, -72(%rcx)
	jne	.L4251
.L3898:
	movl	$10, %edi
	movl	$10, %ecx
.L3900:
	movq	%r10, -232(%rbp)
	cmpb	%bl, %cl
	je	.L3901
	movzbl	%bl, %r15d
	movl	%r15d, %esi
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$10, %cl
	setne	%cl
	cmpb	$10, %bl
	setne	%dl
	testb	%dl, %cl
	je	.L3901
	cmpb	$1, %al
	movq	-232(%rbp), %r10
	jne	.L4252
.L3901:
	movq	16(%r13), %rdx
	movq	0(%r13), %rax
	movq	224(%r12), %rcx
	movzbl	1(%rdx,%rax), %r9d
	movq	192(%r12), %rdx
	movl	-84(%rcx), %esi
	movq	%rdx, %rax
	subq	184(%r12), %rax
	sarq	$4, %rax
	cmpq	%rax, %rsi
	jnb	.L4242
	movzbl	-8(%rdx), %edi
	leaq	-16(%rdx), %rcx
	movq	-16(%rdx), %r15
	movq	%rcx, 192(%r12)
	movq	%rcx, %rdx
	cmpb	%r9b, %dil
	je	.L4033
	movzbl	%r9b, %ebx
	movl	%ebx, %esi
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$10, %dil
	movq	%rcx, %rdx
	setne	%r10b
	cmpb	$10, %r9b
	setne	%sil
	testb	%sil, %r10b
	je	.L4033
.L4246:
	cmpb	$1, %al
	je	.L4033
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	24(%r12), %rdi
	movq	%rax, -240(%rbp)
	leaq	.LC482(%rip), %rax
	movq	%rdi, -248(%rbp)
	movq	%rax, -232(%rbp)
	cmpq	24(%r12), %r15
	jnb	.L4034
	movq	%r15, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	%rax, -232(%rbp)
.L4034:
	movl	%ebx, %edi
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	16(%r12), %rsi
	leaq	.LC482(%rip), %rcx
	movq	%rax, %r9
	cmpq	24(%r12), %rsi
	jnb	.L4035
	movq	-248(%rbp), %rdi
	movq	%rax, -256(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-256(%rbp), %r9
	movq	%rax, %rcx
.L4035:
	pushq	-240(%rbp)
	movq	%r15, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	pushq	-232(%rbp)
	leaq	.LC531(%rip), %rdx
	movl	$1, %r8d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	192(%r12), %rdx
	popq	%rcx
	popq	%rsi
	.p2align 4,,10
	.p2align 3
.L4033:
	movq	224(%r12), %rcx
	movq	%rdx, %rax
	subq	184(%r12), %rax
	sarq	$4, %rax
	movl	-84(%rcx), %esi
	cmpq	%rax, %rsi
	jb	.L4036
	cmpb	$2, -72(%rcx)
	je	.L3874
	movq	16(%r12), %rsi
	leaq	.LC482(%rip), %rcx
	cmpq	24(%r12), %rsi
	jnb	.L4038
	leaq	24(%r12), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %rsi
	movq	%rax, %rcx
.L4038:
	leaq	.LC530(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L3874
.L3886:
	movq	16(%r12), %rax
	leaq	.LC549(%rip), %rcx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	-212(%rbp), %rdx
	leaq	2(%rax), %rsi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIiLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	movl	%eax, -216(%rbp)
	movl	%eax, %ecx
	movq	80(%r12), %rax
	testq	%rax, %rax
	je	.L3916
	movl	-212(%rbp), %r14d
	cmpl	76(%rax), %ecx
	jb	.L3874
.L3916:
	movq	16(%r12), %rax
	leaq	2(%rax), %rsi
	jmp	.L4217
.L3883:
	movq	16(%r12), %rbx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	-220(%rbp), %rdx
	leaq	.LC542(%rip), %rcx
	movl	$0, -220(%rbp)
	leaq	2(%rbx), %rsi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIiLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	xorl	%r8d, %r8d
	leaq	-212(%rbp), %rdx
	movq	%r12, %rdi
	movabsq	$4294967296, %rcx
	movl	%eax, %r15d
	movl	-220(%rbp), %eax
	movq	%rcx, -216(%rbp)
	leaq	.LC539(%rip), %rcx
	leaq	2(%rbx,%rax), %rsi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	movq	80(%r12), %rsi
	movl	-212(%rbp), %r8d
	movl	%eax, %ecx
	movl	-220(%rbp), %r14d
	movq	16(%r12), %rax
	testq	%rsi, %rsi
	je	.L3973
	movq	288(%rsi), %rdx
	subq	280(%rsi), %rdx
	movl	%r15d, %edi
	movabsq	$7905747460161236407, %r9
	sarq	$3, %rdx
	imulq	%r9, %rdx
	cmpq	%rdx, %rdi
	jb	.L3974
.L3973:
	leaq	2(%rax), %rsi
	movl	%r15d, %ecx
.L4219:
	leaq	.LC579(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L3874
.L3882:
	movq	16(%r12), %rax
	leaq	.LC542(%rip), %rcx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	-212(%rbp), %rdx
	leaq	2(%rax), %rsi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIiLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	movl	%eax, -216(%rbp)
	movl	%eax, %ecx
	movq	80(%r12), %rax
	testq	%rax, %rax
	je	.L3986
	movq	288(%rax), %rdx
	subq	280(%rax), %rdx
	movl	%ecx, %esi
	movabsq	$7905747460161236407, %rax
	sarq	$3, %rdx
	movl	-212(%rbp), %r14d
	imulq	%rax, %rdx
	cmpq	%rdx, %rsi
	jb	.L3874
.L3986:
	movq	16(%r12), %rax
	leaq	2(%rax), %rsi
	jmp	.L4219
.L3881:
	movq	16(%r12), %r15
	leaq	-212(%rbp), %rbx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movabsq	$4294967296, %r10
	leaq	.LC539(%rip), %rcx
	movq	%rbx, %rdx
	leaq	2(%r15), %rsi
	movq	%r10, -216(%rbp)
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	xorl	%r8d, %r8d
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movl	%eax, -232(%rbp)
	movl	-212(%rbp), %eax
	movabsq	$4294967296, %r10
	leaq	.LC539(%rip), %rcx
	movq	%r10, -216(%rbp)
	leaq	2(%r15,%rax), %rsi
	movq	%rax, %r14
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	movq	16(%r12), %rsi
	addl	-212(%rbp), %r14d
	movl	%eax, %ecx
	movq	80(%r12), %rax
	testq	%rax, %rax
	je	.L3989
	movq	192(%rax), %rdx
	subq	184(%rax), %rdx
	movl	%ecx, %eax
	sarq	$4, %rdx
	cmpq	%rdx, %rax
	jnb	.L3989
	movl	-232(%rbp), %eax
	cmpq	%rax, %rdx
	jbe	.L4253
	movq	8(%r13), %r15
	leaq	-184(%rbp), %rax
	leaq	-208(%rbp), %rdi
	movq	%rax, %xmm0
	movq	%rax, -232(%rbp)
	leaq	-56(%rbp), %rax
	punpcklqdq	%xmm0, %xmm0
	movslq	%r15d, %rbx
	movq	%rax, -192(%rbp)
	movaps	%xmm0, -208(%rbp)
	cmpq	$8, %rbx
	ja	.L4254
	movq	-232(%rbp), %rax
	salq	$4, %rbx
	addq	%rbx, %rax
	movq	%rax, -200(%rbp)
	subl	$1, %r15d
	js	.L3874
.L4043:
	movl	%r14d, -260(%rbp)
	movslq	%r15d, %rbx
	movl	%r15d, %r14d
	movq	%r13, -240(%rbp)
	jmp	.L4001
	.p2align 4,,10
	.p2align 3
.L4256:
	cmpb	$2, -72(%rcx)
	movq	16(%r12), %r13
	jne	.L4255
.L3996:
	movl	$10, %r15d
.L3998:
	movq	%rbx, %rax
	subl	$1, %r14d
	subq	$1, %rbx
	salq	$4, %rax
	addq	-208(%rbp), %rax
	movq	%r13, (%rax)
	movb	%r15b, 8(%rax)
	cmpl	$-1, %r14d
	je	.L4227
.L4001:
	movq	224(%r12), %rcx
	movq	192(%r12), %rax
	movl	-84(%rcx), %esi
	movq	%rax, %rdx
	subq	184(%r12), %rdx
	sarq	$4, %rdx
	cmpq	%rdx, %rsi
	jnb	.L4256
	movq	-240(%rbp), %rdi
	movzbl	-8(%rax), %r15d
	subq	$16, %rax
	movq	(%rax), %r13
	movq	16(%rdi), %rdx
	addq	%rbx, %rdx
	addq	(%rdi), %rdx
	movzbl	(%rdx), %ecx
	movq	%rax, 192(%r12)
	cmpb	%cl, %r15b
	je	.L3998
	movzbl	%cl, %r9d
	movzbl	%r15b, %edi
	movl	%r9d, %esi
	movl	%r9d, -248(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$10, %cl
	setne	%cl
	cmpb	$10, %r15b
	setne	%dl
	testb	%dl, %cl
	je	.L3998
	cmpb	$1, %al
	je	.L3998
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	24(%r12), %rdi
	cmpq	24(%r12), %r13
	movl	-248(%rbp), %r9d
	movq	%rax, -272(%rbp)
	leaq	.LC482(%rip), %rax
	movq	%rdi, -280(%rbp)
	movq	%rax, -256(%rbp)
	jnb	.L3999
	movq	%r13, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movl	-248(%rbp), %r9d
	movq	%rax, -256(%rbp)
.L3999:
	movl	%r9d, %edi
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	16(%r12), %rsi
	leaq	.LC482(%rip), %rcx
	movq	%rax, %r9
	cmpq	24(%r12), %rsi
	jnb	.L4000
	movq	-280(%rbp), %rdi
	movq	%rax, -248(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-248(%rbp), %r9
	movq	%rax, %rcx
.L4000:
	pushq	-272(%rbp)
	xorl	%eax, %eax
	movl	%r14d, %r8d
	movq	%r13, %rsi
	pushq	-256(%rbp)
	leaq	.LC531(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	popq	%rax
	popq	%rdx
	jmp	.L3998
.L3880:
	movabsq	$4294967296, %rax
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movq	%rax, -216(%rbp)
	movq	16(%r12), %rax
	leaq	.LC539(%rip), %rcx
	leaq	-212(%rbp), %rdx
	leaq	2(%rax), %rsi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	movq	16(%r12), %rsi
	movl	%eax, -216(%rbp)
	movl	%eax, %ecx
	movq	80(%r12), %rax
	movq	%rsi, %r8
	testq	%rax, %rax
	je	.L4020
	movq	192(%rax), %rdx
	subq	184(%rax), %rdx
	movl	%ecx, %edi
	sarq	$4, %rdx
	cmpq	%rdx, %rdi
	jnb	.L4020
	movq	16(%r13), %rdx
	movq	0(%r13), %rax
	movq	224(%r12), %rcx
	movl	-212(%rbp), %r14d
	movzbl	1(%rdx,%rax), %r10d
	movq	192(%r12), %rdx
	movl	-84(%rcx), %edi
	movq	%rdx, %rax
	subq	184(%r12), %rax
	sarq	$4, %rax
	cmpq	%rax, %rdi
	jb	.L4005
	cmpb	$2, -72(%rcx)
	jne	.L4257
.L4008:
	movq	224(%r12), %rcx
	movq	%rdx, %rax
	subq	184(%r12), %rax
	sarq	$4, %rax
	movl	-84(%rcx), %esi
	cmpq	%rax, %rsi
	jb	.L4011
	cmpb	$2, -72(%rcx)
	je	.L4222
	movq	16(%r12), %rsi
	leaq	.LC482(%rip), %rcx
	cmpq	24(%r12), %rsi
	jnb	.L4013
	leaq	24(%r12), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %rsi
	movq	%rax, %rcx
.L4013:
	leaq	.LC530(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L4222
.L3885:
	movq	16(%r12), %rbx
	movq	24(%r12), %rax
	leaq	2(%rbx), %rsi
	cmpq	%rax, %rsi
	ja	.L3919
	cmpl	%esi, %eax
	je	.L3919
	movzbl	2(%rbx), %ecx
	testl	%ecx, %ecx
	jne	.L4258
.L3921:
	leaq	3(%rbx), %rsi
	cmpq	%rax, %rsi
	ja	.L3922
	cmpl	%esi, %eax
	je	.L3922
	movzbl	3(%rbx), %ecx
	testl	%ecx, %ecx
	jne	.L4259
.L3924:
	movq	80(%r12), %rax
	movq	16(%r12), %r15
	testq	%rax, %rax
	je	.L3925
	cmpb	$0, 18(%rax)
	je	.L3925
	movq	16(%r13), %rdx
	movq	0(%r13), %rax
	movq	224(%r12), %rcx
	movzbl	2(%rdx,%rax), %ebx
	movq	192(%r12), %rax
	movl	-84(%rcx), %esi
	movq	%rax, %rdx
	subq	184(%r12), %rdx
	sarq	$4, %rdx
	cmpq	%rdx, %rsi
	jb	.L4260
	cmpb	$2, -72(%rcx)
	jne	.L4261
.L3929:
	movl	$10, %edi
	movl	$10, %ecx
.L3931:
	cmpb	%bl, %cl
	je	.L3932
	movzbl	%bl, %r14d
	movl	%r14d, %esi
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$10, %cl
	setne	%cl
	cmpb	$10, %bl
	setne	%dl
	testb	%dl, %cl
	je	.L3932
	cmpb	$1, %al
	jne	.L4262
.L3932:
	movq	16(%r13), %rdx
	movq	0(%r13), %rax
	movq	224(%r12), %rcx
	movzbl	1(%rdx,%rax), %r8d
	movq	192(%r12), %rdx
	movl	-84(%rcx), %esi
	movq	%rdx, %rax
	subq	184(%r12), %rax
	sarq	$4, %rax
	cmpq	%rax, %rsi
	jb	.L3935
	cmpb	$2, -72(%rcx)
	movq	16(%r12), %rsi
	jne	.L4263
.L3938:
	movq	224(%r12), %rcx
	movq	%rdx, %rax
	subq	184(%r12), %rax
	sarq	$4, %rax
	movl	-84(%rcx), %esi
	cmpq	%rax, %rsi
	jb	.L3941
	cmpb	$2, -72(%rcx)
	jne	.L4264
.L3944:
	movl	$2, %r14d
	jmp	.L3874
.L3884:
	movq	16(%r12), %r8
	movq	24(%r12), %rax
	leaq	2(%r8), %rsi
	movq	%r8, %r15
	cmpq	%rax, %rsi
	ja	.L3947
	cmpl	%esi, %eax
	je	.L3947
	movzbl	2(%r8), %ecx
	testl	%ecx, %ecx
	jne	.L4265
.L3949:
	movq	80(%r12), %rax
	testq	%rax, %rax
	je	.L3950
	cmpb	$0, 18(%rax)
	je	.L3950
	movq	16(%r13), %rdx
	movq	0(%r13), %rax
	movq	224(%r12), %rcx
	movzbl	2(%rdx,%rax), %ebx
	movq	192(%r12), %rax
	movl	-84(%rcx), %esi
	movq	%rax, %rdx
	subq	184(%r12), %rdx
	sarq	$4, %rdx
	cmpq	%rdx, %rsi
	jnb	.L4266
	movzbl	-8(%rax), %edi
	movq	-16(%rax), %r15
	subq	$16, %rax
	movq	%rax, 192(%r12)
	movl	%edi, %ecx
.L3956:
	movq	%r8, -232(%rbp)
	cmpb	%bl, %cl
	je	.L3957
	movzbl	%bl, %r14d
	movl	%r14d, %esi
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$10, %bl
	setne	%sil
	cmpb	$10, %cl
	setne	%dl
	testb	%dl, %sil
	je	.L3957
	cmpb	$1, %al
	movq	-232(%rbp), %r8
	jne	.L4267
.L3957:
	movq	16(%r13), %rdx
	movq	0(%r13), %rax
	movq	224(%r12), %rcx
	movzbl	1(%rdx,%rax), %r8d
	movq	192(%r12), %rdx
	movl	-84(%rcx), %esi
	movq	%rdx, %rax
	subq	184(%r12), %rax
	sarq	$4, %rax
	cmpq	%rax, %rsi
	jb	.L3960
	cmpb	$2, -72(%rcx)
	movq	16(%r12), %rsi
	jne	.L4268
.L3963:
	movq	224(%r12), %rcx
	movq	%rdx, %rax
	subq	184(%r12), %rax
	sarq	$4, %rax
	movl	-84(%rcx), %esi
	cmpq	%rax, %rsi
	jb	.L3966
	cmpb	$2, -72(%rcx)
	jne	.L4269
.L3969:
	movl	$1, %r14d
	jmp	.L3874
	.p2align 4,,10
	.p2align 3
.L4020:
	leaq	.LC580(%rip), %rdx
	movq	%r8, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	xorl	%r14d, %r14d
	jmp	.L3874
	.p2align 4,,10
	.p2align 3
.L3974:
	movq	192(%rsi), %rdx
	subq	184(%rsi), %rdx
	movl	%ecx, %edi
	addl	%r8d, %r14d
	sarq	$4, %rdx
	cmpq	%rdx, %rdi
	jnb	.L4270
	movq	8(%r13), %r15
	leaq	-184(%rbp), %rax
	leaq	-208(%rbp), %rdi
	movq	%rax, %xmm0
	movq	%rax, -232(%rbp)
	leaq	-56(%rbp), %rax
	punpcklqdq	%xmm0, %xmm0
	movslq	%r15d, %rbx
	movq	%rax, -192(%rbp)
	movaps	%xmm0, -208(%rbp)
	cmpq	$8, %rbx
	ja	.L4271
	movq	-232(%rbp), %rax
	salq	$4, %rbx
	addq	%rbx, %rax
	movq	%rax, -200(%rbp)
	subl	$1, %r15d
	js	.L3874
.L4042:
	movl	%r14d, -260(%rbp)
	movslq	%r15d, %rbx
	movl	%r15d, %r14d
	movq	%r13, -240(%rbp)
	jmp	.L3985
	.p2align 4,,10
	.p2align 3
.L4273:
	cmpb	$2, -72(%rcx)
	movq	16(%r12), %r13
	jne	.L4272
.L3980:
	movl	$10, %r15d
.L3982:
	movq	%rbx, %rax
	subl	$1, %r14d
	subq	$1, %rbx
	salq	$4, %rax
	addq	-208(%rbp), %rax
	movq	%r13, (%rax)
	movb	%r15b, 8(%rax)
	cmpl	$-1, %r14d
	je	.L4227
.L3985:
	movq	224(%r12), %rcx
	movq	192(%r12), %rax
	movl	-84(%rcx), %esi
	movq	%rax, %rdx
	subq	184(%r12), %rdx
	sarq	$4, %rdx
	cmpq	%rdx, %rsi
	jnb	.L4273
	movq	-240(%rbp), %rcx
	movzbl	-8(%rax), %r15d
	subq	$16, %rax
	movq	(%rax), %r13
	movq	16(%rcx), %rdx
	addq	%rbx, %rdx
	addq	(%rcx), %rdx
	movzbl	(%rdx), %ecx
	movq	%rax, 192(%r12)
	cmpb	%cl, %r15b
	je	.L3982
	movzbl	%cl, %r9d
	movzbl	%r15b, %edi
	movl	%r9d, %esi
	movl	%r9d, -248(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$10, %r15b
	setne	%sil
	cmpb	$10, %cl
	setne	%dl
	testb	%dl, %sil
	je	.L3982
	cmpb	$1, %al
	je	.L3982
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	24(%r12), %rdi
	cmpq	24(%r12), %r13
	movl	-248(%rbp), %r9d
	movq	%rax, -272(%rbp)
	leaq	.LC482(%rip), %rax
	movq	%rdi, -280(%rbp)
	movq	%rax, -256(%rbp)
	jnb	.L3983
	movq	%r13, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movl	-248(%rbp), %r9d
	movq	%rax, -256(%rbp)
.L3983:
	movl	%r9d, %edi
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	16(%r12), %rsi
	leaq	.LC482(%rip), %rcx
	movq	%rax, %r9
	cmpq	24(%r12), %rsi
	jnb	.L3984
	movq	-280(%rbp), %rdi
	movq	%rax, -248(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-248(%rbp), %r9
	movq	%rax, %rcx
.L3984:
	pushq	-272(%rbp)
	movq	%r13, %rsi
	movl	%r14d, %r8d
	movq	%r12, %rdi
	pushq	-256(%rbp)
	leaq	.LC531(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	popq	%rcx
	popq	%rsi
	jmp	.L3982
	.p2align 4,,10
	.p2align 3
.L4266:
	cmpb	$2, -72(%rcx)
	jne	.L4274
.L3954:
	movl	$10, %edi
	movl	$10, %ecx
	jmp	.L3956
	.p2align 4,,10
	.p2align 3
.L4250:
	leaq	.LC537(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	-216(%rbp), %r14d
	jmp	.L3892
	.p2align 4,,10
	.p2align 3
.L4259:
	leaq	.LC537(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L3924
	.p2align 4,,10
	.p2align 3
.L4265:
	leaq	.LC537(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	16(%r12), %r8
	movq	%r8, %r15
	jmp	.L3949
	.p2align 4,,10
	.p2align 3
.L4258:
	xorl	%eax, %eax
	leaq	.LC537(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%r12), %rax
	jmp	.L3921
	.p2align 4,,10
	.p2align 3
.L3989:
	addq	$1, %rsi
.L4220:
	leaq	.LC580(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L3874
	.p2align 4,,10
	.p2align 3
.L3950:
	leaq	2(%r8), %rsi
.L4218:
	leaq	.LC535(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L3874
	.p2align 4,,10
	.p2align 3
.L3925:
	leaq	2(%r15), %rsi
	jmp	.L4218
	.p2align 4,,10
	.p2align 3
.L3893:
	leaq	2(%r10), %rsi
	movl	%r15d, %ecx
.L4217:
	leaq	.LC578(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L3874
	.p2align 4,,10
	.p2align 3
.L4017:
	movq	16(%r12), %rsi
	jmp	.L4220
	.p2align 4,,10
	.p2align 3
.L4227:
	movl	-260(%rbp), %r14d
	movq	-208(%rbp), %rdi
.L3994:
	cmpq	-232(%rbp), %rdi
	je	.L3874
	call	free@PLT
	jmp	.L3874
	.p2align 4,,10
	.p2align 3
.L4036:
	movq	16(%r13), %rcx
	movq	0(%r13), %rax
	subq	$16, %rdx
	movzbl	8(%rdx), %r8d
	movq	(%rdx), %r13
	movzbl	(%rcx,%rax), %ecx
	movq	%rdx, 192(%r12)
	cmpb	%cl, %r8b
	je	.L3874
	movzbl	%cl, %r15d
	movzbl	%r8b, %edi
	movl	%r15d, %esi
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$10, %r8b
	setne	%sil
	cmpb	$10, %cl
	setne	%dl
	testb	%dl, %sil
	je	.L3874
	cmpb	$1, %al
	je	.L3874
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	24(%r12), %rdi
	leaq	.LC482(%rip), %rbx
	movq	%rax, -232(%rbp)
	movq	%rdi, -240(%rbp)
	cmpq	24(%r12), %r13
	jnb	.L4040
	movq	%r13, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	%rax, %rbx
.L4040:
	movl	%r15d, %edi
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	16(%r12), %rsi
	leaq	.LC482(%rip), %rcx
	movq	%rax, %r9
	cmpq	24(%r12), %rsi
	jnb	.L4041
	movq	-240(%rbp), %rdi
	movq	%rax, -248(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-248(%rbp), %r9
	movq	%rax, %rcx
.L4041:
	pushq	-232(%rbp)
	xorl	%eax, %eax
	xorl	%r8d, %r8d
	movq	%r13, %rsi
	pushq	%rbx
	leaq	.LC531(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	popq	%rax
	popq	%rdx
	jmp	.L3874
	.p2align 4,,10
	.p2align 3
.L4242:
	cmpb	$2, -72(%rcx)
	movq	16(%r12), %rsi
	je	.L4033
	leaq	.LC482(%rip), %rcx
	cmpq	%rsi, 24(%r12)
	jbe	.L4032
	leaq	24(%r12), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %rsi
	movq	%rax, %rcx
.L4032:
	leaq	.LC530(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	192(%r12), %rdx
	jmp	.L4033
	.p2align 4,,10
	.p2align 3
.L4255:
	leaq	.LC482(%rip), %rcx
	cmpq	%r13, 24(%r12)
	jbe	.L3997
	movq	%r13, %rsi
	leaq	24(%r12), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %r13
	movq	%rax, %rcx
.L3997:
	movq	%r13, %rsi
	leaq	.LC530(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	16(%r12), %r13
	jmp	.L3996
	.p2align 4,,10
	.p2align 3
.L4272:
	leaq	.LC482(%rip), %rcx
	cmpq	%r13, 24(%r12)
	jbe	.L3981
	movq	%r13, %rsi
	leaq	24(%r12), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %r13
	movq	%rax, %rcx
.L3981:
	movq	%r13, %rsi
	leaq	.LC530(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	16(%r12), %r13
	jmp	.L3980
	.p2align 4,,10
	.p2align 3
.L3966:
	movq	16(%r13), %rcx
	movq	0(%r13), %rax
	subq	$16, %rdx
	movzbl	8(%rdx), %r8d
	movq	(%rdx), %r13
	movzbl	(%rcx,%rax), %ecx
	movq	%rdx, 192(%r12)
	cmpb	%cl, %r8b
	je	.L3969
	movzbl	%cl, %r15d
	movzbl	%r8b, %edi
	movl	%r15d, %esi
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$10, %r8b
	setne	%sil
	cmpb	$10, %cl
	setne	%dl
	testb	%dl, %sil
	je	.L3969
	cmpb	$1, %al
	je	.L3969
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	24(%r12), %rdi
	leaq	.LC482(%rip), %r14
	movq	%rdi, -232(%rbp)
	movq	%rax, %rbx
	cmpq	24(%r12), %r13
	jnb	.L3970
	movq	%r13, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	%rax, %r14
.L3970:
	movl	%r15d, %edi
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	16(%r12), %rsi
	leaq	.LC482(%rip), %rcx
	movq	%rax, %r9
	cmpq	24(%r12), %rsi
	jnb	.L3971
	movq	-232(%rbp), %rdi
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-240(%rbp), %r9
	movq	%rax, %rcx
.L3971:
	pushq	%rbx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	.LC531(%rip), %rdx
	pushq	%r14
	movq	%r13, %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	popq	%rdi
	popq	%r8
	jmp	.L3969
	.p2align 4,,10
	.p2align 3
.L3960:
	movzbl	-8(%rdx), %edi
	leaq	-16(%rdx), %rcx
	movq	-16(%rdx), %r14
	movq	%rcx, 192(%r12)
	movq	%rcx, %rdx
	cmpb	%r8b, %dil
	je	.L3963
	movzbl	%r8b, %r15d
	movl	%r15d, %esi
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$10, %r8b
	movq	%rcx, %rdx
	setne	%r8b
	cmpb	$10, %dil
	setne	%sil
	testb	%sil, %r8b
	je	.L3963
	cmpb	$1, %al
	je	.L3963
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	24(%r12), %rdi
	leaq	.LC482(%rip), %rbx
	movq	%rax, -232(%rbp)
	movq	%rdi, -240(%rbp)
	cmpq	24(%r12), %r14
	jnb	.L3964
	movq	%r14, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	%rax, %rbx
.L3964:
	movl	%r15d, %edi
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	16(%r12), %rsi
	leaq	.LC482(%rip), %rcx
	movq	%rax, %r9
	cmpq	24(%r12), %rsi
	jnb	.L3965
	movq	-240(%rbp), %rdi
	movq	%rax, -248(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-248(%rbp), %r9
	movq	%rax, %rcx
.L3965:
	pushq	-232(%rbp)
	movq	%r14, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	pushq	%rbx
	leaq	.LC531(%rip), %rdx
	movl	$1, %r8d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	popq	%r9
	movq	192(%r12), %rdx
	popq	%r10
	jmp	.L3963
	.p2align 4,,10
	.p2align 3
.L4260:
	movzbl	-8(%rax), %edi
	movq	-16(%rax), %r15
	subq	$16, %rax
	movq	%rax, 192(%r12)
	movl	%edi, %ecx
	jmp	.L3931
	.p2align 4,,10
	.p2align 3
.L4023:
	movzbl	-8(%rax), %edi
	movq	-16(%rax), %r15
	subq	$16, %rax
	movq	%rax, 192(%r12)
	movl	%edi, %ecx
	jmp	.L4026
	.p2align 4,,10
	.p2align 3
.L3941:
	movq	16(%r13), %rcx
	movq	0(%r13), %rax
	subq	$16, %rdx
	movzbl	8(%rdx), %r8d
	movq	(%rdx), %r13
	movzbl	(%rcx,%rax), %ecx
	movq	%rdx, 192(%r12)
	cmpb	%cl, %r8b
	je	.L3944
	movzbl	%cl, %r15d
	movzbl	%r8b, %edi
	movl	%r15d, %esi
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$10, %r8b
	setne	%sil
	cmpb	$10, %cl
	setne	%dl
	testb	%dl, %sil
	je	.L3944
	cmpb	$1, %al
	je	.L3944
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	24(%r12), %rdi
	leaq	.LC482(%rip), %r14
	movq	%rdi, -232(%rbp)
	movq	%rax, %rbx
	cmpq	24(%r12), %r13
	jnb	.L3945
	movq	%r13, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	%rax, %r14
.L3945:
	movl	%r15d, %edi
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	16(%r12), %rsi
	leaq	.LC482(%rip), %rcx
	movq	%rax, %r9
	cmpq	24(%r12), %rsi
	jnb	.L3946
	movq	-232(%rbp), %rdi
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-240(%rbp), %r9
	movq	%rax, %rcx
.L3946:
	pushq	%rbx
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	pushq	%r14
	leaq	.LC531(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	popq	%r12
	popq	%r13
	jmp	.L3944
	.p2align 4,,10
	.p2align 3
.L3935:
	movzbl	-8(%rdx), %edi
	leaq	-16(%rdx), %rcx
	movq	-16(%rdx), %r14
	movq	%rcx, 192(%r12)
	movq	%rcx, %rdx
	cmpb	%r8b, %dil
	je	.L3938
	movzbl	%r8b, %r15d
	movl	%r15d, %esi
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$10, %r8b
	movq	%rcx, %rdx
	setne	%r8b
	cmpb	$10, %dil
	setne	%sil
	testb	%sil, %r8b
	je	.L3938
	cmpb	$1, %al
	je	.L3938
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	24(%r12), %rdi
	leaq	.LC482(%rip), %rbx
	movq	%rax, -232(%rbp)
	movq	%rdi, -240(%rbp)
	cmpq	24(%r12), %r14
	jnb	.L3939
	movq	%r14, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	%rax, %rbx
.L3939:
	movl	%r15d, %edi
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	16(%r12), %rsi
	leaq	.LC482(%rip), %rcx
	movq	%rax, %r9
	cmpq	24(%r12), %rsi
	jnb	.L3940
	movq	-240(%rbp), %rdi
	movq	%rax, -248(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-248(%rbp), %r9
	movq	%rax, %rcx
.L3940:
	pushq	-232(%rbp)
	movq	%r14, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	pushq	%rbx
	leaq	.LC531(%rip), %rdx
	movl	$1, %r8d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	popq	%r14
	movq	192(%r12), %rdx
	popq	%r15
	jmp	.L3938
	.p2align 4,,10
	.p2align 3
.L4011:
	movq	80(%r12), %rcx
	movl	-216(%rbp), %eax
	subq	$16, %rdx
	movq	(%rdx), %r13
	salq	$4, %rax
	addq	184(%rcx), %rax
	movzbl	8(%rdx), %ecx
	movzbl	(%rax), %r8d
	movq	%rdx, 192(%r12)
	cmpb	%cl, %r8b
	je	.L4222
	movzbl	%r8b, %r15d
	movzbl	%cl, %edi
	movl	%r15d, %esi
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$10, %r8b
	setne	%sil
	cmpb	$10, %cl
	setne	%dl
	testb	%dl, %sil
	je	.L4222
	cmpb	$1, %al
	je	.L4222
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	24(%r12), %rdi
	leaq	.LC482(%rip), %rbx
	movq	%rax, -232(%rbp)
	movq	%rdi, -240(%rbp)
	cmpq	24(%r12), %r13
	jnb	.L4015
	movq	%r13, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	%rax, %rbx
.L4015:
	movl	%r15d, %edi
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	16(%r12), %rsi
	leaq	.LC482(%rip), %rcx
	movq	%rax, %r9
	cmpq	24(%r12), %rsi
	jnb	.L4016
	movq	-240(%rbp), %rdi
	movq	%rax, -248(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-248(%rbp), %r9
	movq	%rax, %rcx
.L4016:
	pushq	-232(%rbp)
	xorl	%r8d, %r8d
	movq	%r13, %rsi
	movq	%r12, %rdi
	pushq	%rbx
	leaq	.LC531(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	popq	%r9
	popq	%r10
	jmp	.L4222
	.p2align 4,,10
	.p2align 3
.L4005:
	movzbl	-8(%rdx), %edi
	leaq	-16(%rdx), %rcx
	movq	-16(%rdx), %r13
	movq	%rsi, -232(%rbp)
	movq	%rcx, 192(%r12)
	movq	%rcx, %rdx
	cmpb	%r10b, %dil
	je	.L4008
	movzbl	%r10b, %r15d
	movl	%r15d, %esi
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$10, %dil
	movq	%rcx, %rdx
	setne	%r11b
	cmpb	$10, %r10b
	setne	%sil
	testb	%sil, %r11b
	je	.L4008
	cmpb	$1, %al
	je	.L4008
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	24(%r12), %rdi
	cmpq	24(%r12), %r13
	movq	-232(%rbp), %r8
	movq	%rax, -240(%rbp)
	leaq	.LC482(%rip), %rbx
	movq	%rdi, -248(%rbp)
	jnb	.L4009
	movq	%r13, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %r8
	movq	%rax, %rbx
.L4009:
	movl	%r15d, %edi
	movq	%r8, -232(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	-232(%rbp), %r8
	leaq	.LC482(%rip), %rcx
	movq	%rax, %r9
	cmpq	24(%r12), %r8
	jnb	.L4010
	movq	-248(%rbp), %rdi
	movq	%r8, %rsi
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-232(%rbp), %r9
	movq	%rax, %rcx
.L4010:
	pushq	-240(%rbp)
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	pushq	%rbx
	leaq	.LC531(%rip), %rdx
	movl	$1, %r8d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	popq	%r11
	movq	192(%r12), %rdx
	popq	%rbx
	jmp	.L4008
	.p2align 4,,10
	.p2align 3
.L3897:
	movzbl	-8(%rax), %edi
	movq	-16(%rax), %r10
	subq	$16, %rax
	movq	%rax, 192(%r12)
	movl	%edi, %ecx
	jmp	.L3900
	.p2align 4,,10
	.p2align 3
.L4253:
	movl	-232(%rbp), %ecx
	addq	$2, %rsi
	jmp	.L4220
	.p2align 4,,10
	.p2align 3
.L3890:
	leaq	.LC536(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movl	-216(%rbp), %r14d
	jmp	.L3892
	.p2align 4,,10
	.p2align 3
.L3919:
	leaq	.LC536(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	24(%r12), %rax
	jmp	.L3921
	.p2align 4,,10
	.p2align 3
.L3922:
	leaq	.LC536(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	jmp	.L3924
	.p2align 4,,10
	.p2align 3
.L3947:
	leaq	.LC536(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	16(%r12), %r8
	movq	%r8, %r15
	jmp	.L3949
	.p2align 4,,10
	.p2align 3
.L4270:
	subq	%r8, %r14
	leaq	-1(%rax,%r14), %rsi
	jmp	.L4220
.L4268:
	leaq	.LC482(%rip), %rcx
	cmpq	%rsi, 24(%r12)
	jbe	.L3962
	leaq	24(%r12), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %rsi
	movq	%rax, %rcx
.L3962:
	leaq	.LC530(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	192(%r12), %rdx
	jmp	.L3963
.L4274:
	leaq	.LC482(%rip), %rcx
	cmpq	%r8, 24(%r12)
	jbe	.L3955
	leaq	24(%r12), %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %r15
	movq	%rax, %rcx
.L3955:
	movq	%r15, %rsi
	leaq	.LC530(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	16(%r12), %r15
	movq	%r15, %r8
	jmp	.L3954
.L4269:
	movq	16(%r12), %rsi
	leaq	.LC482(%rip), %rcx
	cmpq	24(%r12), %rsi
	jnb	.L3968
	leaq	24(%r12), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %rsi
	movq	%rax, %rcx
.L3968:
	leaq	.LC530(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L3969
.L4254:
	movq	%rbx, %rsi
	salq	$4, %rbx
	call	_ZN2v84base11SmallVectorINS_8internal4wasm9ValueBaseELm8EE4GrowEm
	movq	-208(%rbp), %rdi
	leaq	(%rdi,%rbx), %rax
	movq	%rax, -200(%rbp)
	subl	$1, %r15d
	jns	.L4043
	jmp	.L3994
.L4271:
	movq	%rbx, %rsi
	salq	$4, %rbx
	call	_ZN2v84base11SmallVectorINS_8internal4wasm9ValueBaseELm8EE4GrowEm
	movq	-208(%rbp), %rdi
	leaq	(%rdi,%rbx), %rax
	movq	%rax, -200(%rbp)
	subl	$1, %r15d
	jns	.L4042
	jmp	.L3994
.L4267:
	movq	%r8, -248(%rbp)
	leaq	24(%r12), %rbx
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	cmpq	24(%r12), %r15
	movq	-248(%rbp), %r8
	movq	%rax, -240(%rbp)
	leaq	.LC482(%rip), %rax
	movq	%rax, -232(%rbp)
	jnb	.L3958
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %r8
	movq	%rax, -232(%rbp)
.L3958:
	movl	%r14d, %edi
	movq	%r8, -248(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	-248(%rbp), %r8
	leaq	.LC482(%rip), %rcx
	movq	%rax, %r9
	cmpq	24(%r12), %r8
	jnb	.L3959
	movq	%r8, %rsi
	movq	%rbx, %rdi
	movq	%rax, -248(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-248(%rbp), %r9
	movq	%rax, %rcx
.L3959:
	pushq	-240(%rbp)
	movq	%r15, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	pushq	-232(%rbp)
	movl	$2, %r8d
	leaq	.LC531(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	popq	%r11
	popq	%rbx
	jmp	.L3957
.L4261:
	leaq	.LC482(%rip), %rcx
	cmpq	%r15, 24(%r12)
	jbe	.L3930
	movq	%r15, %rsi
	leaq	24(%r12), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %r15
	movq	%rax, %rcx
.L3930:
	movq	%r15, %rsi
	leaq	.LC530(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	16(%r12), %r15
	jmp	.L3929
.L4251:
	leaq	.LC482(%rip), %rcx
	cmpq	%r10, 24(%r12)
	jbe	.L3899
	movq	%r10, %rsi
	leaq	24(%r12), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %r10
	movq	%rax, %rcx
.L3899:
	movq	%r10, %rsi
	leaq	.LC530(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	16(%r12), %r10
	jmp	.L3898
.L4263:
	leaq	.LC482(%rip), %rcx
	cmpq	%rsi, 24(%r12)
	jbe	.L3937
	leaq	24(%r12), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %rsi
	movq	%rax, %rcx
.L3937:
	leaq	.LC530(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	192(%r12), %rdx
	jmp	.L3938
.L4257:
	leaq	.LC482(%rip), %rcx
	cmpq	24(%r12), %rsi
	jnb	.L4007
	leaq	24(%r12), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %rsi
	movq	%rax, %rcx
.L4007:
	leaq	.LC530(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	192(%r12), %rdx
	jmp	.L4008
.L4248:
	leaq	.LC482(%rip), %rcx
	cmpq	24(%r12), %r15
	jnb	.L4025
	movq	%r15, %rsi
	leaq	24(%r12), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %r15
	movq	%rax, %rcx
.L4025:
	movq	%r15, %rsi
	leaq	.LC530(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	16(%r12), %r15
	movq	%r15, %r8
	jmp	.L4024
.L4264:
	movq	16(%r12), %rsi
	leaq	.LC482(%rip), %rcx
	cmpq	24(%r12), %rsi
	jnb	.L3943
	leaq	24(%r12), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %rsi
	movq	%rax, %rcx
.L3943:
	leaq	.LC530(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L3944
.L4262:
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	24(%r12), %rdi
	movq	%rax, %rbx
	leaq	.LC482(%rip), %rax
	movq	%rdi, -240(%rbp)
	movq	%rax, -232(%rbp)
	cmpq	24(%r12), %r15
	jnb	.L3933
	movq	%r15, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	%rax, -232(%rbp)
.L3933:
	movl	%r14d, %edi
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	16(%r12), %rsi
	leaq	.LC482(%rip), %rcx
	movq	%rax, %r9
	cmpq	24(%r12), %rsi
	jnb	.L3934
	movq	-240(%rbp), %rdi
	movq	%rax, -248(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-248(%rbp), %r9
	movq	%rax, %rcx
.L3934:
	pushq	%rbx
	leaq	.LC531(%rip), %rdx
	xorl	%eax, %eax
	movl	$2, %r8d
	pushq	-232(%rbp)
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	popq	%rax
	popq	%rdx
	jmp	.L3932
.L4249:
	movq	%r8, -248(%rbp)
	leaq	.LC482(%rip), %rbx
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	24(%r12), %rdi
	cmpq	24(%r12), %r15
	movl	-240(%rbp), %r9d
	movq	%rax, -232(%rbp)
	movq	-248(%rbp), %r8
	movq	%rdi, -256(%rbp)
	jnb	.L4028
	movq	%r15, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %r8
	movl	-240(%rbp), %r9d
	movq	%rax, %rbx
.L4028:
	movl	%r9d, %edi
	movq	%r8, -240(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	-240(%rbp), %r8
	leaq	.LC482(%rip), %rcx
	movq	%rax, %r9
	cmpq	24(%r12), %r8
	jnb	.L4029
	movq	-256(%rbp), %rdi
	movq	%r8, %rsi
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-240(%rbp), %r9
	movq	%rax, %rcx
.L4029:
	pushq	-232(%rbp)
	movq	%r12, %rdi
	movq	%r15, %rsi
	xorl	%eax, %eax
	pushq	%rbx
	movl	$2, %r8d
	leaq	.LC531(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	popq	%rdi
	popq	%r8
	jmp	.L4027
.L4252:
	movq	%r10, -240(%rbp)
	leaq	.LC482(%rip), %rbx
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	24(%r12), %rdi
	movq	-240(%rbp), %r10
	movq	%rax, -232(%rbp)
	movq	%rdi, -248(%rbp)
	cmpq	24(%r12), %r10
	jnb	.L3902
	movq	%r10, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-240(%rbp), %r10
	movq	%rax, %rbx
.L3902:
	movl	%r15d, %edi
	movq	%r10, -240(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	16(%r12), %rsi
	cmpq	24(%r12), %rsi
	leaq	.LC482(%rip), %rcx
	movq	-240(%rbp), %r10
	movq	%rax, %r9
	jnb	.L3903
	movq	-248(%rbp), %rdi
	movq	%r10, -256(%rbp)
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-256(%rbp), %r10
	movq	-240(%rbp), %r9
	movq	%rax, %rcx
.L3903:
	pushq	-232(%rbp)
	movq	%r10, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	pushq	%rbx
	movl	$2, %r8d
	leaq	.LC531(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	popq	%rcx
	popq	%rsi
	jmp	.L3901
.L4247:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23598:
	.size	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19DecodeNumericOpcodeENS1_10WasmOpcodeE, .-_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19DecodeNumericOpcodeENS1_10WasmOpcodeE
	.section	.text._ZN2v88internal4wasm7Decoder13read_leb_tailIlLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_,"axG",@progbits,_ZN2v88internal4wasm7Decoder13read_leb_tailIlLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm7Decoder13read_leb_tailIlLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	.type	_ZN2v88internal4wasm7Decoder13read_leb_tailIlLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_, @function
_ZN2v88internal4wasm7Decoder13read_leb_tailIlLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_:
.LFB24348:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	24(%rdi), %r10
	cmpq	%rsi, %r10
	ja	.L4309
	movl	$0, (%rdx)
	xorl	%eax, %eax
	leaq	.LC487(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	xorl	%eax, %eax
.L4275:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4309:
	.cfi_restore_state
	movzbl	(%rsi), %r9d
	movq	%r9, %rax
	andl	$127, %eax
	orq	%r8, %rax
	testb	%r9b, %r9b
	js	.L4310
	movl	$1, (%rdx)
	salq	$57, %rax
	addq	$16, %rsp
	popq	%rbx
	sarq	$57, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4310:
	.cfi_restore_state
	leaq	1(%rsi), %r8
	cmpq	%r8, %r10
	jbe	.L4278
	movzbl	1(%rsi), %r9d
	movq	%r9, %r8
	salq	$7, %r8
	andl	$16256, %r8d
	orq	%r8, %rax
	testb	%r9b, %r9b
	js	.L4311
	salq	$50, %rax
	movl	$2, (%rdx)
	sarq	$50, %rax
	jmp	.L4275
	.p2align 4,,10
	.p2align 3
.L4278:
	movl	$1, (%rdx)
.L4308:
	xorl	%eax, %eax
	leaq	.LC487(%rip), %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4311:
	.cfi_restore_state
	leaq	2(%rsi), %r8
	cmpq	%r8, %r10
	jbe	.L4280
	movzbl	2(%rsi), %r8d
	movq	%r8, %r9
	salq	$14, %r9
	andl	$2080768, %r9d
	orq	%r9, %rax
	testb	%r8b, %r8b
	js	.L4312
	salq	$43, %rax
	movl	$3, (%rdx)
	sarq	$43, %rax
	jmp	.L4275
	.p2align 4,,10
	.p2align 3
.L4280:
	movl	$2, (%rdx)
	jmp	.L4308
	.p2align 4,,10
	.p2align 3
.L4312:
	leaq	3(%rsi), %r8
	cmpq	%r8, %r10
	ja	.L4313
	movl	$3, (%rdx)
	jmp	.L4308
.L4313:
	movzbl	3(%rsi), %r9d
	movq	%r9, %r8
	salq	$21, %r8
	andl	$266338304, %r8d
	orq	%r8, %rax
	testb	%r9b, %r9b
	js	.L4314
	salq	$36, %rax
	movl	$4, (%rdx)
	sarq	$36, %rax
	jmp	.L4275
.L4314:
	leaq	4(%rsi), %r8
	cmpq	%r8, %r10
	ja	.L4315
	movl	$4, (%rdx)
	jmp	.L4308
.L4315:
	movabsq	$34091302912, %r11
	movzbl	4(%rsi), %r9d
	movq	%r9, %r8
	salq	$28, %r8
	andq	%r11, %r8
	orq	%r8, %rax
	testb	%r9b, %r9b
	js	.L4316
	salq	$29, %rax
	movl	$5, (%rdx)
	sarq	$29, %rax
	jmp	.L4275
.L4316:
	leaq	5(%rsi), %r8
	cmpq	%r8, %r10
	ja	.L4317
	movl	$5, (%rdx)
	xorl	%eax, %eax
	movq	%r8, %rsi
	leaq	.LC487(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	xorl	%eax, %eax
.L4301:
	salq	$22, %rax
	sarq	$22, %rax
	jmp	.L4275
.L4317:
	movabsq	$4363686772736, %r11
	movzbl	5(%rsi), %r9d
	movq	%r9, %r8
	salq	$35, %r8
	andq	%r11, %r8
	orq	%r8, %rax
	testb	%r9b, %r9b
	js	.L4318
	movl	$6, (%rdx)
	jmp	.L4301
.L4318:
	leaq	6(%rsi), %r8
	cmpq	%r8, %r10
	ja	.L4319
	movl	$6, (%rdx)
	xorl	%eax, %eax
	movq	%r8, %rsi
	leaq	.LC487(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	xorl	%eax, %eax
.L4300:
	salq	$15, %rax
	sarq	$15, %rax
	jmp	.L4275
.L4319:
	movabsq	$558551906910208, %r11
	movzbl	6(%rsi), %r9d
	movq	%r9, %r8
	salq	$42, %r8
	andq	%r11, %r8
	orq	%r8, %rax
	testb	%r9b, %r9b
	js	.L4320
	movl	$7, (%rdx)
	jmp	.L4300
.L4320:
	leaq	7(%rsi), %r8
	cmpq	%r8, %r10
	ja	.L4321
	movl	$7, (%rdx)
	xorl	%eax, %eax
	movq	%r8, %rsi
	leaq	.LC487(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	xorl	%eax, %eax
.L4299:
	salq	$8, %rax
	sarq	$8, %rax
	jmp	.L4275
.L4321:
	movabsq	$71494644084506624, %r11
	movzbl	7(%rsi), %r9d
	movq	%r9, %r8
	salq	$49, %r8
	andq	%r11, %r8
	orq	%r8, %rax
	testb	%r9b, %r9b
	js	.L4322
	movl	$8, (%rdx)
	jmp	.L4299
.L4322:
	leaq	8(%rsi), %r8
	cmpq	%r8, %r10
	ja	.L4323
	movl	$8, (%rdx)
	xorl	%eax, %eax
	movq	%r8, %rsi
	leaq	.LC487(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	xorl	%eax, %eax
.L4298:
	addq	%rax, %rax
	sarq	%rax
	jmp	.L4275
.L4323:
	movabsq	$9151314442816847872, %r11
	movzbl	8(%rsi), %r9d
	movq	%r9, %r8
	salq	$56, %r8
	andq	%r11, %r8
	orq	%rax, %r8
	movq	%r8, %rax
	testb	%r9b, %r9b
	js	.L4324
	movl	$9, (%rdx)
	jmp	.L4298
.L4324:
	leaq	9(%rsi), %r12
	cmpq	%r12, %r10
	jbe	.L4294
	movzbl	9(%rsi), %ebx
	movl	$10, (%rdx)
	testb	%bl, %bl
	js	.L4295
	movq	%rbx, %rax
	salq	$63, %rax
	orq	%r8, %rax
.L4296:
	testb	%bl, %bl
	je	.L4275
	cmpb	$127, %bl
	je	.L4275
	leaq	.LC488(%rip), %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	xorl	%eax, %eax
	jmp	.L4275
.L4294:
	movl	$9, (%rdx)
	xorl	%ebx, %ebx
.L4295:
	xorl	%eax, %eax
	leaq	.LC487(%rip), %rdx
	movq	%r12, %rsi
	movq	%rdi, -24(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-24(%rbp), %rdi
	xorl	%eax, %eax
	jmp	.L4296
	.cfi_endproc
.LFE24348:
	.size	_ZN2v88internal4wasm7Decoder13read_leb_tailIlLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_, .-_ZN2v88internal4wasm7Decoder13read_leb_tailIlLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE18DecodeFunctionBodyEv.str1.8,"aMS",@progbits,1
	.align 8
.LC581:
	.string	"block type index %u out of bounds (%zu signatures)"
	.align 8
.LC582:
	.string	"Invalid opcode (enable with --experimental-wasm-eh)"
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE18DecodeFunctionBodyEv.str1.1,"aMS",@progbits,1
.LC583:
	.string	"exception index"
.LC584:
	.string	"Invalid exception index: %u"
.LC585:
	.string	"catch does not match any try"
.LC586:
	.string	"catch already present for try"
.LC587:
	.string	"branch depth"
.LC588:
	.string	"invalid branch depth: %u"
.LC589:
	.string	"else does not match any if"
.LC590:
	.string	"else does not match an if"
.LC591:
	.string	"else already present for if"
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE18DecodeFunctionBodyEv.str1.8
	.align 8
.LC592:
	.string	"end does not match any if, try, or block"
	.align 8
.LC593:
	.string	"missing catch or catch-all in try"
	.align 8
.LC594:
	.string	"start-arity and end-arity of one-armed if must match"
	.align 8
.LC595:
	.string	"trailing code after function end"
	.align 8
.LC596:
	.string	"select without type is only valid for value type inputs"
	.align 8
.LC597:
	.string	"Invalid opcode (enable with --experimental-wasm-anyref)"
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE18DecodeFunctionBodyEv.str1.1
.LC598:
	.string	"table count"
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE18DecodeFunctionBodyEv.str1.8
	.align 8
.LC599:
	.string	"invalid table count (> max function size): %u"
	.align 8
.LC600:
	.string	"improper branch in br_table target %u (depth %u)"
	.align 8
.LC601:
	.string	"expected %u elements on the stack for return, found %u"
	.align 8
.LC602:
	.string	"type error in return[%u] (expected %s, got %s)"
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE18DecodeFunctionBodyEv.str1.1
.LC603:
	.string	"immi32"
.LC604:
	.string	"immi64"
.LC605:
	.string	"immf32"
.LC606:
	.string	"immf64"
.LC607:
	.string	"function index"
.LC608:
	.string	"invalid function index: %u"
.LC609:
	.string	"local index"
.LC610:
	.string	"invalid local index: %u"
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE18DecodeFunctionBodyEv.str1.8
	.align 8
.LC611:
	.string	"vector::_M_range_check: __n (which is %zu) >= this->size() (which is %zu)"
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE18DecodeFunctionBodyEv.str1.1
.LC612:
	.string	"global index"
.LC613:
	.string	"invalid global index: %u"
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE18DecodeFunctionBodyEv.str1.8
	.align 8
.LC614:
	.string	"immutable global #%u cannot be assigned"
	.align 8
.LC615:
	.string	"grow_memory is not supported for asmjs modules"
	.align 8
.LC616:
	.string	"function table has to exist to execute call_indirect"
	.align 8
.LC617:
	.string	"table of call_indirect must be of type funcref"
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE18DecodeFunctionBodyEv.str1.1
.LC618:
	.string	"invalid signature index: #%u"
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE18DecodeFunctionBodyEv.str1.8
	.align 8
.LC619:
	.string	"Invalid opcode (enable with --experimental-wasm-return_call)"
	.align 8
.LC620:
	.string	"tail call return types mismatch"
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE18DecodeFunctionBodyEv.str1.1
.LC621:
	.string	"%s: %s"
.LC622:
	.string	"numeric index"
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE18DecodeFunctionBodyEv.str1.8
	.align 8
.LC623:
	.string	"Invalid opcode (enable with --experimental-wasm-sat_f2i_conversions)"
	.align 8
.LC624:
	.string	"Invalid opcode (enable with --experimental-wasm-bulk_memory)"
	.align 8
.LC625:
	.string	"Invalid opcode (enable with --experimental-wasm-simd)"
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE18DecodeFunctionBodyEv.str1.1
.LC626:
	.string	"simd index"
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE18DecodeFunctionBodyEv.str1.8
	.align 8
.LC627:
	.string	"Invalid opcode (enable with --experimental-wasm-threads)"
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE18DecodeFunctionBodyEv.str1.1
.LC628:
	.string	"atomic index"
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE18DecodeFunctionBodyEv.str1.8
	.align 8
.LC629:
	.string	"Invalid opcode (enable with --experimental-wasm-se)"
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE18DecodeFunctionBodyEv.str1.1
.LC630:
	.string	"Invalid opcode"
.LC631:
	.string	"Beyond end of code"
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE18DecodeFunctionBodyEv,"axG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE18DecodeFunctionBodyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE18DecodeFunctionBodyEv
	.type	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE18DecodeFunctionBodyEv, @function
_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE18DecodeFunctionBodyEv:
.LFB22650:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$344, %rsp
	.cfi_offset 3, -56
	movq	224(%rdi), %rbx
	movq	216(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%r15, %rbx
	je	.L4326
	cmpb	$0, -72(%rbx)
	setne	%dl
.L4326:
	movq	192(%r12), %r14
	subq	184(%r12), %r14
	sarq	$4, %r14
	cmpq	232(%r12), %rbx
	je	.L4327
	movq	16(%r12), %rax
	pxor	%xmm0, %xmm0
	movb	$2, (%rbx)
	movl	%r14d, 4(%rbx)
	movq	%rax, 8(%rbx)
	movl	%edx, %eax
	xorl	$1, %eax
	movb	%dl, 16(%rbx)
	movb	%al, 48(%rbx)
	movl	$0, 24(%rbx)
	andb	$1, 48(%rbx)
	movl	$0, 56(%rbx)
	movb	$0, 80(%rbx)
	movups	%xmm0, 32(%rbx)
	movups	%xmm0, 64(%rbx)
	movq	224(%r12), %rax
	leaq	88(%rax), %r13
	movq	%r13, 224(%r12)
.L4328:
	movl	$0, -64(%r13)
	movq	112(%r12), %rax
	movq	(%rax), %rbx
	movl	%ebx, -32(%r13)
	cmpl	$1, %ebx
	je	.L5438
	ja	.L5439
.L4337:
	movq	16(%r12), %r13
	movq	24(%r12), %r15
	cmpq	%r13, %r15
	jbe	.L4342
	leaq	.L4345(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L4997:
	movzbl	0(%r13), %r14d
	cmpb	$-2, %r14b
	ja	.L4343
	movzbl	%r14b, %eax
	movslq	(%rbx,%rax,4), %rax
	addq	%rbx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE18DecodeFunctionBodyEv,"aG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE18DecodeFunctionBodyEv,comdat
	.align 4
	.align 4
.L4345:
	.long	.L4532-.L4345
	.long	.L4531-.L4345
	.long	.L4530-.L4345
	.long	.L4529-.L4345
	.long	.L4528-.L4345
	.long	.L4527-.L4345
	.long	.L4526-.L4345
	.long	.L4525-.L4345
	.long	.L4524-.L4345
	.long	.L4523-.L4345
	.long	.L4522-.L4345
	.long	.L4521-.L4345
	.long	.L4520-.L4345
	.long	.L4519-.L4345
	.long	.L4518-.L4345
	.long	.L4517-.L4345
	.long	.L4516-.L4345
	.long	.L4515-.L4345
	.long	.L4514-.L4345
	.long	.L4513-.L4345
	.long	.L4343-.L4345
	.long	.L4343-.L4345
	.long	.L4343-.L4345
	.long	.L4343-.L4345
	.long	.L4343-.L4345
	.long	.L4343-.L4345
	.long	.L4512-.L4345
	.long	.L4511-.L4345
	.long	.L4510-.L4345
	.long	.L4343-.L4345
	.long	.L4343-.L4345
	.long	.L4343-.L4345
	.long	.L4509-.L4345
	.long	.L4508-.L4345
	.long	.L4507-.L4345
	.long	.L4506-.L4345
	.long	.L4505-.L4345
	.long	.L4504-.L4345
	.long	.L4503-.L4345
	.long	.L4343-.L4345
	.long	.L4502-.L4345
	.long	.L4501-.L4345
	.long	.L4500-.L4345
	.long	.L4499-.L4345
	.long	.L4498-.L4345
	.long	.L4497-.L4345
	.long	.L4496-.L4345
	.long	.L4495-.L4345
	.long	.L4494-.L4345
	.long	.L4493-.L4345
	.long	.L4492-.L4345
	.long	.L4491-.L4345
	.long	.L4490-.L4345
	.long	.L4489-.L4345
	.long	.L4488-.L4345
	.long	.L4487-.L4345
	.long	.L4486-.L4345
	.long	.L4485-.L4345
	.long	.L4484-.L4345
	.long	.L4483-.L4345
	.long	.L4482-.L4345
	.long	.L4481-.L4345
	.long	.L4480-.L4345
	.long	.L4479-.L4345
	.long	.L4478-.L4345
	.long	.L4477-.L4345
	.long	.L4476-.L4345
	.long	.L4475-.L4345
	.long	.L4474-.L4345
	.long	.L4473-.L4345
	.long	.L4472-.L4345
	.long	.L4471-.L4345
	.long	.L4470-.L4345
	.long	.L4469-.L4345
	.long	.L4468-.L4345
	.long	.L4467-.L4345
	.long	.L4466-.L4345
	.long	.L4465-.L4345
	.long	.L4464-.L4345
	.long	.L4463-.L4345
	.long	.L4462-.L4345
	.long	.L4461-.L4345
	.long	.L4460-.L4345
	.long	.L4459-.L4345
	.long	.L4458-.L4345
	.long	.L4457-.L4345
	.long	.L4456-.L4345
	.long	.L4455-.L4345
	.long	.L4454-.L4345
	.long	.L4453-.L4345
	.long	.L4452-.L4345
	.long	.L4451-.L4345
	.long	.L4450-.L4345
	.long	.L4449-.L4345
	.long	.L4448-.L4345
	.long	.L4447-.L4345
	.long	.L4446-.L4345
	.long	.L4445-.L4345
	.long	.L4444-.L4345
	.long	.L4443-.L4345
	.long	.L4442-.L4345
	.long	.L4441-.L4345
	.long	.L4440-.L4345
	.long	.L4439-.L4345
	.long	.L4438-.L4345
	.long	.L4437-.L4345
	.long	.L4436-.L4345
	.long	.L4435-.L4345
	.long	.L4434-.L4345
	.long	.L4433-.L4345
	.long	.L4432-.L4345
	.long	.L4431-.L4345
	.long	.L4430-.L4345
	.long	.L4429-.L4345
	.long	.L4428-.L4345
	.long	.L4427-.L4345
	.long	.L4426-.L4345
	.long	.L4425-.L4345
	.long	.L4424-.L4345
	.long	.L4423-.L4345
	.long	.L4422-.L4345
	.long	.L4421-.L4345
	.long	.L4420-.L4345
	.long	.L4419-.L4345
	.long	.L4418-.L4345
	.long	.L4417-.L4345
	.long	.L4416-.L4345
	.long	.L4415-.L4345
	.long	.L4414-.L4345
	.long	.L4413-.L4345
	.long	.L4412-.L4345
	.long	.L4411-.L4345
	.long	.L4410-.L4345
	.long	.L4409-.L4345
	.long	.L4408-.L4345
	.long	.L4407-.L4345
	.long	.L4406-.L4345
	.long	.L4405-.L4345
	.long	.L4404-.L4345
	.long	.L4403-.L4345
	.long	.L4402-.L4345
	.long	.L4401-.L4345
	.long	.L4400-.L4345
	.long	.L4399-.L4345
	.long	.L4398-.L4345
	.long	.L4397-.L4345
	.long	.L4396-.L4345
	.long	.L4395-.L4345
	.long	.L4394-.L4345
	.long	.L4393-.L4345
	.long	.L4392-.L4345
	.long	.L4391-.L4345
	.long	.L4390-.L4345
	.long	.L4389-.L4345
	.long	.L4388-.L4345
	.long	.L4387-.L4345
	.long	.L4386-.L4345
	.long	.L4385-.L4345
	.long	.L4384-.L4345
	.long	.L4383-.L4345
	.long	.L4382-.L4345
	.long	.L4381-.L4345
	.long	.L4380-.L4345
	.long	.L4379-.L4345
	.long	.L4378-.L4345
	.long	.L4377-.L4345
	.long	.L4376-.L4345
	.long	.L4375-.L4345
	.long	.L4374-.L4345
	.long	.L4373-.L4345
	.long	.L4372-.L4345
	.long	.L4371-.L4345
	.long	.L4370-.L4345
	.long	.L4369-.L4345
	.long	.L4368-.L4345
	.long	.L4367-.L4345
	.long	.L4366-.L4345
	.long	.L4365-.L4345
	.long	.L4364-.L4345
	.long	.L4363-.L4345
	.long	.L4362-.L4345
	.long	.L4361-.L4345
	.long	.L4360-.L4345
	.long	.L4359-.L4345
	.long	.L4358-.L4345
	.long	.L4357-.L4345
	.long	.L4356-.L4345
	.long	.L4355-.L4345
	.long	.L4354-.L4345
	.long	.L4353-.L4345
	.long	.L4352-.L4345
	.long	.L4351-.L4345
	.long	.L4349-.L4345
	.long	.L4349-.L4345
	.long	.L4349-.L4345
	.long	.L4349-.L4345
	.long	.L4349-.L4345
	.long	.L4343-.L4345
	.long	.L4343-.L4345
	.long	.L4343-.L4345
	.long	.L4343-.L4345
	.long	.L4343-.L4345
	.long	.L4343-.L4345
	.long	.L4343-.L4345
	.long	.L4343-.L4345
	.long	.L4343-.L4345
	.long	.L4343-.L4345
	.long	.L4343-.L4345
	.long	.L4350-.L4345
	.long	.L4349-.L4345
	.long	.L4348-.L4345
	.long	.L4343-.L4345
	.long	.L4343-.L4345
	.long	.L4343-.L4345
	.long	.L4343-.L4345
	.long	.L4343-.L4345
	.long	.L4343-.L4345
	.long	.L4343-.L4345
	.long	.L4343-.L4345
	.long	.L4343-.L4345
	.long	.L4343-.L4345
	.long	.L4343-.L4345
	.long	.L4343-.L4345
	.long	.L4343-.L4345
	.long	.L4343-.L4345
	.long	.L4343-.L4345
	.long	.L4343-.L4345
	.long	.L4343-.L4345
	.long	.L4343-.L4345
	.long	.L4343-.L4345
	.long	.L4343-.L4345
	.long	.L4343-.L4345
	.long	.L4343-.L4345
	.long	.L4343-.L4345
	.long	.L4343-.L4345
	.long	.L4343-.L4345
	.long	.L4343-.L4345
	.long	.L4343-.L4345
	.long	.L4343-.L4345
	.long	.L4343-.L4345
	.long	.L4343-.L4345
	.long	.L4343-.L4345
	.long	.L4343-.L4345
	.long	.L4343-.L4345
	.long	.L4343-.L4345
	.long	.L4343-.L4345
	.long	.L4343-.L4345
	.long	.L4343-.L4345
	.long	.L4343-.L4345
	.long	.L4343-.L4345
	.long	.L4343-.L4345
	.long	.L4343-.L4345
	.long	.L4347-.L4345
	.long	.L4346-.L4345
	.long	.L4344-.L4345
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE18DecodeFunctionBodyEv,"axG",@progbits,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE18DecodeFunctionBodyEv,comdat
	.p2align 4,,10
	.p2align 3
.L4343:
	movq	80(%r12), %rax
	testq	%rax, %rax
	je	.L4993
	cmpb	$0, 392(%rax)
	je	.L4993
	movl	%r14d, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes14AsmjsSignatureENS1_10WasmOpcodeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L5440
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeEPNS0_9SignatureINS1_9ValueTypeEEE
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	.p2align 4,,10
	.p2align 3
.L4533:
	movq	%r13, 16(%r12)
	cmpq	%r13, %r15
	ja	.L4997
.L4342:
	cmpq	%r13, %r15
	je	.L4325
	cmpq	$0, 56(%r12)
	je	.L5441
.L4325:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5442
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L5439:
	.cfi_restore_state
	movq	128(%r12), %rdi
	movl	%ebx, %esi
	salq	$4, %rsi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L5443
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L4340:
	movq	%rax, -24(%r13)
	movq	112(%r12), %rdx
	leal	-2(%rbx), %edi
	movq	16(%r12), %rcx
	addq	$2, %rdi
	movq	16(%rdx), %rdx
	movzbl	(%rdx), %edx
	movq	%rcx, (%rax)
	movb	%dl, 8(%rax)
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L4341:
	movq	112(%r12), %rdx
	movq	16(%r12), %rsi
	movq	16(%rdx), %rdx
	movzbl	(%rdx,%rax), %ecx
	movq	%rax, %rdx
	addq	$1, %rax
	salq	$4, %rdx
	addq	-24(%r13), %rdx
	movq	%rsi, (%rdx)
	movb	%cl, 8(%rdx)
	cmpq	%rax, %rdi
	jne	.L4341
	jmp	.L4337
.L5441:
	leaq	.LC631(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc
	jmp	.L4325
	.p2align 4,,10
	.p2align 3
.L5438:
	movq	112(%r12), %rax
	movq	16(%r12), %rdx
	movq	16(%rax), %rax
	movzbl	(%rax), %eax
	movq	%rdx, -24(%r13)
	movb	%al, -16(%r13)
	jmp	.L4337
	.p2align 4,,10
	.p2align 3
.L4349:
	movl	%r14d, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes21IsSignExtensionOpcodeENS1_10WasmOpcodeE@PLT
	testb	%al, %al
	je	.L4989
	cmpb	$0, 99(%r12)
	je	.L5444
	movq	104(%r12), %rax
	movb	$1, 11(%rax)
.L4989:
	movl	%r14d, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes14IsAnyRefOpcodeENS1_10WasmOpcodeE@PLT
	testb	%al, %al
	je	.L4991
	cmpb	$0, 95(%r12)
	je	.L5445
	movq	104(%r12), %rax
	movb	$1, 7(%rax)
.L4991:
	movl	%r14d, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes9SignatureENS1_10WasmOpcodeE@PLT
	movl	%r14d, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeEPNS0_9SignatureINS1_9ValueTypeEEE
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4532:
	movq	224(%r12), %rdx
	movl	-84(%rdx), %eax
	salq	$4, %rax
	addq	184(%r12), %rax
	cmpq	%rax, 192(%r12)
	je	.L4790
	movq	%rax, 192(%r12)
.L4790:
	movb	$2, -72(%rdx)
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4531:
	addq	$1, %r13
	jmp	.L4533
.L4530:
	leaq	-256(%rbp), %r15
	leaq	88(%r12), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE1EEC1ERKNS1_12WasmFeaturesEPS3_PKh
	cmpb	$10, -252(%rbp)
	jne	.L4534
	movq	80(%r12), %rax
	movl	-248(%rbp), %ecx
	testq	%rax, %rax
	je	.L5012
	movq	88(%rax), %rdx
	movq	96(%rax), %r8
	movl	%ecx, %esi
	subq	%rdx, %r8
	sarq	$3, %r8
	cmpq	%r8, %rsi
	jnb	.L4535
	movq	(%rdx,%rsi,8), %rax
	movq	%rax, -336(%rbp)
	movq	%rax, -240(%rbp)
	jmp	.L4536
.L4529:
	leaq	-256(%rbp), %r15
	leaq	88(%r12), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE1EEC1ERKNS1_12WasmFeaturesEPS3_PKh
	cmpb	$10, -252(%rbp)
	jne	.L4621
	movq	80(%r12), %rax
	movl	-248(%rbp), %ecx
	testq	%rax, %rax
	je	.L5036
	movq	88(%rax), %rdx
	movq	96(%rax), %r8
	movl	%ecx, %esi
	subq	%rdx, %r8
	sarq	$3, %r8
	cmpq	%r8, %rsi
	jnb	.L4622
	movq	(%rdx,%rsi,8), %rax
	movq	%rax, -336(%rbp)
	movq	%rax, -240(%rbp)
	jmp	.L4623
.L4528:
	leaq	-256(%rbp), %r15
	leaq	88(%r12), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE1EEC1ERKNS1_12WasmFeaturesEPS3_PKh
	cmpb	$10, -252(%rbp)
	jne	.L4638
	movq	80(%r12), %rax
	movl	-248(%rbp), %ecx
	testq	%rax, %rax
	je	.L5042
	movq	88(%rax), %rdx
	movq	96(%rax), %r8
	movl	%ecx, %esi
	subq	%rdx, %r8
	sarq	$3, %r8
	cmpq	%r8, %rsi
	jnb	.L4639
	movq	(%rdx,%rsi,8), %rax
	movq	%rax, -240(%rbp)
.L4638:
	movq	224(%r12), %rcx
	movq	192(%r12), %rdx
	movl	-84(%rcx), %esi
	movq	%rdx, %rax
	subq	184(%r12), %rax
	sarq	$4, %rax
	cmpq	%rax, %rsi
	jnb	.L5446
	movzbl	-8(%rdx), %ecx
	movq	-16(%rdx), %r13
	subq	$16, %rdx
	movq	%rdx, 192(%r12)
	cmpb	$1, %cl
	jne	.L5447
.L4643:
	movq	-240(%rbp), %rax
	movq	%rax, -336(%rbp)
	testq	%rax, %rax
	je	.L4648
	movq	8(%rax), %r14
	leaq	-184(%rbp), %rcx
	leaq	-56(%rbp), %rax
	movq	%rcx, %xmm0
	movq	%rax, -192(%rbp)
	leaq	-208(%rbp), %rdi
	movq	%rcx, %rax
	punpcklqdq	%xmm0, %xmm0
	movslq	%r14d, %r13
	movq	%rcx, -328(%rbp)
	movaps	%xmm0, -208(%rbp)
	cmpq	$8, %r13
	jbe	.L4649
	movq	%r13, %rsi
	call	_ZN2v84base11SmallVectorINS_8internal4wasm9ValueBaseELm8EE4GrowEm
	movq	-208(%rbp), %rax
.L4649:
	salq	$4, %r13
	addq	%rax, %r13
	movq	%r13, -200(%rbp)
	subl	$1, %r14d
	js	.L4651
	movq	%r15, -368(%rbp)
	movslq	%r14d, %r13
	movl	%r14d, %r15d
	jmp	.L4659
	.p2align 4,,10
	.p2align 3
.L5450:
	cmpb	$2, -72(%rcx)
	movq	16(%r12), %r11
	jne	.L5448
.L4653:
	movl	$10, %r14d
.L4655:
	movq	%r13, %rax
	subl	$1, %r15d
	subq	$1, %r13
	salq	$4, %rax
	addq	-208(%rbp), %rax
	movq	%r11, (%rax)
	movb	%r14b, 8(%rax)
	cmpl	$-1, %r15d
	je	.L5449
.L4659:
	movq	224(%r12), %rcx
	movq	192(%r12), %rax
	movl	-84(%rcx), %esi
	movq	%rax, %rdx
	subq	184(%r12), %rdx
	sarq	$4, %rdx
	cmpq	%rdx, %rsi
	jnb	.L5450
	movq	-336(%rbp), %rdi
	movzbl	-8(%rax), %r14d
	subq	$16, %rax
	movq	(%rax), %r11
	movq	16(%rdi), %rdx
	addq	%r13, %rdx
	addq	(%rdi), %rdx
	movzbl	(%rdx), %ecx
	movq	%rax, 192(%r12)
	cmpb	%cl, %r14b
	je	.L4655
	movzbl	%cl, %r9d
	movzbl	%r14b, %edi
	movq	%r11, -344(%rbp)
	movl	%r9d, %esi
	movl	%r9d, -352(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$10, %r14b
	movq	-344(%rbp), %r11
	setne	%sil
	cmpb	$10, %cl
	setne	%dl
	testb	%dl, %sil
	je	.L4655
	cmpb	$1, %al
	je	.L4655
	movq	%r11, -376(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	-376(%rbp), %r11
	leaq	24(%r12), %rdi
	cmpq	24(%r12), %r11
	movq	%rax, -360(%rbp)
	leaq	.LC482(%rip), %rax
	movl	-352(%rbp), %r9d
	movq	%rdi, -384(%rbp)
	movq	%rax, -344(%rbp)
	jnb	.L4656
	movq	%r11, %rsi
	movl	%r9d, -376(%rbp)
	movq	%r11, -352(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movl	-376(%rbp), %r9d
	movq	-352(%rbp), %r11
	movq	%rax, -344(%rbp)
.L4656:
	movl	%r9d, %edi
	movq	%r11, -352(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	16(%r12), %rsi
	cmpq	24(%r12), %rsi
	leaq	.LC482(%rip), %rcx
	movq	-352(%rbp), %r11
	movq	%rax, %r9
	jnb	.L4657
	movq	-384(%rbp), %rdi
	movq	%rax, -376(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-376(%rbp), %r9
	movq	-352(%rbp), %r11
	movq	%rax, %rcx
.L4657:
	pushq	-360(%rbp)
	movl	%r15d, %r8d
	movq	%r11, %rsi
	movq	%r12, %rdi
	pushq	-344(%rbp)
	leaq	.LC531(%rip), %rdx
	xorl	%eax, %eax
	movq	%r11, -344(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	popq	%r8
	movq	-344(%rbp), %r11
	popq	%r9
	jmp	.L4655
.L4527:
	movq	224(%r12), %r14
	cmpq	216(%r12), %r14
	je	.L5451
	movzbl	-88(%r14), %eax
	testb	%al, %al
	jne	.L5452
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE17TypeCheckFallThruEv
	testb	%al, %al
	je	.L5453
	cmpb	$0, -72(%r14)
	movb	$1, -88(%r14)
	jne	.L4665
	movb	$1, -8(%r14)
.L4665:
	leaq	-88(%r14), %rsi
	leaq	-64(%r14), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15PushMergeValuesEPNS1_11ControlBaseINS1_9ValueBaseEEEPNS1_5MergeIS8_EE
	movq	224(%r12), %rax
	cmpb	$0, -160(%rax)
	setne	-72(%r14)
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4523:
	cmpb	$0, 89(%r12)
	je	.L4591
	movq	104(%r12), %rax
	movb	$1, 1(%rax)
	movq	224(%r12), %rcx
	movq	192(%r12), %rdx
	movl	-84(%rcx), %esi
	movq	%rdx, %rax
	subq	184(%r12), %rax
	sarq	$4, %rax
	cmpq	%rax, %rsi
	jb	.L4552
	cmpb	$2, -72(%rcx)
	je	.L4554
	movq	16(%r12), %rsi
	leaq	.LC482(%rip), %rcx
	cmpq	24(%r12), %rsi
	jnb	.L4555
	leaq	24(%r12), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %rsi
	movq	%rax, %rcx
.L4555:
	leaq	.LC530(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L4554
.L4525:
	cmpb	$0, 89(%r12)
	je	.L4591
	movq	104(%r12), %rax
	movb	$1, 1(%rax)
	movq	224(%r12), %r13
	cmpq	216(%r12), %r13
	je	.L4594
	movzbl	-88(%r13), %eax
	cmpb	$4, %al
	je	.L4593
	cmpb	$5, %al
	je	.L5454
.L4594:
	leaq	.LC585(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4521:
	movq	224(%r12), %r14
	cmpq	216(%r12), %r14
	je	.L5455
	movzbl	-88(%r14), %eax
	cmpb	$4, %al
	je	.L5456
	testb	%al, %al
	jne	.L4668
	movl	-64(%r14), %eax
	cmpl	%eax, -32(%r14)
	jne	.L5457
.L4668:
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE17TypeCheckFallThruEv
	testb	%al, %al
	je	.L5458
	movq	224(%r12), %rax
	movq	216(%r12), %rdx
	movq	%rax, %rcx
	subq	%rdx, %rcx
	cmpq	$88, %rcx
	jne	.L4670
	movq	16(%r12), %rcx
	leaq	1(%rcx), %rsi
	cmpq	24(%r12), %rsi
	je	.L4671
	leaq	.LC595(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
	.p2align 4,,10
	.p2align 3
.L4526:
	cmpb	$0, 89(%r12)
	jne	.L4574
	leaq	.LC582(%rip), %rsi
	movq	%r12, %rdi
	movl	$1, %r13d
	call	_ZN2v88internal4wasm7Decoder5errorEPKc
.L4575:
	movq	24(%r12), %r15
	addq	16(%r12), %r13
	jmp	.L4533
.L4522:
	cmpb	$0, 89(%r12)
	jne	.L4597
	leaq	.LC582(%rip), %rsi
	movq	%r12, %rdi
	movl	$1, %r14d
	call	_ZN2v88internal4wasm7Decoder5errorEPKc
.L4598:
	addq	16(%r12), %r14
	movq	24(%r12), %r15
	movq	%r14, %r13
	jmp	.L4533
.L4524:
	cmpb	$0, 89(%r12)
	jne	.L4559
	leaq	.LC582(%rip), %rsi
	movq	%r12, %rdi
	movl	$1, %r14d
	call	_ZN2v88internal4wasm7Decoder5errorEPKc
.L4560:
	addq	16(%r12), %r14
	movq	24(%r12), %r15
	movq	%r14, %r13
	jmp	.L4533
.L4520:
	leaq	1(%r13), %rsi
	leaq	-252(%rbp), %rdx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	.LC587(%rip), %rcx
	movabsq	$3353953467947191203, %r13
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	movq	224(%r12), %rdx
	movl	%eax, %esi
	movq	%rdx, %rax
	subq	216(%r12), %rax
	movl	%esi, -256(%rbp)
	movq	%rsi, %rcx
	sarq	$3, %rax
	imulq	%r13, %rax
	cmpq	%rax, %rsi
	jnb	.L5459
	leaq	(%rsi,%rsi,4), %rax
	movq	%r12, %rdi
	leaq	(%rsi,%rax,2), %rax
	salq	$3, %rax
	subq	%rax, %rdx
	leaq	-88(%rdx), %r14
	xorl	%edx, %edx
	movq	%r14, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15TypeCheckBranchEPNS1_11ControlBaseINS1_9ValueBaseEEEb
	testl	%eax, %eax
	jne	.L4728
	movq	224(%r12), %rax
	subq	216(%r12), %rax
	sarq	$3, %rax
	movl	-256(%rbp), %edx
	imulq	%r13, %rax
	subq	$1, %rax
	cmpq	%rax, %rdx
	je	.L4729
	cmpb	$3, (%r14)
	leaq	24(%r14), %rdx
	leaq	56(%r14), %rax
	cmove	%rdx, %rax
	movb	$1, 24(%rax)
.L4729:
	movl	-252(%rbp), %eax
	movq	%r12, %rdi
	leal	1(%rax), %r13d
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE10EndControlEv
.L4727:
	movq	24(%r12), %r15
	addq	16(%r12), %r13
	jmp	.L4533
.L4519:
	leaq	-252(%rbp), %rdx
	leaq	1(%r13), %rsi
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	.LC587(%rip), %rcx
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	movq	224(%r12), %rcx
	movl	%eax, -256(%rbp)
	movq	192(%r12), %rax
	movl	-84(%rcx), %esi
	movq	%rax, %rdx
	subq	184(%r12), %rdx
	sarq	$4, %rdx
	cmpq	%rdx, %rsi
	jb	.L4732
	cmpb	$2, -72(%rcx)
	movq	16(%r12), %rsi
	jne	.L5460
.L4733:
	cmpq	$0, 56(%r12)
	movl	$1, %r13d
	jne	.L4739
	movq	224(%r12), %rdx
	movl	-256(%rbp), %ecx
	movabsq	$3353953467947191203, %rdi
	movq	%rdx, %rax
	subq	216(%r12), %rax
	sarq	$3, %rax
	imulq	%rdi, %rax
	cmpq	%rax, %rcx
	jnb	.L5461
	leaq	(%rcx,%rcx,4), %rax
	movq	%r12, %rdi
	leaq	(%rcx,%rax,2), %rax
	salq	$3, %rax
	subq	%rax, %rdx
	leaq	-88(%rdx), %r14
	movl	$1, %edx
	movq	%r14, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15TypeCheckBranchEPNS1_11ControlBaseINS1_9ValueBaseEEEb
	testl	%eax, %eax
	jne	.L4741
	cmpb	$3, (%r14)
	leaq	24(%r14), %rdx
	leaq	56(%r14), %rax
	cmove	%rdx, %rax
	movb	$1, 24(%rax)
	movq	16(%r12), %rsi
.L4744:
	movl	-252(%rbp), %eax
	leal	1(%rax), %r13d
.L4739:
	movq	24(%r12), %r15
	addq	%rsi, %r13
	jmp	.L4533
.L4518:
	leaq	1(%r13), %r14
	leaq	-256(%rbp), %r15
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	.LC598(%rip), %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movl	$0, -256(%rbp)
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	movq	%r12, %xmm0
	movq	%r14, %xmm7
	movq	224(%r12), %rcx
	movl	%eax, %r10d
	movl	-256(%rbp), %eax
	punpcklqdq	%xmm7, %xmm0
	movl	$0, -232(%rbp)
	movl	%r10d, -228(%rbp)
	leaq	1(%r13,%rax), %rax
	movaps	%xmm0, -256(%rbp)
	movq	%rax, -240(%rbp)
	movq	192(%r12), %rax
	movl	-84(%rcx), %esi
	movq	%rax, %rdx
	subq	184(%r12), %rdx
	sarq	$4, %rdx
	cmpq	%rdx, %rsi
	jb	.L4745
	cmpb	$2, -72(%rcx)
	movq	16(%r12), %rsi
	jne	.L5462
.L4746:
	movq	56(%r12), %r13
	movl	$1, %r14d
	testq	%r13, %r13
	je	.L5463
.L4752:
	movq	24(%r12), %r15
	leaq	(%rsi,%r14), %r13
	jmp	.L4533
.L4511:
	movq	192(%r12), %rdx
	movq	224(%r12), %rcx
	movq	184(%r12), %r9
	movq	%rdx, %rax
	movl	-84(%rcx), %esi
	subq	%r9, %rax
	sarq	$4, %rax
	cmpq	%rax, %rsi
	jb	.L4678
	cmpb	$2, -72(%rcx)
	jne	.L5464
.L4679:
	cmpb	$2, -72(%rcx)
	movl	$10, %r13d
	movl	$10, %r15d
	jne	.L5465
.L4693:
	movl	$6, %esi
	movl	%r15d, %edi
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	testb	%al, %al
	je	.L5466
.L4699:
	leaq	.LC596(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4510:
	cmpb	$0, 95(%r12)
	jne	.L4700
	leaq	.LC597(%rip), %rsi
	movq	%r12, %rdi
	movl	$1, %r13d
	call	_ZN2v88internal4wasm7Decoder5errorEPKc
.L4701:
	movq	24(%r12), %r15
	addq	16(%r12), %r13
	jmp	.L4533
.L4515:
	movq	88(%r12), %rsi
	leaq	-256(%rbp), %rdi
	movq	%r13, %r8
	movq	%r12, %rcx
	movabsq	$1099511627775, %rdx
	andq	96(%r12), %rdx
	call	_ZN2v88internal4wasm21CallIndirectImmediateILNS1_7Decoder12ValidateFlagE1EEC1ENS1_12WasmFeaturesEPS3_PKh
	movl	-240(%rbp), %eax
	movq	80(%r12), %rdx
	addl	$1, %eax
	movl	%eax, -336(%rbp)
	testq	%rdx, %rdx
	je	.L4906
	movq	184(%rdx), %rsi
	movq	192(%rdx), %rax
	movl	-256(%rbp), %ecx
	subq	%rsi, %rax
	sarq	$4, %rax
	cmpq	%rax, %rcx
	jnb	.L4906
	salq	$4, %rcx
	cmpb	$7, (%rsi,%rcx)
	jne	.L4909
	movq	88(%rdx), %rsi
	movq	96(%rdx), %rax
	movl	-252(%rbp), %edi
	movq	16(%r12), %r15
	subq	%rsi, %rax
	sarq	$3, %rax
	movq	%rdi, %rcx
	cmpq	%rax, %rdi
	jnb	.L5467
	movq	(%rsi,%rdi,8), %rax
	movq	224(%r12), %rcx
	movq	%rax, -248(%rbp)
	movl	-84(%rcx), %esi
	movq	%rax, -344(%rbp)
	movq	192(%r12), %rax
	movq	%rax, %rdx
	subq	184(%r12), %rdx
	sarq	$4, %rdx
	cmpq	%rdx, %rsi
	jb	.L5468
	cmpb	$2, -72(%rcx)
	jne	.L5469
.L4914:
	movq	-344(%rbp), %rax
	leaq	-208(%rbp), %rdi
	testq	%rax, %rax
	je	.L4918
	movq	8(%rax), %r14
	leaq	-184(%rbp), %rcx
	leaq	-56(%rbp), %rax
	movq	%rcx, %xmm0
	movq	%rcx, -328(%rbp)
	punpcklqdq	%xmm0, %xmm0
	movslq	%r14d, %rsi
	movq	%rax, -192(%rbp)
	movaps	%xmm0, -208(%rbp)
	call	_ZN2v84base11SmallVectorINS_8internal4wasm9ValueBaseELm8EE14resize_no_initEm
	subl	$1, %r14d
	js	.L4920
	movslq	%r14d, %r15
	movl	%r14d, %r13d
	jmp	.L4928
.L5471:
	cmpb	$2, -72(%rcx)
	movq	16(%r12), %r11
	jne	.L5470
.L4922:
	movl	$10, %r14d
.L4924:
	movq	%r15, %rax
	subl	$1, %r13d
	subq	$1, %r15
	salq	$4, %rax
	addq	-208(%rbp), %rax
	movq	%r11, (%rax)
	movb	%r14b, 8(%rax)
	cmpl	$-1, %r13d
	je	.L4920
.L4928:
	movq	224(%r12), %rcx
	movq	192(%r12), %rax
	movl	-84(%rcx), %esi
	movq	%rax, %rdx
	subq	184(%r12), %rdx
	sarq	$4, %rdx
	cmpq	%rdx, %rsi
	jnb	.L5471
	movq	-344(%rbp), %rcx
	movzbl	-8(%rax), %r14d
	subq	$16, %rax
	movq	(%rax), %r11
	movq	16(%rcx), %rdx
	addq	%r15, %rdx
	addq	(%rcx), %rdx
	movzbl	(%rdx), %ecx
	movq	%rax, 192(%r12)
	cmpb	%cl, %r14b
	je	.L4924
	movzbl	%cl, %eax
	movzbl	%r14b, %edi
	movq	%r11, -352(%rbp)
	movl	%eax, %esi
	movl	%eax, -360(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	movq	-352(%rbp), %r11
	cmpb	$10, %cl
	setne	%cl
	cmpb	$10, %r14b
	setne	%dl
	testb	%dl, %cl
	je	.L4924
	cmpb	$1, %al
	je	.L4924
	movq	%r11, -376(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	-376(%rbp), %r11
	leaq	24(%r12), %r8
	movq	%rax, -368(%rbp)
	leaq	.LC482(%rip), %rax
	movq	%rax, -352(%rbp)
	cmpq	24(%r12), %r11
	jnb	.L4925
	movq	%r11, %rsi
	movq	%r8, %rdi
	movq	%r11, -384(%rbp)
	movq	%r8, -376(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-384(%rbp), %r11
	movq	-376(%rbp), %r8
	movq	%rax, -352(%rbp)
.L4925:
	movl	-360(%rbp), %edi
	movq	%r11, -376(%rbp)
	movq	%r8, -384(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	16(%r12), %rsi
	cmpq	24(%r12), %rsi
	leaq	.LC482(%rip), %rcx
	movq	-376(%rbp), %r11
	movq	%rax, %r9
	jnb	.L4926
	movq	-384(%rbp), %r8
	movq	%r11, -360(%rbp)
	movq	%rax, -376(%rbp)
	movq	%r8, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-376(%rbp), %r9
	movq	-360(%rbp), %r11
	movq	%rax, %rcx
.L4926:
	pushq	-368(%rbp)
	movq	%r11, %rsi
	movl	%r13d, %r8d
	movq	%r12, %rdi
	pushq	-352(%rbp)
	leaq	.LC531(%rip), %rdx
	xorl	%eax, %eax
	movq	%r11, -352(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	popq	%r9
	movq	-352(%rbp), %r11
	popq	%r10
	jmp	.L4924
.L4507:
	leaq	.LC609(%rip), %rcx
	leaq	-248(%rbp), %rdx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	1(%r13), %rsi
	movb	$0, -252(%rbp)
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	movq	16(%r12), %r9
	movl	%eax, -256(%rbp)
	movl	%eax, %ecx
	movq	120(%r12), %rax
	movq	%r9, %r14
	testq	%rax, %rax
	je	.L4817
	movq	8(%rax), %rdi
	movq	16(%rax), %rdx
	subq	%rdi, %rdx
	cmpl	%edx, %ecx
	jnb	.L4817
	movl	%ecx, %esi
	cmpq	%rdx, %rsi
	jnb	.L5437
	movzbl	(%rdi,%rsi), %eax
	movq	224(%r12), %rcx
	movq	192(%r12), %rdx
	movb	%al, -252(%rbp)
	movl	-84(%rcx), %edi
	movq	%rdx, %rax
	subq	184(%r12), %rax
	sarq	$4, %rax
	cmpq	%rax, %rdi
	jb	.L4821
	cmpb	$2, -72(%rcx)
	jne	.L5472
.L4822:
	movl	$10, %r13d
.L4824:
	leaq	-288(%rbp), %rdx
	leaq	16(%r12), %rsi
	movb	%r13b, -288(%rbp)
	leaq	176(%r12), %rdi
	call	_ZNSt6vectorIN2v88internal4wasm9ValueBaseENS1_13ZoneAllocatorIS3_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_
	movl	-248(%rbp), %eax
	leal	1(%rax), %r13d
.L4819:
	movq	24(%r12), %r15
	addq	16(%r12), %r13
	jmp	.L4533
.L4517:
	movq	224(%r12), %rax
	movq	112(%r12), %rsi
	cmpb	$0, -72(%rax)
	movq	(%rsi), %rdx
	jne	.L4778
	testl	%edx, %edx
	je	.L4779
	movq	192(%r12), %rcx
	movq	%rcx, %r8
	subq	184(%r12), %r8
	sarq	$4, %r8
	subl	-84(%rax), %r8d
	cmpl	%edx, %r8d
	jl	.L5473
	movslq	%edx, %rax
	testl	%edx, %edx
	jle	.L4779
	salq	$4, %rax
	movq	16(%rsi), %r8
	leal	-1(%rdx), %r9d
	xorl	%r14d, %r14d
	subq	%rax, %rcx
	jmp	.L4782
	.p2align 4,,10
	.p2align 3
.L5475:
	movq	%rax, %r14
.L4782:
	movq	%r14, %rdx
	movzbl	(%r8,%r14), %eax
	salq	$4, %rdx
	movzbl	8(%rcx,%rdx), %edx
	cmpb	%dl, %al
	je	.L4781
	movzbl	%al, %r15d
	movzbl	%dl, %edi
	movl	%r15d, %esi
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	testb	%al, %al
	je	.L5474
.L4781:
	leaq	1(%r14), %rax
	cmpq	%r9, %r14
	jne	.L5475
.L4779:
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE10EndControlEv
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4509:
	leaq	.LC609(%rip), %rcx
	leaq	-248(%rbp), %rdx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	1(%r13), %rsi
	movb	$0, -252(%rbp)
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	movl	%eax, -256(%rbp)
	movl	%eax, %ecx
	movq	120(%r12), %rax
	testq	%rax, %rax
	je	.L4800
	movq	8(%rax), %rdi
	movq	16(%rax), %rdx
	subq	%rdi, %rdx
	cmpl	%edx, %ecx
	jnb	.L4800
	movl	%ecx, %esi
	cmpq	%rdx, %rsi
	jnb	.L5437
	movzbl	(%rdi,%rsi), %eax
	leaq	-288(%rbp), %rdx
	leaq	16(%r12), %rsi
	leaq	176(%r12), %rdi
	movb	%al, -252(%rbp)
	movb	%al, -288(%rbp)
	call	_ZNSt6vectorIN2v88internal4wasm9ValueBaseENS1_13ZoneAllocatorIS3_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_
	movl	-248(%rbp), %eax
	leal	1(%rax), %r13d
.L4802:
	movq	24(%r12), %r15
	addq	16(%r12), %r13
	jmp	.L4533
.L4508:
	leaq	.LC609(%rip), %rcx
	leaq	-248(%rbp), %rdx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	1(%r13), %rsi
	movb	$0, -252(%rbp)
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	movq	16(%r12), %r9
	movl	%eax, -256(%rbp)
	movl	%eax, %ecx
	movq	120(%r12), %rax
	movq	%r9, %r14
	testq	%rax, %rax
	je	.L4804
	movq	8(%rax), %rdi
	movq	16(%rax), %rdx
	subq	%rdi, %rdx
	cmpl	%edx, %ecx
	jnb	.L4804
	movl	%ecx, %esi
	cmpq	%rdx, %rsi
	jnb	.L5437
	movzbl	(%rdi,%rsi), %eax
	movq	224(%r12), %rcx
	movq	192(%r12), %rdx
	movb	%al, -252(%rbp)
	movl	-84(%rcx), %edi
	movq	%rdx, %rax
	subq	184(%r12), %rax
	sarq	$4, %rax
	cmpq	%rax, %rdi
	jb	.L4808
	cmpb	$2, -72(%rcx)
	movq	24(%r12), %r15
	jne	.L5476
.L4811:
	movl	-248(%rbp), %eax
	leal	1(%rax), %r13d
.L4806:
	addq	%r9, %r13
	jmp	.L4533
.L4505:
	leaq	1(%r13), %rsi
	leaq	.LC612(%rip), %rcx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	-240(%rbp), %rdx
	movb	$0, -252(%rbp)
	movq	$0, -248(%rbp)
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	movq	16(%r12), %r9
	movq	80(%r12), %rsi
	movl	%eax, -256(%rbp)
	movl	%eax, %ecx
	movl	-240(%rbp), %eax
	movq	%r9, %r14
	leal	1(%rax), %r13d
	testq	%rsi, %rsi
	je	.L4832
	movq	24(%rsi), %rdx
	movq	32(%rsi), %rax
	movl	%ecx, %edi
	subq	%rdx, %rax
	sarq	$5, %rax
	cmpq	%rax, %rdi
	jnb	.L4832
	salq	$5, %rdi
	leaq	(%rdx,%rdi), %rax
	movq	%rax, -248(%rbp)
	movzbl	(%rax), %r10d
	movb	%r10b, -252(%rbp)
	cmpb	$0, 1(%rax)
	je	.L5477
	movq	224(%r12), %rcx
	movq	192(%r12), %rax
	movl	-84(%rcx), %esi
	movq	%rax, %rdx
	subq	184(%r12), %rdx
	sarq	$4, %rdx
	cmpq	%rdx, %rsi
	jnb	.L5478
	movzbl	-8(%rax), %ecx
	movq	-16(%rax), %r15
	subq	$16, %rax
	movq	%rax, 192(%r12)
	cmpb	%r10b, %cl
	je	.L5435
	movzbl	%r10b, %eax
	movzbl	%cl, %edi
	movl	%eax, %esi
	movl	%eax, -336(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$10, %cl
	setne	%cl
	cmpb	$10, %r10b
	setne	%dl
	testb	%dl, %cl
	je	.L5435
	cmpb	$1, %al
	je	.L5435
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	24(%r12), %r10
	movq	%rax, -344(%rbp)
	leaq	.LC482(%rip), %rax
	movq	%rax, -328(%rbp)
	cmpq	24(%r12), %r15
	jnb	.L4842
	movq	%r10, %rdi
	movq	%r15, %rsi
	movq	%r10, -352(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %r14
	movq	-352(%rbp), %r10
	movq	%rax, -328(%rbp)
.L4842:
	movl	-336(%rbp), %edi
	movq	%r10, -352(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	.LC482(%rip), %rcx
	movq	%rax, %r9
	cmpq	24(%r12), %r14
	jnb	.L4843
	movq	-352(%rbp), %r10
	movq	%r14, %rsi
	movq	%rax, -336(%rbp)
	movq	%r10, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-336(%rbp), %r9
	movq	%rax, %rcx
.L4843:
	pushq	-344(%rbp)
	movq	%r15, %rsi
	xorl	%eax, %eax
	xorl	%r8d, %r8d
	pushq	-328(%rbp)
	leaq	.LC531(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	popq	%rax
	movq	16(%r12), %r9
	movq	24(%r12), %r15
	popq	%rdx
	jmp	.L4834
	.p2align 4,,10
	.p2align 3
.L4504:
	cmpb	$0, 95(%r12)
	jne	.L4844
	leaq	.LC597(%rip), %rsi
	movq	%r12, %rdi
	movl	$1, %r13d
	call	_ZN2v88internal4wasm7Decoder5errorEPKc
.L4845:
	movq	24(%r12), %r15
	addq	16(%r12), %r13
	jmp	.L4533
.L4506:
	leaq	1(%r13), %rsi
	leaq	.LC612(%rip), %rcx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	-240(%rbp), %rdx
	movb	$0, -252(%rbp)
	movq	$0, -248(%rbp)
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	movl	%eax, -256(%rbp)
	movl	%eax, %ecx
	movl	-240(%rbp), %eax
	leal	1(%rax), %r13d
	movq	80(%r12), %rax
	testq	%rax, %rax
	je	.L4829
	movq	24(%rax), %rdx
	movq	32(%rax), %rax
	movl	%ecx, %esi
	subq	%rdx, %rax
	sarq	$5, %rax
	cmpq	%rax, %rsi
	jnb	.L4829
	salq	$5, %rsi
	leaq	176(%r12), %rdi
	addq	%rsi, %rdx
	leaq	16(%r12), %rsi
	movq	%rdx, -248(%rbp)
	movzbl	(%rdx), %eax
	leaq	-288(%rbp), %rdx
	movb	%al, -252(%rbp)
	movb	%al, -288(%rbp)
	call	_ZNSt6vectorIN2v88internal4wasm9ValueBaseENS1_13ZoneAllocatorIS3_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_
.L4831:
	movq	24(%r12), %r15
	addq	16(%r12), %r13
	jmp	.L4533
.L4513:
	cmpb	$0, 93(%r12)
	jne	.L4947
	leaq	.LC619(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc
	movq	$1, -336(%rbp)
.L4948:
	movq	-336(%rbp), %r13
	movq	24(%r12), %r15
	addq	16(%r12), %r13
	jmp	.L4533
.L4516:
	leaq	.LC607(%rip), %rcx
	leaq	-240(%rbp), %rdx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movq	$0, -248(%rbp)
	leaq	1(%r13), %rsi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	movl	%eax, -256(%rbp)
	movl	%eax, %ecx
	movl	-240(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -336(%rbp)
	movq	80(%r12), %rax
	testq	%rax, %rax
	je	.L4888
	movq	136(%rax), %rsi
	movq	144(%rax), %rax
	movl	%ecx, %edx
	subq	%rsi, %rax
	sarq	$5, %rax
	cmpq	%rax, %rdx
	jnb	.L4888
	salq	$5, %rdx
	movq	(%rsi,%rdx), %r13
	movq	%r13, -248(%rbp)
	testq	%r13, %r13
	je	.L5479
	movq	8(%r13), %r14
	leaq	-184(%rbp), %rax
	leaq	-208(%rbp), %rdi
	movq	%rax, %xmm0
	movq	%rax, -328(%rbp)
	leaq	-56(%rbp), %rax
	punpcklqdq	%xmm0, %xmm0
	movslq	%r14d, %rsi
	movq	%rax, -192(%rbp)
	movaps	%xmm0, -208(%rbp)
	call	_ZN2v84base11SmallVectorINS_8internal4wasm9ValueBaseELm8EE14resize_no_initEm
	subl	$1, %r14d
	js	.L4893
	movq	%r13, -344(%rbp)
	movslq	%r14d, %r15
	movl	%r14d, %r13d
	jmp	.L4901
.L5481:
	cmpb	$2, -72(%rcx)
	movq	16(%r12), %r11
	jne	.L5480
.L4895:
	movl	$10, %r14d
.L4897:
	movq	%r15, %rax
	subl	$1, %r13d
	subq	$1, %r15
	salq	$4, %rax
	addq	-208(%rbp), %rax
	movq	%r11, (%rax)
	movb	%r14b, 8(%rax)
	cmpl	$-1, %r13d
	je	.L4893
.L4901:
	movq	224(%r12), %rcx
	movq	192(%r12), %rax
	movl	-84(%rcx), %esi
	movq	%rax, %rdx
	subq	184(%r12), %rdx
	sarq	$4, %rdx
	cmpq	%rdx, %rsi
	jnb	.L5481
	movq	-344(%rbp), %rdi
	movzbl	-8(%rax), %r14d
	subq	$16, %rax
	movq	(%rax), %r11
	movq	16(%rdi), %rdx
	addq	%r15, %rdx
	addq	(%rdi), %rdx
	movzbl	(%rdx), %ecx
	movq	%rax, 192(%r12)
	cmpb	%cl, %r14b
	je	.L4897
	movzbl	%cl, %eax
	movzbl	%r14b, %edi
	movq	%r11, -352(%rbp)
	movl	%eax, %esi
	movl	%eax, -360(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	movq	-352(%rbp), %r11
	cmpb	$10, %cl
	setne	%cl
	cmpb	$10, %r14b
	setne	%dl
	testb	%dl, %cl
	je	.L4897
	cmpb	$1, %al
	je	.L4897
	movq	%r11, -376(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	-376(%rbp), %r11
	leaq	24(%r12), %r8
	movq	%rax, -368(%rbp)
	leaq	.LC482(%rip), %rax
	movq	%rax, -352(%rbp)
	cmpq	24(%r12), %r11
	jnb	.L4898
	movq	%r11, %rsi
	movq	%r8, %rdi
	movq	%r11, -384(%rbp)
	movq	%r8, -376(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-384(%rbp), %r11
	movq	-376(%rbp), %r8
	movq	%rax, -352(%rbp)
.L4898:
	movl	-360(%rbp), %edi
	movq	%r11, -376(%rbp)
	movq	%r8, -384(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	16(%r12), %rsi
	cmpq	24(%r12), %rsi
	leaq	.LC482(%rip), %rcx
	movq	-376(%rbp), %r11
	movq	%rax, %r9
	jnb	.L4899
	movq	-384(%rbp), %r8
	movq	%r11, -360(%rbp)
	movq	%rax, -376(%rbp)
	movq	%r8, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-376(%rbp), %r9
	movq	-360(%rbp), %r11
	movq	%rax, %rcx
.L4899:
	pushq	-368(%rbp)
	movq	%r11, %rsi
	xorl	%eax, %eax
	movl	%r13d, %r8d
	pushq	-352(%rbp)
	leaq	.LC531(%rip), %rdx
	movq	%r12, %rdi
	movq	%r11, -352(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	popq	%rax
	movq	-352(%rbp), %r11
	popq	%rdx
	jmp	.L4897
.L4514:
	cmpb	$0, 93(%r12)
	jne	.L4933
	leaq	.LC619(%rip), %rsi
	movq	%r12, %rdi
	movl	$1, %r13d
	call	_ZN2v88internal4wasm7Decoder5errorEPKc
.L4934:
	movq	24(%r12), %r15
	addq	16(%r12), %r13
	jmp	.L4533
.L4512:
	movq	224(%r12), %rcx
	movq	192(%r12), %rdx
	movl	-84(%rcx), %esi
	movq	%rdx, %rax
	subq	184(%r12), %rax
	sarq	$4, %rax
	cmpq	%rax, %rsi
	jb	.L4827
	cmpb	$2, -72(%rcx)
	jne	.L4828
	addq	$1, %r13
	jmp	.L4533
.L4503:
	cmpb	$0, 95(%r12)
	jne	.L4856
	leaq	.LC597(%rip), %rsi
	movq	%r12, %rdi
	movl	$1, %r13d
	call	_ZN2v88internal4wasm7Decoder5errorEPKc
	movq	24(%r12), %r15
	movq	16(%r12), %rsi
.L4857:
	addq	%rsi, %r13
	jmp	.L4533
.L4487:
	xorl	%edx, %edx
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE14DecodeStoreMemENS1_9StoreTypeEi
	movq	24(%r12), %r15
	leal	1(%rax), %r13d
	addq	16(%r12), %r13
	jmp	.L4533
.L4495:
	xorl	%edx, %edx
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE13DecodeLoadMemENS1_8LoadTypeEi
	movq	24(%r12), %r15
	leal	1(%rax), %r13d
	addq	16(%r12), %r13
	jmp	.L4533
.L4494:
	xorl	%edx, %edx
	movl	$6, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE13DecodeLoadMemENS1_8LoadTypeEi
	movq	24(%r12), %r15
	leal	1(%rax), %r13d
	addq	16(%r12), %r13
	jmp	.L4533
.L4493:
	xorl	%edx, %edx
	movl	$7, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE13DecodeLoadMemENS1_8LoadTypeEi
	movq	24(%r12), %r15
	leal	1(%rax), %r13d
	addq	16(%r12), %r13
	jmp	.L4533
.L4492:
	xorl	%edx, %edx
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE13DecodeLoadMemENS1_8LoadTypeEi
	movq	24(%r12), %r15
	leal	1(%rax), %r13d
	addq	16(%r12), %r13
	jmp	.L4533
.L4491:
	xorl	%edx, %edx
	movl	$9, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE13DecodeLoadMemENS1_8LoadTypeEi
	movq	24(%r12), %r15
	leal	1(%rax), %r13d
	addq	16(%r12), %r13
	jmp	.L4533
.L4490:
	xorl	%edx, %edx
	movl	$10, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE13DecodeLoadMemENS1_8LoadTypeEi
	movq	24(%r12), %r15
	leal	1(%rax), %r13d
	addq	16(%r12), %r13
	jmp	.L4533
.L4489:
	xorl	%edx, %edx
	movl	$11, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE13DecodeLoadMemENS1_8LoadTypeEi
	movq	24(%r12), %r15
	leal	1(%rax), %r13d
	addq	16(%r12), %r13
	jmp	.L4533
.L4488:
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE14DecodeStoreMemENS1_9StoreTypeEi
	movq	24(%r12), %r15
	leal	1(%rax), %r13d
	addq	16(%r12), %r13
	jmp	.L4533
.L4499:
	xorl	%edx, %edx
	movl	$13, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE13DecodeLoadMemENS1_8LoadTypeEi
	movq	24(%r12), %r15
	leal	1(%rax), %r13d
	addq	16(%r12), %r13
	jmp	.L4533
.L4498:
	xorl	%edx, %edx
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE13DecodeLoadMemENS1_8LoadTypeEi
	movq	24(%r12), %r15
	leal	1(%rax), %r13d
	addq	16(%r12), %r13
	jmp	.L4533
.L4497:
	xorl	%edx, %edx
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE13DecodeLoadMemENS1_8LoadTypeEi
	movq	24(%r12), %r15
	leal	1(%rax), %r13d
	addq	16(%r12), %r13
	jmp	.L4533
.L4496:
	xorl	%edx, %edx
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE13DecodeLoadMemENS1_8LoadTypeEi
	movq	24(%r12), %r15
	leal	1(%rax), %r13d
	addq	16(%r12), %r13
	jmp	.L4533
.L4501:
	xorl	%edx, %edx
	movl	$5, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE13DecodeLoadMemENS1_8LoadTypeEi
	movq	24(%r12), %r15
	leal	1(%rax), %r13d
	addq	16(%r12), %r13
	jmp	.L4533
.L4500:
	xorl	%edx, %edx
	movl	$12, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE13DecodeLoadMemENS1_8LoadTypeEi
	movq	24(%r12), %r15
	leal	1(%rax), %r13d
	addq	16(%r12), %r13
	jmp	.L4533
.L4502:
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE13DecodeLoadMemENS1_8LoadTypeEi
	movq	24(%r12), %r15
	leal	1(%rax), %r13d
	addq	16(%r12), %r13
	jmp	.L4533
.L4479:
	movq	80(%r12), %rax
	cmpb	$0, 18(%rax)
	je	.L5482
	movq	%r13, %rdx
	leaq	-256(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm20MemoryIndexImmediateILNS1_7Decoder12ValidateFlagE1EEC1EPS3_PKh
	leaq	-288(%rbp), %rdx
	leaq	16(%r12), %rsi
	movb	$1, -288(%rbp)
	leaq	176(%r12), %rdi
	call	_ZNSt6vectorIN2v88internal4wasm9ValueBaseENS1_13ZoneAllocatorIS3_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_
	movl	-252(%rbp), %eax
	leal	1(%rax), %r13d
.L4887:
	movq	24(%r12), %r15
	addq	16(%r12), %r13
	jmp	.L4533
.L4478:
	movq	80(%r12), %rax
	cmpb	$0, 18(%rax)
	je	.L5483
	movq	%r13, %rdx
	leaq	-256(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm20MemoryIndexImmediateILNS1_7Decoder12ValidateFlagE1EEC1EPS3_PKh
	movl	-252(%rbp), %eax
	leal	1(%rax), %r13d
	movq	80(%r12), %rax
	cmpb	$0, 392(%rax)
	jne	.L5484
	movq	224(%r12), %rcx
	movq	192(%r12), %rax
	movl	-84(%rcx), %esi
	movq	%rax, %rdx
	subq	184(%r12), %rdx
	sarq	$4, %rdx
	cmpq	%rdx, %rsi
	jb	.L4879
	cmpb	$2, -72(%rcx)
	je	.L4881
	movq	16(%r12), %rsi
	leaq	.LC482(%rip), %rcx
	cmpq	24(%r12), %rsi
	jnb	.L4882
	leaq	24(%r12), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %rsi
	movq	%rax, %rcx
.L4882:
	leaq	.LC530(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L4881
	.p2align 4,,10
	.p2align 3
.L4477:
	leaq	1(%r13), %rsi
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	-252(%rbp), %rdx
	leaq	.LC603(%rip), %rcx
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIiLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	leaq	-288(%rbp), %rdx
	leaq	16(%r12), %rsi
	movb	$1, -288(%rbp)
	leaq	176(%r12), %rdi
	movl	%eax, -256(%rbp)
	call	_ZNSt6vectorIN2v88internal4wasm9ValueBaseENS1_13ZoneAllocatorIS3_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_
	movl	-252(%rbp), %eax
	movq	24(%r12), %r15
	leal	1(%rax), %r13d
	addq	16(%r12), %r13
	jmp	.L4533
.L4476:
	leaq	1(%r13), %rsi
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	-248(%rbp), %rdx
	leaq	.LC604(%rip), %rcx
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIlLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	leaq	-288(%rbp), %rdx
	leaq	16(%r12), %rsi
	movb	$2, -288(%rbp)
	leaq	176(%r12), %rdi
	movq	%rax, -256(%rbp)
	call	_ZNSt6vectorIN2v88internal4wasm9ValueBaseENS1_13ZoneAllocatorIS3_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_
	movl	-248(%rbp), %eax
	movq	24(%r12), %r15
	leal	1(%rax), %r13d
	addq	16(%r12), %r13
	jmp	.L4533
.L4475:
	leaq	1(%r13), %rsi
	cmpq	%rsi, %r15
	jb	.L4791
	subq	%rsi, %r15
	cmpl	$3, %r15d
	jbe	.L4791
.L4792:
	leaq	-256(%rbp), %rdx
	leaq	16(%r12), %rsi
	movb	$3, -256(%rbp)
	leaq	176(%r12), %rdi
	call	_ZNSt6vectorIN2v88internal4wasm9ValueBaseENS1_13ZoneAllocatorIS3_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	5(%rax), %r13
	jmp	.L4533
.L4474:
	leaq	1(%r13), %rsi
	cmpq	%rsi, %r15
	jb	.L4793
	subq	%rsi, %r15
	cmpl	$7, %r15d
	jbe	.L4793
.L4794:
	leaq	-256(%rbp), %rdx
	leaq	16(%r12), %rsi
	movb	$4, -256(%rbp)
	leaq	176(%r12), %rdi
	call	_ZNSt6vectorIN2v88internal4wasm9ValueBaseENS1_13ZoneAllocatorIS3_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	9(%rax), %r13
	jmp	.L4533
.L4473:
	movl	$1, %ecx
	movl	$1, %edx
	movl	$69, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4472:
	movl	$1, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	movl	$70, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4483:
	xorl	%edx, %edx
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE14DecodeStoreMemENS1_9StoreTypeEi
	movq	24(%r12), %r15
	leal	1(%rax), %r13d
	addq	16(%r12), %r13
	jmp	.L4533
.L4482:
	xorl	%edx, %edx
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE14DecodeStoreMemENS1_9StoreTypeEi
	movq	24(%r12), %r15
	leal	1(%rax), %r13d
	addq	16(%r12), %r13
	jmp	.L4533
.L4481:
	xorl	%edx, %edx
	movl	$5, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE14DecodeStoreMemENS1_9StoreTypeEi
	movq	24(%r12), %r15
	leal	1(%rax), %r13d
	addq	16(%r12), %r13
	jmp	.L4533
.L4480:
	xorl	%edx, %edx
	movl	$6, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE14DecodeStoreMemENS1_9StoreTypeEi
	movq	24(%r12), %r15
	leal	1(%rax), %r13d
	addq	16(%r12), %r13
	jmp	.L4533
.L4485:
	xorl	%edx, %edx
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE14DecodeStoreMemENS1_9StoreTypeEi
	movq	24(%r12), %r15
	leal	1(%rax), %r13d
	addq	16(%r12), %r13
	jmp	.L4533
.L4484:
	xorl	%edx, %edx
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE14DecodeStoreMemENS1_9StoreTypeEi
	movq	24(%r12), %r15
	leal	1(%rax), %r13d
	addq	16(%r12), %r13
	jmp	.L4533
.L4486:
	xorl	%edx, %edx
	movl	$7, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE14DecodeStoreMemENS1_9StoreTypeEi
	movq	24(%r12), %r15
	leal	1(%rax), %r13d
	addq	16(%r12), %r13
	jmp	.L4533
.L4471:
	movl	$1, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	movl	$71, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4470:
	movl	$1, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	movl	$72, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4469:
	movl	$1, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	movl	$73, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4468:
	movl	$1, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	movl	$74, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4467:
	movl	$1, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	movl	$75, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4466:
	movl	$1, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	movl	$76, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4465:
	movl	$1, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	movl	$77, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4464:
	movl	$1, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	movl	$78, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4463:
	movl	$1, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	movl	$79, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4462:
	movl	$2, %ecx
	movl	$1, %edx
	movl	$80, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4461:
	movl	$2, %r8d
	movl	$2, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	movl	$81, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4460:
	movl	$2, %r8d
	movl	$2, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	movl	$82, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4459:
	movl	$2, %r8d
	movl	$2, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	movl	$83, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4458:
	movl	$2, %r8d
	movl	$2, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	movl	$84, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4457:
	movl	$2, %r8d
	movl	$2, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	movl	$85, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4456:
	movl	$2, %r8d
	movl	$2, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	movl	$86, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4455:
	movl	$2, %r8d
	movl	$2, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	movl	$87, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4454:
	movl	$2, %r8d
	movl	$2, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	movl	$88, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4453:
	movl	$2, %r8d
	movl	$2, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	movl	$89, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4452:
	movl	$2, %r8d
	movl	$2, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	movl	$90, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4451:
	movl	$3, %r8d
	movl	$3, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	movl	$91, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4450:
	movl	$3, %r8d
	movl	$3, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	movl	$92, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4449:
	movl	$3, %r8d
	movl	$3, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	movl	$93, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4448:
	movl	$3, %r8d
	movl	$3, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	movl	$94, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4447:
	movl	$3, %r8d
	movl	$3, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	movl	$95, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4446:
	movl	$3, %r8d
	movl	$3, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	movl	$96, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4445:
	movl	$4, %r8d
	movl	$4, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	movl	$97, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4444:
	movl	$4, %r8d
	movl	$4, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	movl	$98, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4443:
	movl	$4, %r8d
	movl	$4, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	movl	$99, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4442:
	movl	$4, %r8d
	movl	$4, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	movl	$100, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4441:
	movl	$4, %r8d
	movl	$4, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	movl	$101, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4440:
	movl	$4, %r8d
	movl	$4, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	movl	$102, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4439:
	movl	$1, %ecx
	movl	$1, %edx
	movl	$103, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4438:
	movl	$1, %ecx
	movl	$1, %edx
	movl	$104, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4437:
	movl	$1, %ecx
	movl	$1, %edx
	movl	$105, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4436:
	movl	$1, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	movl	$106, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4435:
	movl	$1, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	movl	$107, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4434:
	movl	$1, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	movl	$108, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4433:
	movl	$1, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	movl	$109, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4432:
	movl	$1, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	movl	$110, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4431:
	movl	$1, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	movl	$111, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4430:
	movl	$1, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	movl	$112, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4429:
	movl	$1, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	movl	$113, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4428:
	movl	$1, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	movl	$114, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4427:
	movl	$1, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	movl	$115, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4426:
	movl	$1, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	movl	$116, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4425:
	movl	$1, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	movl	$117, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4424:
	movl	$1, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	movl	$118, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4423:
	movl	$1, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	movl	$119, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4422:
	movl	$1, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	movl	$120, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4421:
	movl	$2, %ecx
	movl	$2, %edx
	movl	$121, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4420:
	movl	$2, %ecx
	movl	$2, %edx
	movl	$122, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4419:
	movl	$2, %ecx
	movl	$2, %edx
	movl	$123, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4418:
	movl	$2, %r8d
	movl	$2, %ecx
	movl	$2, %edx
	movq	%r12, %rdi
	movl	$124, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4417:
	movl	$2, %r8d
	movl	$2, %ecx
	movl	$2, %edx
	movq	%r12, %rdi
	movl	$125, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4416:
	movl	$2, %r8d
	movl	$2, %ecx
	movl	$2, %edx
	movq	%r12, %rdi
	movl	$126, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4415:
	movl	$2, %r8d
	movl	$2, %ecx
	movl	$2, %edx
	movq	%r12, %rdi
	movl	$127, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4414:
	movl	$2, %r8d
	movl	$2, %ecx
	movl	$2, %edx
	movq	%r12, %rdi
	movl	$128, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4413:
	movl	$2, %r8d
	movl	$2, %ecx
	movl	$2, %edx
	movq	%r12, %rdi
	movl	$129, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4412:
	movl	$2, %r8d
	movl	$2, %ecx
	movl	$2, %edx
	movq	%r12, %rdi
	movl	$130, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4411:
	movl	$2, %r8d
	movl	$2, %ecx
	movl	$2, %edx
	movq	%r12, %rdi
	movl	$131, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4410:
	movl	$2, %r8d
	movl	$2, %ecx
	movl	$2, %edx
	movq	%r12, %rdi
	movl	$132, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4409:
	movl	$2, %r8d
	movl	$2, %ecx
	movl	$2, %edx
	movq	%r12, %rdi
	movl	$133, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4408:
	movl	$2, %r8d
	movl	$2, %ecx
	movl	$2, %edx
	movq	%r12, %rdi
	movl	$134, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4407:
	movl	$2, %r8d
	movl	$2, %ecx
	movl	$2, %edx
	movq	%r12, %rdi
	movl	$135, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4406:
	movl	$2, %r8d
	movl	$2, %ecx
	movl	$2, %edx
	movq	%r12, %rdi
	movl	$136, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4405:
	movl	$2, %r8d
	movl	$2, %ecx
	movl	$2, %edx
	movq	%r12, %rdi
	movl	$137, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4404:
	movl	$2, %r8d
	movl	$2, %ecx
	movl	$2, %edx
	movq	%r12, %rdi
	movl	$138, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4403:
	movl	$3, %ecx
	movl	$3, %edx
	movl	$139, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4402:
	movl	$3, %ecx
	movl	$3, %edx
	movl	$140, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4401:
	movl	$3, %ecx
	movl	$3, %edx
	movl	$141, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4400:
	movl	$3, %ecx
	movl	$3, %edx
	movl	$142, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4399:
	movl	$3, %ecx
	movl	$3, %edx
	movl	$143, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4398:
	movl	$3, %ecx
	movl	$3, %edx
	movl	$144, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4397:
	movl	$3, %ecx
	movl	$3, %edx
	movl	$145, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4396:
	movl	$3, %r8d
	movl	$3, %ecx
	movl	$3, %edx
	movq	%r12, %rdi
	movl	$146, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4395:
	movl	$3, %r8d
	movl	$3, %ecx
	movl	$3, %edx
	movq	%r12, %rdi
	movl	$147, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4394:
	movl	$3, %r8d
	movl	$3, %ecx
	movl	$3, %edx
	movq	%r12, %rdi
	movl	$148, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4393:
	movl	$3, %r8d
	movl	$3, %ecx
	movl	$3, %edx
	movq	%r12, %rdi
	movl	$149, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4392:
	movl	$3, %r8d
	movl	$3, %ecx
	movl	$3, %edx
	movq	%r12, %rdi
	movl	$150, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4391:
	movl	$3, %r8d
	movl	$3, %ecx
	movl	$3, %edx
	movq	%r12, %rdi
	movl	$151, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4390:
	movl	$3, %r8d
	movl	$3, %ecx
	movl	$3, %edx
	movq	%r12, %rdi
	movl	$152, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4389:
	movl	$4, %ecx
	movl	$4, %edx
	movl	$153, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4388:
	movl	$4, %ecx
	movl	$4, %edx
	movl	$154, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4387:
	movl	$4, %ecx
	movl	$4, %edx
	movl	$155, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4386:
	movl	$4, %ecx
	movl	$4, %edx
	movl	$156, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4385:
	movl	$4, %ecx
	movl	$4, %edx
	movl	$157, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4384:
	movl	$4, %ecx
	movl	$4, %edx
	movl	$158, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4383:
	movl	$4, %ecx
	movl	$4, %edx
	movl	$159, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4382:
	movl	$4, %r8d
	movl	$4, %ecx
	movl	$4, %edx
	movq	%r12, %rdi
	movl	$160, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4381:
	movl	$4, %r8d
	movl	$4, %ecx
	movl	$4, %edx
	movq	%r12, %rdi
	movl	$161, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4380:
	movl	$4, %r8d
	movl	$4, %ecx
	movl	$4, %edx
	movq	%r12, %rdi
	movl	$162, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4379:
	movl	$4, %r8d
	movl	$4, %ecx
	movl	$4, %edx
	movq	%r12, %rdi
	movl	$163, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4378:
	movl	$4, %r8d
	movl	$4, %ecx
	movl	$4, %edx
	movq	%r12, %rdi
	movl	$164, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4377:
	movl	$4, %r8d
	movl	$4, %ecx
	movl	$4, %edx
	movq	%r12, %rdi
	movl	$165, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4376:
	movl	$4, %r8d
	movl	$4, %ecx
	movl	$4, %edx
	movq	%r12, %rdi
	movl	$166, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_S8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4375:
	movl	$2, %ecx
	movl	$1, %edx
	movl	$167, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4374:
	movl	$3, %ecx
	movl	$1, %edx
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4373:
	movl	$3, %ecx
	movl	$1, %edx
	movl	$169, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4372:
	movl	$4, %ecx
	movl	$1, %edx
	movl	$170, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4371:
	movl	$4, %ecx
	movl	$1, %edx
	movl	$171, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4370:
	movl	$1, %ecx
	movl	$2, %edx
	movl	$172, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4369:
	movl	$1, %ecx
	movl	$2, %edx
	movl	$173, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4368:
	movl	$3, %ecx
	movl	$2, %edx
	movl	$174, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4367:
	movl	$3, %ecx
	movl	$2, %edx
	movl	$175, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4366:
	movl	$4, %ecx
	movl	$2, %edx
	movl	$176, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4365:
	movl	$4, %ecx
	movl	$2, %edx
	movl	$177, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4364:
	movl	$1, %ecx
	movl	$3, %edx
	movl	$178, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4363:
	movl	$1, %ecx
	movl	$3, %edx
	movl	$179, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4362:
	movl	$2, %ecx
	movl	$3, %edx
	movl	$180, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4361:
	movl	$2, %ecx
	movl	$3, %edx
	movl	$181, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4360:
	movl	$4, %ecx
	movl	$3, %edx
	movl	$182, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4359:
	movl	$1, %ecx
	movl	$4, %edx
	movl	$183, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4358:
	movl	$1, %ecx
	movl	$4, %edx
	movl	$184, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4357:
	movl	$2, %ecx
	movl	$4, %edx
	movl	$185, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4348:
	cmpb	$0, 95(%r12)
	jne	.L4796
	leaq	.LC597(%rip), %rsi
	movq	%r12, %rdi
	movl	$1, %r13d
	call	_ZN2v88internal4wasm7Decoder5errorEPKc
.L4797:
	movq	24(%r12), %r15
	addq	16(%r12), %r13
	jmp	.L4533
.L4347:
	leaq	1(%r13), %rsi
	cmpq	%r15, %rsi
	ja	.L4975
	cmpl	%esi, %r15d
	je	.L4975
	movzbl	1(%r13), %eax
	orb	$-4, %ah
	movl	%eax, %esi
	cmpl	$64519, %eax
	jle	.L4976
	subl	$64527, %eax
	cmpl	$2, %eax
	ja	.L4980
	cmpb	$0, 95(%r12)
	je	.L5485
	movq	104(%r12), %rax
	movb	$1, 7(%rax)
	jmp	.L4979
.L4344:
	cmpb	$0, 90(%r12)
	je	.L5486
	movq	104(%r12), %rax
	movb	$1, 2(%rax)
	movq	16(%r12), %rdx
	movq	24(%r12), %rax
	leaq	1(%rdx), %rsi
	cmpq	%rax, %rsi
	ja	.L4987
	cmpl	%esi, %eax
	je	.L4987
	movzbl	1(%rdx), %esi
	orl	$65024, %esi
.L4988:
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE18DecodeAtomicOpcodeENS1_10WasmOpcodeE
	movq	24(%r12), %r15
	leal	2(%rax), %r13d
	addq	16(%r12), %r13
	jmp	.L4533
.L4346:
	cmpb	$0, 91(%r12)
	je	.L5487
	movq	104(%r12), %rax
	movb	$1, 3(%rax)
	movq	16(%r12), %rdx
	movq	24(%r12), %rax
	leaq	1(%rdx), %rsi
	cmpq	%rax, %rsi
	ja	.L4984
	cmpl	%esi, %eax
	je	.L4984
	movzbl	1(%rdx), %esi
	orl	$64768, %esi
.L4985:
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16DecodeSimdOpcodeENS1_10WasmOpcodeE
	movq	24(%r12), %r15
	leal	2(%rax), %r13d
	addq	16(%r12), %r13
	jmp	.L4533
.L4350:
	cmpb	$0, 95(%r12)
	jne	.L4795
	leaq	.LC597(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4356:
	movl	$2, %ecx
	movl	$4, %edx
	movl	$186, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4352:
	movl	$1, %ecx
	movl	$3, %edx
	movl	$190, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4351:
	movl	$2, %ecx
	movl	$4, %edx
	movl	$191, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4354:
	movl	$3, %ecx
	movl	$1, %edx
	movl	$188, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4353:
	movl	$4, %ecx
	movl	$2, %edx
	movl	$189, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4355:
	movl	$3, %ecx
	movl	$4, %edx
	movl	$187, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES8_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4327:
	movabsq	$3353953467947191203, %rcx
	movq	%rbx, %r8
	subq	%r15, %r8
	movq	%r8, %rax
	sarq	$3, %rax
	imulq	%rcx, %rax
	cmpq	$24403223, %rax
	je	.L5488
	testq	%rax, %rax
	je	.L5009
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L5489
	movl	$2147483624, %esi
	movl	$2147483624, %r13d
.L4330:
	movq	208(%r12), %rdi
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	subq	%rcx, %rax
	cmpq	%rsi, %rax
	jb	.L5490
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L4333:
	leaq	(%rcx,%r13), %rsi
	leaq	88(%rcx), %r13
.L4331:
	movq	16(%r12), %rax
	leaq	(%rcx,%r8), %rdi
	pxor	%xmm0, %xmm0
	movb	$2, (%rdi)
	movq	%rax, 8(%rdi)
	movl	%edx, %eax
	xorl	$1, %eax
	movl	%r14d, 4(%rdi)
	movb	%al, 48(%rdi)
	movb	%dl, 16(%rdi)
	andb	$1, 48(%rdi)
	movl	$0, 24(%rdi)
	movl	$0, 56(%rdi)
	movb	$0, 80(%rdi)
	movups	%xmm0, 32(%rdi)
	movups	%xmm0, 64(%rdi)
	cmpq	%r15, %rbx
	je	.L4334
	movq	%r15, %rax
	movq	%rcx, %rdx
	.p2align 4,,10
	.p2align 3
.L4335:
	movdqu	(%rax), %xmm1
	addq	$88, %rax
	addq	$88, %rdx
	movups	%xmm1, -88(%rdx)
	movdqu	-72(%rax), %xmm2
	movups	%xmm2, -72(%rdx)
	movdqu	-56(%rax), %xmm3
	movups	%xmm3, -56(%rdx)
	movdqu	-40(%rax), %xmm4
	movups	%xmm4, -40(%rdx)
	movdqu	-24(%rax), %xmm5
	movups	%xmm5, -24(%rdx)
	movq	-8(%rax), %rdi
	movq	%rdi, -8(%rdx)
	cmpq	%rax, %rbx
	jne	.L4335
	movabsq	$1048110458733497251, %rdx
	leaq	-88(%rbx), %rax
	subq	%r15, %rax
	shrq	$3, %rax
	imulq	%rdx, %rax
	movabsq	$2305843009213693951, %rdx
	andq	%rdx, %rax
	addq	$2, %rax
	leaq	(%rax,%rax,4), %rdx
	leaq	(%rax,%rdx,2), %rax
	leaq	(%rcx,%rax,8), %r13
.L4334:
	movq	%rcx, %xmm0
	movq	%r13, %xmm6
	movq	%rsi, 232(%r12)
	punpcklqdq	%xmm6, %xmm0
	movups	%xmm0, 216(%r12)
	jmp	.L4328
.L5489:
	testq	%rcx, %rcx
	jne	.L5491
	movl	$88, %r13d
	xorl	%esi, %esi
	xorl	%ecx, %ecx
	jmp	.L4331
.L4591:
	leaq	.LC582(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4856:
	movq	104(%r12), %rax
	leaq	.LC539(%rip), %rcx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	-252(%rbp), %rdx
	movb	$1, 7(%rax)
	movabsq	$4294967296, %rax
	movq	%rax, -256(%rbp)
	movq	16(%r12), %rax
	leaq	1(%rax), %rsi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	movq	16(%r12), %rsi
	movl	%eax, -256(%rbp)
	movl	%eax, %ecx
	movl	-252(%rbp), %eax
	movq	%rsi, %r14
	addl	$1, %eax
	movl	%eax, -328(%rbp)
	movq	80(%r12), %rax
	testq	%rax, %rax
	je	.L4858
	movq	184(%rax), %rdi
	movq	192(%rax), %rax
	movl	%ecx, %edx
	subq	%rdi, %rax
	sarq	$4, %rax
	cmpq	%rax, %rdx
	jnb	.L4858
	movq	192(%r12), %rcx
	movq	224(%r12), %r9
	movq	184(%r12), %r10
	movq	%rcx, %rax
	movl	-84(%r9), %r11d
	subq	%r10, %rax
	sarq	$4, %rax
	cmpq	%rax, %r11
	jb	.L4860
	cmpb	$2, -72(%r9)
	jne	.L5492
.L4861:
	cmpb	$2, -72(%r9)
	movq	24(%r12), %r15
	movq	%r14, %rsi
	jne	.L5493
.L4870:
	movl	-328(%rbp), %r13d
	jmp	.L4857
.L4796:
	movq	104(%r12), %rax
	leaq	.LC607(%rip), %rcx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	-252(%rbp), %rdx
	movb	$1, 7(%rax)
	movabsq	$4294967296, %rax
	movq	%rax, -256(%rbp)
	movq	16(%r12), %rax
	leaq	1(%rax), %rsi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	movq	80(%r12), %rsi
	movl	%eax, -256(%rbp)
	movl	%eax, %ecx
	testq	%rsi, %rsi
	je	.L4798
	movl	%eax, %edx
	movq	144(%rsi), %rax
	subq	136(%rsi), %rax
	sarq	$5, %rax
	cmpq	%rax, %rdx
	jnb	.L4798
	leaq	-288(%rbp), %rdx
	leaq	16(%r12), %rsi
	movb	$7, -288(%rbp)
	leaq	176(%r12), %rdi
	call	_ZNSt6vectorIN2v88internal4wasm9ValueBaseENS1_13ZoneAllocatorIS3_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_
	movl	-252(%rbp), %eax
	leal	1(%rax), %r13d
	jmp	.L4797
.L4597:
	movq	104(%r12), %rax
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	-252(%rbp), %rdx
	leaq	.LC587(%rip), %rcx
	movb	%r14b, -328(%rbp)
	movb	$1, 1(%rax)
	movq	16(%r12), %r13
	leaq	1(%r13), %rsi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	leaq	-232(%rbp), %rdx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movl	%eax, -256(%rbp)
	movl	-252(%rbp), %eax
	leaq	.LC583(%rip), %rcx
	movq	$0, -240(%rbp)
	leaq	1(%r13,%rax), %rsi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	movl	-252(%rbp), %edx
	movabsq	$3353953467947191203, %rsi
	movl	-256(%rbp), %edi
	movl	%eax, -248(%rbp)
	movl	%eax, %ecx
	movl	-232(%rbp), %eax
	movq	%rdi, -360(%rbp)
	movq	16(%r12), %r14
	addl	%edx, %eax
	movzbl	-328(%rbp), %r10d
	movl	%eax, -224(%rbp)
	movq	224(%r12), %rax
	movq	%rax, -336(%rbp)
	subq	216(%r12), %rax
	sarq	$3, %rax
	imulq	%rsi, %rax
	cmpq	%rax, %rdi
	jnb	.L5494
	movq	80(%r12), %rax
	testq	%rax, %rax
	je	.L4600
	movq	256(%rax), %rsi
	movq	264(%rax), %rax
	movl	%ecx, %edi
	subq	%rsi, %rax
	sarq	$3, %rax
	cmpq	%rax, %rdi
	jnb	.L4600
	leaq	(%rsi,%rdi,8), %rcx
	movq	192(%r12), %rax
	movq	-336(%rbp), %rdi
	movq	%rcx, -240(%rbp)
	movq	%rax, %rdx
	movl	-84(%rdi), %esi
	subq	184(%r12), %rdx
	sarq	$4, %rdx
	cmpq	%rdx, %rsi
	jb	.L5495
	cmpb	$2, -72(%rdi)
	jne	.L5496
.L4603:
	movq	%r14, -328(%rbp)
.L4605:
	movq	(%rcx), %r15
	movq	8(%r15), %r13
	testq	%r13, %r13
	je	.L4611
	leaq	176(%r12), %rax
	xorl	%r14d, %r14d
	movb	%r10b, -368(%rbp)
	leaq	-288(%rbp), %rdx
	movq	%rax, -344(%rbp)
	leaq	16(%r12), %rax
	movq	%rax, -352(%rbp)
	movq	%r14, %rax
	movq	%r15, %r14
	movq	%r12, -376(%rbp)
	movq	%rax, %r15
	movq	%rdx, %r12
.L4612:
	movq	16(%r14), %rcx
	movq	-352(%rbp), %rsi
	movq	%r12, %rdx
	movq	-344(%rbp), %rdi
	addq	%r15, %rcx
	addq	(%r14), %rcx
	addq	$1, %r15
	movzbl	(%rcx), %ecx
	movb	%cl, -288(%rbp)
	call	_ZNSt6vectorIN2v88internal4wasm9ValueBaseENS1_13ZoneAllocatorIS3_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_
	cmpq	%r13, %r15
	jne	.L4612
	movzbl	-368(%rbp), %r10d
	movq	-376(%rbp), %r12
.L4611:
	movq	-360(%rbp), %rdi
	movl	$1, %edx
	movb	%r10b, -344(%rbp)
	leaq	(%rdi,%rdi,4), %rax
	leaq	(%rdi,%rax,2), %rax
	movq	-336(%rbp), %rdi
	salq	$3, %rax
	subq	%rax, %rdi
	leaq	-88(%rdi), %r14
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15TypeCheckBranchEPNS1_11ControlBaseINS1_9ValueBaseEEEb
	movzbl	-344(%rbp), %r10d
	testl	%eax, %eax
	jne	.L5497
	cmpb	$3, (%r14)
	leaq	24(%r14), %rdx
	leaq	56(%r14), %rax
	cmove	%rdx, %rax
	movb	$1, 24(%rax)
.L4615:
	movl	-224(%rbp), %eax
	xorl	%r15d, %r15d
	leal	1(%rax), %r14d
	testq	%r13, %r13
	je	.L4620
	movb	%r10b, -336(%rbp)
	jmp	.L4616
.L5500:
	cmpb	$2, -72(%rcx)
	jne	.L5498
.L4618:
	addq	$1, %r15
	cmpq	%r13, %r15
	je	.L5499
.L4616:
	movq	224(%r12), %rcx
	movq	192(%r12), %rdx
	movl	-84(%rcx), %esi
	movq	%rdx, %rax
	subq	184(%r12), %rax
	sarq	$4, %rax
	cmpq	%rax, %rsi
	jnb	.L5500
	subq	$16, %rdx
	movq	%rdx, 192(%r12)
	jmp	.L4618
.L4559:
	movq	104(%r12), %rax
	leaq	.LC583(%rip), %rcx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	-240(%rbp), %rdx
	movb	$1, 1(%rax)
	movq	16(%r12), %rax
	movq	$0, -248(%rbp)
	leaq	1(%rax), %rsi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	movl	%eax, -256(%rbp)
	movl	%eax, %ecx
	movl	-240(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -336(%rbp)
	movq	80(%r12), %rax
	testq	%rax, %rax
	je	.L4561
	movq	256(%rax), %rdx
	movq	264(%rax), %rax
	movl	%ecx, %esi
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rax, %rsi
	jnb	.L4561
	leaq	(%rdx,%rsi,8), %rax
	movq	%rax, -248(%rbp)
	movq	(%rax), %rax
	movq	%rax, -344(%rbp)
	testq	%rax, %rax
	je	.L5501
	movq	%rax, %rcx
	leaq	-184(%rbp), %rax
	leaq	-208(%rbp), %rdi
	movq	8(%rcx), %r14
	movq	%rax, %xmm0
	movq	%rax, -328(%rbp)
	leaq	-56(%rbp), %rax
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, -192(%rbp)
	movslq	%r14d, %rsi
	movaps	%xmm0, -208(%rbp)
	call	_ZN2v84base11SmallVectorINS_8internal4wasm9ValueBaseELm8EE14resize_no_initEm
	subl	$1, %r14d
	js	.L4565
	movslq	%r14d, %r13
	movl	%r14d, %r15d
	jmp	.L4573
.L5503:
	cmpb	$2, -72(%rcx)
	movq	16(%r12), %r10
	jne	.L5502
.L4567:
	movl	$10, %r14d
.L4569:
	movq	%r13, %rax
	subl	$1, %r15d
	subq	$1, %r13
	salq	$4, %rax
	addq	-208(%rbp), %rax
	movq	%r10, (%rax)
	movb	%r14b, 8(%rax)
	cmpl	$-1, %r15d
	je	.L4565
.L4573:
	movq	224(%r12), %rcx
	movq	192(%r12), %rax
	movl	-84(%rcx), %esi
	movq	%rax, %rdx
	subq	184(%r12), %rdx
	sarq	$4, %rdx
	cmpq	%rdx, %rsi
	jnb	.L5503
	movq	-344(%rbp), %rdi
	movzbl	-8(%rax), %r14d
	subq	$16, %rax
	movq	(%rax), %r10
	movq	16(%rdi), %rdx
	addq	%r13, %rdx
	addq	(%rdi), %rdx
	movzbl	(%rdx), %ecx
	movq	%rax, 192(%r12)
	cmpb	%cl, %r14b
	je	.L4569
	movzbl	%cl, %eax
	movzbl	%r14b, %edi
	movq	%r10, -352(%rbp)
	movl	%eax, %esi
	movl	%eax, -360(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$10, %r14b
	movq	-352(%rbp), %r10
	setne	%sil
	cmpb	$10, %cl
	setne	%dl
	testb	%dl, %sil
	je	.L4569
	cmpb	$1, %al
	je	.L4569
	movq	%r10, -376(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	-376(%rbp), %r10
	leaq	24(%r12), %r8
	movq	%rax, -368(%rbp)
	leaq	.LC482(%rip), %rax
	movq	%rax, -352(%rbp)
	cmpq	24(%r12), %r10
	jnb	.L4570
	movq	%r10, %rsi
	movq	%r8, %rdi
	movq	%r10, -384(%rbp)
	movq	%r8, -376(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-384(%rbp), %r10
	movq	-376(%rbp), %r8
	movq	%rax, -352(%rbp)
.L4570:
	movl	-360(%rbp), %edi
	movq	%r10, -376(%rbp)
	movq	%r8, -384(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	16(%r12), %rsi
	cmpq	24(%r12), %rsi
	leaq	.LC482(%rip), %rcx
	movq	-376(%rbp), %r10
	movq	%rax, %r9
	jnb	.L4571
	movq	-384(%rbp), %r8
	movq	%r10, -360(%rbp)
	movq	%rax, -376(%rbp)
	movq	%r8, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-376(%rbp), %r9
	movq	-360(%rbp), %r10
	movq	%rax, %rcx
.L4571:
	pushq	-368(%rbp)
	movq	%r10, %rsi
	movl	%r15d, %r8d
	movq	%r12, %rdi
	pushq	-352(%rbp)
	leaq	.LC531(%rip), %rdx
	xorl	%eax, %eax
	movq	%r10, -352(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	popq	%r10
	movq	-352(%rbp), %r10
	popq	%r11
	jmp	.L4569
.L4574:
	movq	104(%r12), %rax
	leaq	-256(%rbp), %r15
	leaq	88(%r12), %rsi
	movq	%r12, %rdx
	movq	%r15, %rdi
	movb	$1, 1(%rax)
	movq	16(%r12), %rcx
	call	_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE1EEC1ERKNS1_12WasmFeaturesEPS3_PKh
	cmpb	$10, -252(%rbp)
	jne	.L4576
	movq	80(%r12), %rax
	movl	-248(%rbp), %ecx
	testq	%rax, %rax
	je	.L5025
	movq	88(%rax), %rdx
	movq	96(%rax), %r8
	movl	%ecx, %esi
	subq	%rdx, %r8
	sarq	$3, %r8
	cmpq	%r8, %rsi
	jnb	.L4577
	movq	(%rdx,%rsi,8), %rax
	movq	%rax, -336(%rbp)
	movq	%rax, -240(%rbp)
.L4578:
	leaq	-208(%rbp), %rdi
	testq	%rax, %rax
	je	.L4579
	movq	8(%rax), %r14
	leaq	-184(%rbp), %rcx
	leaq	-56(%rbp), %rax
	movq	%rcx, %xmm0
	movq	%rcx, -328(%rbp)
	punpcklqdq	%xmm0, %xmm0
	movslq	%r14d, %rsi
	movq	%rax, -192(%rbp)
	movaps	%xmm0, -208(%rbp)
	call	_ZN2v84base11SmallVectorINS_8internal4wasm9ValueBaseELm8EE14resize_no_initEm
	subl	$1, %r14d
	js	.L4581
	movq	%r15, -368(%rbp)
	movslq	%r14d, %r13
	jmp	.L4589
.L5506:
	cmpb	$2, -72(%rcx)
	movq	16(%r12), %r11
	jne	.L5504
.L4583:
	movl	$10, %r15d
.L4585:
	movq	%r13, %rax
	subl	$1, %r14d
	subq	$1, %r13
	salq	$4, %rax
	addq	-208(%rbp), %rax
	movq	%r11, (%rax)
	movb	%r15b, 8(%rax)
	cmpl	$-1, %r14d
	je	.L5505
.L4589:
	movq	224(%r12), %rcx
	movq	192(%r12), %rax
	movl	-84(%rcx), %esi
	movq	%rax, %rdx
	subq	184(%r12), %rdx
	sarq	$4, %rdx
	cmpq	%rdx, %rsi
	jnb	.L5506
	movq	-336(%rbp), %rcx
	movzbl	-8(%rax), %r15d
	subq	$16, %rax
	movq	(%rax), %r11
	movq	16(%rcx), %rdx
	addq	%r13, %rdx
	addq	(%rcx), %rdx
	movzbl	(%rdx), %ecx
	movq	%rax, 192(%r12)
	cmpb	%cl, %r15b
	je	.L4585
	movzbl	%cl, %r8d
	movzbl	%r15b, %edi
	movq	%r11, -344(%rbp)
	movl	%r8d, %esi
	movl	%r8d, -352(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$10, %r15b
	movq	-344(%rbp), %r11
	setne	%sil
	cmpb	$10, %cl
	setne	%dl
	testb	%dl, %sil
	je	.L4585
	cmpb	$1, %al
	je	.L4585
	movq	%r11, -376(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	-376(%rbp), %r11
	cmpq	24(%r12), %r11
	leaq	24(%r12), %r10
	movq	%rax, -360(%rbp)
	leaq	.LC482(%rip), %rax
	movl	-352(%rbp), %r8d
	movq	%rax, -344(%rbp)
	jnb	.L4586
	movq	%r11, %rsi
	movq	%r10, %rdi
	movl	%r8d, -384(%rbp)
	movq	%r10, -352(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movl	-384(%rbp), %r8d
	movq	-376(%rbp), %r11
	movq	%rax, -344(%rbp)
	movq	-352(%rbp), %r10
.L4586:
	movl	%r8d, %edi
	movq	%r11, -352(%rbp)
	movq	%r10, -384(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	16(%r12), %rsi
	cmpq	24(%r12), %rsi
	leaq	.LC482(%rip), %rcx
	movq	-352(%rbp), %r11
	movq	%rax, %r9
	jnb	.L4587
	movq	-384(%rbp), %r10
	movq	%rax, -376(%rbp)
	movq	%r10, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-376(%rbp), %r9
	movq	-352(%rbp), %r11
	movq	%rax, %rcx
.L4587:
	pushq	-360(%rbp)
	movl	%r14d, %r8d
	movq	%r11, %rsi
	movq	%r12, %rdi
	pushq	-344(%rbp)
	leaq	.LC531(%rip), %rdx
	xorl	%eax, %eax
	movq	%r11, -344(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	popq	%r8
	movq	-344(%rbp), %r11
	popq	%r9
	jmp	.L4585
.L4534:
	movq	-240(%rbp), %rax
	movq	%rax, -336(%rbp)
.L4536:
	testq	%rax, %rax
	je	.L4538
	movq	8(%rax), %r14
	leaq	-184(%rbp), %rcx
	leaq	-56(%rbp), %rax
	movq	%rcx, %xmm0
	movq	%rax, -192(%rbp)
	leaq	-208(%rbp), %rdi
	movq	%rcx, %rax
	punpcklqdq	%xmm0, %xmm0
	movslq	%r14d, %r13
	movq	%rcx, -328(%rbp)
	movaps	%xmm0, -208(%rbp)
	cmpq	$8, %r13
	ja	.L5507
.L4539:
	salq	$4, %r13
	addq	%rax, %r13
	movq	%r13, -200(%rbp)
	subl	$1, %r14d
	js	.L4541
	movq	%r15, -368(%rbp)
	movslq	%r14d, %r13
	movl	%r14d, %r15d
	jmp	.L4549
	.p2align 4,,10
	.p2align 3
.L5510:
	cmpb	$2, -72(%rcx)
	movq	16(%r12), %r11
	jne	.L5508
.L4543:
	movl	$10, %r14d
.L4545:
	movq	%r13, %rax
	subl	$1, %r15d
	subq	$1, %r13
	salq	$4, %rax
	addq	-208(%rbp), %rax
	movq	%r11, (%rax)
	movb	%r14b, 8(%rax)
	cmpl	$-1, %r15d
	je	.L5509
.L4549:
	movq	224(%r12), %rcx
	movq	192(%r12), %rax
	movl	-84(%rcx), %esi
	movq	%rax, %rdx
	subq	184(%r12), %rdx
	sarq	$4, %rdx
	cmpq	%rdx, %rsi
	jnb	.L5510
	movq	-336(%rbp), %rcx
	movzbl	-8(%rax), %r14d
	subq	$16, %rax
	movq	(%rax), %r11
	movq	16(%rcx), %rdx
	addq	%r13, %rdx
	addq	(%rcx), %rdx
	movzbl	(%rdx), %ecx
	movq	%rax, 192(%r12)
	cmpb	%cl, %r14b
	je	.L4545
	movzbl	%cl, %r9d
	movzbl	%r14b, %edi
	movq	%r11, -344(%rbp)
	movl	%r9d, %esi
	movl	%r9d, -352(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$10, %r14b
	movq	-344(%rbp), %r11
	setne	%sil
	cmpb	$10, %cl
	setne	%dl
	testb	%dl, %sil
	je	.L4545
	cmpb	$1, %al
	je	.L4545
	movq	%r11, -376(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	-376(%rbp), %r11
	leaq	24(%r12), %rdi
	cmpq	24(%r12), %r11
	movq	%rax, -360(%rbp)
	leaq	.LC482(%rip), %rax
	movl	-352(%rbp), %r9d
	movq	%rdi, -384(%rbp)
	movq	%rax, -344(%rbp)
	jb	.L5511
.L4546:
	movl	%r9d, %edi
	movq	%r11, -352(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	16(%r12), %rsi
	cmpq	24(%r12), %rsi
	leaq	.LC482(%rip), %rcx
	movq	-352(%rbp), %r11
	movq	%rax, %r9
	jb	.L5512
.L4547:
	pushq	-360(%rbp)
	movq	%r11, %rsi
	xorl	%eax, %eax
	movl	%r15d, %r8d
	pushq	-344(%rbp)
	leaq	.LC531(%rip), %rdx
	movq	%r12, %rdi
	movq	%r11, -344(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	popq	%rax
	movq	-344(%rbp), %r11
	popq	%rdx
	jmp	.L4545
.L5508:
	leaq	.LC482(%rip), %rcx
	cmpq	%r11, 24(%r12)
	jbe	.L4544
	movq	%r11, %rsi
	leaq	24(%r12), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %r11
	movq	%rax, %rcx
.L4544:
	movq	%r11, %rsi
	leaq	.LC530(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	16(%r12), %r11
	jmp	.L4543
.L5509:
	movq	-368(%rbp), %r15
.L4541:
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE11PushControlENS1_11ControlKindE
	movq	-208(%rbp), %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %r13
	movq	%rax, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE12SetBlockTypeEPNS1_11ControlBaseINS1_9ValueBaseEEERNS1_18BlockTypeImmediateILS4_1EEEPS8_
	leaq	24(%r13), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15PushMergeValuesEPNS1_11ControlBaseINS1_9ValueBaseEEEPNS1_5MergeIS8_EE
	movl	-256(%rbp), %eax
	movq	-208(%rbp), %rdi
	leal	1(%rax), %r13d
	cmpq	-328(%rbp), %rdi
	je	.L4537
	call	free@PLT
.L4537:
	movq	24(%r12), %r15
	addq	16(%r12), %r13
	jmp	.L4533
.L4621:
	movq	-240(%rbp), %rax
	movq	%rax, -336(%rbp)
.L4623:
	testq	%rax, %rax
	je	.L4625
	movq	8(%rax), %r14
	leaq	-184(%rbp), %rcx
	leaq	-56(%rbp), %rax
	movq	%rcx, %xmm0
	movq	%rax, -192(%rbp)
	leaq	-208(%rbp), %rdi
	movq	%rcx, %rax
	punpcklqdq	%xmm0, %xmm0
	movslq	%r14d, %r13
	movq	%rcx, -328(%rbp)
	movaps	%xmm0, -208(%rbp)
	cmpq	$8, %r13
	ja	.L5513
.L4626:
	salq	$4, %r13
	addq	%rax, %r13
	movq	%r13, -200(%rbp)
	subl	$1, %r14d
	js	.L4628
	movq	%r15, -368(%rbp)
	movslq	%r14d, %r13
	movl	%r14d, %r15d
	jmp	.L4636
	.p2align 4,,10
	.p2align 3
.L5516:
	cmpb	$2, -72(%rcx)
	movq	16(%r12), %r11
	jne	.L5514
.L4630:
	movl	$10, %r14d
.L4632:
	movq	%r13, %rax
	subl	$1, %r15d
	subq	$1, %r13
	salq	$4, %rax
	addq	-208(%rbp), %rax
	movq	%r11, (%rax)
	movb	%r14b, 8(%rax)
	cmpl	$-1, %r15d
	je	.L5515
.L4636:
	movq	224(%r12), %rcx
	movq	192(%r12), %rax
	movl	-84(%rcx), %esi
	movq	%rax, %rdx
	subq	184(%r12), %rdx
	sarq	$4, %rdx
	cmpq	%rdx, %rsi
	jnb	.L5516
	movq	-336(%rbp), %rcx
	movzbl	-8(%rax), %r14d
	subq	$16, %rax
	movq	(%rax), %r11
	movq	16(%rcx), %rdx
	addq	%r13, %rdx
	addq	(%rcx), %rdx
	movzbl	(%rdx), %ecx
	movq	%rax, 192(%r12)
	cmpb	%cl, %r14b
	je	.L4632
	movzbl	%cl, %r9d
	movzbl	%r14b, %edi
	movq	%r11, -344(%rbp)
	movl	%r9d, %esi
	movl	%r9d, -352(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	movq	-344(%rbp), %r11
	cmpb	$10, %cl
	setne	%cl
	cmpb	$10, %r14b
	setne	%dl
	testb	%dl, %cl
	je	.L4632
	cmpb	$1, %al
	je	.L4632
	movq	%r11, -376(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	-376(%rbp), %r11
	leaq	24(%r12), %rdi
	cmpq	24(%r12), %r11
	movq	%rax, -360(%rbp)
	leaq	.LC482(%rip), %rax
	movl	-352(%rbp), %r9d
	movq	%rdi, -384(%rbp)
	movq	%rax, -344(%rbp)
	jb	.L5517
.L4633:
	movl	%r9d, %edi
	movq	%r11, -352(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	16(%r12), %rsi
	cmpq	24(%r12), %rsi
	leaq	.LC482(%rip), %rcx
	movq	-352(%rbp), %r11
	movq	%rax, %r9
	jb	.L5518
.L4634:
	pushq	-360(%rbp)
	movq	%r11, %rsi
	xorl	%eax, %eax
	movl	%r15d, %r8d
	pushq	-344(%rbp)
	leaq	.LC531(%rip), %rdx
	movq	%r12, %rdi
	movq	%r11, -344(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	popq	%rax
	movq	-344(%rbp), %r11
	popq	%rdx
	jmp	.L4632
.L5514:
	leaq	.LC482(%rip), %rcx
	cmpq	%r11, 24(%r12)
	jbe	.L4631
	movq	%r11, %rsi
	leaq	24(%r12), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %r11
	movq	%rax, %rcx
.L4631:
	movq	%r11, %rsi
	leaq	.LC530(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	16(%r12), %r11
	jmp	.L4630
.L5515:
	movq	-368(%rbp), %r15
.L4628:
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE11PushControlENS1_11ControlKindE
	movq	-208(%rbp), %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %r14
	movq	224(%r12), %rax
	leaq	-88(%rax), %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE12SetBlockTypeEPNS1_11ControlBaseINS1_9ValueBaseEEERNS1_18BlockTypeImmediateILS4_1EEEPS8_
	movq	%r12, %rdi
	leaq	24(%r14), %rdx
	movq	%r14, %rsi
	movl	-256(%rbp), %eax
	leal	1(%rax), %r13d
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15PushMergeValuesEPNS1_11ControlBaseINS1_9ValueBaseEEEPNS1_5MergeIS8_EE
	movq	-208(%rbp), %rdi
	cmpq	-328(%rbp), %rdi
	je	.L4624
	call	free@PLT
.L4624:
	movq	24(%r12), %r15
	addq	16(%r12), %r13
	jmp	.L4533
.L4678:
	movzbl	-8(%rdx), %r8d
	leaq	-16(%rdx), %r10
	movq	-16(%rdx), %r14
	movq	%r10, 192(%r12)
	cmpb	$1, %r8b
	jne	.L4681
.L5129:
	movq	%r10, %rax
	movl	-84(%rcx), %edx
	subq	%r9, %rax
	sarq	$4, %rax
.L4680:
	cmpq	%rdx, %rax
	jbe	.L4679
	leaq	-16(%r10), %rax
	movzbl	-8(%r10), %r14d
	movq	%rax, %rdx
	movq	%rax, 192(%r12)
	movl	-84(%rcx), %esi
	subq	%r9, %rdx
	movl	%r14d, %r8d
	sarq	$4, %rdx
.L4689:
	cmpq	%rsi, %rdx
	jbe	.L5519
	movq	-16(%rax), %rdi
	movzbl	-8(%rax), %r15d
	subq	$16, %rax
	movq	%rax, 192(%r12)
	movq	%rdi, -328(%rbp)
	movl	%r15d, %r13d
	cmpb	%r15b, %r8b
	je	.L4695
	movl	%r14d, %esi
	movl	%r15d, %edi
	movb	%r8b, -336(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	movzbl	-336(%rbp), %r8d
	testb	%al, %al
	jne	.L4695
	cmpb	$10, %r15b
	je	.L5057
	cmpb	$10, %r8b
	je	.L4696
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	-328(%rbp), %rdi
	leaq	24(%r12), %r8
	movq	%rax, -344(%rbp)
	leaq	.LC482(%rip), %rax
	movq	%rax, -336(%rbp)
	cmpq	24(%r12), %rdi
	jnb	.L4697
	movq	%rdi, %rsi
	movq	%r8, %rdi
	movq	%r8, -352(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-352(%rbp), %r8
	movq	%rax, -336(%rbp)
.L4697:
	movl	%r14d, %edi
	movq	%r8, -352(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	16(%r12), %rsi
	leaq	.LC482(%rip), %rcx
	movq	%rax, %r14
	cmpq	24(%r12), %rsi
	jnb	.L4698
	movq	-352(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	%rax, %rcx
.L4698:
	pushq	-344(%rbp)
	movq	%r14, %r9
	xorl	%eax, %eax
	xorl	%r8d, %r8d
	pushq	-336(%rbp)
	leaq	.LC531(%rip), %rdx
	movq	%r12, %rdi
	movq	-328(%rbp), %rsi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	popq	%r14
	popq	%rax
.L4696:
	cmpb	$6, %r13b
	jne	.L4693
	jmp	.L4699
.L4844:
	movq	104(%r12), %rax
	leaq	.LC539(%rip), %rcx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	-252(%rbp), %rdx
	movb	$1, 7(%rax)
	movabsq	$4294967296, %rax
	movq	%rax, -256(%rbp)
	movq	16(%r12), %rax
	leaq	1(%rax), %rsi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	movq	80(%r12), %r9
	movq	16(%r12), %r15
	movl	%eax, -256(%rbp)
	movl	%eax, %ecx
	movl	-252(%rbp), %eax
	leal	1(%rax), %r13d
	testq	%r9, %r9
	je	.L4846
	movq	184(%r9), %rdx
	movq	192(%r9), %rax
	movl	%ecx, %r10d
	subq	%rdx, %rax
	sarq	$4, %rax
	cmpq	%rax, %r10
	jnb	.L4846
	movq	224(%r12), %rsi
	movq	192(%r12), %rax
	movl	-84(%rsi), %edi
	movq	%rax, %rcx
	subq	184(%r12), %rcx
	sarq	$4, %rcx
	cmpq	%rcx, %rdi
	jb	.L4848
	cmpb	$2, -72(%rsi)
	jne	.L5520
.L4849:
	salq	$4, %r10
	leaq	16(%r12), %rsi
	leaq	176(%r12), %rdi
	movzbl	(%rdx,%r10), %eax
	leaq	-288(%rbp), %rdx
	movb	%al, -288(%rbp)
	call	_ZNSt6vectorIN2v88internal4wasm9ValueBaseENS1_13ZoneAllocatorIS3_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_
	jmp	.L4845
.L4975:
	leaq	.LC622(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movl	$64512, %esi
.L4976:
	cmpb	$0, 98(%r12)
	je	.L5521
	movq	104(%r12), %rax
	movb	$1, 10(%rax)
.L4979:
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE19DecodeNumericOpcodeENS1_10WasmOpcodeE
	movq	24(%r12), %r15
	leal	2(%rax), %r13d
	addq	16(%r12), %r13
	jmp	.L4533
.L4795:
	movq	104(%r12), %rax
	leaq	-256(%rbp), %rdx
	leaq	16(%r12), %rsi
	leaq	176(%r12), %rdi
	movb	$1, 7(%rax)
	movb	$8, -256(%rbp)
	call	_ZNSt6vectorIN2v88internal4wasm9ValueBaseENS1_13ZoneAllocatorIS3_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4827:
	subq	$16, %rdx
	addq	$1, %r13
	movq	%rdx, 192(%r12)
	jmp	.L4533
.L4745:
	movzbl	-8(%rax), %ecx
	movq	-16(%rax), %r13
	subq	$16, %rax
	movq	%rax, 192(%r12)
	cmpb	$1, %cl
	jne	.L4748
.L4749:
	movq	16(%r12), %rsi
	jmp	.L4746
.L4732:
	movzbl	-8(%rax), %ecx
	movq	-16(%rax), %r13
	subq	$16, %rax
	movq	%rax, 192(%r12)
	cmpb	$1, %cl
	jne	.L4735
.L4736:
	movq	16(%r12), %rsi
	jmp	.L4733
.L4947:
	movq	104(%r12), %rax
	leaq	-256(%rbp), %rdi
	movq	%r12, %rcx
	movabsq	$1099511627775, %rdx
	movb	$1, 5(%rax)
	movq	88(%r12), %rsi
	movq	16(%r12), %r8
	andq	96(%r12), %rdx
	call	_ZN2v88internal4wasm21CallIndirectImmediateILNS1_7Decoder12ValidateFlagE1EEC1ENS1_12WasmFeaturesEPS3_PKh
	movl	-240(%rbp), %eax
	movq	80(%r12), %rdx
	addl	$1, %eax
	testq	%rdx, %rdx
	je	.L5522
	movq	%rax, -336(%rbp)
	movq	184(%rdx), %rsi
	movq	192(%rdx), %rax
	movl	-256(%rbp), %ecx
	subq	%rsi, %rax
	sarq	$4, %rax
	cmpq	%rax, %rcx
	jnb	.L4950
	salq	$4, %rcx
	cmpb	$7, (%rsi,%rcx)
	jne	.L4952
	movq	88(%rdx), %rsi
	movq	96(%rdx), %rax
	movl	-252(%rbp), %edi
	movq	16(%r12), %r14
	subq	%rsi, %rax
	sarq	$3, %rax
	movq	%rdi, %rcx
	cmpq	%rax, %rdi
	jnb	.L5523
	movq	(%rsi,%rdi,8), %r15
	movq	%r15, -248(%rbp)
	testq	%r15, %r15
	je	.L4955
	movq	112(%r12), %rax
	movq	(%rax), %rdx
	cmpq	(%r15), %rdx
	jne	.L4955
	testq	%rdx, %rdx
	je	.L4957
	movq	16(%rax), %rsi
	movq	16(%r15), %rcx
	xorl	%eax, %eax
	jmp	.L4958
.L5524:
	addq	$1, %rax
	cmpq	%rax, %rdx
	je	.L4957
.L4958:
	movzbl	(%rcx,%rax), %edi
	cmpb	%dil, (%rsi,%rax)
	je	.L5524
.L4955:
	movl	$19, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	16(%r12), %rsi
	leaq	.LC620(%rip), %r8
	movq	%r12, %rdi
	movq	%rax, %rcx
	leaq	.LC621(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L4948
.L4700:
	movq	104(%r12), %rax
	leaq	-256(%rbp), %rdi
	movq	%r12, %rsi
	movl	$1, %r13d
	movb	$1, 7(%rax)
	movq	16(%r12), %rdx
	call	_ZN2v88internal4wasm19SelectTypeImmediateILNS1_7Decoder12ValidateFlagE1EEC1EPS3_PKh
	cmpq	$0, 56(%r12)
	jne	.L4701
	movq	192(%r12), %rcx
	movq	224(%r12), %r8
	movq	184(%r12), %r10
	movq	%rcx, %rax
	movl	-84(%r8), %edx
	subq	%r10, %rax
	sarq	$4, %rax
	cmpq	%rax, %rdx
	jb	.L4702
	cmpb	$2, -72(%r8)
	movzbl	-252(%rbp), %r9d
	jne	.L5525
.L5006:
	cmpb	$2, -72(%r8)
	jne	.L5526
.L4712:
	cmpb	$2, -72(%r8)
	jne	.L5527
.L4723:
	leaq	-288(%rbp), %rdx
	leaq	16(%r12), %rsi
	movb	%r9b, -288(%rbp)
	leaq	176(%r12), %rdi
	call	_ZNSt6vectorIN2v88internal4wasm9ValueBaseENS1_13ZoneAllocatorIS3_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_
	movl	-256(%rbp), %eax
	leal	1(%rax), %r13d
	jmp	.L4701
.L4933:
	movq	104(%r12), %rax
	leaq	.LC607(%rip), %rcx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	-240(%rbp), %rdx
	movb	$1, 5(%rax)
	movq	16(%r12), %rax
	movq	$0, -248(%rbp)
	leaq	1(%rax), %rsi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	movl	%eax, -256(%rbp)
	movl	%eax, %ecx
	movl	-240(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -336(%rbp)
	movq	80(%r12), %rax
	testq	%rax, %rax
	je	.L4935
	movq	136(%rax), %rsi
	movq	144(%rax), %rax
	movl	%ecx, %edx
	subq	%rsi, %rax
	sarq	$5, %rax
	cmpq	%rax, %rdx
	jnb	.L4935
	salq	$5, %rdx
	movq	(%rsi,%rdx), %r15
	movq	%r15, -248(%rbp)
	testq	%r15, %r15
	je	.L4936
	movq	112(%r12), %rax
	movq	(%rax), %rdx
	cmpq	(%r15), %rdx
	jne	.L4936
	testq	%rdx, %rdx
	je	.L4938
	movq	16(%rax), %rsi
	movq	16(%r15), %rcx
	xorl	%eax, %eax
	jmp	.L4939
.L5528:
	addq	$1, %rax
	cmpq	%rax, %rdx
	je	.L4938
.L4939:
	movzbl	(%rcx,%rax), %edi
	cmpb	%dil, (%rsi,%rax)
	je	.L5528
.L4936:
	movl	$18, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	16(%r12), %rsi
	leaq	.LC620(%rip), %r8
	movq	%r12, %rdi
	movq	%rax, %rcx
	leaq	.LC621(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	-336(%rbp), %r13d
	jmp	.L4934
.L4888:
	movq	16(%r12), %rax
	leaq	.LC608(%rip), %rdx
	movq	%r12, %rdi
	leaq	1(%rax), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
.L4891:
	movl	-336(%rbp), %r13d
	movq	24(%r12), %r15
	addq	16(%r12), %r13
	jmp	.L4533
.L5440:
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4829:
	movq	16(%r12), %rax
	leaq	.LC613(%rip), %rdx
	movq	%r12, %rdi
	leaq	1(%rax), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L4831
.L5452:
	cmpb	$1, %al
	je	.L4663
	movq	%r13, %rsi
	leaq	.LC590(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4906:
	leaq	.LC616(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc
.L4908:
	movl	-336(%rbp), %r13d
	movq	24(%r12), %r15
	addq	16(%r12), %r13
	jmp	.L4533
.L4832:
	leaq	1(%r14), %rsi
	leaq	.LC613(%rip), %rdx
.L5434:
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	16(%r12), %r9
.L5435:
	movq	24(%r12), %r15
.L4834:
	addq	%r9, %r13
	jmp	.L4533
.L5009:
	movl	$88, %esi
	movl	$88, %r13d
	jmp	.L4330
.L4552:
	movzbl	-8(%rdx), %ecx
	movq	-16(%rdx), %r13
	subq	$16, %rdx
	movq	%rdx, 192(%r12)
	cmpb	$9, %cl
	jne	.L5529
.L4554:
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE10EndControlEv
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L5446:
	cmpb	$2, -72(%rcx)
	je	.L4643
	movq	16(%r12), %rsi
	leaq	.LC482(%rip), %rcx
	cmpq	24(%r12), %rsi
	jnb	.L4644
	leaq	24(%r12), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %rsi
	movq	%rax, %rcx
.L4644:
	leaq	.LC530(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L4643
.L5518:
	movq	-384(%rbp), %rdi
	movq	%rax, -376(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-376(%rbp), %r9
	movq	-352(%rbp), %r11
	movq	%rax, %rcx
	jmp	.L4634
.L5517:
	movq	%r11, %rsi
	movl	%r9d, -376(%rbp)
	movq	%r11, -352(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movl	-376(%rbp), %r9d
	movq	-352(%rbp), %r11
	movq	%rax, -344(%rbp)
	jmp	.L4633
.L5512:
	movq	-384(%rbp), %rdi
	movq	%rax, -376(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-376(%rbp), %r9
	movq	-352(%rbp), %r11
	movq	%rax, %rcx
	jmp	.L4547
.L5511:
	movq	%r11, %rsi
	movl	%r9d, -376(%rbp)
	movq	%r11, -352(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movl	-376(%rbp), %r9d
	movq	-352(%rbp), %r11
	movq	%rax, -344(%rbp)
	jmp	.L4546
.L4879:
	movzbl	-8(%rax), %ecx
	movq	-16(%rax), %r14
	subq	$16, %rax
	movq	%rax, 192(%r12)
	cmpb	$1, %cl
	jne	.L5530
.L4881:
	leaq	-288(%rbp), %rdx
	leaq	16(%r12), %rsi
	movb	$1, -288(%rbp)
	leaq	176(%r12), %rdi
	call	_ZNSt6vectorIN2v88internal4wasm9ValueBaseENS1_13ZoneAllocatorIS3_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_
.L4877:
	movq	24(%r12), %r15
	addq	16(%r12), %r13
	jmp	.L4533
.L4980:
	cmpb	$0, 97(%r12)
	je	.L5531
	movq	104(%r12), %rax
	movb	$1, 9(%rax)
	jmp	.L4979
.L4576:
	movq	-240(%rbp), %rax
	movq	%rax, -336(%rbp)
	jmp	.L4578
.L5458:
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L5513:
	movq	%r13, %rsi
	call	_ZN2v84base11SmallVectorINS_8internal4wasm9ValueBaseELm8EE4GrowEm
	movq	-208(%rbp), %rax
	jmp	.L4626
.L5507:
	movq	%r13, %rsi
	call	_ZN2v84base11SmallVectorINS_8internal4wasm9ValueBaseELm8EE4GrowEm
	movq	-208(%rbp), %rax
	jmp	.L4539
.L5457:
	movq	-80(%r14), %rsi
	leaq	.LC594(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L5453:
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L5478:
	cmpb	$2, -72(%rcx)
	movq	24(%r12), %r15
	je	.L4834
	leaq	.LC482(%rip), %rcx
	cmpq	%r15, %r9
	jnb	.L4838
	movq	%r9, %rsi
	leaq	24(%r12), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %r9
	movq	%rax, %rcx
.L4838:
	movq	%r9, %rsi
	leaq	.LC530(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%r12), %r15
	movq	16(%r12), %r9
	jmp	.L4834
.L5522:
	movq	%rax, -336(%rbp)
.L4950:
	leaq	.LC616(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc
	jmp	.L4948
.L4858:
	movq	%r14, %rsi
	leaq	.LC580(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%r12), %r15
	movq	16(%r12), %rsi
	movl	-328(%rbp), %r13d
	jmp	.L4857
.L4846:
	leaq	.LC580(%rip), %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L4845
.L4798:
	movq	16(%r12), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	movl	$1, %r13d
	leaq	.LC608(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L4797
.L5445:
	leaq	.LC597(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc
	jmp	.L4991
.L4828:
	movq	%r13, %rsi
	leaq	24(%r12), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %rsi
	leaq	.LC530(%rip), %rdx
	movq	%r12, %rdi
	movq	%rax, %rcx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L5464:
	leaq	24(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %rsi
	leaq	.LC530(%rip), %rdx
	movq	%r12, %rdi
	movq	%rax, %rcx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	192(%r12), %r10
	movq	184(%r12), %r9
	movq	224(%r12), %rcx
	movq	%r10, %rax
	subq	%r9, %rax
	movl	-84(%rcx), %edx
	sarq	$4, %rax
	jmp	.L4680
.L5462:
	leaq	.LC482(%rip), %rcx
	cmpq	%rsi, 24(%r12)
	jbe	.L4747
	leaq	24(%r12), %rdi
	movl	%r10d, -328(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %rsi
	movl	-328(%rbp), %r10d
	movq	%rax, %rcx
.L4747:
	leaq	.LC530(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	movl	%r10d, -328(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	16(%r12), %rsi
	movl	-328(%rbp), %r10d
	jmp	.L4746
.L5460:
	leaq	.LC482(%rip), %rcx
	cmpq	%rsi, 24(%r12)
	jbe	.L4734
	leaq	24(%r12), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %rsi
	movq	%rax, %rcx
.L4734:
	leaq	.LC530(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	16(%r12), %rsi
	jmp	.L4733
.L4735:
	movzbl	%cl, %edi
	movl	$1, %esi
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$10, %cl
	je	.L4736
	cmpb	$1, %al
	je	.L4736
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	.LC482(%rip), %r15
	leaq	24(%r12), %rdi
	movq	%rax, %r14
	movq	24(%r12), %rax
	cmpq	%rax, %r13
	jnb	.L4737
	movq	%r13, %rsi
	movq	%rdi, -328(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-328(%rbp), %rdi
	movq	%rax, %r15
	movq	24(%r12), %rax
.L4737:
	movq	16(%r12), %rsi
	leaq	.LC482(%rip), %rcx
	cmpq	%rax, %rsi
	jnb	.L4738
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	%rax, %rcx
.L4738:
	pushq	%r14
	movq	%r13, %rsi
	leaq	.LC490(%rip), %r9
	xorl	%r8d, %r8d
	pushq	%r15
	leaq	.LC531(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	popq	%r14
	movq	16(%r12), %rsi
	popq	%r15
	jmp	.L4733
.L4748:
	movzbl	%cl, %edi
	movl	$1, %esi
	movl	%r10d, -328(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	movl	-328(%rbp), %r10d
	cmpb	$1, %al
	je	.L4749
	cmpb	$10, %cl
	je	.L4749
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movl	-328(%rbp), %r10d
	leaq	24(%r12), %rdi
	leaq	.LC482(%rip), %rdx
	movq	%rax, %r14
	movq	24(%r12), %rax
	cmpq	%rax, %r13
	jnb	.L4750
	movq	%r13, %rsi
	movl	%r10d, -336(%rbp)
	movq	%rdi, -328(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movl	-336(%rbp), %r10d
	movq	-328(%rbp), %rdi
	movq	%rax, %rdx
	movq	24(%r12), %rax
.L4750:
	movq	16(%r12), %rsi
	leaq	.LC482(%rip), %rcx
	cmpq	%rax, %rsi
	jnb	.L4751
	movl	%r10d, -336(%rbp)
	movq	%rdx, -328(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movl	-336(%rbp), %r10d
	movq	-328(%rbp), %rdx
	movq	%rax, %rcx
.L4751:
	pushq	%r14
	movq	%r13, %rsi
	leaq	.LC490(%rip), %r9
	xorl	%r8d, %r8d
	pushq	%rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC531(%rip), %rdx
	movl	%r10d, -328(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	popq	%r11
	popq	%r13
	movq	16(%r12), %rsi
	movl	-328(%rbp), %r10d
	jmp	.L4746
.L5521:
	leaq	.LC623(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	2(%rax), %r13
	jmp	.L4533
.L5444:
	leaq	.LC629(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc
	jmp	.L4989
.L4681:
	movzbl	%r8b, %edi
	movl	$1, %esi
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$10, %r8b
	je	.L5129
	cmpb	$1, %al
	je	.L5129
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	24(%r12), %rdi
	leaq	.LC482(%rip), %rdx
	movq	%rax, -328(%rbp)
	cmpq	%r15, %r14
	jnb	.L4684
	movq	%r14, %rsi
	movq	%rdi, -336(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %r13
	movq	24(%r12), %r15
	movq	-336(%rbp), %rdi
	movq	%rax, %rdx
.L4684:
	leaq	.LC482(%rip), %rcx
	cmpq	%r15, %r13
	jnb	.L4685
	movq	%r13, %rsi
	movq	%rdx, -336(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-336(%rbp), %rdx
	movq	%rax, %rcx
.L4685:
	pushq	-328(%rbp)
	movq	%r14, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	pushq	%rdx
	leaq	.LC490(%rip), %r9
	leaq	.LC531(%rip), %rdx
	movl	$2, %r8d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	192(%r12), %r10
	movq	184(%r12), %r9
	movq	224(%r12), %rcx
	popq	%rsi
	movq	%r10, %rax
	popq	%rdi
	subq	%r9, %rax
	movl	-84(%rcx), %edx
	sarq	$4, %rax
	jmp	.L4680
.L5465:
	movq	16(%r12), %rsi
	leaq	.LC482(%rip), %rcx
	cmpq	24(%r12), %rsi
	jnb	.L4688
	leaq	24(%r12), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %rsi
	movq	%rax, %rcx
.L4688:
	leaq	.LC530(%rip), %rdx
	xorl	%eax, %eax
	movq	%r12, %rdi
	movl	$10, %r14d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	192(%r12), %rax
	movq	224(%r12), %rcx
	movl	$10, %r8d
	movq	%rax, %rdx
	subq	184(%r12), %rdx
	movl	-84(%rcx), %esi
	sarq	$4, %rdx
	jmp	.L4689
.L5036:
	xorl	%r8d, %r8d
.L4622:
	movq	16(%r12), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	movl	$1, %r13d
	leaq	.LC581(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L4624
.L5012:
	xorl	%r8d, %r8d
.L4535:
	movq	16(%r12), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	movl	$1, %r13d
	leaq	.LC581(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L4537
.L4561:
	movq	16(%r12), %rax
	leaq	.LC584(%rip), %rdx
	movq	%r12, %rdi
	leaq	1(%rax), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	-336(%rbp), %r14d
	jmp	.L4560
.L5042:
	xorl	%r8d, %r8d
.L4639:
	movq	16(%r12), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	movl	$1, %r13d
	leaq	.LC581(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
.L4640:
	movq	24(%r12), %r15
	addq	16(%r12), %r13
	jmp	.L4533
.L4935:
	movq	16(%r12), %rax
	leaq	.LC608(%rip), %rdx
	movq	%r12, %rdi
	leaq	1(%rax), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	-336(%rbp), %r13d
	jmp	.L4934
.L4663:
	movq	%r13, %rsi
	leaq	.LC591(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L5467:
	leaq	1(%r15), %rsi
	leaq	.LC618(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L4908
.L5454:
	leaq	.LC586(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4600:
	leaq	1(%r14,%rdx), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	movl	$1, %r14d
	leaq	.LC584(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L4598
.L5476:
	leaq	.LC482(%rip), %rcx
	cmpq	%r15, %r9
	jnb	.L4810
	movq	%r9, %rsi
	leaq	24(%r12), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %r9
	movq	%rax, %rcx
.L4810:
	movq	%r9, %rsi
	leaq	.LC530(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%r12), %r15
	movq	16(%r12), %r9
	jmp	.L4811
.L5472:
	leaq	.LC482(%rip), %rcx
	cmpq	24(%r12), %r9
	jnb	.L4823
	movq	%r9, %rsi
	leaq	24(%r12), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %r9
	movq	%rax, %rcx
.L4823:
	leaq	.LC530(%rip), %rdx
	movq	%r9, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L4822
	.p2align 4,,10
	.p2align 3
.L5448:
	leaq	.LC482(%rip), %rcx
	cmpq	%r11, 24(%r12)
	jbe	.L4654
	movq	%r11, %rsi
	leaq	24(%r12), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %r11
	movq	%rax, %rcx
.L4654:
	movq	%r11, %rsi
	leaq	.LC530(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	16(%r12), %r11
	jmp	.L4653
.L5449:
	movq	-368(%rbp), %r15
.L4651:
	cmpq	$0, 56(%r12)
	movl	$1, %r13d
	je	.L5532
.L4660:
	movq	-208(%rbp), %rdi
	cmpq	-328(%rbp), %rdi
	je	.L4640
	call	free@PLT
	jmp	.L4640
.L5504:
	leaq	.LC482(%rip), %rcx
	cmpq	%r11, 24(%r12)
	jbe	.L4584
	movq	%r11, %rsi
	leaq	24(%r12), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %r11
	movq	%rax, %rcx
.L4584:
	movq	%r11, %rsi
	leaq	.LC530(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	16(%r12), %r11
	jmp	.L4583
.L5480:
	leaq	.LC482(%rip), %rcx
	cmpq	%r11, 24(%r12)
	jbe	.L4896
	movq	%r11, %rsi
	leaq	24(%r12), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %r11
	movq	%rax, %rcx
.L4896:
	movq	%r11, %rsi
	leaq	.LC530(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	16(%r12), %r11
	jmp	.L4895
.L5470:
	leaq	.LC482(%rip), %rcx
	cmpq	%r11, 24(%r12)
	jbe	.L4923
	movq	%r11, %rsi
	leaq	24(%r12), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %r11
	movq	%rax, %rcx
.L4923:
	movq	%r11, %rsi
	leaq	.LC530(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	16(%r12), %r11
	jmp	.L4922
.L5519:
	cmpb	$2, -72(%rcx)
	jne	.L5533
.L4691:
	cmpb	$10, %r8b
	je	.L5534
	movb	%r8b, -328(%rbp)
	movzbl	-328(%rbp), %r8d
.L5057:
	movl	%r14d, %r15d
	movl	%r8d, %r13d
	jmp	.L4696
.L5463:
	cmpl	$7654320, %r10d
	ja	.L5535
	movq	24(%r12), %rax
	subq	%rsi, %rax
	cmpl	%eax, %r10d
	ja	.L5536
	movq	224(%r12), %rax
	subq	216(%r12), %rax
	movabsq	$3353953467947191203, %rdx
	movq	$0, -328(%rbp)
	sarq	$3, %rax
	imulq	%rdx, %rax
	testq	%rax, %rax
	jne	.L5537
.L5003:
	movq	$0, -304(%rbp)
	pxor	%xmm0, %xmm0
	leaq	-320(%rbp), %r14
	movaps	%xmm0, -320(%rbp)
.L4761:
	movq	-256(%rbp), %rax
	cmpq	$0, 56(%rax)
	jne	.L4758
	movl	-232(%rbp), %r8d
	cmpl	-228(%rbp), %r8d
	movl	%r8d, -344(%rbp)
	ja	.L4758
	movq	-240(%rbp), %r9
	movq	%r15, %rdi
	movq	%r9, -336(%rbp)
	call	_ZN2v88internal4wasm19BranchTableIteratorILNS1_7Decoder12ValidateFlagE1EE4nextEv
	movq	-336(%rbp), %r9
	movabsq	$3353953467947191203, %rdi
	movl	-344(%rbp), %r8d
	movl	%eax, %ecx
	movq	224(%r12), %rax
	subq	216(%r12), %rax
	sarq	$3, %rax
	movq	%rcx, %rdx
	imulq	%rdi, %rax
	cmpq	%rax, %rcx
	jnb	.L5538
	movq	-328(%rbp), %rax
	shrq	$6, %rcx
	leaq	(%rax,%rcx,8), %rdi
	movl	%edx, %ecx
	movl	$1, %eax
	salq	%cl, %rax
	movq	%rax, %rcx
	movq	(%rdi), %rax
	testq	%rcx, %rax
	jne	.L4761
	orq	%rcx, %rax
	movq	%rax, (%rdi)
	testl	%r8d, %r8d
	jne	.L4762
	leaq	-288(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE28InitializeBrTableResultTypesEj
	movq	-272(%rbp), %rax
	movq	-320(%rbp), %rdi
	pxor	%xmm0, %xmm0
	movdqa	-288(%rbp), %xmm7
	movq	$0, -272(%rbp)
	movq	%rax, -304(%rbp)
	movaps	%xmm7, -320(%rbp)
	movaps	%xmm0, -288(%rbp)
	testq	%rdi, %rdi
	je	.L4761
	call	_ZdlPv@PLT
	movq	-288(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4761
	call	_ZdlPv@PLT
	jmp	.L4761
	.p2align 4,,10
	.p2align 3
.L5487:
	leaq	.LC625(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L5486:
	leaq	.LC627(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L5505:
	movq	-368(%rbp), %r15
.L4581:
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE11PushControlENS1_11ControlKindE
	movq	-208(%rbp), %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r14
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE12SetBlockTypeEPNS1_11ControlBaseINS1_9ValueBaseEEERNS1_18BlockTypeImmediateILS4_1EEEPS8_
	movq	%r12, %rdi
	leaq	24(%r14), %rdx
	movq	%r14, %rsi
	movl	-256(%rbp), %eax
	leal	1(%rax), %r13d
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15PushMergeValuesEPNS1_11ControlBaseINS1_9ValueBaseEEEPNS1_5MergeIS8_EE
	movq	-208(%rbp), %rdi
	cmpq	-328(%rbp), %rdi
	je	.L4575
	call	free@PLT
	jmp	.L4575
.L5479:
	leaq	-184(%rbp), %rax
	leaq	-208(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rax, %xmm0
	movq	%rax, -328(%rbp)
	leaq	-56(%rbp), %rax
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, -192(%rbp)
	movaps	%xmm0, -208(%rbp)
	call	_ZN2v84base11SmallVectorINS_8internal4wasm9ValueBaseELm8EE14resize_no_initEm
.L4893:
	movq	-248(%rbp), %r14
	movq	(%r14), %rax
	movq	%rax, -344(%rbp)
	testq	%rax, %rax
	je	.L4904
	leaq	16(%r12), %rax
	xorl	%r13d, %r13d
	leaq	-288(%rbp), %rdx
	movq	%r12, -360(%rbp)
	movq	%rax, -352(%rbp)
	leaq	176(%r12), %r15
	movq	%r13, %r12
	movq	%rdx, %r13
.L4905:
	movq	16(%r14), %rdx
	movq	-352(%rbp), %rsi
	movq	%r15, %rdi
	movzbl	(%rdx,%r12), %edx
	addq	$1, %r12
	movb	%dl, -288(%rbp)
	movq	%r13, %rdx
	call	_ZNSt6vectorIN2v88internal4wasm9ValueBaseENS1_13ZoneAllocatorIS3_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_
	cmpq	%r12, -344(%rbp)
	jne	.L4905
	movq	-360(%rbp), %r12
.L4904:
	movq	-208(%rbp), %rdi
	cmpq	-328(%rbp), %rdi
	je	.L4891
	call	free@PLT
	jmp	.L4891
.L4918:
	leaq	-184(%rbp), %rax
	xorl	%esi, %esi
	movq	%rax, %xmm0
	movq	%rax, -328(%rbp)
	leaq	-56(%rbp), %rax
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, -192(%rbp)
	movaps	%xmm0, -208(%rbp)
	call	_ZN2v84base11SmallVectorINS_8internal4wasm9ValueBaseELm8EE14resize_no_initEm
.L4920:
	movq	-248(%rbp), %r14
	movq	(%r14), %rax
	movq	%rax, -344(%rbp)
	testq	%rax, %rax
	je	.L4931
	leaq	16(%r12), %rax
	xorl	%r13d, %r13d
	leaq	-288(%rbp), %rdx
	movq	%r12, -360(%rbp)
	movq	%rax, -352(%rbp)
	leaq	176(%r12), %r15
	movq	%r13, %r12
	movq	%rdx, %r13
.L4932:
	movq	16(%r14), %rdx
	movq	-352(%rbp), %rsi
	movq	%r15, %rdi
	movzbl	(%rdx,%r12), %edx
	addq	$1, %r12
	movb	%dl, -288(%rbp)
	movq	%r13, %rdx
	call	_ZNSt6vectorIN2v88internal4wasm9ValueBaseENS1_13ZoneAllocatorIS3_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_
	cmpq	%r12, -344(%rbp)
	jne	.L4932
	movq	-360(%rbp), %r12
.L4931:
	movq	-208(%rbp), %rdi
	cmpq	-328(%rbp), %rdi
	je	.L4908
	call	free@PLT
	jmp	.L4908
.L4695:
	cmpb	$10, %r13b
	jne	.L4696
	jmp	.L5057
.L4808:
	movq	152(%r12), %rax
	movzbl	-8(%rdx), %ecx
	subq	$16, %rdx
	movq	(%rdx), %r15
	movzbl	(%rax,%rsi), %r10d
	movq	%rdx, 192(%r12)
	cmpb	%cl, %r10b
	je	.L5132
	movzbl	%r10b, %eax
	movzbl	%cl, %edi
	movl	%eax, %esi
	movl	%eax, -336(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$10, %r10b
	setne	%sil
	cmpb	$10, %cl
	setne	%dl
	testb	%dl, %sil
	je	.L5132
	cmpb	$1, %al
	je	.L5132
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	24(%r12), %r13
	movq	%rax, -344(%rbp)
	leaq	.LC482(%rip), %rax
	movq	%rax, -328(%rbp)
	cmpq	24(%r12), %r15
	jnb	.L4815
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %r14
	movq	%rax, -328(%rbp)
.L4815:
	movl	-336(%rbp), %edi
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	.LC482(%rip), %rcx
	movq	%rax, %r9
	cmpq	24(%r12), %r14
	jnb	.L4816
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, -336(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-336(%rbp), %r9
	movq	%rax, %rcx
.L4816:
	pushq	-344(%rbp)
	xorl	%r8d, %r8d
	movq	%r15, %rsi
	movq	%r12, %rdi
	pushq	-328(%rbp)
	leaq	.LC531(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	popq	%rdi
	movq	16(%r12), %r9
	movq	24(%r12), %r15
	popq	%r8
	jmp	.L4811
.L4821:
	movq	152(%r12), %rax
	movzbl	-8(%rdx), %r13d
	subq	$16, %rdx
	movq	(%rdx), %r15
	movzbl	(%rax,%rsi), %ecx
	movq	%rdx, 192(%r12)
	cmpb	%r13b, %cl
	je	.L4824
	movzbl	%cl, %eax
	movzbl	%r13b, %edi
	movl	%eax, %esi
	movl	%eax, -336(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$10, %r13b
	setne	%sil
	cmpb	$10, %cl
	setne	%dl
	testb	%dl, %sil
	je	.L4824
	cmpb	$1, %al
	je	.L4824
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	24(%r12), %r10
	movq	%rax, -344(%rbp)
	leaq	.LC482(%rip), %rax
	movq	%rax, -328(%rbp)
	cmpq	24(%r12), %r15
	jnb	.L4825
	movq	%r10, %rdi
	movq	%r15, %rsi
	movq	%r10, -352(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %r14
	movq	-352(%rbp), %r10
	movq	%rax, -328(%rbp)
.L4825:
	movl	-336(%rbp), %edi
	movq	%r10, -352(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	.LC482(%rip), %rcx
	movq	%rax, %r9
	cmpq	24(%r12), %r14
	jnb	.L4826
	movq	-352(%rbp), %r10
	movq	%r14, %rsi
	movq	%rax, -336(%rbp)
	movq	%r10, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-336(%rbp), %r9
	movq	%rax, %rcx
.L4826:
	pushq	-344(%rbp)
	movq	%r15, %rsi
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	pushq	-328(%rbp)
	leaq	.LC531(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	popq	%rcx
	popq	%rsi
	jmp	.L4824
.L5484:
	leaq	.LC615(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc
	jmp	.L4877
.L5532:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE11PushControlENS1_11ControlKindE
	movq	-208(%rbp), %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r14
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE12SetBlockTypeEPNS1_11ControlBaseINS1_9ValueBaseEEERNS1_18BlockTypeImmediateILS4_1EEEPS8_
	leaq	24(%r14), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	-256(%rbp), %eax
	leal	1(%rax), %r13d
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15PushMergeValuesEPNS1_11ControlBaseINS1_9ValueBaseEEEPNS1_5MergeIS8_EE
	jmp	.L4660
.L5466:
	leaq	-256(%rbp), %rdx
	leaq	16(%r12), %rsi
	movb	%r13b, -256(%rbp)
	leaq	176(%r12), %rdi
	call	_ZNSt6vectorIN2v88internal4wasm9ValueBaseENS1_13ZoneAllocatorIS3_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L5482:
	leaq	-1(%r13), %rsi
	leaq	.LC535(%rip), %rdx
	movq	%r12, %rdi
	movl	$1, %r13d
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	jmp	.L4887
.L4538:
	leaq	-184(%rbp), %rdi
	leaq	-56(%rbp), %rax
	movq	%rdi, %xmm0
	movq	%rdi, -328(%rbp)
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, -192(%rbp)
	movaps	%xmm0, -208(%rbp)
	jmp	.L4541
.L5459:
	movq	16(%r12), %rax
	leaq	.LC588(%rip), %rdx
	movq	%r12, %rdi
	movl	$1, %r13d
	leaq	1(%rax), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L4727
.L4817:
	leaq	1(%r14), %rsi
	leaq	.LC610(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	$1, %r13d
	jmp	.L4819
.L4909:
	leaq	.LC617(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc
	jmp	.L4908
.L4793:
	leaq	.LC606(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	jmp	.L4794
.L4791:
	leaq	.LC605(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	jmp	.L4792
.L5483:
	leaq	-1(%r13), %rsi
	leaq	.LC535(%rip), %rdx
	movq	%r12, %rdi
	movl	$1, %r13d
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	jmp	.L4877
.L4625:
	leaq	-184(%rbp), %rcx
	leaq	-56(%rbp), %rax
	movq	%rcx, %xmm0
	movq	%rcx, -328(%rbp)
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, -192(%rbp)
	movaps	%xmm0, -208(%rbp)
	jmp	.L4628
.L4648:
	leaq	-184(%rbp), %rdi
	leaq	-56(%rbp), %rax
	movq	%rdi, %xmm0
	movq	%rdi, -328(%rbp)
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, -192(%rbp)
	movaps	%xmm0, -208(%rbp)
	jmp	.L4651
.L5132:
	movq	24(%r12), %r15
	jmp	.L4811
.L5443:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L4340
.L4804:
	leaq	1(%r14), %rsi
	leaq	.LC610(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	$1, %r13d
	movq	16(%r12), %r9
	movq	24(%r12), %r15
	jmp	.L4806
.L4800:
	movq	16(%r12), %rax
	leaq	.LC610(%rip), %rdx
	movq	%r12, %rdi
	movl	$1, %r13d
	leaq	1(%rax), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L4802
.L5451:
	leaq	.LC589(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L5474:
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movl	%r15d, %edi
	movq	%rax, -328(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	%r13, %rsi
	movl	%r14d, %ecx
	movq	%r12, %rdi
	movq	-328(%rbp), %r9
	movq	%rax, %r8
	xorl	%eax, %eax
	leaq	.LC602(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L5502:
	leaq	.LC482(%rip), %rcx
	cmpq	%r10, 24(%r12)
	jbe	.L4568
	movq	%r10, %rsi
	leaq	24(%r12), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %r10
	movq	%rax, %rcx
.L4568:
	movq	%r10, %rsi
	leaq	.LC530(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	16(%r12), %r10
	jmp	.L4567
.L5456:
	movq	%r13, %rsi
	leaq	.LC593(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L5455:
	leaq	.LC592(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L5477:
	leaq	.LC614(%rip), %rdx
	movq	%r9, %rsi
	jmp	.L5434
.L5533:
	movq	16(%r12), %rsi
	leaq	.LC482(%rip), %rcx
	cmpq	24(%r12), %rsi
	jnb	.L4692
	leaq	24(%r12), %rdi
	movb	%r8b, -328(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %rsi
	movzbl	-328(%rbp), %r8d
	movq	%rax, %rcx
.L4692:
	leaq	.LC530(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	movb	%r8b, -328(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movzbl	-328(%rbp), %r8d
	jmp	.L4691
.L5468:
	movzbl	-8(%rax), %ecx
	movq	-16(%rax), %r13
	subq	$16, %rax
	movq	%rax, 192(%r12)
	cmpb	$1, %cl
	je	.L4914
	movzbl	%cl, %edi
	movl	$1, %esi
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$10, %cl
	je	.L4914
	cmpb	$1, %al
	je	.L4914
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	24(%r12), %rdi
	leaq	.LC482(%rip), %rdx
	movq	%rax, %r14
	movq	24(%r12), %rax
	cmpq	%rax, %r13
	jnb	.L4916
	movq	%r13, %rsi
	movq	%rdi, -328(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %r15
	movq	-328(%rbp), %rdi
	movq	%rax, %rdx
	movq	24(%r12), %rax
.L4916:
	leaq	.LC482(%rip), %rcx
	cmpq	%rax, %r15
	jnb	.L4917
	movq	%r15, %rsi
	movq	%rdx, -328(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-328(%rbp), %rdx
	movq	%rax, %rcx
.L4917:
	pushq	%r14
	movq	%r13, %rsi
	xorl	%eax, %eax
	leaq	.LC490(%rip), %r9
	pushq	%rdx
	xorl	%r8d, %r8d
	leaq	.LC531(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-248(%rbp), %rax
	popq	%r11
	popq	%r13
	movq	%rax, -344(%rbp)
	jmp	.L4914
.L5498:
	movq	16(%r12), %rsi
	leaq	.LC482(%rip), %rcx
	cmpq	24(%r12), %rsi
	jnb	.L4619
	leaq	24(%r12), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %rsi
	movq	%rax, %rcx
.L4619:
	leaq	.LC530(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L4618
.L5501:
	leaq	-184(%rbp), %rax
	leaq	-208(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rax, %xmm0
	movq	%rax, -328(%rbp)
	leaq	-56(%rbp), %rax
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, -192(%rbp)
	movaps	%xmm0, -208(%rbp)
	call	_ZN2v84base11SmallVectorINS_8internal4wasm9ValueBaseELm8EE14resize_no_initEm
.L4565:
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE10EndControlEv
	movq	-208(%rbp), %rdi
	movl	-336(%rbp), %r14d
	cmpq	-328(%rbp), %rdi
	je	.L4560
	call	free@PLT
	jmp	.L4560
.L4860:
	salq	$4, %rdx
	movzbl	-8(%rcx), %r13d
	movq	-16(%rcx), %r15
	subq	$16, %rcx
	movzbl	(%rdi,%rdx), %r11d
	movq	%rcx, 192(%r12)
	cmpb	%r13b, %r11b
	je	.L5135
	movzbl	%r11b, %eax
	movzbl	%r13b, %edi
	movl	%eax, %esi
	movl	%eax, -344(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$10, %r13b
	setne	%sil
	cmpb	$10, %r11b
	setne	%dl
	testb	%dl, %sil
	je	.L5135
	cmpb	$1, %al
	je	.L5135
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	24(%r12), %r13
	movq	%rax, -352(%rbp)
	leaq	.LC482(%rip), %rax
	movq	%rax, -336(%rbp)
	cmpq	24(%r12), %r15
	jnb	.L4867
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %r14
	movq	%rax, -336(%rbp)
.L4867:
	movl	-344(%rbp), %edi
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	.LC482(%rip), %rcx
	movq	%rax, %r9
	cmpq	24(%r12), %r14
	jnb	.L4868
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, -344(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-344(%rbp), %r9
	movq	%rax, %rcx
.L4868:
	pushq	-352(%rbp)
	xorl	%eax, %eax
	movq	%r15, %rsi
	movq	%r12, %rdi
	pushq	-336(%rbp)
	leaq	.LC531(%rip), %rdx
	movl	$1, %r8d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	192(%r12), %rcx
	movq	224(%r12), %r9
	popq	%r10
	popq	%r11
	movq	%rcx, %rax
	movl	-84(%r9), %edx
	subq	184(%r12), %rax
	sarq	$4, %rax
.L4863:
	cmpq	%rax, %rdx
	jnb	.L5539
	movzbl	-8(%rcx), %r8d
	movq	-16(%rcx), %r13
	subq	$16, %rcx
	movq	%rcx, 192(%r12)
	cmpb	$1, %r8b
	jne	.L4872
.L4873:
	movq	24(%r12), %r15
	movq	16(%r12), %rsi
	jmp	.L4870
.L5473:
	movl	%edx, %ecx
	movq	%r13, %rsi
	xorl	%eax, %eax
	movq	%r12, %rdi
	leaq	.LC601(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4987:
	leaq	.LC628(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movl	$65024, %esi
	jmp	.L4988
.L5025:
	xorl	%r8d, %r8d
.L4577:
	movq	16(%r12), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	movl	$1, %r13d
	leaq	.LC581(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L4575
.L4848:
	movzbl	-8(%rax), %ecx
	movq	-16(%rax), %r14
	subq	$16, %rax
	movq	%rax, 192(%r12)
	cmpb	$1, %cl
	jne	.L4851
.L5134:
	movq	184(%r9), %rdx
	jmp	.L4849
.L4670:
	cmpb	$3, -88(%r14)
	je	.L4673
	leaq	-88(%r14), %rsi
	leaq	-32(%r14), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE15PushMergeValuesEPNS1_11ControlBaseINS1_9ValueBaseEEEPNS1_5MergeIS8_EE
	movq	224(%r12), %rax
.L4673:
	cmpb	$0, -72(%r14)
	leaq	-88(%rax), %rdx
	je	.L4674
	cmpb	$0, -8(%r14)
	jne	.L4675
	cmpb	$0, -88(%r14)
	movq	%rdx, 224(%r12)
	jne	.L4676
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4984:
	leaq	.LC626(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movl	$64768, %esi
	jmp	.L4985
.L4702:
	movzbl	-8(%rcx), %r9d
	movq	-16(%rcx), %r13
	subq	$16, %rcx
	movq	%rcx, 192(%r12)
	cmpb	$1, %r9b
	jne	.L4706
.L5130:
	movq	%rcx, %rax
	movl	-84(%r8), %edx
	subq	%r10, %rax
	sarq	$4, %rax
.L4705:
	movzbl	-252(%rbp), %r13d
	movl	%r13d, %r9d
	cmpq	%rdx, %rax
	jbe	.L5006
	movzbl	-8(%rcx), %r11d
	movq	-16(%rcx), %r15
	subq	$16, %rcx
	movq	%rcx, 192(%r12)
	cmpb	%r11b, %r13b
	je	.L5131
	movl	%r13d, %esi
	movzbl	%r11b, %edi
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$10, %r11b
	setne	%sil
	cmpb	$10, %r13b
	setne	%dl
	testb	%dl, %sil
	je	.L5131
	cmpb	$1, %al
	je	.L5131
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	24(%r12), %r14
	movq	%rax, -336(%rbp)
	leaq	.LC482(%rip), %rax
	movq	%rax, -328(%rbp)
	cmpq	24(%r12), %r15
	jnb	.L4718
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	%rax, -328(%rbp)
.L4718:
	movl	%r13d, %edi
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	16(%r12), %rsi
	leaq	.LC482(%rip), %rcx
	movq	%rax, %r13
	cmpq	24(%r12), %rsi
	jnb	.L4719
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	%rax, %rcx
.L4719:
	pushq	-336(%rbp)
	movq	%r13, %r9
	movq	%r15, %rsi
	movq	%r12, %rdi
	pushq	-328(%rbp)
	movl	$1, %r8d
	leaq	.LC531(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	192(%r12), %rcx
	movq	224(%r12), %r8
	movzbl	-252(%rbp), %r13d
	popq	%rsi
	movq	%rcx, %rax
	movl	-84(%r8), %edx
	subq	184(%r12), %rax
	movl	%r13d, %r9d
	sarq	$4, %rax
	popq	%rdi
.L4714:
	cmpq	%rax, %rdx
	jnb	.L4712
	movzbl	-8(%rcx), %r8d
	movq	-16(%rcx), %r15
	subq	$16, %rcx
	movq	%rcx, 192(%r12)
	cmpb	%r9b, %r8b
	je	.L4723
	movzbl	%r8b, %edi
	movl	%r13d, %esi
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$10, %r9b
	setne	%cl
	cmpb	$10, %r8b
	setne	%dl
	testb	%dl, %cl
	je	.L4723
	cmpb	$1, %al
	je	.L4723
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	24(%r12), %r14
	movq	%rax, -336(%rbp)
	leaq	.LC482(%rip), %rax
	movq	%rax, -328(%rbp)
	cmpq	24(%r12), %r15
	jnb	.L4724
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	%rax, -328(%rbp)
.L4724:
	movl	%r13d, %edi
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	16(%r12), %rsi
	leaq	.LC482(%rip), %rcx
	movq	%rax, %r13
	cmpq	24(%r12), %rsi
	jnb	.L4725
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	%rax, %rcx
.L4725:
	pushq	-336(%rbp)
	movq	%r13, %r9
	xorl	%eax, %eax
	xorl	%r8d, %r8d
	pushq	-328(%rbp)
	leaq	.LC531(%rip), %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	popq	%rax
	movzbl	-252(%rbp), %r9d
	popq	%rdx
	jmp	.L4723
.L5131:
	movq	%rcx, %rax
	movl	-84(%r8), %edx
	subq	%r10, %rax
	sarq	$4, %rax
	jmp	.L4714
.L4593:
	movb	$5, -88(%r13)
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE17TypeCheckFallThruEv
	testb	%al, %al
	je	.L4595
	cmpb	$0, -72(%r13)
	jne	.L4595
	movb	$1, -8(%r13)
.L4595:
	movl	-84(%r13), %eax
	leaq	176(%r12), %rdi
	salq	$4, %rax
	addq	184(%r12), %rax
	cmpq	192(%r12), %rax
	je	.L4596
	movq	%rax, 192(%r12)
.L4596:
	movq	224(%r12), %rax
	leaq	-256(%rbp), %rdx
	leaq	16(%r12), %rsi
	cmpb	$0, -160(%rax)
	setne	-72(%r13)
	movb	$9, -256(%rbp)
	call	_ZNSt6vectorIN2v88internal4wasm9ValueBaseENS1_13ZoneAllocatorIS3_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L5494:
	leaq	1(%r14), %rsi
	movl	%edi, %ecx
	leaq	.LC588(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	movl	$1, %r14d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L4598
.L5447:
	movzbl	%cl, %edi
	movl	$1, %esi
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$10, %cl
	je	.L4643
	cmpb	$1, %al
	je	.L4643
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	24(%r12), %rdi
	leaq	.LC482(%rip), %rdx
	movq	%rax, %r14
	movq	24(%r12), %rax
	cmpq	%rax, %r13
	jnb	.L4646
	movq	%r13, %rsi
	movq	%rdi, -328(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-328(%rbp), %rdi
	movq	%rax, %rdx
	movq	24(%r12), %rax
.L4646:
	movq	16(%r12), %rsi
	leaq	.LC482(%rip), %rcx
	cmpq	%rax, %rsi
	jnb	.L4647
	movq	%rdx, -328(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-328(%rbp), %rdx
	movq	%rax, %rcx
.L4647:
	pushq	%r14
	leaq	.LC490(%rip), %r9
	xorl	%r8d, %r8d
	movq	%r13, %rsi
	pushq	%rdx
	movq	%r12, %rdi
	leaq	.LC531(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	popq	%r10
	popq	%r11
	jmp	.L4643
.L4952:
	leaq	.LC617(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc
	jmp	.L4948
.L4778:
	testl	%edx, %edx
	jle	.L4779
	leal	-1(%rdx), %ecx
	xorl	%r13d, %r13d
	movq	%r12, %r15
	movq	%rcx, -328(%rbp)
	movq	%r13, %r12
	jmp	.L4789
.L5541:
	cmpb	$2, -72(%rax)
	jne	.L5540
.L4786:
	leaq	1(%r12), %rdx
	cmpq	%r12, -328(%rbp)
	je	.L5428
	movq	112(%r15), %rsi
	movq	224(%r15), %rax
	movq	%rdx, %r12
.L4789:
	movq	192(%r15), %rdx
	movl	-84(%rax), %edi
	movl	%r12d, -336(%rbp)
	movq	%rdx, %rcx
	subq	184(%r15), %rcx
	sarq	$4, %rcx
	cmpq	%rcx, %rdi
	jnb	.L5541
	movq	16(%rsi), %rax
	movzbl	-8(%rdx), %ecx
	subq	$16, %rdx
	movq	(%rdx), %r14
	movzbl	(%rax,%r12), %r8d
	movq	%rdx, 192(%r15)
	cmpb	%r8b, %cl
	je	.L4786
	movzbl	%r8b, %r13d
	movzbl	%cl, %edi
	movl	%r13d, %esi
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$10, %r8b
	setne	%sil
	cmpb	$10, %cl
	setne	%dl
	testb	%dl, %sil
	je	.L4786
	cmpb	$1, %al
	je	.L4786
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	24(%r15), %r8
	movq	%rax, -344(%rbp)
	leaq	.LC482(%rip), %rax
	movq	%rax, -336(%rbp)
	cmpq	24(%r15), %r14
	jnb	.L4787
	movq	%r8, %rdi
	movq	%r14, %rsi
	movq	%r8, -352(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-352(%rbp), %r8
	movq	%rax, -336(%rbp)
.L4787:
	movl	%r13d, %edi
	movq	%r8, -352(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	16(%r15), %rsi
	leaq	.LC482(%rip), %rcx
	movq	%rax, %r13
	cmpq	24(%r15), %rsi
	jnb	.L4788
	movq	-352(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	%rax, %rcx
.L4788:
	pushq	-344(%rbp)
	movq	%r13, %r9
	movl	%r12d, %r8d
	movq	%r14, %rsi
	pushq	-336(%rbp)
	leaq	.LC531(%rip), %rdx
	movq	%r15, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	popq	%r9
	popq	%r10
	jmp	.L4786
.L5540:
	movq	16(%r15), %rsi
	leaq	.LC482(%rip), %rcx
	cmpq	24(%r15), %rsi
	jnb	.L4785
	leaq	24(%r15), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r15), %rsi
	movq	%rax, %rcx
.L4785:
	leaq	.LC530(%rip), %rdx
	movq	%r15, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L4786
.L5428:
	movq	%r15, %r12
	jmp	.L4779
.L5499:
	movzbl	-336(%rbp), %r10d
.L4620:
	leaq	-288(%rbp), %rdx
	leaq	16(%r12), %rsi
	movb	%r10b, -336(%rbp)
	leaq	176(%r12), %rdi
	movb	$9, -288(%rbp)
	call	_ZNSt6vectorIN2v88internal4wasm9ValueBaseENS1_13ZoneAllocatorIS3_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_
	movq	192(%r12), %rax
	movq	-328(%rbp), %rcx
	movzbl	-336(%rbp), %r10d
	movq	%rcx, -16(%rax)
	movb	%r10b, -8(%rax)
	jmp	.L4598
.L5529:
	movzbl	%cl, %edi
	movl	$9, %esi
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$1, %al
	je	.L4554
	cmpb	$10, %cl
	je	.L4554
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	.LC482(%rip), %r15
	leaq	24(%r12), %rdi
	movq	%rax, %r14
	movq	24(%r12), %rax
	cmpq	%rax, %r13
	jnb	.L4557
	movq	%r13, %rsi
	movq	%rdi, -328(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-328(%rbp), %rdi
	movq	%rax, %r15
	movq	24(%r12), %rax
.L4557:
	movq	16(%r12), %rsi
	leaq	.LC482(%rip), %rcx
	cmpq	%rax, %rsi
	jnb	.L4558
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	%rax, %rcx
.L4558:
	pushq	%r14
	movq	%r13, %rsi
	leaq	.LC496(%rip), %r9
	xorl	%r8d, %r8d
	pushq	%r15
	leaq	.LC531(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	popq	%r13
	popq	%r14
	jmp	.L4554
.L5461:
	addq	$1, %rsi
	leaq	.LC588(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	16(%r12), %rsi
	jmp	.L4739
.L5135:
	movq	%rcx, %rax
	movl	-84(%r9), %edx
	subq	%r10, %rax
	sarq	$4, %rax
	jmp	.L4863
.L4993:
	leaq	.LC630(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc
	jmp	.L4325
.L5485:
	leaq	.LC597(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	2(%rax), %r13
	jmp	.L4533
.L5531:
	leaq	.LC624(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	2(%rax), %r13
	jmp	.L4533
.L5530:
	movzbl	%cl, %edi
	movl	$1, %esi
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$10, %cl
	je	.L4881
	cmpb	$1, %al
	je	.L4881
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	24(%r12), %rdi
	leaq	.LC482(%rip), %rdx
	movq	%rax, %r15
	movq	24(%r12), %rax
	cmpq	%rax, %r14
	jnb	.L4884
	movq	%r14, %rsi
	movq	%rdi, -328(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-328(%rbp), %rdi
	movq	%rax, %rdx
	movq	24(%r12), %rax
.L4884:
	movq	16(%r12), %rsi
	leaq	.LC482(%rip), %rcx
	cmpq	%rax, %rsi
	jnb	.L4885
	movq	%rdx, -328(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-328(%rbp), %rdx
	movq	%rax, %rcx
.L4885:
	pushq	%r15
	movq	%r14, %rsi
	leaq	.LC490(%rip), %r9
	xorl	%r8d, %r8d
	pushq	%rdx
	movq	%r12, %rdi
	leaq	.LC531(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	popq	%rcx
	popq	%rsi
	jmp	.L4881
.L4728:
	movl	$1, %r13d
	cmpl	$2, %eax
	jne	.L4729
	jmp	.L4727
.L5538:
	movq	%r9, %rsi
	movl	%r8d, %r9d
	leaq	.LC600(%rip), %rdx
	movl	%ecx, %r8d
	movq	%r12, %rdi
	movl	%r9d, %ecx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
.L4758:
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$1, %r14d
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16TypeCheckBrTableERKSt6vectorINS1_9ValueTypeESaIS8_EE
	testb	%al, %al
	je	.L4767
	movq	224(%r12), %rax
	cmpb	$0, -72(%rax)
	je	.L4768
.L4771:
	movq	-256(%rbp), %rdi
	movq	-240(%rbp), %rsi
	leaq	-288(%rbp), %r13
	cmpq	$0, 56(%rdi)
	jne	.L4770
.L4769:
	movl	-232(%rbp), %eax
	cmpl	-228(%rbp), %eax
	ja	.L4770
	addl	$1, %eax
	xorl	%r8d, %r8d
	leaq	.LC525(%rip), %rcx
	movq	%r13, %rdx
	movl	%eax, -232(%rbp)
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	movq	-256(%rbp), %rdi
	movl	-288(%rbp), %esi
	addq	-240(%rbp), %rsi
	movq	%rsi, -240(%rbp)
	cmpq	$0, 56(%rdi)
	je	.L4769
.L4770:
	subq	-248(%rbp), %rsi
	movq	%r12, %rdi
	leal	1(%rsi), %r14d
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE10EndControlEv
.L4767:
	movq	-320(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4776
	call	_ZdlPv@PLT
.L4776:
	cmpq	$0, -328(%rbp)
	je	.L5433
	movq	-328(%rbp), %rdi
	call	_ZdlPv@PLT
.L5433:
	movq	16(%r12), %rsi
	jmp	.L4752
.L5535:
	addq	$1, %rsi
	movl	%r10d, %ecx
	leaq	.LC599(%rip), %rdx
	xorl	%eax, %eax
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	16(%r12), %rsi
	jmp	.L4752
.L5523:
	leaq	1(%r14), %rsi
	leaq	.LC618(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L4948
.L4674:
	movq	16(%r12), %rax
	movq	%rdx, 224(%r12)
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L4676:
	cmpb	$0, -160(%rax)
	je	.L4677
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L5490:
	movq	%r8, -336(%rbp)
	movb	%dl, -328(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movzbl	-328(%rbp), %edx
	movq	-336(%rbp), %r8
	movq	%rax, %rcx
	jmp	.L4333
.L5495:
	movq	-16(%rax), %rdi
	movzbl	-8(%rax), %r10d
	subq	$16, %rax
	movq	%rax, 192(%r12)
	movq	%rdi, -328(%rbp)
	cmpb	$9, %r10b
	je	.L4605
	movzbl	%r10b, %edi
	movl	$9, %esi
	movb	%r10b, -344(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	movzbl	-344(%rbp), %r10d
	testb	%al, %al
	jne	.L4605
	cmpb	$10, %r10b
	je	.L4605
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	-328(%rbp), %rsi
	leaq	24(%r12), %rdi
	movzbl	-344(%rbp), %r10d
	movq	%rax, %r13
	movq	24(%r12), %rax
	leaq	.LC482(%rip), %r15
	cmpq	%rax, %rsi
	jnb	.L4606
	movb	%r10b, -352(%rbp)
	movq	%rdi, -344(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %r14
	movzbl	-352(%rbp), %r10d
	movq	%rax, %r15
	movq	-344(%rbp), %rdi
	movq	24(%r12), %rax
.L4606:
	leaq	.LC482(%rip), %rcx
	cmpq	%rax, %r14
	jnb	.L4607
	movq	%r14, %rsi
	movb	%r10b, -344(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movzbl	-344(%rbp), %r10d
	movq	%rax, %rcx
.L4607:
	pushq	%r13
	movq	-328(%rbp), %rsi
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	pushq	%r15
	leaq	.LC496(%rip), %r9
	leaq	.LC531(%rip), %rdx
	xorl	%eax, %eax
	movb	%r10b, -344(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	popq	%rsi
	popq	%rdi
	movq	-240(%rbp), %rcx
	movzbl	-344(%rbp), %r10d
	jmp	.L4605
.L4671:
	cmpq	%rax, %rdx
	je	.L4672
	movq	%rdx, 224(%r12)
.L4672:
	movq	%rsi, 16(%r12)
	jmp	.L4325
.L4851:
	movzbl	%cl, %edi
	movl	$1, %esi
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$10, %cl
	je	.L5134
	cmpb	$1, %al
	je	.L5134
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	24(%r12), %rdi
	leaq	.LC482(%rip), %rdx
	movq	%rax, -328(%rbp)
	movq	24(%r12), %rax
	cmpq	%rax, %r14
	jnb	.L4854
	movq	%r14, %rsi
	movq	%rdi, -336(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %r15
	movq	-336(%rbp), %rdi
	movq	%rax, %rdx
	movq	24(%r12), %rax
.L4854:
	leaq	.LC482(%rip), %rcx
	cmpq	%rax, %r15
	jnb	.L4855
	movq	%r15, %rsi
	movq	%rdx, -336(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-336(%rbp), %rdx
	movq	%rax, %rcx
.L4855:
	pushq	-328(%rbp)
	movq	%r14, %rsi
	xorl	%eax, %eax
	xorl	%r8d, %r8d
	pushq	%rdx
	leaq	.LC490(%rip), %r9
	leaq	.LC531(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	80(%r12), %rax
	popq	%r14
	movl	-256(%rbp), %r10d
	popq	%r15
	movq	184(%rax), %rdx
	jmp	.L4849
.L5492:
	leaq	.LC482(%rip), %rcx
	cmpq	24(%r12), %rsi
	jnb	.L4862
	leaq	24(%r12), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %rsi
	movq	%rax, %rcx
.L4862:
	leaq	.LC530(%rip), %rdx
	xorl	%eax, %eax
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	192(%r12), %rcx
	movq	224(%r12), %r9
	movq	%rcx, %rax
	subq	184(%r12), %rax
	movl	-84(%r9), %edx
	sarq	$4, %rax
	jmp	.L4863
.L5537:
	addq	$63, %rax
	shrq	$6, %rax
	leaq	0(,%rax,8), %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, -328(%rbp)
	call	memset@PLT
	jmp	.L5003
.L4706:
	movzbl	%r9b, %edi
	movl	$1, %esi
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$10, %r9b
	je	.L5130
	cmpb	$1, %al
	je	.L5130
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	.LC482(%rip), %r15
	leaq	24(%r12), %rdi
	movq	%rax, %r14
	movq	24(%r12), %rax
	cmpq	%rax, %r13
	jnb	.L4709
	movq	%r13, %rsi
	movq	%rdi, -328(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-328(%rbp), %rdi
	movq	%rax, %r15
	movq	24(%r12), %rax
.L4709:
	movq	16(%r12), %rsi
	leaq	.LC482(%rip), %rcx
	cmpq	%rax, %rsi
	jnb	.L4710
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	%rax, %rcx
.L4710:
	pushq	%r14
	leaq	.LC490(%rip), %r9
	xorl	%eax, %eax
	movq	%r13, %rsi
	pushq	%r15
	movl	$2, %r8d
	leaq	.LC531(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	192(%r12), %rcx
	movq	184(%r12), %r10
	movq	224(%r12), %r8
	popq	%r9
	movq	%rcx, %rax
	popq	%r11
	subq	%r10, %rax
	movl	-84(%r8), %edx
	sarq	$4, %rax
	jmp	.L4705
.L5469:
	leaq	.LC482(%rip), %rcx
	cmpq	24(%r12), %r15
	jnb	.L4915
	movq	%r15, %rsi
	leaq	24(%r12), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %r15
	movq	%rax, %rcx
.L4915:
	xorl	%eax, %eax
	leaq	.LC530(%rip), %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-248(%rbp), %rax
	movq	%rax, -344(%rbp)
	jmp	.L4914
.L4741:
	movq	16(%r12), %rsi
	cmpl	$2, %eax
	jne	.L4744
	jmp	.L4739
.L4579:
	leaq	-184(%rbp), %rax
	xorl	%esi, %esi
	movq	%rax, %xmm0
	movq	%rax, -328(%rbp)
	leaq	-56(%rbp), %rax
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, -192(%rbp)
	movaps	%xmm0, -208(%rbp)
	call	_ZN2v84base11SmallVectorINS_8internal4wasm9ValueBaseELm8EE14resize_no_initEm
	jmp	.L4581
.L5520:
	leaq	.LC482(%rip), %rcx
	cmpq	24(%r12), %r15
	jnb	.L4850
	movq	%r15, %rsi
	leaq	24(%r12), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %r15
	movq	%rax, %rcx
.L4850:
	leaq	.LC530(%rip), %rdx
	xorl	%eax, %eax
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	80(%r12), %rax
	movl	-256(%rbp), %r10d
	movq	184(%rax), %rdx
	jmp	.L4849
.L5525:
	movq	16(%r12), %rsi
	leaq	.LC482(%rip), %rcx
	cmpq	24(%r12), %rsi
	jnb	.L4704
	leaq	24(%r12), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %rsi
	movq	%rax, %rcx
.L4704:
	leaq	.LC530(%rip), %rdx
	xorl	%eax, %eax
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	192(%r12), %rcx
	movq	184(%r12), %r10
	movq	224(%r12), %r8
	movq	%rcx, %rax
	subq	%r10, %rax
	movl	-84(%r8), %edx
	sarq	$4, %rax
	jmp	.L4705
.L5527:
	movq	16(%r12), %rsi
	leaq	.LC482(%rip), %rcx
	cmpq	24(%r12), %rsi
	jnb	.L4722
	leaq	24(%r12), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %rsi
	movq	%rax, %rcx
.L4722:
	leaq	.LC530(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movzbl	-252(%rbp), %r9d
	jmp	.L4723
.L5526:
	movq	16(%r12), %rsi
	leaq	.LC482(%rip), %rcx
	cmpq	24(%r12), %rsi
	jnb	.L4713
	leaq	24(%r12), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %rsi
	movq	%rax, %rcx
.L4713:
	leaq	.LC530(%rip), %rdx
	xorl	%eax, %eax
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	192(%r12), %rcx
	movzbl	-252(%rbp), %r13d
	movq	224(%r12), %r8
	movq	%rcx, %rax
	subq	184(%r12), %rax
	movl	%r13d, %r9d
	movl	-84(%r8), %edx
	sarq	$4, %rax
	jmp	.L4714
.L5493:
	leaq	.LC482(%rip), %rcx
	cmpq	%r15, %r14
	jnb	.L4871
	leaq	24(%r12), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %rsi
	movq	%rax, %rcx
.L4871:
	leaq	.LC530(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%r12), %r15
	movq	16(%r12), %rsi
	jmp	.L4870
.L4872:
	movzbl	%r8b, %edi
	movl	$1, %esi
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$10, %r8b
	je	.L4873
	cmpb	$1, %al
	je	.L4873
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	.LC482(%rip), %r15
	leaq	24(%r12), %rdi
	movq	%rax, %r14
	movq	24(%r12), %rax
	cmpq	%rax, %r13
	jnb	.L4874
	movq	%r13, %rsi
	movq	%rdi, -336(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-336(%rbp), %rdi
	movq	%rax, %r15
	movq	24(%r12), %rax
.L4874:
	movq	16(%r12), %rsi
	leaq	.LC482(%rip), %rcx
	cmpq	%rax, %rsi
	jnb	.L4875
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	%rax, %rcx
.L4875:
	pushq	%r14
	xorl	%r8d, %r8d
	movq	%r13, %rsi
	movq	%r12, %rdi
	pushq	%r15
	leaq	.LC490(%rip), %r9
	leaq	.LC531(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	popq	%rdi
	movq	24(%r12), %r15
	movq	16(%r12), %rsi
	popq	%r8
	jmp	.L4870
.L5539:
	movq	16(%r12), %r14
	jmp	.L4861
.L5496:
	leaq	.LC482(%rip), %rcx
	cmpq	24(%r12), %r14
	jnb	.L4604
	movq	%r14, %rsi
	leaq	24(%r12), %rdi
	movb	%r10b, -328(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %r14
	movzbl	-328(%rbp), %r10d
	movq	%rax, %rcx
.L4604:
	movq	%r14, %rsi
	leaq	.LC530(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	movb	%r10b, -328(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	16(%r12), %r14
	movq	-240(%rbp), %rcx
	movzbl	-328(%rbp), %r10d
	jmp	.L4603
.L4675:
	movq	16(%r12), %rax
	movq	%rdx, 224(%r12)
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L5536:
	movl	%r10d, %ecx
	leaq	.LC512(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	16(%r12), %rsi
	jmp	.L4752
.L4938:
	movq	8(%r15), %r13
	leaq	-184(%rbp), %rax
	leaq	-208(%rbp), %rdi
	movq	%rax, %xmm0
	movq	%rax, -328(%rbp)
	leaq	-56(%rbp), %rax
	punpcklqdq	%xmm0, %xmm0
	movslq	%r13d, %rsi
	movl	%r13d, %r14d
	movq	%rax, -192(%rbp)
	movaps	%xmm0, -208(%rbp)
	call	_ZN2v84base11SmallVectorINS_8internal4wasm9ValueBaseELm8EE14resize_no_initEm
	subl	$1, %r14d
	js	.L4998
	movslq	%r14d, %rax
	movq	%r15, -376(%rbp)
	movq	%r12, %r15
	movq	%rax, -344(%rbp)
	jmp	.L4946
.L5544:
	cmpb	$2, -72(%rcx)
	movq	16(%r15), %r13
	jne	.L5542
.L4941:
	movb	$10, -352(%rbp)
.L4943:
	movq	-344(%rbp), %rdi
	movzbl	-352(%rbp), %ecx
	subl	$1, %r14d
	movq	%rdi, %rax
	subq	$1, %rdi
	salq	$4, %rax
	addq	-208(%rbp), %rax
	movq	%rdi, -344(%rbp)
	movq	%r13, (%rax)
	movb	%cl, 8(%rax)
	cmpl	$-1, %r14d
	je	.L5543
.L4946:
	movq	224(%r15), %rcx
	movq	192(%r15), %rax
	movl	-84(%rcx), %esi
	movq	%rax, %rdx
	subq	184(%r15), %rdx
	sarq	$4, %rdx
	cmpq	%rdx, %rsi
	jnb	.L5544
	movq	-376(%rbp), %rdi
	movq	-344(%rbp), %rdx
	subq	$16, %rax
	movzbl	8(%rax), %r9d
	movq	(%rax), %r13
	addq	16(%rdi), %rdx
	addq	(%rdi), %rdx
	movzbl	(%rdx), %ecx
	movb	%r9b, -352(%rbp)
	movq	%rax, 192(%r15)
	cmpb	%cl, %r9b
	je	.L4943
	movzbl	%cl, %r12d
	movzbl	%r9b, %edi
	movl	%r12d, %esi
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$10, %cl
	setne	%cl
	cmpb	$10, %r9b
	setne	%dl
	testb	%dl, %cl
	je	.L4943
	cmpb	$1, %al
	je	.L4943
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	24(%r15), %r8
	movq	%rax, -368(%rbp)
	leaq	.LC482(%rip), %rax
	movq	%rax, -360(%rbp)
	cmpq	24(%r15), %r13
	jnb	.L4944
	movq	%r8, %rdi
	movq	%r13, %rsi
	movq	%r8, -384(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-384(%rbp), %r8
	movq	%rax, -360(%rbp)
.L4944:
	movl	%r12d, %edi
	movq	%r8, -384(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	16(%r15), %rsi
	leaq	.LC482(%rip), %rcx
	movq	%rax, %r12
	cmpq	24(%r15), %rsi
	jnb	.L4945
	movq	-384(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	%rax, %rcx
.L4945:
	pushq	-368(%rbp)
	movl	%r14d, %r8d
	movq	%r15, %rdi
	movq	%r12, %r9
	pushq	-360(%rbp)
	leaq	.LC531(%rip), %rdx
	movq	%r13, %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	popq	%rdi
	popq	%r8
	jmp	.L4943
.L5543:
	movq	%r15, %r12
.L4998:
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE10EndControlEv
	movq	-208(%rbp), %rdi
	movl	-336(%rbp), %r13d
	cmpq	-328(%rbp), %rdi
	je	.L4934
	call	free@PLT
	jmp	.L4934
.L4957:
	movq	224(%r12), %rcx
	movq	192(%r12), %rax
	movl	-84(%rcx), %esi
	movq	%rax, %rdx
	subq	184(%r12), %rdx
	sarq	$4, %rdx
	cmpq	%rdx, %rsi
	jb	.L5545
	cmpb	$2, -72(%rcx)
	jne	.L5546
.L5004:
	movq	8(%r15), %r14
	leaq	-184(%rbp), %rax
	leaq	-208(%rbp), %rdi
	movq	%rax, %xmm0
	movq	%rax, -328(%rbp)
	leaq	-56(%rbp), %rax
	punpcklqdq	%xmm0, %xmm0
	movslq	%r14d, %rsi
	movq	%rax, -192(%rbp)
	movaps	%xmm0, -208(%rbp)
	call	_ZN2v84base11SmallVectorINS_8internal4wasm9ValueBaseELm8EE14resize_no_initEm
	subl	$1, %r14d
	js	.L4966
	movslq	%r14d, %rax
	movq	%r15, -376(%rbp)
	movq	%r12, %r15
	movq	%rax, -344(%rbp)
	jmp	.L4974
.L5549:
	cmpb	$2, -72(%rcx)
	movq	16(%r15), %r13
	jne	.L5547
.L4968:
	movb	$10, -352(%rbp)
.L4970:
	movq	-344(%rbp), %rdi
	movzbl	-352(%rbp), %ecx
	subl	$1, %r14d
	movq	%rdi, %rax
	subq	$1, %rdi
	salq	$4, %rax
	addq	-208(%rbp), %rax
	movq	%rdi, -344(%rbp)
	movq	%r13, (%rax)
	movb	%cl, 8(%rax)
	cmpl	$-1, %r14d
	je	.L5548
.L4974:
	movq	224(%r15), %rcx
	movq	192(%r15), %rax
	movl	-84(%rcx), %esi
	movq	%rax, %rdx
	subq	184(%r15), %rdx
	sarq	$4, %rdx
	cmpq	%rdx, %rsi
	jnb	.L5549
	movq	-376(%rbp), %rdi
	movq	-344(%rbp), %rdx
	subq	$16, %rax
	movzbl	8(%rax), %r9d
	movq	(%rax), %r13
	addq	16(%rdi), %rdx
	addq	(%rdi), %rdx
	movzbl	(%rdx), %ecx
	movb	%r9b, -352(%rbp)
	movq	%rax, 192(%r15)
	cmpb	%cl, %r9b
	je	.L4970
	movzbl	%cl, %r12d
	movzbl	%r9b, %edi
	movl	%r12d, %esi
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$10, %cl
	setne	%cl
	cmpb	$10, %r9b
	setne	%dl
	testb	%dl, %cl
	je	.L4970
	cmpb	$1, %al
	je	.L4970
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	24(%r15), %r8
	movq	%rax, -368(%rbp)
	leaq	.LC482(%rip), %rax
	movq	%rax, -360(%rbp)
	cmpq	24(%r15), %r13
	jnb	.L4971
	movq	%r8, %rdi
	movq	%r13, %rsi
	movq	%r8, -384(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-384(%rbp), %r8
	movq	%rax, -360(%rbp)
.L4971:
	movl	%r12d, %edi
	movq	%r8, -384(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	16(%r15), %rsi
	leaq	.LC482(%rip), %rcx
	movq	%rax, %r12
	cmpq	24(%r15), %rsi
	jnb	.L4972
	movq	-384(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	%rax, %rcx
.L4972:
	pushq	-368(%rbp)
	xorl	%eax, %eax
	movq	%r12, %r9
	movl	%r14d, %r8d
	pushq	-360(%rbp)
	leaq	.LC531(%rip), %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	popq	%rax
	popq	%rdx
	jmp	.L4970
.L5547:
	leaq	.LC482(%rip), %rcx
	cmpq	%r13, 24(%r15)
	jbe	.L4969
	movq	%r13, %rsi
	leaq	24(%r15), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r15), %r13
	movq	%rax, %rcx
.L4969:
	movq	%r13, %rsi
	leaq	.LC530(%rip), %rdx
	movq	%r15, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	16(%r15), %r13
	jmp	.L4968
.L5545:
	movzbl	-8(%rax), %ecx
	movq	-16(%rax), %r13
	subq	$16, %rax
	movq	%rax, 192(%r12)
	cmpb	$1, %cl
	je	.L5004
	movzbl	%cl, %edi
	movl	$1, %esi
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	subb	$1, %al
	je	.L5004
	cmpb	$10, %cl
	je	.L5004
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	.LC482(%rip), %r15
	leaq	24(%r12), %rdi
	movq	%rax, -328(%rbp)
	cmpq	24(%r12), %r13
	jnb	.L4962
	movq	%r13, %rsi
	movq	%rdi, -344(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %r14
	movq	-344(%rbp), %rdi
	movq	%rax, %r15
.L4962:
	leaq	.LC482(%rip), %rcx
	cmpq	24(%r12), %r14
	jnb	.L4963
	movq	%r14, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	%rax, %rcx
.L4963:
	pushq	-328(%rbp)
	movq	%r13, %rsi
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	pushq	%r15
	leaq	.LC490(%rip), %r9
	leaq	.LC531(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	popq	%rcx
	popq	%rsi
.L4961:
	movq	-248(%rbp), %r15
	testq	%r15, %r15
	jne	.L5004
	leaq	-184(%rbp), %rax
	leaq	-208(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rax, %xmm0
	movq	%rax, -328(%rbp)
	leaq	-56(%rbp), %rax
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, -192(%rbp)
	movaps	%xmm0, -208(%rbp)
	call	_ZN2v84base11SmallVectorINS_8internal4wasm9ValueBaseELm8EE14resize_no_initEm
.L4966:
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE10EndControlEv
	movq	-208(%rbp), %rdi
	cmpq	-328(%rbp), %rdi
	je	.L4948
	call	free@PLT
	jmp	.L4948
.L5548:
	movq	%r15, %r12
	jmp	.L4966
.L5497:
	movl	$1, %r14d
	cmpl	$2, %eax
	je	.L4598
	jmp	.L4615
.L5542:
	leaq	.LC482(%rip), %rcx
	cmpq	%r13, 24(%r15)
	jbe	.L4942
	movq	%r13, %rsi
	leaq	24(%r15), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r15), %r13
	movq	%rax, %rcx
.L4942:
	movq	%r13, %rsi
	leaq	.LC530(%rip), %rdx
	movq	%r15, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	16(%r15), %r13
	jmp	.L4941
.L4677:
	movb	$1, -160(%rax)
	movq	16(%r12), %rax
	movq	24(%r12), %r15
	leaq	1(%rax), %r13
	jmp	.L4533
.L5488:
	leaq	.LC506(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L5546:
	leaq	.LC482(%rip), %rcx
	cmpq	24(%r12), %r14
	jnb	.L4960
	movq	%r14, %rsi
	leaq	24(%r12), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %r14
	movq	%rax, %rcx
.L4960:
	leaq	.LC530(%rip), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L4961
.L5437:
	leaq	.LC611(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L4762:
	movq	%r9, %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE24UpdateBrTableResultTypesEPSt6vectorINS1_9ValueTypeESaIS8_EEjPKhi
	testb	%al, %al
	jne	.L4761
	jmp	.L4758
.L5491:
	cmpq	$24403223, %rcx
	movl	$24403223, %r13d
	cmova	%r13, %rcx
	imulq	$88, %rcx, %r13
	movq	%r13, %rsi
	jmp	.L4330
.L5442:
	call	__stack_chk_fail@PLT
.L4768:
	subq	216(%r12), %rax
	movabsq	$3353953467947191203, %rdx
	sarq	$3, %rax
	imulq	%rdx, %rax
	testl	%eax, %eax
	jle	.L4771
	leal	-1(%rax), %edi
	movl	$1, %esi
	movq	$-88, %rax
	jmp	.L4775
.L5550:
	movq	%rdx, %r13
.L4775:
	movl	%r13d, %ecx
	movq	%rsi, %r9
	movq	%r13, %rdx
	salq	%cl, %r9
	shrq	$6, %rdx
	movq	%r9, %rcx
	movq	-328(%rbp), %r9
	testq	%rcx, (%r9,%rdx,8)
	je	.L4772
	movq	224(%r12), %rcx
	addq	%rax, %rcx
	cmpb	$3, (%rcx)
	leaq	24(%rcx), %r8
	leaq	56(%rcx), %rdx
	cmove	%r8, %rdx
	movb	$1, 24(%rdx)
.L4772:
	leaq	1(%r13), %rdx
	subq	$88, %rax
	cmpq	%r13, %rdi
	jne	.L5550
	jmp	.L4771
.L5534:
	movl	$10, %r13d
	movl	$10, %r15d
	jmp	.L4693
	.cfi_endproc
.LFE22650:
	.size	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE18DecodeFunctionBodyEv, .-_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE18DecodeFunctionBodyEv
	.section	.rodata._ZN2v88internal4wasm14VerifyWasmCodeEPNS0_19AccountingAllocatorERKNS1_12WasmFeaturesEPKNS1_10WasmModuleEPS4_RKNS1_12FunctionBodyE.str1.8,"aMS",@progbits,1
	.align 8
.LC632:
	.string	"../deps/v8/src/wasm/function-body-decoder.cc:49"
	.section	.rodata._ZN2v88internal4wasm14VerifyWasmCodeEPNS0_19AccountingAllocatorERKNS1_12WasmFeaturesEPKNS1_10WasmModuleEPS4_RKNS1_12FunctionBodyE.str1.1,"aMS",@progbits,1
.LC633:
	.string	"function body end < start"
	.section	.rodata._ZN2v88internal4wasm14VerifyWasmCodeEPNS0_19AccountingAllocatorERKNS1_12WasmFeaturesEPKNS1_10WasmModuleEPS4_RKNS1_12FunctionBodyE.str1.8
	.align 8
.LC634:
	.string	"unterminated control structure"
	.align 8
.LC635:
	.string	"function body must end with \"end\" opcode"
	.section	.text._ZN2v88internal4wasm14VerifyWasmCodeEPNS0_19AccountingAllocatorERKNS1_12WasmFeaturesEPKNS1_10WasmModuleEPS4_RKNS1_12FunctionBodyE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm14VerifyWasmCodeEPNS0_19AccountingAllocatorERKNS1_12WasmFeaturesEPKNS1_10WasmModuleEPS4_RKNS1_12FunctionBodyE
	.type	_ZN2v88internal4wasm14VerifyWasmCodeEPNS0_19AccountingAllocatorERKNS1_12WasmFeaturesEPKNS1_10WasmModuleEPS4_RKNS1_12FunctionBodyE, @function
_ZN2v88internal4wasm14VerifyWasmCodeEPNS0_19AccountingAllocatorERKNS1_12WasmFeaturesEPKNS1_10WasmModuleEPS4_RKNS1_12FunctionBodyE:
.LFB19598:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	leaq	.LC632(%rip), %rdx
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-304(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-512(%rbp), %r13
	pushq	%r12
	movq	%r13, %xmm2
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$536, %rsp
	movq	%rcx, -544(%rbp)
	movq	%r8, -536(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-160(%rbp), %rax
	movq	%rax, %xmm1
	movq	%rax, -568(%rbp)
	punpcklqdq	%xmm2, %xmm1
	movaps	%xmm1, -560(%rbp)
	call	_ZN2v88internal4ZoneC1EPNS0_19AccountingAllocatorEPKc@PLT
	movq	(%rbx), %r8
	movq	16(%rbx), %rdx
	movq	%r13, -128(%rbp)
	movq	-544(%rbp), %rcx
	movzbl	12(%r15), %eax
	movq	%r13, -96(%rbp)
	movl	8(%rbx), %edi
	movq	24(%rbx), %rsi
	movq	%r8, %xmm3
	leaq	-240(%rbp), %rbx
	movq	-536(%rbp), %xmm0
	movq	%rcx, -224(%rbp)
	movq	(%r15), %rcx
	movb	%al, -204(%rbp)
	leaq	16+_ZTVN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEEE(%rip), %rax
	punpcklqdq	%xmm3, %xmm0
	movq	%rdx, -296(%rbp)
	movq	%rcx, -216(%rbp)
	movl	8(%r15), %ecx
	movq	%rdx, -288(%rbp)
	movq	%rsi, -280(%rbp)
	movl	%edi, -272(%rbp)
	movl	$0, -264(%rbp)
	movq	%rbx, -256(%rbp)
	movq	$0, -248(%rbp)
	movb	$0, -240(%rbp)
	movl	%ecx, -208(%rbp)
	movq	%rax, -304(%rbp)
	movq	%r13, -160(%rbp)
	movq	$0, -152(%rbp)
	movq	$0, -144(%rbp)
	movq	$0, -136(%rbp)
	movq	$0, -120(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movups	%xmm0, -200(%rbp)
	movq	$0, -88(%rbp)
	movdqa	-560(%rbp), %xmm1
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	movups	%xmm1, -184(%rbp)
	cmpq	%rdx, %rsi
	jb	.L5572
	movq	-568(%rbp), %rcx
	movq	%r8, %rdx
	movq	%r14, %rsi
	leaq	-216(%rbp), %rdi
	call	_ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12DecodeLocalsERKNS1_12WasmFeaturesEPS3_PKNS0_9SignatureINS1_9ValueTypeEEEPNS0_10ZoneVectorISB_EE
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE18DecodeFunctionBodyEv
	movq	-80(%rbp), %rcx
	movabsq	$3353953467947191203, %rsi
	movq	%rcx, %rdx
	subq	-88(%rbp), %rdx
	movq	%rdx, %rax
	sarq	$3, %rax
	imulq	%rsi, %rax
	cmpq	$1, %rax
	ja	.L5573
	cmpq	$88, %rdx
	je	.L5574
.L5553:
	movq	-248(%rbp), %r14
	movq	-256(%rbp), %r15
	testq	%r14, %r14
	jne	.L5575
	leaq	32(%r12), %rax
	movb	$0, -320(%rbp)
	movdqa	-320(%rbp), %xmm4
	movq	$0, (%r12)
	movl	$0, 8(%r12)
	movq	%rax, 16(%r12)
	movaps	%xmm4, -368(%rbp)
.L5564:
	movdqa	-368(%rbp), %xmm5
	movups	%xmm5, 32(%r12)
.L5566:
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %rax
	movq	%r14, 24(%r12)
	movq	%rax, -304(%rbp)
	cmpq	%rbx, %r15
	je	.L5567
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L5567:
	movq	%r13, %rdi
	call	_ZN2v88internal4ZoneD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5576
	addq	$536, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5573:
	.cfi_restore_state
	movq	-80(%rcx), %rsi
	leaq	.LC634(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	jmp	.L5553
	.p2align 4,,10
	.p2align 3
.L5572:
	leaq	.LC633(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc
	jmp	.L5553
	.p2align 4,,10
	.p2align 3
.L5575:
	movl	-264(%rbp), %eax
	leaq	-424(%rbp), %r8
	movq	%r8, -440(%rbp)
	movl	%eax, -448(%rbp)
	testq	%r15, %r15
	je	.L5577
	movq	%r14, -520(%rbp)
	cmpq	$15, %r14
	ja	.L5578
	cmpq	$1, %r14
	jne	.L5569
	movzbl	(%r15), %eax
	movb	%al, -424(%rbp)
	movq	%r8, %rax
.L5560:
	movq	%r14, -432(%rbp)
	movb	$0, (%rax,%r14)
	movl	-448(%rbp), %ecx
	movq	-440(%rbp), %rax
	movq	$0, -352(%rbp)
	movl	%ecx, -344(%rbp)
	cmpq	%r8, %rax
	je	.L5579
	movq	-424(%rbp), %rdx
	movq	-432(%rbp), %r14
	leaq	-320(%rbp), %rsi
	movq	%rax, -336(%rbp)
	movq	%r8, -440(%rbp)
	movq	%rdx, -320(%rbp)
	movq	%r14, -328(%rbp)
	movq	$0, -432(%rbp)
	movb	$0, -424(%rbp)
	movq	$0, -400(%rbp)
	movl	%ecx, -392(%rbp)
	cmpq	%rsi, %rax
	je	.L5562
	movl	%ecx, 8(%r12)
	leaq	32(%r12), %rcx
	movq	-256(%rbp), %r15
	movq	%rcx, 16(%r12)
	leaq	-368(%rbp), %rcx
	movq	%rax, -384(%rbp)
	movq	%rdx, -368(%rbp)
	movq	%r14, -376(%rbp)
	movq	$0, (%r12)
	cmpq	%rcx, %rax
	je	.L5564
	movq	%rax, 16(%r12)
	movq	%rdx, 32(%r12)
	jmp	.L5566
	.p2align 4,,10
	.p2align 3
.L5569:
	movq	%r8, %rdi
.L5559:
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r8, -536(%rbp)
	call	memcpy@PLT
	movq	-520(%rbp), %r14
	movq	-440(%rbp), %rax
	movq	-536(%rbp), %r8
	jmp	.L5560
	.p2align 4,,10
	.p2align 3
.L5578:
	leaq	-440(%rbp), %rdi
	leaq	-520(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r8, -536(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-536(%rbp), %r8
	movq	%rax, -440(%rbp)
	movq	%rax, %rdi
	movq	-520(%rbp), %rax
	movq	%rax, -424(%rbp)
	jmp	.L5559
	.p2align 4,,10
	.p2align 3
.L5579:
	movdqu	-424(%rbp), %xmm6
	movq	-432(%rbp), %r14
	movaps	%xmm6, -320(%rbp)
.L5562:
	movdqa	-320(%rbp), %xmm7
	leaq	32(%r12), %rax
	movq	$0, (%r12)
	movl	%ecx, 8(%r12)
	movq	-256(%rbp), %r15
	movq	%rax, 16(%r12)
	movaps	%xmm7, -368(%rbp)
	jmp	.L5564
	.p2align 4,,10
	.p2align 3
.L5574:
	leaq	.LC635(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc
	jmp	.L5553
.L5576:
	call	__stack_chk_fail@PLT
.L5577:
	leaq	.LC485(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.cfi_endproc
.LFE19598:
	.size	_ZN2v88internal4wasm14VerifyWasmCodeEPNS0_19AccountingAllocatorERKNS1_12WasmFeaturesEPKNS1_10WasmModuleEPS4_RKNS1_12FunctionBodyE, .-_ZN2v88internal4wasm14VerifyWasmCodeEPNS0_19AccountingAllocatorERKNS1_12WasmFeaturesEPKNS1_10WasmModuleEPS4_RKNS1_12FunctionBodyE
	.section	.rodata._ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12OpcodeLengthEPS3_PKh.constprop.0.str1.1,"aMS",@progbits,1
.LC636:
	.string	"numeric_index"
.LC637:
	.string	"simd_index"
.LC638:
	.string	"atomic_index"
	.section	.text._ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12OpcodeLengthEPS3_PKh.constprop.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12OpcodeLengthEPS3_PKh.constprop.0, @function
_ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12OpcodeLengthEPS3_PKh.constprop.0:
.LFB24947:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	(%rsi), %eax
	cmpb	$-46, %al
	ja	.L5581
	cmpb	$1, %al
	jbe	.L5671
	cmpb	$-46, %al
	ja	.L5671
	leaq	.L5585(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12OpcodeLengthEPS3_PKh.constprop.0,"a",@progbits
	.align 4
	.align 4
.L5585:
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5601-.L5585
	.long	.L5601-.L5585
	.long	.L5601-.L5585
	.long	.L5671-.L5585
	.long	.L5601-.L5585
	.long	.L5671-.L5585
	.long	.L5600-.L5585
	.long	.L5671-.L5585
	.long	.L5599-.L5585
	.long	.L5671-.L5585
	.long	.L5598-.L5585
	.long	.L5598-.L5585
	.long	.L5597-.L5585
	.long	.L5671-.L5585
	.long	.L5596-.L5585
	.long	.L5595-.L5585
	.long	.L5596-.L5585
	.long	.L5595-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5594-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5593-.L5585
	.long	.L5593-.L5585
	.long	.L5593-.L5585
	.long	.L5592-.L5585
	.long	.L5592-.L5585
	.long	.L5591-.L5585
	.long	.L5591-.L5585
	.long	.L5671-.L5585
	.long	.L5590-.L5585
	.long	.L5590-.L5585
	.long	.L5590-.L5585
	.long	.L5590-.L5585
	.long	.L5590-.L5585
	.long	.L5590-.L5585
	.long	.L5590-.L5585
	.long	.L5590-.L5585
	.long	.L5590-.L5585
	.long	.L5590-.L5585
	.long	.L5590-.L5585
	.long	.L5590-.L5585
	.long	.L5590-.L5585
	.long	.L5590-.L5585
	.long	.L5590-.L5585
	.long	.L5590-.L5585
	.long	.L5590-.L5585
	.long	.L5590-.L5585
	.long	.L5590-.L5585
	.long	.L5590-.L5585
	.long	.L5590-.L5585
	.long	.L5590-.L5585
	.long	.L5590-.L5585
	.long	.L5589-.L5585
	.long	.L5589-.L5585
	.long	.L5588-.L5585
	.long	.L5587-.L5585
	.long	.L5586-.L5585
	.long	.L5672-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5671-.L5585
	.long	.L5584-.L5585
	.section	.text._ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12OpcodeLengthEPS3_PKh.constprop.0
.L5672:
	movl	$9, %r12d
.L5580:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5689
	addq	$72, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L5586:
	.cfi_restore_state
	movl	$5, %r12d
	jmp	.L5580
.L5671:
	movl	$1, %r12d
	jmp	.L5580
	.p2align 4,,10
	.p2align 3
.L5581:
	cmpb	$-3, %al
	je	.L5602
	cmpb	$-2, %al
	jne	.L5690
	movq	24(%rdi), %rax
	leaq	1(%rsi), %r14
	cmpq	%rax, %r14
	ja	.L5666
	cmpl	%r14d, %eax
	je	.L5666
	movzbl	1(%rsi), %eax
	movl	$3, %r12d
	orb	$-2, %ah
	cmpl	$65027, %eax
	je	.L5580
	jg	.L5667
	jne	.L5669
.L5668:
	leaq	.LC505(%rip), %rdx
	movq	%r13, %rsi
	movl	$2, %r12d
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	jmp	.L5580
	.p2align 4,,10
	.p2align 3
.L5690:
	cmpb	$-4, %al
	jne	.L5671
	movq	24(%rdi), %rdx
	leaq	1(%rsi), %r14
	cmpq	%rdx, %r14
	ja	.L5627
	cmpl	%r14d, %edx
	je	.L5627
	movzbl	1(%rsi), %eax
	orb	$-4, %ah
	subl	$64512, %eax
	cmpl	$17, %eax
	ja	.L5628
	leaq	.L5630(%rip), %rcx
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12OpcodeLengthEPS3_PKh.constprop.0
	.align 4
	.align 4
.L5630:
	.long	.L5678-.L5630
	.long	.L5678-.L5630
	.long	.L5678-.L5630
	.long	.L5678-.L5630
	.long	.L5678-.L5630
	.long	.L5678-.L5630
	.long	.L5678-.L5630
	.long	.L5678-.L5630
	.long	.L5637-.L5630
	.long	.L5636-.L5630
	.long	.L5635-.L5630
	.long	.L5634-.L5630
	.long	.L5633-.L5630
	.long	.L5632-.L5630
	.long	.L5631-.L5630
	.long	.L5629-.L5630
	.long	.L5629-.L5630
	.long	.L5629-.L5630
	.section	.text._ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12OpcodeLengthEPS3_PKh.constprop.0
.L5584:
	movabsq	$4294967296, %rax
	leaq	-92(%rbp), %rdx
	leaq	1(%rsi), %rsi
	leaq	.LC607(%rip), %rcx
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_.constprop.0
	movl	-92(%rbp), %eax
	leal	1(%rax), %r12d
	jmp	.L5580
.L5587:
	leaq	-88(%rbp), %rdx
	leaq	1(%rsi), %rsi
	xorl	%r8d, %r8d
	leaq	.LC604(%rip), %rcx
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIlLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	movl	-88(%rbp), %eax
	leal	1(%rax), %r12d
	jmp	.L5580
.L5588:
	leaq	-92(%rbp), %rdx
	leaq	1(%rsi), %rsi
	xorl	%r8d, %r8d
	leaq	.LC603(%rip), %rcx
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIiLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	movl	-92(%rbp), %eax
	leal	1(%rax), %r12d
	jmp	.L5580
.L5589:
	movq	24(%rdi), %rax
	leaq	1(%rsi), %rsi
	cmpq	%rax, %rsi
	ja	.L5624
	cmpl	%esi, %eax
	je	.L5624
	movzbl	1(%r13), %ecx
	testl	%ecx, %ecx
	je	.L5678
	leaq	.LC537(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	.p2align 4,,10
	.p2align 3
.L5678:
	movl	$2, %r12d
	jmp	.L5580
.L5590:
	leaq	-96(%rbp), %r8
	movq	%rsi, %rdx
	movl	$-1, %ecx
	movq	%rdi, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE1EEC1EPS3_PKhj
	movl	-88(%rbp), %eax
	leal	1(%rax), %r12d
	jmp	.L5580
.L5591:
	movabsq	$4294967296, %rax
	leaq	-92(%rbp), %rdx
	leaq	1(%rsi), %rsi
	leaq	.LC539(%rip), %rcx
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_.constprop.0
	movl	-92(%rbp), %eax
	leal	1(%rax), %r12d
	jmp	.L5580
.L5592:
	leaq	-80(%rbp), %rdx
	leaq	1(%rsi), %rsi
	movb	$0, -92(%rbp)
	leaq	.LC612(%rip), %rcx
	movq	$0, -88(%rbp)
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_.constprop.0
	movl	-80(%rbp), %eax
	leal	1(%rax), %r12d
	jmp	.L5580
.L5593:
	leaq	-88(%rbp), %rdx
	leaq	1(%rsi), %rsi
	movb	$0, -92(%rbp)
	leaq	.LC609(%rip), %rcx
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_.constprop.0
	movl	-88(%rbp), %eax
	leal	1(%rax), %r12d
	jmp	.L5580
.L5594:
	movq	24(%rdi), %rax
	leaq	1(%rsi), %r14
	cmpq	%rax, %r14
	jb	.L5691
	leaq	.LC522(%rip), %rcx
	movq	%r14, %rsi
	xorl	%eax, %eax
	movq	%rdi, -104(%rbp)
	leaq	.LC487(%rip), %rdx
	movl	$1, %r12d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-104(%rbp), %rdi
.L5616:
	leaq	.LC523(%rip), %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	jmp	.L5580
.L5595:
	leaq	-96(%rbp), %r9
	movq	%rsi, %r8
	movq	%rdi, %rcx
	movq	_ZN2v88internal4wasmL16kAllWasmFeaturesE(%rip), %rsi
	movabsq	$1099511627775, %rdx
	movq	%r9, %rdi
	andq	8+_ZN2v88internal4wasmL16kAllWasmFeaturesE(%rip), %rdx
	call	_ZN2v88internal4wasm21CallIndirectImmediateILNS1_7Decoder12ValidateFlagE1EEC1ENS1_12WasmFeaturesEPS3_PKh
	movl	-80(%rbp), %eax
	leal	1(%rax), %r12d
	jmp	.L5580
.L5598:
	leaq	-92(%rbp), %rdx
	leaq	1(%rsi), %rsi
	leaq	.LC587(%rip), %rcx
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_.constprop.0
	movl	-92(%rbp), %eax
	leal	1(%rax), %r12d
	jmp	.L5580
.L5599:
	leaq	-92(%rbp), %rdx
	leaq	1(%rsi), %rsi
	xorl	%r8d, %r8d
	movq	%rdi, -104(%rbp)
	leaq	.LC587(%rip), %rcx
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	movq	-104(%rbp), %rdi
	leaq	-72(%rbp), %rdx
	xorl	%r8d, %r8d
	movl	%eax, -96(%rbp)
	movl	-92(%rbp), %eax
	leaq	.LC583(%rip), %rcx
	movq	$0, -80(%rbp)
	leaq	1(%r13,%rax), %rsi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	movl	-72(%rbp), %r12d
	addl	-92(%rbp), %r12d
	addl	$1, %r12d
	jmp	.L5580
.L5600:
	leaq	-80(%rbp), %rdx
	leaq	1(%rsi), %rsi
	movq	$0, -88(%rbp)
	leaq	.LC583(%rip), %rcx
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_.constprop.0
	movl	-80(%rbp), %eax
	leal	1(%rax), %r12d
	jmp	.L5580
.L5601:
	leaq	-96(%rbp), %r8
	movq	%rsi, %rcx
	movq	%rdi, %rdx
	leaq	_ZN2v88internal4wasmL16kAllWasmFeaturesE(%rip), %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE1EEC1ERKNS1_12WasmFeaturesEPS3_PKh
	movl	-96(%rbp), %eax
	leal	1(%rax), %r12d
	jmp	.L5580
.L5596:
	leaq	-80(%rbp), %rdx
	leaq	1(%rsi), %rsi
	movq	$0, -88(%rbp)
	leaq	.LC607(%rip), %rcx
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_.constprop.0
	movl	-80(%rbp), %eax
	leal	1(%rax), %r12d
	jmp	.L5580
.L5597:
	leaq	1(%rsi), %rbx
	leaq	-96(%rbp), %r15
	xorl	%r8d, %r8d
	movq	%rdi, -104(%rbp)
	leaq	.LC598(%rip), %rcx
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movl	$0, -96(%rbp)
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	movq	-104(%rbp), %rdi
	movl	%eax, %r14d
	movl	-96(%rbp), %eax
	cmpq	$0, 56(%rdi)
	leaq	1(%r13,%rax), %r12
	jne	.L5622
	xorl	%r13d, %r13d
	leaq	.LC525(%rip), %rcx
.L5621:
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movq	%r15, %rdx
	movq	%rdi, -104(%rbp)
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	movl	-96(%rbp), %eax
	movq	-104(%rbp), %rdi
	leaq	.LC525(%rip), %rcx
	addq	%rax, %r12
	cmpq	$0, 56(%rdi)
	jne	.L5622
	addl	$1, %r13d
	cmpl	%r13d, %r14d
	jnb	.L5621
.L5622:
	subq	%rbx, %r12
	addl	$1, %r12d
	jmp	.L5580
.L5629:
	leaq	-92(%rbp), %rdx
	leaq	.LC539(%rip), %rcx
	movq	%r14, %rsi
	movabsq	$4294967296, %rax
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_.constprop.0
	movl	-92(%rbp), %eax
	leal	2(%rax), %r12d
	jmp	.L5580
.L5666:
	leaq	.LC638(%rip), %rdx
	movq	%r14, %rsi
	movq	%rdi, -104(%rbp)
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	-104(%rbp), %rdi
.L5669:
	leaq	-96(%rbp), %r8
	movl	$-1, %ecx
	movq	%r14, %rdx
.L5688:
	movq	%rdi, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE1EEC1EPS3_PKhj
	movl	-88(%rbp), %eax
	leal	2(%rax), %r12d
	jmp	.L5580
.L5627:
	leaq	.LC636(%rip), %rdx
	movq	%r14, %rsi
	movl	$2, %r12d
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	jmp	.L5580
.L5631:
	leaq	-96(%rbp), %r8
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal4wasm18TableCopyImmediateILNS1_7Decoder12ValidateFlagE1EEC1EPS3_PKh
	movl	-80(%rbp), %eax
	leal	2(%rax), %r12d
	jmp	.L5580
.L5632:
	leaq	-92(%rbp), %rdx
	leaq	2(%rsi), %rsi
	xorl	%r8d, %r8d
	leaq	.LC542(%rip), %rcx
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIiLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	movl	-92(%rbp), %eax
	leal	2(%rax), %r12d
	jmp	.L5580
.L5633:
	leaq	-96(%rbp), %r8
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal4wasm18TableInitImmediateILNS1_7Decoder12ValidateFlagE1EEC1EPS3_PKh
	movl	-84(%rbp), %eax
	leal	2(%rax), %r12d
	jmp	.L5580
.L5634:
	leaq	-96(%rbp), %r8
	movq	%rdi, %rsi
	movq	%r14, %rdx
	movq	%r8, %rdi
	call	_ZN2v88internal4wasm20MemoryIndexImmediateILNS1_7Decoder12ValidateFlagE1EEC1EPS3_PKh
	movl	-92(%rbp), %eax
	leal	2(%rax), %r12d
	jmp	.L5580
.L5635:
	leaq	2(%rsi), %rsi
	cmpq	%rsi, %rdx
	jb	.L5654
	cmpl	%esi, %edx
	je	.L5654
	movzbl	2(%r13), %ecx
	testl	%ecx, %ecx
	je	.L5656
	leaq	.LC537(%rip), %rdx
	xorl	%eax, %eax
	movq	%rdi, -104(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-104(%rbp), %rdi
	movq	24(%rdi), %rdx
.L5656:
	leaq	3(%r13), %rsi
	cmpq	%rdx, %rsi
	ja	.L5657
	cmpl	%esi, %edx
	je	.L5657
	movzbl	3(%r13), %ecx
	testl	%ecx, %ecx
	je	.L5659
	leaq	.LC537(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
.L5659:
	movl	$4, %r12d
	jmp	.L5580
.L5636:
	leaq	-92(%rbp), %rdx
	leaq	2(%rsi), %rsi
	xorl	%r8d, %r8d
	leaq	.LC549(%rip), %rcx
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIiLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	movl	-92(%rbp), %eax
	leal	2(%rax), %r12d
	jmp	.L5580
.L5637:
	leaq	2(%rsi), %r15
	cmpq	%r15, %rdx
	ja	.L5692
	leaq	.LC487(%rip), %rdx
	movq	%r15, %rsi
	xorl	%eax, %eax
	movq	%rdi, -104(%rbp)
	leaq	.LC549(%rip), %rcx
	movl	$3, %r12d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-104(%rbp), %rdi
	movq	24(%rdi), %rdx
.L5640:
	cmpq	%rdx, %r15
	ja	.L5651
	cmpl	%r15d, %edx
	je	.L5651
	movzbl	1(%r14), %ecx
	testl	%ecx, %ecx
	je	.L5580
	leaq	.LC537(%rip), %rdx
	movq	%r15, %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L5580
	.p2align 4,,10
	.p2align 3
.L5602:
	movq	24(%rdi), %rax
	leaq	1(%rsi), %r12
	cmpq	%rax, %r12
	ja	.L5660
	cmpl	%r12d, %eax
	je	.L5660
	movzbl	1(%rsi), %eax
	orb	$-3, %ah
	subl	$64768, %eax
	cmpl	$209, %eax
	ja	.L5661
	leaq	.L5662(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12OpcodeLengthEPS3_PKh.constprop.0
	.align 4
	.align 4
.L5662:
	.long	.L5665-.L5662
	.long	.L5665-.L5662
	.long	.L5661-.L5662
	.long	.L5664-.L5662
	.long	.L5678-.L5662
	.long	.L5663-.L5662
	.long	.L5661-.L5662
	.long	.L5663-.L5662
	.long	.L5678-.L5662
	.long	.L5663-.L5662
	.long	.L5661-.L5662
	.long	.L5663-.L5662
	.long	.L5678-.L5662
	.long	.L5663-.L5662
	.long	.L5663-.L5662
	.long	.L5678-.L5662
	.long	.L5663-.L5662
	.long	.L5663-.L5662
	.long	.L5678-.L5662
	.long	.L5663-.L5662
	.long	.L5663-.L5662
	.long	.L5678-.L5662
	.long	.L5663-.L5662
	.long	.L5663-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5661-.L5662
	.long	.L5661-.L5662
	.long	.L5678-.L5662
	.long	.L5661-.L5662
	.long	.L5661-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5661-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5661-.L5662
	.long	.L5661-.L5662
	.long	.L5661-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5661-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5661-.L5662
	.long	.L5661-.L5662
	.long	.L5661-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5661-.L5662
	.long	.L5661-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5661-.L5662
	.long	.L5661-.L5662
	.long	.L5661-.L5662
	.long	.L5661-.L5662
	.long	.L5661-.L5662
	.long	.L5661-.L5662
	.long	.L5661-.L5662
	.long	.L5661-.L5662
	.long	.L5661-.L5662
	.long	.L5661-.L5662
	.long	.L5661-.L5662
	.long	.L5661-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5661-.L5662
	.long	.L5661-.L5662
	.long	.L5661-.L5662
	.long	.L5661-.L5662
	.long	.L5661-.L5662
	.long	.L5661-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.long	.L5678-.L5662
	.section	.text._ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12OpcodeLengthEPS3_PKh.constprop.0
.L5661:
	leaq	.LC504(%rip), %rdx
	movq	%r13, %rsi
	movl	$2, %r12d
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	jmp	.L5580
.L5663:
	movl	$3, %r12d
	jmp	.L5580
.L5660:
	leaq	.LC637(%rip), %rdx
	movq	%r12, %rsi
	movq	%rdi, -104(%rbp)
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	-104(%rbp), %rdi
.L5665:
	leaq	-96(%rbp), %r8
	movl	$-1, %ecx
	movq	%r12, %rdx
	jmp	.L5688
.L5664:
	movl	$18, %r12d
	jmp	.L5580
	.p2align 4,,10
	.p2align 3
.L5667:
	subl	$65040, %eax
	cmpl	$62, %eax
	ja	.L5668
	jmp	.L5669
	.p2align 4,,10
	.p2align 3
.L5691:
	movzbl	1(%rsi), %esi
	movl	$2, %r12d
	movl	$1, %edx
	movl	%esi, %ecx
	movl	%esi, %r8d
	andl	$127, %ecx
	andl	$127, %r8d
	testb	%sil, %sil
	js	.L5693
.L5606:
	cmpb	$1, %cl
	jne	.L5616
	leaq	1(%r13,%rdx), %rsi
	addl	$1, %r12d
	cmpq	%rax, %rsi
	ja	.L5619
	cmpl	%esi, %eax
	je	.L5619
	movzbl	(%rsi), %ecx
	subl	$104, %ecx
	cmpb	$23, %cl
	ja	.L5620
	movl	$1, %eax
	salq	%cl, %rax
	testl	$16253313, %eax
	jne	.L5580
.L5620:
	leaq	.LC502(%rip), %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	jmp	.L5580
.L5693:
	leaq	2(%r13), %rsi
	cmpq	%rsi, %rax
	ja	.L5694
	leaq	.LC522(%rip), %rcx
	leaq	.LC487(%rip), %rdx
	xorl	%eax, %eax
	movq	%rdi, -104(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-104(%rbp), %rdi
	jmp	.L5616
.L5624:
	leaq	.LC536(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	jmp	.L5678
.L5619:
	leaq	.LC524(%rip), %rdx
	movq	%rdi, -104(%rbp)
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	-104(%rbp), %rdi
	jmp	.L5620
.L5694:
	movzbl	2(%r13), %edx
	movl	%edx, %ecx
	sall	$7, %ecx
	andl	$16256, %ecx
	orl	%r8d, %ecx
	testb	%dl, %dl
	js	.L5608
	movl	$3, %r12d
	movl	$2, %edx
	jmp	.L5606
.L5628:
	leaq	.LC503(%rip), %rdx
	movl	$2, %r12d
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	jmp	.L5580
.L5608:
	leaq	3(%r13), %rsi
	cmpq	%rsi, %rax
	ja	.L5695
	leaq	.LC522(%rip), %rcx
	xorl	%eax, %eax
	movq	%rdi, -104(%rbp)
	movl	$3, %r12d
	leaq	.LC487(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-104(%rbp), %rdi
	jmp	.L5616
.L5692:
	cmpb	$0, 2(%rsi)
	js	.L5639
	movq	%r15, %r14
	movl	$4, %r12d
	leaq	3(%rsi), %r15
	jmp	.L5640
.L5695:
	movzbl	3(%r13), %esi
	movl	%esi, %edx
	sall	$14, %edx
	andl	$2080768, %edx
	orl	%edx, %ecx
	testb	%sil, %sil
	js	.L5610
	movl	$4, %r12d
	movl	$3, %edx
	jmp	.L5606
.L5639:
	leaq	3(%rsi), %rbx
	cmpq	%rbx, %rdx
	ja	.L5696
	leaq	.LC487(%rip), %rdx
	movq	%rbx, %rsi
	xorl	%eax, %eax
	movq	%rdi, -104(%rbp)
	leaq	.LC549(%rip), %rcx
	movq	%r15, %r14
	movl	$4, %r12d
	movq	%rbx, %r15
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-104(%rbp), %rdi
	movq	24(%rdi), %rdx
	jmp	.L5640
.L5654:
	leaq	.LC536(%rip), %rdx
	movq	%rdi, -104(%rbp)
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	-104(%rbp), %rdi
	movq	24(%rdi), %rdx
	jmp	.L5656
.L5651:
	leaq	.LC536(%rip), %rdx
	movq	%r15, %rsi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	jmp	.L5580
.L5657:
	leaq	.LC536(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	jmp	.L5659
.L5610:
	leaq	4(%r13), %rsi
	cmpq	%rsi, %rax
	ja	.L5697
	leaq	.LC522(%rip), %rcx
	xorl	%eax, %eax
	movq	%rdi, -104(%rbp)
	movl	$4, %r12d
	leaq	.LC487(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-104(%rbp), %rdi
	jmp	.L5616
.L5696:
	cmpb	$0, 3(%rsi)
	js	.L5642
	movq	%rbx, %r14
	leaq	4(%rsi), %r15
	movl	$5, %r12d
	jmp	.L5640
.L5689:
	call	__stack_chk_fail@PLT
.L5697:
	movzbl	4(%r13), %esi
	movl	%esi, %edx
	sall	$21, %edx
	andl	$266338304, %edx
	orl	%ecx, %edx
	testb	%sil, %sil
	js	.L5612
	movl	%edx, %ecx
	movl	$5, %r12d
	movl	$4, %edx
	jmp	.L5606
.L5642:
	leaq	4(%rsi), %r15
	cmpq	%r15, %rdx
	ja	.L5698
	leaq	.LC487(%rip), %rdx
	movq	%r15, %rsi
	xorl	%eax, %eax
	movq	%rdi, -104(%rbp)
	leaq	.LC549(%rip), %rcx
	movq	%rbx, %r14
	movl	$5, %r12d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-104(%rbp), %rdi
	movq	24(%rdi), %rdx
	jmp	.L5640
.L5612:
	leaq	5(%r13), %r12
	cmpq	%r12, %rax
	ja	.L5699
	movq	%r12, %rsi
	leaq	.LC522(%rip), %rcx
	xorl	%eax, %eax
	movq	%rdi, -104(%rbp)
	leaq	.LC487(%rip), %rdx
	movl	$5, %r12d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-104(%rbp), %rdi
	jmp	.L5616
.L5698:
	cmpb	$0, 4(%rsi)
	js	.L5644
	movq	%r15, %r14
	movl	$6, %r12d
	leaq	5(%rsi), %r15
	jmp	.L5640
.L5699:
	movzbl	5(%r13), %ebx
	testb	%bl, %bl
	jns	.L5614
	xorl	%eax, %eax
	leaq	.LC522(%rip), %rcx
	movq	%r12, %rsi
	movq	%rdi, -104(%rbp)
	leaq	.LC487(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	andb	$-16, %bl
	movq	-104(%rbp), %rdi
	je	.L5700
.L5615:
	movq	%r12, %rsi
	leaq	.LC488(%rip), %rdx
	movq	%rdi, -104(%rbp)
	movl	$6, %r12d
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	-104(%rbp), %rdi
	jmp	.L5616
.L5644:
	leaq	5(%rsi), %rbx
	cmpq	%rbx, %rdx
	ja	.L5701
	leaq	.LC487(%rip), %rdx
	movq	%rbx, %rsi
	xorl	%eax, %eax
	movq	%rdi, -104(%rbp)
	leaq	.LC549(%rip), %rcx
	movq	%r15, %r14
	movl	$6, %r12d
	movq	%rbx, %r15
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-104(%rbp), %rdi
	movq	24(%rdi), %rdx
	jmp	.L5640
.L5614:
	movl	%ebx, %ecx
	sall	$28, %ecx
	orl	%edx, %ecx
	andb	$-16, %bl
	jne	.L5615
	movl	$6, %r12d
	movl	$5, %edx
	jmp	.L5606
.L5700:
	movl	$6, %r12d
	jmp	.L5616
.L5701:
	cmpb	$0, 5(%rsi)
	js	.L5646
	movq	%rbx, %r14
	leaq	6(%rsi), %r15
	movl	$7, %r12d
	jmp	.L5640
.L5646:
	leaq	6(%rsi), %rsi
	cmpq	%rsi, %rdx
	jbe	.L5676
	movzbl	6(%r13), %ebx
	movl	$5, %r14d
	testb	%bl, %bl
	js	.L5647
.L5648:
	leal	3(%r14), %r12d
	leaq	1(%r13,%r14), %r14
	leaq	1(%r14), %r15
	andb	$-8, %bl
	je	.L5686
	cmpb	$120, %bl
	je	.L5686
	leaq	.LC488(%rip), %rdx
	movq	%rdi, -104(%rbp)
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	-104(%rbp), %rdi
.L5686:
	movq	24(%rdi), %rdx
	jmp	.L5640
.L5676:
	xorl	%ebx, %ebx
	movl	$4, %r14d
.L5647:
	leaq	.LC549(%rip), %rcx
	leaq	.LC487(%rip), %rdx
	xorl	%eax, %eax
	movq	%rsi, -112(%rbp)
	movq	%rdi, -104(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-112(%rbp), %rsi
	movq	-104(%rbp), %rdi
	jmp	.L5648
	.cfi_endproc
.LFE24947:
	.size	_ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12OpcodeLengthEPS3_PKh.constprop.0, .-_ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12OpcodeLengthEPS3_PKh.constprop.0
	.section	.text._ZN2v88internal4wasm31AnalyzeLoopAssignmentForTestingEPNS0_4ZoneEmPKhS5_,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm31AnalyzeLoopAssignmentForTestingEPNS0_4ZoneEmPKhS5_
	.type	_ZN2v88internal4wasm31AnalyzeLoopAssignmentForTestingEPNS0_4ZoneEmPKhS5_, @function
_ZN2v88internal4wasm31AnalyzeLoopAssignmentForTestingEPNS0_4ZoneEmPKhS5_:
.LFB19621:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-80(%rbp), %rax
	movq	%r14, -144(%rbp)
	movq	%rdx, -136(%rbp)
	movq	%rdx, -128(%rbp)
	movq	%rcx, -120(%rbp)
	movl	$0, -112(%rbp)
	movl	$0, -104(%rbp)
	movq	%rax, -152(%rbp)
	movq	%rax, -96(%rbp)
	movq	$0, -88(%rbp)
	movb	$0, -80(%rbp)
	cmpq	%rcx, %rdx
	jnb	.L5740
	xorl	%r12d, %r12d
	cmpb	$3, (%rdx)
	movq	%rdx, %r13
	jne	.L5702
	movq	16(%rdi), %r15
	movq	24(%rdi), %rax
	movq	%rsi, %rbx
	subq	%r15, %rax
	cmpq	$15, %rax
	jbe	.L5758
	leaq	16(%r15), %rax
	movq	%rax, 16(%rdi)
.L5705:
	movl	%ebx, (%r15)
	cmpl	$64, %ebx
	jle	.L5759
	leal	-1(%rbx), %esi
	movq	$0, 8(%r15)
	sarl	$6, %esi
	addl	$1, %esi
	movl	%esi, 4(%r15)
	movq	16(%rdi), %rax
	movslq	%esi, %rsi
	movq	24(%rdi), %rdx
	salq	$3, %rsi
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L5760
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L5709:
	movl	4(%r15), %edx
	movq	%rax, 8(%r15)
	cmpl	$1, %edx
	je	.L5761
	testl	%edx, %edx
	jle	.L5707
	movq	$0, (%rax)
	cmpl	$1, 4(%r15)
	jle	.L5707
	movl	$8, %edx
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L5712:
	movq	8(%r15), %rcx
	addl	$1, %eax
	movq	$0, (%rcx,%rdx)
	addq	$8, %rdx
	cmpl	%eax, 4(%r15)
	jg	.L5712
.L5707:
	movq	-120(%rbp), %rdi
	movq	-88(%rbp), %rax
	cmpq	%rdi, %r13
	jnb	.L5713
	leal	62(%rbx), %r12d
	subl	$1, %ebx
	movl	%ebx, %edx
	cmovns	%ebx, %r12d
	sarl	$31, %edx
	shrl	$26, %edx
	sarl	$6, %r12d
	leal	(%rbx,%rdx), %ecx
	movslq	%r12d, %r12
	andl	$63, %ecx
	leaq	0(,%r12,8), %rsi
	leaq	.L5718(%rip), %r12
	subl	%edx, %ecx
	movl	$1, %edx
	movq	%rsi, -160(%rbp)
	movq	%rdx, %rsi
	salq	%cl, %rsi
	movl	%ebx, %ecx
	xorl	%ebx, %ebx
	salq	%cl, %rdx
	movq	%rsi, -168(%rbp)
	movq	%rdx, -176(%rbp)
	.p2align 4,,10
	.p2align 3
.L5737:
	testq	%rax, %rax
	jne	.L5762
	cmpb	$64, 0(%r13)
	ja	.L5716
	movzbl	0(%r13), %edx
	movslq	(%r12,%rdx,4), %rdx
	addq	%r12, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal4wasm31AnalyzeLoopAssignmentForTestingEPNS0_4ZoneEmPKhS5_,"a",@progbits
	.align 4
	.align 4
.L5718:
	.long	.L5716-.L5718
	.long	.L5716-.L5718
	.long	.L5721-.L5718
	.long	.L5721-.L5718
	.long	.L5721-.L5718
	.long	.L5716-.L5718
	.long	.L5721-.L5718
	.long	.L5716-.L5718
	.long	.L5716-.L5718
	.long	.L5716-.L5718
	.long	.L5716-.L5718
	.long	.L5720-.L5718
	.long	.L5716-.L5718
	.long	.L5716-.L5718
	.long	.L5716-.L5718
	.long	.L5716-.L5718
	.long	.L5717-.L5718
	.long	.L5717-.L5718
	.long	.L5717-.L5718
	.long	.L5717-.L5718
	.long	.L5716-.L5718
	.long	.L5716-.L5718
	.long	.L5716-.L5718
	.long	.L5716-.L5718
	.long	.L5716-.L5718
	.long	.L5716-.L5718
	.long	.L5716-.L5718
	.long	.L5716-.L5718
	.long	.L5716-.L5718
	.long	.L5716-.L5718
	.long	.L5716-.L5718
	.long	.L5716-.L5718
	.long	.L5716-.L5718
	.long	.L5719-.L5718
	.long	.L5719-.L5718
	.long	.L5716-.L5718
	.long	.L5716-.L5718
	.long	.L5716-.L5718
	.long	.L5716-.L5718
	.long	.L5716-.L5718
	.long	.L5716-.L5718
	.long	.L5716-.L5718
	.long	.L5716-.L5718
	.long	.L5716-.L5718
	.long	.L5716-.L5718
	.long	.L5716-.L5718
	.long	.L5716-.L5718
	.long	.L5716-.L5718
	.long	.L5716-.L5718
	.long	.L5716-.L5718
	.long	.L5716-.L5718
	.long	.L5716-.L5718
	.long	.L5716-.L5718
	.long	.L5716-.L5718
	.long	.L5716-.L5718
	.long	.L5716-.L5718
	.long	.L5716-.L5718
	.long	.L5716-.L5718
	.long	.L5716-.L5718
	.long	.L5716-.L5718
	.long	.L5716-.L5718
	.long	.L5716-.L5718
	.long	.L5716-.L5718
	.long	.L5716-.L5718
	.long	.L5717-.L5718
	.section	.text._ZN2v88internal4wasm31AnalyzeLoopAssignmentForTestingEPNS0_4ZoneEmPKhS5_
	.p2align 4,,10
	.p2align 3
.L5717:
	cmpl	$1, 4(%r15)
	movq	8(%r15), %rax
	je	.L5763
	movq	-160(%rbp), %rsi
	movq	-168(%rbp), %rcx
	orq	%rcx, (%rax,%rsi)
	.p2align 4,,10
	.p2align 3
.L5716:
	leaq	-144(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12OpcodeLengthEPS3_PKh.constprop.0
	movl	%eax, %r8d
.L5757:
	movq	-88(%rbp), %rax
.L5734:
	testl	%ebx, %ebx
	jle	.L5713
.L5722:
	movq	-120(%rbp), %rdi
	addq	%r8, %r13
	cmpq	%r13, %rdi
	ja	.L5737
.L5713:
	testq	%rax, %rax
	movl	$0, %r12d
	movq	-96(%rbp), %rdi
	cmove	%r15, %r12
.L5715:
	movq	%r14, -144(%rbp)
	cmpq	-152(%rbp), %rdi
	je	.L5702
	call	_ZdlPv@PLT
.L5702:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5764
	addq	$152, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5721:
	.cfi_restore_state
	leaq	-144(%rbp), %rdi
	movq	%r13, %rsi
	addl	$1, %ebx
	call	_ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12OpcodeLengthEPS3_PKh.constprop.0
	movl	%eax, %r8d
	movq	-88(%rbp), %rax
	jmp	.L5722
	.p2align 4,,10
	.p2align 3
.L5719:
	leaq	1(%r13), %rsi
	cmpq	%rdi, %rsi
	jb	.L5765
	movl	$1, %r8d
	leaq	-144(%rbp), %rdi
	leaq	.LC609(%rip), %rcx
	xorl	%eax, %eax
	leaq	.LC487(%rip), %rdx
	movl	%r8d, -184(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	-184(%rbp), %r8d
.L5731:
	movl	(%r15), %eax
	xorl	%ecx, %ecx
	testl	%eax, %eax
	jle	.L5757
.L5738:
	cmpl	$1, 4(%r15)
	movq	8(%r15), %rax
	je	.L5766
	movl	%ecx, %edx
	movl	$1, %esi
	sarl	$6, %edx
	salq	%cl, %rsi
	movslq	%edx, %rdx
	orq	%rsi, (%rax,%rdx,8)
	jmp	.L5757
	.p2align 4,,10
	.p2align 3
.L5761:
	movq	$0, 8(%r15)
	jmp	.L5707
	.p2align 4,,10
	.p2align 3
.L5720:
	subl	$1, %ebx
	movl	$1, %r8d
	jmp	.L5734
	.p2align 4,,10
	.p2align 3
.L5765:
	movzbl	1(%r13), %eax
	movl	%eax, %ecx
	andl	$127, %ecx
	testb	%al, %al
	jns	.L5724
	leaq	2(%r13), %rsi
	cmpq	%rdi, %rsi
	jb	.L5767
	leaq	-144(%rbp), %rdi
	leaq	.LC609(%rip), %rcx
	xorl	%eax, %eax
	leaq	.LC487(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	$2, %r8d
	jmp	.L5731
	.p2align 4,,10
	.p2align 3
.L5763:
	orq	-176(%rbp), %rax
	movq	%rax, 8(%r15)
	jmp	.L5716
	.p2align 4,,10
	.p2align 3
.L5724:
	movl	$2, %r8d
.L5726:
	movl	(%r15), %eax
	testl	%eax, %eax
	jle	.L5757
	cmpl	%ecx, %eax
	jbe	.L5757
	jmp	.L5738
	.p2align 4,,10
	.p2align 3
.L5740:
	xorl	%r12d, %r12d
	jmp	.L5702
	.p2align 4,,10
	.p2align 3
.L5759:
	movl	$1, 4(%r15)
	movq	$0, 8(%r15)
	jmp	.L5707
	.p2align 4,,10
	.p2align 3
.L5762:
	movq	-96(%rbp), %rdi
	xorl	%r12d, %r12d
	jmp	.L5715
	.p2align 4,,10
	.p2align 3
.L5767:
	movzbl	2(%r13), %edx
	movl	%edx, %eax
	sall	$7, %eax
	andl	$16256, %eax
	orl	%eax, %ecx
	testb	%dl, %dl
	jns	.L5742
	leaq	3(%r13), %rsi
	cmpq	%rdi, %rsi
	jnb	.L5727
	movzbl	3(%r13), %edx
	movl	%edx, %eax
	sall	$14, %eax
	andl	$2080768, %eax
	orl	%eax, %ecx
	testb	%dl, %dl
	jns	.L5743
	leaq	4(%r13), %rsi
	cmpq	%rdi, %rsi
	jb	.L5768
	leaq	-144(%rbp), %rdi
	leaq	.LC609(%rip), %rcx
	xorl	%eax, %eax
	leaq	.LC487(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	$4, %r8d
	jmp	.L5731
	.p2align 4,,10
	.p2align 3
.L5766:
	btsq	%rcx, %rax
	movq	%rax, 8(%r15)
	jmp	.L5757
	.p2align 4,,10
	.p2align 3
.L5727:
	leaq	-144(%rbp), %rdi
	leaq	.LC609(%rip), %rcx
	xorl	%eax, %eax
	leaq	.LC487(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	$3, %r8d
	jmp	.L5731
	.p2align 4,,10
	.p2align 3
.L5742:
	movl	$3, %r8d
	jmp	.L5726
	.p2align 4,,10
	.p2align 3
.L5758:
	movl	$16, %esi
	movq	%rdi, -160(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-160(%rbp), %rdi
	movq	%rax, %r15
	jmp	.L5705
	.p2align 4,,10
	.p2align 3
.L5760:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L5709
	.p2align 4,,10
	.p2align 3
.L5743:
	movl	$4, %r8d
	jmp	.L5726
.L5768:
	movzbl	4(%r13), %edx
	movl	%edx, %eax
	sall	$21, %eax
	andl	$266338304, %eax
	orl	%eax, %ecx
	testb	%dl, %dl
	jns	.L5744
	leaq	5(%r13), %rsi
	cmpq	%rdi, %rsi
	jnb	.L5745
	movzbl	5(%r13), %r10d
	movl	%r10d, %eax
	sall	$28, %eax
	orl	%eax, %ecx
	testb	%r10b, %r10b
	js	.L5769
	movl	$6, %r8d
.L5730:
	andl	$240, %r10d
	je	.L5726
	leaq	-144(%rbp), %rdi
	leaq	.LC488(%rip), %rdx
	movl	%r8d, -184(%rbp)
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movl	-184(%rbp), %r8d
	jmp	.L5731
.L5744:
	movl	$5, %r8d
	jmp	.L5726
.L5745:
	movl	$5, %r8d
	xorl	%r10d, %r10d
.L5729:
	leaq	.LC609(%rip), %rcx
	leaq	-144(%rbp), %rdi
	xorl	%eax, %eax
	movb	%r10b, -189(%rbp)
	leaq	.LC487(%rip), %rdx
	movl	%r8d, -188(%rbp)
	movq	%rsi, -184(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-184(%rbp), %rsi
	movl	-188(%rbp), %r8d
	xorl	%ecx, %ecx
	movzbl	-189(%rbp), %r10d
	jmp	.L5730
.L5764:
	call	__stack_chk_fail@PLT
.L5769:
	movl	$6, %r8d
	jmp	.L5729
	.cfi_endproc
.LFE19621:
	.size	_ZN2v88internal4wasm31AnalyzeLoopAssignmentForTestingEPNS0_4ZoneEmPKhS5_, .-_ZN2v88internal4wasm31AnalyzeLoopAssignmentForTestingEPNS0_4ZoneEmPKhS5_
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal4wasm16DecodeLocalDeclsERKNS1_12WasmFeaturesEPNS1_14BodyLocalDeclsEPKhS8_,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal4wasm16DecodeLocalDeclsERKNS1_12WasmFeaturesEPNS1_14BodyLocalDeclsEPKhS8_, @function
_GLOBAL__sub_I__ZN2v88internal4wasm16DecodeLocalDeclsERKNS1_12WasmFeaturesEPNS1_14BodyLocalDeclsEPKhS8_:
.LFB24563:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE24563:
	.size	_GLOBAL__sub_I__ZN2v88internal4wasm16DecodeLocalDeclsERKNS1_12WasmFeaturesEPNS1_14BodyLocalDeclsEPKhS8_, .-_GLOBAL__sub_I__ZN2v88internal4wasm16DecodeLocalDeclsERKNS1_12WasmFeaturesEPNS1_14BodyLocalDeclsEPKhS8_
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal4wasm16DecodeLocalDeclsERKNS1_12WasmFeaturesEPNS1_14BodyLocalDeclsEPKhS8_
	.weak	_ZTVN2v88internal4wasm7DecoderE
	.section	.data.rel.ro.local._ZTVN2v88internal4wasm7DecoderE,"awG",@progbits,_ZTVN2v88internal4wasm7DecoderE,comdat
	.align 8
	.type	_ZTVN2v88internal4wasm7DecoderE, @object
	.size	_ZTVN2v88internal4wasm7DecoderE, 40
_ZTVN2v88internal4wasm7DecoderE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal4wasm7DecoderD1Ev
	.quad	_ZN2v88internal4wasm7DecoderD0Ev
	.quad	_ZN2v88internal4wasm7Decoder12onFirstErrorEv
	.weak	_ZTVN2v88internal4wasm16BytecodeIteratorE
	.section	.data.rel.ro.local._ZTVN2v88internal4wasm16BytecodeIteratorE,"awG",@progbits,_ZTVN2v88internal4wasm16BytecodeIteratorE,comdat
	.align 8
	.type	_ZTVN2v88internal4wasm16BytecodeIteratorE, @object
	.size	_ZTVN2v88internal4wasm16BytecodeIteratorE, 40
_ZTVN2v88internal4wasm16BytecodeIteratorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal4wasm16BytecodeIteratorD1Ev
	.quad	_ZN2v88internal4wasm16BytecodeIteratorD0Ev
	.quad	_ZN2v88internal4wasm7Decoder12onFirstErrorEv
	.hidden	_ZTCN2v88internal12StdoutStreamE0_So
	.weak	_ZTCN2v88internal12StdoutStreamE0_So
	.section	.rodata._ZTCN2v88internal12StdoutStreamE0_So,"aG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal12StdoutStreamE0_So, @object
	.size	_ZTCN2v88internal12StdoutStreamE0_So, 80
_ZTCN2v88internal12StdoutStreamE0_So:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.hidden	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE
	.weak	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE
	.section	.rodata._ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE,"aG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE, @object
	.size	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE, 80
_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.weak	_ZTTN2v88internal12StdoutStreamE
	.section	.data.rel.ro.local._ZTTN2v88internal12StdoutStreamE,"awG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTTN2v88internal12StdoutStreamE, @object
	.size	_ZTTN2v88internal12StdoutStreamE, 48
_ZTTN2v88internal12StdoutStreamE:
	.quad	_ZTVN2v88internal12StdoutStreamE+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_So+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_So+64
	.quad	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE+64
	.quad	_ZTVN2v88internal12StdoutStreamE+64
	.weak	_ZTVN2v88internal12StdoutStreamE
	.section	.data.rel.ro.local._ZTVN2v88internal12StdoutStreamE,"awG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTVN2v88internal12StdoutStreamE, @object
	.size	_ZTVN2v88internal12StdoutStreamE, 80
_ZTVN2v88internal12StdoutStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	_ZN2v88internal12StdoutStreamD1Ev
	.quad	_ZN2v88internal12StdoutStreamD0Ev
	.quad	-80
	.quad	-80
	.quad	0
	.quad	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.quad	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.weak	_ZTVN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEEE
	.section	.data.rel.ro.local._ZTVN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEEE,"awG",@progbits,_ZTVN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEEE,comdat
	.align 8
	.type	_ZTVN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEEE, @object
	.size	_ZTVN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEEE, 40
_ZTVN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEEE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEED1Ev
	.quad	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEED0Ev
	.quad	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_14EmptyInterfaceEE12onFirstErrorEv
	.weak	_ZTVN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE0EEE
	.section	.data.rel.ro.local._ZTVN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE0EEE,"awG",@progbits,_ZTVN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE0EEE,comdat
	.align 8
	.type	_ZTVN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE0EEE, @object
	.size	_ZTVN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE0EEE, 40
_ZTVN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE0EEE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE0EED1Ev
	.quad	_ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE0EED0Ev
	.quad	_ZN2v88internal4wasm7Decoder12onFirstErrorEv
	.section	.rodata._ZN2v88internal4wasmL16kAllWasmFeaturesE,"a"
	.align 8
	.type	_ZN2v88internal4wasmL16kAllWasmFeaturesE, @object
	.size	_ZN2v88internal4wasmL16kAllWasmFeaturesE, 13
_ZN2v88internal4wasmL16kAllWasmFeaturesE:
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.zero	1
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC541:
	.long	0
	.long	0
	.long	1
	.long	0
	.align 16
.LC543:
	.long	0
	.long	1
	.long	0
	.long	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
