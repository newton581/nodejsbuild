	.file	"regexp-macro-assembler.cc"
	.text
	.section	.text._ZN2v88internal20RegExpMacroAssembler21AbortedCodeGenerationEv,"axG",@progbits,_ZN2v88internal20RegExpMacroAssembler21AbortedCodeGenerationEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20RegExpMacroAssembler21AbortedCodeGenerationEv
	.type	_ZN2v88internal20RegExpMacroAssembler21AbortedCodeGenerationEv, @function
_ZN2v88internal20RegExpMacroAssembler21AbortedCodeGenerationEv:
.LFB7977:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7977:
	.size	_ZN2v88internal20RegExpMacroAssembler21AbortedCodeGenerationEv, .-_ZN2v88internal20RegExpMacroAssembler21AbortedCodeGenerationEv
	.section	.text._ZN2v88internal20RegExpMacroAssembler13CheckPositionEiPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20RegExpMacroAssembler13CheckPositionEiPNS0_5LabelE
	.type	_ZN2v88internal20RegExpMacroAssembler13CheckPositionEiPNS0_5LabelE, @function
_ZN2v88internal20RegExpMacroAssembler13CheckPositionEiPNS0_5LabelE:
.LFB18351:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movl	$1, %r9d
	movl	$1, %r8d
	movl	$1, %ecx
	movq	264(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE18351:
	.size	_ZN2v88internal20RegExpMacroAssembler13CheckPositionEiPNS0_5LabelE, .-_ZN2v88internal20RegExpMacroAssembler13CheckPositionEiPNS0_5LabelE
	.section	.text._ZN2v88internal20RegExpMacroAssembler26CheckSpecialCharacterClassEtPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20RegExpMacroAssembler26CheckSpecialCharacterClassEtPNS0_5LabelE
	.type	_ZN2v88internal20RegExpMacroAssembler26CheckSpecialCharacterClassEtPNS0_5LabelE, @function
_ZN2v88internal20RegExpMacroAssembler26CheckSpecialCharacterClassEtPNS0_5LabelE:
.LFB18353:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE18353:
	.size	_ZN2v88internal20RegExpMacroAssembler26CheckSpecialCharacterClassEtPNS0_5LabelE, .-_ZN2v88internal20RegExpMacroAssembler26CheckSpecialCharacterClassEtPNS0_5LabelE
	.section	.text._ZN2v88internal26NativeRegExpMacroAssembler16CanReadUnalignedEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26NativeRegExpMacroAssembler16CanReadUnalignedEv
	.type	_ZN2v88internal26NativeRegExpMacroAssembler16CanReadUnalignedEv, @function
_ZN2v88internal26NativeRegExpMacroAssembler16CanReadUnalignedEv:
.LFB18361:
	.cfi_startproc
	endbr64
	movzbl	_ZN2v88internal37FLAG_enable_regexp_unaligned_accessesE(%rip), %eax
	testb	%al, %al
	je	.L5
	movzbl	8(%rdi), %eax
	xorl	$1, %eax
.L5:
	ret
	.cfi_endproc
.LFE18361:
	.size	_ZN2v88internal26NativeRegExpMacroAssembler16CanReadUnalignedEv, .-_ZN2v88internal26NativeRegExpMacroAssembler16CanReadUnalignedEv
	.section	.text._ZN2v88internal20RegExpMacroAssemblerC2EPNS0_7IsolateEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20RegExpMacroAssemblerC2EPNS0_7IsolateEPNS0_4ZoneE
	.type	_ZN2v88internal20RegExpMacroAssemblerC2EPNS0_7IsolateEPNS0_4ZoneE, @function
_ZN2v88internal20RegExpMacroAssemblerC2EPNS0_7IsolateEPNS0_4ZoneE:
.LFB18340:
	.cfi_startproc
	endbr64
	movq	%rsi, %xmm0
	movq	%rdx, %xmm1
	movb	$0, 8(%rdi)
	leaq	16+_ZTVN2v88internal20RegExpMacroAssemblerE(%rip), %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, (%rdi)
	movl	$0, 12(%rdi)
	movups	%xmm0, 16(%rdi)
	ret
	.cfi_endproc
.LFE18340:
	.size	_ZN2v88internal20RegExpMacroAssemblerC2EPNS0_7IsolateEPNS0_4ZoneE, .-_ZN2v88internal20RegExpMacroAssemblerC2EPNS0_7IsolateEPNS0_4ZoneE
	.globl	_ZN2v88internal20RegExpMacroAssemblerC1EPNS0_7IsolateEPNS0_4ZoneE
	.set	_ZN2v88internal20RegExpMacroAssemblerC1EPNS0_7IsolateEPNS0_4ZoneE,_ZN2v88internal20RegExpMacroAssemblerC2EPNS0_7IsolateEPNS0_4ZoneE
	.section	.text._ZN2v88internal20RegExpMacroAssemblerD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20RegExpMacroAssemblerD2Ev
	.type	_ZN2v88internal20RegExpMacroAssemblerD2Ev, @function
_ZN2v88internal20RegExpMacroAssemblerD2Ev:
.LFB18343:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE18343:
	.size	_ZN2v88internal20RegExpMacroAssemblerD2Ev, .-_ZN2v88internal20RegExpMacroAssemblerD2Ev
	.globl	_ZN2v88internal20RegExpMacroAssemblerD1Ev
	.set	_ZN2v88internal20RegExpMacroAssemblerD1Ev,_ZN2v88internal20RegExpMacroAssemblerD2Ev
	.section	.text._ZN2v88internal20RegExpMacroAssemblerD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20RegExpMacroAssemblerD0Ev
	.type	_ZN2v88internal20RegExpMacroAssemblerD0Ev, @function
_ZN2v88internal20RegExpMacroAssemblerD0Ev:
.LFB18345:
	.cfi_startproc
	endbr64
	movl	$32, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE18345:
	.size	_ZN2v88internal20RegExpMacroAssemblerD0Ev, .-_ZN2v88internal20RegExpMacroAssemblerD0Ev
	.section	.text._ZN2v88internal20RegExpMacroAssembler23CheckNotInSurrogatePairEiPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20RegExpMacroAssembler23CheckNotInSurrogatePairEiPNS0_5LabelE
	.type	_ZN2v88internal20RegExpMacroAssembler23CheckNotInSurrogatePairEiPNS0_5LabelE, @function
_ZN2v88internal20RegExpMacroAssembler23CheckNotInSurrogatePairEiPNS0_5LabelE:
.LFB18347:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %r9d
	movl	$1, %r8d
	movl	$1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-48(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdx
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%esi, %ebx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	$0, -48(%rbp)
	call	*264(%rax)
	movq	(%r12), %rax
	movq	%r13, %rcx
	movq	%r12, %rdi
	movl	$57343, %edx
	movl	$56320, %esi
	call	*176(%rax)
	movq	(%r12), %rax
	leal	-1(%rbx), %esi
	movq	%r13, %rdx
	movl	$1, %r9d
	movl	$1, %r8d
	movl	$1, %ecx
	movq	%r12, %rdi
	call	*264(%rax)
	movq	(%r12), %rax
	movq	%r14, %rcx
	movq	%r12, %rdi
	movl	$56319, %edx
	movl	$55296, %esi
	call	*168(%rax)
	movq	(%r12), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*64(%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L16
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L16:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18347:
	.size	_ZN2v88internal20RegExpMacroAssembler23CheckNotInSurrogatePairEiPNS0_5LabelE, .-_ZN2v88internal20RegExpMacroAssembler23CheckNotInSurrogatePairEiPNS0_5LabelE
	.section	.text._ZN2v88internal20RegExpMacroAssembler20LoadCurrentCharacterEiPNS0_5LabelEbii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20RegExpMacroAssembler20LoadCurrentCharacterEiPNS0_5LabelEbii
	.type	_ZN2v88internal20RegExpMacroAssembler20LoadCurrentCharacterEiPNS0_5LabelEbii, @function
_ZN2v88internal20RegExpMacroAssembler20LoadCurrentCharacterEiPNS0_5LabelEbii:
.LFB18352:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	cmpl	$-1, %r9d
	movzbl	%cl, %ecx
	cmove	%r8d, %r9d
	movq	264(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE18352:
	.size	_ZN2v88internal20RegExpMacroAssembler20LoadCurrentCharacterEiPNS0_5LabelEbii, .-_ZN2v88internal20RegExpMacroAssembler20LoadCurrentCharacterEiPNS0_5LabelEbii
	.section	.text._ZN2v88internal26NativeRegExpMacroAssemblerC2EPNS0_7IsolateEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26NativeRegExpMacroAssemblerC2EPNS0_7IsolateEPNS0_4ZoneE
	.type	_ZN2v88internal26NativeRegExpMacroAssemblerC2EPNS0_7IsolateEPNS0_4ZoneE, @function
_ZN2v88internal26NativeRegExpMacroAssemblerC2EPNS0_7IsolateEPNS0_4ZoneE:
.LFB18355:
	.cfi_startproc
	endbr64
	movq	%rsi, %xmm0
	movq	%rdx, %xmm1
	movb	$0, 8(%rdi)
	leaq	16+_ZTVN2v88internal26NativeRegExpMacroAssemblerE(%rip), %rax
	punpcklqdq	%xmm1, %xmm0
	movl	$0, 12(%rdi)
	movq	%rax, (%rdi)
	movups	%xmm0, 16(%rdi)
	ret
	.cfi_endproc
.LFE18355:
	.size	_ZN2v88internal26NativeRegExpMacroAssemblerC2EPNS0_7IsolateEPNS0_4ZoneE, .-_ZN2v88internal26NativeRegExpMacroAssemblerC2EPNS0_7IsolateEPNS0_4ZoneE
	.globl	_ZN2v88internal26NativeRegExpMacroAssemblerC1EPNS0_7IsolateEPNS0_4ZoneE
	.set	_ZN2v88internal26NativeRegExpMacroAssemblerC1EPNS0_7IsolateEPNS0_4ZoneE,_ZN2v88internal26NativeRegExpMacroAssemblerC2EPNS0_7IsolateEPNS0_4ZoneE
	.section	.text._ZN2v88internal26NativeRegExpMacroAssemblerD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26NativeRegExpMacroAssemblerD2Ev
	.type	_ZN2v88internal26NativeRegExpMacroAssemblerD2Ev, @function
_ZN2v88internal26NativeRegExpMacroAssemblerD2Ev:
.LFB18358:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE18358:
	.size	_ZN2v88internal26NativeRegExpMacroAssemblerD2Ev, .-_ZN2v88internal26NativeRegExpMacroAssemblerD2Ev
	.globl	_ZN2v88internal26NativeRegExpMacroAssemblerD1Ev
	.set	_ZN2v88internal26NativeRegExpMacroAssemblerD1Ev,_ZN2v88internal26NativeRegExpMacroAssemblerD2Ev
	.section	.text._ZN2v88internal26NativeRegExpMacroAssemblerD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26NativeRegExpMacroAssemblerD0Ev
	.type	_ZN2v88internal26NativeRegExpMacroAssemblerD0Ev, @function
_ZN2v88internal26NativeRegExpMacroAssemblerD0Ev:
.LFB18360:
	.cfi_startproc
	endbr64
	movl	$32, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE18360:
	.size	_ZN2v88internal26NativeRegExpMacroAssemblerD0Ev, .-_ZN2v88internal26NativeRegExpMacroAssemblerD0Ev
	.section	.text._ZN2v88internal26NativeRegExpMacroAssembler23StringCharacterPositionENS0_6StringEiRKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26NativeRegExpMacroAssembler23StringCharacterPositionENS0_6StringEiRKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE
	.type	_ZN2v88internal26NativeRegExpMacroAssembler23StringCharacterPositionENS0_6StringEiRKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE, @function
_ZN2v88internal26NativeRegExpMacroAssembler23StringCharacterPositionENS0_6StringEiRKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE:
.LFB18362:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	-1(%rdi), %rcx
	leaq	15(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	-1(%rdi), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L40
.L23:
	movq	-1(%rdi), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L41
.L24:
	movq	(%rcx), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L42
.L25:
	movq	(%rcx), %rdx
	movslq	%esi, %rbx
	cmpw	$63, 11(%rdx)
	jbe	.L43
.L26:
	movq	(%rcx), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L44
.L28:
	movq	(%rcx), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L45
.L29:
	movq	(%rax), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
.L38:
	addq	$8, %rsp
	leaq	(%rax,%rbx,2), %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	movq	(%rcx), %rdx
	testb	$7, 11(%rdx)
	jne	.L28
	movq	(%rcx), %rdx
	testb	$8, 11(%rdx)
	jne	.L28
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L45:
	movq	(%rcx), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$2, %dx
	jne	.L29
	movq	(%rcx), %rdx
	testb	$8, 11(%rdx)
	je	.L29
	movq	(%rax), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L41:
	movq	-1(%rdi), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$3, %dx
	jne	.L24
	addl	27(%rdi), %esi
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L42:
	movq	(%rcx), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$5, %dx
	jne	.L25
	movq	(%rax), %rax
	movslq	%esi, %rbx
	movq	-1(%rax), %rdx
	leaq	-1(%rax), %rcx
	addq	$15, %rax
	cmpw	$63, 11(%rdx)
	ja	.L26
.L43:
	movq	(%rcx), %rdx
	testb	$7, 11(%rdx)
	jne	.L26
	movq	(%rcx), %rdx
	testb	$8, 11(%rdx)
	je	.L26
.L39:
	addq	$8, %rsp
	addq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
	movq	-1(%rdi), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L23
.L37:
	movq	15(%rdi), %rax
	leaq	-1(%rax), %rcx
	addq	$15, %rax
	jmp	.L24
	.cfi_endproc
.LFE18362:
	.size	_ZN2v88internal26NativeRegExpMacroAssembler23StringCharacterPositionENS0_6StringEiRKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE, .-_ZN2v88internal26NativeRegExpMacroAssembler23StringCharacterPositionENS0_6StringEiRKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE
	.section	.text._ZN2v88internal26NativeRegExpMacroAssembler20CheckStackGuardStateEPNS0_7IsolateEiNS0_6RegExp10CallOriginEPmNS0_4CodeES6_PPKhSA_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26NativeRegExpMacroAssembler20CheckStackGuardStateEPNS0_7IsolateEiNS0_6RegExp10CallOriginEPmNS0_4CodeES6_PPKhSA_
	.type	_ZN2v88internal26NativeRegExpMacroAssembler20CheckStackGuardStateEPNS0_7IsolateEiNS0_6RegExp10CallOriginEPmNS0_4CodeES6_PPKhSA_, @function
_ZN2v88internal26NativeRegExpMacroAssembler20CheckStackGuardStateEPNS0_7IsolateEiNS0_6RegExp10CallOriginEPmNS0_4CodeES6_PPKhSA_:
.LFB18363:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$104, %rsp
	movq	16(%rbp), %rax
	movl	%esi, -108(%rbp)
	xorl	%esi, %esi
	movq	%r9, -96(%rbp)
	movq	%rax, -120(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -64(%rbp)
	leaq	-64(%rbp), %rdi
	call	_ZNK2v88internal15StackLimitCheck15JsHasOverflowedEm@PLT
	movl	%eax, %r12d
	cmpl	$1, %r14d
	je	.L81
	movq	41088(%r15), %rax
	movq	41112(%r15), %rdi
	addl	$1, 41104(%r15)
	movq	%rax, -104(%rbp)
	movq	41096(%r15), %rax
	movq	%rax, -88(%rbp)
	testq	%rdi, %rdi
	je	.L49
	movq	%rbx, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L50:
	movq	-96(%rbp), %rax
	movq	41112(%r15), %rdi
	movq	(%rax), %rsi
	testq	%rdi, %rdi
	je	.L52
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rcx
.L56:
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andw	$9, %ax
	je	.L72
.L82:
	cmpw	$8, %ax
	je	.L73
	movq	15(%rsi), %rsi
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andw	$9, %ax
	jne	.L82
.L72:
	xorl	%edx, %edx
	testb	%r12b, %r12b
	je	.L57
.L85:
	movq	%r15, %rdi
	call	_ZN2v88internal7Isolate13StackOverflowEv@PLT
	movq	(%r14), %rax
	cmpq	%rax, %rbx
	je	.L58
	addq	0(%r13), %rax
	subq	%rbx, %rax
	movq	%rax, 0(%r13)
	movl	$-1, %r13d
.L59:
	movq	-104(%rbp), %rax
	subl	$1, 41104(%r15)
	movq	%rax, 41088(%r15)
	movq	-88(%rbp), %rax
	cmpq	41096(%r15), %rax
	je	.L46
	movq	%rax, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L46:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L83
	addq	$104, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_restore_state
	movq	%rax, %rdx
	movq	-104(%rbp), %rax
	movq	%rax, %r14
	cmpq	%rdx, %rax
	je	.L84
.L51:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r15)
	movq	%rbx, (%r14)
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L73:
	movl	$1, %edx
	testb	%r12b, %r12b
	jne	.L85
.L57:
	movq	-64(%rbp), %rsi
	movb	%dl, -109(%rbp)
	movq	%rcx, -144(%rbp)
	movq	%rsi, -136(%rbp)
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	movq	-136(%rbp), %rsi
	movq	%rax, %r8
	movq	37544(%rsi), %rax
	movq	-144(%rbp), %rcx
	movzbl	-109(%rbp), %edx
	cmpq	%rax, %r8
	jb	.L86
	movq	(%r14), %rax
	cmpq	%rax, %rbx
	je	.L67
	addq	0(%r13), %rax
	subq	%rbx, %rax
	movq	%rax, 0(%r13)
.L67:
	movq	(%rcx), %rdi
	movq	%rdi, %rsi
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andw	$9, %ax
	je	.L62
.L87:
	cmpw	$8, %ax
	je	.L74
	movq	15(%rsi), %rsi
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andw	$9, %ax
	jne	.L87
.L62:
	movl	$-2, %r13d
	cmpb	%r12b, %dl
	jne	.L59
.L89:
	movq	-96(%rbp), %rax
	movq	-128(%rbp), %r14
	leaq	-65(%rbp), %rdx
	xorl	%r13d, %r13d
	movq	-120(%rbp), %r12
	movl	-108(%rbp), %esi
	movq	%rdi, (%rax)
	movq	(%rcx), %rdi
	movq	(%r14), %rbx
	subq	(%r12), %rbx
	call	_ZN2v88internal26NativeRegExpMacroAssembler23StringCharacterPositionENS0_6StringEiRKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE
	movq	%rax, (%r12)
	addq	%rbx, %rax
	movq	%rax, (%r14)
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L81:
	testb	%al, %al
	jne	.L69
	movq	-64(%rbp), %rbx
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	movq	%rax, %r8
	movq	37544(%rbx), %rax
	cmpq	%rax, %r8
	sbbl	%r13d, %r13d
	andl	$-2, %r13d
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L52:
	movq	41088(%r15), %rcx
	cmpq	%rcx, 41096(%r15)
	je	.L88
.L54:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rcx)
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L74:
	movl	$1, %r12d
	movl	$-2, %r13d
	cmpb	%r12b, %dl
	jne	.L59
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L86:
	leaq	37512(%r15), %rdi
	movb	%dl, -144(%rbp)
	movq	%rcx, -136(%rbp)
	call	_ZN2v88internal10StackGuard16HandleInterruptsEv@PLT
	movq	(%r14), %rsi
	movq	312(%r15), %rdi
	movq	-136(%rbp), %rcx
	movzbl	-144(%rbp), %edx
	cmpq	%rbx, %rsi
	je	.L61
	addq	0(%r13), %rsi
	subq	%rbx, %rsi
	movq	%rsi, 0(%r13)
.L61:
	cmpq	%rax, %rdi
	jne	.L67
.L58:
	movl	$-1, %r13d
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L84:
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r14
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L88:
	movq	%r15, %rdi
	movq	%rsi, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L54
.L69:
	movl	$-1, %r13d
	jmp	.L46
.L83:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18363:
	.size	_ZN2v88internal26NativeRegExpMacroAssembler20CheckStackGuardStateEPNS0_7IsolateEiNS0_6RegExp10CallOriginEPmNS0_4CodeES6_PPKhSA_, .-_ZN2v88internal26NativeRegExpMacroAssembler20CheckStackGuardStateEPNS0_7IsolateEiNS0_6RegExp10CallOriginEPmNS0_4CodeES6_PPKhSA_
	.section	.text._ZN2v88internal26NativeRegExpMacroAssembler7ExecuteENS0_6StringEiPKhS4_PiiPNS0_7IsolateENS0_8JSRegExpE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26NativeRegExpMacroAssembler7ExecuteENS0_6StringEiPKhS4_PiiPNS0_7IsolateENS0_8JSRegExpE
	.type	_ZN2v88internal26NativeRegExpMacroAssembler7ExecuteENS0_6StringEiPKhS4_PiiPNS0_7IsolateENS0_8JSRegExpE, @function
_ZN2v88internal26NativeRegExpMacroAssembler7ExecuteENS0_6StringEiPKhS4_PiiPNS0_7IsolateENS0_8JSRegExpE:
.LFB18365:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%r9d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-64(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$56, %rsp
	movl	%esi, -68(%rbp)
	movq	16(%rbp), %r15
	movq	%rdx, -80(%rbp)
	movq	%rcx, -88(%rbp)
	movq	%r15, %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal16RegExpStackScopeC1EPNS0_7IsolateE@PLT
	movq	-64(%rbp), %rax
	movq	%r12, %rcx
	movq	8(%rax), %rdx
	movq	-1(%rcx), %rax
	movzwl	11(%rax), %eax
	andw	$9, %ax
	je	.L95
.L98:
	cmpw	$8, %ax
	je	.L96
	movq	15(%rcx), %rcx
	movq	-1(%rcx), %rax
	movzwl	11(%rax), %eax
	andw	$9, %ax
	jne	.L98
.L95:
	xorl	%esi, %esi
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L96:
	movl	$1, %esi
.L91:
	leaq	24(%rbp), %rdi
	movq	%rdx, -96(%rbp)
	call	_ZNK2v88internal8JSRegExp4CodeEb@PLT
	movq	-96(%rbp), %rdx
	pushq	24(%rbp)
	movq	%r12, %rdi
	pushq	%r15
	addq	$63, %rax
	movl	%r14d, %r9d
	movq	%rbx, %r8
	movq	-88(%rbp), %rcx
	movl	-68(%rbp), %esi
	pushq	$0
	pushq	%rdx
	movq	-80(%rbp), %rdx
	call	*%rax
	movl	%eax, %r12d
	addq	$32, %rsp
	cmpl	$-1, %eax
	je	.L99
.L93:
	movq	%r13, %rdi
	call	_ZN2v88internal16RegExpStackScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L100
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L99:
	.cfi_restore_state
	movq	12480(%r15), %rax
	cmpq	%rax, 96(%r15)
	jne	.L93
	movq	%r15, %rdi
	call	_ZN2v88internal7Isolate13StackOverflowEv@PLT
	jmp	.L93
.L100:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18365:
	.size	_ZN2v88internal26NativeRegExpMacroAssembler7ExecuteENS0_6StringEiPKhS4_PiiPNS0_7IsolateENS0_8JSRegExpE, .-_ZN2v88internal26NativeRegExpMacroAssembler7ExecuteENS0_6StringEiPKhS4_PiiPNS0_7IsolateENS0_8JSRegExpE
	.section	.text._ZN2v88internal26NativeRegExpMacroAssembler5MatchENS0_6HandleINS0_8JSRegExpEEENS2_INS0_6StringEEEPiiiPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26NativeRegExpMacroAssembler5MatchENS0_6HandleINS0_8JSRegExpEEENS2_INS0_6StringEEEPiiiPNS0_7IsolateE
	.type	_ZN2v88internal26NativeRegExpMacroAssembler5MatchENS0_6HandleINS0_8JSRegExpEEENS2_INS0_6StringEEEPiiiPNS0_7IsolateE, @function
_ZN2v88internal26NativeRegExpMacroAssembler5MatchENS0_6HandleINS0_8JSRegExpEEENS2_INS0_6StringEEEPiiiPNS0_7IsolateE:
.LFB18364:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r11
	movl	%ecx, %r10d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%r8d, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	11(%rdi), %ebx
	movq	-1(%rdi), %rdx
	subl	%r8d, %ebx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L102
	movq	15(%rdi), %rdi
	movl	%r8d, %esi
	leaq	-1(%rdi), %rax
.L103:
	movq	(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$5, %dx
	jne	.L104
	movq	15(%rdi), %rdi
	leaq	-1(%rdi), %rax
.L104:
	movq	%r9, -88(%rbp)
	leaq	-57(%rbp), %rdx
	movl	%r10d, -76(%rbp)
	movq	%r11, -72(%rbp)
	movq	(%rax), %rax
	movzwl	11(%rax), %r12d
	call	_ZN2v88internal26NativeRegExpMacroAssembler23StringCharacterPositionENS0_6StringEiRKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE
	movq	-88(%rbp), %r9
	movq	-72(%rbp), %r11
	movq	%r15, %r8
	shrw	$3, %r12w
	movl	-76(%rbp), %r10d
	movq	(%r14), %rdi
	movq	%rax, %rdx
	xorl	$1, %r12d
	pushq	(%r11)
	movl	%r13d, %esi
	andl	$1, %r12d
	pushq	%r9
	movl	%r10d, %r9d
	movl	%r12d, %ecx
	sall	%cl, %ebx
	movslq	%ebx, %rcx
	addq	%rax, %rcx
	call	_ZN2v88internal26NativeRegExpMacroAssembler7ExecuteENS0_6StringEiPKhS4_PiiPNS0_7IsolateENS0_8JSRegExpE
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L108
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L102:
	.cfi_restore_state
	movq	-1(%rdi), %rdx
	leaq	-1(%rdi), %rax
	movl	%r8d, %esi
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$3, %dx
	jne	.L103
	movq	15(%rdi), %rdx
	addl	27(%rdi), %esi
	leaq	-1(%rdx), %rax
	movq	%rdx, %rdi
	jmp	.L103
.L108:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18364:
	.size	_ZN2v88internal26NativeRegExpMacroAssembler5MatchENS0_6HandleINS0_8JSRegExpEEENS2_INS0_6StringEEEPiiiPNS0_7IsolateE, .-_ZN2v88internal26NativeRegExpMacroAssembler5MatchENS0_6HandleINS0_8JSRegExpEEENS2_INS0_6StringEEEPiiiPNS0_7IsolateE
	.section	.text._ZN2v88internal26NativeRegExpMacroAssembler9GrowStackEmPmPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26NativeRegExpMacroAssembler9GrowStackEmPmPNS0_7IsolateE
	.type	_ZN2v88internal26NativeRegExpMacroAssembler9GrowStackEmPmPNS0_7IsolateE, @function
_ZN2v88internal26NativeRegExpMacroAssembler9GrowStackEmPmPNS0_7IsolateE:
.LFB18366:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	41208(%rdx), %rdi
	movq	16(%rdi), %rax
	movq	8(%rdi), %r13
	leaq	(%rax,%rax), %rsi
	call	_ZN2v88internal11RegExpStack14EnsureCapacityEm@PLT
	testq	%rax, %rax
	je	.L109
	subq	%r13, %rbx
	movq	%rax, (%r12)
	addq	%rbx, %rax
.L109:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18366:
	.size	_ZN2v88internal26NativeRegExpMacroAssembler9GrowStackEmPmPNS0_7IsolateE, .-_ZN2v88internal26NativeRegExpMacroAssembler9GrowStackEmPmPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal20RegExpMacroAssemblerC2EPNS0_7IsolateEPNS0_4ZoneE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal20RegExpMacroAssemblerC2EPNS0_7IsolateEPNS0_4ZoneE, @function
_GLOBAL__sub_I__ZN2v88internal20RegExpMacroAssemblerC2EPNS0_7IsolateEPNS0_4ZoneE:
.LFB22097:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE22097:
	.size	_GLOBAL__sub_I__ZN2v88internal20RegExpMacroAssemblerC2EPNS0_7IsolateEPNS0_4ZoneE, .-_GLOBAL__sub_I__ZN2v88internal20RegExpMacroAssemblerC2EPNS0_7IsolateEPNS0_4ZoneE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal20RegExpMacroAssemblerC2EPNS0_7IsolateEPNS0_4ZoneE
	.section	.text._ZN2v88internal20RegExpMacroAssembler26CaseInsensitiveCompareUC16EmmmPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20RegExpMacroAssembler26CaseInsensitiveCompareUC16EmmmPNS0_7IsolateE
	.type	_ZN2v88internal20RegExpMacroAssembler26CaseInsensitiveCompareUC16EmmmPNS0_7IsolateE, @function
_ZN2v88internal20RegExpMacroAssembler26CaseInsensitiveCompareUC16EmmmPNS0_7IsolateE:
.LFB18346:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	shrq	%rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-112(%rbp), %r13
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	movq	%rdi, %rsi
	movq	%r13, %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6713UnicodeStringC1EPKDsi@PLT
	movswl	-104(%rbp), %edx
	testw	%dx, %dx
	js	.L118
	sarl	$5, %edx
.L119:
	subq	$8, %rsp
	movl	%r12d, %r9d
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	pushq	$0
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_6713UnicodeString13doCaseCompareEiiPKDsiij@PLT
	movl	%eax, %r12d
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	popq	%rax
	xorl	%eax, %eax
	testb	%r12b, %r12b
	sete	%al
	popq	%rdx
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L122
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L118:
	.cfi_restore_state
	movl	-100(%rbp), %edx
	jmp	.L119
.L122:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18346:
	.size	_ZN2v88internal20RegExpMacroAssembler26CaseInsensitiveCompareUC16EmmmPNS0_7IsolateE, .-_ZN2v88internal20RegExpMacroAssembler26CaseInsensitiveCompareUC16EmmmPNS0_7IsolateE
	.weak	_ZTVN2v88internal20RegExpMacroAssemblerE
	.section	.data.rel.ro._ZTVN2v88internal20RegExpMacroAssemblerE,"awG",@progbits,_ZTVN2v88internal20RegExpMacroAssemblerE,comdat
	.align 8
	.type	_ZTVN2v88internal20RegExpMacroAssemblerE, @object
	.size	_ZTVN2v88internal20RegExpMacroAssemblerE, 392
_ZTVN2v88internal20RegExpMacroAssemblerE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZN2v88internal20RegExpMacroAssembler21AbortedCodeGenerationEv
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN2v88internal20RegExpMacroAssembler13CheckPositionEiPNS0_5LabelE
	.quad	_ZN2v88internal20RegExpMacroAssembler26CheckSpecialCharacterClassEtPNS0_5LabelE
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.weak	_ZTVN2v88internal26NativeRegExpMacroAssemblerE
	.section	.data.rel.ro._ZTVN2v88internal26NativeRegExpMacroAssemblerE,"awG",@progbits,_ZTVN2v88internal26NativeRegExpMacroAssemblerE,comdat
	.align 8
	.type	_ZTVN2v88internal26NativeRegExpMacroAssemblerE, @object
	.size	_ZTVN2v88internal26NativeRegExpMacroAssemblerE, 392
_ZTVN2v88internal26NativeRegExpMacroAssemblerE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZN2v88internal20RegExpMacroAssembler21AbortedCodeGenerationEv
	.quad	__cxa_pure_virtual
	.quad	_ZN2v88internal26NativeRegExpMacroAssembler16CanReadUnalignedEv
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN2v88internal20RegExpMacroAssembler13CheckPositionEiPNS0_5LabelE
	.quad	_ZN2v88internal20RegExpMacroAssembler26CheckSpecialCharacterClassEtPNS0_5LabelE
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.globl	_ZN2v88internal26NativeRegExpMacroAssembler18word_character_mapE
	.section	.rodata._ZN2v88internal26NativeRegExpMacroAssembler18word_character_mapE,"a"
	.align 32
	.type	_ZN2v88internal26NativeRegExpMacroAssembler18word_character_mapE, @object
	.size	_ZN2v88internal26NativeRegExpMacroAssembler18word_character_mapE, 256
_ZN2v88internal26NativeRegExpMacroAssembler18word_character_mapE:
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\377\377\377\377\377\377\377\377\377\377"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377"
	.string	""
	.string	""
	.string	""
	.string	"\377"
	.string	"\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377"
	.zero	132
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
