	.file	"debug-property-iterator.cc"
	.text
	.section	.text._ZNK2v88internal21DebugPropertyIterator4DoneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal21DebugPropertyIterator4DoneEv
	.type	_ZNK2v88internal21DebugPropertyIterator4DoneEv, @function
_ZNK2v88internal21DebugPropertyIterator4DoneEv:
.LFB18070:
	.cfi_startproc
	endbr64
	movzbl	44(%rdi), %eax
	ret
	.cfi_endproc
.LFE18070:
	.size	_ZNK2v88internal21DebugPropertyIterator4DoneEv, .-_ZNK2v88internal21DebugPropertyIterator4DoneEv
	.section	.text._ZN2v88internal21DebugPropertyIterator6is_ownEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21DebugPropertyIterator6is_ownEv
	.type	_ZN2v88internal21DebugPropertyIterator6is_ownEv, @function
_ZN2v88internal21DebugPropertyIterator6is_ownEv:
.LFB18079:
	.cfi_startproc
	endbr64
	movzbl	84(%rdi), %eax
	ret
	.cfi_endproc
.LFE18079:
	.size	_ZN2v88internal21DebugPropertyIterator6is_ownEv, .-_ZN2v88internal21DebugPropertyIterator6is_ownEv
	.section	.text._ZN2v88internal21DebugPropertyIteratorD2Ev,"axG",@progbits,_ZN2v88internal21DebugPropertyIteratorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21DebugPropertyIteratorD2Ev
	.type	_ZN2v88internal21DebugPropertyIteratorD2Ev, @function
_ZN2v88internal21DebugPropertyIteratorD2Ev:
.LFB22164:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE22164:
	.size	_ZN2v88internal21DebugPropertyIteratorD2Ev, .-_ZN2v88internal21DebugPropertyIteratorD2Ev
	.weak	_ZN2v88internal21DebugPropertyIteratorD1Ev
	.set	_ZN2v88internal21DebugPropertyIteratorD1Ev,_ZN2v88internal21DebugPropertyIteratorD2Ev
	.section	.text._ZN2v88internal21DebugPropertyIteratorD0Ev,"axG",@progbits,_ZN2v88internal21DebugPropertyIteratorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21DebugPropertyIteratorD0Ev
	.type	_ZN2v88internal21DebugPropertyIteratorD0Ev, @function
_ZN2v88internal21DebugPropertyIteratorD0Ev:
.LFB22166:
	.cfi_startproc
	endbr64
	movl	$88, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE22166:
	.size	_ZN2v88internal21DebugPropertyIteratorD0Ev, .-_ZN2v88internal21DebugPropertyIteratorD0Ev
	.section	.text._ZN2v88internal21DebugPropertyIterator35FillKeysForCurrentPrototypeAndStageEv.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal21DebugPropertyIterator35FillKeysForCurrentPrototypeAndStageEv.part.0, @function
_ZN2v88internal21DebugPropertyIterator35FillKeysForCurrentPrototypeAndStageEv.part.0:
.LFB22230:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	32(%rdi), %rdi
	movq	(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %ecx
	movl	56(%rbx), %eax
	testl	%eax, %eax
	jne	.L7
	cmpw	$1086, %cx
	je	.L14
.L6:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	cmpl	$1, %eax
	movl	$18, %edx
	movl	$0, %eax
	cmovne	%eax, %edx
	xorl	%r9d, %r9d
	cmpw	$1086, %cx
	sete	%r9b
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	call	_ZN2v88internal14KeyAccumulator7GetKeysENS0_6HandleINS0_10JSReceiverEEENS0_17KeyCollectionModeENS0_14PropertyFilterENS0_17GetKeysConversionEbb@PLT
	movq	%rax, 64(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore_state
	movq	23(%rdx), %rax
	movl	39(%rax), %eax
	testb	$4, %al
	je	.L9
	movl	$0, 72(%rbx)
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L9:
	movq	47(%rdx), %rax
	movl	%eax, 72(%rbx)
	jmp	.L6
	.cfi_endproc
.LFE22230:
	.size	_ZN2v88internal21DebugPropertyIterator35FillKeysForCurrentPrototypeAndStageEv.part.0, .-_ZN2v88internal21DebugPropertyIterator35FillKeysForCurrentPrototypeAndStageEv.part.0
	.section	.text._ZN2v88internal21DebugPropertyIterator7AdvanceEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21DebugPropertyIterator7AdvanceEv
	.type	_ZN2v88internal21DebugPropertyIterator7AdvanceEv, @function
_ZN2v88internal21DebugPropertyIterator7AdvanceEv:
.LFB18071:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movb	$0, 76(%rdi)
	addl	$1, 60(%rdi)
	movzbl	44(%rbx), %edx
	testb	%dl, %dl
	jne	.L15
.L40:
	movl	56(%rbx), %eax
	testl	%eax, %eax
	je	.L39
	movq	64(%rbx), %rcx
	testq	%rcx, %rcx
	je	.L19
	movq	(%rcx), %rcx
	movl	60(%rbx), %esi
	cmpl	%esi, 11(%rcx)
	ja	.L15
.L19:
	cmpl	$2, %eax
	je	.L20
	ja	.L21
	cmpl	$1, %eax
	jne	.L21
	movl	$2, 56(%rbx)
.L21:
	movq	$0, 60(%rbx)
	movq	$0, 68(%rbx)
.L32:
	movq	%rbx, %rdi
	call	_ZN2v88internal21DebugPropertyIterator35FillKeysForCurrentPrototypeAndStageEv.part.0
	movzbl	44(%rbx), %edx
	testb	%dl, %dl
	je	.L40
.L15:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	.cfi_restore_state
	movl	72(%rbx), %eax
	cmpl	%eax, 60(%rbx)
	jb	.L15
	movl	$1, 56(%rbx)
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L20:
	movq	32(%rbx), %rax
	movl	$0, 56(%rbx)
	movb	$0, 84(%rbx)
	testq	%rax, %rax
	je	.L41
	movq	(%rax), %rax
.L24:
	movq	-1(%rax), %rax
	movq	16(%rbx), %r12
	movq	23(%rax), %rsi
	cmpq	104(%r12), %rsi
	je	.L33
	cmpl	$1, 40(%rbx)
	je	.L42
.L25:
	cmpq	$0, 32(%rbx)
	movb	%dl, 44(%rbx)
	je	.L43
.L26:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L28
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L29:
	movq	%rax, 32(%rbx)
	movzbl	44(%rbx), %edx
.L27:
	movq	$0, 60(%rbx)
	movq	$0, 68(%rbx)
	testb	%dl, %dl
	je	.L32
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L44
.L30:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L42:
	cmpw	$1026, 11(%rax)
	setne	%dl
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L33:
	movl	$1, %edx
	cmpq	$0, 32(%rbx)
	movb	%dl, 44(%rbx)
	jne	.L26
.L43:
	movq	%rsi, 24(%rbx)
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L41:
	movq	24(%rbx), %rax
	jmp	.L24
.L44:
	movq	%r12, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L30
	.cfi_endproc
.LFE18071:
	.size	_ZN2v88internal21DebugPropertyIterator7AdvanceEv, .-_ZN2v88internal21DebugPropertyIterator7AdvanceEv
	.section	.text._ZNK2v88internal21DebugPropertyIterator4nameEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal21DebugPropertyIterator4nameEv
	.type	_ZNK2v88internal21DebugPropertyIterator4nameEv, @function
_ZNK2v88internal21DebugPropertyIterator4nameEv:
.LFB18076:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movl	56(%rdi), %eax
	movq	8(%rdi), %r13
	movl	60(%rdi), %r14d
	testl	%eax, %eax
	jne	.L46
	testl	%r14d, %r14d
	js	.L47
	movq	%r14, %rsi
	movl	$1, %edx
	movq	%r13, %rdi
	salq	$32, %rsi
	call	_ZN2v88internal7Factory14NumberToStringENS0_3SmiEb@PLT
	movq	%rax, %r12
	movq	(%r12), %rax
	movl	11(%rax), %esi
	cmpl	$10, %esi
	jle	.L57
.L52:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	.cfi_restore_state
	movq	64(%rdi), %rdx
	leal	16(,%r14,8), %eax
	cltq
	movq	(%rdx), %rdx
	movq	-1(%rax,%rdx), %rsi
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L53
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$24, %rsp
	movq	%rax, %r12
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore_state
	movq	41088(%r13), %r12
	cmpq	41096(%r13), %r12
	je	.L58
.L55:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r13)
	movq	%r12, %rax
	movq	%rsi, (%r12)
	addq	$24, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore_state
	xorl	%edx, %edx
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal7Factory17NewNumberFromUintEjNS0_14AllocationTypeE@PLT
	movl	$1, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal7Factory14NumberToStringENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%rax, %r12
	movq	(%r12), %rax
	movl	11(%rax), %esi
	cmpl	$10, %esi
	jg	.L52
.L57:
	cmpl	$3, 7(%rax)
	jne	.L52
	movl	%r14d, %edi
	call	_ZN2v88internal12StringHasher18MakeArrayIndexHashEji@PLT
	movq	(%r12), %rdx
	movl	%eax, 7(%rdx)
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L58:
	movq	%r13, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L55
	.cfi_endproc
.LFE18076:
	.size	_ZNK2v88internal21DebugPropertyIterator4nameEv, .-_ZNK2v88internal21DebugPropertyIterator4nameEv
	.section	.text._ZN2v88internal21DebugPropertyIterator14is_array_indexEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21DebugPropertyIterator14is_array_indexEv
	.type	_ZN2v88internal21DebugPropertyIterator14is_array_indexEv, @function
_ZN2v88internal21DebugPropertyIterator14is_array_indexEv:
.LFB18080:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -24
	movl	56(%rdi), %edx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$1, %eax
	testl	%edx, %edx
	jne	.L75
.L59:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L76
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	.cfi_restore_state
	movq	64(%rdi), %rdx
	movl	60(%rdi), %eax
	movl	$0, -36(%rbp)
	movq	8(%rdi), %rbx
	movq	(%rdx), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%rax,%rdx), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L61
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	xorl	%eax, %eax
	movq	-1(%rsi), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L59
.L78:
	movq	%rsi, -32(%rbp)
	movl	7(%rsi), %edx
	testb	$1, %dl
	jne	.L65
	andl	$2, %edx
	jne	.L59
.L65:
	leaq	-36(%rbp), %rsi
	leaq	-32(%rbp), %rdi
	call	_ZN2v88internal6String16SlowAsArrayIndexEPj@PLT
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L61:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L77
.L63:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	movq	-1(%rsi), %rdx
	xorl	%eax, %eax
	cmpw	$63, 11(%rdx)
	ja	.L59
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L77:
	movq	%rbx, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L63
.L76:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18080:
	.size	_ZN2v88internal21DebugPropertyIterator14is_array_indexEv, .-_ZN2v88internal21DebugPropertyIterator14is_array_indexEv
	.section	.rodata._ZN2v85debug16PropertyIterator6CreateENS_5LocalINS_6ObjectEEE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"!handle_.is_null()"
.LC1:
	.string	"Check failed: %s."
	.section	.text._ZN2v85debug16PropertyIterator6CreateENS_5LocalINS_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v85debug16PropertyIterator6CreateENS_5LocalINS_6ObjectEEE
	.type	_ZN2v85debug16PropertyIterator6CreateENS_5LocalINS_6ObjectEEE, @function
_ZN2v85debug16PropertyIterator6CreateENS_5LocalINS_6ObjectEEE:
.LFB18059:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	movq	%rsi, %rdi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	call	_ZN2v86Object10GetIsolateEv@PLT
	movl	$88, %edi
	movq	%rax, %r13
	call	_Znwm@PLT
	movq	%rax, %rbx
	leaq	16+_ZTVN2v88internal21DebugPropertyIteratorE(%rip), %rax
	movq	%rax, (%rbx)
	movq	%r13, 8(%rbx)
	movq	%r13, 16(%rbx)
	movq	$0, 24(%rbx)
	movq	%r12, 32(%rbx)
	movl	$0, 40(%rbx)
	movb	$0, 44(%rbx)
	movl	$0, 48(%rbx)
	testq	%r12, %r12
	je	.L103
	movq	(%r12), %rax
	movq	$0, 56(%rbx)
	movq	$0, 64(%rbx)
	movl	$0, 72(%rbx)
	movb	$0, 76(%rbx)
	movl	$0, 80(%rbx)
	movb	$1, 84(%rbx)
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	je	.L104
.L89:
	movq	%rbx, %rdi
	movq	$0, 60(%rbx)
	movq	$0, 68(%rbx)
	call	_ZN2v88internal21DebugPropertyIterator35FillKeysForCurrentPrototypeAndStageEv.part.0
	cmpb	$0, 44(%rbx)
	jne	.L93
	movl	56(%rbx), %eax
	testl	%eax, %eax
	je	.L105
	movq	64(%rbx), %rax
	testq	%rax, %rax
	je	.L95
	movq	(%rax), %rax
	movl	60(%rbx), %ecx
	cmpl	%ecx, 11(%rax)
	ja	.L93
.L95:
	movq	%rbx, %rdi
	call	_ZN2v88internal21DebugPropertyIterator7AdvanceEv
.L93:
	movq	%rbx, (%r14)
	addq	$16, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L105:
	.cfi_restore_state
	movl	72(%rbx), %eax
	cmpl	%eax, 60(%rbx)
	jb	.L93
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L104:
	movb	$0, 84(%rbx)
	movq	-1(%rax), %rax
	movq	23(%rax), %rsi
	cmpq	104(%r13), %rsi
	je	.L106
	cmpq	$0, 32(%rbx)
	movb	$0, 44(%rbx)
	je	.L107
.L86:
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L90
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L91:
	cmpb	$0, 44(%rbx)
	movq	%rax, 32(%rbx)
	jne	.L93
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L103:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L106:
	cmpq	$0, 32(%rbx)
	movb	$1, 44(%rbx)
	jne	.L86
	movq	%rsi, 24(%rbx)
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L90:
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L108
.L92:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%rsi, (%rax)
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L108:
	movq	%r13, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L107:
	movq	%rsi, 24(%rbx)
	jmp	.L89
	.cfi_endproc
.LFE18059:
	.size	_ZN2v85debug16PropertyIterator6CreateENS_5LocalINS_6ObjectEEE, .-_ZN2v85debug16PropertyIterator6CreateENS_5LocalINS_6ObjectEEE
	.section	.text._ZN2v88internal21DebugPropertyIteratorC2EPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21DebugPropertyIteratorC2EPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEE
	.type	_ZN2v88internal21DebugPropertyIteratorC2EPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEE, @function
_ZN2v88internal21DebugPropertyIteratorC2EPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEE:
.LFB18068:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal21DebugPropertyIteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rax, (%rdi)
	movq	%rsi, 8(%rdi)
	movq	%rsi, 16(%rdi)
	movq	$0, 24(%rdi)
	movq	%rdx, 32(%rdi)
	movl	$0, 40(%rdi)
	movb	$0, 44(%rdi)
	movl	$0, 48(%rdi)
	testq	%rdx, %rdx
	je	.L133
	movq	$0, 64(%rdi)
	movq	(%rdx), %rax
	movq	%rdi, %r12
	movq	$0, 56(%rdi)
	movl	$0, 72(%rdi)
	movb	$0, 76(%rdi)
	movl	$0, 80(%rdi)
	movb	$1, 84(%rdi)
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	je	.L134
.L119:
	movq	$0, 60(%r12)
	movq	%r12, %rdi
	movq	$0, 68(%r12)
	call	_ZN2v88internal21DebugPropertyIterator35FillKeysForCurrentPrototypeAndStageEv.part.0
	cmpb	$0, 44(%r12)
	jne	.L109
	movl	56(%r12), %eax
	testl	%eax, %eax
	je	.L135
	movq	64(%r12), %rax
	testq	%rax, %rax
	je	.L125
	movq	(%rax), %rax
	movl	60(%r12), %ecx
	cmpl	%ecx, 11(%rax)
	ja	.L109
.L125:
	addq	$16, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal21DebugPropertyIterator7AdvanceEv
	.p2align 4,,10
	.p2align 3
.L136:
	.cfi_restore_state
	cmpq	$0, 32(%rdi)
	movb	$1, 44(%rdi)
	jne	.L116
	movq	%r13, 24(%rdi)
.L109:
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L135:
	.cfi_restore_state
	movl	72(%r12), %eax
	cmpl	%eax, 60(%r12)
	jb	.L109
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L134:
	movb	$0, 84(%rdi)
	movq	-1(%rax), %rax
	movq	23(%rax), %r13
	cmpq	104(%rsi), %r13
	je	.L136
	cmpq	$0, 32(%rdi)
	movb	$0, 44(%rdi)
	je	.L137
.L116:
	movq	41112(%rsi), %rdi
	testq	%rdi, %rdi
	je	.L120
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L121:
	cmpb	$0, 44(%r12)
	movq	%rax, 32(%r12)
	jne	.L109
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L133:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L120:
	movq	41088(%rsi), %rax
	cmpq	41096(%rsi), %rax
	je	.L138
.L122:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rsi)
	movq	%r13, (%rax)
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L138:
	movq	%rsi, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L137:
	movq	%r13, 24(%rdi)
	jmp	.L119
	.cfi_endproc
.LFE18068:
	.size	_ZN2v88internal21DebugPropertyIteratorC2EPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEE, .-_ZN2v88internal21DebugPropertyIteratorC2EPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEE
	.globl	_ZN2v88internal21DebugPropertyIteratorC1EPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEE
	.set	_ZN2v88internal21DebugPropertyIteratorC1EPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEE,_ZN2v88internal21DebugPropertyIteratorC2EPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEE
	.section	.text._ZNK2v88internal21DebugPropertyIterator8raw_nameEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal21DebugPropertyIterator8raw_nameEv
	.type	_ZNK2v88internal21DebugPropertyIterator8raw_nameEv, @function
_ZNK2v88internal21DebugPropertyIterator8raw_nameEv:
.LFB18075:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	56(%rdi), %eax
	movq	8(%rdi), %r12
	movl	60(%rdi), %r13d
	testl	%eax, %eax
	jne	.L140
	testl	%r13d, %r13d
	js	.L141
	movq	%r13, %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	salq	$32, %rsi
	call	_ZN2v88internal7Factory14NumberToStringENS0_3SmiEb@PLT
	movq	%rax, %rbx
	movq	(%rbx), %rax
	movl	11(%rax), %esi
	cmpl	$10, %esi
	jle	.L151
.L145:
	addq	$24, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L140:
	.cfi_restore_state
	movq	64(%rdi), %rdx
	leal	16(,%r13,8), %eax
	cltq
	movq	(%rdx), %rdx
	movq	-1(%rax,%rdx), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L147
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L147:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L152
.L149:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L141:
	.cfi_restore_state
	xorl	%edx, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory17NewNumberFromUintEjNS0_14AllocationTypeE@PLT
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal7Factory14NumberToStringENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%rax, %rbx
	movq	(%rbx), %rax
	movl	11(%rax), %esi
	cmpl	$10, %esi
	jg	.L145
.L151:
	cmpl	$3, 7(%rax)
	jne	.L145
	movl	%r13d, %edi
	call	_ZN2v88internal12StringHasher18MakeArrayIndexHashEji@PLT
	movq	(%rbx), %rdx
	movl	%eax, 7(%rdx)
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L152:
	movq	%r12, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L149
	.cfi_endproc
.LFE18075:
	.size	_ZNK2v88internal21DebugPropertyIterator8raw_nameEv, .-_ZNK2v88internal21DebugPropertyIterator8raw_nameEv
	.section	.text._ZN2v88internal21DebugPropertyIterator10descriptorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21DebugPropertyIterator10descriptorEv
	.type	_ZN2v88internal21DebugPropertyIterator10descriptorEv, @function
_ZN2v88internal21DebugPropertyIterator10descriptorEv:
.LFB18078:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	32(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	andb	$-64, -80(%rbp)
	movups	%xmm0, -72(%rbp)
	movups	%xmm0, -56(%rbp)
	call	_ZNK2v88internal21DebugPropertyIterator8raw_nameEv
	movq	8(%rbx), %rdi
	leaq	-80(%rbp), %rcx
	movq	%r13, %rsi
	movq	%rax, %rdx
	call	_ZN2v88internal10JSReceiver24GetOwnPropertyDescriptorEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEEPNS0_18PropertyDescriptorE@PLT
	testb	%al, %al
	jne	.L154
	movb	$0, (%r12)
	pxor	%xmm0, %xmm0
	movq	$0, 32(%r12)
	movups	%xmm0, 16(%r12)
.L153:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L158
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L154:
	.cfi_restore_state
	movzbl	8(%r12), %eax
	movzbl	-80(%rbp), %edx
	movb	$1, (%r12)
	movq	-72(%rbp), %xmm0
	andl	$-64, %eax
	andl	$63, %edx
	orl	%edx, %eax
	movhps	-64(%rbp), %xmm0
	movb	%al, 8(%r12)
	movq	-56(%rbp), %rax
	movups	%xmm0, 16(%r12)
	movq	%rax, 32(%r12)
	jmp	.L153
.L158:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18078:
	.size	_ZN2v88internal21DebugPropertyIterator10descriptorEv, .-_ZN2v88internal21DebugPropertyIterator10descriptorEv
	.section	.text._ZN2v88internal21DebugPropertyIterator10attributesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21DebugPropertyIterator10attributesEv
	.type	_ZN2v88internal21DebugPropertyIterator10attributesEv, @function
_ZN2v88internal21DebugPropertyIterator10attributesEv:
.LFB18077:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$192, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	32(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal21DebugPropertyIterator8raw_nameEv
	movq	%rax, %r12
	movq	(%rbx), %rax
	andq	$-262144, %rax
	movq	24(%rax), %r13
	movq	(%r12), %rax
	movq	-1(%rax), %rdx
	subq	$37592, %r13
	cmpw	$63, 11(%rdx)
	jbe	.L182
.L161:
	movq	(%r12), %rdx
	movl	$3, %eax
	movq	-1(%rdx), %rcx
	cmpw	$64, 11(%rcx)
	je	.L183
.L165:
	movl	%eax, -208(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -196(%rbp)
	movq	%r13, -184(%rbp)
	movq	(%r12), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L184
.L166:
	movq	%r12, -176(%rbp)
	leaq	-208(%rbp), %r12
	movq	%r12, %rdi
	movq	$0, -168(%rbp)
	movq	%rbx, -160(%rbp)
	movq	$0, -152(%rbp)
	movq	%rbx, -144(%rbp)
	movq	$-1, -136(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
.L164:
	movq	%r12, %rdi
	call	_ZN2v88internal10JSReceiver21GetPropertyAttributesEPNS0_14LookupIteratorE@PLT
	movq	%rax, %rcx
	movq	%rax, %rdx
	shrq	$32, %rcx
	testb	%al, %al
	movl	$0, %eax
	cmovne	%ecx, %eax
	cmpb	$1, %dl
	sbbl	%edx, %edx
	addl	$1, %edx
	salq	$32, %rax
	movq	-40(%rbp), %rsi
	xorq	%fs:40, %rsi
	movb	%dl, %al
	jne	.L185
	addq	$192, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L182:
	.cfi_restore_state
	movq	%rax, -128(%rbp)
	movl	7(%rax), %eax
	testb	$1, %al
	jne	.L162
	testb	$2, %al
	jne	.L161
.L162:
	leaq	-128(%rbp), %r14
	leaq	-212(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal6String16SlowAsArrayIndexEPj@PLT
	testb	%al, %al
	je	.L161
	movabsq	$824633720832, %rax
	movq	%r14, %rdi
	movq	%r13, -104(%rbp)
	movq	%rax, -116(%rbp)
	movl	-212(%rbp), %eax
	movl	$3, -128(%rbp)
	movq	$0, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rbx, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	%rbx, -64(%rbp)
	movl	%eax, -56(%rbp)
	movl	$-1, -52(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	movq	%r12, -96(%rbp)
	movdqa	-128(%rbp), %xmm0
	leaq	-208(%rbp), %r12
	movdqa	-112(%rbp), %xmm1
	movdqa	-96(%rbp), %xmm2
	movdqa	-80(%rbp), %xmm3
	movdqa	-64(%rbp), %xmm4
	movaps	%xmm0, -208(%rbp)
	movaps	%xmm1, -192(%rbp)
	movaps	%xmm2, -176(%rbp)
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm4, -144(%rbp)
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L183:
	movl	11(%rdx), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$3, %eax
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L184:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %r12
	jmp	.L166
.L185:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18077:
	.size	_ZN2v88internal21DebugPropertyIterator10attributesEv, .-_ZN2v88internal21DebugPropertyIterator10attributesEv
	.section	.text._ZN2v88internal21DebugPropertyIterator28CalculateNativeAccessorFlagsEv.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal21DebugPropertyIterator28CalculateNativeAccessorFlagsEv.part.0, @function
_ZN2v88internal21DebugPropertyIterator28CalculateNativeAccessorFlagsEv.part.0:
.LFB22233:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$136, %rsp
	movq	32(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal21DebugPropertyIterator8raw_nameEv
	movq	%rax, %r12
	movq	(%rax), %rax
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L187
.L214:
	leaq	-144(%rbp), %r15
.L188:
	movq	0(%r13), %rax
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %r14
	subq	$37592, %r14
	testb	$1, %al
	jne	.L192
.L194:
	movl	$-1, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	%rax, %rdx
.L193:
	movq	(%r12), %rcx
	movl	$1, %eax
	movq	-1(%rcx), %rsi
	cmpw	$64, 11(%rsi)
	jne	.L195
	movl	11(%rcx), %eax
	notl	%eax
	andl	$1, %eax
.L195:
	movl	%eax, -144(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -132(%rbp)
	movq	%r14, -120(%rbp)
	movq	(%r12), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L215
.L196:
	movq	%r15, %rdi
	movq	%r12, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%r13, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rdx, -80(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$5, -140(%rbp)
	jne	.L199
	movq	%r15, %rdi
	call	_ZNK2v88internal14LookupIterator12GetAccessorsEv@PLT
	movq	(%rax), %rdx
	testb	$1, %dl
	jne	.L216
	.p2align 4,,10
	.p2align 3
.L199:
	xorl	%eax, %eax
.L191:
	movl	%eax, 80(%rbx)
	movb	$1, 76(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L217
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L187:
	.cfi_restore_state
	movq	%rax, -144(%rbp)
	movl	7(%rax), %eax
	testb	$1, %al
	jne	.L189
	testb	$2, %al
	jne	.L214
.L189:
	leaq	-144(%rbp), %r15
	leaq	-148(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal6String16SlowAsArrayIndexEPj@PLT
	testb	%al, %al
	jne	.L199
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L192:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L194
	movq	%r13, %rdx
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L215:
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rdx, -168(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-168(%rbp), %rdx
	movq	%rax, %r12
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L216:
	movq	-1(%rdx), %rax
	cmpw	$78, 11(%rax)
	jne	.L199
	movq	0(%r13), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rcx
	cmpq	%rdx, -33224(%rcx)
	je	.L199
	cmpq	-33216(%rcx), %rdx
	je	.L199
	cmpq	-33208(%rcx), %rdx
	je	.L199
	cmpq	-33200(%rcx), %rdx
	je	.L199
	cmpq	-33192(%rcx), %rdx
	je	.L199
	cmpq	-33184(%rcx), %rdx
	je	.L199
	cmpq	-33176(%rcx), %rdx
	je	.L199
	cmpq	-33168(%rcx), %rdx
	je	.L199
	cmpq	-33160(%rcx), %rdx
	je	.L199
	cmpq	-33152(%rcx), %rdx
	je	.L199
	cmpq	-33144(%rcx), %rdx
	je	.L199
	xorl	%eax, %eax
	cmpq	$0, 39(%rdx)
	setne	%al
	cmpq	$0, 31(%rdx)
	je	.L191
	orl	$2, %eax
	jmp	.L191
.L217:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22233:
	.size	_ZN2v88internal21DebugPropertyIterator28CalculateNativeAccessorFlagsEv.part.0, .-_ZN2v88internal21DebugPropertyIterator28CalculateNativeAccessorFlagsEv.part.0
	.section	.text._ZN2v88internal21DebugPropertyIterator18is_native_accessorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21DebugPropertyIterator18is_native_accessorEv
	.type	_ZN2v88internal21DebugPropertyIterator18is_native_accessorEv, @function
_ZN2v88internal21DebugPropertyIterator18is_native_accessorEv:
.LFB18072:
	.cfi_startproc
	endbr64
	movl	56(%rdi), %edx
	testl	%edx, %edx
	je	.L222
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpb	$0, 76(%rdi)
	je	.L227
	movl	80(%rbx), %eax
	testl	%eax, %eax
	setne	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L227:
	.cfi_restore_state
	call	_ZN2v88internal21DebugPropertyIterator28CalculateNativeAccessorFlagsEv.part.0
	movl	80(%rbx), %eax
	testl	%eax, %eax
	setne	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L222:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE18072:
	.size	_ZN2v88internal21DebugPropertyIterator18is_native_accessorEv, .-_ZN2v88internal21DebugPropertyIterator18is_native_accessorEv
	.section	.text._ZN2v88internal21DebugPropertyIterator17has_native_getterEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21DebugPropertyIterator17has_native_getterEv
	.type	_ZN2v88internal21DebugPropertyIterator17has_native_getterEv, @function
_ZN2v88internal21DebugPropertyIterator17has_native_getterEv:
.LFB18073:
	.cfi_startproc
	endbr64
	movl	56(%rdi), %eax
	testl	%eax, %eax
	je	.L232
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpb	$0, 76(%rdi)
	je	.L237
	movl	80(%rbx), %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	andl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L237:
	.cfi_restore_state
	call	_ZN2v88internal21DebugPropertyIterator28CalculateNativeAccessorFlagsEv.part.0
	movl	80(%rbx), %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	andl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L232:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE18073:
	.size	_ZN2v88internal21DebugPropertyIterator17has_native_getterEv, .-_ZN2v88internal21DebugPropertyIterator17has_native_getterEv
	.section	.text._ZN2v88internal21DebugPropertyIterator17has_native_setterEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21DebugPropertyIterator17has_native_setterEv
	.type	_ZN2v88internal21DebugPropertyIterator17has_native_setterEv, @function
_ZN2v88internal21DebugPropertyIterator17has_native_setterEv:
.LFB18074:
	.cfi_startproc
	endbr64
	movl	56(%rdi), %eax
	testl	%eax, %eax
	je	.L242
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpb	$0, 76(%rdi)
	je	.L247
	movl	80(%rbx), %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	shrl	%eax
	andl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L247:
	.cfi_restore_state
	call	_ZN2v88internal21DebugPropertyIterator28CalculateNativeAccessorFlagsEv.part.0
	movl	80(%rbx), %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	shrl	%eax
	andl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L242:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE18074:
	.size	_ZN2v88internal21DebugPropertyIterator17has_native_setterEv, .-_ZN2v88internal21DebugPropertyIterator17has_native_setterEv
	.section	.text._ZN2v88internal21DebugPropertyIterator35FillKeysForCurrentPrototypeAndStageEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21DebugPropertyIterator35FillKeysForCurrentPrototypeAndStageEv
	.type	_ZN2v88internal21DebugPropertyIterator35FillKeysForCurrentPrototypeAndStageEv, @function
_ZN2v88internal21DebugPropertyIterator35FillKeysForCurrentPrototypeAndStageEv:
.LFB18081:
	.cfi_startproc
	endbr64
	cmpb	$0, 44(%rdi)
	movq	$0, 60(%rdi)
	movq	$0, 68(%rdi)
	je	.L261
	ret
	.p2align 4,,10
	.p2align 3
.L261:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	32(%rdi), %rdi
	movq	(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %ecx
	movl	56(%rbx), %eax
	testl	%eax, %eax
	jne	.L251
	cmpw	$1086, %cx
	je	.L262
.L248:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L251:
	.cfi_restore_state
	cmpl	$1, %eax
	movl	$18, %edx
	movl	$0, %eax
	cmovne	%eax, %edx
	xorl	%r9d, %r9d
	cmpw	$1086, %cx
	sete	%r9b
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	call	_ZN2v88internal14KeyAccumulator7GetKeysENS0_6HandleINS0_10JSReceiverEEENS0_17KeyCollectionModeENS0_14PropertyFilterENS0_17GetKeysConversionEbb@PLT
	movq	%rax, 64(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L262:
	.cfi_restore_state
	movq	23(%rdx), %rax
	movl	39(%rax), %eax
	testb	$4, %al
	je	.L253
	movl	$0, 72(%rbx)
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L253:
	movq	47(%rdx), %rax
	movl	%eax, 72(%rbx)
	jmp	.L248
	.cfi_endproc
.LFE18081:
	.size	_ZN2v88internal21DebugPropertyIterator35FillKeysForCurrentPrototypeAndStageEv, .-_ZN2v88internal21DebugPropertyIterator35FillKeysForCurrentPrototypeAndStageEv
	.section	.text._ZNK2v88internal21DebugPropertyIterator25should_move_to_next_stageEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal21DebugPropertyIterator25should_move_to_next_stageEv
	.type	_ZNK2v88internal21DebugPropertyIterator25should_move_to_next_stageEv, @function
_ZNK2v88internal21DebugPropertyIterator25should_move_to_next_stageEv:
.LFB18082:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpb	$0, 44(%rdi)
	jne	.L263
	movl	56(%rdi), %eax
	testl	%eax, %eax
	je	.L269
	movq	64(%rdi), %rdx
	movl	$1, %eax
	testq	%rdx, %rdx
	je	.L263
	movq	(%rdx), %rax
	movl	60(%rdi), %ecx
	cmpl	%ecx, 11(%rax)
	setbe	%al
.L263:
	ret
	.p2align 4,,10
	.p2align 3
.L269:
	movl	72(%rdi), %eax
	cmpl	%eax, 60(%rdi)
	setnb	%al
	ret
	.cfi_endproc
.LFE18082:
	.size	_ZNK2v88internal21DebugPropertyIterator25should_move_to_next_stageEv, .-_ZNK2v88internal21DebugPropertyIterator25should_move_to_next_stageEv
	.section	.text._ZN2v88internal21DebugPropertyIterator28CalculateNativeAccessorFlagsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21DebugPropertyIterator28CalculateNativeAccessorFlagsEv
	.type	_ZN2v88internal21DebugPropertyIterator28CalculateNativeAccessorFlagsEv, @function
_ZN2v88internal21DebugPropertyIterator28CalculateNativeAccessorFlagsEv:
.LFB18087:
	.cfi_startproc
	endbr64
	cmpb	$0, 76(%rdi)
	je	.L272
	ret
	.p2align 4,,10
	.p2align 3
.L272:
	jmp	_ZN2v88internal21DebugPropertyIterator28CalculateNativeAccessorFlagsEv.part.0
	.cfi_endproc
.LFE18087:
	.size	_ZN2v88internal21DebugPropertyIterator28CalculateNativeAccessorFlagsEv, .-_ZN2v88internal21DebugPropertyIterator28CalculateNativeAccessorFlagsEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v85debug16PropertyIterator6CreateENS_5LocalINS_6ObjectEEE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v85debug16PropertyIterator6CreateENS_5LocalINS_6ObjectEEE, @function
_GLOBAL__sub_I__ZN2v85debug16PropertyIterator6CreateENS_5LocalINS_6ObjectEEE:
.LFB22192:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE22192:
	.size	_GLOBAL__sub_I__ZN2v85debug16PropertyIterator6CreateENS_5LocalINS_6ObjectEEE, .-_GLOBAL__sub_I__ZN2v85debug16PropertyIterator6CreateENS_5LocalINS_6ObjectEEE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v85debug16PropertyIterator6CreateENS_5LocalINS_6ObjectEEE
	.weak	_ZTVN2v88internal21DebugPropertyIteratorE
	.section	.data.rel.ro.local._ZTVN2v88internal21DebugPropertyIteratorE,"awG",@progbits,_ZTVN2v88internal21DebugPropertyIteratorE,comdat
	.align 8
	.type	_ZTVN2v88internal21DebugPropertyIteratorE, @object
	.size	_ZTVN2v88internal21DebugPropertyIteratorE, 112
_ZTVN2v88internal21DebugPropertyIteratorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal21DebugPropertyIteratorD1Ev
	.quad	_ZN2v88internal21DebugPropertyIteratorD0Ev
	.quad	_ZNK2v88internal21DebugPropertyIterator4DoneEv
	.quad	_ZN2v88internal21DebugPropertyIterator7AdvanceEv
	.quad	_ZNK2v88internal21DebugPropertyIterator4nameEv
	.quad	_ZN2v88internal21DebugPropertyIterator18is_native_accessorEv
	.quad	_ZN2v88internal21DebugPropertyIterator17has_native_getterEv
	.quad	_ZN2v88internal21DebugPropertyIterator17has_native_setterEv
	.quad	_ZN2v88internal21DebugPropertyIterator10attributesEv
	.quad	_ZN2v88internal21DebugPropertyIterator10descriptorEv
	.quad	_ZN2v88internal21DebugPropertyIterator6is_ownEv
	.quad	_ZN2v88internal21DebugPropertyIterator14is_array_indexEv
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
