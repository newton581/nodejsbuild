	.file	"builtins-callsite.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB5450:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE5450:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB5451:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5451:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB5453:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5453:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.rodata._ZN2v88internalL38Builtin_Impl_CallSitePrototypeToStringENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"toString"
.LC1:
	.string	"(location_) != nullptr"
.LC2:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internalL38Builtin_Impl_CallSitePrototypeToStringENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL38Builtin_Impl_CallSitePrototypeToStringENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL38Builtin_Impl_CallSitePrototypeToStringENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB20138:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L6
.L9:
	leaq	.LC0(%rip), %rax
	leaq	-160(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, -160(%rbp)
	movq	$8, -152(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$62, %esi
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L7
.L40:
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L12:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L25
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L25:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L41
	addq	$136, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L9
	leaq	3616(%rdx), %r15
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal10JSReceiver14HasOwnPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEE@PLT
	testb	%al, %al
	je	.L13
	shrw	$8, %ax
	jne	.L14
.L13:
	leaq	.LC0(%rip), %rax
	xorl	%edx, %edx
	leaq	-144(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -144(%rbp)
	movq	$8, -136(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L7
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$29, %esi
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L7:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L14:
	movq	0(%r13), %rax
	movq	3624(%r12), %rdx
	leaq	3624(%r12), %rsi
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	movq	-1(%rdx), %rcx
	movl	$2, %eax
	subq	$37592, %rdi
	cmpw	$64, 11(%rcx)
	jne	.L15
	xorl	%eax, %eax
	testb	$1, 11(%rdx)
	sete	%al
	addl	%eax, %eax
.L15:
	movl	%eax, -144(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -132(%rbp)
	movq	3624(%r12), %rax
	movq	%rdi, -120(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L42
.L16:
	leaq	-144(%rbp), %r8
	movq	%rsi, -112(%rbp)
	movq	%r8, %rdi
	movq	%r8, -168(%rbp)
	movq	$0, -104(%rbp)
	movq	%r13, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%r13, -80(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -140(%rbp)
	movq	-168(%rbp), %r8
	jne	.L17
	movq	-120(%rbp), %rax
	addq	$88, %rax
.L18:
	movq	(%rax), %rax
	movq	3616(%r12), %rdx
	sarq	$32, %rax
	movq	%rax, -168(%rbp)
	movq	0(%r13), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	movq	-1(%rdx), %rcx
	movl	$2, %eax
	subq	$37592, %rdi
	cmpw	$64, 11(%rcx)
	jne	.L19
	xorl	%eax, %eax
	testb	$1, 11(%rdx)
	sete	%al
	addl	%eax, %eax
.L19:
	movl	%eax, -144(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -132(%rbp)
	movq	3616(%r12), %rax
	movq	%rdi, -120(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L43
.L20:
	movq	%r8, %rdi
	movq	%r8, -176(%rbp)
	movq	%r15, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%r13, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%r13, -80(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -140(%rbp)
	movq	-176(%rbp), %r8
	jne	.L21
	movq	-120(%rbp), %rax
	leaq	88(%rax), %rsi
.L22:
	movl	-168(%rbp), %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory18NewStackTraceFrameENS0_6HandleINS0_10FrameArrayEEEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal24SerializeStackTraceFrameEPNS0_7IsolateENS0_6HandleINS0_15StackTraceFrameEEE@PLT
	testq	%rax, %rax
	je	.L44
	movq	(%rax), %r13
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L17:
	movq	%r8, %rdi
	movq	%r8, -168(%rbp)
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE@PLT
	movq	-168(%rbp), %r8
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L21:
	movq	%r8, %rdi
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE@PLT
	movq	%rax, %rsi
	jmp	.L22
.L43:
	movq	%r15, %rsi
	movq	%r8, -176(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-176(%rbp), %r8
	movq	%rax, %r15
	jmp	.L20
.L42:
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rsi
	jmp	.L16
.L44:
	movq	312(%r12), %r13
	jmp	.L12
.L41:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20138:
	.size	_ZN2v88internalL38Builtin_Impl_CallSitePrototypeToStringENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL38Builtin_Impl_CallSitePrototypeToStringENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL41Builtin_Impl_CallSitePrototypeGetPositionENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"getPosition"
	.section	.text._ZN2v88internalL41Builtin_Impl_CallSitePrototypeGetPositionENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL41Builtin_Impl_CallSitePrototypeGetPositionENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL41Builtin_Impl_CallSitePrototypeGetPositionENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB20105:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L46
.L49:
	leaq	.LC3(%rip), %rax
	leaq	-256(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, -256(%rbp)
	movq	$11, -248(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$62, %esi
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L47
.L79:
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L52:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L64
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L64:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L80
	addq	$232, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L49
	leaq	3616(%rdx), %r15
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal10JSReceiver14HasOwnPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEE@PLT
	testb	%al, %al
	je	.L53
	shrw	$8, %ax
	jne	.L54
.L53:
	leaq	.LC3(%rip), %rax
	xorl	%edx, %edx
	leaq	-240(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -240(%rbp)
	movq	$11, -232(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L47
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$29, %esi
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L47:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L54:
	movq	0(%r13), %rax
	movq	3624(%r12), %rdx
	leaq	3624(%r12), %rsi
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	movq	-1(%rdx), %rcx
	movl	$2, %eax
	subq	$37592, %rdi
	cmpw	$64, 11(%rcx)
	jne	.L55
	xorl	%eax, %eax
	testb	$1, 11(%rdx)
	sete	%al
	addl	%eax, %eax
.L55:
	movl	%eax, -240(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -228(%rbp)
	movq	3624(%r12), %rax
	movq	%rdi, -216(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L81
.L56:
	leaq	-240(%rbp), %r8
	movq	%rsi, -208(%rbp)
	movq	%r8, %rdi
	movq	%r8, -264(%rbp)
	movq	$0, -200(%rbp)
	movq	%r13, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	%r13, -176(%rbp)
	movq	$-1, -168(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -236(%rbp)
	movq	-264(%rbp), %r8
	jne	.L57
	movq	-216(%rbp), %rax
	addq	$88, %rax
.L58:
	movq	(%rax), %rax
	movq	3616(%r12), %rdx
	sarq	$32, %rax
	movq	%rax, -264(%rbp)
	movq	0(%r13), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	movq	-1(%rdx), %rcx
	movl	$2, %eax
	subq	$37592, %rdi
	cmpw	$64, 11(%rcx)
	jne	.L59
	xorl	%eax, %eax
	testb	$1, 11(%rdx)
	sete	%al
	addl	%eax, %eax
.L59:
	movl	%eax, -240(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -228(%rbp)
	movq	3616(%r12), %rax
	movq	%rdi, -216(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L82
.L60:
	movq	%r8, %rdi
	movq	%r8, -272(%rbp)
	movq	%r15, -208(%rbp)
	movq	$0, -200(%rbp)
	movq	%r13, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	%r13, -176(%rbp)
	movq	$-1, -168(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -236(%rbp)
	movq	-272(%rbp), %r8
	jne	.L61
	movq	-216(%rbp), %rax
	leaq	88(%rax), %rdx
.L62:
	movl	-264(%rbp), %ecx
	movq	%r12, %rsi
	movq	%r8, %rdi
	movq	%r8, -264(%rbp)
	call	_ZN2v88internal18FrameArrayIteratorC1EPNS0_7IsolateENS0_6HandleINS0_10FrameArrayEEEi@PLT
	movq	-264(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN2v88internal18FrameArrayIterator5FrameEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*96(%rax)
	movq	%rax, %r13
	salq	$32, %r13
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L57:
	movq	%r8, %rdi
	movq	%r8, -264(%rbp)
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE@PLT
	movq	-264(%rbp), %r8
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L61:
	movq	%r8, %rdi
	movq	%r8, -272(%rbp)
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE@PLT
	movq	-272(%rbp), %r8
	movq	%rax, %rdx
	jmp	.L62
.L82:
	movq	%r15, %rsi
	movq	%r8, -272(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-272(%rbp), %r8
	movq	%rax, %r15
	jmp	.L60
.L81:
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rsi
	jmp	.L56
.L80:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20105:
	.size	_ZN2v88internalL41Builtin_Impl_CallSitePrototypeGetPositionENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL41Builtin_Impl_CallSitePrototypeGetPositionENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL41Builtin_Impl_CallSitePrototypeGetTypeNameENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"getTypeName"
	.section	.text._ZN2v88internalL41Builtin_Impl_CallSitePrototypeGetTypeNameENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL41Builtin_Impl_CallSitePrototypeGetTypeNameENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL41Builtin_Impl_CallSitePrototypeGetTypeNameENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB20117:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L84
.L87:
	leaq	.LC4(%rip), %rax
	leaq	-256(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, -256(%rbp)
	movq	$11, -248(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$62, %esi
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L85
.L117:
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L90:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L102
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L102:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L118
	addq	$232, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L87
	leaq	3616(%rdx), %r15
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal10JSReceiver14HasOwnPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEE@PLT
	testb	%al, %al
	je	.L91
	shrw	$8, %ax
	jne	.L92
.L91:
	leaq	.LC4(%rip), %rax
	xorl	%edx, %edx
	leaq	-240(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -240(%rbp)
	movq	$11, -232(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L85
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$29, %esi
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L85:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L92:
	movq	0(%r13), %rax
	movq	3624(%r12), %rdx
	leaq	3624(%r12), %rsi
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	movq	-1(%rdx), %rcx
	movl	$2, %eax
	subq	$37592, %rdi
	cmpw	$64, 11(%rcx)
	jne	.L93
	xorl	%eax, %eax
	testb	$1, 11(%rdx)
	sete	%al
	addl	%eax, %eax
.L93:
	movl	%eax, -240(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -228(%rbp)
	movq	3624(%r12), %rax
	movq	%rdi, -216(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L119
.L94:
	leaq	-240(%rbp), %r8
	movq	%rsi, -208(%rbp)
	movq	%r8, %rdi
	movq	%r8, -264(%rbp)
	movq	$0, -200(%rbp)
	movq	%r13, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	%r13, -176(%rbp)
	movq	$-1, -168(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -236(%rbp)
	movq	-264(%rbp), %r8
	jne	.L95
	movq	-216(%rbp), %rax
	addq	$88, %rax
.L96:
	movq	(%rax), %rax
	movq	3616(%r12), %rdx
	sarq	$32, %rax
	movq	%rax, -264(%rbp)
	movq	0(%r13), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	movq	-1(%rdx), %rcx
	movl	$2, %eax
	subq	$37592, %rdi
	cmpw	$64, 11(%rcx)
	jne	.L97
	xorl	%eax, %eax
	testb	$1, 11(%rdx)
	sete	%al
	addl	%eax, %eax
.L97:
	movl	%eax, -240(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -228(%rbp)
	movq	3616(%r12), %rax
	movq	%rdi, -216(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L120
.L98:
	movq	%r8, %rdi
	movq	%r8, -272(%rbp)
	movq	%r15, -208(%rbp)
	movq	$0, -200(%rbp)
	movq	%r13, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	%r13, -176(%rbp)
	movq	$-1, -168(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -236(%rbp)
	movq	-272(%rbp), %r8
	jne	.L99
	movq	-216(%rbp), %rax
	leaq	88(%rax), %rdx
.L100:
	movl	-264(%rbp), %ecx
	movq	%r12, %rsi
	movq	%r8, %rdi
	movq	%r8, -264(%rbp)
	call	_ZN2v88internal18FrameArrayIteratorC1EPNS0_7IsolateENS0_6HandleINS0_10FrameArrayEEEi@PLT
	movq	-264(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN2v88internal18FrameArrayIterator5FrameEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*64(%rax)
	movq	(%rax), %r13
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L95:
	movq	%r8, %rdi
	movq	%r8, -264(%rbp)
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE@PLT
	movq	-264(%rbp), %r8
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L99:
	movq	%r8, %rdi
	movq	%r8, -272(%rbp)
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE@PLT
	movq	-272(%rbp), %r8
	movq	%rax, %rdx
	jmp	.L100
.L120:
	movq	%r15, %rsi
	movq	%r8, -272(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-272(%rbp), %r8
	movq	%rax, %r15
	jmp	.L98
.L119:
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rsi
	jmp	.L94
.L118:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20117:
	.size	_ZN2v88internalL41Builtin_Impl_CallSitePrototypeGetTypeNameENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL41Builtin_Impl_CallSitePrototypeGetTypeNameENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL43Builtin_Impl_CallSitePrototypeGetEvalOriginENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"getEvalOrigin"
	.section	.text._ZN2v88internalL43Builtin_Impl_CallSitePrototypeGetEvalOriginENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL43Builtin_Impl_CallSitePrototypeGetEvalOriginENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL43Builtin_Impl_CallSitePrototypeGetEvalOriginENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB20087:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L122
.L125:
	leaq	.LC5(%rip), %rax
	leaq	-256(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, -256(%rbp)
	movq	$13, -248(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$62, %esi
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L123
.L155:
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L128:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L140
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L140:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L156
	addq	$232, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L122:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L125
	leaq	3616(%rdx), %r15
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal10JSReceiver14HasOwnPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEE@PLT
	testb	%al, %al
	je	.L129
	shrw	$8, %ax
	jne	.L130
.L129:
	leaq	.LC5(%rip), %rax
	xorl	%edx, %edx
	leaq	-240(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -240(%rbp)
	movq	$13, -232(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L123
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$29, %esi
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L123:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L130:
	movq	0(%r13), %rax
	movq	3624(%r12), %rdx
	leaq	3624(%r12), %rsi
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	movq	-1(%rdx), %rcx
	movl	$2, %eax
	subq	$37592, %rdi
	cmpw	$64, 11(%rcx)
	jne	.L131
	xorl	%eax, %eax
	testb	$1, 11(%rdx)
	sete	%al
	addl	%eax, %eax
.L131:
	movl	%eax, -240(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -228(%rbp)
	movq	3624(%r12), %rax
	movq	%rdi, -216(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L157
.L132:
	leaq	-240(%rbp), %r8
	movq	%rsi, -208(%rbp)
	movq	%r8, %rdi
	movq	%r8, -264(%rbp)
	movq	$0, -200(%rbp)
	movq	%r13, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	%r13, -176(%rbp)
	movq	$-1, -168(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -236(%rbp)
	movq	-264(%rbp), %r8
	jne	.L133
	movq	-216(%rbp), %rax
	addq	$88, %rax
.L134:
	movq	(%rax), %rax
	movq	3616(%r12), %rdx
	sarq	$32, %rax
	movq	%rax, -264(%rbp)
	movq	0(%r13), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	movq	-1(%rdx), %rcx
	movl	$2, %eax
	subq	$37592, %rdi
	cmpw	$64, 11(%rcx)
	jne	.L135
	xorl	%eax, %eax
	testb	$1, 11(%rdx)
	sete	%al
	addl	%eax, %eax
.L135:
	movl	%eax, -240(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -228(%rbp)
	movq	3616(%r12), %rax
	movq	%rdi, -216(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L158
.L136:
	movq	%r8, %rdi
	movq	%r8, -272(%rbp)
	movq	%r15, -208(%rbp)
	movq	$0, -200(%rbp)
	movq	%r13, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	%r13, -176(%rbp)
	movq	$-1, -168(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -236(%rbp)
	movq	-272(%rbp), %r8
	jne	.L137
	movq	-216(%rbp), %rax
	leaq	88(%rax), %rdx
.L138:
	movl	-264(%rbp), %ecx
	movq	%r12, %rsi
	movq	%r8, %rdi
	movq	%r8, -264(%rbp)
	call	_ZN2v88internal18FrameArrayIteratorC1EPNS0_7IsolateENS0_6HandleINS0_10FrameArrayEEEi@PLT
	movq	-264(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN2v88internal18FrameArrayIterator5FrameEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*72(%rax)
	movq	(%rax), %r13
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L133:
	movq	%r8, %rdi
	movq	%r8, -264(%rbp)
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE@PLT
	movq	-264(%rbp), %r8
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L137:
	movq	%r8, %rdi
	movq	%r8, -272(%rbp)
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE@PLT
	movq	-272(%rbp), %r8
	movq	%rax, %rdx
	jmp	.L138
.L158:
	movq	%r15, %rsi
	movq	%r8, -272(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-272(%rbp), %r8
	movq	%rax, %r15
	jmp	.L136
.L157:
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rsi
	jmp	.L132
.L156:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20087:
	.size	_ZN2v88internalL43Builtin_Impl_CallSitePrototypeGetEvalOriginENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL43Builtin_Impl_CallSitePrototypeGetEvalOriginENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL54Builtin_Impl_CallSitePrototypeGetScriptNameOrSourceURLENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC6:
	.string	"getScriptNameOrSourceUrl"
	.section	.text._ZN2v88internalL54Builtin_Impl_CallSitePrototypeGetScriptNameOrSourceURLENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL54Builtin_Impl_CallSitePrototypeGetScriptNameOrSourceURLENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL54Builtin_Impl_CallSitePrototypeGetScriptNameOrSourceURLENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB20111:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L160
.L163:
	leaq	.LC6(%rip), %rax
	leaq	-256(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, -256(%rbp)
	movq	$24, -248(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$62, %esi
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L161
.L193:
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L166:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L178
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L178:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L194
	addq	$232, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L160:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L163
	leaq	3616(%rdx), %r15
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal10JSReceiver14HasOwnPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEE@PLT
	testb	%al, %al
	je	.L167
	shrw	$8, %ax
	jne	.L168
.L167:
	leaq	.LC6(%rip), %rax
	xorl	%edx, %edx
	leaq	-240(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -240(%rbp)
	movq	$24, -232(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L161
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$29, %esi
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L161:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L168:
	movq	0(%r13), %rax
	movq	3624(%r12), %rdx
	leaq	3624(%r12), %rsi
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	movq	-1(%rdx), %rcx
	movl	$2, %eax
	subq	$37592, %rdi
	cmpw	$64, 11(%rcx)
	jne	.L169
	xorl	%eax, %eax
	testb	$1, 11(%rdx)
	sete	%al
	addl	%eax, %eax
.L169:
	movl	%eax, -240(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -228(%rbp)
	movq	3624(%r12), %rax
	movq	%rdi, -216(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L195
.L170:
	leaq	-240(%rbp), %r8
	movq	%rsi, -208(%rbp)
	movq	%r8, %rdi
	movq	%r8, -264(%rbp)
	movq	$0, -200(%rbp)
	movq	%r13, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	%r13, -176(%rbp)
	movq	$-1, -168(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -236(%rbp)
	movq	-264(%rbp), %r8
	jne	.L171
	movq	-216(%rbp), %rax
	addq	$88, %rax
.L172:
	movq	(%rax), %rax
	movq	3616(%r12), %rdx
	sarq	$32, %rax
	movq	%rax, -264(%rbp)
	movq	0(%r13), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	movq	-1(%rdx), %rcx
	movl	$2, %eax
	subq	$37592, %rdi
	cmpw	$64, 11(%rcx)
	jne	.L173
	xorl	%eax, %eax
	testb	$1, 11(%rdx)
	sete	%al
	addl	%eax, %eax
.L173:
	movl	%eax, -240(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -228(%rbp)
	movq	3616(%r12), %rax
	movq	%rdi, -216(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L196
.L174:
	movq	%r8, %rdi
	movq	%r8, -272(%rbp)
	movq	%r15, -208(%rbp)
	movq	$0, -200(%rbp)
	movq	%r13, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	%r13, -176(%rbp)
	movq	$-1, -168(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -236(%rbp)
	movq	-272(%rbp), %r8
	jne	.L175
	movq	-216(%rbp), %rax
	leaq	88(%rax), %rdx
.L176:
	movl	-264(%rbp), %ecx
	movq	%r12, %rsi
	movq	%r8, %rdi
	movq	%r8, -264(%rbp)
	call	_ZN2v88internal18FrameArrayIteratorC1EPNS0_7IsolateENS0_6HandleINS0_10FrameArrayEEEi@PLT
	movq	-264(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN2v88internal18FrameArrayIterator5FrameEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*48(%rax)
	movq	(%rax), %r13
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L171:
	movq	%r8, %rdi
	movq	%r8, -264(%rbp)
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE@PLT
	movq	-264(%rbp), %r8
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L175:
	movq	%r8, %rdi
	movq	%r8, -272(%rbp)
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE@PLT
	movq	-272(%rbp), %r8
	movq	%rax, %rdx
	jmp	.L176
.L196:
	movq	%r15, %rsi
	movq	%r8, -272(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-272(%rbp), %r8
	movq	%rax, %r15
	jmp	.L174
.L195:
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rsi
	jmp	.L170
.L194:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20111:
	.size	_ZN2v88internalL54Builtin_Impl_CallSitePrototypeGetScriptNameOrSourceURLENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL54Builtin_Impl_CallSitePrototypeGetScriptNameOrSourceURLENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL41Builtin_Impl_CallSitePrototypeGetFileNameENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC7:
	.string	"getFileName"
	.section	.text._ZN2v88internalL41Builtin_Impl_CallSitePrototypeGetFileNameENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL41Builtin_Impl_CallSitePrototypeGetFileNameENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL41Builtin_Impl_CallSitePrototypeGetFileNameENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB20090:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L198
.L201:
	leaq	.LC7(%rip), %rax
	leaq	-256(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, -256(%rbp)
	movq	$11, -248(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$62, %esi
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L199
.L231:
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L204:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L216
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L216:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L232
	addq	$232, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L198:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L201
	leaq	3616(%rdx), %r15
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal10JSReceiver14HasOwnPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEE@PLT
	testb	%al, %al
	je	.L205
	shrw	$8, %ax
	jne	.L206
.L205:
	leaq	.LC7(%rip), %rax
	xorl	%edx, %edx
	leaq	-240(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -240(%rbp)
	movq	$11, -232(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L199
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$29, %esi
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L199:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L206:
	movq	0(%r13), %rax
	movq	3624(%r12), %rdx
	leaq	3624(%r12), %rsi
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	movq	-1(%rdx), %rcx
	movl	$2, %eax
	subq	$37592, %rdi
	cmpw	$64, 11(%rcx)
	jne	.L207
	xorl	%eax, %eax
	testb	$1, 11(%rdx)
	sete	%al
	addl	%eax, %eax
.L207:
	movl	%eax, -240(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -228(%rbp)
	movq	3624(%r12), %rax
	movq	%rdi, -216(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L233
.L208:
	leaq	-240(%rbp), %r8
	movq	%rsi, -208(%rbp)
	movq	%r8, %rdi
	movq	%r8, -264(%rbp)
	movq	$0, -200(%rbp)
	movq	%r13, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	%r13, -176(%rbp)
	movq	$-1, -168(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -236(%rbp)
	movq	-264(%rbp), %r8
	jne	.L209
	movq	-216(%rbp), %rax
	addq	$88, %rax
.L210:
	movq	(%rax), %rax
	movq	3616(%r12), %rdx
	sarq	$32, %rax
	movq	%rax, -264(%rbp)
	movq	0(%r13), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	movq	-1(%rdx), %rcx
	movl	$2, %eax
	subq	$37592, %rdi
	cmpw	$64, 11(%rcx)
	jne	.L211
	xorl	%eax, %eax
	testb	$1, 11(%rdx)
	sete	%al
	addl	%eax, %eax
.L211:
	movl	%eax, -240(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -228(%rbp)
	movq	3616(%r12), %rax
	movq	%rdi, -216(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L234
.L212:
	movq	%r8, %rdi
	movq	%r8, -272(%rbp)
	movq	%r15, -208(%rbp)
	movq	$0, -200(%rbp)
	movq	%r13, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	%r13, -176(%rbp)
	movq	$-1, -168(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -236(%rbp)
	movq	-272(%rbp), %r8
	jne	.L213
	movq	-216(%rbp), %rax
	leaq	88(%rax), %rdx
.L214:
	movl	-264(%rbp), %ecx
	movq	%r12, %rsi
	movq	%r8, %rdi
	movq	%r8, -264(%rbp)
	call	_ZN2v88internal18FrameArrayIteratorC1EPNS0_7IsolateENS0_6HandleINS0_10FrameArrayEEEi@PLT
	movq	-264(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN2v88internal18FrameArrayIterator5FrameEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*32(%rax)
	movq	(%rax), %r13
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L209:
	movq	%r8, %rdi
	movq	%r8, -264(%rbp)
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE@PLT
	movq	-264(%rbp), %r8
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L213:
	movq	%r8, %rdi
	movq	%r8, -272(%rbp)
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE@PLT
	movq	-272(%rbp), %r8
	movq	%rax, %rdx
	jmp	.L214
.L234:
	movq	%r15, %rsi
	movq	%r8, -272(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-272(%rbp), %r8
	movq	%rax, %r15
	jmp	.L212
.L233:
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rsi
	jmp	.L208
.L232:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20090:
	.size	_ZN2v88internalL41Builtin_Impl_CallSitePrototypeGetFileNameENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL41Builtin_Impl_CallSitePrototypeGetFileNameENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL45Builtin_Impl_CallSitePrototypeGetFunctionNameENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC8:
	.string	"getFunctionName"
	.section	.text._ZN2v88internalL45Builtin_Impl_CallSitePrototypeGetFunctionNameENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL45Builtin_Impl_CallSitePrototypeGetFunctionNameENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL45Builtin_Impl_CallSitePrototypeGetFunctionNameENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB20096:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L236
.L239:
	leaq	.LC8(%rip), %rax
	leaq	-256(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, -256(%rbp)
	movq	$15, -248(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$62, %esi
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L237
.L269:
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L242:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L254
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L254:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L270
	addq	$232, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L236:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L239
	leaq	3616(%rdx), %r15
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal10JSReceiver14HasOwnPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEE@PLT
	testb	%al, %al
	je	.L243
	shrw	$8, %ax
	jne	.L244
.L243:
	leaq	.LC8(%rip), %rax
	xorl	%edx, %edx
	leaq	-240(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -240(%rbp)
	movq	$15, -232(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L237
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$29, %esi
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L237:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L244:
	movq	0(%r13), %rax
	movq	3624(%r12), %rdx
	leaq	3624(%r12), %rsi
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	movq	-1(%rdx), %rcx
	movl	$2, %eax
	subq	$37592, %rdi
	cmpw	$64, 11(%rcx)
	jne	.L245
	xorl	%eax, %eax
	testb	$1, 11(%rdx)
	sete	%al
	addl	%eax, %eax
.L245:
	movl	%eax, -240(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -228(%rbp)
	movq	3624(%r12), %rax
	movq	%rdi, -216(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L271
.L246:
	leaq	-240(%rbp), %r8
	movq	%rsi, -208(%rbp)
	movq	%r8, %rdi
	movq	%r8, -264(%rbp)
	movq	$0, -200(%rbp)
	movq	%r13, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	%r13, -176(%rbp)
	movq	$-1, -168(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -236(%rbp)
	movq	-264(%rbp), %r8
	jne	.L247
	movq	-216(%rbp), %rax
	addq	$88, %rax
.L248:
	movq	(%rax), %rax
	movq	3616(%r12), %rdx
	sarq	$32, %rax
	movq	%rax, -264(%rbp)
	movq	0(%r13), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	movq	-1(%rdx), %rcx
	movl	$2, %eax
	subq	$37592, %rdi
	cmpw	$64, 11(%rcx)
	jne	.L249
	xorl	%eax, %eax
	testb	$1, 11(%rdx)
	sete	%al
	addl	%eax, %eax
.L249:
	movl	%eax, -240(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -228(%rbp)
	movq	3616(%r12), %rax
	movq	%rdi, -216(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L272
.L250:
	movq	%r8, %rdi
	movq	%r8, -272(%rbp)
	movq	%r15, -208(%rbp)
	movq	$0, -200(%rbp)
	movq	%r13, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	%r13, -176(%rbp)
	movq	$-1, -168(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -236(%rbp)
	movq	-272(%rbp), %r8
	jne	.L251
	movq	-216(%rbp), %rax
	leaq	88(%rax), %rdx
.L252:
	movl	-264(%rbp), %ecx
	movq	%r12, %rsi
	movq	%r8, %rdi
	movq	%r8, -264(%rbp)
	call	_ZN2v88internal18FrameArrayIteratorC1EPNS0_7IsolateENS0_6HandleINS0_10FrameArrayEEEi@PLT
	movq	-264(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN2v88internal18FrameArrayIterator5FrameEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*40(%rax)
	movq	(%rax), %r13
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L247:
	movq	%r8, %rdi
	movq	%r8, -264(%rbp)
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE@PLT
	movq	-264(%rbp), %r8
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L251:
	movq	%r8, %rdi
	movq	%r8, -272(%rbp)
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE@PLT
	movq	-272(%rbp), %r8
	movq	%rax, %rdx
	jmp	.L252
.L272:
	movq	%r15, %rsi
	movq	%r8, -272(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-272(%rbp), %r8
	movq	%rax, %r15
	jmp	.L250
.L271:
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rsi
	jmp	.L246
.L270:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20096:
	.size	_ZN2v88internalL45Builtin_Impl_CallSitePrototypeGetFunctionNameENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL45Builtin_Impl_CallSitePrototypeGetFunctionNameENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL43Builtin_Impl_CallSitePrototypeGetMethodNameENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC9:
	.string	"getMethodName"
	.section	.text._ZN2v88internalL43Builtin_Impl_CallSitePrototypeGetMethodNameENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL43Builtin_Impl_CallSitePrototypeGetMethodNameENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL43Builtin_Impl_CallSitePrototypeGetMethodNameENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB20102:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L274
.L277:
	leaq	.LC9(%rip), %rax
	leaq	-256(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, -256(%rbp)
	movq	$13, -248(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$62, %esi
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L275
.L307:
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L280:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L292
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L292:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L308
	addq	$232, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L274:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L277
	leaq	3616(%rdx), %r15
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal10JSReceiver14HasOwnPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEE@PLT
	testb	%al, %al
	je	.L281
	shrw	$8, %ax
	jne	.L282
.L281:
	leaq	.LC9(%rip), %rax
	xorl	%edx, %edx
	leaq	-240(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -240(%rbp)
	movq	$13, -232(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L275
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$29, %esi
	jmp	.L307
	.p2align 4,,10
	.p2align 3
.L275:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L282:
	movq	0(%r13), %rax
	movq	3624(%r12), %rdx
	leaq	3624(%r12), %rsi
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	movq	-1(%rdx), %rcx
	movl	$2, %eax
	subq	$37592, %rdi
	cmpw	$64, 11(%rcx)
	jne	.L283
	xorl	%eax, %eax
	testb	$1, 11(%rdx)
	sete	%al
	addl	%eax, %eax
.L283:
	movl	%eax, -240(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -228(%rbp)
	movq	3624(%r12), %rax
	movq	%rdi, -216(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L309
.L284:
	leaq	-240(%rbp), %r8
	movq	%rsi, -208(%rbp)
	movq	%r8, %rdi
	movq	%r8, -264(%rbp)
	movq	$0, -200(%rbp)
	movq	%r13, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	%r13, -176(%rbp)
	movq	$-1, -168(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -236(%rbp)
	movq	-264(%rbp), %r8
	jne	.L285
	movq	-216(%rbp), %rax
	addq	$88, %rax
.L286:
	movq	(%rax), %rax
	movq	3616(%r12), %rdx
	sarq	$32, %rax
	movq	%rax, -264(%rbp)
	movq	0(%r13), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	movq	-1(%rdx), %rcx
	movl	$2, %eax
	subq	$37592, %rdi
	cmpw	$64, 11(%rcx)
	jne	.L287
	xorl	%eax, %eax
	testb	$1, 11(%rdx)
	sete	%al
	addl	%eax, %eax
.L287:
	movl	%eax, -240(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -228(%rbp)
	movq	3616(%r12), %rax
	movq	%rdi, -216(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L310
.L288:
	movq	%r8, %rdi
	movq	%r8, -272(%rbp)
	movq	%r15, -208(%rbp)
	movq	$0, -200(%rbp)
	movq	%r13, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	%r13, -176(%rbp)
	movq	$-1, -168(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -236(%rbp)
	movq	-272(%rbp), %r8
	jne	.L289
	movq	-216(%rbp), %rax
	leaq	88(%rax), %rdx
.L290:
	movl	-264(%rbp), %ecx
	movq	%r12, %rsi
	movq	%r8, %rdi
	movq	%r8, -264(%rbp)
	call	_ZN2v88internal18FrameArrayIteratorC1EPNS0_7IsolateENS0_6HandleINS0_10FrameArrayEEEi@PLT
	movq	-264(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN2v88internal18FrameArrayIterator5FrameEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*56(%rax)
	movq	(%rax), %r13
	jmp	.L280
	.p2align 4,,10
	.p2align 3
.L285:
	movq	%r8, %rdi
	movq	%r8, -264(%rbp)
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE@PLT
	movq	-264(%rbp), %r8
	jmp	.L286
	.p2align 4,,10
	.p2align 3
.L289:
	movq	%r8, %rdi
	movq	%r8, -272(%rbp)
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE@PLT
	movq	-272(%rbp), %r8
	movq	%rax, %rdx
	jmp	.L290
.L310:
	movq	%r15, %rsi
	movq	%r8, -272(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-272(%rbp), %r8
	movq	%rax, %r15
	jmp	.L288
.L309:
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rsi
	jmp	.L284
.L308:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20102:
	.size	_ZN2v88internalL43Builtin_Impl_CallSitePrototypeGetMethodNameENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL43Builtin_Impl_CallSitePrototypeGetMethodNameENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL38Builtin_Impl_CallSitePrototypeIsNativeENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC10:
	.string	"isNative"
	.section	.text._ZN2v88internalL38Builtin_Impl_CallSitePrototypeIsNativeENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL38Builtin_Impl_CallSitePrototypeIsNativeENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL38Builtin_Impl_CallSitePrototypeIsNativeENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB20129:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L312
.L315:
	leaq	.LC10(%rip), %rax
	leaq	-256(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, -256(%rbp)
	movq	$8, -248(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$62, %esi
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L313
.L347:
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L318:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L332
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L332:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L348
	addq	$232, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L312:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L315
	leaq	3616(%rdx), %r15
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal10JSReceiver14HasOwnPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEE@PLT
	testb	%al, %al
	je	.L319
	shrw	$8, %ax
	jne	.L320
.L319:
	leaq	.LC10(%rip), %rax
	xorl	%edx, %edx
	leaq	-240(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -240(%rbp)
	movq	$8, -232(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L313
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$29, %esi
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L313:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L320:
	movq	0(%r13), %rax
	movq	3624(%r12), %rdx
	leaq	3624(%r12), %rsi
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	movq	-1(%rdx), %rcx
	movl	$2, %eax
	subq	$37592, %rdi
	cmpw	$64, 11(%rcx)
	jne	.L321
	xorl	%eax, %eax
	testb	$1, 11(%rdx)
	sete	%al
	addl	%eax, %eax
.L321:
	movl	%eax, -240(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -228(%rbp)
	movq	3624(%r12), %rax
	movq	%rdi, -216(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L349
.L322:
	leaq	-240(%rbp), %r8
	movq	%rsi, -208(%rbp)
	movq	%r8, %rdi
	movq	%r8, -264(%rbp)
	movq	$0, -200(%rbp)
	movq	%r13, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	%r13, -176(%rbp)
	movq	$-1, -168(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -236(%rbp)
	movq	-264(%rbp), %r8
	jne	.L323
	movq	-216(%rbp), %rax
	addq	$88, %rax
.L324:
	movq	(%rax), %rax
	movq	3616(%r12), %rdx
	sarq	$32, %rax
	movq	%rax, -264(%rbp)
	movq	0(%r13), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	movq	-1(%rdx), %rcx
	movl	$2, %eax
	subq	$37592, %rdi
	cmpw	$64, 11(%rcx)
	jne	.L325
	xorl	%eax, %eax
	testb	$1, 11(%rdx)
	sete	%al
	addl	%eax, %eax
.L325:
	movl	%eax, -240(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -228(%rbp)
	movq	3616(%r12), %rax
	movq	%rdi, -216(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L350
.L326:
	movq	%r8, %rdi
	movq	%r8, -272(%rbp)
	movq	%r15, -208(%rbp)
	movq	$0, -200(%rbp)
	movq	%r13, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	%r13, -176(%rbp)
	movq	$-1, -168(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -236(%rbp)
	movq	-272(%rbp), %r8
	jne	.L327
	movq	-216(%rbp), %rax
	leaq	88(%rax), %rdx
.L328:
	movl	-264(%rbp), %ecx
	movq	%r8, %rdi
	movq	%r12, %rsi
	movq	%r8, -264(%rbp)
	call	_ZN2v88internal18FrameArrayIteratorC1EPNS0_7IsolateENS0_6HandleINS0_10FrameArrayEEEi@PLT
	movq	-264(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN2v88internal18FrameArrayIterator5FrameEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*128(%rax)
	testb	%al, %al
	je	.L329
	movq	112(%r12), %r13
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L329:
	movq	120(%r12), %r13
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L327:
	movq	%r8, %rdi
	movq	%r8, -272(%rbp)
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE@PLT
	movq	-272(%rbp), %r8
	movq	%rax, %rdx
	jmp	.L328
	.p2align 4,,10
	.p2align 3
.L323:
	movq	%r8, %rdi
	movq	%r8, -264(%rbp)
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE@PLT
	movq	-264(%rbp), %r8
	jmp	.L324
.L350:
	movq	%r15, %rsi
	movq	%r8, -272(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-272(%rbp), %r8
	movq	%rax, %r15
	jmp	.L326
.L349:
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rsi
	jmp	.L322
.L348:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20129:
	.size	_ZN2v88internalL38Builtin_Impl_CallSitePrototypeIsNativeENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL38Builtin_Impl_CallSitePrototypeIsNativeENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL43Builtin_Impl_CallSitePrototypeIsConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC11:
	.string	"isConstructor"
	.section	.text._ZN2v88internalL43Builtin_Impl_CallSitePrototypeIsConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL43Builtin_Impl_CallSitePrototypeIsConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL43Builtin_Impl_CallSitePrototypeIsConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB20123:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L352
.L355:
	leaq	.LC11(%rip), %rax
	leaq	-256(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, -256(%rbp)
	movq	$13, -248(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$62, %esi
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L353
.L387:
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L358:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L372
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L372:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L388
	addq	$232, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L352:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L355
	leaq	3616(%rdx), %r15
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal10JSReceiver14HasOwnPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEE@PLT
	testb	%al, %al
	je	.L359
	shrw	$8, %ax
	jne	.L360
.L359:
	leaq	.LC11(%rip), %rax
	xorl	%edx, %edx
	leaq	-240(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -240(%rbp)
	movq	$13, -232(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L353
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$29, %esi
	jmp	.L387
	.p2align 4,,10
	.p2align 3
.L353:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L360:
	movq	0(%r13), %rax
	movq	3624(%r12), %rdx
	leaq	3624(%r12), %rsi
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	movq	-1(%rdx), %rcx
	movl	$2, %eax
	subq	$37592, %rdi
	cmpw	$64, 11(%rcx)
	jne	.L361
	xorl	%eax, %eax
	testb	$1, 11(%rdx)
	sete	%al
	addl	%eax, %eax
.L361:
	movl	%eax, -240(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -228(%rbp)
	movq	3624(%r12), %rax
	movq	%rdi, -216(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L389
.L362:
	leaq	-240(%rbp), %r8
	movq	%rsi, -208(%rbp)
	movq	%r8, %rdi
	movq	%r8, -264(%rbp)
	movq	$0, -200(%rbp)
	movq	%r13, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	%r13, -176(%rbp)
	movq	$-1, -168(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -236(%rbp)
	movq	-264(%rbp), %r8
	jne	.L363
	movq	-216(%rbp), %rax
	addq	$88, %rax
.L364:
	movq	(%rax), %rax
	movq	3616(%r12), %rdx
	sarq	$32, %rax
	movq	%rax, -264(%rbp)
	movq	0(%r13), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	movq	-1(%rdx), %rcx
	movl	$2, %eax
	subq	$37592, %rdi
	cmpw	$64, 11(%rcx)
	jne	.L365
	xorl	%eax, %eax
	testb	$1, 11(%rdx)
	sete	%al
	addl	%eax, %eax
.L365:
	movl	%eax, -240(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -228(%rbp)
	movq	3616(%r12), %rax
	movq	%rdi, -216(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L390
.L366:
	movq	%r8, %rdi
	movq	%r8, -272(%rbp)
	movq	%r15, -208(%rbp)
	movq	$0, -200(%rbp)
	movq	%r13, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	%r13, -176(%rbp)
	movq	$-1, -168(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -236(%rbp)
	movq	-272(%rbp), %r8
	jne	.L367
	movq	-216(%rbp), %rax
	leaq	88(%rax), %rdx
.L368:
	movl	-264(%rbp), %ecx
	movq	%r8, %rdi
	movq	%r12, %rsi
	movq	%r8, -264(%rbp)
	call	_ZN2v88internal18FrameArrayIteratorC1EPNS0_7IsolateENS0_6HandleINS0_10FrameArrayEEEi@PLT
	movq	-264(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN2v88internal18FrameArrayIterator5FrameEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*168(%rax)
	testb	%al, %al
	je	.L369
	movq	112(%r12), %r13
	jmp	.L358
	.p2align 4,,10
	.p2align 3
.L369:
	movq	120(%r12), %r13
	jmp	.L358
	.p2align 4,,10
	.p2align 3
.L367:
	movq	%r8, %rdi
	movq	%r8, -272(%rbp)
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE@PLT
	movq	-272(%rbp), %r8
	movq	%rax, %rdx
	jmp	.L368
	.p2align 4,,10
	.p2align 3
.L363:
	movq	%r8, %rdi
	movq	%r8, -264(%rbp)
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE@PLT
	movq	-264(%rbp), %r8
	jmp	.L364
.L390:
	movq	%r15, %rsi
	movq	%r8, -272(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-272(%rbp), %r8
	movq	%rax, %r15
	jmp	.L366
.L389:
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rsi
	jmp	.L362
.L388:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20123:
	.size	_ZN2v88internalL43Builtin_Impl_CallSitePrototypeIsConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL43Builtin_Impl_CallSitePrototypeIsConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL37Builtin_Impl_CallSitePrototypeIsAsyncENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC12:
	.string	"isAsync"
	.section	.text._ZN2v88internalL37Builtin_Impl_CallSitePrototypeIsAsyncENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL37Builtin_Impl_CallSitePrototypeIsAsyncENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL37Builtin_Impl_CallSitePrototypeIsAsyncENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB20120:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L392
.L395:
	leaq	.LC12(%rip), %rax
	leaq	-256(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, -256(%rbp)
	movq	$7, -248(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$62, %esi
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L393
.L427:
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L398:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L412
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L412:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L428
	addq	$232, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L392:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L395
	leaq	3616(%rdx), %r15
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal10JSReceiver14HasOwnPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEE@PLT
	testb	%al, %al
	je	.L399
	shrw	$8, %ax
	jne	.L400
.L399:
	leaq	.LC12(%rip), %rax
	xorl	%edx, %edx
	leaq	-240(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -240(%rbp)
	movq	$7, -232(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L393
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$29, %esi
	jmp	.L427
	.p2align 4,,10
	.p2align 3
.L393:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L400:
	movq	0(%r13), %rax
	movq	3624(%r12), %rdx
	leaq	3624(%r12), %rsi
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	movq	-1(%rdx), %rcx
	movl	$2, %eax
	subq	$37592, %rdi
	cmpw	$64, 11(%rcx)
	jne	.L401
	xorl	%eax, %eax
	testb	$1, 11(%rdx)
	sete	%al
	addl	%eax, %eax
.L401:
	movl	%eax, -240(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -228(%rbp)
	movq	3624(%r12), %rax
	movq	%rdi, -216(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L429
.L402:
	leaq	-240(%rbp), %r8
	movq	%rsi, -208(%rbp)
	movq	%r8, %rdi
	movq	%r8, -264(%rbp)
	movq	$0, -200(%rbp)
	movq	%r13, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	%r13, -176(%rbp)
	movq	$-1, -168(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -236(%rbp)
	movq	-264(%rbp), %r8
	jne	.L403
	movq	-216(%rbp), %rax
	addq	$88, %rax
.L404:
	movq	(%rax), %rax
	movq	3616(%r12), %rdx
	sarq	$32, %rax
	movq	%rax, -264(%rbp)
	movq	0(%r13), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	movq	-1(%rdx), %rcx
	movl	$2, %eax
	subq	$37592, %rdi
	cmpw	$64, 11(%rcx)
	jne	.L405
	xorl	%eax, %eax
	testb	$1, 11(%rdx)
	sete	%al
	addl	%eax, %eax
.L405:
	movl	%eax, -240(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -228(%rbp)
	movq	3616(%r12), %rax
	movq	%rdi, -216(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L430
.L406:
	movq	%r8, %rdi
	movq	%r8, -272(%rbp)
	movq	%r15, -208(%rbp)
	movq	$0, -200(%rbp)
	movq	%r13, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	%r13, -176(%rbp)
	movq	$-1, -168(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -236(%rbp)
	movq	-272(%rbp), %r8
	jne	.L407
	movq	-216(%rbp), %rax
	leaq	88(%rax), %rdx
.L408:
	movl	-264(%rbp), %ecx
	movq	%r8, %rdi
	movq	%r12, %rsi
	movq	%r8, -264(%rbp)
	call	_ZN2v88internal18FrameArrayIteratorC1EPNS0_7IsolateENS0_6HandleINS0_10FrameArrayEEEi@PLT
	movq	-264(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN2v88internal18FrameArrayIterator5FrameEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*152(%rax)
	testb	%al, %al
	je	.L409
	movq	112(%r12), %r13
	jmp	.L398
	.p2align 4,,10
	.p2align 3
.L409:
	movq	120(%r12), %r13
	jmp	.L398
	.p2align 4,,10
	.p2align 3
.L407:
	movq	%r8, %rdi
	movq	%r8, -272(%rbp)
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE@PLT
	movq	-272(%rbp), %r8
	movq	%rax, %rdx
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L403:
	movq	%r8, %rdi
	movq	%r8, -264(%rbp)
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE@PLT
	movq	-264(%rbp), %r8
	jmp	.L404
.L430:
	movq	%r15, %rsi
	movq	%r8, -272(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-272(%rbp), %r8
	movq	%rax, %r15
	jmp	.L406
.L429:
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rsi
	jmp	.L402
.L428:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20120:
	.size	_ZN2v88internalL37Builtin_Impl_CallSitePrototypeIsAsyncENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL37Builtin_Impl_CallSitePrototypeIsAsyncENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL36Builtin_Impl_CallSitePrototypeIsEvalENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC13:
	.string	"isEval"
	.section	.text._ZN2v88internalL36Builtin_Impl_CallSitePrototypeIsEvalENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL36Builtin_Impl_CallSitePrototypeIsEvalENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL36Builtin_Impl_CallSitePrototypeIsEvalENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB20126:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L432
.L435:
	leaq	.LC13(%rip), %rax
	leaq	-256(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, -256(%rbp)
	movq	$6, -248(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$62, %esi
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L433
.L467:
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L438:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L452
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L452:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L468
	addq	$232, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L432:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L435
	leaq	3616(%rdx), %r15
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal10JSReceiver14HasOwnPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEE@PLT
	testb	%al, %al
	je	.L439
	shrw	$8, %ax
	jne	.L440
.L439:
	leaq	.LC13(%rip), %rax
	xorl	%edx, %edx
	leaq	-240(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -240(%rbp)
	movq	$6, -232(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L433
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$29, %esi
	jmp	.L467
	.p2align 4,,10
	.p2align 3
.L433:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L440:
	movq	0(%r13), %rax
	movq	3624(%r12), %rdx
	leaq	3624(%r12), %rsi
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	movq	-1(%rdx), %rcx
	movl	$2, %eax
	subq	$37592, %rdi
	cmpw	$64, 11(%rcx)
	jne	.L441
	xorl	%eax, %eax
	testb	$1, 11(%rdx)
	sete	%al
	addl	%eax, %eax
.L441:
	movl	%eax, -240(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -228(%rbp)
	movq	3624(%r12), %rax
	movq	%rdi, -216(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L469
.L442:
	leaq	-240(%rbp), %r8
	movq	%rsi, -208(%rbp)
	movq	%r8, %rdi
	movq	%r8, -264(%rbp)
	movq	$0, -200(%rbp)
	movq	%r13, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	%r13, -176(%rbp)
	movq	$-1, -168(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -236(%rbp)
	movq	-264(%rbp), %r8
	jne	.L443
	movq	-216(%rbp), %rax
	addq	$88, %rax
.L444:
	movq	(%rax), %rax
	movq	3616(%r12), %rdx
	sarq	$32, %rax
	movq	%rax, -264(%rbp)
	movq	0(%r13), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	movq	-1(%rdx), %rcx
	movl	$2, %eax
	subq	$37592, %rdi
	cmpw	$64, 11(%rcx)
	jne	.L445
	xorl	%eax, %eax
	testb	$1, 11(%rdx)
	sete	%al
	addl	%eax, %eax
.L445:
	movl	%eax, -240(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -228(%rbp)
	movq	3616(%r12), %rax
	movq	%rdi, -216(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L470
.L446:
	movq	%r8, %rdi
	movq	%r8, -272(%rbp)
	movq	%r15, -208(%rbp)
	movq	$0, -200(%rbp)
	movq	%r13, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	%r13, -176(%rbp)
	movq	$-1, -168(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -236(%rbp)
	movq	-272(%rbp), %r8
	jne	.L447
	movq	-216(%rbp), %rax
	leaq	88(%rax), %rdx
.L448:
	movl	-264(%rbp), %ecx
	movq	%r8, %rdi
	movq	%r12, %rsi
	movq	%r8, -264(%rbp)
	call	_ZN2v88internal18FrameArrayIteratorC1EPNS0_7IsolateENS0_6HandleINS0_10FrameArrayEEEi@PLT
	movq	-264(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN2v88internal18FrameArrayIterator5FrameEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*144(%rax)
	testb	%al, %al
	je	.L449
	movq	112(%r12), %r13
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L449:
	movq	120(%r12), %r13
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L447:
	movq	%r8, %rdi
	movq	%r8, -272(%rbp)
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE@PLT
	movq	-272(%rbp), %r8
	movq	%rax, %rdx
	jmp	.L448
	.p2align 4,,10
	.p2align 3
.L443:
	movq	%r8, %rdi
	movq	%r8, -264(%rbp)
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE@PLT
	movq	-264(%rbp), %r8
	jmp	.L444
.L470:
	movq	%r15, %rsi
	movq	%r8, -272(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-272(%rbp), %r8
	movq	%rax, %r15
	jmp	.L446
.L469:
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rsi
	jmp	.L442
.L468:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20126:
	.size	_ZN2v88internalL36Builtin_Impl_CallSitePrototypeIsEvalENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL36Builtin_Impl_CallSitePrototypeIsEvalENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL42Builtin_Impl_CallSitePrototypeIsPromiseAllENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC14:
	.string	"isPromiseAll"
	.section	.text._ZN2v88internalL42Builtin_Impl_CallSitePrototypeIsPromiseAllENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL42Builtin_Impl_CallSitePrototypeIsPromiseAllENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL42Builtin_Impl_CallSitePrototypeIsPromiseAllENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB20132:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L472
.L475:
	leaq	.LC14(%rip), %rax
	leaq	-256(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, -256(%rbp)
	movq	$12, -248(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$62, %esi
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L473
.L507:
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L478:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L492
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L492:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L508
	addq	$232, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L472:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L475
	leaq	3616(%rdx), %r15
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal10JSReceiver14HasOwnPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEE@PLT
	testb	%al, %al
	je	.L479
	shrw	$8, %ax
	jne	.L480
.L479:
	leaq	.LC14(%rip), %rax
	xorl	%edx, %edx
	leaq	-240(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -240(%rbp)
	movq	$12, -232(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L473
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$29, %esi
	jmp	.L507
	.p2align 4,,10
	.p2align 3
.L473:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L480:
	movq	0(%r13), %rax
	movq	3624(%r12), %rdx
	leaq	3624(%r12), %rsi
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	movq	-1(%rdx), %rcx
	movl	$2, %eax
	subq	$37592, %rdi
	cmpw	$64, 11(%rcx)
	jne	.L481
	xorl	%eax, %eax
	testb	$1, 11(%rdx)
	sete	%al
	addl	%eax, %eax
.L481:
	movl	%eax, -240(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -228(%rbp)
	movq	3624(%r12), %rax
	movq	%rdi, -216(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L509
.L482:
	leaq	-240(%rbp), %r8
	movq	%rsi, -208(%rbp)
	movq	%r8, %rdi
	movq	%r8, -264(%rbp)
	movq	$0, -200(%rbp)
	movq	%r13, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	%r13, -176(%rbp)
	movq	$-1, -168(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -236(%rbp)
	movq	-264(%rbp), %r8
	jne	.L483
	movq	-216(%rbp), %rax
	addq	$88, %rax
.L484:
	movq	(%rax), %rax
	movq	3616(%r12), %rdx
	sarq	$32, %rax
	movq	%rax, -264(%rbp)
	movq	0(%r13), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	movq	-1(%rdx), %rcx
	movl	$2, %eax
	subq	$37592, %rdi
	cmpw	$64, 11(%rcx)
	jne	.L485
	xorl	%eax, %eax
	testb	$1, 11(%rdx)
	sete	%al
	addl	%eax, %eax
.L485:
	movl	%eax, -240(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -228(%rbp)
	movq	3616(%r12), %rax
	movq	%rdi, -216(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L510
.L486:
	movq	%r8, %rdi
	movq	%r8, -272(%rbp)
	movq	%r15, -208(%rbp)
	movq	$0, -200(%rbp)
	movq	%r13, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	%r13, -176(%rbp)
	movq	$-1, -168(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -236(%rbp)
	movq	-272(%rbp), %r8
	jne	.L487
	movq	-216(%rbp), %rax
	leaq	88(%rax), %rdx
.L488:
	movl	-264(%rbp), %ecx
	movq	%r8, %rdi
	movq	%r12, %rsi
	movq	%r8, -264(%rbp)
	call	_ZN2v88internal18FrameArrayIteratorC1EPNS0_7IsolateENS0_6HandleINS0_10FrameArrayEEEi@PLT
	movq	-264(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN2v88internal18FrameArrayIterator5FrameEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*160(%rax)
	testb	%al, %al
	je	.L489
	movq	112(%r12), %r13
	jmp	.L478
	.p2align 4,,10
	.p2align 3
.L489:
	movq	120(%r12), %r13
	jmp	.L478
	.p2align 4,,10
	.p2align 3
.L487:
	movq	%r8, %rdi
	movq	%r8, -272(%rbp)
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE@PLT
	movq	-272(%rbp), %r8
	movq	%rax, %rdx
	jmp	.L488
	.p2align 4,,10
	.p2align 3
.L483:
	movq	%r8, %rdi
	movq	%r8, -264(%rbp)
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE@PLT
	movq	-264(%rbp), %r8
	jmp	.L484
.L510:
	movq	%r15, %rsi
	movq	%r8, -272(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-272(%rbp), %r8
	movq	%rax, %r15
	jmp	.L486
.L509:
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rsi
	jmp	.L482
.L508:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20132:
	.size	_ZN2v88internalL42Builtin_Impl_CallSitePrototypeIsPromiseAllENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL42Builtin_Impl_CallSitePrototypeIsPromiseAllENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL40Builtin_Impl_CallSitePrototypeIsToplevelENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC15:
	.string	"isToplevel"
	.section	.text._ZN2v88internalL40Builtin_Impl_CallSitePrototypeIsToplevelENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL40Builtin_Impl_CallSitePrototypeIsToplevelENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL40Builtin_Impl_CallSitePrototypeIsToplevelENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB20135:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L512
.L515:
	leaq	.LC15(%rip), %rax
	leaq	-256(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, -256(%rbp)
	movq	$10, -248(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$62, %esi
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L513
.L547:
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L518:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L532
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L532:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L548
	addq	$232, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L512:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L515
	leaq	3616(%rdx), %r15
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal10JSReceiver14HasOwnPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEE@PLT
	testb	%al, %al
	je	.L519
	shrw	$8, %ax
	jne	.L520
.L519:
	leaq	.LC15(%rip), %rax
	xorl	%edx, %edx
	leaq	-240(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -240(%rbp)
	movq	$10, -232(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L513
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$29, %esi
	jmp	.L547
	.p2align 4,,10
	.p2align 3
.L513:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L520:
	movq	0(%r13), %rax
	movq	3624(%r12), %rdx
	leaq	3624(%r12), %rsi
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	movq	-1(%rdx), %rcx
	movl	$2, %eax
	subq	$37592, %rdi
	cmpw	$64, 11(%rcx)
	jne	.L521
	xorl	%eax, %eax
	testb	$1, 11(%rdx)
	sete	%al
	addl	%eax, %eax
.L521:
	movl	%eax, -240(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -228(%rbp)
	movq	3624(%r12), %rax
	movq	%rdi, -216(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L549
.L522:
	leaq	-240(%rbp), %r8
	movq	%rsi, -208(%rbp)
	movq	%r8, %rdi
	movq	%r8, -264(%rbp)
	movq	$0, -200(%rbp)
	movq	%r13, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	%r13, -176(%rbp)
	movq	$-1, -168(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -236(%rbp)
	movq	-264(%rbp), %r8
	jne	.L523
	movq	-216(%rbp), %rax
	addq	$88, %rax
.L524:
	movq	(%rax), %rax
	movq	3616(%r12), %rdx
	sarq	$32, %rax
	movq	%rax, -264(%rbp)
	movq	0(%r13), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	movq	-1(%rdx), %rcx
	movl	$2, %eax
	subq	$37592, %rdi
	cmpw	$64, 11(%rcx)
	jne	.L525
	xorl	%eax, %eax
	testb	$1, 11(%rdx)
	sete	%al
	addl	%eax, %eax
.L525:
	movl	%eax, -240(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -228(%rbp)
	movq	3616(%r12), %rax
	movq	%rdi, -216(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L550
.L526:
	movq	%r8, %rdi
	movq	%r8, -272(%rbp)
	movq	%r15, -208(%rbp)
	movq	$0, -200(%rbp)
	movq	%r13, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	%r13, -176(%rbp)
	movq	$-1, -168(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -236(%rbp)
	movq	-272(%rbp), %r8
	jne	.L527
	movq	-216(%rbp), %rax
	leaq	88(%rax), %rdx
.L528:
	movl	-264(%rbp), %ecx
	movq	%r8, %rdi
	movq	%r12, %rsi
	movq	%r8, -264(%rbp)
	call	_ZN2v88internal18FrameArrayIteratorC1EPNS0_7IsolateENS0_6HandleINS0_10FrameArrayEEEi@PLT
	movq	-264(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN2v88internal18FrameArrayIterator5FrameEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*136(%rax)
	testb	%al, %al
	je	.L529
	movq	112(%r12), %r13
	jmp	.L518
	.p2align 4,,10
	.p2align 3
.L529:
	movq	120(%r12), %r13
	jmp	.L518
	.p2align 4,,10
	.p2align 3
.L527:
	movq	%r8, %rdi
	movq	%r8, -272(%rbp)
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE@PLT
	movq	-272(%rbp), %r8
	movq	%rax, %rdx
	jmp	.L528
	.p2align 4,,10
	.p2align 3
.L523:
	movq	%r8, %rdi
	movq	%r8, -264(%rbp)
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE@PLT
	movq	-264(%rbp), %r8
	jmp	.L524
.L550:
	movq	%r15, %rsi
	movq	%r8, -272(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-272(%rbp), %r8
	movq	%rax, %r15
	jmp	.L526
.L549:
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rsi
	jmp	.L522
.L548:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20135:
	.size	_ZN2v88internalL40Builtin_Impl_CallSitePrototypeIsToplevelENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL40Builtin_Impl_CallSitePrototypeIsToplevelENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL45Builtin_Impl_CallSitePrototypeGetColumnNumberENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC16:
	.string	"getColumnNumber"
	.section	.text._ZN2v88internalL45Builtin_Impl_CallSitePrototypeGetColumnNumberENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL45Builtin_Impl_CallSitePrototypeGetColumnNumberENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL45Builtin_Impl_CallSitePrototypeGetColumnNumberENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB20081:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L552
.L555:
	leaq	.LC16(%rip), %rax
	leaq	-256(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, -256(%rbp)
	movq	$15, -248(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$62, %esi
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L553
.L587:
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L558:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L572
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L572:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L588
	addq	$232, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L552:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L555
	leaq	3616(%rdx), %r15
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal10JSReceiver14HasOwnPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEE@PLT
	testb	%al, %al
	je	.L559
	shrw	$8, %ax
	jne	.L560
.L559:
	leaq	.LC16(%rip), %rax
	xorl	%edx, %edx
	leaq	-240(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -240(%rbp)
	movq	$15, -232(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L553
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$29, %esi
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L553:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L560:
	movq	0(%r13), %rax
	movq	3624(%r12), %rdx
	leaq	3624(%r12), %rsi
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	movq	-1(%rdx), %rcx
	movl	$2, %eax
	subq	$37592, %rdi
	cmpw	$64, 11(%rcx)
	jne	.L561
	xorl	%eax, %eax
	testb	$1, 11(%rdx)
	sete	%al
	addl	%eax, %eax
.L561:
	movl	%eax, -240(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -228(%rbp)
	movq	3624(%r12), %rax
	movq	%rdi, -216(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L589
.L562:
	leaq	-240(%rbp), %r8
	movq	%rsi, -208(%rbp)
	movq	%r8, %rdi
	movq	%r8, -264(%rbp)
	movq	$0, -200(%rbp)
	movq	%r13, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	%r13, -176(%rbp)
	movq	$-1, -168(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -236(%rbp)
	movq	-264(%rbp), %r8
	jne	.L563
	movq	-216(%rbp), %rax
	addq	$88, %rax
.L564:
	movq	(%rax), %rax
	movq	3616(%r12), %rdx
	sarq	$32, %rax
	movq	%rax, -264(%rbp)
	movq	0(%r13), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	movq	-1(%rdx), %rcx
	movl	$2, %eax
	subq	$37592, %rdi
	cmpw	$64, 11(%rcx)
	jne	.L565
	xorl	%eax, %eax
	testb	$1, 11(%rdx)
	sete	%al
	addl	%eax, %eax
.L565:
	movl	%eax, -240(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -228(%rbp)
	movq	3616(%r12), %rax
	movq	%rdi, -216(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L590
.L566:
	movq	%r8, %rdi
	movq	%r8, -272(%rbp)
	movq	%r15, -208(%rbp)
	movq	$0, -200(%rbp)
	movq	%r13, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	%r13, -176(%rbp)
	movq	$-1, -168(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -236(%rbp)
	movq	-272(%rbp), %r8
	jne	.L567
	movq	-216(%rbp), %rax
	leaq	88(%rax), %rdx
.L568:
	movl	-264(%rbp), %ecx
	movq	%r12, %rsi
	movq	%r8, %rdi
	movq	%r8, -264(%rbp)
	call	_ZN2v88internal18FrameArrayIteratorC1EPNS0_7IsolateENS0_6HandleINS0_10FrameArrayEEEi@PLT
	movq	-264(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN2v88internal18FrameArrayIterator5FrameEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*112(%rax)
	movl	%eax, %esi
	testl	%eax, %eax
	jns	.L591
	movq	104(%r12), %r13
	jmp	.L558
	.p2align 4,,10
	.p2align 3
.L563:
	movq	%r8, %rdi
	movq	%r8, -264(%rbp)
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE@PLT
	movq	-264(%rbp), %r8
	jmp	.L564
	.p2align 4,,10
	.p2align 3
.L567:
	movq	%r8, %rdi
	movq	%r8, -272(%rbp)
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE@PLT
	movq	-272(%rbp), %r8
	movq	%rax, %rdx
	jmp	.L568
	.p2align 4,,10
	.p2align 3
.L591:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory16NewNumberFromIntEiNS0_14AllocationTypeE@PLT
	movq	(%rax), %r13
	jmp	.L558
.L590:
	movq	%r15, %rsi
	movq	%r8, -272(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-272(%rbp), %r8
	movq	%rax, %r15
	jmp	.L566
.L589:
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rsi
	jmp	.L562
.L588:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20081:
	.size	_ZN2v88internalL45Builtin_Impl_CallSitePrototypeGetColumnNumberENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL45Builtin_Impl_CallSitePrototypeGetColumnNumberENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL45Builtin_Impl_CallSitePrototypeGetPromiseIndexENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC17:
	.string	"getPromiseIndex"
	.section	.text._ZN2v88internalL45Builtin_Impl_CallSitePrototypeGetPromiseIndexENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL45Builtin_Impl_CallSitePrototypeGetPromiseIndexENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL45Builtin_Impl_CallSitePrototypeGetPromiseIndexENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB20108:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L593
.L596:
	leaq	.LC17(%rip), %rax
	leaq	-256(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, -256(%rbp)
	movq	$15, -248(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$62, %esi
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L594
.L628:
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L599:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L613
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L613:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L629
	addq	$232, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L593:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L596
	leaq	3616(%rdx), %r15
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal10JSReceiver14HasOwnPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEE@PLT
	testb	%al, %al
	je	.L600
	shrw	$8, %ax
	jne	.L601
.L600:
	leaq	.LC17(%rip), %rax
	xorl	%edx, %edx
	leaq	-240(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -240(%rbp)
	movq	$15, -232(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L594
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$29, %esi
	jmp	.L628
	.p2align 4,,10
	.p2align 3
.L594:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L601:
	movq	0(%r13), %rax
	movq	3624(%r12), %rdx
	leaq	3624(%r12), %rsi
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	movq	-1(%rdx), %rcx
	movl	$2, %eax
	subq	$37592, %rdi
	cmpw	$64, 11(%rcx)
	jne	.L602
	xorl	%eax, %eax
	testb	$1, 11(%rdx)
	sete	%al
	addl	%eax, %eax
.L602:
	movl	%eax, -240(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -228(%rbp)
	movq	3624(%r12), %rax
	movq	%rdi, -216(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L630
.L603:
	leaq	-240(%rbp), %r8
	movq	%rsi, -208(%rbp)
	movq	%r8, %rdi
	movq	%r8, -264(%rbp)
	movq	$0, -200(%rbp)
	movq	%r13, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	%r13, -176(%rbp)
	movq	$-1, -168(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -236(%rbp)
	movq	-264(%rbp), %r8
	jne	.L604
	movq	-216(%rbp), %rax
	addq	$88, %rax
.L605:
	movq	(%rax), %rax
	movq	3616(%r12), %rdx
	sarq	$32, %rax
	movq	%rax, -264(%rbp)
	movq	0(%r13), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	movq	-1(%rdx), %rcx
	movl	$2, %eax
	subq	$37592, %rdi
	cmpw	$64, 11(%rcx)
	jne	.L606
	xorl	%eax, %eax
	testb	$1, 11(%rdx)
	sete	%al
	addl	%eax, %eax
.L606:
	movl	%eax, -240(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -228(%rbp)
	movq	3616(%r12), %rax
	movq	%rdi, -216(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L631
.L607:
	movq	%r8, %rdi
	movq	%r8, -272(%rbp)
	movq	%r15, -208(%rbp)
	movq	$0, -200(%rbp)
	movq	%r13, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	%r13, -176(%rbp)
	movq	$-1, -168(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -236(%rbp)
	movq	-272(%rbp), %r8
	jne	.L608
	movq	-216(%rbp), %rax
	leaq	88(%rax), %rdx
.L609:
	movl	-264(%rbp), %ecx
	movq	%r12, %rsi
	movq	%r8, %rdi
	movq	%r8, -264(%rbp)
	call	_ZN2v88internal18FrameArrayIteratorC1EPNS0_7IsolateENS0_6HandleINS0_10FrameArrayEEEi@PLT
	movq	-264(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN2v88internal18FrameArrayIterator5FrameEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*120(%rax)
	movl	%eax, %esi
	testl	%eax, %eax
	jns	.L632
	movq	104(%r12), %r13
	jmp	.L599
	.p2align 4,,10
	.p2align 3
.L604:
	movq	%r8, %rdi
	movq	%r8, -264(%rbp)
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE@PLT
	movq	-264(%rbp), %r8
	jmp	.L605
	.p2align 4,,10
	.p2align 3
.L608:
	movq	%r8, %rdi
	movq	%r8, -272(%rbp)
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE@PLT
	movq	-272(%rbp), %r8
	movq	%rax, %rdx
	jmp	.L609
	.p2align 4,,10
	.p2align 3
.L632:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory16NewNumberFromIntEiNS0_14AllocationTypeE@PLT
	movq	(%rax), %r13
	jmp	.L599
.L631:
	movq	%r15, %rsi
	movq	%r8, -272(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-272(%rbp), %r8
	movq	%rax, %r15
	jmp	.L607
.L630:
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rsi
	jmp	.L603
.L629:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20108:
	.size	_ZN2v88internalL45Builtin_Impl_CallSitePrototypeGetPromiseIndexENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL45Builtin_Impl_CallSitePrototypeGetPromiseIndexENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL43Builtin_Impl_CallSitePrototypeGetLineNumberENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC18:
	.string	"getLineNumber"
	.section	.text._ZN2v88internalL43Builtin_Impl_CallSitePrototypeGetLineNumberENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL43Builtin_Impl_CallSitePrototypeGetLineNumberENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL43Builtin_Impl_CallSitePrototypeGetLineNumberENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB20099:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L634
.L637:
	leaq	.LC18(%rip), %rax
	leaq	-256(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, -256(%rbp)
	movq	$13, -248(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$62, %esi
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L635
.L669:
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L640:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L654
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L654:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L670
	addq	$232, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L634:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L637
	leaq	3616(%rdx), %r15
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal10JSReceiver14HasOwnPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEE@PLT
	testb	%al, %al
	je	.L641
	shrw	$8, %ax
	jne	.L642
.L641:
	leaq	.LC18(%rip), %rax
	xorl	%edx, %edx
	leaq	-240(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -240(%rbp)
	movq	$13, -232(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L635
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$29, %esi
	jmp	.L669
	.p2align 4,,10
	.p2align 3
.L635:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L642:
	movq	0(%r13), %rax
	movq	3624(%r12), %rdx
	leaq	3624(%r12), %rsi
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	movq	-1(%rdx), %rcx
	movl	$2, %eax
	subq	$37592, %rdi
	cmpw	$64, 11(%rcx)
	jne	.L643
	xorl	%eax, %eax
	testb	$1, 11(%rdx)
	sete	%al
	addl	%eax, %eax
.L643:
	movl	%eax, -240(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -228(%rbp)
	movq	3624(%r12), %rax
	movq	%rdi, -216(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L671
.L644:
	leaq	-240(%rbp), %r8
	movq	%rsi, -208(%rbp)
	movq	%r8, %rdi
	movq	%r8, -264(%rbp)
	movq	$0, -200(%rbp)
	movq	%r13, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	%r13, -176(%rbp)
	movq	$-1, -168(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -236(%rbp)
	movq	-264(%rbp), %r8
	jne	.L645
	movq	-216(%rbp), %rax
	addq	$88, %rax
.L646:
	movq	(%rax), %rax
	movq	3616(%r12), %rdx
	sarq	$32, %rax
	movq	%rax, -264(%rbp)
	movq	0(%r13), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	movq	-1(%rdx), %rcx
	movl	$2, %eax
	subq	$37592, %rdi
	cmpw	$64, 11(%rcx)
	jne	.L647
	xorl	%eax, %eax
	testb	$1, 11(%rdx)
	sete	%al
	addl	%eax, %eax
.L647:
	movl	%eax, -240(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -228(%rbp)
	movq	3616(%r12), %rax
	movq	%rdi, -216(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L672
.L648:
	movq	%r8, %rdi
	movq	%r8, -272(%rbp)
	movq	%r15, -208(%rbp)
	movq	$0, -200(%rbp)
	movq	%r13, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	%r13, -176(%rbp)
	movq	$-1, -168(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -236(%rbp)
	movq	-272(%rbp), %r8
	jne	.L649
	movq	-216(%rbp), %rax
	leaq	88(%rax), %rdx
.L650:
	movl	-264(%rbp), %ecx
	movq	%r12, %rsi
	movq	%r8, %rdi
	movq	%r8, -264(%rbp)
	call	_ZN2v88internal18FrameArrayIteratorC1EPNS0_7IsolateENS0_6HandleINS0_10FrameArrayEEEi@PLT
	movq	-264(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN2v88internal18FrameArrayIterator5FrameEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*104(%rax)
	movl	%eax, %esi
	testl	%eax, %eax
	jns	.L673
	movq	104(%r12), %r13
	jmp	.L640
	.p2align 4,,10
	.p2align 3
.L645:
	movq	%r8, %rdi
	movq	%r8, -264(%rbp)
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE@PLT
	movq	-264(%rbp), %r8
	jmp	.L646
	.p2align 4,,10
	.p2align 3
.L649:
	movq	%r8, %rdi
	movq	%r8, -272(%rbp)
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE@PLT
	movq	-272(%rbp), %r8
	movq	%rax, %rdx
	jmp	.L650
	.p2align 4,,10
	.p2align 3
.L673:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory16NewNumberFromIntEiNS0_14AllocationTypeE@PLT
	movq	(%rax), %r13
	jmp	.L640
.L672:
	movq	%r15, %rsi
	movq	%r8, -272(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-272(%rbp), %r8
	movq	%rax, %r15
	jmp	.L648
.L671:
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rsi
	jmp	.L644
.L670:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20099:
	.size	_ZN2v88internalL43Builtin_Impl_CallSitePrototypeGetLineNumberENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL43Builtin_Impl_CallSitePrototypeGetLineNumberENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL37Builtin_Impl_CallSitePrototypeGetThisENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC19:
	.string	"getThis"
	.section	.text._ZN2v88internalL37Builtin_Impl_CallSitePrototypeGetThisENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL37Builtin_Impl_CallSitePrototypeGetThisENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL37Builtin_Impl_CallSitePrototypeGetThisENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB20114:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L675
.L678:
	leaq	.LC19(%rip), %rax
	leaq	-256(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, -256(%rbp)
	movq	$7, -248(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$62, %esi
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L676
.L710:
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L681:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L695
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L695:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L711
	addq	$232, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L675:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L678
	leaq	3616(%rdx), %r15
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal10JSReceiver14HasOwnPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEE@PLT
	testb	%al, %al
	je	.L682
	shrw	$8, %ax
	jne	.L683
.L682:
	leaq	.LC19(%rip), %rax
	xorl	%edx, %edx
	leaq	-240(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -240(%rbp)
	movq	$7, -232(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L676
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$29, %esi
	jmp	.L710
	.p2align 4,,10
	.p2align 3
.L676:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L683:
	movq	0(%r13), %rax
	movq	3624(%r12), %rdx
	leaq	3624(%r12), %rsi
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	movq	-1(%rdx), %rcx
	movl	$2, %eax
	subq	$37592, %rdi
	cmpw	$64, 11(%rcx)
	jne	.L684
	xorl	%eax, %eax
	testb	$1, 11(%rdx)
	sete	%al
	addl	%eax, %eax
.L684:
	movl	%eax, -240(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -228(%rbp)
	movq	3624(%r12), %rax
	movq	%rdi, -216(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L712
.L685:
	leaq	-240(%rbp), %r8
	movq	%rsi, -208(%rbp)
	movq	%r8, %rdi
	movq	%r8, -264(%rbp)
	movq	$0, -200(%rbp)
	movq	%r13, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	%r13, -176(%rbp)
	movq	$-1, -168(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -236(%rbp)
	movq	-264(%rbp), %r8
	jne	.L686
	movq	-216(%rbp), %rax
	addq	$88, %rax
.L687:
	movq	(%rax), %rax
	movq	3616(%r12), %rdx
	sarq	$32, %rax
	movq	%rax, -264(%rbp)
	movq	0(%r13), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	movq	-1(%rdx), %rcx
	movl	$2, %eax
	subq	$37592, %rdi
	cmpw	$64, 11(%rcx)
	jne	.L688
	xorl	%eax, %eax
	testb	$1, 11(%rdx)
	sete	%al
	addl	%eax, %eax
.L688:
	movl	%eax, -240(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -228(%rbp)
	movq	3616(%r12), %rax
	movq	%rdi, -216(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L713
.L689:
	movq	%r8, %rdi
	movq	%r8, -272(%rbp)
	movq	%r15, -208(%rbp)
	movq	$0, -200(%rbp)
	movq	%r13, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	%r13, -176(%rbp)
	movq	$-1, -168(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -236(%rbp)
	movq	-272(%rbp), %r8
	jne	.L690
	movq	-216(%rbp), %rax
	leaq	88(%rax), %rdx
.L691:
	movl	-264(%rbp), %ecx
	movq	%r8, %rdi
	movq	%r12, %rsi
	movq	%r8, -264(%rbp)
	call	_ZN2v88internal18FrameArrayIteratorC1EPNS0_7IsolateENS0_6HandleINS0_10FrameArrayEEEi@PLT
	movq	-264(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN2v88internal18FrameArrayIterator5FrameEv@PLT
	movq	%rax, %r13
	movq	(%rax), %rax
	movq	%r13, %rdi
	call	*176(%rax)
	testb	%al, %al
	je	.L692
	movq	88(%r12), %r13
	jmp	.L681
	.p2align 4,,10
	.p2align 3
.L686:
	movq	%r8, %rdi
	movq	%r8, -264(%rbp)
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE@PLT
	movq	-264(%rbp), %r8
	jmp	.L687
	.p2align 4,,10
	.p2align 3
.L690:
	movq	%r8, %rdi
	movq	%r8, -272(%rbp)
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE@PLT
	movq	-272(%rbp), %r8
	movq	%rax, %rdx
	jmp	.L691
.L692:
	movl	$77, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate10CountUsageENS_7Isolate17UseCounterFeatureE@PLT
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	movq	(%rax), %r13
	jmp	.L681
.L713:
	movq	%r15, %rsi
	movq	%r8, -272(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-272(%rbp), %r8
	movq	%rax, %r15
	jmp	.L689
.L712:
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rsi
	jmp	.L685
.L711:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20114:
	.size	_ZN2v88internalL37Builtin_Impl_CallSitePrototypeGetThisENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL37Builtin_Impl_CallSitePrototypeGetThisENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL41Builtin_Impl_CallSitePrototypeGetFunctionENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC20:
	.string	"getFunction"
	.section	.text._ZN2v88internalL41Builtin_Impl_CallSitePrototypeGetFunctionENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL41Builtin_Impl_CallSitePrototypeGetFunctionENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL41Builtin_Impl_CallSitePrototypeGetFunctionENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB20093:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L715
.L718:
	leaq	.LC20(%rip), %rax
	leaq	-256(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, -256(%rbp)
	movq	$11, -248(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$62, %esi
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L716
.L750:
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L721:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L735
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L735:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L751
	addq	$232, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L715:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L718
	leaq	3616(%rdx), %r15
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal10JSReceiver14HasOwnPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEE@PLT
	testb	%al, %al
	je	.L722
	shrw	$8, %ax
	jne	.L723
.L722:
	leaq	.LC20(%rip), %rax
	xorl	%edx, %edx
	leaq	-240(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -240(%rbp)
	movq	$11, -232(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L716
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$29, %esi
	jmp	.L750
	.p2align 4,,10
	.p2align 3
.L716:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L723:
	movq	0(%r13), %rax
	movq	3624(%r12), %rdx
	leaq	3624(%r12), %rsi
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	movq	-1(%rdx), %rcx
	movl	$2, %eax
	subq	$37592, %rdi
	cmpw	$64, 11(%rcx)
	jne	.L724
	xorl	%eax, %eax
	testb	$1, 11(%rdx)
	sete	%al
	addl	%eax, %eax
.L724:
	movl	%eax, -240(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -228(%rbp)
	movq	3624(%r12), %rax
	movq	%rdi, -216(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L752
.L725:
	leaq	-240(%rbp), %r8
	movq	%rsi, -208(%rbp)
	movq	%r8, %rdi
	movq	%r8, -264(%rbp)
	movq	$0, -200(%rbp)
	movq	%r13, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	%r13, -176(%rbp)
	movq	$-1, -168(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -236(%rbp)
	movq	-264(%rbp), %r8
	jne	.L726
	movq	-216(%rbp), %rax
	addq	$88, %rax
.L727:
	movq	(%rax), %rax
	movq	3616(%r12), %rdx
	sarq	$32, %rax
	movq	%rax, -264(%rbp)
	movq	0(%r13), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	movq	-1(%rdx), %rcx
	movl	$2, %eax
	subq	$37592, %rdi
	cmpw	$64, 11(%rcx)
	jne	.L728
	xorl	%eax, %eax
	testb	$1, 11(%rdx)
	sete	%al
	addl	%eax, %eax
.L728:
	movl	%eax, -240(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -228(%rbp)
	movq	3616(%r12), %rax
	movq	%rdi, -216(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L753
.L729:
	movq	%r8, %rdi
	movq	%r8, -272(%rbp)
	movq	%r15, -208(%rbp)
	movq	$0, -200(%rbp)
	movq	%r13, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	%r13, -176(%rbp)
	movq	$-1, -168(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -236(%rbp)
	movq	-272(%rbp), %r8
	jne	.L730
	movq	-216(%rbp), %rax
	leaq	88(%rax), %rdx
.L731:
	movl	-264(%rbp), %ecx
	movq	%r8, %rdi
	movq	%r12, %rsi
	movq	%r8, -264(%rbp)
	call	_ZN2v88internal18FrameArrayIteratorC1EPNS0_7IsolateENS0_6HandleINS0_10FrameArrayEEEi@PLT
	movq	-264(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN2v88internal18FrameArrayIterator5FrameEv@PLT
	movq	%rax, %r13
	movq	(%rax), %rax
	movq	%r13, %rdi
	call	*176(%rax)
	testb	%al, %al
	je	.L732
	movq	88(%r12), %r13
	jmp	.L721
	.p2align 4,,10
	.p2align 3
.L726:
	movq	%r8, %rdi
	movq	%r8, -264(%rbp)
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE@PLT
	movq	-264(%rbp), %r8
	jmp	.L727
	.p2align 4,,10
	.p2align 3
.L730:
	movq	%r8, %rdi
	movq	%r8, -272(%rbp)
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE@PLT
	movq	-272(%rbp), %r8
	movq	%rax, %rdx
	jmp	.L731
.L732:
	movl	$76, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate10CountUsageENS_7Isolate17UseCounterFeatureE@PLT
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	movq	(%rax), %r13
	jmp	.L721
.L753:
	movq	%r15, %rsi
	movq	%r8, -272(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-272(%rbp), %r8
	movq	%rax, %r15
	jmp	.L729
.L752:
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rsi
	jmp	.L725
.L751:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20093:
	.size	_ZN2v88internalL41Builtin_Impl_CallSitePrototypeGetFunctionENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL41Builtin_Impl_CallSitePrototypeGetFunctionENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB8771:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L760
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L763
.L754:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L760:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L763:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L754
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE8771:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.rodata._ZN2v88internalL44Builtin_Impl_Stats_CallSitePrototypeToStringEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC21:
	.string	"disabled-by-default-v8.runtime"
	.align 8
.LC22:
	.string	"V8.Builtin_CallSitePrototypeToString"
	.section	.text._ZN2v88internalL44Builtin_Impl_Stats_CallSitePrototypeToStringEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL44Builtin_Impl_Stats_CallSitePrototypeToStringEiPmPNS0_7IsolateE, @function
_ZN2v88internalL44Builtin_Impl_Stats_CallSitePrototypeToStringEiPmPNS0_7IsolateE:
.LFB20136:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L793
.L765:
	movq	_ZZN2v88internalL44Builtin_Impl_Stats_CallSitePrototypeToStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic204(%rip), %rbx
	testq	%rbx, %rbx
	je	.L794
.L767:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L795
.L769:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL38Builtin_Impl_CallSitePrototypeToStringENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L796
.L773:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L797
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L794:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L798
.L768:
	movq	%rbx, _ZZN2v88internalL44Builtin_Impl_Stats_CallSitePrototypeToStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic204(%rip)
	jmp	.L767
	.p2align 4,,10
	.p2align 3
.L795:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L799
.L770:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L771
	movq	(%rdi), %rax
	call	*8(%rax)
.L771:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L772
	movq	(%rdi), %rax
	call	*8(%rax)
.L772:
	leaq	.LC22(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L769
	.p2align 4,,10
	.p2align 3
.L796:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L773
	.p2align 4,,10
	.p2align 3
.L793:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$711, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L765
	.p2align 4,,10
	.p2align 3
.L799:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC22(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L770
	.p2align 4,,10
	.p2align 3
.L798:
	leaq	.LC21(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L768
.L797:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20136:
	.size	_ZN2v88internalL44Builtin_Impl_Stats_CallSitePrototypeToStringEiPmPNS0_7IsolateE, .-_ZN2v88internalL44Builtin_Impl_Stats_CallSitePrototypeToStringEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL51Builtin_Impl_Stats_CallSitePrototypeGetColumnNumberEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC23:
	.string	"V8.Builtin_CallSitePrototypeGetColumnNumber"
	.section	.text._ZN2v88internalL51Builtin_Impl_Stats_CallSitePrototypeGetColumnNumberEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL51Builtin_Impl_Stats_CallSitePrototypeGetColumnNumberEiPmPNS0_7IsolateE, @function
_ZN2v88internalL51Builtin_Impl_Stats_CallSitePrototypeGetColumnNumberEiPmPNS0_7IsolateE:
.LFB20079:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L829
.L801:
	movq	_ZZN2v88internalL51Builtin_Impl_Stats_CallSitePrototypeGetColumnNumberEiPmPNS0_7IsolateEE27trace_event_unique_atomic48(%rip), %rbx
	testq	%rbx, %rbx
	je	.L830
.L803:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L831
.L805:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL45Builtin_Impl_CallSitePrototypeGetColumnNumberENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L832
.L809:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L833
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L830:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L834
.L804:
	movq	%rbx, _ZZN2v88internalL51Builtin_Impl_Stats_CallSitePrototypeGetColumnNumberEiPmPNS0_7IsolateEE27trace_event_unique_atomic48(%rip)
	jmp	.L803
	.p2align 4,,10
	.p2align 3
.L831:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L835
.L806:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L807
	movq	(%rdi), %rax
	call	*8(%rax)
.L807:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L808
	movq	(%rdi), %rax
	call	*8(%rax)
.L808:
	leaq	.LC23(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L805
	.p2align 4,,10
	.p2align 3
.L832:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L809
	.p2align 4,,10
	.p2align 3
.L829:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$693, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L801
	.p2align 4,,10
	.p2align 3
.L835:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC23(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L806
	.p2align 4,,10
	.p2align 3
.L834:
	leaq	.LC21(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L804
.L833:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20079:
	.size	_ZN2v88internalL51Builtin_Impl_Stats_CallSitePrototypeGetColumnNumberEiPmPNS0_7IsolateE, .-_ZN2v88internalL51Builtin_Impl_Stats_CallSitePrototypeGetColumnNumberEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL49Builtin_Impl_Stats_CallSitePrototypeGetEvalOriginEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC24:
	.string	"V8.Builtin_CallSitePrototypeGetEvalOrigin"
	.section	.text._ZN2v88internalL49Builtin_Impl_Stats_CallSitePrototypeGetEvalOriginEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL49Builtin_Impl_Stats_CallSitePrototypeGetEvalOriginEiPmPNS0_7IsolateE, @function
_ZN2v88internalL49Builtin_Impl_Stats_CallSitePrototypeGetEvalOriginEiPmPNS0_7IsolateE:
.LFB20085:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L865
.L837:
	movq	_ZZN2v88internalL49Builtin_Impl_Stats_CallSitePrototypeGetEvalOriginEiPmPNS0_7IsolateEE27trace_event_unique_atomic56(%rip), %rbx
	testq	%rbx, %rbx
	je	.L866
.L839:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L867
.L841:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL43Builtin_Impl_CallSitePrototypeGetEvalOriginENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L868
.L845:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L869
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L866:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L870
.L840:
	movq	%rbx, _ZZN2v88internalL49Builtin_Impl_Stats_CallSitePrototypeGetEvalOriginEiPmPNS0_7IsolateEE27trace_event_unique_atomic56(%rip)
	jmp	.L839
	.p2align 4,,10
	.p2align 3
.L867:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L871
.L842:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L843
	movq	(%rdi), %rax
	call	*8(%rax)
.L843:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L844
	movq	(%rdi), %rax
	call	*8(%rax)
.L844:
	leaq	.LC24(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L841
	.p2align 4,,10
	.p2align 3
.L868:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L845
	.p2align 4,,10
	.p2align 3
.L865:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$694, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L837
	.p2align 4,,10
	.p2align 3
.L871:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC24(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L842
	.p2align 4,,10
	.p2align 3
.L870:
	leaq	.LC21(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L840
.L869:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20085:
	.size	_ZN2v88internalL49Builtin_Impl_Stats_CallSitePrototypeGetEvalOriginEiPmPNS0_7IsolateE, .-_ZN2v88internalL49Builtin_Impl_Stats_CallSitePrototypeGetEvalOriginEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL47Builtin_Impl_Stats_CallSitePrototypeGetFileNameEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC25:
	.string	"V8.Builtin_CallSitePrototypeGetFileName"
	.section	.text._ZN2v88internalL47Builtin_Impl_Stats_CallSitePrototypeGetFileNameEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL47Builtin_Impl_Stats_CallSitePrototypeGetFileNameEiPmPNS0_7IsolateE, @function
_ZN2v88internalL47Builtin_Impl_Stats_CallSitePrototypeGetFileNameEiPmPNS0_7IsolateE:
.LFB20088:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L901
.L873:
	movq	_ZZN2v88internalL47Builtin_Impl_Stats_CallSitePrototypeGetFileNameEiPmPNS0_7IsolateEE27trace_event_unique_atomic64(%rip), %rbx
	testq	%rbx, %rbx
	je	.L902
.L875:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L903
.L877:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL41Builtin_Impl_CallSitePrototypeGetFileNameENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L904
.L881:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L905
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L902:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L906
.L876:
	movq	%rbx, _ZZN2v88internalL47Builtin_Impl_Stats_CallSitePrototypeGetFileNameEiPmPNS0_7IsolateEE27trace_event_unique_atomic64(%rip)
	jmp	.L875
	.p2align 4,,10
	.p2align 3
.L903:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L907
.L878:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L879
	movq	(%rdi), %rax
	call	*8(%rax)
.L879:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L880
	movq	(%rdi), %rax
	call	*8(%rax)
.L880:
	leaq	.LC25(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L877
	.p2align 4,,10
	.p2align 3
.L904:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L881
	.p2align 4,,10
	.p2align 3
.L901:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$695, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L873
	.p2align 4,,10
	.p2align 3
.L907:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC25(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L878
	.p2align 4,,10
	.p2align 3
.L906:
	leaq	.LC21(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L876
.L905:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20088:
	.size	_ZN2v88internalL47Builtin_Impl_Stats_CallSitePrototypeGetFileNameEiPmPNS0_7IsolateE, .-_ZN2v88internalL47Builtin_Impl_Stats_CallSitePrototypeGetFileNameEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL47Builtin_Impl_Stats_CallSitePrototypeGetFunctionEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC26:
	.string	"V8.Builtin_CallSitePrototypeGetFunction"
	.section	.text._ZN2v88internalL47Builtin_Impl_Stats_CallSitePrototypeGetFunctionEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL47Builtin_Impl_Stats_CallSitePrototypeGetFunctionEiPmPNS0_7IsolateE, @function
_ZN2v88internalL47Builtin_Impl_Stats_CallSitePrototypeGetFunctionEiPmPNS0_7IsolateE:
.LFB20091:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L937
.L909:
	movq	_ZZN2v88internalL47Builtin_Impl_Stats_CallSitePrototypeGetFunctionEiPmPNS0_7IsolateEE27trace_event_unique_atomic72(%rip), %rbx
	testq	%rbx, %rbx
	je	.L938
.L911:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L939
.L913:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL41Builtin_Impl_CallSitePrototypeGetFunctionENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L940
.L917:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L941
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L938:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L942
.L912:
	movq	%rbx, _ZZN2v88internalL47Builtin_Impl_Stats_CallSitePrototypeGetFunctionEiPmPNS0_7IsolateEE27trace_event_unique_atomic72(%rip)
	jmp	.L911
	.p2align 4,,10
	.p2align 3
.L939:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L943
.L914:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L915
	movq	(%rdi), %rax
	call	*8(%rax)
.L915:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L916
	movq	(%rdi), %rax
	call	*8(%rax)
.L916:
	leaq	.LC26(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L913
	.p2align 4,,10
	.p2align 3
.L940:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L917
	.p2align 4,,10
	.p2align 3
.L937:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$696, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L909
	.p2align 4,,10
	.p2align 3
.L943:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC26(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L914
	.p2align 4,,10
	.p2align 3
.L942:
	leaq	.LC21(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L912
.L941:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20091:
	.size	_ZN2v88internalL47Builtin_Impl_Stats_CallSitePrototypeGetFunctionEiPmPNS0_7IsolateE, .-_ZN2v88internalL47Builtin_Impl_Stats_CallSitePrototypeGetFunctionEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL51Builtin_Impl_Stats_CallSitePrototypeGetFunctionNameEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC27:
	.string	"V8.Builtin_CallSitePrototypeGetFunctionName"
	.section	.text._ZN2v88internalL51Builtin_Impl_Stats_CallSitePrototypeGetFunctionNameEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL51Builtin_Impl_Stats_CallSitePrototypeGetFunctionNameEiPmPNS0_7IsolateE, @function
_ZN2v88internalL51Builtin_Impl_Stats_CallSitePrototypeGetFunctionNameEiPmPNS0_7IsolateE:
.LFB20094:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L973
.L945:
	movq	_ZZN2v88internalL51Builtin_Impl_Stats_CallSitePrototypeGetFunctionNameEiPmPNS0_7IsolateEE27trace_event_unique_atomic86(%rip), %rbx
	testq	%rbx, %rbx
	je	.L974
.L947:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L975
.L949:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL45Builtin_Impl_CallSitePrototypeGetFunctionNameENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L976
.L953:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L977
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L974:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L978
.L948:
	movq	%rbx, _ZZN2v88internalL51Builtin_Impl_Stats_CallSitePrototypeGetFunctionNameEiPmPNS0_7IsolateEE27trace_event_unique_atomic86(%rip)
	jmp	.L947
	.p2align 4,,10
	.p2align 3
.L975:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L979
.L950:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L951
	movq	(%rdi), %rax
	call	*8(%rax)
.L951:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L952
	movq	(%rdi), %rax
	call	*8(%rax)
.L952:
	leaq	.LC27(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L949
	.p2align 4,,10
	.p2align 3
.L976:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L953
	.p2align 4,,10
	.p2align 3
.L973:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$697, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L945
	.p2align 4,,10
	.p2align 3
.L979:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC27(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L950
	.p2align 4,,10
	.p2align 3
.L978:
	leaq	.LC21(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L948
.L977:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20094:
	.size	_ZN2v88internalL51Builtin_Impl_Stats_CallSitePrototypeGetFunctionNameEiPmPNS0_7IsolateE, .-_ZN2v88internalL51Builtin_Impl_Stats_CallSitePrototypeGetFunctionNameEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL49Builtin_Impl_Stats_CallSitePrototypeGetLineNumberEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC28:
	.string	"V8.Builtin_CallSitePrototypeGetLineNumber"
	.section	.text._ZN2v88internalL49Builtin_Impl_Stats_CallSitePrototypeGetLineNumberEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL49Builtin_Impl_Stats_CallSitePrototypeGetLineNumberEiPmPNS0_7IsolateE, @function
_ZN2v88internalL49Builtin_Impl_Stats_CallSitePrototypeGetLineNumberEiPmPNS0_7IsolateE:
.LFB20097:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1009
.L981:
	movq	_ZZN2v88internalL49Builtin_Impl_Stats_CallSitePrototypeGetLineNumberEiPmPNS0_7IsolateEE27trace_event_unique_atomic94(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1010
.L983:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1011
.L985:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL43Builtin_Impl_CallSitePrototypeGetLineNumberENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1012
.L989:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1013
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1010:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1014
.L984:
	movq	%rbx, _ZZN2v88internalL49Builtin_Impl_Stats_CallSitePrototypeGetLineNumberEiPmPNS0_7IsolateEE27trace_event_unique_atomic94(%rip)
	jmp	.L983
	.p2align 4,,10
	.p2align 3
.L1011:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1015
.L986:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L987
	movq	(%rdi), %rax
	call	*8(%rax)
.L987:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L988
	movq	(%rdi), %rax
	call	*8(%rax)
.L988:
	leaq	.LC28(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L985
	.p2align 4,,10
	.p2align 3
.L1012:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L989
	.p2align 4,,10
	.p2align 3
.L1009:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$698, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L981
	.p2align 4,,10
	.p2align 3
.L1015:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC28(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L986
	.p2align 4,,10
	.p2align 3
.L1014:
	leaq	.LC21(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L984
.L1013:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20097:
	.size	_ZN2v88internalL49Builtin_Impl_Stats_CallSitePrototypeGetLineNumberEiPmPNS0_7IsolateE, .-_ZN2v88internalL49Builtin_Impl_Stats_CallSitePrototypeGetLineNumberEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL49Builtin_Impl_Stats_CallSitePrototypeGetMethodNameEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC29:
	.string	"V8.Builtin_CallSitePrototypeGetMethodName"
	.section	.text._ZN2v88internalL49Builtin_Impl_Stats_CallSitePrototypeGetMethodNameEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL49Builtin_Impl_Stats_CallSitePrototypeGetMethodNameEiPmPNS0_7IsolateE, @function
_ZN2v88internalL49Builtin_Impl_Stats_CallSitePrototypeGetMethodNameEiPmPNS0_7IsolateE:
.LFB20100:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1045
.L1017:
	movq	_ZZN2v88internalL49Builtin_Impl_Stats_CallSitePrototypeGetMethodNameEiPmPNS0_7IsolateEE28trace_event_unique_atomic102(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1046
.L1019:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1047
.L1021:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL43Builtin_Impl_CallSitePrototypeGetMethodNameENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1048
.L1025:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1049
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1046:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1050
.L1020:
	movq	%rbx, _ZZN2v88internalL49Builtin_Impl_Stats_CallSitePrototypeGetMethodNameEiPmPNS0_7IsolateEE28trace_event_unique_atomic102(%rip)
	jmp	.L1019
	.p2align 4,,10
	.p2align 3
.L1047:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1051
.L1022:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1023
	movq	(%rdi), %rax
	call	*8(%rax)
.L1023:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1024
	movq	(%rdi), %rax
	call	*8(%rax)
.L1024:
	leaq	.LC29(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1021
	.p2align 4,,10
	.p2align 3
.L1048:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1025
	.p2align 4,,10
	.p2align 3
.L1045:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$699, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1017
	.p2align 4,,10
	.p2align 3
.L1051:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC29(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L1022
	.p2align 4,,10
	.p2align 3
.L1050:
	leaq	.LC21(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1020
.L1049:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20100:
	.size	_ZN2v88internalL49Builtin_Impl_Stats_CallSitePrototypeGetMethodNameEiPmPNS0_7IsolateE, .-_ZN2v88internalL49Builtin_Impl_Stats_CallSitePrototypeGetMethodNameEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL47Builtin_Impl_Stats_CallSitePrototypeGetPositionEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC30:
	.string	"V8.Builtin_CallSitePrototypeGetPosition"
	.section	.text._ZN2v88internalL47Builtin_Impl_Stats_CallSitePrototypeGetPositionEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL47Builtin_Impl_Stats_CallSitePrototypeGetPositionEiPmPNS0_7IsolateE, @function
_ZN2v88internalL47Builtin_Impl_Stats_CallSitePrototypeGetPositionEiPmPNS0_7IsolateE:
.LFB20103:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1081
.L1053:
	movq	_ZZN2v88internalL47Builtin_Impl_Stats_CallSitePrototypeGetPositionEiPmPNS0_7IsolateEE28trace_event_unique_atomic110(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1082
.L1055:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1083
.L1057:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL41Builtin_Impl_CallSitePrototypeGetPositionENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1084
.L1061:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1085
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1082:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1086
.L1056:
	movq	%rbx, _ZZN2v88internalL47Builtin_Impl_Stats_CallSitePrototypeGetPositionEiPmPNS0_7IsolateEE28trace_event_unique_atomic110(%rip)
	jmp	.L1055
	.p2align 4,,10
	.p2align 3
.L1083:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1087
.L1058:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1059
	movq	(%rdi), %rax
	call	*8(%rax)
.L1059:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1060
	movq	(%rdi), %rax
	call	*8(%rax)
.L1060:
	leaq	.LC30(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1057
	.p2align 4,,10
	.p2align 3
.L1084:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1061
	.p2align 4,,10
	.p2align 3
.L1081:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$700, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1053
	.p2align 4,,10
	.p2align 3
.L1087:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC30(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L1058
	.p2align 4,,10
	.p2align 3
.L1086:
	leaq	.LC21(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1056
.L1085:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20103:
	.size	_ZN2v88internalL47Builtin_Impl_Stats_CallSitePrototypeGetPositionEiPmPNS0_7IsolateE, .-_ZN2v88internalL47Builtin_Impl_Stats_CallSitePrototypeGetPositionEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL51Builtin_Impl_Stats_CallSitePrototypeGetPromiseIndexEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC31:
	.string	"V8.Builtin_CallSitePrototypeGetPromiseIndex"
	.section	.text._ZN2v88internalL51Builtin_Impl_Stats_CallSitePrototypeGetPromiseIndexEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL51Builtin_Impl_Stats_CallSitePrototypeGetPromiseIndexEiPmPNS0_7IsolateE, @function
_ZN2v88internalL51Builtin_Impl_Stats_CallSitePrototypeGetPromiseIndexEiPmPNS0_7IsolateE:
.LFB20106:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1117
.L1089:
	movq	_ZZN2v88internalL51Builtin_Impl_Stats_CallSitePrototypeGetPromiseIndexEiPmPNS0_7IsolateEE28trace_event_unique_atomic118(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1118
.L1091:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1119
.L1093:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL45Builtin_Impl_CallSitePrototypeGetPromiseIndexENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1120
.L1097:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1121
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1118:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1122
.L1092:
	movq	%rbx, _ZZN2v88internalL51Builtin_Impl_Stats_CallSitePrototypeGetPromiseIndexEiPmPNS0_7IsolateEE28trace_event_unique_atomic118(%rip)
	jmp	.L1091
	.p2align 4,,10
	.p2align 3
.L1119:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1123
.L1094:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1095
	movq	(%rdi), %rax
	call	*8(%rax)
.L1095:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1096
	movq	(%rdi), %rax
	call	*8(%rax)
.L1096:
	leaq	.LC31(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1093
	.p2align 4,,10
	.p2align 3
.L1120:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1097
	.p2align 4,,10
	.p2align 3
.L1117:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$701, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1089
	.p2align 4,,10
	.p2align 3
.L1123:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC31(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L1094
	.p2align 4,,10
	.p2align 3
.L1122:
	leaq	.LC21(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1092
.L1121:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20106:
	.size	_ZN2v88internalL51Builtin_Impl_Stats_CallSitePrototypeGetPromiseIndexEiPmPNS0_7IsolateE, .-_ZN2v88internalL51Builtin_Impl_Stats_CallSitePrototypeGetPromiseIndexEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL60Builtin_Impl_Stats_CallSitePrototypeGetScriptNameOrSourceURLEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC32:
	.string	"V8.Builtin_CallSitePrototypeGetScriptNameOrSourceURL"
	.section	.text._ZN2v88internalL60Builtin_Impl_Stats_CallSitePrototypeGetScriptNameOrSourceURLEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL60Builtin_Impl_Stats_CallSitePrototypeGetScriptNameOrSourceURLEiPmPNS0_7IsolateE, @function
_ZN2v88internalL60Builtin_Impl_Stats_CallSitePrototypeGetScriptNameOrSourceURLEiPmPNS0_7IsolateE:
.LFB20109:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1153
.L1125:
	movq	_ZZN2v88internalL60Builtin_Impl_Stats_CallSitePrototypeGetScriptNameOrSourceURLEiPmPNS0_7IsolateEE28trace_event_unique_atomic126(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1154
.L1127:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1155
.L1129:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL54Builtin_Impl_CallSitePrototypeGetScriptNameOrSourceURLENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1156
.L1133:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1157
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1154:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1158
.L1128:
	movq	%rbx, _ZZN2v88internalL60Builtin_Impl_Stats_CallSitePrototypeGetScriptNameOrSourceURLEiPmPNS0_7IsolateEE28trace_event_unique_atomic126(%rip)
	jmp	.L1127
	.p2align 4,,10
	.p2align 3
.L1155:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1159
.L1130:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1131
	movq	(%rdi), %rax
	call	*8(%rax)
.L1131:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1132
	movq	(%rdi), %rax
	call	*8(%rax)
.L1132:
	leaq	.LC32(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1129
	.p2align 4,,10
	.p2align 3
.L1156:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1133
	.p2align 4,,10
	.p2align 3
.L1153:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$702, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1125
	.p2align 4,,10
	.p2align 3
.L1159:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC32(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L1130
	.p2align 4,,10
	.p2align 3
.L1158:
	leaq	.LC21(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1128
.L1157:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20109:
	.size	_ZN2v88internalL60Builtin_Impl_Stats_CallSitePrototypeGetScriptNameOrSourceURLEiPmPNS0_7IsolateE, .-_ZN2v88internalL60Builtin_Impl_Stats_CallSitePrototypeGetScriptNameOrSourceURLEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL43Builtin_Impl_Stats_CallSitePrototypeGetThisEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC33:
	.string	"V8.Builtin_CallSitePrototypeGetThis"
	.section	.text._ZN2v88internalL43Builtin_Impl_Stats_CallSitePrototypeGetThisEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL43Builtin_Impl_Stats_CallSitePrototypeGetThisEiPmPNS0_7IsolateE, @function
_ZN2v88internalL43Builtin_Impl_Stats_CallSitePrototypeGetThisEiPmPNS0_7IsolateE:
.LFB20112:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1189
.L1161:
	movq	_ZZN2v88internalL43Builtin_Impl_Stats_CallSitePrototypeGetThisEiPmPNS0_7IsolateEE28trace_event_unique_atomic134(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1190
.L1163:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1191
.L1165:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL37Builtin_Impl_CallSitePrototypeGetThisENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1192
.L1169:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1193
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1190:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1194
.L1164:
	movq	%rbx, _ZZN2v88internalL43Builtin_Impl_Stats_CallSitePrototypeGetThisEiPmPNS0_7IsolateEE28trace_event_unique_atomic134(%rip)
	jmp	.L1163
	.p2align 4,,10
	.p2align 3
.L1191:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1195
.L1166:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1167
	movq	(%rdi), %rax
	call	*8(%rax)
.L1167:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1168
	movq	(%rdi), %rax
	call	*8(%rax)
.L1168:
	leaq	.LC33(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1165
	.p2align 4,,10
	.p2align 3
.L1192:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1169
	.p2align 4,,10
	.p2align 3
.L1189:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$703, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1161
	.p2align 4,,10
	.p2align 3
.L1195:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC33(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L1166
	.p2align 4,,10
	.p2align 3
.L1194:
	leaq	.LC21(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1164
.L1193:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20112:
	.size	_ZN2v88internalL43Builtin_Impl_Stats_CallSitePrototypeGetThisEiPmPNS0_7IsolateE, .-_ZN2v88internalL43Builtin_Impl_Stats_CallSitePrototypeGetThisEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL47Builtin_Impl_Stats_CallSitePrototypeGetTypeNameEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC34:
	.string	"V8.Builtin_CallSitePrototypeGetTypeName"
	.section	.text._ZN2v88internalL47Builtin_Impl_Stats_CallSitePrototypeGetTypeNameEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL47Builtin_Impl_Stats_CallSitePrototypeGetTypeNameEiPmPNS0_7IsolateE, @function
_ZN2v88internalL47Builtin_Impl_Stats_CallSitePrototypeGetTypeNameEiPmPNS0_7IsolateE:
.LFB20115:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1225
.L1197:
	movq	_ZZN2v88internalL47Builtin_Impl_Stats_CallSitePrototypeGetTypeNameEiPmPNS0_7IsolateEE28trace_event_unique_atomic148(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1226
.L1199:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1227
.L1201:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL41Builtin_Impl_CallSitePrototypeGetTypeNameENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1228
.L1205:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1229
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1226:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1230
.L1200:
	movq	%rbx, _ZZN2v88internalL47Builtin_Impl_Stats_CallSitePrototypeGetTypeNameEiPmPNS0_7IsolateEE28trace_event_unique_atomic148(%rip)
	jmp	.L1199
	.p2align 4,,10
	.p2align 3
.L1227:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1231
.L1202:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1203
	movq	(%rdi), %rax
	call	*8(%rax)
.L1203:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1204
	movq	(%rdi), %rax
	call	*8(%rax)
.L1204:
	leaq	.LC34(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1201
	.p2align 4,,10
	.p2align 3
.L1228:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1205
	.p2align 4,,10
	.p2align 3
.L1225:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$704, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1197
	.p2align 4,,10
	.p2align 3
.L1231:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC34(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L1202
	.p2align 4,,10
	.p2align 3
.L1230:
	leaq	.LC21(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1200
.L1229:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20115:
	.size	_ZN2v88internalL47Builtin_Impl_Stats_CallSitePrototypeGetTypeNameEiPmPNS0_7IsolateE, .-_ZN2v88internalL47Builtin_Impl_Stats_CallSitePrototypeGetTypeNameEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL43Builtin_Impl_Stats_CallSitePrototypeIsAsyncEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC35:
	.string	"V8.Builtin_CallSitePrototypeIsAsync"
	.section	.text._ZN2v88internalL43Builtin_Impl_Stats_CallSitePrototypeIsAsyncEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL43Builtin_Impl_Stats_CallSitePrototypeIsAsyncEiPmPNS0_7IsolateE, @function
_ZN2v88internalL43Builtin_Impl_Stats_CallSitePrototypeIsAsyncEiPmPNS0_7IsolateE:
.LFB20118:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1261
.L1233:
	movq	_ZZN2v88internalL43Builtin_Impl_Stats_CallSitePrototypeIsAsyncEiPmPNS0_7IsolateEE28trace_event_unique_atomic156(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1262
.L1235:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1263
.L1237:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL37Builtin_Impl_CallSitePrototypeIsAsyncENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1264
.L1241:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1265
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1262:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1266
.L1236:
	movq	%rbx, _ZZN2v88internalL43Builtin_Impl_Stats_CallSitePrototypeIsAsyncEiPmPNS0_7IsolateEE28trace_event_unique_atomic156(%rip)
	jmp	.L1235
	.p2align 4,,10
	.p2align 3
.L1263:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1267
.L1238:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1239
	movq	(%rdi), %rax
	call	*8(%rax)
.L1239:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1240
	movq	(%rdi), %rax
	call	*8(%rax)
.L1240:
	leaq	.LC35(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1237
	.p2align 4,,10
	.p2align 3
.L1264:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1241
	.p2align 4,,10
	.p2align 3
.L1261:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$705, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1233
	.p2align 4,,10
	.p2align 3
.L1267:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC35(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L1238
	.p2align 4,,10
	.p2align 3
.L1266:
	leaq	.LC21(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1236
.L1265:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20118:
	.size	_ZN2v88internalL43Builtin_Impl_Stats_CallSitePrototypeIsAsyncEiPmPNS0_7IsolateE, .-_ZN2v88internalL43Builtin_Impl_Stats_CallSitePrototypeIsAsyncEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL49Builtin_Impl_Stats_CallSitePrototypeIsConstructorEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC36:
	.string	"V8.Builtin_CallSitePrototypeIsConstructor"
	.section	.text._ZN2v88internalL49Builtin_Impl_Stats_CallSitePrototypeIsConstructorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL49Builtin_Impl_Stats_CallSitePrototypeIsConstructorEiPmPNS0_7IsolateE, @function
_ZN2v88internalL49Builtin_Impl_Stats_CallSitePrototypeIsConstructorEiPmPNS0_7IsolateE:
.LFB20121:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1297
.L1269:
	movq	_ZZN2v88internalL49Builtin_Impl_Stats_CallSitePrototypeIsConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic164(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1298
.L1271:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1299
.L1273:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL43Builtin_Impl_CallSitePrototypeIsConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1300
.L1277:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1301
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1298:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1302
.L1272:
	movq	%rbx, _ZZN2v88internalL49Builtin_Impl_Stats_CallSitePrototypeIsConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic164(%rip)
	jmp	.L1271
	.p2align 4,,10
	.p2align 3
.L1299:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1303
.L1274:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1275
	movq	(%rdi), %rax
	call	*8(%rax)
.L1275:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1276
	movq	(%rdi), %rax
	call	*8(%rax)
.L1276:
	leaq	.LC36(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1273
	.p2align 4,,10
	.p2align 3
.L1300:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1277
	.p2align 4,,10
	.p2align 3
.L1297:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$706, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1269
	.p2align 4,,10
	.p2align 3
.L1303:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC36(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L1274
	.p2align 4,,10
	.p2align 3
.L1302:
	leaq	.LC21(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1272
.L1301:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20121:
	.size	_ZN2v88internalL49Builtin_Impl_Stats_CallSitePrototypeIsConstructorEiPmPNS0_7IsolateE, .-_ZN2v88internalL49Builtin_Impl_Stats_CallSitePrototypeIsConstructorEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL42Builtin_Impl_Stats_CallSitePrototypeIsEvalEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC37:
	.string	"V8.Builtin_CallSitePrototypeIsEval"
	.section	.text._ZN2v88internalL42Builtin_Impl_Stats_CallSitePrototypeIsEvalEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL42Builtin_Impl_Stats_CallSitePrototypeIsEvalEiPmPNS0_7IsolateE, @function
_ZN2v88internalL42Builtin_Impl_Stats_CallSitePrototypeIsEvalEiPmPNS0_7IsolateE:
.LFB20124:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1333
.L1305:
	movq	_ZZN2v88internalL42Builtin_Impl_Stats_CallSitePrototypeIsEvalEiPmPNS0_7IsolateEE28trace_event_unique_atomic172(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1334
.L1307:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1335
.L1309:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL36Builtin_Impl_CallSitePrototypeIsEvalENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1336
.L1313:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1337
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1334:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1338
.L1308:
	movq	%rbx, _ZZN2v88internalL42Builtin_Impl_Stats_CallSitePrototypeIsEvalEiPmPNS0_7IsolateEE28trace_event_unique_atomic172(%rip)
	jmp	.L1307
	.p2align 4,,10
	.p2align 3
.L1335:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1339
.L1310:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1311
	movq	(%rdi), %rax
	call	*8(%rax)
.L1311:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1312
	movq	(%rdi), %rax
	call	*8(%rax)
.L1312:
	leaq	.LC37(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1309
	.p2align 4,,10
	.p2align 3
.L1336:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1313
	.p2align 4,,10
	.p2align 3
.L1333:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$707, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1305
	.p2align 4,,10
	.p2align 3
.L1339:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC37(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L1310
	.p2align 4,,10
	.p2align 3
.L1338:
	leaq	.LC21(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1308
.L1337:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20124:
	.size	_ZN2v88internalL42Builtin_Impl_Stats_CallSitePrototypeIsEvalEiPmPNS0_7IsolateE, .-_ZN2v88internalL42Builtin_Impl_Stats_CallSitePrototypeIsEvalEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL44Builtin_Impl_Stats_CallSitePrototypeIsNativeEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC38:
	.string	"V8.Builtin_CallSitePrototypeIsNative"
	.section	.text._ZN2v88internalL44Builtin_Impl_Stats_CallSitePrototypeIsNativeEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL44Builtin_Impl_Stats_CallSitePrototypeIsNativeEiPmPNS0_7IsolateE, @function
_ZN2v88internalL44Builtin_Impl_Stats_CallSitePrototypeIsNativeEiPmPNS0_7IsolateE:
.LFB20127:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1369
.L1341:
	movq	_ZZN2v88internalL44Builtin_Impl_Stats_CallSitePrototypeIsNativeEiPmPNS0_7IsolateEE28trace_event_unique_atomic180(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1370
.L1343:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1371
.L1345:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL38Builtin_Impl_CallSitePrototypeIsNativeENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1372
.L1349:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1373
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1370:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1374
.L1344:
	movq	%rbx, _ZZN2v88internalL44Builtin_Impl_Stats_CallSitePrototypeIsNativeEiPmPNS0_7IsolateEE28trace_event_unique_atomic180(%rip)
	jmp	.L1343
	.p2align 4,,10
	.p2align 3
.L1371:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1375
.L1346:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1347
	movq	(%rdi), %rax
	call	*8(%rax)
.L1347:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1348
	movq	(%rdi), %rax
	call	*8(%rax)
.L1348:
	leaq	.LC38(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1345
	.p2align 4,,10
	.p2align 3
.L1372:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1349
	.p2align 4,,10
	.p2align 3
.L1369:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$708, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1341
	.p2align 4,,10
	.p2align 3
.L1375:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC38(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L1346
	.p2align 4,,10
	.p2align 3
.L1374:
	leaq	.LC21(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1344
.L1373:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20127:
	.size	_ZN2v88internalL44Builtin_Impl_Stats_CallSitePrototypeIsNativeEiPmPNS0_7IsolateE, .-_ZN2v88internalL44Builtin_Impl_Stats_CallSitePrototypeIsNativeEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL48Builtin_Impl_Stats_CallSitePrototypeIsPromiseAllEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC39:
	.string	"V8.Builtin_CallSitePrototypeIsPromiseAll"
	.section	.text._ZN2v88internalL48Builtin_Impl_Stats_CallSitePrototypeIsPromiseAllEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL48Builtin_Impl_Stats_CallSitePrototypeIsPromiseAllEiPmPNS0_7IsolateE, @function
_ZN2v88internalL48Builtin_Impl_Stats_CallSitePrototypeIsPromiseAllEiPmPNS0_7IsolateE:
.LFB20130:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1405
.L1377:
	movq	_ZZN2v88internalL48Builtin_Impl_Stats_CallSitePrototypeIsPromiseAllEiPmPNS0_7IsolateEE28trace_event_unique_atomic188(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1406
.L1379:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1407
.L1381:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL42Builtin_Impl_CallSitePrototypeIsPromiseAllENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1408
.L1385:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1409
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1406:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1410
.L1380:
	movq	%rbx, _ZZN2v88internalL48Builtin_Impl_Stats_CallSitePrototypeIsPromiseAllEiPmPNS0_7IsolateEE28trace_event_unique_atomic188(%rip)
	jmp	.L1379
	.p2align 4,,10
	.p2align 3
.L1407:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1411
.L1382:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1383
	movq	(%rdi), %rax
	call	*8(%rax)
.L1383:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1384
	movq	(%rdi), %rax
	call	*8(%rax)
.L1384:
	leaq	.LC39(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1381
	.p2align 4,,10
	.p2align 3
.L1408:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1385
	.p2align 4,,10
	.p2align 3
.L1405:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$709, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1377
	.p2align 4,,10
	.p2align 3
.L1411:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC39(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L1382
	.p2align 4,,10
	.p2align 3
.L1410:
	leaq	.LC21(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1380
.L1409:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20130:
	.size	_ZN2v88internalL48Builtin_Impl_Stats_CallSitePrototypeIsPromiseAllEiPmPNS0_7IsolateE, .-_ZN2v88internalL48Builtin_Impl_Stats_CallSitePrototypeIsPromiseAllEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL46Builtin_Impl_Stats_CallSitePrototypeIsToplevelEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC40:
	.string	"V8.Builtin_CallSitePrototypeIsToplevel"
	.section	.text._ZN2v88internalL46Builtin_Impl_Stats_CallSitePrototypeIsToplevelEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL46Builtin_Impl_Stats_CallSitePrototypeIsToplevelEiPmPNS0_7IsolateE, @function
_ZN2v88internalL46Builtin_Impl_Stats_CallSitePrototypeIsToplevelEiPmPNS0_7IsolateE:
.LFB20133:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1441
.L1413:
	movq	_ZZN2v88internalL46Builtin_Impl_Stats_CallSitePrototypeIsToplevelEiPmPNS0_7IsolateEE28trace_event_unique_atomic196(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1442
.L1415:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1443
.L1417:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL40Builtin_Impl_CallSitePrototypeIsToplevelENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1444
.L1421:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1445
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1442:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1446
.L1416:
	movq	%rbx, _ZZN2v88internalL46Builtin_Impl_Stats_CallSitePrototypeIsToplevelEiPmPNS0_7IsolateEE28trace_event_unique_atomic196(%rip)
	jmp	.L1415
	.p2align 4,,10
	.p2align 3
.L1443:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1447
.L1418:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1419
	movq	(%rdi), %rax
	call	*8(%rax)
.L1419:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1420
	movq	(%rdi), %rax
	call	*8(%rax)
.L1420:
	leaq	.LC40(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1417
	.p2align 4,,10
	.p2align 3
.L1444:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1421
	.p2align 4,,10
	.p2align 3
.L1441:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$710, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1413
	.p2align 4,,10
	.p2align 3
.L1447:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC40(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L1418
	.p2align 4,,10
	.p2align 3
.L1446:
	leaq	.LC21(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1416
.L1445:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20133:
	.size	_ZN2v88internalL46Builtin_Impl_Stats_CallSitePrototypeIsToplevelEiPmPNS0_7IsolateE, .-_ZN2v88internalL46Builtin_Impl_Stats_CallSitePrototypeIsToplevelEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal40Builtin_CallSitePrototypeGetColumnNumberEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal40Builtin_CallSitePrototypeGetColumnNumberEiPmPNS0_7IsolateE
	.type	_ZN2v88internal40Builtin_CallSitePrototypeGetColumnNumberEiPmPNS0_7IsolateE, @function
_ZN2v88internal40Builtin_CallSitePrototypeGetColumnNumberEiPmPNS0_7IsolateE:
.LFB20080:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1452
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL45Builtin_Impl_CallSitePrototypeGetColumnNumberENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1452:
	.cfi_restore 6
	jmp	_ZN2v88internalL51Builtin_Impl_Stats_CallSitePrototypeGetColumnNumberEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE20080:
	.size	_ZN2v88internal40Builtin_CallSitePrototypeGetColumnNumberEiPmPNS0_7IsolateE, .-_ZN2v88internal40Builtin_CallSitePrototypeGetColumnNumberEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal38Builtin_CallSitePrototypeGetEvalOriginEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal38Builtin_CallSitePrototypeGetEvalOriginEiPmPNS0_7IsolateE
	.type	_ZN2v88internal38Builtin_CallSitePrototypeGetEvalOriginEiPmPNS0_7IsolateE, @function
_ZN2v88internal38Builtin_CallSitePrototypeGetEvalOriginEiPmPNS0_7IsolateE:
.LFB20086:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1457
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL43Builtin_Impl_CallSitePrototypeGetEvalOriginENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1457:
	.cfi_restore 6
	jmp	_ZN2v88internalL49Builtin_Impl_Stats_CallSitePrototypeGetEvalOriginEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE20086:
	.size	_ZN2v88internal38Builtin_CallSitePrototypeGetEvalOriginEiPmPNS0_7IsolateE, .-_ZN2v88internal38Builtin_CallSitePrototypeGetEvalOriginEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal36Builtin_CallSitePrototypeGetFileNameEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal36Builtin_CallSitePrototypeGetFileNameEiPmPNS0_7IsolateE
	.type	_ZN2v88internal36Builtin_CallSitePrototypeGetFileNameEiPmPNS0_7IsolateE, @function
_ZN2v88internal36Builtin_CallSitePrototypeGetFileNameEiPmPNS0_7IsolateE:
.LFB20089:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1462
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL41Builtin_Impl_CallSitePrototypeGetFileNameENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1462:
	.cfi_restore 6
	jmp	_ZN2v88internalL47Builtin_Impl_Stats_CallSitePrototypeGetFileNameEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE20089:
	.size	_ZN2v88internal36Builtin_CallSitePrototypeGetFileNameEiPmPNS0_7IsolateE, .-_ZN2v88internal36Builtin_CallSitePrototypeGetFileNameEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal36Builtin_CallSitePrototypeGetFunctionEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal36Builtin_CallSitePrototypeGetFunctionEiPmPNS0_7IsolateE
	.type	_ZN2v88internal36Builtin_CallSitePrototypeGetFunctionEiPmPNS0_7IsolateE, @function
_ZN2v88internal36Builtin_CallSitePrototypeGetFunctionEiPmPNS0_7IsolateE:
.LFB20092:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1467
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL41Builtin_Impl_CallSitePrototypeGetFunctionENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1467:
	.cfi_restore 6
	jmp	_ZN2v88internalL47Builtin_Impl_Stats_CallSitePrototypeGetFunctionEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE20092:
	.size	_ZN2v88internal36Builtin_CallSitePrototypeGetFunctionEiPmPNS0_7IsolateE, .-_ZN2v88internal36Builtin_CallSitePrototypeGetFunctionEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal40Builtin_CallSitePrototypeGetFunctionNameEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal40Builtin_CallSitePrototypeGetFunctionNameEiPmPNS0_7IsolateE
	.type	_ZN2v88internal40Builtin_CallSitePrototypeGetFunctionNameEiPmPNS0_7IsolateE, @function
_ZN2v88internal40Builtin_CallSitePrototypeGetFunctionNameEiPmPNS0_7IsolateE:
.LFB20095:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1472
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL45Builtin_Impl_CallSitePrototypeGetFunctionNameENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1472:
	.cfi_restore 6
	jmp	_ZN2v88internalL51Builtin_Impl_Stats_CallSitePrototypeGetFunctionNameEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE20095:
	.size	_ZN2v88internal40Builtin_CallSitePrototypeGetFunctionNameEiPmPNS0_7IsolateE, .-_ZN2v88internal40Builtin_CallSitePrototypeGetFunctionNameEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal38Builtin_CallSitePrototypeGetLineNumberEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal38Builtin_CallSitePrototypeGetLineNumberEiPmPNS0_7IsolateE
	.type	_ZN2v88internal38Builtin_CallSitePrototypeGetLineNumberEiPmPNS0_7IsolateE, @function
_ZN2v88internal38Builtin_CallSitePrototypeGetLineNumberEiPmPNS0_7IsolateE:
.LFB20098:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1477
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL43Builtin_Impl_CallSitePrototypeGetLineNumberENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1477:
	.cfi_restore 6
	jmp	_ZN2v88internalL49Builtin_Impl_Stats_CallSitePrototypeGetLineNumberEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE20098:
	.size	_ZN2v88internal38Builtin_CallSitePrototypeGetLineNumberEiPmPNS0_7IsolateE, .-_ZN2v88internal38Builtin_CallSitePrototypeGetLineNumberEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal38Builtin_CallSitePrototypeGetMethodNameEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal38Builtin_CallSitePrototypeGetMethodNameEiPmPNS0_7IsolateE
	.type	_ZN2v88internal38Builtin_CallSitePrototypeGetMethodNameEiPmPNS0_7IsolateE, @function
_ZN2v88internal38Builtin_CallSitePrototypeGetMethodNameEiPmPNS0_7IsolateE:
.LFB20101:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1482
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL43Builtin_Impl_CallSitePrototypeGetMethodNameENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1482:
	.cfi_restore 6
	jmp	_ZN2v88internalL49Builtin_Impl_Stats_CallSitePrototypeGetMethodNameEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE20101:
	.size	_ZN2v88internal38Builtin_CallSitePrototypeGetMethodNameEiPmPNS0_7IsolateE, .-_ZN2v88internal38Builtin_CallSitePrototypeGetMethodNameEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal36Builtin_CallSitePrototypeGetPositionEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal36Builtin_CallSitePrototypeGetPositionEiPmPNS0_7IsolateE
	.type	_ZN2v88internal36Builtin_CallSitePrototypeGetPositionEiPmPNS0_7IsolateE, @function
_ZN2v88internal36Builtin_CallSitePrototypeGetPositionEiPmPNS0_7IsolateE:
.LFB20104:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1487
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL41Builtin_Impl_CallSitePrototypeGetPositionENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1487:
	.cfi_restore 6
	jmp	_ZN2v88internalL47Builtin_Impl_Stats_CallSitePrototypeGetPositionEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE20104:
	.size	_ZN2v88internal36Builtin_CallSitePrototypeGetPositionEiPmPNS0_7IsolateE, .-_ZN2v88internal36Builtin_CallSitePrototypeGetPositionEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal40Builtin_CallSitePrototypeGetPromiseIndexEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal40Builtin_CallSitePrototypeGetPromiseIndexEiPmPNS0_7IsolateE
	.type	_ZN2v88internal40Builtin_CallSitePrototypeGetPromiseIndexEiPmPNS0_7IsolateE, @function
_ZN2v88internal40Builtin_CallSitePrototypeGetPromiseIndexEiPmPNS0_7IsolateE:
.LFB20107:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1492
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL45Builtin_Impl_CallSitePrototypeGetPromiseIndexENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1492:
	.cfi_restore 6
	jmp	_ZN2v88internalL51Builtin_Impl_Stats_CallSitePrototypeGetPromiseIndexEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE20107:
	.size	_ZN2v88internal40Builtin_CallSitePrototypeGetPromiseIndexEiPmPNS0_7IsolateE, .-_ZN2v88internal40Builtin_CallSitePrototypeGetPromiseIndexEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal49Builtin_CallSitePrototypeGetScriptNameOrSourceURLEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal49Builtin_CallSitePrototypeGetScriptNameOrSourceURLEiPmPNS0_7IsolateE
	.type	_ZN2v88internal49Builtin_CallSitePrototypeGetScriptNameOrSourceURLEiPmPNS0_7IsolateE, @function
_ZN2v88internal49Builtin_CallSitePrototypeGetScriptNameOrSourceURLEiPmPNS0_7IsolateE:
.LFB20110:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1497
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL54Builtin_Impl_CallSitePrototypeGetScriptNameOrSourceURLENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1497:
	.cfi_restore 6
	jmp	_ZN2v88internalL60Builtin_Impl_Stats_CallSitePrototypeGetScriptNameOrSourceURLEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE20110:
	.size	_ZN2v88internal49Builtin_CallSitePrototypeGetScriptNameOrSourceURLEiPmPNS0_7IsolateE, .-_ZN2v88internal49Builtin_CallSitePrototypeGetScriptNameOrSourceURLEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal32Builtin_CallSitePrototypeGetThisEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal32Builtin_CallSitePrototypeGetThisEiPmPNS0_7IsolateE
	.type	_ZN2v88internal32Builtin_CallSitePrototypeGetThisEiPmPNS0_7IsolateE, @function
_ZN2v88internal32Builtin_CallSitePrototypeGetThisEiPmPNS0_7IsolateE:
.LFB20113:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1502
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL37Builtin_Impl_CallSitePrototypeGetThisENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1502:
	.cfi_restore 6
	jmp	_ZN2v88internalL43Builtin_Impl_Stats_CallSitePrototypeGetThisEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE20113:
	.size	_ZN2v88internal32Builtin_CallSitePrototypeGetThisEiPmPNS0_7IsolateE, .-_ZN2v88internal32Builtin_CallSitePrototypeGetThisEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal36Builtin_CallSitePrototypeGetTypeNameEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal36Builtin_CallSitePrototypeGetTypeNameEiPmPNS0_7IsolateE
	.type	_ZN2v88internal36Builtin_CallSitePrototypeGetTypeNameEiPmPNS0_7IsolateE, @function
_ZN2v88internal36Builtin_CallSitePrototypeGetTypeNameEiPmPNS0_7IsolateE:
.LFB20116:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1507
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL41Builtin_Impl_CallSitePrototypeGetTypeNameENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1507:
	.cfi_restore 6
	jmp	_ZN2v88internalL47Builtin_Impl_Stats_CallSitePrototypeGetTypeNameEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE20116:
	.size	_ZN2v88internal36Builtin_CallSitePrototypeGetTypeNameEiPmPNS0_7IsolateE, .-_ZN2v88internal36Builtin_CallSitePrototypeGetTypeNameEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal32Builtin_CallSitePrototypeIsAsyncEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal32Builtin_CallSitePrototypeIsAsyncEiPmPNS0_7IsolateE
	.type	_ZN2v88internal32Builtin_CallSitePrototypeIsAsyncEiPmPNS0_7IsolateE, @function
_ZN2v88internal32Builtin_CallSitePrototypeIsAsyncEiPmPNS0_7IsolateE:
.LFB20119:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1512
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL37Builtin_Impl_CallSitePrototypeIsAsyncENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1512:
	.cfi_restore 6
	jmp	_ZN2v88internalL43Builtin_Impl_Stats_CallSitePrototypeIsAsyncEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE20119:
	.size	_ZN2v88internal32Builtin_CallSitePrototypeIsAsyncEiPmPNS0_7IsolateE, .-_ZN2v88internal32Builtin_CallSitePrototypeIsAsyncEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal38Builtin_CallSitePrototypeIsConstructorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal38Builtin_CallSitePrototypeIsConstructorEiPmPNS0_7IsolateE
	.type	_ZN2v88internal38Builtin_CallSitePrototypeIsConstructorEiPmPNS0_7IsolateE, @function
_ZN2v88internal38Builtin_CallSitePrototypeIsConstructorEiPmPNS0_7IsolateE:
.LFB20122:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1517
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL43Builtin_Impl_CallSitePrototypeIsConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1517:
	.cfi_restore 6
	jmp	_ZN2v88internalL49Builtin_Impl_Stats_CallSitePrototypeIsConstructorEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE20122:
	.size	_ZN2v88internal38Builtin_CallSitePrototypeIsConstructorEiPmPNS0_7IsolateE, .-_ZN2v88internal38Builtin_CallSitePrototypeIsConstructorEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal31Builtin_CallSitePrototypeIsEvalEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal31Builtin_CallSitePrototypeIsEvalEiPmPNS0_7IsolateE
	.type	_ZN2v88internal31Builtin_CallSitePrototypeIsEvalEiPmPNS0_7IsolateE, @function
_ZN2v88internal31Builtin_CallSitePrototypeIsEvalEiPmPNS0_7IsolateE:
.LFB20125:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1522
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL36Builtin_Impl_CallSitePrototypeIsEvalENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1522:
	.cfi_restore 6
	jmp	_ZN2v88internalL42Builtin_Impl_Stats_CallSitePrototypeIsEvalEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE20125:
	.size	_ZN2v88internal31Builtin_CallSitePrototypeIsEvalEiPmPNS0_7IsolateE, .-_ZN2v88internal31Builtin_CallSitePrototypeIsEvalEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal33Builtin_CallSitePrototypeIsNativeEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal33Builtin_CallSitePrototypeIsNativeEiPmPNS0_7IsolateE
	.type	_ZN2v88internal33Builtin_CallSitePrototypeIsNativeEiPmPNS0_7IsolateE, @function
_ZN2v88internal33Builtin_CallSitePrototypeIsNativeEiPmPNS0_7IsolateE:
.LFB20128:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1527
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL38Builtin_Impl_CallSitePrototypeIsNativeENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1527:
	.cfi_restore 6
	jmp	_ZN2v88internalL44Builtin_Impl_Stats_CallSitePrototypeIsNativeEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE20128:
	.size	_ZN2v88internal33Builtin_CallSitePrototypeIsNativeEiPmPNS0_7IsolateE, .-_ZN2v88internal33Builtin_CallSitePrototypeIsNativeEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal37Builtin_CallSitePrototypeIsPromiseAllEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal37Builtin_CallSitePrototypeIsPromiseAllEiPmPNS0_7IsolateE
	.type	_ZN2v88internal37Builtin_CallSitePrototypeIsPromiseAllEiPmPNS0_7IsolateE, @function
_ZN2v88internal37Builtin_CallSitePrototypeIsPromiseAllEiPmPNS0_7IsolateE:
.LFB20131:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1532
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL42Builtin_Impl_CallSitePrototypeIsPromiseAllENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1532:
	.cfi_restore 6
	jmp	_ZN2v88internalL48Builtin_Impl_Stats_CallSitePrototypeIsPromiseAllEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE20131:
	.size	_ZN2v88internal37Builtin_CallSitePrototypeIsPromiseAllEiPmPNS0_7IsolateE, .-_ZN2v88internal37Builtin_CallSitePrototypeIsPromiseAllEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal35Builtin_CallSitePrototypeIsToplevelEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal35Builtin_CallSitePrototypeIsToplevelEiPmPNS0_7IsolateE
	.type	_ZN2v88internal35Builtin_CallSitePrototypeIsToplevelEiPmPNS0_7IsolateE, @function
_ZN2v88internal35Builtin_CallSitePrototypeIsToplevelEiPmPNS0_7IsolateE:
.LFB20134:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1537
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL40Builtin_Impl_CallSitePrototypeIsToplevelENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1537:
	.cfi_restore 6
	jmp	_ZN2v88internalL46Builtin_Impl_Stats_CallSitePrototypeIsToplevelEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE20134:
	.size	_ZN2v88internal35Builtin_CallSitePrototypeIsToplevelEiPmPNS0_7IsolateE, .-_ZN2v88internal35Builtin_CallSitePrototypeIsToplevelEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal33Builtin_CallSitePrototypeToStringEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal33Builtin_CallSitePrototypeToStringEiPmPNS0_7IsolateE
	.type	_ZN2v88internal33Builtin_CallSitePrototypeToStringEiPmPNS0_7IsolateE, @function
_ZN2v88internal33Builtin_CallSitePrototypeToStringEiPmPNS0_7IsolateE:
.LFB20137:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1542
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL38Builtin_Impl_CallSitePrototypeToStringENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1542:
	.cfi_restore 6
	jmp	_ZN2v88internalL44Builtin_Impl_Stats_CallSitePrototypeToStringEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE20137:
	.size	_ZN2v88internal33Builtin_CallSitePrototypeToStringEiPmPNS0_7IsolateE, .-_ZN2v88internal33Builtin_CallSitePrototypeToStringEiPmPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal40Builtin_CallSitePrototypeGetColumnNumberEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal40Builtin_CallSitePrototypeGetColumnNumberEiPmPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal40Builtin_CallSitePrototypeGetColumnNumberEiPmPNS0_7IsolateE:
.LFB25091:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE25091:
	.size	_GLOBAL__sub_I__ZN2v88internal40Builtin_CallSitePrototypeGetColumnNumberEiPmPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal40Builtin_CallSitePrototypeGetColumnNumberEiPmPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal40Builtin_CallSitePrototypeGetColumnNumberEiPmPNS0_7IsolateE
	.section	.bss._ZZN2v88internalL44Builtin_Impl_Stats_CallSitePrototypeToStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic204,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL44Builtin_Impl_Stats_CallSitePrototypeToStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic204, @object
	.size	_ZZN2v88internalL44Builtin_Impl_Stats_CallSitePrototypeToStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic204, 8
_ZZN2v88internalL44Builtin_Impl_Stats_CallSitePrototypeToStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic204:
	.zero	8
	.section	.bss._ZZN2v88internalL46Builtin_Impl_Stats_CallSitePrototypeIsToplevelEiPmPNS0_7IsolateEE28trace_event_unique_atomic196,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL46Builtin_Impl_Stats_CallSitePrototypeIsToplevelEiPmPNS0_7IsolateEE28trace_event_unique_atomic196, @object
	.size	_ZZN2v88internalL46Builtin_Impl_Stats_CallSitePrototypeIsToplevelEiPmPNS0_7IsolateEE28trace_event_unique_atomic196, 8
_ZZN2v88internalL46Builtin_Impl_Stats_CallSitePrototypeIsToplevelEiPmPNS0_7IsolateEE28trace_event_unique_atomic196:
	.zero	8
	.section	.bss._ZZN2v88internalL48Builtin_Impl_Stats_CallSitePrototypeIsPromiseAllEiPmPNS0_7IsolateEE28trace_event_unique_atomic188,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL48Builtin_Impl_Stats_CallSitePrototypeIsPromiseAllEiPmPNS0_7IsolateEE28trace_event_unique_atomic188, @object
	.size	_ZZN2v88internalL48Builtin_Impl_Stats_CallSitePrototypeIsPromiseAllEiPmPNS0_7IsolateEE28trace_event_unique_atomic188, 8
_ZZN2v88internalL48Builtin_Impl_Stats_CallSitePrototypeIsPromiseAllEiPmPNS0_7IsolateEE28trace_event_unique_atomic188:
	.zero	8
	.section	.bss._ZZN2v88internalL44Builtin_Impl_Stats_CallSitePrototypeIsNativeEiPmPNS0_7IsolateEE28trace_event_unique_atomic180,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL44Builtin_Impl_Stats_CallSitePrototypeIsNativeEiPmPNS0_7IsolateEE28trace_event_unique_atomic180, @object
	.size	_ZZN2v88internalL44Builtin_Impl_Stats_CallSitePrototypeIsNativeEiPmPNS0_7IsolateEE28trace_event_unique_atomic180, 8
_ZZN2v88internalL44Builtin_Impl_Stats_CallSitePrototypeIsNativeEiPmPNS0_7IsolateEE28trace_event_unique_atomic180:
	.zero	8
	.section	.bss._ZZN2v88internalL42Builtin_Impl_Stats_CallSitePrototypeIsEvalEiPmPNS0_7IsolateEE28trace_event_unique_atomic172,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL42Builtin_Impl_Stats_CallSitePrototypeIsEvalEiPmPNS0_7IsolateEE28trace_event_unique_atomic172, @object
	.size	_ZZN2v88internalL42Builtin_Impl_Stats_CallSitePrototypeIsEvalEiPmPNS0_7IsolateEE28trace_event_unique_atomic172, 8
_ZZN2v88internalL42Builtin_Impl_Stats_CallSitePrototypeIsEvalEiPmPNS0_7IsolateEE28trace_event_unique_atomic172:
	.zero	8
	.section	.bss._ZZN2v88internalL49Builtin_Impl_Stats_CallSitePrototypeIsConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic164,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL49Builtin_Impl_Stats_CallSitePrototypeIsConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic164, @object
	.size	_ZZN2v88internalL49Builtin_Impl_Stats_CallSitePrototypeIsConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic164, 8
_ZZN2v88internalL49Builtin_Impl_Stats_CallSitePrototypeIsConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic164:
	.zero	8
	.section	.bss._ZZN2v88internalL43Builtin_Impl_Stats_CallSitePrototypeIsAsyncEiPmPNS0_7IsolateEE28trace_event_unique_atomic156,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL43Builtin_Impl_Stats_CallSitePrototypeIsAsyncEiPmPNS0_7IsolateEE28trace_event_unique_atomic156, @object
	.size	_ZZN2v88internalL43Builtin_Impl_Stats_CallSitePrototypeIsAsyncEiPmPNS0_7IsolateEE28trace_event_unique_atomic156, 8
_ZZN2v88internalL43Builtin_Impl_Stats_CallSitePrototypeIsAsyncEiPmPNS0_7IsolateEE28trace_event_unique_atomic156:
	.zero	8
	.section	.bss._ZZN2v88internalL47Builtin_Impl_Stats_CallSitePrototypeGetTypeNameEiPmPNS0_7IsolateEE28trace_event_unique_atomic148,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL47Builtin_Impl_Stats_CallSitePrototypeGetTypeNameEiPmPNS0_7IsolateEE28trace_event_unique_atomic148, @object
	.size	_ZZN2v88internalL47Builtin_Impl_Stats_CallSitePrototypeGetTypeNameEiPmPNS0_7IsolateEE28trace_event_unique_atomic148, 8
_ZZN2v88internalL47Builtin_Impl_Stats_CallSitePrototypeGetTypeNameEiPmPNS0_7IsolateEE28trace_event_unique_atomic148:
	.zero	8
	.section	.bss._ZZN2v88internalL43Builtin_Impl_Stats_CallSitePrototypeGetThisEiPmPNS0_7IsolateEE28trace_event_unique_atomic134,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL43Builtin_Impl_Stats_CallSitePrototypeGetThisEiPmPNS0_7IsolateEE28trace_event_unique_atomic134, @object
	.size	_ZZN2v88internalL43Builtin_Impl_Stats_CallSitePrototypeGetThisEiPmPNS0_7IsolateEE28trace_event_unique_atomic134, 8
_ZZN2v88internalL43Builtin_Impl_Stats_CallSitePrototypeGetThisEiPmPNS0_7IsolateEE28trace_event_unique_atomic134:
	.zero	8
	.section	.bss._ZZN2v88internalL60Builtin_Impl_Stats_CallSitePrototypeGetScriptNameOrSourceURLEiPmPNS0_7IsolateEE28trace_event_unique_atomic126,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL60Builtin_Impl_Stats_CallSitePrototypeGetScriptNameOrSourceURLEiPmPNS0_7IsolateEE28trace_event_unique_atomic126, @object
	.size	_ZZN2v88internalL60Builtin_Impl_Stats_CallSitePrototypeGetScriptNameOrSourceURLEiPmPNS0_7IsolateEE28trace_event_unique_atomic126, 8
_ZZN2v88internalL60Builtin_Impl_Stats_CallSitePrototypeGetScriptNameOrSourceURLEiPmPNS0_7IsolateEE28trace_event_unique_atomic126:
	.zero	8
	.section	.bss._ZZN2v88internalL51Builtin_Impl_Stats_CallSitePrototypeGetPromiseIndexEiPmPNS0_7IsolateEE28trace_event_unique_atomic118,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL51Builtin_Impl_Stats_CallSitePrototypeGetPromiseIndexEiPmPNS0_7IsolateEE28trace_event_unique_atomic118, @object
	.size	_ZZN2v88internalL51Builtin_Impl_Stats_CallSitePrototypeGetPromiseIndexEiPmPNS0_7IsolateEE28trace_event_unique_atomic118, 8
_ZZN2v88internalL51Builtin_Impl_Stats_CallSitePrototypeGetPromiseIndexEiPmPNS0_7IsolateEE28trace_event_unique_atomic118:
	.zero	8
	.section	.bss._ZZN2v88internalL47Builtin_Impl_Stats_CallSitePrototypeGetPositionEiPmPNS0_7IsolateEE28trace_event_unique_atomic110,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL47Builtin_Impl_Stats_CallSitePrototypeGetPositionEiPmPNS0_7IsolateEE28trace_event_unique_atomic110, @object
	.size	_ZZN2v88internalL47Builtin_Impl_Stats_CallSitePrototypeGetPositionEiPmPNS0_7IsolateEE28trace_event_unique_atomic110, 8
_ZZN2v88internalL47Builtin_Impl_Stats_CallSitePrototypeGetPositionEiPmPNS0_7IsolateEE28trace_event_unique_atomic110:
	.zero	8
	.section	.bss._ZZN2v88internalL49Builtin_Impl_Stats_CallSitePrototypeGetMethodNameEiPmPNS0_7IsolateEE28trace_event_unique_atomic102,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL49Builtin_Impl_Stats_CallSitePrototypeGetMethodNameEiPmPNS0_7IsolateEE28trace_event_unique_atomic102, @object
	.size	_ZZN2v88internalL49Builtin_Impl_Stats_CallSitePrototypeGetMethodNameEiPmPNS0_7IsolateEE28trace_event_unique_atomic102, 8
_ZZN2v88internalL49Builtin_Impl_Stats_CallSitePrototypeGetMethodNameEiPmPNS0_7IsolateEE28trace_event_unique_atomic102:
	.zero	8
	.section	.bss._ZZN2v88internalL49Builtin_Impl_Stats_CallSitePrototypeGetLineNumberEiPmPNS0_7IsolateEE27trace_event_unique_atomic94,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL49Builtin_Impl_Stats_CallSitePrototypeGetLineNumberEiPmPNS0_7IsolateEE27trace_event_unique_atomic94, @object
	.size	_ZZN2v88internalL49Builtin_Impl_Stats_CallSitePrototypeGetLineNumberEiPmPNS0_7IsolateEE27trace_event_unique_atomic94, 8
_ZZN2v88internalL49Builtin_Impl_Stats_CallSitePrototypeGetLineNumberEiPmPNS0_7IsolateEE27trace_event_unique_atomic94:
	.zero	8
	.section	.bss._ZZN2v88internalL51Builtin_Impl_Stats_CallSitePrototypeGetFunctionNameEiPmPNS0_7IsolateEE27trace_event_unique_atomic86,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL51Builtin_Impl_Stats_CallSitePrototypeGetFunctionNameEiPmPNS0_7IsolateEE27trace_event_unique_atomic86, @object
	.size	_ZZN2v88internalL51Builtin_Impl_Stats_CallSitePrototypeGetFunctionNameEiPmPNS0_7IsolateEE27trace_event_unique_atomic86, 8
_ZZN2v88internalL51Builtin_Impl_Stats_CallSitePrototypeGetFunctionNameEiPmPNS0_7IsolateEE27trace_event_unique_atomic86:
	.zero	8
	.section	.bss._ZZN2v88internalL47Builtin_Impl_Stats_CallSitePrototypeGetFunctionEiPmPNS0_7IsolateEE27trace_event_unique_atomic72,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL47Builtin_Impl_Stats_CallSitePrototypeGetFunctionEiPmPNS0_7IsolateEE27trace_event_unique_atomic72, @object
	.size	_ZZN2v88internalL47Builtin_Impl_Stats_CallSitePrototypeGetFunctionEiPmPNS0_7IsolateEE27trace_event_unique_atomic72, 8
_ZZN2v88internalL47Builtin_Impl_Stats_CallSitePrototypeGetFunctionEiPmPNS0_7IsolateEE27trace_event_unique_atomic72:
	.zero	8
	.section	.bss._ZZN2v88internalL47Builtin_Impl_Stats_CallSitePrototypeGetFileNameEiPmPNS0_7IsolateEE27trace_event_unique_atomic64,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL47Builtin_Impl_Stats_CallSitePrototypeGetFileNameEiPmPNS0_7IsolateEE27trace_event_unique_atomic64, @object
	.size	_ZZN2v88internalL47Builtin_Impl_Stats_CallSitePrototypeGetFileNameEiPmPNS0_7IsolateEE27trace_event_unique_atomic64, 8
_ZZN2v88internalL47Builtin_Impl_Stats_CallSitePrototypeGetFileNameEiPmPNS0_7IsolateEE27trace_event_unique_atomic64:
	.zero	8
	.section	.bss._ZZN2v88internalL49Builtin_Impl_Stats_CallSitePrototypeGetEvalOriginEiPmPNS0_7IsolateEE27trace_event_unique_atomic56,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL49Builtin_Impl_Stats_CallSitePrototypeGetEvalOriginEiPmPNS0_7IsolateEE27trace_event_unique_atomic56, @object
	.size	_ZZN2v88internalL49Builtin_Impl_Stats_CallSitePrototypeGetEvalOriginEiPmPNS0_7IsolateEE27trace_event_unique_atomic56, 8
_ZZN2v88internalL49Builtin_Impl_Stats_CallSitePrototypeGetEvalOriginEiPmPNS0_7IsolateEE27trace_event_unique_atomic56:
	.zero	8
	.section	.bss._ZZN2v88internalL51Builtin_Impl_Stats_CallSitePrototypeGetColumnNumberEiPmPNS0_7IsolateEE27trace_event_unique_atomic48,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL51Builtin_Impl_Stats_CallSitePrototypeGetColumnNumberEiPmPNS0_7IsolateEE27trace_event_unique_atomic48, @object
	.size	_ZZN2v88internalL51Builtin_Impl_Stats_CallSitePrototypeGetColumnNumberEiPmPNS0_7IsolateEE27trace_event_unique_atomic48, 8
_ZZN2v88internalL51Builtin_Impl_Stats_CallSitePrototypeGetColumnNumberEiPmPNS0_7IsolateEE27trace_event_unique_atomic48:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
