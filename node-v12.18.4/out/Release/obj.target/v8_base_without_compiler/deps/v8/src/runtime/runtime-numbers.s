	.file	"runtime-numbers.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB5450:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE5450:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB5451:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5451:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB5453:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5453:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB8771:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L11
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L14
.L5:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L5
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE8771:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.rodata._ZN2v88internalL24Stats_Runtime_IsValidSmiEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"disabled-by-default-v8.runtime"
	.section	.rodata._ZN2v88internalL24Stats_Runtime_IsValidSmiEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC1:
	.string	"V8.Runtime_Runtime_IsValidSmi"
.LC2:
	.string	"args[0].IsNumber()"
.LC3:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internalL24Stats_Runtime_IsValidSmiEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL24Stats_Runtime_IsValidSmiEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL24Stats_Runtime_IsValidSmiEiPmPNS0_7IsolateE.isra.0:
.LFB25001:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L46
.L16:
	movq	_ZZN2v88internalL24Stats_Runtime_IsValidSmiEiPmPNS0_7IsolateEE27trace_event_unique_atomic16(%rip), %rbx
	testq	%rbx, %rbx
	je	.L47
.L18:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L48
.L20:
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L49
.L25:
	leaq	-144(%rbp), %rdi
	movq	112(%r12), %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L50
.L15:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L51
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$65, 11(%rax)
	je	.L25
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L48:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L52
.L21:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L22
	movq	(%rdi), %rax
	call	*8(%rax)
.L22:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L23
	movq	(%rdi), %rax
	call	*8(%rax)
.L23:
	leaq	.LC1(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L47:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L53
.L19:
	movq	%rbx, _ZZN2v88internalL24Stats_Runtime_IsValidSmiEiPmPNS0_7IsolateEE27trace_event_unique_atomic16(%rip)
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L46:
	movq	40960(%rsi), %rax
	movl	$399, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L50:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L53:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L52:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC1(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L21
.L51:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25001:
	.size	_ZN2v88internalL24Stats_Runtime_IsValidSmiEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL24Stats_Runtime_IsValidSmiEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL20Stats_Runtime_MaxSmiEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC4:
	.string	"V8.Runtime_Runtime_MaxSmi"
	.section	.text._ZN2v88internalL20Stats_Runtime_MaxSmiEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL20Stats_Runtime_MaxSmiEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL20Stats_Runtime_MaxSmiEiPmPNS0_7IsolateE.isra.0:
.LFB25002:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	$0, -64(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L83
.L55:
	movq	_ZZN2v88internalL20Stats_Runtime_MaxSmiEiPmPNS0_7IsolateEE27trace_event_unique_atomic81(%rip), %rbx
	testq	%rbx, %rbx
	je	.L84
	movq	$0, -128(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L85
.L59:
	leaq	-128(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-96(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L86
.L63:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L87
	movabsq	$9223372032559808512, %rax
	leaq	-16(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L88
.L58:
	movq	%rbx, _ZZN2v88internalL20Stats_Runtime_MaxSmiEiPmPNS0_7IsolateEE27trace_event_unique_atomic81(%rip)
	movq	$0, -128(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	je	.L59
.L85:
	pxor	%xmm0, %xmm0
	xorl	%r12d, %r12d
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L89
.L60:
	movq	-40(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L61
	movq	(%rdi), %rax
	call	*8(%rax)
.L61:
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L62
	movq	(%rdi), %rax
	call	*8(%rax)
.L62:
	leaq	.LC4(%rip), %rax
	movq	%rbx, -120(%rbp)
	movq	%rax, -112(%rbp)
	leaq	-120(%rbp), %rax
	movq	%r12, -104(%rbp)
	movq	%rax, -128(%rbp)
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L86:
	leaq	-88(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L83:
	movq	40960(%rdi), %rdi
	leaq	-88(%rbp), %rsi
	movl	$400, %edx
	addq	$23240, %rdi
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L89:
	subq	$8, %rsp
	leaq	-48(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC4(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r12
	addq	$64, %rsp
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L88:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L58
.L87:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25002:
	.size	_ZN2v88internalL20Stats_Runtime_MaxSmiEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL20Stats_Runtime_MaxSmiEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL19Stats_Runtime_IsSmiEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC5:
	.string	"V8.Runtime_Runtime_IsSmi"
	.section	.text._ZN2v88internalL19Stats_Runtime_IsSmiEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL19Stats_Runtime_IsSmiEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL19Stats_Runtime_IsSmiEiPmPNS0_7IsolateE.isra.0:
.LFB25003:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L121
.L91:
	movq	_ZZN2v88internalL19Stats_Runtime_IsSmiEiPmPNS0_7IsolateEE27trace_event_unique_atomic88(%rip), %rbx
	testq	%rbx, %rbx
	je	.L122
.L93:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L123
.L95:
	testb	$1, 0(%r13)
	jne	.L99
	movq	112(%r12), %r12
.L100:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L124
.L90:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L125
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L99:
	.cfi_restore_state
	movq	120(%r12), %r12
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L123:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L126
.L96:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L97
	movq	(%rdi), %rax
	call	*8(%rax)
.L97:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L98
	movq	(%rdi), %rax
	call	*8(%rax)
.L98:
	leaq	.LC5(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L122:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L127
.L94:
	movq	%rbx, _ZZN2v88internalL19Stats_Runtime_IsSmiEiPmPNS0_7IsolateEE27trace_event_unique_atomic88(%rip)
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L121:
	movq	40960(%rsi), %rax
	movl	$398, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L124:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L127:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L126:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC5(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L96
.L125:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25003:
	.size	_ZN2v88internalL19Stats_Runtime_IsSmiEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL19Stats_Runtime_IsSmiEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL28Stats_Runtime_StringToNumberEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"V8.Runtime_Runtime_StringToNumber"
	.section	.rodata._ZN2v88internalL28Stats_Runtime_StringToNumberEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC7:
	.string	"args[0].IsString()"
	.section	.text._ZN2v88internalL28Stats_Runtime_StringToNumberEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL28Stats_Runtime_StringToNumberEiPmPNS0_7IsolateE, @function
_ZN2v88internalL28Stats_Runtime_StringToNumberEiPmPNS0_7IsolateE:
.LFB20049:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L160
.L129:
	movq	_ZZN2v88internalL28Stats_Runtime_StringToNumberEiPmPNS0_7IsolateEE27trace_event_unique_atomic25(%rip), %rbx
	testq	%rbx, %rbx
	je	.L161
.L131:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L162
.L133:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L137
.L138:
	leaq	.LC7(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L161:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L163
.L132:
	movq	%rbx, _ZZN2v88internalL28Stats_Runtime_StringToNumberEiPmPNS0_7IsolateEE27trace_event_unique_atomic25(%rip)
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L137:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L138
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6String8ToNumberEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%rax), %r13
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L139
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L139:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L164
.L128:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L165
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L162:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L166
.L134:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L135
	movq	(%rdi), %rax
	call	*8(%rax)
.L135:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L136
	movq	(%rdi), %rax
	call	*8(%rax)
.L136:
	leaq	.LC6(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L160:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$404, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L164:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L163:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L166:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC6(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L134
.L165:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20049:
	.size	_ZN2v88internalL28Stats_Runtime_StringToNumberEiPmPNS0_7IsolateE, .-_ZN2v88internalL28Stats_Runtime_StringToNumberEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL30Stats_Runtime_StringParseFloatEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC8:
	.string	"V8.Runtime_Runtime_StringParseFloat"
	.section	.text._ZN2v88internalL30Stats_Runtime_StringParseFloatEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL30Stats_Runtime_StringParseFloatEiPmPNS0_7IsolateE, @function
_ZN2v88internalL30Stats_Runtime_StringParseFloatEiPmPNS0_7IsolateE:
.LFB20055:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L199
.L168:
	movq	_ZZN2v88internalL30Stats_Runtime_StringParseFloatEiPmPNS0_7IsolateEE27trace_event_unique_atomic62(%rip), %rbx
	testq	%rbx, %rbx
	je	.L200
.L170:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L201
.L172:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L176
.L177:
	leaq	.LC7(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L200:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L202
.L171:
	movq	%rbx, _ZZN2v88internalL30Stats_Runtime_StringParseFloatEiPmPNS0_7IsolateEE27trace_event_unique_atomic62(%rip)
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L176:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L177
	movsd	.LC9(%rip), %xmm0
	movq	%r13, %rsi
	movl	$16, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal14StringToDoubleEPNS0_7IsolateENS0_6HandleINS0_6StringEEEid@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	movq	(%rax), %r13
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L178
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L178:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L203
.L167:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L204
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L201:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L205
.L173:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L174
	movq	(%rdi), %rax
	call	*8(%rax)
.L174:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L175
	movq	(%rdi), %rax
	call	*8(%rax)
.L175:
	leaq	.LC8(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L199:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$402, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L203:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L202:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L205:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC8(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L173
.L204:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20055:
	.size	_ZN2v88internalL30Stats_Runtime_StringParseFloatEiPmPNS0_7IsolateE, .-_ZN2v88internalL30Stats_Runtime_StringParseFloatEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL29Stats_Runtime_GetHoleNaNUpperEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC10:
	.string	"V8.Runtime_Runtime_GetHoleNaNUpper"
	.section	.text._ZN2v88internalL29Stats_Runtime_GetHoleNaNUpperEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL29Stats_Runtime_GetHoleNaNUpperEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL29Stats_Runtime_GetHoleNaNUpperEiPmPNS0_7IsolateE.isra.0:
.LFB25007:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L237
.L207:
	movq	_ZZN2v88internalL29Stats_Runtime_GetHoleNaNUpperEiPmPNS0_7IsolateEE27trace_event_unique_atomic96(%rip), %rbx
	testq	%rbx, %rbx
	je	.L238
.L209:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L239
.L211:
	addl	$1, 41104(%r12)
	xorl	%edx, %edx
	movl	$-524289, %esi
	movq	%r12, %rdi
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	call	_ZN2v88internal7Factory17NewNumberFromUintEjNS0_14AllocationTypeE@PLT
	movq	(%rax), %r13
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L217
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L217:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L240
.L206:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L241
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L239:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L242
.L212:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L213
	movq	(%rdi), %rax
	call	*8(%rax)
.L213:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L214
	movq	(%rdi), %rax
	call	*8(%rax)
.L214:
	leaq	.LC10(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L238:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L243
.L210:
	movq	%rbx, _ZZN2v88internalL29Stats_Runtime_GetHoleNaNUpperEiPmPNS0_7IsolateEE27trace_event_unique_atomic96(%rip)
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L237:
	movq	40960(%rdi), %rax
	leaq	-104(%rbp), %rsi
	movl	$397, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L240:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L243:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L242:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC10(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L212
.L241:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25007:
	.size	_ZN2v88internalL29Stats_Runtime_GetHoleNaNUpperEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL29Stats_Runtime_GetHoleNaNUpperEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL29Stats_Runtime_GetHoleNaNLowerEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC11:
	.string	"V8.Runtime_Runtime_GetHoleNaNLower"
	.section	.text._ZN2v88internalL29Stats_Runtime_GetHoleNaNLowerEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL29Stats_Runtime_GetHoleNaNLowerEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL29Stats_Runtime_GetHoleNaNLowerEiPmPNS0_7IsolateE.isra.0:
.LFB25008:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L275
.L245:
	movq	_ZZN2v88internalL29Stats_Runtime_GetHoleNaNLowerEiPmPNS0_7IsolateEE28trace_event_unique_atomic103(%rip), %rbx
	testq	%rbx, %rbx
	je	.L276
.L247:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L277
.L249:
	addl	$1, 41104(%r12)
	xorl	%edx, %edx
	movl	$-524289, %esi
	movq	%r12, %rdi
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	call	_ZN2v88internal7Factory17NewNumberFromUintEjNS0_14AllocationTypeE@PLT
	movq	(%rax), %r13
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L255
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L255:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L278
.L244:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L279
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L277:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L280
.L250:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L251
	movq	(%rdi), %rax
	call	*8(%rax)
.L251:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L252
	movq	(%rdi), %rax
	call	*8(%rax)
.L252:
	leaq	.LC11(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L276:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L281
.L248:
	movq	%rbx, _ZZN2v88internalL29Stats_Runtime_GetHoleNaNLowerEiPmPNS0_7IsolateEE28trace_event_unique_atomic103(%rip)
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L275:
	movq	40960(%rdi), %rax
	leaq	-104(%rbp), %rsi
	movl	$396, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L245
	.p2align 4,,10
	.p2align 3
.L278:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L281:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L280:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC11(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L250
.L279:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25008:
	.size	_ZN2v88internalL29Stats_Runtime_GetHoleNaNLowerEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL29Stats_Runtime_GetHoleNaNLowerEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL28Stats_Runtime_StringParseIntEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC12:
	.string	"V8.Runtime_Runtime_StringParseInt"
	.section	.text._ZN2v88internalL28Stats_Runtime_StringParseIntEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL28Stats_Runtime_StringParseIntEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL28Stats_Runtime_StringParseIntEiPmPNS0_7IsolateE.isra.0:
.LFB25012:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L364
.L283:
	movq	_ZZN2v88internalL28Stats_Runtime_StringParseIntEiPmPNS0_7IsolateEE27trace_event_unique_atomic34(%rip), %rbx
	testq	%rbx, %rbx
	je	.L365
.L285:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L366
.L287:
	addl	$1, 41104(%r13)
	movq	(%r14), %rax
	leaq	-8(%r14), %r15
	movq	41088(%r13), %r12
	movq	41096(%r13), %rbx
	testb	$1, %al
	jne	.L291
.L294:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L317
.L293:
	movq	(%r14), %rax
	movq	-1(%rax), %rdx
	movq	%rax, %rsi
	cmpw	$63, 11(%rdx)
	jbe	.L367
.L297:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	jbe	.L368
.L305:
	movq	(%r15), %rax
	testb	$1, %al
	jne	.L369
.L312:
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
.L319:
	movsd	.LC14(%rip), %xmm2
	movapd	%xmm0, %xmm1
	andpd	.LC13(%rip), %xmm1
	ucomisd	%xmm1, %xmm2
	jb	.L320
	movsd	.LC15(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L320
	comisd	.LC16(%rip), %xmm0
	jnb	.L370
	.p2align 4,,10
	.p2align 3
.L320:
	movabsq	$9218868437227405312, %rax
	movq	%xmm0, %rdx
	testq	%rax, %rdx
	je	.L337
	movq	%xmm0, %rsi
	shrq	$52, %rsi
	andl	$2047, %esi
	movl	%esi, %ecx
	subl	$1075, %ecx
	js	.L371
	cmpl	$31, %ecx
	jg	.L337
	movabsq	$4503599627370495, %rax
	movabsq	$4503599627370496, %rsi
	andq	%rdx, %rax
	addq	%rsi, %rax
	salq	%cl, %rax
	movl	%eax, %eax
.L327:
	sarq	$63, %rdx
	orl	$1, %edx
	imull	%eax, %edx
	movl	%edx, %r8d
.L324:
	subl	$2, %edx
	cmpl	$34, %edx
	jbe	.L325
	testl	%r8d, %r8d
	je	.L325
	movq	1088(%r13), %r14
.L295:
	subl	$1, 41104(%r13)
	movq	%r12, 41088(%r13)
	cmpq	41096(%r13), %rbx
	je	.L331
	movq	%rbx, 41096(%r13)
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L331:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L372
.L282:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L373
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L367:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L297
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L300
	movq	23(%rax), %rdx
	movl	11(%rdx), %edx
	testl	%edx, %edx
	je	.L300
	movq	%r14, %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	movq	%rax, %r14
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L369:
	movq	-1(%rax), %rdx
	xorl	%ecx, %ecx
	cmpw	$65, 11(%rdx)
	jne	.L374
.L316:
	testb	%cl, %cl
	jne	.L312
	movsd	7(%rax), %xmm0
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L368:
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	jne	.L305
	movq	(%r14), %rax
	movq	41112(%r13), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L308
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
	movq	(%r15), %rax
	testb	$1, %al
	je	.L312
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L337:
	xorl	%r8d, %r8d
.L325:
	movq	%r14, %rsi
	movl	%r8d, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal11StringToIntEPNS0_7IsolateENS0_6HandleINS0_6StringEEEi@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	movq	(%rax), %r14
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L366:
	pxor	%xmm0, %xmm0
	xorl	%r12d, %r12d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L375
.L288:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L289
	movq	(%rdi), %rax
	call	*8(%rax)
.L289:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L290
	movq	(%rdi), %rax
	call	*8(%rax)
.L290:
	leaq	.LC12(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r12, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L365:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L376
.L286:
	movq	%rbx, _ZZN2v88internalL28Stats_Runtime_StringParseIntEiPmPNS0_7IsolateEE27trace_event_unique_atomic34(%rip)
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L291:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L294
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L371:
	cmpl	$-52, %ecx
	jl	.L337
	movabsq	$4503599627370495, %rax
	movabsq	$4503599627370496, %rcx
	andq	%rdx, %rax
	addq	%rcx, %rax
	movl	$1075, %ecx
	subl	%esi, %ecx
	shrq	%cl, %rax
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L370:
	cvttsd2sil	%xmm0, %r8d
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%r8d, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L320
	jne	.L320
	movl	%r8d, %edx
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L300:
	movq	41112(%r13), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L302
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r14
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L317:
	movq	312(%r13), %r14
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L374:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	je	.L316
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	testq	%rax, %rax
	je	.L317
	movq	(%rax), %rax
	movq	%rax, %rcx
	notq	%rcx
	andl	$1, %ecx
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L364:
	movq	40960(%rsi), %rax
	movl	$403, %edx
	leaq	-120(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L372:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L308:
	movq	41088(%r13), %r14
	cmpq	41096(%r13), %r14
	je	.L377
.L310:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r14)
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L376:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L286
	.p2align 4,,10
	.p2align 3
.L375:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC12(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r12
	addq	$64, %rsp
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L302:
	movq	41088(%r13), %r14
	cmpq	41096(%r13), %r14
	je	.L378
.L304:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r14)
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L377:
	movq	%r13, %rdi
	movq	%rsi, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L310
.L378:
	movq	%r13, %rdi
	movq	%rsi, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L304
.L373:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25012:
	.size	_ZN2v88internalL28Stats_Runtime_StringParseIntEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL28Stats_Runtime_StringParseIntEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL28Stats_Runtime_NumberToStringEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC17:
	.string	"V8.Runtime_Runtime_NumberToString"
	.section	.text._ZN2v88internalL28Stats_Runtime_NumberToStringEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL28Stats_Runtime_NumberToStringEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL28Stats_Runtime_NumberToStringEiPmPNS0_7IsolateE.isra.0:
.LFB25013:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L412
.L380:
	movq	_ZZN2v88internalL28Stats_Runtime_NumberToStringEiPmPNS0_7IsolateEE27trace_event_unique_atomic73(%rip), %rbx
	testq	%rbx, %rbx
	je	.L413
.L382:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L414
.L384:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L415
.L389:
	movq	%r13, %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory14NumberToStringENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	(%rax), %r13
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L392
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L392:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L416
.L379:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L417
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L415:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$65, 11(%rax)
	je	.L389
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L414:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L418
.L385:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L386
	movq	(%rdi), %rax
	call	*8(%rax)
.L386:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L387
	movq	(%rdi), %rax
	call	*8(%rax)
.L387:
	leaq	.LC17(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L384
	.p2align 4,,10
	.p2align 3
.L413:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L419
.L383:
	movq	%rbx, _ZZN2v88internalL28Stats_Runtime_NumberToStringEiPmPNS0_7IsolateEE27trace_event_unique_atomic73(%rip)
	jmp	.L382
	.p2align 4,,10
	.p2align 3
.L412:
	movq	40960(%rsi), %rax
	movl	$401, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L380
	.p2align 4,,10
	.p2align 3
.L416:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L379
	.p2align 4,,10
	.p2align 3
.L419:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L418:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC17(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L385
.L417:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25013:
	.size	_ZN2v88internalL28Stats_Runtime_NumberToStringEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL28Stats_Runtime_NumberToStringEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal18Runtime_IsValidSmiEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal18Runtime_IsValidSmiEiPmPNS0_7IsolateE
	.type	_ZN2v88internal18Runtime_IsValidSmiEiPmPNS0_7IsolateE, @function
_ZN2v88internal18Runtime_IsValidSmiEiPmPNS0_7IsolateE:
.LFB20047:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsi, %rdi
	testl	%eax, %eax
	jne	.L427
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L428
.L423:
	movq	112(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L428:
	movq	-1(%rax), %rax
	cmpw	$65, 11(%rax)
	je	.L423
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L427:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movq	%rdx, %rsi
	jmp	_ZN2v88internalL24Stats_Runtime_IsValidSmiEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE20047:
	.size	_ZN2v88internal18Runtime_IsValidSmiEiPmPNS0_7IsolateE, .-_ZN2v88internal18Runtime_IsValidSmiEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal22Runtime_StringToNumberEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal22Runtime_StringToNumberEiPmPNS0_7IsolateE
	.type	_ZN2v88internal22Runtime_StringToNumberEiPmPNS0_7IsolateE, @function
_ZN2v88internal22Runtime_StringToNumberEiPmPNS0_7IsolateE:
.LFB20050:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L435
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L431
.L432:
	leaq	.LC7(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L431:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L432
	movq	%rdx, %rdi
	call	_ZN2v88internal6String8ToNumberEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%rax), %r14
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L429
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L429:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L435:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL28Stats_Runtime_StringToNumberEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE20050:
	.size	_ZN2v88internal22Runtime_StringToNumberEiPmPNS0_7IsolateE, .-_ZN2v88internal22Runtime_StringToNumberEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal22Runtime_StringParseIntEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal22Runtime_StringParseIntEiPmPNS0_7IsolateE
	.type	_ZN2v88internal22Runtime_StringParseIntEiPmPNS0_7IsolateE, @function
_ZN2v88internal22Runtime_StringParseIntEiPmPNS0_7IsolateE:
.LFB20053:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L492
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	leaq	-8(%rsi), %r15
	movq	41088(%rdx), %r12
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L438
.L441:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L464
.L440:
	movq	(%r14), %rax
	movq	-1(%rax), %rdx
	movq	%rax, %rsi
	cmpw	$63, 11(%rdx)
	jbe	.L493
.L444:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	jbe	.L494
.L452:
	movq	(%r15), %rax
	testb	$1, %al
	jne	.L495
.L459:
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
.L466:
	movsd	.LC14(%rip), %xmm2
	movapd	%xmm0, %xmm1
	andpd	.LC13(%rip), %xmm1
	ucomisd	%xmm1, %xmm2
	jb	.L467
	movsd	.LC15(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jnb	.L496
.L467:
	movabsq	$9218868437227405312, %rax
	movq	%xmm0, %rdx
	testq	%rax, %rdx
	je	.L480
	movq	%xmm0, %rsi
	shrq	$52, %rsi
	andl	$2047, %esi
	movl	%esi, %ecx
	subl	$1075, %ecx
	js	.L497
	cmpl	$31, %ecx
	jg	.L480
	movabsq	$4503599627370495, %rax
	movabsq	$4503599627370496, %rsi
	andq	%rdx, %rax
	addq	%rsi, %rax
	salq	%cl, %rax
	movl	%eax, %eax
.L474:
	sarq	$63, %rdx
	orl	$1, %edx
	imull	%eax, %edx
	movl	%edx, %r8d
.L471:
	subl	$2, %edx
	cmpl	$34, %edx
	jbe	.L472
	testl	%r8d, %r8d
	je	.L472
	movq	1088(%r13), %r14
.L442:
	subl	$1, 41104(%r13)
	movq	%r12, 41088(%r13)
	cmpq	41096(%r13), %rbx
	je	.L436
	movq	%rbx, 41096(%r13)
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L436:
	addq	$24, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L493:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L444
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L447
	movq	23(%rax), %rdx
	movl	11(%rdx), %edx
	testl	%edx, %edx
	je	.L447
	movq	%r14, %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	movq	%rax, %r14
	jmp	.L452
	.p2align 4,,10
	.p2align 3
.L495:
	movq	-1(%rax), %rdx
	xorl	%ecx, %ecx
	cmpw	$65, 11(%rdx)
	jne	.L498
.L463:
	testb	%cl, %cl
	jne	.L459
	movsd	7(%rax), %xmm0
	jmp	.L466
	.p2align 4,,10
	.p2align 3
.L494:
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	jne	.L452
	movq	(%r14), %rax
	movq	41112(%r13), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L455
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
	jmp	.L452
	.p2align 4,,10
	.p2align 3
.L497:
	cmpl	$-52, %ecx
	jge	.L499
.L480:
	xorl	%r8d, %r8d
.L472:
	movq	%r14, %rsi
	movl	%r8d, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal11StringToIntEPNS0_7IsolateENS0_6HandleINS0_6StringEEEi@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	movq	(%rax), %r14
	jmp	.L442
	.p2align 4,,10
	.p2align 3
.L438:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L441
	jmp	.L440
	.p2align 4,,10
	.p2align 3
.L499:
	movabsq	$4503599627370495, %rax
	movabsq	$4503599627370496, %rcx
	andq	%rdx, %rax
	addq	%rcx, %rax
	movl	$1075, %ecx
	subl	%esi, %ecx
	shrq	%cl, %rax
	jmp	.L474
	.p2align 4,,10
	.p2align 3
.L496:
	comisd	.LC16(%rip), %xmm0
	jb	.L467
	cvttsd2sil	%xmm0, %r8d
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%r8d, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L467
	jne	.L467
	movl	%r8d, %edx
	jmp	.L471
	.p2align 4,,10
	.p2align 3
.L447:
	movq	41112(%r13), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L449
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r14
	jmp	.L444
	.p2align 4,,10
	.p2align 3
.L464:
	movq	312(%r13), %r14
	jmp	.L442
	.p2align 4,,10
	.p2align 3
.L498:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	je	.L463
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	testq	%rax, %rax
	je	.L464
	movq	(%rax), %rax
	movq	%rax, %rcx
	notq	%rcx
	andl	$1, %ecx
	jmp	.L463
	.p2align 4,,10
	.p2align 3
.L492:
	addq	$24, %rsp
	movq	%r14, %rdi
	movq	%rdx, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL28Stats_Runtime_StringParseIntEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L455:
	.cfi_restore_state
	movq	41088(%r13), %r14
	cmpq	41096(%r13), %r14
	je	.L500
.L457:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r14)
	jmp	.L452
	.p2align 4,,10
	.p2align 3
.L449:
	movq	41088(%r13), %r14
	cmpq	41096(%r13), %r14
	je	.L501
.L451:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r14)
	jmp	.L444
	.p2align 4,,10
	.p2align 3
.L500:
	movq	%r13, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L457
.L501:
	movq	%r13, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L451
	.cfi_endproc
.LFE20053:
	.size	_ZN2v88internal22Runtime_StringParseIntEiPmPNS0_7IsolateE, .-_ZN2v88internal22Runtime_StringParseIntEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal24Runtime_StringParseFloatEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal24Runtime_StringParseFloatEiPmPNS0_7IsolateE
	.type	_ZN2v88internal24Runtime_StringParseFloatEiPmPNS0_7IsolateE, @function
_ZN2v88internal24Runtime_StringParseFloatEiPmPNS0_7IsolateE:
.LFB20056:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L508
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L504
.L505:
	leaq	.LC7(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L504:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L505
	movsd	.LC9(%rip), %xmm0
	movl	$16, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal14StringToDoubleEPNS0_7IsolateENS0_6HandleINS0_6StringEEEid@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	movq	(%rax), %r14
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L502
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L502:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L508:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL30Stats_Runtime_StringParseFloatEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE20056:
	.size	_ZN2v88internal24Runtime_StringParseFloatEiPmPNS0_7IsolateE, .-_ZN2v88internal24Runtime_StringParseFloatEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal22Runtime_NumberToStringEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal22Runtime_NumberToStringEiPmPNS0_7IsolateE
	.type	_ZN2v88internal22Runtime_NumberToStringEiPmPNS0_7IsolateE, @function
_ZN2v88internal22Runtime_NumberToStringEiPmPNS0_7IsolateE:
.LFB20059:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L516
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L517
.L512:
	movl	$1, %edx
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory14NumberToStringENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	(%rax), %r14
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L509
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L509:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L517:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$65, 11(%rax)
	je	.L512
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L516:
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	movq	%r8, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL28Stats_Runtime_NumberToStringEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE20059:
	.size	_ZN2v88internal22Runtime_NumberToStringEiPmPNS0_7IsolateE, .-_ZN2v88internal22Runtime_NumberToStringEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal14Runtime_MaxSmiEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal14Runtime_MaxSmiEiPmPNS0_7IsolateE
	.type	_ZN2v88internal14Runtime_MaxSmiEiPmPNS0_7IsolateE, @function
_ZN2v88internal14Runtime_MaxSmiEiPmPNS0_7IsolateE:
.LFB20062:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L520
	movabsq	$9223372032559808512, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L520:
	movq	%rdx, %rdi
	jmp	_ZN2v88internalL20Stats_Runtime_MaxSmiEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE20062:
	.size	_ZN2v88internal14Runtime_MaxSmiEiPmPNS0_7IsolateE, .-_ZN2v88internal14Runtime_MaxSmiEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal13Runtime_IsSmiEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal13Runtime_IsSmiEiPmPNS0_7IsolateE
	.type	_ZN2v88internal13Runtime_IsSmiEiPmPNS0_7IsolateE, @function
_ZN2v88internal13Runtime_IsSmiEiPmPNS0_7IsolateE:
.LFB20065:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsi, %rdi
	testl	%eax, %eax
	jne	.L525
	testb	$1, (%rsi)
	jne	.L523
	movq	112(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L523:
	movq	120(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L525:
	movq	%rdx, %rsi
	jmp	_ZN2v88internalL19Stats_Runtime_IsSmiEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE20065:
	.size	_ZN2v88internal13Runtime_IsSmiEiPmPNS0_7IsolateE, .-_ZN2v88internal13Runtime_IsSmiEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal23Runtime_GetHoleNaNUpperEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal23Runtime_GetHoleNaNUpperEiPmPNS0_7IsolateE
	.type	_ZN2v88internal23Runtime_GetHoleNaNUpperEiPmPNS0_7IsolateE, @function
_ZN2v88internal23Runtime_GetHoleNaNUpperEiPmPNS0_7IsolateE:
.LFB20068:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L530
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r14
	movl	$-524289, %esi
	movq	%r12, %rdi
	movq	41096(%rdx), %rbx
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory17NewNumberFromUintEjNS0_14AllocationTypeE@PLT
	movq	(%rax), %r13
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L526
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L526:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L530:
	.cfi_restore_state
	popq	%rbx
	movq	%rdx, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL29Stats_Runtime_GetHoleNaNUpperEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE20068:
	.size	_ZN2v88internal23Runtime_GetHoleNaNUpperEiPmPNS0_7IsolateE, .-_ZN2v88internal23Runtime_GetHoleNaNUpperEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal23Runtime_GetHoleNaNLowerEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal23Runtime_GetHoleNaNLowerEiPmPNS0_7IsolateE
	.type	_ZN2v88internal23Runtime_GetHoleNaNLowerEiPmPNS0_7IsolateE, @function
_ZN2v88internal23Runtime_GetHoleNaNLowerEiPmPNS0_7IsolateE:
.LFB20071:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L535
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r14
	movl	$-524289, %esi
	movq	%r12, %rdi
	movq	41096(%rdx), %rbx
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory17NewNumberFromUintEjNS0_14AllocationTypeE@PLT
	movq	(%rax), %r13
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L531
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L531:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L535:
	.cfi_restore_state
	popq	%rbx
	movq	%rdx, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL29Stats_Runtime_GetHoleNaNLowerEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE20071:
	.size	_ZN2v88internal23Runtime_GetHoleNaNLowerEiPmPNS0_7IsolateE, .-_ZN2v88internal23Runtime_GetHoleNaNLowerEiPmPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal18Runtime_IsValidSmiEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal18Runtime_IsValidSmiEiPmPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal18Runtime_IsValidSmiEiPmPNS0_7IsolateE:
.LFB24991:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE24991:
	.size	_GLOBAL__sub_I__ZN2v88internal18Runtime_IsValidSmiEiPmPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal18Runtime_IsValidSmiEiPmPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal18Runtime_IsValidSmiEiPmPNS0_7IsolateE
	.section	.bss._ZZN2v88internalL29Stats_Runtime_GetHoleNaNLowerEiPmPNS0_7IsolateEE28trace_event_unique_atomic103,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL29Stats_Runtime_GetHoleNaNLowerEiPmPNS0_7IsolateEE28trace_event_unique_atomic103, @object
	.size	_ZZN2v88internalL29Stats_Runtime_GetHoleNaNLowerEiPmPNS0_7IsolateEE28trace_event_unique_atomic103, 8
_ZZN2v88internalL29Stats_Runtime_GetHoleNaNLowerEiPmPNS0_7IsolateEE28trace_event_unique_atomic103:
	.zero	8
	.section	.bss._ZZN2v88internalL29Stats_Runtime_GetHoleNaNUpperEiPmPNS0_7IsolateEE27trace_event_unique_atomic96,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL29Stats_Runtime_GetHoleNaNUpperEiPmPNS0_7IsolateEE27trace_event_unique_atomic96, @object
	.size	_ZZN2v88internalL29Stats_Runtime_GetHoleNaNUpperEiPmPNS0_7IsolateEE27trace_event_unique_atomic96, 8
_ZZN2v88internalL29Stats_Runtime_GetHoleNaNUpperEiPmPNS0_7IsolateEE27trace_event_unique_atomic96:
	.zero	8
	.section	.bss._ZZN2v88internalL19Stats_Runtime_IsSmiEiPmPNS0_7IsolateEE27trace_event_unique_atomic88,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL19Stats_Runtime_IsSmiEiPmPNS0_7IsolateEE27trace_event_unique_atomic88, @object
	.size	_ZZN2v88internalL19Stats_Runtime_IsSmiEiPmPNS0_7IsolateEE27trace_event_unique_atomic88, 8
_ZZN2v88internalL19Stats_Runtime_IsSmiEiPmPNS0_7IsolateEE27trace_event_unique_atomic88:
	.zero	8
	.section	.bss._ZZN2v88internalL20Stats_Runtime_MaxSmiEiPmPNS0_7IsolateEE27trace_event_unique_atomic81,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL20Stats_Runtime_MaxSmiEiPmPNS0_7IsolateEE27trace_event_unique_atomic81, @object
	.size	_ZZN2v88internalL20Stats_Runtime_MaxSmiEiPmPNS0_7IsolateEE27trace_event_unique_atomic81, 8
_ZZN2v88internalL20Stats_Runtime_MaxSmiEiPmPNS0_7IsolateEE27trace_event_unique_atomic81:
	.zero	8
	.section	.bss._ZZN2v88internalL28Stats_Runtime_NumberToStringEiPmPNS0_7IsolateEE27trace_event_unique_atomic73,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL28Stats_Runtime_NumberToStringEiPmPNS0_7IsolateEE27trace_event_unique_atomic73, @object
	.size	_ZZN2v88internalL28Stats_Runtime_NumberToStringEiPmPNS0_7IsolateEE27trace_event_unique_atomic73, 8
_ZZN2v88internalL28Stats_Runtime_NumberToStringEiPmPNS0_7IsolateEE27trace_event_unique_atomic73:
	.zero	8
	.section	.bss._ZZN2v88internalL30Stats_Runtime_StringParseFloatEiPmPNS0_7IsolateEE27trace_event_unique_atomic62,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL30Stats_Runtime_StringParseFloatEiPmPNS0_7IsolateEE27trace_event_unique_atomic62, @object
	.size	_ZZN2v88internalL30Stats_Runtime_StringParseFloatEiPmPNS0_7IsolateEE27trace_event_unique_atomic62, 8
_ZZN2v88internalL30Stats_Runtime_StringParseFloatEiPmPNS0_7IsolateEE27trace_event_unique_atomic62:
	.zero	8
	.section	.bss._ZZN2v88internalL28Stats_Runtime_StringParseIntEiPmPNS0_7IsolateEE27trace_event_unique_atomic34,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL28Stats_Runtime_StringParseIntEiPmPNS0_7IsolateEE27trace_event_unique_atomic34, @object
	.size	_ZZN2v88internalL28Stats_Runtime_StringParseIntEiPmPNS0_7IsolateEE27trace_event_unique_atomic34, 8
_ZZN2v88internalL28Stats_Runtime_StringParseIntEiPmPNS0_7IsolateEE27trace_event_unique_atomic34:
	.zero	8
	.section	.bss._ZZN2v88internalL28Stats_Runtime_StringToNumberEiPmPNS0_7IsolateEE27trace_event_unique_atomic25,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL28Stats_Runtime_StringToNumberEiPmPNS0_7IsolateEE27trace_event_unique_atomic25, @object
	.size	_ZZN2v88internalL28Stats_Runtime_StringToNumberEiPmPNS0_7IsolateEE27trace_event_unique_atomic25, 8
_ZZN2v88internalL28Stats_Runtime_StringToNumberEiPmPNS0_7IsolateEE27trace_event_unique_atomic25:
	.zero	8
	.section	.bss._ZZN2v88internalL24Stats_Runtime_IsValidSmiEiPmPNS0_7IsolateEE27trace_event_unique_atomic16,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL24Stats_Runtime_IsValidSmiEiPmPNS0_7IsolateEE27trace_event_unique_atomic16, @object
	.size	_ZZN2v88internalL24Stats_Runtime_IsValidSmiEiPmPNS0_7IsolateEE27trace_event_unique_atomic16, 8
_ZZN2v88internalL24Stats_Runtime_IsValidSmiEiPmPNS0_7IsolateEE27trace_event_unique_atomic16:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC9:
	.long	0
	.long	2146959360
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC13:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC14:
	.long	4294967295
	.long	2146435071
	.align 8
.LC15:
	.long	4290772992
	.long	1105199103
	.align 8
.LC16:
	.long	0
	.long	-1042284544
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
