	.file	"module.cc"
	.text
	.section	.text._ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPN2v88internal6HandleINS3_6StringEEESt6vectorIS6_NS3_13ZoneAllocatorIS6_EEEEElS6_NS0_5__ops15_Iter_comp_iterIZNS3_6Module18GetModuleNamespaceEPNS3_7IsolateENS4_ISF_EEEUlS6_S6_E_EEEvT_T0_SM_T1_T2_,"ax",@progbits
	.p2align 4
	.type	_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPN2v88internal6HandleINS3_6StringEEESt6vectorIS6_NS3_13ZoneAllocatorIS6_EEEEElS6_NS0_5__ops15_Iter_comp_iterIZNS3_6Module18GetModuleNamespaceEPNS3_7IsolateENS4_ISF_EEEUlS6_S6_E_EEEvT_T0_SM_T1_T2_, @function
_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPN2v88internal6HandleINS3_6StringEEESt6vectorIS6_NS3_13ZoneAllocatorIS6_EEEEElS6_NS0_5__ops15_Iter_comp_iterIZNS3_6Module18GetModuleNamespaceEPNS3_7IsolateENS4_ISF_EEEUlS6_S6_E_EEEvT_T0_SM_T1_T2_:
.LFB23144:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	-1(%rdx), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rax, %r14
	pushq	%r13
	shrq	$63, %r14
	pushq	%r12
	addq	%rax, %r14
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	movq	%r14, %rax
	sarq	%rax
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%rcx, -88(%rbp)
	movq	%rdx, %rcx
	andl	$1, %ecx
	movq	%rsi, -64(%rbp)
	movq	%rdx, -104(%rbp)
	movq	%rax, -80(%rbp)
	movq	%rcx, -96(%rbp)
	cmpq	%rax, %rsi
	jge	.L2
	movq	%r8, -72(%rbp)
	movq	%rsi, %r12
	.p2align 4,,10
	.p2align 3
.L6:
	leaq	1(%r12), %rax
	leaq	(%rax,%rax), %r13
	salq	$4, %rax
	leaq	(%r15,%rax), %rbx
	leaq	-1(%r13), %r14
	movq	-72(%rbp), %rax
	leaq	(%r15,%r14,8), %r11
	movq	(%rbx), %rsi
	movq	(%r11), %rdx
	movq	(%rax), %rdi
	movq	%r11, -56(%rbp)
	call	_ZN2v88internal6String7CompareEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	movq	-56(%rbp), %r11
	testl	%eax, %eax
	je	.L3
	movq	(%rbx), %rax
	movq	%rax, (%r15,%r12,8)
	cmpq	-80(%rbp), %r13
	jge	.L14
	movq	%r13, %r12
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L3:
	movq	(%r11), %rax
	movq	%rax, (%r15,%r12,8)
	cmpq	-80(%rbp), %r14
	jge	.L11
	movq	%r14, %r12
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L14:
	movq	-72(%rbp), %r12
.L4:
	cmpq	$0, -96(%rbp)
	jne	.L7
.L10:
	movq	-104(%rbp), %rdx
	subq	$2, %rdx
	movq	%rdx, %rax
	shrq	$63, %rax
	addq	%rdx, %rax
	sarq	%rax
	cmpq	%r13, %rax
	jne	.L7
	leaq	1(%r13,%r13), %r13
	leaq	(%r15,%r13,8), %rax
	movq	(%rax), %rdx
	movq	%rdx, (%rbx)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L7:
	leaq	-1(%r13), %rax
	movq	%rax, %r14
	shrq	$63, %r14
	addq	%rax, %r14
	sarq	%r14
	cmpq	-64(%rbp), %r13
	jle	.L8
	movq	%r12, -56(%rbp)
	movq	-88(%rbp), %rbx
	movq	%r15, %r12
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L20:
	movq	(%r15), %rax
	movq	%r14, %r13
	movq	%rax, (%rdx)
	leaq	-1(%r14), %rdx
	movq	%rdx, %rax
	shrq	$63, %rax
	addq	%rdx, %rax
	sarq	%rax
	cmpq	%r14, -64(%rbp)
	jge	.L19
	movq	%rax, %r14
.L9:
	movq	-56(%rbp), %rax
	leaq	(%r12,%r14,8), %r15
	movq	%rbx, %rdx
	movq	(%r15), %rsi
	movq	(%rax), %rdi
	call	_ZN2v88internal6String7CompareEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	leaq	(%r12,%r13,8), %rdx
	testl	%eax, %eax
	je	.L20
	movq	%rdx, %rbx
.L8:
	movq	-88(%rbp), %rax
	movq	%rax, (%rbx)
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	movq	%r15, %rbx
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L11:
	movq	-72(%rbp), %r12
	movq	%r11, %rbx
	movq	%r14, %r13
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L2:
	cmpq	$0, -96(%rbp)
	movq	%rsi, %r13
	leaq	(%rdi,%rsi,8), %rbx
	jne	.L8
	jmp	.L10
	.cfi_endproc
.LFE23144:
	.size	_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPN2v88internal6HandleINS3_6StringEEESt6vectorIS6_NS3_13ZoneAllocatorIS6_EEEEElS6_NS0_5__ops15_Iter_comp_iterIZNS3_6Module18GetModuleNamespaceEPNS3_7IsolateENS4_ISF_EEEUlS6_S6_E_EEEvT_T0_SM_T1_T2_, .-_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPN2v88internal6HandleINS3_6StringEEESt6vectorIS6_NS3_13ZoneAllocatorIS6_EEEEElS6_NS0_5__ops15_Iter_comp_iterIZNS3_6Module18GetModuleNamespaceEPNS3_7IsolateENS4_ISF_EEEUlS6_S6_E_EEEvT_T0_SM_T1_T2_
	.section	.text._ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPN2v88internal6HandleINS3_6StringEEESt6vectorIS6_NS3_13ZoneAllocatorIS6_EEEEElNS0_5__ops15_Iter_comp_iterIZNS3_6Module18GetModuleNamespaceEPNS3_7IsolateENS4_ISF_EEEUlS6_S6_E_EEEvT_SL_T0_T1_,"ax",@progbits
	.p2align 4
	.type	_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPN2v88internal6HandleINS3_6StringEEESt6vectorIS6_NS3_13ZoneAllocatorIS6_EEEEElNS0_5__ops15_Iter_comp_iterIZNS3_6Module18GetModuleNamespaceEPNS3_7IsolateENS4_ISF_EEEUlS6_S6_E_EEEvT_SL_T0_T1_, @function
_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPN2v88internal6HandleINS3_6StringEEESt6vectorIS6_NS3_13ZoneAllocatorIS6_EEEEElNS0_5__ops15_Iter_comp_iterIZNS3_6Module18GetModuleNamespaceEPNS3_7IsolateENS4_ISF_EEEUlS6_S6_E_EEEvT_SL_T0_T1_:
.LFB22553:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	subq	%rdi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -56(%rbp)
	cmpq	$128, %rax
	jle	.L21
	movq	%rdi, %r15
	movq	%rcx, %rbx
	testq	%rdx, %rdx
	je	.L55
	leaq	8(%rdi), %rax
	movq	%rsi, %r13
	movq	%rax, -80(%rbp)
.L25:
	movq	%r13, %rax
	movq	8(%r15), %rsi
	movq	(%rbx), %rdi
	subq	%r15, %rax
	subq	$1, -56(%rbp)
	movq	%rax, %rdx
	shrq	$63, %rax
	sarq	$3, %rdx
	addq	%rdx, %rax
	sarq	%rax
	leaq	(%r15,%rax,8), %r12
	movq	(%r12), %rdx
	call	_ZN2v88internal6String7CompareEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	testl	%eax, %eax
	jne	.L30
	movq	(%r12), %rsi
	movq	-8(%r13), %rdx
	movq	(%rbx), %rdi
	call	_ZN2v88internal6String7CompareEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	testl	%eax, %eax
	jne	.L31
	movq	(%r15), %rax
	movq	(%r12), %rdx
	movq	%rdx, (%r15)
	movq	%rax, (%r12)
.L32:
	movq	-80(%rbp), %r12
	movq	%r13, %r14
	.p2align 4,,10
	.p2align 3
.L36:
	movq	(%r12), %rsi
	movq	(%r15), %rdx
	movq	%r12, -72(%rbp)
	movq	(%rbx), %rdi
	call	_ZN2v88internal6String7CompareEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	testl	%eax, %eax
	je	.L37
	leaq	-8(%r14), %r10
	.p2align 4,,10
	.p2align 3
.L38:
	movq	(%r10), %rdx
	movq	(%r15), %rsi
	movq	%r10, -64(%rbp)
	movq	%r10, %r14
	movq	(%rbx), %rdi
	call	_ZN2v88internal6String7CompareEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	movq	-64(%rbp), %r10
	subq	$8, %r10
	testl	%eax, %eax
	je	.L38
	cmpq	%r14, %r12
	jnb	.L56
	movq	(%r12), %rax
	movq	(%r14), %rdx
	movq	%rdx, (%r12)
	movq	%rax, (%r14)
.L37:
	addq	$8, %r12
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L56:
	movq	-56(%rbp), %rdx
	movq	%rbx, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPN2v88internal6HandleINS3_6StringEEESt6vectorIS6_NS3_13ZoneAllocatorIS6_EEEEElNS0_5__ops15_Iter_comp_iterIZNS3_6Module18GetModuleNamespaceEPNS3_7IsolateENS4_ISF_EEEUlS6_S6_E_EEEvT_SL_T0_T1_
	movq	%r12, %rax
	subq	%r15, %rax
	cmpq	$128, %rax
	jle	.L21
	cmpq	$0, -56(%rbp)
	je	.L23
	movq	%r12, %r13
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L30:
	movq	8(%r15), %rsi
	movq	-8(%r13), %rdx
	movq	(%rbx), %rdi
	call	_ZN2v88internal6String7CompareEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	testl	%eax, %eax
	jne	.L34
	movq	(%r15), %rax
.L54:
	movq	8(%r15), %rdx
	movq	%rax, 8(%r15)
	movq	%rdx, (%r15)
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L55:
	movq	%rsi, -72(%rbp)
.L23:
	sarq	$3, %rax
	leaq	-2(%rax), %r14
	movq	%rax, %r13
	sarq	%r14
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L26:
	subq	$1, %r14
.L28:
	movq	(%r15,%r14,8), %rcx
	movq	%rbx, %r8
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPN2v88internal6HandleINS3_6StringEEESt6vectorIS6_NS3_13ZoneAllocatorIS6_EEEEElS6_NS0_5__ops15_Iter_comp_iterIZNS3_6Module18GetModuleNamespaceEPNS3_7IsolateENS4_ISF_EEEUlS6_S6_E_EEEvT_T0_SM_T1_T2_
	testq	%r14, %r14
	jne	.L26
	movq	-72(%rbp), %r12
	subq	$8, %r12
	.p2align 4,,10
	.p2align 3
.L27:
	movq	(%r15), %rax
	movq	%r12, %r13
	movq	(%r12), %rcx
	movq	%rbx, %r8
	subq	%r15, %r13
	xorl	%esi, %esi
	movq	%r15, %rdi
	subq	$8, %r12
	movq	%rax, 8(%r12)
	movq	%r13, %rdx
	sarq	$3, %rdx
	call	_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPN2v88internal6HandleINS3_6StringEEESt6vectorIS6_NS3_13ZoneAllocatorIS6_EEEEElS6_NS0_5__ops15_Iter_comp_iterIZNS3_6Module18GetModuleNamespaceEPNS3_7IsolateENS4_ISF_EEEUlS6_S6_E_EEEvT_T0_SM_T1_T2_
	cmpq	$8, %r13
	jg	.L27
.L21:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	.cfi_restore_state
	movq	8(%r15), %rsi
	movq	-8(%r13), %rdx
	movq	(%rbx), %rdi
	call	_ZN2v88internal6String7CompareEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	testl	%eax, %eax
	movq	(%r15), %rax
	jne	.L54
.L53:
	movq	-8(%r13), %rdx
	movq	%rdx, (%r15)
	movq	%rax, -8(%r13)
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L34:
	movq	(%r12), %rsi
	movq	-8(%r13), %rdx
	movq	(%rbx), %rdi
	call	_ZN2v88internal6String7CompareEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	testl	%eax, %eax
	movq	(%r15), %rax
	je	.L53
	movq	(%r12), %rdx
	movq	%rdx, (%r15)
	movq	%rax, (%r12)
	jmp	.L32
	.cfi_endproc
.LFE22553:
	.size	_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPN2v88internal6HandleINS3_6StringEEESt6vectorIS6_NS3_13ZoneAllocatorIS6_EEEEElNS0_5__ops15_Iter_comp_iterIZNS3_6Module18GetModuleNamespaceEPNS3_7IsolateENS4_ISF_EEEUlS6_S6_E_EEEvT_SL_T0_T1_, .-_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPN2v88internal6HandleINS3_6StringEEESt6vectorIS6_NS3_13ZoneAllocatorIS6_EEEEElNS0_5__ops15_Iter_comp_iterIZNS3_6Module18GetModuleNamespaceEPNS3_7IsolateENS4_ISF_EEEUlS6_S6_E_EEEvT_SL_T0_T1_
	.section	.text._ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal6HandleINS3_6StringEEESt6vectorIS6_NS3_13ZoneAllocatorIS6_EEEEENS0_5__ops15_Iter_comp_iterIZNS3_6Module18GetModuleNamespaceEPNS3_7IsolateENS4_ISF_EEEUlS6_S6_E_EEEvT_SL_T0_,"ax",@progbits
	.p2align 4
	.type	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal6HandleINS3_6StringEEESt6vectorIS6_NS3_13ZoneAllocatorIS6_EEEEENS0_5__ops15_Iter_comp_iterIZNS3_6Module18GetModuleNamespaceEPNS3_7IsolateENS4_ISF_EEEUlS6_S6_E_EEEvT_SL_T0_, @function
_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal6HandleINS3_6StringEEESt6vectorIS6_NS3_13ZoneAllocatorIS6_EEEEENS0_5__ops15_Iter_comp_iterIZNS3_6Module18GetModuleNamespaceEPNS3_7IsolateENS4_ISF_EEEUlS6_S6_E_EEEvT_SL_T0_:
.LFB22819:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -64(%rbp)
	cmpq	%rdi, %rsi
	je	.L57
	leaq	8(%rdi), %r8
	movq	%rdi, %rbx
	cmpq	%r8, %rsi
	je	.L57
	movq	%rdx, %r12
	movq	%r8, %r15
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L68:
	movq	(%r15), %r14
	cmpq	%r15, %rbx
	je	.L61
	movq	%r15, %rdx
	movl	$8, %eax
	movq	%rbx, %rsi
	subq	%rbx, %rdx
	leaq	(%rbx,%rax), %rdi
	call	memmove@PLT
.L61:
	movq	%r14, (%rbx)
	addq	$8, %r15
	cmpq	%r15, -64(%rbp)
	je	.L57
.L65:
	movq	(%r15), %rsi
	movq	(%rbx), %rdx
	movq	(%r12), %rdi
	call	_ZN2v88internal6String7CompareEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	testl	%eax, %eax
	je	.L68
	movq	(%r15), %r13
	movq	%r15, %r14
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L69:
	movq	(%r14), %rax
	movq	%rax, 8(%r14)
.L64:
	movq	-8(%r14), %rdx
	movq	(%r12), %rdi
	movq	%r13, %rsi
	movq	%r14, -56(%rbp)
	subq	$8, %r14
	call	_ZN2v88internal6String7CompareEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	testl	%eax, %eax
	je	.L69
	movq	-56(%rbp), %rax
	addq	$8, %r15
	movq	%r13, (%rax)
	cmpq	%r15, -64(%rbp)
	jne	.L65
.L57:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22819:
	.size	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal6HandleINS3_6StringEEESt6vectorIS6_NS3_13ZoneAllocatorIS6_EEEEENS0_5__ops15_Iter_comp_iterIZNS3_6Module18GetModuleNamespaceEPNS3_7IsolateENS4_ISF_EEEUlS6_S6_E_EEEvT_SL_T0_, .-_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal6HandleINS3_6StringEEESt6vectorIS6_NS3_13ZoneAllocatorIS6_EEEEENS0_5__ops15_Iter_comp_iterIZNS3_6Module18GetModuleNamespaceEPNS3_7IsolateENS4_ISF_EEEUlS6_S6_E_EEEvT_SL_T0_
	.section	.text._ZN2v88internal6Module9SetStatusENS1_6StatusE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Module9SetStatusENS1_6StatusE
	.type	_ZN2v88internal6Module9SetStatusENS1_6StatusE, @function
_ZN2v88internal6Module9SetStatusENS1_6StatusE:
.LFB18363:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	salq	$32, %rsi
	movq	%rsi, 23(%rax)
	ret
	.cfi_endproc
.LFE18363:
	.size	_ZN2v88internal6Module9SetStatusENS1_6StatusE, .-_ZN2v88internal6Module9SetStatusENS1_6StatusE
	.section	.text._ZN2v88internal6Module11RecordErrorEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Module11RecordErrorEPNS0_7IsolateE
	.type	_ZN2v88internal6Module11RecordErrorEPNS0_7IsolateE, @function
_ZN2v88internal6Module11RecordErrorEPNS0_7IsolateE:
.LFB18364:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	(%rdi), %r14
	movq	12480(%rsi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	-1(%r14), %rax
	cmpw	$119, 11(%rax)
	je	.L103
.L72:
	movabsq	$25769803776, %rdx
	movq	(%rbx), %rax
	movq	%rdx, 23(%rax)
	movq	(%rbx), %rax
	movq	%r12, 39(%rax)
	testb	$1, %r12b
	je	.L71
	movq	%r12, %r13
	movq	(%rbx), %rdi
	andq	$-262144, %r13
	movq	8(%r13), %rax
	leaq	39(%rdi), %rsi
	testl	$262144, %eax
	jne	.L104
	testb	$24, %al
	je	.L71
.L107:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L105
	.p2align 4,,10
	.p2align 3
.L71:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L106
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L104:
	.cfi_restore_state
	movq	%r12, %rdx
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	(%rbx), %rdi
	movq	8(%r13), %rax
	leaq	39(%rdi), %rsi
	testb	$24, %al
	jne	.L107
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L103:
	movq	%r14, %rax
	movq	%r14, %r13
	andq	$-262144, %rax
	movq	24(%rax), %r15
	movq	3520(%r15), %rdi
	subq	$37592, %r15
	testq	%rdi, %rdi
	je	.L73
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r13
	movq	%r13, -72(%rbp)
	cmpl	$3, 27(%r13)
	leaq	47(%r13), %r15
	jle	.L76
.L109:
	movq	47(%r13), %r14
.L77:
	movq	%r14, (%r15)
	testb	$1, %r14b
	je	.L72
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -88(%rbp)
	testl	$262144, %eax
	je	.L82
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rcx
	movq	8(%rcx), %rax
.L82:
	testb	$24, %al
	je	.L72
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L72
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L73:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L108
.L75:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	leaq	47(%r13), %r15
	movq	%r14, (%rax)
	movq	%r13, -72(%rbp)
	cmpl	$3, 27(%r13)
	jg	.L109
.L76:
	leaq	-72(%rbp), %rdi
	call	_ZNK2v88internal16SourceTextModule21GetSharedFunctionInfoEv@PLT
	movq	15(%rax), %rdx
	testb	$1, %dl
	jne	.L78
.L80:
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	subq	$37592, %rdi
	call	_ZN2v88internal9ScopeInfo5EmptyEPNS0_7IsolateE@PLT
	movq	%rax, %rdx
.L79:
	leaq	-64(%rbp), %rdi
	movq	%rdx, -64(%rbp)
	call	_ZNK2v88internal9ScopeInfo20ModuleDescriptorInfoEv@PLT
	movq	%rax, %r14
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L105:
	movq	%r12, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L78:
	movq	-1(%rdx), %rcx
	cmpw	$136, 11(%rcx)
	jne	.L80
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L108:
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L75
.L106:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18364:
	.size	_ZN2v88internal6Module11RecordErrorEPNS0_7IsolateE, .-_ZN2v88internal6Module11RecordErrorEPNS0_7IsolateE
	.section	.text._ZN2v88internal6Module5ResetEPNS0_7IsolateENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Module5ResetEPNS0_7IsolateENS0_6HandleIS1_EE
	.type	_ZN2v88internal6Module5ResetEPNS0_7IsolateENS0_6HandleIS1_EE, @function
_ZN2v88internal6Module5ResetEPNS0_7IsolateENS0_6HandleIS1_EE:
.LFB18366:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	(%rsi), %rax
	movq	-1(%rax), %rdx
	movq	55(%rax), %rax
	xorl	%edx, %edx
	movl	11(%rax), %esi
	call	_ZN2v88internal9HashTableINS0_15ObjectHashTableENS0_20ObjectHashTableShapeEE3NewEPNS0_7IsolateEiNS0_14AllocationTypeENS0_15MinimumCapacityE@PLT
	movq	(%rbx), %r12
	movq	%rax, %r14
	movq	-1(%r12), %rax
	cmpw	$119, 11(%rax)
	je	.L125
.L113:
	movq	(%r14), %r13
	leaq	7(%r12), %r15
	movq	%r13, 7(%r12)
	testb	$1, %r13b
	je	.L117
	movq	%r13, %r14
	andq	$-262144, %r14
	movq	8(%r14), %rax
	testl	$262144, %eax
	jne	.L126
	testb	$24, %al
	je	.L117
.L128:
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L127
.L117:
	movq	(%rbx), %rax
	movq	$0, 23(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L126:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r14), %rax
	testb	$24, %al
	jne	.L128
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L125:
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal16SourceTextModule5ResetEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%rbx), %r12
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L127:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L117
	.cfi_endproc
.LFE18366:
	.size	_ZN2v88internal6Module5ResetEPNS0_7IsolateENS0_6HandleIS1_EE, .-_ZN2v88internal6Module5ResetEPNS0_7IsolateENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal6Module10ResetGraphEPNS0_7IsolateENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Module10ResetGraphEPNS0_7IsolateENS0_6HandleIS1_EE
	.type	_ZN2v88internal6Module10ResetGraphEPNS0_7IsolateENS0_6HandleIS1_EE, @function
_ZN2v88internal6Module10ResetGraphEPNS0_7IsolateENS0_6HandleIS1_EE:
.LFB18365:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	cmpl	$1, 27(%rax)
	je	.L148
	ret
	.p2align 4,,10
	.p2align 3
.L148:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	-1(%rax), %rdx
	cmpw	$119, 11(%rdx)
	je	.L149
.L131:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Module5ResetEPNS0_7IsolateENS0_6HandleIS1_EE
	movq	(%rbx), %rax
	movq	-1(%rax), %rax
	cmpw	$119, 11(%rax)
	je	.L150
.L129:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L149:
	.cfi_restore_state
	movq	41112(%rdi), %rdi
	movq	71(%rax), %rsi
	testq	%rdi, %rdi
	je	.L132
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L150:
	movq	0(%r13), %rax
	movl	11(%rax), %edx
	testl	%edx, %edx
	jle	.L129
	xorl	%ebx, %ebx
.L141:
	movq	15(%rax,%rbx,8), %r14
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L135
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r14
	movq	%rax, %rsi
	testb	$1, %r14b
	jne	.L151
.L139:
	movq	0(%r13), %rax
	addq	$1, %rbx
	cmpl	%ebx, 11(%rax)
	jg	.L141
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L135:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L152
.L137:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	testb	$1, %r14b
	je	.L139
.L151:
	movq	-1(%r14), %rax
	movzwl	11(%rax), %eax
	subl	$119, %eax
	cmpw	$1, %ax
	ja	.L139
	movq	%r12, %rdi
	call	_ZN2v88internal6Module10ResetGraphEPNS0_7IsolateENS0_6HandleIS1_EE
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L132:
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L153
.L133:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L131
.L153:
	movq	%r12, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L152:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L137
	.cfi_endproc
.LFE18365:
	.size	_ZN2v88internal6Module10ResetGraphEPNS0_7IsolateENS0_6HandleIS1_EE, .-_ZN2v88internal6Module10ResetGraphEPNS0_7IsolateENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal6Module12GetExceptionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Module12GetExceptionEv
	.type	_ZN2v88internal6Module12GetExceptionEv, @function
_ZN2v88internal6Module12GetExceptionEv:
.LFB18367:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	39(%rax), %rax
	ret
	.cfi_endproc
.LFE18367:
	.size	_ZN2v88internal6Module12GetExceptionEv, .-_ZN2v88internal6Module12GetExceptionEv
	.section	.text._ZN2v88internal6Module13ResolveExportEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEES7_NS0_15MessageLocationEbPNS1_10ResolveSetE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Module13ResolveExportEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEES7_NS0_15MessageLocationEbPNS1_10ResolveSetE
	.type	_ZN2v88internal6Module13ResolveExportEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEES7_NS0_15MessageLocationEbPNS1_10ResolveSetE, @function
_ZN2v88internal6Module13ResolveExportEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEES7_NS0_15MessageLocationEbPNS1_10ResolveSetE:
.LFB18368:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	movzbl	%r8b, %r8d
	movq	-1(%rax), %rax
	cmpw	$119, 11(%rax)
	je	.L157
	jmp	_ZN2v88internal15SyntheticModule13ResolveExportEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEES7_NS0_15MessageLocationEb@PLT
	.p2align 4,,10
	.p2align 3
.L157:
	jmp	_ZN2v88internal16SourceTextModule13ResolveExportEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEES7_NS0_15MessageLocationEbPNS0_6Module10ResolveSetE@PLT
	.cfi_endproc
.LFE18368:
	.size	_ZN2v88internal6Module13ResolveExportEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEES7_NS0_15MessageLocationEbPNS1_10ResolveSetE, .-_ZN2v88internal6Module13ResolveExportEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEES7_NS0_15MessageLocationEbPNS1_10ResolveSetE
	.section	.text._ZN2v88internal6Module18PrepareInstantiateEPNS0_7IsolateENS0_6HandleIS1_EENS_5LocalINS_7ContextEEEPFNS_10MaybeLocalINS_6ModuleEEES8_NS6_INS_6StringEEENS6_ISA_EEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Module18PrepareInstantiateEPNS0_7IsolateENS0_6HandleIS1_EENS_5LocalINS_7ContextEEEPFNS_10MaybeLocalINS_6ModuleEEES8_NS6_INS_6StringEEENS6_ISA_EEE
	.type	_ZN2v88internal6Module18PrepareInstantiateEPNS0_7IsolateENS0_6HandleIS1_EENS_5LocalINS_7ContextEEEPFNS_10MaybeLocalINS_6ModuleEEES8_NS6_INS_6StringEEENS6_ISA_EEE, @function
_ZN2v88internal6Module18PrepareInstantiateEPNS0_7IsolateENS0_6HandleIS1_EENS_5LocalINS_7ContextEEEPFNS_10MaybeLocalINS_6ModuleEEES8_NS6_INS_6StringEEENS6_ISA_EEE:
.LFB18373:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	movl	27(%rax), %r8d
	testl	%r8d, %r8d
	jle	.L168
	movl	$1, %r8d
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L168:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	movabsq	$4294967296, %rdx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movq	%rdx, 23(%rax)
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	37528(%r15), %rax
	jb	.L169
	movq	(%r12), %rax
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	-1(%rax), %rax
	cmpw	$119, 11(%rax)
	je	.L170
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal15SyntheticModule18PrepareInstantiateEPNS0_7IsolateENS0_6HandleIS1_EENS_5LocalINS_7ContextEEEPFNS_10MaybeLocalINS_6ModuleEEES8_NS6_INS_6StringEEENS6_ISA_EEE@PLT
	.p2align 4,,10
	.p2align 3
.L170:
	.cfi_restore_state
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal16SourceTextModule18PrepareInstantiateEPNS0_7IsolateENS0_6HandleIS1_EENS_5LocalINS_7ContextEEEPFNS_10MaybeLocalINS_6ModuleEEES8_NS6_INS_6StringEEENS6_ISA_EEE@PLT
	.p2align 4,,10
	.p2align 3
.L169:
	.cfi_restore_state
	movq	%r15, %rdi
	call	_ZN2v88internal7Isolate13StackOverflowEv@PLT
	xorl	%r8d, %r8d
	popq	%r12
	popq	%r13
	movl	%r8d, %eax
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18373:
	.size	_ZN2v88internal6Module18PrepareInstantiateEPNS0_7IsolateENS0_6HandleIS1_EENS_5LocalINS_7ContextEEEPFNS_10MaybeLocalINS_6ModuleEEES8_NS6_INS_6StringEEENS6_ISA_EEE, .-_ZN2v88internal6Module18PrepareInstantiateEPNS0_7IsolateENS0_6HandleIS1_EENS_5LocalINS_7ContextEEEPFNS_10MaybeLocalINS_6ModuleEEES8_NS6_INS_6StringEEENS6_ISA_EEE
	.section	.text._ZN2v88internal6Module17FinishInstantiateEPNS0_7IsolateENS0_6HandleIS1_EEPNS0_15ZoneForwardListINS4_INS0_16SourceTextModuleEEEEEPjPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Module17FinishInstantiateEPNS0_7IsolateENS0_6HandleIS1_EEPNS0_15ZoneForwardListINS4_INS0_16SourceTextModuleEEEEEPjPNS0_4ZoneE
	.type	_ZN2v88internal6Module17FinishInstantiateEPNS0_7IsolateENS0_6HandleIS1_EEPNS0_15ZoneForwardListINS4_INS0_16SourceTextModuleEEEEEPjPNS0_4ZoneE, @function
_ZN2v88internal6Module17FinishInstantiateEPNS0_7IsolateENS0_6HandleIS1_EEPNS0_15ZoneForwardListINS4_INS0_16SourceTextModuleEEEEEPjPNS0_4ZoneE:
.LFB18374:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rsi), %rdx
	cmpl	$1, 27(%rdx)
	jle	.L178
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L178:
	.cfi_restore_state
	movq	%rdi, %r13
	movq	%rsi, %r12
	movq	%rcx, %r14
	movq	%r8, %rbx
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	37528(%r13), %rax
	jb	.L179
	movq	(%r12), %rax
	movq	-1(%rax), %rax
	cmpw	$119, 11(%rax)
	je	.L180
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal15SyntheticModule17FinishInstantiateEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	.p2align 4,,10
	.p2align 3
.L180:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%rbx, %r8
	movq	%r14, %rcx
	movq	%r15, %rdx
	popq	%rbx
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal16SourceTextModule17FinishInstantiateEPNS0_7IsolateENS0_6HandleIS1_EEPNS0_15ZoneForwardListIS5_EEPjPNS0_4ZoneE@PLT
	.p2align 4,,10
	.p2align 3
.L179:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal7Isolate13StackOverflowEv@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18374:
	.size	_ZN2v88internal6Module17FinishInstantiateEPNS0_7IsolateENS0_6HandleIS1_EEPNS0_15ZoneForwardListINS4_INS0_16SourceTextModuleEEEEEPjPNS0_4ZoneE, .-_ZN2v88internal6Module17FinishInstantiateEPNS0_7IsolateENS0_6HandleIS1_EEPNS0_15ZoneForwardListINS4_INS0_16SourceTextModuleEEEEEPjPNS0_4ZoneE
	.section	.rodata._ZN2v88internal6Module11InstantiateEPNS0_7IsolateENS0_6HandleIS1_EENS_5LocalINS_7ContextEEEPFNS_10MaybeLocalINS_6ModuleEEES8_NS6_INS_6StringEEENS6_ISA_EEE.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/v8/src/objects/module.cc:177"
	.section	.text._ZN2v88internal6Module11InstantiateEPNS0_7IsolateENS0_6HandleIS1_EENS_5LocalINS_7ContextEEEPFNS_10MaybeLocalINS_6ModuleEEES8_NS6_INS_6StringEEENS6_ISA_EEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Module11InstantiateEPNS0_7IsolateENS0_6HandleIS1_EENS_5LocalINS_7ContextEEEPFNS_10MaybeLocalINS_6ModuleEEES8_NS6_INS_6StringEEENS6_ISA_EEE
	.type	_ZN2v88internal6Module11InstantiateEPNS0_7IsolateENS0_6HandleIS1_EENS_5LocalINS_7ContextEEEPFNS_10MaybeLocalINS_6ModuleEEES8_NS6_INS_6StringEEENS6_ISA_EEE, @function
_ZN2v88internal6Module11InstantiateEPNS0_7IsolateENS0_6HandleIS1_EENS_5LocalINS_7ContextEEEPFNS_10MaybeLocalINS_6ModuleEEES8_NS6_INS_6StringEEENS6_ISA_EEE:
.LFB18369:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal6Module18PrepareInstantiateEPNS0_7IsolateENS0_6HandleIS1_EENS_5LocalINS_7ContextEEEPFNS_10MaybeLocalINS_6ModuleEEES8_NS6_INS_6StringEEENS6_ISA_EEE
	testb	%al, %al
	je	.L198
	leaq	-128(%rbp), %r14
	movq	41136(%r13), %rsi
	leaq	.LC0(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal4ZoneC1EPNS0_19AccountingAllocatorEPKc@PLT
	movq	%r14, %r8
	movq	%r15, %rsi
	movq	%r13, %rdi
	leaq	-148(%rbp), %rcx
	leaq	-144(%rbp), %rdx
	movq	%r14, -144(%rbp)
	movq	$0, -136(%rbp)
	movl	$0, -148(%rbp)
	call	_ZN2v88internal6Module17FinishInstantiateEPNS0_7IsolateENS0_6HandleIS1_EEPNS0_15ZoneForwardListINS4_INS0_16SourceTextModuleEEEEEPjPNS0_4ZoneE
	movl	%eax, %r12d
	testb	%al, %al
	je	.L199
.L184:
	movq	-136(%rbp), %rax
	testq	%rax, %rax
	je	.L185
	.p2align 4,,10
	.p2align 3
.L187:
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L187
.L185:
	movq	%r14, %rdi
	call	_ZN2v88internal4ZoneD1Ev@PLT
.L181:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L200
	addq	$120, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L199:
	.cfi_restore_state
	movq	-136(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L185
	.p2align 4,,10
	.p2align 3
.L186:
	movq	8(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal6Module5ResetEPNS0_7IsolateENS0_6HandleIS1_EE
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L186
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L198:
	movq	%r15, %rsi
	movq	%r13, %rdi
	movl	%eax, %r12d
	call	_ZN2v88internal6Module10ResetGraphEPNS0_7IsolateENS0_6HandleIS1_EE
	jmp	.L181
.L200:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18369:
	.size	_ZN2v88internal6Module11InstantiateEPNS0_7IsolateENS0_6HandleIS1_EENS_5LocalINS_7ContextEEEPFNS_10MaybeLocalINS_6ModuleEEES8_NS6_INS_6StringEEENS6_ISA_EEE, .-_ZN2v88internal6Module11InstantiateEPNS0_7IsolateENS0_6HandleIS1_EENS_5LocalINS_7ContextEEEPFNS_10MaybeLocalINS_6ModuleEEES8_NS6_INS_6StringEEENS6_ISA_EEE
	.section	.text._ZN2v88internal6Module8EvaluateEPNS0_7IsolateENS0_6HandleIS1_EEPNS0_15ZoneForwardListINS4_INS0_16SourceTextModuleEEEEEPj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Module8EvaluateEPNS0_7IsolateENS0_6HandleIS1_EEPNS0_15ZoneForwardListINS4_INS0_16SourceTextModuleEEEEEPj
	.type	_ZN2v88internal6Module8EvaluateEPNS0_7IsolateENS0_6HandleIS1_EEPNS0_15ZoneForwardListINS4_INS0_16SourceTextModuleEEEEEPj, @function
_ZN2v88internal6Module8EvaluateEPNS0_7IsolateENS0_6HandleIS1_EEPNS0_15ZoneForwardListINS4_INS0_16SourceTextModuleEEEEEPj:
.LFB18376:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	subq	$24, %rsp
	.cfi_offset 13, -40
	movq	(%rsi), %rdx
	movslq	27(%rdx), %rax
	cmpq	$6, %rax
	je	.L208
	cmpq	$3, %rax
	jle	.L204
	addq	$24, %rsp
	leaq	88(%rdi), %rax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L204:
	.cfi_restore_state
	movq	%rdi, -40(%rbp)
	movq	%rsi, %r13
	movq	%rcx, %r15
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	movq	-40(%rbp), %rdi
	cmpq	37528(%rdi), %rax
	jb	.L209
	movq	0(%r13), %rax
	movq	-1(%rax), %rax
	cmpw	$119, 11(%rax)
	je	.L210
	addq	$24, %rsp
	movq	%r13, %rsi
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal15SyntheticModule8EvaluateEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	.p2align 4,,10
	.p2align 3
.L208:
	.cfi_restore_state
	movq	39(%rdx), %rsi
	xorl	%edx, %edx
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L209:
	.cfi_restore_state
	call	_ZN2v88internal7Isolate13StackOverflowEv@PLT
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L210:
	.cfi_restore_state
	addq	$24, %rsp
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal16SourceTextModule8EvaluateEPNS0_7IsolateENS0_6HandleIS1_EEPNS0_15ZoneForwardListIS5_EEPj@PLT
	.cfi_endproc
.LFE18376:
	.size	_ZN2v88internal6Module8EvaluateEPNS0_7IsolateENS0_6HandleIS1_EEPNS0_15ZoneForwardListINS4_INS0_16SourceTextModuleEEEEEPj, .-_ZN2v88internal6Module8EvaluateEPNS0_7IsolateENS0_6HandleIS1_EEPNS0_15ZoneForwardListINS4_INS0_16SourceTextModuleEEEEEPj
	.section	.rodata._ZN2v88internal6Module8EvaluateEPNS0_7IsolateENS0_6HandleIS1_EE.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"../deps/v8/src/objects/module.cc:253"
	.section	.text._ZN2v88internal6Module8EvaluateEPNS0_7IsolateENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Module8EvaluateEPNS0_7IsolateENS0_6HandleIS1_EE
	.type	_ZN2v88internal6Module8EvaluateEPNS0_7IsolateENS0_6HandleIS1_EE, @function
_ZN2v88internal6Module8EvaluateEPNS0_7IsolateENS0_6HandleIS1_EE:
.LFB18375:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	cmpl	$6, 27(%rax)
	je	.L228
	movq	%rsi, %r14
	leaq	-112(%rbp), %r13
	movq	41136(%rdi), %rsi
	leaq	.LC1(%rip), %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal4ZoneC1EPNS0_19AccountingAllocatorEPKc@PLT
	leaq	-128(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	-140(%rbp), %rcx
	movq	%r13, -128(%rbp)
	movq	$0, -120(%rbp)
	movl	$0, -140(%rbp)
	call	_ZN2v88internal6Module8EvaluateEPNS0_7IsolateENS0_6HandleIS1_EEPNS0_15ZoneForwardListINS4_INS0_16SourceTextModuleEEEEEPj
	testq	%rax, %rax
	je	.L229
	movq	-120(%rbp), %rdx
	movq	%rax, %r12
.L217:
	testq	%rdx, %rdx
	je	.L216
	.p2align 4,,10
	.p2align 3
.L218:
	movq	(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L218
.L216:
	movq	%r13, %rdi
	call	_ZN2v88internal4ZoneD1Ev@PLT
.L213:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L230
	addq	$112, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L228:
	.cfi_restore_state
	movq	39(%rax), %rsi
	xorl	%edx, %edx
	xorl	%r12d, %r12d
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L229:
	movq	-120(%rbp), %r14
	leaq	-136(%rbp), %rbx
	testq	%r14, %r14
	je	.L231
	.p2align 4,,10
	.p2align 3
.L215:
	movq	8(%r14), %rax
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	(%rax), %rax
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal6Module11RecordErrorEPNS0_7IsolateE
	movq	(%r14), %r14
	testq	%r14, %r14
	jne	.L215
	movq	-120(%rbp), %rdx
	xorl	%r12d, %r12d
	jmp	.L217
.L231:
	xorl	%r12d, %r12d
	jmp	.L216
.L230:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18375:
	.size	_ZN2v88internal6Module8EvaluateEPNS0_7IsolateENS0_6HandleIS1_EE, .-_ZN2v88internal6Module8EvaluateEPNS0_7IsolateENS0_6HandleIS1_EE
	.section	.rodata._ZN2v88internal6Module18GetModuleNamespaceEPNS0_7IsolateENS0_6HandleIS1_EE.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"../deps/v8/src/objects/module.cc:303"
	.section	.rodata._ZN2v88internal6Module18GetModuleNamespaceEPNS0_7IsolateENS0_6HandleIS1_EE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"vector::reserve"
.LC5:
	.string	"vector::_M_realloc_insert"
.LC6:
	.string	"JSModuleNamespace"
	.section	.text._ZN2v88internal6Module18GetModuleNamespaceEPNS0_7IsolateENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Module18GetModuleNamespaceEPNS0_7IsolateENS0_6HandleIS1_EE
	.type	_ZN2v88internal6Module18GetModuleNamespaceEPNS0_7IsolateENS0_6HandleIS1_EE, @function
_ZN2v88internal6Module18GetModuleNamespaceEPNS0_7IsolateENS0_6HandleIS1_EE:
.LFB18377:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$216, %rsp
	movq	%rdi, -200(%rbp)
	movq	41112(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	31(%rax), %rsi
	testq	%rdi, %rdi
	je	.L233
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-200(%rbp), %rbx
	movq	(%rax), %rsi
	movq	%rax, %r14
	cmpq	%rsi, 88(%rbx)
	je	.L343
.L237:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L344
	addq	$216, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L233:
	.cfi_restore_state
	movq	41088(%rbx), %r14
	cmpq	41096(%rbx), %r14
	je	.L345
.L235:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r14)
	movq	-200(%rbp), %rbx
	cmpq	%rsi, 88(%rbx)
	jne	.L237
.L343:
	leaq	-192(%rbp), %rdi
	leaq	-72(%rbp), %r14
	movq	41136(%rbx), %rsi
	movq	%rdi, %xmm0
	movq	%r14, %xmm2
	leaq	.LC2(%rip), %rdx
	movq	%rdi, -240(%rbp)
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -224(%rbp)
	call	_ZN2v88internal4ZoneC1EPNS0_19AccountingAllocatorEPKc@PLT
	leaq	-88(%rbp), %rdi
	movdqa	-224(%rbp), %xmm0
	movl	$2, %esi
	movq	$1, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	$0, -96(%rbp)
	movl	$0x3f800000, -88(%rbp)
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	movaps	%xmm0, -128(%rbp)
	call	_ZNKSt8__detail20_Prime_rehash_policy11_M_next_bktEm@PLT
	movq	%rax, %r12
	cmpq	-112(%rbp), %rax
	jbe	.L238
	cmpq	$1, %rax
	je	.L346
	movq	-128(%rbp), %rdi
	leaq	0(,%rax,8), %rdx
	movq	%rdx, %rsi
	movq	16(%rdi), %r14
	movq	24(%rdi), %rax
	subq	%r14, %rax
	cmpq	%rax, %rdx
	ja	.L347
	addq	%r14, %rsi
	movq	%rsi, 16(%rdi)
.L242:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	memset@PLT
.L240:
	movq	%r14, -120(%rbp)
	movq	%r12, -112(%rbp)
.L238:
	movq	0(%r13), %rax
	movq	-1(%rax), %rdx
	cmpw	$119, 11(%rdx)
	je	.L348
.L243:
	movq	-200(%rbp), %r12
	movq	7(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L244
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -224(%rbp)
	movq	(%rax), %rsi
.L245:
	movslq	19(%rsi), %rax
	cmpq	$268435455, %rax
	ja	.L349
	movq	$0, -232(%rbp)
	xorl	%r12d, %r12d
	testq	%rax, %rax
	jne	.L350
.L248:
	movslq	35(%rsi), %rax
	testq	%rax, %rax
	jle	.L299
	subl	$1, %eax
	leaq	56(%rbx), %r14
	movq	%r12, %rbx
	movq	%r13, -248(%rbp)
	salq	$4, %rax
	movl	$40, %r15d
	addq	$56, %rax
	movq	%rax, -208(%rbp)
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L351:
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
	cmpq	%r12, -232(%rbp)
	je	.L257
.L353:
	movq	%rcx, (%r12)
	addq	$8, %r12
.L253:
	addq	$16, %r15
	cmpq	%r15, -208(%rbp)
	je	.L269
.L357:
	movq	-224(%rbp), %rax
	movq	(%rax), %rsi
.L270:
	movq	-1(%rsi,%r15), %r13
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal9HashTableINS0_15ObjectHashTableENS0_20ObjectHashTableShapeEE5IsKeyENS0_13ReadOnlyRootsENS0_6ObjectE@PLT
	testb	%al, %al
	je	.L253
	movq	-200(%rbp), %rdi
	movq	41112(%rdi), %r8
	testq	%r8, %r8
	jne	.L351
	movq	41088(%rdi), %rcx
	cmpq	41096(%rdi), %rcx
	je	.L352
.L256:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rdi)
	movq	%r13, (%rcx)
	cmpq	%r12, -232(%rbp)
	jne	.L353
.L257:
	movq	-232(%rbp), %r8
	subq	%rbx, %r8
	movq	%r8, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L354
	testq	%rax, %rax
	je	.L300
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L355
	movl	$2147483640, %esi
	movl	$2147483640, %r13d
.L260:
	movq	-176(%rbp), %rax
	movq	-168(%rbp), %rdi
	subq	%rax, %rdi
	cmpq	%rsi, %rdi
	jb	.L356
	addq	%rax, %rsi
	movq	%rsi, -176(%rbp)
.L263:
	leaq	(%rax,%r13), %rdx
	leaq	8(%rax), %rsi
	movq	%rdx, -232(%rbp)
.L261:
	movq	%rcx, (%rax,%r8)
	cmpq	%rbx, %r12
	je	.L303
	subq	$8, %r12
	leaq	15(%rax), %rcx
	subq	%rbx, %r12
	subq	%rbx, %rcx
	movq	%r12, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rcx
	jbe	.L304
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L304
	leaq	1(%rsi), %r8
	xorl	%ecx, %ecx
	movq	%r8, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L266:
	movdqu	(%rbx,%rcx), %xmm1
	movups	%xmm1, (%rax,%rcx)
	addq	$16, %rcx
	cmpq	%rcx, %rdi
	jne	.L266
	movq	%r8, %rcx
	andq	$-2, %rcx
	leaq	0(,%rcx,8), %rsi
	addq	%rsi, %rbx
	addq	%rax, %rsi
	cmpq	%r8, %rcx
	je	.L268
	movq	(%rbx), %rcx
	movq	%rcx, (%rsi)
.L268:
	leaq	16(%rax,%r12), %r12
.L264:
	movq	%rax, %rbx
	addq	$16, %r15
	cmpq	%r15, -208(%rbp)
	jne	.L357
	.p2align 4,,10
	.p2align 3
.L269:
	movq	%r12, %r14
	movq	-248(%rbp), %r13
	subq	%rbx, %r14
	movq	%r14, %rax
	sarq	$3, %rax
	movl	%eax, -224(%rbp)
	cmpq	%rbx, %r12
	je	.L251
	bsrq	%rax, %rax
	movl	$63, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	xorq	$63, %rax
	leaq	-200(%rbp), %r15
	subl	%eax, %edx
	movq	%r15, %rcx
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	call	_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPN2v88internal6HandleINS3_6StringEEESt6vectorIS6_NS3_13ZoneAllocatorIS6_EEEEElNS0_5__ops15_Iter_comp_iterIZNS3_6Module18GetModuleNamespaceEPNS3_7IsolateENS4_ISF_EEEUlS6_S6_E_EEEvT_SL_T0_T1_
	cmpq	$128, %r14
	jle	.L271
	leaq	128(%rbx), %r14
	movq	%r15, %rdx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal6HandleINS3_6StringEEESt6vectorIS6_NS3_13ZoneAllocatorIS6_EEEEENS0_5__ops15_Iter_comp_iterIZNS3_6Module18GetModuleNamespaceEPNS3_7IsolateENS4_ISF_EEEUlS6_S6_E_EEEvT_SL_T0_
	cmpq	%r12, %r14
	je	.L251
	movq	%rbx, -208(%rbp)
	movq	%r14, %r15
	movq	%r13, -232(%rbp)
	.p2align 4,,10
	.p2align 3
.L276:
	movq	(%r15), %rbx
	movq	%r15, %r13
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L358:
	movq	0(%r13), %rax
	movq	%rax, 8(%r13)
.L275:
	movq	-8(%r13), %rdx
	movq	-200(%rbp), %rdi
	movq	%rbx, %rsi
	movq	%r13, %r14
	subq	$8, %r13
	call	_ZN2v88internal6String7CompareEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	testl	%eax, %eax
	je	.L358
	addq	$8, %r15
	movq	%rbx, (%r14)
	cmpq	%r15, %r12
	jne	.L276
	movq	-208(%rbp), %rbx
	movq	-232(%rbp), %r13
.L251:
	movq	-200(%rbp), %rdi
	call	_ZN2v88internal7Factory20NewJSModuleNamespaceEv@PLT
	movq	0(%r13), %r15
	movq	(%rax), %rdi
	movq	%rax, %r14
	movq	%r15, 23(%rdi)
	leaq	23(%rdi), %rsi
	testb	$1, %r15b
	je	.L296
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -208(%rbp)
	testl	$262144, %eax
	je	.L279
	movq	%r15, %rdx
	movq	%rsi, -248(%rbp)
	movq	%rdi, -232(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-208(%rbp), %rcx
	movq	-248(%rbp), %rsi
	movq	-232(%rbp), %rdi
	movq	8(%rcx), %rax
.L279:
	testb	$24, %al
	je	.L296
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L359
	.p2align 4,,10
	.p2align 3
.L296:
	movq	0(%r13), %r15
	movq	(%r14), %r13
	movq	%r13, 31(%r15)
	leaq	31(%r15), %rsi
	testb	$1, %r13b
	je	.L295
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -208(%rbp)
	testl	$262144, %eax
	je	.L282
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rsi, -232(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-208(%rbp), %rcx
	movq	-232(%rbp), %rsi
	movq	8(%rcx), %rax
.L282:
	testb	$24, %al
	je	.L295
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L360
	.p2align 4,,10
	.p2align 3
.L295:
	movl	-224(%rbp), %ecx
	movq	-200(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r14, %rsi
	leaq	.LC6(%rip), %r8
	call	_ZN2v88internal8JSObject19NormalizePropertiesEPNS0_7IsolateENS0_6HandleIS1_EENS0_25PropertyNormalizationModeEiPKc@PLT
	cmpq	%r12, %rbx
	je	.L288
	.p2align 4,,10
	.p2align 3
.L287:
	movq	(%rbx), %rsi
	movq	-200(%rbp), %rdi
	addq	$8, %rbx
	call	_ZN2v88internal9Accessors28MakeModuleNamespaceEntryInfoEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-8(%rbx), %rsi
	movl	$225, %ecx
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8JSObject21SetNormalizedPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6ObjectEEENS0_15PropertyDetailsE@PLT
	cmpq	%rbx, %r12
	jne	.L287
.L288:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8JSObject17PreventExtensionsENS0_6HandleIS1_EENS0_11ShouldThrowE@PLT
	testb	%al, %al
	je	.L361
.L285:
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8JSObject19OptimizeAsPrototypeENS0_6HandleIS1_EEb@PLT
	movq	-200(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal3Map24GetOrCreatePrototypeInfoENS0_6HandleINS0_8JSObjectEEEPNS0_7IsolateE@PLT
	movq	(%r14), %r12
	movq	(%rax), %r13
	movq	%r12, 7(%r13)
	leaq	7(%r13), %r15
	testb	$1, %r12b
	je	.L294
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L290
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
.L290:
	testb	$24, %al
	je	.L294
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L362
	.p2align 4,,10
	.p2align 3
.L294:
	movq	-104(%rbp), %rax
	testq	%rax, %rax
	je	.L292
	.p2align 4,,10
	.p2align 3
.L293:
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L293
.L292:
	movq	-112(%rbp), %rax
	movq	-120(%rbp), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-240(%rbp), %rdi
	call	_ZN2v88internal4ZoneD1Ev@PLT
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L244:
	movq	41088(%r12), %rax
	movq	%rax, -224(%rbp)
	cmpq	41096(%r12), %rax
	je	.L363
.L246:
	movq	-224(%rbp), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdx)
	jmp	.L245
	.p2align 4,,10
	.p2align 3
.L355:
	testq	%rsi, %rsi
	jne	.L364
	movq	$0, -232(%rbp)
	movl	$8, %esi
	xorl	%eax, %eax
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L352:
	movq	%rdi, -256(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-256(%rbp), %rdi
	movq	%rax, %rcx
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L345:
	movq	%rbx, %rdi
	movq	%rsi, -224(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-224(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L350:
	movq	-176(%rbp), %r12
	movq	-168(%rbp), %rdx
	leaq	0(,%rax,8), %r15
	movq	%r15, %rsi
	subq	%r12, %rdx
	cmpq	%rdx, %r15
	ja	.L365
	addq	%r12, %rsi
	movq	%rsi, -176(%rbp)
.L250:
	leaq	(%r12,%r15), %rax
	movq	%rax, -232(%rbp)
	movq	-224(%rbp), %rax
	movq	(%rax), %rsi
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L300:
	movl	$8, %esi
	movl	$8, %r13d
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L271:
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal6HandleINS3_6StringEEESt6vectorIS6_NS3_13ZoneAllocatorIS6_EEEEENS0_5__ops15_Iter_comp_iterIZNS3_6Module18GetModuleNamespaceEPNS3_7IsolateENS4_ISF_EEEUlS6_S6_E_EEEvT_SL_T0_
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L348:
	movq	-240(%rbp), %rdx
	movq	-200(%rbp), %rdi
	leaq	-128(%rbp), %rcx
	movq	%r13, %rsi
	call	_ZN2v88internal16SourceTextModule16FetchStarExportsEPNS0_7IsolateENS0_6HandleIS1_EEPNS0_4ZoneEPNS0_18UnorderedModuleSetE@PLT
	movq	0(%r13), %rax
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L362:
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L360:
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L359:
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L304:
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L265:
	movq	(%rbx,%rcx,8), %rdx
	movq	%rdx, (%rax,%rcx,8)
	movq	%rcx, %rdx
	addq	$1, %rcx
	cmpq	%rsi, %rdx
	jne	.L265
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L299:
	movl	$0, -224(%rbp)
	movq	%r12, %rbx
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L303:
	movq	%rsi, %r12
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L361:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L285
.L363:
	movq	%r12, %rdi
	movq	%rsi, -208(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-208(%rbp), %rsi
	movq	%rax, -224(%rbp)
	jmp	.L246
.L356:
	movq	-240(%rbp), %rdi
	movq	%r8, -256(%rbp)
	movq	%rcx, -232(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-232(%rbp), %rcx
	movq	-256(%rbp), %r8
	jmp	.L263
.L346:
	movq	$0, -72(%rbp)
	jmp	.L240
.L347:
	movq	%rdx, -224(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-224(%rbp), %rdx
	movq	%rax, %r14
	jmp	.L242
.L365:
	movq	-240(%rbp), %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L250
.L344:
	call	__stack_chk_fail@PLT
.L349:
	leaq	.LC4(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L354:
	leaq	.LC5(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L364:
	cmpq	$268435455, %rsi
	movl	$268435455, %r13d
	cmovbe	%rsi, %r13
	salq	$3, %r13
	movq	%r13, %rsi
	jmp	.L260
	.cfi_endproc
.LFE18377:
	.size	_ZN2v88internal6Module18GetModuleNamespaceEPNS0_7IsolateENS0_6HandleIS1_EE, .-_ZN2v88internal6Module18GetModuleNamespaceEPNS0_7IsolateENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal17JSModuleNamespace9GetExportEPNS0_7IsolateENS0_6HandleINS0_6StringEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17JSModuleNamespace9GetExportEPNS0_7IsolateENS0_6HandleINS0_6StringEEE
	.type	_ZN2v88internal17JSModuleNamespace9GetExportEPNS0_7IsolateENS0_6HandleINS0_6StringEEE, @function
_ZN2v88internal17JSModuleNamespace9GetExportEPNS0_7IsolateENS0_6HandleINS0_6StringEEE:
.LFB18385:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	movq	%rdx, %rsi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	leaq	-32(%rbp), %rdi
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal19ObjectHashTableBaseINS0_15ObjectHashTableENS0_20ObjectHashTableShapeEE6LookupENS0_6HandleINS0_6ObjectEEE@PLT
	movq	41112(%r12), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L367
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rax
	cmpq	%rax, 96(%r12)
	je	.L378
.L370:
	movq	41112(%r12), %rdi
	movq	7(%rax), %rsi
	testq	%rdi, %rdi
	je	.L372
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	cmpq	%rsi, 96(%r12)
	je	.L379
.L371:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L380
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L372:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L381
.L374:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	cmpq	%rsi, 96(%r12)
	jne	.L371
.L379:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movl	$177, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory17NewReferenceErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	xorl	%eax, %eax
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L367:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L382
.L369:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	movq	(%rax), %rax
	cmpq	%rax, 96(%r12)
	jne	.L370
.L378:
	leaq	88(%r12), %rax
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L382:
	movq	%r12, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L381:
	movq	%r12, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L374
.L380:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18385:
	.size	_ZN2v88internal17JSModuleNamespace9GetExportEPNS0_7IsolateENS0_6HandleINS0_6StringEEE, .-_ZN2v88internal17JSModuleNamespace9GetExportEPNS0_7IsolateENS0_6HandleINS0_6StringEEE
	.section	.text._ZN2v88internal17JSModuleNamespace21GetPropertyAttributesEPNS0_14LookupIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17JSModuleNamespace21GetPropertyAttributesEPNS0_14LookupIteratorE
	.type	_ZN2v88internal17JSModuleNamespace21GetPropertyAttributesEPNS0_14LookupIteratorE, @function
_ZN2v88internal17JSModuleNamespace21GetPropertyAttributesEPNS0_14LookupIteratorE:
.LFB18386:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	32(%rdi), %r13
	movq	56(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %r12
	testq	%r13, %r13
	je	.L401
.L384:
	movq	(%r14), %rax
	leaq	-64(%rbp), %rdi
	movq	%r13, %rsi
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal19ObjectHashTableBaseINS0_15ObjectHashTableENS0_20ObjectHashTableShapeEE6LookupENS0_6HandleINS0_6ObjectEEE@PLT
	movq	41112(%r12), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L390
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rax
	cmpq	%rax, 96(%r12)
	je	.L402
.L393:
	movq	41112(%r12), %rdi
	movq	7(%rax), %rsi
	testq	%rdi, %rdi
	je	.L395
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	cmpq	%rsi, 96(%r12)
	je	.L403
.L398:
	movl	16(%rbx), %eax
	shrl	$3, %eax
	andl	$7, %eax
	salq	$32, %rax
	orq	$1, %rax
.L394:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L404
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L395:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L405
.L397:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	cmpq	%rsi, 96(%r12)
	jne	.L398
.L403:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movl	$177, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory17NewReferenceErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	xorl	%eax, %eax
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L390:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L406
.L392:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	movq	(%rax), %rax
	cmpq	%rax, 96(%r12)
	jne	.L393
.L402:
	movabsq	$274877906945, %rax
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L401:
	movl	72(%rdi), %r15d
	testl	%r15d, %r15d
	js	.L385
	movq	%r15, %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	salq	$32, %rsi
	call	_ZN2v88internal7Factory14NumberToStringENS0_3SmiEb@PLT
	movq	%rax, %r13
.L386:
	movq	0(%r13), %rax
	movl	11(%rax), %esi
	cmpl	$10, %esi
	jle	.L407
.L389:
	movq	%r13, 32(%rbx)
	movq	24(%rbx), %r12
	jmp	.L384
	.p2align 4,,10
	.p2align 3
.L406:
	movq	%r12, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	jmp	.L392
	.p2align 4,,10
	.p2align 3
.L385:
	xorl	%edx, %edx
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory17NewNumberFromUintEjNS0_14AllocationTypeE@PLT
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal7Factory14NumberToStringENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%rax, %r13
	jmp	.L386
	.p2align 4,,10
	.p2align 3
.L407:
	cmpl	$3, 7(%rax)
	jne	.L389
	movl	%r15d, %edi
	call	_ZN2v88internal12StringHasher18MakeArrayIndexHashEji@PLT
	movq	0(%r13), %rdx
	movl	%eax, 7(%rdx)
	jmp	.L389
	.p2align 4,,10
	.p2align 3
.L405:
	movq	%r12, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	jmp	.L397
.L404:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18386:
	.size	_ZN2v88internal17JSModuleNamespace21GetPropertyAttributesEPNS0_14LookupIteratorE, .-_ZN2v88internal17JSModuleNamespace21GetPropertyAttributesEPNS0_14LookupIteratorE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal6Module9SetStatusENS1_6StatusE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal6Module9SetStatusENS1_6StatusE, @function
_GLOBAL__sub_I__ZN2v88internal6Module9SetStatusENS1_6StatusE:
.LFB23215:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE23215:
	.size	_GLOBAL__sub_I__ZN2v88internal6Module9SetStatusENS1_6StatusE, .-_GLOBAL__sub_I__ZN2v88internal6Module9SetStatusENS1_6StatusE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal6Module9SetStatusENS1_6StatusE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
