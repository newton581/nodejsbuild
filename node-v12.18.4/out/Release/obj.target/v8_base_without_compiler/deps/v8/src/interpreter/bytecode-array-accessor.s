	.file	"bytecode-array-accessor.cc"
	.text
	.section	.text._ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray6lengthEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray6lengthEv, @function
_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray6lengthEv:
.LFB17798:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	(%rax), %rax
	movslq	11(%rax), %rax
	ret
	.cfi_endproc
.LFE17798:
	.size	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray6lengthEv, .-_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray6lengthEv
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray3setEih,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray3setEih, @function
_ZN2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray3setEih:
.LFB17801:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	addl	$54, %esi
	movslq	%esi, %rsi
	movq	(%rax), %rax
	movb	%dl, -1(%rsi,%rax)
	ret
	.cfi_endproc
.LFE17801:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray3setEih, .-_ZN2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray3setEih
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArrayD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArrayD2Ev, @function
_ZN2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArrayD2Ev:
.LFB20753:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE20753:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArrayD2Ev, .-_ZN2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArrayD2Ev
	.set	_ZN2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArrayD1Ev,_ZN2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArrayD2Ev
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArrayD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArrayD0Ev, @function
_ZN2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArrayD0Ev:
.LFB20755:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE20755:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArrayD0Ev, .-_ZN2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArrayD0Ev
	.section	.text._ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray23GetConstantAtIndexAsSmiEi,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray23GetConstantAtIndexAsSmiEi, @function
_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray23GetConstantAtIndexAsSmiEi:
.LFB17805:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	(%rax), %rdx
	leal	16(,%rsi,8), %eax
	cltq
	movq	15(%rdx), %rdx
	movq	-1(%rax,%rdx), %rax
	ret
	.cfi_endproc
.LFE17805:
	.size	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray23GetConstantAtIndexAsSmiEi, .-_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray23GetConstantAtIndexAsSmiEi
	.section	.text._ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray20IsConstantAtIndexSmiEi,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray20IsConstantAtIndexSmiEi, @function
_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray20IsConstantAtIndexSmiEi:
.LFB17804:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	(%rax), %rdx
	leal	16(,%rsi,8), %eax
	cltq
	movq	15(%rdx), %rdx
	movq	-1(%rax,%rdx), %rax
	notq	%rax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE17804:
	.size	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray20IsConstantAtIndexSmiEi, .-_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray20IsConstantAtIndexSmiEi
	.section	.text._ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray18GetConstantAtIndexEiPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray18GetConstantAtIndexEiPNS0_7IsolateE, @function
_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray18GetConstantAtIndexEiPNS0_7IsolateE:
.LFB17803:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdx, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %rax
	movq	(%rax), %rdx
	leal	16(,%rsi,8), %eax
	cltq
	movq	15(%rdx), %rdx
	movq	-1(%rax,%rdx), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L9
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L13
.L11:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L11
	.cfi_endproc
.LFE17803:
	.size	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray18GetConstantAtIndexEiPNS0_7IsolateE, .-_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray18GetConstantAtIndexEiPNS0_7IsolateE
	.section	.text._ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray23GetFirstBytecodeAddressEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray23GetFirstBytecodeAddressEv, @function
_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray23GetFirstBytecodeAddressEv:
.LFB17802:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	(%rax), %rax
	addq	$53, %rax
	ret
	.cfi_endproc
.LFE17802:
	.size	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray23GetFirstBytecodeAddressEv, .-_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray23GetFirstBytecodeAddressEv
	.section	.text._ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray15parameter_countEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray15parameter_countEv, @function
_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray15parameter_countEv:
.LFB17799:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	(%rax), %rax
	movl	43(%rax), %eax
	sarl	$3, %eax
	ret
	.cfi_endproc
.LFE17799:
	.size	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray15parameter_countEv, .-_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray15parameter_countEv
	.section	.text._ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray3getEi,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray3getEi, @function
_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray3getEi:
.LFB17800:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	addl	$54, %esi
	movslq	%esi, %rsi
	movq	(%rax), %rax
	movzbl	-1(%rsi,%rax), %eax
	ret
	.cfi_endproc
.LFE17800:
	.size	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray3getEi, .-_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray3getEi
	.section	.rodata._ZN2v88internal11interpreter21BytecodeArrayAccessorC2ESt10unique_ptrINS1_21AbstractBytecodeArrayESt14default_deleteIS4_EEi.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal11interpreter21BytecodeArrayAccessorC2ESt10unique_ptrINS1_21AbstractBytecodeArrayESt14default_deleteIS4_EEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter21BytecodeArrayAccessorC2ESt10unique_ptrINS1_21AbstractBytecodeArrayESt14default_deleteIS4_EEi
	.type	_ZN2v88internal11interpreter21BytecodeArrayAccessorC2ESt10unique_ptrINS1_21AbstractBytecodeArrayESt14default_deleteIS4_EEi, @function
_ZN2v88internal11interpreter21BytecodeArrayAccessorC2ESt10unique_ptrINS1_21AbstractBytecodeArrayESt14default_deleteIS4_EEi:
.LFB17808:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	(%rsi), %rdi
	movl	%edx, 8(%rbx)
	movq	$0, (%rsi)
	movq	%rdi, (%rbx)
	movb	$1, 12(%rbx)
	movl	$0, 16(%rbx)
	testl	%edx, %edx
	js	.L17
	movq	(%rdi), %rax
	movl	%edx, %r12d
	leaq	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray6lengthEv(%rip), %rdx
	movq	(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L20
	movq	8(%rdi), %rax
	movq	(%rax), %rax
	movl	11(%rax), %eax
.L21:
	cmpl	%eax, %r12d
	jge	.L17
	movq	(%rbx), %rdi
	leaq	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray3getEi(%rip), %rdx
	movl	8(%rbx), %esi
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L23
	movq	8(%rdi), %rax
	addl	$54, %esi
	movslq	%esi, %rsi
	movq	(%rax), %rax
	movzbl	-1(%rsi,%rax), %eax
.L24:
	cmpb	$3, %al
	ja	.L34
	cmpb	$2, %al
	je	.L26
	cmpb	$3, %al
	je	.L29
	testb	%al, %al
	jne	.L35
	movl	$2, %eax
.L26:
	movb	%al, 12(%rbx)
	movl	$1, 16(%rbx)
.L17:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	call	*%rax
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L34:
	movb	$1, 12(%rbx)
	movl	$0, 16(%rbx)
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L35:
	cmpb	$1, %al
	jne	.L36
.L29:
	movl	$4, %eax
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L23:
	call	*%rax
	jmp	.L24
.L36:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE17808:
	.size	_ZN2v88internal11interpreter21BytecodeArrayAccessorC2ESt10unique_ptrINS1_21AbstractBytecodeArrayESt14default_deleteIS4_EEi, .-_ZN2v88internal11interpreter21BytecodeArrayAccessorC2ESt10unique_ptrINS1_21AbstractBytecodeArrayESt14default_deleteIS4_EEi
	.globl	_ZN2v88internal11interpreter21BytecodeArrayAccessorC1ESt10unique_ptrINS1_21AbstractBytecodeArrayESt14default_deleteIS4_EEi
	.set	_ZN2v88internal11interpreter21BytecodeArrayAccessorC1ESt10unique_ptrINS1_21AbstractBytecodeArrayESt14default_deleteIS4_EEi,_ZN2v88internal11interpreter21BytecodeArrayAccessorC2ESt10unique_ptrINS1_21AbstractBytecodeArrayESt14default_deleteIS4_EEi
	.section	.text._ZN2v88internal11interpreter21BytecodeArrayAccessorC2ENS0_6HandleINS0_13BytecodeArrayEEEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter21BytecodeArrayAccessorC2ENS0_6HandleINS0_13BytecodeArrayEEEi
	.type	_ZN2v88internal11interpreter21BytecodeArrayAccessorC2ENS0_6HandleINS0_13BytecodeArrayEEEi, @function
_ZN2v88internal11interpreter21BytecodeArrayAccessorC2ENS0_6HandleINS0_13BytecodeArrayEEEi:
.LFB17816:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movl	$16, %edi
	subq	$8, %rsp
	call	_Znwm@PLT
	leaq	16+_ZTVN2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArrayE(%rip), %rcx
	movl	%r12d, 8(%rbx)
	movq	%rcx, (%rax)
	movq	%r13, 8(%rax)
	movq	%rax, (%rbx)
	movb	$1, 12(%rbx)
	movl	$0, 16(%rbx)
	testl	%r12d, %r12d
	js	.L37
	movq	0(%r13), %rax
	cmpl	%r12d, 11(%rax)
	jle	.L37
	addl	$54, %r12d
	movslq	%r12d, %r12
	movzbl	-1(%rax,%r12), %eax
	cmpb	$3, %al
	ja	.L37
	cmpb	$2, %al
	je	.L39
	cmpb	$3, %al
	je	.L42
	testb	%al, %al
	jne	.L47
	movl	$2, %eax
.L39:
	movb	%al, 12(%rbx)
	movl	$1, 16(%rbx)
.L37:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore_state
	cmpb	$1, %al
	jne	.L48
.L42:
	movl	$4, %eax
	jmp	.L39
.L48:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE17816:
	.size	_ZN2v88internal11interpreter21BytecodeArrayAccessorC2ENS0_6HandleINS0_13BytecodeArrayEEEi, .-_ZN2v88internal11interpreter21BytecodeArrayAccessorC2ENS0_6HandleINS0_13BytecodeArrayEEEi
	.globl	_ZN2v88internal11interpreter21BytecodeArrayAccessorC1ENS0_6HandleINS0_13BytecodeArrayEEEi
	.set	_ZN2v88internal11interpreter21BytecodeArrayAccessorC1ENS0_6HandleINS0_13BytecodeArrayEEEi,_ZN2v88internal11interpreter21BytecodeArrayAccessorC2ENS0_6HandleINS0_13BytecodeArrayEEEi
	.section	.text._ZN2v88internal11interpreter21BytecodeArrayAccessor9SetOffsetEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter21BytecodeArrayAccessor9SetOffsetEi
	.type	_ZN2v88internal11interpreter21BytecodeArrayAccessor9SetOffsetEi, @function
_ZN2v88internal11interpreter21BytecodeArrayAccessor9SetOffsetEi:
.LFB17818:
	.cfi_startproc
	endbr64
	movl	%esi, 8(%rdi)
	testl	%esi, %esi
	js	.L66
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray6lengthEv(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %rdi
	movq	(%rdi), %rax
	movq	(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L52
	movq	8(%rdi), %rax
	movq	(%rax), %rax
	movl	11(%rax), %eax
.L53:
	cmpl	%eax, %esi
	jge	.L49
	movq	(%rbx), %rdi
	leaq	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray3getEi(%rip), %rdx
	movl	8(%rbx), %esi
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L55
	movq	8(%rdi), %rax
	addl	$54, %esi
	movslq	%esi, %rsi
	movq	(%rax), %rax
	movzbl	-1(%rsi,%rax), %eax
.L56:
	cmpb	$3, %al
	ja	.L69
	cmpb	$2, %al
	je	.L58
	cmpb	$3, %al
	je	.L61
	testb	%al, %al
	jne	.L70
	movl	$2, %eax
.L58:
	movb	%al, 12(%rbx)
	movl	$1, 16(%rbx)
.L49:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	%esi, -20(%rbp)
	call	*%rax
	movl	-20(%rbp), %esi
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L69:
	movb	$1, 12(%rbx)
	movl	$0, 16(%rbx)
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L70:
	cmpb	$1, %al
	jne	.L71
.L61:
	movl	$4, %eax
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L55:
	call	*%rax
	jmp	.L56
.L71:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE17818:
	.size	_ZN2v88internal11interpreter21BytecodeArrayAccessor9SetOffsetEi, .-_ZN2v88internal11interpreter21BytecodeArrayAccessor9SetOffsetEi
	.section	.text._ZN2v88internal11interpreter21BytecodeArrayAccessor15ApplyDebugBreakEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter21BytecodeArrayAccessor15ApplyDebugBreakEv
	.type	_ZN2v88internal11interpreter21BytecodeArrayAccessor15ApplyDebugBreakEv, @function
_ZN2v88internal11interpreter21BytecodeArrayAccessor15ApplyDebugBreakEv:
.LFB17819:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray3getEi(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movl	8(%rbx), %esi
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L73
	movq	8(%rdi), %rax
	addl	$54, %esi
	movslq	%esi, %rsi
	movq	(%rax), %rax
	movzbl	-1(%rsi,%rax), %r12d
.L74:
	movl	%r12d, %edi
	call	_ZN2v88internal11interpreter9Bytecodes12IsDebugBreakENS1_8BytecodeE@PLT
	testb	%al, %al
	je	.L78
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L78:
	.cfi_restore_state
	movl	%r12d, %edi
	call	_ZN2v88internal11interpreter9Bytecodes13GetDebugBreakENS1_8BytecodeE@PLT
	movq	(%rbx), %rdi
	movl	8(%rbx), %esi
	movq	(%rdi), %rdx
	movq	24(%rdx), %rcx
	leaq	_ZN2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray3setEih(%rip), %rdx
	cmpq	%rdx, %rcx
	jne	.L76
	movq	8(%rdi), %rdx
	addl	$54, %esi
	movslq	%esi, %rsi
	movq	(%rdx), %rdx
	movb	%al, -1(%rsi,%rdx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	.cfi_restore_state
	call	*%rax
	movl	%eax, %r12d
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L76:
	popq	%rbx
	movzbl	%al, %edx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rcx
	.cfi_endproc
.LFE17819:
	.size	_ZN2v88internal11interpreter21BytecodeArrayAccessor15ApplyDebugBreakEv, .-_ZN2v88internal11interpreter21BytecodeArrayAccessor15ApplyDebugBreakEv
	.section	.text._ZN2v88internal11interpreter21BytecodeArrayAccessor18UpdateOperandScaleEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter21BytecodeArrayAccessor18UpdateOperandScaleEv
	.type	_ZN2v88internal11interpreter21BytecodeArrayAccessor18UpdateOperandScaleEv, @function
_ZN2v88internal11interpreter21BytecodeArrayAccessor18UpdateOperandScaleEv:
.LFB17820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	8(%rdi), %r12d
	testl	%r12d, %r12d
	js	.L79
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	leaq	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray6lengthEv(%rip), %rdx
	movq	(%rdi), %rax
	movq	(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L82
	movq	8(%rdi), %rax
	movq	(%rax), %rax
	movl	11(%rax), %eax
.L83:
	cmpl	%eax, %r12d
	jge	.L79
	movq	(%rbx), %rdi
	leaq	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray3getEi(%rip), %rdx
	movl	8(%rbx), %esi
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L85
	movq	8(%rdi), %rax
	addl	$54, %esi
	movslq	%esi, %rsi
	movq	(%rax), %rax
	movzbl	-1(%rsi,%rax), %eax
.L86:
	cmpb	$3, %al
	ja	.L96
	cmpb	$2, %al
	je	.L88
	cmpb	$3, %al
	je	.L91
	testb	%al, %al
	jne	.L97
	movl	$2, %eax
.L88:
	movb	%al, 12(%rbx)
	movl	$1, 16(%rbx)
.L79:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L82:
	.cfi_restore_state
	call	*%rax
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L96:
	movb	$1, 12(%rbx)
	movl	$0, 16(%rbx)
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L97:
	cmpb	$1, %al
	jne	.L98
.L91:
	movl	$4, %eax
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L85:
	call	*%rax
	jmp	.L86
.L98:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE17820:
	.size	_ZN2v88internal11interpreter21BytecodeArrayAccessor18UpdateOperandScaleEv, .-_ZN2v88internal11interpreter21BytecodeArrayAccessor18UpdateOperandScaleEv
	.section	.text._ZNK2v88internal11interpreter21BytecodeArrayAccessor14OffsetInBoundsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14OffsetInBoundsEv
	.type	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14OffsetInBoundsEv, @function
_ZNK2v88internal11interpreter21BytecodeArrayAccessor14OffsetInBoundsEv:
.LFB17821:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movl	8(%rdi), %ebx
	testl	%ebx, %ebx
	js	.L99
	movq	(%rdi), %rdi
	leaq	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray6lengthEv(%rip), %rdx
	movq	(%rdi), %rax
	movq	(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L101
	movq	8(%rdi), %rax
	movq	(%rax), %rax
	movl	11(%rax), %eax
.L102:
	cmpl	%eax, %ebx
	setl	%al
.L99:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L101:
	.cfi_restore_state
	call	*%rax
	jmp	.L102
	.cfi_endproc
.LFE17821:
	.size	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14OffsetInBoundsEv, .-_ZNK2v88internal11interpreter21BytecodeArrayAccessor14OffsetInBoundsEv
	.section	.text._ZNK2v88internal11interpreter21BytecodeArrayAccessor16current_bytecodeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11interpreter21BytecodeArrayAccessor16current_bytecodeEv
	.type	_ZNK2v88internal11interpreter21BytecodeArrayAccessor16current_bytecodeEv, @function
_ZNK2v88internal11interpreter21BytecodeArrayAccessor16current_bytecodeEv:
.LFB17822:
	.cfi_startproc
	endbr64
	movq	(%rdi), %r8
	leaq	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray3getEi(%rip), %rdx
	movl	16(%rdi), %esi
	addl	8(%rdi), %esi
	movq	(%r8), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L107
	movq	8(%r8), %rax
	addl	$54, %esi
	movslq	%esi, %rsi
	movq	(%rax), %rax
	movzbl	-1(%rsi,%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L107:
	movq	%r8, %rdi
	jmp	*%rax
	.cfi_endproc
.LFE17822:
	.size	_ZNK2v88internal11interpreter21BytecodeArrayAccessor16current_bytecodeEv, .-_ZNK2v88internal11interpreter21BytecodeArrayAccessor16current_bytecodeEv
	.section	.text._ZNK2v88internal11interpreter21BytecodeArrayAccessor21current_bytecode_sizeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11interpreter21BytecodeArrayAccessor21current_bytecode_sizeEv
	.type	_ZNK2v88internal11interpreter21BytecodeArrayAccessor21current_bytecode_sizeEv, @function
_ZNK2v88internal11interpreter21BytecodeArrayAccessor21current_bytecode_sizeEv:
.LFB17823:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray3getEi(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r8
	movl	16(%rdi), %r12d
	movl	8(%rdi), %esi
	movq	(%r8), %rax
	movzbl	12(%rdi), %ebx
	addl	%r12d, %esi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L111
	movq	8(%r8), %rax
	addl	$54, %esi
	movslq	%esi, %rsi
	movq	(%rax), %rax
	movzbl	-1(%rsi,%rax), %eax
.L112:
	shrq	%rbx
	movzbl	%al, %edx
	andl	$127, %ebx
	imulq	$183, %rbx, %rax
	popq	%rbx
	addq	%rdx, %rax
	leaq	_ZN2v88internal11interpreter9Bytecodes14kBytecodeSizesE(%rip), %rdx
	addl	(%rdx,%rax,4), %r12d
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L111:
	.cfi_restore_state
	movq	%r8, %rdi
	call	*%rax
	jmp	.L112
	.cfi_endproc
.LFE17823:
	.size	_ZNK2v88internal11interpreter21BytecodeArrayAccessor21current_bytecode_sizeEv, .-_ZNK2v88internal11interpreter21BytecodeArrayAccessor21current_bytecode_sizeEv
	.section	.text._ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetUnsignedOperandEiNS1_11OperandTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetUnsignedOperandEiNS1_11OperandTypeE
	.type	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetUnsignedOperandEiNS1_11OperandTypeE, @function
_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetUnsignedOperandEiNS1_11OperandTypeE:
.LFB17824:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray23GetFirstBytecodeAddressEv(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	movq	(%rdi), %rdx
	movq	32(%rdx), %rax
	cmpq	%rcx, %rax
	jne	.L115
	movq	8(%rdi), %rax
	movq	(%rax), %rax
	addq	$53, %rax
.L116:
	movslq	8(%rbx), %r12
	movslq	16(%rbx), %r8
	movzbl	12(%rbx), %r15d
	movq	%r12, %rsi
	addq	%r8, %r12
	addq	%rax, %r12
	movq	16(%rdx), %rax
	leaq	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray3getEi(%rip), %rdx
	addl	%r8d, %esi
	cmpq	%rdx, %rax
	jne	.L117
	movq	8(%rdi), %rax
	addl	$54, %esi
	movslq	%esi, %rsi
	movq	(%rax), %rax
	movzbl	-1(%rsi,%rax), %edi
.L118:
	movl	%r15d, %edx
	movl	%r14d, %esi
	call	_ZN2v88internal11interpreter9Bytecodes16GetOperandOffsetENS1_8BytecodeEiNS1_12OperandScaleE@PLT
	movzbl	12(%rbx), %edx
	addq	$8, %rsp
	movl	%r13d, %esi
	movslq	%eax, %rdi
	popq	%rbx
	addq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter15BytecodeDecoder21DecodeUnsignedOperandEmNS1_11OperandTypeENS1_12OperandScaleE@PLT
	.p2align 4,,10
	.p2align 3
.L115:
	.cfi_restore_state
	call	*%rax
	movq	(%rbx), %rdi
	movq	(%rdi), %rdx
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L117:
	call	*%rax
	movl	%eax, %edi
	jmp	.L118
	.cfi_endproc
.LFE17824:
	.size	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetUnsignedOperandEiNS1_11OperandTypeE, .-_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetUnsignedOperandEiNS1_11OperandTypeE
	.section	.text._ZNK2v88internal11interpreter21BytecodeArrayAccessor16GetSignedOperandEiNS1_11OperandTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11interpreter21BytecodeArrayAccessor16GetSignedOperandEiNS1_11OperandTypeE
	.type	_ZNK2v88internal11interpreter21BytecodeArrayAccessor16GetSignedOperandEiNS1_11OperandTypeE, @function
_ZNK2v88internal11interpreter21BytecodeArrayAccessor16GetSignedOperandEiNS1_11OperandTypeE:
.LFB17825:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray23GetFirstBytecodeAddressEv(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	movq	(%rdi), %rdx
	movq	32(%rdx), %rax
	cmpq	%rcx, %rax
	jne	.L121
	movq	8(%rdi), %rax
	movq	(%rax), %rax
	addq	$53, %rax
.L122:
	movslq	8(%rbx), %r12
	movslq	16(%rbx), %r8
	movzbl	12(%rbx), %r15d
	movq	%r12, %rsi
	addq	%r8, %r12
	addq	%rax, %r12
	movq	16(%rdx), %rax
	leaq	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray3getEi(%rip), %rdx
	addl	%r8d, %esi
	cmpq	%rdx, %rax
	jne	.L123
	movq	8(%rdi), %rax
	addl	$54, %esi
	movslq	%esi, %rsi
	movq	(%rax), %rax
	movzbl	-1(%rsi,%rax), %edi
.L124:
	movl	%r15d, %edx
	movl	%r14d, %esi
	call	_ZN2v88internal11interpreter9Bytecodes16GetOperandOffsetENS1_8BytecodeEiNS1_12OperandScaleE@PLT
	movzbl	12(%rbx), %edx
	addq	$8, %rsp
	movl	%r13d, %esi
	movslq	%eax, %rdi
	popq	%rbx
	addq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter15BytecodeDecoder19DecodeSignedOperandEmNS1_11OperandTypeENS1_12OperandScaleE@PLT
	.p2align 4,,10
	.p2align 3
.L121:
	.cfi_restore_state
	call	*%rax
	movq	(%rbx), %rdi
	movq	(%rdi), %rdx
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L123:
	call	*%rax
	movl	%eax, %edi
	jmp	.L124
	.cfi_endproc
.LFE17825:
	.size	_ZNK2v88internal11interpreter21BytecodeArrayAccessor16GetSignedOperandEiNS1_11OperandTypeE, .-_ZNK2v88internal11interpreter21BytecodeArrayAccessor16GetSignedOperandEiNS1_11OperandTypeE
	.section	.text._ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetFlagOperandEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetFlagOperandEi
	.type	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetFlagOperandEi, @function
_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetFlagOperandEi:
.LFB17826:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray23GetFirstBytecodeAddressEv(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movq	(%rdi), %rdx
	movq	32(%rdx), %rax
	cmpq	%rcx, %rax
	jne	.L127
	movq	8(%rdi), %rax
	movq	(%rax), %rax
	addq	$53, %rax
.L128:
	movslq	8(%rbx), %r12
	movslq	16(%rbx), %r8
	movzbl	12(%rbx), %r14d
	movq	%r12, %rsi
	addq	%r8, %r12
	addq	%rax, %r12
	movq	16(%rdx), %rax
	leaq	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray3getEi(%rip), %rdx
	addl	%r8d, %esi
	cmpq	%rdx, %rax
	jne	.L129
	movq	8(%rdi), %rax
	addl	$54, %esi
	movslq	%esi, %rsi
	movq	(%rax), %rax
	movzbl	-1(%rsi,%rax), %edi
.L130:
	movl	%r14d, %edx
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter9Bytecodes16GetOperandOffsetENS1_8BytecodeEiNS1_12OperandScaleE@PLT
	movzbl	12(%rbx), %edx
	movl	$1, %esi
	popq	%rbx
	movslq	%eax, %rdi
	addq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter15BytecodeDecoder21DecodeUnsignedOperandEmNS1_11OperandTypeENS1_12OperandScaleE@PLT
	.p2align 4,,10
	.p2align 3
.L127:
	.cfi_restore_state
	call	*%rax
	movq	(%rbx), %rdi
	movq	(%rdi), %rdx
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L129:
	call	*%rax
	movl	%eax, %edi
	jmp	.L130
	.cfi_endproc
.LFE17826:
	.size	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetFlagOperandEi, .-_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetFlagOperandEi
	.section	.text._ZNK2v88internal11interpreter21BytecodeArrayAccessor27GetUnsignedImmediateOperandEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11interpreter21BytecodeArrayAccessor27GetUnsignedImmediateOperandEi
	.type	_ZNK2v88internal11interpreter21BytecodeArrayAccessor27GetUnsignedImmediateOperandEi, @function
_ZNK2v88internal11interpreter21BytecodeArrayAccessor27GetUnsignedImmediateOperandEi:
.LFB17827:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray23GetFirstBytecodeAddressEv(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movq	(%rdi), %rdx
	movq	32(%rdx), %rax
	cmpq	%rcx, %rax
	jne	.L133
	movq	8(%rdi), %rax
	movq	(%rax), %rax
	addq	$53, %rax
.L134:
	movslq	8(%rbx), %r12
	movslq	16(%rbx), %r8
	movzbl	12(%rbx), %r14d
	movq	%r12, %rsi
	addq	%r8, %r12
	addq	%rax, %r12
	movq	16(%rdx), %rax
	leaq	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray3getEi(%rip), %rdx
	addl	%r8d, %esi
	cmpq	%rdx, %rax
	jne	.L135
	movq	8(%rdi), %rax
	addl	$54, %esi
	movslq	%esi, %rsi
	movq	(%rax), %rax
	movzbl	-1(%rsi,%rax), %edi
.L136:
	movl	%r14d, %edx
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter9Bytecodes16GetOperandOffsetENS1_8BytecodeEiNS1_12OperandScaleE@PLT
	movzbl	12(%rbx), %edx
	movl	$6, %esi
	popq	%rbx
	movslq	%eax, %rdi
	addq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter15BytecodeDecoder21DecodeUnsignedOperandEmNS1_11OperandTypeENS1_12OperandScaleE@PLT
	.p2align 4,,10
	.p2align 3
.L133:
	.cfi_restore_state
	call	*%rax
	movq	(%rbx), %rdi
	movq	(%rdi), %rdx
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L135:
	call	*%rax
	movl	%eax, %edi
	jmp	.L136
	.cfi_endproc
.LFE17827:
	.size	_ZNK2v88internal11interpreter21BytecodeArrayAccessor27GetUnsignedImmediateOperandEi, .-_ZNK2v88internal11interpreter21BytecodeArrayAccessor27GetUnsignedImmediateOperandEi
	.section	.text._ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetImmediateOperandEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetImmediateOperandEi
	.type	_ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetImmediateOperandEi, @function
_ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetImmediateOperandEi:
.LFB17828:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray23GetFirstBytecodeAddressEv(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movq	(%rdi), %rdx
	movq	32(%rdx), %rax
	cmpq	%rcx, %rax
	jne	.L139
	movq	8(%rdi), %rax
	movq	(%rax), %rax
	addq	$53, %rax
.L140:
	movslq	8(%rbx), %r12
	movslq	16(%rbx), %r8
	movzbl	12(%rbx), %r14d
	movq	%r12, %rsi
	addq	%r8, %r12
	addq	%rax, %r12
	movq	16(%rdx), %rax
	leaq	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray3getEi(%rip), %rdx
	addl	%r8d, %esi
	cmpq	%rdx, %rax
	jne	.L141
	movq	8(%rdi), %rax
	addl	$54, %esi
	movslq	%esi, %rsi
	movq	(%rax), %rax
	movzbl	-1(%rsi,%rax), %edi
.L142:
	movl	%r14d, %edx
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter9Bytecodes16GetOperandOffsetENS1_8BytecodeEiNS1_12OperandScaleE@PLT
	movzbl	12(%rbx), %edx
	movl	$8, %esi
	popq	%rbx
	movslq	%eax, %rdi
	addq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter15BytecodeDecoder19DecodeSignedOperandEmNS1_11OperandTypeENS1_12OperandScaleE@PLT
	.p2align 4,,10
	.p2align 3
.L139:
	.cfi_restore_state
	call	*%rax
	movq	(%rbx), %rdi
	movq	(%rdi), %rdx
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L141:
	call	*%rax
	movl	%eax, %edi
	jmp	.L142
	.cfi_endproc
.LFE17828:
	.size	_ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetImmediateOperandEi, .-_ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetImmediateOperandEi
	.section	.text._ZNK2v88internal11interpreter21BytecodeArrayAccessor23GetRegisterCountOperandEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11interpreter21BytecodeArrayAccessor23GetRegisterCountOperandEi
	.type	_ZNK2v88internal11interpreter21BytecodeArrayAccessor23GetRegisterCountOperandEi, @function
_ZNK2v88internal11interpreter21BytecodeArrayAccessor23GetRegisterCountOperandEi:
.LFB17829:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray23GetFirstBytecodeAddressEv(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movq	(%rdi), %rdx
	movq	32(%rdx), %rax
	cmpq	%rcx, %rax
	jne	.L145
	movq	8(%rdi), %rax
	movq	(%rax), %rax
	addq	$53, %rax
.L146:
	movslq	8(%rbx), %r12
	movslq	16(%rbx), %r8
	movzbl	12(%rbx), %r14d
	movq	%r12, %rsi
	addq	%r8, %r12
	addq	%rax, %r12
	movq	16(%rdx), %rax
	leaq	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray3getEi(%rip), %rdx
	addl	%r8d, %esi
	cmpq	%rdx, %rax
	jne	.L147
	movq	8(%rdi), %rax
	addl	$54, %esi
	movslq	%esi, %rsi
	movq	(%rax), %rax
	movzbl	-1(%rsi,%rax), %edi
.L148:
	movl	%r14d, %edx
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter9Bytecodes16GetOperandOffsetENS1_8BytecodeEiNS1_12OperandScaleE@PLT
	movzbl	12(%rbx), %edx
	movl	$7, %esi
	popq	%rbx
	movslq	%eax, %rdi
	addq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter15BytecodeDecoder21DecodeUnsignedOperandEmNS1_11OperandTypeENS1_12OperandScaleE@PLT
	.p2align 4,,10
	.p2align 3
.L145:
	.cfi_restore_state
	call	*%rax
	movq	(%rbx), %rdi
	movq	(%rdi), %rdx
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L147:
	call	*%rax
	movl	%eax, %edi
	jmp	.L148
	.cfi_endproc
.LFE17829:
	.size	_ZNK2v88internal11interpreter21BytecodeArrayAccessor23GetRegisterCountOperandEi, .-_ZNK2v88internal11interpreter21BytecodeArrayAccessor23GetRegisterCountOperandEi
	.section	.text._ZNK2v88internal11interpreter21BytecodeArrayAccessor15GetIndexOperandEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11interpreter21BytecodeArrayAccessor15GetIndexOperandEi
	.type	_ZNK2v88internal11interpreter21BytecodeArrayAccessor15GetIndexOperandEi, @function
_ZNK2v88internal11interpreter21BytecodeArrayAccessor15GetIndexOperandEi:
.LFB17830:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray3getEi(%rip), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %rdi
	movl	16(%rbx), %esi
	addl	8(%rbx), %esi
	movq	(%rdi), %rcx
	movq	16(%rcx), %rax
	cmpq	%r15, %rax
	jne	.L151
	movq	8(%rdi), %rax
	addl	$54, %esi
	movslq	%esi, %rsi
	movq	(%rax), %rax
	movzbl	-1(%rsi,%rax), %eax
.L152:
	movzbl	%al, %eax
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandTypesE(%rip), %rsi
	movslq	%r14d, %rdx
	movq	(%rsi,%rax,8), %rsi
	movq	32(%rcx), %rax
	movzbl	(%rsi,%rdx), %r13d
	leaq	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray23GetFirstBytecodeAddressEv(%rip), %rdx
	cmpq	%rdx, %rax
	jne	.L153
	movq	8(%rdi), %rax
	movq	(%rax), %rax
	addq	$53, %rax
.L154:
	movslq	16(%rbx), %rdx
	movslq	8(%rbx), %r12
	movq	%r12, %rsi
	addq	%rdx, %r12
	movq	%rdx, %r8
	movzbl	12(%rbx), %edx
	addq	%rax, %r12
	movq	16(%rcx), %rax
	addl	%r8d, %esi
	cmpq	%r15, %rax
	jne	.L155
	movq	8(%rdi), %rax
	addl	$54, %esi
	movslq	%esi, %rsi
	movq	(%rax), %rax
	movzbl	-1(%rsi,%rax), %edi
.L156:
	movl	%r14d, %esi
	call	_ZN2v88internal11interpreter9Bytecodes16GetOperandOffsetENS1_8BytecodeEiNS1_12OperandScaleE@PLT
	movzbl	12(%rbx), %edx
	addq	$24, %rsp
	movl	%r13d, %esi
	movslq	%eax, %rdi
	popq	%rbx
	addq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter15BytecodeDecoder21DecodeUnsignedOperandEmNS1_11OperandTypeENS1_12OperandScaleE@PLT
	.p2align 4,,10
	.p2align 3
.L151:
	.cfi_restore_state
	call	*%rax
	movq	(%rbx), %rdi
	movq	(%rdi), %rcx
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L155:
	movb	%dl, -49(%rbp)
	call	*%rax
	movzbl	-49(%rbp), %edx
	movl	%eax, %edi
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L153:
	call	*%rax
	movq	(%rbx), %rdi
	movq	(%rdi), %rcx
	jmp	.L154
	.cfi_endproc
.LFE17830:
	.size	_ZNK2v88internal11interpreter21BytecodeArrayAccessor15GetIndexOperandEi, .-_ZNK2v88internal11interpreter21BytecodeArrayAccessor15GetIndexOperandEi
	.section	.text._ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi
	.type	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi, @function
_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi:
.LFB17831:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray3getEi(%rip), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %rdi
	movl	16(%rbx), %esi
	addl	8(%rbx), %esi
	movq	(%rdi), %rcx
	movq	16(%rcx), %rax
	cmpq	%r15, %rax
	jne	.L159
	movq	8(%rdi), %rax
	addl	$54, %esi
	movslq	%esi, %rsi
	movq	(%rax), %rax
	movzbl	-1(%rsi,%rax), %eax
.L160:
	movzbl	%al, %eax
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandTypesE(%rip), %rsi
	movslq	%r14d, %rdx
	movq	(%rsi,%rax,8), %rsi
	movq	32(%rcx), %rax
	movzbl	(%rsi,%rdx), %r13d
	leaq	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray23GetFirstBytecodeAddressEv(%rip), %rdx
	cmpq	%rdx, %rax
	jne	.L161
	movq	8(%rdi), %rax
	movq	(%rax), %rax
	addq	$53, %rax
.L162:
	movslq	16(%rbx), %rdx
	movslq	8(%rbx), %r12
	movq	%r12, %rsi
	addq	%rdx, %r12
	movq	%rdx, %r8
	movzbl	12(%rbx), %edx
	addq	%rax, %r12
	movq	16(%rcx), %rax
	addl	%r8d, %esi
	cmpq	%r15, %rax
	jne	.L163
	movq	8(%rdi), %rax
	addl	$54, %esi
	movslq	%esi, %rsi
	movq	(%rax), %rax
	movzbl	-1(%rsi,%rax), %edi
.L164:
	movl	%r14d, %esi
	call	_ZN2v88internal11interpreter9Bytecodes16GetOperandOffsetENS1_8BytecodeEiNS1_12OperandScaleE@PLT
	movzbl	12(%rbx), %edx
	movl	%r13d, %esi
	movslq	%eax, %rdi
	addq	%r12, %rdi
	call	_ZN2v88internal11interpreter15BytecodeDecoder21DecodeUnsignedOperandEmNS1_11OperandTypeENS1_12OperandScaleE@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L159:
	.cfi_restore_state
	call	*%rax
	movq	(%rbx), %rdi
	movq	(%rdi), %rcx
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L163:
	movb	%dl, -49(%rbp)
	call	*%rax
	movzbl	-49(%rbp), %edx
	movl	%eax, %edi
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L161:
	call	*%rax
	movq	(%rbx), %rdi
	movq	(%rdi), %rcx
	jmp	.L162
	.cfi_endproc
.LFE17831:
	.size	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi, .-_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi
	.section	.text._ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi
	.type	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi, @function
_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi:
.LFB17832:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray3getEi(%rip), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %rdi
	movl	16(%rbx), %esi
	addl	8(%rbx), %esi
	movq	(%rdi), %rcx
	movq	16(%rcx), %rax
	cmpq	%r15, %rax
	jne	.L167
	movq	8(%rdi), %rax
	addl	$54, %esi
	movslq	%esi, %rsi
	movq	(%rax), %rax
	movzbl	-1(%rsi,%rax), %eax
.L168:
	movzbl	%al, %eax
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandTypesE(%rip), %rsi
	movslq	%r14d, %rdx
	movq	(%rsi,%rax,8), %rsi
	movq	32(%rcx), %rax
	movzbl	(%rsi,%rdx), %r13d
	leaq	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray23GetFirstBytecodeAddressEv(%rip), %rdx
	cmpq	%rdx, %rax
	jne	.L169
	movq	8(%rdi), %rax
	movq	(%rax), %rax
	addq	$53, %rax
.L170:
	movslq	16(%rbx), %rdx
	movslq	8(%rbx), %r12
	movq	%r12, %rsi
	addq	%rdx, %r12
	movq	%rdx, %r8
	movzbl	12(%rbx), %edx
	addq	%rax, %r12
	movq	16(%rcx), %rax
	addl	%r8d, %esi
	cmpq	%r15, %rax
	jne	.L171
	movq	8(%rdi), %rax
	addl	$54, %esi
	movslq	%esi, %rsi
	movq	(%rax), %rax
	movzbl	-1(%rsi,%rax), %edi
.L172:
	movl	%r14d, %esi
	call	_ZN2v88internal11interpreter9Bytecodes16GetOperandOffsetENS1_8BytecodeEiNS1_12OperandScaleE@PLT
	movzbl	12(%rbx), %edx
	addq	$24, %rsp
	movl	%r13d, %esi
	movslq	%eax, %rdi
	popq	%rbx
	addq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter15BytecodeDecoder21DecodeRegisterOperandEmNS1_11OperandTypeENS1_12OperandScaleE@PLT
	.p2align 4,,10
	.p2align 3
.L167:
	.cfi_restore_state
	call	*%rax
	movq	(%rbx), %rdi
	movq	(%rdi), %rcx
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L171:
	movb	%dl, -49(%rbp)
	call	*%rax
	movzbl	-49(%rbp), %edx
	movl	%eax, %edi
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L169:
	call	*%rax
	movq	(%rbx), %rdi
	movq	(%rdi), %rcx
	jmp	.L170
	.cfi_endproc
.LFE17832:
	.size	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi, .-_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi
	.section	.text._ZNK2v88internal11interpreter21BytecodeArrayAccessor23GetRegisterOperandRangeEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11interpreter21BytecodeArrayAccessor23GetRegisterOperandRangeEi
	.type	_ZNK2v88internal11interpreter21BytecodeArrayAccessor23GetRegisterOperandRangeEi, @function
_ZNK2v88internal11interpreter21BytecodeArrayAccessor23GetRegisterOperandRangeEi:
.LFB17833:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray3getEi(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rdi), %rdi
	movl	%esi, %ebx
	movl	16(%r12), %esi
	addl	8(%r12), %esi
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L175
	movq	8(%rdi), %rax
	addl	$54, %esi
	movslq	%esi, %rsi
	movq	(%rax), %rax
	movzbl	-1(%rsi,%rax), %eax
.L176:
	movzbl	%al, %eax
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandTypesE(%rip), %rcx
	movslq	%ebx, %rdx
	movq	(%rcx,%rax,8), %rcx
	movzbl	(%rcx,%rdx), %eax
	cmpb	$10, %al
	je	.L186
	cmpb	$13, %al
	je	.L186
	subl	$9, %eax
	cmpb	$6, %al
	ja	.L179
	leaq	.L181(%rip), %rdx
	movzbl	%al, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZNK2v88internal11interpreter21BytecodeArrayAccessor23GetRegisterOperandRangeEi,"a",@progbits
	.align 4
	.align 4
.L181:
	.long	.L185-.L181
	.long	.L179-.L181
	.long	.L182-.L181
	.long	.L185-.L181
	.long	.L183-.L181
	.long	.L182-.L181
	.long	.L180-.L181
	.section	.text._ZNK2v88internal11interpreter21BytecodeArrayAccessor23GetRegisterOperandRangeEi
.L182:
	popq	%rbx
	movl	$2, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L185:
	.cfi_restore_state
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L186:
	.cfi_restore_state
	leal	1(%rbx), %esi
	movq	%r12, %rdi
	popq	%rbx
	movl	$7, %edx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetUnsignedOperandEiNS1_11OperandTypeE
	.p2align 4,,10
	.p2align 3
.L175:
	.cfi_restore_state
	call	*%rax
	jmp	.L176
.L180:
	popq	%rbx
	movl	$3, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L179:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L183:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE17833:
	.size	_ZNK2v88internal11interpreter21BytecodeArrayAccessor23GetRegisterOperandRangeEi, .-_ZNK2v88internal11interpreter21BytecodeArrayAccessor23GetRegisterOperandRangeEi
	.section	.text._ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetRuntimeIdOperandEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetRuntimeIdOperandEi
	.type	_ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetRuntimeIdOperandEi, @function
_ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetRuntimeIdOperandEi:
.LFB17834:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray3getEi(%rip), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %rdi
	movl	16(%rbx), %esi
	addl	8(%rbx), %esi
	movq	(%rdi), %rcx
	movq	16(%rcx), %rax
	cmpq	%r15, %rax
	jne	.L189
	movq	8(%rdi), %rax
	addl	$54, %esi
	movslq	%esi, %rsi
	movq	(%rax), %rax
	movzbl	-1(%rsi,%rax), %eax
.L190:
	movzbl	%al, %eax
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandTypesE(%rip), %rsi
	movslq	%r14d, %rdx
	movq	(%rsi,%rax,8), %rsi
	movq	32(%rcx), %rax
	movzbl	(%rsi,%rdx), %r13d
	leaq	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray23GetFirstBytecodeAddressEv(%rip), %rdx
	cmpq	%rdx, %rax
	jne	.L191
	movq	8(%rdi), %rax
	movq	(%rax), %rax
	addq	$53, %rax
.L192:
	movslq	16(%rbx), %rdx
	movslq	8(%rbx), %r12
	movq	%r12, %rsi
	addq	%rdx, %r12
	movq	%rdx, %r8
	movzbl	12(%rbx), %edx
	addq	%rax, %r12
	movq	16(%rcx), %rax
	addl	%r8d, %esi
	cmpq	%r15, %rax
	jne	.L193
	movq	8(%rdi), %rax
	addl	$54, %esi
	movslq	%esi, %rsi
	movq	(%rax), %rax
	movzbl	-1(%rsi,%rax), %edi
.L194:
	movl	%r14d, %esi
	call	_ZN2v88internal11interpreter9Bytecodes16GetOperandOffsetENS1_8BytecodeEiNS1_12OperandScaleE@PLT
	movzbl	12(%rbx), %edx
	addq	$24, %rsp
	movl	%r13d, %esi
	movslq	%eax, %rdi
	popq	%rbx
	addq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter15BytecodeDecoder21DecodeUnsignedOperandEmNS1_11OperandTypeENS1_12OperandScaleE@PLT
	.p2align 4,,10
	.p2align 3
.L189:
	.cfi_restore_state
	call	*%rax
	movq	(%rbx), %rdi
	movq	(%rdi), %rcx
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L193:
	movb	%dl, -49(%rbp)
	call	*%rax
	movzbl	-49(%rbp), %edx
	movl	%eax, %edi
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L191:
	call	*%rax
	movq	(%rbx), %rdi
	movq	(%rdi), %rcx
	jmp	.L192
	.cfi_endproc
.LFE17834:
	.size	_ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetRuntimeIdOperandEi, .-_ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetRuntimeIdOperandEi
	.section	.text._ZNK2v88internal11interpreter21BytecodeArrayAccessor28GetNativeContextIndexOperandEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11interpreter21BytecodeArrayAccessor28GetNativeContextIndexOperandEi
	.type	_ZNK2v88internal11interpreter21BytecodeArrayAccessor28GetNativeContextIndexOperandEi, @function
_ZNK2v88internal11interpreter21BytecodeArrayAccessor28GetNativeContextIndexOperandEi:
.LFB21703:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray3getEi(%rip), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %rdi
	movl	16(%rbx), %esi
	addl	8(%rbx), %esi
	movq	(%rdi), %rcx
	movq	16(%rcx), %rax
	cmpq	%r15, %rax
	jne	.L197
	movq	8(%rdi), %rax
	addl	$54, %esi
	movslq	%esi, %rsi
	movq	(%rax), %rax
	movzbl	-1(%rsi,%rax), %eax
.L198:
	movzbl	%al, %eax
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandTypesE(%rip), %rsi
	movslq	%r14d, %rdx
	movq	(%rsi,%rax,8), %rsi
	movq	32(%rcx), %rax
	movzbl	(%rsi,%rdx), %r13d
	leaq	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray23GetFirstBytecodeAddressEv(%rip), %rdx
	cmpq	%rdx, %rax
	jne	.L199
	movq	8(%rdi), %rax
	movq	(%rax), %rax
	addq	$53, %rax
.L200:
	movslq	16(%rbx), %rdx
	movslq	8(%rbx), %r12
	movq	%r12, %rsi
	addq	%rdx, %r12
	movq	%rdx, %r8
	movzbl	12(%rbx), %edx
	addq	%rax, %r12
	movq	16(%rcx), %rax
	addl	%r8d, %esi
	cmpq	%r15, %rax
	jne	.L201
	movq	8(%rdi), %rax
	addl	$54, %esi
	movslq	%esi, %rsi
	movq	(%rax), %rax
	movzbl	-1(%rsi,%rax), %edi
.L202:
	movl	%r14d, %esi
	call	_ZN2v88internal11interpreter9Bytecodes16GetOperandOffsetENS1_8BytecodeEiNS1_12OperandScaleE@PLT
	movzbl	12(%rbx), %edx
	addq	$24, %rsp
	movl	%r13d, %esi
	movslq	%eax, %rdi
	popq	%rbx
	addq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter15BytecodeDecoder21DecodeUnsignedOperandEmNS1_11OperandTypeENS1_12OperandScaleE@PLT
	.p2align 4,,10
	.p2align 3
.L197:
	.cfi_restore_state
	call	*%rax
	movq	(%rbx), %rdi
	movq	(%rdi), %rcx
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L201:
	movb	%dl, -49(%rbp)
	call	*%rax
	movzbl	-49(%rbp), %edx
	movl	%eax, %edi
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L199:
	call	*%rax
	movq	(%rbx), %rdi
	movq	(%rdi), %rcx
	jmp	.L200
	.cfi_endproc
.LFE21703:
	.size	_ZNK2v88internal11interpreter21BytecodeArrayAccessor28GetNativeContextIndexOperandEi, .-_ZNK2v88internal11interpreter21BytecodeArrayAccessor28GetNativeContextIndexOperandEi
	.section	.text._ZNK2v88internal11interpreter21BytecodeArrayAccessor21GetIntrinsicIdOperandEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11interpreter21BytecodeArrayAccessor21GetIntrinsicIdOperandEi
	.type	_ZNK2v88internal11interpreter21BytecodeArrayAccessor21GetIntrinsicIdOperandEi, @function
_ZNK2v88internal11interpreter21BytecodeArrayAccessor21GetIntrinsicIdOperandEi:
.LFB17836:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray3getEi(%rip), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %rdi
	movl	16(%rbx), %esi
	addl	8(%rbx), %esi
	movq	(%rdi), %rcx
	movq	16(%rcx), %rax
	cmpq	%r15, %rax
	jne	.L205
	movq	8(%rdi), %rax
	addl	$54, %esi
	movslq	%esi, %rsi
	movq	(%rax), %rax
	movzbl	-1(%rsi,%rax), %eax
.L206:
	movzbl	%al, %eax
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandTypesE(%rip), %rsi
	movslq	%r14d, %rdx
	movq	(%rsi,%rax,8), %rsi
	movq	32(%rcx), %rax
	movzbl	(%rsi,%rdx), %r13d
	leaq	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray23GetFirstBytecodeAddressEv(%rip), %rdx
	cmpq	%rdx, %rax
	jne	.L207
	movq	8(%rdi), %rax
	movq	(%rax), %rax
	addq	$53, %rax
.L208:
	movslq	16(%rbx), %rdx
	movslq	8(%rbx), %r12
	movq	%r12, %rsi
	addq	%rdx, %r12
	movq	%rdx, %r8
	movzbl	12(%rbx), %edx
	addq	%rax, %r12
	movq	16(%rcx), %rax
	addl	%r8d, %esi
	cmpq	%r15, %rax
	jne	.L209
	movq	8(%rdi), %rax
	addl	$54, %esi
	movslq	%esi, %rsi
	movq	(%rax), %rax
	movzbl	-1(%rsi,%rax), %edi
.L210:
	movl	%r14d, %esi
	call	_ZN2v88internal11interpreter9Bytecodes16GetOperandOffsetENS1_8BytecodeEiNS1_12OperandScaleE@PLT
	movzbl	12(%rbx), %edx
	movl	%r13d, %esi
	movslq	%eax, %rdi
	addq	%r12, %rdi
	call	_ZN2v88internal11interpreter15BytecodeDecoder21DecodeUnsignedOperandEmNS1_11OperandTypeENS1_12OperandScaleE@PLT
	addq	$24, %rsp
	popq	%rbx
	movl	%eax, %edi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter16IntrinsicsHelper11ToRuntimeIdENS2_11IntrinsicIdE@PLT
	.p2align 4,,10
	.p2align 3
.L205:
	.cfi_restore_state
	call	*%rax
	movq	(%rbx), %rdi
	movq	(%rdi), %rcx
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L209:
	movb	%dl, -49(%rbp)
	call	*%rax
	movzbl	-49(%rbp), %edx
	movl	%eax, %edi
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L207:
	call	*%rax
	movq	(%rbx), %rdi
	movq	(%rdi), %rcx
	jmp	.L208
	.cfi_endproc
.LFE17836:
	.size	_ZNK2v88internal11interpreter21BytecodeArrayAccessor21GetIntrinsicIdOperandEi, .-_ZNK2v88internal11interpreter21BytecodeArrayAccessor21GetIntrinsicIdOperandEi
	.section	.text._ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetConstantAtIndexEiPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetConstantAtIndexEiPNS0_7IsolateE
	.type	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetConstantAtIndexEiPNS0_7IsolateE, @function
_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetConstantAtIndexEiPNS0_7IsolateE:
.LFB17837:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	leaq	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray18GetConstantAtIndexEiPNS0_7IsolateE(%rip), %rcx
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L213
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	8(%rdi), %rax
	movq	(%rax), %rcx
	leal	16(,%rsi,8), %eax
	cltq
	movq	15(%rcx), %rcx
	movq	-1(%rax,%rcx), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L214
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L214:
	.cfi_restore_state
	movq	41088(%rdx), %rax
	cmpq	41096(%rdx), %rax
	je	.L222
.L216:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%rdx)
	movq	%rsi, (%rax)
	leave
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L213:
	.cfi_restore 6
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L222:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	movq	%rdx, %rdi
	movq	%rsi, -16(%rbp)
	movq	%rdx, -8(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-16(%rbp), %rsi
	movq	-8(%rbp), %rdx
	jmp	.L216
	.cfi_endproc
.LFE17837:
	.size	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetConstantAtIndexEiPNS0_7IsolateE, .-_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetConstantAtIndexEiPNS0_7IsolateE
	.section	.text._ZNK2v88internal11interpreter21BytecodeArrayAccessor20IsConstantAtIndexSmiEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11interpreter21BytecodeArrayAccessor20IsConstantAtIndexSmiEi
	.type	_ZNK2v88internal11interpreter21BytecodeArrayAccessor20IsConstantAtIndexSmiEi, @function
_ZNK2v88internal11interpreter21BytecodeArrayAccessor20IsConstantAtIndexSmiEi:
.LFB17838:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	leaq	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray20IsConstantAtIndexSmiEi(%rip), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L224
	movq	8(%rdi), %rax
	movq	(%rax), %rdx
	leal	16(,%rsi,8), %eax
	cltq
	movq	15(%rdx), %rdx
	movq	-1(%rax,%rdx), %rax
	notq	%rax
	andl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L224:
	jmp	*%rax
	.cfi_endproc
.LFE17838:
	.size	_ZNK2v88internal11interpreter21BytecodeArrayAccessor20IsConstantAtIndexSmiEi, .-_ZNK2v88internal11interpreter21BytecodeArrayAccessor20IsConstantAtIndexSmiEi
	.section	.text._ZNK2v88internal11interpreter21BytecodeArrayAccessor23GetConstantAtIndexAsSmiEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11interpreter21BytecodeArrayAccessor23GetConstantAtIndexAsSmiEi
	.type	_ZNK2v88internal11interpreter21BytecodeArrayAccessor23GetConstantAtIndexAsSmiEi, @function
_ZNK2v88internal11interpreter21BytecodeArrayAccessor23GetConstantAtIndexAsSmiEi:
.LFB17839:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	leaq	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray23GetConstantAtIndexAsSmiEi(%rip), %rdx
	movq	(%rdi), %rax
	movq	56(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L228
	movq	8(%rdi), %rax
	movq	(%rax), %rdx
	leal	16(,%rsi,8), %eax
	cltq
	movq	15(%rdx), %rdx
	movq	-1(%rax,%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L228:
	jmp	*%rax
	.cfi_endproc
.LFE17839:
	.size	_ZNK2v88internal11interpreter21BytecodeArrayAccessor23GetConstantAtIndexAsSmiEi, .-_ZNK2v88internal11interpreter21BytecodeArrayAccessor23GetConstantAtIndexAsSmiEi
	.section	.text._ZNK2v88internal11interpreter21BytecodeArrayAccessor26GetConstantForIndexOperandEiPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11interpreter21BytecodeArrayAccessor26GetConstantForIndexOperandEiPNS0_7IsolateE
	.type	_ZNK2v88internal11interpreter21BytecodeArrayAccessor26GetConstantForIndexOperandEiPNS0_7IsolateE, @function
_ZNK2v88internal11interpreter21BytecodeArrayAccessor26GetConstantForIndexOperandEiPNS0_7IsolateE:
.LFB17840:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	leaq	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray3getEi(%rip), %rdx
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %rdi
	movl	16(%rbx), %esi
	addl	8(%rbx), %esi
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L232
	movq	8(%rdi), %rax
	addl	$54, %esi
	movslq	%esi, %rsi
	movq	(%rax), %rax
	movzbl	-1(%rsi,%rax), %eax
.L233:
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandTypesE(%rip), %rcx
	movzbl	%al, %eax
	movslq	%r13d, %rdx
	movl	%r13d, %esi
	movq	(%rcx,%rax,8), %rax
	movq	%rbx, %rdi
	movzbl	(%rax,%rdx), %edx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetUnsignedOperandEiNS1_11OperandTypeE
	movq	(%rbx), %rdi
	leaq	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray18GetConstantAtIndexEiPNS0_7IsolateE(%rip), %rdx
	movl	%eax, %esi
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L234
	movq	8(%rdi), %rax
	movq	(%rax), %rdx
	leal	16(,%rsi,8), %eax
	cltq
	movq	15(%rdx), %rdx
	movq	-1(%rax,%rdx), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L235
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L235:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L240
.L237:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L234:
	.cfi_restore_state
	movq	%r12, %rdx
	call	*%rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L232:
	.cfi_restore_state
	call	*%rax
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L240:
	movq	%r12, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L237
	.cfi_endproc
.LFE17840:
	.size	_ZNK2v88internal11interpreter21BytecodeArrayAccessor26GetConstantForIndexOperandEiPNS0_7IsolateE, .-_ZNK2v88internal11interpreter21BytecodeArrayAccessor26GetConstantForIndexOperandEiPNS0_7IsolateE
	.section	.text._ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetJumpTargetOffsetEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetJumpTargetOffsetEv
	.type	_ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetJumpTargetOffsetEv, @function
_ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetJumpTargetOffsetEv:
.LFB17841:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray3getEi(%rip), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	movl	16(%rbx), %esi
	addl	8(%rbx), %esi
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	cmpq	%r13, %rax
	jne	.L242
	movq	8(%rdi), %rax
	addl	$54, %esi
	movslq	%esi, %rsi
	movq	(%rax), %rax
	movzbl	-1(%rsi,%rax), %r12d
.L243:
	leal	118(%r12), %eax
	cmpb	$1, %al
	jbe	.L244
	leal	105(%r12), %eax
	cmpb	$9, %al
	jbe	.L245
	addl	$116, %r12d
	cmpb	$10, %r12b
	ja	.L257
	movq	(%rbx), %rdi
	movl	16(%rbx), %esi
	addl	8(%rbx), %esi
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	cmpq	%r13, %rax
	jne	.L250
	movq	8(%rdi), %rax
	addl	$54, %esi
	movslq	%esi, %rsi
	movq	(%rax), %rax
	movzbl	-1(%rsi,%rax), %eax
.L251:
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandTypesE(%rip), %rdx
	movzbl	%al, %eax
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	(%rdx,%rax,8), %rax
	movzbl	(%rax), %edx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetUnsignedOperandEiNS1_11OperandTypeE
	movq	(%rbx), %rdi
	leaq	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray23GetConstantAtIndexAsSmiEi(%rip), %rdx
	movl	%eax, %esi
	movq	(%rdi), %rax
	movq	56(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L252
	movq	8(%rdi), %rax
	movq	(%rax), %rdx
	leal	16(,%rsi,8), %eax
	cltq
	movq	15(%rdx), %rdx
	movq	-1(%rax,%rdx), %rax
.L253:
	sarq	$32, %rax
.L256:
	addl	8(%rbx), %eax
	addl	16(%rbx), %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L245:
	.cfi_restore_state
	movl	$6, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetUnsignedOperandEiNS1_11OperandTypeE
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L244:
	movl	$6, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetUnsignedOperandEiNS1_11OperandTypeE
	movl	%eax, %edx
	negl	%edx
	cmpb	$-118, %r12b
	cmove	%edx, %eax
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L242:
	call	*%rax
	movl	%eax, %r12d
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L250:
	call	*%rax
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L252:
	call	*%rax
	jmp	.L253
.L257:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE17841:
	.size	_ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetJumpTargetOffsetEv, .-_ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetJumpTargetOffsetEv
	.section	.text._ZNK2v88internal11interpreter21BytecodeArrayAccessor25GetJumpTableTargetOffsetsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11interpreter21BytecodeArrayAccessor25GetJumpTableTargetOffsetsEv
	.type	_ZNK2v88internal11interpreter21BytecodeArrayAccessor25GetJumpTableTargetOffsetsEv, @function
_ZNK2v88internal11interpreter21BytecodeArrayAccessor25GetJumpTableTargetOffsetsEv:
.LFB17842:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray3getEi(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rsi), %rdi
	movq	%rsi, %rbx
	movl	16(%rsi), %esi
	addl	8(%rbx), %esi
	movq	(%rdi), %rax
	movq	16(%rax), %rdx
	cmpq	%r13, %rdx
	jne	.L259
	movq	8(%rdi), %rcx
	leal	54(%rsi), %eax
	cltq
	movq	(%rcx), %rcx
	movzbl	-1(%rax,%rcx), %eax
.L260:
	cmpb	$-81, %al
	je	.L268
	cmpq	%r13, %rdx
	jne	.L265
	movq	8(%rdi), %rax
	addl	$54, %esi
	movslq	%esi, %rsi
	movq	(%rax), %rax
	movzbl	-1(%rsi,%rax), %eax
.L266:
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandTypesE(%rip), %rdx
	movzbl	%al, %eax
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	(%rdx,%rax,8), %rax
	movzbl	(%rax), %edx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetUnsignedOperandEiNS1_11OperandTypeE
	movl	$6, %edx
	movl	$1, %esi
	movq	%rbx, %rdi
	movl	%eax, %r14d
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetUnsignedOperandEiNS1_11OperandTypeE
	movl	$8, %edx
	movl	$2, %esi
	movq	%rbx, %rdi
	movl	%eax, %r13d
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor16GetSignedOperandEiNS1_11OperandTypeE
.L264:
	movq	%rbx, (%r12)
	popq	%rbx
	movl	%r14d, 8(%r12)
	movl	%r13d, 12(%r12)
	movl	%eax, 16(%r12)
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L268:
	.cfi_restore_state
	cmpq	%r13, %rdx
	jne	.L262
	movq	8(%rdi), %rax
	addl	$54, %esi
	movslq	%esi, %rsi
	movq	(%rax), %rax
	movzbl	-1(%rsi,%rax), %eax
.L263:
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandTypesE(%rip), %rdx
	movzbl	%al, %eax
	movl	$1, %esi
	movq	%rbx, %rdi
	movq	(%rdx,%rax,8), %rax
	movzbl	1(%rax), %edx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetUnsignedOperandEiNS1_11OperandTypeE
	movl	$6, %edx
	movl	$2, %esi
	movq	%rbx, %rdi
	movl	%eax, %r14d
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetUnsignedOperandEiNS1_11OperandTypeE
	movl	%eax, %r13d
	xorl	%eax, %eax
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L259:
	call	*%rdx
	movq	(%rbx), %rdi
	movl	16(%rbx), %esi
	addl	8(%rbx), %esi
	movq	(%rdi), %rdx
	movq	16(%rdx), %rdx
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L265:
	call	*%rdx
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L262:
	call	*%rdx
	jmp	.L263
	.cfi_endproc
.LFE17842:
	.size	_ZNK2v88internal11interpreter21BytecodeArrayAccessor25GetJumpTableTargetOffsetsEv, .-_ZNK2v88internal11interpreter21BytecodeArrayAccessor25GetJumpTableTargetOffsetsEv
	.section	.text._ZNK2v88internal11interpreter21BytecodeArrayAccessor17GetAbsoluteOffsetEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11interpreter21BytecodeArrayAccessor17GetAbsoluteOffsetEi
	.type	_ZNK2v88internal11interpreter21BytecodeArrayAccessor17GetAbsoluteOffsetEi, @function
_ZNK2v88internal11interpreter21BytecodeArrayAccessor17GetAbsoluteOffsetEi:
.LFB17843:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	addl	%esi, %eax
	addl	16(%rdi), %eax
	ret
	.cfi_endproc
.LFE17843:
	.size	_ZNK2v88internal11interpreter21BytecodeArrayAccessor17GetAbsoluteOffsetEi, .-_ZNK2v88internal11interpreter21BytecodeArrayAccessor17GetAbsoluteOffsetEi
	.section	.text._ZNK2v88internal11interpreter21BytecodeArrayAccessor20OffsetWithinBytecodeEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11interpreter21BytecodeArrayAccessor20OffsetWithinBytecodeEi
	.type	_ZNK2v88internal11interpreter21BytecodeArrayAccessor20OffsetWithinBytecodeEi, @function
_ZNK2v88internal11interpreter21BytecodeArrayAccessor20OffsetWithinBytecodeEi:
.LFB17844:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	8(%rdi), %r14d
	cmpl	%r14d, %esi
	jl	.L270
	movl	16(%rdi), %r13d
	movzbl	12(%rdi), %ebx
	leaq	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray3getEi(%rip), %rdx
	movl	%esi, %r12d
	movq	(%rdi), %rdi
	leal	0(%r13,%r14), %esi
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L272
	movq	8(%rdi), %rax
	addl	$54, %esi
	movslq	%esi, %rsi
	movq	(%rax), %rax
	movzbl	-1(%rsi,%rax), %eax
.L273:
	shrq	%rbx
	movzbl	%al, %edx
	andl	$127, %ebx
	imulq	$183, %rbx, %rax
	addq	%rdx, %rax
	leaq	_ZN2v88internal11interpreter9Bytecodes14kBytecodeSizesE(%rip), %rdx
	addl	(%rdx,%rax,4), %r13d
	addl	%r14d, %r13d
	cmpl	%r12d, %r13d
	setg	%al
.L270:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L272:
	.cfi_restore_state
	call	*%rax
	jmp	.L273
	.cfi_endproc
.LFE17844:
	.size	_ZNK2v88internal11interpreter21BytecodeArrayAccessor20OffsetWithinBytecodeEi, .-_ZNK2v88internal11interpreter21BytecodeArrayAccessor20OffsetWithinBytecodeEi
	.section	.text._ZNK2v88internal11interpreter21BytecodeArrayAccessor7PrintToERSo,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11interpreter21BytecodeArrayAccessor7PrintToERSo
	.type	_ZNK2v88internal11interpreter21BytecodeArrayAccessor7PrintToERSo, @function
_ZNK2v88internal11interpreter21BytecodeArrayAccessor7PrintToERSo:
.LFB17845:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray23GetFirstBytecodeAddressEv(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	(%rdi), %rdi
	movq	(%rdi), %rdx
	movq	32(%rdx), %rax
	cmpq	%rcx, %rax
	jne	.L278
	movq	8(%rdi), %rax
	movq	(%rax), %rsi
	leaq	53(%rsi), %rax
.L279:
	movslq	8(%rbx), %rsi
	addq	%rax, %rsi
	movq	8(%rdx), %rax
	leaq	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray15parameter_countEv(%rip), %rdx
	cmpq	%rdx, %rax
	jne	.L280
	movq	8(%rdi), %rax
	movq	%r12, %rdi
	movq	(%rax), %rax
	movl	43(%rax), %edx
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	sarl	$3, %edx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi@PLT
	.p2align 4,,10
	.p2align 3
.L278:
	.cfi_restore_state
	call	*%rax
	movq	(%rbx), %rdi
	movq	(%rdi), %rdx
	jmp	.L279
	.p2align 4,,10
	.p2align 3
.L280:
	movq	%rsi, -24(%rbp)
	call	*%rax
	movq	-24(%rbp), %rsi
	addq	$16, %rsp
	movq	%r12, %rdi
	popq	%rbx
	movl	%eax, %edx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi@PLT
	.cfi_endproc
.LFE17845:
	.size	_ZNK2v88internal11interpreter21BytecodeArrayAccessor7PrintToERSo, .-_ZNK2v88internal11interpreter21BytecodeArrayAccessor7PrintToERSo
	.section	.text._ZN2v88internal11interpreter22JumpTableTargetOffsetsC2EPKNS1_21BytecodeArrayAccessorEiii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter22JumpTableTargetOffsetsC2EPKNS1_21BytecodeArrayAccessorEiii
	.type	_ZN2v88internal11interpreter22JumpTableTargetOffsetsC2EPKNS1_21BytecodeArrayAccessorEiii, @function
_ZN2v88internal11interpreter22JumpTableTargetOffsetsC2EPKNS1_21BytecodeArrayAccessorEiii:
.LFB17847:
	.cfi_startproc
	endbr64
	movq	%rsi, (%rdi)
	movl	%edx, 8(%rdi)
	movl	%ecx, 12(%rdi)
	movl	%r8d, 16(%rdi)
	ret
	.cfi_endproc
.LFE17847:
	.size	_ZN2v88internal11interpreter22JumpTableTargetOffsetsC2EPKNS1_21BytecodeArrayAccessorEiii, .-_ZN2v88internal11interpreter22JumpTableTargetOffsetsC2EPKNS1_21BytecodeArrayAccessorEiii
	.globl	_ZN2v88internal11interpreter22JumpTableTargetOffsetsC1EPKNS1_21BytecodeArrayAccessorEiii
	.set	_ZN2v88internal11interpreter22JumpTableTargetOffsetsC1EPKNS1_21BytecodeArrayAccessorEiii,_ZN2v88internal11interpreter22JumpTableTargetOffsetsC2EPKNS1_21BytecodeArrayAccessorEiii
	.section	.text._ZN2v88internal11interpreter22JumpTableTargetOffsets8iteratordeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter22JumpTableTargetOffsets8iteratordeEv
	.type	_ZN2v88internal11interpreter22JumpTableTargetOffsets8iteratordeEv, @function
_ZN2v88internal11interpreter22JumpTableTargetOffsets8iteratordeEv:
.LFB17855:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movl	8(%rdx), %eax
	addl	12(%rdi), %eax
	addl	16(%rdx), %eax
	movl	16(%rdi), %edx
	salq	$32, %rax
	orq	%rdx, %rax
	ret
	.cfi_endproc
.LFE17855:
	.size	_ZN2v88internal11interpreter22JumpTableTargetOffsets8iteratordeEv, .-_ZN2v88internal11interpreter22JumpTableTargetOffsets8iteratordeEv
	.section	.text._ZN2v88internal11interpreter22JumpTableTargetOffsets8iteratorneERKS3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter22JumpTableTargetOffsets8iteratorneERKS3_
	.type	_ZN2v88internal11interpreter22JumpTableTargetOffsets8iteratorneERKS3_, @function
_ZN2v88internal11interpreter22JumpTableTargetOffsets8iteratorneERKS3_:
.LFB17857:
	.cfi_startproc
	endbr64
	movl	16(%rsi), %eax
	cmpl	%eax, 16(%rdi)
	setne	%al
	ret
	.cfi_endproc
.LFE17857:
	.size	_ZN2v88internal11interpreter22JumpTableTargetOffsets8iteratorneERKS3_, .-_ZN2v88internal11interpreter22JumpTableTargetOffsets8iteratorneERKS3_
	.section	.text._ZN2v88internal11interpreter22JumpTableTargetOffsets8iterator23UpdateAndAdvanceToValidEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter22JumpTableTargetOffsets8iterator23UpdateAndAdvanceToValidEv
	.type	_ZN2v88internal11interpreter22JumpTableTargetOffsets8iterator23UpdateAndAdvanceToValidEv, @function
_ZN2v88internal11interpreter22JumpTableTargetOffsets8iterator23UpdateAndAdvanceToValidEv:
.LFB17858:
	.cfi_startproc
	endbr64
	movl	20(%rdi), %esi
	cmpl	%esi, 24(%rdi)
	jle	.L298
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray20IsConstantAtIndexSmiEi(%rip), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	jmp	.L292
	.p2align 4,,10
	.p2align 3
.L302:
	movq	8(%rdi), %rax
	movq	(%rax), %rdx
	leal	16(,%rsi,8), %eax
	cltq
	movq	15(%rdx), %rdx
	movq	-1(%rax,%rdx), %rax
	movl	24(%rbx), %edx
	movl	20(%rbx), %esi
	notq	%rax
	andl	$1, %eax
	testb	%al, %al
	jne	.L301
.L290:
	addl	$1, %esi
	addl	$1, 16(%rbx)
	movl	%esi, 20(%rbx)
	cmpl	%edx, %esi
	jge	.L286
.L292:
	movq	(%rbx), %rax
	movq	(%rax), %rdi
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%r12, %rax
	je	.L302
	call	*%rax
	movl	24(%rbx), %edx
	movl	20(%rbx), %esi
	testb	%al, %al
	je	.L290
	.p2align 4,,10
	.p2align 3
.L301:
	cmpl	%esi, %edx
	jle	.L286
	movq	(%rbx), %rax
	leaq	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray23GetConstantAtIndexAsSmiEi(%rip), %rdx
	movq	(%rax), %rdi
	movq	(%rdi), %rax
	movq	56(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L294
	movq	8(%rdi), %rax
	movq	(%rax), %rdx
	leal	16(,%rsi,8), %eax
	cltq
	movq	15(%rdx), %rdx
	movq	-1(%rax,%rdx), %rax
.L295:
	movq	%rax, 8(%rbx)
.L286:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L294:
	.cfi_restore_state
	call	*%rax
	jmp	.L295
.L298:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE17858:
	.size	_ZN2v88internal11interpreter22JumpTableTargetOffsets8iterator23UpdateAndAdvanceToValidEv, .-_ZN2v88internal11interpreter22JumpTableTargetOffsets8iterator23UpdateAndAdvanceToValidEv
	.section	.text._ZN2v88internal11interpreter22JumpTableTargetOffsets8iteratorC2EiiiPKNS1_21BytecodeArrayAccessorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter22JumpTableTargetOffsets8iteratorC2EiiiPKNS1_21BytecodeArrayAccessorE
	.type	_ZN2v88internal11interpreter22JumpTableTargetOffsets8iteratorC2EiiiPKNS1_21BytecodeArrayAccessorE, @function
_ZN2v88internal11interpreter22JumpTableTargetOffsets8iteratorC2EiiiPKNS1_21BytecodeArrayAccessorE:
.LFB17853:
	.cfi_startproc
	endbr64
	movq	%r8, (%rdi)
	movq	$0, 8(%rdi)
	movl	%esi, 16(%rdi)
	movl	%edx, 20(%rdi)
	movl	%ecx, 24(%rdi)
	jmp	_ZN2v88internal11interpreter22JumpTableTargetOffsets8iterator23UpdateAndAdvanceToValidEv
	.cfi_endproc
.LFE17853:
	.size	_ZN2v88internal11interpreter22JumpTableTargetOffsets8iteratorC2EiiiPKNS1_21BytecodeArrayAccessorE, .-_ZN2v88internal11interpreter22JumpTableTargetOffsets8iteratorC2EiiiPKNS1_21BytecodeArrayAccessorE
	.globl	_ZN2v88internal11interpreter22JumpTableTargetOffsets8iteratorC1EiiiPKNS1_21BytecodeArrayAccessorE
	.set	_ZN2v88internal11interpreter22JumpTableTargetOffsets8iteratorC1EiiiPKNS1_21BytecodeArrayAccessorE,_ZN2v88internal11interpreter22JumpTableTargetOffsets8iteratorC2EiiiPKNS1_21BytecodeArrayAccessorE
	.section	.text._ZNK2v88internal11interpreter22JumpTableTargetOffsets5beginEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11interpreter22JumpTableTargetOffsets5beginEv
	.type	_ZNK2v88internal11interpreter22JumpTableTargetOffsets5beginEv, @function
_ZNK2v88internal11interpreter22JumpTableTargetOffsets5beginEv:
.LFB17849:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movl	8(%rsi), %eax
	movl	12(%rsi), %edx
	movq	$0, 8(%r12)
	movq	(%rsi), %rdi
	movl	16(%rsi), %ecx
	addl	%eax, %edx
	movl	%eax, 20(%r12)
	movq	%rdi, (%r12)
	movq	%r12, %rdi
	movl	%ecx, 16(%r12)
	movl	%edx, 24(%r12)
	call	_ZN2v88internal11interpreter22JumpTableTargetOffsets8iterator23UpdateAndAdvanceToValidEv
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17849:
	.size	_ZNK2v88internal11interpreter22JumpTableTargetOffsets5beginEv, .-_ZNK2v88internal11interpreter22JumpTableTargetOffsets5beginEv
	.section	.text._ZNK2v88internal11interpreter22JumpTableTargetOffsets3endEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11interpreter22JumpTableTargetOffsets3endEv
	.type	_ZNK2v88internal11interpreter22JumpTableTargetOffsets3endEv, @function
_ZNK2v88internal11interpreter22JumpTableTargetOffsets3endEv:
.LFB17850:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movl	12(%rsi), %eax
	movl	8(%rsi), %edx
	movq	$0, 8(%rdi)
	movq	(%rsi), %rcx
	addl	%eax, %edx
	addl	16(%rsi), %eax
	movl	%eax, 16(%rdi)
	movq	%rcx, (%rdi)
	movl	%edx, 20(%rdi)
	movl	%edx, 24(%rdi)
	call	_ZN2v88internal11interpreter22JumpTableTargetOffsets8iterator23UpdateAndAdvanceToValidEv
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17850:
	.size	_ZNK2v88internal11interpreter22JumpTableTargetOffsets3endEv, .-_ZNK2v88internal11interpreter22JumpTableTargetOffsets3endEv
	.section	.text._ZN2v88internal11interpreter22JumpTableTargetOffsets8iteratorppEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter22JumpTableTargetOffsets8iteratorppEv
	.type	_ZN2v88internal11interpreter22JumpTableTargetOffsets8iteratorppEv, @function
_ZN2v88internal11interpreter22JumpTableTargetOffsets8iteratorppEv:
.LFB17856:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	addl	$1, 20(%rdi)
	addl	$1, 16(%rdi)
	call	_ZN2v88internal11interpreter22JumpTableTargetOffsets8iterator23UpdateAndAdvanceToValidEv
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17856:
	.size	_ZN2v88internal11interpreter22JumpTableTargetOffsets8iteratorppEv, .-_ZN2v88internal11interpreter22JumpTableTargetOffsets8iteratorppEv
	.section	.text._ZNK2v88internal11interpreter22JumpTableTargetOffsets4sizeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11interpreter22JumpTableTargetOffsets4sizeEv
	.type	_ZNK2v88internal11interpreter22JumpTableTargetOffsets4sizeEv, @function
_ZNK2v88internal11interpreter22JumpTableTargetOffsets4sizeEv:
.LFB17851:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	leaq	-96(%rbp), %rbx
	subq	$80, %rsp
	movl	12(%rdi), %edx
	movq	(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	8(%rdi), %eax
	movl	16(%rdi), %ecx
	movq	%rbx, %rdi
	movq	%rsi, -96(%rbp)
	addl	%eax, %edx
	movl	%ecx, -80(%rbp)
	movl	%eax, -76(%rbp)
	movl	%edx, -72(%rbp)
	movq	$0, -88(%rbp)
	call	_ZN2v88internal11interpreter22JumpTableTargetOffsets8iterator23UpdateAndAdvanceToValidEv
	movl	12(%r12), %eax
	movl	8(%r12), %edx
	leaq	-64(%rbp), %rdi
	movq	(%r12), %rcx
	movq	$0, -56(%rbp)
	addl	%eax, %edx
	addl	16(%r12), %eax
	xorl	%r12d, %r12d
	movl	%eax, -48(%rbp)
	movq	%rcx, -64(%rbp)
	movl	%edx, -44(%rbp)
	movl	%edx, -40(%rbp)
	call	_ZN2v88internal11interpreter22JumpTableTargetOffsets8iterator23UpdateAndAdvanceToValidEv
	movl	-80(%rbp), %eax
	cmpl	-48(%rbp), %eax
	je	.L310
	.p2align 4,,10
	.p2align 3
.L312:
	addl	$1, %eax
	movq	%rbx, %rdi
	addl	$1, -76(%rbp)
	addl	$1, %r12d
	movl	%eax, -80(%rbp)
	call	_ZN2v88internal11interpreter22JumpTableTargetOffsets8iterator23UpdateAndAdvanceToValidEv
	movl	-80(%rbp), %eax
	cmpl	-48(%rbp), %eax
	jne	.L312
.L310:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L317
	addq	$80, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L317:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17851:
	.size	_ZNK2v88internal11interpreter22JumpTableTargetOffsets4sizeEv, .-_ZNK2v88internal11interpreter22JumpTableTargetOffsets4sizeEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal11interpreter21BytecodeArrayAccessorC2ESt10unique_ptrINS1_21AbstractBytecodeArrayESt14default_deleteIS4_EEi,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal11interpreter21BytecodeArrayAccessorC2ESt10unique_ptrINS1_21AbstractBytecodeArrayESt14default_deleteIS4_EEi, @function
_GLOBAL__sub_I__ZN2v88internal11interpreter21BytecodeArrayAccessorC2ESt10unique_ptrINS1_21AbstractBytecodeArrayESt14default_deleteIS4_EEi:
.LFB21678:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE21678:
	.size	_GLOBAL__sub_I__ZN2v88internal11interpreter21BytecodeArrayAccessorC2ESt10unique_ptrINS1_21AbstractBytecodeArrayESt14default_deleteIS4_EEi, .-_GLOBAL__sub_I__ZN2v88internal11interpreter21BytecodeArrayAccessorC2ESt10unique_ptrINS1_21AbstractBytecodeArrayESt14default_deleteIS4_EEi
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal11interpreter21BytecodeArrayAccessorC2ESt10unique_ptrINS1_21AbstractBytecodeArrayESt14default_deleteIS4_EEi
	.section	.data.rel.ro.local._ZTVN2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArrayE,"aw"
	.align 8
	.type	_ZTVN2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArrayE, @object
	.size	_ZTVN2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArrayE, 96
_ZTVN2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArrayE:
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray6lengthEv
	.quad	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray15parameter_countEv
	.quad	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray3getEi
	.quad	_ZN2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray3setEih
	.quad	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray23GetFirstBytecodeAddressEv
	.quad	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray18GetConstantAtIndexEiPNS0_7IsolateE
	.quad	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray20IsConstantAtIndexSmiEi
	.quad	_ZNK2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArray23GetConstantAtIndexAsSmiEi
	.quad	_ZN2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArrayD1Ev
	.quad	_ZN2v88internal11interpreter12_GLOBAL__N_119OnHeapBytecodeArrayD0Ev
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
