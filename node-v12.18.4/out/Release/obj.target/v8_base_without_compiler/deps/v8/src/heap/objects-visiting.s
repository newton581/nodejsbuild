	.file	"objects-visiting.cc"
	.text
	.section	.text._ZN2v88internalL13ClearWeakListINS0_4CodeEEEvPNS0_4HeapENS0_6ObjectE,"axG",@progbits,_ZN2v88internal13VisitWeakListINS0_7ContextEEENS0_6ObjectEPNS0_4HeapES3_PNS0_18WeakObjectRetainerE,comdat
	.p2align 4
	.type	_ZN2v88internalL13ClearWeakListINS0_4CodeEEEvPNS0_4HeapENS0_6ObjectE, @function
_ZN2v88internalL13ClearWeakListINS0_4CodeEEEvPNS0_4HeapENS0_6ObjectE:
.LFB24069:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	-37504(%rdi), %r12
	cmpq	%rsi, %r12
	je	.L1
	movq	%rsi, %rbx
	testb	$1, %r12b
	jne	.L12
	.p2align 4,,10
	.p2align 3
.L3:
	movq	31(%rbx), %rdx
	leaq	31(%rbx), %rax
	movq	7(%rdx), %rbx
	movq	(%rax), %rax
	movq	%r12, 7(%rax)
	cmpq	%rbx, %r12
	jne	.L3
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	movq	%r12, %r13
	andq	$-262144, %r13
	.p2align 4,,10
	.p2align 3
.L5:
	movq	31(%rbx), %rdx
	leaq	31(%rbx), %rax
	movq	7(%rdx), %rbx
	movq	(%rax), %rdi
	leaq	7(%rdi), %rsi
	movq	%r12, 7(%rdi)
	testb	$24, 8(%r13)
	je	.L6
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L6
	movq	%r12, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
.L6:
	cmpq	%rbx, %r12
	jne	.L5
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24069:
	.size	_ZN2v88internalL13ClearWeakListINS0_4CodeEEEvPNS0_4HeapENS0_6ObjectE, .-_ZN2v88internalL13ClearWeakListINS0_4CodeEEEvPNS0_4HeapENS0_6ObjectE
	.section	.rodata._ZN2v88internal13VisitWeakListINS0_14AllocationSiteEEENS0_6ObjectEPNS0_4HeapES3_PNS0_18WeakObjectRetainerE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"NewArray"
	.section	.text._ZN2v88internal13VisitWeakListINS0_14AllocationSiteEEENS0_6ObjectEPNS0_4HeapES3_PNS0_18WeakObjectRetainerE,"axG",@progbits,_ZN2v88internal13VisitWeakListINS0_14AllocationSiteEEENS0_6ObjectEPNS0_4HeapES3_PNS0_18WeakObjectRetainerE,comdat
	.p2align 4
	.weak	_ZN2v88internal13VisitWeakListINS0_14AllocationSiteEEENS0_6ObjectEPNS0_4HeapES3_PNS0_18WeakObjectRetainerE
	.type	_ZN2v88internal13VisitWeakListINS0_14AllocationSiteEEENS0_6ObjectEPNS0_4HeapES3_PNS0_18WeakObjectRetainerE, @function
_ZN2v88internal13VisitWeakListINS0_14AllocationSiteEEENS0_6ObjectEPNS0_4HeapES3_PNS0_18WeakObjectRetainerE:
.LFB24071:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$40, %rsp
	cmpl	$2, 392(%rdi)
	movb	$0, -49(%rbp)
	movq	-37504(%rdi), %r12
	jne	.L14
	movq	2016(%rdi), %rax
	movzbl	98(%rax), %eax
	movb	%al, -49(%rbp)
.L14:
	movq	%r12, %r14
	cmpq	%rsi, %r12
	je	.L15
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L26:
	movq	(%rbx), %rax
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	*16(%rax)
	movq	39(%r15), %r15
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L16
	cmpq	%r14, %r12
	je	.L31
	movq	%rax, 39(%r13)
	leaq	39(%r13), %rcx
	testb	$1, %al
	je	.L17
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L17
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L58
	.p2align 4,,10
	.p2align 3
.L17:
	cmpb	$0, -49(%rbp)
	jne	.L59
.L36:
	movq	%rdx, %r13
.L16:
	cmpq	%r15, %r12
	jne	.L26
	testq	%r13, %r13
	jne	.L60
.L15:
	addq	$40, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	.cfi_restore_state
	movq	%rax, %r13
	movq	%rax, %r14
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L59:
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	8(%rax), %rax
	testb	$64, %al
	je	.L36
	andq	$-262144, %r13
	movq	8(%r13), %rax
	testb	$88, %al
	jne	.L61
.L19:
	movq	112(%r13), %rax
	testq	%rax, %rax
	je	.L62
.L21:
	movq	%rcx, %rsi
	andl	$262143, %ecx
	subq	%r13, %rsi
	movl	%ecx, %r11d
	movl	%ecx, %edi
	sarl	$13, %ecx
	shrq	$18, %rsi
	movslq	%ecx, %rcx
	sarl	$3, %edi
	leaq	(%rsi,%rsi,2), %rsi
	sarl	$8, %r11d
	andl	$31, %edi
	salq	$7, %rsi
	andl	$31, %r11d
	movl	%edi, %r8d
	leaq	(%rsi,%rcx,8), %r13
	addq	%rax, %r13
	movq	0(%r13), %r10
	testq	%r10, %r10
	je	.L63
.L23:
	movslq	%r11d, %r11
	movl	$1, %esi
	movl	%r8d, %ecx
	movq	%rdx, %r13
	leaq	(%r10,%r11,4), %rdi
	sall	%cl, %esi
	movl	(%rdi), %eax
	testl	%eax, %esi
	jne	.L16
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L64:
	movl	%esi, %r10d
	movl	%ecx, %eax
	orl	%ecx, %r10d
	lock cmpxchgl	%r10d, (%rdi)
	cmpl	%eax, %ecx
	je	.L36
.L25:
	movl	(%rdi), %ecx
	movl	%esi, %eax
	andl	%ecx, %eax
	cmpl	%eax, %esi
	jne	.L64
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L58:
	movq	%rcx, %rsi
	movq	%r13, %rdi
	movq	%rdx, -72(%rbp)
	movq	%rcx, -64(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rdx
	movq	-64(%rbp), %rcx
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L60:
	movq	%r12, 39(%r13)
	leaq	39(%r13), %rsi
	testb	$1, %r12b
	je	.L15
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L15
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L15
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L61:
	testb	$-128, %ah
	je	.L36
	jmp	.L19
.L63:
	movl	%edi, -72(%rbp)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	movq	%rdx, -80(%rbp)
	movl	%r11d, -64(%rbp)
	call	_ZnamRKSt9nothrow_t@PLT
	movl	-64(%rbp), %r11d
	movl	-72(%rbp), %r8d
	testq	%rax, %rax
	movq	-80(%rbp), %rdx
	movq	%rax, %r10
	je	.L65
.L24:
	leaq	8(%r10), %rdi
	movq	%r10, %rcx
	xorl	%eax, %eax
	movq	$0, (%r10)
	andq	$-8, %rdi
	movq	$0, 120(%r10)
	subq	%rdi, %rcx
	subl	$-128, %ecx
	shrl	$3, %ecx
	rep stosq
	lock cmpxchgq	%r10, 0(%r13)
	testq	%rax, %rax
	je	.L23
	movq	%r10, %rdi
	movq	%rdx, -80(%rbp)
	movl	%r8d, -72(%rbp)
	movl	%r11d, -64(%rbp)
	call	_ZdaPv@PLT
	movq	0(%r13), %r10
	movq	-80(%rbp), %rdx
	movl	-72(%rbp), %r8d
	movl	-64(%rbp), %r11d
	jmp	.L23
.L62:
	movq	%r13, %rdi
	movq	%rdx, -72(%rbp)
	movq	%rcx, -64(%rbp)
	call	_ZN2v88internal11MemoryChunk15AllocateSlotSetILNS0_17RememberedSetTypeE1EEEPNS0_7SlotSetEv@PLT
	movq	-72(%rbp), %rdx
	movq	-64(%rbp), %rcx
	jmp	.L21
.L65:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movl	-64(%rbp), %r11d
	movl	-72(%rbp), %r8d
	testq	%rax, %rax
	movq	-80(%rbp), %rdx
	movq	%rax, %r10
	jne	.L24
	leaq	.LC0(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
	.cfi_endproc
.LFE24071:
	.size	_ZN2v88internal13VisitWeakListINS0_14AllocationSiteEEENS0_6ObjectEPNS0_4HeapES3_PNS0_18WeakObjectRetainerE, .-_ZN2v88internal13VisitWeakListINS0_14AllocationSiteEEENS0_6ObjectEPNS0_4HeapES3_PNS0_18WeakObjectRetainerE
	.section	.text._ZN2v88internal13VisitWeakListINS0_4CodeEEENS0_6ObjectEPNS0_4HeapES3_PNS0_18WeakObjectRetainerE,"axG",@progbits,_ZN2v88internal13VisitWeakListINS0_4CodeEEENS0_6ObjectEPNS0_4HeapES3_PNS0_18WeakObjectRetainerE,comdat
	.p2align 4
	.weak	_ZN2v88internal13VisitWeakListINS0_4CodeEEENS0_6ObjectEPNS0_4HeapES3_PNS0_18WeakObjectRetainerE
	.type	_ZN2v88internal13VisitWeakListINS0_4CodeEEENS0_6ObjectEPNS0_4HeapES3_PNS0_18WeakObjectRetainerE, @function
_ZN2v88internal13VisitWeakListINS0_4CodeEEENS0_6ObjectEPNS0_4HeapES3_PNS0_18WeakObjectRetainerE:
.LFB25478:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	-37592(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpl	$2, 392(%rdi)
	movq	%rax, -64(%rbp)
	movb	$0, -49(%rbp)
	movq	-37504(%rdi), %r12
	jne	.L67
	movq	2016(%rdi), %rax
	movzbl	98(%rax), %eax
	movb	%al, -49(%rbp)
.L67:
	cmpq	%r12, %rsi
	je	.L86
	movq	%r12, %r13
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L82:
	movq	(%r14), %rax
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	*16(%rax)
	leaq	31(%r15), %rcx
	movq	%rax, %rdx
	movq	31(%r15), %rax
	movq	7(%rax), %r15
	testq	%rdx, %rdx
	je	.L69
	cmpq	%r13, %r12
	je	.L87
	movq	31(%rbx), %rdi
	leaq	31(%rbx), %rax
	movq	%rdx, 7(%rdi)
	leaq	7(%rdi), %rsi
	testb	$1, %dl
	je	.L71
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	testb	$24, 8(%rcx)
	je	.L71
	movq	%rdi, %rcx
	andq	$-262144, %rcx
	testb	$24, 8(%rcx)
	je	.L116
	.p2align 4,,10
	.p2align 3
.L71:
	cmpb	$0, -49(%rbp)
	movq	%rdx, %rbx
	jne	.L117
.L70:
	cmpq	%r15, %r12
	jne	.L82
	testq	%rbx, %rbx
	jne	.L118
.L68:
	addq	$56, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	.cfi_restore_state
	movq	%rdx, %rbx
	movq	%rdx, %r13
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L69:
	movq	-64(%rbp), %rax
	movq	88(%rax), %rdx
	movq	(%rcx), %rdi
	movq	%rdx, 7(%rdi)
	leaq	7(%rdi), %rsi
	testb	$1, %dl
	je	.L70
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L70
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L70
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L117:
	movq	(%rax), %rcx
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	8(%rax), %rax
	testb	$64, %al
	je	.L70
	movq	%rcx, %rsi
	andq	$-262144, %rsi
	movq	8(%rsi), %rax
	testb	$88, %al
	je	.L73
	testb	$-128, %ah
	je	.L70
.L73:
	movq	112(%rsi), %rax
	testq	%rax, %rax
	je	.L119
.L75:
	addq	$7, %rcx
	movq	%rcx, %rbx
	andl	$262143, %ecx
	subq	%rsi, %rbx
	movl	%ecx, %r11d
	movl	%ecx, %edi
	sarl	$13, %ecx
	sarl	$3, %edi
	movq	%rbx, %rsi
	movslq	%ecx, %rcx
	sarl	$8, %r11d
	shrq	$18, %rsi
	andl	$31, %edi
	andl	$31, %r11d
	leaq	(%rsi,%rsi,2), %rsi
	movl	%edi, %r8d
	salq	$7, %rsi
	leaq	(%rsi,%rcx,8), %rbx
	addq	%rax, %rbx
	movq	(%rbx), %r10
	testq	%r10, %r10
	je	.L120
.L77:
	movslq	%r11d, %r11
	movl	$1, %esi
	movl	%r8d, %ecx
	movq	%rdx, %rbx
	leaq	(%r10,%r11,4), %rdi
	sall	%cl, %esi
	movl	(%rdi), %eax
	testl	%eax, %esi
	jne	.L70
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L116:
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %rax
	movq	-72(%rbp), %rdx
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L121:
	movl	%esi, %r8d
	movl	%ecx, %eax
	orl	%ecx, %r8d
	lock cmpxchgl	%r8d, (%rdi)
	cmpl	%eax, %ecx
	je	.L92
.L79:
	movl	(%rdi), %ecx
	movl	%esi, %eax
	andl	%ecx, %eax
	cmpl	%eax, %esi
	jne	.L121
.L92:
	movq	%rdx, %rbx
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L118:
	movq	31(%rbx), %rdi
	movq	%r12, 7(%rdi)
	leaq	7(%rdi), %rsi
	testb	$1, %r12b
	je	.L68
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L68
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L68
	movq	%r12, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L68
.L120:
	movl	%edi, -80(%rbp)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	movq	%rdx, -88(%rbp)
	movl	%r11d, -72(%rbp)
	call	_ZnamRKSt9nothrow_t@PLT
	movl	-72(%rbp), %r11d
	movl	-80(%rbp), %r8d
	testq	%rax, %rax
	movq	-88(%rbp), %rdx
	movq	%rax, %r10
	je	.L122
.L78:
	leaq	8(%r10), %rdi
	movq	%r10, %rcx
	xorl	%eax, %eax
	movq	$0, (%r10)
	andq	$-8, %rdi
	movq	$0, 120(%r10)
	subq	%rdi, %rcx
	subl	$-128, %ecx
	shrl	$3, %ecx
	rep stosq
	lock cmpxchgq	%r10, (%rbx)
	testq	%rax, %rax
	je	.L77
	movq	%r10, %rdi
	movq	%rdx, -88(%rbp)
	movl	%r8d, -80(%rbp)
	movl	%r11d, -72(%rbp)
	call	_ZdaPv@PLT
	movq	(%rbx), %r10
	movq	-88(%rbp), %rdx
	movl	-80(%rbp), %r8d
	movl	-72(%rbp), %r11d
	jmp	.L77
.L119:
	movq	%rsi, %rdi
	movq	%rdx, -88(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11MemoryChunk15AllocateSlotSetILNS0_17RememberedSetTypeE1EEEPNS0_7SlotSetEv@PLT
	movq	-88(%rbp), %rdx
	movq	-80(%rbp), %rcx
	movq	-72(%rbp), %rsi
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L86:
	movq	%rsi, %r13
	jmp	.L68
.L122:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movl	-72(%rbp), %r11d
	movl	-80(%rbp), %r8d
	testq	%rax, %rax
	movq	-88(%rbp), %rdx
	movq	%rax, %r10
	jne	.L78
	leaq	.LC0(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
	.cfi_endproc
.LFE25478:
	.size	_ZN2v88internal13VisitWeakListINS0_4CodeEEENS0_6ObjectEPNS0_4HeapES3_PNS0_18WeakObjectRetainerE, .-_ZN2v88internal13VisitWeakListINS0_4CodeEEENS0_6ObjectEPNS0_4HeapES3_PNS0_18WeakObjectRetainerE
	.section	.text._ZN2v88internal13VisitWeakListINS0_7ContextEEENS0_6ObjectEPNS0_4HeapES3_PNS0_18WeakObjectRetainerE,"axG",@progbits,_ZN2v88internal13VisitWeakListINS0_7ContextEEENS0_6ObjectEPNS0_4HeapES3_PNS0_18WeakObjectRetainerE,comdat
	.p2align 4
	.weak	_ZN2v88internal13VisitWeakListINS0_7ContextEEENS0_6ObjectEPNS0_4HeapES3_PNS0_18WeakObjectRetainerE
	.type	_ZN2v88internal13VisitWeakListINS0_7ContextEEENS0_6ObjectEPNS0_4HeapES3_PNS0_18WeakObjectRetainerE, @function
_ZN2v88internal13VisitWeakListINS0_7ContextEEENS0_6ObjectEPNS0_4HeapES3_PNS0_18WeakObjectRetainerE:
.LFB24070:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	-37504(%rdi), %rax
	cmpl	$2, 392(%rdi)
	movb	$0, -73(%rbp)
	movq	%rax, -56(%rbp)
	jne	.L124
	movq	2016(%rdi), %rax
	movzbl	98(%rax), %eax
	movb	%al, -73(%rbp)
.L124:
	movq	-56(%rbp), %rax
	movq	%rax, -72(%rbp)
	cmpq	%rsi, %rax
	je	.L125
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L176:
	movq	(%r14), %rax
	movq	%r12, -64(%rbp)
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	*16(%rax)
	movq	1959(%r12), %r12
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L126
	movq	-72(%rbp), %rdi
	cmpq	%rdi, -56(%rbp)
	je	.L183
	movq	%rax, 1959(%r13)
	leaq	1959(%r13), %r8
	testb	$1, %al
	je	.L128
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L128
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L274
	.p2align 4,,10
	.p2align 3
.L128:
	cmpb	$0, -73(%rbp)
	jne	.L275
.L127:
	cmpl	$2, 392(%r15)
	je	.L276
.L137:
	movq	%rbx, %r13
.L175:
	cmpq	%r12, -56(%rbp)
	jne	.L176
	testq	%r13, %r13
	jne	.L277
.L125:
	movq	-72(%rbp), %rax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L183:
	.cfi_restore_state
	cmpl	$2, 392(%r15)
	movq	%rax, -72(%rbp)
	jne	.L137
.L276:
	leaq	1943(%rbx), %rax
	leaq	1967(%rbx), %r8
	movq	%r15, %r9
	movq	%r14, %r10
	movq	%rax, -88(%rbp)
	movq	%rax, %r13
	movq	%rbx, %rax
	movq	%r12, %r15
	andq	$-262144, %rax
	movq	%rbx, %r12
	movq	%r8, %r14
	movq	%rax, -64(%rbp)
.L140:
	movq	0(%r13), %rax
	andq	$-262144, %rax
	movq	8(%rax), %rax
	testb	$64, %al
	jne	.L138
.L142:
	addq	$8, %r13
	cmpq	%r13, %r14
	jne	.L140
	movq	%r12, %rbx
	movq	%r10, %rdx
	movq	%r9, %rdi
	movq	%r15, %r12
	movq	1943(%rbx), %rsi
	movq	%r9, %r15
	movq	%r10, %r14
	call	_ZN2v88internal13VisitWeakListINS0_4CodeEEENS0_6ObjectEPNS0_4HeapES3_PNS0_18WeakObjectRetainerE
	movq	%rax, 1943(%rbx)
	movq	%rax, %r13
	testb	$1, %al
	je	.L180
	movq	%rax, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -96(%rbp)
	testl	$262144, %eax
	je	.L151
	movq	-88(%rbp), %rsi
	movq	%r13, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-96(%rbp), %rcx
	movq	8(%rcx), %rax
.L151:
	testb	$24, %al
	je	.L180
	movq	-64(%rbp), %rax
	testb	$24, 8(%rax)
	je	.L278
	.p2align 4,,10
	.p2align 3
.L180:
	cmpl	$2, 392(%r15)
	je	.L153
.L156:
	movq	1951(%rbx), %rsi
	movq	%r14, %rdx
	movq	%r15, %rdi
	leaq	1951(%rbx), %r13
	call	_ZN2v88internal13VisitWeakListINS0_4CodeEEENS0_6ObjectEPNS0_4HeapES3_PNS0_18WeakObjectRetainerE
	movq	%rax, 1951(%rbx)
	testb	$1, %al
	je	.L179
	movq	%rax, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rdx
	movq	%rcx, -88(%rbp)
	testl	$262144, %edx
	je	.L164
	movq	%rax, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rcx
	movq	-96(%rbp), %rax
	movq	8(%rcx), %rdx
.L164:
	andl	$24, %edx
	je	.L179
	movq	-64(%rbp), %rdi
	testb	$24, 8(%rdi)
	je	.L279
.L179:
	cmpl	$2, 392(%r15)
	jne	.L137
.L289:
	movq	2016(%r15), %rdx
	cmpb	$0, 98(%rdx)
	je	.L137
	andq	$-262144, %rax
	movq	8(%rax), %rax
	testb	$64, %al
	je	.L137
	movq	-64(%rbp), %rax
	movq	8(%rax), %rax
	testb	$88, %al
	je	.L168
	testb	$-128, %ah
	je	.L137
.L168:
	movq	-64(%rbp), %rax
	movq	112(%rax), %rsi
	testq	%rsi, %rsi
	je	.L280
.L170:
	movq	%r13, %rcx
	andl	$262143, %r13d
	subq	-64(%rbp), %rcx
	movl	%r13d, %eax
	shrq	$18, %rcx
	movl	%r13d, %edx
	sarl	$8, %eax
	sarl	$13, %r13d
	leaq	(%rcx,%rcx,2), %rcx
	salq	$7, %rcx
	andl	$31, %eax
	sarl	$3, %edx
	movl	%eax, %r8d
	movslq	%r13d, %rax
	andl	$31, %edx
	leaq	(%rcx,%rax,8), %r13
	addq	%rsi, %r13
	movq	0(%r13), %r9
	testq	%r9, %r9
	je	.L281
.L172:
	movl	%edx, %ecx
	movl	$1, %eax
	sall	%cl, %eax
	movl	%eax, %ecx
	movslq	%r8d, %rax
	leaq	(%r9,%rax,4), %rsi
	movl	(%rsi), %eax
	testl	%eax, %ecx
	jne	.L137
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L126:
	movq	-64(%rbp), %rbx
	movq	%r15, %rdi
	movq	1943(%rbx), %rsi
	call	_ZN2v88internalL13ClearWeakListINS0_4CodeEEEvPNS0_4HeapENS0_6ObjectE
	movq	1951(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internalL13ClearWeakListINS0_4CodeEEEvPNS0_4HeapENS0_6ObjectE
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L138:
	movq	-64(%rbp), %rax
	movq	8(%rax), %rax
	testb	$88, %al
	je	.L141
	testb	$-128, %ah
	je	.L142
.L141:
	movq	-64(%rbp), %rax
	movq	112(%rax), %rbx
	testq	%rbx, %rbx
	je	.L282
.L144:
	movq	%r13, %rax
	movl	%r13d, %ecx
	subq	-64(%rbp), %rax
	andl	$262143, %ecx
	shrq	$18, %rax
	movl	%ecx, %edx
	movl	%ecx, %r11d
	leaq	(%rax,%rax,2), %rax
	sarl	$13, %ecx
	salq	$7, %rax
	movslq	%ecx, %rcx
	sarl	$8, %edx
	leaq	(%rax,%rcx,8), %rax
	sarl	$3, %r11d
	andl	$31, %edx
	addq	%rax, %rbx
	andl	$31, %r11d
	movq	(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L283
.L146:
	movl	%r11d, %ecx
	movl	$1, %eax
	movslq	%edx, %rdx
	sall	%cl, %eax
	leaq	(%rsi,%rdx,4), %rsi
	movl	%eax, %ecx
	movl	(%rsi), %eax
	testl	%eax, %ecx
	jne	.L142
	movl	(%rsi), %edx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ecx
	je	.L142
	.p2align 4,,10
	.p2align 3
.L284:
	movl	%ecx, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	je	.L142
	movl	(%rsi), %edx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ecx
	jne	.L284
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L275:
	movq	%rbx, %rax
	andq	$-262144, %rax
	movq	8(%rax), %rax
	testb	$64, %al
	je	.L127
	andq	$-262144, %r13
	movq	8(%r13), %rax
	testb	$88, %al
	je	.L130
	testb	$-128, %ah
	je	.L127
.L130:
	movq	112(%r13), %rcx
	testq	%rcx, %rcx
	je	.L285
.L132:
	movl	%r8d, %eax
	movq	%r8, %rsi
	andl	$262143, %eax
	subq	%r13, %rsi
	movl	%eax, %edx
	shrq	$18, %rsi
	sarl	$8, %edx
	leaq	(%rsi,%rsi,2), %rsi
	andl	$31, %edx
	salq	$7, %rsi
	movl	%edx, %r8d
	movl	%eax, %edx
	sarl	$13, %eax
	cltq
	sarl	$3, %edx
	leaq	(%rsi,%rax,8), %r13
	andl	$31, %edx
	addq	%rcx, %r13
	movq	0(%r13), %r9
	testq	%r9, %r9
	je	.L286
.L134:
	movl	%edx, %ecx
	movl	$1, %eax
	sall	%cl, %eax
	movl	%eax, %ecx
	movslq	%r8d, %rax
	leaq	(%r9,%rax,4), %rsi
	movl	(%rsi), %eax
	testl	%eax, %ecx
	jne	.L127
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L153:
	movq	2016(%r15), %rax
	cmpb	$0, 98(%rax)
	je	.L156
	andq	$-262144, %r13
	movq	8(%r13), %rax
	testb	$64, %al
	je	.L156
	movq	-64(%rbp), %rax
	movq	8(%rax), %rax
	testb	$88, %al
	je	.L157
	testb	$-128, %ah
	je	.L156
.L157:
	movq	-64(%rbp), %rax
	movq	112(%rax), %r13
	testq	%r13, %r13
	je	.L287
.L159:
	movq	-88(%rbp), %rax
	movq	%rax, %rcx
	subq	-64(%rbp), %rcx
	andl	$262143, %eax
	shrq	$18, %rcx
	movl	%eax, %edx
	movl	%eax, %r9d
	sarl	$13, %eax
	leaq	(%rcx,%rcx,2), %rcx
	sarl	$8, %edx
	cltq
	sarl	$3, %r9d
	salq	$7, %rcx
	andl	$31, %edx
	andl	$31, %r9d
	leaq	(%rcx,%rax,8), %rax
	addq	%rax, %r13
	movq	0(%r13), %r10
	testq	%r10, %r10
	je	.L288
.L161:
	movl	%r9d, %ecx
	movl	$1, %eax
	movslq	%edx, %rdx
	sall	%cl, %eax
	leaq	(%r10,%rdx,4), %rsi
	movl	%eax, %ecx
	movl	(%rsi), %eax
	testl	%eax, %ecx
	jne	.L156
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L274:
	movq	%r8, %rsi
	movq	%rbx, %rdx
	movq	%r13, %rdi
	movq	%r8, -64(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-64(%rbp), %r8
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L279:
	movq	%rax, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	cmpl	$2, 392(%r15)
	movq	-88(%rbp), %rax
	jne	.L137
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L278:
	movq	-88(%rbp), %rsi
	movq	%r13, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L290:
	movl	%ecx, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	je	.L127
.L136:
	movl	(%rsi), %edx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ecx
	jne	.L290
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L283:
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	movq	%r10, -120(%rbp)
	movq	%r9, -112(%rbp)
	movl	%r11d, -104(%rbp)
	movl	%edx, -96(%rbp)
	call	_ZnamRKSt9nothrow_t@PLT
	movl	-96(%rbp), %edx
	movl	-104(%rbp), %r11d
	testq	%rax, %rax
	movq	-112(%rbp), %r9
	movq	-120(%rbp), %r10
	movq	%rax, %rsi
	je	.L291
.L147:
	leaq	8(%rsi), %rdi
	movq	%rsi, %rcx
	xorl	%eax, %eax
	movq	$0, (%rsi)
	andq	$-8, %rdi
	movq	$0, 120(%rsi)
	subq	%rdi, %rcx
	subl	$-128, %ecx
	shrl	$3, %ecx
	rep stosq
	lock cmpxchgq	%rsi, (%rbx)
	testq	%rax, %rax
	je	.L146
	movq	%rsi, %rdi
	movq	%r10, -120(%rbp)
	movq	%r9, -112(%rbp)
	movl	%r11d, -104(%rbp)
	movl	%edx, -96(%rbp)
	call	_ZdaPv@PLT
	movq	(%rbx), %rsi
	movq	-120(%rbp), %r10
	movq	-112(%rbp), %r9
	movl	-104(%rbp), %r11d
	movl	-96(%rbp), %edx
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L282:
	movq	-64(%rbp), %rdi
	movq	%r10, -104(%rbp)
	movq	%r9, -96(%rbp)
	call	_ZN2v88internal11MemoryChunk15AllocateSlotSetILNS0_17RememberedSetTypeE1EEEPNS0_7SlotSetEv@PLT
	movq	-104(%rbp), %r10
	movq	-96(%rbp), %r9
	movq	%rax, %rbx
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L277:
	movq	-56(%rbp), %rdx
	leaq	1959(%r13), %rsi
	movq	%rdx, 1959(%r13)
	testb	$1, %dl
	je	.L125
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L125
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L125
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L125
.L286:
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	movl	%edx, -88(%rbp)
	movl	%r8d, -64(%rbp)
	call	_ZnamRKSt9nothrow_t@PLT
	movl	-64(%rbp), %r8d
	movl	-88(%rbp), %edx
	testq	%rax, %rax
	movq	%rax, %r9
	je	.L292
.L135:
	leaq	8(%r9), %rdi
	movq	%r9, %rcx
	xorl	%eax, %eax
	movq	$0, (%r9)
	andq	$-8, %rdi
	movq	$0, 120(%r9)
	subq	%rdi, %rcx
	subl	$-128, %ecx
	shrl	$3, %ecx
	rep stosq
	lock cmpxchgq	%r9, 0(%r13)
	testq	%rax, %rax
	je	.L134
	movq	%r9, %rdi
	movl	%edx, -88(%rbp)
	movl	%r8d, -64(%rbp)
	call	_ZdaPv@PLT
	movq	0(%r13), %r9
	movl	-88(%rbp), %edx
	movl	-64(%rbp), %r8d
	jmp	.L134
.L285:
	movq	%r13, %rdi
	movq	%r8, -64(%rbp)
	call	_ZN2v88internal11MemoryChunk15AllocateSlotSetILNS0_17RememberedSetTypeE1EEEPNS0_7SlotSetEv@PLT
	movq	-64(%rbp), %r8
	movq	%rax, %rcx
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L293:
	movl	%ecx, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	je	.L137
.L174:
	movl	(%rsi), %edx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ecx
	jne	.L293
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L294:
	movl	%ecx, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	je	.L156
.L163:
	movl	(%rsi), %edx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ecx
	jne	.L294
	jmp	.L156
.L288:
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	movl	%r9d, -96(%rbp)
	movl	%edx, -88(%rbp)
	call	_ZnamRKSt9nothrow_t@PLT
	movl	-88(%rbp), %edx
	movl	-96(%rbp), %r9d
	testq	%rax, %rax
	movq	%rax, %r10
	je	.L295
.L162:
	leaq	8(%r10), %rdi
	movq	%r10, %rcx
	xorl	%eax, %eax
	movq	$0, (%r10)
	andq	$-8, %rdi
	movq	$0, 120(%r10)
	subq	%rdi, %rcx
	subl	$-128, %ecx
	shrl	$3, %ecx
	rep stosq
	lock cmpxchgq	%r10, 0(%r13)
	testq	%rax, %rax
	je	.L161
	movq	%r10, %rdi
	movl	%r9d, -96(%rbp)
	movl	%edx, -88(%rbp)
	call	_ZdaPv@PLT
	movq	0(%r13), %r10
	movl	-96(%rbp), %r9d
	movl	-88(%rbp), %edx
	jmp	.L161
.L287:
	movq	-64(%rbp), %rdi
	call	_ZN2v88internal11MemoryChunk15AllocateSlotSetILNS0_17RememberedSetTypeE1EEEPNS0_7SlotSetEv@PLT
	movq	%rax, %r13
	jmp	.L159
.L281:
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	movl	%edx, -88(%rbp)
	movl	%r8d, -64(%rbp)
	call	_ZnamRKSt9nothrow_t@PLT
	movl	-64(%rbp), %r8d
	movl	-88(%rbp), %edx
	testq	%rax, %rax
	movq	%rax, %r9
	je	.L296
.L173:
	leaq	8(%r9), %rdi
	movq	%r9, %rcx
	xorl	%eax, %eax
	movq	$0, (%r9)
	andq	$-8, %rdi
	movq	$0, 120(%r9)
	subq	%rdi, %rcx
	subl	$-128, %ecx
	shrl	$3, %ecx
	rep stosq
	lock cmpxchgq	%r9, 0(%r13)
	testq	%rax, %rax
	je	.L172
	movq	%r9, %rdi
	movl	%edx, -88(%rbp)
	movl	%r8d, -64(%rbp)
	call	_ZdaPv@PLT
	movq	0(%r13), %r9
	movl	-88(%rbp), %edx
	movl	-64(%rbp), %r8d
	jmp	.L172
.L280:
	movq	-64(%rbp), %rdi
	call	_ZN2v88internal11MemoryChunk15AllocateSlotSetILNS0_17RememberedSetTypeE1EEEPNS0_7SlotSetEv@PLT
	movq	%rax, %rsi
	jmp	.L170
.L292:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movl	-64(%rbp), %r8d
	movl	-88(%rbp), %edx
	testq	%rax, %rax
	movq	%rax, %r9
	jne	.L135
.L148:
	leaq	.LC0(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
.L291:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movl	-96(%rbp), %edx
	movl	-104(%rbp), %r11d
	testq	%rax, %rax
	movq	-112(%rbp), %r9
	movq	-120(%rbp), %r10
	movq	%rax, %rsi
	jne	.L147
	jmp	.L148
.L295:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movl	-88(%rbp), %edx
	movl	-96(%rbp), %r9d
	testq	%rax, %rax
	movq	%rax, %r10
	jne	.L162
	jmp	.L148
.L296:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movl	-64(%rbp), %r8d
	movl	-88(%rbp), %edx
	testq	%rax, %rax
	movq	%rax, %r9
	jne	.L173
	jmp	.L148
	.cfi_endproc
.LFE24070:
	.size	_ZN2v88internal13VisitWeakListINS0_7ContextEEENS0_6ObjectEPNS0_4HeapES3_PNS0_18WeakObjectRetainerE, .-_ZN2v88internal13VisitWeakListINS0_7ContextEEENS0_6ObjectEPNS0_4HeapES3_PNS0_18WeakObjectRetainerE
	.section	.text.startup._GLOBAL__sub_I_objects_visiting.cc,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I_objects_visiting.cc, @function
_GLOBAL__sub_I_objects_visiting.cc:
.LFB27306:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE27306:
	.size	_GLOBAL__sub_I_objects_visiting.cc, .-_GLOBAL__sub_I_objects_visiting.cc
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I_objects_visiting.cc
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
