	.file	"pending-compilation-error-handler.cc"
	.text
	.section	.rodata._ZNK2v88internal30PendingCompilationErrorHandler14MessageDetails14ArgumentStringEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"(location_) != nullptr"
.LC1:
	.string	"Check failed: %s."
	.section	.text._ZNK2v88internal30PendingCompilationErrorHandler14MessageDetails14ArgumentStringEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal30PendingCompilationErrorHandler14MessageDetails14ArgumentStringEPNS0_7IsolateE
	.type	_ZNK2v88internal30PendingCompilationErrorHandler14MessageDetails14ArgumentStringEPNS0_7IsolateE, @function
_ZNK2v88internal30PendingCompilationErrorHandler14MessageDetails14ArgumentStringEPNS0_7IsolateE:
.LFB17955:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.L2
	movq	(%rax), %rax
.L3:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L9
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	movq	24(%rdi), %rbx
	movq	%rsi, %r12
	testq	%rbx, %rbx
	je	.L4
	movq	%rbx, %rdi
	call	strlen@PLT
	xorl	%edx, %edx
	leaq	-48(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rbx, -48(%rbp)
	movq	%rax, -40(%rbp)
	call	_ZN2v88internal7Factory17NewStringFromUtf8ERKNS0_6VectorIKcEENS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	jne	.L3
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4:
	leaq	3512(%rsi), %rax
	jmp	.L3
.L9:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17955:
	.size	_ZNK2v88internal30PendingCompilationErrorHandler14MessageDetails14ArgumentStringEPNS0_7IsolateE, .-_ZNK2v88internal30PendingCompilationErrorHandler14MessageDetails14ArgumentStringEPNS0_7IsolateE
	.section	.text._ZNK2v88internal30PendingCompilationErrorHandler14MessageDetails11GetLocationENS0_6HandleINS0_6ScriptEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal30PendingCompilationErrorHandler14MessageDetails11GetLocationENS0_6HandleINS0_6ScriptEEE
	.type	_ZNK2v88internal30PendingCompilationErrorHandler14MessageDetails11GetLocationENS0_6HandleINS0_6ScriptEEE, @function
_ZNK2v88internal30PendingCompilationErrorHandler14MessageDetails11GetLocationENS0_6HandleINS0_6ScriptEEE:
.LFB17956:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movl	4(%rsi), %ecx
	movl	(%rsi), %edx
	movq	%r8, %rsi
	call	_ZN2v88internal15MessageLocationC1ENS0_6HandleINS0_6ScriptEEEii@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17956:
	.size	_ZNK2v88internal30PendingCompilationErrorHandler14MessageDetails11GetLocationENS0_6HandleINS0_6ScriptEEE, .-_ZNK2v88internal30PendingCompilationErrorHandler14MessageDetails11GetLocationENS0_6HandleINS0_6ScriptEEE
	.section	.text._ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc
	.type	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc, @function
_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc:
.LFB17957:
	.cfi_startproc
	endbr64
	cmpb	$0, (%rdi)
	jne	.L12
	movb	$1, (%rdi)
	movl	%esi, 8(%rdi)
	movl	%edx, 12(%rdi)
	movl	%ecx, 16(%rdi)
	movq	$0, 24(%rdi)
	movq	%r8, 32(%rdi)
.L12:
	ret
	.cfi_endproc
.LFE17957:
	.size	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc, .-_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKc
	.section	.text._ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKNS0_12AstRawStringE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKNS0_12AstRawStringE
	.type	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKNS0_12AstRawStringE, @function
_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKNS0_12AstRawStringE:
.LFB17958:
	.cfi_startproc
	endbr64
	cmpb	$0, (%rdi)
	jne	.L14
	movb	$1, (%rdi)
	movl	%esi, 8(%rdi)
	movl	%edx, 12(%rdi)
	movl	%ecx, 16(%rdi)
	movq	%r8, 24(%rdi)
	movq	$0, 32(%rdi)
.L14:
	ret
	.cfi_endproc
.LFE17958:
	.size	_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKNS0_12AstRawStringE, .-_ZN2v88internal30PendingCompilationErrorHandler15ReportMessageAtEiiNS0_15MessageTemplateEPKNS0_12AstRawStringE
	.section	.text._ZN2v88internal30PendingCompilationErrorHandler15ReportWarningAtEiiNS0_15MessageTemplateEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal30PendingCompilationErrorHandler15ReportWarningAtEiiNS0_15MessageTemplateEPKc
	.type	_ZN2v88internal30PendingCompilationErrorHandler15ReportWarningAtEiiNS0_15MessageTemplateEPKc, @function
_ZN2v88internal30PendingCompilationErrorHandler15ReportWarningAtEiiNS0_15MessageTemplateEPKc:
.LFB17959:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	$40, %edi
	subq	$8, %rsp
	call	_Znwm@PLT
	movq	40(%rbx), %rdx
	movl	%r15d, 8(%rax)
	movl	%r14d, 12(%rax)
	movl	%r13d, 16(%rax)
	movq	$0, 24(%rax)
	movq	%r12, 32(%rax)
	movq	%rdx, (%rax)
	movq	%rax, 40(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17959:
	.size	_ZN2v88internal30PendingCompilationErrorHandler15ReportWarningAtEiiNS0_15MessageTemplateEPKc, .-_ZN2v88internal30PendingCompilationErrorHandler15ReportWarningAtEiiNS0_15MessageTemplateEPKc
	.section	.text._ZN2v88internal30PendingCompilationErrorHandler14ReportWarningsEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal30PendingCompilationErrorHandler14ReportWarningsEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEE
	.type	_ZN2v88internal30PendingCompilationErrorHandler14ReportWarningsEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEE, @function
_ZN2v88internal30PendingCompilationErrorHandler14ReportWarningsEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEE:
.LFB17960:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	40(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.L18
	movq	%rsi, %r13
	movq	%rdx, %r14
	leaq	-96(%rbp), %r12
	movabsq	$68719476736, %r15
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L31:
	movq	(%rax), %rcx
.L21:
	movl	16(%rbx), %esi
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal14MessageHandler17MakeMessageObjectEPNS0_7IsolateENS0_15MessageTemplateEPKNS0_15MessageLocationENS0_6HandleINS0_6ObjectEEENS8_INS0_10FixedArrayEEE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	movq	(%rax), %rax
	movq	%r15, 87(%rax)
	call	_ZN2v88internal14MessageHandler13ReportMessageEPNS0_7IsolateEPKNS0_15MessageLocationENS0_6HandleINS0_15JSMessageObjectEEE@PLT
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L18
.L24:
	movl	12(%rbx), %ecx
	movl	8(%rbx), %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal15MessageLocationC1ENS0_6HandleINS0_6ScriptEEEii@PLT
	movq	24(%rbx), %rax
	testq	%rax, %rax
	jne	.L31
	movq	32(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L22
	movq	%rdx, %rdi
	movq	%rdx, -120(%rbp)
	call	strlen@PLT
	movq	-120(%rbp), %rdx
	leaq	-112(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, -104(%rbp)
	movq	%rdx, -112(%rbp)
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory17NewStringFromUtf8ERKNS0_6VectorIKcEENS0_14AllocationTypeE@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	jne	.L21
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L22:
	leaq	3512(%r13), %rcx
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L18:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L32
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L32:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17960:
	.size	_ZN2v88internal30PendingCompilationErrorHandler14ReportWarningsEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEE, .-_ZN2v88internal30PendingCompilationErrorHandler14ReportWarningsEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEE
	.section	.text._ZN2v88internal30PendingCompilationErrorHandler12ReportErrorsEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEEPNS0_15AstValueFactoryE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal30PendingCompilationErrorHandler12ReportErrorsEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEEPNS0_15AstValueFactoryE
	.type	_ZN2v88internal30PendingCompilationErrorHandler12ReportErrorsEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEEPNS0_15AstValueFactoryE, @function
_ZN2v88internal30PendingCompilationErrorHandler12ReportErrorsEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEEPNS0_15AstValueFactoryE:
.LFB17961:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 1(%rdi)
	je	.L34
	movq	%rsi, %rdi
	call	_ZN2v88internal7Isolate13StackOverflowEv@PLT
.L33:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L66
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	.cfi_restore_state
	movq	%rcx, %rdi
	movq	%rdx, %r13
	call	_ZN2v88internal15AstValueFactory11InternalizeEPNS0_7IsolateE@PLT
	cmpb	$0, (%rbx)
	je	.L33
	movl	12(%rbx), %ecx
	movl	8(%rbx), %edx
	leaq	-96(%rbp), %r14
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal15MessageLocationC1ENS0_6HandleINS0_6ScriptEEEii@PLT
	movq	24(%rbx), %rax
	testq	%rax, %rax
	je	.L37
	movq	(%rax), %r15
.L38:
	movq	41472(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal5Debug14OnCompileErrorENS0_6HandleINS0_6ScriptEEE@PLT
	movl	16(%rbx), %esi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory14NewSyntaxErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	movq	(%rax), %rsi
	movq	%rax, %rbx
	testb	$1, %sil
	jne	.L67
.L65:
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L37:
	movq	32(%rbx), %r15
	testq	%r15, %r15
	je	.L39
	movq	%r15, %rdi
	call	strlen@PLT
	leaq	-112(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r15, -112(%rbp)
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal7Factory17NewStringFromUtf8ERKNS0_6VectorIKcEENS0_14AllocationTypeE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L38
.L48:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L67:
	movq	-1(%rsi), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L65
	movslq	-88(%rbp), %rsi
	movq	41112(%r12), %rdi
	leaq	3696(%r12), %r15
	salq	$32, %rsi
	testq	%rdi, %rdi
	je	.L68
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L46:
	xorl	%r8d, %r8d
	movl	$1, %r9d
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object11SetPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEES5_NS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testq	%rax, %rax
	je	.L48
	movslq	-84(%rbp), %rsi
	movq	41112(%r12), %rdi
	leaq	3680(%r12), %r15
	salq	$32, %rsi
	testq	%rdi, %rdi
	je	.L49
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L50:
	xorl	%r8d, %r8d
	movl	$1, %r9d
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object11SetPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEES5_NS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testq	%rax, %rax
	je	.L48
	xorl	%r8d, %r8d
	movl	$1, %r9d
	movq	%r13, %rcx
	movq	%rbx, %rsi
	leaq	3688(%r12), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal6Object11SetPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEES5_NS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testq	%rax, %rax
	je	.L48
	movq	(%rbx), %rsi
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L39:
	leaq	3512(%r12), %r15
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L68:
	movq	41088(%r12), %rcx
	cmpq	41096(%r12), %rcx
	je	.L69
.L47:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rcx)
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L49:
	movq	41088(%r12), %rcx
	cmpq	41096(%r12), %rcx
	je	.L70
.L51:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rcx)
	jmp	.L50
.L69:
	movq	%r12, %rdi
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L47
.L70:
	movq	%r12, %rdi
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L51
.L66:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17961:
	.size	_ZN2v88internal30PendingCompilationErrorHandler12ReportErrorsEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEEPNS0_15AstValueFactoryE, .-_ZN2v88internal30PendingCompilationErrorHandler12ReportErrorsEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEEPNS0_15AstValueFactoryE
	.section	.text._ZN2v88internal30PendingCompilationErrorHandler17ThrowPendingErrorEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal30PendingCompilationErrorHandler17ThrowPendingErrorEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEE
	.type	_ZN2v88internal30PendingCompilationErrorHandler17ThrowPendingErrorEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEE, @function
_ZN2v88internal30PendingCompilationErrorHandler17ThrowPendingErrorEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEE:
.LFB17962:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, (%rdi)
	jne	.L102
.L71:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L103
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L102:
	.cfi_restore_state
	movl	12(%rdi), %ecx
	movq	%rdx, %r13
	leaq	-96(%rbp), %r14
	movl	8(%rdi), %edx
	movq	%rdi, %rbx
	movq	%rsi, %r12
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal15MessageLocationC1ENS0_6HandleINS0_6ScriptEEEii@PLT
	movq	24(%rbx), %rax
	testq	%rax, %rax
	je	.L73
	movq	(%rax), %r15
.L74:
	movq	41472(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal5Debug14OnCompileErrorENS0_6HandleINS0_6ScriptEEE@PLT
	movl	16(%rbx), %esi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory14NewSyntaxErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	movq	(%rax), %rsi
	movq	%rax, %rbx
	testb	$1, %sil
	jne	.L104
.L101:
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L104:
	movq	-1(%rsi), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L101
	movslq	-88(%rbp), %rsi
	movq	41112(%r12), %rdi
	leaq	3696(%r12), %r15
	salq	$32, %rsi
	testq	%rdi, %rdi
	je	.L105
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L82:
	xorl	%r8d, %r8d
	movl	$1, %r9d
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object11SetPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEES5_NS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testq	%rax, %rax
	je	.L84
	movslq	-84(%rbp), %rsi
	movq	41112(%r12), %rdi
	leaq	3680(%r12), %r15
	salq	$32, %rsi
	testq	%rdi, %rdi
	je	.L85
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L86:
	xorl	%r8d, %r8d
	movl	$1, %r9d
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object11SetPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEES5_NS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testq	%rax, %rax
	je	.L84
	xorl	%r8d, %r8d
	movl	$1, %r9d
	movq	%r13, %rcx
	movq	%rbx, %rsi
	leaq	3688(%r12), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal6Object11SetPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEES5_NS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testq	%rax, %rax
	je	.L84
	movq	(%rbx), %rsi
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L73:
	movq	32(%rbx), %r15
	testq	%r15, %r15
	je	.L75
	movq	%r15, %rdi
	call	strlen@PLT
	leaq	-112(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r15, -112(%rbp)
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal7Factory17NewStringFromUtf8ERKNS0_6VectorIKcEENS0_14AllocationTypeE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L74
.L84:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L75:
	leaq	3512(%r12), %r15
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L105:
	movq	41088(%r12), %rcx
	cmpq	41096(%r12), %rcx
	je	.L106
.L83:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rcx)
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L85:
	movq	41088(%r12), %rcx
	cmpq	41096(%r12), %rcx
	je	.L107
.L87:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rcx)
	jmp	.L86
.L106:
	movq	%r12, %rdi
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L83
.L107:
	movq	%r12, %rdi
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L87
.L103:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17962:
	.size	_ZN2v88internal30PendingCompilationErrorHandler17ThrowPendingErrorEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEE, .-_ZN2v88internal30PendingCompilationErrorHandler17ThrowPendingErrorEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEE
	.section	.text._ZNK2v88internal30PendingCompilationErrorHandler25FormatErrorMessageForTestEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal30PendingCompilationErrorHandler25FormatErrorMessageForTestEPNS0_7IsolateE
	.type	_ZNK2v88internal30PendingCompilationErrorHandler25FormatErrorMessageForTestEPNS0_7IsolateE, @function
_ZNK2v88internal30PendingCompilationErrorHandler25FormatErrorMessageForTestEPNS0_7IsolateE:
.LFB17963:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	testq	%rax, %rax
	je	.L109
	movq	(%rax), %rdx
.L110:
	movl	16(%rbx), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal16MessageFormatter6FormatEPNS0_7IsolateENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEE@PLT
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L115
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L109:
	.cfi_restore_state
	movq	32(%rdi), %r13
	testq	%r13, %r13
	je	.L111
	movq	%r13, %rdi
	call	strlen@PLT
	xorl	%edx, %edx
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r13, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal7Factory17NewStringFromUtf8ERKNS0_6VectorIKcEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	jne	.L110
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L111:
	leaq	3512(%rsi), %rdx
	jmp	.L110
.L115:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17963:
	.size	_ZNK2v88internal30PendingCompilationErrorHandler25FormatErrorMessageForTestEPNS0_7IsolateE, .-_ZNK2v88internal30PendingCompilationErrorHandler25FormatErrorMessageForTestEPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZNK2v88internal30PendingCompilationErrorHandler14MessageDetails14ArgumentStringEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZNK2v88internal30PendingCompilationErrorHandler14MessageDetails14ArgumentStringEPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZNK2v88internal30PendingCompilationErrorHandler14MessageDetails14ArgumentStringEPNS0_7IsolateE:
.LFB21716:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE21716:
	.size	_GLOBAL__sub_I__ZNK2v88internal30PendingCompilationErrorHandler14MessageDetails14ArgumentStringEPNS0_7IsolateE, .-_GLOBAL__sub_I__ZNK2v88internal30PendingCompilationErrorHandler14MessageDetails14ArgumentStringEPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZNK2v88internal30PendingCompilationErrorHandler14MessageDetails14ArgumentStringEPNS0_7IsolateE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
