	.file	"embedded-data.cc"
	.text
	.section	.text._ZN2v88internal17InstructionStream11PcIsOffHeapEPNS0_7IsolateEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17InstructionStream11PcIsOffHeapEPNS0_7IsolateEm
	.type	_ZN2v88internal17InstructionStream11PcIsOffHeapEPNS0_7IsolateEm, @function
_ZN2v88internal17InstructionStream11PcIsOffHeapEPNS0_7IsolateEm:
.LFB21908:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	_ZNK2v88internal7Isolate13embedded_blobEv@PLT
	movq	%rax, %r12
	xorl	%eax, %eax
	cmpq	%rbx, %r12
	jbe	.L7
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZNK2v88internal7Isolate18embedded_blob_sizeEv@PLT
	movl	%eax, %eax
	addq	%rax, %r12
	cmpq	%rbx, %r12
	seta	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21908:
	.size	_ZN2v88internal17InstructionStream11PcIsOffHeapEPNS0_7IsolateEm, .-_ZN2v88internal17InstructionStream11PcIsOffHeapEPNS0_7IsolateEm
	.section	.rodata._ZN2v88internal17InstructionStream13TryLookupCodeEPNS0_7IsolateEm.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal17InstructionStream13TryLookupCodeEPNS0_7IsolateEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17InstructionStream13TryLookupCodeEPNS0_7IsolateEm
	.type	_ZN2v88internal17InstructionStream13TryLookupCodeEPNS0_7IsolateEm, @function
_ZN2v88internal17InstructionStream13TryLookupCodeEPNS0_7IsolateEm:
.LFB21909:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	_ZNK2v88internal7Isolate13embedded_blobEv@PLT
	cmpq	%rax, %rbx
	jnb	.L22
.L19:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L22:
	.cfi_restore_state
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZNK2v88internal7Isolate18embedded_blob_sizeEv@PLT
	movl	%eax, %eax
	addq	%rax, %r13
	cmpq	%r13, %rbx
	jnb	.L19
	call	_ZN2v88internal7Isolate23CurrentEmbeddedBlobSizeEv@PLT
	call	_ZN2v88internal7Isolate19CurrentEmbeddedBlobEv@PLT
	movq	%rax, %r8
	movl	16(%rax), %eax
	leaq	12448(%r8,%rax), %rax
	cmpq	%rax, %rbx
	jb	.L19
	movl	$1553, %ecx
	xorl	%edi, %edi
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L18:
	movl	%esi, %ecx
.L15:
	cmpl	%edi, %ecx
	jle	.L13
	leal	(%rcx,%rdi), %esi
	sarl	%esi
	movslq	%esi, %rax
	leaq	16(%r8,%rax,8), %rax
	movl	(%rax), %edx
	movl	4(%rax), %eax
	leaq	12448(%r8,%rdx), %rdx
	movq	%rdx, %r9
	testl	%eax, %eax
	je	.L14
	addl	$32, %eax
	andl	$-32, %eax
	leaq	(%rax,%rdx), %r9
.L14:
	cmpq	%rdx, %rbx
	jb	.L18
	cmpq	%rbx, %r9
	ja	.L16
	leal	1(%rsi), %edi
	jmp	.L15
.L16:
	addq	$8, %rsp
	leaq	41184(%r12), %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8Builtins7builtinEi@PLT
.L13:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21909:
	.size	_ZN2v88internal17InstructionStream13TryLookupCodeEPNS0_7IsolateEm, .-_ZN2v88internal17InstructionStream13TryLookupCodeEPNS0_7IsolateEm
	.section	.rodata._ZN2v88internal17InstructionStream28FreeOffHeapInstructionStreamEPhj.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"FreePages(page_allocator, data, RoundUp(size, page_size))"
	.section	.rodata._ZN2v88internal17InstructionStream28FreeOffHeapInstructionStreamEPhj.str1.1,"aMS",@progbits,1
.LC2:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal17InstructionStream28FreeOffHeapInstructionStreamEPhj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17InstructionStream28FreeOffHeapInstructionStreamEPhj
	.type	_ZN2v88internal17InstructionStream28FreeOffHeapInstructionStreamEPhj, @function
_ZN2v88internal17InstructionStream28FreeOffHeapInstructionStreamEPhj:
.LFB21911:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	%esi, %ebx
	subq	$8, %rsp
	call	_ZN2v88internal24GetPlatformPageAllocatorEv@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	movq	%r13, %rsi
	movq	%r12, %rdi
	leal	-1(%rbx,%rax), %edx
	negl	%eax
	andl	%eax, %edx
	call	_ZN2v88internal9FreePagesEPNS_13PageAllocatorEPvm@PLT
	testb	%al, %al
	je	.L26
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21911:
	.size	_ZN2v88internal17InstructionStream28FreeOffHeapInstructionStreamEPhj, .-_ZN2v88internal17InstructionStream28FreeOffHeapInstructionStreamEPhj
	.section	.text._ZNK2v88internal12EmbeddedData25InstructionStartOfBuiltinEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal12EmbeddedData25InstructionStartOfBuiltinEi
	.type	_ZNK2v88internal12EmbeddedData25InstructionStartOfBuiltinEi, @function
_ZNK2v88internal12EmbeddedData25InstructionStartOfBuiltinEi:
.LFB21922:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movslq	%esi, %rsi
	movl	16(%rax,%rsi,8), %edx
	leaq	12448(%rax,%rdx), %rax
	ret
	.cfi_endproc
.LFE21922:
	.size	_ZNK2v88internal12EmbeddedData25InstructionStartOfBuiltinEi, .-_ZNK2v88internal12EmbeddedData25InstructionStartOfBuiltinEi
	.section	.text._ZNK2v88internal12EmbeddedData24InstructionSizeOfBuiltinEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal12EmbeddedData24InstructionSizeOfBuiltinEi
	.type	_ZNK2v88internal12EmbeddedData24InstructionSizeOfBuiltinEi, @function
_ZNK2v88internal12EmbeddedData24InstructionSizeOfBuiltinEi:
.LFB21923:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movslq	%esi, %rsi
	movl	20(%rax,%rsi,8), %eax
	ret
	.cfi_endproc
.LFE21923:
	.size	_ZNK2v88internal12EmbeddedData24InstructionSizeOfBuiltinEi, .-_ZNK2v88internal12EmbeddedData24InstructionSizeOfBuiltinEi
	.section	.text._ZNK2v88internal12EmbeddedData34InstructionStartOfBytecodeHandlersEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal12EmbeddedData34InstructionStartOfBytecodeHandlersEv
	.type	_ZNK2v88internal12EmbeddedData34InstructionStartOfBytecodeHandlersEv, @function
_ZNK2v88internal12EmbeddedData34InstructionStartOfBytecodeHandlersEv:
.LFB21924:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movl	8576(%rax), %edx
	leaq	12448(%rax,%rdx), %rax
	ret
	.cfi_endproc
.LFE21924:
	.size	_ZNK2v88internal12EmbeddedData34InstructionStartOfBytecodeHandlersEv, .-_ZNK2v88internal12EmbeddedData34InstructionStartOfBytecodeHandlersEv
	.section	.text._ZNK2v88internal12EmbeddedData32InstructionEndOfBytecodeHandlersEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal12EmbeddedData32InstructionEndOfBytecodeHandlersEv
	.type	_ZNK2v88internal12EmbeddedData32InstructionEndOfBytecodeHandlersEv, @function
_ZNK2v88internal12EmbeddedData32InstructionEndOfBytecodeHandlersEv:
.LFB21925:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movl	12432(%rdx), %eax
	leaq	12448(%rdx,%rax), %rax
	movl	12436(%rdx), %edx
	addq	%rdx, %rax
	ret
	.cfi_endproc
.LFE21925:
	.size	_ZNK2v88internal12EmbeddedData32InstructionEndOfBytecodeHandlersEv, .-_ZNK2v88internal12EmbeddedData32InstructionEndOfBytecodeHandlersEv
	.section	.text._ZNK2v88internal12EmbeddedData22CreateEmbeddedBlobHashEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal12EmbeddedData22CreateEmbeddedBlobHashEv
	.type	_ZNK2v88internal12EmbeddedData22CreateEmbeddedBlobHashEv, @function
_ZNK2v88internal12EmbeddedData22CreateEmbeddedBlobHashEv:
.LFB21926:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	(%rdi), %rbx
	movl	8(%rdi), %r12d
	xorl	%edi, %edi
	addq	%rbx, %r12
	addq	$8, %rbx
	cmpq	%rbx, %r12
	je	.L31
	.p2align 4,,10
	.p2align 3
.L33:
	call	_ZN2v84base10hash_valueEm@PLT
	movzbl	(%rbx), %esi
	xorl	%edi, %edi
	addq	$1, %rbx
	movq	%rax, %r13
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%rax, %rdi
	cmpq	%rbx, %r12
	jne	.L33
.L31:
	addq	$8, %rsp
	movq	%rdi, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21926:
	.size	_ZNK2v88internal12EmbeddedData22CreateEmbeddedBlobHashEv, .-_ZNK2v88internal12EmbeddedData22CreateEmbeddedBlobHashEv
	.section	.text._ZSt13__adjust_heapIPiliN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_,"axG",@progbits,_ZSt13__adjust_heapIPiliN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_,comdat
	.p2align 4
	.weak	_ZSt13__adjust_heapIPiliN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_
	.type	_ZSt13__adjust_heapIPiliN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_, @function
_ZSt13__adjust_heapIPiliN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_:
.LFB26115:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	-1(%rdx), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rax, %rbx
	andl	$1, %r12d
	shrq	$63, %rbx
	addq	%rax, %rbx
	sarq	%rbx
	cmpq	%rbx, %rsi
	jge	.L38
	movq	%rsi, %r10
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L51:
	subq	$1, %rax
	leaq	(%rdi,%rax,4), %r9
	movl	(%r9), %r8d
	movl	%r8d, (%rdi,%r10,4)
	cmpq	%rbx, %rax
	jge	.L40
.L41:
	movq	%rax, %r10
.L42:
	leaq	1(%r10), %r8
	leaq	(%r8,%r8), %rax
	salq	$3, %r8
	leaq	(%rdi,%r8), %r9
	movl	(%r9), %r11d
	cmpl	-4(%rdi,%r8), %r11d
	jl	.L51
	movl	%r11d, (%rdi,%r10,4)
	cmpq	%rbx, %rax
	jl	.L41
.L40:
	testq	%r12, %r12
	je	.L46
.L43:
	leaq	-1(%rax), %rdx
	movq	%rdx, %r8
	shrq	$63, %r8
	addq	%rdx, %r8
	sarq	%r8
	cmpq	%rsi, %rax
	jg	.L45
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L53:
	movl	%edx, (%r9)
	leaq	-1(%r8), %rdx
	movq	%rdx, %rax
	shrq	$63, %rax
	addq	%rdx, %rax
	sarq	%rax
	movq	%rax, %rdx
	movq	%r8, %rax
	cmpq	%r8, %rsi
	jge	.L52
	movq	%rdx, %r8
.L45:
	leaq	(%rdi,%r8,4), %r10
	leaq	(%rdi,%rax,4), %r9
	movl	(%r10), %edx
	cmpl	%edx, %ecx
	jg	.L53
.L44:
	movl	%ecx, (%r9)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_restore_state
	leaq	(%rdi,%rsi,4), %r9
	testq	%r12, %r12
	jne	.L44
	movq	%rsi, %rax
	.p2align 4,,10
	.p2align 3
.L46:
	leaq	-2(%rdx), %r8
	movq	%r8, %rdx
	shrq	$63, %rdx
	addq	%r8, %rdx
	sarq	%rdx
	cmpq	%rax, %rdx
	jne	.L43
	leaq	2(%rax,%rax), %rax
	movl	-4(%rdi,%rax,4), %edx
	subq	$1, %rax
	movl	%edx, (%r9)
	leaq	(%rdi,%rax,4), %r9
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L52:
	movq	%r10, %r9
	movl	%ecx, (%r9)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26115:
	.size	_ZSt13__adjust_heapIPiliN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_, .-_ZSt13__adjust_heapIPiliN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_
	.section	.text._ZSt16__introsort_loopIPilN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZSt16__introsort_loopIPilN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0, @function
_ZSt16__introsort_loopIPilN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0:
.LFB27016:
	.cfi_startproc
	movq	%rsi, %rax
	subq	%rdi, %rax
	cmpq	$64, %rax
	jle	.L80
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	4(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	testq	%rdx, %rdx
	je	.L85
.L58:
	sarq	$3, %rax
	movl	4(%rbx), %edi
	movl	-4(%rsi), %edx
	subq	$1, %r15
	leaq	(%rbx,%rax,4), %r8
	movl	(%rbx), %ecx
	movl	(%r8), %eax
	cmpl	%eax, %edi
	jge	.L61
	cmpl	%edx, %eax
	jl	.L66
	cmpl	%edx, %edi
	jl	.L83
.L84:
	movl	%edi, (%rbx)
	movl	%ecx, 4(%rbx)
	movl	-4(%rsi), %ecx
.L63:
	movq	%r13, %r12
	movq	%rsi, %rax
	.p2align 4,,10
	.p2align 3
.L67:
	movl	(%r12), %edx
	movq	%r12, %r14
	cmpl	%edi, %edx
	jl	.L68
	subq	$4, %rax
	cmpl	%ecx, %edi
	jge	.L69
	.p2align 4,,10
	.p2align 3
.L70:
	movl	-4(%rax), %ecx
	subq	$4, %rax
	cmpl	%edi, %ecx
	jg	.L70
.L69:
	cmpq	%rax, %r12
	jnb	.L86
	movl	%ecx, (%r12)
	movl	-4(%rax), %ecx
	movl	%edx, (%rax)
	movl	(%rbx), %edi
.L68:
	addq	$4, %r12
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L61:
	cmpl	%edx, %edi
	jl	.L84
	cmpl	%edx, %eax
	jge	.L66
.L83:
	movl	%edx, (%rbx)
	movl	%ecx, -4(%rsi)
	movl	(%rbx), %edi
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L86:
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	_ZSt16__introsort_loopIPilN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0
	movq	%r12, %rax
	subq	%rbx, %rax
	cmpq	$64, %rax
	jle	.L54
	testq	%r15, %r15
	je	.L56
	movq	%r12, %rsi
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L66:
	movl	%eax, (%rbx)
	movl	%ecx, (%r8)
	movl	(%rbx), %edi
	movl	-4(%rsi), %ecx
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L85:
	movq	%rsi, %r14
.L56:
	sarq	$2, %rax
	movq	%rbx, %rdi
	leaq	-2(%rax), %r13
	movq	%rax, %rdx
	movq	%rax, %r12
	sarq	%r13
	movl	(%rbx,%r13,4), %ecx
	movq	%r13, %rsi
	call	_ZSt13__adjust_heapIPiliN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_
.L59:
	subq	$1, %r13
	movq	%r12, %rdx
	movq	%rbx, %rdi
	movl	(%rbx,%r13,4), %ecx
	movq	%r13, %rsi
	call	_ZSt13__adjust_heapIPiliN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_
	testq	%r13, %r13
	jne	.L59
	.p2align 4,,10
	.p2align 3
.L60:
	subq	$4, %r14
	movl	(%rbx), %eax
	movl	(%r14), %ecx
	xorl	%esi, %esi
	movq	%r14, %r12
	movq	%rbx, %rdi
	subq	%rbx, %r12
	movl	%eax, (%r14)
	movq	%r12, %rdx
	sarq	$2, %rdx
	call	_ZSt13__adjust_heapIPiliN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_
	cmpq	$4, %r12
	jg	.L60
.L54:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L80:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE27016:
	.size	_ZSt16__introsort_loopIPilN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0, .-_ZSt16__introsort_loopIPilN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0
	.section	.rodata._ZNK2v88internal12EmbeddedData15PrintStatisticsEv.str1.1,"aMS",@progbits,1
.LC3:
	.string	"EmbeddedData:\n"
	.section	.rodata._ZNK2v88internal12EmbeddedData15PrintStatisticsEv.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"  Total size:                         %d\n"
	.align 8
.LC5:
	.string	"  Metadata size:                      %d\n"
	.align 8
.LC6:
	.string	"  Instruction size:                   %d\n"
	.align 8
.LC7:
	.string	"  Padding:                            %d\n"
	.align 8
.LC8:
	.string	"  Embedded builtin count:             %d\n"
	.align 8
.LC9:
	.string	"  Instruction size (50th percentile): %d\n"
	.align 8
.LC10:
	.string	"  Instruction size (75th percentile): %d\n"
	.align 8
.LC11:
	.string	"  Instruction size (90th percentile): %d\n"
	.align 8
.LC12:
	.string	"  Instruction size (99th percentile): %d\n"
	.section	.rodata._ZNK2v88internal12EmbeddedData15PrintStatisticsEv.str1.1
.LC13:
	.string	"\n"
	.section	.text._ZNK2v88internal12EmbeddedData15PrintStatisticsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal12EmbeddedData15PrintStatisticsEv
	.type	_ZNK2v88internal12EmbeddedData15PrintStatisticsEv, @function
_ZNK2v88internal12EmbeddedData15PrintStatisticsEv:
.LFB21927:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$2152, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	leaq	-6272(%rbp), %r14
	movq	%rdi, %r13
	leaq	-60(%rbp), %r12
	leaq	12440(%rax), %rdx
	cmpq	%rdx, %r14
	jnb	.L104
	leaq	20(%rax), %rdx
	cmpq	%rdx, %r12
	ja	.L101
.L104:
	xorl	%edx, %edx
	pxor	%xmm0, %xmm0
	.p2align 4,,10
	.p2align 3
.L90:
	movdqu	20(%rax,%rdx,2), %xmm1
	movdqu	36(%rax,%rdx,2), %xmm2
	shufps	$136, %xmm2, %xmm1
	movaps	%xmm1, (%r14,%rdx)
	addq	$16, %rdx
	paddd	%xmm1, %xmm0
	cmpq	$6208, %rdx
	jne	.L90
	movdqa	%xmm0, %xmm1
	movl	12436(%rax), %edx
	psrldq	$8, %xmm1
	paddd	%xmm1, %xmm0
	movl	%edx, -64(%rbp)
	movdqa	%xmm0, %xmm1
	psrldq	$4, %xmm1
	paddd	%xmm1, %xmm0
	movd	%xmm0, %eax
	leal	(%rax,%rdx), %ebx
.L91:
	movl	$20, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZSt16__introsort_loopIPilN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_.isra.0
	leaq	-6208(%rbp), %rax
	leaq	-6268(%rbp), %r15
	movq	%rax, -6288(%rbp)
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L115:
	movq	%r15, %rdx
	movl	$4, %eax
	movq	%r14, %rsi
	movl	%r9d, -6276(%rbp)
	subq	%r14, %rdx
	leaq	(%r14,%rax), %rdi
	addq	$4, %r15
	call	memmove@PLT
	movl	-6276(%rbp), %r9d
	movl	%r9d, -6272(%rbp)
	cmpq	-6288(%rbp), %r15
	je	.L114
.L96:
	movl	(%r15), %r9d
	cmpl	-6272(%rbp), %r9d
	jl	.L115
	movl	-4(%r15), %edx
	leaq	-4(%r15), %rax
	cmpl	%edx, %r9d
	jge	.L102
	.p2align 4,,10
	.p2align 3
.L95:
	movl	%edx, 4(%rax)
	movq	%rax, %rsi
	movl	-4(%rax), %edx
	subq	$4, %rax
	cmpl	%edx, %r9d
	jl	.L95
.L94:
	movl	%r9d, (%rsi)
	addq	$4, %r15
	cmpq	-6288(%rbp), %r15
	jne	.L96
.L114:
	movq	%r15, %rsi
	.p2align 4,,10
	.p2align 3
.L99:
	movl	(%rsi), %ecx
	movl	-4(%rsi), %edx
	leaq	-4(%rsi), %rax
	cmpl	%edx, %ecx
	jge	.L103
	.p2align 4,,10
	.p2align 3
.L98:
	movl	%edx, 4(%rax)
	movq	%rax, %rdi
	movl	-4(%rax), %edx
	subq	$4, %rax
	cmpl	%edx, %ecx
	jl	.L98
	addq	$4, %rsi
	movl	%ecx, (%rdi)
	cmpq	%r12, %rsi
	jne	.L99
.L117:
	xorl	%eax, %eax
	leaq	.LC3(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movl	8(%r13), %esi
	xorl	%eax, %eax
	leaq	.LC4(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	xorl	%eax, %eax
	movl	$12440, %esi
	leaq	.LC5(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	xorl	%eax, %eax
	movl	%ebx, %esi
	leaq	.LC6(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movl	8(%r13), %esi
	xorl	%eax, %eax
	leaq	.LC7(%rip), %rdi
	subl	$12440, %esi
	subl	%ebx, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	xorl	%eax, %eax
	movl	$1553, %esi
	leaq	.LC8(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movl	-3168(%rbp), %esi
	xorl	%eax, %eax
	leaq	.LC9(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movl	-1616(%rbp), %esi
	xorl	%eax, %eax
	leaq	.LC10(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movl	-684(%rbp), %esi
	xorl	%eax, %eax
	leaq	.LC11(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movl	-124(%rbp), %esi
	xorl	%eax, %eax
	leaq	.LC12(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	xorl	%eax, %eax
	leaq	.LC13(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L116
	addq	$6248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L101:
	.cfi_restore_state
	xorl	%edx, %edx
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L88:
	movl	20(%rax,%rdx,8), %esi
	movl	%esi, (%r14,%rdx,4)
	addq	$1, %rdx
	addl	%esi, %ebx
	cmpq	$1553, %rdx
	jne	.L88
	jmp	.L91
.L103:
	movq	%rsi, %rdi
	addq	$4, %rsi
	movl	%ecx, (%rdi)
	cmpq	%r12, %rsi
	jne	.L99
	jmp	.L117
.L102:
	movq	%r15, %rsi
	jmp	.L94
.L116:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21927:
	.size	_ZNK2v88internal12EmbeddedData15PrintStatisticsEv, .-_ZNK2v88internal12EmbeddedData15PrintStatisticsEv
	.section	.rodata._ZN2v88internal12EmbeddedData11FromIsolateEPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC14:
	.string	"%s is not isolate-independent.\n"
	.align 8
.LC15:
	.string	"%s is a wasm runtime stub but needs relocation.\n"
	.align 8
.LC16:
	.string	"%s aliases the off-heap trampoline register.\n"
	.align 8
.LC17:
	.string	"One or more builtins marked as isolate-independent either contains isolate-dependent code or aliases the off-heap trampoline register. If in doubt, ask jgruber@"
	.align 8
.LC18:
	.string	"address < start || address >= end"
	.align 8
.LC19:
	.string	"Builtins::IsIsolateIndependentBuiltin(target)"
	.section	.text._ZN2v88internal12EmbeddedData11FromIsolateEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12EmbeddedData11FromIsolateEPNS0_7IsolateE
	.type	_ZN2v88internal12EmbeddedData11FromIsolateEPNS0_7IsolateE, @function
_ZN2v88internal12EmbeddedData11FromIsolateEPNS0_7IsolateE:
.LFB21921:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	41184(%rdi), %r14
	movl	$12424, %edi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%rax, %r12
	leaq	12416(%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L119:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L119
	leaq	-192(%rbp), %rax
	movb	$0, -240(%rbp)
	xorl	%ebx, %ebx
	movq	$0, 12416(%r12)
	movl	$0, -216(%rbp)
	movq	%rax, -232(%rbp)
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L126:
	movq	-192(%rbp), %rax
	movl	-216(%rbp), %ecx
	movl	39(%rax), %eax
	movl	%ecx, (%r12,%rbx,8)
	movl	%eax, 4(%r12,%rbx,8)
	addl	$32, %eax
	addq	$1, %rbx
	andl	$-32, %eax
	addl	%eax, %ecx
	movl	%ecx, -216(%rbp)
	cmpq	$1553, %rbx
	je	.L163
.L127:
	movl	%ebx, %esi
	movq	%r14, %rdi
	movl	%ebx, %r13d
	call	_ZN2v88internal8Builtins7builtinEi@PLT
	movq	-232(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal4Code20IsIsolateIndependentEPNS0_7IsolateE@PLT
	testb	%al, %al
	je	.L164
.L120:
	movl	%r13d, %edi
	call	_ZN2v88internal8Builtins17IsWasmRuntimeStubEi@PLT
	testb	%al, %al
	jne	.L165
.L121:
	movq	-192(%rbp), %rdx
	movl	59(%rdx), %edi
	movq	%rdx, -224(%rbp)
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	subl	$5, %eax
	cmpl	$1, %eax
	jbe	.L126
	movq	-224(%rbp), %rdx
	leaq	-128(%rbp), %r8
	movq	%r15, %rsi
	movq	%r8, %rdi
	movl	59(%rdx), %edx
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-112(%rbp), %rdx
	movq	%rdx, -224(%rbp)
	call	_ZN2v88internal23CallInterfaceDescriptor15ContextRegisterEv@PLT
	cmpl	$10, %eax
	je	.L123
	movq	-224(%rbp), %rdx
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jle	.L126
	movq	24(%rdx), %rax
	leal	-1(%rcx), %edx
	leaq	4(%rax,%rdx,4), %rdx
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L166:
	addq	$4, %rax
	cmpq	%rax, %rdx
	je	.L126
.L125:
	cmpl	$10, (%rax)
	jne	.L166
.L123:
	movl	%r13d, %edi
	call	_ZN2v88internal8Builtins4nameEi@PLT
	movq	stderr(%rip), %rdi
	movl	$1, %esi
	leaq	.LC16(%rip), %rdx
	movq	%rax, %rcx
	xorl	%eax, %eax
	call	__fprintf_chk@PLT
	movb	$1, -240(%rbp)
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L165:
	movq	-192(%rbp), %rdi
	call	_ZN2v88internal9RelocInfo18RequiresRelocationENS0_4CodeE@PLT
	testb	%al, %al
	je	.L121
	movl	%r13d, %edi
	movb	%al, -224(%rbp)
	call	_ZN2v88internal8Builtins4nameEi@PLT
	movq	stderr(%rip), %rdi
	movl	$1, %esi
	leaq	.LC15(%rip), %rdx
	movq	%rax, %rcx
	xorl	%eax, %eax
	call	__fprintf_chk@PLT
	movzbl	-224(%rbp), %r8d
	movb	%r8b, -240(%rbp)
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L164:
	movl	%ebx, %edi
	call	_ZN2v88internal8Builtins4nameEi@PLT
	movq	stderr(%rip), %rdi
	movl	$1, %esi
	leaq	.LC14(%rip), %rdx
	movq	%rax, %rcx
	xorl	%eax, %eax
	call	__fprintf_chk@PLT
	movb	$1, -240(%rbp)
	jmp	.L120
.L163:
	cmpb	$0, -240(%rbp)
	jne	.L167
	leal	12448(%rcx), %edx
	movq	%rdx, %rdi
	movq	%rdx, -216(%rbp)
	movq	%rdx, %r13
	call	_Znam@PLT
	movq	-216(%rbp), %rdx
	movl	$204, %esi
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	memset@PLT
	movq	%r15, %rdi
	xorl	%r15d, %r15d
	call	_ZN2v88internal7Isolate26HashIsolateForEmbeddedBlobEv@PLT
	leaq	16(%rbx), %rdi
	movl	$12424, %edx
	movq	%r12, %rsi
	movq	%rax, 8(%rbx)
	call	memcpy@PLT
	.p2align 4,,10
	.p2align 3
.L129:
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8Builtins7builtinEi@PLT
	movl	(%r12,%r15,8), %edx
	addq	$1, %r15
	leaq	63(%rax), %rsi
	leaq	12448(%rbx,%rdx), %rdi
	movslq	39(%rax), %rdx
	call	memcpy@PLT
	cmpq	$1553, %r15
	jne	.L129
	leaq	-208(%rbp), %rax
	leaq	-128(%rbp), %r8
	movq	%rbx, -208(%rbp)
	movq	%rax, -256(%rbp)
	leaq	-112(%rbp), %rax
	movl	%r13d, -200(%rbp)
	movl	$0, -244(%rbp)
	movq	%rax, -240(%rbp)
.L134:
	movl	-244(%rbp), %esi
	movq	%r14, %rdi
	movq	%r8, -216(%rbp)
	call	_ZN2v88internal8Builtins7builtinEi@PLT
	movq	-232(%rbp), %rdi
	movl	$3, %edx
	movq	%rax, %rsi
	movq	%rax, %r13
	call	_ZN2v88internal13RelocIteratorC1ENS0_4CodeEi@PLT
	movq	-216(%rbp), %r8
	movl	$3, %ecx
	movq	%r13, %rdx
	movq	-256(%rbp), %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal13RelocIteratorC1EPNS0_12EmbeddedDataENS0_4CodeEi@PLT
	cmpb	$0, -136(%rbp)
	movq	-216(%rbp), %r8
	jne	.L130
.L133:
	movq	-176(%rbp), %rax
	movq	%r8, -224(%rbp)
	movslq	(%rax), %r13
	addq	%rax, %r13
	call	_ZN2v88internal7Isolate19CurrentEmbeddedBlobEv@PLT
	leaq	4(%r13), %r15
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal7Isolate23CurrentEmbeddedBlobSizeEv@PLT
	movq	-216(%rbp), %rdx
	movq	-224(%rbp), %r8
	movl	%eax, %eax
	addq	%rdx, %rax
	cmpq	%rax, %r15
	jnb	.L131
	cmpq	%r15, %rdx
	jbe	.L168
.L131:
	leaq	-59(%r13), %rdi
	movq	%r8, -216(%rbp)
	call	_ZN2v88internal8Builtins27IsIsolateIndependentBuiltinENS0_4CodeE@PLT
	movq	-216(%rbp), %r8
	testb	%al, %al
	je	.L169
	movq	-208(%rbp), %rax
	movslq	0(%r13), %rdx
	xorl	%ecx, %ecx
	movq	%r8, -216(%rbp)
	movq	-240(%rbp), %rdi
	movl	16(%rax,%rdx,8), %edx
	leaq	12448(%rax,%rdx), %rsi
	xorl	%edx, %edx
	call	_ZN2v88internal9RelocInfo18set_target_addressEmNS0_16WriteBarrierModeENS0_15ICacheFlushModeE@PLT
	movq	-232(%rbp), %rdi
	call	_ZN2v88internal13RelocIterator4nextEv@PLT
	movq	-216(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN2v88internal13RelocIterator4nextEv@PLT
	cmpb	$0, -136(%rbp)
	movq	-216(%rbp), %r8
	je	.L133
.L130:
	addl	$1, -244(%rbp)
	movl	-244(%rbp), %eax
	cmpl	$1553, %eax
	jne	.L134
	movq	-208(%rbp), %r13
	movl	-200(%rbp), %r14d
	xorl	%edi, %edi
	addq	%r13, %r14
	addq	$8, %r13
	cmpq	%r13, %r14
	je	.L135
.L136:
	call	_ZN2v84base10hash_valueEm@PLT
	movzbl	0(%r13), %esi
	xorl	%edi, %edi
	addq	$1, %r13
	movq	%rax, %r15
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%rax, %rdi
	cmpq	%r13, %r14
	jne	.L136
.L135:
	cmpb	$0, _ZN2v88internal29FLAG_serialization_statisticsE(%rip)
	movq	%rdi, (%rbx)
	jne	.L170
.L137:
	movq	%r12, %rdi
	movq	-208(%rbp), %r14
	movq	-200(%rbp), %r15
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L171
	addq	$216, %rsp
	movq	%r14, %rax
	movq	%r15, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L168:
	.cfi_restore_state
	leaq	.LC18(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L169:
	leaq	.LC19(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L167:
	leaq	.LC17(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L170:
	movq	-256(%rbp), %rdi
	call	_ZNK2v88internal12EmbeddedData15PrintStatisticsEv
	jmp	.L137
.L171:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21921:
	.size	_ZN2v88internal12EmbeddedData11FromIsolateEPNS0_7IsolateE, .-_ZN2v88internal12EmbeddedData11FromIsolateEPNS0_7IsolateE
	.section	.rodata._ZN2v88internal17InstructionStream30CreateOffHeapInstructionStreamEPNS0_7IsolateEPPhPj.str1.1,"aMS",@progbits,1
.LC20:
	.string	"(allocated_bytes) != nullptr"
	.section	.rodata._ZN2v88internal17InstructionStream30CreateOffHeapInstructionStreamEPNS0_7IsolateEPPhPj.str1.8,"aMS",@progbits,1
	.align 8
.LC21:
	.string	"SetPermissions(page_allocator, allocated_bytes, allocation_size, PageAllocator::kReadExecute)"
	.section	.text._ZN2v88internal17InstructionStream30CreateOffHeapInstructionStreamEPNS0_7IsolateEPPhPj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17InstructionStream30CreateOffHeapInstructionStreamEPNS0_7IsolateEPPhPj
	.type	_ZN2v88internal17InstructionStream30CreateOffHeapInstructionStreamEPNS0_7IsolateEPPhPj, @function
_ZN2v88internal17InstructionStream30CreateOffHeapInstructionStreamEPNS0_7IsolateEPPhPj:
.LFB21910:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal12EmbeddedData11FromIsolateEPNS0_7IsolateE
	movq	%rdx, %r12
	movq	%rax, %r13
	call	_ZN2v88internal24GetPlatformPageAllocatorEv@PLT
	movq	%rax, %r15
	movq	(%rax), %rax
	movq	%r15, %rdi
	call	*16(%rax)
	movl	%eax, %ecx
	movq	%rax, %rbx
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal17GetRandomMmapAddrEv@PLT
	movq	-56(%rbp), %rcx
	leal	-1(%r12,%rbx), %r10d
	negl	%ebx
	movl	%r10d, %r14d
	movl	$2, %r8d
	movq	%r15, %rdi
	movq	%rcx, %rsi
	andl	%ebx, %r14d
	negq	%rsi
	movq	%r14, %rdx
	andq	%rax, %rsi
	call	_ZN2v88internal13AllocatePagesEPNS_13PageAllocatorEPvmmNS1_10PermissionE@PLT
	testq	%rax, %rax
	je	.L176
	movq	%rax, %rbx
	movl	%r12d, %edx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	memcpy@PLT
	movl	$4, %ecx
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal14SetPermissionsEPNS_13PageAllocatorEPvmNS1_10PermissionE@PLT
	testb	%al, %al
	je	.L177
	movq	-64(%rbp), %rax
	movq	%r13, %rdi
	movq	%rbx, (%rax)
	movq	-72(%rbp), %rax
	movl	%r12d, (%rax)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdaPv@PLT
	.p2align 4,,10
	.p2align 3
.L176:
	.cfi_restore_state
	leaq	.LC20(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L177:
	leaq	.LC21(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21910:
	.size	_ZN2v88internal17InstructionStream30CreateOffHeapInstructionStreamEPNS0_7IsolateEPPhPj, .-_ZN2v88internal17InstructionStream30CreateOffHeapInstructionStreamEPNS0_7IsolateEPPhPj
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal17InstructionStream11PcIsOffHeapEPNS0_7IsolateEm,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal17InstructionStream11PcIsOffHeapEPNS0_7IsolateEm, @function
_GLOBAL__sub_I__ZN2v88internal17InstructionStream11PcIsOffHeapEPNS0_7IsolateEm:
.LFB26965:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE26965:
	.size	_GLOBAL__sub_I__ZN2v88internal17InstructionStream11PcIsOffHeapEPNS0_7IsolateEm, .-_GLOBAL__sub_I__ZN2v88internal17InstructionStream11PcIsOffHeapEPNS0_7IsolateEm
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal17InstructionStream11PcIsOffHeapEPNS0_7IsolateEm
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
