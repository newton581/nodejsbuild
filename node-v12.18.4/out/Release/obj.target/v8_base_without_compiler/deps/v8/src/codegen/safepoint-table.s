	.file	"safepoint-table.cc"
	.text
	.section	.text._ZN2v88internal14SafepointTableC2Emmjb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14SafepointTableC2Emmjb
	.type	_ZN2v88internal14SafepointTableC2Emmjb, @function
_ZN2v88internal14SafepointTableC2Emmjb:
.LFB19766:
	.cfi_startproc
	endbr64
	addq	%rsi, %rdx
	movl	%ecx, 8(%rdi)
	movl	(%rdx), %eax
	movl	4(%rdx), %ecx
	addq	$8, %rdx
	movq	%rsi, (%rdi)
	movq	%rdx, 24(%rdi)
	movl	%eax, 12(%rdi)
	leal	(%rax,%rax,2), %eax
	sall	$2, %eax
	movb	%r8b, 40(%rdi)
	addq	%rax, %rdx
	movl	%ecx, 16(%rdi)
	movq	%rdx, 32(%rdi)
	ret
	.cfi_endproc
.LFE19766:
	.size	_ZN2v88internal14SafepointTableC2Emmjb, .-_ZN2v88internal14SafepointTableC2Emmjb
	.globl	_ZN2v88internal14SafepointTableC1Emmjb
	.set	_ZN2v88internal14SafepointTableC1Emmjb,_ZN2v88internal14SafepointTableC2Emmjb
	.section	.text._ZN2v88internal14SafepointTableC2ENS0_4CodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14SafepointTableC2ENS0_4CodeE
	.type	_ZN2v88internal14SafepointTableC2ENS0_4CodeE, @function
_ZN2v88internal14SafepointTableC2ENS0_4CodeE:
.LFB19769:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	addq	$63, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%rsi, -40(%rbp)
	movl	43(%rsi), %edx
	movslq	47(%rsi), %r12
	movl	%edx, %r13d
	shrl	$7, %r13d
	andl	$16777215, %r13d
	testl	%edx, %edx
	js	.L7
.L5:
	movq	%rax, (%rbx)
	addq	%r12, %rax
	movl	(%rax), %edx
	movl	4(%rax), %ecx
	addq	$8, %rax
	movl	%r13d, 8(%rbx)
	movq	%rax, 24(%rbx)
	movl	%edx, 12(%rbx)
	leal	(%rdx,%rdx,2), %edx
	sall	$2, %edx
	movb	$1, 40(%rbx)
	addq	%rdx, %rax
	movl	%ecx, 16(%rbx)
	movq	%rax, 32(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	leaq	-40(%rbp), %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	jmp	.L5
	.cfi_endproc
.LFE19769:
	.size	_ZN2v88internal14SafepointTableC2ENS0_4CodeE, .-_ZN2v88internal14SafepointTableC2ENS0_4CodeE
	.globl	_ZN2v88internal14SafepointTableC1ENS0_4CodeE
	.set	_ZN2v88internal14SafepointTableC1ENS0_4CodeE,_ZN2v88internal14SafepointTableC2ENS0_4CodeE
	.section	.rodata._ZN2v88internal14SafepointTable14find_return_pcEj.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal14SafepointTable14find_return_pcEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14SafepointTable14find_return_pcEj
	.type	_ZN2v88internal14SafepointTable14find_return_pcEj, @function
_ZN2v88internal14SafepointTable14find_return_pcEj:
.LFB19771:
	.cfi_startproc
	endbr64
	movl	12(%rdi), %r9d
	testl	%r9d, %r9d
	je	.L9
	movq	24(%rdi), %rdi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L18:
	cmpl	%r8d, %esi
	je	.L8
	addl	$1, %ecx
	addl	$12, %edx
	cmpl	%r9d, %ecx
	je	.L9
.L11:
	movl	%edx, %eax
	addq	%rdi, %rax
	movl	(%rax), %r8d
	cmpl	%esi, 8(%rax)
	jne	.L18
.L8:
	movl	%r8d, %eax
	ret
.L9:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19771:
	.size	_ZN2v88internal14SafepointTable14find_return_pcEj, .-_ZN2v88internal14SafepointTable14find_return_pcEj
	.section	.rodata._ZNK2v88internal14SafepointTable9FindEntryEm.str1.1,"aMS",@progbits,1
.LC1:
	.string	"len > 0"
.LC2:
	.string	"Check failed: %s."
	.section	.text._ZNK2v88internal14SafepointTable9FindEntryEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal14SafepointTable9FindEntryEm
	.type	_ZNK2v88internal14SafepointTable9FindEntryEm, @function
_ZNK2v88internal14SafepointTable9FindEntryEm:
.LFB19772:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	12(%rsi), %r10d
	subl	(%rsi), %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testl	%r10d, %r10d
	je	.L36
	movq	24(%rsi), %r11
	movzbl	40(%rsi), %ecx
	movq	%rdi, %rax
	cmpl	$1, %r10d
	je	.L37
.L21:
	testb	%cl, %cl
	je	.L38
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L39:
	movl	8(%r8), %edi
	cmpl	%edi, %edx
	je	.L29
	addl	$1, %ecx
	addl	$12, %r9d
	cmpl	%r10d, %ecx
	je	.L26
.L24:
	movl	%r9d, %r8d
	addq	%r11, %r8
	cmpl	(%r8), %edx
	jne	.L39
	movl	8(%r8), %edi
	movl	4(%r8), %edx
	imull	16(%rsi), %ecx
	addq	32(%rsi), %rcx
	movl	%edx, (%rax)
	movq	%rcx, 8(%rax)
	movl	%edi, 16(%rax)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_restore_state
	xorl	%edi, %edi
	xorl	%ecx, %ecx
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L40:
	addl	$1, %ecx
	addl	$12, %edi
	cmpl	%ecx, %r10d
	je	.L26
.L27:
	movl	%edi, %r8d
	addq	%r11, %r8
	cmpl	(%r8), %edx
	jne	.L40
	movl	4(%r8), %edx
	imull	16(%rsi), %ecx
	movl	$-1, %edi
	addq	32(%rsi), %rcx
.L31:
	movl	%edx, (%rax)
	movq	%rcx, 8(%rax)
	movl	%edi, 16(%rax)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	.cfi_restore_state
	cmpl	$-1, (%r11)
	jne	.L21
	movl	4(%r11), %edi
	movq	32(%rsi), %rsi
	movl	$-1, %edx
	testb	%cl, %cl
	je	.L22
	movl	8(%r11), %edx
.L22:
	movl	%edi, (%rax)
	movq	%rsi, 8(%rax)
	movl	%edx, 16(%rax)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L29:
	.cfi_restore_state
	imull	16(%rsi), %ecx
	movl	4(%r8), %edx
	addq	32(%rsi), %rcx
	jmp	.L31
.L36:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L26:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19772:
	.size	_ZNK2v88internal14SafepointTable9FindEntryEm, .-_ZNK2v88internal14SafepointTable9FindEntryEm
	.section	.rodata._ZNK2v88internal14SafepointTable10PrintEntryEjRSo.str1.1,"aMS",@progbits,1
.LC3:
	.string	"0"
.LC4:
	.string	"1"
	.section	.text._ZNK2v88internal14SafepointTable10PrintEntryEjRSo,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal14SafepointTable10PrintEntryEjRSo
	.type	_ZNK2v88internal14SafepointTable10PrintEntryEjRSo, @function
_ZNK2v88internal14SafepointTable10PrintEntryEjRSo:
.LFB19773:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6disasm13NameConverterE(%rip), %rcx
	movq	%rcx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -224(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-184(%rbp), %rax
	movq	$128, -192(%rbp)
	movq	%rax, %xmm1
	movl	16(%rdi), %eax
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -208(%rbp)
	testl	%eax, %eax
	je	.L41
	imull	%eax, %esi
	leal	-1(%rax), %ecx
	addq	32(%rdi), %rsi
	movq	%rdx, %rbx
	movq	%rsi, -240(%rbp)
	movq	%rsi, %r13
	movl	%ecx, -228(%rbp)
	testl	%ecx, %ecx
	jle	.L43
	subl	$2, %eax
	leaq	.LC4(%rip), %r14
	leaq	1(%rsi,%rax), %rax
	movq	%rax, -216(%rbp)
	.p2align 4,,10
	.p2align 3
.L46:
	movzbl	0(%r13), %r12d
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L53:
	btl	%r15d, %r12d
	movl	$1, %edx
	leaq	.LC3(%rip), %rsi
	jnc	.L69
	movl	$1, %edx
	movq	%r14, %rsi
.L69:
	movq	%rbx, %rdi
	addl	$1, %r15d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	$8, %r15d
	jne	.L53
	addq	$1, %r13
	cmpq	%r13, -216(%rbp)
	jne	.L46
.L43:
	movq	-224(%rbp), %rdx
	movl	-228(%rbp), %ecx
	movl	8(%rdx), %r13d
	leal	0(,%rcx,8), %eax
	subl	%eax, %r13d
	movslq	%ecx, %rax
	movq	-240(%rbp), %rcx
	movzbl	(%rcx,%rax), %r14d
	testl	%r13d, %r13d
	jle	.L41
	xorl	%r12d, %r12d
	leaq	.LC4(%rip), %r15
	.p2align 4,,10
	.p2align 3
.L50:
	btl	%r12d, %r14d
	movl	$1, %edx
	jc	.L66
	leaq	.LC3(%rip), %rsi
	movq	%rbx, %rdi
	addl	$1, %r12d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r13d, %r12d
	jne	.L50
.L41:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L70
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%rbx, %rdi
	addl	$1, %r12d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r12d, %r13d
	jne	.L50
	jmp	.L41
.L70:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19773:
	.size	_ZNK2v88internal14SafepointTable10PrintEntryEjRSo, .-_ZNK2v88internal14SafepointTable10PrintEntryEjRSo
	.section	.text._ZN2v88internal14SafepointTable9PrintBitsERSohi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14SafepointTable9PrintBitsERSohi
	.type	_ZN2v88internal14SafepointTable9PrintBitsERSohi, @function
_ZN2v88internal14SafepointTable9PrintBitsERSohi:
.LFB19777:
	.cfi_startproc
	endbr64
	testl	%edx, %edx
	jle	.L79
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	.LC4(%rip), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movzbl	%sil, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$8, %rsp
	.p2align 4,,10
	.p2align 3
.L76:
	btl	%ebx, %r12d
	movl	$1, %edx
	jc	.L77
	leaq	.LC3(%rip), %rsi
	movq	%r13, %rdi
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r14d, %ebx
	jne	.L76
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%r13, %rdi
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, %r14d
	jne	.L76
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE19777:
	.size	_ZN2v88internal14SafepointTable9PrintBitsERSohi, .-_ZN2v88internal14SafepointTable9PrintBitsERSohi
	.section	.text._ZNK2v88internal21SafepointTableBuilder13GetCodeOffsetEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal21SafepointTableBuilder13GetCodeOffsetEv
	.type	_ZNK2v88internal21SafepointTableBuilder13GetCodeOffsetEv, @function
_ZNK2v88internal21SafepointTableBuilder13GetCodeOffsetEv:
.LFB19779:
	.cfi_startproc
	endbr64
	movl	32(%rdi), %eax
	ret
	.cfi_endproc
.LFE19779:
	.size	_ZNK2v88internal21SafepointTableBuilder13GetCodeOffsetEv, .-_ZNK2v88internal21SafepointTableBuilder13GetCodeOffsetEv
	.section	.text._ZN2v88internal21SafepointTableBuilder24UpdateDeoptimizationInfoEiiij,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21SafepointTableBuilder24UpdateDeoptimizationInfoEiiij
	.type	_ZN2v88internal21SafepointTableBuilder24UpdateDeoptimizationInfoEiiij, @function
_ZN2v88internal21SafepointTableBuilder24UpdateDeoptimizationInfoEiiij:
.LFB19780:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%ecx, %r9
	movq	%r9, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	16(%rdi), %rbx
	movl	(%rbx), %r11d
	movq	%rbx, %r10
	cmpq	%r11, %r9
	jb	.L92
	.p2align 4,,10
	.p2align 3
.L84:
	movq	8(%r10), %r10
	subq	%r11, %r9
	movl	(%r10), %r11d
	cmpq	%r9, %r11
	jbe	.L84
.L92:
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L85
	movl	4(%rdi), %r12d
	movl	(%rdi), %ebx
	movl	$0, %ecx
	cmpl	%ebx, %r12d
	movq	%r12, %r11
	cmove	%rcx, %r12
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L86:
	cmpq	%r10, %rcx
	jne	.L98
	cmpq	%r9, %r12
	je	.L87
.L98:
	addq	$1, %r9
	leaq	(%r9,%r9,2), %rcx
	leaq	(%r10,%rcx,8), %rcx
	cmpl	(%rcx), %esi
	je	.L89
	movl	(%r10), %ecx
	cmpq	%rcx, %r9
	jb	.L90
	movq	8(%r10), %r10
	xorl	%r9d, %r9d
.L90:
	addl	$1, %eax
.L91:
	movq	%rdi, %rcx
	cmpl	%ebx, %r11d
	jne	.L86
	movq	8(%rdi), %rcx
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L93:
	addq	$1, %r9
	leaq	(%r9,%r9,2), %rcx
	leaq	(%r10,%rcx,8), %rcx
	cmpl	%esi, (%rcx)
	je	.L89
	movl	(%r10), %ecx
	cmpq	%rcx, %r9
	jb	.L95
	movq	8(%r10), %r10
	xorl	%r9d, %r9d
.L95:
	addl	$1, %eax
.L85:
	testq	%r9, %r9
	jne	.L93
	cmpq	%r10, %rbx
	jne	.L93
.L87:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L89:
	movl	%edx, 8(%rcx)
	movl	%r8d, 4(%rcx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19780:
	.size	_ZN2v88internal21SafepointTableBuilder24UpdateDeoptimizationInfoEiiij, .-_ZN2v88internal21SafepointTableBuilder24UpdateDeoptimizationInfoEiiij
	.section	.text._ZN2v88internal21SafepointTableBuilder16RemoveDuplicatesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21SafepointTableBuilder16RemoveDuplicatesEv
	.type	_ZN2v88internal21SafepointTableBuilder16RemoveDuplicatesEv, @function
_ZN2v88internal21SafepointTableBuilder16RemoveDuplicatesEv:
.LFB19782:
	.cfi_startproc
	endbr64
	cmpq	$1, 8(%rdi)
	jbe	.L167
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r9
	movl	(%r9), %ebx
	movq	%r9, %rdx
	movq	%rbx, %rcx
	cmpq	$1, %rbx
	ja	.L118
	.p2align 4,,10
	.p2align 3
.L116:
	movq	8(%rdx), %rdx
	subq	%rcx, %rax
	movl	(%rdx), %ecx
	cmpq	%rax, %rcx
	jbe	.L116
.L118:
	movq	24(%rdi), %r11
.L117:
	testq	%r11, %r11
	je	.L141
.L172:
	movl	4(%r11), %r8d
	movq	%r11, %rcx
	cmpl	(%r11), %r8d
	jne	.L119
	movq	8(%r11), %rcx
	xorl	%r8d, %r8d
.L119:
	cmpq	%rdx, %rcx
	jne	.L143
	cmpq	%r8, %rax
	je	.L121
.L143:
	addq	$1, %rax
	leaq	(%rax,%rax,2), %rcx
	leaq	(%rdx,%rcx,8), %rcx
	movl	4(%rcx), %esi
	cmpl	%esi, 28(%r9)
	jne	.L113
	movq	16(%rcx), %rcx
	movq	40(%r9), %rsi
	movq	8(%rcx), %r15
	cmpq	%r15, 8(%rsi)
	jne	.L113
	movq	24(%rsi), %r13
	movq	16(%rcx), %r10
	testq	%r13, %r13
	je	.L124
	movl	4(%r13), %r15d
	testq	%r15, %r15
	movq	%r15, -48(%rbp)
	movq	%r15, %rcx
	sete	%r15b
	cmpl	0(%r13), %ecx
	je	.L170
.L126:
	movq	16(%rsi), %r8
	cmpq	%r13, %r8
	je	.L171
.L144:
	xorl	%esi, %esi
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L128:
	movl	24(%r10,%rsi,4), %r14d
	cmpl	%r14d, 24(%r8,%rcx,4)
	jne	.L113
	movl	(%r8), %r12d
	addq	$1, %rcx
	cmpq	%r12, %rcx
	jnb	.L131
	cmpq	%rcx, -48(%rbp)
	sete	%r14b
.L132:
	movl	(%r10), %r12d
	addq	$1, %rsi
	cmpq	%r12, %rsi
	jb	.L133
	movq	8(%r10), %r10
	xorl	%esi, %esi
.L133:
	cmpq	%r8, %r13
	jne	.L128
	testb	%r14b, %r14b
	je	.L128
.L124:
	movl	(%rdx), %ecx
	cmpq	%rcx, %rax
	jb	.L117
	movq	8(%rdx), %rdx
	xorl	%eax, %eax
	testq	%r11, %r11
	jne	.L172
.L141:
	movq	%r9, %rcx
	xorl	%r8d, %r8d
	jmp	.L119
.L142:
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L135:
	movl	%eax, 4(%r9)
	movq	%r9, 24(%rdi)
	movq	8(%r9), %rax
	testq	%rax, %rax
	je	.L138
	.p2align 4,,10
	.p2align 3
.L137:
	movl	$0, 4(%rax)
	movq	8(%rax), %rax
	testq	%rax, %rax
	jne	.L137
.L138:
	movq	16(%rdi), %rax
	movq	$1, 8(%rdi)
	movl	$-1, 24(%rax)
.L113:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L131:
	.cfi_restore_state
	movq	8(%r8), %r8
	movl	%r15d, %r14d
	xorl	%ecx, %ecx
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L167:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L171:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	testb	%r15b, %r15b
	je	.L144
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L170:
	movq	$0, -48(%rbp)
	movq	8(%r13), %r13
	movl	$1, %r15d
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L121:
	cmpq	$1, %rbx
	ja	.L142
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L136:
	movq	8(%r9), %r9
	subq	%rbx, %rax
	movl	(%r9), %ebx
	cmpq	%rax, %rbx
	jbe	.L136
	jmp	.L135
	.cfi_endproc
.LFE19782:
	.size	_ZN2v88internal21SafepointTableBuilder16RemoveDuplicatesEv, .-_ZN2v88internal21SafepointTableBuilder16RemoveDuplicatesEv
	.section	.rodata._ZN2v88internal21SafepointTableBuilder4EmitEPNS0_9AssemblerEi.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"cannot create std::vector larger than max_size()"
	.section	.text._ZN2v88internal21SafepointTableBuilder4EmitEPNS0_9AssemblerEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21SafepointTableBuilder4EmitEPNS0_9AssemblerEi
	.type	_ZN2v88internal21SafepointTableBuilder4EmitEPNS0_9AssemblerEi, @function
_ZN2v88internal21SafepointTableBuilder4EmitEPNS0_9AssemblerEi:
.LFB19781:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$136, %rsp
	movq	%rdi, -152(%rbp)
	movl	%edx, -136(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal21SafepointTableBuilder16RemoveDuplicatesEv
	movl	$4, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler5AlignEi@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L269
.L174:
	movq	-152(%rbp), %r15
	movq	32(%rbx), %rax
	movq	%rbx, %rdi
	subq	16(%rbx), %rax
	movl	%eax, 32(%r15)
	movl	-136(%rbp), %eax
	addl	$7, %eax
	sarl	$3, %eax
	movl	%eax, -132(%rbp)
	movl	%eax, %r14d
	movq	8(%r15), %rax
	movq	%rax, -120(%rbp)
	movl	-120(%rbp), %esi
	call	_ZN2v88internal9Assembler2ddEj@PLT
	movl	%r14d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler2ddEj@PLT
	movq	24(%r15), %r12
	movq	16(%r15), %r13
	testq	%r12, %r12
	je	.L181
	movl	4(%r12), %eax
	testq	%rax, %rax
	movq	%rax, -128(%rbp)
	sete	-120(%rbp)
	cmpl	%eax, (%r12)
	je	.L270
	cmpq	%r12, %r13
	je	.L268
	.p2align 4,,10
	.p2align 3
.L221:
	xorl	%r15d, %r15d
.L183:
	addq	$1, %r15
	movq	%rbx, %rdi
	leaq	(%r15,%r15,2), %rax
	leaq	0(%r13,%rax,8), %r14
	movl	(%r14), %esi
	call	_ZN2v88internal9Assembler2ddEj@PLT
	movl	4(%r14), %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler2ddEj@PLT
	movl	8(%r14), %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler2ddEj@PLT
	movl	0(%r13), %eax
	cmpq	%r15, %rax
	ja	.L190
	movq	8(%r13), %r13
	cmpq	%r13, %r12
	jne	.L221
.L268:
	cmpb	$0, -120(%rbp)
	je	.L221
.L181:
	movq	-152(%rbp), %rax
	movslq	-132(%rbp), %r12
	movq	40(%rax), %rdi
	cmpq	$2147483647, %r12
	ja	.L271
	testq	%r12, %r12
	je	.L219
	movq	16(%rdi), %r15
	movq	24(%rdi), %rax
	leaq	7(%r12), %rsi
	andq	$-8, %rsi
	subq	%r15, %rax
	cmpq	%rax, %rsi
	ja	.L272
	addq	%r15, %rsi
	movq	%rsi, 16(%rdi)
.L187:
	movq	%r12, %rdx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	memset@PLT
	leaq	(%r15,%r12), %rax
	movq	%rax, -128(%rbp)
.L185:
	movq	-152(%rbp), %rax
	movq	16(%rax), %rdi
	movq	24(%rax), %rax
	movq	%rdi, -120(%rbp)
	movq	%rax, -144(%rbp)
	testq	%rax, %rax
	je	.L188
	movl	4(%rax), %edx
	testq	%rdx, %rdx
	movq	%rdx, -168(%rbp)
	sete	-153(%rbp)
	cmpl	(%rax), %edx
	jne	.L194
	movq	8(%rax), %rax
	movb	$1, -153(%rbp)
	movq	$0, -168(%rbp)
	movq	%rax, -144(%rbp)
.L194:
	movq	-120(%rbp), %rdi
	cmpq	%rdi, -144(%rbp)
	jne	.L222
	cmpb	$0, -153(%rbp)
	jne	.L188
.L222:
	movl	-132(%rbp), %eax
	xorl	%r13d, %r13d
	subl	$1, %eax
	leaq	1(%r15,%rax), %r12
	.p2align 4,,10
	.p2align 3
.L196:
	movq	-120(%rbp), %rdi
	addq	$1, %r13
	leaq	0(%r13,%r13,2), %rax
	movq	16(%rdi,%rax,8), %r14
	cmpq	%r15, -128(%rbp)
	je	.L201
	movq	-128(%rbp), %rdx
	xorl	%esi, %esi
	movq	%r15, %rdi
	subq	%r15, %rdx
	call	memset@PLT
.L201:
	movq	24(%r14), %rdi
	movq	16(%r14), %rsi
	testq	%rdi, %rdi
	je	.L199
	movl	4(%rdi), %r10d
	testq	%r10, %r10
	sete	%r11b
	cmpl	(%rdi), %r10d
	jne	.L205
	movq	8(%rdi), %rdi
	movl	$1, %r11d
	xorl	%r10d, %r10d
.L205:
	cmpq	%rsi, %rdi
	jne	.L207
	testb	%r11b, %r11b
	jne	.L199
.L207:
	movl	-136(%rbp), %eax
	xorl	%edx, %edx
	movl	$1, %r8d
	leal	-1(%rax), %r9d
	.p2align 4,,10
	.p2align 3
.L214:
	movl	%r9d, %ecx
	subl	24(%rsi,%rdx,4), %ecx
	movl	%r8d, %r14d
	addq	$1, %rdx
	movl	%ecx, %eax
	andl	$7, %ecx
	sarl	$3, %eax
	sall	%cl, %r14d
	cltq
	orb	%r14b, (%r15,%rax)
	movl	(%rsi), %eax
	cmpq	%rdx, %rax
	jbe	.L208
	cmpq	%rsi, %rdi
	jne	.L214
	cmpq	%rdx, %r10
	jne	.L214
.L199:
	movl	-132(%rbp), %eax
	movq	%r15, %r14
	testl	%eax, %eax
	jle	.L202
	.p2align 4,,10
	.p2align 3
.L215:
	movzbl	(%r14), %esi
	movq	%rbx, %rdi
	addq	$1, %r14
	call	_ZN2v88internal9Assembler2dbEh@PLT
	cmpq	%r14, %r12
	jne	.L215
.L202:
	movq	-120(%rbp), %rax
	movl	(%rax), %eax
	cmpq	%rax, %r13
	jb	.L273
	movq	-120(%rbp), %rax
	movq	8(%rax), %rax
	movq	%rax, -120(%rbp)
	cmpq	%rax, -144(%rbp)
	jne	.L224
	cmpb	$0, -153(%rbp)
	jne	.L188
.L224:
	xorl	%r13d, %r13d
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L190:
	cmpq	%r13, %r12
	jne	.L183
	cmpq	%r15, -128(%rbp)
	jne	.L183
	jmp	.L181
.L270:
	movq	8(%r12), %r12
	movb	$1, -120(%rbp)
	movq	$0, -128(%rbp)
	cmpq	%r12, %r13
	jne	.L221
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L208:
	movq	8(%rsi), %rsi
	cmpq	%rsi, %rdi
	jne	.L223
	testb	%r11b, %r11b
	jne	.L199
.L223:
	xorl	%edx, %edx
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L273:
	movq	-120(%rbp), %rdi
	cmpq	%rdi, -144(%rbp)
	jne	.L196
	cmpq	%r13, -168(%rbp)
	jne	.L196
.L188:
	movq	-152(%rbp), %rax
	movb	$1, 36(%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L274
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L219:
	.cfi_restore_state
	movq	$0, -128(%rbp)
	xorl	%r15d, %r15d
	jmp	.L185
.L269:
	leaq	-96(%rbp), %r13
	leaq	-104(%rbp), %rsi
	xorl	%edx, %edx
	movq	$20, -104(%rbp)
	movq	%r13, %rdi
	leaq	-80(%rbp), %r12
	movq	%r12, -96(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-104(%rbp), %rdx
	leaq	40(%rbx), %rdi
	movdqa	.LC6(%rip), %xmm0
	movq	%rax, -96(%rbp)
	movq	%rdx, -80(%rbp)
	movups	%xmm0, (%rax)
	movq	-96(%rbp), %rdx
	movl	$778398818, 16(%rax)
	movq	-104(%rbp), %rax
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	movq	%r13, %rdx
	movq	32(%rbx), %rsi
	subq	16(%rbx), %rsi
	call	_ZN2v88internal18CodeCommentsWriter3AddEjNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L174
	call	_ZdlPv@PLT
	jmp	.L174
.L272:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r15
	jmp	.L187
.L274:
	call	__stack_chk_fail@PLT
.L271:
	leaq	.LC5(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE19781:
	.size	_ZN2v88internal21SafepointTableBuilder4EmitEPNS0_9AssemblerEi, .-_ZN2v88internal21SafepointTableBuilder4EmitEPNS0_9AssemblerEi
	.section	.text._ZNK2v88internal21SafepointTableBuilder22IsIdenticalExceptForPcERKNS1_18DeoptimizationInfoES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal21SafepointTableBuilder22IsIdenticalExceptForPcERKNS1_18DeoptimizationInfoES4_
	.type	_ZNK2v88internal21SafepointTableBuilder22IsIdenticalExceptForPcERKNS1_18DeoptimizationInfoES4_, @function
_ZNK2v88internal21SafepointTableBuilder22IsIdenticalExceptForPcERKNS1_18DeoptimizationInfoES4_:
.LFB19783:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movl	4(%rdx), %ebx
	cmpl	%ebx, 4(%rsi)
	jne	.L275
	movq	16(%rdx), %rdx
	movq	16(%rsi), %rcx
	movq	8(%rdx), %rdi
	cmpq	%rdi, 8(%rcx)
	je	.L295
.L275:
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L295:
	.cfi_restore_state
	movq	24(%rcx), %r8
	movq	16(%rdx), %rdi
	testq	%r8, %r8
	je	.L277
	movl	4(%r8), %ebx
	testq	%rbx, %rbx
	sete	%r11b
	cmpl	(%r8), %ebx
	jne	.L279
	movq	8(%r8), %r8
	movl	$1, %r11d
	xorl	%ebx, %ebx
.L279:
	movq	16(%rcx), %rsi
	cmpq	%r8, %rsi
	sete	%al
	andb	%r11b, %al
	jne	.L277
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L280:
	movl	24(%rdi,%rcx,4), %r10d
	cmpl	%r10d, 24(%rsi,%rdx,4)
	jne	.L275
	movl	(%rsi), %r9d
	addq	$1, %rdx
	cmpq	%r9, %rdx
	jnb	.L283
	cmpq	%rdx, %rbx
	sete	%r9b
.L284:
	movl	(%rdi), %r10d
	addq	$1, %rcx
	cmpq	%r10, %rcx
	jb	.L285
	movq	8(%rdi), %rdi
	xorl	%ecx, %ecx
.L285:
	cmpq	%rsi, %r8
	jne	.L280
	testb	%r9b, %r9b
	je	.L280
.L277:
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L283:
	.cfi_restore_state
	movq	8(%rsi), %rsi
	movl	%r11d, %r9d
	xorl	%edx, %edx
	jmp	.L284
	.cfi_endproc
.LFE19783:
	.size	_ZNK2v88internal21SafepointTableBuilder22IsIdenticalExceptForPcERKNS1_18DeoptimizationInfoES4_, .-_ZNK2v88internal21SafepointTableBuilder22IsIdenticalExceptForPcERKNS1_18DeoptimizationInfoES4_
	.section	.text._ZN2v88internal13ZoneChunkListIiEC2EPNS0_4ZoneENS2_9StartModeE,"axG",@progbits,_ZN2v88internal13ZoneChunkListIiEC5EPNS0_4ZoneENS2_9StartModeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ZoneChunkListIiEC2EPNS0_4ZoneENS2_9StartModeE
	.type	_ZN2v88internal13ZoneChunkListIiEC2EPNS0_4ZoneENS2_9StartModeE, @function
_ZN2v88internal13ZoneChunkListIiEC2EPNS0_4ZoneENS2_9StartModeE:
.LFB20199:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%rsi, (%rdi)
	movq	$0, 8(%rdi)
	movups	%xmm0, 16(%rdi)
	testl	%edx, %edx
	jne	.L304
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L304:
	.cfi_restore_state
	movq	%rsi, %rdi
	movl	%edx, %eax
	leaq	31(,%rax,4), %rsi
	movq	24(%rdi), %rcx
	movq	16(%rdi), %rax
	andq	$-8, %rsi
	subq	%rax, %rcx
	cmpq	%rcx, %rsi
	ja	.L305
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L299:
	pxor	%xmm0, %xmm0
	movl	$0, 4(%rax)
	movl	%edx, (%rax)
	movups	%xmm0, 8(%rax)
	movq	%rax, 16(%rbx)
	movq	%rax, 24(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L305:
	.cfi_restore_state
	movl	%edx, -20(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-20(%rbp), %edx
	jmp	.L299
	.cfi_endproc
.LFE20199:
	.size	_ZN2v88internal13ZoneChunkListIiEC2EPNS0_4ZoneENS2_9StartModeE, .-_ZN2v88internal13ZoneChunkListIiEC2EPNS0_4ZoneENS2_9StartModeE
	.weak	_ZN2v88internal13ZoneChunkListIiEC1EPNS0_4ZoneENS2_9StartModeE
	.set	_ZN2v88internal13ZoneChunkListIiEC1EPNS0_4ZoneENS2_9StartModeE,_ZN2v88internal13ZoneChunkListIiEC2EPNS0_4ZoneENS2_9StartModeE
	.section	.text._ZN2v88internal21SafepointTableBuilder15DefineSafepointEPNS0_9AssemblerENS0_9Safepoint9DeoptModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21SafepointTableBuilder15DefineSafepointEPNS0_9AssemblerENS0_9Safepoint9DeoptModeE
	.type	_ZN2v88internal21SafepointTableBuilder15DefineSafepointEPNS0_9AssemblerENS0_9Safepoint9DeoptModeE, @function
_ZN2v88internal21SafepointTableBuilder15DefineSafepointEPNS0_9AssemblerENS0_9Safepoint9DeoptModeE:
.LFB19778:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	40(%rdi), %r13
	movq	%rdi, %rbx
	movq	32(%rsi), %r12
	subq	16(%rsi), %r12
	movq	16(%r13), %r14
	movq	24(%r13), %rax
	subq	%r14, %rax
	cmpq	$31, %rax
	jbe	.L319
	leaq	32(%r14), %rax
	movq	%rax, 16(%r13)
.L308:
	movl	$8, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal13ZoneChunkListIiEC1EPNS0_4ZoneENS2_9StartModeE
	movq	24(%rbx), %rax
	testq	%rax, %rax
	je	.L320
.L309:
	movl	4(%rax), %edx
	cmpl	(%rax), %edx
	je	.L321
.L312:
	leaq	3(%rdx,%rdx,2), %rdx
	leaq	(%rax,%rdx,8), %rax
	movq	$-1, 4(%rax)
	movl	%r12d, (%rax)
	movq	%r14, 16(%rax)
	movq	24(%rbx), %rax
	addl	$1, 4(%rax)
	movq	24(%rbx), %rax
	addq	$1, 8(%rbx)
	movl	4(%rax), %edx
	testl	%edx, %edx
	je	.L322
	subl	$1, %edx
	popq	%rbx
	popq	%r12
	leaq	3(%rdx,%rdx,2), %rdx
	popq	%r13
	popq	%r14
	leaq	(%rax,%rdx,8), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	16(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L322:
	.cfi_restore_state
	movq	16(%rax), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	movl	4(%rax), %edx
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	subl	$1, %edx
	leaq	3(%rdx,%rdx,2), %rdx
	leaq	(%rax,%rdx,8), %rax
	movq	16(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L321:
	.cfi_restore_state
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L323
.L313:
	movq	%rax, 24(%rbx)
	movl	4(%rax), %edx
	jmp	.L312
	.p2align 4,,10
	.p2align 3
.L320:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$215, %rdx
	jbe	.L324
	leaq	216(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L311:
	pxor	%xmm0, %xmm0
	movq	$8, (%rax)
	movups	%xmm0, 8(%rax)
	movq	%rax, 16(%rbx)
	movq	%rax, 24(%rbx)
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L323:
	leal	(%rdx,%rdx), %r13d
	movl	$256, %eax
	movq	(%rbx), %rdi
	cmpl	$256, %r13d
	cmova	%eax, %r13d
	movq	24(%rdi), %rdx
	movl	%r13d, %eax
	leaq	3(%rax,%rax,2), %rsi
	movq	16(%rdi), %rax
	salq	$3, %rsi
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L325
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L315:
	pxor	%xmm0, %xmm0
	movl	$0, 4(%rax)
	movups	%xmm0, 8(%rax)
	movl	%r13d, (%rax)
	movq	24(%rbx), %rdx
	movq	%rax, 8(%rdx)
	movq	24(%rbx), %rdx
	movq	%rdx, 16(%rax)
	movq	24(%rbx), %rax
	movq	8(%rax), %rax
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L319:
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r14
	jmp	.L308
	.p2align 4,,10
	.p2align 3
.L324:
	movl	$216, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L325:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L315
	.cfi_endproc
.LFE19778:
	.size	_ZN2v88internal21SafepointTableBuilder15DefineSafepointEPNS0_9AssemblerENS0_9Safepoint9DeoptModeE, .-_ZN2v88internal21SafepointTableBuilder15DefineSafepointEPNS0_9AssemblerENS0_9Safepoint9DeoptModeE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal14SafepointTableC2Emmjb,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal14SafepointTableC2Emmjb, @function
_GLOBAL__sub_I__ZN2v88internal14SafepointTableC2Emmjb:
.LFB24039:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE24039:
	.size	_GLOBAL__sub_I__ZN2v88internal14SafepointTableC2Emmjb, .-_GLOBAL__sub_I__ZN2v88internal14SafepointTableC2Emmjb
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal14SafepointTableC2Emmjb
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC6:
	.quad	7306634455106140987
	.quad	7022273403418144624
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
