	.file	"messages.cc"
	.text
	.section	.text._ZNK2v88internal12JSStackFrame11GetReceiverEv,"axG",@progbits,_ZNK2v88internal12JSStackFrame11GetReceiverEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal12JSStackFrame11GetReceiverEv
	.type	_ZNK2v88internal12JSStackFrame11GetReceiverEv, @function
_ZNK2v88internal12JSStackFrame11GetReceiverEv:
.LFB4555:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE4555:
	.size	_ZNK2v88internal12JSStackFrame11GetReceiverEv, .-_ZNK2v88internal12JSStackFrame11GetReceiverEv
	.section	.text._ZNK2v88internal12JSStackFrame7IsAsyncEv,"axG",@progbits,_ZNK2v88internal12JSStackFrame7IsAsyncEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal12JSStackFrame7IsAsyncEv
	.type	_ZNK2v88internal12JSStackFrame7IsAsyncEv, @function
_ZNK2v88internal12JSStackFrame7IsAsyncEv:
.LFB4556:
	.cfi_startproc
	endbr64
	movzbl	52(%rdi), %eax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE4556:
	.size	_ZNK2v88internal12JSStackFrame7IsAsyncEv, .-_ZNK2v88internal12JSStackFrame7IsAsyncEv
	.section	.text._ZNK2v88internal12JSStackFrame12IsPromiseAllEv,"axG",@progbits,_ZNK2v88internal12JSStackFrame12IsPromiseAllEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal12JSStackFrame12IsPromiseAllEv
	.type	_ZNK2v88internal12JSStackFrame12IsPromiseAllEv, @function
_ZNK2v88internal12JSStackFrame12IsPromiseAllEv:
.LFB4557:
	.cfi_startproc
	endbr64
	movzbl	52(%rdi), %eax
	shrb	$2, %al
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE4557:
	.size	_ZNK2v88internal12JSStackFrame12IsPromiseAllEv, .-_ZNK2v88internal12JSStackFrame12IsPromiseAllEv
	.section	.text._ZN2v88internal12JSStackFrame13IsConstructorEv,"axG",@progbits,_ZN2v88internal12JSStackFrame13IsConstructorEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12JSStackFrame13IsConstructorEv
	.type	_ZN2v88internal12JSStackFrame13IsConstructorEv, @function
_ZN2v88internal12JSStackFrame13IsConstructorEv:
.LFB4558:
	.cfi_startproc
	endbr64
	movzbl	52(%rdi), %eax
	shrb	%al
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE4558:
	.size	_ZN2v88internal12JSStackFrame13IsConstructorEv, .-_ZN2v88internal12JSStackFrame13IsConstructorEv
	.section	.text._ZNK2v88internal12JSStackFrame8IsStrictEv,"axG",@progbits,_ZNK2v88internal12JSStackFrame8IsStrictEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal12JSStackFrame8IsStrictEv
	.type	_ZNK2v88internal12JSStackFrame8IsStrictEv, @function
_ZNK2v88internal12JSStackFrame8IsStrictEv:
.LFB4559:
	.cfi_startproc
	endbr64
	movzbl	52(%rdi), %eax
	shrb	$3, %al
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE4559:
	.size	_ZNK2v88internal12JSStackFrame8IsStrictEv, .-_ZNK2v88internal12JSStackFrame8IsStrictEv
	.section	.text._ZN2v88internal14WasmStackFrame11GetFileNameEv,"axG",@progbits,_ZN2v88internal14WasmStackFrame11GetFileNameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14WasmStackFrame11GetFileNameEv
	.type	_ZN2v88internal14WasmStackFrame11GetFileNameEv, @function
_ZN2v88internal14WasmStackFrame11GetFileNameEv:
.LFB4560:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	addq	$104, %rax
	ret
	.cfi_endproc
.LFE4560:
	.size	_ZN2v88internal14WasmStackFrame11GetFileNameEv, .-_ZN2v88internal14WasmStackFrame11GetFileNameEv
	.section	.text._ZN2v88internal14WasmStackFrame24GetScriptNameOrSourceUrlEv,"axG",@progbits,_ZN2v88internal14WasmStackFrame24GetScriptNameOrSourceUrlEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14WasmStackFrame24GetScriptNameOrSourceUrlEv
	.type	_ZN2v88internal14WasmStackFrame24GetScriptNameOrSourceUrlEv, @function
_ZN2v88internal14WasmStackFrame24GetScriptNameOrSourceUrlEv:
.LFB4561:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	addq	$104, %rax
	ret
	.cfi_endproc
.LFE4561:
	.size	_ZN2v88internal14WasmStackFrame24GetScriptNameOrSourceUrlEv, .-_ZN2v88internal14WasmStackFrame24GetScriptNameOrSourceUrlEv
	.section	.text._ZN2v88internal14WasmStackFrame13GetMethodNameEv,"axG",@progbits,_ZN2v88internal14WasmStackFrame13GetMethodNameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14WasmStackFrame13GetMethodNameEv
	.type	_ZN2v88internal14WasmStackFrame13GetMethodNameEv, @function
_ZN2v88internal14WasmStackFrame13GetMethodNameEv:
.LFB4562:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	addq	$104, %rax
	ret
	.cfi_endproc
.LFE4562:
	.size	_ZN2v88internal14WasmStackFrame13GetMethodNameEv, .-_ZN2v88internal14WasmStackFrame13GetMethodNameEv
	.section	.text._ZN2v88internal14WasmStackFrame11GetTypeNameEv,"axG",@progbits,_ZN2v88internal14WasmStackFrame11GetTypeNameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14WasmStackFrame11GetTypeNameEv
	.type	_ZN2v88internal14WasmStackFrame11GetTypeNameEv, @function
_ZN2v88internal14WasmStackFrame11GetTypeNameEv:
.LFB4563:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	addq	$104, %rax
	ret
	.cfi_endproc
.LFE4563:
	.size	_ZN2v88internal14WasmStackFrame11GetTypeNameEv, .-_ZN2v88internal14WasmStackFrame11GetTypeNameEv
	.section	.text._ZN2v88internal14WasmStackFrame13GetLineNumberEv,"axG",@progbits,_ZN2v88internal14WasmStackFrame13GetLineNumberEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14WasmStackFrame13GetLineNumberEv
	.type	_ZN2v88internal14WasmStackFrame13GetLineNumberEv, @function
_ZN2v88internal14WasmStackFrame13GetLineNumberEv:
.LFB4564:
	.cfi_startproc
	endbr64
	movl	24(%rdi), %eax
	ret
	.cfi_endproc
.LFE4564:
	.size	_ZN2v88internal14WasmStackFrame13GetLineNumberEv, .-_ZN2v88internal14WasmStackFrame13GetLineNumberEv
	.section	.text._ZNK2v88internal14WasmStackFrame15GetPromiseIndexEv,"axG",@progbits,_ZNK2v88internal14WasmStackFrame15GetPromiseIndexEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal14WasmStackFrame15GetPromiseIndexEv
	.type	_ZNK2v88internal14WasmStackFrame15GetPromiseIndexEv, @function
_ZNK2v88internal14WasmStackFrame15GetPromiseIndexEv:
.LFB4565:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*96(%rax)
	.cfi_endproc
.LFE4565:
	.size	_ZNK2v88internal14WasmStackFrame15GetPromiseIndexEv, .-_ZNK2v88internal14WasmStackFrame15GetPromiseIndexEv
	.section	.text._ZN2v88internal14WasmStackFrame8IsNativeEv,"axG",@progbits,_ZN2v88internal14WasmStackFrame8IsNativeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14WasmStackFrame8IsNativeEv
	.type	_ZN2v88internal14WasmStackFrame8IsNativeEv, @function
_ZN2v88internal14WasmStackFrame8IsNativeEv:
.LFB4566:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4566:
	.size	_ZN2v88internal14WasmStackFrame8IsNativeEv, .-_ZN2v88internal14WasmStackFrame8IsNativeEv
	.section	.text._ZN2v88internal14WasmStackFrame10IsToplevelEv,"axG",@progbits,_ZN2v88internal14WasmStackFrame10IsToplevelEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14WasmStackFrame10IsToplevelEv
	.type	_ZN2v88internal14WasmStackFrame10IsToplevelEv, @function
_ZN2v88internal14WasmStackFrame10IsToplevelEv:
.LFB4567:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4567:
	.size	_ZN2v88internal14WasmStackFrame10IsToplevelEv, .-_ZN2v88internal14WasmStackFrame10IsToplevelEv
	.section	.text._ZNK2v88internal14WasmStackFrame7IsAsyncEv,"axG",@progbits,_ZNK2v88internal14WasmStackFrame7IsAsyncEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal14WasmStackFrame7IsAsyncEv
	.type	_ZNK2v88internal14WasmStackFrame7IsAsyncEv, @function
_ZNK2v88internal14WasmStackFrame7IsAsyncEv:
.LFB4568:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4568:
	.size	_ZNK2v88internal14WasmStackFrame7IsAsyncEv, .-_ZNK2v88internal14WasmStackFrame7IsAsyncEv
	.section	.text._ZNK2v88internal14WasmStackFrame12IsPromiseAllEv,"axG",@progbits,_ZNK2v88internal14WasmStackFrame12IsPromiseAllEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal14WasmStackFrame12IsPromiseAllEv
	.type	_ZNK2v88internal14WasmStackFrame12IsPromiseAllEv, @function
_ZNK2v88internal14WasmStackFrame12IsPromiseAllEv:
.LFB4569:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4569:
	.size	_ZNK2v88internal14WasmStackFrame12IsPromiseAllEv, .-_ZNK2v88internal14WasmStackFrame12IsPromiseAllEv
	.section	.text._ZN2v88internal14WasmStackFrame13IsConstructorEv,"axG",@progbits,_ZN2v88internal14WasmStackFrame13IsConstructorEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14WasmStackFrame13IsConstructorEv
	.type	_ZN2v88internal14WasmStackFrame13IsConstructorEv, @function
_ZN2v88internal14WasmStackFrame13IsConstructorEv:
.LFB4570:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4570:
	.size	_ZN2v88internal14WasmStackFrame13IsConstructorEv, .-_ZN2v88internal14WasmStackFrame13IsConstructorEv
	.section	.text._ZNK2v88internal14WasmStackFrame8IsStrictEv,"axG",@progbits,_ZNK2v88internal14WasmStackFrame8IsStrictEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal14WasmStackFrame8IsStrictEv
	.type	_ZNK2v88internal14WasmStackFrame8IsStrictEv, @function
_ZNK2v88internal14WasmStackFrame8IsStrictEv:
.LFB4571:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4571:
	.size	_ZNK2v88internal14WasmStackFrame8IsStrictEv, .-_ZNK2v88internal14WasmStackFrame8IsStrictEv
	.section	.text._ZN2v88internal14StackFrameBase17GetWasmModuleNameEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14StackFrameBase17GetWasmModuleNameEv
	.type	_ZN2v88internal14StackFrameBase17GetWasmModuleNameEv, @function
_ZN2v88internal14StackFrameBase17GetWasmModuleNameEv:
.LFB20647:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	addq	$88, %rax
	ret
	.cfi_endproc
.LFE20647:
	.size	_ZN2v88internal14StackFrameBase17GetWasmModuleNameEv, .-_ZN2v88internal14StackFrameBase17GetWasmModuleNameEv
	.globl	_ZN2v88internal14StackFrameBase15GetWasmInstanceEv
	.set	_ZN2v88internal14StackFrameBase15GetWasmInstanceEv,_ZN2v88internal14StackFrameBase17GetWasmModuleNameEv
	.section	.text._ZNK2v88internal12JSStackFrame11GetFunctionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal12JSStackFrame11GetFunctionEv
	.type	_ZNK2v88internal12JSStackFrame11GetFunctionEv, @function
_ZNK2v88internal12JSStackFrame11GetFunctionEv:
.LFB20659:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	ret
	.cfi_endproc
.LFE20659:
	.size	_ZNK2v88internal12JSStackFrame11GetFunctionEv, .-_ZNK2v88internal12JSStackFrame11GetFunctionEv
	.section	.text._ZNK2v88internal12JSStackFrame15GetPromiseIndexEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal12JSStackFrame15GetPromiseIndexEv
	.type	_ZNK2v88internal12JSStackFrame15GetPromiseIndexEv, @function
_ZNK2v88internal12JSStackFrame15GetPromiseIndexEv:
.LFB20669:
	.cfi_startproc
	endbr64
	testb	$4, 52(%rdi)
	je	.L23
	movl	40(%rdi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE20669:
	.size	_ZNK2v88internal12JSStackFrame15GetPromiseIndexEv, .-_ZNK2v88internal12JSStackFrame15GetPromiseIndexEv
	.section	.text._ZNK2v88internal14WasmStackFrame11GetReceiverEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal14WasmStackFrame11GetReceiverEv
	.type	_ZNK2v88internal14WasmStackFrame11GetReceiverEv, @function
_ZNK2v88internal14WasmStackFrame11GetReceiverEv:
.LFB20677:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE20677:
	.size	_ZNK2v88internal14WasmStackFrame11GetReceiverEv, .-_ZNK2v88internal14WasmStackFrame11GetReceiverEv
	.globl	_ZN2v88internal14WasmStackFrame15GetWasmInstanceEv
	.set	_ZN2v88internal14WasmStackFrame15GetWasmInstanceEv,_ZNK2v88internal14WasmStackFrame11GetReceiverEv
	.section	.text._ZNK2v88internal14WasmStackFrame9HasScriptEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal14WasmStackFrame9HasScriptEv
	.type	_ZNK2v88internal14WasmStackFrame9HasScriptEv, @function
_ZNK2v88internal14WasmStackFrame9HasScriptEv:
.LFB20686:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE20686:
	.size	_ZNK2v88internal14WasmStackFrame9HasScriptEv, .-_ZNK2v88internal14WasmStackFrame9HasScriptEv
	.section	.text._ZNK2v88internal19AsmJsWasmStackFrame11GetFunctionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal19AsmJsWasmStackFrame11GetFunctionEv
	.type	_ZNK2v88internal19AsmJsWasmStackFrame11GetFunctionEv, @function
_ZNK2v88internal19AsmJsWasmStackFrame11GetFunctionEv:
.LFB20690:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	addq	$104, %rax
	ret
	.cfi_endproc
.LFE20690:
	.size	_ZNK2v88internal19AsmJsWasmStackFrame11GetFunctionEv, .-_ZNK2v88internal19AsmJsWasmStackFrame11GetFunctionEv
	.section	.text._ZN2v88internal14WasmStackFrameD2Ev,"axG",@progbits,_ZN2v88internal14WasmStackFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14WasmStackFrameD2Ev
	.type	_ZN2v88internal14WasmStackFrameD2Ev, @function
_ZN2v88internal14WasmStackFrameD2Ev:
.LFB20705:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE20705:
	.size	_ZN2v88internal14WasmStackFrameD2Ev, .-_ZN2v88internal14WasmStackFrameD2Ev
	.weak	_ZN2v88internal14WasmStackFrameD1Ev
	.set	_ZN2v88internal14WasmStackFrameD1Ev,_ZN2v88internal14WasmStackFrameD2Ev
	.section	.text._ZN2v88internal19AsmJsWasmStackFrameD2Ev,"axG",@progbits,_ZN2v88internal19AsmJsWasmStackFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal19AsmJsWasmStackFrameD2Ev
	.type	_ZN2v88internal19AsmJsWasmStackFrameD2Ev, @function
_ZN2v88internal19AsmJsWasmStackFrameD2Ev:
.LFB25841:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE25841:
	.size	_ZN2v88internal19AsmJsWasmStackFrameD2Ev, .-_ZN2v88internal19AsmJsWasmStackFrameD2Ev
	.weak	_ZN2v88internal19AsmJsWasmStackFrameD1Ev
	.set	_ZN2v88internal19AsmJsWasmStackFrameD1Ev,_ZN2v88internal19AsmJsWasmStackFrameD2Ev
	.section	.text._ZN2v88internal12JSStackFrameD2Ev,"axG",@progbits,_ZN2v88internal12JSStackFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12JSStackFrameD2Ev
	.type	_ZN2v88internal12JSStackFrameD2Ev, @function
_ZN2v88internal12JSStackFrameD2Ev:
.LFB25845:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE25845:
	.size	_ZN2v88internal12JSStackFrameD2Ev, .-_ZN2v88internal12JSStackFrameD2Ev
	.weak	_ZN2v88internal12JSStackFrameD1Ev
	.set	_ZN2v88internal12JSStackFrameD1Ev,_ZN2v88internal12JSStackFrameD2Ev
	.section	.text._ZNK2v88internal14WasmStackFrame11GetFunctionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal14WasmStackFrame11GetFunctionEv
	.type	_ZNK2v88internal14WasmStackFrame11GetFunctionEv, @function
_ZNK2v88internal14WasmStackFrame11GetFunctionEv:
.LFB20678:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	8(%rdi), %rbx
	movslq	24(%rdi), %rsi
	movq	41112(%rbx), %rdi
	salq	$32, %rsi
	testq	%rdi, %rdi
	je	.L31
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L35
.L33:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L33
	.cfi_endproc
.LFE20678:
	.size	_ZNK2v88internal14WasmStackFrame11GetFunctionEv, .-_ZNK2v88internal14WasmStackFrame11GetFunctionEv
	.section	.text._ZNK2v88internal14WasmStackFrame9GetScriptEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal14WasmStackFrame9GetScriptEv
	.type	_ZNK2v88internal14WasmStackFrame9GetScriptEv, @function
_ZNK2v88internal14WasmStackFrame9GetScriptEv:
.LFB20687:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	16(%rdi), %rax
	movq	8(%rdi), %rbx
	movq	(%rax), %rax
	movq	41112(%rbx), %rdi
	movq	135(%rax), %rax
	movq	39(%rax), %rsi
	testq	%rdi, %rdi
	je	.L37
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L41
.L39:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L39
	.cfi_endproc
.LFE20687:
	.size	_ZNK2v88internal14WasmStackFrame9GetScriptEv, .-_ZNK2v88internal14WasmStackFrame9GetScriptEv
	.section	.text._ZN2v88internal19AsmJsWasmStackFrame11GetFileNameEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19AsmJsWasmStackFrame11GetFileNameEv
	.type	_ZN2v88internal19AsmJsWasmStackFrame11GetFileNameEv, @function
_ZN2v88internal19AsmJsWasmStackFrame11GetFileNameEv:
.LFB20691:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	16(%rdi), %rax
	movq	8(%rdi), %r12
	movq	(%rax), %rax
	movq	41112(%r12), %rdi
	movq	135(%rax), %rax
	movq	39(%rax), %rsi
	testq	%rdi, %rdi
	je	.L43
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L44:
	movq	8(%rbx), %rbx
	movq	15(%rsi), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L46
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L50
.L45:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L46:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L51
.L48:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	.cfi_restore_state
	movq	%r12, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L51:
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L48
	.cfi_endproc
.LFE20691:
	.size	_ZN2v88internal19AsmJsWasmStackFrame11GetFileNameEv, .-_ZN2v88internal19AsmJsWasmStackFrame11GetFileNameEv
	.section	.text._ZN2v88internal19AsmJsWasmStackFrame13GetLineNumberEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19AsmJsWasmStackFrame13GetLineNumberEv
	.type	_ZN2v88internal19AsmJsWasmStackFrame13GetLineNumberEv, @function
_ZN2v88internal19AsmJsWasmStackFrame13GetLineNumberEv:
.LFB20694:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	16(%rdi), %rax
	movq	8(%rdi), %rbx
	movq	(%rax), %rax
	movq	41112(%rbx), %rdi
	movq	135(%rax), %rax
	movq	39(%rax), %rsi
	testq	%rdi, %rdi
	je	.L53
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L54:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*96(%rax)
	movq	%r13, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal6Script13GetLineNumberENS0_6HandleIS1_EEi@PLT
	addq	$24, %rsp
	popq	%rbx
	addl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore_state
	movq	41088(%rbx), %r13
	cmpq	41096(%rbx), %r13
	je	.L57
.L55:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, 0(%r13)
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L57:
	movq	%rbx, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L55
	.cfi_endproc
.LFE20694:
	.size	_ZN2v88internal19AsmJsWasmStackFrame13GetLineNumberEv, .-_ZN2v88internal19AsmJsWasmStackFrame13GetLineNumberEv
	.section	.text._ZN2v88internal19AsmJsWasmStackFrame15GetColumnNumberEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19AsmJsWasmStackFrame15GetColumnNumberEv
	.type	_ZN2v88internal19AsmJsWasmStackFrame15GetColumnNumberEv, @function
_ZN2v88internal19AsmJsWasmStackFrame15GetColumnNumberEv:
.LFB20695:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	16(%rdi), %rax
	movq	8(%rdi), %rbx
	movq	(%rax), %rax
	movq	41112(%rbx), %rdi
	movq	135(%rax), %rax
	movq	39(%rax), %rsi
	testq	%rdi, %rdi
	je	.L59
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L60:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*96(%rax)
	movq	%r13, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal6Script15GetColumnNumberENS0_6HandleIS1_EEi@PLT
	addq	$24, %rsp
	popq	%rbx
	addl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	movq	41088(%rbx), %r13
	cmpq	41096(%rbx), %r13
	je	.L63
.L61:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, 0(%r13)
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L63:
	movq	%rbx, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L61
	.cfi_endproc
.LFE20695:
	.size	_ZN2v88internal19AsmJsWasmStackFrame15GetColumnNumberEv, .-_ZN2v88internal19AsmJsWasmStackFrame15GetColumnNumberEv
	.section	.text._ZN2v88internal14WasmStackFrame15GetFunctionNameEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14WasmStackFrame15GetFunctionNameEv
	.type	_ZN2v88internal14WasmStackFrame15GetFunctionNameEv, @function
_ZN2v88internal14WasmStackFrame15GetFunctionNameEv:
.LFB20679:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rax
	movq	8(%rdi), %r12
	movq	(%rax), %rax
	movq	41112(%r12), %rdi
	movq	135(%rax), %r13
	testq	%rdi, %rdi
	je	.L65
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L66:
	movl	24(%rbx), %edx
	movq	8(%rbx), %rdi
	call	_ZN2v88internal16WasmModuleObject21GetFunctionNameOrNullEPNS0_7IsolateENS0_6HandleIS1_EEj@PLT
	testq	%rax, %rax
	je	.L71
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L72
.L67:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, (%rsi)
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L71:
	movq	8(%rbx), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	addq	$104, %rax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L67
	.cfi_endproc
.LFE20679:
	.size	_ZN2v88internal14WasmStackFrame15GetFunctionNameEv, .-_ZN2v88internal14WasmStackFrame15GetFunctionNameEv
	.section	.text._ZN2v88internal14WasmStackFrame17GetWasmModuleNameEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14WasmStackFrame17GetWasmModuleNameEv
	.type	_ZN2v88internal14WasmStackFrame17GetWasmModuleNameEv, @function
_ZN2v88internal14WasmStackFrame17GetWasmModuleNameEv:
.LFB20680:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rax
	movq	8(%rdi), %r12
	movq	(%rax), %rax
	movq	41112(%r12), %rdi
	movq	135(%rax), %r13
	testq	%rdi, %rdi
	je	.L74
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	8(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal16WasmModuleObject19GetModuleNameOrNullEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	testq	%rax, %rax
	je	.L80
.L78:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L81
.L76:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, (%rsi)
	movq	8(%rbx), %rdi
	call	_ZN2v88internal16WasmModuleObject19GetModuleNameOrNullEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	testq	%rax, %rax
	jne	.L78
.L80:
	movq	8(%rbx), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	addq	$104, %rax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L76
	.cfi_endproc
.LFE20680:
	.size	_ZN2v88internal14WasmStackFrame17GetWasmModuleNameEv, .-_ZN2v88internal14WasmStackFrame17GetWasmModuleNameEv
	.section	.text._ZNK2v88internal14WasmStackFrame11GetPositionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal14WasmStackFrame11GetPositionEv
	.type	_ZNK2v88internal14WasmStackFrame11GetPositionEv, @function
_ZNK2v88internal14WasmStackFrame11GetPositionEv:
.LFB20682:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %r8
	movl	40(%rdi), %esi
	testq	%r8, %r8
	je	.L82
	movq	%r8, %rdi
	jmp	_ZN2v88internal12FrameSummary24WasmCompiledFrameSummary21GetWasmSourcePositionEPKNS0_4wasm8WasmCodeEi@PLT
	.p2align 4,,10
	.p2align 3
.L82:
	movl	%esi, %eax
	ret
	.cfi_endproc
.LFE20682:
	.size	_ZNK2v88internal14WasmStackFrame11GetPositionEv, .-_ZNK2v88internal14WasmStackFrame11GetPositionEv
	.section	.text._ZNK2v88internal19AsmJsWasmStackFrame11GetReceiverEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal19AsmJsWasmStackFrame11GetReceiverEv
	.type	_ZNK2v88internal19AsmJsWasmStackFrame11GetReceiverEv, @function
_ZNK2v88internal19AsmJsWasmStackFrame11GetReceiverEv:
.LFB20689:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -24
	movq	8(%rdi), %rbx
	leaq	-32(%rbp), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	12464(%rbx), %rax
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal7Context12global_proxyEv@PLT
	movq	41112(%rbx), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L85
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L86:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L90
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L91
.L87:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L91:
	movq	%rbx, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L87
.L90:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20689:
	.size	_ZNK2v88internal19AsmJsWasmStackFrame11GetReceiverEv, .-_ZNK2v88internal19AsmJsWasmStackFrame11GetReceiverEv
	.section	.text._ZNK2v88internal19AsmJsWasmStackFrame11GetPositionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal19AsmJsWasmStackFrame11GetPositionEv
	.type	_ZNK2v88internal19AsmJsWasmStackFrame11GetPositionEv, @function
_ZNK2v88internal19AsmJsWasmStackFrame11GetPositionEv:
.LFB20693:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	40(%rdi), %esi
	movq	32(%rdi), %rdi
	call	_ZN2v88internal12FrameSummary24WasmCompiledFrameSummary21GetWasmSourcePositionEPKNS0_4wasm8WasmCodeEi@PLT
	movq	8(%rbx), %r13
	movl	%eax, %r12d
	movq	16(%rbx), %rax
	movq	41112(%r13), %rdi
	movq	(%rax), %rax
	movq	135(%rax), %rsi
	testq	%rdi, %rdi
	je	.L93
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdi
.L94:
	movzbl	44(%rbx), %ecx
	movl	24(%rbx), %esi
	addq	$24, %rsp
	movl	%r12d, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal16WasmModuleObject17GetSourcePositionENS0_6HandleIS1_EEjjb@PLT
	.p2align 4,,10
	.p2align 3
.L93:
	.cfi_restore_state
	movq	41088(%r13), %rdi
	cmpq	41096(%r13), %rdi
	je	.L97
.L95:
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%rdi)
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L97:
	movq	%r13, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	movq	%rax, %rdi
	jmp	.L95
	.cfi_endproc
.LFE20693:
	.size	_ZNK2v88internal19AsmJsWasmStackFrame11GetPositionEv, .-_ZNK2v88internal19AsmJsWasmStackFrame11GetPositionEv
	.section	.text._ZN2v88internal14WasmStackFrameD0Ev,"axG",@progbits,_ZN2v88internal14WasmStackFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14WasmStackFrameD0Ev
	.type	_ZN2v88internal14WasmStackFrameD0Ev, @function
_ZN2v88internal14WasmStackFrameD0Ev:
.LFB20707:
	.cfi_startproc
	endbr64
	movl	$48, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE20707:
	.size	_ZN2v88internal14WasmStackFrameD0Ev, .-_ZN2v88internal14WasmStackFrameD0Ev
	.section	.text._ZN2v88internal19AsmJsWasmStackFrameD0Ev,"axG",@progbits,_ZN2v88internal19AsmJsWasmStackFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal19AsmJsWasmStackFrameD0Ev
	.type	_ZN2v88internal19AsmJsWasmStackFrameD0Ev, @function
_ZN2v88internal19AsmJsWasmStackFrameD0Ev:
.LFB25843:
	.cfi_startproc
	endbr64
	movl	$48, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE25843:
	.size	_ZN2v88internal19AsmJsWasmStackFrameD0Ev, .-_ZN2v88internal19AsmJsWasmStackFrameD0Ev
	.section	.text._ZN2v88internal12JSStackFrameD0Ev,"axG",@progbits,_ZN2v88internal12JSStackFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12JSStackFrameD0Ev
	.type	_ZN2v88internal12JSStackFrameD0Ev, @function
_ZN2v88internal12JSStackFrameD0Ev:
.LFB25847:
	.cfi_startproc
	endbr64
	movl	$56, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE25847:
	.size	_ZN2v88internal12JSStackFrameD0Ev, .-_ZN2v88internal12JSStackFrameD0Ev
	.section	.text._ZN2v88internal14StackFrameBase6IsEvalEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14StackFrameBase6IsEvalEv
	.type	_ZN2v88internal14StackFrameBase6IsEvalEv, @function
_ZN2v88internal14StackFrameBase6IsEvalEv:
.LFB20650:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rdi), %rax
	call	*184(%rax)
	testb	%al, %al
	je	.L101
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*192(%rax)
	movq	(%rax), %rax
	movslq	99(%rax), %rax
	andl	$1, %eax
.L101:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20650:
	.size	_ZN2v88internal14StackFrameBase6IsEvalEv, .-_ZN2v88internal14StackFrameBase6IsEvalEv
	.section	.text._ZN2v88internal12JSStackFrame15GetFunctionNameEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12JSStackFrame15GetFunctionNameEv
	.type	_ZN2v88internal12JSStackFrame15GetFunctionNameEv, @function
_ZN2v88internal12JSStackFrame15GetFunctionNameEv:
.LFB20661:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	24(%rdi), %rdi
	call	_ZN2v88internal10JSFunction12GetDebugNameENS0_6HandleIS1_EE@PLT
	movq	(%rax), %rdx
	movl	11(%rdx), %edx
	testl	%edx, %edx
	jne	.L109
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*184(%rax)
	testb	%al, %al
	je	.L110
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*192(%rax)
	movq	(%rax), %rax
	testb	$1, 99(%rax)
	je	.L110
	movq	8(%rbx), %rax
	addq	$2496, %rax
.L109:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L110:
	.cfi_restore_state
	movq	8(%rbx), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	addq	$104, %rax
	ret
	.cfi_endproc
.LFE20661:
	.size	_ZN2v88internal12JSStackFrame15GetFunctionNameEv, .-_ZN2v88internal12JSStackFrame15GetFunctionNameEv
	.section	.text._ZNK2v88internal12JSStackFrame11GetPositionEv.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK2v88internal12JSStackFrame11GetPositionEv.part.0, @function
_ZNK2v88internal12JSStackFrame11GetPositionEv.part.0:
.LFB26069:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	movq	(%rax), %rax
	movq	23(%rax), %r13
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L120
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L121:
	movq	8(%rbx), %rdi
	call	_ZN2v88internal18SharedFunctionInfo30EnsureSourcePositionsAvailableEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	32(%rbx), %rax
	movl	40(%rbx), %esi
	leaq	-48(%rbp), %rdi
	movq	(%rax), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal12AbstractCode14SourcePositionEi@PLT
	cmpb	$0, 44(%rbx)
	movl	%eax, 48(%rbx)
	jne	.L119
	movb	$1, 44(%rbx)
.L119:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L127
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L120:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L128
.L122:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, (%rsi)
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L128:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L122
.L127:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26069:
	.size	_ZNK2v88internal12JSStackFrame11GetPositionEv.part.0, .-_ZNK2v88internal12JSStackFrame11GetPositionEv.part.0
	.section	.text._ZN2v88internal12JSStackFrame13GetLineNumberEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12JSStackFrame13GetLineNumberEv
	.type	_ZN2v88internal12JSStackFrame13GetLineNumberEv, @function
_ZN2v88internal12JSStackFrame13GetLineNumberEv:
.LFB20667:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	(%rdi), %rax
	movq	%rdi, %r12
	call	*184(%rax)
	testb	%al, %al
	je	.L134
	movq	(%r12), %rdx
	leaq	_ZNK2v88internal12JSStackFrame11GetPositionEv(%rip), %rcx
	movq	96(%rdx), %rax
	cmpq	%rcx, %rax
	jne	.L131
	cmpb	$0, 44(%r12)
	je	.L132
	movl	48(%r12), %r13d
.L133:
	movq	%r12, %rdi
	call	*192(%rdx)
	movl	%r13d, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal6Script13GetLineNumberENS0_6HandleIS1_EEi@PLT
	addl	$1, %eax
.L129:
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L132:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal12JSStackFrame11GetPositionEv.part.0
	movq	(%r12), %rdx
	movl	%eax, %r13d
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L131:
	movq	%r12, %rdi
	call	*%rax
	movq	(%r12), %rdx
	movl	%eax, %r13d
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L134:
	movl	$-1, %eax
	jmp	.L129
	.cfi_endproc
.LFE20667:
	.size	_ZN2v88internal12JSStackFrame13GetLineNumberEv, .-_ZN2v88internal12JSStackFrame13GetLineNumberEv
	.section	.text._ZN2v88internal12JSStackFrame15GetColumnNumberEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12JSStackFrame15GetColumnNumberEv
	.type	_ZN2v88internal12JSStackFrame15GetColumnNumberEv, @function
_ZN2v88internal12JSStackFrame15GetColumnNumberEv:
.LFB20668:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	(%rdi), %rax
	movq	%rdi, %r12
	call	*184(%rax)
	testb	%al, %al
	je	.L141
	movq	(%r12), %rdx
	leaq	_ZNK2v88internal12JSStackFrame11GetPositionEv(%rip), %rcx
	movq	96(%rdx), %rax
	cmpq	%rcx, %rax
	jne	.L138
	cmpb	$0, 44(%r12)
	je	.L139
	movl	48(%r12), %r13d
.L140:
	movq	%r12, %rdi
	call	*192(%rdx)
	movl	%r13d, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal6Script15GetColumnNumberENS0_6HandleIS1_EEi@PLT
	addl	$1, %eax
.L136:
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L139:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal12JSStackFrame11GetPositionEv.part.0
	movq	(%r12), %rdx
	movl	%eax, %r13d
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L138:
	movq	%r12, %rdi
	call	*%rax
	movq	(%r12), %rdx
	movl	%eax, %r13d
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L141:
	movl	$-1, %eax
	jmp	.L136
	.cfi_endproc
.LFE20668:
	.size	_ZN2v88internal12JSStackFrame15GetColumnNumberEv, .-_ZN2v88internal12JSStackFrame15GetColumnNumberEv
	.section	.text._ZN2v88internal12_GLOBAL__N_114GetStackFramesEPNS0_7IsolateENS0_6HandleINS0_10FixedArrayEEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_114GetStackFramesEPNS0_7IsolateENS0_6HandleINS0_10FixedArrayEEE, @function
_ZN2v88internal12_GLOBAL__N_114GetStackFramesEPNS0_7IsolateENS0_6HandleINS0_10FixedArrayEEE:
.LFB20731:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rsi), %rax
	movslq	11(%rax), %rbx
	movl	%ebx, %esi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	%rax, -56(%rbp)
	testq	%rbx, %rbx
	jle	.L167
	leal	-1(%rbx), %eax
	movl	$16, %r12d
	leaq	24(,%rax,8), %rax
	movq	%rax, -64(%rbp)
	leaq	3616(%r14), %rax
	movq	%rax, -72(%rbp)
.L168:
	movq	0(%r13), %rax
	movq	-1(%r12,%rax), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L146
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rbx
.L147:
	movq	12464(%r14), %rax
	movq	39(%rax), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L149
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L150:
	movq	295(%rsi), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L152
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdi
.L153:
	xorl	%edx, %edx
	movq	%rdi, %rsi
	call	_ZN2v88internal8JSObject3NewENS0_6HandleINS0_10JSFunctionEEENS2_INS0_10JSReceiverEEENS2_INS0_14AllocationSiteEEE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L156
	movq	(%rbx), %rax
	movq	41112(%r14), %rdi
	movq	7(%rax), %rsi
	testq	%rdi, %rdi
	je	.L157
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L158:
	movq	(%rbx), %rax
	movq	-72(%rbp), %rsi
	movl	$2, %ecx
	movq	%r15, %rdi
	movq	15(%rax), %rbx
	call	_ZN2v88internal8JSObject30SetOwnPropertyIgnoreAttributesENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	testq	%rax, %rax
	je	.L156
	sarq	$32, %rbx
	movq	41112(%r14), %rdi
	leaq	3624(%r14), %r10
	movq	%rbx, %rsi
	salq	$32, %rsi
	testq	%rdi, %rdi
	je	.L161
	movq	%r10, -80(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-80(%rbp), %r10
	movq	%rax, %rdx
.L162:
	movl	$2, %ecx
	movq	%r10, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8JSObject30SetOwnPropertyIgnoreAttributesENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	testq	%rax, %rax
	je	.L156
	movq	-56(%rbp), %rax
	movq	(%r15), %rbx
	movq	(%rax), %rdi
	leaq	-1(%rdi,%r12), %rsi
	movq	%rbx, (%rsi)
	testb	$1, %bl
	je	.L169
	movq	%rbx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -80(%rbp)
	testl	$262144, %eax
	je	.L165
	movq	%rbx, %rdx
	movq	%rsi, -96(%rbp)
	movq	%rdi, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %rcx
	movq	-96(%rbp), %rsi
	movq	-88(%rbp), %rdi
	movq	8(%rcx), %rax
.L165:
	testb	$24, %al
	je	.L169
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L169
	movq	%rbx, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L169:
	addq	$8, %r12
	cmpq	-64(%rbp), %r12
	jne	.L168
.L167:
	movq	-56(%rbp), %rsi
	movq	%r14, %rdi
	xorl	%r8d, %r8d
	movl	$3, %edx
	movq	(%rsi), %rax
	movslq	11(%rax), %rcx
	call	_ZN2v88internal7Factory22NewJSArrayWithElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE@PLT
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L152:
	.cfi_restore_state
	movq	41088(%r14), %rdi
	cmpq	41096(%r14), %rdi
	je	.L184
.L154:
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%rdi)
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L149:
	movq	41088(%r14), %rax
	cmpq	41096(%r14), %rax
	je	.L185
.L151:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r14)
	movq	%rsi, (%rax)
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L146:
	movq	41088(%r14), %rbx
	cmpq	41096(%r14), %rbx
	je	.L186
.L148:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%rbx)
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L157:
	movq	41088(%r14), %rdx
	cmpq	41096(%r14), %rdx
	je	.L187
.L159:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%rdx)
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L161:
	movq	41088(%r14), %rdx
	cmpq	41096(%r14), %rdx
	je	.L188
.L163:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%rdx)
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L156:
	addq	$56, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L186:
	.cfi_restore_state
	movq	%r14, %rdi
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L185:
	movq	%r14, %rdi
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %rsi
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L184:
	movq	%r14, %rdi
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %rsi
	movq	%rax, %rdi
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L187:
	movq	%r14, %rdi
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L188:
	movq	%r14, %rdi
	movq	%rsi, -88(%rbp)
	movq	%r10, -80(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movq	-80(%rbp), %r10
	movq	%rax, %rdx
	jmp	.L163
	.cfi_endproc
.LFE20731:
	.size	_ZN2v88internal12_GLOBAL__N_114GetStackFramesEPNS0_7IsolateENS0_6HandleINS0_10FixedArrayEEE, .-_ZN2v88internal12_GLOBAL__N_114GetStackFramesEPNS0_7IsolateENS0_6HandleINS0_10FixedArrayEEE
	.section	.text._ZNK2v88internal12JSStackFrame11GetPositionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal12JSStackFrame11GetPositionEv
	.type	_ZNK2v88internal12JSStackFrame11GetPositionEv, @function
_ZNK2v88internal12JSStackFrame11GetPositionEv:
.LFB20672:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 44(%rdi)
	je	.L190
	movl	48(%rdi), %eax
.L189:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L199
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L190:
	.cfi_restore_state
	movq	24(%rdi), %rax
	movq	8(%rdi), %r12
	movq	(%rax), %rax
	movq	23(%rax), %r13
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L192
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L193:
	movq	8(%rbx), %rdi
	call	_ZN2v88internal18SharedFunctionInfo30EnsureSourcePositionsAvailableEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	32(%rbx), %rax
	movl	40(%rbx), %esi
	leaq	-48(%rbp), %rdi
	movq	(%rax), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal12AbstractCode14SourcePositionEi@PLT
	cmpb	$0, 44(%rbx)
	movl	%eax, 48(%rbx)
	jne	.L189
	movb	$1, 44(%rbx)
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L192:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L200
.L194:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, (%rsi)
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L200:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L194
.L199:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20672:
	.size	_ZNK2v88internal12JSStackFrame11GetPositionEv, .-_ZNK2v88internal12JSStackFrame11GetPositionEv
	.section	.text._ZN2v88internal14WasmStackFrame15GetColumnNumberEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14WasmStackFrame15GetColumnNumberEv
	.type	_ZN2v88internal14WasmStackFrame15GetColumnNumberEv, @function
_ZN2v88internal14WasmStackFrame15GetColumnNumberEv:
.LFB20683:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -32
	movl	24(%rdi), %esi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	leaq	-32(%rbp), %rdi
	movq	(%rax), %rax
	movq	135(%rax), %rax
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal16WasmModuleObject17GetFunctionOffsetEj@PLT
	movq	%r12, %rdi
	movl	%eax, %ebx
	movq	(%r12), %rax
	call	*96(%rax)
	addl	%ebx, %eax
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L204
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L204:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20683:
	.size	_ZN2v88internal14WasmStackFrame15GetColumnNumberEv, .-_ZN2v88internal14WasmStackFrame15GetColumnNumberEv
	.section	.text._ZN2v88internal12JSStackFrame10IsToplevelEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12JSStackFrame10IsToplevelEv
	.type	_ZN2v88internal12JSStackFrame10IsToplevelEv, @function
_ZN2v88internal12JSStackFrame10IsToplevelEv:
.LFB20671:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L211
	movq	8(%rdi), %rdx
	movl	$1, %r8d
	cmpq	%rax, 104(%rdx)
	jne	.L212
.L205:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L211:
	movq	-1(%rax), %rdx
	movl	$1, %r8d
	cmpw	$1026, 11(%rdx)
	je	.L205
	movq	8(%rdi), %rdx
	movl	$1, %r8d
	cmpq	%rax, 104(%rdx)
	je	.L205
.L212:
	cmpq	%rax, 88(%rdx)
	sete	%r8b
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE20671:
	.size	_ZN2v88internal12JSStackFrame10IsToplevelEv, .-_ZN2v88internal12JSStackFrame10IsToplevelEv
	.section	.text._ZN2v88internal12_GLOBAL__N_115CheckMethodNameEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_4NameEEENS4_INS0_10JSFunctionEEENS0_14LookupIterator13ConfigurationE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_115CheckMethodNameEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_4NameEEENS4_INS0_10JSFunctionEEENS0_14LookupIterator13ConfigurationE, @function
_ZN2v88internal12_GLOBAL__N_115CheckMethodNameEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_4NameEEENS4_INS0_10JSFunctionEEENS0_14LookupIterator13ConfigurationE:
.LFB20662:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	movq	%rdx, %rcx
	pushq	%r14
	.cfi_offset 14, -32
	movl	%r8d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$216, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L250
.L215:
	movq	(%rbx), %rax
	testb	$1, %al
	jne	.L222
.L224:
	movl	$-1, %edx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rcx, -248(%rbp)
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	-248(%rbp), %rcx
	movq	%rax, %rdx
.L223:
	movq	(%r12), %rax
	movq	-1(%rax), %rsi
	cmpw	$64, 11(%rsi)
	jne	.L225
	testb	$1, 11(%rax)
	movl	$0, %eax
	cmovne	%eax, %r14d
.L225:
	movabsq	$824633720832, %rax
	movl	%r14d, -224(%rbp)
	movq	%rax, -212(%rbp)
	movq	(%r12), %rax
	movq	%r13, -200(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L251
.L226:
	leaq	-224(%rbp), %rdi
	movq	%rcx, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	%rbx, -176(%rbp)
	movq	$0, -168(%rbp)
	movq	%rdx, -160(%rbp)
	movq	$-1, -152(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	movl	-220(%rbp), %eax
	cmpl	$6, %eax
	je	.L252
.L227:
	xorl	%r8d, %r8d
	cmpl	$5, %eax
	je	.L253
.L213:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L254
	addq	$216, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L250:
	.cfi_restore_state
	movq	%rax, -144(%rbp)
	movl	7(%rax), %eax
	testb	$1, %al
	jne	.L216
	testb	$2, %al
	jne	.L215
.L216:
	leaq	-144(%rbp), %r8
	leaq	-228(%rbp), %rsi
	movq	%rcx, -256(%rbp)
	movq	%r8, %rdi
	movq	%r8, -248(%rbp)
	call	_ZN2v88internal6String16SlowAsArrayIndexEPj@PLT
	movq	-256(%rbp), %rcx
	testb	%al, %al
	je	.L215
	movq	(%rbx), %rax
	movl	-228(%rbp), %edx
	movq	-248(%rbp), %r8
	testb	$1, %al
	jne	.L218
.L220:
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%r8, -256(%rbp)
	movl	%edx, -248(%rbp)
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movl	-248(%rbp), %edx
	movq	-256(%rbp), %r8
.L219:
	movabsq	$824633720832, %rsi
	movq	%r8, %rdi
	movq	%rax, -80(%rbp)
	movl	%r14d, -144(%rbp)
	movq	%rsi, -132(%rbp)
	movq	%r13, -120(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%rbx, -96(%rbp)
	movq	$0, -88(%rbp)
	movl	%edx, -72(%rbp)
	movl	$-1, -68(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	movq	%r12, -112(%rbp)
	movdqa	-128(%rbp), %xmm1
	movdqa	-144(%rbp), %xmm0
	movdqa	-112(%rbp), %xmm2
	movdqa	-96(%rbp), %xmm3
	movdqa	-80(%rbp), %xmm4
	movaps	%xmm1, -208(%rbp)
	movaps	%xmm0, -224(%rbp)
	movl	-220(%rbp), %eax
	movaps	%xmm2, -192(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm4, -160(%rbp)
	cmpl	$6, %eax
	jne	.L227
.L252:
	leaq	-224(%rbp), %rdi
	call	_ZNK2v88internal14LookupIterator12GetDataValueEv@PLT
	cmpq	%r15, %rax
	je	.L234
	testq	%r15, %r15
	je	.L231
	testq	%rax, %rax
	je	.L231
	movq	(%rax), %rax
	cmpq	%rax, (%r15)
	sete	%r8b
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L222:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L224
	movq	%rbx, %rdx
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L253:
	leaq	-224(%rbp), %rdi
	call	_ZNK2v88internal14LookupIterator12GetAccessorsEv@PLT
	movq	(%rax), %rax
	testb	$1, %al
	je	.L231
	movq	-1(%rax), %rdx
	cmpw	$79, 11(%rdx)
	je	.L255
	.p2align 4,,10
	.p2align 3
.L231:
	xorl	%r8d, %r8d
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L218:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L220
	movq	%rbx, %rax
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L251:
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rdx, -248(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-248(%rbp), %rdx
	movq	%rax, %rcx
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L234:
	movl	$1, %r8d
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L255:
	movq	(%r15), %rdx
	movl	$1, %r8d
	cmpq	7(%rax), %rdx
	je	.L213
	cmpq	%rdx, 15(%rax)
	sete	%r8b
	jmp	.L213
.L254:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20662:
	.size	_ZN2v88internal12_GLOBAL__N_115CheckMethodNameEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_4NameEEENS4_INS0_10JSFunctionEEENS0_14LookupIterator13ConfigurationE, .-_ZN2v88internal12_GLOBAL__N_115CheckMethodNameEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_4NameEEENS4_INS0_10JSFunctionEEENS0_14LookupIterator13ConfigurationE
	.section	.rodata._ZN2v88internal12_GLOBAL__N_120BuildDefaultCallSiteEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE.str1.1,"aMS",@progbits,1
.LC0:
	.string	" \""
.LC1:
	.string	"(location_) != nullptr"
.LC2:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal12_GLOBAL__N_120BuildDefaultCallSiteEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_120BuildDefaultCallSiteEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE, @function
_ZN2v88internal12_GLOBAL__N_120BuildDefaultCallSiteEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE:
.LFB20769:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	movq	%rdi, %rsi
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-80(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal24IncrementalStringBuilderC1EPNS0_7IsolateE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object6TypeOfEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	movq	(%r14), %rax
	movq	%rax, %rdx
	notq	%rdx
	testb	$1, %al
	jne	.L318
.L258:
	cmpq	%rax, 104(%r12)
	je	.L319
	cmpq	112(%r12), %rax
	je	.L320
	cmpq	120(%r12), %rax
	je	.L321
	andl	$1, %edx
	je	.L322
.L292:
	movl	-72(%rbp), %esi
	testl	%esi, %esi
	jne	.L323
	movq	-48(%rbp), %rax
	movq	(%rax), %rdx
	movl	-60(%rbp), %eax
	leal	1(%rax), %ecx
	addl	$16, %eax
	cltq
	movl	%ecx, -60(%rbp)
	movb	$32, -1(%rdx,%rax)
	movl	-64(%rbp), %eax
	cmpl	%eax, -60(%rbp)
	je	.L297
.L296:
	movl	$1, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory14NumberToStringENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6FinishEv@PLT
	testq	%rax, %rax
	jne	.L298
.L325:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L318:
	movq	-1(%rax), %rcx
	cmpw	$63, 11(%rcx)
	ja	.L258
	movl	-72(%rbp), %edi
	testl	%edi, %edi
	je	.L300
	movl	-60(%rbp), %eax
	movl	$32, %edx
	leaq	.LC0(%rip), %r12
	.p2align 4,,10
	.p2align 3
.L261:
	movq	-48(%rbp), %rcx
	leal	1(%rax), %esi
	leal	16(%rax,%rax), %eax
	addq	$1, %r12
	cltq
	movq	(%rcx), %rcx
	movl	%esi, -60(%rbp)
	movw	%dx, -1(%rcx,%rax)
	movl	-60(%rbp), %eax
	cmpl	-64(%rbp), %eax
	je	.L324
	movzbl	(%r12), %edx
	testb	%dl, %dl
	jne	.L261
.L262:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	movq	-48(%rbp), %rax
	movl	-72(%rbp), %esi
	movq	(%rax), %rdx
	movl	-60(%rbp), %eax
	testl	%esi, %esi
	leal	1(%rax), %ecx
	movl	%ecx, -60(%rbp)
	jne	.L311
	addl	$16, %eax
	cltq
	movb	$34, -1(%rdx,%rax)
	movl	-64(%rbp), %eax
	cmpl	%eax, -60(%rbp)
	jne	.L269
.L272:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L319:
	movl	-72(%rbp), %r14d
	testl	%r14d, %r14d
	je	.L274
	movq	-48(%rbp), %rax
	movl	$32, %r11d
	movq	(%rax), %rdx
	movl	-60(%rbp), %eax
	leal	1(%rax), %ecx
	leal	16(%rax,%rax), %eax
	cltq
	movl	%ecx, -60(%rbp)
	movw	%r11w, -1(%rdx,%rax)
	movl	-64(%rbp), %eax
	cmpl	%eax, -60(%rbp)
	je	.L278
.L277:
	leaq	2960(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
.L269:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6FinishEv@PLT
	testq	%rax, %rax
	je	.L325
	.p2align 4,,10
	.p2align 3
.L298:
	movq	-40(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L326
	addq	$56, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L274:
	.cfi_restore_state
	movq	-48(%rbp), %rax
	movq	(%rax), %rdx
	movl	-60(%rbp), %eax
	leal	1(%rax), %ecx
	addl	$16, %eax
	cltq
	movl	%ecx, -60(%rbp)
	movb	$32, -1(%rdx,%rax)
	movl	-64(%rbp), %eax
	cmpl	%eax, -60(%rbp)
	jne	.L277
.L278:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L320:
	movl	-72(%rbp), %r10d
	testl	%r10d, %r10d
	jne	.L327
	movq	-48(%rbp), %rax
	movq	(%rax), %rdx
	movl	-60(%rbp), %eax
	leal	1(%rax), %ecx
	addl	$16, %eax
	cltq
	movl	%ecx, -60(%rbp)
	movb	$32, -1(%rdx,%rax)
	movl	-64(%rbp), %eax
	cmpl	%eax, -60(%rbp)
	je	.L284
.L283:
	leaq	3464(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L322:
	movq	-1(%rax), %rax
	cmpw	$65, 11(%rax)
	jne	.L269
	jmp	.L292
	.p2align 4,,10
	.p2align 3
.L327:
	movq	-48(%rbp), %rax
	movl	$32, %r9d
	movq	(%rax), %rdx
	movl	-60(%rbp), %eax
	leal	1(%rax), %ecx
	leal	16(%rax,%rax), %eax
	cltq
	movl	%ecx, -60(%rbp)
	movw	%r9w, -1(%rdx,%rax)
	movl	-64(%rbp), %eax
	cmpl	%eax, -60(%rbp)
	jne	.L283
.L284:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L323:
	movq	-48(%rbp), %rax
	movq	(%rax), %rdx
	movl	-60(%rbp), %eax
	leal	1(%rax), %ecx
	leal	16(%rax,%rax), %eax
	movl	%ecx, -60(%rbp)
	cltq
	movl	$32, %ecx
	movw	%cx, -1(%rdx,%rax)
	movl	-64(%rbp), %eax
	cmpl	%eax, -60(%rbp)
	jne	.L296
.L297:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L311:
	leal	16(%rax,%rax), %eax
	movl	$34, %ecx
	cltq
	movw	%cx, -1(%rdx,%rax)
	movl	-64(%rbp), %eax
	cmpl	%eax, -60(%rbp)
	jne	.L269
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L324:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%r12), %edx
	testb	%dl, %dl
	je	.L262
	movl	-60(%rbp), %eax
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L321:
	movl	-72(%rbp), %r8d
	testl	%r8d, %r8d
	jne	.L328
	movq	-48(%rbp), %rax
	movq	(%rax), %rdx
	movl	-60(%rbp), %eax
	leal	1(%rax), %ecx
	addl	$16, %eax
	cltq
	movl	%ecx, -60(%rbp)
	movb	$32, -1(%rdx,%rax)
	movl	-64(%rbp), %eax
	cmpl	%eax, -60(%rbp)
	je	.L290
.L289:
	leaq	2520(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L328:
	movq	-48(%rbp), %rax
	movl	$32, %edi
	movq	(%rax), %rdx
	movl	-60(%rbp), %eax
	leal	1(%rax), %ecx
	leal	16(%rax,%rax), %eax
	cltq
	movl	%ecx, -60(%rbp)
	movw	%di, -1(%rdx,%rax)
	movl	-64(%rbp), %eax
	cmpl	%eax, -60(%rbp)
	jne	.L289
.L290:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L300:
	movl	$32, %edx
	leaq	.LC0(%rip), %r12
.L317:
	movl	-60(%rbp), %eax
.L260:
	movq	-48(%rbp), %rcx
	leal	1(%rax), %esi
	addl	$16, %eax
	addq	$1, %r12
	cltq
	movq	(%rcx), %rcx
	movl	%esi, -60(%rbp)
	movb	%dl, -1(%rcx,%rax)
	movl	-60(%rbp), %eax
	cmpl	-64(%rbp), %eax
	je	.L329
	movzbl	(%r12), %edx
	testb	%dl, %dl
	jne	.L260
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L329:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%r12), %edx
	testb	%dl, %dl
	jne	.L317
	jmp	.L262
.L326:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20769:
	.size	_ZN2v88internal12_GLOBAL__N_120BuildDefaultCallSiteEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE, .-_ZN2v88internal12_GLOBAL__N_120BuildDefaultCallSiteEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	.section	.text._ZNK2v88internal12JSStackFrame9HasScriptEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal12JSStackFrame9HasScriptEv
	.type	_ZNK2v88internal12JSStackFrame9HasScriptEv, @function
_ZNK2v88internal12JSStackFrame9HasScriptEv:
.LFB20674:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	movq	(%rax), %rax
	movq	23(%rax), %rax
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L331
.L334:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L331:
	movq	-1(%rax), %rcx
	leaq	-1(%rax), %rdx
	cmpw	$86, 11(%rcx)
	je	.L335
	movq	(%rdx), %rax
	cmpw	$96, 11(%rax)
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L335:
	movq	23(%rax), %rdx
	testb	$1, %dl
	je	.L334
	movq	-1(%rdx), %rax
	subq	$1, %rdx
	cmpw	$96, 11(%rax)
	sete	%al
	ret
	.cfi_endproc
.LFE20674:
	.size	_ZNK2v88internal12JSStackFrame9HasScriptEv, .-_ZNK2v88internal12JSStackFrame9HasScriptEv
	.section	.text._ZN2v88internal12JSStackFrame11GetTypeNameEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12JSStackFrame11GetTypeNameEv
	.type	_ZN2v88internal12JSStackFrame11GetTypeNameEv, @function
_ZN2v88internal12JSStackFrame11GetTypeNameEv:
.LFB20666:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rsi
	movq	8(%rdi), %rdi
	movq	(%rsi), %rdx
	cmpq	%rdx, 104(%rdi)
	jne	.L347
.L337:
	leaq	104(%rdi), %rax
.L345:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L347:
	.cfi_restore_state
	cmpq	%rdx, 88(%rdi)
	je	.L337
	testb	$1, %dl
	jne	.L348
.L340:
	xorl	%edx, %edx
	call	_ZN2v88internal6Object12ToObjectImplEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L349
.L344:
	movq	%rsi, %rdi
	call	_ZN2v88internal10JSReceiver18GetConstructorNameENS0_6HandleIS1_EE@PLT
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L348:
	.cfi_restore_state
	movq	-1(%rdx), %rcx
	leaq	3120(%rdi), %rax
	cmpw	$1024, 11(%rcx)
	je	.L345
	movq	-1(%rdx), %rax
	cmpw	$1023, 11(%rax)
	ja	.L344
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L349:
	movq	8(%rbx), %rax
	movq	96(%rax), %rdx
	movq	%rdx, 12480(%rax)
	movq	8(%rbx), %rax
	movb	$0, 12545(%rax)
	movq	8(%rbx), %rax
	addq	$104, %rax
	jmp	.L345
	.cfi_endproc
.LFE20666:
	.size	_ZN2v88internal12JSStackFrame11GetTypeNameEv, .-_ZN2v88internal12JSStackFrame11GetTypeNameEv
	.section	.text._ZNK2v88internal12JSStackFrame9GetScriptEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal12JSStackFrame9GetScriptEv
	.type	_ZNK2v88internal12JSStackFrame9GetScriptEv, @function
_ZNK2v88internal12JSStackFrame9GetScriptEv:
.LFB20675:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	24(%rdi), %rax
	movq	8(%rdi), %rbx
	movq	(%rax), %rax
	movq	23(%rax), %rax
	movq	31(%rax), %rsi
	testb	$1, %sil
	jne	.L356
.L351:
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L352
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L352:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L357
.L354:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L356:
	.cfi_restore_state
	movq	-1(%rsi), %rax
	cmpw	$86, 11(%rax)
	jne	.L351
	movq	23(%rsi), %rsi
	jmp	.L351
	.p2align 4,,10
	.p2align 3
.L357:
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L354
	.cfi_endproc
.LFE20675:
	.size	_ZNK2v88internal12JSStackFrame9GetScriptEv, .-_ZNK2v88internal12JSStackFrame9GetScriptEv
	.section	.text._ZN2v88internal19AsmJsWasmStackFrame24GetScriptNameOrSourceUrlEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19AsmJsWasmStackFrame24GetScriptNameOrSourceUrlEv
	.type	_ZN2v88internal19AsmJsWasmStackFrame24GetScriptNameOrSourceUrlEv, @function
_ZN2v88internal19AsmJsWasmStackFrame24GetScriptNameOrSourceUrlEv:
.LFB20692:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	16(%rdi), %rax
	movq	8(%rdi), %r12
	movq	(%rax), %rax
	movq	41112(%r12), %rdi
	movq	135(%rax), %rax
	movq	39(%rax), %rsi
	testq	%rdi, %rdi
	je	.L359
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	8(%rbx), %rbx
	movq	(%rax), %rsi
	movq	103(%rsi), %r12
	testb	$1, %r12b
	jne	.L368
.L362:
	movq	15(%rsi), %r12
.L363:
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L364
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L359:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L369
.L361:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	movq	103(%rsi), %r12
	movq	8(%rbx), %rbx
	testb	$1, %r12b
	je	.L362
.L368:
	movq	-1(%r12), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L363
	movq	(%rax), %rsi
	jmp	.L362
	.p2align 4,,10
	.p2align 3
.L364:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L370
.L366:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%r12, (%rax)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L370:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L366
	.p2align 4,,10
	.p2align 3
.L369:
	movq	%r12, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L361
	.cfi_endproc
.LFE20692:
	.size	_ZN2v88internal19AsmJsWasmStackFrame24GetScriptNameOrSourceUrlEv, .-_ZN2v88internal19AsmJsWasmStackFrame24GetScriptNameOrSourceUrlEv
	.section	.text._ZN2v88internal12_GLOBAL__N_115ComputeLocationEPNS0_7IsolateEPNS0_15MessageLocationE.constprop.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_115ComputeLocationEPNS0_7IsolateEPNS0_15MessageLocationE.constprop.0, @function
_ZN2v88internal12_GLOBAL__N_115ComputeLocationEPNS0_7IsolateEPNS0_15MessageLocationE.constprop.0:
.LFB26074:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-1504(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$1528, %rsp
	movq	%rsi, -1560(%rbp)
	movq	%rdi, %rsi
	movq	%r12, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateE@PLT
	cmpq	$0, -88(%rbp)
	je	.L388
	movq	%r12, %rdi
	call	_ZN2v88internal23JavaScriptFrameIterator7AdvanceEv@PLT
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L388
	pxor	%xmm0, %xmm0
	leaq	-1536(%rbp), %rsi
	movq	$0, -1520(%rbp)
	movaps	%xmm0, -1536(%rbp)
	movq	(%rdi), %rax
	call	*136(%rax)
	movq	-1528(%rbp), %r12
	movq	-32(%r12), %rax
	movq	(%rax), %rax
	movq	23(%rax), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L375
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r13
	movq	31(%rsi), %rsi
	testb	$1, %sil
	jne	.L405
.L378:
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L379
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L380:
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal18SharedFunctionInfo30EnsureSourcePositionsAvailableEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	-24(%r12), %rax
	leaq	-1544(%rbp), %rdi
	movq	(%rax), %rax
	movq	%rax, -1544(%rbp)
	movl	-16(%r12), %esi
	call	_ZN2v88internal12AbstractCode14SourcePositionEi@PLT
	movq	(%r14), %rcx
	testb	$1, %cl
	jne	.L382
	movq	-1528(%rbp), %r15
	movq	-1536(%rbp), %r12
.L383:
	cmpq	%r12, %r15
	je	.L386
	.p2align 4,,10
	.p2align 3
.L387:
	movq	%r12, %rdi
	addq	$56, %r12
	call	_ZN2v88internal12FrameSummaryD1Ev@PLT
	cmpq	%r15, %r12
	jne	.L387
	movq	-1536(%rbp), %r12
.L386:
	testq	%r12, %r12
	je	.L388
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L388:
	xorl	%eax, %eax
.L371:
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L406
	addq	$1528, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L375:
	.cfi_restore_state
	movq	41088(%rbx), %r13
	cmpq	41096(%rbx), %r13
	je	.L407
.L377:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, 0(%r13)
	movq	31(%rsi), %rsi
	testb	$1, %sil
	je	.L378
.L405:
	movq	-1(%rsi), %rax
	cmpw	$86, 11(%rax)
	jne	.L378
	movq	23(%rsi), %rsi
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L379:
	movq	41088(%rbx), %r14
	cmpq	41096(%rbx), %r14
	je	.L408
.L381:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r14)
	jmp	.L380
	.p2align 4,,10
	.p2align 3
.L382:
	movq	-1(%rcx), %rsi
	movq	-1528(%rbp), %r15
	movq	-1536(%rbp), %r12
	cmpw	$96, 11(%rsi)
	jne	.L383
	movq	88(%rbx), %rdx
	cmpq	%rdx, 7(%rcx)
	je	.L383
	movq	-1560(%rbp), %rdx
	movl	%eax, 8(%rdx)
	addl	$1, %eax
	movq	%r14, (%rdx)
	movl	%eax, 12(%rdx)
	movl	$-1, 16(%rdx)
	movq	%r13, 24(%rdx)
	cmpq	%r12, %r15
	je	.L389
	.p2align 4,,10
	.p2align 3
.L390:
	movq	%r12, %rdi
	addq	$56, %r12
	call	_ZN2v88internal12FrameSummaryD1Ev@PLT
	cmpq	%r15, %r12
	jne	.L390
	movq	-1536(%rbp), %r12
.L389:
	testq	%r12, %r12
	je	.L391
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L391:
	movl	$1, %eax
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L408:
	movq	%rbx, %rdi
	movq	%rsi, -1568(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-1568(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L381
	.p2align 4,,10
	.p2align 3
.L407:
	movq	%rbx, %rdi
	movq	%rsi, -1568(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-1568(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L377
.L406:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26074:
	.size	_ZN2v88internal12_GLOBAL__N_115ComputeLocationEPNS0_7IsolateEPNS0_15MessageLocationE.constprop.0, .-_ZN2v88internal12_GLOBAL__N_115ComputeLocationEPNS0_7IsolateEPNS0_15MessageLocationE.constprop.0
	.section	.text._ZN2v88internal12_GLOBAL__N_126GetStringPropertyOrDefaultEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_6StringEEES8_,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_126GetStringPropertyOrDefaultEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_6StringEEES8_, @function
_ZN2v88internal12_GLOBAL__N_126GetStringPropertyOrDefaultEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_6StringEEES8_:
.LFB20752:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	movq	%rdx, %rsi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rcx, %rbx
	subq	$104, %rsp
	movq	(%rdx), %rcx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rdx, %rax
	movl	$3, %edx
	movq	-1(%rcx), %rdi
	cmpw	$64, 11(%rdi)
	jne	.L410
	movl	11(%rcx), %edx
	andl	$1, %edx
	cmpb	$1, %dl
	sbbl	%edx, %edx
	andl	$3, %edx
.L410:
	movabsq	$824633720832, %rcx
	movl	%edx, -128(%rbp)
	movq	(%rsi), %rdx
	movq	%rcx, -116(%rbp)
	movq	%r12, -104(%rbp)
	movq	-1(%rdx), %rdx
	movzwl	11(%rdx), %edx
	andl	$-32, %edx
	cmpl	$32, %edx
	je	.L428
.L411:
	movq	%r13, -80(%rbp)
	movq	%r13, -64(%rbp)
	leaq	-128(%rbp), %r13
	movq	%r13, %rdi
	movq	%rax, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	$0, -72(%rbp)
	movq	$-1, -56(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -124(%rbp)
	jne	.L412
	movq	-104(%rbp), %rax
	leaq	88(%rax), %rsi
.L413:
	movq	(%rsi), %rax
	cmpq	%rax, 88(%r12)
	jne	.L415
	movq	%rbx, %rsi
.L416:
	movq	%rsi, %rax
.L414:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L429
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L415:
	.cfi_restore_state
	testb	$1, %al
	jne	.L417
.L419:
	movq	%r12, %rdi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L416
	xorl	%eax, %eax
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L412:
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L413
	xorl	%eax, %eax
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L428:
	movq	%r12, %rdi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	jmp	.L411
	.p2align 4,,10
	.p2align 3
.L417:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L419
	jmp	.L416
.L429:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20752:
	.size	_ZN2v88internal12_GLOBAL__N_126GetStringPropertyOrDefaultEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_6StringEEES8_, .-_ZN2v88internal12_GLOBAL__N_126GetStringPropertyOrDefaultEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_6StringEEES8_
	.section	.text._ZN2v88internal12JSStackFrame8IsNativeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12JSStackFrame8IsNativeEv
	.type	_ZN2v88internal12JSStackFrame8IsNativeEv, @function
_ZN2v88internal12JSStackFrame8IsNativeEv:
.LFB20670:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNK2v88internal12JSStackFrame9HasScriptEv(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	(%rdi), %rax
	movq	184(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L431
	movq	24(%rdi), %rdx
	movq	(%rdx), %rdx
	movq	23(%rdx), %rdx
	movq	31(%rdx), %rdx
	testb	$1, %dl
	jne	.L441
.L435:
	xorl	%eax, %eax
.L430:
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L441:
	.cfi_restore_state
	movq	-1(%rdx), %rsi
	leaq	-1(%rdx), %rcx
	cmpw	$86, 11(%rsi)
	je	.L442
.L433:
	movq	(%rcx), %rdx
	cmpw	$96, 11(%rdx)
	jne	.L435
.L436:
	call	*192(%rax)
	movq	(%rax), %rax
	movl	51(%rax), %eax
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L431:
	.cfi_restore_state
	movq	%rdi, -8(%rbp)
	call	*%rdx
	testb	%al, %al
	je	.L430
	movq	-8(%rbp), %rdi
	movq	(%rdi), %rax
	jmp	.L436
	.p2align 4,,10
	.p2align 3
.L442:
	movq	23(%rdx), %rcx
	testb	$1, %cl
	je	.L435
	subq	$1, %rcx
	jmp	.L433
	.cfi_endproc
.LFE20670:
	.size	_ZN2v88internal12JSStackFrame8IsNativeEv, .-_ZN2v88internal12JSStackFrame8IsNativeEv
	.section	.text._ZN2v88internal12JSStackFrame11GetFileNameEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12JSStackFrame11GetFileNameEv
	.type	_ZN2v88internal12JSStackFrame11GetFileNameEv, @function
_ZN2v88internal12JSStackFrame11GetFileNameEv:
.LFB20660:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNK2v88internal12JSStackFrame9HasScriptEv(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	184(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L444
	movq	24(%rdi), %rdx
	movq	(%rdx), %rdx
	movq	23(%rdx), %rdx
	movq	31(%rdx), %rdx
	testb	$1, %dl
	jne	.L456
.L447:
	movq	8(%rdi), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	addq	$104, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L456:
	.cfi_restore_state
	movq	-1(%rdx), %rsi
	leaq	-1(%rdx), %rcx
	cmpw	$86, 11(%rsi)
	je	.L457
.L446:
	movq	(%rcx), %rdx
	cmpw	$96, 11(%rdx)
	jne	.L447
.L449:
	movq	8(%rdi), %rbx
	call	*192(%rax)
	movq	(%rax), %rax
	movq	41112(%rbx), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L451
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L451:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L458
.L453:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L444:
	.cfi_restore_state
	movq	%rdi, -24(%rbp)
	call	*%rdx
	movq	-24(%rbp), %rdi
	testb	%al, %al
	je	.L447
	movq	(%rdi), %rax
	jmp	.L449
	.p2align 4,,10
	.p2align 3
.L457:
	movq	23(%rdx), %rcx
	testb	$1, %cl
	je	.L447
	subq	$1, %rcx
	jmp	.L446
	.p2align 4,,10
	.p2align 3
.L458:
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L453
	.cfi_endproc
.LFE20660:
	.size	_ZN2v88internal12JSStackFrame11GetFileNameEv, .-_ZN2v88internal12JSStackFrame11GetFileNameEv
	.section	.text._ZN2v88internal12JSStackFrame24GetScriptNameOrSourceUrlEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12JSStackFrame24GetScriptNameOrSourceUrlEv
	.type	_ZN2v88internal12JSStackFrame24GetScriptNameOrSourceUrlEv, @function
_ZN2v88internal12JSStackFrame24GetScriptNameOrSourceUrlEv:
.LFB20664:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNK2v88internal12JSStackFrame9HasScriptEv(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	184(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L460
	movq	24(%rdi), %rdx
	movq	(%rdx), %rdx
	movq	23(%rdx), %rdx
	movq	31(%rdx), %rdx
	testb	$1, %dl
	jne	.L474
.L463:
	movq	8(%rdi), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	addq	$104, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L474:
	.cfi_restore_state
	movq	-1(%rdx), %rsi
	leaq	-1(%rdx), %rcx
	cmpw	$86, 11(%rsi)
	je	.L475
.L462:
	movq	(%rcx), %rdx
	cmpw	$96, 11(%rdx)
	jne	.L463
.L465:
	movq	8(%rdi), %rbx
	call	*192(%rax)
	movq	(%rax), %rax
	movq	103(%rax), %rsi
	testb	$1, %sil
	jne	.L476
.L467:
	movq	15(%rax), %rsi
.L468:
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L469
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L469:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L477
.L471:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L460:
	.cfi_restore_state
	movq	%rdi, -24(%rbp)
	call	*%rdx
	movq	-24(%rbp), %rdi
	testb	%al, %al
	je	.L463
	movq	(%rdi), %rax
	jmp	.L465
	.p2align 4,,10
	.p2align 3
.L475:
	movq	23(%rdx), %rcx
	testb	$1, %cl
	je	.L463
	subq	$1, %rcx
	jmp	.L462
	.p2align 4,,10
	.p2align 3
.L476:
	movq	-1(%rsi), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L468
	jmp	.L467
	.p2align 4,,10
	.p2align 3
.L477:
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L471
	.cfi_endproc
.LFE20664:
	.size	_ZN2v88internal12JSStackFrame24GetScriptNameOrSourceUrlEv, .-_ZN2v88internal12JSStackFrame24GetScriptNameOrSourceUrlEv
	.section	.rodata._ZN2v88internal12_GLOBAL__N_116FormatEvalOriginEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"eval at "
.LC4:
	.string	"<anonymous>"
.LC5:
	.string	" ("
.LC6:
	.string	"unknown source"
	.section	.text._ZN2v88internal12_GLOBAL__N_116FormatEvalOriginEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_116FormatEvalOriginEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEE, @function
_ZN2v88internal12_GLOBAL__N_116FormatEvalOriginEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEE:
.LFB20645:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal6Script18GetNameOrSourceURLEv@PLT
	movq	41112(%r12), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L479
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	88(%r12), %rbx
	cmpq	%rbx, (%rax)
	je	.L659
.L483:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L660
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L479:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L661
.L481:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	movq	88(%r12), %rbx
	cmpq	%rbx, (%rax)
	jne	.L483
.L659:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal24IncrementalStringBuilderC1EPNS0_7IsolateE@PLT
	movl	-88(%rbp), %ebx
	testl	%ebx, %ebx
	jne	.L662
	movl	$101, %edx
	leaq	.LC3(%rip), %r15
.L652:
	movl	-76(%rbp), %eax
.L484:
	movq	-64(%rbp), %rcx
	leal	1(%rax), %esi
	addl	$16, %eax
	addq	$1, %r15
	cltq
	movq	(%rcx), %rcx
	movl	%esi, -76(%rbp)
	movb	%dl, -1(%rcx,%rax)
	movl	-76(%rbp), %eax
	cmpl	-80(%rbp), %eax
	je	.L663
	movzbl	(%r15), %edx
	testb	%dl, %dl
	jne	.L484
.L486:
	movq	0(%r13), %rax
	movq	71(%rax), %rbx
	testb	$1, %bl
	jne	.L491
.L493:
	movq	88(%r12), %rsi
	leaq	-112(%rbp), %r15
.L492:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L517
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rbx
.L518:
	movq	%rsi, -112(%rbp)
	movq	%r15, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal6Object12BooleanValueEPNS0_7IsolateE@PLT
	testb	%al, %al
	jne	.L664
	movl	-88(%rbp), %r11d
	testl	%r11d, %r11d
	je	.L595
	movl	-76(%rbp), %eax
	movl	$60, %edx
	leaq	.LC4(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L527:
	movq	-64(%rbp), %rcx
	leal	1(%rax), %esi
	leal	16(%rax,%rax), %eax
	addq	$1, %rbx
	cltq
	movq	(%rcx), %rcx
	movl	%esi, -76(%rbp)
	movw	%dx, -1(%rcx,%rax)
	movl	-76(%rbp), %eax
	cmpl	-80(%rbp), %eax
	je	.L665
	movzbl	(%rbx), %edx
	testb	%dl, %dl
	jne	.L527
.L525:
	movq	0(%r13), %rax
	movq	71(%rax), %rbx
	testb	$1, %bl
	jne	.L666
	.p2align 4,,10
	.p2align 3
.L535:
	movq	%r14, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6FinishEv@PLT
	jmp	.L483
	.p2align 4,,10
	.p2align 3
.L662:
	movl	-76(%rbp), %eax
	movl	$101, %edx
	leaq	.LC3(%rip), %r15
	.p2align 4,,10
	.p2align 3
.L485:
	movq	-64(%rbp), %rcx
	leal	1(%rax), %esi
	leal	16(%rax,%rax), %eax
	addq	$1, %r15
	cltq
	movq	(%rcx), %rcx
	movl	%esi, -76(%rbp)
	movw	%dx, -1(%rcx,%rax)
	movl	-76(%rbp), %eax
	cmpl	-80(%rbp), %eax
	je	.L667
	movzbl	(%r15), %edx
	testb	%dl, %dl
	jne	.L485
	jmp	.L486
	.p2align 4,,10
	.p2align 3
.L667:
	movq	%r14, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%r15), %edx
	testb	%dl, %dl
	je	.L486
	movl	-76(%rbp), %eax
	jmp	.L485
	.p2align 4,,10
	.p2align 3
.L663:
	movq	%r14, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%r15), %edx
	testb	%dl, %dl
	jne	.L652
	jmp	.L486
	.p2align 4,,10
	.p2align 3
.L665:
	movq	%r14, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%rbx), %edx
	testb	%dl, %dl
	je	.L525
	movl	-76(%rbp), %eax
	jmp	.L527
	.p2align 4,,10
	.p2align 3
.L517:
	movq	41088(%r12), %rbx
	cmpq	41096(%r12), %rbx
	je	.L668
.L519:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rbx)
	jmp	.L518
	.p2align 4,,10
	.p2align 3
.L661:
	movq	%r12, %rdi
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %rsi
	jmp	.L481
	.p2align 4,,10
	.p2align 3
.L595:
	movl	$60, %edx
	leaq	.LC4(%rip), %rbx
.L653:
	movl	-76(%rbp), %eax
.L526:
	movq	-64(%rbp), %rcx
	leal	1(%rax), %esi
	addl	$16, %eax
	addq	$1, %rbx
	cltq
	movq	(%rcx), %rcx
	movl	%esi, -76(%rbp)
	movb	%dl, -1(%rcx,%rax)
	movl	-76(%rbp), %eax
	cmpl	-80(%rbp), %eax
	je	.L669
	movzbl	(%rbx), %edx
	testb	%dl, %dl
	jne	.L526
	jmp	.L525
	.p2align 4,,10
	.p2align 3
.L669:
	movq	%r14, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%rbx), %edx
	testb	%dl, %dl
	jne	.L653
	jmp	.L525
	.p2align 4,,10
	.p2align 3
.L664:
	movq	(%rbx), %rax
	testb	$1, %al
	jne	.L521
.L524:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rbx
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.L483
.L523:
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	jmp	.L525
	.p2align 4,,10
	.p2align 3
.L491:
	movq	-1(%rbx), %rax
	cmpw	$160, 11(%rax)
	jne	.L493
	movq	41112(%r12), %rdi
	movq	%rbx, %rsi
	testq	%rdi, %rdi
	je	.L494
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rdx
.L495:
	movq	15(%rsi), %rax
	leaq	15(%rsi), %rbx
	testb	$1, %al
	jne	.L670
.L497:
	cmpq	%rax, _ZN2v88internal18SharedFunctionInfo21kNoSharedNameSentinelE(%rip)
	leaq	-112(%rbp), %r15
	setne	%al
.L498:
	testb	%al, %al
	je	.L502
	movq	(%rbx), %rbx
	testb	$1, %bl
	jne	.L671
.L500:
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rdx, -120(%rbp)
	movq	%rbx, -112(%rbp)
	call	_ZN2v88internal6Object12BooleanValueEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %rdx
	testb	%al, %al
	movq	(%rdx), %rbx
	movq	15(%rbx), %rax
	je	.L503
	testb	$1, %al
	jne	.L672
.L504:
	cmpq	%rax, _ZN2v88internal18SharedFunctionInfo21kNoSharedNameSentinelE(%rip)
	setne	%al
.L505:
	testb	%al, %al
	je	.L513
	movq	15(%rbx), %rsi
	testb	$1, %sil
	je	.L492
	movq	-1(%rsi), %rax
	cmpw	$136, 11(%rax)
	jne	.L492
	movq	%r15, %rdi
	movq	%rsi, -112(%rbp)
	movq	%rsi, -120(%rbp)
	call	_ZNK2v88internal9ScopeInfo15HasFunctionNameEv@PLT
	testb	%al, %al
	je	.L513
	movq	-120(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rsi, -112(%rbp)
	call	_ZNK2v88internal9ScopeInfo12FunctionNameEv@PLT
	movq	%rax, %rsi
	jmp	.L492
	.p2align 4,,10
	.p2align 3
.L666:
	movq	-1(%rbx), %rax
	cmpw	$160, 11(%rax)
	jne	.L535
	movq	41112(%r12), %rdi
	movq	%rbx, %rsi
	testq	%rdi, %rdi
	je	.L536
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L537:
	movq	31(%rsi), %rdx
	testb	$1, %dl
	jne	.L673
.L562:
	movl	-76(%rbp), %eax
.L542:
	movl	-88(%rbp), %esi
	movq	-64(%rbp), %rdx
	leal	1(%rax), %ecx
	testl	%esi, %esi
	movq	(%rdx), %rdx
	movl	%ecx, -76(%rbp)
	jne	.L650
	addl	$16, %eax
	cltq
	movb	$41, -1(%rdx,%rax)
	movl	-80(%rbp), %eax
	cmpl	%eax, -76(%rbp)
	jne	.L535
.L591:
	movq	%r14, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L535
	.p2align 4,,10
	.p2align 3
.L650:
	leal	16(%rax,%rax), %eax
	movl	$41, %ecx
	cltq
	movw	%cx, -1(%rdx,%rax)
	movl	-80(%rbp), %eax
	cmpl	%eax, -76(%rbp)
	jne	.L535
	jmp	.L591
	.p2align 4,,10
	.p2align 3
.L536:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L674
.L538:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rbx, (%rax)
	jmp	.L537
	.p2align 4,,10
	.p2align 3
.L503:
	testb	$1, %al
	jne	.L675
.L510:
	movq	7(%rbx), %rax
	testb	$1, %al
	jne	.L676
.L513:
	andq	$-262144, %rbx
	movq	24(%rbx), %rax
	movq	-37464(%rax), %rsi
	jmp	.L492
	.p2align 4,,10
	.p2align 3
.L671:
	movq	-1(%rbx), %rax
	cmpw	$136, 11(%rax)
	jne	.L500
	movq	%r15, %rdi
	movq	%rsi, -128(%rbp)
	movq	%rdx, -120(%rbp)
	movq	%rbx, -112(%rbp)
	call	_ZNK2v88internal9ScopeInfo15HasFunctionNameEv@PLT
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %rsi
	testb	%al, %al
	je	.L502
	movq	%r15, %rdi
	movq	%rbx, -112(%rbp)
	call	_ZNK2v88internal9ScopeInfo12FunctionNameEv@PLT
	movq	-120(%rbp), %rdx
	movq	%rax, %rbx
	jmp	.L500
	.p2align 4,,10
	.p2align 3
.L521:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L524
	jmp	.L523
	.p2align 4,,10
	.p2align 3
.L672:
	movq	-1(%rax), %rdx
	cmpw	$136, 11(%rdx)
	jne	.L504
	movq	%r15, %rdi
	movq	%rax, -112(%rbp)
	call	_ZNK2v88internal9ScopeInfo21HasSharedFunctionNameEv@PLT
	jmp	.L505
	.p2align 4,,10
	.p2align 3
.L675:
	movq	-1(%rax), %rdx
	cmpw	$136, 11(%rdx)
	jne	.L510
	movq	%r15, %rdi
	movq	%rax, -112(%rbp)
	call	_ZNK2v88internal9ScopeInfo23HasInferredFunctionNameEv@PLT
	testb	%al, %al
	je	.L513
	movq	%r15, %rdi
	call	_ZNK2v88internal9ScopeInfo20InferredFunctionNameEv@PLT
	movq	%rax, %rsi
	testb	$1, %al
	je	.L513
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L513
	jmp	.L492
	.p2align 4,,10
	.p2align 3
.L674:
	movq	%r12, %rdi
	movq	%rbx, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %rsi
	jmp	.L538
	.p2align 4,,10
	.p2align 3
.L494:
	movq	41088(%r12), %rdx
	cmpq	41096(%r12), %rdx
	je	.L677
.L496:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%rbx, (%rdx)
	jmp	.L495
	.p2align 4,,10
	.p2align 3
.L673:
	movq	-1(%rdx), %rsi
	leaq	-1(%rdx), %rcx
	cmpw	$86, 11(%rsi)
	je	.L678
.L540:
	movq	(%rcx), %rdx
	cmpw	$96, 11(%rdx)
	jne	.L562
	movq	(%rax), %rax
	movq	31(%rax), %rsi
	testb	$1, %sil
	jne	.L679
.L543:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L680
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rbx
.L546:
	movl	-88(%rbp), %r10d
	testl	%r10d, %r10d
	je	.L596
	movl	-76(%rbp), %eax
	movl	$32, %edx
	leaq	.LC5(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L549:
	movq	-64(%rbp), %rsi
	leal	1(%rax), %edi
	leal	16(%rax,%rax), %eax
	addq	$1, %rcx
	cltq
	movq	(%rsi), %rsi
	movl	%edi, -76(%rbp)
	movw	%dx, -1(%rsi,%rax)
	movl	-76(%rbp), %eax
	cmpl	-80(%rbp), %eax
	je	.L681
	movzbl	(%rcx), %edx
	testb	%dl, %dl
	jne	.L549
.L550:
	movq	(%rbx), %rax
	testb	$1, 99(%rax)
	jne	.L555
	movq	41112(%r12), %rdi
	movq	15(%rax), %r8
	testq	%rdi, %rdi
	je	.L682
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L559:
	movq	(%rbx), %rax
	movq	15(%rax), %rax
	testb	$1, %al
	jne	.L683
.L561:
	movl	-88(%rbp), %edi
	movl	-76(%rbp), %eax
	movl	$117, %edx
	leaq	.LC6(%rip), %rbx
	testl	%edi, %edi
	jne	.L582
	movl	$117, %edx
	leaq	.LC6(%rip), %rbx
	jmp	.L581
	.p2align 4,,10
	.p2align 3
.L657:
	movzbl	(%rbx), %edx
	testb	%dl, %dl
	je	.L542
.L581:
	movq	-64(%rbp), %rcx
	leal	1(%rax), %esi
	addl	$16, %eax
	addq	$1, %rbx
	cltq
	movq	(%rcx), %rcx
	movl	%esi, -76(%rbp)
	movb	%dl, -1(%rcx,%rax)
	movl	-76(%rbp), %eax
	cmpl	-80(%rbp), %eax
	jne	.L657
	movq	%r14, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movl	-76(%rbp), %eax
	jmp	.L657
.L684:
	movq	%r14, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movl	-76(%rbp), %eax
.L655:
	movzbl	(%rbx), %edx
	testb	%dl, %dl
	je	.L542
.L582:
	movq	-64(%rbp), %rcx
	leal	1(%rax), %esi
	leal	16(%rax,%rax), %eax
	addq	$1, %rbx
	cltq
	movq	(%rcx), %rcx
	movl	%esi, -76(%rbp)
	movw	%dx, -1(%rcx,%rax)
	movl	-76(%rbp), %eax
	cmpl	-80(%rbp), %eax
	jne	.L655
	jmp	.L684
	.p2align 4,,10
	.p2align 3
.L502:
	andq	$-262144, %rsi
	movq	24(%rsi), %rax
	movq	-37464(%rax), %rbx
	jmp	.L500
	.p2align 4,,10
	.p2align 3
.L670:
	movq	-1(%rax), %rcx
	cmpw	$136, 11(%rcx)
	jne	.L497
	leaq	-112(%rbp), %r15
	movq	%rsi, -128(%rbp)
	movq	%r15, %rdi
	movq	%rdx, -120(%rbp)
	movq	%rax, -112(%rbp)
	call	_ZNK2v88internal9ScopeInfo21HasSharedFunctionNameEv@PLT
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %rsi
	jmp	.L498
	.p2align 4,,10
	.p2align 3
.L668:
	movq	%r12, %rdi
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L519
	.p2align 4,,10
	.p2align 3
.L676:
	movq	-1(%rax), %rdx
	cmpw	$165, 11(%rdx)
	je	.L516
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L513
.L516:
	movq	7(%rbx), %rax
	movq	7(%rax), %rsi
	jmp	.L492
	.p2align 4,,10
	.p2align 3
.L678:
	movq	23(%rdx), %rcx
	testb	$1, %cl
	je	.L562
	subq	$1, %rcx
	jmp	.L540
	.p2align 4,,10
	.p2align 3
.L681:
	movq	%r14, %rdi
	movq	%rcx, -120(%rbp)
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movq	-120(%rbp), %rcx
	movzbl	(%rcx), %edx
	testb	%dl, %dl
	je	.L550
	movl	-76(%rbp), %eax
	jmp	.L549
.L677:
	movq	%r12, %rdi
	movq	%rbx, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L496
.L555:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_116FormatEvalOriginEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEE
	movq	%rax, %rsi
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L483
.L658:
	movq	%r14, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	jmp	.L562
.L596:
	movl	$32, %ecx
	leaq	.LC5(%rip), %rdx
.L654:
	movl	-76(%rbp), %eax
.L548:
	movq	-64(%rbp), %rsi
	leal	1(%rax), %edi
	addl	$16, %eax
	addq	$1, %rdx
	cltq
	movq	(%rsi), %rsi
	movl	%edi, -76(%rbp)
	movb	%cl, -1(%rsi,%rax)
	movl	-76(%rbp), %eax
	cmpl	-80(%rbp), %eax
	je	.L685
	movzbl	(%rdx), %ecx
	testb	%cl, %cl
	jne	.L548
	jmp	.L550
	.p2align 4,,10
	.p2align 3
.L685:
	movq	%r14, %rdi
	movq	%rdx, -120(%rbp)
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movq	-120(%rbp), %rdx
	movzbl	(%rdx), %ecx
	testb	%cl, %cl
	jne	.L654
	jmp	.L550
.L680:
	movq	41088(%r12), %rbx
	cmpq	41096(%r12), %rbx
	je	.L686
.L547:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rbx)
	jmp	.L546
.L679:
	movq	-1(%rsi), %rax
	cmpw	$86, 11(%rax)
	jne	.L543
	movq	23(%rsi), %rsi
	jmp	.L543
.L682:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L687
.L560:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r8, (%rsi)
	jmp	.L559
.L683:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L561
	movq	%r14, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	pcmpeqd	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	%r12, %rdi
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal6Script15GetEvalPositionEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	%rbx, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal6Script15GetPositionInfoENS0_6HandleIS1_EEiPNS1_12PositionInfoENS1_10OffsetFlagE@PLT
	testb	%al, %al
	je	.L562
	cmpl	$0, -88(%rbp)
	jne	.L688
	movq	-64(%rbp), %rax
	movq	(%rax), %rdx
	movl	-76(%rbp), %eax
	leal	1(%rax), %ecx
	addl	$16, %eax
	cltq
	movl	%ecx, -76(%rbp)
	movb	$58, -1(%rdx,%rax)
	movl	-80(%rbp), %eax
	cmpl	%eax, -76(%rbp)
	je	.L569
.L568:
	movl	-112(%rbp), %eax
	movq	41112(%r12), %rdi
	leal	1(%rax), %esi
	movq	%rsi, %r13
	salq	$32, %r13
	testq	%rdi, %rdi
	je	.L689
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r8
.L570:
	movq	%r8, %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory14NumberToStringENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	cmpl	$0, -88(%rbp)
	jne	.L690
	movq	-64(%rbp), %rax
	movq	(%rax), %rdx
	movl	-76(%rbp), %eax
	leal	1(%rax), %ecx
	addl	$16, %eax
	cltq
	movl	%ecx, -76(%rbp)
	movb	$58, -1(%rdx,%rax)
	movl	-80(%rbp), %eax
	cmpl	%eax, -76(%rbp)
	je	.L578
.L577:
	movl	-108(%rbp), %eax
	movq	41112(%r12), %rdi
	leal	1(%rax), %esi
	movq	%rsi, %r13
	salq	$32, %r13
	testq	%rdi, %rdi
	je	.L691
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r8
.L579:
	movq	%r8, %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory14NumberToStringENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%rax, %rsi
	jmp	.L658
.L688:
	movq	-64(%rbp), %rax
	movl	$58, %r9d
	movq	(%rax), %rdx
	movl	-76(%rbp), %eax
	leal	1(%rax), %ecx
	leal	16(%rax,%rax), %eax
	cltq
	movl	%ecx, -76(%rbp)
	movw	%r9w, -1(%rdx,%rax)
	movl	-80(%rbp), %eax
	cmpl	%eax, -76(%rbp)
	jne	.L568
.L569:
	movq	%r14, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L568
.L690:
	movq	-64(%rbp), %rax
	movl	$58, %r8d
	movq	(%rax), %rdx
	movl	-76(%rbp), %eax
	leal	1(%rax), %ecx
	leal	16(%rax,%rax), %eax
	cltq
	movl	%ecx, -76(%rbp)
	movw	%r8w, -1(%rdx,%rax)
	movl	-80(%rbp), %eax
	cmpl	%eax, -76(%rbp)
	jne	.L577
.L578:
	movq	%r14, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L577
.L686:
	movq	%r12, %rdi
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L547
.L687:
	movq	%r12, %rdi
	movq	%r8, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L560
.L660:
	call	__stack_chk_fail@PLT
.L689:
	movq	41088(%r12), %r8
	cmpq	41096(%r12), %r8
	je	.L692
.L571:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, (%r8)
	jmp	.L570
.L691:
	movq	41088(%r12), %r8
	cmpq	41096(%r12), %r8
	je	.L693
.L580:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, (%r8)
	jmp	.L579
.L692:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r8
	jmp	.L571
.L693:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r8
	jmp	.L580
	.cfi_endproc
.LFE20645:
	.size	_ZN2v88internal12_GLOBAL__N_116FormatEvalOriginEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEE, .-_ZN2v88internal12_GLOBAL__N_116FormatEvalOriginEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEE
	.section	.text._ZN2v88internal14StackFrameBase13GetEvalOriginEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14StackFrameBase13GetEvalOriginEv
	.type	_ZN2v88internal14StackFrameBase13GetEvalOriginEv, @function
_ZN2v88internal14StackFrameBase13GetEvalOriginEv:
.LFB20646:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	call	*184(%rax)
	testb	%al, %al
	je	.L696
	movq	(%rbx), %rax
	leaq	_ZN2v88internal14StackFrameBase6IsEvalEv(%rip), %rcx
	movq	%rbx, %rdi
	movq	144(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L697
	call	*184(%rax)
	testb	%al, %al
	je	.L696
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*192(%rax)
	movq	(%rax), %rax
	testb	$1, 99(%rax)
	jne	.L700
	.p2align 4,,10
	.p2align 3
.L696:
	movq	8(%rbx), %rax
	addq	$88, %rax
.L701:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L697:
	.cfi_restore_state
	call	*%rdx
	testb	%al, %al
	je	.L696
.L700:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*192(%rax)
	movq	8(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal12_GLOBAL__N_116FormatEvalOriginEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEE
	testq	%rax, %rax
	jne	.L701
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20646:
	.size	_ZN2v88internal14StackFrameBase13GetEvalOriginEv, .-_ZN2v88internal14StackFrameBase13GetEvalOriginEv
	.section	.rodata._ZN2v88internal12JSStackFrame13GetMethodNameEv.str1.1,"aMS",@progbits,1
.LC7:
	.string	"<static_fields_initializer>"
.LC8:
	.string	"get "
.LC9:
	.string	"set "
	.section	.text._ZN2v88internal12JSStackFrame13GetMethodNameEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12JSStackFrame13GetMethodNameEv
	.type	_ZN2v88internal12JSStackFrame13GetMethodNameEv, @function
_ZN2v88internal12JSStackFrame13GetMethodNameEv:
.LFB20665:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r12
	movq	8(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%r12), %rax
	cmpq	104(%r13), %rax
	jne	.L799
.L711:
	leaq	104(%r13), %rax
.L716:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L800
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L799:
	.cfi_restore_state
	cmpq	88(%r13), %rax
	je	.L711
	movq	%rdi, %rbx
	testb	$1, %al
	jne	.L712
.L715:
	movq	%r12, %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal6Object12ToObjectImplEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L713
	movq	8(%rbx), %r13
.L714:
	movq	24(%rbx), %rax
	movq	(%rax), %rax
	movq	23(%rax), %r15
	movq	15(%r15), %rax
	testb	$1, %al
	jne	.L801
.L717:
	cmpq	%rax, _ZN2v88internal18SharedFunctionInfo21kNoSharedNameSentinelE(%rip)
	leaq	-64(%rbp), %r14
	setne	%al
.L718:
	testb	%al, %al
	je	.L722
	movq	15(%r15), %rsi
	testb	$1, %sil
	jne	.L802
.L720:
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L723
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r15
.L724:
	movq	8(%rbx), %r13
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	ja	.L728
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$1, %ax
	je	.L803
.L728:
	movq	(%r15), %rsi
.L727:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	ja	.L797
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	je	.L738
.L797:
	movq	(%r15), %rsi
.L735:
	movq	%rsi, -64(%rbp)
	movl	$27, %edx
	movq	%r14, %rdi
	leaq	.LC7(%rip), %rsi
	call	_ZN2v88internal6String16HasOneBytePrefixENS0_6VectorIKcEE@PLT
	testb	%al, %al
	je	.L742
.L798:
	movq	%r15, %rax
	jmp	.L716
	.p2align 4,,10
	.p2align 3
.L723:
	movq	41088(%r13), %r15
	cmpq	41096(%r13), %r15
	je	.L804
.L725:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r15)
	jmp	.L724
	.p2align 4,,10
	.p2align 3
.L712:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L715
	jmp	.L714
	.p2align 4,,10
	.p2align 3
.L722:
	andq	$-262144, %r15
	movq	24(%r15), %rax
	movq	-37464(%rax), %rsi
	jmp	.L720
	.p2align 4,,10
	.p2align 3
.L738:
	movq	(%r15), %rax
	movq	41112(%r13), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L739
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
	jmp	.L797
	.p2align 4,,10
	.p2align 3
.L803:
	movq	(%r15), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$1, %ax
	jne	.L730
	movq	23(%rdx), %rax
	movl	11(%rax), %ecx
	testl	%ecx, %ecx
	je	.L730
	movq	%r15, %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	movq	(%rax), %rsi
	movq	%rax, %r15
	jmp	.L735
	.p2align 4,,10
	.p2align 3
.L801:
	movq	-1(%rax), %rdx
	cmpw	$136, 11(%rdx)
	jne	.L717
	leaq	-64(%rbp), %r14
	movq	%rax, -64(%rbp)
	movq	%r14, %rdi
	call	_ZNK2v88internal9ScopeInfo21HasSharedFunctionNameEv@PLT
	jmp	.L718
	.p2align 4,,10
	.p2align 3
.L742:
	movq	(%r15), %rax
	leaq	-72(%rbp), %rdi
	movl	$4, %edx
	leaq	.LC8(%rip), %rsi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal6String16HasOneBytePrefixENS0_6VectorIKcEE@PLT
	testb	%al, %al
	jne	.L743
	movq	(%r15), %rax
	leaq	.LC9(%rip), %rsi
	movl	$4, %edx
	movq	%r14, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6String16HasOneBytePrefixENS0_6VectorIKcEE@PLT
	testb	%al, %al
	je	.L744
.L743:
	movq	(%r15), %rax
	movq	8(%rbx), %rdi
	movq	%r15, %rsi
	movl	$4, %edx
	movl	11(%rax), %ecx
	call	_ZN2v88internal7Factory18NewProperSubStringENS0_6HandleINS0_6StringEEEii@PLT
	movq	%rax, %r15
.L744:
	movq	8(%rbx), %rdi
	movq	24(%rbx), %rcx
	movq	%r15, %rdx
	movq	%r12, %rsi
	movl	$2, %r8d
	call	_ZN2v88internal12_GLOBAL__N_115CheckMethodNameEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_4NameEEENS4_INS0_10JSFunctionEEENS0_14LookupIterator13ConfigurationE
	testb	%al, %al
	jne	.L798
	movq	8(%rbx), %r9
	movq	$0, -96(%rbp)
	movq	%r12, -88(%rbp)
	movq	41088(%r9), %rax
	addl	$1, 41104(%r9)
	movq	%r9, -112(%rbp)
	movq	%rax, -120(%rbp)
	movq	41096(%r9), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -104(%rbp)
	movq	8(%rbx), %rax
	movq	%rax, -128(%rbp)
.L770:
	movq	-88(%rbp), %rax
	movq	(%rax), %r13
	testb	$1, %r13b
	jne	.L805
.L795:
	movq	-112(%rbp), %r9
	movl	41104(%r9), %eax
	movq	41096(%r9), %rcx
	leal	-1(%rax), %edx
	movq	-96(%rbp), %rax
	testq	%rax, %rax
	je	.L771
	movq	(%rax), %r12
	movq	-120(%rbp), %rax
	movl	%edx, 41104(%r9)
	movq	%rax, 41088(%r9)
	movq	-104(%rbp), %rax
	cmpq	%rcx, %rax
	je	.L772
	movq	%rax, 41096(%r9)
	movq	%r9, %rdi
	movq	%r9, -88(%rbp)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %r9
.L772:
	movq	41112(%r9), %rdi
	testq	%rdi, %rdi
	je	.L773
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	jmp	.L716
	.p2align 4,,10
	.p2align 3
.L802:
	movq	-1(%rsi), %rax
	cmpw	$136, 11(%rax)
	jne	.L720
	movq	%r14, %rdi
	movq	%rsi, -64(%rbp)
	movq	%rsi, -88(%rbp)
	call	_ZNK2v88internal9ScopeInfo15HasFunctionNameEv@PLT
	testb	%al, %al
	je	.L722
	movq	-88(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZNK2v88internal9ScopeInfo12FunctionNameEv@PLT
	movq	%rax, %rsi
	jmp	.L720
	.p2align 4,,10
	.p2align 3
.L713:
	movq	8(%rbx), %rax
	movq	96(%rax), %rdx
	movq	%rdx, 12480(%rax)
	movq	8(%rbx), %rax
	movb	$0, 12545(%rax)
	movq	8(%rbx), %rax
	addq	$104, %rax
	jmp	.L716
	.p2align 4,,10
	.p2align 3
.L739:
	movq	41088(%r13), %r15
	cmpq	41096(%r13), %r15
	je	.L806
.L741:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r15)
	jmp	.L735
	.p2align 4,,10
	.p2align 3
.L730:
	movq	41112(%r13), %rdi
	movq	15(%rdx), %rsi
	testq	%rdi, %rdi
	je	.L732
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r15
	jmp	.L727
	.p2align 4,,10
	.p2align 3
.L804:
	movq	%r13, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L725
	.p2align 4,,10
	.p2align 3
.L771:
	movq	8(%rbx), %rax
	addq	$104, %rax
.L761:
	movq	-120(%rbp), %rsi
	movl	%edx, 41104(%r9)
	movq	%rsi, 41088(%r9)
	cmpq	%rcx, -104(%rbp)
	je	.L716
	movq	%rax, -88(%rbp)
	movq	-104(%rbp), %rax
	movq	%r9, %rdi
	movq	%rax, 41096(%r9)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rax
	jmp	.L716
.L805:
	movq	-1(%r13), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L795
	movq	-1(%r13), %rax
	cmpw	$1026, 11(%rax)
	je	.L807
	movq	-1(%r13), %rax
	movzbl	13(%rax), %eax
	shrb	$5, %al
	andl	$1, %eax
.L753:
	testb	%al, %al
	jne	.L795
	movq	8(%rbx), %rdi
	movq	-88(%rbp), %rsi
	call	_ZN2v88internal14KeyAccumulator22GetOwnEnumPropertyKeysEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEE@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	movl	11(%rax), %eax
	testl	%eax, %eax
	jle	.L754
	movq	%rbx, %rax
	xorl	%r14d, %r14d
	movq	%r12, %rbx
	movq	%rax, %r12
	jmp	.L766
	.p2align 4,,10
	.p2align 3
.L755:
	subl	$1, 41104(%r13)
	movq	%r10, 41088(%r13)
	cmpq	41096(%r13), %r15
	je	.L780
	movq	%r15, 41096(%r13)
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L780:
	movq	(%rbx), %rax
	addq	$1, %r14
	cmpl	%r14d, 11(%rax)
	jle	.L808
.L766:
	movq	8(%r12), %r13
	leaq	16(,%r14,8), %rcx
	addl	$1, 41104(%r13)
	movq	(%rbx), %rdx
	movq	41088(%r13), %r10
	movq	41096(%r13), %r15
	movq	-1(%rcx,%rdx), %rax
	testb	$1, %al
	je	.L755
	movq	-1(%rax), %rax
	cmpw	$64, 11(%rax)
	ja	.L755
	movq	8(%r12), %rsi
	movq	-1(%rcx,%rdx), %r8
	movq	41112(%rsi), %rdi
	testq	%rdi, %rdi
	je	.L756
	movq	%r8, %rsi
	movq	%r10, -144(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-144(%rbp), %r10
	movq	%rax, %rdx
.L757:
	movq	24(%r12), %rcx
	movq	8(%r12), %rdi
	xorl	%r8d, %r8d
	movq	%r10, -144(%rbp)
	movq	-88(%rbp), %rsi
	movq	%rdx, -152(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_115CheckMethodNameEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_4NameEEENS4_INS0_10JSFunctionEEENS0_14LookupIterator13ConfigurationE
	movq	-144(%rbp), %r10
	testb	%al, %al
	je	.L755
	movl	41104(%r13), %eax
	cmpq	$0, -96(%rbp)
	movq	41096(%r13), %rsi
	movq	-152(%rbp), %rdx
	leal	-1(%rax), %ecx
	jne	.L809
	movq	(%rdx), %r8
	movq	%r10, 41088(%r13)
	movl	%ecx, 41104(%r13)
	cmpq	%rsi, %r15
	je	.L762
	movq	%r15, 41096(%r13)
	movq	%r13, %rdi
	movq	%r8, -96(%rbp)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %r8
.L762:
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L763
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -96(%rbp)
	jmp	.L780
.L732:
	movq	41088(%r13), %r15
	cmpq	41096(%r13), %r15
	je	.L810
.L734:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r15)
	jmp	.L727
.L773:
	movq	41088(%r9), %rax
	cmpq	41096(%r9), %rax
	je	.L811
.L775:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r9)
	movq	%r12, (%rax)
	jmp	.L716
.L756:
	movq	41088(%rsi), %rdx
	cmpq	41096(%rsi), %rdx
	je	.L812
.L758:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%rsi)
	movq	%r8, (%rdx)
	jmp	.L757
.L808:
	movq	%r12, %rbx
.L754:
	movq	-88(%rbp), %rax
	movq	(%rax), %rax
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	je	.L795
	movq	-1(%rax), %rax
	movq	23(%rax), %r13
	movq	-128(%rbp), %rax
	movq	41112(%rax), %rdi
	movq	104(%rax), %r14
	testq	%rdi, %rdi
	je	.L813
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -88(%rbp)
.L768:
	cmpq	%r14, %r13
	jne	.L770
	jmp	.L795
	.p2align 4,,10
	.p2align 3
.L763:
	movq	41088(%r13), %rax
	movq	%rax, -96(%rbp)
	cmpq	41096(%r13), %rax
	je	.L814
.L765:
	movq	-96(%rbp), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r13)
	movq	%r8, (%rdx)
	jmp	.L780
.L806:
	movq	%r13, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L741
.L807:
	movq	%r13, %r14
	movq	-136(%rbp), %rdi
	andq	$-262144, %r14
	movq	24(%r14), %rax
	movq	-25128(%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal7Context13global_objectEv@PLT
	movq	24(%r14), %rdx
	movq	-1(%r13), %rcx
	cmpw	$1024, 11(%rcx)
	je	.L815
	movq	-1(%r13), %rdx
	movq	23(%rdx), %rdx
.L752:
	cmpq	%rdx, %rax
	setne	%al
	jmp	.L753
.L810:
	movq	%r13, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L734
.L811:
	movq	%r9, %rdi
	movq	%r9, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %r9
	jmp	.L775
.L813:
	movq	41088(%rax), %rdx
	movq	%rdx, -88(%rbp)
	cmpq	41096(%rax), %rdx
	je	.L816
.L769:
	movq	-88(%rbp), %rdx
	movq	-128(%rbp), %rcx
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%rcx)
	movq	%r13, (%rdx)
	jmp	.L768
.L812:
	movq	%rsi, %rdi
	movq	%r10, -160(%rbp)
	movq	%r8, -152(%rbp)
	movq	%rsi, -144(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-160(%rbp), %r10
	movq	-152(%rbp), %r8
	movq	-144(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L758
.L814:
	movq	%r13, %rdi
	movq	%r8, -144(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-144(%rbp), %r8
	movq	%rax, -96(%rbp)
	jmp	.L765
.L809:
	movq	8(%r12), %rax
	movq	-112(%rbp), %r9
	movq	%r10, 41088(%r13)
	movl	%ecx, 41104(%r13)
	addq	$104, %rax
	cmpq	%rsi, %r15
	je	.L760
	movq	%r15, 41096(%r13)
	movq	%r13, %rdi
	movq	%rax, -96(%rbp)
	movq	%r9, -88(%rbp)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rax
	movq	-88(%rbp), %r9
.L760:
	movl	41104(%r9), %edx
	movq	41096(%r9), %rcx
	subl	$1, %edx
	jmp	.L761
.L800:
	call	__stack_chk_fail@PLT
.L816:
	movq	%rax, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, -88(%rbp)
	jmp	.L769
.L815:
	movq	-37488(%rdx), %rdx
	jmp	.L752
	.cfi_endproc
.LFE20665:
	.size	_ZN2v88internal12JSStackFrame13GetMethodNameEv, .-_ZN2v88internal12JSStackFrame13GetMethodNameEv
	.section	.text._ZN2v88internal15MessageLocationC2ENS0_6HandleINS0_6ScriptEEEii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15MessageLocationC2ENS0_6HandleINS0_6ScriptEEEii
	.type	_ZN2v88internal15MessageLocationC2ENS0_6HandleINS0_6ScriptEEEii, @function
_ZN2v88internal15MessageLocationC2ENS0_6HandleINS0_6ScriptEEEii:
.LFB20602:
	.cfi_startproc
	endbr64
	movq	%rsi, (%rdi)
	movl	%edx, 8(%rdi)
	movl	%ecx, 12(%rdi)
	movl	$-1, 16(%rdi)
	movq	$0, 24(%rdi)
	ret
	.cfi_endproc
.LFE20602:
	.size	_ZN2v88internal15MessageLocationC2ENS0_6HandleINS0_6ScriptEEEii, .-_ZN2v88internal15MessageLocationC2ENS0_6HandleINS0_6ScriptEEEii
	.globl	_ZN2v88internal15MessageLocationC1ENS0_6HandleINS0_6ScriptEEEii
	.set	_ZN2v88internal15MessageLocationC1ENS0_6HandleINS0_6ScriptEEEii,_ZN2v88internal15MessageLocationC2ENS0_6HandleINS0_6ScriptEEEii
	.section	.text._ZN2v88internal15MessageLocationC2ENS0_6HandleINS0_6ScriptEEEiiNS2_INS0_18SharedFunctionInfoEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15MessageLocationC2ENS0_6HandleINS0_6ScriptEEEiiNS2_INS0_18SharedFunctionInfoEEE
	.type	_ZN2v88internal15MessageLocationC2ENS0_6HandleINS0_6ScriptEEEiiNS2_INS0_18SharedFunctionInfoEEE, @function
_ZN2v88internal15MessageLocationC2ENS0_6HandleINS0_6ScriptEEEiiNS2_INS0_18SharedFunctionInfoEEE:
.LFB20605:
	.cfi_startproc
	endbr64
	movq	%rsi, (%rdi)
	movl	%edx, 8(%rdi)
	movl	%ecx, 12(%rdi)
	movl	$-1, 16(%rdi)
	movq	%r8, 24(%rdi)
	ret
	.cfi_endproc
.LFE20605:
	.size	_ZN2v88internal15MessageLocationC2ENS0_6HandleINS0_6ScriptEEEiiNS2_INS0_18SharedFunctionInfoEEE, .-_ZN2v88internal15MessageLocationC2ENS0_6HandleINS0_6ScriptEEEiiNS2_INS0_18SharedFunctionInfoEEE
	.globl	_ZN2v88internal15MessageLocationC1ENS0_6HandleINS0_6ScriptEEEiiNS2_INS0_18SharedFunctionInfoEEE
	.set	_ZN2v88internal15MessageLocationC1ENS0_6HandleINS0_6ScriptEEEiiNS2_INS0_18SharedFunctionInfoEEE,_ZN2v88internal15MessageLocationC2ENS0_6HandleINS0_6ScriptEEEiiNS2_INS0_18SharedFunctionInfoEEE
	.section	.text._ZN2v88internal15MessageLocationC2ENS0_6HandleINS0_6ScriptEEENS2_INS0_18SharedFunctionInfoEEEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15MessageLocationC2ENS0_6HandleINS0_6ScriptEEENS2_INS0_18SharedFunctionInfoEEEi
	.type	_ZN2v88internal15MessageLocationC2ENS0_6HandleINS0_6ScriptEEENS2_INS0_18SharedFunctionInfoEEEi, @function
_ZN2v88internal15MessageLocationC2ENS0_6HandleINS0_6ScriptEEENS2_INS0_18SharedFunctionInfoEEEi:
.LFB20608:
	.cfi_startproc
	endbr64
	movq	%rsi, (%rdi)
	movq	$-1, 8(%rdi)
	movl	%ecx, 16(%rdi)
	movq	%rdx, 24(%rdi)
	ret
	.cfi_endproc
.LFE20608:
	.size	_ZN2v88internal15MessageLocationC2ENS0_6HandleINS0_6ScriptEEENS2_INS0_18SharedFunctionInfoEEEi, .-_ZN2v88internal15MessageLocationC2ENS0_6HandleINS0_6ScriptEEENS2_INS0_18SharedFunctionInfoEEEi
	.globl	_ZN2v88internal15MessageLocationC1ENS0_6HandleINS0_6ScriptEEENS2_INS0_18SharedFunctionInfoEEEi
	.set	_ZN2v88internal15MessageLocationC1ENS0_6HandleINS0_6ScriptEEENS2_INS0_18SharedFunctionInfoEEEi,_ZN2v88internal15MessageLocationC2ENS0_6HandleINS0_6ScriptEEENS2_INS0_18SharedFunctionInfoEEEi
	.section	.text._ZN2v88internal15MessageLocationC2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15MessageLocationC2Ev
	.type	_ZN2v88internal15MessageLocationC2Ev, @function
_ZN2v88internal15MessageLocationC2Ev:
.LFB20611:
	.cfi_startproc
	endbr64
	movq	$0, (%rdi)
	movq	$-1, 8(%rdi)
	movl	$-1, 16(%rdi)
	movq	$0, 24(%rdi)
	ret
	.cfi_endproc
.LFE20611:
	.size	_ZN2v88internal15MessageLocationC2Ev, .-_ZN2v88internal15MessageLocationC2Ev
	.globl	_ZN2v88internal15MessageLocationC1Ev
	.set	_ZN2v88internal15MessageLocationC1Ev,_ZN2v88internal15MessageLocationC2Ev
	.section	.text._ZN2v88internal14MessageHandler17MakeMessageObjectEPNS0_7IsolateENS0_15MessageTemplateEPKNS0_15MessageLocationENS0_6HandleINS0_6ObjectEEENS8_INS0_10FixedArrayEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MessageHandler17MakeMessageObjectEPNS0_7IsolateENS0_15MessageTemplateEPKNS0_15MessageLocationENS0_6HandleINS0_6ObjectEEENS8_INS0_10FixedArrayEEE
	.type	_ZN2v88internal14MessageHandler17MakeMessageObjectEPNS0_7IsolateENS0_15MessageTemplateEPKNS0_15MessageLocationENS0_6HandleINS0_6ObjectEEENS8_INS0_10FixedArrayEEE, @function
_ZN2v88internal14MessageHandler17MakeMessageObjectEPNS0_7IsolateENS0_15MessageTemplateEPKNS0_15MessageLocationENS0_6HandleINS0_6ObjectEEENS8_INS0_10FixedArrayEEE:
.LFB20639:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r10
	movq	%r8, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	testq	%rdx, %rdx
	je	.L824
	movl	8(%rdx), %r11d
	movl	12(%rdx), %r8d
	movq	(%rdx), %rcx
	movl	16(%rdx), %ebx
	movq	24(%rdx), %r9
.L822:
	leaq	88(%rdi), %rdx
	testq	%rax, %rax
	cmove	%rdx, %rax
	subq	$8, %rsp
	movq	%r10, %rdx
	pushq	%rax
	pushq	%rcx
	movl	%r11d, %ecx
	pushq	%rbx
	call	_ZN2v88internal7Factory18NewJSMessageObjectENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEEiiNS3_INS0_18SharedFunctionInfoEEEiNS3_INS0_6ScriptEEES5_@PLT
	movq	-8(%rbp), %rbx
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L824:
	.cfi_restore_state
	leaq	4472(%rdi), %rcx
	xorl	%r9d, %r9d
	movl	$-1, %ebx
	movl	$-1, %r8d
	movl	$-1, %r11d
	jmp	.L822
	.cfi_endproc
.LFE20639:
	.size	_ZN2v88internal14MessageHandler17MakeMessageObjectEPNS0_7IsolateENS0_15MessageTemplateEPKNS0_15MessageLocationENS0_6HandleINS0_6ObjectEEENS8_INS0_10FixedArrayEEE, .-_ZN2v88internal14MessageHandler17MakeMessageObjectEPNS0_7IsolateENS0_15MessageTemplateEPKNS0_15MessageLocationENS0_6HandleINS0_6ObjectEEENS8_INS0_10FixedArrayEEE
	.section	.text._ZNK2v88internal14StackFrameBase11GetScriptIdEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal14StackFrameBase11GetScriptIdEv
	.type	_ZNK2v88internal14StackFrameBase11GetScriptIdEv, @function
_ZNK2v88internal14StackFrameBase11GetScriptIdEv:
.LFB20649:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rdi), %rax
	call	*184(%rax)
	testb	%al, %al
	je	.L828
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*192(%rax)
	movq	(%rax), %rax
	movl	67(%rax), %eax
.L826:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L828:
	.cfi_restore_state
	movl	$-1, %eax
	jmp	.L826
	.cfi_endproc
.LFE20649:
	.size	_ZNK2v88internal14StackFrameBase11GetScriptIdEv, .-_ZNK2v88internal14StackFrameBase11GetScriptIdEv
	.section	.text._ZN2v88internal12JSStackFrame14FromFrameArrayEPNS0_7IsolateENS0_6HandleINS0_10FrameArrayEEEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12JSStackFrame14FromFrameArrayEPNS0_7IsolateENS0_6HandleINS0_10FrameArrayEEEi
	.type	_ZN2v88internal12JSStackFrame14FromFrameArrayEPNS0_7IsolateENS0_6HandleINS0_10FrameArrayEEEi, @function
_ZN2v88internal12JSStackFrame14FromFrameArrayEPNS0_7IsolateENS0_6HandleINS0_10FrameArrayEEEi:
.LFB20651:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	(%rcx,%rcx,2), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leal	3(%rax,%rax), %r13d
	pushq	%r12
	sall	$3, %r13d
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	movslq	%r13d, %rdx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%rsi, 8(%rdi)
	movq	(%r14), %rax
	movq	-1(%rdx,%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L831
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L832:
	movq	%rax, 16(%rbx)
	leal	8(%r13), %eax
	movq	(%r14), %rdx
	cltq
	movq	-1(%rax,%rdx), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L834
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L835:
	movq	%rax, 24(%rbx)
	leal	16(%r13), %eax
	movq	(%r14), %rdx
	cltq
	movq	-1(%rax,%rdx), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L837
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L838:
	movq	%rax, 32(%rbx)
	leal	3(%r15,%r15,2), %eax
	movq	(%r14), %rdx
	sall	$4, %eax
	cltq
	movq	-1(%rax,%rdx), %rax
	sarq	$32, %rax
	cmpb	$0, 44(%rbx)
	movl	%eax, 40(%rbx)
	je	.L840
	movb	$0, 44(%rbx)
.L840:
	movq	(%r14), %rax
	addl	$32, %r13d
	movslq	%r13d, %r13
	movq	-1(%r13,%rax), %rdx
	movq	%rdx, %rax
	movq	%rdx, %rsi
	movq	%rdx, %rdi
	shrq	$35, %rdx
	shrq	$36, %rax
	shrq	$38, %rsi
	andl	$1, %edx
	shrq	$39, %rdi
	andl	$1, %eax
	movq	%rsi, %rcx
	sall	$3, %edx
	andl	$1, %ecx
	addl	%eax, %eax
	orl	%ecx, %eax
	movq	%rdi, %rcx
	andl	$1, %ecx
	sall	$2, %ecx
	orl	%ecx, %eax
	orl	%edx, %eax
	movzbl	52(%rbx), %edx
	andl	$15, %eax
	andl	$-16, %edx
	orl	%edx, %eax
	movb	%al, 52(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L831:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L842
.L833:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L832
	.p2align 4,,10
	.p2align 3
.L837:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L843
.L839:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L838
	.p2align 4,,10
	.p2align 3
.L834:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L844
.L836:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L835
	.p2align 4,,10
	.p2align 3
.L842:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L833
	.p2align 4,,10
	.p2align 3
.L844:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L836
	.p2align 4,,10
	.p2align 3
.L843:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L839
	.cfi_endproc
.LFE20651:
	.size	_ZN2v88internal12JSStackFrame14FromFrameArrayEPNS0_7IsolateENS0_6HandleINS0_10FrameArrayEEEi, .-_ZN2v88internal12JSStackFrame14FromFrameArrayEPNS0_7IsolateENS0_6HandleINS0_10FrameArrayEEEi
	.section	.text._ZN2v88internal12JSStackFrameC2EPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_10JSFunctionEEENS4_INS0_12AbstractCodeEEEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12JSStackFrameC2EPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_10JSFunctionEEENS4_INS0_12AbstractCodeEEEi
	.type	_ZN2v88internal12JSStackFrameC2EPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_10JSFunctionEEENS4_INS0_12AbstractCodeEEEi, @function
_ZN2v88internal12JSStackFrameC2EPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_10JSFunctionEEENS4_INS0_12AbstractCodeEEEi:
.LFB20657:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal12JSStackFrameE(%rip), %rax
	andb	$-12, 52(%rdi)
	movq	%rsi, 8(%rdi)
	movq	%rax, (%rdi)
	movq	%rdx, 16(%rdi)
	movq	%rcx, 24(%rdi)
	movq	%r8, 32(%rdi)
	movl	%r9d, 40(%rdi)
	movb	$0, 44(%rdi)
	movb	$0, 48(%rdi)
	ret
	.cfi_endproc
.LFE20657:
	.size	_ZN2v88internal12JSStackFrameC2EPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_10JSFunctionEEENS4_INS0_12AbstractCodeEEEi, .-_ZN2v88internal12JSStackFrameC2EPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_10JSFunctionEEENS4_INS0_12AbstractCodeEEEi
	.globl	_ZN2v88internal12JSStackFrameC1EPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_10JSFunctionEEENS4_INS0_12AbstractCodeEEEi
	.set	_ZN2v88internal12JSStackFrameC1EPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_10JSFunctionEEENS4_INS0_12AbstractCodeEEEi,_ZN2v88internal12JSStackFrameC2EPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_10JSFunctionEEENS4_INS0_12AbstractCodeEEEi
	.section	.text._ZN2v88internal14WasmStackFrame14FromFrameArrayEPNS0_7IsolateENS0_6HandleINS0_10FrameArrayEEEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14WasmStackFrame14FromFrameArrayEPNS0_7IsolateENS0_6HandleINS0_10FrameArrayEEEi
	.type	_ZN2v88internal14WasmStackFrame14FromFrameArrayEPNS0_7IsolateENS0_6HandleINS0_10FrameArrayEEEi, @function
_ZN2v88internal14WasmStackFrame14FromFrameArrayEPNS0_7IsolateENS0_6HandleINS0_10FrameArrayEEEi:
.LFB20676:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	(%rcx,%rcx,2), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leal	3(%rax,%rax), %r12d
	pushq	%rbx
	sall	$3, %r12d
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movslq	%r12d, %rdx
	subq	$24, %rsp
	movq	%rsi, 8(%rdi)
	movq	(%r14), %rax
	movq	-1(%rdx,%rax), %rsi
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L847
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L848:
	movq	%rax, 16(%rbx)
	leal	8(%r12), %eax
	movq	(%r14), %rdx
	cltq
	movq	-1(%rax,%rdx), %rax
	sarq	$32, %rax
	movl	%eax, 24(%rbx)
	leal	32(%r12), %eax
	movq	(%r14), %rdx
	cltq
	movq	-1(%rax,%rdx), %rax
	btq	$33, %rax
	jnc	.L850
	movq	$0, 32(%rbx)
.L851:
	leal	3(%r15,%r15,2), %eax
	movq	(%r14), %rdx
	sall	$4, %eax
	cltq
	movq	-1(%rax,%rdx), %rax
	sarq	$32, %rax
	movl	%eax, 40(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L847:
	.cfi_restore_state
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L853
.L849:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%rsi, (%rax)
	jmp	.L848
	.p2align 4,,10
	.p2align 3
.L850:
	addl	$16, %r12d
	movslq	%r12d, %r12
	movq	-1(%r12,%rdx), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	%rax, 32(%rbx)
	jmp	.L851
	.p2align 4,,10
	.p2align 3
.L853:
	movq	%r13, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L849
	.cfi_endproc
.LFE20676:
	.size	_ZN2v88internal14WasmStackFrame14FromFrameArrayEPNS0_7IsolateENS0_6HandleINS0_10FrameArrayEEEi, .-_ZN2v88internal14WasmStackFrame14FromFrameArrayEPNS0_7IsolateENS0_6HandleINS0_10FrameArrayEEEi
	.section	.text._ZNK2v88internal14WasmStackFrame15GetModuleOffsetEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal14WasmStackFrame15GetModuleOffsetEv
	.type	_ZNK2v88internal14WasmStackFrame15GetModuleOffsetEv, @function
_ZNK2v88internal14WasmStackFrame15GetModuleOffsetEv:
.LFB20684:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -32
	movl	24(%rdi), %esi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	leaq	-32(%rbp), %rdi
	movq	(%rax), %rax
	movq	135(%rax), %rax
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal16WasmModuleObject17GetFunctionOffsetEj@PLT
	movq	%r12, %rdi
	movl	%eax, %ebx
	movq	(%r12), %rax
	call	*96(%rax)
	addl	%ebx, %eax
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L857
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L857:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20684:
	.size	_ZNK2v88internal14WasmStackFrame15GetModuleOffsetEv, .-_ZNK2v88internal14WasmStackFrame15GetModuleOffsetEv
	.section	.text._ZNK2v88internal14WasmStackFrame4NullEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal14WasmStackFrame4NullEv
	.type	_ZNK2v88internal14WasmStackFrame4NullEv, @function
_ZNK2v88internal14WasmStackFrame4NullEv:
.LFB20685:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	addq	$104, %rax
	ret
	.cfi_endproc
.LFE20685:
	.size	_ZNK2v88internal14WasmStackFrame4NullEv, .-_ZNK2v88internal14WasmStackFrame4NullEv
	.section	.text._ZN2v88internal19AsmJsWasmStackFrame14FromFrameArrayEPNS0_7IsolateENS0_6HandleINS0_10FrameArrayEEEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19AsmJsWasmStackFrame14FromFrameArrayEPNS0_7IsolateENS0_6HandleINS0_10FrameArrayEEEi
	.type	_ZN2v88internal19AsmJsWasmStackFrame14FromFrameArrayEPNS0_7IsolateENS0_6HandleINS0_10FrameArrayEEEi, @function
_ZN2v88internal19AsmJsWasmStackFrame14FromFrameArrayEPNS0_7IsolateENS0_6HandleINS0_10FrameArrayEEEi:
.LFB20688:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%ecx, %ebx
	subq	$8, %rsp
	call	_ZN2v88internal14WasmStackFrame14FromFrameArrayEPNS0_7IsolateENS0_6HandleINS0_10FrameArrayEEEi
	leal	(%rbx,%rbx,2), %eax
	movq	0(%r13), %rdx
	leal	7(%rax,%rax), %eax
	sall	$3, %eax
	cltq
	movq	-1(%rax,%rdx), %rax
	shrq	$37, %rax
	andl	$1, %eax
	movb	%al, 44(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20688:
	.size	_ZN2v88internal19AsmJsWasmStackFrame14FromFrameArrayEPNS0_7IsolateENS0_6HandleINS0_10FrameArrayEEEi, .-_ZN2v88internal19AsmJsWasmStackFrame14FromFrameArrayEPNS0_7IsolateENS0_6HandleINS0_10FrameArrayEEEi
	.section	.text._ZN2v88internal18FrameArrayIteratorC2EPNS0_7IsolateENS0_6HandleINS0_10FrameArrayEEEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18FrameArrayIteratorC2EPNS0_7IsolateENS0_6HandleINS0_10FrameArrayEEEi
	.type	_ZN2v88internal18FrameArrayIteratorC2EPNS0_7IsolateENS0_6HandleINS0_10FrameArrayEEEi, @function
_ZN2v88internal18FrameArrayIteratorC2EPNS0_7IsolateENS0_6HandleINS0_10FrameArrayEEEi:
.LFB20725:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal14WasmStackFrameE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%rsi, (%rdi)
	movq	%rax, 24(%rdi)
	leaq	16+_ZTVN2v88internal19AsmJsWasmStackFrameE(%rip), %rax
	movq	%rax, 72(%rdi)
	leaq	16+_ZTVN2v88internal12JSStackFrameE(%rip), %rax
	movq	%rdx, 8(%rdi)
	movl	%ecx, 16(%rdi)
	movq	$0, 40(%rdi)
	movq	$0, 88(%rdi)
	movq	%rax, 120(%rdi)
	movq	$0, 152(%rdi)
	movb	$0, 164(%rdi)
	movb	$0, 168(%rdi)
	movups	%xmm0, 136(%rdi)
	ret
	.cfi_endproc
.LFE20725:
	.size	_ZN2v88internal18FrameArrayIteratorC2EPNS0_7IsolateENS0_6HandleINS0_10FrameArrayEEEi, .-_ZN2v88internal18FrameArrayIteratorC2EPNS0_7IsolateENS0_6HandleINS0_10FrameArrayEEEi
	.globl	_ZN2v88internal18FrameArrayIteratorC1EPNS0_7IsolateENS0_6HandleINS0_10FrameArrayEEEi
	.set	_ZN2v88internal18FrameArrayIteratorC1EPNS0_7IsolateENS0_6HandleINS0_10FrameArrayEEEi,_ZN2v88internal18FrameArrayIteratorC2EPNS0_7IsolateENS0_6HandleINS0_10FrameArrayEEEi
	.section	.text._ZNK2v88internal18FrameArrayIterator8HasFrameEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal18FrameArrayIterator8HasFrameEv
	.type	_ZNK2v88internal18FrameArrayIterator8HasFrameEv, @function
_ZNK2v88internal18FrameArrayIterator8HasFrameEv:
.LFB20727:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	(%rax), %rax
	movq	15(%rax), %rax
	sarq	$32, %rax
	cmpl	%eax, 16(%rdi)
	setl	%al
	ret
	.cfi_endproc
.LFE20727:
	.size	_ZNK2v88internal18FrameArrayIterator8HasFrameEv, .-_ZNK2v88internal18FrameArrayIterator8HasFrameEv
	.section	.text._ZN2v88internal18FrameArrayIterator7AdvanceEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18FrameArrayIterator7AdvanceEv
	.type	_ZN2v88internal18FrameArrayIterator7AdvanceEv, @function
_ZN2v88internal18FrameArrayIterator7AdvanceEv:
.LFB20728:
	.cfi_startproc
	endbr64
	addl	$1, 16(%rdi)
	ret
	.cfi_endproc
.LFE20728:
	.size	_ZN2v88internal18FrameArrayIterator7AdvanceEv, .-_ZN2v88internal18FrameArrayIterator7AdvanceEv
	.section	.rodata._ZN2v88internal18FrameArrayIterator5FrameEv.str1.1,"aMS",@progbits,1
.LC10:
	.string	"unreachable code"
	.section	.text._ZN2v88internal18FrameArrayIterator5FrameEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18FrameArrayIterator5FrameEv
	.type	_ZN2v88internal18FrameArrayIterator5FrameEv, @function
_ZN2v88internal18FrameArrayIterator5FrameEv:
.LFB20729:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	16(%rdi), %ecx
	movq	%rdi, %rbx
	movq	8(%rdi), %r13
	leal	(%rcx,%rcx,2), %eax
	leal	7(%rax,%rax), %r12d
	movq	0(%r13), %rax
	sall	$3, %r12d
	movslq	%r12d, %r12
	movq	-1(%r12,%rax), %rax
	sarq	$32, %rax
	andl	$7, %eax
	cmpl	$2, %eax
	jg	.L865
	testl	%eax, %eax
	jne	.L874
	leaq	120(%rdi), %r14
	movq	(%rdi), %rsi
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal12JSStackFrame14FromFrameArrayEPNS0_7IsolateENS0_6HandleINS0_10FrameArrayEEEi
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L865:
	.cfi_restore_state
	cmpl	$4, %eax
	jne	.L875
	leaq	72(%rdi), %r14
	movq	(%rdi), %rsi
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal14WasmStackFrame14FromFrameArrayEPNS0_7IsolateENS0_6HandleINS0_10FrameArrayEEEi
	movq	0(%r13), %rax
	movq	-1(%r12,%rax), %rax
	shrq	$37, %rax
	andl	$1, %eax
	movb	%al, 116(%rbx)
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L874:
	.cfi_restore_state
	leaq	24(%rdi), %r14
	movq	(%rdi), %rsi
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal14WasmStackFrame14FromFrameArrayEPNS0_7IsolateENS0_6HandleINS0_10FrameArrayEEEi
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L875:
	.cfi_restore_state
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20729:
	.size	_ZN2v88internal18FrameArrayIterator5FrameEv, .-_ZN2v88internal18FrameArrayIterator5FrameEv
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.1,"aMS",@progbits,1
.LC11:
	.string	""
.LC12:
	.string	"Debugger: %"
.LC13:
	.string	"Error loading debugger"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.8,"aMS",@progbits,1
	.align 8
.LC14:
	.string	"Internal % error. Default options are missing."
	.align 8
.LC15:
	.string	"Private fields can not be deleted"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.1
.LC16:
	.string	"Uncaught %"
.LC17:
	.string	"Not supported"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.8
	.align 8
.LC18:
	.string	"Internal error, wrong service type: %"
	.align 8
.LC19:
	.string	"Internal error. Wrong value type."
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.1
.LC20:
	.string	"Internal error. Icu error."
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.8
	.align 8
.LC21:
	.string	"Function.prototype.apply was called on %, which is a % and not a function"
	.align 8
.LC22:
	.string	"'arguments' is not allowed in class field initializer"
	.align 8
.LC23:
	.string	"Derived ArrayBuffer constructor created a buffer which was too small"
	.align 8
.LC24:
	.string	"ArrayBuffer subclass returned this from species constructor"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.1
.LC25:
	.string	"array %[%] is not type %"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.8
	.align 8
.LC26:
	.string	"await is only valid in async function"
	.align 8
.LC27:
	.string	"Atomics.wait cannot be called in this context"
	.align 8
.LC28:
	.string	"The comparison function must be either a function or undefined"
	.align 8
.LC29:
	.string	"The number % cannot be converted to a BigInt because it is not an integer"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.1
.LC30:
	.string	"Cannot convert % to a BigInt"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.8
	.align 8
.LC31:
	.string	"Cannot mix BigInt and other types, use explicit conversions"
	.align 8
.LC32:
	.string	"Do not know how to serialize a BigInt"
	.align 8
.LC33:
	.string	"BigInts have no unsigned right shift, use >> instead"
	.align 8
.LC34:
	.string	"Cannot convert a BigInt value to a number"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.1
.LC35:
	.string	"% is not a function"
.LC36:
	.string	"% called on non-object"
.LC37:
	.string	"% called on null or undefined"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.8
	.align 8
.LC38:
	.string	"CallSite expects wasm object as first or function as second argument, got <%, %>"
	.align 8
.LC39:
	.string	"CallSite method % expects CallSite as receiver"
	.align 8
.LC40:
	.string	"Cannot convert object to primitive value"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.1
.LC41:
	.string	"Cannot prevent extensions"
.LC42:
	.string	"Cannot freeze"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.8
	.align 8
.LC43:
	.string	"Cannot freeze array buffer views with elements"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.1
.LC44:
	.string	"Cannot seal"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.8
	.align 8
.LC45:
	.string	"Converting circular structure to JSON%"
	.align 8
.LC46:
	.string	"Abstract class % not directly constructable"
	.align 8
.LC47:
	.string	"Assignment to constant variable."
	.align 8
.LC48:
	.string	"Classes may not have a field named 'constructor'"
	.align 8
.LC49:
	.string	"Class constructor % cannot be invoked without 'new'"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.1
.LC50:
	.string	"Constructor % requires 'new'"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.8
	.align 8
.LC51:
	.string	"The .constructor property is not an object"
	.align 8
.LC52:
	.string	"Currency code is required with currency style."
	.align 8
.LC53:
	.string	"Detected cycle while resolving name '%' in '%'"
	.align 8
.LC54:
	.string	"First argument to DataView constructor must be an ArrayBuffer"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.1
.LC55:
	.string	"this is not a Date object."
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.8
	.align 8
.LC56:
	.string	"Debugger: Invalid frame index."
	.align 8
.LC57:
	.string	"Debugger: Parameters have wrong types."
	.align 8
.LC58:
	.string	"Missing initializer in % declaration"
	.align 8
.LC59:
	.string	"Cannot define property %, object is not extensible"
	.align 8
.LC60:
	.string	"Cannot perform % on a neutered ArrayBuffer"
	.align 8
.LC61:
	.string	"Object template has duplicate property '%'"
	.align 8
.LC62:
	.string	"Class extends value % is not a constructor or null"
	.align 8
.LC63:
	.string	"First argument to % must not be a regular expression"
	.align 8
.LC64:
	.string	"Bind must be called on a function"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.1
.LC65:
	.string	"Generator is already running"
.LC66:
	.string	"Illegal invocation"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.8
	.align 8
.LC67:
	.string	"Immutable prototype object '%' cannot have their prototype set"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.1
.LC68:
	.string	"Cannot use new with import"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.8
	.align 8
.LC69:
	.string	"Cannot use import statement outside a module"
	.align 8
.LC70:
	.string	"Cannot use 'import.meta' outside a module"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.1
.LC71:
	.string	"import() requires a specifier"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.8
	.align 8
.LC72:
	.string	"Method % called on incompatible receiver %"
	.align 8
.LC73:
	.string	"Function has non-object prototype '%' in instanceof check"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.1
.LC74:
	.string	"invalid_argument"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.8
	.align 8
.LC75:
	.string	"Cannot use 'in' operator to search for '%' in %"
	.align 8
.LC76:
	.string	"RegExp exec method returned something other than an Object or null"
	.align 8
.LC77:
	.string	"Invalid unit argument for %() '%'"
	.align 8
.LC78:
	.string	"Iterator result % is not an object"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.1
.LC79:
	.string	"Found non-callable @@iterator"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.8
	.align 8
.LC80:
	.string	"Iterator value % is not an entry object"
	.align 8
.LC81:
	.string	"Language ID should be string or object."
	.align 8
.LC82:
	.string	"First argument to Intl.Locale constructor can't be empty or missing"
	.align 8
.LC83:
	.string	"Incorrect locale information provided"
	.align 8
.LC84:
	.string	"Incorrect ListFormat information provided"
	.align 8
.LC85:
	.string	"flatMap mapper function is not callable"
	.align 8
.LC86:
	.string	"Method % called on a non-object or on a wrong type of object."
	.align 8
.LC87:
	.string	"Method invoked on undefined or null value."
	.align 8
.LC88:
	.string	"Method invoked on an object that is not %."
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.1
.LC89:
	.string	"no access"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.8
	.align 8
.LC90:
	.string	"Right-hand side of 'instanceof' is not callable"
	.align 8
.LC91:
	.string	"Cannot destructure '%' as it is %."
	.align 8
.LC92:
	.string	"Cannot destructure property '%' of '%' as it is %."
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.1
.LC93:
	.string	"% is not extensible"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.8
	.align 8
.LC94:
	.string	"Right-hand side of 'instanceof' is not an object"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.1
.LC95:
	.string	"Cannot read property '%' of %"
.LC96:
	.string	"Cannot set property '%' of %"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.8
	.align 8
.LC97:
	.string	"Cannot set property % of % which has only a getter"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.1
.LC98:
	.string	"% is not an iterator"
.LC99:
	.string	"% is not a promise"
.LC100:
	.string	"% is not a constructor"
.LC101:
	.string	"% requires that 'this' be a %"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.8
	.align 8
.LC102:
	.string	"% is not a function or its return value is not iterable"
	.align 8
.LC103:
	.string	"% is not a function or its return value is not async iterable"
	.align 8
.LC104:
	.string	"Value need to be finite number for %()"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.1
.LC105:
	.string	"% is not iterable"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.8
	.align 8
.LC106:
	.string	"% is not iterable (cannot read property %)"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.1
.LC107:
	.string	"% is not async iterable"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.8
	.align 8
.LC108:
	.string	"% is not a valid property name"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.1
.LC109:
	.string	"this is not a typed array."
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.8
	.align 8
.LC110:
	.string	"Super constructor % of % is not a constructor"
	.align 8
.LC111:
	.string	"Super constructor % of anonymous class is not a constructor"
	.align 8
.LC112:
	.string	"% is not an integer shared typed array."
	.align 8
.LC113:
	.string	"% is not an int32 or BigInt64 shared typed array."
	.align 8
.LC114:
	.string	"Object.prototype.__defineGetter__: Expecting function"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.1
.LC115:
	.string	"Getter must be a function: %"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.8
	.align 8
.LC116:
	.string	"Cannot add property %, object is not extensible"
	.align 8
.LC117:
	.string	"Object.prototype.__defineSetter__: Expecting function"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.1
.LC118:
	.string	"Setter must be a function: %"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.8
	.align 8
.LC119:
	.string	"Function object that's not a constructor was created with new"
	.align 8
.LC120:
	.string	"Chaining cycle detected for promise %"
	.align 8
.LC121:
	.string	"Promise executor has already been invoked with non-undefined arguments"
	.align 8
.LC122:
	.string	"Promise resolve or reject function is not callable"
	.align 8
.LC123:
	.string	"Property description must be an object: %"
	.align 8
.LC124:
	.string	"'%' returned for property '%' of object '%' is not a function"
	.align 8
.LC125:
	.string	"Object prototype may only be an Object or null: %"
	.align 8
.LC126:
	.string	"Class extends value does not have valid prototype property %"
	.align 8
.LC127:
	.string	"'construct' on proxy: trap returned non-object ('%')"
	.align 8
.LC128:
	.string	"'defineProperty' on proxy: trap returned truish for defining non-configurable property '%' which is either non-existent or configurable in the proxy target"
	.align 8
.LC129:
	.string	"'defineProperty' on proxy: trap returned truish for defining non-configurable property '%' which cannot be non-writable, unless there exists a corresponding non-configurable, non-writable own property of the target object."
	.align 8
.LC130:
	.string	"'defineProperty' on proxy: trap returned truish for adding property '%'  to the non-extensible proxy target"
	.align 8
.LC131:
	.string	"'defineProperty' on proxy: trap returned truish for adding property '%'  that is incompatible with the existing property in the proxy target"
	.align 8
.LC132:
	.string	"'deleteProperty' on proxy: trap returned truish for property '%' which is non-configurable in the proxy target"
	.align 8
.LC133:
	.string	"'deleteProperty' on proxy: trap returned truish for property '%' but the proxy target is non-extensible"
	.align 8
.LC134:
	.string	"'get' on proxy: property '%' is a read-only and non-configurable data property on the proxy target but the proxy did not return its actual value (expected '%' but got '%')"
	.align 8
.LC135:
	.string	"'get' on proxy: property '%' is a non-configurable accessor property on the proxy target and does not have a getter function, but the trap did not return 'undefined' (got '%')"
	.align 8
.LC136:
	.string	"'getOwnPropertyDescriptor' on proxy: trap returned descriptor for property '%' that is incompatible with the existing property in the proxy target"
	.align 8
.LC137:
	.string	"'getOwnPropertyDescriptor' on proxy: trap returned neither object nor undefined for property '%'"
	.align 8
.LC138:
	.string	"'getOwnPropertyDescriptor' on proxy: trap reported non-configurability for property '%' which is either non-existent or configurable in the proxy target"
	.align 8
.LC139:
	.string	"'getOwnPropertyDescriptor' on proxy: trap reported non-configurable and writable for property '%' which is non-configurable, non-writable in the proxy target"
	.align 8
.LC140:
	.string	"'getOwnPropertyDescriptor' on proxy: trap returned undefined for property '%' which exists in the non-extensible proxy target"
	.align 8
.LC141:
	.string	"'getOwnPropertyDescriptor' on proxy: trap returned undefined for property '%' which is non-configurable in the proxy target"
	.align 8
.LC142:
	.string	"'getPrototypeOf' on proxy: trap returned neither object nor null"
	.align 8
.LC143:
	.string	"'getPrototypeOf' on proxy: proxy target is non-extensible but the trap did not return its actual prototype"
	.align 8
.LC144:
	.string	"Cannot create proxy with a revoked proxy as target or handler"
	.align 8
.LC145:
	.string	"'has' on proxy: trap returned falsish for property '%' which exists in the proxy target as non-configurable"
	.align 8
.LC146:
	.string	"'has' on proxy: trap returned falsish for property '%' but the proxy target is not extensible"
	.align 8
.LC147:
	.string	"'isExtensible' on proxy: trap result does not reflect extensibility of proxy target (which is '%')"
	.align 8
.LC148:
	.string	"Cannot create proxy with a non-object as target or handler"
	.align 8
.LC149:
	.string	"'ownKeys' on proxy: trap result did not include '%'"
	.align 8
.LC150:
	.string	"'ownKeys' on proxy: trap returned extra keys but proxy target is non-extensible"
	.align 8
.LC151:
	.string	"'ownKeys' on proxy: trap returned duplicate entries"
	.align 8
.LC152:
	.string	"'preventExtensions' on proxy: trap returned truish but the proxy target is extensible"
	.align 8
.LC153:
	.string	"Cannot pass private property name to proxy trap"
	.align 8
.LC154:
	.string	"Cannot perform '%' on a proxy that has been revoked"
	.align 8
.LC155:
	.string	"'set' on proxy: trap returned truish for property '%' which exists in the proxy target as a non-configurable and non-writable data property with a different value"
	.align 8
.LC156:
	.string	"'set' on proxy: trap returned truish for property '%' which exists in the proxy target as a non-configurable and non-writable accessor property without a setter"
	.align 8
.LC157:
	.string	"'setPrototypeOf' on proxy: trap returned truish for setting a new prototype on the non-extensible proxy target"
	.align 8
.LC158:
	.string	"'%' on proxy: trap returned falsish"
	.align 8
.LC159:
	.string	"'%' on proxy: trap returned falsish for property '%'"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.1
.LC160:
	.string	"Cannot redefine property: %"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.8
	.align 8
.LC161:
	.string	"Cannot redefine a property of an object with external array elements"
	.align 8
.LC162:
	.string	"Reduce of empty array with no initial value"
	.align 8
.LC163:
	.string	"Cannot supply flags when constructing one RegExp from another"
	.align 8
.LC164:
	.string	"% getter called on non-object %"
	.align 8
.LC165:
	.string	"% getter called on non-RegExp object"
	.align 8
.LC166:
	.string	"Incorrect RelativeDateTimeFormatter provided"
	.align 8
.LC167:
	.string	"Promise resolver % is not a function"
	.align 8
.LC168:
	.string	"The iterator's 'return' method is not callable"
	.align 8
.LC169:
	.string	"Derived SharedArrayBuffer constructor created a buffer which was too small"
	.align 8
.LC170:
	.string	"SharedArrayBuffer subclass returned this from species constructor"
	.align 8
.LC171:
	.string	"Classes may not have a static property named 'prototype'"
	.align 8
.LC172:
	.string	"Cannot delete property '%' of %"
	.align 8
.LC173:
	.string	"'caller', 'callee', and 'arguments' properties may not be accessed on strict mode functions or the arguments objects for calls to them"
	.align 8
.LC174:
	.string	"Cannot assign to read only property '%' of % '%'"
	.align 8
.LC175:
	.string	"Cannot create property '%' on % '%'"
	.align 8
.LC176:
	.string	"Result of the Symbol.iterator method is not an object"
	.align 8
.LC177:
	.string	"Result of the Symbol.asyncIterator method is not an object"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.1
.LC178:
	.string	"% is not a symbol"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.8
	.align 8
.LC179:
	.string	"Cannot convert a Symbol value to a number"
	.align 8
.LC180:
	.string	"Cannot convert a Symbol value to a string"
	.align 8
.LC181:
	.string	"The iterator does not provide a 'throw' method."
	.align 8
.LC182:
	.string	"Cannot convert undefined or null to object"
	.align 8
.LC183:
	.string	"Invalid property descriptor. Cannot both specify accessors and a value or writable attribute, %"
	.align 8
.LC184:
	.string	"Identifier '%' has already been declared"
	.align 8
.LC185:
	.string	"%: Arguments list has wrong type"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.1
.LC186:
	.string	"% is not defined"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.8
	.align 8
.LC187:
	.string	"Super constructor may only be called once"
	.align 8
.LC188:
	.string	"Cannot access '%' before initialization"
	.align 8
.LC189:
	.string	"Unsupported reference to 'super'"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.1
.LC190:
	.string	"Division by zero"
.LC191:
	.string	"Exponent must be positive"
.LC192:
	.string	"Maximum BigInt size exceeded"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.8
	.align 8
.LC193:
	.string	"Provided date is not in valid range."
	.align 8
.LC194:
	.string	"Expected letters optionally connected with underscores or hyphens for a location, got %"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.1
.LC195:
	.string	"Invalid array buffer length"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.8
	.align 8
.LC196:
	.string	"Array buffer allocation failed"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.1
.LC197:
	.string	"Invalid %s : %"
.LC198:
	.string	"Invalid array length"
.LC199:
	.string	"Invalid atomic access index"
.LC200:
	.string	"Invalid code point %"
.LC201:
	.string	"Invalid count value"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.8
	.align 8
.LC202:
	.string	"Offset is outside the bounds of the DataView"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.1
.LC203:
	.string	"Invalid DataView length %"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.8
	.align 8
.LC204:
	.string	"Start offset % is outside the bounds of the buffer"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.1
.LC205:
	.string	"Invalid hint: %"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.8
	.align 8
.LC206:
	.string	"Invalid value: not (convertible to) a safe integer"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.1
.LC207:
	.string	"Invalid language tag: %"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.8
	.align 8
.LC208:
	.string	"Invalid value used as weak map key"
	.align 8
.LC209:
	.string	"Invalid value used in weak set"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.1
.LC210:
	.string	"Invalid string length"
.LC211:
	.string	"Invalid time value"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.8
	.align 8
.LC212:
	.string	"Invalid time zone specified: %"
	.align 8
.LC213:
	.string	"% of % should be a multiple of %"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.1
.LC214:
	.string	"Invalid typed array index"
.LC215:
	.string	"Invalid typed array length: %"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.8
	.align 8
.LC216:
	.string	"let is disallowed as a lexically bound name"
	.align 8
.LC217:
	.string	"Illegal value for localeMatcher:%"
	.align 8
.LC218:
	.string	"The normalization form should be one of %."
	.align 8
.LC219:
	.string	"Paramenter % of function %() is % and out of range"
	.align 8
.LC220:
	.string	"Numeric separator can not be used after leading 0."
	.align 8
.LC221:
	.string	"% argument must be between 0 and 100"
	.align 8
.LC222:
	.string	"Numeric separators are not allowed at the end of numeric literals"
	.align 8
.LC223:
	.string	"Only one underscore is allowed as numeric separator"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.1
.LC224:
	.string	"% value is out of range."
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.8
	.align 8
.LC225:
	.string	"Maximum call stack size exceeded"
	.align 8
.LC226:
	.string	"toPrecision() argument must be between 1 and 100"
	.align 8
.LC227:
	.string	"toString() radix argument must be between 2 and 36"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.1
.LC228:
	.string	"offset is out of bounds"
.LC229:
	.string	"Source is too large"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.8
	.align 8
.LC230:
	.string	"Value % out of range for % options property %"
	.align 8
.LC231:
	.string	"The requested module '%' contains conflicting star exports for name '%'"
	.align 8
.LC232:
	.string	"Getter must not have any formal parameters."
	.align 8
.LC233:
	.string	"Setter must have exactly one formal parameter."
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.1
.LC234:
	.string	"Invalid BigInt string"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.8
	.align 8
.LC235:
	.string	"Class constructor may not be an accessor"
	.align 8
.LC236:
	.string	"Class constructor may not be a generator"
	.align 8
.LC237:
	.string	"Class constructor may not be an async method"
	.align 8
.LC238:
	.string	"Class constructor may not be a private method"
	.align 8
.LC239:
	.string	"Derived constructors may only return object or undefined"
	.align 8
.LC240:
	.string	"A class may only have one constructor"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.1
.LC241:
	.string	"Duplicate export of '%'"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.8
	.align 8
.LC242:
	.string	"Duplicate __proto__ fields are not allowed in object literals"
	.align 8
.LC243:
	.string	"% loop variable declaration may not have an initializer."
	.align 8
.LC244:
	.string	"The left-hand side of a for-of loop may not start with 'let'."
	.align 8
.LC245:
	.string	"Invalid left-hand side in % loop: Must have a single binding."
	.align 8
.LC246:
	.string	"Generators can only be declared at the top level or inside a block."
	.align 8
.LC247:
	.string	"Async functions can only be declared at the top level or inside a block."
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.1
.LC248:
	.string	"Illegal break statement"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.8
	.align 8
.LC249:
	.string	"Illegal continue statement: no surrounding iteration statement"
	.align 8
.LC250:
	.string	"Illegal continue statement: '%' does not denote an iteration statement"
	.align 8
.LC251:
	.string	"Illegal '%' directive in function with non-simple parameter list"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.1
.LC252:
	.string	"Illegal return statement"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.8
	.align 8
.LC253:
	.string	"Intrinsic calls do not support spread arguments"
	.align 8
.LC254:
	.string	"`...` must be followed by an identifier in declaration contexts"
	.align 8
.LC255:
	.string	"Illegal property in declaration context"
	.align 8
.LC256:
	.string	"`...` must be followed by an assignable reference in assignment contexts"
	.align 8
.LC257:
	.string	"Keyword must not contain escaped characters"
	.align 8
.LC258:
	.string	"'%' must not contain escaped characters"
	.align 8
.LC259:
	.string	"Invalid left-hand side in assignment"
	.align 8
.LC260:
	.string	"Invalid shorthand property initializer"
	.align 8
.LC261:
	.string	"Invalid destructuring assignment target"
	.align 8
.LC262:
	.string	"Invalid left-hand side in for-loop"
	.align 8
.LC263:
	.string	"Invalid left-hand side expression in postfix operation"
	.align 8
.LC264:
	.string	"Invalid left-hand side expression in prefix operation"
	.align 8
.LC265:
	.string	"Invalid flags supplied to RegExp constructor '%'"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.1
.LC266:
	.string	"Invalid or unexpected token"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.8
	.align 8
.LC267:
	.string	"Private field '%' must be declared in an enclosing class"
	.align 8
.LC268:
	.string	"Cannot read private member % from an object whose class did not declare it"
	.align 8
.LC269:
	.string	"Cannot write private member % to an object whose class did not declare it"
	.align 8
.LC270:
	.string	"Private method '%' is not writable"
	.align 8
.LC271:
	.string	"'%' was defined without a getter"
	.align 8
.LC272:
	.string	"'%' was defined without a setter"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.1
.LC273:
	.string	"Unexpected end of JSON input"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.8
	.align 8
.LC274:
	.string	"Unexpected token % in JSON at position %"
	.align 8
.LC275:
	.string	"Unexpected number in JSON at position %"
	.align 8
.LC276:
	.string	"Unexpected string in JSON at position %"
	.align 8
.LC277:
	.string	"Label '%' has already been declared"
	.align 8
.LC278:
	.string	"Labelled function declaration not allowed as the body of a control flow structure"
	.align 8
.LC279:
	.string	"Malformed arrow function parameter list"
	.align 8
.LC280:
	.string	"Invalid regular expression: /%/: %"
	.align 8
.LC281:
	.string	"Invalid regular expression flags"
	.align 8
.LC282:
	.string	"Export '%' is not defined in module"
	.align 8
.LC283:
	.string	"Function statements require a function name"
	.align 8
.LC284:
	.string	"HTML comments are not allowed in modules"
	.align 8
.LC285:
	.string	"More than one default clause in switch statement"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.1
.LC286:
	.string	"Illegal newline after throw"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.8
	.align 8
.LC287:
	.string	"Missing catch or finally after try"
	.align 8
.LC288:
	.string	"Rest parameter must be last formal parameter"
	.align 8
.LC289:
	.string	"Flattening % elements on an array-like of length % is disallowed, as the total surpasses 2**53-1"
	.align 8
.LC290:
	.string	"Pushing % elements on an array-like of length % is disallowed, as the total surpasses 2**53-1"
	.align 8
.LC291:
	.string	"Rest element must be last element"
	.align 8
.LC292:
	.string	"Setter function argument must not be a rest parameter"
	.align 8
.LC293:
	.string	"Duplicate parameter name not allowed in this context"
	.align 8
.LC294:
	.string	"Function arg string contains parenthesis"
	.align 8
.LC295:
	.string	"Arg string terminates parameters early"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.1
.LC296:
	.string	"Unexpected end of arg string"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.8
	.align 8
.LC297:
	.string	"Rest parameter may not have a default initializer"
	.align 8
.LC298:
	.string	"Runtime function given wrong number of arguments"
	.align 8
.LC299:
	.string	"Must call super constructor in derived class before accessing 'this' or returning from derived constructor"
	.align 8
.LC300:
	.string	"Single function literal required"
	.align 8
.LC301:
	.string	"In non-strict mode code, functions can only be declared at top level, inside a block, or as the body of an if statement."
	.align 8
.LC302:
	.string	"object.constructor[Symbol.species] is not a constructor"
	.align 8
.LC303:
	.string	"Delete of an unqualified identifier in strict mode."
	.align 8
.LC304:
	.string	"Unexpected eval or arguments in strict mode"
	.align 8
.LC305:
	.string	"In strict mode code, functions can only be declared at top level or inside a block."
	.align 8
.LC306:
	.string	"Octal literals are not allowed in strict mode."
	.align 8
.LC307:
	.string	"Decimals with leading zeros are not allowed in strict mode."
	.align 8
.LC308:
	.string	"Octal escape sequences are not allowed in strict mode."
	.align 8
.LC309:
	.string	"Strict mode code may not include a with statement"
	.align 8
.LC310:
	.string	"Octal escape sequences are not allowed in template strings."
	.align 8
.LC311:
	.string	"'this' is not a valid formal parameter name"
	.align 8
.LC312:
	.string	"'await' is not a valid identifier name in an async function"
	.align 8
.LC313:
	.string	"Illegal await-expression in formal parameters of async function"
	.align 8
.LC314:
	.string	"Too many arguments in function call (only 65535 allowed)"
	.align 8
.LC315:
	.string	"Too many parameters in function definition (only 65534 allowed)"
	.align 8
.LC316:
	.string	"Too many properties to enumerate"
	.align 8
.LC317:
	.string	"Literal containing too many nested spreads (up to 65534 allowed)"
	.align 8
.LC318:
	.string	"Too many variables declared (only 4194303 allowed)"
	.align 8
.LC319:
	.string	"Too many elements passed to Promise.all"
	.align 8
.LC320:
	.string	"Derived TypedArray constructor created an array which was too small"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.1
.LC321:
	.string	"Unexpected end of input"
.LC322:
	.string	"Unexpected private field"
.LC323:
	.string	"Unexpected reserved word"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.8
	.align 8
.LC324:
	.string	"Unexpected strict mode reserved word"
	.align 8
.LC325:
	.string	"'super' keyword unexpected here"
	.align 8
.LC326:
	.string	"new.target expression is not allowed here"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.1
.LC327:
	.string	"Unexpected template string"
.LC328:
	.string	"Unexpected token '%'"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.8
	.align 8
.LC329:
	.string	"Unary operator used immediately before exponentiation expression. Parenthesis must be used to disambiguate operator precedence"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.1
.LC330:
	.string	"Unexpected identifier"
.LC331:
	.string	"Unexpected number"
.LC332:
	.string	"Unexpected string"
.LC333:
	.string	"Unexpected regular expression"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.8
	.align 8
.LC334:
	.string	"Lexical declaration cannot appear in a single-statement context"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.1
.LC335:
	.string	"Undefined label '%'"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.8
	.align 8
.LC336:
	.string	"The requested module '%' does not provide an export named '%'"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.1
.LC337:
	.string	"missing ) after argument list"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.8
	.align 8
.LC338:
	.string	"Invalid regular expression: missing /"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.1
.LC339:
	.string	"Unterminated template literal"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.8
	.align 8
.LC340:
	.string	"Missing } in template expression"
	.align 8
.LC341:
	.string	"Found non-callable @@hasInstance"
	.align 8
.LC342:
	.string	"Invalid hexadecimal escape sequence"
	.align 8
.LC343:
	.string	"Invalid Unicode escape sequence"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.1
.LC344:
	.string	"Undefined Unicode code-point"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.8
	.align 8
.LC345:
	.string	"Yield expression not allowed in formal parameter"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.1
.LC346:
	.string	"%"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.8
	.align 8
.LC347:
	.string	"Possible side-effect in debug-evaluate"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.1
.LC348:
	.string	"URI malformed"
.LC349:
	.string	"unreachable"
.LC350:
	.string	"memory access out of bounds"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.8
	.align 8
.LC351:
	.string	"operation does not support unaligned accesses"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.1
.LC352:
	.string	"divide by zero"
.LC353:
	.string	"divide result unrepresentable"
.LC354:
	.string	"remainder by zero"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.8
	.align 8
.LC355:
	.string	"float unrepresentable in integer range"
	.align 8
.LC356:
	.string	"invalid index into function table"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.1
.LC357:
	.string	"function signature mismatch"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.8
	.align 8
.LC358:
	.string	"wasm function signature contains illegal type"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.1
.LC359:
	.string	"data segment has been dropped"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.8
	.align 8
.LC360:
	.string	"element segment has been dropped"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.1
.LC361:
	.string	"table access out of bounds"
.LC362:
	.string	"wasm exception"
.LC363:
	.string	"Invalid asm.js: %"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.8
	.align 8
.LC364:
	.string	"Converted asm.js to WebAssembly: %"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.1
.LC365:
	.string	"Instantiated asm.js: %"
.LC366:
	.string	"Linking failure in asm.js: %"
.LC367:
	.string	"% could not be cloned."
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.8
	.align 8
.LC368:
	.string	"Data cannot be cloned, out of memory."
	.align 8
.LC369:
	.string	"An ArrayBuffer is neutered and could not be cloned."
	.align 8
.LC370:
	.string	"A SharedArrayBuffer could not be cloned. SharedArrayBuffer must not be transferred."
	.align 8
.LC371:
	.string	"Unable to deserialize cloned data."
	.align 8
.LC372:
	.string	"Unable to deserialize cloned data due to invalid or unsupported version."
	.align 8
.LC373:
	.string	"Trace event category must be a string."
	.align 8
.LC374:
	.string	"Trace event name must be a string."
	.align 8
.LC375:
	.string	"Trace event name must not be an empty string."
	.align 8
.LC376:
	.string	"Trace event phase must be a number."
	.align 8
.LC377:
	.string	"Trace event id must be a number."
	.align 8
.LC378:
	.string	"unregisterToken ('%') must be an object"
	.align 8
.LC379:
	.string	"FinalizationGroup: cleanup must be callable"
	.align 8
.LC380:
	.string	"FinalizationGroup.prototype.register: target must be an object"
	.align 8
.LC381:
	.string	"FinalizationGroup.prototype.register: target and holdings must not be same"
	.align 8
.LC382:
	.string	"WeakRef: target must be an object"
	.align 8
.LC383:
	.string	"Invalid optional chain from new expression"
	.align 8
.LC384:
	.string	"Invalid optional chain from super property"
	.align 8
.LC385:
	.string	"Invalid tagged template on optional chain"
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE.str1.1
.LC386:
	.string	"Cyclic __proto__ value"
	.section	.text._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE
	.type	_ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE, @function
_ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE:
.LFB20749:
	.cfi_startproc
	endbr64
	cmpl	$376, %edi
	ja	.L877
	leaq	.L879(%rip), %rdx
	movl	%edi, %edi
	movslq	(%rdx,%rdi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE,"a",@progbits
	.align 4
	.align 4
.L879:
	.long	.L1256-.L879
	.long	.L1254-.L879
	.long	.L1253-.L879
	.long	.L1252-.L879
	.long	.L1251-.L879
	.long	.L1250-.L879
	.long	.L1249-.L879
	.long	.L1248-.L879
	.long	.L1247-.L879
	.long	.L1246-.L879
	.long	.L1245-.L879
	.long	.L1244-.L879
	.long	.L1243-.L879
	.long	.L1242-.L879
	.long	.L1241-.L879
	.long	.L1240-.L879
	.long	.L1239-.L879
	.long	.L1238-.L879
	.long	.L1237-.L879
	.long	.L1236-.L879
	.long	.L1235-.L879
	.long	.L1234-.L879
	.long	.L1233-.L879
	.long	.L1232-.L879
	.long	.L1231-.L879
	.long	.L1230-.L879
	.long	.L1229-.L879
	.long	.L1228-.L879
	.long	.L1227-.L879
	.long	.L1226-.L879
	.long	.L1225-.L879
	.long	.L1224-.L879
	.long	.L1223-.L879
	.long	.L1222-.L879
	.long	.L1221-.L879
	.long	.L1220-.L879
	.long	.L1219-.L879
	.long	.L1218-.L879
	.long	.L1217-.L879
	.long	.L1216-.L879
	.long	.L1215-.L879
	.long	.L1214-.L879
	.long	.L1213-.L879
	.long	.L1212-.L879
	.long	.L1211-.L879
	.long	.L1210-.L879
	.long	.L1209-.L879
	.long	.L1208-.L879
	.long	.L1207-.L879
	.long	.L1206-.L879
	.long	.L1205-.L879
	.long	.L1204-.L879
	.long	.L1203-.L879
	.long	.L1202-.L879
	.long	.L1201-.L879
	.long	.L1200-.L879
	.long	.L1199-.L879
	.long	.L1198-.L879
	.long	.L1197-.L879
	.long	.L1196-.L879
	.long	.L1195-.L879
	.long	.L1194-.L879
	.long	.L1193-.L879
	.long	.L1192-.L879
	.long	.L1191-.L879
	.long	.L1190-.L879
	.long	.L1189-.L879
	.long	.L1188-.L879
	.long	.L1187-.L879
	.long	.L1186-.L879
	.long	.L1185-.L879
	.long	.L1184-.L879
	.long	.L1183-.L879
	.long	.L1182-.L879
	.long	.L1181-.L879
	.long	.L1180-.L879
	.long	.L1179-.L879
	.long	.L1178-.L879
	.long	.L1177-.L879
	.long	.L1176-.L879
	.long	.L1175-.L879
	.long	.L1174-.L879
	.long	.L1173-.L879
	.long	.L1172-.L879
	.long	.L1171-.L879
	.long	.L1170-.L879
	.long	.L1169-.L879
	.long	.L1168-.L879
	.long	.L1167-.L879
	.long	.L1166-.L879
	.long	.L1165-.L879
	.long	.L1164-.L879
	.long	.L1163-.L879
	.long	.L1162-.L879
	.long	.L1161-.L879
	.long	.L1160-.L879
	.long	.L1159-.L879
	.long	.L1158-.L879
	.long	.L1157-.L879
	.long	.L1156-.L879
	.long	.L1155-.L879
	.long	.L1154-.L879
	.long	.L1153-.L879
	.long	.L1152-.L879
	.long	.L1151-.L879
	.long	.L1150-.L879
	.long	.L1149-.L879
	.long	.L1148-.L879
	.long	.L1147-.L879
	.long	.L1146-.L879
	.long	.L1145-.L879
	.long	.L1144-.L879
	.long	.L1143-.L879
	.long	.L1142-.L879
	.long	.L1141-.L879
	.long	.L1140-.L879
	.long	.L1139-.L879
	.long	.L1138-.L879
	.long	.L1137-.L879
	.long	.L1136-.L879
	.long	.L1135-.L879
	.long	.L1134-.L879
	.long	.L1133-.L879
	.long	.L1132-.L879
	.long	.L1131-.L879
	.long	.L1130-.L879
	.long	.L1129-.L879
	.long	.L1128-.L879
	.long	.L1127-.L879
	.long	.L1126-.L879
	.long	.L1125-.L879
	.long	.L1124-.L879
	.long	.L1123-.L879
	.long	.L1122-.L879
	.long	.L1121-.L879
	.long	.L1120-.L879
	.long	.L1119-.L879
	.long	.L1118-.L879
	.long	.L1117-.L879
	.long	.L1116-.L879
	.long	.L1115-.L879
	.long	.L1114-.L879
	.long	.L1113-.L879
	.long	.L1112-.L879
	.long	.L1111-.L879
	.long	.L1110-.L879
	.long	.L1109-.L879
	.long	.L1108-.L879
	.long	.L1107-.L879
	.long	.L1106-.L879
	.long	.L1105-.L879
	.long	.L1104-.L879
	.long	.L1103-.L879
	.long	.L1102-.L879
	.long	.L1101-.L879
	.long	.L1100-.L879
	.long	.L1099-.L879
	.long	.L1098-.L879
	.long	.L1097-.L879
	.long	.L1096-.L879
	.long	.L1095-.L879
	.long	.L1094-.L879
	.long	.L1093-.L879
	.long	.L1092-.L879
	.long	.L1091-.L879
	.long	.L1090-.L879
	.long	.L1089-.L879
	.long	.L1088-.L879
	.long	.L1087-.L879
	.long	.L1086-.L879
	.long	.L1085-.L879
	.long	.L1084-.L879
	.long	.L1083-.L879
	.long	.L1082-.L879
	.long	.L1081-.L879
	.long	.L1080-.L879
	.long	.L1079-.L879
	.long	.L1078-.L879
	.long	.L1077-.L879
	.long	.L1076-.L879
	.long	.L1075-.L879
	.long	.L1074-.L879
	.long	.L1073-.L879
	.long	.L1072-.L879
	.long	.L1071-.L879
	.long	.L1070-.L879
	.long	.L1069-.L879
	.long	.L1068-.L879
	.long	.L1067-.L879
	.long	.L1066-.L879
	.long	.L1065-.L879
	.long	.L1064-.L879
	.long	.L1063-.L879
	.long	.L1062-.L879
	.long	.L1061-.L879
	.long	.L1060-.L879
	.long	.L1059-.L879
	.long	.L1058-.L879
	.long	.L1057-.L879
	.long	.L1056-.L879
	.long	.L1055-.L879
	.long	.L1054-.L879
	.long	.L1053-.L879
	.long	.L1052-.L879
	.long	.L1051-.L879
	.long	.L1050-.L879
	.long	.L1049-.L879
	.long	.L1048-.L879
	.long	.L1047-.L879
	.long	.L1046-.L879
	.long	.L1045-.L879
	.long	.L1044-.L879
	.long	.L1043-.L879
	.long	.L1042-.L879
	.long	.L1041-.L879
	.long	.L1040-.L879
	.long	.L1039-.L879
	.long	.L1038-.L879
	.long	.L1037-.L879
	.long	.L1036-.L879
	.long	.L1035-.L879
	.long	.L1034-.L879
	.long	.L1033-.L879
	.long	.L1032-.L879
	.long	.L1031-.L879
	.long	.L1030-.L879
	.long	.L1029-.L879
	.long	.L1028-.L879
	.long	.L1027-.L879
	.long	.L1026-.L879
	.long	.L1025-.L879
	.long	.L1024-.L879
	.long	.L1023-.L879
	.long	.L1022-.L879
	.long	.L1021-.L879
	.long	.L1020-.L879
	.long	.L1019-.L879
	.long	.L1018-.L879
	.long	.L1017-.L879
	.long	.L1016-.L879
	.long	.L1015-.L879
	.long	.L1014-.L879
	.long	.L1013-.L879
	.long	.L1012-.L879
	.long	.L1011-.L879
	.long	.L1010-.L879
	.long	.L1009-.L879
	.long	.L1008-.L879
	.long	.L1007-.L879
	.long	.L1006-.L879
	.long	.L1005-.L879
	.long	.L1004-.L879
	.long	.L1003-.L879
	.long	.L1002-.L879
	.long	.L1001-.L879
	.long	.L1000-.L879
	.long	.L999-.L879
	.long	.L998-.L879
	.long	.L997-.L879
	.long	.L996-.L879
	.long	.L995-.L879
	.long	.L994-.L879
	.long	.L993-.L879
	.long	.L992-.L879
	.long	.L991-.L879
	.long	.L990-.L879
	.long	.L989-.L879
	.long	.L988-.L879
	.long	.L987-.L879
	.long	.L986-.L879
	.long	.L985-.L879
	.long	.L984-.L879
	.long	.L983-.L879
	.long	.L982-.L879
	.long	.L981-.L879
	.long	.L980-.L879
	.long	.L979-.L879
	.long	.L978-.L879
	.long	.L977-.L879
	.long	.L976-.L879
	.long	.L975-.L879
	.long	.L974-.L879
	.long	.L973-.L879
	.long	.L972-.L879
	.long	.L971-.L879
	.long	.L970-.L879
	.long	.L969-.L879
	.long	.L968-.L879
	.long	.L967-.L879
	.long	.L966-.L879
	.long	.L965-.L879
	.long	.L964-.L879
	.long	.L963-.L879
	.long	.L962-.L879
	.long	.L961-.L879
	.long	.L960-.L879
	.long	.L959-.L879
	.long	.L958-.L879
	.long	.L957-.L879
	.long	.L956-.L879
	.long	.L955-.L879
	.long	.L954-.L879
	.long	.L953-.L879
	.long	.L952-.L879
	.long	.L951-.L879
	.long	.L950-.L879
	.long	.L949-.L879
	.long	.L948-.L879
	.long	.L947-.L879
	.long	.L946-.L879
	.long	.L945-.L879
	.long	.L944-.L879
	.long	.L943-.L879
	.long	.L942-.L879
	.long	.L941-.L879
	.long	.L940-.L879
	.long	.L939-.L879
	.long	.L938-.L879
	.long	.L937-.L879
	.long	.L936-.L879
	.long	.L935-.L879
	.long	.L934-.L879
	.long	.L933-.L879
	.long	.L932-.L879
	.long	.L931-.L879
	.long	.L930-.L879
	.long	.L929-.L879
	.long	.L928-.L879
	.long	.L927-.L879
	.long	.L926-.L879
	.long	.L925-.L879
	.long	.L924-.L879
	.long	.L923-.L879
	.long	.L922-.L879
	.long	.L921-.L879
	.long	.L920-.L879
	.long	.L919-.L879
	.long	.L918-.L879
	.long	.L917-.L879
	.long	.L916-.L879
	.long	.L915-.L879
	.long	.L914-.L879
	.long	.L913-.L879
	.long	.L912-.L879
	.long	.L911-.L879
	.long	.L910-.L879
	.long	.L909-.L879
	.long	.L908-.L879
	.long	.L907-.L879
	.long	.L906-.L879
	.long	.L905-.L879
	.long	.L904-.L879
	.long	.L903-.L879
	.long	.L902-.L879
	.long	.L901-.L879
	.long	.L900-.L879
	.long	.L899-.L879
	.long	.L898-.L879
	.long	.L897-.L879
	.long	.L896-.L879
	.long	.L895-.L879
	.long	.L894-.L879
	.long	.L893-.L879
	.long	.L892-.L879
	.long	.L891-.L879
	.long	.L890-.L879
	.long	.L889-.L879
	.long	.L888-.L879
	.long	.L887-.L879
	.long	.L886-.L879
	.long	.L885-.L879
	.long	.L884-.L879
	.long	.L883-.L879
	.long	.L882-.L879
	.long	.L881-.L879
	.long	.L880-.L879
	.long	.L878-.L879
	.section	.text._ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE
.L1254:
	leaq	.LC386(%rip), %rax
	ret
.L1256:
	leaq	.LC11(%rip), %rax
	ret
.L1006:
	leaq	.LC258(%rip), %rax
	ret
.L1007:
	leaq	.LC257(%rip), %rax
	ret
.L1008:
	leaq	.LC256(%rip), %rax
	ret
.L1009:
	leaq	.LC255(%rip), %rax
	ret
.L1010:
	leaq	.LC254(%rip), %rax
	ret
.L1011:
	leaq	.LC253(%rip), %rax
	ret
.L1012:
	leaq	.LC252(%rip), %rax
	ret
.L1013:
	leaq	.LC251(%rip), %rax
	ret
.L1014:
	leaq	.LC250(%rip), %rax
	ret
.L1015:
	leaq	.LC249(%rip), %rax
	ret
.L1016:
	leaq	.LC248(%rip), %rax
	ret
.L1017:
	leaq	.LC247(%rip), %rax
	ret
.L1018:
	leaq	.LC246(%rip), %rax
	ret
.L1019:
	leaq	.LC245(%rip), %rax
	ret
.L1020:
	leaq	.LC244(%rip), %rax
	ret
.L1021:
	leaq	.LC243(%rip), %rax
	ret
.L1022:
	leaq	.LC242(%rip), %rax
	ret
.L1023:
	leaq	.LC241(%rip), %rax
	ret
.L1024:
	leaq	.LC240(%rip), %rax
	ret
.L1025:
	leaq	.LC239(%rip), %rax
	ret
.L1026:
	leaq	.LC238(%rip), %rax
	ret
.L1027:
	leaq	.LC237(%rip), %rax
	ret
.L1028:
	leaq	.LC236(%rip), %rax
	ret
.L1029:
	leaq	.LC235(%rip), %rax
	ret
.L1030:
	leaq	.LC234(%rip), %rax
	ret
.L1031:
	leaq	.LC233(%rip), %rax
	ret
.L1032:
	leaq	.LC232(%rip), %rax
	ret
.L1033:
	leaq	.LC231(%rip), %rax
	ret
.L1034:
	leaq	.LC230(%rip), %rax
	ret
.L1035:
	leaq	.LC229(%rip), %rax
	ret
.L1036:
	leaq	.LC228(%rip), %rax
	ret
.L1037:
	leaq	.LC227(%rip), %rax
	ret
.L1038:
	leaq	.LC226(%rip), %rax
	ret
.L1039:
	leaq	.LC225(%rip), %rax
	ret
.L1040:
	leaq	.LC224(%rip), %rax
	ret
.L1041:
	leaq	.LC223(%rip), %rax
	ret
.L1042:
	leaq	.LC222(%rip), %rax
	ret
.L1043:
	leaq	.LC221(%rip), %rax
	ret
.L1044:
	leaq	.LC220(%rip), %rax
	ret
.L1045:
	leaq	.LC219(%rip), %rax
	ret
.L1046:
	leaq	.LC218(%rip), %rax
	ret
.L1047:
	leaq	.LC217(%rip), %rax
	ret
.L1048:
	leaq	.LC216(%rip), %rax
	ret
.L1049:
	leaq	.LC215(%rip), %rax
	ret
.L1050:
	leaq	.LC214(%rip), %rax
	ret
.L1051:
	leaq	.LC213(%rip), %rax
	ret
.L1052:
	leaq	.LC212(%rip), %rax
	ret
.L1053:
	leaq	.LC211(%rip), %rax
	ret
.L1054:
	leaq	.LC210(%rip), %rax
	ret
.L1055:
	leaq	.LC209(%rip), %rax
	ret
.L1056:
	leaq	.LC208(%rip), %rax
	ret
.L1057:
	leaq	.LC207(%rip), %rax
	ret
.L1058:
	leaq	.LC206(%rip), %rax
	ret
.L1059:
	leaq	.LC205(%rip), %rax
	ret
.L1060:
	leaq	.LC204(%rip), %rax
	ret
.L1061:
	leaq	.LC203(%rip), %rax
	ret
.L1062:
	leaq	.LC202(%rip), %rax
	ret
.L1063:
	leaq	.LC201(%rip), %rax
	ret
.L1064:
	leaq	.LC200(%rip), %rax
	ret
.L1065:
	leaq	.LC199(%rip), %rax
	ret
.L1066:
	leaq	.LC198(%rip), %rax
	ret
.L1067:
	leaq	.LC197(%rip), %rax
	ret
.L1068:
	leaq	.LC196(%rip), %rax
	ret
.L1069:
	leaq	.LC195(%rip), %rax
	ret
.L1070:
	leaq	.LC194(%rip), %rax
	ret
.L1071:
	leaq	.LC193(%rip), %rax
	ret
.L1072:
	leaq	.LC192(%rip), %rax
	ret
.L1073:
	leaq	.LC191(%rip), %rax
	ret
.L1074:
	leaq	.LC190(%rip), %rax
	ret
.L1075:
	leaq	.LC189(%rip), %rax
	ret
.L1076:
	leaq	.LC188(%rip), %rax
	ret
.L1077:
	leaq	.LC187(%rip), %rax
	ret
.L1078:
	leaq	.LC186(%rip), %rax
	ret
.L1079:
	leaq	.LC185(%rip), %rax
	ret
.L1080:
	leaq	.LC184(%rip), %rax
	ret
.L1081:
	leaq	.LC183(%rip), %rax
	ret
.L1082:
	leaq	.LC182(%rip), %rax
	ret
.L1083:
	leaq	.LC181(%rip), %rax
	ret
.L1084:
	leaq	.LC180(%rip), %rax
	ret
.L1085:
	leaq	.LC179(%rip), %rax
	ret
.L1086:
	leaq	.LC178(%rip), %rax
	ret
.L1087:
	leaq	.LC177(%rip), %rax
	ret
.L1088:
	leaq	.LC176(%rip), %rax
	ret
.L1089:
	leaq	.LC175(%rip), %rax
	ret
.L1090:
	leaq	.LC174(%rip), %rax
	ret
.L1091:
	leaq	.LC173(%rip), %rax
	ret
.L1092:
	leaq	.LC172(%rip), %rax
	ret
.L1093:
	leaq	.LC171(%rip), %rax
	ret
.L1094:
	leaq	.LC170(%rip), %rax
	ret
.L1095:
	leaq	.LC169(%rip), %rax
	ret
.L1096:
	leaq	.LC168(%rip), %rax
	ret
.L1097:
	leaq	.LC167(%rip), %rax
	ret
.L1098:
	leaq	.LC166(%rip), %rax
	ret
.L1099:
	leaq	.LC165(%rip), %rax
	ret
.L1100:
	leaq	.LC164(%rip), %rax
	ret
.L1101:
	leaq	.LC163(%rip), %rax
	ret
.L1102:
	leaq	.LC162(%rip), %rax
	ret
.L1103:
	leaq	.LC161(%rip), %rax
	ret
.L1104:
	leaq	.LC160(%rip), %rax
	ret
.L1105:
	leaq	.LC159(%rip), %rax
	ret
.L1106:
	leaq	.LC158(%rip), %rax
	ret
.L1107:
	leaq	.LC157(%rip), %rax
	ret
.L1108:
	leaq	.LC156(%rip), %rax
	ret
.L1109:
	leaq	.LC155(%rip), %rax
	ret
.L1110:
	leaq	.LC154(%rip), %rax
	ret
.L1111:
	leaq	.LC153(%rip), %rax
	ret
.L1112:
	leaq	.LC152(%rip), %rax
	ret
.L1113:
	leaq	.LC151(%rip), %rax
	ret
.L1114:
	leaq	.LC150(%rip), %rax
	ret
.L1115:
	leaq	.LC149(%rip), %rax
	ret
.L1116:
	leaq	.LC148(%rip), %rax
	ret
.L1117:
	leaq	.LC147(%rip), %rax
	ret
.L1118:
	leaq	.LC146(%rip), %rax
	ret
.L1119:
	leaq	.LC145(%rip), %rax
	ret
.L1120:
	leaq	.LC144(%rip), %rax
	ret
.L1121:
	leaq	.LC143(%rip), %rax
	ret
.L1122:
	leaq	.LC142(%rip), %rax
	ret
.L1123:
	leaq	.LC141(%rip), %rax
	ret
.L1124:
	leaq	.LC140(%rip), %rax
	ret
.L1125:
	leaq	.LC139(%rip), %rax
	ret
.L1253:
	leaq	.LC12(%rip), %rax
	ret
.L1251:
	leaq	.LC14(%rip), %rax
	ret
.L1252:
	leaq	.LC13(%rip), %rax
	ret
.L1247:
	leaq	.LC18(%rip), %rax
	ret
.L1248:
	leaq	.LC17(%rip), %rax
	ret
.L1249:
	leaq	.LC16(%rip), %rax
	ret
.L1250:
	leaq	.LC15(%rip), %rax
	ret
.L1239:
	leaq	.LC26(%rip), %rax
	ret
.L1240:
	leaq	.LC25(%rip), %rax
	ret
.L1241:
	leaq	.LC24(%rip), %rax
	ret
.L1242:
	leaq	.LC23(%rip), %rax
	ret
.L1243:
	leaq	.LC22(%rip), %rax
	ret
.L1244:
	leaq	.LC21(%rip), %rax
	ret
.L1245:
	leaq	.LC20(%rip), %rax
	ret
.L1246:
	leaq	.LC19(%rip), %rax
	ret
.L1223:
	leaq	.LC42(%rip), %rax
	ret
.L1224:
	leaq	.LC41(%rip), %rax
	ret
.L1225:
	leaq	.LC40(%rip), %rax
	ret
.L1226:
	leaq	.LC39(%rip), %rax
	ret
.L1227:
	leaq	.LC38(%rip), %rax
	ret
.L1228:
	leaq	.LC37(%rip), %rax
	ret
.L1229:
	leaq	.LC36(%rip), %rax
	ret
.L1230:
	leaq	.LC35(%rip), %rax
	ret
.L1231:
	leaq	.LC34(%rip), %rax
	ret
.L1232:
	leaq	.LC33(%rip), %rax
	ret
.L1233:
	leaq	.LC32(%rip), %rax
	ret
.L1234:
	leaq	.LC31(%rip), %rax
	ret
.L1235:
	leaq	.LC30(%rip), %rax
	ret
.L1236:
	leaq	.LC29(%rip), %rax
	ret
.L1237:
	leaq	.LC28(%rip), %rax
	ret
.L1238:
	leaq	.LC27(%rip), %rax
	ret
.L1191:
	leaq	.LC74(%rip), %rax
	ret
.L1192:
	leaq	.LC73(%rip), %rax
	ret
.L1193:
	leaq	.LC72(%rip), %rax
	ret
.L1194:
	leaq	.LC71(%rip), %rax
	ret
.L1195:
	leaq	.LC70(%rip), %rax
	ret
.L1196:
	leaq	.LC69(%rip), %rax
	ret
.L1197:
	leaq	.LC68(%rip), %rax
	ret
.L1198:
	leaq	.LC67(%rip), %rax
	ret
.L1199:
	leaq	.LC66(%rip), %rax
	ret
.L1200:
	leaq	.LC65(%rip), %rax
	ret
.L1201:
	leaq	.LC64(%rip), %rax
	ret
.L1202:
	leaq	.LC63(%rip), %rax
	ret
.L1203:
	leaq	.LC62(%rip), %rax
	ret
.L1204:
	leaq	.LC61(%rip), %rax
	ret
.L1205:
	leaq	.LC60(%rip), %rax
	ret
.L1206:
	leaq	.LC59(%rip), %rax
	ret
.L1207:
	leaq	.LC58(%rip), %rax
	ret
.L1208:
	leaq	.LC57(%rip), %rax
	ret
.L1209:
	leaq	.LC56(%rip), %rax
	ret
.L1210:
	leaq	.LC55(%rip), %rax
	ret
.L1211:
	leaq	.LC54(%rip), %rax
	ret
.L1212:
	leaq	.LC53(%rip), %rax
	ret
.L1213:
	leaq	.LC52(%rip), %rax
	ret
.L1214:
	leaq	.LC51(%rip), %rax
	ret
.L1215:
	leaq	.LC50(%rip), %rax
	ret
.L1216:
	leaq	.LC49(%rip), %rax
	ret
.L1217:
	leaq	.LC48(%rip), %rax
	ret
.L1218:
	leaq	.LC47(%rip), %rax
	ret
.L1219:
	leaq	.LC46(%rip), %rax
	ret
.L1220:
	leaq	.LC45(%rip), %rax
	ret
.L1221:
	leaq	.LC44(%rip), %rax
	ret
.L1222:
	leaq	.LC43(%rip), %rax
	ret
.L882:
	leaq	.LC382(%rip), %rax
	ret
.L883:
	leaq	.LC381(%rip), %rax
	ret
.L884:
	leaq	.LC380(%rip), %rax
	ret
.L885:
	leaq	.LC379(%rip), %rax
	ret
.L880:
	leaq	.LC384(%rip), %rax
	ret
.L881:
	leaq	.LC383(%rip), %rax
	ret
.L878:
	leaq	.LC385(%rip), %rax
	ret
.L1134:
	leaq	.LC130(%rip), %rax
	ret
.L1135:
	leaq	.LC129(%rip), %rax
	ret
.L1136:
	leaq	.LC128(%rip), %rax
	ret
.L1137:
	leaq	.LC127(%rip), %rax
	ret
.L1138:
	leaq	.LC126(%rip), %rax
	ret
.L1139:
	leaq	.LC125(%rip), %rax
	ret
.L1140:
	leaq	.LC124(%rip), %rax
	ret
.L1141:
	leaq	.LC123(%rip), %rax
	ret
.L1142:
	leaq	.LC122(%rip), %rax
	ret
.L1143:
	leaq	.LC121(%rip), %rax
	ret
.L1144:
	leaq	.LC120(%rip), %rax
	ret
.L1145:
	leaq	.LC119(%rip), %rax
	ret
.L1146:
	leaq	.LC118(%rip), %rax
	ret
.L1147:
	leaq	.LC117(%rip), %rax
	ret
.L1148:
	leaq	.LC116(%rip), %rax
	ret
.L1149:
	leaq	.LC115(%rip), %rax
	ret
.L1150:
	leaq	.LC114(%rip), %rax
	ret
.L1151:
	leaq	.LC113(%rip), %rax
	ret
.L1152:
	leaq	.LC112(%rip), %rax
	ret
.L1153:
	leaq	.LC111(%rip), %rax
	ret
.L1154:
	leaq	.LC110(%rip), %rax
	ret
.L1155:
	leaq	.LC109(%rip), %rax
	ret
.L1156:
	leaq	.LC108(%rip), %rax
	ret
.L1157:
	leaq	.LC107(%rip), %rax
	ret
.L1158:
	leaq	.LC106(%rip), %rax
	ret
.L1159:
	leaq	.LC105(%rip), %rax
	ret
.L1160:
	leaq	.LC104(%rip), %rax
	ret
.L1161:
	leaq	.LC103(%rip), %rax
	ret
.L1162:
	leaq	.LC102(%rip), %rax
	ret
.L1163:
	leaq	.LC101(%rip), %rax
	ret
.L1164:
	leaq	.LC55(%rip), %rax
	ret
.L1165:
	leaq	.LC100(%rip), %rax
	ret
.L1166:
	leaq	.LC99(%rip), %rax
	ret
.L1167:
	leaq	.LC98(%rip), %rax
	ret
.L1168:
	leaq	.LC97(%rip), %rax
	ret
.L1169:
	leaq	.LC96(%rip), %rax
	ret
.L1170:
	leaq	.LC95(%rip), %rax
	ret
.L1171:
	leaq	.LC94(%rip), %rax
	ret
.L1172:
	leaq	.LC93(%rip), %rax
	ret
.L1173:
	leaq	.LC92(%rip), %rax
	ret
.L1174:
	leaq	.LC91(%rip), %rax
	ret
.L1175:
	leaq	.LC90(%rip), %rax
	ret
.L1176:
	leaq	.LC89(%rip), %rax
	ret
.L1177:
	leaq	.LC88(%rip), %rax
	ret
.L1178:
	leaq	.LC87(%rip), %rax
	ret
.L1179:
	leaq	.LC86(%rip), %rax
	ret
.L1180:
	leaq	.LC85(%rip), %rax
	ret
.L1181:
	leaq	.LC84(%rip), %rax
	ret
.L1182:
	leaq	.LC83(%rip), %rax
	ret
.L1183:
	leaq	.LC82(%rip), %rax
	ret
.L1184:
	leaq	.LC81(%rip), %rax
	ret
.L1185:
	leaq	.LC80(%rip), %rax
	ret
.L1186:
	leaq	.LC79(%rip), %rax
	ret
.L1187:
	leaq	.LC78(%rip), %rax
	ret
.L1188:
	leaq	.LC77(%rip), %rax
	ret
.L1189:
	leaq	.LC76(%rip), %rax
	ret
.L1190:
	leaq	.LC75(%rip), %rax
	ret
.L1126:
	leaq	.LC138(%rip), %rax
	ret
.L1127:
	leaq	.LC137(%rip), %rax
	ret
.L1128:
	leaq	.LC136(%rip), %rax
	ret
.L1129:
	leaq	.LC135(%rip), %rax
	ret
.L1130:
	leaq	.LC134(%rip), %rax
	ret
.L1131:
	leaq	.LC133(%rip), %rax
	ret
.L1132:
	leaq	.LC132(%rip), %rax
	ret
.L1133:
	leaq	.LC131(%rip), %rax
	ret
.L942:
	leaq	.LC322(%rip), %rax
	ret
.L943:
	leaq	.LC321(%rip), %rax
	ret
.L944:
	leaq	.LC320(%rip), %rax
	ret
.L945:
	leaq	.LC319(%rip), %rax
	ret
.L946:
	leaq	.LC318(%rip), %rax
	ret
.L947:
	leaq	.LC317(%rip), %rax
	ret
.L948:
	leaq	.LC316(%rip), %rax
	ret
.L949:
	leaq	.LC315(%rip), %rax
	ret
.L950:
	leaq	.LC314(%rip), %rax
	ret
.L951:
	leaq	.LC313(%rip), %rax
	ret
.L952:
	leaq	.LC312(%rip), %rax
	ret
.L953:
	leaq	.LC311(%rip), %rax
	ret
.L954:
	leaq	.LC310(%rip), %rax
	ret
.L955:
	leaq	.LC309(%rip), %rax
	ret
.L956:
	leaq	.LC308(%rip), %rax
	ret
.L957:
	leaq	.LC307(%rip), %rax
	ret
.L958:
	leaq	.LC306(%rip), %rax
	ret
.L959:
	leaq	.LC305(%rip), %rax
	ret
.L960:
	leaq	.LC304(%rip), %rax
	ret
.L961:
	leaq	.LC303(%rip), %rax
	ret
.L962:
	leaq	.LC302(%rip), %rax
	ret
.L963:
	leaq	.LC301(%rip), %rax
	ret
.L964:
	leaq	.LC300(%rip), %rax
	ret
.L965:
	leaq	.LC299(%rip), %rax
	ret
.L966:
	leaq	.LC298(%rip), %rax
	ret
.L967:
	leaq	.LC297(%rip), %rax
	ret
.L968:
	leaq	.LC296(%rip), %rax
	ret
.L969:
	leaq	.LC295(%rip), %rax
	ret
.L970:
	leaq	.LC294(%rip), %rax
	ret
.L971:
	leaq	.LC293(%rip), %rax
	ret
.L972:
	leaq	.LC292(%rip), %rax
	ret
.L973:
	leaq	.LC291(%rip), %rax
	ret
.L974:
	leaq	.LC290(%rip), %rax
	ret
.L975:
	leaq	.LC289(%rip), %rax
	ret
.L976:
	leaq	.LC288(%rip), %rax
	ret
.L977:
	leaq	.LC287(%rip), %rax
	ret
.L978:
	leaq	.LC286(%rip), %rax
	ret
.L979:
	leaq	.LC285(%rip), %rax
	ret
.L980:
	leaq	.LC284(%rip), %rax
	ret
.L981:
	leaq	.LC283(%rip), %rax
	ret
.L982:
	leaq	.LC282(%rip), %rax
	ret
.L983:
	leaq	.LC281(%rip), %rax
	ret
.L984:
	leaq	.LC280(%rip), %rax
	ret
.L985:
	leaq	.LC279(%rip), %rax
	ret
.L986:
	leaq	.LC278(%rip), %rax
	ret
.L987:
	leaq	.LC277(%rip), %rax
	ret
.L988:
	leaq	.LC276(%rip), %rax
	ret
.L989:
	leaq	.LC275(%rip), %rax
	ret
.L990:
	leaq	.LC274(%rip), %rax
	ret
.L991:
	leaq	.LC273(%rip), %rax
	ret
.L992:
	leaq	.LC272(%rip), %rax
	ret
.L993:
	leaq	.LC271(%rip), %rax
	ret
.L994:
	leaq	.LC270(%rip), %rax
	ret
.L995:
	leaq	.LC269(%rip), %rax
	ret
.L996:
	leaq	.LC268(%rip), %rax
	ret
.L997:
	leaq	.LC267(%rip), %rax
	ret
.L998:
	leaq	.LC266(%rip), %rax
	ret
.L999:
	leaq	.LC265(%rip), %rax
	ret
.L1000:
	leaq	.LC264(%rip), %rax
	ret
.L1001:
	leaq	.LC263(%rip), %rax
	ret
.L1002:
	leaq	.LC262(%rip), %rax
	ret
.L1003:
	leaq	.LC261(%rip), %rax
	ret
.L1004:
	leaq	.LC260(%rip), %rax
	ret
.L1005:
	leaq	.LC259(%rip), %rax
	ret
.L910:
	leaq	.LC354(%rip), %rax
	ret
.L911:
	leaq	.LC353(%rip), %rax
	ret
.L912:
	leaq	.LC352(%rip), %rax
	ret
.L913:
	leaq	.LC351(%rip), %rax
	ret
.L914:
	leaq	.LC350(%rip), %rax
	ret
.L915:
	leaq	.LC349(%rip), %rax
	ret
.L916:
	leaq	.LC348(%rip), %rax
	ret
.L917:
	leaq	.LC347(%rip), %rax
	ret
.L918:
	leaq	.LC346(%rip), %rax
	ret
.L919:
	leaq	.LC345(%rip), %rax
	ret
.L920:
	leaq	.LC344(%rip), %rax
	ret
.L921:
	leaq	.LC343(%rip), %rax
	ret
.L922:
	leaq	.LC342(%rip), %rax
	ret
.L923:
	leaq	.LC341(%rip), %rax
	ret
.L924:
	leaq	.LC340(%rip), %rax
	ret
.L925:
	leaq	.LC339(%rip), %rax
	ret
.L926:
	leaq	.LC338(%rip), %rax
	ret
.L927:
	leaq	.LC337(%rip), %rax
	ret
.L928:
	leaq	.LC336(%rip), %rax
	ret
.L929:
	leaq	.LC335(%rip), %rax
	ret
.L930:
	leaq	.LC334(%rip), %rax
	ret
.L931:
	leaq	.LC333(%rip), %rax
	ret
.L932:
	leaq	.LC332(%rip), %rax
	ret
.L933:
	leaq	.LC331(%rip), %rax
	ret
.L934:
	leaq	.LC330(%rip), %rax
	ret
.L935:
	leaq	.LC329(%rip), %rax
	ret
.L936:
	leaq	.LC328(%rip), %rax
	ret
.L937:
	leaq	.LC327(%rip), %rax
	ret
.L938:
	leaq	.LC326(%rip), %rax
	ret
.L939:
	leaq	.LC325(%rip), %rax
	ret
.L940:
	leaq	.LC324(%rip), %rax
	ret
.L941:
	leaq	.LC323(%rip), %rax
	ret
.L894:
	leaq	.LC370(%rip), %rax
	ret
.L895:
	leaq	.LC369(%rip), %rax
	ret
.L896:
	leaq	.LC368(%rip), %rax
	ret
.L897:
	leaq	.LC367(%rip), %rax
	ret
.L898:
	leaq	.LC366(%rip), %rax
	ret
.L899:
	leaq	.LC365(%rip), %rax
	ret
.L900:
	leaq	.LC364(%rip), %rax
	ret
.L901:
	leaq	.LC363(%rip), %rax
	ret
.L902:
	leaq	.LC362(%rip), %rax
	ret
.L903:
	leaq	.LC361(%rip), %rax
	ret
.L904:
	leaq	.LC360(%rip), %rax
	ret
.L905:
	leaq	.LC359(%rip), %rax
	ret
.L906:
	leaq	.LC358(%rip), %rax
	ret
.L907:
	leaq	.LC357(%rip), %rax
	ret
.L908:
	leaq	.LC356(%rip), %rax
	ret
.L909:
	leaq	.LC355(%rip), %rax
	ret
.L886:
	leaq	.LC378(%rip), %rax
	ret
.L887:
	leaq	.LC377(%rip), %rax
	ret
.L888:
	leaq	.LC376(%rip), %rax
	ret
.L889:
	leaq	.LC375(%rip), %rax
	ret
.L890:
	leaq	.LC374(%rip), %rax
	ret
.L891:
	leaq	.LC373(%rip), %rax
	ret
.L892:
	leaq	.LC372(%rip), %rax
	ret
.L893:
	leaq	.LC371(%rip), %rax
	ret
.L877:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE20749:
	.size	_ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE, .-_ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE
	.section	.text._ZN2v88internal16MessageFormatter6FormatEPNS0_7IsolateENS0_15MessageTemplateENS0_6HandleINS0_6StringEEES7_S7_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16MessageFormatter6FormatEPNS0_7IsolateENS0_15MessageTemplateENS0_6HandleINS0_6StringEEES7_S7_
	.type	_ZN2v88internal16MessageFormatter6FormatEPNS0_7IsolateENS0_15MessageTemplateENS0_6HandleINS0_6StringEEES7_S7_, @function
_ZN2v88internal16MessageFormatter6FormatEPNS0_7IsolateENS0_15MessageTemplateENS0_6HandleINS0_6StringEEES7_S7_:
.LFB20750:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r9
	movl	%esi, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE
	testq	%rax, %rax
	je	.L1278
	leaq	-128(%rbp), %r14
	movq	%rax, %rbx
	movq	%r9, %rsi
	movq	%rcx, %r13
	movq	%r14, %rdi
	movq	%r8, %r12
	call	_ZN2v88internal24IncrementalStringBuilderC1EPNS0_7IsolateE@PLT
	movzbl	(%rbx), %edx
	movq	%r13, -72(%rbp)
	xorl	%r13d, %r13d
	movq	%r15, -80(%rbp)
	movq	%r12, -64(%rbp)
	testb	%dl, %dl
	jne	.L1260
	jmp	.L1266
	.p2align 4,,10
	.p2align 3
.L1261:
	movl	-108(%rbp), %eax
	movl	-120(%rbp), %edi
	movq	-96(%rbp), %rcx
	leal	1(%rax), %esi
	testl	%edi, %edi
	movq	(%rcx), %rcx
	movl	%esi, -108(%rbp)
	jne	.L1265
	addl	$16, %eax
	cltq
	movb	%dl, -1(%rcx,%rax)
	movl	-112(%rbp), %eax
	cmpl	%eax, -108(%rbp)
	je	.L1276
.L1270:
	movq	%r12, %rax
	movq	%rbx, %r12
	movq	%rax, %rbx
.L1264:
	movzbl	1(%r12), %edx
	testb	%dl, %dl
	je	.L1266
.L1260:
	leaq	1(%rbx), %r12
	cmpb	$37, %dl
	jne	.L1261
	cmpb	$37, 1(%rbx)
	jne	.L1262
	movl	-108(%rbp), %eax
	movl	-120(%rbp), %r9d
	addq	$2, %rbx
	movq	-96(%rbp), %rdx
	leal	1(%rax), %ecx
	testl	%r9d, %r9d
	movq	(%rdx), %rdx
	movl	%ecx, -108(%rbp)
	jne	.L1263
	addl	$16, %eax
	cltq
	movb	$37, -1(%rdx,%rax)
	movl	-112(%rbp), %eax
	cmpl	%eax, -108(%rbp)
	jne	.L1264
.L1277:
	movq	%r14, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	1(%r12), %edx
	testb	%dl, %dl
	jne	.L1260
	.p2align 4,,10
	.p2align 3
.L1266:
	movq	%r14, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6FinishEv@PLT
.L1259:
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L1279
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1265:
	.cfi_restore_state
	leal	16(%rax,%rax), %eax
	cltq
	movw	%dx, -1(%rcx,%rax)
	movl	-112(%rbp), %eax
	cmpl	%eax, -108(%rbp)
	jne	.L1270
.L1276:
	movq	%r14, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movq	%r12, %rax
	movq	%rbx, %r12
	movq	%rax, %rbx
	jmp	.L1264
	.p2align 4,,10
	.p2align 3
.L1262:
	movq	-80(%rbp,%r13,8), %rsi
	movq	%r14, %rdi
	leal	1(%r13), %r15d
	movl	%r15d, %r13d
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	movq	%r12, %rax
	movq	%rbx, %r12
	movq	%rax, %rbx
	jmp	.L1264
	.p2align 4,,10
	.p2align 3
.L1263:
	leal	16(%rax,%rax), %eax
	movl	$37, %r8d
	cltq
	movw	%r8w, -1(%rdx,%rax)
	movl	-112(%rbp), %eax
	cmpl	%eax, -108(%rbp)
	jne	.L1264
	jmp	.L1277
	.p2align 4,,10
	.p2align 3
.L1278:
	movq	%r9, %rdi
	call	_ZN2v88internal7Isolate21ThrowIllegalOperationEv@PLT
	xorl	%eax, %eax
	jmp	.L1259
.L1279:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20750:
	.size	_ZN2v88internal16MessageFormatter6FormatEPNS0_7IsolateENS0_15MessageTemplateENS0_6HandleINS0_6StringEEES7_S7_, .-_ZN2v88internal16MessageFormatter6FormatEPNS0_7IsolateENS0_15MessageTemplateENS0_6HandleINS0_6StringEEES7_S7_
	.section	.rodata._ZN2v88internal16MessageFormatter6FormatEPNS0_7IsolateENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEE.str1.1,"aMS",@progbits,1
.LC387:
	.string	"<error>"
	.section	.text._ZN2v88internal16MessageFormatter6FormatEPNS0_7IsolateENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16MessageFormatter6FormatEPNS0_7IsolateENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEE
	.type	_ZN2v88internal16MessageFormatter6FormatEPNS0_7IsolateENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEE, @function
_ZN2v88internal16MessageFormatter6FormatEPNS0_7IsolateENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEE:
.LFB20744:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	movq	%rdx, %rsi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal6Object21NoSideEffectsToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	leaq	128(%r12), %rcx
	movl	%r13d, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rcx, %r8
	call	_ZN2v88internal16MessageFormatter6FormatEPNS0_7IsolateENS0_15MessageTemplateENS0_6HandleINS0_6StringEEES7_S7_
	testq	%rax, %rax
	je	.L1300
	movq	(%rax), %rdx
	movq	-1(%rdx), %rcx
	movq	%rdx, %rsi
	cmpw	$63, 11(%rcx)
	jbe	.L1301
.L1284:
	movq	-1(%rsi), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L1302
.L1282:
	movq	-24(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L1303
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1301:
	.cfi_restore_state
	movq	-1(%rdx), %rcx
	movzwl	11(%rcx), %ecx
	andl	$7, %ecx
	cmpw	$1, %cx
	jne	.L1284
	movq	-1(%rdx), %rcx
	movzwl	11(%rcx), %ecx
	andl	$7, %ecx
	cmpw	$1, %cx
	jne	.L1287
	movq	23(%rdx), %rcx
	movl	11(%rcx), %ecx
	testl	%ecx, %ecx
	je	.L1287
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	jmp	.L1282
	.p2align 4,,10
	.p2align 3
.L1302:
	movq	-1(%rsi), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$5, %dx
	jne	.L1282
	movq	(%rax), %rax
	movq	41112(%r12), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1295
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	jmp	.L1282
	.p2align 4,,10
	.p2align 3
.L1287:
	movq	41112(%r12), %rdi
	movq	15(%rdx), %rsi
	testq	%rdi, %rdi
	je	.L1289
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	jmp	.L1284
	.p2align 4,,10
	.p2align 3
.L1300:
	movq	96(%r12), %rax
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	$7, -40(%rbp)
	movq	%rax, 12480(%r12)
	leaq	.LC387(%rip), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal7Factory17InternalizeStringIhEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb@PLT
	jmp	.L1282
	.p2align 4,,10
	.p2align 3
.L1295:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L1304
.L1297:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L1282
	.p2align 4,,10
	.p2align 3
.L1289:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L1305
.L1291:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L1284
	.p2align 4,,10
	.p2align 3
.L1304:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1297
.L1305:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1291
.L1303:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20744:
	.size	_ZN2v88internal16MessageFormatter6FormatEPNS0_7IsolateENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEE, .-_ZN2v88internal16MessageFormatter6FormatEPNS0_7IsolateENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEE
	.section	.text._ZN2v88internal14MessageHandler10GetMessageEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MessageHandler10GetMessageEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	.type	_ZN2v88internal14MessageHandler10GetMessageEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE, @function
_ZN2v88internal14MessageHandler10GetMessageEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE:
.LFB20642:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	41112(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	31(%rax), %r13
	testq	%rdi, %rdi
	je	.L1307
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1308:
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movslq	27(%rax), %rbx
	call	_ZN2v88internal6Object21NoSideEffectsToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	leaq	128(%r12), %rcx
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rcx, %r8
	movl	%ebx, %esi
	call	_ZN2v88internal16MessageFormatter6FormatEPNS0_7IsolateENS0_15MessageTemplateENS0_6HandleINS0_6StringEEES7_S7_
	testq	%rax, %rax
	je	.L1329
	movq	(%rax), %rdx
	movq	-1(%rdx), %rcx
	movq	%rdx, %rsi
	cmpw	$63, 11(%rcx)
	jbe	.L1330
.L1313:
	movq	-1(%rsi), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L1331
.L1311:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1332
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1330:
	.cfi_restore_state
	movq	-1(%rdx), %rcx
	movzwl	11(%rcx), %ecx
	andl	$7, %ecx
	cmpw	$1, %cx
	jne	.L1313
	movq	-1(%rdx), %rcx
	movzwl	11(%rcx), %ecx
	andl	$7, %ecx
	cmpw	$1, %cx
	jne	.L1316
	movq	23(%rdx), %rcx
	movl	11(%rcx), %ecx
	testl	%ecx, %ecx
	je	.L1316
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	jmp	.L1311
	.p2align 4,,10
	.p2align 3
.L1331:
	movq	-1(%rsi), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$5, %dx
	jne	.L1311
	movq	(%rax), %rax
	movq	41112(%r12), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1324
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	jmp	.L1311
	.p2align 4,,10
	.p2align 3
.L1307:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L1333
.L1309:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, (%rsi)
	jmp	.L1308
	.p2align 4,,10
	.p2align 3
.L1316:
	movq	41112(%r12), %rdi
	movq	15(%rdx), %rsi
	testq	%rdi, %rdi
	je	.L1318
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	jmp	.L1313
	.p2align 4,,10
	.p2align 3
.L1329:
	movq	96(%r12), %rax
	leaq	-64(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	$7, -56(%rbp)
	movq	%rax, 12480(%r12)
	leaq	.LC387(%rip), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal7Factory17InternalizeStringIhEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb@PLT
	jmp	.L1311
	.p2align 4,,10
	.p2align 3
.L1324:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L1334
.L1326:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L1311
	.p2align 4,,10
	.p2align 3
.L1333:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L1309
	.p2align 4,,10
	.p2align 3
.L1318:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L1335
.L1320:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L1313
	.p2align 4,,10
	.p2align 3
.L1334:
	movq	%r12, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	jmp	.L1326
.L1335:
	movq	%r12, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	jmp	.L1320
.L1332:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20642:
	.size	_ZN2v88internal14MessageHandler10GetMessageEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE, .-_ZN2v88internal14MessageHandler10GetMessageEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	.section	.text._ZN2v88internal14MessageHandler19GetLocalizedMessageEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MessageHandler19GetLocalizedMessageEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	.type	_ZN2v88internal14MessageHandler19GetLocalizedMessageEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE, @function
_ZN2v88internal14MessageHandler19GetLocalizedMessageEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE:
.LFB20643:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	movq	%rdx, %rsi
	pushq	%rbx
	movq	%r12, %rdi
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%r12)
	call	_ZN2v88internal14MessageHandler10GetMessageEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	leaq	-48(%rbp), %rsi
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	movq	(%rax), %rax
	movl	$1, %ecx
	movl	$1, %edx
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1336
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1336:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1340
	addq	$16, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1340:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20643:
	.size	_ZN2v88internal14MessageHandler19GetLocalizedMessageEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE, .-_ZN2v88internal14MessageHandler19GetLocalizedMessageEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	.section	.rodata._ZN2v88internal14MessageHandler20DefaultMessageReportEPNS0_7IsolateEPKNS0_15MessageLocationENS0_6HandleINS0_6ObjectEEE.str1.1,"aMS",@progbits,1
.LC388:
	.string	"%s\n"
.LC389:
	.string	"%s:%i: %s\n"
.LC390:
	.string	"<unknown>"
	.section	.text._ZN2v88internal14MessageHandler20DefaultMessageReportEPNS0_7IsolateEPKNS0_15MessageLocationENS0_6HandleINS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MessageHandler20DefaultMessageReportEPNS0_7IsolateEPKNS0_15MessageLocationENS0_6HandleINS0_6ObjectEEE
	.type	_ZN2v88internal14MessageHandler20DefaultMessageReportEPNS0_7IsolateEPKNS0_15MessageLocationENS0_6HandleINS0_6ObjectEEE, @function
_ZN2v88internal14MessageHandler20DefaultMessageReportEPNS0_7IsolateEPKNS0_15MessageLocationENS0_6HandleINS0_6ObjectEEE:
.LFB20613:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-64(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movq	%rdx, %rsi
	subq	$56, %rsp
	movq	41088(%rdi), %r14
	movq	41096(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdi)
	call	_ZN2v88internal14MessageHandler10GetMessageEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	leaq	-80(%rbp), %rdi
	xorl	%r8d, %r8d
	movq	%r15, %rsi
	movq	(%rax), %rax
	movl	$1, %ecx
	movl	$1, %edx
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r13
	je	.L1342
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1342:
	testq	%rbx, %rbx
	je	.L1363
	movq	41112(%r12), %rdi
	movq	41088(%r12), %r13
	addl	$1, 41104(%r12)
	movq	(%rbx), %rax
	movq	41096(%r12), %r14
	movq	(%rax), %rax
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1345
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	testb	$1, %sil
	jne	.L1348
.L1362:
	movq	-80(%rbp), %rcx
	movl	8(%rbx), %edx
.L1349:
	leaq	.LC390(%rip), %rsi
	leaq	.LC389(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
.L1355:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L1344
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1344:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1341
	call	_ZdaPv@PLT
.L1341:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1364
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1345:
	.cfi_restore_state
	movq	%r13, %rax
	cmpq	%r14, %r13
	je	.L1365
.L1347:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	testb	$1, %sil
	je	.L1362
.L1348:
	movq	-1(%rsi), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L1362
	movq	(%rax), %rax
	movl	$1, %ecx
	movl	$1, %edx
	movq	%r15, %rdi
	leaq	-72(%rbp), %rsi
	xorl	%r8d, %r8d
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	-64(%rbp), %r15
	movq	-80(%rbp), %rcx
	movl	8(%rbx), %edx
	testq	%r15, %r15
	je	.L1349
	movq	%r15, %rsi
	leaq	.LC389(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	%r15, %rdi
	call	_ZdaPv@PLT
	jmp	.L1355
	.p2align 4,,10
	.p2align 3
.L1363:
	movq	-80(%rbp), %rsi
	leaq	.LC388(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1344
	.p2align 4,,10
	.p2align 3
.L1365:
	movq	%r12, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	jmp	.L1347
.L1364:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20613:
	.size	_ZN2v88internal14MessageHandler20DefaultMessageReportEPNS0_7IsolateEPKNS0_15MessageLocationENS0_6HandleINS0_6ObjectEEE, .-_ZN2v88internal14MessageHandler20DefaultMessageReportEPNS0_7IsolateEPKNS0_15MessageLocationENS0_6HandleINS0_6ObjectEEE
	.section	.text._ZN2v88internal14MessageHandler25ReportMessageNoExceptionsEPNS0_7IsolateEPKNS0_15MessageLocationENS0_6HandleINS0_6ObjectEEENS_5LocalINS_5ValueEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MessageHandler25ReportMessageNoExceptionsEPNS0_7IsolateEPKNS0_15MessageLocationENS0_6HandleINS0_6ObjectEEENS_5LocalINS_5ValueEEE
	.type	_ZN2v88internal14MessageHandler25ReportMessageNoExceptionsEPNS0_7IsolateEPKNS0_15MessageLocationENS0_6HandleINS0_6ObjectEEENS_5LocalINS_5ValueEEE, @function
_ZN2v88internal14MessageHandler25ReportMessageNoExceptionsEPNS0_7IsolateEPKNS0_15MessageLocationENS0_6HandleINS0_6ObjectEEENS_5LocalINS_5ValueEEE:
.LFB20641:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movq	%rdx, %rdi
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 3, -56
	movq	%rcx, -208(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v87Message10ErrorLevelEv@PLT
	movl	%eax, -172(%rbp)
	movq	4744(%r15), %rax
	movq	15(%rax), %rdx
	sarq	$32, %rdx
	testq	%rdx, %rdx
	je	.L1396
	jle	.L1366
	subl	$1, %edx
	movl	$24, %r14d
	leaq	32(,%rdx,8), %rbx
	movq	%rbx, -168(%rbp)
	jmp	.L1380
	.p2align 4,,10
	.p2align 3
.L1399:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L1373:
	pxor	%xmm0, %xmm0
	movq	$0, -128(%rbp)
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm0, -144(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %edx
	testl	%edx, %edx
	jne	.L1397
.L1375:
	leaq	-112(%rbp), %r11
	movq	%r15, %rsi
	movq	%rax, -200(%rbp)
	movq	%r11, %rdi
	movq	%r11, -192(%rbp)
	call	_ZN2v88TryCatchC1EPNS_7IsolateE@PLT
	movq	-200(%rbp), %rax
	movq	%r12, %rdi
	movq	(%rax), %rcx
	cmpq	%rcx, 88(%r15)
	cmove	-208(%rbp), %rax
	movq	%rax, %rsi
	movq	-184(%rbp), %rax
	call	*%rax
	movq	-192(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN2v88TryCatchD1Ev@PLT
	movq	-160(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1398
.L1378:
	movq	96(%r15), %rax
	cmpq	12552(%r15), %rax
	je	.L1379
	movq	%rax, 12552(%r15)
.L1379:
	subl	$1, 41104(%r15)
	movq	%r13, 41088(%r15)
	cmpq	41096(%r15), %rbx
	je	.L1381
	movq	%rbx, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1381:
	addq	$8, %r14
	cmpq	-168(%rbp), %r14
	je	.L1366
	movq	4744(%r15), %rax
.L1380:
	addl	$1, 41104(%r15)
	movq	41088(%r15), %r13
	movq	41096(%r15), %rbx
	movq	-1(%r14,%rax), %rdx
	cmpq	%rdx, 88(%r15)
	je	.L1379
	movq	-1(%r14,%rax), %rdx
	movq	15(%rdx), %rsi
	movq	31(%rdx), %rax
	sarq	$32, %rax
	testl	%eax, -172(%rbp)
	je	.L1379
	movq	7(%rsi), %rax
	movq	%rax, -184(%rbp)
	movq	23(%rdx), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	jne	.L1399
	movq	%r13, %rax
	cmpq	41096(%r15), %r13
	je	.L1400
.L1374:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%rsi, (%rax)
	jmp	.L1373
	.p2align 4,,10
	.p2align 3
.L1396:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal14MessageHandler20DefaultMessageReportEPNS0_7IsolateEPKNS0_15MessageLocationENS0_6HandleINS0_6ObjectEEE
	movq	96(%r15), %rax
	cmpq	12552(%r15), %rax
	je	.L1366
	movq	%rax, 12552(%r15)
.L1366:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1401
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1398:
	.cfi_restore_state
	leaq	-152(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1378
	.p2align 4,,10
	.p2align 3
.L1397:
	movq	%rax, -192(%rbp)
	movq	40960(%r15), %rax
	movl	$168, %edx
	leaq	-152(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -160(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	movq	-192(%rbp), %rax
	jmp	.L1375
	.p2align 4,,10
	.p2align 3
.L1400:
	movq	%r15, %rdi
	movq	%rsi, -192(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-192(%rbp), %rsi
	jmp	.L1374
.L1401:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20641:
	.size	_ZN2v88internal14MessageHandler25ReportMessageNoExceptionsEPNS0_7IsolateEPKNS0_15MessageLocationENS0_6HandleINS0_6ObjectEEENS_5LocalINS_5ValueEEE, .-_ZN2v88internal14MessageHandler25ReportMessageNoExceptionsEPNS0_7IsolateEPKNS0_15MessageLocationENS0_6HandleINS0_6ObjectEEENS_5LocalINS_5ValueEEE
	.section	.rodata._ZN2v88internal14MessageHandler13ReportMessageEPNS0_7IsolateEPKNS0_15MessageLocationENS0_6HandleINS0_15JSMessageObjectEEE.str1.1,"aMS",@progbits,1
.LC391:
	.string	"exception"
	.section	.text._ZN2v88internal14MessageHandler13ReportMessageEPNS0_7IsolateEPKNS0_15MessageLocationENS0_6HandleINS0_15JSMessageObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MessageHandler13ReportMessageEPNS0_7IsolateEPKNS0_15MessageLocationENS0_6HandleINS0_15JSMessageObjectEEE
	.type	_ZN2v88internal14MessageHandler13ReportMessageEPNS0_7IsolateEPKNS0_15MessageLocationENS0_6HandleINS0_15JSMessageObjectEEE, @function
_ZN2v88internal14MessageHandler13ReportMessageEPNS0_7IsolateEPKNS0_15MessageLocationENS0_6HandleINS0_15JSMessageObjectEEE:
.LFB20640:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rdx, %rdi
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v87Message10ErrorLevelEv@PLT
	cmpl	$8, %eax
	jne	.L1403
	movq	41112(%r12), %rdi
	movq	12480(%r12), %rsi
	cmpq	%rsi, 96(%r12)
	cmove	88(%r12), %rsi
	testq	%rdi, %rdi
	je	.L1405
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L1406:
	movq	41112(%r12), %rdi
	movq	12480(%r12), %rsi
	testq	%rdi, %rdi
	je	.L1408
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rbx
.L1409:
	movb	$0, 12545(%r12)
	movq	96(%r12), %rax
	movq	%rax, 12480(%r12)
	movq	(%r14), %rax
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L1442
.L1412:
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal14MessageHandler25ReportMessageNoExceptionsEPNS0_7IsolateEPKNS0_15MessageLocationENS0_6HandleINS0_6ObjectEEENS_5LocalINS_5ValueEEE
	movq	(%rbx), %rax
	movq	%rax, 12480(%r12)
.L1402:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1443
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1405:
	.cfi_restore_state
	movq	41088(%r12), %r15
	cmpq	41096(%r12), %r15
	je	.L1444
.L1407:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r15)
	jmp	.L1406
	.p2align 4,,10
	.p2align 3
.L1403:
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal14MessageHandler25ReportMessageNoExceptionsEPNS0_7IsolateEPKNS0_15MessageLocationENS0_6HandleINS0_6ObjectEEENS_5LocalINS_5ValueEEE
	jmp	.L1402
	.p2align 4,,10
	.p2align 3
.L1408:
	movq	41088(%r12), %rbx
	cmpq	41096(%r12), %rbx
	je	.L1445
.L1410:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rbx)
	jmp	.L1409
	.p2align 4,,10
	.p2align 3
.L1442:
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L1412
	movq	41088(%r12), %rax
	movq	41112(%r12), %rdi
	addl	$1, 41104(%r12)
	movq	%rax, -136(%rbp)
	movq	41096(%r12), %rax
	movq	%rax, -128(%rbp)
	movq	(%r14), %rax
	movq	31(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1414
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r10
.L1415:
	testb	$1, %sil
	jne	.L1446
.L1417:
	leaq	-112(%rbp), %rax
	movq	%r12, %rsi
	movq	%r10, -144(%rbp)
	movq	%rax, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN2v88TryCatchC1EPNS_7IsolateE@PLT
	movq	-120(%rbp), %rdi
	xorl	%esi, %esi
	call	_ZN2v88TryCatch10SetVerboseEb@PLT
	movq	-120(%rbp), %rdi
	xorl	%esi, %esi
	call	_ZN2v88TryCatch17SetCaptureMessageEb@PLT
	movq	-144(%rbp), %r10
	movq	(%r10), %rax
	testb	$1, %al
	jne	.L1447
.L1419:
	movq	%r10, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %r10
.L1422:
	movq	-120(%rbp), %rdi
	movq	%r10, -144(%rbp)
	call	_ZN2v88TryCatchD1Ev@PLT
	movq	-144(%rbp), %r10
.L1421:
	testq	%r10, %r10
	je	.L1448
.L1423:
	movq	(%r14), %rdi
	movq	(%r10), %rdx
	movq	%rdx, 31(%rdi)
	leaq	31(%rdi), %rsi
	testb	$1, %dl
	je	.L1428
	movq	%rdx, %r10
	andq	$-262144, %r10
	movq	8(%r10), %rax
	movq	%r10, -120(%rbp)
	testl	$262144, %eax
	je	.L1426
	movq	%rdx, -160(%rbp)
	movq	%rsi, -152(%rbp)
	movq	%rdi, -144(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-120(%rbp), %r10
	movq	-160(%rbp), %rdx
	movq	-152(%rbp), %rsi
	movq	-144(%rbp), %rdi
	movq	8(%r10), %rax
.L1426:
	testb	$24, %al
	je	.L1428
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1428
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L1428:
	subl	$1, 41104(%r12)
	movq	-136(%rbp), %rax
	movq	%rax, 41088(%r12)
	movq	-128(%rbp), %rax
	cmpq	41096(%r12), %rax
	je	.L1412
	movq	%rax, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	jmp	.L1412
	.p2align 4,,10
	.p2align 3
.L1444:
	movq	%r12, %rdi
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L1407
	.p2align 4,,10
	.p2align 3
.L1445:
	movq	%r12, %rdi
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L1410
	.p2align 4,,10
	.p2align 3
.L1414:
	movq	-136(%rbp), %rax
	movq	-128(%rbp), %rcx
	movq	%rax, %r10
	cmpq	%rcx, %rax
	je	.L1449
.L1416:
	leaq	8(%r10), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r10)
	jmp	.L1415
	.p2align 4,,10
	.p2align 3
.L1446:
	movq	-1(%rsi), %rax
	cmpw	$1067, 11(%rax)
	jne	.L1417
	movq	%r10, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object21NoSideEffectsToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %r10
	jmp	.L1421
	.p2align 4,,10
	.p2align 3
.L1447:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1419
	jmp	.L1422
	.p2align 4,,10
	.p2align 3
.L1448:
	movq	96(%r12), %rax
	leaq	-112(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movb	$0, 12545(%r12)
	movq	%rax, 12480(%r12)
	leaq	.LC391(%rip), %rax
	movq	%rax, -112(%rbp)
	movq	$9, -104(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %r10
	testq	%rax, %rax
	jne	.L1423
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1449:
	movq	%r12, %rdi
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %rsi
	movq	%rax, %r10
	jmp	.L1416
.L1443:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20640:
	.size	_ZN2v88internal14MessageHandler13ReportMessageEPNS0_7IsolateEPKNS0_15MessageLocationENS0_6HandleINS0_15JSMessageObjectEEE, .-_ZN2v88internal14MessageHandler13ReportMessageEPNS0_7IsolateEPKNS0_15MessageLocationENS0_6HandleINS0_15JSMessageObjectEEE
	.section	.text._ZN2v88internal10ErrorUtils9ConstructEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEENS4_INS0_6ObjectEEES8_NS0_13FrameSkipModeES8_NS1_20StackTraceCollectionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10ErrorUtils9ConstructEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEENS4_INS0_6ObjectEEES8_NS0_13FrameSkipModeES8_NS1_20StackTraceCollectionE
	.type	_ZN2v88internal10ErrorUtils9ConstructEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEENS4_INS0_6ObjectEEES8_NS0_13FrameSkipModeES8_NS1_20StackTraceCollectionE, @function
_ZN2v88internal10ErrorUtils9ConstructEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEENS4_INS0_6ObjectEEES8_NS0_13FrameSkipModeES8_NS1_20StackTraceCollectionE:
.LFB20751:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r9, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%r8d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	(%rdx), %rax
	testb	$1, %al
	jne	.L1451
.L1453:
	movq	%rdi, %rsi
.L1452:
	xorl	%edx, %edx
	call	_ZN2v88internal8JSObject3NewENS0_6HandleINS0_10JSFunctionEEENS2_INS0_10JSReceiverEEENS2_INS0_14AllocationSiteEEE@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L1458
	movq	(%r15), %rax
	movq	%r15, %rdx
	cmpq	%rax, 88(%r12)
	jne	.L1480
.L1456:
	movl	16(%rbp), %eax
	testl	%eax, %eax
	je	.L1461
	cmpl	$1, 16(%rbp)
	je	.L1462
	movq	%rbx, %rax
.L1481:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1480:
	.cfi_restore_state
	testb	$1, %al
	jne	.L1457
.L1460:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L1458
.L1459:
	leaq	2832(%r12), %rsi
	movl	$2, %ecx
	movq	%rbx, %rdi
	call	_ZN2v88internal8JSObject30SetOwnPropertyIgnoreAttributesENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	testq	%rax, %rax
	jne	.L1456
.L1458:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1461:
	.cfi_restore_state
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate31CaptureAndSetDetailedStackTraceENS0_6HandleINS0_10JSReceiverEEE@PLT
	testq	%rax, %rax
	je	.L1458
.L1462:
	movq	%r14, %rcx
	movl	%r13d, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate29CaptureAndSetSimpleStackTraceENS0_6HandleINS0_10JSReceiverEEENS0_13FrameSkipModeENS2_INS0_6ObjectEEE@PLT
	testq	%rax, %rax
	je	.L1458
	movq	%rbx, %rax
	jmp	.L1481
	.p2align 4,,10
	.p2align 3
.L1451:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L1453
	movq	%rdx, %rsi
	jmp	.L1452
	.p2align 4,,10
	.p2align 3
.L1457:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1460
	jmp	.L1459
	.cfi_endproc
.LFE20751:
	.size	_ZN2v88internal10ErrorUtils9ConstructEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEENS4_INS0_6ObjectEEES8_NS0_13FrameSkipModeES8_NS1_20StackTraceCollectionE, .-_ZN2v88internal10ErrorUtils9ConstructEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEENS4_INS0_6ObjectEEES8_NS0_13FrameSkipModeES8_NS1_20StackTraceCollectionE
	.section	.rodata._ZN2v88internal10ErrorUtils8ToStringEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE.str1.1,"aMS",@progbits,1
.LC392:
	.string	": "
.LC393:
	.string	"Error.prototype.toString"
	.section	.text._ZN2v88internal10ErrorUtils8ToStringEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10ErrorUtils8ToStringEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	.type	_ZN2v88internal10ErrorUtils8ToStringEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE, @function
_ZN2v88internal10ErrorUtils8ToStringEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE:
.LFB20753:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L1483
.L1486:
	leaq	.LC393(%rip), %rax
	xorl	%edx, %edx
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	movq	$24, -72(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L1518
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	xorl	%eax, %eax
.L1489:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1519
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1483:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L1486
	leaq	2872(%rdi), %rdx
	leaq	2480(%rdi), %rcx
	call	_ZN2v88internal12_GLOBAL__N_126GetStringPropertyOrDefaultEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_6StringEEES8_
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1517
	movq	%r13, %rsi
	leaq	2832(%r12), %rdx
	leaq	128(%r12), %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_126GetStringPropertyOrDefaultEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_6StringEEES8_
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1517
	movq	(%r14), %rax
	movl	11(%rax), %ecx
	movq	%r13, %rax
	testl	%ecx, %ecx
	je	.L1489
	movq	0(%r13), %rax
	movl	11(%rax), %edx
	movq	%r14, %rax
	testl	%edx, %edx
	je	.L1489
	leaq	-80(%rbp), %r15
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilderC1EPNS0_7IsolateE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	movl	-72(%rbp), %eax
	testl	%eax, %eax
	je	.L1502
	movl	-60(%rbp), %eax
	movl	$58, %edx
	leaq	.LC392(%rip), %r12
	.p2align 4,,10
	.p2align 3
.L1494:
	movq	-48(%rbp), %rcx
	leal	1(%rax), %esi
	leal	16(%rax,%rax), %eax
	addq	$1, %r12
	cltq
	movq	(%rcx), %rcx
	movl	%esi, -60(%rbp)
	movw	%dx, -1(%rcx,%rax)
	movl	-60(%rbp), %eax
	cmpl	-64(%rbp), %eax
	je	.L1520
	movzbl	(%r12), %edx
	testb	%dl, %dl
	jne	.L1494
.L1495:
	movq	%r15, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6FinishEv@PLT
	jmp	.L1489
	.p2align 4,,10
	.p2align 3
.L1518:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1517:
	xorl	%eax, %eax
	jmp	.L1489
	.p2align 4,,10
	.p2align 3
.L1520:
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%r12), %edx
	testb	%dl, %dl
	je	.L1495
	movl	-60(%rbp), %eax
	jmp	.L1494
.L1502:
	movl	$58, %edx
	leaq	.LC392(%rip), %r12
.L1516:
	movl	-60(%rbp), %eax
.L1493:
	movq	-48(%rbp), %rcx
	leal	1(%rax), %esi
	addl	$16, %eax
	addq	$1, %r12
	cltq
	movq	(%rcx), %rcx
	movl	%esi, -60(%rbp)
	movb	%dl, -1(%rcx,%rax)
	movl	-60(%rbp), %eax
	cmpl	-64(%rbp), %eax
	je	.L1521
	movzbl	(%r12), %edx
	testb	%dl, %dl
	jne	.L1493
	jmp	.L1495
	.p2align 4,,10
	.p2align 3
.L1521:
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%r12), %edx
	testb	%dl, %dl
	jne	.L1516
	jmp	.L1495
.L1519:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20753:
	.size	_ZN2v88internal10ErrorUtils8ToStringEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE, .-_ZN2v88internal10ErrorUtils8ToStringEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	.section	.rodata._ZN2v88internal10ErrorUtils16FormatStackTraceEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6ObjectEEE.str1.1,"aMS",@progbits,1
.LC394:
	.string	"<error: "
.LC395:
	.string	"\n    at "
.LC396:
	.string	"prepareStackTrace"
.LC397:
	.string	"NewArray"
	.section	.text._ZN2v88internal10ErrorUtils16FormatStackTraceEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10ErrorUtils16FormatStackTraceEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6ObjectEEE
	.type	_ZN2v88internal10ErrorUtils16FormatStackTraceEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6ObjectEEE, @function
_ZN2v88internal10ErrorUtils16FormatStackTraceEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6ObjectEEE:
.LFB20742:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$184, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 41824(%rdi)
	jne	.L1523
	movq	(%rsi), %rax
	movq	%rax, -144(%rbp)
	leaq	-144(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal10JSReceiver18GetCreationContextEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %r12
	call	_ZNK2v88internal7Isolate28HasPrepareStackTraceCallbackEv@PLT
	testb	%al, %al
	je	.L1524
	movb	$1, 41824(%rbx)
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal12_GLOBAL__N_114GetStackFramesEPNS0_7IsolateENS0_6HandleINS0_10FixedArrayEEE
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1695
	movq	%r12, %rsi
	movq	%r13, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal7Isolate28RunPrepareStackTraceCallbackENS0_6HandleINS0_7ContextEEENS2_INS0_8JSObjectEEENS2_INS0_7JSArrayEEE@PLT
	movq	%rax, %r12
.L1539:
	movb	$0, 41824(%rbx)
	jmp	.L1528
	.p2align 4,,10
	.p2align 3
.L1524:
	movq	(%r12), %rax
	movq	1607(%rax), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1529
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r12
.L1530:
	movq	-216(%rbp), %rsi
	movq	%rbx, %rdi
	leaq	.LC396(%rip), %rax
	movq	$17, -136(%rbp)
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	(%rax), %rdx
	movq	%rax, %rsi
	movl	$3, %eax
	movq	-1(%rdx), %rcx
	cmpw	$64, 11(%rcx)
	jne	.L1532
	movl	11(%rdx), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$3, %eax
.L1532:
	movl	%eax, -144(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -132(%rbp)
	movq	%rbx, -120(%rbp)
	movq	(%rsi), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L1697
.L1533:
	movq	-216(%rbp), %rdi
	movq	%rsi, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%r12, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%r12, -80(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -140(%rbp)
	jne	.L1534
	movq	-120(%rbp), %rax
	leaq	88(%rax), %r15
.L1535:
	movq	(%r15), %rax
	testb	$1, %al
	je	.L1523
	movq	-1(%rax), %rax
	cmpw	$1105, 11(%rax)
	je	.L1698
	.p2align 4,,10
	.p2align 3
.L1523:
	leaq	-192(%rbp), %r12
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilderC1EPNS0_7IsolateE@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal10ErrorUtils8ToStringEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1699
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
.L1566:
	testq	%r13, %r13
	je	.L1567
	leaq	-144(%rbp), %rax
	xorl	%r13d, %r13d
	movq	%rax, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal4wasm16WasmCodeRefScopeC1Ev@PLT
	movq	(%r14), %rax
	movl	11(%rax), %edi
	testl	%edi, %edi
	jle	.L1605
	.p2align 4,,10
	.p2align 3
.L1604:
	movl	-184(%rbp), %esi
	testl	%esi, %esi
	je	.L1613
	movl	-172(%rbp), %edx
	movl	$10, %eax
	leaq	.LC395(%rip), %r15
	.p2align 4,,10
	.p2align 3
.L1570:
	movq	-160(%rbp), %rsi
	leal	1(%rdx), %edi
	leal	16(%rdx,%rdx), %edx
	addq	$1, %r15
	movslq	%edx, %rdx
	movq	(%rsi), %rsi
	movl	%edi, -172(%rbp)
	movw	%ax, -1(%rsi,%rdx)
	movl	-172(%rbp), %edx
	cmpl	-176(%rbp), %edx
	je	.L1700
	movzbl	(%r15), %eax
	testb	%al, %al
	jne	.L1570
.L1571:
	movq	(%r14), %rax
	movq	15(%rax,%r13,8), %r8
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1576
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1577:
	movq	%r12, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal24SerializeStackTraceFrameEPNS0_7IsolateENS0_6HandleINS0_15StackTraceFrameEEERNS0_24IncrementalStringBuilderE@PLT
	movq	12480(%rbx), %r8
	cmpq	%r8, 96(%rbx)
	je	.L1579
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1580
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1581:
	movq	96(%rbx), %rax
	movb	$0, 12545(%rbx)
	movq	%rbx, %rdi
	movq	%rax, 12480(%rbx)
	call	_ZN2v88internal10ErrorUtils8ToStringEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1701
	movl	-184(%rbp), %edx
	testl	%edx, %edx
	je	.L1615
	movl	-172(%rbp), %edx
	movl	$60, %ecx
	leaq	.LC394(%rip), %rax
	.p2align 4,,10
	.p2align 3
.L1592:
	movq	-160(%rbp), %rdi
	leal	1(%rdx), %r8d
	leal	16(%rdx,%rdx), %edx
	addq	$1, %rax
	movslq	%edx, %rdx
	movq	(%rdi), %rdi
	movl	%r8d, -172(%rbp)
	movw	%cx, -1(%rdi,%rdx)
	movl	-172(%rbp), %edx
	cmpl	-176(%rbp), %edx
	je	.L1702
	movzbl	(%rax), %ecx
	testb	%cl, %cl
	jne	.L1592
.L1593:
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	movl	-184(%rbp), %eax
	testl	%eax, %eax
	je	.L1616
	movl	-172(%rbp), %edx
	movl	$60, %eax
	leaq	.LC387(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L1599:
	movq	-160(%rbp), %rsi
	leal	1(%rdx), %edi
	leal	16(%rdx,%rdx), %edx
	addq	$1, %rcx
	movslq	%edx, %rdx
	movq	(%rsi), %rsi
	movl	%edi, -172(%rbp)
	movw	%ax, -1(%rsi,%rdx)
	movl	-172(%rbp), %edx
	cmpl	-176(%rbp), %edx
	je	.L1703
	movzbl	(%rcx), %eax
	testb	%al, %al
	jne	.L1599
	.p2align 4,,10
	.p2align 3
.L1579:
	movq	(%r14), %rax
	addq	$1, %r13
	cmpl	%r13d, 11(%rax)
	jg	.L1604
.L1605:
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6FinishEv@PLT
	movq	-216(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal4wasm16WasmCodeRefScopeD1Ev@PLT
.L1528:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1704
	addq	$184, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1699:
	.cfi_restore_state
	movq	41112(%rbx), %rdi
	movq	12480(%rbx), %r15
	testq	%rdi, %rdi
	je	.L1545
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1546:
	movq	96(%rbx), %rax
	movb	$0, 12545(%rbx)
	movq	%rbx, %rdi
	movq	%rax, 12480(%rbx)
	call	_ZN2v88internal10ErrorUtils8ToStringEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1705
	movl	-184(%rbp), %r10d
	testl	%r10d, %r10d
	je	.L1612
	movl	-172(%rbp), %edx
	movl	$60, %ecx
	leaq	.LC394(%rip), %r15
	.p2align 4,,10
	.p2align 3
.L1558:
	movq	-160(%rbp), %rax
	leal	1(%rdx), %edi
	leal	16(%rdx,%rdx), %edx
	addq	$1, %r15
	movslq	%edx, %rdx
	movq	(%rax), %rax
	movl	%edi, -172(%rbp)
	movw	%cx, -1(%rax,%rdx)
	movl	-172(%rbp), %edx
	cmpl	-176(%rbp), %edx
	je	.L1706
	movzbl	(%r15), %ecx
	testb	%cl, %cl
	jne	.L1558
.L1559:
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	movq	-160(%rbp), %rax
	movl	-184(%rbp), %r9d
	movq	(%rax), %rdx
	movl	-172(%rbp), %eax
	testl	%r9d, %r9d
	leal	1(%rax), %ecx
	movl	%ecx, -172(%rbp)
	jne	.L1564
	addl	$16, %eax
	cltq
	movb	$62, -1(%rdx,%rax)
	movl	-176(%rbp), %eax
	cmpl	%eax, -172(%rbp)
	jne	.L1566
.L1565:
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L1566
	.p2align 4,,10
	.p2align 3
.L1700:
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%r15), %eax
	testb	%al, %al
	je	.L1571
	movl	-172(%rbp), %edx
	jmp	.L1570
	.p2align 4,,10
	.p2align 3
.L1613:
	movl	$10, %ecx
	leaq	.LC395(%rip), %r15
.L1691:
	movl	-172(%rbp), %edx
.L1569:
	movq	-160(%rbp), %rsi
	leal	1(%rdx), %edi
	addl	$16, %edx
	addq	$1, %r15
	movslq	%edx, %rdx
	movq	(%rsi), %rsi
	movl	%edi, -172(%rbp)
	movb	%cl, -1(%rsi,%rdx)
	movl	-172(%rbp), %edx
	cmpl	-176(%rbp), %edx
	je	.L1707
	movzbl	(%r15), %ecx
	testb	%cl, %cl
	jne	.L1569
	jmp	.L1571
	.p2align 4,,10
	.p2align 3
.L1707:
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%r15), %ecx
	testb	%cl, %cl
	jne	.L1691
	jmp	.L1571
	.p2align 4,,10
	.p2align 3
.L1576:
	movq	41088(%rbx), %rsi
	cmpq	41096(%rbx), %rsi
	je	.L1708
.L1578:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rbx)
	movq	%r8, (%rsi)
	jmp	.L1577
	.p2align 4,,10
	.p2align 3
.L1702:
	movq	%r12, %rdi
	movq	%rsi, -208(%rbp)
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movq	-200(%rbp), %rax
	movq	-208(%rbp), %rsi
	movzbl	(%rax), %ecx
	testb	%cl, %cl
	je	.L1593
	movl	-172(%rbp), %edx
	jmp	.L1592
	.p2align 4,,10
	.p2align 3
.L1703:
	movq	%r12, %rdi
	movq	%rcx, -200(%rbp)
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movq	-200(%rbp), %rcx
	movzbl	(%rcx), %eax
	testb	%al, %al
	je	.L1579
	movl	-172(%rbp), %edx
	jmp	.L1599
	.p2align 4,,10
	.p2align 3
.L1580:
	movq	41088(%rbx), %rsi
	cmpq	41096(%rbx), %rsi
	je	.L1709
.L1582:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rbx)
	movq	%r8, (%rsi)
	jmp	.L1581
	.p2align 4,,10
	.p2align 3
.L1616:
	movl	$60, %ecx
	leaq	.LC387(%rip), %rax
.L1694:
	movl	-172(%rbp), %edx
.L1598:
	movq	-160(%rbp), %rsi
	leal	1(%rdx), %edi
	addl	$16, %edx
	addq	$1, %rax
	movslq	%edx, %rdx
	movq	(%rsi), %rsi
	movl	%edi, -172(%rbp)
	movb	%cl, -1(%rsi,%rdx)
	movl	-172(%rbp), %edx
	cmpl	-176(%rbp), %edx
	je	.L1710
	movzbl	(%rax), %ecx
	testb	%cl, %cl
	jne	.L1598
	jmp	.L1579
	.p2align 4,,10
	.p2align 3
.L1710:
	movq	%r12, %rdi
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movq	-200(%rbp), %rax
	movzbl	(%rax), %ecx
	testb	%cl, %cl
	jne	.L1694
	movq	(%r14), %rax
	addq	$1, %r13
	cmpl	%r13d, 11(%rax)
	jg	.L1604
	jmp	.L1605
	.p2align 4,,10
	.p2align 3
.L1615:
	movl	$60, %ecx
	leaq	.LC394(%rip), %rax
.L1693:
	movl	-172(%rbp), %edx
.L1591:
	movq	-160(%rbp), %rdi
	leal	1(%rdx), %r8d
	addl	$16, %edx
	addq	$1, %rax
	movslq	%edx, %rdx
	movq	(%rdi), %rdi
	movl	%r8d, -172(%rbp)
	movb	%cl, -1(%rdi,%rdx)
	movl	-172(%rbp), %edx
	cmpl	-176(%rbp), %edx
	je	.L1711
	movzbl	(%rax), %ecx
	testb	%cl, %cl
	jne	.L1591
	jmp	.L1593
	.p2align 4,,10
	.p2align 3
.L1711:
	movq	%r12, %rdi
	movq	%rsi, -208(%rbp)
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movq	-200(%rbp), %rax
	movq	-208(%rbp), %rsi
	movzbl	(%rax), %ecx
	testb	%cl, %cl
	jne	.L1693
	jmp	.L1593
	.p2align 4,,10
	.p2align 3
.L1701:
	movl	-184(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L1614
	movl	-172(%rbp), %edx
	movl	$60, %eax
	leaq	.LC387(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L1585:
	movq	-160(%rbp), %rsi
	leal	1(%rdx), %edi
	leal	16(%rdx,%rdx), %edx
	addq	$1, %rcx
	movslq	%edx, %rdx
	movq	(%rsi), %rsi
	movl	%edi, -172(%rbp)
	movw	%ax, -1(%rsi,%rdx)
	movl	-172(%rbp), %edx
	cmpl	-176(%rbp), %edx
	je	.L1712
	movzbl	(%rcx), %eax
	testb	%al, %al
	jne	.L1585
	jmp	.L1579
	.p2align 4,,10
	.p2align 3
.L1712:
	movq	%r12, %rdi
	movq	%rcx, -200(%rbp)
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movq	-200(%rbp), %rcx
	movzbl	(%rcx), %eax
	testb	%al, %al
	je	.L1579
	movl	-172(%rbp), %edx
	jmp	.L1585
.L1534:
	movq	-216(%rbp), %rdi
	movl	$1, %esi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L1535
	.p2align 4,,10
	.p2align 3
.L1567:
	xorl	%r12d, %r12d
	jmp	.L1528
	.p2align 4,,10
	.p2align 3
.L1614:
	movl	$60, %ecx
	leaq	.LC387(%rip), %rax
.L1692:
	movl	-172(%rbp), %edx
.L1584:
	movq	-160(%rbp), %rsi
	leal	1(%rdx), %edi
	addl	$16, %edx
	addq	$1, %rax
	movslq	%edx, %rdx
	movq	(%rsi), %rsi
	movl	%edi, -172(%rbp)
	movb	%cl, -1(%rsi,%rdx)
	movl	-172(%rbp), %edx
	cmpl	-176(%rbp), %edx
	je	.L1713
	movzbl	(%rax), %ecx
	testb	%cl, %cl
	jne	.L1584
	jmp	.L1579
	.p2align 4,,10
	.p2align 3
.L1713:
	movq	%r12, %rdi
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movq	-200(%rbp), %rax
	movzbl	(%rax), %ecx
	testb	%cl, %cl
	jne	.L1692
	jmp	.L1579
	.p2align 4,,10
	.p2align 3
.L1708:
	movq	%rbx, %rdi
	movq	%r8, -200(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-200(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L1578
	.p2align 4,,10
	.p2align 3
.L1545:
	movq	41088(%rbx), %rsi
	cmpq	41096(%rbx), %rsi
	je	.L1714
.L1547:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rbx)
	movq	%r15, (%rsi)
	jmp	.L1546
.L1529:
	movq	41088(%rbx), %r12
	cmpq	41096(%rbx), %r12
	je	.L1715
.L1531:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r12)
	jmp	.L1530
	.p2align 4,,10
	.p2align 3
.L1706:
	movq	%r12, %rdi
	movq	%rsi, -200(%rbp)
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%r15), %ecx
	movq	-200(%rbp), %rsi
	testb	%cl, %cl
	je	.L1559
	movl	-172(%rbp), %edx
	jmp	.L1558
	.p2align 4,,10
	.p2align 3
.L1709:
	movq	%rbx, %rdi
	movq	%r8, -200(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-200(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L1582
.L1612:
	movl	$60, %eax
	leaq	.LC394(%rip), %r15
.L1690:
	movl	-172(%rbp), %edx
.L1557:
	movq	-160(%rbp), %rcx
	leal	1(%rdx), %edi
	addl	$16, %edx
	addq	$1, %r15
	movslq	%edx, %rdx
	movq	(%rcx), %rcx
	movl	%edi, -172(%rbp)
	movb	%al, -1(%rcx,%rdx)
	movl	-172(%rbp), %edx
	cmpl	-176(%rbp), %edx
	je	.L1716
	movzbl	(%r15), %eax
	testb	%al, %al
	jne	.L1557
	jmp	.L1559
	.p2align 4,,10
	.p2align 3
.L1716:
	movq	%r12, %rdi
	movq	%rsi, -200(%rbp)
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%r15), %eax
	movq	-200(%rbp), %rsi
	testb	%al, %al
	jne	.L1690
	jmp	.L1559
.L1564:
	leal	16(%rax,%rax), %eax
	movl	$62, %r8d
	cltq
	movw	%r8w, -1(%rdx,%rax)
	movl	-176(%rbp), %eax
	cmpl	%eax, -172(%rbp)
	jne	.L1566
	jmp	.L1565
.L1705:
	movq	96(%rbx), %rax
	movl	-184(%rbp), %r11d
	movb	$0, 12545(%rbx)
	movq	%rax, 12480(%rbx)
	testl	%r11d, %r11d
	jne	.L1717
	movl	$60, %eax
	leaq	.LC387(%rip), %r15
.L1689:
	movl	-172(%rbp), %edx
.L1549:
	movq	-160(%rbp), %rcx
	leal	1(%rdx), %esi
	addl	$16, %edx
	addq	$1, %r15
	movslq	%edx, %rdx
	movq	(%rcx), %rcx
	movl	%esi, -172(%rbp)
	movb	%al, -1(%rcx,%rdx)
	movl	-172(%rbp), %edx
	cmpl	-176(%rbp), %edx
	je	.L1718
	movzbl	(%r15), %eax
	testb	%al, %al
	jne	.L1549
	jmp	.L1566
.L1697:
	movq	%rbx, %rdi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rsi
	jmp	.L1533
.L1717:
	movl	-172(%rbp), %edx
	movl	$60, %eax
	leaq	.LC387(%rip), %r15
	.p2align 4,,10
	.p2align 3
.L1550:
	movq	-160(%rbp), %rcx
	leal	1(%rdx), %esi
	leal	16(%rdx,%rdx), %edx
	addq	$1, %r15
	movslq	%edx, %rdx
	movq	(%rcx), %rcx
	movl	%esi, -172(%rbp)
	movw	%ax, -1(%rcx,%rdx)
	movl	-172(%rbp), %edx
	cmpl	-176(%rbp), %edx
	je	.L1719
	movzbl	(%r15), %eax
	testb	%al, %al
	jne	.L1550
	jmp	.L1566
.L1698:
	movb	$1, 41824(%rbx)
	movl	$44, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Isolate10CountUsageENS_7Isolate17UseCounterFeatureE@PLT
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal12_GLOBAL__N_114GetStackFramesEPNS0_7IsolateENS0_6HandleINS0_10FixedArrayEEE
	movq	%rax, -200(%rbp)
	testq	%rax, %rax
	je	.L1695
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$16, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1540
.L1606:
	pxor	%xmm0, %xmm0
	movq	%r12, %rdx
	movq	%rbx, %rdi
	movq	%r14, %r8
	movups	%xmm0, (%r14)
	movq	%r13, %xmm0
	movl	$2, %ecx
	movq	%r15, %rsi
	movhps	-200(%rbp), %xmm0
	movups	%xmm0, (%r14)
	call	_ZN2v88internal9Execution4CallEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_iPS6_@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZdaPv@PLT
	jmp	.L1539
.L1718:
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%r15), %eax
	testb	%al, %al
	jne	.L1689
	jmp	.L1566
.L1719:
	movq	%r12, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%r15), %eax
	testb	%al, %al
	je	.L1566
	movl	-172(%rbp), %edx
	jmp	.L1550
.L1695:
	xorl	%r12d, %r12d
	jmp	.L1539
.L1714:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L1547
.L1715:
	movq	%rbx, %rdi
	movq	%rsi, -200(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-200(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L1531
.L1704:
	call	__stack_chk_fail@PLT
.L1540:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$16, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	jne	.L1606
	leaq	.LC397(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
	.cfi_endproc
.LFE20742:
	.size	_ZN2v88internal10ErrorUtils16FormatStackTraceEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6ObjectEEE, .-_ZN2v88internal10ErrorUtils16FormatStackTraceEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6ObjectEEE
	.section	.rodata._ZN2v88internal10ErrorUtils16MakeGenericErrorEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEENS0_15MessageTemplateENS4_INS0_6ObjectEEES9_S9_NS0_13FrameSkipModeE.str1.8,"aMS",@progbits,1
	.align 8
.LC398:
	.string	"Message suppressed for fuzzers (--correctness-fuzzer-suppressions)"
	.section	.text._ZN2v88internal10ErrorUtils16MakeGenericErrorEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEENS0_15MessageTemplateENS4_INS0_6ObjectEEES9_S9_NS0_13FrameSkipModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10ErrorUtils16MakeGenericErrorEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEENS0_15MessageTemplateENS4_INS0_6ObjectEEES9_S9_NS0_13FrameSkipModeE
	.type	_ZN2v88internal10ErrorUtils16MakeGenericErrorEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEENS0_15MessageTemplateENS4_INS0_6ObjectEEES9_S9_NS0_13FrameSkipModeE, @function
_ZN2v88internal10ErrorUtils16MakeGenericErrorEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEENS0_15MessageTemplateENS4_INS0_6ObjectEEES9_S9_NS0_13FrameSkipModeE:
.LFB20755:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movq	%rcx, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal33FLAG_clear_exceptions_on_js_entryE(%rip)
	je	.L1721
	movq	96(%rdi), %rax
	movq	%rax, 12480(%rdi)
.L1721:
	cmpb	$0, _ZN2v88internal36FLAG_correctness_fuzzer_suppressionsE(%rip)
	je	.L1722
	leaq	.LC398(%rip), %rax
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	$66, -72(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	%rax, %rcx
.L1723:
	subq	$8, %rsp
	movl	16(%rbp), %r8d
	xorl	%r9d, %r9d
	movq	%r13, %rdx
	pushq	$0
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10ErrorUtils9ConstructEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEENS4_INS0_6ObjectEEES8_NS0_13FrameSkipModeES8_NS1_20StackTraceCollectionE
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1732
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1722:
	.cfi_restore_state
	movq	%r12, %rdi
	movq	%r9, -96(%rbp)
	call	_ZN2v88internal6Object21NoSideEffectsToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal6Object21NoSideEffectsToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	-96(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, -88(%rbp)
	movq	%r9, %rsi
	call	_ZN2v88internal6Object21NoSideEffectsToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rbx
	movq	12464(%r12), %rax
	movq	39(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1724
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L1725:
	leaq	-80(%rbp), %r9
	movq	%rsi, -80(%rbp)
	movq	%r9, %rdi
	movq	%r9, -96(%rbp)
	call	_ZN2v88internal13NativeContext21IncrementErrorsThrownEv@PLT
	movq	-88(%rbp), %rcx
	movq	%rbx, %r8
	movq	%r15, %rdx
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal16MessageFormatter6FormatEPNS0_7IsolateENS0_15MessageTemplateENS0_6HandleINS0_6StringEEES7_S7_
	movq	-96(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rcx
	jne	.L1723
	movq	96(%r12), %rax
	xorl	%edx, %edx
	movq	%r9, %rsi
	movq	%r12, %rdi
	movb	$0, 12545(%r12)
	movq	%rax, 12480(%r12)
	leaq	.LC387(%rip), %rax
	movq	%rax, -80(%rbp)
	movq	$7, -72(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	jne	.L1723
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1724:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L1733
.L1726:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L1725
	.p2align 4,,10
	.p2align 3
.L1733:
	movq	%r12, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rsi
	jmp	.L1726
.L1732:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20755:
	.size	_ZN2v88internal10ErrorUtils16MakeGenericErrorEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEENS0_15MessageTemplateENS4_INS0_6ObjectEEES9_S9_NS0_13FrameSkipModeE, .-_ZN2v88internal10ErrorUtils16MakeGenericErrorEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEENS0_15MessageTemplateENS4_INS0_6ObjectEEES9_S9_NS0_13FrameSkipModeE
	.section	.text._ZN2v88internal10ErrorUtils16NewIteratorErrorEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10ErrorUtils16NewIteratorErrorEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	.type	_ZN2v88internal10ErrorUtils16NewIteratorErrorEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE, @function
_ZN2v88internal10ErrorUtils16NewIteratorErrorEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE:
.LFB20772:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	leaq	-400(%rbp), %rsi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$376, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -400(%rbp)
	movq	$-1, -392(%rbp)
	movl	$-1, -384(%rbp)
	movq	$0, -376(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_115ComputeLocationEPNS0_7IsolateEPNS0_15MessageLocationE.constprop.0
	testb	%al, %al
	jne	.L1735
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_120BuildDefaultCallSiteEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	movq	%rax, %rdx
.L1736:
	leaq	3856(%r12), %rcx
	xorl	%r8d, %r8d
	movl	$97, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
.L1745:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1751
	addq	$376, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1735:
	.cfi_restore_state
	movq	-376(%rbp), %rdx
	leaq	-288(%rbp), %r15
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal9ParseInfoC1EPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEE@PLT
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movq	%r15, %rdi
	movq	-376(%rbp), %rsi
	call	_ZN2v88internal7parsing8ParseAnyEPNS0_9ParseInfoENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateENS1_29ReportErrorsAndStatisticsModeE@PLT
	testb	%al, %al
	jne	.L1752
	movq	96(%r12), %rax
	xorl	%ebx, %ebx
	movq	%rax, 12480(%r12)
.L1743:
	movq	%r15, %rdi
	call	_ZN2v88internal9ParseInfoD1Ev@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_120BuildDefaultCallSiteEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	movq	%rax, %rdx
.L1742:
	testl	%ebx, %ebx
	je	.L1736
	leal	-1(%rbx), %ecx
	cmpl	$3, %ecx
	ja	.L1749
	leaq	CSWTCH.4398(%rip), %rax
	movl	(%rax,%rcx,4), %esi
.L1746:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	jmp	.L1745
	.p2align 4,,10
	.p2align 3
.L1752:
	movq	-176(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal15AstValueFactory11InternalizeEPNS0_7IsolateE@PLT
	movq	-376(%rbp), %rax
	movq	(%rax), %rax
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L1753
.L1738:
	leaq	-368(%rbp), %r13
	movq	%rax, -368(%rbp)
	movq	%r13, %rdi
	call	_ZN2v88internal6Script16IsUserJavaScriptEv@PLT
	movzbl	%al, %edx
.L1740:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal11CallPrinterC1EPNS0_7IsolateEb@PLT
	movl	-392(%rbp), %edx
	movq	-120(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal11CallPrinter5PrintEPNS0_15FunctionLiteralEi@PLT
	movq	%r13, %rdi
	movq	%rax, -408(%rbp)
	call	_ZNK2v88internal11CallPrinter12GetErrorHintEv@PLT
	movq	-408(%rbp), %rdx
	movq	%r13, %rdi
	movl	%eax, %ebx
	movq	(%rdx), %rax
	movl	11(%rax), %eax
	testl	%eax, %eax
	jle	.L1741
	call	_ZN2v88internal11CallPrinterD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal9ParseInfoD1Ev@PLT
	movq	-408(%rbp), %rdx
	jmp	.L1742
	.p2align 4,,10
	.p2align 3
.L1749:
	movl	$97, %esi
	jmp	.L1746
	.p2align 4,,10
	.p2align 3
.L1741:
	call	_ZN2v88internal11CallPrinterD1Ev@PLT
	jmp	.L1743
	.p2align 4,,10
	.p2align 3
.L1753:
	movq	-1(%rax), %rdx
	cmpw	$86, 11(%rdx)
	je	.L1754
.L1739:
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	%rax, -37504(%rdx)
	jne	.L1738
	xorl	%edx, %edx
	leaq	-368(%rbp), %r13
	jmp	.L1740
	.p2align 4,,10
	.p2align 3
.L1754:
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L1739
	jmp	.L1738
.L1751:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20772:
	.size	_ZN2v88internal10ErrorUtils16NewIteratorErrorEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE, .-_ZN2v88internal10ErrorUtils16NewIteratorErrorEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	.section	.text._ZN2v88internal10ErrorUtils25NewCalledNonCallableErrorEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10ErrorUtils25NewCalledNonCallableErrorEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	.type	_ZN2v88internal10ErrorUtils25NewCalledNonCallableErrorEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE, @function
_ZN2v88internal10ErrorUtils25NewCalledNonCallableErrorEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE:
.LFB20773:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	leaq	-400(%rbp), %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$376, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -400(%rbp)
	movq	$-1, -392(%rbp)
	movl	$-1, -384(%rbp)
	movq	$0, -376(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_115ComputeLocationEPNS0_7IsolateEPNS0_15MessageLocationE.constprop.0
	testb	%al, %al
	jne	.L1756
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_120BuildDefaultCallSiteEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	movl	$25, %esi
	movq	%rax, %rdx
.L1765:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1770
	addq	$376, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1756:
	.cfi_restore_state
	movq	-376(%rbp), %rdx
	leaq	-288(%rbp), %r14
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal9ParseInfoC1EPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEE@PLT
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	-376(%rbp), %rsi
	call	_ZN2v88internal7parsing8ParseAnyEPNS0_9ParseInfoENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateENS1_29ReportErrorsAndStatisticsModeE@PLT
	testb	%al, %al
	jne	.L1771
	movq	96(%r12), %rax
	xorl	%r15d, %r15d
	movq	%rax, 12480(%r12)
.L1764:
	movq	%r14, %rdi
	call	_ZN2v88internal9ParseInfoD1Ev@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_120BuildDefaultCallSiteEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	movq	%rax, %rdx
.L1763:
	movl	$25, %esi
	cmpl	$4, %r15d
	ja	.L1765
	leaq	CSWTCH.4401(%rip), %rax
	movl	(%rax,%r15,4), %esi
	jmp	.L1765
	.p2align 4,,10
	.p2align 3
.L1771:
	movq	-176(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal15AstValueFactory11InternalizeEPNS0_7IsolateE@PLT
	movq	-376(%rbp), %rax
	movq	(%rax), %rax
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L1772
.L1759:
	leaq	-368(%rbp), %rbx
	movq	%rax, -368(%rbp)
	movq	%rbx, %rdi
	call	_ZN2v88internal6Script16IsUserJavaScriptEv@PLT
	movzbl	%al, %edx
.L1761:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal11CallPrinterC1EPNS0_7IsolateEb@PLT
	movl	-392(%rbp), %edx
	movq	-120(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal11CallPrinter5PrintEPNS0_15FunctionLiteralEi@PLT
	movq	%rbx, %rdi
	movq	%rax, -408(%rbp)
	call	_ZNK2v88internal11CallPrinter12GetErrorHintEv@PLT
	movq	-408(%rbp), %rdx
	movq	%rbx, %rdi
	movl	%eax, %r15d
	movq	(%rdx), %rax
	movl	11(%rax), %eax
	testl	%eax, %eax
	jle	.L1762
	call	_ZN2v88internal11CallPrinterD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal9ParseInfoD1Ev@PLT
	movq	-408(%rbp), %rdx
	jmp	.L1763
	.p2align 4,,10
	.p2align 3
.L1762:
	call	_ZN2v88internal11CallPrinterD1Ev@PLT
	jmp	.L1764
	.p2align 4,,10
	.p2align 3
.L1772:
	movq	-1(%rax), %rdx
	cmpw	$86, 11(%rdx)
	je	.L1773
.L1760:
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	-37504(%rdx), %rax
	jne	.L1759
	xorl	%edx, %edx
	leaq	-368(%rbp), %rbx
	jmp	.L1761
	.p2align 4,,10
	.p2align 3
.L1773:
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L1760
	jmp	.L1759
.L1770:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20773:
	.size	_ZN2v88internal10ErrorUtils25NewCalledNonCallableErrorEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE, .-_ZN2v88internal10ErrorUtils25NewCalledNonCallableErrorEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	.section	.text._ZN2v88internal10ErrorUtils30NewConstructedNonConstructableEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10ErrorUtils30NewConstructedNonConstructableEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	.type	_ZN2v88internal10ErrorUtils30NewConstructedNonConstructableEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE, @function
_ZN2v88internal10ErrorUtils30NewConstructedNonConstructableEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE:
.LFB20774:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	leaq	-400(%rbp), %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$376, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -400(%rbp)
	movq	$-1, -392(%rbp)
	movl	$-1, -384(%rbp)
	movq	$0, -376(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_115ComputeLocationEPNS0_7IsolateEPNS0_15MessageLocationE.constprop.0
	testb	%al, %al
	je	.L1775
	movq	-376(%rbp), %rdx
	leaq	-288(%rbp), %r14
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal9ParseInfoC1EPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEE@PLT
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	-376(%rbp), %rsi
	call	_ZN2v88internal7parsing8ParseAnyEPNS0_9ParseInfoENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateENS1_29ReportErrorsAndStatisticsModeE@PLT
	testb	%al, %al
	jne	.L1789
	movq	96(%r12), %rax
	movq	%rax, 12480(%r12)
.L1782:
	movq	%r14, %rdi
	call	_ZN2v88internal9ParseInfoD1Ev@PLT
.L1775:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_120BuildDefaultCallSiteEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	movq	%rax, %rdx
.L1781:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$90, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1790
	addq	$376, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1789:
	.cfi_restore_state
	movq	-176(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal15AstValueFactory11InternalizeEPNS0_7IsolateE@PLT
	movq	-376(%rbp), %rax
	movq	(%rax), %rax
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L1791
.L1777:
	leaq	-368(%rbp), %r15
	movq	%rax, -368(%rbp)
	movq	%r15, %rdi
	call	_ZN2v88internal6Script16IsUserJavaScriptEv@PLT
	movzbl	%al, %edx
.L1779:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal11CallPrinterC1EPNS0_7IsolateEb@PLT
	movl	-392(%rbp), %edx
	movq	-120(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal11CallPrinter5PrintEPNS0_15FunctionLiteralEi@PLT
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZNK2v88internal11CallPrinter12GetErrorHintEv@PLT
	movq	(%rbx), %rax
	movq	%rbx, -408(%rbp)
	movq	%r15, %rdi
	movl	11(%rax), %eax
	testl	%eax, %eax
	jle	.L1780
	call	_ZN2v88internal11CallPrinterD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal9ParseInfoD1Ev@PLT
	movq	-408(%rbp), %rdx
	jmp	.L1781
	.p2align 4,,10
	.p2align 3
.L1780:
	call	_ZN2v88internal11CallPrinterD1Ev@PLT
	jmp	.L1782
	.p2align 4,,10
	.p2align 3
.L1791:
	movq	-1(%rax), %rdx
	cmpw	$86, 11(%rdx)
	je	.L1792
.L1778:
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	%rax, -37504(%rdx)
	jne	.L1777
	xorl	%edx, %edx
	leaq	-368(%rbp), %r15
	jmp	.L1779
	.p2align 4,,10
	.p2align 3
.L1792:
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L1778
	jmp	.L1777
.L1790:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20774:
	.size	_ZN2v88internal10ErrorUtils30NewConstructedNonConstructableEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE, .-_ZN2v88internal10ErrorUtils30NewConstructedNonConstructableEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	.section	.text.unlikely._ZN2v88internal10ErrorUtils28ThrowLoadFromNullOrUndefinedEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS0_11MaybeHandleIS5_EE,"ax",@progbits
	.align 2
.LCOLDB399:
	.section	.text._ZN2v88internal10ErrorUtils28ThrowLoadFromNullOrUndefinedEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS0_11MaybeHandleIS5_EE,"ax",@progbits
.LHOTB399:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10ErrorUtils28ThrowLoadFromNullOrUndefinedEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS0_11MaybeHandleIS5_EE
	.type	_ZN2v88internal10ErrorUtils28ThrowLoadFromNullOrUndefinedEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS0_11MaybeHandleIS5_EE, @function
_ZN2v88internal10ErrorUtils28ThrowLoadFromNullOrUndefinedEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS0_11MaybeHandleIS5_EE:
.LFB20776:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$408, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L1818
	movq	(%rdx), %rax
	testb	$1, %al
	jne	.L1838
.L1818:
	movq	$0, -408(%rbp)
.L1794:
	leaq	-400(%rbp), %r14
	movq	%r15, %rdi
	movq	$0, -400(%rbp)
	movq	$-1, -392(%rbp)
	movq	%r14, %rsi
	movl	$-1, -384(%rbp)
	movq	$0, -376(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_115ComputeLocationEPNS0_7IsolateEPNS0_15MessageLocationE.constprop.0
	xorl	%ecx, %ecx
	movl	%eax, %r12d
	testb	%al, %al
	jne	.L1839
.L1797:
	movq	%r13, %rsi
	movq	%r15, %rdi
	movb	%cl, -416(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_120BuildDefaultCallSiteEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	movzbl	-416(%rbp), %ecx
	movq	%rax, %r10
.L1814:
	testb	%cl, %cl
	je	.L1808
	cmpq	$0, -408(%rbp)
	je	.L1840
	movq	-408(%rbp), %rdx
	movq	%r13, %r8
	movq	%r10, %rcx
	movq	%r15, %rdi
	movl	$82, %esi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
.L1810:
	testb	%r12b, %r12b
	movl	$0, %edx
	movq	(%rax), %rsi
	movq	%r15, %rdi
	cmovne	%r14, %rdx
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1841
	addq	$408, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1808:
	.cfi_restore_state
	leaq	88(%r15), %rdx
	testq	%rbx, %rbx
	movq	3856(%r15), %rax
	cmovne	%rbx, %rdx
	cmpq	%rax, (%rdx)
	je	.L1842
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$85, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	jmp	.L1810
	.p2align 4,,10
	.p2align 3
.L1839:
	leaq	-288(%rbp), %rax
	movq	-376(%rbp), %rdx
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -416(%rbp)
	call	_ZN2v88internal9ParseInfoC1EPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEE@PLT
	movq	-376(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	-416(%rbp), %rdi
	call	_ZN2v88internal7parsing8ParseAnyEPNS0_9ParseInfoENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateENS1_29ReportErrorsAndStatisticsModeE@PLT
	movb	%al, -417(%rbp)
	testb	%al, %al
	jne	.L1843
	movq	96(%r15), %rax
	movq	-416(%rbp), %rdi
	movq	%rax, 12480(%r15)
	call	_ZN2v88internal9ParseInfoD1Ev@PLT
	movzbl	-417(%rbp), %ecx
	jmp	.L1797
	.p2align 4,,10
	.p2align 3
.L1838:
	movq	-1(%rax), %rax
	movq	%rdx, -408(%rbp)
	cmpw	$63, 11(%rax)
	jbe	.L1794
	jmp	.L1818
	.p2align 4,,10
	.p2align 3
.L1842:
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal10ErrorUtils16NewIteratorErrorEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	jmp	.L1810
	.p2align 4,,10
	.p2align 3
.L1840:
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%r10, %rdx
	movl	$81, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	jmp	.L1810
	.p2align 4,,10
	.p2align 3
.L1843:
	movq	-176(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal15AstValueFactory11InternalizeEPNS0_7IsolateE@PLT
	movq	-376(%rbp), %rax
	movzbl	-417(%rbp), %ecx
	movq	(%rax), %rax
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L1844
.L1799:
	leaq	-368(%rbp), %r12
	movb	%cl, -417(%rbp)
	movq	%r12, %rdi
	movq	%rax, -368(%rbp)
	call	_ZN2v88internal6Script16IsUserJavaScriptEv@PLT
	movzbl	-417(%rbp), %ecx
	movzbl	%al, %edx
.L1801:
	movq	%r15, %rsi
	movq	%r12, %rdi
	movb	%cl, -432(%rbp)
	call	_ZN2v88internal11CallPrinterC1EPNS0_7IsolateEb@PLT
	movl	-392(%rbp), %edx
	movq	-120(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter5PrintEPNS0_15FunctionLiteralEi@PLT
	movzbl	-432(%rbp), %ecx
	movq	%rax, %r10
	movq	-320(%rbp), %rax
	testq	%rax, %rax
	setne	-417(%rbp)
	je	.L1802
	cmpq	$0, -408(%rbp)
	je	.L1845
.L1802:
	movq	(%r10), %rax
	movl	11(%rax), %eax
	testl	%eax, %eax
	jle	.L1846
	movq	%r12, %rdi
	movq	%r10, -440(%rbp)
	movb	%cl, -432(%rbp)
	call	_ZN2v88internal11CallPrinterD1Ev@PLT
	movq	-416(%rbp), %rdi
	call	_ZN2v88internal9ParseInfoD1Ev@PLT
	movzbl	-432(%rbp), %ecx
	movq	-440(%rbp), %r10
	movl	%ecx, %r12d
	movzbl	-417(%rbp), %ecx
	jmp	.L1814
	.p2align 4,,10
	.p2align 3
.L1846:
	movq	%r12, %rdi
	movb	%cl, -432(%rbp)
	call	_ZN2v88internal11CallPrinterD1Ev@PLT
	movq	-416(%rbp), %rdi
	call	_ZN2v88internal9ParseInfoD1Ev@PLT
	movzbl	-432(%rbp), %ecx
	movl	%ecx, %r12d
	movzbl	-417(%rbp), %ecx
	jmp	.L1797
	.p2align 4,,10
	.p2align 3
.L1844:
	movq	-1(%rax), %rdx
	cmpw	$86, 11(%rdx)
	je	.L1847
.L1800:
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	-37504(%rdx), %rax
	jne	.L1799
	xorl	%edx, %edx
	leaq	-368(%rbp), %r12
	jmp	.L1801
	.p2align 4,,10
	.p2align 3
.L1845:
	movq	-328(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L1803
	movq	(%rdx), %rdi
	movq	%r10, -448(%rbp)
	movb	%cl, -440(%rbp)
	andq	$-4, %rdi
	movq	%rdx, -432(%rbp)
	call	_ZNK2v88internal10Expression14IsPropertyNameEv@PLT
	movq	-432(%rbp), %rdx
	movzbl	-440(%rbp), %ecx
	testb	%al, %al
	movq	-448(%rbp), %r10
	je	.L1806
	movq	(%rdx), %rax
	andq	$-4, %rax
	movzbl	4(%rax), %edx
	andl	$63, %edx
	cmpb	$41, %dl
	jne	.L1805
	movq	8(%rax), %rdx
	movq	(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L1806
	movq	%rdx, -408(%rbp)
	movl	(%rax), %eax
	.p2align 4,,10
	.p2align 3
.L1807:
	cmpl	$-1, %eax
	je	.L1802
	movl	%eax, -392(%rbp)
	addl	$1, %eax
	movl	%eax, -388(%rbp)
	movl	$-1, -384(%rbp)
	jmp	.L1802
.L1806:
	movq	-320(%rbp), %rax
.L1803:
	movq	16(%rax), %rax
	movl	(%rax), %eax
	jmp	.L1807
	.p2align 4,,10
	.p2align 3
.L1847:
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L1800
	jmp	.L1799
.L1841:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal10ErrorUtils28ThrowLoadFromNullOrUndefinedEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS0_11MaybeHandleIS5_EE
	.cfi_startproc
	.type	_ZN2v88internal10ErrorUtils28ThrowLoadFromNullOrUndefinedEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS0_11MaybeHandleIS5_EE.cold, @function
_ZN2v88internal10ErrorUtils28ThrowLoadFromNullOrUndefinedEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS0_11MaybeHandleIS5_EE.cold:
.LFSB20776:
.L1805:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	8, %rax
	ud2
	.cfi_endproc
.LFE20776:
	.section	.text._ZN2v88internal10ErrorUtils28ThrowLoadFromNullOrUndefinedEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS0_11MaybeHandleIS5_EE
	.size	_ZN2v88internal10ErrorUtils28ThrowLoadFromNullOrUndefinedEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS0_11MaybeHandleIS5_EE, .-_ZN2v88internal10ErrorUtils28ThrowLoadFromNullOrUndefinedEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS0_11MaybeHandleIS5_EE
	.section	.text.unlikely._ZN2v88internal10ErrorUtils28ThrowLoadFromNullOrUndefinedEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS0_11MaybeHandleIS5_EE
	.size	_ZN2v88internal10ErrorUtils28ThrowLoadFromNullOrUndefinedEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS0_11MaybeHandleIS5_EE.cold, .-_ZN2v88internal10ErrorUtils28ThrowLoadFromNullOrUndefinedEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS0_11MaybeHandleIS5_EE.cold
.LCOLDE399:
	.section	.text._ZN2v88internal10ErrorUtils28ThrowLoadFromNullOrUndefinedEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS0_11MaybeHandleIS5_EE
.LHOTE399:
	.section	.text._ZN2v88internal10ErrorUtils28ThrowLoadFromNullOrUndefinedEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10ErrorUtils28ThrowLoadFromNullOrUndefinedEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	.type	_ZN2v88internal10ErrorUtils28ThrowLoadFromNullOrUndefinedEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE, @function
_ZN2v88internal10ErrorUtils28ThrowLoadFromNullOrUndefinedEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE:
.LFB20775:
	.cfi_startproc
	endbr64
	xorl	%edx, %edx
	jmp	_ZN2v88internal10ErrorUtils28ThrowLoadFromNullOrUndefinedEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS0_11MaybeHandleIS5_EE
	.cfi_endproc
.LFE20775:
	.size	_ZN2v88internal10ErrorUtils28ThrowLoadFromNullOrUndefinedEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE, .-_ZN2v88internal10ErrorUtils28ThrowLoadFromNullOrUndefinedEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal15MessageLocationC2ENS0_6HandleINS0_6ScriptEEEii,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal15MessageLocationC2ENS0_6HandleINS0_6ScriptEEEii, @function
_GLOBAL__sub_I__ZN2v88internal15MessageLocationC2ENS0_6HandleINS0_6ScriptEEEii:
.LFB25941:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE25941:
	.size	_GLOBAL__sub_I__ZN2v88internal15MessageLocationC2ENS0_6HandleINS0_6ScriptEEEii, .-_GLOBAL__sub_I__ZN2v88internal15MessageLocationC2ENS0_6HandleINS0_6ScriptEEEii
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal15MessageLocationC2ENS0_6HandleINS0_6ScriptEEEii
	.section	.rodata.CSWTCH.4401,"a"
	.align 16
	.type	CSWTCH.4401, @object
	.size	CSWTCH.4401, 20
CSWTCH.4401:
	.long	25
	.long	96
	.long	98
	.long	93
	.long	94
	.section	.rodata.CSWTCH.4398,"a"
	.align 16
	.type	CSWTCH.4398, @object
	.size	CSWTCH.4398, 16
CSWTCH.4398:
	.long	96
	.long	98
	.long	93
	.long	94
	.weak	_ZTVN2v88internal14StackFrameBaseE
	.section	.data.rel.ro._ZTVN2v88internal14StackFrameBaseE,"awG",@progbits,_ZTVN2v88internal14StackFrameBaseE,comdat
	.align 8
	.type	_ZTVN2v88internal14StackFrameBaseE, @object
	.size	_ZTVN2v88internal14StackFrameBaseE, 216
_ZTVN2v88internal14StackFrameBaseE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN2v88internal14StackFrameBase13GetEvalOriginEv
	.quad	_ZN2v88internal14StackFrameBase17GetWasmModuleNameEv
	.quad	_ZN2v88internal14StackFrameBase15GetWasmInstanceEv
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN2v88internal14StackFrameBase6IsEvalEv
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.weak	_ZTVN2v88internal12JSStackFrameE
	.section	.data.rel.ro.local._ZTVN2v88internal12JSStackFrameE,"awG",@progbits,_ZTVN2v88internal12JSStackFrameE,comdat
	.align 8
	.type	_ZTVN2v88internal12JSStackFrameE, @object
	.size	_ZTVN2v88internal12JSStackFrameE, 216
_ZTVN2v88internal12JSStackFrameE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal12JSStackFrameD1Ev
	.quad	_ZN2v88internal12JSStackFrameD0Ev
	.quad	_ZNK2v88internal12JSStackFrame11GetReceiverEv
	.quad	_ZNK2v88internal12JSStackFrame11GetFunctionEv
	.quad	_ZN2v88internal12JSStackFrame11GetFileNameEv
	.quad	_ZN2v88internal12JSStackFrame15GetFunctionNameEv
	.quad	_ZN2v88internal12JSStackFrame24GetScriptNameOrSourceUrlEv
	.quad	_ZN2v88internal12JSStackFrame13GetMethodNameEv
	.quad	_ZN2v88internal12JSStackFrame11GetTypeNameEv
	.quad	_ZN2v88internal14StackFrameBase13GetEvalOriginEv
	.quad	_ZN2v88internal14StackFrameBase17GetWasmModuleNameEv
	.quad	_ZN2v88internal14StackFrameBase15GetWasmInstanceEv
	.quad	_ZNK2v88internal12JSStackFrame11GetPositionEv
	.quad	_ZN2v88internal12JSStackFrame13GetLineNumberEv
	.quad	_ZN2v88internal12JSStackFrame15GetColumnNumberEv
	.quad	_ZNK2v88internal12JSStackFrame15GetPromiseIndexEv
	.quad	_ZN2v88internal12JSStackFrame8IsNativeEv
	.quad	_ZN2v88internal12JSStackFrame10IsToplevelEv
	.quad	_ZN2v88internal14StackFrameBase6IsEvalEv
	.quad	_ZNK2v88internal12JSStackFrame7IsAsyncEv
	.quad	_ZNK2v88internal12JSStackFrame12IsPromiseAllEv
	.quad	_ZN2v88internal12JSStackFrame13IsConstructorEv
	.quad	_ZNK2v88internal12JSStackFrame8IsStrictEv
	.quad	_ZNK2v88internal12JSStackFrame9HasScriptEv
	.quad	_ZNK2v88internal12JSStackFrame9GetScriptEv
	.weak	_ZTVN2v88internal14WasmStackFrameE
	.section	.data.rel.ro.local._ZTVN2v88internal14WasmStackFrameE,"awG",@progbits,_ZTVN2v88internal14WasmStackFrameE,comdat
	.align 8
	.type	_ZTVN2v88internal14WasmStackFrameE, @object
	.size	_ZTVN2v88internal14WasmStackFrameE, 216
_ZTVN2v88internal14WasmStackFrameE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal14WasmStackFrameD1Ev
	.quad	_ZN2v88internal14WasmStackFrameD0Ev
	.quad	_ZNK2v88internal14WasmStackFrame11GetReceiverEv
	.quad	_ZNK2v88internal14WasmStackFrame11GetFunctionEv
	.quad	_ZN2v88internal14WasmStackFrame11GetFileNameEv
	.quad	_ZN2v88internal14WasmStackFrame15GetFunctionNameEv
	.quad	_ZN2v88internal14WasmStackFrame24GetScriptNameOrSourceUrlEv
	.quad	_ZN2v88internal14WasmStackFrame13GetMethodNameEv
	.quad	_ZN2v88internal14WasmStackFrame11GetTypeNameEv
	.quad	_ZN2v88internal14StackFrameBase13GetEvalOriginEv
	.quad	_ZN2v88internal14WasmStackFrame17GetWasmModuleNameEv
	.quad	_ZN2v88internal14WasmStackFrame15GetWasmInstanceEv
	.quad	_ZNK2v88internal14WasmStackFrame11GetPositionEv
	.quad	_ZN2v88internal14WasmStackFrame13GetLineNumberEv
	.quad	_ZN2v88internal14WasmStackFrame15GetColumnNumberEv
	.quad	_ZNK2v88internal14WasmStackFrame15GetPromiseIndexEv
	.quad	_ZN2v88internal14WasmStackFrame8IsNativeEv
	.quad	_ZN2v88internal14WasmStackFrame10IsToplevelEv
	.quad	_ZN2v88internal14StackFrameBase6IsEvalEv
	.quad	_ZNK2v88internal14WasmStackFrame7IsAsyncEv
	.quad	_ZNK2v88internal14WasmStackFrame12IsPromiseAllEv
	.quad	_ZN2v88internal14WasmStackFrame13IsConstructorEv
	.quad	_ZNK2v88internal14WasmStackFrame8IsStrictEv
	.quad	_ZNK2v88internal14WasmStackFrame9HasScriptEv
	.quad	_ZNK2v88internal14WasmStackFrame9GetScriptEv
	.weak	_ZTVN2v88internal19AsmJsWasmStackFrameE
	.section	.data.rel.ro.local._ZTVN2v88internal19AsmJsWasmStackFrameE,"awG",@progbits,_ZTVN2v88internal19AsmJsWasmStackFrameE,comdat
	.align 8
	.type	_ZTVN2v88internal19AsmJsWasmStackFrameE, @object
	.size	_ZTVN2v88internal19AsmJsWasmStackFrameE, 216
_ZTVN2v88internal19AsmJsWasmStackFrameE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal19AsmJsWasmStackFrameD1Ev
	.quad	_ZN2v88internal19AsmJsWasmStackFrameD0Ev
	.quad	_ZNK2v88internal19AsmJsWasmStackFrame11GetReceiverEv
	.quad	_ZNK2v88internal19AsmJsWasmStackFrame11GetFunctionEv
	.quad	_ZN2v88internal19AsmJsWasmStackFrame11GetFileNameEv
	.quad	_ZN2v88internal14WasmStackFrame15GetFunctionNameEv
	.quad	_ZN2v88internal19AsmJsWasmStackFrame24GetScriptNameOrSourceUrlEv
	.quad	_ZN2v88internal14WasmStackFrame13GetMethodNameEv
	.quad	_ZN2v88internal14WasmStackFrame11GetTypeNameEv
	.quad	_ZN2v88internal14StackFrameBase13GetEvalOriginEv
	.quad	_ZN2v88internal14WasmStackFrame17GetWasmModuleNameEv
	.quad	_ZN2v88internal14WasmStackFrame15GetWasmInstanceEv
	.quad	_ZNK2v88internal19AsmJsWasmStackFrame11GetPositionEv
	.quad	_ZN2v88internal19AsmJsWasmStackFrame13GetLineNumberEv
	.quad	_ZN2v88internal19AsmJsWasmStackFrame15GetColumnNumberEv
	.quad	_ZNK2v88internal14WasmStackFrame15GetPromiseIndexEv
	.quad	_ZN2v88internal14WasmStackFrame8IsNativeEv
	.quad	_ZN2v88internal14WasmStackFrame10IsToplevelEv
	.quad	_ZN2v88internal14StackFrameBase6IsEvalEv
	.quad	_ZNK2v88internal14WasmStackFrame7IsAsyncEv
	.quad	_ZNK2v88internal14WasmStackFrame12IsPromiseAllEv
	.quad	_ZN2v88internal14WasmStackFrame13IsConstructorEv
	.quad	_ZNK2v88internal14WasmStackFrame8IsStrictEv
	.quad	_ZNK2v88internal14WasmStackFrame9HasScriptEv
	.quad	_ZNK2v88internal14WasmStackFrame9GetScriptEv
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
