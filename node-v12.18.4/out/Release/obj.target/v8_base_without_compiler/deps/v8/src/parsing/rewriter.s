	.file	"rewriter.cc"
	.text
	.section	.text._ZN2v88internal8Variable16SetMaybeAssignedEv,"axG",@progbits,_ZN2v88internal8Variable16SetMaybeAssignedEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8Variable16SetMaybeAssignedEv
	.type	_ZN2v88internal8Variable16SetMaybeAssignedEv, @function
_ZN2v88internal8Variable16SetMaybeAssignedEv:
.LFB9361:
	.cfi_startproc
	endbr64
	movzwl	40(%rdi), %eax
	movl	%eax, %edx
	andl	$15, %edx
	cmpb	$1, %dl
	je	.L11
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L3
	testb	$64, %ah
	je	.L15
.L3:
	orb	$64, %ah
	movw	%ax, 40(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal8Variable16SetMaybeAssignedEv
	movzwl	40(%rbx), %eax
	orb	$64, %ah
	movw	%ax, 40(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9361:
	.size	_ZN2v88internal8Variable16SetMaybeAssignedEv, .-_ZN2v88internal8Variable16SetMaybeAssignedEv
	.section	.text._ZN2v88internal9Processor24VisitExpressionStatementEPNS0_19ExpressionStatementE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Processor24VisitExpressionStatementEPNS0_19ExpressionStatementE
	.type	_ZN2v88internal9Processor24VisitExpressionStatementEPNS0_19ExpressionStatementE, @function
_ZN2v88internal9Processor24VisitExpressionStatementEPNS0_19ExpressionStatementE:
.LFB21103:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 82(%rdi)
	je	.L43
.L17:
	movq	%r12, 8(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
	movq	8(%rsi), %r15
	movq	(%rdi), %r14
	movb	$1, 81(%rdi)
	movq	32(%rdi), %rdi
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	subq	%r13, %rax
	cmpq	$23, %rax
	jbe	.L44
	leaq	24(%r13), %rax
	movq	%rax, 16(%rdi)
.L19:
	movl	$-1, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal13VariableProxyC1EPNS0_8VariableEi@PLT
	movl	4(%r13), %eax
	movl	%eax, %edx
	andl	$63, %edx
	cmpb	$54, %dl
	je	.L45
.L21:
	movq	32(%rbx), %rdi
	movq	16(%rdi), %r14
	movq	24(%rdi), %rax
	subq	%r14, %rax
	cmpq	$23, %rax
	jbe	.L46
	leaq	24(%r14), %rax
	movq	%rax, 16(%rdi)
.L26:
	movl	$-1, %r9d
	movq	%r15, %r8
	movq	%r13, %rcx
	movq	%r14, %rdi
	movl	$17, %edx
	movl	$24, %esi
	call	_ZN2v88internal10AssignmentC1ENS0_7AstNode8NodeTypeENS0_5Token5ValueEPNS0_10ExpressionES7_i@PLT
	movq	%r14, 8(%r12)
	movb	$1, 82(%rbx)
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L45:
	movl	%eax, %edx
	orb	$-128, %dl
	movl	%edx, 4(%r13)
	testb	$1, %ah
	je	.L21
	movq	8(%r13), %r14
	movzwl	40(%r14), %eax
	movl	%eax, %edx
	andl	$15, %edx
	cmpb	$1, %dl
	je	.L21
	movq	16(%r14), %rdx
	testq	%rdx, %rdx
	je	.L23
	testb	$64, %ah
	je	.L47
.L23:
	orb	$64, %ah
	movw	%ax, 40(%r14)
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L44:
	movl	$24, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r13
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L46:
	movl	$24, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r14
	jmp	.L26
.L47:
	movzwl	40(%rdx), %ecx
	movl	%ecx, %esi
	andl	$15, %esi
	cmpb	$1, %sil
	je	.L23
	movq	16(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L24
	testb	$64, %ch
	je	.L48
.L24:
	orb	$64, %ch
	movw	%cx, 40(%rdx)
	movzwl	40(%r14), %eax
	jmp	.L23
.L48:
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal8Variable16SetMaybeAssignedEv
	movq	-56(%rbp), %rdx
	movzwl	40(%rdx), %ecx
	jmp	.L24
	.cfi_endproc
.LFE21103:
	.size	_ZN2v88internal9Processor24VisitExpressionStatementEPNS0_19ExpressionStatementE, .-_ZN2v88internal9Processor24VisitExpressionStatementEPNS0_19ExpressionStatementE
	.section	.text._ZN2v88internal9Processor22VisitContinueStatementEPNS0_17ContinueStatementE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Processor22VisitContinueStatementEPNS0_17ContinueStatementE
	.type	_ZN2v88internal9Processor22VisitContinueStatementEPNS0_17ContinueStatementE, @function
_ZN2v88internal9Processor22VisitContinueStatementEPNS0_17ContinueStatementE:
.LFB21114:
	.cfi_startproc
	endbr64
	movb	$0, 82(%rdi)
	movq	%rsi, 8(%rdi)
	ret
	.cfi_endproc
.LFE21114:
	.size	_ZN2v88internal9Processor22VisitContinueStatementEPNS0_17ContinueStatementE, .-_ZN2v88internal9Processor22VisitContinueStatementEPNS0_17ContinueStatementE
	.section	.text._ZN2v88internal9Processor19VisitBreakStatementEPNS0_14BreakStatementE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Processor19VisitBreakStatementEPNS0_14BreakStatementE
	.type	_ZN2v88internal9Processor19VisitBreakStatementEPNS0_14BreakStatementE, @function
_ZN2v88internal9Processor19VisitBreakStatementEPNS0_14BreakStatementE:
.LFB21115:
	.cfi_startproc
	endbr64
	movb	$0, 82(%rdi)
	movq	%rsi, 8(%rdi)
	ret
	.cfi_endproc
.LFE21115:
	.size	_ZN2v88internal9Processor19VisitBreakStatementEPNS0_14BreakStatementE, .-_ZN2v88internal9Processor19VisitBreakStatementEPNS0_14BreakStatementE
	.section	.text._ZN2v88internal9Processor19VisitEmptyStatementEPNS0_14EmptyStatementE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Processor19VisitEmptyStatementEPNS0_14EmptyStatementE
	.type	_ZN2v88internal9Processor19VisitEmptyStatementEPNS0_14EmptyStatementE, @function
_ZN2v88internal9Processor19VisitEmptyStatementEPNS0_14EmptyStatementE:
.LFB21118:
	.cfi_startproc
	endbr64
	movq	%rsi, 8(%rdi)
	ret
	.cfi_endproc
.LFE21118:
	.size	_ZN2v88internal9Processor19VisitEmptyStatementEPNS0_14EmptyStatementE, .-_ZN2v88internal9Processor19VisitEmptyStatementEPNS0_14EmptyStatementE
	.section	.text._ZN2v88internal9Processor20VisitReturnStatementEPNS0_15ReturnStatementE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Processor20VisitReturnStatementEPNS0_15ReturnStatementE
	.type	_ZN2v88internal9Processor20VisitReturnStatementEPNS0_15ReturnStatementE, @function
_ZN2v88internal9Processor20VisitReturnStatementEPNS0_15ReturnStatementE:
.LFB21119:
	.cfi_startproc
	endbr64
	movb	$1, 82(%rdi)
	movq	%rsi, 8(%rdi)
	ret
	.cfi_endproc
.LFE21119:
	.size	_ZN2v88internal9Processor20VisitReturnStatementEPNS0_15ReturnStatementE, .-_ZN2v88internal9Processor20VisitReturnStatementEPNS0_15ReturnStatementE
	.section	.text._ZN2v88internal9Processor22VisitDebuggerStatementEPNS0_17DebuggerStatementE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Processor22VisitDebuggerStatementEPNS0_17DebuggerStatementE
	.type	_ZN2v88internal9Processor22VisitDebuggerStatementEPNS0_17DebuggerStatementE, @function
_ZN2v88internal9Processor22VisitDebuggerStatementEPNS0_17DebuggerStatementE:
.LFB21120:
	.cfi_startproc
	endbr64
	movq	%rsi, 8(%rdi)
	ret
	.cfi_endproc
.LFE21120:
	.size	_ZN2v88internal9Processor22VisitDebuggerStatementEPNS0_17DebuggerStatementE, .-_ZN2v88internal9Processor22VisitDebuggerStatementEPNS0_17DebuggerStatementE
	.section	.text._ZN2v88internal9Processor36VisitInitializeClassMembersStatementEPNS0_31InitializeClassMembersStatementE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Processor36VisitInitializeClassMembersStatementEPNS0_31InitializeClassMembersStatementE
	.type	_ZN2v88internal9Processor36VisitInitializeClassMembersStatementEPNS0_31InitializeClassMembersStatementE, @function
_ZN2v88internal9Processor36VisitInitializeClassMembersStatementEPNS0_31InitializeClassMembersStatementE:
.LFB21121:
	.cfi_startproc
	endbr64
	movq	%rsi, 8(%rdi)
	ret
	.cfi_endproc
.LFE21121:
	.size	_ZN2v88internal9Processor36VisitInitializeClassMembersStatementEPNS0_31InitializeClassMembersStatementE, .-_ZN2v88internal9Processor36VisitInitializeClassMembersStatementEPNS0_31InitializeClassMembersStatementE
	.section	.rodata._ZN2v88internal9Processor18VisitRegExpLiteralEPNS0_13RegExpLiteralE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal9Processor18VisitRegExpLiteralEPNS0_13RegExpLiteralE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Processor18VisitRegExpLiteralEPNS0_13RegExpLiteralE
	.type	_ZN2v88internal9Processor18VisitRegExpLiteralEPNS0_13RegExpLiteralE, @function
_ZN2v88internal9Processor18VisitRegExpLiteralEPNS0_13RegExpLiteralE:
.LFB27299:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE27299:
	.size	_ZN2v88internal9Processor18VisitRegExpLiteralEPNS0_13RegExpLiteralE, .-_ZN2v88internal9Processor18VisitRegExpLiteralEPNS0_13RegExpLiteralE
	.section	.text._ZN2v88internal9Processor18VisitObjectLiteralEPNS0_13ObjectLiteralE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Processor18VisitObjectLiteralEPNS0_13ObjectLiteralE
	.type	_ZN2v88internal9Processor18VisitObjectLiteralEPNS0_13ObjectLiteralE, @function
_ZN2v88internal9Processor18VisitObjectLiteralEPNS0_13ObjectLiteralE:
.LFB27301:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE27301:
	.size	_ZN2v88internal9Processor18VisitObjectLiteralEPNS0_13ObjectLiteralE, .-_ZN2v88internal9Processor18VisitObjectLiteralEPNS0_13ObjectLiteralE
	.section	.text._ZN2v88internal9Processor17VisitArrayLiteralEPNS0_12ArrayLiteralE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Processor17VisitArrayLiteralEPNS0_12ArrayLiteralE
	.type	_ZN2v88internal9Processor17VisitArrayLiteralEPNS0_12ArrayLiteralE, @function
_ZN2v88internal9Processor17VisitArrayLiteralEPNS0_12ArrayLiteralE:
.LFB27303:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE27303:
	.size	_ZN2v88internal9Processor17VisitArrayLiteralEPNS0_12ArrayLiteralE, .-_ZN2v88internal9Processor17VisitArrayLiteralEPNS0_12ArrayLiteralE
	.section	.text._ZN2v88internal9Processor15VisitAssignmentEPNS0_10AssignmentE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Processor15VisitAssignmentEPNS0_10AssignmentE
	.type	_ZN2v88internal9Processor15VisitAssignmentEPNS0_10AssignmentE, @function
_ZN2v88internal9Processor15VisitAssignmentEPNS0_10AssignmentE:
.LFB27305:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE27305:
	.size	_ZN2v88internal9Processor15VisitAssignmentEPNS0_10AssignmentE, .-_ZN2v88internal9Processor15VisitAssignmentEPNS0_10AssignmentE
	.section	.text._ZN2v88internal9Processor10VisitAwaitEPNS0_5AwaitE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Processor10VisitAwaitEPNS0_5AwaitE
	.type	_ZN2v88internal9Processor10VisitAwaitEPNS0_5AwaitE, @function
_ZN2v88internal9Processor10VisitAwaitEPNS0_5AwaitE:
.LFB27307:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE27307:
	.size	_ZN2v88internal9Processor10VisitAwaitEPNS0_5AwaitE, .-_ZN2v88internal9Processor10VisitAwaitEPNS0_5AwaitE
	.section	.text._ZN2v88internal9Processor20VisitBinaryOperationEPNS0_15BinaryOperationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Processor20VisitBinaryOperationEPNS0_15BinaryOperationE
	.type	_ZN2v88internal9Processor20VisitBinaryOperationEPNS0_15BinaryOperationE, @function
_ZN2v88internal9Processor20VisitBinaryOperationEPNS0_15BinaryOperationE:
.LFB27309:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE27309:
	.size	_ZN2v88internal9Processor20VisitBinaryOperationEPNS0_15BinaryOperationE, .-_ZN2v88internal9Processor20VisitBinaryOperationEPNS0_15BinaryOperationE
	.section	.text._ZN2v88internal9Processor18VisitNaryOperationEPNS0_13NaryOperationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Processor18VisitNaryOperationEPNS0_13NaryOperationE
	.type	_ZN2v88internal9Processor18VisitNaryOperationEPNS0_13NaryOperationE, @function
_ZN2v88internal9Processor18VisitNaryOperationEPNS0_13NaryOperationE:
.LFB27311:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE27311:
	.size	_ZN2v88internal9Processor18VisitNaryOperationEPNS0_13NaryOperationE, .-_ZN2v88internal9Processor18VisitNaryOperationEPNS0_13NaryOperationE
	.section	.text._ZN2v88internal9Processor9VisitCallEPNS0_4CallE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Processor9VisitCallEPNS0_4CallE
	.type	_ZN2v88internal9Processor9VisitCallEPNS0_4CallE, @function
_ZN2v88internal9Processor9VisitCallEPNS0_4CallE:
.LFB27313:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE27313:
	.size	_ZN2v88internal9Processor9VisitCallEPNS0_4CallE, .-_ZN2v88internal9Processor9VisitCallEPNS0_4CallE
	.section	.text._ZN2v88internal9Processor12VisitCallNewEPNS0_7CallNewE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Processor12VisitCallNewEPNS0_7CallNewE
	.type	_ZN2v88internal9Processor12VisitCallNewEPNS0_7CallNewE, @function
_ZN2v88internal9Processor12VisitCallNewEPNS0_7CallNewE:
.LFB27315:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE27315:
	.size	_ZN2v88internal9Processor12VisitCallNewEPNS0_7CallNewE, .-_ZN2v88internal9Processor12VisitCallNewEPNS0_7CallNewE
	.section	.text._ZN2v88internal9Processor16VisitCallRuntimeEPNS0_11CallRuntimeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Processor16VisitCallRuntimeEPNS0_11CallRuntimeE
	.type	_ZN2v88internal9Processor16VisitCallRuntimeEPNS0_11CallRuntimeE, @function
_ZN2v88internal9Processor16VisitCallRuntimeEPNS0_11CallRuntimeE:
.LFB27317:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE27317:
	.size	_ZN2v88internal9Processor16VisitCallRuntimeEPNS0_11CallRuntimeE, .-_ZN2v88internal9Processor16VisitCallRuntimeEPNS0_11CallRuntimeE
	.section	.text._ZN2v88internal9Processor17VisitClassLiteralEPNS0_12ClassLiteralE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Processor17VisitClassLiteralEPNS0_12ClassLiteralE
	.type	_ZN2v88internal9Processor17VisitClassLiteralEPNS0_12ClassLiteralE, @function
_ZN2v88internal9Processor17VisitClassLiteralEPNS0_12ClassLiteralE:
.LFB27319:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE27319:
	.size	_ZN2v88internal9Processor17VisitClassLiteralEPNS0_12ClassLiteralE, .-_ZN2v88internal9Processor17VisitClassLiteralEPNS0_12ClassLiteralE
	.section	.text._ZN2v88internal9Processor21VisitCompareOperationEPNS0_16CompareOperationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Processor21VisitCompareOperationEPNS0_16CompareOperationE
	.type	_ZN2v88internal9Processor21VisitCompareOperationEPNS0_16CompareOperationE, @function
_ZN2v88internal9Processor21VisitCompareOperationEPNS0_16CompareOperationE:
.LFB27321:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE27321:
	.size	_ZN2v88internal9Processor21VisitCompareOperationEPNS0_16CompareOperationE, .-_ZN2v88internal9Processor21VisitCompareOperationEPNS0_16CompareOperationE
	.section	.text._ZN2v88internal9Processor23VisitCompoundAssignmentEPNS0_18CompoundAssignmentE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Processor23VisitCompoundAssignmentEPNS0_18CompoundAssignmentE
	.type	_ZN2v88internal9Processor23VisitCompoundAssignmentEPNS0_18CompoundAssignmentE, @function
_ZN2v88internal9Processor23VisitCompoundAssignmentEPNS0_18CompoundAssignmentE:
.LFB27323:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE27323:
	.size	_ZN2v88internal9Processor23VisitCompoundAssignmentEPNS0_18CompoundAssignmentE, .-_ZN2v88internal9Processor23VisitCompoundAssignmentEPNS0_18CompoundAssignmentE
	.section	.text._ZN2v88internal9Processor16VisitConditionalEPNS0_11ConditionalE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Processor16VisitConditionalEPNS0_11ConditionalE
	.type	_ZN2v88internal9Processor16VisitConditionalEPNS0_11ConditionalE, @function
_ZN2v88internal9Processor16VisitConditionalEPNS0_11ConditionalE:
.LFB27325:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE27325:
	.size	_ZN2v88internal9Processor16VisitConditionalEPNS0_11ConditionalE, .-_ZN2v88internal9Processor16VisitConditionalEPNS0_11ConditionalE
	.section	.text._ZN2v88internal9Processor19VisitCountOperationEPNS0_14CountOperationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Processor19VisitCountOperationEPNS0_14CountOperationE
	.type	_ZN2v88internal9Processor19VisitCountOperationEPNS0_14CountOperationE, @function
_ZN2v88internal9Processor19VisitCountOperationEPNS0_14CountOperationE:
.LFB27327:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE27327:
	.size	_ZN2v88internal9Processor19VisitCountOperationEPNS0_14CountOperationE, .-_ZN2v88internal9Processor19VisitCountOperationEPNS0_14CountOperationE
	.section	.text._ZN2v88internal9Processor17VisitDoExpressionEPNS0_12DoExpressionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Processor17VisitDoExpressionEPNS0_12DoExpressionE
	.type	_ZN2v88internal9Processor17VisitDoExpressionEPNS0_12DoExpressionE, @function
_ZN2v88internal9Processor17VisitDoExpressionEPNS0_12DoExpressionE:
.LFB27329:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE27329:
	.size	_ZN2v88internal9Processor17VisitDoExpressionEPNS0_12DoExpressionE, .-_ZN2v88internal9Processor17VisitDoExpressionEPNS0_12DoExpressionE
	.section	.text._ZN2v88internal9Processor21VisitEmptyParenthesesEPNS0_16EmptyParenthesesE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Processor21VisitEmptyParenthesesEPNS0_16EmptyParenthesesE
	.type	_ZN2v88internal9Processor21VisitEmptyParenthesesEPNS0_16EmptyParenthesesE, @function
_ZN2v88internal9Processor21VisitEmptyParenthesesEPNS0_16EmptyParenthesesE:
.LFB27331:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE27331:
	.size	_ZN2v88internal9Processor21VisitEmptyParenthesesEPNS0_16EmptyParenthesesE, .-_ZN2v88internal9Processor21VisitEmptyParenthesesEPNS0_16EmptyParenthesesE
	.section	.text._ZN2v88internal9Processor20VisitFunctionLiteralEPNS0_15FunctionLiteralE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Processor20VisitFunctionLiteralEPNS0_15FunctionLiteralE
	.type	_ZN2v88internal9Processor20VisitFunctionLiteralEPNS0_15FunctionLiteralE, @function
_ZN2v88internal9Processor20VisitFunctionLiteralEPNS0_15FunctionLiteralE:
.LFB27333:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE27333:
	.size	_ZN2v88internal9Processor20VisitFunctionLiteralEPNS0_15FunctionLiteralE, .-_ZN2v88internal9Processor20VisitFunctionLiteralEPNS0_15FunctionLiteralE
	.section	.text._ZN2v88internal9Processor22VisitGetTemplateObjectEPNS0_17GetTemplateObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Processor22VisitGetTemplateObjectEPNS0_17GetTemplateObjectE
	.type	_ZN2v88internal9Processor22VisitGetTemplateObjectEPNS0_17GetTemplateObjectE, @function
_ZN2v88internal9Processor22VisitGetTemplateObjectEPNS0_17GetTemplateObjectE:
.LFB27335:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE27335:
	.size	_ZN2v88internal9Processor22VisitGetTemplateObjectEPNS0_17GetTemplateObjectE, .-_ZN2v88internal9Processor22VisitGetTemplateObjectEPNS0_17GetTemplateObjectE
	.section	.text._ZN2v88internal9Processor25VisitImportCallExpressionEPNS0_20ImportCallExpressionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Processor25VisitImportCallExpressionEPNS0_20ImportCallExpressionE
	.type	_ZN2v88internal9Processor25VisitImportCallExpressionEPNS0_20ImportCallExpressionE, @function
_ZN2v88internal9Processor25VisitImportCallExpressionEPNS0_20ImportCallExpressionE:
.LFB27337:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE27337:
	.size	_ZN2v88internal9Processor25VisitImportCallExpressionEPNS0_20ImportCallExpressionE, .-_ZN2v88internal9Processor25VisitImportCallExpressionEPNS0_20ImportCallExpressionE
	.section	.text._ZN2v88internal9Processor12VisitLiteralEPNS0_7LiteralE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Processor12VisitLiteralEPNS0_7LiteralE
	.type	_ZN2v88internal9Processor12VisitLiteralEPNS0_7LiteralE, @function
_ZN2v88internal9Processor12VisitLiteralEPNS0_7LiteralE:
.LFB27339:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE27339:
	.size	_ZN2v88internal9Processor12VisitLiteralEPNS0_7LiteralE, .-_ZN2v88internal9Processor12VisitLiteralEPNS0_7LiteralE
	.section	.text._ZN2v88internal9Processor26VisitNativeFunctionLiteralEPNS0_21NativeFunctionLiteralE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Processor26VisitNativeFunctionLiteralEPNS0_21NativeFunctionLiteralE
	.type	_ZN2v88internal9Processor26VisitNativeFunctionLiteralEPNS0_21NativeFunctionLiteralE, @function
_ZN2v88internal9Processor26VisitNativeFunctionLiteralEPNS0_21NativeFunctionLiteralE:
.LFB27341:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE27341:
	.size	_ZN2v88internal9Processor26VisitNativeFunctionLiteralEPNS0_21NativeFunctionLiteralE, .-_ZN2v88internal9Processor26VisitNativeFunctionLiteralEPNS0_21NativeFunctionLiteralE
	.section	.text._ZN2v88internal9Processor18VisitOptionalChainEPNS0_13OptionalChainE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Processor18VisitOptionalChainEPNS0_13OptionalChainE
	.type	_ZN2v88internal9Processor18VisitOptionalChainEPNS0_13OptionalChainE, @function
_ZN2v88internal9Processor18VisitOptionalChainEPNS0_13OptionalChainE:
.LFB27343:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE27343:
	.size	_ZN2v88internal9Processor18VisitOptionalChainEPNS0_13OptionalChainE, .-_ZN2v88internal9Processor18VisitOptionalChainEPNS0_13OptionalChainE
	.section	.text._ZN2v88internal9Processor13VisitPropertyEPNS0_8PropertyE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Processor13VisitPropertyEPNS0_8PropertyE
	.type	_ZN2v88internal9Processor13VisitPropertyEPNS0_8PropertyE, @function
_ZN2v88internal9Processor13VisitPropertyEPNS0_8PropertyE:
.LFB27345:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE27345:
	.size	_ZN2v88internal9Processor13VisitPropertyEPNS0_8PropertyE, .-_ZN2v88internal9Processor13VisitPropertyEPNS0_8PropertyE
	.section	.text._ZN2v88internal9Processor21VisitResolvedPropertyEPNS0_16ResolvedPropertyE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Processor21VisitResolvedPropertyEPNS0_16ResolvedPropertyE
	.type	_ZN2v88internal9Processor21VisitResolvedPropertyEPNS0_16ResolvedPropertyE, @function
_ZN2v88internal9Processor21VisitResolvedPropertyEPNS0_16ResolvedPropertyE:
.LFB27347:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE27347:
	.size	_ZN2v88internal9Processor21VisitResolvedPropertyEPNS0_16ResolvedPropertyE, .-_ZN2v88internal9Processor21VisitResolvedPropertyEPNS0_16ResolvedPropertyE
	.section	.text._ZN2v88internal9Processor11VisitSpreadEPNS0_6SpreadE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Processor11VisitSpreadEPNS0_6SpreadE
	.type	_ZN2v88internal9Processor11VisitSpreadEPNS0_6SpreadE, @function
_ZN2v88internal9Processor11VisitSpreadEPNS0_6SpreadE:
.LFB27349:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE27349:
	.size	_ZN2v88internal9Processor11VisitSpreadEPNS0_6SpreadE, .-_ZN2v88internal9Processor11VisitSpreadEPNS0_6SpreadE
	.section	.text._ZN2v88internal9Processor24VisitStoreInArrayLiteralEPNS0_19StoreInArrayLiteralE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Processor24VisitStoreInArrayLiteralEPNS0_19StoreInArrayLiteralE
	.type	_ZN2v88internal9Processor24VisitStoreInArrayLiteralEPNS0_19StoreInArrayLiteralE, @function
_ZN2v88internal9Processor24VisitStoreInArrayLiteralEPNS0_19StoreInArrayLiteralE:
.LFB27351:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE27351:
	.size	_ZN2v88internal9Processor24VisitStoreInArrayLiteralEPNS0_19StoreInArrayLiteralE, .-_ZN2v88internal9Processor24VisitStoreInArrayLiteralEPNS0_19StoreInArrayLiteralE
	.section	.text._ZN2v88internal9Processor23VisitSuperCallReferenceEPNS0_18SuperCallReferenceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Processor23VisitSuperCallReferenceEPNS0_18SuperCallReferenceE
	.type	_ZN2v88internal9Processor23VisitSuperCallReferenceEPNS0_18SuperCallReferenceE, @function
_ZN2v88internal9Processor23VisitSuperCallReferenceEPNS0_18SuperCallReferenceE:
.LFB27353:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE27353:
	.size	_ZN2v88internal9Processor23VisitSuperCallReferenceEPNS0_18SuperCallReferenceE, .-_ZN2v88internal9Processor23VisitSuperCallReferenceEPNS0_18SuperCallReferenceE
	.section	.text._ZN2v88internal9Processor27VisitSuperPropertyReferenceEPNS0_22SuperPropertyReferenceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Processor27VisitSuperPropertyReferenceEPNS0_22SuperPropertyReferenceE
	.type	_ZN2v88internal9Processor27VisitSuperPropertyReferenceEPNS0_22SuperPropertyReferenceE, @function
_ZN2v88internal9Processor27VisitSuperPropertyReferenceEPNS0_22SuperPropertyReferenceE:
.LFB27355:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE27355:
	.size	_ZN2v88internal9Processor27VisitSuperPropertyReferenceEPNS0_22SuperPropertyReferenceE, .-_ZN2v88internal9Processor27VisitSuperPropertyReferenceEPNS0_22SuperPropertyReferenceE
	.section	.text._ZN2v88internal9Processor20VisitTemplateLiteralEPNS0_15TemplateLiteralE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Processor20VisitTemplateLiteralEPNS0_15TemplateLiteralE
	.type	_ZN2v88internal9Processor20VisitTemplateLiteralEPNS0_15TemplateLiteralE, @function
_ZN2v88internal9Processor20VisitTemplateLiteralEPNS0_15TemplateLiteralE:
.LFB27357:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE27357:
	.size	_ZN2v88internal9Processor20VisitTemplateLiteralEPNS0_15TemplateLiteralE, .-_ZN2v88internal9Processor20VisitTemplateLiteralEPNS0_15TemplateLiteralE
	.section	.text._ZN2v88internal9Processor19VisitThisExpressionEPNS0_14ThisExpressionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Processor19VisitThisExpressionEPNS0_14ThisExpressionE
	.type	_ZN2v88internal9Processor19VisitThisExpressionEPNS0_14ThisExpressionE, @function
_ZN2v88internal9Processor19VisitThisExpressionEPNS0_14ThisExpressionE:
.LFB27359:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE27359:
	.size	_ZN2v88internal9Processor19VisitThisExpressionEPNS0_14ThisExpressionE, .-_ZN2v88internal9Processor19VisitThisExpressionEPNS0_14ThisExpressionE
	.section	.text._ZN2v88internal9Processor10VisitThrowEPNS0_5ThrowE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Processor10VisitThrowEPNS0_5ThrowE
	.type	_ZN2v88internal9Processor10VisitThrowEPNS0_5ThrowE, @function
_ZN2v88internal9Processor10VisitThrowEPNS0_5ThrowE:
.LFB27361:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE27361:
	.size	_ZN2v88internal9Processor10VisitThrowEPNS0_5ThrowE, .-_ZN2v88internal9Processor10VisitThrowEPNS0_5ThrowE
	.section	.text._ZN2v88internal9Processor19VisitUnaryOperationEPNS0_14UnaryOperationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Processor19VisitUnaryOperationEPNS0_14UnaryOperationE
	.type	_ZN2v88internal9Processor19VisitUnaryOperationEPNS0_14UnaryOperationE, @function
_ZN2v88internal9Processor19VisitUnaryOperationEPNS0_14UnaryOperationE:
.LFB27363:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE27363:
	.size	_ZN2v88internal9Processor19VisitUnaryOperationEPNS0_14UnaryOperationE, .-_ZN2v88internal9Processor19VisitUnaryOperationEPNS0_14UnaryOperationE
	.section	.text._ZN2v88internal9Processor18VisitVariableProxyEPNS0_13VariableProxyE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Processor18VisitVariableProxyEPNS0_13VariableProxyE
	.type	_ZN2v88internal9Processor18VisitVariableProxyEPNS0_13VariableProxyE, @function
_ZN2v88internal9Processor18VisitVariableProxyEPNS0_13VariableProxyE:
.LFB27365:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE27365:
	.size	_ZN2v88internal9Processor18VisitVariableProxyEPNS0_13VariableProxyE, .-_ZN2v88internal9Processor18VisitVariableProxyEPNS0_13VariableProxyE
	.section	.text._ZN2v88internal9Processor10VisitYieldEPNS0_5YieldE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Processor10VisitYieldEPNS0_5YieldE
	.type	_ZN2v88internal9Processor10VisitYieldEPNS0_5YieldE, @function
_ZN2v88internal9Processor10VisitYieldEPNS0_5YieldE:
.LFB27367:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE27367:
	.size	_ZN2v88internal9Processor10VisitYieldEPNS0_5YieldE, .-_ZN2v88internal9Processor10VisitYieldEPNS0_5YieldE
	.section	.text._ZN2v88internal9Processor14VisitYieldStarEPNS0_9YieldStarE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Processor14VisitYieldStarEPNS0_9YieldStarE
	.type	_ZN2v88internal9Processor14VisitYieldStarEPNS0_9YieldStarE, @function
_ZN2v88internal9Processor14VisitYieldStarEPNS0_9YieldStarE:
.LFB27369:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE27369:
	.size	_ZN2v88internal9Processor14VisitYieldStarEPNS0_9YieldStarE, .-_ZN2v88internal9Processor14VisitYieldStarEPNS0_9YieldStarE
	.section	.text._ZN2v88internal9Processor24VisitVariableDeclarationEPNS0_19VariableDeclarationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Processor24VisitVariableDeclarationEPNS0_19VariableDeclarationE
	.type	_ZN2v88internal9Processor24VisitVariableDeclarationEPNS0_19VariableDeclarationE, @function
_ZN2v88internal9Processor24VisitVariableDeclarationEPNS0_19VariableDeclarationE:
.LFB21158:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21158:
	.size	_ZN2v88internal9Processor24VisitVariableDeclarationEPNS0_19VariableDeclarationE, .-_ZN2v88internal9Processor24VisitVariableDeclarationEPNS0_19VariableDeclarationE
	.section	.text._ZN2v88internal9Processor24VisitFunctionDeclarationEPNS0_19FunctionDeclarationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Processor24VisitFunctionDeclarationEPNS0_19FunctionDeclarationE
	.type	_ZN2v88internal9Processor24VisitFunctionDeclarationEPNS0_19FunctionDeclarationE, @function
_ZN2v88internal9Processor24VisitFunctionDeclarationEPNS0_19FunctionDeclarationE:
.LFB27297:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE27297:
	.size	_ZN2v88internal9Processor24VisitFunctionDeclarationEPNS0_19FunctionDeclarationE, .-_ZN2v88internal9Processor24VisitFunctionDeclarationEPNS0_19FunctionDeclarationE
	.section	.text._ZN2v88internal8ZoneListIPNS0_9StatementEE3AddERKS3_PNS0_4ZoneE,"axG",@progbits,_ZN2v88internal8ZoneListIPNS0_9StatementEE3AddERKS3_PNS0_4ZoneE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8ZoneListIPNS0_9StatementEE3AddERKS3_PNS0_4ZoneE
	.type	_ZN2v88internal8ZoneListIPNS0_9StatementEE3AddERKS3_PNS0_4ZoneE, @function
_ZN2v88internal8ZoneListIPNS0_9StatementEE3AddERKS3_PNS0_4ZoneE:
.LFB23474:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movslq	12(%rdi), %rax
	movl	8(%rdi), %ecx
	cmpl	%ecx, %eax
	jge	.L132
	leal	1(%rax), %ecx
	movq	(%rdi), %rdx
	movl	%ecx, 12(%rdi)
	movq	(%rsi), %rcx
	movq	%rcx, (%rdx,%rax,8)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L132:
	.cfi_restore_state
	leal	1(%rcx,%rcx), %r13d
	movq	24(%rdx), %rax
	movq	16(%rdx), %rcx
	movq	(%rsi), %r12
	movslq	%r13d, %rsi
	salq	$3, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L138
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdx)
.L135:
	movslq	12(%rbx), %rdx
	movq	%rdx, %rax
	salq	$3, %rdx
	testl	%eax, %eax
	jg	.L139
.L136:
	addl	$1, %eax
	movl	%r13d, 8(%rbx)
	movq	%rcx, (%rbx)
	movl	%eax, 12(%rbx)
	movq	%r12, (%rcx,%rdx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L139:
	.cfi_restore_state
	movq	(%rbx), %rsi
	movq	%rcx, %rdi
	call	memcpy@PLT
	movslq	12(%rbx), %rdx
	movq	%rax, %rcx
	movq	%rdx, %rax
	salq	$3, %rdx
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L138:
	movq	%rdx, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L135
	.cfi_endproc
.LFE23474:
	.size	_ZN2v88internal8ZoneListIPNS0_9StatementEE3AddERKS3_PNS0_4ZoneE, .-_ZN2v88internal8ZoneListIPNS0_9StatementEE3AddERKS3_PNS0_4ZoneE
	.section	.text._ZN2v88internal9Processor21AssignUndefinedBeforeEPNS0_9StatementE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Processor21AssignUndefinedBeforeEPNS0_9StatementE
	.type	_ZN2v88internal9Processor21AssignUndefinedBeforeEPNS0_9StatementE, @function
_ZN2v88internal9Processor21AssignUndefinedBeforeEPNS0_9StatementE:
.LFB21100:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%rsi, -72(%rbp)
	movq	32(%rdi), %rdi
	movq	16(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	subq	%r13, %rax
	cmpq	$15, %rax
	jbe	.L182
	leaq	16(%r13), %rax
	movq	%rax, 16(%rdi)
.L142:
	movabsq	$3478923509759, %rax
	movq	%rax, 0(%r13)
	movq	32(%rbx), %rdi
	movb	$1, 81(%rbx)
	movq	(%rbx), %r14
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	$23, %rax
	jbe	.L183
	leaq	24(%r12), %rax
	movq	%rax, 16(%rdi)
.L144:
	movl	$-1, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal13VariableProxyC1EPNS0_8VariableEi@PLT
	movl	4(%r12), %eax
	movl	%eax, %edx
	andl	$63, %edx
	cmpb	$54, %dl
	je	.L184
.L146:
	movq	32(%rbx), %rdi
	movq	16(%rdi), %r14
	movq	24(%rdi), %rax
	subq	%r14, %rax
	cmpq	$23, %rax
	jbe	.L185
	leaq	24(%r14), %rax
	movq	%rax, 16(%rdi)
.L152:
	movq	%r12, %rcx
	movq	%r14, %rdi
	movl	$-1, %r9d
	movq	%r13, %r8
	movl	$17, %edx
	movl	$24, %esi
	call	_ZN2v88internal10AssignmentC1ENS0_7AstNode8NodeTypeENS0_5Token5ValueEPNS0_10ExpressionES7_i@PLT
	movq	32(%rbx), %rdi
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	$31, %rax
	jbe	.L186
	leaq	32(%r12), %rax
	movq	%rax, 16(%rdi)
.L154:
	movabsq	$309237645311, %rax
	movq	32(%rbx), %rdi
	movq	%rax, (%r12)
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L187
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L156:
	movq	%rax, 8(%r12)
	leaq	8(%r12), %r13
	movq	$2, 16(%r12)
	movq	$0, 24(%r12)
	movq	32(%rbx), %rdi
	movq	16(%rbx), %r15
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L188
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L158:
	movq	%r14, 8(%rax)
	leaq	-64(%rbp), %rsi
	movq	%r15, %rdx
	movq	%r13, %rdi
	movabsq	$42949672959, %rcx
	movq	%rcx, (%rax)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8ZoneListIPNS0_9StatementEE3AddERKS3_PNS0_4ZoneE
	movq	16(%rbx), %rdx
	leaq	-72(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8ZoneListIPNS0_9StatementEE3AddERKS3_PNS0_4ZoneE
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L189
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L184:
	.cfi_restore_state
	movl	%eax, %edx
	orb	$-128, %dl
	movl	%edx, 4(%r12)
	testb	$1, %ah
	je	.L146
	movq	8(%r12), %r14
	movzwl	40(%r14), %eax
	movl	%eax, %edx
	andl	$15, %edx
	cmpb	$1, %dl
	je	.L146
	movq	16(%r14), %r15
	testq	%r15, %r15
	je	.L148
	testb	$64, %ah
	je	.L190
.L148:
	orb	$64, %ah
	movw	%ax, 40(%r14)
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L186:
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L187:
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L188:
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L182:
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r13
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L183:
	movl	$24, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L190:
	movzwl	40(%r15), %edx
	movl	%edx, %ecx
	andl	$15, %ecx
	cmpb	$1, %cl
	je	.L148
	movq	16(%r15), %rax
	testq	%rax, %rax
	je	.L149
	testb	$64, %dh
	je	.L191
.L149:
	orb	$64, %dh
	movw	%dx, 40(%r15)
	movzwl	40(%r14), %eax
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L185:
	movl	$24, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r14
	jmp	.L152
.L191:
	movzwl	40(%rax), %ecx
	movl	%ecx, %esi
	andl	$15, %esi
	cmpb	$1, %sil
	je	.L149
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.L150
	testb	$64, %ch
	je	.L192
.L150:
	orb	$64, %ch
	movw	%cx, 40(%rax)
	movzwl	40(%r15), %edx
	jmp	.L149
.L192:
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8Variable16SetMaybeAssignedEv
	movq	-80(%rbp), %rax
	movzwl	40(%rax), %ecx
	jmp	.L150
.L189:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21100:
	.size	_ZN2v88internal9Processor21AssignUndefinedBeforeEPNS0_9StatementE, .-_ZN2v88internal9Processor21AssignUndefinedBeforeEPNS0_9StatementE
	.section	.rodata._ZN2v88internal9Processor24VisitTryFinallyStatementEPNS0_19TryFinallyStatementE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"(closure_scope()) != nullptr"
.LC2:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal9Processor24VisitTryFinallyStatementEPNS0_19TryFinallyStatementE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Processor24VisitTryFinallyStatementEPNS0_19TryFinallyStatementE
	.type	_ZN2v88internal9Processor24VisitTryFinallyStatementEPNS0_19TryFinallyStatementE, @function
_ZN2v88internal9Processor24VisitTryFinallyStatementEPNS0_19TryFinallyStatementE:
.LFB21112:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 83(%rdi)
	movzbl	80(%rdi), %eax
	je	.L194
	movb	$1, 82(%rdi)
	testb	%al, %al
	je	.L278
.L195:
	movq	8(%rbx), %rax
	movzbl	4(%rax), %edx
	andl	$63, %edx
	cmpb	$7, %dl
	movl	$0, %edx
	cmovne	%rdx, %rax
	movq	%rax, 16(%r12)
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L279
	movq	40(%rbx), %rax
	movq	56(%rax), %rax
	movq	248(%rax), %rsi
	call	_ZN2v88internal5Scope12NewTemporaryEPKNS0_12AstRawStringE@PLT
	movq	32(%rbx), %rdi
	movq	%rax, %r13
	movq	16(%rdi), %r15
	movq	24(%rdi), %rax
	subq	%r15, %rax
	cmpq	$23, %rax
	jbe	.L280
	leaq	24(%r15), %rax
	movq	%rax, 16(%rdi)
.L200:
	movq	%r13, %rsi
	movq	%r15, %rdi
	movl	$-1, %edx
	call	_ZN2v88internal13VariableProxyC1EPNS0_8VariableEi@PLT
	movq	32(%rbx), %rdi
	movq	(%rbx), %r14
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	subq	%r13, %rax
	cmpq	$23, %rax
	jbe	.L281
	leaq	24(%r13), %rax
	movq	%rax, 16(%rdi)
.L202:
	movl	$-1, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal13VariableProxyC1EPNS0_8VariableEi@PLT
	movl	4(%r15), %eax
	movl	%eax, %edx
	andl	$63, %edx
	cmpb	$54, %dl
	je	.L282
.L204:
	movq	32(%rbx), %rdi
	movq	16(%rdi), %r10
	movq	24(%rdi), %rax
	subq	%r10, %rax
	cmpq	$23, %rax
	jbe	.L283
	leaq	24(%r10), %rax
	movq	%rax, 16(%rdi)
.L210:
	movl	$17, %edx
	movq	%r10, %rdi
	movq	%r13, %r8
	movq	%r15, %rcx
	movl	$-1, %r9d
	movl	$24, %esi
	movq	%r10, -72(%rbp)
	call	_ZN2v88internal10AssignmentC1ENS0_7AstNode8NodeTypeENS0_5Token5ValueEPNS0_10ExpressionES7_i@PLT
	movl	4(%r13), %eax
	movq	-72(%rbp), %r10
	movl	%eax, %edx
	andl	$63, %edx
	cmpb	$54, %dl
	je	.L284
.L212:
	movq	32(%rbx), %rdi
	movq	16(%rdi), %r14
	movq	24(%rdi), %rax
	subq	%r14, %rax
	cmpq	$23, %rax
	jbe	.L285
	leaq	24(%r14), %rax
	movq	%rax, 16(%rdi)
.L218:
	movq	%r13, %rcx
	movq	%r15, %r8
	movl	$17, %edx
	movq	%r14, %rdi
	movl	$-1, %r9d
	movl	$24, %esi
	movq	%r10, -72(%rbp)
	call	_ZN2v88internal10AssignmentC1ENS0_7AstNode8NodeTypeENS0_5Token5ValueEPNS0_10ExpressionES7_i@PLT
	movq	32(%rbx), %rdi
	movq	16(%r12), %r13
	movq	16(%rbx), %rdx
	movq	-72(%rbp), %r10
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	leaq	8(%r13), %r8
	subq	%rax, %rcx
	cmpq	$15, %rcx
	jbe	.L286
	leaq	16(%rax), %rcx
	movq	%rcx, 16(%rdi)
.L220:
	movq	%r10, 8(%rax)
	leaq	-64(%rbp), %r15
	movq	%r8, %rdi
	movabsq	$42949672959, %rcx
	movq	%rcx, (%rax)
	movq	%r15, %rsi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8ZoneListIPNS0_9StatementEE3AddERKS3_PNS0_4ZoneE
	movl	20(%r13), %edx
	leal	-1(%rdx), %eax
	testl	%eax, %eax
	jle	.L224
	movslq	%edx, %rcx
	subl	$2, %edx
	cltq
	subq	%rdx, %rcx
	salq	$3, %rax
	leaq	-16(,%rcx,8), %rsi
	.p2align 4,,10
	.p2align 3
.L225:
	movq	8(%r13), %rdx
	movq	-8(%rdx,%rax), %rcx
	movq	%rcx, (%rdx,%rax)
	subq	$8, %rax
	cmpq	%rax, %rsi
	jne	.L225
.L224:
	movq	8(%r13), %rax
	movq	-64(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	16(%r12), %rax
	movq	32(%rbx), %rdi
	movq	16(%rbx), %r8
	leaq	8(%rax), %r13
	movq	24(%rdi), %rdx
	movq	16(%rdi), %rax
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L287
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L226:
	movq	%r14, 8(%rax)
	movq	%r8, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movabsq	$42949672959, %rcx
	movq	%rcx, (%rax)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8ZoneListIPNS0_9StatementEE3AddERKS3_PNS0_4ZoneE
	movb	$0, 82(%rbx)
	movzbl	80(%rbx), %eax
.L194:
	testb	%al, %al
	je	.L288
.L227:
	movq	8(%rbx), %rax
	movzbl	4(%rax), %edx
	andl	$63, %edx
	cmpb	$7, %dl
	movl	$0, %edx
	cmovne	%rdx, %rax
	movq	%rax, 8(%r12)
	cmpb	$0, 82(%rbx)
	je	.L289
.L230:
	movq	%r12, 8(%rbx)
	movb	$1, 82(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L290
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L288:
	.cfi_restore_state
	movq	8(%r12), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	72(%rbx), %rax
	jb	.L291
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Processor25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L278:
	movq	16(%rsi), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	72(%rbx), %rax
	jb	.L292
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Processor25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L289:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Processor21AssignUndefinedBeforeEPNS0_9StatementE
	movq	%rax, %r12
	jmp	.L230
	.p2align 4,,10
	.p2align 3
.L292:
	movb	$1, 80(%rbx)
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L291:
	movb	$1, 80(%rbx)
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L282:
	movl	%eax, %edx
	orb	$-128, %dl
	movl	%edx, 4(%r15)
	testb	$1, %ah
	je	.L204
	movq	8(%r15), %r14
	movzwl	40(%r14), %eax
	movl	%eax, %edx
	andl	$15, %edx
	cmpb	$1, %dl
	je	.L204
	movq	16(%r14), %rdx
	testq	%rdx, %rdx
	je	.L206
	testb	$64, %ah
	je	.L293
.L206:
	orb	$64, %ah
	movw	%ax, 40(%r14)
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L284:
	movl	%eax, %edx
	orb	$-128, %dl
	movl	%edx, 4(%r13)
	testb	$1, %ah
	je	.L212
	movq	8(%r13), %r14
	movzwl	40(%r14), %eax
	movl	%eax, %edx
	andl	$15, %edx
	cmpb	$1, %dl
	je	.L212
	movq	16(%r14), %rdx
	testq	%rdx, %rdx
	je	.L214
	testb	$64, %ah
	je	.L294
.L214:
	orb	$64, %ah
	movw	%ax, 40(%r14)
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L279:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L286:
	movl	$16, %esi
	movq	%r10, -88(%rbp)
	movq	%r8, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-72(%rbp), %rdx
	movq	-80(%rbp), %r8
	movq	-88(%rbp), %r10
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L287:
	movl	$16, %esi
	movq	%r8, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-72(%rbp), %r8
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L281:
	movl	$24, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r13
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L280:
	movl	$24, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r15
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L283:
	movl	$24, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r10
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L285:
	movl	$24, %esi
	movq	%r10, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-72(%rbp), %r10
	movq	%rax, %r14
	jmp	.L218
.L293:
	movzwl	40(%rdx), %ecx
	movl	%ecx, %esi
	andl	$15, %esi
	cmpb	$1, %sil
	je	.L206
	movq	16(%rdx), %rax
	testq	%rax, %rax
	je	.L207
	testb	$64, %ch
	je	.L295
.L207:
	orb	$64, %ch
	movw	%cx, 40(%rdx)
	movzwl	40(%r14), %eax
	jmp	.L206
.L294:
	movzwl	40(%rdx), %ecx
	movl	%ecx, %esi
	andl	$15, %esi
	cmpb	$1, %sil
	je	.L214
	movq	16(%rdx), %rax
	testq	%rax, %rax
	je	.L215
	testb	$64, %ch
	je	.L296
.L215:
	orb	$64, %ch
	movw	%cx, 40(%rdx)
	movzwl	40(%r14), %eax
	jmp	.L214
.L296:
	movzwl	40(%rax), %esi
	movl	%esi, %edi
	andl	$15, %edi
	cmpb	$1, %dil
	je	.L215
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.L216
	testw	$16384, %si
	je	.L297
.L216:
	orw	$16384, %si
	movw	%si, 40(%rax)
	movzwl	40(%rdx), %ecx
	jmp	.L215
.L295:
	movzwl	40(%rax), %esi
	movl	%esi, %edi
	andl	$15, %edi
	cmpb	$1, %dil
	je	.L207
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.L208
	testw	$16384, %si
	je	.L298
.L208:
	orw	$16384, %si
	movw	%si, 40(%rax)
	movzwl	40(%rdx), %ecx
	jmp	.L207
.L290:
	call	__stack_chk_fail@PLT
.L298:
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8Variable16SetMaybeAssignedEv
	movq	-80(%rbp), %rax
	movq	-72(%rbp), %rdx
	movzwl	40(%rax), %esi
	jmp	.L208
.L297:
	movq	%rax, -88(%rbp)
	movq	%rdx, -80(%rbp)
	call	_ZN2v88internal8Variable16SetMaybeAssignedEv
	movq	-88(%rbp), %rax
	movq	-80(%rbp), %rdx
	movq	-72(%rbp), %r10
	movzwl	40(%rax), %esi
	jmp	.L216
	.cfi_endproc
.LFE21112:
	.size	_ZN2v88internal9Processor24VisitTryFinallyStatementEPNS0_19TryFinallyStatementE, .-_ZN2v88internal9Processor24VisitTryFinallyStatementEPNS0_19TryFinallyStatementE
	.section	.text._ZN2v88internal9Processor25VisitNoStackOverflowCheckEPNS0_7AstNodeE,"axG",@progbits,_ZN2v88internal9Processor25VisitNoStackOverflowCheckEPNS0_7AstNodeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal9Processor25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	.type	_ZN2v88internal9Processor25VisitNoStackOverflowCheckEPNS0_7AstNodeE, @function
_ZN2v88internal9Processor25VisitNoStackOverflowCheckEPNS0_7AstNodeE:
.LFB21091:
	.cfi_startproc
	endbr64
	movzbl	4(%rsi), %edx
	andl	$63, %edx
	cmpb	$57, %dl
	ja	.L367
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.L302(%rip), %rcx
	movzbl	%dl, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal9Processor25VisitNoStackOverflowCheckEPNS0_7AstNodeE,"aG",@progbits,_ZN2v88internal9Processor25VisitNoStackOverflowCheckEPNS0_7AstNodeE,comdat
	.align 4
	.align 4
.L302:
	.long	.L301-.L302
	.long	.L301-.L302
	.long	.L314-.L302
	.long	.L314-.L302
	.long	.L314-.L302
	.long	.L314-.L302
	.long	.L314-.L302
	.long	.L313-.L302
	.long	.L312-.L302
	.long	.L311-.L302
	.long	.L370-.L302
	.long	.L310-.L302
	.long	.L309-.L302
	.long	.L308-.L302
	.long	.L308-.L302
	.long	.L307-.L302
	.long	.L306-.L302
	.long	.L305-.L302
	.long	.L304-.L302
	.long	.L370-.L302
	.long	.L370-.L302
	.long	.L301-.L302
	.long	.L301-.L302
	.long	.L301-.L302
	.long	.L301-.L302
	.long	.L301-.L302
	.long	.L301-.L302
	.long	.L301-.L302
	.long	.L301-.L302
	.long	.L301-.L302
	.long	.L301-.L302
	.long	.L301-.L302
	.long	.L301-.L302
	.long	.L301-.L302
	.long	.L301-.L302
	.long	.L301-.L302
	.long	.L301-.L302
	.long	.L301-.L302
	.long	.L301-.L302
	.long	.L301-.L302
	.long	.L301-.L302
	.long	.L301-.L302
	.long	.L301-.L302
	.long	.L301-.L302
	.long	.L301-.L302
	.long	.L301-.L302
	.long	.L301-.L302
	.long	.L301-.L302
	.long	.L301-.L302
	.long	.L301-.L302
	.long	.L301-.L302
	.long	.L301-.L302
	.long	.L301-.L302
	.long	.L301-.L302
	.long	.L301-.L302
	.long	.L301-.L302
	.long	.L301-.L302
	.long	.L301-.L302
	.section	.text._ZN2v88internal9Processor25VisitNoStackOverflowCheckEPNS0_7AstNodeE,"axG",@progbits,_ZN2v88internal9Processor25VisitNoStackOverflowCheckEPNS0_7AstNodeE,comdat
	.p2align 4,,10
	.p2align 3
.L314:
	cmpb	$0, 80(%rdi)
	movzbl	83(%rdi), %r14d
	movb	$1, 83(%rdi)
	je	.L377
.L327:
	movq	8(%r12), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, 24(%r13)
	call	_ZN2v88internal9Processor21AssignUndefinedBeforeEPNS0_9StatementE
	movb	$1, 82(%r12)
	movq	%rax, 8(%r12)
	movb	%r14b, 83(%r12)
.L299:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L311:
	.cfi_restore_state
	cmpb	$0, 82(%rdi)
	jne	.L370
	movq	8(%rsi), %r15
	movq	(%rdi), %r8
	movb	$1, 81(%rdi)
	movq	32(%rdi), %rdi
	movq	16(%rdi), %r14
	movq	24(%rdi), %rax
	subq	%r14, %rax
	cmpq	$23, %rax
	jbe	.L378
	leaq	24(%r14), %rax
	movq	%rax, 16(%rdi)
.L331:
	movl	$-1, %edx
	movq	%r8, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal13VariableProxyC1EPNS0_8VariableEi@PLT
	movl	4(%r14), %eax
	movl	%eax, %edx
	andl	$63, %edx
	cmpb	$54, %dl
	je	.L379
.L333:
	movq	32(%r12), %rdi
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$23, %rax
	jbe	.L380
	leaq	24(%rbx), %rax
	movq	%rax, 16(%rdi)
.L339:
	movl	$-1, %r9d
	movq	%r15, %r8
	movq	%r14, %rcx
	movq	%rbx, %rdi
	movl	$17, %edx
	movl	$24, %esi
	call	_ZN2v88internal10AssignmentC1ENS0_7AstNode8NodeTypeENS0_5Token5ValueEPNS0_10ExpressionES7_i@PLT
	movq	%rbx, 8(%r13)
	movb	$1, 82(%r12)
.L370:
	movq	%r13, 8(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L377:
	.cfi_restore_state
	movq	24(%rsi), %r15
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	72(%r12), %rax
	jb	.L381
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Processor25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L308:
	movb	$0, 82(%rdi)
	movq	%rsi, 8(%r12)
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L301:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L307:
	movb	$1, 82(%rdi)
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L306:
	cmpb	$0, 80(%rdi)
	jne	.L342
	movq	24(%rsi), %r14
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	72(%r12), %rax
	jb	.L382
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Processor25VisitNoStackOverflowCheckEPNS0_7AstNodeE
.L342:
	movq	8(%r12), %rax
	movq	%rax, 24(%r13)
	cmpb	$0, 82(%r12)
	je	.L383
.L344:
	movq	%r13, 8(%r12)
	movb	$1, 82(%r12)
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L305:
	addq	$24, %rsp
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Processor22VisitTryCatchStatementEPNS0_17TryCatchStatementE
	.p2align 4,,10
	.p2align 3
.L312:
	.cfi_restore_state
	addq	$24, %rsp
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Processor20VisitSwitchStatementEPNS0_15SwitchStatementE
	.p2align 4,,10
	.p2align 3
.L309:
	.cfi_restore_state
	addq	$24, %rsp
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Processor16VisitIfStatementEPNS0_11IfStatementE
	.p2align 4,,10
	.p2align 3
.L310:
	.cfi_restore_state
	cmpb	$0, 80(%rdi)
	jne	.L340
	movq	16(%rsi), %r14
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	72(%r12), %rax
	jb	.L384
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Processor25VisitNoStackOverflowCheckEPNS0_7AstNodeE
.L340:
	movq	8(%r12), %rax
	movq	%rax, 16(%r13)
	movq	%r13, 8(%r12)
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L313:
	addq	$24, %rsp
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Processor10VisitBlockEPNS0_5BlockE
	.p2align 4,,10
	.p2align 3
.L304:
	.cfi_restore_state
	addq	$24, %rsp
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Processor24VisitTryFinallyStatementEPNS0_19TryFinallyStatementE
	.p2align 4,,10
	.p2align 3
.L381:
	.cfi_restore_state
	movb	$1, 80(%r12)
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L383:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Processor21AssignUndefinedBeforeEPNS0_9StatementE
	movq	%rax, %r13
	jmp	.L344
.L382:
	movb	$1, 80(%r12)
	jmp	.L342
.L384:
	movb	$1, 80(%r12)
	jmp	.L340
.L379:
	movl	%eax, %edx
	orb	$-128, %dl
	movl	%edx, 4(%r14)
	testb	$1, %ah
	je	.L333
	movq	8(%r14), %rsi
	movzwl	40(%rsi), %eax
	movl	%eax, %edx
	andl	$15, %edx
	cmpb	$1, %dl
	je	.L333
	movq	16(%rsi), %rbx
	testq	%rbx, %rbx
	je	.L335
	testb	$64, %ah
	je	.L385
.L335:
	orb	$64, %ah
	movw	%ax, 40(%rsi)
	jmp	.L333
.L378:
	movl	$24, %esi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r8
	movq	%rax, %r14
	jmp	.L331
.L380:
	movl	$24, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rbx
	jmp	.L339
.L385:
	movzwl	40(%rbx), %edx
	movl	%edx, %ecx
	andl	$15, %ecx
	subb	$1, %cl
	je	.L335
	movq	16(%rbx), %rcx
	testq	%rcx, %rcx
	je	.L336
	testb	$64, %dh
	je	.L386
.L336:
	orb	$64, %dh
	movw	%dx, 40(%rbx)
	movzwl	40(%rsi), %eax
	jmp	.L335
.L386:
	movzwl	40(%rcx), %eax
	movl	%eax, %edi
	andl	$15, %edi
	subb	$1, %dil
	je	.L336
	movq	16(%rcx), %rdi
	testq	%rdi, %rdi
	je	.L337
	testb	$64, %ah
	je	.L387
.L337:
	orb	$64, %ah
	movw	%ax, 40(%rcx)
	movzwl	40(%rbx), %edx
	jmp	.L336
.L367:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
.L387:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	%rcx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal8Variable16SetMaybeAssignedEv
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %rsi
	movzwl	40(%rcx), %eax
	jmp	.L337
	.cfi_endproc
.LFE21091:
	.size	_ZN2v88internal9Processor25VisitNoStackOverflowCheckEPNS0_7AstNodeE, .-_ZN2v88internal9Processor25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	.section	.text._ZN2v88internal9Processor7ProcessEPNS0_8ZoneListIPNS0_9StatementEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Processor7ProcessEPNS0_8ZoneListIPNS0_9StatementEEE
	.type	_ZN2v88internal9Processor7ProcessEPNS0_8ZoneListIPNS0_9StatementEEE, @function
_ZN2v88internal9Processor7ProcessEPNS0_8ZoneListIPNS0_9StatementEEE:
.LFB21101:
	.cfi_startproc
	endbr64
	movslq	12(%rsi), %rax
	movl	%eax, %edx
	subl	$1, %edx
	js	.L396
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movslq	%edx, %r12
	movl	%edx, %edx
	pushq	%rbx
	subq	%rdx, %rax
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	salq	$3, %r12
	leaq	-16(,%rax,8), %r13
	subq	$8, %rsp
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L393:
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Processor25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movq	(%r14), %rax
	addq	%r12, %rax
.L392:
	movq	8(%rbx), %rdx
	subq	$8, %r12
	movq	%rdx, (%rax)
	cmpq	%r13, %r12
	je	.L388
.L394:
	cmpb	$0, 83(%rbx)
	jne	.L390
	cmpb	$0, 82(%rbx)
	jne	.L388
.L390:
	movq	(%r14), %rax
	addq	%r12, %rax
	cmpb	$0, 80(%rbx)
	jne	.L392
	movq	(%rax), %r15
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	72(%rbx), %rax
	jnb	.L393
	movb	$1, 80(%rbx)
	movq	(%r14), %rax
	movq	8(%rbx), %rdx
	addq	%r12, %rax
	subq	$8, %r12
	movq	%rdx, (%rax)
	cmpq	%r13, %r12
	jne	.L394
.L388:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
.L396:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE21101:
	.size	_ZN2v88internal9Processor7ProcessEPNS0_8ZoneListIPNS0_9StatementEEE, .-_ZN2v88internal9Processor7ProcessEPNS0_8ZoneListIPNS0_9StatementEEE
	.section	.text._ZN2v88internal9Processor18VisitWithStatementEPNS0_13WithStatementE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Processor18VisitWithStatementEPNS0_13WithStatementE
	.type	_ZN2v88internal9Processor18VisitWithStatementEPNS0_13WithStatementE, @function
_ZN2v88internal9Processor18VisitWithStatementEPNS0_13WithStatementE:
.LFB21116:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpb	$0, 80(%rdi)
	je	.L404
.L400:
	movq	8(%rbx), %rax
	movq	%rax, 24(%r12)
	cmpb	$0, 82(%rbx)
	je	.L405
.L402:
	movq	%r12, 8(%rbx)
	movb	$1, 82(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L404:
	.cfi_restore_state
	movq	24(%rsi), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	72(%rbx), %rax
	jb	.L406
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Processor25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movq	8(%rbx), %rax
	movq	%rax, 24(%r12)
	cmpb	$0, 82(%rbx)
	jne	.L402
.L405:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Processor21AssignUndefinedBeforeEPNS0_9StatementE
	movq	%rax, %r12
	jmp	.L402
	.p2align 4,,10
	.p2align 3
.L406:
	movb	$1, 80(%rbx)
	jmp	.L400
	.cfi_endproc
.LFE21116:
	.size	_ZN2v88internal9Processor18VisitWithStatementEPNS0_13WithStatementE, .-_ZN2v88internal9Processor18VisitWithStatementEPNS0_13WithStatementE
	.section	.text._ZN2v88internal9Processor23VisitIterationStatementEPNS0_18IterationStatementE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Processor23VisitIterationStatementEPNS0_18IterationStatementE
	.type	_ZN2v88internal9Processor23VisitIterationStatementEPNS0_18IterationStatementE, @function
_ZN2v88internal9Processor23VisitIterationStatementEPNS0_18IterationStatementE:
.LFB21105:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 80(%rdi)
	movq	%rdi, %rbx
	movzbl	83(%rdi), %r13d
	movb	$1, 83(%rdi)
	je	.L411
.L408:
	movq	8(%rbx), %rax
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, 24(%r12)
	call	_ZN2v88internal9Processor21AssignUndefinedBeforeEPNS0_9StatementE
	movb	%r13b, 83(%rbx)
	movq	%rax, 8(%rbx)
	movb	$1, 82(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L411:
	.cfi_restore_state
	movq	24(%rsi), %r14
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	72(%rbx), %rax
	jb	.L412
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Processor25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L412:
	movb	$1, 80(%rbx)
	jmp	.L408
	.cfi_endproc
.LFE21105:
	.size	_ZN2v88internal9Processor23VisitIterationStatementEPNS0_18IterationStatementE, .-_ZN2v88internal9Processor23VisitIterationStatementEPNS0_18IterationStatementE
	.section	.text._ZN2v88internal9Processor20VisitSwitchStatementEPNS0_15SwitchStatementE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Processor20VisitSwitchStatementEPNS0_15SwitchStatementE
	.type	_ZN2v88internal9Processor20VisitSwitchStatementEPNS0_15SwitchStatementE, @function
_ZN2v88internal9Processor20VisitSwitchStatementEPNS0_15SwitchStatementE:
.LFB21113:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movzbl	83(%rdi), %eax
	movb	$1, 83(%rdi)
	movb	%al, -73(%rbp)
	movslq	36(%rsi), %rax
	movl	%eax, %edx
	subl	$1, %edx
	js	.L416
	movslq	%edx, %rbx
	movl	%edx, %edx
	subq	%rdx, %rax
	salq	$3, %rbx
	leaq	-16(,%rax,8), %rax
	movq	%rax, -72(%rbp)
	.p2align 4,,10
	.p2align 3
.L417:
	movq	24(%r13), %rax
	movq	(%rax,%rbx), %r12
	movslq	20(%r12), %rax
	movl	%eax, %esi
	subl	$1, %esi
	js	.L419
	movslq	%esi, %rdx
	movl	%esi, %esi
	subq	%rsi, %rax
	leaq	0(,%rdx,8), %r14
	leaq	-16(,%rax,8), %rax
	movq	%rax, -64(%rbp)
	jmp	.L422
	.p2align 4,,10
	.p2align 3
.L421:
	movq	%r15, %rdi
	call	_ZN2v88internal9Processor25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movq	8(%r12), %rax
	addq	%r14, %rax
.L420:
	movq	8(%r15), %rsi
	subq	$8, %r14
	movq	%rsi, (%rax)
	cmpq	-64(%rbp), %r14
	je	.L419
.L422:
	cmpb	$0, 83(%r15)
	jne	.L418
	cmpb	$0, 82(%r15)
	jne	.L419
.L418:
	movq	8(%r12), %rax
	addq	%r14, %rax
	cmpb	$0, 80(%r15)
	jne	.L420
	movq	(%rax), %rsi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	72(%r15), %rax
	movq	-56(%rbp), %rsi
	jnb	.L421
	movb	$1, 80(%r15)
	movq	8(%r12), %rax
	addq	%r14, %rax
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L419:
	subq	$8, %rbx
	cmpq	%rbx, -72(%rbp)
	jne	.L417
.L416:
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal9Processor21AssignUndefinedBeforeEPNS0_9StatementE
	movb	$1, 82(%r15)
	movq	%rax, 8(%r15)
	movzbl	-73(%rbp), %eax
	movb	%al, 83(%r15)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21113:
	.size	_ZN2v88internal9Processor20VisitSwitchStatementEPNS0_15SwitchStatementE, .-_ZN2v88internal9Processor20VisitSwitchStatementEPNS0_15SwitchStatementE
	.section	.text._ZN2v88internal9Processor22VisitTryCatchStatementEPNS0_17TryCatchStatementE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Processor22VisitTryCatchStatementEPNS0_17TryCatchStatementE
	.type	_ZN2v88internal9Processor22VisitTryCatchStatementEPNS0_17TryCatchStatementE, @function
_ZN2v88internal9Processor22VisitTryCatchStatementEPNS0_17TryCatchStatementE:
.LFB21111:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 80(%rdi)
	movq	%rdi, %rbx
	movzbl	82(%rdi), %r13d
	je	.L434
.L426:
	movq	8(%rbx), %rax
	movq	%rax, 8(%r12)
	cmpb	$0, 80(%rbx)
	movzbl	82(%rbx), %r14d
	movb	%r13b, 82(%rbx)
	je	.L435
.L428:
	movq	8(%rbx), %rax
	movq	%rax, 24(%r12)
	cmpb	$0, 82(%rbx)
	je	.L432
	testb	%r14b, %r14b
	je	.L432
.L430:
	movq	%r12, 8(%rbx)
	movb	$1, 82(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L432:
	.cfi_restore_state
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Processor21AssignUndefinedBeforeEPNS0_9StatementE
	movq	%rax, %r12
	jmp	.L430
	.p2align 4,,10
	.p2align 3
.L434:
	movq	8(%rsi), %r14
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	72(%rbx), %rax
	jb	.L436
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Processor25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	jmp	.L426
	.p2align 4,,10
	.p2align 3
.L435:
	movq	24(%r12), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	72(%rbx), %rax
	jb	.L437
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Processor25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	jmp	.L428
	.p2align 4,,10
	.p2align 3
.L436:
	movb	$1, 80(%rbx)
	jmp	.L426
	.p2align 4,,10
	.p2align 3
.L437:
	movb	$1, 80(%rbx)
	jmp	.L428
	.cfi_endproc
.LFE21111:
	.size	_ZN2v88internal9Processor22VisitTryCatchStatementEPNS0_17TryCatchStatementE, .-_ZN2v88internal9Processor22VisitTryCatchStatementEPNS0_17TryCatchStatementE
	.section	.text._ZN2v88internal9Processor16VisitIfStatementEPNS0_11IfStatementE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Processor16VisitIfStatementEPNS0_11IfStatementE
	.type	_ZN2v88internal9Processor16VisitIfStatementEPNS0_11IfStatementE, @function
_ZN2v88internal9Processor16VisitIfStatementEPNS0_11IfStatementE:
.LFB21104:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 80(%rdi)
	movq	%rdi, %rbx
	movzbl	82(%rdi), %r14d
	je	.L449
.L439:
	movq	8(%rbx), %rax
	movq	%rax, 16(%r12)
	cmpb	$0, 80(%rbx)
	movzbl	82(%rbx), %r13d
	movb	%r14b, 82(%rbx)
	je	.L450
.L441:
	movq	8(%rbx), %rax
	movq	%rax, 24(%r12)
	testb	%r13b, %r13b
	je	.L443
	cmpb	$0, 82(%rbx)
	jne	.L444
.L443:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Processor21AssignUndefinedBeforeEPNS0_9StatementE
	movq	%rax, %r12
.L444:
	movq	%r12, 8(%rbx)
	movb	$1, 82(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L449:
	.cfi_restore_state
	movq	16(%rsi), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	72(%rbx), %rax
	jb	.L451
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Processor25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	jmp	.L439
	.p2align 4,,10
	.p2align 3
.L450:
	movq	24(%r12), %r14
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	72(%rbx), %rax
	jb	.L452
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Processor25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	jmp	.L441
	.p2align 4,,10
	.p2align 3
.L451:
	movb	$1, 80(%rbx)
	jmp	.L439
	.p2align 4,,10
	.p2align 3
.L452:
	movb	$1, 80(%rbx)
	jmp	.L441
	.cfi_endproc
.LFE21104:
	.size	_ZN2v88internal9Processor16VisitIfStatementEPNS0_11IfStatementE, .-_ZN2v88internal9Processor16VisitIfStatementEPNS0_11IfStatementE
	.section	.text._ZN2v88internal9Processor19VisitForInStatementEPNS0_14ForInStatementE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Processor19VisitForInStatementEPNS0_14ForInStatementE
	.type	_ZN2v88internal9Processor19VisitForInStatementEPNS0_14ForInStatementE, @function
_ZN2v88internal9Processor19VisitForInStatementEPNS0_14ForInStatementE:
.LFB21109:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 80(%rdi)
	movq	%rdi, %rbx
	movzbl	83(%rdi), %r13d
	movb	$1, 83(%rdi)
	je	.L457
.L454:
	movq	8(%rbx), %rax
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, 24(%r12)
	call	_ZN2v88internal9Processor21AssignUndefinedBeforeEPNS0_9StatementE
	movb	%r13b, 83(%rbx)
	movq	%rax, 8(%rbx)
	movb	$1, 82(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L457:
	.cfi_restore_state
	movq	24(%rsi), %r14
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	72(%rbx), %rax
	jb	.L458
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Processor25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	jmp	.L454
	.p2align 4,,10
	.p2align 3
.L458:
	movb	$1, 80(%rbx)
	jmp	.L454
	.cfi_endproc
.LFE21109:
	.size	_ZN2v88internal9Processor19VisitForInStatementEPNS0_14ForInStatementE, .-_ZN2v88internal9Processor19VisitForInStatementEPNS0_14ForInStatementE
	.section	.text._ZN2v88internal9Processor19VisitForOfStatementEPNS0_14ForOfStatementE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Processor19VisitForOfStatementEPNS0_14ForOfStatementE
	.type	_ZN2v88internal9Processor19VisitForOfStatementEPNS0_14ForOfStatementE, @function
_ZN2v88internal9Processor19VisitForOfStatementEPNS0_14ForOfStatementE:
.LFB21110:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 80(%rdi)
	movq	%rdi, %rbx
	movzbl	83(%rdi), %r13d
	movb	$1, 83(%rdi)
	je	.L463
.L460:
	movq	8(%rbx), %rax
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, 24(%r12)
	call	_ZN2v88internal9Processor21AssignUndefinedBeforeEPNS0_9StatementE
	movb	%r13b, 83(%rbx)
	movq	%rax, 8(%rbx)
	movb	$1, 82(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L463:
	.cfi_restore_state
	movq	24(%rsi), %r14
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	72(%rbx), %rax
	jb	.L464
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Processor25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	jmp	.L460
	.p2align 4,,10
	.p2align 3
.L464:
	movb	$1, 80(%rbx)
	jmp	.L460
	.cfi_endproc
.LFE21110:
	.size	_ZN2v88internal9Processor19VisitForOfStatementEPNS0_14ForOfStatementE, .-_ZN2v88internal9Processor19VisitForOfStatementEPNS0_14ForOfStatementE
	.section	.text._ZN2v88internal9Processor17VisitForStatementEPNS0_12ForStatementE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Processor17VisitForStatementEPNS0_12ForStatementE
	.type	_ZN2v88internal9Processor17VisitForStatementEPNS0_12ForStatementE, @function
_ZN2v88internal9Processor17VisitForStatementEPNS0_12ForStatementE:
.LFB21108:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 80(%rdi)
	movq	%rdi, %rbx
	movzbl	83(%rdi), %r13d
	movb	$1, 83(%rdi)
	je	.L469
.L466:
	movq	8(%rbx), %rax
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, 24(%r12)
	call	_ZN2v88internal9Processor21AssignUndefinedBeforeEPNS0_9StatementE
	movb	%r13b, 83(%rbx)
	movq	%rax, 8(%rbx)
	movb	$1, 82(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L469:
	.cfi_restore_state
	movq	24(%rsi), %r14
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	72(%rbx), %rax
	jb	.L470
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Processor25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	jmp	.L466
	.p2align 4,,10
	.p2align 3
.L470:
	movb	$1, 80(%rbx)
	jmp	.L466
	.cfi_endproc
.LFE21108:
	.size	_ZN2v88internal9Processor17VisitForStatementEPNS0_12ForStatementE, .-_ZN2v88internal9Processor17VisitForStatementEPNS0_12ForStatementE
	.section	.text._ZN2v88internal9Processor21VisitDoWhileStatementEPNS0_16DoWhileStatementE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Processor21VisitDoWhileStatementEPNS0_16DoWhileStatementE
	.type	_ZN2v88internal9Processor21VisitDoWhileStatementEPNS0_16DoWhileStatementE, @function
_ZN2v88internal9Processor21VisitDoWhileStatementEPNS0_16DoWhileStatementE:
.LFB21106:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 80(%rdi)
	movq	%rdi, %rbx
	movzbl	83(%rdi), %r13d
	movb	$1, 83(%rdi)
	je	.L475
.L472:
	movq	8(%rbx), %rax
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, 24(%r12)
	call	_ZN2v88internal9Processor21AssignUndefinedBeforeEPNS0_9StatementE
	movb	%r13b, 83(%rbx)
	movq	%rax, 8(%rbx)
	movb	$1, 82(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L475:
	.cfi_restore_state
	movq	24(%rsi), %r14
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	72(%rbx), %rax
	jb	.L476
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Processor25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L476:
	movb	$1, 80(%rbx)
	jmp	.L472
	.cfi_endproc
.LFE21106:
	.size	_ZN2v88internal9Processor21VisitDoWhileStatementEPNS0_16DoWhileStatementE, .-_ZN2v88internal9Processor21VisitDoWhileStatementEPNS0_16DoWhileStatementE
	.section	.text._ZN2v88internal9Processor19VisitWhileStatementEPNS0_14WhileStatementE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Processor19VisitWhileStatementEPNS0_14WhileStatementE
	.type	_ZN2v88internal9Processor19VisitWhileStatementEPNS0_14WhileStatementE, @function
_ZN2v88internal9Processor19VisitWhileStatementEPNS0_14WhileStatementE:
.LFB21107:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 80(%rdi)
	movq	%rdi, %rbx
	movzbl	83(%rdi), %r13d
	movb	$1, 83(%rdi)
	je	.L481
.L478:
	movq	8(%rbx), %rax
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, 24(%r12)
	call	_ZN2v88internal9Processor21AssignUndefinedBeforeEPNS0_9StatementE
	movb	%r13b, 83(%rbx)
	movq	%rax, 8(%rbx)
	movb	$1, 82(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L481:
	.cfi_restore_state
	movq	24(%rsi), %r14
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	72(%rbx), %rax
	jb	.L482
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Processor25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	jmp	.L478
	.p2align 4,,10
	.p2align 3
.L482:
	movb	$1, 80(%rbx)
	jmp	.L478
	.cfi_endproc
.LFE21107:
	.size	_ZN2v88internal9Processor19VisitWhileStatementEPNS0_14WhileStatementE, .-_ZN2v88internal9Processor19VisitWhileStatementEPNS0_14WhileStatementE
	.section	.text._ZN2v88internal9Processor10VisitBlockEPNS0_5BlockE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Processor10VisitBlockEPNS0_5BlockE
	.type	_ZN2v88internal9Processor10VisitBlockEPNS0_5BlockE, @function
_ZN2v88internal9Processor10VisitBlockEPNS0_5BlockE:
.LFB21102:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	4(%rsi), %eax
	testb	$-128, %al
	jne	.L484
	movzbl	83(%rdi), %r14d
	movl	%r14d, %edx
	testb	$1, %ah
	je	.L485
	movq	32(%rsi), %rax
	testb	%r14b, %r14b
	je	.L495
.L485:
	movb	%dl, 83(%rbx)
	movslq	20(%r12), %rax
	movl	%eax, %ecx
	subl	$1, %ecx
	js	.L486
	movslq	%ecx, %r13
	movl	%ecx, %ecx
	subq	%rcx, %rax
	salq	$3, %r13
	leaq	-16(,%rax,8), %rax
	movq	%rax, -56(%rbp)
	jmp	.L490
	.p2align 4,,10
	.p2align 3
.L489:
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Processor25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movq	8(%r12), %rax
	addq	%r13, %rax
.L488:
	movq	8(%rbx), %rdx
	subq	$8, %r13
	movq	%rdx, (%rax)
	cmpq	%r13, -56(%rbp)
	je	.L486
	movzbl	83(%rbx), %edx
.L490:
	testb	%dl, %dl
	jne	.L487
	cmpb	$0, 82(%rbx)
	jne	.L486
.L487:
	movq	8(%r12), %rax
	addq	%r13, %rax
	cmpb	$0, 80(%rbx)
	jne	.L488
	movq	(%rax), %r15
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	72(%rbx), %rax
	jnb	.L489
	movb	$1, 80(%rbx)
	movq	8(%r12), %rax
	addq	%r13, %rax
	jmp	.L488
	.p2align 4,,10
	.p2align 3
.L486:
	movb	%r14b, 83(%rbx)
.L484:
	movq	%r12, 8(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L495:
	.cfi_restore_state
	testq	%rax, %rax
	setne	%dl
	jmp	.L485
	.cfi_endproc
.LFE21102:
	.size	_ZN2v88internal9Processor10VisitBlockEPNS0_5BlockE, .-_ZN2v88internal9Processor10VisitBlockEPNS0_5BlockE
	.section	.text._ZN2v88internal9Processor33VisitSloppyBlockFunctionStatementEPNS0_28SloppyBlockFunctionStatementE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Processor33VisitSloppyBlockFunctionStatementEPNS0_28SloppyBlockFunctionStatementE
	.type	_ZN2v88internal9Processor33VisitSloppyBlockFunctionStatementEPNS0_28SloppyBlockFunctionStatementE, @function
_ZN2v88internal9Processor33VisitSloppyBlockFunctionStatementEPNS0_28SloppyBlockFunctionStatementE:
.LFB21117:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	cmpb	$0, 80(%rdi)
	je	.L573
.L497:
	movq	8(%rbx), %r13
.L499:
	movq	%r13, 16(%r12)
	movq	%r12, 8(%rbx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L573:
	.cfi_restore_state
	movq	16(%rsi), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	72(%rbx), %rax
	jb	.L574
	movzbl	4(%r13), %eax
	andl	$63, %eax
	cmpb	$57, %al
	ja	.L497
	leaq	.L502(%rip), %rdx
	movzbl	%al, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal9Processor33VisitSloppyBlockFunctionStatementEPNS0_28SloppyBlockFunctionStatementE,"a",@progbits
	.align 4
	.align 4
.L502:
	.long	.L501-.L502
	.long	.L501-.L502
	.long	.L514-.L502
	.long	.L514-.L502
	.long	.L514-.L502
	.long	.L514-.L502
	.long	.L514-.L502
	.long	.L513-.L502
	.long	.L512-.L502
	.long	.L511-.L502
	.long	.L499-.L502
	.long	.L510-.L502
	.long	.L509-.L502
	.long	.L507-.L502
	.long	.L507-.L502
	.long	.L544-.L502
	.long	.L505-.L502
	.long	.L504-.L502
	.long	.L503-.L502
	.long	.L499-.L502
	.long	.L499-.L502
	.long	.L501-.L502
	.long	.L501-.L502
	.long	.L501-.L502
	.long	.L501-.L502
	.long	.L501-.L502
	.long	.L501-.L502
	.long	.L501-.L502
	.long	.L501-.L502
	.long	.L501-.L502
	.long	.L501-.L502
	.long	.L501-.L502
	.long	.L501-.L502
	.long	.L501-.L502
	.long	.L501-.L502
	.long	.L501-.L502
	.long	.L501-.L502
	.long	.L501-.L502
	.long	.L501-.L502
	.long	.L501-.L502
	.long	.L501-.L502
	.long	.L501-.L502
	.long	.L501-.L502
	.long	.L501-.L502
	.long	.L501-.L502
	.long	.L501-.L502
	.long	.L501-.L502
	.long	.L501-.L502
	.long	.L501-.L502
	.long	.L501-.L502
	.long	.L501-.L502
	.long	.L501-.L502
	.long	.L501-.L502
	.long	.L501-.L502
	.long	.L501-.L502
	.long	.L501-.L502
	.long	.L501-.L502
	.long	.L501-.L502
	.section	.text._ZN2v88internal9Processor33VisitSloppyBlockFunctionStatementEPNS0_28SloppyBlockFunctionStatementE
	.p2align 4,,10
	.p2align 3
.L514:
	cmpb	$0, 80(%rbx)
	movzbl	83(%rbx), %r14d
	movb	$1, 83(%rbx)
	je	.L575
.L527:
	movq	8(%rbx), %rax
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%rax, 24(%r13)
	call	_ZN2v88internal9Processor21AssignUndefinedBeforeEPNS0_9StatementE
	movb	$1, 82(%rbx)
	movb	%r14b, 83(%rbx)
	movq	%rax, %r13
	jmp	.L499
	.p2align 4,,10
	.p2align 3
.L574:
	movb	$1, 80(%rbx)
	movq	8(%rbx), %r13
	jmp	.L499
	.p2align 4,,10
	.p2align 3
.L501:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L575:
	movq	24(%r13), %r15
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	72(%rbx), %rax
	jb	.L576
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Processor25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	jmp	.L527
	.p2align 4,,10
	.p2align 3
.L507:
	movb	$0, 82(%rbx)
	jmp	.L499
.L505:
	cmpb	$0, 80(%rbx)
	jne	.L542
	movq	24(%r13), %r14
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	72(%rbx), %rax
	jb	.L577
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Processor25VisitNoStackOverflowCheckEPNS0_7AstNodeE
.L542:
	movq	8(%rbx), %rax
	movq	%rax, 24(%r13)
	cmpb	$0, 82(%rbx)
	jne	.L544
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Processor21AssignUndefinedBeforeEPNS0_9StatementE
	movq	%rax, %r13
.L544:
	movb	$1, 82(%rbx)
	jmp	.L499
.L509:
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Processor16VisitIfStatementEPNS0_11IfStatementE
	movq	8(%rbx), %r13
	jmp	.L499
.L503:
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Processor24VisitTryFinallyStatementEPNS0_19TryFinallyStatementE
	movq	8(%rbx), %r13
	jmp	.L499
.L510:
	cmpb	$0, 80(%rbx)
	jne	.L540
	movq	16(%r13), %r14
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	72(%rbx), %rax
	jb	.L578
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Processor25VisitNoStackOverflowCheckEPNS0_7AstNodeE
.L540:
	movq	8(%rbx), %rax
	movq	%rax, 16(%r13)
	jmp	.L499
.L504:
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Processor22VisitTryCatchStatementEPNS0_17TryCatchStatementE
	movq	8(%rbx), %r13
	jmp	.L499
.L513:
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Processor10VisitBlockEPNS0_5BlockE
	movq	8(%rbx), %r13
	jmp	.L499
.L512:
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Processor20VisitSwitchStatementEPNS0_15SwitchStatementE
	movq	8(%rbx), %r13
	jmp	.L499
.L511:
	cmpb	$0, 82(%rbx)
	jne	.L499
	movq	32(%rbx), %rdi
	movq	8(%r13), %r8
	movb	$1, 81(%rbx)
	movq	(%rbx), %r15
	movq	16(%rdi), %r14
	movq	24(%rdi), %rax
	subq	%r14, %rax
	cmpq	$23, %rax
	jbe	.L579
	leaq	24(%r14), %rax
	movq	%rax, 16(%rdi)
.L531:
	movl	$-1, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal13VariableProxyC1EPNS0_8VariableEi@PLT
	movl	4(%r14), %eax
	movq	-56(%rbp), %r8
	movl	%eax, %edx
	andl	$63, %edx
	cmpb	$54, %dl
	je	.L580
.L533:
	movq	32(%rbx), %rdi
	movq	16(%rdi), %r15
	movq	24(%rdi), %rax
	subq	%r15, %rax
	cmpq	$23, %rax
	jbe	.L581
	leaq	24(%r15), %rax
	movq	%rax, 16(%rdi)
.L539:
	movq	%r14, %rcx
	movl	$17, %edx
	movl	$24, %esi
	movq	%r15, %rdi
	movl	$-1, %r9d
	call	_ZN2v88internal10AssignmentC1ENS0_7AstNode8NodeTypeENS0_5Token5ValueEPNS0_10ExpressionES7_i@PLT
	movq	%r15, 8(%r13)
	movb	$1, 82(%rbx)
	jmp	.L499
	.p2align 4,,10
	.p2align 3
.L576:
	movb	$1, 80(%rbx)
	jmp	.L527
.L578:
	movb	$1, 80(%rbx)
	jmp	.L540
.L577:
	movb	$1, 80(%rbx)
	jmp	.L542
.L580:
	movl	%eax, %edx
	orb	$-128, %dl
	movl	%edx, 4(%r14)
	testb	$1, %ah
	je	.L533
	movq	8(%r14), %rcx
	movzwl	40(%rcx), %eax
	movl	%eax, %edx
	andl	$15, %edx
	subb	$1, %dl
	je	.L533
	movq	16(%rcx), %r15
	testq	%r15, %r15
	je	.L535
	testb	$64, %ah
	je	.L582
.L535:
	orb	$64, %ah
	movw	%ax, 40(%rcx)
	jmp	.L533
.L579:
	movl	$24, %esi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r8
	movq	%rax, %r14
	jmp	.L531
.L581:
	movl	$24, %esi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r8
	movq	%rax, %r15
	jmp	.L539
.L582:
	movzwl	40(%r15), %edx
	movl	%edx, %esi
	andl	$15, %esi
	subb	$1, %sil
	je	.L535
	movq	16(%r15), %rsi
	testq	%rsi, %rsi
	je	.L536
	testb	$64, %dh
	je	.L583
.L536:
	orb	$64, %dh
	movw	%dx, 40(%r15)
	movzwl	40(%rcx), %eax
	jmp	.L535
.L583:
	movzwl	40(%rsi), %eax
	movl	%eax, %edi
	andl	$15, %edi
	subb	$1, %dil
	je	.L536
	movq	16(%rsi), %rdi
	testq	%rdi, %rdi
	je	.L537
	testb	$64, %ah
	je	.L584
.L537:
	orb	$64, %ah
	movw	%ax, 40(%rsi)
	movzwl	40(%r15), %edx
	jmp	.L536
.L584:
	movq	%rsi, -72(%rbp)
	movq	%rcx, -64(%rbp)
	call	_ZN2v88internal8Variable16SetMaybeAssignedEv
	movq	-72(%rbp), %rsi
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %r8
	movzwl	40(%rsi), %eax
	jmp	.L537
	.cfi_endproc
.LFE21117:
	.size	_ZN2v88internal9Processor33VisitSloppyBlockFunctionStatementEPNS0_28SloppyBlockFunctionStatementE, .-_ZN2v88internal9Processor33VisitSloppyBlockFunctionStatementEPNS0_28SloppyBlockFunctionStatementE
	.section	.text._ZN2v88internal8Rewriter7RewriteEPNS0_9ParseInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Rewriter7RewriteEPNS0_9ParseInfoE
	.type	_ZN2v88internal8Rewriter7RewriteEPNS0_9ParseInfoE, @function
_ZN2v88internal8Rewriter7RewriteEPNS0_9ParseInfoE:
.LFB21160:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$200, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	8(%rdi), %eax
	movq	136(%rdi), %rdi
	movq	$0, -160(%rbp)
	movaps	%xmm0, -192(%rbp)
	andl	$8192, %eax
	movaps	%xmm0, -176(%rbp)
	cmpl	$1, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	sbbl	%edx, %edx
	andl	$13, %edx
	addl	$122, %edx
	testq	%rdi, %rdi
	je	.L587
	testl	%eax, %eax
	jne	.L619
.L587:
	movq	168(%rbx), %r14
	movq	40(%r14), %r13
	movzbl	128(%r13), %eax
	leal	-3(%rax), %edx
	cmpb	$1, %dl
	seta	%r12b
	cmpb	$1, %al
	setne	%al
	andb	%al, %r12b
	jne	.L588
	movl	60(%r14), %eax
	testl	%eax, %eax
	jne	.L620
.L610:
	movl	$1, %r12d
.L588:
	movq	-192(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L621
.L585:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L622
	addq	$200, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L620:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movq	%rax, %rdi
	movq	112(%rbx), %rax
	movq	56(%rax), %rax
	movq	248(%rax), %rsi
	call	_ZN2v88internal5Scope12NewTemporaryEPKNS0_12AstRawStringE@PLT
	movq	112(%rbx), %rdx
	movq	%r13, %rdi
	movq	%rax, -232(%rbp)
	movq	%rdx, -216(%rbp)
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movq	-216(%rbp), %rdx
	movq	-232(%rbp), %rcx
	movq	$0, -136(%rbp)
	movq	%rax, -120(%rbp)
	movq	32(%rbx), %r15
	movq	1096(%rdx), %r13
	movq	%rdx, -104(%rbp)
	movq	%rcx, -144(%rbp)
	movq	%r13, -112(%rbp)
	movq	24(%r13), %rdx
	movq	%r13, -128(%rbp)
	movq	16(%r13), %rax
	subq	%rax, %rdx
	cmpq	$7, %rdx
	jbe	.L623
	leaq	8(%rax), %rdx
	movq	%rdx, 16(%r13)
.L590:
	movabsq	$47244640255, %rcx
	movq	%rcx, (%rax)
	movq	%rax, -96(%rbp)
	movq	16(%r13), %rax
	movq	24(%r13), %rdx
	subq	%rax, %rdx
	cmpq	$7, %rdx
	jbe	.L624
	leaq	8(%rax), %rdx
	movq	%rdx, 16(%r13)
.L592:
	movabsq	$223338299391, %rcx
	movq	%rcx, (%rax)
	movq	%rax, -88(%rbp)
	movq	16(%r13), %rax
	movq	24(%r13), %rdx
	subq	%rax, %rdx
	cmpq	$7, %rdx
	jbe	.L625
	leaq	8(%rax), %rdx
	movq	%rdx, 16(%r13)
.L594:
	movabsq	$249108103167, %rcx
	movq	%rcx, (%rax)
	movq	%rax, -80(%rbp)
	movq	%r15, -72(%rbp)
	movl	$0, -64(%rbp)
	movslq	60(%r14), %rax
	movl	%eax, %esi
	subl	$1, %esi
	js	.L610
	movslq	%esi, %rdx
	movl	%esi, %esi
	leaq	-144(%rbp), %r15
	subq	%rsi, %rax
	leaq	0(,%rdx,8), %r13
	leaq	-16(,%rax,8), %rax
	movq	%rax, -224(%rbp)
	xorl	%eax, %eax
	jmp	.L600
	.p2align 4,,10
	.p2align 3
.L599:
	movq	%r15, %rdi
	call	_ZN2v88internal9Processor25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movq	48(%r14), %rax
	addq	%r13, %rax
.L598:
	movq	-136(%rbp), %rsi
	subq	$8, %r13
	movq	%rsi, (%rax)
	cmpq	%r13, -224(%rbp)
	je	.L597
	movzbl	-61(%rbp), %eax
.L600:
	testb	%al, %al
	jne	.L596
	cmpb	$0, -62(%rbp)
	jne	.L597
.L596:
	movq	48(%r14), %rax
	addq	%r13, %rax
	cmpb	$0, -64(%rbp)
	jne	.L598
	movq	(%rax), %rsi
	movq	%rsi, -216(%rbp)
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	-72(%rbp), %rax
	movq	-216(%rbp), %rsi
	jnb	.L599
	movb	$1, -64(%rbp)
	movq	48(%r14), %rax
	addq	%r13, %rax
	jmp	.L598
	.p2align 4,,10
	.p2align 3
.L597:
	cmpb	$0, -63(%rbp)
	jne	.L601
.L618:
	movzbl	-64(%rbp), %eax
	testb	%al, %al
	je	.L610
	jmp	.L588
	.p2align 4,,10
	.p2align 3
.L601:
	movq	-112(%rbp), %rdi
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	subq	%r13, %rax
	cmpq	$23, %rax
	jbe	.L626
	leaq	24(%r13), %rax
	movq	%rax, 16(%rdi)
.L604:
	movq	-232(%rbp), %rsi
	movl	$-1, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal13VariableProxyC1EPNS0_8VariableEi@PLT
	movq	-112(%rbp), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$23, %rdx
	jbe	.L627
	leaq	24(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L606:
	movq	%r13, 8(%rax)
	leaq	-200(%rbp), %rsi
	leaq	48(%r14), %rdi
	movabsq	$68719476735, %rcx
	movq	%rcx, (%rax)
	movl	$-1, 16(%rax)
	movq	(%rbx), %rdx
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal8ZoneListIPNS0_9StatementEE3AddERKS3_PNS0_4ZoneE
	jmp	.L618
	.p2align 4,,10
	.p2align 3
.L621:
	leaq	-184(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L585
.L619:
	leaq	-184(%rbp), %rsi
	movq	%rdi, -192(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L624:
	movl	$8, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L592
	.p2align 4,,10
	.p2align 3
.L625:
	movl	$8, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L623:
	movl	$8, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L590
.L627:
	movl	$24, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L606
.L626:
	movl	$24, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r13
	jmp	.L604
.L622:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21160:
	.size	_ZN2v88internal8Rewriter7RewriteEPNS0_9ParseInfoE, .-_ZN2v88internal8Rewriter7RewriteEPNS0_9ParseInfoE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal9Processor21AssignUndefinedBeforeEPNS0_9StatementE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal9Processor21AssignUndefinedBeforeEPNS0_9StatementE, @function
_GLOBAL__sub_I__ZN2v88internal9Processor21AssignUndefinedBeforeEPNS0_9StatementE:
.LFB27238:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE27238:
	.size	_GLOBAL__sub_I__ZN2v88internal9Processor21AssignUndefinedBeforeEPNS0_9StatementE, .-_GLOBAL__sub_I__ZN2v88internal9Processor21AssignUndefinedBeforeEPNS0_9StatementE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal9Processor21AssignUndefinedBeforeEPNS0_9StatementE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
