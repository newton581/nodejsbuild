	.file	"source-position.cc"
	.text
	.section	.rodata._ZN2v88internallsERSoRKNS0_18SourcePositionInfoE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"<"
.LC1:
	.string	"unknown"
.LC2:
	.string	":"
.LC3:
	.string	">"
	.section	.text._ZN2v88internallsERSoRKNS0_18SourcePositionInfoE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internallsERSoRKNS0_18SourcePositionInfoE
	.type	_ZN2v88internallsERSoRKNS0_18SourcePositionInfoE, @function
_ZN2v88internallsERSoRKNS0_18SourcePositionInfoE:
.LFB18086:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	leaq	.LC0(%rip), %rsi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	16(%rbx), %rax
	testq	%rax, %rax
	je	.L2
	movq	(%rax), %rax
	movq	15(%rax), %rax
	testb	$1, %al
	jne	.L14
.L2:
	movl	$7, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L7:
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	24(%rbx), %eax
	movq	%r12, %rdi
	leal	1(%rax), %esi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	28(%rbx), %esi
	movq	%r13, %rdi
	addl	$1, %esi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC3(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L15
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L2
	leaq	-48(%rbp), %rdi
	leaq	-56(%rbp), %rsi
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	-48(%rbp), %r13
	testq	%r13, %r13
	je	.L16
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L4:
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L7
	call	_ZdaPv@PLT
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L16:
	movq	(%r12), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L4
.L15:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18086:
	.size	_ZN2v88internallsERSoRKNS0_18SourcePositionInfoE, .-_ZN2v88internallsERSoRKNS0_18SourcePositionInfoE
	.section	.rodata._ZN2v88internallsERSoRKSt6vectorINS0_18SourcePositionInfoESaIS3_EE.str1.1,"aMS",@progbits,1
.LC4:
	.string	" inlined at "
	.section	.text._ZN2v88internallsERSoRKSt6vectorINS0_18SourcePositionInfoESaIS3_EE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internallsERSoRKSt6vectorINS0_18SourcePositionInfoESaIS3_EE
	.type	_ZN2v88internallsERSoRKSt6vectorINS0_18SourcePositionInfoESaIS3_EE, @function
_ZN2v88internallsERSoRKSt6vectorINS0_18SourcePositionInfoESaIS3_EE:
.LFB18087:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	.LC4(%rip), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rsi), %rbx
	movq	8(%rsi), %r13
	cmpq	%r13, %rbx
	je	.L21
.L19:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	addq	$32, %rbx
	call	_ZN2v88internallsERSoRKNS0_18SourcePositionInfoE
	cmpq	%rbx, %r13
	je	.L21
	movl	$12, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L21:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18087:
	.size	_ZN2v88internallsERSoRKSt6vectorINS0_18SourcePositionInfoESaIS3_EE, .-_ZN2v88internallsERSoRKSt6vectorINS0_18SourcePositionInfoESaIS3_EE
	.section	.rodata._ZN2v88internallsERSoRKNS0_14SourcePositionE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"<inlined("
.LC6:
	.string	"):"
.LC7:
	.string	"<not inlined:"
.LC8:
	.string	", "
	.section	.text._ZN2v88internallsERSoRKNS0_14SourcePositionE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internallsERSoRKNS0_14SourcePositionE
	.type	_ZN2v88internallsERSoRKNS0_14SourcePositionE, @function
_ZN2v88internallsERSoRKNS0_14SourcePositionE:
.LFB18088:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L24
	shrq	$31, %rax
	testw	%ax, %ax
	jne	.L33
.L24:
	movl	$13, %edx
	leaq	.LC7(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L25:
	movq	(%rbx), %rax
	movq	%rax, %rsi
	shrq	%rsi
	testb	$1, %al
	je	.L26
	andl	$1048575, %esi
	movq	%r12, %rdi
	call	_ZNSolsEi@PLT
	leaq	.LC8(%rip), %rsi
	movl	$2, %edx
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rsi
	movq	%r13, %rdi
	shrq	$21, %rsi
	andl	$1023, %esi
.L32:
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC3(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	andl	$1073741823, %esi
	movq	%r12, %rdi
	subl	$1, %esi
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L33:
	movl	$9, %edx
	leaq	.LC5(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	shrq	$31, %rsi
	movzwl	%si, %esi
	subl	$1, %esi
	call	_ZNSolsEi@PLT
	movl	$2, %edx
	leaq	.LC6(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L25
	.cfi_endproc
.LFE18088:
	.size	_ZN2v88internallsERSoRKNS0_14SourcePositionE, .-_ZN2v88internallsERSoRKNS0_14SourcePositionE
	.section	.text._ZNK2v88internal14SourcePosition5PrintERSoNS0_18SharedFunctionInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal14SourcePosition5PrintERSoNS0_18SharedFunctionInfoE
	.type	_ZNK2v88internal14SourcePosition5PrintERSoNS0_18SharedFunctionInfoE, @function
_ZNK2v88internal14SourcePosition5PrintERSoNS0_18SharedFunctionInfoE:
.LFB18106:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pcmpeqd	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	31(%rdx), %rax
	movaps	%xmm0, -64(%rbp)
	testb	$1, %al
	jne	.L50
.L35:
	movl	$1, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L43:
	movl	$7, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L42:
	movl	$1, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-64(%rbp), %eax
	movq	%r12, %rdi
	leal	1(%rax), %esi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-60(%rbp), %eax
	movq	%r12, %rdi
	leal	1(%rax), %esi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC3(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L51
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	.cfi_restore_state
	movq	-1(%rax), %rcx
	leaq	-1(%rax), %rdx
	cmpw	$86, 11(%rcx)
	je	.L52
.L36:
	movq	(%rdx), %rdx
	cmpw	$96, 11(%rdx)
	jne	.L35
	testb	$1, %al
	jne	.L53
.L37:
	movq	(%rdi), %rsi
	leaq	-72(%rbp), %r13
	movq	%rax, -72(%rbp)
	leaq	-64(%rbp), %rdx
	movq	15(%rax), %rbx
	movq	%r13, %rdi
	movl	$1, %ecx
	shrq	%rsi
	andl	$1073741823, %esi
	subl	$1, %esi
	call	_ZNK2v88internal6Script15GetPositionInfoEiPNS1_12PositionInfoENS1_10OffsetFlagE@PLT
	movl	$1, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	testb	$1, %bl
	je	.L43
	movq	-1(%rbx), %rax
	cmpw	$63, 11(%rax)
	ja	.L43
	movq	%r13, %rdi
	leaq	-80(%rbp), %rsi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$1, %edx
	movq	%rbx, -80(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	-72(%rbp), %r13
	testq	%r13, %r13
	je	.L54
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L40:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L42
	call	_ZdaPv@PLT
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L52:
	movq	23(%rax), %rdx
	testb	$1, %dl
	je	.L35
	subq	$1, %rdx
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L54:
	movq	(%r12), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L53:
	movq	-1(%rax), %rdx
	cmpw	$86, 11(%rdx)
	jne	.L37
	movq	23(%rax), %rax
	jmp	.L37
.L51:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18106:
	.size	_ZNK2v88internal14SourcePosition5PrintERSoNS0_18SharedFunctionInfoE, .-_ZNK2v88internal14SourcePosition5PrintERSoNS0_18SharedFunctionInfoE
	.section	.rodata._ZNK2v88internal14SourcePosition9PrintJsonERSo.str1.1,"aMS",@progbits,1
.LC9:
	.string	"{ \"line\" : "
.LC10:
	.string	"  \"fileId\" : "
.LC11:
	.string	"  \"inliningId\" : "
.LC12:
	.string	"}"
.LC13:
	.string	"{ \"scriptOffset\" : "
	.section	.text._ZNK2v88internal14SourcePosition9PrintJsonERSo,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal14SourcePosition9PrintJsonERSo
	.type	_ZNK2v88internal14SourcePosition9PrintJsonERSo, @function
_ZNK2v88internal14SourcePosition9PrintJsonERSo:
.LFB18107:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	testb	$1, (%rdi)
	je	.L56
	movl	$11, %edx
	movq	%r12, %rdi
	leaq	.LC9(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	shrq	%rsi
	andl	$1048575, %esi
	call	_ZNSolsEi@PLT
	movl	$2, %edx
	leaq	.LC8(%rip), %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	.LC10(%rip), %rsi
	movl	$13, %edx
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rsi
	shrq	$21, %rsi
	andl	$1023, %esi
.L58:
	movq	%r12, %rdi
	call	_ZNSolsEi@PLT
	movl	$2, %edx
	leaq	.LC8(%rip), %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$17, %edx
	movq	%r12, %rdi
	leaq	.LC11(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	shrq	$31, %rsi
	movzwl	%si, %esi
	subl	$1, %esi
	call	_ZNSolsEi@PLT
	popq	%rbx
	popq	%r12
	movl	$1, %edx
	movq	%rax, %rdi
	leaq	.LC12(%rip), %rsi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	.p2align 4,,10
	.p2align 3
.L56:
	.cfi_restore_state
	leaq	.LC13(%rip), %rsi
	movl	$19, %edx
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rsi
	shrq	%rsi
	andl	$1073741823, %esi
	subl	$1, %esi
	jmp	.L58
	.cfi_endproc
.LFE18107:
	.size	_ZNK2v88internal14SourcePosition9PrintJsonERSo, .-_ZNK2v88internal14SourcePosition9PrintJsonERSo
	.section	.text._ZNK2v88internal14SourcePosition5PrintERSoNS0_4CodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal14SourcePosition5PrintERSoNS0_4CodeE
	.type	_ZNK2v88internal14SourcePosition5PrintERSoNS0_4CodeE, @function
_ZNK2v88internal14SourcePosition5PrintERSoNS0_4CodeE:
.LFB18108:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$56, %rsp
	movq	15(%rdx), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%rdx, -72(%rbp)
	testb	$1, %al
	jne	.L60
	shrq	$31, %rax
	andl	$65535, %eax
	jne	.L61
.L60:
	movq	63(%rdx), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK2v88internal14SourcePosition5PrintERSoNS0_18SharedFunctionInfoE
.L59:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L67
	addq	$56, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	.cfi_restore_state
	subl	$1, %eax
	movq	71(%rdx), %rdx
	sall	$4, %eax
	cltq
	movq	15(%rdx,%rax), %rcx
	movq	23(%rdx,%rax), %rsi
	movq	%rcx, -64(%rbp)
	movl	%esi, -56(%rbp)
	cmpl	$-1, %esi
	je	.L68
	leaq	-72(%rbp), %rdi
	call	_ZN2v88internal18DeoptimizationData18GetInlinedFunctionEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZNK2v88internal14SourcePosition5PrintERSoNS0_18SharedFunctionInfoE
.L64:
	movq	%r13, %rdi
	movl	$12, %edx
	leaq	.LC4(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-64(%rbp), %rdi
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	_ZNK2v88internal14SourcePosition5PrintERSoNS0_4CodeE
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L68:
	movq	%rdi, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internallsERSoRKNS0_14SourcePositionE
	jmp	.L64
.L67:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18108:
	.size	_ZNK2v88internal14SourcePosition5PrintERSoNS0_4CodeE, .-_ZNK2v88internal14SourcePosition5PrintERSoNS0_4CodeE
	.section	.text._ZN2v88internal18SourcePositionInfoC2ENS0_14SourcePositionENS0_6HandleINS0_18SharedFunctionInfoEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18SourcePositionInfoC2ENS0_14SourcePositionENS0_6HandleINS0_18SharedFunctionInfoEEE
	.type	_ZN2v88internal18SourcePositionInfoC2ENS0_14SourcePositionENS0_6HandleINS0_18SharedFunctionInfoEEE, @function
_ZN2v88internal18SourcePositionInfoC2ENS0_14SourcePositionENS0_6HandleINS0_18SharedFunctionInfoEEE:
.LFB18110:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rsi, (%rdi)
	movq	%rdx, 8(%rdi)
	testq	%rdx, %rdx
	je	.L70
	movq	(%rdx), %rdx
	movq	31(%rdx), %rax
	testb	$1, %al
	jne	.L88
.L70:
	movq	$0, 16(%rbx)
	movq	$-1, 24(%rbx)
.L69:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L89
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	.cfi_restore_state
	movq	%rsi, %r12
	movq	-1(%rax), %rsi
	leaq	-1(%rax), %rcx
	cmpw	$86, 11(%rsi)
	je	.L90
.L71:
	movq	(%rcx), %rax
	cmpw	$96, 11(%rax)
	jne	.L70
	movq	%rdx, %rax
	movq	31(%rdx), %rsi
	andq	$-262144, %rax
	movq	24(%rax), %r13
	subq	$37592, %r13
	testb	$1, %sil
	jne	.L91
.L72:
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L92
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	$-1, 24(%rbx)
	movq	%rax, 16(%rbx)
	testq	%rax, %rax
	je	.L69
.L76:
	shrq	%r12
	movq	16(%rbx), %rdi
	pcmpeqd	%xmm0, %xmm0
	leaq	-64(%rbp), %rdx
	andl	$1073741823, %r12d
	movl	$1, %ecx
	movaps	%xmm0, -64(%rbp)
	leal	-1(%r12), %esi
	call	_ZN2v88internal6Script15GetPositionInfoENS0_6HandleIS1_EEiPNS1_12PositionInfoENS1_10OffsetFlagE@PLT
	testb	%al, %al
	je	.L69
	movq	-64(%rbp), %rax
	movq	%rax, 24(%rbx)
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L90:
	movq	23(%rax), %rcx
	testb	$1, %cl
	je	.L70
	subq	$1, %rcx
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L92:
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L93
.L77:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%rsi, (%rax)
	movq	%rax, 16(%rbx)
	movq	$-1, 24(%rbx)
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L91:
	movq	-1(%rsi), %rax
	cmpw	$86, 11(%rax)
	jne	.L72
	movq	23(%rsi), %rsi
	jmp	.L72
.L93:
	movq	%r13, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	jmp	.L77
.L89:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18110:
	.size	_ZN2v88internal18SourcePositionInfoC2ENS0_14SourcePositionENS0_6HandleINS0_18SharedFunctionInfoEEE, .-_ZN2v88internal18SourcePositionInfoC2ENS0_14SourcePositionENS0_6HandleINS0_18SharedFunctionInfoEEE
	.globl	_ZN2v88internal18SourcePositionInfoC1ENS0_14SourcePositionENS0_6HandleINS0_18SharedFunctionInfoEEE
	.set	_ZN2v88internal18SourcePositionInfoC1ENS0_14SourcePositionENS0_6HandleINS0_18SharedFunctionInfoEEE,_ZN2v88internal18SourcePositionInfoC2ENS0_14SourcePositionENS0_6HandleINS0_18SharedFunctionInfoEEE
	.section	.rodata._ZNSt6vectorIN2v88internal18SourcePositionInfoESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_.str1.1,"aMS",@progbits,1
.LC14:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIN2v88internal18SourcePositionInfoESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal18SourcePositionInfoESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal18SourcePositionInfoESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal18SourcePositionInfoESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, @function
_ZNSt6vectorIN2v88internal18SourcePositionInfoESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_:
.LFB21595:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rcx
	movq	(%rdi), %r14
	movabsq	$288230376151711743, %rdi
	movq	%rcx, %rax
	subq	%r14, %rax
	sarq	$5, %rax
	cmpq	%rdi, %rax
	je	.L111
	movq	%rsi, %r12
	subq	%r14, %rsi
	testq	%rax, %rax
	je	.L103
	movabsq	$9223372036854775776, %r13
	leaq	(%rax,%rax), %r8
	cmpq	%r8, %rax
	jbe	.L112
.L96:
	movq	%r13, %rdi
	movq	%rdx, -80(%rbp)
	movq	%rsi, -72(%rbp)
	movq	%rcx, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %rsi
	movq	%rax, %rbx
	leaq	(%rax,%r13), %rax
	movq	-80(%rbp), %rdx
	movq	%rax, -56(%rbp)
	leaq	32(%rbx), %r13
.L102:
	movdqu	(%rdx), %xmm3
	movdqu	16(%rdx), %xmm4
	movups	%xmm3, (%rbx,%rsi)
	movups	%xmm4, 16(%rbx,%rsi)
	cmpq	%r14, %r12
	je	.L98
	movq	%rbx, %rdx
	movq	%r14, %rax
	.p2align 4,,10
	.p2align 3
.L99:
	movdqu	(%rax), %xmm1
	addq	$32, %rax
	addq	$32, %rdx
	movups	%xmm1, -32(%rdx)
	movdqu	-16(%rax), %xmm2
	movups	%xmm2, -16(%rdx)
	cmpq	%rax, %r12
	jne	.L99
	movq	%r12, %rax
	subq	%r14, %rax
	leaq	32(%rbx,%rax), %r13
.L98:
	cmpq	%rcx, %r12
	je	.L100
	subq	%r12, %rcx
	movq	%r13, %rdi
	movq	%r12, %rsi
	movq	%rcx, %rdx
	movq	%rcx, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %rcx
	addq	%rcx, %r13
.L100:
	testq	%r14, %r14
	je	.L101
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L101:
	movq	-56(%rbp), %rax
	movq	%rbx, %xmm0
	movq	%r13, %xmm5
	punpcklqdq	%xmm5, %xmm0
	movq	%rax, 16(%r15)
	movups	%xmm0, (%r15)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	.cfi_restore_state
	testq	%r8, %r8
	jne	.L97
	movq	$0, -56(%rbp)
	movl	$32, %r13d
	xorl	%ebx, %ebx
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L103:
	movl	$32, %r13d
	jmp	.L96
.L97:
	cmpq	%rdi, %r8
	cmovbe	%r8, %rdi
	movq	%rdi, %r13
	salq	$5, %r13
	jmp	.L96
.L111:
	leaq	.LC14(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE21595:
	.size	_ZNSt6vectorIN2v88internal18SourcePositionInfoESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, .-_ZNSt6vectorIN2v88internal18SourcePositionInfoESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.section	.text._ZNK2v88internal14SourcePosition13InliningStackEPNS0_24OptimizedCompilationInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal14SourcePosition13InliningStackEPNS0_24OptimizedCompilationInfoE
	.type	_ZNK2v88internal14SourcePosition13InliningStackEPNS0_24OptimizedCompilationInfoE, @function
_ZNK2v88internal14SourcePosition13InliningStackEPNS0_24OptimizedCompilationInfoE:
.LFB18089:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-96(%rbp), %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, 16(%rdi)
	movups	%xmm0, (%rdi)
	testb	$1, %r15b
	jne	.L114
.L127:
	movq	%r15, %rbx
	shrq	$31, %rbx
	andl	$65535, %ebx
	je	.L114
	subl	$1, %ebx
	movslq	%ebx, %rbx
	salq	$5, %rbx
	addq	96(%r14), %rbx
	movq	(%rbx), %rax
	movq	%r15, -96(%rbp)
	movq	%rax, -88(%rbp)
	testq	%rax, %rax
	je	.L116
	movq	(%rax), %rdx
	movq	31(%rdx), %rax
	testb	$1, %al
	jne	.L145
.L116:
	movq	$0, -80(%rbp)
	movq	$-1, -72(%rbp)
.L121:
	movq	8(%r13), %rsi
	cmpq	16(%r13), %rsi
	je	.L125
.L150:
	movdqa	-96(%rbp), %xmm1
	movups	%xmm1, (%rsi)
	movdqa	-80(%rbp), %xmm2
	movups	%xmm2, 16(%rsi)
	addq	$32, 8(%r13)
.L126:
	movq	16(%rbx), %r15
	testb	$1, %r15b
	je	.L127
.L114:
	movq	24(%r14), %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal18SourcePositionInfoC1ENS0_14SourcePositionENS0_6HandleINS0_18SharedFunctionInfoEEE
	movq	8(%r13), %rsi
	cmpq	16(%r13), %rsi
	je	.L128
	movdqa	-96(%rbp), %xmm4
	movups	%xmm4, (%rsi)
	movdqa	-80(%rbp), %xmm5
	movups	%xmm5, 16(%rsi)
	addq	$32, 8(%r13)
.L113:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L146
	addq	$88, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L145:
	.cfi_restore_state
	movq	-1(%rax), %rsi
	leaq	-1(%rax), %rcx
	cmpw	$86, 11(%rsi)
	je	.L147
.L117:
	movq	(%rcx), %rax
	cmpw	$96, 11(%rax)
	jne	.L116
	movq	%rdx, %rax
	movq	31(%rdx), %rsi
	andq	$-262144, %rax
	movq	24(%rax), %rcx
	subq	$37592, %rcx
	testb	$1, %sil
	jne	.L148
.L118:
	movq	41112(%rcx), %rdi
	testq	%rdi, %rdi
	je	.L149
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	$-1, -72(%rbp)
	movq	%rax, -80(%rbp)
	testq	%rax, %rax
	je	.L121
.L122:
	shrq	%r15
	movq	-80(%rbp), %rdi
	pcmpeqd	%xmm3, %xmm3
	leaq	-112(%rbp), %rdx
	andl	$1073741823, %r15d
	movl	$1, %ecx
	movaps	%xmm3, -112(%rbp)
	leal	-1(%r15), %esi
	call	_ZN2v88internal6Script15GetPositionInfoENS0_6HandleIS1_EEiPNS1_12PositionInfoENS1_10OffsetFlagE@PLT
	testb	%al, %al
	je	.L121
	movq	-112(%rbp), %rax
	movq	8(%r13), %rsi
	movq	%rax, -72(%rbp)
	cmpq	16(%r13), %rsi
	jne	.L150
.L125:
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIN2v88internal18SourcePositionInfoESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L147:
	movq	23(%rax), %rcx
	testb	$1, %cl
	je	.L116
	subq	$1, %rcx
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L128:
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIN2v88internal18SourcePositionInfoESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L149:
	movq	41088(%rcx), %rax
	cmpq	41096(%rcx), %rax
	je	.L151
.L123:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rcx)
	movq	%rsi, (%rax)
	movq	%rax, -80(%rbp)
	movq	$-1, -72(%rbp)
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L148:
	movq	-1(%rsi), %rax
	cmpw	$86, 11(%rax)
	jne	.L118
	movq	23(%rsi), %rsi
	jmp	.L118
.L151:
	movq	%rcx, %rdi
	movq	%rsi, -128(%rbp)
	movq	%rcx, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rcx
	jmp	.L123
.L146:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18089:
	.size	_ZNK2v88internal14SourcePosition13InliningStackEPNS0_24OptimizedCompilationInfoE, .-_ZNK2v88internal14SourcePosition13InliningStackEPNS0_24OptimizedCompilationInfoE
	.section	.text._ZNK2v88internal14SourcePosition13InliningStackENS0_6HandleINS0_4CodeEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal14SourcePosition13InliningStackENS0_6HandleINS0_4CodeEEE
	.type	_ZNK2v88internal14SourcePosition13InliningStackENS0_6HandleINS0_4CodeEEE, @function
_ZNK2v88internal14SourcePosition13InliningStackENS0_6HandleINS0_4CodeEEE:
.LFB18105:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	movq	%rax, %rdx
	movq	15(%rax), %rsi
	andq	$-262144, %rdx
	movq	24(%rdx), %rbx
	movq	3520(%rbx), %rdi
	subq	$37592, %rbx
	testq	%rdi, %rdi
	je	.L153
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -104(%rbp)
.L154:
	movq	(%r12), %r12
	pxor	%xmm0, %xmm0
	leaq	-96(%rbp), %r15
	movq	$0, 16(%r13)
	movups	%xmm0, 0(%r13)
	testb	$1, %r12b
	jne	.L156
	movq	%r12, %r14
	shrq	$31, %r14
	andl	$65535, %r14d
	je	.L156
.L157:
	movq	-104(%rbp), %rax
	subl	$1, %r14d
	movq	%r15, %rdi
	sall	$4, %r14d
	movq	(%rax), %rdx
	movl	%r14d, %eax
	movq	71(%rdx), %rcx
	movq	%r12, -112(%rbp)
	movq	23(%rcx,%rax), %rsi
	movq	15(%rcx,%rax), %r14
	movq	%rdx, -96(%rbp)
	call	_ZN2v88internal18DeoptimizationData18GetInlinedFunctionEi@PLT
	movq	41112(%rbx), %rdi
	movq	%r14, %r12
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L158
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L159:
	movq	-112(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal18SourcePositionInfoC1ENS0_14SourcePositionENS0_6HandleINS0_18SharedFunctionInfoEEE
	movq	8(%r13), %rsi
	cmpq	16(%r13), %rsi
	je	.L161
	movdqa	-96(%rbp), %xmm1
	movups	%xmm1, (%rsi)
	movdqa	-80(%rbp), %xmm2
	movups	%xmm2, 16(%rsi)
	addq	$32, 8(%r13)
.L162:
	testb	$1, %r14b
	jne	.L156
	shrq	$31, %r14
	andl	$65535, %r14d
	jne	.L157
	.p2align 4,,10
	.p2align 3
.L156:
	movq	-104(%rbp), %rax
	movq	(%rax), %rax
	movq	63(%rax), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L164
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L165:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal18SourcePositionInfoC1ENS0_14SourcePositionENS0_6HandleINS0_18SharedFunctionInfoEEE
	movq	8(%r13), %rsi
	cmpq	16(%r13), %rsi
	je	.L167
	movdqa	-96(%rbp), %xmm3
	movups	%xmm3, (%rsi)
	movdqa	-80(%rbp), %xmm4
	movups	%xmm4, 16(%rsi)
	addq	$32, 8(%r13)
.L152:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L177
	addq	$88, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L158:
	.cfi_restore_state
	movq	41088(%rbx), %rdx
	cmpq	41096(%rbx), %rdx
	je	.L178
.L160:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rdx)
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L153:
	movq	41088(%rbx), %rax
	movq	%rax, -104(%rbp)
	cmpq	%rax, 41096(%rbx)
	je	.L179
.L155:
	movq	-104(%rbp), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rcx)
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L164:
	movq	41088(%rbx), %rdx
	cmpq	41096(%rbx), %rdx
	je	.L180
.L166:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rdx)
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L161:
	movq	%r15, %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIN2v88internal18SourcePositionInfoESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L167:
	movq	%r15, %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIN2v88internal18SourcePositionInfoESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L178:
	movq	%rbx, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L180:
	movq	%rbx, %rdi
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L179:
	movq	%rbx, %rdi
	movq	%rsi, -112(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-112(%rbp), %rsi
	movq	%rax, -104(%rbp)
	jmp	.L155
.L177:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18105:
	.size	_ZNK2v88internal14SourcePosition13InliningStackENS0_6HandleINS0_4CodeEEE, .-_ZNK2v88internal14SourcePosition13InliningStackENS0_6HandleINS0_4CodeEEE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internallsERSoRKNS0_18SourcePositionInfoE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internallsERSoRKNS0_18SourcePositionInfoE, @function
_GLOBAL__sub_I__ZN2v88internallsERSoRKNS0_18SourcePositionInfoE:
.LFB21955:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE21955:
	.size	_GLOBAL__sub_I__ZN2v88internallsERSoRKNS0_18SourcePositionInfoE, .-_GLOBAL__sub_I__ZN2v88internallsERSoRKNS0_18SourcePositionInfoE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internallsERSoRKNS0_18SourcePositionInfoE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
