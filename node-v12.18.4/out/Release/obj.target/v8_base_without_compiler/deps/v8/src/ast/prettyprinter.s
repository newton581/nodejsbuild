	.file	"prettyprinter.cc"
	.text
	.section	.text._ZN2v88internal11CallPrinter5PrintEPKc.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11CallPrinter5PrintEPKc.part.0, @function
_ZN2v88internal11CallPrinter5PrintEPKc.part.0:
.LFB23719:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	16(%rdi), %rbx
	addl	$1, 8(%rdi)
	movl	8(%rbx), %eax
	testl	%eax, %eax
	jne	.L32
.L29:
	movzbl	(%r12), %edx
	testb	%dl, %dl
	je	.L1
	movl	20(%rbx), %eax
.L5:
	movq	32(%rbx), %rcx
	leal	1(%rax), %esi
	addl	$16, %eax
	addq	$1, %r12
	cltq
	movq	(%rcx), %rcx
	movl	%esi, 20(%rbx)
	movb	%dl, -1(%rcx,%rax)
	movl	20(%rbx), %eax
	cmpl	16(%rbx), %eax
	je	.L35
	movzbl	(%r12), %edx
	testb	%dl, %dl
	jne	.L5
.L1:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
.L32:
	movzbl	(%r12), %edx
	testb	%dl, %dl
	je	.L1
	movl	20(%rbx), %eax
.L4:
	movq	32(%rbx), %rcx
	leal	1(%rax), %esi
	leal	16(%rax,%rax), %eax
	addq	$1, %r12
	cltq
	movq	(%rcx), %rcx
	movl	%esi, 20(%rbx)
	movw	%dx, -1(%rcx,%rax)
	movl	20(%rbx), %eax
	cmpl	16(%rbx), %eax
	je	.L36
	movzbl	(%r12), %edx
	testb	%dl, %dl
	jne	.L4
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L29
	.cfi_endproc
.LFE23719:
	.size	_ZN2v88internal11CallPrinter5PrintEPKc.part.0, .-_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	.section	.text._ZN2v88internal11CallPrinterC2EPNS0_7IsolateEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinterC2EPNS0_7IsolateEb
	.type	_ZN2v88internal11CallPrinterC2EPNS0_7IsolateEb, @function
_ZN2v88internal11CallPrinterC2EPNS0_7IsolateEb:
.LFB19241:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%edx, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movl	$40, %edi
	call	_Znwm@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal24IncrementalStringBuilderC1EPNS0_7IsolateE@PLT
	xorl	%eax, %eax
	movq	%r13, 16(%rbx)
	xorl	%edx, %edx
	movw	%ax, 28(%rbx)
	movq	37528(%r12), %rax
	pxor	%xmm0, %xmm0
	movq	%r12, (%rbx)
	movb	%r14b, 30(%rbx)
	movl	$0, 8(%rbx)
	movl	$0, 24(%rbx)
	movb	$0, 31(%rbx)
	movw	%dx, 32(%rbx)
	movb	$0, 56(%rbx)
	movq	%rax, 64(%rbx)
	movb	$0, 72(%rbx)
	movups	%xmm0, 40(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19241:
	.size	_ZN2v88internal11CallPrinterC2EPNS0_7IsolateEb, .-_ZN2v88internal11CallPrinterC2EPNS0_7IsolateEb
	.globl	_ZN2v88internal11CallPrinterC1EPNS0_7IsolateEb
	.set	_ZN2v88internal11CallPrinterC1EPNS0_7IsolateEb,_ZN2v88internal11CallPrinterC2EPNS0_7IsolateEb
	.section	.text._ZN2v88internal11CallPrinterD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinterD2Ev
	.type	_ZN2v88internal11CallPrinterD2Ev, @function
_ZN2v88internal11CallPrinterD2Ev:
.LFB19244:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L39
	movl	$40, %esi
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L39:
	ret
	.cfi_endproc
.LFE19244:
	.size	_ZN2v88internal11CallPrinterD2Ev, .-_ZN2v88internal11CallPrinterD2Ev
	.globl	_ZN2v88internal11CallPrinterD1Ev
	.set	_ZN2v88internal11CallPrinterD1Ev,_ZN2v88internal11CallPrinterD2Ev
	.section	.text._ZNK2v88internal11CallPrinter12GetErrorHintEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11CallPrinter12GetErrorHintEv
	.type	_ZNK2v88internal11CallPrinter12GetErrorHintEv, @function
_ZNK2v88internal11CallPrinter12GetErrorHintEv:
.LFB19246:
	.cfi_startproc
	endbr64
	cmpb	$0, 33(%rdi)
	movzbl	31(%rdi), %edx
	je	.L42
	movl	$3, %eax
	testb	%dl, %dl
	jne	.L41
	xorl	%eax, %eax
	cmpb	$0, 32(%rdi)
	setne	%al
	sall	$2, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	movl	$1, %eax
	testb	%dl, %dl
	jne	.L41
	xorl	%eax, %eax
	cmpb	$0, 32(%rdi)
	setne	%al
	addl	%eax, %eax
.L41:
	ret
	.cfi_endproc
.LFE19246:
	.size	_ZNK2v88internal11CallPrinter12GetErrorHintEv, .-_ZNK2v88internal11CallPrinter12GetErrorHintEv
	.section	.text._ZN2v88internal11CallPrinter5PrintEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter5PrintEPKc
	.type	_ZN2v88internal11CallPrinter5PrintEPKc, @function
_ZN2v88internal11CallPrinter5PrintEPKc:
.LFB19249:
	.cfi_startproc
	endbr64
	cmpb	$0, 28(%rdi)
	je	.L48
	cmpb	$0, 29(%rdi)
	jne	.L48
	jmp	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	.p2align 4,,10
	.p2align 3
.L48:
	ret
	.cfi_endproc
.LFE19249:
	.size	_ZN2v88internal11CallPrinter5PrintEPKc, .-_ZN2v88internal11CallPrinter5PrintEPKc
	.section	.text._ZN2v88internal11CallPrinter5PrintENS0_6HandleINS0_6StringEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter5PrintENS0_6HandleINS0_6StringEEE
	.type	_ZN2v88internal11CallPrinter5PrintENS0_6HandleINS0_6StringEEE, @function
_ZN2v88internal11CallPrinter5PrintENS0_6HandleINS0_6StringEEE:
.LFB19250:
	.cfi_startproc
	endbr64
	cmpb	$0, 28(%rdi)
	je	.L50
	cmpb	$0, 29(%rdi)
	je	.L52
.L50:
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	addl	$1, 8(%rdi)
	movq	16(%rdi), %rdi
	jmp	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	.cfi_endproc
.LFE19250:
	.size	_ZN2v88internal11CallPrinter5PrintENS0_6HandleINS0_6StringEEE, .-_ZN2v88internal11CallPrinter5PrintENS0_6HandleINS0_6StringEEE
	.section	.text._ZN2v88internal11CallPrinter24VisitVariableDeclarationEPNS0_19VariableDeclarationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter24VisitVariableDeclarationEPNS0_19VariableDeclarationE
	.type	_ZN2v88internal11CallPrinter24VisitVariableDeclarationEPNS0_19VariableDeclarationE, @function
_ZN2v88internal11CallPrinter24VisitVariableDeclarationEPNS0_19VariableDeclarationE:
.LFB19252:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE19252:
	.size	_ZN2v88internal11CallPrinter24VisitVariableDeclarationEPNS0_19VariableDeclarationE, .-_ZN2v88internal11CallPrinter24VisitVariableDeclarationEPNS0_19VariableDeclarationE
	.section	.text._ZN2v88internal11CallPrinter24VisitFunctionDeclarationEPNS0_19FunctionDeclarationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter24VisitFunctionDeclarationEPNS0_19FunctionDeclarationE
	.type	_ZN2v88internal11CallPrinter24VisitFunctionDeclarationEPNS0_19FunctionDeclarationE, @function
_ZN2v88internal11CallPrinter24VisitFunctionDeclarationEPNS0_19FunctionDeclarationE:
.LFB23731:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE23731:
	.size	_ZN2v88internal11CallPrinter24VisitFunctionDeclarationEPNS0_19FunctionDeclarationE, .-_ZN2v88internal11CallPrinter24VisitFunctionDeclarationEPNS0_19FunctionDeclarationE
	.section	.text._ZN2v88internal11CallPrinter19VisitEmptyStatementEPNS0_14EmptyStatementE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter19VisitEmptyStatementEPNS0_14EmptyStatementE
	.type	_ZN2v88internal11CallPrinter19VisitEmptyStatementEPNS0_14EmptyStatementE, @function
_ZN2v88internal11CallPrinter19VisitEmptyStatementEPNS0_14EmptyStatementE:
.LFB23733:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE23733:
	.size	_ZN2v88internal11CallPrinter19VisitEmptyStatementEPNS0_14EmptyStatementE, .-_ZN2v88internal11CallPrinter19VisitEmptyStatementEPNS0_14EmptyStatementE
	.section	.text._ZN2v88internal11CallPrinter22VisitContinueStatementEPNS0_17ContinueStatementE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter22VisitContinueStatementEPNS0_17ContinueStatementE
	.type	_ZN2v88internal11CallPrinter22VisitContinueStatementEPNS0_17ContinueStatementE, @function
_ZN2v88internal11CallPrinter22VisitContinueStatementEPNS0_17ContinueStatementE:
.LFB23735:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE23735:
	.size	_ZN2v88internal11CallPrinter22VisitContinueStatementEPNS0_17ContinueStatementE, .-_ZN2v88internal11CallPrinter22VisitContinueStatementEPNS0_17ContinueStatementE
	.section	.text._ZN2v88internal11CallPrinter19VisitBreakStatementEPNS0_14BreakStatementE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter19VisitBreakStatementEPNS0_14BreakStatementE
	.type	_ZN2v88internal11CallPrinter19VisitBreakStatementEPNS0_14BreakStatementE, @function
_ZN2v88internal11CallPrinter19VisitBreakStatementEPNS0_14BreakStatementE:
.LFB23737:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE23737:
	.size	_ZN2v88internal11CallPrinter19VisitBreakStatementEPNS0_14BreakStatementE, .-_ZN2v88internal11CallPrinter19VisitBreakStatementEPNS0_14BreakStatementE
	.section	.text._ZN2v88internal11CallPrinter22VisitDebuggerStatementEPNS0_17DebuggerStatementE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter22VisitDebuggerStatementEPNS0_17DebuggerStatementE
	.type	_ZN2v88internal11CallPrinter22VisitDebuggerStatementEPNS0_17DebuggerStatementE, @function
_ZN2v88internal11CallPrinter22VisitDebuggerStatementEPNS0_17DebuggerStatementE:
.LFB23739:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE23739:
	.size	_ZN2v88internal11CallPrinter22VisitDebuggerStatementEPNS0_17DebuggerStatementE, .-_ZN2v88internal11CallPrinter22VisitDebuggerStatementEPNS0_17DebuggerStatementE
	.section	.text._ZN2v88internal11CallPrinter26VisitNativeFunctionLiteralEPNS0_21NativeFunctionLiteralE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter26VisitNativeFunctionLiteralEPNS0_21NativeFunctionLiteralE
	.type	_ZN2v88internal11CallPrinter26VisitNativeFunctionLiteralEPNS0_21NativeFunctionLiteralE, @function
_ZN2v88internal11CallPrinter26VisitNativeFunctionLiteralEPNS0_21NativeFunctionLiteralE:
.LFB23743:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE23743:
	.size	_ZN2v88internal11CallPrinter26VisitNativeFunctionLiteralEPNS0_21NativeFunctionLiteralE, .-_ZN2v88internal11CallPrinter26VisitNativeFunctionLiteralEPNS0_21NativeFunctionLiteralE
	.section	.text._ZN2v88internal11CallPrinter21VisitResolvedPropertyEPNS0_16ResolvedPropertyE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter21VisitResolvedPropertyEPNS0_16ResolvedPropertyE
	.type	_ZN2v88internal11CallPrinter21VisitResolvedPropertyEPNS0_16ResolvedPropertyE, @function
_ZN2v88internal11CallPrinter21VisitResolvedPropertyEPNS0_16ResolvedPropertyE:
.LFB23745:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE23745:
	.size	_ZN2v88internal11CallPrinter21VisitResolvedPropertyEPNS0_16ResolvedPropertyE, .-_ZN2v88internal11CallPrinter21VisitResolvedPropertyEPNS0_16ResolvedPropertyE
	.section	.rodata._ZN2v88internal11CallPrinter21VisitEmptyParenthesesEPNS0_16EmptyParenthesesE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal11CallPrinter21VisitEmptyParenthesesEPNS0_16EmptyParenthesesE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter21VisitEmptyParenthesesEPNS0_16EmptyParenthesesE
	.type	_ZN2v88internal11CallPrinter21VisitEmptyParenthesesEPNS0_16EmptyParenthesesE, @function
_ZN2v88internal11CallPrinter21VisitEmptyParenthesesEPNS0_16EmptyParenthesesE:
.LFB19301:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19301:
	.size	_ZN2v88internal11CallPrinter21VisitEmptyParenthesesEPNS0_16EmptyParenthesesE, .-_ZN2v88internal11CallPrinter21VisitEmptyParenthesesEPNS0_16EmptyParenthesesE
	.section	.text._ZN2v88internal11CallPrinter22VisitGetTemplateObjectEPNS0_17GetTemplateObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter22VisitGetTemplateObjectEPNS0_17GetTemplateObjectE
	.type	_ZN2v88internal11CallPrinter22VisitGetTemplateObjectEPNS0_17GetTemplateObjectE, @function
_ZN2v88internal11CallPrinter22VisitGetTemplateObjectEPNS0_17GetTemplateObjectE:
.LFB23741:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE23741:
	.size	_ZN2v88internal11CallPrinter22VisitGetTemplateObjectEPNS0_17GetTemplateObjectE, .-_ZN2v88internal11CallPrinter22VisitGetTemplateObjectEPNS0_17GetTemplateObjectE
	.section	.rodata._ZN2v88internal11CallPrinter19VisitThisExpressionEPNS0_14ThisExpressionE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"this"
	.section	.text._ZN2v88internal11CallPrinter19VisitThisExpressionEPNS0_14ThisExpressionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter19VisitThisExpressionEPNS0_14ThisExpressionE
	.type	_ZN2v88internal11CallPrinter19VisitThisExpressionEPNS0_14ThisExpressionE, @function
_ZN2v88internal11CallPrinter19VisitThisExpressionEPNS0_14ThisExpressionE:
.LFB19305:
	.cfi_startproc
	endbr64
	cmpb	$0, 28(%rdi)
	je	.L64
	cmpb	$0, 29(%rdi)
	jne	.L64
	leaq	.LC1(%rip), %rsi
	jmp	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	.p2align 4,,10
	.p2align 3
.L64:
	ret
	.cfi_endproc
.LFE19305:
	.size	_ZN2v88internal11CallPrinter19VisitThisExpressionEPNS0_14ThisExpressionE, .-_ZN2v88internal11CallPrinter19VisitThisExpressionEPNS0_14ThisExpressionE
	.section	.text._ZN2v88internal11CallPrinter27VisitSuperPropertyReferenceEPNS0_22SuperPropertyReferenceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter27VisitSuperPropertyReferenceEPNS0_22SuperPropertyReferenceE
	.type	_ZN2v88internal11CallPrinter27VisitSuperPropertyReferenceEPNS0_22SuperPropertyReferenceE, @function
_ZN2v88internal11CallPrinter27VisitSuperPropertyReferenceEPNS0_22SuperPropertyReferenceE:
.LFB23747:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE23747:
	.size	_ZN2v88internal11CallPrinter27VisitSuperPropertyReferenceEPNS0_22SuperPropertyReferenceE, .-_ZN2v88internal11CallPrinter27VisitSuperPropertyReferenceEPNS0_22SuperPropertyReferenceE
	.section	.rodata._ZN2v88internal11CallPrinter23VisitSuperCallReferenceEPNS0_18SuperCallReferenceE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"super"
	.section	.text._ZN2v88internal11CallPrinter23VisitSuperCallReferenceEPNS0_18SuperCallReferenceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter23VisitSuperCallReferenceEPNS0_18SuperCallReferenceE
	.type	_ZN2v88internal11CallPrinter23VisitSuperCallReferenceEPNS0_18SuperCallReferenceE, @function
_ZN2v88internal11CallPrinter23VisitSuperCallReferenceEPNS0_18SuperCallReferenceE:
.LFB19307:
	.cfi_startproc
	endbr64
	cmpb	$0, 28(%rdi)
	je	.L67
	cmpb	$0, 29(%rdi)
	jne	.L67
	leaq	.LC2(%rip), %rsi
	jmp	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	.p2align 4,,10
	.p2align 3
.L67:
	ret
	.cfi_endproc
.LFE19307:
	.size	_ZN2v88internal11CallPrinter23VisitSuperCallReferenceEPNS0_18SuperCallReferenceE, .-_ZN2v88internal11CallPrinter23VisitSuperCallReferenceEPNS0_18SuperCallReferenceE
	.section	.rodata._ZN2v88internal11CallPrinter12PrintLiteralENS0_6HandleINS0_6ObjectEEEb.str1.1,"aMS",@progbits,1
.LC3:
	.string	"\""
.LC4:
	.string	"null"
.LC5:
	.string	"true"
.LC6:
	.string	"false"
.LC7:
	.string	"undefined"
	.section	.text._ZN2v88internal11CallPrinter12PrintLiteralENS0_6HandleINS0_6ObjectEEEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter12PrintLiteralENS0_6HandleINS0_6ObjectEEEb
	.type	_ZN2v88internal11CallPrinter12PrintLiteralENS0_6HandleINS0_6ObjectEEEb, @function
_ZN2v88internal11CallPrinter12PrintLiteralENS0_6HandleINS0_6ObjectEEEb:
.LFB19310:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$16, %rsp
	movq	(%rsi), %rax
	movq	%rax, %rcx
	notq	%rcx
	testb	$1, %al
	jne	.L102
.L71:
	movq	(%r12), %rdi
	cmpq	%rax, 104(%rdi)
	je	.L103
	cmpq	%rax, 112(%rdi)
	je	.L104
	cmpq	%rax, 120(%rdi)
	je	.L105
	cmpq	%rax, 88(%rdi)
	je	.L106
	andl	$1, %ecx
	jne	.L83
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	je	.L83
	testb	$1, %al
	je	.L69
	movq	-1(%rax), %rdx
	cmpw	$64, 11(%rdx)
	jne	.L69
	movq	41112(%rdi), %r8
	movq	15(%rax), %r13
	testq	%r8, %r8
	je	.L88
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L89:
	addq	$16, %rsp
	movq	%r12, %rdi
	xorl	%edx, %edx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter12PrintLiteralENS0_6HandleINS0_6ObjectEEEb
	.p2align 4,,10
	.p2align 3
.L102:
	.cfi_restore_state
	movq	-1(%rax), %rsi
	cmpw	$63, 11(%rsi)
	ja	.L71
	movzbl	28(%rdi), %eax
	testb	%dl, %dl
	je	.L73
	testb	%al, %al
	je	.L69
	cmpb	$0, 29(%rdi)
	jne	.L76
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	cmpb	$0, 28(%r12)
	je	.L69
.L76:
	cmpb	$0, 29(%r12)
	je	.L107
.L92:
	cmpb	$0, 29(%r12)
	leaq	.LC3(%rip), %rsi
	je	.L100
.L69:
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L103:
	.cfi_restore_state
	cmpb	$0, 28(%r12)
	je	.L69
	cmpb	$0, 29(%r12)
	jne	.L69
	leaq	.LC4(%rip), %rsi
.L100:
	addq	$16, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	.p2align 4,,10
	.p2align 3
.L104:
	.cfi_restore_state
	cmpb	$0, 28(%r12)
	je	.L69
	cmpb	$0, 29(%r12)
	leaq	.LC5(%rip), %rsi
	je	.L100
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L106:
	cmpb	$0, 28(%r12)
	je	.L69
	cmpb	$0, 29(%r12)
	leaq	.LC7(%rip), %rsi
	je	.L100
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L83:
	movl	$1, %edx
	movq	%r13, %rsi
	call	_ZN2v88internal7Factory14NumberToStringENS0_6HandleINS0_6ObjectEEEb@PLT
	cmpb	$0, 28(%r12)
	je	.L69
	cmpb	$0, 29(%r12)
	jne	.L69
	addl	$1, 8(%r12)
	movq	16(%r12), %rdi
	movq	%rax, %rsi
.L101:
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	.p2align 4,,10
	.p2align 3
.L105:
	.cfi_restore_state
	cmpb	$0, 28(%r12)
	je	.L69
	cmpb	$0, 29(%r12)
	leaq	.LC6(%rip), %rsi
	je	.L100
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L73:
	testb	%al, %al
	je	.L69
	cmpb	$0, 29(%rdi)
	jne	.L69
	addl	$1, 8(%rdi)
	movq	%r13, %rsi
	movq	16(%rdi), %rdi
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L107:
	addl	$1, 8(%r12)
	movq	16(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	cmpb	$0, 28(%r12)
	jne	.L92
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L88:
	movq	41088(%rdi), %rsi
	cmpq	41096(%rdi), %rsi
	je	.L108
.L90:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rdi)
	movq	%r13, (%rsi)
	jmp	.L89
.L108:
	movq	%rdi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rdi
	movq	%rax, %rsi
	jmp	.L90
	.cfi_endproc
.LFE19310:
	.size	_ZN2v88internal11CallPrinter12PrintLiteralENS0_6HandleINS0_6ObjectEEEb, .-_ZN2v88internal11CallPrinter12PrintLiteralENS0_6HandleINS0_6ObjectEEEb
	.section	.rodata._ZN2v88internal11CallPrinter18VisitRegExpLiteralEPNS0_13RegExpLiteralE.str1.1,"aMS",@progbits,1
.LC8:
	.string	"/"
.LC9:
	.string	"g"
.LC10:
	.string	"i"
.LC11:
	.string	"m"
.LC12:
	.string	"u"
.LC13:
	.string	"y"
	.section	.text._ZN2v88internal11CallPrinter18VisitRegExpLiteralEPNS0_13RegExpLiteralE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter18VisitRegExpLiteralEPNS0_13RegExpLiteralE
	.type	_ZN2v88internal11CallPrinter18VisitRegExpLiteralEPNS0_13RegExpLiteralE, @function
_ZN2v88internal11CallPrinter18VisitRegExpLiteralEPNS0_13RegExpLiteralE:
.LFB19278:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 28(%rdi)
	movq	%rsi, %rbx
	je	.L110
	cmpb	$0, 29(%rdi)
	jne	.L110
	leaq	.LC8(%rip), %rsi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
.L110:
	movq	16(%rbx), %rax
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal11CallPrinter12PrintLiteralENS0_6HandleINS0_6ObjectEEEb
	cmpb	$0, 28(%r12)
	je	.L141
	cmpb	$0, 29(%r12)
	jne	.L112
	leaq	.LC8(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	movl	8(%rbx), %eax
	testb	$1, %al
	je	.L113
	cmpb	$0, 28(%r12)
	je	.L113
	cmpb	$0, 29(%r12)
	jne	.L114
	leaq	.LC9(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
.L141:
	movl	8(%rbx), %eax
.L113:
	testb	$2, %al
	je	.L115
	cmpb	$0, 28(%r12)
	je	.L115
.L126:
	cmpb	$0, 29(%r12)
	jne	.L116
	leaq	.LC10(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	movl	8(%rbx), %eax
.L115:
	testb	$4, %al
	je	.L117
	cmpb	$0, 28(%r12)
	je	.L118
	cmpb	$0, 29(%r12)
	jne	.L118
	leaq	.LC11(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	movl	8(%rbx), %eax
.L117:
	testb	$16, %al
	je	.L120
	cmpb	$0, 28(%r12)
	je	.L109
	cmpb	$0, 29(%r12)
	jne	.L109
	leaq	.LC12(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	movl	8(%rbx), %eax
.L120:
	testb	$8, %al
	je	.L109
	cmpb	$0, 28(%r12)
	je	.L109
	cmpb	$0, 29(%r12)
	jne	.L109
	popq	%rbx
	movq	%r12, %rdi
	leaq	.LC13(%rip), %rsi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	.p2align 4,,10
	.p2align 3
.L112:
	.cfi_restore_state
	movl	8(%rbx), %eax
	testb	$1, %al
	je	.L139
.L114:
	testb	$2, %al
	je	.L115
.L116:
	testb	$4, %al
	je	.L117
	.p2align 4,,10
	.p2align 3
.L118:
	testb	$16, %al
	je	.L120
.L109:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L139:
	.cfi_restore_state
	testb	$2, %al
	je	.L115
	jmp	.L126
	.cfi_endproc
.LFE19278:
	.size	_ZN2v88internal11CallPrinter18VisitRegExpLiteralEPNS0_13RegExpLiteralE, .-_ZN2v88internal11CallPrinter18VisitRegExpLiteralEPNS0_13RegExpLiteralE
	.section	.rodata._ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE.str1.1,"aMS",@progbits,1
.LC14:
	.string	"(intermediate value)"
.LC15:
	.string	"ImportCall("
.LC16:
	.string	")"
.LC17:
	.string	"(..."
.LC18:
	.string	"(var)"
.LC19:
	.string	"yield* "
	.section	.text._ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE,"axG",@progbits,_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	.type	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE, @function
_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE:
.LFB10246:
	.cfi_startproc
	endbr64
	movl	4(%rsi), %ecx
	movl	%ecx, %eax
	andl	$63, %eax
	cmpb	$57, %al
	ja	.L229
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.L146(%rip), %rdx
	movzbl	%al, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE,"aG",@progbits,_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE,comdat
	.align 4
	.align 4
.L146:
	.long	.L143-.L146
	.long	.L143-.L146
	.long	.L185-.L146
	.long	.L184-.L146
	.long	.L183-.L146
	.long	.L182-.L146
	.long	.L181-.L146
	.long	.L180-.L146
	.long	.L179-.L146
	.long	.L233-.L146
	.long	.L143-.L146
	.long	.L236-.L146
	.long	.L177-.L146
	.long	.L143-.L146
	.long	.L143-.L146
	.long	.L233-.L146
	.long	.L237-.L146
	.long	.L175-.L146
	.long	.L174-.L146
	.long	.L143-.L146
	.long	.L173-.L146
	.long	.L172-.L146
	.long	.L171-.L146
	.long	.L170-.L146
	.long	.L162-.L146
	.long	.L233-.L146
	.long	.L169-.L146
	.long	.L168-.L146
	.long	.L167-.L146
	.long	.L166-.L146
	.long	.L165-.L146
	.long	.L164-.L146
	.long	.L163-.L146
	.long	.L162-.L146
	.long	.L154-.L146
	.long	.L161-.L146
	.long	.L233-.L146
	.long	.L145-.L146
	.long	.L159-.L146
	.long	.L143-.L146
	.long	.L158-.L146
	.long	.L157-.L146
	.long	.L143-.L146
	.long	.L233-.L146
	.long	.L156-.L146
	.long	.L143-.L146
	.long	.L155-.L146
	.long	.L154-.L146
	.long	.L153-.L146
	.long	.L143-.L146
	.long	.L152-.L146
	.long	.L151-.L146
	.long	.L233-.L146
	.long	.L150-.L146
	.long	.L149-.L146
	.long	.L233-.L146
	.long	.L147-.L146
	.long	.L145-.L146
	.section	.text._ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE,"axG",@progbits,_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE,comdat
	.p2align 4,,10
	.p2align 3
.L147:
	cmpb	$0, 28(%rdi)
	movq	8(%rsi), %rsi
	jne	.L202
	movl	(%rsi), %eax
	cmpl	%eax, 24(%rdi)
	jne	.L202
	movzbl	56(%rdi), %eax
	movb	$1, 28(%rdi)
	subl	$9, %eax
	cmpb	$4, %al
	ja	.L203
	movb	$1, 32(%rdi)
.L204:
	cmpb	$0, 29(%r13)
	jne	.L233
	leaq	.LC19(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	.p2align 4,,10
	.p2align 3
.L233:
	movq	8(%r12), %rsi
.L202:
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter4FindEPNS0_7AstNodeEb.constprop.1
	.p2align 4,,10
	.p2align 3
.L159:
	.cfi_restore_state
	movzbl	56(%rdi), %r14d
	movq	%rsi, %rdi
	xorl	%ebx, %ebx
	call	_ZNK2v88internal15FunctionLiteral4kindEv@PLT
	movb	%al, 56(%r13)
	movl	60(%r12), %eax
	testl	%eax, %eax
	jle	.L196
	.p2align 4,,10
	.p2align 3
.L195:
	movq	48(%r12), %rax
	movq	%r13, %rdi
	movq	(%rax,%rbx,8), %rsi
	addq	$1, %rbx
	call	_ZN2v88internal11CallPrinter4FindEPNS0_7AstNodeEb.constprop.1
	cmpl	%ebx, 60(%r12)
	jg	.L195
.L196:
	movb	%r14b, 56(%r13)
.L143:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L154:
	.cfi_restore_state
	movq	8(%rsi), %rsi
	call	_ZN2v88internal11CallPrinter4FindEPNS0_7AstNodeEb.constprop.1
.L237:
	movq	16(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal11CallPrinter4FindEPNS0_7AstNodeEb.constprop.1
	movq	24(%r12), %rsi
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L162:
	addq	$8, %rsp
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter15VisitAssignmentEPNS0_10AssignmentE
.L145:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L174:
	movq	8(%rsi), %rsi
	call	_ZN2v88internal11CallPrinter4FindEPNS0_7AstNodeEb.constprop.1
.L236:
	movq	16(%r12), %rsi
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L177:
	movq	8(%rsi), %rsi
	call	_ZN2v88internal11CallPrinter4FindEPNS0_7AstNodeEb.constprop.1
	movq	16(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal11CallPrinter4FindEPNS0_7AstNodeEb.constprop.1
	movq	24(%r12), %rsi
	movzbl	4(%rsi), %eax
	andl	$63, %eax
	cmpb	$10, %al
	je	.L143
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L184:
	movq	32(%rsi), %rsi
	call	_ZN2v88internal11CallPrinter4FindEPNS0_7AstNodeEb.constprop.1
	movq	24(%r12), %rsi
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L168:
	addq	$8, %rsp
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter18VisitNaryOperationEPNS0_13NaryOperationE
	.p2align 4,,10
	.p2align 3
.L169:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter20VisitBinaryOperationEPNS0_15BinaryOperationE
	.p2align 4,,10
	.p2align 3
.L153:
	.cfi_restore_state
	cmpb	$0, 28(%rdi)
	je	.L143
	cmpb	$0, 29(%rdi)
	leaq	.LC2(%rip), %rsi
	jne	.L143
.L234:
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	.p2align 4,,10
	.p2align 3
.L155:
	.cfi_restore_state
	cmpb	$0, 28(%rdi)
	je	.L198
	cmpb	$0, 29(%rdi)
	jne	.L198
	leaq	.LC17(%rip), %rsi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
.L198:
	movq	16(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal11CallPrinter4FindEPNS0_7AstNodeEb.constprop.0
	cmpb	$0, 28(%r13)
	je	.L143
.L232:
	cmpb	$0, 29(%r13)
	leaq	.LC16(%rip), %rsi
	je	.L234
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L156:
	addq	$8, %rsp
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter13VisitPropertyEPNS0_8PropertyE
	.p2align 4,,10
	.p2align 3
.L175:
	.cfi_restore_state
	movq	8(%rsi), %rsi
	call	_ZN2v88internal11CallPrinter4FindEPNS0_7AstNodeEb.constprop.1
	movq	24(%r12), %rsi
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L171:
	addq	$8, %rsp
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter18VisitObjectLiteralEPNS0_13ObjectLiteralE
	.p2align 4,,10
	.p2align 3
.L172:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter18VisitRegExpLiteralEPNS0_13RegExpLiteralE
	.p2align 4,,10
	.p2align 3
.L173:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter36VisitInitializeClassMembersStatementEPNS0_31InitializeClassMembersStatementE
	.p2align 4,,10
	.p2align 3
.L183:
	.cfi_restore_state
	movq	32(%rsi), %rsi
	testq	%rsi, %rsi
	je	.L186
	call	_ZN2v88internal11CallPrinter4FindEPNS0_7AstNodeEb.constprop.1
.L186:
	movq	40(%r12), %rsi
	testq	%rsi, %rsi
	je	.L187
	movq	%r13, %rdi
	call	_ZN2v88internal11CallPrinter4FindEPNS0_7AstNodeEb.constprop.1
.L187:
	movq	48(%r12), %rsi
	testq	%rsi, %rsi
	je	.L188
	movq	%r13, %rdi
	call	_ZN2v88internal11CallPrinter4FindEPNS0_7AstNodeEb.constprop.1
.L188:
	movq	24(%r12), %rsi
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L150:
	addq	$8, %rsp
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter19VisitUnaryOperationEPNS0_14UnaryOperationE
	.p2align 4,,10
	.p2align 3
.L151:
	.cfi_restore_state
	cmpb	$0, 28(%rdi)
	je	.L143
	cmpb	$0, 29(%rdi)
	leaq	.LC1(%rip), %rsi
	je	.L234
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L152:
	addq	$8, %rsp
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter20VisitTemplateLiteralEPNS0_15TemplateLiteralE
	.p2align 4,,10
	.p2align 3
.L164:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter17VisitClassLiteralEPNS0_12ClassLiteralE
	.p2align 4,,10
	.p2align 3
.L165:
	.cfi_restore_state
	cmpb	$0, 28(%rdi)
	jne	.L143
	movl	36(%rsi), %edx
	testl	%edx, %edx
	jle	.L143
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L194:
	movq	24(%r12), %rax
	movq	%r13, %rdi
	movq	(%rax,%rbx,8), %rsi
	addq	$1, %rbx
	call	_ZN2v88internal11CallPrinter4FindEPNS0_7AstNodeEb.constprop.1
	cmpl	%ebx, 36(%r12)
	jg	.L194
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L166:
	addq	$8, %rsp
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter12VisitCallNewEPNS0_7CallNewE
	.p2align 4,,10
	.p2align 3
.L167:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter9VisitCallEPNS0_4CallE
	.p2align 4,,10
	.p2align 3
.L170:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter17VisitArrayLiteralEPNS0_12ArrayLiteralE
	.p2align 4,,10
	.p2align 3
.L157:
	.cfi_restore_state
	movq	(%rdi), %rsi
	movq	%r12, %rdi
	call	_ZNK2v88internal7Literal10BuildValueEPNS0_7IsolateE@PLT
	movl	$1, %edx
	movq	%rax, %rsi
.L235:
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter12PrintLiteralENS0_6HandleINS0_6ObjectEEEb
	.p2align 4,,10
	.p2align 3
.L158:
	.cfi_restore_state
	cmpb	$0, 28(%rdi)
	je	.L197
	cmpb	$0, 29(%rdi)
	jne	.L197
	leaq	.LC15(%rip), %rsi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
.L197:
	movq	8(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal11CallPrinter4FindEPNS0_7AstNodeEb.constprop.0
	cmpb	$0, 28(%r13)
	jne	.L232
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L185:
	movq	24(%rsi), %rsi
	call	_ZN2v88internal11CallPrinter4FindEPNS0_7AstNodeEb.constprop.1
	movq	32(%r12), %rsi
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L163:
	addq	$8, %rsp
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter21VisitCompareOperationEPNS0_16CompareOperationE
	.p2align 4,,10
	.p2align 3
.L161:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter19VisitCountOperationEPNS0_14CountOperationE
	.p2align 4,,10
	.p2align 3
.L149:
	.cfi_restore_state
	cmpb	$0, 30(%rdi)
	je	.L200
	movq	8(%rsi), %rax
	andb	$1, %ch
	je	.L201
	movq	8(%rax), %rax
.L201:
	movq	(%rax), %rsi
	xorl	%edx, %edx
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L179:
	addq	$8, %rsp
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter20VisitSwitchStatementEPNS0_15SwitchStatementE
	.p2align 4,,10
	.p2align 3
.L181:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter19VisitForOfStatementEPNS0_14ForOfStatementE
	.p2align 4,,10
	.p2align 3
.L182:
	.cfi_restore_state
	movq	32(%rsi), %rsi
	call	_ZN2v88internal11CallPrinter4FindEPNS0_7AstNodeEb.constprop.1
	movq	40(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal11CallPrinter4FindEPNS0_7AstNodeEb.constprop.1
	movq	24(%r12), %rsi
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L180:
	movl	20(%rsi), %eax
	xorl	%ebx, %ebx
	leaq	.LC14(%rip), %r14
	testl	%eax, %eax
	jg	.L189
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L238:
	cmpb	$0, 29(%r13)
	jne	.L191
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	movl	20(%r12), %eax
.L191:
	addq	$1, %rbx
	cmpl	%ebx, %eax
	jle	.L143
.L189:
	cmpb	$0, 28(%r13)
	jne	.L238
	cmpb	$0, 72(%r13)
	jne	.L191
	movq	8(%r12), %rax
	movq	(%rax,%rbx,8), %r15
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r13), %rax
	jb	.L239
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movl	20(%r12), %eax
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L229:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L200:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	cmpb	$0, 28(%rdi)
	je	.L143
	cmpb	$0, 29(%rdi)
	jne	.L143
	leaq	.LC18(%rip), %rsi
	jmp	.L234
.L239:
	movb	$1, 72(%r13)
	movl	20(%r12), %eax
	jmp	.L191
.L203:
	movb	$1, 31(%rdi)
	jmp	.L204
	.cfi_endproc
.LFE10246:
	.size	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE, .-_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	.section	.text._ZN2v88internal11CallPrinter4FindEPNS0_7AstNodeEb.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11CallPrinter4FindEPNS0_7AstNodeEb.constprop.0, @function
_ZN2v88internal11CallPrinter4FindEPNS0_7AstNodeEb.constprop.0:
.LFB23749:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	cmpb	$0, 28(%rdi)
	movzbl	72(%rdi), %eax
	je	.L241
	testb	%al, %al
	je	.L251
.L242:
	cmpb	$0, 29(%r12)
	jne	.L240
	addq	$8, %rsp
	movq	%r12, %rdi
	leaq	.LC14(%rip), %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	.p2align 4,,10
	.p2align 3
.L251:
	.cfi_restore_state
	movl	8(%rdi), %ebx
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L252
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
.L244:
	cmpl	8(%r12), %ebx
	je	.L253
.L240:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L241:
	.cfi_restore_state
	testb	%al, %al
	jne	.L240
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L254
	addq	$8, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
.L253:
	.cfi_restore_state
	cmpb	$0, 28(%r12)
	jne	.L242
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L252:
	.cfi_restore_state
	movb	$1, 72(%r12)
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L254:
	movb	$1, 72(%r12)
	jmp	.L240
	.cfi_endproc
.LFE23749:
	.size	_ZN2v88internal11CallPrinter4FindEPNS0_7AstNodeEb.constprop.0, .-_ZN2v88internal11CallPrinter4FindEPNS0_7AstNodeEb.constprop.0
	.section	.text._ZN2v88internal11CallPrinter4FindEPNS0_7AstNodeEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter4FindEPNS0_7AstNodeEb
	.type	_ZN2v88internal11CallPrinter4FindEPNS0_7AstNodeEb, @function
_ZN2v88internal11CallPrinter4FindEPNS0_7AstNodeEb:
.LFB19248:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	cmpb	$0, 28(%rdi)
	je	.L256
	testb	%dl, %dl
	jne	.L257
.L260:
	cmpb	$0, 29(%r12)
	jne	.L255
	addq	$8, %rsp
	movq	%r12, %rdi
	leaq	.LC14(%rip), %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	.p2align 4,,10
	.p2align 3
.L257:
	.cfi_restore_state
	cmpb	$0, 72(%rdi)
	jne	.L260
	movl	8(%rdi), %ebx
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L269
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
.L262:
	cmpl	%ebx, 8(%r12)
	je	.L270
.L255:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L256:
	.cfi_restore_state
	cmpb	$0, 72(%rdi)
	jne	.L255
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L271
	addq	$8, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
.L270:
	.cfi_restore_state
	cmpb	$0, 28(%r12)
	jne	.L260
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L271:
	.cfi_restore_state
	movb	$1, 72(%r12)
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L269:
	movb	$1, 72(%r12)
	jmp	.L262
	.cfi_endproc
.LFE19248:
	.size	_ZN2v88internal11CallPrinter4FindEPNS0_7AstNodeEb, .-_ZN2v88internal11CallPrinter4FindEPNS0_7AstNodeEb
	.section	.text._ZN2v88internal11CallPrinter4FindEPNS0_7AstNodeEb.constprop.1,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11CallPrinter4FindEPNS0_7AstNodeEb.constprop.1, @function
_ZN2v88internal11CallPrinter4FindEPNS0_7AstNodeEb.constprop.1:
.LFB23748:
	.cfi_startproc
	cmpb	$0, 28(%rdi)
	je	.L273
	cmpb	$0, 29(%rdi)
	jne	.L277
	leaq	.LC14(%rip), %rsi
	jmp	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	.p2align 4,,10
	.p2align 3
.L277:
	ret
	.p2align 4,,10
	.p2align 3
.L273:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	cmpb	$0, 72(%rdi)
	movq	%rsi, -16(%rbp)
	movq	%rdi, -8(%rbp)
	je	.L279
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L279:
	.cfi_restore_state
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	movq	-8(%rbp), %rdi
	movq	-16(%rbp), %rsi
	cmpq	64(%rdi), %rax
	jb	.L280
	leave
	.cfi_remember_state
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	.p2align 4,,10
	.p2align 3
.L280:
	.cfi_restore_state
	movb	$1, 72(%rdi)
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23748:
	.size	_ZN2v88internal11CallPrinter4FindEPNS0_7AstNodeEb.constprop.1, .-_ZN2v88internal11CallPrinter4FindEPNS0_7AstNodeEb.constprop.1
	.section	.text._ZN2v88internal11CallPrinter15VisitAssignmentEPNS0_10AssignmentE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter15VisitAssignmentEPNS0_10AssignmentE
	.type	_ZN2v88internal11CallPrinter15VisitAssignmentEPNS0_10AssignmentE, @function
_ZN2v88internal11CallPrinter15VisitAssignmentEPNS0_10AssignmentE:
.LFB19282:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	8(%rsi), %rsi
	movzbl	4(%rsi), %eax
	andl	$63, %eax
	cmpb	$22, %al
	jne	.L282
	movl	24(%rdi), %edi
	cmpl	(%rsi), %edi
	je	.L293
	movq	24(%rsi), %rax
	movslq	36(%rsi), %rdx
	leaq	(%rax,%rdx,8), %r8
	cmpq	%r8, %rax
	jne	.L286
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L285:
	addq	$8, %rax
	cmpq	%rax, %r8
	je	.L282
.L286:
	movq	(%rax), %rdx
	movq	8(%rdx), %rcx
	cmpl	(%rcx), %edi
	jne	.L285
	movzbl	28(%r12), %eax
	movq	%rdx, %xmm0
	movq	%rbx, %xmm1
	movb	$1, 28(%r12)
	punpcklqdq	%xmm1, %xmm0
	xorl	$1, %eax
	movups	%xmm0, 40(%r12)
	testb	%al, %al
	je	.L294
.L287:
	movq	16(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter4FindEPNS0_7AstNodeEb.constprop.0
.L292:
	movl	$256, %eax
	movw	%ax, 28(%r12)
.L281:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L293:
	.cfi_restore_state
	movzbl	28(%r12), %eax
	movq	%rbx, 48(%r12)
	movb	$1, 28(%r12)
	xorl	$1, %eax
	testb	%al, %al
	jne	.L287
.L294:
	movq	8(%rbx), %rsi
.L282:
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter4FindEPNS0_7AstNodeEb.constprop.1
	movq	8(%rbx), %rax
	movq	16(%rbx), %rsi
	movl	4(%rax), %eax
	andl	$63, %eax
	cmpb	$23, %al
	jne	.L288
	movl	(%rsi), %eax
	cmpl	%eax, 24(%r12)
	je	.L289
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter4FindEPNS0_7AstNodeEb.constprop.0
	.p2align 4,,10
	.p2align 3
.L288:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter4FindEPNS0_7AstNodeEb.constprop.1
	.p2align 4,,10
	.p2align 3
.L289:
	.cfi_restore_state
	movzbl	28(%r12), %r13d
	movb	$1, 31(%r12)
	movq	%r12, %rdi
	movb	$1, 28(%r12)
	movq	16(%rbx), %rsi
	call	_ZN2v88internal11CallPrinter4FindEPNS0_7AstNodeEb.constprop.0
	testb	%r13b, %r13b
	je	.L292
	jmp	.L281
	.cfi_endproc
.LFE19282:
	.size	_ZN2v88internal11CallPrinter15VisitAssignmentEPNS0_10AssignmentE, .-_ZN2v88internal11CallPrinter15VisitAssignmentEPNS0_10AssignmentE
	.section	.text._ZN2v88internal11CallPrinter20VisitSwitchStatementEPNS0_15SwitchStatementE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter20VisitSwitchStatementEPNS0_15SwitchStatementE
	.type	_ZN2v88internal11CallPrinter20VisitSwitchStatementEPNS0_15SwitchStatementE, @function
_ZN2v88internal11CallPrinter20VisitSwitchStatementEPNS0_15SwitchStatementE:
.LFB19262:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	16(%rsi), %rsi
	call	_ZN2v88internal11CallPrinter4FindEPNS0_7AstNodeEb.constprop.1
	movq	24(%r12), %r13
	movslq	36(%r12), %rax
	leaq	0(%r13,%rax,8), %rax
	movq	%rax, -64(%rbp)
	cmpq	%r13, %rax
	je	.L295
	leaq	.LC14(%rip), %r15
	.p2align 4,,10
	.p2align 3
.L300:
	movq	0(%r13), %r14
	movq	(%r14), %rsi
	testq	%rsi, %rsi
	je	.L297
	movq	%rbx, %rdi
	call	_ZN2v88internal11CallPrinter4FindEPNS0_7AstNodeEb.constprop.1
.L297:
	movl	20(%r14), %eax
	xorl	%r12d, %r12d
	testl	%eax, %eax
	jg	.L298
	jmp	.L304
	.p2align 4,,10
	.p2align 3
.L313:
	cmpb	$0, 29(%rbx)
	jne	.L302
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	movl	20(%r14), %eax
.L302:
	addq	$1, %r12
	cmpl	%r12d, %eax
	jle	.L304
.L298:
	cmpb	$0, 28(%rbx)
	jne	.L313
	cmpb	$0, 72(%rbx)
	jne	.L302
	movq	8(%r14), %rax
	movq	(%rax,%r12,8), %rsi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%rbx), %rax
	movq	-56(%rbp), %rsi
	jb	.L314
	movq	%rbx, %rdi
	addq	$1, %r12
	call	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movl	20(%r14), %eax
	cmpl	%r12d, %eax
	jg	.L298
	.p2align 4,,10
	.p2align 3
.L304:
	addq	$8, %r13
	cmpq	%r13, -64(%rbp)
	jne	.L300
.L295:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L314:
	.cfi_restore_state
	movb	$1, 72(%rbx)
	movl	20(%r14), %eax
	jmp	.L302
	.cfi_endproc
.LFE19262:
	.size	_ZN2v88internal11CallPrinter20VisitSwitchStatementEPNS0_15SwitchStatementE, .-_ZN2v88internal11CallPrinter20VisitSwitchStatementEPNS0_15SwitchStatementE
	.section	.text._ZN2v88internal11CallPrinter14FindStatementsEPKNS0_8ZoneListIPNS0_9StatementEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter14FindStatementsEPKNS0_8ZoneListIPNS0_9StatementEEE
	.type	_ZN2v88internal11CallPrinter14FindStatementsEPKNS0_8ZoneListIPNS0_9StatementEEE, @function
_ZN2v88internal11CallPrinter14FindStatementsEPKNS0_8ZoneListIPNS0_9StatementEEE:
.LFB19308:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L324
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	12(%rsi), %eax
	testl	%eax, %eax
	jle	.L315
	movq	%rdi, %r12
	xorl	%ebx, %ebx
	leaq	.LC14(%rip), %r14
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L327:
	cmpb	$0, 29(%r12)
	jne	.L319
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	movl	12(%r13), %eax
.L319:
	addq	$1, %rbx
	cmpl	%ebx, %eax
	jle	.L315
.L321:
	cmpb	$0, 28(%r12)
	jne	.L327
	cmpb	$0, 72(%r12)
	jne	.L319
	movq	0(%r13), %rax
	movq	(%rax,%rbx,8), %r15
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L328
	movq	%r15, %rsi
	movq	%r12, %rdi
	addq	$1, %rbx
	call	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movl	12(%r13), %eax
	cmpl	%ebx, %eax
	jg	.L321
.L315:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L328:
	.cfi_restore_state
	movb	$1, 72(%r12)
	movl	12(%r13), %eax
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L324:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE19308:
	.size	_ZN2v88internal11CallPrinter14FindStatementsEPKNS0_8ZoneListIPNS0_9StatementEEE, .-_ZN2v88internal11CallPrinter14FindStatementsEPKNS0_8ZoneListIPNS0_9StatementEEE
	.section	.text._ZN2v88internal11CallPrinter13FindArgumentsEPKNS0_8ZoneListIPNS0_10ExpressionEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter13FindArgumentsEPKNS0_8ZoneListIPNS0_10ExpressionEEE
	.type	_ZN2v88internal11CallPrinter13FindArgumentsEPKNS0_8ZoneListIPNS0_10ExpressionEEE, @function
_ZN2v88internal11CallPrinter13FindArgumentsEPKNS0_8ZoneListIPNS0_10ExpressionEEE:
.LFB19309:
	.cfi_startproc
	endbr64
	movzbl	28(%rdi), %eax
	testb	%al, %al
	jne	.L336
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	12(%rsi), %edx
	testl	%edx, %edx
	jle	.L329
	movq	%rdi, %r12
	xorl	%ebx, %ebx
	leaq	.LC14(%rip), %r14
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L339:
	cmpb	$0, 29(%r12)
	jne	.L332
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	movl	12(%r13), %edx
.L332:
	addq	$1, %rbx
	cmpl	%ebx, %edx
	jle	.L329
.L341:
	movzbl	28(%r12), %eax
.L334:
	testb	%al, %al
	jne	.L339
	cmpb	$0, 72(%r12)
	jne	.L332
	movq	0(%r13), %rax
	movq	(%rax,%rbx,8), %r15
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L340
	movq	%r15, %rsi
	movq	%r12, %rdi
	addq	$1, %rbx
	call	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movl	12(%r13), %edx
	cmpl	%ebx, %edx
	jg	.L341
.L329:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L336:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L340:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movb	$1, 72(%r12)
	movl	12(%r13), %edx
	jmp	.L332
	.cfi_endproc
.LFE19309:
	.size	_ZN2v88internal11CallPrinter13FindArgumentsEPKNS0_8ZoneListIPNS0_10ExpressionEEE, .-_ZN2v88internal11CallPrinter13FindArgumentsEPKNS0_8ZoneListIPNS0_10ExpressionEEE
	.section	.text._ZN2v88internal11CallPrinter20VisitTemplateLiteralEPNS0_15TemplateLiteralE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter20VisitTemplateLiteralEPNS0_15TemplateLiteralE
	.type	_ZN2v88internal11CallPrinter20VisitTemplateLiteralEPNS0_15TemplateLiteralE, @function
_ZN2v88internal11CallPrinter20VisitTemplateLiteralEPNS0_15TemplateLiteralE:
.LFB19303:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rsi), %rax
	movq	(%rax), %rbx
	movslq	12(%rax), %rax
	leaq	(%rbx,%rax,8), %r13
	cmpq	%r13, %rbx
	je	.L342
	movq	%rdi, %r12
	jmp	.L353
	.p2align 4,,10
	.p2align 3
.L357:
	testb	%al, %al
	je	.L356
.L345:
	cmpb	$0, 29(%r12)
	jne	.L349
	leaq	.LC14(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	.p2align 4,,10
	.p2align 3
.L349:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	je	.L342
.L353:
	cmpb	$0, 28(%r12)
	movq	(%rbx), %r14
	movzbl	72(%r12), %eax
	jne	.L357
	testb	%al, %al
	jne	.L349
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L358
	movq	%r14, %rsi
	movq	%r12, %rdi
	addq	$8, %rbx
	call	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	cmpq	%rbx, %r13
	jne	.L353
.L342:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L356:
	.cfi_restore_state
	movl	8(%r12), %r15d
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L359
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
.L347:
	cmpl	8(%r12), %r15d
	jne	.L349
	cmpb	$0, 28(%r12)
	je	.L349
	jmp	.L345
	.p2align 4,,10
	.p2align 3
.L359:
	movb	$1, 72(%r12)
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L358:
	movb	$1, 72(%r12)
	jmp	.L349
	.cfi_endproc
.LFE19303:
	.size	_ZN2v88internal11CallPrinter20VisitTemplateLiteralEPNS0_15TemplateLiteralE, .-_ZN2v88internal11CallPrinter20VisitTemplateLiteralEPNS0_15TemplateLiteralE
	.section	.text._ZN2v88internal11CallPrinter10VisitBlockEPNS0_5BlockE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter10VisitBlockEPNS0_5BlockE
	.type	_ZN2v88internal11CallPrinter10VisitBlockEPNS0_5BlockE, @function
_ZN2v88internal11CallPrinter10VisitBlockEPNS0_5BlockE:
.LFB19251:
	.cfi_startproc
	endbr64
	movl	20(%rsi), %eax
	testl	%eax, %eax
	jle	.L370
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	.LC14(%rip), %r14
	pushq	%r13
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	jmp	.L361
	.p2align 4,,10
	.p2align 3
.L374:
	cmpb	$0, 29(%rbx)
	jne	.L363
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	movl	20(%r12), %eax
.L363:
	addq	$1, %r13
	cmpl	%r13d, %eax
	jle	.L373
.L361:
	cmpb	$0, 28(%rbx)
	jne	.L374
	cmpb	$0, 72(%rbx)
	jne	.L363
	movq	8(%r12), %rax
	movq	(%rax,%r13,8), %r15
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%rbx), %rax
	jb	.L375
	movq	%r15, %rsi
	movq	%rbx, %rdi
	addq	$1, %r13
	call	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movl	20(%r12), %eax
	cmpl	%r13d, %eax
	jg	.L361
.L373:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L375:
	.cfi_restore_state
	movb	$1, 72(%rbx)
	movl	20(%r12), %eax
	jmp	.L363
	.p2align 4,,10
	.p2align 3
.L370:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE19251:
	.size	_ZN2v88internal11CallPrinter10VisitBlockEPNS0_5BlockE, .-_ZN2v88internal11CallPrinter10VisitBlockEPNS0_5BlockE
	.section	.text._ZN2v88internal11CallPrinter20VisitFunctionLiteralEPNS0_15FunctionLiteralE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter20VisitFunctionLiteralEPNS0_15FunctionLiteralE
	.type	_ZN2v88internal11CallPrinter20VisitFunctionLiteralEPNS0_15FunctionLiteralE, @function
_ZN2v88internal11CallPrinter20VisitFunctionLiteralEPNS0_15FunctionLiteralE:
.LFB19271:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	.LC14(%rip), %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movzbl	56(%rdi), %r14d
	movq	%rsi, %rdi
	call	_ZNK2v88internal15FunctionLiteral4kindEv@PLT
	movb	%al, 56(%rbx)
	movl	60(%r12), %eax
	testl	%eax, %eax
	jg	.L377
	jmp	.L381
	.p2align 4,,10
	.p2align 3
.L388:
	cmpb	$0, 29(%rbx)
	jne	.L379
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	movl	60(%r12), %eax
.L379:
	addq	$1, %r13
	cmpl	%r13d, %eax
	jle	.L381
.L377:
	cmpb	$0, 28(%rbx)
	jne	.L388
	cmpb	$0, 72(%rbx)
	jne	.L379
	movq	48(%r12), %rax
	movq	(%rax,%r13,8), %rsi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%rbx), %rax
	movq	-56(%rbp), %rsi
	jb	.L389
	movq	%rbx, %rdi
	addq	$1, %r13
	call	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movl	60(%r12), %eax
	cmpl	%r13d, %eax
	jg	.L377
.L381:
	movb	%r14b, 56(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L389:
	.cfi_restore_state
	movb	$1, 72(%rbx)
	movl	60(%r12), %eax
	jmp	.L379
	.cfi_endproc
.LFE19271:
	.size	_ZN2v88internal11CallPrinter20VisitFunctionLiteralEPNS0_15FunctionLiteralE, .-_ZN2v88internal11CallPrinter20VisitFunctionLiteralEPNS0_15FunctionLiteralE
	.section	.text._ZN2v88internal11CallPrinter36VisitInitializeClassMembersStatementEPNS0_31InitializeClassMembersStatementE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter36VisitInitializeClassMembersStatementEPNS0_31InitializeClassMembersStatementE
	.type	_ZN2v88internal11CallPrinter36VisitInitializeClassMembersStatementEPNS0_31InitializeClassMembersStatementE, @function
_ZN2v88internal11CallPrinter36VisitInitializeClassMembersStatementEPNS0_31InitializeClassMembersStatementE:
.LFB19273:
	.cfi_startproc
	endbr64
	movq	8(%rsi), %rdx
	movl	12(%rdx), %eax
	testl	%eax, %eax
	jle	.L400
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	.LC14(%rip), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$8, %rsp
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L404:
	cmpb	$0, 29(%r12)
	jne	.L393
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	movq	8(%r13), %rdx
	movl	12(%rdx), %eax
.L393:
	addq	$1, %rbx
	cmpl	%ebx, %eax
	jle	.L403
.L391:
	cmpb	$0, 28(%r12)
	jne	.L404
	cmpb	$0, 72(%r12)
	jne	.L393
	movq	(%rdx), %rax
	movq	(%rax,%rbx,8), %rax
	movq	8(%rax), %r15
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L405
	movq	%r15, %rsi
	movq	%r12, %rdi
	addq	$1, %rbx
	call	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movq	8(%r13), %rdx
	movl	12(%rdx), %eax
	cmpl	%ebx, %eax
	jg	.L391
.L403:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L405:
	.cfi_restore_state
	movb	$1, 72(%r12)
	movq	8(%r13), %rdx
	movl	12(%rdx), %eax
	jmp	.L393
	.p2align 4,,10
	.p2align 3
.L400:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE19273:
	.size	_ZN2v88internal11CallPrinter36VisitInitializeClassMembersStatementEPNS0_31InitializeClassMembersStatementE, .-_ZN2v88internal11CallPrinter36VisitInitializeClassMembersStatementEPNS0_31InitializeClassMembersStatementE
	.section	.text._ZN2v88internal11CallPrinter17VisitClassLiteralEPNS0_12ClassLiteralE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter17VisitClassLiteralEPNS0_12ClassLiteralE
	.type	_ZN2v88internal11CallPrinter17VisitClassLiteralEPNS0_12ClassLiteralE, @function
_ZN2v88internal11CallPrinter17VisitClassLiteralEPNS0_12ClassLiteralE:
.LFB19272:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	32(%rsi), %rsi
	testq	%rsi, %rsi
	je	.L407
	call	_ZN2v88internal11CallPrinter4FindEPNS0_7AstNodeEb.constprop.1
.L407:
	movq	48(%r13), %rdx
	xorl	%ebx, %ebx
	leaq	.LC14(%rip), %r14
	movl	12(%rdx), %eax
	testl	%eax, %eax
	jg	.L408
	jmp	.L406
	.p2align 4,,10
	.p2align 3
.L420:
	cmpb	$0, 29(%r12)
	jne	.L410
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	movq	48(%r13), %rdx
	movl	12(%rdx), %eax
.L410:
	addq	$1, %rbx
	cmpl	%ebx, %eax
	jle	.L406
.L408:
	cmpb	$0, 28(%r12)
	jne	.L420
	cmpb	$0, 72(%r12)
	jne	.L410
	movq	(%rdx), %rax
	movq	(%rax,%rbx,8), %rax
	movq	8(%rax), %r15
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L421
	movq	%r15, %rsi
	movq	%r12, %rdi
	addq	$1, %rbx
	call	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movq	48(%r13), %rdx
	movl	12(%rdx), %eax
	cmpl	%ebx, %eax
	jg	.L408
.L406:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L421:
	.cfi_restore_state
	movb	$1, 72(%r12)
	movq	48(%r13), %rdx
	movl	12(%rdx), %eax
	jmp	.L410
	.cfi_endproc
.LFE19272:
	.size	_ZN2v88internal11CallPrinter17VisitClassLiteralEPNS0_12ClassLiteralE, .-_ZN2v88internal11CallPrinter17VisitClassLiteralEPNS0_12ClassLiteralE
	.section	.text._ZN2v88internal11CallPrinter16VisitCallRuntimeEPNS0_11CallRuntimeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter16VisitCallRuntimeEPNS0_11CallRuntimeE
	.type	_ZN2v88internal11CallPrinter16VisitCallRuntimeEPNS0_11CallRuntimeE, @function
_ZN2v88internal11CallPrinter16VisitCallRuntimeEPNS0_11CallRuntimeE:
.LFB19293:
	.cfi_startproc
	endbr64
	movzbl	28(%rdi), %edx
	testb	%dl, %dl
	jne	.L429
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	36(%rsi), %eax
	testl	%eax, %eax
	jle	.L422
	movq	%rdi, %rbx
	xorl	%r13d, %r13d
	leaq	.LC14(%rip), %r14
	jmp	.L427
	.p2align 4,,10
	.p2align 3
.L432:
	cmpb	$0, 29(%rbx)
	jne	.L425
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	movl	36(%r12), %eax
.L425:
	addq	$1, %r13
	cmpl	%r13d, %eax
	jle	.L422
.L434:
	movzbl	28(%rbx), %edx
.L427:
	testb	%dl, %dl
	jne	.L432
	cmpb	$0, 72(%rbx)
	jne	.L425
	movq	24(%r12), %rax
	movq	(%rax,%r13,8), %r15
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%rbx), %rax
	jb	.L433
	movq	%r15, %rsi
	movq	%rbx, %rdi
	addq	$1, %r13
	call	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movl	36(%r12), %eax
	cmpl	%r13d, %eax
	jg	.L434
.L422:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L429:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L433:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movb	$1, 72(%rbx)
	movl	36(%r12), %eax
	jmp	.L425
	.cfi_endproc
.LFE19293:
	.size	_ZN2v88internal11CallPrinter16VisitCallRuntimeEPNS0_11CallRuntimeE, .-_ZN2v88internal11CallPrinter16VisitCallRuntimeEPNS0_11CallRuntimeE
	.section	.rodata._ZN2v88internal11CallPrinter18VisitObjectLiteralEPNS0_13ObjectLiteralE.str1.1,"aMS",@progbits,1
.LC20:
	.string	"{"
.LC21:
	.string	"}"
	.section	.text._ZN2v88internal11CallPrinter18VisitObjectLiteralEPNS0_13ObjectLiteralE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter18VisitObjectLiteralEPNS0_13ObjectLiteralE
	.type	_ZN2v88internal11CallPrinter18VisitObjectLiteralEPNS0_13ObjectLiteralE, @function
_ZN2v88internal11CallPrinter18VisitObjectLiteralEPNS0_13ObjectLiteralE:
.LFB19279:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movzbl	28(%rdi), %eax
	testb	%al, %al
	je	.L436
	movzbl	29(%rdi), %eax
	testb	%al, %al
	jne	.L437
	leaq	.LC20(%rip), %rsi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	movl	36(%r13), %esi
	movzbl	28(%r12), %eax
	testl	%esi, %esi
	jle	.L438
.L449:
	xorl	%ebx, %ebx
	leaq	.LC14(%rip), %r14
	jmp	.L447
	.p2align 4,,10
	.p2align 3
.L441:
	cmpb	$0, 29(%r12)
	jne	.L457
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	movzbl	28(%r12), %eax
.L443:
	addq	$1, %rbx
	cmpl	%ebx, 36(%r13)
	jle	.L438
.L447:
	testb	%al, %al
	jne	.L441
.L439:
	cmpb	$0, 72(%r12)
	je	.L444
	addq	$1, %rbx
	cmpl	%ebx, 36(%r13)
	jg	.L439
.L435:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L437:
	.cfi_restore_state
	movl	36(%rsi), %edx
	testl	%edx, %edx
	jg	.L449
	.p2align 4,,10
	.p2align 3
.L442:
	cmpb	$0, 29(%r12)
	jne	.L435
	addq	$8, %rsp
	movq	%r12, %rdi
	leaq	.LC21(%rip), %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	.p2align 4,,10
	.p2align 3
.L444:
	.cfi_restore_state
	movq	24(%r13), %rax
	movq	(%rax,%rbx,8), %rax
	movq	8(%rax), %r15
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L458
	movq	%r15, %rsi
	movq	%r12, %rdi
	addq	$1, %rbx
	call	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movzbl	28(%r12), %eax
	cmpl	%ebx, 36(%r13)
	jg	.L447
	.p2align 4,,10
	.p2align 3
.L438:
	testb	%al, %al
	jne	.L442
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L457:
	.cfi_restore_state
	addq	$1, %rbx
	cmpl	%ebx, 36(%r13)
	jg	.L441
	jmp	.L442
	.p2align 4,,10
	.p2align 3
.L458:
	movb	$1, 72(%r12)
	movzbl	28(%r12), %eax
	jmp	.L443
	.p2align 4,,10
	.p2align 3
.L436:
	movl	36(%rsi), %ecx
	testl	%ecx, %ecx
	jg	.L449
	jmp	.L435
	.cfi_endproc
.LFE19279:
	.size	_ZN2v88internal11CallPrinter18VisitObjectLiteralEPNS0_13ObjectLiteralE, .-_ZN2v88internal11CallPrinter18VisitObjectLiteralEPNS0_13ObjectLiteralE
	.section	.rodata._ZN2v88internal11CallPrinter18VisitNaryOperationEPNS0_13NaryOperationE.str1.1,"aMS",@progbits,1
.LC22:
	.string	"("
.LC23:
	.string	" "
	.section	.text._ZN2v88internal11CallPrinter18VisitNaryOperationEPNS0_13NaryOperationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter18VisitNaryOperationEPNS0_13NaryOperationE
	.type	_ZN2v88internal11CallPrinter18VisitNaryOperationEPNS0_13NaryOperationE, @function
_ZN2v88internal11CallPrinter18VisitNaryOperationEPNS0_13NaryOperationE:
.LFB19297:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	cmpb	$0, 28(%rdi)
	je	.L460
	cmpb	$0, 29(%rdi)
	jne	.L460
	leaq	.LC22(%rip), %rsi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
.L460:
	movq	8(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter4FindEPNS0_7AstNodeEb.constprop.0
	movq	24(%r13), %rdx
	cmpq	%rdx, 32(%r13)
	movzbl	28(%r12), %ecx
	je	.L478
	xorl	%ebx, %ebx
	jmp	.L477
	.p2align 4,,10
	.p2align 3
.L498:
	cmpb	$0, 29(%r12)
	jne	.L463
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	movl	4(%r13), %eax
	leaq	_ZN2v88internal5Token7string_E(%rip), %rdi
	shrl	$7, %eax
	andl	$127, %eax
	cmpb	$0, 28(%r12)
	movq	(%rdi,%rax,8), %rsi
	je	.L465
	cmpb	$0, 29(%r12)
	jne	.L496
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	cmpb	$0, 28(%r12)
	je	.L465
	movq	%rbx, %r15
	salq	$4, %r15
	cmpb	$0, 29(%r12)
	jne	.L497
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	movq	24(%r13), %rdx
	cmpb	$0, 28(%r12)
	movq	(%rdx,%r15), %r14
	je	.L468
	cmpb	$0, 72(%r12)
	je	.L480
.L473:
	movzbl	29(%r12), %ecx
	testb	%cl, %cl
	jne	.L495
	leaq	.LC14(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	movzbl	28(%r12), %ecx
	movq	24(%r13), %rdx
	.p2align 4,,10
	.p2align 3
.L472:
	movq	32(%r13), %rax
	addq	$1, %rbx
	subq	%rdx, %rax
	sarq	$4, %rax
	cmpq	%rax, %rbx
	jnb	.L478
.L477:
	testb	%cl, %cl
	jne	.L498
.L481:
	movq	%rbx, %rax
	salq	$4, %rax
	movq	(%rdx,%rax), %r14
.L468:
	cmpb	$0, 72(%r12)
	je	.L475
	movzbl	28(%r12), %ecx
	jmp	.L472
.L496:
	movq	24(%r13), %rdx
	.p2align 4,,10
	.p2align 3
.L463:
	movq	%rbx, %r15
	salq	$4, %r15
.L466:
	cmpb	$0, 72(%r12)
	movq	(%rdx,%r15), %r14
	je	.L480
	movzbl	28(%r12), %eax
.L471:
	movl	%eax, %ecx
	testb	%al, %al
	jne	.L473
.L495:
	movq	24(%r13), %rdx
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L480:
	movl	8(%r12), %r15d
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L499
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
.L470:
	movzbl	28(%r12), %ecx
	movl	%ecx, %eax
	cmpl	8(%r12), %r15d
	je	.L471
	movq	24(%r13), %rdx
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L475:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L500
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movzbl	28(%r12), %ecx
	movq	24(%r13), %rdx
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L478:
	testb	%cl, %cl
	je	.L459
	cmpb	$0, 29(%r12)
	jne	.L459
	addq	$8, %rsp
	movq	%r12, %rdi
	leaq	.LC16(%rip), %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	.p2align 4,,10
	.p2align 3
.L459:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L499:
	.cfi_restore_state
	movb	$1, 72(%r12)
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L500:
	movb	$1, 72(%r12)
	movzbl	28(%r12), %ecx
	movq	24(%r13), %rdx
	jmp	.L472
.L465:
	movq	24(%r13), %rdx
	jmp	.L481
.L497:
	movq	24(%r13), %rdx
	jmp	.L466
	.cfi_endproc
.LFE19297:
	.size	_ZN2v88internal11CallPrinter18VisitNaryOperationEPNS0_13NaryOperationE, .-_ZN2v88internal11CallPrinter18VisitNaryOperationEPNS0_13NaryOperationE
	.section	.text._ZN2v88internal11CallPrinter12VisitCallNewEPNS0_7CallNewE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter12VisitCallNewEPNS0_7CallNewE
	.type	_ZN2v88internal11CallPrinter12VisitCallNewEPNS0_7CallNewE, @function
_ZN2v88internal11CallPrinter12VisitCallNewEPNS0_7CallNewE:
.LFB19292:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	(%rsi), %eax
	cmpl	%eax, 24(%rdi)
	je	.L517
.L502:
	movzbl	31(%rbx), %edx
	movq	8(%r15), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal11CallPrinter4FindEPNS0_7AstNodeEb
	movzbl	28(%rbx), %r13d
	testb	%r13b, %r13b
	jne	.L501
	movl	28(%r15), %edx
	testl	%edx, %edx
	jle	.L501
.L508:
	xorl	%r12d, %r12d
	xorl	%eax, %eax
	leaq	.LC14(%rip), %r14
	jmp	.L515
	.p2align 4,,10
	.p2align 3
.L518:
	cmpb	$0, 29(%rbx)
	jne	.L512
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
.L512:
	addq	$1, %r12
	cmpl	%r12d, 28(%r15)
	jle	.L514
.L520:
	movzbl	28(%rbx), %eax
.L515:
	testb	%al, %al
	jne	.L518
	cmpb	$0, 72(%rbx)
	jne	.L512
	movq	16(%r15), %rax
	movq	(%rax,%r12,8), %rsi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%rbx), %rax
	movq	-56(%rbp), %rsi
	jb	.L519
	movq	%rbx, %rdi
	addq	$1, %r12
	call	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	cmpl	%r12d, 28(%r15)
	jg	.L520
	.p2align 4,,10
	.p2align 3
.L514:
	testb	%r13b, %r13b
	je	.L501
.L510:
	movl	$256, %ecx
	movw	%cx, 28(%rbx)
.L501:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L517:
	.cfi_restore_state
	cmpb	$0, 28(%rdi)
	movb	$1, 33(%rdi)
	jne	.L502
	cmpb	$0, 30(%rdi)
	jne	.L503
	movq	8(%rsi), %rax
	movl	4(%rax), %eax
	andl	$63, %eax
	cmpb	$54, %al
	je	.L504
.L503:
	movb	$1, 28(%rbx)
	movq	8(%r15), %rsi
	movl	$1, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal11CallPrinter4FindEPNS0_7AstNodeEb
	cmpb	$0, 28(%rbx)
	jne	.L510
	movl	28(%r15), %eax
	testl	%eax, %eax
	jle	.L510
	movl	$1, %r13d
	jmp	.L508
	.p2align 4,,10
	.p2align 3
.L519:
	movb	$1, 72(%rbx)
	jmp	.L512
	.p2align 4,,10
	.p2align 3
.L504:
	movb	$1, 29(%rdi)
	jmp	.L501
	.cfi_endproc
.LFE19292:
	.size	_ZN2v88internal11CallPrinter12VisitCallNewEPNS0_7CallNewE, .-_ZN2v88internal11CallPrinter12VisitCallNewEPNS0_7CallNewE
	.section	.rodata._ZN2v88internal11CallPrinter9VisitCallEPNS0_4CallE.str1.1,"aMS",@progbits,1
.LC24:
	.string	"(...)"
	.section	.text._ZN2v88internal11CallPrinter9VisitCallEPNS0_4CallE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter9VisitCallEPNS0_4CallE
	.type	_ZN2v88internal11CallPrinter9VisitCallEPNS0_4CallE, @function
_ZN2v88internal11CallPrinter9VisitCallEPNS0_4CallE:
.LFB19291:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	(%rsi), %eax
	cmpl	%eax, 24(%rdi)
	jne	.L523
	cmpb	$0, 28(%rdi)
	movb	$1, 33(%rdi)
	jne	.L523
	cmpb	$0, 30(%rdi)
	jne	.L524
	movq	8(%rsi), %rax
	movl	4(%rax), %eax
	andl	$63, %eax
	cmpb	$54, %al
	je	.L525
.L524:
	movb	$1, 28(%rbx)
	movq	8(%r15), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal11CallPrinter4FindEPNS0_7AstNodeEb.constprop.0
	cmpb	$0, 28(%rbx)
	jne	.L534
	movl	28(%r15), %eax
	testl	%eax, %eax
	jle	.L534
	movb	$1, -49(%rbp)
	jmp	.L532
	.p2align 4,,10
	.p2align 3
.L523:
	movq	8(%r15), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal11CallPrinter4FindEPNS0_7AstNodeEb.constprop.0
	cmpb	$0, 31(%rbx)
	movzbl	28(%rbx), %eax
	jne	.L529
	testb	%al, %al
	je	.L530
	cmpb	$0, 29(%rbx)
	jne	.L521
	leaq	.LC24(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	movzbl	28(%rbx), %eax
.L529:
	testb	%al, %al
	jne	.L521
.L530:
	movl	28(%r15), %edx
	movb	$0, -49(%rbp)
	testl	%edx, %edx
	jle	.L521
.L532:
	xorl	%r12d, %r12d
	xorl	%eax, %eax
	leaq	.LC14(%rip), %r13
	jmp	.L539
	.p2align 4,,10
	.p2align 3
.L545:
	cmpb	$0, 29(%rbx)
	jne	.L536
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
.L536:
	addq	$1, %r12
	cmpl	%r12d, 28(%r15)
	jle	.L538
.L547:
	movzbl	28(%rbx), %eax
.L539:
	testb	%al, %al
	jne	.L545
	cmpb	$0, 72(%rbx)
	jne	.L536
	movq	16(%r15), %rax
	movq	(%rax,%r12,8), %r14
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%rbx), %rax
	jb	.L546
	movq	%r14, %rsi
	movq	%rbx, %rdi
	addq	$1, %r12
	call	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	cmpl	%r12d, 28(%r15)
	jg	.L547
	.p2align 4,,10
	.p2align 3
.L538:
	cmpb	$0, -49(%rbp)
	je	.L521
.L534:
	movl	$256, %ecx
	movw	%cx, 28(%rbx)
.L521:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L546:
	.cfi_restore_state
	movb	$1, 72(%rbx)
	jmp	.L536
	.p2align 4,,10
	.p2align 3
.L525:
	movb	$1, 29(%rdi)
	jmp	.L521
	.cfi_endproc
.LFE19291:
	.size	_ZN2v88internal11CallPrinter9VisitCallEPNS0_4CallE, .-_ZN2v88internal11CallPrinter9VisitCallEPNS0_4CallE
	.section	.rodata._ZN2v88internal11CallPrinter5PrintEPNS0_15FunctionLiteralEi.str1.1,"aMS",@progbits,1
.LC25:
	.string	"(location_) != nullptr"
.LC26:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal11CallPrinter5PrintEPNS0_15FunctionLiteralEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter5PrintEPNS0_15FunctionLiteralEi
	.type	_ZN2v88internal11CallPrinter5PrintEPNS0_15FunctionLiteralEi, @function
_ZN2v88internal11CallPrinter5PrintEPNS0_15FunctionLiteralEi:
.LFB19247:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 28(%rdi)
	movl	$0, 8(%rdi)
	movl	%edx, 24(%rdi)
	je	.L549
	cmpb	$0, 29(%rdi)
	jne	.L550
	leaq	.LC14(%rip), %rsi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
.L550:
	movq	16(%rbx), %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6FinishEv@PLT
	testq	%rax, %rax
	je	.L554
.L552:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L549:
	.cfi_restore_state
	cmpb	$0, 72(%rdi)
	movq	%rsi, -24(%rbp)
	jne	.L550
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%rbx), %rax
	movq	-24(%rbp), %rsi
	jb	.L555
	movq	%rbx, %rdi
	call	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movq	16(%rbx), %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6FinishEv@PLT
	testq	%rax, %rax
	jne	.L552
	.p2align 4,,10
	.p2align 3
.L554:
	leaq	.LC25(%rip), %rsi
	leaq	.LC26(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L555:
	movb	$1, 72(%rbx)
	jmp	.L550
	.cfi_endproc
.LFE19247:
	.size	_ZN2v88internal11CallPrinter5PrintEPNS0_15FunctionLiteralEi, .-_ZN2v88internal11CallPrinter5PrintEPNS0_15FunctionLiteralEi
	.section	.text._ZN2v88internal11CallPrinter25VisitImportCallExpressionEPNS0_20ImportCallExpressionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter25VisitImportCallExpressionEPNS0_20ImportCallExpressionE
	.type	_ZN2v88internal11CallPrinter25VisitImportCallExpressionEPNS0_20ImportCallExpressionE, @function
_ZN2v88internal11CallPrinter25VisitImportCallExpressionEPNS0_20ImportCallExpressionE:
.LFB19304:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	cmpb	$0, 28(%rdi)
	je	.L557
	cmpb	$0, 29(%rdi)
	jne	.L558
	leaq	.LC15(%rip), %rsi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	cmpb	$0, 28(%r12)
	movq	8(%rbx), %r13
	je	.L559
	cmpb	$0, 72(%r12)
	je	.L576
.L560:
	cmpb	$0, 29(%r12)
	jne	.L556
	leaq	.LC14(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	movzbl	28(%r12), %eax
	jmp	.L563
	.p2align 4,,10
	.p2align 3
.L558:
	cmpb	$0, 72(%r12)
	movq	8(%rsi), %r13
	jne	.L560
.L576:
	movl	8(%r12), %ebx
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L577
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
.L562:
	movzbl	28(%r12), %eax
	cmpl	8(%r12), %ebx
	je	.L578
.L563:
	testb	%al, %al
	je	.L556
	cmpb	$0, 29(%r12)
	jne	.L556
	addq	$8, %rsp
	movq	%r12, %rdi
	leaq	.LC16(%rip), %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
.L578:
	.cfi_restore_state
	testb	%al, %al
	jne	.L560
.L556:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L557:
	.cfi_restore_state
	movq	8(%rsi), %r13
.L559:
	cmpb	$0, 72(%r12)
	jne	.L556
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L579
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movzbl	28(%r12), %eax
	jmp	.L563
	.p2align 4,,10
	.p2align 3
.L577:
	movb	$1, 72(%r12)
	jmp	.L562
	.p2align 4,,10
	.p2align 3
.L579:
	movb	$1, 72(%r12)
	movzbl	28(%r12), %eax
	jmp	.L563
	.cfi_endproc
.LFE19304:
	.size	_ZN2v88internal11CallPrinter25VisitImportCallExpressionEPNS0_20ImportCallExpressionE, .-_ZN2v88internal11CallPrinter25VisitImportCallExpressionEPNS0_20ImportCallExpressionE
	.section	.text._ZN2v88internal11CallPrinter11VisitSpreadEPNS0_6SpreadE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter11VisitSpreadEPNS0_6SpreadE
	.type	_ZN2v88internal11CallPrinter11VisitSpreadEPNS0_6SpreadE, @function
_ZN2v88internal11CallPrinter11VisitSpreadEPNS0_6SpreadE:
.LFB19299:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	cmpb	$0, 28(%rdi)
	je	.L581
	cmpb	$0, 29(%rdi)
	jne	.L582
	leaq	.LC17(%rip), %rsi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	cmpb	$0, 28(%r12)
	movq	16(%rbx), %r13
	je	.L583
	cmpb	$0, 72(%r12)
	je	.L600
.L584:
	cmpb	$0, 29(%r12)
	jne	.L580
	leaq	.LC14(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	movzbl	28(%r12), %eax
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L582:
	cmpb	$0, 72(%r12)
	movq	16(%rsi), %r13
	jne	.L584
.L600:
	movl	8(%r12), %ebx
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L601
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
.L586:
	movzbl	28(%r12), %eax
	cmpl	8(%r12), %ebx
	je	.L602
.L587:
	testb	%al, %al
	je	.L580
	cmpb	$0, 29(%r12)
	jne	.L580
	addq	$8, %rsp
	movq	%r12, %rdi
	leaq	.LC16(%rip), %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
.L602:
	.cfi_restore_state
	testb	%al, %al
	jne	.L584
.L580:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L581:
	.cfi_restore_state
	movq	16(%rsi), %r13
.L583:
	cmpb	$0, 72(%r12)
	jne	.L580
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L603
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movzbl	28(%r12), %eax
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L601:
	movb	$1, 72(%r12)
	jmp	.L586
	.p2align 4,,10
	.p2align 3
.L603:
	movb	$1, 72(%r12)
	movzbl	28(%r12), %eax
	jmp	.L587
	.cfi_endproc
.LFE19299:
	.size	_ZN2v88internal11CallPrinter11VisitSpreadEPNS0_6SpreadE, .-_ZN2v88internal11CallPrinter11VisitSpreadEPNS0_6SpreadE
	.section	.text._ZN2v88internal11CallPrinter10VisitYieldEPNS0_5YieldE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter10VisitYieldEPNS0_5YieldE
	.type	_ZN2v88internal11CallPrinter10VisitYieldEPNS0_5YieldE, @function
_ZN2v88internal11CallPrinter10VisitYieldEPNS0_5YieldE:
.LFB19284:
	.cfi_startproc
	endbr64
	cmpb	$0, 28(%rdi)
	je	.L605
	cmpb	$0, 29(%rdi)
	jne	.L609
	leaq	.LC14(%rip), %rsi
	jmp	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	.p2align 4,,10
	.p2align 3
.L609:
	ret
	.p2align 4,,10
	.p2align 3
.L605:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	subq	$24, %rsp
	.cfi_offset 13, -24
	cmpb	$0, 72(%rdi)
	movq	%rdi, -24(%rbp)
	je	.L611
.L604:
	addq	$24, %rsp
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L611:
	.cfi_restore_state
	movq	8(%rsi), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	movq	-24(%rbp), %rdi
	cmpq	64(%rdi), %rax
	jb	.L612
	addq	$24, %rsp
	movq	%r13, %rsi
	popq	%r13
	.cfi_remember_state
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	.p2align 4,,10
	.p2align 3
.L612:
	.cfi_restore_state
	movb	$1, 72(%rdi)
	jmp	.L604
	.cfi_endproc
.LFE19284:
	.size	_ZN2v88internal11CallPrinter10VisitYieldEPNS0_5YieldE, .-_ZN2v88internal11CallPrinter10VisitYieldEPNS0_5YieldE
	.section	.text._ZN2v88internal11CallPrinter20VisitReturnStatementEPNS0_15ReturnStatementE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter20VisitReturnStatementEPNS0_15ReturnStatementE
	.type	_ZN2v88internal11CallPrinter20VisitReturnStatementEPNS0_15ReturnStatementE, @function
_ZN2v88internal11CallPrinter20VisitReturnStatementEPNS0_15ReturnStatementE:
.LFB19260:
	.cfi_startproc
	endbr64
	cmpb	$0, 28(%rdi)
	je	.L614
	cmpb	$0, 29(%rdi)
	jne	.L618
	leaq	.LC14(%rip), %rsi
	jmp	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	.p2align 4,,10
	.p2align 3
.L618:
	ret
	.p2align 4,,10
	.p2align 3
.L614:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	subq	$24, %rsp
	.cfi_offset 13, -24
	cmpb	$0, 72(%rdi)
	movq	%rdi, -24(%rbp)
	je	.L620
.L613:
	addq	$24, %rsp
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L620:
	.cfi_restore_state
	movq	8(%rsi), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	movq	-24(%rbp), %rdi
	cmpq	64(%rdi), %rax
	jb	.L621
	addq	$24, %rsp
	movq	%r13, %rsi
	popq	%r13
	.cfi_remember_state
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	.p2align 4,,10
	.p2align 3
.L621:
	.cfi_restore_state
	movb	$1, 72(%rdi)
	jmp	.L613
	.cfi_endproc
.LFE19260:
	.size	_ZN2v88internal11CallPrinter20VisitReturnStatementEPNS0_15ReturnStatementE, .-_ZN2v88internal11CallPrinter20VisitReturnStatementEPNS0_15ReturnStatementE
	.section	.text._ZN2v88internal11CallPrinter17VisitDoExpressionEPNS0_12DoExpressionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter17VisitDoExpressionEPNS0_12DoExpressionE
	.type	_ZN2v88internal11CallPrinter17VisitDoExpressionEPNS0_12DoExpressionE, @function
_ZN2v88internal11CallPrinter17VisitDoExpressionEPNS0_12DoExpressionE:
.LFB19275:
	.cfi_startproc
	endbr64
	cmpb	$0, 28(%rdi)
	je	.L623
	cmpb	$0, 29(%rdi)
	jne	.L627
	leaq	.LC14(%rip), %rsi
	jmp	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	.p2align 4,,10
	.p2align 3
.L627:
	ret
	.p2align 4,,10
	.p2align 3
.L623:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	subq	$24, %rsp
	.cfi_offset 13, -24
	cmpb	$0, 72(%rdi)
	movq	%rdi, -24(%rbp)
	je	.L629
.L622:
	addq	$24, %rsp
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L629:
	.cfi_restore_state
	movq	8(%rsi), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	movq	-24(%rbp), %rdi
	cmpq	64(%rdi), %rax
	jb	.L630
	addq	$24, %rsp
	movq	%r13, %rsi
	popq	%r13
	.cfi_remember_state
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	.p2align 4,,10
	.p2align 3
.L630:
	.cfi_restore_state
	movb	$1, 72(%rdi)
	jmp	.L622
	.cfi_endproc
.LFE19275:
	.size	_ZN2v88internal11CallPrinter17VisitDoExpressionEPNS0_12DoExpressionE, .-_ZN2v88internal11CallPrinter17VisitDoExpressionEPNS0_12DoExpressionE
	.section	.text._ZN2v88internal11CallPrinter33VisitSloppyBlockFunctionStatementEPNS0_28SloppyBlockFunctionStatementE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter33VisitSloppyBlockFunctionStatementEPNS0_28SloppyBlockFunctionStatementE
	.type	_ZN2v88internal11CallPrinter33VisitSloppyBlockFunctionStatementEPNS0_28SloppyBlockFunctionStatementE, @function
_ZN2v88internal11CallPrinter33VisitSloppyBlockFunctionStatementEPNS0_28SloppyBlockFunctionStatementE:
.LFB19256:
	.cfi_startproc
	endbr64
	cmpb	$0, 28(%rdi)
	je	.L632
	cmpb	$0, 29(%rdi)
	jne	.L636
	leaq	.LC14(%rip), %rsi
	jmp	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	.p2align 4,,10
	.p2align 3
.L636:
	ret
	.p2align 4,,10
	.p2align 3
.L632:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	subq	$24, %rsp
	.cfi_offset 13, -24
	cmpb	$0, 72(%rdi)
	movq	%rdi, -24(%rbp)
	je	.L638
.L631:
	addq	$24, %rsp
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L638:
	.cfi_restore_state
	movq	16(%rsi), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	movq	-24(%rbp), %rdi
	cmpq	64(%rdi), %rax
	jb	.L639
	addq	$24, %rsp
	movq	%r13, %rsi
	popq	%r13
	.cfi_remember_state
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	.p2align 4,,10
	.p2align 3
.L639:
	.cfi_restore_state
	movb	$1, 72(%rdi)
	jmp	.L631
	.cfi_endproc
.LFE19256:
	.size	_ZN2v88internal11CallPrinter33VisitSloppyBlockFunctionStatementEPNS0_28SloppyBlockFunctionStatementE, .-_ZN2v88internal11CallPrinter33VisitSloppyBlockFunctionStatementEPNS0_28SloppyBlockFunctionStatementE
	.section	.text._ZN2v88internal11CallPrinter24VisitExpressionStatementEPNS0_19ExpressionStatementE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter24VisitExpressionStatementEPNS0_19ExpressionStatementE
	.type	_ZN2v88internal11CallPrinter24VisitExpressionStatementEPNS0_19ExpressionStatementE, @function
_ZN2v88internal11CallPrinter24VisitExpressionStatementEPNS0_19ExpressionStatementE:
.LFB19254:
	.cfi_startproc
	endbr64
	cmpb	$0, 28(%rdi)
	je	.L641
	cmpb	$0, 29(%rdi)
	jne	.L645
	leaq	.LC14(%rip), %rsi
	jmp	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	.p2align 4,,10
	.p2align 3
.L645:
	ret
	.p2align 4,,10
	.p2align 3
.L641:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	subq	$24, %rsp
	.cfi_offset 13, -24
	cmpb	$0, 72(%rdi)
	movq	%rdi, -24(%rbp)
	je	.L647
.L640:
	addq	$24, %rsp
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L647:
	.cfi_restore_state
	movq	8(%rsi), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	movq	-24(%rbp), %rdi
	cmpq	64(%rdi), %rax
	jb	.L648
	addq	$24, %rsp
	movq	%r13, %rsi
	popq	%r13
	.cfi_remember_state
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	.p2align 4,,10
	.p2align 3
.L648:
	.cfi_restore_state
	movb	$1, 72(%rdi)
	jmp	.L640
	.cfi_endproc
.LFE19254:
	.size	_ZN2v88internal11CallPrinter24VisitExpressionStatementEPNS0_19ExpressionStatementE, .-_ZN2v88internal11CallPrinter24VisitExpressionStatementEPNS0_19ExpressionStatementE
	.section	.text._ZN2v88internal11CallPrinter10VisitThrowEPNS0_5ThrowE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter10VisitThrowEPNS0_5ThrowE
	.type	_ZN2v88internal11CallPrinter10VisitThrowEPNS0_5ThrowE, @function
_ZN2v88internal11CallPrinter10VisitThrowEPNS0_5ThrowE:
.LFB19287:
	.cfi_startproc
	endbr64
	cmpb	$0, 28(%rdi)
	je	.L650
	cmpb	$0, 29(%rdi)
	jne	.L654
	leaq	.LC14(%rip), %rsi
	jmp	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	.p2align 4,,10
	.p2align 3
.L654:
	ret
	.p2align 4,,10
	.p2align 3
.L650:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	subq	$24, %rsp
	.cfi_offset 13, -24
	cmpb	$0, 72(%rdi)
	movq	%rdi, -24(%rbp)
	je	.L656
.L649:
	addq	$24, %rsp
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L656:
	.cfi_restore_state
	movq	8(%rsi), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	movq	-24(%rbp), %rdi
	cmpq	64(%rdi), %rax
	jb	.L657
	addq	$24, %rsp
	movq	%r13, %rsi
	popq	%r13
	.cfi_remember_state
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	.p2align 4,,10
	.p2align 3
.L657:
	.cfi_restore_state
	movb	$1, 72(%rdi)
	jmp	.L649
	.cfi_endproc
.LFE19287:
	.size	_ZN2v88internal11CallPrinter10VisitThrowEPNS0_5ThrowE, .-_ZN2v88internal11CallPrinter10VisitThrowEPNS0_5ThrowE
	.section	.text._ZN2v88internal11CallPrinter10VisitAwaitEPNS0_5AwaitE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter10VisitAwaitEPNS0_5AwaitE
	.type	_ZN2v88internal11CallPrinter10VisitAwaitEPNS0_5AwaitE, @function
_ZN2v88internal11CallPrinter10VisitAwaitEPNS0_5AwaitE:
.LFB19286:
	.cfi_startproc
	endbr64
	cmpb	$0, 28(%rdi)
	je	.L659
	cmpb	$0, 29(%rdi)
	jne	.L663
	leaq	.LC14(%rip), %rsi
	jmp	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	.p2align 4,,10
	.p2align 3
.L663:
	ret
	.p2align 4,,10
	.p2align 3
.L659:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	subq	$24, %rsp
	.cfi_offset 13, -24
	cmpb	$0, 72(%rdi)
	movq	%rdi, -24(%rbp)
	je	.L665
.L658:
	addq	$24, %rsp
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L665:
	.cfi_restore_state
	movq	8(%rsi), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	movq	-24(%rbp), %rdi
	cmpq	64(%rdi), %rax
	jb	.L666
	addq	$24, %rsp
	movq	%r13, %rsi
	popq	%r13
	.cfi_remember_state
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	.p2align 4,,10
	.p2align 3
.L666:
	.cfi_restore_state
	movb	$1, 72(%rdi)
	jmp	.L658
	.cfi_endproc
.LFE19286:
	.size	_ZN2v88internal11CallPrinter10VisitAwaitEPNS0_5AwaitE, .-_ZN2v88internal11CallPrinter10VisitAwaitEPNS0_5AwaitE
	.section	.text._ZN2v88internal11CallPrinter18VisitOptionalChainEPNS0_13OptionalChainE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter18VisitOptionalChainEPNS0_13OptionalChainE
	.type	_ZN2v88internal11CallPrinter18VisitOptionalChainEPNS0_13OptionalChainE, @function
_ZN2v88internal11CallPrinter18VisitOptionalChainEPNS0_13OptionalChainE:
.LFB19288:
	.cfi_startproc
	endbr64
	cmpb	$0, 28(%rdi)
	je	.L668
	cmpb	$0, 29(%rdi)
	jne	.L672
	leaq	.LC14(%rip), %rsi
	jmp	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	.p2align 4,,10
	.p2align 3
.L672:
	ret
	.p2align 4,,10
	.p2align 3
.L668:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	subq	$24, %rsp
	.cfi_offset 13, -24
	cmpb	$0, 72(%rdi)
	movq	%rdi, -24(%rbp)
	je	.L674
.L667:
	addq	$24, %rsp
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L674:
	.cfi_restore_state
	movq	8(%rsi), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	movq	-24(%rbp), %rdi
	cmpq	64(%rdi), %rax
	jb	.L675
	addq	$24, %rsp
	movq	%r13, %rsi
	popq	%r13
	.cfi_remember_state
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	.p2align 4,,10
	.p2align 3
.L675:
	.cfi_restore_state
	movb	$1, 72(%rdi)
	jmp	.L667
	.cfi_endproc
.LFE19288:
	.size	_ZN2v88internal11CallPrinter18VisitOptionalChainEPNS0_13OptionalChainE, .-_ZN2v88internal11CallPrinter18VisitOptionalChainEPNS0_13OptionalChainE
	.section	.text._ZN2v88internal11CallPrinter19VisitWhileStatementEPNS0_14WhileStatementE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter19VisitWhileStatementEPNS0_14WhileStatementE
	.type	_ZN2v88internal11CallPrinter19VisitWhileStatementEPNS0_14WhileStatementE, @function
_ZN2v88internal11CallPrinter19VisitWhileStatementEPNS0_14WhileStatementE:
.LFB19264:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	cmpb	$0, 28(%rdi)
	je	.L677
	cmpb	$0, 29(%rdi)
	jne	.L676
	leaq	.LC14(%rip), %rsi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	cmpb	$0, 28(%r12)
	je	.L684
.L687:
	movzbl	29(%r12), %eax
	testb	%al, %al
	jne	.L676
	addq	$8, %rsp
	movq	%r12, %rdi
	leaq	.LC14(%rip), %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	.p2align 4,,10
	.p2align 3
.L689:
	.cfi_restore_state
	movb	$1, 72(%r12)
.L676:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L677:
	.cfi_restore_state
	cmpb	$0, 72(%rdi)
	jne	.L676
	movq	32(%rsi), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L688
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	cmpb	$0, 28(%r12)
	jne	.L687
.L684:
	cmpb	$0, 72(%r12)
	movq	24(%rbx), %r13
	jne	.L676
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L689
	addq	$8, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	.p2align 4,,10
	.p2align 3
.L688:
	.cfi_restore_state
	cmpb	$0, 28(%r12)
	movb	$1, 72(%r12)
	jne	.L687
	jmp	.L676
	.cfi_endproc
.LFE19264:
	.size	_ZN2v88internal11CallPrinter19VisitWhileStatementEPNS0_14WhileStatementE, .-_ZN2v88internal11CallPrinter19VisitWhileStatementEPNS0_14WhileStatementE
	.section	.text._ZN2v88internal11CallPrinter21VisitDoWhileStatementEPNS0_16DoWhileStatementE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter21VisitDoWhileStatementEPNS0_16DoWhileStatementE
	.type	_ZN2v88internal11CallPrinter21VisitDoWhileStatementEPNS0_16DoWhileStatementE, @function
_ZN2v88internal11CallPrinter21VisitDoWhileStatementEPNS0_16DoWhileStatementE:
.LFB19263:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	cmpb	$0, 28(%rdi)
	je	.L691
	cmpb	$0, 29(%rdi)
	jne	.L690
	leaq	.LC14(%rip), %rsi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	cmpb	$0, 28(%r12)
	je	.L698
.L701:
	movzbl	29(%r12), %eax
	testb	%al, %al
	jne	.L690
	addq	$8, %rsp
	movq	%r12, %rdi
	leaq	.LC14(%rip), %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	.p2align 4,,10
	.p2align 3
.L703:
	.cfi_restore_state
	movb	$1, 72(%r12)
.L690:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L691:
	.cfi_restore_state
	cmpb	$0, 72(%rdi)
	jne	.L690
	movq	24(%rsi), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L702
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	cmpb	$0, 28(%r12)
	jne	.L701
.L698:
	cmpb	$0, 72(%r12)
	movq	32(%rbx), %r13
	jne	.L690
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L703
	addq	$8, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	.p2align 4,,10
	.p2align 3
.L702:
	.cfi_restore_state
	cmpb	$0, 28(%r12)
	movb	$1, 72(%r12)
	jne	.L701
	jmp	.L690
	.cfi_endproc
.LFE19263:
	.size	_ZN2v88internal11CallPrinter21VisitDoWhileStatementEPNS0_16DoWhileStatementE, .-_ZN2v88internal11CallPrinter21VisitDoWhileStatementEPNS0_16DoWhileStatementE
	.section	.text._ZN2v88internal11CallPrinter22VisitTryCatchStatementEPNS0_17TryCatchStatementE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter22VisitTryCatchStatementEPNS0_17TryCatchStatementE
	.type	_ZN2v88internal11CallPrinter22VisitTryCatchStatementEPNS0_17TryCatchStatementE, @function
_ZN2v88internal11CallPrinter22VisitTryCatchStatementEPNS0_17TryCatchStatementE:
.LFB19268:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	cmpb	$0, 28(%rdi)
	je	.L705
	cmpb	$0, 29(%rdi)
	jne	.L704
	leaq	.LC14(%rip), %rsi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	cmpb	$0, 28(%r12)
	je	.L712
.L715:
	movzbl	29(%r12), %eax
	testb	%al, %al
	jne	.L704
	addq	$8, %rsp
	movq	%r12, %rdi
	leaq	.LC14(%rip), %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	.p2align 4,,10
	.p2align 3
.L717:
	.cfi_restore_state
	movb	$1, 72(%r12)
.L704:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L705:
	.cfi_restore_state
	cmpb	$0, 72(%rdi)
	jne	.L704
	movq	8(%rsi), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L716
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	cmpb	$0, 28(%r12)
	jne	.L715
.L712:
	cmpb	$0, 72(%r12)
	movq	24(%rbx), %r13
	jne	.L704
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L717
	addq	$8, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	.p2align 4,,10
	.p2align 3
.L716:
	.cfi_restore_state
	cmpb	$0, 28(%r12)
	movb	$1, 72(%r12)
	jne	.L715
	jmp	.L704
	.cfi_endproc
.LFE19268:
	.size	_ZN2v88internal11CallPrinter22VisitTryCatchStatementEPNS0_17TryCatchStatementE, .-_ZN2v88internal11CallPrinter22VisitTryCatchStatementEPNS0_17TryCatchStatementE
	.section	.text._ZN2v88internal11CallPrinter18VisitWithStatementEPNS0_13WithStatementE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter18VisitWithStatementEPNS0_13WithStatementE
	.type	_ZN2v88internal11CallPrinter18VisitWithStatementEPNS0_13WithStatementE, @function
_ZN2v88internal11CallPrinter18VisitWithStatementEPNS0_13WithStatementE:
.LFB19261:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	cmpb	$0, 28(%rdi)
	je	.L719
	cmpb	$0, 29(%rdi)
	jne	.L718
	leaq	.LC14(%rip), %rsi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	cmpb	$0, 28(%r12)
	je	.L726
.L729:
	movzbl	29(%r12), %eax
	testb	%al, %al
	jne	.L718
	addq	$8, %rsp
	movq	%r12, %rdi
	leaq	.LC14(%rip), %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	.p2align 4,,10
	.p2align 3
.L731:
	.cfi_restore_state
	movb	$1, 72(%r12)
.L718:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L719:
	.cfi_restore_state
	cmpb	$0, 72(%rdi)
	jne	.L718
	movq	16(%rsi), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L730
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	cmpb	$0, 28(%r12)
	jne	.L729
.L726:
	cmpb	$0, 72(%r12)
	movq	24(%rbx), %r13
	jne	.L718
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L731
	addq	$8, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	.p2align 4,,10
	.p2align 3
.L730:
	.cfi_restore_state
	cmpb	$0, 28(%r12)
	movb	$1, 72(%r12)
	jne	.L729
	jmp	.L718
	.cfi_endproc
.LFE19261:
	.size	_ZN2v88internal11CallPrinter18VisitWithStatementEPNS0_13WithStatementE, .-_ZN2v88internal11CallPrinter18VisitWithStatementEPNS0_13WithStatementE
	.section	.text._ZN2v88internal11CallPrinter24VisitTryFinallyStatementEPNS0_19TryFinallyStatementE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter24VisitTryFinallyStatementEPNS0_19TryFinallyStatementE
	.type	_ZN2v88internal11CallPrinter24VisitTryFinallyStatementEPNS0_19TryFinallyStatementE, @function
_ZN2v88internal11CallPrinter24VisitTryFinallyStatementEPNS0_19TryFinallyStatementE:
.LFB19269:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	cmpb	$0, 28(%rdi)
	je	.L733
	cmpb	$0, 29(%rdi)
	jne	.L732
	leaq	.LC14(%rip), %rsi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	cmpb	$0, 28(%r12)
	je	.L740
.L743:
	movzbl	29(%r12), %eax
	testb	%al, %al
	jne	.L732
	addq	$8, %rsp
	movq	%r12, %rdi
	leaq	.LC14(%rip), %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	.p2align 4,,10
	.p2align 3
.L745:
	.cfi_restore_state
	movb	$1, 72(%r12)
.L732:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L733:
	.cfi_restore_state
	cmpb	$0, 72(%rdi)
	jne	.L732
	movq	8(%rsi), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L744
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	cmpb	$0, 28(%r12)
	jne	.L743
.L740:
	cmpb	$0, 72(%r12)
	movq	16(%rbx), %r13
	jne	.L732
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L745
	addq	$8, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	.p2align 4,,10
	.p2align 3
.L744:
	.cfi_restore_state
	cmpb	$0, 28(%r12)
	movb	$1, 72(%r12)
	jne	.L743
	jmp	.L732
	.cfi_endproc
.LFE19269:
	.size	_ZN2v88internal11CallPrinter24VisitTryFinallyStatementEPNS0_19TryFinallyStatementE, .-_ZN2v88internal11CallPrinter24VisitTryFinallyStatementEPNS0_19TryFinallyStatementE
	.section	.text._ZN2v88internal11CallPrinter23VisitCompoundAssignmentEPNS0_18CompoundAssignmentE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter23VisitCompoundAssignmentEPNS0_18CompoundAssignmentE
	.type	_ZN2v88internal11CallPrinter23VisitCompoundAssignmentEPNS0_18CompoundAssignmentE, @function
_ZN2v88internal11CallPrinter23VisitCompoundAssignmentEPNS0_18CompoundAssignmentE:
.LFB19283:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	8(%rsi), %r13
	movzbl	28(%rdi), %ecx
	movzbl	4(%r13), %eax
	andl	$63, %eax
	cmpb	$22, %al
	jne	.L747
	movl	24(%rdi), %edi
	cmpl	%edi, 0(%r13)
	je	.L765
	movq	24(%r13), %rdx
	movslq	36(%r13), %rsi
	leaq	(%rdx,%rsi,8), %r9
	cmpq	%r9, %rdx
	jne	.L751
	jmp	.L747
	.p2align 4,,10
	.p2align 3
.L750:
	addq	$8, %rdx
	cmpq	%rdx, %r9
	je	.L747
.L751:
	movq	(%rdx), %rsi
	movq	8(%rsi), %r8
	cmpl	(%r8), %edi
	jne	.L750
	movq	%rsi, %xmm0
	movq	%rbx, %xmm1
	xorl	$1, %ecx
	movb	$1, 28(%r12)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 40(%r12)
	testb	%cl, %cl
	jne	.L766
	.p2align 4,,10
	.p2align 3
.L760:
	cmpb	$0, 29(%r12)
	jne	.L763
	leaq	.LC14(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	movq	8(%rbx), %rax
	movl	4(%rax), %eax
	andl	$63, %eax
.L754:
	movq	16(%rbx), %rsi
	cmpb	$23, %al
	jne	.L756
.L768:
	movl	24(%r12), %eax
	cmpl	%eax, (%rsi)
	je	.L757
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter4FindEPNS0_7AstNodeEb.constprop.0
	.p2align 4,,10
	.p2align 3
.L747:
	.cfi_restore_state
	testb	%cl, %cl
	jne	.L760
	cmpb	$0, 72(%r12)
	jne	.L754
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L767
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
.L763:
	movq	8(%rbx), %rax
	movq	16(%rbx), %rsi
	movl	4(%rax), %eax
	andl	$63, %eax
	cmpb	$23, %al
	je	.L768
.L756:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter4FindEPNS0_7AstNodeEb.constprop.1
	.p2align 4,,10
	.p2align 3
.L765:
	.cfi_restore_state
	xorl	$1, %ecx
	movb	$1, 28(%r12)
	movq	%rsi, 48(%r12)
	testb	%cl, %cl
	je	.L760
.L766:
	movq	16(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter4FindEPNS0_7AstNodeEb.constprop.0
.L764:
	movl	$256, %eax
	movw	%ax, 28(%r12)
.L746:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L757:
	.cfi_restore_state
	movzbl	28(%r12), %r13d
	movb	$1, 31(%r12)
	movq	%r12, %rdi
	movb	$1, 28(%r12)
	movq	16(%rbx), %rsi
	call	_ZN2v88internal11CallPrinter4FindEPNS0_7AstNodeEb.constprop.0
	testb	%r13b, %r13b
	je	.L764
	jmp	.L746
	.p2align 4,,10
	.p2align 3
.L767:
	movb	$1, 72(%r12)
	movq	8(%rbx), %rax
	movl	4(%rax), %eax
	andl	$63, %eax
	jmp	.L754
	.cfi_endproc
.LFE19283:
	.size	_ZN2v88internal11CallPrinter23VisitCompoundAssignmentEPNS0_18CompoundAssignmentE, .-_ZN2v88internal11CallPrinter23VisitCompoundAssignmentEPNS0_18CompoundAssignmentE
	.section	.text._ZN2v88internal11CallPrinter16VisitConditionalEPNS0_11ConditionalE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter16VisitConditionalEPNS0_11ConditionalE
	.type	_ZN2v88internal11CallPrinter16VisitConditionalEPNS0_11ConditionalE, @function
_ZN2v88internal11CallPrinter16VisitConditionalEPNS0_11ConditionalE:
.LFB19276:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	cmpb	$0, 28(%rdi)
	je	.L770
	cmpb	$0, 29(%rdi)
	jne	.L769
	leaq	.LC14(%rip), %rsi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
.L772:
	cmpb	$0, 28(%r12)
	je	.L775
.L787:
	cmpb	$0, 29(%r12)
	jne	.L769
	leaq	.LC14(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	cmpb	$0, 28(%r12)
	je	.L781
.L785:
	movzbl	29(%r12), %eax
	testb	%al, %al
	jne	.L769
	addq	$8, %rsp
	movq	%r12, %rdi
	leaq	.LC14(%rip), %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	.p2align 4,,10
	.p2align 3
.L789:
	.cfi_restore_state
	movb	$1, 72(%r12)
.L769:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L770:
	.cfi_restore_state
	cmpb	$0, 72(%rdi)
	jne	.L769
	movq	8(%rsi), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L786
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	cmpb	$0, 28(%r12)
	jne	.L787
	.p2align 4,,10
	.p2align 3
.L775:
	cmpb	$0, 72(%r12)
	movq	16(%rbx), %r13
	jne	.L769
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L788
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	cmpb	$0, 28(%r12)
	jne	.L785
.L781:
	cmpb	$0, 72(%r12)
	movq	24(%rbx), %r13
	jne	.L769
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L789
	addq	$8, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	.p2align 4,,10
	.p2align 3
.L786:
	.cfi_restore_state
	movb	$1, 72(%r12)
	jmp	.L772
	.p2align 4,,10
	.p2align 3
.L788:
	cmpb	$0, 28(%r12)
	movb	$1, 72(%r12)
	jne	.L785
	jmp	.L769
	.cfi_endproc
.LFE19276:
	.size	_ZN2v88internal11CallPrinter16VisitConditionalEPNS0_11ConditionalE, .-_ZN2v88internal11CallPrinter16VisitConditionalEPNS0_11ConditionalE
	.section	.text._ZN2v88internal11CallPrinter24VisitStoreInArrayLiteralEPNS0_19StoreInArrayLiteralE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter24VisitStoreInArrayLiteralEPNS0_19StoreInArrayLiteralE
	.type	_ZN2v88internal11CallPrinter24VisitStoreInArrayLiteralEPNS0_19StoreInArrayLiteralE, @function
_ZN2v88internal11CallPrinter24VisitStoreInArrayLiteralEPNS0_19StoreInArrayLiteralE:
.LFB19300:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	cmpb	$0, 28(%rdi)
	je	.L791
	cmpb	$0, 29(%rdi)
	jne	.L790
	leaq	.LC14(%rip), %rsi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
.L793:
	cmpb	$0, 28(%r12)
	je	.L796
.L808:
	cmpb	$0, 29(%r12)
	jne	.L790
	leaq	.LC14(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	cmpb	$0, 28(%r12)
	je	.L802
.L806:
	movzbl	29(%r12), %eax
	testb	%al, %al
	jne	.L790
	addq	$8, %rsp
	movq	%r12, %rdi
	leaq	.LC14(%rip), %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	.p2align 4,,10
	.p2align 3
.L810:
	.cfi_restore_state
	movb	$1, 72(%r12)
.L790:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L791:
	.cfi_restore_state
	cmpb	$0, 72(%rdi)
	jne	.L790
	movq	8(%rsi), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L807
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	cmpb	$0, 28(%r12)
	jne	.L808
	.p2align 4,,10
	.p2align 3
.L796:
	cmpb	$0, 72(%r12)
	movq	16(%rbx), %r13
	jne	.L790
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L809
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	cmpb	$0, 28(%r12)
	jne	.L806
.L802:
	cmpb	$0, 72(%r12)
	movq	24(%rbx), %r13
	jne	.L790
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L810
	addq	$8, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	.p2align 4,,10
	.p2align 3
.L807:
	.cfi_restore_state
	movb	$1, 72(%r12)
	jmp	.L793
	.p2align 4,,10
	.p2align 3
.L809:
	cmpb	$0, 28(%r12)
	movb	$1, 72(%r12)
	jne	.L806
	jmp	.L790
	.cfi_endproc
.LFE19300:
	.size	_ZN2v88internal11CallPrinter24VisitStoreInArrayLiteralEPNS0_19StoreInArrayLiteralE, .-_ZN2v88internal11CallPrinter24VisitStoreInArrayLiteralEPNS0_19StoreInArrayLiteralE
	.section	.text._ZN2v88internal11CallPrinter19VisitForInStatementEPNS0_14ForInStatementE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter19VisitForInStatementEPNS0_14ForInStatementE
	.type	_ZN2v88internal11CallPrinter19VisitForInStatementEPNS0_14ForInStatementE, @function
_ZN2v88internal11CallPrinter19VisitForInStatementEPNS0_14ForInStatementE:
.LFB19266:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	cmpb	$0, 28(%rdi)
	je	.L812
	cmpb	$0, 29(%rdi)
	jne	.L811
	leaq	.LC14(%rip), %rsi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
.L814:
	cmpb	$0, 28(%r12)
	je	.L817
.L829:
	cmpb	$0, 29(%r12)
	jne	.L811
	leaq	.LC14(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	cmpb	$0, 28(%r12)
	je	.L823
.L827:
	movzbl	29(%r12), %eax
	testb	%al, %al
	jne	.L811
	addq	$8, %rsp
	movq	%r12, %rdi
	leaq	.LC14(%rip), %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	.p2align 4,,10
	.p2align 3
.L831:
	.cfi_restore_state
	movb	$1, 72(%r12)
.L811:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L812:
	.cfi_restore_state
	cmpb	$0, 72(%rdi)
	jne	.L811
	movq	32(%rsi), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L828
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	cmpb	$0, 28(%r12)
	jne	.L829
	.p2align 4,,10
	.p2align 3
.L817:
	cmpb	$0, 72(%r12)
	movq	40(%rbx), %r13
	jne	.L811
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L830
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	cmpb	$0, 28(%r12)
	jne	.L827
.L823:
	cmpb	$0, 72(%r12)
	movq	24(%rbx), %r13
	jne	.L811
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L831
	addq	$8, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	.p2align 4,,10
	.p2align 3
.L828:
	.cfi_restore_state
	movb	$1, 72(%r12)
	jmp	.L814
	.p2align 4,,10
	.p2align 3
.L830:
	cmpb	$0, 28(%r12)
	movb	$1, 72(%r12)
	jne	.L827
	jmp	.L811
	.cfi_endproc
.LFE19266:
	.size	_ZN2v88internal11CallPrinter19VisitForInStatementEPNS0_14ForInStatementE, .-_ZN2v88internal11CallPrinter19VisitForInStatementEPNS0_14ForInStatementE
	.section	.text._ZN2v88internal11CallPrinter16VisitIfStatementEPNS0_11IfStatementE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter16VisitIfStatementEPNS0_11IfStatementE
	.type	_ZN2v88internal11CallPrinter16VisitIfStatementEPNS0_11IfStatementE, @function
_ZN2v88internal11CallPrinter16VisitIfStatementEPNS0_11IfStatementE:
.LFB19257:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	cmpb	$0, 28(%rdi)
	je	.L833
	cmpb	$0, 29(%rdi)
	jne	.L838
	leaq	.LC14(%rip), %rsi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	cmpb	$0, 28(%r12)
	je	.L840
.L844:
	movzbl	29(%r12), %eax
	testb	%al, %al
	jne	.L838
	leaq	.LC14(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
.L838:
	movq	24(%rbx), %rsi
	movzbl	4(%rsi), %eax
	andl	$63, %eax
	cmpb	$10, %al
	jne	.L845
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L845:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter4FindEPNS0_7AstNodeEb.constprop.1
	.p2align 4,,10
	.p2align 3
.L833:
	.cfi_restore_state
	cmpb	$0, 72(%rdi)
	jne	.L838
	movq	8(%rsi), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L846
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	cmpb	$0, 28(%r12)
	jne	.L844
.L840:
	cmpb	$0, 72(%r12)
	movq	16(%rbx), %r13
	jne	.L838
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L847
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	jmp	.L838
	.p2align 4,,10
	.p2align 3
.L846:
	cmpb	$0, 28(%r12)
	movb	$1, 72(%r12)
	jne	.L844
	jmp	.L838
	.p2align 4,,10
	.p2align 3
.L847:
	movb	$1, 72(%r12)
	jmp	.L838
	.cfi_endproc
.LFE19257:
	.size	_ZN2v88internal11CallPrinter16VisitIfStatementEPNS0_11IfStatementE, .-_ZN2v88internal11CallPrinter16VisitIfStatementEPNS0_11IfStatementE
	.section	.text._ZN2v88internal11CallPrinter19VisitUnaryOperationEPNS0_14UnaryOperationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter19VisitUnaryOperationEPNS0_14UnaryOperationE
	.type	_ZN2v88internal11CallPrinter19VisitUnaryOperationEPNS0_14UnaryOperationE, @function
_ZN2v88internal11CallPrinter19VisitUnaryOperationEPNS0_14UnaryOperationE:
.LFB19294:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	4(%rsi), %eax
	shrl	$7, %eax
	andl	$127, %eax
	cmpb	$0, 28(%rdi)
	leal	-48(%rax), %r14d
	je	.L877
	cmpb	$0, 29(%rdi)
	movzbl	%al, %ebx
	jne	.L876
	leaq	.LC22(%rip), %rsi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	movslq	%ebx, %rax
	leaq	_ZN2v88internal5Token7string_E(%rip), %rdx
	cmpb	$0, 28(%r12)
	movq	(%rdx,%rax,8), %rsi
	je	.L877
	cmpb	$0, 29(%r12)
	jne	.L876
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	movzbl	28(%r12), %eax
	cmpb	$2, %r14b
	ja	.L852
	testb	%al, %al
	je	.L877
	cmpb	$0, 29(%r12)
	jne	.L876
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	movzbl	28(%r12), %eax
.L852:
	movq	8(%r13), %r13
	testb	%al, %al
	je	.L855
	cmpb	$0, 72(%r12)
	jne	.L862
.L856:
	movl	8(%r12), %ebx
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L878
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
.L860:
	cmpl	8(%r12), %ebx
	je	.L862
.L861:
	cmpb	$0, 28(%r12)
	je	.L848
	cmpb	$0, 29(%r12)
	jne	.L848
	popq	%rbx
	movq	%r12, %rdi
	leaq	.LC16(%rip), %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	.p2align 4,,10
	.p2align 3
.L876:
	.cfi_restore_state
	cmpb	$0, 72(%r12)
	movq	8(%r13), %r13
	je	.L856
.L862:
	cmpb	$0, 28(%r12)
	je	.L848
	cmpb	$0, 29(%r12)
	jne	.L848
	leaq	.LC14(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	jmp	.L861
	.p2align 4,,10
	.p2align 3
.L877:
	movq	8(%r13), %r13
.L855:
	cmpb	$0, 72(%r12)
	je	.L879
.L848:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L879:
	.cfi_restore_state
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L880
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	jmp	.L861
	.p2align 4,,10
	.p2align 3
.L878:
	movb	$1, 72(%r12)
	jmp	.L860
	.p2align 4,,10
	.p2align 3
.L880:
	movb	$1, 72(%r12)
	jmp	.L861
	.cfi_endproc
.LFE19294:
	.size	_ZN2v88internal11CallPrinter19VisitUnaryOperationEPNS0_14UnaryOperationE, .-_ZN2v88internal11CallPrinter19VisitUnaryOperationEPNS0_14UnaryOperationE
	.section	.text._ZN2v88internal11CallPrinter14VisitYieldStarEPNS0_9YieldStarE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter14VisitYieldStarEPNS0_9YieldStarE
	.type	_ZN2v88internal11CallPrinter14VisitYieldStarEPNS0_9YieldStarE, @function
_ZN2v88internal11CallPrinter14VisitYieldStarEPNS0_9YieldStarE:
.LFB19285:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	cmpb	$0, 28(%rdi)
	jne	.L887
	movq	8(%rsi), %r13
	movq	%rsi, %rbx
	movl	0(%r13), %eax
	cmpl	%eax, 24(%rdi)
	je	.L893
.L883:
	cmpb	$0, 72(%r12)
	je	.L894
.L881:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L893:
	.cfi_restore_state
	movzbl	56(%rdi), %eax
	movb	$1, 28(%rdi)
	subl	$9, %eax
	cmpb	$4, %al
	ja	.L884
	movb	$1, 32(%rdi)
.L885:
	cmpb	$0, 29(%r12)
	jne	.L881
	leaq	.LC19(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	cmpb	$0, 28(%r12)
	je	.L895
	.p2align 4,,10
	.p2align 3
.L887:
	movzbl	29(%r12), %eax
	testb	%al, %al
	jne	.L881
	addq	$8, %rsp
	movq	%r12, %rdi
	leaq	.LC14(%rip), %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	.p2align 4,,10
	.p2align 3
.L894:
	.cfi_restore_state
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L896
	addq	$8, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	.p2align 4,,10
	.p2align 3
.L884:
	.cfi_restore_state
	movb	$1, 31(%rdi)
	jmp	.L885
	.p2align 4,,10
	.p2align 3
.L896:
	movb	$1, 72(%r12)
	jmp	.L881
.L895:
	movq	8(%rbx), %r13
	jmp	.L883
	.cfi_endproc
.LFE19285:
	.size	_ZN2v88internal11CallPrinter14VisitYieldStarEPNS0_9YieldStarE, .-_ZN2v88internal11CallPrinter14VisitYieldStarEPNS0_9YieldStarE
	.section	.text._ZN2v88internal11CallPrinter17VisitForStatementEPNS0_12ForStatementE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter17VisitForStatementEPNS0_12ForStatementE
	.type	_ZN2v88internal11CallPrinter17VisitForStatementEPNS0_12ForStatementE, @function
_ZN2v88internal11CallPrinter17VisitForStatementEPNS0_12ForStatementE:
.LFB19265:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	32(%rsi), %rsi
	testq	%rsi, %rsi
	je	.L898
	call	_ZN2v88internal11CallPrinter4FindEPNS0_7AstNodeEb.constprop.1
.L898:
	movq	40(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L899
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter4FindEPNS0_7AstNodeEb.constprop.1
.L899:
	movq	48(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L900
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter4FindEPNS0_7AstNodeEb.constprop.1
.L900:
	cmpb	$0, 28(%r12)
	je	.L901
	cmpb	$0, 29(%r12)
	jne	.L897
	addq	$8, %rsp
	movq	%r12, %rdi
	leaq	.LC14(%rip), %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	.p2align 4,,10
	.p2align 3
.L914:
	.cfi_restore_state
	movb	$1, 72(%r12)
.L897:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L901:
	.cfi_restore_state
	cmpb	$0, 72(%r12)
	jne	.L897
	movq	24(%rbx), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L914
	addq	$8, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	.cfi_endproc
.LFE19265:
	.size	_ZN2v88internal11CallPrinter17VisitForStatementEPNS0_12ForStatementE, .-_ZN2v88internal11CallPrinter17VisitForStatementEPNS0_12ForStatementE
	.section	.text._ZN2v88internal11CallPrinter19VisitCountOperationEPNS0_14CountOperationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter19VisitCountOperationEPNS0_14CountOperationE
	.type	_ZN2v88internal11CallPrinter19VisitCountOperationEPNS0_14CountOperationE, @function
_ZN2v88internal11CallPrinter19VisitCountOperationEPNS0_14CountOperationE:
.LFB19295:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 28(%rdi)
	movq	%rsi, %rbx
	je	.L950
	cmpb	$0, 29(%rdi)
	jne	.L949
	leaq	.LC22(%rip), %rsi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	movl	4(%rbx), %eax
	testb	$-128, %al
	je	.L952
	cmpb	$0, 28(%r12)
	je	.L950
	shrl	$8, %eax
	leaq	_ZN2v88internal5Token7string_E(%rip), %rdx
	andl	$127, %eax
	cmpb	$0, 29(%r12)
	movq	(%rdx,%rax,8), %rsi
	jne	.L949
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
.L952:
	movzbl	28(%r12), %eax
	movq	8(%rbx), %r13
	testb	%al, %al
	je	.L922
	cmpb	$0, 72(%r12)
	je	.L923
.L929:
	cmpb	$0, 28(%r12)
	je	.L915
	cmpb	$0, 29(%r12)
	jne	.L930
	leaq	.LC14(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	movzbl	28(%r12), %edx
	jmp	.L928
	.p2align 4,,10
	.p2align 3
.L949:
	cmpb	$0, 72(%r12)
	movq	8(%rbx), %r13
	jne	.L929
.L923:
	movl	8(%r12), %r14d
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L953
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
.L927:
	movzbl	28(%r12), %edx
	cmpl	8(%r12), %r14d
	je	.L929
.L928:
	movl	4(%rbx), %eax
	testb	$-128, %al
	jne	.L932
	testb	%dl, %dl
	je	.L915
	shrl	$8, %eax
	leaq	_ZN2v88internal5Token7string_E(%rip), %rdx
	andl	$127, %eax
	cmpb	$0, 29(%r12)
	movq	(%rdx,%rax,8), %rsi
	jne	.L915
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	movzbl	28(%r12), %edx
.L932:
	testb	%dl, %dl
	je	.L915
.L936:
	cmpb	$0, 29(%r12)
	jne	.L915
	popq	%rbx
	movq	%r12, %rdi
	leaq	.LC16(%rip), %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	.p2align 4,,10
	.p2align 3
.L950:
	.cfi_restore_state
	movq	8(%rbx), %r13
.L922:
	cmpb	$0, 72(%r12)
	je	.L954
.L915:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L954:
	.cfi_restore_state
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L955
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movzbl	28(%r12), %edx
	jmp	.L928
	.p2align 4,,10
	.p2align 3
.L930:
	testb	$-128, 4(%rbx)
	je	.L915
	jmp	.L936
	.p2align 4,,10
	.p2align 3
.L953:
	movb	$1, 72(%r12)
	jmp	.L927
	.p2align 4,,10
	.p2align 3
.L955:
	movb	$1, 72(%r12)
	movzbl	28(%r12), %edx
	jmp	.L928
	.cfi_endproc
.LFE19295:
	.size	_ZN2v88internal11CallPrinter19VisitCountOperationEPNS0_14CountOperationE, .-_ZN2v88internal11CallPrinter19VisitCountOperationEPNS0_14CountOperationE
	.section	.text._ZN2v88internal11CallPrinter21VisitCompareOperationEPNS0_16CompareOperationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter21VisitCompareOperationEPNS0_16CompareOperationE
	.type	_ZN2v88internal11CallPrinter21VisitCompareOperationEPNS0_16CompareOperationE, @function
_ZN2v88internal11CallPrinter21VisitCompareOperationEPNS0_16CompareOperationE:
.LFB19298:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 28(%rdi)
	movq	%rsi, %rbx
	je	.L957
	cmpb	$0, 29(%rdi)
	jne	.L958
	leaq	.LC22(%rip), %rsi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	cmpb	$0, 28(%r12)
	movq	8(%rbx), %r13
	je	.L959
	cmpb	$0, 72(%r12)
	je	.L991
.L960:
	cmpb	$0, 29(%r12)
	jne	.L972
	leaq	.LC14(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	movzbl	28(%r12), %eax
	jmp	.L963
	.p2align 4,,10
	.p2align 3
.L958:
	cmpb	$0, 72(%r12)
	movq	8(%rsi), %r13
	jne	.L960
.L991:
	movl	8(%r12), %r14d
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L992
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
.L962:
	movzbl	28(%r12), %eax
	cmpl	8(%r12), %r14d
	je	.L993
.L963:
	testb	%al, %al
	je	.L971
	cmpb	$0, 29(%r12)
	jne	.L972
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	movl	4(%rbx), %eax
	leaq	_ZN2v88internal5Token7string_E(%rip), %rdx
	shrl	$7, %eax
	andl	$127, %eax
	cmpb	$0, 28(%r12)
	movq	(%rdx,%rax,8), %rsi
	je	.L971
	cmpb	$0, 29(%r12)
	jne	.L972
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	cmpb	$0, 28(%r12)
	je	.L971
	cmpb	$0, 29(%r12)
	jne	.L972
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	cmpb	$0, 28(%r12)
	movq	16(%rbx), %r13
	je	.L994
	cmpb	$0, 72(%r12)
	je	.L983
.L975:
	cmpb	$0, 29(%r12)
	jne	.L956
	leaq	.LC14(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	jmp	.L978
	.p2align 4,,10
	.p2align 3
.L972:
	cmpb	$0, 72(%r12)
	movq	16(%rbx), %r13
	je	.L983
.L979:
	cmpb	$0, 28(%r12)
	jne	.L975
.L956:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L983:
	.cfi_restore_state
	movl	8(%r12), %ebx
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L995
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
.L977:
	cmpl	8(%r12), %ebx
	je	.L979
.L978:
	cmpb	$0, 28(%r12)
	je	.L956
	cmpb	$0, 29(%r12)
	jne	.L956
	popq	%rbx
	movq	%r12, %rdi
	leaq	.LC16(%rip), %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	.p2align 4,,10
	.p2align 3
.L957:
	.cfi_restore_state
	movq	8(%rsi), %r13
.L959:
	cmpb	$0, 72(%r12)
	jne	.L956
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L996
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movzbl	28(%r12), %eax
	jmp	.L963
.L993:
	testb	%al, %al
	jne	.L960
.L971:
	movzbl	72(%r12), %eax
	movq	16(%rbx), %r13
.L984:
	testb	%al, %al
	jne	.L956
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L997
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	jmp	.L978
	.p2align 4,,10
	.p2align 3
.L995:
	movb	$1, 72(%r12)
	jmp	.L977
	.p2align 4,,10
	.p2align 3
.L992:
	movb	$1, 72(%r12)
	jmp	.L962
	.p2align 4,,10
	.p2align 3
.L997:
	movb	$1, 72(%r12)
	jmp	.L978
	.p2align 4,,10
	.p2align 3
.L996:
	movb	$1, 72(%r12)
	movzbl	28(%r12), %eax
	jmp	.L963
.L994:
	movzbl	72(%r12), %eax
	jmp	.L984
	.cfi_endproc
.LFE19298:
	.size	_ZN2v88internal11CallPrinter21VisitCompareOperationEPNS0_16CompareOperationE, .-_ZN2v88internal11CallPrinter21VisitCompareOperationEPNS0_16CompareOperationE
	.section	.text._ZN2v88internal11CallPrinter20VisitBinaryOperationEPNS0_15BinaryOperationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter20VisitBinaryOperationEPNS0_15BinaryOperationE
	.type	_ZN2v88internal11CallPrinter20VisitBinaryOperationEPNS0_15BinaryOperationE, @function
_ZN2v88internal11CallPrinter20VisitBinaryOperationEPNS0_15BinaryOperationE:
.LFB19296:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 28(%rdi)
	movq	%rsi, %rbx
	je	.L999
	cmpb	$0, 29(%rdi)
	jne	.L1000
	leaq	.LC22(%rip), %rsi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	cmpb	$0, 28(%r12)
	movq	8(%rbx), %r13
	je	.L1001
	cmpb	$0, 72(%r12)
	je	.L1033
.L1002:
	cmpb	$0, 29(%r12)
	jne	.L1014
	leaq	.LC14(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	movzbl	28(%r12), %eax
	jmp	.L1005
	.p2align 4,,10
	.p2align 3
.L1000:
	cmpb	$0, 72(%r12)
	movq	8(%rsi), %r13
	jne	.L1002
.L1033:
	movl	8(%r12), %r14d
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L1034
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
.L1004:
	movzbl	28(%r12), %eax
	cmpl	8(%r12), %r14d
	je	.L1035
.L1005:
	testb	%al, %al
	je	.L1013
	cmpb	$0, 29(%r12)
	jne	.L1014
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	movl	4(%rbx), %eax
	leaq	_ZN2v88internal5Token7string_E(%rip), %rdx
	shrl	$7, %eax
	andl	$127, %eax
	cmpb	$0, 28(%r12)
	movq	(%rdx,%rax,8), %rsi
	je	.L1013
	cmpb	$0, 29(%r12)
	jne	.L1014
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	cmpb	$0, 28(%r12)
	je	.L1013
	cmpb	$0, 29(%r12)
	jne	.L1014
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	cmpb	$0, 28(%r12)
	movq	16(%rbx), %r13
	je	.L1036
	cmpb	$0, 72(%r12)
	je	.L1025
.L1017:
	cmpb	$0, 29(%r12)
	jne	.L998
	leaq	.LC14(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	jmp	.L1020
	.p2align 4,,10
	.p2align 3
.L1014:
	cmpb	$0, 72(%r12)
	movq	16(%rbx), %r13
	je	.L1025
.L1021:
	cmpb	$0, 28(%r12)
	jne	.L1017
.L998:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1025:
	.cfi_restore_state
	movl	8(%r12), %ebx
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L1037
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
.L1019:
	cmpl	8(%r12), %ebx
	je	.L1021
.L1020:
	cmpb	$0, 28(%r12)
	je	.L998
	cmpb	$0, 29(%r12)
	jne	.L998
	popq	%rbx
	movq	%r12, %rdi
	leaq	.LC16(%rip), %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	.p2align 4,,10
	.p2align 3
.L999:
	.cfi_restore_state
	movq	8(%rsi), %r13
.L1001:
	cmpb	$0, 72(%r12)
	jne	.L998
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L1038
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movzbl	28(%r12), %eax
	jmp	.L1005
.L1035:
	testb	%al, %al
	jne	.L1002
.L1013:
	movzbl	72(%r12), %eax
	movq	16(%rbx), %r13
.L1026:
	testb	%al, %al
	jne	.L998
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L1039
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	jmp	.L1020
	.p2align 4,,10
	.p2align 3
.L1037:
	movb	$1, 72(%r12)
	jmp	.L1019
	.p2align 4,,10
	.p2align 3
.L1034:
	movb	$1, 72(%r12)
	jmp	.L1004
	.p2align 4,,10
	.p2align 3
.L1039:
	movb	$1, 72(%r12)
	jmp	.L1020
	.p2align 4,,10
	.p2align 3
.L1038:
	movb	$1, 72(%r12)
	movzbl	28(%r12), %eax
	jmp	.L1005
.L1036:
	movzbl	72(%r12), %eax
	jmp	.L1026
	.cfi_endproc
.LFE19296:
	.size	_ZN2v88internal11CallPrinter20VisitBinaryOperationEPNS0_15BinaryOperationE, .-_ZN2v88internal11CallPrinter20VisitBinaryOperationEPNS0_15BinaryOperationE
	.section	.text._ZN2v88internal11CallPrinter19VisitForOfStatementEPNS0_14ForOfStatementE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter19VisitForOfStatementEPNS0_14ForOfStatementE
	.type	_ZN2v88internal11CallPrinter19VisitForOfStatementEPNS0_14ForOfStatementE, @function
_ZN2v88internal11CallPrinter19VisitForOfStatementEPNS0_14ForOfStatementE:
.LFB19267:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
	cmpb	$0, 28(%rdi)
	je	.L1041
	cmpb	$0, 29(%rdi)
	jne	.L1042
	leaq	.LC14(%rip), %rsi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	movzbl	72(%r12), %r13d
.L1043:
	movq	40(%rbx), %r14
	movzbl	28(%r12), %eax
	movl	(%r14), %ecx
	cmpl	%ecx, 24(%r12)
	je	.L1088
	testb	%al, %al
	jne	.L1048
	testb	%r13b, %r13b
	jne	.L1040
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L1089
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	jmp	.L1060
	.p2align 4,,10
	.p2align 3
.L1092:
	movb	$1, 72(%r12)
.L1040:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1041:
	.cfi_restore_state
	movzbl	72(%rdi), %r13d
	testb	%r13b, %r13b
	je	.L1090
	movq	40(%rsi), %rax
	movl	(%rax), %eax
	cmpl	%eax, 24(%rdi)
	jne	.L1040
	movl	48(%rsi), %eax
	cmpl	$1, %eax
	sete	32(%rdi)
	setne	31(%rdi)
.L1075:
	movb	$1, 28(%r12)
	movl	%r13d, %eax
	movl	$1, %r13d
.L1047:
	movq	40(%rbx), %r14
	testb	%al, %al
	je	.L1069
.L1055:
	cmpb	$0, 28(%r12)
	jne	.L1091
	movzbl	72(%r12), %eax
	testb	%r13b, %r13b
	jne	.L1067
	movq	24(%rbx), %r13
	jmp	.L1061
	.p2align 4,,10
	.p2align 3
.L1048:
	testb	%r13b, %r13b
	je	.L1069
	movzbl	29(%r12), %r13d
	testb	%r13b, %r13b
	jne	.L1040
.L1073:
	leaq	.LC14(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	testb	%r13b, %r13b
	je	.L1060
.L1087:
	movzbl	72(%r12), %eax
.L1067:
	movl	$256, %edx
	movw	%dx, 28(%r12)
	movq	24(%rbx), %r13
.L1061:
	testb	%al, %al
	jne	.L1040
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L1092
	addq	$8, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	.p2align 4,,10
	.p2align 3
.L1069:
	.cfi_restore_state
	movl	8(%r12), %r15d
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L1093
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
.L1053:
	cmpl	8(%r12), %r15d
	je	.L1055
	testb	%r13b, %r13b
	jne	.L1087
.L1060:
	cmpb	$0, 28(%r12)
	je	.L1062
	cmpb	$0, 29(%r12)
	jne	.L1040
	addq	$8, %rsp
	movq	%r12, %rdi
	leaq	.LC14(%rip), %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	.p2align 4,,10
	.p2align 3
.L1088:
	.cfi_restore_state
	movl	48(%rbx), %edx
	cmpl	$1, %edx
	sete	32(%r12)
	setne	31(%r12)
	testb	%al, %al
	je	.L1075
	movl	%r13d, %eax
	xorl	%r13d, %r13d
	jmp	.L1047
	.p2align 4,,10
	.p2align 3
.L1090:
	movq	32(%rsi), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L1094
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movzbl	72(%r12), %r13d
	jmp	.L1043
	.p2align 4,,10
	.p2align 3
.L1042:
	movq	40(%rsi), %r14
	movzbl	72(%rdi), %r13d
	movl	24(%rdi), %eax
	cmpl	%eax, (%r14)
	jne	.L1048
	movl	48(%rsi), %eax
	cmpl	$1, %eax
	movl	%r13d, %eax
	sete	32(%rdi)
	setne	31(%rdi)
	xorl	%r13d, %r13d
	jmp	.L1047
	.p2align 4,,10
	.p2align 3
.L1093:
	movb	$1, 72(%r12)
	jmp	.L1053
	.p2align 4,,10
	.p2align 3
.L1094:
	movb	$1, 72(%r12)
	movl	$1, %r13d
	jmp	.L1043
	.p2align 4,,10
	.p2align 3
.L1089:
	movb	$1, 72(%r12)
	jmp	.L1060
	.p2align 4,,10
	.p2align 3
.L1091:
	cmpb	$0, 29(%r12)
	je	.L1073
	testb	%r13b, %r13b
	jne	.L1087
	jmp	.L1040
.L1062:
	movq	24(%rbx), %r13
	movzbl	72(%r12), %eax
	jmp	.L1061
	.cfi_endproc
.LFE19267:
	.size	_ZN2v88internal11CallPrinter19VisitForOfStatementEPNS0_14ForOfStatementE, .-_ZN2v88internal11CallPrinter19VisitForOfStatementEPNS0_14ForOfStatementE
	.section	.rodata._ZN2v88internal11CallPrinter13VisitPropertyEPNS0_8PropertyE.str1.1,"aMS",@progbits,1
.LC27:
	.string	"?"
.LC28:
	.string	"."
.LC29:
	.string	"?."
.LC30:
	.string	"["
.LC31:
	.string	"]"
	.section	.text._ZN2v88internal11CallPrinter13VisitPropertyEPNS0_8PropertyE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter13VisitPropertyEPNS0_8PropertyE
	.type	_ZN2v88internal11CallPrinter13VisitPropertyEPNS0_8PropertyE, @function
_ZN2v88internal11CallPrinter13VisitPropertyEPNS0_8PropertyE:
.LFB19289:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	16(%rsi), %r13
	movzbl	4(%r13), %eax
	andl	$63, %eax
	cmpb	$41, %al
	jne	.L1096
	testq	%r13, %r13
	jne	.L1147
.L1096:
	cmpb	$0, 28(%r12)
	movq	8(%rbx), %r15
	movzbl	72(%r12), %eax
	je	.L1099
	testb	%al, %al
	je	.L1148
.L1100:
	cmpb	$0, 29(%r12)
	jne	.L1105
	leaq	.LC14(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	movzbl	28(%r12), %eax
	jmp	.L1103
	.p2align 4,,10
	.p2align 3
.L1148:
	movl	8(%r12), %r14d
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L1149
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
.L1102:
	movzbl	28(%r12), %eax
	cmpl	8(%r12), %r14d
	je	.L1150
.L1103:
	testb	$-128, 4(%rbx)
	je	.L1108
	testb	%al, %al
	je	.L1117
	cmpb	$0, 29(%r12)
	jne	.L1128
	leaq	.LC29(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	movzbl	28(%r12), %eax
.L1108:
	testb	%al, %al
	je	.L1117
.L1127:
	cmpb	$0, 29(%r12)
	jne	.L1128
	leaq	.LC30(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	cmpb	$0, 28(%r12)
	je	.L1117
	cmpb	$0, 72(%r12)
	je	.L1115
.L1118:
	cmpb	$0, 29(%r12)
	jne	.L1095
	leaq	.LC14(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	jmp	.L1122
	.p2align 4,,10
	.p2align 3
.L1147:
	movq	(%rdi), %rsi
	movq	%r13, %rdi
	call	_ZNK2v88internal7Literal10BuildValueEPNS0_7IsolateE@PLT
	movq	(%rax), %rax
	testb	$1, %al
	je	.L1096
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	testl	$65504, %eax
	jne	.L1096
	movq	8(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter4FindEPNS0_7AstNodeEb.constprop.0
	testb	$-128, 4(%rbx)
	je	.L1098
	cmpb	$0, 28(%r12)
	je	.L1110
	cmpb	$0, 29(%r12)
	jne	.L1110
	leaq	.LC27(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
.L1098:
	cmpb	$0, 28(%r12)
	je	.L1110
	cmpb	$0, 29(%r12)
	jne	.L1110
	leaq	.LC28(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
.L1110:
	movq	(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNK2v88internal7Literal10BuildValueEPNS0_7IsolateE@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	xorl	%edx, %edx
	popq	%rbx
	movq	%rax, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter12PrintLiteralENS0_6HandleINS0_6ObjectEEEb
	.p2align 4,,10
	.p2align 3
.L1105:
	.cfi_restore_state
	testb	$-128, 4(%rbx)
	je	.L1127
	.p2align 4,,10
	.p2align 3
.L1128:
	cmpb	$0, 72(%r12)
	je	.L1115
.L1114:
	cmpb	$0, 28(%r12)
	jne	.L1118
.L1095:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1099:
	.cfi_restore_state
	testb	%al, %al
	je	.L1151
.L1117:
	cmpb	$0, 72(%r12)
	jne	.L1095
.L1155:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L1152
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	jmp	.L1122
	.p2align 4,,10
	.p2align 3
.L1115:
	movl	8(%r12), %ebx
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L1153
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
.L1121:
	cmpl	8(%r12), %ebx
	je	.L1114
.L1122:
	cmpb	$0, 28(%r12)
	je	.L1095
	cmpb	$0, 29(%r12)
	jne	.L1095
	addq	$8, %rsp
	movq	%r12, %rdi
	leaq	.LC31(%rip), %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	.p2align 4,,10
	.p2align 3
.L1151:
	.cfi_restore_state
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L1154
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movzbl	28(%r12), %eax
	jmp	.L1103
.L1150:
	testb	%al, %al
	jne	.L1100
	cmpb	$0, 72(%r12)
	jne	.L1095
	jmp	.L1155
	.p2align 4,,10
	.p2align 3
.L1149:
	movb	$1, 72(%r12)
	jmp	.L1102
	.p2align 4,,10
	.p2align 3
.L1153:
	movb	$1, 72(%r12)
	jmp	.L1121
	.p2align 4,,10
	.p2align 3
.L1152:
	movb	$1, 72(%r12)
	jmp	.L1122
	.p2align 4,,10
	.p2align 3
.L1154:
	movb	$1, 72(%r12)
	movzbl	28(%r12), %eax
	jmp	.L1103
	.cfi_endproc
.LFE19289:
	.size	_ZN2v88internal11CallPrinter13VisitPropertyEPNS0_8PropertyE, .-_ZN2v88internal11CallPrinter13VisitPropertyEPNS0_8PropertyE
	.section	.rodata._ZN2v88internal11CallPrinter17VisitArrayLiteralEPNS0_12ArrayLiteralE.str1.1,"aMS",@progbits,1
.LC32:
	.string	","
	.section	.text._ZN2v88internal11CallPrinter17VisitArrayLiteralEPNS0_12ArrayLiteralE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter17VisitArrayLiteralEPNS0_12ArrayLiteralE
	.type	_ZN2v88internal11CallPrinter17VisitArrayLiteralEPNS0_12ArrayLiteralE, @function
_ZN2v88internal11CallPrinter17VisitArrayLiteralEPNS0_12ArrayLiteralE:
.LFB19280:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movzbl	28(%rdi), %esi
	testb	%sil, %sil
	je	.L1157
	movzbl	29(%rdi), %eax
	testb	%al, %al
	jne	.L1158
	leaq	.LC30(%rip), %rsi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	movzbl	28(%r12), %esi
	movl	36(%r15), %edi
	movl	%esi, %eax
	testl	%edi, %edi
	jle	.L1159
.L1185:
	xorl	%ebx, %ebx
	jmp	.L1173
	.p2align 4,,10
	.p2align 3
.L1167:
	movzbl	29(%r12), %eax
	testb	%al, %al
	jne	.L1170
	leaq	.LC14(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	movzbl	28(%r12), %esi
	movl	%esi, %eax
	.p2align 4,,10
	.p2align 3
.L1170:
	addq	$1, %rbx
	cmpl	%ebx, 36(%r15)
	jle	.L1159
.L1173:
	leaq	0(,%rbx,8), %r13
	testq	%rbx, %rbx
	je	.L1160
	testb	%al, %al
	je	.L1161
	cmpb	$0, 29(%r12)
	jne	.L1162
	leaq	.LC32(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	movzbl	28(%r12), %esi
	movl	%esi, %eax
.L1160:
	movq	24(%r15), %rdx
	movq	(%rdx,%r13), %r13
	movzbl	4(%r13), %edx
	andl	$63, %edx
	cmpb	$46, %dl
	jne	.L1163
	testq	%r13, %r13
	je	.L1163
	testb	%al, %al
	je	.L1183
.L1164:
	cmpb	$0, 72(%r12)
	jne	.L1167
	movl	8(%r12), %r14d
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L1208
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
.L1169:
	movzbl	28(%r12), %esi
	movl	%esi, %eax
	cmpl	8(%r12), %r14d
	jne	.L1170
	testb	%sil, %sil
	jne	.L1167
	addq	$1, %rbx
	cmpl	%ebx, 36(%r15)
	jg	.L1173
	.p2align 4,,10
	.p2align 3
.L1159:
	testb	%al, %al
	je	.L1156
.L1181:
	cmpb	$0, 29(%r12)
	jne	.L1156
	addq	$8, %rsp
	movq	%r12, %rdi
	leaq	.LC31(%rip), %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	.p2align 4,,10
	.p2align 3
.L1158:
	.cfi_restore_state
	movl	36(%r15), %edx
	testl	%edx, %edx
	jg	.L1185
	jmp	.L1181
	.p2align 4,,10
	.p2align 3
.L1163:
	testb	%al, %al
	jne	.L1164
	.p2align 4,,10
	.p2align 3
.L1166:
	cmpb	$0, 72(%r12)
	jne	.L1206
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L1209
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movzbl	28(%r12), %esi
.L1206:
	movl	%esi, %eax
	jmp	.L1170
	.p2align 4,,10
	.p2align 3
.L1161:
	movq	24(%r15), %rax
	movq	(%rax,%r13), %r13
	movzbl	4(%r13), %eax
	andl	$63, %eax
	cmpb	$46, %al
	jne	.L1166
	testq	%r13, %r13
	je	.L1166
.L1183:
	movq	16(%r13), %rax
	movl	(%rax), %eax
	cmpl	%eax, 24(%r12)
	jne	.L1166
	cmpb	$0, 72(%r12)
	movb	$1, 28(%r12)
	movb	$1, 31(%r12)
	je	.L1210
.L1174:
	cmpb	$0, 29(%r12)
	jne	.L1178
	leaq	.LC14(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
.L1178:
	movb	$1, 29(%r12)
.L1156:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1162:
	.cfi_restore_state
	movq	24(%r15), %rax
	movq	(%rax,%r13), %r13
	jmp	.L1164
	.p2align 4,,10
	.p2align 3
.L1208:
	movb	$1, 72(%r12)
	jmp	.L1169
	.p2align 4,,10
	.p2align 3
.L1209:
	movzbl	28(%r12), %esi
	movb	$1, 72(%r12)
	movl	%esi, %eax
	jmp	.L1170
	.p2align 4,,10
	.p2align 3
.L1157:
	movl	36(%r15), %ecx
	movl	%esi, %eax
	testl	%ecx, %ecx
	jg	.L1185
	jmp	.L1156
.L1210:
	movq	16(%r13), %r13
	movl	8(%r12), %ebx
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	64(%r12), %rax
	jb	.L1211
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter25VisitNoStackOverflowCheckEPNS0_7AstNodeE
.L1176:
	cmpl	8(%r12), %ebx
	jne	.L1178
	cmpb	$0, 28(%r12)
	jne	.L1174
	jmp	.L1178
	.p2align 4,,10
	.p2align 3
.L1211:
	movb	$1, 72(%r12)
	jmp	.L1176
	.cfi_endproc
.LFE19280:
	.size	_ZN2v88internal11CallPrinter17VisitArrayLiteralEPNS0_12ArrayLiteralE, .-_ZN2v88internal11CallPrinter17VisitArrayLiteralEPNS0_12ArrayLiteralE
	.section	.text._ZN2v88internal11CallPrinter12VisitLiteralEPNS0_7LiteralE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter12VisitLiteralEPNS0_7LiteralE
	.type	_ZN2v88internal11CallPrinter12VisitLiteralEPNS0_7LiteralE, @function
_ZN2v88internal11CallPrinter12VisitLiteralEPNS0_7LiteralE:
.LFB19277:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%rsi, %rdi
	subq	$16, %rsp
	movq	(%r12), %rsi
	call	_ZNK2v88internal7Literal10BuildValueEPNS0_7IsolateE@PLT
	movq	(%rax), %rdx
	movq	%rax, %r13
	movq	%rdx, %rax
	notq	%rax
	testb	$1, %dl
	jne	.L1240
.L1214:
	movq	(%r12), %rdi
	cmpq	%rdx, 104(%rdi)
	je	.L1241
	cmpq	%rdx, 112(%rdi)
	je	.L1242
	cmpq	%rdx, 120(%rdi)
	je	.L1243
	cmpq	%rdx, 88(%rdi)
	je	.L1244
	testb	$1, %al
	jne	.L1226
	movq	-1(%rdx), %rax
	cmpw	$65, 11(%rax)
	je	.L1226
	testb	$1, %dl
	je	.L1212
	movq	-1(%rdx), %rax
	cmpw	$64, 11(%rax)
	jne	.L1212
	movq	41112(%rdi), %r8
	movq	15(%rdx), %r13
	testq	%r8, %r8
	je	.L1231
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1232:
	addq	$16, %rsp
	movq	%r12, %rdi
	xorl	%edx, %edx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter12PrintLiteralENS0_6HandleINS0_6ObjectEEEb
	.p2align 4,,10
	.p2align 3
.L1240:
	.cfi_restore_state
	movq	-1(%rdx), %rcx
	cmpw	$63, 11(%rcx)
	ja	.L1214
	cmpb	$0, 28(%r12)
	je	.L1212
	cmpb	$0, 29(%r12)
	jne	.L1234
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	cmpb	$0, 28(%r12)
	je	.L1212
.L1234:
	cmpb	$0, 29(%r12)
	jne	.L1219
	addl	$1, 8(%r12)
	movq	16(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	cmpb	$0, 28(%r12)
	je	.L1212
.L1219:
	cmpb	$0, 29(%r12)
	leaq	.LC3(%rip), %rsi
	je	.L1239
.L1212:
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1241:
	.cfi_restore_state
	cmpb	$0, 28(%r12)
	je	.L1212
	cmpb	$0, 29(%r12)
	jne	.L1212
	leaq	.LC4(%rip), %rsi
.L1239:
	addq	$16, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	.p2align 4,,10
	.p2align 3
.L1242:
	.cfi_restore_state
	cmpb	$0, 28(%r12)
	je	.L1212
	cmpb	$0, 29(%r12)
	leaq	.LC5(%rip), %rsi
	je	.L1239
	jmp	.L1212
	.p2align 4,,10
	.p2align 3
.L1244:
	cmpb	$0, 28(%r12)
	je	.L1212
	cmpb	$0, 29(%r12)
	leaq	.LC7(%rip), %rsi
	je	.L1239
	jmp	.L1212
	.p2align 4,,10
	.p2align 3
.L1226:
	movl	$1, %edx
	movq	%r13, %rsi
	call	_ZN2v88internal7Factory14NumberToStringENS0_6HandleINS0_6ObjectEEEb@PLT
	cmpb	$0, 28(%r12)
	je	.L1212
	cmpb	$0, 29(%r12)
	jne	.L1212
	addl	$1, 8(%r12)
	movq	16(%r12), %rdi
	addq	$16, %rsp
	movq	%rax, %rsi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	.p2align 4,,10
	.p2align 3
.L1243:
	.cfi_restore_state
	cmpb	$0, 28(%r12)
	je	.L1212
	cmpb	$0, 29(%r12)
	leaq	.LC6(%rip), %rsi
	je	.L1239
	jmp	.L1212
.L1231:
	movq	41088(%rdi), %rsi
	cmpq	41096(%rdi), %rsi
	je	.L1245
.L1233:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rdi)
	movq	%r13, (%rsi)
	jmp	.L1232
.L1245:
	movq	%rdi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rdi
	movq	%rax, %rsi
	jmp	.L1233
	.cfi_endproc
.LFE19277:
	.size	_ZN2v88internal11CallPrinter12VisitLiteralEPNS0_7LiteralE, .-_ZN2v88internal11CallPrinter12VisitLiteralEPNS0_7LiteralE
	.section	.text._ZN2v88internal11CallPrinter18VisitVariableProxyEPNS0_13VariableProxyE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter18VisitVariableProxyEPNS0_13VariableProxyE
	.type	_ZN2v88internal11CallPrinter18VisitVariableProxyEPNS0_13VariableProxyE, @function
_ZN2v88internal11CallPrinter18VisitVariableProxyEPNS0_13VariableProxyE:
.LFB19281:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$16, %rsp
	cmpb	$0, 30(%rdi)
	je	.L1247
	movq	8(%rsi), %rax
	testb	$1, 5(%rsi)
	je	.L1248
	movq	8(%rax), %rax
.L1248:
	movq	(%rax), %rsi
	movq	(%rsi), %rax
	movq	%rax, %rdx
	notq	%rdx
	testb	$1, %al
	jne	.L1273
.L1250:
	movq	(%r12), %rdi
	cmpq	104(%rdi), %rax
	je	.L1274
	cmpq	112(%rdi), %rax
	je	.L1275
	cmpq	120(%rdi), %rax
	je	.L1276
	cmpq	88(%rdi), %rax
	je	.L1277
	andl	$1, %edx
	je	.L1278
.L1258:
	movl	$1, %edx
	call	_ZN2v88internal7Factory14NumberToStringENS0_6HandleINS0_6ObjectEEEb@PLT
	cmpb	$0, 28(%r12)
	je	.L1246
	cmpb	$0, 29(%r12)
	jne	.L1246
	addl	$1, 8(%r12)
	movq	16(%r12), %rdi
	movq	%rax, %rsi
.L1271:
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	.p2align 4,,10
	.p2align 3
.L1247:
	.cfi_restore_state
	cmpb	$0, 28(%rdi)
	je	.L1246
	cmpb	$0, 29(%rdi)
	jne	.L1246
	leaq	.LC18(%rip), %rsi
.L1272:
	addq	$16, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	.p2align 4,,10
	.p2align 3
.L1273:
	.cfi_restore_state
	movq	-1(%rax), %rcx
	cmpw	$63, 11(%rcx)
	ja	.L1250
	cmpb	$0, 28(%r12)
	je	.L1246
	cmpb	$0, 29(%r12)
	je	.L1279
.L1246:
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1275:
	.cfi_restore_state
	cmpb	$0, 28(%r12)
	je	.L1246
	cmpb	$0, 29(%r12)
	leaq	.LC5(%rip), %rsi
	je	.L1272
	jmp	.L1246
	.p2align 4,,10
	.p2align 3
.L1274:
	cmpb	$0, 28(%r12)
	je	.L1246
	cmpb	$0, 29(%r12)
	leaq	.LC4(%rip), %rsi
	je	.L1272
	jmp	.L1246
	.p2align 4,,10
	.p2align 3
.L1276:
	cmpb	$0, 28(%r12)
	je	.L1246
	cmpb	$0, 29(%r12)
	leaq	.LC6(%rip), %rsi
	je	.L1272
	jmp	.L1246
	.p2align 4,,10
	.p2align 3
.L1278:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	je	.L1258
	testb	$1, %al
	je	.L1246
	movq	-1(%rax), %rdx
	cmpw	$64, 11(%rdx)
	jne	.L1246
	movq	41112(%rdi), %r8
	movq	15(%rax), %r13
	testq	%r8, %r8
	je	.L1263
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1264:
	addq	$16, %rsp
	movq	%r12, %rdi
	xorl	%edx, %edx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter12PrintLiteralENS0_6HandleINS0_6ObjectEEEb
	.p2align 4,,10
	.p2align 3
.L1279:
	.cfi_restore_state
	addl	$1, 8(%r12)
	movq	16(%r12), %rdi
	jmp	.L1271
	.p2align 4,,10
	.p2align 3
.L1277:
	cmpb	$0, 28(%r12)
	je	.L1246
	cmpb	$0, 29(%r12)
	leaq	.LC7(%rip), %rsi
	je	.L1272
	jmp	.L1246
.L1263:
	movq	41088(%rdi), %rsi
	cmpq	41096(%rdi), %rsi
	je	.L1280
.L1265:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rdi)
	movq	%r13, (%rsi)
	jmp	.L1264
.L1280:
	movq	%rdi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rdi
	movq	%rax, %rsi
	jmp	.L1265
	.cfi_endproc
.LFE19281:
	.size	_ZN2v88internal11CallPrinter18VisitVariableProxyEPNS0_13VariableProxyE, .-_ZN2v88internal11CallPrinter18VisitVariableProxyEPNS0_13VariableProxyE
	.section	.text._ZN2v88internal11CallPrinter12PrintLiteralEPKNS0_12AstRawStringEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallPrinter12PrintLiteralEPKNS0_12AstRawStringEb
	.type	_ZN2v88internal11CallPrinter12PrintLiteralEPKNS0_12AstRawStringEb, @function
_ZN2v88internal11CallPrinter12PrintLiteralEPKNS0_12AstRawStringEb:
.LFB19311:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$16, %rsp
	movq	(%rsi), %r13
	movq	0(%r13), %rax
	movq	%rax, %rcx
	notq	%rcx
	testb	$1, %al
	jne	.L1314
.L1283:
	movq	(%r12), %rdi
	cmpq	%rax, 104(%rdi)
	je	.L1315
	cmpq	%rax, 112(%rdi)
	je	.L1316
	cmpq	%rax, 120(%rdi)
	je	.L1317
	cmpq	%rax, 88(%rdi)
	je	.L1318
	andl	$1, %ecx
	jne	.L1295
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	je	.L1295
	testb	$1, %al
	je	.L1281
	movq	-1(%rax), %rdx
	cmpw	$64, 11(%rdx)
	jne	.L1281
	movq	41112(%rdi), %r8
	movq	15(%rax), %r13
	testq	%r8, %r8
	je	.L1300
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1301:
	addq	$16, %rsp
	movq	%r12, %rdi
	xorl	%edx, %edx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter12PrintLiteralENS0_6HandleINS0_6ObjectEEEb
	.p2align 4,,10
	.p2align 3
.L1314:
	.cfi_restore_state
	movq	-1(%rax), %rsi
	cmpw	$63, 11(%rsi)
	ja	.L1283
	movzbl	28(%rdi), %eax
	testb	%dl, %dl
	je	.L1285
	testb	%al, %al
	je	.L1281
	cmpb	$0, 29(%rdi)
	jne	.L1288
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	cmpb	$0, 28(%r12)
	je	.L1281
.L1288:
	cmpb	$0, 29(%r12)
	je	.L1319
.L1304:
	cmpb	$0, 29(%r12)
	leaq	.LC3(%rip), %rsi
	je	.L1312
.L1281:
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1315:
	.cfi_restore_state
	cmpb	$0, 28(%r12)
	je	.L1281
	cmpb	$0, 29(%r12)
	jne	.L1281
	leaq	.LC4(%rip), %rsi
.L1312:
	addq	$16, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11CallPrinter5PrintEPKc.part.0
	.p2align 4,,10
	.p2align 3
.L1316:
	.cfi_restore_state
	cmpb	$0, 28(%r12)
	je	.L1281
	cmpb	$0, 29(%r12)
	leaq	.LC5(%rip), %rsi
	je	.L1312
	jmp	.L1281
	.p2align 4,,10
	.p2align 3
.L1318:
	cmpb	$0, 28(%r12)
	je	.L1281
	cmpb	$0, 29(%r12)
	leaq	.LC7(%rip), %rsi
	je	.L1312
	jmp	.L1281
	.p2align 4,,10
	.p2align 3
.L1295:
	movl	$1, %edx
	movq	%r13, %rsi
	call	_ZN2v88internal7Factory14NumberToStringENS0_6HandleINS0_6ObjectEEEb@PLT
	cmpb	$0, 28(%r12)
	je	.L1281
	cmpb	$0, 29(%r12)
	jne	.L1281
	addl	$1, 8(%r12)
	movq	16(%r12), %rdi
	movq	%rax, %rsi
.L1313:
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	.p2align 4,,10
	.p2align 3
.L1317:
	.cfi_restore_state
	cmpb	$0, 28(%r12)
	je	.L1281
	cmpb	$0, 29(%r12)
	leaq	.LC6(%rip), %rsi
	je	.L1312
	jmp	.L1281
	.p2align 4,,10
	.p2align 3
.L1285:
	testb	%al, %al
	je	.L1281
	cmpb	$0, 29(%rdi)
	jne	.L1281
	addl	$1, 8(%rdi)
	movq	%r13, %rsi
	movq	16(%rdi), %rdi
	jmp	.L1313
	.p2align 4,,10
	.p2align 3
.L1319:
	addl	$1, 8(%r12)
	movq	16(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	cmpb	$0, 28(%r12)
	jne	.L1304
	jmp	.L1281
	.p2align 4,,10
	.p2align 3
.L1300:
	movq	41088(%rdi), %rsi
	cmpq	41096(%rdi), %rsi
	je	.L1320
.L1302:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rdi)
	movq	%r13, (%rsi)
	jmp	.L1301
.L1320:
	movq	%rdi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rdi
	movq	%rax, %rsi
	jmp	.L1302
	.cfi_endproc
.LFE19311:
	.size	_ZN2v88internal11CallPrinter12PrintLiteralEPKNS0_12AstRawStringEb, .-_ZN2v88internal11CallPrinter12PrintLiteralEPKNS0_12AstRawStringEb
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal11CallPrinterC2EPNS0_7IsolateEb,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal11CallPrinterC2EPNS0_7IsolateEb, @function
_GLOBAL__sub_I__ZN2v88internal11CallPrinterC2EPNS0_7IsolateEb:
.LFB23622:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE23622:
	.size	_GLOBAL__sub_I__ZN2v88internal11CallPrinterC2EPNS0_7IsolateEb, .-_GLOBAL__sub_I__ZN2v88internal11CallPrinterC2EPNS0_7IsolateEb
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal11CallPrinterC2EPNS0_7IsolateEb
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
