	.file	"execution.cc"
	.text
	.section	.text._ZN2v88Platform19DumpWithoutCrashingEv,"axG",@progbits,_ZN2v88Platform19DumpWithoutCrashingEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88Platform19DumpWithoutCrashingEv
	.type	_ZN2v88Platform19DumpWithoutCrashingEv, @function
_ZN2v88Platform19DumpWithoutCrashingEv:
.LFB4876:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4876:
	.size	_ZN2v88Platform19DumpWithoutCrashingEv, .-_ZN2v88Platform19DumpWithoutCrashingEv
	.section	.rodata._ZN2v88internal12_GLOBAL__N_16InvokeEPNS0_7IsolateERKNS1_12InvokeParamsE.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"AllowJavascriptExecution::IsAllowed(isolate)"
	.section	.rodata._ZN2v88internal12_GLOBAL__N_16InvokeEPNS0_7IsolateERKNS1_12InvokeParamsE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"Check failed: %s."
.LC2:
	.string	"unreachable code"
	.section	.text._ZN2v88internal12_GLOBAL__N_16InvokeEPNS0_7IsolateERKNS1_12InvokeParamsE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_16InvokeEPNS0_7IsolateERKNS1_12InvokeParamsE, @function
_ZN2v88internal12_GLOBAL__N_16InvokeEPNS0_7IsolateERKNS1_12InvokeParamsE:
.LFB19064:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -112(%rbp)
	movaps	%xmm0, -144(%rbp)
	movaps	%xmm0, -128(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L78
.L4:
	movq	(%rbx), %r13
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L79
.L6:
	movl	12616(%r12), %r13d
	movq	%r12, %rdi
	movl	$0, 12616(%r12)
	call	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb1EE9IsAllowedEPNS0_7IsolateE@PLT
	testb	%al, %al
	je	.L80
	movq	%r12, %rdi
	call	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE1ELb0EE9IsAllowedEPNS0_7IsolateE@PLT
	movq	%r12, %rdi
	testb	%al, %al
	jne	.L18
	call	_ZN2v88internal7Isolate21ThrowIllegalOperationEv@PLT
.L75:
	movl	48(%rbx), %esi
	testl	%esi, %esi
	je	.L81
.L77:
	xorl	%eax, %eax
.L21:
	movl	%r13d, 12616(%r12)
.L16:
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L82
.L50:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L83
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore_state
	call	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE2ELb0EE9IsAllowedEPNS0_7IsolateE@PLT
	testb	%al, %al
	je	.L84
	movl	68(%rbx), %eax
	testl	%eax, %eax
	je	.L85
	cmpb	$0, 64(%rbx)
	jne	.L51
.L32:
	cmpl	$1, %eax
	jne	.L33
	leaq	41184(%r12), %rdi
	movl	$42, %esi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	movq	%rax, %r14
.L31:
	leaq	-176(%rbp), %r15
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal11SaveContextC1EPNS0_7IsolateE@PLT
	cmpb	$0, _ZN2v88internal33FLAG_clear_exceptions_on_js_entryE(%rip)
	je	.L34
	movq	96(%r12), %rax
	movq	%rax, 12480(%r12)
.L34:
	movl	68(%rbx), %ecx
	movq	(%r14), %rdx
	testl	%ecx, %ecx
	movq	%rdx, -96(%rbp)
	leaq	63(%rdx), %r14
	jne	.L35
	movl	43(%rdx), %edx
	testl	%edx, %edx
	js	.L86
.L37:
	movq	32(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	24(%rbx), %r9
	movq	(%rax), %r10
	movq	(%rbx), %rax
	movq	(%rax), %r11
	movq	8(%rbx), %rax
	movq	(%rax), %rcx
	movq	$0, -64(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L87
.L38:
	leaq	128(%r12), %rdi
	movslq	16(%rbx), %r8
	movq	%r11, %rdx
	movq	%r10, %rsi
	call	*%r14
	movq	-96(%rbp), %rdi
	movq	%rax, %r14
	testq	%rdi, %rdi
	jne	.L76
.L40:
	movq	%r15, %rdi
	call	_ZN2v88internal11SaveContextD1Ev@PLT
	cmpq	%r14, 312(%r12)
	je	.L75
	movq	96(%r12), %rax
	movq	41112(%r12), %rdi
	movq	%rax, 12536(%r12)
	testq	%rdi, %rdi
	je	.L47
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L79:
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L6
	cmpb	$0, 64(%rbx)
	je	.L8
	movq	-1(%rax), %rdx
	testb	$64, 13(%rdx)
	je	.L6
.L8:
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	testb	$1, %al
	je	.L6
	movq	-1(%rax), %rax
	cmpw	$88, 11(%rax)
	jne	.L6
	movq	0(%r13), %rax
	leaq	-96(%rbp), %r14
	movq	%r14, %rdi
	movq	23(%rax), %rax
	movq	%rax, -96(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo12BreakAtEntryEv@PLT
	testb	%al, %al
	jne	.L6
	movq	0(%r13), %rax
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	31(%rax), %rdx
	call	_ZN2v88internal20SaveAndSwitchContextC1EPNS0_7IsolateENS0_7ContextE@PLT
	movzbl	64(%rbx), %eax
	leaq	96(%r12), %rcx
	testb	%al, %al
	jne	.L12
	movq	8(%rbx), %rcx
.L12:
	subq	$8, %rsp
	pushq	32(%rbx)
	movq	24(%rbx), %r9
	movzbl	%al, %esi
	movl	16(%rbx), %r8d
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8Builtins17InvokeApiFunctionEPNS0_7IsolateEbNS0_6HandleINS0_10HeapObjectEEENS4_INS0_6ObjectEEEiPS8_S6_@PLT
	popq	%r8
	popq	%r9
	testq	%rax, %rax
	je	.L88
	movq	96(%r12), %rdx
	movq	%rdx, 12536(%r12)
.L15:
	movq	%r14, %rdi
	movq	%rax, -184(%rbp)
	call	_ZN2v88internal11SaveContextD2Ev@PLT
	movq	-184(%rbp), %rax
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L81:
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate21ReportPendingMessagesEv@PLT
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L85:
	movq	12464(%r12), %rax
	movq	39(%rax), %r14
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L25
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r14
	movq	%rax, %rsi
.L26:
	movq	1143(%r14), %rax
	cmpq	%rax, 88(%r12)
	je	.L28
	movq	(%rsi), %rax
	movq	1143(%rax), %rax
	cmpq	%rax, _ZN2v88internal3Smi5kZeroE(%rip)
	je	.L53
	movq	7(%rax), %rax
.L29:
	movq	%r12, %rdi
	call	*%rax
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate21ThrowIllegalOperationEv@PLT
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L84:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	leaq	_ZN2v88Platform19DumpWithoutCrashingEv(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	152(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L89
.L23:
	leaq	88(%r12), %rax
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L35:
	movl	43(%rdx), %eax
	testl	%eax, %eax
	js	.L90
.L42:
	pxor	%xmm0, %xmm0
	movq	$0, -64(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L91
.L43:
	leaq	128(%r12), %rdi
	movq	40(%rbx), %rsi
	call	*%r14
	movq	-96(%rbp), %rdi
	movq	%rax, %r14
	testq	%rdi, %rdi
	je	.L40
.L76:
	leaq	-88(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L78:
	movq	40960(%rdi), %rax
	leaq	-136(%rbp), %rsi
	movl	$161, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -144(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L80:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L82:
	leaq	-136(%rbp), %rsi
	movq	%rax, -184(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	movq	-184(%rbp), %rax
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L28:
	cmpb	$0, 64(%rbx)
	jne	.L51
	movl	68(%rbx), %eax
	testl	%eax, %eax
	jne	.L32
	leaq	41184(%r12), %rdi
	movl	$40, %esi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	movq	%rax, %r14
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L51:
	leaq	41184(%r12), %rdi
	movl	$41, %esi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	movq	%rax, %r14
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L25:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L92
.L27:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L47:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L93
.L49:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%r14, (%rax)
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L89:
	call	*%rax
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L90:
	leaq	-96(%rbp), %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	movq	%rax, %r14
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L86:
	leaq	-96(%rbp), %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	movq	%rax, %r14
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L93:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L92:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L91:
	movq	40960(%r12), %rax
	leaq	-88(%rbp), %rsi
	movl	$164, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L87:
	leaq	-88(%rbp), %rsi
	movq	40960(%r12), %rax
	movl	$164, %edx
	movq	%r10, -208(%rbp)
	movq	%r11, -200(%rbp)
	leaq	23240(%rax), %rdi
	movq	%rcx, -192(%rbp)
	movq	%r9, -184(%rbp)
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	movq	-208(%rbp), %r10
	movq	-200(%rbp), %r11
	movq	-192(%rbp), %rcx
	movq	-184(%rbp), %r9
	jmp	.L38
.L53:
	xorl	%eax, %eax
	jmp	.L29
.L88:
	movl	48(%rbx), %edi
	testl	%edi, %edi
	je	.L94
	xorl	%eax, %eax
	jmp	.L15
.L94:
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate21ReportPendingMessagesEv@PLT
	xorl	%eax, %eax
	jmp	.L15
.L83:
	call	__stack_chk_fail@PLT
.L33:
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19064:
	.size	_ZN2v88internal12_GLOBAL__N_16InvokeEPNS0_7IsolateERKNS1_12InvokeParamsE, .-_ZN2v88internal12_GLOBAL__N_16InvokeEPNS0_7IsolateERKNS1_12InvokeParamsE
	.section	.text._ZN2v88internal9Execution4CallEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_iPS6_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Execution4CallEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_iPS6_
	.type	_ZN2v88internal9Execution4CallEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_iPS6_, @function
_ZN2v88internal9Execution4CallEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_iPS6_:
.LFB19066:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%ecx, %ebx
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	movq	%rsi, -96(%rbp)
	testb	$1, %al
	jne	.L104
.L97:
	leaq	88(%r12), %rax
	leaq	-96(%rbp), %rsi
	movq	%r12, %rdi
	movl	%ebx, -80(%rbp)
	movq	%rdx, -88(%rbp)
	movq	%r8, -72(%rbp)
	movq	%rax, -64(%rbp)
	movq	$0, -56(%rbp)
	movl	$0, -48(%rbp)
	movq	$0, -40(%rbp)
	movb	$0, -32(%rbp)
	movl	$0, -28(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_16InvokeEPNS0_7IsolateERKNS1_12InvokeParamsE
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L105
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L104:
	.cfi_restore_state
	movq	-1(%rax), %rcx
	cmpw	$1025, 11(%rcx)
	jne	.L97
	movq	41112(%rdi), %rdi
	movq	31(%rax), %rsi
	testq	%rdi, %rdi
	je	.L99
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-104(%rbp), %r8
	movq	%rax, %rdx
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L99:
	movq	41088(%r12), %rdx
	cmpq	41096(%r12), %rdx
	je	.L106
.L101:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdx)
	jmp	.L97
.L106:
	movq	%r12, %rdi
	movq	%r8, -112(%rbp)
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-112(%rbp), %r8
	movq	-104(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L101
.L105:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19066:
	.size	_ZN2v88internal9Execution4CallEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_iPS6_, .-_ZN2v88internal9Execution4CallEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_iPS6_
	.section	.text._ZN2v88internal9Execution11CallBuiltinEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEENS4_INS0_6ObjectEEEiPS8_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Execution11CallBuiltinEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEENS4_INS0_6ObjectEEEiPS8_
	.type	_ZN2v88internal9Execution11CallBuiltinEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEENS4_INS0_6ObjectEEEiPS8_, @function
_ZN2v88internal9Execution11CallBuiltinEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEENS4_INS0_6ObjectEEEiPS8_:
.LFB19067:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%ecx, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$96, %rsp
	.cfi_offset 3, -48
	movq	41472(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -112(%rbp)
	movzbl	12(%r13), %ebx
	movb	$1, 12(%r13)
	movq	(%rdx), %rax
	testb	$1, %al
	jne	.L116
.L109:
	leaq	88(%r12), %rax
	leaq	-112(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rdx, -104(%rbp)
	movl	%r14d, -96(%rbp)
	movq	%r8, -88(%rbp)
	movq	%rax, -80(%rbp)
	movq	$0, -72(%rbp)
	movl	$0, -64(%rbp)
	movq	$0, -56(%rbp)
	movb	$0, -48(%rbp)
	movl	$0, -44(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_16InvokeEPNS0_7IsolateERKNS1_12InvokeParamsE
	movb	%bl, 12(%r13)
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L117
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L116:
	.cfi_restore_state
	movq	-1(%rax), %rcx
	cmpw	$1025, 11(%rcx)
	jne	.L109
	movq	41112(%rdi), %rdi
	movq	31(%rax), %rsi
	testq	%rdi, %rdi
	je	.L111
	movq	%r8, -120(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-120(%rbp), %r8
	movq	%rax, %rdx
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L111:
	movq	41088(%r12), %rdx
	cmpq	41096(%r12), %rdx
	je	.L118
.L113:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdx)
	jmp	.L109
.L118:
	movq	%r12, %rdi
	movq	%r8, -128(%rbp)
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-128(%rbp), %r8
	movq	-120(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L113
.L117:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19067:
	.size	_ZN2v88internal9Execution11CallBuiltinEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEENS4_INS0_6ObjectEEEiPS8_, .-_ZN2v88internal9Execution11CallBuiltinEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEENS4_INS0_6ObjectEEEiPS8_
	.section	.text._ZN2v88internal9Execution3NewEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEiPS6_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Execution3NewEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEiPS6_
	.type	_ZN2v88internal9Execution3NewEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEiPS6_, @function
_ZN2v88internal9Execution3NewEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEiPS6_:
.LFB19068:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	88(%rdi), %rax
	movq	%rsi, -48(%rbp)
	leaq	-80(%rbp), %rsi
	movq	%rax, %xmm2
	movl	%edx, -64(%rbp)
	punpcklqdq	%xmm2, %xmm1
	movq	%rcx, -56(%rbp)
	movq	$0, -40(%rbp)
	movl	$0, -32(%rbp)
	movq	$0, -24(%rbp)
	movb	$1, -16(%rbp)
	movl	$0, -12(%rbp)
	movaps	%xmm1, -80(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_16InvokeEPNS0_7IsolateERKNS1_12InvokeParamsE
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L122
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L122:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19068:
	.size	_ZN2v88internal9Execution3NewEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEiPS6_, .-_ZN2v88internal9Execution3NewEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEiPS6_
	.section	.text._ZN2v88internal9Execution3NewEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_iPS6_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Execution3NewEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_iPS6_
	.type	_ZN2v88internal9Execution3NewEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_iPS6_, @function
_ZN2v88internal9Execution3NewEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_iPS6_:
.LFB19069:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	88(%rdi), %rax
	leaq	-80(%rbp), %rsi
	movq	%rdx, -48(%rbp)
	movq	%rax, %xmm1
	movl	%ecx, -64(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movq	%r8, -56(%rbp)
	movq	$0, -40(%rbp)
	movl	$0, -32(%rbp)
	movq	$0, -24(%rbp)
	movb	$1, -16(%rbp)
	movl	$0, -12(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_16InvokeEPNS0_7IsolateERKNS1_12InvokeParamsE
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L126
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L126:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19069:
	.size	_ZN2v88internal9Execution3NewEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_iPS6_, .-_ZN2v88internal9Execution3NewEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_iPS6_
	.section	.text._ZN2v88internal9Execution7TryCallEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_iPS6_NS1_15MessageHandlingEPNS0_11MaybeHandleIS5_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Execution7TryCallEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_iPS6_NS1_15MessageHandlingEPNS0_11MaybeHandleIS5_EE
	.type	_ZN2v88internal9Execution7TryCallEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_iPS6_NS1_15MessageHandlingEPNS0_11MaybeHandleIS5_EE, @function
_ZN2v88internal9Execution7TryCallEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_iPS6_NS1_15MessageHandlingEPNS0_11MaybeHandleIS5_EE:
.LFB19070:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$168, %rsp
	movq	16(%rbp), %r14
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	movq	%rsi, -112(%rbp)
	testb	$1, %al
	jne	.L150
.L129:
	leaq	88(%r12), %rax
	movq	%rdx, -104(%rbp)
	movl	%r13d, -96(%rbp)
	movq	%r8, -88(%rbp)
	movq	%rax, -80(%rbp)
	movq	$0, -72(%rbp)
	movl	%r9d, -64(%rbp)
	movq	%r14, -56(%rbp)
	movb	$0, -48(%rbp)
	movl	$0, -44(%rbp)
	testq	%r14, %r14
	je	.L134
	movq	$0, (%r14)
.L134:
	leaq	-160(%rbp), %r13
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88TryCatchC1EPNS_7IsolateE@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88TryCatch10SetVerboseEb@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88TryCatch17SetCaptureMessageEb@PLT
	leaq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_16InvokeEPNS0_7IsolateERKNS1_12InvokeParamsE
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L135
.L149:
	movq	%r13, %rdi
	call	_ZN2v88TryCatchD1Ev@PLT
.L136:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L151
	addq	$168, %rsp
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L150:
	.cfi_restore_state
	movq	-1(%rax), %rcx
	cmpw	$1025, 11(%rcx)
	jne	.L129
	movq	41112(%rdi), %rdi
	movq	31(%rax), %rsi
	testq	%rdi, %rdi
	je	.L131
	movl	%r9d, -176(%rbp)
	movq	%r8, -168(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-168(%rbp), %r8
	movl	-176(%rbp), %r9d
	movq	%rax, %rdx
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L135:
	movq	12480(%r12), %rax
	cmpq	%rax, 320(%r12)
	je	.L137
	cmpq	$0, -56(%rbp)
	je	.L138
	movq	%r13, %rdi
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, (%rdx)
	movl	-64(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L149
.L139:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate27OptionalRescheduleExceptionEb@PLT
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L137:
	movl	-64(%rbp), %eax
	testl	%eax, %eax
	je	.L141
	movq	%r13, %rdi
	call	_ZN2v88TryCatchD1Ev@PLT
.L142:
	leaq	37512(%r12), %rdi
	movl	$1, %esi
	call	_ZN2v88internal10StackGuard16RequestInterruptENS1_13InterruptFlagE@PLT
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L138:
	movl	-64(%rbp), %edx
	testl	%edx, %edx
	jne	.L149
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L131:
	movq	41088(%r12), %rdx
	cmpq	41096(%r12), %rdx
	je	.L152
.L133:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdx)
	jmp	.L129
.L152:
	movq	%r12, %rdi
	movl	%r9d, -180(%rbp)
	movq	%r8, -176(%rbp)
	movq	%rsi, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movl	-180(%rbp), %r9d
	movq	-176(%rbp), %r8
	movq	-168(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L141:
	movq	%r12, %rdi
	movl	$1, %esi
	call	_ZN2v88internal7Isolate27OptionalRescheduleExceptionEb@PLT
	movq	%r13, %rdi
	call	_ZN2v88TryCatchD1Ev@PLT
	jmp	.L142
.L151:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19070:
	.size	_ZN2v88internal9Execution7TryCallEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_iPS6_NS1_15MessageHandlingEPNS0_11MaybeHandleIS5_EE, .-_ZN2v88internal9Execution7TryCallEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_iPS6_NS1_15MessageHandlingEPNS0_11MaybeHandleIS5_EE
	.section	.text._ZN2v88internal9Execution16TryRunMicrotasksEPNS0_7IsolateEPNS0_14MicrotaskQueueEPNS0_11MaybeHandleINS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Execution16TryRunMicrotasksEPNS0_7IsolateEPNS0_14MicrotaskQueueEPNS0_11MaybeHandleINS0_6ObjectEEE
	.type	_ZN2v88internal9Execution16TryRunMicrotasksEPNS0_7IsolateEPNS0_14MicrotaskQueueEPNS0_11MaybeHandleINS0_6ObjectEEE, @function
_ZN2v88internal9Execution16TryRunMicrotasksEPNS0_7IsolateEPNS0_14MicrotaskQueueEPNS0_11MaybeHandleINS0_6ObjectEEE:
.LFB19071:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	88(%rdi), %rax
	movl	$0, -96(%rbp)
	movq	%rax, %xmm0
	movq	$0, -88(%rbp)
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, -80(%rbp)
	movq	%rsi, -72(%rbp)
	movl	$0, -64(%rbp)
	movq	%rdx, -56(%rbp)
	movb	$0, -48(%rbp)
	movl	$1, -44(%rbp)
	movaps	%xmm0, -112(%rbp)
	testq	%rdx, %rdx
	je	.L154
	movq	$0, (%rdx)
.L154:
	leaq	-160(%rbp), %r13
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88TryCatchC1EPNS_7IsolateE@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88TryCatch10SetVerboseEb@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88TryCatch17SetCaptureMessageEb@PLT
	leaq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_16InvokeEPNS0_7IsolateERKNS1_12InvokeParamsE
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L155
.L169:
	movq	%r13, %rdi
	call	_ZN2v88TryCatchD1Ev@PLT
.L156:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L170
	addq	$136, %rsp
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L155:
	.cfi_restore_state
	movq	12480(%r12), %rax
	cmpq	%rax, 320(%r12)
	je	.L157
	cmpq	$0, -56(%rbp)
	je	.L158
	movq	%r13, %rdi
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, (%rdx)
	movl	-64(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L169
.L159:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate27OptionalRescheduleExceptionEb@PLT
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L157:
	movl	-64(%rbp), %eax
	testl	%eax, %eax
	je	.L161
	movq	%r13, %rdi
	call	_ZN2v88TryCatchD1Ev@PLT
.L162:
	leaq	37512(%r12), %rdi
	movl	$1, %esi
	call	_ZN2v88internal10StackGuard16RequestInterruptENS1_13InterruptFlagE@PLT
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L158:
	movl	-64(%rbp), %edx
	testl	%edx, %edx
	jne	.L169
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L161:
	movq	%r12, %rdi
	movl	$1, %esi
	call	_ZN2v88internal7Isolate27OptionalRescheduleExceptionEb@PLT
	movq	%r13, %rdi
	call	_ZN2v88TryCatchD1Ev@PLT
	jmp	.L162
.L170:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19071:
	.size	_ZN2v88internal9Execution16TryRunMicrotasksEPNS0_7IsolateEPNS0_14MicrotaskQueueEPNS0_11MaybeHandleINS0_6ObjectEEE, .-_ZN2v88internal9Execution16TryRunMicrotasksEPNS0_7IsolateEPNS0_14MicrotaskQueueEPNS0_11MaybeHandleINS0_6ObjectEEE
	.section	.text._ZN2v88internal9Execution8CallWasmEPNS0_7IsolateENS0_6HandleINS0_4CodeEEEmNS4_INS0_6ObjectEEEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Execution8CallWasmEPNS0_7IsolateENS0_6HandleINS0_4CodeEEEmNS4_INS0_6ObjectEEEm
	.type	_ZN2v88internal9Execution8CallWasmEPNS0_7IsolateENS0_6HandleINS0_4CodeEEEmNS4_INS0_6ObjectEEEm, @function
_ZN2v88internal9Execution8CallWasmEPNS0_7IsolateENS0_6HandleINS0_4CodeEEEmNS4_INS0_6ObjectEEEm:
.LFB19072:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -160(%rbp)
	movq	%rcx, -168(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	%rax, -96(%rbp)
	movl	43(%rax), %edx
	leaq	63(%rax), %rbx
	movq	%rbx, -152(%rbp)
	testl	%edx, %edx
	js	.L194
.L173:
	leaq	-128(%rbp), %r14
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal11SaveContextC1EPNS0_7IsolateE@PLT
	movq	12600(%r15), %rbx
	movq	12560(%r15), %r12
	testq	%rbx, %rbx
	je	.L195
.L174:
	movq	12568(%r15), %rax
	cmpb	$0, _ZN2v88internal12trap_handler25g_is_trap_handler_enabledE(%rip)
	movq	$0, -136(%rbp)
	movq	_ZN2v88internal12trap_handler21g_thread_in_wasm_codeE@gottpoff(%rip), %r8
	movq	%rax, -144(%rbp)
	leaq	-144(%rbp), %rax
	movq	%rax, 12568(%r15)
	je	.L175
	movl	$1, %fs:(%r8)
.L175:
	pxor	%xmm0, %xmm0
	movq	$0, -64(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L196
.L176:
	movq	-168(%rbp), %rax
	movq	%r8, -176(%rbp)
	movq	%r12, %rcx
	movq	%r13, %rdx
	movq	-160(%rbp), %rdi
	movq	(%rax), %rsi
	movq	-152(%rbp), %rax
	call	*%rax
	movq	-176(%rbp), %r8
	testq	%rax, %rax
	je	.L177
	movq	%rax, 12480(%r15)
.L177:
	movq	-96(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L197
.L178:
	movl	%fs:(%r8), %eax
	testl	%eax, %eax
	je	.L179
	cmpb	$0, _ZN2v88internal12trap_handler25g_is_trap_handler_enabledE(%rip)
	jne	.L198
.L179:
	movq	-144(%rbp), %rax
	movq	%rax, 12568(%r15)
	testq	%rbx, %rbx
	jne	.L180
	movq	$0, 12600(%r15)
.L180:
	movq	%r12, 12560(%r15)
	movq	%r14, %rdi
	call	_ZN2v88internal11SaveContextD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L199
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L198:
	.cfi_restore_state
	movl	$0, %fs:(%r8)
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L195:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	movq	%rax, 12600(%r15)
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L194:
	leaq	-96(%rbp), %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	movq	%rax, -152(%rbp)
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L197:
	leaq	-88(%rbp), %rsi
	movq	%r8, -152(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	movq	-152(%rbp), %r8
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L196:
	movq	40960(%r15), %rax
	leaq	-88(%rbp), %rsi
	movl	$164, %edx
	movq	%r8, -176(%rbp)
	leaq	23240(%rax), %rdi
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	movq	-176(%rbp), %r8
	jmp	.L176
.L199:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19072:
	.size	_ZN2v88internal9Execution8CallWasmEPNS0_7IsolateENS0_6HandleINS0_4CodeEEEmNS4_INS0_6ObjectEEEm, .-_ZN2v88internal9Execution8CallWasmEPNS0_7IsolateENS0_6HandleINS0_4CodeEEEmNS4_INS0_6ObjectEEEm
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal9Execution4CallEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_iPS6_,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal9Execution4CallEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_iPS6_, @function
_GLOBAL__sub_I__ZN2v88internal9Execution4CallEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_iPS6_:
.LFB23423:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE23423:
	.size	_GLOBAL__sub_I__ZN2v88internal9Execution4CallEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_iPS6_, .-_GLOBAL__sub_I__ZN2v88internal9Execution4CallEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_iPS6_
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal9Execution4CallEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_iPS6_
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
