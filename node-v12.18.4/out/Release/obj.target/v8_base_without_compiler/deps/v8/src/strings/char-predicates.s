	.file	"char-predicates.cc"
	.text
	.section	.text._ZN2v88internal21IsIdentifierStartSlowEi,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal21IsIdentifierStartSlowEi
	.type	_ZN2v88internal21IsIdentifierStartSlowEi, @function
_ZN2v88internal21IsIdentifierStartSlowEi:
.LFB5009:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$16, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movl	%edi, %ebx
	subq	$8, %rsp
	call	u_hasBinaryProperty_67@PLT
	movl	%eax, %r8d
	movl	$1, %eax
	testb	%r8b, %r8b
	jne	.L1
	xorl	%eax, %eax
	cmpl	$95, %ebx
	jg	.L1
	leal	-36(%rbx), %ecx
	cmpl	$59, %ecx
	ja	.L1
	movabsq	$648518346341351425, %rax
	shrq	%cl, %rax
	andl	$1, %eax
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5009:
	.size	_ZN2v88internal21IsIdentifierStartSlowEi, .-_ZN2v88internal21IsIdentifierStartSlowEi
	.section	.text._ZN2v88internal20IsIdentifierPartSlowEi,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal20IsIdentifierPartSlowEi
	.type	_ZN2v88internal20IsIdentifierPartSlowEi, @function
_ZN2v88internal20IsIdentifierPartSlowEi:
.LFB5010:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$15, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movl	%edi, %ebx
	subq	$8, %rsp
	call	u_hasBinaryProperty_67@PLT
	movl	%eax, %r8d
	movl	$1, %eax
	testb	%r8b, %r8b
	jne	.L9
	cmpl	$95, %ebx
	jg	.L11
	leal	-36(%rbx), %edx
	cmpl	$59, %edx
	ja	.L11
	movabsq	$648518346341351425, %rcx
	btq	%rdx, %rcx
	jnc	.L11
.L9:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	subl	$8204, %ebx
	cmpl	$1, %ebx
	setbe	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5010:
	.size	_ZN2v88internal20IsIdentifierPartSlowEi, .-_ZN2v88internal20IsIdentifierPartSlowEi
	.section	.text._ZN2v88internal16IsWhiteSpaceSlowEi,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal16IsWhiteSpaceSlowEi
	.type	_ZN2v88internal16IsWhiteSpaceSlowEi, @function
_ZN2v88internal16IsWhiteSpaceSlowEi:
.LFB5011:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movl	%edi, %ebx
	subq	$8, %rsp
	call	u_charType_67@PLT
	movl	%eax, %r8d
	movl	$1, %eax
	cmpb	$12, %r8b
	je	.L16
	cmpl	$12, %ebx
	jg	.L18
	leal	-11(%rbx), %eax
	cmpl	$1, %eax
	setbe	%al
	cmpl	$9, %ebx
	sete	%dl
	orl	%edx, %eax
.L16:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore_state
	cmpl	$65279, %ebx
	sete	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5011:
	.size	_ZN2v88internal16IsWhiteSpaceSlowEi, .-_ZN2v88internal16IsWhiteSpaceSlowEi
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal21IsIdentifierStartSlowEi,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal21IsIdentifierStartSlowEi, @function
_GLOBAL__sub_I__ZN2v88internal21IsIdentifierStartSlowEi:
.LFB5777:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE5777:
	.size	_GLOBAL__sub_I__ZN2v88internal21IsIdentifierStartSlowEi, .-_GLOBAL__sub_I__ZN2v88internal21IsIdentifierStartSlowEi
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal21IsIdentifierStartSlowEi
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
