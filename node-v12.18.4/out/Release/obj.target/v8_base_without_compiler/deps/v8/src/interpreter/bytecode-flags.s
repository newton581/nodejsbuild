	.file	"bytecode-flags.cc"
	.text
	.section	.text._ZN2v88internal11interpreter23CreateArrayLiteralFlags6EncodeEbi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter23CreateArrayLiteralFlags6EncodeEbi
	.type	_ZN2v88internal11interpreter23CreateArrayLiteralFlags6EncodeEbi, @function
_ZN2v88internal11interpreter23CreateArrayLiteralFlags6EncodeEbi:
.LFB19065:
	.cfi_startproc
	endbr64
	sall	$5, %edi
	movl	%edi, %eax
	orl	%esi, %eax
	ret
	.cfi_endproc
.LFE19065:
	.size	_ZN2v88internal11interpreter23CreateArrayLiteralFlags6EncodeEbi, .-_ZN2v88internal11interpreter23CreateArrayLiteralFlags6EncodeEbi
	.section	.text._ZN2v88internal11interpreter24CreateObjectLiteralFlags6EncodeEib,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter24CreateObjectLiteralFlags6EncodeEib
	.type	_ZN2v88internal11interpreter24CreateObjectLiteralFlags6EncodeEib, @function
_ZN2v88internal11interpreter24CreateObjectLiteralFlags6EncodeEib:
.LFB19068:
	.cfi_startproc
	endbr64
	sall	$5, %esi
	movl	%esi, %eax
	orl	%edi, %eax
	ret
	.cfi_endproc
.LFE19068:
	.size	_ZN2v88internal11interpreter24CreateObjectLiteralFlags6EncodeEib, .-_ZN2v88internal11interpreter24CreateObjectLiteralFlags6EncodeEib
	.section	.text._ZN2v88internal11interpreter18CreateClosureFlags6EncodeEbbb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter18CreateClosureFlags6EncodeEbbb
	.type	_ZN2v88internal11interpreter18CreateClosureFlags6EncodeEbbb, @function
_ZN2v88internal11interpreter18CreateClosureFlags6EncodeEbbb:
.LFB19069:
	.cfi_startproc
	endbr64
	movl	%edi, %eax
	orl	%edx, %edi
	cmpb	$1, %dil
	je	.L4
	testb	%sil, %sil
	movl	$2, %edx
	cmovne	%edx, %eax
.L4:
	ret
	.cfi_endproc
.LFE19069:
	.size	_ZN2v88internal11interpreter18CreateClosureFlags6EncodeEbbb, .-_ZN2v88internal11interpreter18CreateClosureFlags6EncodeEbbb
	.section	.text._ZN2v88internal11interpreter15TestTypeOfFlags17GetFlagForLiteralEPKNS0_18AstStringConstantsEPNS0_7LiteralE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter15TestTypeOfFlags17GetFlagForLiteralEPKNS0_18AstStringConstantsEPNS0_7LiteralE
	.type	_ZN2v88internal11interpreter15TestTypeOfFlags17GetFlagForLiteralEPKNS0_18AstStringConstantsEPNS0_7LiteralE, @function
_ZN2v88internal11interpreter15TestTypeOfFlags17GetFlagForLiteralEPKNS0_18AstStringConstantsEPNS0_7LiteralE:
.LFB19072:
	.cfi_startproc
	endbr64
	movq	8(%rsi), %rax
	cmpq	%rax, 376(%rdi)
	je	.L11
	cmpq	%rax, 448(%rdi)
	je	.L12
	cmpq	%rax, 456(%rdi)
	je	.L13
	cmpq	%rax, 152(%rdi)
	je	.L14
	cmpq	%rax, 144(%rdi)
	je	.L15
	cmpq	%rax, 496(%rdi)
	je	.L16
	cmpq	%rax, 296(%rdi)
	je	.L17
	cmpq	%rax, 384(%rdi)
	setne	%al
	addl	$7, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	movl	$4, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movl	$2, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movl	$3, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	movl	$5, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	movl	$6, %eax
	ret
	.cfi_endproc
.LFE19072:
	.size	_ZN2v88internal11interpreter15TestTypeOfFlags17GetFlagForLiteralEPKNS0_18AstStringConstantsEPNS0_7LiteralE, .-_ZN2v88internal11interpreter15TestTypeOfFlags17GetFlagForLiteralEPKNS0_18AstStringConstantsEPNS0_7LiteralE
	.section	.text._ZN2v88internal11interpreter15TestTypeOfFlags6EncodeENS2_11LiteralFlagE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter15TestTypeOfFlags6EncodeENS2_11LiteralFlagE
	.type	_ZN2v88internal11interpreter15TestTypeOfFlags6EncodeENS2_11LiteralFlagE, @function
_ZN2v88internal11interpreter15TestTypeOfFlags6EncodeENS2_11LiteralFlagE:
.LFB19073:
	.cfi_startproc
	endbr64
	movl	%edi, %eax
	ret
	.cfi_endproc
.LFE19073:
	.size	_ZN2v88internal11interpreter15TestTypeOfFlags6EncodeENS2_11LiteralFlagE, .-_ZN2v88internal11interpreter15TestTypeOfFlags6EncodeENS2_11LiteralFlagE
	.section	.text._ZN2v88internal11interpreter15TestTypeOfFlags6DecodeEh,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter15TestTypeOfFlags6DecodeEh
	.type	_ZN2v88internal11interpreter15TestTypeOfFlags6DecodeEh, @function
_ZN2v88internal11interpreter15TestTypeOfFlags6DecodeEh:
.LFB19074:
	.cfi_startproc
	endbr64
	movl	%edi, %eax
	ret
	.cfi_endproc
.LFE19074:
	.size	_ZN2v88internal11interpreter15TestTypeOfFlags6DecodeEh, .-_ZN2v88internal11interpreter15TestTypeOfFlags6DecodeEh
	.section	.text._ZN2v88internal11interpreter20StoreLookupSlotFlags6EncodeENS0_12LanguageModeENS0_18LookupHoistingModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20StoreLookupSlotFlags6EncodeENS0_12LanguageModeENS0_18LookupHoistingModeE
	.type	_ZN2v88internal11interpreter20StoreLookupSlotFlags6EncodeENS0_12LanguageModeENS0_18LookupHoistingModeE, @function
_ZN2v88internal11interpreter20StoreLookupSlotFlags6EncodeENS0_12LanguageModeENS0_18LookupHoistingModeE:
.LFB19075:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	setne	%dl
	leal	(%rdx,%rdx), %eax
	orl	%edi, %eax
	ret
	.cfi_endproc
.LFE19075:
	.size	_ZN2v88internal11interpreter20StoreLookupSlotFlags6EncodeENS0_12LanguageModeENS0_18LookupHoistingModeE, .-_ZN2v88internal11interpreter20StoreLookupSlotFlags6EncodeENS0_12LanguageModeENS0_18LookupHoistingModeE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal11interpreter23CreateArrayLiteralFlags6EncodeEbi,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal11interpreter23CreateArrayLiteralFlags6EncodeEbi, @function
_GLOBAL__sub_I__ZN2v88internal11interpreter23CreateArrayLiteralFlags6EncodeEbi:
.LFB23267:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE23267:
	.size	_GLOBAL__sub_I__ZN2v88internal11interpreter23CreateArrayLiteralFlags6EncodeEbi, .-_GLOBAL__sub_I__ZN2v88internal11interpreter23CreateArrayLiteralFlags6EncodeEbi
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal11interpreter23CreateArrayLiteralFlags6EncodeEbi
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
