	.file	"code.cc"
	.text
	.section	.text._ZNKSt5ctypeIcE8do_widenEc,"axG",@progbits,_ZNKSt5ctypeIcE8do_widenEc,comdat
	.align 2
	.p2align 4
	.weak	_ZNKSt5ctypeIcE8do_widenEc
	.type	_ZNKSt5ctypeIcE8do_widenEc, @function
_ZNKSt5ctypeIcE8do_widenEc:
.LFB1338:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	ret
	.cfi_endproc
.LFE1338:
	.size	_ZNKSt5ctypeIcE8do_widenEc, .-_ZNKSt5ctypeIcE8do_widenEc
	.section	.text._ZN2v88internal8OFStreamD1Ev,"axG",@progbits,_ZN2v88internal8OFStreamD1Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8OFStreamD1Ev
	.type	_ZN2v88internal8OFStreamD1Ev, @function
_ZN2v88internal8OFStreamD1Ev:
.LFB19587:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE19587:
	.size	_ZN2v88internal8OFStreamD1Ev, .-_ZN2v88internal8OFStreamD1Ev
	.section	.text._ZN2v88internal8OFStreamD0Ev,"axG",@progbits,_ZN2v88internal8OFStreamD0Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal8OFStreamD0Ev
	.type	_ZTv0_n24_N2v88internal8OFStreamD0Ev, @function
_ZTv0_n24_N2v88internal8OFStreamD0Ev:
.LFB24039:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %rax
	addq	-24(%rax), %rdi
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	movq	%rax, 80(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE24039:
	.size	_ZTv0_n24_N2v88internal8OFStreamD0Ev, .-_ZTv0_n24_N2v88internal8OFStreamD0Ev
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8OFStreamD0Ev
	.type	_ZN2v88internal8OFStreamD0Ev, @function
_ZN2v88internal8OFStreamD0Ev:
.LFB19588:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE19588:
	.size	_ZN2v88internal8OFStreamD0Ev, .-_ZN2v88internal8OFStreamD0Ev
	.section	.text._ZN2v88internal8OFStreamD1Ev,"axG",@progbits,_ZN2v88internal8OFStreamD1Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal8OFStreamD1Ev
	.type	_ZTv0_n24_N2v88internal8OFStreamD1Ev, @function
_ZTv0_n24_N2v88internal8OFStreamD1Ev:
.LFB24040:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	-24(%rax), %rbx
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	addq	%rdi, %rbx
	movq	%rax, 80(%rbx)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	64(%rbx), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE24040:
	.size	_ZTv0_n24_N2v88internal8OFStreamD1Ev, .-_ZTv0_n24_N2v88internal8OFStreamD1Ev
	.section	.text._ZNK2v88internal4Code20safepoint_table_sizeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4Code20safepoint_table_sizeEv
	.type	_ZNK2v88internal4Code20safepoint_table_sizeEv, @function
_ZNK2v88internal4Code20safepoint_table_sizeEv:
.LFB19672:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movl	51(%rdx), %eax
	subl	47(%rdx), %eax
	ret
	.cfi_endproc
.LFE19672:
	.size	_ZNK2v88internal4Code20safepoint_table_sizeEv, .-_ZNK2v88internal4Code20safepoint_table_sizeEv
	.section	.text._ZNK2v88internal4Code19has_safepoint_tableEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4Code19has_safepoint_tableEv
	.type	_ZNK2v88internal4Code19has_safepoint_tableEv, @function
_ZNK2v88internal4Code19has_safepoint_tableEv:
.LFB19673:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movl	51(%rdx), %eax
	subl	47(%rdx), %eax
	testl	%eax, %eax
	setg	%al
	ret
	.cfi_endproc
.LFE19673:
	.size	_ZNK2v88internal4Code19has_safepoint_tableEv, .-_ZNK2v88internal4Code19has_safepoint_tableEv
	.section	.text._ZNK2v88internal4Code18handler_table_sizeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4Code18handler_table_sizeEv
	.type	_ZNK2v88internal4Code18handler_table_sizeEv, @function
_ZNK2v88internal4Code18handler_table_sizeEv:
.LFB19674:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movl	55(%rdx), %eax
	subl	51(%rdx), %eax
	ret
	.cfi_endproc
.LFE19674:
	.size	_ZNK2v88internal4Code18handler_table_sizeEv, .-_ZNK2v88internal4Code18handler_table_sizeEv
	.section	.text._ZNK2v88internal4Code17has_handler_tableEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4Code17has_handler_tableEv
	.type	_ZNK2v88internal4Code17has_handler_tableEv, @function
_ZNK2v88internal4Code17has_handler_tableEv:
.LFB19675:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movl	55(%rdx), %eax
	subl	51(%rdx), %eax
	testl	%eax, %eax
	setg	%al
	ret
	.cfi_endproc
.LFE19675:
	.size	_ZNK2v88internal4Code17has_handler_tableEv, .-_ZNK2v88internal4Code17has_handler_tableEv
	.section	.text._ZNK2v88internal4Code18constant_pool_sizeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4Code18constant_pool_sizeEv
	.type	_ZNK2v88internal4Code18constant_pool_sizeEv, @function
_ZNK2v88internal4Code18constant_pool_sizeEv:
.LFB19676:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE19676:
	.size	_ZNK2v88internal4Code18constant_pool_sizeEv, .-_ZNK2v88internal4Code18constant_pool_sizeEv
	.section	.text._ZNK2v88internal4Code17has_constant_poolEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4Code17has_constant_poolEv
	.type	_ZNK2v88internal4Code17has_constant_poolEv, @function
_ZNK2v88internal4Code17has_constant_poolEv:
.LFB19677:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE19677:
	.size	_ZNK2v88internal4Code17has_constant_poolEv, .-_ZNK2v88internal4Code17has_constant_poolEv
	.section	.text._ZNK2v88internal4Code18code_comments_sizeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4Code18code_comments_sizeEv
	.type	_ZNK2v88internal4Code18code_comments_sizeEv, @function
_ZNK2v88internal4Code18code_comments_sizeEv:
.LFB19678:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	43(%rdx), %eax
	testl	%eax, %eax
	js	.L23
	movl	39(%rdx), %eax
.L20:
	subl	55(%rdx), %eax
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L24
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	.cfi_restore_state
	movq	%rdi, %rbx
	call	_ZN2v88internal7Isolate19CurrentEmbeddedBlobEv@PLT
	testq	%rax, %rax
	je	.L25
	call	_ZN2v88internal7Isolate23CurrentEmbeddedBlobSizeEv@PLT
	movl	%eax, %r12d
	call	_ZN2v88internal7Isolate19CurrentEmbeddedBlobEv@PLT
	movl	%r12d, -40(%rbp)
	leaq	-48(%rbp), %rdi
	movq	%rax, -48(%rbp)
	movq	(%rbx), %rax
	movl	59(%rax), %esi
	call	_ZNK2v88internal12EmbeddedData24InstructionSizeOfBuiltinEi@PLT
	movq	(%rbx), %rdx
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L25:
	movq	(%rbx), %rdx
	movl	39(%rdx), %eax
	jmp	.L20
.L24:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19678:
	.size	_ZNK2v88internal4Code18code_comments_sizeEv, .-_ZNK2v88internal4Code18code_comments_sizeEv
	.section	.text._ZNK2v88internal4Code17has_code_commentsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4Code17has_code_commentsEv
	.type	_ZNK2v88internal4Code17has_code_commentsEv, @function
_ZNK2v88internal4Code17has_code_commentsEv:
.LFB19679:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	43(%rdx), %eax
	testl	%eax, %eax
	js	.L32
	movl	39(%rdx), %eax
.L29:
	subl	55(%rdx), %eax
	testl	%eax, %eax
	setg	%al
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L33
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	movq	%rdi, %rbx
	call	_ZN2v88internal7Isolate19CurrentEmbeddedBlobEv@PLT
	testq	%rax, %rax
	je	.L34
	call	_ZN2v88internal7Isolate23CurrentEmbeddedBlobSizeEv@PLT
	movl	%eax, %r12d
	call	_ZN2v88internal7Isolate19CurrentEmbeddedBlobEv@PLT
	movl	%r12d, -40(%rbp)
	leaq	-48(%rbp), %rdi
	movq	%rax, -48(%rbp)
	movq	(%rbx), %rax
	movl	59(%rax), %esi
	call	_ZNK2v88internal12EmbeddedData24InstructionSizeOfBuiltinEi@PLT
	movq	(%rbx), %rdx
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L34:
	movq	(%rbx), %rdx
	movl	39(%rdx), %eax
	jmp	.L29
.L33:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19679:
	.size	_ZNK2v88internal4Code17has_code_commentsEv, .-_ZNK2v88internal4Code17has_code_commentsEv
	.section	.text._ZNK2v88internal4Code25ExecutableInstructionSizeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4Code25ExecutableInstructionSizeEv
	.type	_ZNK2v88internal4Code25ExecutableInstructionSizeEv, @function
_ZNK2v88internal4Code25ExecutableInstructionSizeEv:
.LFB19680:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movl	47(%rax), %eax
	ret
	.cfi_endproc
.LFE19680:
	.size	_ZNK2v88internal4Code25ExecutableInstructionSizeEv, .-_ZNK2v88internal4Code25ExecutableInstructionSizeEv
	.section	.text._ZN2v88internal4Code20ClearEmbeddedObjectsEPNS0_4HeapE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4Code20ClearEmbeddedObjectsEPNS0_4HeapE
	.type	_ZN2v88internal4Code20ClearEmbeddedObjectsEPNS0_4HeapE, @function
_ZN2v88internal4Code20ClearEmbeddedObjectsEPNS0_4HeapE:
.LFB19681:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$12, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	leaq	-112(%rbp), %rbx
	subq	$88, %rsp
	movq	-37504(%rsi), %r12
	movq	(%rdi), %rsi
	movq	%rbx, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal13RelocIteratorC1ENS0_4CodeEi@PLT
	cmpb	$0, -56(%rbp)
	jne	.L37
	.p2align 4,,10
	.p2align 3
.L38:
	movq	-96(%rbp), %rax
	movl	$8, %esi
	movq	%r12, (%rax)
	movq	-96(%rbp), %rdi
	call	_ZN2v88internal21FlushInstructionCacheEPvm@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal13RelocIterator4nextEv@PLT
	cmpb	$0, -56(%rbp)
	je	.L38
.L37:
	movq	0(%r13), %rax
	movq	31(%rax), %rax
	movl	15(%rax), %eax
	movq	0(%r13), %rdx
	movq	31(%rdx), %rdx
	orl	$2, %eax
	movl	%eax, 15(%rdx)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L42
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L42:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19681:
	.size	_ZN2v88internal4Code20ClearEmbeddedObjectsEPNS0_4HeapE, .-_ZN2v88internal4Code20ClearEmbeddedObjectsEPNS0_4HeapE
	.section	.text._ZN2v88internal4Code8RelocateEl,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4Code8RelocateEl
	.type	_ZN2v88internal4Code8RelocateEl, @function
_ZN2v88internal4Code8RelocateEl:
.LFB19682:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	leaq	-112(%rbp), %rbx
	subq	$88, %rsp
	movq	(%rdi), %rsi
	movl	_ZN2v88internal9RelocInfo10kApplyMaskE(%rip), %edx
	movq	%rbx, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal13RelocIteratorC1ENS0_4CodeEi@PLT
	cmpb	$0, -56(%rbp)
	je	.L48
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L54:
	testb	%al, %al
	je	.L50
	cmpb	$8, %al
	je	.L53
.L47:
	movq	%rbx, %rdi
	call	_ZN2v88internal13RelocIterator4nextEv@PLT
	cmpb	$0, -56(%rbp)
	jne	.L44
.L48:
	movzbl	-88(%rbp), %eax
	cmpb	$6, %al
	jne	.L54
.L50:
	movq	-96(%rbp), %rax
	movq	%rbx, %rdi
	subl	%r12d, (%rax)
	call	_ZN2v88internal13RelocIterator4nextEv@PLT
	cmpb	$0, -56(%rbp)
	je	.L48
.L44:
	movq	0(%r13), %rdi
	movslq	39(%rdi), %rsi
	addq	$63, %rdi
	call	_ZN2v88internal21FlushInstructionCacheEPvm@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L55
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore_state
	movq	-96(%rbp), %rax
	addq	%r12, (%rax)
	jmp	.L47
.L55:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19682:
	.size	_ZN2v88internal4Code8RelocateEl, .-_ZN2v88internal4Code8RelocateEl
	.section	.text._ZNK2v88internal4Code11FlushICacheEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4Code11FlushICacheEv
	.type	_ZNK2v88internal4Code11FlushICacheEv, @function
_ZNK2v88internal4Code11FlushICacheEv:
.LFB19683:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	movslq	39(%rdi), %rsi
	addq	$63, %rdi
	jmp	_ZN2v88internal21FlushInstructionCacheEPvm@PLT
	.cfi_endproc
.LFE19683:
	.size	_ZNK2v88internal4Code11FlushICacheEv, .-_ZNK2v88internal4Code11FlushICacheEv
	.section	.text._ZN2v88internal4Code15CopyFromNoFlushEPNS0_4HeapERKNS0_8CodeDescE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4Code15CopyFromNoFlushEPNS0_4HeapERKNS0_8CodeDescE
	.type	_ZN2v88internal4Code15CopyFromNoFlushEPNS0_4HeapERKNS0_8CodeDescE, @function
_ZN2v88internal4Code15CopyFromNoFlushEPNS0_4HeapERKNS0_8CodeDescE:
.LFB19684:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$120, %rsp
	movslq	12(%rdx), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	testq	%rdx, %rdx
	jne	.L103
.L58:
	cmpq	$0, 56(%rbx)
	je	.L101
	movl	39(%rax), %ecx
	leal	71(%rcx), %edx
	movslq	64(%rbx), %rcx
	andl	$-8, %edx
	movslq	%edx, %rdx
	movq	%rcx, -1(%rax,%rdx)
	movq	0(%r13), %rcx
	movslq	64(%rbx), %rdx
	leaq	7(%rcx), %rax
	testq	%rdx, %rdx
	jne	.L104
	movslq	52(%rbx), %rdx
	testq	%rdx, %rdx
	jne	.L105
.L66:
	movq	0(%r13), %rsi
	leaq	-128(%rbp), %r12
	movq	72(%rbx), %r15
	leaq	-112(%rbp), %r14
	movl	_ZN2v88internal9RelocInfo10kApplyMaskE(%rip), %edx
	movq	%r12, %rdi
	orl	$79, %edx
	call	_ZN2v88internal13RelocIteratorC1ENS0_4CodeEi@PLT
	cmpb	$0, -72(%rbp)
	je	.L70
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L77:
	cmpb	$6, %al
	je	.L106
	cmpb	$8, %al
	je	.L107
.L73:
	movq	%r12, %rdi
	call	_ZN2v88internal13RelocIterator4nextEv@PLT
	cmpb	$0, -72(%rbp)
	jne	.L57
.L70:
	movzbl	-104(%rbp), %eax
	leal	-2(%rax), %edx
	cmpb	$1, %dl
	jbe	.L108
	cmpb	$1, %al
	jg	.L77
	movq	-112(%rbp), %rdx
	testb	%al, %al
	je	.L109
	movq	(%rdx), %rax
.L79:
	movq	(%rax), %rsi
	movl	$1, %ecx
	movl	$4, %edx
	movq	%r14, %rdi
	addq	$63, %rsi
	call	_ZN2v88internal9RelocInfo18set_target_addressEmNS0_16WriteBarrierModeENS0_15ICacheFlushModeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal13RelocIterator4nextEv@PLT
	cmpb	$0, -72(%rbp)
	je	.L70
.L57:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L110
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L108:
	.cfi_restore_state
	movq	-112(%rbp), %rax
	movq	(%rax), %rdx
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L73
	testb	$1, %dl
	je	.L73
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -136(%rbp)
	testb	$24, %al
	je	.L75
	movq	%r14, %rsi
	movq	%rdx, -152(%rbp)
	movq	%rdi, -144(%rbp)
	call	_ZN2v88internal35Heap_GenerationalBarrierForCodeSlowENS0_4CodeEPNS0_9RelocInfoENS0_10HeapObjectE@PLT
	movq	-136(%rbp), %rcx
	movq	-152(%rbp), %rdx
	movq	-144(%rbp), %rdi
	movq	8(%rcx), %rax
.L75:
	testl	$262144, %eax
	je	.L73
	movq	%r14, %rsi
	call	_ZN2v88internal30Heap_MarkingBarrierForCodeSlowENS0_4CodeEPNS0_9RelocInfoENS0_10HeapObjectE@PLT
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L106:
	movq	-112(%rbp), %rax
	movq	184(%r15), %rsi
	leaq	4(%rax), %rdx
	cmpq	%rdx, %rsi
	je	.L73
	movslq	(%rax), %rax
	movl	$1, %ecx
	movl	$4, %edx
	movq	%r14, %rdi
	addq	%rax, %rsi
	call	_ZN2v88internal9RelocInfo18set_target_addressEmNS0_16WriteBarrierModeENS0_15ICacheFlushModeE@PLT
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L109:
	movslq	(%rdx), %rsi
	movq	%r15, %rdi
	call	_ZNK2v88internal13AssemblerBase13GetCodeTargetEl@PLT
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L107:
	movq	-112(%rbp), %rdx
	movq	0(%r13), %rax
	movq	(%rdx), %rcx
	leaq	63(%rcx,%rax), %rax
	subq	(%rbx), %rax
	movq	%rax, (%rdx)
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L63:
	call	memcpy@PLT
.L102:
	movq	0(%r13), %rax
.L101:
	movslq	52(%rbx), %rdx
	addq	$7, %rax
	testq	%rdx, %rdx
	je	.L66
.L105:
	movq	(%rax), %rax
	movslq	8(%rbx), %rsi
	leaq	15(%rax), %rdi
	subq	%rdx, %rsi
	addq	(%rbx), %rsi
	cmpq	$7, %rdx
	ja	.L67
	leaq	15(%rax,%rdx), %rcx
	subq	%rax, %rsi
.L69:
	movq	%rdi, %rax
	movzbl	-15(%rdi,%rsi), %edx
	addq	$1, %rdi
	movb	%dl, (%rax)
	cmpq	%rcx, %rdi
	jne	.L69
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L104:
	movl	39(%rcx), %edi
	movq	56(%rbx), %rsi
	addl	$71, %edi
	andl	$-8, %edi
	movslq	%edi, %rdi
	addq	%rax, %rdi
	movq	%rdi, %rax
	cmpq	$7, %rdx
	ja	.L63
	addq	%rdi, %rdx
	subq	%rdi, %rsi
.L64:
	movq	%rax, %rcx
	movzbl	(%rax,%rsi), %edi
	addq	$1, %rax
	movb	%dil, (%rcx)
	cmpq	%rdx, %rax
	jne	.L64
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L103:
	leaq	63(%rax), %rdi
	movq	(%rbx), %rsi
	movq	%rdi, %rcx
	cmpq	$7, %rdx
	ja	.L59
	addq	%rdi, %rdx
	subq	%rax, %rsi
.L60:
	movq	%rcx, %rax
	movzbl	-63(%rcx,%rsi), %edi
	addq	$1, %rcx
	movb	%dil, (%rax)
	cmpq	%rdx, %rcx
	jne	.L60
	movq	0(%r13), %rax
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L67:
	call	memcpy@PLT
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L59:
	call	memcpy@PLT
	movq	0(%r13), %rax
	jmp	.L58
.L110:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19684:
	.size	_ZN2v88internal4Code15CopyFromNoFlushEPNS0_4HeapERKNS0_8CodeDescE, .-_ZN2v88internal4Code15CopyFromNoFlushEPNS0_4HeapERKNS0_8CodeDescE
	.section	.text._ZN2v88internal4Code17GetSafepointEntryEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4Code17GetSafepointEntryEm
	.type	_ZN2v88internal4Code17GetSafepointEntryEm, @function
_ZN2v88internal4Code17GetSafepointEntryEm:
.LFB19685:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-96(%rbp), %r14
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r14, %rdi
	subq	$72, %rsp
	movq	(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal14SafepointTableC1ENS0_4CodeE@PLT
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNK2v88internal14SafepointTable9FindEntryEm@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L114
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L114:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19685:
	.size	_ZN2v88internal4Code17GetSafepointEntryEm, .-_ZN2v88internal4Code17GetSafepointEntryEm
	.section	.text._ZNK2v88internal4Code22OffHeapInstructionSizeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4Code22OffHeapInstructionSizeEv
	.type	_ZNK2v88internal4Code22OffHeapInstructionSizeEv, @function
_ZNK2v88internal4Code22OffHeapInstructionSizeEv:
.LFB19686:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal7Isolate19CurrentEmbeddedBlobEv@PLT
	testq	%rax, %rax
	je	.L120
	call	_ZN2v88internal7Isolate23CurrentEmbeddedBlobSizeEv@PLT
	movl	%eax, %r12d
	call	_ZN2v88internal7Isolate19CurrentEmbeddedBlobEv@PLT
	movl	%r12d, -40(%rbp)
	leaq	-48(%rbp), %rdi
	movq	%rax, -48(%rbp)
	movq	(%rbx), %rax
	movl	59(%rax), %esi
	call	_ZNK2v88internal12EmbeddedData24InstructionSizeOfBuiltinEi@PLT
.L115:
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L121
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L120:
	.cfi_restore_state
	movq	(%rbx), %rax
	movl	39(%rax), %eax
	jmp	.L115
.L121:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19686:
	.size	_ZNK2v88internal4Code22OffHeapInstructionSizeEv, .-_ZNK2v88internal4Code22OffHeapInstructionSizeEv
	.section	.text._ZNK2v88internal4Code23OffHeapInstructionStartEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4Code23OffHeapInstructionStartEv
	.type	_ZNK2v88internal4Code23OffHeapInstructionStartEv, @function
_ZNK2v88internal4Code23OffHeapInstructionStartEv:
.LFB19687:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal7Isolate19CurrentEmbeddedBlobEv@PLT
	testq	%rax, %rax
	je	.L127
	call	_ZN2v88internal7Isolate23CurrentEmbeddedBlobSizeEv@PLT
	movl	%eax, %r12d
	call	_ZN2v88internal7Isolate19CurrentEmbeddedBlobEv@PLT
	movl	%r12d, -40(%rbp)
	leaq	-48(%rbp), %rdi
	movq	%rax, -48(%rbp)
	movq	(%rbx), %rax
	movl	59(%rax), %esi
	call	_ZNK2v88internal12EmbeddedData25InstructionStartOfBuiltinEi@PLT
.L122:
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L128
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L127:
	.cfi_restore_state
	movq	(%rbx), %rax
	addq	$63, %rax
	jmp	.L122
.L128:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19687:
	.size	_ZNK2v88internal4Code23OffHeapInstructionStartEv, .-_ZNK2v88internal4Code23OffHeapInstructionStartEv
	.section	.text._ZNK2v88internal4Code21OffHeapInstructionEndEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4Code21OffHeapInstructionEndEv
	.type	_ZNK2v88internal4Code21OffHeapInstructionEndEv, @function
_ZNK2v88internal4Code21OffHeapInstructionEndEv:
.LFB19688:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal7Isolate19CurrentEmbeddedBlobEv@PLT
	testq	%rax, %rax
	je	.L134
	call	_ZN2v88internal7Isolate23CurrentEmbeddedBlobSizeEv@PLT
	leaq	-64(%rbp), %r13
	movl	%eax, %r12d
	call	_ZN2v88internal7Isolate19CurrentEmbeddedBlobEv@PLT
	movl	%r12d, -56(%rbp)
	movq	%r13, %rdi
	movq	%rax, -64(%rbp)
	movq	(%rbx), %rax
	movl	59(%rax), %esi
	call	_ZNK2v88internal12EmbeddedData25InstructionStartOfBuiltinEi@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	movq	(%rbx), %rax
	movl	59(%rax), %esi
	call	_ZNK2v88internal12EmbeddedData24InstructionSizeOfBuiltinEi@PLT
	movl	%eax, %eax
	addq	%r12, %rax
.L129:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L135
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L134:
	.cfi_restore_state
	movq	(%rbx), %rax
	movslq	39(%rax), %rdx
	leaq	63(%rax,%rdx), %rax
	jmp	.L129
.L135:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19688:
	.size	_ZNK2v88internal4Code21OffHeapInstructionEndEv, .-_ZNK2v88internal4Code21OffHeapInstructionEndEv
	.section	.text._ZN2v88internal12AbstractCode18SetStackFrameCacheENS0_6HandleIS1_EENS2_INS0_22SimpleNumberDictionaryEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12AbstractCode18SetStackFrameCacheENS0_6HandleIS1_EENS2_INS0_22SimpleNumberDictionaryEEE
	.type	_ZN2v88internal12AbstractCode18SetStackFrameCacheENS0_6HandleIS1_EENS2_INS0_22SimpleNumberDictionaryEEE, @function
_ZN2v88internal12AbstractCode18SetStackFrameCacheENS0_6HandleIS1_EENS2_INS0_22SimpleNumberDictionaryEEE:
.LFB19690:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r15
	movq	-1(%r15), %rax
	movq	%r15, %rsi
	cmpw	$69, 11(%rax)
	movq	%r15, %rax
	je	.L221
	andq	$-262144, %rax
	movq	24(%rax), %r12
	movq	3520(%r12), %rdi
	subq	$37592, %r12
	testq	%rdi, %rdi
	je	.L158
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rbx
.L159:
	movq	(%r14), %rax
	movq	31(%rsi), %rsi
	andq	$-262144, %rax
	movq	24(%rax), %r12
	movq	3520(%r12), %rdi
	subq	$37592, %r12
	testq	%rdi, %rdi
	je	.L161
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r8
.L162:
	cmpq	%rsi, 312(%r12)
	je	.L136
	testb	$1, %sil
	jne	.L222
.L166:
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal7Factory36NewSourcePositionTableWithFrameCacheENS0_6HandleINS0_9ByteArrayEEENS2_INS0_22SimpleNumberDictionaryEEE@PLT
	movq	(%rbx), %r13
	movq	(%rax), %r12
	leaq	31(%r13), %r14
	movq	%r12, 31(%r13)
	testb	$1, %r12b
	je	.L136
.L216:
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L223
.L174:
	testb	$24, %al
	je	.L136
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L224
.L136:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L158:
	.cfi_restore_state
	movq	41088(%r12), %rbx
	cmpq	41096(%r12), %rbx
	je	.L225
.L160:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r12)
	movq	%r15, (%rbx)
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L161:
	movq	41088(%r12), %r8
	cmpq	41096(%r12), %r8
	je	.L226
.L163:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r8)
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L222:
	movq	%rsi, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	cmpq	%rsi, -37504(%rax)
	je	.L136
	movq	-1(%rsi), %rax
	cmpw	$97, 11(%rax)
	jne	.L166
.L220:
	movq	(%r8), %r14
	movq	0(%r13), %r12
	movq	%r12, 15(%r14)
	leaq	15(%r14), %r13
	testb	$1, %r12b
	je	.L136
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L171
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
.L171:
	testb	$24, %al
	je	.L136
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L136
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L221:
	andq	$-262144, %rax
	movq	24(%rax), %rbx
	movq	3520(%rbx), %rdi
	subq	$37592, %rbx
	testq	%rdi, %rdi
	je	.L138
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rcx
.L139:
	movq	(%r14), %rax
	movq	23(%rsi), %rsi
	andq	$-262144, %rax
	movq	24(%rax), %r12
	movq	3520(%r12), %rdi
	subq	$37592, %r12
	testq	%rdi, %rdi
	je	.L141
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-56(%rbp), %rcx
	movq	(%rax), %rsi
	movq	%rax, %r8
.L142:
	cmpq	%rsi, 312(%r12)
	je	.L136
	testb	$1, %sil
	jne	.L227
.L146:
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%r8, %rsi
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal7Factory36NewSourcePositionTableWithFrameCacheENS0_6HandleINS0_9ByteArrayEEENS2_INS0_22SimpleNumberDictionaryEEE@PLT
	movq	-56(%rbp), %rcx
	movq	(%rax), %r12
	movq	(%rcx), %r13
	movq	%r12, 23(%r13)
	leaq	23(%r13), %r14
	testb	$1, %r12b
	jne	.L216
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L223:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L138:
	movq	41088(%rbx), %rcx
	cmpq	41096(%rbx), %rcx
	je	.L228
.L140:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rbx)
	movq	%r15, (%rcx)
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L224:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
.L206:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L141:
	.cfi_restore_state
	movq	41088(%r12), %r8
	cmpq	41096(%r12), %r8
	je	.L229
.L143:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r8)
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L226:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L225:
	movq	%r12, %rdi
	movq	%r15, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L227:
	movq	%rsi, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	cmpq	%rsi, -37504(%rax)
	je	.L136
	movq	-1(%rsi), %rax
	cmpw	$97, 11(%rax)
	jne	.L146
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L229:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rcx
	movq	%rax, %r8
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L228:
	movq	%rbx, %rdi
	movq	%r15, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L140
	.cfi_endproc
.LFE19690:
	.size	_ZN2v88internal12AbstractCode18SetStackFrameCacheENS0_6HandleIS1_EENS2_INS0_22SimpleNumberDictionaryEEE, .-_ZN2v88internal12AbstractCode18SetStackFrameCacheENS0_6HandleIS1_EENS2_INS0_22SimpleNumberDictionaryEEE
	.section	.text._ZN2v88internal12AbstractCode19DropStackFrameCacheEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12AbstractCode19DropStackFrameCacheEv
	.type	_ZN2v88internal12AbstractCode19DropStackFrameCacheEv, @function
_ZN2v88internal12AbstractCode19DropStackFrameCacheEv:
.LFB19692:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rdi), %r12
	movq	-1(%r12), %rax
	cmpw	$69, 11(%rax)
	je	.L270
	movq	31(%r12), %rax
	leaq	31(%r12), %r14
	testb	$1, %al
	jne	.L271
.L238:
	movq	7(%rax), %r13
	movq	%r13, 31(%r12)
	testb	$1, %r13b
	je	.L230
.L269:
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L242
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
.L242:
	testb	$24, %al
	je	.L230
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L272
.L230:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L271:
	.cfi_restore_state
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	-37504(%rdx), %rax
	je	.L230
	movq	-1(%rax), %rcx
	cmpw	$70, 11(%rcx)
	je	.L230
	cmpq	-37280(%rdx), %rax
	je	.L230
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L270:
	movq	23(%r12), %rax
	leaq	23(%r12), %r14
	testb	$1, %al
	jne	.L273
.L232:
	movq	7(%rax), %r13
	movq	%r13, 23(%r12)
	testb	$1, %r13b
	jne	.L269
	jmp	.L230
	.p2align 4,,10
	.p2align 3
.L272:
	popq	%rbx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L273:
	.cfi_restore_state
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	-37504(%rdx), %rax
	je	.L230
	movq	-1(%rax), %rcx
	cmpw	$70, 11(%rcx)
	je	.L230
	cmpq	-37280(%rdx), %rax
	je	.L230
	jmp	.L232
	.cfi_endproc
.LFE19692:
	.size	_ZN2v88internal12AbstractCode19DropStackFrameCacheEv, .-_ZN2v88internal12AbstractCode19DropStackFrameCacheEv
	.section	.text._ZN2v88internal12AbstractCode14SourcePositionEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12AbstractCode14SourcePositionEi
	.type	_ZN2v88internal12AbstractCode14SourcePositionEi, @function
_ZN2v88internal12AbstractCode14SourcePositionEi:
.LFB19693:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	%esi, %ebx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	-1(%rax), %rdx
	cmpw	$69, 11(%rdx)
	je	.L302
	movq	31(%rax), %rsi
	testb	$1, %sil
	jne	.L280
.L283:
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	leaq	-37592(%rdx), %rcx
	cmpq	-37280(%rdx), %rsi
	je	.L303
.L299:
	movq	7(%rsi), %rsi
	movq	%rsi, %rdx
	notq	%rdx
	andl	$1, %edx
.L277:
	testb	%dl, %dl
	je	.L279
.L288:
	movq	-1(%rax), %rax
	leaq	-112(%rbp), %r13
	movq	%r13, %rdi
	cmpw	$69, 11(%rax)
	sete	%al
	xorl	%edx, %edx
	xorl	%r12d, %r12d
	movzbl	%al, %eax
	subl	%eax, %ebx
	call	_ZN2v88internal27SourcePositionTableIteratorC1ENS0_9ByteArrayENS1_15IterationFilterE@PLT
	cmpl	$-1, -88(%rbp)
	jne	.L291
	jmp	.L304
	.p2align 4,,10
	.p2align 3
.L290:
	movq	-72(%rbp), %rax
	movq	%r13, %rdi
	shrq	%rax
	andl	$1073741823, %eax
	leal	-1(%rax), %r12d
	call	_ZN2v88internal27SourcePositionTableIterator7AdvanceEv@PLT
	cmpl	$-1, -88(%rbp)
	je	.L274
.L291:
	cmpl	-80(%rbp), %ebx
	jge	.L290
.L274:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L305
	addq	$88, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L280:
	.cfi_restore_state
	movq	-1(%rsi), %rdx
	cmpw	$70, 11(%rdx)
	jne	.L283
.L279:
	movq	%rsi, %rdx
	movl	$-1, %r12d
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	%rsi, -37280(%rdx)
	je	.L274
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L302:
	movq	23(%rax), %rsi
	testb	$1, %sil
	je	.L299
	movq	-1(%rsi), %rdx
	cmpw	$70, 11(%rdx)
	jne	.L299
	jmp	.L279
	.p2align 4,,10
	.p2align 3
.L303:
	movq	976(%rcx), %rsi
	movq	%rsi, %rdx
	notq	%rdx
	andl	$1, %edx
	jmp	.L277
.L304:
	xorl	%r12d, %r12d
	jmp	.L274
.L305:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19693:
	.size	_ZN2v88internal12AbstractCode14SourcePositionEi, .-_ZN2v88internal12AbstractCode14SourcePositionEi
	.section	.text._ZN2v88internal12AbstractCode23SourceStatementPositionEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12AbstractCode23SourceStatementPositionEi
	.type	_ZN2v88internal12AbstractCode23SourceStatementPositionEi, @function
_ZN2v88internal12AbstractCode23SourceStatementPositionEi:
.LFB19694:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal12AbstractCode14SourcePositionEi
	movl	%eax, %r13d
	movq	(%rbx), %rax
	movq	-1(%rax), %rdx
	cmpw	$69, 11(%rdx)
	je	.L327
	movq	31(%rax), %rsi
	testb	$1, %sil
	jne	.L328
.L312:
	andq	$-262144, %rax
	movq	24(%rax), %rax
	cmpq	-37280(%rax), %rsi
	je	.L329
.L314:
	movq	7(%rsi), %rsi
.L311:
	leaq	-112(%rbp), %rbx
	xorl	%edx, %edx
	xorl	%r12d, %r12d
	movq	%rbx, %rdi
	call	_ZN2v88internal27SourcePositionTableIteratorC1ENS0_9ByteArrayENS1_15IterationFilterE@PLT
	cmpl	$-1, -88(%rbp)
	je	.L306
	.p2align 4,,10
	.p2align 3
.L318:
	cmpb	$0, -64(%rbp)
	je	.L316
	movq	-72(%rbp), %rax
	shrq	%rax
	andl	$1073741823, %eax
	subl	$1, %eax
	cmpl	%r12d, %eax
	jle	.L316
	cmpl	%eax, %r13d
	cmovge	%eax, %r12d
.L316:
	movq	%rbx, %rdi
	call	_ZN2v88internal27SourcePositionTableIterator7AdvanceEv@PLT
	cmpl	$-1, -88(%rbp)
	jne	.L318
.L306:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L330
	addq	$88, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L328:
	.cfi_restore_state
	movq	-1(%rsi), %rdx
	cmpw	$70, 11(%rdx)
	jne	.L312
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L327:
	movq	23(%rax), %rsi
	testb	$1, %sil
	je	.L314
	movq	-1(%rsi), %rax
	cmpw	$70, 11(%rax)
	jne	.L314
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L329:
	movq	-36616(%rax), %rsi
	jmp	.L311
.L330:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19694:
	.size	_ZN2v88internal12AbstractCode23SourceStatementPositionEi, .-_ZN2v88internal12AbstractCode23SourceStatementPositionEi
	.section	.rodata._ZN2v88internal4Code18PrintDeoptLocationEP8_IO_FILEPKcm.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%s"
.LC1:
	.string	", %s\n"
	.section	.text._ZN2v88internal4Code18PrintDeoptLocationEP8_IO_FILEPKcm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4Code18PrintDeoptLocationEP8_IO_FILEPKcm
	.type	_ZN2v88internal4Code18PrintDeoptLocationEP8_IO_FILEPKcm, @function
_ZN2v88internal4Code18PrintDeoptLocationEP8_IO_FILEPKcm:
.LFB19695:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	movq	%rcx, %rsi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$368, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11Deoptimizer12GetDeoptInfoENS0_4CodeEm@PLT
	movq	%rax, -392(%rbp)
	movq	%rdx, %r13
	cmpb	$32, %dl
	jne	.L332
	testb	$1, %al
	jne	.L332
	movq	%rax, %rdx
	shrq	$31, %rax
	shrq	%rdx
	movzwl	%ax, %eax
	andl	$1073741823, %edx
	orl	%eax, %edx
	je	.L333
.L332:
	movq	%r14, %rdx
	xorl	%eax, %eax
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	leaq	-384(%rbp), %r14
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8OFStreamC1EP8_IO_FILE@PLT
	movq	(%rbx), %rdx
	movq	%r14, %rsi
	leaq	-392(%rbp), %rdi
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rbx
	call	_ZNK2v88internal14SourcePosition5PrintERSoNS0_4CodeE@PLT
	movl	%r13d, %edi
	call	_ZN2v88internal24DeoptimizeReasonToStringENS0_16DeoptimizeReasonE@PLT
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	movq	%rbx, %xmm0
	leaq	-320(%rbp), %rdi
	movq	%rax, -304(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	-304(%rbp), %rdi
	movq	%rax, -384(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -304(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
.L333:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L339
	addq	$368, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L339:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19695:
	.size	_ZN2v88internal4Code18PrintDeoptLocationEP8_IO_FILEPKcm, .-_ZN2v88internal4Code18PrintDeoptLocationEP8_IO_FILEPKcm
	.section	.text._ZN2v88internal4Code10CanDeoptAtEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4Code10CanDeoptAtEm
	.type	_ZN2v88internal4Code10CanDeoptAtEm, @function
_ZN2v88internal4Code10CanDeoptAtEm:
.LFB19696:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$32, %rsp
	movq	(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	15(%rdx), %r13
	leaq	63(%rdx), %rax
	movl	43(%rdx), %edx
	testl	%edx, %edx
	js	.L360
.L343:
	xorl	%edi, %edi
	leaq	7(%r13), %r8
	leaq	103(%r13), %rsi
	cmpl	$11, 11(%r13)
	jg	.L348
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L346:
	movq	(%r8), %rdx
	addl	$1, %edi
	addq	$24, %rsi
	sarq	$32, %rdx
	subl	$9, %edx
	movslq	%edx, %rcx
	sarl	$31, %edx
	imulq	$1431655766, %rcx, %rcx
	shrq	$32, %rcx
	subl	%edx, %ecx
	cmpl	%ecx, %edi
	jge	.L349
.L348:
	movq	(%rsi), %rdx
	sarq	$32, %rdx
	cmpq	$-1, %rdx
	je	.L346
	movq	(%rsi), %rdx
	sarq	$32, %rdx
	addq	%rax, %rdx
	cmpq	%rbx, %rdx
	jne	.L346
	movq	-16(%rsi), %rdx
	sarq	$32, %rdx
	cmpq	$-1, %rdx
	je	.L346
	movl	$1, %eax
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L349:
	xorl	%eax, %eax
.L340:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L361
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L360:
	.cfi_restore_state
	movq	%rdi, %r12
	call	_ZN2v88internal7Isolate19CurrentEmbeddedBlobEv@PLT
	testq	%rax, %rax
	je	.L362
	call	_ZN2v88internal7Isolate23CurrentEmbeddedBlobSizeEv@PLT
	movl	%eax, %r14d
	call	_ZN2v88internal7Isolate19CurrentEmbeddedBlobEv@PLT
	movl	%r14d, -56(%rbp)
	leaq	-64(%rbp), %rdi
	movq	%rax, -64(%rbp)
	movq	(%r12), %rax
	movl	59(%rax), %esi
	call	_ZNK2v88internal12EmbeddedData25InstructionStartOfBuiltinEi@PLT
	jmp	.L343
.L362:
	movq	(%r12), %rax
	addq	$63, %rax
	jmp	.L343
.L361:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19696:
	.size	_ZN2v88internal4Code10CanDeoptAtEm, .-_ZN2v88internal4Code10CanDeoptAtEm
	.section	.rodata._ZN2v88internal4Code11Kind2StringENS1_4KindE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"C_WASM_ENTRY"
.LC3:
	.string	"OPTIMIZED_FUNCTION"
.LC4:
	.string	"STUB"
.LC5:
	.string	"BUILTIN"
.LC6:
	.string	"REGEXP"
.LC7:
	.string	"WASM_FUNCTION"
.LC8:
	.string	"WASM_TO_CAPI_FUNCTION"
.LC9:
	.string	"WASM_TO_JS_FUNCTION"
.LC10:
	.string	"JS_TO_WASM_FUNCTION"
.LC11:
	.string	"JS_TO_JS_FUNCTION"
.LC12:
	.string	"WASM_INTERPRETER_ENTRY"
.LC13:
	.string	"BYTECODE_HANDLER"
.LC14:
	.string	"unreachable code"
	.section	.text._ZN2v88internal4Code11Kind2StringENS1_4KindE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4Code11Kind2StringENS1_4KindE
	.type	_ZN2v88internal4Code11Kind2StringENS1_4KindE, @function
_ZN2v88internal4Code11Kind2StringENS1_4KindE:
.LFB19697:
	.cfi_startproc
	endbr64
	cmpl	$11, %edi
	ja	.L364
	leaq	.L366(%rip), %rdx
	movl	%edi, %edi
	movslq	(%rdx,%rdi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4Code11Kind2StringENS1_4KindE,"a",@progbits
	.align 4
	.align 4
.L366:
	.long	.L377-.L366
	.long	.L376-.L366
	.long	.L378-.L366
	.long	.L374-.L366
	.long	.L373-.L366
	.long	.L372-.L366
	.long	.L371-.L366
	.long	.L370-.L366
	.long	.L369-.L366
	.long	.L368-.L366
	.long	.L367-.L366
	.long	.L365-.L366
	.section	.text._ZN2v88internal4Code11Kind2StringENS1_4KindE
	.p2align 4,,10
	.p2align 3
.L367:
	leaq	.LC12(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L365:
	leaq	.LC2(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L377:
	leaq	.LC3(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L376:
	leaq	.LC13(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L378:
	leaq	.LC4(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L374:
	leaq	.LC5(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L373:
	leaq	.LC6(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L372:
	leaq	.LC7(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L371:
	leaq	.LC8(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L370:
	leaq	.LC9(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L369:
	leaq	.LC10(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L368:
	leaq	.LC11(%rip), %rax
	ret
.L364:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC14(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19697:
	.size	_ZN2v88internal4Code11Kind2StringENS1_4KindE, .-_ZN2v88internal4Code11Kind2StringENS1_4KindE
	.section	.rodata._ZN2v88internal12AbstractCode11Kind2StringENS1_4KindE.str1.1,"aMS",@progbits,1
.LC15:
	.string	"INTERPRETED_FUNCTION"
	.section	.text._ZN2v88internal12AbstractCode11Kind2StringENS1_4KindE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12AbstractCode11Kind2StringENS1_4KindE
	.type	_ZN2v88internal12AbstractCode11Kind2StringENS1_4KindE, @function
_ZN2v88internal12AbstractCode11Kind2StringENS1_4KindE:
.LFB19698:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpl	$11, %edi
	jle	.L401
	cmpl	$12, %edi
	jne	.L400
	leaq	.LC15(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L401:
	.cfi_restore_state
	ja	.L400
	leaq	.L385(%rip), %rdx
	movl	%edi, %edi
	movslq	(%rdx,%rdi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal12AbstractCode11Kind2StringENS1_4KindE,"a",@progbits
	.align 4
	.align 4
.L385:
	.long	.L396-.L385
	.long	.L395-.L385
	.long	.L397-.L385
	.long	.L393-.L385
	.long	.L392-.L385
	.long	.L391-.L385
	.long	.L390-.L385
	.long	.L389-.L385
	.long	.L388-.L385
	.long	.L387-.L385
	.long	.L386-.L385
	.long	.L384-.L385
	.section	.text._ZN2v88internal12AbstractCode11Kind2StringENS1_4KindE
.L397:
	leaq	.LC4(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L395:
	.cfi_restore_state
	leaq	.LC13(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L386:
	.cfi_restore_state
	leaq	.LC12(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L387:
	.cfi_restore_state
	leaq	.LC11(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L388:
	.cfi_restore_state
	leaq	.LC10(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L389:
	.cfi_restore_state
	leaq	.LC9(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L390:
	.cfi_restore_state
	leaq	.LC8(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L391:
	.cfi_restore_state
	leaq	.LC7(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L392:
	.cfi_restore_state
	leaq	.LC6(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L393:
	.cfi_restore_state
	leaq	.LC5(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L396:
	.cfi_restore_state
	leaq	.LC3(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L384:
	.cfi_restore_state
	leaq	.LC2(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L400:
	.cfi_restore_state
	leaq	.LC14(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19698:
	.size	_ZN2v88internal12AbstractCode11Kind2StringENS1_4KindE, .-_ZN2v88internal12AbstractCode11Kind2StringENS1_4KindE
	.section	.rodata._ZN2v88internal4Code20IsIsolateIndependentEPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC16:
	.string	"address < start || address >= end"
	.section	.rodata._ZN2v88internal4Code20IsIsolateIndependentEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC17:
	.string	"Check failed: %s."
.LC18:
	.string	"target.IsCode()"
	.section	.text._ZN2v88internal4Code20IsIsolateIndependentEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4Code20IsIsolateIndependentEPNS0_7IsolateE
	.type	_ZN2v88internal4Code20IsIsolateIndependentEPNS0_7IsolateE, @function
_ZN2v88internal4Code20IsIsolateIndependentEPNS0_7IsolateE:
.LFB19699:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1023, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-128(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rsi
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal13RelocIteratorC1ENS0_4CodeEi@PLT
	movzbl	-72(%rbp), %r12d
	testb	%r12b, %r12b
	jne	.L402
	movl	$1, %r12d
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L411:
	xorl	%r12d, %r12d
.L404:
	movq	%r13, %rdi
	call	_ZN2v88internal13RelocIterator4nextEv@PLT
	cmpb	$0, -72(%rbp)
	jne	.L402
.L408:
	cmpb	$1, -104(%rbp)
	jg	.L411
	movq	-112(%rbp), %rax
	movq	%r14, %rdi
	movslq	(%rax), %rbx
	addq	%rax, %rbx
	leaq	4(%rbx), %r15
	movq	%r15, %rsi
	call	_ZN2v88internal17InstructionStream11PcIsOffHeapEPNS0_7IsolateEm@PLT
	testb	%al, %al
	jne	.L404
	call	_ZN2v88internal7Isolate19CurrentEmbeddedBlobEv@PLT
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal7Isolate23CurrentEmbeddedBlobSizeEv@PLT
	movq	-136(%rbp), %rcx
	movl	%eax, %eax
	addq	%rcx, %rax
	cmpq	%rax, %r15
	jnb	.L406
	cmpq	%r15, %rcx
	jbe	.L420
.L406:
	movq	-60(%rbx), %rax
	leaq	-59(%rbx), %rdi
	cmpw	$69, 11(%rax)
	jne	.L421
	call	_ZN2v88internal8Builtins27IsIsolateIndependentBuiltinENS0_4CodeE@PLT
	movq	%r13, %rdi
	testb	%al, %al
	movl	$0, %eax
	cmove	%eax, %r12d
	call	_ZN2v88internal13RelocIterator4nextEv@PLT
	cmpb	$0, -72(%rbp)
	je	.L408
	.p2align 4,,10
	.p2align 3
.L402:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L422
	addq	$104, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L420:
	.cfi_restore_state
	leaq	.LC16(%rip), %rsi
	leaq	.LC17(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L421:
	leaq	.LC18(%rip), %rsi
	leaq	.LC17(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L422:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19699:
	.size	_ZN2v88internal4Code20IsIsolateIndependentEPNS0_7IsolateE, .-_ZN2v88internal4Code20IsIsolateIndependentEPNS0_7IsolateE
	.section	.text._ZN2v88internal4Code7InlinesENS0_18SharedFunctionInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4Code7InlinesENS0_18SharedFunctionInfoE
	.type	_ZN2v88internal4Code7InlinesENS0_18SharedFunctionInfoE, @function
_ZN2v88internal4Code7InlinesENS0_18SharedFunctionInfoE:
.LFB19700:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	15(%rax), %rax
	movl	11(%rax), %edx
	testl	%edx, %edx
	jne	.L424
.L427:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L424:
	movq	63(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L428
	movq	31(%rax), %rcx
	movq	23(%rax), %rdx
	sarq	$32, %rdx
	testq	%rdx, %rdx
	jle	.L427
	subl	$1, %edx
	leaq	15(%rcx), %rax
	leaq	23(%rcx,%rdx,8), %rcx
	jmp	.L429
	.p2align 4,,10
	.p2align 3
.L430:
	addq	$8, %rax
	cmpq	%rax, %rcx
	je	.L427
.L429:
	movq	(%rax), %rdx
	cmpq	%rsi, %rdx
	jne	.L430
.L428:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE19700:
	.size	_ZN2v88internal4Code7InlinesENS0_18SharedFunctionInfoE, .-_ZN2v88internal4Code7InlinesENS0_18SharedFunctionInfoE
	.section	.text._ZN2v88internal4Code21OptimizedCodeIteratorC2EPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4Code21OptimizedCodeIteratorC2EPNS0_7IsolateE
	.type	_ZN2v88internal4Code21OptimizedCodeIteratorC2EPNS0_7IsolateE, @function
_ZN2v88internal4Code21OptimizedCodeIteratorC2EPNS0_7IsolateE:
.LFB19702:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movq	%rsi, 16(%rdi)
	movups	%xmm0, (%rdi)
	movq	39120(%rsi), %rax
	cmpq	%rax, 88(%rsi)
	je	.L431
	movq	%rax, (%rdi)
.L431:
	ret
	.cfi_endproc
.LFE19702:
	.size	_ZN2v88internal4Code21OptimizedCodeIteratorC2EPNS0_7IsolateE, .-_ZN2v88internal4Code21OptimizedCodeIteratorC2EPNS0_7IsolateE
	.globl	_ZN2v88internal4Code21OptimizedCodeIteratorC1EPNS0_7IsolateE
	.set	_ZN2v88internal4Code21OptimizedCodeIteratorC1EPNS0_7IsolateE,_ZN2v88internal4Code21OptimizedCodeIteratorC2EPNS0_7IsolateE
	.section	.text._ZN2v88internal4Code21OptimizedCodeIterator4NextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4Code21OptimizedCodeIterator4NextEv
	.type	_ZN2v88internal4Code21OptimizedCodeIterator4NextEv, @function
_ZN2v88internal4Code21OptimizedCodeIterator4NextEv:
.LFB19704:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L434
	movq	31(%rax), %rax
	movq	16(%rdi), %rcx
	movq	7(%rax), %rax
	cmpq	%rax, 88(%rcx)
	je	.L447
.L439:
	movq	%rax, 8(%rbx)
	testq	%rax, %rax
	jne	.L438
.L434:
	cmpq	$0, (%rbx)
	je	.L436
.L448:
	movq	%rbx, %rdi
	call	_ZN2v88internal13NativeContext21OptimizedCodeListHeadEv@PLT
	movq	(%rbx), %rdx
	movl	$0, %esi
	movq	1959(%rdx), %rdx
	movq	16(%rbx), %rcx
	cmpq	%rdx, 88(%rcx)
	cmove	%rsi, %rdx
	movq	%rdx, (%rbx)
	cmpq	%rax, 88(%rcx)
	jne	.L439
.L447:
	cmpq	$0, (%rbx)
	movq	$0, 8(%rbx)
	jne	.L448
.L436:
	xorl	%eax, %eax
.L438:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19704:
	.size	_ZN2v88internal4Code21OptimizedCodeIterator4NextEv, .-_ZN2v88internal4Code21OptimizedCodeIterator4NextEv
	.section	.text._ZN2v88internal18DeoptimizationData3NewEPNS0_7IsolateEiNS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18DeoptimizationData3NewEPNS0_7IsolateEiNS0_14AllocationTypeE
	.type	_ZN2v88internal18DeoptimizationData3NewEPNS0_7IsolateEiNS0_14AllocationTypeE, @function
_ZN2v88internal18DeoptimizationData3NewEPNS0_7IsolateEiNS0_14AllocationTypeE:
.LFB19705:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	9(%rsi,%rsi,2), %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19705:
	.size	_ZN2v88internal18DeoptimizationData3NewEPNS0_7IsolateEiNS0_14AllocationTypeE, .-_ZN2v88internal18DeoptimizationData3NewEPNS0_7IsolateEiNS0_14AllocationTypeE
	.section	.text._ZN2v88internal18DeoptimizationData5EmptyEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18DeoptimizationData5EmptyEPNS0_7IsolateE
	.type	_ZN2v88internal18DeoptimizationData5EmptyEPNS0_7IsolateE, @function
_ZN2v88internal18DeoptimizationData5EmptyEPNS0_7IsolateE:
.LFB19706:
	.cfi_startproc
	endbr64
	leaq	288(%rdi), %rax
	ret
	.cfi_endproc
.LFE19706:
	.size	_ZN2v88internal18DeoptimizationData5EmptyEPNS0_7IsolateE, .-_ZN2v88internal18DeoptimizationData5EmptyEPNS0_7IsolateE
	.section	.text._ZN2v88internal18DeoptimizationData18GetInlinedFunctionEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18DeoptimizationData18GetInlinedFunctionEi
	.type	_ZN2v88internal18DeoptimizationData18GetInlinedFunctionEi, @function
_ZN2v88internal18DeoptimizationData18GetInlinedFunctionEi:
.LFB19707:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	cmpl	$-1, %esi
	je	.L455
	movq	31(%rax), %rdx
	leal	16(,%rsi,8), %eax
	cltq
	movq	-1(%rdx,%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L455:
	movq	63(%rax), %rax
	ret
	.cfi_endproc
.LFE19707:
	.size	_ZN2v88internal18DeoptimizationData18GetInlinedFunctionEi, .-_ZN2v88internal18DeoptimizationData18GetInlinedFunctionEi
	.section	.text._ZNK2v88internal4Code7GetNameEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4Code7GetNameEPNS0_7IsolateE
	.type	_ZNK2v88internal4Code7GetNameEPNS0_7IsolateE, @function
_ZNK2v88internal4Code7GetNameEPNS0_7IsolateE:
.LFB19708:
	.cfi_startproc
	endbr64
	movq	(%rdi), %r8
	movl	43(%r8), %eax
	shrl	%eax
	andl	$31, %eax
	cmpl	$1, %eax
	je	.L458
	addq	$63, %r8
	leaq	41184(%rsi), %rdi
	movq	%r8, %rsi
	jmp	_ZN2v88internal8Builtins6LookupEm@PLT
	.p2align 4,,10
	.p2align 3
.L458:
	movq	41504(%rsi), %rdi
	movq	%r8, %rsi
	jmp	_ZN2v88internal11interpreter11Interpreter27LookupNameOfBytecodeHandlerENS0_4CodeE@PLT
	.cfi_endproc
.LFE19708:
	.size	_ZNK2v88internal4Code7GetNameEPNS0_7IsolateE, .-_ZNK2v88internal4Code7GetNameEPNS0_7IsolateE
	.section	.rodata._ZN2v88internal18DeoptimizationData23DeoptimizationDataPrintERSo.str1.1,"aMS",@progbits,1
.LC19:
	.string	"invalid"
	.section	.rodata._ZN2v88internal18DeoptimizationData23DeoptimizationDataPrintERSo.str1.8,"aMS",@progbits,1
	.align 8
.LC20:
	.string	"Deoptimization Input Data invalidated by lazy deoptimization\n"
	.section	.rodata._ZN2v88internal18DeoptimizationData23DeoptimizationDataPrintERSo.str1.1
.LC21:
	.string	"Inlined functions (count = "
.LC22:
	.string	")\n"
.LC23:
	.string	"\n"
	.section	.rodata._ZN2v88internal18DeoptimizationData23DeoptimizationDataPrintERSo.str1.8
	.align 8
.LC24:
	.string	"Deoptimization Input Data (deopt points = "
	.section	.rodata._ZN2v88internal18DeoptimizationData23DeoptimizationDataPrintERSo.str1.1
.LC25:
	.string	" "
.LC26:
	.string	" index  bytecode-offset    pc"
.LC27:
	.string	"  commands"
.LC28:
	.string	"  "
.LC29:
	.string	"NA"
.LC30:
	.string	" {frame count="
.LC31:
	.string	", js frame count="
.LC32:
	.string	", update_feedback_count="
.LC33:
	.string	"}\n"
.LC34:
	.string	"    "
.LC35:
	.string	"{bytecode_offset="
.LC36:
	.string	", function="
.LC37:
	.string	", height="
.LC38:
	.string	", retval=@"
.LC39:
	.string	"(#"
.LC40:
	.string	")}"
.LC41:
	.string	"{bailout_id="
.LC42:
	.string	"}"
.LC43:
	.string	"{function="
.LC44:
	.string	"{input="
.LC45:
	.string	" (int32)}"
.LC46:
	.string	" (int64)}"
.LC47:
	.string	" (uint32)}"
.LC48:
	.string	" (bool)}"
.LC49:
	.string	"{literal_id="
.LC50:
	.string	" ("
.LC51:
	.string	"{object_index="
.LC52:
	.string	"{arguments_type="
.LC53:
	.string	"MAPPED_ARGUMENTS"
.LC54:
	.string	"UNMAPPED_ARGUMENTS"
.LC55:
	.string	"REST_PARAMETER"
.LC56:
	.string	"{length="
.LC57:
	.string	"{feedback={vector_index="
.LC58:
	.string	", slot="
.LC59:
	.string	"}}"
	.section	.text._ZN2v88internal18DeoptimizationData23DeoptimizationDataPrintERSo,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18DeoptimizationData23DeoptimizationDataPrintERSo
	.type	_ZN2v88internal18DeoptimizationData23DeoptimizationDataPrintERSo, @function
_ZN2v88internal18DeoptimizationData23DeoptimizationDataPrintERSo:
.LFB19710:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movl	11(%rax), %edx
	testl	%edx, %edx
	je	.L536
	leaq	16+_ZTVN6disasm13NameConverterE(%rip), %rcx
	leaq	-184(%rbp), %rdx
	movq	%rdi, %r14
	movq	%r12, %rdi
	movq	%rdx, %xmm1
	movq	%rcx, %xmm0
	movq	$128, -192(%rbp)
	movl	$27, %edx
	punpcklqdq	%xmm1, %xmm0
	leaq	.LC21(%rip), %rsi
	movaps	%xmm0, -208(%rbp)
	movq	23(%rax), %rbx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	sarq	$32, %rbx
	movl	%ebx, %esi
	call	_ZNSolsEi@PLT
	movl	$2, %edx
	leaq	.LC22(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	testq	%rbx, %rbx
	jle	.L465
	leal	-1(%rbx), %eax
	movl	$16, %r15d
	leaq	-224(%rbp), %r13
	movq	%r14, %rbx
	leaq	24(,%rax,8), %rax
	movq	%rax, -248(%rbp)
	.p2align 4,,10
	.p2align 3
.L466:
	movq	(%rbx), %rax
	movl	$1, %edx
	leaq	.LC25(%rip), %rsi
	movq	%r12, %rdi
	movq	31(%rax), %rax
	movq	-1(%rax,%r15), %r14
	addq	$8, %r15
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r14, -224(%rbp)
	call	_ZN2v88internallsERSoRKNS0_5BriefE@PLT
	movl	$1, %edx
	leaq	.LC23(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpq	%r15, -248(%rbp)
	jne	.L466
	movq	%rbx, %r14
.L465:
	movl	$1, %edx
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rax
	leaq	.LC24(%rip), %rsi
	movq	%r12, %rdi
	movslq	11(%rax), %rbx
	leal	-9(%rbx), %edx
	movslq	%edx, %rax
	sarl	$31, %edx
	imulq	$1431655766, %rax, %rax
	shrq	$32, %rax
	movl	%eax, %r15d
	subl	%edx, %r15d
	movl	$42, %edx
	movl	%r15d, -268(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZNSolsEi@PLT
	movl	$2, %edx
	leaq	.LC22(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	testl	%r15d, %r15d
	jne	.L537
.L459:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L538
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L537:
	.cfi_restore_state
	movl	$29, %edx
	leaq	.LC26(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, _ZN2v88internal23FLAG_print_code_verboseE(%rip)
	jne	.L539
.L467:
	movl	$1, %edx
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpq	$11, %rbx
	jle	.L459
	movq	$88, -256(%rbp)
	movl	$0, -248(%rbp)
	jmp	.L518
	.p2align 4,,10
	.p2align 3
.L541:
	movl	$1, %edx
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L471:
	addl	$1, -248(%rbp)
	movl	-248(%rbp), %eax
	addq	$24, -256(%rbp)
	cmpl	-268(%rbp), %eax
	jge	.L459
.L518:
	movq	(%r12), %rax
	movl	-248(%rbp), %esi
	movq	%r12, %rdi
	movq	-24(%rax), %rax
	movq	$6, 16(%r12,%rax)
	call	_ZNSolsEi@PLT
	movl	$2, %edx
	leaq	.LC28(%rip), %rsi
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	0(%r13), %rax
	movq	-256(%rbp), %r15
	movq	%r13, %rdi
	movq	-24(%rax), %rax
	movq	$15, 16(%r13,%rax)
	movq	(%r14), %rax
	movq	-1(%r15,%rax), %rsi
	sarq	$32, %rsi
	call	_ZNSolsEi@PLT
	leaq	.LC28(%rip), %rsi
	movl	$2, %edx
	movq	%rax, %rbx
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movq	-24(%rax), %rax
	movq	$4, 16(%rbx,%rax)
	movq	(%r14), %rax
	movq	15(%r15,%rax), %rsi
	sarq	$32, %rsi
	cmpq	$-1, %rsi
	je	.L540
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	-24(%rax), %rdx
	addq	%r12, %rdx
	movl	24(%rdx), %eax
	andl	$-75, %eax
	orl	$8, %eax
	movl	%eax, 24(%rdx)
	call	_ZNSolsEi@PLT
	movq	(%rax), %rdx
	addq	-24(%rdx), %rax
	movl	24(%rax), %edx
	andl	$-75, %edx
	orl	$2, %edx
	movl	%edx, 24(%rax)
.L469:
	movq	(%r12), %rax
	cmpb	$0, _ZN2v88internal23FLAG_print_code_verboseE(%rip)
	movq	-24(%rax), %rax
	movq	$2, 16(%r12,%rax)
	je	.L541
	movq	(%r14), %rax
	movq	-256(%rbp), %rbx
	movq	7(%rbx,%rax), %rdx
	movq	15(%rax), %rsi
	leaq	-224(%rbp), %rbx
	movq	%rbx, %rdi
	sarq	$32, %rdx
	call	_ZN2v88internal19TranslationIteratorC1ENS0_9ByteArrayEi@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal19TranslationIterator4NextEv@PLT
	movq	%rbx, %rdi
	movl	%eax, -280(%rbp)
	call	_ZN2v88internal19TranslationIterator4NextEv@PLT
	movq	%rbx, %rdi
	movl	%eax, %r15d
	call	_ZN2v88internal19TranslationIterator4NextEv@PLT
	movq	%rbx, %rdi
	movl	%eax, -264(%rbp)
	call	_ZN2v88internal19TranslationIterator4NextEv@PLT
	movq	%r12, %rdi
	movl	$2, %edx
	leaq	.LC28(%rip), %rsi
	movl	%eax, %r13d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-280(%rbp), %r8d
	movl	%r8d, %edi
	call	_ZN2v88internal11Translation9StringForENS1_6OpcodeE@PLT
	testq	%rax, %rax
	je	.L542
	movq	%rax, %rdi
	movq	%rax, -280(%rbp)
	call	strlen@PLT
	movq	-280(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L473:
	movl	$14, %edx
	leaq	.LC30(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZNSolsEi@PLT
	movl	$17, %edx
	leaq	.LC31(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-264(%rbp), %esi
	movq	%r15, %rdi
	call	_ZNSolsEi@PLT
	movl	$24, %edx
	leaq	.LC32(%rip), %rsi
	movq	%rax, %r15
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	_ZNSolsEi@PLT
	movl	$2, %edx
	leaq	.LC33(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-232(%rbp), %rax
	movq	%rax, -264(%rbp)
	.p2align 4,,10
	.p2align 3
.L517:
	movq	%rbx, %rdi
	call	_ZNK2v88internal19TranslationIterator7HasNextEv@PLT
	testb	%al, %al
	je	.L471
	movq	%rbx, %rdi
	call	_ZN2v88internal19TranslationIterator4NextEv@PLT
	movl	%eax, %r13d
	testl	%r13d, %r13d
	je	.L471
	movq	(%r12), %rax
	movl	$4, %edx
	leaq	.LC34(%rip), %rsi
	movq	%r12, %rdi
	movq	-24(%rax), %rax
	movq	$31, 16(%r12,%rax)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%r13d, %edi
	call	_ZN2v88internal11Translation9StringForENS1_6OpcodeE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L543
	movq	%rax, %rdi
	call	strlen@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L476:
	movl	$1, %edx
	leaq	.LC25(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	$26, %r13d
	ja	.L477
	leaq	.L479(%rip), %rcx
	movslq	(%rcx,%r13,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal18DeoptimizationData23DeoptimizationDataPrintERSo,"a",@progbits
	.align 4
	.align 4
.L479:
	.long	.L477-.L479
	.long	.L499-.L479
	.long	.L498-.L479
	.long	.L498-.L479
	.long	.L498-.L479
	.long	.L498-.L479
	.long	.L496-.L479
	.long	.L495-.L479
	.long	.L494-.L479
	.long	.L494-.L479
	.long	.L493-.L479
	.long	.L492-.L479
	.long	.L491-.L479
	.long	.L490-.L479
	.long	.L489-.L479
	.long	.L488-.L479
	.long	.L486-.L479
	.long	.L486-.L479
	.long	.L481-.L479
	.long	.L485-.L479
	.long	.L484-.L479
	.long	.L483-.L479
	.long	.L482-.L479
	.long	.L481-.L479
	.long	.L481-.L479
	.long	.L480-.L479
	.long	.L478-.L479
	.section	.text._ZN2v88internal18DeoptimizationData23DeoptimizationDataPrintERSo
	.p2align 4,,10
	.p2align 3
.L498:
	movq	%rbx, %rdi
	call	_ZN2v88internal19TranslationIterator4NextEv@PLT
	movq	%rbx, %rdi
	movl	%eax, %r13d
	call	_ZN2v88internal19TranslationIterator4NextEv@PLT
	movq	(%r14), %rdx
	movq	%rbx, %rdi
	leal	16(,%rax,8), %eax
	movq	31(%rdx), %rdx
	cltq
	movq	-1(%rdx,%rax), %rcx
	movq	%rcx, -280(%rbp)
	call	_ZN2v88internal19TranslationIterator4NextEv@PLT
	movl	$12, %edx
	leaq	.LC41(%rip), %rsi
	movq	%r12, %rdi
	movl	%eax, %r15d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZNSolsEi@PLT
	leaq	.LC36(%rip), %rsi
	movl	$11, %edx
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-280(%rbp), %rcx
	leaq	-240(%rbp), %rdi
	movq	%rcx, -240(%rbp)
	call	_ZN2v88internal18SharedFunctionInfo9DebugNameEv@PLT
	movq	-264(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, -232(%rbp)
.L532:
	call	_ZN2v88internallsERSoRKNS0_5BriefE@PLT
	movl	$9, %edx
	leaq	.LC37(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-280(%rbp), %rdi
	movl	%r15d, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC42(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	.p2align 4,,10
	.p2align 3
.L477:
	movl	$1, %edx
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L517
	.p2align 4,,10
	.p2align 3
.L481:
	movq	%rbx, %rdi
	call	_ZN2v88internal19TranslationIterator4NextEv@PLT
	movl	$7, %edx
	leaq	.LC44(%rip), %rsi
	movl	%eax, %r13d
.L533:
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC42(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L477
	.p2align 4,,10
	.p2align 3
.L486:
	movq	%rbx, %rdi
	call	_ZN2v88internal19TranslationIterator4NextEv@PLT
	movl	$7, %edx
	leaq	.LC44(%rip), %rsi
	movq	%r12, %rdi
	movslq	%eax, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	$-1, %r13d
	je	.L521
	leaq	_ZZN2v88internal12RegisterNameENS0_11XMMRegisterEE5Names(%rip), %rax
	movq	(%rax,%r13,8), %r13
	testq	%r13, %r13
	je	.L513
.L500:
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%rax, %rdx
.L531:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L501:
	movl	$1, %edx
	leaq	.LC42(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L477
	.p2align 4,,10
	.p2align 3
.L494:
	movq	%rbx, %rdi
	call	_ZN2v88internal19TranslationIterator4NextEv@PLT
	movl	$16, %edx
	leaq	.LC52(%rip), %rsi
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$1, %r13b
	je	.L514
	cmpb	$2, %r13b
	jne	.L544
	movl	$14, %edx
	leaq	.LC55(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L501
	.p2align 4,,10
	.p2align 3
.L495:
	movq	%rbx, %rdi
	call	_ZN2v88internal19TranslationIterator4NextEv@PLT
	movl	$14, %edx
	leaq	.LC51(%rip), %rsi
	movl	%eax, %r13d
	jmp	.L533
	.p2align 4,,10
	.p2align 3
.L483:
	movq	%rbx, %rdi
	call	_ZN2v88internal19TranslationIterator4NextEv@PLT
	movl	$7, %edx
	movq	%r12, %rdi
	leaq	.LC44(%rip), %rsi
	movl	%eax, %r13d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZNSolsEi@PLT
	movl	$10, %edx
	leaq	.LC47(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L477
	.p2align 4,,10
	.p2align 3
.L484:
	movq	%rbx, %rdi
	call	_ZN2v88internal19TranslationIterator4NextEv@PLT
	movl	$7, %edx
	movq	%r12, %rdi
	leaq	.LC44(%rip), %rsi
	movl	%eax, %r13d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZNSolsEi@PLT
	movl	$9, %edx
	leaq	.LC46(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L477
	.p2align 4,,10
	.p2align 3
.L485:
	movq	%rbx, %rdi
	call	_ZN2v88internal19TranslationIterator4NextEv@PLT
	movl	$7, %edx
	movq	%r12, %rdi
	leaq	.LC44(%rip), %rsi
	movl	%eax, %r13d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZNSolsEi@PLT
	movl	$9, %edx
	leaq	.LC45(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L477
	.p2align 4,,10
	.p2align 3
.L480:
	movq	%rbx, %rdi
	call	_ZN2v88internal19TranslationIterator4NextEv@PLT
	movq	%r12, %rdi
	leaq	.LC49(%rip), %rsi
	movl	%eax, %r13d
	movq	(%r14), %rax
	movq	31(%rax), %rdx
	leal	16(,%r13,8), %eax
	cltq
	movq	-1(%rdx,%rax), %rcx
	movl	$12, %edx
	movq	%rcx, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZNSolsEi@PLT
	movl	$2, %edx
	leaq	.LC50(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-280(%rbp), %rcx
	movq	-264(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rcx, -232(%rbp)
	call	_ZN2v88internallsERSoRKNS0_5BriefE@PLT
	movl	$2, %edx
	leaq	.LC40(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L477
	.p2align 4,,10
	.p2align 3
.L488:
	movq	%rbx, %rdi
	call	_ZN2v88internal19TranslationIterator4NextEv@PLT
	movl	$7, %edx
	leaq	.LC44(%rip), %rsi
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%r13d, %esi
	leaq	-208(%rbp), %rdi
	call	_ZNK6disasm13NameConverter17NameOfCPURegisterEi@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L545
	movq	%rax, %rdi
	call	strlen@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L509:
	movl	$8, %edx
	leaq	.LC48(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L477
	.p2align 4,,10
	.p2align 3
.L489:
	movq	%rbx, %rdi
	call	_ZN2v88internal19TranslationIterator4NextEv@PLT
	movl	$7, %edx
	leaq	.LC44(%rip), %rsi
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%r13d, %esi
	leaq	-208(%rbp), %rdi
	call	_ZNK6disasm13NameConverter17NameOfCPURegisterEi@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L546
	movq	%rax, %rdi
	call	strlen@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L507:
	movl	$10, %edx
	leaq	.LC47(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L477
	.p2align 4,,10
	.p2align 3
.L499:
	movq	%rbx, %rdi
	call	_ZN2v88internal19TranslationIterator4NextEv@PLT
	movq	%rbx, %rdi
	movl	%eax, %r13d
	call	_ZN2v88internal19TranslationIterator4NextEv@PLT
	movq	%rbx, %rdi
	movl	%eax, -280(%rbp)
	call	_ZN2v88internal19TranslationIterator4NextEv@PLT
	movq	%rbx, %rdi
	movl	%eax, %r15d
	call	_ZN2v88internal19TranslationIterator4NextEv@PLT
	movq	%rbx, %rdi
	movl	%eax, -284(%rbp)
	call	_ZN2v88internal19TranslationIterator4NextEv@PLT
	movq	%r12, %rdi
	leaq	.LC35(%rip), %rsi
	movl	%eax, -272(%rbp)
	movq	(%r14), %rax
	movq	31(%rax), %rcx
	movl	-280(%rbp), %edx
	leal	16(,%rdx,8), %eax
	movl	$17, %edx
	cltq
	movq	-1(%rcx,%rax), %rcx
	movq	%rcx, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZNSolsEi@PLT
	movl	$11, %edx
	leaq	.LC36(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-280(%rbp), %rcx
	leaq	-240(%rbp), %rdi
	movq	%rcx, -240(%rbp)
	call	_ZN2v88internal18SharedFunctionInfo9DebugNameEv@PLT
	movq	-264(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, -232(%rbp)
	call	_ZN2v88internallsERSoRKNS0_5BriefE@PLT
	movl	$9, %edx
	leaq	.LC37(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-280(%rbp), %rdi
	movl	%r15d, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$10, %edx
	leaq	.LC38(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-284(%rbp), %r9d
	movq	%r13, %rdi
	movl	%r9d, %esi
	call	_ZNSolsEi@PLT
	movl	$2, %edx
	leaq	.LC39(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-272(%rbp), %r8d
	movq	%r13, %rdi
	movl	%r8d, %esi
	call	_ZNSolsEi@PLT
	movl	$2, %edx
	leaq	.LC40(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L477
	.p2align 4,,10
	.p2align 3
.L482:
	movq	%rbx, %rdi
	call	_ZN2v88internal19TranslationIterator4NextEv@PLT
	movl	$7, %edx
	movq	%r12, %rdi
	leaq	.LC44(%rip), %rsi
	movl	%eax, %r13d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZNSolsEi@PLT
	movl	$8, %edx
	leaq	.LC48(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L477
	.p2align 4,,10
	.p2align 3
.L490:
	movq	%rbx, %rdi
	call	_ZN2v88internal19TranslationIterator4NextEv@PLT
	movl	$7, %edx
	leaq	.LC44(%rip), %rsi
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%r13d, %esi
	leaq	-208(%rbp), %rdi
	call	_ZNK6disasm13NameConverter17NameOfCPURegisterEi@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L547
	movq	%rax, %rdi
	call	strlen@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L505:
	movl	$9, %edx
	leaq	.LC46(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L477
	.p2align 4,,10
	.p2align 3
.L491:
	movq	%rbx, %rdi
	call	_ZN2v88internal19TranslationIterator4NextEv@PLT
	movl	$7, %edx
	leaq	.LC44(%rip), %rsi
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%r13d, %esi
	leaq	-208(%rbp), %rdi
	call	_ZNK6disasm13NameConverter17NameOfCPURegisterEi@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L548
	movq	%rax, %rdi
	call	strlen@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L503:
	movl	$9, %edx
	leaq	.LC45(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L477
	.p2align 4,,10
	.p2align 3
.L496:
	movq	%rbx, %rdi
	call	_ZN2v88internal19TranslationIterator4NextEv@PLT
	movq	(%r14), %rdx
	movq	%rbx, %rdi
	leal	16(,%rax,8), %eax
	movq	31(%rdx), %rdx
	cltq
	movq	-1(%rdx,%rax), %r13
	call	_ZN2v88internal19TranslationIterator4NextEv@PLT
	leaq	.LC43(%rip), %rsi
	movl	$10, %edx
	movq	%r12, %rdi
	movl	%eax, %r15d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-240(%rbp), %rdi
	movq	%r13, -240(%rbp)
	call	_ZN2v88internal18SharedFunctionInfo9DebugNameEv@PLT
	movq	-264(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -232(%rbp)
	jmp	.L532
	.p2align 4,,10
	.p2align 3
.L492:
	movq	%rbx, %rdi
	call	_ZN2v88internal19TranslationIterator4NextEv@PLT
	movl	$7, %edx
	leaq	.LC44(%rip), %rsi
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%r13d, %esi
	leaq	-208(%rbp), %rdi
	call	_ZNK6disasm13NameConverter17NameOfCPURegisterEi@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L500
.L513:
	movq	(%r12), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L501
	.p2align 4,,10
	.p2align 3
.L478:
	movq	%rbx, %rdi
	call	_ZN2v88internal19TranslationIterator4NextEv@PLT
	movq	%rbx, %rdi
	movl	%eax, %r13d
	call	_ZN2v88internal19TranslationIterator4NextEv@PLT
	movl	$24, %edx
	movq	%r12, %rdi
	leaq	.LC57(%rip), %rsi
	movl	%eax, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZNSolsEi@PLT
	movl	$7, %edx
	leaq	.LC58(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-280(%rbp), %r8d
	movq	%r13, %rdi
	movl	%r8d, %esi
	call	_ZN2v88internallsERSoNS0_12FeedbackSlotE@PLT
	movl	$2, %edx
	leaq	.LC59(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L477
	.p2align 4,,10
	.p2align 3
.L493:
	movq	%rbx, %rdi
	call	_ZN2v88internal19TranslationIterator4NextEv@PLT
	movl	$8, %edx
	leaq	.LC56(%rip), %rsi
	movl	%eax, %r13d
	jmp	.L533
	.p2align 4,,10
	.p2align 3
.L543:
	movq	(%r12), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L476
	.p2align 4,,10
	.p2align 3
.L540:
	movl	$2, %edx
	leaq	.LC29(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L544:
	testb	%r13b, %r13b
	je	.L549
	leaq	.LC14(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L542:
	movq	(%r12), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L473
	.p2align 4,,10
	.p2align 3
.L521:
	movl	$7, %edx
	leaq	.LC19(%rip), %r13
	jmp	.L531
	.p2align 4,,10
	.p2align 3
.L536:
	movl	$61, %edx
	leaq	.LC20(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L459
	.p2align 4,,10
	.p2align 3
.L514:
	movl	$18, %edx
	leaq	.LC54(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L501
	.p2align 4,,10
	.p2align 3
.L549:
	movl	$16, %edx
	leaq	.LC53(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L501
	.p2align 4,,10
	.p2align 3
.L547:
	movq	(%r12), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L505
	.p2align 4,,10
	.p2align 3
.L545:
	movq	(%r12), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L509
	.p2align 4,,10
	.p2align 3
.L546:
	movq	(%r12), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L507
	.p2align 4,,10
	.p2align 3
.L548:
	movq	(%r12), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L503
.L539:
	movl	$10, %edx
	leaq	.LC27(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L467
.L538:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19710:
	.size	_ZN2v88internal18DeoptimizationData23DeoptimizationDataPrintERSo, .-_ZN2v88internal18DeoptimizationData23DeoptimizationDataPrintERSo
	.section	.rodata._ZN2v88internal4Code11DisassembleEPKcRSom.str1.1,"aMS",@progbits,1
.LC60:
	.string	"turbofan"
.LC61:
	.string	"unknown"
.LC62:
	.string	"kind = "
.LC63:
	.string	"name = "
.LC64:
	.string	"stack_slots = "
.LC65:
	.string	"compiler = "
.LC66:
	.string	"Trampoline (size = "
.LC67:
	.string	"Instructions (size = "
	.section	.rodata._ZN2v88internal4Code11DisassembleEPKcRSom.str1.8,"aMS",@progbits,1
	.align 8
.LC68:
	.string	"Source positions:\n pc offset  position\n"
	.align 8
.LC69:
	.string	"External Source positions:\n pc offset  fileid  line\n"
	.section	.rodata._ZN2v88internal4Code11DisassembleEPKcRSom.str1.1
.LC70:
	.string	"Safepoints (size = "
.LC71:
	.string	" (sp -> fp)  "
.LC72:
	.string	"<none>"
.LC73:
	.string	"Handler Table (size = "
.LC74:
	.string	"RelocInfo (size = "
.LC75:
	.string	"UnwindingInfo (size = "
.LC76:
	.string	"  statement"
.LC77:
	.string	""
.LC78:
	.string	"address = "
.LC79:
	.string	"\n\n"
	.section	.text._ZN2v88internal4Code11DisassembleEPKcRSom,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4Code11DisassembleEPKcRSom
	.type	_ZN2v88internal4Code11DisassembleEPKcRSom, @function
_ZN2v88internal4Code11DisassembleEPKcRSom:
.LFB19715:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	movl	$7, %edx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	leaq	.LC62(%rip), %rsi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$152, %rsp
	movq	%rcx, -160(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%r13, %rdi
	andq	$-262144, %rax
	movq	24(%rax), %rax
	subq	$37592, %rax
	movq	%rax, -152(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movl	43(%rax), %eax
	shrl	%eax
	andl	$31, %eax
	cmpl	$11, %eax
	ja	.L551
	leaq	.L553(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4Code11DisassembleEPKcRSom,"a",@progbits
	.align 4
	.align 4
.L553:
	.long	.L564-.L553
	.long	.L563-.L553
	.long	.L562-.L553
	.long	.L639-.L553
	.long	.L560-.L553
	.long	.L559-.L553
	.long	.L558-.L553
	.long	.L557-.L553
	.long	.L556-.L553
	.long	.L555-.L553
	.long	.L554-.L553
	.long	.L552-.L553
	.section	.text._ZN2v88internal4Code11DisassembleEPKcRSom
.L554:
	movl	$22, %edx
	leaq	.LC12(%rip), %rsi
	.p2align 4,,10
	.p2align 3
.L561:
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	leaq	.LC23(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	testq	%r12, %r12
	je	.L652
.L565:
	cmpb	$0, (%r12)
	jne	.L653
.L568:
	movq	(%rbx), %rax
	testb	$62, 43(%rax)
	je	.L654
.L570:
	movl	$11, %edx
	leaq	.LC65(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	leaq	.LC60(%rip), %rsi
	movq	%r13, %rdi
	movl	43(%rax), %eax
	andl	$64, %eax
	cmpl	$1, %eax
	sbbq	%rdx, %rdx
	addq	$8, %rdx
	testl	%eax, %eax
	leaq	.LC61(%rip), %rax
	cmove	%rax, %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	leaq	.LC23(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$10, %edx
	leaq	.LC78(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZNSo9_M_insertIPKvEERSoT_@PLT
	movl	$2, %edx
	leaq	.LC79(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movl	43(%rax), %r15d
	testl	%r15d, %r15d
	js	.L655
.L637:
	movslq	47(%rax), %r15
	movl	$21, %edx
	movq	%r13, %rdi
	leaq	-128(%rbp), %r12
	leaq	.LC67(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%r15d, %esi
	movq	%r13, %rdi
	call	_ZNSolsEi@PLT
	movl	$2, %edx
	leaq	.LC22(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rsi
	movl	43(%rsi), %r14d
	leaq	63(%rsi), %rdx
	testl	%r14d, %r14d
	js	.L656
.L577:
	movq	-152(%rbp), %rax
	leaq	(%r15,%rdx), %rcx
	movq	41112(%rax), %rdi
	addl	$1, 41104(%rax)
	movq	41088(%rax), %r14
	movq	41096(%rax), %r15
	testq	%rdi, %rdi
	je	.L578
	movq	%rcx, -176(%rbp)
	movq	%rdx, -168(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-168(%rbp), %rdx
	movq	-176(%rbp), %rcx
	movq	%rax, %r9
.L579:
	subq	$8, %rsp
	pushq	-160(%rbp)
	movq	%r13, %rsi
	movq	-152(%rbp), %rdi
	movl	$1, %r8d
	call	_ZN2v88internal12Disassembler6DecodeEPNS0_7IsolateEPSoPhS5_NS0_13CodeReferenceEm@PLT
	movq	-152(%rbp), %rax
	subl	$1, 41104(%rax)
	movq	%r14, 41088(%rax)
	popq	%rsi
	popq	%rdi
	cmpq	41096(%rax), %r15
	je	.L633
	movq	%r15, 41096(%rax)
	movq	%rax, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L633:
	movl	$1, %edx
	leaq	.LC23(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rdx
	movq	%rdx, %rax
	movq	23(%rdx), %rsi
	andq	$-262144, %rax
	movq	24(%rax), %rax
	leaq	-37592(%rax), %rcx
	cmpq	-37504(%rax), %rsi
	je	.L581
	cmpq	312(%rcx), %rsi
	je	.L581
	testb	$1, %sil
	jne	.L584
.L586:
	movq	7(%rsi), %rsi
.L583:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal27SourcePositionTableIteratorC1ENS0_9ByteArrayENS1_15IterationFilterE@PLT
	cmpl	$-1, -104(%rbp)
	je	.L587
	movl	$39, %edx
	leaq	.LC68(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	$-1, -104(%rbp)
	je	.L588
	leaq	.LC77(%rip), %r14
	.p2align 4,,10
	.p2align 3
.L631:
	movq	0(%r13), %rax
	movl	-96(%rbp), %esi
	movq	%r13, %rdi
	movq	-24(%rax), %rax
	movq	$10, 16(%r13,%rax)
	movq	0(%r13), %rax
	movq	-24(%rax), %rdx
	addq	%r13, %rdx
	movl	24(%rdx), %eax
	andl	$-75, %eax
	orl	$8, %eax
	movl	%eax, 24(%rdx)
	call	_ZNSolsEi@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	-24(%rax), %rdx
	addq	%rdi, %rdx
	movl	24(%rdx), %eax
	andl	$-75, %eax
	orl	$2, %eax
	movl	%eax, 24(%rdx)
	movq	(%rdi), %rax
	movq	-24(%rax), %rax
	movq	$10, 16(%rdi,%rax)
	movq	-88(%rbp), %rsi
	shrq	%rsi
	andl	$1073741823, %esi
	subl	$1, %esi
	call	_ZNSolsEi@PLT
	cmpb	$0, -80(%rbp)
	movl	$11, %edx
	leaq	.LC76(%rip), %rsi
	movq	%rax, %r15
	jne	.L651
	xorl	%edx, %edx
	movq	%r14, %rsi
.L651:
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	leaq	.LC23(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal27SourcePositionTableIterator7AdvanceEv@PLT
	cmpl	$-1, -104(%rbp)
	jne	.L631
.L588:
	movl	$1, %edx
	leaq	.LC23(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L587:
	movq	(%rbx), %rdx
	movq	%rdx, %rax
	movq	23(%rdx), %rsi
	andq	$-262144, %rax
	movq	24(%rax), %rax
	leaq	-37592(%rax), %rcx
	cmpq	-37504(%rax), %rsi
	je	.L591
	cmpq	312(%rcx), %rsi
	je	.L591
	testb	$1, %sil
	jne	.L594
.L596:
	movq	7(%rsi), %rsi
.L593:
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal27SourcePositionTableIteratorC1ENS0_9ByteArrayENS1_15IterationFilterE@PLT
	cmpl	$-1, -104(%rbp)
	je	.L597
	movl	$52, %edx
	leaq	.LC69(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	$-1, -104(%rbp)
	leaq	.LC23(%rip), %r14
	je	.L599
	.p2align 4,,10
	.p2align 3
.L598:
	movq	0(%r13), %rax
	movl	-96(%rbp), %esi
	movq	%r13, %rdi
	movq	-24(%rax), %rax
	movq	$10, 16(%r13,%rax)
	movq	0(%r13), %rax
	movq	-24(%rax), %rdx
	addq	%r13, %rdx
	movl	24(%rdx), %eax
	andl	$-75, %eax
	orl	$8, %eax
	movl	%eax, 24(%rdx)
	call	_ZNSolsEi@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	-24(%rax), %rdx
	addq	%rdi, %rdx
	movl	24(%rdx), %eax
	andl	$-75, %eax
	orl	$2, %eax
	movl	%eax, 24(%rdx)
	movq	(%rdi), %rax
	movq	-24(%rax), %rax
	movq	$10, 16(%rdi,%rax)
	movq	-88(%rbp), %rsi
	shrq	$21, %rsi
	andl	$1023, %esi
	call	_ZNSolsEi@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	$10, 16(%rdi,%rax)
	movq	-88(%rbp), %rsi
	shrq	%rsi
	andl	$1048575, %esi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal27SourcePositionTableIterator7AdvanceEv@PLT
	cmpl	$-1, -104(%rbp)
	jne	.L598
.L599:
	movl	$1, %edx
	leaq	.LC23(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L597:
	movq	(%rbx), %rax
	testb	$62, 43(%rax)
	je	.L657
.L600:
	movl	$1, %edx
	leaq	.LC23(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movl	43(%rax), %edx
	testb	$64, %dl
	jne	.L601
	shrl	%edx
	andl	$31, %edx
	cmpl	$5, %edx
	je	.L601
	movl	55(%rax), %edx
	subl	51(%rax), %edx
	testl	%edx, %edx
	jg	.L658
	.p2align 4,,10
	.p2align 3
.L612:
	movl	$18, %edx
	leaq	.LC74(%rip), %rsi
	leaq	-112(%rbp), %r14
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movq	%r13, %rdi
	movq	7(%rax), %rax
	movslq	11(%rax), %rsi
	call	_ZNSolsEi@PLT
	movl	$2, %edx
	leaq	.LC22(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rsi
	movl	$-1, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal13RelocIteratorC1ENS0_4CodeEi@PLT
	cmpb	$0, -72(%rbp)
	jne	.L617
	.p2align 4,,10
	.p2align 3
.L614:
	movq	-152(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal9RelocInfo5PrintEPNS0_7IsolateERSo@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal13RelocIterator4nextEv@PLT
	cmpb	$0, -72(%rbp)
	je	.L614
.L617:
	movl	$1, %edx
	leaq	.LC23(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rcx
	movl	43(%rcx), %eax
	testb	$1, %al
	jne	.L659
	testl	%eax, %eax
	js	.L660
.L618:
	movslq	55(%rcx), %rsi
	movl	39(%rcx), %edx
	subl	%esi, %edx
	testl	%edx, %edx
	jg	.L627
.L550:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L661
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L552:
	.cfi_restore_state
	movl	$12, %edx
	leaq	.LC2(%rip), %rsi
	jmp	.L561
.L564:
	movl	$18, %edx
	leaq	.LC3(%rip), %rsi
	jmp	.L561
.L563:
	movl	$16, %edx
	leaq	.LC13(%rip), %rsi
	jmp	.L561
.L562:
	movl	$4, %edx
	leaq	.LC4(%rip), %rsi
	jmp	.L561
.L639:
	movl	$7, %edx
	leaq	.LC5(%rip), %rsi
	jmp	.L561
.L560:
	movl	$6, %edx
	leaq	.LC6(%rip), %rsi
	jmp	.L561
.L559:
	movl	$13, %edx
	leaq	.LC7(%rip), %rsi
	jmp	.L561
.L558:
	movl	$21, %edx
	leaq	.LC8(%rip), %rsi
	jmp	.L561
.L557:
	movl	$19, %edx
	leaq	.LC9(%rip), %rsi
	jmp	.L561
.L556:
	movl	$19, %edx
	leaq	.LC10(%rip), %rsi
	jmp	.L561
.L555:
	movl	$17, %edx
	leaq	.LC11(%rip), %rsi
	jmp	.L561
	.p2align 4,,10
	.p2align 3
.L591:
	movq	976(%rcx), %rsi
	jmp	.L593
	.p2align 4,,10
	.p2align 3
.L581:
	movq	976(%rcx), %rsi
	jmp	.L583
	.p2align 4,,10
	.p2align 3
.L601:
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	xorl	%r15d, %r15d
	call	_ZN2v88internal14SafepointTableC1ENS0_4CodeE@PLT
	movl	$19, %edx
	leaq	.LC70(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-112(%rbp), %eax
	movq	%r13, %rdi
	leal	12(%rax), %esi
	imull	-116(%rbp), %esi
	addl	$8, %esi
	call	_ZNSolsEi@PLT
	movl	$2, %edx
	leaq	.LC22(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-116(%rbp), %r11d
	leaq	-144(%rbp), %rax
	movq	%rax, -176(%rbp)
	testl	%r11d, %r11d
	je	.L611
	.p2align 4,,10
	.p2align 3
.L603:
	movq	(%rbx), %rdx
	movq	-104(%rbp), %rax
	leal	(%r15,%r15,2), %r14d
	sall	$2, %r14d
	movl	43(%rdx), %r10d
	movl	(%rax,%r14), %r8d
	leaq	63(%rdx), %rax
	testl	%r10d, %r10d
	js	.L662
.L606:
	leaq	(%r8,%rax), %rsi
	movq	%r13, %rdi
	movq	%r8, -160(%rbp)
	call	_ZNSo9_M_insertIPKvEERSoT_@PLT
	movl	$2, %edx
	leaq	.LC28(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	0(%r13), %rax
	movq	-160(%rbp), %r8
	movq	%r13, %rdi
	movq	-24(%rax), %rax
	movq	%r8, %rsi
	movq	$6, 16(%r13,%rax)
	movq	0(%r13), %rax
	movq	-24(%rax), %rdx
	addq	%r13, %rdx
	movl	24(%rdx), %eax
	andl	$-75, %eax
	orl	$8, %eax
	movl	%eax, 24(%rdx)
	call	_ZNSo9_M_insertImEERSoT_@PLT
	leaq	.LC28(%rip), %rsi
	movl	$2, %edx
	movq	%rax, %rdi
	movq	%rax, -160(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-160(%rbp), %rcx
	movq	(%rcx), %rax
	movq	-24(%rax), %rax
	movq	$4, 16(%rcx,%rax)
	movq	-104(%rbp), %rax
	movl	8(%r14,%rax), %esi
	cmpl	$-1, %esi
	je	.L663
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	-24(%rax), %rdx
	addq	%r13, %rdx
	movl	24(%rdx), %eax
	andl	$-75, %eax
	orl	$8, %eax
	movl	%eax, 24(%rdx)
	call	_ZNSolsEi@PLT
	movq	(%rax), %rdx
	addq	-24(%rdx), %rax
	movl	24(%rax), %edx
	andl	$-75, %edx
	orl	$2, %edx
	movl	%edx, 24(%rax)
.L608:
	movq	0(%r13), %rax
	leaq	.LC28(%rip), %rsi
	movq	%r13, %rdi
	movq	-24(%rax), %rdx
	addq	%r13, %rdx
	movl	24(%rdx), %eax
	andl	$-75, %eax
	orl	$2, %eax
	movl	%eax, 24(%rdx)
	movl	$2, %edx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r13, %rdx
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal14SafepointTable10PrintEntryEjRSo@PLT
	leaq	.LC71(%rip), %rsi
	movl	$13, %edx
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-104(%rbp), %rax
	movl	4(%r14,%rax), %esi
	cmpl	$-1, %esi
	je	.L609
	movq	0(%r13), %rax
	movq	%r13, %rdi
	addl	$1, %r15d
	movq	-24(%rax), %rax
	movq	$6, 16(%r13,%rax)
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC23(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r15d, -116(%rbp)
	ja	.L603
.L611:
	movl	$1, %edx
	leaq	.LC23(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movl	55(%rax), %edx
	subl	51(%rax), %edx
	testl	%edx, %edx
	jle	.L612
.L658:
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12HandlerTableC1ENS0_4CodeE@PLT
	movl	$22, %edx
	leaq	.LC73(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal12HandlerTable21NumberOfReturnEntriesEv@PLT
	movq	%r13, %rdi
	movl	%eax, %esi
	call	_ZNSolsEi@PLT
	movl	$2, %edx
	leaq	.LC22(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	testb	$62, 43(%rax)
	je	.L664
.L613:
	movl	$1, %edx
	leaq	.LC23(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L609:
	movl	$6, %edx
	leaq	.LC72(%rip), %rsi
	movq	%r13, %rdi
	addl	$1, %r15d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	leaq	.LC23(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	-116(%rbp), %r15d
	jb	.L603
	jmp	.L611
	.p2align 4,,10
	.p2align 3
.L663:
	movl	$2, %edx
	leaq	.LC29(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L608
	.p2align 4,,10
	.p2align 3
.L662:
	movl	%r8d, -160(%rbp)
	call	_ZN2v88internal7Isolate19CurrentEmbeddedBlobEv@PLT
	movl	-160(%rbp), %r8d
	testq	%rax, %rax
	je	.L665
	movl	%r8d, -168(%rbp)
	call	_ZN2v88internal7Isolate23CurrentEmbeddedBlobSizeEv@PLT
	movl	%eax, -160(%rbp)
	call	_ZN2v88internal7Isolate19CurrentEmbeddedBlobEv@PLT
	movl	-160(%rbp), %edx
	movq	-176(%rbp), %rdi
	movq	%rax, -144(%rbp)
	movq	(%rbx), %rax
	movl	%edx, -136(%rbp)
	movl	59(%rax), %esi
	call	_ZNK2v88internal12EmbeddedData25InstructionStartOfBuiltinEi@PLT
	movl	-168(%rbp), %r8d
	jmp	.L606
	.p2align 4,,10
	.p2align 3
.L665:
	movq	(%rbx), %rax
	addq	$63, %rax
	jmp	.L606
	.p2align 4,,10
	.p2align 3
.L578:
	movq	%r14, %r9
	cmpq	%r15, %r14
	je	.L666
.L580:
	movq	-152(%rbp), %rdi
	leaq	8(%r9), %rax
	movq	%rax, 41088(%rdi)
	movq	%rsi, (%r9)
	jmp	.L579
	.p2align 4,,10
	.p2align 3
.L657:
	movq	15(%rax), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal18DeoptimizationData23DeoptimizationDataPrintERSo
	jmp	.L600
	.p2align 4,,10
	.p2align 3
.L654:
	movl	$14, %edx
	movq	%r13, %rdi
	leaq	.LC64(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movq	%r13, %rdi
	movl	43(%rax), %esi
	shrl	$7, %esi
	andl	$16777215, %esi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC23(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L570
	.p2align 4,,10
	.p2align 3
.L659:
	movl	$22, %edx
	leaq	.LC75(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rdx
	movq	%r13, %rdi
	movl	39(%rdx), %eax
	addl	$71, %eax
	andl	$-8, %eax
	cltq
	movq	-1(%rdx,%rax), %rsi
	call	_ZNSolsEi@PLT
	movl	$2, %edx
	leaq	.LC22(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	39(%rdx), %eax
	addl	$71, %eax
	andl	$-8, %eax
	cltq
	addq	%rdx, %rax
	leaq	7(%rax), %rdx
	movslq	-1(%rax), %rax
	movq	%rdx, -128(%rbp)
	addq	%rdx, %rax
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal19EhFrameDisassembler19DisassembleToStreamERSo@PLT
	movl	$1, %edx
	leaq	.LC23(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rcx
	movl	43(%rcx), %eax
	testl	%eax, %eax
	jns	.L618
.L660:
	call	_ZN2v88internal7Isolate19CurrentEmbeddedBlobEv@PLT
	testq	%rax, %rax
	je	.L667
	call	_ZN2v88internal7Isolate23CurrentEmbeddedBlobSizeEv@PLT
	movl	%eax, %r14d
	call	_ZN2v88internal7Isolate19CurrentEmbeddedBlobEv@PLT
	movl	%r14d, -120(%rbp)
	movq	%r12, %rdi
	movq	%rax, -128(%rbp)
	movq	(%rbx), %rax
	movl	59(%rax), %esi
	call	_ZNK2v88internal12EmbeddedData24InstructionSizeOfBuiltinEi@PLT
	movq	(%rbx), %rcx
.L620:
	movslq	55(%rcx), %rsi
	subl	%esi, %eax
	testl	%eax, %eax
	jle	.L550
	movl	43(%rcx), %r9d
	testl	%r9d, %r9d
	jns	.L668
	call	_ZN2v88internal7Isolate19CurrentEmbeddedBlobEv@PLT
	testq	%rax, %rax
	je	.L669
	call	_ZN2v88internal7Isolate23CurrentEmbeddedBlobSizeEv@PLT
	movl	%eax, %r14d
	call	_ZN2v88internal7Isolate19CurrentEmbeddedBlobEv@PLT
	movl	%r14d, -120(%rbp)
	movq	%r12, %rdi
	movq	%rax, -128(%rbp)
	movq	(%rbx), %rax
	movl	59(%rax), %esi
	call	_ZNK2v88internal12EmbeddedData24InstructionSizeOfBuiltinEi@PLT
	movq	(%rbx), %rcx
	movl	%eax, %edx
.L626:
	movslq	55(%rcx), %rsi
	movl	43(%rcx), %r8d
	subl	%esi, %edx
	testl	%r8d, %r8d
	jns	.L627
	movl	%edx, -152(%rbp)
	call	_ZN2v88internal7Isolate19CurrentEmbeddedBlobEv@PLT
	movl	-152(%rbp), %edx
	testq	%rax, %rax
	je	.L670
	movl	%edx, -152(%rbp)
	call	_ZN2v88internal7Isolate23CurrentEmbeddedBlobSizeEv@PLT
	movl	%eax, %r14d
	call	_ZN2v88internal7Isolate19CurrentEmbeddedBlobEv@PLT
	movl	%r14d, -120(%rbp)
	movq	%r12, %rdi
	movq	%rax, -128(%rbp)
	movq	(%rbx), %rax
	movl	59(%rax), %esi
	call	_ZNK2v88internal12EmbeddedData25InstructionStartOfBuiltinEi@PLT
	movl	-152(%rbp), %edx
	movq	%rax, %rcx
	movq	(%rbx), %rax
	movslq	55(%rax), %rsi
	jmp	.L629
.L668:
	movl	39(%rcx), %edx
	subl	%esi, %edx
	.p2align 4,,10
	.p2align 3
.L627:
	addq	$63, %rcx
.L629:
	addq	%rcx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal24PrintCodeCommentsSectionERSomj@PLT
	jmp	.L550
	.p2align 4,,10
	.p2align 3
.L656:
	call	_ZN2v88internal7Isolate19CurrentEmbeddedBlobEv@PLT
	testq	%rax, %rax
	je	.L671
	call	_ZN2v88internal7Isolate23CurrentEmbeddedBlobSizeEv@PLT
	movl	%eax, %r14d
	call	_ZN2v88internal7Isolate19CurrentEmbeddedBlobEv@PLT
	movl	%r14d, -120(%rbp)
	movq	%r12, %rdi
	movq	%rax, -128(%rbp)
	movq	(%rbx), %rax
	movl	59(%rax), %esi
	call	_ZNK2v88internal12EmbeddedData25InstructionStartOfBuiltinEi@PLT
	movq	(%rbx), %rsi
	movq	%rax, %rdx
	jmp	.L577
	.p2align 4,,10
	.p2align 3
.L655:
	movslq	39(%rax), %r12
	movl	$19, %edx
	leaq	.LC66(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r13, %rdi
	movl	%r12d, %esi
	call	_ZNSolsEi@PLT
	leaq	.LC22(%rip), %rsi
	movl	$2, %edx
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-152(%rbp), %rax
	movq	(%rbx), %rsi
	movq	41112(%rax), %rdi
	leaq	63(%rsi), %r15
	addl	$1, 41104(%rax)
	leaq	(%r12,%r15), %rcx
	movq	41096(%rax), %r14
	movq	41088(%rax), %r12
	testq	%rdi, %rdi
	je	.L572
	movq	%rcx, -168(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-168(%rbp), %rcx
	movq	%rax, %r9
.L573:
	subq	$8, %rsp
	movq	%r15, %rdx
	pushq	-160(%rbp)
	movq	%r13, %rsi
	movq	-152(%rbp), %r15
	movl	$1, %r8d
	movq	%r15, %rdi
	call	_ZN2v88internal12Disassembler6DecodeEPNS0_7IsolateEPSoPhS5_NS0_13CodeReferenceEm@PLT
	subl	$1, 41104(%r15)
	movq	%r12, 41088(%r15)
	popq	%rdx
	popq	%rcx
	cmpq	41096(%r15), %r14
	je	.L635
	movq	-152(%rbp), %rax
	movq	%r14, 41096(%rax)
	movq	%rax, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L635:
	movl	$1, %edx
	leaq	.LC23(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	jmp	.L637
	.p2align 4,,10
	.p2align 3
.L653:
	movl	$7, %edx
	leaq	.LC63(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	call	strlen@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	leaq	.LC23(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L568
	.p2align 4,,10
	.p2align 3
.L652:
	movq	(%rbx), %rsi
	movl	43(%rsi), %eax
	shrl	%eax
	andl	$31, %eax
	cmpl	$1, %eax
	je	.L672
	movq	-152(%rbp), %rax
	addq	$63, %rsi
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins6LookupEm@PLT
	movq	%rax, %r12
.L567:
	testq	%r12, %r12
	je	.L568
	jmp	.L565
	.p2align 4,,10
	.p2align 3
.L664:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12HandlerTable23HandlerTableReturnPrintERSo@PLT
	jmp	.L613
	.p2align 4,,10
	.p2align 3
.L572:
	movq	%r12, %r9
	cmpq	%r14, %r12
	je	.L673
.L574:
	movq	-152(%rbp), %rdi
	leaq	8(%r9), %rax
	movq	%rax, 41088(%rdi)
	movq	%rsi, (%r9)
	jmp	.L573
	.p2align 4,,10
	.p2align 3
.L667:
	movq	(%rbx), %rcx
	movl	39(%rcx), %eax
	jmp	.L620
	.p2align 4,,10
	.p2align 3
.L671:
	movq	(%rbx), %rsi
	leaq	63(%rsi), %rdx
	jmp	.L577
	.p2align 4,,10
	.p2align 3
.L594:
	movq	-1(%rsi), %rax
	cmpw	$70, 11(%rax)
	jne	.L596
	jmp	.L593
	.p2align 4,,10
	.p2align 3
.L584:
	movq	-1(%rsi), %rax
	cmpw	$70, 11(%rax)
	jne	.L586
	jmp	.L583
.L666:
	movq	%rax, %rdi
	movq	%rcx, -184(%rbp)
	movq	%rdx, -176(%rbp)
	movq	%rsi, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-184(%rbp), %rcx
	movq	-176(%rbp), %rdx
	movq	-168(%rbp), %rsi
	movq	%rax, %r9
	jmp	.L580
.L672:
	movq	-152(%rbp), %rax
	movq	41504(%rax), %rdi
	call	_ZN2v88internal11interpreter11Interpreter27LookupNameOfBytecodeHandlerENS0_4CodeE@PLT
	movq	%rax, %r12
	jmp	.L567
.L669:
	movq	(%rbx), %rcx
	movl	39(%rcx), %edx
	jmp	.L626
.L670:
	movq	(%rbx), %rax
	movslq	55(%rax), %rsi
	leaq	63(%rax), %rcx
	jmp	.L629
.L673:
	movq	%rax, %rdi
	movq	%rcx, -176(%rbp)
	movq	%rsi, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-176(%rbp), %rcx
	movq	-168(%rbp), %rsi
	movq	%rax, %r9
	jmp	.L574
.L551:
	leaq	.LC14(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L661:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19715:
	.size	_ZN2v88internal4Code11DisassembleEPKcRSom, .-_ZN2v88internal4Code11DisassembleEPKcRSom
	.section	.rodata._ZN2v88internal13BytecodeArray11DisassembleERSo.str1.1,"aMS",@progbits,1
.LC80:
	.string	" S> "
.LC81:
	.string	" E> "
.LC82:
	.string	"Parameter count "
.LC83:
	.string	"Register count "
.LC84:
	.string	"Frame size "
.LC85:
	.string	"         "
.LC86:
	.string	" @ "
.LC87:
	.string	" : "
.LC88:
	.string	")"
.LC89:
	.string	" {"
.LC90:
	.string	","
.LC91:
	.string	": @"
.LC92:
	.string	" }"
.LC93:
	.string	"Constant pool (size = "
	.section	.text._ZN2v88internal13BytecodeArray11DisassembleERSo,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13BytecodeArray11DisassembleERSo
	.type	_ZN2v88internal13BytecodeArray11DisassembleERSo, @function
_ZN2v88internal13BytecodeArray11DisassembleERSo:
.LFB19717:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$16, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	leaq	.LC82(%rip), %rsi
	pushq	%rbx
	movq	%r12, %rdi
	subq	$264, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r15), %rax
	movq	%r12, %rdi
	movl	43(%rax), %esi
	sarl	$3, %esi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC23(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$15, %edx
	leaq	.LC83(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r15), %rax
	movq	%r12, %rdi
	movl	39(%rax), %eax
	testl	%eax, %eax
	leal	7(%rax), %esi
	cmovns	%eax, %esi
	sarl	$3, %esi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC23(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$11, %edx
	leaq	.LC84(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r15), %rax
	movq	%r12, %rdi
	movl	39(%rax), %esi
	call	_ZNSolsEi@PLT
	leaq	.LC23(%rip), %rsi
	movl	$1, %edx
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r15), %rax
	movq	31(%rax), %rsi
	leaq	53(%rax), %rcx
	movq	%rcx, -296(%rbp)
	testb	$1, %sil
	jne	.L709
.L675:
	andq	$-262144, %rax
	movq	24(%rax), %rax
	cmpq	-37280(%rax), %rsi
	je	.L710
	movq	7(%rsi), %rsi
.L679:
	leaq	-128(%rbp), %rax
	xorl	%edx, %edx
	leaq	-256(%rbp), %r13
	movq	%rax, %rdi
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal27SourcePositionTableIteratorC1ENS0_9ByteArrayENS1_15IterationFilterE@PLT
	movq	(%r15), %rax
	leaq	-264(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, -264(%rbp)
	call	_ZN2v88internal11interpreter21BytecodeArrayIteratorC1ENS0_6HandleINS0_13BytecodeArrayEEE@PLT
	jmp	.L692
	.p2align 4,,10
	.p2align 3
.L715:
	movsbl	67(%r14), %esi
.L691:
	movq	%r12, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter21BytecodeArrayIterator7AdvanceEv@PLT
.L692:
	movq	%r13, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayIterator4doneEv@PLT
	testb	%al, %al
	jne	.L681
	cmpl	$-1, -104(%rbp)
	je	.L682
	movl	-248(%rbp), %eax
	cmpl	%eax, -96(%rbp)
	je	.L711
.L682:
	movl	$9, %edx
	leaq	.LC85(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L695:
	movslq	-248(%rbp), %rbx
	addq	-296(%rbp), %rbx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZNSo9_M_insertIPKvEERSoT_@PLT
	movl	$3, %edx
	leaq	.LC86(%rip), %rsi
	movq	%rax, %r14
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rax
	movl	-248(%rbp), %esi
	movq	%r14, %rdi
	movq	-24(%rax), %rax
	movq	$4, 16(%r14,%rax)
	call	_ZNSolsEi@PLT
	movl	$3, %edx
	leaq	.LC87(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r15), %rax
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	43(%rax), %edx
	sarl	$3, %edx
	call	_ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor16current_bytecodeEv@PLT
	addl	$118, %eax
	cmpb	$22, %al
	jbe	.L712
.L684:
	movq	%r13, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor16current_bytecodeEv@PLT
	cmpb	$-95, %al
	sete	%bl
	cmpb	$-81, %al
	sete	%al
	orb	%al, %bl
	jne	.L713
.L685:
	movq	(%r12), %rax
	movq	-24(%rax), %rax
	movq	240(%r12,%rax), %r14
	testq	%r14, %r14
	je	.L714
	cmpb	$0, 56(%r14)
	jne	.L715
	movq	%r14, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r14), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L691
	movq	%r14, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L691
	.p2align 4,,10
	.p2align 3
.L712:
	movq	%r13, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetJumpTargetOffsetEv@PLT
	movl	$2, %edx
	movq	%r12, %rdi
	leaq	.LC50(%rip), %rsi
	movslq	%eax, %r14
	addq	-296(%rbp), %r14
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNSo9_M_insertIPKvEERSoT_@PLT
	movl	$3, %edx
	leaq	.LC86(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetJumpTargetOffsetEv@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC88(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L684
	.p2align 4,,10
	.p2align 3
.L711:
	movq	(%r12), %rax
	movq	-88(%rbp), %rsi
	movq	%r12, %rdi
	movq	-24(%rax), %rax
	shrq	%rsi
	andl	$1073741823, %esi
	movq	$5, 16(%r12,%rax)
	subl	$1, %esi
	call	_ZNSolsEi@PLT
	cmpb	$0, -80(%rbp)
	movq	%r12, %rdi
	movl	$4, %edx
	leaq	.LC80(%rip), %rax
	leaq	.LC81(%rip), %rsi
	cmovne	%rax, %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-304(%rbp), %rdi
	call	_ZN2v88internal27SourcePositionTableIterator7AdvanceEv@PLT
	jmp	.L695
	.p2align 4,,10
	.p2align 3
.L713:
	movl	$2, %edx
	movq	%r12, %rdi
	leaq	.LC89(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-224(%rbp), %r8
	movq	%r13, %rsi
	leaq	-192(%rbp), %r14
	movq	%r8, %rdi
	movq	%r8, -280(%rbp)
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor25GetJumpTableTargetOffsetsEv@PLT
	movq	-280(%rbp), %r8
	movq	%r14, %rdi
	movq	%r8, %rsi
	call	_ZNK2v88internal11interpreter22JumpTableTargetOffsets5beginEv@PLT
	movq	-280(%rbp), %r8
	leaq	-160(%rbp), %rdi
	movq	%rdi, -288(%rbp)
	movq	%r8, %rsi
	call	_ZNK2v88internal11interpreter22JumpTableTargetOffsets3endEv@PLT
	jmp	.L688
	.p2align 4,,10
	.p2align 3
.L687:
	movl	$1, %edx
	leaq	.LC25(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-280(%rbp), %esi
	movq	%r12, %rdi
	call	_ZNSolsEi@PLT
	movl	$3, %edx
	leaq	.LC91(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-280(%rbp), %rsi
	movq	%rbx, %rdi
	xorl	%ebx, %ebx
	sarq	$32, %rsi
	call	_ZNSolsEi@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter22JumpTableTargetOffsets8iteratorppEv@PLT
.L688:
	movq	-288(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter22JumpTableTargetOffsets8iteratorneERKS3_@PLT
	testb	%al, %al
	je	.L686
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter22JumpTableTargetOffsets8iteratordeEv@PLT
	movq	%rax, -280(%rbp)
	testb	%bl, %bl
	jne	.L687
	movl	$1, %edx
	leaq	.LC90(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L687
	.p2align 4,,10
	.p2align 3
.L686:
	movl	$2, %edx
	leaq	.LC92(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L685
	.p2align 4,,10
	.p2align 3
.L681:
	movl	$22, %edx
	leaq	.LC93(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r15), %rax
	movq	%r12, %rdi
	movq	15(%rax), %rax
	movslq	11(%rax), %rsi
	call	_ZNSolsEi@PLT
	movl	$2, %edx
	leaq	.LC22(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$22, %edx
	leaq	.LC73(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r15), %rax
	movq	%r12, %rdi
	movq	23(%rax), %rax
	movslq	11(%rax), %rsi
	call	_ZNSolsEi@PLT
	leaq	.LC22(%rip), %rsi
	movl	$2, %edx
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r15), %rsi
	movq	23(%rsi), %rax
	movl	11(%rax), %eax
	testl	%eax, %eax
	jle	.L693
	leaq	-160(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal12HandlerTableC1ENS0_13BytecodeArrayE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal12HandlerTable22HandlerTableRangePrintERSo@PLT
.L693:
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L674
	movq	(%rdi), %rax
	call	*72(%rax)
.L674:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L716
	addq	$264, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L709:
	.cfi_restore_state
	movq	%rsi, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	leaq	-37592(%rdx), %rcx
	cmpq	-37504(%rdx), %rsi
	jne	.L717
.L676:
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-36616(%rax), %rsi
	jmp	.L679
	.p2align 4,,10
	.p2align 3
.L717:
	cmpq	312(%rcx), %rsi
	je	.L676
	movq	-1(%rsi), %rdx
	cmpw	$70, 11(%rdx)
	jne	.L675
	jmp	.L679
	.p2align 4,,10
	.p2align 3
.L710:
	movq	-36616(%rax), %rsi
	jmp	.L679
.L714:
	call	_ZSt16__throw_bad_castv@PLT
.L716:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19717:
	.size	_ZN2v88internal13BytecodeArray11DisassembleERSo, .-_ZN2v88internal13BytecodeArray11DisassembleERSo
	.section	.text._ZN2v88internal13BytecodeArray15CopyBytecodesToES1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13BytecodeArray15CopyBytecodesToES1_
	.type	_ZN2v88internal13BytecodeArray15CopyBytecodesToES1_, @function
_ZN2v88internal13BytecodeArray15CopyBytecodesToES1_:
.LFB19724:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rcx
	movslq	11(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.L724
	ret
	.p2align 4,,10
	.p2align 3
.L724:
	leaq	53(%rcx), %r8
	leaq	53(%rsi), %rdi
	movq	%r8, %rax
	cmpq	$7, %rdx
	ja	.L721
	addq	%r8, %rdx
	subq	%rcx, %rsi
.L723:
	movq	%rax, %rdi
	leaq	(%rax,%rsi), %rcx
	addq	$1, %rax
	movzbl	(%rdi), %edi
	movb	%dil, (%rcx)
	cmpq	%rdx, %rax
	jne	.L723
	ret
	.p2align 4,,10
	.p2align 3
.L721:
	movq	%r8, %rsi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE19724:
	.size	_ZN2v88internal13BytecodeArray15CopyBytecodesToES1_, .-_ZN2v88internal13BytecodeArray15CopyBytecodesToES1_
	.section	.text._ZN2v88internal13BytecodeArray9MakeOlderEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13BytecodeArray9MakeOlderEv
	.type	_ZN2v88internal13BytecodeArray9MakeOlderEv, @function
_ZN2v88internal13BytecodeArray9MakeOlderEv:
.LFB19725:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	leaq	52(%rax), %rdx
	movzbl	52(%rax), %eax
	cmpb	$4, %al
	jg	.L725
	leal	1(%rax), %ecx
	lock cmpxchgb	%cl, (%rdx)
.L725:
	ret
	.cfi_endproc
.LFE19725:
	.size	_ZN2v88internal13BytecodeArray9MakeOlderEv, .-_ZN2v88internal13BytecodeArray9MakeOlderEv
	.section	.text._ZNK2v88internal13BytecodeArray5IsOldEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal13BytecodeArray5IsOldEv
	.type	_ZNK2v88internal13BytecodeArray5IsOldEv, @function
_ZNK2v88internal13BytecodeArray5IsOldEv:
.LFB19726:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movzbl	52(%rax), %eax
	cmpb	$2, %al
	setg	%al
	ret
	.cfi_endproc
.LFE19726:
	.size	_ZNK2v88internal13BytecodeArray5IsOldEv, .-_ZNK2v88internal13BytecodeArray5IsOldEv
	.section	.text._ZN2v88internal13DependentCode16GetDependentCodeENS0_6HandleINS0_10HeapObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13DependentCode16GetDependentCodeENS0_6HandleINS0_10HeapObjectEEE
	.type	_ZN2v88internal13DependentCode16GetDependentCodeENS0_6HandleINS0_10HeapObjectEEE, @function
_ZN2v88internal13DependentCode16GetDependentCodeENS0_6HandleINS0_10HeapObjectEEE:
.LFB19727:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	-1(%rax), %rdx
	cmpw	$68, 11(%rdx)
	jne	.L729
	movq	55(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L729:
	movq	-1(%rax), %rdx
	cmpw	$159, 11(%rdx)
	jne	.L731
	movq	31(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L731:
	movq	-1(%rax), %rdx
	cmpw	$121, 11(%rdx)
	jne	.L732
	movq	23(%rax), %rax
	ret
.L732:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC14(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19727:
	.size	_ZN2v88internal13DependentCode16GetDependentCodeENS0_6HandleINS0_10HeapObjectEEE, .-_ZN2v88internal13DependentCode16GetDependentCodeENS0_6HandleINS0_10HeapObjectEEE
	.section	.text._ZN2v88internal13DependentCode16SetDependentCodeENS0_6HandleINS0_10HeapObjectEEENS2_IS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13DependentCode16SetDependentCodeENS0_6HandleINS0_10HeapObjectEEENS2_IS1_EE
	.type	_ZN2v88internal13DependentCode16SetDependentCodeENS0_6HandleINS0_10HeapObjectEEENS2_IS1_EE, @function
_ZN2v88internal13DependentCode16SetDependentCodeENS0_6HandleINS0_10HeapObjectEEENS2_IS1_EE:
.LFB19728:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rdi), %r12
	movq	-1(%r12), %rax
	cmpw	$68, 11(%rax)
	je	.L780
	movq	-1(%r12), %rax
	cmpw	$159, 11(%rax)
	je	.L781
	movq	-1(%r12), %rax
	cmpw	$121, 11(%rax)
	jne	.L745
	movq	(%rsi), %r13
	leaq	23(%r12), %r14
	movq	%r13, 23(%r12)
	testb	$1, %r13b
	je	.L735
.L778:
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L747
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
.L747:
	testb	$24, %al
	je	.L735
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L782
.L735:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L781:
	.cfi_restore_state
	movq	(%rsi), %r13
	leaq	31(%r12), %r14
	movq	%r13, 31(%r12)
	testb	$1, %r13b
	jne	.L778
	jmp	.L735
	.p2align 4,,10
	.p2align 3
.L780:
	movq	(%rsi), %r13
	leaq	55(%r12), %r14
	movq	%r13, 55(%r12)
	testb	$1, %r13b
	jne	.L778
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L782:
	.cfi_restore_state
	popq	%rbx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
.L745:
	.cfi_restore_state
	leaq	.LC14(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19728:
	.size	_ZN2v88internal13DependentCode16SetDependentCodeENS0_6HandleINS0_10HeapObjectEEENS2_IS1_EE, .-_ZN2v88internal13DependentCode16SetDependentCodeENS0_6HandleINS0_10HeapObjectEEENS2_IS1_EE
	.section	.rodata._ZN2v88internal13DependentCode3NewEPNS0_7IsolateENS1_15DependencyGroupERKNS0_17MaybeObjectHandleENS0_6HandleIS1_EE.str1.1,"aMS",@progbits,1
.LC94:
	.string	"(location_) != nullptr"
	.section	.text._ZN2v88internal13DependentCode3NewEPNS0_7IsolateENS1_15DependencyGroupERKNS0_17MaybeObjectHandleENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13DependentCode3NewEPNS0_7IsolateENS1_15DependencyGroupERKNS0_17MaybeObjectHandleENS0_6HandleIS1_EE
	.type	_ZN2v88internal13DependentCode3NewEPNS0_7IsolateENS1_15DependencyGroupERKNS0_17MaybeObjectHandleENS0_6HandleIS1_EE, @function
_ZN2v88internal13DependentCode3NewEPNS0_7IsolateENS1_15DependencyGroupERKNS0_17MaybeObjectHandleENS0_6HandleIS1_EE:
.LFB19731:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	movl	$1, %edx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, %ebx
	movl	$3, %esi
	subq	$24, %rsp
	call	_ZN2v88internal7Factory17NewWeakFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	(%rax), %r15
	movq	%rax, %r12
	movq	(%r14), %rax
	movq	%rax, 15(%r15)
	testb	$1, %al
	je	.L795
	cmpl	$3, %eax
	je	.L795
	movq	%rax, %rdx
	andq	$-262144, %rax
	leaq	15(%r15), %rsi
	movq	%rax, %r14
	movq	8(%rax), %rax
	andq	$-3, %rdx
	testl	$262144, %eax
	jne	.L812
.L785:
	testb	$24, %al
	je	.L795
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L813
	.p2align 4,,10
	.p2align 3
.L795:
	movq	(%r12), %rax
	orl	$8, %ebx
	salq	$32, %rbx
	movq	%rbx, 23(%rax)
	movl	0(%r13), %eax
	movq	(%r12), %r14
	testl	%eax, %eax
	movq	8(%r13), %rax
	jne	.L787
	testq	%rax, %rax
	je	.L790
	movq	(%rax), %rax
	orq	$2, %rax
	jmp	.L789
	.p2align 4,,10
	.p2align 3
.L787:
	testq	%rax, %rax
	je	.L790
	movq	(%rax), %rax
.L789:
	movq	%rax, 31(%r14)
	leaq	31(%r14), %r13
	testb	$1, %al
	je	.L794
	cmpl	$3, %eax
	je	.L794
	movq	%rax, %r15
	andq	$-262144, %rax
	movq	%rax, %rbx
	movq	8(%rax), %rax
	andq	$-3, %r15
	testl	$262144, %eax
	jne	.L814
	testb	$24, %al
	je	.L794
.L815:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L794
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
.L794:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L812:
	.cfi_restore_state
	movq	%r15, %rdi
	movq	%rdx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r14), %rax
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rsi
	jmp	.L785
	.p2align 4,,10
	.p2align 3
.L814:
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L815
	jmp	.L794
	.p2align 4,,10
	.p2align 3
.L813:
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L795
	.p2align 4,,10
	.p2align 3
.L790:
	leaq	.LC94(%rip), %rsi
	leaq	.LC17(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19731:
	.size	_ZN2v88internal13DependentCode3NewEPNS0_7IsolateENS1_15DependencyGroupERKNS0_17MaybeObjectHandleENS0_6HandleIS1_EE, .-_ZN2v88internal13DependentCode3NewEPNS0_7IsolateENS1_15DependencyGroupERKNS0_17MaybeObjectHandleENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal13DependentCode7CompactEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13DependentCode7CompactEv
	.type	_ZN2v88internal13DependentCode7CompactEv, @function
_ZN2v88internal13DependentCode7CompactEv:
.LFB19735:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rdx
	movq	23(%rdx), %r13
	movq	%rdx, %rdi
	shrq	$35, %r13
	andl	$134217727, %r13d
	je	.L817
	leal	-1(%r13), %r12d
	xorl	%ebx, %ebx
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L824:
	leaq	0(,%rbx,8), %rax
	leaq	32(%rax), %rcx
	movq	31(%rdi,%rax), %rax
	movq	%rdx, %rdi
	cmpl	$3, %eax
	je	.L819
	cmpl	%ebx, %r14d
	je	.L820
	movq	-1(%rdx,%rcx), %rax
	leal	32(,%r14,8), %ecx
	movslq	%ecx, %rcx
	movq	%rax, -1(%rdx,%rcx)
	movq	(%r15), %rdi
	movq	%rdi, %rdx
	testb	$1, %al
	je	.L820
	cmpl	$3, %eax
	je	.L820
	movq	%rax, %r8
	andq	$-262144, %rax
	movq	%rcx, -64(%rbp)
	leaq	-1(%rcx,%rdi), %rsi
	movq	8(%rax), %rdx
	movq	%rax, -56(%rbp)
	andq	$-3, %r8
	testl	$262144, %edx
	jne	.L852
.L822:
	andl	$24, %edx
	je	.L851
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L849
.L851:
	movq	(%r15), %rdx
	.p2align 4,,10
	.p2align 3
.L820:
	addl	$1, %r14d
.L819:
	leaq	1(%rbx), %rax
	cmpq	%rbx, %r12
	je	.L853
	movq	%rax, %rbx
	jmp	.L824
	.p2align 4,,10
	.p2align 3
.L853:
	leal	0(,%r14,8), %ecx
.L825:
	movq	23(%rdi), %rax
	sarq	$32, %rax
	andl	$-1073741817, %eax
	orl	%ecx, %eax
	salq	$32, %rax
	movq	%rax, 23(%rdx)
	movq	(%r15), %rdx
	cmpl	%r14d, %r13d
	jle	.L826
	leal	-1(%r13), %eax
	movslq	%r14d, %rcx
	leal	32(,%r14,8), %r12d
	subl	%r14d, %eax
	movslq	%r12d, %r12
	leaq	5(%rax,%rcx), %rcx
	salq	$3, %rcx
	jmp	.L830
	.p2align 4,,10
	.p2align 3
.L854:
	movq	(%r15), %rdx
.L830:
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-37504(%rax), %rax
	movq	%rax, -1(%r12,%rdx)
	testb	$1, %al
	je	.L831
	cmpl	$3, %eax
	je	.L831
	movq	%rax, %rdx
	andq	$-262144, %rax
	movq	(%r15), %rdi
	movq	%rax, %rbx
	movq	8(%rax), %rax
	andq	$-3, %rdx
	leaq	-1(%r12,%rdi), %rsi
	testl	$262144, %eax
	je	.L828
	movq	%rcx, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	(%r15), %rdi
	movq	8(%rbx), %rax
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %rdx
	leaq	-1(%r12,%rdi), %rsi
.L828:
	testb	$24, %al
	je	.L831
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L831
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	.p2align 4,,10
	.p2align 3
.L831:
	addq	$8, %r12
	cmpq	%rcx, %r12
	jne	.L854
.L826:
	cmpl	%r14d, %r13d
	setg	%al
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L852:
	.cfi_restore_state
	movq	%r8, %rdx
	movq	%r8, -72(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rax
	movq	(%r15), %rdi
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %r8
	movq	8(%rax), %rdx
	leaq	-1(%rcx,%rdi), %rsi
	jmp	.L822
	.p2align 4,,10
	.p2align 3
.L849:
	movq	%r8, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	(%r15), %rdi
	movq	%rdi, %rdx
	jmp	.L820
	.p2align 4,,10
	.p2align 3
.L817:
	xorl	%r14d, %r14d
	xorl	%ecx, %ecx
	jmp	.L825
	.cfi_endproc
.LFE19735:
	.size	_ZN2v88internal13DependentCode7CompactEv, .-_ZN2v88internal13DependentCode7CompactEv
	.section	.text._ZN2v88internal13DependentCode11EnsureSpaceEPNS0_7IsolateENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13DependentCode11EnsureSpaceEPNS0_7IsolateENS0_6HandleIS1_EE
	.type	_ZN2v88internal13DependentCode11EnsureSpaceEPNS0_7IsolateENS0_6HandleIS1_EE, @function
_ZN2v88internal13DependentCode11EnsureSpaceEPNS0_7IsolateENS0_6HandleIS1_EE:
.LFB19734:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	leaq	-32(%rbp), %rdi
	.cfi_offset 12, -32
	movq	%rsi, %r12
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal13DependentCode7CompactEv
	testb	%al, %al
	je	.L856
	movq	%r12, %rax
.L857:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L862
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L856:
	.cfi_restore_state
	movq	(%r12), %rcx
	movq	23(%rcx), %rax
	shrq	$35, %rax
	andl	$134217727, %eax
	leal	1(%rax), %edx
	cmpl	$4, %eax
	jle	.L859
	leal	(%rax,%rax,4), %edx
	sarl	$2, %edx
.L859:
	addl	$2, %edx
	movq	%r12, %rsi
	subl	11(%rcx), %edx
	movq	%r13, %rdi
	movl	$1, %ecx
	call	_ZN2v88internal7Factory25CopyWeakFixedArrayAndGrowENS0_6HandleINS0_14WeakFixedArrayEEEiNS0_14AllocationTypeE@PLT
	jmp	.L857
.L862:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19734:
	.size	_ZN2v88internal13DependentCode11EnsureSpaceEPNS0_7IsolateENS0_6HandleIS1_EE, .-_ZN2v88internal13DependentCode11EnsureSpaceEPNS0_7IsolateENS0_6HandleIS1_EE
	.section	.text.unlikely._ZN2v88internal13DependentCode14InsertWeakCodeEPNS0_7IsolateENS0_6HandleIS1_EENS1_15DependencyGroupERKNS0_17MaybeObjectHandleE,"ax",@progbits
	.align 2
.LCOLDB95:
	.section	.text._ZN2v88internal13DependentCode14InsertWeakCodeEPNS0_7IsolateENS0_6HandleIS1_EENS1_15DependencyGroupERKNS0_17MaybeObjectHandleE,"ax",@progbits
.LHOTB95:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13DependentCode14InsertWeakCodeEPNS0_7IsolateENS0_6HandleIS1_EENS1_15DependencyGroupERKNS0_17MaybeObjectHandleE
	.type	_ZN2v88internal13DependentCode14InsertWeakCodeEPNS0_7IsolateENS0_6HandleIS1_EENS1_15DependencyGroupERKNS0_17MaybeObjectHandleE, @function
_ZN2v88internal13DependentCode14InsertWeakCodeEPNS0_7IsolateENS0_6HandleIS1_EENS1_15DependencyGroupERKNS0_17MaybeObjectHandleE:
.LFB19730:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	movq	%rcx, %rdx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rdi
	movl	11(%rdi), %r8d
	testl	%r8d, %r8d
	je	.L865
	movq	23(%rdi), %rax
	sarq	$32, %rax
	andl	$7, %eax
	cmpl	%eax, %r13d
	jge	.L919
.L865:
	addq	$24, %rsp
	movq	%r12, %rcx
	movl	%r13d, %esi
	movq	%r14, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal13DependentCode3NewEPNS0_7IsolateENS1_15DependencyGroupERKNS0_17MaybeObjectHandleENS0_6HandleIS1_EE
	.p2align 4,,10
	.p2align 3
.L919:
	.cfi_restore_state
	movq	23(%rdi), %rax
	movq	%rsi, %r15
	sarq	$32, %rax
	andl	$7, %eax
	cmpl	%eax, %r13d
	jle	.L866
	movq	15(%rdi), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L867
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-56(%rbp), %rdx
	movq	%r14, %rdi
	movq	%rax, %rsi
	movq	%rax, %rbx
	movq	%rdx, %rcx
	movl	%r13d, %edx
	call	_ZN2v88internal13DependentCode14InsertWeakCodeEPNS0_7IsolateENS0_6HandleIS1_EENS1_15DependencyGroupERKNS0_17MaybeObjectHandleE
	cmpq	%rax, %rbx
	je	.L910
	testq	%rbx, %rbx
	je	.L920
.L872:
	testq	%rax, %rax
	je	.L874
	movq	(%rax), %rax
	cmpq	(%rbx), %rax
	je	.L910
.L873:
	movq	(%r12), %r12
	movq	%rax, 15(%r12)
	leaq	15(%r12), %r13
	testb	$1, %al
	je	.L910
	cmpl	$3, %eax
	je	.L910
	movq	%rax, %r14
	andq	$-262144, %rax
	movq	%rax, %rbx
	movq	8(%rax), %rax
	andq	$-3, %r14
	testl	$262144, %eax
	je	.L876
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
.L876:
	testb	$24, %al
	je	.L910
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L910
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
.L910:
	addq	$24, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L866:
	.cfi_restore_state
	movq	23(%rdi), %rax
	shrq	$35, %rax
	andl	$134217727, %eax
	movl	%eax, %r13d
	je	.L887
	leal	-1(%rax), %eax
	movq	8(%rcx), %rsi
	leaq	40(,%rax,8), %r9
	movl	$32, %eax
	jmp	.L888
	.p2align 4,,10
	.p2align 3
.L921:
	testq	%rsi, %rsi
	je	.L885
	movq	(%rsi), %rcx
	orq	$2, %rcx
.L884:
	movq	-1(%rax,%rdi), %r8
	cmpq	%r8, %rcx
	je	.L910
	addq	$8, %rax
	cmpq	%r9, %rax
	je	.L887
.L888:
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	je	.L921
	testq	%rsi, %rsi
	je	.L885
	movq	(%rsi), %rcx
	jmp	.L884
	.p2align 4,,10
	.p2align 3
.L867:
	movq	41088(%r14), %rbx
	cmpq	41096(%r14), %rbx
	je	.L922
.L870:
	leaq	8(%rbx), %rax
	movq	%rdx, %rcx
	movq	%r14, %rdi
	movl	%r13d, %edx
	movq	%rax, 41088(%r14)
	movq	%rsi, (%rbx)
	movq	%rbx, %rsi
	call	_ZN2v88internal13DependentCode14InsertWeakCodeEPNS0_7IsolateENS0_6HandleIS1_EENS1_15DependencyGroupERKNS0_17MaybeObjectHandleE
	cmpq	%rax, %rbx
	jne	.L872
	jmp	.L910
	.p2align 4,,10
	.p2align 3
.L885:
	leaq	.LC94(%rip), %rsi
	leaq	.LC17(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L887:
	leal	2(%r13), %eax
	cmpl	%eax, 11(%rdi)
	jle	.L923
.L881:
	movq	8(%rdx), %rax
	movl	(%rdx), %edx
	testl	%edx, %edx
	jne	.L889
	testq	%rax, %rax
	je	.L885
	movq	(%rax), %rax
	orq	$2, %rax
.L890:
	leal	32(,%r13,8), %edx
	movslq	%edx, %rdx
	leaq	-1(%rdi,%rdx), %r12
	movq	%rax, (%r12)
	testb	$1, %al
	je	.L895
	cmpl	$3, %eax
	je	.L895
	movq	%rax, %r14
	andq	$-262144, %rax
	movq	%rax, %rbx
	movq	8(%rax), %rax
	andq	$-3, %r14
	testl	$262144, %eax
	je	.L892
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%rdi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-56(%rbp), %rdi
.L892:
	testb	$24, %al
	je	.L895
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L895
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L895:
	movq	(%r15), %rdx
	leal	8(,%r13,8), %ecx
	movq	23(%rdx), %rax
	sarq	$32, %rax
	andl	$-1073741817, %eax
	orl	%ecx, %eax
	salq	$32, %rax
	movq	%rax, 23(%rdx)
	jmp	.L910
	.p2align 4,,10
	.p2align 3
.L920:
	movq	(%rax), %rax
	jmp	.L873
	.p2align 4,,10
	.p2align 3
.L889:
	testq	%rax, %rax
	je	.L885
	movq	(%rax), %rax
	jmp	.L890
.L923:
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal13DependentCode11EnsureSpaceEPNS0_7IsolateENS0_6HandleIS1_EE
	movq	(%rax), %rdi
	movq	%rax, %r15
	movq	23(%rdi), %rax
	movq	-56(%rbp), %rdx
	shrq	$35, %rax
	andl	$134217727, %eax
	movl	%eax, %r13d
	jmp	.L881
.L922:
	movq	%r14, %rdi
	movq	%rsi, -56(%rbp)
	movq	%rcx, -64(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L870
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal13DependentCode14InsertWeakCodeEPNS0_7IsolateENS0_6HandleIS1_EENS1_15DependencyGroupERKNS0_17MaybeObjectHandleE
	.cfi_startproc
	.type	_ZN2v88internal13DependentCode14InsertWeakCodeEPNS0_7IsolateENS0_6HandleIS1_EENS1_15DependencyGroupERKNS0_17MaybeObjectHandleE.cold, @function
_ZN2v88internal13DependentCode14InsertWeakCodeEPNS0_7IsolateENS0_6HandleIS1_EENS1_15DependencyGroupERKNS0_17MaybeObjectHandleE.cold:
.LFSB19730:
.L874:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	0, %rax
	ud2
	.cfi_endproc
.LFE19730:
	.section	.text._ZN2v88internal13DependentCode14InsertWeakCodeEPNS0_7IsolateENS0_6HandleIS1_EENS1_15DependencyGroupERKNS0_17MaybeObjectHandleE
	.size	_ZN2v88internal13DependentCode14InsertWeakCodeEPNS0_7IsolateENS0_6HandleIS1_EENS1_15DependencyGroupERKNS0_17MaybeObjectHandleE, .-_ZN2v88internal13DependentCode14InsertWeakCodeEPNS0_7IsolateENS0_6HandleIS1_EENS1_15DependencyGroupERKNS0_17MaybeObjectHandleE
	.section	.text.unlikely._ZN2v88internal13DependentCode14InsertWeakCodeEPNS0_7IsolateENS0_6HandleIS1_EENS1_15DependencyGroupERKNS0_17MaybeObjectHandleE
	.size	_ZN2v88internal13DependentCode14InsertWeakCodeEPNS0_7IsolateENS0_6HandleIS1_EENS1_15DependencyGroupERKNS0_17MaybeObjectHandleE.cold, .-_ZN2v88internal13DependentCode14InsertWeakCodeEPNS0_7IsolateENS0_6HandleIS1_EENS1_15DependencyGroupERKNS0_17MaybeObjectHandleE.cold
.LCOLDE95:
	.section	.text._ZN2v88internal13DependentCode14InsertWeakCodeEPNS0_7IsolateENS0_6HandleIS1_EENS1_15DependencyGroupERKNS0_17MaybeObjectHandleE
.LHOTE95:
	.section	.text._ZN2v88internal13DependentCode17InstallDependencyEPNS0_7IsolateERKNS0_17MaybeObjectHandleENS0_6HandleINS0_10HeapObjectEEENS1_15DependencyGroupE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13DependentCode17InstallDependencyEPNS0_7IsolateERKNS0_17MaybeObjectHandleENS0_6HandleINS0_10HeapObjectEEENS1_15DependencyGroupE
	.type	_ZN2v88internal13DependentCode17InstallDependencyEPNS0_7IsolateERKNS0_17MaybeObjectHandleENS0_6HandleINS0_10HeapObjectEEENS1_15DependencyGroupE, @function
_ZN2v88internal13DependentCode17InstallDependencyEPNS0_7IsolateERKNS0_17MaybeObjectHandleENS0_6HandleINS0_10HeapObjectEEENS1_15DependencyGroupE:
.LFB19729:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movq	%rdx, %rdi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	call	_ZN2v88internal13DependentCode16GetDependentCodeENS0_6HandleINS0_10HeapObjectEEE
	movq	41112(%r15), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L925
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rbx
.L926:
	movq	%rbx, %rsi
	movq	%r14, %rcx
	movl	%r13d, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal13DependentCode14InsertWeakCodeEPNS0_7IsolateENS0_6HandleIS1_EENS1_15DependencyGroupERKNS0_17MaybeObjectHandleE
	movq	%rax, %rsi
	cmpq	%rbx, %rax
	je	.L924
	testq	%rax, %rax
	je	.L929
	testq	%rbx, %rbx
	je	.L929
	movq	(%rbx), %rax
	cmpq	%rax, (%rsi)
	je	.L924
.L929:
	addq	$24, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal13DependentCode16SetDependentCodeENS0_6HandleINS0_10HeapObjectEEENS2_IS1_EE
	.p2align 4,,10
	.p2align 3
.L924:
	.cfi_restore_state
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L925:
	.cfi_restore_state
	movq	41088(%r15), %rbx
	cmpq	41096(%r15), %rbx
	je	.L931
.L927:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rbx)
	jmp	.L926
	.p2align 4,,10
	.p2align 3
.L931:
	movq	%r15, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L927
	.cfi_endproc
.LFE19729:
	.size	_ZN2v88internal13DependentCode17InstallDependencyEPNS0_7IsolateERKNS0_17MaybeObjectHandleENS0_6HandleINS0_10HeapObjectEEENS1_15DependencyGroupE, .-_ZN2v88internal13DependentCode17InstallDependencyEPNS0_7IsolateERKNS0_17MaybeObjectHandleENS0_6HandleINS0_10HeapObjectEEENS1_15DependencyGroupE
	.section	.rodata._ZN2v88internal4Code26SetMarkedForDeoptimizationEPKc.str1.1,"aMS",@progbits,1
.LC96:
	.string	"ab"
	.section	.rodata._ZN2v88internal4Code26SetMarkedForDeoptimizationEPKc.str1.8,"aMS",@progbits,1
	.align 8
.LC97:
	.string	"[marking dependent code 0x%012lx (opt #%d) for deoptimization, reason: %s]\n"
	.section	.text._ZN2v88internal4Code26SetMarkedForDeoptimizationEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4Code26SetMarkedForDeoptimizationEPKc
	.type	_ZN2v88internal4Code26SetMarkedForDeoptimizationEPKc, @function
_ZN2v88internal4Code26SetMarkedForDeoptimizationEPKc:
.LFB19738:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	31(%rax), %rax
	movl	15(%rax), %eax
	movq	(%rdi), %rdx
	movq	31(%rdx), %rdx
	orl	$1, %eax
	movl	%eax, 15(%rdx)
	cmpb	$0, _ZN2v88internal16FLAG_trace_deoptE(%rip)
	jne	.L945
	ret
	.p2align 4,,10
	.p2align 3
.L945:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rdi), %rdx
	movq	%rdi, %rbx
	movq	%rdx, %rax
	movq	15(%rdx), %r14
	andq	$-262144, %rax
	movq	24(%rax), %rax
	leaq	-37592(%rax), %rdi
	cmpq	%r14, -37304(%rax)
	jne	.L946
.L932:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L946:
	.cfi_restore_state
	movq	%rsi, %r12
	call	_ZN2v88internal7Isolate13GetCodeTracerEv@PLT
	cmpb	$0, _ZN2v88internal25FLAG_redirect_code_tracesE(%rip)
	movq	%rax, %r13
	je	.L936
	cmpq	$0, 144(%rax)
	je	.L947
.L937:
	addl	$1, 152(%r13)
.L936:
	movq	55(%r14), %rcx
	movq	144(%r13), %rdi
	xorl	%eax, %eax
	movq	%r12, %r8
	movq	(%rbx), %rdx
	leaq	.LC97(%rip), %rsi
	sarq	$32, %rcx
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	cmpb	$0, _ZN2v88internal25FLAG_redirect_code_tracesE(%rip)
	je	.L932
	subl	$1, 152(%r13)
	jne	.L932
	movq	144(%r13), %rdi
	call	fclose@PLT
	movq	$0, 144(%r13)
	jmp	.L932
	.p2align 4,,10
	.p2align 3
.L947:
	movq	(%rax), %rdi
	leaq	.LC96(%rip), %rsi
	call	_ZN2v84base2OS5FOpenEPKcS3_@PLT
	movq	%rax, 144(%r13)
	jmp	.L937
	.cfi_endproc
.LFE19738:
	.size	_ZN2v88internal4Code26SetMarkedForDeoptimizationEPKc, .-_ZN2v88internal4Code26SetMarkedForDeoptimizationEPKc
	.section	.rodata._ZN2v88internal13DependentCode25MarkCodeForDeoptimizationEPNS0_7IsolateENS1_15DependencyGroupE.str1.8,"aMS",@progbits,1
	.align 8
.LC98:
	.string	"allocation-site-transition-changed"
	.section	.rodata._ZN2v88internal13DependentCode25MarkCodeForDeoptimizationEPNS0_7IsolateENS1_15DependencyGroupE.str1.1,"aMS",@progbits,1
.LC99:
	.string	"transition"
.LC100:
	.string	"property-cell-changed"
.LC101:
	.string	"field-owner"
.LC102:
	.string	"initial-map-changed"
	.section	.rodata._ZN2v88internal13DependentCode25MarkCodeForDeoptimizationEPNS0_7IsolateENS1_15DependencyGroupE.str1.8
	.align 8
.LC103:
	.string	"allocation-site-tenuring-changed"
	.section	.rodata._ZN2v88internal13DependentCode25MarkCodeForDeoptimizationEPNS0_7IsolateENS1_15DependencyGroupE.str1.1
.LC104:
	.string	"prototype-check"
	.section	.text._ZN2v88internal13DependentCode25MarkCodeForDeoptimizationEPNS0_7IsolateENS1_15DependencyGroupE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13DependentCode25MarkCodeForDeoptimizationEPNS0_7IsolateENS1_15DependencyGroupE
	.type	_ZN2v88internal13DependentCode25MarkCodeForDeoptimizationEPNS0_7IsolateENS1_15DependencyGroupE, @function
_ZN2v88internal13DependentCode25MarkCodeForDeoptimizationEPNS0_7IsolateENS1_15DependencyGroupE:
.LFB19736:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	11(%rdx), %eax
	testl	%eax, %eax
	jne	.L949
.L951:
	xorl	%r15d, %r15d
.L948:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L985
	addq	$40, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L949:
	.cfi_restore_state
	movq	23(%rdx), %rax
	sarq	$32, %rax
	andl	$7, %eax
	cmpl	%eax, %r12d
	jl	.L951
	movq	23(%rdx), %rax
	sarq	$32, %rax
	andl	$7, %eax
	cmpl	%eax, %r12d
	jg	.L986
	movq	23(%rdx), %rax
	shrq	$35, %rax
	andl	$134217727, %eax
	je	.L953
	subl	$1, %eax
	movq	%rdi, %rbx
	movl	$32, %ecx
	xorl	%r15d, %r15d
	leaq	40(,%rax,8), %r14
	leaq	.L959(%rip), %r8
	leaq	.LC100(%rip), %r13
	jmp	.L966
	.p2align 4,,10
	.p2align 3
.L955:
	addq	$8, %rcx
	cmpq	%rcx, %r14
	je	.L983
.L987:
	movq	(%rbx), %rdx
.L966:
	movq	-1(%rdx,%rcx), %rdx
	cmpl	$3, %edx
	je	.L955
	andq	$-3, %rdx
	movq	%rdx, -64(%rbp)
	movq	31(%rdx), %rdx
	movl	15(%rdx), %edx
	andl	$1, %edx
	jne	.L955
	cmpl	$6, %r12d
	ja	.L957
	movl	%r12d, %edx
	movslq	(%r8,%rdx,4), %rdx
	addq	%r8, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal13DependentCode25MarkCodeForDeoptimizationEPNS0_7IsolateENS1_15DependencyGroupE,"a",@progbits
	.align 4
	.align 4
.L959:
	.long	.L965-.L959
	.long	.L964-.L959
	.long	.L974-.L959
	.long	.L962-.L959
	.long	.L961-.L959
	.long	.L960-.L959
	.long	.L958-.L959
	.section	.text._ZN2v88internal13DependentCode25MarkCodeForDeoptimizationEPNS0_7IsolateENS1_15DependencyGroupE
.L974:
	movq	%r13, %rsi
.L963:
	leaq	-64(%rbp), %rdi
	movq	%rcx, -72(%rbp)
	movl	$1, %r15d
	call	_ZN2v88internal4Code26SetMarkedForDeoptimizationEPKc
	movq	-72(%rbp), %rcx
	leaq	.L959(%rip), %r8
	addq	$8, %rcx
	cmpq	%rcx, %r14
	jne	.L987
.L983:
	movq	(%rbx), %rdi
	movl	$32, %r13d
	.p2align 4,,10
	.p2align 3
.L971:
	movq	%rdi, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	movq	-37504(%rdx), %rdx
	movq	%rdx, -1(%rdi,%r13)
	movq	(%rbx), %rdi
	testb	$1, %dl
	je	.L972
	cmpl	$3, %edx
	je	.L972
	movq	%rdx, %r8
	andq	$-262144, %rdx
	leaq	-1(%rdi,%r13), %rsi
	movq	%rdx, %r12
	movq	8(%rdx), %rdx
	andq	$-3, %r8
	testl	$262144, %edx
	je	.L968
	movq	%r8, %rdx
	movq	%r8, -72(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	(%rbx), %rdi
	movq	8(%r12), %rdx
	movq	-72(%rbp), %r8
	leaq	-1(%rdi,%r13), %rsi
.L968:
	andl	$24, %edx
	je	.L972
	movq	%rdi, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	jne	.L972
	movq	%r8, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	(%rbx), %rdi
	.p2align 4,,10
	.p2align 3
.L972:
	addq	$8, %r13
	cmpq	%r14, %r13
	jne	.L971
	movq	(%rbx), %rdx
.L970:
	movq	23(%rdi), %rax
	sarq	$32, %rax
	andl	$-1073741817, %eax
	salq	$32, %rax
	movq	%rax, 23(%rdx)
	jmp	.L948
.L964:
	leaq	.LC104(%rip), %rsi
	jmp	.L963
	.p2align 4,,10
	.p2align 3
.L986:
	movq	15(%rdx), %rax
	leaq	-64(%rbp), %rdi
	movl	%r12d, %edx
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal13DependentCode25MarkCodeForDeoptimizationEPNS0_7IsolateENS1_15DependencyGroupE
	movl	%eax, %r15d
	jmp	.L948
.L960:
	leaq	.LC103(%rip), %rsi
	jmp	.L963
.L961:
	leaq	.LC102(%rip), %rsi
	jmp	.L963
.L962:
	leaq	.LC101(%rip), %rsi
	jmp	.L963
.L965:
	leaq	.LC99(%rip), %rsi
	jmp	.L963
.L958:
	leaq	.LC98(%rip), %rsi
	jmp	.L963
	.p2align 4,,10
	.p2align 3
.L953:
	movq	%rdx, %rdi
	xorl	%r15d, %r15d
	jmp	.L970
.L985:
	call	__stack_chk_fail@PLT
.L957:
	leaq	.LC14(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19736:
	.size	_ZN2v88internal13DependentCode25MarkCodeForDeoptimizationEPNS0_7IsolateENS1_15DependencyGroupE, .-_ZN2v88internal13DependentCode25MarkCodeForDeoptimizationEPNS0_7IsolateENS1_15DependencyGroupE
	.section	.text._ZN2v88internal13DependentCode28DeoptimizeDependentCodeGroupEPNS0_7IsolateENS1_15DependencyGroupE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13DependentCode28DeoptimizeDependentCodeGroupEPNS0_7IsolateENS1_15DependencyGroupE
	.type	_ZN2v88internal13DependentCode28DeoptimizeDependentCodeGroupEPNS0_7IsolateENS1_15DependencyGroupE, @function
_ZN2v88internal13DependentCode28DeoptimizeDependentCodeGroupEPNS0_7IsolateENS1_15DependencyGroupE:
.LFB19737:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal13DependentCode25MarkCodeForDeoptimizationEPNS0_7IsolateENS1_15DependencyGroupE
	testb	%al, %al
	jne	.L991
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L991:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11Deoptimizer20DeoptimizeMarkedCodeEPNS0_7IsolateE@PLT
	.cfi_endproc
.LFE19737:
	.size	_ZN2v88internal13DependentCode28DeoptimizeDependentCodeGroupEPNS0_7IsolateENS1_15DependencyGroupE, .-_ZN2v88internal13DependentCode28DeoptimizeDependentCodeGroupEPNS0_7IsolateENS1_15DependencyGroupE
	.section	.text._ZN2v88internal13DependentCode19DependencyGroupNameENS1_15DependencyGroupE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13DependentCode19DependencyGroupNameENS1_15DependencyGroupE
	.type	_ZN2v88internal13DependentCode19DependencyGroupNameENS1_15DependencyGroupE, @function
_ZN2v88internal13DependentCode19DependencyGroupNameENS1_15DependencyGroupE:
.LFB19739:
	.cfi_startproc
	endbr64
	cmpl	$6, %edi
	ja	.L993
	leaq	.L995(%rip), %rdx
	movl	%edi, %edi
	movslq	(%rdx,%rdi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal13DependentCode19DependencyGroupNameENS1_15DependencyGroupE,"a",@progbits
	.align 4
	.align 4
.L995:
	.long	.L1001-.L995
	.long	.L1000-.L995
	.long	.L1002-.L995
	.long	.L998-.L995
	.long	.L997-.L995
	.long	.L996-.L995
	.long	.L994-.L995
	.section	.text._ZN2v88internal13DependentCode19DependencyGroupNameENS1_15DependencyGroupE
	.p2align 4,,10
	.p2align 3
.L1002:
	leaq	.LC100(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1000:
	leaq	.LC104(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L998:
	leaq	.LC101(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L997:
	leaq	.LC102(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1001:
	leaq	.LC99(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L996:
	leaq	.LC103(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L994:
	leaq	.LC98(%rip), %rax
	ret
.L993:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC14(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19739:
	.size	_ZN2v88internal13DependentCode19DependencyGroupNameENS1_15DependencyGroupE, .-_ZN2v88internal13DependentCode19DependencyGroupNameENS1_15DependencyGroupE
	.section	.text.startup._GLOBAL__sub_I__ZNK2v88internal4Code20safepoint_table_sizeEv,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZNK2v88internal4Code20safepoint_table_sizeEv, @function
_GLOBAL__sub_I__ZNK2v88internal4Code20safepoint_table_sizeEv:
.LFB23932:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE23932:
	.size	_GLOBAL__sub_I__ZNK2v88internal4Code20safepoint_table_sizeEv, .-_GLOBAL__sub_I__ZNK2v88internal4Code20safepoint_table_sizeEv
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZNK2v88internal4Code20safepoint_table_sizeEv
	.hidden	_ZTCN2v88internal8OFStreamE0_So
	.weak	_ZTCN2v88internal8OFStreamE0_So
	.section	.rodata._ZTCN2v88internal8OFStreamE0_So,"aG",@progbits,_ZTVN2v88internal8OFStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal8OFStreamE0_So, @object
	.size	_ZTCN2v88internal8OFStreamE0_So, 80
_ZTCN2v88internal8OFStreamE0_So:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.weak	_ZTTN2v88internal8OFStreamE
	.section	.data.rel.ro.local._ZTTN2v88internal8OFStreamE,"awG",@progbits,_ZTVN2v88internal8OFStreamE,comdat
	.align 8
	.type	_ZTTN2v88internal8OFStreamE, @object
	.size	_ZTTN2v88internal8OFStreamE, 32
_ZTTN2v88internal8OFStreamE:
	.quad	_ZTVN2v88internal8OFStreamE+24
	.quad	_ZTCN2v88internal8OFStreamE0_So+24
	.quad	_ZTCN2v88internal8OFStreamE0_So+64
	.quad	_ZTVN2v88internal8OFStreamE+64
	.weak	_ZTVN2v88internal8OFStreamE
	.section	.data.rel.ro.local._ZTVN2v88internal8OFStreamE,"awG",@progbits,_ZTVN2v88internal8OFStreamE,comdat
	.align 8
	.type	_ZTVN2v88internal8OFStreamE, @object
	.size	_ZTVN2v88internal8OFStreamE, 80
_ZTVN2v88internal8OFStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	_ZN2v88internal8OFStreamD1Ev
	.quad	_ZN2v88internal8OFStreamD0Ev
	.quad	-80
	.quad	-80
	.quad	0
	.quad	_ZTv0_n24_N2v88internal8OFStreamD1Ev
	.quad	_ZTv0_n24_N2v88internal8OFStreamD0Ev
	.weak	_ZZN2v88internal12RegisterNameENS0_11XMMRegisterEE5Names
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC105:
	.string	"xmm0"
.LC106:
	.string	"xmm1"
.LC107:
	.string	"xmm2"
.LC108:
	.string	"xmm3"
.LC109:
	.string	"xmm4"
.LC110:
	.string	"xmm5"
.LC111:
	.string	"xmm6"
.LC112:
	.string	"xmm7"
.LC113:
	.string	"xmm8"
.LC114:
	.string	"xmm9"
.LC115:
	.string	"xmm10"
.LC116:
	.string	"xmm11"
.LC117:
	.string	"xmm12"
.LC118:
	.string	"xmm13"
.LC119:
	.string	"xmm14"
.LC120:
	.string	"xmm15"
	.section	.data.rel.ro.local._ZZN2v88internal12RegisterNameENS0_11XMMRegisterEE5Names,"awG",@progbits,_ZZN2v88internal12RegisterNameENS0_11XMMRegisterEE5Names,comdat
	.align 32
	.type	_ZZN2v88internal12RegisterNameENS0_11XMMRegisterEE5Names, @gnu_unique_object
	.size	_ZZN2v88internal12RegisterNameENS0_11XMMRegisterEE5Names, 128
_ZZN2v88internal12RegisterNameENS0_11XMMRegisterEE5Names:
	.quad	.LC105
	.quad	.LC106
	.quad	.LC107
	.quad	.LC108
	.quad	.LC109
	.quad	.LC110
	.quad	.LC111
	.quad	.LC112
	.quad	.LC113
	.quad	.LC114
	.quad	.LC115
	.quad	.LC116
	.quad	.LC117
	.quad	.LC118
	.quad	.LC119
	.quad	.LC120
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
