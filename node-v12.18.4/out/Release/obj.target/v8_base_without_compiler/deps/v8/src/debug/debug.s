	.file	"debug.cc"
	.text
	.section	.text._ZN2v85debug13DebugDelegate14ScriptCompiledENS_5LocalINS0_6ScriptEEEbb,"axG",@progbits,_ZN2v85debug13DebugDelegate14ScriptCompiledENS_5LocalINS0_6ScriptEEEbb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v85debug13DebugDelegate14ScriptCompiledENS_5LocalINS0_6ScriptEEEbb
	.type	_ZN2v85debug13DebugDelegate14ScriptCompiledENS_5LocalINS0_6ScriptEEEbb, @function
_ZN2v85debug13DebugDelegate14ScriptCompiledENS_5LocalINS0_6ScriptEEEbb:
.LFB7087:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7087:
	.size	_ZN2v85debug13DebugDelegate14ScriptCompiledENS_5LocalINS0_6ScriptEEEbb, .-_ZN2v85debug13DebugDelegate14ScriptCompiledENS_5LocalINS0_6ScriptEEEbb
	.section	.text._ZN2v85debug13DebugDelegate21BreakProgramRequestedENS_5LocalINS_7ContextEEERKSt6vectorIiSaIiEE,"axG",@progbits,_ZN2v85debug13DebugDelegate21BreakProgramRequestedENS_5LocalINS_7ContextEEERKSt6vectorIiSaIiEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v85debug13DebugDelegate21BreakProgramRequestedENS_5LocalINS_7ContextEEERKSt6vectorIiSaIiEE
	.type	_ZN2v85debug13DebugDelegate21BreakProgramRequestedENS_5LocalINS_7ContextEEERKSt6vectorIiSaIiEE, @function
_ZN2v85debug13DebugDelegate21BreakProgramRequestedENS_5LocalINS_7ContextEEERKSt6vectorIiSaIiEE:
.LFB7088:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7088:
	.size	_ZN2v85debug13DebugDelegate21BreakProgramRequestedENS_5LocalINS_7ContextEEERKSt6vectorIiSaIiEE, .-_ZN2v85debug13DebugDelegate21BreakProgramRequestedENS_5LocalINS_7ContextEEERKSt6vectorIiSaIiEE
	.section	.text._ZN2v85debug13DebugDelegate15ExceptionThrownENS_5LocalINS_7ContextEEENS2_INS_5ValueEEES6_bNS0_13ExceptionTypeE,"axG",@progbits,_ZN2v85debug13DebugDelegate15ExceptionThrownENS_5LocalINS_7ContextEEENS2_INS_5ValueEEES6_bNS0_13ExceptionTypeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v85debug13DebugDelegate15ExceptionThrownENS_5LocalINS_7ContextEEENS2_INS_5ValueEEES6_bNS0_13ExceptionTypeE
	.type	_ZN2v85debug13DebugDelegate15ExceptionThrownENS_5LocalINS_7ContextEEENS2_INS_5ValueEEES6_bNS0_13ExceptionTypeE, @function
_ZN2v85debug13DebugDelegate15ExceptionThrownENS_5LocalINS_7ContextEEENS2_INS_5ValueEEES6_bNS0_13ExceptionTypeE:
.LFB7089:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7089:
	.size	_ZN2v85debug13DebugDelegate15ExceptionThrownENS_5LocalINS_7ContextEEENS2_INS_5ValueEEES6_bNS0_13ExceptionTypeE, .-_ZN2v85debug13DebugDelegate15ExceptionThrownENS_5LocalINS_7ContextEEENS2_INS_5ValueEEES6_bNS0_13ExceptionTypeE
	.section	.text._ZN2v85debug13DebugDelegate20IsFunctionBlackboxedENS_5LocalINS0_6ScriptEEERKNS0_8LocationES7_,"axG",@progbits,_ZN2v85debug13DebugDelegate20IsFunctionBlackboxedENS_5LocalINS0_6ScriptEEERKNS0_8LocationES7_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v85debug13DebugDelegate20IsFunctionBlackboxedENS_5LocalINS0_6ScriptEEERKNS0_8LocationES7_
	.type	_ZN2v85debug13DebugDelegate20IsFunctionBlackboxedENS_5LocalINS0_6ScriptEEERKNS0_8LocationES7_, @function
_ZN2v85debug13DebugDelegate20IsFunctionBlackboxedENS_5LocalINS0_6ScriptEEERKNS0_8LocationES7_:
.LFB7090:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE7090:
	.size	_ZN2v85debug13DebugDelegate20IsFunctionBlackboxedENS_5LocalINS0_6ScriptEEERKNS0_8LocationES7_, .-_ZN2v85debug13DebugDelegate20IsFunctionBlackboxedENS_5LocalINS0_6ScriptEEERKNS0_8LocationES7_
	.section	.text._ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE,"axG",@progbits,_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE
	.type	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE, @function
_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE:
.LFB8321:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	leaq	8(%rcx), %r8
	movq	16(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE8321:
	.size	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE, .-_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE
	.section	.text._ZN2v88internal27HeapObjectAllocationTracker21UpdateObjectSizeEventEmi,"axG",@progbits,_ZN2v88internal27HeapObjectAllocationTracker21UpdateObjectSizeEventEmi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal27HeapObjectAllocationTracker21UpdateObjectSizeEventEmi
	.type	_ZN2v88internal27HeapObjectAllocationTracker21UpdateObjectSizeEventEmi, @function
_ZN2v88internal27HeapObjectAllocationTracker21UpdateObjectSizeEventEmi:
.LFB8513:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8513:
	.size	_ZN2v88internal27HeapObjectAllocationTracker21UpdateObjectSizeEventEmi, .-_ZN2v88internal27HeapObjectAllocationTracker21UpdateObjectSizeEventEmi
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal5Debug19ClearAllBreakPointsEvEUlNS2_6HandleINS2_9DebugInfoEEEE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal5Debug19ClearAllBreakPointsEvEUlNS2_6HandleINS2_9DebugInfoEEEE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal5Debug19ClearAllBreakPointsEvEUlNS2_6HandleINS2_9DebugInfoEEEE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation:
.LFB27444:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L9
	cmpl	$3, %edx
	je	.L10
	cmpl	$1, %edx
	je	.L14
.L10:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE27444:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal5Debug19ClearAllBreakPointsEvEUlNS2_6HandleINS2_9DebugInfoEEEE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal5Debug19ClearAllBreakPointsEvEUlNS2_6HandleINS2_9DebugInfoEEEE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal5Debug22RemoveAllCoverageInfosEvEUlNS2_6HandleINS2_9DebugInfoEEEE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal5Debug22RemoveAllCoverageInfosEvEUlNS2_6HandleINS2_9DebugInfoEEEE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal5Debug22RemoveAllCoverageInfosEvEUlNS2_6HandleINS2_9DebugInfoEEEE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation:
.LFB27602:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L16
	cmpl	$3, %edx
	je	.L17
	cmpl	$1, %edx
	je	.L21
.L17:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE27602:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal5Debug22RemoveAllCoverageInfosEvEUlNS2_6HandleINS2_9DebugInfoEEEE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal5Debug22RemoveAllCoverageInfosEvEUlNS2_6HandleINS2_9DebugInfoEEEE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal5Debug21ClearAllDebuggerHintsEvEUlNS2_6HandleINS2_9DebugInfoEEEE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal5Debug21ClearAllDebuggerHintsEvEUlNS2_6HandleINS2_9DebugInfoEEEE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal5Debug21ClearAllDebuggerHintsEvEUlNS2_6HandleINS2_9DebugInfoEEEE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation:
.LFB27606:
	.cfi_startproc
	endbr64
	cmpl	$1, %edx
	je	.L24
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE27606:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal5Debug21ClearAllDebuggerHintsEvEUlNS2_6HandleINS2_9DebugInfoEEEE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal5Debug21ClearAllDebuggerHintsEvEUlNS2_6HandleINS2_9DebugInfoEEEE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation
	.section	.text._ZN2v88internal5Debug23TemporaryObjectsTrackerD0Ev,"axG",@progbits,_ZN2v88internal5Debug23TemporaryObjectsTrackerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal5Debug23TemporaryObjectsTrackerD0Ev
	.type	_ZN2v88internal5Debug23TemporaryObjectsTrackerD0Ev, @function
_ZN2v88internal5Debug23TemporaryObjectsTrackerD0Ev:
.LFB27397:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal5Debug23TemporaryObjectsTrackerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	addq	$64, %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rax, -64(%rdi)
	call	_ZN2v84base5MutexD1Ev@PLT
	movq	24(%r12), %rbx
	testq	%rbx, %rbx
	je	.L26
	.p2align 4,,10
	.p2align 3
.L27:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L27
.L26:
	movq	16(%r12), %rax
	movq	8(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	8(%r12), %rdi
	leaq	56(%r12), %rax
	movq	$0, 32(%r12)
	movq	$0, 24(%r12)
	cmpq	%rax, %rdi
	je	.L28
	call	_ZdlPv@PLT
.L28:
	popq	%rbx
	movq	%r12, %rdi
	movl	$104, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE27397:
	.size	_ZN2v88internal5Debug23TemporaryObjectsTrackerD0Ev, .-_ZN2v88internal5Debug23TemporaryObjectsTrackerD0Ev
	.section	.text._ZNSt17_Function_handlerIFvN2v88internal6HandleINS1_9DebugInfoEEEEZNS1_5Debug21ClearAllDebuggerHintsEvEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvN2v88internal6HandleINS1_9DebugInfoEEEEZNS1_5Debug21ClearAllDebuggerHintsEvEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_, @function
_ZNSt17_Function_handlerIFvN2v88internal6HandleINS1_9DebugInfoEEEEZNS1_5Debug21ClearAllDebuggerHintsEvEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_:
.LFB27605:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	movq	(%rax), %rax
	movq	$0, 15(%rax)
	ret
	.cfi_endproc
.LFE27605:
	.size	_ZNSt17_Function_handlerIFvN2v88internal6HandleINS1_9DebugInfoEEEEZNS1_5Debug21ClearAllDebuggerHintsEvEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_, .-_ZNSt17_Function_handlerIFvN2v88internal6HandleINS1_9DebugInfoEEEEZNS1_5Debug21ClearAllDebuggerHintsEvEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_
	.section	.text._ZN2v88internal15InterruptsScopeD2Ev,"axG",@progbits,_ZN2v88internal15InterruptsScopeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15InterruptsScopeD2Ev
	.type	_ZN2v88internal15InterruptsScopeD2Ev, @function
_ZN2v88internal15InterruptsScopeD2Ev:
.LFB7166:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal15InterruptsScopeE(%rip), %rax
	cmpl	$2, 32(%rdi)
	movq	%rax, (%rdi)
	jne	.L37
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	movq	8(%rdi), %rdi
	jmp	_ZN2v88internal10StackGuard18PopInterruptsScopeEv@PLT
	.cfi_endproc
.LFE7166:
	.size	_ZN2v88internal15InterruptsScopeD2Ev, .-_ZN2v88internal15InterruptsScopeD2Ev
	.weak	_ZN2v88internal15InterruptsScopeD1Ev
	.set	_ZN2v88internal15InterruptsScopeD1Ev,_ZN2v88internal15InterruptsScopeD2Ev
	.section	.text._ZN2v88internal15InterruptsScopeD0Ev,"axG",@progbits,_ZN2v88internal15InterruptsScopeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15InterruptsScopeD0Ev
	.type	_ZN2v88internal15InterruptsScopeD0Ev, @function
_ZN2v88internal15InterruptsScopeD0Ev:
.LFB7168:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal15InterruptsScopeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	cmpl	$2, 32(%rdi)
	movq	%rax, (%rdi)
	je	.L39
	movq	8(%rdi), %rdi
	call	_ZN2v88internal10StackGuard18PopInterruptsScopeEv@PLT
.L39:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$48, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE7168:
	.size	_ZN2v88internal15InterruptsScopeD0Ev, .-_ZN2v88internal15InterruptsScopeD0Ev
	.section	.text._ZN2v88internal23PostponeInterruptsScopeD2Ev,"axG",@progbits,_ZN2v88internal23PostponeInterruptsScopeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23PostponeInterruptsScopeD2Ev
	.type	_ZN2v88internal23PostponeInterruptsScopeD2Ev, @function
_ZN2v88internal23PostponeInterruptsScopeD2Ev:
.LFB30161:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal15InterruptsScopeE(%rip), %rax
	cmpl	$2, 32(%rdi)
	movq	%rax, (%rdi)
	jne	.L43
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	movq	8(%rdi), %rdi
	jmp	_ZN2v88internal10StackGuard18PopInterruptsScopeEv@PLT
	.cfi_endproc
.LFE30161:
	.size	_ZN2v88internal23PostponeInterruptsScopeD2Ev, .-_ZN2v88internal23PostponeInterruptsScopeD2Ev
	.weak	_ZN2v88internal23PostponeInterruptsScopeD1Ev
	.set	_ZN2v88internal23PostponeInterruptsScopeD1Ev,_ZN2v88internal23PostponeInterruptsScopeD2Ev
	.section	.text._ZN2v88internal23PostponeInterruptsScopeD0Ev,"axG",@progbits,_ZN2v88internal23PostponeInterruptsScopeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23PostponeInterruptsScopeD0Ev
	.type	_ZN2v88internal23PostponeInterruptsScopeD0Ev, @function
_ZN2v88internal23PostponeInterruptsScopeD0Ev:
.LFB30163:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal15InterruptsScopeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	cmpl	$2, 32(%rdi)
	movq	%rax, (%rdi)
	je	.L45
	movq	8(%rdi), %rdi
	call	_ZN2v88internal10StackGuard18PopInterruptsScopeEv@PLT
.L45:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$48, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE30163:
	.size	_ZN2v88internal23PostponeInterruptsScopeD0Ev, .-_ZN2v88internal23PostponeInterruptsScopeD0Ev
	.section	.text._ZNSt17_Function_handlerIFvN2v88internal6HandleINS1_9DebugInfoEEEEZNS1_5Debug22RemoveAllCoverageInfosEvEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvN2v88internal6HandleINS1_9DebugInfoEEEEZNS1_5Debug22RemoveAllCoverageInfosEvEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_, @function
_ZNSt17_Function_handlerIFvN2v88internal6HandleINS1_9DebugInfoEEEEZNS1_5Debug22RemoveAllCoverageInfosEvEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_:
.LFB27601:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	(%rax), %rax
	movq	%rax, -16(%rbp)
	movq	(%rdi), %rax
	leaq	-16(%rbp), %rdi
	movq	136(%rax), %rsi
	call	_ZN2v88internal9DebugInfo17ClearCoverageInfoEPNS0_7IsolateE@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L50
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L50:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27601:
	.size	_ZNSt17_Function_handlerIFvN2v88internal6HandleINS1_9DebugInfoEEEEZNS1_5Debug22RemoveAllCoverageInfosEvEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_, .-_ZNSt17_Function_handlerIFvN2v88internal6HandleINS1_9DebugInfoEEEEZNS1_5Debug22RemoveAllCoverageInfosEvEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_
	.section	.text._ZN2v88internal5Debug23TemporaryObjectsTrackerD2Ev,"axG",@progbits,_ZN2v88internal5Debug23TemporaryObjectsTrackerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal5Debug23TemporaryObjectsTrackerD2Ev
	.type	_ZN2v88internal5Debug23TemporaryObjectsTrackerD2Ev, @function
_ZN2v88internal5Debug23TemporaryObjectsTrackerD2Ev:
.LFB27395:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal5Debug23TemporaryObjectsTrackerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	addq	$64, %rdi
	movq	%rax, -64(%rdi)
	call	_ZN2v84base5MutexD1Ev@PLT
	movq	24(%rbx), %r12
	testq	%r12, %r12
	je	.L52
	.p2align 4,,10
	.p2align 3
.L53:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L53
.L52:
	movq	16(%rbx), %rax
	movq	8(%rbx), %rdi
	xorl	%esi, %esi
	addq	$56, %rbx
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-48(%rbx), %rdi
	movq	$0, -24(%rbx)
	movq	$0, -32(%rbx)
	cmpq	%rbx, %rdi
	je	.L51
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L51:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE27395:
	.size	_ZN2v88internal5Debug23TemporaryObjectsTrackerD2Ev, .-_ZN2v88internal5Debug23TemporaryObjectsTrackerD2Ev
	.weak	_ZN2v88internal5Debug23TemporaryObjectsTrackerD1Ev
	.set	_ZN2v88internal5Debug23TemporaryObjectsTrackerD1Ev,_ZN2v88internal5Debug23TemporaryObjectsTrackerD2Ev
	.section	.text._ZN2v88internal13BreakIterator13SetDebugBreakEv.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal13BreakIterator13SetDebugBreakEv.part.0, @function
_ZN2v88internal13BreakIterator13SetDebugBreakEv.part.0:
.LFB30437:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	(%rax), %rax
	andq	$-262144, %rax
	movq	24(%rax), %r12
	subq	$37592, %r12
	movq	41088(%r12), %r15
	movq	41096(%r12), %r14
	addl	$1, 41104(%r12)
	movq	(%rdi), %rax
	movq	(%rax), %rax
	movq	%rax, %rdx
	movq	39(%rax), %r8
	andq	$-262144, %rdx
	movq	24(%rdx), %r13
	movq	3520(%r13), %rdi
	subq	$37592, %r13
	testq	%rdi, %rdi
	je	.L61
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L62:
	movl	56(%rbx), %edx
	leaq	-80(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter21BytecodeArrayAccessorC1ENS0_6HandleINS0_13BytecodeArrayEEEi@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter21BytecodeArrayAccessor15ApplyDebugBreakEv@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L64
	movq	(%rdi), %rax
	call	*72(%rax)
.L64:
	subl	$1, 41104(%r12)
	movq	%r15, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L60
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L60:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L72
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	.cfi_restore_state
	movq	41088(%r13), %rsi
	cmpq	41096(%r13), %rsi
	je	.L73
.L63:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r13)
	movq	%r8, (%rsi)
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L73:
	movq	%r13, %rdi
	movq	%r8, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L63
.L72:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE30437:
	.size	_ZN2v88internal13BreakIterator13SetDebugBreakEv.part.0, .-_ZN2v88internal13BreakIterator13SetDebugBreakEv.part.0
	.section	.text._ZN2v88internal5DebugC2EPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5DebugC2EPNS0_7IsolateE
	.type	_ZN2v88internal5DebugC2EPNS0_7IsolateE, @function
_ZN2v88internal5DebugC2EPNS0_7IsolateE:
.LFB22815:
	.cfi_startproc
	endbr64
	movabsq	$1099511627776, %rax
	pxor	%xmm0, %xmm0
	movb	$0, 16(%rdi)
	movq	%rax, 8(%rdi)
	movq	%rsi, 136(%rdi)
	movq	$0, (%rdi)
	movq	$0, 40(%rdi)
	movq	%rsi, 48(%rdi)
	movl	$0, 56(%rdi)
	movq	$0, 80(%rdi)
	movl	$0, 72(%rdi)
	movb	$-1, 76(%rdi)
	movb	$0, 88(%rdi)
	movl	$-1, 92(%rdi)
	movq	$-1, 96(%rdi)
	movq	$0, 120(%rdi)
	movl	$0, 128(%rdi)
	movups	%xmm0, 24(%rdi)
	movups	%xmm0, 104(%rdi)
	movq	$0, 64(%rdi)
	movb	$0, 132(%rdi)
	movq	136(%rdi), %rax
	cmpl	$32, 41828(%rax)
	sete	%al
	movb	%al, 9(%rdi)
	ret
	.cfi_endproc
.LFE22815:
	.size	_ZN2v88internal5DebugC2EPNS0_7IsolateE, .-_ZN2v88internal5DebugC2EPNS0_7IsolateE
	.globl	_ZN2v88internal5DebugC1EPNS0_7IsolateE
	.set	_ZN2v88internal5DebugC1EPNS0_7IsolateE,_ZN2v88internal5DebugC2EPNS0_7IsolateE
	.section	.text._ZN2v88internal5DebugD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5DebugD2Ev
	.type	_ZN2v88internal5DebugD2Ev, @function
_ZN2v88internal5DebugD2Ev:
.LFB22818:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	32(%rdi), %r12
	testq	%r12, %r12
	je	.L77
	movq	(%r12), %rax
	leaq	_ZN2v88internal5Debug23TemporaryObjectsTrackerD0Ev(%rip), %rdx
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L79
	leaq	16+_ZTVN2v88internal5Debug23TemporaryObjectsTrackerE(%rip), %rax
	leaq	64(%r12), %rdi
	movq	%rax, (%r12)
	call	_ZN2v84base5MutexD1Ev@PLT
	movq	24(%r12), %rbx
	testq	%rbx, %rbx
	je	.L80
	.p2align 4,,10
	.p2align 3
.L81:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L81
.L80:
	movq	16(%r12), %rax
	movq	8(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	8(%r12), %rdi
	leaq	56(%r12), %rax
	movq	$0, 32(%r12)
	movq	$0, 24(%r12)
	cmpq	%rax, %rdi
	je	.L82
	call	_ZdlPv@PLT
.L82:
	popq	%rbx
	movq	%r12, %rdi
	movl	$104, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L77:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore_state
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE22818:
	.size	_ZN2v88internal5DebugD2Ev, .-_ZN2v88internal5DebugD2Ev
	.globl	_ZN2v88internal5DebugD1Ev
	.set	_ZN2v88internal5DebugD1Ev,_ZN2v88internal5DebugD2Ev
	.section	.text._ZNK2v88internal13BreakLocation35GetGeneratorObjectForSuspendedFrameEPNS0_15JavaScriptFrameE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal13BreakLocation35GetGeneratorObjectForSuspendedFrameEPNS0_15JavaScriptFrameE
	.type	_ZNK2v88internal13BreakLocation35GetGeneratorObjectForSuspendedFrameEPNS0_15JavaScriptFrameE, @function
_ZNK2v88internal13BreakLocation35GetGeneratorObjectForSuspendedFrameEPNS0_15JavaScriptFrameE:
.LFB22822:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movl	20(%r8), %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNK2v88internal16InterpretedFrame23ReadInterpreterRegisterEi@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22822:
	.size	_ZNK2v88internal13BreakLocation35GetGeneratorObjectForSuspendedFrameEPNS0_15JavaScriptFrameE, .-_ZNK2v88internal13BreakLocation35GetGeneratorObjectForSuspendedFrameEPNS0_15JavaScriptFrameE
	.section	.text._ZNK2v88internal13BreakLocation4typeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal13BreakLocation4typeEv
	.type	_ZNK2v88internal13BreakLocation4typeEv, @function
_ZNK2v88internal13BreakLocation4typeEv:
.LFB22825:
	.cfi_startproc
	endbr64
	movl	12(%rdi), %eax
	movl	$3, %r8d
	subl	$1, %eax
	cmpl	$3, %eax
	ja	.L90
	leaq	CSWTCH.1024(%rip), %rdx
	movl	(%rdx,%rax,4), %r8d
.L90:
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE22825:
	.size	_ZNK2v88internal13BreakLocation4typeEv, .-_ZNK2v88internal13BreakLocation4typeEv
	.section	.text._ZN2v88internal13BreakIterator22BreakIndexFromPositionEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13BreakIterator22BreakIndexFromPositionEi
	.type	_ZN2v88internal13BreakIterator22BreakIndexFromPositionEi, @function
_ZN2v88internal13BreakIterator22BreakIndexFromPositionEi:
.LFB22829:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpl	$-1, 48(%rdi)
	movl	8(%rdi), %r15d
	je	.L93
	movl	12(%rdi), %eax
	movq	%rdi, %rbx
	movl	%esi, %r12d
	movl	%r15d, %edx
	movl	$2147483647, %r14d
	leaq	24(%rdi), %r13
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L119:
	movl	$108543, %ecx
	btq	%rdx, %rcx
	jnc	.L101
.L118:
	movl	8(%rbx), %edx
	movl	48(%rbx), %ecx
	addl	$1, %edx
	movl	%edx, 8(%rbx)
	cmpl	$-1, %ecx
	je	.L93
.L103:
	cmpl	%eax, %r12d
	jg	.L95
	subl	%r12d, %eax
	cmpl	%r14d, %eax
	jge	.L95
	movl	%edx, %r15d
	testl	%eax, %eax
	je	.L93
	movl	%eax, %r14d
.L95:
	cmpl	$-1, %edx
	jne	.L102
	.p2align 4,,10
	.p2align 3
.L104:
	movq	64(%rbx), %rax
	movzbl	72(%rbx), %esi
	shrq	%rax
	andl	$1073741823, %eax
	subl	$1, %eax
	movl	%eax, 12(%rbx)
	testb	%sil, %sil
	je	.L96
	movl	%eax, 16(%rbx)
.L96:
	movq	(%rbx), %rdx
	movl	56(%rbx), %ecx
	movq	(%rdx), %rdx
	movq	31(%rdx), %rdi
	leal	54(%rcx), %edx
	movslq	%edx, %rdx
	movzbl	-1(%rdi,%rdx), %edx
	cmpb	$3, %dl
	ja	.L97
	addl	$55, %ecx
	movslq	%ecx, %rcx
	movzbl	-1(%rdi,%rcx), %edx
.L97:
	leal	85(%rdx), %ecx
	andl	$247, %ecx
	je	.L118
	cmpb	$-80, %dl
	je	.L118
	subl	$86, %edx
	cmpb	$16, %dl
	jbe	.L119
.L101:
	testb	%sil, %sil
	jne	.L118
.L102:
	movq	%r13, %rdi
	call	_ZN2v88internal27SourcePositionTableIterator7AdvanceEv@PLT
	cmpl	$-1, 48(%rbx)
	jne	.L104
.L93:
	addq	$8, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22829:
	.size	_ZN2v88internal13BreakIterator22BreakIndexFromPositionEi, .-_ZN2v88internal13BreakIterator22BreakIndexFromPositionEi
	.section	.text._ZN2v88internal13BreakIterator4NextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13BreakIterator4NextEv
	.type	_ZN2v88internal13BreakIterator4NextEv, @function
_ZN2v88internal13BreakIterator4NextEv:
.LFB22830:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpl	$-1, 48(%rdi)
	movl	8(%rdi), %eax
	je	.L121
	cmpl	$-1, %eax
	jne	.L130
	.p2align 4,,10
	.p2align 3
.L122:
	movq	64(%rbx), %rax
	movzbl	72(%rbx), %esi
	shrq	%rax
	andl	$1073741823, %eax
	subl	$1, %eax
	movl	%eax, 12(%rbx)
	testb	%sil, %sil
	je	.L124
	movl	%eax, 16(%rbx)
.L124:
	movq	(%rbx), %rax
	movl	56(%rbx), %edx
	movq	(%rax), %rax
	movq	31(%rax), %rcx
	leal	54(%rdx), %eax
	cltq
	movzbl	-1(%rcx,%rax), %eax
	cmpb	$3, %al
	ja	.L125
	addl	$55, %edx
	movslq	%edx, %rdx
	movzbl	-1(%rcx,%rdx), %eax
.L125:
	leal	85(%rax), %edx
	andl	$247, %edx
	je	.L129
	cmpb	$-80, %al
	je	.L129
	subl	$86, %eax
	cmpb	$16, %al
	ja	.L128
	movl	$108543, %edx
	btq	%rax, %rdx
	jnc	.L128
.L129:
	movl	8(%rbx), %eax
.L121:
	addl	$1, %eax
	movl	%eax, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L128:
	.cfi_restore_state
	testb	%sil, %sil
	jne	.L129
.L130:
	leaq	24(%rbx), %rdi
	call	_ZN2v88internal27SourcePositionTableIterator7AdvanceEv@PLT
	cmpl	$-1, 48(%rbx)
	jne	.L122
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22830:
	.size	_ZN2v88internal13BreakIterator4NextEv, .-_ZN2v88internal13BreakIterator4NextEv
	.section	.text._ZN2v88internal13BreakIteratorC2ENS0_6HandleINS0_9DebugInfoEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13BreakIteratorC2ENS0_6HandleINS0_9DebugInfoEEE
	.type	_ZN2v88internal13BreakIteratorC2ENS0_6HandleINS0_9DebugInfoEEE, @function
_ZN2v88internal13BreakIteratorC2ENS0_6HandleINS0_9DebugInfoEEE:
.LFB22827:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	addq	$24, %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	%rsi, -24(%rdi)
	movl	$-1, -16(%rdi)
	movq	39(%rax), %rax
	movq	31(%rax), %rsi
	testb	$1, %sil
	jne	.L148
.L143:
	andq	$-262144, %rax
	movq	24(%rax), %rax
	cmpq	-37280(%rax), %rsi
	je	.L149
	movq	7(%rsi), %rsi
.L144:
	xorl	%edx, %edx
	call	_ZN2v88internal27SourcePositionTableIteratorC1ENS0_9ByteArrayENS1_15IterationFilterE@PLT
	movq	(%rbx), %rax
	leaq	-32(%rbp), %rdi
	movq	7(%rax), %rax
	movq	%rax, -32(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	movq	%r12, %rdi
	movl	%eax, 12(%r12)
	movl	%eax, 16(%r12)
	call	_ZN2v88internal13BreakIterator4NextEv
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L150
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L148:
	.cfi_restore_state
	movq	-1(%rsi), %rdx
	cmpw	$70, 11(%rdx)
	jne	.L143
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L149:
	movq	-36616(%rax), %rsi
	jmp	.L144
.L150:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22827:
	.size	_ZN2v88internal13BreakIteratorC2ENS0_6HandleINS0_9DebugInfoEEE, .-_ZN2v88internal13BreakIteratorC2ENS0_6HandleINS0_9DebugInfoEEE
	.globl	_ZN2v88internal13BreakIteratorC1ENS0_6HandleINS0_9DebugInfoEEE
	.set	_ZN2v88internal13BreakIteratorC1ENS0_6HandleINS0_9DebugInfoEEE,_ZN2v88internal13BreakIteratorC2ENS0_6HandleINS0_9DebugInfoEEE
	.section	.text._ZN2v88internal13BreakLocation24BreakIndexFromCodeOffsetENS0_6HandleINS0_9DebugInfoEEENS2_INS0_12AbstractCodeEEEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13BreakLocation24BreakIndexFromCodeOffsetENS0_6HandleINS0_9DebugInfoEEENS2_INS0_12AbstractCodeEEEi
	.type	_ZN2v88internal13BreakLocation24BreakIndexFromCodeOffsetENS0_6HandleINS0_9DebugInfoEEENS2_INS0_12AbstractCodeEEEi, @function
_ZN2v88internal13BreakLocation24BreakIndexFromCodeOffsetENS0_6HandleINS0_9DebugInfoEEENS2_INS0_12AbstractCodeEEEi:
.LFB22823:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	leaq	-144(%rbp), %rdi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	%edx, %ebx
	subq	$112, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal13BreakIteratorC1ENS0_6HandleINS0_9DebugInfoEEE
	cmpl	$-1, -96(%rbp)
	je	.L164
	movl	-136(%rbp), %eax
	movl	-88(%rbp), %edx
	xorl	%r14d, %r14d
	leaq	-120(%rbp), %r12
	movl	$2147483647, %r13d
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L179:
	movl	$108543, %esi
	btq	%rax, %rsi
	jnc	.L159
.L178:
	movl	-136(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -136(%rbp)
.L161:
	cmpl	%edx, %ebx
	jl	.L153
	movl	%ebx, %ecx
	subl	%edx, %ecx
	cmpl	%r13d, %ecx
	jge	.L153
	testl	%ecx, %ecx
	je	.L165
	movl	%ecx, %r13d
	movl	%eax, %r14d
.L153:
	cmpl	$-1, %eax
	jne	.L160
	.p2align 4,,10
	.p2align 3
.L162:
	movq	-80(%rbp), %rax
	movzbl	-72(%rbp), %ecx
	shrq	%rax
	andl	$1073741823, %eax
	subl	$1, %eax
	movl	%eax, -132(%rbp)
	testb	%cl, %cl
	je	.L154
	movl	%eax, -128(%rbp)
.L154:
	movq	-144(%rbp), %rax
	movq	(%rax), %rax
	movq	31(%rax), %rsi
	leal	54(%rdx), %eax
	cltq
	movzbl	-1(%rsi,%rax), %eax
	cmpb	$3, %al
	ja	.L155
	leal	55(%rdx), %eax
	cltq
	movzbl	-1(%rsi,%rax), %eax
.L155:
	leal	85(%rax), %esi
	andl	$247, %esi
	je	.L178
	cmpb	$-80, %al
	je	.L178
	subl	$86, %eax
	cmpb	$16, %al
	jbe	.L179
.L159:
	testb	%cl, %cl
	jne	.L178
.L160:
	movq	%r12, %rdi
	call	_ZN2v88internal27SourcePositionTableIterator7AdvanceEv@PLT
	cmpl	$-1, -96(%rbp)
	je	.L151
	movl	-88(%rbp), %edx
	jmp	.L162
.L164:
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L151:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L180
	addq	$112, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L165:
	.cfi_restore_state
	movl	%eax, %r14d
	jmp	.L151
.L180:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22823:
	.size	_ZN2v88internal13BreakLocation24BreakIndexFromCodeOffsetENS0_6HandleINS0_9DebugInfoEEENS2_INS0_12AbstractCodeEEEi, .-_ZN2v88internal13BreakLocation24BreakIndexFromCodeOffsetENS0_6HandleINS0_9DebugInfoEEENS2_INS0_12AbstractCodeEEEi
	.section	.text._ZN2v88internal13BreakIterator17GetDebugBreakTypeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13BreakIterator17GetDebugBreakTypeEv
	.type	_ZN2v88internal13BreakIterator17GetDebugBreakTypeEv, @function
_ZN2v88internal13BreakIterator17GetDebugBreakTypeEv:
.LFB22831:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movl	56(%rdi), %edx
	movq	(%rax), %rax
	movq	31(%rax), %rcx
	leal	54(%rdx), %eax
	cltq
	movzbl	-1(%rcx,%rax), %eax
	cmpb	$3, %al
	ja	.L182
	addl	$55, %edx
	movslq	%edx, %rdx
	movzbl	-1(%rcx,%rdx), %eax
.L182:
	movl	$1, %r8d
	cmpb	$-77, %al
	je	.L181
	cmpb	$-85, %al
	je	.L186
	cmpb	$-80, %al
	je	.L187
	subl	$86, %eax
	cmpb	$16, %al
	ja	.L184
	movl	$108543, %edx
	movl	$3, %r8d
	btq	%rax, %rdx
	jnc	.L184
.L181:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L184:
	xorl	%r8d, %r8d
	cmpb	$0, 72(%rdi)
	setne	%r8b
	addl	%r8d, %r8d
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L186:
	movl	$4, %r8d
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L187:
	movl	$5, %r8d
	jmp	.L181
	.cfi_endproc
.LFE22831:
	.size	_ZN2v88internal13BreakIterator17GetDebugBreakTypeEv, .-_ZN2v88internal13BreakIterator17GetDebugBreakTypeEv
	.section	.text._ZN2v88internal13BreakIterator14SkipToPositionEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13BreakIterator14SkipToPositionEi
	.type	_ZN2v88internal13BreakIterator14SkipToPositionEi, @function
_ZN2v88internal13BreakIterator14SkipToPositionEi:
.LFB22832:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	leaq	-160(%rbp), %rdi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$120, %rsp
	movq	(%r15), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal13BreakIteratorC1ENS0_6HandleINS0_9DebugInfoEEE
	cmpl	$-1, -112(%rbp)
	movl	-152(%rbp), %r14d
	je	.L197
	movl	-148(%rbp), %eax
	movl	%r14d, %edx
	movl	$2147483647, %r12d
	leaq	-136(%rbp), %r13
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L237:
	movl	$108543, %ecx
	btq	%rdx, %rcx
	jnc	.L203
.L236:
	movl	-152(%rbp), %edx
	addl	$1, %edx
	movl	%edx, -152(%rbp)
.L210:
	cmpl	%eax, %ebx
	jg	.L196
	subl	%ebx, %eax
	cmpl	%r12d, %eax
	jge	.L196
	movl	%edx, %r14d
	testl	%eax, %eax
	je	.L197
	movl	%eax, %r12d
.L196:
	cmpl	$-1, %edx
	jne	.L204
	.p2align 4,,10
	.p2align 3
.L211:
	movq	-96(%rbp), %rax
	movzbl	-88(%rbp), %esi
	shrq	%rax
	andl	$1073741823, %eax
	subl	$1, %eax
	movl	%eax, -148(%rbp)
	testb	%sil, %sil
	je	.L198
	movl	%eax, -144(%rbp)
.L198:
	movq	-160(%rbp), %rdx
	movl	-104(%rbp), %ecx
	movq	(%rdx), %rdx
	movq	31(%rdx), %rdi
	leal	54(%rcx), %edx
	movslq	%edx, %rdx
	movzbl	-1(%rdi,%rdx), %edx
	cmpb	$3, %dl
	ja	.L199
	addl	$55, %ecx
	movslq	%ecx, %rcx
	movzbl	-1(%rdi,%rcx), %edx
.L199:
	leal	85(%rdx), %ecx
	andl	$247, %ecx
	je	.L236
	cmpb	$-80, %dl
	je	.L236
	subl	$86, %edx
	cmpb	$16, %dl
	jbe	.L237
.L203:
	testb	%sil, %sil
	jne	.L236
.L204:
	movq	%r13, %rdi
	call	_ZN2v88internal27SourcePositionTableIterator7AdvanceEv@PLT
	cmpl	$-1, -112(%rbp)
	jne	.L211
.L197:
	leal	-1(%r14), %ebx
	leaq	24(%r15), %r12
	testl	%r14d, %r14d
	jle	.L192
	.p2align 4,,10
	.p2align 3
.L194:
	cmpl	$-1, 48(%r15)
	movl	8(%r15), %eax
	je	.L205
	cmpl	$-1, %eax
	jne	.L209
	.p2align 4,,10
	.p2align 3
.L206:
	movq	64(%r15), %rax
	shrq	%rax
	andl	$1073741823, %eax
	subl	$1, %eax
	cmpb	$0, 72(%r15)
	movl	%eax, 12(%r15)
	je	.L208
	movl	%eax, 16(%r15)
.L208:
	movq	%r15, %rdi
	call	_ZN2v88internal13BreakIterator17GetDebugBreakTypeEv
	testl	%eax, %eax
	jne	.L238
.L209:
	movq	%r12, %rdi
	call	_ZN2v88internal27SourcePositionTableIterator7AdvanceEv@PLT
	cmpl	$-1, 48(%r15)
	jne	.L206
	subl	$1, %ebx
	cmpl	$-1, %ebx
	jne	.L194
.L192:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L239
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L238:
	.cfi_restore_state
	movl	8(%r15), %eax
.L205:
	addl	$1, %eax
	subl	$1, %ebx
	movl	%eax, 8(%r15)
	cmpl	$-1, %ebx
	jne	.L194
	jmp	.L192
.L239:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22832:
	.size	_ZN2v88internal13BreakIterator14SkipToPositionEi, .-_ZN2v88internal13BreakIterator14SkipToPositionEi
	.section	.text._ZNK2v88internal13BreakLocation13HasBreakPointEPNS0_7IsolateENS0_6HandleINS0_9DebugInfoEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal13BreakLocation13HasBreakPointEPNS0_7IsolateENS0_6HandleINS0_9DebugInfoEEE
	.type	_ZNK2v88internal13BreakLocation13HasBreakPointEPNS0_7IsolateENS0_6HandleINS0_9DebugInfoEEE, @function
_ZNK2v88internal13BreakLocation13HasBreakPointEPNS0_7IsolateENS0_6HandleINS0_9DebugInfoEEE:
.LFB22824:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-144(%rbp), %r13
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	movl	16(%rdi), %edx
	movq	%r13, %rdi
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal9DebugInfo13HasBreakPointEPNS0_7IsolateEi@PLT
	testb	%al, %al
	jne	.L248
.L240:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L249
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L248:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	%r13, %rdi
	movq	%rax, -144(%rbp)
	call	_ZNK2v88internal9DebugInfo15CanBreakAtEntryEv@PLT
	testb	%al, %al
	je	.L242
	movq	(%r12), %rax
	movq	%r13, %rdi
	movq	%rax, -144(%rbp)
	call	_ZNK2v88internal9DebugInfo12BreakAtEntryEv@PLT
	jmp	.L240
	.p2align 4,,10
	.p2align 3
.L242:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal13BreakIteratorC1ENS0_6HandleINS0_9DebugInfoEEE
	movl	16(%rbx), %esi
	movq	%r13, %rdi
	call	_ZN2v88internal13BreakIterator14SkipToPositionEi
	movl	-88(%rbp), %eax
	cmpl	%eax, 8(%rbx)
	sete	%al
	jmp	.L240
.L249:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22824:
	.size	_ZNK2v88internal13BreakLocation13HasBreakPointEPNS0_7IsolateENS0_6HandleINS0_9DebugInfoEEE, .-_ZNK2v88internal13BreakLocation13HasBreakPointEPNS0_7IsolateENS0_6HandleINS0_9DebugInfoEEE
	.section	.text._ZN2v88internal13BreakIterator13SetDebugBreakEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13BreakIterator13SetDebugBreakEv
	.type	_ZN2v88internal13BreakIterator13SetDebugBreakEv, @function
_ZN2v88internal13BreakIterator13SetDebugBreakEv:
.LFB22833:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	(%rax), %rax
	movq	31(%rax), %rcx
	movl	56(%rdi), %eax
	leal	54(%rax), %edx
	movslq	%edx, %rdx
	movzbl	-1(%rcx,%rdx), %edx
	cmpb	$3, %dl
	ja	.L251
	addl	$55, %eax
	cltq
	movzbl	-1(%rcx,%rax), %edx
.L251:
	cmpb	$-77, %dl
	je	.L250
	jmp	_ZN2v88internal13BreakIterator13SetDebugBreakEv.part.0
	.p2align 4,,10
	.p2align 3
.L250:
	ret
	.cfi_endproc
.LFE22833:
	.size	_ZN2v88internal13BreakIterator13SetDebugBreakEv, .-_ZN2v88internal13BreakIterator13SetDebugBreakEv
	.section	.text._ZN2v88internal13BreakIterator15ClearDebugBreakEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13BreakIterator15ClearDebugBreakEv
	.type	_ZN2v88internal13BreakIterator15ClearDebugBreakEv, @function
_ZN2v88internal13BreakIterator15ClearDebugBreakEv:
.LFB22837:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	(%rax), %rsi
	movl	56(%rdi), %eax
	movq	31(%rsi), %r8
	leal	54(%rax), %edx
	movslq	%edx, %rdx
	movzbl	-1(%r8,%rdx), %ecx
	movl	%ecx, %edi
	cmpb	$3, %cl
	ja	.L254
	addl	$55, %eax
	cltq
	movzbl	-1(%r8,%rax), %edi
.L254:
	cmpb	$-77, %dil
	je	.L253
	movq	39(%rsi), %rax
	movb	%cl, -1(%rdx,%rax)
.L253:
	ret
	.cfi_endproc
.LFE22837:
	.size	_ZN2v88internal13BreakIterator15ClearDebugBreakEv, .-_ZN2v88internal13BreakIterator15ClearDebugBreakEv
	.section	.text._ZN2v88internal13BreakIterator16GetBreakLocationEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13BreakIterator16GetBreakLocationEv
	.type	_ZN2v88internal13BreakIterator16GetBreakLocationEv, @function
_ZN2v88internal13BreakIterator16GetBreakLocationEv:
.LFB22838:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	(%rax), %rax
	movq	%rax, %rdx
	movq	39(%rax), %rsi
	andq	$-262144, %rdx
	movq	24(%rdx), %r13
	movq	3520(%r13), %rdi
	subq	$37592, %r13
	testq	%rdi, %rdi
	je	.L261
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L262:
	movq	(%rbx), %rax
	movl	56(%rbx), %r13d
	movq	(%rax), %rcx
	leal	54(%r13), %eax
	cltq
	movq	31(%rcx), %rsi
	movzbl	-1(%rsi,%rax), %edx
	cmpb	$3, %dl
	ja	.L264
	leal	55(%r13), %eax
	cltq
	movzbl	-1(%rsi,%rax), %edx
.L264:
	cmpb	$-77, %dl
	je	.L273
	cmpb	$-85, %dl
	je	.L274
	cmpb	$-80, %dl
	je	.L266
	subl	$86, %edx
	cmpb	$16, %dl
	jbe	.L282
.L267:
	cmpb	$1, 72(%rbx)
	movl	$-1, %r15d
	sbbl	%ecx, %ecx
	notl	%ecx
	andl	$2, %ecx
.L265:
	movl	12(%rbx), %eax
	movq	%r14, (%r12)
	movl	%r13d, 8(%r12)
	movl	%ecx, 12(%r12)
	movl	%eax, 16(%r12)
	movl	%r15d, 20(%r12)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L283
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L282:
	.cfi_restore_state
	movl	$108543, %eax
	movl	$3, %ecx
	movl	$-1, %r15d
	btq	%rdx, %rax
	jc	.L265
	jmp	.L267
	.p2align 4,,10
	.p2align 3
.L261:
	movq	41088(%r13), %r14
	cmpq	41096(%r13), %r14
	je	.L284
.L263:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r14)
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L273:
	movl	$1, %ecx
	movl	$-1, %r15d
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L266:
	andq	$-262144, %rcx
	movq	24(%rcx), %r15
	movq	3520(%r15), %rdi
	subq	$37592, %r15
	testq	%rdi, %rdi
	je	.L268
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r8
.L269:
	leaq	-80(%rbp), %r15
	movq	%r8, %rsi
	movl	%r13d, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter21BytecodeArrayAccessorC1ENS0_6HandleINS0_13BytecodeArrayEEEi@PLT
	movq	%r15, %rdi
	xorl	%esi, %esi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	-80(%rbp), %rdi
	movl	%eax, %r15d
	testq	%rdi, %rdi
	je	.L271
	movq	(%rdi), %rax
	call	*72(%rax)
.L271:
	movl	56(%rbx), %r13d
	movl	$5, %ecx
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L268:
	movq	41088(%r15), %r8
	cmpq	41096(%r15), %r8
	je	.L285
.L270:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r8)
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L274:
	movl	$4, %ecx
	movl	$-1, %r15d
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L284:
	movq	%r13, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L285:
	movq	%r15, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L270
.L283:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22838:
	.size	_ZN2v88internal13BreakIterator16GetBreakLocationEv, .-_ZN2v88internal13BreakIterator16GetBreakLocationEv
	.section	.text._ZN2v88internal13BreakLocation9FromFrameENS0_6HandleINS0_9DebugInfoEEEPNS0_15JavaScriptFrameE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13BreakLocation9FromFrameENS0_6HandleINS0_9DebugInfoEEEPNS0_15JavaScriptFrameE
	.type	_ZN2v88internal13BreakLocation9FromFrameENS0_6HandleINS0_9DebugInfoEEEPNS0_15JavaScriptFrameE, @function
_ZN2v88internal13BreakLocation9FromFrameENS0_6HandleINS0_9DebugInfoEEEPNS0_15JavaScriptFrameE:
.LFB22820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-160(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	%rax, -160(%rbp)
	call	_ZNK2v88internal9DebugInfo15CanBreakAtEntryEv@PLT
	testb	%al, %al
	je	.L287
	movabsq	$25769803776, %rax
	movq	$0, (%r12)
	movq	%rax, 8(%r12)
	movq	$0, 16(%r12)
.L286:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L303
	addq	$232, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L287:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%r13, %rdi
	leaq	-256(%rbp), %r15
	call	_ZN2v88internal12FrameSummary6GetTopEPKNS0_13StandardFrameE@PLT
	movq	%r13, %rdi
	movl	-120(%rbp), %ebx
	call	_ZN2v88internal12FrameSummaryD1Ev@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal13BreakIteratorC1ENS0_6HandleINS0_9DebugInfoEEE
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal13BreakIteratorC1ENS0_6HandleINS0_9DebugInfoEEE
	cmpl	$-1, -112(%rbp)
	je	.L293
	movl	$0, -260(%rbp)
	movl	$2147483647, %r14d
	.p2align 4,,10
	.p2align 3
.L292:
	movl	-104(%rbp), %eax
	cmpl	%eax, %ebx
	jl	.L290
	movl	%ebx, %ecx
	subl	%eax, %ecx
	cmpl	%r14d, %ecx
	jge	.L290
	movl	-152(%rbp), %edx
	movl	%edx, -260(%rbp)
	testl	%ecx, %ecx
	je	.L291
	movl	%ecx, %r14d
.L290:
	movq	%r13, %rdi
	call	_ZN2v88internal13BreakIterator4NextEv
	cmpl	$-1, -112(%rbp)
	jne	.L292
.L291:
	movl	-260(%rbp), %eax
	leal	-1(%rax), %ebx
	testl	%eax, %eax
	jle	.L293
	.p2align 4,,10
	.p2align 3
.L295:
	movq	%r15, %rdi
	subl	$1, %ebx
	call	_ZN2v88internal13BreakIterator4NextEv
	cmpl	$-1, %ebx
	jne	.L295
.L293:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal13BreakIterator16GetBreakLocationEv
	jmp	.L286
.L303:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22820:
	.size	_ZN2v88internal13BreakLocation9FromFrameENS0_6HandleINS0_9DebugInfoEEEPNS0_15JavaScriptFrameE, .-_ZN2v88internal13BreakLocation9FromFrameENS0_6HandleINS0_9DebugInfoEEEPNS0_15JavaScriptFrameE
	.section	.text._ZN2v88internal13BreakIterator7isolateEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13BreakIterator7isolateEv
	.type	_ZN2v88internal13BreakIterator7isolateEv, @function
_ZN2v88internal13BreakIterator7isolateEv:
.LFB22839:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	(%rax), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	subq	$37592, %rax
	ret
	.cfi_endproc
.LFE22839:
	.size	_ZN2v88internal13BreakIterator7isolateEv, .-_ZN2v88internal13BreakIterator7isolateEv
	.section	.text._ZN2v88internal19DebugFeatureTracker5TrackENS1_7FeatureE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19DebugFeatureTracker5TrackENS1_7FeatureE
	.type	_ZN2v88internal19DebugFeatureTracker5TrackENS1_7FeatureE, @function
_ZN2v88internal19DebugFeatureTracker5TrackENS1_7FeatureE:
.LFB22840:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	$1, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	sall	%cl, %r12d
	testl	%r12d, 8(%rdi)
	jne	.L305
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	movq	40960(%rax), %rdi
	addq	$248, %rdi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	orl	%r12d, 8(%rbx)
.L305:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22840:
	.size	_ZN2v88internal19DebugFeatureTracker5TrackENS1_7FeatureE, .-_ZN2v88internal19DebugFeatureTracker5TrackENS1_7FeatureE
	.section	.text._ZN2v88internal5Debug10ThreadInitEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug10ThreadInitEv
	.type	_ZN2v88internal5Debug10ThreadInitEv, @function
_ZN2v88internal5Debug10ThreadInitEv:
.LFB22841:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movl	$0, 72(%rdi)
	movb	$-1, 76(%rdi)
	movb	$0, 88(%rdi)
	movq	$0, 80(%rdi)
	movl	$-1, 92(%rdi)
	movq	$-1, 96(%rdi)
	movq	$0, 120(%rdi)
	movl	$0, 128(%rdi)
	movups	%xmm0, 104(%rdi)
	movq	$0, 64(%rdi)
	movb	$0, 132(%rdi)
	movq	136(%rdi), %rax
	cmpl	$32, 41828(%rax)
	sete	%al
	movb	%al, 9(%rdi)
	ret
	.cfi_endproc
.LFE22841:
	.size	_ZN2v88internal5Debug10ThreadInitEv, .-_ZN2v88internal5Debug10ThreadInitEv
	.section	.text._ZN2v88internal5Debug12ArchiveDebugEPc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug12ArchiveDebugEPc
	.type	_ZN2v88internal5Debug12ArchiveDebugEPc, @function
_ZN2v88internal5Debug12ArchiveDebugEPc:
.LFB22842:
	.cfi_startproc
	endbr64
	movdqu	64(%rdi), %xmm0
	movups	%xmm0, (%rsi)
	movdqu	80(%rdi), %xmm1
	movups	%xmm1, 16(%rsi)
	movdqu	96(%rdi), %xmm2
	movups	%xmm2, 32(%rsi)
	movdqu	112(%rdi), %xmm3
	movups	%xmm3, 48(%rsi)
	movq	128(%rdi), %rax
	movq	%rax, 64(%rsi)
	leaq	72(%rsi), %rax
	ret
	.cfi_endproc
.LFE22842:
	.size	_ZN2v88internal5Debug12ArchiveDebugEPc, .-_ZN2v88internal5Debug12ArchiveDebugEPc
	.section	.text._ZN2v88internal5Debug21ArchiveSpacePerThreadEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug21ArchiveSpacePerThreadEv
	.type	_ZN2v88internal5Debug21ArchiveSpacePerThreadEv, @function
_ZN2v88internal5Debug21ArchiveSpacePerThreadEv:
.LFB22844:
	.cfi_startproc
	endbr64
	movl	$72, %eax
	ret
	.cfi_endproc
.LFE22844:
	.size	_ZN2v88internal5Debug21ArchiveSpacePerThreadEv, .-_ZN2v88internal5Debug21ArchiveSpacePerThreadEv
	.section	.text._ZN2v88internal5Debug7IterateEPNS0_11RootVisitorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug7IterateEPNS0_11RootVisitorE
	.type	_ZN2v88internal5Debug7IterateEPNS0_11RootVisitorE, @function
_ZN2v88internal5Debug7IterateEPNS0_11RootVisitorE:
.LFB22845:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	104(%rdi), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rsi), %rax
	movq	%rdi, %rbx
	movq	24(%rax), %r8
	cmpq	%r13, %r8
	jne	.L314
	leaq	112(%rdi), %r14
	xorl	%edx, %edx
	movl	$8, %esi
	movq	%r12, %rdi
	movq	%r14, %r8
	call	*16(%rax)
	movq	(%r12), %rax
	movq	24(%rax), %r8
	cmpq	%r13, %r8
	jne	.L316
.L320:
	leaq	120(%rbx), %r8
	movq	%r14, %rcx
	xorl	%edx, %edx
	movl	$8, %esi
	movq	%r12, %rdi
	call	*16(%rax)
.L317:
	movq	(%r12), %rax
	leaq	80(%rbx), %rcx
	movq	24(%rax), %r8
	cmpq	%r13, %r8
	jne	.L318
	leaq	88(%rbx), %r8
	movq	%r12, %rdi
	popq	%rbx
	movq	16(%rax), %rax
	popq	%r12
	xorl	%edx, %edx
	popq	%r13
	movl	$8, %esi
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L314:
	.cfi_restore_state
	xorl	%edx, %edx
	movl	$8, %esi
	leaq	112(%rbx), %r14
	movq	%r12, %rdi
	call	*%r8
	movq	(%r12), %rax
	movq	24(%rax), %r8
	cmpq	%r13, %r8
	je	.L320
.L316:
	movq	%r14, %rcx
	xorl	%edx, %edx
	movl	$8, %esi
	movq	%r12, %rdi
	call	*%r8
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L318:
	popq	%rbx
	movq	%r12, %rdi
	xorl	%edx, %edx
	popq	%r12
	movl	$8, %esi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%r8
	.cfi_endproc
.LFE22845:
	.size	_ZN2v88internal5Debug7IterateEPNS0_11RootVisitorE, .-_ZN2v88internal5Debug7IterateEPNS0_11RootVisitorE
	.section	.text._ZN2v88internal17DebugInfoListNodeC2EPNS0_7IsolateENS0_9DebugInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17DebugInfoListNodeC2EPNS0_7IsolateENS0_9DebugInfoE
	.type	_ZN2v88internal17DebugInfoListNodeC2EPNS0_7IsolateENS0_9DebugInfoE, @function
_ZN2v88internal17DebugInfoListNodeC2EPNS0_7IsolateENS0_9DebugInfoE:
.LFB22847:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rdx, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	$0, 8(%rdi)
	movq	41152(%r8), %rdi
	call	_ZN2v88internal13GlobalHandles6CreateENS0_6ObjectE@PLT
	movq	%rax, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22847:
	.size	_ZN2v88internal17DebugInfoListNodeC2EPNS0_7IsolateENS0_9DebugInfoE, .-_ZN2v88internal17DebugInfoListNodeC2EPNS0_7IsolateENS0_9DebugInfoE
	.globl	_ZN2v88internal17DebugInfoListNodeC1EPNS0_7IsolateENS0_9DebugInfoE
	.set	_ZN2v88internal17DebugInfoListNodeC1EPNS0_7IsolateENS0_9DebugInfoE,_ZN2v88internal17DebugInfoListNodeC2EPNS0_7IsolateENS0_9DebugInfoE
	.section	.text._ZN2v88internal17DebugInfoListNodeD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17DebugInfoListNodeD2Ev
	.type	_ZN2v88internal17DebugInfoListNodeD2Ev, @function
_ZN2v88internal17DebugInfoListNodeD2Ev:
.LFB22850:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L323
	jmp	_ZN2v88internal13GlobalHandles7DestroyEPm@PLT
	.p2align 4,,10
	.p2align 3
.L323:
	ret
	.cfi_endproc
.LFE22850:
	.size	_ZN2v88internal17DebugInfoListNodeD2Ev, .-_ZN2v88internal17DebugInfoListNodeD2Ev
	.globl	_ZN2v88internal17DebugInfoListNodeD1Ev
	.set	_ZN2v88internal17DebugInfoListNodeD1Ev,_ZN2v88internal17DebugInfoListNodeD2Ev
	.section	.text._ZN2v88internal5Debug15CheckBreakPointENS0_6HandleINS0_10BreakPointEEEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug15CheckBreakPointENS0_6HandleINS0_10BreakPointEEEb
	.type	_ZN2v88internal5Debug15CheckBreakPointENS0_6HandleINS0_10BreakPointEEEb, @function
_ZN2v88internal5Debug15CheckBreakPointENS0_6HandleINS0_10BreakPointEEEb:
.LFB22865:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	136(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	41104(%r12), %eax
	movq	41088(%r12), %r15
	movq	41096(%r12), %r14
	leal	1(%rax), %edx
	movl	%edx, 41104(%r12)
	movq	(%rsi), %rdx
	movq	15(%rdx), %rsi
	movl	11(%rsi), %edx
	testl	%edx, %edx
	je	.L326
	movq	136(%rdi), %rdx
	movq	%rdi, %rbx
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L327
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	136(%rbx), %rdi
	movq	%rax, %rcx
	testb	%r13b, %r13b
	je	.L330
.L343:
	movq	%rcx, %rsi
	call	_ZN2v88internal13DebugEvaluate20WithTopmostArgumentsEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	136(%rbx), %rsi
	testq	%rax, %rax
	je	.L340
.L332:
	movq	(%rax), %rax
	leaq	-64(%rbp), %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6Object12BooleanValueEPNS0_7IsolateE@PLT
	movq	41096(%r12), %rdx
	movl	%eax, %r13d
	movl	41104(%r12), %eax
	subl	$1, %eax
.L334:
	movq	%r15, 41088(%r12)
	movl	%eax, 41104(%r12)
	cmpq	%rdx, %r14
	je	.L325
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L325:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L341
	addq	$40, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L327:
	.cfi_restore_state
	movq	41088(%rdx), %rcx
	cmpq	41096(%rdx), %rcx
	je	.L342
.L329:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%rcx)
	movq	136(%rbx), %rdi
	testb	%r13b, %r13b
	jne	.L343
.L330:
	movl	72(%rbx), %esi
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	call	_ZN2v88internal13DebugEvaluate5LocalEPNS0_7IsolateENS0_12StackFrameIdEiNS0_6HandleINS0_6StringEEEb@PLT
	movq	136(%rbx), %rsi
	testq	%rax, %rax
	jne	.L332
.L340:
	movq	96(%rsi), %rax
	cmpq	12480(%rsi), %rax
	je	.L339
	movq	%rax, 12480(%rsi)
.L339:
	movl	41104(%r12), %eax
	movq	41096(%r12), %rdx
	xorl	%r13d, %r13d
	subl	$1, %eax
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L326:
	movl	%eax, 41104(%r12)
	movl	$1, %r13d
	jmp	.L325
	.p2align 4,,10
	.p2align 3
.L342:
	movq	%rdx, %rdi
	movq	%rsi, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %rsi
	movq	-72(%rbp), %rdx
	movq	%rax, %rcx
	jmp	.L329
.L341:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22865:
	.size	_ZN2v88internal5Debug15CheckBreakPointENS0_6HandleINS0_10BreakPointEEEb, .-_ZN2v88internal5Debug15CheckBreakPointENS0_6HandleINS0_10BreakPointEEEb
	.section	.text._ZN2v88internal5Debug21FindBreakablePositionENS0_6HandleINS0_9DebugInfoEEEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug21FindBreakablePositionENS0_6HandleINS0_9DebugInfoEEEi
	.type	_ZN2v88internal5Debug21FindBreakablePositionENS0_6HandleINS0_9DebugInfoEEEi, @function
_ZN2v88internal5Debug21FindBreakablePositionENS0_6HandleINS0_9DebugInfoEEEi:
.LFB22868:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-160(%rbp), %r14
	pushq	%r13
	movq	%r14, %rdi
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$216, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	%rax, -160(%rbp)
	call	_ZNK2v88internal9DebugInfo15CanBreakAtEntryEv@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	testb	%r8b, %r8b
	je	.L392
.L344:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L393
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L392:
	.cfi_restore_state
	leaq	-256(%rbp), %r12
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal13BreakIteratorC1ENS0_6HandleINS0_9DebugInfoEEE
	movq	-256(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal13BreakIteratorC1ENS0_6HandleINS0_9DebugInfoEEE
	cmpl	$-1, -112(%rbp)
	movl	-152(%rbp), %r14d
	je	.L350
	movl	-148(%rbp), %eax
	movl	%r14d, %edx
	movl	$2147483647, %r13d
	leaq	-136(%rbp), %r15
	jmp	.L363
	.p2align 4,,10
	.p2align 3
.L394:
	movl	$108543, %ecx
	btq	%rdx, %rcx
	jnc	.L356
.L391:
	movl	-152(%rbp), %edx
	addl	$1, %edx
	movl	%edx, -152(%rbp)
.L363:
	cmpl	%eax, %ebx
	jg	.L349
	subl	%ebx, %eax
	cmpl	%r13d, %eax
	jge	.L349
	movl	%edx, %r14d
	testl	%eax, %eax
	je	.L350
	movl	%eax, %r13d
.L349:
	cmpl	$-1, %edx
	jne	.L357
	.p2align 4,,10
	.p2align 3
.L364:
	movq	-96(%rbp), %rax
	movzbl	-88(%rbp), %esi
	shrq	%rax
	andl	$1073741823, %eax
	subl	$1, %eax
	movl	%eax, -148(%rbp)
	testb	%sil, %sil
	je	.L351
	movl	%eax, -144(%rbp)
.L351:
	movq	-160(%rbp), %rdx
	movl	-104(%rbp), %ecx
	movq	(%rdx), %rdx
	movq	31(%rdx), %rdi
	leal	54(%rcx), %edx
	movslq	%edx, %rdx
	movzbl	-1(%rdi,%rdx), %edx
	cmpb	$3, %dl
	ja	.L352
	addl	$55, %ecx
	movslq	%ecx, %rcx
	movzbl	-1(%rdi,%rcx), %edx
.L352:
	leal	85(%rdx), %ecx
	andl	$247, %ecx
	je	.L391
	cmpb	$-80, %dl
	je	.L391
	subl	$86, %edx
	cmpb	$16, %dl
	jbe	.L394
.L356:
	testb	%sil, %sil
	jne	.L391
.L357:
	movq	%r15, %rdi
	call	_ZN2v88internal27SourcePositionTableIterator7AdvanceEv@PLT
	cmpl	$-1, -112(%rbp)
	jne	.L364
.L350:
	leal	-1(%r14), %ebx
	leaq	-232(%rbp), %r13
	testl	%r14d, %r14d
	jle	.L348
	.p2align 4,,10
	.p2align 3
.L347:
	cmpl	$-1, -208(%rbp)
	movl	-248(%rbp), %eax
	je	.L358
	cmpl	$-1, %eax
	jne	.L362
	.p2align 4,,10
	.p2align 3
.L359:
	movq	-192(%rbp), %rax
	shrq	%rax
	andl	$1073741823, %eax
	subl	$1, %eax
	cmpb	$0, -184(%rbp)
	movl	%eax, -244(%rbp)
	je	.L361
	movl	%eax, -240(%rbp)
.L361:
	movq	%r12, %rdi
	call	_ZN2v88internal13BreakIterator17GetDebugBreakTypeEv
	testl	%eax, %eax
	jne	.L395
.L362:
	movq	%r13, %rdi
	call	_ZN2v88internal27SourcePositionTableIterator7AdvanceEv@PLT
	cmpl	$-1, -208(%rbp)
	jne	.L359
	subl	$1, %ebx
	cmpl	$-1, %ebx
	jne	.L347
.L348:
	movl	-244(%rbp), %eax
	jmp	.L344
	.p2align 4,,10
	.p2align 3
.L395:
	movl	-248(%rbp), %eax
.L358:
	addl	$1, %eax
	subl	$1, %ebx
	movl	%eax, -248(%rbp)
	cmpl	$-1, %ebx
	jne	.L347
	jmp	.L348
.L393:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22868:
	.size	_ZN2v88internal5Debug21FindBreakablePositionENS0_6HandleINS0_9DebugInfoEEEi, .-_ZN2v88internal5Debug21FindBreakablePositionENS0_6HandleINS0_9DebugInfoEEEi
	.section	.text._ZN2v88internal5Debug16ApplyBreakPointsENS0_6HandleINS0_9DebugInfoEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug16ApplyBreakPointsENS0_6HandleINS0_9DebugInfoEEE
	.type	_ZN2v88internal5Debug16ApplyBreakPointsENS0_6HandleINS0_9DebugInfoEEE, @function
_ZN2v88internal5Debug16ApplyBreakPointsENS0_6HandleINS0_9DebugInfoEEE:
.LFB22869:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-160(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$152, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	%rax, -160(%rbp)
	call	_ZNK2v88internal9DebugInfo15CanBreakAtEntryEv@PLT
	testb	%al, %al
	movq	(%rbx), %rax
	je	.L397
	movq	%r12, %rdi
	movq	%rax, -160(%rbp)
	call	_ZN2v88internal9DebugInfo15SetBreakAtEntryEv@PLT
	movq	(%rbx), %rax
.L398:
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -160(%rbp)
	call	_ZN2v88internal9DebugInfo21SetDebugExecutionModeENS1_13ExecutionModeE@PLT
.L396:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L415
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L397:
	.cfi_restore_state
	movq	39(%rax), %rdx
	testb	$1, %dl
	je	.L396
	movq	-1(%rdx), %rdx
	cmpw	$72, 11(%rdx)
	jne	.L396
	movq	47(%rax), %rdx
	leaq	7(%rdx), %rcx
	movq	%rcx, -184(%rbp)
	movl	11(%rdx), %ecx
	testl	%ecx, %ecx
	jle	.L398
	leaq	-168(%rbp), %rax
	leaq	15(%rdx), %r15
	xorl	%r14d, %r14d
	movq	%rax, -192(%rbp)
	.p2align 4,,10
	.p2align 3
.L403:
	movq	(%r15), %rax
	movq	136(%r13), %rsi
	cmpq	%rax, 88(%rsi)
	je	.L405
	movq	(%r15), %rax
	movq	-192(%rbp), %rdi
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal14BreakPointInfo18GetBreakPointCountEPNS0_7IsolateE@PLT
	testl	%eax, %eax
	je	.L405
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal13BreakIteratorC1ENS0_6HandleINS0_9DebugInfoEEE
	movq	-168(%rbp), %rax
	movq	%r12, %rdi
	movslq	11(%rax), %rsi
	call	_ZN2v88internal13BreakIterator14SkipToPositionEi
	movq	%r12, %rdi
	call	_ZN2v88internal13BreakIterator17GetDebugBreakTypeEv
	cmpl	$1, %eax
	je	.L405
	call	_ZN2v88internal13BreakIterator13SetDebugBreakEv.part.0
.L405:
	movq	-184(%rbp), %rax
	addl	$1, %r14d
	addq	$8, %r15
	cmpl	%r14d, 4(%rax)
	jg	.L403
	movq	(%rbx), %rax
	jmp	.L398
.L415:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22869:
	.size	_ZN2v88internal5Debug16ApplyBreakPointsENS0_6HandleINS0_9DebugInfoEEE, .-_ZN2v88internal5Debug16ApplyBreakPointsENS0_6HandleINS0_9DebugInfoEEE
	.section	.text._ZN2v88internal5Debug16ClearBreakPointsENS0_6HandleINS0_9DebugInfoEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug16ClearBreakPointsENS0_6HandleINS0_9DebugInfoEEE
	.type	_ZN2v88internal5Debug16ClearBreakPointsENS0_6HandleINS0_9DebugInfoEEE, @function
_ZN2v88internal5Debug16ClearBreakPointsENS0_6HandleINS0_9DebugInfoEEE:
.LFB22870:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-128(%rbp), %r13
	movq	%rsi, %r12
	movq	%r13, %rdi
	subq	$112, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	%rax, -128(%rbp)
	call	_ZNK2v88internal9DebugInfo15CanBreakAtEntryEv@PLT
	testb	%al, %al
	movq	(%r12), %rax
	je	.L417
	movq	%r13, %rdi
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal9DebugInfo17ClearBreakAtEntryEv@PLT
.L416:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L432
	addq	$112, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L417:
	.cfi_restore_state
	movq	39(%rax), %rdx
	testb	$1, %dl
	je	.L416
	movq	-1(%rdx), %rdx
	cmpw	$72, 11(%rdx)
	jne	.L416
	movq	%r13, %rdi
	movq	%rax, -128(%rbp)
	call	_ZNK2v88internal9DebugInfo12HasBreakInfoEv@PLT
	testb	%al, %al
	je	.L416
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal13BreakIteratorC1ENS0_6HandleINS0_9DebugInfoEEE
	cmpl	$-1, -80(%rbp)
	je	.L416
.L421:
	movq	%r13, %rdi
	call	_ZN2v88internal13BreakIterator17GetDebugBreakTypeEv
	cmpl	$1, %eax
	je	.L422
.L433:
	movq	-128(%rbp), %rax
	movq	(%rax), %rdx
	movl	-72(%rbp), %eax
	movq	31(%rdx), %rcx
	addl	$54, %eax
	movq	39(%rdx), %rdx
	cltq
	movzbl	-1(%rax,%rcx), %ecx
	movb	%cl, -1(%rax,%rdx)
	call	_ZN2v88internal13BreakIterator4NextEv
	cmpl	$-1, -80(%rbp)
	je	.L416
	movq	%r13, %rdi
	call	_ZN2v88internal13BreakIterator17GetDebugBreakTypeEv
	cmpl	$1, %eax
	jne	.L433
.L422:
	call	_ZN2v88internal13BreakIterator4NextEv
	cmpl	$-1, -80(%rbp)
	jne	.L421
	jmp	.L416
.L432:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22870:
	.size	_ZN2v88internal5Debug16ClearBreakPointsENS0_6HandleINS0_9DebugInfoEEE, .-_ZN2v88internal5Debug16ClearBreakPointsENS0_6HandleINS0_9DebugInfoEEE
	.section	.text._ZNSt17_Function_handlerIFvN2v88internal6HandleINS1_9DebugInfoEEEEZNS1_5Debug19ClearAllBreakPointsEvEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvN2v88internal6HandleINS1_9DebugInfoEEEEZNS1_5Debug19ClearAllBreakPointsEvEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_, @function
_ZNSt17_Function_handlerIFvN2v88internal6HandleINS1_9DebugInfoEEEEZNS1_5Debug19ClearAllBreakPointsEvEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_:
.LFB27443:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	(%rsi), %r12
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%r12, %rsi
	call	_ZN2v88internal5Debug16ClearBreakPointsENS0_6HandleINS0_9DebugInfoEEE
	movq	(%r12), %rax
	leaq	-32(%rbp), %rdi
	movq	%rax, -32(%rbp)
	movq	(%rbx), %rax
	movq	136(%rax), %rsi
	call	_ZN2v88internal9DebugInfo14ClearBreakInfoEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L437
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L437:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27443:
	.size	_ZNSt17_Function_handlerIFvN2v88internal6HandleINS1_9DebugInfoEEEEZNS1_5Debug19ClearAllBreakPointsEvEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_, .-_ZNSt17_Function_handlerIFvN2v88internal6HandleINS1_9DebugInfoEEEEZNS1_5Debug19ClearAllBreakPointsEvEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_
	.section	.text._ZN2v88internal5Debug22ChangeBreakOnExceptionENS0_18ExceptionBreakTypeEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug22ChangeBreakOnExceptionENS0_18ExceptionBreakTypeEb
	.type	_ZN2v88internal5Debug22ChangeBreakOnExceptionENS0_18ExceptionBreakTypeEb, @function
_ZN2v88internal5Debug22ChangeBreakOnExceptionENS0_18ExceptionBreakTypeEb:
.LFB22881:
	.cfi_startproc
	endbr64
	cmpl	$1, %esi
	je	.L441
	movb	%dl, 14(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L441:
	movb	%dl, 15(%rdi)
	ret
	.cfi_endproc
.LFE22881:
	.size	_ZN2v88internal5Debug22ChangeBreakOnExceptionENS0_18ExceptionBreakTypeEb, .-_ZN2v88internal5Debug22ChangeBreakOnExceptionENS0_18ExceptionBreakTypeEb
	.section	.text._ZN2v88internal5Debug18IsBreakOnExceptionENS0_18ExceptionBreakTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug18IsBreakOnExceptionENS0_18ExceptionBreakTypeE
	.type	_ZN2v88internal5Debug18IsBreakOnExceptionENS0_18ExceptionBreakTypeE, @function
_ZN2v88internal5Debug18IsBreakOnExceptionENS0_18ExceptionBreakTypeE:
.LFB22882:
	.cfi_startproc
	endbr64
	movzbl	14(%rdi), %edx
	movzbl	15(%rdi), %eax
	cmpl	$1, %esi
	cmovne	%edx, %eax
	ret
	.cfi_endproc
.LFE22882:
	.size	_ZN2v88internal5Debug18IsBreakOnExceptionENS0_18ExceptionBreakTypeE, .-_ZN2v88internal5Debug18IsBreakOnExceptionENS0_18ExceptionBreakTypeE
	.section	.text._ZN2v88internal5Debug17GetHitBreakPointsENS0_6HandleINS0_9DebugInfoEEEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug17GetHitBreakPointsENS0_6HandleINS0_9DebugInfoEEEi
	.type	_ZN2v88internal5Debug17GetHitBreakPointsENS0_6HandleINS0_9DebugInfoEEEi, @function
_ZN2v88internal5Debug17GetHitBreakPointsENS0_6HandleINS0_9DebugInfoEEEi:
.LFB22883:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-64(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	136(%rdi), %rsi
	movq	%r13, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal9DebugInfo14GetBreakPointsEPNS0_7IsolateEi@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	movq	(%r12), %rax
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal9DebugInfo12BreakAtEntryEv@PLT
	movq	(%r14), %r15
	movl	%eax, %r12d
	testb	$1, %r15b
	jne	.L447
.L450:
	movzbl	%r12b, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal5Debug15CheckBreakPointENS0_6HandleINS0_10BreakPointEEEb
	testb	%al, %al
	jne	.L495
.L469:
	xorl	%eax, %eax
.L457:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L496
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L495:
	.cfi_restore_state
	movq	136(%rbx), %rdi
	xorl	%edx, %edx
	movl	$1, %esi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	(%r14), %r12
	movq	(%rax), %r13
	movq	%rax, %rbx
	movq	%r12, 15(%r13)
	leaq	15(%r13), %r15
	testb	$1, %r12b
	je	.L471
	movq	%r12, %r14
	andq	$-262144, %r14
	movq	8(%r14), %rax
	testl	$262144, %eax
	je	.L455
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r14), %rax
.L455:
	testb	$24, %al
	je	.L471
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L497
	.p2align 4,,10
	.p2align 3
.L471:
	movq	%rbx, %rax
	jmp	.L457
	.p2align 4,,10
	.p2align 3
.L447:
	movq	-1(%r15), %rax
	movzwl	11(%rax), %eax
	subl	$123, %eax
	cmpw	$14, %ax
	ja	.L450
	movq	136(%rbx), %rdx
	movq	%r15, %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L498
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r14
.L458:
	movslq	11(%rsi), %r15
	movq	136(%rbx), %rdi
	xorl	%edx, %edx
	movl	%r15d, %esi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	%rax, -88(%rbp)
	testq	%r15, %r15
	jle	.L469
	leal	-1(%r15), %eax
	movzbl	%r12b, %ecx
	movl	$16, %r12d
	movq	%r13, -96(%rbp)
	leaq	24(,%rax,8), %rax
	movl	$0, -80(%rbp)
	movq	%r12, %r15
	movl	%ecx, %r13d
	movq	%rax, -72(%rbp)
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L501:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r12
.L462:
	movl	%r13d, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal5Debug15CheckBreakPointENS0_6HandleINS0_10BreakPointEEEb
	testb	%al, %al
	jne	.L499
	addq	$8, %r15
	cmpq	%r15, -72(%rbp)
	je	.L500
.L468:
	movq	136(%rbx), %rdx
	movq	(%r14), %rax
	movq	-1(%r15,%rax), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	jne	.L501
	movq	41088(%rdx), %r12
	cmpq	41096(%rdx), %r12
	je	.L502
.L463:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%r12)
	jmp	.L462
	.p2align 4,,10
	.p2align 3
.L499:
	movq	-88(%rbp), %rax
	movq	(%r12), %rdx
	movq	(%rax), %rdi
	movl	-80(%rbp), %eax
	leal	1(%rax), %ecx
	leal	16(,%rax,8), %eax
	cltq
	leaq	-1(%rdi,%rax), %r12
	movq	%rdx, (%r12)
	testb	$1, %dl
	je	.L470
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	8(%rax), %rsi
	movq	%rax, -80(%rbp)
	testl	$262144, %esi
	je	.L466
	movq	%r12, %rsi
	movl	%ecx, -116(%rbp)
	movq	%rdx, -112(%rbp)
	movq	%rdi, -104(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %rax
	movl	-116(%rbp), %ecx
	movq	-112(%rbp), %rdx
	movq	-104(%rbp), %rdi
	movq	8(%rax), %rsi
.L466:
	andl	$24, %esi
	je	.L470
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L470
	movq	%r12, %rsi
	movl	%ecx, -80(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movl	-80(%rbp), %ecx
	.p2align 4,,10
	.p2align 3
.L470:
	movl	%ecx, -80(%rbp)
	addq	$8, %r15
	cmpq	%r15, -72(%rbp)
	jne	.L468
.L500:
	movl	-80(%rbp), %eax
	movq	-96(%rbp), %r13
	testl	%eax, %eax
	je	.L469
	movq	-88(%rbp), %r14
	movq	136(%rbx), %rsi
	movq	%r13, %rdi
	movl	-80(%rbp), %edx
	movq	(%r14), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal10FixedArray6ShrinkEPNS0_7IsolateEi@PLT
	movq	%r14, %rax
	jmp	.L457
	.p2align 4,,10
	.p2align 3
.L502:
	movq	%rdx, %rdi
	movq	%rsi, -112(%rbp)
	movq	%rdx, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-112(%rbp), %rsi
	movq	-104(%rbp), %rdx
	movq	%rax, %r12
	jmp	.L463
	.p2align 4,,10
	.p2align 3
.L498:
	movq	41088(%rdx), %r14
	cmpq	41096(%rdx), %r14
	je	.L503
.L459:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%rdx)
	movq	%r15, (%r14)
	jmp	.L458
	.p2align 4,,10
	.p2align 3
.L497:
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L471
.L503:
	movq	%rdx, %rdi
	movq	%rdx, -72(%rbp)
	movq	%r15, -80(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %rsi
	movq	-72(%rbp), %rdx
	movq	%rax, %r14
	jmp	.L459
.L496:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22883:
	.size	_ZN2v88internal5Debug17GetHitBreakPointsENS0_6HandleINS0_9DebugInfoEEEi, .-_ZN2v88internal5Debug17GetHitBreakPointsENS0_6HandleINS0_9DebugInfoEEEi
	.section	.text._ZN2v88internal5Debug16CheckBreakPointsENS0_6HandleINS0_9DebugInfoEEEPNS0_13BreakLocationEPb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug16CheckBreakPointsENS0_6HandleINS0_9DebugInfoEEEPNS0_13BreakLocationEPb
	.type	_ZN2v88internal5Debug16CheckBreakPointsENS0_6HandleINS0_9DebugInfoEEEPNS0_13BreakLocationEPb, @function
_ZN2v88internal5Debug16CheckBreakPointsENS0_6HandleINS0_9DebugInfoEEEPNS0_13BreakLocationEPb:
.LFB22854:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 13(%rdi)
	jne	.L520
.L505:
	testq	%rbx, %rbx
	je	.L509
	movb	$0, (%rbx)
.L509:
	xorl	%eax, %eax
.L511:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L521
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L520:
	.cfi_restore_state
	movq	%rsi, %r14
	movq	%rdx, %r13
	movq	136(%rdi), %rsi
	movl	16(%rdx), %edx
	movq	(%r14), %rax
	leaq	-160(%rbp), %r15
	movq	%rdi, %r12
	movq	%r15, %rdi
	movq	%rax, -160(%rbp)
	call	_ZN2v88internal9DebugInfo13HasBreakPointEPNS0_7IsolateEi@PLT
	testb	%al, %al
	je	.L505
	movq	(%r14), %rax
	movq	%r15, %rdi
	movq	%rax, -160(%rbp)
	call	_ZNK2v88internal9DebugInfo15CanBreakAtEntryEv@PLT
	testb	%al, %al
	je	.L506
	movq	(%r14), %rax
	movq	%r15, %rdi
	movq	%rax, -160(%rbp)
	call	_ZNK2v88internal9DebugInfo12BreakAtEntryEv@PLT
.L507:
	testb	%al, %al
	je	.L505
	testq	%rbx, %rbx
	je	.L510
	movb	$1, (%rbx)
.L510:
	movl	16(%r13), %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal5Debug17GetHitBreakPointsENS0_6HandleINS0_9DebugInfoEEEi
	jmp	.L511
	.p2align 4,,10
	.p2align 3
.L506:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal13BreakIteratorC1ENS0_6HandleINS0_9DebugInfoEEE
	movl	16(%r13), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal13BreakIterator14SkipToPositionEi
	movl	8(%r13), %eax
	cmpl	%eax, -104(%rbp)
	sete	%al
	jmp	.L507
.L521:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22854:
	.size	_ZN2v88internal5Debug16CheckBreakPointsENS0_6HandleINS0_9DebugInfoEEEPNS0_13BreakLocationEPb, .-_ZN2v88internal5Debug16CheckBreakPointsENS0_6HandleINS0_9DebugInfoEEEPNS0_13BreakLocationEPb
	.section	.text._ZN2v88internal5Debug26SetBreakOnNextFunctionCallEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug26SetBreakOnNextFunctionCallEv
	.type	_ZN2v88internal5Debug26SetBreakOnNextFunctionCallEv, @function
_ZN2v88internal5Debug26SetBreakOnNextFunctionCallEv:
.LFB22884:
	.cfi_startproc
	endbr64
	movb	$1, 132(%rdi)
	movb	$1, 9(%rdi)
	ret
	.cfi_endproc
.LFE22884:
	.size	_ZN2v88internal5Debug26SetBreakOnNextFunctionCallEv, .-_ZN2v88internal5Debug26SetBreakOnNextFunctionCallEv
	.section	.text._ZN2v88internal5Debug28ClearBreakOnNextFunctionCallEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug28ClearBreakOnNextFunctionCallEv
	.type	_ZN2v88internal5Debug28ClearBreakOnNextFunctionCallEv, @function
_ZN2v88internal5Debug28ClearBreakOnNextFunctionCallEv:
.LFB22885:
	.cfi_startproc
	endbr64
	cmpb	$2, 76(%rdi)
	movb	$0, 132(%rdi)
	movl	$1, %eax
	je	.L524
	movq	136(%rdi), %rax
	cmpl	$32, 41828(%rax)
	sete	%al
.L524:
	movb	%al, 9(%rdi)
	ret
	.cfi_endproc
.LFE22885:
	.size	_ZN2v88internal5Debug28ClearBreakOnNextFunctionCallEv, .-_ZN2v88internal5Debug28ClearBreakOnNextFunctionCallEv
	.section	.text._ZN2v88internal5Debug23GetSourceBreakLocationsEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug23GetSourceBreakLocationsEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEE
	.type	_ZN2v88internal5Debug23GetSourceBreakLocationsEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEE, @function
_ZN2v88internal5Debug23GetSourceBreakLocationsEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEE:
.LFB22923:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-64(%rbp), %rbx
	movq	%rbx, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo12HasBreakInfoEv@PLT
	testb	%al, %al
	je	.L532
	movq	(%r12), %rax
	movq	41112(%r13), %rdi
	movq	31(%rax), %rsi
	testq	%rdi, %rdi
	je	.L529
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r12
.L530:
	movq	%rsi, -64(%rbp)
	movq	%rbx, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal9DebugInfo18GetBreakPointCountEPNS0_7IsolateE@PLT
	testl	%eax, %eax
	jne	.L551
.L532:
	leaq	88(%r13), %rax
.L528:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L552
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L529:
	.cfi_restore_state
	movq	41088(%r13), %r12
	cmpq	%r12, 41096(%r13)
	je	.L553
.L531:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r12)
	jmp	.L530
	.p2align 4,,10
	.p2align 3
.L551:
	movq	(%r12), %rax
	movq	%r13, %rsi
	movq	%rbx, %rdi
	xorl	%r14d, %r14d
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal9DebugInfo18GetBreakPointCountEPNS0_7IsolateE@PLT
	xorl	%edx, %edx
	movq	%r13, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	(%r12), %rcx
	movl	$0, -72(%rbp)
	movq	%rax, %r15
	movq	47(%rcx), %rax
	movl	11(%rax), %edx
	testl	%edx, %edx
	jg	.L538
	jmp	.L539
	.p2align 4,,10
	.p2align 3
.L550:
	movq	(%r12), %rcx
.L534:
	movq	47(%rcx), %rax
	addq	$1, %r14
	cmpl	%r14d, 11(%rax)
	jle	.L539
.L538:
	leaq	16(,%r14,8), %rdx
	movq	-1(%rax,%rdx), %rax
	cmpq	%rax, 88(%r13)
	je	.L534
	movq	47(%rcx), %rax
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	-1(%rdx,%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal14BreakPointInfo18GetBreakPointCountEPNS0_7IsolateE@PLT
	testl	%eax, %eax
	je	.L550
	jle	.L550
	movslq	-72(%rbp), %rcx
	leal	-1(%rax), %esi
	leal	16(,%rcx,8), %edx
	leaq	3(%rcx,%rsi), %rdi
	movslq	%edx, %rdx
	salq	$3, %rdi
	.p2align 4,,10
	.p2align 3
.L537:
	movq	-64(%rbp), %rcx
	movq	(%r15), %rsi
	addq	$8, %rdx
	movslq	11(%rcx), %rcx
	salq	$32, %rcx
	movq	%rcx, -9(%rdx,%rsi)
	cmpq	%rdx, %rdi
	jne	.L537
	addl	%eax, -72(%rbp)
	jmp	.L550
	.p2align 4,,10
	.p2align 3
.L539:
	movq	%r15, %rax
	jmp	.L528
	.p2align 4,,10
	.p2align 3
.L553:
	movq	%r13, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L531
.L552:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22923:
	.size	_ZN2v88internal5Debug23GetSourceBreakLocationsEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEE, .-_ZN2v88internal5Debug23GetSourceBreakLocationsEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEE
	.section	.text._ZN2v88internal5Debug13ClearSteppingEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug13ClearSteppingEv
	.type	_ZN2v88internal5Debug13ClearSteppingEv, @function
_ZN2v88internal5Debug13ClearSteppingEv:
.LFB22924:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	24(%rdi), %r12
	testq	%r12, %r12
	je	.L555
	.p2align 4,,10
	.p2align 3
.L556:
	movq	(%r12), %r13
	movq	%rbx, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal5Debug16ClearBreakPointsENS0_6HandleINS0_9DebugInfoEEE
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal5Debug16ApplyBreakPointsENS0_6HandleINS0_9DebugInfoEEE
	movq	8(%r12), %r12
	testq	%r12, %r12
	jne	.L556
.L555:
	movq	136(%rbx), %rax
	movb	$-1, 76(%rbx)
	movq	$0, 80(%rbx)
	movb	$0, 88(%rbx)
	movl	$-1, 92(%rbx)
	movq	$-1, 96(%rbx)
	movb	$0, 132(%rbx)
	cmpl	$32, 41828(%rax)
	sete	9(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22924:
	.size	_ZN2v88internal5Debug13ClearSteppingEv, .-_ZN2v88internal5Debug13ClearSteppingEv
	.section	.text._ZN2v88internal5Debug12ClearOneShotEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug12ClearOneShotEv
	.type	_ZN2v88internal5Debug12ClearOneShotEv, @function
_ZN2v88internal5Debug12ClearOneShotEv:
.LFB22925:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	24(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L562
	movq	%rdi, %r13
	.p2align 4,,10
	.p2align 3
.L564:
	movq	(%rbx), %r12
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal5Debug16ClearBreakPointsENS0_6HandleINS0_9DebugInfoEEE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal5Debug16ApplyBreakPointsENS0_6HandleINS0_9DebugInfoEEE
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L564
.L562:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22925:
	.size	_ZN2v88internal5Debug12ClearOneShotEv, .-_ZN2v88internal5Debug12ClearOneShotEv
	.section	.text._ZN2v88internal5Debug18DeoptimizeFunctionENS0_6HandleINS0_18SharedFunctionInfoEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug18DeoptimizeFunctionENS0_6HandleINS0_18SharedFunctionInfoEEE
	.type	_ZN2v88internal5Debug18DeoptimizeFunctionENS0_6HandleINS0_18SharedFunctionInfoEEE, @function
_ZN2v88internal5Debug18DeoptimizeFunctionENS0_6HandleINS0_18SharedFunctionInfoEEE:
.LFB22926:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-88(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-80(%rbp), %rbx
	subq	$56, %rsp
	movq	136(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal7Isolate27AbortConcurrentOptimizationENS0_16BlockingBehaviorE@PLT
	movq	136(%r14), %rax
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movl	$5, %edx
	leaq	37592(%rax), %rdi
	call	_ZN2v88internal4Heap24PreciseCollectAllGarbageEiNS0_23GarbageCollectionReasonENS_15GCCallbackFlagsE@PLT
	movq	136(%r14), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal4Code21OptimizedCodeIteratorC1EPNS0_7IsolateE@PLT
	.p2align 4,,10
	.p2align 3
.L573:
	movq	%rbx, %rdi
	call	_ZN2v88internal4Code21OptimizedCodeIterator4NextEv@PLT
	movq	%rax, -88(%rbp)
	testq	%rax, %rax
	je	.L571
	movq	(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal4Code7InlinesENS0_18SharedFunctionInfoE@PLT
	testb	%al, %al
	je	.L573
	movq	-88(%rbp), %rdx
	movl	%eax, %r15d
	movq	31(%rdx), %rdx
	movl	15(%rdx), %edx
	movq	-88(%rbp), %rcx
	movq	31(%rcx), %rcx
	orl	$1, %edx
	movl	%edx, 15(%rcx)
	jmp	.L573
	.p2align 4,,10
	.p2align 3
.L571:
	testb	%r15b, %r15b
	jne	.L583
.L570:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L584
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L583:
	.cfi_restore_state
	movq	136(%r14), %rdi
	call	_ZN2v88internal11Deoptimizer20DeoptimizeMarkedCodeEPNS0_7IsolateE@PLT
	jmp	.L570
.L584:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22926:
	.size	_ZN2v88internal5Debug18DeoptimizeFunctionENS0_6HandleINS0_18SharedFunctionInfoEEE, .-_ZN2v88internal5Debug18DeoptimizeFunctionENS0_6HandleINS0_18SharedFunctionInfoEEE
	.section	.text._ZN2v88internal5Debug16GetPrivateFieldsENS0_6HandleINS0_10JSReceiverEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug16GetPrivateFieldsENS0_6HandleINS0_10JSReceiverEEE
	.type	_ZN2v88internal5Debug16GetPrivateFieldsENS0_6HandleINS0_10JSReceiverEEE, @function
_ZN2v88internal5Debug16GetPrivateFieldsENS0_6HandleINS0_10JSReceiverEEE:
.LFB22975:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	136(%rdi), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal10JSReceiver17GetPrivateEntriesEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	testq	%rax, %rax
	je	.L590
	movq	%rax, %rsi
	movq	(%rax), %rax
	movslq	11(%rax), %rcx
	testq	%rcx, %rcx
	je	.L591
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	movl	$3, %edx
	call	_ZN2v88internal7Factory22NewJSArrayWithElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE@PLT
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L591:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%r9d, %r9d
	movl	$1, %r8d
	xorl	%edx, %edx
	movl	$3, %esi
	call	_ZN2v88internal7Factory10NewJSArrayENS0_12ElementsKindEiiNS0_26ArrayStorageAllocationModeENS0_14AllocationTypeE@PLT
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L590:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22975:
	.size	_ZN2v88internal5Debug16GetPrivateFieldsENS0_6HandleINS0_10JSReceiverEEE, .-_ZN2v88internal5Debug16GetPrivateFieldsENS0_6HandleINS0_10JSReceiverEEE
	.section	.text._ZN2v88internal5Debug20GetOrCreateDebugInfoENS0_6HandleINS0_18SharedFunctionInfoEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug20GetOrCreateDebugInfoENS0_6HandleINS0_18SharedFunctionInfoEEE
	.type	_ZN2v88internal5Debug20GetOrCreateDebugInfoENS0_6HandleINS0_18SharedFunctionInfoEEE, @function
_ZN2v88internal5Debug20GetOrCreateDebugInfoENS0_6HandleINS0_18SharedFunctionInfoEEE:
.LFB22988:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rsi), %rax
	movq	31(%rax), %r13
	testb	$1, %r13b
	jne	.L593
.L595:
	movq	136(%r12), %rdi
	call	_ZN2v88internal7Factory12NewDebugInfoENS0_6HandleINS0_18SharedFunctionInfoEEE@PLT
	movl	$16, %edi
	movq	(%rax), %r14
	movq	%rax, %rbx
	call	_Znwm@PLT
	movq	%rax, %r13
	movq	136(%r12), %rax
	movq	%r14, %rsi
	movq	$0, 8(%r13)
	movq	41152(%rax), %rdi
	call	_ZN2v88internal13GlobalHandles6CreateENS0_6ObjectE@PLT
	movq	%rax, %xmm0
	movq	%rbx, %rax
	movhps	24(%r12), %xmm0
	movq	%r13, 24(%r12)
	movups	%xmm0, 0(%r13)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L593:
	.cfi_restore_state
	movq	-1(%r13), %rax
	cmpw	$86, 11(%rax)
	jne	.L595
	movq	136(%rdi), %rbx
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L596
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L596:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L600
.L598:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%r13, (%rax)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L600:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L598
	.cfi_endproc
.LFE22988:
	.size	_ZN2v88internal5Debug20GetOrCreateDebugInfoENS0_6HandleINS0_18SharedFunctionInfoEEE, .-_ZN2v88internal5Debug20GetOrCreateDebugInfoENS0_6HandleINS0_18SharedFunctionInfoEEE
	.section	.text._ZN2v88internal5Debug22GetFunctionDebuggingIdENS0_6HandleINS0_10JSFunctionEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug22GetFunctionDebuggingIdENS0_6HandleINS0_10JSFunctionEEE
	.type	_ZN2v88internal5Debug22GetFunctionDebuggingIdENS0_6HandleINS0_10JSFunctionEEE, @function
_ZN2v88internal5Debug22GetFunctionDebuggingIdENS0_6HandleINS0_10JSFunctionEEE:
.LFB22872:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	136(%rdi), %r12
	movq	(%rsi), %rax
	movq	23(%rax), %r13
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L602
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L603:
	movq	%rbx, %rdi
	call	_ZN2v88internal5Debug20GetOrCreateDebugInfoENS0_6HandleINS0_18SharedFunctionInfoEEE
	movq	%rax, %rcx
	movq	(%rax), %rax
	movslq	19(%rax), %rax
	shrl	$4, %eax
	andl	$1048575, %eax
	jne	.L601
	movq	136(%rbx), %rsi
	movslq	4812(%rsi), %rdx
	cmpq	$1048575, %rdx
	je	.L607
	leal	1(%rdx), %eax
	movq	%rax, %rdx
	movl	%eax, %edi
	salq	$32, %rdx
	sall	$4, %edi
.L606:
	movq	%rdx, 4808(%rsi)
	movq	(%rcx), %rcx
	movslq	19(%rcx), %rdx
	andl	$-16777201, %edx
	orl	%edi, %edx
	salq	$32, %rdx
	movq	%rdx, 15(%rcx)
.L601:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L602:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L609
.L604:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, (%rsi)
	jmp	.L603
	.p2align 4,,10
	.p2align 3
.L607:
	movl	$16, %edi
	movl	$1, %eax
	movabsq	$4294967296, %rdx
	jmp	.L606
	.p2align 4,,10
	.p2align 3
.L609:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L604
	.cfi_endproc
.LFE22872:
	.size	_ZN2v88internal5Debug22GetFunctionDebuggingIdENS0_6HandleINS0_10JSFunctionEEE, .-_ZN2v88internal5Debug22GetFunctionDebuggingIdENS0_6HandleINS0_10JSFunctionEEE
	.section	.text._ZN2v88internal5Debug15CreateBreakInfoENS0_6HandleINS0_18SharedFunctionInfoEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug15CreateBreakInfoENS0_6HandleINS0_18SharedFunctionInfoEEE
	.type	_ZN2v88internal5Debug15CreateBreakInfoENS0_6HandleINS0_18SharedFunctionInfoEEE, @function
_ZN2v88internal5Debug15CreateBreakInfoENS0_6HandleINS0_18SharedFunctionInfoEEE:
.LFB22987:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	136(%rdi), %r12
	movq	41088(%r12), %rax
	movq	41096(%r12), %r13
	addl	$1, 41104(%r12)
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal5Debug20GetOrCreateDebugInfoENS0_6HandleINS0_18SharedFunctionInfoEEE
	movq	136(%rbx), %rdi
	xorl	%edx, %edx
	movl	$4, %esi
	movq	%rax, %r14
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	(%r14), %rsi
	movq	(%r15), %rdi
	movslq	59(%rsi), %rdx
	movl	47(%rdi), %r8d
	andl	$32, %r8d
	je	.L611
.L614:
	orl	$17, %edx
.L612:
	salq	$32, %rdx
	movq	%rdx, 55(%rsi)
	movq	(%r14), %rdi
	movq	(%rax), %rdx
	movq	%rdx, 47(%rdi)
	leaq	47(%rdi), %rsi
	testb	$1, %dl
	je	.L618
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -64(%rbp)
	testl	$262144, %eax
	jne	.L629
	testb	$24, %al
	je	.L618
.L632:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L630
	.p2align 4,,10
	.p2align 3
.L618:
	movq	136(%rbx), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal18SharedFunctionInfo30EnsureSourcePositionsAvailableEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	-56(%rbp), %rax
	subl	$1, 41104(%r12)
	movq	%rax, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L631
	movq	%r13, 41096(%r12)
	addq	$56, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	.p2align 4,,10
	.p2align 3
.L631:
	.cfi_restore_state
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L629:
	.cfi_restore_state
	movq	%rdx, -88(%rbp)
	movq	%rsi, -80(%rbp)
	movq	%rdi, -72(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-64(%rbp), %rcx
	movq	-88(%rbp), %rdx
	movq	-80(%rbp), %rsi
	movq	-72(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L632
	jmp	.L618
	.p2align 4,,10
	.p2align 3
.L611:
	movq	7(%rdi), %rsi
	testb	$1, %sil
	jne	.L633
.L613:
	movq	(%r14), %rsi
	orl	$1, %edx
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L630:
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L618
	.p2align 4,,10
	.p2align 3
.L633:
	movq	-1(%rsi), %rsi
	cmpw	$88, 11(%rsi)
	jne	.L613
	movq	(%r14), %rsi
	jmp	.L614
	.cfi_endproc
.LFE22987:
	.size	_ZN2v88internal5Debug15CreateBreakInfoENS0_6HandleINS0_18SharedFunctionInfoEEE, .-_ZN2v88internal5Debug15CreateBreakInfoENS0_6HandleINS0_18SharedFunctionInfoEEE
	.section	.text._ZN2v88internal5Debug30FindSharedFunctionInfoInScriptENS0_6HandleINS0_6ScriptEEEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug30FindSharedFunctionInfoInScriptENS0_6HandleINS0_6ScriptEEEi
	.type	_ZN2v88internal5Debug30FindSharedFunctionInfoInScriptENS0_6HandleINS0_6ScriptEEEi, @function
_ZN2v88internal5Debug30FindSharedFunctionInfoInScriptENS0_6HandleINS0_6ScriptEEEi:
.LFB22985:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-136(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-112(%rbp), %rbx
	subq	$248, %rsp
	movq	%rdi, -240(%rbp)
	movq	%rsi, -248(%rbp)
	movl	%edx, -252(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -256(%rbp)
	.p2align 4,,10
	.p2align 3
.L684:
	movl	-252(%rbp), %eax
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdi
	movq	$0, -128(%rbp)
	movb	$0, -120(%rbp)
	movl	%eax, -76(%rbp)
	movq	-248(%rbp), %rax
	movl	$-1, -80(%rbp)
	movq	(%rax), %rdx
	movq	-240(%rbp), %rax
	movaps	%xmm0, -96(%rbp)
	movq	136(%rax), %rsi
	call	_ZN2v88internal18SharedFunctionInfo14ScriptIteratorC1EPNS0_7IsolateENS0_6ScriptE@PLT
	.p2align 4,,10
	.p2align 3
.L706:
	movq	%rbx, %rdi
	call	_ZN2v88internal18SharedFunctionInfo14ScriptIterator4NextEv@PLT
	testq	%rax, %rax
	je	.L709
.L635:
	movq	%rax, -144(%rbp)
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L710
.L638:
	movq	%r12, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal6Script16IsUserJavaScriptEv@PLT
	testb	%al, %al
	je	.L706
	movq	-144(%rbp), %rax
	movq	7(%rax), %rax
	testb	$1, %al
	jne	.L642
.L646:
	movq	-144(%rbp), %rax
	leaq	-144(%rbp), %r15
	movzwl	45(%rax), %r14d
	cmpl	$65535, %r14d
	je	.L643
	movq	%r15, %rdi
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	subl	%r14d, %eax
	movl	%eax, %r13d
	cmpl	$-1, %eax
	je	.L643
.L647:
	movl	-76(%rbp), %r14d
	cmpl	%r13d, %r14d
	jl	.L706
	movq	%r15, %rdi
	call	_ZNK2v88internal18SharedFunctionInfo11EndPositionEv@PLT
	cmpl	%eax, %r14d
	jg	.L706
	cmpq	$0, -96(%rbp)
	je	.L652
	movl	-80(%rbp), %eax
	cmpl	%eax, %r13d
	je	.L711
.L649:
	cmpl	%eax, %r13d
	jl	.L706
	leaq	-96(%rbp), %rdi
	call	_ZNK2v88internal18SharedFunctionInfo11EndPositionEv@PLT
	movq	%r15, %rdi
	movl	%eax, %r14d
	call	_ZNK2v88internal18SharedFunctionInfo11EndPositionEv@PLT
	cmpl	%eax, %r14d
	jl	.L706
.L652:
	movq	-144(%rbp), %rax
	movq	%rbx, %rdi
	movl	%r13d, -80(%rbp)
	movq	$0, -88(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal18SharedFunctionInfo14ScriptIterator4NextEv@PLT
	testq	%rax, %rax
	jne	.L635
	.p2align 4,,10
	.p2align 3
.L709:
	movq	-96(%rbp), %r9
	testq	%r9, %r9
	je	.L682
	movq	%r9, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	7(%r9), %rdx
	testb	$1, %dl
	jne	.L654
.L657:
	movq	7(%r9), %rdx
	testb	$1, %dl
	jne	.L712
.L655:
	xorl	%eax, %eax
.L666:
	movabsq	$287762808832, %rcx
	movq	7(%r9), %rdx
	cmpq	%rcx, %rdx
	je	.L667
	testb	$1, %dl
	jne	.L713
	movb	$1, -232(%rbp)
	movq	-232(%rbp), %rsi
	movq	%rax, -208(%rbp)
	movq	%rsi, -200(%rbp)
	movq	%rax, -128(%rbp)
	movb	%sil, -120(%rbp)
.L672:
	movq	-240(%rbp), %rax
	movq	136(%rax), %r12
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L673
	movq	%r9, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rbx
.L674:
	cmpl	$1, -256(%rbp)
	jle	.L676
	movq	-240(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal5Debug15CreateBreakInfoENS0_6HandleINS0_18SharedFunctionInfoEEE
.L676:
	movq	%rbx, %rax
	jmp	.L677
	.p2align 4,,10
	.p2align 3
.L710:
	movq	-1(%rax), %rdx
	cmpw	$86, 11(%rdx)
	je	.L714
.L639:
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	%rax, -37504(%rdx)
	je	.L706
	jmp	.L638
	.p2align 4,,10
	.p2align 3
.L643:
	movq	%r15, %rdi
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	movl	%eax, %r13d
	jmp	.L647
	.p2align 4,,10
	.p2align 3
.L712:
	movq	-1(%rdx), %rdx
	cmpw	$91, 11(%rdx)
	jne	.L655
	movq	31(%r9), %rdx
	testb	$1, %dl
	jne	.L715
.L658:
	movq	7(%r9), %rdx
	testb	$1, %dl
	jne	.L716
.L662:
	movq	7(%r9), %rdx
	movq	7(%rdx), %rsi
.L661:
	movq	3520(%rax), %rdi
	leaq	-37592(%rax), %r13
	testq	%rdi, %rdi
	je	.L663
	movq	%r9, -264(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-264(%rbp), %r9
	jmp	.L666
	.p2align 4,,10
	.p2align 3
.L714:
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L639
	jmp	.L638
	.p2align 4,,10
	.p2align 3
.L642:
	movq	-1(%rax), %rax
	cmpw	$83, 11(%rax)
	jne	.L646
	jmp	.L706
	.p2align 4,,10
	.p2align 3
.L667:
	movb	$0, -232(%rbp)
	movq	-232(%rbp), %rcx
	movq	%rax, -224(%rbp)
	movq	%rcx, -216(%rbp)
	movq	%rax, -128(%rbp)
	movb	%cl, -120(%rbp)
.L671:
	movq	-240(%rbp), %rax
	movq	136(%rax), %r13
	addl	$1, 41104(%r13)
	movq	136(%rax), %rdx
	movq	41088(%r13), %r14
	movq	41096(%r13), %rcx
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L678
	movq	%r9, %rsi
	movq	%rcx, -264(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdi
.L708:
	leaq	-128(%rbp), %rdx
	movl	$1, %esi
	call	_ZN2v88internal8Compiler7CompileENS0_6HandleINS0_18SharedFunctionInfoEEENS1_18ClearExceptionFlagEPNS0_15IsCompiledScopeE@PLT
	movq	-264(%rbp), %rcx
	testb	%al, %al
	je	.L703
	subl	$1, 41104(%r13)
	movq	%r14, 41088(%r13)
	cmpq	41096(%r13), %rcx
	je	.L683
	movq	%rcx, 41096(%r13)
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L683:
	addl	$1, -256(%rbp)
	jmp	.L684
	.p2align 4,,10
	.p2align 3
.L678:
	movq	41088(%rdx), %rdi
	cmpq	41096(%rdx), %rdi
	je	.L717
.L681:
	leaq	8(%rdi), %rax
	movq	%rcx, -264(%rbp)
	movq	%rax, 41088(%rdx)
	movq	%r9, (%rdi)
	jmp	.L708
	.p2align 4,,10
	.p2align 3
.L654:
	movq	-1(%rdx), %rdx
	cmpw	$72, 11(%rdx)
	jne	.L657
	movq	31(%r9), %rdx
	testb	$1, %dl
	je	.L658
.L715:
	movq	-1(%rdx), %rcx
	cmpw	$86, 11(%rcx)
	jne	.L658
	movq	39(%rdx), %rcx
	testb	$1, %cl
	je	.L658
	movq	-1(%rcx), %rcx
	cmpw	$72, 11(%rcx)
	jne	.L658
	movq	31(%rdx), %rsi
	jmp	.L661
	.p2align 4,,10
	.p2align 3
.L713:
	movq	-1(%rdx), %rcx
	cmpw	$165, 11(%rcx)
	je	.L669
	movq	-1(%rdx), %rdx
	cmpw	$166, 11(%rdx)
	je	.L718
	movb	$1, -232(%rbp)
	movq	-232(%rbp), %rsi
	movq	%rax, -176(%rbp)
	movq	%rsi, -168(%rbp)
	movq	%rax, -128(%rbp)
	movb	%sil, -120(%rbp)
	jmp	.L672
	.p2align 4,,10
	.p2align 3
.L711:
	movq	%r15, %rdi
	call	_ZNK2v88internal18SharedFunctionInfo11EndPositionEv@PLT
	leaq	-96(%rbp), %rdi
	movl	%eax, %r14d
	call	_ZNK2v88internal18SharedFunctionInfo11EndPositionEv@PLT
	cmpl	%eax, %r14d
	je	.L650
	movl	-80(%rbp), %eax
	jmp	.L649
	.p2align 4,,10
	.p2align 3
.L663:
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L719
.L665:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%rsi, (%rax)
	jmp	.L666
	.p2align 4,,10
	.p2align 3
.L669:
	movb	$0, -232(%rbp)
	movq	-232(%rbp), %rsi
	movq	%rax, -192(%rbp)
	movq	%rsi, -184(%rbp)
	movq	%rax, -128(%rbp)
	movb	%sil, -120(%rbp)
	jmp	.L671
	.p2align 4,,10
	.p2align 3
.L716:
	movq	-1(%rdx), %rdx
	cmpw	$72, 11(%rdx)
	jne	.L662
	movq	7(%r9), %rsi
	jmp	.L661
.L717:
	movq	%rdx, %rdi
	movq	%r9, -280(%rbp)
	movq	%rcx, -272(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-280(%rbp), %r9
	movq	-272(%rbp), %rcx
	movq	-264(%rbp), %rdx
	movq	%rax, %rdi
	jmp	.L681
.L703:
	subl	$1, 41104(%r13)
	movq	%r13, %r15
	movq	%r14, 41088(%r13)
	cmpq	41096(%r13), %rcx
	je	.L682
	movq	%rcx, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L682:
	movq	-240(%rbp), %rax
	movq	136(%rax), %rax
	addq	$88, %rax
.L677:
	movq	-56(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L720
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L650:
	.cfi_restore_state
	cmpq	$0, -88(%rbp)
	jne	.L706
	movq	-96(%rbp), %rax
	movl	47(%rax), %eax
	testl	$268435456, %eax
	jne	.L652
	movq	-144(%rbp), %rax
	movl	47(%rax), %eax
	testl	$268435456, %eax
	je	.L652
	jmp	.L706
.L718:
	movb	$0, -232(%rbp)
	movq	-232(%rbp), %rsi
	movq	%rax, -160(%rbp)
	movq	%rsi, -152(%rbp)
	movq	%rax, -128(%rbp)
	movb	%sil, -120(%rbp)
	jmp	.L671
.L719:
	movq	%r13, %rdi
	movq	%r9, -272(%rbp)
	movq	%rsi, -264(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-272(%rbp), %r9
	movq	-264(%rbp), %rsi
	jmp	.L665
.L673:
	movq	41088(%r12), %rbx
	cmpq	41096(%r12), %rbx
	je	.L721
.L675:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r12)
	movq	%r9, (%rbx)
	jmp	.L674
.L721:
	movq	%r12, %rdi
	movq	%r9, -232(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-232(%rbp), %r9
	movq	%rax, %rbx
	jmp	.L675
.L720:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22985:
	.size	_ZN2v88internal5Debug30FindSharedFunctionInfoInScriptENS0_6HandleINS0_6ScriptEEEi, .-_ZN2v88internal5Debug30FindSharedFunctionInfoInScriptENS0_6HandleINS0_6ScriptEEEi
	.section	.text._ZN2v88internal5Debug15EnsureBreakInfoENS0_6HandleINS0_18SharedFunctionInfoEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug15EnsureBreakInfoENS0_6HandleINS0_18SharedFunctionInfoEEE
	.type	_ZN2v88internal5Debug15EnsureBreakInfoENS0_6HandleINS0_18SharedFunctionInfoEEE, @function
_ZN2v88internal5Debug15EnsureBreakInfoENS0_6HandleINS0_18SharedFunctionInfoEEE:
.LFB22986:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r15
	movq	%rdi, %r14
	pushq	%r13
	movq	%r15, %rdi
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	%rax, -80(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo12HasBreakInfoEv@PLT
	movl	%eax, %r12d
	testb	%al, %al
	je	.L767
.L722:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L768
	addq	$56, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L767:
	.cfi_restore_state
	movq	0(%r13), %rbx
	movq	31(%rbx), %rax
	testb	$1, %al
	jne	.L769
.L724:
	movq	%r15, %rdi
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal6Script16IsUserJavaScriptEv@PLT
	testb	%al, %al
	jne	.L727
	movq	0(%r13), %rbx
.L726:
	movl	47(%rbx), %eax
	testb	$32, %al
	je	.L770
.L731:
	movq	%rbx, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	7(%rbx), %rdx
	testb	$1, %dl
	jne	.L734
.L737:
	movq	7(%rbx), %rdx
	testb	$1, %dl
	jne	.L771
.L735:
	xorl	%eax, %eax
.L746:
	movabsq	$287762808832, %rcx
	movq	7(%rbx), %rdx
	cmpq	%rcx, %rdx
	je	.L747
	testb	$1, %dl
	jne	.L772
	movq	%rax, -80(%rbp)
.L766:
	movb	$1, -72(%rbp)
.L752:
	movq	%r13, %rsi
	movq	%r14, %rdi
	movl	$1, %r12d
	call	_ZN2v88internal5Debug15CreateBreakInfoENS0_6HandleINS0_18SharedFunctionInfoEEE
	jmp	.L722
	.p2align 4,,10
	.p2align 3
.L770:
	movq	7(%rbx), %rax
	testb	$1, %al
	je	.L722
	movq	-1(%rax), %rax
	cmpw	$88, 11(%rax)
	jne	.L722
	jmp	.L764
	.p2align 4,,10
	.p2align 3
.L771:
	movq	-1(%rdx), %rdx
	cmpw	$91, 11(%rdx)
	jne	.L735
.L740:
	movq	31(%rbx), %rdx
	testb	$1, %dl
	jne	.L773
.L738:
	movq	7(%rbx), %rdx
	testb	$1, %dl
	jne	.L774
.L742:
	movq	7(%rbx), %rdx
	movq	7(%rdx), %rsi
.L741:
	movq	3520(%rax), %rdi
	leaq	-37592(%rax), %r12
	testq	%rdi, %rdi
	je	.L743
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	jmp	.L746
	.p2align 4,,10
	.p2align 3
.L772:
	movq	-1(%rdx), %rcx
	cmpw	$165, 11(%rcx)
	jne	.L775
.L747:
	movq	%rax, -80(%rbp)
.L765:
	movq	%r15, %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	movb	$0, -72(%rbp)
	call	_ZN2v88internal8Compiler7CompileENS0_6HandleINS0_18SharedFunctionInfoEEENS1_18ClearExceptionFlagEPNS0_15IsCompiledScopeE@PLT
	movl	%eax, %r12d
	testb	%al, %al
	jne	.L752
	jmp	.L722
	.p2align 4,,10
	.p2align 3
.L769:
	movq	-1(%rax), %rdx
	cmpw	$86, 11(%rdx)
	je	.L776
.L725:
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	%rax, -37504(%rdx)
	je	.L726
	jmp	.L724
	.p2align 4,,10
	.p2align 3
.L727:
	movq	7(%rbx), %rax
	testb	$1, %al
	jne	.L760
.L764:
	movq	0(%r13), %rbx
	jmp	.L731
	.p2align 4,,10
	.p2align 3
.L734:
	movq	-1(%rdx), %rdx
	cmpw	$72, 11(%rdx)
	jne	.L737
	jmp	.L740
	.p2align 4,,10
	.p2align 3
.L743:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L777
.L745:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L746
	.p2align 4,,10
	.p2align 3
.L776:
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L725
	jmp	.L724
	.p2align 4,,10
	.p2align 3
.L760:
	movq	-1(%rax), %rax
	movq	0(%r13), %rbx
	cmpw	$83, 11(%rax)
	jne	.L731
	jmp	.L726
	.p2align 4,,10
	.p2align 3
.L775:
	movq	-1(%rdx), %rdx
	cmpw	$166, 11(%rdx)
	movq	%rax, -80(%rbp)
	jne	.L766
	jmp	.L765
	.p2align 4,,10
	.p2align 3
.L773:
	movq	-1(%rdx), %rcx
	cmpw	$86, 11(%rcx)
	jne	.L738
	movq	39(%rdx), %rcx
	testb	$1, %cl
	je	.L738
	movq	-1(%rcx), %rcx
	cmpw	$72, 11(%rcx)
	jne	.L738
	movq	31(%rdx), %rsi
	jmp	.L741
	.p2align 4,,10
	.p2align 3
.L774:
	movq	-1(%rdx), %rdx
	cmpw	$72, 11(%rdx)
	jne	.L742
	movq	7(%rbx), %rsi
	jmp	.L741
	.p2align 4,,10
	.p2align 3
.L777:
	movq	%r12, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	jmp	.L745
.L768:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22986:
	.size	_ZN2v88internal5Debug15EnsureBreakInfoENS0_6HandleINS0_18SharedFunctionInfoEEE, .-_ZN2v88internal5Debug15EnsureBreakInfoENS0_6HandleINS0_18SharedFunctionInfoEEE
	.section	.text._ZN2v88internal5Debug19InstallCoverageInfoENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_12CoverageInfoEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug19InstallCoverageInfoENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_12CoverageInfoEEE
	.type	_ZN2v88internal5Debug19InstallCoverageInfoENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_12CoverageInfoEEE, @function
_ZN2v88internal5Debug19InstallCoverageInfoENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_12CoverageInfoEEE:
.LFB22989:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rax
	movq	31(%rax), %r14
	testb	$1, %r14b
	jne	.L779
.L781:
	movq	136(%r12), %rdi
	call	_ZN2v88internal7Factory12NewDebugInfoENS0_6HandleINS0_18SharedFunctionInfoEEE@PLT
	movl	$16, %edi
	movq	(%rax), %r15
	movq	%rax, %rbx
	call	_Znwm@PLT
	movq	%rax, %r14
	movq	136(%r12), %rax
	movq	%r15, %rsi
	movq	$0, 8(%r14)
	movq	41152(%rax), %rdi
	call	_ZN2v88internal13GlobalHandles6CreateENS0_6ObjectE@PLT
	movq	%rax, %xmm0
	movhps	24(%r12), %xmm0
	movq	%r14, 24(%r12)
	movups	%xmm0, (%r14)
	movq	(%rbx), %rsi
.L780:
	movslq	59(%rsi), %rax
	orl	$4, %eax
	salq	$32, %rax
	movq	%rax, 55(%rsi)
	movq	(%rbx), %r14
	movq	0(%r13), %r12
	movq	%r12, 63(%r14)
	leaq	63(%r14), %r13
	testb	$1, %r12b
	je	.L778
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L796
	testb	$24, %al
	je	.L778
.L798:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L797
.L778:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L796:
	.cfi_restore_state
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L798
	jmp	.L778
	.p2align 4,,10
	.p2align 3
.L779:
	movq	-1(%r14), %rax
	cmpw	$86, 11(%rax)
	jne	.L781
	movq	136(%rdi), %r12
	movq	%r14, %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L782
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rbx
	jmp	.L780
	.p2align 4,,10
	.p2align 3
.L782:
	movq	41088(%r12), %rbx
	cmpq	41096(%r12), %rbx
	je	.L799
.L784:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rbx)
	jmp	.L780
	.p2align 4,,10
	.p2align 3
.L797:
	addq	$24, %rsp
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L799:
	.cfi_restore_state
	movq	%r12, %rdi
	movq	%r14, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L784
	.cfi_endproc
.LFE22989:
	.size	_ZN2v88internal5Debug19InstallCoverageInfoENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_12CoverageInfoEEE, .-_ZN2v88internal5Debug19InstallCoverageInfoENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_12CoverageInfoEEE
	.section	.rodata._ZN2v88internal5Debug13FindDebugInfoENS0_6HandleINS0_9DebugInfoEEEPPNS0_17DebugInfoListNodeES7_.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal5Debug13FindDebugInfoENS0_6HandleINS0_9DebugInfoEEEPPNS0_17DebugInfoListNodeES7_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug13FindDebugInfoENS0_6HandleINS0_9DebugInfoEEEPPNS0_17DebugInfoListNodeES7_
	.type	_ZN2v88internal5Debug13FindDebugInfoENS0_6HandleINS0_9DebugInfoEEEPPNS0_17DebugInfoListNodeES7_, @function
_ZN2v88internal5Debug13FindDebugInfoENS0_6HandleINS0_9DebugInfoEEEPPNS0_17DebugInfoListNodeES7_:
.LFB22994:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	136(%rdi), %r8
	addl	$1, 41104(%r8)
	movq	41088(%r8), %r11
	movq	41096(%r8), %r10
	movq	$0, (%rdx)
	movq	24(%rdi), %rax
	movq	%rax, (%rcx)
	testq	%rax, %rax
	je	.L801
	testq	%rsi, %rsi
	sete	%r9b
.L805:
	movq	(%rax), %rdi
	cmpq	%rsi, %rdi
	je	.L802
	testq	%rdi, %rdi
	je	.L803
	testb	%r9b, %r9b
	jne	.L803
	movq	(%rsi), %rbx
	cmpq	%rbx, (%rdi)
	je	.L802
.L803:
	movq	%rax, (%rdx)
	movq	(%rcx), %rax
	movq	8(%rax), %rax
	movq	%rax, (%rcx)
	testq	%rax, %rax
	jne	.L805
.L801:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L802:
	subl	$1, 41104(%r8)
	movq	%r11, 41088(%r8)
	cmpq	41096(%r8), %r10
	je	.L800
	movq	%r10, 41096(%r8)
	addq	$8, %rsp
	movq	%r8, %rdi
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	.p2align 4,,10
	.p2align 3
.L800:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22994:
	.size	_ZN2v88internal5Debug13FindDebugInfoENS0_6HandleINS0_9DebugInfoEEEPPNS0_17DebugInfoListNodeES7_, .-_ZN2v88internal5Debug13FindDebugInfoENS0_6HandleINS0_9DebugInfoEEEPPNS0_17DebugInfoListNodeES7_
	.section	.text._ZN2v88internal5Debug21FreeDebugInfoListNodeEPNS0_17DebugInfoListNodeES3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug21FreeDebugInfoListNodeEPNS0_17DebugInfoListNodeES3_
	.type	_ZN2v88internal5Debug21FreeDebugInfoListNodeEPNS0_17DebugInfoListNodeES3_, @function
_ZN2v88internal5Debug21FreeDebugInfoListNodeEPNS0_17DebugInfoListNodeES3_:
.LFB22997:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	8(%rdx), %rax
	testq	%rsi, %rsi
	je	.L829
	movq	%rax, 8(%rsi)
.L813:
	movq	(%r12), %rax
	movq	(%rax), %rax
	movq	7(%rax), %r14
	movq	23(%rax), %r13
	movq	%r13, 31(%r14)
	leaq	31(%r14), %r15
	testb	$1, %r13b
	je	.L818
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L830
	testb	$24, %al
	je	.L818
.L832:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L831
	.p2align 4,,10
	.p2align 3
.L818:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L817
	call	_ZN2v88internal13GlobalHandles7DestroyEPm@PLT
.L817:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$16, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L830:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L832
	jmp	.L818
	.p2align 4,,10
	.p2align 3
.L829:
	movq	%rax, 24(%rdi)
	jmp	.L813
	.p2align 4,,10
	.p2align 3
.L831:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L818
	.cfi_endproc
.LFE22997:
	.size	_ZN2v88internal5Debug21FreeDebugInfoListNodeEPNS0_17DebugInfoListNodeES3_, .-_ZN2v88internal5Debug21FreeDebugInfoListNodeEPNS0_17DebugInfoListNodeES3_
	.section	.text._ZN2v88internal5Debug18ClearAllDebugInfosERKSt8functionIFvNS0_6HandleINS0_9DebugInfoEEEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug18ClearAllDebugInfosERKSt8functionIFvNS0_6HandleINS0_9DebugInfoEEEEE
	.type	_ZN2v88internal5Debug18ClearAllDebugInfosERKSt8functionIFvNS0_6HandleINS0_9DebugInfoEEEEE, @function
_ZN2v88internal5Debug18ClearAllDebugInfosERKSt8functionIFvNS0_6HandleINS0_9DebugInfoEEEEE:
.LFB22995:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -80(%rbp)
	movq	24(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.L833
	movq	$0, -72(%rbp)
	movq	%rsi, %r12
	leaq	-64(%rbp), %r13
	jmp	.L835
	.p2align 4,,10
	.p2align 3
.L839:
	movq	%r14, -72(%rbp)
	testq	%rbx, %rbx
	je	.L833
.L835:
	movq	%rbx, %r14
	cmpq	$0, 16(%r12)
	movq	8(%rbx), %rbx
	movq	(%r14), %r15
	movq	%r15, -64(%rbp)
	je	.L847
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	*24(%r12)
	movq	(%r15), %rax
	movq	%r13, %rdi
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal9DebugInfo7IsEmptyEv@PLT
	testb	%al, %al
	je	.L839
	movq	-72(%rbp), %rsi
	movq	-80(%rbp), %rdi
	movq	%r14, %rdx
	call	_ZN2v88internal5Debug21FreeDebugInfoListNodeEPNS0_17DebugInfoListNodeES3_
	testq	%rbx, %rbx
	jne	.L835
.L833:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L848
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L847:
	.cfi_restore_state
	call	_ZSt25__throw_bad_function_callv@PLT
.L848:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22995:
	.size	_ZN2v88internal5Debug18ClearAllDebugInfosERKSt8functionIFvNS0_6HandleINS0_9DebugInfoEEEEE, .-_ZN2v88internal5Debug18ClearAllDebugInfosERKSt8functionIFvNS0_6HandleINS0_9DebugInfoEEEEE
	.section	.text._ZN2v88internal5Debug27RemoveBreakInfoAndMaybeFreeENS0_6HandleINS0_9DebugInfoEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug27RemoveBreakInfoAndMaybeFreeENS0_6HandleINS0_9DebugInfoEEE
	.type	_ZN2v88internal5Debug27RemoveBreakInfoAndMaybeFreeENS0_6HandleINS0_9DebugInfoEEE, @function
_ZN2v88internal5Debug27RemoveBreakInfoAndMaybeFreeENS0_6HandleINS0_9DebugInfoEEE:
.LFB22996:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	136(%rdi), %rsi
	movq	%r13, %rdi
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal9DebugInfo14ClearBreakInfoEPNS0_7IsolateE@PLT
	movq	(%rbx), %rax
	movq	%r13, %rdi
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal9DebugInfo7IsEmptyEv@PLT
	testb	%al, %al
	jne	.L867
.L849:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L868
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L867:
	.cfi_restore_state
	movq	136(%r12), %rcx
	movl	41104(%rcx), %edi
	leal	1(%rdi), %eax
	movl	%eax, 41104(%rcx)
	movq	24(%r12), %rdx
	testq	%rdx, %rdx
	je	.L851
	movq	(%rdx), %rax
	xorl	%esi, %esi
	cmpq	%rax, %rbx
	je	.L852
.L869:
	testq	%rax, %rax
	je	.L853
	movq	(%rbx), %r8
	cmpq	%r8, (%rax)
	je	.L852
.L853:
	movq	8(%rdx), %rax
	movq	%rdx, %rsi
	testq	%rax, %rax
	je	.L851
	movq	%rax, %rdx
	movq	(%rdx), %rax
	cmpq	%rax, %rbx
	jne	.L869
.L852:
	movl	%edi, 41104(%rcx)
	movq	%r12, %rdi
	call	_ZN2v88internal5Debug21FreeDebugInfoListNodeEPNS0_17DebugInfoListNodeES3_
	jmp	.L849
.L851:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L868:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22996:
	.size	_ZN2v88internal5Debug27RemoveBreakInfoAndMaybeFreeENS0_6HandleINS0_9DebugInfoEEE, .-_ZN2v88internal5Debug27RemoveBreakInfoAndMaybeFreeENS0_6HandleINS0_9DebugInfoEEE
	.section	.text._ZN2v88internal5Debug15ClearBreakPointENS0_6HandleINS0_10BreakPointEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug15ClearBreakPointENS0_6HandleINS0_10BreakPointEEE
	.type	_ZN2v88internal5Debug15ClearBreakPointENS0_6HandleINS0_10BreakPointEEE, @function
_ZN2v88internal5Debug15ClearBreakPointENS0_6HandleINS0_10BreakPointEEE:
.LFB22871:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	136(%rdi), %rax
	movq	41088(%rax), %rcx
	movq	%rax, -72(%rbp)
	movq	%rcx, -88(%rbp)
	movq	41096(%rax), %rcx
	movq	%rcx, -80(%rbp)
	movq	%rax, %rcx
	movl	41104(%rax), %eax
	leal	1(%rax), %edx
	movl	%edx, 41104(%rcx)
	movq	24(%rdi), %r14
	testq	%r14, %r14
	je	.L871
	movq	%rdi, %r12
	movq	%rsi, %rbx
	leaq	-64(%rbp), %r13
	.p2align 4,,10
	.p2align 3
.L877:
	movq	(%r14), %rax
	movq	%r13, %rdi
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal9DebugInfo12HasBreakInfoEv@PLT
	testb	%al, %al
	je	.L873
	movq	136(%r12), %rdi
	movq	(%r14), %rsi
	movq	%rbx, %rdx
	call	_ZN2v88internal9DebugInfo18FindBreakPointInfoEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10BreakPointEEE@PLT
	movq	136(%r12), %rdi
	movq	(%rax), %rax
	cmpq	%rax, 88(%rdi)
	je	.L873
	movq	(%r14), %r15
	movq	%rbx, %rdx
	movq	%r15, %rsi
	call	_ZN2v88internal9DebugInfo15ClearBreakPointEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10BreakPointEEE@PLT
	testb	%al, %al
	jne	.L890
.L873:
	movq	8(%r14), %r14
	testq	%r14, %r14
	jne	.L877
	movq	-72(%rbp), %rax
	movq	-88(%rbp), %rbx
	movq	-80(%rbp), %rcx
	subl	$1, 41104(%rax)
	movq	%rbx, 41088(%rax)
	cmpq	41096(%rax), %rcx
	je	.L870
	movq	%rcx, 41096(%rax)
	movq	%rax, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L870:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L891
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L890:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal5Debug16ClearBreakPointsENS0_6HandleINS0_9DebugInfoEEE
	movq	(%r15), %rax
	movq	136(%r12), %rsi
	movq	%r13, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal9DebugInfo18GetBreakPointCountEPNS0_7IsolateE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	testl	%eax, %eax
	je	.L892
	call	_ZN2v88internal5Debug16ApplyBreakPointsENS0_6HandleINS0_9DebugInfoEEE
.L875:
	movq	-72(%rbp), %rax
	movq	-88(%rbp), %rdx
	subl	$1, 41104(%rax)
	movq	%rdx, 41088(%rax)
	movq	-80(%rbp), %rdx
	cmpq	41096(%rax), %rdx
	je	.L870
	movq	%rdx, 41096(%rax)
	movq	%rax, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	jmp	.L870
.L892:
	call	_ZN2v88internal5Debug27RemoveBreakInfoAndMaybeFreeENS0_6HandleINS0_9DebugInfoEEE
	jmp	.L875
.L871:
	movq	-72(%rbp), %rbx
	movl	%eax, 41104(%rbx)
	jmp	.L870
.L891:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22871:
	.size	_ZN2v88internal5Debug15ClearBreakPointENS0_6HandleINS0_10BreakPointEEE, .-_ZN2v88internal5Debug15ClearBreakPointENS0_6HandleINS0_10BreakPointEEE
	.section	.text._ZN2v88internal5Debug16RemoveBreakpointEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug16RemoveBreakpointEi
	.type	_ZN2v88internal5Debug16RemoveBreakpointEi, @function
_ZN2v88internal5Debug16RemoveBreakpointEi:
.LFB22874:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	136(%rdi), %rdi
	leaq	128(%rdi), %rdx
	call	_ZN2v88internal7Factory13NewBreakPointEiNS0_6HandleINS0_6StringEEE@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal5Debug15ClearBreakPointENS0_6HandleINS0_10BreakPointEEE
	.cfi_endproc
.LFE22874:
	.size	_ZN2v88internal5Debug16RemoveBreakpointEi, .-_ZN2v88internal5Debug16RemoveBreakpointEi
	.section	.text._ZN2v88internal5Debug6UnloadEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug6UnloadEv
	.type	_ZN2v88internal5Debug6UnloadEv, @function
_ZN2v88internal5Debug6UnloadEv:
.LFB22852:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNSt17_Function_handlerIFvN2v88internal6HandleINS1_9DebugInfoEEEEZNS1_5Debug19ClearAllBreakPointsEvEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_(%rip), %rdx
	movq	%rdx, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$168, %rsp
	movq	24(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal5Debug19ClearAllBreakPointsEvEUlNS2_6HandleINS2_9DebugInfoEEEE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation(%rip), %rax
	movq	%rdi, -96(%rbp)
	movq	%rax, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	testq	%r15, %r15
	je	.L919
	leaq	-184(%rbp), %rcx
	xorl	%r13d, %r13d
	movq	%rcx, -208(%rbp)
	jmp	.L897
	.p2align 4,,10
	.p2align 3
.L900:
	movq	-80(%rbp), %rax
	testq	%r12, %r12
	je	.L898
	movq	%r15, %r13
.L901:
	movq	%r12, %r15
.L897:
	movq	(%r15), %rdx
	movq	8(%r15), %r12
	movq	%rdx, -184(%rbp)
	testq	%rax, %rax
	je	.L908
	leaq	-96(%rbp), %r14
	movq	%rdx, -200(%rbp)
	movq	-208(%rbp), %rsi
	movq	%r14, %rdi
	call	*-72(%rbp)
	movq	-200(%rbp), %rdx
	leaq	-168(%rbp), %rdi
	movq	(%rdx), %rax
	movq	%rax, -168(%rbp)
	call	_ZNK2v88internal9DebugInfo7IsEmptyEv@PLT
	testb	%al, %al
	je	.L900
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal5Debug21FreeDebugInfoListNodeEPNS0_17DebugInfoListNodeES3_
	movq	-80(%rbp), %rax
	testq	%r12, %r12
	jne	.L901
.L898:
	testq	%rax, %rax
	je	.L902
.L896:
	movl	$3, %edx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	*%rax
.L902:
	movq	24(%rbx), %r12
	testq	%r12, %r12
	je	.L903
	.p2align 4,,10
	.p2align 3
.L904:
	movq	(%r12), %r13
	movq	%rbx, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal5Debug16ClearBreakPointsENS0_6HandleINS0_9DebugInfoEEE
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal5Debug16ApplyBreakPointsENS0_6HandleINS0_9DebugInfoEEE
	movq	8(%r12), %r12
	testq	%r12, %r12
	jne	.L904
	movq	24(%rbx), %r12
.L903:
	movq	136(%rbx), %rax
	movb	$-1, 76(%rbx)
	leaq	_ZNSt17_Function_handlerIFvN2v88internal6HandleINS1_9DebugInfoEEEEZNS1_5Debug22RemoveAllCoverageInfosEvEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_(%rip), %rdx
	movq	$0, 80(%rbx)
	movq	%rdx, %xmm2
	movb	$0, 88(%rbx)
	movl	$-1, 92(%rbx)
	movq	$-1, 96(%rbx)
	movb	$0, 132(%rbx)
	cmpl	$32, 41828(%rax)
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal5Debug22RemoveAllCoverageInfosEvEUlNS2_6HandleINS2_9DebugInfoEEEE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation(%rip), %rax
	movq	%rax, %xmm0
	movq	%rbx, -128(%rbp)
	punpcklqdq	%xmm2, %xmm0
	sete	9(%rbx)
	movaps	%xmm0, -112(%rbp)
	testq	%r12, %r12
	je	.L920
	leaq	-176(%rbp), %rcx
	xorl	%r14d, %r14d
	movq	%rcx, -208(%rbp)
	jmp	.L906
	.p2align 4,,10
	.p2align 3
.L909:
	movq	-112(%rbp), %rax
	testq	%r13, %r13
	je	.L907
	movq	%r12, %r14
.L910:
	movq	%r13, %r12
.L906:
	movq	(%r12), %rdx
	movq	8(%r12), %r13
	movq	%rdx, -176(%rbp)
	movq	%rdx, -200(%rbp)
	testq	%rax, %rax
	je	.L908
	leaq	-128(%rbp), %r15
	movq	-208(%rbp), %rsi
	movq	%r15, %rdi
	call	*-104(%rbp)
	movq	-200(%rbp), %rdx
	leaq	-168(%rbp), %rdi
	movq	(%rdx), %rax
	movq	%rax, -168(%rbp)
	call	_ZNK2v88internal9DebugInfo7IsEmptyEv@PLT
	testb	%al, %al
	je	.L909
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal5Debug21FreeDebugInfoListNodeEPNS0_17DebugInfoListNodeES3_
	movq	-112(%rbp), %rax
	testq	%r13, %r13
	jne	.L910
.L907:
	testq	%rax, %rax
	je	.L911
.L905:
	movl	$3, %edx
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	*%rax
.L911:
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal5Debug21ClearAllDebuggerHintsEvEUlNS2_6HandleINS2_9DebugInfoEEEE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation(%rip), %rax
	leaq	_ZNSt17_Function_handlerIFvN2v88internal6HandleINS1_9DebugInfoEEEEZNS1_5Debug21ClearAllDebuggerHintsEvEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_(%rip), %rdx
	movq	24(%rbx), %r15
	movq	%rax, %xmm0
	movq	%rdx, %xmm3
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -144(%rbp)
	testq	%r15, %r15
	je	.L921
	movq	$0, -200(%rbp)
	leaq	-168(%rbp), %r13
	jmp	.L913
	.p2align 4,,10
	.p2align 3
.L915:
	movq	-144(%rbp), %rax
	testq	%r12, %r12
	je	.L914
	movq	%r15, -200(%rbp)
.L916:
	movq	%r12, %r15
.L913:
	movq	(%r15), %rdx
	movq	8(%r15), %r12
	movq	%rdx, -208(%rbp)
	movq	%rdx, -168(%rbp)
	testq	%rax, %rax
	je	.L908
	leaq	-160(%rbp), %r14
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	*-136(%rbp)
	movq	-208(%rbp), %rdx
	movq	%r13, %rdi
	movq	(%rdx), %rax
	movq	%rax, -168(%rbp)
	call	_ZNK2v88internal9DebugInfo7IsEmptyEv@PLT
	testb	%al, %al
	je	.L915
	movq	-200(%rbp), %rsi
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal5Debug21FreeDebugInfoListNodeEPNS0_17DebugInfoListNodeES3_
	movq	-144(%rbp), %rax
	testq	%r12, %r12
	jne	.L916
.L914:
	testq	%rax, %rax
	je	.L917
.L912:
	movl	$3, %edx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	*%rax
.L917:
	movq	$0, (%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L951
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L919:
	.cfi_restore_state
	leaq	-96(%rbp), %r14
	jmp	.L896
	.p2align 4,,10
	.p2align 3
.L920:
	leaq	-128(%rbp), %r15
	jmp	.L905
	.p2align 4,,10
	.p2align 3
.L921:
	leaq	-160(%rbp), %r14
	jmp	.L912
	.p2align 4,,10
	.p2align 3
.L908:
	call	_ZSt25__throw_bad_function_callv@PLT
.L951:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22852:
	.size	_ZN2v88internal5Debug6UnloadEv, .-_ZN2v88internal5Debug6UnloadEv
	.section	.text._ZN2v88internal5Debug21ClearAllDebuggerHintsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug21ClearAllDebuggerHintsEv
	.type	_ZN2v88internal5Debug21ClearAllDebuggerHintsEv, @function
_ZN2v88internal5Debug21ClearAllDebuggerHintsEv:
.LFB22992:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNSt17_Function_handlerIFvN2v88internal6HandleINS1_9DebugInfoEEEEZNS1_5Debug21ClearAllDebuggerHintsEvEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_(%rip), %rdx
	movq	%rdx, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal5Debug21ClearAllDebuggerHintsEvEUlNS2_6HandleINS2_9DebugInfoEEEE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation(%rip), %rax
	movq	%rax, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	testq	%r15, %r15
	je	.L961
	movq	$0, -120(%rbp)
	movq	%rdi, %rbx
	leaq	-104(%rbp), %r13
	jmp	.L954
	.p2align 4,,10
	.p2align 3
.L957:
	movq	-80(%rbp), %rax
	testq	%r12, %r12
	je	.L955
	movq	%r15, -120(%rbp)
.L958:
	movq	%r12, %r15
.L954:
	movq	(%r15), %rdx
	movq	8(%r15), %r12
	movq	%rdx, -104(%rbp)
	testq	%rax, %rax
	je	.L969
	leaq	-96(%rbp), %r14
	movq	%rdx, -128(%rbp)
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	*-72(%rbp)
	movq	-128(%rbp), %rdx
	movq	%r13, %rdi
	movq	(%rdx), %rax
	movq	%rax, -104(%rbp)
	call	_ZNK2v88internal9DebugInfo7IsEmptyEv@PLT
	testb	%al, %al
	je	.L957
	movq	-120(%rbp), %rsi
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal5Debug21FreeDebugInfoListNodeEPNS0_17DebugInfoListNodeES3_
	movq	-80(%rbp), %rax
	testq	%r12, %r12
	jne	.L958
.L955:
	testq	%rax, %rax
	je	.L952
.L953:
	movl	$3, %edx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	*%rax
.L952:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L970
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L961:
	.cfi_restore_state
	leaq	-96(%rbp), %r14
	jmp	.L953
.L969:
	call	_ZSt25__throw_bad_function_callv@PLT
.L970:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22992:
	.size	_ZN2v88internal5Debug21ClearAllDebuggerHintsEv, .-_ZN2v88internal5Debug21ClearAllDebuggerHintsEv
	.section	.text._ZN2v88internal5Debug19ClearAllBreakPointsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug19ClearAllBreakPointsEv
	.type	_ZN2v88internal5Debug19ClearAllBreakPointsEv, @function
_ZN2v88internal5Debug19ClearAllBreakPointsEv:
.LFB22875:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNSt17_Function_handlerIFvN2v88internal6HandleINS1_9DebugInfoEEEEZNS1_5Debug19ClearAllBreakPointsEvEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_(%rip), %rdx
	movq	%rdx, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal5Debug19ClearAllBreakPointsEvEUlNS2_6HandleINS2_9DebugInfoEEEE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation(%rip), %rax
	movq	%rdi, -96(%rbp)
	movq	%rax, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	testq	%r12, %r12
	je	.L980
	movq	$0, -120(%rbp)
	movq	%rdi, %r15
	leaq	-104(%rbp), %r13
	jmp	.L973
	.p2align 4,,10
	.p2align 3
.L976:
	movq	-80(%rbp), %rax
	testq	%rbx, %rbx
	je	.L974
	movq	%r12, -120(%rbp)
.L977:
	movq	%rbx, %r12
.L973:
	movq	(%r12), %rdx
	movq	8(%r12), %rbx
	movq	%rdx, -104(%rbp)
	testq	%rax, %rax
	je	.L988
	leaq	-96(%rbp), %r14
	movq	%rdx, -128(%rbp)
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	*-72(%rbp)
	movq	-128(%rbp), %rdx
	movq	%r13, %rdi
	movq	(%rdx), %rax
	movq	%rax, -104(%rbp)
	call	_ZNK2v88internal9DebugInfo7IsEmptyEv@PLT
	testb	%al, %al
	je	.L976
	movq	-120(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal5Debug21FreeDebugInfoListNodeEPNS0_17DebugInfoListNodeES3_
	movq	-80(%rbp), %rax
	testq	%rbx, %rbx
	jne	.L977
.L974:
	testq	%rax, %rax
	je	.L971
.L972:
	movl	$3, %edx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	*%rax
.L971:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L989
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L980:
	.cfi_restore_state
	leaq	-96(%rbp), %r14
	jmp	.L972
.L988:
	call	_ZSt25__throw_bad_function_callv@PLT
.L989:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22875:
	.size	_ZN2v88internal5Debug19ClearAllBreakPointsEv, .-_ZN2v88internal5Debug19ClearAllBreakPointsEv
	.section	.text._ZN2v88internal5Debug22RemoveAllCoverageInfosEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug22RemoveAllCoverageInfosEv
	.type	_ZN2v88internal5Debug22RemoveAllCoverageInfosEv, @function
_ZN2v88internal5Debug22RemoveAllCoverageInfosEv:
.LFB22990:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNSt17_Function_handlerIFvN2v88internal6HandleINS1_9DebugInfoEEEEZNS1_5Debug22RemoveAllCoverageInfosEvEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_(%rip), %rdx
	movq	%rdx, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal5Debug22RemoveAllCoverageInfosEvEUlNS2_6HandleINS2_9DebugInfoEEEE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation(%rip), %rax
	movq	%rdi, -96(%rbp)
	movq	%rax, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	testq	%r12, %r12
	je	.L999
	movq	$0, -120(%rbp)
	movq	%rdi, %r15
	leaq	-104(%rbp), %r13
	jmp	.L992
	.p2align 4,,10
	.p2align 3
.L995:
	movq	-80(%rbp), %rax
	testq	%rbx, %rbx
	je	.L993
	movq	%r12, -120(%rbp)
.L996:
	movq	%rbx, %r12
.L992:
	movq	(%r12), %rdx
	movq	8(%r12), %rbx
	movq	%rdx, -104(%rbp)
	testq	%rax, %rax
	je	.L1007
	leaq	-96(%rbp), %r14
	movq	%rdx, -128(%rbp)
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	*-72(%rbp)
	movq	-128(%rbp), %rdx
	movq	%r13, %rdi
	movq	(%rdx), %rax
	movq	%rax, -104(%rbp)
	call	_ZNK2v88internal9DebugInfo7IsEmptyEv@PLT
	testb	%al, %al
	je	.L995
	movq	-120(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal5Debug21FreeDebugInfoListNodeEPNS0_17DebugInfoListNodeES3_
	movq	-80(%rbp), %rax
	testq	%rbx, %rbx
	jne	.L996
.L993:
	testq	%rax, %rax
	je	.L990
.L991:
	movl	$3, %edx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	*%rax
.L990:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1008
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L999:
	.cfi_restore_state
	leaq	-96(%rbp), %r14
	jmp	.L991
.L1007:
	call	_ZSt25__throw_bad_function_callv@PLT
.L1008:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22990:
	.size	_ZN2v88internal5Debug22RemoveAllCoverageInfosEv, .-_ZN2v88internal5Debug22RemoveAllCoverageInfosEv
	.section	.text._ZN2v88internal5Debug15IsBreakAtReturnEPNS0_15JavaScriptFrameE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug15IsBreakAtReturnEPNS0_15JavaScriptFrameE
	.type	_ZN2v88internal5Debug15IsBreakAtReturnEPNS0_15JavaScriptFrameE, @function
_ZN2v88internal5Debug15IsBreakAtReturnEPNS0_15JavaScriptFrameE:
.LFB22998:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	136(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	41088(%r12), %rax
	movq	41096(%r12), %r14
	addl	$1, 41104(%r12)
	movq	136(%rdi), %r15
	movq	%rsi, %rdi
	movq	%rax, -88(%rbp)
	movq	(%rsi), %rax
	call	*152(%rax)
	movq	23(%rax), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1010
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rdx
.L1011:
	leaq	-80(%rbp), %r15
	movq	%rdx, -96(%rbp)
	movq	%r15, %rdi
	movq	%rsi, -80(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo12HasBreakInfoEv@PLT
	testb	%al, %al
	je	.L1013
	movq	136(%rbx), %rbx
	movq	-96(%rbp), %rdx
	movq	41112(%rbx), %rdi
	movq	(%rdx), %rax
	movq	31(%rax), %r9
	testq	%rdi, %rdi
	je	.L1014
	movq	%r9, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1015:
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal13BreakLocation9FromFrameENS0_6HandleINS0_9DebugInfoEEEPNS0_15JavaScriptFrameE
	cmpl	$4, -68(%rbp)
	sete	%al
.L1013:
	subl	$1, 41104(%r12)
	movq	-88(%rbp), %rcx
	movq	%rcx, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L1009
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	movb	%al, -88(%rbp)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movzbl	-88(%rbp), %eax
.L1009:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1024
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1014:
	.cfi_restore_state
	movq	41088(%rbx), %rsi
	cmpq	41096(%rbx), %rsi
	je	.L1025
.L1016:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rbx)
	movq	%r9, (%rsi)
	jmp	.L1015
	.p2align 4,,10
	.p2align 3
.L1010:
	movq	41088(%r15), %rdx
	cmpq	41096(%r15), %rdx
	je	.L1026
.L1012:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rdx)
	jmp	.L1011
	.p2align 4,,10
	.p2align 3
.L1026:
	movq	%r15, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L1012
	.p2align 4,,10
	.p2align 3
.L1025:
	movq	%rbx, %rdi
	movq	%r9, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %r9
	movq	%rax, %rsi
	jmp	.L1016
.L1024:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22998:
	.size	_ZN2v88internal5Debug15IsBreakAtReturnEPNS0_15JavaScriptFrameE, .-_ZN2v88internal5Debug15IsBreakAtReturnEPNS0_15JavaScriptFrameE
	.section	.text._ZN2v88internal5Debug20ScheduleFrameRestartEPNS0_10StackFrameE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug20ScheduleFrameRestartEPNS0_10StackFrameE
	.type	_ZN2v88internal5Debug20ScheduleFrameRestartEPNS0_10StackFrameE, @function
_ZN2v88internal5Debug20ScheduleFrameRestartEPNS0_10StackFrameE:
.LFB22999:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$2896, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	32(%rsi), %rax
	cmpq	%rax, 120(%rdi)
	jb	.L1039
.L1027:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1040
	addq	$2896, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1039:
	.cfi_restore_state
	movq	%rax, 120(%rdi)
	movq	136(%rdi), %rsi
	movq	%rdi, %rbx
	leaq	-2912(%rbp), %rdi
	leaq	-1472(%rbp), %r12
	call	_ZN2v88internal23StackTraceFrameIteratorC1EPNS0_7IsolateE@PLT
	movl	$0, 72(%rbx)
	movq	136(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal23StackTraceFrameIteratorC1EPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1031
	jmp	.L1027
	.p2align 4,,10
	.p2align 3
.L1030:
	movq	%r12, %rdi
	call	_ZN2v88internal23StackTraceFrameIterator7AdvanceEv@PLT
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1027
.L1031:
	movq	32(%rdi), %rax
	cmpq	%rax, 120(%rbx)
	jnb	.L1030
	movq	(%rdi), %rax
	call	*56(%rax)
	movl	%eax, 72(%rbx)
	jmp	.L1027
.L1040:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22999:
	.size	_ZN2v88internal5Debug20ScheduleFrameRestartEPNS0_10StackFrameE, .-_ZN2v88internal5Debug20ScheduleFrameRestartEPNS0_10StackFrameE
	.section	.text._ZN2v88internal5Debug16GetLoadedScriptsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug16GetLoadedScriptsEv
	.type	_ZN2v88internal5Debug16GetLoadedScriptsEv, @function
_ZN2v88internal5Debug16GetLoadedScriptsEv:
.LFB23000:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	movl	$5, %edx
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	136(%rdi), %rax
	leaq	37592(%rax), %rdi
	call	_ZN2v88internal4Heap17CollectAllGarbageEiNS0_23GarbageCollectionReasonENS_15GCCallbackFlagsE@PLT
	movq	136(%rbx), %rdi
	movq	4680(%rdi), %rax
	movq	-1(%rax), %rdx
	cmpw	$167, 11(%rdx)
	je	.L1042
	leaq	288(%rdi), %rax
.L1043:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1068
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1042:
	.cfi_restore_state
	movslq	19(%rax), %rsi
	xorl	%edx, %edx
	leaq	-80(%rbp), %r15
	xorl	%r13d, %r13d
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	136(%rbx), %rsi
	movq	%r15, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal6Script8IteratorC1EPNS0_7IsolateE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal6Script8Iterator4NextEv@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1044
	.p2align 4,,10
	.p2align 3
.L1054:
	movq	7(%r12), %rax
	testb	$1, %al
	jne	.L1045
.L1048:
	movq	(%r14), %rdi
	leal	16(,%r13,8), %eax
	cltq
	leaq	-1(%rdi,%rax), %rsi
	movq	%r12, (%rsi)
	testb	$1, %r12b
	je	.L1055
	movq	%r12, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -88(%rbp)
	testl	$262144, %eax
	jne	.L1069
.L1052:
	testb	$24, %al
	je	.L1055
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1070
	.p2align 4,,10
	.p2align 3
.L1055:
	addl	$1, %r13d
.L1051:
	movq	%r15, %rdi
	call	_ZN2v88internal6Script8Iterator4NextEv@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L1054
.L1044:
	movq	136(%rbx), %rdi
	movl	%r13d, %edx
	movq	%r14, %rsi
	call	_ZN2v88internal10FixedArray13ShrinkOrEmptyEPNS0_7IsolateENS0_6HandleIS1_EEi@PLT
	jmp	.L1043
	.p2align 4,,10
	.p2align 3
.L1045:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L1048
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$2, %dx
	jne	.L1048
	movq	-1(%rax), %rdx
	testb	$8, 11(%rdx)
	je	.L1049
.L1067:
	cmpq	$0, 15(%rax)
	setne	%al
	testb	%al, %al
	je	.L1051
	jmp	.L1048
	.p2align 4,,10
	.p2align 3
.L1069:
	movq	%r12, %rdx
	movq	%rsi, -104(%rbp)
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rcx
	movq	-104(%rbp), %rsi
	movq	-96(%rbp), %rdi
	movq	8(%rcx), %rax
	jmp	.L1052
	.p2align 4,,10
	.p2align 3
.L1070:
	movq	%r12, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1055
	.p2align 4,,10
	.p2align 3
.L1049:
	movq	-1(%rax), %rdx
	testb	$8, 11(%rdx)
	jne	.L1048
	jmp	.L1067
.L1068:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23000:
	.size	_ZN2v88internal5Debug16GetLoadedScriptsEv, .-_ZN2v88internal5Debug16GetLoadedScriptsEv
	.section	.text._ZN2v88internal5Debug17IsFrameBlackboxedEPNS0_15JavaScriptFrameE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug17IsFrameBlackboxedEPNS0_15JavaScriptFrameE
	.type	_ZN2v88internal5Debug17IsFrameBlackboxedEPNS0_15JavaScriptFrameE, @function
_ZN2v88internal5Debug17IsFrameBlackboxedEPNS0_15JavaScriptFrameE:
.LFB23004:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%rsi, %rdi
	leaq	-144(%rbp), %rsi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	136(%r14), %rax
	movaps	%xmm0, -144(%rbp)
	movq	$0, -128(%rbp)
	movq	41088(%rax), %rcx
	addl	$1, 41104(%rax)
	movq	%rax, -224(%rbp)
	movq	%rcx, -240(%rbp)
	movq	41096(%rax), %rcx
	movq	%rcx, -232(%rbp)
	call	_ZNK2v88internal15JavaScriptFrame12GetFunctionsEPSt6vectorINS0_6HandleINS0_18SharedFunctionInfoEEESaIS5_EE@PLT
	movq	-136(%rbp), %rcx
	movq	-144(%rbp), %rax
	movq	%rcx, -200(%rbp)
	cmpq	%rcx, %rax
	je	.L1105
	movq	%rax, %r15
	jmp	.L1098
	.p2align 4,,10
	.p2align 3
.L1073:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal5Debug20GetOrCreateDebugInfoENS0_6HandleINS0_18SharedFunctionInfoEEE
	movq	(%rax), %rdx
	movq	%rax, %r12
	movslq	19(%rdx), %rbx
	movl	%ebx, %eax
	andl	$8, %ebx
	je	.L1125
.L1081:
	shrl	$2, %eax
	andl	$1, %eax
.L1080:
	testb	%al, %al
	je	.L1079
.L1076:
	addq	$8, %r15
	cmpq	%r15, -200(%rbp)
	je	.L1126
.L1098:
	cmpq	$0, (%r14)
	movq	(%r15), %r13
	jne	.L1073
	movq	0(%r13), %rbx
	movq	31(%rbx), %rax
	testb	$1, %al
	jne	.L1127
.L1074:
	leaq	-112(%rbp), %rdi
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal6Script16IsUserJavaScriptEv@PLT
	testb	%al, %al
	je	.L1076
	movq	7(%rbx), %rax
	testb	$1, %al
	jne	.L1128
.L1079:
	movq	-144(%rbp), %rax
	xorl	%r12d, %r12d
	movq	%rax, -200(%rbp)
.L1072:
	testq	%rax, %rax
	je	.L1099
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L1099:
	movq	-224(%rbp), %rax
	movq	-240(%rbp), %rsi
	movq	-232(%rbp), %rcx
	subl	$1, 41104(%rax)
	movq	%rsi, 41088(%rax)
	cmpq	41096(%rax), %rcx
	je	.L1071
	movq	%rcx, 41096(%rax)
	movq	%rax, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1071:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1129
	addq	$248, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1125:
	.cfi_restore_state
	movq	0(%r13), %rcx
	movq	31(%rcx), %rax
	testb	$1, %al
	jne	.L1130
.L1082:
	leaq	-112(%rbp), %rdi
	movq	%rcx, -216(%rbp)
	movq	%rdi, -208(%rbp)
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal6Script16IsUserJavaScriptEv@PLT
	movq	-208(%rbp), %rdi
	movq	-216(%rbp), %rcx
	testb	%al, %al
	jne	.L1116
.L1122:
	movq	(%r12), %rdx
.L1084:
	movl	$4, %ebx
.L1093:
	movslq	19(%rdx), %rax
	andl	$-5, %eax
	orl	%eax, %ebx
	salq	$32, %rbx
	movq	%rbx, 15(%rdx)
	movq	(%r12), %rdx
	movslq	19(%rdx), %rax
	orl	$8, %eax
	salq	$32, %rax
	movq	%rax, 15(%rdx)
	movq	(%r12), %rax
	movl	19(%rax), %eax
	jmp	.L1081
	.p2align 4,,10
	.p2align 3
.L1127:
	movq	-1(%rax), %rdx
	cmpw	$86, 11(%rdx)
	je	.L1131
.L1075:
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	%rax, -37504(%rdx)
	jne	.L1074
	addq	$8, %r15
	cmpq	%r15, -200(%rbp)
	jne	.L1098
	.p2align 4,,10
	.p2align 3
.L1126:
	movq	-144(%rbp), %rax
	movl	$1, %r12d
	movq	%rax, -200(%rbp)
	jmp	.L1072
	.p2align 4,,10
	.p2align 3
.L1116:
	movq	7(%rcx), %rax
	testb	$1, %al
	jne	.L1086
	movq	0(%r13), %rax
	movq	31(%rax), %rax
	testb	$1, %al
	je	.L1122
.L1117:
	movq	-1(%rax), %rcx
	leaq	-1(%rax), %rdx
	cmpw	$86, 11(%rcx)
	je	.L1132
.L1089:
	movq	(%rdx), %rax
	cmpw	$96, 11(%rax)
	jne	.L1122
	movq	136(%r14), %r8
	movzbl	10(%r14), %eax
	movb	$1, 10(%r14)
	xorl	%ecx, %ecx
	movl	$255, %edx
	addl	$1, 41104(%r8)
	movq	136(%r14), %rsi
	movb	%al, -249(%rbp)
	movq	41088(%r8), %rax
	movq	%r8, -208(%rbp)
	movq	%rax, -288(%rbp)
	movq	41096(%r8), %rax
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal15InterruptsScopeC2EPNS0_7IsolateElNS1_4ModeE@PLT
	leaq	16+_ZTVN2v88internal23PostponeInterruptsScopeE(%rip), %rax
	movq	136(%r14), %rdx
	movq	-208(%rbp), %r8
	movq	%rax, -112(%rbp)
	movzbl	12(%r14), %eax
	movb	$1, 12(%r14)
	movb	%al, -250(%rbp)
	movq	0(%r13), %rax
	movq	31(%rax), %rsi
	testb	$1, %sil
	jne	.L1133
.L1090:
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L1134
	movq	%r8, -248(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-248(%rbp), %r8
	movq	%rax, -208(%rbp)
.L1094:
	leaq	-192(%rbp), %r11
	movq	0(%r13), %rax
	movq	%r8, -280(%rbp)
	movq	%r11, %rdi
	movq	%r11, -272(%rbp)
	movq	%rax, -192(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	movq	-208(%rbp), %rdi
	pcmpeqd	%xmm1, %xmm1
	movl	$1, %ecx
	movl	%eax, %esi
	leaq	-160(%rbp), %rax
	movaps	%xmm1, -160(%rbp)
	movq	%rax, %rdx
	movq	%rax, -248(%rbp)
	call	_ZN2v88internal6Script15GetPositionInfoENS0_6HandleIS1_EEiPNS1_12PositionInfoENS1_10OffsetFlagE@PLT
	movl	-156(%rbp), %eax
	xorl	%r9d, %r9d
	leaq	-172(%rbp), %r10
	movl	%r9d, %ecx
	movl	%r9d, %esi
	movq	%r10, %rdi
	movq	%r10, -264(%rbp)
	testl	%eax, %eax
	cmovns	-156(%rbp), %ecx
	movl	%ecx, %edx
	movl	-160(%rbp), %ecx
	testl	%ecx, %ecx
	cmovns	-160(%rbp), %esi
	call	_ZN2v85debug8LocationC1Eii@PLT
	movq	-172(%rbp), %rax
	movq	-272(%rbp), %r11
	movq	%rax, -184(%rbp)
	movzbl	-164(%rbp), %eax
	movq	%r11, %rdi
	movb	%al, -176(%rbp)
	movq	0(%r13), %rax
	movq	%rax, -192(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo11EndPositionEv@PLT
	movq	-248(%rbp), %rdx
	pcmpeqd	%xmm1, %xmm1
	movq	-208(%rbp), %rdi
	movl	$1, %ecx
	movl	%eax, %esi
	movaps	%xmm1, -160(%rbp)
	call	_ZN2v88internal6Script15GetPositionInfoENS0_6HandleIS1_EEiPNS1_12PositionInfoENS1_10OffsetFlagE@PLT
	movl	-156(%rbp), %esi
	movl	-160(%rbp), %edi
	movl	$0, %r9d
	movq	-264(%rbp), %r10
	movl	%r9d, %edx
	testl	%esi, %esi
	movl	%r9d, %esi
	cmovns	-156(%rbp), %edx
	testl	%edi, %edi
	cmovns	-160(%rbp), %esi
	movq	%r10, %rdi
	call	_ZN2v85debug8LocationC1Eii@PLT
	movq	-172(%rbp), %rax
	movq	(%r14), %rdi
	leaq	_ZN2v85debug13DebugDelegate20IsFunctionBlackboxedENS_5LocalINS0_6ScriptEEERKNS0_8LocationES7_(%rip), %rcx
	movq	-280(%rbp), %r8
	movq	%rax, -160(%rbp)
	movzbl	-164(%rbp), %eax
	movb	%al, -152(%rbp)
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1135
.L1096:
	movzbl	-250(%rbp), %eax
	cmpl	$2, -80(%rbp)
	movb	%al, 12(%r14)
	leaq	16+_ZTVN2v88internal15InterruptsScopeE(%rip), %rax
	movq	%rax, -112(%rbp)
	je	.L1097
	movq	-104(%rbp), %rdi
	movq	%r8, -208(%rbp)
	call	_ZN2v88internal10StackGuard18PopInterruptsScopeEv@PLT
	movq	-208(%rbp), %r8
.L1097:
	movq	-288(%rbp), %rax
	subl	$1, 41104(%r8)
	movq	%rax, 41088(%r8)
	movq	-216(%rbp), %rax
	cmpq	41096(%r8), %rax
	je	.L1103
	movq	%rax, 41096(%r8)
	movq	%r8, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1103:
	movzbl	-249(%rbp), %eax
	movb	%al, 10(%r14)
	movq	(%r12), %rdx
	jmp	.L1093
	.p2align 4,,10
	.p2align 3
.L1130:
	movq	-1(%rax), %rsi
	cmpw	$86, 11(%rsi)
	je	.L1136
.L1083:
	movq	%rax, %rsi
	andq	$-262144, %rsi
	movq	24(%rsi), %rsi
	cmpq	%rax, -37504(%rsi)
	je	.L1084
	jmp	.L1082
	.p2align 4,,10
	.p2align 3
.L1136:
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L1083
	jmp	.L1082
	.p2align 4,,10
	.p2align 3
.L1086:
	movq	-1(%rax), %rax
	cmpw	$83, 11(%rax)
	je	.L1122
	movq	0(%r13), %rax
	movq	31(%rax), %rax
	testb	$1, %al
	je	.L1122
	jmp	.L1117
	.p2align 4,,10
	.p2align 3
.L1128:
	movq	-1(%rax), %rax
	cmpw	$83, 11(%rax)
	sete	%al
	jmp	.L1080
	.p2align 4,,10
	.p2align 3
.L1131:
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L1075
	jmp	.L1074
	.p2align 4,,10
	.p2align 3
.L1132:
	movq	23(%rax), %rdx
	testb	$1, %dl
	je	.L1122
	subq	$1, %rdx
	jmp	.L1089
.L1105:
	movl	$1, %r12d
	movq	%rcx, %rax
	jmp	.L1072
.L1134:
	movq	41088(%rdx), %rax
	movq	%rax, -208(%rbp)
	cmpq	41096(%rdx), %rax
	je	.L1137
.L1095:
	movq	-208(%rbp), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%rcx)
	jmp	.L1094
.L1133:
	movq	-1(%rsi), %rax
	cmpw	$86, 11(%rax)
	jne	.L1090
	movq	23(%rsi), %rsi
	jmp	.L1090
.L1135:
	movq	%r8, -264(%rbp)
	movq	-248(%rbp), %rcx
	leaq	-184(%rbp), %rdx
	movq	-208(%rbp), %rsi
	call	*%rax
	movq	-264(%rbp), %r8
	movzbl	%al, %ebx
	sall	$2, %ebx
	jmp	.L1096
.L1137:
	movq	%rdx, %rdi
	movq	%r8, -272(%rbp)
	movq	%rsi, -264(%rbp)
	movq	%rdx, -248(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-272(%rbp), %r8
	movq	-264(%rbp), %rsi
	movq	%rax, -208(%rbp)
	movq	-248(%rbp), %rdx
	jmp	.L1095
.L1129:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23004:
	.size	_ZN2v88internal5Debug17IsFrameBlackboxedEPNS0_15JavaScriptFrameE, .-_ZN2v88internal5Debug17IsFrameBlackboxedEPNS0_15JavaScriptFrameE
	.section	.text._ZN2v88internal5Debug12IsBlackboxedENS0_6HandleINS0_18SharedFunctionInfoEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug12IsBlackboxedENS0_6HandleINS0_18SharedFunctionInfoEEE
	.type	_ZN2v88internal5Debug12IsBlackboxedENS0_6HandleINS0_18SharedFunctionInfoEEE, @function
_ZN2v88internal5Debug12IsBlackboxedENS0_6HandleINS0_18SharedFunctionInfoEEE:
.LFB23018:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, (%rdi)
	je	.L1180
	movq	%rdi, %rbx
	call	_ZN2v88internal5Debug20GetOrCreateDebugInfoENS0_6HandleINS0_18SharedFunctionInfoEEE
	movq	(%rax), %rdx
	movq	%rax, %r14
	movslq	19(%rdx), %r13
	movl	%r13d, %eax
	andl	$8, %r13d
	je	.L1181
.L1144:
	shrl	$2, %eax
	andl	$1, %eax
.L1138:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1182
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1181:
	.cfi_restore_state
	movq	(%r12), %r15
	movq	31(%r15), %rax
	testb	$1, %al
	jne	.L1183
.L1145:
	leaq	-112(%rbp), %rdi
	movq	%rax, -112(%rbp)
	movq	%rdi, -168(%rbp)
	call	_ZN2v88internal6Script16IsUserJavaScriptEv@PLT
	movq	-168(%rbp), %rdi
	testb	%al, %al
	jne	.L1173
.L1179:
	movq	(%r14), %rdx
.L1147:
	movl	$4, %r13d
.L1156:
	movslq	19(%rdx), %rax
	andl	$-5, %eax
	orl	%eax, %r13d
	salq	$32, %r13
	movq	%r13, 15(%rdx)
	movq	(%r14), %rdx
	movslq	19(%rdx), %rax
	orl	$8, %eax
	salq	$32, %rax
	movq	%rax, 15(%rdx)
	movq	(%r14), %rax
	movl	19(%rax), %eax
	jmp	.L1144
	.p2align 4,,10
	.p2align 3
.L1180:
	movq	(%rsi), %rbx
	movq	31(%rbx), %rax
	testb	$1, %al
	jne	.L1184
.L1140:
	leaq	-112(%rbp), %rdi
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal6Script16IsUserJavaScriptEv@PLT
	movl	%eax, %r8d
	movl	$1, %eax
	testb	%r8b, %r8b
	je	.L1138
	movq	7(%rbx), %rdx
	xorl	%eax, %eax
	testb	$1, %dl
	je	.L1138
	movq	-1(%rdx), %rax
	cmpw	$83, 11(%rax)
	sete	%al
	jmp	.L1138
	.p2align 4,,10
	.p2align 3
.L1183:
	movq	-1(%rax), %rcx
	cmpw	$86, 11(%rcx)
	je	.L1185
.L1146:
	movq	%rax, %rcx
	andq	$-262144, %rcx
	movq	24(%rcx), %rcx
	cmpq	-37504(%rcx), %rax
	je	.L1147
	jmp	.L1145
	.p2align 4,,10
	.p2align 3
.L1184:
	movq	-1(%rax), %rdx
	cmpw	$86, 11(%rdx)
	je	.L1186
.L1141:
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	%rax, -37504(%rdx)
	jne	.L1140
	movl	$1, %eax
	jmp	.L1138
	.p2align 4,,10
	.p2align 3
.L1173:
	movq	7(%r15), %rax
	testb	$1, %al
	jne	.L1149
.L1151:
	movq	(%r12), %rax
	movq	31(%rax), %rax
	testb	$1, %al
	je	.L1179
	movq	-1(%rax), %rcx
	leaq	-1(%rax), %rdx
	cmpw	$86, 11(%rcx)
	je	.L1187
.L1152:
	movq	(%rdx), %rax
	cmpw	$96, 11(%rax)
	jne	.L1179
	movzbl	10(%rbx), %eax
	movq	136(%rbx), %r15
	movb	$1, 10(%rbx)
	xorl	%ecx, %ecx
	movl	$255, %edx
	addl	$1, 41104(%r15)
	movq	136(%rbx), %rsi
	movb	%al, -178(%rbp)
	movq	41088(%r15), %rax
	movq	%rax, -216(%rbp)
	movq	41096(%r15), %rax
	movq	%rax, -176(%rbp)
	call	_ZN2v88internal15InterruptsScopeC2EPNS0_7IsolateElNS1_4ModeE@PLT
	leaq	16+_ZTVN2v88internal23PostponeInterruptsScopeE(%rip), %rax
	movq	136(%rbx), %rdx
	movq	%rax, -112(%rbp)
	movzbl	12(%rbx), %eax
	movb	$1, 12(%rbx)
	movb	%al, -177(%rbp)
	movq	(%r12), %rax
	movq	31(%rax), %rsi
	testb	$1, %sil
	jne	.L1188
.L1153:
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L1189
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -168(%rbp)
.L1157:
	leaq	-160(%rbp), %r11
	movq	(%r12), %rax
	movq	%r11, %rdi
	movq	%r11, -208(%rbp)
	movq	%rax, -160(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	leaq	-128(%rbp), %r9
	pcmpeqd	%xmm0, %xmm0
	movq	-168(%rbp), %rdi
	movq	%r9, %rdx
	movl	%eax, %esi
	movl	$1, %ecx
	movaps	%xmm0, -128(%rbp)
	movq	%r9, -192(%rbp)
	call	_ZN2v88internal6Script15GetPositionInfoENS0_6HandleIS1_EEiPNS1_12PositionInfoENS1_10OffsetFlagE@PLT
	movl	-128(%rbp), %ecx
	movl	-124(%rbp), %eax
	xorl	%r8d, %r8d
	leaq	-140(%rbp), %r10
	movl	%r8d, %edx
	movl	%r8d, %esi
	testl	%eax, %eax
	cmovns	-124(%rbp), %edx
	testl	%ecx, %ecx
	cmovns	-128(%rbp), %esi
	movq	%r10, %rdi
	movq	%r10, -200(%rbp)
	call	_ZN2v85debug8LocationC1Eii@PLT
	movq	-140(%rbp), %rax
	movq	-208(%rbp), %r11
	movq	%rax, -152(%rbp)
	movzbl	-132(%rbp), %eax
	movq	%r11, %rdi
	movb	%al, -144(%rbp)
	movq	(%r12), %rax
	movq	%rax, -160(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo11EndPositionEv@PLT
	movq	-192(%rbp), %r9
	pcmpeqd	%xmm0, %xmm0
	movq	-168(%rbp), %rdi
	movl	%eax, %esi
	movl	$1, %ecx
	movaps	%xmm0, -128(%rbp)
	movq	%r9, %rdx
	call	_ZN2v88internal6Script15GetPositionInfoENS0_6HandleIS1_EEiPNS1_12PositionInfoENS1_10OffsetFlagE@PLT
	movl	-124(%rbp), %esi
	movl	-128(%rbp), %edi
	movl	$0, %r8d
	movq	-200(%rbp), %r10
	movl	%r8d, %edx
	testl	%esi, %esi
	movl	%r8d, %esi
	cmovns	-124(%rbp), %edx
	testl	%edi, %edi
	cmovns	-128(%rbp), %esi
	movq	%r10, %rdi
	call	_ZN2v85debug8LocationC1Eii@PLT
	movq	-140(%rbp), %rax
	movq	(%rbx), %rdi
	leaq	_ZN2v85debug13DebugDelegate20IsFunctionBlackboxedENS_5LocalINS0_6ScriptEEERKNS0_8LocationES7_(%rip), %rdx
	movq	-192(%rbp), %r9
	movq	%rax, -128(%rbp)
	movzbl	-132(%rbp), %eax
	movb	%al, -120(%rbp)
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1190
.L1159:
	movzbl	-177(%rbp), %eax
	cmpl	$2, -80(%rbp)
	movb	%al, 12(%rbx)
	leaq	16+_ZTVN2v88internal15InterruptsScopeE(%rip), %rax
	movq	%rax, -112(%rbp)
	je	.L1160
	movq	-104(%rbp), %rdi
	call	_ZN2v88internal10StackGuard18PopInterruptsScopeEv@PLT
.L1160:
	movq	-216(%rbp), %rax
	subl	$1, 41104(%r15)
	movq	%rax, 41088(%r15)
	movq	-176(%rbp), %rax
	cmpq	41096(%r15), %rax
	je	.L1162
	movq	%rax, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1162:
	movzbl	-178(%rbp), %eax
	movb	%al, 10(%rbx)
	movq	(%r14), %rdx
	jmp	.L1156
	.p2align 4,,10
	.p2align 3
.L1186:
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L1141
	jmp	.L1140
	.p2align 4,,10
	.p2align 3
.L1185:
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L1146
	jmp	.L1145
	.p2align 4,,10
	.p2align 3
.L1149:
	movq	-1(%rax), %rax
	cmpw	$83, 11(%rax)
	jne	.L1151
	jmp	.L1179
.L1187:
	movq	23(%rax), %rdx
	testb	$1, %dl
	je	.L1179
	subq	$1, %rdx
	jmp	.L1152
.L1189:
	movq	41088(%rdx), %rax
	movq	%rax, -168(%rbp)
	cmpq	41096(%rdx), %rax
	je	.L1191
.L1158:
	movq	-168(%rbp), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%rcx)
	jmp	.L1157
.L1188:
	movq	-1(%rsi), %rax
	cmpw	$86, 11(%rax)
	jne	.L1153
	movq	23(%rsi), %rsi
	jmp	.L1153
.L1190:
	movq	-168(%rbp), %rsi
	leaq	-152(%rbp), %rdx
	movq	%r9, %rcx
	call	*%rax
	movzbl	%al, %r13d
	sall	$2, %r13d
	jmp	.L1159
.L1191:
	movq	%rdx, %rdi
	movq	%rsi, -200(%rbp)
	movq	%rdx, -192(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-200(%rbp), %rsi
	movq	-192(%rbp), %rdx
	movq	%rax, -168(%rbp)
	jmp	.L1158
.L1182:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23018:
	.size	_ZN2v88internal5Debug12IsBlackboxedENS0_6HandleINS0_18SharedFunctionInfoEEE, .-_ZN2v88internal5Debug12IsBlackboxedENS0_6HandleINS0_18SharedFunctionInfoEEE
	.section	.text._ZN2v88internal5Debug29AllFramesOnStackAreBlackboxedEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug29AllFramesOnStackAreBlackboxedEv
	.type	_ZN2v88internal5Debug29AllFramesOnStackAreBlackboxedEv, @function
_ZN2v88internal5Debug29AllFramesOnStackAreBlackboxedEv:
.LFB23019:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$1736, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	136(%rdi), %rax
	movq	41088(%rax), %rcx
	addl	$1, 41104(%rax)
	movq	%rax, -1712(%rbp)
	movq	136(%rdi), %rsi
	movq	%rcx, -1728(%rbp)
	movq	41096(%rax), %rcx
	leaq	-1504(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -1680(%rbp)
	movq	%rcx, -1720(%rbp)
	call	_ZN2v88internal23StackTraceFrameIteratorC1EPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1224
	movq	%r15, -1704(%rbp)
	.p2align 4,,10
	.p2align 3
.L1193:
	movq	(%rdi), %rax
	call	*8(%rax)
	movl	%eax, %ecx
	cmpl	$20, %eax
	ja	.L1195
	movl	$1150992, %eax
	shrq	%cl, %rax
	notq	%rax
	andl	$1, %eax
	movb	%al, -1657(%rbp)
	jne	.L1195
	movq	-1704(%rbp), %r13
	movq	-88(%rbp), %rdi
	pxor	%xmm0, %xmm0
	leaq	-1584(%rbp), %rsi
	movq	$0, -1568(%rbp)
	movq	136(%r13), %r14
	movaps	%xmm0, -1584(%rbp)
	movq	41088(%r14), %rax
	addl	$1, 41104(%r14)
	movq	%rax, -1696(%rbp)
	movq	41096(%r14), %rax
	movq	%rax, -1672(%rbp)
	call	_ZNK2v88internal15JavaScriptFrame12GetFunctionsEPSt6vectorINS0_6HandleINS0_18SharedFunctionInfoEEESaIS5_EE@PLT
	movq	-1576(%rbp), %rsi
	movq	-1584(%rbp), %rax
	movq	%rsi, -1640(%rbp)
	cmpq	%rsi, %rax
	je	.L1232
	movq	%r14, -1688(%rbp)
	movq	%rax, %r15
	jmp	.L1222
	.p2align 4,,10
	.p2align 3
.L1197:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal5Debug20GetOrCreateDebugInfoENS0_6HandleINS0_18SharedFunctionInfoEEE
	movq	(%rax), %rdx
	movq	%rax, %r12
	movslq	19(%rdx), %rbx
	movl	%ebx, %eax
	andl	$8, %ebx
	je	.L1254
.L1205:
	shrl	$2, %eax
	andl	$1, %eax
.L1204:
	testb	%al, %al
	je	.L1203
.L1200:
	addq	$8, %r15
	cmpq	%r15, -1640(%rbp)
	je	.L1255
.L1222:
	cmpq	$0, 0(%r13)
	movq	(%r15), %r14
	jne	.L1197
	movq	(%r14), %rbx
	movq	31(%rbx), %rax
	testb	$1, %al
	jne	.L1256
.L1198:
	leaq	-1552(%rbp), %rdi
	movq	%rax, -1552(%rbp)
	call	_ZN2v88internal6Script16IsUserJavaScriptEv@PLT
	testb	%al, %al
	je	.L1200
	movq	7(%rbx), %rax
	testb	$1, %al
	jne	.L1257
.L1203:
	movq	-1584(%rbp), %rax
	movq	-1688(%rbp), %r14
	movq	%rax, -1640(%rbp)
.L1196:
	testq	%rax, %rax
	je	.L1223
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L1223:
	movq	-1696(%rbp), %rax
	subl	$1, 41104(%r14)
	movq	%rax, 41088(%r14)
	movq	-1672(%rbp), %rax
	cmpq	41096(%r14), %rax
	je	.L1228
	movq	%rax, 41096(%r14)
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1228:
	cmpb	$0, -1657(%rbp)
	je	.L1194
.L1195:
	movq	-1680(%rbp), %rdi
	call	_ZN2v88internal23StackTraceFrameIterator7AdvanceEv@PLT
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1193
.L1224:
	movb	$1, -1657(%rbp)
.L1194:
	movq	-1712(%rbp), %rax
	movq	-1728(%rbp), %rcx
	subl	$1, 41104(%rax)
	movq	%rcx, 41088(%rax)
	movq	-1720(%rbp), %rcx
	cmpq	41096(%rax), %rcx
	je	.L1192
	movq	%rcx, 41096(%rax)
	movq	%rax, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1192:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1258
	movzbl	-1657(%rbp), %eax
	addq	$1736, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1254:
	.cfi_restore_state
	movq	(%r14), %rcx
	movq	31(%rcx), %rax
	testb	$1, %al
	jne	.L1259
.L1206:
	leaq	-1552(%rbp), %rdi
	movq	%rcx, -1656(%rbp)
	movq	%rdi, -1648(%rbp)
	movq	%rax, -1552(%rbp)
	call	_ZN2v88internal6Script16IsUserJavaScriptEv@PLT
	movq	-1648(%rbp), %rdi
	movq	-1656(%rbp), %rcx
	testb	%al, %al
	jne	.L1245
.L1252:
	movq	(%r12), %rdx
.L1208:
	movl	$4, %ebx
.L1217:
	movslq	19(%rdx), %rax
	andl	$-5, %eax
	orl	%eax, %ebx
	salq	$32, %rbx
	movq	%rbx, 15(%rdx)
	movq	(%r12), %rdx
	movslq	19(%rdx), %rax
	orl	$8, %eax
	salq	$32, %rax
	movq	%rax, 15(%rdx)
	movq	(%r12), %rax
	movl	19(%rax), %eax
	jmp	.L1205
	.p2align 4,,10
	.p2align 3
.L1256:
	movq	-1(%rax), %rdx
	cmpw	$86, 11(%rdx)
	je	.L1260
.L1199:
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	%rax, -37504(%rdx)
	jne	.L1198
	addq	$8, %r15
	cmpq	%r15, -1640(%rbp)
	jne	.L1222
	.p2align 4,,10
	.p2align 3
.L1255:
	movq	-1584(%rbp), %rax
	movb	$1, -1657(%rbp)
	movq	-1688(%rbp), %r14
	movq	%rax, -1640(%rbp)
	jmp	.L1196
	.p2align 4,,10
	.p2align 3
.L1245:
	movq	7(%rcx), %rax
	testb	$1, %al
	jne	.L1210
.L1212:
	movq	(%r14), %rax
	movq	31(%rax), %rax
	testb	$1, %al
	je	.L1252
	movq	-1(%rax), %rcx
	leaq	-1(%rax), %rdx
	cmpw	$86, 11(%rcx)
	je	.L1261
.L1213:
	movq	(%rdx), %rax
	cmpw	$96, 11(%rax)
	jne	.L1252
	movq	136(%r13), %r10
	movzbl	10(%r13), %eax
	movb	$1, 10(%r13)
	xorl	%ecx, %ecx
	movl	$255, %edx
	addl	$1, 41104(%r10)
	movq	136(%r13), %rsi
	movb	%al, -1658(%rbp)
	movq	41088(%r10), %rax
	movq	%r10, -1648(%rbp)
	movq	%rax, -1768(%rbp)
	movq	41096(%r10), %rax
	movq	%rax, -1656(%rbp)
	call	_ZN2v88internal15InterruptsScopeC2EPNS0_7IsolateElNS1_4ModeE@PLT
	leaq	16+_ZTVN2v88internal23PostponeInterruptsScopeE(%rip), %rax
	movq	136(%r13), %rdx
	movq	-1648(%rbp), %r10
	movq	%rax, -1552(%rbp)
	movzbl	12(%r13), %eax
	movb	$1, 12(%r13)
	movb	%al, -1659(%rbp)
	movq	(%r14), %rax
	movq	31(%rax), %rsi
	testb	$1, %sil
	jne	.L1262
.L1214:
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L1263
	movq	%r10, -1736(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-1736(%rbp), %r10
	movq	%rax, -1648(%rbp)
.L1218:
	leaq	-1632(%rbp), %r11
	movq	(%r14), %rax
	movq	%r10, -1760(%rbp)
	movq	%r11, %rdi
	movq	%r11, -1752(%rbp)
	movq	%rax, -1632(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	movq	-1648(%rbp), %rdi
	pcmpeqd	%xmm1, %xmm1
	movl	$1, %ecx
	movl	%eax, %esi
	leaq	-1600(%rbp), %rax
	movaps	%xmm1, -1600(%rbp)
	movq	%rax, %rdx
	movq	%rax, -1736(%rbp)
	call	_ZN2v88internal6Script15GetPositionInfoENS0_6HandleIS1_EEiPNS1_12PositionInfoENS1_10OffsetFlagE@PLT
	movl	-1596(%rbp), %eax
	xorl	%r9d, %r9d
	leaq	-1612(%rbp), %r8
	movl	%r9d, %ecx
	movl	%r9d, %esi
	movq	%r8, %rdi
	movq	%r8, -1744(%rbp)
	testl	%eax, %eax
	cmovns	-1596(%rbp), %ecx
	movl	%ecx, %edx
	movl	-1600(%rbp), %ecx
	testl	%ecx, %ecx
	cmovns	-1600(%rbp), %esi
	call	_ZN2v85debug8LocationC1Eii@PLT
	movq	-1612(%rbp), %rax
	movq	-1752(%rbp), %r11
	movq	%rax, -1624(%rbp)
	movzbl	-1604(%rbp), %eax
	movq	%r11, %rdi
	movb	%al, -1616(%rbp)
	movq	(%r14), %rax
	movq	%rax, -1632(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo11EndPositionEv@PLT
	movq	-1736(%rbp), %rdx
	pcmpeqd	%xmm1, %xmm1
	movq	-1648(%rbp), %rdi
	movl	$1, %ecx
	movl	%eax, %esi
	movaps	%xmm1, -1600(%rbp)
	call	_ZN2v88internal6Script15GetPositionInfoENS0_6HandleIS1_EEiPNS1_12PositionInfoENS1_10OffsetFlagE@PLT
	movl	-1596(%rbp), %esi
	movl	-1600(%rbp), %edi
	movl	$0, %r9d
	movq	-1744(%rbp), %r8
	movl	%r9d, %edx
	testl	%esi, %esi
	movl	%r9d, %esi
	cmovns	-1596(%rbp), %edx
	testl	%edi, %edi
	cmovns	-1600(%rbp), %esi
	movq	%r8, %rdi
	call	_ZN2v85debug8LocationC1Eii@PLT
	movq	-1612(%rbp), %rax
	movq	0(%r13), %rdi
	leaq	_ZN2v85debug13DebugDelegate20IsFunctionBlackboxedENS_5LocalINS0_6ScriptEEERKNS0_8LocationES7_(%rip), %rcx
	movq	-1760(%rbp), %r10
	movq	%rax, -1600(%rbp)
	movzbl	-1604(%rbp), %eax
	movb	%al, -1592(%rbp)
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1264
.L1220:
	movzbl	-1659(%rbp), %eax
	cmpl	$2, -1520(%rbp)
	movb	%al, 12(%r13)
	leaq	16+_ZTVN2v88internal15InterruptsScopeE(%rip), %rax
	movq	%rax, -1552(%rbp)
	je	.L1221
	movq	-1544(%rbp), %rdi
	movq	%r10, -1648(%rbp)
	call	_ZN2v88internal10StackGuard18PopInterruptsScopeEv@PLT
	movq	-1648(%rbp), %r10
.L1221:
	movq	-1768(%rbp), %rax
	subl	$1, 41104(%r10)
	movq	%rax, 41088(%r10)
	movq	-1656(%rbp), %rax
	cmpq	41096(%r10), %rax
	je	.L1230
	movq	%rax, 41096(%r10)
	movq	%r10, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1230:
	movzbl	-1658(%rbp), %eax
	movb	%al, 10(%r13)
	movq	(%r12), %rdx
	jmp	.L1217
	.p2align 4,,10
	.p2align 3
.L1259:
	movq	-1(%rax), %rsi
	cmpw	$86, 11(%rsi)
	je	.L1265
.L1207:
	movq	%rax, %rsi
	andq	$-262144, %rsi
	movq	24(%rsi), %rsi
	cmpq	%rax, -37504(%rsi)
	je	.L1208
	jmp	.L1206
	.p2align 4,,10
	.p2align 3
.L1257:
	movq	-1(%rax), %rax
	cmpw	$83, 11(%rax)
	sete	%al
	jmp	.L1204
	.p2align 4,,10
	.p2align 3
.L1260:
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L1199
	jmp	.L1198
	.p2align 4,,10
	.p2align 3
.L1265:
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L1207
	jmp	.L1206
	.p2align 4,,10
	.p2align 3
.L1210:
	movq	-1(%rax), %rax
	cmpw	$83, 11(%rax)
	jne	.L1212
	jmp	.L1252
	.p2align 4,,10
	.p2align 3
.L1261:
	movq	23(%rax), %rdx
	testb	$1, %dl
	je	.L1252
	subq	$1, %rdx
	jmp	.L1213
	.p2align 4,,10
	.p2align 3
.L1232:
	movb	$1, -1657(%rbp)
	movq	%rsi, %rax
	jmp	.L1196
	.p2align 4,,10
	.p2align 3
.L1263:
	movq	41088(%rdx), %rax
	movq	%rax, -1648(%rbp)
	cmpq	41096(%rdx), %rax
	je	.L1266
.L1219:
	movq	-1648(%rbp), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%rcx)
	jmp	.L1218
	.p2align 4,,10
	.p2align 3
.L1262:
	movq	-1(%rsi), %rax
	cmpw	$86, 11(%rax)
	jne	.L1214
	movq	23(%rsi), %rsi
	jmp	.L1214
.L1264:
	movq	%r10, -1744(%rbp)
	movq	-1736(%rbp), %rcx
	leaq	-1624(%rbp), %rdx
	movq	-1648(%rbp), %rsi
	call	*%rax
	movq	-1744(%rbp), %r10
	movzbl	%al, %ebx
	sall	$2, %ebx
	jmp	.L1220
.L1266:
	movq	%rdx, %rdi
	movq	%r10, -1752(%rbp)
	movq	%rsi, -1744(%rbp)
	movq	%rdx, -1736(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-1752(%rbp), %r10
	movq	-1744(%rbp), %rsi
	movq	%rax, -1648(%rbp)
	movq	-1736(%rbp), %rdx
	jmp	.L1219
.L1258:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23019:
	.size	_ZN2v88internal5Debug29AllFramesOnStackAreBlackboxedEv, .-_ZN2v88internal5Debug29AllFramesOnStackAreBlackboxedEv
	.section	.text._ZN2v88internal5Debug21IsExceptionBlackboxedEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug21IsExceptionBlackboxedEb
	.type	_ZN2v88internal5Debug21IsExceptionBlackboxedEb, @function
_ZN2v88internal5Debug21IsExceptionBlackboxedEb:
.LFB23003:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	leaq	-1488(%rbp), %rbx
	subq	$1464, %rsp
	movq	136(%rdi), %rsi
	movq	%rbx, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal23StackTraceFrameIteratorC1EPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1271
	jmp	.L1272
	.p2align 4,,10
	.p2align 3
.L1276:
	movq	%rbx, %rdi
	call	_ZN2v88internal23StackTraceFrameIterator7AdvanceEv@PLT
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1272
.L1271:
	movq	(%rdi), %rax
	call	*8(%rax)
	cmpl	$5, %eax
	je	.L1276
	cmpl	$8, %eax
	je	.L1276
	movq	-72(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L1272
	movq	%r12, %rdi
	call	_ZN2v88internal5Debug17IsFrameBlackboxedEPNS0_15JavaScriptFrameE
	testb	%al, %al
	jne	.L1272
.L1267:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L1285
	addq	$1464, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1272:
	.cfi_restore_state
	testb	%r13b, %r13b
	je	.L1286
	movq	%r12, %rdi
	call	_ZN2v88internal5Debug29AllFramesOnStackAreBlackboxedEv
	jmp	.L1267
	.p2align 4,,10
	.p2align 3
.L1286:
	movl	$1, %eax
	jmp	.L1267
.L1285:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23003:
	.size	_ZN2v88internal5Debug21IsExceptionBlackboxedEb, .-_ZN2v88internal5Debug21IsExceptionBlackboxedEb
	.section	.text._ZN2v88internal5Debug15CanBreakAtEntryENS0_6HandleINS0_18SharedFunctionInfoEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug15CanBreakAtEntryENS0_6HandleINS0_18SharedFunctionInfoEEE
	.type	_ZN2v88internal5Debug15CanBreakAtEntryENS0_6HandleINS0_18SharedFunctionInfoEEE, @function
_ZN2v88internal5Debug15CanBreakAtEntryENS0_6HandleINS0_18SharedFunctionInfoEEE:
.LFB23020:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	movl	47(%rax), %edx
	andl	$32, %edx
	je	.L1288
.L1291:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1288:
	movq	7(%rax), %rax
	testb	$1, %al
	jne	.L1292
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1292:
	movq	-1(%rax), %rax
	cmpw	$88, 11(%rax)
	je	.L1291
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE23020:
	.size	_ZN2v88internal5Debug15CanBreakAtEntryENS0_6HandleINS0_18SharedFunctionInfoEEE, .-_ZN2v88internal5Debug15CanBreakAtEntryENS0_6HandleINS0_18SharedFunctionInfoEEE
	.section	.text._ZN2v88internal5Debug17CurrentFrameCountEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug17CurrentFrameCountEv
	.type	_ZN2v88internal5Debug17CurrentFrameCountEv, @function
_ZN2v88internal5Debug17CurrentFrameCountEv:
.LFB23025:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	leaq	-1472(%rbp), %rbx
	subq	$1488, %rsp
	movq	136(%rdi), %rsi
	movq	%rbx, %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal23StackTraceFrameIteratorC1EPNS0_7IsolateE@PLT
	movl	72(%r12), %eax
	testl	%eax, %eax
	je	.L1298
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1299
	.p2align 4,,10
	.p2align 3
.L1296:
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L1293:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1315
	addq	$1488, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1316:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal23StackTraceFrameIterator7AdvanceEv@PLT
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1296
.L1299:
	movq	(%rdi), %rax
	call	*56(%rax)
	cmpl	%eax, 72(%r12)
	jne	.L1316
.L1298:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1296
	xorl	%r12d, %r12d
	jmp	.L1295
	.p2align 4,,10
	.p2align 3
.L1300:
	addl	$1, %r12d
.L1302:
	movq	%rbx, %rdi
	call	_ZN2v88internal23StackTraceFrameIterator7AdvanceEv@PLT
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1293
.L1295:
	movq	(%rdi), %rax
	call	*8(%rax)
	cmpl	$4, %eax
	jne	.L1300
	movq	-56(%rbp), %rdi
	pxor	%xmm0, %xmm0
	movq	$0, -1488(%rbp)
	leaq	-1504(%rbp), %rsi
	movaps	%xmm0, -1504(%rbp)
	movq	(%rdi), %rax
	call	*160(%rax)
	movq	-1504(%rbp), %rdi
	movq	-1496(%rbp), %rax
	subq	%rdi, %rax
	sarq	$3, %rax
	addl	%eax, %r12d
	testq	%rdi, %rdi
	je	.L1302
	call	_ZdlPv@PLT
	jmp	.L1302
.L1315:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23025:
	.size	_ZN2v88internal5Debug17CurrentFrameCountEv, .-_ZN2v88internal5Debug17CurrentFrameCountEv
	.section	.text._ZN2v88internal5Debug16SetDebugDelegateEPNS_5debug13DebugDelegateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug16SetDebugDelegateEPNS_5debug13DebugDelegateE
	.type	_ZN2v88internal5Debug16SetDebugDelegateEPNS_5debug13DebugDelegateE, @function
_ZN2v88internal5Debug16SetDebugDelegateEPNS_5debug13DebugDelegateE:
.LFB23026:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	movq	%rsi, (%rdi)
	setne	%al
	cmpb	8(%rdi), %al
	je	.L1317
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	136(%rdi), %rax
	movq	%rdi, %rbx
	movq	40952(%rax), %rdi
	testq	%rsi, %rsi
	je	.L1319
	call	_ZN2v88internal16CompilationCache7DisableEv@PLT
	movl	$1, %r12d
	testb	$2, 56(%rbx)
	je	.L1325
.L1320:
	movb	%r12b, 8(%rbx)
	movq	136(%rbx), %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal7Isolate23PromiseHookStateUpdatedEv@PLT
	.p2align 4,,10
	.p2align 3
.L1325:
	.cfi_restore_state
	movq	48(%rbx), %rax
	movl	$1, %esi
	movq	40960(%rax), %rdi
	addq	$248, %rdi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	orl	$2, 56(%rbx)
	jmp	.L1320
	.p2align 4,,10
	.p2align 3
.L1319:
	call	_ZN2v88internal16CompilationCache6EnableEv@PLT
	movq	%rbx, %rdi
	xorl	%r12d, %r12d
	call	_ZN2v88internal5Debug6UnloadEv
	jmp	.L1320
	.p2align 4,,10
	.p2align 3
.L1317:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE23026:
	.size	_ZN2v88internal5Debug16SetDebugDelegateEPNS_5debug13DebugDelegateE, .-_ZN2v88internal5Debug16SetDebugDelegateEPNS_5debug13DebugDelegateE
	.section	.text._ZN2v88internal5Debug11UpdateStateEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug11UpdateStateEv
	.type	_ZN2v88internal5Debug11UpdateStateEv, @function
_ZN2v88internal5Debug11UpdateStateEv:
.LFB23027:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	testq	%rax, %rax
	setne	%dl
	cmpb	%dl, 8(%rdi)
	je	.L1326
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	136(%rdi), %rdx
	movq	%rdi, %rbx
	movq	40952(%rdx), %rdi
	testq	%rax, %rax
	je	.L1328
	call	_ZN2v88internal16CompilationCache7DisableEv@PLT
	movl	$1, %r12d
	testb	$2, 56(%rbx)
	je	.L1334
.L1329:
	movb	%r12b, 8(%rbx)
	movq	136(%rbx), %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal7Isolate23PromiseHookStateUpdatedEv@PLT
	.p2align 4,,10
	.p2align 3
.L1334:
	.cfi_restore_state
	movq	48(%rbx), %rax
	movl	$1, %esi
	movq	40960(%rax), %rdi
	addq	$248, %rdi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	orl	$2, 56(%rbx)
	jmp	.L1329
	.p2align 4,,10
	.p2align 3
.L1328:
	call	_ZN2v88internal16CompilationCache6EnableEv@PLT
	movq	%rbx, %rdi
	xorl	%r12d, %r12d
	call	_ZN2v88internal5Debug6UnloadEv
	jmp	.L1329
	.p2align 4,,10
	.p2align 3
.L1326:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE23027:
	.size	_ZN2v88internal5Debug11UpdateStateEv, .-_ZN2v88internal5Debug11UpdateStateEv
	.section	.text._ZN2v88internal5Debug24UpdateHookOnFunctionCallEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug24UpdateHookOnFunctionCallEv
	.type	_ZN2v88internal5Debug24UpdateHookOnFunctionCallEv, @function
_ZN2v88internal5Debug24UpdateHookOnFunctionCallEv:
.LFB23028:
	.cfi_startproc
	endbr64
	cmpb	$2, 76(%rdi)
	movl	$1, %eax
	je	.L1336
	movq	136(%rdi), %rdx
	cmpl	$32, 41828(%rdx)
	je	.L1336
	movzbl	132(%rdi), %eax
.L1336:
	movb	%al, 9(%rdi)
	ret
	.cfi_endproc
.LFE23028:
	.size	_ZN2v88internal5Debug24UpdateHookOnFunctionCallEv, .-_ZN2v88internal5Debug24UpdateHookOnFunctionCallEv
	.section	.text._ZN2v88internal10DebugScopeC2EPNS0_5DebugE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10DebugScopeC2EPNS0_5DebugE
	.type	_ZN2v88internal10DebugScopeC2EPNS0_5DebugE, @function
_ZN2v88internal10DebugScopeC2EPNS0_5DebugE:
.LFB23031:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	movl	$255, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	addq	$24, %rdi
	subq	$1456, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -24(%rdi)
	movq	64(%rsi), %rax
	movq	%rax, -16(%rdi)
	movq	136(%rsi), %rsi
	call	_ZN2v88internal15InterruptsScopeC2EPNS0_7IsolateElNS1_4ModeE@PLT
	leaq	16+_ZTVN2v88internal23PostponeInterruptsScopeE(%rip), %rax
	leaq	-1472(%rbp), %rdi
	movq	%rax, 24(%rbx)
	movq	(%rbx), %rax
	movq	%rbx, 64(%rax)
	movq	(%rbx), %rax
	movl	72(%rax), %edx
	movl	%edx, 16(%rbx)
	movq	136(%rax), %rsi
	call	_ZN2v88internal23StackTraceFrameIteratorC1EPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rdi
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L1340
	movq	(%rdi), %rax
	call	*56(%rax)
.L1340:
	movq	(%rbx), %rdx
	movl	%eax, 72(%rdx)
	movq	(%rbx), %rbx
	movq	(%rbx), %rax
	testq	%rax, %rax
	setne	%dl
	cmpb	8(%rbx), %dl
	je	.L1339
	movq	136(%rbx), %rdx
	movq	40952(%rdx), %rdi
	testq	%rax, %rax
	je	.L1342
	call	_ZN2v88internal16CompilationCache7DisableEv@PLT
	movl	$1, %r12d
	testb	$2, 56(%rbx)
	je	.L1350
.L1343:
	movb	%r12b, 8(%rbx)
	movq	136(%rbx), %rdi
	call	_ZN2v88internal7Isolate23PromiseHookStateUpdatedEv@PLT
.L1339:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1351
	addq	$1456, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1350:
	.cfi_restore_state
	movq	48(%rbx), %rax
	movl	$1, %esi
	movq	40960(%rax), %rdi
	addq	$248, %rdi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	orl	$2, 56(%rbx)
	jmp	.L1343
	.p2align 4,,10
	.p2align 3
.L1342:
	call	_ZN2v88internal16CompilationCache6EnableEv@PLT
	movq	%rbx, %rdi
	xorl	%r12d, %r12d
	call	_ZN2v88internal5Debug6UnloadEv
	jmp	.L1343
.L1351:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23031:
	.size	_ZN2v88internal10DebugScopeC2EPNS0_5DebugE, .-_ZN2v88internal10DebugScopeC2EPNS0_5DebugE
	.globl	_ZN2v88internal10DebugScopeC1EPNS0_5DebugE
	.set	_ZN2v88internal10DebugScopeC1EPNS0_5DebugE,_ZN2v88internal10DebugScopeC2EPNS0_5DebugE
	.section	.text._ZN2v88internal5Debug15SetScriptSourceENS0_6HandleINS0_6ScriptEEENS2_INS0_6StringEEEbPNS_5debug14LiveEditResultE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug15SetScriptSourceENS0_6HandleINS0_6ScriptEEENS2_INS0_6StringEEEbPNS_5debug14LiveEditResultE
	.type	_ZN2v88internal5Debug15SetScriptSourceENS0_6HandleINS0_6ScriptEEENS2_INS0_6StringEEEbPNS_5debug14LiveEditResultE, @function
_ZN2v88internal5Debug15SetScriptSourceENS0_6HandleINS0_6ScriptEEENS2_INS0_6StringEEEbPNS_5debug14LiveEditResultE:
.LFB23021:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	-128(%rbp), %rdi
	movq	%rbx, %rsi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal10DebugScopeC1EPNS0_5DebugE
	movb	$1, 11(%rbx)
	movzbl	%r12b, %ecx
	movq	%r14, %rdx
	movq	136(%rbx), %rdi
	movq	%r15, %r8
	movq	%r13, %rsi
	call	_ZN2v88internal8LiveEdit11PatchScriptEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEENS4_INS0_6StringEEEbPNS_5debug14LiveEditResultE@PLT
	movb	$0, 11(%rbx)
	movl	(%r15), %eax
	movq	-120(%rbp), %rdx
	testl	%eax, %eax
	movq	-128(%rbp), %rax
	sete	%r12b
	movq	%rdx, 64(%rax)
	movq	-128(%rbp), %rbx
	movl	-112(%rbp), %eax
	movl	%eax, 72(%rbx)
	movq	(%rbx), %rax
	testq	%rax, %rax
	setne	%dl
	cmpb	8(%rbx), %dl
	je	.L1353
	movq	136(%rbx), %rdx
	movq	40952(%rdx), %rdi
	testq	%rax, %rax
	je	.L1354
	call	_ZN2v88internal16CompilationCache7DisableEv@PLT
	movl	$1, %r13d
	testb	$2, 56(%rbx)
	je	.L1360
.L1355:
	movb	%r13b, 8(%rbx)
	movq	136(%rbx), %rdi
	call	_ZN2v88internal7Isolate23PromiseHookStateUpdatedEv@PLT
.L1353:
	leaq	16+_ZTVN2v88internal15InterruptsScopeE(%rip), %rax
	cmpl	$2, -72(%rbp)
	movq	%rax, -104(%rbp)
	je	.L1352
	movq	-96(%rbp), %rdi
	call	_ZN2v88internal10StackGuard18PopInterruptsScopeEv@PLT
.L1352:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1361
	addq	$88, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1360:
	.cfi_restore_state
	movq	48(%rbx), %rax
	movl	$1, %esi
	movq	40960(%rax), %rdi
	addq	$248, %rdi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	orl	$2, 56(%rbx)
	jmp	.L1355
	.p2align 4,,10
	.p2align 3
.L1354:
	call	_ZN2v88internal16CompilationCache6EnableEv@PLT
	movq	%rbx, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal5Debug6UnloadEv
	jmp	.L1355
.L1361:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23021:
	.size	_ZN2v88internal5Debug15SetScriptSourceENS0_6HandleINS0_6ScriptEEENS2_INS0_6StringEEEbPNS_5debug14LiveEditResultE, .-_ZN2v88internal5Debug15SetScriptSourceENS0_6HandleINS0_6ScriptEEENS2_INS0_6StringEEEbPNS_5debug14LiveEditResultE
	.section	.text._ZN2v88internal10DebugScopeD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10DebugScopeD2Ev
	.type	_ZN2v88internal10DebugScopeD2Ev, @function
_ZN2v88internal10DebugScopeD2Ev:
.LFB23034:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	movq	8(%rdi), %rdx
	movq	%rdx, 64(%rax)
	movq	(%rdi), %rax
	movl	16(%rdi), %edx
	movl	%edx, 72(%rax)
	movq	(%rdi), %r12
	movq	(%r12), %rax
	testq	%rax, %rax
	setne	%dl
	cmpb	8(%r12), %dl
	je	.L1363
	movq	136(%r12), %rdx
	movq	40952(%rdx), %rdi
	testq	%rax, %rax
	je	.L1364
	call	_ZN2v88internal16CompilationCache7DisableEv@PLT
	movl	$1, %r13d
	testb	$2, 56(%r12)
	je	.L1369
.L1365:
	movb	%r13b, 8(%r12)
	movq	136(%r12), %rdi
	call	_ZN2v88internal7Isolate23PromiseHookStateUpdatedEv@PLT
.L1363:
	leaq	16+_ZTVN2v88internal15InterruptsScopeE(%rip), %rax
	cmpl	$2, 56(%rbx)
	movq	%rax, 24(%rbx)
	jne	.L1370
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1369:
	.cfi_restore_state
	movq	48(%r12), %rax
	movl	$1, %esi
	movq	40960(%rax), %rdi
	addq	$248, %rdi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	orl	$2, 56(%r12)
	jmp	.L1365
	.p2align 4,,10
	.p2align 3
.L1370:
	movq	32(%rbx), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal10StackGuard18PopInterruptsScopeEv@PLT
	.p2align 4,,10
	.p2align 3
.L1364:
	.cfi_restore_state
	call	_ZN2v88internal16CompilationCache6EnableEv@PLT
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal5Debug6UnloadEv
	jmp	.L1365
	.cfi_endproc
.LFE23034:
	.size	_ZN2v88internal10DebugScopeD2Ev, .-_ZN2v88internal10DebugScopeD2Ev
	.globl	_ZN2v88internal10DebugScopeD1Ev
	.set	_ZN2v88internal10DebugScopeD1Ev,_ZN2v88internal10DebugScopeD2Ev
	.section	.text._ZN2v88internal5Debug19ProcessCompileEventEbNS0_6HandleINS0_6ScriptEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug19ProcessCompileEventEbNS0_6HandleINS0_6ScriptEEE
	.type	_ZN2v88internal5Debug19ProcessCompileEventEbNS0_6HandleINS0_6ScriptEEE, @function
_ZN2v88internal5Debug19ProcessCompileEventEbNS0_6HandleINS0_6ScriptEEE:
.LFB23024:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, -148(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 11(%rdi)
	je	.L1396
.L1371:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1397
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1396:
	.cfi_restore_state
	movq	136(%rdi), %r12
	movq	(%rdx), %r13
	movq	%rdi, %r14
	movq	%rdx, %rbx
	movq	12464(%r12), %rax
	movq	39(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1373
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L1374:
	movq	335(%rsi), %r12
	leaq	39(%r13), %r15
	movq	%r12, 39(%r13)
	testb	$1, %r12b
	je	.L1387
	movq	%r12, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -160(%rbp)
	testl	$262144, %eax
	jne	.L1398
.L1377:
	testb	$24, %al
	je	.L1387
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1399
	.p2align 4,,10
	.p2align 3
.L1387:
	cmpb	$0, 10(%r14)
	jne	.L1371
	cmpb	$0, 8(%r14)
	je	.L1371
	movq	136(%r14), %rax
	cmpl	$32, 41828(%rax)
	je	.L1371
	movq	(%rbx), %rax
	leaq	-128(%rbp), %r12
	movq	%r12, %rdi
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal6Script16IsUserJavaScriptEv@PLT
	testb	%al, %al
	jne	.L1379
	movq	(%rbx), %rax
	cmpl	$3, 51(%rax)
	jne	.L1371
.L1379:
	cmpq	$0, (%r14)
	je	.L1371
	movzbl	10(%r14), %eax
	movb	$1, 10(%r14)
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	-144(%rbp), %r13
	movb	%al, -149(%rbp)
	call	_ZN2v88internal10DebugScopeC1EPNS0_5DebugE
	movq	136(%r14), %r15
	movq	%r13, %rdi
	movq	41088(%r15), %rax
	addl	$1, 41104(%r15)
	movq	136(%r14), %rsi
	movq	%rax, -168(%rbp)
	movq	41096(%r15), %rax
	movq	%rax, -160(%rbp)
	movzbl	12(%r14), %eax
	movb	$1, 12(%r14)
	movb	%al, -150(%rbp)
	call	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb1EEC1EPNS0_7IsolateE@PLT
	movq	(%r14), %rdi
	movzbl	11(%r14), %esi
	leaq	_ZN2v85debug13DebugDelegate14ScriptCompiledENS_5LocalINS0_6ScriptEEEbb(%rip), %rdx
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1400
.L1383:
	movq	%r13, %rdi
	call	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb1EED1Ev@PLT
	movzbl	-150(%rbp), %eax
	movb	%al, 12(%r14)
	movq	-168(%rbp), %rax
	subl	$1, 41104(%r15)
	movq	%rax, 41088(%r15)
	movq	-160(%rbp), %rax
	cmpq	41096(%r15), %rax
	je	.L1386
	movq	%rax, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1386:
	movq	%r12, %rdi
	call	_ZN2v88internal10DebugScopeD1Ev
	movzbl	-149(%rbp), %eax
	movb	%al, 10(%r14)
	jmp	.L1371
	.p2align 4,,10
	.p2align 3
.L1373:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L1401
.L1375:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L1374
	.p2align 4,,10
	.p2align 3
.L1398:
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-160(%rbp), %rcx
	movq	8(%rcx), %rax
	jmp	.L1377
	.p2align 4,,10
	.p2align 3
.L1399:
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1387
	.p2align 4,,10
	.p2align 3
.L1401:
	movq	%r12, %rdi
	movq	%rsi, -160(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-160(%rbp), %rsi
	jmp	.L1375
.L1400:
	movzbl	%sil, %edx
	movzbl	-148(%rbp), %ecx
	movq	%rbx, %rsi
	call	*%rax
	jmp	.L1383
.L1397:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23024:
	.size	_ZN2v88internal5Debug19ProcessCompileEventEbNS0_6HandleINS0_6ScriptEEE, .-_ZN2v88internal5Debug19ProcessCompileEventEbNS0_6HandleINS0_6ScriptEEE
	.section	.text._ZN2v88internal5Debug14OnCompileErrorENS0_6HandleINS0_6ScriptEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug14OnCompileErrorENS0_6HandleINS0_6ScriptEEE
	.type	_ZN2v88internal5Debug14OnCompileErrorENS0_6HandleINS0_6ScriptEEE, @function
_ZN2v88internal5Debug14OnCompileErrorENS0_6HandleINS0_6ScriptEEE:
.LFB23022:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 11(%rdi)
	je	.L1427
.L1402:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1428
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1427:
	.cfi_restore_state
	movq	136(%rdi), %rbx
	movq	(%rsi), %r13
	movq	%rdi, %r15
	movq	%rsi, %r14
	movq	12464(%rbx), %rax
	movq	39(%rax), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1404
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L1405:
	movq	335(%rsi), %r12
	leaq	39(%r13), %rsi
	movq	%r12, 39(%r13)
	testb	$1, %r12b
	je	.L1418
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L1429
.L1408:
	testb	$24, %al
	je	.L1418
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1430
	.p2align 4,,10
	.p2align 3
.L1418:
	cmpb	$0, 10(%r15)
	jne	.L1402
	cmpb	$0, 8(%r15)
	je	.L1402
	movq	136(%r15), %rax
	cmpl	$32, 41828(%rax)
	je	.L1402
	movq	(%r14), %rax
	leaq	-128(%rbp), %r12
	movq	%r12, %rdi
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal6Script16IsUserJavaScriptEv@PLT
	testb	%al, %al
	jne	.L1410
	movq	(%r14), %rax
	cmpl	$3, 51(%rax)
	jne	.L1402
.L1410:
	cmpq	$0, (%r15)
	je	.L1402
	movzbl	10(%r15), %eax
	movb	$1, 10(%r15)
	movq	%r15, %rsi
	movq	%r12, %rdi
	leaq	-144(%rbp), %r13
	movb	%al, -152(%rbp)
	call	_ZN2v88internal10DebugScopeC1EPNS0_5DebugE
	movq	136(%r15), %r8
	movq	%r13, %rdi
	movq	41088(%r8), %rax
	addl	$1, 41104(%r8)
	movq	41096(%r8), %rbx
	movq	136(%r15), %rsi
	movq	%r8, -160(%rbp)
	movq	%rax, -168(%rbp)
	movzbl	12(%r15), %eax
	movb	$1, 12(%r15)
	movb	%al, -169(%rbp)
	call	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb1EEC1EPNS0_7IsolateE@PLT
	movq	(%r15), %rdi
	leaq	_ZN2v85debug13DebugDelegate14ScriptCompiledENS_5LocalINS0_6ScriptEEEbb(%rip), %rdx
	movzbl	11(%r15), %ecx
	movq	-160(%rbp), %r8
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1431
.L1414:
	movq	%r13, %rdi
	movq	%r8, -160(%rbp)
	call	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb1EED1Ev@PLT
	movzbl	-169(%rbp), %eax
	movq	-160(%rbp), %r8
	movb	%al, 12(%r15)
	movq	-168(%rbp), %rax
	subl	$1, 41104(%r8)
	movq	%rax, 41088(%r8)
	cmpq	41096(%r8), %rbx
	je	.L1417
	movq	%rbx, 41096(%r8)
	movq	%r8, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1417:
	movq	%r12, %rdi
	call	_ZN2v88internal10DebugScopeD1Ev
	movzbl	-152(%rbp), %eax
	movb	%al, 10(%r15)
	jmp	.L1402
	.p2align 4,,10
	.p2align 3
.L1404:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L1432
.L1406:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L1405
	.p2align 4,,10
	.p2align 3
.L1429:
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rsi, -152(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-152(%rbp), %rsi
	jmp	.L1408
	.p2align 4,,10
	.p2align 3
.L1430:
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1418
	.p2align 4,,10
	.p2align 3
.L1432:
	movq	%rbx, %rdi
	movq	%rsi, -152(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-152(%rbp), %rsi
	jmp	.L1406
.L1431:
	movzbl	%cl, %edx
	movq	%r14, %rsi
	movl	$1, %ecx
	call	*%rax
	movq	-160(%rbp), %r8
	jmp	.L1414
.L1428:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23022:
	.size	_ZN2v88internal5Debug14OnCompileErrorENS0_6HandleINS0_6ScriptEEE, .-_ZN2v88internal5Debug14OnCompileErrorENS0_6HandleINS0_6ScriptEEE
	.section	.text._ZN2v88internal5Debug14OnAfterCompileENS0_6HandleINS0_6ScriptEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug14OnAfterCompileENS0_6HandleINS0_6ScriptEEE
	.type	_ZN2v88internal5Debug14OnAfterCompileENS0_6HandleINS0_6ScriptEEE, @function
_ZN2v88internal5Debug14OnAfterCompileENS0_6HandleINS0_6ScriptEEE:
.LFB23023:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 11(%rdi)
	je	.L1458
.L1433:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1459
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1458:
	.cfi_restore_state
	movq	136(%rdi), %rbx
	movq	(%rsi), %r13
	movq	%rdi, %r15
	movq	%rsi, %r14
	movq	12464(%rbx), %rax
	movq	39(%rax), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1435
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L1436:
	movq	335(%rsi), %r12
	leaq	39(%r13), %rsi
	movq	%r12, 39(%r13)
	testb	$1, %r12b
	je	.L1449
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L1460
.L1439:
	testb	$24, %al
	je	.L1449
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1461
	.p2align 4,,10
	.p2align 3
.L1449:
	cmpb	$0, 10(%r15)
	jne	.L1433
	cmpb	$0, 8(%r15)
	je	.L1433
	movq	136(%r15), %rax
	cmpl	$32, 41828(%rax)
	je	.L1433
	movq	(%r14), %rax
	leaq	-128(%rbp), %r12
	movq	%r12, %rdi
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal6Script16IsUserJavaScriptEv@PLT
	testb	%al, %al
	jne	.L1441
	movq	(%r14), %rax
	cmpl	$3, 51(%rax)
	jne	.L1433
.L1441:
	cmpq	$0, (%r15)
	je	.L1433
	movzbl	10(%r15), %eax
	movb	$1, 10(%r15)
	movq	%r15, %rsi
	movq	%r12, %rdi
	leaq	-144(%rbp), %r13
	movb	%al, -152(%rbp)
	call	_ZN2v88internal10DebugScopeC1EPNS0_5DebugE
	movq	136(%r15), %r8
	movq	%r13, %rdi
	movq	41088(%r8), %rax
	addl	$1, 41104(%r8)
	movq	41096(%r8), %rbx
	movq	136(%r15), %rsi
	movq	%r8, -160(%rbp)
	movq	%rax, -168(%rbp)
	movzbl	12(%r15), %eax
	movb	$1, 12(%r15)
	movb	%al, -169(%rbp)
	call	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb1EEC1EPNS0_7IsolateE@PLT
	movq	(%r15), %rdi
	leaq	_ZN2v85debug13DebugDelegate14ScriptCompiledENS_5LocalINS0_6ScriptEEEbb(%rip), %rdx
	movzbl	11(%r15), %ecx
	movq	-160(%rbp), %r8
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1462
.L1445:
	movq	%r13, %rdi
	movq	%r8, -160(%rbp)
	call	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb1EED1Ev@PLT
	movzbl	-169(%rbp), %eax
	movq	-160(%rbp), %r8
	movb	%al, 12(%r15)
	movq	-168(%rbp), %rax
	subl	$1, 41104(%r8)
	movq	%rax, 41088(%r8)
	cmpq	41096(%r8), %rbx
	je	.L1448
	movq	%rbx, 41096(%r8)
	movq	%r8, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1448:
	movq	%r12, %rdi
	call	_ZN2v88internal10DebugScopeD1Ev
	movzbl	-152(%rbp), %eax
	movb	%al, 10(%r15)
	jmp	.L1433
	.p2align 4,,10
	.p2align 3
.L1435:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L1463
.L1437:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L1436
	.p2align 4,,10
	.p2align 3
.L1460:
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rsi, -152(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-152(%rbp), %rsi
	jmp	.L1439
	.p2align 4,,10
	.p2align 3
.L1461:
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1449
	.p2align 4,,10
	.p2align 3
.L1463:
	movq	%rbx, %rdi
	movq	%rsi, -152(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-152(%rbp), %rsi
	jmp	.L1437
.L1462:
	movzbl	%cl, %edx
	movq	%r14, %rsi
	xorl	%ecx, %ecx
	call	*%rax
	movq	-160(%rbp), %r8
	jmp	.L1445
.L1459:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23023:
	.size	_ZN2v88internal5Debug14OnAfterCompileENS0_6HandleINS0_6ScriptEEE, .-_ZN2v88internal5Debug14OnAfterCompileENS0_6HandleINS0_6ScriptEEE
	.section	.text._ZN2v88internal16ReturnValueScopeC2EPNS0_5DebugE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16ReturnValueScopeC2EPNS0_5DebugE
	.type	_ZN2v88internal16ReturnValueScopeC2EPNS0_5DebugE, @function
_ZN2v88internal16ReturnValueScopeC2EPNS0_5DebugE:
.LFB23037:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	$0, 8(%rdi)
	movq	136(%rsi), %r12
	movq	%rsi, (%rdi)
	movq	104(%rsi), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1465
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, 8(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1465:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L1469
.L1467:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	movq	%rax, 8(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1469:
	.cfi_restore_state
	movq	%r12, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L1467
	.cfi_endproc
.LFE23037:
	.size	_ZN2v88internal16ReturnValueScopeC2EPNS0_5DebugE, .-_ZN2v88internal16ReturnValueScopeC2EPNS0_5DebugE
	.globl	_ZN2v88internal16ReturnValueScopeC1EPNS0_5DebugE
	.set	_ZN2v88internal16ReturnValueScopeC1EPNS0_5DebugE,_ZN2v88internal16ReturnValueScopeC2EPNS0_5DebugE
	.section	.text._ZN2v88internal16ReturnValueScopeD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16ReturnValueScopeD2Ev
	.type	_ZN2v88internal16ReturnValueScopeD2Ev, @function
_ZN2v88internal16ReturnValueScopeD2Ev:
.LFB23040:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdx
	movq	(%rdi), %rax
	movq	(%rdx), %rdx
	movq	%rdx, 104(%rax)
	ret
	.cfi_endproc
.LFE23040:
	.size	_ZN2v88internal16ReturnValueScopeD2Ev, .-_ZN2v88internal16ReturnValueScopeD2Ev
	.globl	_ZN2v88internal16ReturnValueScopeD1Ev
	.set	_ZN2v88internal16ReturnValueScopeD1Ev,_ZN2v88internal16ReturnValueScopeD2Ev
	.section	.text._ZN2v88internal5Debug21ApplySideEffectChecksENS0_6HandleINS0_9DebugInfoEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug21ApplySideEffectChecksENS0_6HandleINS0_9DebugInfoEEE
	.type	_ZN2v88internal5Debug21ApplySideEffectChecksENS0_6HandleINS0_9DebugInfoEEE, @function
_ZN2v88internal5Debug21ApplySideEffectChecksENS0_6HandleINS0_9DebugInfoEEE:
.LFB23055:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$32, %rsp
	movq	136(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	41112(%r12), %rdi
	movq	39(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1472
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdi
.L1473:
	call	_ZN2v88internal13DebugEvaluate21ApplySideEffectChecksENS0_6HandleINS0_13BytecodeArrayEEE@PLT
	movq	(%rbx), %rax
	leaq	-32(%rbp), %rdi
	movl	$32, %esi
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal9DebugInfo21SetDebugExecutionModeENS1_13ExecutionModeE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1477
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1472:
	.cfi_restore_state
	movq	41088(%r12), %rdi
	cmpq	41096(%r12), %rdi
	je	.L1478
.L1474:
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdi)
	jmp	.L1473
	.p2align 4,,10
	.p2align 3
.L1478:
	movq	%r12, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	movq	%rax, %rdi
	jmp	.L1474
.L1477:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23055:
	.size	_ZN2v88internal5Debug21ApplySideEffectChecksENS0_6HandleINS0_9DebugInfoEEE, .-_ZN2v88internal5Debug21ApplySideEffectChecksENS0_6HandleINS0_9DebugInfoEEE
	.section	.text._ZN2v88internal5Debug21ClearSideEffectChecksENS0_6HandleINS0_9DebugInfoEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug21ClearSideEffectChecksENS0_6HandleINS0_9DebugInfoEEE
	.type	_ZN2v88internal5Debug21ClearSideEffectChecksENS0_6HandleINS0_9DebugInfoEEE, @function
_ZN2v88internal5Debug21ClearSideEffectChecksENS0_6HandleINS0_9DebugInfoEEE:
.LFB23056:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$48, %rsp
	movq	136(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	41112(%r14), %rdi
	movq	39(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1480
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r12
.L1481:
	movq	136(%r13), %r14
	movq	(%rbx), %rax
	movq	41112(%r14), %rdi
	movq	31(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1483
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L1484:
	leaq	-64(%rbp), %rbx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter21BytecodeArrayIteratorC1ENS0_6HandleINS0_13BytecodeArrayEEE@PLT
	jmp	.L1487
	.p2align 4,,10
	.p2align 3
.L1494:
	movl	-56(%rbp), %eax
	movq	0(%r13), %rdx
	movq	%rbx, %rdi
	addl	$54, %eax
	cltq
	movzbl	-1(%rax,%rdx), %ecx
	movq	(%r12), %rdx
	movb	%cl, -1(%rax,%rdx)
	call	_ZN2v88internal11interpreter21BytecodeArrayIterator7AdvanceEv@PLT
.L1487:
	movq	%rbx, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayIterator4doneEv@PLT
	testb	%al, %al
	je	.L1494
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1479
	movq	(%rdi), %rax
	call	*72(%rax)
.L1479:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1495
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1480:
	.cfi_restore_state
	movq	41088(%r14), %r12
	cmpq	41096(%r14), %r12
	je	.L1496
.L1482:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%r12)
	jmp	.L1481
	.p2align 4,,10
	.p2align 3
.L1483:
	movq	41088(%r14), %r13
	cmpq	%r13, 41096(%r14)
	je	.L1497
.L1485:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, 0(%r13)
	jmp	.L1484
.L1496:
	movq	%r14, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L1482
.L1497:
	movq	%r14, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L1485
.L1495:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23056:
	.size	_ZN2v88internal5Debug21ClearSideEffectChecksENS0_6HandleINS0_9DebugInfoEEE, .-_ZN2v88internal5Debug21ClearSideEffectChecksENS0_6HandleINS0_9DebugInfoEEE
	.section	.text._ZN2v88internal5Debug32UpdateDebugInfosForExecutionModeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug32UpdateDebugInfosForExecutionModeEv
	.type	_ZN2v88internal5Debug32UpdateDebugInfosForExecutionModeEv, @function
_ZN2v88internal5Debug32UpdateDebugInfosForExecutionModeEv:
.LFB23042:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.L1498
	movq	%rdi, %r13
	leaq	-64(%rbp), %r14
	jmp	.L1507
	.p2align 4,,10
	.p2align 3
.L1503:
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L1498
.L1507:
	movq	(%rbx), %r12
	movq	(%r12), %rax
	movq	39(%rax), %rdx
	testb	$1, %dl
	je	.L1503
	movq	-1(%rdx), %rdx
	cmpw	$72, 11(%rdx)
	jne	.L1503
	movq	%r14, %rdi
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal9DebugInfo18DebugExecutionModeEv@PLT
	movl	%eax, %r8d
	movq	136(%r13), %rax
	movl	41828(%rax), %eax
	cmpl	%eax, %r8d
	je	.L1503
	movq	%r12, %rsi
	movq	%r13, %rdi
	testl	%eax, %eax
	jne	.L1513
	call	_ZN2v88internal5Debug21ClearSideEffectChecksENS0_6HandleINS0_9DebugInfoEEE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal5Debug16ApplyBreakPointsENS0_6HandleINS0_9DebugInfoEEE
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L1507
	.p2align 4,,10
	.p2align 3
.L1498:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1515
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1513:
	.cfi_restore_state
	call	_ZN2v88internal5Debug16ClearBreakPointsENS0_6HandleINS0_9DebugInfoEEE
	movq	136(%r13), %r15
	movq	(%r12), %rax
	movq	41112(%r15), %rdi
	movq	39(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1504
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdi
.L1505:
	call	_ZN2v88internal13DebugEvaluate21ApplySideEffectChecksENS0_6HandleINS0_13BytecodeArrayEEE@PLT
	movq	(%r12), %rax
	movl	$32, %esi
	movq	%r14, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal9DebugInfo21SetDebugExecutionModeENS1_13ExecutionModeE@PLT
	jmp	.L1503
	.p2align 4,,10
	.p2align 3
.L1504:
	movq	41088(%r15), %rdi
	cmpq	41096(%r15), %rdi
	je	.L1516
.L1506:
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rdi)
	jmp	.L1505
.L1516:
	movq	%r15, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %rdi
	jmp	.L1506
.L1515:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23042:
	.size	_ZN2v88internal5Debug32UpdateDebugInfosForExecutionModeEv, .-_ZN2v88internal5Debug32UpdateDebugInfosForExecutionModeEv
	.section	.text._ZN2v88internal5Debug24StartSideEffectCheckModeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug24StartSideEffectCheckModeEv
	.type	_ZN2v88internal5Debug24StartSideEffectCheckModeEv, @function
_ZN2v88internal5Debug24StartSideEffectCheckModeEv:
.LFB23043:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	136(%rdi), %rax
	movl	$32, 41828(%rax)
	cmpb	$2, 76(%rdi)
	movl	$1, %eax
	je	.L1518
	movq	136(%rdi), %rdx
	cmpl	$32, 41828(%rdx)
	je	.L1518
	movzbl	132(%rdi), %eax
.L1518:
	movb	%al, 9(%r12)
	movl	$104, %edi
	leaq	16+_ZTVN2v88internal5Debug23TemporaryObjectsTrackerE(%rip), %rbx
	movb	$0, 16(%r12)
	call	_Znwm@PLT
	movl	$13, %ecx
	movq	%rax, %r13
	xorl	%eax, %eax
	movq	%r13, %rdi
	rep stosq
	leaq	56(%r13), %rax
	movq	%rbx, 0(%r13)
	leaq	64(%r13), %rdi
	movq	%rax, 8(%r13)
	movq	$1, 16(%r13)
	movl	$0x3f800000, 40(%r13)
	call	_ZN2v84base5MutexC1Ev@PLT
	movq	32(%r12), %r14
	movq	%r13, 32(%r12)
	testq	%r14, %r14
	je	.L1519
	movq	(%r14), %rax
	leaq	_ZN2v88internal5Debug23TemporaryObjectsTrackerD0Ev(%rip), %rdx
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1520
	movq	%rbx, (%r14)
	leaq	64(%r14), %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L1524
	.p2align 4,,10
	.p2align 3
.L1521:
	movq	%r13, %rdi
	movq	0(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L1521
.L1524:
	movq	16(%r14), %rax
	movq	8(%r14), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	8(%r14), %rdi
	leaq	56(%r14), %rax
	movq	$0, 32(%r14)
	movq	$0, 24(%r14)
	cmpq	%rax, %rdi
	je	.L1522
	call	_ZdlPv@PLT
.L1522:
	movl	$104, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
	movq	32(%r12), %r13
.L1519:
	movq	136(%r12), %rax
	movq	%r13, %rsi
	leaq	37592(%rax), %rdi
	call	_ZN2v88internal4Heap30AddHeapObjectAllocationTrackerEPNS0_27HeapObjectAllocationTrackerE@PLT
	movq	136(%r12), %r13
	movq	12464(%r13), %rax
	movq	39(%rax), %rsi
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1525
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L1526:
	movq	1055(%rsi), %r14
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1528
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1529:
	movq	136(%r12), %rdi
	call	_ZN2v88internal7Factory14CopyFixedArrayENS0_6HandleINS0_10FixedArrayEEE@PLT
	movq	%r12, %rdi
	movq	%rax, 40(%r12)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal5Debug32UpdateDebugInfosForExecutionModeEv
	.p2align 4,,10
	.p2align 3
.L1528:
	.cfi_restore_state
	movq	41088(%r13), %rsi
	cmpq	41096(%r13), %rsi
	je	.L1542
.L1530:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r13)
	movq	%r14, (%rsi)
	jmp	.L1529
	.p2align 4,,10
	.p2align 3
.L1525:
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L1543
.L1527:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%rsi, (%rax)
	jmp	.L1526
	.p2align 4,,10
	.p2align 3
.L1520:
	movq	%r14, %rdi
	call	*%rax
	movq	32(%r12), %r13
	jmp	.L1519
	.p2align 4,,10
	.p2align 3
.L1543:
	movq	%r13, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L1527
	.p2align 4,,10
	.p2align 3
.L1542:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L1530
	.cfi_endproc
.LFE23043:
	.size	_ZN2v88internal5Debug24StartSideEffectCheckModeEv, .-_ZN2v88internal5Debug24StartSideEffectCheckModeEv
	.section	.text._ZN2v88internal5Debug23StopSideEffectCheckModeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug23StopSideEffectCheckModeEv
	.type	_ZN2v88internal5Debug23StopSideEffectCheckModeEv, @function
_ZN2v88internal5Debug23StopSideEffectCheckModeEv:
.LFB23054:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	cmpb	$0, 16(%r12)
	movq	136(%rdi), %rdi
	jne	.L1577
.L1545:
	movl	$0, 41828(%rdi)
	cmpb	$2, 76(%r12)
	movl	$1, %eax
	movq	136(%r12), %rdi
	je	.L1547
	cmpl	$32, 41828(%rdi)
	je	.L1547
	movzbl	132(%r12), %eax
.L1547:
	movb	%al, 9(%r12)
	movq	32(%r12), %rsi
	addq	$37592, %rdi
	movb	$0, 16(%r12)
	call	_ZN2v88internal4Heap33RemoveHeapObjectAllocationTrackerEPNS0_27HeapObjectAllocationTrackerE@PLT
	movq	32(%r12), %r13
	movq	$0, 32(%r12)
	testq	%r13, %r13
	je	.L1548
	movq	0(%r13), %rax
	leaq	_ZN2v88internal5Debug23TemporaryObjectsTrackerD0Ev(%rip), %rdx
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1549
	leaq	16+_ZTVN2v88internal5Debug23TemporaryObjectsTrackerE(%rip), %rax
	leaq	64(%r13), %rdi
	movq	%rax, 0(%r13)
	call	_ZN2v84base5MutexD1Ev@PLT
	movq	24(%r13), %rbx
	testq	%rbx, %rbx
	je	.L1553
	.p2align 4,,10
	.p2align 3
.L1550:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1550
.L1553:
	movq	16(%r13), %rax
	movq	8(%r13), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	8(%r13), %rdi
	leaq	56(%r13), %rax
	movq	$0, 32(%r13)
	movq	$0, 24(%r13)
	cmpq	%rax, %rdi
	je	.L1551
	call	_ZdlPv@PLT
.L1551:
	movl	$104, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1548:
	movq	136(%r12), %rbx
	movq	12464(%rbx), %rax
	movq	39(%rax), %r13
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1554
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r13
.L1555:
	movq	40(%r12), %rax
	leaq	1055(%r13), %r15
	movq	(%rax), %r14
	movq	%r14, 1055(%r13)
	testb	$1, %r14b
	je	.L1560
	movq	%r14, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L1578
	testb	$24, %al
	je	.L1560
.L1580:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1579
.L1560:
	movq	$0, 40(%r12)
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal5Debug32UpdateDebugInfosForExecutionModeEv
	.p2align 4,,10
	.p2align 3
.L1578:
	.cfi_restore_state
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L1580
	jmp	.L1560
	.p2align 4,,10
	.p2align 3
.L1554:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L1581
.L1556:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%r13, (%rax)
	jmp	.L1555
	.p2align 4,,10
	.p2align 3
.L1577:
	call	_ZN2v88internal7Isolate24CancelTerminateExecutionEv@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	136(%r12), %r13
	movl	$338, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal7Factory12NewEvalErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	movq	%r13, %rdi
	xorl	%edx, %edx
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	136(%r12), %rdi
	jmp	.L1545
	.p2align 4,,10
	.p2align 3
.L1579:
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1560
	.p2align 4,,10
	.p2align 3
.L1549:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1548
	.p2align 4,,10
	.p2align 3
.L1581:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L1556
	.cfi_endproc
.LFE23054:
	.size	_ZN2v88internal5Debug23StopSideEffectCheckModeEv, .-_ZN2v88internal5Debug23StopSideEffectCheckModeEv
	.section	.text._ZN2v88internal5Debug19return_value_handleEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug19return_value_handleEv
	.type	_ZN2v88internal5Debug19return_value_handleEv, @function
_ZN2v88internal5Debug19return_value_handleEv:
.LFB23061:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	136(%rdi), %rbx
	movq	104(%rdi), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1583
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1583:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L1587
.L1585:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1587:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L1585
	.cfi_endproc
.LFE23061:
	.size	_ZN2v88internal5Debug19return_value_handleEv, .-_ZN2v88internal5Debug19return_value_handleEv
	.section	.rodata._ZN2v88internal5Debug31PerformSideEffectCheckForObjectENS0_6HandleINS0_6ObjectEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"[debug-evaluate] failed runtime side effect check.\n"
	.section	.text._ZN2v88internal5Debug31PerformSideEffectCheckForObjectENS0_6HandleINS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug31PerformSideEffectCheckForObjectENS0_6HandleINS0_6ObjectEEE
	.type	_ZN2v88internal5Debug31PerformSideEffectCheckForObjectENS0_6HandleINS0_6ObjectEEE, @function
_ZN2v88internal5Debug31PerformSideEffectCheckForObjectENS0_6HandleINS0_6ObjectEEE:
.LFB23064:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rsi), %rax
	movq	%rax, %rdx
	notq	%rdx
	movl	%edx, %r12d
	andl	$1, %r12d
	je	.L1589
.L1601:
	movl	$1, %r12d
.L1588:
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1589:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	je	.L1601
	movq	-1(%rax), %rdx
	leaq	-1(%rax), %rcx
	cmpw	$64, 11(%rdx)
	jbe	.L1601
	movq	32(%rdi), %r14
	movq	(%rcx), %rdx
	movq	%rdi, %r13
	cmpw	$1024, 11(%rdx)
	ja	.L1615
.L1597:
	movq	16(%r14), %r8
	leaq	-1(%rax), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%r8
	movq	8(%r14), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L1599
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L1602
	.p2align 4,,10
	.p2align 3
.L1616:
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%r8
	cmpq	%rdx, %r9
	jne	.L1599
.L1602:
	cmpq	%rsi, %rdi
	je	.L1601
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.L1616
.L1599:
	cmpb	$0, _ZN2v88internal42FLAG_trace_side_effect_free_debug_evaluateE(%rip)
	jne	.L1617
.L1603:
	movb	$1, 16(%r13)
	movq	136(%r13), %rdi
	call	_ZN2v88internal7Isolate18TerminateExecutionEv@PLT
	jmp	.L1588
	.p2align 4,,10
	.p2align 3
.L1615:
	movq	-1(%rax), %rcx
	movzbl	7(%rcx), %edx
	sall	$3, %edx
	movl	%edx, %r15d
	je	.L1597
	movzwl	11(%rcx), %edi
	movq	%rsi, %rbx
	movl	$24, %edx
	cmpw	$1057, %di
	je	.L1598
	movsbl	13(%rcx), %esi
	movq	%rcx, -56(%rbp)
	shrl	$31, %esi
	call	_ZN2v88internal8JSObject13GetHeaderSizeENS0_12InstanceTypeEb@PLT
	movq	-56(%rbp), %rcx
	movl	%eax, %edx
.L1598:
	movzbl	7(%rcx), %eax
	subl	%edx, %r15d
	movzbl	8(%rcx), %ecx
	movl	%r15d, %edx
	sarl	$3, %edx
	subl	%ecx, %eax
	cmpl	%eax, %edx
	jne	.L1599
	movq	(%rbx), %rax
	jmp	.L1597
	.p2align 4,,10
	.p2align 3
.L1617:
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1603
	.cfi_endproc
.LFE23064:
	.size	_ZN2v88internal5Debug31PerformSideEffectCheckForObjectENS0_6HandleINS0_6ObjectEEE, .-_ZN2v88internal5Debug31PerformSideEffectCheckForObjectENS0_6HandleINS0_6ObjectEEE
	.section	.rodata._ZN2v88internal5Debug33PerformSideEffectCheckForCallbackENS0_6HandleINS0_6ObjectEEES4_NS1_12AccessorKindE.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"[debug-evaluate] API Callback '"
	.section	.rodata._ZN2v88internal5Debug33PerformSideEffectCheckForCallbackENS0_6HandleINS0_6ObjectEEES4_NS1_12AccessorKindE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"' may cause side effect.\n"
	.section	.rodata._ZN2v88internal5Debug33PerformSideEffectCheckForCallbackENS0_6HandleINS0_6ObjectEEES4_NS1_12AccessorKindE.str1.8
	.align 8
.LC5:
	.string	"[debug-evaluate] API Interceptor may cause side effect.\n"
	.align 8
.LC6:
	.string	"[debug-evaluate] API CallHandlerInfo may cause side effect.\n"
	.section	.text._ZN2v88internal5Debug33PerformSideEffectCheckForCallbackENS0_6HandleINS0_6ObjectEEES4_NS1_12AccessorKindE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug33PerformSideEffectCheckForCallbackENS0_6HandleINS0_6ObjectEEES4_NS1_12AccessorKindE
	.type	_ZN2v88internal5Debug33PerformSideEffectCheckForCallbackENS0_6HandleINS0_6ObjectEEES4_NS1_12AccessorKindE, @function
_ZN2v88internal5Debug33PerformSideEffectCheckForCallbackENS0_6HandleINS0_6ObjectEEES4_NS1_12AccessorKindE:
.LFB23062:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L1637
	movq	(%rsi), %r12
	testb	$1, %r12b
	jne	.L1667
.L1637:
	movb	$1, 16(%rbx)
	movq	136(%rbx), %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal7Isolate18TerminateExecutionEv@PLT
	movq	136(%rbx), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal7Isolate27OptionalRescheduleExceptionEb@PLT
.L1618:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1668
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1667:
	.cfi_restore_state
	movq	-1(%r12), %rax
	cmpw	$150, 11(%rax)
	je	.L1621
.L1665:
	xorl	%eax, %eax
	testb	%al, %al
	jne	.L1637
	movq	-1(%r12), %rax
	cmpw	$78, 11(%rax)
	je	.L1628
	testb	$1, %r12b
	je	.L1637
	movq	-1(%r12), %rax
	cmpw	$90, 11(%rax)
	je	.L1638
	movq	-1(%r12), %rax
	cmpw	$150, 11(%rax)
	jne	.L1637
	movq	%r12, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rdx
	movq	-1(%r12), %rax
	cmpq	%rax, -36992(%rdx)
	je	.L1666
	cmpb	$0, _ZN2v88internal42FLAG_trace_side_effect_free_debug_evaluateE(%rip)
	je	.L1637
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1637
	.p2align 4,,10
	.p2align 3
.L1628:
	movslq	19(%r12), %rax
	cmpl	$2, %ecx
	je	.L1669
	shrl	$5, %eax
	andl	$3, %eax
.L1633:
	movl	$1, %r13d
	cmpl	$1, %eax
	je	.L1618
	cmpl	$2, %eax
	je	.L1670
	cmpb	$0, _ZN2v88internal42FLAG_trace_side_effect_free_debug_evaluateE(%rip)
	je	.L1637
	xorl	%eax, %eax
	leaq	.LC3(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	7(%r12), %rax
	movq	stdout(%rip), %rsi
	leaq	-48(%rbp), %rdi
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal6Object10ShortPrintEP8_IO_FILE@PLT
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1637
	.p2align 4,,10
	.p2align 3
.L1638:
	testb	$16, 75(%r12)
	jne	.L1666
	cmpb	$0, _ZN2v88internal42FLAG_trace_side_effect_free_debug_evaluateE(%rip)
	je	.L1637
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1637
	.p2align 4,,10
	.p2align 3
.L1621:
	movq	%r12, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-1(%r12), %rsi
	leaq	-37592(%rax), %rdi
	cmpq	%rsi, -36984(%rax)
	jne	.L1665
	movq	592(%rdi), %rdx
	movq	%rdx, -1(%r12)
	testq	%rdx, %rdx
	je	.L1666
	testb	$1, %dl
	je	.L1666
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L1666
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L1666:
	movl	$1, %r13d
	jmp	.L1618
	.p2align 4,,10
	.p2align 3
.L1670:
	movq	%rdx, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal5Debug31PerformSideEffectCheckForObjectENS0_6HandleINS0_6ObjectEEE
	movl	%eax, %r13d
	testb	%al, %al
	jne	.L1666
	movq	136(%rbx), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal7Isolate27OptionalRescheduleExceptionEb@PLT
	jmp	.L1618
	.p2align 4,,10
	.p2align 3
.L1669:
	shrl	$7, %eax
	andl	$3, %eax
	jmp	.L1633
.L1668:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23062:
	.size	_ZN2v88internal5Debug33PerformSideEffectCheckForCallbackENS0_6HandleINS0_6ObjectEEES4_NS1_12AccessorKindE, .-_ZN2v88internal5Debug33PerformSideEffectCheckForCallbackENS0_6HandleINS0_6ObjectEEES4_NS1_12AccessorKindE
	.section	.text._ZN2v88internal5Debug32PerformSideEffectCheckAtBytecodeEPNS0_16InterpretedFrameE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug32PerformSideEffectCheckAtBytecodeEPNS0_16InterpretedFrameE
	.type	_ZN2v88internal5Debug32PerformSideEffectCheckAtBytecodeEPNS0_16InterpretedFrameE, @function
_ZN2v88internal5Debug32PerformSideEffectCheckAtBytecodeEPNS0_16InterpretedFrameE:
.LFB23063:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	call	*152(%rax)
	movq	23(%rax), %rax
	movq	31(%rax), %rdx
	testb	$1, %dl
	jne	.L1689
.L1672:
	movq	7(%rax), %rdx
	testb	$1, %dl
	jne	.L1690
.L1674:
	movq	7(%rax), %rax
	movq	7(%rax), %rbx
.L1673:
	movq	%r12, %rdi
	call	_ZNK2v88internal16InterpretedFrame17GetBytecodeOffsetEv@PLT
	movq	136(%r13), %r15
	movl	%eax, %r14d
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1675
	movq	%rbx, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1676:
	leaq	-80(%rbp), %r15
	movl	%r14d, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter21BytecodeArrayAccessorC1ENS0_6HandleINS0_13BytecodeArrayEEEi@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor16current_bytecodeEv@PLT
	cmpb	$29, %al
	je	.L1691
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movl	%eax, %esi
.L1679:
	movq	136(%r13), %r14
	movq	%r12, %rdi
	call	_ZNK2v88internal16InterpretedFrame23ReadInterpreterRegisterEi@PLT
	movq	41112(%r14), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L1680
	movq	%rax, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1681:
	movq	%r13, %rdi
	call	_ZN2v88internal5Debug31PerformSideEffectCheckForObjectENS0_6HandleINS0_6ObjectEEE
	movq	-80(%rbp), %rdi
	movl	%eax, %r12d
	testq	%rdi, %rdi
	je	.L1671
	movq	(%rdi), %rax
	call	*72(%rax)
.L1671:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1692
	addq	$40, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1675:
	.cfi_restore_state
	movq	41088(%r15), %rsi
	cmpq	41096(%r15), %rsi
	je	.L1693
.L1677:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r15)
	movq	%rbx, (%rsi)
	jmp	.L1676
	.p2align 4,,10
	.p2align 3
.L1680:
	movq	41088(%r14), %rsi
	cmpq	41096(%r14), %rsi
	je	.L1694
.L1682:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r14)
	movq	%r12, (%rsi)
	jmp	.L1681
	.p2align 4,,10
	.p2align 3
.L1691:
	call	_ZN2v88internal11interpreter8Register15current_contextEv@PLT
	movl	%eax, %esi
	jmp	.L1679
	.p2align 4,,10
	.p2align 3
.L1689:
	movq	-1(%rdx), %rcx
	cmpw	$86, 11(%rcx)
	jne	.L1672
	movq	39(%rdx), %rcx
	testb	$1, %cl
	je	.L1672
	movq	-1(%rcx), %rcx
	cmpw	$72, 11(%rcx)
	jne	.L1672
	movq	31(%rdx), %rbx
	jmp	.L1673
	.p2align 4,,10
	.p2align 3
.L1690:
	movq	-1(%rdx), %rdx
	cmpw	$72, 11(%rdx)
	jne	.L1674
	movq	7(%rax), %rbx
	jmp	.L1673
	.p2align 4,,10
	.p2align 3
.L1694:
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L1682
	.p2align 4,,10
	.p2align 3
.L1693:
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L1677
.L1692:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23063:
	.size	_ZN2v88internal5Debug32PerformSideEffectCheckAtBytecodeEPNS0_16InterpretedFrameE, .-_ZN2v88internal5Debug32PerformSideEffectCheckAtBytecodeEPNS0_16InterpretedFrameE
	.section	.text._ZNSt8_Rb_treeIN2v88internal12AccessorPairES2_St9_IdentityIS2_ESt4lessIS2_ESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal12AccessorPairES2_St9_IdentityIS2_ESt4lessIS2_ESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal12AccessorPairES2_St9_IdentityIS2_ESt4lessIS2_ESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	.type	_ZNSt8_Rb_treeIN2v88internal12AccessorPairES2_St9_IdentityIS2_ESt4lessIS2_ESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E, @function
_ZNSt8_Rb_treeIN2v88internal12AccessorPairES2_St9_IdentityIS2_ESt4lessIS2_ESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E:
.LFB27523:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L1703
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L1697:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal12AccessorPairES2_St9_IdentityIS2_ESt4lessIS2_ESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1697
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1703:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE27523:
	.size	_ZNSt8_Rb_treeIN2v88internal12AccessorPairES2_St9_IdentityIS2_ESt4lessIS2_ESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E, .-_ZNSt8_Rb_treeIN2v88internal12AccessorPairES2_St9_IdentityIS2_ESt4lessIS2_ESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	.section	.rodata._ZNSt6vectorISt4pairIN2v88internal6HandleINS2_12AccessorPairEEENS3_INS2_13NativeContextEEEESaIS8_EE17_M_realloc_insertIJS5_S7_EEEvN9__gnu_cxx17__normal_iteratorIPS8_SA_EEDpOT_.str1.1,"aMS",@progbits,1
.LC7:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorISt4pairIN2v88internal6HandleINS2_12AccessorPairEEENS3_INS2_13NativeContextEEEESaIS8_EE17_M_realloc_insertIJS5_S7_EEEvN9__gnu_cxx17__normal_iteratorIPS8_SA_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt4pairIN2v88internal6HandleINS2_12AccessorPairEEENS3_INS2_13NativeContextEEEESaIS8_EE17_M_realloc_insertIJS5_S7_EEEvN9__gnu_cxx17__normal_iteratorIPS8_SA_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt4pairIN2v88internal6HandleINS2_12AccessorPairEEENS3_INS2_13NativeContextEEEESaIS8_EE17_M_realloc_insertIJS5_S7_EEEvN9__gnu_cxx17__normal_iteratorIPS8_SA_EEDpOT_
	.type	_ZNSt6vectorISt4pairIN2v88internal6HandleINS2_12AccessorPairEEENS3_INS2_13NativeContextEEEESaIS8_EE17_M_realloc_insertIJS5_S7_EEEvN9__gnu_cxx17__normal_iteratorIPS8_SA_EEDpOT_, @function
_ZNSt6vectorISt4pairIN2v88internal6HandleINS2_12AccessorPairEEENS3_INS2_13NativeContextEEEESaIS8_EE17_M_realloc_insertIJS5_S7_EEEvN9__gnu_cxx17__normal_iteratorIPS8_SA_EEDpOT_:
.LFB27536:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movabsq	$576460752303423487, %rsi
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %r8
	movq	(%rdi), %r13
	movq	%r8, %rax
	subq	%r13, %rax
	sarq	$4, %rax
	cmpq	%rsi, %rax
	je	.L1727
	movq	%r12, %r9
	movq	%rdi, %r15
	subq	%r13, %r9
	testq	%rax, %rax
	je	.L1718
	movabsq	$9223372036854775792, %r14
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L1728
.L1708:
	movq	%r14, %rdi
	movq	%rcx, -80(%rbp)
	movq	%rdx, -72(%rbp)
	movq	%r9, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %rdx
	movq	-80(%rbp), %rcx
	leaq	(%rax,%r14), %rsi
	movq	%rax, %rbx
	leaq	16(%rax), %r14
.L1717:
	movq	(%rdx), %xmm0
	movhps	(%rcx), %xmm0
	movups	%xmm0, (%rbx,%r9)
	cmpq	%r13, %r12
	je	.L1710
	leaq	-16(%r12), %rdi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	subq	%r13, %rdi
	movq	%rdi, %rax
	shrq	$4, %rax
	addq	$1, %rax
	.p2align 4,,10
	.p2align 3
.L1711:
	movdqu	0(%r13,%rdx), %xmm1
	addq	$1, %rcx
	movups	%xmm1, (%rbx,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rax
	ja	.L1711
	leaq	32(%rbx,%rdi), %r14
	cmpq	%r8, %r12
	je	.L1712
.L1713:
	subq	%r12, %r8
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	leaq	-16(%r8), %rdi
	movq	%rdi, %rax
	shrq	$4, %rax
	addq	$1, %rax
	.p2align 4,,10
	.p2align 3
.L1715:
	movdqu	(%r12,%rdx), %xmm2
	addq	$1, %rcx
	movups	%xmm2, (%r14,%rdx)
	addq	$16, %rdx
	cmpq	%rax, %rcx
	jb	.L1715
	leaq	16(%r14,%rdi), %r14
.L1714:
	testq	%r13, %r13
	jne	.L1712
.L1716:
	movq	%rbx, %xmm0
	movq	%r14, %xmm3
	movq	%rsi, 16(%r15)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, (%r15)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1712:
	.cfi_restore_state
	movq	%r13, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1716
	.p2align 4,,10
	.p2align 3
.L1728:
	testq	%rdi, %rdi
	jne	.L1709
	movl	$16, %r14d
	xorl	%esi, %esi
	xorl	%ebx, %ebx
	jmp	.L1717
	.p2align 4,,10
	.p2align 3
.L1718:
	movl	$16, %r14d
	jmp	.L1708
	.p2align 4,,10
	.p2align 3
.L1710:
	cmpq	%r8, %r12
	jne	.L1713
	jmp	.L1714
.L1709:
	cmpq	%rsi, %rdi
	cmovbe	%rdi, %rsi
	salq	$4, %rsi
	movq	%rsi, %r14
	jmp	.L1708
.L1727:
	leaq	.LC7(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE27536:
	.size	_ZNSt6vectorISt4pairIN2v88internal6HandleINS2_12AccessorPairEEENS3_INS2_13NativeContextEEEESaIS8_EE17_M_realloc_insertIJS5_S7_EEEvN9__gnu_cxx17__normal_iteratorIPS8_SA_EEDpOT_, .-_ZNSt6vectorISt4pairIN2v88internal6HandleINS2_12AccessorPairEEENS3_INS2_13NativeContextEEEESaIS8_EE17_M_realloc_insertIJS5_S7_EEEvN9__gnu_cxx17__normal_iteratorIPS8_SA_EEDpOT_
	.section	.text._ZNSt8_Rb_treeIN2v88internal12AccessorPairES2_St9_IdentityIS2_ESt4lessIS2_ESaIS2_EE16_M_insert_uniqueIRKS2_EESt4pairISt17_Rb_tree_iteratorIS2_EbEOT_,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal12AccessorPairES2_St9_IdentityIS2_ESt4lessIS2_ESaIS2_EE16_M_insert_uniqueIRKS2_EESt4pairISt17_Rb_tree_iteratorIS2_EbEOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal12AccessorPairES2_St9_IdentityIS2_ESt4lessIS2_ESaIS2_EE16_M_insert_uniqueIRKS2_EESt4pairISt17_Rb_tree_iteratorIS2_EbEOT_
	.type	_ZNSt8_Rb_treeIN2v88internal12AccessorPairES2_St9_IdentityIS2_ESt4lessIS2_ESaIS2_EE16_M_insert_uniqueIRKS2_EESt4pairISt17_Rb_tree_iteratorIS2_EbEOT_, @function
_ZNSt8_Rb_treeIN2v88internal12AccessorPairES2_St9_IdentityIS2_ESt4lessIS2_ESaIS2_EE16_M_insert_uniqueIRKS2_EESt4pairISt17_Rb_tree_iteratorIS2_EbEOT_:
.LFB27539:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	8(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	16(%rdi), %r12
	testq	%r12, %r12
	je	.L1730
	movq	(%rsi), %r13
	jmp	.L1731
	.p2align 4,,10
	.p2align 3
.L1747:
	movq	16(%r12), %rax
	movl	$1, %edx
	testq	%rax, %rax
	je	.L1732
.L1748:
	movq	%rax, %r12
.L1731:
	movq	32(%r12), %rcx
	cmpq	%rcx, %r13
	jb	.L1747
	movq	24(%r12), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	jne	.L1748
.L1732:
	testb	%dl, %dl
	jne	.L1749
	cmpq	%rcx, %r13
	jbe	.L1740
.L1739:
	movl	$1, %r8d
	cmpq	%r12, %r15
	jne	.L1750
.L1737:
	movl	$40, %edi
	movl	%r8d, -52(%rbp)
	call	_Znwm@PLT
	movl	-52(%rbp), %r8d
	movq	%r12, %rdx
	movq	%r15, %rcx
	movq	%rax, %r13
	movq	(%r14), %rax
	movq	%r13, %rsi
	movl	%r8d, %edi
	movq	%rax, 32(%r13)
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 40(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	movl	$1, %edx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1749:
	.cfi_restore_state
	cmpq	24(%rbx), %r12
	je	.L1739
.L1741:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	cmpq	32(%rax), %r13
	ja	.L1739
	movq	%rax, %r12
.L1740:
	addq	$24, %rsp
	movq	%r12, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1750:
	.cfi_restore_state
	xorl	%r8d, %r8d
	cmpq	32(%r12), %r13
	setb	%r8b
	jmp	.L1737
	.p2align 4,,10
	.p2align 3
.L1730:
	movq	%r15, %r12
	cmpq	24(%rdi), %r15
	je	.L1743
	movq	(%rsi), %r13
	jmp	.L1741
	.p2align 4,,10
	.p2align 3
.L1743:
	movl	$1, %r8d
	jmp	.L1737
	.cfi_endproc
.LFE27539:
	.size	_ZNSt8_Rb_treeIN2v88internal12AccessorPairES2_St9_IdentityIS2_ESt4lessIS2_ESaIS2_EE16_M_insert_uniqueIRKS2_EESt4pairISt17_Rb_tree_iteratorIS2_EbEOT_, .-_ZNSt8_Rb_treeIN2v88internal12AccessorPairES2_St9_IdentityIS2_ESt4lessIS2_ESaIS2_EE16_M_insert_uniqueIRKS2_EESt4pairISt17_Rb_tree_iteratorIS2_EbEOT_
	.section	.rodata._ZN2v88internal5Debug27InstallDebugBreakTrampolineEv.str1.1,"aMS",@progbits,1
.LC8:
	.string	"(location_) != nullptr"
.LC9:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal5Debug27InstallDebugBreakTrampolineEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug27InstallDebugBreakTrampolineEv
	.type	_ZN2v88internal5Debug27InstallDebugBreakTrampolineEv, @function
_ZN2v88internal5Debug27InstallDebugBreakTrampolineEv:
.LFB22928:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$280, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	136(%rdi), %rax
	movq	41088(%rax), %rcx
	movq	%rax, -240(%rbp)
	movq	%rcx, -264(%rbp)
	movq	41096(%rax), %rcx
	movq	%rcx, -248(%rbp)
	movq	%rax, %rcx
	movl	41104(%rax), %eax
	leal	1(%rax), %edx
	movl	%edx, 41104(%rcx)
	movq	24(%rdi), %r15
	testq	%r15, %r15
	je	.L1752
	movq	%rdi, %r13
	xorl	%ebx, %ebx
	leaq	-112(%rbp), %r12
	jmp	.L1758
	.p2align 4,,10
	.p2align 3
.L1753:
	movq	8(%r15), %r15
	testq	%r15, %r15
	je	.L1922
.L1758:
	movq	(%r15), %rax
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	%rax, -112(%rbp)
	call	_ZNK2v88internal9DebugInfo15CanBreakAtEntryEv@PLT
	testb	%al, %al
	je	.L1753
	movq	(%r15), %rdx
	movq	(%rdx), %rdx
	movq	7(%rdx), %rdx
	movq	7(%rdx), %rdx
	testb	$1, %dl
	jne	.L1754
.L1755:
	movq	8(%r15), %r15
	movl	%eax, %ebx
	testq	%r15, %r15
	jne	.L1758
.L1922:
	xorl	%r14d, %r14d
	testb	%bl, %bl
	jne	.L1756
	movq	-240(%rbp), %rax
	movq	-264(%rbp), %rcx
	subl	$1, 41104(%rax)
	movq	%rcx, 41088(%rax)
	movq	-248(%rbp), %rcx
	cmpq	%rcx, 41096(%rax)
	je	.L1751
.L1921:
	movq	%rcx, 41096(%rax)
	movq	%rax, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1751:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1923
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1754:
	.cfi_restore_state
	movq	-1(%rdx), %rdx
	cmpw	$88, 11(%rdx)
	jne	.L1755
	movl	%eax, %r14d
.L1756:
	movq	136(%r13), %rax
	movl	$88, %esi
	leaq	-160(%rbp), %rbx
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	pxor	%xmm0, %xmm0
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%rax, -280(%rbp)
	leaq	-104(%rbp), %rax
	movq	%rax, -296(%rbp)
	movq	%rax, -88(%rbp)
	movq	%rax, -80(%rbp)
	movq	136(%r13), %rax
	movq	$0, -176(%rbp)
	leaq	37592(%rax), %rsi
	movl	$0, -104(%rbp)
	movq	$0, -96(%rbp)
	movq	$0, -72(%rbp)
	movaps	%xmm0, -192(%rbp)
	call	_ZN2v88internal18HeapObjectIteratorC1EPNS0_4HeapENS1_20HeapObjectsFilteringE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal18HeapObjectIterator4NextEv@PLT
	testq	%rax, %rax
	je	.L1846
	movq	$0, -288(%rbp)
	movq	%rax, %r15
	movq	$0, -232(%rbp)
	movq	$0, -256(%rbp)
	movq	%r12, -272(%rbp)
	jmp	.L1805
	.p2align 4,,10
	.p2align 3
.L1763:
	movq	(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	ja	.L1924
.L1762:
	movq	%rbx, %rdi
	call	_ZN2v88internal18HeapObjectIterator4NextEv@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1925
.L1805:
	leaq	-1(%r15), %rax
	testb	%r14b, %r14b
	je	.L1761
	movq	-1(%r15), %rdx
	cmpw	$155, 11(%rdx)
	je	.L1926
.L1761:
	movq	(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L1763
	movq	23(%r15), %rax
	movq	31(%rax), %rax
	testb	$1, %al
	je	.L1762
	movq	-1(%rax), %rcx
	cmpw	$86, 11(%rcx)
	jne	.L1762
	leaq	-200(%rbp), %rdi
	movq	%rax, -200(%rbp)
	call	_ZNK2v88internal9DebugInfo15CanBreakAtEntryEv@PLT
	testb	%al, %al
	je	.L1762
	movq	47(%r15), %rax
	leaq	47(%r15), %rsi
	cmpl	$67, 59(%rax)
	je	.L1765
	movabsq	$287762808832, %rdx
	movq	23(%r15), %rax
	movq	7(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1765
	testb	$1, %al
	jne	.L1927
.L1767:
	movq	-280(%rbp), %rax
	movq	(%rax), %rdx
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L1762
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L1762
	movq	%r15, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1762
.L1927:
	movq	-1(%rax), %rdx
	cmpw	$165, 11(%rdx)
	je	.L1765
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L1767
	.p2align 4,,10
	.p2align 3
.L1765:
	movq	136(%r13), %rcx
	movq	41112(%rcx), %rdi
	testq	%rdi, %rdi
	je	.L1771
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L1772:
	movq	-232(%rbp), %rax
	cmpq	%rax, -288(%rbp)
	je	.L1774
	movq	%rdx, (%rax)
	movq	%rbx, %rdi
	addq	$8, %rax
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal18HeapObjectIterator4NextEv@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L1805
	.p2align 4,,10
	.p2align 3
.L1925:
	movq	-272(%rbp), %r12
.L1760:
	movq	%rbx, %rdi
	call	_ZN2v88internal18HeapObjectIteratorD1Ev@PLT
	movq	-96(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L1809
.L1806:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal12AccessorPairES2_St9_IdentityIS2_ESt4lessIS2_ESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1806
.L1809:
	movq	-184(%rbp), %r14
	movq	-192(%rbp), %rbx
	cmpq	%rbx, %r14
	je	.L1808
	movq	%r12, -288(%rbp)
	jmp	.L1831
	.p2align 4,,10
	.p2align 3
.L1812:
	movq	15(%rax), %rsi
	testb	$1, %sil
	jne	.L1928
.L1822:
	addq	$16, %rbx
	cmpq	%rbx, %r14
	je	.L1929
.L1831:
	movq	(%rbx), %r15
	movq	8(%rbx), %r12
	movq	(%r15), %rax
	movq	7(%rax), %rsi
	testb	$1, %sil
	je	.L1812
	movq	-1(%rsi), %rdx
	cmpw	$88, 11(%rdx)
	jne	.L1812
	movq	136(%r13), %rcx
	movq	41112(%rcx), %rdi
	testq	%rdi, %rdi
	je	.L1814
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L1815:
	movq	136(%r13), %rdi
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	call	_ZN2v88internal10ApiNatives19InstantiateFunctionEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEENS4_INS0_20FunctionTemplateInfoEEENS0_11MaybeHandleINS0_4NameEEE@PLT
	testq	%rax, %rax
	je	.L1827
	movq	(%r15), %rdi
	movq	(%rax), %rdx
	movq	%rdx, 7(%rdi)
	leaq	7(%rdi), %rsi
	testb	$1, %dl
	je	.L1843
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -272(%rbp)
	testl	$262144, %eax
	je	.L1819
	movq	%rdx, -312(%rbp)
	movq	%rsi, -304(%rbp)
	movq	%rdi, -296(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-272(%rbp), %rcx
	movq	-312(%rbp), %rdx
	movq	-304(%rbp), %rsi
	movq	-296(%rbp), %rdi
	movq	8(%rcx), %rax
.L1819:
	testb	$24, %al
	je	.L1843
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1930
.L1843:
	movq	(%r15), %rax
.L1935:
	movq	15(%rax), %rsi
	testb	$1, %sil
	je	.L1822
	.p2align 4,,10
	.p2align 3
.L1928:
	movq	-1(%rsi), %rax
	cmpw	$88, 11(%rax)
	jne	.L1822
	movq	136(%r13), %rcx
	movq	%rsi, %r9
	movq	41112(%rcx), %rdi
	testq	%rdi, %rdi
	je	.L1824
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L1825:
	movq	136(%r13), %rdi
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	call	_ZN2v88internal10ApiNatives19InstantiateFunctionEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEENS4_INS0_20FunctionTemplateInfoEEENS0_11MaybeHandleINS0_4NameEEE@PLT
	testq	%rax, %rax
	je	.L1827
	movq	(%r15), %rdi
	movq	(%rax), %r15
	movq	%r15, 15(%rdi)
	leaq	15(%rdi), %rsi
	testb	$1, %r15b
	je	.L1822
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -272(%rbp)
	testl	$262144, %eax
	je	.L1829
	movq	%r15, %rdx
	movq	%rsi, -304(%rbp)
	movq	%rdi, -296(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-272(%rbp), %rcx
	movq	-304(%rbp), %rsi
	movq	-296(%rbp), %rdi
	movq	8(%rcx), %rax
.L1829:
	testb	$24, %al
	je	.L1822
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1822
	movq	%r15, %rdx
	addq	$16, %rbx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	cmpq	%rbx, %r14
	jne	.L1831
	.p2align 4,,10
	.p2align 3
.L1929:
	movq	-288(%rbp), %r12
.L1808:
	movq	-256(%rbp), %rax
	movq	%rax, %rbx
	cmpq	%rax, -232(%rbp)
	je	.L1811
	movq	-232(%rbp), %r13
	movq	-280(%rbp), %r14
	.p2align 4,,10
	.p2align 3
.L1836:
	movq	(%rbx), %r15
	movq	%r12, %rdx
	movl	$1, %esi
	movb	$0, -104(%rbp)
	movq	$0, -112(%rbp)
	movq	%r15, %rdi
	call	_ZN2v88internal8Compiler7CompileENS0_6HandleINS0_10JSFunctionEEENS1_18ClearExceptionFlagEPNS0_15IsCompiledScopeE@PLT
	movq	(%r15), %rdi
	movq	(%r14), %rdx
	movq	%rdx, 47(%rdi)
	leaq	47(%rdi), %rsi
	testb	$1, %dl
	je	.L1834
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L1834
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
.L1834:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jne	.L1836
.L1811:
	movq	-192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1833
	call	_ZdlPv@PLT
.L1833:
	movq	-256(%rbp), %rax
	testq	%rax, %rax
	je	.L1837
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L1837:
	movq	-240(%rbp), %rax
	movq	-264(%rbp), %rcx
	subl	$1, 41104(%rax)
	movq	%rcx, 41088(%rax)
	movq	-248(%rbp), %rcx
	cmpq	41096(%rax), %rcx
	jne	.L1921
	jmp	.L1751
	.p2align 4,,10
	.p2align 3
.L1814:
	movq	41088(%rcx), %rdx
	cmpq	41096(%rcx), %rdx
	je	.L1931
.L1816:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%rcx)
	movq	%rsi, (%rdx)
	jmp	.L1815
	.p2align 4,,10
	.p2align 3
.L1824:
	movq	41088(%rcx), %rdx
	cmpq	41096(%rcx), %rdx
	je	.L1932
.L1826:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%rcx)
	movq	%r9, (%rdx)
	jmp	.L1825
	.p2align 4,,10
	.p2align 3
.L1924:
	movq	%r15, -224(%rbp)
	movq	(%rax), %rax
	xorl	%r9d, %r9d
	movl	%r9d, %r12d
	movq	39(%rax), %r8
	addq	$31, %r8
	jmp	.L1788
	.p2align 4,,10
	.p2align 3
.L1790:
	addq	$24, %r8
.L1788:
	movq	-1(%r15), %rax
	movl	15(%rax), %eax
	shrl	$10, %eax
	andl	$1023, %eax
	cmpl	%eax, %r12d
	jge	.L1762
	movq	(%r8), %rax
	addl	$1, %r12d
	movabsq	$4294967296, %rcx
	testq	%rcx, %rax
	je	.L1790
	movq	8(%r8), %rax
	testb	$1, %al
	je	.L1790
	movq	-1(%rax), %rdx
	cmpw	$79, 11(%rdx)
	jne	.L1790
	movq	%rax, -216(%rbp)
	movq	7(%rax), %rdx
	testb	$1, %dl
	jne	.L1933
.L1792:
	movq	15(%rax), %rax
	testb	$1, %al
	je	.L1790
	movq	-1(%rax), %rax
	cmpw	$88, 11(%rax)
	jne	.L1790
.L1796:
	movq	-96(%rbp), %rax
	testq	%rax, %rax
	je	.L1794
	movq	-216(%rbp), %rdx
	movq	-296(%rbp), %rcx
	jmp	.L1797
.L1934:
	movq	%rax, %rcx
	movq	16(%rax), %rax
.L1800:
	testq	%rax, %rax
	je	.L1798
.L1797:
	cmpq	%rdx, 32(%rax)
	jnb	.L1934
	movq	24(%rax), %rax
	jmp	.L1800
	.p2align 4,,10
	.p2align 3
.L1926:
	movq	136(%r13), %rsi
	leaq	-200(%rbp), %rdi
	movq	%r15, -200(%rbp)
	call	_ZN2v88internal14FeedbackVector10ClearSlotsEPNS0_7IsolateE@PLT
	jmp	.L1762
	.p2align 4,,10
	.p2align 3
.L1827:
	leaq	.LC8(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1930:
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	(%r15), %rax
	jmp	.L1935
.L1771:
	movq	41088(%rcx), %rdx
	cmpq	41096(%rcx), %rdx
	je	.L1936
.L1773:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%rcx)
	movq	%r15, (%rdx)
	jmp	.L1772
.L1931:
	movq	%rcx, %rdi
	movq	%rsi, -296(%rbp)
	movq	%rcx, -272(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-296(%rbp), %rsi
	movq	-272(%rbp), %rcx
	movq	%rax, %rdx
	jmp	.L1816
.L1932:
	movq	%rcx, %rdi
	movq	%rcx, -272(%rbp)
	movq	%rsi, -296(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-296(%rbp), %r9
	movq	-272(%rbp), %rcx
	movq	%rax, %rdx
	jmp	.L1826
.L1846:
	movq	$0, -232(%rbp)
	movq	$0, -256(%rbp)
	jmp	.L1760
.L1752:
	movq	-240(%rbp), %rcx
	movl	%eax, 41104(%rcx)
	jmp	.L1751
.L1933:
	movq	-1(%rdx), %rdx
	cmpw	$88, 11(%rdx)
	jne	.L1792
	jmp	.L1796
.L1774:
	movq	-288(%rbp), %rsi
	subq	-256(%rbp), %rsi
	movabsq	$1152921504606846975, %rax
	movq	%rsi, %rcx
	sarq	$3, %rcx
	cmpq	%rax, %rcx
	je	.L1937
	testq	%rcx, %rcx
	je	.L1938
	leaq	(%rcx,%rcx), %rax
	cmpq	%rcx, %rax
	jb	.L1939
	testq	%rax, %rax
	jne	.L1940
	movq	$0, -288(%rbp)
	movl	$8, %eax
	xorl	%r15d, %r15d
.L1779:
	movq	-256(%rbp), %rdi
	movq	-232(%rbp), %rcx
	movq	%rdx, (%r15,%rsi)
	cmpq	%rcx, %rdi
	je	.L1849
	subq	$8, %rcx
	leaq	15(%rdi), %rax
	subq	%rdi, %rcx
	subq	%r15, %rax
	movq	%rcx, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rax
	jbe	.L1850
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rsi
	je	.L1850
	addq	$1, %rsi
	xorl	%eax, %eax
	movq	%rsi, %rdx
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L1782:
	movdqu	(%rdi,%rax), %xmm1
	movups	%xmm1, (%r15,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L1782
	movq	%rsi, %rdi
	movq	-256(%rbp), %rax
	andq	$-2, %rdi
	leaq	0(,%rdi,8), %rdx
	addq	%rdx, %rax
	addq	%r15, %rdx
	cmpq	%rsi, %rdi
	je	.L1784
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L1784:
	leaq	16(%r15,%rcx), %rax
	movq	%rax, -232(%rbp)
.L1780:
	movq	-256(%rbp), %rax
	testq	%rax, %rax
	je	.L1785
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L1785:
	movq	%r15, -256(%rbp)
	jmp	.L1762
.L1938:
	movl	$8, %ecx
.L1777:
	movq	%rcx, %rdi
	movq	%rsi, -312(%rbp)
	movq	%rdx, -304(%rbp)
	movq	%rcx, -288(%rbp)
	call	_Znwm@PLT
	movq	-288(%rbp), %rcx
	movq	-304(%rbp), %rdx
	movq	%rax, %r15
	movq	-312(%rbp), %rsi
	leaq	(%rax,%rcx), %rax
	movq	%rax, -288(%rbp)
	leaq	8(%r15), %rax
	jmp	.L1779
.L1798:
	cmpq	-296(%rbp), %rcx
	je	.L1794
	cmpq	32(%rcx), %rdx
	jnb	.L1790
.L1794:
	leaq	-224(%rbp), %rdi
	movq	%r8, -304(%rbp)
	call	_ZN2v88internal10JSReceiver18GetCreationContextEv@PLT
	movq	136(%r13), %rcx
	movq	-216(%rbp), %r15
	movq	%rax, -200(%rbp)
	movq	-304(%rbp), %r8
	movq	41112(%rcx), %rdi
	testq	%rdi, %rdi
	je	.L1941
	movq	%r15, %rsi
	movq	%r8, -304(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-304(%rbp), %r8
.L1801:
	movq	%rax, -208(%rbp)
	movq	-184(%rbp), %rsi
	cmpq	-176(%rbp), %rsi
	je	.L1803
	movq	-200(%rbp), %rdx
	movq	%rax, (%rsi)
	movq	%rdx, 8(%rsi)
	addq	$16, -184(%rbp)
.L1804:
	movq	-272(%rbp), %rdi
	leaq	-216(%rbp), %rsi
	movq	%r8, -304(%rbp)
	call	_ZNSt8_Rb_treeIN2v88internal12AccessorPairES2_St9_IdentityIS2_ESt4lessIS2_ESaIS2_EE16_M_insert_uniqueIRKS2_EESt4pairISt17_Rb_tree_iteratorIS2_EbEOT_
	movq	-224(%rbp), %r15
	movq	-304(%rbp), %r8
	jmp	.L1790
.L1936:
	movq	%rcx, %rdi
	movq	%rcx, -304(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-304(%rbp), %rcx
	movq	%rax, %rdx
	jmp	.L1773
.L1939:
	movabsq	$9223372036854775800, %rcx
	jmp	.L1777
.L1850:
	movq	-256(%rbp), %rax
	movq	-232(%rbp), %rdi
	movq	%r15, %rdx
.L1781:
	movq	(%rax), %rsi
	addq	$8, %rax
	addq	$8, %rdx
	movq	%rsi, -8(%rdx)
	cmpq	%rax, %rdi
	jne	.L1781
	jmp	.L1784
.L1849:
	movq	%rax, -232(%rbp)
	jmp	.L1780
.L1940:
	movabsq	$1152921504606846975, %rcx
	cmpq	%rcx, %rax
	cmova	%rcx, %rax
	leaq	0(,%rax,8), %rcx
	jmp	.L1777
.L1941:
	movq	41088(%rcx), %rax
	cmpq	41096(%rcx), %rax
	je	.L1942
.L1802:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rcx)
	movq	%r15, (%rax)
	jmp	.L1801
.L1803:
	leaq	-200(%rbp), %rcx
	leaq	-208(%rbp), %rdx
	movq	%r8, -304(%rbp)
	leaq	-192(%rbp), %rdi
	call	_ZNSt6vectorISt4pairIN2v88internal6HandleINS2_12AccessorPairEEENS3_INS2_13NativeContextEEEESaIS8_EE17_M_realloc_insertIJS5_S7_EEEvN9__gnu_cxx17__normal_iteratorIPS8_SA_EEDpOT_
	movq	-304(%rbp), %r8
	jmp	.L1804
.L1942:
	movq	%rcx, %rdi
	movq	%r8, -312(%rbp)
	movq	%rcx, -304(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-312(%rbp), %r8
	movq	-304(%rbp), %rcx
	jmp	.L1802
.L1923:
	call	__stack_chk_fail@PLT
.L1937:
	leaq	.LC7(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE22928:
	.size	_ZN2v88internal5Debug27InstallDebugBreakTrampolineEv, .-_ZN2v88internal5Debug27InstallDebugBreakTrampolineEv
	.section	.text._ZN2v88internal5Debug32PrepareFunctionForDebugExecutionENS0_6HandleINS0_18SharedFunctionInfoEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug32PrepareFunctionForDebugExecutionENS0_6HandleINS0_18SharedFunctionInfoEEE
	.type	_ZN2v88internal5Debug32PrepareFunctionForDebugExecutionENS0_6HandleINS0_18SharedFunctionInfoEEE, @function
_ZN2v88internal5Debug32PrepareFunctionForDebugExecutionENS0_6HandleINS0_18SharedFunctionInfoEEE:
.LFB22927:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal5Debug20GetOrCreateDebugInfoENS0_6HandleINS0_18SharedFunctionInfoEEE
	movq	%rax, %rbx
	movq	(%rax), %rax
	testb	$2, 59(%rax)
	je	.L2003
.L1943:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2004
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2003:
	.cfi_restore_state
	movq	136(%r12), %rax
	leaq	88(%rax), %r14
	movq	0(%r13), %rax
	movq	7(%rax), %rdx
	testb	$1, %dl
	jne	.L2005
.L1945:
	movq	7(%rax), %rax
	testb	$1, %al
	jne	.L2006
.L1950:
	movq	(%rbx), %r15
	movq	(%r14), %r14
	movq	%r14, 31(%r15)
	leaq	31(%r15), %rsi
	testb	$1, %r14b
	je	.L1972
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -88(%rbp)
	testl	$262144, %eax
	jne	.L2007
.L1968:
	testb	$24, %al
	je	.L1972
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2008
	.p2align 4,,10
	.p2align 3
.L1972:
	movq	(%rbx), %rax
	leaq	-80(%rbp), %r14
	movq	%r14, %rdi
	movq	%rax, -80(%rbp)
	call	_ZNK2v88internal9DebugInfo15CanBreakAtEntryEv@PLT
	testb	%al, %al
	je	.L1970
	movq	136(%r12), %rdi
	call	_ZN2v88internal11Deoptimizer13DeoptimizeAllEPNS0_7IsolateE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal5Debug27InstallDebugBreakTrampolineEv
.L1971:
	movq	(%rbx), %rdx
	movslq	59(%rdx), %rax
	orl	$2, %eax
	salq	$32, %rax
	movq	%rax, 55(%rdx)
	jmp	.L1943
	.p2align 4,,10
	.p2align 3
.L1970:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal5Debug18DeoptimizeFunctionENS0_6HandleINS0_18SharedFunctionInfoEEE
	movq	0(%r13), %rsi
	movq	%r14, %rdi
	movl	$1, %edx
	call	_ZN2v88internal23RedirectActiveFunctionsC1ENS0_18SharedFunctionInfoENS1_4ModeE@PLT
	movq	136(%r12), %rsi
	movq	%r14, %rdi
	leaq	12448(%rsi), %rdx
	call	_ZN2v88internal23RedirectActiveFunctions11VisitThreadEPNS0_7IsolateEPNS0_14ThreadLocalTopE@PLT
	movq	136(%r12), %rax
	movq	%r14, %rsi
	movq	41168(%rax), %rdi
	call	_ZN2v88internal13ThreadManager22IterateArchivedThreadsEPNS0_13ThreadVisitorE@PLT
	jmp	.L1971
	.p2align 4,,10
	.p2align 3
.L2007:
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rcx
	movq	-96(%rbp), %rsi
	movq	8(%rcx), %rax
	jmp	.L1968
	.p2align 4,,10
	.p2align 3
.L2008:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1972
	.p2align 4,,10
	.p2align 3
.L2005:
	movq	-1(%rdx), %rdx
	cmpw	$72, 11(%rdx)
	jne	.L1945
.L1949:
	movq	0(%r13), %rax
	movq	136(%r12), %r15
	movq	31(%rax), %rdx
	testb	$1, %dl
	jne	.L2009
.L1946:
	movq	7(%rax), %rdx
	testb	$1, %dl
	jne	.L2010
.L1952:
	movq	7(%rax), %rax
	movq	7(%rax), %rsi
.L1951:
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1953
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L1954:
	movq	136(%r12), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal7Factory17CopyBytecodeArrayENS0_6HandleINS0_13BytecodeArrayEEE@PLT
	movq	(%rbx), %rdi
	movq	(%rax), %rdx
	movq	%rax, %r15
	leaq	39(%rdi), %rsi
	movq	%rdx, 39(%rdi)
	testb	$1, %dl
	je	.L1975
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -88(%rbp)
	testl	$262144, %eax
	je	.L1957
	movq	%rdx, -112(%rbp)
	movq	%rsi, -104(%rbp)
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rcx
	movq	-112(%rbp), %rdx
	movq	-104(%rbp), %rsi
	movq	-96(%rbp), %rdi
	movq	8(%rcx), %rax
.L1957:
	testb	$24, %al
	je	.L1975
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2011
	.p2align 4,,10
	.p2align 3
.L1975:
	movq	(%r15), %r8
	movq	0(%r13), %rdi
	movq	%r8, %rax
	movq	7(%rdi), %rdx
	leaq	7(%rdi), %r15
	notq	%rax
	andl	$1, %eax
	testb	$1, %dl
	jne	.L2012
.L1959:
	movq	7(%rdi), %r15
	movq	%r8, 7(%r15)
	leaq	7(%r15), %rsi
	testb	%al, %al
	jne	.L1950
	movq	%r8, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -88(%rbp)
	testl	$262144, %eax
	jne	.L2013
.L1965:
	testb	$24, %al
	je	.L1950
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1950
	movq	%r8, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1950
	.p2align 4,,10
	.p2align 3
.L2006:
	movq	-1(%rax), %rax
	cmpw	$91, 11(%rax)
	jne	.L1950
	jmp	.L1949
	.p2align 4,,10
	.p2align 3
.L1953:
	movq	41088(%r15), %r14
	cmpq	41096(%r15), %r14
	je	.L2014
.L1955:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r14)
	jmp	.L1954
	.p2align 4,,10
	.p2align 3
.L2012:
	movq	-1(%rdx), %rdx
	cmpw	$72, 11(%rdx)
	jne	.L1959
	movq	%r8, 7(%rdi)
	testb	%al, %al
	jne	.L1950
	movq	%r8, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -88(%rbp)
	testl	$262144, %eax
	je	.L1961
	movq	%r8, %rdx
	movq	%r15, %rsi
	movq	%r8, -104(%rbp)
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rcx
	movq	-104(%rbp), %r8
	movq	-96(%rbp), %rdi
	movq	8(%rcx), %rax
.L1961:
	testb	$24, %al
	je	.L1950
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1950
	movq	%r8, %rdx
	movq	%r15, %rsi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1950
	.p2align 4,,10
	.p2align 3
.L2009:
	movq	-1(%rdx), %rcx
	cmpw	$86, 11(%rcx)
	jne	.L1946
	movq	39(%rdx), %rcx
	testb	$1, %cl
	je	.L1946
	movq	-1(%rcx), %rcx
	cmpw	$72, 11(%rcx)
	jne	.L1946
	movq	31(%rdx), %rsi
	jmp	.L1951
	.p2align 4,,10
	.p2align 3
.L2013:
	movq	%r8, %rdx
	movq	%r15, %rdi
	movq	%r8, -104(%rbp)
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rcx
	movq	-104(%rbp), %r8
	movq	-96(%rbp), %rsi
	movq	8(%rcx), %rax
	jmp	.L1965
	.p2align 4,,10
	.p2align 3
.L2011:
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1975
	.p2align 4,,10
	.p2align 3
.L2010:
	movq	-1(%rdx), %rdx
	cmpw	$72, 11(%rdx)
	jne	.L1952
	movq	7(%rax), %rsi
	jmp	.L1951
	.p2align 4,,10
	.p2align 3
.L2014:
	movq	%r15, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L1955
.L2004:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22927:
	.size	_ZN2v88internal5Debug32PrepareFunctionForDebugExecutionENS0_6HandleINS0_18SharedFunctionInfoEEE, .-_ZN2v88internal5Debug32PrepareFunctionForDebugExecutionENS0_6HandleINS0_18SharedFunctionInfoEEE
	.section	.text._ZN2v88internal5Debug13SetBreakpointENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_10BreakPointEEEPi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug13SetBreakpointENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_10BreakPointEEEPi
	.type	_ZN2v88internal5Debug13SetBreakpointENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_10BreakPointEEEPi, @function
_ZN2v88internal5Debug13SetBreakpointENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_10BreakPointEEEPi:
.LFB22866:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$168, %rsp
	movq	%rdx, -184(%rbp)
	movq	136(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	41088(%r13), %rax
	addl	$1, 41104(%r13)
	movq	%rax, -176(%rbp)
	movq	41096(%r13), %rax
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal5Debug15EnsureBreakInfoENS0_6HandleINS0_18SharedFunctionInfoEEE
	movl	%eax, %r12d
	testb	%al, %al
	jne	.L2029
.L2016:
	movq	-176(%rbp), %rax
	subl	$1, 41104(%r13)
	movq	%rax, 41088(%r13)
	movq	-168(%rbp), %rax
	cmpq	41096(%r13), %rax
	je	.L2015
	movq	%rax, 41096(%r13)
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2015:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2030
	addq	$168, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2029:
	.cfi_restore_state
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal5Debug32PrepareFunctionForDebugExecutionENS0_6HandleINS0_18SharedFunctionInfoEEE
	movq	136(%r15), %rdx
	movq	(%r14), %rax
	movq	41112(%rdx), %rdi
	movq	31(%rax), %rsi
	testq	%rdi, %rdi
	je	.L2017
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r14
.L2018:
	movl	(%rbx), %eax
	leaq	-160(%rbp), %rdi
	movq	%rsi, -160(%rbp)
	movq	%rdi, -200(%rbp)
	movl	%eax, -192(%rbp)
	call	_ZNK2v88internal9DebugInfo15CanBreakAtEntryEv@PLT
	xorl	%edx, %edx
	testb	%al, %al
	jne	.L2020
	movq	-200(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal13BreakIteratorC1ENS0_6HandleINS0_9DebugInfoEEE
	movl	-192(%rbp), %esi
	movq	-200(%rbp), %rdi
	call	_ZN2v88internal13BreakIterator14SkipToPositionEi
	movl	-148(%rbp), %edx
.L2020:
	movl	%edx, (%rbx)
	movq	-184(%rbp), %rcx
	movq	%r14, %rsi
	movq	136(%r15), %rdi
	call	_ZN2v88internal9DebugInfo13SetBreakPointEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_10BreakPointEEE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal5Debug16ClearBreakPointsENS0_6HandleINS0_9DebugInfoEEE
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal5Debug16ApplyBreakPointsENS0_6HandleINS0_9DebugInfoEEE
	testb	$4, 56(%r15)
	jne	.L2016
	movq	48(%r15), %rax
	movl	$2, %esi
	movq	40960(%rax), %rdi
	addq	$248, %rdi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	orl	$4, 56(%r15)
	jmp	.L2016
	.p2align 4,,10
	.p2align 3
.L2017:
	movq	41088(%rdx), %r14
	cmpq	41096(%rdx), %r14
	je	.L2031
.L2019:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%r14)
	jmp	.L2018
	.p2align 4,,10
	.p2align 3
.L2031:
	movq	%rdx, %rdi
	movq	%rsi, -200(%rbp)
	movq	%rdx, -192(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-200(%rbp), %rsi
	movq	-192(%rbp), %rdx
	movq	%rax, %r14
	jmp	.L2019
.L2030:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22866:
	.size	_ZN2v88internal5Debug13SetBreakpointENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_10BreakPointEEEPi, .-_ZN2v88internal5Debug13SetBreakpointENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_10BreakPointEEEPi
	.section	.text._ZN2v88internal5Debug22SetBreakPointForScriptENS0_6HandleINS0_6ScriptEEENS2_INS0_6StringEEEPiS7_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug22SetBreakPointForScriptENS0_6HandleINS0_6ScriptEEENS2_INS0_6StringEEEPiS7_
	.type	_ZN2v88internal5Debug22SetBreakPointForScriptENS0_6HandleINS0_6ScriptEEENS2_INS0_6StringEEEPiS7_, @function
_ZN2v88internal5Debug22SetBreakPointForScriptENS0_6HandleINS0_6ScriptEEENS2_INS0_6StringEEEPiS7_:
.LFB22867:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	128(%rdi), %eax
	leal	1(%rax), %esi
	movl	%esi, 128(%rdi)
	movl	%esi, (%r8)
	movq	136(%rdi), %rdi
	call	_ZN2v88internal7Factory13NewBreakPointEiNS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %r13
	movq	(%r15), %rax
	cmpl	$3, 51(%rax)
	jne	.L2033
	movq	136(%rbx), %rbx
	movq	71(%rax), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2034
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdi
.L2035:
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal16WasmModuleObject13SetBreakPointENS0_6HandleIS1_EEPiNS2_INS0_10BreakPointEEE@PLT
	movl	%eax, %r15d
.L2032:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2051
	addq	$56, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2034:
	.cfi_restore_state
	movq	41088(%rbx), %rdi
	cmpq	41096(%rbx), %rdi
	je	.L2052
.L2036:
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rdi)
	jmp	.L2035
	.p2align 4,,10
	.p2align 3
.L2033:
	movq	136(%rbx), %r14
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	41088(%r14), %rax
	addl	$1, 41104(%r14)
	movl	(%r12), %edx
	movq	%rax, -80(%rbp)
	movq	41096(%r14), %rax
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal5Debug30FindSharedFunctionInfoInScriptENS0_6HandleINS0_6ScriptEEEi
	movq	%rax, %rdx
	movq	136(%rbx), %rax
	movq	(%rdx), %rcx
	cmpq	%rcx, 88(%rax)
	je	.L2038
	movq	%rdx, %rsi
	movq	%rbx, %rdi
	movq	%rdx, -88(%rbp)
	call	_ZN2v88internal5Debug15EnsureBreakInfoENS0_6HandleINS0_18SharedFunctionInfoEEE
	movl	%eax, %r15d
	testb	%al, %al
	jne	.L2053
.L2038:
	xorl	%r15d, %r15d
.L2043:
	movq	-80(%rbp), %rax
	subl	$1, 41104(%r14)
	movq	%rax, 41088(%r14)
	movq	-72(%rbp), %rax
	cmpq	41096(%r14), %rax
	je	.L2032
	movq	%rax, 41096(%r14)
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	jmp	.L2032
	.p2align 4,,10
	.p2align 3
.L2053:
	movq	-88(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%rdx, %rsi
	call	_ZN2v88internal5Debug32PrepareFunctionForDebugExecutionENS0_6HandleINS0_18SharedFunctionInfoEEE
	movq	-88(%rbp), %rdx
	leaq	-64(%rbp), %rdi
	movq	%rdi, -88(%rbp)
	movq	(%rdx), %rax
	movq	%rdx, -96(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	cmpl	(%r12), %eax
	movq	-88(%rbp), %rdi
	movq	-96(%rbp), %rdx
	jg	.L2054
.L2039:
	movq	136(%rbx), %rcx
	movq	(%rdx), %rax
	movq	41112(%rcx), %rdi
	movq	31(%rax), %r10
	testq	%rdi, %rdi
	je	.L2040
	movq	%r10, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L2041:
	movl	(%r12), %edx
	movq	%rbx, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal5Debug21FindBreakablePositionENS0_6HandleINS0_9DebugInfoEEEi
	movl	%eax, %edx
	cmpl	%eax, (%r12)
	jg	.L2038
	movl	%eax, (%r12)
	movq	-88(%rbp), %rsi
	movq	%r13, %rcx
	movq	136(%rbx), %rdi
	call	_ZN2v88internal9DebugInfo13SetBreakPointEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_10BreakPointEEE@PLT
	movq	-88(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal5Debug16ClearBreakPointsENS0_6HandleINS0_9DebugInfoEEE
	movq	-88(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal5Debug16ApplyBreakPointsENS0_6HandleINS0_9DebugInfoEEE
	testb	$4, 56(%rbx)
	jne	.L2043
	movq	48(%rbx), %rax
	movl	$2, %esi
	movq	40960(%rax), %rdi
	addq	$248, %rdi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	orl	$4, 56(%rbx)
	jmp	.L2043
	.p2align 4,,10
	.p2align 3
.L2040:
	movq	41088(%rcx), %rsi
	cmpq	41096(%rcx), %rsi
	je	.L2055
.L2042:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rcx)
	movq	%r10, (%rsi)
	jmp	.L2041
	.p2align 4,,10
	.p2align 3
.L2054:
	movq	(%rdx), %rax
	movq	%rdx, -88(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	movq	-88(%rbp), %rdx
	movl	%eax, (%r12)
	jmp	.L2039
	.p2align 4,,10
	.p2align 3
.L2052:
	movq	%rbx, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %rdi
	jmp	.L2036
	.p2align 4,,10
	.p2align 3
.L2055:
	movq	%rcx, %rdi
	movq	%r10, -96(%rbp)
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %r10
	movq	-88(%rbp), %rcx
	movq	%rax, %rsi
	jmp	.L2042
.L2051:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22867:
	.size	_ZN2v88internal5Debug22SetBreakPointForScriptENS0_6HandleINS0_6ScriptEEENS2_INS0_6StringEEEPiS7_, .-_ZN2v88internal5Debug22SetBreakPointForScriptENS0_6HandleINS0_6ScriptEEENS2_INS0_6StringEEEPiS7_
	.section	.text._ZN2v88internal5Debug16FloodWithOneShotENS0_6HandleINS0_18SharedFunctionInfoEEEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug16FloodWithOneShotENS0_6HandleINS0_18SharedFunctionInfoEEEb
	.type	_ZN2v88internal5Debug16FloodWithOneShotENS0_6HandleINS0_18SharedFunctionInfoEEEb, @function
_ZN2v88internal5Debug16FloodWithOneShotENS0_6HandleINS0_18SharedFunctionInfoEEEb:
.LFB22880:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$144, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal5Debug12IsBlackboxedENS0_6HandleINS0_18SharedFunctionInfoEEE
	testb	%al, %al
	je	.L2081
.L2056:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2082
	addq	$144, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2081:
	.cfi_restore_state
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal5Debug15EnsureBreakInfoENS0_6HandleINS0_18SharedFunctionInfoEEE
	testb	%al, %al
	je	.L2056
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal5Debug32PrepareFunctionForDebugExecutionENS0_6HandleINS0_18SharedFunctionInfoEEE
	movq	136(%r12), %r12
	movq	(%rbx), %rax
	movq	41112(%r12), %rdi
	movq	31(%rax), %r14
	testq	%rdi, %rdi
	je	.L2058
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L2059:
	leaq	-144(%rbp), %rbx
	movq	%rbx, %rdi
	call	_ZN2v88internal13BreakIteratorC1ENS0_6HandleINS0_9DebugInfoEEE
	cmpl	$-1, -96(%rbp)
	je	.L2056
	leaq	-176(%rbp), %r12
	testb	%r13b, %r13b
	jne	.L2062
	.p2align 4,,10
	.p2align 3
.L2066:
	movq	-144(%rbp), %rax
	movq	(%rax), %rax
	movq	31(%rax), %rcx
	movl	-88(%rbp), %eax
	leal	54(%rax), %edx
	movslq	%edx, %rdx
	movzbl	-1(%rcx,%rdx), %edx
	cmpb	$3, %dl
	ja	.L2063
	addl	$55, %eax
	cltq
	movzbl	-1(%rcx,%rax), %edx
.L2063:
	movq	%rbx, %rdi
	cmpb	$-77, %dl
	je	.L2064
	call	_ZN2v88internal13BreakIterator13SetDebugBreakEv.part.0
	movq	%rbx, %rdi
	call	_ZN2v88internal13BreakIterator4NextEv
	cmpl	$-1, -96(%rbp)
	jne	.L2066
	jmp	.L2056
	.p2align 4,,10
	.p2align 3
.L2062:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal13BreakIterator16GetBreakLocationEv
	cmpl	$3, -164(%rbp)
	jle	.L2068
	movq	-144(%rbp), %rax
	movq	(%rax), %rax
	movq	31(%rax), %rcx
	movl	-88(%rbp), %eax
	leal	54(%rax), %edx
	movslq	%edx, %rdx
	movzbl	-1(%rcx,%rdx), %edx
	cmpb	$3, %dl
	ja	.L2069
	addl	$55, %eax
	cltq
	movzbl	-1(%rcx,%rax), %edx
.L2069:
	cmpb	$-77, %dl
	je	.L2068
	movq	%rbx, %rdi
	call	_ZN2v88internal13BreakIterator13SetDebugBreakEv.part.0
.L2068:
	movq	%rbx, %rdi
	call	_ZN2v88internal13BreakIterator4NextEv
	cmpl	$-1, -96(%rbp)
	jne	.L2062
	jmp	.L2056
	.p2align 4,,10
	.p2align 3
.L2058:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L2083
.L2060:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L2059
	.p2align 4,,10
	.p2align 3
.L2064:
	call	_ZN2v88internal13BreakIterator4NextEv
	cmpl	$-1, -96(%rbp)
	jne	.L2066
	jmp	.L2056
.L2083:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L2060
.L2082:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22880:
	.size	_ZN2v88internal5Debug16FloodWithOneShotENS0_6HandleINS0_18SharedFunctionInfoEEEb, .-_ZN2v88internal5Debug16FloodWithOneShotENS0_6HandleINS0_18SharedFunctionInfoEEEb
	.section	.rodata._ZN2v88internal5Debug13PrepareStepInENS0_6HandleINS0_10JSFunctionEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC10:
	.string	"last_step_action() >= StepIn || break_on_next_function_call()"
	.section	.text._ZN2v88internal5Debug13PrepareStepInENS0_6HandleINS0_10JSFunctionEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug13PrepareStepInENS0_6HandleINS0_10JSFunctionEEE
	.type	_ZN2v88internal5Debug13PrepareStepInENS0_6HandleINS0_10JSFunctionEEE, @function
_ZN2v88internal5Debug13PrepareStepInENS0_6HandleINS0_10JSFunctionEEE:
.LFB22886:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$1, 76(%rdi)
	movq	%rsi, %rbx
	jle	.L2094
.L2085:
	cmpb	$0, 10(%r12)
	jne	.L2084
	cmpb	$0, 8(%r12)
	je	.L2084
	movq	136(%r12), %r13
	cmpl	$32, 41828(%r13)
	je	.L2084
	movq	64(%r12), %rax
	testq	%rax, %rax
	jne	.L2084
	cmpb	$0, 12(%r12)
	jne	.L2084
	movq	(%rbx), %rax
	movq	23(%rax), %r14
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2087
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L2088:
	movq	%r12, %rdi
	call	_ZN2v88internal5Debug12IsBlackboxedENS0_6HandleINS0_18SharedFunctionInfoEEE
	testb	%al, %al
	jne	.L2084
	movq	80(%r12), %rax
	cmpq	%rax, (%rbx)
	je	.L2084
	movq	$0, 80(%r12)
	movq	136(%r12), %r13
	movq	(%rbx), %rax
	movq	23(%rax), %r14
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2090
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L2091:
	popq	%rbx
	movq	%r12, %rdi
	xorl	%edx, %edx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal5Debug16FloodWithOneShotENS0_6HandleINS0_18SharedFunctionInfoEEEb
	.p2align 4,,10
	.p2align 3
.L2084:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2094:
	.cfi_restore_state
	cmpb	$0, 132(%rdi)
	jne	.L2085
	leaq	.LC10(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2087:
	movq	41088(%r13), %rsi
	cmpq	%rsi, 41096(%r13)
	je	.L2095
.L2089:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r13)
	movq	%r14, (%rsi)
	jmp	.L2088
	.p2align 4,,10
	.p2align 3
.L2090:
	movq	41088(%r13), %rsi
	cmpq	41096(%r13), %rsi
	je	.L2096
.L2092:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r13)
	movq	%r14, (%rsi)
	jmp	.L2091
.L2095:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L2089
.L2096:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L2092
	.cfi_endproc
.LFE22886:
	.size	_ZN2v88internal5Debug13PrepareStepInENS0_6HandleINS0_10JSFunctionEEE, .-_ZN2v88internal5Debug13PrepareStepInENS0_6HandleINS0_10JSFunctionEEE
	.section	.rodata._ZN2v88internal5Debug31PrepareStepInSuspendedGeneratorEv.str1.1,"aMS",@progbits,1
.LC11:
	.string	"has_suspended_generator()"
	.section	.text._ZN2v88internal5Debug31PrepareStepInSuspendedGeneratorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug31PrepareStepInSuspendedGeneratorEv
	.type	_ZN2v88internal5Debug31PrepareStepInSuspendedGeneratorEv, @function
_ZN2v88internal5Debug31PrepareStepInSuspendedGeneratorEv:
.LFB22887:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	112(%rdi), %rax
	cmpq	_ZN2v88internal3Smi5kZeroE(%rip), %rax
	je	.L2109
	cmpb	$0, 10(%rdi)
	movq	%rdi, %rbx
	jne	.L2097
	cmpb	$0, 8(%rdi)
	je	.L2097
	movq	136(%rdi), %r12
	cmpl	$32, 41828(%r12)
	je	.L2097
	movq	64(%rdi), %rdx
	testq	%rdx, %rdx
	jne	.L2097
	cmpb	$0, 12(%rdi)
	jne	.L2097
	movb	$2, 76(%rdi)
	movb	$1, 9(%rdi)
	movq	41112(%r12), %rdi
	movq	23(%rax), %rsi
	testq	%rdi, %rdi
	je	.L2102
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L2103:
	movq	136(%rbx), %r12
	movq	23(%rsi), %r13
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2105
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L2106:
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal5Debug16FloodWithOneShotENS0_6HandleINS0_18SharedFunctionInfoEEEb
	movq	$0, 112(%rbx)
.L2097:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2109:
	.cfi_restore_state
	leaq	.LC11(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2102:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L2110
.L2104:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L2103
	.p2align 4,,10
	.p2align 3
.L2105:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L2111
.L2107:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, (%rsi)
	jmp	.L2106
.L2110:
	movq	%r12, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L2104
.L2111:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L2107
	.cfi_endproc
.LFE22887:
	.size	_ZN2v88internal5Debug31PrepareStepInSuspendedGeneratorEv, .-_ZN2v88internal5Debug31PrepareStepInSuspendedGeneratorEv
	.section	.rodata._ZN2v88internal5Debug18PrepareStepOnThrowEv.str1.8,"aMS",@progbits,1
	.align 8
.LC12:
	.string	"AbstractCode::INTERPRETED_FUNCTION == code->kind()"
	.section	.text._ZN2v88internal5Debug18PrepareStepOnThrowEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug18PrepareStepOnThrowEv
	.type	_ZN2v88internal5Debug18PrepareStepOnThrowEv, @function
_ZN2v88internal5Debug18PrepareStepOnThrowEv:
.LFB22888:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$1560, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$-1, 76(%rdi)
	je	.L2112
	cmpb	$0, 10(%rdi)
	movq	%rdi, %r14
	je	.L2181
.L2112:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2182
	addq	$1560, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2181:
	.cfi_restore_state
	movzbl	8(%rdi), %r12d
	testb	%r12b, %r12b
	je	.L2112
	movq	136(%rdi), %rax
	cmpl	$32, 41828(%rax)
	je	.L2112
	movq	64(%rdi), %rax
	testq	%rax, %rax
	jne	.L2112
	movzbl	12(%rdi), %eax
	movb	%al, -1576(%rbp)
	testb	%al, %al
	jne	.L2112
	movq	24(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L2117
	.p2align 4,,10
	.p2align 3
.L2114:
	movq	(%rbx), %r13
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal5Debug16ClearBreakPointsENS0_6HandleINS0_9DebugInfoEEE
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal5Debug16ApplyBreakPointsENS0_6HandleINS0_9DebugInfoEEE
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L2114
.L2117:
	movq	%r14, %rdi
	leaq	-1504(%rbp), %r13
	call	_ZN2v88internal5Debug17CurrentFrameCountEv
	movq	136(%r14), %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateE@PLT
	cmpq	$0, -88(%rbp)
	je	.L2112
	movq	%r13, %rdi
	call	_ZN2v88internal23JavaScriptFrameIterator7AdvanceEv@PLT
	.p2align 4,,10
	.p2align 3
.L2179:
	movq	-88(%rbp), %r15
	testq	%r15, %r15
	je	.L2112
.L2121:
	movq	(%r15), %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	*168(%rax)
	testl	%eax, %eax
	jg	.L2118
	pxor	%xmm1, %xmm1
	leaq	-1536(%rbp), %r10
	movq	%r15, %rdi
	movq	$0, -1520(%rbp)
	movaps	%xmm1, -1536(%rbp)
	movq	(%r15), %rax
	movq	%r10, %rsi
	call	*160(%rax)
	movq	-1528(%rbp), %rax
	movq	%r13, %rdi
	subq	-1536(%rbp), %rax
	sarq	$3, %rax
	subl	%eax, %ebx
	call	_ZN2v88internal23JavaScriptFrameIterator7AdvanceEv@PLT
	movq	-1536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2179
	call	_ZdlPv@PLT
	movq	-88(%rbp), %r15
	testq	%r15, %r15
	jne	.L2121
	jmp	.L2112
	.p2align 4,,10
	.p2align 3
.L2118:
	movq	-88(%rbp), %r15
	testq	%r15, %r15
	je	.L2112
	leaq	-1536(%rbp), %r10
.L2144:
	cmpb	$2, 76(%r14)
	jne	.L2122
	movq	(%r15), %rax
	movq	%r10, -1584(%rbp)
	movq	%r15, %rdi
	call	*152(%rax)
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11Deoptimizer18DeoptimizeFunctionENS0_10JSFunctionENS0_4CodeE@PLT
	movq	-1584(%rbp), %r10
.L2122:
	pxor	%xmm0, %xmm0
	movq	%r10, -1584(%rbp)
	movq	%r10, %rsi
	movq	%r15, %rdi
	movq	$0, -1520(%rbp)
	movaps	%xmm0, -1536(%rbp)
	movq	(%r15), %rax
	call	*136(%rax)
	movq	-1528(%rbp), %r15
	movq	-1536(%rbp), %rax
	movabsq	$7905747460161236407, %rcx
	movq	-1584(%rbp), %r10
	movq	%r15, %r9
	subq	%rax, %r9
	sarq	$3, %r9
	imulq	%rcx, %r9
	testq	%r9, %r9
	je	.L2123
.L2138:
	subq	$1, %r9
	leaq	0(,%r9,8), %rdx
	subq	%r9, %rdx
	cmpb	$0, -1576(%rbp)
	leaq	(%rax,%rdx,8), %r15
	jne	.L2124
	movq	-1528(%rbp), %rcx
	subq	%rax, %rcx
	movq	%rcx, %rax
	movabsq	$7905747460161236407, %rcx
	sarq	$3, %rax
	imulq	%rcx, %rax
	cmpq	$1, %rax
	ja	.L2183
.L2124:
	cmpb	$1, 76(%r14)
	ja	.L2129
	cmpl	%ebx, 100(%r14)
	jl	.L2130
.L2129:
	movq	24(%r15), %rax
	movq	136(%r14), %rdx
	movq	(%rax), %rax
	movq	23(%rax), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L2131
	movq	%r10, -1584(%rbp)
	movq	%r9, -1576(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-1576(%rbp), %r9
	movq	-1584(%rbp), %r10
	movq	%rax, %r15
.L2132:
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%r10, -1584(%rbp)
	movq	%r9, -1576(%rbp)
	call	_ZN2v88internal5Debug12IsBlackboxedENS0_6HandleINS0_18SharedFunctionInfoEEE
	movq	-1576(%rbp), %r9
	movq	-1584(%rbp), %r10
	testb	%al, %al
	je	.L2184
.L2130:
	movb	%r12b, -1576(%rbp)
.L2128:
	movq	-1536(%rbp), %rax
	subl	$1, %ebx
	testq	%r9, %r9
	jne	.L2138
	movq	-1528(%rbp), %r15
.L2123:
	cmpq	%r15, %rax
	je	.L2142
	.p2align 4,,10
	.p2align 3
.L2139:
	movq	%rax, %rdi
	movq	%r10, -1592(%rbp)
	movq	%rax, -1584(%rbp)
	call	_ZN2v88internal12FrameSummaryD1Ev@PLT
	movq	-1584(%rbp), %rax
	movq	-1592(%rbp), %r10
	addq	$56, %rax
	cmpq	%rax, %r15
	jne	.L2139
.L2142:
	movq	-1536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2185
	movq	%r10, -1584(%rbp)
	call	_ZdlPv@PLT
.L2180:
	movq	%r13, %rdi
	call	_ZN2v88internal23JavaScriptFrameIterator7AdvanceEv@PLT
	movq	-88(%rbp), %r15
	movq	-1584(%rbp), %r10
	testq	%r15, %r15
	jne	.L2144
	jmp	.L2112
	.p2align 4,,10
	.p2align 3
.L2131:
	movq	41088(%rdx), %r15
	cmpq	%r15, 41096(%rdx)
	je	.L2186
.L2133:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%r15)
	jmp	.L2132
	.p2align 4,,10
	.p2align 3
.L2183:
	movq	32(%r15), %rax
	movq	(%rax), %rsi
	movq	-1(%rsi), %rax
	cmpw	$69, 11(%rax)
	jne	.L2126
	movl	43(%rsi), %eax
	shrl	%eax
	andl	$31, %eax
	cmpl	$12, %eax
	jne	.L2187
.L2126:
	leaq	-1552(%rbp), %r8
	movq	%r10, -1600(%rbp)
	movq	%r8, %rdi
	movq	%r9, -1592(%rbp)
	movq	%r8, -1584(%rbp)
	call	_ZN2v88internal12HandlerTableC1ENS0_13BytecodeArrayE@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal12FrameSummary11code_offsetEv@PLT
	movq	-1584(%rbp), %r8
	xorl	%edx, %edx
	leaq	-1556(%rbp), %rcx
	movl	%eax, %esi
	movq	%r8, %rdi
	call	_ZN2v88internal12HandlerTable11LookupRangeEiPiPNS1_15CatchPredictionE@PLT
	movq	-1592(%rbp), %r9
	movq	-1600(%rbp), %r10
	testl	%eax, %eax
	jg	.L2124
	jmp	.L2128
	.p2align 4,,10
	.p2align 3
.L2184:
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal5Debug16FloodWithOneShotENS0_6HandleINS0_18SharedFunctionInfoEEEb
	movq	-1528(%rbp), %rbx
	movq	-1536(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L2134
	.p2align 4,,10
	.p2align 3
.L2135:
	movq	%r12, %rdi
	addq	$56, %r12
	call	_ZN2v88internal12FrameSummaryD1Ev@PLT
	cmpq	%r12, %rbx
	jne	.L2135
	movq	-1536(%rbp), %r12
.L2134:
	testq	%r12, %r12
	je	.L2112
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	jmp	.L2112
.L2186:
	movq	%rdx, %rdi
	movq	%r10, -1600(%rbp)
	movq	%rsi, -1592(%rbp)
	movq	%r9, -1584(%rbp)
	movq	%rdx, -1576(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-1600(%rbp), %r10
	movq	-1592(%rbp), %rsi
	movq	-1584(%rbp), %r9
	movq	-1576(%rbp), %rdx
	movq	%rax, %r15
	jmp	.L2133
.L2187:
	leaq	.LC12(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2185:
	movq	%r10, -1584(%rbp)
	jmp	.L2180
.L2182:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22888:
	.size	_ZN2v88internal5Debug18PrepareStepOnThrowEv, .-_ZN2v88internal5Debug18PrepareStepOnThrowEv
	.section	.text._ZN2v88internal5Debug11PrepareStepENS0_10StepActionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug11PrepareStepENS0_10StepActionE
	.type	_ZN2v88internal5Debug11PrepareStepENS0_10StepActionE, @function
_ZN2v88internal5Debug11PrepareStepENS0_10StepActionE:
.LFB22910:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$1640, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	136(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	41088(%r13), %rax
	movq	%rax, -1608(%rbp)
	movq	41096(%r13), %rax
	movq	%rax, -1616(%rbp)
	movl	41104(%r13), %eax
	leal	1(%rax), %edx
	movl	%edx, 41104(%r13)
	movl	72(%rdi), %r14d
	testl	%r14d, %r14d
	je	.L2189
	movq	%rdi, %r12
	movl	%esi, %ebx
	testb	$8, 56(%rdi)
	je	.L2267
.L2190:
	movb	%bl, 76(%r12)
	leaq	-1504(%rbp), %rax
	movq	136(%r12), %rsi
	movl	%r14d, %edx
	movq	%rax, %rdi
	movq	%rax, -1624(%rbp)
	call	_ZN2v88internal23StackTraceFrameIteratorC1EPNS0_7IsolateENS0_12StackFrameIdE@PLT
	movq	-88(%rbp), %r15
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*8(%rax)
	cmpl	$5, %eax
	je	.L2239
	cmpl	$8, %eax
	je	.L2239
	leaq	-1568(%rbp), %r14
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%r14, -1640(%rbp)
	call	_ZN2v88internal12FrameSummary6GetTopEPKNS0_13StandardFrameE@PLT
	movq	-1536(%rbp), %rcx
	movq	-1544(%rbp), %rax
	movq	%r14, %rdi
	movq	%rcx, -1648(%rbp)
	movl	-1528(%rbp), %ecx
	movq	%rax, -1632(%rbp)
	movl	%ecx, -1656(%rbp)
	call	_ZN2v88internal12FrameSummaryD1Ev@PLT
	movq	-1632(%rbp), %rax
	movq	136(%r12), %rdx
	movq	(%rax), %rax
	movq	23(%rax), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L2195
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L2196:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal5Debug15EnsureBreakInfoENS0_6HandleINS0_18SharedFunctionInfoEEE
	movb	%al, -1664(%rbp)
	testb	%al, %al
	je	.L2194
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal5Debug32PrepareFunctionForDebugExecutionENS0_6HandleINS0_18SharedFunctionInfoEEE
	movq	136(%r12), %rdx
	movq	(%r14), %rax
	movq	41112(%rdx), %rdi
	movq	31(%rax), %r11
	testq	%rdi, %rdi
	je	.L2198
	movq	%r11, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L2199:
	movq	%r15, %rdx
	leaq	-1600(%rbp), %rdi
	call	_ZN2v88internal13BreakLocation9FromFrameENS0_6HandleINS0_9DebugInfoEEEPNS0_15JavaScriptFrameE
	movl	-1588(%rbp), %r15d
	movzbl	76(%r12), %edx
	cmpl	$4, %r15d
	je	.L2202
	testb	%bl, %bl
	jne	.L2203
	cmpl	$5, %r15d
	jne	.L2203
.L2202:
	testb	%dl, %dl
	jne	.L2204
	movq	-1632(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, 80(%r12)
.L2204:
	movb	$2, 76(%r12)
	movb	$1, 9(%r12)
.L2211:
	movq	-1648(%rbp), %rax
	movl	-1656(%rbp), %esi
	movq	-1640(%rbp), %rdi
	movq	(%rax), %rax
	movq	%rax, -1568(%rbp)
	call	_ZN2v88internal12AbstractCode23SourceStatementPositionEi@PLT
	movq	%r12, %rdi
	movl	%eax, 92(%r12)
	call	_ZN2v88internal5Debug17CurrentFrameCountEv
	movq	$0, 112(%r12)
	movl	%eax, -1632(%rbp)
.L2205:
	movq	$-1, 92(%r12)
	cmpl	$3, %r15d
	jle	.L2268
.L2213:
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2229
	movq	%r13, -1672(%rbp)
	movzbl	-1664(%rbp), %r14d
.L2224:
	movq	(%rdi), %rax
	call	*8(%rax)
	cmpl	$5, %eax
	sete	%bl
	cmpl	$8, %eax
	sete	%al
	orb	%al, %bl
	je	.L2269
.L2216:
	movq	-1624(%rbp), %rdi
	call	_ZN2v88internal23StackTraceFrameIterator7AdvanceEv@PLT
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2224
	movq	-1672(%rbp), %r13
	jmp	.L2229
	.p2align 4,,10
	.p2align 3
.L2267:
	movq	48(%rdi), %rax
	movl	$3, %esi
	movq	40960(%rax), %rdi
	addq	$248, %rdi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	orl	$8, 56(%r12)
	jmp	.L2190
	.p2align 4,,10
	.p2align 3
.L2239:
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*8(%rax)
	cmpl	$5, %eax
	je	.L2194
	movq	%r15, %rdi
	call	_ZNK2v88internal25WasmInterpreterEntryFrame10debug_infoEv@PLT
	movsbl	%bl, %esi
	leaq	-1568(%rbp), %rdi
	movq	%rax, -1568(%rbp)
	call	_ZN2v88internal13WasmDebugInfo11PrepareStepENS0_10StepActionE@PLT
.L2194:
	movq	-1608(%rbp), %rax
	subl	$1, 41104(%r13)
	movq	%rax, 41088(%r13)
	movq	-1616(%rbp), %rax
	cmpq	%rax, 41096(%r13)
	je	.L2188
.L2266:
	movq	%rax, 41096(%r13)
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2188:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2270
	addq	$1640, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2189:
	.cfi_restore_state
	movl	%eax, 41104(%r13)
	jmp	.L2188
	.p2align 4,,10
	.p2align 3
.L2195:
	movq	41088(%rdx), %r14
	cmpq	%r14, 41096(%rdx)
	je	.L2271
.L2197:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%r14)
	jmp	.L2196
	.p2align 4,,10
	.p2align 3
.L2203:
	movzbl	-1664(%rbp), %ecx
	movl	%ecx, %eax
	cmpb	$2, %dl
	je	.L2206
	movq	136(%r12), %rdx
	cmpl	$32, 41828(%rdx)
	je	.L2206
	movzbl	132(%r12), %eax
.L2206:
	movb	%al, 9(%r12)
	cmpb	$1, %bl
	je	.L2207
	movq	-1648(%rbp), %rax
	movl	-1656(%rbp), %esi
	movq	-1640(%rbp), %rdi
	movq	(%rax), %rax
	movq	%rax, -1568(%rbp)
	call	_ZN2v88internal12AbstractCode23SourceStatementPositionEi@PLT
	movq	%r12, %rdi
	movl	%eax, 92(%r12)
	call	_ZN2v88internal5Debug17CurrentFrameCountEv
	movq	$0, 112(%r12)
	movl	%eax, -1632(%rbp)
	movl	%eax, 96(%r12)
	cmpb	$2, %bl
	je	.L2208
	jg	.L2229
	cmpb	$-1, %bl
	je	.L2210
	testb	%bl, %bl
	je	.L2205
	jmp	.L2229
	.p2align 4,,10
	.p2align 3
.L2207:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal5Debug12IsBlackboxedENS0_6HandleINS0_18SharedFunctionInfoEEE
	testb	%al, %al
	jne	.L2211
	movq	-1648(%rbp), %rax
	movl	-1656(%rbp), %esi
	movq	-1640(%rbp), %rdi
	movq	(%rax), %rax
	movq	%rax, -1568(%rbp)
	call	_ZN2v88internal12AbstractCode23SourceStatementPositionEi@PLT
	movq	%r12, %rdi
	movl	%eax, 92(%r12)
	call	_ZN2v88internal5Debug17CurrentFrameCountEv
	movq	$0, 112(%r12)
	movl	%eax, 96(%r12)
	movl	%eax, 100(%r12)
.L2208:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal5Debug16FloodWithOneShotENS0_6HandleINS0_18SharedFunctionInfoEEEb
.L2229:
	movq	-1608(%rbp), %rax
	subl	$1, 41104(%r13)
	movq	%rax, 41088(%r13)
	movq	-1616(%rbp), %rax
	cmpq	41096(%r13), %rax
	jne	.L2266
	jmp	.L2188
	.p2align 4,,10
	.p2align 3
.L2198:
	movq	41088(%rdx), %rsi
	cmpq	41096(%rdx), %rsi
	je	.L2272
.L2200:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rdx)
	movq	%r11, (%rsi)
	jmp	.L2199
	.p2align 4,,10
	.p2align 3
.L2269:
	cmpb	$2, 76(%r12)
	movq	-88(%rbp), %r15
	jne	.L2217
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*152(%rax)
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal11Deoptimizer18DeoptimizeFunctionENS0_10JSFunctionENS0_4CodeE@PLT
.L2217:
	movq	136(%r12), %r13
	movq	-1640(%rbp), %rsi
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	movq	41088(%r13), %rax
	addl	$1, 41104(%r13)
	movq	%rax, -1656(%rbp)
	movq	41096(%r13), %rax
	movq	$0, -1552(%rbp)
	movq	%rax, -1648(%rbp)
	movaps	%xmm0, -1568(%rbp)
	call	_ZNK2v88internal15JavaScriptFrame12GetFunctionsEPSt6vectorINS0_6HandleINS0_18SharedFunctionInfoEEESaIS5_EE@PLT
	movq	-1560(%rbp), %rdi
	movq	-1568(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2273
	movl	-1632(%rbp), %r15d
	movb	%bl, -1664(%rbp)
	movq	%r12, %rbx
.L2222:
	movq	-8(%rdi), %r12
	subq	$8, %rdi
	movq	%rdi, -1560(%rbp)
	testb	%r14b, %r14b
	jne	.L2219
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal5Debug12IsBlackboxedENS0_6HandleINS0_18SharedFunctionInfoEEE
	testb	%al, %al
	je	.L2220
	movq	-1560(%rbp), %rdi
	movq	-1568(%rbp), %rax
.L2219:
	subl	$1, %r15d
	cmpq	%rdi, %rax
	je	.L2274
	xorl	%r14d, %r14d
	jmp	.L2222
	.p2align 4,,10
	.p2align 3
.L2220:
	movq	%rbx, %rdi
	xorl	%edx, %edx
	movq	%r12, %rsi
	movl	%r15d, -1632(%rbp)
	movq	%r13, %r14
	movq	-1672(%rbp), %r13
	call	_ZN2v88internal5Debug16FloodWithOneShotENS0_6HandleINS0_18SharedFunctionInfoEEEb
	movl	-1632(%rbp), %eax
	movq	-1568(%rbp), %rdi
	movl	%eax, 100(%rbx)
	testq	%rdi, %rdi
	je	.L2221
	call	_ZdlPv@PLT
.L2221:
	movq	-1656(%rbp), %rax
	subl	$1, 41104(%r14)
	movq	%rax, 41088(%r14)
	movq	-1648(%rbp), %rax
	cmpq	41096(%r14), %rax
	je	.L2194
	movq	%rax, 41096(%r14)
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	jmp	.L2194
	.p2align 4,,10
	.p2align 3
.L2271:
	movq	%rdx, %rdi
	movq	%rsi, -1672(%rbp)
	movq	%rdx, -1664(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-1672(%rbp), %rsi
	movq	-1664(%rbp), %rdx
	movq	%rax, %r14
	jmp	.L2197
.L2268:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal5Debug12IsBlackboxedENS0_6HandleINS0_18SharedFunctionInfoEEE
	testb	%al, %al
	jne	.L2213
	movl	-1632(%rbp), %eax
	movb	$1, 88(%r12)
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$1, %edx
	movl	%eax, 100(%r12)
	call	_ZN2v88internal5Debug16FloodWithOneShotENS0_6HandleINS0_18SharedFunctionInfoEEEb
	jmp	.L2194
.L2272:
	movq	%rdx, %rdi
	movq	%r11, -1680(%rbp)
	movq	%rdx, -1672(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-1680(%rbp), %r11
	movq	-1672(%rbp), %rdx
	movq	%rax, %rsi
	jmp	.L2200
.L2274:
	movl	%r15d, -1632(%rbp)
	movq	%rbx, %r12
	movzbl	-1664(%rbp), %ebx
.L2218:
	testq	%rdi, %rdi
	je	.L2223
	call	_ZdlPv@PLT
.L2223:
	movq	-1656(%rbp), %rax
	subl	$1, 41104(%r13)
	movq	%rax, 41088(%r13)
	movq	-1648(%rbp), %rax
	cmpq	41096(%r13), %rax
	je	.L2233
	movq	%rax, 41096(%r13)
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2233:
	movl	%ebx, %r14d
	jmp	.L2216
.L2273:
	movl	%r14d, %ebx
	jmp	.L2218
.L2270:
	call	__stack_chk_fail@PLT
.L2210:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE22910:
	.size	_ZN2v88internal5Debug11PrepareStepENS0_10StepActionE, .-_ZN2v88internal5Debug11PrepareStepENS0_10StepActionE
	.section	.text._ZN2v88internal5Debug12RestoreDebugEPc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug12RestoreDebugEPc
	.type	_ZN2v88internal5Debug12RestoreDebugEPc, @function
_ZN2v88internal5Debug12RestoreDebugEPc:
.LFB22843:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$80, %rsp
	movdqu	(%rsi), %xmm0
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movups	%xmm0, 64(%rdi)
	movdqu	16(%rsi), %xmm1
	movups	%xmm1, 80(%rdi)
	movdqu	32(%rsi), %xmm2
	movups	%xmm2, 96(%rdi)
	movdqu	48(%rsi), %xmm3
	movups	%xmm3, 112(%rdi)
	movq	64(%rsi), %rdx
	movq	%r12, %rsi
	movq	%rdx, 128(%rdi)
	leaq	-112(%rbp), %rdi
	call	_ZN2v88internal10DebugScopeC1EPNS0_5DebugE
	movq	24(%r12), %r13
	testq	%r13, %r13
	je	.L2279
	.p2align 4,,10
	.p2align 3
.L2276:
	movq	0(%r13), %r14
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal5Debug16ClearBreakPointsENS0_6HandleINS0_9DebugInfoEEE
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal5Debug16ApplyBreakPointsENS0_6HandleINS0_9DebugInfoEEE
	movq	8(%r13), %r13
	testq	%r13, %r13
	jne	.L2276
.L2279:
	movsbl	76(%r12), %esi
	cmpb	$-1, %sil
	je	.L2278
	movq	%r12, %rdi
	call	_ZN2v88internal5Debug11PrepareStepENS0_10StepActionE
.L2278:
	movq	-112(%rbp), %rax
	movq	-104(%rbp), %rdx
	leaq	72(%rbx), %r12
	movq	%rdx, 64(%rax)
	movq	-112(%rbp), %rbx
	movl	-96(%rbp), %eax
	movl	%eax, 72(%rbx)
	movq	(%rbx), %rax
	testq	%rax, %rax
	setne	%dl
	cmpb	8(%rbx), %dl
	je	.L2280
	movq	136(%rbx), %rdx
	movq	40952(%rdx), %rdi
	testq	%rax, %rax
	je	.L2281
	call	_ZN2v88internal16CompilationCache7DisableEv@PLT
	movl	$1, %r13d
	testb	$2, 56(%rbx)
	je	.L2291
.L2282:
	movb	%r13b, 8(%rbx)
	movq	136(%rbx), %rdi
	call	_ZN2v88internal7Isolate23PromiseHookStateUpdatedEv@PLT
.L2280:
	leaq	16+_ZTVN2v88internal15InterruptsScopeE(%rip), %rax
	cmpl	$2, -56(%rbp)
	movq	%rax, -88(%rbp)
	je	.L2275
	movq	-80(%rbp), %rdi
	call	_ZN2v88internal10StackGuard18PopInterruptsScopeEv@PLT
.L2275:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2292
	addq	$80, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2291:
	.cfi_restore_state
	movq	48(%rbx), %rax
	movl	$1, %esi
	movq	40960(%rax), %rdi
	addq	$248, %rdi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	orl	$2, 56(%rbx)
	jmp	.L2282
	.p2align 4,,10
	.p2align 3
.L2281:
	call	_ZN2v88internal16CompilationCache6EnableEv@PLT
	movq	%rbx, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal5Debug6UnloadEv
	jmp	.L2282
.L2292:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22843:
	.size	_ZN2v88internal5Debug12RestoreDebugEPc, .-_ZN2v88internal5Debug12RestoreDebugEPc
	.section	.rodata._ZN2v88internal5Debug22PerformSideEffectCheckENS0_6HandleINS0_10JSFunctionEEENS2_INS0_6ObjectEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC13:
	.string	"[debug-evaluate] Function %s failed side effect check.\n"
	.section	.text._ZN2v88internal5Debug22PerformSideEffectCheckENS0_6HandleINS0_10JSFunctionEEENS2_INS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug22PerformSideEffectCheckENS0_6HandleINS0_10JSFunctionEEENS2_INS0_6ObjectEEE
	.type	_ZN2v88internal5Debug22PerformSideEffectCheckENS0_6HandleINS0_10JSFunctionEEENS2_INS0_6ObjectEEE, @function
_ZN2v88internal5Debug22PerformSideEffectCheckENS0_6HandleINS0_10JSFunctionEEENS2_INS0_6ObjectEEE:
.LFB23060:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$104, %rsp
	movq	%rdx, -144(%rbp)
	movq	136(%rdi), %rsi
	movq	%r15, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb0EEC1EPNS0_7IsolateE@PLT
	movq	(%rbx), %rax
	movq	23(%rax), %r13
	movq	%r13, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	7(%r13), %rdx
	testb	$1, %dl
	jne	.L2294
.L2297:
	movq	7(%r13), %rdx
	testb	$1, %dl
	jne	.L2341
.L2295:
	xorl	%eax, %eax
.L2306:
	movabsq	$287762808832, %rcx
	movq	7(%r13), %rdx
	cmpq	%rcx, %rdx
	je	.L2309
	movl	$1, %ecx
	testb	$1, %dl
	jne	.L2342
.L2308:
	movq	%rax, -80(%rbp)
	movq	(%rbx), %rax
	movb	%cl, -72(%rbp)
	movq	47(%rax), %rdx
	cmpl	$67, 59(%rdx)
	jne	.L2343
.L2310:
	leaq	-80(%rbp), %rdx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8Compiler7CompileENS0_6HandleINS0_10JSFunctionEEENS1_18ClearExceptionFlagEPNS0_15IsCompiledScopeE@PLT
	movl	%eax, %r13d
	testb	%al, %al
	je	.L2315
.L2314:
	movq	136(%r12), %r14
	movq	(%rbx), %rax
	movq	23(%rax), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L2316
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L2317:
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	-104(%rbp), %r14
	call	_ZN2v88internal5Debug20GetOrCreateDebugInfoENS0_6HandleINS0_18SharedFunctionInfoEEE
	movq	136(%r12), %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	movq	(%rax), %rax
	movq	%rdx, -136(%rbp)
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal9DebugInfo18GetSideEffectStateEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rdx
	cmpl	$2, %eax
	je	.L2319
	cmpl	$3, %eax
	jne	.L2344
	movl	$1, %r13d
.L2315:
	movq	%r15, %rdi
	call	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb0EED1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2345
	addq	$104, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2316:
	.cfi_restore_state
	movq	41088(%r14), %r13
	cmpq	41096(%r14), %r13
	je	.L2346
.L2318:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, 0(%r13)
	jmp	.L2317
	.p2align 4,,10
	.p2align 3
.L2341:
	movq	-1(%rdx), %rdx
	cmpw	$91, 11(%rdx)
	jne	.L2295
	movq	31(%r13), %rdx
	testb	$1, %dl
	jne	.L2347
.L2298:
	movq	7(%r13), %rdx
	testb	$1, %dl
	jne	.L2348
.L2302:
	movq	7(%r13), %rdx
	movq	7(%rdx), %rsi
.L2301:
	movq	3520(%rax), %rdi
	leaq	-37592(%rax), %r14
	testq	%rdi, %rdi
	je	.L2303
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	jmp	.L2306
	.p2align 4,,10
	.p2align 3
.L2342:
	movq	-1(%rdx), %rcx
	cmpw	$165, 11(%rcx)
	jne	.L2349
.L2309:
	xorl	%ecx, %ecx
	jmp	.L2308
	.p2align 4,,10
	.p2align 3
.L2344:
	cmpl	$1, %eax
	je	.L2350
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2343:
	movabsq	$287762808832, %rdx
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2310
	testb	$1, %al
	je	.L2314
	movq	-1(%rax), %rdx
	cmpw	$165, 11(%rdx)
	je	.L2310
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L2314
	jmp	.L2310
	.p2align 4,,10
	.p2align 3
.L2294:
	movq	-1(%rdx), %rdx
	cmpw	$72, 11(%rdx)
	jne	.L2297
	movq	31(%r13), %rdx
	testb	$1, %dl
	je	.L2298
.L2347:
	movq	-1(%rdx), %rcx
	cmpw	$86, 11(%rcx)
	jne	.L2298
	movq	39(%rdx), %rcx
	testb	$1, %cl
	je	.L2298
	movq	-1(%rcx), %rcx
	cmpw	$72, 11(%rcx)
	jne	.L2298
	movq	31(%rdx), %rsi
	jmp	.L2301
	.p2align 4,,10
	.p2align 3
.L2319:
	movq	0(%r13), %rax
	movq	7(%rax), %rcx
	testb	$1, %cl
	jne	.L2323
.L2327:
	movq	7(%rax), %rax
	testb	$1, %al
	jne	.L2351
.L2324:
	movq	-144(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal5Debug31PerformSideEffectCheckForObjectENS0_6HandleINS0_6ObjectEEE
	movl	%eax, %r13d
	jmp	.L2315
	.p2align 4,,10
	.p2align 3
.L2350:
	cmpb	$0, _ZN2v88internal42FLAG_trace_side_effect_free_debug_evaluateE(%rip)
	jne	.L2352
.L2321:
	movb	$1, 16(%r12)
	movq	136(%r12), %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal7Isolate18TerminateExecutionEv@PLT
	jmp	.L2315
	.p2align 4,,10
	.p2align 3
.L2303:
	movq	41088(%r14), %rax
	cmpq	41096(%r14), %rax
	je	.L2353
.L2305:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%r14)
	movq	%rsi, (%rax)
	jmp	.L2306
	.p2align 4,,10
	.p2align 3
.L2349:
	movq	-1(%rdx), %rdx
	cmpw	$166, 11(%rdx)
	setne	%cl
	jmp	.L2308
	.p2align 4,,10
	.p2align 3
.L2352:
	movq	(%rbx), %rax
	leaq	-120(%rbp), %rdi
	movq	23(%rax), %rax
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal18SharedFunctionInfo9DebugNameEv@PLT
	leaq	-112(%rbp), %rsi
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movl	$1, %ecx
	movl	$1, %edx
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	-104(%rbp), %rsi
	leaq	.LC13(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2321
	call	_ZdaPv@PLT
	jmp	.L2321
	.p2align 4,,10
	.p2align 3
.L2323:
	movq	-1(%rcx), %rcx
	cmpw	$72, 11(%rcx)
	jne	.L2327
.L2326:
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rdx, -136(%rbp)
	call	_ZN2v88internal5Debug32PrepareFunctionForDebugExecutionENS0_6HandleINS0_18SharedFunctionInfoEEE
	movq	136(%r12), %rbx
	movq	-136(%rbp), %rdx
	movq	41112(%rbx), %rdi
	movq	(%rdx), %rax
	movq	39(%rax), %rsi
	testq	%rdi, %rdi
	je	.L2328
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-136(%rbp), %rdx
	movq	%rax, %rdi
.L2329:
	movq	%rdx, -136(%rbp)
	movl	$1, %r13d
	call	_ZN2v88internal13DebugEvaluate21ApplySideEffectChecksENS0_6HandleINS0_13BytecodeArrayEEE@PLT
	movq	-136(%rbp), %rdx
	movl	$32, %esi
	movq	%r14, %rdi
	movq	(%rdx), %rax
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal9DebugInfo21SetDebugExecutionModeENS1_13ExecutionModeE@PLT
	jmp	.L2315
	.p2align 4,,10
	.p2align 3
.L2351:
	movq	-1(%rax), %rax
	cmpw	$91, 11(%rax)
	jne	.L2324
	jmp	.L2326
	.p2align 4,,10
	.p2align 3
.L2348:
	movq	-1(%rdx), %rdx
	cmpw	$72, 11(%rdx)
	jne	.L2302
	movq	7(%r13), %rsi
	jmp	.L2301
	.p2align 4,,10
	.p2align 3
.L2346:
	movq	%r14, %rdi
	movq	%rsi, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L2318
	.p2align 4,,10
	.p2align 3
.L2328:
	movq	41088(%rbx), %rdi
	cmpq	41096(%rbx), %rdi
	je	.L2354
.L2330:
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rdi)
	jmp	.L2329
	.p2align 4,,10
	.p2align 3
.L2353:
	movq	%r14, %rdi
	movq	%rsi, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rsi
	jmp	.L2305
	.p2align 4,,10
	.p2align 3
.L2354:
	movq	%rbx, %rdi
	movq	%rsi, -144(%rbp)
	movq	%rdx, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-144(%rbp), %rsi
	movq	-136(%rbp), %rdx
	movq	%rax, %rdi
	jmp	.L2330
.L2345:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23060:
	.size	_ZN2v88internal5Debug22PerformSideEffectCheckENS0_6HandleINS0_10JSFunctionEEENS2_INS0_6ObjectEEE, .-_ZN2v88internal5Debug22PerformSideEffectCheckENS0_6HandleINS0_10JSFunctionEEENS2_INS0_6ObjectEEE
	.section	.text._ZN2v88internal5Debug24SetBreakpointForFunctionENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_6StringEEEPi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug24SetBreakpointForFunctionENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_6StringEEEPi
	.type	_ZN2v88internal5Debug24SetBreakpointForFunctionENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_6StringEEEPi, @function
_ZN2v88internal5Debug24SetBreakpointForFunctionENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_6StringEEEPi:
.LFB22873:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$152, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	128(%rdi), %eax
	leal	1(%rax), %esi
	movl	%esi, 128(%rdi)
	movl	%esi, (%rcx)
	movq	136(%rdi), %rdi
	call	_ZN2v88internal7Factory13NewBreakPointEiNS0_6HandleINS0_6StringEEE@PLT
	movq	136(%r15), %r13
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	%rax, -176(%rbp)
	addl	$1, 41104(%r13)
	movq	41088(%r13), %rax
	movq	41096(%r13), %r14
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal5Debug15EnsureBreakInfoENS0_6HandleINS0_18SharedFunctionInfoEEE
	movl	%eax, %r12d
	testb	%al, %al
	jne	.L2369
.L2356:
	movq	-168(%rbp), %rax
	subl	$1, 41104(%r13)
	movq	%rax, 41088(%r13)
	cmpq	41096(%r13), %r14
	je	.L2355
	movq	%r14, 41096(%r13)
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2355:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2370
	addq	$152, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2369:
	.cfi_restore_state
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal5Debug32PrepareFunctionForDebugExecutionENS0_6HandleINS0_18SharedFunctionInfoEEE
	movq	136(%r15), %rdx
	movq	(%rbx), %rax
	movq	41112(%rdx), %rdi
	movq	31(%rax), %rsi
	testq	%rdi, %rdi
	je	.L2357
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rbx
.L2358:
	leaq	-160(%rbp), %rdi
	movq	%rsi, -160(%rbp)
	movq	%rdi, -184(%rbp)
	call	_ZNK2v88internal9DebugInfo15CanBreakAtEntryEv@PLT
	xorl	%edx, %edx
	testb	%al, %al
	jne	.L2360
	movq	-184(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal13BreakIteratorC1ENS0_6HandleINS0_9DebugInfoEEE
	movq	-184(%rbp), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal13BreakIterator14SkipToPositionEi
	movl	-148(%rbp), %edx
.L2360:
	movq	136(%r15), %rdi
	movq	-176(%rbp), %rcx
	movq	%rbx, %rsi
	call	_ZN2v88internal9DebugInfo13SetBreakPointEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_10BreakPointEEE@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal5Debug16ClearBreakPointsENS0_6HandleINS0_9DebugInfoEEE
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal5Debug16ApplyBreakPointsENS0_6HandleINS0_9DebugInfoEEE
	testb	$4, 56(%r15)
	jne	.L2356
	movq	48(%r15), %rax
	movl	$2, %esi
	movq	40960(%rax), %rdi
	addq	$248, %rdi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	orl	$4, 56(%r15)
	jmp	.L2356
	.p2align 4,,10
	.p2align 3
.L2357:
	movq	41088(%rdx), %rbx
	cmpq	41096(%rdx), %rbx
	je	.L2371
.L2359:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%rbx)
	jmp	.L2358
	.p2align 4,,10
	.p2align 3
.L2371:
	movq	%rdx, %rdi
	movq	%rsi, -192(%rbp)
	movq	%rdx, -184(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-192(%rbp), %rsi
	movq	-184(%rbp), %rdx
	movq	%rax, %rbx
	jmp	.L2359
.L2370:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22873:
	.size	_ZN2v88internal5Debug24SetBreakpointForFunctionENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_6StringEEEPi, .-_ZN2v88internal5Debug24SetBreakpointForFunctionENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_6StringEEEPi
	.section	.text._ZNSt6vectorIN2v88internal15IsCompiledScopeESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal15IsCompiledScopeESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal15IsCompiledScopeESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal15IsCompiledScopeESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, @function
_ZNSt6vectorIN2v88internal15IsCompiledScopeESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_:
.LFB27595:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r12
	movq	(%rdi), %r15
	movabsq	$576460752303423487, %rdi
	movq	%r12, %rax
	subq	%r15, %rax
	sarq	$4, %rax
	cmpq	%rdi, %rax
	je	.L2391
	movq	%rsi, %rcx
	movq	%rsi, %rbx
	subq	%r15, %rcx
	testq	%rax, %rax
	je	.L2382
	movabsq	$9223372036854775792, %rsi
	leaq	(%rax,%rax), %r8
	cmpq	%r8, %rax
	jbe	.L2392
.L2374:
	movq	%rsi, %rdi
	movq	%rdx, -72(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rcx
	movq	%rax, %r14
	movq	-72(%rbp), %rdx
	leaq	(%rax,%rsi), %rax
	leaq	16(%r14), %r8
.L2381:
	movzbl	8(%rdx), %esi
	movq	(%rdx), %rdx
	addq	%r14, %rcx
	movq	%rdx, (%rcx)
	movb	%sil, 8(%rcx)
	cmpq	%r15, %rbx
	je	.L2376
	movq	%r14, %rcx
	movq	%r15, %rdx
	.p2align 4,,10
	.p2align 3
.L2377:
	movq	(%rdx), %rdi
	movzbl	8(%rdx), %esi
	addq	$16, %rdx
	addq	$16, %rcx
	movq	%rdi, -16(%rcx)
	movb	%sil, -8(%rcx)
	cmpq	%rdx, %rbx
	jne	.L2377
	movq	%rbx, %rdx
	subq	%r15, %rdx
	leaq	16(%r14,%rdx), %r8
.L2376:
	cmpq	%r12, %rbx
	je	.L2378
	movq	%rbx, %rdx
	movq	%r8, %rcx
	.p2align 4,,10
	.p2align 3
.L2379:
	movzbl	8(%rdx), %esi
	movq	(%rdx), %rdi
	addq	$16, %rdx
	addq	$16, %rcx
	movq	%rdi, -16(%rcx)
	movb	%sil, -8(%rcx)
	cmpq	%r12, %rdx
	jne	.L2379
	subq	%rbx, %rdx
	addq	%rdx, %r8
.L2378:
	testq	%r15, %r15
	je	.L2380
	movq	%r15, %rdi
	movq	%rax, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %rax
	movq	-56(%rbp), %r8
.L2380:
	movq	%r14, %xmm0
	movq	%r8, %xmm1
	movq	%rax, 16(%r13)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 0(%r13)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2392:
	.cfi_restore_state
	testq	%r8, %r8
	jne	.L2375
	movl	$16, %r8d
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	jmp	.L2381
	.p2align 4,,10
	.p2align 3
.L2382:
	movl	$16, %esi
	jmp	.L2374
.L2375:
	cmpq	%rdi, %r8
	movq	%rdi, %rax
	cmovbe	%r8, %rax
	salq	$4, %rax
	movq	%rax, %rsi
	jmp	.L2374
.L2391:
	leaq	.LC7(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE27595:
	.size	_ZNSt6vectorIN2v88internal15IsCompiledScopeESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, .-_ZNSt6vectorIN2v88internal15IsCompiledScopeESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.section	.text._ZNSt6vectorIN2v88internal13BreakLocationESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal13BreakLocationESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal13BreakLocationESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal13BreakLocationESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, @function
_ZNSt6vectorIN2v88internal13BreakLocationESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_:
.LFB28596:
	.cfi_startproc
	endbr64
	movabsq	$-6148914691236517205, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rsi
	movq	(%rdi), %r14
	movq	%rsi, %rax
	subq	%r14, %rax
	sarq	$3, %rax
	imulq	%rcx, %rax
	movabsq	$384307168202282325, %rcx
	cmpq	%rcx, %rax
	je	.L2410
	movq	%r12, %r8
	movq	%rdi, %r15
	subq	%r14, %r8
	testq	%rax, %rax
	je	.L2402
	movabsq	$9223372036854775800, %rbx
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L2411
.L2395:
	movq	%rbx, %rdi
	movq	%rdx, -80(%rbp)
	movq	%r8, -72(%rbp)
	movq	%rsi, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rsi
	movq	-72(%rbp), %r8
	movq	%rax, %r13
	leaq	(%rax,%rbx), %rax
	movq	-80(%rbp), %rdx
	movq	%rax, -56(%rbp)
	leaq	24(%r13), %rbx
.L2401:
	movdqu	(%rdx), %xmm2
	movq	16(%rdx), %rax
	movups	%xmm2, 0(%r13,%r8)
	movq	%rax, 16(%r13,%r8)
	cmpq	%r14, %r12
	je	.L2397
	movq	%r13, %rdx
	movq	%r14, %rax
	.p2align 4,,10
	.p2align 3
.L2398:
	movdqu	(%rax), %xmm1
	addq	$24, %rax
	addq	$24, %rdx
	movups	%xmm1, -24(%rdx)
	movq	-8(%rax), %rcx
	movq	%rcx, -8(%rdx)
	cmpq	%rax, %r12
	jne	.L2398
	leaq	-24(%r12), %rax
	subq	%r14, %rax
	shrq	$3, %rax
	leaq	48(%r13,%rax,8), %rbx
.L2397:
	cmpq	%rsi, %r12
	je	.L2399
	subq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	-24(%rsi), %rax
	movq	%r12, %rsi
	shrq	$3, %rax
	leaq	24(,%rax,8), %rdx
	movq	%rdx, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %rdx
	addq	%rdx, %rbx
.L2399:
	testq	%r14, %r14
	je	.L2400
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2400:
	movq	-56(%rbp), %rax
	movq	%r13, %xmm0
	movq	%rbx, %xmm3
	punpcklqdq	%xmm3, %xmm0
	movq	%rax, 16(%r15)
	movups	%xmm0, (%r15)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2411:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L2396
	movq	$0, -56(%rbp)
	movl	$24, %ebx
	xorl	%r13d, %r13d
	jmp	.L2401
	.p2align 4,,10
	.p2align 3
.L2402:
	movl	$24, %ebx
	jmp	.L2395
.L2396:
	cmpq	%rcx, %rdi
	movq	%rcx, %rbx
	cmovbe	%rdi, %rbx
	imulq	$24, %rbx, %rbx
	jmp	.L2395
.L2410:
	leaq	.LC7(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE28596:
	.size	_ZNSt6vectorIN2v88internal13BreakLocationESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, .-_ZNSt6vectorIN2v88internal13BreakLocationESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.section	.rodata._ZN2v88internal5Debug22GetPossibleBreakpointsENS0_6HandleINS0_6ScriptEEEiibPSt6vectorINS0_13BreakLocationESaIS6_EE.str1.1,"aMS",@progbits,1
.LC14:
	.string	"candidate->HasBreakInfo()"
	.section	.text._ZN2v88internal5Debug22GetPossibleBreakpointsENS0_6HandleINS0_6ScriptEEEiibPSt6vectorINS0_13BreakLocationESaIS6_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug22GetPossibleBreakpointsENS0_6HandleINS0_6ScriptEEEiibPSt6vectorINS0_13BreakLocationESaIS6_EE
	.type	_ZN2v88internal5Debug22GetPossibleBreakpointsENS0_6HandleINS0_6ScriptEEEiibPSt6vectorINS0_13BreakLocationESaIS6_EE, @function
_ZN2v88internal5Debug22GetPossibleBreakpointsENS0_6HandleINS0_6ScriptEEEiibPSt6vectorINS0_13BreakLocationESaIS6_EE:
.LFB22965:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$328, %rsp
	movq	%rsi, -264(%rbp)
	movl	%ecx, -248(%rbp)
	movl	%r8d, -312(%rbp)
	movq	%r9, -328(%rbp)
	movb	%r8b, -272(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	testb	%r8b, %r8b
	jne	.L2413
	movl	%edx, -304(%rbp)
	leaq	-240(%rbp), %r15
.L2414:
	movq	136(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	movq	$0, -208(%rbp)
	movaps	%xmm0, -224(%rbp)
	movq	41088(%rax), %rcx
	addl	$1, 41104(%rax)
	movq	%rax, -288(%rbp)
	movq	136(%rbx), %rsi
	movq	%rcx, -320(%rbp)
	movq	41096(%rax), %rcx
	movq	-264(%rbp), %rax
	movq	%rcx, -296(%rbp)
	movq	(%rax), %rdx
	call	_ZN2v88internal18SharedFunctionInfo14ScriptIteratorC1EPNS0_7IsolateENS0_6ScriptE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal18SharedFunctionInfo14ScriptIterator4NextEv@PLT
	movq	%rax, -192(%rbp)
	testq	%rax, %rax
	je	.L2427
	xorl	%r12d, %r12d
	xorl	%r14d, %r14d
	movq	$0, -256(%rbp)
	leaq	-192(%rbp), %r13
	movq	%r12, -280(%rbp)
	movl	-304(%rbp), %r12d
	.p2align 4,,10
	.p2align 3
.L2453:
	movq	%r13, %rdi
	call	_ZNK2v88internal18SharedFunctionInfo11EndPositionEv@PLT
	cmpl	%r12d, %eax
	jl	.L2428
	movq	%r13, %rdi
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	cmpl	-248(%rbp), %eax
	jge	.L2428
	movq	-192(%rbp), %rax
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L2574
.L2429:
	leaq	-160(%rbp), %rdi
	movq	%rax, -160(%rbp)
	call	_ZN2v88internal6Script16IsUserJavaScriptEv@PLT
	testb	%al, %al
	jne	.L2575
	.p2align 4,,10
	.p2align 3
.L2428:
	movq	%r15, %rdi
	call	_ZN2v88internal18SharedFunctionInfo14ScriptIterator4NextEv@PLT
	movq	%rax, -192(%rbp)
	testq	%rax, %rax
	jne	.L2453
	movq	-256(%rbp), %r12
	cmpq	%r12, %r14
	je	.L2454
	movzbl	-272(%rbp), %eax
	movq	%r15, -280(%rbp)
	movq	%r12, -344(%rbp)
	movq	%r13, -336(%rbp)
	movq	%r12, %r13
	movl	%eax, %r12d
	.p2align 4,,10
	.p2align 3
.L2477:
	movq	0(%r13), %rax
	movq	(%rax), %r15
	movq	%r15, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	7(%r15), %rsi
	testb	$1, %sil
	jne	.L2455
.L2458:
	movq	7(%r15), %rsi
	testb	$1, %sil
	jne	.L2576
.L2456:
	xorl	%eax, %eax
.L2467:
	movabsq	$287762808832, %rcx
	movq	7(%r15), %rdx
	cmpq	%rcx, %rdx
	je	.L2468
	testb	$1, %dl
	jne	.L2577
	movq	%rax, -160(%rbp)
.L2573:
	movb	$1, -152(%rbp)
.L2473:
	movq	-216(%rbp), %rsi
	cmpq	-208(%rbp), %rsi
	je	.L2475
	movzbl	-152(%rbp), %eax
	movq	-160(%rbp), %rdx
	movb	%al, 8(%rsi)
	movq	%rdx, (%rsi)
	addq	$16, -216(%rbp)
.L2476:
	movq	0(%r13), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal5Debug15EnsureBreakInfoENS0_6HandleINS0_18SharedFunctionInfoEEE
	testb	%al, %al
	je	.L2474
	movq	0(%r13), %rsi
	movq	%rbx, %rdi
	addq	$8, %r13
	call	_ZN2v88internal5Debug32PrepareFunctionForDebugExecutionENS0_6HandleINS0_18SharedFunctionInfoEEE
	cmpq	%r13, %r14
	jne	.L2477
	movq	-280(%rbp), %r15
	movq	-336(%rbp), %r13
	testb	%r12b, %r12b
	jne	.L2567
	movq	%r14, -272(%rbp)
	movq	-328(%rbp), %r15
	leaq	-160(%rbp), %r12
	movq	%rbx, -280(%rbp)
	movl	-304(%rbp), %r14d
	movl	-248(%rbp), %ebx
	movq	%r13, -248(%rbp)
	movq	-344(%rbp), %r13
	.p2align 4,,10
	.p2align 3
.L2480:
	movq	0(%r13), %rax
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	%rax, -160(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo12HasBreakInfoEv@PLT
	movb	%al, -264(%rbp)
	testb	%al, %al
	je	.L2578
	movq	-280(%rbp), %rax
	movq	136(%rax), %rdx
	movq	0(%r13), %rax
	movq	41112(%rdx), %rdi
	movq	(%rax), %rax
	movq	31(%rax), %r8
	testq	%rdi, %rdi
	je	.L2484
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L2485:
	movq	%r12, %rdi
	call	_ZN2v88internal13BreakIteratorC1ENS0_6HandleINS0_9DebugInfoEEE
	cmpl	$-1, -112(%rbp)
	jne	.L2487
	jmp	.L2491
	.p2align 4,,10
	.p2align 3
.L2488:
	movq	%r12, %rdi
	call	_ZN2v88internal13BreakIterator4NextEv
	cmpl	$-1, -112(%rbp)
	je	.L2491
.L2487:
	movl	-148(%rbp), %eax
	cmpl	%eax, %ebx
	jle	.L2488
	cmpl	%eax, %r14d
	jg	.L2488
	movq	-248(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal13BreakIterator16GetBreakLocationEv
	movq	8(%r15), %rsi
	cmpq	16(%r15), %rsi
	je	.L2489
	movdqa	-192(%rbp), %xmm1
	movq	%r12, %rdi
	movups	%xmm1, (%rsi)
	movq	-176(%rbp), %rax
	movq	%rax, 16(%rsi)
	addq	$24, 8(%r15)
	call	_ZN2v88internal13BreakIterator4NextEv
	cmpl	$-1, -112(%rbp)
	jne	.L2487
	.p2align 4,,10
	.p2align 3
.L2491:
	addq	$8, %r13
	cmpq	%r13, -272(%rbp)
	jne	.L2480
	movzbl	-264(%rbp), %r15d
	jmp	.L2479
	.p2align 4,,10
	.p2align 3
.L2576:
	movq	-1(%rsi), %rsi
	cmpw	$91, 11(%rsi)
	jne	.L2456
	movq	31(%r15), %rsi
	testb	$1, %sil
	jne	.L2579
.L2459:
	movq	7(%r15), %rsi
	testb	$1, %sil
	jne	.L2580
.L2463:
	movq	7(%r15), %rsi
	movq	7(%rsi), %r8
.L2462:
	movq	3520(%rax), %rdi
	leaq	-37592(%rax), %rsi
	testq	%rdi, %rdi
	je	.L2464
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	jmp	.L2467
	.p2align 4,,10
	.p2align 3
.L2468:
	movq	%rax, -160(%rbp)
.L2572:
	movq	0(%r13), %rdi
	leaq	-160(%rbp), %rdx
	movl	$1, %esi
	movb	$0, -152(%rbp)
	call	_ZN2v88internal8Compiler7CompileENS0_6HandleINS0_18SharedFunctionInfoEEENS1_18ClearExceptionFlagEPNS0_15IsCompiledScopeE@PLT
	movl	%eax, %r12d
	testb	%al, %al
	jne	.L2473
.L2474:
	movzbl	-312(%rbp), %r15d
.L2479:
	movq	-224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2492
	call	_ZdlPv@PLT
.L2492:
	movq	-256(%rbp), %rax
	testq	%rax, %rax
	je	.L2493
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L2493:
	movq	-288(%rbp), %rax
	movq	-320(%rbp), %rcx
	subl	$1, 41104(%rax)
	movq	%rcx, 41088(%rax)
	movq	-296(%rbp), %rcx
	cmpq	41096(%rax), %rcx
	je	.L2412
	movq	%rcx, 41096(%rax)
	movq	%rax, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2412:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2581
	addq	$328, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2455:
	.cfi_restore_state
	movq	-1(%rsi), %rsi
	cmpw	$72, 11(%rsi)
	jne	.L2458
	movq	31(%r15), %rsi
	testb	$1, %sil
	je	.L2459
.L2579:
	movq	-1(%rsi), %rdi
	cmpw	$86, 11(%rdi)
	jne	.L2459
	movq	39(%rsi), %rdi
	testb	$1, %dil
	je	.L2459
	movq	-1(%rdi), %rdi
	cmpw	$72, 11(%rdi)
	jne	.L2459
	movq	31(%rsi), %r8
	jmp	.L2462
	.p2align 4,,10
	.p2align 3
.L2577:
	movq	-1(%rdx), %rsi
	cmpw	$165, 11(%rsi)
	je	.L2468
	movq	-1(%rdx), %rdx
	cmpw	$166, 11(%rdx)
	movq	%rax, -160(%rbp)
	jne	.L2573
	jmp	.L2572
	.p2align 4,,10
	.p2align 3
.L2475:
	leaq	-160(%rbp), %rdx
	leaq	-224(%rbp), %rdi
	call	_ZNSt6vectorIN2v88internal15IsCompiledScopeESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	jmp	.L2476
	.p2align 4,,10
	.p2align 3
.L2464:
	movq	41088(%rsi), %rax
	cmpq	41096(%rsi), %rax
	je	.L2582
.L2466:
	leaq	8(%rax), %rdi
	movq	%rdi, 41088(%rsi)
	movq	%r8, (%rax)
	jmp	.L2467
	.p2align 4,,10
	.p2align 3
.L2580:
	movq	-1(%rsi), %rsi
	cmpw	$72, 11(%rsi)
	jne	.L2463
	movq	7(%r15), %r8
	jmp	.L2462
	.p2align 4,,10
	.p2align 3
.L2575:
	movq	-192(%rbp), %rax
	movq	7(%rax), %rax
	testb	$1, %al
	jne	.L2431
.L2434:
	movq	-192(%rbp), %rax
	movabsq	$287762808832, %rcx
	movq	7(%rax), %rax
	cmpq	%rcx, %rax
	je	.L2433
	testb	$1, %al
	jne	.L2436
.L2435:
	movq	136(%rbx), %rcx
	movq	-192(%rbp), %rsi
	movq	41112(%rcx), %rdi
	testq	%rdi, %rdi
	je	.L2583
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
	cmpq	%r14, -280(%rbp)
	je	.L2441
.L2586:
	movq	%rdx, (%r14)
	addq	$8, %r14
	jmp	.L2428
	.p2align 4,,10
	.p2align 3
.L2574:
	movq	-1(%rax), %rdx
	cmpw	$86, 11(%rdx)
	je	.L2584
.L2430:
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	-37504(%rdx), %rax
	je	.L2428
	jmp	.L2429
	.p2align 4,,10
	.p2align 3
.L2583:
	movq	41088(%rcx), %rdx
	cmpq	41096(%rcx), %rdx
	je	.L2585
.L2440:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%rcx)
	movq	%rsi, (%rdx)
	cmpq	%r14, -280(%rbp)
	jne	.L2586
.L2441:
	movq	-280(%rbp), %rsi
	subq	-256(%rbp), %rsi
	movabsq	$1152921504606846975, %rcx
	movq	%rsi, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L2587
	testq	%rax, %rax
	je	.L2588
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	ja	.L2589
	testq	%rcx, %rcx
	jne	.L2590
	movq	$0, -280(%rbp)
	movl	$8, %ecx
	xorl	%eax, %eax
.L2446:
	movq	-256(%rbp), %r8
	movq	%rdx, (%rax,%rsi)
	cmpq	%r8, %r14
	je	.L2502
	leaq	-8(%r14), %rsi
	leaq	15(%rax), %rdx
	subq	%r8, %rsi
	subq	%r8, %rdx
	movq	%rsi, %rdi
	shrq	$3, %rdi
	cmpq	$30, %rdx
	jbe	.L2503
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rdi
	je	.L2503
	addq	$1, %rdi
	xorl	%edx, %edx
	movq	%rdi, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L2449:
	movdqu	(%r8,%rdx), %xmm2
	movups	%xmm2, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rcx
	jne	.L2449
	movq	%rdi, %r8
	movq	-256(%rbp), %rdx
	andq	$-2, %r8
	leaq	0(,%r8,8), %rcx
	addq	%rcx, %rdx
	addq	%rax, %rcx
	cmpq	%rdi, %r8
	je	.L2451
	movq	(%rdx), %rdx
	movq	%rdx, (%rcx)
.L2451:
	leaq	16(%rax,%rsi), %r14
.L2447:
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2452
	movq	%rax, -256(%rbp)
	call	_ZdlPv@PLT
	movq	-256(%rbp), %rax
.L2452:
	movq	%rax, -256(%rbp)
	jmp	.L2428
	.p2align 4,,10
	.p2align 3
.L2436:
	movq	-1(%rax), %rdx
	cmpw	$165, 11(%rdx)
	je	.L2433
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L2435
	.p2align 4,,10
	.p2align 3
.L2433:
	movq	-192(%rbp), %rax
	movl	47(%rax), %eax
	testb	$16, %ah
	jne	.L2435
	jmp	.L2428
	.p2align 4,,10
	.p2align 3
.L2413:
	call	_ZN2v88internal5Debug30FindSharedFunctionInfoInScriptENS0_6HandleINS0_6ScriptEEEi
	movq	%rax, %r12
	movq	136(%rbx), %rax
	movq	(%r12), %rcx
	cmpq	%rcx, 88(%rax)
	je	.L2417
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal5Debug15EnsureBreakInfoENS0_6HandleINS0_18SharedFunctionInfoEEE
	movl	%eax, %r15d
	testb	%al, %al
	jne	.L2591
.L2417:
	xorl	%r15d, %r15d
	jmp	.L2412
	.p2align 4,,10
	.p2align 3
.L2582:
	movq	%rsi, %rdi
	movq	%r8, -360(%rbp)
	movq	%rsi, -352(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-360(%rbp), %r8
	movq	-352(%rbp), %rsi
	jmp	.L2466
	.p2align 4,,10
	.p2align 3
.L2584:
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L2430
	jmp	.L2429
	.p2align 4,,10
	.p2align 3
.L2431:
	movq	-1(%rax), %rax
	cmpw	$83, 11(%rax)
	jne	.L2434
	jmp	.L2428
	.p2align 4,,10
	.p2align 3
.L2567:
	movq	-224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2481
	call	_ZdlPv@PLT
.L2481:
	movq	-256(%rbp), %rax
	testq	%rax, %rax
	je	.L2482
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L2482:
	movq	-288(%rbp), %rax
	movq	-320(%rbp), %rcx
	subl	$1, 41104(%rax)
	movq	%rcx, 41088(%rax)
	movq	-296(%rbp), %rcx
	cmpq	41096(%rax), %rcx
	je	.L2414
	movq	%rcx, 41096(%rax)
	movq	%rax, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	jmp	.L2414
.L2591:
	movq	%rbx, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal5Debug32PrepareFunctionForDebugExecutionENS0_6HandleINS0_18SharedFunctionInfoEEE
	movq	136(%rbx), %rbx
	movq	(%r12), %rax
	movq	41112(%rbx), %rdi
	movq	31(%rax), %r12
	testq	%rdi, %rdi
	je	.L2418
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L2419:
	leaq	-160(%rbp), %r12
	leaq	-192(%rbp), %rbx
	movq	%r12, %rdi
	call	_ZN2v88internal13BreakIteratorC1ENS0_6HandleINS0_9DebugInfoEEE
	cmpl	$-1, -112(%rbp)
	je	.L2412
	movb	%r15b, -256(%rbp)
	movq	-328(%rbp), %r13
	movl	-248(%rbp), %r15d
	jmp	.L2421
	.p2align 4,,10
	.p2align 3
.L2422:
	movq	%r12, %rdi
	call	_ZN2v88internal13BreakIterator4NextEv
	cmpl	$-1, -112(%rbp)
	je	.L2592
.L2421:
	movl	-148(%rbp), %eax
	cmpl	%eax, %r14d
	jg	.L2422
	cmpl	%eax, %r15d
	jle	.L2422
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal13BreakIterator16GetBreakLocationEv
	movq	8(%r13), %rsi
	cmpq	16(%r13), %rsi
	je	.L2423
	movdqa	-192(%rbp), %xmm3
	movups	%xmm3, (%rsi)
	movq	-176(%rbp), %rax
	movq	%rax, 16(%rsi)
	addq	$24, 8(%r13)
	jmp	.L2422
	.p2align 4,,10
	.p2align 3
.L2484:
	movq	41088(%rdx), %rsi
	cmpq	41096(%rdx), %rsi
	je	.L2593
.L2486:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rdx)
	movq	%r8, (%rsi)
	jmp	.L2485
	.p2align 4,,10
	.p2align 3
.L2489:
	movq	-248(%rbp), %rdx
	movq	%r15, %rdi
	call	_ZNSt6vectorIN2v88internal13BreakLocationESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	jmp	.L2488
	.p2align 4,,10
	.p2align 3
.L2578:
	leaq	.LC14(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2593:
	movq	%rdx, %rdi
	movq	%r8, -312(%rbp)
	movq	%rdx, -304(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-312(%rbp), %r8
	movq	-304(%rbp), %rdx
	movq	%rax, %rsi
	jmp	.L2486
.L2585:
	movq	%rcx, %rdi
	movq	%rsi, -344(%rbp)
	movq	%rcx, -336(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-344(%rbp), %rsi
	movq	-336(%rbp), %rcx
	movq	%rax, %rdx
	jmp	.L2440
.L2418:
	movq	41088(%rbx), %rsi
	cmpq	41096(%rbx), %rsi
	je	.L2594
.L2420:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rbx)
	movq	%r12, (%rsi)
	jmp	.L2419
.L2590:
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rcx
	cmova	%rax, %rcx
	salq	$3, %rcx
.L2444:
	movq	%rcx, %rdi
	movq	%rsi, -344(%rbp)
	movq	%rdx, -336(%rbp)
	movq	%rcx, -280(%rbp)
	call	_Znwm@PLT
	movq	-280(%rbp), %rcx
	movq	-336(%rbp), %rdx
	movq	-344(%rbp), %rsi
	addq	%rax, %rcx
	movq	%rcx, -280(%rbp)
	leaq	8(%rax), %rcx
	jmp	.L2446
.L2592:
	movzbl	-256(%rbp), %r15d
	jmp	.L2412
.L2588:
	movl	$8, %ecx
	jmp	.L2444
.L2589:
	movabsq	$9223372036854775800, %rcx
	jmp	.L2444
.L2427:
	movq	$0, -256(%rbp)
.L2454:
	movl	$1, %r15d
	jmp	.L2479
.L2503:
	movq	-256(%rbp), %rdx
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L2448:
	movq	(%rdx), %rdi
	addq	$8, %rdx
	addq	$8, %rcx
	movq	%rdi, -8(%rcx)
	cmpq	%r14, %rdx
	jne	.L2448
	jmp	.L2451
.L2423:
	movq	%rbx, %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIN2v88internal13BreakLocationESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	jmp	.L2422
.L2502:
	movq	%rcx, %r14
	jmp	.L2447
.L2594:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L2420
.L2587:
	leaq	.LC7(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L2581:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22965:
	.size	_ZN2v88internal5Debug22GetPossibleBreakpointsENS0_6HandleINS0_6ScriptEEEiibPSt6vectorINS0_13BreakLocationESaIS6_EE, .-_ZN2v88internal5Debug22GetPossibleBreakpointsENS0_6HandleINS0_6ScriptEEEiibPSt6vectorINS0_13BreakLocationESaIS6_EE
	.section	.text._ZN2v88internal13BreakLocation21AllAtCurrentStatementENS0_6HandleINS0_9DebugInfoEEEPNS0_15JavaScriptFrameEPSt6vectorIS1_SaIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13BreakLocation21AllAtCurrentStatementENS0_6HandleINS0_9DebugInfoEEEPNS0_15JavaScriptFrameEPSt6vectorIS1_SaIS1_EE
	.type	_ZN2v88internal13BreakLocation21AllAtCurrentStatementENS0_6HandleINS0_9DebugInfoEEEPNS0_15JavaScriptFrameEPSt6vectorIS1_SaIS1_EE, @function
_ZN2v88internal13BreakLocation21AllAtCurrentStatementENS0_6HandleINS0_9DebugInfoEEEPNS0_15JavaScriptFrameEPSt6vectorIS1_SaIS1_EE:
.LFB22821:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-256(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-160(%rbp), %r12
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -272(%rbp)
	movq	%rdi, -280(%rbp)
	movq	%r12, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal12FrameSummary6GetTopEPKNS0_13StandardFrameE@PLT
	movq	-128(%rbp), %r13
	movq	%r12, %rdi
	movl	-120(%rbp), %ebx
	call	_ZN2v88internal12FrameSummaryD1Ev@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	0(%r13), %rax
	movq	-1(%rax), %rax
	cmpw	$69, 11(%rax)
	sete	%al
	movzbl	%al, %eax
	subl	%eax, %ebx
	call	_ZN2v88internal13BreakIteratorC1ENS0_6HandleINS0_9DebugInfoEEE
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal13BreakIteratorC1ENS0_6HandleINS0_9DebugInfoEEE
	cmpl	$-1, -112(%rbp)
	je	.L2607
	movl	-152(%rbp), %eax
	movl	-104(%rbp), %edx
	movl	$0, -260(%rbp)
	movl	$2147483647, %r13d
	leaq	-136(%rbp), %r15
	jmp	.L2625
	.p2align 4,,10
	.p2align 3
.L2657:
	movl	$108543, %esi
	btq	%rax, %rsi
	jnc	.L2605
.L2655:
	movl	-152(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -152(%rbp)
.L2625:
	cmpl	%edx, %ebx
	jl	.L2598
	movl	%ebx, %ecx
	subl	%edx, %ecx
	cmpl	%r13d, %ecx
	jge	.L2598
	testl	%ecx, %ecx
	je	.L2628
	movl	%eax, -260(%rbp)
	movl	%ecx, %r13d
.L2598:
	cmpl	$-1, %eax
	jne	.L2606
	.p2align 4,,10
	.p2align 3
.L2626:
	movq	-96(%rbp), %rax
	movzbl	-88(%rbp), %ecx
	shrq	%rax
	andl	$1073741823, %eax
	subl	$1, %eax
	movl	%eax, -148(%rbp)
	testb	%cl, %cl
	je	.L2600
	movl	%eax, -144(%rbp)
.L2600:
	movq	-160(%rbp), %rax
	movq	(%rax), %rax
	movq	31(%rax), %rsi
	leal	54(%rdx), %eax
	cltq
	movzbl	-1(%rsi,%rax), %eax
	cmpb	$3, %al
	ja	.L2601
	leal	55(%rdx), %eax
	cltq
	movzbl	-1(%rsi,%rax), %eax
.L2601:
	leal	85(%rax), %esi
	andl	$247, %esi
	je	.L2655
	cmpb	$-80, %al
	je	.L2655
	subl	$86, %eax
	cmpb	$16, %al
	jbe	.L2657
.L2605:
	testb	%cl, %cl
	jne	.L2655
.L2606:
	movq	%r15, %rdi
	call	_ZN2v88internal27SourcePositionTableIterator7AdvanceEv@PLT
	cmpl	$-1, -112(%rbp)
	je	.L2599
	movl	-104(%rbp), %edx
	jmp	.L2626
	.p2align 4,,10
	.p2align 3
.L2661:
	movl	-248(%rbp), %eax
.L2610:
	addl	$1, %eax
	subl	$1, %ebx
	movl	%eax, -248(%rbp)
	cmpl	$-1, %ebx
	jne	.L2608
.L2607:
	movq	-280(%rbp), %rsi
	movq	%r12, %rdi
	movl	-240(%rbp), %ebx
	leaq	-136(%rbp), %r13
	call	_ZN2v88internal13BreakIteratorC1ENS0_6HandleINS0_9DebugInfoEEE
	cmpl	$-1, -112(%rbp)
	je	.L2595
	movl	-152(%rbp), %eax
	cmpl	-144(%rbp), %ebx
	je	.L2658
.L2624:
	cmpl	$-1, %eax
	jne	.L2623
	.p2align 4,,10
	.p2align 3
.L2620:
	movq	-96(%rbp), %rax
	shrq	%rax
	andl	$1073741823, %eax
	subl	$1, %eax
	cmpb	$0, -88(%rbp)
	movl	%eax, -148(%rbp)
	je	.L2621
	movl	%eax, -144(%rbp)
.L2621:
	movq	%r12, %rdi
	call	_ZN2v88internal13BreakIterator17GetDebugBreakTypeEv
	testl	%eax, %eax
	jne	.L2659
.L2623:
	movq	%r13, %rdi
	call	_ZN2v88internal27SourcePositionTableIterator7AdvanceEv@PLT
	cmpl	$-1, -112(%rbp)
	jne	.L2620
.L2595:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2660
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2659:
	.cfi_restore_state
	addl	$1, -152(%rbp)
	movl	-152(%rbp), %eax
	cmpl	-144(%rbp), %ebx
	jne	.L2624
.L2658:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal13BreakIterator16GetBreakLocationEv
	movq	-272(%rbp), %rdx
	movq	8(%rdx), %rsi
	cmpq	16(%rdx), %rsi
	je	.L2617
	movdqa	-256(%rbp), %xmm0
	movups	%xmm0, (%rsi)
	movq	-240(%rbp), %rax
	movq	%rax, 16(%rsi)
	addq	$24, 8(%rdx)
.L2618:
	cmpl	$-1, -112(%rbp)
	movl	-152(%rbp), %eax
	jne	.L2624
	jmp	.L2595
	.p2align 4,,10
	.p2align 3
.L2628:
	movl	%eax, -260(%rbp)
	.p2align 4,,10
	.p2align 3
.L2599:
	movl	-260(%rbp), %eax
	leaq	-232(%rbp), %r13
	leal	-1(%rax), %ebx
	testl	%eax, %eax
	jle	.L2607
	.p2align 4,,10
	.p2align 3
.L2608:
	cmpl	$-1, -208(%rbp)
	movl	-248(%rbp), %eax
	je	.L2610
	cmpl	$-1, %eax
	jne	.L2614
	.p2align 4,,10
	.p2align 3
.L2611:
	movq	-192(%rbp), %rax
	shrq	%rax
	andl	$1073741823, %eax
	subl	$1, %eax
	cmpb	$0, -184(%rbp)
	movl	%eax, -244(%rbp)
	je	.L2613
	movl	%eax, -240(%rbp)
.L2613:
	movq	%r14, %rdi
	call	_ZN2v88internal13BreakIterator17GetDebugBreakTypeEv
	testl	%eax, %eax
	jne	.L2661
.L2614:
	movq	%r13, %rdi
	call	_ZN2v88internal27SourcePositionTableIterator7AdvanceEv@PLT
	cmpl	$-1, -208(%rbp)
	jne	.L2611
	subl	$1, %ebx
	cmpl	$-1, %ebx
	jne	.L2608
	jmp	.L2607
	.p2align 4,,10
	.p2align 3
.L2617:
	movq	%rdx, %rdi
	movq	%r14, %rdx
	call	_ZNSt6vectorIN2v88internal13BreakLocationESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	jmp	.L2618
.L2660:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22821:
	.size	_ZN2v88internal13BreakLocation21AllAtCurrentStatementENS0_6HandleINS0_9DebugInfoEEEPNS0_15JavaScriptFrameEPSt6vectorIS1_SaIS1_EE, .-_ZN2v88internal13BreakLocation21AllAtCurrentStatementENS0_6HandleINS0_9DebugInfoEEEPNS0_15JavaScriptFrameEPSt6vectorIS1_SaIS1_EE
	.section	.text._ZN2v88internal5Debug24IsMutedAtCurrentLocationEPNS0_15JavaScriptFrameE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug24IsMutedAtCurrentLocationEPNS0_15JavaScriptFrameE
	.type	_ZN2v88internal5Debug24IsMutedAtCurrentLocationEPNS0_15JavaScriptFrameE, @function
_ZN2v88internal5Debug24IsMutedAtCurrentLocationEPNS0_15JavaScriptFrameE:
.LFB22855:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-160(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$360, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	136(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	41088(%r12), %rax
	addl	$1, 41104(%r12)
	movq	%rax, -368(%rbp)
	movq	41096(%r12), %rax
	movq	%rax, -360(%rbp)
	leaq	-304(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -376(%rbp)
	call	_ZN2v88internal12FrameSummary6GetTopEPKNS0_13StandardFrameE@PLT
	movq	-280(%rbp), %rbx
	movq	%r14, %rdi
	movq	(%rbx), %rax
	movq	23(%rax), %rax
	movq	%rax, -160(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo12HasBreakInfoEv@PLT
	movb	%al, -344(%rbp)
	testb	%al, %al
	jne	.L2690
.L2663:
	movq	-376(%rbp), %rdi
	call	_ZN2v88internal12FrameSummaryD1Ev@PLT
	movq	-368(%rbp), %rax
	subl	$1, 41104(%r12)
	movq	%rax, 41088(%r12)
	movq	-360(%rbp), %rax
	cmpq	41096(%r12), %rax
	je	.L2662
	movq	%rax, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2662:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2691
	movzbl	-344(%rbp), %eax
	addq	$360, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2690:
	.cfi_restore_state
	movq	136(%r13), %rdx
	movq	(%rbx), %rax
	movq	23(%rax), %rax
	movq	41112(%rdx), %rdi
	movq	31(%rax), %rsi
	testq	%rdi, %rdi
	je	.L2664
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rbx
.L2665:
	leaq	-240(%rbp), %rax
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -384(%rbp)
	call	_ZN2v88internal10DebugScopeC1EPNS0_5DebugE
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdi
	movq	%r15, %rsi
	leaq	-336(%rbp), %rdx
	movaps	%xmm0, -336(%rbp)
	movq	$0, -320(%rbp)
	call	_ZN2v88internal13BreakLocation21AllAtCurrentStatementENS0_6HandleINS0_9DebugInfoEEEPNS0_15JavaScriptFrameEPSt6vectorIS1_SaIS1_EE
	movq	-336(%rbp), %rdi
	movq	-328(%rbp), %rdx
	cmpq	%rdx, %rdi
	je	.L2680
	xorl	%r15d, %r15d
	movq	%r12, -392(%rbp)
	movb	$0, -344(%rbp)
	movq	%r15, %r12
	jmp	.L2675
	.p2align 4,,10
	.p2align 3
.L2674:
	movq	%rdx, %rax
	addq	$1, %r12
	movabsq	$-6148914691236517205, %rcx
	subq	%rdi, %rax
	sarq	$3, %rax
	imulq	%rcx, %rax
	cmpq	%rax, %r12
	jnb	.L2692
.L2675:
	cmpb	$0, 13(%r13)
	je	.L2674
	leaq	(%r12,%r12,2), %rax
	movq	136(%r13), %rsi
	leaq	(%rdi,%rax,8), %r15
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	%rax, -160(%rbp)
	movl	16(%r15), %edx
	call	_ZN2v88internal9DebugInfo13HasBreakPointEPNS0_7IsolateEi@PLT
	testb	%al, %al
	je	.L2689
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	%rax, -160(%rbp)
	call	_ZNK2v88internal9DebugInfo15CanBreakAtEntryEv@PLT
	testb	%al, %al
	je	.L2670
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	%rax, -160(%rbp)
	call	_ZNK2v88internal9DebugInfo12BreakAtEntryEv@PLT
	movb	%al, -352(%rbp)
.L2671:
	cmpb	$0, -352(%rbp)
	je	.L2689
	movl	16(%r15), %edx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal5Debug17GetHitBreakPointsENS0_6HandleINS0_9DebugInfoEEEi
	testq	%rax, %rax
	jne	.L2673
	movzbl	-352(%rbp), %eax
	movq	-336(%rbp), %rdi
	movq	-328(%rbp), %rdx
	movb	%al, -344(%rbp)
	jmp	.L2674
	.p2align 4,,10
	.p2align 3
.L2689:
	movq	-336(%rbp), %rdi
	movq	-328(%rbp), %rdx
	jmp	.L2674
	.p2align 4,,10
	.p2align 3
.L2670:
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal13BreakIteratorC1ENS0_6HandleINS0_9DebugInfoEEE
	movl	16(%r15), %esi
	movq	%r14, %rdi
	call	_ZN2v88internal13BreakIterator14SkipToPositionEi
	movl	8(%r15), %eax
	cmpl	%eax, -104(%rbp)
	sete	-352(%rbp)
	jmp	.L2671
	.p2align 4,,10
	.p2align 3
.L2692:
	movq	-392(%rbp), %r12
.L2667:
	testq	%rdi, %rdi
	je	.L2676
	call	_ZdlPv@PLT
.L2676:
	movq	-384(%rbp), %rdi
	call	_ZN2v88internal10DebugScopeD1Ev
	jmp	.L2663
	.p2align 4,,10
	.p2align 3
.L2664:
	movq	41088(%rdx), %rbx
	cmpq	41096(%rdx), %rbx
	je	.L2693
.L2666:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%rbx)
	jmp	.L2665
	.p2align 4,,10
	.p2align 3
.L2673:
	movb	$0, -344(%rbp)
	movq	-392(%rbp), %r12
	movq	-336(%rbp), %rdi
	jmp	.L2667
.L2693:
	movq	%rdx, %rdi
	movq	%rsi, -352(%rbp)
	movq	%rdx, -344(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-352(%rbp), %rsi
	movq	-344(%rbp), %rdx
	movq	%rax, %rbx
	jmp	.L2666
.L2680:
	movb	$0, -344(%rbp)
	jmp	.L2667
.L2691:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22855:
	.size	_ZN2v88internal5Debug24IsMutedAtCurrentLocationEPNS0_15JavaScriptFrameE, .-_ZN2v88internal5Debug24IsMutedAtCurrentLocationEPNS0_15JavaScriptFrameE
	.section	.text._ZN2v88internal5Debug11OnExceptionENS0_6HandleINS0_6ObjectEEES4_NS_5debug13ExceptionTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug11OnExceptionENS0_6HandleINS0_6ObjectEEES4_NS_5debug13ExceptionTypeE
	.type	_ZN2v88internal5Debug11OnExceptionENS0_6HandleINS0_6ObjectEEES4_NS_5debug13ExceptionTypeE, @function
_ZN2v88internal5Debug11OnExceptionENS0_6HandleINS0_6ObjectEEES4_NS_5debug13ExceptionTypeE:
.LFB23005:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$1528, %rsp
	movl	%ecx, -1524(%rbp)
	movq	136(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal7Isolate23PredictExceptionCatcherEv@PLT
	cmpl	$3, %eax
	je	.L2694
	testl	%eax, %eax
	movq	(%r12), %rax
	sete	%r14b
	testb	$1, %al
	jne	.L2722
.L2697:
	cmpq	$0, (%rbx)
	je	.L2694
	testb	%r14b, %r14b
	jne	.L2723
.L2699:
	cmpb	$0, 14(%rbx)
	je	.L2694
.L2700:
	movq	136(%rbx), %rsi
	leaq	-1504(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateE@PLT
	cmpq	$0, -88(%rbp)
	je	.L2694
	movq	%r15, %rdi
	call	_ZN2v88internal23JavaScriptFrameIterator7AdvanceEv@PLT
	movq	-88(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L2694
	movq	%rbx, %rdi
	call	_ZN2v88internal5Debug24IsMutedAtCurrentLocationEPNS0_15JavaScriptFrameE
	testb	%al, %al
	je	.L2724
	.p2align 4,,10
	.p2align 3
.L2694:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2725
	addq	$1528, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2723:
	.cfi_restore_state
	cmpb	$0, 15(%rbx)
	jne	.L2700
	jmp	.L2699
	.p2align 4,,10
	.p2align 3
.L2722:
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L2697
	movq	136(%rbx), %rdi
	movl	$1, %r9d
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	leaq	3776(%rdi), %rdx
	movq	%rdx, %rcx
	call	_ZN2v88internal6Object11SetPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEES5_NS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE@PLT
	movq	136(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal7Isolate34PromiseHasUserDefinedRejectHandlerENS0_6HandleINS0_6ObjectEEE@PLT
	xorl	$1, %eax
	movl	%eax, %r14d
	jmp	.L2697
	.p2align 4,,10
	.p2align 3
.L2724:
	movzbl	%r14b, %r14d
	movq	%rbx, %rdi
	movl	%r14d, %esi
	call	_ZN2v88internal5Debug21IsExceptionBlackboxedEb
	testb	%al, %al
	jne	.L2694
	cmpq	$0, -88(%rbp)
	je	.L2694
	movq	136(%rbx), %rax
	xorl	%esi, %esi
	leaq	-1512(%rbp), %rdi
	movq	%rax, -1512(%rbp)
	call	_ZNK2v88internal15StackLimitCheck15JsHasOverflowedEm@PLT
	testb	%al, %al
	jne	.L2694
	movq	%r15, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal10DebugScopeC1EPNS0_5DebugE
	movq	136(%rbx), %r11
	movq	41088(%r11), %rax
	addl	$1, 41104(%r11)
	movq	136(%rbx), %rdx
	movzbl	12(%rbx), %r10d
	movq	%rax, -1544(%rbp)
	movq	41096(%r11), %rax
	movb	$1, 12(%rbx)
	movq	%rax, -1536(%rbp)
	movq	12464(%rdx), %rax
	movq	39(%rax), %r8
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L2703
	movq	%r8, %rsi
	movb	%r10b, -1560(%rbp)
	movq	%r11, -1552(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-1552(%rbp), %r11
	movzbl	-1560(%rbp), %r10d
	movq	%rax, %rsi
.L2704:
	movq	(%rbx), %rdi
	leaq	_ZN2v85debug13DebugDelegate15ExceptionThrownENS_5LocalINS_7ContextEEENS2_INS_5ValueEEES6_bNS0_13ExceptionTypeE(%rip), %rdx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2726
.L2706:
	movq	-1544(%rbp), %rax
	movb	%r10b, 12(%rbx)
	subl	$1, 41104(%r11)
	movq	%rax, 41088(%r11)
	movq	-1536(%rbp), %rax
	cmpq	41096(%r11), %rax
	je	.L2709
	movq	%rax, 41096(%r11)
	movq	%r11, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2709:
	movq	%r15, %rdi
	call	_ZN2v88internal10DebugScopeD1Ev
	jmp	.L2694
	.p2align 4,,10
	.p2align 3
.L2703:
	movq	41088(%rdx), %rsi
	cmpq	41096(%rdx), %rsi
	je	.L2727
.L2705:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rdx)
	movq	%r8, (%rsi)
	jmp	.L2704
	.p2align 4,,10
	.p2align 3
.L2726:
	movb	%r10b, -1560(%rbp)
	movl	%r14d, %r8d
	movq	%r12, %rcx
	movq	%r13, %rdx
	movq	%r11, -1552(%rbp)
	movl	-1524(%rbp), %r9d
	call	*%rax
	movq	-1552(%rbp), %r11
	movzbl	-1560(%rbp), %r10d
	jmp	.L2706
.L2727:
	movq	%rdx, %rdi
	movq	%r8, -1568(%rbp)
	movb	%r10b, -1525(%rbp)
	movq	%r11, -1560(%rbp)
	movq	%rdx, -1552(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-1568(%rbp), %r8
	movzbl	-1525(%rbp), %r10d
	movq	-1560(%rbp), %r11
	movq	-1552(%rbp), %rdx
	movq	%rax, %rsi
	jmp	.L2705
.L2725:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23005:
	.size	_ZN2v88internal5Debug11OnExceptionENS0_6HandleINS0_6ObjectEEES4_NS_5debug13ExceptionTypeE, .-_ZN2v88internal5Debug11OnExceptionENS0_6HandleINS0_6ObjectEEES4_NS_5debug13ExceptionTypeE
	.section	.text._ZN2v88internal5Debug15OnPromiseRejectENS0_6HandleINS0_6ObjectEEES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug15OnPromiseRejectENS0_6HandleINS0_6ObjectEEES4_
	.type	_ZN2v88internal5Debug15OnPromiseRejectENS0_6HandleINS0_6ObjectEEES4_, @function
_ZN2v88internal5Debug15OnPromiseRejectENS0_6HandleINS0_6ObjectEEES4_:
.LFB23002:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	64(%rdi), %rax
	testq	%rax, %rax
	jne	.L2728
	cmpb	$0, 10(%rdi)
	movq	%rdi, %r12
	je	.L2743
.L2728:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2744
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2743:
	.cfi_restore_state
	cmpb	$0, 8(%rdi)
	je	.L2728
	movq	136(%rdi), %r15
	cmpl	$32, 41828(%r15)
	je	.L2728
	movq	41088(%r15), %rax
	movq	41096(%r15), %rbx
	movq	%rsi, %r14
	movq	%rdx, %r13
	addl	$1, 41104(%r15)
	movq	%rax, -152(%rbp)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L2745
.L2730:
	movl	$1, %ecx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal5Debug11OnExceptionENS0_6HandleINS0_6ObjectEEES4_NS_5debug13ExceptionTypeE
.L2736:
	movq	-152(%rbp), %rax
	subl	$1, 41104(%r15)
	movq	%rax, 41088(%r15)
	cmpq	41096(%r15), %rbx
	je	.L2728
	movq	%rbx, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	jmp	.L2728
	.p2align 4,,10
	.p2align 3
.L2745:
	movq	136(%rdi), %rdx
	movq	-1(%rax), %rcx
	cmpw	$1024, 11(%rcx)
	jbe	.L2730
	movq	3776(%rdx), %rcx
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	movl	$2, %eax
	movq	-1(%rcx), %rsi
	subq	$37592, %rdi
	cmpw	$64, 11(%rsi)
	jne	.L2732
	xorl	%eax, %eax
	testb	$1, 11(%rcx)
	sete	%al
	addl	%eax, %eax
.L2732:
	movl	%eax, -144(%rbp)
	leaq	3776(%rdx), %rsi
	movabsq	$824633720832, %rax
	movq	%rax, -132(%rbp)
	movq	3776(%rdx), %rax
	movq	%rdi, -120(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L2746
.L2733:
	leaq	-144(%rbp), %rdi
	movq	%rsi, -112(%rbp)
	movq	%rdi, -160(%rbp)
	movq	$0, -104(%rbp)
	movq	%r14, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%r14, -80(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -140(%rbp)
	movq	-160(%rbp), %rdi
	jne	.L2734
	movq	-120(%rbp), %rax
	addq	$88, %rax
.L2735:
	movq	136(%r12), %rdx
	movq	(%rax), %rax
	cmpq	%rax, 88(%rdx)
	jne	.L2736
	jmp	.L2730
.L2734:
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE@PLT
	jmp	.L2735
.L2746:
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rsi
	jmp	.L2733
.L2744:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23002:
	.size	_ZN2v88internal5Debug15OnPromiseRejectENS0_6HandleINS0_6ObjectEEES4_, .-_ZN2v88internal5Debug15OnPromiseRejectENS0_6HandleINS0_6ObjectEEES4_
	.section	.text._ZN2v88internal5Debug7OnThrowENS0_6HandleINS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug7OnThrowENS0_6HandleINS0_6ObjectEEE
	.type	_ZN2v88internal5Debug7OnThrowENS0_6HandleINS0_6ObjectEEE, @function
_ZN2v88internal5Debug7OnThrowENS0_6HandleINS0_6ObjectEEE:
.LFB23001:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rax
	testq	%rax, %rax
	jne	.L2762
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	cmpb	$0, 10(%rdi)
	je	.L2765
.L2747:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2765:
	.cfi_restore_state
	cmpb	$0, 8(%rdi)
	je	.L2747
	movq	136(%rdi), %r15
	cmpl	$32, 41828(%r15)
	je	.L2747
	addl	$1, 41104(%r15)
	movq	41088(%r15), %rax
	xorl	%ebx, %ebx
	movq	%rsi, %r13
	movq	136(%rdi), %rdi
	movq	41096(%r15), %r14
	movq	%rax, -56(%rbp)
	movq	12552(%rdi), %rsi
	cmpq	%rsi, 96(%rdi)
	je	.L2750
	movq	41112(%rdi), %r9
	testq	%r9, %r9
	je	.L2751
	movq	%r9, %rdi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rbx
.L2752:
	movq	136(%r12), %rax
	movq	96(%rax), %rdx
	movq	%rdx, 12552(%rax)
	movq	136(%r12), %rdi
.L2750:
	call	_ZN2v88internal7Isolate24GetPromiseOnStackOnThrowEv@PLT
	xorl	%ecx, %ecx
	movq	%rax, %rdx
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L2766
.L2754:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal5Debug11OnExceptionENS0_6HandleINS0_6ObjectEEES4_NS_5debug13ExceptionTypeE
	testq	%rbx, %rbx
	je	.L2755
	movq	136(%r12), %rax
	movq	(%rbx), %rdx
	movq	%rdx, 12552(%rax)
.L2755:
	movq	%r12, %rdi
	call	_ZN2v88internal5Debug18PrepareStepOnThrowEv
	movq	-56(%rbp), %rax
	subl	$1, 41104(%r15)
	movq	%rax, 41088(%r15)
	cmpq	41096(%r15), %r14
	je	.L2747
	movq	%r14, 41096(%r15)
	addq	$40, %rsp
	movq	%r15, %rdi
	popq	%rbx
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	.p2align 4,,10
	.p2align 3
.L2762:
	ret
	.p2align 4,,10
	.p2align 3
.L2751:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	41088(%rdi), %rbx
	cmpq	41096(%rdi), %rbx
	je	.L2767
.L2753:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%rdi)
	movq	%rsi, (%rbx)
	jmp	.L2752
	.p2align 4,,10
	.p2align 3
.L2766:
	movq	-1(%rax), %rax
	xorl	%ecx, %ecx
	cmpw	$1074, 11(%rax)
	sete	%cl
	jmp	.L2754
.L2767:
	movq	%rsi, -72(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	-64(%rbp), %rdi
	movq	%rax, %rbx
	jmp	.L2753
	.cfi_endproc
.LFE23001:
	.size	_ZN2v88internal5Debug7OnThrowENS0_6HandleINS0_6ObjectEEE, .-_ZN2v88internal5Debug7OnThrowENS0_6HandleINS0_6ObjectEEE
	.section	.text._ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_,"axG",@progbits,_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	.type	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_, @function
_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_:
.LFB28740:
	.cfi_startproc
	endbr64
	movabsq	$2305843009213693951, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$2, %rax
	cmpq	%rcx, %rax
	je	.L2782
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L2778
	movabsq	$9223372036854775804, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L2783
.L2770:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L2777:
	movl	(%r15), %eax
	subq	%r8, %r13
	leaq	4(%rbx,%rdx), %r15
	movl	%eax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L2784
	testq	%r13, %r13
	jg	.L2773
	testq	%r9, %r9
	jne	.L2776
.L2774:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2784:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L2773
.L2776:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L2774
	.p2align 4,,10
	.p2align 3
.L2783:
	testq	%rsi, %rsi
	jne	.L2771
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L2777
	.p2align 4,,10
	.p2align 3
.L2773:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L2774
	jmp	.L2776
	.p2align 4,,10
	.p2align 3
.L2778:
	movl	$4, %r14d
	jmp	.L2770
.L2782:
	leaq	.LC7(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L2771:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$2, %r14
	jmp	.L2770
	.cfi_endproc
.LFE28740:
	.size	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_, .-_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	.section	.text._ZN2v88internal5Debug12OnDebugBreakENS0_6HandleINS0_10FixedArrayEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug12OnDebugBreakENS0_6HandleINS0_10FixedArrayEEE
	.type	_ZN2v88internal5Debug12OnDebugBreakENS0_6HandleINS0_10FixedArrayEEE, @function
_ZN2v88internal5Debug12OnDebugBreakENS0_6HandleINS0_10FixedArrayEEE:
.LFB23006:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 10(%rdi)
	jne	.L2785
	cmpb	$0, 8(%rdi)
	movq	%rdi, %rbx
	jne	.L2803
.L2785:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2804
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2803:
	.cfi_restore_state
	movq	136(%rdi), %r14
	cmpl	$32, 41828(%r14)
	je	.L2785
	cmpq	$0, (%rdi)
	je	.L2785
	movq	41088(%r14), %rax
	pxor	%xmm0, %xmm0
	movq	%rsi, %r13
	addl	$1, 41104(%r14)
	movq	$0, -64(%rbp)
	movq	%rax, -120(%rbp)
	movq	41096(%r14), %rax
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -104(%rbp)
	movzbl	12(%rdi), %eax
	movb	$1, 12(%rdi)
	movq	(%rsi), %rdx
	movb	%al, -105(%rbp)
	movl	11(%rdx), %eax
	testl	%eax, %eax
	jle	.L2787
	xorl	%r12d, %r12d
	leaq	-84(%rbp), %r15
	.p2align 4,,10
	.p2align 3
.L2790:
	movq	15(%rdx,%r12,8), %rdx
	movq	-72(%rbp), %rsi
	movslq	11(%rdx), %rdx
	movl	%edx, -84(%rbp)
	cmpq	-64(%rbp), %rsi
	je	.L2788
	movl	%edx, (%rsi)
	movq	0(%r13), %rdx
	addq	$1, %r12
	addq	$4, -72(%rbp)
	cmpl	%r12d, 11(%rdx)
	jg	.L2790
.L2787:
	movq	136(%rbx), %r12
	movq	12464(%r12), %rax
	movq	39(%rax), %r9
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2791
	movq	%r9, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L2792:
	movq	(%rbx), %rdi
	leaq	_ZN2v85debug13DebugDelegate21BreakProgramRequestedENS_5LocalINS_7ContextEEERKSt6vectorIiSaIiEE(%rip), %rdx
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2805
.L2794:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2795
	call	_ZdlPv@PLT
.L2795:
	movzbl	-105(%rbp), %eax
	movb	%al, 12(%rbx)
	movq	-120(%rbp), %rax
	subl	$1, 41104(%r14)
	movq	%rax, 41088(%r14)
	movq	-104(%rbp), %rax
	cmpq	41096(%r14), %rax
	je	.L2785
	movq	%rax, 41096(%r14)
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	jmp	.L2785
	.p2align 4,,10
	.p2align 3
.L2788:
	movq	%r15, %rdx
	leaq	-80(%rbp), %rdi
	addq	$1, %r12
	call	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	movq	0(%r13), %rdx
	cmpl	%r12d, 11(%rdx)
	jg	.L2790
	jmp	.L2787
	.p2align 4,,10
	.p2align 3
.L2791:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L2806
.L2793:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r9, (%rsi)
	jmp	.L2792
	.p2align 4,,10
	.p2align 3
.L2805:
	leaq	-80(%rbp), %rdx
	call	*%rax
	jmp	.L2794
.L2806:
	movq	%r12, %rdi
	movq	%r9, -128(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-128(%rbp), %r9
	movq	%rax, %rsi
	jmp	.L2793
.L2804:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23006:
	.size	_ZN2v88internal5Debug12OnDebugBreakENS0_6HandleINS0_10FixedArrayEEE, .-_ZN2v88internal5Debug12OnDebugBreakENS0_6HandleINS0_10FixedArrayEEE
	.section	.text._ZN2v88internal5Debug16HandleDebugBreakENS0_15IgnoreBreakModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug16HandleDebugBreakENS0_15IgnoreBreakModeE
	.type	_ZN2v88internal5Debug16HandleDebugBreakENS0_15IgnoreBreakModeE, @function
_ZN2v88internal5Debug16HandleDebugBreakENS0_15IgnoreBreakModeE:
.LFB23029:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$1496, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8LiveEdit21InitializeThreadLocalEPNS0_5DebugE@PLT
	movq	136(%r12), %rbx
	movq	40936(%rbx), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L2807
	cmpb	$0, 12(%r12)
	jne	.L2807
	cmpb	$0, 8(%r12)
	je	.L2807
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	37528(%rbx), %rax
	jnb	.L2837
	.p2align 4,,10
	.p2align 3
.L2807:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2838
	addq	$1496, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2837:
	.cfi_restore_state
	movq	136(%r12), %rsi
	leaq	-1504(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2809
	movq	%r14, %rdi
	call	_ZN2v88internal23JavaScriptFrameIterator7AdvanceEv@PLT
	movq	-88(%rbp), %rdi
.L2809:
	movq	(%rdi), %rax
	call	*152(%rax)
	movq	%rax, %rsi
	testb	$1, %al
	jne	.L2839
.L2821:
	movq	24(%r12), %rbx
	testq	%rbx, %rbx
	je	.L2825
	.p2align 4,,10
	.p2align 3
.L2823:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal5Debug16ClearBreakPointsENS0_6HandleINS0_9DebugInfoEEE
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal5Debug16ApplyBreakPointsENS0_6HandleINS0_9DebugInfoEEE
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L2823
.L2825:
	movq	136(%r12), %r13
	movb	$-1, 76(%r12)
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	$0, 80(%r12)
	movb	$0, 88(%r12)
	movl	$-1, 92(%r12)
	movq	$-1, 96(%r12)
	movb	$0, 132(%r12)
	cmpl	$32, 41828(%r13)
	sete	9(%r12)
	movq	41088(%r13), %r15
	movq	41096(%r13), %rbx
	addl	$1, 41104(%r13)
	call	_ZN2v88internal10DebugScopeC1EPNS0_5DebugE
	movq	136(%r12), %rax
	movq	%r12, %rdi
	leaq	288(%rax), %rsi
	call	_ZN2v88internal5Debug12OnDebugBreakENS0_6HandleINS0_10FixedArrayEEE
	movq	%r14, %rdi
	call	_ZN2v88internal10DebugScopeD1Ev
	subl	$1, 41104(%r13)
	movq	%r15, 41088(%r13)
	cmpq	41096(%r13), %rbx
	je	.L2807
	movq	%rbx, 41096(%r13)
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	jmp	.L2807
	.p2align 4,,10
	.p2align 3
.L2839:
	movq	-1(%rax), %rax
	cmpw	$1105, 11(%rax)
	jne	.L2821
	movq	136(%r12), %r15
	addl	$1, 41104(%r15)
	movq	136(%r12), %rdx
	movq	41088(%r15), %rax
	movq	41096(%r15), %rbx
	movq	41112(%rdx), %rdi
	movq	%rax, -1512(%rbp)
	testq	%rdi, %rdi
	je	.L2811
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L2812:
	movq	136(%r12), %rdx
	movq	23(%rsi), %r9
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L2814
	movq	%r9, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L2815:
	movq	%r12, %rdi
	cmpl	$1, %r13d
	je	.L2840
	call	_ZN2v88internal5Debug29AllFramesOnStackAreBlackboxedEv
.L2818:
	testb	%al, %al
	jne	.L2819
	movq	-88(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal5Debug24IsMutedAtCurrentLocationEPNS0_15JavaScriptFrameE
	testb	%al, %al
	jne	.L2819
	movq	-1512(%rbp), %rax
	subl	$1, 41104(%r15)
	movq	%rax, 41088(%r15)
	cmpq	41096(%r15), %rbx
	je	.L2821
	movq	%rbx, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	jmp	.L2821
.L2819:
	movq	-1512(%rbp), %rax
	subl	$1, 41104(%r15)
	movq	%rax, 41088(%r15)
	cmpq	41096(%r15), %rbx
	je	.L2807
	movq	%rbx, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	jmp	.L2807
.L2814:
	movq	41088(%rdx), %rsi
	cmpq	41096(%rdx), %rsi
	je	.L2841
.L2816:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rdx)
	movq	%r9, (%rsi)
	jmp	.L2815
.L2811:
	movq	41088(%rdx), %rax
	cmpq	41096(%rdx), %rax
	je	.L2842
.L2813:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%rdx)
	movq	%rsi, (%rax)
	jmp	.L2812
.L2840:
	call	_ZN2v88internal5Debug12IsBlackboxedENS0_6HandleINS0_18SharedFunctionInfoEEE
	jmp	.L2818
.L2842:
	movq	%rdx, %rdi
	movq	%rsi, -1528(%rbp)
	movq	%rdx, -1520(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-1528(%rbp), %rsi
	movq	-1520(%rbp), %rdx
	jmp	.L2813
.L2841:
	movq	%rdx, %rdi
	movq	%r9, -1528(%rbp)
	movq	%rdx, -1520(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-1528(%rbp), %r9
	movq	-1520(%rbp), %rdx
	movq	%rax, %rsi
	jmp	.L2816
.L2838:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23029:
	.size	_ZN2v88internal5Debug16HandleDebugBreakENS0_15IgnoreBreakModeE, .-_ZN2v88internal5Debug16HandleDebugBreakENS0_15IgnoreBreakModeE
	.section	.text._ZN2v88internal5Debug5BreakEPNS0_15JavaScriptFrameENS0_6HandleINS0_10JSFunctionEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5Debug5BreakEPNS0_15JavaScriptFrameENS0_6HandleINS0_10JSFunctionEEE
	.type	_ZN2v88internal5Debug5BreakEPNS0_15JavaScriptFrameENS0_6HandleINS0_10JSFunctionEEE, @function
_ZN2v88internal5Debug5BreakEPNS0_15JavaScriptFrameENS0_6HandleINS0_10JSFunctionEEE:
.LFB22853:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$216, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8LiveEdit21InitializeThreadLocalEPNS0_5DebugE@PLT
	cmpb	$0, 12(%r14)
	je	.L2915
.L2843:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2916
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2915:
	.cfi_restore_state
	leaq	-128(%rbp), %r13
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal10DebugScopeC1EPNS0_5DebugE
	movzbl	12(%r14), %eax
	movq	136(%r14), %rdx
	movb	$1, 12(%r14)
	movb	%al, -225(%rbp)
	movq	(%rbx), %rax
	movq	23(%rax), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L2845
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L2846:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal5Debug15EnsureBreakInfoENS0_6HandleINS0_18SharedFunctionInfoEEE
	movl	%eax, %ebx
	testb	%al, %al
	je	.L2859
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal5Debug32PrepareFunctionForDebugExecutionENS0_6HandleINS0_18SharedFunctionInfoEEE
	movq	136(%r14), %rdx
	movq	(%r15), %rax
	movq	41112(%rdx), %rdi
	movq	31(%rax), %rsi
	testq	%rdi, %rdi
	je	.L2849
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L2850:
	leaq	-224(%rbp), %r9
	movq	%r15, %rsi
	movq	%r12, %rdx
	movq	%r9, %rdi
	movq	%r9, -240(%rbp)
	call	_ZN2v88internal13BreakLocation9FromFrameENS0_6HandleINS0_9DebugInfoEEEPNS0_15JavaScriptFrameE
	movq	%r15, %rsi
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	movq	-240(%rbp), %r9
	movq	%r9, %rdx
	call	_ZN2v88internal5Debug16CheckBreakPointsENS0_6HandleINS0_9DebugInfoEEEPNS0_13BreakLocationEPb
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L2917
	movq	24(%r14), %rbx
	testq	%rbx, %rbx
	je	.L2918
	.p2align 4,,10
	.p2align 3
.L2855:
	movq	(%rbx), %r12
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal5Debug16ClearBreakPointsENS0_6HandleINS0_9DebugInfoEEE
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal5Debug16ApplyBreakPointsENS0_6HandleINS0_9DebugInfoEEE
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L2855
	movq	136(%r14), %rax
	movb	$-1, 76(%r14)
	movq	$0, 80(%r14)
	movb	$0, 88(%r14)
	movl	$-1, 92(%r14)
	movq	$-1, 96(%r14)
	movb	$0, 132(%r14)
	cmpl	$32, 41828(%rax)
	sete	9(%r14)
	testq	%r15, %r15
	je	.L2857
.L2856:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal5Debug12OnDebugBreakENS0_6HandleINS0_10FixedArrayEEE
.L2859:
	movzbl	-225(%rbp), %eax
	movq	%r13, %rdi
	movb	%al, 12(%r14)
	call	_ZN2v88internal10DebugScopeD1Ev
	jmp	.L2843
	.p2align 4,,10
	.p2align 3
.L2845:
	movq	41088(%rdx), %r15
	cmpq	%r15, 41096(%rdx)
	je	.L2919
.L2847:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%r15)
	jmp	.L2846
	.p2align 4,,10
	.p2align 3
.L2849:
	movq	41088(%rdx), %r15
	cmpq	41096(%rdx), %r15
	je	.L2920
.L2851:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%r15)
	jmp	.L2850
	.p2align 4,,10
	.p2align 3
.L2917:
	cmpb	$0, 132(%r14)
	je	.L2921
	movq	24(%r14), %rbx
	testq	%rbx, %rbx
	jne	.L2855
	movq	136(%r14), %rax
	movb	$-1, 76(%r14)
	movq	$0, 80(%r14)
	movb	$0, 88(%r14)
	movl	$-1, 92(%r14)
	movq	$-1, 96(%r14)
	movb	$0, 132(%r14)
	cmpl	$32, 41828(%rax)
	sete	9(%r14)
	.p2align 4,,10
	.p2align 3
.L2857:
	leaq	288(%rax), %r15
	jmp	.L2856
	.p2align 4,,10
	.p2align 3
.L2921:
	cmpl	$6, -212(%rbp)
	je	.L2859
	movq	%r14, %rdi
	movzbl	76(%r14), %r15d
	call	_ZN2v88internal5Debug17CurrentFrameCountEv
	movzbl	88(%r14), %ecx
	movl	100(%r14), %edx
	movb	%cl, -240(%rbp)
	testb	%cl, %cl
	je	.L2860
	cmpl	%edx, %eax
	jg	.L2859
	movq	24(%r14), %rbx
	testq	%rbx, %rbx
	je	.L2861
	.p2align 4,,10
	.p2align 3
.L2862:
	movq	(%rbx), %r12
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal5Debug16ClearBreakPointsENS0_6HandleINS0_9DebugInfoEEE
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal5Debug16ApplyBreakPointsENS0_6HandleINS0_9DebugInfoEEE
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L2862
.L2861:
	movq	136(%r14), %rax
	movb	$-1, 76(%r14)
	movq	%r14, %rdi
	movq	$0, 80(%r14)
	movb	$0, 88(%r14)
	movl	$-1, 92(%r14)
	movq	$-1, 96(%r14)
	movb	$0, 132(%r14)
	cmpl	$32, 41828(%rax)
	sete	9(%r14)
	xorl	%esi, %esi
	call	_ZN2v88internal5Debug11PrepareStepENS0_10StepActionE
	jmp	.L2859
	.p2align 4,,10
	.p2align 3
.L2919:
	movq	%rdx, %rdi
	movq	%rsi, -248(%rbp)
	movq	%rdx, -240(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-248(%rbp), %rsi
	movq	-240(%rbp), %rdx
	movq	%rax, %r15
	jmp	.L2847
	.p2align 4,,10
	.p2align 3
.L2920:
	movq	%rdx, %rdi
	movq	%rsi, -248(%rbp)
	movq	%rdx, -240(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-248(%rbp), %rsi
	movq	-240(%rbp), %rdx
	movq	%rax, %r15
	jmp	.L2851
	.p2align 4,,10
	.p2align 3
.L2860:
	cmpb	$1, %r15b
	je	.L2863
	jg	.L2864
	cmpb	$-1, %r15b
	je	.L2859
	testb	%r15b, %r15b
	jne	.L2866
	cmpl	%edx, %eax
	jg	.L2859
	movq	24(%r14), %r12
	testq	%r12, %r12
	je	.L2882
	movb	%bl, -240(%rbp)
	.p2align 4,,10
	.p2align 3
.L2874:
	movq	(%r12), %rbx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal5Debug16ClearBreakPointsENS0_6HandleINS0_9DebugInfoEEE
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal5Debug16ApplyBreakPointsENS0_6HandleINS0_9DebugInfoEEE
	movq	8(%r12), %r12
	testq	%r12, %r12
	jne	.L2874
.L2873:
	movq	136(%r14), %rax
	movb	$-1, 76(%r14)
	movq	$0, 80(%r14)
	movb	$0, 88(%r14)
	movl	$-1, 92(%r14)
	movq	$-1, 96(%r14)
	movb	$0, 132(%r14)
	cmpl	$32, 41828(%rax)
	sete	9(%r14)
	cmpb	$0, -240(%rbp)
	je	.L2875
.L2881:
	leaq	288(%rax), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal5Debug12OnDebugBreakENS0_6HandleINS0_10FixedArrayEEE
	jmp	.L2859
.L2864:
	cmpb	$2, %r15b
	jne	.L2866
.L2867:
	cmpl	$5, -212(%rbp)
	je	.L2922
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	movl	%eax, -240(%rbp)
	movl	96(%r14), %ebx
	movq	%rdi, -248(%rbp)
	call	_ZN2v88internal12FrameSummary6GetTopEPKNS0_13StandardFrameE@PLT
	movl	-240(%rbp), %eax
	cmpl	$4, -212(%rbp)
	sete	%dl
	movq	-248(%rbp), %rdi
	cmpl	%ebx, %eax
	setne	%al
	orb	%al, %dl
	movb	%dl, -240(%rbp)
	je	.L2923
.L2872:
	call	_ZN2v88internal12FrameSummaryD1Ev@PLT
	movq	24(%r14), %r12
	testq	%r12, %r12
	jne	.L2874
	jmp	.L2873
	.p2align 4,,10
	.p2align 3
.L2918:
	movq	136(%r14), %rax
	movb	$-1, 76(%r14)
	movq	$0, 80(%r14)
	movb	$0, 88(%r14)
	movl	$-1, 92(%r14)
	movq	$-1, 96(%r14)
	movb	$0, 132(%r14)
	cmpl	$32, 41828(%rax)
	sete	9(%r14)
	jmp	.L2856
.L2866:
	movq	24(%r14), %r12
	testq	%r12, %r12
	jne	.L2874
	movq	136(%r14), %rax
	movb	$-1, 76(%r14)
	movq	$0, 80(%r14)
	movb	$0, 88(%r14)
	movl	$-1, 92(%r14)
	movq	$-1, 96(%r14)
	movb	$0, 132(%r14)
	cmpl	$32, 41828(%rax)
	sete	9(%r14)
.L2875:
	movsbl	%r15b, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal5Debug11PrepareStepENS0_10StepActionE
	jmp	.L2859
.L2863:
	cmpl	%edx, %eax
	jle	.L2867
	jmp	.L2859
.L2922:
	movl	-204(%rbp), %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal16InterpretedFrame23ReadInterpreterRegisterEi@PLT
	movq	24(%r14), %rbx
	movq	%rax, 112(%r14)
	testq	%rbx, %rbx
	je	.L2870
	.p2align 4,,10
	.p2align 3
.L2871:
	movq	(%rbx), %r12
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal5Debug16ClearBreakPointsENS0_6HandleINS0_9DebugInfoEEE
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal5Debug16ApplyBreakPointsENS0_6HandleINS0_9DebugInfoEEE
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L2871
.L2870:
	movq	136(%r14), %rax
	movb	$-1, 76(%r14)
	movq	$0, 80(%r14)
	movb	$0, 88(%r14)
	movl	$-1, 92(%r14)
	movq	$-1, 96(%r14)
	movb	$0, 132(%r14)
	cmpl	$32, 41828(%rax)
	sete	9(%r14)
	jmp	.L2859
.L2923:
	movl	92(%r14), %ebx
	call	_ZNK2v88internal12FrameSummary23SourceStatementPositionEv@PLT
	movq	-248(%rbp), %rdi
	cmpl	%eax, %ebx
	setne	-240(%rbp)
	jmp	.L2872
.L2916:
	call	__stack_chk_fail@PLT
.L2882:
	movq	136(%r14), %rax
	movb	$-1, 76(%r14)
	movq	$0, 80(%r14)
	movb	$0, 88(%r14)
	movl	$-1, 92(%r14)
	movq	$-1, 96(%r14)
	movb	$0, 132(%r14)
	cmpl	$32, 41828(%rax)
	sete	9(%r14)
	jmp	.L2881
	.cfi_endproc
.LFE22853:
	.size	_ZN2v88internal5Debug5BreakEPNS0_15JavaScriptFrameENS0_6HandleINS0_10JSFunctionEEE, .-_ZN2v88internal5Debug5BreakEPNS0_15JavaScriptFrameENS0_6HandleINS0_10JSFunctionEEE
	.section	.text._ZNSt10_HashtableImmSaImENSt8__detail9_IdentityESt8equal_toImESt4hashImENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS1_10_Hash_nodeImLb0EEEm,"axG",@progbits,_ZNSt10_HashtableImmSaImENSt8__detail9_IdentityESt8equal_toImESt4hashImENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS1_10_Hash_nodeImLb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableImmSaImENSt8__detail9_IdentityESt8equal_toImESt4hashImENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS1_10_Hash_nodeImLb0EEEm
	.type	_ZNSt10_HashtableImmSaImENSt8__detail9_IdentityESt8equal_toImESt4hashImENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS1_10_Hash_nodeImLb0EEEm, @function
_ZNSt10_HashtableImmSaImENSt8__detail9_IdentityESt8equal_toImESt4hashImENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS1_10_Hash_nodeImLb0EEEm:
.LFB29169:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L2925
	movq	(%rbx), %r8
.L2926:
	movq	(%r8,%r15,8), %rax
	leaq	0(,%r15,8), %rcx
	testq	%rax, %rax
	je	.L2935
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r13, (%rax)
.L2936:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2925:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L2949
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L2950
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%rbx), %r10
	movq	%rax, %r8
.L2928:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L2930
	xorl	%edi, %edi
	leaq	16(%rbx), %r9
	jmp	.L2931
	.p2align 4,,10
	.p2align 3
.L2932:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L2933:
	testq	%rsi, %rsi
	je	.L2930
.L2931:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	8(%rcx), %rax
	divq	%r12
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L2932
	movq	16(%rbx), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L2938
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L2931
	.p2align 4,,10
	.p2align 3
.L2930:
	movq	(%rbx), %rdi
	cmpq	%rdi, %r10
	je	.L2934
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L2934:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	divq	%r12
	movq	%r8, (%rbx)
	movq	%rdx, %r15
	jmp	.L2926
	.p2align 4,,10
	.p2align 3
.L2935:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L2937
	movq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r13, (%rax,%rdx,8)
.L2937:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L2936
	.p2align 4,,10
	.p2align 3
.L2938:
	movq	%rdx, %rdi
	jmp	.L2933
	.p2align 4,,10
	.p2align 3
.L2949:
	leaq	48(%rbx), %r8
	movq	$0, 48(%rbx)
	movq	%r8, %r10
	jmp	.L2928
.L2950:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE29169:
	.size	_ZNSt10_HashtableImmSaImENSt8__detail9_IdentityESt8equal_toImESt4hashImENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS1_10_Hash_nodeImLb0EEEm, .-_ZNSt10_HashtableImmSaImENSt8__detail9_IdentityESt8equal_toImESt4hashImENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS1_10_Hash_nodeImLb0EEEm
	.section	.text._ZNSt10_HashtableImmSaImENSt8__detail9_IdentityESt8equal_toImESt4hashImENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE9_M_insertIRKmNS1_10_AllocNodeISaINS1_10_Hash_nodeImLb0EEEEEEEESt4pairINS1_14_Node_iteratorImLb1ELb0EEEbEOT_RKT0_St17integral_constantIbLb1EEm,"axG",@progbits,_ZNSt10_HashtableImmSaImENSt8__detail9_IdentityESt8equal_toImESt4hashImENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE9_M_insertIRKmNS1_10_AllocNodeISaINS1_10_Hash_nodeImLb0EEEEEEEESt4pairINS1_14_Node_iteratorImLb1ELb0EEEbEOT_RKT0_St17integral_constantIbLb1EEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableImmSaImENSt8__detail9_IdentityESt8equal_toImESt4hashImENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE9_M_insertIRKmNS1_10_AllocNodeISaINS1_10_Hash_nodeImLb0EEEEEEEESt4pairINS1_14_Node_iteratorImLb1ELb0EEEbEOT_RKT0_St17integral_constantIbLb1EEm
	.type	_ZNSt10_HashtableImmSaImENSt8__detail9_IdentityESt8equal_toImESt4hashImENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE9_M_insertIRKmNS1_10_AllocNodeISaINS1_10_Hash_nodeImLb0EEEEEEEESt4pairINS1_14_Node_iteratorImLb1ELb0EEEbEOT_RKT0_St17integral_constantIbLb1EEm, @function
_ZNSt10_HashtableImmSaImENSt8__detail9_IdentityESt8equal_toImESt4hashImENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE9_M_insertIRKmNS1_10_AllocNodeISaINS1_10_Hash_nodeImLb0EEEEEEEESt4pairINS1_14_Node_iteratorImLb1ELb0EEEbEOT_RKT0_St17integral_constantIbLb1EEm:
.LFB28566:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	(%rsi), %r13
	movq	8(%rdi), %r9
	movq	%r13, %rax
	divq	%r9
	movq	(%rdi), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r14
	testq	%rax, %rax
	je	.L2952
	movq	(%rax), %rdi
	movq	8(%rdi), %r10
	jmp	.L2954
	.p2align 4,,10
	.p2align 3
.L2965:
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2952
	movq	8(%rdi), %r10
	xorl	%edx, %edx
	movq	%r10, %rax
	divq	%r9
	cmpq	%rdx, %r14
	jne	.L2952
.L2954:
	cmpq	%r10, %r13
	jne	.L2965
	addq	$8, %rsp
	movq	%rdi, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2952:
	.cfi_restore_state
	movl	$16, %edi
	call	_Znwm@PLT
	movq	%r13, %rdx
	movq	%r15, %r8
	movq	%r14, %rsi
	movq	%rax, %r9
	movq	$0, (%rax)
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movq	%r9, %rcx
	movq	%rax, 8(%r9)
	call	_ZNSt10_HashtableImmSaImENSt8__detail9_IdentityESt8equal_toImESt4hashImENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS1_10_Hash_nodeImLb0EEEm
	addq	$8, %rsp
	movl	$1, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE28566:
	.size	_ZNSt10_HashtableImmSaImENSt8__detail9_IdentityESt8equal_toImESt4hashImENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE9_M_insertIRKmNS1_10_AllocNodeISaINS1_10_Hash_nodeImLb0EEEEEEEESt4pairINS1_14_Node_iteratorImLb1ELb0EEEbEOT_RKT0_St17integral_constantIbLb1EEm, .-_ZNSt10_HashtableImmSaImENSt8__detail9_IdentityESt8equal_toImESt4hashImENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE9_M_insertIRKmNS1_10_AllocNodeISaINS1_10_Hash_nodeImLb0EEEEEEEESt4pairINS1_14_Node_iteratorImLb1ELb0EEEbEOT_RKT0_St17integral_constantIbLb1EEm
	.section	.text._ZN2v88internal5Debug23TemporaryObjectsTracker15AllocationEventEmi,"axG",@progbits,_ZN2v88internal5Debug23TemporaryObjectsTracker15AllocationEventEmi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal5Debug23TemporaryObjectsTracker15AllocationEventEmi
	.type	_ZN2v88internal5Debug23TemporaryObjectsTracker15AllocationEventEmi, @function
_ZN2v88internal5Debug23TemporaryObjectsTracker15AllocationEventEmi:
.LFB22808:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$8, %rdi
	movl	$1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rsi, -24(%rbp)
	leaq	-16(%rbp), %rdx
	leaq	-24(%rbp), %rsi
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -16(%rbp)
	call	_ZNSt10_HashtableImmSaImENSt8__detail9_IdentityESt8equal_toImESt4hashImENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE9_M_insertIRKmNS1_10_AllocNodeISaINS1_10_Hash_nodeImLb0EEEEEEEESt4pairINS1_14_Node_iteratorImLb1ELb0EEEbEOT_RKT0_St17integral_constantIbLb1EEm
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2969
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2969:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22808:
	.size	_ZN2v88internal5Debug23TemporaryObjectsTracker15AllocationEventEmi, .-_ZN2v88internal5Debug23TemporaryObjectsTracker15AllocationEventEmi
	.section	.text._ZNSt10_HashtableImmSaImENSt8__detail9_IdentityESt8equal_toImESt4hashImENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseEmPNS1_15_Hash_node_baseEPNS1_10_Hash_nodeImLb0EEE,"axG",@progbits,_ZNSt10_HashtableImmSaImENSt8__detail9_IdentityESt8equal_toImESt4hashImENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseEmPNS1_15_Hash_node_baseEPNS1_10_Hash_nodeImLb0EEE,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableImmSaImENSt8__detail9_IdentityESt8equal_toImESt4hashImENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseEmPNS1_15_Hash_node_baseEPNS1_10_Hash_nodeImLb0EEE
	.type	_ZNSt10_HashtableImmSaImENSt8__detail9_IdentityESt8equal_toImESt4hashImENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseEmPNS1_15_Hash_node_baseEPNS1_10_Hash_nodeImLb0EEE, @function
_ZNSt10_HashtableImmSaImENSt8__detail9_IdentityESt8equal_toImESt4hashImENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseEmPNS1_15_Hash_node_baseEPNS1_10_Hash_nodeImLb0EEE:
.LFB29179:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	0(,%rsi,8), %r9
	movq	%rdx, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rcx, %rdi
	movq	(%rbx), %rcx
	movq	(%rdi), %r12
	leaq	(%rcx,%r9), %rax
	cmpq	%rdx, (%rax)
	je	.L2980
	testq	%r12, %r12
	je	.L2973
	movq	8(%r12), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	cmpq	%rdx, %rsi
	je	.L2973
	movq	%r8, (%rcx,%rdx,8)
	movq	(%rdi), %r12
.L2973:
	movq	%r12, (%r8)
	call	_ZdlPv@PLT
	movq	%r12, %rax
	subq	$1, 24(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2980:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L2972
	movq	8(%r12), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	cmpq	%rdx, %rsi
	je	.L2973
	movq	%r8, (%rcx,%rdx,8)
	movq	(%rbx), %rax
	addq	%r9, %rax
	movq	(%rax), %rdx
.L2972:
	leaq	16(%rbx), %rcx
	cmpq	%rcx, %rdx
	je	.L2981
.L2974:
	movq	$0, (%rax)
	movq	(%rdi), %r12
	jmp	.L2973
	.p2align 4,,10
	.p2align 3
.L2981:
	movq	%r12, 16(%rbx)
	jmp	.L2974
	.cfi_endproc
.LFE29179:
	.size	_ZNSt10_HashtableImmSaImENSt8__detail9_IdentityESt8equal_toImESt4hashImENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseEmPNS1_15_Hash_node_baseEPNS1_10_Hash_nodeImLb0EEE, .-_ZNSt10_HashtableImmSaImENSt8__detail9_IdentityESt8equal_toImESt4hashImENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseEmPNS1_15_Hash_node_baseEPNS1_10_Hash_nodeImLb0EEE
	.section	.text._ZN2v88internal5Debug23TemporaryObjectsTracker9MoveEventEmmi,"axG",@progbits,_ZN2v88internal5Debug23TemporaryObjectsTracker9MoveEventEmmi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal5Debug23TemporaryObjectsTracker9MoveEventEmmi
	.type	_ZN2v88internal5Debug23TemporaryObjectsTracker9MoveEventEmmi, @function
_ZN2v88internal5Debug23TemporaryObjectsTracker9MoveEventEmmi:
.LFB22809:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdx, -56(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -40(%rbp)
	xorl	%ecx, %ecx
	cmpq	%rsi, %rdx
	je	.L2982
	leaq	64(%rdi), %r13
	movq	%rdi, %r12
	movq	%rsi, %rbx
	movq	%r13, %rdi
	leaq	8(%r12), %r14
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	16(%r12), %rcx
	movq	%rbx, %rax
	xorl	%edx, %edx
	movq	8(%r12), %r9
	divq	%rcx
	movq	(%r9,%rdx,8), %rax
	movq	%rdx, %rdi
	testq	%rax, %rax
	je	.L2984
	movq	(%rax), %r8
	movq	8(%r8), %rsi
	jmp	.L2986
	.p2align 4,,10
	.p2align 3
.L3009:
	movq	(%r8), %r8
	testq	%r8, %r8
	je	.L2984
	movq	8(%r8), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rcx
	cmpq	%rdx, %rdi
	jne	.L2984
.L2986:
	cmpq	%rbx, %rsi
	jne	.L3009
	movq	%rsi, %rax
	xorl	%edx, %edx
	divq	%rcx
	movq	(%r9,%rdx,8), %rax
	movq	%rdx, %rsi
	.p2align 4,,10
	.p2align 3
.L2990:
	movq	%rax, %rdx
	movq	(%rax), %rax
	cmpq	%rax, %r8
	jne	.L2990
	movq	%r8, %rcx
	movq	%r14, %rdi
	call	_ZNSt10_HashtableImmSaImENSt8__detail9_IdentityESt8equal_toImESt4hashImENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseEmPNS1_15_Hash_node_baseEPNS1_10_Hash_nodeImLb0EEE
	leaq	-48(%rbp), %rdx
	leaq	-56(%rbp), %rsi
	movq	%r14, %rdi
	movl	$1, %ecx
	movq	%r14, -48(%rbp)
	call	_ZNSt10_HashtableImmSaImENSt8__detail9_IdentityESt8equal_toImESt4hashImENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE9_M_insertIRKmNS1_10_AllocNodeISaINS1_10_Hash_nodeImLb0EEEEEEEESt4pairINS1_14_Node_iteratorImLb1ELb0EEEbEOT_RKT0_St17integral_constantIbLb1EEm
.L3008:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
.L2982:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3010
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2984:
	.cfi_restore_state
	movq	-56(%rbp), %r10
	xorl	%edx, %edx
	movq	%r10, %rax
	divq	%rcx
	movq	(%r9,%rdx,8), %r9
	movq	%rdx, %r11
	testq	%r9, %r9
	je	.L3008
	movq	(%r9), %r8
	movq	8(%r8), %rsi
	jmp	.L2989
	.p2align 4,,10
	.p2align 3
.L3011:
	movq	(%r8), %rdi
	testq	%rdi, %rdi
	je	.L3008
	movq	8(%rdi), %rsi
	xorl	%edx, %edx
	movq	%r8, %r9
	movq	%rsi, %rax
	divq	%rcx
	cmpq	%rdx, %r11
	jne	.L3008
	movq	%rdi, %r8
.L2989:
	cmpq	%r10, %rsi
	jne	.L3011
	movq	%r8, %rcx
	movq	%r9, %rdx
	movq	%r11, %rsi
	movq	%r14, %rdi
	call	_ZNSt10_HashtableImmSaImENSt8__detail9_IdentityESt8equal_toImESt4hashImENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseEmPNS1_15_Hash_node_baseEPNS1_10_Hash_nodeImLb0EEE
	jmp	.L3008
.L3010:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22809:
	.size	_ZN2v88internal5Debug23TemporaryObjectsTracker9MoveEventEmmi, .-_ZN2v88internal5Debug23TemporaryObjectsTracker9MoveEventEmmi
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal5DebugC2EPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal5DebugC2EPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal5DebugC2EPNS0_7IsolateE:
.LFB30237:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE30237:
	.size	_GLOBAL__sub_I__ZN2v88internal5DebugC2EPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal5DebugC2EPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal5DebugC2EPNS0_7IsolateE
	.section	.rodata.CSWTCH.1024,"a"
	.align 16
	.type	CSWTCH.1024, @object
	.size	CSWTCH.1024, 16
CSWTCH.1024:
	.long	2
	.long	3
	.long	0
	.long	1
	.weak	_ZTVN2v88internal15InterruptsScopeE
	.section	.data.rel.ro.local._ZTVN2v88internal15InterruptsScopeE,"awG",@progbits,_ZTVN2v88internal15InterruptsScopeE,comdat
	.align 8
	.type	_ZTVN2v88internal15InterruptsScopeE, @object
	.size	_ZTVN2v88internal15InterruptsScopeE, 32
_ZTVN2v88internal15InterruptsScopeE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal15InterruptsScopeD1Ev
	.quad	_ZN2v88internal15InterruptsScopeD0Ev
	.weak	_ZTVN2v88internal23PostponeInterruptsScopeE
	.section	.data.rel.ro.local._ZTVN2v88internal23PostponeInterruptsScopeE,"awG",@progbits,_ZTVN2v88internal23PostponeInterruptsScopeE,comdat
	.align 8
	.type	_ZTVN2v88internal23PostponeInterruptsScopeE, @object
	.size	_ZTVN2v88internal23PostponeInterruptsScopeE, 32
_ZTVN2v88internal23PostponeInterruptsScopeE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23PostponeInterruptsScopeD1Ev
	.quad	_ZN2v88internal23PostponeInterruptsScopeD0Ev
	.weak	_ZTVN2v88internal5Debug23TemporaryObjectsTrackerE
	.section	.data.rel.ro.local._ZTVN2v88internal5Debug23TemporaryObjectsTrackerE,"awG",@progbits,_ZTVN2v88internal5Debug23TemporaryObjectsTrackerE,comdat
	.align 8
	.type	_ZTVN2v88internal5Debug23TemporaryObjectsTrackerE, @object
	.size	_ZTVN2v88internal5Debug23TemporaryObjectsTrackerE, 56
_ZTVN2v88internal5Debug23TemporaryObjectsTrackerE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal5Debug23TemporaryObjectsTracker15AllocationEventEmi
	.quad	_ZN2v88internal5Debug23TemporaryObjectsTracker9MoveEventEmmi
	.quad	_ZN2v88internal27HeapObjectAllocationTracker21UpdateObjectSizeEventEmi
	.quad	_ZN2v88internal5Debug23TemporaryObjectsTrackerD1Ev
	.quad	_ZN2v88internal5Debug23TemporaryObjectsTrackerD0Ev
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
