	.file	"interrupts-scope.cc"
	.text
	.section	.text._ZN2v88internal15InterruptsScopeD2Ev,"axG",@progbits,_ZN2v88internal15InterruptsScopeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15InterruptsScopeD2Ev
	.type	_ZN2v88internal15InterruptsScopeD2Ev, @function
_ZN2v88internal15InterruptsScopeD2Ev:
.LFB3183:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal15InterruptsScopeE(%rip), %rax
	cmpl	$2, 32(%rdi)
	movq	%rax, (%rdi)
	jne	.L4
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movq	8(%rdi), %rdi
	jmp	_ZN2v88internal10StackGuard18PopInterruptsScopeEv@PLT
	.cfi_endproc
.LFE3183:
	.size	_ZN2v88internal15InterruptsScopeD2Ev, .-_ZN2v88internal15InterruptsScopeD2Ev
	.weak	_ZN2v88internal15InterruptsScopeD1Ev
	.set	_ZN2v88internal15InterruptsScopeD1Ev,_ZN2v88internal15InterruptsScopeD2Ev
	.section	.text._ZN2v88internal15InterruptsScopeD0Ev,"axG",@progbits,_ZN2v88internal15InterruptsScopeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15InterruptsScopeD0Ev
	.type	_ZN2v88internal15InterruptsScopeD0Ev, @function
_ZN2v88internal15InterruptsScopeD0Ev:
.LFB3185:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal15InterruptsScopeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	cmpl	$2, 32(%rdi)
	movq	%rax, (%rdi)
	je	.L6
	movq	8(%rdi), %rdi
	call	_ZN2v88internal10StackGuard18PopInterruptsScopeEv@PLT
.L6:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$48, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE3185:
	.size	_ZN2v88internal15InterruptsScopeD0Ev, .-_ZN2v88internal15InterruptsScopeD0Ev
	.section	.text._ZN2v88internal15InterruptsScopeC2EPNS0_7IsolateElNS1_4ModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15InterruptsScopeC2EPNS0_7IsolateElNS1_4ModeE
	.type	_ZN2v88internal15InterruptsScopeC2EPNS0_7IsolateElNS1_4ModeE, @function
_ZN2v88internal15InterruptsScopeC2EPNS0_7IsolateElNS1_4ModeE:
.LFB8617:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal15InterruptsScopeE(%rip), %rax
	movq	%rdi, %r8
	movq	%rax, (%rdi)
	leaq	37512(%rsi), %rdi
	movq	%rdi, 8(%r8)
	movq	%rdx, 16(%r8)
	movq	$0, 24(%r8)
	movl	%ecx, 32(%r8)
	cmpl	$2, %ecx
	jne	.L10
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movq	%r8, %rsi
	jmp	_ZN2v88internal10StackGuard19PushInterruptsScopeEPNS0_15InterruptsScopeE@PLT
	.cfi_endproc
.LFE8617:
	.size	_ZN2v88internal15InterruptsScopeC2EPNS0_7IsolateElNS1_4ModeE, .-_ZN2v88internal15InterruptsScopeC2EPNS0_7IsolateElNS1_4ModeE
	.globl	_ZN2v88internal15InterruptsScopeC1EPNS0_7IsolateElNS1_4ModeE
	.set	_ZN2v88internal15InterruptsScopeC1EPNS0_7IsolateElNS1_4ModeE,_ZN2v88internal15InterruptsScopeC2EPNS0_7IsolateElNS1_4ModeE
	.section	.text._ZN2v88internal15InterruptsScope9InterceptENS0_10StackGuard13InterruptFlagE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15InterruptsScope9InterceptENS0_10StackGuard13InterruptFlagE
	.type	_ZN2v88internal15InterruptsScope9InterceptENS0_10StackGuard13InterruptFlagE, @function
_ZN2v88internal15InterruptsScope9InterceptENS0_10StackGuard13InterruptFlagE:
.LFB8619:
	.cfi_startproc
	endbr64
	movslq	%esi, %rsi
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L14:
	testq	%rsi, 16(%rdi)
	je	.L12
	cmpl	$1, 32(%rdi)
	je	.L13
	movq	%rdi, %rax
.L12:
	movq	40(%rdi), %rdi
	testq	%rdi, %rdi
	jne	.L14
.L13:
	testq	%rax, %rax
	je	.L16
	orq	%rsi, 24(%rax)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE8619:
	.size	_ZN2v88internal15InterruptsScope9InterceptENS0_10StackGuard13InterruptFlagE, .-_ZN2v88internal15InterruptsScope9InterceptENS0_10StackGuard13InterruptFlagE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal15InterruptsScopeC2EPNS0_7IsolateElNS1_4ModeE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal15InterruptsScopeC2EPNS0_7IsolateElNS1_4ModeE, @function
_GLOBAL__sub_I__ZN2v88internal15InterruptsScopeC2EPNS0_7IsolateElNS1_4ModeE:
.LFB9880:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE9880:
	.size	_GLOBAL__sub_I__ZN2v88internal15InterruptsScopeC2EPNS0_7IsolateElNS1_4ModeE, .-_GLOBAL__sub_I__ZN2v88internal15InterruptsScopeC2EPNS0_7IsolateElNS1_4ModeE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal15InterruptsScopeC2EPNS0_7IsolateElNS1_4ModeE
	.weak	_ZTVN2v88internal15InterruptsScopeE
	.section	.data.rel.ro.local._ZTVN2v88internal15InterruptsScopeE,"awG",@progbits,_ZTVN2v88internal15InterruptsScopeE,comdat
	.align 8
	.type	_ZTVN2v88internal15InterruptsScopeE, @object
	.size	_ZTVN2v88internal15InterruptsScopeE, 32
_ZTVN2v88internal15InterruptsScopeE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal15InterruptsScopeD1Ev
	.quad	_ZN2v88internal15InterruptsScopeD0Ev
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
