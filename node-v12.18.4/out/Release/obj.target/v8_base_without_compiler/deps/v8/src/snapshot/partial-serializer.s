	.file	"partial-serializer.cc"
	.text
	.section	.text._ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE,"axG",@progbits,_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE
	.type	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE, @function
_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE:
.LFB7689:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	leaq	8(%rcx), %r8
	movq	16(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE7689:
	.size	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE, .-_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE
	.section	.text._ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE,"axG",@progbits,_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE
	.type	_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE, @function
_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE:
.LFB7690:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7690:
	.size	_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE, .-_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE
	.section	.rodata._ZN2v88internal17PartialSerializerD2Ev.str1.1,"aMS",@progbits,1
.LC0:
	.string	"PartialSerializer"
	.section	.text._ZN2v88internal17PartialSerializerD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17PartialSerializerD2Ev
	.type	_ZN2v88internal17PartialSerializerD2Ev, @function
_ZN2v88internal17PartialSerializerD2Ev:
.LFB18538:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal17PartialSerializerE(%rip), %rax
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN2v88internal10Serializer16OutputStatisticsEPKc@PLT
	movq	400(%r12), %rdi
	testq	%rdi, %rdi
	je	.L5
	call	_ZdlPv@PLT
.L5:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal10SerializerD2Ev@PLT
	.cfi_endproc
.LFE18538:
	.size	_ZN2v88internal17PartialSerializerD2Ev, .-_ZN2v88internal17PartialSerializerD2Ev
	.globl	_ZN2v88internal17PartialSerializerD1Ev
	.set	_ZN2v88internal17PartialSerializerD1Ev,_ZN2v88internal17PartialSerializerD2Ev
	.section	.text._ZN2v88internal17PartialSerializerD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17PartialSerializerD0Ev
	.type	_ZN2v88internal17PartialSerializerD0Ev, @function
_ZN2v88internal17PartialSerializerD0Ev:
.LFB18540:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal17PartialSerializerE(%rip), %rax
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN2v88internal10Serializer16OutputStatisticsEPKc@PLT
	movq	400(%r12), %rdi
	testq	%rdi, %rdi
	je	.L11
	call	_ZdlPv@PLT
.L11:
	movq	%r12, %rdi
	call	_ZN2v88internal10SerializerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$424, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE18540:
	.size	_ZN2v88internal17PartialSerializerD0Ev, .-_ZN2v88internal17PartialSerializerD0Ev
	.section	.text._ZN2v88internal17PartialSerializerC2EPNS0_7IsolateEPNS0_17StartupSerializerENS_31SerializeInternalFieldsCallbackE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17PartialSerializerC2EPNS0_7IsolateEPNS0_17StartupSerializerENS_31SerializeInternalFieldsCallbackE
	.type	_ZN2v88internal17PartialSerializerC2EPNS0_7IsolateEPNS0_17StartupSerializerENS_31SerializeInternalFieldsCallbackE, @function
_ZN2v88internal17PartialSerializerC2EPNS0_7IsolateEPNS0_17StartupSerializerENS_31SerializeInternalFieldsCallbackE:
.LFB18535:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	call	_ZN2v88internal10SerializerC2EPNS0_7IsolateE@PLT
	leaq	16+_ZTVN2v88internal17PartialSerializerE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%r14, 360(%rbx)
	movq	%r13, 368(%rbx)
	movq	%rbx, %rdi
	movq	%r12, 376(%rbx)
	movq	%rax, (%rbx)
	movb	$1, 384(%rbx)
	movq	$0, 392(%rbx)
	movq	$0, 416(%rbx)
	movups	%xmm0, 400(%rbx)
	call	_ZN2v88internal10Serializer24InitializeCodeAddressMapEv@PLT
	movl	_ZN2v88internal29FLAG_serialization_chunk_sizeE(%rip), %esi
	leaq	216(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal19SerializerAllocator18UseCustomChunkSizeEj@PLT
	.cfi_endproc
.LFE18535:
	.size	_ZN2v88internal17PartialSerializerC2EPNS0_7IsolateEPNS0_17StartupSerializerENS_31SerializeInternalFieldsCallbackE, .-_ZN2v88internal17PartialSerializerC2EPNS0_7IsolateEPNS0_17StartupSerializerENS_31SerializeInternalFieldsCallbackE
	.globl	_ZN2v88internal17PartialSerializerC1EPNS0_7IsolateEPNS0_17StartupSerializerENS_31SerializeInternalFieldsCallbackE
	.set	_ZN2v88internal17PartialSerializerC1EPNS0_7IsolateEPNS0_17StartupSerializerENS_31SerializeInternalFieldsCallbackE,_ZN2v88internal17PartialSerializerC2EPNS0_7IsolateEPNS0_17StartupSerializerENS_31SerializeInternalFieldsCallbackE
	.section	.text._ZN2v88internal17PartialSerializer33ShouldBeInThePartialSnapshotCacheENS0_10HeapObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17PartialSerializer33ShouldBeInThePartialSnapshotCacheENS0_10HeapObjectE
	.type	_ZN2v88internal17PartialSerializer33ShouldBeInThePartialSnapshotCacheENS0_10HeapObjectE, @function
_ZN2v88internal17PartialSerializer33ShouldBeInThePartialSnapshotCacheENS0_10HeapObjectE:
.LFB18543:
	.cfi_startproc
	endbr64
	movq	-1(%rsi), %rax
	cmpw	$64, 11(%rax)
	ja	.L19
.L21:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	movq	-1(%rsi), %rax
	cmpw	$160, 11(%rax)
	je	.L21
	movq	-1(%rsi), %rax
	cmpw	$65, 11(%rax)
	je	.L21
	movq	-1(%rsi), %rax
	cmpw	$69, 11(%rax)
	je	.L21
	movq	-1(%rsi), %rax
	cmpw	$136, 11(%rax)
	je	.L21
	movq	-1(%rsi), %rax
	cmpw	$78, 11(%rax)
	je	.L21
	movq	-1(%rsi), %rax
	cmpw	$92, 11(%rax)
	je	.L21
	movq	-1(%rsi), %rax
	cmpw	$88, 11(%rax)
	je	.L21
	movq	-1(%rsi), %rax
	cmpw	$85, 11(%rax)
	je	.L21
	movq	360(%rdi), %rax
	movq	104(%rax), %rdx
	movq	-1(%rsi), %rax
	cmpq	%rax, 160(%rdx)
	sete	%al
	ret
	.cfi_endproc
.LFE18543:
	.size	_ZN2v88internal17PartialSerializer33ShouldBeInThePartialSnapshotCacheENS0_10HeapObjectE, .-_ZN2v88internal17PartialSerializer33ShouldBeInThePartialSnapshotCacheENS0_10HeapObjectE
	.section	.text._ZN2v88internal17PartialSerializer18CheckRehashabilityENS0_10HeapObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17PartialSerializer18CheckRehashabilityENS0_10HeapObjectE
	.type	_ZN2v88internal17PartialSerializer18CheckRehashabilityENS0_10HeapObjectE, @function
_ZN2v88internal17PartialSerializer18CheckRehashabilityENS0_10HeapObjectE:
.LFB18564:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	cmpb	$0, 384(%rdi)
	movq	%rsi, -24(%rbp)
	jne	.L30
.L22:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	leaq	-24(%rbp), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZNK2v88internal10HeapObject14NeedsRehashingEv@PLT
	testb	%al, %al
	je	.L22
	movq	%r12, %rdi
	call	_ZNK2v88internal10HeapObject13CanBeRehashedEv@PLT
	testb	%al, %al
	jne	.L22
	movb	$0, 384(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18564:
	.size	_ZN2v88internal17PartialSerializer18CheckRehashabilityENS0_10HeapObjectE, .-_ZN2v88internal17PartialSerializer18CheckRehashabilityENS0_10HeapObjectE
	.section	.rodata._ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_.str1.1,"aMS",@progbits,1
.LC1:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_,"axG",@progbits,_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	.type	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_, @function
_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_:
.LFB21005:
	.cfi_startproc
	endbr64
	movabsq	$9223372036854775807, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rsi
	movq	(%rdi), %r15
	movq	%rsi, %rax
	subq	%r15, %rax
	cmpq	%rcx, %rax
	je	.L45
	movq	%rdx, %r13
	movq	%r8, %rdx
	movq	%rdi, %r12
	subq	%r15, %rdx
	testq	%rax, %rax
	je	.L40
	leaq	(%rax,%rax), %r14
	cmpq	%r14, %rax
	jbe	.L46
.L42:
	movq	%rcx, %r14
.L33:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L39:
	movzbl	0(%r13), %eax
	subq	%r8, %rsi
	leaq	1(%rbx,%rdx), %r10
	movq	%rsi, %r13
	movb	%al, (%rbx,%rdx)
	leaq	(%r10,%rsi), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L47
	testq	%rsi, %rsi
	jg	.L35
	testq	%r15, %r15
	jne	.L38
.L36:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r10, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r10
	movq	-72(%rbp), %r8
	jg	.L35
.L38:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L46:
	testq	%r14, %r14
	js	.L42
	jne	.L33
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L35:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r10, %rdi
	call	memcpy@PLT
	testq	%r15, %r15
	je	.L36
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L40:
	movl	$1, %r14d
	jmp	.L33
.L45:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE21005:
	.size	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_, .-_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	.section	.text._ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_,"axG",@progbits,_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	.type	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_, @function
_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_:
.LFB21950:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L62
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L58
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L63
.L50:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L57:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L64
	testq	%r13, %r13
	jg	.L53
	testq	%r9, %r9
	jne	.L56
.L54:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L53
.L56:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L63:
	testq	%rsi, %rsi
	jne	.L51
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L53:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L54
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L58:
	movl	$8, %r14d
	jmp	.L50
.L62:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L51:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L50
	.cfi_endproc
.LFE21950:
	.size	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_, .-_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	.section	.text._ZNSt6vectorIN2v811StartupDataESaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v811StartupDataESaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v811StartupDataESaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	.type	_ZNSt6vectorIN2v811StartupDataESaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_, @function
_ZNSt6vectorIN2v811StartupDataESaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_:
.LFB21956:
	.cfi_startproc
	endbr64
	movabsq	$576460752303423487, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r9
	movq	(%rdi), %r15
	movq	%r9, %rax
	subq	%r15, %rax
	sarq	$4, %rax
	cmpq	%rcx, %rax
	je	.L79
	movq	%rdx, %r13
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r15, %rdx
	testq	%rax, %rax
	je	.L75
	movabsq	$9223372036854775792, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L80
.L67:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L74:
	movl	8(%r13), %ecx
	movq	0(%r13), %rsi
	leaq	(%rbx,%rdx), %rax
	subq	%r8, %r9
	leaq	16(%rbx,%rdx), %r10
	movq	%r9, %r13
	movq	%rsi, (%rax)
	movl	%ecx, 8(%rax)
	leaq	(%r10,%r9), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L81
	testq	%r9, %r9
	jg	.L70
	testq	%r15, %r15
	jne	.L73
.L71:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r10, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r10
	movq	-72(%rbp), %r8
	jg	.L70
.L73:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L80:
	testq	%rsi, %rsi
	jne	.L68
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L70:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r10, %rdi
	call	memcpy@PLT
	testq	%r15, %r15
	je	.L71
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L75:
	movl	$16, %r14d
	jmp	.L67
.L79:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L68:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$4, %r14
	jmp	.L67
	.cfi_endproc
.LFE21956:
	.size	_ZNSt6vectorIN2v811StartupDataESaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_, .-_ZNSt6vectorIN2v811StartupDataESaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	.section	.text._ZNSt6vectorIN2v811StartupDataESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v811StartupDataESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v811StartupDataESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	.type	_ZNSt6vectorIN2v811StartupDataESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_, @function
_ZNSt6vectorIN2v811StartupDataESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_:
.LFB22574:
	.cfi_startproc
	endbr64
	movabsq	$576460752303423487, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r9
	movq	(%rdi), %r15
	movq	%r9, %rax
	subq	%r15, %rax
	sarq	$4, %rax
	cmpq	%rcx, %rax
	je	.L96
	movq	%rdx, %r13
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r15, %rdx
	testq	%rax, %rax
	je	.L92
	movabsq	$9223372036854775792, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L97
.L84:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L91:
	movl	8(%r13), %ecx
	movq	0(%r13), %rsi
	leaq	(%rbx,%rdx), %rax
	subq	%r8, %r9
	leaq	16(%rbx,%rdx), %r10
	movq	%r9, %r13
	movq	%rsi, (%rax)
	movl	%ecx, 8(%rax)
	leaq	(%r10,%r9), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L98
	testq	%r9, %r9
	jg	.L87
	testq	%r15, %r15
	jne	.L90
.L88:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r10, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r10
	movq	-72(%rbp), %r8
	jg	.L87
.L90:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L97:
	testq	%rsi, %rsi
	jne	.L85
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L87:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r10, %rdi
	call	memcpy@PLT
	testq	%r15, %r15
	je	.L88
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L92:
	movl	$16, %r14d
	jmp	.L84
.L96:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L85:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$4, %r14
	jmp	.L84
	.cfi_endproc
.LFE22574:
	.size	_ZNSt6vectorIN2v811StartupDataESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_, .-_ZNSt6vectorIN2v811StartupDataESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	.section	.rodata._ZN2v88internal17PartialSerializer35SerializeJSObjectWithEmbedderFieldsENS0_6ObjectE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"embedder_fields_count > 0"
.LC3:
	.string	"Check failed: %s."
.LC4:
	.string	"BackRefChunkIndex"
.LC5:
	.string	"BackRefChunkOffset"
.LC6:
	.string	"embedder field index"
.LC7:
	.string	"embedder fields data size"
.LC8:
	.string	"embedder fields data"
	.section	.text._ZN2v88internal17PartialSerializer35SerializeJSObjectWithEmbedderFieldsENS0_6ObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17PartialSerializer35SerializeJSObjectWithEmbedderFieldsENS0_6ObjectE
	.type	_ZN2v88internal17PartialSerializer35SerializeJSObjectWithEmbedderFieldsENS0_6ObjectE, @function
_ZN2v88internal17PartialSerializer35SerializeJSObjectWithEmbedderFieldsENS0_6ObjectE:
.LFB18545:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rsi, %rax
	notq	%rax
	andl	$1, %eax
	je	.L100
.L102:
	xorl	%eax, %eax
.L99:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L168
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	.cfi_restore_state
	movq	-1(%rsi), %rdx
	movq	%rsi, %r14
	leaq	-1(%rsi), %r12
	cmpw	$1024, 11(%rdx)
	jbe	.L102
	movq	-1(%rsi), %r15
	movzbl	7(%r15), %r13d
	sall	$3, %r13d
	je	.L99
	movq	%rdi, %rbx
	movzwl	11(%r15), %edi
	movl	$24, %esi
	cmpw	$1057, %di
	je	.L104
	movsbl	13(%r15), %esi
	shrl	$31, %esi
	call	_ZN2v88internal8JSObject13GetHeaderSizeENS0_12InstanceTypeEb@PLT
	movl	%eax, %esi
.L104:
	movzbl	7(%r15), %eax
	movzbl	8(%r15), %edx
	subl	%esi, %r13d
	sarl	$3, %r13d
	subl	%edx, %eax
	subl	%eax, %r13d
	movl	%r13d, -184(%rbp)
	testl	%r13d, %r13d
	je	.L102
	jle	.L169
	leaq	-176(%rbp), %rax
	movq	104(%rbx), %rsi
	movq	%rax, %rdi
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb0EEC1EPNS0_7IsolateE@PLT
	movq	104(%rbx), %rax
	addl	$1, 41104(%rax)
	movq	41088(%rax), %rcx
	movq	104(%rbx), %r13
	movq	%rax, -224(%rbp)
	movq	%rcx, -248(%rbp)
	movq	41096(%rax), %rcx
	movq	41112(%r13), %rdi
	movq	%rcx, -232(%rbp)
	testq	%rdi, %rdi
	je	.L106
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L107:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	leaq	-96(%rbp), %rax
	movq	%r14, -192(%rbp)
	movq	$0, -144(%rbp)
	movl	%r13d, %r14d
	movq	%r15, %r13
	movl	-184(%rbp), %r15d
	movq	$0, -112(%rbp)
	movq	%rax, -200(%rbp)
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm0, -128(%rbp)
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L112:
	movq	368(%rbx), %r8
	orq	%r8, %rax
	je	.L116
	movl	%r14d, %esi
	movq	376(%rbx), %rdx
	movq	%r13, %rdi
	call	*%r8
	movq	-120(%rbp), %rsi
	movq	%rax, -96(%rbp)
	movq	%rdx, -88(%rbp)
	cmpq	-112(%rbp), %rsi
	je	.L117
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %rdx
	movl	%eax, 8(%rsi)
	movq	%rdx, (%rsi)
	addq	$16, -120(%rbp)
.L115:
	addl	$1, %r14d
	cmpl	%r15d, %r14d
	je	.L170
.L121:
	movq	(%r12), %rdx
	movl	$24, %eax
	movzwl	11(%rdx), %edi
	cmpw	$1057, %di
	je	.L109
	movsbl	13(%rdx), %esi
	shrl	$31, %esi
	call	_ZN2v88internal8JSObject13GetHeaderSizeENS0_12InstanceTypeEb@PLT
.L109:
	leal	(%rax,%r14,8), %eax
	movq	-152(%rbp), %rsi
	cltq
	addq	%r12, %rax
	movq	(%rax), %rdx
	movq	%rdx, -96(%rbp)
	cmpq	-144(%rbp), %rsi
	je	.L110
	movq	%rdx, (%rsi)
	addq	$8, -152(%rbp)
.L111:
	movq	(%rax), %rax
	testb	$1, %al
	je	.L112
.L116:
	movq	$0, -96(%rbp)
	movq	-120(%rbp), %rsi
	movl	$0, -88(%rbp)
	cmpq	-112(%rbp), %rsi
	je	.L119
	movq	$0, (%rsi)
	movl	$0, 8(%rsi)
	addq	$16, -120(%rbp)
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L110:
	movq	-200(%rbp), %rdx
	leaq	-160(%rbp), %rdi
	movq	%rax, -184(%rbp)
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	movq	-184(%rbp), %rax
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L170:
	movl	%r14d, %r13d
	xorl	%r15d, %r15d
	movq	-192(%rbp), %r14
	subl	$1, %r13d
	movq	%r13, -192(%rbp)
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L122:
	leaq	1(%r15), %rax
	cmpq	%r15, %r13
	je	.L171
	movq	%rax, %r15
.L124:
	movq	%r15, %rax
	salq	$4, %rax
	addq	-128(%rbp), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	je	.L122
	movq	(%r12), %rdx
	movl	$24, %eax
	movzwl	11(%rdx), %edi
	cmpw	$1057, %di
	je	.L123
	movsbl	13(%rdx), %esi
	shrl	$31, %esi
	call	_ZN2v88internal8JSObject13GetHeaderSizeENS0_12InstanceTypeEb@PLT
.L123:
	leal	(%rax,%r15,8), %eax
	cltq
	movq	$0, (%rax,%r12)
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L171:
	movq	-200(%rbp), %rdi
	leaq	16+_ZTVN2v88internal10Serializer16ObjectSerializerE(%rip), %rax
	movq	%rbx, -88(%rbp)
	movq	%rax, -96(%rbp)
	leaq	80(%rbx), %rax
	movq	%rax, -72(%rbp)
	movq	%r14, -80(%rbp)
	movl	$0, -64(%rbp)
	call	_ZN2v88internal10Serializer16ObjectSerializer9SerializeEv@PLT
	movl	120(%rbx), %eax
	movq	112(%rbx), %rdi
	leal	-1(%rax), %esi
	movl	%esi, %eax
	andl	%r14d, %eax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rdi,%rdx,8), %rdx
	cmpb	$0, 20(%rdx)
	jne	.L127
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L172:
	addq	$1, %rax
	andq	%rsi, %rax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rdi,%rdx,8), %rdx
	cmpb	$0, 20(%rdx)
	je	.L143
.L127:
	cmpq	(%rdx), %r14
	jne	.L172
	movl	8(%rdx), %eax
	movl	12(%rdx), %ecx
	movl	%eax, -212(%rbp)
	shrl	$4, %eax
	movl	%ecx, -216(%rbp)
	movq	%rax, -208(%rbp)
.L125:
	movq	%r12, -184(%rbp)
	movq	-128(%rbp), %rdi
	xorl	%r14d, %r14d
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L145:
	movq	%rax, %r14
.L133:
	movq	%r14, %rax
	salq	$4, %rax
	addq	%rdi, %rax
	movl	8(%rax), %r12d
	testl	%r12d, %r12d
	je	.L128
	movq	(%rax), %r13
	movq	-184(%rbp), %rax
	movq	(%rax), %rdx
	movl	$24, %eax
	movzwl	11(%rdx), %edi
	cmpw	$1057, %di
	je	.L129
	movsbl	13(%rdx), %esi
	shrl	$31, %esi
	call	_ZN2v88internal8JSObject13GetHeaderSizeENS0_12InstanceTypeEb@PLT
.L129:
	movq	-160(%rbp), %rdx
	movq	-184(%rbp), %rcx
	leal	(%rax,%r14,8), %eax
	leaq	400(%rbx), %r15
	cltq
	movq	(%rdx,%r14,8), %rdx
	movq	%rdx, (%rax,%rcx)
	movl	-212(%rbp), %eax
	movq	408(%rbx), %rsi
	andl	$15, %eax
	movb	%al, -96(%rbp)
	cmpq	416(%rbx), %rsi
	je	.L130
	movb	%al, (%rsi)
	addq	$1, 408(%rbx)
.L131:
	movq	-208(%rbp), %rsi
	leaq	.LC4(%rip), %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal16SnapshotByteSink6PutIntEmPKc@PLT
	movl	-216(%rbp), %esi
	leaq	.LC5(%rip), %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal16SnapshotByteSink6PutIntEmPKc@PLT
	leaq	.LC6(%rip), %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal16SnapshotByteSink6PutIntEmPKc@PLT
	movslq	%r12d, %rsi
	leaq	.LC7(%rip), %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal16SnapshotByteSink6PutIntEmPKc@PLT
	movl	%r12d, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	leaq	.LC8(%rip), %rcx
	call	_ZN2v88internal16SnapshotByteSink6PutRawEPKhiPKc@PLT
	testq	%r13, %r13
	je	.L166
	movq	%r13, %rdi
	call	_ZdaPv@PLT
.L166:
	movq	-128(%rbp), %rdi
.L128:
	leaq	1(%r14), %rax
	cmpq	-192(%rbp), %r14
	jne	.L145
	testq	%rdi, %rdi
	je	.L134
	call	_ZdlPv@PLT
.L134:
	movq	-160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L135
	call	_ZdlPv@PLT
.L135:
	movq	-224(%rbp), %rax
	movq	-248(%rbp), %rcx
	subl	$1, 41104(%rax)
	movq	%rcx, 41088(%rax)
	movq	-232(%rbp), %rcx
	cmpq	41096(%rax), %rcx
	je	.L137
	movq	%rcx, 41096(%rax)
	movq	%rax, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L137:
	movq	-240(%rbp), %rdi
	call	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb0EED1Ev@PLT
	movl	$1, %eax
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L117:
	movq	-200(%rbp), %rdx
	leaq	-128(%rbp), %rdi
	call	_ZNSt6vectorIN2v811StartupDataESaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L130:
	movq	-200(%rbp), %rdx
	movq	%r15, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L119:
	movq	-200(%rbp), %rdx
	leaq	-128(%rbp), %rdi
	call	_ZNSt6vectorIN2v811StartupDataESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L143:
	movq	$0, -208(%rbp)
	movl	$0, -216(%rbp)
	movl	$6, -212(%rbp)
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L106:
	movq	41088(%r13), %r15
	cmpq	%r15, 41096(%r13)
	je	.L173
.L108:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r13)
	movq	%r14, (%r15)
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L169:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L173:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r15
	jmp	.L108
.L168:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18545:
	.size	_ZN2v88internal17PartialSerializer35SerializeJSObjectWithEmbedderFieldsENS0_6ObjectE, .-_ZN2v88internal17PartialSerializer35SerializeJSObjectWithEmbedderFieldsENS0_6ObjectE
	.section	.text._ZN2v88internal17PartialSerializer15SerializeObjectENS0_10HeapObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17PartialSerializer15SerializeObjectENS0_10HeapObjectE
	.type	_ZN2v88internal17PartialSerializer15SerializeObjectENS0_10HeapObjectE, @function
_ZN2v88internal17PartialSerializer15SerializeObjectENS0_10HeapObjectE:
.LFB18542:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal10Serializer18SerializeHotObjectENS0_10HeapObjectE@PLT
	testb	%al, %al
	je	.L229
.L174:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L230
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L229:
	.cfi_restore_state
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal10Serializer13SerializeRootENS0_10HeapObjectE@PLT
	testb	%al, %al
	jne	.L174
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal10Serializer22SerializeBackReferenceENS0_10HeapObjectE@PLT
	testb	%al, %al
	jne	.L174
	leaq	80(%rbx), %r13
	movq	360(%rbx), %rdi
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	_ZN2v88internal17StartupSerializer33SerializeUsingReadOnlyObjectCacheEPNS0_16SnapshotByteSinkENS0_10HeapObjectE@PLT
	testb	%al, %al
	jne	.L174
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17PartialSerializer33ShouldBeInThePartialSnapshotCacheENS0_10HeapObjectE
	testb	%al, %al
	jne	.L231
	movq	-1(%r12), %rax
	cmpw	$155, 11(%rax)
	je	.L232
.L178:
	movq	-1(%r12), %rax
	cmpw	$154, 11(%rax)
	je	.L233
.L179:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17PartialSerializer35SerializeJSObjectWithEmbedderFieldsENS0_6ObjectE
	testb	%al, %al
	jne	.L174
	movq	-1(%r12), %rax
	leaq	-96(%rbp), %r14
	cmpw	$1105, 11(%rax)
	je	.L234
.L182:
	cmpb	$0, 384(%rbx)
	movq	%r12, -96(%rbp)
	je	.L204
	movq	%r14, %rdi
	call	_ZNK2v88internal10HeapObject14NeedsRehashingEv@PLT
	testb	%al, %al
	je	.L204
	movq	%r14, %rdi
	call	_ZNK2v88internal10HeapObject13CanBeRehashedEv@PLT
	testb	%al, %al
	jne	.L204
	movb	$0, 384(%rbx)
	.p2align 4,,10
	.p2align 3
.L204:
	leaq	16+_ZTVN2v88internal10Serializer16ObjectSerializerE(%rip), %rax
	movq	%r14, %rdi
	movq	%rbx, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	%r12, -80(%rbp)
	movq	%r13, -72(%rbp)
	movl	$0, -64(%rbp)
	call	_ZN2v88internal10Serializer16ObjectSerializer9SerializeEv@PLT
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L231:
	movq	360(%rbx), %rdi
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	_ZN2v88internal17StartupSerializer34SerializeUsingPartialSnapshotCacheEPNS0_16SnapshotByteSinkENS0_10HeapObjectE@PLT
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L233:
	cmpb	$0, _ZN2v88internal29FLAG_lazy_feedback_allocationE(%rip)
	movl	_ZN2v88internal21FLAG_interrupt_budgetE(%rip), %eax
	je	.L181
	movl	_ZN2v88internal42FLAG_budget_for_feedback_vector_allocationE(%rip), %eax
.L181:
	movl	%eax, 15(%r12)
	jmp	.L179
.L232:
	leaq	-96(%rbp), %r14
	movq	104(%rbx), %rsi
	movq	%r12, -96(%rbp)
	movq	%r14, %rdi
	call	_ZN2v88internal14FeedbackVector10ClearSlotsEPNS0_7IsolateE@PLT
	jmp	.L178
.L234:
	cmpb	$0, _ZN2v88internal19FLAG_flush_bytecodeE(%rip)
	leaq	47(%r12), %r15
	jne	.L235
.L183:
	movq	47(%r12), %rax
	leaq	-96(%rbp), %r14
	cmpl	$67, 59(%rax)
	je	.L182
	movabsq	$287762808832, %rdx
	movq	23(%r12), %rax
	movq	7(%rax), %rax
	cmpq	%rdx, %rax
	je	.L182
	testb	$1, %al
	jne	.L236
.L199:
	movq	23(%r12), %rax
	leaq	-96(%rbp), %r14
	movq	%r14, %rdi
	movq	%rax, -96(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo7GetCodeEv@PLT
	movq	%rax, 47(%r12)
	movq	%rax, %rdx
	testb	$1, %al
	je	.L182
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L182
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L182
.L235:
	movq	23(%r12), %rax
	movq	47(%r12), %rdx
	testb	$1, %al
	je	.L183
	movq	-1(%rax), %rcx
	cmpw	$160, 11(%rcx)
	jne	.L183
	testb	$1, %dl
	je	.L183
	movq	-1(%rdx), %rcx
	cmpw	$69, 11(%rcx)
	jne	.L183
	movq	7(%rax), %rax
	movl	$67, %ecx
	salq	$32, %rcx
	cmpq	%rcx, %rax
	je	.L189
	testb	$1, %al
	je	.L183
	movq	-1(%rax), %rcx
	cmpw	$165, 11(%rcx)
	je	.L189
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L183
.L189:
	cmpl	$67, 59(%rdx)
	je	.L183
	movq	%r12, %rax
	movl	$67, %esi
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	addq	$3592, %rdi
	call	_ZN2v88internal8Builtins7builtinEi@PLT
	movq	%rax, 47(%r12)
	movq	%rax, %rdx
	testb	$1, %al
	je	.L190
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L190
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
.L190:
	movq	39(%r12), %r14
	movq	%r14, %rcx
	leaq	7(%r14), %rsi
	andq	$-262144, %rcx
	movq	24(%rcx), %rax
	movq	-37504(%rax), %rdx
	movq	%rdx, 7(%r14)
	testb	$1, %dl
	je	.L207
	movq	%rdx, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rax
	movq	%r8, -128(%rbp)
	testl	$262144, %eax
	je	.L194
	movq	%r14, %rdi
	movq	%rcx, -120(%rbp)
	movq	%rdx, -112(%rbp)
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-128(%rbp), %r8
	movq	-120(%rbp), %rcx
	movq	-112(%rbp), %rdx
	movq	-104(%rbp), %rsi
	movq	8(%r8), %rax
.L194:
	testb	$24, %al
	je	.L207
	testb	$24, 8(%rcx)
	jne	.L207
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
.L207:
	cmpb	$0, _ZN2v88internal29FLAG_lazy_feedback_allocationE(%rip)
	movl	_ZN2v88internal21FLAG_interrupt_budgetE(%rip), %eax
	je	.L197
	movl	_ZN2v88internal42FLAG_budget_for_feedback_vector_allocationE(%rip), %eax
.L197:
	movl	%eax, 15(%r14)
	jmp	.L183
.L230:
	call	__stack_chk_fail@PLT
.L236:
	movq	-1(%rax), %rdx
	cmpw	$165, 11(%rdx)
	je	.L182
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L199
	jmp	.L182
	.cfi_endproc
.LFE18542:
	.size	_ZN2v88internal17PartialSerializer15SerializeObjectENS0_10HeapObjectE, .-_ZN2v88internal17PartialSerializer15SerializeObjectENS0_10HeapObjectE
	.section	.rodata._ZN2v84base19TemplateHashMapImplImNS_8internal19SerializerReferenceENS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE6ResizeES6_.str1.8,"aMS",@progbits,1
	.align 8
.LC9:
	.string	"Out of memory: HashMap::Initialize"
	.section	.text._ZN2v84base19TemplateHashMapImplImNS_8internal19SerializerReferenceENS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE6ResizeES6_,"axG",@progbits,_ZN2v84base19TemplateHashMapImplImNS_8internal19SerializerReferenceENS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE6ResizeES6_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base19TemplateHashMapImplImNS_8internal19SerializerReferenceENS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE6ResizeES6_
	.type	_ZN2v84base19TemplateHashMapImplImNS_8internal19SerializerReferenceENS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE6ResizeES6_, @function
_ZN2v84base19TemplateHashMapImplImNS_8internal19SerializerReferenceENS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE6ResizeES6_:
.LFB22676:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	8(%rdi), %eax
	movq	(%rdi), %r13
	movl	12(%rdi), %r15d
	addl	%eax, %eax
	leaq	(%rax,%rax,2), %rdi
	movq	%rax, %rbx
	salq	$3, %rdi
	call	malloc@PLT
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L262
	movl	%ebx, 8(%r12)
	testl	%ebx, %ebx
	je	.L239
	movb	$0, 20(%rax)
	movl	$24, %edx
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L240:
	movq	(%r12), %rcx
	addq	$1, %rax
	movb	$0, 20(%rcx,%rdx)
	movl	8(%r12), %ecx
	addq	$24, %rdx
	cmpq	%rax, %rcx
	ja	.L240
.L239:
	movl	$0, 12(%r12)
	movq	%r13, %r14
	testl	%r15d, %r15d
	je	.L246
.L241:
	cmpb	$0, 20(%r14)
	jne	.L263
.L242:
	addq	$24, %r14
	cmpb	$0, 20(%r14)
	je	.L242
.L263:
	movl	8(%r12), %eax
	movl	16(%r14), %ebx
	movq	(%r12), %rdi
	movq	(%r14), %rsi
	leal	-1(%rax), %ecx
	movl	%ebx, %eax
	andl	%ecx, %eax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rdi,%rdx,8), %rdx
	cmpb	$0, 20(%rdx)
	jne	.L244
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L264:
	addq	$1, %rax
	andq	%rcx, %rax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rdi,%rdx,8), %rdx
	cmpb	$0, 20(%rdx)
	je	.L243
.L244:
	cmpq	%rsi, (%rdx)
	jne	.L264
.L243:
	movl	12(%r14), %eax
	movl	8(%r14), %ecx
	movq	%rsi, (%rdx)
	movl	%ebx, 16(%rdx)
	movl	%ecx, 8(%rdx)
	movl	%eax, 12(%rdx)
	movb	$1, 20(%rdx)
	movl	12(%r12), %eax
	addl	$1, %eax
	movl	%eax, %edx
	movl	%eax, 12(%r12)
	shrl	$2, %edx
	addl	%edx, %eax
	cmpl	8(%r12), %eax
	jnb	.L245
.L248:
	addq	$24, %r14
	subl	$1, %r15d
	jne	.L241
.L246:
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	free@PLT
	.p2align 4,,10
	.p2align 3
.L245:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v84base19TemplateHashMapImplImNS_8internal19SerializerReferenceENS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE6ResizeES6_
	movl	8(%r12), %eax
	movq	(%r12), %rsi
	leal	-1(%rax), %ecx
	movl	%ebx, %eax
	andl	%ecx, %eax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rsi,%rdx,8), %rdx
	cmpb	$0, 20(%rdx)
	je	.L248
	movq	(%r14), %rdi
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L265:
	addq	$1, %rax
	andq	%rcx, %rax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rsi,%rdx,8), %rdx
	cmpb	$0, 20(%rdx)
	je	.L248
.L249:
	cmpq	%rdi, (%rdx)
	jne	.L265
	jmp	.L248
.L262:
	leaq	.LC9(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE22676:
	.size	_ZN2v84base19TemplateHashMapImplImNS_8internal19SerializerReferenceENS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE6ResizeES6_, .-_ZN2v84base19TemplateHashMapImplImNS_8internal19SerializerReferenceENS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE6ResizeES6_
	.section	.text._ZN2v88internal17PartialSerializer9SerializeEPNS0_7ContextEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17PartialSerializer9SerializeEPNS0_7ContextEb
	.type	_ZN2v88internal17PartialSerializer9SerializeEPNS0_7ContextEb, @function
_ZN2v88internal17PartialSerializer9SerializeEPNS0_7ContextEb:
.LFB18541:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$392, %rdi
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	call	_ZN2v88internal7Context12global_proxyEv@PLT
	movl	132(%r12), %r14d
	movq	112(%r12), %rcx
	movq	%rax, %rbx
	leal	1(%r14), %eax
	movl	%eax, 132(%r12)
	movl	120(%r12), %eax
	subl	$1, %eax
	movl	%eax, %esi
	andl	%ebx, %esi
	leaq	(%rsi,%rsi,2), %rdx
	leaq	(%rcx,%rdx,8), %rdx
	cmpb	$0, 20(%rdx)
	jne	.L269
	jmp	.L267
	.p2align 4,,10
	.p2align 3
.L295:
	addq	$1, %rsi
	andq	%rax, %rsi
	leaq	(%rsi,%rsi,2), %rdx
	leaq	(%rcx,%rdx,8), %rdx
	cmpb	$0, 20(%rdx)
	je	.L267
.L269:
	cmpq	(%rdx), %rbx
	jne	.L295
.L268:
	movl	$22, 8(%rdx)
	movl	%r14d, 12(%rdx)
	movq	104(%r12), %rax
	movq	88(%rax), %r14
	movq	392(%r12), %rax
	movq	%r14, 1959(%rax)
	testb	$1, %r14b
	je	.L282
	movq	%r14, %rbx
	movq	392(%r12), %rdi
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	leaq	1959(%rdi), %rsi
	testl	$262144, %eax
	jne	.L296
	testb	$24, %al
	je	.L282
.L299:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L297
	.p2align 4,,10
	.p2align 3
.L282:
	movq	392(%r12), %rdi
	call	_ZN2v88internal10MathRandom12ResetContextENS0_7ContextE@PLT
	movq	392(%r12), %rax
	leaq	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE(%rip), %rdx
	movq	39(%rax), %rax
	movq	$0, 1967(%rax)
	movq	(%r12), %rax
	movq	24(%rax), %r8
	cmpq	%rdx, %r8
	jne	.L275
	leaq	8(%r13), %r8
	movq	%r13, %rcx
	xorl	%edx, %edx
	movl	$19, %esi
	movq	%r12, %rdi
	call	*16(%rax)
.L276:
	movq	%r12, %rdi
	call	_ZN2v88internal10Serializer24SerializeDeferredObjectsEv@PLT
	movq	400(%r12), %rax
	cmpq	%rax, 408(%r12)
	je	.L277
	movb	$29, -41(%rbp)
	movq	88(%r12), %rsi
	leaq	80(%r12), %r13
	cmpq	96(%r12), %rsi
	je	.L278
	movb	$29, (%rsi)
	addq	$1, 88(%r12)
.L279:
	leaq	400(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal16SnapshotByteSink6AppendERKS1_@PLT
	movb	$26, -41(%rbp)
	movq	88(%r12), %rsi
	cmpq	96(%r12), %rsi
	je	.L280
	movb	$26, (%rsi)
	addq	$1, 88(%r12)
.L277:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal10Serializer3PadEi@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L298
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L296:
	.cfi_restore_state
	movq	%r14, %rdx
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	392(%r12), %rdi
	movq	8(%rbx), %rax
	leaq	1959(%rdi), %rsi
	testb	$24, %al
	jne	.L299
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L267:
	movq	%rbx, (%rdx)
	movq	$6, 8(%rdx)
	movl	%ebx, 16(%rdx)
	movb	$1, 20(%rdx)
	movl	124(%r12), %eax
	addl	$1, %eax
	movl	%eax, %ecx
	movl	%eax, 124(%r12)
	shrl	$2, %ecx
	addl	%ecx, %eax
	cmpl	120(%r12), %eax
	jb	.L268
	leaq	112(%r12), %rdi
	call	_ZN2v84base19TemplateHashMapImplImNS_8internal19SerializerReferenceENS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE6ResizeES6_
	movl	120(%r12), %eax
	movq	112(%r12), %rsi
	leal	-1(%rax), %ecx
	movl	%ecx, %eax
	andl	%ebx, %eax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rsi,%rdx,8), %rdx
	cmpb	$0, 20(%rdx)
	jne	.L271
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L300:
	addq	$1, %rax
	andq	%rcx, %rax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rsi,%rdx,8), %rdx
	cmpb	$0, 20(%rdx)
	je	.L268
.L271:
	cmpq	(%rdx), %rbx
	jne	.L300
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L297:
	movq	%r14, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L278:
	leaq	-41(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	jmp	.L279
	.p2align 4,,10
	.p2align 3
.L275:
	movq	%r13, %rcx
	xorl	%edx, %edx
	movl	$19, %esi
	movq	%r12, %rdi
	call	*%r8
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L280:
	leaq	-41(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	jmp	.L277
.L298:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18541:
	.size	_ZN2v88internal17PartialSerializer9SerializeEPNS0_7ContextEb, .-_ZN2v88internal17PartialSerializer9SerializeEPNS0_7ContextEb
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal17PartialSerializerC2EPNS0_7IsolateEPNS0_17StartupSerializerENS_31SerializeInternalFieldsCallbackE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal17PartialSerializerC2EPNS0_7IsolateEPNS0_17StartupSerializerENS_31SerializeInternalFieldsCallbackE, @function
_GLOBAL__sub_I__ZN2v88internal17PartialSerializerC2EPNS0_7IsolateEPNS0_17StartupSerializerENS_31SerializeInternalFieldsCallbackE:
.LFB23113:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE23113:
	.size	_GLOBAL__sub_I__ZN2v88internal17PartialSerializerC2EPNS0_7IsolateEPNS0_17StartupSerializerENS_31SerializeInternalFieldsCallbackE, .-_GLOBAL__sub_I__ZN2v88internal17PartialSerializerC2EPNS0_7IsolateEPNS0_17StartupSerializerENS_31SerializeInternalFieldsCallbackE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal17PartialSerializerC2EPNS0_7IsolateEPNS0_17StartupSerializerENS_31SerializeInternalFieldsCallbackE
	.weak	_ZTVN2v88internal17PartialSerializerE
	.section	.data.rel.ro._ZTVN2v88internal17PartialSerializerE,"awG",@progbits,_ZTVN2v88internal17PartialSerializerE,comdat
	.align 8
	.type	_ZTVN2v88internal17PartialSerializerE, @object
	.size	_ZTVN2v88internal17PartialSerializerE, 72
_ZTVN2v88internal17PartialSerializerE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal17PartialSerializerD1Ev
	.quad	_ZN2v88internal17PartialSerializerD0Ev
	.quad	_ZN2v88internal10Serializer17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_
	.quad	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE
	.quad	_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE
	.quad	_ZN2v88internal17PartialSerializer15SerializeObjectENS0_10HeapObjectE
	.quad	_ZN2v88internal10Serializer14MustBeDeferredENS0_10HeapObjectE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
