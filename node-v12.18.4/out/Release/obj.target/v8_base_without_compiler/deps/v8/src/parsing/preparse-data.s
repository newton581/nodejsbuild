	.file	"preparse-data.cc"
	.text
	.section	.text._ZN2v88internal26OnHeapProducedPreparseData9SerializeEPNS0_7IsolateE,"axG",@progbits,_ZN2v88internal26OnHeapProducedPreparseData9SerializeEPNS0_7IsolateE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal26OnHeapProducedPreparseData9SerializeEPNS0_7IsolateE
	.type	_ZN2v88internal26OnHeapProducedPreparseData9SerializeEPNS0_7IsolateE, @function
_ZN2v88internal26OnHeapProducedPreparseData9SerializeEPNS0_7IsolateE:
.LFB20985:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE20985:
	.size	_ZN2v88internal26OnHeapProducedPreparseData9SerializeEPNS0_7IsolateE, .-_ZN2v88internal26OnHeapProducedPreparseData9SerializeEPNS0_7IsolateE
	.section	.text._ZN2v88internal24ZoneProducedPreparseData9SerializeEPNS0_4ZoneE,"axG",@progbits,_ZN2v88internal24ZoneProducedPreparseData9SerializeEPNS0_4ZoneE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal24ZoneProducedPreparseData9SerializeEPNS0_4ZoneE
	.type	_ZN2v88internal24ZoneProducedPreparseData9SerializeEPNS0_4ZoneE, @function
_ZN2v88internal24ZoneProducedPreparseData9SerializeEPNS0_4ZoneE:
.LFB20991:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE20991:
	.size	_ZN2v88internal24ZoneProducedPreparseData9SerializeEPNS0_4ZoneE, .-_ZN2v88internal24ZoneProducedPreparseData9SerializeEPNS0_4ZoneE
	.section	.text._ZN2v88internal26OnHeapConsumedPreparseData12GetScopeDataEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26OnHeapConsumedPreparseData12GetScopeDataEv
	.type	_ZN2v88internal26OnHeapConsumedPreparseData12GetScopeDataEv, @function
_ZN2v88internal26OnHeapConsumedPreparseData12GetScopeDataEv:
.LFB21000:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	movq	(%rax), %rax
	ret
	.cfi_endproc
.LFE21000:
	.size	_ZN2v88internal26OnHeapConsumedPreparseData12GetScopeDataEv, .-_ZN2v88internal26OnHeapConsumedPreparseData12GetScopeDataEv
	.section	.text._ZN2v88internal24ZoneConsumedPreparseData12GetScopeDataEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24ZoneConsumedPreparseData12GetScopeDataEv
	.type	_ZN2v88internal24ZoneConsumedPreparseData12GetScopeDataEv, @function
_ZN2v88internal24ZoneConsumedPreparseData12GetScopeDataEv:
.LFB21029:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	ret
	.cfi_endproc
.LFE21029:
	.size	_ZN2v88internal24ZoneConsumedPreparseData12GetScopeDataEv, .-_ZN2v88internal24ZoneConsumedPreparseData12GetScopeDataEv
	.section	.rodata._ZN2v88internal26OnHeapProducedPreparseData9SerializeEPNS0_4ZoneE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal26OnHeapProducedPreparseData9SerializeEPNS0_4ZoneE,"axG",@progbits,_ZN2v88internal26OnHeapProducedPreparseData9SerializeEPNS0_4ZoneE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal26OnHeapProducedPreparseData9SerializeEPNS0_4ZoneE
	.type	_ZN2v88internal26OnHeapProducedPreparseData9SerializeEPNS0_4ZoneE, @function
_ZN2v88internal26OnHeapProducedPreparseData9SerializeEPNS0_4ZoneE:
.LFB20986:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20986:
	.size	_ZN2v88internal26OnHeapProducedPreparseData9SerializeEPNS0_4ZoneE, .-_ZN2v88internal26OnHeapProducedPreparseData9SerializeEPNS0_4ZoneE
	.section	.text._ZN2v88internal26OnHeapConsumedPreparseDataD2Ev,"axG",@progbits,_ZN2v88internal26OnHeapConsumedPreparseDataD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal26OnHeapConsumedPreparseDataD2Ev
	.type	_ZN2v88internal26OnHeapConsumedPreparseDataD2Ev, @function
_ZN2v88internal26OnHeapConsumedPreparseDataD2Ev:
.LFB24873:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal24BaseConsumedPreparseDataINS0_12PreparseDataEEE(%rip), %rax
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L8
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L8:
	ret
	.cfi_endproc
.LFE24873:
	.size	_ZN2v88internal26OnHeapConsumedPreparseDataD2Ev, .-_ZN2v88internal26OnHeapConsumedPreparseDataD2Ev
	.weak	_ZN2v88internal26OnHeapConsumedPreparseDataD1Ev
	.set	_ZN2v88internal26OnHeapConsumedPreparseDataD1Ev,_ZN2v88internal26OnHeapConsumedPreparseDataD2Ev
	.section	.text._ZN2v88internal26OnHeapConsumedPreparseDataD0Ev,"axG",@progbits,_ZN2v88internal26OnHeapConsumedPreparseDataD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal26OnHeapConsumedPreparseDataD0Ev
	.type	_ZN2v88internal26OnHeapConsumedPreparseDataD0Ev, @function
_ZN2v88internal26OnHeapConsumedPreparseDataD0Ev:
.LFB24875:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal24BaseConsumedPreparseDataINS0_12PreparseDataEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L11
	movl	$16, %esi
	call	_ZdlPvm@PLT
.L11:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$40, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE24875:
	.size	_ZN2v88internal26OnHeapConsumedPreparseDataD0Ev, .-_ZN2v88internal26OnHeapConsumedPreparseDataD0Ev
	.section	.text._ZN2v88internal24ZoneConsumedPreparseDataD2Ev,"axG",@progbits,_ZN2v88internal24ZoneConsumedPreparseDataD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal24ZoneConsumedPreparseDataD2Ev
	.type	_ZN2v88internal24ZoneConsumedPreparseDataD2Ev, @function
_ZN2v88internal24ZoneConsumedPreparseDataD2Ev:
.LFB24907:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal24BaseConsumedPreparseDataINS0_17ZoneVectorWrapperEEE(%rip), %rax
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L16
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L16:
	ret
	.cfi_endproc
.LFE24907:
	.size	_ZN2v88internal24ZoneConsumedPreparseDataD2Ev, .-_ZN2v88internal24ZoneConsumedPreparseDataD2Ev
	.weak	_ZN2v88internal24ZoneConsumedPreparseDataD1Ev
	.set	_ZN2v88internal24ZoneConsumedPreparseDataD1Ev,_ZN2v88internal24ZoneConsumedPreparseDataD2Ev
	.section	.text._ZN2v88internal24ZoneConsumedPreparseDataD0Ev,"axG",@progbits,_ZN2v88internal24ZoneConsumedPreparseDataD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal24ZoneConsumedPreparseDataD0Ev
	.type	_ZN2v88internal24ZoneConsumedPreparseDataD0Ev, @function
_ZN2v88internal24ZoneConsumedPreparseDataD0Ev:
.LFB24909:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal24BaseConsumedPreparseDataINS0_17ZoneVectorWrapperEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L19
	movl	$16, %esi
	call	_ZdlPvm@PLT
.L19:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$40, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE24909:
	.size	_ZN2v88internal24ZoneConsumedPreparseDataD0Ev, .-_ZN2v88internal24ZoneConsumedPreparseDataD0Ev
	.section	.text._ZN2v88internal26OnHeapConsumedPreparseData12GetChildDataEPNS0_4ZoneEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26OnHeapConsumedPreparseData12GetChildDataEPNS0_4ZoneEi
	.type	_ZN2v88internal26OnHeapConsumedPreparseData12GetChildDataEPNS0_4ZoneEi, @function
_ZN2v88internal26OnHeapConsumedPreparseData12GetChildDataEPNS0_4ZoneEi:
.LFB21001:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	32(%rdi), %rax
	movq	24(%rdi), %r13
	movq	(%rax), %rdx
	movl	7(%rdx), %eax
	addl	$23, %eax
	andl	$-8, %eax
	leal	(%rax,%r8,8), %eax
	cltq
	movq	-1(%rdx,%rax), %rsi
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L25
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rbx
.L26:
	movq	16(%r12), %rax
	movq	24(%r12), %rdx
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L31
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%r12)
.L29:
	leaq	16+_ZTVN2v88internal26OnHeapProducedPreparseDataE(%rip), %rcx
	movq	%rbx, 8(%rax)
	movq	%rcx, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	movq	41088(%r13), %rbx
	cmpq	41096(%r13), %rbx
	je	.L32
.L27:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%rbx)
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L31:
	movl	$16, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L32:
	movq	%r13, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L27
	.cfi_endproc
.LFE21001:
	.size	_ZN2v88internal26OnHeapConsumedPreparseData12GetChildDataEPNS0_4ZoneEi, .-_ZN2v88internal26OnHeapConsumedPreparseData12GetChildDataEPNS0_4ZoneEi
	.section	.rodata._ZN2v88internal24BaseConsumedPreparseDataINS0_12PreparseDataEE27GetDataForSkippableFunctionEPNS0_4ZoneEiPiS6_S6_S6_PbPNS0_12LanguageModeE.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"scope_data_->HasRemainingBytes( PreparseByteDataConstants::kSkippableFunctionMinDataSize)"
	.section	.rodata._ZN2v88internal24BaseConsumedPreparseDataINS0_12PreparseDataEE27GetDataForSkippableFunctionEPNS0_4ZoneEiPiS6_S6_S6_PbPNS0_12LanguageModeE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"Check failed: %s."
	.section	.rodata._ZN2v88internal24BaseConsumedPreparseDataINS0_12PreparseDataEE27GetDataForSkippableFunctionEPNS0_4ZoneEiPiS6_S6_S6_PbPNS0_12LanguageModeE.str1.8
	.align 8
.LC3:
	.string	"start_position == start_position_from_data"
	.section	.text._ZN2v88internal24BaseConsumedPreparseDataINS0_12PreparseDataEE27GetDataForSkippableFunctionEPNS0_4ZoneEiPiS6_S6_S6_PbPNS0_12LanguageModeE,"axG",@progbits,_ZN2v88internal24BaseConsumedPreparseDataINS0_12PreparseDataEE27GetDataForSkippableFunctionEPNS0_4ZoneEiPiS6_S6_S6_PbPNS0_12LanguageModeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal24BaseConsumedPreparseDataINS0_12PreparseDataEE27GetDataForSkippableFunctionEPNS0_4ZoneEiPiS6_S6_S6_PbPNS0_12LanguageModeE
	.type	_ZN2v88internal24BaseConsumedPreparseDataINS0_12PreparseDataEE27GetDataForSkippableFunctionEPNS0_4ZoneEiPiS6_S6_S6_PbPNS0_12LanguageModeE, @function
_ZN2v88internal24BaseConsumedPreparseDataINS0_12PreparseDataEE27GetDataForSkippableFunctionEPNS0_4ZoneEiPiS6_S6_S6_PbPNS0_12LanguageModeE:
.LFB27561:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	leaq	_ZN2v88internal26OnHeapConsumedPreparseData12GetScopeDataEv(%rip), %rdx
	subq	$40, %rsp
	movq	24(%rbp), %rax
	movq	16(%rbp), %r15
	movq	%r9, -64(%rbp)
	movq	32(%rbp), %r14
	movq	%rax, -56(%rbp)
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L34
	movq	32(%rdi), %rax
	movq	(%rax), %rax
.L35:
	movq	8(%rdi), %rdx
	movq	%rax, (%rdx)
	movq	8(%rdi), %rdx
	movq	(%rdx), %r8
	movl	8(%rdx), %eax
	movl	7(%r8), %ecx
	cmpl	%eax, %ecx
	jge	.L55
.L36:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L55:
	subl	%eax, %ecx
	cmpl	$4, %ecx
	jle	.L36
	addl	$1, %eax
	xorl	%ecx, %ecx
	xorl	%r11d, %r11d
	cltq
	.p2align 4,,10
	.p2align 3
.L38:
	movl	%eax, 8(%rdx)
	movzbl	14(%r8,%rax), %r10d
	addq	$1, %rax
	movl	%r10d, %r9d
	andl	$127, %r9d
	sall	%cl, %r9d
	addl	$7, %ecx
	orl	%r9d, %r11d
	testb	%r10b, %r10b
	js	.L38
	movb	$0, 12(%rdx)
	cmpl	%r11d, %ebx
	jne	.L56
	movq	8(%rdi), %r10
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	movl	8(%r10), %eax
	movq	(%r10), %r11
	addl	$1, %eax
	cltq
	.p2align 4,,10
	.p2align 3
.L40:
	movl	%eax, 8(%r10)
	movzbl	14(%r11,%rax), %r8d
	addq	$1, %rax
	movl	%r8d, %edx
	andl	$127, %edx
	sall	%cl, %edx
	addl	$7, %ecx
	orl	%edx, %r9d
	testb	%r8b, %r8b
	js	.L40
	movb	$0, 12(%r10)
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	%r9d, 0(%r13)
	movq	8(%rdi), %r10
	movl	8(%r10), %eax
	movq	(%r10), %r11
	addl	$1, %eax
	cltq
	.p2align 4,,10
	.p2align 3
.L41:
	movl	%eax, 8(%r10)
	movzbl	14(%r11,%rax), %r9d
	addq	$1, %rax
	movl	%r9d, %r8d
	andl	$127, %r8d
	sall	%cl, %r8d
	addl	$7, %ecx
	orl	%r8d, %edx
	testb	%r9b, %r9b
	js	.L41
	movl	%edx, %eax
	movl	%edx, %r9d
	movl	%edx, %ecx
	movb	$0, 12(%r10)
	shrl	$2, %eax
	andl	$1, %r9d
	movzwl	%ax, %eax
	movl	%eax, (%r12)
	andl	$2, %ecx
	je	.L42
	movq	-64(%rbp), %rbx
	movl	%eax, (%rbx)
.L43:
	movq	8(%rdi), %r11
	xorl	%ecx, %ecx
	xorl	%r10d, %r10d
	movl	8(%r11), %eax
	movq	(%r11), %rbx
	addl	$1, %eax
	cltq
	.p2align 4,,10
	.p2align 3
.L45:
	movl	%eax, 8(%r11)
	movzbl	14(%rbx,%rax), %r8d
	addq	$1, %rax
	movl	%r8d, %edx
	andl	$127, %edx
	sall	%cl, %edx
	addl	$7, %ecx
	orl	%edx, %r10d
	testb	%r8b, %r8b
	js	.L45
	movb	$0, 12(%r11)
	movl	%r10d, (%r15)
	movq	8(%rdi), %rdx
	movzbl	12(%rdx), %ecx
	testb	%cl, %cl
	je	.L46
	movzbl	13(%rdx), %eax
	subl	$1, %ecx
.L47:
	movl	%eax, %r8d
	movb	%cl, 12(%rdx)
	leal	0(,%rax,4), %ecx
	movq	-56(%rbp), %rbx
	shrb	$6, %r8b
	movb	%cl, 13(%rdx)
	shrb	$7, %al
	movl	%r8d, %edx
	andl	$1, %edx
	movb	%dl, (%r14)
	movb	%al, (%rbx)
	testb	%r9b, %r9b
	je	.L48
	movl	16(%rdi), %edx
	movq	(%rdi), %rax
	leal	1(%rdx), %ecx
	movq	40(%rax), %rax
	movl	%ecx, 16(%rdi)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L42:
	.cfi_restore_state
	movq	8(%rdi), %rbx
	xorl	%r11d, %r11d
	movl	8(%rbx), %eax
	movq	(%rbx), %r12
	addl	$1, %eax
	cltq
	.p2align 4,,10
	.p2align 3
.L44:
	movl	%eax, 8(%rbx)
	movzbl	14(%r12,%rax), %r8d
	addq	$1, %rax
	movl	%r8d, %edx
	andl	$127, %edx
	sall	%cl, %edx
	addl	$7, %ecx
	orl	%edx, %r11d
	testb	%r8b, %r8b
	js	.L44
	movq	-64(%rbp), %rax
	movb	$0, 12(%rbx)
	movl	%r11d, (%rax)
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L46:
	movl	8(%rdx), %eax
	leal	1(%rax), %ecx
	addl	$16, %eax
	movl	%ecx, 8(%rdx)
	movq	(%rdx), %rcx
	cltq
	movzbl	-1(%rax,%rcx), %eax
	movl	$3, %ecx
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L34:
	movq	%rsi, -80(%rbp)
	movq	%rdi, -72(%rbp)
	call	*%rax
	movq	-80(%rbp), %rsi
	movq	-72(%rbp), %rdi
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L48:
	addq	$40, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	.cfi_restore_state
	leaq	.LC3(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE27561:
	.size	_ZN2v88internal24BaseConsumedPreparseDataINS0_12PreparseDataEE27GetDataForSkippableFunctionEPNS0_4ZoneEiPiS6_S6_S6_PbPNS0_12LanguageModeE, .-_ZN2v88internal24BaseConsumedPreparseDataINS0_12PreparseDataEE27GetDataForSkippableFunctionEPNS0_4ZoneEiPiS6_S6_S6_PbPNS0_12LanguageModeE
	.section	.rodata._ZN2v88internal24ZoneConsumedPreparseData12GetChildDataEPNS0_4ZoneEi.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"data_->children_length() > child_index"
	.section	.text._ZN2v88internal24ZoneConsumedPreparseData12GetChildDataEPNS0_4ZoneEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24ZoneConsumedPreparseData12GetChildDataEPNS0_4ZoneEi
	.type	_ZN2v88internal24ZoneConsumedPreparseData12GetChildDataEPNS0_4ZoneEi, @function
_ZN2v88internal24ZoneConsumedPreparseData12GetChildDataEPNS0_4ZoneEi:
.LFB21030:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	24(%rdi), %rax
	movq	40(%rax), %rcx
	movq	48(%rax), %rax
	subq	%rcx, %rax
	sarq	$3, %rax
	cmpl	%eax, %edx
	jge	.L64
	movslq	%edx, %rdx
	movq	(%rcx,%rdx,8), %rbx
	testq	%rbx, %rbx
	je	.L62
	movq	16(%rsi), %rax
	movq	24(%rsi), %rdx
	movq	%rsi, %rdi
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L65
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%rsi)
.L61:
	leaq	16+_ZTVN2v88internal24ZoneProducedPreparseDataE(%rip), %rcx
	movq	%rbx, 8(%rax)
	movq	%rcx, (%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	.cfi_restore_state
	leaq	.LC4(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L65:
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L61
	.cfi_endproc
.LFE21030:
	.size	_ZN2v88internal24ZoneConsumedPreparseData12GetChildDataEPNS0_4ZoneEi, .-_ZN2v88internal24ZoneConsumedPreparseData12GetChildDataEPNS0_4ZoneEi
	.section	.rodata._ZN2v88internal24BaseConsumedPreparseDataINS0_17ZoneVectorWrapperEE27GetDataForSkippableFunctionEPNS0_4ZoneEiPiS6_S6_S6_PbPNS0_12LanguageModeE.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"vector::_M_range_check: __n (which is %zu) >= this->size() (which is %zu)"
	.section	.text._ZN2v88internal24BaseConsumedPreparseDataINS0_17ZoneVectorWrapperEE27GetDataForSkippableFunctionEPNS0_4ZoneEiPiS6_S6_S6_PbPNS0_12LanguageModeE,"axG",@progbits,_ZN2v88internal24BaseConsumedPreparseDataINS0_17ZoneVectorWrapperEE27GetDataForSkippableFunctionEPNS0_4ZoneEiPiS6_S6_S6_PbPNS0_12LanguageModeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal24BaseConsumedPreparseDataINS0_17ZoneVectorWrapperEE27GetDataForSkippableFunctionEPNS0_4ZoneEiPiS6_S6_S6_PbPNS0_12LanguageModeE
	.type	_ZN2v88internal24BaseConsumedPreparseDataINS0_17ZoneVectorWrapperEE27GetDataForSkippableFunctionEPNS0_4ZoneEiPiS6_S6_S6_PbPNS0_12LanguageModeE, @function
_ZN2v88internal24BaseConsumedPreparseDataINS0_17ZoneVectorWrapperEE27GetDataForSkippableFunctionEPNS0_4ZoneEiPiS6_S6_S6_PbPNS0_12LanguageModeE:
.LFB27559:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	leaq	_ZN2v88internal24ZoneConsumedPreparseData12GetScopeDataEv(%rip), %rdx
	subq	$40, %rsp
	movq	16(%rbp), %rax
	movq	%rax, -56(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -64(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -72(%rbp)
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L67
	movq	32(%rdi), %rax
.L68:
	movq	8(%r12), %rdx
	movq	%rax, (%rdx)
	movq	8(%r12), %rdi
	movq	(%rdi), %rax
	movslq	8(%rdi), %rsi
	movq	16(%rax), %rdx
	subq	8(%rax), %rdx
	cmpl	%esi, %edx
	jge	.L105
.L69:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L105:
	subl	%esi, %edx
	cmpl	$4, %edx
	jle	.L69
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r11d
	.p2align 4,,10
	.p2align 3
.L72:
	leal	(%r11,%rsi), %r8d
	movl	%r8d, 8(%rdi)
	movq	8(%rax), %r10
	movq	16(%rax), %r8
	subq	%r10, %r8
	cmpq	%rsi, %r8
	jbe	.L103
	movzbl	(%r10,%rsi), %r10d
	addq	$1, %rsi
	movl	%r10d, %r8d
	andl	$127, %r8d
	sall	%cl, %r8d
	addl	$7, %ecx
	orl	%r8d, %edx
	testb	%r10b, %r10b
	js	.L72
	movb	$0, 12(%rdi)
	cmpl	%edx, %ebx
	jne	.L106
	movq	8(%r12), %rax
	xorl	%ecx, %ecx
	xorl	%edi, %edi
	movl	$1, %r11d
	movq	(%rax), %r10
	movslq	8(%rax), %rsi
	.p2align 4,,10
	.p2align 3
.L75:
	leal	(%r11,%rsi), %edx
	movl	%edx, 8(%rax)
	movq	8(%r10), %r8
	movq	16(%r10), %rdx
	subq	%r8, %rdx
	cmpq	%rsi, %rdx
	jbe	.L104
	movzbl	(%r8,%rsi), %r8d
	addq	$1, %rsi
	movl	%r8d, %edx
	andl	$127, %edx
	sall	%cl, %edx
	addl	$7, %ecx
	orl	%edx, %edi
	testb	%r8b, %r8b
	js	.L75
	movb	$0, 12(%rax)
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	movl	$1, %r11d
	movl	%edi, (%r14)
	movq	8(%r12), %rdx
	movq	(%rdx), %r10
	movslq	8(%rdx), %rsi
	.p2align 4,,10
	.p2align 3
.L77:
	leal	(%r11,%rsi), %edi
	movl	%edi, 8(%rdx)
	movq	8(%r10), %rdi
	movq	16(%r10), %r8
	subq	%rdi, %r8
	cmpq	%rsi, %r8
	jbe	.L103
	movzbl	(%rdi,%rsi), %r8d
	addq	$1, %rsi
	movl	%r8d, %edi
	andl	$127, %edi
	sall	%cl, %edi
	addl	$7, %ecx
	orl	%edi, %eax
	testb	%r8b, %r8b
	js	.L77
	movb	$0, 12(%rdx)
	movl	%eax, %edx
	movl	%eax, %r11d
	shrl	$2, %edx
	andl	$1, %r11d
	andl	$2, %eax
	movzwl	%dx, %edx
	movl	%eax, %ecx
	movl	%edx, (%r15)
	je	.L78
	movl	%edx, (%r9)
.L79:
	movq	8(%r12), %rax
	xorl	%ecx, %ecx
	xorl	%edi, %edi
	movl	$1, %r10d
	movq	(%rax), %r9
	movslq	8(%rax), %rsi
	.p2align 4,,10
	.p2align 3
.L83:
	leal	(%r10,%rsi), %edx
	movl	%edx, 8(%rax)
	movq	8(%r9), %r8
	movq	16(%r9), %rdx
	subq	%r8, %rdx
	cmpq	%rdx, %rsi
	jnb	.L104
	movzbl	(%r8,%rsi), %r8d
	addq	$1, %rsi
	movl	%r8d, %edx
	andl	$127, %edx
	sall	%cl, %edx
	addl	$7, %ecx
	orl	%edx, %edi
	testb	%r8b, %r8b
	js	.L83
	movb	$0, 12(%rax)
	movq	-56(%rbp), %rax
	movl	%edi, (%rax)
	movq	8(%r12), %rax
	movzbl	12(%rax), %ecx
	testb	%cl, %cl
	je	.L84
	movzbl	13(%rax), %edx
	subl	$1, %ecx
.L85:
	movl	%edx, %esi
	movb	%cl, 12(%rax)
	leal	0(,%rdx,4), %ecx
	movq	-72(%rbp), %rbx
	shrb	$6, %sil
	movb	%cl, 13(%rax)
	shrb	$7, %dl
	movl	%esi, %eax
	andl	$1, %eax
	movb	%al, (%rbx)
	movq	-64(%rbp), %rax
	movb	%dl, (%rax)
	testb	%r11b, %r11b
	je	.L91
	movslq	16(%r12), %rdx
	movq	(%r12), %rax
	leal	1(%rdx), %ecx
	movq	40(%rax), %rax
	movl	%ecx, 16(%r12)
	leaq	_ZN2v88internal24ZoneConsumedPreparseData12GetChildDataEPNS0_4ZoneEi(%rip), %rcx
	cmpq	%rcx, %rax
	jne	.L89
	movq	24(%r12), %rax
	movq	40(%rax), %rcx
	movq	48(%rax), %rax
	subq	%rcx, %rax
	sarq	$3, %rax
	cmpl	%eax, %edx
	jge	.L107
	movq	(%rcx,%rdx,8), %rbx
	testq	%rbx, %rbx
	je	.L91
	movq	16(%r13), %rax
	movq	24(%r13), %rdx
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L108
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%r13)
.L93:
	leaq	16+_ZTVN2v88internal24ZoneProducedPreparseDataE(%rip), %rcx
	movq	%rbx, 8(%rax)
	movq	%rcx, (%rax)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore_state
	movslq	8(%rax), %rsi
	leal	1(%rsi), %edx
	movl	%edx, 8(%rax)
	movq	(%rax), %rdx
	movq	8(%rdx), %rcx
	movq	16(%rdx), %rdx
	subq	%rcx, %rdx
	cmpq	%rdx, %rsi
	jnb	.L104
	movzbl	(%rcx,%rsi), %edx
	movl	$3, %ecx
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L78:
	movq	8(%r12), %rbx
	xorl	%r8d, %r8d
	movl	$1, %r14d
	movq	(%rbx), %r10
	movslq	8(%rbx), %rsi
	.p2align 4,,10
	.p2align 3
.L81:
	leal	(%r14,%rsi), %eax
	movl	%eax, 8(%rbx)
	movq	8(%r10), %rax
	movq	16(%r10), %rdx
	subq	%rax, %rdx
	cmpq	%rsi, %rdx
	jbe	.L104
	movzbl	(%rax,%rsi), %edx
	addq	$1, %rsi
	movl	%edx, %eax
	andl	$127, %eax
	sall	%cl, %eax
	addl	$7, %ecx
	orl	%eax, %r8d
	testb	%dl, %dl
	js	.L81
	movb	$0, 12(%rbx)
	movl	%r8d, (%r9)
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L91:
	addq	$40, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_restore_state
	movq	%r9, -80(%rbp)
	call	*%rax
	movq	-80(%rbp), %r9
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L89:
	addq	$40, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L106:
	.cfi_restore_state
	leaq	.LC3(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L107:
	leaq	.LC4(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L108:
	movl	$16, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L103:
	movq	%r8, %rdx
.L104:
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE27559:
	.size	_ZN2v88internal24BaseConsumedPreparseDataINS0_17ZoneVectorWrapperEE27GetDataForSkippableFunctionEPNS0_4ZoneEiPiS6_S6_S6_PbPNS0_12LanguageModeE, .-_ZN2v88internal24BaseConsumedPreparseDataINS0_17ZoneVectorWrapperEE27GetDataForSkippableFunctionEPNS0_4ZoneEiPiS6_S6_S6_PbPNS0_12LanguageModeE
	.section	.text._ZN2v88internal8Variable16SetMaybeAssignedEv,"axG",@progbits,_ZN2v88internal8Variable16SetMaybeAssignedEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8Variable16SetMaybeAssignedEv
	.type	_ZN2v88internal8Variable16SetMaybeAssignedEv, @function
_ZN2v88internal8Variable16SetMaybeAssignedEv:
.LFB9450:
	.cfi_startproc
	endbr64
	movzwl	40(%rdi), %eax
	movl	%eax, %edx
	andl	$15, %edx
	cmpb	$1, %dl
	je	.L119
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L111
	testb	$64, %ah
	je	.L122
.L111:
	orb	$64, %ah
	movw	%ax, 40(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L119:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L122:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal8Variable16SetMaybeAssignedEv
	movzwl	40(%rbx), %eax
	orb	$64, %ah
	movw	%ax, 40(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9450:
	.size	_ZN2v88internal8Variable16SetMaybeAssignedEv, .-_ZN2v88internal8Variable16SetMaybeAssignedEv
	.section	.text._ZN2v88internal19PreparseDataBuilderC2EPNS0_4ZoneEPS1_PSt6vectorIPvSaIS6_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19PreparseDataBuilderC2EPNS0_4ZoneEPS1_PSt6vectorIPvSaIS6_EE
	.type	_ZN2v88internal19PreparseDataBuilderC2EPNS0_4ZoneEPS1_PSt6vectorIPvSaIS6_EE, @function
_ZN2v88internal19PreparseDataBuilderC2EPNS0_4ZoneEPS1_PSt6vectorIPvSaIS6_EE:
.LFB20947:
	.cfi_startproc
	endbr64
	movq	%rdx, (%rdi)
	movq	$0, 8(%rdi)
	movl	$0, 16(%rdi)
	movb	$0, 24(%rdi)
	movq	8(%rcx), %rax
	subq	(%rcx), %rax
	movq	%rcx, 32(%rdi)
	sarq	$3, %rax
	andb	$-4, 76(%rdi)
	movq	%rax, %xmm0
	movl	$4294967295, %eax
	movq	$0, 56(%rdi)
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, 64(%rdi)
	movl	$0, 72(%rdi)
	movups	%xmm0, 40(%rdi)
	ret
	.cfi_endproc
.LFE20947:
	.size	_ZN2v88internal19PreparseDataBuilderC2EPNS0_4ZoneEPS1_PSt6vectorIPvSaIS6_EE, .-_ZN2v88internal19PreparseDataBuilderC2EPNS0_4ZoneEPS1_PSt6vectorIPvSaIS6_EE
	.globl	_ZN2v88internal19PreparseDataBuilderC1EPNS0_4ZoneEPS1_PSt6vectorIPvSaIS6_EE
	.set	_ZN2v88internal19PreparseDataBuilderC1EPNS0_4ZoneEPS1_PSt6vectorIPvSaIS6_EE,_ZN2v88internal19PreparseDataBuilderC2EPNS0_4ZoneEPS1_PSt6vectorIPvSaIS6_EE
	.section	.text._ZN2v88internal19PreparseDataBuilder18DataGatheringScope5StartEPNS0_16DeclarationScopeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19PreparseDataBuilder18DataGatheringScope5StartEPNS0_16DeclarationScopeE
	.type	_ZN2v88internal19PreparseDataBuilder18DataGatheringScope5StartEPNS0_16DeclarationScopeE, @function
_ZN2v88internal19PreparseDataBuilder18DataGatheringScope5StartEPNS0_16DeclarationScopeE:
.LFB20949:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %r12
	movq	40(%r12), %rax
	movq	288(%r12), %r15
	leaq	296(%r12), %r14
	movq	1096(%rax), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$79, %rdx
	jbe	.L128
	leaq	80(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L126:
	movq	%r15, (%rax)
	movl	$4294967295, %ecx
	movq	$0, 8(%rax)
	movl	$0, 16(%rax)
	movb	$0, 24(%rax)
	movq	304(%r12), %rdx
	subq	296(%r12), %rdx
	movq	%r14, 32(%rax)
	sarq	$3, %rdx
	andb	$-4, 76(%rax)
	movq	%rdx, %xmm0
	movq	$0, 56(%rax)
	punpcklqdq	%xmm0, %xmm0
	movq	%rcx, 64(%rax)
	movl	$0, 72(%rax)
	movups	%xmm0, 40(%rax)
	movq	(%rbx), %rdx
	movq	%rax, 8(%rbx)
	movq	%rax, 288(%rdx)
	movq	%rax, 208(%r13)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L128:
	.cfi_restore_state
	movl	$80, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L126
	.cfi_endproc
.LFE20949:
	.size	_ZN2v88internal19PreparseDataBuilder18DataGatheringScope5StartEPNS0_16DeclarationScopeE, .-_ZN2v88internal19PreparseDataBuilder18DataGatheringScope5StartEPNS0_16DeclarationScopeE
	.section	.text._ZN2v88internal19PreparseDataBuilder8ByteData5StartEPSt6vectorIhSaIhEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19PreparseDataBuilder8ByteData5StartEPSt6vectorIhSaIhEE
	.type	_ZN2v88internal19PreparseDataBuilder8ByteData5StartEPSt6vectorIhSaIhEE, @function
_ZN2v88internal19PreparseDataBuilder8ByteData5StartEPSt6vectorIhSaIhEE:
.LFB20951:
	.cfi_startproc
	endbr64
	movq	%rsi, (%rdi)
	ret
	.cfi_endproc
.LFE20951:
	.size	_ZN2v88internal19PreparseDataBuilder8ByteData5StartEPSt6vectorIhSaIhEE, .-_ZN2v88internal19PreparseDataBuilder8ByteData5StartEPSt6vectorIhSaIhEE
	.section	.rodata._ZN2v88internal19PreparseDataBuilder8ByteData7ReserveEm.str1.1,"aMS",@progbits,1
.LC6:
	.string	"vector::_M_fill_insert"
	.section	.text._ZN2v88internal19PreparseDataBuilder8ByteData7ReserveEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19PreparseDataBuilder8ByteData7ReserveEm
	.type	_ZN2v88internal19PreparseDataBuilder8ByteData7ReserveEm, @function
_ZN2v88internal19PreparseDataBuilder8ByteData7ReserveEm:
.LFB20953:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r12
	movslq	8(%rdi), %rax
	movq	8(%r12), %r14
	movq	%r14, %r13
	subq	(%r12), %r13
	movq	%r13, %rdx
	subq	%rax, %rdx
	cmpq	%rsi, %rdx
	jb	.L156
.L130:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L156:
	.cfi_restore_state
	addq	%rax, %rsi
	movq	%rsi, %rbx
	subq	%r13, %rbx
	je	.L130
	movq	16(%r12), %rax
	subq	%r14, %rax
	cmpq	%rax, %rbx
	ja	.L133
	movq	%rbx, %rdx
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	memset@PLT
	leaq	(%r14,%rbx), %rsi
	movq	%rsi, 8(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L133:
	.cfi_restore_state
	movabsq	$9223372036854775807, %rdx
	movq	%rdx, %rax
	subq	%r13, %rax
	cmpq	%rax, %rbx
	ja	.L157
	cmpq	%r13, %rbx
	movq	%r13, %rax
	cmovnb	%rbx, %rax
	addq	%r13, %rax
	movq	%rax, %r15
	jc	.L143
	testq	%rax, %rax
	js	.L143
	jne	.L137
	movq	$0, -56(%rbp)
	xorl	%r15d, %r15d
.L141:
	movq	-56(%rbp), %rax
	xorl	%esi, %esi
	movq	%rbx, %rdx
	leaq	(%rax,%r13), %rdi
	xorl	%r13d, %r13d
	call	memset@PLT
	movq	(%r12), %rsi
	movq	%r14, %rax
	subq	%rsi, %rax
	jne	.L158
.L138:
	addq	%r13, %rbx
	movq	8(%r12), %r13
	addq	-56(%rbp), %rbx
	subq	%r14, %r13
	jne	.L159
.L139:
	movq	(%r12), %rdi
	addq	%r13, %rbx
	testq	%rdi, %rdi
	je	.L140
	call	_ZdlPv@PLT
.L140:
	movq	-56(%rbp), %xmm0
	movq	%rbx, %xmm1
	movq	%r15, 16(%r12)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%r12)
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L143:
	movq	%rdx, %r15
.L137:
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%rax, -56(%rbp)
	addq	%rax, %r15
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L158:
	movq	-56(%rbp), %rdi
	movq	%rax, %rdx
	movq	%rax, %r13
	call	memmove@PLT
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L159:
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	memmove@PLT
	jmp	.L139
.L157:
	leaq	.LC6(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE20953:
	.size	_ZN2v88internal19PreparseDataBuilder8ByteData7ReserveEm, .-_ZN2v88internal19PreparseDataBuilder8ByteData7ReserveEm
	.section	.text._ZNK2v88internal19PreparseDataBuilder8ByteData6lengthEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal19PreparseDataBuilder8ByteData6lengthEv
	.type	_ZNK2v88internal19PreparseDataBuilder8ByteData6lengthEv, @function
_ZNK2v88internal19PreparseDataBuilder8ByteData6lengthEv:
.LFB20954:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE20954:
	.size	_ZNK2v88internal19PreparseDataBuilder8ByteData6lengthEv, .-_ZNK2v88internal19PreparseDataBuilder8ByteData6lengthEv
	.section	.text._ZN2v88internal19PreparseDataBuilder8ByteData3AddEh,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19PreparseDataBuilder8ByteData3AddEh
	.type	_ZN2v88internal19PreparseDataBuilder8ByteData3AddEh, @function
_ZN2v88internal19PreparseDataBuilder8ByteData3AddEh:
.LFB20955:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	leal	1(%rax), %edx
	movl	%edx, 8(%rdi)
	movq	(%rdi), %rdx
	movq	(%rdx), %rdx
	movb	%sil, (%rdx,%rax)
	ret
	.cfi_endproc
.LFE20955:
	.size	_ZN2v88internal19PreparseDataBuilder8ByteData3AddEh, .-_ZN2v88internal19PreparseDataBuilder8ByteData3AddEh
	.section	.text._ZN2v88internal19PreparseDataBuilder8ByteData13WriteVarint32Ej,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19PreparseDataBuilder8ByteData13WriteVarint32Ej
	.type	_ZN2v88internal19PreparseDataBuilder8ByteData13WriteVarint32Ej, @function
_ZN2v88internal19PreparseDataBuilder8ByteData13WriteVarint32Ej:
.LFB20956:
	.cfi_startproc
	endbr64
	.p2align 4,,10
	.p2align 3
.L169:
	movslq	8(%rdi), %rax
	movl	%esi, %edx
	andl	$127, %edx
	shrl	$7, %esi
	leal	1(%rax), %ecx
	movl	%ecx, 8(%rdi)
	movq	(%rdi), %rcx
	movq	(%rcx), %rcx
	jne	.L164
	movb	%dl, (%rcx,%rax)
	movb	$0, 16(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L164:
	orl	$-128, %edx
	movb	%dl, (%rcx,%rax)
	jmp	.L169
	.cfi_endproc
.LFE20956:
	.size	_ZN2v88internal19PreparseDataBuilder8ByteData13WriteVarint32Ej, .-_ZN2v88internal19PreparseDataBuilder8ByteData13WriteVarint32Ej
	.section	.text._ZN2v88internal19PreparseDataBuilder8ByteData10WriteUint8Eh,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19PreparseDataBuilder8ByteData10WriteUint8Eh
	.type	_ZN2v88internal19PreparseDataBuilder8ByteData10WriteUint8Eh, @function
_ZN2v88internal19PreparseDataBuilder8ByteData10WriteUint8Eh:
.LFB20957:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	leal	1(%rax), %edx
	movl	%edx, 8(%rdi)
	movq	(%rdi), %rdx
	movq	(%rdx), %rdx
	movb	%sil, (%rdx,%rax)
	movb	$0, 16(%rdi)
	ret
	.cfi_endproc
.LFE20957:
	.size	_ZN2v88internal19PreparseDataBuilder8ByteData10WriteUint8Eh, .-_ZN2v88internal19PreparseDataBuilder8ByteData10WriteUint8Eh
	.section	.text._ZN2v88internal19PreparseDataBuilder8ByteData12WriteQuarterEh,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19PreparseDataBuilder8ByteData12WriteQuarterEh
	.type	_ZN2v88internal19PreparseDataBuilder8ByteData12WriteQuarterEh, @function
_ZN2v88internal19PreparseDataBuilder8ByteData12WriteQuarterEh:
.LFB20958:
	.cfi_startproc
	endbr64
	movzbl	16(%rdi), %ecx
	movslq	8(%rdi), %rax
	movq	(%rdi), %rdx
	testb	%cl, %cl
	jne	.L172
	leal	1(%rax), %ecx
	movl	%ecx, 8(%rdi)
	movq	(%rdx), %rdx
	movl	$6, %ecx
	movb	$0, (%rdx,%rax)
	movl	8(%rdi), %eax
	movb	$3, 16(%rdi)
	movq	(%rdi), %rdx
.L173:
	subl	$1, %eax
	movzbl	%sil, %esi
	cltq
	addq	(%rdx), %rax
	sall	%cl, %esi
	orb	%sil, (%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L172:
	subl	$1, %ecx
	movb	%cl, 16(%rdi)
	addl	%ecx, %ecx
	movzbl	%cl, %ecx
	jmp	.L173
	.cfi_endproc
.LFE20958:
	.size	_ZN2v88internal19PreparseDataBuilder8ByteData12WriteQuarterEh, .-_ZN2v88internal19PreparseDataBuilder8ByteData12WriteQuarterEh
	.section	.text._ZN2v88internal19PreparseDataBuilder18DataGatheringScope20SetSkippableFunctionEPNS0_16DeclarationScopeEii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19PreparseDataBuilder18DataGatheringScope20SetSkippableFunctionEPNS0_16DeclarationScopeEii
	.type	_ZN2v88internal19PreparseDataBuilder18DataGatheringScope20SetSkippableFunctionEPNS0_16DeclarationScopeEii, @function
_ZN2v88internal19PreparseDataBuilder18DataGatheringScope20SetSkippableFunctionEPNS0_16DeclarationScopeEii:
.LFB20959:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	%rsi, 56(%rax)
	movq	8(%rdi), %rax
	movl	%edx, 64(%rax)
	movq	8(%rdi), %rax
	movl	%ecx, 68(%rax)
	movq	8(%rdi), %rax
	movq	(%rax), %rax
	orb	$2, 76(%rax)
	ret
	.cfi_endproc
.LFE20959:
	.size	_ZN2v88internal19PreparseDataBuilder18DataGatheringScope20SetSkippableFunctionEPNS0_16DeclarationScopeEii, .-_ZN2v88internal19PreparseDataBuilder18DataGatheringScope20SetSkippableFunctionEPNS0_16DeclarationScopeEii
	.section	.text._ZNK2v88internal19PreparseDataBuilder17HasInnerFunctionsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal19PreparseDataBuilder17HasInnerFunctionsEv
	.type	_ZNK2v88internal19PreparseDataBuilder17HasInnerFunctionsEv, @function
_ZNK2v88internal19PreparseDataBuilder17HasInnerFunctionsEv:
.LFB20960:
	.cfi_startproc
	endbr64
	cmpq	$0, 40(%rdi)
	setne	%al
	ret
	.cfi_endproc
.LFE20960:
	.size	_ZNK2v88internal19PreparseDataBuilder17HasInnerFunctionsEv, .-_ZNK2v88internal19PreparseDataBuilder17HasInnerFunctionsEv
	.section	.text._ZNK2v88internal19PreparseDataBuilder7HasDataEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal19PreparseDataBuilder7HasDataEv
	.type	_ZNK2v88internal19PreparseDataBuilder7HasDataEv, @function
_ZNK2v88internal19PreparseDataBuilder7HasDataEv:
.LFB20961:
	.cfi_startproc
	endbr64
	movzbl	76(%rdi), %edx
	xorl	%eax, %eax
	testb	$1, %dl
	jne	.L176
	movl	%edx, %eax
	shrb	%al
	andl	$1, %eax
.L176:
	ret
	.cfi_endproc
.LFE20961:
	.size	_ZNK2v88internal19PreparseDataBuilder7HasDataEv, .-_ZNK2v88internal19PreparseDataBuilder7HasDataEv
	.section	.text._ZNK2v88internal19PreparseDataBuilder16HasDataForParentEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal19PreparseDataBuilder16HasDataForParentEv
	.type	_ZNK2v88internal19PreparseDataBuilder16HasDataForParentEv, @function
_ZNK2v88internal19PreparseDataBuilder16HasDataForParentEv:
.LFB20962:
	.cfi_startproc
	endbr64
	movzbl	76(%rdi), %eax
	testb	$1, %al
	jne	.L180
	shrb	%al
	andl	$1, %eax
	je	.L180
	ret
	.p2align 4,,10
	.p2align 3
.L180:
	cmpq	$0, 56(%rdi)
	setne	%al
	ret
	.cfi_endproc
.LFE20962:
	.size	_ZNK2v88internal19PreparseDataBuilder16HasDataForParentEv, .-_ZNK2v88internal19PreparseDataBuilder16HasDataForParentEv
	.section	.text._ZN2v88internal19PreparseDataBuilder14ScopeNeedsDataEPNS0_5ScopeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19PreparseDataBuilder14ScopeNeedsDataEPNS0_5ScopeE
	.type	_ZN2v88internal19PreparseDataBuilder14ScopeNeedsDataEPNS0_5ScopeE, @function
_ZN2v88internal19PreparseDataBuilder14ScopeNeedsDataEPNS0_5ScopeE:
.LFB20965:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	cmpb	$2, 128(%rdi)
	je	.L196
	testb	$16, 129(%rdi)
	jne	.L185
	movq	64(%rdi), %rcx
	leaq	56(%rdi), %rax
	cmpq	%rcx, %rax
	je	.L185
.L187:
	movq	(%rax), %rdx
	movzbl	40(%rdx), %eax
	andl	$15, %eax
	cmpb	$2, %al
	jbe	.L186
	subl	$7, %eax
	cmpb	$3, %al
	ja	.L197
.L186:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L197:
	.cfi_restore_state
	leaq	24(%rdx), %rax
	cmpq	%rax, %rcx
	jne	.L187
	.p2align 4,,10
	.p2align 3
.L185:
	movq	16(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L189
.L188:
	movq	%rbx, %rdi
	call	_ZN2v88internal19PreparseDataBuilder14ScopeNeedsDataEPNS0_5ScopeE
	testb	%al, %al
	jne	.L186
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L188
.L189:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L196:
	.cfi_restore_state
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movzbl	133(%rax), %eax
	subl	$3, %eax
	cmpb	$1, %al
	seta	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20965:
	.size	_ZN2v88internal19PreparseDataBuilder14ScopeNeedsDataEPNS0_5ScopeE, .-_ZN2v88internal19PreparseDataBuilder14ScopeNeedsDataEPNS0_5ScopeE
	.section	.text._ZN2v88internal19PreparseDataBuilder28SaveDataForSkippableFunctionEPS1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19PreparseDataBuilder28SaveDataForSkippableFunctionEPS1_
	.type	_ZN2v88internal19PreparseDataBuilder28SaveDataForSkippableFunctionEPS1_, @function
_ZN2v88internal19PreparseDataBuilder28SaveDataForSkippableFunctionEPS1_:
.LFB20966:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	56(%rsi), %rcx
	movl	112(%rcx), %edx
	movl	%edx, %r8d
	andl	$127, %r8d
	shrl	$7, %edx
	jne	.L200
	.p2align 4,,10
	.p2align 3
.L199:
	movslq	16(%rdi), %rax
	leal	1(%rax), %edx
	movl	%edx, 16(%rdi)
	movq	8(%rdi), %rdx
	movq	(%rdx), %rdx
	movb	%r8b, (%rdx,%rax)
	movb	$0, 24(%rdi)
	movl	116(%rcx), %edx
	movl	%edx, %r8d
	andl	$127, %r8d
	shrl	$7, %edx
	jne	.L201
	.p2align 4,,10
	.p2align 3
.L202:
	movslq	16(%rdi), %rax
	leal	1(%rax), %edx
	movl	%edx, 16(%rdi)
	movq	8(%rdi), %rdx
	movq	(%rdx), %rdx
	movb	%r8b, (%rdx,%rax)
	movb	$0, 24(%rdi)
	movzbl	76(%rsi), %r8d
	testb	$1, %r8b
	jne	.L217
	shrb	%r8b
	xorl	%r9d, %r9d
	andl	$1, %r8d
	setne	%r9b
.L203:
	movl	136(%rcx), %r10d
	movl	64(%rsi), %r11d
	xorl	%edx, %edx
	leal	0(,%r10,4), %eax
	andl	$262140, %eax
	cmpl	%r10d, %r11d
	sete	%dl
	addl	%edx, %edx
	orl	%edx, %eax
	orl	%r9d, %eax
	movl	%eax, %ebx
	andl	$127, %ebx
	shrl	$7, %eax
	jne	.L205
	.p2align 4,,10
	.p2align 3
.L204:
	movslq	16(%rdi), %rax
	leal	1(%rax), %edx
	movl	%edx, 16(%rdi)
	movq	8(%rdi), %rdx
	movq	(%rdx), %rdx
	movb	%bl, (%rdx,%rax)
	movb	$0, 24(%rdi)
	cmpl	%r10d, %r11d
	je	.L213
	movl	64(%rsi), %edx
	movl	%edx, %r9d
	andl	$127, %r9d
	shrl	$7, %edx
	je	.L206
	.p2align 4,,10
	.p2align 3
.L207:
	movslq	16(%rdi), %rax
	orl	$-128, %r9d
	leal	1(%rax), %r10d
	movl	%r10d, 16(%rdi)
	movq	8(%rdi), %r10
	movq	(%r10), %r10
	movb	%r9b, (%r10,%rax)
	movl	%edx, %r9d
	andl	$127, %r9d
	shrl	$7, %edx
	jne	.L207
.L206:
	movslq	16(%rdi), %rax
	leal	1(%rax), %edx
	movl	%edx, 16(%rdi)
	movq	8(%rdi), %rdx
	movq	(%rdx), %rdx
	movb	%r9b, (%rdx,%rax)
	movb	$0, 24(%rdi)
.L213:
	movl	68(%rsi), %edx
	movl	%edx, %esi
	andl	$127, %esi
	shrl	$7, %edx
	jne	.L209
	.p2align 4,,10
	.p2align 3
.L208:
	movslq	16(%rdi), %rax
	leal	1(%rax), %edx
	movl	%edx, 16(%rdi)
	movq	8(%rdi), %rdx
	movq	(%rdx), %rdx
	movb	%sil, (%rdx,%rax)
	movb	$0, 24(%rdi)
	movzbl	129(%rcx), %eax
	movl	%eax, %edx
	andl	$1, %edx
	testb	$32, 131(%rcx)
	je	.L212
	orl	$2, %edx
.L210:
	movslq	16(%rdi), %rax
	sall	$6, %edx
	leal	1(%rax), %ecx
	movl	%ecx, 16(%rdi)
	movq	8(%rdi), %rcx
	movq	(%rcx), %rcx
	movb	$0, (%rcx,%rax)
	movl	16(%rdi), %eax
	movq	8(%rdi), %rcx
	movb	$3, 24(%rdi)
	subl	$1, %eax
	cltq
	addq	(%rcx), %rax
	orb	%dl, (%rax)
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L200:
	.cfi_restore_state
	movslq	16(%rdi), %rax
	orl	$-128, %r8d
	leal	1(%rax), %r9d
	movl	%r9d, 16(%rdi)
	movq	8(%rdi), %r9
	movq	(%r9), %r9
	movb	%r8b, (%r9,%rax)
	movl	%edx, %r8d
	andl	$127, %r8d
	shrl	$7, %edx
	jne	.L200
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L205:
	movslq	16(%rdi), %rdx
	orl	$-128, %ebx
	leal	1(%rdx), %r9d
	movl	%r9d, 16(%rdi)
	movq	8(%rdi), %r9
	movq	(%r9), %r9
	movb	%bl, (%r9,%rdx)
	movl	%eax, %ebx
	andl	$127, %ebx
	shrl	$7, %eax
	jne	.L205
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L201:
	movslq	16(%rdi), %rax
	orl	$-128, %r8d
	leal	1(%rax), %r9d
	movl	%r9d, 16(%rdi)
	movq	8(%rdi), %r9
	movq	(%r9), %r9
	movb	%r8b, (%r9,%rax)
	movl	%edx, %r8d
	andl	$127, %r8d
	shrl	$7, %edx
	jne	.L201
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L209:
	movslq	16(%rdi), %rax
	orl	$-128, %esi
	leal	1(%rax), %r9d
	movl	%r9d, 16(%rdi)
	movq	8(%rdi), %r9
	movq	(%r9), %r9
	movb	%sil, (%r9,%rax)
	movl	%edx, %esi
	andl	$127, %esi
	shrl	$7, %edx
	jne	.L209
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L217:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L212:
	testb	$64, %al
	je	.L210
	movzbl	133(%rcx), %ecx
	cmpb	$17, %cl
	ja	.L210
	movl	$235772, %eax
	shrq	%cl, %rax
	andl	$1, %eax
	addl	%eax, %eax
	orl	%eax, %edx
	jmp	.L210
	.cfi_endproc
.LFE20966:
	.size	_ZN2v88internal19PreparseDataBuilder28SaveDataForSkippableFunctionEPS1_, .-_ZN2v88internal19PreparseDataBuilder28SaveDataForSkippableFunctionEPS1_
	.section	.text._ZN2v88internal19PreparseDataBuilder19SaveDataForVariableEPNS0_8VariableE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19PreparseDataBuilder19SaveDataForVariableEPNS0_8VariableE
	.type	_ZN2v88internal19PreparseDataBuilder19SaveDataForVariableEPNS0_8VariableE, @function
_ZN2v88internal19PreparseDataBuilder19SaveDataForVariableEPNS0_8VariableE:
.LFB20969:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movzwl	40(%rsi), %eax
	movq	8(%rdi), %r12
	movslq	16(%rdi), %rcx
	movl	%eax, %edx
	shrl	$14, %eax
	movq	8(%r12), %r14
	shrw	$10, %dx
	andl	$1, %eax
	andl	$1, %edx
	addl	%edx, %edx
	orl	%eax, %edx
	movq	%rcx, %rax
	movl	%edx, %r13d
	movq	%r14, %rdx
	subq	(%r12), %rdx
	cmpq	%rcx, %rdx
	je	.L268
.L243:
	movzbl	24(%rbx), %ecx
	movzbl	%r13b, %edx
	testb	%cl, %cl
	je	.L269
	subl	$1, %ecx
	movb	%cl, 24(%rbx)
	addl	%ecx, %ecx
	movzbl	%cl, %ecx
.L253:
	subl	$1, %eax
	sall	%cl, %edx
	cltq
	addq	(%r12), %rax
	orb	%dl, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L269:
	.cfi_restore_state
	leal	1(%rax), %ecx
	movl	%ecx, 16(%rbx)
	movq	(%r12), %rcx
	movb	$0, (%rcx,%rax)
	movl	$6, %ecx
	movl	16(%rbx), %eax
	movb	$3, 24(%rbx)
	movq	8(%rbx), %r12
	jmp	.L253
	.p2align 4,,10
	.p2align 3
.L268:
	cmpq	16(%r12), %r14
	je	.L244
	movb	$0, (%r14)
	addq	$1, %r14
	movq	%r14, 8(%r12)
.L245:
	movslq	16(%rbx), %rax
	movq	8(%rbx), %r12
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L244:
	testq	%rdx, %rdx
	movl	$1, %eax
	cmovne	%rdx, %rax
	addq	%rdx, %rax
	movq	%rax, %r15
	jc	.L256
	testq	%rax, %rax
	js	.L256
	jne	.L248
	xorl	%r15d, %r15d
	xorl	%ecx, %ecx
.L254:
	movb	$0, (%rcx,%rdx)
	movq	(%r12), %rsi
	movq	%r14, %rax
	xorl	%edx, %edx
	subq	%rsi, %rax
	jne	.L270
.L249:
	leaq	1(%rcx,%rdx), %rdi
	movq	8(%r12), %rdx
	subq	%r14, %rdx
	jne	.L271
.L250:
	leaq	(%rdi,%rdx), %r14
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L251
	movq	%rcx, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rcx
.L251:
	movq	%rcx, %xmm0
	movq	%r14, %xmm1
	movq	%r15, 16(%r12)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%r12)
	jmp	.L245
	.p2align 4,,10
	.p2align 3
.L256:
	movabsq	$9223372036854775807, %r15
.L248:
	movq	%r15, %rdi
	movq	%rdx, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %rcx
	addq	%rax, %r15
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L271:
	movq	%r14, %rsi
	movq	%rcx, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	memmove@PLT
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %rdx
	movq	%rax, %rdi
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L270:
	movq	%rax, %rdx
	movq	%rcx, %rdi
	movq	%rax, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %rcx
	jmp	.L249
	.cfi_endproc
.LFE20969:
	.size	_ZN2v88internal19PreparseDataBuilder19SaveDataForVariableEPNS0_8VariableE, .-_ZN2v88internal19PreparseDataBuilder19SaveDataForVariableEPNS0_8VariableE
	.section	.text._ZN2v88internal19PreparseDataBuilder8ByteData10CopyToHeapEPNS0_7IsolateEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19PreparseDataBuilder8ByteData10CopyToHeapEPNS0_7IsolateEi
	.type	_ZN2v88internal19PreparseDataBuilder8ByteData10CopyToHeapEPNS0_7IsolateEi, @function
_ZN2v88internal19PreparseDataBuilder8ByteData10CopyToHeapEPNS0_7IsolateEi:
.LFB20971:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	8(%r13), %rbx
	movl	%ebx, %esi
	call	_ZN2v88internal7Factory15NewPreparseDataEii@PLT
	movq	0(%r13), %rsi
	movslq	%ebx, %rdx
	movq	%rax, %r12
	movq	(%rax), %rax
	leaq	15(%rax), %rdi
	call	memcpy@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20971:
	.size	_ZN2v88internal19PreparseDataBuilder8ByteData10CopyToHeapEPNS0_7IsolateEi, .-_ZN2v88internal19PreparseDataBuilder8ByteData10CopyToHeapEPNS0_7IsolateEi
	.section	.text._ZN2v88internal19PreparseDataBuilder9SerializeEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19PreparseDataBuilder9SerializeEPNS0_7IsolateE
	.type	_ZN2v88internal19PreparseDataBuilder9SerializeEPNS0_7IsolateE, @function
_ZN2v88internal19PreparseDataBuilder9SerializeEPNS0_7IsolateE:
.LFB20972:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rbx
	movl	72(%rdi), %edx
	movq	%r13, %rdi
	movl	%ebx, %esi
	call	_ZN2v88internal7Factory15NewPreparseDataEii@PLT
	movq	8(%r12), %rsi
	movslq	%ebx, %rdx
	movq	%rax, %r14
	movq	(%rax), %rax
	leaq	15(%rax), %rdi
	call	memcpy@PLT
	movq	32(%r12), %rbx
	movq	40(%r12), %rax
	leaq	(%rbx,%rax,8), %r12
	cmpq	%r12, %rbx
	je	.L287
	xorl	%r15d, %r15d
.L282:
	movq	(%rbx), %rdi
	movzbl	76(%rdi), %eax
	testb	$1, %al
	jne	.L280
	testb	$2, %al
	je	.L280
	movq	%r13, %rsi
	call	_ZN2v88internal19PreparseDataBuilder9SerializeEPNS0_7IsolateE
	movq	(%r14), %rdi
	leal	1(%r15), %ecx
	movq	(%rax), %rdx
	movl	7(%rdi), %eax
	addl	$23, %eax
	andl	$-8, %eax
	leal	(%rax,%r15,8), %eax
	cltq
	leaq	-1(%rdi,%rax), %r15
	movq	%rdx, (%r15)
	testb	$1, %dl
	je	.L283
	movq	%rdx, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rax
	movq	%r8, -56(%rbp)
	testl	$262144, %eax
	jne	.L296
	testb	$24, %al
	je	.L283
.L298:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L297
	.p2align 4,,10
	.p2align 3
.L283:
	movl	%ecx, %r15d
.L280:
	addq	$8, %rbx
	cmpq	%rbx, %r12
	jne	.L282
.L287:
	addq	$40, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L296:
	.cfi_restore_state
	movq	%r15, %rsi
	movl	%ecx, -76(%rbp)
	movq	%rdx, -72(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %r8
	movl	-76(%rbp), %ecx
	movq	-72(%rbp), %rdx
	movq	-64(%rbp), %rdi
	movq	8(%r8), %rax
	testb	$24, %al
	jne	.L298
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L297:
	movq	%r15, %rsi
	movl	%ecx, -56(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movl	-56(%rbp), %ecx
	jmp	.L283
	.cfi_endproc
.LFE20972:
	.size	_ZN2v88internal19PreparseDataBuilder9SerializeEPNS0_7IsolateE, .-_ZN2v88internal19PreparseDataBuilder9SerializeEPNS0_7IsolateE
	.section	.text._ZN2v88internal27BuilderProducedPreparseData9SerializeEPNS0_7IsolateE,"axG",@progbits,_ZN2v88internal27BuilderProducedPreparseData9SerializeEPNS0_7IsolateE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal27BuilderProducedPreparseData9SerializeEPNS0_7IsolateE
	.type	_ZN2v88internal27BuilderProducedPreparseData9SerializeEPNS0_7IsolateE, @function
_ZN2v88internal27BuilderProducedPreparseData9SerializeEPNS0_7IsolateE:
.LFB20980:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	jmp	_ZN2v88internal19PreparseDataBuilder9SerializeEPNS0_7IsolateE
	.cfi_endproc
.LFE20980:
	.size	_ZN2v88internal27BuilderProducedPreparseData9SerializeEPNS0_7IsolateE, .-_ZN2v88internal27BuilderProducedPreparseData9SerializeEPNS0_7IsolateE
	.section	.text._ZN2v88internal20ProducedPreparseData3ForEPNS0_19PreparseDataBuilderEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20ProducedPreparseData3ForEPNS0_19PreparseDataBuilderEPNS0_4ZoneE
	.type	_ZN2v88internal20ProducedPreparseData3ForEPNS0_19PreparseDataBuilderEPNS0_4ZoneE, @function
_ZN2v88internal20ProducedPreparseData3ForEPNS0_19PreparseDataBuilderEPNS0_4ZoneE:
.LFB20992:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$8, %rsp
	movq	16(%rsi), %rax
	movq	24(%rsi), %rdx
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L304
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%rsi)
.L302:
	leaq	16+_ZTVN2v88internal27BuilderProducedPreparseDataE(%rip), %rcx
	movq	%rbx, 8(%rax)
	movq	%rcx, (%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L304:
	.cfi_restore_state
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L302
	.cfi_endproc
.LFE20992:
	.size	_ZN2v88internal20ProducedPreparseData3ForEPNS0_19PreparseDataBuilderEPNS0_4ZoneE, .-_ZN2v88internal20ProducedPreparseData3ForEPNS0_19PreparseDataBuilderEPNS0_4ZoneE
	.section	.text._ZN2v88internal20ProducedPreparseData3ForENS0_6HandleINS0_12PreparseDataEEEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20ProducedPreparseData3ForENS0_6HandleINS0_12PreparseDataEEEPNS0_4ZoneE
	.type	_ZN2v88internal20ProducedPreparseData3ForENS0_6HandleINS0_12PreparseDataEEEPNS0_4ZoneE, @function
_ZN2v88internal20ProducedPreparseData3ForENS0_6HandleINS0_12PreparseDataEEEPNS0_4ZoneE:
.LFB20993:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$8, %rsp
	movq	16(%rsi), %rax
	movq	24(%rsi), %rdx
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L309
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%rsi)
.L307:
	leaq	16+_ZTVN2v88internal26OnHeapProducedPreparseDataE(%rip), %rcx
	movq	%rbx, 8(%rax)
	movq	%rcx, (%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L309:
	.cfi_restore_state
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L307
	.cfi_endproc
.LFE20993:
	.size	_ZN2v88internal20ProducedPreparseData3ForENS0_6HandleINS0_12PreparseDataEEEPNS0_4ZoneE, .-_ZN2v88internal20ProducedPreparseData3ForENS0_6HandleINS0_12PreparseDataEEEPNS0_4ZoneE
	.section	.text._ZN2v88internal20ProducedPreparseData3ForEPNS0_16ZonePreparseDataEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20ProducedPreparseData3ForEPNS0_16ZonePreparseDataEPNS0_4ZoneE
	.type	_ZN2v88internal20ProducedPreparseData3ForEPNS0_16ZonePreparseDataEPNS0_4ZoneE, @function
_ZN2v88internal20ProducedPreparseData3ForEPNS0_16ZonePreparseDataEPNS0_4ZoneE:
.LFB20994:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$8, %rsp
	movq	16(%rsi), %rax
	movq	24(%rsi), %rdx
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L314
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%rsi)
.L312:
	leaq	16+_ZTVN2v88internal24ZoneProducedPreparseDataE(%rip), %rcx
	movq	%rbx, 8(%rax)
	movq	%rcx, (%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L314:
	.cfi_restore_state
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L312
	.cfi_endproc
.LFE20994:
	.size	_ZN2v88internal20ProducedPreparseData3ForEPNS0_16ZonePreparseDataEPNS0_4ZoneE, .-_ZN2v88internal20ProducedPreparseData3ForEPNS0_16ZonePreparseDataEPNS0_4ZoneE
	.section	.text._ZN2v88internal26OnHeapConsumedPreparseDataC2EPNS0_7IsolateENS0_6HandleINS0_12PreparseDataEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26OnHeapConsumedPreparseDataC2EPNS0_7IsolateENS0_6HandleINS0_12PreparseDataEEE
	.type	_ZN2v88internal26OnHeapConsumedPreparseDataC2EPNS0_7IsolateENS0_6HandleINS0_12PreparseDataEEE, @function
_ZN2v88internal26OnHeapConsumedPreparseDataC2EPNS0_7IsolateENS0_6HandleINS0_12PreparseDataEEE:
.LFB21011:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal24BaseConsumedPreparseDataINS0_12PreparseDataEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movl	$16, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movq	%r13, 24(%rbx)
	movq	$0, (%rax)
	movl	$0, 8(%rax)
	movw	%dx, 12(%rax)
	movq	%rax, 8(%rbx)
	leaq	16+_ZTVN2v88internal26OnHeapConsumedPreparseDataE(%rip), %rax
	movq	%r12, 32(%rbx)
	movl	$0, 16(%rbx)
	movq	%rax, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21011:
	.size	_ZN2v88internal26OnHeapConsumedPreparseDataC2EPNS0_7IsolateENS0_6HandleINS0_12PreparseDataEEE, .-_ZN2v88internal26OnHeapConsumedPreparseDataC2EPNS0_7IsolateENS0_6HandleINS0_12PreparseDataEEE
	.globl	_ZN2v88internal26OnHeapConsumedPreparseDataC1EPNS0_7IsolateENS0_6HandleINS0_12PreparseDataEEE
	.set	_ZN2v88internal26OnHeapConsumedPreparseDataC1EPNS0_7IsolateENS0_6HandleINS0_12PreparseDataEEE,_ZN2v88internal26OnHeapConsumedPreparseDataC2EPNS0_7IsolateENS0_6HandleINS0_12PreparseDataEEE
	.section	.rodata._ZN2v88internal16ZonePreparseDataC2EPNS0_4ZoneEPNS0_6VectorIhEEi.str1.8,"aMS",@progbits,1
	.align 8
.LC7:
	.string	"cannot create std::vector larger than max_size()"
	.section	.text._ZN2v88internal16ZonePreparseDataC2EPNS0_4ZoneEPNS0_6VectorIhEEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16ZonePreparseDataC2EPNS0_4ZoneEPNS0_6VectorIhEEi
	.type	_ZN2v88internal16ZonePreparseDataC2EPNS0_4ZoneEPNS0_6VectorIhEEi, @function
_ZN2v88internal16ZonePreparseDataC2EPNS0_4ZoneEPNS0_6VectorIhEEi:
.LFB21019:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	(%rdx), %rbx
	movq	8(%rdx), %r13
	movq	%rsi, (%rdi)
	movq	$0, 8(%rdi)
	movq	$0, 16(%rdi)
	leaq	(%rbx,%r13), %r15
	movq	$0, 24(%rdi)
	cmpq	$2147483647, %r13
	ja	.L329
	movq	%rsi, %r14
	xorl	%edx, %edx
	testq	%r13, %r13
	je	.L322
	movq	16(%r14), %rax
	movq	24(%r14), %rdx
	leaq	7(%r13), %rsi
	andq	$-8, %rsi
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L343
	addq	%rax, %rsi
	movq	%rsi, 16(%r14)
.L321:
	leaq	(%rax,%r13), %rsi
	movq	%rax, 8(%r12)
	movq	%rax, %rdx
	movq	%rsi, 24(%r12)
	cmpq	%r15, %rbx
	je	.L322
	leaq	15(%rbx), %rdx
	subq	%rax, %rdx
	cmpq	$30, %rdx
	jbe	.L323
	leaq	-1(%r13), %rdx
	cmpq	$14, %rdx
	jbe	.L323
	movq	%r13, %rdi
	movq	%rbx, %r8
	movq	%rax, %rdx
	andq	$-16, %rdi
	subq	%rax, %r8
	addq	%rax, %rdi
	.p2align 4,,10
	.p2align 3
.L325:
	movdqu	(%rdx,%r8), %xmm0
	addq	$16, %rdx
	movups	%xmm0, -16(%rdx)
	cmpq	%rdi, %rdx
	jne	.L325
	movq	%r13, %rdi
	andq	$-16, %rdi
	leaq	(%rbx,%rdi), %rdx
	addq	%rdi, %rax
	cmpq	%rdi, %r13
	je	.L327
	movzbl	(%rdx), %edi
	movb	%dil, (%rax)
	leaq	1(%rdx), %rdi
	cmpq	%rdi, %r15
	je	.L327
	movzbl	1(%rdx), %edi
	movb	%dil, 1(%rax)
	leaq	2(%rdx), %rdi
	cmpq	%rdi, %r15
	je	.L327
	movzbl	2(%rdx), %edi
	movb	%dil, 2(%rax)
	leaq	3(%rdx), %rdi
	cmpq	%rdi, %r15
	je	.L327
	movzbl	3(%rdx), %edi
	movb	%dil, 3(%rax)
	leaq	4(%rdx), %rdi
	cmpq	%rdi, %r15
	je	.L327
	movzbl	4(%rdx), %edi
	movb	%dil, 4(%rax)
	leaq	5(%rdx), %rdi
	cmpq	%rdi, %r15
	je	.L327
	movzbl	5(%rdx), %edi
	movb	%dil, 5(%rax)
	leaq	6(%rdx), %rdi
	cmpq	%rdi, %r15
	je	.L327
	movzbl	6(%rdx), %edi
	movb	%dil, 6(%rax)
	leaq	7(%rdx), %rdi
	cmpq	%rdi, %r15
	je	.L327
	movzbl	7(%rdx), %edi
	movb	%dil, 7(%rax)
	leaq	8(%rdx), %rdi
	cmpq	%rdi, %r15
	je	.L327
	movzbl	8(%rdx), %edi
	movb	%dil, 8(%rax)
	leaq	9(%rdx), %rdi
	cmpq	%rdi, %r15
	je	.L327
	movzbl	9(%rdx), %edi
	movb	%dil, 9(%rax)
	leaq	10(%rdx), %rdi
	cmpq	%rdi, %r15
	je	.L327
	movzbl	10(%rdx), %edi
	movb	%dil, 10(%rax)
	leaq	11(%rdx), %rdi
	cmpq	%rdi, %r15
	je	.L327
	movzbl	11(%rdx), %edi
	movb	%dil, 11(%rax)
	leaq	12(%rdx), %rdi
	cmpq	%rdi, %r15
	je	.L327
	movzbl	12(%rdx), %edi
	movb	%dil, 12(%rax)
	leaq	13(%rdx), %rdi
	cmpq	%rdi, %r15
	je	.L327
	movzbl	13(%rdx), %edi
	movb	%dil, 13(%rax)
	leaq	14(%rdx), %rdi
	cmpq	%rdi, %r15
	je	.L327
	movzbl	14(%rdx), %edx
	movb	%dl, 14(%rax)
	.p2align 4,,10
	.p2align 3
.L327:
	movq	%rsi, %rdx
.L322:
	movslq	%ecx, %rcx
	movq	%rdx, 16(%r12)
	cmpq	$268435455, %rcx
	ja	.L329
	movq	%r14, 32(%r12)
	xorl	%ebx, %ebx
	leaq	0(,%rcx,8), %rdx
	movq	$0, 40(%r12)
	movq	$0, 48(%r12)
	movq	$0, 56(%r12)
	testq	%rcx, %rcx
	je	.L333
	movq	16(%r14), %rdi
	movq	24(%r14), %rax
	movq	%rdx, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rdx
	ja	.L344
	addq	%rdi, %rsi
	movq	%rsi, 16(%r14)
.L332:
	leaq	(%rdi,%rdx), %rbx
	movq	%rdi, 40(%r12)
	xorl	%esi, %esi
	movq	%rbx, 56(%r12)
	call	memset@PLT
.L333:
	movq	%rbx, 48(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L344:
	.cfi_restore_state
	movq	%r14, %rdi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %rdi
	jmp	.L332
	.p2align 4,,10
	.p2align 3
.L343:
	movq	%r14, %rdi
	movl	%ecx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-56(%rbp), %ecx
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L323:
	subq	%rax, %rbx
	movq	%rbx, %rdx
	.p2align 4,,10
	.p2align 3
.L328:
	movzbl	(%rax,%rdx), %edi
	addq	$1, %rax
	movb	%dil, -1(%rax)
	cmpq	%rsi, %rax
	jne	.L328
	movq	%rsi, %rdx
	jmp	.L322
.L329:
	leaq	.LC7(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE21019:
	.size	_ZN2v88internal16ZonePreparseDataC2EPNS0_4ZoneEPNS0_6VectorIhEEi, .-_ZN2v88internal16ZonePreparseDataC2EPNS0_4ZoneEPNS0_6VectorIhEEi
	.globl	_ZN2v88internal16ZonePreparseDataC1EPNS0_4ZoneEPNS0_6VectorIhEEi
	.set	_ZN2v88internal16ZonePreparseDataC1EPNS0_4ZoneEPNS0_6VectorIhEEi,_ZN2v88internal16ZonePreparseDataC2EPNS0_4ZoneEPNS0_6VectorIhEEi
	.section	.text._ZN2v88internal19PreparseDataBuilder9SerializeEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19PreparseDataBuilder9SerializeEPNS0_4ZoneE
	.type	_ZN2v88internal19PreparseDataBuilder9SerializeEPNS0_4ZoneE, @function
_ZN2v88internal19PreparseDataBuilder9SerializeEPNS0_4ZoneE:
.LFB20973:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	16(%rsi), %r14
	movq	24(%rsi), %rax
	movl	72(%rdi), %ecx
	subq	%r14, %rax
	cmpq	$63, %rax
	jbe	.L356
	leaq	64(%r14), %rax
	movq	%rax, 16(%rsi)
.L347:
	leaq	8(%r13), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal16ZonePreparseDataC1EPNS0_4ZoneEPNS0_6VectorIhEEi
	movq	32(%r13), %rbx
	movq	40(%r13), %rax
	leaq	(%rbx,%rax,8), %r13
	cmpq	%r13, %rbx
	je	.L345
	xorl	%r15d, %r15d
.L350:
	movq	(%rbx), %rdi
	movzbl	76(%rdi), %eax
	testb	$1, %al
	jne	.L349
	testb	$2, %al
	je	.L349
	movq	%r12, %rsi
	call	_ZN2v88internal19PreparseDataBuilder9SerializeEPNS0_4ZoneE
	movq	40(%r14), %rcx
	movslq	%r15d, %rdx
	addl	$1, %r15d
	movq	%rax, (%rcx,%rdx,8)
.L349:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jne	.L350
.L345:
	addq	$24, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L356:
	.cfi_restore_state
	movl	$64, %esi
	movq	%r12, %rdi
	movl	%ecx, -52(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-52(%rbp), %ecx
	movq	%rax, %r14
	jmp	.L347
	.cfi_endproc
.LFE20973:
	.size	_ZN2v88internal19PreparseDataBuilder9SerializeEPNS0_4ZoneE, .-_ZN2v88internal19PreparseDataBuilder9SerializeEPNS0_4ZoneE
	.section	.text._ZN2v88internal27BuilderProducedPreparseData9SerializeEPNS0_4ZoneE,"axG",@progbits,_ZN2v88internal27BuilderProducedPreparseData9SerializeEPNS0_4ZoneE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal27BuilderProducedPreparseData9SerializeEPNS0_4ZoneE
	.type	_ZN2v88internal27BuilderProducedPreparseData9SerializeEPNS0_4ZoneE, @function
_ZN2v88internal27BuilderProducedPreparseData9SerializeEPNS0_4ZoneE:
.LFB20981:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	16(%rsi), %r13
	movq	24(%rsi), %rax
	movq	8(%rdi), %r14
	subq	%r13, %rax
	movl	72(%r14), %ecx
	cmpq	$63, %rax
	jbe	.L368
	leaq	64(%r13), %rax
	movq	%rax, 16(%rsi)
.L359:
	leaq	8(%r14), %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal16ZonePreparseDataC1EPNS0_4ZoneEPNS0_6VectorIhEEi
	movq	32(%r14), %rbx
	movq	40(%r14), %rax
	leaq	(%rbx,%rax,8), %r15
	cmpq	%r15, %rbx
	je	.L357
	xorl	%r14d, %r14d
.L362:
	movq	(%rbx), %rdi
	movzbl	76(%rdi), %eax
	testb	$1, %al
	jne	.L361
	testb	$2, %al
	je	.L361
	movq	%r12, %rsi
	call	_ZN2v88internal19PreparseDataBuilder9SerializeEPNS0_4ZoneE
	movq	40(%r13), %rcx
	movslq	%r14d, %rdx
	addl	$1, %r14d
	movq	%rax, (%rcx,%rdx,8)
.L361:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	jne	.L362
.L357:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L368:
	.cfi_restore_state
	movl	$64, %esi
	movq	%r12, %rdi
	movl	%ecx, -52(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-52(%rbp), %ecx
	movq	%rax, %r13
	jmp	.L359
	.cfi_endproc
.LFE20981:
	.size	_ZN2v88internal27BuilderProducedPreparseData9SerializeEPNS0_4ZoneE, .-_ZN2v88internal27BuilderProducedPreparseData9SerializeEPNS0_4ZoneE
	.section	.text._ZN2v88internal16ZonePreparseData9SerializeEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16ZonePreparseData9SerializeEPNS0_7IsolateE
	.type	_ZN2v88internal16ZonePreparseData9SerializeEPNS0_7IsolateE, @function
_ZN2v88internal16ZonePreparseData9SerializeEPNS0_7IsolateE:
.LFB21021:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	48(%rdi), %r13
	subq	40(%rdi), %r13
	movq	16(%rdi), %r14
	sarq	$3, %r13
	subq	8(%rdi), %r14
	movq	%rbx, %rdi
	movl	%r13d, %edx
	movl	%r14d, %esi
	call	_ZN2v88internal7Factory15NewPreparseDataEii@PLT
	movq	8(%r12), %rsi
	movslq	%r14d, %rdx
	movq	%rax, -56(%rbp)
	movq	(%rax), %rax
	leaq	15(%rax), %rdi
	movq	%rax, -64(%rbp)
	call	memcpy@PLT
	testl	%r13d, %r13d
	jle	.L379
	leal	-1(%r13), %eax
	xorl	%r14d, %r14d
	leaq	8(,%rax,8), %r13
.L375:
	movq	40(%r12), %rax
	movq	%rbx, %rsi
	movq	(%rax,%r14), %rdi
	call	_ZN2v88internal16ZonePreparseData9SerializeEPNS0_7IsolateE
	movq	-56(%rbp), %rcx
	movq	(%rax), %r15
	movq	(%rcx), %rdi
	movl	7(%rdi), %eax
	addl	$23, %eax
	andl	$-8, %eax
	addl	%r14d, %eax
	cltq
	leaq	-1(%rdi,%rax), %rsi
	movq	%r15, (%rsi)
	testb	$1, %r15b
	je	.L376
	movq	%r15, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rax
	movq	%r8, -64(%rbp)
	testl	$262144, %eax
	je	.L372
	movq	%r15, %rdx
	movq	%rsi, -80(%rbp)
	movq	%rdi, -72(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-64(%rbp), %r8
	movq	-80(%rbp), %rsi
	movq	-72(%rbp), %rdi
	movq	8(%r8), %rax
.L372:
	testb	$24, %al
	je	.L376
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L376
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L376:
	addq	$8, %r14
	cmpq	%r14, %r13
	jne	.L375
.L379:
	movq	-56(%rbp), %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21021:
	.size	_ZN2v88internal16ZonePreparseData9SerializeEPNS0_7IsolateE, .-_ZN2v88internal16ZonePreparseData9SerializeEPNS0_7IsolateE
	.section	.text._ZN2v88internal24ZoneProducedPreparseData9SerializeEPNS0_7IsolateE,"axG",@progbits,_ZN2v88internal24ZoneProducedPreparseData9SerializeEPNS0_7IsolateE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal24ZoneProducedPreparseData9SerializeEPNS0_7IsolateE
	.type	_ZN2v88internal24ZoneProducedPreparseData9SerializeEPNS0_7IsolateE, @function
_ZN2v88internal24ZoneProducedPreparseData9SerializeEPNS0_7IsolateE:
.LFB20990:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	jmp	_ZN2v88internal16ZonePreparseData9SerializeEPNS0_7IsolateE
	.cfi_endproc
.LFE20990:
	.size	_ZN2v88internal24ZoneProducedPreparseData9SerializeEPNS0_7IsolateE, .-_ZN2v88internal24ZoneProducedPreparseData9SerializeEPNS0_7IsolateE
	.section	.text._ZN2v88internal24ZoneConsumedPreparseDataC2EPNS0_4ZoneEPNS0_16ZonePreparseDataE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24ZoneConsumedPreparseDataC2EPNS0_4ZoneEPNS0_16ZonePreparseDataE
	.type	_ZN2v88internal24ZoneConsumedPreparseDataC2EPNS0_4ZoneEPNS0_16ZonePreparseDataE, @function
_ZN2v88internal24ZoneConsumedPreparseDataC2EPNS0_4ZoneEPNS0_16ZonePreparseDataE:
.LFB21027:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal24BaseConsumedPreparseDataINS0_17ZoneVectorWrapperEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	movl	$16, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movq	%r12, 24(%rbx)
	movq	$0, (%rax)
	movl	$0, 8(%rax)
	movw	%dx, 12(%rax)
	movq	%rax, 8(%rbx)
	leaq	16+_ZTVN2v88internal24ZoneConsumedPreparseDataE(%rip), %rax
	movq	%r12, 32(%rbx)
	movl	$0, 16(%rbx)
	movq	%rax, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21027:
	.size	_ZN2v88internal24ZoneConsumedPreparseDataC2EPNS0_4ZoneEPNS0_16ZonePreparseDataE, .-_ZN2v88internal24ZoneConsumedPreparseDataC2EPNS0_4ZoneEPNS0_16ZonePreparseDataE
	.globl	_ZN2v88internal24ZoneConsumedPreparseDataC1EPNS0_4ZoneEPNS0_16ZonePreparseDataE
	.set	_ZN2v88internal24ZoneConsumedPreparseDataC1EPNS0_4ZoneEPNS0_16ZonePreparseDataE,_ZN2v88internal24ZoneConsumedPreparseDataC2EPNS0_4ZoneEPNS0_16ZonePreparseDataE
	.section	.text._ZN2v88internal20ConsumedPreparseData3ForEPNS0_7IsolateENS0_6HandleINS0_12PreparseDataEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20ConsumedPreparseData3ForEPNS0_7IsolateENS0_6HandleINS0_12PreparseDataEEE
	.type	_ZN2v88internal20ConsumedPreparseData3ForEPNS0_7IsolateENS0_6HandleINS0_12PreparseDataEEE, @function
_ZN2v88internal20ConsumedPreparseData3ForEPNS0_7IsolateENS0_6HandleINS0_12PreparseDataEEE:
.LFB21033:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	$40, %edi
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -32
	movq	%rdx, -32(%rbp)
	movq	%rsi, -24(%rbp)
	call	_Znwm@PLT
	movl	$16, %edi
	movq	%rax, %rbx
	leaq	16+_ZTVN2v88internal24BaseConsumedPreparseDataINS0_12PreparseDataEEE(%rip), %rax
	movq	%rax, (%rbx)
	call	_Znwm@PLT
	movq	-24(%rbp), %xmm0
	xorl	%edx, %edx
	movq	%rbx, (%r12)
	movq	$0, (%rax)
	movl	$0, 8(%rax)
	movhps	-32(%rbp), %xmm0
	movw	%dx, 12(%rax)
	movq	%rax, 8(%rbx)
	leaq	16+_ZTVN2v88internal26OnHeapConsumedPreparseDataE(%rip), %rax
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$0, 16(%rbx)
	movups	%xmm0, 24(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21033:
	.size	_ZN2v88internal20ConsumedPreparseData3ForEPNS0_7IsolateENS0_6HandleINS0_12PreparseDataEEE, .-_ZN2v88internal20ConsumedPreparseData3ForEPNS0_7IsolateENS0_6HandleINS0_12PreparseDataEEE
	.section	.text._ZN2v88internal20ConsumedPreparseData3ForEPNS0_4ZoneEPNS0_16ZonePreparseDataE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20ConsumedPreparseData3ForEPNS0_4ZoneEPNS0_16ZonePreparseDataE
	.type	_ZN2v88internal20ConsumedPreparseData3ForEPNS0_4ZoneEPNS0_16ZonePreparseDataE, @function
_ZN2v88internal20ConsumedPreparseData3ForEPNS0_4ZoneEPNS0_16ZonePreparseDataE:
.LFB21036:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	testq	%rdx, %rdx
	je	.L394
	movl	$40, %edi
	movq	%rdx, %r12
	call	_Znwm@PLT
	movl	$16, %edi
	movq	%rax, %rbx
	leaq	16+_ZTVN2v88internal24BaseConsumedPreparseDataINS0_17ZoneVectorWrapperEEE(%rip), %rax
	movq	%rax, (%rbx)
	call	_Znwm@PLT
	xorl	%edx, %edx
	movq	%r12, 24(%rbx)
	movq	$0, (%rax)
	movl	$0, 8(%rax)
	movw	%dx, 12(%rax)
	movq	%rax, 8(%rbx)
	leaq	16+_ZTVN2v88internal24ZoneConsumedPreparseDataE(%rip), %rax
	movq	%rax, (%rbx)
	movq	%r13, %rax
	movq	%r12, 32(%rbx)
	movq	%rbx, 0(%r13)
	movl	$0, 16(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L394:
	.cfi_restore_state
	movq	$0, (%rdi)
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21036:
	.size	_ZN2v88internal20ConsumedPreparseData3ForEPNS0_4ZoneEPNS0_16ZonePreparseDataE, .-_ZN2v88internal20ConsumedPreparseData3ForEPNS0_4ZoneEPNS0_16ZonePreparseDataE
	.section	.rodata._ZNSt6vectorIhSaIhEE17_M_default_appendEm.str1.1,"aMS",@progbits,1
.LC8:
	.string	"vector::_M_default_append"
	.section	.text._ZNSt6vectorIhSaIhEE17_M_default_appendEm,"axG",@progbits,_ZNSt6vectorIhSaIhEE17_M_default_appendEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIhSaIhEE17_M_default_appendEm
	.type	_ZNSt6vectorIhSaIhEE17_M_default_appendEm, @function
_ZNSt6vectorIhSaIhEE17_M_default_appendEm:
.LFB24792:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L415
	movabsq	$9223372036854775807, %rdx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movq	%rdx, %rsi
	subq	$24, %rsp
	movq	8(%rdi), %rcx
	movq	16(%rdi), %rax
	movq	%rcx, %r13
	subq	%rcx, %rax
	subq	(%rdi), %r13
	subq	%r13, %rsi
	cmpq	%rbx, %rax
	jb	.L397
	movq	%rbx, %rdx
	xorl	%esi, %esi
	movq	%rcx, %rdi
	call	memset@PLT
	addq	%rax, %rbx
	movq	%rbx, 8(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L397:
	.cfi_restore_state
	cmpq	%rbx, %rsi
	jb	.L418
	cmpq	%r13, %rbx
	movq	%r13, %rax
	cmovnb	%rbx, %rax
	addq	%r13, %rax
	movq	%rax, %r15
	jc	.L407
	testq	%rax, %rax
	js	.L407
	jne	.L401
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
.L405:
	movq	%rbx, %rdx
	leaq	(%r14,%r13), %rdi
	xorl	%esi, %esi
	call	memset@PLT
	movq	(%r12), %r8
	movq	8(%r12), %rdx
	subq	%r8, %rdx
	testq	%rdx, %rdx
	jg	.L419
	testq	%r8, %r8
	jne	.L403
.L404:
	addq	%r13, %rbx
	movq	%r14, (%r12)
	addq	%rbx, %r14
	movq	%r15, 16(%r12)
	movq	%r14, 8(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L415:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L407:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	%rdx, %r15
.L401:
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%rax, %r14
	addq	%rax, %r15
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L419:
	movq	%r8, %rsi
	movq	%r14, %rdi
	movq	%r8, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %r8
.L403:
	movq	%r8, %rdi
	call	_ZdlPv@PLT
	jmp	.L404
.L418:
	leaq	.LC8(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE24792:
	.size	_ZNSt6vectorIhSaIhEE17_M_default_appendEm, .-_ZNSt6vectorIhSaIhEE17_M_default_appendEm
	.section	.text._ZN2v88internal19PreparseDataBuilder8ByteData8FinalizeEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19PreparseDataBuilder8ByteData8FinalizeEPNS0_4ZoneE
	.type	_ZN2v88internal19PreparseDataBuilder8ByteData8FinalizeEPNS0_4ZoneE, @function
_ZN2v88internal19PreparseDataBuilder8ByteData8FinalizeEPNS0_4ZoneE:
.LFB20952:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$8, %rsp
	movslq	8(%rbx), %rsi
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	addq	$7, %rsi
	andq	$-8, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L425
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L422:
	movq	(%rbx), %rax
	movslq	8(%rbx), %rdx
	movq	%rcx, %rdi
	movq	(%rax), %rsi
	call	memcpy@PLT
	movq	%rax, %rcx
	movq	(%rbx), %rax
	movq	(%rax), %rdx
	cmpq	8(%rax), %rdx
	je	.L423
	movq	%rdx, 8(%rax)
.L423:
	movslq	8(%rbx), %rax
	movq	%rcx, (%rbx)
	movq	%rax, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L425:
	.cfi_restore_state
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L422
	.cfi_endproc
.LFE20952:
	.size	_ZN2v88internal19PreparseDataBuilder8ByteData8FinalizeEPNS0_4ZoneE, .-_ZN2v88internal19PreparseDataBuilder8ByteData8FinalizeEPNS0_4ZoneE
	.section	.text._ZNSt6vectorIhSaIhEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPhS1_EEmRKh,"axG",@progbits,_ZNSt6vectorIhSaIhEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPhS1_EEmRKh,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIhSaIhEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPhS1_EEmRKh
	.type	_ZNSt6vectorIhSaIhEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPhS1_EEmRKh, @function
_ZNSt6vectorIhSaIhEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPhS1_EEmRKh:
.LFB24800:
	.cfi_startproc
	endbr64
	testq	%rdx, %rdx
	jne	.L467
	ret
	.p2align 4,,10
	.p2align 3
.L467:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %rdi
	movq	16(%rbx), %rax
	subq	%rdi, %rax
	cmpq	%rdx, %rax
	jb	.L428
	movq	%rdi, %r15
	movzbl	(%rcx), %r14d
	subq	%rsi, %r15
	cmpq	%r15, %rdx
	jnb	.L429
	movq	%rdi, %r15
	subq	%rdx, %r15
	movq	%r15, %rsi
	call	memmove@PLT
	addq	%r12, 8(%rbx)
	movq	%rax, %rdi
	subq	%r13, %r15
	jne	.L468
.L430:
	movzbl	%r14b, %esi
	movq	%r12, %rdx
.L466:
	addq	$24, %rsp
	movq	%r13, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	memset@PLT
	.p2align 4,,10
	.p2align 3
.L429:
	.cfi_restore_state
	subq	%r15, %rdx
	leaq	(%rdi,%rdx), %r12
	jne	.L469
.L431:
	movq	%r12, 8(%rbx)
	testq	%r15, %r15
	jne	.L470
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L428:
	.cfi_restore_state
	movabsq	$9223372036854775807, %rsi
	movq	(%rbx), %rax
	movq	%rsi, %rdx
	subq	%rax, %rdi
	subq	%rdi, %rdx
	cmpq	%rdx, %r12
	ja	.L471
	cmpq	%rdi, %r12
	movq	%rdi, %rdx
	cmovnb	%r12, %rdx
	xorl	%r8d, %r8d
	addq	%rdx, %rdi
	movq	%r13, %rdx
	setc	%r8b
	movq	%rdi, %r15
	subq	%rax, %rdx
	testq	%r8, %r8
	jne	.L443
	testq	%rdi, %rdi
	js	.L443
	jne	.L435
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
.L441:
	movzbl	(%rcx), %esi
	leaq	(%r14,%rdx), %rdi
	movq	%r12, %rdx
	call	memset@PLT
	movq	(%rbx), %r9
	movq	%r13, %rdx
	subq	%r9, %rdx
	addq	%rdx, %r12
	addq	%r14, %r12
	testq	%rdx, %rdx
	jne	.L472
	movq	8(%rbx), %rdx
	subq	%r13, %rdx
	leaq	(%r12,%rdx), %rax
	movq	%rax, -56(%rbp)
	jne	.L437
.L439:
	testq	%r9, %r9
	jne	.L438
.L440:
	movq	%r14, %xmm0
	movq	%r15, 16(%rbx)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L443:
	.cfi_restore_state
	movq	%rsi, %r15
.L435:
	movq	%r15, %rdi
	movq	%rcx, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rcx
	movq	%rax, %r14
	addq	%rax, %r15
	jmp	.L441
	.p2align 4,,10
	.p2align 3
.L472:
	movq	%r9, %rsi
	movq	%r14, %rdi
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	movq	8(%rbx), %rdx
	movq	-64(%rbp), %r9
	subq	%r13, %rdx
	leaq	(%r12,%rdx), %rax
	movq	%rax, -56(%rbp)
	jne	.L437
.L438:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L440
	.p2align 4,,10
	.p2align 3
.L468:
	subq	%r15, %rdi
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	memmove@PLT
	jmp	.L430
	.p2align 4,,10
	.p2align 3
.L470:
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	memmove@PLT
	addq	%r15, 8(%rbx)
	movzbl	%r14b, %esi
	movq	%r15, %rdx
	jmp	.L466
	.p2align 4,,10
	.p2align 3
.L469:
	movzbl	%r14b, %esi
	call	memset@PLT
	jmp	.L431
	.p2align 4,,10
	.p2align 3
.L437:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	memmove@PLT
	movq	(%rbx), %r9
	jmp	.L439
.L471:
	leaq	.LC6(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE24800:
	.size	_ZNSt6vectorIhSaIhEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPhS1_EEmRKh, .-_ZNSt6vectorIhSaIhEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPhS1_EEmRKh
	.section	.text._ZN2v88internal19PreparseDataBuilder16SaveDataForScopeEPNS0_5ScopeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19PreparseDataBuilder16SaveDataForScopeEPNS0_5ScopeE
	.type	_ZN2v88internal19PreparseDataBuilder16SaveDataForScopeEPNS0_5ScopeE, @function
_ZN2v88internal19PreparseDataBuilder16SaveDataForScopeEPNS0_5ScopeE:
.LFB20968:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	$1, 130(%rsi)
	jne	.L517
.L474:
	movzbl	129(%rbx), %r13d
	leaq	8(%r12), %rdi
	movl	$1, %esi
	shrb	$5, %r13b
	andl	$2, %r13d
	orl	%eax, %r13d
	call	_ZN2v88internal19PreparseDataBuilder8ByteData7ReserveEm
	movslq	16(%r12), %rax
	leal	1(%rax), %edx
	movl	%edx, 16(%r12)
	movq	8(%r12), %rdx
	movq	(%rdx), %rdx
	movb	%r13b, (%rdx,%rax)
	movb	$0, 24(%r12)
	cmpb	$2, 128(%rbx)
	je	.L518
.L476:
	movq	64(%rbx), %r14
	leaq	56(%rbx), %r13
	cmpq	%r13, %r14
	jne	.L478
	jmp	.L486
	.p2align 4,,10
	.p2align 3
.L519:
	leal	1(%rax), %edx
	movl	$6, %ecx
	movl	%edx, 16(%r12)
	movq	(%rdi), %rdx
	movb	$0, (%rdx,%rax)
	movl	16(%r12), %eax
	movb	$3, 24(%r12)
	movq	8(%r12), %rdi
.L485:
	subl	$1, %eax
	sall	%cl, %r15d
	cltq
	addq	(%rdi), %rax
	orb	%r15b, (%rax)
	movq	0(%r13), %rdx
.L482:
	leaq	24(%rdx), %r13
	cmpq	%r13, %r14
	je	.L486
.L478:
	movq	0(%r13), %rdx
	movzwl	40(%rdx), %eax
	movl	%eax, %ecx
	andl	$15, %ecx
	cmpb	$2, %cl
	jbe	.L481
	subl	$7, %ecx
	cmpb	$3, %cl
	ja	.L482
.L481:
	movq	8(%r12), %rdi
	movl	%eax, %r15d
	movslq	16(%r12), %rdx
	shrl	$14, %eax
	shrw	$10, %r15w
	andl	$1, %eax
	movq	8(%rdi), %rsi
	andl	$1, %r15d
	addl	%r15d, %r15d
	movq	%rsi, %rcx
	subq	(%rdi), %rcx
	orl	%eax, %r15d
	movq	%rdx, %rax
	cmpq	%rdx, %rcx
	jne	.L483
	leaq	-57(%rbp), %rcx
	movl	$1, %edx
	movb	$0, -57(%rbp)
	call	_ZNSt6vectorIhSaIhEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPhS1_EEmRKh
	movslq	16(%r12), %rax
	movq	8(%r12), %rdi
.L483:
	movzbl	24(%r12), %ecx
	movzbl	%r15b, %r15d
	testb	%cl, %cl
	je	.L519
	subl	$1, %ecx
	movb	%cl, 24(%r12)
	addl	%ecx, %ecx
	movzbl	%cl, %ecx
	jmp	.L485
	.p2align 4,,10
	.p2align 3
.L486:
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L473
	.p2align 4,,10
	.p2align 3
.L480:
	movq	%rbx, %rdi
	call	_ZN2v88internal5Scope24IsSkippableFunctionScopeEv@PLT
	testb	%al, %al
	jne	.L488
	cmpb	$2, 128(%rbx)
	je	.L520
	testb	$16, 129(%rbx)
	jne	.L492
	movq	64(%rbx), %rcx
	leaq	56(%rbx), %rax
	cmpq	%rcx, %rax
	je	.L492
.L493:
	movq	(%rax), %rdx
	movzbl	40(%rdx), %eax
	andl	$15, %eax
	cmpb	$2, %al
	jbe	.L490
	subl	$7, %eax
	cmpb	$3, %al
	jbe	.L490
	leaq	24(%rdx), %rax
	cmpq	%rax, %rcx
	jne	.L493
	.p2align 4,,10
	.p2align 3
.L492:
	movq	16(%rbx), %r13
	testq	%r13, %r13
	je	.L488
.L494:
	movq	%r13, %rdi
	call	_ZN2v88internal19PreparseDataBuilder14ScopeNeedsDataEPNS0_5ScopeE
	testb	%al, %al
	jne	.L490
	movq	24(%r13), %r13
	testq	%r13, %r13
	jne	.L494
	.p2align 4,,10
	.p2align 3
.L488:
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L480
.L473:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L521
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L520:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movzbl	133(%rax), %eax
	subl	$3, %eax
	cmpb	$1, %al
	jbe	.L488
	.p2align 4,,10
	.p2align 3
.L490:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19PreparseDataBuilder16SaveDataForScopeEPNS0_5ScopeE
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L480
	jmp	.L473
	.p2align 4,,10
	.p2align 3
.L517:
	movq	%rsi, %rdi
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movzbl	129(%rax), %r13d
	movl	%r13d, %eax
	shrb	$2, %al
	andl	$1, %eax
	jmp	.L474
	.p2align 4,,10
	.p2align 3
.L518:
	movq	%rbx, %rdi
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movq	184(%rax), %rsi
	testq	%rsi, %rsi
	je	.L476
	movq	%r12, %rdi
	call	_ZN2v88internal19PreparseDataBuilder19SaveDataForVariableEPNS0_8VariableE
	jmp	.L476
.L521:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20968:
	.size	_ZN2v88internal19PreparseDataBuilder16SaveDataForScopeEPNS0_5ScopeE, .-_ZN2v88internal19PreparseDataBuilder16SaveDataForScopeEPNS0_5ScopeE
	.section	.text._ZN2v88internal19PreparseDataBuilder22SaveDataForInnerScopesEPNS0_5ScopeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19PreparseDataBuilder22SaveDataForInnerScopesEPNS0_5ScopeE
	.type	_ZN2v88internal19PreparseDataBuilder22SaveDataForInnerScopesEPNS0_5ScopeE, @function
_ZN2v88internal19PreparseDataBuilder22SaveDataForInnerScopesEPNS0_5ScopeE:
.LFB20970:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	16(%rsi), %rbx
	testq	%rbx, %rbx
	je	.L522
	movq	%rdi, %r13
	.p2align 4,,10
	.p2align 3
.L532:
	movq	%rbx, %rdi
	call	_ZN2v88internal5Scope24IsSkippableFunctionScopeEv@PLT
	testb	%al, %al
	jne	.L525
	cmpb	$2, 128(%rbx)
	je	.L545
	testb	$16, 129(%rbx)
	jne	.L529
	movq	64(%rbx), %rcx
	leaq	56(%rbx), %rax
	cmpq	%rcx, %rax
	je	.L529
.L530:
	movq	(%rax), %rdx
	movzbl	40(%rdx), %eax
	andl	$15, %eax
	cmpb	$2, %al
	jbe	.L527
	subl	$7, %eax
	cmpb	$3, %al
	jbe	.L527
	leaq	24(%rdx), %rax
	cmpq	%rax, %rcx
	jne	.L530
	.p2align 4,,10
	.p2align 3
.L529:
	movq	16(%rbx), %r12
	testq	%r12, %r12
	je	.L525
.L531:
	movq	%r12, %rdi
	call	_ZN2v88internal19PreparseDataBuilder14ScopeNeedsDataEPNS0_5ScopeE
	testb	%al, %al
	jne	.L527
	movq	24(%r12), %r12
	testq	%r12, %r12
	jne	.L531
	.p2align 4,,10
	.p2align 3
.L525:
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L532
.L522:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L545:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movzbl	133(%rax), %eax
	subl	$3, %eax
	cmpb	$1, %al
	jbe	.L525
	.p2align 4,,10
	.p2align 3
.L527:
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal19PreparseDataBuilder16SaveDataForScopeEPNS0_5ScopeE
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L532
	jmp	.L522
	.cfi_endproc
.LFE20970:
	.size	_ZN2v88internal19PreparseDataBuilder22SaveDataForInnerScopesEPNS0_5ScopeE, .-_ZN2v88internal19PreparseDataBuilder22SaveDataForInnerScopesEPNS0_5ScopeE
	.section	.text._ZN2v88internal19PreparseDataBuilder23SaveScopeAllocationDataEPNS0_16DeclarationScopeEPNS0_6ParserE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19PreparseDataBuilder23SaveScopeAllocationDataEPNS0_16DeclarationScopeEPNS0_6ParserE
	.type	_ZN2v88internal19PreparseDataBuilder23SaveScopeAllocationDataEPNS0_16DeclarationScopeEPNS0_6ParserE, @function
_ZN2v88internal19PreparseDataBuilder23SaveScopeAllocationDataEPNS0_16DeclarationScopeEPNS0_6ParserE:
.LFB20967:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -56(%rbp)
	testb	$2, 76(%rdi)
	jne	.L571
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L571:
	.cfi_restore_state
	leaq	1120(%rdx), %rax
	movq	%rdx, %r13
	leaq	8(%rdi), %r15
	movq	%rdi, %r14
	movq	%rax, 8(%rdi)
	movq	40(%rdi), %rax
	movq	%r15, %rdi
	leaq	(%rax,%rax,4), %rdx
	leaq	(%rax,%rdx,4), %rsi
	call	_ZN2v88internal19PreparseDataBuilder8ByteData7ReserveEm
	movq	32(%r14), %rbx
	movq	40(%r14), %rax
	leaq	(%rbx,%rax,8), %r12
	cmpq	%rbx, %r12
	je	.L548
	.p2align 4,,10
	.p2align 3
.L550:
	movq	(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal19PreparseDataBuilder28SaveDataForSkippableFunctionEPS1_
	testb	%al, %al
	je	.L549
	addl	$1, 72(%r14)
.L549:
	addq	$8, %rbx
	cmpq	%rbx, %r12
	jne	.L550
.L548:
	testb	$1, 76(%r14)
	je	.L572
.L552:
	movq	104(%r13), %rsi
	addq	$24, %rsp
	movq	%r15, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal19PreparseDataBuilder8ByteData8FinalizeEPNS0_4ZoneE
	.p2align 4,,10
	.p2align 3
.L572:
	.cfi_restore_state
	movq	-56(%rbp), %rax
	cmpb	$2, 128(%rax)
	je	.L573
	movq	%rax, %rcx
	testb	$16, 129(%rax)
	jne	.L556
	movq	64(%rcx), %rcx
	leaq	56(%rax), %rax
	cmpq	%rcx, %rax
	je	.L556
.L557:
	movq	(%rax), %rdx
	movzbl	40(%rdx), %eax
	andl	$15, %eax
	cmpb	$2, %al
	jbe	.L554
	subl	$7, %eax
	cmpb	$3, %al
	jbe	.L554
	leaq	24(%rdx), %rax
	cmpq	%rax, %rcx
	jne	.L557
	.p2align 4,,10
	.p2align 3
.L556:
	movq	-56(%rbp), %rax
	movq	16(%rax), %rbx
	testq	%rbx, %rbx
	je	.L552
.L558:
	movq	%rbx, %rdi
	call	_ZN2v88internal19PreparseDataBuilder14ScopeNeedsDataEPNS0_5ScopeE
	testb	%al, %al
	jne	.L554
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L558
	jmp	.L552
.L573:
	movq	%rax, %rdi
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movzbl	133(%rax), %eax
	subl	$3, %eax
	cmpb	$1, %al
	jbe	.L552
	.p2align 4,,10
	.p2align 3
.L554:
	movq	-56(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal19PreparseDataBuilder16SaveDataForScopeEPNS0_5ScopeE
	jmp	.L552
	.cfi_endproc
.LFE20967:
	.size	_ZN2v88internal19PreparseDataBuilder23SaveScopeAllocationDataEPNS0_16DeclarationScopeEPNS0_6ParserE, .-_ZN2v88internal19PreparseDataBuilder23SaveScopeAllocationDataEPNS0_16DeclarationScopeEPNS0_6ParserE
	.section	.text._ZNSt6vectorIPvSaIS0_EE17_M_default_appendEm,"axG",@progbits,_ZNSt6vectorIPvSaIS0_EE17_M_default_appendEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPvSaIS0_EE17_M_default_appendEm
	.type	_ZNSt6vectorIPvSaIS0_EE17_M_default_appendEm, @function
_ZNSt6vectorIPvSaIS0_EE17_M_default_appendEm:
.LFB25769:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L593
	movabsq	$1152921504606846975, %rdx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %rcx
	movq	16(%r12), %rax
	movq	%rcx, %rsi
	subq	(%rdi), %rsi
	subq	%rcx, %rax
	movq	%rdx, %rdi
	movq	%rsi, %r13
	sarq	$3, %rax
	sarq	$3, %r13
	subq	%r13, %rdi
	cmpq	%rbx, %rax
	jb	.L576
	salq	$3, %rbx
	movq	%rcx, %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	call	memset@PLT
	movq	%rax, %rcx
	addq	%rbx, %rcx
	movq	%rcx, 8(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L593:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L576:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	cmpq	%rbx, %rdi
	jb	.L596
	cmpq	%rbx, %r13
	movq	%rbx, %r14
	movq	%rsi, -56(%rbp)
	cmovnb	%r13, %r14
	addq	%r13, %r14
	cmpq	%rdx, %r14
	cmova	%rdx, %r14
	salq	$3, %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	leaq	0(,%rbx,8), %rdx
	movq	%rax, %r15
	leaq	(%rax,%rsi), %rdi
	xorl	%esi, %esi
	call	memset@PLT
	movq	(%r12), %r8
	movq	8(%r12), %rdx
	subq	%r8, %rdx
	testq	%rdx, %rdx
	jg	.L597
	testq	%r8, %r8
	jne	.L580
.L581:
	addq	%r13, %rbx
	addq	%r15, %r14
	movq	%r15, (%r12)
	leaq	(%r15,%rbx,8), %rax
	movq	%r14, 16(%r12)
	movq	%rax, 8(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L597:
	.cfi_restore_state
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r8, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %r8
.L580:
	movq	%r8, %rdi
	call	_ZdlPv@PLT
	jmp	.L581
.L596:
	leaq	.LC8(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE25769:
	.size	_ZNSt6vectorIPvSaIS0_EE17_M_default_appendEm, .-_ZNSt6vectorIPvSaIS0_EE17_M_default_appendEm
	.section	.text._ZN2v88internal19PreparseDataBuilder16FinalizeChildrenEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19PreparseDataBuilder16FinalizeChildrenEPNS0_4ZoneE
	.type	_ZN2v88internal19PreparseDataBuilder16FinalizeChildrenEPNS0_4ZoneE, @function
_ZN2v88internal19PreparseDataBuilder16FinalizeChildrenEPNS0_4ZoneE:
.LFB20964:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$8, %rsp
	movq	48(%rbx), %rsi
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	subl	40(%rbx), %esi
	movslq	%esi, %rsi
	salq	$3, %rsi
	subq	%r13, %rax
	cmpq	%rax, %rsi
	ja	.L605
	addq	%r13, %rsi
	movq	%rsi, 16(%rdi)
.L600:
	movq	48(%rbx), %rax
	movq	40(%rbx), %rsi
	movq	32(%rbx), %rdi
	movl	%eax, %r12d
	subl	%esi, %r12d
	movq	(%rdi), %rcx
	movslq	%r12d, %r12
	cmpl	%esi, %eax
	je	.L601
	leaq	(%rcx,%rsi,8), %rsi
	leaq	0(,%r12,8), %rdx
	movq	%r13, %rdi
	call	memcpy@PLT
	movq	32(%rbx), %rdi
	movq	40(%rbx), %rsi
	movl	48(%rbx), %r12d
	movq	(%rdi), %rcx
	subl	%esi, %r12d
	movslq	%r12d, %r12
.L601:
	movq	8(%rdi), %rdx
	movq	%rdx, %rax
	subq	%rcx, %rax
	sarq	$3, %rax
	cmpq	%rsi, %rax
	jb	.L606
	jbe	.L603
	leaq	(%rcx,%rsi,8), %rax
	cmpq	%rax, %rdx
	je	.L603
	movq	%rax, 8(%rdi)
	movq	40(%rbx), %rsi
.L603:
	movq	%r12, %xmm0
	movq	%rsi, %xmm1
	movq	%r13, 32(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 40(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L606:
	.cfi_restore_state
	subq	%rax, %rsi
	call	_ZNSt6vectorIPvSaIS0_EE17_M_default_appendEm
	movq	40(%rbx), %rsi
	jmp	.L603
	.p2align 4,,10
	.p2align 3
.L605:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r13
	jmp	.L600
	.cfi_endproc
.LFE20964:
	.size	_ZN2v88internal19PreparseDataBuilder16FinalizeChildrenEPNS0_4ZoneE, .-_ZN2v88internal19PreparseDataBuilder16FinalizeChildrenEPNS0_4ZoneE
	.section	.rodata._ZNSt6vectorIPvSaIS0_EE17_M_realloc_insertIJS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_.str1.1,"aMS",@progbits,1
.LC9:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIPvSaIS0_EE17_M_realloc_insertIJS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPvSaIS0_EE17_M_realloc_insertIJS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPvSaIS0_EE17_M_realloc_insertIJS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_
	.type	_ZNSt6vectorIPvSaIS0_EE17_M_realloc_insertIJS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_, @function
_ZNSt6vectorIPvSaIS0_EE17_M_realloc_insertIJS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_:
.LFB26268:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L621
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L617
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L622
.L609:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L616:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L623
	testq	%r13, %r13
	jg	.L612
	testq	%r9, %r9
	jne	.L615
.L613:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L623:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L612
.L615:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L613
	.p2align 4,,10
	.p2align 3
.L622:
	testq	%rsi, %rsi
	jne	.L610
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L616
	.p2align 4,,10
	.p2align 3
.L612:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L613
	jmp	.L615
	.p2align 4,,10
	.p2align 3
.L617:
	movl	$8, %r14d
	jmp	.L609
.L621:
	leaq	.LC9(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L610:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L609
	.cfi_endproc
.LFE26268:
	.size	_ZNSt6vectorIPvSaIS0_EE17_M_realloc_insertIJS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_, .-_ZNSt6vectorIPvSaIS0_EE17_M_realloc_insertIJS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_
	.section	.text._ZN2v88internal19PreparseDataBuilder18DataGatheringScope5CloseEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19PreparseDataBuilder18DataGatheringScope5CloseEv
	.type	_ZN2v88internal19PreparseDataBuilder18DataGatheringScope5CloseEv, @function
_ZN2v88internal19PreparseDataBuilder18DataGatheringScope5CloseEv:
.LFB20950:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	(%rax), %r14
	movq	(%rdi), %rax
	movq	%r14, 288(%rax)
	movq	(%rdi), %rax
	movq	8(%rdi), %rbx
	movq	40(%rax), %rax
	movq	48(%rbx), %rsi
	subl	40(%rbx), %esi
	movq	1096(%rax), %rdi
	movslq	%esi, %rsi
	salq	$3, %rsi
	movq	16(%rdi), %r15
	movq	24(%rdi), %rax
	subq	%r15, %rax
	cmpq	%rax, %rsi
	ja	.L640
	addq	%r15, %rsi
	movq	%rsi, 16(%rdi)
.L626:
	movq	48(%rbx), %rax
	movq	40(%rbx), %rsi
	movq	32(%rbx), %rdi
	movl	%eax, %r12d
	subl	%esi, %r12d
	movq	(%rdi), %rcx
	movslq	%r12d, %r12
	cmpl	%esi, %eax
	je	.L627
	leaq	(%rcx,%rsi,8), %rsi
	leaq	0(,%r12,8), %rdx
	movq	%r15, %rdi
	call	memcpy@PLT
	movq	32(%rbx), %rdi
	movq	40(%rbx), %rsi
	movl	48(%rbx), %r12d
	movq	(%rdi), %rcx
	subl	%esi, %r12d
	movslq	%r12d, %r12
.L627:
	movq	8(%rdi), %rdx
	movq	%rdx, %rax
	subq	%rcx, %rax
	sarq	$3, %rax
	cmpq	%rsi, %rax
	jb	.L641
	jbe	.L629
	leaq	(%rcx,%rsi,8), %rax
	cmpq	%rax, %rdx
	je	.L629
	movq	%rax, 8(%rdi)
	movq	40(%rbx), %rsi
.L629:
	movq	%r12, %xmm0
	movq	%rsi, %xmm1
	movq	%r15, 32(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 40(%rbx)
	testq	%r14, %r14
	je	.L624
	movq	8(%r13), %rax
	movzbl	76(%rax), %edx
	testb	$1, %dl
	jne	.L631
	andl	$2, %edx
	je	.L631
.L632:
	movq	32(%r14), %rdi
	movq	%rax, -64(%rbp)
	movq	8(%rdi), %rsi
	cmpq	16(%rdi), %rsi
	je	.L633
	movq	%rax, (%rsi)
	addq	$8, 8(%rdi)
.L634:
	addq	$1, 48(%r14)
.L624:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L642
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L631:
	.cfi_restore_state
	cmpq	$0, 56(%rax)
	jne	.L632
	jmp	.L624
	.p2align 4,,10
	.p2align 3
.L641:
	subq	%rax, %rsi
	call	_ZNSt6vectorIPvSaIS0_EE17_M_default_appendEm
	movq	40(%rbx), %rsi
	jmp	.L629
	.p2align 4,,10
	.p2align 3
.L633:
	leaq	-64(%rbp), %rdx
	call	_ZNSt6vectorIPvSaIS0_EE17_M_realloc_insertIJS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_
	jmp	.L634
	.p2align 4,,10
	.p2align 3
.L640:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r15
	jmp	.L626
.L642:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20950:
	.size	_ZN2v88internal19PreparseDataBuilder18DataGatheringScope5CloseEv, .-_ZN2v88internal19PreparseDataBuilder18DataGatheringScope5CloseEv
	.section	.text._ZN2v88internal19PreparseDataBuilder8AddChildEPS1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19PreparseDataBuilder8AddChildEPS1_
	.type	_ZN2v88internal19PreparseDataBuilder8AddChildEPS1_, @function
_ZN2v88internal19PreparseDataBuilder8AddChildEPS1_:
.LFB20963:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	32(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -32(%rbp)
	movq	8(%rdi), %r8
	cmpq	16(%rdi), %r8
	je	.L644
	movq	%rsi, (%r8)
	addq	$8, 8(%rdi)
.L645:
	addq	$1, 48(%rbx)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L648
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L644:
	.cfi_restore_state
	leaq	-32(%rbp), %rdx
	movq	%r8, %rsi
	call	_ZNSt6vectorIPvSaIS0_EE17_M_realloc_insertIJS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_
	jmp	.L645
.L648:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20963:
	.size	_ZN2v88internal19PreparseDataBuilder8AddChildEPS1_, .-_ZN2v88internal19PreparseDataBuilder8AddChildEPS1_
	.section	.text._ZN2v88internal24BaseConsumedPreparseDataINS0_17ZoneVectorWrapperEE22RestoreDataForVariableEPNS0_8VariableE,"axG",@progbits,_ZN2v88internal24BaseConsumedPreparseDataINS0_17ZoneVectorWrapperEE22RestoreDataForVariableEPNS0_8VariableE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal24BaseConsumedPreparseDataINS0_17ZoneVectorWrapperEE22RestoreDataForVariableEPNS0_8VariableE
	.type	_ZN2v88internal24BaseConsumedPreparseDataINS0_17ZoneVectorWrapperEE22RestoreDataForVariableEPNS0_8VariableE, @function
_ZN2v88internal24BaseConsumedPreparseDataINS0_17ZoneVectorWrapperEE22RestoreDataForVariableEPNS0_8VariableE:
.LFB27593:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	8(%rdi), %rax
	movzbl	12(%rax), %edx
	testb	%dl, %dl
	je	.L650
	movzbl	13(%rax), %ecx
	subl	$1, %edx
.L651:
	movl	%ecx, %ebx
	movb	%dl, 12(%rax)
	leal	0(,%rcx,4), %edx
	shrb	$6, %bl
	andl	$64, %ecx
	movb	%dl, 13(%rax)
	jne	.L690
.L653:
	andl	$2, %ebx
	je	.L649
	orw	$3072, 40(%rsi)
.L649:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L650:
	.cfi_restore_state
	movslq	8(%rax), %r8
	leal	1(%r8), %edx
	movl	%edx, 8(%rax)
	movq	(%rax), %rdx
	movq	8(%rdx), %rcx
	movq	16(%rdx), %rdx
	subq	%rcx, %rdx
	cmpq	%rdx, %r8
	jnb	.L691
	movzbl	(%rcx,%r8), %ecx
	movl	$3, %edx
	jmp	.L651
	.p2align 4,,10
	.p2align 3
.L690:
	movzwl	40(%rsi), %eax
	movl	%eax, %edx
	andl	$15, %edx
	cmpb	$1, %dl
	je	.L653
	movq	16(%rsi), %r12
	testq	%r12, %r12
	je	.L654
	testb	$64, %ah
	jne	.L654
	movzwl	40(%r12), %edx
	movl	%edx, %ecx
	andl	$15, %ecx
	cmpb	$1, %cl
	je	.L654
	movq	16(%r12), %r13
	testq	%r13, %r13
	je	.L655
	testb	$64, %dh
	je	.L692
.L655:
	orb	$64, %dh
	movw	%dx, 40(%r12)
	movzwl	40(%rsi), %eax
	.p2align 4,,10
	.p2align 3
.L654:
	orb	$64, %ah
	movw	%ax, 40(%rsi)
	jmp	.L653
.L691:
	movq	%r8, %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L692:
	movzwl	40(%r13), %eax
	movl	%eax, %ecx
	andl	$15, %ecx
	cmpb	$1, %cl
	je	.L655
	movq	16(%r13), %r14
	testq	%r14, %r14
	je	.L656
	testb	$64, %ah
	je	.L693
.L656:
	orb	$64, %ah
	movw	%ax, 40(%r13)
	movzwl	40(%r12), %edx
	jmp	.L655
.L693:
	movzwl	40(%r14), %edx
	movl	%edx, %ecx
	andl	$15, %ecx
	cmpb	$1, %cl
	je	.L656
	movq	16(%r14), %rdi
	testq	%rdi, %rdi
	je	.L657
	testb	$64, %dh
	je	.L694
.L657:
	orb	$64, %dh
	movw	%dx, 40(%r14)
	movzwl	40(%r13), %eax
	jmp	.L656
.L694:
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal8Variable16SetMaybeAssignedEv
	movzwl	40(%r14), %edx
	movq	-40(%rbp), %rsi
	jmp	.L657
	.cfi_endproc
.LFE27593:
	.size	_ZN2v88internal24BaseConsumedPreparseDataINS0_17ZoneVectorWrapperEE22RestoreDataForVariableEPNS0_8VariableE, .-_ZN2v88internal24BaseConsumedPreparseDataINS0_17ZoneVectorWrapperEE22RestoreDataForVariableEPNS0_8VariableE
	.section	.rodata._ZN2v88internal24BaseConsumedPreparseDataINS0_17ZoneVectorWrapperEE19RestoreDataForScopeEPNS0_5ScopeE.str1.8,"aMS",@progbits,1
	.align 8
.LC10:
	.string	"scope_data_->HasRemainingBytes(ByteData::kUint8Size)"
	.section	.text._ZN2v88internal24BaseConsumedPreparseDataINS0_17ZoneVectorWrapperEE19RestoreDataForScopeEPNS0_5ScopeE,"axG",@progbits,_ZN2v88internal24BaseConsumedPreparseDataINS0_17ZoneVectorWrapperEE19RestoreDataForScopeEPNS0_5ScopeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal24BaseConsumedPreparseDataINS0_17ZoneVectorWrapperEE19RestoreDataForScopeEPNS0_5ScopeE
	.type	_ZN2v88internal24BaseConsumedPreparseDataINS0_17ZoneVectorWrapperEE19RestoreDataForScopeEPNS0_5ScopeE, @function
_ZN2v88internal24BaseConsumedPreparseDataINS0_17ZoneVectorWrapperEE19RestoreDataForScopeEPNS0_5ScopeE:
.LFB27575:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	testb	$1, 130(%rsi)
	je	.L699
	movq	%rsi, %rdi
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	testb	$1, 132(%rax)
	jne	.L695
.L699:
	cmpb	$2, 128(%r14)
	je	.L804
	testb	$16, 129(%r14)
	jne	.L702
	movq	64(%r14), %rcx
	leaq	56(%r14), %rax
	cmpq	%rcx, %rax
	je	.L702
.L703:
	movq	(%rax), %rdx
	movzbl	40(%rdx), %eax
	andl	$15, %eax
	cmpb	$2, %al
	jbe	.L700
	subl	$7, %eax
	cmpb	$3, %al
	ja	.L805
	.p2align 4,,10
	.p2align 3
.L700:
	movq	8(%r12), %rdx
	movq	(%rdx), %rax
	movslq	8(%rdx), %rsi
	movq	16(%rax), %rcx
	subq	8(%rax), %rcx
	cmpl	%ecx, %esi
	jl	.L806
	leaq	.LC10(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L804:
	movq	%r14, %rdi
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movzbl	133(%rax), %eax
	subl	$3, %eax
	cmpb	$1, %al
	ja	.L700
.L695:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L805:
	.cfi_restore_state
	leaq	24(%rdx), %rax
	cmpq	%rax, %rcx
	jne	.L703
	.p2align 4,,10
	.p2align 3
.L702:
	movq	16(%r14), %rbx
	testq	%rbx, %rbx
	je	.L695
.L704:
	movq	%rbx, %rdi
	call	_ZN2v88internal19PreparseDataBuilder14ScopeNeedsDataEPNS0_5ScopeE
	testb	%al, %al
	jne	.L700
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L704
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L806:
	.cfi_restore_state
	leal	1(%rsi), %ecx
	movb	$0, 12(%rdx)
	movl	%ecx, 8(%rdx)
	movq	8(%rax), %rcx
	movq	16(%rax), %rdx
	subq	%rcx, %rdx
	cmpq	%rdx, %rsi
	jnb	.L803
	movzbl	(%rcx,%rsi), %ebx
	testb	$1, %bl
	jne	.L708
.L712:
	andl	$2, %ebx
	je	.L710
	movq	8(%r14), %rax
	orb	$64, 129(%r14)
	testq	%rax, %rax
	jne	.L716
	jmp	.L710
	.p2align 4,,10
	.p2align 3
.L807:
	orl	$64, %edx
	movb	%dl, 129(%rax)
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L710
.L716:
	movzbl	129(%rax), %edx
	testb	$64, %dl
	je	.L807
.L710:
	cmpb	$2, 128(%r14)
	je	.L808
.L717:
	movq	64(%r14), %r15
	leaq	56(%r14), %r13
	cmpq	%r13, %r15
	jne	.L718
	jmp	.L732
	.p2align 4,,10
	.p2align 3
.L810:
	movzbl	13(%rax), %ecx
	subl	$1, %edx
.L723:
	movl	%ecx, %esi
	movb	%dl, 12(%rax)
	leal	0(,%rcx,4), %edx
	shrb	$6, %sil
	andl	$64, %ecx
	movb	%dl, 13(%rax)
	jne	.L809
.L725:
	andl	$2, %esi
	je	.L802
	orw	$3072, 40(%rbx)
.L802:
	movq	0(%r13), %rbx
.L721:
	leaq	24(%rbx), %r13
	cmpq	%r13, %r15
	je	.L732
.L718:
	movq	0(%r13), %rbx
	movzbl	40(%rbx), %eax
	andl	$15, %eax
	cmpb	$2, %al
	jbe	.L720
	subl	$7, %eax
	cmpb	$3, %al
	ja	.L721
.L720:
	movq	8(%r12), %rax
	movzbl	12(%rax), %edx
	testb	%dl, %dl
	jne	.L810
	movslq	8(%rax), %rsi
	leal	1(%rsi), %edx
	movl	%edx, 8(%rax)
	movq	(%rax), %rdx
	movq	8(%rdx), %rcx
	movq	16(%rdx), %rdx
	subq	%rcx, %rdx
	cmpq	%rdx, %rsi
	jnb	.L803
	movzbl	(%rcx,%rsi), %ecx
	movl	$3, %edx
	jmp	.L723
	.p2align 4,,10
	.p2align 3
.L809:
	movzwl	40(%rbx), %eax
	movl	%eax, %edx
	andl	$15, %edx
	cmpb	$1, %dl
	je	.L725
	movq	16(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L726
	testb	$64, %ah
	je	.L811
.L726:
	orb	$64, %ah
	movw	%ax, 40(%rbx)
	jmp	.L725
	.p2align 4,,10
	.p2align 3
.L732:
	movq	16(%r14), %rbx
	testq	%rbx, %rbx
	je	.L695
	.p2align 4,,10
	.p2align 3
.L719:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal24BaseConsumedPreparseDataINS0_17ZoneVectorWrapperEE19RestoreDataForScopeEPNS0_5ScopeE
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L719
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L811:
	.cfi_restore_state
	movzwl	40(%rdx), %ecx
	movl	%ecx, %edi
	andl	$15, %edi
	cmpb	$1, %dil
	je	.L726
	movq	16(%rdx), %rax
	testq	%rax, %rax
	je	.L727
	testb	$64, %ch
	je	.L812
.L727:
	orb	$64, %ch
	movw	%cx, 40(%rdx)
	movzwl	40(%rbx), %eax
	jmp	.L726
	.p2align 4,,10
	.p2align 3
.L708:
	orb	$2, 129(%r14)
	movq	%r14, %rdi
	call	_ZN2v88internal5Scope19GetDeclarationScopeEv@PLT
	movzbl	129(%rax), %ecx
	movl	%ecx, %edx
	orl	$2, %edx
	movb	%dl, 129(%rax)
	andl	$1, %edx
	jne	.L711
	movzbl	128(%rax), %edx
	cmpb	$4, %dl
	je	.L711
	cmpb	$1, %dl
	je	.L711
	orl	$6, %ecx
	movb	%cl, 129(%rax)
.L711:
	movq	8(%r14), %rax
	orb	$64, 129(%r14)
	testq	%rax, %rax
	jne	.L713
	jmp	.L712
	.p2align 4,,10
	.p2align 3
.L813:
	orl	$64, %edx
	movb	%dl, 129(%rax)
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L712
.L713:
	movzbl	129(%rax), %edx
	testb	$64, %dl
	je	.L813
	jmp	.L712
	.p2align 4,,10
	.p2align 3
.L808:
	movq	%r14, %rdi
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movq	184(%rax), %rsi
	testq	%rsi, %rsi
	je	.L717
	movq	%r12, %rdi
	call	_ZN2v88internal24BaseConsumedPreparseDataINS0_17ZoneVectorWrapperEE22RestoreDataForVariableEPNS0_8VariableE
	jmp	.L717
.L812:
	movzwl	40(%rax), %edi
	movl	%edi, %r8d
	andl	$15, %r8d
	cmpb	$1, %r8b
	je	.L727
	movq	16(%rax), %rcx
	testq	%rcx, %rcx
	je	.L728
	testw	$16384, %di
	je	.L814
.L728:
	orw	$16384, %di
	movw	%di, 40(%rax)
	movzwl	40(%rdx), %ecx
	jmp	.L727
.L814:
	movzwl	40(%rcx), %r8d
	movl	%r8d, %r9d
	andl	$15, %r9d
	cmpb	$1, %r9b
	je	.L728
	movq	16(%rcx), %r9
	testq	%r9, %r9
	je	.L729
	testw	$16384, %r8w
	je	.L815
.L729:
	orw	$16384, %r8w
	movw	%r8w, 40(%rcx)
	movzwl	40(%rax), %edi
	jmp	.L728
.L803:
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L815:
	movzwl	40(%r9), %edi
	movl	%edi, %r10d
	andl	$15, %r10d
	cmpb	$1, %r10b
	je	.L729
	movq	16(%r9), %r8
	testq	%r8, %r8
	je	.L730
	testw	$16384, %di
	je	.L816
.L730:
	orw	$16384, %di
	movw	%di, 40(%r9)
	movzwl	40(%rcx), %r8d
	jmp	.L729
.L816:
	movq	%r8, %rdi
	movq	%r9, -88(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%rax, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movb	%sil, -49(%rbp)
	call	_ZN2v88internal8Variable16SetMaybeAssignedEv
	movq	-88(%rbp), %r9
	movq	-80(%rbp), %rcx
	movq	-72(%rbp), %rax
	movq	-64(%rbp), %rdx
	movzwl	40(%r9), %edi
	movzbl	-49(%rbp), %esi
	jmp	.L730
	.cfi_endproc
.LFE27575:
	.size	_ZN2v88internal24BaseConsumedPreparseDataINS0_17ZoneVectorWrapperEE19RestoreDataForScopeEPNS0_5ScopeE, .-_ZN2v88internal24BaseConsumedPreparseDataINS0_17ZoneVectorWrapperEE19RestoreDataForScopeEPNS0_5ScopeE
	.section	.text._ZN2v88internal24BaseConsumedPreparseDataINS0_17ZoneVectorWrapperEE26RestoreScopeAllocationDataEPNS0_16DeclarationScopeE,"axG",@progbits,_ZN2v88internal24BaseConsumedPreparseDataINS0_17ZoneVectorWrapperEE26RestoreScopeAllocationDataEPNS0_16DeclarationScopeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal24BaseConsumedPreparseDataINS0_17ZoneVectorWrapperEE26RestoreScopeAllocationDataEPNS0_16DeclarationScopeE
	.type	_ZN2v88internal24BaseConsumedPreparseDataINS0_17ZoneVectorWrapperEE26RestoreScopeAllocationDataEPNS0_16DeclarationScopeE, @function
_ZN2v88internal24BaseConsumedPreparseDataINS0_17ZoneVectorWrapperEE26RestoreScopeAllocationDataEPNS0_16DeclarationScopeE:
.LFB27560:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	leaq	_ZN2v88internal24ZoneConsumedPreparseData12GetScopeDataEv(%rip), %rdx
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L818
	movq	32(%rdi), %rax
	movq	8(%rdi), %rdx
	movq	%rax, (%rdx)
	jmp	_ZN2v88internal24BaseConsumedPreparseDataINS0_17ZoneVectorWrapperEE19RestoreDataForScopeEPNS0_5ScopeE
	.p2align 4,,10
	.p2align 3
.L818:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rsi, -16(%rbp)
	movq	%rdi, -8(%rbp)
	call	*%rax
	movq	-8(%rbp), %rdi
	movq	-16(%rbp), %rsi
	movq	8(%rdi), %rdx
	movq	%rax, (%rdx)
	leave
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal24BaseConsumedPreparseDataINS0_17ZoneVectorWrapperEE19RestoreDataForScopeEPNS0_5ScopeE
	.cfi_endproc
.LFE27560:
	.size	_ZN2v88internal24BaseConsumedPreparseDataINS0_17ZoneVectorWrapperEE26RestoreScopeAllocationDataEPNS0_16DeclarationScopeE, .-_ZN2v88internal24BaseConsumedPreparseDataINS0_17ZoneVectorWrapperEE26RestoreScopeAllocationDataEPNS0_16DeclarationScopeE
	.section	.text._ZN2v88internal24BaseConsumedPreparseDataINS0_12PreparseDataEE22RestoreDataForVariableEPNS0_8VariableE,"axG",@progbits,_ZN2v88internal24BaseConsumedPreparseDataINS0_12PreparseDataEE22RestoreDataForVariableEPNS0_8VariableE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal24BaseConsumedPreparseDataINS0_12PreparseDataEE22RestoreDataForVariableEPNS0_8VariableE
	.type	_ZN2v88internal24BaseConsumedPreparseDataINS0_12PreparseDataEE22RestoreDataForVariableEPNS0_8VariableE, @function
_ZN2v88internal24BaseConsumedPreparseDataINS0_12PreparseDataEE22RestoreDataForVariableEPNS0_8VariableE:
.LFB27601:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	8(%rdi), %rax
	movzbl	12(%rax), %edx
	testb	%dl, %dl
	je	.L824
	movzbl	13(%rax), %ecx
	subl	$1, %edx
.L825:
	movl	%ecx, %ebx
	movb	%dl, 12(%rax)
	leal	0(,%rcx,4), %edx
	shrb	$6, %bl
	andl	$64, %ecx
	movb	%dl, 13(%rax)
	jne	.L863
.L826:
	andl	$2, %ebx
	je	.L823
	orw	$3072, 40(%rsi)
.L823:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L824:
	.cfi_restore_state
	movl	8(%rax), %edx
	leal	1(%rdx), %ecx
	addl	$16, %edx
	movl	%ecx, 8(%rax)
	movq	(%rax), %rcx
	movslq	%edx, %rdx
	movzbl	-1(%rdx,%rcx), %ecx
	movl	$3, %edx
	jmp	.L825
	.p2align 4,,10
	.p2align 3
.L863:
	movzwl	40(%rsi), %eax
	movl	%eax, %edx
	andl	$15, %edx
	cmpb	$1, %dl
	je	.L826
	movq	16(%rsi), %r12
	testq	%r12, %r12
	je	.L827
	testb	$64, %ah
	jne	.L827
	movzwl	40(%r12), %edx
	movl	%edx, %ecx
	andl	$15, %ecx
	cmpb	$1, %cl
	je	.L827
	movq	16(%r12), %r13
	testq	%r13, %r13
	je	.L828
	testb	$64, %dh
	je	.L864
.L828:
	orb	$64, %dh
	movw	%dx, 40(%r12)
	movzwl	40(%rsi), %eax
	.p2align 4,,10
	.p2align 3
.L827:
	orb	$64, %ah
	movw	%ax, 40(%rsi)
	jmp	.L826
	.p2align 4,,10
	.p2align 3
.L864:
	movzwl	40(%r13), %eax
	movl	%eax, %ecx
	andl	$15, %ecx
	cmpb	$1, %cl
	je	.L828
	movq	16(%r13), %r14
	testq	%r14, %r14
	je	.L829
	testb	$64, %ah
	je	.L865
.L829:
	orb	$64, %ah
	movw	%ax, 40(%r13)
	movzwl	40(%r12), %edx
	jmp	.L828
.L865:
	movzwl	40(%r14), %edx
	movl	%edx, %ecx
	andl	$15, %ecx
	cmpb	$1, %cl
	je	.L829
	movq	16(%r14), %rdi
	testq	%rdi, %rdi
	je	.L830
	testb	$64, %dh
	je	.L866
.L830:
	orb	$64, %dh
	movw	%dx, 40(%r14)
	movzwl	40(%r13), %eax
	jmp	.L829
.L866:
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal8Variable16SetMaybeAssignedEv
	movzwl	40(%r14), %edx
	movq	-40(%rbp), %rsi
	jmp	.L830
	.cfi_endproc
.LFE27601:
	.size	_ZN2v88internal24BaseConsumedPreparseDataINS0_12PreparseDataEE22RestoreDataForVariableEPNS0_8VariableE, .-_ZN2v88internal24BaseConsumedPreparseDataINS0_12PreparseDataEE22RestoreDataForVariableEPNS0_8VariableE
	.section	.text._ZN2v88internal24BaseConsumedPreparseDataINS0_12PreparseDataEE19RestoreDataForScopeEPNS0_5ScopeE,"axG",@progbits,_ZN2v88internal24BaseConsumedPreparseDataINS0_12PreparseDataEE19RestoreDataForScopeEPNS0_5ScopeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal24BaseConsumedPreparseDataINS0_12PreparseDataEE19RestoreDataForScopeEPNS0_5ScopeE
	.type	_ZN2v88internal24BaseConsumedPreparseDataINS0_12PreparseDataEE19RestoreDataForScopeEPNS0_5ScopeE, @function
_ZN2v88internal24BaseConsumedPreparseDataINS0_12PreparseDataEE19RestoreDataForScopeEPNS0_5ScopeE:
.LFB27586:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	testb	$1, 130(%rsi)
	je	.L871
	movq	%rsi, %rdi
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	testb	$1, 132(%rax)
	jne	.L867
.L871:
	cmpb	$2, 128(%r14)
	je	.L971
	testb	$16, 129(%r14)
	jne	.L874
	movq	64(%r14), %rcx
	leaq	56(%r14), %rax
	cmpq	%rcx, %rax
	je	.L874
.L875:
	movq	(%rax), %rdx
	movzbl	40(%rdx), %eax
	andl	$15, %eax
	cmpb	$2, %al
	jbe	.L872
	subl	$7, %eax
	cmpb	$3, %al
	ja	.L972
	.p2align 4,,10
	.p2align 3
.L872:
	movq	8(%r12), %rdx
	movq	(%rdx), %rcx
	movl	8(%rdx), %eax
	cmpl	%eax, 7(%rcx)
	jg	.L973
	leaq	.LC10(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L971:
	movq	%r14, %rdi
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movzbl	133(%rax), %eax
	subl	$3, %eax
	cmpb	$1, %al
	ja	.L872
.L867:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L972:
	.cfi_restore_state
	leaq	24(%rdx), %rax
	cmpq	%rax, %rcx
	jne	.L875
	.p2align 4,,10
	.p2align 3
.L874:
	movq	16(%r14), %rbx
	testq	%rbx, %rbx
	je	.L867
.L876:
	movq	%rbx, %rdi
	call	_ZN2v88internal19PreparseDataBuilder14ScopeNeedsDataEPNS0_5ScopeE
	testb	%al, %al
	jne	.L872
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L876
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L973:
	.cfi_restore_state
	leal	1(%rax), %esi
	addl	$16, %eax
	movb	$0, 12(%rdx)
	cltq
	movl	%esi, 8(%rdx)
	movzbl	-1(%rcx,%rax), %ebx
	testb	$1, %bl
	jne	.L878
.L882:
	andl	$2, %ebx
	je	.L880
	movq	8(%r14), %rax
	orb	$64, 129(%r14)
	testq	%rax, %rax
	jne	.L886
	jmp	.L880
	.p2align 4,,10
	.p2align 3
.L974:
	orl	$64, %edx
	movb	%dl, 129(%rax)
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L880
.L886:
	movzbl	129(%rax), %edx
	testb	$64, %dl
	je	.L974
.L880:
	cmpb	$2, 128(%r14)
	je	.L975
.L887:
	movq	64(%r14), %r15
	leaq	56(%r14), %r13
	cmpq	%r13, %r15
	jne	.L888
	jmp	.L901
	.p2align 4,,10
	.p2align 3
.L977:
	movzbl	13(%rax), %ecx
	subl	$1, %edx
.L893:
	movl	%ecx, %esi
	movb	%dl, 12(%rax)
	leal	0(,%rcx,4), %edx
	shrb	$6, %sil
	andl	$64, %ecx
	movb	%dl, 13(%rax)
	jne	.L976
.L894:
	andl	$2, %esi
	je	.L970
	orw	$3072, 40(%rbx)
.L970:
	movq	0(%r13), %rbx
.L891:
	leaq	24(%rbx), %r13
	cmpq	%r13, %r15
	je	.L901
.L888:
	movq	0(%r13), %rbx
	movzbl	40(%rbx), %eax
	andl	$15, %eax
	cmpb	$2, %al
	jbe	.L890
	subl	$7, %eax
	cmpb	$3, %al
	ja	.L891
.L890:
	movq	8(%r12), %rax
	movzbl	12(%rax), %edx
	testb	%dl, %dl
	jne	.L977
	movl	8(%rax), %edx
	leal	1(%rdx), %ecx
	addl	$16, %edx
	movl	%ecx, 8(%rax)
	movq	(%rax), %rcx
	movslq	%edx, %rdx
	movzbl	-1(%rdx,%rcx), %ecx
	movl	$3, %edx
	jmp	.L893
	.p2align 4,,10
	.p2align 3
.L976:
	movzwl	40(%rbx), %eax
	movl	%eax, %edx
	andl	$15, %edx
	cmpb	$1, %dl
	je	.L894
	movq	16(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L895
	testb	$64, %ah
	je	.L978
.L895:
	orb	$64, %ah
	movw	%ax, 40(%rbx)
	jmp	.L894
	.p2align 4,,10
	.p2align 3
.L901:
	movq	16(%r14), %rbx
	testq	%rbx, %rbx
	je	.L867
	.p2align 4,,10
	.p2align 3
.L889:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal24BaseConsumedPreparseDataINS0_12PreparseDataEE19RestoreDataForScopeEPNS0_5ScopeE
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L889
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L978:
	.cfi_restore_state
	movzwl	40(%rdx), %ecx
	movl	%ecx, %edi
	andl	$15, %edi
	cmpb	$1, %dil
	je	.L895
	movq	16(%rdx), %rax
	testq	%rax, %rax
	je	.L896
	testb	$64, %ch
	je	.L979
.L896:
	orb	$64, %ch
	movw	%cx, 40(%rdx)
	movzwl	40(%rbx), %eax
	jmp	.L895
	.p2align 4,,10
	.p2align 3
.L878:
	orb	$2, 129(%r14)
	movq	%r14, %rdi
	call	_ZN2v88internal5Scope19GetDeclarationScopeEv@PLT
	movzbl	129(%rax), %ecx
	movl	%ecx, %edx
	orl	$2, %edx
	movb	%dl, 129(%rax)
	andl	$1, %edx
	jne	.L881
	movzbl	128(%rax), %edx
	cmpb	$4, %dl
	je	.L881
	cmpb	$1, %dl
	je	.L881
	orl	$6, %ecx
	movb	%cl, 129(%rax)
.L881:
	movq	8(%r14), %rax
	orb	$64, 129(%r14)
	testq	%rax, %rax
	jne	.L883
	jmp	.L882
	.p2align 4,,10
	.p2align 3
.L980:
	orl	$64, %edx
	movb	%dl, 129(%rax)
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L882
.L883:
	movzbl	129(%rax), %edx
	testb	$64, %dl
	je	.L980
	jmp	.L882
	.p2align 4,,10
	.p2align 3
.L975:
	movq	%r14, %rdi
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movq	184(%rax), %rsi
	testq	%rsi, %rsi
	je	.L887
	movq	%r12, %rdi
	call	_ZN2v88internal24BaseConsumedPreparseDataINS0_12PreparseDataEE22RestoreDataForVariableEPNS0_8VariableE
	jmp	.L887
.L979:
	movzwl	40(%rax), %edi
	movl	%edi, %r8d
	andl	$15, %r8d
	cmpb	$1, %r8b
	je	.L896
	movq	16(%rax), %rcx
	testq	%rcx, %rcx
	je	.L897
	testw	$16384, %di
	je	.L981
.L897:
	orw	$16384, %di
	movw	%di, 40(%rax)
	movzwl	40(%rdx), %ecx
	jmp	.L896
.L981:
	movzwl	40(%rcx), %r8d
	movl	%r8d, %r9d
	andl	$15, %r9d
	cmpb	$1, %r9b
	je	.L897
	movq	16(%rcx), %r9
	testq	%r9, %r9
	je	.L898
	testw	$16384, %r8w
	je	.L982
.L898:
	orw	$16384, %r8w
	movw	%r8w, 40(%rcx)
	movzwl	40(%rax), %edi
	jmp	.L897
.L982:
	movzwl	40(%r9), %edi
	movl	%edi, %r10d
	andl	$15, %r10d
	cmpb	$1, %r10b
	je	.L898
	movq	16(%r9), %r8
	testq	%r8, %r8
	je	.L899
	testw	$16384, %di
	je	.L983
.L899:
	orw	$16384, %di
	movw	%di, 40(%r9)
	movzwl	40(%rcx), %r8d
	jmp	.L898
.L983:
	movq	%r8, %rdi
	movq	%r9, -88(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%rax, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movb	%sil, -49(%rbp)
	call	_ZN2v88internal8Variable16SetMaybeAssignedEv
	movq	-88(%rbp), %r9
	movq	-80(%rbp), %rcx
	movq	-72(%rbp), %rax
	movq	-64(%rbp), %rdx
	movzwl	40(%r9), %edi
	movzbl	-49(%rbp), %esi
	jmp	.L899
	.cfi_endproc
.LFE27586:
	.size	_ZN2v88internal24BaseConsumedPreparseDataINS0_12PreparseDataEE19RestoreDataForScopeEPNS0_5ScopeE, .-_ZN2v88internal24BaseConsumedPreparseDataINS0_12PreparseDataEE19RestoreDataForScopeEPNS0_5ScopeE
	.section	.text._ZN2v88internal24BaseConsumedPreparseDataINS0_12PreparseDataEE26RestoreScopeAllocationDataEPNS0_16DeclarationScopeE,"axG",@progbits,_ZN2v88internal24BaseConsumedPreparseDataINS0_12PreparseDataEE26RestoreScopeAllocationDataEPNS0_16DeclarationScopeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal24BaseConsumedPreparseDataINS0_12PreparseDataEE26RestoreScopeAllocationDataEPNS0_16DeclarationScopeE
	.type	_ZN2v88internal24BaseConsumedPreparseDataINS0_12PreparseDataEE26RestoreScopeAllocationDataEPNS0_16DeclarationScopeE, @function
_ZN2v88internal24BaseConsumedPreparseDataINS0_12PreparseDataEE26RestoreScopeAllocationDataEPNS0_16DeclarationScopeE:
.LFB27562:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	leaq	_ZN2v88internal26OnHeapConsumedPreparseData12GetScopeDataEv(%rip), %rdx
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L985
	movq	32(%rdi), %rax
	movq	8(%rdi), %rdx
	movq	(%rax), %rax
	movq	%rax, (%rdx)
	jmp	_ZN2v88internal24BaseConsumedPreparseDataINS0_12PreparseDataEE19RestoreDataForScopeEPNS0_5ScopeE
	.p2align 4,,10
	.p2align 3
.L985:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rsi, -16(%rbp)
	movq	%rdi, -8(%rbp)
	call	*%rax
	movq	-8(%rbp), %rdi
	movq	-16(%rbp), %rsi
	movq	8(%rdi), %rdx
	movq	%rax, (%rdx)
	leave
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal24BaseConsumedPreparseDataINS0_12PreparseDataEE19RestoreDataForScopeEPNS0_5ScopeE
	.cfi_endproc
.LFE27562:
	.size	_ZN2v88internal24BaseConsumedPreparseDataINS0_12PreparseDataEE26RestoreScopeAllocationDataEPNS0_16DeclarationScopeE, .-_ZN2v88internal24BaseConsumedPreparseDataINS0_12PreparseDataEE26RestoreScopeAllocationDataEPNS0_16DeclarationScopeE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal19PreparseDataBuilderC2EPNS0_4ZoneEPS1_PSt6vectorIPvSaIS6_EE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal19PreparseDataBuilderC2EPNS0_4ZoneEPS1_PSt6vectorIPvSaIS6_EE, @function
_GLOBAL__sub_I__ZN2v88internal19PreparseDataBuilderC2EPNS0_4ZoneEPS1_PSt6vectorIPvSaIS6_EE:
.LFB27613:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE27613:
	.size	_GLOBAL__sub_I__ZN2v88internal19PreparseDataBuilderC2EPNS0_4ZoneEPS1_PSt6vectorIPvSaIS6_EE, .-_GLOBAL__sub_I__ZN2v88internal19PreparseDataBuilderC2EPNS0_4ZoneEPS1_PSt6vectorIPvSaIS6_EE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal19PreparseDataBuilderC2EPNS0_4ZoneEPS1_PSt6vectorIPvSaIS6_EE
	.weak	_ZTVN2v88internal24BaseConsumedPreparseDataINS0_12PreparseDataEEE
	.section	.data.rel.ro._ZTVN2v88internal24BaseConsumedPreparseDataINS0_12PreparseDataEEE,"awG",@progbits,_ZTVN2v88internal24BaseConsumedPreparseDataINS0_12PreparseDataEEE,comdat
	.align 8
	.type	_ZTVN2v88internal24BaseConsumedPreparseDataINS0_12PreparseDataEEE, @object
	.size	_ZTVN2v88internal24BaseConsumedPreparseDataINS0_12PreparseDataEEE, 64
_ZTVN2v88internal24BaseConsumedPreparseDataINS0_12PreparseDataEEE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZN2v88internal24BaseConsumedPreparseDataINS0_12PreparseDataEE27GetDataForSkippableFunctionEPNS0_4ZoneEiPiS6_S6_S6_PbPNS0_12LanguageModeE
	.quad	_ZN2v88internal24BaseConsumedPreparseDataINS0_12PreparseDataEE26RestoreScopeAllocationDataEPNS0_16DeclarationScopeE
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.weak	_ZTVN2v88internal24BaseConsumedPreparseDataINS0_17ZoneVectorWrapperEEE
	.section	.data.rel.ro._ZTVN2v88internal24BaseConsumedPreparseDataINS0_17ZoneVectorWrapperEEE,"awG",@progbits,_ZTVN2v88internal24BaseConsumedPreparseDataINS0_17ZoneVectorWrapperEEE,comdat
	.align 8
	.type	_ZTVN2v88internal24BaseConsumedPreparseDataINS0_17ZoneVectorWrapperEEE, @object
	.size	_ZTVN2v88internal24BaseConsumedPreparseDataINS0_17ZoneVectorWrapperEEE, 64
_ZTVN2v88internal24BaseConsumedPreparseDataINS0_17ZoneVectorWrapperEEE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZN2v88internal24BaseConsumedPreparseDataINS0_17ZoneVectorWrapperEE27GetDataForSkippableFunctionEPNS0_4ZoneEiPiS6_S6_S6_PbPNS0_12LanguageModeE
	.quad	_ZN2v88internal24BaseConsumedPreparseDataINS0_17ZoneVectorWrapperEE26RestoreScopeAllocationDataEPNS0_16DeclarationScopeE
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.weak	_ZTVN2v88internal27BuilderProducedPreparseDataE
	.section	.data.rel.ro.local._ZTVN2v88internal27BuilderProducedPreparseDataE,"awG",@progbits,_ZTVN2v88internal27BuilderProducedPreparseDataE,comdat
	.align 8
	.type	_ZTVN2v88internal27BuilderProducedPreparseDataE, @object
	.size	_ZTVN2v88internal27BuilderProducedPreparseDataE, 32
_ZTVN2v88internal27BuilderProducedPreparseDataE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal27BuilderProducedPreparseData9SerializeEPNS0_7IsolateE
	.quad	_ZN2v88internal27BuilderProducedPreparseData9SerializeEPNS0_4ZoneE
	.weak	_ZTVN2v88internal26OnHeapProducedPreparseDataE
	.section	.data.rel.ro.local._ZTVN2v88internal26OnHeapProducedPreparseDataE,"awG",@progbits,_ZTVN2v88internal26OnHeapProducedPreparseDataE,comdat
	.align 8
	.type	_ZTVN2v88internal26OnHeapProducedPreparseDataE, @object
	.size	_ZTVN2v88internal26OnHeapProducedPreparseDataE, 32
_ZTVN2v88internal26OnHeapProducedPreparseDataE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal26OnHeapProducedPreparseData9SerializeEPNS0_7IsolateE
	.quad	_ZN2v88internal26OnHeapProducedPreparseData9SerializeEPNS0_4ZoneE
	.weak	_ZTVN2v88internal24ZoneProducedPreparseDataE
	.section	.data.rel.ro.local._ZTVN2v88internal24ZoneProducedPreparseDataE,"awG",@progbits,_ZTVN2v88internal24ZoneProducedPreparseDataE,comdat
	.align 8
	.type	_ZTVN2v88internal24ZoneProducedPreparseDataE, @object
	.size	_ZTVN2v88internal24ZoneProducedPreparseDataE, 32
_ZTVN2v88internal24ZoneProducedPreparseDataE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal24ZoneProducedPreparseData9SerializeEPNS0_7IsolateE
	.quad	_ZN2v88internal24ZoneProducedPreparseData9SerializeEPNS0_4ZoneE
	.weak	_ZTVN2v88internal26OnHeapConsumedPreparseDataE
	.section	.data.rel.ro.local._ZTVN2v88internal26OnHeapConsumedPreparseDataE,"awG",@progbits,_ZTVN2v88internal26OnHeapConsumedPreparseDataE,comdat
	.align 8
	.type	_ZTVN2v88internal26OnHeapConsumedPreparseDataE, @object
	.size	_ZTVN2v88internal26OnHeapConsumedPreparseDataE, 64
_ZTVN2v88internal26OnHeapConsumedPreparseDataE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal26OnHeapConsumedPreparseDataD1Ev
	.quad	_ZN2v88internal26OnHeapConsumedPreparseDataD0Ev
	.quad	_ZN2v88internal24BaseConsumedPreparseDataINS0_12PreparseDataEE27GetDataForSkippableFunctionEPNS0_4ZoneEiPiS6_S6_S6_PbPNS0_12LanguageModeE
	.quad	_ZN2v88internal24BaseConsumedPreparseDataINS0_12PreparseDataEE26RestoreScopeAllocationDataEPNS0_16DeclarationScopeE
	.quad	_ZN2v88internal26OnHeapConsumedPreparseData12GetScopeDataEv
	.quad	_ZN2v88internal26OnHeapConsumedPreparseData12GetChildDataEPNS0_4ZoneEi
	.weak	_ZTVN2v88internal24ZoneConsumedPreparseDataE
	.section	.data.rel.ro.local._ZTVN2v88internal24ZoneConsumedPreparseDataE,"awG",@progbits,_ZTVN2v88internal24ZoneConsumedPreparseDataE,comdat
	.align 8
	.type	_ZTVN2v88internal24ZoneConsumedPreparseDataE, @object
	.size	_ZTVN2v88internal24ZoneConsumedPreparseDataE, 64
_ZTVN2v88internal24ZoneConsumedPreparseDataE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal24ZoneConsumedPreparseDataD1Ev
	.quad	_ZN2v88internal24ZoneConsumedPreparseDataD0Ev
	.quad	_ZN2v88internal24BaseConsumedPreparseDataINS0_17ZoneVectorWrapperEE27GetDataForSkippableFunctionEPNS0_4ZoneEiPiS6_S6_S6_PbPNS0_12LanguageModeE
	.quad	_ZN2v88internal24BaseConsumedPreparseDataINS0_17ZoneVectorWrapperEE26RestoreScopeAllocationDataEPNS0_16DeclarationScopeE
	.quad	_ZN2v88internal24ZoneConsumedPreparseData12GetScopeDataEv
	.quad	_ZN2v88internal24ZoneConsumedPreparseData12GetChildDataEPNS0_4ZoneEi
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
