	.file	"wasm-module-builder.cc"
	.text
	.section	.text._ZN2v88internal4wasm10ZoneBuffer11EnsureSpaceEm.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm10ZoneBuffer11EnsureSpaceEm.constprop.0, @function
_ZN2v88internal4wasm10ZoneBuffer11EnsureSpaceEm.constprop.0:
.LFB23307:
	.cfi_startproc
	movq	16(%rdi), %rax
	movq	24(%rdi), %rsi
	addq	$1, %rax
	cmpq	%rax, %rsi
	jb	.L10
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	8(%rdi), %rsi
	movq	(%rdi), %rdi
	addq	%rsi, %rsi
	leaq	1(%rsi), %r12
	addq	$8, %rsi
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	andq	$-8, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L11
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L4:
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rdx
	movq	%rcx, %rdi
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	%rax, %rcx
	movq	16(%rbx), %rax
	subq	8(%rbx), %rax
	addq	%rcx, %r12
	addq	%rcx, %rax
	movq	%rcx, 8(%rbx)
	movq	%rax, 16(%rbx)
	movq	%r12, 24(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L4
	.cfi_endproc
.LFE23307:
	.size	_ZN2v88internal4wasm10ZoneBuffer11EnsureSpaceEm.constprop.0, .-_ZN2v88internal4wasm10ZoneBuffer11EnsureSpaceEm.constprop.0
	.section	.text._ZN2v88internal4wasm12_GLOBAL__N_111EmitSectionENS1_11SectionCodeEPNS1_10ZoneBufferE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal4wasm12_GLOBAL__N_111EmitSectionENS1_11SectionCodeEPNS1_10ZoneBufferE, @function
_ZN2v88internal4wasm12_GLOBAL__N_111EmitSectionENS1_11SectionCodeEPNS1_10ZoneBufferE:
.LFB17965:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edi, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	16(%rsi), %rdx
	movq	24(%rsi), %rsi
	leaq	1(%rdx), %rax
	cmpq	%rsi, %rax
	ja	.L20
.L13:
	movq	%rax, 16(%rbx)
	movb	%r13b, (%rdx)
	movq	16(%rbx), %rax
	movq	8(%rbx), %rdx
	movq	24(%rbx), %rsi
	movq	%rax, %r13
	addq	$5, %rax
	subq	%rdx, %r13
	cmpq	%rsi, %rax
	ja	.L21
.L16:
	movq	%rax, 16(%rbx)
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	movq	(%rbx), %rdi
	subq	8(%rbx), %rsi
	addq	%rsi, %rsi
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	leaq	1(%rsi), %r12
	addq	$8, %rsi
	andq	$-8, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L22
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L15:
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rdx
	movq	%rcx, %rdi
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	16(%rbx), %rdx
	subq	8(%rbx), %rdx
	addq	%rax, %r12
	addq	%rax, %rdx
	movq	%rax, 8(%rbx)
	movq	%r12, 24(%rbx)
	leaq	1(%rdx), %rax
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L21:
	movq	(%rbx), %rdi
	subq	%rdx, %rsi
	addq	%rsi, %rsi
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	leaq	5(%rsi), %r12
	addq	$12, %rsi
	andq	$-8, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L23
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L18:
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rdx
	movq	%rcx, %rdi
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	%rax, %rcx
	movq	16(%rbx), %rax
	subq	8(%rbx), %rax
	addq	%rcx, %r12
	movq	%rcx, 8(%rbx)
	leaq	5(%rax,%rcx), %rax
	movq	%r12, 24(%rbx)
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L22:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L23:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L18
	.cfi_endproc
.LFE17965:
	.size	_ZN2v88internal4wasm12_GLOBAL__N_111EmitSectionENS1_11SectionCodeEPNS1_10ZoneBufferE, .-_ZN2v88internal4wasm12_GLOBAL__N_111EmitSectionENS1_11SectionCodeEPNS1_10ZoneBufferE
	.section	.text._ZN2v88internal4wasm10ZoneBuffer10write_u32vEj,"axG",@progbits,_ZN2v88internal4wasm10ZoneBuffer10write_u32vEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm10ZoneBuffer10write_u32vEj
	.type	_ZN2v88internal4wasm10ZoneBuffer10write_u32vEj, @function
_ZN2v88internal4wasm10ZoneBuffer10write_u32vEj:
.LFB17938:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%esi, %ebx
	subq	$8, %rsp
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	leaq	5(%rax), %rcx
	cmpq	%rcx, %rdx
	jnb	.L35
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L34:
	movl	%ebx, %edx
	shrl	$7, %ebx
	orl	$-128, %edx
	movb	%dl, (%rax)
	movq	16(%r12), %rax
.L35:
	leaq	1(%rax), %rdx
	movq	%rdx, 16(%r12)
	cmpl	$127, %ebx
	ja	.L34
	movb	%bl, (%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	.cfi_restore_state
	subq	8(%rdi), %rdx
	movq	(%rdi), %rdi
	addq	%rdx, %rdx
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	leaq	5(%rdx), %r13
	addq	$12, %rdx
	andq	$-8, %rdx
	subq	%rcx, %rax
	cmpq	%rax, %rdx
	ja	.L37
	addq	%rcx, %rdx
	movq	%rdx, 16(%rdi)
.L27:
	movq	8(%r12), %rsi
	movq	16(%r12), %rdx
	movq	%rcx, %rdi
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	%rax, %rcx
	movq	16(%r12), %rax
	subq	8(%r12), %rax
	addq	%rcx, %r13
	movq	%rcx, 8(%r12)
	addq	%rcx, %rax
	movq	%r13, 24(%r12)
	jmp	.L35
.L37:
	movq	%rdx, %rsi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L27
	.cfi_endproc
.LFE17938:
	.size	_ZN2v88internal4wasm10ZoneBuffer10write_u32vEj, .-_ZN2v88internal4wasm10ZoneBuffer10write_u32vEj
	.section	.text._ZN2v88internal4wasm10ZoneBuffer10write_sizeEm,"axG",@progbits,_ZN2v88internal4wasm10ZoneBuffer10write_sizeEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm10ZoneBuffer10write_sizeEm
	.type	_ZN2v88internal4wasm10ZoneBuffer10write_sizeEm, @function
_ZN2v88internal4wasm10ZoneBuffer10write_sizeEm:
.LFB17942:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	leaq	5(%rdx), %rcx
	cmpq	%rcx, %rax
	jb	.L47
.L39:
	movl	%r13d, %eax
	cmpl	$127, %r13d
	jbe	.L42
	leaq	1(%rdx), %rax
	movq	%rax, 16(%rbx)
	movl	%r13d, %eax
	orl	$-128, %eax
	movb	%al, (%rdx)
	movl	%r13d, %eax
	shrl	$7, %eax
	cmpl	$16383, %r13d
	jbe	.L44
	.p2align 4,,10
	.p2align 3
.L43:
	movq	16(%rbx), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 16(%rbx)
	movl	%eax, %ecx
	shrl	$7, %eax
	orl	$-128, %ecx
	movb	%cl, (%rdx)
	cmpl	$127, %eax
	ja	.L43
.L44:
	movq	16(%rbx), %rdx
.L42:
	leaq	1(%rdx), %rcx
	movq	%rcx, 16(%rbx)
	movb	%al, (%rdx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore_state
	subq	8(%rdi), %rax
	movq	(%rdi), %rdi
	addq	%rax, %rax
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rdx
	leaq	5(%rax), %r12
	addq	$12, %rax
	andq	$-8, %rax
	subq	%rcx, %rdx
	cmpq	%rdx, %rax
	ja	.L48
	addq	%rcx, %rax
	movq	%rax, 16(%rdi)
.L41:
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rdx
	movq	%rcx, %rdi
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	16(%rbx), %rdx
	subq	8(%rbx), %rdx
	addq	%rax, %r12
	movq	%rax, 8(%rbx)
	addq	%rax, %rdx
	movq	%r12, 24(%rbx)
	jmp	.L39
.L48:
	movq	%rax, %rsi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L41
	.cfi_endproc
.LFE17942:
	.size	_ZN2v88internal4wasm10ZoneBuffer10write_sizeEm, .-_ZN2v88internal4wasm10ZoneBuffer10write_sizeEm
	.section	.text._ZN2v88internal4wasm10ZoneBuffer11EnsureSpaceEm,"axG",@progbits,_ZN2v88internal4wasm10ZoneBuffer11EnsureSpaceEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm10ZoneBuffer11EnsureSpaceEm
	.type	_ZN2v88internal4wasm10ZoneBuffer11EnsureSpaceEm, @function
_ZN2v88internal4wasm10ZoneBuffer11EnsureSpaceEm:
.LFB17954:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	addq	%rsi, %rdx
	cmpq	%rdx, %rax
	jb	.L57
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	8(%rdi), %rax
	movq	(%rdi), %rdi
	leaq	(%rsi,%rax,2), %r12
	leaq	7(%r12), %rsi
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	andq	$-8, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L58
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L52:
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rdx
	movq	%rcx, %rdi
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	%rax, %rcx
	movq	16(%rbx), %rax
	subq	8(%rbx), %rax
	addq	%rcx, %rax
	movq	%rcx, 8(%rbx)
	addq	%r12, %rcx
	movq	%rax, 16(%rbx)
	movq	%rcx, 24(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	.cfi_restore_state
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L52
	.cfi_endproc
.LFE17954:
	.size	_ZN2v88internal4wasm10ZoneBuffer11EnsureSpaceEm, .-_ZN2v88internal4wasm10ZoneBuffer11EnsureSpaceEm
	.section	.text._ZN2v88internal4wasm19WasmFunctionBuilderC2EPNS1_17WasmModuleBuilderE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm19WasmFunctionBuilderC2EPNS1_17WasmModuleBuilderE
	.type	_ZN2v88internal4wasm19WasmFunctionBuilderC2EPNS1_17WasmModuleBuilderE, @function
_ZN2v88internal4wasm19WasmFunctionBuilderC2EPNS1_17WasmModuleBuilderE:
.LFB17977:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rsi), %rax
	movq	%rdi, %rbx
	movq	$0, 24(%rdi)
	movq	%rax, 16(%rdi)
	movq	$0, 8(%rdi)
	movq	$0, 32(%rdi)
	movq	$0, 40(%rdi)
	movq	$0, 48(%rdi)
	movq	152(%rsi), %rax
	subq	144(%rsi), %rax
	movq	%rsi, (%rdi)
	sarq	$3, %rax
	movl	$0, 56(%rdi)
	movl	%eax, 60(%rdi)
	movq	(%rsi), %rdi
	movq	%rdi, 64(%rbx)
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$255, %rdx
	jbe	.L65
	leaq	256(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L61:
	movq	%rdx, 88(%rbx)
	movq	%rax, 72(%rbx)
	movq	%rax, 80(%rbx)
	movq	$0, 96(%rbx)
	movq	$0, 104(%rbx)
	movq	(%r12), %rax
	movq	$0, 120(%rbx)
	movq	%rax, 112(%rbx)
	movq	$0, 128(%rbx)
	movq	$0, 136(%rbx)
	movq	(%r12), %rax
	movq	$0, 152(%rbx)
	movq	%rax, 144(%rbx)
	movq	$0, 160(%rbx)
	movq	$0, 168(%rbx)
	movq	(%r12), %rax
	movq	$0, 184(%rbx)
	movq	%rax, 176(%rbx)
	movq	$0, 192(%rbx)
	movq	$0, 200(%rbx)
	movq	(%r12), %rax
	movq	$0, 216(%rbx)
	movq	%rax, 208(%rbx)
	movq	$0, 224(%rbx)
	movq	$0, 232(%rbx)
	movq	(%r12), %rax
	movq	$0, 248(%rbx)
	movq	%rax, 240(%rbx)
	movq	$0, 256(%rbx)
	movq	$0, 264(%rbx)
	movq	(%r12), %rdi
	movq	%rdi, 272(%rbx)
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$7, %rdx
	jbe	.L66
	leaq	8(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L63:
	movq	%rax, 280(%rbx)
	movq	%rax, 288(%rbx)
	movq	%rdx, 296(%rbx)
	movq	$0, 304(%rbx)
	movl	$0, 312(%rbx)
	movb	$-1, 316(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_restore_state
	movl	$256, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	leaq	256(%rax), %rdx
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L66:
	movl	$8, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	leaq	8(%rax), %rdx
	jmp	.L63
	.cfi_endproc
.LFE17977:
	.size	_ZN2v88internal4wasm19WasmFunctionBuilderC2EPNS1_17WasmModuleBuilderE, .-_ZN2v88internal4wasm19WasmFunctionBuilderC2EPNS1_17WasmModuleBuilderE
	.globl	_ZN2v88internal4wasm19WasmFunctionBuilderC1EPNS1_17WasmModuleBuilderE
	.set	_ZN2v88internal4wasm19WasmFunctionBuilderC1EPNS1_17WasmModuleBuilderE,_ZN2v88internal4wasm19WasmFunctionBuilderC2EPNS1_17WasmModuleBuilderE
	.section	.text._ZN2v88internal4wasm19WasmFunctionBuilder8EmitI32VEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm19WasmFunctionBuilder8EmitI32VEi
	.type	_ZN2v88internal4wasm19WasmFunctionBuilder8EmitI32VEi, @function
_ZN2v88internal4wasm19WasmFunctionBuilder8EmitI32VEi:
.LFB17979:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	80(%rdi), %rdx
	movq	88(%rdi), %rsi
	leaq	5(%rdx), %rax
	cmpq	%rax, %rsi
	jb	.L86
.L68:
	testl	%r12d, %r12d
	js	.L87
	cmpl	$63, %r12d
	jle	.L76
	leaq	1(%rdx), %rax
	movq	%rax, 80(%rbx)
	movl	%r12d, %eax
	sarl	$7, %r12d
	orl	$-128, %eax
	movb	%al, (%rdx)
	cmpl	$63, %r12d
	jle	.L78
	.p2align 4,,10
	.p2align 3
.L77:
	movq	80(%rbx), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 80(%rbx)
	movl	%r12d, %edx
	sarl	$7, %r12d
	orl	$-128, %edx
	movb	%dl, (%rax)
	cmpl	$63, %r12d
	jg	.L77
.L78:
	movq	80(%rbx), %rdx
.L76:
	leaq	1(%rdx), %rax
	movq	%rax, 80(%rbx)
	movb	%r12b, (%rdx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	.cfi_restore_state
	movl	%r12d, %eax
	sarl	$6, %eax
	cmpl	$-1, %eax
	je	.L79
	leaq	1(%rdx), %rax
	movq	%rax, 80(%rbx)
	movl	%r12d, %eax
	orl	$-128, %eax
	movb	%al, (%rdx)
	movl	%r12d, %eax
	sarl	$13, %r12d
	sarl	$7, %eax
	cmpl	$-1, %r12d
	je	.L74
	.p2align 4,,10
	.p2align 3
.L73:
	movq	80(%rbx), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 80(%rbx)
	movl	%eax, %ecx
	orl	$-128, %ecx
	movb	%cl, (%rdx)
	movl	%eax, %edx
	sarl	$7, %eax
	sarl	$13, %edx
	cmpl	$-1, %edx
	jne	.L73
.L74:
	movq	80(%rbx), %rdx
.L72:
	leaq	1(%rdx), %rcx
	andl	$127, %eax
	movq	%rcx, 80(%rbx)
	movb	%al, (%rdx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L86:
	.cfi_restore_state
	subq	72(%rdi), %rsi
	movq	64(%rdi), %rdi
	addq	%rsi, %rsi
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	leaq	5(%rsi), %r13
	addq	$12, %rsi
	andq	$-8, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L88
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L70:
	movq	72(%rbx), %rsi
	movq	80(%rbx), %rdx
	movq	%rcx, %rdi
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	80(%rbx), %rdx
	subq	72(%rbx), %rdx
	addq	%rax, %rdx
	addq	%rax, %r13
	movq	%rax, 72(%rbx)
	movq	%rdx, 80(%rbx)
	movq	%r13, 88(%rbx)
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L79:
	movl	%r12d, %eax
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L88:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L70
	.cfi_endproc
.LFE17979:
	.size	_ZN2v88internal4wasm19WasmFunctionBuilder8EmitI32VEi, .-_ZN2v88internal4wasm19WasmFunctionBuilder8EmitI32VEi
	.section	.text._ZN2v88internal4wasm19WasmFunctionBuilder8EmitU32VEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm19WasmFunctionBuilder8EmitU32VEj
	.type	_ZN2v88internal4wasm19WasmFunctionBuilder8EmitU32VEj, @function
_ZN2v88internal4wasm19WasmFunctionBuilder8EmitU32VEj:
.LFB17980:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%esi, %ebx
	subq	$8, %rsp
	movq	80(%rdi), %rdx
	movq	88(%rdi), %rsi
	leaq	5(%rdx), %rax
	cmpq	%rax, %rsi
	jb	.L98
.L90:
	cmpl	$127, %ebx
	jbe	.L93
	leaq	1(%rdx), %rax
	movq	%rax, 80(%r12)
	movl	%ebx, %eax
	shrl	$7, %ebx
	orl	$-128, %eax
	movb	%al, (%rdx)
	cmpl	$127, %ebx
	jbe	.L95
	.p2align 4,,10
	.p2align 3
.L94:
	movq	80(%r12), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 80(%r12)
	movl	%ebx, %edx
	shrl	$7, %ebx
	orl	$-128, %edx
	movb	%dl, (%rax)
	cmpl	$127, %ebx
	ja	.L94
.L95:
	movq	80(%r12), %rdx
.L93:
	leaq	1(%rdx), %rax
	movq	%rax, 80(%r12)
	movb	%bl, (%rdx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	.cfi_restore_state
	subq	72(%rdi), %rsi
	movq	64(%rdi), %rdi
	addq	%rsi, %rsi
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	leaq	5(%rsi), %r13
	addq	$12, %rsi
	andq	$-8, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L99
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L92:
	movq	72(%r12), %rsi
	movq	80(%r12), %rdx
	movq	%rcx, %rdi
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	80(%r12), %rdx
	subq	72(%r12), %rdx
	addq	%rax, %r13
	movq	%rax, 72(%r12)
	addq	%rax, %rdx
	movq	%r13, 88(%r12)
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L99:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L92
	.cfi_endproc
.LFE17980:
	.size	_ZN2v88internal4wasm19WasmFunctionBuilder8EmitU32VEj, .-_ZN2v88internal4wasm19WasmFunctionBuilder8EmitU32VEj
	.section	.text._ZN2v88internal4wasm19WasmFunctionBuilder8AddLocalENS1_9ValueTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm19WasmFunctionBuilder8AddLocalENS1_9ValueTypeE
	.type	_ZN2v88internal4wasm19WasmFunctionBuilder8AddLocalENS1_9ValueTypeE, @function
_ZN2v88internal4wasm19WasmFunctionBuilder8AddLocalENS1_9ValueTypeE:
.LFB17982:
	.cfi_startproc
	endbr64
	movzbl	%sil, %edx
	addq	$8, %rdi
	movl	$1, %esi
	jmp	_ZN2v88internal4wasm16LocalDeclEncoder9AddLocalsEjNS1_9ValueTypeE@PLT
	.cfi_endproc
.LFE17982:
	.size	_ZN2v88internal4wasm19WasmFunctionBuilder8AddLocalENS1_9ValueTypeE, .-_ZN2v88internal4wasm19WasmFunctionBuilder8AddLocalENS1_9ValueTypeE
	.section	.text._ZN2v88internal4wasm19WasmFunctionBuilder12EmitGetLocalEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitGetLocalEj
	.type	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitGetLocalEj, @function
_ZN2v88internal4wasm19WasmFunctionBuilder12EmitGetLocalEj:
.LFB17983:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	80(%rdi), %rdx
	movq	88(%rdi), %rsi
	leaq	1(%rdx), %rax
	cmpq	%rsi, %rax
	ja	.L113
.L102:
	movq	%rax, 80(%rbx)
	movb	$32, (%rdx)
	movq	80(%rbx), %rdx
	movq	88(%rbx), %rsi
	leaq	5(%rdx), %rax
	cmpq	%rax, %rsi
	jb	.L114
.L105:
	cmpl	$127, %r12d
	jbe	.L108
	leaq	1(%rdx), %rax
	movq	%rax, 80(%rbx)
	movl	%r12d, %eax
	shrl	$7, %r12d
	orl	$-128, %eax
	movb	%al, (%rdx)
	cmpl	$127, %r12d
	jbe	.L110
	.p2align 4,,10
	.p2align 3
.L109:
	movq	80(%rbx), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 80(%rbx)
	movl	%r12d, %edx
	shrl	$7, %r12d
	orl	$-128, %edx
	movb	%dl, (%rax)
	cmpl	$127, %r12d
	ja	.L109
.L110:
	movq	80(%rbx), %rdx
.L108:
	leaq	1(%rdx), %rax
	movq	%rax, 80(%rbx)
	movb	%r12b, (%rdx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L113:
	.cfi_restore_state
	subq	72(%rdi), %rsi
	movq	64(%rdi), %rdi
	addq	%rsi, %rsi
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	leaq	1(%rsi), %r13
	addq	$8, %rsi
	andq	$-8, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L115
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L104:
	movq	72(%rbx), %rsi
	movq	80(%rbx), %rdx
	movq	%rcx, %rdi
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	80(%rbx), %rdx
	subq	72(%rbx), %rdx
	addq	%rax, %r13
	addq	%rax, %rdx
	movq	%rax, 72(%rbx)
	movq	%r13, 88(%rbx)
	leaq	1(%rdx), %rax
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L114:
	movq	64(%rbx), %rdi
	subq	72(%rbx), %rsi
	addq	%rsi, %rsi
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	leaq	5(%rsi), %r13
	addq	$12, %rsi
	andq	$-8, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L116
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L107:
	movq	72(%rbx), %rsi
	movq	80(%rbx), %rdx
	movq	%rcx, %rdi
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	80(%rbx), %rdx
	subq	72(%rbx), %rdx
	addq	%rax, %r13
	movq	%rax, 72(%rbx)
	addq	%rax, %rdx
	movq	%r13, 88(%rbx)
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L115:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L116:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L107
	.cfi_endproc
.LFE17983:
	.size	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitGetLocalEj, .-_ZN2v88internal4wasm19WasmFunctionBuilder12EmitGetLocalEj
	.section	.text._ZN2v88internal4wasm19WasmFunctionBuilder12EmitSetLocalEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitSetLocalEj
	.type	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitSetLocalEj, @function
_ZN2v88internal4wasm19WasmFunctionBuilder12EmitSetLocalEj:
.LFB17984:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	80(%rdi), %rdx
	movq	88(%rdi), %rsi
	leaq	1(%rdx), %rax
	cmpq	%rsi, %rax
	ja	.L129
.L118:
	movq	%rax, 80(%rbx)
	movb	$33, (%rdx)
	movq	80(%rbx), %rdx
	movq	88(%rbx), %rsi
	leaq	5(%rdx), %rax
	cmpq	%rax, %rsi
	jb	.L130
.L121:
	cmpl	$127, %r12d
	jbe	.L124
	leaq	1(%rdx), %rax
	movq	%rax, 80(%rbx)
	movl	%r12d, %eax
	shrl	$7, %r12d
	orl	$-128, %eax
	movb	%al, (%rdx)
	cmpl	$127, %r12d
	jbe	.L126
	.p2align 4,,10
	.p2align 3
.L125:
	movq	80(%rbx), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 80(%rbx)
	movl	%r12d, %edx
	shrl	$7, %r12d
	orl	$-128, %edx
	movb	%dl, (%rax)
	cmpl	$127, %r12d
	ja	.L125
.L126:
	movq	80(%rbx), %rdx
.L124:
	leaq	1(%rdx), %rax
	movq	%rax, 80(%rbx)
	movb	%r12b, (%rdx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L129:
	.cfi_restore_state
	subq	72(%rdi), %rsi
	movq	64(%rdi), %rdi
	addq	%rsi, %rsi
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	leaq	1(%rsi), %r13
	addq	$8, %rsi
	andq	$-8, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L131
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L120:
	movq	72(%rbx), %rsi
	movq	80(%rbx), %rdx
	movq	%rcx, %rdi
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	80(%rbx), %rdx
	subq	72(%rbx), %rdx
	addq	%rax, %r13
	addq	%rax, %rdx
	movq	%rax, 72(%rbx)
	movq	%r13, 88(%rbx)
	leaq	1(%rdx), %rax
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L130:
	movq	64(%rbx), %rdi
	subq	72(%rbx), %rsi
	addq	%rsi, %rsi
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	leaq	5(%rsi), %r13
	addq	$12, %rsi
	andq	$-8, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L132
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L123:
	movq	72(%rbx), %rsi
	movq	80(%rbx), %rdx
	movq	%rcx, %rdi
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	80(%rbx), %rdx
	subq	72(%rbx), %rdx
	addq	%rax, %r13
	movq	%rax, 72(%rbx)
	addq	%rax, %rdx
	movq	%r13, 88(%rbx)
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L131:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L132:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L123
	.cfi_endproc
.LFE17984:
	.size	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitSetLocalEj, .-_ZN2v88internal4wasm19WasmFunctionBuilder12EmitSetLocalEj
	.section	.text._ZN2v88internal4wasm19WasmFunctionBuilder12EmitTeeLocalEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitTeeLocalEj
	.type	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitTeeLocalEj, @function
_ZN2v88internal4wasm19WasmFunctionBuilder12EmitTeeLocalEj:
.LFB17985:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	80(%rdi), %rdx
	movq	88(%rdi), %rsi
	leaq	1(%rdx), %rax
	cmpq	%rsi, %rax
	ja	.L145
.L134:
	movq	%rax, 80(%rbx)
	movb	$34, (%rdx)
	movq	80(%rbx), %rdx
	movq	88(%rbx), %rsi
	leaq	5(%rdx), %rax
	cmpq	%rax, %rsi
	jb	.L146
.L137:
	cmpl	$127, %r12d
	jbe	.L140
	leaq	1(%rdx), %rax
	movq	%rax, 80(%rbx)
	movl	%r12d, %eax
	shrl	$7, %r12d
	orl	$-128, %eax
	movb	%al, (%rdx)
	cmpl	$127, %r12d
	jbe	.L142
	.p2align 4,,10
	.p2align 3
.L141:
	movq	80(%rbx), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 80(%rbx)
	movl	%r12d, %edx
	shrl	$7, %r12d
	orl	$-128, %edx
	movb	%dl, (%rax)
	cmpl	$127, %r12d
	ja	.L141
.L142:
	movq	80(%rbx), %rdx
.L140:
	leaq	1(%rdx), %rax
	movq	%rax, 80(%rbx)
	movb	%r12b, (%rdx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L145:
	.cfi_restore_state
	subq	72(%rdi), %rsi
	movq	64(%rdi), %rdi
	addq	%rsi, %rsi
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	leaq	1(%rsi), %r13
	addq	$8, %rsi
	andq	$-8, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L147
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L136:
	movq	72(%rbx), %rsi
	movq	80(%rbx), %rdx
	movq	%rcx, %rdi
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	80(%rbx), %rdx
	subq	72(%rbx), %rdx
	addq	%rax, %r13
	addq	%rax, %rdx
	movq	%rax, 72(%rbx)
	movq	%r13, 88(%rbx)
	leaq	1(%rdx), %rax
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L146:
	movq	64(%rbx), %rdi
	subq	72(%rbx), %rsi
	addq	%rsi, %rsi
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	leaq	5(%rsi), %r13
	addq	$12, %rsi
	andq	$-8, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L148
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L139:
	movq	72(%rbx), %rsi
	movq	80(%rbx), %rdx
	movq	%rcx, %rdi
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	80(%rbx), %rdx
	subq	72(%rbx), %rdx
	addq	%rax, %r13
	movq	%rax, 72(%rbx)
	addq	%rax, %rdx
	movq	%r13, 88(%rbx)
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L147:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L148:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L139
	.cfi_endproc
.LFE17985:
	.size	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitTeeLocalEj, .-_ZN2v88internal4wasm19WasmFunctionBuilder12EmitTeeLocalEj
	.section	.text._ZN2v88internal4wasm19WasmFunctionBuilder8EmitCodeEPKhj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm19WasmFunctionBuilder8EmitCodeEPKhj
	.type	_ZN2v88internal4wasm19WasmFunctionBuilder8EmitCodeEPKhj, @function
_ZN2v88internal4wasm19WasmFunctionBuilder8EmitCodeEPKhj:
.LFB17986:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	80(%rdi), %rdi
	movq	88(%rbx), %rax
	leaq	(%rdi,%r12), %rdx
	cmpq	%rdx, %rax
	jb	.L154
.L150:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	addq	%r12, 80(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L154:
	.cfi_restore_state
	movq	64(%rbx), %rdi
	subq	72(%rbx), %rax
	leaq	(%r12,%rax,2), %r14
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	leaq	7(%r14), %rsi
	andq	$-8, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L155
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L152:
	movq	72(%rbx), %rsi
	movq	80(%rbx), %rdx
	movq	%rcx, %rdi
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	80(%rbx), %rdi
	subq	72(%rbx), %rdi
	movq	%rax, %rcx
	addq	%rax, %rdi
	movq	%rax, 72(%rbx)
	addq	%r14, %rcx
	movq	%rdi, 80(%rbx)
	movq	%rcx, 88(%rbx)
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L155:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L152
	.cfi_endproc
.LFE17986:
	.size	_ZN2v88internal4wasm19WasmFunctionBuilder8EmitCodeEPKhj, .-_ZN2v88internal4wasm19WasmFunctionBuilder8EmitCodeEPKhj
	.section	.text._ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE
	.type	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE, @function
_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE:
.LFB17987:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	80(%rdi), %rdx
	movq	88(%rdi), %rsi
	leaq	1(%rdx), %rax
	cmpq	%rsi, %rax
	ja	.L161
.L157:
	movq	%rax, 80(%rbx)
	movb	%r13b, (%rdx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L161:
	.cfi_restore_state
	subq	72(%rdi), %rsi
	movq	64(%rdi), %rdi
	addq	%rsi, %rsi
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	leaq	1(%rsi), %r12
	addq	$8, %rsi
	andq	$-8, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L162
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L159:
	movq	72(%rbx), %rsi
	movq	80(%rbx), %rdx
	movq	%rcx, %rdi
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	80(%rbx), %rdx
	subq	72(%rbx), %rdx
	addq	%rax, %r12
	addq	%rax, %rdx
	movq	%rax, 72(%rbx)
	movq	%r12, 88(%rbx)
	leaq	1(%rdx), %rax
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L162:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L159
	.cfi_endproc
.LFE17987:
	.size	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE, .-_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE
	.section	.text._ZN2v88internal4wasm19WasmFunctionBuilder10EmitWithU8ENS1_10WasmOpcodeEh,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm19WasmFunctionBuilder10EmitWithU8ENS1_10WasmOpcodeEh
	.type	_ZN2v88internal4wasm19WasmFunctionBuilder10EmitWithU8ENS1_10WasmOpcodeEh, @function
_ZN2v88internal4wasm19WasmFunctionBuilder10EmitWithU8ENS1_10WasmOpcodeEh:
.LFB17988:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	80(%rdi), %rdx
	movq	%rdi, %rbx
	movq	88(%rdi), %rsi
	leaq	1(%rdx), %rax
	cmpq	%rsi, %rax
	ja	.L171
.L164:
	movq	%rax, 80(%rbx)
	movb	%r14b, (%rdx)
	movq	80(%rbx), %rdx
	movq	88(%rbx), %rsi
	leaq	1(%rdx), %rax
	cmpq	%rsi, %rax
	ja	.L172
	movq	%rax, 80(%rbx)
	movb	%r13b, (%rdx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L171:
	.cfi_restore_state
	subq	72(%rdi), %rsi
	movq	64(%rdi), %rdi
	addq	%rsi, %rsi
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	leaq	1(%rsi), %r12
	addq	$8, %rsi
	andq	$-8, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L173
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L166:
	movq	72(%rbx), %rsi
	movq	80(%rbx), %rdx
	movq	%rcx, %rdi
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	80(%rbx), %rdx
	subq	72(%rbx), %rdx
	addq	%rax, %r12
	addq	%rax, %rdx
	movq	%rax, 72(%rbx)
	movq	%r12, 88(%rbx)
	leaq	1(%rdx), %rax
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L172:
	movq	64(%rbx), %rdi
	subq	72(%rbx), %rsi
	addq	%rsi, %rsi
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	leaq	1(%rsi), %r12
	addq	$8, %rsi
	andq	$-8, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L174
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L169:
	movq	72(%rbx), %rsi
	movq	80(%rbx), %rdx
	movq	%rcx, %rdi
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	80(%rbx), %rdx
	subq	72(%rbx), %rdx
	addq	%rax, %rdx
	movq	%rax, 72(%rbx)
	addq	%rax, %r12
	leaq	1(%rdx), %rax
	movq	%r12, 88(%rbx)
	movq	%rax, 80(%rbx)
	movb	%r13b, (%rdx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L173:
	.cfi_restore_state
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L174:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L169
	.cfi_endproc
.LFE17988:
	.size	_ZN2v88internal4wasm19WasmFunctionBuilder10EmitWithU8ENS1_10WasmOpcodeEh, .-_ZN2v88internal4wasm19WasmFunctionBuilder10EmitWithU8ENS1_10WasmOpcodeEh
	.section	.text._ZN2v88internal4wasm19WasmFunctionBuilder12EmitWithU8U8ENS1_10WasmOpcodeEhh,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitWithU8U8ENS1_10WasmOpcodeEhh
	.type	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitWithU8U8ENS1_10WasmOpcodeEhh, @function
_ZN2v88internal4wasm19WasmFunctionBuilder12EmitWithU8U8ENS1_10WasmOpcodeEhh:
.LFB17989:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	80(%rdi), %rdx
	movq	88(%rdi), %rsi
	leaq	1(%rdx), %rax
	cmpq	%rsi, %rax
	ja	.L186
.L176:
	movq	%rax, 80(%rbx)
	movb	%r15b, (%rdx)
	movq	80(%rbx), %rdx
	movq	88(%rbx), %rsi
	leaq	1(%rdx), %rax
	cmpq	%rsi, %rax
	ja	.L187
.L179:
	movq	%rax, 80(%rbx)
	movb	%r14b, (%rdx)
	movq	80(%rbx), %rdx
	movq	88(%rbx), %rsi
	leaq	1(%rdx), %rax
	cmpq	%rsi, %rax
	ja	.L188
.L182:
	movq	%rax, 80(%rbx)
	movb	%r12b, (%rdx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L186:
	.cfi_restore_state
	subq	72(%rdi), %rsi
	movq	64(%rdi), %rdi
	addq	%rsi, %rsi
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	leaq	1(%rsi), %r13
	addq	$8, %rsi
	andq	$-8, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L189
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L178:
	movq	72(%rbx), %rsi
	movq	80(%rbx), %rdx
	movq	%rcx, %rdi
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	80(%rbx), %rdx
	subq	72(%rbx), %rdx
	addq	%rax, %r13
	addq	%rax, %rdx
	movq	%rax, 72(%rbx)
	movq	%r13, 88(%rbx)
	leaq	1(%rdx), %rax
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L188:
	movq	64(%rbx), %rdi
	subq	72(%rbx), %rsi
	addq	%rsi, %rsi
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	leaq	1(%rsi), %r13
	addq	$8, %rsi
	andq	$-8, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L190
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L184:
	movq	72(%rbx), %rsi
	movq	80(%rbx), %rdx
	movq	%rcx, %rdi
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	80(%rbx), %rdx
	subq	72(%rbx), %rdx
	addq	%rax, %r13
	addq	%rax, %rdx
	movq	%rax, 72(%rbx)
	movq	%r13, 88(%rbx)
	leaq	1(%rdx), %rax
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L187:
	movq	64(%rbx), %rdi
	subq	72(%rbx), %rsi
	addq	%rsi, %rsi
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	leaq	1(%rsi), %r13
	addq	$8, %rsi
	andq	$-8, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L191
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L181:
	movq	72(%rbx), %rsi
	movq	80(%rbx), %rdx
	movq	%rcx, %rdi
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	80(%rbx), %rdx
	subq	72(%rbx), %rdx
	addq	%rax, %r13
	addq	%rax, %rdx
	movq	%rax, 72(%rbx)
	movq	%r13, 88(%rbx)
	leaq	1(%rdx), %rax
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L189:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L190:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L191:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L181
	.cfi_endproc
.LFE17989:
	.size	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitWithU8U8ENS1_10WasmOpcodeEhh, .-_ZN2v88internal4wasm19WasmFunctionBuilder12EmitWithU8U8ENS1_10WasmOpcodeEhh
	.section	.text._ZN2v88internal4wasm19WasmFunctionBuilder12EmitWithI32VENS1_10WasmOpcodeEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitWithI32VENS1_10WasmOpcodeEi
	.type	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitWithI32VENS1_10WasmOpcodeEi, @function
_ZN2v88internal4wasm19WasmFunctionBuilder12EmitWithI32VENS1_10WasmOpcodeEi:
.LFB17990:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%esi, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	80(%rdi), %rdx
	movq	%rdi, %rbx
	movq	88(%rdi), %rsi
	leaq	1(%rdx), %rax
	cmpq	%rsi, %rax
	ja	.L214
.L193:
	movq	%rax, 80(%rbx)
	movb	%r14b, (%rdx)
	movq	80(%rbx), %rdx
	movq	88(%rbx), %rsi
	leaq	5(%rdx), %rax
	cmpq	%rax, %rsi
	jb	.L215
.L196:
	testl	%r12d, %r12d
	js	.L216
	cmpl	$63, %r12d
	jle	.L204
	leaq	1(%rdx), %rax
	movq	%rax, 80(%rbx)
	movl	%r12d, %eax
	sarl	$7, %r12d
	orl	$-128, %eax
	movb	%al, (%rdx)
	cmpl	$63, %r12d
	jle	.L206
	.p2align 4,,10
	.p2align 3
.L205:
	movq	80(%rbx), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 80(%rbx)
	movl	%r12d, %edx
	sarl	$7, %r12d
	orl	$-128, %edx
	movb	%dl, (%rax)
	cmpl	$63, %r12d
	jg	.L205
.L206:
	movq	80(%rbx), %rdx
.L204:
	leaq	1(%rdx), %rax
	movq	%rax, 80(%rbx)
	movb	%r12b, (%rdx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L216:
	.cfi_restore_state
	movl	%r12d, %eax
	sarl	$6, %eax
	cmpl	$-1, %eax
	je	.L207
	leaq	1(%rdx), %rax
	movq	%rax, 80(%rbx)
	movl	%r12d, %eax
	orl	$-128, %eax
	movb	%al, (%rdx)
	movl	%r12d, %eax
	sarl	$13, %r12d
	sarl	$7, %eax
	cmpl	$-1, %r12d
	je	.L202
	.p2align 4,,10
	.p2align 3
.L201:
	movq	80(%rbx), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 80(%rbx)
	movl	%eax, %ecx
	orl	$-128, %ecx
	movb	%cl, (%rdx)
	movl	%eax, %edx
	sarl	$7, %eax
	sarl	$13, %edx
	cmpl	$-1, %edx
	jne	.L201
.L202:
	movq	80(%rbx), %rdx
.L200:
	leaq	1(%rdx), %rcx
	andl	$127, %eax
	movq	%rcx, 80(%rbx)
	movb	%al, (%rdx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L215:
	.cfi_restore_state
	movq	64(%rbx), %rdi
	subq	72(%rbx), %rsi
	addq	%rsi, %rsi
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	leaq	5(%rsi), %r13
	addq	$12, %rsi
	andq	$-8, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L217
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L198:
	movq	72(%rbx), %rsi
	movq	80(%rbx), %rdx
	movq	%rcx, %rdi
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	80(%rbx), %rdx
	subq	72(%rbx), %rdx
	addq	%rax, %rdx
	addq	%rax, %r13
	movq	%rax, 72(%rbx)
	movq	%rdx, 80(%rbx)
	movq	%r13, 88(%rbx)
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L214:
	subq	72(%rdi), %rsi
	movq	64(%rdi), %rdi
	addq	%rsi, %rsi
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	leaq	1(%rsi), %r13
	addq	$8, %rsi
	andq	$-8, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L218
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L195:
	movq	72(%rbx), %rsi
	movq	80(%rbx), %rdx
	movq	%rcx, %rdi
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	80(%rbx), %rdx
	subq	72(%rbx), %rdx
	addq	%rax, %r13
	addq	%rax, %rdx
	movq	%rax, 72(%rbx)
	movq	%r13, 88(%rbx)
	leaq	1(%rdx), %rax
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L207:
	movl	%r12d, %eax
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L217:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L218:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L195
	.cfi_endproc
.LFE17990:
	.size	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitWithI32VENS1_10WasmOpcodeEi, .-_ZN2v88internal4wasm19WasmFunctionBuilder12EmitWithI32VENS1_10WasmOpcodeEi
	.section	.text._ZN2v88internal4wasm19WasmFunctionBuilder12EmitWithU32VENS1_10WasmOpcodeEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitWithU32VENS1_10WasmOpcodeEj
	.type	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitWithU32VENS1_10WasmOpcodeEj, @function
_ZN2v88internal4wasm19WasmFunctionBuilder12EmitWithU32VENS1_10WasmOpcodeEj:
.LFB17991:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%esi, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	80(%rdi), %rdx
	movq	%rdi, %rbx
	movq	88(%rdi), %rsi
	leaq	1(%rdx), %rax
	cmpq	%rsi, %rax
	ja	.L231
.L220:
	movq	%rax, 80(%rbx)
	movb	%r14b, (%rdx)
	movq	80(%rbx), %rdx
	movq	88(%rbx), %rsi
	leaq	5(%rdx), %rax
	cmpq	%rax, %rsi
	jb	.L232
.L223:
	cmpl	$127, %r12d
	jbe	.L226
	leaq	1(%rdx), %rax
	movq	%rax, 80(%rbx)
	movl	%r12d, %eax
	shrl	$7, %r12d
	orl	$-128, %eax
	movb	%al, (%rdx)
	cmpl	$127, %r12d
	jbe	.L228
	.p2align 4,,10
	.p2align 3
.L227:
	movq	80(%rbx), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 80(%rbx)
	movl	%r12d, %edx
	shrl	$7, %r12d
	orl	$-128, %edx
	movb	%dl, (%rax)
	cmpl	$127, %r12d
	ja	.L227
.L228:
	movq	80(%rbx), %rdx
.L226:
	leaq	1(%rdx), %rax
	movq	%rax, 80(%rbx)
	movb	%r12b, (%rdx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L231:
	.cfi_restore_state
	subq	72(%rdi), %rsi
	movq	64(%rdi), %rdi
	addq	%rsi, %rsi
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	leaq	1(%rsi), %r13
	addq	$8, %rsi
	andq	$-8, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L233
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L222:
	movq	72(%rbx), %rsi
	movq	80(%rbx), %rdx
	movq	%rcx, %rdi
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	80(%rbx), %rdx
	subq	72(%rbx), %rdx
	addq	%rax, %r13
	addq	%rax, %rdx
	movq	%rax, 72(%rbx)
	movq	%r13, 88(%rbx)
	leaq	1(%rdx), %rax
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L232:
	movq	64(%rbx), %rdi
	subq	72(%rbx), %rsi
	addq	%rsi, %rsi
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	leaq	5(%rsi), %r13
	addq	$12, %rsi
	andq	$-8, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L234
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L225:
	movq	72(%rbx), %rsi
	movq	80(%rbx), %rdx
	movq	%rcx, %rdi
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	80(%rbx), %rdx
	subq	72(%rbx), %rdx
	addq	%rax, %r13
	movq	%rax, 72(%rbx)
	addq	%rax, %rdx
	movq	%r13, 88(%rbx)
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L233:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L234:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L225
	.cfi_endproc
.LFE17991:
	.size	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitWithU32VENS1_10WasmOpcodeEj, .-_ZN2v88internal4wasm19WasmFunctionBuilder12EmitWithU32VENS1_10WasmOpcodeEj
	.section	.text._ZN2v88internal4wasm19WasmFunctionBuilder12EmitI32ConstEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitI32ConstEi
	.type	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitI32ConstEi, @function
_ZN2v88internal4wasm19WasmFunctionBuilder12EmitI32ConstEi:
.LFB17992:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	80(%rdi), %rdx
	movq	88(%rdi), %rsi
	leaq	1(%rdx), %rax
	cmpq	%rsi, %rax
	ja	.L257
.L236:
	movq	%rax, 80(%rbx)
	movb	$65, (%rdx)
	movq	80(%rbx), %rdx
	movq	88(%rbx), %rsi
	leaq	5(%rdx), %rax
	cmpq	%rax, %rsi
	jb	.L258
.L239:
	testl	%r12d, %r12d
	js	.L259
	cmpl	$63, %r12d
	jle	.L247
	leaq	1(%rdx), %rax
	movq	%rax, 80(%rbx)
	movl	%r12d, %eax
	sarl	$7, %r12d
	orl	$-128, %eax
	movb	%al, (%rdx)
	cmpl	$63, %r12d
	jle	.L249
	.p2align 4,,10
	.p2align 3
.L248:
	movq	80(%rbx), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 80(%rbx)
	movl	%r12d, %edx
	sarl	$7, %r12d
	orl	$-128, %edx
	movb	%dl, (%rax)
	cmpl	$63, %r12d
	jg	.L248
.L249:
	movq	80(%rbx), %rdx
.L247:
	leaq	1(%rdx), %rax
	movq	%rax, 80(%rbx)
	movb	%r12b, (%rdx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L259:
	.cfi_restore_state
	movl	%r12d, %eax
	sarl	$6, %eax
	cmpl	$-1, %eax
	je	.L250
	leaq	1(%rdx), %rax
	movq	%rax, 80(%rbx)
	movl	%r12d, %eax
	orl	$-128, %eax
	movb	%al, (%rdx)
	movl	%r12d, %eax
	sarl	$13, %r12d
	sarl	$7, %eax
	cmpl	$-1, %r12d
	je	.L245
	.p2align 4,,10
	.p2align 3
.L244:
	movq	80(%rbx), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 80(%rbx)
	movl	%eax, %ecx
	orl	$-128, %ecx
	movb	%cl, (%rdx)
	movl	%eax, %edx
	sarl	$7, %eax
	sarl	$13, %edx
	cmpl	$-1, %edx
	jne	.L244
.L245:
	movq	80(%rbx), %rdx
.L243:
	leaq	1(%rdx), %rcx
	andl	$127, %eax
	movq	%rcx, 80(%rbx)
	movb	%al, (%rdx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L258:
	.cfi_restore_state
	movq	64(%rbx), %rdi
	subq	72(%rbx), %rsi
	addq	%rsi, %rsi
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	leaq	5(%rsi), %r13
	addq	$12, %rsi
	andq	$-8, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L260
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L241:
	movq	72(%rbx), %rsi
	movq	80(%rbx), %rdx
	movq	%rcx, %rdi
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	80(%rbx), %rdx
	subq	72(%rbx), %rdx
	addq	%rax, %rdx
	addq	%rax, %r13
	movq	%rax, 72(%rbx)
	movq	%rdx, 80(%rbx)
	movq	%r13, 88(%rbx)
	jmp	.L239
	.p2align 4,,10
	.p2align 3
.L257:
	subq	72(%rdi), %rsi
	movq	64(%rdi), %rdi
	addq	%rsi, %rsi
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	leaq	1(%rsi), %r13
	addq	$8, %rsi
	andq	$-8, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L261
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L238:
	movq	72(%rbx), %rsi
	movq	80(%rbx), %rdx
	movq	%rcx, %rdi
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	80(%rbx), %rdx
	subq	72(%rbx), %rdx
	addq	%rax, %r13
	addq	%rax, %rdx
	movq	%rax, 72(%rbx)
	movq	%r13, 88(%rbx)
	leaq	1(%rdx), %rax
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L250:
	movl	%r12d, %eax
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L260:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L261:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L238
	.cfi_endproc
.LFE17992:
	.size	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitI32ConstEi, .-_ZN2v88internal4wasm19WasmFunctionBuilder12EmitI32ConstEi
	.section	.text._ZN2v88internal4wasm19WasmFunctionBuilder12EmitI64ConstEl,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitI64ConstEl
	.type	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitI64ConstEl, @function
_ZN2v88internal4wasm19WasmFunctionBuilder12EmitI64ConstEl:
.LFB17993:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	80(%rdi), %rdx
	movq	88(%rdi), %rsi
	leaq	1(%rdx), %rax
	cmpq	%rsi, %rax
	ja	.L284
.L263:
	movq	%rax, 80(%rbx)
	movb	$66, (%rdx)
	movq	80(%rbx), %rdx
	movq	88(%rbx), %rsi
	leaq	10(%rdx), %rax
	cmpq	%rax, %rsi
	jb	.L285
.L266:
	testq	%r12, %r12
	js	.L286
	cmpq	$63, %r12
	jle	.L274
	leaq	1(%rdx), %rax
	movq	%rax, 80(%rbx)
	movl	%r12d, %eax
	sarq	$7, %r12
	orl	$-128, %eax
	movb	%al, (%rdx)
	cmpq	$63, %r12
	jle	.L276
	.p2align 4,,10
	.p2align 3
.L275:
	movq	80(%rbx), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 80(%rbx)
	movl	%r12d, %edx
	sarq	$7, %r12
	orl	$-128, %edx
	movb	%dl, (%rax)
	cmpq	$63, %r12
	jg	.L275
.L276:
	movq	80(%rbx), %rdx
.L274:
	leaq	1(%rdx), %rax
	movq	%rax, 80(%rbx)
	movb	%r12b, (%rdx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L286:
	.cfi_restore_state
	movq	%r12, %rax
	sarq	$6, %rax
	cmpq	$-1, %rax
	je	.L277
	leaq	1(%rdx), %rax
	movq	%rax, 80(%rbx)
	movl	%r12d, %eax
	orl	$-128, %eax
	movb	%al, (%rdx)
	movq	%r12, %rax
	sarq	$13, %r12
	sarq	$7, %rax
	cmpq	$-1, %r12
	je	.L272
	.p2align 4,,10
	.p2align 3
.L271:
	movq	80(%rbx), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 80(%rbx)
	movl	%eax, %ecx
	orl	$-128, %ecx
	movb	%cl, (%rdx)
	movq	%rax, %rdx
	sarq	$7, %rax
	sarq	$13, %rdx
	cmpq	$-1, %rdx
	jne	.L271
.L272:
	movq	80(%rbx), %rdx
.L270:
	leaq	1(%rdx), %rcx
	andl	$127, %eax
	movq	%rcx, 80(%rbx)
	movb	%al, (%rdx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L285:
	.cfi_restore_state
	movq	64(%rbx), %rdi
	subq	72(%rbx), %rsi
	addq	%rsi, %rsi
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	leaq	10(%rsi), %r13
	addq	$17, %rsi
	andq	$-8, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L287
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L268:
	movq	72(%rbx), %rsi
	movq	80(%rbx), %rdx
	movq	%rcx, %rdi
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	80(%rbx), %rdx
	subq	72(%rbx), %rdx
	addq	%rax, %rdx
	addq	%rax, %r13
	movq	%rax, 72(%rbx)
	movq	%rdx, 80(%rbx)
	movq	%r13, 88(%rbx)
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L284:
	subq	72(%rdi), %rsi
	movq	64(%rdi), %rdi
	addq	%rsi, %rsi
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	leaq	1(%rsi), %r13
	addq	$8, %rsi
	andq	$-8, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L288
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L265:
	movq	72(%rbx), %rsi
	movq	80(%rbx), %rdx
	movq	%rcx, %rdi
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	80(%rbx), %rdx
	subq	72(%rbx), %rdx
	addq	%rax, %r13
	addq	%rax, %rdx
	movq	%rax, 72(%rbx)
	movq	%r13, 88(%rbx)
	leaq	1(%rdx), %rax
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L277:
	movq	%r12, %rax
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L287:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L288:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L265
	.cfi_endproc
.LFE17993:
	.size	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitI64ConstEl, .-_ZN2v88internal4wasm19WasmFunctionBuilder12EmitI64ConstEl
	.section	.text._ZN2v88internal4wasm19WasmFunctionBuilder12EmitF32ConstEf,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitF32ConstEf
	.type	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitF32ConstEf, @function
_ZN2v88internal4wasm19WasmFunctionBuilder12EmitF32ConstEf:
.LFB17994:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movd	%xmm0, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	80(%rdi), %rdx
	movq	88(%rdi), %rsi
	leaq	1(%rdx), %rax
	cmpq	%rsi, %rax
	ja	.L297
.L290:
	movq	%rax, 80(%rbx)
	movb	$67, (%rdx)
	movq	80(%rbx), %rdx
	movq	88(%rbx), %rsi
	leaq	4(%rdx), %rax
	cmpq	%rax, %rsi
	jb	.L298
.L293:
	movl	%r13d, (%rdx)
	addq	$4, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L297:
	.cfi_restore_state
	subq	72(%rdi), %rsi
	movq	64(%rdi), %rdi
	addq	%rsi, %rsi
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	leaq	1(%rsi), %r12
	addq	$8, %rsi
	andq	$-8, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L299
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L292:
	movq	72(%rbx), %rsi
	movq	80(%rbx), %rdx
	movq	%rcx, %rdi
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	80(%rbx), %rdx
	subq	72(%rbx), %rdx
	addq	%rax, %r12
	addq	%rax, %rdx
	movq	%rax, 72(%rbx)
	movq	%r12, 88(%rbx)
	leaq	1(%rdx), %rax
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L298:
	movq	64(%rbx), %rdi
	subq	72(%rbx), %rsi
	addq	%rsi, %rsi
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	leaq	4(%rsi), %r12
	addq	$11, %rsi
	andq	$-8, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L300
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L295:
	movq	72(%rbx), %rsi
	movq	80(%rbx), %rdx
	movq	%rcx, %rdi
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	80(%rbx), %rdx
	subq	72(%rbx), %rdx
	addq	%rax, %rdx
	addq	%rax, %r12
	movq	%rax, 72(%rbx)
	movq	%rdx, 80(%rbx)
	movq	%r12, 88(%rbx)
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L299:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L292
	.p2align 4,,10
	.p2align 3
.L300:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L295
	.cfi_endproc
.LFE17994:
	.size	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitF32ConstEf, .-_ZN2v88internal4wasm19WasmFunctionBuilder12EmitF32ConstEf
	.section	.text._ZN2v88internal4wasm19WasmFunctionBuilder12EmitF64ConstEd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitF64ConstEd
	.type	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitF64ConstEd, @function
_ZN2v88internal4wasm19WasmFunctionBuilder12EmitF64ConstEd:
.LFB17995:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%xmm0, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	80(%rdi), %rdx
	movq	88(%rdi), %rsi
	leaq	1(%rdx), %rax
	cmpq	%rsi, %rax
	ja	.L309
.L302:
	movq	%rax, 80(%rbx)
	movb	$68, (%rdx)
	movq	80(%rbx), %rdx
	movq	88(%rbx), %rsi
	leaq	8(%rdx), %rax
	cmpq	%rax, %rsi
	jb	.L310
.L305:
	movq	%r13, (%rdx)
	addq	$8, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L309:
	.cfi_restore_state
	subq	72(%rdi), %rsi
	movq	64(%rdi), %rdi
	addq	%rsi, %rsi
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	leaq	1(%rsi), %r12
	addq	$8, %rsi
	andq	$-8, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L311
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L304:
	movq	72(%rbx), %rsi
	movq	80(%rbx), %rdx
	movq	%rcx, %rdi
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	80(%rbx), %rdx
	subq	72(%rbx), %rdx
	addq	%rax, %r12
	addq	%rax, %rdx
	movq	%rax, 72(%rbx)
	movq	%r12, 88(%rbx)
	leaq	1(%rdx), %rax
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L310:
	movq	64(%rbx), %rdi
	subq	72(%rbx), %rsi
	addq	%rsi, %rsi
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	leaq	8(%rsi), %r12
	addq	$15, %rsi
	andq	$-8, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L312
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L307:
	movq	72(%rbx), %rsi
	movq	80(%rbx), %rdx
	movq	%rcx, %rdi
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	80(%rbx), %rdx
	subq	72(%rbx), %rdx
	addq	%rax, %rdx
	addq	%rax, %r12
	movq	%rax, 72(%rbx)
	movq	%rdx, 80(%rbx)
	movq	%r12, 88(%rbx)
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L311:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L304
	.p2align 4,,10
	.p2align 3
.L312:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L307
	.cfi_endproc
.LFE17995:
	.size	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitF64ConstEd, .-_ZN2v88internal4wasm19WasmFunctionBuilder12EmitF64ConstEd
	.section	.rodata._ZN2v88internal4wasm19WasmFunctionBuilder19EmitDirectCallIndexEj.str1.1,"aMS",@progbits,1
.LC0:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZN2v88internal4wasm19WasmFunctionBuilder19EmitDirectCallIndexEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm19WasmFunctionBuilder19EmitDirectCallIndexEj
	.type	_ZN2v88internal4wasm19WasmFunctionBuilder19EmitDirectCallIndexEj, @function
_ZN2v88internal4wasm19WasmFunctionBuilder19EmitDirectCallIndexEj:
.LFB17996:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	256(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	80(%rdi), %rax
	subq	72(%rdi), %rax
	movq	%rax, %r13
	cmpq	264(%rdi), %r12
	je	.L314
	movq	%rax, (%r12)
	movl	%esi, 8(%r12)
	addq	$16, 256(%rdi)
.L315:
	movq	80(%rbx), %rdx
	movq	88(%rbx), %rsi
	movl	$0, -61(%rbp)
	movb	$0, -57(%rbp)
	leaq	5(%rdx), %rax
	cmpq	%rax, %rsi
	jb	.L332
.L323:
	movl	-61(%rbp), %eax
	movl	%eax, (%rdx)
	movzbl	-57(%rbp), %eax
	movb	%al, 4(%rdx)
	addq	$5, 80(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L333
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L332:
	.cfi_restore_state
	movq	64(%rbx), %rdi
	subq	72(%rbx), %rsi
	addq	%rsi, %rsi
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	leaq	5(%rsi), %r12
	addq	$12, %rsi
	andq	$-8, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L334
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L325:
	movq	72(%rbx), %rsi
	movq	80(%rbx), %rdx
	movq	%rcx, %rdi
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	80(%rbx), %rdx
	subq	72(%rbx), %rdx
	addq	%rax, %rdx
	addq	%rax, %r12
	movq	%rax, 72(%rbx)
	movq	%rdx, 80(%rbx)
	movq	%r12, 88(%rbx)
	jmp	.L323
	.p2align 4,,10
	.p2align 3
.L314:
	movq	248(%rdi), %r8
	movq	%r12, %r15
	subq	%r8, %r15
	movq	%r15, %rax
	sarq	$4, %rax
	cmpq	$134217727, %rax
	je	.L335
	testq	%rax, %rax
	je	.L327
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L336
	movl	$2147483632, %esi
	movl	$2147483632, %r9d
.L317:
	movq	240(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rsi, %rdx
	jb	.L337
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L320:
	addq	%rax, %r9
	leaq	16(%rax), %rdx
.L318:
	addq	%rax, %r15
	movq	%r13, (%r15)
	movl	%r14d, 8(%r15)
	cmpq	%r8, %r12
	je	.L321
	movq	%r8, %rdx
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L322:
	movq	(%rdx), %rdi
	movl	8(%rdx), %esi
	addq	$16, %rdx
	addq	$16, %rcx
	movq	%rdi, -16(%rcx)
	movl	%esi, -8(%rcx)
	cmpq	%rdx, %r12
	jne	.L322
	subq	%r8, %r12
	leaq	16(%rax,%r12), %rdx
.L321:
	movq	%rax, %xmm0
	movq	%rdx, %xmm1
	movq	%r9, 264(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 248(%rbx)
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L336:
	testq	%rdx, %rdx
	jne	.L338
	movl	$16, %edx
	xorl	%r9d, %r9d
	xorl	%eax, %eax
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L327:
	movl	$16, %esi
	movl	$16, %r9d
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L334:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L325
.L337:
	movq	%r8, -80(%rbp)
	movq	%r9, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-72(%rbp), %r9
	movq	-80(%rbp), %r8
	jmp	.L320
.L333:
	call	__stack_chk_fail@PLT
.L335:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L338:
	cmpq	$134217727, %rdx
	movl	$134217727, %r9d
	cmovbe	%rdx, %r9
	salq	$4, %r9
	movq	%r9, %rsi
	jmp	.L317
	.cfi_endproc
.LFE17996:
	.size	_ZN2v88internal4wasm19WasmFunctionBuilder19EmitDirectCallIndexEj, .-_ZN2v88internal4wasm19WasmFunctionBuilder19EmitDirectCallIndexEj
	.section	.text._ZN2v88internal4wasm19WasmFunctionBuilder7SetNameENS0_6VectorIKcEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm19WasmFunctionBuilder7SetNameENS0_6VectorIKcEE
	.type	_ZN2v88internal4wasm19WasmFunctionBuilder7SetNameENS0_6VectorIKcEE, @function
_ZN2v88internal4wasm19WasmFunctionBuilder7SetNameENS0_6VectorIKcEE:
.LFB17997:
	.cfi_startproc
	endbr64
	movq	%rsi, 96(%rdi)
	movq	%rdx, 104(%rdi)
	ret
	.cfi_endproc
.LFE17997:
	.size	_ZN2v88internal4wasm19WasmFunctionBuilder7SetNameENS0_6VectorIKcEE, .-_ZN2v88internal4wasm19WasmFunctionBuilder7SetNameENS0_6VectorIKcEE
	.section	.text._ZN2v88internal4wasm19WasmFunctionBuilder16AddAsmWasmOffsetEmm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm19WasmFunctionBuilder16AddAsmWasmOffsetEmm
	.type	_ZN2v88internal4wasm19WasmFunctionBuilder16AddAsmWasmOffsetEmm, @function
_ZN2v88internal4wasm19WasmFunctionBuilder16AddAsmWasmOffsetEmm:
.LFB17998:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	288(%rdi), %rdx
	movq	296(%rdi), %rsi
	movq	80(%rdi), %rcx
	subq	72(%rdi), %rcx
	leaq	5(%rdx), %rax
	movl	%ecx, %r12d
	subl	304(%rdi), %r12d
	cmpq	%rax, %rsi
	jb	.L383
.L341:
	cmpl	$127, %r12d
	jbe	.L344
	leaq	1(%rdx), %rax
	movq	%rax, 288(%rbx)
	movl	%r12d, %eax
	shrl	$7, %r12d
	orl	$-128, %eax
	movb	%al, (%rdx)
	cmpl	$127, %r12d
	jbe	.L346
	.p2align 4,,10
	.p2align 3
.L345:
	movq	288(%rbx), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 288(%rbx)
	movl	%r12d, %edx
	shrl	$7, %r12d
	orl	$-128, %edx
	movb	%dl, (%rax)
	cmpl	$127, %r12d
	ja	.L345
.L346:
	movq	288(%rbx), %rdx
.L344:
	leaq	1(%rdx), %rax
	movl	%r15d, %r13d
	movq	%rax, 288(%rbx)
	movb	%r12b, (%rdx)
	movq	288(%rbx), %rdx
	movq	296(%rbx), %rsi
	subl	308(%rbx), %r13d
	movl	%ecx, 304(%rbx)
	leaq	5(%rdx), %rax
	movl	%r13d, %ecx
	cmpq	%rax, %rsi
	jb	.L384
.L347:
	testl	%r13d, %r13d
	js	.L385
	cmpl	$63, %r13d
	jle	.L355
	leaq	1(%rdx), %rax
	movl	%r13d, %ecx
	movq	%rax, 288(%rbx)
	movl	%r13d, %eax
	sarl	$7, %ecx
	orl	$-128, %eax
	movb	%al, (%rdx)
	cmpl	$8191, %r13d
	jle	.L357
	.p2align 4,,10
	.p2align 3
.L356:
	movq	288(%rbx), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 288(%rbx)
	movl	%ecx, %edx
	sarl	$7, %ecx
	orl	$-128, %edx
	movb	%dl, (%rax)
	cmpl	$63, %ecx
	jg	.L356
.L357:
	movq	288(%rbx), %rdx
.L355:
	leaq	1(%rdx), %rax
	movq	%rax, 288(%rbx)
	movb	%cl, (%rdx)
.L354:
	movq	288(%rbx), %rdx
	movl	%r14d, %r12d
	movq	296(%rbx), %rsi
	subl	%r15d, %r12d
	leaq	5(%rdx), %rax
	movl	%r12d, %r15d
	cmpq	%rax, %rsi
	jb	.L386
.L358:
	testl	%r12d, %r12d
	js	.L387
	cmpl	$63, %r12d
	jle	.L366
	leaq	1(%rdx), %rax
	movl	%r12d, %r15d
	movq	%rax, 288(%rbx)
	movl	%r12d, %eax
	sarl	$7, %r15d
	orl	$-128, %eax
	movb	%al, (%rdx)
	cmpl	$8191, %r12d
	jle	.L368
	.p2align 4,,10
	.p2align 3
.L367:
	movq	288(%rbx), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 288(%rbx)
	movl	%r15d, %edx
	sarl	$7, %r15d
	orl	$-128, %edx
	movb	%dl, (%rax)
	cmpl	$63, %r15d
	jg	.L367
.L368:
	movq	288(%rbx), %rdx
.L366:
	leaq	1(%rdx), %rax
	movq	%rax, 288(%rbx)
	movb	%r15b, (%rdx)
.L365:
	movl	%r14d, 308(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L385:
	.cfi_restore_state
	movl	%r13d, %eax
	sarl	$6, %eax
	cmpl	$-1, %eax
	je	.L369
	leaq	1(%rdx), %rax
	movq	%rax, 288(%rbx)
	movl	%r13d, %eax
	orl	$-128, %eax
	movb	%al, (%rdx)
	movl	%r13d, %eax
	sarl	$13, %r13d
	sarl	$7, %eax
	cmpl	$-1, %r13d
	je	.L353
	.p2align 4,,10
	.p2align 3
.L352:
	movq	288(%rbx), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 288(%rbx)
	movl	%eax, %ecx
	orl	$-128, %ecx
	movb	%cl, (%rdx)
	movl	%eax, %edx
	sarl	$7, %eax
	sarl	$13, %edx
	cmpl	$-1, %edx
	jne	.L352
.L353:
	movq	288(%rbx), %rdx
.L351:
	leaq	1(%rdx), %rcx
	andl	$127, %eax
	movq	%rcx, 288(%rbx)
	movb	%al, (%rdx)
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L387:
	movl	%r12d, %eax
	sarl	$6, %eax
	cmpl	$-1, %eax
	je	.L370
	leaq	1(%rdx), %rax
	movq	%rax, 288(%rbx)
	movl	%r12d, %eax
	orl	$-128, %eax
	movb	%al, (%rdx)
	movl	%r12d, %eax
	sarl	$13, %r12d
	sarl	$7, %eax
	cmpl	$-1, %r12d
	je	.L364
	.p2align 4,,10
	.p2align 3
.L363:
	movq	288(%rbx), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 288(%rbx)
	movl	%eax, %ecx
	orl	$-128, %ecx
	movb	%cl, (%rdx)
	movl	%eax, %edx
	sarl	$7, %eax
	sarl	$13, %edx
	cmpl	$-1, %edx
	jne	.L363
.L364:
	movq	288(%rbx), %rdx
.L362:
	leaq	1(%rdx), %rcx
	andl	$127, %eax
	movq	%rcx, 288(%rbx)
	movb	%al, (%rdx)
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L386:
	movq	272(%rbx), %rdi
	subq	280(%rbx), %rsi
	addq	%rsi, %rsi
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	leaq	5(%rsi), %r13
	addq	$12, %rsi
	andq	$-8, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L388
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L360:
	movq	280(%rbx), %rsi
	movq	288(%rbx), %rdx
	movq	%rcx, %rdi
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	288(%rbx), %rdx
	subq	280(%rbx), %rdx
	addq	%rax, %rdx
	addq	%rax, %r13
	movq	%rax, 280(%rbx)
	movq	%rdx, 288(%rbx)
	movq	%r13, 296(%rbx)
	jmp	.L358
	.p2align 4,,10
	.p2align 3
.L384:
	movq	272(%rbx), %r8
	subq	280(%rbx), %rsi
	addq	%rsi, %rsi
	movq	16(%r8), %rdi
	movq	24(%r8), %rax
	leaq	5(%rsi), %r12
	addq	$12, %rsi
	andq	$-8, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L389
	addq	%rdi, %rsi
	movq	%rsi, 16(%r8)
.L349:
	movq	280(%rbx), %rsi
	movq	288(%rbx), %rdx
	movl	%ecx, -56(%rbp)
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	288(%rbx), %rdx
	movl	-56(%rbp), %ecx
	subq	280(%rbx), %rdx
	addq	%rax, %r12
	movq	%rax, 280(%rbx)
	addq	%rax, %rdx
	movq	%r12, 296(%rbx)
	movq	%rdx, 288(%rbx)
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L383:
	movq	272(%rdi), %r8
	subq	280(%rdi), %rsi
	addq	%rsi, %rsi
	movq	16(%r8), %rdi
	movq	24(%r8), %rax
	leaq	5(%rsi), %r13
	addq	$12, %rsi
	andq	$-8, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L390
	addq	%rdi, %rsi
	movq	%rsi, 16(%r8)
.L343:
	movq	280(%rbx), %rsi
	movq	288(%rbx), %rdx
	movq	%rcx, -56(%rbp)
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	288(%rbx), %rdx
	movq	-56(%rbp), %rcx
	subq	280(%rbx), %rdx
	addq	%rax, %r13
	movq	%rax, 280(%rbx)
	movq	%r13, 296(%rbx)
	addq	%rax, %rdx
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L369:
	movl	%r13d, %eax
	jmp	.L351
	.p2align 4,,10
	.p2align 3
.L370:
	movl	%r12d, %eax
	jmp	.L362
	.p2align 4,,10
	.p2align 3
.L388:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L360
	.p2align 4,,10
	.p2align 3
.L389:
	movq	%r8, %rdi
	movl	%r13d, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-56(%rbp), %ecx
	movq	%rax, %rdi
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L390:
	movq	%r8, %rdi
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	%rax, %rdi
	jmp	.L343
	.cfi_endproc
.LFE17998:
	.size	_ZN2v88internal4wasm19WasmFunctionBuilder16AddAsmWasmOffsetEmm, .-_ZN2v88internal4wasm19WasmFunctionBuilder16AddAsmWasmOffsetEmm
	.section	.text._ZN2v88internal4wasm19WasmFunctionBuilder27SetAsmFunctionStartPositionEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm19WasmFunctionBuilder27SetAsmFunctionStartPositionEm
	.type	_ZN2v88internal4wasm19WasmFunctionBuilder27SetAsmFunctionStartPositionEm, @function
_ZN2v88internal4wasm19WasmFunctionBuilder27SetAsmFunctionStartPositionEm:
.LFB17999:
	.cfi_startproc
	endbr64
	movl	%esi, 312(%rdi)
	movl	%esi, 308(%rdi)
	ret
	.cfi_endproc
.LFE17999:
	.size	_ZN2v88internal4wasm19WasmFunctionBuilder27SetAsmFunctionStartPositionEm, .-_ZN2v88internal4wasm19WasmFunctionBuilder27SetAsmFunctionStartPositionEm
	.section	.text._ZN2v88internal4wasm19WasmFunctionBuilder18SetCompilationHintENS1_27WasmCompilationHintStrategyENS1_23WasmCompilationHintTierES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm19WasmFunctionBuilder18SetCompilationHintENS1_27WasmCompilationHintStrategyENS1_23WasmCompilationHintTierES4_
	.type	_ZN2v88internal4wasm19WasmFunctionBuilder18SetCompilationHintENS1_27WasmCompilationHintStrategyENS1_23WasmCompilationHintTierES4_, @function
_ZN2v88internal4wasm19WasmFunctionBuilder18SetCompilationHintENS1_27WasmCompilationHintStrategyENS1_23WasmCompilationHintTierES4_:
.LFB18000:
	.cfi_startproc
	endbr64
	sall	$4, %ecx
	sall	$2, %edx
	orl	%esi, %ecx
	orl	%ecx, %edx
	movb	%dl, 316(%rdi)
	ret
	.cfi_endproc
.LFE18000:
	.size	_ZN2v88internal4wasm19WasmFunctionBuilder18SetCompilationHintENS1_27WasmCompilationHintStrategyENS1_23WasmCompilationHintTierES4_, .-_ZN2v88internal4wasm19WasmFunctionBuilder18SetCompilationHintENS1_27WasmCompilationHintStrategyENS1_23WasmCompilationHintTierES4_
	.section	.text._ZN2v88internal4wasm19WasmFunctionBuilder15DeleteCodeAfterEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm19WasmFunctionBuilder15DeleteCodeAfterEm
	.type	_ZN2v88internal4wasm19WasmFunctionBuilder15DeleteCodeAfterEm, @function
_ZN2v88internal4wasm19WasmFunctionBuilder15DeleteCodeAfterEm:
.LFB18001:
	.cfi_startproc
	endbr64
	addq	72(%rdi), %rsi
	movq	%rsi, 80(%rdi)
	ret
	.cfi_endproc
.LFE18001:
	.size	_ZN2v88internal4wasm19WasmFunctionBuilder15DeleteCodeAfterEm, .-_ZN2v88internal4wasm19WasmFunctionBuilder15DeleteCodeAfterEm
	.section	.text._ZNK2v88internal4wasm19WasmFunctionBuilder14WriteSignatureEPNS1_10ZoneBufferE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4wasm19WasmFunctionBuilder14WriteSignatureEPNS1_10ZoneBufferE
	.type	_ZNK2v88internal4wasm19WasmFunctionBuilder14WriteSignatureEPNS1_10ZoneBufferE, @function
_ZNK2v88internal4wasm19WasmFunctionBuilder14WriteSignatureEPNS1_10ZoneBufferE:
.LFB18002:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	16(%rsi), %rdx
	movq	24(%rsi), %rsi
	movl	56(%rdi), %r12d
	leaq	5(%rdx), %rax
	cmpq	%rax, %rsi
	jb	.L403
.L395:
	cmpl	$127, %r12d
	jbe	.L398
	leaq	1(%rdx), %rax
	movq	%rax, 16(%rbx)
	movl	%r12d, %eax
	shrl	$7, %r12d
	orl	$-128, %eax
	movb	%al, (%rdx)
	cmpl	$127, %r12d
	jbe	.L400
	.p2align 4,,10
	.p2align 3
.L399:
	movq	16(%rbx), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movl	%r12d, %edx
	shrl	$7, %r12d
	orl	$-128, %edx
	movb	%dl, (%rax)
	cmpl	$127, %r12d
	ja	.L399
.L400:
	movq	16(%rbx), %rdx
.L398:
	leaq	1(%rdx), %rax
	movq	%rax, 16(%rbx)
	movb	%r12b, (%rdx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L403:
	.cfi_restore_state
	movq	(%rbx), %rdi
	subq	8(%rbx), %rsi
	addq	%rsi, %rsi
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	leaq	5(%rsi), %r13
	addq	$12, %rsi
	andq	$-8, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L404
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L397:
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rdx
	movq	%rcx, %rdi
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	16(%rbx), %rdx
	subq	8(%rbx), %rdx
	addq	%rax, %r13
	movq	%rax, 8(%rbx)
	addq	%rax, %rdx
	movq	%r13, 24(%rbx)
	jmp	.L395
	.p2align 4,,10
	.p2align 3
.L404:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L397
	.cfi_endproc
.LFE18002:
	.size	_ZNK2v88internal4wasm19WasmFunctionBuilder14WriteSignatureEPNS1_10ZoneBufferE, .-_ZNK2v88internal4wasm19WasmFunctionBuilder14WriteSignatureEPNS1_10ZoneBufferE
	.section	.text._ZNK2v88internal4wasm19WasmFunctionBuilder9WriteBodyEPNS1_10ZoneBufferE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4wasm19WasmFunctionBuilder9WriteBodyEPNS1_10ZoneBufferE
	.type	_ZNK2v88internal4wasm19WasmFunctionBuilder9WriteBodyEPNS1_10ZoneBufferE, @function
_ZNK2v88internal4wasm19WasmFunctionBuilder9WriteBodyEPNS1_10ZoneBufferE:
.LFB18003:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	8(%rdi), %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r15, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	call	_ZNK2v88internal4wasm16LocalDeclEncoder4SizeEv@PLT
	movq	80(%r12), %r14
	movq	16(%rbx), %rdx
	subq	72(%r12), %r14
	movq	%rax, %r13
	addq	%rax, %r14
	movq	24(%rbx), %rax
	leaq	5(%rdx), %rcx
	cmpq	%rcx, %rax
	jb	.L424
.L406:
	movl	%r14d, %eax
	cmpl	$127, %r14d
	jbe	.L409
	leaq	1(%rdx), %rax
	movq	%rax, 16(%rbx)
	movl	%r14d, %eax
	orl	$-128, %eax
	movb	%al, (%rdx)
	movl	%r14d, %eax
	shrl	$7, %eax
	cmpl	$16383, %r14d
	jbe	.L411
	.p2align 4,,10
	.p2align 3
.L410:
	movq	16(%rbx), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 16(%rbx)
	movl	%eax, %ecx
	shrl	$7, %eax
	orl	$-128, %ecx
	movb	%cl, (%rdx)
	cmpl	$127, %eax
	ja	.L410
.L411:
	movq	16(%rbx), %rdx
.L409:
	leaq	1(%rdx), %rcx
	movq	%rcx, 16(%rbx)
	movb	%al, (%rdx)
	movq	16(%rbx), %rsi
	movq	24(%rbx), %rax
	leaq	(%rsi,%r13), %rdx
	cmpq	%rdx, %rax
	jb	.L425
.L412:
	movq	%r15, %rdi
	call	_ZNK2v88internal4wasm16LocalDeclEncoder4EmitEPh@PLT
	movq	16(%rbx), %rdi
	addq	%r13, %rdi
	movq	%rdi, 16(%rbx)
	movq	72(%r12), %r15
	movq	80(%r12), %rax
	cmpq	%rax, %r15
	je	.L405
	subq	%r15, %rax
	movq	8(%rbx), %rdx
	movq	%rdi, %r13
	movq	%rax, %r14
	movq	24(%rbx), %rax
	leaq	(%rdi,%r14), %rcx
	subq	%rdx, %r13
	cmpq	%rcx, %rax
	jb	.L426
.L416:
	movq	%r14, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	addq	%r14, 16(%rbx)
	movq	248(%r12), %rcx
	movq	256(%r12), %rdi
	cmpq	%rdi, %rcx
	je	.L405
	movabsq	$-6148914691236517205, %r8
	.p2align 4,,10
	.p2align 3
.L420:
	movq	(%r12), %rdx
	addq	$16, %rcx
	movq	56(%rdx), %rax
	subq	48(%rdx), %rax
	sarq	$3, %rax
	movq	-16(%rcx), %rdx
	imulq	%r8, %rax
	addl	-8(%rcx), %eax
	movl	%eax, %esi
	addq	%r13, %rdx
	addq	8(%rbx), %rdx
	orl	$-128, %esi
	movb	%sil, (%rdx)
	movl	%eax, %esi
	shrl	$7, %esi
	orl	$-128, %esi
	movb	%sil, 1(%rdx)
	movl	%eax, %esi
	shrl	$14, %esi
	orl	$-128, %esi
	movb	%sil, 2(%rdx)
	movl	%eax, %esi
	shrl	$28, %eax
	shrl	$21, %esi
	movb	%al, 4(%rdx)
	orl	$-128, %esi
	movb	%sil, 3(%rdx)
	cmpq	%rcx, %rdi
	jne	.L420
.L405:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L425:
	.cfi_restore_state
	movq	(%rbx), %rdi
	subq	8(%rbx), %rax
	leaq	0(%r13,%rax,2), %r14
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	leaq	7(%r14), %rsi
	andq	$-8, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L427
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L414:
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rdx
	movq	%rcx, %rdi
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	16(%rbx), %rsi
	subq	8(%rbx), %rsi
	addq	%rax, %rsi
	movq	%rax, 8(%rbx)
	leaq	(%rax,%r14), %rax
	movq	%rsi, 16(%rbx)
	movq	%rax, 24(%rbx)
	jmp	.L412
	.p2align 4,,10
	.p2align 3
.L424:
	subq	8(%rbx), %rax
	addq	%rax, %rax
	leaq	5(%rax), %rdi
	leaq	12(%rax), %rsi
	movq	%rdi, -56(%rbp)
	movq	(%rbx), %rdi
	andq	$-8, %rsi
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L428
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L408:
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rdx
	movq	%rcx, %rdi
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	16(%rbx), %rdx
	subq	8(%rbx), %rdx
	movq	%rax, %rcx
	movq	%rax, 8(%rbx)
	addq	%rax, %rdx
	movq	-56(%rbp), %rax
	addq	%rcx, %rax
	movq	%rax, 24(%rbx)
	jmp	.L406
.L426:
	movq	(%rbx), %rdi
	subq	%rdx, %rax
	leaq	(%r14,%rax,2), %r8
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	leaq	7(%r8), %rsi
	andq	$-8, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L429
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L418:
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rdx
	movq	%rcx, %rdi
	movq	%r8, -56(%rbp)
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	-56(%rbp), %r8
	movq	16(%rbx), %rdi
	movq	%rax, %rcx
	subq	8(%rbx), %rdi
	movq	%rax, 8(%rbx)
	addq	%rax, %rdi
	addq	%r8, %rcx
	movq	%rdi, 16(%rbx)
	movq	%rcx, 24(%rbx)
	jmp	.L416
.L428:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L408
.L427:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L414
.L429:
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r8
	movq	%rax, %rcx
	jmp	.L418
	.cfi_endproc
.LFE18003:
	.size	_ZNK2v88internal4wasm19WasmFunctionBuilder9WriteBodyEPNS1_10ZoneBufferE, .-_ZNK2v88internal4wasm19WasmFunctionBuilder9WriteBodyEPNS1_10ZoneBufferE
	.section	.text._ZNK2v88internal4wasm19WasmFunctionBuilder23WriteAsmWasmOffsetTableEPNS1_10ZoneBufferE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4wasm19WasmFunctionBuilder23WriteAsmWasmOffsetTableEPNS1_10ZoneBufferE
	.type	_ZNK2v88internal4wasm19WasmFunctionBuilder23WriteAsmWasmOffsetTableEPNS1_10ZoneBufferE, @function
_ZNK2v88internal4wasm19WasmFunctionBuilder23WriteAsmWasmOffsetTableEPNS1_10ZoneBufferE:
.LFB18004:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
	movl	312(%rdi), %eax
	testl	%eax, %eax
	jne	.L431
	movq	288(%rdi), %rax
	cmpq	%rax, 280(%rdi)
	je	.L468
.L431:
	leaq	8(%r13), %r12
	movq	%r12, %rdi
	call	_ZNK2v88internal4wasm16LocalDeclEncoder4SizeEv@PLT
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L436:
	addq	$1, %rcx
	shrq	$7, %rax
	jne	.L436
	movl	312(%r13), %edx
	.p2align 4,,10
	.p2align 3
.L437:
	addq	$1, %rax
	shrq	$7, %rdx
	jne	.L437
	movq	288(%r13), %r14
	subq	280(%r13), %r14
	addq	%r14, %rcx
	movq	16(%rbx), %rdx
	leaq	(%rcx,%rax), %r14
	movq	24(%rbx), %rax
	leaq	5(%rdx), %rcx
	cmpq	%rcx, %rax
	jb	.L469
.L438:
	movl	%r14d, %eax
	cmpl	$127, %r14d
	jbe	.L441
	leaq	1(%rdx), %rax
	movq	%rax, 16(%rbx)
	movl	%r14d, %eax
	orl	$-128, %eax
	movb	%al, (%rdx)
	movl	%r14d, %eax
	shrl	$7, %eax
	cmpl	$16383, %r14d
	jbe	.L443
	.p2align 4,,10
	.p2align 3
.L442:
	movq	16(%rbx), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 16(%rbx)
	movl	%eax, %ecx
	shrl	$7, %eax
	orl	$-128, %ecx
	movb	%cl, (%rdx)
	cmpl	$127, %eax
	ja	.L442
.L443:
	movq	16(%rbx), %rdx
.L441:
	leaq	1(%rdx), %rcx
	movq	%r12, %rdi
	movq	%rcx, 16(%rbx)
	movb	%al, (%rdx)
	call	_ZNK2v88internal4wasm16LocalDeclEncoder4SizeEv@PLT
	movq	24(%rbx), %rdx
	movq	%rax, %r15
	movl	%eax, %r12d
	movq	16(%rbx), %rax
	leaq	5(%rax), %rcx
	cmpq	%rcx, %rdx
	jb	.L470
.L444:
	cmpl	$127, %r15d
	jbe	.L447
	leaq	1(%rax), %rdx
	movl	%r15d, %r12d
	movq	%rdx, 16(%rbx)
	movl	%r15d, %edx
	shrl	$7, %r12d
	orl	$-128, %edx
	movb	%dl, (%rax)
	cmpl	$16383, %r15d
	jbe	.L449
	.p2align 4,,10
	.p2align 3
.L448:
	movq	16(%rbx), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movl	%r12d, %edx
	shrl	$7, %r12d
	orl	$-128, %edx
	movb	%dl, (%rax)
	cmpl	$127, %r12d
	ja	.L448
.L449:
	movq	16(%rbx), %rax
.L447:
	leaq	1(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movb	%r12b, (%rax)
	movq	16(%rbx), %rax
	movq	24(%rbx), %rdx
	movl	312(%r13), %r12d
	leaq	5(%rax), %rcx
	cmpq	%rcx, %rdx
	jnb	.L467
	jmp	.L471
	.p2align 4,,10
	.p2align 3
.L466:
	movl	%r12d, %edx
	shrl	$7, %r12d
	orl	$-128, %edx
	movb	%dl, (%rax)
	movq	16(%rbx), %rax
.L467:
	leaq	1(%rax), %rdx
	movq	%rdx, 16(%rbx)
	cmpl	$127, %r12d
	ja	.L466
	movb	%r12b, (%rax)
	movq	280(%r13), %r14
	movq	288(%r13), %r12
	movq	16(%rbx), %rdi
	movq	24(%rbx), %rax
	subq	%r14, %r12
	leaq	(%rdi,%r12), %rdx
	cmpq	%rdx, %rax
	jb	.L472
.L456:
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	addq	%r12, 16(%rbx)
.L430:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L469:
	.cfi_restore_state
	movq	(%rbx), %rdi
	subq	8(%rbx), %rax
	addq	%rax, %rax
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rdx
	leaq	5(%rax), %r15
	addq	$12, %rax
	andq	$-8, %rax
	subq	%rcx, %rdx
	cmpq	%rdx, %rax
	ja	.L473
	addq	%rcx, %rax
	movq	%rax, 16(%rdi)
.L440:
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rdx
	movq	%rcx, %rdi
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	16(%rbx), %rdx
	subq	8(%rbx), %rdx
	addq	%rax, %r15
	movq	%rax, 8(%rbx)
	addq	%rax, %rdx
	movq	%r15, 24(%rbx)
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L472:
	movq	(%rbx), %rdi
	subq	8(%rbx), %rax
	leaq	(%r12,%rax,2), %r13
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	leaq	7(%r13), %rsi
	andq	$-8, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L474
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L458:
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rdx
	movq	%rcx, %rdi
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	16(%rbx), %rdi
	subq	8(%rbx), %rdi
	movq	%rax, %rcx
	addq	%rax, %rdi
	movq	%rax, 8(%rbx)
	addq	%r13, %rcx
	movq	%rdi, 16(%rbx)
	movq	%rcx, 24(%rbx)
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L471:
	movq	(%rbx), %rdi
	subq	8(%rbx), %rdx
	addq	%rdx, %rdx
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	leaq	5(%rdx), %r14
	addq	$12, %rdx
	andq	$-8, %rdx
	subq	%rcx, %rax
	cmpq	%rax, %rdx
	ja	.L475
	addq	%rcx, %rdx
	movq	%rdx, 16(%rdi)
.L452:
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rdx
	movq	%rcx, %rdi
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	%rax, %rcx
	movq	16(%rbx), %rax
	subq	8(%rbx), %rax
	addq	%rcx, %r14
	movq	%rcx, 8(%rbx)
	addq	%rcx, %rax
	movq	%r14, 24(%rbx)
	jmp	.L467
	.p2align 4,,10
	.p2align 3
.L470:
	movq	(%rbx), %rdi
	subq	8(%rbx), %rdx
	addq	%rdx, %rdx
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	leaq	5(%rdx), %r14
	addq	$12, %rdx
	andq	$-8, %rdx
	subq	%rcx, %rax
	cmpq	%rax, %rdx
	ja	.L476
	addq	%rcx, %rdx
	movq	%rdx, 16(%rdi)
.L446:
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rdx
	movq	%rcx, %rdi
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	%rax, %rcx
	movq	16(%rbx), %rax
	subq	8(%rbx), %rax
	addq	%rcx, %r14
	movq	%rcx, 8(%rbx)
	addq	%rcx, %rax
	movq	%r14, 24(%rbx)
	jmp	.L444
.L468:
	movq	16(%rsi), %rdx
	movq	24(%rsi), %rax
	leaq	5(%rdx), %rcx
	cmpq	%rcx, %rax
	jb	.L477
.L432:
	leaq	1(%rdx), %rax
	movq	%rax, 16(%rbx)
	movb	$0, (%rdx)
	jmp	.L430
.L477:
	movq	(%rsi), %rdi
	subq	8(%rsi), %rax
	addq	%rax, %rax
	leaq	12(%rax), %rsi
	movq	16(%rdi), %rcx
	leaq	5(%rax), %r12
	movq	24(%rdi), %rax
	andq	$-8, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L478
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L434:
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rdx
	movq	%rcx, %rdi
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	16(%rbx), %rdx
	subq	8(%rbx), %rdx
	movq	%rax, %rcx
	movq	%rax, 8(%rbx)
	addq	%rax, %rdx
	addq	%r12, %rcx
	movq	%rcx, 24(%rbx)
	jmp	.L432
.L473:
	movq	%rax, %rsi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L440
.L475:
	movq	%rdx, %rsi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L452
.L476:
	movq	%rdx, %rsi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L446
.L474:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L458
.L478:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L434
	.cfi_endproc
.LFE18004:
	.size	_ZNK2v88internal4wasm19WasmFunctionBuilder23WriteAsmWasmOffsetTableEPNS1_10ZoneBufferE, .-_ZNK2v88internal4wasm19WasmFunctionBuilder23WriteAsmWasmOffsetTableEPNS1_10ZoneBufferE
	.section	.text._ZN2v88internal4wasm17WasmModuleBuilderC2EPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm17WasmModuleBuilderC2EPNS0_4ZoneE
	.type	_ZN2v88internal4wasm17WasmModuleBuilderC2EPNS0_4ZoneE, @function
_ZN2v88internal4wasm17WasmModuleBuilderC2EPNS0_4ZoneE:
.LFB18036:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %xmm1
	punpcklqdq	%xmm1, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	352(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	addq	$336, %rdi
	subq	$24, %rsp
	movups	%xmm1, -336(%rdi)
	pxor	%xmm1, %xmm1
	movq	%rsi, -296(%rdi)
	movq	%rsi, -264(%rdi)
	movq	%rsi, -232(%rdi)
	movq	%rsi, -200(%rdi)
	movq	%rsi, -168(%rdi)
	movq	%rsi, -136(%rdi)
	movq	%rsi, -104(%rdi)
	movq	%rsi, -72(%rdi)
	movups	%xmm1, -320(%rdi)
	movq	$0, -304(%rdi)
	movups	%xmm1, -288(%rdi)
	movq	$0, -272(%rdi)
	movups	%xmm1, -256(%rdi)
	movq	$0, -240(%rdi)
	movups	%xmm1, -224(%rdi)
	movq	$0, -208(%rdi)
	movups	%xmm1, -192(%rdi)
	movq	$0, -176(%rdi)
	movups	%xmm1, -160(%rdi)
	movq	$0, -144(%rdi)
	movups	%xmm1, -128(%rdi)
	movq	$0, -112(%rdi)
	movups	%xmm1, -96(%rdi)
	movq	$0, -80(%rdi)
	movups	%xmm1, -64(%rdi)
	movq	$0, -48(%rdi)
	movq	%rsi, -40(%rdi)
	movl	$100, %esi
	movq	%r13, -32(%rdi)
	movq	$1, -24(%rdi)
	movq	$0, -16(%rdi)
	movq	$0, -8(%rdi)
	movl	$0x3f800000, (%rdi)
	movq	$0, 8(%rdi)
	movq	$0, 16(%rdi)
	call	_ZNKSt8__detail20_Prime_rehash_policy11_M_next_bktEm@PLT
	cmpq	312(%rbx), %rax
	jbe	.L480
	movq	%rax, %r12
	cmpq	$1, %rax
	je	.L486
	movq	296(%rbx), %rdi
	leaq	0(,%rax,8), %rdx
	movq	%rdx, %rsi
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	subq	%r13, %rax
	cmpq	%rax, %rdx
	ja	.L487
	addq	%r13, %rsi
	movq	%rsi, 16(%rdi)
.L484:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	memset@PLT
.L482:
	movq	%r13, 304(%rbx)
	movq	%r12, 312(%rbx)
.L480:
	movq	$16, 368(%rbx)
	movl	$4294967295, %eax
	movq	%rax, 360(%rbx)
	xorl	%eax, %eax
	movw	%ax, 376(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L486:
	.cfi_restore_state
	movq	$0, 352(%rbx)
	jmp	.L482
	.p2align 4,,10
	.p2align 3
.L487:
	movq	%rdx, -40(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-40(%rbp), %rdx
	movq	%rax, %r13
	jmp	.L484
	.cfi_endproc
.LFE18036:
	.size	_ZN2v88internal4wasm17WasmModuleBuilderC2EPNS0_4ZoneE, .-_ZN2v88internal4wasm17WasmModuleBuilderC2EPNS0_4ZoneE
	.globl	_ZN2v88internal4wasm17WasmModuleBuilderC1EPNS0_4ZoneE
	.set	_ZN2v88internal4wasm17WasmModuleBuilderC1EPNS0_4ZoneE,_ZN2v88internal4wasm17WasmModuleBuilderC2EPNS0_4ZoneE
	.section	.text._ZN2v88internal4wasm17WasmModuleBuilder19SetIndirectFunctionEjj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm17WasmModuleBuilder19SetIndirectFunctionEjj
	.type	_ZN2v88internal4wasm17WasmModuleBuilder19SetIndirectFunctionEjj, @function
_ZN2v88internal4wasm17WasmModuleBuilder19SetIndirectFunctionEjj:
.LFB18054:
	.cfi_startproc
	endbr64
	movq	240(%rdi), %rax
	movl	%esi, %esi
	movl	%edx, (%rax,%rsi,4)
	ret
	.cfi_endproc
.LFE18054:
	.size	_ZN2v88internal4wasm17WasmModuleBuilder19SetIndirectFunctionEjj, .-_ZN2v88internal4wasm17WasmModuleBuilder19SetIndirectFunctionEjj
	.section	.text._ZN2v88internal4wasm17WasmModuleBuilder15SetMaxTableSizeEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm17WasmModuleBuilder15SetMaxTableSizeEj
	.type	_ZN2v88internal4wasm17WasmModuleBuilder15SetMaxTableSizeEj, @function
_ZN2v88internal4wasm17WasmModuleBuilder15SetMaxTableSizeEj:
.LFB18055:
	.cfi_startproc
	endbr64
	movl	%esi, 364(%rdi)
	movq	176(%rdi), %rax
	cmpq	184(%rdi), %rax
	je	.L489
	movl	%esi, 8(%rax)
.L489:
	ret
	.cfi_endproc
.LFE18055:
	.size	_ZN2v88internal4wasm17WasmModuleBuilder15SetMaxTableSizeEj, .-_ZN2v88internal4wasm17WasmModuleBuilder15SetMaxTableSizeEj
	.section	.text._ZN2v88internal4wasm17WasmModuleBuilder17MarkStartFunctionEPNS1_19WasmFunctionBuilderE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm17WasmModuleBuilder17MarkStartFunctionEPNS1_19WasmFunctionBuilderE
	.type	_ZN2v88internal4wasm17WasmModuleBuilder17MarkStartFunctionEPNS1_19WasmFunctionBuilderE, @function
_ZN2v88internal4wasm17WasmModuleBuilder17MarkStartFunctionEPNS1_19WasmFunctionBuilderE:
.LFB18060:
	.cfi_startproc
	endbr64
	movl	60(%rsi), %eax
	movl	%eax, 360(%rdi)
	ret
	.cfi_endproc
.LFE18060:
	.size	_ZN2v88internal4wasm17WasmModuleBuilder17MarkStartFunctionEPNS1_19WasmFunctionBuilderE, .-_ZN2v88internal4wasm17WasmModuleBuilder17MarkStartFunctionEPNS1_19WasmFunctionBuilderE
	.section	.text._ZN2v88internal4wasm17WasmModuleBuilder16SetMinMemorySizeEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm17WasmModuleBuilder16SetMinMemorySizeEj
	.type	_ZN2v88internal4wasm17WasmModuleBuilder16SetMinMemorySizeEj, @function
_ZN2v88internal4wasm17WasmModuleBuilder16SetMinMemorySizeEj:
.LFB18065:
	.cfi_startproc
	endbr64
	movl	%esi, 368(%rdi)
	ret
	.cfi_endproc
.LFE18065:
	.size	_ZN2v88internal4wasm17WasmModuleBuilder16SetMinMemorySizeEj, .-_ZN2v88internal4wasm17WasmModuleBuilder16SetMinMemorySizeEj
	.section	.text._ZN2v88internal4wasm17WasmModuleBuilder16SetMaxMemorySizeEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm17WasmModuleBuilder16SetMaxMemorySizeEj
	.type	_ZN2v88internal4wasm17WasmModuleBuilder16SetMaxMemorySizeEj, @function
_ZN2v88internal4wasm17WasmModuleBuilder16SetMaxMemorySizeEj:
.LFB18066:
	.cfi_startproc
	endbr64
	movb	$1, 376(%rdi)
	movl	%esi, 372(%rdi)
	ret
	.cfi_endproc
.LFE18066:
	.size	_ZN2v88internal4wasm17WasmModuleBuilder16SetMaxMemorySizeEj, .-_ZN2v88internal4wasm17WasmModuleBuilder16SetMaxMemorySizeEj
	.section	.text._ZN2v88internal4wasm17WasmModuleBuilder18SetHasSharedMemoryEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm17WasmModuleBuilder18SetHasSharedMemoryEv
	.type	_ZN2v88internal4wasm17WasmModuleBuilder18SetHasSharedMemoryEv, @function
_ZN2v88internal4wasm17WasmModuleBuilder18SetHasSharedMemoryEv:
.LFB18067:
	.cfi_startproc
	endbr64
	movb	$1, 377(%rdi)
	ret
	.cfi_endproc
.LFE18067:
	.size	_ZN2v88internal4wasm17WasmModuleBuilder18SetHasSharedMemoryEv, .-_ZN2v88internal4wasm17WasmModuleBuilder18SetHasSharedMemoryEv
	.section	.rodata._ZNK2v88internal4wasm17WasmModuleBuilder7WriteToEPNS1_10ZoneBufferE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"unreachable code"
.LC3:
	.string	"unimplemented code"
	.section	.text._ZNK2v88internal4wasm17WasmModuleBuilder7WriteToEPNS1_10ZoneBufferE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4wasm17WasmModuleBuilder7WriteToEPNS1_10ZoneBufferE
	.type	_ZNK2v88internal4wasm17WasmModuleBuilder7WriteToEPNS1_10ZoneBufferE, @function
_ZNK2v88internal4wasm17WasmModuleBuilder7WriteToEPNS1_10ZoneBufferE:
.LFB18068:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rcx
	movl	$4, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	%rdi, -104(%rbp)
	movq	%rcx, %rdi
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal4wasm10ZoneBuffer11EnsureSpaceEm
	movq	-88(%rbp), %rcx
	movl	$4, %esi
	movq	16(%rcx), %rax
	movq	%rcx, %rdi
	movl	$1836278016, (%rax)
	addq	$4, 16(%rcx)
	call	_ZN2v88internal4wasm10ZoneBuffer11EnsureSpaceEm
	movq	-88(%rbp), %rcx
	movq	16(%rcx), %rax
	movl	$1, (%rax)
	addq	$4, 16(%rcx)
	movq	24(%rbx), %rax
	cmpq	%rax, 16(%rbx)
	je	.L496
	movq	%rcx, %rsi
	movl	$1, %edi
	leaq	.L510(%rip), %r15
	call	_ZN2v88internal4wasm12_GLOBAL__N_111EmitSectionENS1_11SectionCodeEPNS1_10ZoneBufferE
	movq	-88(%rbp), %rcx
	movq	24(%rbx), %rsi
	leaq	.L533(%rip), %r14
	subq	16(%rbx), %rsi
	movq	%rax, -136(%rbp)
	movq	%rcx, %rdi
	sarq	$3, %rsi
	call	_ZN2v88internal4wasm10ZoneBuffer10write_sizeEm
	movq	24(%rbx), %rax
	movq	16(%rbx), %rdi
	movq	-88(%rbp), %rcx
	cmpq	%rax, %rdi
	movq	%rax, -128(%rbp)
	movq	%rdi, -112(%rbp)
	je	.L531
	movq	%rcx, %r13
	.p2align 4,,10
	.p2align 3
.L530:
	movq	-112(%rbp), %rax
	movq	16(%r13), %rdx
	movq	(%rax), %r12
	movq	24(%r13), %rax
	leaq	1(%rdx), %rsi
	cmpq	%rax, %rsi
	ja	.L928
.L498:
	movq	%rsi, 16(%r13)
	movb	$96, (%rdx)
	movq	16(%r13), %rdx
	movq	24(%r13), %rax
	movq	8(%r12), %r8
	leaq	5(%rdx), %rsi
	cmpq	%rsi, %rax
	jb	.L929
.L501:
	movl	%r8d, %eax
	cmpl	$127, %r8d
	jbe	.L504
	leaq	1(%rdx), %rax
	movq	%rax, 16(%r13)
	movl	%r8d, %eax
	orl	$-128, %eax
	movb	%al, (%rdx)
	movl	%r8d, %eax
	shrl	$7, %eax
	cmpl	$16383, %r8d
	jbe	.L506
	.p2align 4,,10
	.p2align 3
.L505:
	movq	16(%r13), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 16(%r13)
	movl	%eax, %ecx
	shrl	$7, %eax
	orl	$-128, %ecx
	movb	%cl, (%rdx)
	cmpl	$127, %eax
	ja	.L505
.L506:
	movq	16(%r13), %rdx
.L504:
	leaq	1(%rdx), %rsi
	movq	%rsi, 16(%r13)
	movb	%al, (%rdx)
	movq	(%r12), %r9
	movq	8(%r12), %rax
	movq	16(%r12), %r10
	addq	%r9, %rax
	leaq	(%r10,%r9), %r8
	addq	%rax, %r10
	cmpq	%r8, %r10
	je	.L507
	movq	%r12, -120(%rbp)
	movq	%r10, %r12
	.p2align 4,,10
	.p2align 3
.L522:
	cmpb	$9, (%r8)
	ja	.L508
	movzbl	(%r8), %eax
	movslq	(%r15,%rax,4), %rax
	addq	%r15, %rax
	notrack jmp	*%rax
	.section	.rodata._ZNK2v88internal4wasm17WasmModuleBuilder7WriteToEPNS1_10ZoneBufferE,"a",@progbits
	.align 4
	.align 4
.L510:
	.long	.L518-.L510
	.long	.L517-.L510
	.long	.L516-.L510
	.long	.L805-.L510
	.long	.L514-.L510
	.long	.L513-.L510
	.long	.L512-.L510
	.long	.L511-.L510
	.long	.L508-.L510
	.long	.L509-.L510
	.section	.text._ZNK2v88internal4wasm17WasmModuleBuilder7WriteToEPNS1_10ZoneBufferE
	.p2align 4,,10
	.p2align 3
.L532:
	movl	$104, %ecx
	.p2align 4,,10
	.p2align 3
.L538:
	movq	16(%r13), %rdx
	movq	24(%r13), %rsi
	leaq	1(%rdx), %rax
	cmpq	%rsi, %rax
	ja	.L930
.L542:
	addq	$1, %r12
	movq	%rax, 16(%r13)
	movb	%cl, (%rdx)
	cmpq	%r12, %r10
	jne	.L546
.L545:
	addq	$8, -112(%rbp)
	movq	-112(%rbp), %rax
	cmpq	%rax, -128(%rbp)
	jne	.L530
	movq	%r13, %rcx
.L531:
	movq	8(%rcx), %rdx
	movq	16(%rcx), %rax
	movq	-136(%rbp), %rdi
	subq	%rdx, %rax
	subl	$5, %eax
	addq	%rdi, %rdx
	subl	%edi, %eax
	movl	%eax, %esi
	orl	$-128, %esi
	movb	%sil, (%rdx)
	movl	%eax, %esi
	shrl	$7, %esi
	orl	$-128, %esi
	movb	%sil, 1(%rdx)
	movl	%eax, %esi
	shrl	$14, %esi
	orl	$-128, %esi
	movb	%sil, 2(%rdx)
	movl	%eax, %esi
	shrl	$28, %eax
	shrl	$21, %esi
	movb	%al, 4(%rdx)
	orl	$-128, %esi
	movb	%sil, 3(%rdx)
.L496:
	movabsq	$-6148914691236517205, %rbx
	movq	-104(%rbp), %rdi
	movq	88(%rdi), %rax
	movq	56(%rdi), %rdx
	subq	80(%rdi), %rax
	subq	48(%rdi), %rdx
	sarq	$3, %rax
	sarq	$3, %rdx
	addq	%rdx, %rax
	imulq	%rbx, %rax
	testq	%rax, %rax
	jne	.L931
.L547:
	movq	-104(%rbp), %rbx
	xorl	%r15d, %r15d
	movl	%r15d, -136(%rbp)
	movq	152(%rbx), %rax
	cmpq	%rax, 144(%rbx)
	je	.L593
	movq	%rcx, %rsi
	movl	$3, %edi
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal4wasm12_GLOBAL__N_111EmitSectionENS1_11SectionCodeEPNS1_10ZoneBufferE
	movq	-88(%rbp), %rcx
	movq	152(%rbx), %rsi
	subq	144(%rbx), %rsi
	movq	%rax, -112(%rbp)
	sarq	$3, %rsi
	movq	%rcx, %rdi
	call	_ZN2v88internal4wasm10ZoneBuffer10write_sizeEm
	movq	144(%rbx), %r14
	movq	152(%rbx), %r12
	movq	-88(%rbp), %rcx
	cmpq	%r12, %r14
	je	.L594
	movq	%rcx, %r13
	movq	%r12, %r8
	movl	%r15d, %ecx
	.p2align 4,,10
	.p2align 3
.L602:
	movq	16(%r13), %rax
	movq	(%r14), %r15
	movq	24(%r13), %rdx
	leaq	5(%rax), %rsi
	movl	56(%r15), %ebx
	cmpq	%rsi, %rdx
	jnb	.L924
	jmp	.L932
	.p2align 4,,10
	.p2align 3
.L908:
	movl	%ebx, %edx
	shrl	$7, %ebx
	orl	$-128, %edx
	movb	%dl, (%rax)
	movq	16(%r13), %rax
.L924:
	leaq	1(%rax), %rdx
	movq	%rdx, 16(%r13)
	cmpl	$127, %ebx
	ja	.L908
	movb	%bl, (%rax)
	cmpq	$1, 104(%r15)
	sbbl	$-1, %ecx
	addq	$8, %r14
	cmpq	%r14, %r8
	jne	.L602
	movl	%ecx, -136(%rbp)
	movq	%r13, %rcx
.L594:
	movq	8(%rcx), %rdx
	movq	16(%rcx), %rax
	movq	-112(%rbp), %rdi
	subq	%rdx, %rax
	subl	$5, %eax
	subl	%edi, %eax
	addq	%rdx, %rdi
	movl	%eax, %edx
	orl	$-128, %edx
	movb	%dl, (%rdi)
	movl	%eax, %edx
	shrl	$7, %edx
	orl	$-128, %edx
	movb	%dl, 1(%rdi)
	movl	%eax, %edx
	shrl	$14, %edx
	orl	$-128, %edx
	movb	%dl, 2(%rdi)
	movl	%eax, %edx
	shrl	$28, %eax
	shrl	$21, %edx
	movb	%al, 4(%rdi)
	orl	$-128, %edx
	movb	%dl, 3(%rdi)
.L593:
	movq	-104(%rbp), %rbx
	movq	184(%rbx), %rax
	cmpq	%rax, 176(%rbx)
	je	.L603
	movq	%rcx, %rsi
	movl	$4, %edi
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal4wasm12_GLOBAL__N_111EmitSectionENS1_11SectionCodeEPNS1_10ZoneBufferE
	movq	-88(%rbp), %rcx
	movq	184(%rbx), %rsi
	subq	176(%rbx), %rsi
	movq	%rax, -96(%rbp)
	sarq	$4, %rsi
	movq	%rcx, %rdi
	call	_ZN2v88internal4wasm10ZoneBuffer10write_sizeEm
	movq	184(%rbx), %r12
	movq	176(%rbx), %r14
	leaq	.L606(%rip), %rbx
	movq	-88(%rbp), %rcx
	cmpq	%r14, %r12
	je	.L629
	movq	%r12, -88(%rbp)
	movq	%rcx, %r15
	movq	%r14, %r12
	.p2align 4,,10
	.p2align 3
.L628:
	cmpb	$9, (%r12)
	ja	.L508
	movzbl	(%r12), %eax
	movslq	(%rbx,%rax,4), %rax
	addq	%rbx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZNK2v88internal4wasm17WasmModuleBuilder7WriteToEPNS1_10ZoneBufferE
	.align 4
	.align 4
.L606:
	.long	.L614-.L606
	.long	.L613-.L606
	.long	.L612-.L606
	.long	.L809-.L606
	.long	.L610-.L606
	.long	.L609-.L606
	.long	.L608-.L606
	.long	.L607-.L606
	.long	.L508-.L606
	.long	.L605-.L606
	.section	.text._ZNK2v88internal4wasm17WasmModuleBuilder7WriteToEPNS1_10ZoneBufferE
	.p2align 4,,10
	.p2align 3
.L605:
	movl	$104, %r14d
	.p2align 4,,10
	.p2align 3
.L611:
	movq	16(%r15), %rdx
	movq	24(%r15), %rax
	leaq	1(%rdx), %rsi
	cmpq	%rax, %rsi
	ja	.L933
.L615:
	movq	%rsi, 16(%r15)
	movb	%r14b, (%rdx)
	movq	16(%r15), %rdx
	movq	24(%r15), %rax
	movzbl	12(%r12), %r14d
	leaq	1(%rdx), %rsi
	cmpq	%rax, %rsi
	ja	.L934
.L618:
	movq	%rsi, 16(%r15)
	movb	%r14b, (%rdx)
	movq	16(%r15), %rax
	movq	24(%r15), %rdx
	movl	4(%r12), %r13d
	leaq	5(%rax), %rsi
	cmpq	%rsi, %rdx
	jnb	.L925
	jmp	.L935
	.p2align 4,,10
	.p2align 3
.L910:
	movl	%r13d, %edx
	shrl	$7, %r13d
	orl	$-128, %edx
	movb	%dl, (%rax)
	movq	16(%r15), %rax
.L925:
	leaq	1(%rax), %rdx
	movq	%rdx, 16(%r15)
	cmpl	$127, %r13d
	ja	.L910
	movb	%r13b, (%rax)
	cmpb	$0, 12(%r12)
	jne	.L936
.L627:
	addq	$16, %r12
	cmpq	%r12, -88(%rbp)
	jne	.L628
	movq	%r15, %rcx
.L629:
	movq	8(%rcx), %rdx
	movq	16(%rcx), %rax
	movq	-96(%rbp), %rdi
	subq	%rdx, %rax
	subl	$5, %eax
	subl	%edi, %eax
	addq	%rdx, %rdi
	movl	%eax, %edx
	orl	$-128, %edx
	movb	%dl, (%rdi)
	movl	%eax, %edx
	shrl	$7, %edx
	orl	$-128, %edx
	movb	%dl, 1(%rdi)
	movl	%eax, %edx
	shrl	$14, %edx
	orl	$-128, %edx
	movb	%dl, 2(%rdi)
	movl	%eax, %edx
	shrl	$28, %eax
	shrl	$21, %edx
	movb	%al, 4(%rdi)
	orl	$-128, %edx
	movb	%dl, 3(%rdi)
.L603:
	movq	%rcx, %rsi
	movl	$5, %edi
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal4wasm12_GLOBAL__N_111EmitSectionENS1_11SectionCodeEPNS1_10ZoneBufferE
	movq	-88(%rbp), %rcx
	movq	%rax, %rbx
	movq	%rcx, %rdi
	call	_ZN2v88internal4wasm10ZoneBuffer11EnsureSpaceEm.constprop.0
	movq	-88(%rbp), %rcx
	movq	16(%rcx), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movb	$1, (%rax)
	movq	-104(%rbp), %rax
	cmpb	$0, 377(%rax)
	jne	.L937
	movq	%rcx, -88(%rbp)
	movzbl	376(%rax), %r12d
	movq	%rcx, %rdi
.L911:
	call	_ZN2v88internal4wasm10ZoneBuffer11EnsureSpaceEm.constprop.0
	movq	-88(%rbp), %rcx
	movq	16(%rcx), %rax
	movq	%rcx, %rdi
	leaq	1(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movb	%r12b, (%rax)
	movq	-104(%rbp), %r15
	movq	%rcx, -88(%rbp)
	movl	368(%r15), %esi
	call	_ZN2v88internal4wasm10ZoneBuffer10write_u32vEj
	cmpb	$0, 376(%r15)
	movq	-88(%rbp), %rcx
	jne	.L938
.L633:
	movq	8(%rcx), %rdx
	movq	16(%rcx), %rax
	subq	%rdx, %rax
	subl	$5, %eax
	subl	%ebx, %eax
	addq	%rdx, %rbx
	movl	%eax, %edx
	orl	$-128, %edx
	movb	%dl, (%rbx)
	movl	%eax, %edx
	shrl	$7, %edx
	orl	$-128, %edx
	movb	%dl, 1(%rbx)
	movl	%eax, %edx
	shrl	$14, %edx
	orl	$-128, %edx
	movb	%dl, 2(%rbx)
	movl	%eax, %edx
	shrl	$28, %eax
	shrl	$21, %edx
	movb	%al, 4(%rbx)
	orl	$-128, %edx
	movb	%dl, 3(%rbx)
	movq	-104(%rbp), %rbx
	movq	280(%rbx), %rax
	cmpq	%rax, 272(%rbx)
	je	.L634
	movq	%rcx, %rsi
	movl	$6, %edi
	movq	%rcx, -88(%rbp)
	leaq	.L637(%rip), %r12
	call	_ZN2v88internal4wasm12_GLOBAL__N_111EmitSectionENS1_11SectionCodeEPNS1_10ZoneBufferE
	movq	280(%rbx), %rsi
	movq	-88(%rbp), %rcx
	subq	272(%rbx), %rsi
	movq	%rax, -144(%rbp)
	movabsq	$-6148914691236517205, %rax
	sarq	$3, %rsi
	movq	%rcx, %rdi
	movq	%rcx, -96(%rbp)
	imulq	%rax, %rsi
	call	_ZN2v88internal4wasm10ZoneBuffer10write_sizeEm
	movq	280(%rbx), %rax
	movq	272(%rbx), %r14
	leaq	.L654(%rip), %rbx
	movq	-96(%rbp), %rcx
	cmpq	%rax, %r14
	movq	%rax, -88(%rbp)
	je	.L688
	movq	%r14, %r15
	movq	%rcx, %r13
	.p2align 4,,10
	.p2align 3
.L687:
	movdqu	(%r15), %xmm2
	movq	16(%r15), %rax
	movzbl	(%r15), %r9d
	movzbl	1(%r15), %r10d
	movq	%rax, -64(%rbp)
	movl	8(%r15), %r8d
	movaps	%xmm2, -80(%rbp)
	cmpb	$9, %r9b
	ja	.L508
	movzbl	%r9b, %eax
	movslq	(%r12,%rax,4), %rax
	addq	%r12, %rax
	notrack jmp	*%rax
	.section	.rodata._ZNK2v88internal4wasm17WasmModuleBuilder7WriteToEPNS1_10ZoneBufferE
	.align 4
	.align 4
.L637:
	.long	.L645-.L637
	.long	.L644-.L637
	.long	.L643-.L637
	.long	.L811-.L637
	.long	.L641-.L637
	.long	.L640-.L637
	.long	.L639-.L637
	.long	.L638-.L637
	.long	.L508-.L637
	.long	.L636-.L637
	.section	.text._ZNK2v88internal4wasm17WasmModuleBuilder7WriteToEPNS1_10ZoneBufferE
	.p2align 4,,10
	.p2align 3
.L636:
	movl	$104, %r11d
	.p2align 4,,10
	.p2align 3
.L642:
	movq	16(%r13), %rdx
	movq	24(%r13), %rax
	leaq	1(%rdx), %rsi
	cmpq	%rax, %rsi
	ja	.L939
.L646:
	movq	%rsi, 16(%r13)
	movb	%r11b, (%rdx)
	movq	16(%r13), %rdx
	movq	24(%r13), %rax
	leaq	1(%rdx), %rsi
	cmpq	%rax, %rsi
	ja	.L940
.L649:
	movq	%rsi, 16(%r13)
	movb	%r10b, (%rdx)
	cmpl	$7, %r8d
	ja	.L652
	movslq	(%rbx,%r8,4), %rax
	addq	%rbx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZNK2v88internal4wasm17WasmModuleBuilder7WriteToEPNS1_10ZoneBufferE
	.align 4
	.align 4
.L654:
	.long	.L661-.L654
	.long	.L660-.L654
	.long	.L659-.L654
	.long	.L658-.L654
	.long	.L657-.L654
	.long	.L656-.L654
	.long	.L655-.L654
	.long	.L653-.L654
	.section	.text._ZNK2v88internal4wasm17WasmModuleBuilder7WriteToEPNS1_10ZoneBufferE
	.p2align 4,,10
	.p2align 3
.L655:
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm10ZoneBuffer11EnsureSpaceEm.constprop.0
	movq	16(%r13), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 16(%r13)
	movb	$-48, (%rax)
	movq	16(%r13), %rdx
	.p2align 4,,10
	.p2align 3
.L665:
	movq	24(%r13), %rax
	leaq	1(%rdx), %rsi
	cmpq	%rax, %rsi
	ja	.L941
.L684:
	movq	%rsi, 16(%r13)
	addq	$24, %r15
	movb	$11, (%rdx)
	cmpq	%r15, -88(%rbp)
	jne	.L687
	movq	%r13, %rcx
.L688:
	movq	8(%rcx), %rdx
	movq	16(%rcx), %rax
	movq	-144(%rbp), %rdi
	subq	%rdx, %rax
	subl	$5, %eax
	addq	%rdi, %rdx
	subl	%edi, %eax
	movl	%eax, %esi
	orl	$-128, %esi
	movb	%sil, (%rdx)
	movl	%eax, %esi
	shrl	$7, %esi
	orl	$-128, %esi
	movb	%sil, 1(%rdx)
	movl	%eax, %esi
	shrl	$14, %esi
	orl	$-128, %esi
	movb	%sil, 2(%rdx)
	movl	%eax, %esi
	shrl	$28, %eax
	shrl	$21, %esi
	movb	%al, 4(%rdx)
	orl	$-128, %esi
	movb	%sil, 3(%rdx)
.L634:
	movq	-104(%rbp), %r15
	movq	120(%r15), %rax
	cmpq	%rax, 112(%r15)
	je	.L689
	movq	%rcx, %rsi
	movl	$7, %edi
	movq	%rcx, -88(%rbp)
	movabsq	$-6148914691236517205, %rbx
	call	_ZN2v88internal4wasm12_GLOBAL__N_111EmitSectionENS1_11SectionCodeEPNS1_10ZoneBufferE
	movq	120(%r15), %rsi
	subq	112(%r15), %rsi
	sarq	$3, %rsi
	movq	-88(%rbp), %rcx
	movq	%rax, -128(%rbp)
	imulq	%rbx, %rsi
	movq	%rcx, %rdi
	call	_ZN2v88internal4wasm10ZoneBuffer10write_sizeEm
	movq	120(%r15), %rax
	movq	112(%r15), %r14
	movq	%rbx, -120(%rbp)
	movq	-88(%rbp), %rcx
	cmpq	%rax, %r14
	movq	%rax, -96(%rbp)
	je	.L709
	movq	%rcx, %r12
	.p2align 4,,10
	.p2align 3
.L708:
	movl	20(%r14), %eax
	movq	16(%r12), %rdx
	movq	(%r14), %r11
	movzbl	16(%r14), %r13d
	movl	%eax, -88(%rbp)
	movq	24(%r12), %rax
	leaq	5(%rdx), %rsi
	movslq	8(%r14), %rbx
	cmpq	%rsi, %rax
	jb	.L942
.L691:
	movl	%ebx, %eax
	cmpl	$127, %ebx
	jbe	.L694
	leaq	1(%rdx), %rax
	movq	%rax, 16(%r12)
	movl	%ebx, %eax
	orl	$-128, %eax
	movb	%al, (%rdx)
	movl	%ebx, %eax
	shrl	$7, %eax
	cmpl	$16383, %ebx
	jbe	.L696
	.p2align 4,,10
	.p2align 3
.L695:
	movq	16(%r12), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 16(%r12)
	movl	%eax, %ecx
	shrl	$7, %eax
	orl	$-128, %ecx
	movb	%cl, (%rdx)
	cmpl	$127, %eax
	ja	.L695
.L696:
	movq	16(%r12), %rdx
.L694:
	leaq	1(%rdx), %rsi
	movq	%rsi, 16(%r12)
	movb	%al, (%rdx)
	movq	16(%r12), %r8
	movq	24(%r12), %rdx
	leaq	(%r8,%rbx), %rsi
	cmpq	%rsi, %rdx
	jb	.L943
.L697:
	movq	%rbx, %rdx
	movq	%r8, %rdi
	movq	%r11, %rsi
	call	memcpy@PLT
	addq	16(%r12), %rbx
	movq	24(%r12), %rax
	leaq	1(%rbx), %rdx
	movq	%rbx, 16(%r12)
	movq	%rbx, %r8
	cmpq	%rax, %rdx
	ja	.L944
.L700:
	movq	%rdx, 16(%r12)
	movb	%r13b, (%r8)
	cmpb	$2, %r13b
	ja	.L703
	testb	%r13b, %r13b
	jne	.L945
	movq	-104(%rbp), %rax
	movq	56(%rax), %rsi
	subq	48(%rax), %rsi
.L918:
	sarq	$3, %rsi
	imulq	-120(%rbp), %rsi
	movslq	-88(%rbp), %rbx
	movq	%r12, %rdi
	addq	%rbx, %rsi
	call	_ZN2v88internal4wasm10ZoneBuffer10write_sizeEm
.L707:
	addq	$24, %r14
	cmpq	%r14, -96(%rbp)
	jne	.L708
	movq	%r12, %rcx
.L709:
	movq	8(%rcx), %rdx
	movq	16(%rcx), %rax
	movq	-128(%rbp), %rdi
	subq	%rdx, %rax
	subl	$5, %eax
	addq	%rdi, %rdx
	subl	%edi, %eax
	movl	%eax, %esi
	orl	$-128, %esi
	movb	%sil, (%rdx)
	movl	%eax, %esi
	shrl	$7, %esi
	orl	$-128, %esi
	movb	%sil, 1(%rdx)
	movl	%eax, %esi
	shrl	$14, %esi
	orl	$-128, %esi
	movb	%sil, 2(%rdx)
	movl	%eax, %esi
	shrl	$28, %eax
	shrl	$21, %esi
	movb	%al, 4(%rdx)
	orl	$-128, %esi
	movb	%sil, 3(%rdx)
.L689:
	movq	-104(%rbp), %r15
	movl	360(%r15), %esi
	testl	%esi, %esi
	js	.L710
	movq	%rcx, %rsi
	movl	$8, %edi
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal4wasm12_GLOBAL__N_111EmitSectionENS1_11SectionCodeEPNS1_10ZoneBufferE
	movq	56(%r15), %rsi
	subq	48(%r15), %rsi
	movq	%rax, %rbx
	sarq	$3, %rsi
	movq	-88(%rbp), %rcx
	movabsq	$-6148914691236517205, %rax
	imulq	%rax, %rsi
	movslq	360(%r15), %rax
	movq	%rcx, %rdi
	addq	%rax, %rsi
	call	_ZN2v88internal4wasm10ZoneBuffer10write_sizeEm
	movq	-88(%rbp), %rcx
	movq	8(%rcx), %rax
	movq	16(%rcx), %rdx
	subq	%rax, %rdx
	addq	%rbx, %rax
	subl	$5, %edx
	subl	%ebx, %edx
	movl	%edx, %esi
	orl	$-128, %esi
	movb	%sil, (%rax)
	movl	%edx, %esi
	shrl	$7, %esi
	orl	$-128, %esi
	movb	%sil, 1(%rax)
	movl	%edx, %esi
	shrl	$14, %esi
	orl	$-128, %esi
	movb	%sil, 2(%rax)
	movl	%edx, %esi
	shrl	$28, %edx
	shrl	$21, %esi
	movb	%dl, 4(%rax)
	orl	$-128, %esi
	movb	%sil, 3(%rax)
.L710:
	movq	-104(%rbp), %rbx
	movq	248(%rbx), %rax
	cmpq	%rax, 240(%rbx)
	je	.L711
	movq	%rcx, %rsi
	movl	$9, %edi
	movq	%rcx, -88(%rbp)
	xorl	%r13d, %r13d
	call	_ZN2v88internal4wasm12_GLOBAL__N_111EmitSectionENS1_11SectionCodeEPNS1_10ZoneBufferE
	movq	-88(%rbp), %rcx
	movq	%rax, -112(%rbp)
	movq	%rcx, %rdi
	call	_ZN2v88internal4wasm10ZoneBuffer11EnsureSpaceEm.constprop.0
	movq	-88(%rbp), %rcx
	movq	16(%rcx), %rax
	movq	%rcx, %rdi
	leaq	1(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movb	$1, (%rax)
	call	_ZN2v88internal4wasm10ZoneBuffer11EnsureSpaceEm.constprop.0
	movq	-88(%rbp), %rcx
	movq	16(%rcx), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movb	$0, (%rax)
	movq	248(%rbx), %rax
	movq	240(%rbx), %rdx
	movq	%rax, %rsi
	movq	%rax, -88(%rbp)
	subq	%rdx, %rsi
	sarq	$2, %rsi
	je	.L812
	xorl	%eax, %eax
	jmp	.L714
	.p2align 4,,10
	.p2align 3
.L946:
	leal	1(%r13), %eax
	movq	%rax, %r13
	cmpq	%rsi, %rax
	jnb	.L713
.L714:
	cmpl	$-1, (%rdx,%rax,4)
	je	.L946
.L713:
	leal	-1(%rsi), %r12d
	cmpl	%r13d, %r12d
	jnb	.L718
	jmp	.L947
	.p2align 4,,10
	.p2align 3
.L716:
	leal	-1(%r12), %r14d
	cmpl	%r13d, %r14d
	jb	.L715
	movl	%r14d, %r12d
.L718:
	movl	%r12d, %eax
	cmpl	$-1, (%rdx,%rax,4)
	je	.L716
	movl	%r12d, %r14d
	addl	$1, %r12d
.L715:
	movq	%rcx, %rdi
	movq	%rcx, -88(%rbp)
	movabsq	$-6148914691236517205, %r15
	call	_ZN2v88internal4wasm10ZoneBuffer11EnsureSpaceEm.constprop.0
	movq	-88(%rbp), %rcx
	movl	%r13d, %esi
	movq	16(%rcx), %rax
	movq	%rcx, %rdi
	leaq	1(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movb	$65, (%rax)
	call	_ZN2v88internal4wasm10ZoneBuffer10write_u32vEj
	movq	-88(%rbp), %rcx
	movq	%rcx, %rdi
	call	_ZN2v88internal4wasm10ZoneBuffer11EnsureSpaceEm.constprop.0
	movq	-88(%rbp), %rcx
	movl	%r12d, %esi
	subl	%r13d, %esi
	movq	16(%rcx), %rax
	movq	%rcx, %rdi
	leaq	1(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movb	$11, (%rax)
	call	_ZN2v88internal4wasm10ZoneBuffer10write_sizeEm
	cmpl	%r14d, %r13d
	movq	-88(%rbp), %rcx
	ja	.L725
	movq	-104(%rbp), %r12
	movl	%r14d, %r8d
	movq	%rcx, %rbx
	.p2align 4,,10
	.p2align 3
.L717:
	movq	240(%r12), %rdx
	movl	%r13d, %eax
	movl	(%rdx,%rax,4), %ecx
	movq	56(%r12), %rax
	subq	48(%r12), %rax
	movq	16(%rbx), %rdx
	sarq	$3, %rax
	imulq	%r15, %rax
	leaq	5(%rdx), %rsi
	addq	%rax, %rcx
	movq	24(%rbx), %rax
	cmpq	%rsi, %rax
	jb	.L948
.L719:
	movl	%ecx, %eax
	cmpl	$127, %ecx
	jbe	.L722
	leaq	1(%rdx), %rax
	movq	%rax, 16(%rbx)
	movl	%ecx, %eax
	orl	$-128, %eax
	movb	%al, (%rdx)
	movl	%ecx, %eax
	shrl	$7, %eax
	cmpl	$16383, %ecx
	jbe	.L724
	.p2align 4,,10
	.p2align 3
.L723:
	movq	16(%rbx), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 16(%rbx)
	movl	%eax, %ecx
	shrl	$7, %eax
	orl	$-128, %ecx
	movb	%cl, (%rdx)
	cmpl	$127, %eax
	ja	.L723
.L724:
	movq	16(%rbx), %rdx
.L722:
	leaq	1(%rdx), %rcx
	addl	$1, %r13d
	movq	%rcx, 16(%rbx)
	movb	%al, (%rdx)
	cmpl	%r8d, %r13d
	jbe	.L717
	movq	%rbx, %rcx
.L725:
	movq	8(%rcx), %rdx
	movq	16(%rcx), %rax
	movq	-112(%rbp), %rdi
	subq	%rdx, %rax
	subl	$5, %eax
	subl	%edi, %eax
	addq	%rdx, %rdi
	movl	%eax, %edx
	orl	$-128, %edx
	movb	%dl, (%rdi)
	movl	%eax, %edx
	shrl	$7, %edx
	orl	$-128, %edx
	movb	%dl, 1(%rdi)
	movl	%eax, %edx
	shrl	$14, %edx
	orl	$-128, %edx
	movb	%dl, 2(%rdi)
	movl	%eax, %edx
	shrl	$28, %eax
	shrl	$21, %edx
	movb	%al, 4(%rdi)
	orl	$-128, %edx
	movb	%dl, 3(%rdi)
.L711:
	movq	-104(%rbp), %rax
	movq	152(%rax), %rsi
	movq	144(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L729
	jmp	.L727
	.p2align 4,,10
	.p2align 3
.L949:
	addq	$8, %rax
	cmpq	%rax, %rsi
	je	.L735
.L729:
	movq	(%rax), %rdx
	cmpb	$-1, 316(%rdx)
	je	.L949
	movq	%rcx, %rdi
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal4wasm10ZoneBuffer11EnsureSpaceEm.constprop.0
	movq	-88(%rbp), %rcx
	movl	$5, %esi
	movq	16(%rcx), %rax
	movq	%rcx, %rdi
	leaq	1(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movb	$0, (%rax)
	movq	16(%rcx), %rbx
	subq	8(%rcx), %rbx
	movq	%rbx, -96(%rbp)
	call	_ZN2v88internal4wasm10ZoneBuffer11EnsureSpaceEm
	movq	-88(%rbp), %rcx
	movl	$16, %esi
	addq	$5, 16(%rcx)
	movq	%rcx, %rdi
	call	_ZN2v88internal4wasm10ZoneBuffer10write_sizeEm
	movq	-88(%rbp), %rcx
	movq	16(%rcx), %rdx
	movq	24(%rcx), %rax
	leaq	16(%rdx), %rsi
	cmpq	%rsi, %rax
	jb	.L950
.L804:
	movdqa	.LC4(%rip), %xmm0
	movq	-104(%rbp), %rbx
	movq	%rcx, %rdi
	movq	%rcx, -88(%rbp)
	movups	%xmm0, (%rdx)
	addq	$16, 16(%rcx)
	movq	152(%rbx), %rsi
	subq	144(%rbx), %rsi
	sarq	$3, %rsi
	call	_ZN2v88internal4wasm10ZoneBuffer10write_sizeEm
	movq	152(%rbx), %r15
	movq	144(%rbx), %r14
	xorl	%ebx, %ebx
	movq	-88(%rbp), %rcx
	cmpq	%r15, %r14
	je	.L742
	movq	%rcx, %r13
	movq	%r15, %rcx
	movq	%r14, %r15
	jmp	.L741
	.p2align 4,,10
	.p2align 3
.L738:
	addq	$8, %r15
	movq	%rsi, 16(%r13)
	movb	%r14b, (%rdx)
	cmpq	%r15, %rcx
	je	.L951
.L741:
	movq	(%r15), %rax
	movq	16(%r13), %rdx
	movzbl	316(%rax), %r14d
	movq	24(%r13), %rax
	leaq	1(%rdx), %rsi
	cmpb	$-1, %r14b
	cmove	%ebx, %r14d
	cmpq	%rax, %rsi
	jbe	.L738
	movq	0(%r13), %r8
	subq	8(%r13), %rax
	addq	%rax, %rax
	leaq	8(%rax), %rsi
	movq	16(%r8), %rdi
	leaq	1(%rax), %r12
	movq	24(%r8), %rax
	andq	$-8, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L952
	addq	%rdi, %rsi
	movq	%rsi, 16(%r8)
.L740:
	movq	8(%r13), %rsi
	movq	16(%r13), %rdx
	movq	%rcx, -88(%rbp)
	addq	$8, %r15
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	16(%r13), %rdx
	subq	8(%r13), %rdx
	addq	%rax, %rdx
	movq	-88(%rbp), %rcx
	addq	%rax, %r12
	movq	%rax, 8(%r13)
	leaq	1(%rdx), %rsi
	movq	%r12, 24(%r13)
	movq	%rsi, 16(%r13)
	movb	%r14b, (%rdx)
	cmpq	%r15, %rcx
	jne	.L741
.L951:
	movq	%r13, %rcx
.L742:
	movq	8(%rcx), %rdx
	movq	16(%rcx), %rax
	movq	-96(%rbp), %rdi
	subq	%rdx, %rax
	subl	$5, %eax
	subl	%edi, %eax
	addq	%rdx, %rdi
	movl	%eax, %edx
	orl	$-128, %edx
	movb	%dl, (%rdi)
	movl	%eax, %edx
	shrl	$7, %edx
	orl	$-128, %edx
	movb	%dl, 1(%rdi)
	movl	%eax, %edx
	shrl	$14, %edx
	orl	$-128, %edx
	movb	%dl, 2(%rdi)
	movl	%eax, %edx
	shrl	$28, %eax
	movb	%al, 4(%rdi)
	shrl	$21, %edx
	movq	-104(%rbp), %rax
	orl	$-128, %edx
	movb	%dl, 3(%rdi)
	movq	152(%rax), %rdi
	cmpq	%rdi, 144(%rax)
	jne	.L735
.L727:
	movq	-104(%rbp), %rbx
	movq	216(%rbx), %rax
	cmpq	%rax, 208(%rbx)
	je	.L744
	movq	%rcx, %rsi
	movl	$11, %edi
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal4wasm12_GLOBAL__N_111EmitSectionENS1_11SectionCodeEPNS1_10ZoneBufferE
	movq	216(%rbx), %rsi
	movq	-88(%rbp), %rcx
	subq	208(%rbx), %rsi
	movq	%rax, -128(%rbp)
	movabsq	$-3689348814741910323, %rax
	sarq	$3, %rsi
	movq	%rcx, %rdi
	movq	%rcx, -96(%rbp)
	imulq	%rax, %rsi
	call	_ZN2v88internal4wasm10ZoneBuffer10write_sizeEm
	movq	216(%rbx), %rax
	movq	208(%rbx), %r9
	movq	-96(%rbp), %rcx
	cmpq	%rax, %r9
	movq	%rax, -88(%rbp)
	je	.L745
	movq	%r9, %r13
	movq	%rcx, %rbx
	.p2align 4,,10
	.p2align 3
.L778:
	movq	16(%r13), %rsi
	movq	8(%r13), %rax
	xorl	%r15d, %r15d
	movq	0(%r13), %rdi
	movq	%rsi, %rdx
	subq	%rax, %rdx
	je	.L746
	movq	16(%rdi), %r15
	movq	24(%rdi), %rax
	leaq	7(%rdx), %rsi
	andq	$-8, %rsi
	subq	%r15, %rax
	cmpq	%rax, %rsi
	ja	.L953
	addq	%r15, %rsi
	movq	%rsi, 16(%rdi)
.L748:
	movq	16(%r13), %rsi
	movq	8(%r13), %rax
.L746:
	cmpq	%rax, %rsi
	je	.L817
	leaq	15(%r15), %rdx
	movq	%rsi, %r14
	subq	%rax, %rdx
	subq	%rax, %r14
	cmpq	$30, %rdx
	jbe	.L818
	leaq	-1(%r14), %rdx
	cmpq	$14, %rdx
	jbe	.L818
	movq	%r14, %rdi
	xorl	%edx, %edx
	andq	$-16, %rdi
	.p2align 4,,10
	.p2align 3
.L751:
	movdqu	(%rax,%rdx), %xmm1
	movups	%xmm1, (%r15,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rdi
	jne	.L751
	movq	%r14, %rdi
	andq	$-16, %rdi
	addq	%rdi, %rax
	leaq	(%r15,%rdi), %rdx
	cmpq	%rdi, %r14
	je	.L753
	movzbl	(%rax), %edi
	movb	%dil, (%rdx)
	leaq	1(%rax), %rdi
	cmpq	%rdi, %rsi
	je	.L753
	movzbl	1(%rax), %edi
	movb	%dil, 1(%rdx)
	leaq	2(%rax), %rdi
	cmpq	%rdi, %rsi
	je	.L753
	movzbl	2(%rax), %edi
	movb	%dil, 2(%rdx)
	leaq	3(%rax), %rdi
	cmpq	%rdi, %rsi
	je	.L753
	movzbl	3(%rax), %edi
	movb	%dil, 3(%rdx)
	leaq	4(%rax), %rdi
	cmpq	%rdi, %rsi
	je	.L753
	movzbl	4(%rax), %edi
	movb	%dil, 4(%rdx)
	leaq	5(%rax), %rdi
	cmpq	%rdi, %rsi
	je	.L753
	movzbl	5(%rax), %edi
	movb	%dil, 5(%rdx)
	leaq	6(%rax), %rdi
	cmpq	%rdi, %rsi
	je	.L753
	movzbl	6(%rax), %edi
	movb	%dil, 6(%rdx)
	leaq	7(%rax), %rdi
	cmpq	%rdi, %rsi
	je	.L753
	movzbl	7(%rax), %edi
	movb	%dil, 7(%rdx)
	leaq	8(%rax), %rdi
	cmpq	%rdi, %rsi
	je	.L753
	movzbl	8(%rax), %edi
	movb	%dil, 8(%rdx)
	leaq	9(%rax), %rdi
	cmpq	%rdi, %rsi
	je	.L753
	movzbl	9(%rax), %edi
	movb	%dil, 9(%rdx)
	leaq	10(%rax), %rdi
	cmpq	%rdi, %rsi
	je	.L753
	movzbl	10(%rax), %edi
	movb	%dil, 10(%rdx)
	leaq	11(%rax), %rdi
	cmpq	%rdi, %rsi
	je	.L753
	movzbl	11(%rax), %edi
	movb	%dil, 11(%rdx)
	leaq	12(%rax), %rdi
	cmpq	%rdi, %rsi
	je	.L753
	movzbl	12(%rax), %edi
	movb	%dil, 12(%rdx)
	leaq	13(%rax), %rdi
	cmpq	%rdi, %rsi
	je	.L753
	movzbl	13(%rax), %edi
	movb	%dil, 13(%rdx)
	leaq	14(%rax), %rdi
	cmpq	%rdi, %rsi
	je	.L753
	movzbl	14(%rax), %eax
	movb	%al, 14(%rdx)
	.p2align 4,,10
	.p2align 3
.L753:
	movl	%r14d, %r8d
	movl	%r14d, %r12d
.L749:
	movq	16(%rbx), %rdx
	movq	24(%rbx), %rax
	movl	32(%r13), %r9d
	leaq	1(%rdx), %rsi
	cmpq	%rax, %rsi
	ja	.L954
.L754:
	movq	%rsi, 16(%rbx)
	movb	$0, (%rdx)
	movq	16(%rbx), %rdx
	movq	24(%rbx), %rax
	leaq	1(%rdx), %rsi
	cmpq	%rax, %rsi
	ja	.L955
.L757:
	movq	%rsi, 16(%rbx)
	movb	$65, (%rdx)
	movq	16(%rbx), %rax
	movq	24(%rbx), %rdx
	leaq	5(%rax), %rsi
	cmpq	%rsi, %rdx
	jnb	.L927
	jmp	.L956
	.p2align 4,,10
	.p2align 3
.L920:
	movl	%r9d, %edx
	shrl	$7, %r9d
	orl	$-128, %edx
	movb	%dl, (%rax)
	movq	16(%rbx), %rax
.L927:
	leaq	1(%rax), %rdx
	movq	%rdx, 16(%rbx)
	cmpl	$127, %r9d
	ja	.L920
	movb	%r9b, (%rax)
	movq	16(%rbx), %rdx
	movq	24(%rbx), %rax
	leaq	1(%rdx), %rsi
	cmpq	%rax, %rsi
	ja	.L957
.L766:
	movq	%rsi, 16(%rbx)
	movb	$11, (%rdx)
	movq	16(%rbx), %rax
	movq	24(%rbx), %rdx
	leaq	5(%rax), %rsi
	cmpq	%rsi, %rdx
	jb	.L958
.L769:
	cmpl	$127, %r8d
	jbe	.L772
	leaq	1(%rax), %rdx
	movl	%r8d, %r12d
	movq	%rdx, 16(%rbx)
	movl	%r8d, %edx
	shrl	$7, %r12d
	orl	$-128, %edx
	movb	%dl, (%rax)
	cmpl	$16383, %r8d
	jbe	.L774
	.p2align 4,,10
	.p2align 3
.L773:
	movq	16(%rbx), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movl	%r12d, %edx
	shrl	$7, %r12d
	orl	$-128, %edx
	movb	%dl, (%rax)
	cmpl	$127, %r12d
	ja	.L773
.L774:
	movq	16(%rbx), %rax
.L772:
	leaq	1(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movb	%r12b, (%rax)
	movq	16(%rbx), %rdi
	movq	24(%rbx), %rax
	leaq	(%rdi,%r14), %rdx
	cmpq	%rdx, %rax
	jb	.L959
.L775:
	movq	%r14, %rdx
	movq	%r15, %rsi
	addq	$40, %r13
	call	memcpy@PLT
	addq	16(%rbx), %r14
	movq	%r14, 16(%rbx)
	cmpq	%r13, -88(%rbp)
	jne	.L778
	movq	%rbx, %rcx
.L779:
	movq	8(%rcx), %rdx
	movq	-128(%rbp), %rdi
	movl	$-5, %eax
	subl	%edi, %eax
	subq	%rdx, %r14
	addl	%eax, %r14d
	movq	%rdi, %rax
	addq	%rdx, %rax
	movl	%r14d, %edx
	orl	$-128, %edx
	movb	%dl, (%rax)
	movl	%r14d, %edx
	shrl	$7, %edx
	orl	$-128, %edx
	movb	%dl, 1(%rax)
	movl	%r14d, %edx
	shrl	$14, %edx
	orl	$-128, %edx
	movb	%dl, 2(%rax)
	movl	%r14d, %edx
	shrl	$28, %r14d
	shrl	$21, %edx
	movb	%r14b, 4(%rax)
	orl	$-128, %edx
	movb	%dl, 3(%rax)
.L744:
	movl	-136(%rbp), %edx
	testl	%edx, %edx
	jne	.L780
	movq	-104(%rbp), %rax
	movq	48(%rax), %rdi
	cmpq	%rdi, 56(%rax)
	je	.L495
.L780:
	movq	%rcx, %rdi
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal4wasm10ZoneBuffer11EnsureSpaceEm.constprop.0
	movq	-88(%rbp), %rcx
	movl	$5, %esi
	movq	16(%rcx), %rax
	movq	%rcx, %rdi
	leaq	1(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movb	$0, (%rax)
	movq	16(%rcx), %r14
	subq	8(%rcx), %r14
	movq	%r14, -120(%rbp)
	call	_ZN2v88internal4wasm10ZoneBuffer11EnsureSpaceEm
	movq	-88(%rbp), %rcx
	movl	$4, %esi
	addq	$5, 16(%rcx)
	movq	%rcx, %rdi
	call	_ZN2v88internal4wasm10ZoneBuffer10write_sizeEm
	movq	-88(%rbp), %rcx
	movl	$4, %esi
	movq	%rcx, %rdi
	call	_ZN2v88internal4wasm10ZoneBuffer11EnsureSpaceEm
	movq	-88(%rbp), %rcx
	movq	16(%rcx), %rax
	movq	%rcx, %rdi
	movl	$1701667182, (%rax)
	addq	$4, 16(%rcx)
	call	_ZN2v88internal4wasm10ZoneBuffer11EnsureSpaceEm.constprop.0
	movq	-88(%rbp), %rcx
	movl	$5, %esi
	movq	16(%rcx), %rax
	movq	%rcx, %rdi
	leaq	1(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movb	$1, (%rax)
	movq	16(%rcx), %r15
	subq	8(%rcx), %r15
	movq	%r15, -112(%rbp)
	call	_ZN2v88internal4wasm10ZoneBuffer11EnsureSpaceEm
	movq	-88(%rbp), %rcx
	movq	-104(%rbp), %r15
	movabsq	$-6148914691236517205, %rax
	addq	$5, 16(%rcx)
	movq	56(%r15), %r12
	movq	%rcx, %rdi
	subq	48(%r15), %r12
	sarq	$3, %r12
	imulq	%rax, %r12
	movl	-136(%rbp), %eax
	leal	(%rax,%r12), %esi
	movl	%r12d, %ebx
	call	_ZN2v88internal4wasm10ZoneBuffer10write_sizeEm
	testl	%r12d, %r12d
	movq	-88(%rbp), %rcx
	je	.L782
	leal	-1(%r12), %edi
	xorl	%r14d, %r14d
	movq	16(%rcx), %rax
	movl	%r12d, -128(%rbp)
	movq	%rdi, -88(%rbp)
	movq	%r14, %rbx
	movq	%rcx, %r12
	.p2align 4,,10
	.p2align 3
.L798:
	movq	48(%r15), %rsi
	leaq	(%rbx,%rbx,2), %rdx
	movl	%ebx, %ecx
	leaq	(%rsi,%rdx,8), %r13
	movq	24(%r12), %rdx
	leaq	5(%rax), %rsi
	cmpq	%rsi, %rdx
	jb	.L960
.L783:
	cmpq	$127, %rbx
	jbe	.L786
	leaq	1(%rax), %rdx
	movq	%rdx, 16(%r12)
	movl	%ebx, %edx
	jmp	.L922
	.p2align 4,,10
	.p2align 3
.L787:
	leaq	1(%rax), %rdx
	movq	%rdx, 16(%r12)
	movl	%ecx, %edx
.L922:
	orl	$-128, %edx
	shrl	$7, %ecx
	movb	%dl, (%rax)
	movq	16(%r12), %rax
	cmpl	$127, %ecx
	ja	.L787
.L786:
	leaq	1(%rax), %rdx
	movq	%rdx, 16(%r12)
	movb	%cl, (%rax)
	movq	16(%r12), %rdx
	movq	24(%r12), %rax
	movq	0(%r13), %r9
	leaq	5(%rdx), %rsi
	movslq	8(%r13), %r13
	cmpq	%rsi, %rax
	jb	.L961
.L789:
	movl	%r13d, %eax
	cmpl	$127, %r13d
	jbe	.L792
	leaq	1(%rdx), %rax
	movq	%rax, 16(%r12)
	movl	%r13d, %eax
	orl	$-128, %eax
	movb	%al, (%rdx)
	movl	%r13d, %eax
	shrl	$7, %eax
	cmpl	$16383, %r13d
	jbe	.L794
	.p2align 4,,10
	.p2align 3
.L793:
	movq	16(%r12), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 16(%r12)
	movl	%eax, %ecx
	shrl	$7, %eax
	orl	$-128, %ecx
	movb	%cl, (%rdx)
	cmpl	$127, %eax
	ja	.L793
.L794:
	movq	16(%r12), %rdx
.L792:
	leaq	1(%rdx), %rsi
	movq	%rsi, 16(%r12)
	movb	%al, (%rdx)
	movq	16(%r12), %rdi
	movq	24(%r12), %rax
	leaq	(%rdi,%r13), %rdx
	cmpq	%rdx, %rax
	jb	.L962
.L795:
	movq	%r13, %rdx
	movq	%r9, %rsi
	call	memcpy@PLT
	movq	16(%r12), %rax
	leaq	1(%rbx), %rdx
	addq	%r13, %rax
	movq	%rax, 16(%r12)
	cmpq	%rbx, -88(%rbp)
	je	.L963
	movq	%rdx, %rbx
	jmp	.L798
	.p2align 4,,10
	.p2align 3
.L656:
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm10ZoneBuffer11EnsureSpaceEm.constprop.0
	movq	16(%r13), %rax
	movq	%r13, %rdi
	movq	-64(%rbp), %r14
	movl	$8, %esi
	leaq	1(%rax), %rdx
	movq	%rdx, 16(%r13)
	movb	$68, (%rax)
	call	_ZN2v88internal4wasm10ZoneBuffer11EnsureSpaceEm
	movq	16(%r13), %rax
	movq	%r14, (%rax)
	movq	16(%r13), %rax
	leaq	8(%rax), %rdx
	movq	24(%r13), %rax
	leaq	1(%rdx), %rsi
	movq	%rdx, 16(%r13)
	cmpq	%rax, %rsi
	jbe	.L684
	.p2align 4,,10
	.p2align 3
.L941:
	movq	0(%r13), %r8
	subq	8(%r13), %rax
	addq	%rax, %rax
	movq	16(%r8), %rdi
	movq	24(%r8), %rdx
	leaq	1(%rax), %r14
	addq	$8, %rax
	andq	$-8, %rax
	subq	%rdi, %rdx
	cmpq	%rdx, %rax
	ja	.L964
	addq	%rdi, %rax
	movq	%rax, 16(%r8)
.L686:
	movq	8(%r13), %rsi
	movq	16(%r13), %rdx
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	16(%r13), %rdx
	subq	8(%r13), %rdx
	addq	%rax, %rdx
	addq	%rax, %r14
	movq	%rax, 8(%r13)
	movq	%r14, 24(%r13)
	leaq	1(%rdx), %rsi
	jmp	.L684
	.p2align 4,,10
	.p2align 3
.L657:
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm10ZoneBuffer11EnsureSpaceEm.constprop.0
	movq	16(%r13), %rax
	movq	%r13, %rdi
	movl	-64(%rbp), %r14d
	movl	$4, %esi
	leaq	1(%rax), %rdx
	movq	%rdx, 16(%r13)
	movb	$67, (%rax)
	call	_ZN2v88internal4wasm10ZoneBuffer11EnsureSpaceEm
	movq	16(%r13), %rax
	movl	%r14d, (%rax)
	movq	16(%r13), %rax
	leaq	4(%rax), %rdx
	movq	%rdx, 16(%r13)
	jmp	.L665
	.p2align 4,,10
	.p2align 3
.L658:
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm10ZoneBuffer11EnsureSpaceEm.constprop.0
	movq	16(%r13), %rax
	movq	-64(%rbp), %r14
	movq	%r13, %rdi
	movl	$10, %esi
	leaq	1(%rax), %rdx
	movq	%rdx, 16(%r13)
	movb	$66, (%rax)
	call	_ZN2v88internal4wasm10ZoneBuffer11EnsureSpaceEm
	testq	%r14, %r14
	jns	.L913
	jmp	.L965
	.p2align 4,,10
	.p2align 3
.L672:
	movq	16(%r13), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 16(%r13)
	movl	%r14d, %edx
	sarq	$7, %r14
	orl	$-128, %edx
	movb	%dl, (%rax)
.L913:
	cmpq	$63, %r14
	jg	.L672
.L917:
	movq	16(%r13), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 16(%r13)
	movb	%r14b, (%rax)
	movq	16(%r13), %rdx
	jmp	.L665
	.p2align 4,,10
	.p2align 3
.L659:
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm10ZoneBuffer11EnsureSpaceEm.constprop.0
	movq	16(%r13), %rax
	movl	-64(%rbp), %r14d
	movq	%r13, %rdi
	movl	$5, %esi
	leaq	1(%rax), %rdx
	movq	%rdx, 16(%r13)
	movb	$65, (%rax)
	call	_ZN2v88internal4wasm10ZoneBuffer11EnsureSpaceEm
	testl	%r14d, %r14d
	jns	.L912
	jmp	.L966
	.p2align 4,,10
	.p2align 3
.L667:
	movq	16(%r13), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 16(%r13)
	movl	%r14d, %edx
	sarl	$7, %r14d
	orl	$-128, %edx
	movb	%dl, (%rax)
.L912:
	cmpl	$63, %r14d
	jg	.L667
	jmp	.L917
	.p2align 4,,10
	.p2align 3
.L660:
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm10ZoneBuffer11EnsureSpaceEm.constprop.0
	movq	16(%r13), %rax
	movl	-64(%rbp), %r14d
	leaq	1(%rax), %rdx
	movq	%rdx, 16(%r13)
	movb	$35, (%rax)
	movq	16(%r13), %rax
	movq	24(%r13), %rdx
	leaq	5(%rax), %rsi
	cmpq	%rsi, %rdx
	jnb	.L926
	jmp	.L967
	.p2align 4,,10
	.p2align 3
.L915:
	movl	%r14d, %edx
	shrl	$7, %r14d
	orl	$-128, %edx
	movb	%dl, (%rax)
	movq	16(%r13), %rax
.L926:
	leaq	1(%rax), %rdx
	movq	%rdx, 16(%r13)
	cmpl	$127, %r14d
	ja	.L915
	movb	%r14b, (%rax)
	movq	16(%r13), %rdx
	jmp	.L665
	.p2align 4,,10
	.p2align 3
.L661:
	cmpb	$3, %r9b
	je	.L679
	ja	.L680
	cmpb	$1, %r9b
	jne	.L968
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm10ZoneBuffer11EnsureSpaceEm.constprop.0
	movq	16(%r13), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 16(%r13)
	movb	$65, (%rax)
.L916:
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm10ZoneBuffer11EnsureSpaceEm.constprop.0
	movq	16(%r13), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 16(%r13)
	movb	$0, (%rax)
	movq	16(%r13), %rdx
	jmp	.L665
	.p2align 4,,10
	.p2align 3
.L638:
	movl	$112, %r11d
	jmp	.L642
	.p2align 4,,10
	.p2align 3
.L640:
	movl	$123, %r11d
	jmp	.L642
	.p2align 4,,10
	.p2align 3
.L641:
	movl	$124, %r11d
	jmp	.L642
	.p2align 4,,10
	.p2align 3
.L644:
	movl	$127, %r11d
	jmp	.L642
	.p2align 4,,10
	.p2align 3
.L645:
	movl	$64, %r11d
	jmp	.L642
	.p2align 4,,10
	.p2align 3
.L639:
	movl	$111, %r11d
	jmp	.L642
	.p2align 4,,10
	.p2align 3
.L811:
	movl	$125, %r11d
	jmp	.L642
	.p2align 4,,10
	.p2align 3
.L643:
	movl	$126, %r11d
	jmp	.L642
	.p2align 4,,10
	.p2align 3
.L937:
	cmpb	$0, 376(%rax)
	movq	%rcx, %rdi
	setne	%r12b
	addl	$2, %r12d
	jmp	.L911
	.p2align 4,,10
	.p2align 3
.L509:
	movl	$104, %ecx
	.p2align 4,,10
	.p2align 3
.L515:
	movq	16(%r13), %rdx
	movq	24(%r13), %rsi
	leaq	1(%rdx), %rax
	cmpq	%rsi, %rax
	ja	.L969
.L519:
	addq	$1, %r8
	movq	%rax, 16(%r13)
	movb	%cl, (%rdx)
	cmpq	%r8, %r12
	jne	.L522
	movq	-120(%rbp), %r12
	movq	(%r12), %r9
.L507:
	movq	16(%r13), %rdx
	movq	24(%r13), %rax
	leaq	5(%rdx), %rsi
	cmpq	%rsi, %rax
	jb	.L970
.L523:
	movl	%r9d, %eax
	cmpl	$127, %r9d
	jbe	.L526
	leaq	1(%rdx), %rax
	movq	%rax, 16(%r13)
	movl	%r9d, %eax
	orl	$-128, %eax
	movb	%al, (%rdx)
	movl	%r9d, %eax
	shrl	$7, %eax
	cmpl	$16383, %r9d
	jbe	.L528
	.p2align 4,,10
	.p2align 3
.L527:
	movq	16(%r13), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 16(%r13)
	movl	%eax, %ecx
	shrl	$7, %eax
	orl	$-128, %ecx
	movb	%cl, (%rdx)
	cmpl	$127, %eax
	ja	.L527
.L528:
	movq	16(%r13), %rdx
.L526:
	leaq	1(%rdx), %rsi
	movq	%rsi, 16(%r13)
	movb	%al, (%rdx)
	movq	16(%r12), %r8
	movq	(%r12), %r10
	movq	%r8, %r12
	addq	%r8, %r10
	cmpq	%r8, %r10
	je	.L545
	.p2align 4,,10
	.p2align 3
.L546:
	cmpb	$9, (%r12)
	ja	.L508
	movzbl	(%r12), %eax
	movslq	(%r14,%rax,4), %rax
	addq	%r14, %rax
	notrack jmp	*%rax
	.section	.rodata._ZNK2v88internal4wasm17WasmModuleBuilder7WriteToEPNS1_10ZoneBufferE
	.align 4
	.align 4
.L533:
	.long	.L541-.L533
	.long	.L540-.L533
	.long	.L539-.L533
	.long	.L806-.L533
	.long	.L537-.L533
	.long	.L536-.L533
	.long	.L535-.L533
	.long	.L534-.L533
	.long	.L508-.L533
	.long	.L532-.L533
	.section	.text._ZNK2v88internal4wasm17WasmModuleBuilder7WriteToEPNS1_10ZoneBufferE
	.p2align 4,,10
	.p2align 3
.L511:
	movl	$112, %ecx
	jmp	.L515
	.p2align 4,,10
	.p2align 3
.L513:
	movl	$123, %ecx
	jmp	.L515
	.p2align 4,,10
	.p2align 3
.L514:
	movl	$124, %ecx
	jmp	.L515
	.p2align 4,,10
	.p2align 3
.L517:
	movl	$127, %ecx
	jmp	.L515
	.p2align 4,,10
	.p2align 3
.L518:
	movl	$64, %ecx
	jmp	.L515
	.p2align 4,,10
	.p2align 3
.L512:
	movl	$111, %ecx
	jmp	.L515
	.p2align 4,,10
	.p2align 3
.L805:
	movl	$125, %ecx
	jmp	.L515
	.p2align 4,,10
	.p2align 3
.L516:
	movl	$126, %ecx
	jmp	.L515
	.p2align 4,,10
	.p2align 3
.L534:
	movl	$112, %ecx
	jmp	.L538
	.p2align 4,,10
	.p2align 3
.L535:
	movl	$111, %ecx
	jmp	.L538
	.p2align 4,,10
	.p2align 3
.L536:
	movl	$123, %ecx
	jmp	.L538
	.p2align 4,,10
	.p2align 3
.L537:
	movl	$124, %ecx
	jmp	.L538
	.p2align 4,,10
	.p2align 3
.L540:
	movl	$127, %ecx
	jmp	.L538
	.p2align 4,,10
	.p2align 3
.L541:
	movl	$64, %ecx
	jmp	.L538
	.p2align 4,,10
	.p2align 3
.L806:
	movl	$125, %ecx
	jmp	.L538
	.p2align 4,,10
	.p2align 3
.L539:
	movl	$126, %ecx
	jmp	.L538
	.p2align 4,,10
	.p2align 3
.L607:
	movl	$112, %r14d
	jmp	.L611
	.p2align 4,,10
	.p2align 3
.L608:
	movl	$111, %r14d
	jmp	.L611
	.p2align 4,,10
	.p2align 3
.L609:
	movl	$123, %r14d
	jmp	.L611
	.p2align 4,,10
	.p2align 3
.L610:
	movl	$124, %r14d
	jmp	.L611
	.p2align 4,,10
	.p2align 3
.L613:
	movl	$127, %r14d
	jmp	.L611
	.p2align 4,,10
	.p2align 3
.L614:
	movl	$64, %r14d
	jmp	.L611
	.p2align 4,,10
	.p2align 3
.L809:
	movl	$125, %r14d
	jmp	.L611
	.p2align 4,,10
	.p2align 3
.L612:
	movl	$126, %r14d
	jmp	.L611
	.p2align 4,,10
	.p2align 3
.L969:
	movq	0(%r13), %r9
	subq	8(%r13), %rsi
	addq	%rsi, %rsi
	movq	16(%r9), %rdi
	movq	24(%r9), %rax
	leaq	1(%rsi), %rbx
	addq	$8, %rsi
	andq	$-8, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L971
	addq	%rdi, %rsi
	movq	%rsi, 16(%r9)
.L521:
	movq	8(%r13), %rsi
	movq	16(%r13), %rdx
	movq	%r8, -96(%rbp)
	movb	%cl, -88(%rbp)
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	16(%r13), %rdx
	subq	8(%r13), %rdx
	addq	%rax, %rbx
	addq	%rax, %rdx
	movq	%rax, 8(%r13)
	movq	-96(%rbp), %r8
	movq	%rbx, 24(%r13)
	movzbl	-88(%rbp), %ecx
	leaq	1(%rdx), %rax
	jmp	.L519
	.p2align 4,,10
	.p2align 3
.L930:
	movq	0(%r13), %r8
	subq	8(%r13), %rsi
	addq	%rsi, %rsi
	movq	16(%r8), %rdi
	movq	24(%r8), %rax
	leaq	1(%rsi), %rbx
	addq	$8, %rsi
	andq	$-8, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L972
	addq	%rdi, %rsi
	movq	%rsi, 16(%r8)
.L544:
	movq	8(%r13), %rsi
	movq	16(%r13), %rdx
	movq	%r10, -96(%rbp)
	movb	%cl, -88(%rbp)
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	16(%r13), %rdx
	subq	8(%r13), %rdx
	addq	%rax, %rbx
	addq	%rax, %rdx
	movq	%rax, 8(%r13)
	movq	-96(%rbp), %r10
	movq	%rbx, 24(%r13)
	movzbl	-88(%rbp), %ecx
	leaq	1(%rdx), %rax
	jmp	.L542
	.p2align 4,,10
	.p2align 3
.L703:
	cmpb	$3, %r13b
	jne	.L973
	movq	-104(%rbp), %rax
	movq	88(%rax), %rsi
	subq	80(%rax), %rsi
	jmp	.L918
	.p2align 4,,10
	.p2align 3
.L973:
	cmpb	$4, %r13b
	jne	.L707
	.p2align 4,,10
	.p2align 3
.L508:
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L940:
	movq	0(%r13), %r11
	subq	8(%r13), %rax
	addq	%rax, %rax
	movq	16(%r11), %rdi
	movq	24(%r11), %rdx
	leaq	1(%rax), %r14
	addq	$8, %rax
	andq	$-8, %rax
	subq	%rdi, %rdx
	cmpq	%rdx, %rax
	ja	.L974
	addq	%rdi, %rax
	movq	%rax, 16(%r11)
.L651:
	movq	8(%r13), %rsi
	movq	16(%r13), %rdx
	movb	%r9b, -120(%rbp)
	movl	%r8d, -112(%rbp)
	subq	%rsi, %rdx
	movb	%r10b, -96(%rbp)
	call	memcpy@PLT
	movq	16(%r13), %rdx
	subq	8(%r13), %rdx
	addq	%rax, %rdx
	addq	%rax, %r14
	movq	%rax, 8(%r13)
	movzbl	-120(%rbp), %r9d
	movq	%r14, 24(%r13)
	movl	-112(%rbp), %r8d
	leaq	1(%rdx), %rsi
	movzbl	-96(%rbp), %r10d
	jmp	.L649
	.p2align 4,,10
	.p2align 3
.L939:
	movq	0(%r13), %rdx
	subq	8(%r13), %rax
	addq	%rax, %rax
	movq	16(%rdx), %rdi
	movq	24(%rdx), %rsi
	leaq	1(%rax), %r14
	addq	$8, %rax
	andq	$-8, %rax
	subq	%rdi, %rsi
	cmpq	%rsi, %rax
	ja	.L975
	addq	%rdi, %rax
	movq	%rax, 16(%rdx)
.L648:
	movq	8(%r13), %rsi
	movq	16(%r13), %rdx
	movb	%r11b, -128(%rbp)
	movb	%r9b, -120(%rbp)
	subq	%rsi, %rdx
	movl	%r8d, -112(%rbp)
	movb	%r10b, -96(%rbp)
	call	memcpy@PLT
	movq	16(%r13), %rdx
	subq	8(%r13), %rdx
	addq	%rax, %rdx
	addq	%rax, %r14
	movq	%rax, 8(%r13)
	movzbl	-128(%rbp), %r11d
	movq	%r14, 24(%r13)
	movzbl	-120(%rbp), %r9d
	leaq	1(%rdx), %rsi
	movl	-112(%rbp), %r8d
	movzbl	-96(%rbp), %r10d
	jmp	.L646
	.p2align 4,,10
	.p2align 3
.L944:
	movq	(%r12), %r8
	subq	8(%r12), %rax
	addq	%rax, %rax
	movq	16(%r8), %rdi
	movq	24(%r8), %rdx
	leaq	1(%rax), %r15
	addq	$8, %rax
	andq	$-8, %rax
	subq	%rdi, %rdx
	cmpq	%rdx, %rax
	ja	.L976
	addq	%rdi, %rax
	movq	%rax, 16(%r8)
.L702:
	movq	8(%r12), %rsi
	movq	16(%r12), %rdx
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	16(%r12), %r8
	subq	8(%r12), %r8
	addq	%rax, %r8
	addq	%rax, %r15
	movq	%rax, 8(%r12)
	movq	%r15, 24(%r12)
	leaq	1(%r8), %rdx
	jmp	.L700
	.p2align 4,,10
	.p2align 3
.L943:
	movq	(%r12), %r8
	subq	8(%r12), %rdx
	leaq	(%rbx,%rdx,2), %r15
	movq	16(%r8), %rdi
	movq	24(%r8), %rdx
	leaq	7(%r15), %rsi
	andq	$-8, %rsi
	subq	%rdi, %rdx
	cmpq	%rdx, %rsi
	ja	.L977
	addq	%rdi, %rsi
	movq	%rsi, 16(%r8)
.L699:
	movq	8(%r12), %rsi
	movq	16(%r12), %rdx
	movq	%r11, -112(%rbp)
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	-112(%rbp), %r11
	movq	%rax, %rdi
	movq	16(%r12), %rax
	subq	8(%r12), %rax
	leaq	(%rdi,%rax), %r8
	addq	%rdi, %r15
	movq	%rdi, 8(%r12)
	movq	%r8, 16(%r12)
	movq	%r15, 24(%r12)
	jmp	.L697
	.p2align 4,,10
	.p2align 3
.L942:
	movq	(%r12), %r8
	subq	8(%r12), %rax
	addq	%rax, %rax
	movq	16(%r8), %rdi
	movq	24(%r8), %rsi
	leaq	5(%rax), %r15
	addq	$12, %rax
	andq	$-8, %rax
	subq	%rdi, %rsi
	cmpq	%rsi, %rax
	ja	.L978
	addq	%rdi, %rax
	movq	%rax, 16(%r8)
.L693:
	movq	8(%r12), %rsi
	movq	16(%r12), %rdx
	movq	%r11, -112(%rbp)
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	16(%r12), %rdx
	subq	8(%r12), %rdx
	addq	%rax, %r15
	movq	%rax, 8(%r12)
	movq	-112(%rbp), %r11
	addq	%rax, %rdx
	movq	%r15, 24(%r12)
	jmp	.L691
	.p2align 4,,10
	.p2align 3
.L945:
	movslq	-88(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm10ZoneBuffer10write_sizeEm
	jmp	.L707
.L652:
	movq	16(%r13), %rdx
	jmp	.L665
	.p2align 4,,10
	.p2align 3
.L968:
	cmpb	$2, %r9b
	jne	.L508
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm10ZoneBuffer11EnsureSpaceEm.constprop.0
	movq	16(%r13), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 16(%r13)
	movb	$66, (%rax)
	jmp	.L916
	.p2align 4,,10
	.p2align 3
.L680:
	cmpb	$4, %r9b
	jne	.L508
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm10ZoneBuffer11EnsureSpaceEm.constprop.0
	movq	16(%r13), %rax
	movl	$8, %esi
	movq	%r13, %rdi
	leaq	1(%rax), %rdx
	movq	%rdx, 16(%r13)
	movb	$68, (%rax)
	call	_ZN2v88internal4wasm10ZoneBuffer11EnsureSpaceEm
	movq	16(%r13), %rax
	movq	$0, (%rax)
	movq	16(%r13), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 16(%r13)
	jmp	.L665
	.p2align 4,,10
	.p2align 3
.L935:
	movq	(%r15), %r8
	subq	8(%r15), %rdx
	addq	%rdx, %rdx
	movq	16(%r8), %rdi
	movq	24(%r8), %rax
	leaq	5(%rdx), %r14
	addq	$12, %rdx
	andq	$-8, %rdx
	subq	%rdi, %rax
	cmpq	%rax, %rdx
	ja	.L979
	addq	%rdi, %rdx
	movq	%rdx, 16(%r8)
.L623:
	movq	8(%r15), %rsi
	movq	16(%r15), %rdx
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	%rax, %rdi
	movq	16(%r15), %rax
	subq	8(%r15), %rax
	addq	%rdi, %r14
	movq	%rdi, 8(%r15)
	addq	%rdi, %rax
	movq	%r14, 24(%r15)
	jmp	.L925
	.p2align 4,,10
	.p2align 3
.L934:
	movq	(%r15), %r8
	subq	8(%r15), %rax
	addq	%rax, %rax
	movq	16(%r8), %rdi
	movq	24(%r8), %rdx
	leaq	1(%rax), %r13
	addq	$8, %rax
	andq	$-8, %rax
	subq	%rdi, %rdx
	cmpq	%rdx, %rax
	ja	.L980
	addq	%rdi, %rax
	movq	%rax, 16(%r8)
.L620:
	movq	8(%r15), %rsi
	movq	16(%r15), %rdx
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	16(%r15), %rdx
	subq	8(%r15), %rdx
	addq	%rax, %rdx
	addq	%rax, %r13
	movq	%rax, 8(%r15)
	movq	%r13, 24(%r15)
	leaq	1(%rdx), %rsi
	jmp	.L618
	.p2align 4,,10
	.p2align 3
.L933:
	movq	(%r15), %r8
	subq	8(%r15), %rax
	addq	%rax, %rax
	movq	16(%r8), %rdi
	movq	24(%r8), %rdx
	leaq	1(%rax), %r13
	addq	$8, %rax
	andq	$-8, %rax
	subq	%rdi, %rdx
	cmpq	%rdx, %rax
	ja	.L981
	addq	%rdi, %rax
	movq	%rax, 16(%r8)
.L617:
	movq	8(%r15), %rsi
	movq	16(%r15), %rdx
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	16(%r15), %rdx
	subq	8(%r15), %rdx
	addq	%rax, %rdx
	addq	%rax, %r13
	movq	%rax, 8(%r15)
	movq	%r13, 24(%r15)
	leaq	1(%rdx), %rsi
	jmp	.L615
	.p2align 4,,10
	.p2align 3
.L932:
	movq	0(%r13), %r9
	subq	8(%r13), %rdx
	addq	%rdx, %rdx
	movq	16(%r9), %rdi
	movq	24(%r9), %rax
	leaq	5(%rdx), %r12
	addq	$12, %rdx
	andq	$-8, %rdx
	subq	%rdi, %rax
	cmpq	%rax, %rdx
	ja	.L982
	addq	%rdi, %rdx
	movq	%rdx, 16(%r9)
.L597:
	movq	8(%r13), %rsi
	movq	16(%r13), %rdx
	movq	%r8, -96(%rbp)
	movl	%ecx, -88(%rbp)
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	-96(%rbp), %r8
	movl	-88(%rbp), %ecx
	movq	%rax, %rdi
	movq	16(%r13), %rax
	subq	8(%r13), %rax
	addq	%rdi, %r12
	movq	%rdi, 8(%r13)
	addq	%rdi, %rax
	movq	%r12, 24(%r13)
	jmp	.L924
	.p2align 4,,10
	.p2align 3
.L936:
	movl	8(%r12), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm10ZoneBuffer10write_sizeEm
	jmp	.L627
	.p2align 4,,10
	.p2align 3
.L970:
	movq	0(%r13), %r8
	subq	8(%r13), %rax
	addq	%rax, %rax
	leaq	12(%rax), %rsi
	movq	16(%r8), %rdi
	leaq	5(%rax), %rbx
	movq	24(%r8), %rax
	andq	$-8, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L983
	addq	%rdi, %rsi
	movq	%rsi, 16(%r8)
.L525:
	movq	8(%r13), %rsi
	movq	16(%r13), %rdx
	movq	%r9, -88(%rbp)
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	16(%r13), %rdx
	subq	8(%r13), %rdx
	addq	%rax, %rbx
	movq	%rax, 8(%r13)
	movq	-88(%rbp), %r9
	addq	%rax, %rdx
	movq	%rbx, 24(%r13)
	jmp	.L523
	.p2align 4,,10
	.p2align 3
.L929:
	movq	0(%r13), %r9
	subq	8(%r13), %rax
	addq	%rax, %rax
	leaq	12(%rax), %rsi
	movq	16(%r9), %rdi
	leaq	5(%rax), %rbx
	movq	24(%r9), %rax
	andq	$-8, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L984
	addq	%rdi, %rsi
	movq	%rsi, 16(%r9)
.L503:
	movq	8(%r13), %rsi
	movq	16(%r13), %rdx
	movq	%r8, -88(%rbp)
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	16(%r13), %rdx
	subq	8(%r13), %rdx
	addq	%rax, %rbx
	movq	%rax, 8(%r13)
	movq	-88(%rbp), %r8
	addq	%rax, %rdx
	movq	%rbx, 24(%r13)
	jmp	.L501
	.p2align 4,,10
	.p2align 3
.L928:
	movq	0(%r13), %r8
	subq	8(%r13), %rax
	addq	%rax, %rax
	leaq	8(%rax), %rsi
	movq	16(%r8), %rdi
	leaq	1(%rax), %rbx
	movq	24(%r8), %rax
	andq	$-8, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L985
	addq	%rdi, %rsi
	movq	%rsi, 16(%r8)
.L500:
	movq	8(%r13), %rsi
	movq	16(%r13), %rdx
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	16(%r13), %rdx
	subq	8(%r13), %rdx
	addq	%rax, %rdx
	addq	%rax, %rbx
	movq	%rax, 8(%r13)
	movq	%rbx, 24(%r13)
	leaq	1(%rdx), %rsi
	jmp	.L498
	.p2align 4,,10
	.p2align 3
.L948:
	movq	(%rbx), %r9
	subq	8(%rbx), %rax
	addq	%rax, %rax
	movq	16(%r9), %rdi
	movq	24(%r9), %rdx
	leaq	5(%rax), %r14
	addq	$12, %rax
	andq	$-8, %rax
	subq	%rdi, %rdx
	cmpq	%rdx, %rax
	ja	.L986
	addq	%rdi, %rax
	movq	%rax, 16(%r9)
.L721:
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rdx
	movl	%r8d, -96(%rbp)
	movq	%rcx, -88(%rbp)
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	16(%rbx), %rdx
	subq	8(%rbx), %rdx
	addq	%rax, %r14
	movq	%rax, 8(%rbx)
	movl	-96(%rbp), %r8d
	addq	%rax, %rdx
	movq	%r14, 24(%rbx)
	movq	-88(%rbp), %rcx
	jmp	.L719
	.p2align 4,,10
	.p2align 3
.L971:
	movq	%r9, %rdi
	movq	%r8, -96(%rbp)
	movb	%cl, -88(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movzbl	-88(%rbp), %ecx
	movq	-96(%rbp), %r8
	movq	%rax, %rdi
	jmp	.L521
	.p2align 4,,10
	.p2align 3
.L972:
	movq	%r8, %rdi
	movq	%r10, -96(%rbp)
	movb	%cl, -88(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movzbl	-88(%rbp), %ecx
	movq	-96(%rbp), %r10
	movq	%rax, %rdi
	jmp	.L544
	.p2align 4,,10
	.p2align 3
.L961:
	movq	(%r12), %r10
	subq	8(%r12), %rax
	addq	%rax, %rax
	leaq	12(%rax), %rsi
	movq	16(%r10), %rdi
	leaq	5(%rax), %r14
	movq	24(%r10), %rax
	andq	$-8, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L987
	addq	%rdi, %rsi
	movq	%rsi, 16(%r10)
.L791:
	movq	8(%r12), %rsi
	movq	16(%r12), %rdx
	movq	%r9, -96(%rbp)
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	16(%r12), %rdx
	subq	8(%r12), %rdx
	addq	%rax, %r14
	movq	%rax, 8(%r12)
	movq	-96(%rbp), %r9
	addq	%rax, %rdx
	movq	%r14, 24(%r12)
	jmp	.L789
	.p2align 4,,10
	.p2align 3
.L960:
	movq	(%r12), %r9
	subq	8(%r12), %rdx
	addq	%rdx, %rdx
	movq	16(%r9), %rdi
	movq	24(%r9), %rax
	leaq	12(%rdx), %rsi
	leaq	5(%rdx), %r14
	andq	$-8, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L988
	addq	%rdi, %rsi
	movq	%rsi, 16(%r9)
.L785:
	movq	8(%r12), %rsi
	movq	16(%r12), %rdx
	movl	%ecx, -96(%rbp)
	subq	%rsi, %rdx
	call	memcpy@PLT
	movl	-96(%rbp), %ecx
	movq	%rax, %rdi
	movq	16(%r12), %rax
	subq	8(%r12), %rax
	addq	%rdi, %r14
	movq	%rdi, 8(%r12)
	addq	%rdi, %rax
	movq	%r14, 24(%r12)
	jmp	.L783
	.p2align 4,,10
	.p2align 3
.L962:
	movq	(%r12), %rdi
	subq	8(%r12), %rax
	leaq	0(%r13,%rax,2), %r14
	movq	16(%rdi), %r10
	movq	24(%rdi), %rax
	leaq	7(%r14), %rsi
	andq	$-8, %rsi
	subq	%r10, %rax
	cmpq	%rax, %rsi
	ja	.L989
	addq	%r10, %rsi
	movq	%rsi, 16(%rdi)
.L797:
	movq	8(%r12), %rsi
	movq	16(%r12), %rdx
	movq	%r10, %rdi
	movq	%r9, -96(%rbp)
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	16(%r12), %rdi
	subq	8(%r12), %rdi
	movq	%rax, %r10
	addq	%rax, %rdi
	movq	%rax, 8(%r12)
	movq	-96(%rbp), %r9
	addq	%r14, %r10
	movq	%rdi, 16(%r12)
	movq	%r10, 24(%r12)
	jmp	.L795
	.p2align 4,,10
	.p2align 3
.L959:
	movq	(%rbx), %rdi
	subq	8(%rbx), %rax
	leaq	(%r14,%rax,2), %r12
	movq	16(%rdi), %r8
	movq	24(%rdi), %rax
	leaq	7(%r12), %rsi
	andq	$-8, %rsi
	subq	%r8, %rax
	cmpq	%rax, %rsi
	ja	.L990
	addq	%r8, %rsi
	movq	%rsi, 16(%rdi)
.L777:
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rdx
	movq	%r8, %rdi
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	16(%rbx), %rdi
	subq	8(%rbx), %rdi
	movq	%rax, %r8
	addq	%rax, %rdi
	movq	%rax, 8(%rbx)
	addq	%r12, %r8
	movq	%rdi, 16(%rbx)
	movq	%r8, 24(%rbx)
	jmp	.L775
	.p2align 4,,10
	.p2align 3
.L958:
	subq	8(%rbx), %rdx
	movq	(%rbx), %r9
	addq	%rdx, %rdx
	leaq	5(%rdx), %rax
	movq	16(%r9), %rdi
	addq	$12, %rdx
	movq	%rax, -96(%rbp)
	movq	24(%r9), %rax
	andq	$-8, %rdx
	subq	%rdi, %rax
	cmpq	%rax, %rdx
	ja	.L991
	addq	%rdi, %rdx
	movq	%rdx, 16(%r9)
.L771:
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rdx
	movl	%r8d, -112(%rbp)
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	-96(%rbp), %rdx
	movl	-112(%rbp), %r8d
	movq	%rax, %rdi
	movq	16(%rbx), %rax
	subq	8(%rbx), %rax
	addq	%rdi, %rdx
	movq	%rdi, 8(%rbx)
	addq	%rdi, %rax
	movq	%rdx, 24(%rbx)
	jmp	.L769
	.p2align 4,,10
	.p2align 3
.L957:
	subq	8(%rbx), %rax
	movq	(%rbx), %r9
	addq	%rax, %rax
	leaq	1(%rax), %rdi
	movq	24(%r9), %rdx
	addq	$8, %rax
	movq	%rdi, -96(%rbp)
	movq	16(%r9), %rdi
	andq	$-8, %rax
	subq	%rdi, %rdx
	cmpq	%rdx, %rax
	ja	.L992
	addq	%rdi, %rax
	movq	%rax, 16(%r9)
.L768:
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rdx
	movl	%r8d, -112(%rbp)
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	16(%rbx), %rdx
	subq	8(%rbx), %rdx
	movq	%rax, %rdi
	addq	%rax, %rdx
	movq	%rax, 8(%rbx)
	movq	-96(%rbp), %rax
	movl	-112(%rbp), %r8d
	leaq	1(%rdx), %rsi
	addq	%rdi, %rax
	movq	%rax, 24(%rbx)
	jmp	.L766
	.p2align 4,,10
	.p2align 3
.L954:
	subq	8(%rbx), %rax
	movq	(%rbx), %r10
	addq	%rax, %rax
	leaq	1(%rax), %rdi
	movq	24(%r10), %rdx
	addq	$8, %rax
	movq	%rdi, -96(%rbp)
	movq	16(%r10), %rdi
	andq	$-8, %rax
	subq	%rdi, %rdx
	cmpq	%rdx, %rax
	ja	.L993
	addq	%rdi, %rax
	movq	%rax, 16(%r10)
.L756:
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rdx
	movl	%r8d, -120(%rbp)
	movl	%r9d, -112(%rbp)
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	16(%rbx), %rdx
	subq	8(%rbx), %rdx
	movq	%rax, %rdi
	addq	%rax, %rdx
	movq	%rax, 8(%rbx)
	movq	-96(%rbp), %rax
	movl	-120(%rbp), %r8d
	movl	-112(%rbp), %r9d
	leaq	1(%rdx), %rsi
	addq	%rdi, %rax
	movq	%rax, 24(%rbx)
	jmp	.L754
	.p2align 4,,10
	.p2align 3
.L956:
	subq	8(%rbx), %rdx
	movq	(%rbx), %r10
	addq	%rdx, %rdx
	leaq	5(%rdx), %rax
	movq	16(%r10), %rdi
	addq	$12, %rdx
	movq	%rax, -96(%rbp)
	movq	24(%r10), %rax
	andq	$-8, %rdx
	subq	%rdi, %rax
	cmpq	%rax, %rdx
	ja	.L994
	addq	%rdi, %rdx
	movq	%rdx, 16(%r10)
.L762:
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rdx
	movl	%r8d, -120(%rbp)
	movl	%r9d, -112(%rbp)
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	-96(%rbp), %rdx
	movl	-120(%rbp), %r8d
	movq	%rax, %rdi
	movq	16(%rbx), %rax
	subq	8(%rbx), %rax
	addq	%rdi, %rdx
	movq	%rdi, 8(%rbx)
	movl	-112(%rbp), %r9d
	addq	%rdi, %rax
	movq	%rdx, 24(%rbx)
	jmp	.L927
	.p2align 4,,10
	.p2align 3
.L955:
	subq	8(%rbx), %rax
	movq	(%rbx), %r10
	addq	%rax, %rax
	leaq	1(%rax), %rdi
	movq	24(%r10), %rdx
	addq	$8, %rax
	movq	%rdi, -96(%rbp)
	movq	16(%r10), %rdi
	andq	$-8, %rax
	subq	%rdi, %rdx
	cmpq	%rdx, %rax
	ja	.L995
	addq	%rdi, %rax
	movq	%rax, 16(%r10)
.L759:
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rdx
	movl	%r8d, -120(%rbp)
	movl	%r9d, -112(%rbp)
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	16(%rbx), %rdx
	subq	8(%rbx), %rdx
	movq	%rax, %rdi
	addq	%rax, %rdx
	movq	%rax, 8(%rbx)
	movq	-96(%rbp), %rax
	movl	-120(%rbp), %r8d
	movl	-112(%rbp), %r9d
	leaq	1(%rdx), %rsi
	addq	%rdi, %rax
	movq	%rax, 24(%rbx)
	jmp	.L757
.L996:
	movq	%r14, %rcx
.L800:
	movq	8(%rcx), %rdx
	movq	16(%rcx), %rax
	movq	-112(%rbp), %rdi
	subq	%rdx, %rax
	subl	$5, %eax
	subl	%edi, %eax
	addq	%rdx, %rdi
	movl	%eax, %edx
	orl	$-128, %edx
	movb	%dl, (%rdi)
	movl	%eax, %edx
	shrl	$7, %edx
	orl	$-128, %edx
	movb	%dl, 1(%rdi)
	movl	%eax, %edx
	shrl	$14, %edx
	orl	$-128, %edx
	movb	%dl, 2(%rdi)
	movl	%eax, %edx
	shrl	$28, %eax
	shrl	$21, %edx
	movb	%al, 4(%rdi)
	orl	$-128, %edx
	movb	%dl, 3(%rdi)
	movq	8(%rcx), %rdx
	movq	16(%rcx), %rax
	movq	-120(%rbp), %rdi
	subq	%rdx, %rax
	subl	$5, %eax
	subl	%edi, %eax
	addq	%rdx, %rdi
	movl	%eax, %edx
	orl	$-128, %edx
	movb	%dl, (%rdi)
	movl	%eax, %edx
	shrl	$7, %edx
	orl	$-128, %edx
	movb	%dl, 1(%rdi)
	movl	%eax, %edx
	shrl	$14, %edx
	orl	$-128, %edx
	movb	%dl, 2(%rdi)
	movl	%eax, %edx
	shrl	$28, %eax
	shrl	$21, %edx
	movb	%al, 4(%rdi)
	orl	$-128, %edx
	movb	%dl, 3(%rdi)
.L495:
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L963:
	.cfi_restore_state
	movl	-128(%rbp), %ebx
	movq	%r12, %rcx
.L782:
	movl	-136(%rbp), %eax
	testl	%eax, %eax
	je	.L800
	movq	-104(%rbp), %rax
	movq	144(%rax), %r12
	movq	152(%rax), %r8
	cmpq	%r8, %r12
	je	.L800
	movq	%r8, %r15
	movq	%rcx, %r14
	jmp	.L802
	.p2align 4,,10
	.p2align 3
.L801:
	addq	$8, %r12
	addl	$1, %ebx
	cmpq	%r12, %r15
	je	.L996
.L802:
	movq	(%r12), %r13
	cmpq	$0, 104(%r13)
	je	.L801
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm10ZoneBuffer10write_u32vEj
	movq	96(%r13), %r8
	movslq	104(%r13), %r13
	movq	%r14, %rdi
	movq	%r13, %rsi
	movq	%r8, -88(%rbp)
	call	_ZN2v88internal4wasm10ZoneBuffer10write_sizeEm
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm10ZoneBuffer11EnsureSpaceEm
	movq	-88(%rbp), %r8
	movq	16(%r14), %rdi
	movq	%r13, %rdx
	movq	%r8, %rsi
	call	memcpy@PLT
	addq	%r13, 16(%r14)
	jmp	.L801
	.p2align 4,,10
	.p2align 3
.L818:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L750:
	movzbl	(%rax,%rdx), %ecx
	movb	%cl, (%r15,%rdx)
	addq	$1, %rdx
	cmpq	%rdx, %r14
	jne	.L750
	jmp	.L753
	.p2align 4,,10
	.p2align 3
.L966:
	movl	%r14d, %eax
	sarl	$6, %eax
	cmpl	$-1, %eax
	je	.L669
	.p2align 4,,10
	.p2align 3
.L664:
	movq	16(%r13), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 16(%r13)
	movl	%r14d, %edx
	orl	$-128, %edx
	movb	%dl, (%rax)
	movl	%r14d, %eax
	sarl	$7, %r14d
	sarl	$13, %eax
	cmpl	$-1, %eax
	jne	.L664
.L669:
	movq	16(%r13), %rax
	andl	$127, %r14d
	leaq	1(%rax), %rdx
	movq	%rdx, 16(%r13)
	movb	%r14b, (%rax)
	movq	16(%r13), %rdx
	jmp	.L665
	.p2align 4,,10
	.p2align 3
.L965:
	movq	%r14, %rax
	sarq	$6, %rax
	cmpq	$-1, %rax
	je	.L669
	.p2align 4,,10
	.p2align 3
.L670:
	movq	16(%r13), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 16(%r13)
	movl	%r14d, %edx
	orl	$-128, %edx
	movb	%dl, (%rax)
	movq	%r14, %rax
	sarq	$7, %r14
	sarq	$13, %rax
	cmpq	$-1, %rax
	jne	.L670
	jmp	.L669
	.p2align 4,,10
	.p2align 3
.L967:
	subq	8(%r13), %rdx
	movq	0(%r13), %r9
	leaq	(%rdx,%rdx), %rax
	leaq	5(%rax), %rdi
	movq	24(%r9), %rdx
	addq	$12, %rax
	movq	%rdi, -96(%rbp)
	movq	16(%r9), %rdi
	andq	$-8, %rax
	subq	%rdi, %rdx
	cmpq	%rdx, %rax
	ja	.L997
	addq	%rdi, %rax
	movq	%rax, 16(%r9)
.L675:
	movq	8(%r13), %rsi
	movq	16(%r13), %rdx
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	%rax, %rdi
	movq	16(%r13), %rax
	subq	8(%r13), %rax
	addq	%rdi, %rax
	movq	%rdi, 8(%r13)
	addq	-96(%rbp), %rdi
	movq	%rdi, 24(%r13)
	jmp	.L926
	.p2align 4,,10
	.p2align 3
.L735:
	movq	%rcx, %rsi
	movl	$10, %edi
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal4wasm12_GLOBAL__N_111EmitSectionENS1_11SectionCodeEPNS1_10ZoneBufferE
	movq	-104(%rbp), %rbx
	movq	-88(%rbp), %rcx
	movq	%rax, %r12
	movq	152(%rbx), %rsi
	subq	144(%rbx), %rsi
	movq	%rcx, %rdi
	sarq	$3, %rsi
	call	_ZN2v88internal4wasm10ZoneBuffer10write_sizeEm
	movq	%rbx, %rax
	movq	152(%rbx), %rbx
	movq	-88(%rbp), %rcx
	movq	144(%rax), %r13
	cmpq	%rbx, %r13
	je	.L731
	movq	%rcx, %r14
	.p2align 4,,10
	.p2align 3
.L743:
	movq	0(%r13), %rdi
	movq	%r14, %rsi
	addq	$8, %r13
	call	_ZNK2v88internal4wasm19WasmFunctionBuilder9WriteBodyEPNS1_10ZoneBufferE
	cmpq	%r13, %rbx
	jne	.L743
	movq	%r14, %rcx
.L731:
	movq	8(%rcx), %rdx
	movq	16(%rcx), %rax
	subq	%rdx, %rax
	subl	$5, %eax
	subl	%r12d, %eax
	addq	%rdx, %r12
	movl	%eax, %edx
	orl	$-128, %edx
	movb	%dl, (%r12)
	movl	%eax, %edx
	shrl	$7, %edx
	orl	$-128, %edx
	movb	%dl, 1(%r12)
	movl	%eax, %edx
	shrl	$14, %edx
	orl	$-128, %edx
	movb	%dl, 2(%r12)
	movl	%eax, %edx
	shrl	$28, %eax
	shrl	$21, %edx
	movb	%al, 4(%r12)
	orl	$-128, %edx
	movb	%dl, 3(%r12)
	jmp	.L727
	.p2align 4,,10
	.p2align 3
.L817:
	xorl	%r12d, %r12d
	xorl	%r14d, %r14d
	xorl	%r8d, %r8d
	jmp	.L749
	.p2align 4,,10
	.p2align 3
.L974:
	movq	%r11, %rdi
	movq	%rax, %rsi
	movb	%r9b, -120(%rbp)
	movl	%r8d, -112(%rbp)
	movb	%r10b, -96(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movzbl	-96(%rbp), %r10d
	movl	-112(%rbp), %r8d
	movzbl	-120(%rbp), %r9d
	movq	%rax, %rdi
	jmp	.L651
	.p2align 4,,10
	.p2align 3
.L964:
	movq	%r8, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdi
	jmp	.L686
	.p2align 4,,10
	.p2align 3
.L975:
	movq	%rdx, %rdi
	movq	%rax, %rsi
	movb	%r11b, -128(%rbp)
	movb	%r9b, -120(%rbp)
	movl	%r8d, -112(%rbp)
	movb	%r10b, -96(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movzbl	-96(%rbp), %r10d
	movl	-112(%rbp), %r8d
	movzbl	-120(%rbp), %r9d
	movzbl	-128(%rbp), %r11d
	movq	%rax, %rdi
	jmp	.L648
	.p2align 4,,10
	.p2align 3
.L679:
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm10ZoneBuffer11EnsureSpaceEm.constprop.0
	movq	16(%r13), %rax
	movl	$4, %esi
	movq	%r13, %rdi
	leaq	1(%rax), %rdx
	movq	%rdx, 16(%r13)
	movb	$67, (%rax)
	call	_ZN2v88internal4wasm10ZoneBuffer11EnsureSpaceEm
	movq	16(%r13), %rax
	movl	$0, (%rax)
	movq	16(%r13), %rax
	leaq	4(%rax), %rdx
	movq	%rdx, 16(%r13)
	jmp	.L665
.L931:
	movq	%rcx, %rsi
	movl	$2, %edi
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal4wasm12_GLOBAL__N_111EmitSectionENS1_11SectionCodeEPNS1_10ZoneBufferE
	movq	-104(%rbp), %r15
	movq	-88(%rbp), %rcx
	movq	%rax, -120(%rbp)
	movq	88(%r15), %rdx
	movq	56(%r15), %rax
	movq	%rcx, %rdi
	subq	48(%r15), %rax
	subq	80(%r15), %rdx
	sarq	$3, %rax
	sarq	$3, %rdx
	leaq	(%rdx,%rax), %rsi
	imulq	%rbx, %rsi
	call	_ZN2v88internal4wasm10ZoneBuffer10write_sizeEm
	movq	88(%r15), %rax
	movq	80(%r15), %r15
	movq	-88(%rbp), %rcx
	cmpq	%rax, %r15
	movq	%rax, -112(%rbp)
	je	.L573
	movq	%r15, %r14
	movq	%rcx, %rbx
	.p2align 4,,10
	.p2align 3
.L572:
	movzbl	16(%r14), %eax
	movq	16(%rbx), %rdx
	movq	(%r14), %r13
	movq	8(%r14), %r12
	movb	%al, -96(%rbp)
	movzbl	17(%r14), %eax
	leaq	5(%rdx), %rsi
	movb	%al, -88(%rbp)
	movq	24(%rbx), %rax
	cmpq	%rsi, %rax
	jb	.L998
.L551:
	leaq	1(%rdx), %rax
	movslq	%r12d, %r12
	movq	%rax, 16(%rbx)
	movb	$0, (%rdx)
	movq	16(%rbx), %rdx
	movq	24(%rbx), %rax
	leaq	5(%rdx), %rsi
	cmpq	%rsi, %rax
	jb	.L999
.L554:
	movl	%r12d, %eax
	cmpl	$127, %r12d
	jbe	.L557
	leaq	1(%rdx), %rax
	movq	%rax, 16(%rbx)
	movl	%r12d, %eax
	orl	$-128, %eax
	movb	%al, (%rdx)
	movl	%r12d, %eax
	shrl	$7, %eax
	cmpl	$16383, %r12d
	jbe	.L559
	.p2align 4,,10
	.p2align 3
.L558:
	movq	16(%rbx), %rdx
	leaq	1(%rdx), %rsi
	movq	%rsi, 16(%rbx)
	movl	%eax, %esi
	shrl	$7, %eax
	orl	$-128, %esi
	movb	%sil, (%rdx)
	cmpl	$127, %eax
	ja	.L558
.L559:
	movq	16(%rbx), %rdx
.L557:
	leaq	1(%rdx), %rsi
	movq	%rsi, 16(%rbx)
	movb	%al, (%rdx)
	movq	16(%rbx), %rdi
	movq	24(%rbx), %rax
	leaq	(%rdi,%r12), %rdx
	cmpq	%rdx, %rax
	jb	.L1000
.L560:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	16(%rbx), %r9
	movq	24(%rbx), %rax
	addq	%r12, %r9
	leaq	1(%r9), %rdx
	movq	%r9, 16(%rbx)
	cmpq	%rax, %rdx
	ja	.L1001
.L563:
	movq	%rdx, 16(%rbx)
	movb	$3, (%r9)
	movq	16(%rbx), %rdx
	movq	24(%rbx), %rax
	leaq	1(%rdx), %rsi
	cmpq	%rax, %rsi
	ja	.L1002
.L566:
	movzbl	-96(%rbp), %eax
	movq	%rsi, 16(%rbx)
	movb	%al, (%rdx)
	movq	16(%rbx), %rdx
	movq	24(%rbx), %rax
	leaq	1(%rdx), %rsi
	cmpq	%rax, %rsi
	ja	.L1003
.L569:
	movzbl	-88(%rbp), %eax
	movq	%rsi, 16(%rbx)
	addq	$24, %r14
	movb	%al, (%rdx)
	cmpq	%r14, -112(%rbp)
	jne	.L572
	movq	%rbx, %rcx
.L573:
	movq	-104(%rbp), %rax
	movq	%rcx, %rbx
	movq	56(%rax), %rdi
	movq	48(%rax), %r15
	movq	%rdi, -88(%rbp)
	movq	%r15, %r13
	cmpq	%rdi, %r15
	je	.L550
	.p2align 4,,10
	.p2align 3
.L592:
	movq	16(%rbx), %rdx
	movq	24(%rbx), %rax
	movq	0(%r13), %r14
	movq	8(%r13), %r8
	leaq	5(%rdx), %rsi
	movl	16(%r13), %r12d
	cmpq	%rsi, %rax
	jb	.L1004
.L574:
	leaq	1(%rdx), %rax
	movslq	%r8d, %r15
	movq	%rax, 16(%rbx)
	movb	$0, (%rdx)
	movq	16(%rbx), %rdx
	movq	24(%rbx), %rax
	leaq	5(%rdx), %rsi
	cmpq	%rsi, %rax
	jb	.L1005
.L577:
	movl	%r15d, %eax
	cmpl	$127, %r15d
	jbe	.L580
	leaq	1(%rdx), %rax
	movq	%rax, 16(%rbx)
	movl	%r15d, %eax
	orl	$-128, %eax
	movb	%al, (%rdx)
	movl	%r15d, %eax
	shrl	$7, %eax
	cmpl	$16383, %r15d
	jbe	.L582
	.p2align 4,,10
	.p2align 3
.L581:
	movq	16(%rbx), %rdx
	leaq	1(%rdx), %rsi
	movq	%rsi, 16(%rbx)
	movl	%eax, %esi
	shrl	$7, %eax
	orl	$-128, %esi
	movb	%sil, (%rdx)
	cmpl	$127, %eax
	ja	.L581
.L582:
	movq	16(%rbx), %rdx
.L580:
	leaq	1(%rdx), %rsi
	movq	%rbx, %rdi
	movq	%rsi, 16(%rbx)
	movq	%r15, %rsi
	movb	%al, (%rdx)
	call	_ZN2v88internal4wasm10ZoneBuffer11EnsureSpaceEm
	movq	16(%rbx), %rdi
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	addq	16(%rbx), %r15
	movq	24(%rbx), %rax
	leaq	1(%r15), %rdx
	movq	%r15, 16(%rbx)
	movq	%r15, %r8
	cmpq	%rax, %rdx
	ja	.L1006
.L583:
	movq	%rdx, 16(%rbx)
	movb	$0, (%r8)
	movq	16(%rbx), %rax
	movq	24(%rbx), %rdx
	leaq	5(%rax), %rsi
	cmpq	%rsi, %rdx
	jnb	.L923
	jmp	.L1007
	.p2align 4,,10
	.p2align 3
.L906:
	movl	%r12d, %edx
	shrl	$7, %r12d
	orl	$-128, %edx
	movb	%dl, (%rax)
	movq	16(%rbx), %rax
.L923:
	leaq	1(%rax), %rdx
	movq	%rdx, 16(%rbx)
	cmpl	$127, %r12d
	ja	.L906
	movb	%r12b, (%rax)
	addq	$24, %r13
	cmpq	%r13, -88(%rbp)
	jne	.L592
	movq	%rbx, %rcx
.L550:
	movq	8(%rcx), %rdx
	movq	16(%rcx), %rax
	movq	-120(%rbp), %rdi
	subq	%rdx, %rax
	subl	$5, %eax
	subl	%edi, %eax
	addq	%rdx, %rdi
	movl	%eax, %edx
	orl	$-128, %edx
	movb	%dl, (%rdi)
	movl	%eax, %edx
	shrl	$7, %edx
	orl	$-128, %edx
	movb	%dl, 1(%rdi)
	movl	%eax, %edx
	shrl	$14, %edx
	orl	$-128, %edx
	movb	%dl, 2(%rdi)
	movl	%eax, %edx
	shrl	$28, %eax
	shrl	$21, %edx
	movb	%al, 4(%rdi)
	orl	$-128, %edx
	movb	%dl, 3(%rdi)
	jmp	.L547
	.p2align 4,,10
	.p2align 3
.L1005:
	subq	8(%rbx), %rax
	movq	(%rbx), %r9
	addq	%rax, %rax
	leaq	5(%rax), %rdi
	leaq	12(%rax), %rsi
	movq	24(%r9), %rax
	movq	%rdi, -96(%rbp)
	movq	16(%r9), %rdi
	andq	$-8, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L1008
	addq	%rdi, %rsi
	movq	%rsi, 16(%r9)
.L579:
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rdx
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	16(%rbx), %rdx
	subq	8(%rbx), %rdx
	movq	%rax, %rdi
	movq	%rax, 8(%rbx)
	addq	%rax, %rdx
	movq	-96(%rbp), %rax
	addq	%rdi, %rax
	movq	%rax, 24(%rbx)
	jmp	.L577
	.p2align 4,,10
	.p2align 3
.L1004:
	movq	(%rbx), %r9
	subq	8(%rbx), %rax
	addq	%rax, %rax
	leaq	12(%rax), %rsi
	movq	16(%r9), %rdi
	leaq	5(%rax), %r15
	movq	24(%r9), %rax
	andq	$-8, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L1009
	addq	%rdi, %rsi
	movq	%rsi, 16(%r9)
.L576:
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rdx
	movq	%r8, -96(%rbp)
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	16(%rbx), %rdx
	subq	8(%rbx), %rdx
	addq	%rax, %r15
	movq	%rax, 8(%rbx)
	movq	-96(%rbp), %r8
	addq	%rax, %rdx
	movq	%r15, 24(%rbx)
	jmp	.L574
	.p2align 4,,10
	.p2align 3
.L1007:
	movq	(%rbx), %r8
	subq	8(%rbx), %rdx
	addq	%rdx, %rdx
	movq	16(%r8), %rdi
	movq	24(%r8), %rax
	leaq	12(%rdx), %rsi
	leaq	5(%rdx), %r14
	andq	$-8, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L1010
	addq	%rdi, %rsi
	movq	%rsi, 16(%r8)
.L588:
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rdx
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	%rax, %rdi
	movq	16(%rbx), %rax
	subq	8(%rbx), %rax
	addq	%rdi, %r14
	movq	%rdi, 8(%rbx)
	addq	%rdi, %rax
	movq	%r14, 24(%rbx)
	jmp	.L923
	.p2align 4,,10
	.p2align 3
.L1006:
	movq	(%rbx), %r8
	subq	8(%rbx), %rax
	addq	%rax, %rax
	leaq	8(%rax), %rsi
	movq	16(%r8), %rdi
	leaq	1(%rax), %r14
	movq	24(%r8), %rax
	andq	$-8, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L1011
	addq	%rdi, %rsi
	movq	%rsi, 16(%r8)
.L585:
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rdx
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	16(%rbx), %r8
	subq	8(%rbx), %r8
	addq	%rax, %r8
	addq	%rax, %r14
	movq	%rax, 8(%rbx)
	movq	%r14, 24(%rbx)
	leaq	1(%r8), %rdx
	jmp	.L583
	.p2align 4,,10
	.p2align 3
.L1003:
	movq	(%rbx), %r8
	subq	8(%rbx), %rax
	addq	%rax, %rax
	leaq	8(%rax), %rsi
	movq	16(%r8), %rdi
	leaq	1(%rax), %r13
	movq	24(%r8), %rax
	andq	$-8, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L1012
	addq	%rdi, %rsi
	movq	%rsi, 16(%r8)
.L571:
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rdx
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	16(%rbx), %rdx
	subq	8(%rbx), %rdx
	addq	%rax, %rdx
	addq	%rax, %r13
	movq	%rax, 8(%rbx)
	movq	%r13, 24(%rbx)
	leaq	1(%rdx), %rsi
	jmp	.L569
	.p2align 4,,10
	.p2align 3
.L1002:
	movq	(%rbx), %r8
	subq	8(%rbx), %rax
	addq	%rax, %rax
	leaq	8(%rax), %rsi
	movq	16(%r8), %rdi
	leaq	1(%rax), %r15
	movq	24(%r8), %rax
	andq	$-8, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L1013
	addq	%rdi, %rsi
	movq	%rsi, 16(%r8)
.L568:
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rdx
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	16(%rbx), %rdx
	subq	8(%rbx), %rdx
	addq	%rax, %rdx
	addq	%rax, %r15
	movq	%rax, 8(%rbx)
	movq	%r15, 24(%rbx)
	leaq	1(%rdx), %rsi
	jmp	.L566
	.p2align 4,,10
	.p2align 3
.L999:
	movq	(%rbx), %r10
	subq	8(%rbx), %rax
	addq	%rax, %rax
	leaq	12(%rax), %rsi
	movq	16(%r10), %rdi
	leaq	5(%rax), %r15
	movq	24(%r10), %rax
	andq	$-8, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L1014
	addq	%rdi, %rsi
	movq	%rsi, 16(%r10)
.L556:
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rdx
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	16(%rbx), %rdx
	subq	8(%rbx), %rdx
	addq	%rax, %r15
	movq	%rax, 8(%rbx)
	addq	%rax, %rdx
	movq	%r15, 24(%rbx)
	jmp	.L554
	.p2align 4,,10
	.p2align 3
.L998:
	movq	(%rbx), %r10
	subq	8(%rbx), %rax
	addq	%rax, %rax
	leaq	12(%rax), %rsi
	movq	16(%r10), %rdi
	leaq	5(%rax), %r15
	movq	24(%r10), %rax
	andq	$-8, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L1015
	addq	%rdi, %rsi
	movq	%rsi, 16(%r10)
.L553:
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rdx
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	16(%rbx), %rdx
	subq	8(%rbx), %rdx
	addq	%rax, %r15
	movq	%rax, 8(%rbx)
	addq	%rax, %rdx
	movq	%r15, 24(%rbx)
	jmp	.L551
	.p2align 4,,10
	.p2align 3
.L1001:
	movq	(%rbx), %r8
	subq	8(%rbx), %rax
	addq	%rax, %rax
	leaq	8(%rax), %rsi
	movq	16(%r8), %rdi
	leaq	1(%rax), %r15
	movq	24(%r8), %rax
	andq	$-8, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L1016
	addq	%rdi, %rsi
	movq	%rsi, 16(%r8)
.L565:
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rdx
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	16(%rbx), %r9
	subq	8(%rbx), %r9
	addq	%rax, %r9
	addq	%rax, %r15
	movq	%rax, 8(%rbx)
	movq	%r15, 24(%rbx)
	leaq	1(%r9), %rdx
	jmp	.L563
	.p2align 4,,10
	.p2align 3
.L1000:
	movq	(%rbx), %rdi
	subq	8(%rbx), %rax
	leaq	(%r12,%rax,2), %r15
	movq	16(%rdi), %r10
	movq	24(%rdi), %rax
	leaq	7(%r15), %rsi
	andq	$-8, %rsi
	subq	%r10, %rax
	cmpq	%rax, %rsi
	ja	.L1017
	addq	%r10, %rsi
	movq	%rsi, 16(%rdi)
.L562:
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rdx
	movq	%r10, %rdi
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	16(%rbx), %rdi
	subq	8(%rbx), %rdi
	movq	%rax, %r10
	addq	%rax, %rdi
	movq	%rax, 8(%rbx)
	addq	%r15, %r10
	movq	%rdi, 16(%rbx)
	movq	%r10, 24(%rbx)
	jmp	.L560
.L938:
	movq	-104(%rbp), %rax
	movq	%rcx, %rdi
	movl	372(%rax), %esi
	call	_ZN2v88internal4wasm10ZoneBuffer10write_u32vEj
	movq	-88(%rbp), %rcx
	jmp	.L633
.L978:
	movq	%r8, %rdi
	movq	%rax, %rsi
	movq	%r11, -112(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-112(%rbp), %r11
	movq	%rax, %rdi
	jmp	.L693
.L977:
	movq	%r8, %rdi
	movq	%r11, -112(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-112(%rbp), %r11
	movq	%rax, %rdi
	jmp	.L699
.L976:
	movq	%r8, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdi
	jmp	.L702
.L953:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r15
	jmp	.L748
.L988:
	movq	%r9, %rdi
	movl	%ebx, -96(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-96(%rbp), %ecx
	movq	%rax, %rdi
	jmp	.L785
.L989:
	movq	%r9, -96(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-96(%rbp), %r9
	movq	%rax, %r10
	jmp	.L797
.L987:
	movq	%r10, %rdi
	movq	%r9, -96(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-96(%rbp), %r9
	movq	%rax, %rdi
	jmp	.L791
.L981:
	movq	%r8, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdi
	jmp	.L617
.L980:
	movq	%r8, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdi
	jmp	.L620
.L979:
	movq	%r8, %rdi
	movq	%rdx, %rsi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdi
	jmp	.L623
.L985:
	movq	%r8, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdi
	jmp	.L500
.L984:
	movq	%r9, %rdi
	movq	%r8, -88(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-88(%rbp), %r8
	movq	%rax, %rdi
	jmp	.L503
.L983:
	movq	%r8, %rdi
	movq	%r9, -88(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-88(%rbp), %r9
	movq	%rax, %rdi
	jmp	.L525
.L982:
	movq	%r9, %rdi
	movq	%rdx, %rsi
	movq	%r8, -96(%rbp)
	movl	%ecx, -88(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-88(%rbp), %ecx
	movq	-96(%rbp), %r8
	movq	%rax, %rdi
	jmp	.L597
.L991:
	movq	%r9, %rdi
	movq	%rdx, %rsi
	movl	%r8d, -112(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-112(%rbp), %r8d
	movq	%rax, %rdi
	jmp	.L771
.L990:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r8
	jmp	.L777
.L986:
	movq	%r9, %rdi
	movq	%rax, %rsi
	movl	%r8d, -96(%rbp)
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-88(%rbp), %rcx
	movl	-96(%rbp), %r8d
	movq	%rax, %rdi
	jmp	.L721
.L995:
	movq	%r10, %rdi
	movq	%rax, %rsi
	movl	%r8d, -120(%rbp)
	movl	%r9d, -112(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-112(%rbp), %r9d
	movl	-120(%rbp), %r8d
	movq	%rax, %rdi
	jmp	.L759
.L994:
	movq	%r10, %rdi
	movq	%rdx, %rsi
	movl	%r8d, -120(%rbp)
	movl	%r9d, -112(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-112(%rbp), %r9d
	movl	-120(%rbp), %r8d
	movq	%rax, %rdi
	jmp	.L762
.L993:
	movq	%r10, %rdi
	movq	%rax, %rsi
	movl	%r8d, -120(%rbp)
	movl	%r9d, -112(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-112(%rbp), %r9d
	movl	-120(%rbp), %r8d
	movq	%rax, %rdi
	jmp	.L756
.L992:
	movq	%r9, %rdi
	movq	%rax, %rsi
	movl	%r8d, -112(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-112(%rbp), %r8d
	movq	%rax, %rdi
	jmp	.L768
.L950:
	movq	(%rcx), %r8
	subq	8(%rcx), %rax
	addq	%rax, %rax
	leaq	23(%rax), %rsi
	movq	16(%r8), %rdi
	leaq	16(%rax), %r12
	movq	24(%r8), %rax
	andq	$-8, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L1018
	addq	%rdi, %rsi
	movq	%rsi, 16(%r8)
.L733:
	movq	8(%rcx), %rsi
	movq	16(%rcx), %rdx
	movq	%rcx, -88(%rbp)
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	-88(%rbp), %rcx
	movq	%rax, %rdi
	movq	16(%rcx), %rdx
	subq	8(%rcx), %rdx
	addq	%r12, %rdi
	movq	%rax, 8(%rcx)
	addq	%rax, %rdx
	movq	%rdi, 24(%rcx)
	movq	%rdx, 16(%rcx)
	jmp	.L804
.L1017:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r10
	jmp	.L562
.L1014:
	movq	%r10, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdi
	jmp	.L556
.L1013:
	movq	%r8, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdi
	jmp	.L568
.L1012:
	movq	%r8, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdi
	jmp	.L571
.L1011:
	movq	%r8, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdi
	jmp	.L585
.L952:
	movq	%r8, %rdi
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-88(%rbp), %rcx
	movq	%rax, %rdi
	jmp	.L740
.L1016:
	movq	%r8, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdi
	jmp	.L565
.L1015:
	movq	%r10, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdi
	jmp	.L553
.L1010:
	movq	%r8, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdi
	jmp	.L588
.L1009:
	movq	%r9, %rdi
	movq	%r8, -96(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-96(%rbp), %r8
	movq	%rax, %rdi
	jmp	.L576
.L1008:
	movq	%r9, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdi
	jmp	.L579
.L745:
	movq	16(%rcx), %r14
	jmp	.L779
.L997:
	movq	%r9, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdi
	jmp	.L675
	.p2align 4,,10
	.p2align 3
.L653:
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L812:
	movl	$-1, %r12d
	jmp	.L718
.L947:
	movl	%r12d, %r14d
	movl	%esi, %r12d
	jmp	.L715
.L1018:
	movq	%r8, %rdi
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-88(%rbp), %rcx
	movq	%rax, %rdi
	jmp	.L733
	.cfi_endproc
.LFE18068:
	.size	_ZNK2v88internal4wasm17WasmModuleBuilder7WriteToEPNS1_10ZoneBufferE, .-_ZNK2v88internal4wasm17WasmModuleBuilder7WriteToEPNS1_10ZoneBufferE
	.section	.text._ZNK2v88internal4wasm17WasmModuleBuilder21WriteAsmJsOffsetTableEPNS1_10ZoneBufferE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4wasm17WasmModuleBuilder21WriteAsmJsOffsetTableEPNS1_10ZoneBufferE
	.type	_ZNK2v88internal4wasm17WasmModuleBuilder21WriteAsmJsOffsetTableEPNS1_10ZoneBufferE, @function
_ZNK2v88internal4wasm17WasmModuleBuilder21WriteAsmJsOffsetTableEPNS1_10ZoneBufferE:
.LFB18075:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	16(%rsi), %rdx
	movq	%rsi, %rbx
	movq	24(%rsi), %rax
	movq	152(%rdi), %r12
	leaq	5(%rdx), %rcx
	subq	144(%rdi), %r12
	sarq	$3, %r12
	cmpq	%rcx, %rax
	jb	.L1037
.L1020:
	movl	%r12d, %eax
	cmpl	$127, %r12d
	jbe	.L1023
	leaq	1(%rdx), %rax
	movq	%rax, 16(%rbx)
	movl	%r12d, %eax
	orl	$-128, %eax
	movb	%al, (%rdx)
	movl	%r12d, %eax
	shrl	$7, %eax
	cmpl	$16383, %r12d
	jbe	.L1025
	.p2align 4,,10
	.p2align 3
.L1024:
	movq	16(%rbx), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 16(%rbx)
	movl	%eax, %ecx
	shrl	$7, %eax
	orl	$-128, %ecx
	movb	%cl, (%rdx)
	cmpl	$127, %eax
	ja	.L1024
.L1025:
	movq	16(%rbx), %rdx
.L1023:
	leaq	1(%rdx), %rcx
	movq	%rcx, 16(%rbx)
	movb	%al, (%rdx)
	movq	152(%r14), %r13
	movq	144(%r14), %r12
	cmpq	%r13, %r12
	je	.L1030
	.p2align 4,,10
	.p2align 3
.L1029:
	movq	(%r12), %rdi
	movq	%rbx, %rsi
	addq	$8, %r12
	call	_ZNK2v88internal4wasm19WasmFunctionBuilder23WriteAsmWasmOffsetTableEPNS1_10ZoneBufferE
	cmpq	%r12, %r13
	jne	.L1029
.L1030:
	movq	16(%rbx), %rdx
	movq	24(%rbx), %rax
	leaq	1(%rdx), %rcx
	cmpq	%rax, %rcx
	ja	.L1038
	movq	%rcx, 16(%rbx)
	movb	$0, (%rdx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1037:
	.cfi_restore_state
	movq	(%rsi), %rdi
	subq	8(%rsi), %rax
	addq	%rax, %rax
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rdx
	leaq	5(%rax), %r13
	addq	$12, %rax
	andq	$-8, %rax
	subq	%rcx, %rdx
	cmpq	%rdx, %rax
	ja	.L1039
	addq	%rcx, %rax
	movq	%rax, 16(%rdi)
.L1022:
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rdx
	movq	%rcx, %rdi
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	16(%rbx), %rdx
	subq	8(%rbx), %rdx
	addq	%rax, %r13
	movq	%rax, 8(%rbx)
	addq	%rax, %rdx
	movq	%r13, 24(%rbx)
	jmp	.L1020
	.p2align 4,,10
	.p2align 3
.L1038:
	movq	(%rbx), %rdi
	subq	8(%rbx), %rax
	addq	%rax, %rax
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rdx
	leaq	1(%rax), %r12
	addq	$8, %rax
	andq	$-8, %rax
	subq	%rcx, %rdx
	cmpq	%rdx, %rax
	ja	.L1040
	addq	%rcx, %rax
	movq	%rax, 16(%rdi)
.L1032:
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rdx
	movq	%rcx, %rdi
	subq	%rsi, %rdx
	call	memcpy@PLT
	movq	16(%rbx), %rdx
	subq	8(%rbx), %rdx
	addq	%rax, %rdx
	addq	%rax, %r12
	movq	%rax, 8(%rbx)
	leaq	1(%rdx), %rcx
	movq	%r12, 24(%rbx)
	movq	%rcx, 16(%rbx)
	movb	$0, (%rdx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1039:
	.cfi_restore_state
	movq	%rax, %rsi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L1022
.L1040:
	movq	%rax, %rsi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L1032
	.cfi_endproc
.LFE18075:
	.size	_ZNK2v88internal4wasm17WasmModuleBuilder21WriteAsmJsOffsetTableEPNS1_10ZoneBufferE, .-_ZNK2v88internal4wasm17WasmModuleBuilder21WriteAsmJsOffsetTableEPNS1_10ZoneBufferE
	.section	.rodata._ZNSt6vectorIjN2v88internal13ZoneAllocatorIjEEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPjS4_EEmRKj.str1.1,"aMS",@progbits,1
.LC5:
	.string	"vector::_M_fill_insert"
	.section	.text._ZNSt6vectorIjN2v88internal13ZoneAllocatorIjEEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPjS4_EEmRKj,"axG",@progbits,_ZNSt6vectorIjN2v88internal13ZoneAllocatorIjEEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPjS4_EEmRKj,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIjN2v88internal13ZoneAllocatorIjEEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPjS4_EEmRKj
	.type	_ZNSt6vectorIjN2v88internal13ZoneAllocatorIjEEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPjS4_EEmRKj, @function
_ZNSt6vectorIjN2v88internal13ZoneAllocatorIjEEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPjS4_EEmRKj:
.LFB21322:
	.cfi_startproc
	endbr64
	testq	%rdx, %rdx
	je	.L1157
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	16(%rdi), %rdi
	movq	24(%r12), %rax
	subq	%rdi, %rax
	sarq	$2, %rax
	cmpq	%rdx, %rax
	jb	.L1044
	movq	%rdi, %rsi
	movl	(%rcx), %r14d
	subq	%rbx, %rsi
	movq	%rsi, %rax
	sarq	$2, %rax
	cmpq	%rax, %rdx
	jnb	.L1045
	leaq	0(,%rdx,4), %r15
	movq	%rdi, %rdx
	subq	%r15, %rdx
	cmpq	%rdx, %rdi
	je	.L1092
	leaq	-4(%rdi), %rcx
	movl	$16, %eax
	subq	%rdx, %rcx
	subq	%r15, %rax
	shrq	$2, %rcx
	testq	%rax, %rax
	leaq	16(%rdi), %rax
	setle	%sil
	cmpq	%rax, %rdx
	setnb	%al
	orb	%al, %sil
	je	.L1093
	movabsq	$4611686018427387900, %rax
	testq	%rax, %rcx
	je	.L1093
	addq	$1, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rsi
	shrq	$2, %rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L1048:
	movdqu	(%rdx,%rax), %xmm1
	movups	%xmm1, (%rdi,%rax)
	addq	$16, %rax
	cmpq	%rsi, %rax
	jne	.L1048
	movq	%rcx, %rsi
	andq	$-4, %rsi
	leaq	0(,%rsi,4), %rax
	leaq	(%rdx,%rax), %r8
	addq	%rdi, %rax
	cmpq	%rsi, %rcx
	je	.L1050
	movl	(%r8), %ecx
	movl	%ecx, (%rax)
	leaq	4(%r8), %rcx
	cmpq	%rcx, %rdi
	je	.L1050
	movl	4(%r8), %ecx
	movl	%ecx, 4(%rax)
	leaq	8(%r8), %rcx
	cmpq	%rcx, %rdi
	je	.L1050
	movl	8(%r8), %ecx
	movl	%ecx, 8(%rax)
.L1050:
	movq	16(%r12), %rax
.L1046:
	addq	%r15, %rax
	movq	%rax, 16(%r12)
	cmpq	%rdx, %rbx
	je	.L1051
	subq	%rbx, %rdx
	movq	%rbx, %rsi
	subq	%rdx, %rdi
	call	memmove@PLT
.L1051:
	leaq	(%rbx,%r15), %rdx
	cmpq	%rdx, %rbx
	je	.L1041
	movq	%rdx, %rcx
	movq	%rbx, %rax
	subq	%rbx, %rcx
	subq	$4, %rcx
	movq	%rcx, %rsi
	shrq	$2, %rsi
	addq	$1, %rsi
	cmpq	$8, %rcx
	jbe	.L1053
	movq	%rsi, %rcx
	movd	%r14d, %xmm6
	shrq	$2, %rcx
	pshufd	$0, %xmm6, %xmm0
	salq	$4, %rcx
	addq	%rbx, %rcx
	.p2align 4,,10
	.p2align 3
.L1055:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rcx, %rax
	jne	.L1055
	movq	%rsi, %rax
	andq	$-4, %rax
	leaq	(%rbx,%rax,4), %r13
	cmpq	%rax, %rsi
	je	.L1041
.L1053:
	leaq	4(%r13), %rax
	movl	%r14d, 0(%r13)
	cmpq	%rax, %rdx
	je	.L1041
	leaq	8(%r13), %rax
	movl	%r14d, 4(%r13)
	cmpq	%rax, %rdx
	je	.L1041
.L1160:
	movl	%r14d, 8(%r13)
.L1041:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1157:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L1045:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	subq	%rax, %rdx
	je	.L1094
	leaq	-1(%rdx), %rax
	cmpq	$2, %rax
	jbe	.L1095
	movq	%rdx, %r8
	movd	%r14d, %xmm7
	xorl	%eax, %eax
	shrq	$2, %r8
	pshufd	$0, %xmm7, %xmm0
	.p2align 4,,10
	.p2align 3
.L1059:
	movq	%rax, %rcx
	addq	$1, %rax
	salq	$4, %rcx
	movups	%xmm0, (%rdi,%rcx)
	cmpq	%rax, %r8
	jne	.L1059
	movq	%rdx, %rcx
	movq	%rdx, %r8
	andq	$-4, %rcx
	andl	$3, %r8d
	leaq	(%rdi,%rcx,4), %rax
	cmpq	%rcx, %rdx
	je	.L1060
.L1058:
	movl	%r14d, (%rax)
	cmpq	$1, %r8
	je	.L1060
	movl	%r14d, 4(%rax)
	cmpq	$2, %r8
	je	.L1060
	movl	%r14d, 8(%rax)
.L1060:
	leaq	(%rdi,%rdx,4), %r8
.L1057:
	movq	%r8, 16(%r12)
	cmpq	%rbx, %rdi
	je	.L1061
	movq	%rdi, %rax
	leaq	15(%rbx), %rcx
	movq	%rbx, %rdx
	subq	%rbx, %rax
	subq	%r8, %rcx
	leaq	-4(%rax), %r9
	movq	%r9, %rax
	shrq	$2, %rax
	cmpq	$30, %rcx
	jbe	.L1096
	movabsq	$4611686018427387900, %rcx
	testq	%rcx, %rax
	je	.L1096
	leaq	1(%rax), %rcx
	xorl	%eax, %eax
	movq	%rcx, %r9
	shrq	$2, %r9
	salq	$4, %r9
	.p2align 4,,10
	.p2align 3
.L1063:
	movdqu	(%rbx,%rax), %xmm2
	movups	%xmm2, (%r8,%rax)
	addq	$16, %rax
	cmpq	%r9, %rax
	jne	.L1063
	movq	%rcx, %r9
	andq	$-4, %r9
	leaq	0(,%r9,4), %rax
	leaq	(%rbx,%rax), %r10
	addq	%r8, %rax
	cmpq	%r9, %rcx
	je	.L1064
	movl	(%r10), %r8d
	movl	%r8d, (%rax)
	leaq	4(%r10), %r8
	cmpq	%r8, %rdi
	je	.L1064
	movl	4(%r10), %r8d
	movl	%r8d, 4(%rax)
	leaq	8(%r10), %r8
	cmpq	%r8, %rdi
	je	.L1064
	movl	8(%r10), %r8d
	movl	%r8d, 8(%rax)
.L1064:
	addq	%rsi, 16(%r12)
	movq	%rcx, %rax
.L1090:
	movq	%rax, %rcx
	movd	%r14d, %xmm6
	shrq	$2, %rcx
	pshufd	$0, %xmm6, %xmm0
	salq	$4, %rcx
	addq	%rbx, %rcx
	.p2align 4,,10
	.p2align 3
.L1068:
	movups	%xmm0, (%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L1068
	movq	%rax, %rdx
	andq	$-4, %rdx
	leaq	(%rbx,%rdx,4), %r13
	cmpq	%rax, %rdx
	je	.L1041
.L1091:
	leaq	4(%r13), %rax
	movl	%r14d, 0(%r13)
	cmpq	%rax, %rdi
	je	.L1041
	leaq	8(%r13), %rax
	movl	%r14d, 4(%r13)
	cmpq	%rax, %rdi
	jne	.L1160
	jmp	.L1041
	.p2align 4,,10
	.p2align 3
.L1044:
	movq	8(%r12), %rsi
	movl	$536870911, %r8d
	movq	%r8, %rax
	subq	%rsi, %rdi
	sarq	$2, %rdi
	subq	%rdi, %rax
	cmpq	%rax, %rdx
	ja	.L1161
	cmpq	%rdi, %rdx
	movq	%rdi, %rax
	cmovnb	%rdx, %rax
	addq	%rax, %rdi
	setc	%al
	subq	%rsi, %r13
	movzbl	%al, %eax
	testq	%rax, %rax
	jne	.L1097
	testq	%rdi, %rdi
	jne	.L1162
	xorl	%r8d, %r8d
	xorl	%eax, %eax
.L1073:
	leaq	0(,%rdx,4), %r9
	leaq	(%rax,%r13), %r11
	leaq	0(%r13,%r9), %rsi
	addq	%rax, %rsi
	cmpq	%rsi, %rcx
	leaq	4(%rcx), %rsi
	setnb	%dil
	cmpq	%rsi, %r11
	setnb	%sil
	orb	%sil, %dil
	je	.L1099
	leaq	-1(%rdx), %rsi
	cmpq	$3, %rsi
	jbe	.L1099
	movd	(%rcx), %xmm7
	leaq	-4(%rdx), %rdi
	xorl	%esi, %esi
	shrq	$2, %rdi
	addq	$1, %rdi
	pshufd	$0, %xmm7, %xmm0
	.p2align 4,,10
	.p2align 3
.L1077:
	movq	%rsi, %r10
	addq	$1, %rsi
	salq	$4, %r10
	movups	%xmm0, (%r11,%r10)
	cmpq	%rsi, %rdi
	ja	.L1077
	leaq	0(,%rdi,4), %rsi
	movq	%rdx, %r10
	salq	$4, %rdi
	subq	%rsi, %r10
	addq	%r11, %rdi
	cmpq	%rsi, %rdx
	je	.L1079
	movl	(%rcx), %edx
	movl	%edx, (%rdi)
	cmpq	$1, %r10
	je	.L1079
	movl	(%rcx), %edx
	movl	%edx, 4(%rdi)
	cmpq	$2, %r10
	je	.L1079
	movl	(%rcx), %edx
	movl	%edx, 8(%rdi)
.L1079:
	movq	8(%r12), %rcx
	cmpq	%rcx, %rbx
	je	.L1100
	leaq	-4(%rbx), %r10
	leaq	15(%rcx), %rdx
	subq	%rcx, %r10
	subq	%rax, %rdx
	movq	%r10, %rsi
	shrq	$2, %rsi
	cmpq	$30, %rdx
	jbe	.L1101
	movabsq	$4611686018427387900, %rdx
	testq	%rdx, %rsi
	je	.L1101
	addq	$1, %rsi
	xorl	%edx, %edx
	movq	%rsi, %rdi
	shrq	$2, %rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L1082:
	movdqu	(%rcx,%rdx), %xmm3
	movups	%xmm3, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rdi, %rdx
	jne	.L1082
	movq	%rsi, %rdx
	andq	$-4, %rdx
	leaq	0(,%rdx,4), %rdi
	addq	%rdi, %rcx
	addq	%rax, %rdi
	cmpq	%rdx, %rsi
	je	.L1084
	movl	(%rcx), %edx
	movl	%edx, (%rdi)
	leaq	4(%rcx), %rdx
	cmpq	%rdx, %rbx
	je	.L1084
	movl	4(%rcx), %edx
	movl	%edx, 4(%rdi)
	leaq	8(%rcx), %rdx
	cmpq	%rdx, %rbx
	je	.L1084
	movl	8(%rcx), %edx
	movl	%edx, 8(%rdi)
.L1084:
	leaq	4(%rax,%r10), %rdx
.L1080:
	movq	16(%r12), %r10
	leaq	(%rdx,%r9), %rcx
	cmpq	%r10, %rbx
	je	.L1085
	movq	%r10, %rsi
	leaq	16(%rdx,%r9), %rdx
	subq	%rbx, %rsi
	leaq	-4(%rsi), %rdi
	movq	%rdi, %r11
	shrq	$2, %r11
	cmpq	%rdx, %rbx
	leaq	16(%rbx), %rdx
	setnb	%sil
	cmpq	%rdx, %rcx
	setnb	%dl
	orb	%dl, %sil
	je	.L1102
	movabsq	$4611686018427387900, %rdx
	testq	%rdx, %r11
	je	.L1102
	addq	$1, %r11
	xorl	%edx, %edx
	movq	%r11, %rsi
	shrq	$2, %rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L1087:
	movdqu	(%rbx,%rdx), %xmm4
	movups	%xmm4, (%rcx,%rdx)
	addq	$16, %rdx
	cmpq	%rsi, %rdx
	jne	.L1087
	movq	%r11, %rdx
	andq	$-4, %rdx
	leaq	0(,%rdx,4), %r9
	addq	%r9, %rbx
	addq	%rcx, %r9
	cmpq	%rdx, %r11
	je	.L1089
	movl	(%rbx), %edx
	movl	%edx, (%r9)
	leaq	4(%rbx), %rdx
	cmpq	%rdx, %r10
	je	.L1089
	movl	4(%rbx), %edx
	movl	%edx, 4(%r9)
	leaq	8(%rbx), %rdx
	cmpq	%rdx, %r10
	je	.L1089
	movl	8(%rbx), %edx
	movl	%edx, 8(%r9)
.L1089:
	leaq	4(%rcx,%rdi), %rcx
.L1085:
	movq	%rax, %xmm0
	movq	%rcx, %xmm5
	movq	%r8, 24(%r12)
	punpcklqdq	%xmm5, %xmm0
	movups	%xmm0, 8(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1097:
	.cfi_restore_state
	movl	$2147483648, %esi
	movl	$2147483644, %r14d
.L1072:
	movq	(%r12), %r8
	movq	16(%r8), %rax
	movq	24(%r8), %rdi
	subq	%rax, %rdi
	cmpq	%rsi, %rdi
	jb	.L1163
	addq	%rax, %rsi
	movq	%rsi, 16(%r8)
.L1075:
	leaq	(%rax,%r14), %r8
	jmp	.L1073
	.p2align 4,,10
	.p2align 3
.L1099:
	xorl	%esi, %esi
	.p2align 4,,10
	.p2align 3
.L1076:
	movl	(%rcx), %edi
	movl	%edi, (%r11,%rsi,4)
	addq	$1, %rsi
	cmpq	%rsi, %rdx
	jne	.L1076
	jmp	.L1079
	.p2align 4,,10
	.p2align 3
.L1093:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1047:
	movl	(%rdx,%rax,4), %esi
	movl	%esi, (%rdi,%rax,4)
	movq	%rax, %rsi
	addq	$1, %rax
	cmpq	%rsi, %rcx
	jne	.L1047
	jmp	.L1050
	.p2align 4,,10
	.p2align 3
.L1096:
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L1062:
	movl	(%rbx,%rcx,4), %r10d
	movl	%r10d, (%r8,%rcx,4)
	movq	%rcx, %r10
	addq	$1, %rcx
	cmpq	%r10, %rax
	jne	.L1062
	addq	%rsi, 16(%r12)
	addq	$1, %rax
	cmpq	$8, %r9
	jbe	.L1091
	jmp	.L1090
	.p2align 4,,10
	.p2align 3
.L1101:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L1081:
	movl	(%rcx,%rdx,4), %edi
	movl	%edi, (%rax,%rdx,4)
	movq	%rdx, %rdi
	addq	$1, %rdx
	cmpq	%rdi, %rsi
	jne	.L1081
	jmp	.L1084
	.p2align 4,,10
	.p2align 3
.L1102:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L1086:
	movl	(%rbx,%rdx,4), %esi
	movl	%esi, (%rcx,%rdx,4)
	movq	%rdx, %rsi
	addq	$1, %rdx
	cmpq	%rsi, %r11
	jne	.L1086
	jmp	.L1089
	.p2align 4,,10
	.p2align 3
.L1094:
	movq	%rdi, %r8
	jmp	.L1057
	.p2align 4,,10
	.p2align 3
.L1092:
	movq	%rdi, %rax
	jmp	.L1046
	.p2align 4,,10
	.p2align 3
.L1061:
	leaq	(%r8,%rsi), %rax
	movq	%rax, 16(%r12)
	jmp	.L1041
	.p2align 4,,10
	.p2align 3
.L1100:
	movq	%rax, %rdx
	jmp	.L1080
.L1095:
	movq	%rdi, %rax
	movq	%rdx, %r8
	jmp	.L1058
.L1163:
	movq	%r8, %rdi
	movq	%rcx, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rcx
	jmp	.L1075
.L1162:
	cmpq	$536870911, %rdi
	cmova	%r8, %rdi
	leaq	0(,%rdi,4), %r14
	leaq	7(%r14), %rsi
	andq	$-8, %rsi
	jmp	.L1072
.L1161:
	leaq	.LC5(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE21322:
	.size	_ZNSt6vectorIjN2v88internal13ZoneAllocatorIjEEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPjS4_EEmRKj, .-_ZNSt6vectorIjN2v88internal13ZoneAllocatorIjEEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPjS4_EEmRKj
	.section	.text._ZNSt6vectorIN2v88internal4wasm17WasmModuleBuilder15WasmDataSegmentENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal4wasm17WasmModuleBuilder15WasmDataSegmentENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal4wasm17WasmModuleBuilder15WasmDataSegmentENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal4wasm17WasmModuleBuilder15WasmDataSegmentENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, @function
_ZNSt6vectorIN2v88internal4wasm17WasmModuleBuilder15WasmDataSegmentENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_:
.LFB21983:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	movabsq	$-3689348814741910323, %rdx
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r13
	movq	8(%rdi), %r14
	movq	%r13, %rax
	subq	%r14, %rax
	sarq	$3, %rax
	imulq	%rdx, %rax
	cmpq	$53687091, %rax
	je	.L1180
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %rbx
	subq	%r14, %rdx
	testq	%rax, %rax
	je	.L1174
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L1181
	movl	$2147483640, %esi
	movl	$2147483640, %ecx
.L1166:
	movq	(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %r8
	subq	%rax, %r8
	cmpq	%rsi, %r8
	jb	.L1182
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1169:
	leaq	(%rax,%rcx), %rdi
	leaq	40(%rax), %r8
	jmp	.L1167
	.p2align 4,,10
	.p2align 3
.L1181:
	testq	%rcx, %rcx
	jne	.L1183
	movl	$40, %r8d
	xorl	%edi, %edi
	xorl	%eax, %eax
.L1167:
	movq	(%r15), %rcx
	addq	%rax, %rdx
	pxor	%xmm0, %xmm0
	movq	%rcx, (%rdx)
	movq	24(%r15), %rcx
	movdqu	8(%r15), %xmm3
	movq	$0, 24(%r15)
	movq	%rcx, 24(%rdx)
	movups	%xmm3, 8(%rdx)
	movl	32(%r15), %ecx
	movups	%xmm0, 8(%r15)
	movl	%ecx, 32(%rdx)
	cmpq	%r14, %rbx
	je	.L1170
	movq	%r14, %rdx
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L1171:
	movq	(%rdx), %rsi
	addq	$40, %rdx
	addq	$40, %rcx
	movq	%rsi, -40(%rcx)
	movdqu	-32(%rdx), %xmm1
	movups	%xmm1, -32(%rcx)
	movq	-16(%rdx), %rsi
	movq	%rsi, -16(%rcx)
	movl	-8(%rdx), %esi
	movq	$0, -16(%rdx)
	movups	%xmm0, -32(%rdx)
	movl	%esi, -8(%rcx)
	cmpq	%rdx, %rbx
	jne	.L1171
	leaq	-40(%rbx), %rdx
	subq	%r14, %rdx
	shrq	$3, %rdx
	leaq	80(%rax,%rdx,8), %r8
.L1170:
	cmpq	%r13, %rbx
	je	.L1172
	movq	%rbx, %rdx
	movq	%r8, %rcx
	pxor	%xmm0, %xmm0
	.p2align 4,,10
	.p2align 3
.L1173:
	movq	(%rdx), %rsi
	addq	$40, %rdx
	addq	$40, %rcx
	movq	%rsi, -40(%rcx)
	movdqu	-32(%rdx), %xmm2
	movups	%xmm2, -32(%rcx)
	movq	-16(%rdx), %rsi
	movq	%rsi, -16(%rcx)
	movl	-8(%rdx), %esi
	movq	$0, -16(%rdx)
	movups	%xmm0, -32(%rdx)
	movl	%esi, -8(%rcx)
	cmpq	%rdx, %r13
	jne	.L1173
	subq	%rbx, %r13
	leaq	-40(%r13), %rdx
	shrq	$3, %rdx
	leaq	40(%r8,%rdx,8), %r8
.L1172:
	movq	%rax, %xmm0
	movq	%r8, %xmm4
	movq	%rdi, 24(%r12)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, 8(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1174:
	.cfi_restore_state
	movl	$40, %esi
	movl	$40, %ecx
	jmp	.L1166
.L1182:
	movq	%rdx, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rdx
	jmp	.L1169
.L1183:
	cmpq	$53687091, %rcx
	movl	$53687091, %eax
	cmova	%rax, %rcx
	imulq	$40, %rcx, %rcx
	movq	%rcx, %rsi
	jmp	.L1166
.L1180:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE21983:
	.size	_ZNSt6vectorIN2v88internal4wasm17WasmModuleBuilder15WasmDataSegmentENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, .-_ZNSt6vectorIN2v88internal4wasm17WasmModuleBuilder15WasmDataSegmentENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.section	.text._ZN2v88internal4wasm17WasmModuleBuilder14AddDataSegmentEPKhjj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm17WasmModuleBuilder14AddDataSegmentEPKhjj
	.type	_ZN2v88internal4wasm17WasmModuleBuilder14AddDataSegmentEPKhjj, @function
_ZN2v88internal4wasm17WasmModuleBuilder14AddDataSegmentEPKhjj:
.LFB18039:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	216(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movl	%ecx, -64(%rbp)
	movq	$0, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	cmpq	224(%rdi), %rsi
	je	.L1185
	movq	%rax, (%rsi)
	movq	-88(%rbp), %rax
	movq	%rax, 8(%rsi)
	movq	-80(%rbp), %rax
	movq	%rax, 16(%rsi)
	movq	-72(%rbp), %rax
	movq	%rax, 24(%rsi)
	movl	-64(%rbp), %eax
	movl	%eax, 32(%rsi)
	movq	216(%rdi), %rax
	leaq	40(%rax), %r14
	movq	%r14, 216(%rdi)
.L1186:
	testl	%r13d, %r13d
	je	.L1184
	leal	-1(%r13), %eax
	movq	-24(%r14), %rbx
	movl	$2147483648, %ecx
	leaq	1(%r12,%rax), %r15
	jmp	.L1200
	.p2align 4,,10
	.p2align 3
.L1215:
	movzbl	(%r12), %eax
	addq	$1, %r12
	movb	%al, (%rbx)
	movq	-24(%r14), %rax
	leaq	1(%rax), %rbx
	movq	%rbx, -24(%r14)
	cmpq	%r15, %r12
	je	.L1184
.L1200:
	cmpq	%rbx, -16(%r14)
	jne	.L1215
	movq	-32(%r14), %r13
	movq	%rbx, %rdx
	subq	%r13, %rdx
	cmpq	$2147483647, %rdx
	je	.L1216
	testq	%rdx, %rdx
	je	.L1202
	leaq	(%rdx,%rdx), %rax
	movq	%rcx, %rsi
	movl	$2147483647, %r8d
	cmpq	%rax, %rdx
	jbe	.L1217
.L1191:
	movq	-40(%r14), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %r9
	subq	%rax, %r9
	cmpq	%rsi, %r9
	jb	.L1218
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1193:
	movzbl	(%r12), %esi
	addq	%rax, %r8
	leaq	1(%rax), %rdi
	movb	%sil, (%rax,%rdx)
	cmpq	%rbx, %r13
	je	.L1204
	leaq	15(%rax), %rdx
	movq	%rbx, %rsi
	subq	%r13, %rdx
	subq	%r13, %rsi
	cmpq	$30, %rdx
	jbe	.L1205
	leaq	-1(%rsi), %rdx
	cmpq	$14, %rdx
	jbe	.L1205
	movq	%rsi, %rdi
	xorl	%edx, %edx
	andq	$-16, %rdi
	.p2align 4,,10
	.p2align 3
.L1196:
	movdqu	0(%r13,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rdi, %rdx
	jne	.L1196
	movq	%rsi, %rdx
	andq	$-16, %rdx
	addq	%rdx, %r13
	leaq	(%rax,%rdx), %rdi
	cmpq	%rsi, %rdx
	je	.L1198
	movzbl	0(%r13), %edx
	movb	%dl, (%rdi)
	leaq	1(%r13), %rdx
	cmpq	%rdx, %rbx
	je	.L1198
	movzbl	1(%r13), %edx
	movb	%dl, 1(%rdi)
	leaq	2(%r13), %rdx
	cmpq	%rdx, %rbx
	je	.L1198
	movzbl	2(%r13), %edx
	movb	%dl, 2(%rdi)
	leaq	3(%r13), %rdx
	cmpq	%rdx, %rbx
	je	.L1198
	movzbl	3(%r13), %edx
	movb	%dl, 3(%rdi)
	leaq	4(%r13), %rdx
	cmpq	%rdx, %rbx
	je	.L1198
	movzbl	4(%r13), %edx
	movb	%dl, 4(%rdi)
	leaq	5(%r13), %rdx
	cmpq	%rdx, %rbx
	je	.L1198
	movzbl	5(%r13), %edx
	movb	%dl, 5(%rdi)
	leaq	6(%r13), %rdx
	cmpq	%rdx, %rbx
	je	.L1198
	movzbl	6(%r13), %edx
	movb	%dl, 6(%rdi)
	leaq	7(%r13), %rdx
	cmpq	%rdx, %rbx
	je	.L1198
	movzbl	7(%r13), %edx
	movb	%dl, 7(%rdi)
	leaq	8(%r13), %rdx
	cmpq	%rdx, %rbx
	je	.L1198
	movzbl	8(%r13), %edx
	movb	%dl, 8(%rdi)
	leaq	9(%r13), %rdx
	cmpq	%rdx, %rbx
	je	.L1198
	movzbl	9(%r13), %edx
	movb	%dl, 9(%rdi)
	leaq	10(%r13), %rdx
	cmpq	%rdx, %rbx
	je	.L1198
	movzbl	10(%r13), %edx
	movb	%dl, 10(%rdi)
	leaq	11(%r13), %rdx
	cmpq	%rdx, %rbx
	je	.L1198
	movzbl	11(%r13), %edx
	movb	%dl, 11(%rdi)
	leaq	12(%r13), %rdx
	cmpq	%rdx, %rbx
	je	.L1198
	movzbl	12(%r13), %edx
	movb	%dl, 12(%rdi)
	leaq	13(%r13), %rdx
	cmpq	%rdx, %rbx
	je	.L1198
	movzbl	13(%r13), %edx
	movb	%dl, 13(%rdi)
	leaq	14(%r13), %rdx
	cmpq	%rdx, %rbx
	je	.L1198
	movzbl	14(%r13), %edx
	movb	%dl, 14(%rdi)
	.p2align 4,,10
	.p2align 3
.L1198:
	leaq	1(%rax,%rsi), %rbx
.L1194:
	movq	%rax, %xmm0
	movq	%rbx, %xmm2
	addq	$1, %r12
	movq	%r8, -16(%r14)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, -32(%r14)
	cmpq	%r15, %r12
	jne	.L1200
	.p2align 4,,10
	.p2align 3
.L1184:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1219
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1202:
	.cfi_restore_state
	movl	$8, %esi
	movl	$1, %r8d
	jmp	.L1191
	.p2align 4,,10
	.p2align 3
.L1205:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L1195:
	movzbl	0(%r13,%rdx), %edi
	movb	%dil, (%rax,%rdx)
	addq	$1, %rdx
	cmpq	%rsi, %rdx
	jne	.L1195
	jmp	.L1198
	.p2align 4,,10
	.p2align 3
.L1185:
	leaq	-96(%rbp), %rdx
	leaq	200(%rdi), %rdi
	call	_ZNSt6vectorIN2v88internal4wasm17WasmModuleBuilder15WasmDataSegmentENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	movq	216(%rbx), %r14
	jmp	.L1186
	.p2align 4,,10
	.p2align 3
.L1204:
	movq	%rdi, %rbx
	jmp	.L1194
.L1218:
	movq	%r8, -112(%rbp)
	movq	%rdx, -104(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %r8
	movl	$2147483648, %ecx
	jmp	.L1193
.L1217:
	cmpq	%rcx, %rax
	cmovb	%rax, %r8
	leaq	7(%r8), %rsi
	andq	$-8, %rsi
	jmp	.L1191
.L1216:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1219:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18039:
	.size	_ZN2v88internal4wasm17WasmModuleBuilder14AddDataSegmentEPKhjj, .-_ZN2v88internal4wasm17WasmModuleBuilder14AddDataSegmentEPKhjj
	.section	.text._ZNSt6vectorIN2v88internal4wasm17WasmModuleBuilder9WasmTableENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal4wasm17WasmModuleBuilder9WasmTableENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal4wasm17WasmModuleBuilder9WasmTableENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal4wasm17WasmModuleBuilder9WasmTableENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, @function
_ZNSt6vectorIN2v88internal4wasm17WasmModuleBuilder9WasmTableENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_:
.LFB22041:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r13
	movq	8(%rdi), %r14
	movq	%r13, %rax
	subq	%r14, %rax
	sarq	$4, %rax
	cmpq	$134217727, %rax
	je	.L1236
	movq	%rsi, %rcx
	movq	%rdi, %r12
	movq	%rsi, %rbx
	movq	%rdx, %r15
	subq	%r14, %rcx
	testq	%rax, %rax
	je	.L1230
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L1237
	movl	$2147483632, %esi
	movl	$2147483632, %r10d
.L1222:
	movq	(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rsi, %rdx
	jb	.L1238
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1225:
	addq	%rax, %r10
	leaq	16(%rax), %r11
	jmp	.L1223
	.p2align 4,,10
	.p2align 3
.L1237:
	testq	%rdx, %rdx
	jne	.L1239
	movl	$16, %r11d
	xorl	%r10d, %r10d
	xorl	%eax, %eax
.L1223:
	movzbl	(%r15), %edi
	movzbl	12(%r15), %edx
	addq	%rax, %rcx
	movq	4(%r15), %rsi
	movb	%dil, (%rcx)
	movq	%rsi, 4(%rcx)
	movb	%dl, 12(%rcx)
	cmpq	%r14, %rbx
	je	.L1226
	movq	%r14, %rdx
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L1227:
	movzbl	(%rdx), %r9d
	movl	4(%rdx), %r8d
	addq	$16, %rdx
	addq	$16, %rcx
	movl	-8(%rdx), %edi
	movzbl	-4(%rdx), %esi
	movb	%r9b, -16(%rcx)
	movl	%r8d, -12(%rcx)
	movl	%edi, -8(%rcx)
	movb	%sil, -4(%rcx)
	cmpq	%rdx, %rbx
	jne	.L1227
	movq	%rbx, %rdx
	subq	%r14, %rdx
	leaq	16(%rax,%rdx), %r11
.L1226:
	cmpq	%r13, %rbx
	je	.L1228
	movq	%rbx, %rdx
	movq	%r11, %rcx
	.p2align 4,,10
	.p2align 3
.L1229:
	movzbl	(%rdx), %r9d
	movl	4(%rdx), %r8d
	addq	$16, %rdx
	addq	$16, %rcx
	movl	-8(%rdx), %edi
	movzbl	-4(%rdx), %esi
	movb	%r9b, -16(%rcx)
	movl	%r8d, -12(%rcx)
	movl	%edi, -8(%rcx)
	movb	%sil, -4(%rcx)
	cmpq	%rdx, %r13
	jne	.L1229
	subq	%rbx, %r13
	addq	%r13, %r11
.L1228:
	movq	%rax, %xmm0
	movq	%r11, %xmm1
	movq	%r10, 24(%r12)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1230:
	.cfi_restore_state
	movl	$16, %esi
	movl	$16, %r10d
	jmp	.L1222
.L1238:
	movq	%rcx, -64(%rbp)
	movq	%r10, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r10
	movq	-64(%rbp), %rcx
	jmp	.L1225
.L1239:
	cmpq	$134217727, %rdx
	movl	$134217727, %r10d
	cmovbe	%rdx, %r10
	salq	$4, %r10
	movq	%r10, %rsi
	jmp	.L1222
.L1236:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE22041:
	.size	_ZNSt6vectorIN2v88internal4wasm17WasmModuleBuilder9WasmTableENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, .-_ZNSt6vectorIN2v88internal4wasm17WasmModuleBuilder9WasmTableENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.section	.text._ZN2v88internal4wasm17WasmModuleBuilder25AllocateIndirectFunctionsEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm17WasmModuleBuilder25AllocateIndirectFunctionsEj
	.type	_ZN2v88internal4wasm17WasmModuleBuilder25AllocateIndirectFunctionsEj, @function
_ZN2v88internal4wasm17WasmModuleBuilder25AllocateIndirectFunctionsEj:
.LFB18053:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	$-1, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	248(%rdi), %r8
	movl	_ZN2v88internal24FLAG_wasm_max_table_sizeE(%rip), %edx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	240(%rdi), %rcx
	movq	%r8, %rax
	subq	%rcx, %rax
	sarq	$2, %rax
	subl	%eax, %edx
	cmpl	%esi, %edx
	jnb	.L1252
.L1240:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1253
	addq	$40, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1252:
	.cfi_restore_state
	leal	(%rsi,%rax), %edx
	movq	%rdi, %rbx
	movl	%eax, %r13d
	movq	%rdx, %r12
	cmpq	%rax, %rdx
	ja	.L1254
	jnb	.L1243
	leaq	(%rcx,%rdx,4), %rax
	cmpq	%rax, %r8
	je	.L1243
	movq	%rax, 248(%rdi)
.L1243:
	movl	364(%rbx), %eax
	movq	184(%rbx), %rsi
	movq	176(%rbx), %rdx
	testl	%eax, %eax
	cmove	%r12d, %eax
	cmpq	%rdx, %rsi
	je	.L1255
	movl	%r12d, 4(%rdx)
	movq	176(%rbx), %rdx
	movl	%eax, 8(%rdx)
	jmp	.L1240
	.p2align 4,,10
	.p2align 3
.L1254:
	subq	%rax, %rdx
	leaq	232(%rdi), %rdi
	leaq	_ZN2v88internal4wasm15WasmElemSegment10kNullIndexE(%rip), %rcx
	movq	%r8, %rsi
	call	_ZNSt6vectorIjN2v88internal13ZoneAllocatorIjEEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPjS4_EEmRKj
	jmp	.L1243
	.p2align 4,,10
	.p2align 3
.L1255:
	movb	$7, -64(%rbp)
	movl	%r12d, -60(%rbp)
	movl	%eax, -56(%rbp)
	movb	$1, -52(%rbp)
	cmpq	192(%rbx), %rsi
	je	.L1246
	movb	$7, (%rsi)
	movl	%r12d, 4(%rsi)
	movl	%eax, 8(%rsi)
	movb	$1, 12(%rsi)
	addq	$16, 184(%rbx)
	jmp	.L1240
	.p2align 4,,10
	.p2align 3
.L1246:
	leaq	-64(%rbp), %rdx
	leaq	168(%rbx), %rdi
	call	_ZNSt6vectorIN2v88internal4wasm17WasmModuleBuilder9WasmTableENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1240
.L1253:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18053:
	.size	_ZN2v88internal4wasm17WasmModuleBuilder25AllocateIndirectFunctionsEj, .-_ZN2v88internal4wasm17WasmModuleBuilder25AllocateIndirectFunctionsEj
	.section	.text._ZN2v88internal4wasm17WasmModuleBuilder8AddTableENS1_9ValueTypeEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm17WasmModuleBuilder8AddTableENS1_9ValueTypeEj
	.type	_ZN2v88internal4wasm17WasmModuleBuilder8AddTableENS1_9ValueTypeEj, @function
_ZN2v88internal4wasm17WasmModuleBuilder8AddTableENS1_9ValueTypeEj:
.LFB18056:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rcx
	movq	%rcx, -24(%rbp)
	xorl	%ecx, %ecx
	movb	%sil, -48(%rbp)
	movq	184(%rdi), %rsi
	movl	%edx, -44(%rbp)
	movl	$0, -40(%rbp)
	movb	$0, -36(%rbp)
	cmpq	192(%rdi), %rsi
	je	.L1257
	movb	%al, (%rsi)
	movl	%edx, 4(%rsi)
	movl	$0, 8(%rsi)
	movb	$0, 12(%rsi)
	movq	184(%rdi), %rax
	addq	$16, %rax
	movq	%rax, 184(%rdi)
.L1258:
	subq	176(%rbx), %rax
	sarq	$4, %rax
	subl	$1, %eax
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1261
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1257:
	.cfi_restore_state
	leaq	-48(%rbp), %rdx
	leaq	168(%rdi), %rdi
	call	_ZNSt6vectorIN2v88internal4wasm17WasmModuleBuilder9WasmTableENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	movq	184(%rbx), %rax
	jmp	.L1258
.L1261:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18056:
	.size	_ZN2v88internal4wasm17WasmModuleBuilder8AddTableENS1_9ValueTypeEj, .-_ZN2v88internal4wasm17WasmModuleBuilder8AddTableENS1_9ValueTypeEj
	.section	.text._ZN2v88internal4wasm17WasmModuleBuilder8AddTableENS1_9ValueTypeEjj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm17WasmModuleBuilder8AddTableENS1_9ValueTypeEjj
	.type	_ZN2v88internal4wasm17WasmModuleBuilder8AddTableENS1_9ValueTypeEjj, @function
_ZN2v88internal4wasm17WasmModuleBuilder8AddTableENS1_9ValueTypeEjj:
.LFB18057:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rdi
	movq	%rdi, -24(%rbp)
	xorl	%edi, %edi
	movb	%sil, -48(%rbp)
	movq	184(%rbx), %rsi
	movl	%edx, -44(%rbp)
	movl	%ecx, -40(%rbp)
	movb	$1, -36(%rbp)
	cmpq	192(%rbx), %rsi
	je	.L1263
	movb	%al, (%rsi)
	movl	%edx, 4(%rsi)
	movl	%ecx, 8(%rsi)
	movb	$1, 12(%rsi)
	movq	184(%rbx), %rax
	addq	$16, %rax
	movq	%rax, 184(%rbx)
.L1264:
	subq	176(%rbx), %rax
	sarq	$4, %rax
	subl	$1, %eax
	movq	-24(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L1267
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1263:
	.cfi_restore_state
	leaq	-48(%rbp), %rdx
	leaq	168(%rbx), %rdi
	call	_ZNSt6vectorIN2v88internal4wasm17WasmModuleBuilder9WasmTableENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	movq	184(%rbx), %rax
	jmp	.L1264
.L1267:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18057:
	.size	_ZN2v88internal4wasm17WasmModuleBuilder8AddTableENS1_9ValueTypeEjj, .-_ZN2v88internal4wasm17WasmModuleBuilder8AddTableENS1_9ValueTypeEjj
	.section	.text._ZNSt6vectorIN2v88internal4wasm17WasmModuleBuilder18WasmFunctionImportENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal4wasm17WasmModuleBuilder18WasmFunctionImportENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal4wasm17WasmModuleBuilder18WasmFunctionImportENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal4wasm17WasmModuleBuilder18WasmFunctionImportENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, @function
_ZNSt6vectorIN2v88internal4wasm17WasmModuleBuilder18WasmFunctionImportENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_:
.LFB22046:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	movabsq	$-6148914691236517205, %rdx
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r12
	movq	8(%rdi), %r14
	movq	%r12, %rax
	subq	%r14, %rax
	sarq	$3, %rax
	imulq	%rdx, %rax
	cmpq	$89478485, %rax
	je	.L1284
	movq	%rsi, %rdx
	movq	%rdi, %r13
	movq	%rsi, %rbx
	subq	%r14, %rdx
	testq	%rax, %rax
	je	.L1278
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L1285
	movl	$2147483640, %esi
	movl	$2147483640, %r8d
.L1270:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	%rsi, %rcx
	jb	.L1286
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1273:
	addq	%rax, %r8
	leaq	24(%rax), %rdi
	jmp	.L1271
	.p2align 4,,10
	.p2align 3
.L1285:
	testq	%rcx, %rcx
	jne	.L1287
	movl	$24, %edi
	xorl	%r8d, %r8d
	xorl	%eax, %eax
.L1271:
	movdqu	(%r15), %xmm3
	movups	%xmm3, (%rax,%rdx)
	movq	16(%r15), %rcx
	movq	%rcx, 16(%rax,%rdx)
	cmpq	%r14, %rbx
	je	.L1274
	movq	%r14, %rdx
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L1275:
	movdqu	(%rdx), %xmm1
	addq	$24, %rdx
	addq	$24, %rcx
	movups	%xmm1, -24(%rcx)
	movq	-8(%rdx), %rsi
	movq	%rsi, -8(%rcx)
	cmpq	%rdx, %rbx
	jne	.L1275
	leaq	-24(%rbx), %rdx
	subq	%r14, %rdx
	shrq	$3, %rdx
	leaq	48(%rax,%rdx,8), %rdi
.L1274:
	cmpq	%r12, %rbx
	je	.L1276
	movq	%rbx, %rdx
	movq	%rdi, %rcx
	.p2align 4,,10
	.p2align 3
.L1277:
	movdqu	(%rdx), %xmm2
	addq	$24, %rdx
	addq	$24, %rcx
	movups	%xmm2, -24(%rcx)
	movq	-8(%rdx), %rsi
	movq	%rsi, -8(%rcx)
	cmpq	%rdx, %r12
	jne	.L1277
	subq	%rbx, %r12
	leaq	-24(%r12), %rdx
	shrq	$3, %rdx
	leaq	24(%rdi,%rdx,8), %rdi
.L1276:
	movq	%rax, %xmm0
	movq	%rdi, %xmm4
	movq	%r8, 24(%r13)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, 8(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1278:
	.cfi_restore_state
	movl	$24, %esi
	movl	$24, %r8d
	jmp	.L1270
.L1286:
	movq	%rdx, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %rdx
	jmp	.L1273
.L1287:
	cmpq	$89478485, %rcx
	movl	$89478485, %r8d
	cmovbe	%rcx, %r8
	imulq	$24, %r8, %r8
	movq	%r8, %rsi
	jmp	.L1270
.L1284:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE22046:
	.size	_ZNSt6vectorIN2v88internal4wasm17WasmModuleBuilder18WasmFunctionImportENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, .-_ZNSt6vectorIN2v88internal4wasm17WasmModuleBuilder18WasmFunctionImportENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.section	.text._ZNSt6vectorIN2v88internal4wasm17WasmModuleBuilder16WasmGlobalImportENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal4wasm17WasmModuleBuilder16WasmGlobalImportENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal4wasm17WasmModuleBuilder16WasmGlobalImportENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal4wasm17WasmModuleBuilder16WasmGlobalImportENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, @function
_ZNSt6vectorIN2v88internal4wasm17WasmModuleBuilder16WasmGlobalImportENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_:
.LFB22051:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	movabsq	$-6148914691236517205, %rdx
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r12
	movq	8(%rdi), %r14
	movq	%r12, %rax
	subq	%r14, %rax
	sarq	$3, %rax
	imulq	%rdx, %rax
	cmpq	$89478485, %rax
	je	.L1304
	movq	%rsi, %rdx
	movq	%rdi, %r13
	movq	%rsi, %rbx
	subq	%r14, %rdx
	testq	%rax, %rax
	je	.L1298
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L1305
	movl	$2147483640, %esi
	movl	$2147483640, %r8d
.L1290:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	%rsi, %rcx
	jb	.L1306
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1293:
	addq	%rax, %r8
	leaq	24(%rax), %rdi
	jmp	.L1291
	.p2align 4,,10
	.p2align 3
.L1305:
	testq	%rcx, %rcx
	jne	.L1307
	movl	$24, %edi
	xorl	%r8d, %r8d
	xorl	%eax, %eax
.L1291:
	movdqu	(%r15), %xmm3
	movups	%xmm3, (%rax,%rdx)
	movq	16(%r15), %rcx
	movq	%rcx, 16(%rax,%rdx)
	cmpq	%r14, %rbx
	je	.L1294
	movq	%r14, %rdx
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L1295:
	movdqu	(%rdx), %xmm1
	addq	$24, %rdx
	addq	$24, %rcx
	movups	%xmm1, -24(%rcx)
	movq	-8(%rdx), %rsi
	movq	%rsi, -8(%rcx)
	cmpq	%rdx, %rbx
	jne	.L1295
	leaq	-24(%rbx), %rdx
	subq	%r14, %rdx
	shrq	$3, %rdx
	leaq	48(%rax,%rdx,8), %rdi
.L1294:
	cmpq	%r12, %rbx
	je	.L1296
	movq	%rbx, %rdx
	movq	%rdi, %rcx
	.p2align 4,,10
	.p2align 3
.L1297:
	movdqu	(%rdx), %xmm2
	addq	$24, %rdx
	addq	$24, %rcx
	movups	%xmm2, -24(%rcx)
	movq	-8(%rdx), %rsi
	movq	%rsi, -8(%rcx)
	cmpq	%rdx, %r12
	jne	.L1297
	subq	%rbx, %r12
	leaq	-24(%r12), %rdx
	shrq	$3, %rdx
	leaq	24(%rdi,%rdx,8), %rdi
.L1296:
	movq	%rax, %xmm0
	movq	%rdi, %xmm4
	movq	%r8, 24(%r13)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, 8(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1298:
	.cfi_restore_state
	movl	$24, %esi
	movl	$24, %r8d
	jmp	.L1290
.L1306:
	movq	%rdx, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %rdx
	jmp	.L1293
.L1307:
	cmpq	$89478485, %rcx
	movl	$89478485, %r8d
	cmovbe	%rcx, %r8
	imulq	$24, %r8, %r8
	movq	%r8, %rsi
	jmp	.L1290
.L1304:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE22051:
	.size	_ZNSt6vectorIN2v88internal4wasm17WasmModuleBuilder16WasmGlobalImportENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, .-_ZNSt6vectorIN2v88internal4wasm17WasmModuleBuilder16WasmGlobalImportENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.section	.text._ZN2v88internal4wasm17WasmModuleBuilder15AddGlobalImportENS0_6VectorIKcEENS1_9ValueTypeEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm17WasmModuleBuilder15AddGlobalImportENS0_6VectorIKcEENS1_9ValueTypeEb
	.type	_ZN2v88internal4wasm17WasmModuleBuilder15AddGlobalImportENS0_6VectorIKcEENS1_9ValueTypeEb, @function
_ZN2v88internal4wasm17WasmModuleBuilder15AddGlobalImportENS0_6VectorIKcEENS1_9ValueTypeEb:
.LFB18059:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rdi
	movq	%rdi, -24(%rbp)
	xorl	%edi, %edi
	movq	%rsi, -48(%rbp)
	movq	%rdx, -40(%rbp)
	cmpb	$9, %cl
	ja	.L1309
	leaq	.L1311(%rip), %rdx
	movzbl	%cl, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm17WasmModuleBuilder15AddGlobalImportENS0_6VectorIKcEENS1_9ValueTypeEb,"a",@progbits
	.align 4
	.align 4
.L1311:
	.long	.L1319-.L1311
	.long	.L1318-.L1311
	.long	.L1317-.L1311
	.long	.L1323-.L1311
	.long	.L1315-.L1311
	.long	.L1314-.L1311
	.long	.L1313-.L1311
	.long	.L1312-.L1311
	.long	.L1309-.L1311
	.long	.L1310-.L1311
	.section	.text._ZN2v88internal4wasm17WasmModuleBuilder15AddGlobalImportENS0_6VectorIKcEENS1_9ValueTypeEb
	.p2align 4,,10
	.p2align 3
.L1310:
	movl	$104, %eax
	.p2align 4,,10
	.p2align 3
.L1316:
	movb	%al, -32(%rbp)
	movq	88(%rbx), %rsi
	movb	%r8b, -31(%rbp)
	cmpq	96(%rbx), %rsi
	je	.L1320
	movdqa	-48(%rbp), %xmm0
	movups	%xmm0, (%rsi)
	movq	-32(%rbp), %rax
	movq	%rax, 16(%rsi)
	movq	88(%rbx), %rax
	addq	$24, %rax
	movq	%rax, 88(%rbx)
.L1321:
	subq	80(%rbx), %rax
	sarq	$3, %rax
	imull	$-1431655765, %eax, %eax
	subl	$1, %eax
	movq	-24(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L1325
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1319:
	.cfi_restore_state
	movl	$64, %eax
	jmp	.L1316
	.p2align 4,,10
	.p2align 3
.L1315:
	movl	$124, %eax
	jmp	.L1316
	.p2align 4,,10
	.p2align 3
.L1314:
	movl	$123, %eax
	jmp	.L1316
	.p2align 4,,10
	.p2align 3
.L1313:
	movl	$111, %eax
	jmp	.L1316
	.p2align 4,,10
	.p2align 3
.L1312:
	movl	$112, %eax
	jmp	.L1316
	.p2align 4,,10
	.p2align 3
.L1318:
	movl	$127, %eax
	jmp	.L1316
	.p2align 4,,10
	.p2align 3
.L1323:
	movl	$125, %eax
	jmp	.L1316
	.p2align 4,,10
	.p2align 3
.L1317:
	movl	$126, %eax
	jmp	.L1316
	.p2align 4,,10
	.p2align 3
.L1320:
	leaq	-48(%rbp), %rdx
	leaq	72(%rbx), %rdi
	call	_ZNSt6vectorIN2v88internal4wasm17WasmModuleBuilder16WasmGlobalImportENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	movq	88(%rbx), %rax
	jmp	.L1321
.L1309:
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1325:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18059:
	.size	_ZN2v88internal4wasm17WasmModuleBuilder15AddGlobalImportENS0_6VectorIKcEENS1_9ValueTypeEb, .-_ZN2v88internal4wasm17WasmModuleBuilder15AddGlobalImportENS0_6VectorIKcEENS1_9ValueTypeEb
	.section	.text._ZNSt6vectorIN2v88internal4wasm17WasmModuleBuilder10WasmExportENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal4wasm17WasmModuleBuilder10WasmExportENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal4wasm17WasmModuleBuilder10WasmExportENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal4wasm17WasmModuleBuilder10WasmExportENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, @function
_ZNSt6vectorIN2v88internal4wasm17WasmModuleBuilder10WasmExportENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_:
.LFB22056:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	movabsq	$-6148914691236517205, %rdx
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r12
	movq	8(%rdi), %r14
	movq	%r12, %rax
	subq	%r14, %rax
	sarq	$3, %rax
	imulq	%rdx, %rax
	cmpq	$89478485, %rax
	je	.L1342
	movq	%rsi, %rdx
	movq	%rdi, %r13
	movq	%rsi, %rbx
	subq	%r14, %rdx
	testq	%rax, %rax
	je	.L1336
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L1343
	movl	$2147483640, %esi
	movl	$2147483640, %r8d
.L1328:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	%rsi, %rcx
	jb	.L1344
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1331:
	addq	%rax, %r8
	leaq	24(%rax), %rdi
	jmp	.L1329
	.p2align 4,,10
	.p2align 3
.L1343:
	testq	%rcx, %rcx
	jne	.L1345
	movl	$24, %edi
	xorl	%r8d, %r8d
	xorl	%eax, %eax
.L1329:
	movdqu	(%r15), %xmm3
	movups	%xmm3, (%rax,%rdx)
	movq	16(%r15), %rcx
	movq	%rcx, 16(%rax,%rdx)
	cmpq	%r14, %rbx
	je	.L1332
	movq	%r14, %rdx
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L1333:
	movdqu	(%rdx), %xmm1
	addq	$24, %rdx
	addq	$24, %rcx
	movups	%xmm1, -24(%rcx)
	movq	-8(%rdx), %rsi
	movq	%rsi, -8(%rcx)
	cmpq	%rdx, %rbx
	jne	.L1333
	leaq	-24(%rbx), %rdx
	subq	%r14, %rdx
	shrq	$3, %rdx
	leaq	48(%rax,%rdx,8), %rdi
.L1332:
	cmpq	%r12, %rbx
	je	.L1334
	movq	%rbx, %rdx
	movq	%rdi, %rcx
	.p2align 4,,10
	.p2align 3
.L1335:
	movdqu	(%rdx), %xmm2
	addq	$24, %rdx
	addq	$24, %rcx
	movups	%xmm2, -24(%rcx)
	movq	-8(%rdx), %rsi
	movq	%rsi, -8(%rcx)
	cmpq	%rdx, %r12
	jne	.L1335
	subq	%rbx, %r12
	leaq	-24(%r12), %rdx
	shrq	$3, %rdx
	leaq	24(%rdi,%rdx,8), %rdi
.L1334:
	movq	%rax, %xmm0
	movq	%rdi, %xmm4
	movq	%r8, 24(%r13)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, 8(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1336:
	.cfi_restore_state
	movl	$24, %esi
	movl	$24, %r8d
	jmp	.L1328
.L1344:
	movq	%rdx, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %rdx
	jmp	.L1331
.L1345:
	cmpq	$89478485, %rcx
	movl	$89478485, %r8d
	cmovbe	%rcx, %r8
	imulq	$24, %r8, %r8
	movq	%r8, %rsi
	jmp	.L1328
.L1342:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE22056:
	.size	_ZNSt6vectorIN2v88internal4wasm17WasmModuleBuilder10WasmExportENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, .-_ZNSt6vectorIN2v88internal4wasm17WasmModuleBuilder10WasmExportENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.section	.text._ZN2v88internal4wasm17WasmModuleBuilder9AddExportENS0_6VectorIKcEENS1_20ImportExportKindCodeEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm17WasmModuleBuilder9AddExportENS0_6VectorIKcEENS1_20ImportExportKindCodeEj
	.type	_ZN2v88internal4wasm17WasmModuleBuilder9AddExportENS0_6VectorIKcEENS1_20ImportExportKindCodeEj, @function
_ZN2v88internal4wasm17WasmModuleBuilder9AddExportENS0_6VectorIKcEENS1_20ImportExportKindCodeEj:
.LFB18061:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -32(%rbp)
	movq	120(%rdi), %rsi
	movq	%rdx, -24(%rbp)
	movb	%cl, -16(%rbp)
	movl	%r8d, -12(%rbp)
	cmpq	128(%rdi), %rsi
	je	.L1347
	movdqa	-32(%rbp), %xmm0
	movups	%xmm0, (%rsi)
	movq	-16(%rbp), %rax
	movq	%rax, 16(%rsi)
	addq	$24, 120(%rdi)
.L1346:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1351
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1347:
	.cfi_restore_state
	leaq	-32(%rbp), %rdx
	addq	$104, %rdi
	call	_ZNSt6vectorIN2v88internal4wasm17WasmModuleBuilder10WasmExportENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1346
.L1351:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18061:
	.size	_ZN2v88internal4wasm17WasmModuleBuilder9AddExportENS0_6VectorIKcEENS1_20ImportExportKindCodeEj, .-_ZN2v88internal4wasm17WasmModuleBuilder9AddExportENS0_6VectorIKcEENS1_20ImportExportKindCodeEj
	.section	.text._ZN2v88internal4wasm17WasmModuleBuilder22ExportImportedFunctionENS0_6VectorIKcEEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm17WasmModuleBuilder22ExportImportedFunctionENS0_6VectorIKcEEi
	.type	_ZN2v88internal4wasm17WasmModuleBuilder22ExportImportedFunctionENS0_6VectorIKcEEi, @function
_ZN2v88internal4wasm17WasmModuleBuilder22ExportImportedFunctionENS0_6VectorIKcEEi:
.LFB18063:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	56(%rdi), %rax
	subq	48(%rdi), %rax
	movq	%rsi, -32(%rbp)
	sarq	$3, %rax
	movq	%rdx, -24(%rbp)
	movq	120(%rdi), %rsi
	imull	$1431655765, %eax, %eax
	movb	$0, -16(%rbp)
	addl	%ecx, %eax
	movl	%eax, -12(%rbp)
	cmpq	128(%rdi), %rsi
	je	.L1353
	movdqa	-32(%rbp), %xmm0
	movups	%xmm0, (%rsi)
	movq	-16(%rbp), %rax
	movq	%rax, 16(%rsi)
	addq	$24, 120(%rdi)
.L1352:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1357
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1353:
	.cfi_restore_state
	leaq	-32(%rbp), %rdx
	addq	$104, %rdi
	call	_ZNSt6vectorIN2v88internal4wasm17WasmModuleBuilder10WasmExportENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1352
.L1357:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18063:
	.size	_ZN2v88internal4wasm17WasmModuleBuilder22ExportImportedFunctionENS0_6VectorIKcEEi, .-_ZN2v88internal4wasm17WasmModuleBuilder22ExportImportedFunctionENS0_6VectorIKcEEi
	.section	.text._ZNSt6vectorIN2v88internal4wasm17WasmModuleBuilder10WasmGlobalENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal4wasm17WasmModuleBuilder10WasmGlobalENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal4wasm17WasmModuleBuilder10WasmGlobalENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal4wasm17WasmModuleBuilder10WasmGlobalENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, @function
_ZNSt6vectorIN2v88internal4wasm17WasmModuleBuilder10WasmGlobalENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_:
.LFB22061:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	movabsq	$-6148914691236517205, %rdx
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r12
	movq	8(%rdi), %r14
	movq	%r12, %rax
	subq	%r14, %rax
	sarq	$3, %rax
	imulq	%rdx, %rax
	cmpq	$89478485, %rax
	je	.L1374
	movq	%rsi, %rdx
	movq	%rdi, %r13
	movq	%rsi, %rbx
	subq	%r14, %rdx
	testq	%rax, %rax
	je	.L1368
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L1375
	movl	$2147483640, %esi
	movl	$2147483640, %r8d
.L1360:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	%rsi, %rcx
	jb	.L1376
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1363:
	addq	%rax, %r8
	leaq	24(%rax), %rdi
	jmp	.L1361
	.p2align 4,,10
	.p2align 3
.L1375:
	testq	%rcx, %rcx
	jne	.L1377
	movl	$24, %edi
	xorl	%r8d, %r8d
	xorl	%eax, %eax
.L1361:
	movdqu	(%r15), %xmm3
	movups	%xmm3, (%rax,%rdx)
	movq	16(%r15), %rcx
	movq	%rcx, 16(%rax,%rdx)
	cmpq	%r14, %rbx
	je	.L1364
	movq	%r14, %rdx
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L1365:
	movdqu	(%rdx), %xmm1
	addq	$24, %rdx
	addq	$24, %rcx
	movups	%xmm1, -24(%rcx)
	movq	-8(%rdx), %rsi
	movq	%rsi, -8(%rcx)
	cmpq	%rdx, %rbx
	jne	.L1365
	leaq	-24(%rbx), %rdx
	subq	%r14, %rdx
	shrq	$3, %rdx
	leaq	48(%rax,%rdx,8), %rdi
.L1364:
	cmpq	%r12, %rbx
	je	.L1366
	movq	%rbx, %rdx
	movq	%rdi, %rcx
	.p2align 4,,10
	.p2align 3
.L1367:
	movdqu	(%rdx), %xmm2
	addq	$24, %rdx
	addq	$24, %rcx
	movups	%xmm2, -24(%rcx)
	movq	-8(%rdx), %rsi
	movq	%rsi, -8(%rcx)
	cmpq	%rdx, %r12
	jne	.L1367
	subq	%rbx, %r12
	leaq	-24(%r12), %rdx
	shrq	$3, %rdx
	leaq	24(%rdi,%rdx,8), %rdi
.L1366:
	movq	%rax, %xmm0
	movq	%rdi, %xmm4
	movq	%r8, 24(%r13)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, 8(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1368:
	.cfi_restore_state
	movl	$24, %esi
	movl	$24, %r8d
	jmp	.L1360
.L1376:
	movq	%rdx, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %rdx
	jmp	.L1363
.L1377:
	cmpq	$89478485, %rcx
	movl	$89478485, %r8d
	cmovbe	%rcx, %r8
	imulq	$24, %r8, %r8
	movq	%r8, %rsi
	jmp	.L1360
.L1374:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE22061:
	.size	_ZNSt6vectorIN2v88internal4wasm17WasmModuleBuilder10WasmGlobalENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, .-_ZNSt6vectorIN2v88internal4wasm17WasmModuleBuilder10WasmGlobalENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.section	.text._ZN2v88internal4wasm17WasmModuleBuilder17AddExportedGlobalENS1_9ValueTypeEbRKNS1_12WasmInitExprENS0_6VectorIKcEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm17WasmModuleBuilder17AddExportedGlobalENS1_9ValueTypeEbRKNS1_12WasmInitExprENS0_6VectorIKcEE
	.type	_ZN2v88internal4wasm17WasmModuleBuilder17AddExportedGlobalENS1_9ValueTypeEbRKNS1_12WasmInitExprENS0_6VectorIKcEE, @function
_ZN2v88internal4wasm17WasmModuleBuilder17AddExportedGlobalENS1_9ValueTypeEbRKNS1_12WasmInitExprENS0_6VectorIKcEE:
.LFB18062:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%r9, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$56, %rsp
	movdqu	(%rcx), %xmm0
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movb	%sil, -64(%rbp)
	movq	280(%rdi), %rsi
	movb	%dl, -63(%rbp)
	movups	%xmm0, -56(%rbp)
	cmpq	288(%rdi), %rsi
	je	.L1379
	movdqa	-64(%rbp), %xmm1
	movups	%xmm1, (%rsi)
	movq	-48(%rbp), %rax
	movq	%rax, 16(%rsi)
	movq	280(%rdi), %rax
	addq	$24, %rax
	movq	%rax, 280(%rdi)
.L1380:
	subq	272(%rbx), %rax
	movq	120(%rbx), %rsi
	movq	%r13, -64(%rbp)
	sarq	$3, %rax
	movq	%r12, -56(%rbp)
	imull	$-1431655765, %eax, %eax
	movb	$3, -48(%rbp)
	subl	$1, %eax
	movl	%eax, -44(%rbp)
	cmpq	128(%rbx), %rsi
	je	.L1381
	movdqa	-64(%rbp), %xmm2
	movups	%xmm2, (%rsi)
	movq	-48(%rbp), %rdx
	movq	%rdx, 16(%rsi)
	addq	$24, 120(%rbx)
.L1378:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1385
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1379:
	.cfi_restore_state
	leaq	-64(%rbp), %rdx
	leaq	264(%rdi), %rdi
	call	_ZNSt6vectorIN2v88internal4wasm17WasmModuleBuilder10WasmGlobalENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	movq	280(%rbx), %rax
	jmp	.L1380
	.p2align 4,,10
	.p2align 3
.L1381:
	leaq	-64(%rbp), %rdx
	leaq	104(%rbx), %rdi
	movl	%eax, -68(%rbp)
	call	_ZNSt6vectorIN2v88internal4wasm17WasmModuleBuilder10WasmExportENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	movl	-68(%rbp), %eax
	jmp	.L1378
.L1385:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18062:
	.size	_ZN2v88internal4wasm17WasmModuleBuilder17AddExportedGlobalENS1_9ValueTypeEbRKNS1_12WasmInitExprENS0_6VectorIKcEE, .-_ZN2v88internal4wasm17WasmModuleBuilder17AddExportedGlobalENS1_9ValueTypeEbRKNS1_12WasmInitExprENS0_6VectorIKcEE
	.section	.text._ZN2v88internal4wasm17WasmModuleBuilder9AddGlobalENS1_9ValueTypeEbRKNS1_12WasmInitExprE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm17WasmModuleBuilder9AddGlobalENS1_9ValueTypeEbRKNS1_12WasmInitExprE
	.type	_ZN2v88internal4wasm17WasmModuleBuilder9AddGlobalENS1_9ValueTypeEbRKNS1_12WasmInitExprE, @function
_ZN2v88internal4wasm17WasmModuleBuilder9AddGlobalENS1_9ValueTypeEbRKNS1_12WasmInitExprE:
.LFB18064:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$40, %rsp
	movdqu	(%rcx), %xmm0
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movb	%sil, -48(%rbp)
	movq	280(%rdi), %rsi
	movb	%dl, -47(%rbp)
	movups	%xmm0, -40(%rbp)
	cmpq	288(%rdi), %rsi
	je	.L1387
	movdqa	-48(%rbp), %xmm1
	movups	%xmm1, (%rsi)
	movq	-32(%rbp), %rax
	movq	%rax, 16(%rsi)
	movq	280(%rdi), %rax
	addq	$24, %rax
	movq	%rax, 280(%rdi)
.L1388:
	subq	272(%rbx), %rax
	sarq	$3, %rax
	imull	$-1431655765, %eax, %eax
	subl	$1, %eax
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1391
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1387:
	.cfi_restore_state
	leaq	-48(%rbp), %rdx
	leaq	264(%rdi), %rdi
	call	_ZNSt6vectorIN2v88internal4wasm17WasmModuleBuilder10WasmGlobalENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	movq	280(%rbx), %rax
	jmp	.L1388
.L1391:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18064:
	.size	_ZN2v88internal4wasm17WasmModuleBuilder9AddGlobalENS1_9ValueTypeEbRKNS1_12WasmInitExprE, .-_ZN2v88internal4wasm17WasmModuleBuilder9AddGlobalENS1_9ValueTypeEbRKNS1_12WasmInitExprE
	.section	.text._ZNKSt10_HashtableIN2v88internal9SignatureINS1_4wasm9ValueTypeEEESt4pairIKS5_jENS1_13ZoneAllocatorIS8_EENSt8__detail10_Select1stESt8equal_toIS5_ENS0_4base4hashIS5_EENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmRS7_m,"axG",@progbits,_ZNKSt10_HashtableIN2v88internal9SignatureINS1_4wasm9ValueTypeEEESt4pairIKS5_jENS1_13ZoneAllocatorIS8_EENSt8__detail10_Select1stESt8equal_toIS5_ENS0_4base4hashIS5_EENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmRS7_m,comdat
	.align 2
	.p2align 4
	.weak	_ZNKSt10_HashtableIN2v88internal9SignatureINS1_4wasm9ValueTypeEEESt4pairIKS5_jENS1_13ZoneAllocatorIS8_EENSt8__detail10_Select1stESt8equal_toIS5_ENS0_4base4hashIS5_EENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmRS7_m
	.type	_ZNKSt10_HashtableIN2v88internal9SignatureINS1_4wasm9ValueTypeEEESt4pairIKS5_jENS1_13ZoneAllocatorIS8_EENSt8__detail10_Select1stESt8equal_toIS5_ENS0_4base4hashIS5_EENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmRS7_m, @function
_ZNKSt10_HashtableIN2v88internal9SignatureINS1_4wasm9ValueTypeEEESt4pairIKS5_jENS1_13ZoneAllocatorIS8_EENSt8__detail10_Select1stESt8equal_toIS5_ENS0_4base4hashIS5_EENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmRS7_m:
.LFB22293:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	(%rax,%rsi,8), %r10
	testq	%r10, %r10
	je	.L1403
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	(%r10), %rax
	movq	40(%rax), %r8
	cmpq	%rcx, %r8
	je	.L1406
	.p2align 4,,10
	.p2align 3
.L1394:
	movq	(%rax), %r9
	testq	%r9, %r9
	je	.L1397
	movq	40(%r9), %r8
	movq	%rax, %r10
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	16(%rdi)
	cmpq	%rdx, %rsi
	jne	.L1397
	movq	%r9, %rax
	cmpq	%rcx, %r8
	jne	.L1394
.L1406:
	leaq	8(%rax), %rdx
	cmpq	%rdx, %r11
	je	.L1392
	movq	8(%r11), %r9
	cmpq	16(%rax), %r9
	jne	.L1394
	movq	(%r11), %rbx
	cmpq	8(%rax), %rbx
	jne	.L1394
	movq	16(%r11), %rdx
	addq	%rbx, %r9
	movq	24(%rax), %r8
	addq	%rdx, %r9
	cmpq	%r9, %rdx
	je	.L1392
	.p2align 4,,10
	.p2align 3
.L1395:
	movzbl	(%r8), %ebx
	cmpb	%bl, (%rdx)
	jne	.L1394
	addq	$1, %rdx
	addq	$1, %r8
	cmpq	%rdx, %r9
	jne	.L1395
.L1392:
	movq	%r10, %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1397:
	.cfi_restore_state
	xorl	%r10d, %r10d
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	%r10, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1403:
	.cfi_restore 3
	.cfi_restore 6
	movq	%r10, %rax
	ret
	.cfi_endproc
.LFE22293:
	.size	_ZNKSt10_HashtableIN2v88internal9SignatureINS1_4wasm9ValueTypeEEESt4pairIKS5_jENS1_13ZoneAllocatorIS8_EENSt8__detail10_Select1stESt8equal_toIS5_ENS0_4base4hashIS5_EENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmRS7_m, .-_ZNKSt10_HashtableIN2v88internal9SignatureINS1_4wasm9ValueTypeEEESt4pairIKS5_jENS1_13ZoneAllocatorIS8_EENSt8__detail10_Select1stESt8equal_toIS5_ENS0_4base4hashIS5_EENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmRS7_m
	.section	.text._ZNSt10_HashtableIN2v88internal9SignatureINS1_4wasm9ValueTypeEEESt4pairIKS5_jENS1_13ZoneAllocatorIS8_EENSt8__detail10_Select1stESt8equal_toIS5_ENS0_4base4hashIS5_EENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS8_Lb1EEEm,"axG",@progbits,_ZNSt10_HashtableIN2v88internal9SignatureINS1_4wasm9ValueTypeEEESt4pairIKS5_jENS1_13ZoneAllocatorIS8_EENSt8__detail10_Select1stESt8equal_toIS5_ENS0_4base4hashIS5_EENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS8_Lb1EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIN2v88internal9SignatureINS1_4wasm9ValueTypeEEESt4pairIKS5_jENS1_13ZoneAllocatorIS8_EENSt8__detail10_Select1stESt8equal_toIS5_ENS0_4base4hashIS5_EENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS8_Lb1EEEm
	.type	_ZNSt10_HashtableIN2v88internal9SignatureINS1_4wasm9ValueTypeEEESt4pairIKS5_jENS1_13ZoneAllocatorIS8_EENSt8__detail10_Select1stESt8equal_toIS5_ENS0_4base4hashIS5_EENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS8_Lb1EEEm, @function
_ZNSt10_HashtableIN2v88internal9SignatureINS1_4wasm9ValueTypeEEESt4pairIKS5_jENS1_13ZoneAllocatorIS8_EENSt8__detail10_Select1stESt8equal_toIS5_ENS0_4base4hashIS5_EENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS8_Lb1EEEm:
.LFB22321:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	movq	%r8, %rcx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$40, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L1431
.L1408:
	movq	%r14, 40(%r12)
	movq	8(%rbx), %rax
	leaq	0(,%r15,8), %rcx
	movq	(%rax,%r15,8), %rax
	testq	%rax, %rax
	je	.L1417
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	8(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r12, (%rax)
.L1418:
	addq	$1, 32(%rbx)
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1431:
	.cfi_restore_state
	movq	%rdx, %r13
	cmpq	$1, %rdx
	je	.L1432
	movq	(%rbx), %r8
	leaq	0(,%rdx,8), %rdx
	movq	%rdx, %rsi
	movq	16(%r8), %rdi
	movq	24(%r8), %rax
	subq	%rdi, %rax
	cmpq	%rax, %rdx
	ja	.L1433
	addq	%rdi, %rsi
	movq	%rsi, 16(%r8)
.L1412:
	xorl	%esi, %esi
	call	memset@PLT
	movq	%rax, %rdi
.L1410:
	movq	24(%rbx), %rsi
	movq	$0, 24(%rbx)
	testq	%rsi, %rsi
	je	.L1413
	xorl	%r9d, %r9d
	leaq	24(%rbx), %r10
	jmp	.L1414
	.p2align 4,,10
	.p2align 3
.L1415:
	movq	(%r8), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L1416:
	testq	%rsi, %rsi
	je	.L1413
.L1414:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	40(%rcx), %rax
	divq	%r13
	leaq	(%rdi,%rdx,8), %rax
	movq	(%rax), %r8
	testq	%r8, %r8
	jne	.L1415
	movq	24(%rbx), %r8
	movq	%r8, (%rcx)
	movq	%rcx, 24(%rbx)
	movq	%r10, (%rax)
	cmpq	$0, (%rcx)
	je	.L1420
	movq	%rcx, (%rdi,%r9,8)
	movq	%rdx, %r9
	testq	%rsi, %rsi
	jne	.L1414
	.p2align 4,,10
	.p2align 3
.L1413:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r13, 16(%rbx)
	divq	%r13
	movq	%rdi, 8(%rbx)
	movq	%rdx, %r15
	jmp	.L1408
	.p2align 4,,10
	.p2align 3
.L1417:
	movq	24(%rbx), %rax
	movq	%rax, (%r12)
	movq	%r12, 24(%rbx)
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L1419
	movq	40(%rax), %rax
	xorl	%edx, %edx
	divq	16(%rbx)
	movq	8(%rbx), %rax
	movq	%r12, (%rax,%rdx,8)
.L1419:
	movq	8(%rbx), %rax
	leaq	24(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L1418
	.p2align 4,,10
	.p2align 3
.L1420:
	movq	%rdx, %r9
	jmp	.L1416
	.p2align 4,,10
	.p2align 3
.L1432:
	movq	$0, 56(%rbx)
	leaq	56(%rbx), %rdi
	jmp	.L1410
	.p2align 4,,10
	.p2align 3
.L1433:
	movq	%r8, %rdi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %rdi
	jmp	.L1412
	.cfi_endproc
.LFE22321:
	.size	_ZNSt10_HashtableIN2v88internal9SignatureINS1_4wasm9ValueTypeEEESt4pairIKS5_jENS1_13ZoneAllocatorIS8_EENSt8__detail10_Select1stESt8equal_toIS5_ENS0_4base4hashIS5_EENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS8_Lb1EEEm, .-_ZNSt10_HashtableIN2v88internal9SignatureINS1_4wasm9ValueTypeEEESt4pairIKS5_jENS1_13ZoneAllocatorIS8_EENSt8__detail10_Select1stESt8equal_toIS5_ENS0_4base4hashIS5_EENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS8_Lb1EEEm
	.section	.text._ZN2v88internal4wasm17WasmModuleBuilder12AddSignatureEPNS0_9SignatureINS1_9ValueTypeEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm17WasmModuleBuilder12AddSignatureEPNS0_9SignatureINS1_9ValueTypeEEE
	.type	_ZN2v88internal4wasm17WasmModuleBuilder12AddSignatureEPNS0_9SignatureINS1_9ValueTypeEEE, @function
_ZN2v88internal4wasm17WasmModuleBuilder12AddSignatureEPNS0_9SignatureINS1_9ValueTypeEEE:
.LFB18052:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	296(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	8(%rsi), %rdi
	movq	(%rsi), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	16(%r13), %r12
	movq	8(%r13), %r14
	movq	%rax, %rcx
	addq	%r12, %r14
	addq	0(%r13), %r14
	cmpq	%r14, %r12
	je	.L1435
	.p2align 4,,10
	.p2align 3
.L1436:
	movq	%rcx, %rdi
	addq	$1, %r12
	call	_ZN2v84base10hash_valueEm@PLT
	movzbl	-1(%r12), %esi
	xorl	%edi, %edi
	movq	%rax, %r15
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%rax, %rcx
	cmpq	%r14, %r12
	jne	.L1436
.L1435:
	xorl	%edx, %edx
	movq	%rcx, %rax
	movq	-56(%rbp), %rdi
	divq	312(%rbx)
	movq	%rdx, %rsi
	movq	%r13, %rdx
	call	_ZNKSt10_HashtableIN2v88internal9SignatureINS1_4wasm9ValueTypeEEESt4pairIKS5_jENS1_13ZoneAllocatorIS8_EENSt8__detail10_Select1stESt8equal_toIS5_ENS0_4base4hashIS5_EENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmRS7_m
	testq	%rax, %rax
	je	.L1437
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L1437
	movl	32(%rax), %r14d
.L1434:
	addq	$40, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1437:
	.cfi_restore_state
	movq	296(%rbx), %rdi
	movq	16(%rbx), %r14
	movq	24(%rbx), %r12
	movq	16(%rdi), %r15
	movq	24(%rdi), %rax
	subq	%r15, %rax
	cmpq	$47, %rax
	jbe	.L1480
	leaq	48(%r15), %rax
	movq	%rax, 16(%rdi)
.L1440:
	movq	$0, (%r15)
	subq	%r14, %r12
	movq	16(%r13), %rax
	sarq	$3, %r12
	movdqu	0(%r13), %xmm1
	movq	0(%r13), %rsi
	movq	%rax, 24(%r15)
	leaq	8(%r15), %rax
	movl	%r12d, %r14d
	movl	%r12d, 32(%r15)
	movups	%xmm1, 8(%r15)
	movq	16(%r15), %rdi
	movq	%rax, -80(%rbp)
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	24(%r15), %r12
	movq	%rax, %r10
	movq	16(%r15), %rax
	addq	%r12, %rax
	addq	8(%r15), %rax
	movq	%rax, -72(%rbp)
	cmpq	%rax, %r12
	je	.L1441
	.p2align 4,,10
	.p2align 3
.L1442:
	movq	%r10, %rdi
	addq	$1, %r12
	call	_ZN2v84base10hash_valueEm@PLT
	movzbl	-1(%r12), %esi
	xorl	%edi, %edi
	movq	%rax, -64(%rbp)
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	-64(%rbp), %r8
	movq	%rax, %rdi
	movq	%r8, %rsi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%rax, %r10
	cmpq	-72(%rbp), %r12
	jne	.L1442
.L1441:
	movq	%r10, %rax
	xorl	%edx, %edx
	movq	-56(%rbp), %rdi
	movq	%r10, %rcx
	divq	312(%rbx)
	movq	%r10, -64(%rbp)
	movq	%rdx, %r12
	movq	-80(%rbp), %rdx
	movq	%r12, %rsi
	call	_ZNKSt10_HashtableIN2v88internal9SignatureINS1_4wasm9ValueTypeEEESt4pairIKS5_jENS1_13ZoneAllocatorIS8_EENSt8__detail10_Select1stESt8equal_toIS5_ENS0_4base4hashIS5_EENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmRS7_m
	movq	-64(%rbp), %r10
	testq	%rax, %rax
	je	.L1443
	cmpq	$0, (%rax)
	je	.L1443
.L1444:
	movq	24(%rbx), %r12
	cmpq	32(%rbx), %r12
	je	.L1445
	movq	%r13, (%r12)
	addq	$8, 24(%rbx)
	jmp	.L1434
	.p2align 4,,10
	.p2align 3
.L1443:
	movq	-56(%rbp), %rdi
	movl	$1, %r8d
	movq	%r15, %rcx
	movq	%r10, %rdx
	movq	%r12, %rsi
	call	_ZNSt10_HashtableIN2v88internal9SignatureINS1_4wasm9ValueTypeEEESt4pairIKS5_jENS1_13ZoneAllocatorIS8_EENSt8__detail10_Select1stESt8equal_toIS5_ENS0_4base4hashIS5_EENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS8_Lb1EEEm
	jmp	.L1444
	.p2align 4,,10
	.p2align 3
.L1445:
	movq	16(%rbx), %r8
	movq	%r12, %rcx
	subq	%r8, %rcx
	movq	%rcx, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L1481
	testq	%rax, %rax
	je	.L1456
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L1482
	movl	$2147483640, %esi
	movl	$2147483640, %r15d
.L1447:
	movq	8(%rbx), %rdi
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	subq	%rdx, %rax
	cmpq	%rax, %rsi
	ja	.L1483
	addq	%rdx, %rsi
	movq	%rsi, 16(%rdi)
.L1450:
	leaq	(%rdx,%r15), %rsi
	leaq	8(%rdx), %rax
.L1448:
	movq	%r13, (%rdx,%rcx)
	cmpq	%r8, %r12
	je	.L1451
	subq	$8, %r12
	leaq	15(%rdx), %rax
	subq	%r8, %r12
	subq	%r8, %rax
	movq	%r12, %rcx
	shrq	$3, %rcx
	cmpq	$30, %rax
	jbe	.L1459
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rcx
	je	.L1459
	leaq	1(%rcx), %rdi
	xorl	%eax, %eax
	movq	%rdi, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L1453:
	movdqu	(%r8,%rax), %xmm2
	movups	%xmm2, (%rdx,%rax)
	addq	$16, %rax
	cmpq	%rcx, %rax
	jne	.L1453
	movq	%rdi, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rcx
	leaq	(%r8,%rcx), %r13
	addq	%rdx, %rcx
	cmpq	%rax, %rdi
	je	.L1455
	movq	0(%r13), %rax
	movq	%rax, (%rcx)
.L1455:
	leaq	16(%rdx,%r12), %rax
.L1451:
	movq	%rdx, %xmm0
	movq	%rax, %xmm3
	movq	%rsi, 32(%rbx)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 16(%rbx)
	jmp	.L1434
.L1482:
	testq	%rdx, %rdx
	jne	.L1484
	movl	$8, %eax
	xorl	%esi, %esi
	xorl	%edx, %edx
	jmp	.L1448
	.p2align 4,,10
	.p2align 3
.L1480:
	movl	$48, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r15
	jmp	.L1440
	.p2align 4,,10
	.p2align 3
.L1456:
	movl	$8, %esi
	movl	$8, %r15d
	jmp	.L1447
.L1459:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1452:
	movq	(%r8,%rax,8), %rdi
	movq	%rdi, (%rdx,%rax,8)
	movq	%rax, %rdi
	addq	$1, %rax
	cmpq	%rdi, %rcx
	jne	.L1452
	jmp	.L1455
.L1483:
	movq	%rcx, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %rcx
	movq	%rax, %rdx
	jmp	.L1450
.L1481:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1484:
	movl	$268435455, %r15d
	cmpq	$268435455, %rdx
	cmova	%r15, %rdx
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rsi
	jmp	.L1447
	.cfi_endproc
.LFE18052:
	.size	_ZN2v88internal4wasm17WasmModuleBuilder12AddSignatureEPNS0_9SignatureINS1_9ValueTypeEEE, .-_ZN2v88internal4wasm17WasmModuleBuilder12AddSignatureEPNS0_9SignatureINS1_9ValueTypeEEE
	.section	.text._ZN2v88internal4wasm19WasmFunctionBuilder12SetSignatureEPNS0_9SignatureINS1_9ValueTypeEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm19WasmFunctionBuilder12SetSignatureEPNS0_9SignatureINS1_9ValueTypeEEE
	.type	_ZN2v88internal4wasm19WasmFunctionBuilder12SetSignatureEPNS0_9SignatureINS1_9ValueTypeEEE, @function
_ZN2v88internal4wasm19WasmFunctionBuilder12SetSignatureEPNS0_9SignatureINS1_9ValueTypeEEE:
.LFB17981:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rsi, 8(%rdi)
	movq	(%rdi), %rdi
	call	_ZN2v88internal4wasm17WasmModuleBuilder12AddSignatureEPNS0_9SignatureINS1_9ValueTypeEEE
	movl	%eax, 56(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17981:
	.size	_ZN2v88internal4wasm19WasmFunctionBuilder12SetSignatureEPNS0_9SignatureINS1_9ValueTypeEEE, .-_ZN2v88internal4wasm19WasmFunctionBuilder12SetSignatureEPNS0_9SignatureINS1_9ValueTypeEEE
	.section	.text._ZN2v88internal4wasm17WasmModuleBuilder11AddFunctionEPNS0_9SignatureINS1_9ValueTypeEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm17WasmModuleBuilder11AddFunctionEPNS0_9SignatureINS1_9ValueTypeEEE
	.type	_ZN2v88internal4wasm17WasmModuleBuilder11AddFunctionEPNS0_9SignatureINS1_9ValueTypeEEE, @function
_ZN2v88internal4wasm17WasmModuleBuilder11AddFunctionEPNS0_9SignatureINS1_9ValueTypeEEE:
.LFB18038:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$319, %rax
	jbe	.L1523
	leaq	320(%rbx), %rax
	movq	%rax, 16(%rdi)
.L1489:
	movq	(%r12), %rax
	movq	$0, 8(%rbx)
	movq	$0, 24(%rbx)
	movq	%rax, 16(%rbx)
	movq	$0, 32(%rbx)
	movq	$0, 40(%rbx)
	movq	$0, 48(%rbx)
	movq	(%r12), %rdi
	movq	152(%r12), %rax
	subq	144(%r12), %rax
	movq	%r12, (%rbx)
	sarq	$3, %rax
	movl	$0, 56(%rbx)
	movl	%eax, 60(%rbx)
	movq	%rdi, 64(%rbx)
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$255, %rdx
	jbe	.L1524
	leaq	256(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L1491:
	movq	%rdx, 88(%rbx)
	movq	%rax, 72(%rbx)
	movq	%rax, 80(%rbx)
	movq	$0, 96(%rbx)
	movq	$0, 104(%rbx)
	movq	(%r12), %rax
	movq	$0, 120(%rbx)
	movq	%rax, 112(%rbx)
	movq	$0, 128(%rbx)
	movq	$0, 136(%rbx)
	movq	(%r12), %rax
	movq	$0, 152(%rbx)
	movq	%rax, 144(%rbx)
	movq	$0, 160(%rbx)
	movq	$0, 168(%rbx)
	movq	(%r12), %rax
	movq	$0, 184(%rbx)
	movq	%rax, 176(%rbx)
	movq	$0, 192(%rbx)
	movq	$0, 200(%rbx)
	movq	(%r12), %rax
	movq	$0, 216(%rbx)
	movq	%rax, 208(%rbx)
	movq	$0, 224(%rbx)
	movq	$0, 232(%rbx)
	movq	(%r12), %rax
	movq	$0, 248(%rbx)
	movq	%rax, 240(%rbx)
	movq	$0, 256(%rbx)
	movq	$0, 264(%rbx)
	movq	(%r12), %rdi
	movq	%rdi, 272(%rbx)
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$7, %rdx
	jbe	.L1525
	leaq	8(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L1493:
	movq	%rax, 280(%rbx)
	movq	%rax, 288(%rbx)
	movq	%rdx, 296(%rbx)
	movq	$0, 304(%rbx)
	movl	$0, 312(%rbx)
	movb	$-1, 316(%rbx)
	movq	152(%r12), %r14
	cmpq	160(%r12), %r14
	je	.L1494
	movq	%rbx, (%r14)
	movq	152(%r12), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 152(%r12)
.L1495:
	movq	-8(%rdx), %r14
	testq	%r13, %r13
	je	.L1487
	movq	%r13, 8(%r14)
	movq	(%r14), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal4wasm17WasmModuleBuilder12AddSignatureEPNS0_9SignatureINS1_9ValueTypeEEE
	movl	%eax, 56(%r14)
	movq	152(%r12), %rax
	movq	-8(%rax), %r14
.L1487:
	addq	$24, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1494:
	.cfi_restore_state
	movq	144(%r12), %r15
	movq	%r14, %rcx
	subq	%r15, %rcx
	movq	%rcx, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L1526
	testq	%rax, %rax
	je	.L1507
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L1527
	movl	$2147483640, %esi
	movl	$2147483640, %edx
.L1497:
	movq	136(%r12), %r8
	movq	16(%r8), %rax
	movq	24(%r8), %rdi
	subq	%rax, %rdi
	cmpq	%rdi, %rsi
	ja	.L1528
	addq	%rax, %rsi
	movq	%rsi, 16(%r8)
.L1500:
	leaq	(%rax,%rdx), %rsi
	leaq	8(%rax), %rdx
.L1498:
	movq	%rbx, (%rax,%rcx)
	cmpq	%r15, %r14
	je	.L1501
	subq	$8, %r14
	leaq	15(%rax), %rdx
	subq	%r15, %r14
	subq	%r15, %rdx
	movq	%r14, %rcx
	shrq	$3, %rcx
	cmpq	$30, %rdx
	jbe	.L1510
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rcx
	je	.L1510
	leaq	1(%rcx), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L1503:
	movdqu	(%r15,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L1503
	movq	%rdi, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rcx
	addq	%rcx, %r15
	addq	%rax, %rcx
	cmpq	%rdx, %rdi
	je	.L1505
	movq	(%r15), %rdx
	movq	%rdx, (%rcx)
.L1505:
	leaq	16(%rax,%r14), %rdx
.L1501:
	movq	%rax, %xmm0
	movq	%rdx, %xmm2
	movq	%rsi, 160(%r12)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 144(%r12)
	jmp	.L1495
	.p2align 4,,10
	.p2align 3
.L1527:
	testq	%rdx, %rdx
	jne	.L1529
	movl	$8, %edx
	xorl	%esi, %esi
	xorl	%eax, %eax
	jmp	.L1498
	.p2align 4,,10
	.p2align 3
.L1523:
	movl	$320, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rbx
	jmp	.L1489
	.p2align 4,,10
	.p2align 3
.L1524:
	movl	$256, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	leaq	256(%rax), %rdx
	jmp	.L1491
	.p2align 4,,10
	.p2align 3
.L1525:
	movl	$8, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	leaq	8(%rax), %rdx
	jmp	.L1493
	.p2align 4,,10
	.p2align 3
.L1507:
	movl	$8, %esi
	movl	$8, %edx
	jmp	.L1497
	.p2align 4,,10
	.p2align 3
.L1510:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L1502:
	movq	(%r15,%rdx,8), %rdi
	movq	%rdi, (%rax,%rdx,8)
	movq	%rdx, %rdi
	addq	$1, %rdx
	cmpq	%rdi, %rcx
	jne	.L1502
	jmp	.L1505
.L1528:
	movq	%r8, %rdi
	movq	%rcx, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rcx
	jmp	.L1500
.L1526:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1529:
	cmpq	$268435455, %rdx
	movl	$268435455, %eax
	cmova	%rax, %rdx
	salq	$3, %rdx
	movq	%rdx, %rsi
	jmp	.L1497
	.cfi_endproc
.LFE18038:
	.size	_ZN2v88internal4wasm17WasmModuleBuilder11AddFunctionEPNS0_9SignatureINS1_9ValueTypeEEE, .-_ZN2v88internal4wasm17WasmModuleBuilder11AddFunctionEPNS0_9SignatureINS1_9ValueTypeEEE
	.section	.text._ZN2v88internal4wasm17WasmModuleBuilder9AddImportENS0_6VectorIKcEEPNS0_9SignatureINS1_9ValueTypeEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm17WasmModuleBuilder9AddImportENS0_6VectorIKcEEPNS0_9SignatureINS1_9ValueTypeEEE
	.type	_ZN2v88internal4wasm17WasmModuleBuilder9AddImportENS0_6VectorIKcEEPNS0_9SignatureINS1_9ValueTypeEEE, @function
_ZN2v88internal4wasm17WasmModuleBuilder9AddImportENS0_6VectorIKcEEPNS0_9SignatureINS1_9ValueTypeEEE:
.LFB18058:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rcx, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%r8, -48(%rbp)
	movq	%rdx, -40(%rbp)
	call	_ZN2v88internal4wasm17WasmModuleBuilder12AddSignatureEPNS0_9SignatureINS1_9ValueTypeEEE
	movq	56(%rbx), %rsi
	movl	%eax, -32(%rbp)
	cmpq	64(%rbx), %rsi
	je	.L1531
	movdqa	-48(%rbp), %xmm0
	movups	%xmm0, (%rsi)
	movq	-32(%rbp), %rax
	movq	%rax, 16(%rsi)
	movq	56(%rbx), %rax
	addq	$24, %rax
	movq	%rax, 56(%rbx)
.L1532:
	subq	48(%rbx), %rax
	sarq	$3, %rax
	imull	$-1431655765, %eax, %eax
	subl	$1, %eax
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1535
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1531:
	.cfi_restore_state
	leaq	-48(%rbp), %rdx
	leaq	40(%rbx), %rdi
	call	_ZNSt6vectorIN2v88internal4wasm17WasmModuleBuilder18WasmFunctionImportENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	movq	56(%rbx), %rax
	jmp	.L1532
.L1535:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18058:
	.size	_ZN2v88internal4wasm17WasmModuleBuilder9AddImportENS0_6VectorIKcEEPNS0_9SignatureINS1_9ValueTypeEEE, .-_ZN2v88internal4wasm17WasmModuleBuilder9AddImportENS0_6VectorIKcEEPNS0_9SignatureINS1_9ValueTypeEEE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal4wasm19WasmFunctionBuilderC2EPNS1_17WasmModuleBuilderE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal4wasm19WasmFunctionBuilderC2EPNS1_17WasmModuleBuilderE, @function
_GLOBAL__sub_I__ZN2v88internal4wasm19WasmFunctionBuilderC2EPNS1_17WasmModuleBuilderE:
.LFB22961:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE22961:
	.size	_GLOBAL__sub_I__ZN2v88internal4wasm19WasmFunctionBuilderC2EPNS1_17WasmModuleBuilderE, .-_GLOBAL__sub_I__ZN2v88internal4wasm19WasmFunctionBuilderC2EPNS1_17WasmModuleBuilderE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal4wasm19WasmFunctionBuilderC2EPNS1_17WasmModuleBuilderE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC4:
	.quad	8386103181254160227
	.quad	8319395810125967209
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
