	.file	"debug-coverage.cc"
	.text
	.section	.text._ZN2v88internal12_GLOBAL__N_120CompareCoverageBlockERKNS0_13CoverageBlockES4_,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_120CompareCoverageBlockERKNS0_13CoverageBlockES4_, @function
_ZN2v88internal12_GLOBAL__N_120CompareCoverageBlockERKNS0_13CoverageBlockES4_:
.LFB19874:
	.cfi_startproc
	endbr64
	movl	(%rdi), %edx
	movl	(%rsi), %eax
	cmpl	%eax, %edx
	setl	%r8b
	je	.L5
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movl	4(%rsi), %eax
	cmpl	%eax, 4(%rdi)
	setg	%r8b
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE19874:
	.size	_ZN2v88internal12_GLOBAL__N_120CompareCoverageBlockERKNS0_13CoverageBlockES4_, .-_ZN2v88internal12_GLOBAL__N_120CompareCoverageBlockERKNS0_13CoverageBlockES4_
	.section	.rodata._ZN2v88internal12_GLOBAL__N_118PrintBlockCoverageEPKNS0_16CoverageFunctionENS0_18SharedFunctionInfoEbb.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"Coverage for function='%s', SFI=%p, has_nonempty_source_range=%d, function_is_relevant=%d\n"
	.align 8
.LC1:
	.string	"{start: %d, end: %d, count: %d}\n"
	.section	.text._ZN2v88internal12_GLOBAL__N_118PrintBlockCoverageEPKNS0_16CoverageFunctionENS0_18SharedFunctionInfoEbb,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_118PrintBlockCoverageEPKNS0_16CoverageFunctionENS0_18SharedFunctionInfoEbb, @function
_ZN2v88internal12_GLOBAL__N_118PrintBlockCoverageEPKNS0_16CoverageFunctionENS0_18SharedFunctionInfoEbb:
.LFB19921:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%ecx, %r14d
	xorl	%ecx, %ecx
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	leaq	-48(%rbp), %rsi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%edx, %ebx
	movl	$1, %edx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	leaq	-56(%rbp), %rdi
	movq	(%rax), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	-56(%rbp), %rsi
	movzbl	%bl, %ecx
	movzbl	%r14b, %r8d
	movq	%r13, %rdx
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movl	8(%r12), %ecx
	movl	(%r12), %esi
	xorl	%eax, %eax
	movl	4(%r12), %edx
	leaq	.LC1(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	24(%r12), %rbx
	movq	32(%r12), %r12
	cmpq	%rbx, %r12
	je	.L7
	leaq	.LC1(%rip), %r13
	.p2align 4,,10
	.p2align 3
.L8:
	movl	8(%rbx), %ecx
	movl	4(%rbx), %edx
	movq	%r13, %rdi
	xorl	%eax, %eax
	movl	(%rbx), %esi
	addq	$12, %rbx
	call	_ZN2v88internal6PrintFEPKcz@PLT
	cmpq	%rbx, %r12
	jne	.L8
.L7:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L6
	call	_ZdaPv@PLT
.L6:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L16
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L16:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19921:
	.size	_ZN2v88internal12_GLOBAL__N_118PrintBlockCoverageEPKNS0_16CoverageFunctionENS0_18SharedFunctionInfoEbb, .-_ZN2v88internal12_GLOBAL__N_118PrintBlockCoverageEPKNS0_16CoverageFunctionENS0_18SharedFunctionInfoEbb
	.section	.rodata._ZNSt6vectorIN2v88internal12_GLOBAL__N_126SharedFunctionInfoAndCountESaIS3_EE17_M_realloc_insertIJRNS1_18SharedFunctionInfoEjEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_.str1.1,"aMS",@progbits,1
.LC2:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIN2v88internal12_GLOBAL__N_126SharedFunctionInfoAndCountESaIS3_EE17_M_realloc_insertIJRNS1_18SharedFunctionInfoEjEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt6vectorIN2v88internal12_GLOBAL__N_126SharedFunctionInfoAndCountESaIS3_EE17_M_realloc_insertIJRNS1_18SharedFunctionInfoEjEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, @function
_ZNSt6vectorIN2v88internal12_GLOBAL__N_126SharedFunctionInfoAndCountESaIS3_EE17_M_realloc_insertIJRNS1_18SharedFunctionInfoEjEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_:
.LFB23608:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movabsq	$-6148914691236517205, %rdi
	movq	%rax, -104(%rbp)
	subq	%r12, %rax
	sarq	$3, %rax
	imulq	%rdi, %rax
	movabsq	$384307168202282325, %rdi
	cmpq	%rdi, %rax
	je	.L41
	movq	%rsi, %r8
	movq	%rsi, %rbx
	subq	%r12, %r8
	testq	%rax, %rax
	je	.L30
	movabsq	$9223372036854775800, %r15
	leaq	(%rax,%rax), %r9
	cmpq	%r9, %rax
	jbe	.L42
.L19:
	movq	%r15, %rdi
	movq	%rcx, -128(%rbp)
	movq	%rdx, -120(%rbp)
	movq	%r8, -112(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %r8
	movq	-120(%rbp), %rdx
	movq	%rax, -88(%rbp)
	addq	%rax, %r15
	movq	-128(%rbp), %rcx
	movq	%r15, -96(%rbp)
	leaq	24(%rax), %r15
.L28:
	movq	-88(%rbp), %rax
	movl	(%rcx), %ecx
	leaq	(%rax,%r8), %r14
	movq	(%rdx), %rax
	movl	%ecx, 8(%r14)
	movq	%rax, -72(%rbp)
	movq	%rax, (%r14)
	movq	%rax, -64(%rbp)
	movzwl	45(%rax), %edx
	cmpl	$65535, %edx
	je	.L43
	leaq	-64(%rbp), %rdi
	movl	%edx, -120(%rbp)
	movq	%rdi, -112(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	movl	-120(%rbp), %edx
	movq	-112(%rbp), %rdi
	subl	%edx, %eax
	cmpl	$-1, %eax
	je	.L23
.L22:
	movl	%eax, 12(%r14)
	leaq	-72(%rbp), %rdi
	call	_ZNK2v88internal18SharedFunctionInfo11EndPositionEv@PLT
	movl	%eax, 16(%r14)
	cmpq	%r12, %rbx
	je	.L24
	movq	-88(%rbp), %rdx
	movq	%r12, %rax
	.p2align 4,,10
	.p2align 3
.L25:
	movdqu	(%rax), %xmm1
	addq	$24, %rax
	addq	$24, %rdx
	movups	%xmm1, -24(%rdx)
	movq	-8(%rax), %rcx
	movq	%rcx, -8(%rdx)
	cmpq	%rax, %rbx
	jne	.L25
	leaq	-24(%rbx), %rax
	movq	-88(%rbp), %rsi
	subq	%r12, %rax
	shrq	$3, %rax
	leaq	48(%rsi,%rax,8), %r15
.L24:
	movq	-104(%rbp), %rax
	cmpq	%rax, %rbx
	je	.L26
	movq	%rax, %r14
	movq	%r15, %rdi
	movq	%rbx, %rsi
	subq	%rbx, %r14
	leaq	-24(%r14), %rax
	shrq	$3, %rax
	leaq	24(,%rax,8), %r14
	movq	%r14, %rdx
	addq	%r14, %r15
	call	memcpy@PLT
.L26:
	testq	%r12, %r12
	je	.L27
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L27:
	movq	-96(%rbp), %rax
	movq	-88(%rbp), %xmm0
	movq	%r15, %xmm2
	movq	%rax, 16(%r13)
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, 0(%r13)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L44
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	.cfi_restore_state
	testq	%r9, %r9
	jne	.L20
	movq	$0, -96(%rbp)
	movl	$24, %r15d
	movq	$0, -88(%rbp)
	jmp	.L28
.L43:
	leaq	-64(%rbp), %rdi
	.p2align 4,,10
	.p2align 3
.L23:
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L30:
	movl	$24, %r15d
	jmp	.L19
.L41:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L44:
	call	__stack_chk_fail@PLT
.L20:
	cmpq	%rdi, %r9
	cmovbe	%r9, %rdi
	imulq	$24, %rdi, %r15
	jmp	.L19
	.cfi_endproc
.LFE23608:
	.size	_ZNSt6vectorIN2v88internal12_GLOBAL__N_126SharedFunctionInfoAndCountESaIS3_EE17_M_realloc_insertIJRNS1_18SharedFunctionInfoEjEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, .-_ZNSt6vectorIN2v88internal12_GLOBAL__N_126SharedFunctionInfoAndCountESaIS3_EE17_M_realloc_insertIJRNS1_18SharedFunctionInfoEjEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.section	.text._ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPN2v88internal12_GLOBAL__N_126SharedFunctionInfoAndCountESt6vectorIS5_SaIS5_EEEElS5_NS0_5__ops15_Iter_less_iterEEvT_T0_SE_T1_T2_.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPN2v88internal12_GLOBAL__N_126SharedFunctionInfoAndCountESt6vectorIS5_SaIS5_EEEElS5_NS0_5__ops15_Iter_less_iterEEvT_T0_SE_T1_T2_.isra.0, @function
_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPN2v88internal12_GLOBAL__N_126SharedFunctionInfoAndCountESt6vectorIS5_SaIS5_EEEElS5_NS0_5__ops15_Iter_less_iterEEvT_T0_SE_T1_T2_.isra.0:
.LFB25959:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	-1(%rdx), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rax, %r13
	andl	$1, %r14d
	pushq	%r12
	shrq	$63, %r13
	pushq	%rbx
	addq	%rax, %r13
	sarq	%r13
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	24(%rbp), %ebx
	movl	28(%rbp), %r10d
	movl	32(%rbp), %r11d
	cmpq	%r13, %rsi
	jge	.L46
	movq	%rsi, %r9
	.p2align 4,,10
	.p2align 3
.L53:
	leaq	1(%r9), %rcx
	leaq	(%rcx,%rcx), %rax
	leaq	-1(%rax), %r8
	leaq	(%rax,%rcx,4), %rcx
	leaq	(%r8,%r8,2), %r12
	leaq	(%rdi,%rcx,8), %rcx
	leaq	(%rdi,%r12,8), %r12
	movl	12(%rcx), %r15d
	cmpl	%r15d, 12(%r12)
	je	.L47
	setg	%r15b
.L48:
	testb	%r15b, %r15b
	jne	.L50
.L71:
	movdqu	(%rcx), %xmm1
	leaq	(%r9,%r9,2), %r8
	leaq	(%rdi,%r8,8), %r8
	movups	%xmm1, (%r8)
	movl	16(%rcx), %r9d
	movl	%r9d, 16(%r8)
	cmpq	%rax, %r13
	jle	.L51
	movq	%rax, %r9
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L47:
	movl	16(%rcx), %r15d
	cmpl	%r15d, 16(%r12)
	je	.L49
	setl	%r15b
	testb	%r15b, %r15b
	je	.L71
.L50:
	movdqu	(%r12), %xmm2
	leaq	(%r9,%r9,2), %rax
	leaq	(%rdi,%rax,8), %rax
	movups	%xmm2, (%rax)
	movl	16(%r12), %ecx
	movl	%ecx, 16(%rax)
	cmpq	%r8, %r13
	jle	.L62
	movq	%r8, %r9
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L49:
	movl	8(%rcx), %r15d
	cmpl	%r15d, 8(%r12)
	seta	%r15b
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L62:
	movq	%r12, %rcx
	movq	%r8, %rax
.L51:
	testq	%r14, %r14
	je	.L60
.L54:
	movq	32(%rbp), %rdx
	movdqu	16(%rbp), %xmm4
	movq	%rdx, -64(%rbp)
	leaq	-1(%rax), %rdx
	movq	%rdx, %r8
	movups	%xmm4, -80(%rbp)
	shrq	$63, %r8
	addq	%rdx, %r8
	sarq	%r8
	cmpq	%rsi, %rax
	jg	.L59
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L73:
	setl	%dl
.L57:
	leaq	(%rax,%rax,2), %rax
	leaq	(%rdi,%rax,8), %rcx
	testb	%dl, %dl
	je	.L55
.L74:
	movdqu	(%r9), %xmm0
	leaq	-1(%r8), %rdx
	movups	%xmm0, (%rcx)
	movl	16(%r9), %eax
	movl	%eax, 16(%rcx)
	movq	%rdx, %rax
	shrq	$63, %rax
	addq	%rdx, %rax
	sarq	%rax
	movq	%rax, %rdx
	movq	%r8, %rax
	cmpq	%r8, %rsi
	jge	.L72
	movq	%rdx, %r8
.L59:
	leaq	(%r8,%r8,2), %rdx
	leaq	(%rdi,%rdx,8), %r9
	cmpl	%r10d, 12(%r9)
	jne	.L73
	cmpl	%r11d, 16(%r9)
	je	.L58
	leaq	(%rax,%rax,2), %rax
	setg	%dl
	leaq	(%rdi,%rax,8), %rcx
	testb	%dl, %dl
	jne	.L74
.L55:
	movl	%r11d, 16(%rcx)
	movl	%ebx, -72(%rbp)
	movl	%r10d, -68(%rbp)
	movdqu	-80(%rbp), %xmm3
	movups	%xmm3, (%rcx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	.cfi_restore_state
	leaq	(%rsi,%rsi,2), %rax
	leaq	(%rdi,%rax,8), %rcx
	testq	%r14, %r14
	jne	.L75
	movq	%rsi, %rax
	.p2align 4,,10
	.p2align 3
.L60:
	leaq	-2(%rdx), %r8
	movq	%r8, %rdx
	shrq	$63, %rdx
	addq	%r8, %rdx
	sarq	%rdx
	cmpq	%rax, %rdx
	jne	.L54
	leaq	1(%rax,%rax), %rax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rdi,%rdx,8), %rdx
	movdqu	(%rdx), %xmm5
	movups	%xmm5, (%rcx)
	movl	16(%rdx), %r8d
	movl	%r8d, 16(%rcx)
	movq	%rdx, %rcx
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L58:
	cmpl	%ebx, 8(%r9)
	setb	%dl
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L72:
	movq	%r9, %rcx
	jmp	.L55
.L75:
	movdqu	16(%rbp), %xmm6
	movups	%xmm6, -80(%rbp)
	jmp	.L55
	.cfi_endproc
.LFE25959:
	.size	_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPN2v88internal12_GLOBAL__N_126SharedFunctionInfoAndCountESt6vectorIS5_SaIS5_EEEElS5_NS0_5__ops15_Iter_less_iterEEvT_T0_SE_T1_T2_.isra.0, .-_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPN2v88internal12_GLOBAL__N_126SharedFunctionInfoAndCountESt6vectorIS5_SaIS5_EEEElS5_NS0_5__ops15_Iter_less_iterEEvT_T0_SE_T1_T2_.isra.0
	.section	.text._ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPN2v88internal12_GLOBAL__N_126SharedFunctionInfoAndCountESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_less_iterEEvT_SD_T0_T1_.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPN2v88internal12_GLOBAL__N_126SharedFunctionInfoAndCountESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_less_iterEEvT_SD_T0_T1_.isra.0, @function
_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPN2v88internal12_GLOBAL__N_126SharedFunctionInfoAndCountESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_less_iterEEvT_SD_T0_T1_.isra.0:
.LFB25965:
	.cfi_startproc
	movq	%rsi, %rax
	subq	%rdi, %rax
	cmpq	$384, %rax
	jle	.L125
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	testq	%rdx, %rdx
	je	.L78
	leaq	24(%rdi), %r15
.L80:
	movq	%rsi, %rax
	movl	36(%rbx), %edi
	subq	$1, %r14
	movabsq	$-6148914691236517205, %rcx
	subq	%rbx, %rax
	sarq	$3, %rax
	imulq	%rax, %rcx
	movq	%rcx, %rax
	shrq	$63, %rax
	addq	%rcx, %rax
	movq	%rax, %rcx
	andq	$-2, %rax
	sarq	%rcx
	addq	%rcx, %rax
	leaq	(%rbx,%rax,8), %rax
	movl	12(%rax), %ecx
	cmpl	%ecx, %edi
	je	.L85
	setl	%r9b
.L86:
	movl	-12(%rsi), %r8d
	testb	%r9b, %r9b
	je	.L88
	cmpl	%ecx, %r8d
	jne	.L128
	movl	-8(%rsi), %edx
	cmpl	%edx, 16(%rax)
	je	.L91
.L128:
	setg	%cl
.L90:
	testb	%cl, %cl
	je	.L92
	movdqu	(%rax), %xmm5
	movdqu	(%rbx), %xmm0
	movq	16(%rbx), %rcx
	movups	%xmm5, (%rbx)
	movl	16(%rax), %edi
	movq	%rcx, -64(%rbp)
	movl	%edi, 16(%rbx)
	movl	%ecx, 16(%rax)
	movaps	%xmm0, -80(%rbp)
	movups	%xmm0, (%rax)
.L93:
	movq	%r15, %r12
	movl	12(%rbx), %r8d
	movq	%rsi, %rax
	movq	%r12, %r13
	cmpl	%r8d, 12(%r12)
	je	.L107
	.p2align 4,,10
	.p2align 3
.L135:
	setl	%cl
.L108:
	testb	%cl, %cl
	jne	.L110
	leaq	-24(%rax), %rcx
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L111:
	movl	16(%rcx), %edx
	cmpl	%edx, 16(%rbx)
	jne	.L132
	movl	8(%rcx), %edi
	cmpl	%edi, 8(%rbx)
	setb	%dil
	subq	$24, %rcx
	testb	%dil, %dil
	je	.L133
.L114:
	movq	%rcx, %rax
	cmpl	%r8d, 12(%rcx)
	je	.L111
.L132:
	setg	%dil
	subq	$24, %rcx
	testb	%dil, %dil
	jne	.L114
.L133:
	cmpq	%rax, %r12
	jnb	.L134
	movdqu	(%rax), %xmm1
	movq	16(%r12), %rcx
	movdqu	(%r12), %xmm0
	movups	%xmm1, (%r12)
	movl	16(%rax), %edi
	movq	%rcx, -64(%rbp)
	movl	%edi, 16(%r12)
	movl	%ecx, 16(%rax)
	movups	%xmm0, (%rax)
	movl	12(%rbx), %r8d
	movaps	%xmm0, -80(%rbp)
.L110:
	addq	$24, %r12
	movq	%r12, %r13
	cmpl	%r8d, 12(%r12)
	jne	.L135
.L107:
	movl	16(%rbx), %edx
	cmpl	%edx, 16(%r12)
	je	.L109
	setg	%cl
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L109:
	movl	8(%rbx), %edx
	cmpl	%edx, 8(%r12)
	setb	%cl
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L134:
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPN2v88internal12_GLOBAL__N_126SharedFunctionInfoAndCountESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_less_iterEEvT_SD_T0_T1_.isra.0
	movq	%r12, %rax
	subq	%rbx, %rax
	cmpq	$384, %rax
	jle	.L76
	testq	%r14, %r14
	je	.L78
	movq	%r12, %rsi
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L88:
	cmpl	%edi, %r8d
	jne	.L130
	movl	-8(%rsi), %edi
	cmpl	%edi, 40(%rbx)
	je	.L100
.L130:
	setg	%dil
.L99:
	testb	%dil, %dil
	je	.L101
	movdqu	(%rbx), %xmm0
	movq	16(%rbx), %rax
	movdqu	24(%rbx), %xmm6
	movl	40(%rbx), %ecx
	movq	%rax, -64(%rbp)
	movl	%ecx, 16(%rbx)
	movl	%eax, 40(%rbx)
	movaps	%xmm0, -80(%rbp)
	movups	%xmm6, (%rbx)
	movups	%xmm0, 24(%rbx)
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L85:
	movl	16(%rax), %edx
	cmpl	%edx, 40(%rbx)
	je	.L87
	setg	%r9b
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L92:
	cmpl	%edi, %r8d
	je	.L94
.L129:
	setg	%al
.L95:
	movdqu	(%rbx), %xmm0
	testb	%al, %al
	movaps	%xmm0, -80(%rbp)
	movq	16(%rbx), %rax
	movq	%rax, -64(%rbp)
	je	.L97
	movdqu	-24(%rsi), %xmm7
	movups	%xmm7, (%rbx)
	movl	-8(%rsi), %ecx
	movl	%ecx, 16(%rbx)
	movl	%eax, -8(%rsi)
	movups	%xmm0, -24(%rsi)
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L87:
	movl	8(%rax), %edx
	cmpl	%edx, 32(%rbx)
	setb	%r9b
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L101:
	cmpl	%ecx, %r8d
	je	.L102
.L131:
	setg	%cl
.L103:
	movdqu	(%rbx), %xmm0
	movaps	%xmm0, -80(%rbp)
	testb	%cl, %cl
	je	.L105
	movdqu	-24(%rsi), %xmm5
	movq	16(%rbx), %rax
	movups	%xmm5, (%rbx)
	movl	-8(%rsi), %ecx
	movq	%rax, -64(%rbp)
	movl	%ecx, 16(%rbx)
	movl	%eax, -8(%rsi)
	movups	%xmm0, -24(%rsi)
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L78:
	movabsq	$-6148914691236517205, %r12
	sarq	$3, %rax
	imulq	%rax, %r12
	leaq	-2(%r12), %rsi
	sarq	%rsi
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L81:
	subq	$1, %rsi
.L83:
	leaq	(%rsi,%rsi,2), %rax
	subq	$8, %rsp
	movq	%r12, %rdx
	movq	%rbx, %rdi
	movdqu	(%rbx,%rax,8), %xmm4
	movq	16(%rbx,%rax,8), %rax
	movaps	%xmm4, -80(%rbp)
	movq	%rax, -64(%rbp)
	pushq	-64(%rbp)
	pushq	-72(%rbp)
	pushq	-80(%rbp)
	call	_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPN2v88internal12_GLOBAL__N_126SharedFunctionInfoAndCountESt6vectorIS5_SaIS5_EEEElS5_NS0_5__ops15_Iter_less_iterEEvT_T0_SE_T1_T2_.isra.0
	addq	$32, %rsp
	testq	%rsi, %rsi
	jne	.L81
	movabsq	$-6148914691236517205, %r12
	subq	$24, %r13
	.p2align 4,,10
	.p2align 3
.L82:
	movq	16(%r13), %rax
	movdqu	(%rbx), %xmm3
	movq	%r13, %r14
	subq	$8, %rsp
	subq	%rbx, %r14
	movdqu	0(%r13), %xmm2
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movups	%xmm3, 0(%r13)
	movq	%r14, %rdx
	subq	$24, %r13
	movq	%rax, -64(%rbp)
	movl	16(%rbx), %eax
	sarq	$3, %rdx
	movaps	%xmm2, -80(%rbp)
	imulq	%r12, %rdx
	movl	%eax, 40(%r13)
	pushq	-64(%rbp)
	pushq	-72(%rbp)
	pushq	-80(%rbp)
	call	_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPN2v88internal12_GLOBAL__N_126SharedFunctionInfoAndCountESt6vectorIS5_SaIS5_EEEElS5_NS0_5__ops15_Iter_less_iterEEvT_T0_SE_T1_T2_.isra.0
	addq	$32, %rsp
	cmpq	$24, %r14
	jg	.L82
.L76:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L105:
	.cfi_restore_state
	movdqu	(%rax), %xmm6
	movq	16(%rbx), %rcx
	movups	%xmm6, (%rbx)
	movl	16(%rax), %edi
	movq	%rcx, -64(%rbp)
	movl	%edi, 16(%rbx)
	movl	%ecx, 16(%rax)
	movups	%xmm0, (%rax)
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L102:
	movl	-8(%rsi), %edi
	cmpl	%edi, 16(%rax)
	jne	.L131
	movl	-16(%rsi), %edi
	cmpl	%edi, 8(%rax)
	setb	%cl
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L97:
	movdqu	24(%rbx), %xmm7
	movl	40(%rbx), %ecx
	movups	%xmm0, 24(%rbx)
	movl	%eax, 40(%rbx)
	movl	%ecx, 16(%rbx)
	movups	%xmm7, (%rbx)
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L94:
	movl	-8(%rsi), %eax
	cmpl	%eax, 40(%rbx)
	jne	.L129
	movl	-16(%rsi), %eax
	cmpl	%eax, 32(%rbx)
	setb	%al
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L100:
	movl	-16(%rsi), %edi
	cmpl	%edi, 32(%rbx)
	setb	%dil
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L91:
	movl	-16(%rsi), %edx
	cmpl	%edx, 8(%rax)
	setb	%cl
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L125:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE25965:
	.size	_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPN2v88internal12_GLOBAL__N_126SharedFunctionInfoAndCountESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_less_iterEEvT_SD_T0_T1_.isra.0, .-_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPN2v88internal12_GLOBAL__N_126SharedFunctionInfoAndCountESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_less_iterEEvT_SD_T0_T1_.isra.0
	.section	.text._ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal12_GLOBAL__N_126SharedFunctionInfoAndCountESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_less_iterEEvT_SD_T0_.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal12_GLOBAL__N_126SharedFunctionInfoAndCountESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_less_iterEEvT_SD_T0_.isra.0, @function
_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal12_GLOBAL__N_126SharedFunctionInfoAndCountESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_less_iterEEvT_SD_T0_.isra.0:
.LFB25771:
	.cfi_startproc
	cmpq	%rsi, %rdi
	je	.L152
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	leaq	24(%rdi), %rbx
	subq	$32, %rsp
	cmpq	%rbx, %rsi
	je	.L136
	movl	$24, %r13d
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L155:
	setl	%al
.L139:
	testb	%al, %al
	je	.L141
.L156:
	movdqu	(%rbx), %xmm1
	movq	16(%rbx), %rax
	movaps	%xmm1, -64(%rbp)
	movq	%rax, -48(%rbp)
	cmpq	%rbx, %r12
	je	.L142
	movq	%rbx, %rdx
	leaq	(%r12,%r13), %rdi
	movq	%r12, %rsi
	subq	%r12, %rdx
	call	memmove@PLT
.L142:
	movdqa	-64(%rbp), %xmm2
	movl	-48(%rbp), %eax
	addq	$24, %rbx
	movl	%eax, 16(%r12)
	movups	%xmm2, (%r12)
	cmpq	%rbx, %r14
	je	.L136
.L149:
	movl	12(%rbx), %ecx
	cmpl	12(%r12), %ecx
	jne	.L155
	movl	16(%r12), %eax
	cmpl	%eax, 16(%rbx)
	je	.L140
	setg	%al
	testb	%al, %al
	jne	.L156
.L141:
	movq	16(%rbx), %rax
	movdqu	(%rbx), %xmm3
	movl	8(%rbx), %r8d
	movl	16(%rbx), %esi
	movq	%rax, -48(%rbp)
	movq	%rbx, %rax
	movaps	%xmm3, -64(%rbp)
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L157:
	setl	%dl
.L145:
	subq	$24, %rax
	testb	%dl, %dl
	je	.L147
.L158:
	movl	16(%rax), %edx
	movdqu	(%rax), %xmm0
	movl	%edx, 40(%rax)
	movups	%xmm0, 24(%rax)
.L148:
	movq	%rax, %rdi
	cmpl	-12(%rax), %ecx
	jne	.L157
	cmpl	-8(%rax), %esi
	je	.L146
	setg	%dl
	subq	$24, %rax
	testb	%dl, %dl
	jne	.L158
.L147:
	movl	%r8d, -56(%rbp)
	movdqa	-64(%rbp), %xmm4
	addq	$24, %rbx
	movl	%esi, -48(%rbp)
	movl	%esi, 16(%rdi)
	movups	%xmm4, (%rdi)
	cmpq	%rbx, %r14
	jne	.L149
.L136:
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L146:
	.cfi_restore_state
	cmpl	-16(%rax), %r8d
	setb	%dl
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L140:
	movl	8(%r12), %eax
	cmpl	%eax, 8(%rbx)
	setb	%al
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L152:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.cfi_endproc
.LFE25771:
	.size	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal12_GLOBAL__N_126SharedFunctionInfoAndCountESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_less_iterEEvT_SD_T0_.isra.0, .-_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal12_GLOBAL__N_126SharedFunctionInfoAndCountESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_less_iterEEvT_SD_T0_.isra.0
	.section	.text._ZN2v88internal8Coverage10SelectModeEPNS0_7IsolateENS_5debug12CoverageModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Coverage10SelectModeEPNS0_7IsolateENS_5debug12CoverageModeE
	.type	_ZN2v88internal8Coverage10SelectModeEPNS0_7IsolateENS_5debug12CoverageModeE, @function
_ZN2v88internal8Coverage10SelectModeEPNS0_7IsolateENS_5debug12CoverageModeE:
.LFB19967:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	41832(%rdi), %esi
	je	.L160
	call	_ZN2v88internal7Isolate42CollectSourcePositionsForAllBytecodeArraysEv@PLT
.L160:
	testl	%r12d, %r12d
	je	.L161
	leal	-1(%r12), %eax
	cmpl	$3, %eax
	ja	.L162
	movq	%rbx, %rdi
	leaq	-96(%rbp), %r15
	leal	-2(%r12), %r13d
	xorl	%r14d, %r14d
	movq	41088(%rbx), %rax
	addl	$1, 41104(%rbx)
	andl	$-3, %r13d
	movq	%rax, -128(%rbp)
	movq	41096(%rbx), %rax
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal11Deoptimizer13DeoptimizeAllEPNS0_7IsolateE@PLT
	leaq	37592(%rbx), %rsi
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal18HeapObjectIteratorC1EPNS0_4HeapENS1_20HeapObjectsFilteringE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal18HeapObjectIterator4NextEv@PLT
	movq	$0, -120(%rbp)
	movq	$0, -104(%rbp)
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L163
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L165:
	testl	%r13d, %r13d
	je	.L230
.L190:
	movq	(%rax), %rax
	cmpw	$155, 11(%rax)
	jne	.L189
	movl	$0, 35(%rsi)
.L189:
	movq	%r15, %rdi
	call	_ZN2v88internal18HeapObjectIterator4NextEv@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L231
.L163:
	movq	-1(%rsi), %rdx
	leaq	-1(%rsi), %rax
	cmpw	$1105, 11(%rdx)
	jne	.L165
	movabsq	$287762808832, %rcx
	movq	23(%rsi), %rax
	movq	7(%rax), %rax
	cmpq	%rcx, %rax
	je	.L189
	testb	$1, %al
	jne	.L168
.L172:
	movq	39(%rsi), %rax
	movq	7(%rax), %rax
	movq	-1(%rax), %rax
	cmpw	$125, 11(%rax)
	jne	.L189
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L223
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L175:
	cmpq	%r14, -120(%rbp)
	je	.L177
	movq	%rdx, (%r14)
	movq	%r15, %rdi
	addq	$8, %r14
	call	_ZN2v88internal18HeapObjectIterator4NextEv@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L163
	.p2align 4,,10
	.p2align 3
.L231:
	movq	%r15, %rdi
	call	_ZN2v88internal18HeapObjectIteratorD1Ev@PLT
	movq	-104(%rbp), %rax
	cmpq	%rax, %r14
	je	.L193
	movq	%rax, %r13
	.p2align 4,,10
	.p2align 3
.L194:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZN2v88internal10JSFunction20EnsureFeedbackVectorENS0_6HandleIS1_EE@PLT
	cmpq	%r13, %r14
	jne	.L194
.L193:
	movq	%rbx, %rdi
	call	_ZN2v88internal7Isolate33MaybeInitializeVectorListFromHeapEv@PLT
	movq	-104(%rbp), %rax
	testq	%rax, %rax
	je	.L195
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L195:
	movq	-128(%rbp), %rax
	subl	$1, 41104(%rbx)
	movq	%rax, 41088(%rbx)
	movq	-112(%rbp), %rax
	cmpq	41096(%rbx), %rax
	je	.L162
	movq	%rax, 41096(%rbx)
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	.p2align 4,,10
	.p2align 3
.L162:
	movl	%r12d, 41832(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L232
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L230:
	.cfi_restore_state
	movq	(%rax), %rdx
	cmpw	$160, 11(%rdx)
	jne	.L190
	movl	47(%rsi), %eax
	andl	$-134217729, %eax
	movl	%eax, 47(%rsi)
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L161:
	movq	41472(%rbx), %rdi
	call	_ZN2v88internal5Debug22RemoveAllCoverageInfosEv@PLT
	cmpl	$1, 41836(%rbx)
	je	.L162
	movq	88(%rbx), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Isolate35SetFeedbackVectorsForProfilingToolsENS0_6ObjectE@PLT
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L223:
	movq	41088(%rbx), %rdx
	cmpq	41096(%rbx), %rdx
	je	.L233
.L176:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rdx)
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L168:
	movq	-1(%rax), %rdx
	cmpw	$165, 11(%rdx)
	je	.L189
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L172
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L177:
	movabsq	$1152921504606846975, %rcx
	movq	-120(%rbp), %rsi
	subq	-104(%rbp), %rsi
	movq	%rsi, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L234
	testq	%rax, %rax
	je	.L235
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	ja	.L236
	testq	%rcx, %rcx
	jne	.L237
	movq	$0, -120(%rbp)
	movl	$8, %ecx
	xorl	%eax, %eax
.L182:
	movq	-104(%rbp), %r8
	movq	%rdx, (%rax,%rsi)
	cmpq	%r8, %r14
	je	.L203
	leaq	-8(%r14), %rsi
	leaq	15(%rax), %rdx
	subq	%r8, %rsi
	subq	%r8, %rdx
	movq	%rsi, %rdi
	shrq	$3, %rdi
	cmpq	$30, %rdx
	jbe	.L204
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rdi
	je	.L204
	addq	$1, %rdi
	xorl	%edx, %edx
	movq	%rdi, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L185:
	movdqu	(%r8,%rdx), %xmm0
	movups	%xmm0, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L185
	movq	%rdi, %r8
	movq	-104(%rbp), %rdx
	andq	$-2, %r8
	leaq	0(,%r8,8), %rcx
	addq	%rcx, %rdx
	addq	%rax, %rcx
	cmpq	%r8, %rdi
	je	.L187
	movq	(%rdx), %rdx
	movq	%rdx, (%rcx)
.L187:
	leaq	16(%rax,%rsi), %r14
.L183:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L188
	movq	%rax, -104(%rbp)
	call	_ZdlPv@PLT
	movq	-104(%rbp), %rax
.L188:
	movq	%rax, -104(%rbp)
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L229:
	movq	%r15, %rdi
	call	_ZN2v88internal18HeapObjectIteratorD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal7Isolate33MaybeInitializeVectorListFromHeapEv@PLT
	jmp	.L195
.L233:
	movq	%rbx, %rdi
	movq	%rsi, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L176
.L235:
	movl	$8, %ecx
.L180:
	movq	%rcx, %rdi
	movq	%rsi, -144(%rbp)
	movq	%rdx, -136(%rbp)
	movq	%rcx, -120(%rbp)
	call	_Znwm@PLT
	movq	-120(%rbp), %rcx
	movq	-136(%rbp), %rdx
	movq	-144(%rbp), %rsi
	addq	%rax, %rcx
	movq	%rcx, -120(%rbp)
	leaq	8(%rax), %rcx
	jmp	.L182
.L236:
	movabsq	$9223372036854775800, %rcx
	jmp	.L180
.L204:
	movq	-104(%rbp), %rdx
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L184:
	movq	(%rdx), %rdi
	addq	$8, %rdx
	addq	$8, %rcx
	movq	%rdi, -8(%rcx)
	cmpq	%r14, %rdx
	jne	.L184
	jmp	.L187
.L237:
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rcx
	cmova	%rax, %rcx
	salq	$3, %rcx
	jmp	.L180
.L203:
	movq	%rcx, %r14
	jmp	.L183
.L232:
	call	__stack_chk_fail@PLT
.L234:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE19967:
	.size	_ZN2v88internal8Coverage10SelectModeEPNS0_7IsolateENS_5debug12CoverageModeE, .-_ZN2v88internal8Coverage10SelectModeEPNS0_7IsolateENS_5debug12CoverageModeE
	.section	.text._ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE12emplace_backIJRiS6_RjEEEvDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE12emplace_backIJRiS6_RjEEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE12emplace_backIJRiS6_RjEEEvDpOT_
	.type	_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE12emplace_backIJRiS6_RjEEEvDpOT_, @function
_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE12emplace_backIJRiS6_RjEEEvDpOT_:
.LFB22156:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	8(%rdi), %r12
	cmpq	16(%rdi), %r12
	je	.L239
	movl	(%rsi), %esi
	movl	(%rdx), %edx
	movl	(%rcx), %eax
	movl	%esi, (%r12)
	movl	%edx, 4(%r12)
	movl	%eax, 8(%r12)
	addq	$12, 8(%rdi)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L239:
	.cfi_restore_state
	movq	(%rdi), %r14
	movq	%r12, %r8
	movabsq	$-6148914691236517205, %rdi
	subq	%r14, %r8
	movq	%r8, %rax
	sarq	$2, %rax
	imulq	%rdi, %rax
	movabsq	$768614336404564650, %rdi
	cmpq	%rdi, %rax
	je	.L255
	testq	%rax, %rax
	je	.L247
	movabsq	$9223372036854775800, %r15
	leaq	(%rax,%rax), %r9
	cmpq	%r9, %rax
	jbe	.L256
.L242:
	movq	%r15, %rdi
	movq	%rcx, -80(%rbp)
	movq	%rdx, -72(%rbp)
	movq	%rsi, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %rsi
	movq	-72(%rbp), %rdx
	movq	-80(%rbp), %rcx
	movq	%rax, %r13
	addq	%rax, %r15
	leaq	12(%rax), %r9
.L243:
	movl	(%rdx), %edi
	movl	(%rcx), %edx
	leaq	0(%r13,%r8), %rax
	movl	(%rsi), %ecx
	movl	%edi, 4(%rax)
	movl	%ecx, (%rax)
	movl	%edx, 8(%rax)
	cmpq	%r14, %r12
	je	.L244
	movq	%r13, %rcx
	movq	%r14, %rdx
	.p2align 4,,10
	.p2align 3
.L245:
	movq	(%rdx), %rsi
	addq	$12, %rdx
	addq	$12, %rcx
	movq	%rsi, -12(%rcx)
	movl	-4(%rdx), %esi
	movl	%esi, -4(%rcx)
	cmpq	%rdx, %r12
	jne	.L245
	subq	$12, %r12
	subq	%r14, %r12
	shrq	$2, %r12
	leaq	24(%r13,%r12,4), %r9
.L244:
	testq	%r14, %r14
	je	.L246
	movq	%r14, %rdi
	movq	%r9, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r9
.L246:
	movq	%r13, %xmm0
	movq	%r9, %xmm1
	movq	%r15, 16(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L256:
	.cfi_restore_state
	testq	%r9, %r9
	jne	.L257
	movl	$12, %r9d
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L247:
	movl	$12, %r15d
	jmp	.L242
.L255:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L257:
	cmpq	%rdi, %r9
	cmovbe	%r9, %rdi
	imulq	$12, %rdi, %r15
	jmp	.L242
	.cfi_endproc
.LFE22156:
	.size	_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE12emplace_backIJRiS6_RjEEEvDpOT_, .-_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE12emplace_backIJRiS6_RjEEEvDpOT_
	.section	.text._ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE12emplace_backIJRS2_EEEvDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE12emplace_backIJRS2_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE12emplace_backIJRS2_EEEvDpOT_
	.type	_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE12emplace_backIJRS2_EEEvDpOT_, @function
_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE12emplace_backIJRS2_EEEvDpOT_:
.LFB22157:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %r12
	cmpq	16(%rdi), %r12
	je	.L259
	movq	(%rsi), %rax
	movq	%rax, (%r12)
	movl	8(%rsi), %eax
	movl	%eax, 8(%r12)
	addq	$12, 8(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L259:
	.cfi_restore_state
	movabsq	$-6148914691236517205, %rcx
	movq	(%rdi), %r15
	movq	%r12, %rdx
	movabsq	$768614336404564650, %rsi
	subq	%r15, %rdx
	movq	%rdx, %rax
	sarq	$2, %rax
	imulq	%rcx, %rax
	cmpq	%rsi, %rax
	je	.L275
	testq	%rax, %rax
	je	.L267
	movabsq	$9223372036854775800, %rcx
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L276
.L262:
	movq	%rcx, %rdi
	movq	%rdx, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rdx
	movq	%rax, %r14
	leaq	(%rax,%rcx), %rax
	leaq	12(%r14), %rcx
.L263:
	movq	0(%r13), %rsi
	movq	%rsi, (%r14,%rdx)
	movl	8(%r13), %esi
	movl	%esi, 8(%r14,%rdx)
	cmpq	%r15, %r12
	je	.L264
	movq	%r14, %rcx
	movq	%r15, %rdx
	.p2align 4,,10
	.p2align 3
.L265:
	movq	(%rdx), %rsi
	addq	$12, %rdx
	addq	$12, %rcx
	movq	%rsi, -12(%rcx)
	movl	-4(%rdx), %esi
	movl	%esi, -4(%rcx)
	cmpq	%rdx, %r12
	jne	.L265
	subq	$12, %r12
	subq	%r15, %r12
	shrq	$2, %r12
	leaq	24(%r14,%r12,4), %rcx
.L264:
	testq	%r15, %r15
	je	.L266
	movq	%r15, %rdi
	movq	%rax, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %rax
	movq	-56(%rbp), %rcx
.L266:
	movq	%r14, %xmm0
	movq	%rcx, %xmm1
	movq	%rax, 16(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L276:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L277
	movl	$12, %ecx
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L267:
	movl	$12, %ecx
	jmp	.L262
.L275:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L277:
	cmpq	%rsi, %rdi
	movq	%rsi, %rax
	cmovbe	%rdi, %rax
	imulq	$12, %rax, %rcx
	jmp	.L262
	.cfi_endproc
.LFE22157:
	.size	_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE12emplace_backIJRS2_EEEvDpOT_, .-_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE12emplace_backIJRS2_EEEvDpOT_
	.section	.rodata._ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE17_M_default_appendEm.str1.1,"aMS",@progbits,1
.LC3:
	.string	"vector::_M_default_append"
	.section	.text._ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE17_M_default_appendEm,"axG",@progbits,_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE17_M_default_appendEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE17_M_default_appendEm
	.type	_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE17_M_default_appendEm, @function
_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE17_M_default_appendEm:
.LFB23534:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L324
	movabsq	$-6148914691236517205, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %rsi
	movq	16(%r12), %rax
	movq	%rsi, %rdx
	subq	(%rdi), %rdx
	subq	%rsi, %rax
	movabsq	$768614336404564650, %rdi
	movq	%rdx, %r13
	sarq	$2, %rax
	movq	%rdi, %r8
	sarq	$2, %r13
	imulq	%rcx, %rax
	imulq	%rcx, %r13
	subq	%r13, %r8
	cmpq	%rbx, %rax
	jb	.L280
	leaq	-1(%rbx), %rax
	cmpq	$3, %rax
	jbe	.L295
	leaq	-4(%rbx), %rcx
	leaq	8(%rsi), %rax
	xorl	%edx, %edx
	shrq	$2, %rcx
	addq	$1, %rcx
	.p2align 4,,10
	.p2align 3
.L282:
	addq	$1, %rdx
	movq	$-1, -8(%rax)
	addq	$48, %rax
	movq	$-1, -44(%rax)
	movq	$-1, -32(%rax)
	movq	$-1, -20(%rax)
	movl	$0, -48(%rax)
	movl	$0, -36(%rax)
	movl	$0, -24(%rax)
	movl	$0, -12(%rax)
	cmpq	%rdx, %rcx
	ja	.L282
	leaq	0(,%rcx,4), %rdx
	movq	%rbx, %rdi
	leaq	(%rdx,%rcx,8), %rax
	subq	%rdx, %rdi
	leaq	(%rsi,%rax,4), %rax
	cmpq	%rdx, %rbx
	je	.L283
.L281:
	movq	$-1, (%rax)
	movl	$0, 8(%rax)
	cmpq	$1, %rdi
	je	.L283
	movq	$-1, 12(%rax)
	movl	$0, 20(%rax)
	cmpq	$2, %rdi
	je	.L283
	movq	$-1, 24(%rax)
	movl	$0, 32(%rax)
	cmpq	$3, %rdi
	je	.L283
	movq	$-1, 36(%rax)
	movl	$0, 44(%rax)
.L283:
	leaq	(%rbx,%rbx,2), %rax
	leaq	(%rsi,%rax,4), %rax
	movq	%rax, 8(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L324:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L280:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	cmpq	%rbx, %r8
	jb	.L327
	cmpq	%rbx, %r13
	movq	%rbx, %rax
	movq	%rdx, -56(%rbp)
	cmovnb	%r13, %rax
	addq	%r13, %rax
	cmpq	%rdi, %rax
	cmovbe	%rax, %rdi
	leaq	(%rdi,%rdi,2), %r14
	salq	$2, %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	-56(%rbp), %rdx
	leaq	-1(%rbx), %rcx
	movq	%rax, %r15
	leaq	(%rax,%rdx), %rax
	cmpq	$3, %rcx
	jbe	.L328
	leaq	-4(%rbx), %rdi
	movq	%rax, %rcx
	leaq	8(%r15,%rdx), %rdx
	xorl	%esi, %esi
	shrq	$2, %rdi
	addq	$1, %rdi
	.p2align 4,,10
	.p2align 3
.L289:
	addq	$1, %rsi
	movq	$-1, (%rcx)
	addq	$48, %rdx
	addq	$48, %rcx
	movq	$-1, -36(%rcx)
	movq	$-1, -24(%rcx)
	movq	$-1, -12(%rcx)
	movl	$0, -48(%rdx)
	movl	$0, -36(%rdx)
	movl	$0, -24(%rdx)
	movl	$0, -12(%rdx)
	cmpq	%rsi, %rdi
	ja	.L289
	leaq	0(,%rdi,4), %rdx
	movq	%rbx, %rcx
	leaq	(%rdx,%rdi,8), %rsi
	subq	%rdx, %rcx
	leaq	(%rax,%rsi,4), %rax
	cmpq	%rdx, %rbx
	je	.L287
.L294:
	movq	$-1, (%rax)
	movl	$0, 8(%rax)
	cmpq	$1, %rcx
	je	.L287
	movq	$-1, 12(%rax)
	movl	$0, 20(%rax)
	cmpq	$2, %rcx
	je	.L287
	movq	$-1, 24(%rax)
	movl	$0, 32(%rax)
	cmpq	$3, %rcx
	je	.L287
	movq	$-1, 36(%rax)
	movl	$0, 44(%rax)
.L287:
	movq	(%r12), %rdi
	movq	8(%r12), %rax
	movq	%r15, %rcx
	movq	%rdi, %rdx
	cmpq	%rdi, %rax
	je	.L293
	.p2align 4,,10
	.p2align 3
.L290:
	movq	(%rdx), %rsi
	addq	$12, %rdx
	addq	$12, %rcx
	movq	%rsi, -12(%rcx)
	movl	-4(%rdx), %esi
	movl	%esi, -4(%rcx)
	cmpq	%rdx, %rax
	jne	.L290
.L293:
	testq	%rdi, %rdi
	je	.L292
	call	_ZdlPv@PLT
.L292:
	addq	%r13, %rbx
	addq	%r15, %r14
	movq	%r15, (%r12)
	leaq	(%rbx,%rbx,2), %rax
	movq	%r14, 16(%r12)
	leaq	(%r15,%rax,4), %rax
	movq	%rax, 8(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L295:
	.cfi_restore_state
	movq	%rsi, %rax
	movq	%rbx, %rdi
	jmp	.L281
.L328:
	movq	%rbx, %rcx
	jmp	.L294
.L327:
	leaq	.LC3(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE23534:
	.size	_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE17_M_default_appendEm, .-_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE17_M_default_appendEm
	.section	.text._ZN2v88internal12_GLOBAL__N_122MergeConsecutiveRangesEPNS0_16CoverageFunctionE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_122MergeConsecutiveRangesEPNS0_16CoverageFunctionE, @function
_ZN2v88internal12_GLOBAL__N_122MergeConsecutiveRangesEPNS0_16CoverageFunctionE:
.LFB19906:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-88(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movabsq	$-6148914691236517205, %rbx
	subq	$72, %rsp
	movq	24(%rdi), %r8
	movq	32(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	xorl	%eax, %eax
	movq	%rdi, -96(%rbp)
	subq	%r8, %rdx
	movw	%ax, -64(%rbp)
	movl	$-1, %eax
	sarq	$2, %rdx
	movq	$0, -88(%rbp)
	imulq	%rbx, %rdx
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	$-1, -60(%rbp)
	testl	%edx, %edx
	jle	.L388
	.p2align 4,,10
	.p2align 3
.L372:
	movslq	-56(%rbp), %rdx
	testl	%eax, %eax
	js	.L336
	cmpl	%edx, %eax
	je	.L336
	cltq
	leaq	(%rdx,%rdx,2), %rdx
	leaq	(%rax,%rax,2), %rax
	leaq	(%r8,%rdx,4), %rdx
	leaq	(%r8,%rax,4), %rax
	movq	(%rax), %rcx
	movq	%rcx, (%rdx)
	movl	8(%rax), %eax
	movl	%eax, 8(%rdx)
	movl	-56(%rbp), %edx
.L336:
	addl	$1, %edx
	movq	-96(%rbp), %rsi
	movl	%edx, -56(%rbp)
	movslq	-60(%rbp), %rdx
	cmpl	$-1, %edx
	je	.L332
	cmpb	$0, -63(%rbp)
	jne	.L333
	movq	24(%rsi), %rax
	leaq	(%rdx,%rdx,2), %rdx
	movq	%r12, %rdi
	leaq	(%rax,%rdx,4), %rsi
	call	_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE12emplace_backIJRS2_EEEvDpOT_
	movl	-60(%rbp), %edx
	movq	-96(%rbp), %rsi
.L333:
	leal	1(%rdx), %eax
	movq	-88(%rbp), %r13
	movq	-80(%rbp), %rcx
	movb	$0, -63(%rbp)
	movslq	%eax, %r11
	movl	%eax, -60(%rbp)
	movq	24(%rsi), %r8
	leaq	(%r11,%r11,2), %r9
	salq	$2, %r9
	leaq	(%r8,%r9), %r10
	jmp	.L387
	.p2align 4,,10
	.p2align 3
.L389:
	movl	(%r10), %edi
	cmpl	%edi, -8(%rcx)
	jg	.L339
	subq	$12, %rcx
	movq	%rcx, -80(%rbp)
.L387:
	movq	%rcx, %rdi
	subq	%r13, %rdi
	sarq	$2, %rdi
	imulq	%rbx, %rdi
	cmpq	$1, %rdi
	ja	.L389
.L339:
	leal	2(%rdx), %r13d
	movq	32(%rsi), %rdx
	subq	%r8, %rdx
	sarq	$2, %rdx
	imulq	%rbx, %rdx
	cmpl	%edx, %r13d
	jl	.L357
	cmpb	$0, -64(%rbp)
	je	.L390
.L341:
	movb	$1, -64(%rbp)
	movq	24(%rsi), %rdi
	movabsq	$-6148914691236517205, %rbx
	movq	32(%rsi), %rdx
	movzbl	-63(%rbp), %ecx
	subq	%rdi, %rdx
	sarq	$2, %rdx
	imulq	%rbx, %rdx
	cmpl	%edx, %r13d
	jge	.L380
	testb	%cl, %cl
	jne	.L391
.L381:
	movslq	-56(%rbp), %rdx
	testl	%eax, %eax
	js	.L349
	cmpl	%edx, %eax
	je	.L350
	cltq
	leaq	(%rdx,%rdx,2), %rdx
	leaq	(%rax,%rax,2), %rax
	leaq	(%rdi,%rdx,4), %rdx
	leaq	(%rdi,%rax,4), %rax
	movq	(%rax), %rcx
	movq	%rcx, (%rdx)
	movl	8(%rax), %eax
	movq	-96(%rbp), %rsi
	movl	%eax, 8(%rdx)
	movl	-60(%rbp), %eax
	movl	-56(%rbp), %edx
.L349:
	addl	$1, %edx
	movl	%edx, -56(%rbp)
	cmpl	$-1, %eax
	je	.L361
.L351:
	cmpb	$0, -63(%rbp)
	movslq	%eax, %r9
	jne	.L352
	movq	24(%rsi), %rax
	leaq	(%r9,%r9,2), %rdx
	leaq	-88(%rbp), %rdi
	leaq	(%rax,%rdx,4), %rsi
	call	_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE12emplace_backIJRS2_EEEvDpOT_
	movl	-60(%rbp), %r9d
	movq	-96(%rbp), %rsi
.L352:
	leal	1(%r9), %eax
	movq	-80(%rbp), %rcx
	movq	-88(%rbp), %r10
	movb	$0, -63(%rbp)
	movl	%eax, -60(%rbp)
	movslq	%eax, %rdx
	movq	24(%rsi), %rdi
	leaq	(%rdx,%rdx,2), %rdx
	leaq	(%rdi,%rdx,4), %r8
	movq	%rcx, %rdx
	subq	%r10, %rdx
	sarq	$2, %rdx
	imulq	%rbx, %rdx
	cmpq	$1, %rdx
	jbe	.L353
	subq	$12, %rcx
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L392:
	subq	%r10, %rdx
	movq	%rcx, -80(%rbp)
	subq	$12, %rcx
	sarq	$2, %rdx
	imulq	%rbx, %rdx
	cmpq	$1, %rdx
	jbe	.L353
.L354:
	movl	(%r8), %r11d
	movq	%rcx, %rdx
	cmpl	%r11d, 4(%rcx)
	jle	.L392
.L353:
	movq	32(%rsi), %rdx
	addl	$2, %r9d
	subq	%rdi, %rdx
	sarq	$2, %rdx
	imulq	%rbx, %rdx
	cmpl	%edx, %r9d
	jl	.L381
	cmpb	$0, -64(%rbp)
	movslq	-56(%rbp), %rdx
	jne	.L335
	testl	%eax, %eax
	js	.L345
	cmpl	%edx, %eax
	je	.L345
	leaq	(%rdx,%rdx,2), %rax
	movq	(%r8), %rdx
	leaq	(%rdi,%rax,4), %rax
	movq	%rdx, (%rax)
	movl	8(%r8), %edx
	movq	-96(%rbp), %rsi
	movl	%edx, 8(%rax)
	movl	-56(%rbp), %edx
.L345:
	addl	$1, %edx
	movl	%edx, -56(%rbp)
.L335:
	movb	$1, -64(%rbp)
	movq	32(%rsi), %rcx
	movslq	%edx, %rdx
	movabsq	$-6148914691236517205, %r8
	movq	24(%rsi), %rdi
	movq	%rcx, %rax
	subq	%rdi, %rax
	sarq	$2, %rax
	imulq	%r8, %rax
	cmpq	%rax, %rdx
	ja	.L393
	jb	.L394
.L355:
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L329
	call	_ZdlPv@PLT
.L329:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L395
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L361:
	.cfi_restore_state
	leaq	8(%rsi), %rcx
	leaq	4(%rsi), %rdx
	leaq	-88(%rbp), %rdi
	call	_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE12emplace_backIJRiS6_RjEEEvDpOT_
	movl	-60(%rbp), %r9d
	movq	-96(%rbp), %rsi
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L357:
	leaq	12(%r8,%r9), %rdx
	movl	(%rdx), %esi
	cmpl	-8(%rcx), %esi
	jge	.L372
	cmpl	%esi, 4(%r10)
	jne	.L372
	movl	8(%r10), %esi
	cmpl	%esi, 8(%rdx)
	jne	.L372
	movl	(%r10), %eax
	movl	%eax, (%rdx)
	movq	-96(%rbp), %rsi
	movb	$1, -63(%rbp)
	movl	-60(%rbp), %eax
	movq	32(%rsi), %rdx
	subq	24(%rsi), %rdx
	sarq	$2, %rdx
	leal	1(%rax), %r13d
	imulq	%rbx, %rdx
	cmpl	%edx, %r13d
	jge	.L341
	movl	%eax, %edx
	cmpl	$-1, %eax
	jne	.L333
.L332:
	leaq	4(%rsi), %rdx
	leaq	8(%rsi), %rcx
	movq	%r12, %rdi
	call	_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE12emplace_backIJRiS6_RjEEEvDpOT_
	movl	-60(%rbp), %edx
	movq	-96(%rbp), %rsi
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L391:
	cmpl	$-1, %eax
	je	.L361
	movl	%eax, %r9d
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L380:
	movl	-56(%rbp), %edx
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L350:
	leal	1(%rax), %edx
	movl	%edx, -56(%rbp)
	jmp	.L351
	.p2align 4,,10
	.p2align 3
.L390:
	movslq	-56(%rbp), %rdx
	testl	%eax, %eax
	js	.L331
	cmpl	%edx, %eax
	je	.L331
	leaq	(%rdx,%rdx,2), %rax
	leaq	(%r11,%r11,2), %rdx
	leaq	(%r8,%rdx,4), %rdx
	leaq	(%r8,%rax,4), %rax
	movq	(%rdx), %rcx
	movq	%rcx, (%rax)
	movl	8(%rdx), %edx
	movq	-96(%rbp), %rsi
	movl	%edx, 8(%rax)
	movl	-60(%rbp), %eax
	movl	-56(%rbp), %edx
	leal	1(%rax), %r13d
.L331:
	addl	$1, %edx
	movl	%edx, -56(%rbp)
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L394:
	leaq	(%rdx,%rdx,2), %rax
	leaq	(%rdi,%rax,4), %rax
	cmpq	%rax, %rcx
	je	.L355
	movq	%rax, 32(%rsi)
	jmp	.L355
	.p2align 4,,10
	.p2align 3
.L388:
	movl	-56(%rbp), %edx
	movq	%rdi, %rsi
	xorl	%r13d, %r13d
	addl	$1, %edx
	movl	%edx, -56(%rbp)
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L393:
	subq	%rax, %rdx
	leaq	24(%rsi), %rdi
	movq	%rdx, %rsi
	call	_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE17_M_default_appendEm
	jmp	.L355
.L395:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19906:
	.size	_ZN2v88internal12_GLOBAL__N_122MergeConsecutiveRangesEPNS0_16CoverageFunctionE, .-_ZN2v88internal12_GLOBAL__N_122MergeConsecutiveRangesEPNS0_16CoverageFunctionE
	.section	.text._ZNSt6vectorIN2v88internal14CoverageScriptESaIS2_EE17_M_realloc_insertIJRNS1_6HandleINS1_6ScriptEEEEEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal14CoverageScriptESaIS2_EE17_M_realloc_insertIJRNS1_6HandleINS1_6ScriptEEEEEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal14CoverageScriptESaIS2_EE17_M_realloc_insertIJRNS1_6HandleINS1_6ScriptEEEEEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal14CoverageScriptESaIS2_EE17_M_realloc_insertIJRNS1_6HandleINS1_6ScriptEEEEEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, @function
_ZNSt6vectorIN2v88internal14CoverageScriptESaIS2_EE17_M_realloc_insertIJRNS1_6HandleINS1_6ScriptEEEEEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_:
.LFB23590:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r15
	movq	(%rdi), %r13
	movabsq	$288230376151711743, %rdi
	movq	%r15, %rax
	subq	%r13, %rax
	sarq	$5, %rax
	cmpq	%rdi, %rax
	je	.L415
	movq	%rsi, %rcx
	movq	%rsi, %r12
	subq	%r13, %rcx
	testq	%rax, %rax
	je	.L406
	movabsq	$9223372036854775776, %rsi
	leaq	(%rax,%rax), %r8
	cmpq	%r8, %rax
	jbe	.L416
.L398:
	movq	%rsi, %rdi
	movq	%rdx, -72(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %rdx
	movq	%rax, %rbx
	leaq	32(%rax), %r8
	addq	%rax, %rsi
.L405:
	movq	(%rdx), %rdx
	leaq	(%rbx,%rcx), %rax
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	cmpq	%r13, %r12
	je	.L400
	movq	%rbx, %rdx
	movq	%r13, %rax
	.p2align 4,,10
	.p2align 3
.L401:
	movq	(%rax), %rcx
	addq	$32, %rax
	addq	$32, %rdx
	movq	%rcx, -32(%rdx)
	movdqu	-24(%rax), %xmm1
	movups	%xmm1, -24(%rdx)
	movq	-8(%rax), %rcx
	movq	%rcx, -8(%rdx)
	cmpq	%rax, %r12
	jne	.L401
	movq	%r12, %rax
	subq	%r13, %rax
	leaq	32(%rbx,%rax), %r8
.L400:
	cmpq	%r15, %r12
	je	.L402
	movq	%r8, %rdx
	movq	%r12, %rax
	.p2align 4,,10
	.p2align 3
.L403:
	movq	(%rax), %rcx
	movdqu	8(%rax), %xmm2
	addq	$32, %rax
	addq	$32, %rdx
	movq	%rcx, -32(%rdx)
	movq	-8(%rax), %rcx
	movups	%xmm2, -24(%rdx)
	movq	%rcx, -8(%rdx)
	cmpq	%r15, %rax
	jne	.L403
	subq	%r12, %rax
	addq	%rax, %r8
.L402:
	testq	%r13, %r13
	je	.L404
	movq	%r13, %rdi
	movq	%rsi, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %r8
.L404:
	movq	%rbx, %xmm0
	movq	%r8, %xmm3
	movq	%rsi, 16(%r14)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, (%r14)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L416:
	.cfi_restore_state
	testq	%r8, %r8
	jne	.L399
	movl	$32, %r8d
	xorl	%esi, %esi
	xorl	%ebx, %ebx
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L406:
	movl	$32, %esi
	jmp	.L398
.L415:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L399:
	cmpq	%rdi, %r8
	cmovbe	%r8, %rdi
	movq	%rdi, %rsi
	salq	$5, %rsi
	jmp	.L398
	.cfi_endproc
.LFE23590:
	.size	_ZNSt6vectorIN2v88internal14CoverageScriptESaIS2_EE17_M_realloc_insertIJRNS1_6HandleINS1_6ScriptEEEEEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, .-_ZNSt6vectorIN2v88internal14CoverageScriptESaIS2_EE17_M_realloc_insertIJRNS1_6HandleINS1_6ScriptEEEEEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.section	.text._ZNSt6vectorIN2v88internal16CoverageFunctionESaIS2_EE17_M_realloc_insertIJRS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal16CoverageFunctionESaIS2_EE17_M_realloc_insertIJRS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal16CoverageFunctionESaIS2_EE17_M_realloc_insertIJRS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal16CoverageFunctionESaIS2_EE17_M_realloc_insertIJRS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, @function
_ZNSt6vectorIN2v88internal16CoverageFunctionESaIS2_EE17_M_realloc_insertIJRS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_:
.LFB23624:
	.cfi_startproc
	endbr64
	movabsq	$7905747460161236407, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rbx
	movq	(%rdi), %r13
	movq	%rdi, -72(%rbp)
	movq	%rbx, %rax
	subq	%r13, %rax
	sarq	$3, %rax
	imulq	%rcx, %rax
	movabsq	$164703072086692425, %rcx
	cmpq	%rcx, %rax
	je	.L445
	movq	%rsi, %r9
	movq	%rsi, %r12
	subq	%r13, %r9
	testq	%rax, %rax
	je	.L435
	leaq	(%rax,%rax), %rsi
	movq	%rsi, -64(%rbp)
	cmpq	%rsi, %rax
	jbe	.L446
	movabsq	$9223372036854775800, %rax
	movq	%rax, -64(%rbp)
.L419:
	movq	-64(%rbp), %rdi
	movq	%rdx, -88(%rbp)
	movq	%r9, -80(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %r9
	movq	-88(%rbp), %rdx
	movq	%rax, -56(%rbp)
.L434:
	movq	-56(%rbp), %rax
	movq	32(%rdx), %r8
	movabsq	$-6148914691236517205, %rdi
	pxor	%xmm0, %xmm0
	movq	24(%rdx), %r10
	movq	(%rdx), %rcx
	leaq	(%rax,%r9), %r14
	movl	8(%rdx), %eax
	movq	%r8, %r15
	subq	%r10, %r15
	movq	%rcx, (%r14)
	movl	%eax, 8(%r14)
	movq	16(%rdx), %rax
	movq	$0, 40(%r14)
	movq	%rax, 16(%r14)
	movq	%r15, %rax
	sarq	$2, %rax
	movups	%xmm0, 24(%r14)
	imulq	%rdi, %rax
	testq	%rax, %rax
	je	.L422
	movabsq	$768614336404564650, %rdi
	cmpq	%rdi, %rax
	ja	.L447
	movq	%r15, %rdi
	movq	%rdx, -80(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rdx
	movq	32(%rdx), %r8
	movq	24(%rdx), %r10
.L422:
	movq	%rax, %xmm0
	leaq	(%rax,%r15), %rcx
	punpcklqdq	%xmm0, %xmm0
	movq	%rcx, 40(%r14)
	movups	%xmm0, 24(%r14)
	cmpq	%r10, %r8
	je	.L424
	movq	%r10, %rcx
	movq	%rax, %rdi
	.p2align 4,,10
	.p2align 3
.L425:
	movq	(%rcx), %r11
	addq	$12, %rcx
	addq	$12, %rdi
	movq	%r11, -12(%rdi)
	movl	-4(%rcx), %r11d
	movl	%r11d, -4(%rdi)
	cmpq	%rcx, %r8
	jne	.L425
	subq	$12, %r8
	subq	%r10, %r8
	shrq	$2, %r8
	leaq	12(%rax,%r8,4), %rax
.L424:
	movq	%rax, 32(%r14)
	movzbl	48(%rdx), %eax
	movb	%al, 48(%r14)
	cmpq	%r13, %r12
	je	.L437
	movq	-56(%rbp), %r14
	movq	%r13, %r15
	pxor	%xmm0, %xmm0
	.p2align 4,,10
	.p2align 3
.L430:
	movl	(%r15), %ecx
	movl	%ecx, (%r14)
	movl	4(%r15), %ecx
	movl	%ecx, 4(%r14)
	movl	8(%r15), %ecx
	movl	%ecx, 8(%r14)
	movq	16(%r15), %rcx
	movq	%rcx, 16(%r14)
	movdqu	24(%r15), %xmm1
	movups	%xmm1, 24(%r14)
	movq	40(%r15), %rcx
	movq	%rcx, 40(%r14)
	movzbl	48(%r15), %ecx
	movups	%xmm0, 24(%r15)
	movq	$0, 40(%r15)
	movb	%cl, 48(%r14)
	movq	24(%r15), %rdi
	testq	%rdi, %rdi
	je	.L427
	call	_ZdlPv@PLT
	addq	$56, %r15
	addq	$56, %r14
	pxor	%xmm0, %xmm0
	cmpq	%r15, %r12
	jne	.L430
.L428:
	leaq	-56(%r12), %rax
	movq	-56(%rbp), %rsi
	movabsq	$988218432520154551, %rdx
	subq	%r13, %rax
	shrq	$3, %rax
	imulq	%rdx, %rax
	movabsq	$2305843009213693951, %rdx
	andq	%rdx, %rax
	addq	$1, %rax
	leaq	0(,%rax,8), %rdx
	subq	%rax, %rdx
	leaq	(%rsi,%rdx,8), %r8
.L426:
	addq	$56, %r8
	cmpq	%rbx, %r12
	je	.L431
	movq	%r12, %rax
	movq	%r8, %rdx
	.p2align 4,,10
	.p2align 3
.L432:
	movl	(%rax), %ecx
	movdqu	24(%rax), %xmm2
	addq	$56, %rax
	addq	$56, %rdx
	movl	%ecx, -56(%rdx)
	movl	-52(%rax), %ecx
	movups	%xmm2, -32(%rdx)
	movl	%ecx, -52(%rdx)
	movl	-48(%rax), %ecx
	movl	%ecx, -48(%rdx)
	movq	-40(%rax), %rcx
	movq	%rcx, -40(%rdx)
	movq	-16(%rax), %rcx
	movq	%rcx, -16(%rdx)
	movzbl	-8(%rax), %ecx
	movb	%cl, -8(%rdx)
	cmpq	%rbx, %rax
	jne	.L432
	movabsq	$988218432520154551, %rdx
	subq	%r12, %rax
	subq	$56, %rax
	shrq	$3, %rax
	imulq	%rdx, %rax
	movabsq	$2305843009213693951, %rdx
	andq	%rdx, %rax
	addq	$1, %rax
	leaq	0(,%rax,8), %rdx
	subq	%rax, %rdx
	leaq	(%r8,%rdx,8), %r8
.L431:
	testq	%r13, %r13
	je	.L433
	movq	%r13, %rdi
	movq	%r8, -80(%rbp)
	call	_ZdlPv@PLT
	movq	-80(%rbp), %r8
.L433:
	movq	-56(%rbp), %rax
	movq	-72(%rbp), %rsi
	movq	%r8, %xmm3
	movq	%rax, %xmm0
	addq	-64(%rbp), %rax
	punpcklqdq	%xmm3, %xmm0
	movq	%rax, 16(%rsi)
	movups	%xmm0, (%rsi)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L446:
	.cfi_restore_state
	testq	%rsi, %rsi
	jne	.L420
	movq	$0, -56(%rbp)
	jmp	.L434
	.p2align 4,,10
	.p2align 3
.L427:
	addq	$56, %r15
	addq	$56, %r14
	cmpq	%r15, %r12
	jne	.L430
	jmp	.L428
	.p2align 4,,10
	.p2align 3
.L435:
	movq	$56, -64(%rbp)
	jmp	.L419
	.p2align 4,,10
	.p2align 3
.L437:
	movq	-56(%rbp), %r8
	jmp	.L426
.L447:
	call	_ZSt17__throw_bad_allocv@PLT
.L420:
	movq	-64(%rbp), %rax
	cmpq	%rcx, %rax
	cmova	%rcx, %rax
	imulq	$56, %rax, %rax
	movq	%rax, -64(%rbp)
	jmp	.L419
.L445:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE23624:
	.size	_ZNSt6vectorIN2v88internal16CoverageFunctionESaIS2_EE17_M_realloc_insertIJRS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, .-_ZNSt6vectorIN2v88internal16CoverageFunctionESaIS2_EE17_M_realloc_insertIJRS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.section	.text._ZNSt6vectorIN2v88internal16CoverageFunctionESaIS2_EE12emplace_backIJRS2_EEEvDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal16CoverageFunctionESaIS2_EE12emplace_backIJRS2_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal16CoverageFunctionESaIS2_EE12emplace_backIJRS2_EEEvDpOT_
	.type	_ZNSt6vectorIN2v88internal16CoverageFunctionESaIS2_EE12emplace_backIJRS2_EEEvDpOT_, @function
_ZNSt6vectorIN2v88internal16CoverageFunctionESaIS2_EE12emplace_backIJRS2_EEEvDpOT_:
.LFB22255:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	8(%rdi), %r13
	cmpq	16(%rdi), %r13
	je	.L449
	movl	8(%rsi), %eax
	movq	(%rsi), %rdx
	pxor	%xmm0, %xmm0
	movl	%eax, 8(%r13)
	movq	16(%rsi), %rax
	movq	%rdx, 0(%r13)
	movabsq	$-6148914691236517205, %rdx
	movq	%rax, 16(%r13)
	movq	32(%rsi), %rbx
	subq	24(%rsi), %rbx
	movq	$0, 40(%r13)
	movq	%rbx, %rax
	movups	%xmm0, 24(%r13)
	sarq	$2, %rax
	imulq	%rdx, %rax
	testq	%rax, %rax
	je	.L451
	movabsq	$768614336404564650, %rdx
	cmpq	%rdx, %rax
	ja	.L459
	movq	%rbx, %rdi
	call	_Znwm@PLT
.L451:
	movq	%rax, %xmm0
	addq	%rax, %rbx
	punpcklqdq	%xmm0, %xmm0
	movq	%rbx, 40(%r13)
	movups	%xmm0, 24(%r13)
	movq	32(%r12), %rdx
	movq	24(%r12), %rdi
	cmpq	%rdi, %rdx
	je	.L453
	movq	%rdi, %rcx
	movq	%rax, %rsi
	.p2align 4,,10
	.p2align 3
.L454:
	movq	(%rcx), %r8
	addq	$12, %rcx
	addq	$12, %rsi
	movq	%r8, -12(%rsi)
	movl	-4(%rcx), %r8d
	movl	%r8d, -4(%rsi)
	cmpq	%rcx, %rdx
	jne	.L454
	subq	$12, %rdx
	subq	%rdi, %rdx
	shrq	$2, %rdx
	leaq	12(%rax,%rdx,4), %rax
.L453:
	movq	%rax, 32(%r13)
	movzbl	48(%r12), %eax
	movb	%al, 48(%r13)
	popq	%rbx
	addq	$56, 8(%r14)
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L449:
	.cfi_restore_state
	popq	%rbx
	movq	%rsi, %rdx
	popq	%r12
	movq	%r13, %rsi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNSt6vectorIN2v88internal16CoverageFunctionESaIS2_EE17_M_realloc_insertIJRS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
.L459:
	.cfi_restore_state
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE22255:
	.size	_ZNSt6vectorIN2v88internal16CoverageFunctionESaIS2_EE12emplace_backIJRS2_EEEvDpOT_, .-_ZNSt6vectorIN2v88internal16CoverageFunctionESaIS2_EE12emplace_backIJRS2_EEEvDpOT_
	.section	.rodata._ZN2v84base19TemplateHashMapImplINS_8internal18SharedFunctionInfoEjNS0_18KeyEqualityMatcherINS2_6ObjectEEENS0_23DefaultAllocationPolicyEE6ResizeES7_.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"Out of memory: HashMap::Initialize"
	.section	.text._ZN2v84base19TemplateHashMapImplINS_8internal18SharedFunctionInfoEjNS0_18KeyEqualityMatcherINS2_6ObjectEEENS0_23DefaultAllocationPolicyEE6ResizeES7_,"axG",@progbits,_ZN2v84base19TemplateHashMapImplINS_8internal18SharedFunctionInfoEjNS0_18KeyEqualityMatcherINS2_6ObjectEEENS0_23DefaultAllocationPolicyEE6ResizeES7_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base19TemplateHashMapImplINS_8internal18SharedFunctionInfoEjNS0_18KeyEqualityMatcherINS2_6ObjectEEENS0_23DefaultAllocationPolicyEE6ResizeES7_
	.type	_ZN2v84base19TemplateHashMapImplINS_8internal18SharedFunctionInfoEjNS0_18KeyEqualityMatcherINS2_6ObjectEEENS0_23DefaultAllocationPolicyEE6ResizeES7_, @function
_ZN2v84base19TemplateHashMapImplINS_8internal18SharedFunctionInfoEjNS0_18KeyEqualityMatcherINS2_6ObjectEEENS0_23DefaultAllocationPolicyEE6ResizeES7_:
.LFB24354:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	8(%rdi), %eax
	movq	(%rdi), %r13
	movl	12(%rdi), %r15d
	addl	%eax, %eax
	leaq	(%rax,%rax,2), %rdi
	movq	%rax, %rbx
	salq	$3, %rdi
	call	malloc@PLT
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L485
	movl	%ebx, 8(%r12)
	testl	%ebx, %ebx
	je	.L462
	movb	$0, 16(%rax)
	movl	$24, %edx
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L463:
	movq	(%r12), %rcx
	addq	$1, %rax
	movb	$0, 16(%rcx,%rdx)
	movl	8(%r12), %ecx
	addq	$24, %rdx
	cmpq	%rax, %rcx
	ja	.L463
.L462:
	movl	$0, 12(%r12)
	movq	%r13, %r14
	testl	%r15d, %r15d
	je	.L469
.L464:
	cmpb	$0, 16(%r14)
	jne	.L486
.L465:
	addq	$24, %r14
	cmpb	$0, 16(%r14)
	je	.L465
.L486:
	movl	8(%r12), %eax
	movl	12(%r14), %ebx
	movq	(%r12), %rdi
	movq	(%r14), %rsi
	leal	-1(%rax), %ecx
	movl	%ebx, %eax
	andl	%ecx, %eax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rdi,%rdx,8), %rdx
	cmpb	$0, 16(%rdx)
	jne	.L467
	jmp	.L466
	.p2align 4,,10
	.p2align 3
.L487:
	addq	$1, %rax
	andq	%rcx, %rax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rdi,%rdx,8), %rdx
	cmpb	$0, 16(%rdx)
	je	.L466
.L467:
	cmpq	%rsi, (%rdx)
	jne	.L487
.L466:
	movl	8(%r14), %eax
	movq	%rsi, (%rdx)
	movl	%ebx, 12(%rdx)
	movl	%eax, 8(%rdx)
	movb	$1, 16(%rdx)
	movl	12(%r12), %eax
	addl	$1, %eax
	movl	%eax, %edx
	movl	%eax, 12(%r12)
	shrl	$2, %edx
	addl	%edx, %eax
	cmpl	8(%r12), %eax
	jnb	.L468
.L471:
	addq	$24, %r14
	subl	$1, %r15d
	jne	.L464
.L469:
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	free@PLT
	.p2align 4,,10
	.p2align 3
.L468:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v84base19TemplateHashMapImplINS_8internal18SharedFunctionInfoEjNS0_18KeyEqualityMatcherINS2_6ObjectEEENS0_23DefaultAllocationPolicyEE6ResizeES7_
	movl	8(%r12), %eax
	movq	(%r12), %rsi
	leal	-1(%rax), %ecx
	movl	%ebx, %eax
	andl	%ecx, %eax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rsi,%rdx,8), %rdx
	cmpb	$0, 16(%rdx)
	je	.L471
	movq	(%r14), %rdi
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L488:
	addq	$1, %rax
	andq	%rcx, %rax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rsi,%rdx,8), %rdx
	cmpb	$0, 16(%rdx)
	je	.L471
.L472:
	cmpq	(%rdx), %rdi
	jne	.L488
	jmp	.L471
.L485:
	leaq	.LC4(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE24354:
	.size	_ZN2v84base19TemplateHashMapImplINS_8internal18SharedFunctionInfoEjNS0_18KeyEqualityMatcherINS2_6ObjectEEENS0_23DefaultAllocationPolicyEE6ResizeES7_, .-_ZN2v84base19TemplateHashMapImplINS_8internal18SharedFunctionInfoEjNS0_18KeyEqualityMatcherINS2_6ObjectEEENS0_23DefaultAllocationPolicyEE6ResizeES7_
	.section	.text._ZN2v88internal12_GLOBAL__N_126CollectAndMaybeResetCountsEPNS0_7IsolateEPNS0_18SharedToCounterMapENS_5debug12CoverageModeE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_126CollectAndMaybeResetCountsEPNS0_7IsolateEPNS0_18SharedToCounterMapENS_5debug12CoverageModeE, @function
_ZN2v88internal12_GLOBAL__N_126CollectAndMaybeResetCountsEPNS0_7IsolateEPNS0_18SharedToCounterMapENS_5debug12CoverageModeE:
.LFB19924:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$1528, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	41832(%rdi), %eax
	testl	%eax, %eax
	je	.L490
	subl	$1, %eax
	cmpl	$3, %eax
	ja	.L489
	movq	4720(%rdi), %rax
	movl	11(%rax), %edi
	testl	%edi, %edi
	jne	.L593
	.p2align 4,,10
	.p2align 3
.L489:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L594
	addq	$1528, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L593:
	.cfi_restore_state
	xorl	%r12d, %r12d
.L500:
	movq	15(%rax), %rcx
	sarq	$32, %rcx
	cmpl	%r12d, %ecx
	jle	.L489
	movq	23(%rax,%r12,8), %rax
	movq	7(%rax), %rbx
	movl	35(%rax), %r15d
	testl	%edx, %edx
	je	.L493
	movl	$0, 35(%rax)
.L493:
	movl	8(%r14), %eax
	movq	(%r14), %rdi
	leal	-1(%rax), %esi
	movl	%esi, %eax
	andl	%ebx, %eax
	leaq	(%rax,%rax,2), %rcx
	leaq	(%rdi,%rcx,8), %rcx
	cmpb	$0, 16(%rcx)
	jne	.L496
	jmp	.L494
	.p2align 4,,10
	.p2align 3
.L595:
	addq	$1, %rax
	andq	%rsi, %rax
	leaq	(%rax,%rax,2), %rcx
	leaq	(%rdi,%rcx,8), %rcx
	cmpb	$0, 16(%rcx)
	je	.L494
.L496:
	cmpq	(%rcx), %rbx
	jne	.L595
.L495:
	movl	%r15d, %esi
	movl	8(%rcx), %eax
	notl	%esi
	cmpl	%esi, %eax
	ja	.L498
.L597:
	addl	%eax, %r15d
	addq	$1, %r12
	movl	%r15d, 8(%rcx)
	movq	4720(%r13), %rax
	movl	11(%rax), %esi
	testl	%esi, %esi
	je	.L489
	jmp	.L500
	.p2align 4,,10
	.p2align 3
.L494:
	movq	%rbx, (%rcx)
	movl	$0, 8(%rcx)
	movl	%ebx, 12(%rcx)
	movb	$1, 16(%rcx)
	movl	12(%r14), %eax
	addl	$1, %eax
	movl	%eax, %esi
	movl	%eax, 12(%r14)
	shrl	$2, %esi
	addl	%esi, %eax
	cmpl	8(%r14), %eax
	jb	.L495
	movq	%r14, %rdi
	movl	%edx, -1560(%rbp)
	call	_ZN2v84base19TemplateHashMapImplINS_8internal18SharedFunctionInfoEjNS0_18KeyEqualityMatcherINS2_6ObjectEEENS0_23DefaultAllocationPolicyEE6ResizeES7_
	movl	8(%r14), %eax
	movq	(%r14), %rdi
	movl	-1560(%rbp), %edx
	leal	-1(%rax), %esi
	movl	%esi, %eax
	andl	%ebx, %eax
	leaq	(%rax,%rax,2), %rcx
	leaq	(%rdi,%rcx,8), %rcx
	cmpb	$0, 16(%rcx)
	jne	.L497
	jmp	.L495
	.p2align 4,,10
	.p2align 3
.L596:
	addq	$1, %rax
	andq	%rsi, %rax
	leaq	(%rax,%rax,2), %rcx
	leaq	(%rdi,%rcx,8), %rcx
	cmpb	$0, 16(%rcx)
	je	.L495
.L497:
	cmpq	(%rcx), %rbx
	jne	.L596
	movl	%r15d, %esi
	movl	8(%rcx), %eax
	notl	%esi
	cmpl	%esi, %eax
	jbe	.L597
	.p2align 4,,10
	.p2align 3
.L498:
	movl	$-1, 8(%rcx)
	movq	4720(%r13), %rax
	addq	$1, %r12
	movl	11(%rax), %ecx
	testl	%ecx, %ecx
	je	.L489
	jmp	.L500
	.p2align 4,,10
	.p2align 3
.L490:
	leaq	-1552(%rbp), %r12
	leaq	37592(%rdi), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	leaq	-1504(%rbp), %rbx
	call	_ZN2v88internal18HeapObjectIteratorC1EPNS0_4HeapENS1_20HeapObjectsFilteringE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal18HeapObjectIterator4NextEv@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L501
	jmp	.L535
	.p2align 4,,10
	.p2align 3
.L534:
	movq	%r12, %rdi
	call	_ZN2v88internal18HeapObjectIterator4NextEv@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L535
.L501:
	movq	-1(%r15), %rax
	cmpw	$1105, 11(%rax)
	jne	.L534
	movq	23(%r15), %rcx
	leaq	23(%r15), %rsi
	movq	31(%rcx), %rax
	testb	$1, %al
	jne	.L598
.L505:
	movq	%rbx, %rdi
	movq	%rcx, -1568(%rbp)
	movq	%rsi, -1560(%rbp)
	movq	%rax, -1504(%rbp)
	call	_ZN2v88internal6Script16IsUserJavaScriptEv@PLT
	testb	%al, %al
	je	.L534
	movq	-1568(%rbp), %rcx
	movq	7(%rcx), %rax
	testb	$1, %al
	movq	-1560(%rbp), %rsi
	jne	.L508
.L511:
	movabsq	$287762808832, %rdi
	movq	(%rsi), %rax
	movq	7(%rax), %rax
	cmpq	%rdi, %rax
	je	.L514
	testb	$1, %al
	jne	.L512
.L515:
	movq	39(%r15), %rax
	leaq	39(%r15), %rdi
	movq	7(%rax), %rax
	movq	-1(%rax), %rax
	cmpw	$155, 11(%rax)
	je	.L516
.L514:
	movabsq	$287762808832, %rdi
	movq	(%rsi), %rax
	movq	7(%rax), %rax
	cmpq	%rdi, %rax
	je	.L534
	testb	$1, %al
	jne	.L519
.L522:
	movq	39(%r15), %rax
	leaq	39(%r15), %rdi
	movq	7(%rax), %rax
	movq	-1(%rax), %rax
	cmpw	$125, 11(%rax)
	jne	.L534
.L516:
	movabsq	$287762808832, %rdx
	movq	(%rsi), %rax
	movq	7(%rax), %rax
	cmpq	%rdx, %rax
	je	.L590
	testb	$1, %al
	jne	.L524
.L527:
	movq	(%rdi), %rax
	movq	7(%rax), %rdx
	movq	-1(%rdx), %rsi
	cmpw	$155, 11(%rsi)
	je	.L599
.L526:
	xorl	%r15d, %r15d
	movl	15(%rax), %eax
	cmpl	%eax, _ZN2v88internal42FLAG_budget_for_feedback_vector_allocationE(%rip)
	setg	%r15b
.L528:
	movl	8(%r14), %eax
	movq	(%r14), %rdi
	leal	-1(%rax), %esi
	movl	%esi, %eax
	andl	%ecx, %eax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rdi,%rdx,8), %rdx
	cmpb	$0, 16(%rdx)
	jne	.L531
	jmp	.L529
	.p2align 4,,10
	.p2align 3
.L600:
	addq	$1, %rax
	andq	%rsi, %rax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rdi,%rdx,8), %rdx
	cmpb	$0, 16(%rdx)
	je	.L529
.L531:
	cmpq	(%rdx), %rcx
	jne	.L600
.L530:
	movl	8(%rdx), %ecx
	movl	%r15d, %esi
	movq	%r12, %rdi
	notl	%esi
	leal	(%r15,%rcx), %eax
	cmpl	%esi, %ecx
	movl	$-1, %ecx
	cmova	%ecx, %eax
	movl	%eax, 8(%rdx)
	call	_ZN2v88internal18HeapObjectIterator4NextEv@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L501
	.p2align 4,,10
	.p2align 3
.L535:
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateE@PLT
	cmpq	$0, -88(%rbp)
	je	.L503
	.p2align 4,,10
	.p2align 3
.L502:
	movq	%rbx, %rdi
	call	_ZN2v88internal23JavaScriptFrameIterator7AdvanceEv@PLT
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L503
	movq	(%rdi), %rax
	call	*152(%rax)
	movq	23(%rax), %r13
	movl	8(%r14), %eax
	movq	(%r14), %rdi
	leal	-1(%rax), %r8d
	movl	%r8d, %edx
	andl	%r13d, %edx
	leaq	(%rdx,%rdx,2), %rax
	leaq	(%rdi,%rax,8), %rcx
	cmpb	$0, 16(%rcx)
	je	.L550
	movq	%rcx, %rsi
	movq	%rdx, %rax
	movl	%r8d, %r9d
	jmp	.L538
	.p2align 4,,10
	.p2align 3
.L601:
	addq	$1, %rax
	andq	%r9, %rax
	leaq	(%rax,%rax,2), %rsi
	leaq	(%rdi,%rsi,8), %rsi
	cmpb	$0, 16(%rsi)
	je	.L548
.L538:
	cmpq	%r13, (%rsi)
	jne	.L601
	movl	8(%rsi), %eax
	testl	%eax, %eax
	jne	.L502
.L548:
	movl	%r8d, %eax
	jmp	.L540
	.p2align 4,,10
	.p2align 3
.L602:
	addq	$1, %rdx
	andq	%rax, %rdx
	leaq	(%rdx,%rdx,2), %rcx
	leaq	(%rdi,%rcx,8), %rcx
	cmpb	$0, 16(%rcx)
	je	.L550
.L540:
	cmpq	(%rcx), %r13
	jne	.L602
.L539:
	movl	8(%rcx), %eax
	xorl	%edx, %edx
	cmpl	$-1, %eax
	setne	%dl
	addl	%edx, %eax
	movl	%eax, 8(%rcx)
	jmp	.L502
	.p2align 4,,10
	.p2align 3
.L503:
	movq	%r12, %rdi
	call	_ZN2v88internal18HeapObjectIteratorD1Ev@PLT
	jmp	.L489
	.p2align 4,,10
	.p2align 3
.L550:
	movq	%r13, (%rcx)
	movl	$0, 8(%rcx)
	movl	%r13d, 12(%rcx)
	movb	$1, 16(%rcx)
	movl	12(%r14), %eax
	addl	$1, %eax
	movl	%eax, %edx
	movl	%eax, 12(%r14)
	shrl	$2, %edx
	addl	%edx, %eax
	cmpl	8(%r14), %eax
	jb	.L539
	movq	%r14, %rdi
	call	_ZN2v84base19TemplateHashMapImplINS_8internal18SharedFunctionInfoEjNS0_18KeyEqualityMatcherINS2_6ObjectEEENS0_23DefaultAllocationPolicyEE6ResizeES7_
	movl	8(%r14), %eax
	movq	(%r14), %rdi
	leal	-1(%rax), %esi
	movl	%esi, %eax
	andl	%r13d, %eax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rdi,%rdx,8), %rcx
	cmpb	$0, 16(%rcx)
	jne	.L542
	jmp	.L539
	.p2align 4,,10
	.p2align 3
.L603:
	addq	$1, %rax
	andq	%rsi, %rax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rdi,%rdx,8), %rcx
	cmpb	$0, 16(%rcx)
	je	.L539
.L542:
	cmpq	(%rcx), %r13
	jne	.L603
	movl	8(%rcx), %eax
	xorl	%edx, %edx
	cmpl	$-1, %eax
	setne	%dl
	addl	%edx, %eax
	movl	%eax, 8(%rcx)
	jmp	.L502
	.p2align 4,,10
	.p2align 3
.L598:
	movq	-1(%rax), %rdi
	cmpw	$86, 11(%rdi)
	je	.L604
.L506:
	movq	%rax, %rdi
	andq	$-262144, %rdi
	movq	24(%rdi), %rdi
	cmpq	%rax, -37504(%rdi)
	je	.L534
	jmp	.L505
.L604:
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L506
	jmp	.L505
	.p2align 4,,10
	.p2align 3
.L524:
	movq	-1(%rax), %rdx
	cmpw	$165, 11(%rdx)
	jne	.L588
.L590:
	movq	(%rdi), %rax
	jmp	.L526
.L529:
	movq	%rcx, (%rdx)
	movl	$0, 8(%rdx)
	movl	%ecx, 12(%rdx)
	movb	$1, 16(%rdx)
	movl	12(%r14), %eax
	addl	$1, %eax
	movl	%eax, %esi
	movl	%eax, 12(%r14)
	shrl	$2, %esi
	addl	%esi, %eax
	cmpl	8(%r14), %eax
	jb	.L530
	movq	%r14, %rdi
	movq	%rcx, -1560(%rbp)
	call	_ZN2v84base19TemplateHashMapImplINS_8internal18SharedFunctionInfoEjNS0_18KeyEqualityMatcherINS2_6ObjectEEENS0_23DefaultAllocationPolicyEE6ResizeES7_
	movl	8(%r14), %eax
	movq	-1560(%rbp), %rcx
	movq	(%r14), %rdi
	leal	-1(%rax), %esi
	movl	%esi, %eax
	andl	%ecx, %eax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rdi,%rdx,8), %rdx
	cmpb	$0, 16(%rdx)
	jne	.L532
	jmp	.L530
	.p2align 4,,10
	.p2align 3
.L605:
	addq	$1, %rax
	andq	%rsi, %rax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rdi,%rdx,8), %rdx
	cmpb	$0, 16(%rdx)
	je	.L530
.L532:
	cmpq	(%rdx), %rcx
	jne	.L605
	jmp	.L530
.L599:
	movl	35(%rdx), %r15d
	jmp	.L528
.L512:
	movq	-1(%rax), %rdi
	cmpw	$165, 11(%rdi)
	je	.L514
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L515
	jmp	.L514
.L508:
	movq	-1(%rax), %rax
	cmpw	$83, 11(%rax)
	jne	.L511
	jmp	.L534
.L519:
	movq	-1(%rax), %rdi
	cmpw	$165, 11(%rdi)
	je	.L534
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L522
	jmp	.L534
.L588:
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L527
	jmp	.L590
.L594:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19924:
	.size	_ZN2v88internal12_GLOBAL__N_126CollectAndMaybeResetCountsEPNS0_7IsolateEPNS0_18SharedToCounterMapENS_5debug12CoverageModeE, .-_ZN2v88internal12_GLOBAL__N_126CollectAndMaybeResetCountsEPNS0_7IsolateEPNS0_18SharedToCounterMapENS_5debug12CoverageModeE
	.section	.text._ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_,"axG",@progbits,_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	.type	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_, @function
_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_:
.LFB24474:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L620
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L616
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L621
.L608:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L615:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L622
	testq	%r13, %r13
	jg	.L611
	testq	%r9, %r9
	jne	.L614
.L612:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L622:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L611
.L614:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L621:
	testq	%rsi, %rsi
	jne	.L609
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L615
	.p2align 4,,10
	.p2align 3
.L611:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L612
	jmp	.L614
	.p2align 4,,10
	.p2align 3
.L616:
	movl	$8, %r14d
	jmp	.L608
.L620:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L609:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L608
	.cfi_endproc
.LFE24474:
	.size	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_, .-_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	.section	.text._ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPN2v88internal13CoverageBlockESt6vectorIS4_SaIS4_EEEElS4_NS0_5__ops15_Iter_comp_iterIPFbRKS4_SD_EEEEvT_T0_SI_T1_T2_,"axG",@progbits,_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPN2v88internal13CoverageBlockESt6vectorIS4_SaIS4_EEEElS4_NS0_5__ops15_Iter_comp_iterIPFbRKS4_SD_EEEEvT_T0_SI_T1_T2_,comdat
	.p2align 4
	.weak	_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPN2v88internal13CoverageBlockESt6vectorIS4_SaIS4_EEEElS4_NS0_5__ops15_Iter_comp_iterIPFbRKS4_SD_EEEEvT_T0_SI_T1_T2_
	.type	_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPN2v88internal13CoverageBlockESt6vectorIS4_SaIS4_EEEElS4_NS0_5__ops15_Iter_comp_iterIPFbRKS4_SD_EEEEvT_T0_SI_T1_T2_, @function
_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPN2v88internal13CoverageBlockESt6vectorIS4_SaIS4_EEEElS4_NS0_5__ops15_Iter_comp_iterIPFbRKS4_SD_EEEEvT_T0_SI_T1_T2_:
.LFB25423:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%rdx, %rdi
	andl	$1, %edi
	subq	$104, %rsp
	movq	%rsi, -120(%rbp)
	movq	%rdx, -144(%rbp)
	movq	%r9, -104(%rbp)
	movq	%rcx, -96(%rbp)
	movl	%r8d, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-1(%rdx), %rax
	movq	%rdi, -136(%rbp)
	movq	%rax, %r13
	shrq	$63, %r13
	addq	%rax, %r13
	movq	%r13, %rax
	sarq	%rax
	movq	%rax, -128(%rbp)
	cmpq	%rax, %rsi
	jge	.L624
	movq	%rsi, %r8
	.p2align 4,,10
	.p2align 3
.L628:
	leaq	1(%r8), %rax
	movq	%r8, -112(%rbp)
	leaq	(%rax,%rax), %r13
	leaq	-1(%r13), %r15
	leaq	0(%r13,%rax,4), %rax
	leaq	(%r15,%r15,2), %rsi
	leaq	(%rbx,%rax,4), %r12
	movq	-104(%rbp), %rax
	leaq	(%rbx,%rsi,4), %r14
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	*%rax
	movq	-112(%rbp), %r8
	testb	%al, %al
	leaq	(%r8,%r8,2), %rax
	leaq	(%rbx,%rax,4), %rax
	jne	.L625
	movq	(%r12), %rdx
	movq	%rdx, (%rax)
	movl	8(%r12), %edx
	movl	%edx, 8(%rax)
	cmpq	-128(%rbp), %r13
	jge	.L626
	movq	%r13, %r8
	jmp	.L628
	.p2align 4,,10
	.p2align 3
.L625:
	movq	(%r14), %rcx
	movq	%rcx, (%rax)
	movl	8(%r14), %ecx
	movl	%ecx, 8(%rax)
	cmpq	-128(%rbp), %r15
	jge	.L635
	movq	%r15, %r8
	jmp	.L628
	.p2align 4,,10
	.p2align 3
.L635:
	movq	%r14, %r12
	movq	%r15, %r13
.L626:
	cmpq	$0, -136(%rbp)
	je	.L632
.L629:
	movq	-96(%rbp), %rax
	movq	%rax, -68(%rbp)
	movl	-88(%rbp), %eax
	movl	%eax, -60(%rbp)
	leaq	-1(%r13), %rax
	movq	%rax, %r14
	shrq	$63, %r14
	addq	%rax, %r14
	sarq	%r14
	cmpq	-120(%rbp), %r13
	jle	.L630
	leaq	-68(%rbp), %rax
	movq	%rax, -112(%rbp)
	jmp	.L631
	.p2align 4,,10
	.p2align 3
.L645:
	movq	(%r15), %rax
	leaq	-1(%r14), %rdx
	movq	%r14, %r13
	movq	%rax, (%r12)
	movl	8(%r15), %eax
	movl	%eax, 8(%r12)
	movq	%rdx, %rax
	shrq	$63, %rax
	addq	%rdx, %rax
	sarq	%rax
	cmpq	%r14, -120(%rbp)
	jge	.L644
	movq	%rax, %r14
.L631:
	leaq	(%r14,%r14,2), %rax
	movq	-112(%rbp), %rsi
	leaq	(%rbx,%rax,4), %r15
	movq	-104(%rbp), %rax
	movq	%r15, %rdi
	call	*%rax
	leaq	0(%r13,%r13,2), %rdx
	leaq	(%rbx,%rdx,4), %r12
	testb	%al, %al
	jne	.L645
.L630:
	movq	-68(%rbp), %rax
	movq	%rax, (%r12)
	movl	-60(%rbp), %eax
	movl	%eax, 8(%r12)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L646
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L624:
	.cfi_restore_state
	leaq	(%rsi,%rsi,2), %rax
	cmpq	$0, -136(%rbp)
	leaq	(%rbx,%rax,4), %r12
	jne	.L647
	movq	-120(%rbp), %r13
	.p2align 4,,10
	.p2align 3
.L632:
	movq	-144(%rbp), %rdx
	subq	$2, %rdx
	movq	%rdx, %r14
	shrq	$63, %r14
	addq	%rdx, %r14
	sarq	%r14
	cmpq	%r13, %r14
	jne	.L629
	leaq	1(%r13,%r13), %r13
	leaq	0(%r13,%r13,2), %rax
	leaq	(%rbx,%rax,4), %rax
	movq	(%rax), %rdx
	movq	%rdx, (%r12)
	movl	8(%rax), %edx
	movl	%edx, 8(%r12)
	movq	%rax, %r12
	jmp	.L629
	.p2align 4,,10
	.p2align 3
.L644:
	movq	%r15, %r12
	jmp	.L630
.L647:
	movq	%rcx, -68(%rbp)
	movl	%r8d, -60(%rbp)
	jmp	.L630
.L646:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25423:
	.size	_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPN2v88internal13CoverageBlockESt6vectorIS4_SaIS4_EEEElS4_NS0_5__ops15_Iter_comp_iterIPFbRKS4_SD_EEEEvT_T0_SI_T1_T2_, .-_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPN2v88internal13CoverageBlockESt6vectorIS4_SaIS4_EEEElS4_NS0_5__ops15_Iter_comp_iterIPFbRKS4_SD_EEEEvT_T0_SI_T1_T2_
	.section	.text._ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPN2v88internal13CoverageBlockESt6vectorIS4_SaIS4_EEEElNS0_5__ops15_Iter_comp_iterIPFbRKS4_SD_EEEEvT_SH_T0_T1_,"axG",@progbits,_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPN2v88internal13CoverageBlockESt6vectorIS4_SaIS4_EEEElNS0_5__ops15_Iter_comp_iterIPFbRKS4_SD_EEEEvT_SH_T0_T1_,comdat
	.p2align 4
	.weak	_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPN2v88internal13CoverageBlockESt6vectorIS4_SaIS4_EEEElNS0_5__ops15_Iter_comp_iterIPFbRKS4_SD_EEEEvT_SH_T0_T1_
	.type	_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPN2v88internal13CoverageBlockESt6vectorIS4_SaIS4_EEEElNS0_5__ops15_Iter_comp_iterIPFbRKS4_SD_EEEEvT_SH_T0_T1_, @function
_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPN2v88internal13CoverageBlockESt6vectorIS4_SaIS4_EEEElNS0_5__ops15_Iter_comp_iterIPFbRKS4_SD_EEEEvT_SH_T0_T1_:
.LFB24357:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	subq	%rdi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -72(%rbp)
	cmpq	$192, %rax
	jle	.L648
	movq	%rdi, %r15
	movq	%rcx, %r12
	testq	%rdx, %rdx
	je	.L678
	leaq	12(%rdi), %rax
	movq	%rsi, %r13
	movq	%rax, -80(%rbp)
.L652:
	movq	%r13, %rax
	subq	$1, -72(%rbp)
	movq	-80(%rbp), %rdi
	leaq	-12(%r13), %rbx
	movabsq	$-6148914691236517205, %rdx
	subq	%r15, %rax
	sarq	$2, %rax
	imulq	%rax, %rdx
	movq	%rdx, %rax
	shrq	$63, %rax
	addq	%rdx, %rax
	movq	%rax, %rdx
	andq	$-2, %rax
	sarq	%rdx
	addq	%rdx, %rax
	leaq	(%r15,%rax,4), %r14
	movq	%r14, %rsi
	call	*%r12
	movq	%rbx, %rsi
	testb	%al, %al
	je	.L657
	movq	%r14, %rdi
	call	*%r12
	testb	%al, %al
	jne	.L662
	movq	-80(%rbp), %rdi
	movq	%rbx, %rsi
	call	*%r12
	testb	%al, %al
	jne	.L676
.L677:
	movq	12(%r15), %rcx
	movl	8(%r15), %eax
	movq	(%r15), %rdx
	movq	%rcx, (%r15)
	movl	20(%r15), %ecx
	movq	%rdx, 12(%r15)
	movl	%ecx, 8(%r15)
	movl	%eax, 20(%r15)
.L659:
	movq	-80(%rbp), %rbx
	movq	%r13, %r14
	.p2align 4,,10
	.p2align 3
.L663:
	movq	%rbx, -96(%rbp)
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	*%r12
	testb	%al, %al
	jne	.L664
	leaq	-12(%r14), %rcx
	.p2align 4,,10
	.p2align 3
.L665:
	movq	%rcx, -88(%rbp)
	movq	%rcx, %r14
	movq	%rcx, %rsi
	movq	%r15, %rdi
	call	*%r12
	movq	-88(%rbp), %rcx
	subq	$12, %rcx
	testb	%al, %al
	jne	.L665
	cmpq	%r14, %rbx
	jnb	.L679
	movq	(%r14), %rdi
	movl	(%rbx), %esi
	movl	4(%rbx), %ecx
	movl	8(%rbx), %eax
	movq	%rdi, (%rbx)
	movl	8(%r14), %edi
	movl	%edi, 8(%rbx)
	movl	%esi, (%r14)
	movl	%ecx, 4(%r14)
	movl	%eax, 8(%r14)
.L664:
	addq	$12, %rbx
	jmp	.L663
	.p2align 4,,10
	.p2align 3
.L657:
	movq	-80(%rbp), %rdi
	call	*%r12
	testb	%al, %al
	jne	.L677
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	*%r12
	testb	%al, %al
	je	.L662
.L676:
	movq	-12(%r13), %rsi
	movl	(%r15), %ecx
	movl	4(%r15), %edx
	movl	8(%r15), %eax
	movq	%rsi, (%r15)
	movl	-4(%r13), %esi
	movl	%esi, 8(%r15)
	movl	%ecx, -12(%r13)
	movl	%edx, -8(%r13)
	movl	%eax, -4(%r13)
	jmp	.L659
	.p2align 4,,10
	.p2align 3
.L679:
	movq	-72(%rbp), %rdx
	movq	%r12, %rcx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPN2v88internal13CoverageBlockESt6vectorIS4_SaIS4_EEEElNS0_5__ops15_Iter_comp_iterIPFbRKS4_SD_EEEEvT_SH_T0_T1_
	movq	%rbx, %rax
	subq	%r15, %rax
	cmpq	$192, %rax
	jle	.L648
	cmpq	$0, -72(%rbp)
	je	.L650
	movq	%rbx, %r13
	jmp	.L652
	.p2align 4,,10
	.p2align 3
.L662:
	movq	(%r14), %rsi
	movl	8(%r15), %eax
	movq	(%r15), %rcx
	movq	%rsi, (%r15)
	movl	8(%r14), %esi
	movl	%esi, 8(%r15)
	movq	%rcx, (%r14)
	movl	%eax, 8(%r14)
	jmp	.L659
	.p2align 4,,10
	.p2align 3
.L678:
	movq	%rsi, -96(%rbp)
.L650:
	movabsq	$-6148914691236517205, %r14
	sarq	$2, %rax
	imulq	%rax, %r14
	leaq	-2(%r14), %r13
	sarq	%r13
	jmp	.L655
	.p2align 4,,10
	.p2align 3
.L653:
	subq	$1, %r13
.L655:
	leaq	0(%r13,%r13,2), %rax
	movq	%r12, %r9
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	(%r15,%rax,4), %rcx
	movl	8(%r15,%rax,4), %r8d
	movq	%r15, %rdi
	movq	%rcx, -60(%rbp)
	movl	%r8d, -52(%rbp)
	call	_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPN2v88internal13CoverageBlockESt6vectorIS4_SaIS4_EEEElS4_NS0_5__ops15_Iter_comp_iterIPFbRKS4_SD_EEEEvT_T0_SI_T1_T2_
	testq	%r13, %r13
	jne	.L653
	movabsq	$-6148914691236517205, %r13
	movq	-96(%rbp), %rbx
	subq	$12, %rbx
	.p2align 4,,10
	.p2align 3
.L654:
	movq	(%r15), %rax
	movq	%rbx, %r14
	movq	(%rbx), %rcx
	movq	%r12, %r9
	subq	%r15, %r14
	movl	8(%rbx), %r8d
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, (%rbx)
	movq	%r14, %rdx
	movl	8(%r15), %eax
	subq	$12, %rbx
	sarq	$2, %rdx
	movq	%rcx, -60(%rbp)
	movl	%eax, 20(%rbx)
	imulq	%r13, %rdx
	movl	%r8d, -52(%rbp)
	call	_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPN2v88internal13CoverageBlockESt6vectorIS4_SaIS4_EEEElS4_NS0_5__ops15_Iter_comp_iterIPFbRKS4_SD_EEEEvT_T0_SI_T1_T2_
	cmpq	$12, %r14
	jg	.L654
.L648:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24357:
	.size	_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPN2v88internal13CoverageBlockESt6vectorIS4_SaIS4_EEEElNS0_5__ops15_Iter_comp_iterIPFbRKS4_SD_EEEEvT_SH_T0_T1_, .-_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPN2v88internal13CoverageBlockESt6vectorIS4_SaIS4_EEEElNS0_5__ops15_Iter_comp_iterIPFbRKS4_SD_EEEEvT_SH_T0_T1_
	.section	.text._ZN2v88internal12_GLOBAL__N_128CollectBlockCoverageInternalEPNS0_16CoverageFunctionENS0_18SharedFunctionInfoENS_5debug12CoverageModeE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_128CollectBlockCoverageInternalEPNS0_16CoverageFunctionENS0_18SharedFunctionInfoENS_5debug12CoverageModeE, @function
_ZN2v88internal12_GLOBAL__N_128CollectBlockCoverageInternalEPNS0_16CoverageFunctionENS0_18SharedFunctionInfoENS_5debug12CoverageModeE:
.LFB19916:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%rdi), %eax
	cmpl	4(%rdi), %eax
	setl	%dl
	notl	%eax
	shrl	$31, %eax
	andb	%al, %dl
	movb	%dl, -153(%rbp)
	jne	.L1196
.L680:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1197
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1196:
	.cfi_restore_state
	movb	$1, 48(%rdi)
	movq	31(%rsi), %rax
	leaq	-120(%rbp), %r12
	movq	%rdi, %r15
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
	movq	63(%rax), %rax
	movq	%rax, -120(%rbp)
	call	_ZNK2v88internal12CoverageInfo9SlotCountEv@PLT
	movq	$0, -136(%rbp)
	testl	%eax, %eax
	je	.L683
	movq	%r14, -144(%rbp)
	xorl	%ebx, %ebx
	movq	%r12, %r13
	movq	%r15, -168(%rbp)
	jmp	.L693
	.p2align 4,,10
	.p2align 3
.L1198:
	movl	%r15d, (%r14)
	addq	$12, %r14
	movl	%r12d, -8(%r14)
	movl	%eax, -4(%r14)
.L686:
	addl	$1, %ebx
.L693:
	movq	%r13, %rdi
	call	_ZNK2v88internal12CoverageInfo9SlotCountEv@PLT
	cmpl	%ebx, %eax
	jle	.L684
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal12CoverageInfo19StartSourcePositionEi@PLT
	movl	%ebx, %esi
	movq	%r13, %rdi
	movl	%eax, %r15d
	call	_ZNK2v88internal12CoverageInfo17EndSourcePositionEi@PLT
	movl	%ebx, %esi
	movq	%r13, %rdi
	movl	%eax, %r12d
	call	_ZNK2v88internal12CoverageInfo10BlockCountEi@PLT
	movl	%eax, %ecx
	cmpq	%r14, -136(%rbp)
	jne	.L1198
	movq	-136(%rbp), %rsi
	subq	-144(%rbp), %rsi
	movabsq	$-6148914691236517205, %rdi
	movq	%rsi, %rax
	sarq	$2, %rax
	imulq	%rdi, %rax
	movabsq	$768614336404564650, %rdi
	cmpq	%rdi, %rax
	je	.L1199
	testq	%rax, %rax
	je	.L1026
	movabsq	$9223372036854775800, %rdi
	leaq	(%rax,%rax), %r8
	cmpq	%r8, %rax
	jbe	.L1200
.L688:
	movq	%rsi, -184(%rbp)
	movl	%ecx, -176(%rbp)
	movq	%rdi, -136(%rbp)
	call	_Znwm@PLT
	movq	-136(%rbp), %rdi
	movl	-176(%rbp), %ecx
	leaq	(%rax,%rdi), %rsi
	leaq	12(%rax), %rdi
	movq	%rsi, -136(%rbp)
	movq	-184(%rbp), %rsi
.L689:
	addq	%rax, %rsi
	movl	%ecx, 8(%rsi)
	movq	-144(%rbp), %rcx
	movl	%r15d, (%rsi)
	movl	%r12d, 4(%rsi)
	cmpq	%rcx, %r14
	je	.L1029
	movq	%rax, %rsi
	.p2align 4,,10
	.p2align 3
.L691:
	movq	(%rcx), %rdx
	addq	$12, %rcx
	addq	$12, %rsi
	movq	%rdx, -12(%rsi)
	movl	-4(%rcx), %edx
	movl	%edx, -4(%rsi)
	cmpq	%r14, %rcx
	jne	.L691
	movabsq	$3074457345618258603, %rsi
	leaq	-12(%r14), %rdx
	subq	-144(%rbp), %rdx
	shrq	$2, %rdx
	imulq	%rsi, %rdx
	movabsq	$4611686018427387903, %rsi
	andq	%rsi, %rdx
	leaq	6(%rdx,%rdx,2), %rdx
	leaq	(%rax,%rdx,4), %r14
.L690:
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L692
	movq	%rax, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %rax
.L692:
	movq	%rax, -144(%rbp)
	jmp	.L686
	.p2align 4,,10
	.p2align 3
.L1224:
	movq	%r15, %r13
	movq	-168(%rbp), %r15
	.p2align 4,,10
	.p2align 3
.L683:
	movq	24(%r15), %rdi
	movq	%r14, %xmm0
	movq	%r13, %xmm1
	movq	-136(%rbp), %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, 40(%r15)
	movups	%xmm0, 24(%r15)
	testq	%rdi, %rdi
	je	.L722
	call	_ZdlPv@PLT
	movq	24(%r15), %r14
	movq	32(%r15), %r13
.L722:
	cmpl	$4, -152(%rbp)
	je	.L1201
.L723:
	movq	%r13, %r9
	xorl	%eax, %eax
	movq	%r15, -112(%rbp)
	movabsq	$-6148914691236517205, %r12
	subq	%r14, %r9
	movq	$0, -104(%rbp)
	sarq	$2, %r9
	movq	$0, -96(%rbp)
	imulq	%r12, %r9
	movq	$0, -88(%rbp)
	movw	%ax, -80(%rbp)
	movl	$-1, -76(%rbp)
	movl	$0, -72(%rbp)
	testl	%r9d, %r9d
	jle	.L1202
	leaq	-104(%rbp), %rbx
	leaq	8(%r15), %rcx
	movq	%r15, %rsi
	leaq	4(%r15), %rdx
	movq	%rbx, %rdi
	call	_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE12emplace_backIJRiS6_RjEEEvDpOT_
	movl	-76(%rbp), %r11d
	movq	-112(%rbp), %rsi
	movb	$0, -79(%rbp)
	movq	-96(%rbp), %rcx
	movq	-104(%rbp), %r10
	leal	1(%r11), %eax
	movl	%eax, -76(%rbp)
	movslq	%eax, %rdx
	movq	24(%rsi), %r9
	leaq	(%rdx,%rdx,2), %rdx
	leaq	(%r9,%rdx,4), %rdi
	movq	%rcx, %rdx
	subq	%r10, %rdx
	sarq	$2, %rdx
	imulq	%r12, %rdx
	cmpq	$1, %rdx
	jbe	.L754
	subq	$12, %rcx
	jmp	.L756
	.p2align 4,,10
	.p2align 3
.L1203:
	subq	%r10, %rdx
	movq	%rcx, -96(%rbp)
	subq	$12, %rcx
	sarq	$2, %rdx
	imulq	%r12, %rdx
	cmpq	$1, %rdx
	jbe	.L754
.L756:
	movl	(%rdi), %r13d
	movq	%rcx, %rdx
	cmpl	%r13d, 4(%rcx)
	jle	.L1203
.L755:
	xorl	%ecx, %ecx
	cmpl	$-2, %r13d
	je	.L993
.L757:
	movq	32(%rsi), %rdx
	addl	$2, %r11d
	movabsq	$-6148914691236517205, %r12
	subq	%r9, %rdx
	sarq	$2, %rdx
	imulq	%r12, %rdx
	cmpl	%edx, %r11d
	jge	.L758
	testb	%cl, %cl
	jne	.L1204
	.p2align 4,,10
	.p2align 3
.L1150:
	movslq	-72(%rbp), %rdx
	testl	%eax, %eax
	js	.L765
	cmpl	%edx, %eax
	je	.L766
	cltq
	leaq	(%rdx,%rdx,2), %rdx
	leaq	(%rax,%rax,2), %rax
	leaq	(%r9,%rdx,4), %rdx
	leaq	(%r9,%rax,4), %rax
	movq	(%rax), %rcx
	movq	%rcx, (%rdx)
	movl	8(%rax), %eax
	movl	%eax, 8(%rdx)
	movl	-76(%rbp), %eax
	movq	-112(%rbp), %rsi
	movl	-72(%rbp), %edx
.L765:
	addl	$1, %edx
	movl	%edx, -72(%rbp)
	cmpl	$-1, %eax
	je	.L1019
.L767:
	cmpb	$0, -79(%rbp)
	jne	.L1184
	cltq
	movq	%rbx, %rdi
	leaq	(%rax,%rax,2), %rdx
	movq	24(%rsi), %rax
	leaq	(%rax,%rdx,4), %rsi
	call	_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE12emplace_backIJRS2_EEEvDpOT_
	movl	-76(%rbp), %eax
	movq	-112(%rbp), %rsi
.L1184:
	movq	-104(%rbp), %r10
.L768:
	leal	1(%rax), %edi
	movb	$0, -79(%rbp)
	movl	%edi, -76(%rbp)
	movslq	%edi, %rdx
	movq	24(%rsi), %r9
	leaq	(%rdx,%rdx,2), %rdx
	leaq	(%r9,%rdx,4), %r11
	movq	-96(%rbp), %rdx
	movq	%rdx, %rcx
	subq	%r10, %rcx
	sarq	$2, %rcx
	imulq	%r12, %rcx
	cmpq	$1, %rcx
	jbe	.L770
	subq	$12, %rdx
	jmp	.L771
	.p2align 4,,10
	.p2align 3
.L1205:
	subq	%r10, %rcx
	movq	%rdx, -96(%rbp)
	subq	$12, %rdx
	sarq	$2, %rcx
	imulq	%r12, %rcx
	cmpq	$1, %rcx
	jbe	.L770
.L771:
	movl	(%r11), %r14d
	movq	%rdx, %rcx
	cmpl	%r14d, 4(%rdx)
	jle	.L1205
.L770:
	movq	32(%rsi), %rdx
	addl	$2, %eax
	subq	%r9, %rdx
	sarq	$2, %rdx
	imulq	%r12, %rdx
	cmpl	%edx, %eax
	jge	.L760
	movl	%edi, %eax
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1202:
	movq	32(%r15), %rcx
	movq	24(%r15), %rdi
	movb	$1, -80(%rbp)
	movq	%r15, %rsi
	xorl	%edx, %edx
	movq	%rcx, %rax
	subq	%rdi, %rax
	sarq	$2, %rax
	imulq	%rax, %r12
	.p2align 4,,10
	.p2align 3
.L753:
	cmpq	%rdx, %r12
	jbe	.L772
	leaq	(%rdx,%rdx,2), %rax
	leaq	(%rdi,%rax,4), %rax
	cmpq	%rcx, %rax
	je	.L772
	movq	%rax, 32(%rsi)
.L772:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L773
	call	_ZdlPv@PLT
.L773:
	movq	32(%r15), %rdx
	movq	24(%r15), %rdi
	cmpq	%rdi, %rdx
	je	.L680
	movq	%rdx, %rax
	xorl	%r12d, %r12d
	movq	%r15, -112(%rbp)
	movabsq	$-6148914691236517205, %rbx
	subq	%rdi, %rax
	movq	$0, -104(%rbp)
	sarq	$2, %rax
	movq	$0, -96(%rbp)
	imulq	%rbx, %rax
	movq	$0, -88(%rbp)
	movw	%r12w, -80(%rbp)
	movl	$-1, -76(%rbp)
	movl	$0, -72(%rbp)
	testl	%eax, %eax
	jle	.L1206
	leaq	8(%r15), %rcx
	leaq	4(%r15), %rdx
	movq	%r15, %rsi
	leaq	-104(%rbp), %rdi
	call	_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE12emplace_backIJRiS6_RjEEEvDpOT_
	movl	-76(%rbp), %eax
	movq	-112(%rbp), %rsi
	movb	$0, -79(%rbp)
	movq	-96(%rbp), %rcx
	movq	-104(%rbp), %r10
	addl	$1, %eax
	movl	%eax, -76(%rbp)
	movslq	%eax, %rdx
	movq	24(%rsi), %rdi
	leaq	(%rdx,%rdx,2), %rdx
	leaq	(%rdi,%rdx,4), %r11
	movq	%rcx, %rdx
	subq	%r10, %rdx
	sarq	$2, %rdx
	imulq	%rbx, %rdx
	cmpq	$1, %rdx
	jbe	.L777
	movq	%rbx, %r9
	jmp	.L779
	.p2align 4,,10
	.p2align 3
.L778:
	subq	$12, %rcx
	movq	%rcx, %rdx
	movq	%rcx, -96(%rbp)
	subq	%r10, %rdx
	sarq	$2, %rdx
	imulq	%r9, %rdx
	cmpq	$1, %rdx
	jbe	.L777
.L779:
	movl	(%r11), %ebx
	cmpl	%ebx, -8(%rcx)
	jle	.L778
.L777:
	movq	32(%rsi), %rdx
.L776:
	movabsq	$-6148914691236517205, %rbx
	.p2align 4,,10
	.p2align 3
.L992:
	subq	%rdi, %rdx
	leal	1(%rax), %ecx
	sarq	$2, %rdx
	imulq	%rbx, %rdx
	cmpl	%edx, %ecx
	jge	.L1207
	cmpb	$0, -79(%rbp)
	jne	.L785
	movslq	-72(%rbp), %rdx
	testl	%eax, %eax
	js	.L786
	cmpl	%edx, %eax
	je	.L787
	cltq
	leaq	(%rdx,%rdx,2), %rdx
	leaq	(%rax,%rax,2), %rax
	leaq	(%rdi,%rdx,4), %rdx
	leaq	(%rdi,%rax,4), %rax
	movq	(%rax), %rcx
	movq	%rcx, (%rdx)
	movl	8(%rax), %eax
	movl	%eax, 8(%rdx)
	movl	-76(%rbp), %eax
	movq	-112(%rbp), %rsi
	movl	-72(%rbp), %edx
.L786:
	addl	$1, %edx
	movl	%edx, -72(%rbp)
	cmpl	$-1, %eax
	je	.L1017
.L788:
	movslq	%eax, %rdx
	addl	$1, %eax
	leaq	(%rdx,%rdx,2), %rdx
	salq	$2, %rdx
	cmpb	$0, -79(%rbp)
	jne	.L789
	addq	24(%rsi), %rdx
	leaq	-104(%rbp), %rdi
	movq	%rdx, %rsi
	call	_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE12emplace_backIJRS2_EEEvDpOT_
.L1185:
	movslq	-76(%rbp), %rdx
	movq	-112(%rbp), %rsi
	leal	1(%rdx), %eax
	leaq	(%rdx,%rdx,2), %rdx
	salq	$2, %rdx
.L789:
	movb	$0, -79(%rbp)
	movslq	%eax, %rcx
	movq	-104(%rbp), %r10
	movl	%eax, -76(%rbp)
	movq	24(%rsi), %rdi
	leaq	(%rcx,%rcx,2), %rcx
	leaq	(%rdi,%rcx,4), %r9
	movq	-96(%rbp), %rcx
	movq	%rcx, %r8
	subq	%r10, %r8
	sarq	$2, %r8
	imulq	%rbx, %r8
	cmpq	$1, %r8
	jbe	.L791
	subq	$12, %rcx
	jmp	.L793
	.p2align 4,,10
	.p2align 3
.L1208:
	subq	%r10, %r8
	movq	%rcx, -96(%rbp)
	subq	$12, %rcx
	sarq	$2, %r8
	imulq	%rbx, %r8
	cmpq	$1, %r8
	jbe	.L791
.L793:
	movl	(%r9), %r11d
	movq	%rcx, %r8
	cmpl	%r11d, 4(%rcx)
	jle	.L1208
.L792:
	cmpl	%r11d, (%rdi,%rdx)
	jne	.L1040
	cmpl	$-1, 4(%r9)
	je	.L990
.L1040:
	movq	32(%rsi), %rdx
	jmp	.L992
	.p2align 4,,10
	.p2align 3
.L1204:
	cmpl	$-1, %eax
	jne	.L768
.L1019:
	leaq	8(%rsi), %rcx
	leaq	4(%rsi), %rdx
	movq	%rbx, %rdi
	call	_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE12emplace_backIJRiS6_RjEEEvDpOT_
	movl	-76(%rbp), %eax
	movq	-112(%rbp), %rsi
	movq	-104(%rbp), %r10
	jmp	.L768
	.p2align 4,,10
	.p2align 3
.L791:
	movl	(%r9), %r11d
	jmp	.L792
	.p2align 4,,10
	.p2align 3
.L1207:
	cmpb	$0, -80(%rbp)
	jne	.L781
	cmpb	$0, -79(%rbp)
	jne	.L781
	movslq	-72(%rbp), %rdx
	testl	%eax, %eax
	js	.L782
	cmpl	%edx, %eax
	je	.L782
	cltq
	leaq	(%rdx,%rdx,2), %rdx
	leaq	(%rax,%rax,2), %rax
	leaq	(%rdi,%rdx,4), %rdx
	leaq	(%rdi,%rax,4), %rax
	movq	(%rax), %rcx
	movq	%rcx, (%rdx)
	movl	8(%rax), %eax
	movl	%eax, 8(%rdx)
	movl	-76(%rbp), %eax
	movl	-72(%rbp), %edx
	movq	-112(%rbp), %rsi
	leal	1(%rax), %ecx
.L782:
	addl	$1, %edx
	movl	%edx, -72(%rbp)
.L781:
	movabsq	$-6148914691236517205, %rbx
	movb	$1, -80(%rbp)
	movq	24(%rsi), %rdi
	movq	32(%rsi), %rdx
	subq	%rdi, %rdx
	sarq	$2, %rdx
	imulq	%rbx, %rdx
	cmpl	%edx, %ecx
	jge	.L1209
	cmpb	$0, -79(%rbp)
	movslq	%eax, %rdx
	jne	.L800
.L1151:
	movslq	-72(%rbp), %rcx
	testl	%eax, %eax
	js	.L801
	cmpl	%ecx, %eax
	je	.L802
	cltq
	leaq	(%rcx,%rcx,2), %rdx
	leaq	(%rax,%rax,2), %rax
	leaq	(%rdi,%rdx,4), %rdx
	leaq	(%rdi,%rax,4), %rax
	movq	(%rax), %rcx
	movq	%rcx, (%rdx)
	movl	8(%rax), %eax
	movl	%eax, 8(%rdx)
	movslq	-76(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movl	-72(%rbp), %ecx
.L801:
	addl	$1, %ecx
	movl	%ecx, -72(%rbp)
	cmpl	$-1, %edx
	je	.L1016
.L803:
	cmpb	$0, -79(%rbp)
	jne	.L804
	movq	24(%rsi), %rax
	leaq	(%rdx,%rdx,2), %rdx
	leaq	-104(%rbp), %rdi
	leaq	(%rax,%rdx,4), %rsi
	call	_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE12emplace_backIJRS2_EEEvDpOT_
	movl	-76(%rbp), %edx
	movq	-112(%rbp), %rsi
.L804:
	leal	1(%rdx), %eax
	movb	$0, -79(%rbp)
	movq	-104(%rbp), %r11
	movl	%eax, -76(%rbp)
	movslq	%eax, %rcx
	movq	24(%rsi), %rdi
	leaq	(%rcx,%rcx,2), %rcx
	leaq	(%rdi,%rcx,4), %r10
	movq	-96(%rbp), %rcx
	movq	%rcx, %r9
	subq	%r11, %r9
	sarq	$2, %r9
	imulq	%rbx, %r9
	cmpq	$1, %r9
	ja	.L806
	jmp	.L805
	.p2align 4,,10
	.p2align 3
.L1210:
	subq	$12, %rcx
	movq	%rcx, %r8
	movq	%rcx, -96(%rbp)
	subq	%r11, %r8
	sarq	$2, %r8
	imulq	%rbx, %r8
	cmpq	$1, %r8
	jbe	.L805
.L806:
	movl	(%r10), %r14d
	cmpl	%r14d, -8(%rcx)
	jle	.L1210
.L805:
	movq	32(%rsi), %rcx
	addl	$2, %edx
	subq	%rdi, %rcx
	sarq	$2, %rcx
	imulq	%rbx, %rcx
	cmpl	%ecx, %edx
	jge	.L795
	movslq	%eax, %rdx
	jmp	.L1151
	.p2align 4,,10
	.p2align 3
.L800:
	cmpl	$-1, %edx
	jne	.L804
.L1016:
	leaq	4(%rsi), %rdx
	leaq	8(%rsi), %rcx
	leaq	-104(%rbp), %rdi
	call	_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE12emplace_backIJRiS6_RjEEEvDpOT_
	movl	-76(%rbp), %edx
	movq	-112(%rbp), %rsi
	jmp	.L804
	.p2align 4,,10
	.p2align 3
.L785:
	cmpl	$-1, %eax
	je	.L1017
	cltq
	leaq	(%rax,%rax,2), %rdx
	movl	%ecx, %eax
	salq	$2, %rdx
	jmp	.L789
	.p2align 4,,10
	.p2align 3
.L760:
	cmpb	$0, -80(%rbp)
	movslq	-72(%rbp), %rdx
	je	.L1018
.L761:
	movb	$1, -80(%rbp)
	movq	32(%rsi), %rcx
	movslq	%edx, %rdx
	movabsq	$-6148914691236517205, %r9
	movq	24(%rsi), %rdi
	movq	%rcx, %rax
	subq	%rdi, %rax
	sarq	$2, %rax
	imulq	%r9, %rax
	movq	%rax, %r12
	cmpq	%rax, %rdx
	jbe	.L753
	subq	%rax, %rdx
	leaq	24(%rsi), %rdi
	movq	%rdx, %rsi
	call	_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE17_M_default_appendEm
	jmp	.L772
	.p2align 4,,10
	.p2align 3
.L1017:
	leaq	8(%rsi), %rcx
	leaq	4(%rsi), %rdx
	leaq	-104(%rbp), %rdi
	call	_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE12emplace_backIJRiS6_RjEEEvDpOT_
	jmp	.L1185
	.p2align 4,,10
	.p2align 3
.L1209:
	movl	-72(%rbp), %edx
.L784:
	movb	$1, -80(%rbp)
	movq	32(%rsi), %rcx
	movslq	%edx, %rdx
	movabsq	$-6148914691236517205, %r9
	movq	24(%rsi), %rdi
	movq	%rcx, %rax
	subq	%rdi, %rax
	sarq	$2, %rax
	imulq	%r9, %rax
	cmpq	%rax, %rdx
	ja	.L1211
	jnb	.L807
	leaq	(%rdx,%rdx,2), %rax
	leaq	(%rdi,%rax,4), %rax
	cmpq	%rax, %rcx
	je	.L807
	movq	%rax, 32(%rsi)
.L807:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L808
	call	_ZdlPv@PLT
.L808:
	movq	24(%r15), %rcx
	movq	32(%r15), %rax
	xorl	%ebx, %ebx
	movq	%r15, -112(%rbp)
	movw	%bx, -80(%rbp)
	movabsq	$-6148914691236517205, %rbx
	subq	%rcx, %rax
	movq	$0, -104(%rbp)
	sarq	$2, %rax
	movq	$0, -96(%rbp)
	imulq	%rbx, %rax
	movq	$0, -88(%rbp)
	movq	$-1, -76(%rbp)
	testl	%eax, %eax
	jle	.L1212
	xorl	%edx, %edx
	movq	%r15, %rsi
	movl	$-1, %edi
	movl	$-1, %eax
	testb	%dl, %dl
	jne	.L815
	.p2align 4,,10
	.p2align 3
.L1215:
	movslq	-72(%rbp), %rdx
	testl	%eax, %eax
	js	.L816
	cmpl	%edx, %eax
	je	.L817
	cltq
	leaq	(%rdx,%rdx,2), %rdx
	leaq	(%rax,%rax,2), %rax
	leaq	(%rcx,%rdx,4), %rdx
	leaq	(%rcx,%rax,4), %rax
	movq	(%rax), %rcx
	movq	%rcx, (%rdx)
	movl	8(%rax), %eax
	movl	%eax, 8(%rdx)
	movl	-76(%rbp), %eax
	movq	-112(%rbp), %rsi
	movl	-72(%rbp), %edx
.L816:
	addl	$1, %edx
	movl	%edx, -72(%rbp)
	cmpl	$-1, %eax
	je	.L1013
.L818:
	cmpb	$0, -79(%rbp)
	movslq	%eax, %r9
	jne	.L819
	movq	24(%rsi), %rax
	leaq	(%r9,%r9,2), %rdx
	leaq	-104(%rbp), %rdi
	leaq	(%rax,%rdx,4), %rsi
	call	_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE12emplace_backIJRS2_EEEvDpOT_
	movl	-76(%rbp), %r9d
	movq	-112(%rbp), %rsi
.L819:
	leal	1(%r9), %eax
	movq	-104(%rbp), %r12
	movq	-96(%rbp), %rdi
	movb	$0, -79(%rbp)
	movslq	%eax, %rdx
	movl	%eax, -76(%rbp)
	movq	24(%rsi), %rcx
	leaq	(%rdx,%rdx,2), %r10
	salq	$2, %r10
	leaq	(%rcx,%r10), %r11
	jmp	.L1194
	.p2align 4,,10
	.p2align 3
.L1213:
	cmpl	%edx, -8(%rdi)
	jg	.L821
	subq	$12, %rdi
	movq	%rdi, -96(%rbp)
.L1194:
	movq	%rdi, %rdx
	subq	%r12, %rdx
	sarq	$2, %rdx
	imulq	%rbx, %rdx
	cmpq	$1, %rdx
	movl	(%r11), %edx
	ja	.L1213
.L821:
	cmpl	%edx, 4(%r15)
	jg	.L1214
	movb	$1, -79(%rbp)
.L1186:
	movq	24(%rsi), %rcx
	movq	32(%rsi), %rdx
	subq	%rcx, %rdx
	sarq	$2, %rdx
	imull	%ebx, %edx
.L823:
	leal	1(%rax), %r9d
	cmpl	%edx, %r9d
	jge	.L811
	movzbl	-79(%rbp), %edx
	movl	%eax, %edi
	testb	%dl, %dl
	je	.L1215
.L815:
	cmpl	$-1, %edi
	je	.L1013
	movl	%eax, %r9d
	jmp	.L819
	.p2align 4,,10
	.p2align 3
.L1214:
	movq	32(%rsi), %r8
	subq	%rcx, %r8
	sarq	$2, %r8
	imulq	%rbx, %r8
	cmpl	$-1, 4(%r11)
	movl	%r8d, %edx
	jne	.L823
	addl	$2, %r9d
	movl	-8(%rdi), %eax
	cmpl	%r8d, %r9d
	jge	.L824
	movl	12(%rcx,%r10), %edx
	cmpl	%eax, %edx
	jge	.L824
	movl	%edx, 4(%r11)
.L1187:
	movq	-112(%rbp), %rsi
	movl	-76(%rbp), %eax
	jmp	.L1186
	.p2align 4,,10
	.p2align 3
.L811:
	cmpb	$0, -80(%rbp)
	je	.L1216
.L812:
	movabsq	$-6148914691236517205, %rbx
	movb	$1, -80(%rbp)
	movq	24(%rsi), %rdi
	movq	32(%rsi), %rdx
	subq	%rdi, %rdx
	sarq	$2, %rdx
	imulq	%rbx, %rdx
	cmpl	%edx, %r9d
	jge	.L1217
	cmpb	$0, -79(%rbp)
	jne	.L831
.L1153:
	movslq	-72(%rbp), %rdx
	testl	%eax, %eax
	js	.L832
	cmpl	%edx, %eax
	je	.L833
	cltq
	leaq	(%rdx,%rdx,2), %rdx
	leaq	(%rax,%rax,2), %rax
	leaq	(%rdi,%rdx,4), %rdx
	leaq	(%rdi,%rax,4), %rax
	movq	(%rax), %rcx
	movq	%rcx, (%rdx)
	movl	8(%rax), %eax
	movl	%eax, 8(%rdx)
	movl	-76(%rbp), %eax
	movq	-112(%rbp), %rsi
	movl	-72(%rbp), %edx
.L832:
	addl	$1, %edx
	movl	%edx, -72(%rbp)
	cmpl	$-1, %eax
	je	.L1012
.L834:
	cmpb	$0, -79(%rbp)
	jne	.L835
	cltq
	leaq	-104(%rbp), %rdi
	leaq	(%rax,%rax,2), %rdx
	movq	24(%rsi), %rax
	leaq	(%rax,%rdx,4), %rsi
	call	_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE12emplace_backIJRS2_EEEvDpOT_
	movl	-76(%rbp), %eax
	movq	-112(%rbp), %rsi
.L835:
	leal	1(%rax), %r9d
	movb	$0, -79(%rbp)
	movq	-104(%rbp), %r11
	movl	%r9d, -76(%rbp)
	movslq	%r9d, %rdx
	movq	24(%rsi), %rdi
	leaq	(%rdx,%rdx,2), %rdx
	leaq	(%rdi,%rdx,4), %r10
	movq	-96(%rbp), %rdx
	jmp	.L1188
	.p2align 4,,10
	.p2align 3
.L1218:
	movl	(%r10), %ecx
	cmpl	%ecx, -8(%rdx)
	jg	.L836
	subq	$12, %rdx
	movq	%rdx, -96(%rbp)
.L1188:
	movq	%rdx, %rcx
	subq	%r11, %rcx
	sarq	$2, %rcx
	imulq	%rbx, %rcx
	cmpq	$1, %rcx
	ja	.L1218
.L836:
	movq	32(%rsi), %rdx
	addl	$2, %eax
	subq	%rdi, %rdx
	sarq	$2, %rdx
	imulq	%rbx, %rdx
	cmpl	%edx, %eax
	jge	.L826
	movl	%r9d, %eax
	jmp	.L1153
	.p2align 4,,10
	.p2align 3
.L831:
	cmpl	$-1, %eax
	jne	.L835
.L1012:
	leaq	8(%rsi), %rcx
	leaq	4(%rsi), %rdx
	leaq	-104(%rbp), %rdi
	call	_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE12emplace_backIJRiS6_RjEEEvDpOT_
	movl	-76(%rbp), %eax
	movq	-112(%rbp), %rsi
	jmp	.L835
	.p2align 4,,10
	.p2align 3
.L826:
	cmpb	$0, -80(%rbp)
	movslq	-72(%rbp), %rax
	jne	.L814
	testl	%r9d, %r9d
	js	.L828
	cmpl	%eax, %r9d
	je	.L828
	movq	(%r10), %rdx
	leaq	(%rax,%rax,2), %rax
	leaq	(%rdi,%rax,4), %rax
	movq	%rdx, (%rax)
	movl	8(%r10), %edx
	movl	%edx, 8(%rax)
	movl	-72(%rbp), %eax
	movq	-112(%rbp), %rsi
.L828:
	addl	$1, %eax
	movl	%eax, -72(%rbp)
.L814:
	movabsq	$-6148914691236517205, %r9
	movb	$1, -80(%rbp)
	cltq
	movq	32(%rsi), %rcx
	movq	24(%rsi), %rdi
	movq	%rcx, %rdx
	subq	%rdi, %rdx
	sarq	$2, %rdx
	imulq	%r9, %rdx
	cmpq	%rdx, %rax
	ja	.L1219
	jnb	.L838
	leaq	(%rax,%rax,2), %rax
	leaq	(%rdi,%rax,4), %rax
	cmpq	%rax, %rcx
	je	.L838
	movq	%rax, 32(%rsi)
.L838:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L839
	call	_ZdlPv@PLT
.L839:
	movq	%r15, %rdi
	call	_ZN2v88internal12_GLOBAL__N_122MergeConsecutiveRangesEPNS0_16CoverageFunctionE
	movq	32(%r15), %rbx
	movq	24(%r15), %r14
	cmpq	%r14, %rbx
	je	.L840
	movq	%rbx, %r13
	leaq	_ZN2v88internal12_GLOBAL__N_120CompareCoverageBlockERKNS0_13CoverageBlockES4_(%rip), %rcx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	subq	%r14, %r13
	leaq	12(%r14), %r12
	movabsq	$-6148914691236517205, %rdx
	movq	%r13, %rax
	sarq	$2, %rax
	imulq	%rdx, %rax
	movl	$63, %edx
	bsrq	%rax, %rax
	xorq	$63, %rax
	subl	%eax, %edx
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	call	_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPN2v88internal13CoverageBlockESt6vectorIS4_SaIS4_EEEElNS0_5__ops15_Iter_comp_iterIPFbRKS4_SD_EEEEvT_SH_T0_T1_
	cmpq	$192, %r13
	jle	.L841
	leaq	192(%r14), %r13
	movq	%rbx, -144(%rbp)
	movq	%r15, -152(%rbp)
	movq	%r13, %rbx
	movq	%r14, %r13
	movq	%r12, %r14
	jmp	.L850
	.p2align 4,,10
	.p2align 3
.L1222:
	cmpq	%r14, %r13
	je	.L845
	movl	$12, %eax
	movq	%r14, %rdx
	movq	%r13, %rsi
	movl	%ecx, -136(%rbp)
	subq	%r13, %rdx
	leaq	0(%r13,%rax), %rdi
	call	memmove@PLT
	movl	-136(%rbp), %ecx
.L845:
	movl	%r12d, 0(%r13)
	movl	%ecx, 4(%r13)
	movl	%r15d, 8(%r13)
.L846:
	addq	$12, %r14
	cmpq	%r14, %rbx
	je	.L1220
.L850:
	movl	(%r14), %r12d
	movl	0(%r13), %eax
	movl	4(%r14), %ecx
	cmpl	%eax, %r12d
	setl	%dl
	je	.L1221
.L843:
	movl	8(%r14), %r15d
	testb	%dl, %dl
	jne	.L1222
	movq	%r14, %rax
	jmp	.L844
	.p2align 4,,10
	.p2align 3
.L847:
	cmpl	%edx, %r12d
	jge	.L849
.L848:
	movq	(%rax), %rdx
	movq	%rdx, 12(%rax)
	movl	8(%rax), %edx
	movl	%edx, 20(%rax)
.L844:
	movl	-12(%rax), %edx
	movq	%rax, %rsi
	subq	$12, %rax
	cmpl	%edx, %r12d
	jne	.L847
	cmpl	%ecx, -8(%rsi)
	jl	.L848
.L849:
	movl	%r12d, (%rsi)
	movl	%ecx, 4(%rsi)
	movl	%r15d, 8(%rsi)
	jmp	.L846
	.p2align 4,,10
	.p2align 3
.L787:
	movl	%ecx, -72(%rbp)
	jmp	.L788
	.p2align 4,,10
	.p2align 3
.L795:
	cmpb	$0, -80(%rbp)
	movslq	-72(%rbp), %rdx
	jne	.L784
	testl	%eax, %eax
	js	.L797
	cmpl	%edx, %eax
	je	.L797
	leaq	(%rdx,%rdx,2), %rax
	movq	(%r10), %rdx
	leaq	(%rdi,%rax,4), %rax
	movq	%rdx, (%rax)
	movl	8(%r10), %edx
	movl	%edx, 8(%rax)
	movl	-72(%rbp), %edx
	movq	-112(%rbp), %rsi
.L797:
	addl	$1, %edx
	movl	%edx, -72(%rbp)
	jmp	.L784
.L758:
	cmpb	$0, -80(%rbp)
	movslq	-72(%rbp), %rdx
	jne	.L761
	testb	%cl, %cl
	jne	.L761
	movl	%eax, %edi
	.p2align 4,,10
	.p2align 3
.L1018:
	testl	%edi, %edi
	js	.L762
	cmpl	%edx, %edi
	je	.L762
	movslq	%edi, %rdi
	leaq	(%rdx,%rdx,2), %rax
	leaq	(%rdi,%rdi,2), %rdx
	leaq	(%r9,%rax,4), %rax
	leaq	(%r9,%rdx,4), %rdx
	movq	(%rdx), %rcx
	movq	%rcx, (%rax)
	movl	8(%rdx), %edx
	movl	%edx, 8(%rax)
	movl	-72(%rbp), %edx
	movq	-112(%rbp), %rsi
.L762:
	addl	$1, %edx
	movl	%edx, -72(%rbp)
	jmp	.L761
	.p2align 4,,10
	.p2align 3
.L1013:
	leaq	8(%rsi), %rcx
	leaq	4(%rsi), %rdx
	leaq	-104(%rbp), %rdi
	call	_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE12emplace_backIJRiS6_RjEEEvDpOT_
	movl	-76(%rbp), %r9d
	movq	-112(%rbp), %rsi
	jmp	.L819
	.p2align 4,,10
	.p2align 3
.L1216:
	cmpb	$0, -79(%rbp)
	jne	.L812
	movslq	-72(%rbp), %rdx
	testl	%eax, %eax
	js	.L810
	cmpl	%edx, %eax
	je	.L810
	cltq
	leaq	(%rdx,%rdx,2), %rdx
	leaq	(%rax,%rax,2), %rax
	leaq	(%rcx,%rdx,4), %rdx
	leaq	(%rcx,%rax,4), %rax
	movq	(%rax), %rcx
	movq	%rcx, (%rdx)
	movl	8(%rax), %eax
	movl	%eax, 8(%rdx)
	movl	-76(%rbp), %eax
	movl	-72(%rbp), %edx
	movq	-112(%rbp), %rsi
	leal	1(%rax), %r9d
.L810:
	addl	$1, %edx
	movl	%edx, -72(%rbp)
	jmp	.L812
	.p2align 4,,10
	.p2align 3
.L766:
	leal	1(%rax), %edx
	movl	%edx, -72(%rbp)
	jmp	.L767
	.p2align 4,,10
	.p2align 3
.L1217:
	movl	-72(%rbp), %eax
	jmp	.L814
	.p2align 4,,10
	.p2align 3
.L824:
	subq	%r12, %rdi
	cmpq	$12, %rdi
	jne	.L825
	subl	$1, %eax
.L825:
	movl	%eax, 4(%r11)
	jmp	.L1187
	.p2align 4,,10
	.p2align 3
.L990:
	movb	$1, -79(%rbp)
	movq	24(%rsi), %rdi
	movq	32(%rsi), %rdx
	jmp	.L992
	.p2align 4,,10
	.p2align 3
.L833:
	leal	1(%rax), %edx
	movl	%edx, -72(%rbp)
	jmp	.L834
	.p2align 4,,10
	.p2align 3
.L802:
	addl	$1, %eax
	movl	%eax, -72(%rbp)
	jmp	.L803
	.p2align 4,,10
	.p2align 3
.L817:
	leal	1(%rax), %edx
	movl	%edx, -72(%rbp)
	jmp	.L818
	.p2align 4,,10
	.p2align 3
.L684:
	movq	%r14, %r13
	movq	-144(%rbp), %r14
	movq	-168(%rbp), %r15
	cmpq	%r14, %r13
	je	.L683
	movq	%r13, %rbx
	leaq	_ZN2v88internal12_GLOBAL__N_120CompareCoverageBlockERKNS0_13CoverageBlockES4_(%rip), %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	subq	%r14, %rbx
	leaq	12(%r14), %r12
	movabsq	$-6148914691236517205, %rdx
	movq	%rbx, %rax
	sarq	$2, %rax
	imulq	%rdx, %rax
	movl	$63, %edx
	bsrq	%rax, %rax
	xorq	$63, %rax
	subl	%eax, %edx
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	call	_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPN2v88internal13CoverageBlockESt6vectorIS4_SaIS4_EEEElNS0_5__ops15_Iter_comp_iterIPFbRKS4_SD_EEEEvT_SH_T0_T1_
	cmpq	$192, %rbx
	jg	.L1223
	cmpq	%r12, %r13
	je	.L683
	movq	%r15, -168(%rbp)
	movq	%r13, %r15
	jmp	.L720
	.p2align 4,,10
	.p2align 3
.L1226:
	cmpq	%r14, %r12
	je	.L715
	movl	$12, %eax
	movq	%r12, %rdx
	movq	%r14, %rsi
	movl	%ecx, -144(%rbp)
	subq	%r14, %rdx
	leaq	(%r14,%rax), %rdi
	call	memmove@PLT
	movl	-144(%rbp), %ecx
.L715:
	movl	%ebx, (%r14)
	movl	%r13d, 4(%r14)
	movl	%ecx, 8(%r14)
.L716:
	addq	$12, %r12
	cmpq	%r12, %r15
	je	.L1224
.L720:
	movl	(%r12), %ebx
	movl	(%r14), %eax
	movl	4(%r12), %r13d
	cmpl	%eax, %ebx
	setl	%dl
	je	.L1225
.L713:
	movl	8(%r12), %ecx
	testb	%dl, %dl
	jne	.L1226
	movq	%r12, %rax
	jmp	.L714
	.p2align 4,,10
	.p2align 3
.L717:
	cmpl	%edx, %ebx
	jge	.L719
.L718:
	movq	(%rax), %rdx
	movq	%rdx, 12(%rax)
	movl	8(%rax), %edx
	movl	%edx, 20(%rax)
.L714:
	movl	-12(%rax), %edx
	movq	%rax, %rsi
	subq	$12, %rax
	cmpl	%edx, %ebx
	jne	.L717
	cmpl	%r13d, -8(%rsi)
	jl	.L718
.L719:
	movl	%ebx, (%rsi)
	movl	%r13d, 4(%rsi)
	movl	%ecx, 8(%rsi)
	jmp	.L716
	.p2align 4,,10
	.p2align 3
.L754:
	movl	(%rdi), %r13d
	jmp	.L755
	.p2align 4,,10
	.p2align 3
.L1200:
	testq	%r8, %r8
	jne	.L1227
	movq	$0, -136(%rbp)
	movl	$12, %edi
	xorl	%eax, %eax
	jmp	.L689
	.p2align 4,,10
	.p2align 3
.L1221:
	cmpl	%ecx, 4(%r13)
	setl	%dl
	jmp	.L843
	.p2align 4,,10
	.p2align 3
.L1026:
	movl	$12, %edi
	jmp	.L688
	.p2align 4,,10
	.p2align 3
.L840:
	movq	%r15, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	$0, -96(%rbp)
	movq	$0, -88(%rbp)
	movb	$0, -79(%rbp)
	movl	$-1, -76(%rbp)
.L868:
	movl	$0, -72(%rbp)
	movq	%r15, %rsi
	xorl	%edi, %edi
	movq	$-1, %rax
.L895:
	movabsq	$-6148914691236517205, %rbx
	movb	$1, -80(%rbp)
	movq	24(%rsi), %rdx
	movq	32(%rsi), %rcx
	subq	%rdx, %rcx
	sarq	$2, %rcx
	imulq	%rbx, %rcx
	cmpl	%ecx, %edi
	jge	.L1228
	cmpb	$0, -79(%rbp)
	jne	.L885
.L1155:
	movslq	-72(%rbp), %rcx
	testl	%eax, %eax
	js	.L886
	cmpl	%ecx, %eax
	je	.L887
	leaq	(%rax,%rax,2), %rax
	leaq	(%rcx,%rcx,2), %rcx
	leaq	(%rdx,%rax,4), %rax
	leaq	(%rdx,%rcx,4), %rcx
	movq	(%rax), %rdx
	movq	%rdx, (%rcx)
	movl	8(%rax), %eax
	movl	%eax, 8(%rcx)
	movslq	-76(%rbp), %rax
	movq	-112(%rbp), %rsi
	movl	-72(%rbp), %ecx
.L886:
	addl	$1, %ecx
	movl	%ecx, -72(%rbp)
	cmpl	$-1, %eax
	je	.L1009
.L888:
	cmpb	$0, -79(%rbp)
	jne	.L889
	leaq	(%rax,%rax,2), %rdx
	movq	24(%rsi), %rax
	leaq	-104(%rbp), %rdi
	leaq	(%rax,%rdx,4), %rsi
	call	_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE12emplace_backIJRS2_EEEvDpOT_
	movl	-76(%rbp), %eax
	movq	-112(%rbp), %rsi
.L889:
	leal	1(%rax), %ecx
	movb	$0, -79(%rbp)
	movq	-104(%rbp), %r11
	movl	%ecx, -76(%rbp)
	movslq	%ecx, %rdi
	movq	24(%rsi), %rdx
	leaq	(%rdi,%rdi,2), %rdi
	leaq	(%rdx,%rdi,4), %r9
	movq	-96(%rbp), %rdi
	movq	%rdi, %r10
	subq	%r11, %r10
	sarq	$2, %r10
	imulq	%rbx, %r10
	cmpq	$1, %r10
	ja	.L891
	jmp	.L890
	.p2align 4,,10
	.p2align 3
.L1229:
	subq	$12, %rdi
	movq	%rdi, %r8
	movq	%rdi, -96(%rbp)
	subq	%r11, %r8
	sarq	$2, %r8
	imulq	%rbx, %r8
	cmpq	$1, %r8
	jbe	.L890
.L891:
	movl	(%r9), %r14d
	cmpl	%r14d, -8(%rdi)
	jle	.L1229
.L890:
	movq	32(%rsi), %rdi
	addl	$2, %eax
	subq	%rdx, %rdi
	sarq	$2, %rdi
	imulq	%rbx, %rdi
	cmpl	%edi, %eax
	jge	.L880
	movslq	%ecx, %rax
	jmp	.L1155
	.p2align 4,,10
	.p2align 3
.L885:
	cmpl	$-1, %eax
	jne	.L889
.L1009:
	leaq	8(%rsi), %rcx
	leaq	4(%rsi), %rdx
	leaq	-104(%rbp), %rdi
	call	_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE12emplace_backIJRiS6_RjEEEvDpOT_
	movl	-76(%rbp), %eax
	movq	-112(%rbp), %rsi
	jmp	.L889
	.p2align 4,,10
	.p2align 3
.L880:
	cmpb	$0, -80(%rbp)
	movslq	-72(%rbp), %rax
	jne	.L873
.L1008:
	testl	%ecx, %ecx
	js	.L882
	cmpl	%eax, %ecx
	je	.L882
	leaq	(%rax,%rax,2), %rax
	leaq	(%rdx,%rax,4), %rax
	movq	(%r9), %rdx
	movq	%rdx, (%rax)
	movl	8(%r9), %edx
	movl	%edx, 8(%rax)
	movl	-72(%rbp), %eax
	movq	-112(%rbp), %rsi
.L882:
	addl	$1, %eax
	movl	%eax, -72(%rbp)
.L873:
	movabsq	$-6148914691236517205, %r9
	movb	$1, -80(%rbp)
	cltq
	movq	32(%rsi), %rcx
	movq	24(%rsi), %rdi
	movq	%rcx, %rdx
	subq	%rdi, %rdx
	sarq	$2, %rdx
	imulq	%r9, %rdx
	cmpq	%rdx, %rax
	ja	.L1230
	jnb	.L892
	leaq	(%rax,%rax,2), %rax
	leaq	(%rdi,%rax,4), %rax
	cmpq	%rax, %rcx
	je	.L892
	movq	%rax, 32(%rsi)
.L892:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L893
	call	_ZdlPv@PLT
.L893:
	xorl	%r10d, %r10d
	movq	%r15, -112(%rbp)
	movq	%r15, %rsi
	movl	$-1, %eax
	movq	$0, -104(%rbp)
	movabsq	$-6148914691236517205, %rbx
	movq	$0, -96(%rbp)
	movq	$0, -88(%rbp)
	movw	%r10w, -80(%rbp)
	movq	$-1, -76(%rbp)
.L894:
	movq	24(%rsi), %rdi
	.p2align 4,,10
	.p2align 3
.L985:
	movq	32(%rsi), %rdx
	leal	1(%rax), %ecx
	subq	%rdi, %rdx
	sarq	$2, %rdx
	imulq	%rbx, %rdx
	cmpl	%edx, %ecx
	jge	.L1231
	cmpb	$0, -79(%rbp)
	jne	.L902
	movslq	-72(%rbp), %rdx
	testl	%eax, %eax
	js	.L903
	cmpl	%edx, %eax
	je	.L904
	cltq
	leaq	(%rdx,%rdx,2), %rdx
	leaq	(%rax,%rax,2), %rax
	leaq	(%rdi,%rdx,4), %rdx
	leaq	(%rdi,%rax,4), %rax
	movq	(%rax), %rcx
	movq	%rcx, (%rdx)
	movl	8(%rax), %eax
	movl	%eax, 8(%rdx)
	movl	-76(%rbp), %eax
	movq	-112(%rbp), %rsi
	movl	-72(%rbp), %edx
.L903:
	addl	$1, %edx
	movl	%edx, -72(%rbp)
	cmpl	$-1, %eax
	je	.L1007
.L905:
	cmpb	$0, -79(%rbp)
	je	.L907
	addl	$1, %eax
.L906:
	movb	$0, -79(%rbp)
	movslq	%eax, %rdx
	movq	-104(%rbp), %r9
	movl	%eax, -76(%rbp)
	movq	24(%rsi), %rdi
	leaq	(%rdx,%rdx,2), %rdx
	leaq	(%rdi,%rdx,4), %r8
	movq	-96(%rbp), %rdx
	jmp	.L1190
	.p2align 4,,10
	.p2align 3
.L1232:
	movl	(%r8), %ecx
	cmpl	%ecx, -8(%rdx)
	jg	.L908
	subq	$12, %rdx
	movq	%rdx, -96(%rbp)
.L1190:
	movq	%rdx, %rcx
	subq	%r9, %rcx
	sarq	$2, %rcx
	imulq	%rbx, %rcx
	cmpq	$1, %rcx
	ja	.L1232
.L908:
	movl	8(%r8), %ecx
	cmpl	%ecx, -4(%rdx)
	jne	.L985
	movb	$1, -79(%rbp)
	jmp	.L894
	.p2align 4,,10
	.p2align 3
.L1231:
	cmpb	$0, -80(%rbp)
	jne	.L898
	cmpb	$0, -79(%rbp)
	jne	.L898
	movslq	-72(%rbp), %rdx
	testl	%eax, %eax
	js	.L899
	cmpl	%edx, %eax
	je	.L899
	cltq
	leaq	(%rdx,%rdx,2), %rdx
	leaq	(%rax,%rax,2), %rax
	leaq	(%rdi,%rdx,4), %rdx
	leaq	(%rdi,%rax,4), %rax
	movq	(%rax), %rcx
	movq	%rcx, (%rdx)
	movl	8(%rax), %eax
	movl	%eax, 8(%rdx)
	movl	-76(%rbp), %eax
	movl	-72(%rbp), %edx
	movq	-112(%rbp), %rsi
	leal	1(%rax), %ecx
.L899:
	addl	$1, %edx
	movl	%edx, -72(%rbp)
.L898:
	movabsq	$-6148914691236517205, %rbx
	movb	$1, -80(%rbp)
	movq	24(%rsi), %rdi
	movq	32(%rsi), %rdx
	subq	%rdi, %rdx
	sarq	$2, %rdx
	imulq	%rbx, %rdx
	cmpl	%edx, %ecx
	jge	.L1233
	cmpb	$0, -79(%rbp)
	jne	.L915
.L1158:
	movslq	-72(%rbp), %rdx
	testl	%eax, %eax
	js	.L916
	cmpl	%edx, %eax
	je	.L917
	cltq
	leaq	(%rdx,%rdx,2), %rdx
	leaq	(%rax,%rax,2), %rax
	leaq	(%rdi,%rdx,4), %rdx
	leaq	(%rdi,%rax,4), %rax
	movq	(%rax), %rcx
	movq	%rcx, (%rdx)
	movl	8(%rax), %eax
	movl	%eax, 8(%rdx)
	movl	-76(%rbp), %eax
	movq	-112(%rbp), %rsi
	movl	-72(%rbp), %edx
.L916:
	addl	$1, %edx
	movl	%edx, -72(%rbp)
	cmpl	$-1, %eax
	je	.L1006
.L918:
	cmpb	$0, -79(%rbp)
	jne	.L919
	cltq
	leaq	-104(%rbp), %rdi
	leaq	(%rax,%rax,2), %rdx
	movq	24(%rsi), %rax
	leaq	(%rax,%rdx,4), %rsi
	call	_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE12emplace_backIJRS2_EEEvDpOT_
	movl	-76(%rbp), %eax
	movq	-112(%rbp), %rsi
.L919:
	leal	1(%rax), %r9d
	movb	$0, -79(%rbp)
	movq	-104(%rbp), %r11
	movl	%r9d, -76(%rbp)
	movslq	%r9d, %rdx
	movq	24(%rsi), %rdi
	leaq	(%rdx,%rdx,2), %rdx
	leaq	(%rdi,%rdx,4), %r10
	movq	-96(%rbp), %rdx
	jmp	.L1191
	.p2align 4,,10
	.p2align 3
.L1234:
	movl	(%r10), %ecx
	cmpl	%ecx, -8(%rdx)
	jg	.L920
	subq	$12, %rdx
	movq	%rdx, -96(%rbp)
.L1191:
	movq	%rdx, %rcx
	subq	%r11, %rcx
	sarq	$2, %rcx
	imulq	%rbx, %rcx
	cmpq	$1, %rcx
	ja	.L1234
.L920:
	movq	32(%rsi), %rdx
	addl	$2, %eax
	subq	%rdi, %rdx
	sarq	$2, %rdx
	imulq	%rbx, %rdx
	cmpl	%edx, %eax
	jge	.L910
	movl	%r9d, %eax
	jmp	.L1158
	.p2align 4,,10
	.p2align 3
.L915:
	cmpl	$-1, %eax
	jne	.L919
.L1006:
	leaq	8(%rsi), %rcx
	leaq	4(%rsi), %rdx
	leaq	-104(%rbp), %rdi
	call	_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE12emplace_backIJRiS6_RjEEEvDpOT_
	movl	-76(%rbp), %eax
	movq	-112(%rbp), %rsi
	jmp	.L919
	.p2align 4,,10
	.p2align 3
.L902:
	cmpl	$-1, %eax
	je	.L1007
	movl	%ecx, %eax
	jmp	.L906
	.p2align 4,,10
	.p2align 3
.L907:
	cltq
	leaq	-104(%rbp), %rdi
	leaq	(%rax,%rax,2), %rdx
	movq	24(%rsi), %rax
	leaq	(%rax,%rdx,4), %rsi
	call	_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE12emplace_backIJRS2_EEEvDpOT_
	movl	-76(%rbp), %eax
	movq	-112(%rbp), %rsi
	addl	$1, %eax
	jmp	.L906
	.p2align 4,,10
	.p2align 3
.L1007:
	leaq	8(%rsi), %rcx
	leaq	4(%rsi), %rdx
	leaq	-104(%rbp), %rdi
	call	_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE12emplace_backIJRiS6_RjEEEvDpOT_
	movl	-76(%rbp), %eax
	movq	-112(%rbp), %rsi
	addl	$1, %eax
	jmp	.L906
	.p2align 4,,10
	.p2align 3
.L1233:
	movl	-72(%rbp), %eax
.L901:
	movabsq	$-6148914691236517205, %r9
	movb	$1, -80(%rbp)
	cltq
	movq	32(%rsi), %rcx
	movq	24(%rsi), %rdi
	movq	%rcx, %rdx
	subq	%rdi, %rdx
	sarq	$2, %rdx
	imulq	%r9, %rdx
	cmpq	%rdx, %rax
	ja	.L1235
	jnb	.L922
	leaq	(%rax,%rax,2), %rax
	leaq	(%rdi,%rax,4), %rax
	cmpq	%rax, %rcx
	je	.L922
	movq	%rax, 32(%rsi)
.L922:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L923
	call	_ZdlPv@PLT
.L923:
	movabsq	$-6148914691236517205, %rbx
	movq	%r15, %rdi
	call	_ZN2v88internal12_GLOBAL__N_122MergeConsecutiveRangesEPNS0_16CoverageFunctionE
	xorl	%r9d, %r9d
	movq	32(%r15), %rax
	movq	%r15, -112(%rbp)
	movw	%r9w, -80(%rbp)
	movq	24(%r15), %r9
	movq	$0, -104(%rbp)
	subq	%r9, %rax
	movq	$0, -96(%rbp)
	sarq	$2, %rax
	movq	$0, -88(%rbp)
	imulq	%rbx, %rax
	movq	$-1, -76(%rbp)
	testl	%eax, %eax
	jle	.L1236
	xorl	%r11d, %r11d
	movzbl	-153(%rbp), %r13d
	movq	%r15, %rsi
	xorl	%edi, %edi
	xorl	%r12d, %r12d
	movl	$-1, %edx
	movq	$-1, %rax
	testb	%r11b, %r11b
	jne	.L929
	.p2align 4,,10
	.p2align 3
.L1239:
	movslq	-72(%rbp), %rdx
	testl	%eax, %eax
	js	.L930
	cmpl	%edx, %eax
	je	.L931
	leaq	(%rax,%rax,2), %rax
	leaq	(%rdx,%rdx,2), %rdx
	leaq	(%r9,%rax,4), %rax
	leaq	(%r9,%rdx,4), %rdx
	movq	(%rax), %rcx
	movq	%rcx, (%rdx)
	movl	8(%rax), %eax
	movl	%eax, 8(%rdx)
	movslq	-76(%rbp), %rax
	movq	-112(%rbp), %rsi
	movl	-72(%rbp), %edx
.L930:
	addl	$1, %edx
	movl	%edx, -72(%rbp)
	cmpl	$-1, %eax
	je	.L1003
.L932:
	cmpb	$0, -79(%rbp)
	jne	.L1192
	leaq	(%rax,%rax,2), %rdx
	movq	24(%rsi), %rax
	leaq	-104(%rbp), %rdi
	leaq	(%rax,%rdx,4), %rsi
	call	_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE12emplace_backIJRS2_EEEvDpOT_
	movl	-76(%rbp), %eax
	movq	-112(%rbp), %rsi
.L1192:
	movq	-96(%rbp), %rdi
	movq	-104(%rbp), %r12
.L933:
	leal	1(%rax), %edx
	movb	$0, -79(%rbp)
	movslq	%edx, %rcx
	movl	%edx, -76(%rbp)
	movq	24(%rsi), %r9
	leaq	(%rcx,%rcx,2), %r10
	salq	$2, %r10
	leaq	(%r9,%r10), %r8
	jmp	.L1193
	.p2align 4,,10
	.p2align 3
.L1237:
	movl	(%r8), %ecx
	cmpl	%ecx, -8(%rdi)
	jg	.L935
	subq	$12, %rdi
	movq	%rdi, -96(%rbp)
.L1193:
	movq	%rdi, %rcx
	subq	%r12, %rcx
	sarq	$2, %rcx
	imulq	%rbx, %rcx
	cmpq	$1, %rcx
	ja	.L1237
.L935:
	movl	8(%r8), %ecx
	xorl	%r11d, %r11d
	testl	%ecx, %ecx
	jne	.L937
	movl	-4(%rdi), %r8d
	testl	%r8d, %r8d
	jne	.L937
	movb	$1, -79(%rbp)
	movq	24(%rsi), %r9
	movl	%r13d, %r11d
.L937:
	movq	32(%rsi), %rcx
	addl	$2, %eax
	subq	%r9, %rcx
	sarq	$2, %rcx
	imulq	%rbx, %rcx
	cmpl	%ecx, %eax
	jge	.L1238
	movslq	%edx, %rax
	testb	%r11b, %r11b
	je	.L1239
.L929:
	cmpl	$-1, %edx
	jne	.L933
.L1003:
	leaq	8(%rsi), %rcx
	leaq	4(%rsi), %rdx
	leaq	-104(%rbp), %rdi
	call	_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE12emplace_backIJRiS6_RjEEEvDpOT_
	movl	-76(%rbp), %eax
	movq	-112(%rbp), %rsi
	movq	-96(%rbp), %rdi
	movq	-104(%rbp), %r12
	jmp	.L933
	.p2align 4,,10
	.p2align 3
.L1238:
	cmpb	$0, -80(%rbp)
	je	.L1240
.L926:
	movabsq	$-6148914691236517205, %rbx
	movb	$1, -80(%rbp)
	movq	24(%rsi), %rdi
	movq	32(%rsi), %rcx
	subq	%rdi, %rcx
	sarq	$2, %rcx
	imulq	%rbx, %rcx
	cmpl	%ecx, %eax
	jge	.L1241
	cmpb	$0, -79(%rbp)
	jne	.L943
.L1160:
	movslq	-72(%rbp), %rax
	testl	%edx, %edx
	js	.L944
	cmpl	%eax, %edx
	je	.L945
	movslq	%edx, %rdx
	leaq	(%rax,%rax,2), %rax
	leaq	(%rdx,%rdx,2), %rdx
	leaq	(%rdi,%rax,4), %rax
	leaq	(%rdi,%rdx,4), %rdx
	movq	(%rdx), %rcx
	movq	%rcx, (%rax)
	movl	8(%rdx), %edx
	movl	%edx, 8(%rax)
	movl	-76(%rbp), %edx
	movq	-112(%rbp), %rsi
	movl	-72(%rbp), %eax
.L944:
	addl	$1, %eax
	movl	%eax, -72(%rbp)
	cmpl	$-1, %edx
	je	.L1002
.L946:
	cmpb	$0, -79(%rbp)
	jne	.L947
	movq	24(%rsi), %rax
	movslq	%edx, %rdx
	leaq	-104(%rbp), %rdi
	leaq	(%rdx,%rdx,2), %rdx
	leaq	(%rax,%rdx,4), %rsi
	call	_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE12emplace_backIJRS2_EEEvDpOT_
	movl	-76(%rbp), %edx
	movq	-112(%rbp), %rsi
.L947:
	leal	1(%rdx), %r9d
	movb	$0, -79(%rbp)
	movq	-104(%rbp), %r11
	movl	%r9d, -76(%rbp)
	movslq	%r9d, %rax
	movq	24(%rsi), %rdi
	leaq	(%rax,%rax,2), %rax
	leaq	(%rdi,%rax,4), %r10
	movq	-96(%rbp), %rax
	movq	%rax, %rcx
	subq	%r11, %rcx
	sarq	$2, %rcx
	imulq	%rbx, %rcx
	cmpq	$1, %rcx
	jbe	.L948
	subq	$12, %rax
	jmp	.L949
	.p2align 4,,10
	.p2align 3
.L1242:
	subq	%r11, %rcx
	movq	%rax, -96(%rbp)
	subq	$12, %rax
	sarq	$2, %rcx
	imulq	%rbx, %rcx
	cmpq	$1, %rcx
	jbe	.L948
.L949:
	movl	(%r10), %r14d
	movq	%rax, %rcx
	cmpl	%r14d, 4(%rax)
	jle	.L1242
.L948:
	movq	32(%rsi), %rax
	addl	$2, %edx
	subq	%rdi, %rax
	sarq	$2, %rax
	imulq	%rbx, %rax
	cmpl	%eax, %edx
	jge	.L938
	movl	%r9d, %edx
	jmp	.L1160
	.p2align 4,,10
	.p2align 3
.L943:
	cmpl	$-1, %edx
	jne	.L947
.L1002:
	leaq	4(%rsi), %rdx
	leaq	8(%rsi), %rcx
	leaq	-104(%rbp), %rdi
	call	_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE12emplace_backIJRiS6_RjEEEvDpOT_
	movl	-76(%rbp), %edx
	movq	-112(%rbp), %rsi
	jmp	.L947
	.p2align 4,,10
	.p2align 3
.L938:
	cmpb	$0, -80(%rbp)
	movslq	-72(%rbp), %rax
	jne	.L928
	testl	%r9d, %r9d
	js	.L940
	cmpl	%eax, %r9d
	je	.L940
	movq	(%r10), %rdx
	leaq	(%rax,%rax,2), %rax
	leaq	(%rdi,%rax,4), %rax
	movq	%rdx, (%rax)
	movl	8(%r10), %edx
	movl	%edx, 8(%rax)
	movl	-72(%rbp), %eax
	movq	-112(%rbp), %rsi
.L940:
	addl	$1, %eax
	movl	%eax, -72(%rbp)
.L928:
	movabsq	$-6148914691236517205, %r9
	movb	$1, -80(%rbp)
	cltq
	movq	32(%rsi), %rcx
	movq	24(%rsi), %rdi
	movq	%rcx, %rdx
	subq	%rdi, %rdx
	sarq	$2, %rdx
	imulq	%r9, %rdx
	cmpq	%rdx, %rax
	ja	.L1243
	jnb	.L950
	leaq	(%rax,%rax,2), %rax
	leaq	(%rdi,%rax,4), %rax
	cmpq	%rax, %rcx
	je	.L950
	movq	%rax, 32(%rsi)
.L950:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L951
	call	_ZdlPv@PLT
.L951:
	xorl	%esi, %esi
	movq	%r15, -112(%rbp)
	movl	$-1, %eax
	movabsq	$-6148914691236517205, %rbx
	movq	$0, -104(%rbp)
	movq	$0, -96(%rbp)
	movq	$0, -88(%rbp)
	movw	%si, -80(%rbp)
	movq	$-1, -76(%rbp)
.L966:
	movq	24(%r15), %rsi
	.p2align 4,,10
	.p2align 3
.L982:
	movq	32(%r15), %rdx
	leal	1(%rax), %ecx
	subq	%rsi, %rdx
	sarq	$2, %rdx
	imulq	%rbx, %rdx
	cmpl	%edx, %ecx
	jge	.L1244
	cmpb	$0, -79(%rbp)
	jne	.L957
	movslq	-72(%rbp), %rdx
	testl	%eax, %eax
	js	.L958
	cmpl	%edx, %eax
	je	.L959
	cltq
	leaq	(%rdx,%rdx,2), %rdx
	leaq	(%rax,%rax,2), %rax
	leaq	(%rsi,%rdx,4), %rdx
	leaq	(%rsi,%rax,4), %rax
	movq	(%rax), %rcx
	movq	%rcx, (%rdx)
	movl	8(%rax), %eax
	movl	%eax, 8(%rdx)
	movl	-76(%rbp), %eax
	movq	-112(%rbp), %r15
	movl	-72(%rbp), %edx
.L958:
	addl	$1, %edx
	movl	%edx, -72(%rbp)
	cmpl	$-1, %eax
	je	.L999
.L960:
	cmpb	$0, -79(%rbp)
	je	.L962
	addl	$1, %eax
.L961:
	movb	$0, -79(%rbp)
	movslq	%eax, %rdx
	movq	-104(%rbp), %r8
	movl	%eax, -76(%rbp)
	movq	24(%r15), %rsi
	leaq	(%rdx,%rdx,2), %rdx
	leaq	(%rsi,%rdx,4), %rdi
	movq	-96(%rbp), %rdx
	jmp	.L1195
	.p2align 4,,10
	.p2align 3
.L1245:
	cmpl	%ecx, -8(%rdx)
	jg	.L964
	subq	$12, %rdx
	movq	%rdx, -96(%rbp)
.L1195:
	movq	%rdx, %rcx
	subq	%r8, %rcx
	sarq	$2, %rcx
	imulq	%rbx, %rcx
	cmpq	$1, %rcx
	movl	(%rdi), %ecx
	ja	.L1245
.L964:
	cmpl	%ecx, 4(%rdi)
	jne	.L982
	movb	$1, -79(%rbp)
	jmp	.L966
	.p2align 4,,10
	.p2align 3
.L1244:
	cmpb	$0, -80(%rbp)
	jne	.L953
	cmpb	$0, -79(%rbp)
	jne	.L953
	movslq	-72(%rbp), %rdx
	testl	%eax, %eax
	js	.L954
	cmpl	%edx, %eax
	je	.L954
	cltq
	leaq	(%rdx,%rdx,2), %rdx
	leaq	(%rax,%rax,2), %rax
	leaq	(%rsi,%rdx,4), %rdx
	leaq	(%rsi,%rax,4), %rax
	movq	(%rax), %rcx
	movq	%rcx, (%rdx)
	movl	8(%rax), %eax
	movl	%eax, 8(%rdx)
	movl	-76(%rbp), %eax
	movl	-72(%rbp), %edx
	movq	-112(%rbp), %r15
	leal	1(%rax), %ecx
.L954:
	addl	$1, %edx
	movl	%edx, -72(%rbp)
.L953:
	movb	$1, -80(%rbp)
	movq	24(%r15), %rdi
	movabsq	$-6148914691236517205, %rbx
	movq	32(%r15), %rdx
	movl	-72(%rbp), %esi
	subq	%rdi, %rdx
	sarq	$2, %rdx
	imulq	%rbx, %rdx
	cmpl	%edx, %ecx
	jge	.L956
	cmpb	$0, -79(%rbp)
	movslq	%eax, %rcx
	jne	.L972
.L1162:
	movslq	-72(%rbp), %rdx
	testl	%eax, %eax
	js	.L973
	cmpl	%edx, %eax
	je	.L974
	cltq
	leaq	(%rdx,%rdx,2), %rdx
	leaq	(%rax,%rax,2), %rax
	leaq	(%rdi,%rdx,4), %rdx
	leaq	(%rdi,%rax,4), %rax
	movq	(%rax), %rcx
	movq	%rcx, (%rdx)
	movl	8(%rax), %eax
	movl	%eax, 8(%rdx)
	movslq	-76(%rbp), %rcx
	movq	-112(%rbp), %r15
	movl	-72(%rbp), %edx
.L973:
	addl	$1, %edx
	movl	%edx, -72(%rbp)
	cmpl	$-1, %ecx
	je	.L998
.L975:
	cmpb	$0, -79(%rbp)
	jne	.L976
	movq	24(%r15), %rax
	leaq	(%rcx,%rcx,2), %rdx
	leaq	-104(%rbp), %rdi
	leaq	(%rax,%rdx,4), %rsi
	call	_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE12emplace_backIJRS2_EEEvDpOT_
	movl	-76(%rbp), %ecx
	movq	-112(%rbp), %r15
.L976:
	leal	1(%rcx), %eax
	movb	$0, -79(%rbp)
	movq	-104(%rbp), %r10
	movl	%eax, -76(%rbp)
	movslq	%eax, %rdx
	movq	24(%r15), %rdi
	leaq	(%rdx,%rdx,2), %rdx
	leaq	(%rdi,%rdx,4), %r9
	movq	-96(%rbp), %rdx
	movq	%rdx, %rsi
	subq	%r10, %rsi
	sarq	$2, %rsi
	imulq	%rbx, %rsi
	cmpq	$1, %rsi
	jbe	.L977
	subq	$12, %rdx
	jmp	.L978
	.p2align 4,,10
	.p2align 3
.L1246:
	subq	%r10, %rsi
	movq	%rdx, -96(%rbp)
	subq	$12, %rdx
	sarq	$2, %rsi
	imulq	%rbx, %rsi
	cmpq	$1, %rsi
	jbe	.L977
.L978:
	movl	(%r9), %r14d
	movq	%rdx, %rsi
	cmpl	%r14d, 4(%rdx)
	jle	.L1246
.L977:
	movq	32(%r15), %rdx
	addl	$2, %ecx
	subq	%rdi, %rdx
	sarq	$2, %rdx
	imulq	%rbx, %rdx
	cmpl	%edx, %ecx
	jge	.L967
	movslq	%eax, %rcx
	jmp	.L1162
	.p2align 4,,10
	.p2align 3
.L972:
	cmpl	$-1, %ecx
	jne	.L976
.L998:
	leaq	8(%r15), %rcx
	leaq	4(%r15), %rdx
	movq	%r15, %rsi
	leaq	-104(%rbp), %rdi
	call	_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE12emplace_backIJRiS6_RjEEEvDpOT_
	movl	-76(%rbp), %ecx
	movq	-112(%rbp), %r15
	jmp	.L976
	.p2align 4,,10
	.p2align 3
.L957:
	cmpl	$-1, %eax
	je	.L999
	movl	%ecx, %eax
	jmp	.L961
	.p2align 4,,10
	.p2align 3
.L962:
	cltq
	leaq	-104(%rbp), %rdi
	leaq	(%rax,%rax,2), %rdx
	movq	24(%r15), %rax
	leaq	(%rax,%rdx,4), %rsi
	call	_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE12emplace_backIJRS2_EEEvDpOT_
	movl	-76(%rbp), %eax
	movq	-112(%rbp), %r15
	addl	$1, %eax
	jmp	.L961
	.p2align 4,,10
	.p2align 3
.L967:
	cmpb	$0, -80(%rbp)
	movslq	-72(%rbp), %rsi
	jne	.L956
	testl	%eax, %eax
	js	.L969
	cmpl	%esi, %eax
	je	.L969
	movq	(%r9), %rdx
	leaq	(%rsi,%rsi,2), %rax
	leaq	(%rdi,%rax,4), %rax
	movq	%rdx, (%rax)
	movl	8(%r9), %edx
	movl	%edx, 8(%rax)
	movl	-72(%rbp), %esi
	movq	-112(%rbp), %r15
.L969:
	addl	$1, %esi
	movl	%esi, -72(%rbp)
.L956:
	movb	$1, -80(%rbp)
	movq	32(%r15), %rdx
	movslq	%esi, %rsi
	movabsq	$-6148914691236517205, %rdi
	movq	24(%r15), %rcx
	movq	%rdx, %rax
	subq	%rcx, %rax
	sarq	$2, %rax
	imulq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L1247
	jnb	.L979
	leaq	(%rsi,%rsi,2), %rax
	leaq	(%rcx,%rax,4), %rax
	cmpq	%rax, %rdx
	je	.L979
	movq	%rax, 32(%r15)
.L979:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L680
	call	_ZdlPv@PLT
	jmp	.L680
	.p2align 4,,10
	.p2align 3
.L999:
	leaq	8(%r15), %rcx
	leaq	4(%r15), %rdx
	movq	%r15, %rsi
	leaq	-104(%rbp), %rdi
	call	_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE12emplace_backIJRiS6_RjEEEvDpOT_
	movl	-76(%rbp), %eax
	movq	-112(%rbp), %r15
	addl	$1, %eax
	jmp	.L961
	.p2align 4,,10
	.p2align 3
.L959:
	movl	%ecx, -72(%rbp)
	jmp	.L960
	.p2align 4,,10
	.p2align 3
.L904:
	movl	%ecx, -72(%rbp)
	jmp	.L905
	.p2align 4,,10
	.p2align 3
.L910:
	cmpb	$0, -80(%rbp)
	movslq	-72(%rbp), %rax
	jne	.L901
	testl	%r9d, %r9d
	js	.L912
	cmpl	%eax, %r9d
	je	.L912
	movq	(%r10), %rdx
	leaq	(%rax,%rax,2), %rax
	leaq	(%rdi,%rax,4), %rax
	movq	%rdx, (%rax)
	movl	8(%r10), %edx
	movl	%edx, 8(%rax)
	movl	-72(%rbp), %eax
	movq	-112(%rbp), %rsi
.L912:
	addl	$1, %eax
	movl	%eax, -72(%rbp)
	jmp	.L901
	.p2align 4,,10
	.p2align 3
.L1240:
	testb	%r11b, %r11b
	jne	.L926
	movslq	-72(%rbp), %rcx
	testl	%edx, %edx
	js	.L925
	cmpl	%ecx, %edx
	je	.L925
	movq	(%r9,%r10), %rdx
	leaq	(%rcx,%rcx,2), %rax
	leaq	(%r9,%rax,4), %rax
	movq	%rdx, (%rax)
	movl	8(%r9,%r10), %edx
	movl	%edx, 8(%rax)
	movl	-76(%rbp), %edx
	movl	-72(%rbp), %ecx
	movq	-112(%rbp), %rsi
	leal	1(%rdx), %eax
.L925:
	addl	$1, %ecx
	movl	%ecx, -72(%rbp)
	jmp	.L926
	.p2align 4,,10
	.p2align 3
.L1241:
	movl	-72(%rbp), %eax
	jmp	.L928
	.p2align 4,,10
	.p2align 3
.L945:
	leal	1(%rdx), %eax
	movl	%eax, -72(%rbp)
	jmp	.L946
	.p2align 4,,10
	.p2align 3
.L931:
	leal	1(%rax), %edx
	movl	%edx, -72(%rbp)
	jmp	.L932
	.p2align 4,,10
	.p2align 3
.L974:
	addl	$1, %eax
	movl	%eax, -72(%rbp)
	jmp	.L975
	.p2align 4,,10
	.p2align 3
.L917:
	leal	1(%rax), %edx
	movl	%edx, -72(%rbp)
	jmp	.L918
	.p2align 4,,10
	.p2align 3
.L887:
	leal	1(%rax), %edx
	movl	%edx, -72(%rbp)
	jmp	.L888
	.p2align 4,,10
	.p2align 3
.L1228:
	movl	-72(%rbp), %eax
	jmp	.L873
	.p2align 4,,10
	.p2align 3
.L1236:
	movl	-72(%rbp), %ecx
	movq	%r15, %rsi
	xorl	%eax, %eax
	movl	$-1, %edx
	jmp	.L925
	.p2align 4,,10
	.p2align 3
.L1212:
	movl	-72(%rbp), %edx
	movq	%r15, %rsi
	xorl	%r9d, %r9d
	movl	$-1, %eax
	jmp	.L810
	.p2align 4,,10
	.p2align 3
.L1206:
	movb	$1, -80(%rbp)
	movq	%r15, %rsi
	movl	$-1, %eax
	jmp	.L776
	.p2align 4,,10
	.p2align 3
.L1211:
	subq	%rax, %rdx
	leaq	24(%rsi), %rdi
	movq	%rdx, %rsi
	call	_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE17_M_default_appendEm
	jmp	.L807
	.p2align 4,,10
	.p2align 3
.L1219:
	subq	%rdx, %rax
	leaq	24(%rsi), %rdi
	movq	%rax, %rsi
	call	_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE17_M_default_appendEm
	jmp	.L838
	.p2align 4,,10
	.p2align 3
.L1235:
	subq	%rdx, %rax
	leaq	24(%rsi), %rdi
	movq	%rax, %rsi
	call	_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE17_M_default_appendEm
	jmp	.L922
	.p2align 4,,10
	.p2align 3
.L1243:
	subq	%rdx, %rax
	leaq	24(%rsi), %rdi
	movq	%rax, %rsi
	call	_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE17_M_default_appendEm
	jmp	.L950
	.p2align 4,,10
	.p2align 3
.L1230:
	subq	%rdx, %rax
	leaq	24(%rsi), %rdi
	movq	%rax, %rsi
	call	_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE17_M_default_appendEm
	jmp	.L892
	.p2align 4,,10
	.p2align 3
.L1247:
	subq	%rax, %rsi
	leaq	24(%r15), %rdi
	call	_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE17_M_default_appendEm
	jmp	.L979
	.p2align 4,,10
	.p2align 3
.L1201:
	movq	%r15, -112(%rbp)
	xorl	%eax, %eax
	movq	%r15, %rsi
	movabsq	$-6148914691236517205, %rbx
	movq	$0, -104(%rbp)
	movq	$0, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	$-1, -76(%rbp)
	movw	%ax, -80(%rbp)
	movl	$-1, %eax
	.p2align 4,,10
	.p2align 3
.L995:
	subq	%r14, %r13
	leal	1(%rax), %ecx
	sarq	$2, %r13
	imulq	%rbx, %r13
	cmpl	%r13d, %ecx
	jge	.L1248
	cmpb	$0, -79(%rbp)
	jne	.L729
	movslq	-72(%rbp), %rdx
	testl	%eax, %eax
	js	.L730
	cmpl	%edx, %eax
	je	.L731
	cltq
	leaq	(%rdx,%rdx,2), %rdx
	leaq	(%rax,%rax,2), %rax
	leaq	(%r14,%rdx,4), %rdx
	leaq	(%r14,%rax,4), %rax
	movq	(%rax), %rcx
	movq	%rcx, (%rdx)
	movl	8(%rax), %eax
	movl	%eax, 8(%rdx)
	movl	-76(%rbp), %eax
	movq	-112(%rbp), %rsi
	movl	-72(%rbp), %edx
.L730:
	addl	$1, %edx
	movl	%edx, -72(%rbp)
	cmpl	$-1, %eax
	je	.L1023
.L732:
	cmpb	$0, -79(%rbp)
	je	.L734
	addl	$1, %eax
.L733:
	movq	-96(%rbp), %rcx
	movslq	%eax, %rdx
	movq	-104(%rbp), %r8
	movb	$0, -79(%rbp)
	movl	%eax, -76(%rbp)
	movq	24(%rsi), %r14
	leaq	(%rdx,%rdx,2), %rdx
	leaq	(%r14,%rdx,4), %rdi
	movq	%rcx, %rdx
	subq	%r8, %rdx
	sarq	$2, %rdx
	imulq	%rbx, %rdx
	cmpq	$1, %rdx
	jbe	.L735
	subq	$12, %rcx
	jmp	.L736
	.p2align 4,,10
	.p2align 3
.L1249:
	subq	%r8, %rdx
	movq	%rcx, -96(%rbp)
	subq	$12, %rcx
	sarq	$2, %rdx
	imulq	%rbx, %rdx
	cmpq	$1, %rdx
	jbe	.L735
.L736:
	movl	(%rdi), %r11d
	movq	%rcx, %rdx
	cmpl	%r11d, 4(%rcx)
	jle	.L1249
.L735:
	movl	8(%rdi), %edx
	testl	%edx, %edx
	jne	.L994
	movq	32(%rsi), %r13
	jmp	.L995
	.p2align 4,,10
	.p2align 3
.L1248:
	cmpb	$0, -80(%rbp)
	jne	.L725
	cmpb	$0, -79(%rbp)
	jne	.L725
	movslq	-72(%rbp), %rdx
	testl	%eax, %eax
	js	.L726
	cmpl	%edx, %eax
	je	.L726
	cltq
	leaq	(%rdx,%rdx,2), %rdx
	leaq	(%rax,%rax,2), %rax
	leaq	(%r14,%rdx,4), %rdx
	leaq	(%r14,%rax,4), %rax
	movq	(%rax), %rcx
	movq	%rcx, (%rdx)
	movl	8(%rax), %eax
	movl	%eax, 8(%rdx)
	movl	-76(%rbp), %eax
	movl	-72(%rbp), %edx
	movq	-112(%rbp), %rsi
	leal	1(%rax), %ecx
.L726:
	addl	$1, %edx
	movl	%edx, -72(%rbp)
.L725:
	movabsq	$-6148914691236517205, %rbx
	movb	$1, -80(%rbp)
	movq	24(%rsi), %rdi
	movq	32(%rsi), %rdx
	subq	%rdi, %rdx
	sarq	$2, %rdx
	imulq	%rbx, %rdx
	cmpl	%edx, %ecx
	jge	.L1250
	cmpb	$0, -79(%rbp)
	jne	.L743
.L1148:
	movslq	-72(%rbp), %rdx
	testl	%eax, %eax
	js	.L744
	cmpl	%edx, %eax
	je	.L745
	cltq
	leaq	(%rdx,%rdx,2), %rdx
	leaq	(%rax,%rax,2), %rax
	leaq	(%rdi,%rdx,4), %rdx
	leaq	(%rdi,%rax,4), %rax
	movq	(%rax), %rcx
	movq	%rcx, (%rdx)
	movl	8(%rax), %eax
	movl	%eax, 8(%rdx)
	movl	-76(%rbp), %eax
	movq	-112(%rbp), %rsi
	movl	-72(%rbp), %edx
.L744:
	addl	$1, %edx
	movl	%edx, -72(%rbp)
	cmpl	$-1, %eax
	je	.L1022
.L746:
	cmpb	$0, -79(%rbp)
	jne	.L747
	cltq
	leaq	-104(%rbp), %rdi
	leaq	(%rax,%rax,2), %rdx
	movq	24(%rsi), %rax
	leaq	(%rax,%rdx,4), %rsi
	call	_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE12emplace_backIJRS2_EEEvDpOT_
	movl	-76(%rbp), %eax
	movq	-112(%rbp), %rsi
.L747:
	leal	1(%rax), %r10d
	movq	-96(%rbp), %rcx
	movq	-104(%rbp), %r9
	movb	$0, -79(%rbp)
	movl	%r10d, -76(%rbp)
	movslq	%r10d, %rdx
	movq	24(%rsi), %rdi
	leaq	(%rdx,%rdx,2), %rdx
	leaq	(%rdi,%rdx,4), %r11
	movq	%rcx, %rdx
	subq	%r9, %rdx
	sarq	$2, %rdx
	imulq	%rbx, %rdx
	cmpq	$1, %rdx
	jbe	.L748
	subq	$12, %rcx
	jmp	.L749
	.p2align 4,,10
	.p2align 3
.L1251:
	subq	%r9, %rdx
	movq	%rcx, -96(%rbp)
	subq	$12, %rcx
	sarq	$2, %rdx
	imulq	%rbx, %rdx
	cmpq	$1, %rdx
	jbe	.L748
.L749:
	movl	(%r11), %r14d
	movq	%rcx, %rdx
	cmpl	%r14d, 4(%rcx)
	jle	.L1251
.L748:
	movq	32(%rsi), %rdx
	addl	$2, %eax
	subq	%rdi, %rdx
	sarq	$2, %rdx
	imulq	%rbx, %rdx
	cmpl	%edx, %eax
	jge	.L738
	movl	%r10d, %eax
	jmp	.L1148
	.p2align 4,,10
	.p2align 3
.L743:
	cmpl	$-1, %eax
	jne	.L747
.L1022:
	leaq	8(%rsi), %rcx
	leaq	4(%rsi), %rdx
	leaq	-104(%rbp), %rdi
	call	_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE12emplace_backIJRiS6_RjEEEvDpOT_
	movl	-76(%rbp), %eax
	movq	-112(%rbp), %rsi
	jmp	.L747
	.p2align 4,,10
	.p2align 3
.L729:
	cmpl	$-1, %eax
	je	.L1023
	movl	%ecx, %eax
	jmp	.L733
	.p2align 4,,10
	.p2align 3
.L734:
	cltq
	leaq	-104(%rbp), %rdi
	leaq	(%rax,%rax,2), %rdx
	movq	24(%rsi), %rax
	leaq	(%rax,%rdx,4), %rsi
	call	_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE12emplace_backIJRS2_EEEvDpOT_
	movl	-76(%rbp), %eax
	movq	-112(%rbp), %rsi
	addl	$1, %eax
	jmp	.L733
	.p2align 4,,10
	.p2align 3
.L1023:
	leaq	8(%rsi), %rcx
	leaq	4(%rsi), %rdx
	leaq	-104(%rbp), %rdi
	call	_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE12emplace_backIJRiS6_RjEEEvDpOT_
	movl	-76(%rbp), %eax
	movq	-112(%rbp), %rsi
	addl	$1, %eax
	jmp	.L733
	.p2align 4,,10
	.p2align 3
.L1250:
	movl	-72(%rbp), %eax
.L728:
	movabsq	$-6148914691236517205, %r9
	movb	$1, -80(%rbp)
	cltq
	movq	32(%rsi), %rcx
	movq	24(%rsi), %rdi
	movq	%rcx, %rdx
	subq	%rdi, %rdx
	sarq	$2, %rdx
	imulq	%r9, %rdx
	cmpq	%rdx, %rax
	ja	.L1252
	jnb	.L750
	leaq	(%rax,%rax,2), %rax
	leaq	(%rdi,%rax,4), %rax
	cmpq	%rax, %rcx
	je	.L750
	movq	%rax, 32(%rsi)
.L750:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L751
	call	_ZdlPv@PLT
.L751:
	movq	24(%r15), %r14
	movq	32(%r15), %r13
	jmp	.L723
	.p2align 4,,10
	.p2align 3
.L731:
	movl	%ecx, -72(%rbp)
	jmp	.L732
	.p2align 4,,10
	.p2align 3
.L738:
	cmpb	$0, -80(%rbp)
	movslq	-72(%rbp), %rax
	jne	.L728
	testl	%r10d, %r10d
	js	.L740
	cmpl	%eax, %r10d
	je	.L740
	movq	(%r11), %rdx
	leaq	(%rax,%rax,2), %rax
	leaq	(%rdi,%rax,4), %rax
	movq	%rdx, (%rax)
	movl	8(%r11), %edx
	movl	%edx, 8(%rax)
	movl	-72(%rbp), %eax
	movq	-112(%rbp), %rsi
.L740:
	addl	$1, %eax
	movl	%eax, -72(%rbp)
	jmp	.L728
	.p2align 4,,10
	.p2align 3
.L994:
	movl	$1, 8(%rdi)
	movl	-76(%rbp), %eax
	movq	24(%rsi), %r14
	movq	32(%rsi), %r13
	jmp	.L995
	.p2align 4,,10
	.p2align 3
.L841:
	cmpq	%r12, %rbx
	je	.L852
	movq	%r15, -144(%rbp)
	movq	%rbx, %r15
	jmp	.L866
	.p2align 4,,10
	.p2align 3
.L1255:
	cmpq	%r12, %r14
	je	.L861
	movl	$12, %eax
	movq	%r12, %rdx
	movq	%r14, %rsi
	movl	%ecx, -136(%rbp)
	subq	%r14, %rdx
	leaq	(%r14,%rax), %rdi
	call	memmove@PLT
	movl	-136(%rbp), %ecx
.L861:
	movl	%r13d, (%r14)
	movl	%ebx, 4(%r14)
	movl	%ecx, 8(%r14)
.L862:
	addq	$12, %r12
	cmpq	%r12, %r15
	je	.L1253
.L866:
	movl	(%r12), %r13d
	movl	(%r14), %eax
	movl	4(%r12), %ebx
	cmpl	%eax, %r13d
	setl	%dl
	je	.L1254
.L859:
	movl	8(%r12), %ecx
	testb	%dl, %dl
	jne	.L1255
	movq	%r12, %rax
	jmp	.L860
	.p2align 4,,10
	.p2align 3
.L863:
	cmpl	%edx, %r13d
	jge	.L865
.L864:
	movq	(%rax), %rdx
	movq	%rdx, 12(%rax)
	movl	8(%rax), %edx
	movl	%edx, 20(%rax)
.L860:
	movl	-12(%rax), %edx
	movq	%rax, %rsi
	subq	$12, %rax
	cmpl	%edx, %r13d
	jne	.L863
	cmpl	%ebx, -8(%rsi)
	jl	.L864
.L865:
	movl	%r13d, (%rsi)
	movl	%ebx, 4(%rsi)
	movl	%ecx, 8(%rsi)
	jmp	.L862
	.p2align 4,,10
	.p2align 3
.L1253:
	movq	-144(%rbp), %r15
.L852:
	movq	24(%r15), %rdx
	movq	32(%r15), %rax
	xorl	%r11d, %r11d
	movq	%r15, -112(%rbp)
	movabsq	$-6148914691236517205, %r12
	movq	$0, -104(%rbp)
	subq	%rdx, %rax
	movq	$0, -96(%rbp)
	sarq	$2, %rax
	movq	$0, -88(%rbp)
	imulq	%r12, %rax
	movw	%r11w, -80(%rbp)
	movq	$-1, -76(%rbp)
	testl	%eax, %eax
	jle	.L868
	movq	%r15, %rsi
	movl	$-1, %ecx
	leaq	-104(%rbp), %rbx
	.p2align 4,,10
	.p2align 3
.L1121:
	movslq	-72(%rbp), %rax
	testl	%ecx, %ecx
	js	.L874
	cmpl	%eax, %ecx
	je	.L874
	movslq	%ecx, %rcx
	leaq	(%rax,%rax,2), %rax
	leaq	(%rcx,%rcx,2), %rcx
	leaq	(%rdx,%rax,4), %rax
	leaq	(%rdx,%rcx,4), %rdx
	movq	(%rdx), %rcx
	movq	%rcx, (%rax)
	movl	8(%rdx), %edx
	movl	%edx, 8(%rax)
	movq	-112(%rbp), %rsi
	movl	-72(%rbp), %eax
.L874:
	addl	$1, %eax
	movl	%eax, -72(%rbp)
	movslq	-76(%rbp), %rax
	cmpl	$-1, %eax
	je	.L870
	cmpb	$0, -79(%rbp)
	jne	.L871
	leaq	(%rax,%rax,2), %rdx
	movq	24(%rsi), %rax
	movq	%rbx, %rdi
	leaq	(%rax,%rdx,4), %rsi
	call	_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE12emplace_backIJRS2_EEEvDpOT_
	movl	-76(%rbp), %eax
	movq	-112(%rbp), %rsi
.L871:
	leal	1(%rax), %ecx
	movb	$0, -79(%rbp)
	movq	-104(%rbp), %r11
	movslq	%ecx, %rdi
	movl	%ecx, -76(%rbp)
	movq	24(%rsi), %rdx
	leaq	(%rdi,%rdi,2), %r10
	movq	-96(%rbp), %rdi
	salq	$2, %r10
	leaq	(%rdx,%r10), %r9
	jmp	.L1189
	.p2align 4,,10
	.p2align 3
.L1256:
	movl	(%r9), %r14d
	cmpl	%r14d, -8(%rdi)
	jg	.L877
	subq	$12, %rdi
	movq	%rdi, -96(%rbp)
.L1189:
	movq	%rdi, %r8
	subq	%r11, %r8
	sarq	$2, %r8
	imulq	%r12, %r8
	cmpq	$1, %r8
	ja	.L1256
.L877:
	movq	32(%rsi), %rdi
	addl	$2, %eax
	subq	%rdx, %rdi
	sarq	$2, %rdi
	imulq	%r12, %rdi
	cmpl	%edi, %eax
	jge	.L1257
	leaq	12(%rdx,%r10), %rax
	movl	(%rax), %edi
	cmpl	%edi, (%r9)
	jne	.L1121
	movl	4(%rax), %edi
	cmpl	%edi, 4(%r9)
	jne	.L1121
	movl	8(%rax), %edx
	cmpl	%edx, 8(%r9)
	cmovnb	8(%r9), %edx
	movl	%edx, 8(%rax)
	movslq	-76(%rbp), %rax
	movb	$1, -79(%rbp)
	movq	32(%rsi), %rdx
	subq	24(%rsi), %rdx
	leal	1(%rax), %edi
	sarq	$2, %rdx
	imulq	%r12, %rdx
	cmpl	%edx, %edi
	jge	.L895
	cmpl	$-1, %eax
	jne	.L871
.L870:
	leaq	8(%rsi), %rcx
	leaq	4(%rsi), %rdx
	movq	%rbx, %rdi
	call	_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE12emplace_backIJRiS6_RjEEEvDpOT_
	movl	-76(%rbp), %eax
	movq	-112(%rbp), %rsi
	jmp	.L871
	.p2align 4,,10
	.p2align 3
.L1257:
	cmpb	$0, -80(%rbp)
	movslq	-72(%rbp), %rax
	je	.L1008
	jmp	.L873
	.p2align 4,,10
	.p2align 3
.L1220:
	movq	%rbx, %r13
	movq	-144(%rbp), %rbx
	movq	-152(%rbp), %r15
	leaq	-112(%rbp), %rdi
	movq	%r13, %r10
	cmpq	%r13, %rbx
	je	.L852
	.p2align 4,,10
	.p2align 3
.L855:
	movq	(%r10), %rax
	leaq	-12(%r10), %rcx
	movq	%rax, -112(%rbp)
	movl	8(%r10), %eax
	movl	%eax, -104(%rbp)
	jmp	.L854
	.p2align 4,,10
	.p2align 3
.L1258:
	movq	(%rcx), %rax
	subq	$12, %rcx
	movq	%rax, 24(%rcx)
	movl	20(%rcx), %eax
	movl	%eax, 32(%rcx)
.L854:
	movq	%rcx, %rsi
	leaq	12(%rcx), %r9
	call	_ZN2v88internal12_GLOBAL__N_120CompareCoverageBlockERKNS0_13CoverageBlockES4_
	testb	%al, %al
	jne	.L1258
	movq	-112(%rbp), %rax
	addq	$12, %r10
	movq	%rax, (%r9)
	movl	-104(%rbp), %eax
	movl	%eax, 8(%r9)
	cmpq	%r10, %rbx
	jne	.L855
	jmp	.L852
	.p2align 4,,10
	.p2align 3
.L993:
	cmpl	$-2, 4(%rdi)
	jne	.L757
	movl	8(%rdi), %edx
	movzbl	-153(%rbp), %ecx
	movb	$1, -79(%rbp)
	movl	%edx, 8(%r15)
	movq	24(%rsi), %r9
	jmp	.L757
	.p2align 4,,10
	.p2align 3
.L1223:
	leaq	192(%r14), %rbx
	movq	%r13, -168(%rbp)
	movq	%r15, -176(%rbp)
	movq	%rbx, %r13
	movq	%r12, %rbx
	jmp	.L704
	.p2align 4,,10
	.p2align 3
.L1261:
	cmpq	%r14, %rbx
	je	.L699
	movl	$12, %eax
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movl	%ecx, -144(%rbp)
	subq	%r14, %rdx
	leaq	(%r14,%rax), %rdi
	call	memmove@PLT
	movl	-144(%rbp), %ecx
.L699:
	movl	%r12d, (%r14)
	movl	%ecx, 4(%r14)
	movl	%r15d, 8(%r14)
.L700:
	addq	$12, %rbx
	cmpq	%rbx, %r13
	je	.L1259
.L704:
	movl	(%rbx), %r12d
	movl	(%r14), %eax
	movl	4(%rbx), %ecx
	cmpl	%eax, %r12d
	setl	%dl
	je	.L1260
.L697:
	movl	8(%rbx), %r15d
	testb	%dl, %dl
	jne	.L1261
	movq	%rbx, %rax
	jmp	.L698
	.p2align 4,,10
	.p2align 3
.L701:
	cmpl	%edx, %r12d
	jge	.L703
.L702:
	movq	(%rax), %rdx
	movq	%rdx, 12(%rax)
	movl	8(%rax), %edx
	movl	%edx, 20(%rax)
.L698:
	movl	-12(%rax), %edx
	movq	%rax, %rsi
	subq	$12, %rax
	cmpl	%edx, %r12d
	jne	.L701
	cmpl	%ecx, -8(%rsi)
	jl	.L702
.L703:
	movl	%r12d, (%rsi)
	movl	%ecx, 4(%rsi)
	movl	%r15d, 8(%rsi)
	jmp	.L700
	.p2align 4,,10
	.p2align 3
.L1254:
	cmpl	%ebx, 4(%r14)
	setl	%dl
	jmp	.L859
	.p2align 4,,10
	.p2align 3
.L1260:
	cmpl	%ecx, 4(%r14)
	setl	%dl
	jmp	.L697
	.p2align 4,,10
	.p2align 3
.L1225:
	cmpl	%r13d, 4(%r14)
	setl	%dl
	jmp	.L713
	.p2align 4,,10
	.p2align 3
.L745:
	leal	1(%rax), %edx
	movl	%edx, -72(%rbp)
	jmp	.L746
	.p2align 4,,10
	.p2align 3
.L1259:
	movq	%r13, %rbx
	movq	-168(%rbp), %r13
	movq	-176(%rbp), %r15
	leaq	-112(%rbp), %rdi
	cmpq	%r13, %rbx
	je	.L683
	.p2align 4,,10
	.p2align 3
.L709:
	movq	(%rbx), %rax
	leaq	-12(%rbx), %rcx
	movq	%rax, -112(%rbp)
	movl	8(%rbx), %eax
	movl	%eax, -104(%rbp)
	jmp	.L708
	.p2align 4,,10
	.p2align 3
.L1262:
	movq	(%rcx), %rax
	subq	$12, %rcx
	movq	%rax, 24(%rcx)
	movl	20(%rcx), %eax
	movl	%eax, 32(%rcx)
.L708:
	movq	%rcx, %rsi
	leaq	12(%rcx), %r9
	call	_ZN2v88internal12_GLOBAL__N_120CompareCoverageBlockERKNS0_13CoverageBlockES4_
	testb	%al, %al
	jne	.L1262
	movq	-112(%rbp), %rax
	addq	$12, %rbx
	movq	%rax, (%r9)
	movl	-104(%rbp), %eax
	movl	%eax, 8(%r9)
	cmpq	%rbx, %r13
	jne	.L709
	jmp	.L683
	.p2align 4,,10
	.p2align 3
.L1029:
	movq	%rdi, %r14
	jmp	.L690
	.p2align 4,,10
	.p2align 3
.L1252:
	subq	%rdx, %rax
	leaq	24(%rsi), %rdi
	movq	%rax, %rsi
	call	_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE17_M_default_appendEm
	jmp	.L750
.L1197:
	call	__stack_chk_fail@PLT
.L1199:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1227:
	movabsq	$768614336404564650, %rax
	cmpq	%rax, %r8
	movq	%rax, %rdi
	cmovbe	%r8, %rdi
	imulq	$12, %rdi, %rdi
	jmp	.L688
	.cfi_endproc
.LFE19916:
	.size	_ZN2v88internal12_GLOBAL__N_128CollectBlockCoverageInternalEPNS0_16CoverageFunctionENS0_18SharedFunctionInfoENS_5debug12CoverageModeE, .-_ZN2v88internal12_GLOBAL__N_128CollectBlockCoverageInternalEPNS0_16CoverageFunctionENS0_18SharedFunctionInfoENS_5debug12CoverageModeE
	.section	.rodata._ZN2v88internal8Coverage7CollectEPNS0_7IsolateENS_5debug12CoverageModeE.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"vector::_M_range_check: __n (which is %zu) >= this->size() (which is %zu)"
	.section	.text._ZN2v88internal8Coverage7CollectEPNS0_7IsolateENS_5debug12CoverageModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Coverage7CollectEPNS0_7IsolateENS_5debug12CoverageModeE
	.type	_ZN2v88internal8Coverage7CollectEPNS0_7IsolateENS_5debug12CoverageModeE, @function
_ZN2v88internal8Coverage7CollectEPNS0_7IsolateENS_5debug12CoverageModeE:
.LFB19936:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$328, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -328(%rbp)
	movl	$192, %edi
	movq	%rsi, -280(%rbp)
	movl	%edx, -308(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	malloc@PLT
	movq	%rax, -208(%rbp)
	testq	%rax, %rax
	je	.L1371
	movl	-308(%rbp), %r15d
	movq	-280(%rbp), %rbx
	movb	$0, 16(%rax)
	leaq	-208(%rbp), %rsi
	movb	$0, 40(%rax)
	movb	$0, 64(%rax)
	movl	%r15d, %edx
	movq	%rbx, %rdi
	movb	$0, 88(%rax)
	movb	$0, 112(%rax)
	movb	$0, 136(%rax)
	movb	$0, 160(%rax)
	movb	$0, 184(%rax)
	movq	$8, -200(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_126CollectAndMaybeResetCountsEPNS0_7IsolateEPNS0_18SharedToCounterMapENS_5debug12CoverageModeE
	movl	$24, %edi
	call	_Znwm@PLT
	movq	-328(%rbp), %rcx
	pxor	%xmm0, %xmm0
	movq	%rbx, %rsi
	movq	$0, 16(%rax)
	movq	%rax, (%rcx)
	movups	%xmm0, (%rax)
	leaq	-224(%rbp), %rax
	movq	%rax, %rbx
	movq	%rax, %rdi
	movq	%rax, -360(%rbp)
	call	_ZN2v88internal6Script8IteratorC1EPNS0_7IsolateE@PLT
	movq	%rbx, %rdi
	leal	-3(%r15), %ebx
	call	_ZN2v88internal6Script8Iterator4NextEv@PLT
	leal	-2(%r15), %edx
	movl	%ebx, -296(%rbp)
	leaq	-256(%rbp), %rbx
	andl	$-3, %edx
	movq	%rax, -256(%rbp)
	movq	%rbx, -352(%rbp)
	movl	%edx, -312(%rbp)
	testq	%rax, %rax
	jne	.L1267
	jmp	.L1268
	.p2align 4,,10
	.p2align 3
.L1332:
	movq	-360(%rbp), %rdi
	call	_ZN2v88internal6Script8Iterator4NextEv@PLT
	movq	%rax, -256(%rbp)
	testq	%rax, %rax
	je	.L1268
.L1267:
	movq	-352(%rbp), %rdi
	call	_ZN2v88internal6Script16IsUserJavaScriptEv@PLT
	movb	%al, -290(%rbp)
	testb	%al, %al
	je	.L1332
	movq	-280(%rbp), %rax
	movq	-256(%rbp), %r13
	movq	41112(%rax), %rdi
	testq	%rdi, %rdi
	je	.L1269
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L1270:
	movq	-328(%rbp), %rbx
	movq	%rax, -248(%rbp)
	movq	(%rbx), %rdi
	movq	8(%rdi), %rsi
	cmpq	16(%rdi), %rsi
	je	.L1272
	movq	%rax, (%rsi)
	movq	%rbx, %rax
	movq	$0, 8(%rsi)
	movq	$0, 16(%rsi)
	movq	$0, 24(%rsi)
	addq	$32, 8(%rdi)
.L1273:
	movq	(%rax), %rax
	pxor	%xmm0, %xmm0
	movq	-280(%rbp), %rsi
	movq	8(%rax), %r15
	movaps	%xmm0, -176(%rbp)
	movq	$0, -160(%rbp)
	leaq	-24(%r15), %rax
	movq	%rax, -320(%rbp)
	movq	-248(%rbp), %rax
	movq	(%rax), %rdx
	leaq	-144(%rbp), %rax
	movq	%rax, %rbx
	movq	%rax, %rdi
	movq	%rax, -344(%rbp)
	call	_ZN2v88internal18SharedFunctionInfo14ScriptIteratorC1EPNS0_7IsolateENS0_6ScriptE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal18SharedFunctionInfo14ScriptIterator4NextEv@PLT
	movq	-344(%rbp), %rbx
	movq	%rax, -240(%rbp)
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1285
	.p2align 4,,10
	.p2align 3
.L1274:
	movl	-200(%rbp), %eax
	movq	-208(%rbp), %rdi
	leal	-1(%rax), %esi
	movl	%esi, %eax
	andl	%ecx, %eax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rdi,%rdx,8), %rdx
	cmpb	$0, 16(%rdx)
	jne	.L1279
	jmp	.L1335
	.p2align 4,,10
	.p2align 3
.L1372:
	addq	$1, %rax
	andq	%rsi, %rax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rdi,%rdx,8), %rdx
	cmpb	$0, 16(%rdx)
	je	.L1335
.L1279:
	cmpq	%rcx, (%rdx)
	jne	.L1372
	movl	8(%rdx), %eax
.L1277:
	movl	%eax, -260(%rbp)
	movq	-168(%rbp), %r12
	cmpq	-160(%rbp), %r12
	je	.L1280
	movq	%rcx, -232(%rbp)
	leaq	-112(%rbp), %r14
	movq	%rcx, (%r12)
	movl	%eax, 8(%r12)
	movq	-232(%rbp), %rax
	movq	%rax, -112(%rbp)
	movzwl	45(%rax), %r13d
	cmpl	$65535, %r13d
	je	.L1283
	movq	%r14, %rdi
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	subl	%r13d, %eax
	cmpl	$-1, %eax
	je	.L1283
.L1282:
	movl	%eax, 12(%r12)
	leaq	-232(%rbp), %rdi
	call	_ZNK2v88internal18SharedFunctionInfo11EndPositionEv@PLT
	addq	$24, -168(%rbp)
	movl	%eax, 16(%r12)
.L1284:
	movq	%rbx, %rdi
	call	_ZN2v88internal18SharedFunctionInfo14ScriptIterator4NextEv@PLT
	movq	%rax, -240(%rbp)
	movq	%rax, %rcx
	testq	%rax, %rax
	jne	.L1274
.L1285:
	movq	-168(%rbp), %rbx
	movq	-176(%rbp), %r13
	cmpq	%rbx, %r13
	je	.L1373
	movq	%rbx, %r14
	movl	$63, %edx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movabsq	$-6148914691236517205, %rcx
	subq	%r13, %r14
	movq	%r14, %rax
	sarq	$3, %rax
	imulq	%rcx, %rax
	bsrq	%rax, %rax
	xorq	$63, %rax
	subl	%eax, %edx
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	call	_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPN2v88internal12_GLOBAL__N_126SharedFunctionInfoAndCountESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_less_iterEEvT_SD_T0_T1_.isra.0
	cmpq	$384, %r14
	jle	.L1286
	leaq	384(%r13), %r14
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal12_GLOBAL__N_126SharedFunctionInfoAndCountESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_less_iterEEvT_SD_T0_.isra.0
	movq	%r14, %rsi
	cmpq	%r14, %rbx
	je	.L1288
.L1294:
	movdqu	(%rsi), %xmm2
	movaps	%xmm2, -112(%rbp)
	movq	16(%rsi), %rax
	movq	%rax, -96(%rbp)
	movq	%rsi, %rax
	movl	8(%rsi), %r9d
	movl	12(%rsi), %edi
	movl	16(%rsi), %r8d
	jmp	.L1293
	.p2align 4,,10
	.p2align 3
.L1374:
	setg	%dl
.L1290:
	subq	$24, %rax
	testb	%dl, %dl
	je	.L1292
.L1375:
	movl	16(%rax), %edx
	movdqu	(%rax), %xmm1
	movl	%edx, 40(%rax)
	movups	%xmm1, 24(%rax)
.L1293:
	movq	%rax, %rcx
	cmpl	%edi, -12(%rax)
	jne	.L1374
	cmpl	%r8d, -8(%rax)
	je	.L1291
	setl	%dl
	subq	$24, %rax
	testb	%dl, %dl
	jne	.L1375
.L1292:
	movl	%r9d, -104(%rbp)
	addq	$24, %rsi
	movl	%edi, -100(%rbp)
	movdqa	-112(%rbp), %xmm3
	movl	%r8d, -96(%rbp)
	movups	%xmm3, (%rcx)
	movl	-96(%rbp), %eax
	movl	%eax, 16(%rcx)
	cmpq	%rsi, %rbx
	jne	.L1294
.L1288:
	movq	-168(%rbp), %rax
	movq	-176(%rbp), %rbx
	pxor	%xmm0, %xmm0
	movq	$0, -128(%rbp)
	movaps	%xmm0, -144(%rbp)
	movq	%rax, -304(%rbp)
	cmpq	%rbx, %rax
	je	.L1296
	xorl	%r10d, %r10d
	xorl	%eax, %eax
	movq	%r15, %r14
	movabsq	$7905747460161236407, %r12
	.p2align 4,,10
	.p2align 3
.L1322:
	movq	(%rbx), %rdx
	movq	%rdx, -240(%rbp)
	movl	16(%rbx), %edi
	movl	12(%rbx), %ecx
	movl	8(%rbx), %r13d
	movl	%edi, -284(%rbp)
	cmpq	%r10, %rax
	jne	.L1297
	jmp	.L1301
	.p2align 4,,10
	.p2align 3
.L1376:
	subq	$8, %rax
	movq	%rax, -136(%rbp)
	cmpq	%r10, %rax
	je	.L1301
.L1297:
	movq	-24(%r14), %rdi
	movq	-16(%r14), %rdx
	movq	-8(%rax), %rsi
	subq	%rdi, %rdx
	sarq	$3, %rdx
	imulq	%r12, %rdx
	cmpq	%rdx, %rsi
	jnb	.L1370
	leaq	0(,%rsi,8), %rdx
	subq	%rsi, %rdx
	cmpl	%ecx, 4(%rdi,%rdx,8)
	jle	.L1376
.L1301:
	testl	%r13d, %r13d
	jne	.L1298
	movb	$0, -289(%rbp)
.L1299:
	leaq	-240(%rbp), %r15
	movl	%ecx, -288(%rbp)
	movq	%r15, %rdi
	call	_ZN2v88internal18SharedFunctionInfo9DebugNameEv@PLT
	movl	-288(%rbp), %ecx
	movq	%rax, %rsi
	movq	-280(%rbp), %rax
	movq	41112(%rax), %rdi
	testq	%rdi, %rdi
	je	.L1303
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-288(%rbp), %ecx
.L1304:
	movl	%ecx, -112(%rbp)
	movl	-284(%rbp), %ecx
	cmpl	$1, -296(%rbp)
	movl	%r13d, -104(%rbp)
	movl	%ecx, -108(%rbp)
	movq	%rax, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	movb	$0, -64(%rbp)
	jbe	.L1377
.L1307:
	movq	-136(%rbp), %rsi
	cmpq	-144(%rbp), %rsi
	je	.L1311
	movq	-24(%r14), %rcx
	movq	-16(%r14), %rdx
	movq	-8(%rsi), %r10
	subq	%rcx, %rdx
	sarq	$3, %rdx
	imulq	%r12, %rdx
	movq	%rdx, %rdi
	cmpq	%rdx, %r10
	jnb	.L1378
	movl	-112(%rbp), %eax
	cmpl	-108(%rbp), %eax
	setl	%dl
	notl	%eax
	shrl	$31, %eax
	andl	%edx, %eax
	leaq	0(,%r10,8), %rdx
	subq	%r10, %rdx
	movl	8(%rcx,%rdx,8), %edx
	testl	%edx, %edx
	je	.L1313
	testb	%al, %al
	jne	.L1314
	movzbl	-290(%rbp), %r15d
	xorl	%edx, %edx
.L1315:
	cmpb	$0, _ZN2v88internal25FLAG_trace_block_coverageE(%rip)
	jne	.L1379
.L1318:
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1319
.L1380:
	call	_ZdlPv@PLT
	addq	$24, %rbx
	cmpq	%rbx, -304(%rbp)
	je	.L1367
.L1320:
	movq	-136(%rbp), %rax
	movq	-144(%rbp), %r10
	jmp	.L1322
	.p2align 4,,10
	.p2align 3
.L1291:
	cmpl	-16(%rax), %r9d
	setb	%dl
	jmp	.L1290
	.p2align 4,,10
	.p2align 3
.L1335:
	xorl	%eax, %eax
	jmp	.L1277
.L1280:
	leaq	-260(%rbp), %rcx
	leaq	-240(%rbp), %rdx
	movq	%r12, %rsi
	leaq	-176(%rbp), %rdi
	call	_ZNSt6vectorIN2v88internal12_GLOBAL__N_126SharedFunctionInfoAndCountESaIS3_EE17_M_realloc_insertIJRNS1_18SharedFunctionInfoEjEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L1284
	.p2align 4,,10
	.p2align 3
.L1311:
	movl	-112(%rbp), %edx
	cmpl	-108(%rbp), %edx
	setl	%al
	notl	%edx
	shrl	$31, %edx
	andl	%edx, %eax
.L1313:
	movq	-88(%rbp), %rcx
	movzbl	-289(%rbp), %r15d
	cmpq	%rcx, -80(%rbp)
	setne	%cl
	xorl	%edx, %edx
	orl	%ecx, %r15d
	testb	%al, %al
	je	.L1315
	testb	%r15b, %r15b
	je	.L1338
	movq	-16(%r14), %rdi
	subq	-24(%r14), %rdi
	sarq	$3, %rdi
	imulq	%r12, %rdi
.L1314:
	movq	%rdi, -232(%rbp)
	cmpq	-128(%rbp), %rsi
	je	.L1316
	movq	%rdi, (%rsi)
	addq	$8, -136(%rbp)
.L1317:
	leaq	-112(%rbp), %rdi
	movq	%rdi, %rsi
	movq	-320(%rbp), %rdi
	call	_ZNSt6vectorIN2v88internal16CoverageFunctionESaIS2_EE12emplace_backIJRS2_EEEvDpOT_
	cmpb	$0, _ZN2v88internal25FLAG_trace_block_coverageE(%rip)
	movl	$1, %edx
	movzbl	-290(%rbp), %r15d
	je	.L1318
.L1379:
	movq	-240(%rbp), %rsi
	leaq	-112(%rbp), %rdi
	movzbl	%r15b, %ecx
	call	_ZN2v88internal12_GLOBAL__N_118PrintBlockCoverageEPKNS0_16CoverageFunctionENS0_18SharedFunctionInfoEbb
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1380
.L1319:
	addq	$24, %rbx
	cmpq	%rbx, -304(%rbp)
	jne	.L1320
.L1367:
	movq	-24(%r14), %rax
	cmpq	%rax, -16(%r14)
	je	.L1333
.L1323:
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1369
	call	_ZdlPv@PLT
.L1369:
	movq	-176(%rbp), %rbx
.L1331:
	testq	%rbx, %rbx
	je	.L1332
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
	jmp	.L1332
	.p2align 4,,10
	.p2align 3
.L1298:
	movl	-312(%rbp), %edi
	testl	%edi, %edi
	je	.L1302
	movzbl	-290(%rbp), %eax
	movl	-308(%rbp), %esi
	movb	%al, -289(%rbp)
	testl	%esi, %esi
	movl	$1, %eax
	cmove	%eax, %r13d
	jmp	.L1299
	.p2align 4,,10
	.p2align 3
.L1303:
	movq	%rax, %rdi
	movq	41088(%rax), %rax
	cmpq	41096(%rdi), %rax
	je	.L1381
.L1305:
	movq	-280(%rbp), %rdi
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rdi)
	movq	%rsi, (%rax)
	jmp	.L1304
	.p2align 4,,10
	.p2align 3
.L1377:
	movq	%r15, %rdi
	call	_ZNK2v88internal18SharedFunctionInfo15HasCoverageInfoEv@PLT
	testb	%al, %al
	je	.L1307
	movq	-240(%rbp), %r13
	movl	-308(%rbp), %edx
	leaq	-112(%rbp), %rdi
	xorl	%r15d, %r15d
	movq	%r13, %rsi
	call	_ZN2v88internal12_GLOBAL__N_128CollectBlockCoverageInternalEPNS0_16CoverageFunctionENS0_18SharedFunctionInfoENS_5debug12CoverageModeE
	movq	31(%r13), %rax
	leaq	-232(%rbp), %r13
	movq	63(%rax), %rax
	movq	%rax, -232(%rbp)
	jmp	.L1310
	.p2align 4,,10
	.p2align 3
.L1382:
	movl	%r15d, %esi
	movq	%r13, %rdi
	addl	$1, %r15d
	call	_ZN2v88internal12CoverageInfo15ResetBlockCountEi@PLT
.L1310:
	movq	%r13, %rdi
	call	_ZNK2v88internal12CoverageInfo9SlotCountEv@PLT
	cmpl	%r15d, %eax
	jg	.L1382
	jmp	.L1307
.L1302:
	movq	-240(%rbp), %rax
	movl	47(%rax), %r9d
	shrl	$27, %r9d
	xorl	$1, %r9d
	movl	%r9d, %esi
	andl	$1, %r9d
	andl	$1, %esi
	movl	%r9d, %r13d
	movb	%sil, -289(%rbp)
	movl	47(%rax), %edx
	orl	$134217728, %edx
	movl	%edx, 47(%rax)
	jmp	.L1299
.L1283:
	movq	%r14, %rdi
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	jmp	.L1282
.L1381:
	movq	%rsi, -336(%rbp)
	movl	%ecx, -288(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-336(%rbp), %rsi
	movl	-288(%rbp), %ecx
	jmp	.L1305
.L1316:
	leaq	-232(%rbp), %r13
	movq	-344(%rbp), %rdi
	movq	%r13, %rdx
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L1317
.L1269:
	movq	%rax, %rbx
	movq	41088(%rax), %rax
	cmpq	%rax, 41096(%rbx)
	je	.L1383
.L1271:
	movq	-280(%rbp), %rbx
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%r13, (%rax)
	jmp	.L1270
.L1286:
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal12_GLOBAL__N_126SharedFunctionInfoAndCountESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_less_iterEEvT_SD_T0_.isra.0
	jmp	.L1288
.L1272:
	leaq	-248(%rbp), %rdx
	call	_ZNSt6vectorIN2v88internal14CoverageScriptESaIS2_EE17_M_realloc_insertIJRNS1_6HandleINS1_6ScriptEEEEEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	movq	-328(%rbp), %rax
	jmp	.L1273
.L1373:
	movq	$0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -144(%rbp)
.L1296:
	movq	-16(%r15), %rax
	cmpq	%rax, -24(%r15)
	jne	.L1331
.L1333:
	movq	-328(%rbp), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rbx
	leaq	-32(%rbx), %rdx
	movq	%rdx, 8(%rax)
	movq	-16(%rbx), %r14
	movq	-24(%rbx), %r13
	cmpq	%r13, %r14
	je	.L1324
.L1328:
	movq	24(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1325
	call	_ZdlPv@PLT
	addq	$56, %r13
	cmpq	%r13, %r14
	jne	.L1328
.L1326:
	movq	-24(%rbx), %r13
.L1324:
	testq	%r13, %r13
	je	.L1323
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	jmp	.L1323
.L1325:
	addq	$56, %r13
	cmpq	%r13, %r14
	jne	.L1328
	jmp	.L1326
.L1378:
	movq	%r10, %rsi
.L1370:
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L1383:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L1271
.L1268:
	movq	-208(%rbp), %rdi
	call	free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1384
	movq	-328(%rbp), %rax
	addq	$328, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1384:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
.L1338:
	movl	$1, %edx
	jmp	.L1315
.L1371:
	leaq	.LC4(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19936:
	.size	_ZN2v88internal8Coverage7CollectEPNS0_7IsolateENS_5debug12CoverageModeE, .-_ZN2v88internal8Coverage7CollectEPNS0_7IsolateENS_5debug12CoverageModeE
	.section	.text._ZN2v88internal8Coverage14CollectPreciseEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Coverage14CollectPreciseEPNS0_7IsolateE
	.type	_ZN2v88internal8Coverage14CollectPreciseEPNS0_7IsolateE, @function
_ZN2v88internal8Coverage14CollectPreciseEPNS0_7IsolateE:
.LFB19934:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	subq	$16, %rsp
	movl	41832(%rsi), %edx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8Coverage7CollectEPNS0_7IsolateENS_5debug12CoverageModeE
	cmpl	$1, 41836(%r12)
	je	.L1385
	movl	41832(%r12), %eax
	subl	$2, %eax
	andl	$-3, %eax
	je	.L1389
.L1385:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1390
	addq	$16, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1389:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9ArrayList3NewEPNS0_7IsolateEi@PLT
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate35SetFeedbackVectorsForProfilingToolsENS0_6ObjectE@PLT
	jmp	.L1385
.L1390:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19934:
	.size	_ZN2v88internal8Coverage14CollectPreciseEPNS0_7IsolateE, .-_ZN2v88internal8Coverage14CollectPreciseEPNS0_7IsolateE
	.section	.text._ZN2v88internal8Coverage17CollectBestEffortEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Coverage17CollectBestEffortEPNS0_7IsolateE
	.type	_ZN2v88internal8Coverage17CollectBestEffortEPNS0_7IsolateE, @function
_ZN2v88internal8Coverage17CollectBestEffortEPNS0_7IsolateE:
.LFB19935:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$312, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -280(%rbp)
	movl	$192, %edi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	malloc@PLT
	movq	%rax, -208(%rbp)
	testq	%rax, %rax
	je	.L1494
	movb	$0, 16(%rax)
	xorl	%edx, %edx
	leaq	-208(%rbp), %rsi
	movq	%r14, %rdi
	movb	$0, 40(%rax)
	movb	$0, 64(%rax)
	movb	$0, 88(%rax)
	movb	$0, 112(%rax)
	movb	$0, 136(%rax)
	movb	$0, 160(%rax)
	movb	$0, 184(%rax)
	movq	$8, -200(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_126CollectAndMaybeResetCountsEPNS0_7IsolateEPNS0_18SharedToCounterMapENS_5debug12CoverageModeE
	movl	$24, %edi
	call	_Znwm@PLT
	movq	-280(%rbp), %rbx
	pxor	%xmm0, %xmm0
	movq	%r14, %rsi
	movq	$0, 16(%rax)
	movq	%rax, (%rbx)
	movups	%xmm0, (%rax)
	leaq	-224(%rbp), %rax
	movq	%rax, %rbx
	movq	%rax, %rdi
	movq	%rax, -344(%rbp)
	call	_ZN2v88internal6Script8IteratorC1EPNS0_7IsolateE@PLT
	movq	%rbx, %rdi
	leaq	-256(%rbp), %rbx
	call	_ZN2v88internal6Script8Iterator4NextEv@PLT
	movq	%rbx, -336(%rbp)
	movq	%rax, -256(%rbp)
	testq	%rax, %rax
	jne	.L1393
	jmp	.L1395
	.p2align 4,,10
	.p2align 3
.L1452:
	movq	-344(%rbp), %rdi
	call	_ZN2v88internal6Script8Iterator4NextEv@PLT
	movq	%rax, -256(%rbp)
	testq	%rax, %rax
	je	.L1395
.L1393:
	movq	-336(%rbp), %rdi
	call	_ZN2v88internal6Script16IsUserJavaScriptEv@PLT
	movb	%al, -297(%rbp)
	testb	%al, %al
	je	.L1452
	movq	41112(%r14), %rdi
	movq	-256(%rbp), %r12
	testq	%rdi, %rdi
	je	.L1396
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L1397:
	movq	-280(%rbp), %rbx
	movq	%rax, -248(%rbp)
	movq	(%rbx), %rdi
	movq	8(%rdi), %rsi
	cmpq	16(%rdi), %rsi
	je	.L1399
	movq	%rax, (%rsi)
	movq	%rbx, %rax
	movq	$0, 8(%rsi)
	movq	$0, 16(%rsi)
	movq	$0, 24(%rsi)
	addq	$32, 8(%rdi)
.L1400:
	movq	(%rax), %rax
	pxor	%xmm1, %xmm1
	movq	%r14, %rsi
	movq	8(%rax), %r15
	movaps	%xmm1, -176(%rbp)
	movq	$0, -160(%rbp)
	leaq	-24(%r15), %rax
	movq	%rax, -312(%rbp)
	movq	-248(%rbp), %rax
	movq	(%rax), %rdx
	leaq	-144(%rbp), %rax
	movq	%rax, %rbx
	movq	%rax, %rdi
	movq	%rax, -296(%rbp)
	call	_ZN2v88internal18SharedFunctionInfo14ScriptIteratorC1EPNS0_7IsolateENS0_6ScriptE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal18SharedFunctionInfo14ScriptIterator4NextEv@PLT
	movq	%rax, -240(%rbp)
	testq	%rax, %rax
	je	.L1412
	.p2align 4,,10
	.p2align 3
.L1401:
	movl	-200(%rbp), %ebx
	movq	-208(%rbp), %rdi
	leal	-1(%rbx), %ecx
	movl	%ecx, %edx
	andl	%eax, %edx
	leaq	(%rdx,%rdx,2), %rsi
	leaq	(%rdi,%rsi,8), %rsi
	cmpb	$0, 16(%rsi)
	jne	.L1406
	jmp	.L1457
	.p2align 4,,10
	.p2align 3
.L1495:
	addq	$1, %rdx
	andq	%rcx, %rdx
	leaq	(%rdx,%rdx,2), %rsi
	leaq	(%rdi,%rsi,8), %rsi
	cmpb	$0, 16(%rsi)
	je	.L1457
.L1406:
	cmpq	%rax, (%rsi)
	jne	.L1495
	movl	8(%rsi), %edx
.L1404:
	movl	%edx, -260(%rbp)
	movq	-168(%rbp), %r12
	cmpq	-160(%rbp), %r12
	je	.L1407
	movq	%rax, -232(%rbp)
	leaq	-112(%rbp), %r13
	movq	%rax, (%r12)
	movl	%edx, 8(%r12)
	movq	-232(%rbp), %rax
	movq	%rax, -112(%rbp)
	movzwl	45(%rax), %ebx
	cmpl	$65535, %ebx
	je	.L1410
	movq	%r13, %rdi
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	subl	%ebx, %eax
	cmpl	$-1, %eax
	je	.L1410
.L1409:
	movl	%eax, 12(%r12)
	leaq	-232(%rbp), %rdi
	call	_ZNK2v88internal18SharedFunctionInfo11EndPositionEv@PLT
	addq	$24, -168(%rbp)
	movl	%eax, 16(%r12)
.L1411:
	movq	-296(%rbp), %rdi
	call	_ZN2v88internal18SharedFunctionInfo14ScriptIterator4NextEv@PLT
	movq	%rax, -240(%rbp)
	testq	%rax, %rax
	jne	.L1401
.L1412:
	movq	-168(%rbp), %r13
	movq	-176(%rbp), %r12
	cmpq	%r12, %r13
	je	.L1496
	movq	%r13, %rbx
	movl	$63, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movabsq	$-6148914691236517205, %rcx
	subq	%r12, %rbx
	movq	%rbx, %rax
	sarq	$3, %rax
	imulq	%rcx, %rax
	bsrq	%rax, %rax
	xorq	$63, %rax
	subl	%eax, %edx
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	call	_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPN2v88internal12_GLOBAL__N_126SharedFunctionInfoAndCountESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_less_iterEEvT_SD_T0_T1_.isra.0
	cmpq	$384, %rbx
	jle	.L1413
	leaq	384(%r12), %rbx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal12_GLOBAL__N_126SharedFunctionInfoAndCountESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_less_iterEEvT_SD_T0_.isra.0
	movq	%rbx, %rdx
	cmpq	%rbx, %r13
	je	.L1415
	.p2align 4,,10
	.p2align 3
.L1421:
	movdqu	(%rdx), %xmm2
	movaps	%xmm2, -112(%rbp)
	movq	16(%rdx), %rax
	movq	%rax, -96(%rbp)
	movq	%rdx, %rax
	movl	8(%rdx), %r9d
	movl	12(%rdx), %r8d
	movl	16(%rdx), %edi
	jmp	.L1420
	.p2align 4,,10
	.p2align 3
.L1497:
	setg	%cl
.L1417:
	subq	$24, %rax
	testb	%cl, %cl
	je	.L1419
.L1498:
	movl	16(%rax), %ecx
	movdqu	(%rax), %xmm3
	movl	%ecx, 40(%rax)
	movups	%xmm3, 24(%rax)
.L1420:
	movq	%rax, %rsi
	cmpl	%r8d, -12(%rax)
	jne	.L1497
	cmpl	%edi, -8(%rax)
	je	.L1418
	setl	%cl
	subq	$24, %rax
	testb	%cl, %cl
	jne	.L1498
.L1419:
	movl	%r9d, -104(%rbp)
	addq	$24, %rdx
	movl	%r8d, -100(%rbp)
	movdqa	-112(%rbp), %xmm4
	movl	%edi, -96(%rbp)
	movups	%xmm4, (%rsi)
	movl	-96(%rbp), %eax
	movl	%eax, 16(%rsi)
	cmpq	%rdx, %r13
	jne	.L1421
.L1415:
	movq	-168(%rbp), %rax
	movq	-176(%rbp), %r12
	pxor	%xmm5, %xmm5
	movq	$0, -128(%rbp)
	movaps	%xmm5, -144(%rbp)
	movq	%rax, -320(%rbp)
	cmpq	%rax, %r12
	je	.L1423
	movq	%r12, %r13
	xorl	%eax, %eax
	xorl	%edi, %edi
	.p2align 4,,10
	.p2align 3
.L1442:
	movq	0(%r13), %rdx
	movq	%rdx, -240(%rbp)
	movl	16(%r13), %ebx
	movl	12(%r13), %r12d
	movl	%ebx, -284(%rbp)
	movl	8(%r13), %ebx
	movl	%ebx, -288(%rbp)
	cmpq	%rdi, %rax
	jne	.L1424
	jmp	.L1428
	.p2align 4,,10
	.p2align 3
.L1499:
	subq	$8, %rax
	movq	%rax, -136(%rbp)
	cmpq	%rdi, %rax
	je	.L1428
.L1424:
	movq	-24(%r15), %rcx
	movq	-16(%r15), %rdx
	movabsq	$7905747460161236407, %rbx
	movq	-8(%rax), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	imulq	%rbx, %rdx
	cmpq	%rdx, %rsi
	jnb	.L1493
	leaq	0(,%rsi,8), %rdx
	subq	%rsi, %rdx
	cmpl	4(%rcx,%rdx,8), %r12d
	jge	.L1499
.L1428:
	movl	-288(%rbp), %ecx
	leaq	-240(%rbp), %rdi
	testl	%ecx, %ecx
	setne	%al
	setne	%bl
	movzbl	%al, %eax
	movl	%eax, -304(%rbp)
	call	_ZN2v88internal18SharedFunctionInfo9DebugNameEv@PLT
	movq	41112(%r14), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L1500
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L1429:
	movl	-284(%rbp), %ecx
	movl	%r12d, -112(%rbp)
	movq	%rax, -96(%rbp)
	movq	-136(%rbp), %rsi
	movl	%ecx, -108(%rbp)
	movl	-304(%rbp), %ecx
	movq	$0, -88(%rbp)
	movl	%ecx, -104(%rbp)
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	movb	$0, -64(%rbp)
	cmpq	-144(%rbp), %rsi
	je	.L1431
	movq	-24(%r15), %rdi
	movq	-16(%r15), %rdx
	movabsq	$7905747460161236407, %rax
	movq	-8(%rsi), %r8
	subq	%rdi, %rdx
	sarq	$3, %rdx
	imulq	%rax, %rdx
	movq	%rdx, %rax
	cmpq	%rdx, %r8
	jnb	.L1501
	movl	%r12d, %ecx
	cmpl	-284(%rbp), %r12d
	notl	%ecx
	setl	%dl
	shrl	$31, %ecx
	andl	%edx, %ecx
	leaq	0(,%r8,8), %rdx
	subq	%r8, %rdx
	movl	8(%rdi,%rdx,8), %edx
	testl	%edx, %edx
	je	.L1433
	testb	%cl, %cl
	jne	.L1434
	movzbl	-297(%rbp), %ebx
	xorl	%edx, %edx
.L1435:
	cmpb	$0, _ZN2v88internal25FLAG_trace_block_coverageE(%rip)
	leaq	-112(%rbp), %r12
	jne	.L1454
.L1440:
	addq	$24, %r13
	cmpq	%r13, -320(%rbp)
	je	.L1441
.L1503:
	movq	-144(%rbp), %rdi
	movq	-136(%rbp), %rax
	jmp	.L1442
	.p2align 4,,10
	.p2align 3
.L1410:
	movq	%r13, %rdi
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	jmp	.L1409
	.p2align 4,,10
	.p2align 3
.L1407:
	leaq	-260(%rbp), %rcx
	leaq	-240(%rbp), %rdx
	movq	%r12, %rsi
	leaq	-176(%rbp), %rdi
	call	_ZNSt6vectorIN2v88internal12_GLOBAL__N_126SharedFunctionInfoAndCountESaIS3_EE17_M_realloc_insertIJRNS1_18SharedFunctionInfoEjEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L1411
	.p2align 4,,10
	.p2align 3
.L1413:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal12_GLOBAL__N_126SharedFunctionInfoAndCountESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_less_iterEEvT_SD_T0_.isra.0
	jmp	.L1415
	.p2align 4,,10
	.p2align 3
.L1418:
	cmpl	%r9d, -16(%rax)
	seta	%cl
	jmp	.L1417
	.p2align 4,,10
	.p2align 3
.L1457:
	xorl	%edx, %edx
	jmp	.L1404
	.p2align 4,,10
	.p2align 3
.L1431:
	movl	%r12d, %ecx
	cmpl	-284(%rbp), %r12d
	notl	%ecx
	setl	%al
	shrl	$31, %ecx
	andl	%eax, %ecx
.L1433:
	xorl	%edx, %edx
	testb	%cl, %cl
	je	.L1435
	movl	-288(%rbp), %eax
	testl	%eax, %eax
	je	.L1459
	movabsq	$7905747460161236407, %rbx
	movq	-16(%r15), %rax
	subq	-24(%r15), %rax
	sarq	$3, %rax
	imulq	%rbx, %rax
.L1434:
	movq	%rax, -232(%rbp)
	cmpq	-128(%rbp), %rsi
	je	.L1436
	movq	%rax, (%rsi)
	addq	$8, -136(%rbp)
.L1437:
	movq	-312(%rbp), %rdi
	leaq	-112(%rbp), %r12
	movq	%r12, %rsi
	call	_ZNSt6vectorIN2v88internal16CoverageFunctionESaIS2_EE12emplace_backIJRS2_EEEvDpOT_
	movzbl	_ZN2v88internal25FLAG_trace_block_coverageE(%rip), %ebx
	testb	%bl, %bl
	jne	.L1502
.L1438:
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1440
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, -320(%rbp)
	jne	.L1503
.L1441:
	movq	-24(%r15), %rax
	cmpq	%rax, -16(%r15)
	je	.L1453
.L1443:
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1492
	call	_ZdlPv@PLT
.L1492:
	movq	-176(%rbp), %r12
.L1451:
	testq	%r12, %r12
	je	.L1452
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	jmp	.L1452
	.p2align 4,,10
	.p2align 3
.L1502:
	movl	$1, %edx
.L1454:
	movq	-240(%rbp), %rsi
	movzbl	%bl, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_118PrintBlockCoverageEPKNS0_16CoverageFunctionENS0_18SharedFunctionInfoEbb
	jmp	.L1438
	.p2align 4,,10
	.p2align 3
.L1436:
	movq	-296(%rbp), %rdi
	leaq	-232(%rbp), %rdx
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L1437
	.p2align 4,,10
	.p2align 3
.L1500:
	movq	41088(%r14), %rax
	cmpq	41096(%r14), %rax
	je	.L1504
.L1430:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r14)
	movq	%rsi, (%rax)
	jmp	.L1429
	.p2align 4,,10
	.p2align 3
.L1504:
	movq	%r14, %rdi
	movq	%rsi, -328(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-328(%rbp), %rsi
	jmp	.L1430
	.p2align 4,,10
	.p2align 3
.L1396:
	movq	41088(%r14), %rax
	cmpq	41096(%r14), %rax
	je	.L1505
.L1398:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r14)
	movq	%r12, (%rax)
	jmp	.L1397
	.p2align 4,,10
	.p2align 3
.L1399:
	leaq	-248(%rbp), %rdx
	call	_ZNSt6vectorIN2v88internal14CoverageScriptESaIS2_EE17_M_realloc_insertIJRNS1_6HandleINS1_6ScriptEEEEEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	movq	-280(%rbp), %rax
	jmp	.L1400
	.p2align 4,,10
	.p2align 3
.L1496:
	movq	$0, -128(%rbp)
	pxor	%xmm6, %xmm6
	movaps	%xmm6, -144(%rbp)
.L1423:
	movq	-16(%r15), %rax
	cmpq	%rax, -24(%r15)
	jne	.L1451
	.p2align 4,,10
	.p2align 3
.L1453:
	movq	-280(%rbp), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rbx
	leaq	-32(%rbx), %rdx
	movq	%rdx, 8(%rax)
	movq	-16(%rbx), %r13
	movq	-24(%rbx), %r12
	cmpq	%r12, %r13
	je	.L1444
	.p2align 4,,10
	.p2align 3
.L1448:
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1445
	call	_ZdlPv@PLT
	addq	$56, %r12
	cmpq	%r12, %r13
	jne	.L1448
.L1446:
	movq	-24(%rbx), %r12
.L1444:
	testq	%r12, %r12
	je	.L1443
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	jmp	.L1443
	.p2align 4,,10
	.p2align 3
.L1445:
	addq	$56, %r12
	cmpq	%r12, %r13
	jne	.L1448
	jmp	.L1446
	.p2align 4,,10
	.p2align 3
.L1501:
	movq	%r8, %rsi
	.p2align 4,,10
	.p2align 3
.L1493:
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1395:
	movq	-208(%rbp), %rdi
	call	free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1506
	movq	-280(%rbp), %rax
	addq	$312, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1505:
	.cfi_restore_state
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L1398
.L1494:
	leaq	.LC4(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L1506:
	call	__stack_chk_fail@PLT
.L1459:
	movl	$1, %edx
	xorl	%ebx, %ebx
	jmp	.L1435
	.cfi_endproc
.LFE19935:
	.size	_ZN2v88internal8Coverage17CollectBestEffortEPNS0_7IsolateE, .-_ZN2v88internal8Coverage17CollectBestEffortEPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8Coverage14CollectPreciseEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8Coverage14CollectPreciseEPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal8Coverage14CollectPreciseEPNS0_7IsolateE:
.LFB25734:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE25734:
	.size	_GLOBAL__sub_I__ZN2v88internal8Coverage14CollectPreciseEPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal8Coverage14CollectPreciseEPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8Coverage14CollectPreciseEPNS0_7IsolateE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
