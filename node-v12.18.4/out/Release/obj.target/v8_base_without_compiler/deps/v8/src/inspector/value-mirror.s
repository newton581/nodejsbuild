	.file	"value-mirror.cc"
	.text
	.section	.text._ZN12v8_inspector17V8InspectorClient12valueSubtypeEN2v85LocalINS1_5ValueEEE,"axG",@progbits,_ZN12v8_inspector17V8InspectorClient12valueSubtypeEN2v85LocalINS1_5ValueEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector17V8InspectorClient12valueSubtypeEN2v85LocalINS1_5ValueEEE
	.type	_ZN12v8_inspector17V8InspectorClient12valueSubtypeEN2v85LocalINS1_5ValueEEE, @function
_ZN12v8_inspector17V8InspectorClient12valueSubtypeEN2v85LocalINS1_5ValueEEE:
.LFB3633:
	.cfi_startproc
	endbr64
	movq	$0, (%rdi)
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE3633:
	.size	_ZN12v8_inspector17V8InspectorClient12valueSubtypeEN2v85LocalINS1_5ValueEEE, .-_ZN12v8_inspector17V8InspectorClient12valueSubtypeEN2v85LocalINS1_5ValueEEE
	.section	.text._ZN12v8_inspector17V8InspectorClient27formatAccessorsAsPropertiesEN2v85LocalINS1_5ValueEEE,"axG",@progbits,_ZN12v8_inspector17V8InspectorClient27formatAccessorsAsPropertiesEN2v85LocalINS1_5ValueEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector17V8InspectorClient27formatAccessorsAsPropertiesEN2v85LocalINS1_5ValueEEE
	.type	_ZN12v8_inspector17V8InspectorClient27formatAccessorsAsPropertiesEN2v85LocalINS1_5ValueEEE, @function
_ZN12v8_inspector17V8InspectorClient27formatAccessorsAsPropertiesEN2v85LocalINS1_5ValueEEE:
.LFB3634:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3634:
	.size	_ZN12v8_inspector17V8InspectorClient27formatAccessorsAsPropertiesEN2v85LocalINS1_5ValueEEE, .-_ZN12v8_inspector17V8InspectorClient27formatAccessorsAsPropertiesEN2v85LocalINS1_5ValueEEE
	.section	.text._ZNK12v8_inspector11ValueMirror20buildPropertyPreviewEN2v85LocalINS1_7ContextEEERKNS_8String16EPSt10unique_ptrINS_8protocol7Runtime15PropertyPreviewESt14default_deleteISB_EE,"axG",@progbits,_ZNK12v8_inspector11ValueMirror20buildPropertyPreviewEN2v85LocalINS1_7ContextEEERKNS_8String16EPSt10unique_ptrINS_8protocol7Runtime15PropertyPreviewESt14default_deleteISB_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK12v8_inspector11ValueMirror20buildPropertyPreviewEN2v85LocalINS1_7ContextEEERKNS_8String16EPSt10unique_ptrINS_8protocol7Runtime15PropertyPreviewESt14default_deleteISB_EE
	.type	_ZNK12v8_inspector11ValueMirror20buildPropertyPreviewEN2v85LocalINS1_7ContextEEERKNS_8String16EPSt10unique_ptrINS_8protocol7Runtime15PropertyPreviewESt14default_deleteISB_EE, @function
_ZNK12v8_inspector11ValueMirror20buildPropertyPreviewEN2v85LocalINS1_7ContextEEERKNS_8String16EPSt10unique_ptrINS_8protocol7Runtime15PropertyPreviewESt14default_deleteISB_EE:
.LFB5510:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5510:
	.size	_ZNK12v8_inspector11ValueMirror20buildPropertyPreviewEN2v85LocalINS1_7ContextEEERKNS_8String16EPSt10unique_ptrINS_8protocol7Runtime15PropertyPreviewESt14default_deleteISB_EE, .-_ZNK12v8_inspector11ValueMirror20buildPropertyPreviewEN2v85LocalINS1_7ContextEEERKNS_8String16EPSt10unique_ptrINS_8protocol7Runtime15PropertyPreviewESt14default_deleteISB_EE
	.section	.text._ZNK12v8_inspector11ValueMirror18buildObjectPreviewEN2v85LocalINS1_7ContextEEEbPiS5_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteIS9_EE,"axG",@progbits,_ZNK12v8_inspector11ValueMirror18buildObjectPreviewEN2v85LocalINS1_7ContextEEEbPiS5_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteIS9_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK12v8_inspector11ValueMirror18buildObjectPreviewEN2v85LocalINS1_7ContextEEEbPiS5_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteIS9_EE
	.type	_ZNK12v8_inspector11ValueMirror18buildObjectPreviewEN2v85LocalINS1_7ContextEEEbPiS5_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteIS9_EE, @function
_ZNK12v8_inspector11ValueMirror18buildObjectPreviewEN2v85LocalINS1_7ContextEEEbPiS5_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteIS9_EE:
.LFB5511:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5511:
	.size	_ZNK12v8_inspector11ValueMirror18buildObjectPreviewEN2v85LocalINS1_7ContextEEEbPiS5_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteIS9_EE, .-_ZNK12v8_inspector11ValueMirror18buildObjectPreviewEN2v85LocalINS1_7ContextEEEbPiS5_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteIS9_EE
	.section	.text._ZNK12v8_inspector11ValueMirror17buildEntryPreviewEN2v85LocalINS1_7ContextEEEPiS5_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteIS9_EE,"axG",@progbits,_ZNK12v8_inspector11ValueMirror17buildEntryPreviewEN2v85LocalINS1_7ContextEEEPiS5_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteIS9_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK12v8_inspector11ValueMirror17buildEntryPreviewEN2v85LocalINS1_7ContextEEEPiS5_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteIS9_EE
	.type	_ZNK12v8_inspector11ValueMirror17buildEntryPreviewEN2v85LocalINS1_7ContextEEEPiS5_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteIS9_EE, @function
_ZNK12v8_inspector11ValueMirror17buildEntryPreviewEN2v85LocalINS1_7ContextEEEPiS5_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteIS9_EE:
.LFB5512:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5512:
	.size	_ZNK12v8_inspector11ValueMirror17buildEntryPreviewEN2v85LocalINS1_7ContextEEEPiS5_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteIS9_EE, .-_ZNK12v8_inspector11ValueMirror17buildEntryPreviewEN2v85LocalINS1_7ContextEEEPiS5_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteIS9_EE
	.section	.text._ZNK12v8_inspector12_GLOBAL__N_120PrimitiveValueMirror7v8ValueEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK12v8_inspector12_GLOBAL__N_120PrimitiveValueMirror7v8ValueEv, @function
_ZNK12v8_inspector12_GLOBAL__N_120PrimitiveValueMirror7v8ValueEv:
.LFB7843:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE7843:
	.size	_ZNK12v8_inspector12_GLOBAL__N_120PrimitiveValueMirror7v8ValueEv, .-_ZNK12v8_inspector12_GLOBAL__N_120PrimitiveValueMirror7v8ValueEv
	.section	.text._ZNK12v8_inspector12_GLOBAL__N_112NumberMirror7v8ValueEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK12v8_inspector12_GLOBAL__N_112NumberMirror7v8ValueEv, @function
_ZNK12v8_inspector12_GLOBAL__N_112NumberMirror7v8ValueEv:
.LFB7850:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE7850:
	.size	_ZNK12v8_inspector12_GLOBAL__N_112NumberMirror7v8ValueEv, .-_ZNK12v8_inspector12_GLOBAL__N_112NumberMirror7v8ValueEv
	.section	.text._ZNK12v8_inspector12_GLOBAL__N_112BigIntMirror7v8ValueEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK12v8_inspector12_GLOBAL__N_112BigIntMirror7v8ValueEv, @function
_ZNK12v8_inspector12_GLOBAL__N_112BigIntMirror7v8ValueEv:
.LFB7861:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE7861:
	.size	_ZNK12v8_inspector12_GLOBAL__N_112BigIntMirror7v8ValueEv, .-_ZNK12v8_inspector12_GLOBAL__N_112BigIntMirror7v8ValueEv
	.section	.text._ZNK12v8_inspector12_GLOBAL__N_112SymbolMirror7v8ValueEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK12v8_inspector12_GLOBAL__N_112SymbolMirror7v8ValueEv, @function
_ZNK12v8_inspector12_GLOBAL__N_112SymbolMirror7v8ValueEv:
.LFB7867:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE7867:
	.size	_ZNK12v8_inspector12_GLOBAL__N_112SymbolMirror7v8ValueEv, .-_ZNK12v8_inspector12_GLOBAL__N_112SymbolMirror7v8ValueEv
	.section	.text._ZNK12v8_inspector12_GLOBAL__N_114LocationMirror7v8ValueEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK12v8_inspector12_GLOBAL__N_114LocationMirror7v8ValueEv, @function
_ZNK12v8_inspector12_GLOBAL__N_114LocationMirror7v8ValueEv:
.LFB7873:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE7873:
	.size	_ZNK12v8_inspector12_GLOBAL__N_114LocationMirror7v8ValueEv, .-_ZNK12v8_inspector12_GLOBAL__N_114LocationMirror7v8ValueEv
	.section	.text._ZNK12v8_inspector12_GLOBAL__N_114FunctionMirror7v8ValueEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK12v8_inspector12_GLOBAL__N_114FunctionMirror7v8ValueEv, @function
_ZNK12v8_inspector12_GLOBAL__N_114FunctionMirror7v8ValueEv:
.LFB7881:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE7881:
	.size	_ZNK12v8_inspector12_GLOBAL__N_114FunctionMirror7v8ValueEv, .-_ZNK12v8_inspector12_GLOBAL__N_114FunctionMirror7v8ValueEv
	.section	.text._ZNK12v8_inspector12_GLOBAL__N_112ObjectMirror7v8ValueEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK12v8_inspector12_GLOBAL__N_112ObjectMirror7v8ValueEv, @function
_ZNK12v8_inspector12_GLOBAL__N_112ObjectMirror7v8ValueEv:
.LFB7991:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE7991:
	.size	_ZNK12v8_inspector12_GLOBAL__N_112ObjectMirror7v8ValueEv, .-_ZNK12v8_inspector12_GLOBAL__N_112ObjectMirror7v8ValueEv
	.section	.text._ZN12v8_inspector12_GLOBAL__N_114LocationMirrorD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_114LocationMirrorD2Ev, @function
_ZN12v8_inspector12_GLOBAL__N_114LocationMirrorD2Ev:
.LFB10839:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE10839:
	.size	_ZN12v8_inspector12_GLOBAL__N_114LocationMirrorD2Ev, .-_ZN12v8_inspector12_GLOBAL__N_114LocationMirrorD2Ev
	.set	_ZN12v8_inspector12_GLOBAL__N_114LocationMirrorD1Ev,_ZN12v8_inspector12_GLOBAL__N_114LocationMirrorD2Ev
	.section	.text._ZN12v8_inspector12_GLOBAL__N_112NumberMirrorD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_112NumberMirrorD2Ev, @function
_ZN12v8_inspector12_GLOBAL__N_112NumberMirrorD2Ev:
.LFB11122:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11122:
	.size	_ZN12v8_inspector12_GLOBAL__N_112NumberMirrorD2Ev, .-_ZN12v8_inspector12_GLOBAL__N_112NumberMirrorD2Ev
	.set	_ZN12v8_inspector12_GLOBAL__N_112NumberMirrorD1Ev,_ZN12v8_inspector12_GLOBAL__N_112NumberMirrorD2Ev
	.section	.text._ZN12v8_inspector12_GLOBAL__N_112BigIntMirrorD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_112BigIntMirrorD2Ev, @function
_ZN12v8_inspector12_GLOBAL__N_112BigIntMirrorD2Ev:
.LFB11147:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11147:
	.size	_ZN12v8_inspector12_GLOBAL__N_112BigIntMirrorD2Ev, .-_ZN12v8_inspector12_GLOBAL__N_112BigIntMirrorD2Ev
	.set	_ZN12v8_inspector12_GLOBAL__N_112BigIntMirrorD1Ev,_ZN12v8_inspector12_GLOBAL__N_112BigIntMirrorD2Ev
	.section	.text._ZN12v8_inspector12_GLOBAL__N_112SymbolMirrorD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_112SymbolMirrorD2Ev, @function
_ZN12v8_inspector12_GLOBAL__N_112SymbolMirrorD2Ev:
.LFB11171:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11171:
	.size	_ZN12v8_inspector12_GLOBAL__N_112SymbolMirrorD2Ev, .-_ZN12v8_inspector12_GLOBAL__N_112SymbolMirrorD2Ev
	.set	_ZN12v8_inspector12_GLOBAL__N_112SymbolMirrorD1Ev,_ZN12v8_inspector12_GLOBAL__N_112SymbolMirrorD2Ev
	.section	.text._ZN12v8_inspector12_GLOBAL__N_114FunctionMirrorD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_114FunctionMirrorD2Ev, @function
_ZN12v8_inspector12_GLOBAL__N_114FunctionMirrorD2Ev:
.LFB11196:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11196:
	.size	_ZN12v8_inspector12_GLOBAL__N_114FunctionMirrorD2Ev, .-_ZN12v8_inspector12_GLOBAL__N_114FunctionMirrorD2Ev
	.set	_ZN12v8_inspector12_GLOBAL__N_114FunctionMirrorD1Ev,_ZN12v8_inspector12_GLOBAL__N_114FunctionMirrorD2Ev
	.section	.text._ZN12v8_inspector12_GLOBAL__N_114LocationMirrorD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_114LocationMirrorD0Ev, @function
_ZN12v8_inspector12_GLOBAL__N_114LocationMirrorD0Ev:
.LFB10841:
	.cfi_startproc
	endbr64
	movl	$32, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10841:
	.size	_ZN12v8_inspector12_GLOBAL__N_114LocationMirrorD0Ev, .-_ZN12v8_inspector12_GLOBAL__N_114LocationMirrorD0Ev
	.section	.text._ZN12v8_inspector12_GLOBAL__N_112NumberMirrorD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_112NumberMirrorD0Ev, @function
_ZN12v8_inspector12_GLOBAL__N_112NumberMirrorD0Ev:
.LFB11124:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11124:
	.size	_ZN12v8_inspector12_GLOBAL__N_112NumberMirrorD0Ev, .-_ZN12v8_inspector12_GLOBAL__N_112NumberMirrorD0Ev
	.section	.text._ZN12v8_inspector12_GLOBAL__N_112BigIntMirrorD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_112BigIntMirrorD0Ev, @function
_ZN12v8_inspector12_GLOBAL__N_112BigIntMirrorD0Ev:
.LFB11149:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11149:
	.size	_ZN12v8_inspector12_GLOBAL__N_112BigIntMirrorD0Ev, .-_ZN12v8_inspector12_GLOBAL__N_112BigIntMirrorD0Ev
	.section	.text._ZN12v8_inspector12_GLOBAL__N_112SymbolMirrorD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_112SymbolMirrorD0Ev, @function
_ZN12v8_inspector12_GLOBAL__N_112SymbolMirrorD0Ev:
.LFB11173:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11173:
	.size	_ZN12v8_inspector12_GLOBAL__N_112SymbolMirrorD0Ev, .-_ZN12v8_inspector12_GLOBAL__N_112SymbolMirrorD0Ev
	.section	.text._ZN12v8_inspector12_GLOBAL__N_114FunctionMirrorD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_114FunctionMirrorD0Ev, @function
_ZN12v8_inspector12_GLOBAL__N_114FunctionMirrorD0Ev:
.LFB11198:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11198:
	.size	_ZN12v8_inspector12_GLOBAL__N_114FunctionMirrorD0Ev, .-_ZN12v8_inspector12_GLOBAL__N_114FunctionMirrorD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime13CustomPreviewD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13CustomPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD2Ev, @function
_ZN12v8_inspector8protocol7Runtime13CustomPreviewD2Ev:
.LFB4606:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	56(%rdi), %rdi
	leaq	72(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L25
	call	_ZdlPv@PLT
.L25:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L24
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4606:
	.size	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD2Ev, .-_ZN12v8_inspector8protocol7Runtime13CustomPreviewD2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD1Ev,_ZN12v8_inspector8protocol7Runtime13CustomPreviewD2Ev
	.section	.text._ZN12v8_inspector12_GLOBAL__N_112ObjectMirrorD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_112ObjectMirrorD2Ev, @function
_ZN12v8_inspector12_GLOBAL__N_112ObjectMirrorD2Ev:
.LFB11074:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector12_GLOBAL__N_112ObjectMirrorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	64(%rdi), %rdi
	leaq	80(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L29
	call	_ZdlPv@PLT
.L29:
	movq	16(%rbx), %rdi
	addq	$32, %rbx
	cmpq	%rbx, %rdi
	je	.L28
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11074:
	.size	_ZN12v8_inspector12_GLOBAL__N_112ObjectMirrorD2Ev, .-_ZN12v8_inspector12_GLOBAL__N_112ObjectMirrorD2Ev
	.set	_ZN12v8_inspector12_GLOBAL__N_112ObjectMirrorD1Ev,_ZN12v8_inspector12_GLOBAL__N_112ObjectMirrorD2Ev
	.section	.text._ZN12v8_inspector12_GLOBAL__N_120PrimitiveValueMirrorD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_120PrimitiveValueMirrorD2Ev, @function
_ZN12v8_inspector12_GLOBAL__N_120PrimitiveValueMirrorD2Ev:
.LFB11098:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector12_GLOBAL__N_120PrimitiveValueMirrorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	56(%rdi), %rdi
	leaq	72(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L33
	call	_ZdlPv@PLT
.L33:
	movq	16(%rbx), %rdi
	addq	$32, %rbx
	cmpq	%rbx, %rdi
	je	.L32
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11098:
	.size	_ZN12v8_inspector12_GLOBAL__N_120PrimitiveValueMirrorD2Ev, .-_ZN12v8_inspector12_GLOBAL__N_120PrimitiveValueMirrorD2Ev
	.set	_ZN12v8_inspector12_GLOBAL__N_120PrimitiveValueMirrorD1Ev,_ZN12v8_inspector12_GLOBAL__N_120PrimitiveValueMirrorD2Ev
	.section	.rodata._ZN12v8_inspector12_GLOBAL__N_120nativeGetterCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"name"
.LC1:
	.string	"object"
	.section	.text._ZN12v8_inspector12_GLOBAL__N_120nativeGetterCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE,"ax",@progbits
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_120nativeGetterCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN12v8_inspector12_GLOBAL__N_120nativeGetterCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB8016:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-96(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	8(%rax), %r14
	leaq	32(%rax), %r15
	movq	%r14, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN2v86Object20GetRealNamedPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEE@PLT
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L64
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	movq	%rax, -104(%rbp)
	cmpq	%rax, %rdi
	je	.L49
	call	_ZdlPv@PLT
.L49:
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN2v86Object20GetRealNamedPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L39
	movq	%rax, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L39
	movq	-96(%rbp), %rdi
	cmpq	-104(%rbp), %rdi
	je	.L43
	call	_ZdlPv@PLT
.L43:
	movq	-112(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	testq	%rax, %rax
	je	.L36
	movq	(%rbx), %rdx
	movq	(%rax), %rax
	movq	%rax, 24(%rdx)
.L36:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L65
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	.cfi_restore_state
	movq	-96(%rbp), %rdi
	cmpq	-104(%rbp), %rdi
	je	.L36
	call	_ZdlPv@PLT
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L64:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L36
	call	_ZdlPv@PLT
	jmp	.L36
.L65:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8016:
	.size	_ZN12v8_inspector12_GLOBAL__N_120nativeGetterCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN12v8_inspector12_GLOBAL__N_120nativeGetterCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.text._ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv:
.LFB4615:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime13CustomPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L66
	movq	(%rdi), %rax
	call	*24(%rax)
.L66:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L73
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L73:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4615:
	.size	_ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv, .-_ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv, @function
_ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv:
.LFB4614:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime13CustomPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L74
	movq	(%rdi), %rax
	call	*24(%rax)
.L74:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L81
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L81:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4614:
	.size	_ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv, .-_ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv:
.LFB4661:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime13ObjectPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L82
	movq	(%rdi), %rax
	call	*24(%rax)
.L82:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L89
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L89:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4661:
	.size	_ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv, .-_ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv, @function
_ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv:
.LFB4660:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime13ObjectPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L90
	movq	(%rdi), %rax
	call	*24(%rax)
.L90:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L97
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L97:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4660:
	.size	_ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv, .-_ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv:
.LFB4762:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime12EntryPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L98
	movq	(%rdi), %rax
	call	*24(%rax)
.L98:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L105
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L105:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4762:
	.size	_ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv, .-_ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv, @function
_ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv:
.LFB4761:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime12EntryPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L106
	movq	(%rdi), %rax
	call	*24(%rax)
.L106:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L113
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L113:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4761:
	.size	_ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv, .-_ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv:
.LFB4730:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime15PropertyPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L114
	movq	(%rdi), %rax
	call	*24(%rax)
.L114:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L121
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L121:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4730:
	.size	_ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv, .-_ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv, @function
_ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv:
.LFB4729:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime15PropertyPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L122
	movq	(%rdi), %rax
	call	*24(%rax)
.L122:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L129
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L129:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4729:
	.size	_ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv, .-_ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv
	.section	.rodata._ZN12v8_inspector12_GLOBAL__N_111isArrayLikeEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEEPm.str1.1,"aMS",@progbits,1
.LC2:
	.string	"splice"
.LC3:
	.string	"length"
	.section	.text._ZN12v8_inspector12_GLOBAL__N_111isArrayLikeEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEEPm,"ax",@progbits
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_111isArrayLikeEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEEPm, @function
_ZN12v8_inspector12_GLOBAL__N_111isArrayLikeEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEEPm:
.LFB7885:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -192(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v85Value8IsObjectEv@PLT
	movl	%eax, %r8d
	testb	%al, %al
	jne	.L168
.L130:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L169
	addq	$168, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L168:
	.cfi_restore_state
	movq	%r13, %rdi
	leaq	-144(%rbp), %r15
	leaq	-176(%rbp), %rbx
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%r15, %rdi
	movq	%rax, %r14
	movq	%rax, %rsi
	call	_ZN2v88TryCatchC1EPNS_7IsolateE@PLT
	movl	$1, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v815MicrotasksScopeC1EPNS_7IsolateENS0_4TypeE@PLT
	movq	%r12, %rdi
	call	_ZNK2v85Value17IsArgumentsObjectEv@PLT
	movl	%eax, %r8d
	leaq	-96(%rbp), %rax
	movq	%rax, -184(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rax, -200(%rbp)
	testb	%r8b, %r8b
	je	.L170
.L147:
	movq	-184(%rbp), %rdi
	leaq	.LC3(%rip), %rsi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-184(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	movq	%rax, %rdx
	call	_ZN2v86Object14HasOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L137
	movl	%eax, -204(%rbp)
	call	_ZdlPv@PLT
	movl	-204(%rbp), %eax
.L137:
	movl	%eax, %r8d
	testb	%al, %al
	je	.L135
	movzbl	%ah, %eax
	movl	%eax, %r8d
	testb	%al, %al
	je	.L135
	movq	-184(%rbp), %rdi
	leaq	.LC3(%rip), %rsi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-184(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	movq	%rax, %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L139
	movq	%rax, %rdi
	call	_ZNK2v85Value8IsUint32Ev@PLT
	movl	%eax, %r8d
	testb	%al, %al
	je	.L139
	movq	-96(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	jne	.L171
.L145:
	movq	%r12, %rdi
	movb	%r8b, -184(%rbp)
	call	_ZNK2v86Uint325ValueEv@PLT
	movq	-192(%rbp), %rcx
	movzbl	-184(%rbp), %r8d
	movl	%eax, %eax
	movq	%rax, (%rcx)
	.p2align 4,,10
	.p2align 3
.L135:
	movq	%rbx, %rdi
	movb	%r8b, -184(%rbp)
	call	_ZN2v815MicrotasksScopeD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88TryCatchD1Ev@PLT
	movzbl	-184(%rbp), %r8d
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L170:
	movq	-184(%rbp), %rdi
	leaq	.LC2(%rip), %rsi
	movb	%r8b, -204(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-184(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	movq	%rax, %rdx
	call	_ZN2v86Object20GetRealNamedPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEE@PLT
	movzbl	-204(%rbp), %r8d
	testq	%rax, %rax
	movq	%rax, %rdi
	je	.L149
	movb	%r8b, -204(%rbp)
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L172
	movq	-96(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L147
	call	_ZdlPv@PLT
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L172:
	movzbl	-204(%rbp), %r8d
.L149:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L135
	movb	%r8b, -184(%rbp)
	call	_ZdlPv@PLT
	movzbl	-184(%rbp), %r8d
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L139:
	movq	-96(%rbp), %rdi
	xorl	%r8d, %r8d
	cmpq	-200(%rbp), %rdi
	je	.L135
	call	_ZdlPv@PLT
	xorl	%r8d, %r8d
	jmp	.L135
.L169:
	call	__stack_chk_fail@PLT
.L171:
	movb	%r8b, -184(%rbp)
	call	_ZdlPv@PLT
	movzbl	-184(%rbp), %r8d
	jmp	.L145
	.cfi_endproc
.LFE7885:
	.size	_ZN12v8_inspector12_GLOBAL__N_111isArrayLikeEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEEPm, .-_ZN12v8_inspector12_GLOBAL__N_111isArrayLikeEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEEPm
	.section	.rodata._ZN12v8_inspector12_GLOBAL__N_127descriptionForPrimitiveTypeEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"true"
.LC5:
	.string	"false"
.LC6:
	.string	"unreachable code"
	.section	.text._ZN12v8_inspector12_GLOBAL__N_127descriptionForPrimitiveTypeEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE,"ax",@progbits
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_127descriptionForPrimitiveTypeEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE, @function
_ZN12v8_inspector12_GLOBAL__N_127descriptionForPrimitiveTypeEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE:
.LFB7798:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L174
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	je	.L184
.L174:
	movq	%r12, %rdi
	call	_ZNK2v85Value9IsBooleanEv@PLT
	testb	%al, %al
	jne	.L185
	movq	(%r12), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L179
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L179
	movq	%r14, %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN12v8_inspector16toProtocolStringEPN2v87IsolateENS0_5LocalINS0_6StringEEE@PLT
.L173:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L186
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L179:
	.cfi_restore_state
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L184:
	movslq	43(%rax), %rax
	cmpq	$5, %rax
	je	.L187
	cmpq	$3, %rax
	jne	.L174
	movq	_ZN12v8_inspector8protocol7Runtime12RemoteObject11SubtypeEnum4NullE(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L185:
	movq	%r12, %rdi
	call	_ZNK2v87Boolean5ValueEv@PLT
	leaq	.LC4(%rip), %rsi
	movq	%r13, %rdi
	testb	%al, %al
	leaq	.LC5(%rip), %rax
	cmove	%rax, %rsi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L187:
	movq	_ZN12v8_inspector8protocol7Runtime12RemoteObject8TypeEnum9UndefinedE(%rip), %rsi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	jmp	.L173
.L186:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7798:
	.size	_ZN12v8_inspector12_GLOBAL__N_127descriptionForPrimitiveTypeEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE, .-_ZN12v8_inspector12_GLOBAL__N_127descriptionForPrimitiveTypeEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE
	.section	.text._ZN12v8_inspector12_GLOBAL__N_120nativeSetterCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE,"ax",@progbits
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_120nativeSetterCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN12v8_inspector12_GLOBAL__N_120nativeSetterCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB8018:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	16(%rdi), %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	jle	.L188
	movq	(%rdi), %rax
	leaq	-96(%rbp), %r13
	movq	%rdi, %rbx
	movq	8(%rax), %r14
	leaq	32(%rax), %r15
	movq	%r14, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN2v86Object20GetRealNamedPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEE@PLT
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L215
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	movq	%rax, -104(%rbp)
	cmpq	%rax, %rdi
	je	.L203
	call	_ZdlPv@PLT
.L203:
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN2v86Object20GetRealNamedPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L192
	movq	%rax, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L192
	movq	-96(%rbp), %rdi
	cmpq	-104(%rbp), %rdi
	je	.L197
	call	_ZdlPv@PLT
.L197:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L195
	movq	(%rbx), %rax
	movq	8(%rax), %rcx
	addq	$88, %rcx
.L196:
	movq	-112(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	.p2align 4,,10
	.p2align 3
.L188:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L216
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L192:
	.cfi_restore_state
	movq	-96(%rbp), %rdi
	cmpq	-104(%rbp), %rdi
	je	.L188
.L214:
	call	_ZdlPv@PLT
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L215:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L214
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L195:
	movq	8(%rbx), %rcx
	jmp	.L196
.L216:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8018:
	.size	_ZN12v8_inspector12_GLOBAL__N_120nativeSetterCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN12v8_inspector12_GLOBAL__N_120nativeSetterCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.text._ZN12v8_inspector12_GLOBAL__N_120PrimitiveValueMirrorD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_120PrimitiveValueMirrorD0Ev, @function
_ZN12v8_inspector12_GLOBAL__N_120PrimitiveValueMirrorD0Ev:
.LFB11100:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector12_GLOBAL__N_120PrimitiveValueMirrorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	56(%rdi), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L218
	call	_ZdlPv@PLT
.L218:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L219
	call	_ZdlPv@PLT
.L219:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$96, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11100:
	.size	_ZN12v8_inspector12_GLOBAL__N_120PrimitiveValueMirrorD0Ev, .-_ZN12v8_inspector12_GLOBAL__N_120PrimitiveValueMirrorD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13CustomPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev, @function
_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev:
.LFB4608:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	56(%rdi), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L222
	call	_ZdlPv@PLT
.L222:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L223
	call	_ZdlPv@PLT
.L223:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$96, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4608:
	.size	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev, .-_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev
	.section	.text._ZN12v8_inspector12_GLOBAL__N_112ObjectMirrorD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_112ObjectMirrorD0Ev, @function
_ZN12v8_inspector12_GLOBAL__N_112ObjectMirrorD0Ev:
.LFB11076:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector12_GLOBAL__N_112ObjectMirrorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	64(%rdi), %rdi
	leaq	80(%r12), %rax
	cmpq	%rax, %rdi
	je	.L226
	call	_ZdlPv@PLT
.L226:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L227
	call	_ZdlPv@PLT
.L227:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$104, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11076:
	.size	_ZN12v8_inspector12_GLOBAL__N_112ObjectMirrorD0Ev, .-_ZN12v8_inspector12_GLOBAL__N_112ObjectMirrorD0Ev
	.section	.text._ZN12v8_inspector12_GLOBAL__N_126PreviewPropertyAccumulatorD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_126PreviewPropertyAccumulatorD2Ev, @function
_ZN12v8_inspector12_GLOBAL__N_126PreviewPropertyAccumulatorD2Ev:
.LFB14634:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector12_GLOBAL__N_126PreviewPropertyAccumulatorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %r13
	movq	8(%rdi), %r12
	movq	%rax, (%rdi)
	cmpq	%r12, %r13
	je	.L230
	.p2align 4,,10
	.p2align 3
.L234:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L231
	call	_ZdlPv@PLT
	addq	$40, %r12
	cmpq	%r13, %r12
	jne	.L234
.L232:
	movq	8(%rbx), %r12
.L230:
	testq	%r12, %r12
	je	.L229
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L231:
	.cfi_restore_state
	addq	$40, %r12
	cmpq	%r12, %r13
	jne	.L234
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L229:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE14634:
	.size	_ZN12v8_inspector12_GLOBAL__N_126PreviewPropertyAccumulatorD2Ev, .-_ZN12v8_inspector12_GLOBAL__N_126PreviewPropertyAccumulatorD2Ev
	.set	_ZN12v8_inspector12_GLOBAL__N_126PreviewPropertyAccumulatorD1Ev,_ZN12v8_inspector12_GLOBAL__N_126PreviewPropertyAccumulatorD2Ev
	.section	.text._ZN12v8_inspector12_GLOBAL__N_126PreviewPropertyAccumulatorD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_126PreviewPropertyAccumulatorD0Ev, @function
_ZN12v8_inspector12_GLOBAL__N_126PreviewPropertyAccumulatorD0Ev:
.LFB14636:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector12_GLOBAL__N_126PreviewPropertyAccumulatorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	16(%rdi), %rbx
	movq	8(%rdi), %r12
	movq	%rax, (%rdi)
	cmpq	%r12, %rbx
	je	.L238
	.p2align 4,,10
	.p2align 3
.L242:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L239
	call	_ZdlPv@PLT
	addq	$40, %r12
	cmpq	%rbx, %r12
	jne	.L242
.L240:
	movq	8(%r13), %r12
.L238:
	testq	%r12, %r12
	je	.L243
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L243:
	addq	$8, %rsp
	movq	%r13, %rdi
	movl	$72, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L239:
	.cfi_restore_state
	addq	$40, %r12
	cmpq	%r12, %rbx
	jne	.L242
	jmp	.L240
	.cfi_endproc
.LFE14636:
	.size	_ZN12v8_inspector12_GLOBAL__N_126PreviewPropertyAccumulatorD0Ev, .-_ZN12v8_inspector12_GLOBAL__N_126PreviewPropertyAccumulatorD0Ev
	.section	.rodata._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0.str1.8,"aMS",@progbits,1
	.align 8
.LC7:
	.string	"basic_string::_M_construct null not valid"
	.section	.rodata._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0.str1.1,"aMS",@progbits,1
.LC8:
	.string	"basic_string::_M_create"
	.section	.text._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0, @function
_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0:
.LFB14979:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	testq	%rsi, %rsi
	jne	.L249
	testq	%rdx, %rdx
	jne	.L265
.L249:
	subq	%r13, %rbx
	movq	%rbx, %r14
	sarq	%r14
	cmpq	$15, %rbx
	ja	.L250
	movq	(%r12), %rdi
.L251:
	cmpq	$2, %rbx
	je	.L266
	testq	%rbx, %rbx
	je	.L254
	movq	%rbx, %rdx
	movq	%r13, %rsi
	call	memmove@PLT
	movq	(%r12), %rdi
.L254:
	xorl	%eax, %eax
	movq	%r14, 8(%r12)
	movw	%ax, (%rdi,%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L266:
	.cfi_restore_state
	movzwl	0(%r13), %eax
	movw	%ax, (%rdi)
	movq	(%r12), %rdi
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L250:
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %r14
	ja	.L267
	leaq	2(%rbx), %rdi
	call	_Znwm@PLT
	movq	%r14, 16(%r12)
	movq	%rax, (%r12)
	movq	%rax, %rdi
	jmp	.L251
.L265:
	leaq	.LC7(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L267:
	leaq	.LC8(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE14979:
	.size	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0, .-_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	.section	.text._ZN2v84base11make_uniqueIN12v8_inspector12_GLOBAL__N_120PrimitiveValueMirrorEJRNS_5LocalINS_5ValueEEERPKcEEESt10unique_ptrIT_St14default_deleteISD_EEDpOT0_.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v84base11make_uniqueIN12v8_inspector12_GLOBAL__N_120PrimitiveValueMirrorEJRNS_5LocalINS_5ValueEEERPKcEEESt10unique_ptrIT_St14default_deleteISD_EEDpOT0_.isra.0, @function
_ZN2v84base11make_uniqueIN12v8_inspector12_GLOBAL__N_120PrimitiveValueMirrorEJRNS_5LocalINS_5ValueEEERPKcEEESt10unique_ptrIT_St14default_deleteISD_EEDpOT0_.isra.0:
.LFB14893:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	movq	%rdx, %rsi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	leaq	-80(%rbp), %rdi
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movl	$96, %edi
	call	_Znwm@PLT
	movq	-80(%rbp), %rsi
	movq	%rax, %rbx
	leaq	16+_ZTVN12v8_inspector12_GLOBAL__N_120PrimitiveValueMirrorE(%rip), %rax
	movq	%rax, (%rbx)
	leaq	32(%rbx), %rax
	leaq	16(%rbx), %rdi
	movq	%rax, 16(%rbx)
	movq	-72(%rbp), %rax
	movq	%r13, 8(%rbx)
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-48(%rbp), %rax
	movq	-80(%rbp), %rdi
	movq	$0, 64(%rbx)
	movq	$0, 88(%rbx)
	movq	%rax, 48(%rbx)
	leaq	72(%rbx), %rax
	movq	%rax, 56(%rbx)
	xorl	%eax, %eax
	movw	%ax, 72(%rbx)
	leaq	-64(%rbp), %rax
	movq	%rbx, (%r12)
	cmpq	%rax, %rdi
	je	.L268
	call	_ZdlPv@PLT
.L268:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L272
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L272:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE14893:
	.size	_ZN2v84base11make_uniqueIN12v8_inspector12_GLOBAL__N_120PrimitiveValueMirrorEJRNS_5LocalINS_5ValueEEERPKcEEESt10unique_ptrIT_St14default_deleteISD_EEDpOT0_.isra.0, .-_ZN2v84base11make_uniqueIN12v8_inspector12_GLOBAL__N_120PrimitiveValueMirrorEJRNS_5LocalINS_5ValueEEERPKcEEESt10unique_ptrIT_St14default_deleteISD_EEDpOT0_.isra.0
	.section	.rodata._ZN12v8_inspector12_GLOBAL__N_115toProtocolValueEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEEiPSt10unique_ptrINS_8protocol5ValueESt14default_deleteIS9_EE.constprop.0.str1.8,"aMS",@progbits,1
	.align 8
.LC9:
	.string	"Object reference chain is too long"
	.align 8
.LC12:
	.string	"Object couldn't be returned by value"
	.section	.text._ZN12v8_inspector12_GLOBAL__N_115toProtocolValueEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEEiPSt10unique_ptrINS_8protocol5ValueESt14default_deleteIS9_EE.constprop.0,"ax",@progbits
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_115toProtocolValueEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEEiPSt10unique_ptrINS_8protocol5ValueESt14default_deleteIS9_EE.constprop.0, @function
_ZN12v8_inspector12_GLOBAL__N_115toProtocolValueEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEEiPSt10unique_ptrINS_8protocol5ValueESt14default_deleteIS9_EE.constprop.0:
.LFB14982:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	je	.L405
	movq	(%rdx), %rax
	movq	%rdx, %r12
	movq	%rsi, %r14
	movl	%ecx, %ebx
	movq	%r8, %r15
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	je	.L406
.L277:
	movq	%r12, %rdi
	call	_ZNK2v85Value9IsBooleanEv@PLT
	testb	%al, %al
	je	.L407
	movq	%r12, %rdi
	call	_ZNK2v87Boolean5ValueEv@PLT
	movl	$24, %edi
	movl	%eax, %ebx
	call	_Znwm@PLT
	movq	(%r15), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rcx
	movl	$1, 8(%rax)
	movq	%rcx, (%rax)
	movb	%bl, 16(%rax)
	movq	%rax, (%r15)
	testq	%rdi, %rdi
	je	.L291
.L400:
	movq	(%rdi), %rax
	call	*24(%rax)
.L291:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
.L273:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L408
	addq	$200, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L406:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	jne	.L277
	movslq	43(%rax), %rax
	subq	$3, %rax
	testq	$-3, %rax
	jne	.L277
	movl	$16, %edi
	leaq	16+_ZTVN12v8_inspector8protocol5ValueE(%rip), %rbx
	call	_Znwm@PLT
	movq	(%r15), %rdi
	movq	%rbx, (%rax)
	movl	$0, 8(%rax)
	movq	%rax, (%r15)
	testq	%rdi, %rdi
	jne	.L400
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L407:
	movq	%r12, %rdi
	call	_ZNK2v85Value8IsNumberEv@PLT
	testb	%al, %al
	jne	.L409
	movq	(%r12), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L289
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	jbe	.L410
.L289:
	leal	-1(%rbx), %eax
	movq	%r12, %rdi
	movl	%eax, -184(%rbp)
	call	_ZNK2v85Value7IsArrayEv@PLT
	testb	%al, %al
	je	.L292
	movl	$40, %edi
	xorl	%ebx, %ebx
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, -192(%rbp)
	call	_ZN12v8_inspector8protocol9ListValueC1Ev@PLT
	movq	%r12, %rdi
	call	_ZNK2v85Array6LengthEv@PLT
	leaq	-112(%rbp), %rcx
	movl	%eax, -208(%rbp)
	movq	%rcx, -200(%rbp)
	testl	%eax, %eax
	je	.L303
.L293:
	movl	%ebx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L411
	movl	-184(%rbp), %ecx
	movq	-200(%rbp), %rdi
	leaq	-176(%rbp), %r8
	movq	%r14, %rsi
	movq	$0, -176(%rbp)
	call	_ZN12v8_inspector12_GLOBAL__N_115toProtocolValueEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEEiPSt10unique_ptrINS_8protocol5ValueESt14default_deleteIS9_EE.constprop.0
	movl	-112(%rbp), %eax
	testl	%eax, %eax
	jne	.L412
	movq	-176(%rbp), %rax
	movq	-192(%rbp), %rdi
	leaq	-168(%rbp), %rsi
	movq	$0, -176(%rbp)
	movq	%rax, -168(%rbp)
	call	_ZN12v8_inspector8protocol9ListValue9pushValueESt10unique_ptrINS0_5ValueESt14default_deleteIS3_EE@PLT
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L300
	movq	(%rdi), %rax
	call	*24(%rax)
.L300:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L301
	call	_ZdlPv@PLT
.L301:
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L302
	movq	(%rdi), %rax
	addl	$1, %ebx
	call	*24(%rax)
	cmpl	%ebx, -208(%rbp)
	jne	.L293
.L303:
	movq	(%r15), %rdi
	movq	-192(%rbp), %rax
	movq	%rax, (%r15)
	testq	%rdi, %rdi
	jne	.L400
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L405:
	leaq	-112(%rbp), %r12
	leaq	.LC9(%rip), %rsi
.L404:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L273
	call	_ZdlPv@PLT
	jmp	.L273
	.p2align 4,,10
	.p2align 3
.L409:
	movq	%r12, %rdi
	call	_ZNK2v86Number5ValueEv@PLT
	comisd	.LC10(%rip), %xmm0
	jb	.L285
	movsd	.LC11(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L285
	movabsq	$-9223372036854775808, %rax
	movq	%xmm0, %rbx
	cmpq	%rax, %rbx
	je	.L285
	cvttsd2sil	%xmm0, %ebx
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L285
	jne	.L285
	movl	$24, %edi
	call	_Znwm@PLT
	movq	(%r15), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rcx
	movl	$2, 8(%rax)
	movq	%rcx, (%rax)
	movl	%ebx, 16(%rax)
	movq	%rax, (%r15)
	testq	%rdi, %rdi
	jne	.L400
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L285:
	movl	$24, %edi
	movsd	%xmm0, -184(%rbp)
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rbx
	call	_Znwm@PLT
	movsd	-184(%rbp), %xmm0
	movq	(%r15), %rdi
	movl	$3, 8(%rax)
	movq	%rbx, (%rax)
	movq	%rax, (%r15)
	movsd	%xmm0, 16(%rax)
	testq	%rdi, %rdi
	jne	.L400
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L302:
	addl	$1, %ebx
	cmpl	%ebx, -208(%rbp)
	jne	.L293
	jmp	.L303
	.p2align 4,,10
	.p2align 3
.L292:
	movq	%r12, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L305
	movl	$96, %edi
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, -200(%rbp)
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object16GetPropertyNamesENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, -208(%rbp)
	testq	%rax, %rax
	je	.L306
	movq	%rax, %rdi
	call	_ZNK2v85Array6LengthEv@PLT
	movl	%eax, -192(%rbp)
	testl	%eax, %eax
	je	.L323
	leaq	-112(%rbp), %rax
	movq	%r13, -216(%rbp)
	xorl	%ebx, %ebx
	movq	-208(%rbp), %r13
	movq	%rax, -224(%rbp)
	movq	%r12, -208(%rbp)
	movq	%r15, -232(%rbp)
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L392:
	movq	(%rax), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L310
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	jbe	.L413
.L310:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNK2v85Value8ToStringENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L312
	movq	-208(%rbp), %rdi
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L401
	movq	(%rdx), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L315
	movq	-1(%rax), %rcx
	cmpw	$67, 11(%rcx)
	jne	.L315
	cmpl	$5, 43(%rax)
	je	.L312
	.p2align 4,,10
	.p2align 3
.L315:
	movl	-184(%rbp), %ecx
	movq	-224(%rbp), %rdi
	leaq	-176(%rbp), %r8
	movq	%r14, %rsi
	movq	$0, -176(%rbp)
	call	_ZN12v8_inspector12_GLOBAL__N_115toProtocolValueEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEEiPSt10unique_ptrINS_8protocol5ValueESt14default_deleteIS9_EE.constprop.0
	movl	-112(%rbp), %eax
	testl	%eax, %eax
	jne	.L414
	movq	-176(%rbp), %rax
	movq	%r14, %rdi
	leaq	-160(%rbp), %r12
	movq	$0, -176(%rbp)
	movq	%rax, -168(%rbp)
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN12v8_inspector16toProtocolStringEPN2v87IsolateENS0_5LocalINS0_6StringEEE@PLT
	movq	-200(%rbp), %rdi
	leaq	-168(%rbp), %rdx
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L320
	call	_ZdlPv@PLT
.L320:
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L321
	movq	(%rdi), %rax
	call	*24(%rax)
.L321:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L322
	call	_ZdlPv@PLT
.L322:
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L312
	movq	(%rdi), %rax
	call	*24(%rax)
.L312:
	addl	$1, %ebx
	cmpl	%ebx, -192(%rbp)
	je	.L415
.L324:
	movl	%ebx, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L392
.L401:
	movq	-216(%rbp), %r13
.L306:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse13InternalErrorEv@PLT
.L325:
	movq	-200(%rbp), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L273
	.p2align 4,,10
	.p2align 3
.L305:
	leaq	-112(%rbp), %r12
	leaq	.LC12(%rip), %rsi
	jmp	.L404
	.p2align 4,,10
	.p2align 3
.L410:
	movq	%r14, %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%r12, %rdx
	leaq	-112(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN12v8_inspector16toProtocolStringEPN2v87IsolateENS0_5LocalINS0_6StringEEE@PLT
	movl	$56, %edi
	call	_Znwm@PLT
	movq	-112(%rbp), %rsi
	movq	%rax, %rbx
	movl	$4, 8(%rax)
	leaq	16+_ZTVN12v8_inspector8protocol11StringValueE(%rip), %rax
	movq	%rax, (%rbx)
	leaq	32(%rbx), %rax
	leaq	16(%rbx), %rdi
	movq	%rax, 16(%rbx)
	movq	-104(%rbp), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-80(%rbp), %rax
	movq	(%r15), %rdi
	movq	%rbx, (%r15)
	movq	%rax, 48(%rbx)
	testq	%rdi, %rdi
	je	.L290
	movq	(%rdi), %rax
	call	*24(%rax)
.L290:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L291
	call	_ZdlPv@PLT
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L411:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse13InternalErrorEv@PLT
.L299:
	movq	-192(%rbp), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L273
	.p2align 4,,10
	.p2align 3
.L413:
	movq	-208(%rbp), %rdi
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	_ZN2v86Object20HasRealNamedPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEE@PLT
	testb	%al, %al
	je	.L312
	shrw	$8, %ax
	jne	.L310
	jmp	.L312
	.p2align 4,,10
	.p2align 3
.L412:
	movl	%eax, 0(%r13)
	leaq	24(%r13), %rax
	leaq	-88(%rbp), %rdx
	movq	%rax, 8(%r13)
	movq	-104(%rbp), %rax
	cmpq	%rdx, %rax
	je	.L416
	movq	%rax, 8(%r13)
	movq	-88(%rbp), %rax
	movq	%rax, 24(%r13)
.L298:
	movq	-96(%rbp), %rax
	movq	-176(%rbp), %rdi
	movq	%rax, 16(%r13)
	movq	-72(%rbp), %rax
	movq	%rax, 40(%r13)
	movl	-64(%rbp), %eax
	movl	%eax, 48(%r13)
	testq	%rdi, %rdi
	je	.L299
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L299
.L415:
	movq	-216(%rbp), %r13
	movq	-232(%rbp), %r15
.L323:
	movq	(%r15), %rdi
	movq	-200(%rbp), %rax
	movq	%rax, (%r15)
	testq	%rdi, %rdi
	jne	.L400
	jmp	.L291
.L416:
	movdqu	-88(%rbp), %xmm2
	movups	%xmm2, 24(%r13)
	jmp	.L298
.L414:
	movq	-216(%rbp), %r13
	leaq	-88(%rbp), %rdx
	movl	%eax, 0(%r13)
	leaq	24(%r13), %rax
	movq	%rax, 8(%r13)
	movq	-104(%rbp), %rax
	cmpq	%rdx, %rax
	je	.L417
	movq	%rax, 8(%r13)
	movq	-88(%rbp), %rax
	movq	%rax, 24(%r13)
.L318:
	movq	-96(%rbp), %rax
	movq	-176(%rbp), %rdi
	movq	%rax, 16(%r13)
	movq	-72(%rbp), %rax
	movq	%rax, 40(%r13)
	movl	-64(%rbp), %eax
	movl	%eax, 48(%r13)
	testq	%rdi, %rdi
	je	.L325
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L325
.L417:
	movdqu	-88(%rbp), %xmm3
	movups	%xmm3, 24(%r13)
	jmp	.L318
.L408:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE14982:
	.size	_ZN12v8_inspector12_GLOBAL__N_115toProtocolValueEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEEiPSt10unique_ptrINS_8protocol5ValueESt14default_deleteIS9_EE.constprop.0, .-_ZN12v8_inspector12_GLOBAL__N_115toProtocolValueEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEEiPSt10unique_ptrINS_8protocol5ValueESt14default_deleteIS9_EE.constprop.0
	.section	.text._ZN2v84base11make_uniqueIN12v8_inspector12_GLOBAL__N_112ObjectMirrorEJRNS_5LocalINS_5ValueEEERA19_KcNS2_8String16EEEESt10unique_ptrIT_St14default_deleteISE_EEDpOT0_.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v84base11make_uniqueIN12v8_inspector12_GLOBAL__N_112ObjectMirrorEJRNS_5LocalINS_5ValueEEERA19_KcNS2_8String16EEEESt10unique_ptrIT_St14default_deleteISE_EEDpOT0_.isra.0, @function
_ZN2v84base11make_uniqueIN12v8_inspector12_GLOBAL__N_112ObjectMirrorEJRNS_5LocalINS_5ValueEEERA19_KcNS2_8String16EEEESt10unique_ptrIT_St14default_deleteISE_EEDpOT0_.isra.0:
.LFB14890:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	movq	%rdx, %rsi
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	leaq	-80(%rbp), %rdi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rcx, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movl	$104, %edi
	call	_Znwm@PLT
	movq	(%r12), %rsi
	movq	%rax, %rbx
	leaq	16+_ZTVN12v8_inspector12_GLOBAL__N_112ObjectMirrorE(%rip), %rax
	movq	%rax, (%rbx)
	leaq	32(%rbx), %rax
	leaq	16(%rbx), %rdi
	movq	%rax, 16(%rbx)
	movq	8(%r12), %rax
	movq	%r14, 8(%rbx)
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	32(%r12), %rax
	movq	-80(%rbp), %rsi
	leaq	64(%rbx), %rdi
	movb	$1, 56(%rbx)
	movq	%rax, 48(%rbx)
	leaq	80(%rbx), %rax
	movq	%rax, 64(%rbx)
	movq	-72(%rbp), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-48(%rbp), %rax
	movq	-80(%rbp), %rdi
	movq	%rbx, 0(%r13)
	movq	%rax, 96(%rbx)
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L418
	call	_ZdlPv@PLT
.L418:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L422
	addq	$48, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L422:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE14890:
	.size	_ZN2v84base11make_uniqueIN12v8_inspector12_GLOBAL__N_112ObjectMirrorEJRNS_5LocalINS_5ValueEEERA19_KcNS2_8String16EEEESt10unique_ptrIT_St14default_deleteISE_EEDpOT0_.isra.0, .-_ZN2v84base11make_uniqueIN12v8_inspector12_GLOBAL__N_112ObjectMirrorEJRNS_5LocalINS_5ValueEEERA19_KcNS2_8String16EEEESt10unique_ptrIT_St14default_deleteISE_EEDpOT0_.isra.0
	.set	_ZN2v84base11make_uniqueIN12v8_inspector12_GLOBAL__N_112ObjectMirrorEJRNS_5LocalINS_5ValueEEERA15_KcNS2_8String16EEEESt10unique_ptrIT_St14default_deleteISE_EEDpOT0_.isra.0,_ZN2v84base11make_uniqueIN12v8_inspector12_GLOBAL__N_112ObjectMirrorEJRNS_5LocalINS_5ValueEEERA19_KcNS2_8String16EEEESt10unique_ptrIT_St14default_deleteISE_EEDpOT0_.isra.0
	.section	.text._ZN2v84base11make_uniqueIN12v8_inspector12_GLOBAL__N_112ObjectMirrorEJRNS_5LocalINS_5ValueEEERPKcNS2_8String16EEEESt10unique_ptrIT_St14default_deleteISE_EEDpOT0_.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v84base11make_uniqueIN12v8_inspector12_GLOBAL__N_112ObjectMirrorEJRNS_5LocalINS_5ValueEEERPKcNS2_8String16EEEESt10unique_ptrIT_St14default_deleteISE_EEDpOT0_.isra.0, @function
_ZN2v84base11make_uniqueIN12v8_inspector12_GLOBAL__N_112ObjectMirrorEJRNS_5LocalINS_5ValueEEERPKcNS2_8String16EEEESt10unique_ptrIT_St14default_deleteISE_EEDpOT0_.isra.0:
.LFB14886:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	movq	%rdx, %rsi
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	leaq	-80(%rbp), %rdi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rcx, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movl	$104, %edi
	call	_Znwm@PLT
	movq	(%r12), %rsi
	movq	%rax, %rbx
	leaq	16+_ZTVN12v8_inspector12_GLOBAL__N_112ObjectMirrorE(%rip), %rax
	movq	%rax, (%rbx)
	leaq	32(%rbx), %rax
	leaq	16(%rbx), %rdi
	movq	%rax, 16(%rbx)
	movq	8(%r12), %rax
	movq	%r14, 8(%rbx)
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	32(%r12), %rax
	movq	-80(%rbp), %rsi
	leaq	64(%rbx), %rdi
	movb	$1, 56(%rbx)
	movq	%rax, 48(%rbx)
	leaq	80(%rbx), %rax
	movq	%rax, 64(%rbx)
	movq	-72(%rbp), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-48(%rbp), %rax
	movq	-80(%rbp), %rdi
	movq	%rbx, 0(%r13)
	movq	%rax, 96(%rbx)
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L423
	call	_ZdlPv@PLT
.L423:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L427
	addq	$48, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L427:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE14886:
	.size	_ZN2v84base11make_uniqueIN12v8_inspector12_GLOBAL__N_112ObjectMirrorEJRNS_5LocalINS_5ValueEEERPKcNS2_8String16EEEESt10unique_ptrIT_St14default_deleteISE_EEDpOT0_.isra.0, .-_ZN2v84base11make_uniqueIN12v8_inspector12_GLOBAL__N_112ObjectMirrorEJRNS_5LocalINS_5ValueEEERPKcNS2_8String16EEEESt10unique_ptrIT_St14default_deleteISE_EEDpOT0_.isra.0
	.section	.text._ZN12v8_inspector8protocol7Runtime13ObjectPreviewD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD2Ev, @function
_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD2Ev:
.LFB4640:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	160(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L429
	movq	8(%r13), %rax
	movq	0(%r13), %r12
	movq	%rax, -56(%rbp)
	cmpq	%r12, %rax
	je	.L430
	movq	%r13, -64(%rbp)
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %r15
	jmp	.L437
	.p2align 4,,10
	.p2align 3
.L518:
	movq	16(%r14), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r14)
	testq	%r13, %r13
	je	.L433
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%r15, %rax
	jne	.L434
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L433:
	movq	8(%r14), %r13
	testq	%r13, %r13
	je	.L435
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%r15, %rax
	jne	.L436
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L435:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L431:
	addq	$8, %r12
	cmpq	%r12, -56(%rbp)
	je	.L517
.L437:
	movq	(%r12), %r14
	testq	%r14, %r14
	je	.L431
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L518
	movq	%r14, %rdi
	addq	$8, %r12
	call	*%rax
	cmpq	%r12, -56(%rbp)
	jne	.L437
	.p2align 4,,10
	.p2align 3
.L517:
	movq	-64(%rbp), %r13
	movq	0(%r13), %r12
.L430:
	testq	%r12, %r12
	je	.L438
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L438:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L429:
	movq	152(%rbx), %rax
	movq	%rax, -64(%rbp)
	testq	%rax, %rax
	je	.L439
	movq	8(%rax), %rcx
	movq	(%rax), %r15
	movq	%rcx, -88(%rbp)
	cmpq	%r15, %rcx
	je	.L440
	movq	%r15, -56(%rbp)
	movq	%rbx, -96(%rbp)
	.p2align 4,,10
	.p2align 3
.L463:
	movq	-56(%rbp), %rax
	movq	(%rax), %r12
	testq	%r12, %r12
	je	.L441
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L442
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L443
	call	_ZdlPv@PLT
.L443:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L444
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L445
	movq	160(%r13), %r15
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r15, %r15
	je	.L446
	movq	8(%r15), %rax
	movq	(%r15), %r14
	movq	%rax, -80(%rbp)
	cmpq	%r14, %rax
	jne	.L454
	jmp	.L447
	.p2align 4,,10
	.p2align 3
.L520:
	movq	16(%rbx), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%rbx)
	testq	%rdi, %rdi
	je	.L450
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L451
	movq	%rdi, -72(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L450:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L452
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L453
	movq	%rdi, -72(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L452:
	movl	$24, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L448:
	addq	$8, %r14
	cmpq	%r14, -80(%rbp)
	je	.L519
.L454:
	movq	(%r14), %rbx
	testq	%rbx, %rbx
	je	.L448
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L520
	movq	%rbx, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -80(%rbp)
	jne	.L454
	.p2align 4,,10
	.p2align 3
.L519:
	movq	(%r15), %r14
.L447:
	testq	%r14, %r14
	je	.L455
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L455:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L446:
	movq	152(%r13), %rdi
	testq	%rdi, %rdi
	je	.L456
	call	_ZNKSt14default_deleteISt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime15PropertyPreviewES_IS5_EESaIS7_EEEclEPS9_.isra.0.part.0
.L456:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L457
	call	_ZdlPv@PLT
.L457:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L458
	call	_ZdlPv@PLT
.L458:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L459
	call	_ZdlPv@PLT
.L459:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L444:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L460
	call	_ZdlPv@PLT
.L460:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L461
	call	_ZdlPv@PLT
.L461:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L462
	call	_ZdlPv@PLT
.L462:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L441:
	addq	$8, -56(%rbp)
	movq	-56(%rbp), %rax
	cmpq	%rax, -88(%rbp)
	jne	.L463
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rbx
	movq	(%rax), %r15
.L440:
	testq	%r15, %r15
	je	.L464
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L464:
	movq	-64(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L439:
	movq	104(%rbx), %rdi
	leaq	120(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L465
	call	_ZdlPv@PLT
.L465:
	movq	56(%rbx), %rdi
	leaq	72(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L466
	call	_ZdlPv@PLT
.L466:
	movq	8(%rbx), %r8
	leaq	24(%rbx), %rdi
	cmpq	%rdi, %r8
	je	.L428
	addq	$56, %rsp
	movq	%r8, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L442:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L441
	.p2align 4,,10
	.p2align 3
.L428:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L451:
	.cfi_restore_state
	call	*%rax
	jmp	.L450
	.p2align 4,,10
	.p2align 3
.L453:
	call	*%rax
	jmp	.L452
	.p2align 4,,10
	.p2align 3
.L434:
	call	*%rax
	jmp	.L433
	.p2align 4,,10
	.p2align 3
.L445:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L444
	.p2align 4,,10
	.p2align 3
.L436:
	call	*%rax
	jmp	.L435
	.cfi_endproc
.LFE4640:
	.size	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD2Ev, .-_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev,_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD2Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime12EntryPreviewD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12EntryPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD2Ev, @function
_ZN12v8_inspector8protocol7Runtime12EntryPreviewD2Ev:
.LFB4753:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	16(%rdi), %r12
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L522
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L523
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L522:
	movq	8(%rbx), %r12
	testq	%r12, %r12
	je	.L521
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L525
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	popq	%rbx
	movq	%r12, %rdi
	movl	$168, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L521:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L523:
	.cfi_restore_state
	call	*%rax
	jmp	.L522
	.p2align 4,,10
	.p2align 3
.L525:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE4753:
	.size	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD2Ev, .-_ZN12v8_inspector8protocol7Runtime12EntryPreviewD2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD1Ev,_ZN12v8_inspector8protocol7Runtime12EntryPreviewD2Ev
	.section	.text._ZNKSt14default_deleteISt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime15PropertyPreviewES_IS5_EESaIS7_EEEclEPS9_.isra.0.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNKSt14default_deleteISt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime15PropertyPreviewES_IS5_EESaIS7_EEEclEPS9_.isra.0.part.0, @function
_ZNKSt14default_deleteISt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime15PropertyPreviewES_IS5_EESaIS7_EEEclEPS9_.isra.0.part.0:
.LFB14914:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rdx
	movq	(%rdi), %rbx
	movq	%rdi, -64(%rbp)
	movq	%rdx, -72(%rbp)
	cmpq	%rbx, %rdx
	je	.L531
	movq	%rbx, -56(%rbp)
	.p2align 4,,10
	.p2align 3
.L554:
	movq	-56(%rbp), %rax
	movq	(%rax), %r12
	testq	%r12, %r12
	je	.L532
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L533
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L534
	call	_ZdlPv@PLT
.L534:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L535
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L536
	movq	160(%r13), %r15
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r15, %r15
	je	.L537
	movq	8(%r15), %rax
	movq	(%r15), %r14
	movq	%rax, -80(%rbp)
	cmpq	%r14, %rax
	je	.L538
	movq	%r14, %rbx
	jmp	.L545
	.p2align 4,,10
	.p2align 3
.L587:
	movq	16(%r14), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r14)
	testq	%rdi, %rdi
	je	.L541
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L542
	movq	%rdi, -88(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-88(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L541:
	movq	8(%r14), %rdi
	testq	%rdi, %rdi
	je	.L543
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L544
	movq	%rdi, -88(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-88(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L543:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L539:
	addq	$8, %rbx
	cmpq	%rbx, -80(%rbp)
	je	.L586
.L545:
	movq	(%rbx), %r14
	testq	%r14, %r14
	je	.L539
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L587
	movq	%r14, %rdi
	addq	$8, %rbx
	call	*%rax
	cmpq	%rbx, -80(%rbp)
	jne	.L545
	.p2align 4,,10
	.p2align 3
.L586:
	movq	(%r15), %r14
.L538:
	testq	%r14, %r14
	je	.L546
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L546:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L537:
	movq	152(%r13), %rdi
	testq	%rdi, %rdi
	je	.L547
	call	_ZNKSt14default_deleteISt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime15PropertyPreviewES_IS5_EESaIS7_EEEclEPS9_.isra.0.part.0
.L547:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L548
	call	_ZdlPv@PLT
.L548:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L549
	call	_ZdlPv@PLT
.L549:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L550
	call	_ZdlPv@PLT
.L550:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L535:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L551
	call	_ZdlPv@PLT
.L551:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L552
	call	_ZdlPv@PLT
.L552:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L553
	call	_ZdlPv@PLT
.L553:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L532:
	addq	$8, -56(%rbp)
	movq	-56(%rbp), %rax
	cmpq	%rax, -72(%rbp)
	jne	.L554
	movq	-64(%rbp), %rax
	movq	(%rax), %rbx
.L531:
	testq	%rbx, %rbx
	je	.L555
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L555:
	movq	-64(%rbp), %rdi
	addq	$56, %rsp
	movl	$24, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L533:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L532
	.p2align 4,,10
	.p2align 3
.L542:
	call	*%rax
	jmp	.L541
	.p2align 4,,10
	.p2align 3
.L544:
	call	*%rax
	jmp	.L543
	.p2align 4,,10
	.p2align 3
.L536:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L535
	.cfi_endproc
.LFE14914:
	.size	_ZNKSt14default_deleteISt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime15PropertyPreviewES_IS5_EESaIS7_EEEclEPS9_.isra.0.part.0, .-_ZNKSt14default_deleteISt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime15PropertyPreviewES_IS5_EESaIS7_EEEclEPS9_.isra.0.part.0
	.section	.text._ZN12v8_inspector8protocol7Runtime15PropertyPreviewD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD2Ev, @function
_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD2Ev:
.LFB4713:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	movq	152(%rdi), %rdi
	leaq	168(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L589
	call	_ZdlPv@PLT
.L589:
	movq	136(%rbx), %r12
	testq	%r12, %r12
	je	.L590
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L591
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L590:
	movq	96(%rbx), %rdi
	leaq	112(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L592
	call	_ZdlPv@PLT
.L592:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L593
	call	_ZdlPv@PLT
.L593:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L588
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L588:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L591:
	.cfi_restore_state
	call	*%rax
	jmp	.L590
	.cfi_endproc
.LFE4713:
	.size	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD2Ev, .-_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD1Ev,_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD2Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev, @function
_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev:
.LFB4642:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	160(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L600
	movq	8(%r13), %rax
	movq	0(%r13), %r12
	movq	%rax, -56(%rbp)
	cmpq	%r12, %rax
	je	.L601
	movq	%r13, -64(%rbp)
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %r15
	jmp	.L608
	.p2align 4,,10
	.p2align 3
.L689:
	movq	16(%r14), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r14)
	testq	%r13, %r13
	je	.L604
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%r15, %rax
	jne	.L605
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L604:
	movq	8(%r14), %r13
	testq	%r13, %r13
	je	.L606
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%r15, %rax
	jne	.L607
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L606:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L602:
	addq	$8, %r12
	cmpq	%r12, -56(%rbp)
	je	.L688
.L608:
	movq	(%r12), %r14
	testq	%r14, %r14
	je	.L602
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L689
	movq	%r14, %rdi
	addq	$8, %r12
	call	*%rax
	cmpq	%r12, -56(%rbp)
	jne	.L608
	.p2align 4,,10
	.p2align 3
.L688:
	movq	-64(%rbp), %r13
	movq	0(%r13), %r12
.L601:
	testq	%r12, %r12
	je	.L609
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L609:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L600:
	movq	152(%rbx), %rax
	movq	%rax, -64(%rbp)
	testq	%rax, %rax
	je	.L610
	movq	8(%rax), %rcx
	movq	(%rax), %r15
	movq	%rcx, -88(%rbp)
	cmpq	%r15, %rcx
	je	.L611
	movq	%r15, -56(%rbp)
	movq	%rbx, -96(%rbp)
	.p2align 4,,10
	.p2align 3
.L634:
	movq	-56(%rbp), %rax
	movq	(%rax), %r12
	testq	%r12, %r12
	je	.L612
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L613
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L614
	call	_ZdlPv@PLT
.L614:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L615
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L616
	movq	160(%r13), %r15
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r15, %r15
	je	.L617
	movq	8(%r15), %rax
	movq	(%r15), %r14
	movq	%rax, -80(%rbp)
	cmpq	%r14, %rax
	jne	.L625
	jmp	.L618
	.p2align 4,,10
	.p2align 3
.L691:
	movq	16(%rbx), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%rbx)
	testq	%rdi, %rdi
	je	.L621
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L622
	movq	%rdi, -72(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L621:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L623
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L624
	movq	%rdi, -72(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L623:
	movl	$24, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L619:
	addq	$8, %r14
	cmpq	%r14, -80(%rbp)
	je	.L690
.L625:
	movq	(%r14), %rbx
	testq	%rbx, %rbx
	je	.L619
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L691
	movq	%rbx, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -80(%rbp)
	jne	.L625
	.p2align 4,,10
	.p2align 3
.L690:
	movq	(%r15), %r14
.L618:
	testq	%r14, %r14
	je	.L626
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L626:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L617:
	movq	152(%r13), %rdi
	testq	%rdi, %rdi
	je	.L627
	call	_ZNKSt14default_deleteISt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime15PropertyPreviewES_IS5_EESaIS7_EEEclEPS9_.isra.0.part.0
.L627:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L628
	call	_ZdlPv@PLT
.L628:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L629
	call	_ZdlPv@PLT
.L629:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L630
	call	_ZdlPv@PLT
.L630:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L615:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L631
	call	_ZdlPv@PLT
.L631:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L632
	call	_ZdlPv@PLT
.L632:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L633
	call	_ZdlPv@PLT
.L633:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L612:
	addq	$8, -56(%rbp)
	movq	-56(%rbp), %rax
	cmpq	%rax, -88(%rbp)
	jne	.L634
.L692:
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rbx
	movq	(%rax), %r15
.L611:
	testq	%r15, %r15
	je	.L635
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L635:
	movq	-64(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L610:
	movq	104(%rbx), %rdi
	leaq	120(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L636
	call	_ZdlPv@PLT
.L636:
	movq	56(%rbx), %rdi
	leaq	72(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L637
	call	_ZdlPv@PLT
.L637:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L638
	call	_ZdlPv@PLT
.L638:
	addq	$56, %rsp
	movq	%rbx, %rdi
	movl	$168, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L613:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	addq	$8, -56(%rbp)
	movq	-56(%rbp), %rax
	cmpq	%rax, -88(%rbp)
	jne	.L634
	jmp	.L692
	.p2align 4,,10
	.p2align 3
.L624:
	call	*%rax
	jmp	.L623
	.p2align 4,,10
	.p2align 3
.L622:
	call	*%rax
	jmp	.L621
	.p2align 4,,10
	.p2align 3
.L607:
	call	*%rax
	jmp	.L606
	.p2align 4,,10
	.p2align 3
.L616:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L615
	.p2align 4,,10
	.p2align 3
.L605:
	call	*%rax
	jmp	.L604
	.cfi_endproc
.LFE4642:
	.size	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev, .-_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12EntryPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev, @function
_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev:
.LFB4755:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r12
	movq	%r15, (%rdi)
	testq	%r12, %r12
	je	.L694
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rbx
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L695
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	movq	160(%r12), %rax
	movq	%rax, -80(%rbp)
	testq	%rax, %rax
	je	.L696
	movq	8(%rax), %rdx
	movq	(%rax), %r14
	movq	%rdx, -72(%rbp)
	cmpq	%r14, %rdx
	jne	.L704
	jmp	.L697
	.p2align 4,,10
	.p2align 3
.L772:
	movq	16(%r8), %rdi
	movq	%r15, (%r8)
	testq	%rdi, %rdi
	je	.L700
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L701
	movq	%r8, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-56(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
	movq	-64(%rbp), %r8
.L700:
	movq	8(%r8), %rdi
	testq	%rdi, %rdi
	je	.L702
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L703
	movq	%r8, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-56(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
	movq	-64(%rbp), %r8
.L702:
	movl	$24, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L698:
	addq	$8, %r14
	cmpq	%r14, -72(%rbp)
	je	.L771
.L704:
	movq	(%r14), %r8
	testq	%r8, %r8
	je	.L698
	movq	(%r8), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L772
	movq	%r8, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -72(%rbp)
	jne	.L704
	.p2align 4,,10
	.p2align 3
.L771:
	movq	-80(%rbp), %rax
	movq	(%rax), %r14
.L697:
	testq	%r14, %r14
	je	.L705
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L705:
	movq	-80(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L696:
	movq	152(%r12), %rdi
	testq	%rdi, %rdi
	je	.L706
	call	_ZNKSt14default_deleteISt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime15PropertyPreviewES_IS5_EESaIS7_EEEclEPS9_.isra.0.part.0
.L706:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L707
	call	_ZdlPv@PLT
.L707:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L708
	call	_ZdlPv@PLT
.L708:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L709
	call	_ZdlPv@PLT
.L709:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L694:
	movq	8(%r13), %r12
	testq	%r12, %r12
	je	.L710
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rbx
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L711
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	movq	160(%r12), %rax
	movq	%rax, -80(%rbp)
	testq	%rax, %rax
	je	.L712
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	movq	%rcx, -72(%rbp)
	cmpq	%r14, %rcx
	jne	.L720
	jmp	.L713
	.p2align 4,,10
	.p2align 3
.L774:
	movq	16(%r8), %rdi
	movq	%r15, (%r8)
	testq	%rdi, %rdi
	je	.L716
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L717
	movq	%r8, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-56(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
	movq	-64(%rbp), %r8
.L716:
	movq	8(%r8), %rdi
	testq	%rdi, %rdi
	je	.L718
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L719
	movq	%r8, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-56(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
	movq	-64(%rbp), %r8
.L718:
	movl	$24, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L714:
	addq	$8, %r14
	cmpq	%r14, -72(%rbp)
	je	.L773
.L720:
	movq	(%r14), %r8
	testq	%r8, %r8
	je	.L714
	movq	(%r8), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L774
	movq	%r8, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -72(%rbp)
	jne	.L720
	.p2align 4,,10
	.p2align 3
.L773:
	movq	-80(%rbp), %rax
	movq	(%rax), %r14
.L713:
	testq	%r14, %r14
	je	.L721
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L721:
	movq	-80(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L712:
	movq	152(%r12), %rdi
	testq	%rdi, %rdi
	je	.L722
	call	_ZNKSt14default_deleteISt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime15PropertyPreviewES_IS5_EESaIS7_EEEclEPS9_.isra.0.part.0
.L722:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L723
	call	_ZdlPv@PLT
.L723:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L724
	call	_ZdlPv@PLT
.L724:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L725
	call	_ZdlPv@PLT
.L725:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L710:
	addq	$40, %rsp
	movq	%r13, %rdi
	movl	$24, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L703:
	.cfi_restore_state
	movq	%r8, -56(%rbp)
	call	*%rax
	movq	-56(%rbp), %r8
	jmp	.L702
	.p2align 4,,10
	.p2align 3
.L701:
	movq	%r8, -56(%rbp)
	call	*%rax
	movq	-56(%rbp), %r8
	jmp	.L700
	.p2align 4,,10
	.p2align 3
.L719:
	movq	%r8, -56(%rbp)
	call	*%rax
	movq	-56(%rbp), %r8
	jmp	.L718
	.p2align 4,,10
	.p2align 3
.L717:
	movq	%r8, -56(%rbp)
	call	*%rax
	movq	-56(%rbp), %r8
	jmp	.L716
	.p2align 4,,10
	.p2align 3
.L695:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L694
	.p2align 4,,10
	.p2align 3
.L711:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L710
	.cfi_endproc
.LFE4755:
	.size	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev, .-_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev, @function
_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev:
.LFB4715:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%rax, (%rdi)
	movq	152(%rdi), %rdi
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L776
	call	_ZdlPv@PLT
.L776:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L777
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rbx
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L778
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -72(%rbp)
	testq	%rax, %rax
	je	.L779
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	movq	%rcx, -64(%rbp)
	cmpq	%r14, %rcx
	jne	.L787
	jmp	.L780
	.p2align 4,,10
	.p2align 3
.L820:
	movq	16(%r15), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%rdi, %rdi
	je	.L783
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L784
	movq	%rdi, -56(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-56(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L783:
	movq	8(%r15), %rdi
	testq	%rdi, %rdi
	je	.L785
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L786
	movq	%rdi, -56(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-56(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L785:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L781:
	addq	$8, %r14
	cmpq	%r14, -64(%rbp)
	je	.L819
.L787:
	movq	(%r14), %r15
	testq	%r15, %r15
	je	.L781
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L820
	movq	%r15, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -64(%rbp)
	jne	.L787
	.p2align 4,,10
	.p2align 3
.L819:
	movq	-72(%rbp), %rax
	movq	(%rax), %r14
.L780:
	testq	%r14, %r14
	je	.L788
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L788:
	movq	-72(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L779:
	movq	152(%r13), %rdi
	testq	%rdi, %rdi
	je	.L789
	call	_ZNKSt14default_deleteISt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime15PropertyPreviewES_IS5_EESaIS7_EEEclEPS9_.isra.0.part.0
.L789:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L790
	call	_ZdlPv@PLT
.L790:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L791
	call	_ZdlPv@PLT
.L791:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L792
	call	_ZdlPv@PLT
.L792:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L777:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L793
	call	_ZdlPv@PLT
.L793:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L794
	call	_ZdlPv@PLT
.L794:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L795
	call	_ZdlPv@PLT
.L795:
	addq	$40, %rsp
	movq	%r12, %rdi
	movl	$192, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L786:
	.cfi_restore_state
	call	*%rax
	jmp	.L785
	.p2align 4,,10
	.p2align 3
.L784:
	call	*%rax
	jmp	.L783
	.p2align 4,,10
	.p2align 3
.L778:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L777
	.cfi_endproc
.LFE4715:
	.size	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev, .-_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev
	.section	.rodata._ZNK12v8_inspector8String169substringEmm.str1.1,"aMS",@progbits,1
.LC13:
	.string	"basic_string::substr"
	.section	.rodata._ZNK12v8_inspector8String169substringEmm.str1.8,"aMS",@progbits,1
	.align 8
.LC14:
	.string	"%s: __pos (which is %zu) > this->size() (which is %zu)"
	.section	.text._ZNK12v8_inspector8String169substringEmm,"axG",@progbits,_ZNK12v8_inspector8String169substringEmm,comdat
	.align 2
	.p2align 4
	.weak	_ZNK12v8_inspector8String169substringEmm
	.type	_ZNK12v8_inspector8String169substringEmm, @function
_ZNK12v8_inspector8String169substringEmm:
.LFB3062:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rsi), %r8
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%r8, %rdx
	ja	.L841
	subq	%rdx, %r8
	movq	(%rsi), %rax
	movq	%rcx, %rbx
	leaq	-80(%rbp), %r14
	cmpq	%rcx, %r8
	movq	%r14, -96(%rbp)
	movq	%rdi, %r13
	cmovbe	%r8, %rbx
	leaq	(%rax,%rdx,2), %r15
	movq	%r15, %rax
	leaq	(%rbx,%rbx), %r12
	addq	%r12, %rax
	je	.L823
	testq	%r15, %r15
	je	.L842
.L823:
	movq	%r12, %rcx
	movq	%r14, %rdi
	sarq	%rcx
	cmpq	$14, %r12
	ja	.L843
.L824:
	cmpq	$2, %r12
	je	.L844
	testq	%r12, %r12
	je	.L827
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%rcx, -104(%rbp)
	call	memmove@PLT
	movq	-96(%rbp), %rdi
	movq	-104(%rbp), %rcx
.L827:
	xorl	%eax, %eax
	movq	%rcx, -88(%rbp)
	leaq	-96(%rbp), %rsi
	movw	%ax, (%rdi,%rbx,2)
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EONSt7__cxx1112basic_stringItSt11char_traitsItESaItEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L821
	call	_ZdlPv@PLT
.L821:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L845
	addq	$72, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L844:
	.cfi_restore_state
	movzwl	(%r15), %eax
	movw	%ax, (%rdi)
	movq	-96(%rbp), %rdi
	jmp	.L827
	.p2align 4,,10
	.p2align 3
.L843:
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %rcx
	ja	.L846
	leaq	2(%r12), %rdi
	movq	%rcx, -104(%rbp)
	call	_Znwm@PLT
	movq	-104(%rbp), %rcx
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	%rcx, -80(%rbp)
	jmp	.L824
.L842:
	leaq	.LC7(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L841:
	movq	%r8, %rcx
	leaq	.LC13(%rip), %rsi
	leaq	.LC14(%rip), %rdi
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L845:
	call	__stack_chk_fail@PLT
.L846:
	leaq	.LC8(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE3062:
	.size	_ZNK12v8_inspector8String169substringEmm, .-_ZNK12v8_inspector8String169substringEmm
	.section	.text._ZN12v8_inspector8protocol7Runtime12RemoteObjectD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12RemoteObjectD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD2Ev, @function
_ZN12v8_inspector8protocol7Runtime12RemoteObjectD2Ev:
.LFB4457:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12RemoteObjectE(%rip), %rax
	leaq	-64(%rax), %rcx
	movq	%rax, %xmm1
	movq	%rcx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	punpcklqdq	%xmm1, %xmm0
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	312(%rdi), %r12
	movups	%xmm0, (%rdi)
	testq	%r12, %r12
	je	.L848
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L849
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE(%rip), %rax
	movq	56(%r12), %rdi
	movq	%rax, (%r12)
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L850
	call	_ZdlPv@PLT
.L850:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L851
	call	_ZdlPv@PLT
.L851:
	movl	$96, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L848:
	movq	304(%rbx), %r15
	testq	%r15, %r15
	je	.L852
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L853
	movq	160(%r15), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r13, %r13
	je	.L854
	movq	8(%r13), %r14
	movq	0(%r13), %r12
	cmpq	%r12, %r14
	je	.L855
	movq	%r15, -56(%rbp)
	movq	%r13, -64(%rbp)
	jmp	.L862
	.p2align 4,,10
	.p2align 3
.L959:
	movq	16(%r13), %r15
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r15, %r15
	je	.L858
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%r15, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L859
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L858:
	movq	8(%r13), %r15
	testq	%r15, %r15
	je	.L860
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r15, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L861
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L860:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L856:
	addq	$8, %r12
	cmpq	%r12, %r14
	je	.L958
.L862:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L856
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L959
	addq	$8, %r12
	movq	%r13, %rdi
	call	*%rax
	cmpq	%r12, %r14
	jne	.L862
	.p2align 4,,10
	.p2align 3
.L958:
	movq	-64(%rbp), %r13
	movq	-56(%rbp), %r15
	movq	0(%r13), %r12
.L855:
	testq	%r12, %r12
	je	.L863
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L863:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L854:
	movq	152(%r15), %rax
	movq	%rax, -80(%rbp)
	testq	%rax, %rax
	je	.L864
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -72(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rcx
	je	.L865
	movq	%r15, -96(%rbp)
	movq	%rbx, -104(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L888:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L866
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L867
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L868
	call	_ZdlPv@PLT
.L868:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L869
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L870
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rax, %rax
	je	.L871
	movq	8(%rax), %rsi
	movq	(%rax), %r14
	movq	%rsi, -64(%rbp)
	cmpq	%r14, %rsi
	je	.L872
	movq	%r12, -88(%rbp)
	jmp	.L879
	.p2align 4,,10
	.p2align 3
.L961:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L875
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L876
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L875:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L877
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L878
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L877:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L873:
	addq	$8, %r14
	cmpq	%r14, -64(%rbp)
	je	.L960
.L879:
	movq	(%r14), %r15
	testq	%r15, %r15
	je	.L873
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L961
	movq	%r15, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -64(%rbp)
	jne	.L879
	.p2align 4,,10
	.p2align 3
.L960:
	movq	-56(%rbp), %rax
	movq	-88(%rbp), %r12
	movq	(%rax), %r14
.L872:
	testq	%r14, %r14
	je	.L880
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L880:
	movq	-56(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L871:
	movq	152(%r13), %rdi
	testq	%rdi, %rdi
	je	.L881
	call	_ZNKSt14default_deleteISt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime15PropertyPreviewES_IS5_EESaIS7_EEEclEPS9_.isra.0.part.0
.L881:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L882
	call	_ZdlPv@PLT
.L882:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L883
	call	_ZdlPv@PLT
.L883:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L884
	call	_ZdlPv@PLT
.L884:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L869:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L885
	call	_ZdlPv@PLT
.L885:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L886
	call	_ZdlPv@PLT
.L886:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L887
	call	_ZdlPv@PLT
.L887:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L866:
	addq	$8, %rbx
	cmpq	%rbx, -72(%rbp)
	jne	.L888
	movq	-80(%rbp), %rax
	movq	-96(%rbp), %r15
	movq	-104(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L865:
	testq	%rdi, %rdi
	je	.L889
	call	_ZdlPv@PLT
.L889:
	movq	-80(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L864:
	movq	104(%r15), %rdi
	leaq	120(%r15), %rax
	cmpq	%rax, %rdi
	je	.L890
	call	_ZdlPv@PLT
.L890:
	movq	56(%r15), %rdi
	leaq	72(%r15), %rax
	cmpq	%rax, %rdi
	je	.L891
	call	_ZdlPv@PLT
.L891:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L892
	call	_ZdlPv@PLT
.L892:
	movl	$168, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L852:
	movq	264(%rbx), %rdi
	leaq	280(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L893
	call	_ZdlPv@PLT
.L893:
	movq	216(%rbx), %rdi
	leaq	232(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L894
	call	_ZdlPv@PLT
.L894:
	movq	168(%rbx), %rdi
	leaq	184(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L895
	call	_ZdlPv@PLT
.L895:
	movq	152(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L896
	movq	(%rdi), %rax
	call	*24(%rax)
.L896:
	movq	112(%rbx), %rdi
	leaq	128(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L897
	call	_ZdlPv@PLT
.L897:
	movq	64(%rbx), %rdi
	leaq	80(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L898
	call	_ZdlPv@PLT
.L898:
	movq	16(%rbx), %r8
	leaq	32(%rbx), %rdi
	cmpq	%rdi, %r8
	je	.L847
	addq	$72, %rsp
	movq	%r8, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L847:
	.cfi_restore_state
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L867:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L866
	.p2align 4,,10
	.p2align 3
.L878:
	call	*%rax
	jmp	.L877
	.p2align 4,,10
	.p2align 3
.L876:
	call	*%rax
	jmp	.L875
	.p2align 4,,10
	.p2align 3
.L861:
	call	*%rax
	jmp	.L860
	.p2align 4,,10
	.p2align 3
.L859:
	call	*%rax
	jmp	.L858
	.p2align 4,,10
	.p2align 3
.L870:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L869
	.p2align 4,,10
	.p2align 3
.L853:
	movq	%r15, %rdi
	call	*%rax
	jmp	.L852
	.p2align 4,,10
	.p2align 3
.L849:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L848
	.cfi_endproc
.LFE4457:
	.size	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD2Ev, .-_ZN12v8_inspector8protocol7Runtime12RemoteObjectD2Ev
	.set	.LTHUNK0,_ZN12v8_inspector8protocol7Runtime12RemoteObjectD2Ev
	.p2align 4
	.weak	_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	.type	_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD1Ev, @function
_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD1Ev:
.LFB15010:
	.cfi_startproc
	endbr64
	subq	$8, %rdi
	jmp	.LTHUNK0
	.cfi_endproc
.LFE15010:
	.size	_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD1Ev, .-_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	.weak	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev,_ZN12v8_inspector8protocol7Runtime12RemoteObjectD2Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12RemoteObjectD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev, @function
_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev:
.LFB4459:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$320, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4459:
	.size	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev, .-_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev
	.p2align 4
	.weak	_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD0Ev
	.type	_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD0Ev, @function
_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD0Ev:
.LFB14978:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-8(%rdi), %r12
	movq	%r12, %rdi
	subq	$8, %rsp
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$320, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE14978:
	.size	_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD0Ev, .-_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime12RemoteObjectC2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12RemoteObjectC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12RemoteObjectC2Ev
	.type	_ZN12v8_inspector8protocol7Runtime12RemoteObjectC2Ev, @function
_ZN12v8_inspector8protocol7Runtime12RemoteObjectC2Ev:
.LFB4603:
	.cfi_startproc
	endbr64
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12RemoteObjectE(%rip), %rax
	movq	$0, 24(%rdi)
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm1
	leaq	32(%rdi), %rax
	movq	$0, 48(%rdi)
	movq	%rax, 16(%rdi)
	xorl	%eax, %eax
	movq	%rdx, %xmm0
	movw	%ax, 32(%rdi)
	leaq	80(%rdi), %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, 64(%rdi)
	leaq	128(%rdi), %rax
	movq	%rax, 112(%rdi)
	leaq	184(%rdi), %rax
	movq	%rax, 168(%rdi)
	leaq	232(%rdi), %rax
	movups	%xmm0, (%rdi)
	pxor	%xmm0, %xmm0
	movq	%rax, 216(%rdi)
	leaq	280(%rdi), %rax
	movups	%xmm0, 80(%rdi)
	movups	%xmm0, 128(%rdi)
	movups	%xmm0, 184(%rdi)
	movups	%xmm0, 232(%rdi)
	movups	%xmm0, 280(%rdi)
	pxor	%xmm0, %xmm0
	movb	$0, 56(%rdi)
	movq	$0, 96(%rdi)
	movq	$0, 72(%rdi)
	movb	$0, 104(%rdi)
	movq	$0, 144(%rdi)
	movq	$0, 120(%rdi)
	movq	$0, 152(%rdi)
	movb	$0, 160(%rdi)
	movq	$0, 200(%rdi)
	movq	$0, 176(%rdi)
	movb	$0, 208(%rdi)
	movq	$0, 248(%rdi)
	movq	$0, 224(%rdi)
	movb	$0, 256(%rdi)
	movq	$0, 296(%rdi)
	movq	%rax, 264(%rdi)
	movq	$0, 272(%rdi)
	movups	%xmm0, 304(%rdi)
	ret
	.cfi_endproc
.LFE4603:
	.size	_ZN12v8_inspector8protocol7Runtime12RemoteObjectC2Ev, .-_ZN12v8_inspector8protocol7Runtime12RemoteObjectC2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime12RemoteObjectC1Ev
	.set	_ZN12v8_inspector8protocol7Runtime12RemoteObjectC1Ev,_ZN12v8_inspector8protocol7Runtime12RemoteObjectC2Ev
	.section	.text._ZN12v8_inspector11ValueMirrorD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector11ValueMirrorD2Ev
	.type	_ZN12v8_inspector11ValueMirrorD2Ev, @function
_ZN12v8_inspector11ValueMirrorD2Ev:
.LFB8024:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8024:
	.size	_ZN12v8_inspector11ValueMirrorD2Ev, .-_ZN12v8_inspector11ValueMirrorD2Ev
	.globl	_ZN12v8_inspector11ValueMirrorD1Ev
	.set	_ZN12v8_inspector11ValueMirrorD1Ev,_ZN12v8_inspector11ValueMirrorD2Ev
	.section	.text._ZN12v8_inspector11ValueMirrorD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector11ValueMirrorD0Ev
	.type	_ZN12v8_inspector11ValueMirrorD0Ev, @function
_ZN12v8_inspector11ValueMirrorD0Ev:
.LFB8026:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8026:
	.size	_ZN12v8_inspector11ValueMirrorD0Ev, .-_ZN12v8_inspector11ValueMirrorD0Ev
	.section	.text._ZN12v8_inspector14PropertyMirrorD2Ev,"axG",@progbits,_ZN12v8_inspector14PropertyMirrorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector14PropertyMirrorD2Ev
	.type	_ZN12v8_inspector14PropertyMirrorD2Ev, @function
_ZN12v8_inspector14PropertyMirrorD2Ev:
.LFB8034:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	80(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L970
	movq	(%rdi), %rax
	call	*8(%rax)
.L970:
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L971
	movq	(%rdi), %rax
	call	*8(%rax)
.L971:
	movq	64(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L972
	movq	(%rdi), %rax
	call	*8(%rax)
.L972:
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L973
	movq	(%rdi), %rax
	call	*8(%rax)
.L973:
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L974
	movq	(%rdi), %rax
	call	*8(%rax)
.L974:
	movq	(%rbx), %rdi
	addq	$16, %rbx
	cmpq	%rbx, %rdi
	je	.L969
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L969:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8034:
	.size	_ZN12v8_inspector14PropertyMirrorD2Ev, .-_ZN12v8_inspector14PropertyMirrorD2Ev
	.weak	_ZN12v8_inspector14PropertyMirrorD1Ev
	.set	_ZN12v8_inspector14PropertyMirrorD1Ev,_ZN12v8_inspector14PropertyMirrorD2Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime13ObjectPreview20ObjectPreviewBuilderILi0EEC2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13ObjectPreview20ObjectPreviewBuilderILi0EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13ObjectPreview20ObjectPreviewBuilderILi0EEC2Ev
	.type	_ZN12v8_inspector8protocol7Runtime13ObjectPreview20ObjectPreviewBuilderILi0EEC2Ev, @function
_ZN12v8_inspector8protocol7Runtime13ObjectPreview20ObjectPreviewBuilderILi0EEC2Ev:
.LFB8709:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$168, %edi
	subq	$8, %rsp
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rcx
	leaq	24(%rax), %rdx
	movups	%xmm0, 72(%rax)
	movq	%rdx, 8(%rax)
	xorl	%edx, %edx
	movw	%dx, 24(%rax)
	leaq	72(%rax), %rdx
	movq	%rdx, 56(%rax)
	leaq	120(%rax), %rdx
	movups	%xmm0, 120(%rax)
	pxor	%xmm0, %xmm0
	movq	%rcx, (%rax)
	movq	$0, 16(%rax)
	movq	$0, 40(%rax)
	movb	$0, 48(%rax)
	movq	$0, 88(%rax)
	movq	$0, 64(%rax)
	movb	$0, 96(%rax)
	movq	$0, 136(%rax)
	movq	%rdx, 104(%rax)
	movq	$0, 112(%rax)
	movb	$0, 144(%rax)
	movups	%xmm0, 152(%rax)
	movq	%rax, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8709:
	.size	_ZN12v8_inspector8protocol7Runtime13ObjectPreview20ObjectPreviewBuilderILi0EEC2Ev, .-_ZN12v8_inspector8protocol7Runtime13ObjectPreview20ObjectPreviewBuilderILi0EEC2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime13ObjectPreview20ObjectPreviewBuilderILi0EEC1Ev
	.set	_ZN12v8_inspector8protocol7Runtime13ObjectPreview20ObjectPreviewBuilderILi0EEC1Ev,_ZN12v8_inspector8protocol7Runtime13ObjectPreview20ObjectPreviewBuilderILi0EEC2Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime15PropertyPreview22PropertyPreviewBuilderILi0EEC2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime15PropertyPreview22PropertyPreviewBuilderILi0EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime15PropertyPreview22PropertyPreviewBuilderILi0EEC2Ev
	.type	_ZN12v8_inspector8protocol7Runtime15PropertyPreview22PropertyPreviewBuilderILi0EEC2Ev, @function
_ZN12v8_inspector8protocol7Runtime15PropertyPreview22PropertyPreviewBuilderILi0EEC2Ev:
.LFB8737:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$192, %edi
	subq	$8, %rsp
	call	_Znwm@PLT
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rcx
	pxor	%xmm0, %xmm0
	leaq	24(%rax), %rdx
	movq	%rcx, (%rax)
	xorl	%ecx, %ecx
	movq	%rdx, 8(%rax)
	xorl	%edx, %edx
	movw	%dx, 24(%rax)
	leaq	64(%rax), %rdx
	movq	%rdx, 48(%rax)
	leaq	112(%rax), %rdx
	movq	%rdx, 96(%rax)
	leaq	168(%rax), %rdx
	movq	$0, 16(%rax)
	movq	$0, 40(%rax)
	movq	$0, 56(%rax)
	movw	%cx, 64(%rax)
	movq	$0, 80(%rax)
	movb	$0, 88(%rax)
	movq	$0, 128(%rax)
	movq	$0, 104(%rax)
	movq	$0, 136(%rax)
	movb	$0, 144(%rax)
	movq	$0, 184(%rax)
	movq	%rdx, 152(%rax)
	movq	$0, 160(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 168(%rax)
	movq	%rax, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8737:
	.size	_ZN12v8_inspector8protocol7Runtime15PropertyPreview22PropertyPreviewBuilderILi0EEC2Ev, .-_ZN12v8_inspector8protocol7Runtime15PropertyPreview22PropertyPreviewBuilderILi0EEC2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime15PropertyPreview22PropertyPreviewBuilderILi0EEC1Ev
	.set	_ZN12v8_inspector8protocol7Runtime15PropertyPreview22PropertyPreviewBuilderILi0EEC1Ev,_ZN12v8_inspector8protocol7Runtime15PropertyPreview22PropertyPreviewBuilderILi0EEC2Ev
	.section	.text._ZN12v8_inspector8String166concatIJS0_S0_S0_EEES0_DpT_,"axG",@progbits,_ZN12v8_inspector8String166concatIJS0_S0_S0_EEES0_DpT_,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8String166concatIJS0_S0_S0_EEES0_DpT_
	.type	_ZN12v8_inspector8String166concatIJS0_S0_S0_EEES0_DpT_, @function
_ZN12v8_inspector8String166concatIJS0_S0_S0_EEES0_DpT_:
.LFB9415:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-368(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$360, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector15String16BuilderC1Ev@PLT
	leaq	-224(%rbp), %rax
	movq	(%r15), %rsi
	leaq	-240(%rbp), %rdi
	movq	%rax, -376(%rbp)
	movq	%rax, -240(%rbp)
	movq	8(%r15), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	32(%r15), %rax
	movq	(%rbx), %rsi
	leaq	-288(%rbp), %rdi
	leaq	-336(%rbp), %r15
	movq	%rax, -208(%rbp)
	leaq	-272(%rbp), %rax
	movq	%rax, -384(%rbp)
	movq	%rax, -288(%rbp)
	movq	8(%rbx), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	32(%rbx), %rax
	movq	(%r14), %rsi
	movq	%r15, %rdi
	leaq	-320(%rbp), %rbx
	movq	%rax, -256(%rbp)
	movq	8(%r14), %rax
	movq	%rbx, -336(%rbp)
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	32(%r14), %rax
	movq	%r15, %rsi
	movq	%r12, %rdi
	leaq	-128(%rbp), %r14
	leaq	-176(%rbp), %r15
	movq	%rax, -304(%rbp)
	call	_ZN12v8_inspector15String16Builder6appendERKNS_8String16E@PLT
	movq	-240(%rbp), %rsi
	movq	-232(%rbp), %rax
	leaq	-144(%rbp), %rdi
	movq	%r14, -144(%rbp)
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-208(%rbp), %rax
	movq	-288(%rbp), %rsi
	leaq	-192(%rbp), %r8
	movq	%r8, %rdi
	movq	%r8, -392(%rbp)
	movq	%rax, -112(%rbp)
	movq	-280(%rbp), %rax
	movq	%r15, -192(%rbp)
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-392(%rbp), %r8
	movq	-256(%rbp), %rax
	movq	%r12, %rdi
	movq	%r8, %rsi
	movq	%rax, -160(%rbp)
	call	_ZN12v8_inspector15String16Builder6appendERKNS_8String16E@PLT
	movq	-144(%rbp), %rsi
	movq	-136(%rbp), %rdx
	leaq	-96(%rbp), %r8
	leaq	-80(%rbp), %rax
	movq	%r8, %rdi
	movq	%r8, -392(%rbp)
	leaq	(%rsi,%rdx,2), %rdx
	movq	%rax, -96(%rbp)
	movq	%rax, -400(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-392(%rbp), %r8
	movq	-112(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r8, %rsi
	movq	%rdx, -64(%rbp)
	call	_ZN12v8_inspector15String16Builder6appendERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	-400(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L997
	call	_ZdlPv@PLT
.L997:
	movq	-192(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L998
	call	_ZdlPv@PLT
.L998:
	movq	-144(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L999
	call	_ZdlPv@PLT
.L999:
	movq	-336(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1000
	call	_ZdlPv@PLT
.L1000:
	movq	-288(%rbp), %rdi
	cmpq	-384(%rbp), %rdi
	je	.L1001
	call	_ZdlPv@PLT
.L1001:
	movq	-240(%rbp), %rdi
	cmpq	-376(%rbp), %rdi
	je	.L1002
	call	_ZdlPv@PLT
.L1002:
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZN12v8_inspector15String16Builder8toStringEv@PLT
	movq	-368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L996
	call	_ZdlPv@PLT
.L996:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1009
	addq	$360, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1009:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9415:
	.size	_ZN12v8_inspector8String166concatIJS0_S0_S0_EEES0_DpT_, .-_ZN12v8_inspector8String166concatIJS0_S0_S0_EEES0_DpT_
	.section	.text._ZN12v8_inspector12_GLOBAL__N_116abbreviateStringERKNS_8String16ENS0_14AbbreviateModeE,"ax",@progbits
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_116abbreviateStringERKNS_8String16ENS0_14AbbreviateModeE, @function
_ZN12v8_inspector12_GLOBAL__N_116abbreviateStringERKNS_8String16ENS0_14AbbreviateModeE:
.LFB7795:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$296, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rsi), %rax
	cmpq	$100, %rax
	jbe	.L1047
	movl	$8230, %edi
	movq	(%rsi), %r13
	movw	%di, -290(%rbp)
	testl	%edx, %edx
	jne	.L1013
	leaq	-208(%rbp), %r15
	leaq	-98(%r13,%rax,2), %r13
	movq	%r15, -224(%rbp)
	leaq	-224(%rbp), %r8
	testq	%r13, %r13
	je	.L1016
	movl	$100, %edi
	movq	%r8, -312(%rbp)
	leaq	-96(%rbp), %r14
	call	_Znwm@PLT
	movl	$98, %edx
	movq	%r13, %rsi
	movq	$49, -208(%rbp)
	movq	%rax, %rdi
	movq	%rax, -224(%rbp)
	call	memmove@PLT
	xorl	%esi, %esi
	movq	-312(%rbp), %r8
	movq	%r14, %rdi
	movw	%si, 98(%rax)
	movq	$49, -216(%rbp)
	movq	%r8, %rsi
	call	_ZN12v8_inspector8String16C1EONSt7__cxx1112basic_stringItSt11char_traitsItESaItEEE@PLT
	movq	-224(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L1015
	call	_ZdlPv@PLT
.L1015:
	leaq	-144(%rbp), %r15
	leaq	-290(%rbp), %rsi
	movl	$1, %edx
	movq	%r15, %rdi
	leaq	-240(%rbp), %r13
	call	_ZN12v8_inspector8String16C1EPKtm@PLT
	movq	8(%rbx), %rax
	movq	(%rbx), %rsi
	movl	$50, %ebx
	movq	%r13, -256(%rbp)
	cmpq	$50, %rax
	cmovbe	%rax, %rbx
	movq	%rsi, %rax
	leaq	(%rbx,%rbx), %rdx
	addq	%rdx, %rax
	je	.L1018
	testq	%rsi, %rsi
	je	.L1016
.L1018:
	movq	%rdx, %rcx
	sarq	%rcx
	cmpq	$14, %rdx
	ja	.L1048
	cmpq	$2, %rdx
	jne	.L1020
	movzwl	(%rsi), %eax
	movw	%ax, -240(%rbp)
	movq	-256(%rbp), %rax
.L1021:
	xorl	%edx, %edx
	movq	%rcx, -248(%rbp)
	leaq	-256(%rbp), %rsi
	movw	%dx, (%rax,%rbx,2)
	leaq	-192(%rbp), %rbx
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8String16C1EONSt7__cxx1112basic_stringItSt11char_traitsItESaItEEE@PLT
	movq	-256(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L1022
	call	_ZdlPv@PLT
.L1022:
	movq	%r12, %rdi
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%rbx, %rsi
	call	_ZN12v8_inspector8String166concatIJS0_S0_S0_EEES0_DpT_
	movq	-192(%rbp), %rdi
	leaq	-176(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1023
	call	_ZdlPv@PLT
.L1023:
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1024
	call	_ZdlPv@PLT
.L1024:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1010
.L1046:
	call	_ZdlPv@PLT
.L1010:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1049
	addq	$296, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1013:
	.cfi_restore_state
	leaq	-176(%rbp), %rbx
	leaq	-192(%rbp), %r14
	movq	%rbx, -192(%rbp)
	testq	%r13, %r13
	je	.L1016
	movl	$200, %edi
	call	_Znwm@PLT
	movl	$198, %edx
	movq	%r13, %rsi
	movq	$99, -176(%rbp)
	movq	%rax, %rdi
	movq	%rax, -192(%rbp)
	call	memmove@PLT
	leaq	-144(%rbp), %rdi
	movq	%r14, %rsi
	movq	$99, -184(%rbp)
	movq	%rax, %rcx
	xorl	%eax, %eax
	movw	%ax, 198(%rcx)
	call	_ZN12v8_inspector8String16C1EONSt7__cxx1112basic_stringItSt11char_traitsItESaItEEE@PLT
	movq	-192(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1026
	call	_ZdlPv@PLT
.L1026:
	leaq	-288(%rbp), %r13
	leaq	-96(%rbp), %r14
	movq	%r13, %rdi
	leaq	-80(%rbp), %rbx
	call	_ZN12v8_inspector15String16BuilderC1Ev@PLT
	movq	-144(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rbx, -96(%rbp)
	movq	-136(%rbp), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-112(%rbp), %rax
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN12v8_inspector15String16Builder6appendERKNS_8String16E@PLT
	movq	%r13, %rdi
	movl	$8230, %esi
	call	_ZN12v8_inspector15String16Builder6appendEt@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1027
	call	_ZdlPv@PLT
.L1027:
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector15String16Builder8toStringEv@PLT
	movq	-288(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1028
	call	_ZdlPv@PLT
.L1028:
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L1046
	jmp	.L1010
	.p2align 4,,10
	.p2align 3
.L1047:
	leaq	16(%rdi), %rax
	movq	%rax, (%rdi)
	movq	8(%rbx), %rax
	movq	(%rsi), %rsi
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	32(%rbx), %rax
	movq	%rax, 32(%r12)
	jmp	.L1010
	.p2align 4,,10
	.p2align 3
.L1020:
	movq	%r13, %rax
	testq	%rdx, %rdx
	je	.L1021
	movq	%r13, %rdi
	jmp	.L1019
	.p2align 4,,10
	.p2align 3
.L1048:
	leaq	2(%rdx), %rdi
	movq	%rcx, -328(%rbp)
	movq	%rsi, -320(%rbp)
	movq	%rdx, -312(%rbp)
	call	_Znwm@PLT
	movq	-328(%rbp), %rcx
	movq	-312(%rbp), %rdx
	movq	%rax, -256(%rbp)
	movq	-320(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rcx, -240(%rbp)
.L1019:
	movq	%rcx, -312(%rbp)
	call	memmove@PLT
	movq	-256(%rbp), %rax
	movq	-312(%rbp), %rcx
	jmp	.L1021
.L1049:
	call	__stack_chk_fail@PLT
.L1016:
	leaq	.LC7(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.cfi_endproc
.LFE7795:
	.size	_ZN12v8_inspector12_GLOBAL__N_116abbreviateStringERKNS_8String16ENS0_14AbbreviateModeE, .-_ZN12v8_inspector12_GLOBAL__N_116abbreviateStringERKNS_8String16ENS0_14AbbreviateModeE
	.section	.text._ZN12v8_inspector8String166concatIJPKcS0_S3_EEES0_DpT_,"axG",@progbits,_ZN12v8_inspector8String166concatIJPKcS0_S3_EEES0_DpT_,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8String166concatIJPKcS0_S3_EEES0_DpT_
	.type	_ZN12v8_inspector8String166concatIJPKcS0_S3_EEES0_DpT_, @function
_ZN12v8_inspector8String166concatIJPKcS0_S3_EEES0_DpT_:
.LFB9418:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-224(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	leaq	-176(%rbp), %rbx
	subq	$216, %rsp
	movq	%rcx, -232(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector15String16BuilderC1Ev@PLT
	movq	0(%r13), %rsi
	movq	8(%r13), %rax
	leaq	-192(%rbp), %rdi
	movq	%rbx, -192(%rbp)
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	32(%r13), %rax
	leaq	-96(%rbp), %r13
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, -160(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector15String16Builder6appendERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	movq	%rax, -240(%rbp)
	cmpq	%rax, %rdi
	je	.L1051
	call	_ZdlPv@PLT
.L1051:
	movq	-192(%rbp), %rsi
	movq	-184(%rbp), %rax
	leaq	-144(%rbp), %r8
	leaq	-128(%rbp), %r15
	movq	%r8, %rdi
	movq	%r8, -248(%rbp)
	leaq	(%rsi,%rax,2), %rdx
	movq	%r15, -144(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-248(%rbp), %r8
	movq	-160(%rbp), %rax
	movq	%r12, %rdi
	movq	%r8, %rsi
	movq	%rax, -112(%rbp)
	call	_ZN12v8_inspector15String16Builder6appendERKNS_8String16E@PLT
	movq	-232(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector15String16Builder6appendERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	-240(%rbp), %rdi
	je	.L1052
	call	_ZdlPv@PLT
.L1052:
	movq	-144(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L1053
	call	_ZdlPv@PLT
.L1053:
	movq	-192(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1054
	call	_ZdlPv@PLT
.L1054:
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZN12v8_inspector15String16Builder8toStringEv@PLT
	movq	-224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1050
	call	_ZdlPv@PLT
.L1050:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1061
	addq	$216, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1061:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9418:
	.size	_ZN12v8_inspector8String166concatIJPKcS0_S3_EEES0_DpT_, .-_ZN12v8_inspector8String166concatIJPKcS0_S3_EEES0_DpT_
	.section	.text._ZN12v8_inspector8String166concatIJPKcS0_cEEES0_DpT_,"axG",@progbits,_ZN12v8_inspector8String166concatIJPKcS0_cEEES0_DpT_,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8String166concatIJPKcS0_cEEES0_DpT_
	.type	_ZN12v8_inspector8String166concatIJPKcS0_cEEES0_DpT_, @function
_ZN12v8_inspector8String166concatIJPKcS0_cEEES0_DpT_:
.LFB9440:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	leaq	-128(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-176(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$152, %rsp
	movq	%rsi, -192(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector15String16BuilderC1Ev@PLT
	movsbl	%r13b, %eax
	movq	(%rbx), %rsi
	leaq	-144(%rbp), %rdi
	movl	%eax, -180(%rbp)
	movq	8(%rbx), %rax
	leaq	-80(%rbp), %r13
	movq	%r15, -144(%rbp)
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-192(%rbp), %r8
	movq	32(%rbx), %rax
	leaq	-96(%rbp), %rbx
	movq	%rbx, %rdi
	movq	%r8, %rsi
	movq	%rax, -112(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN12v8_inspector15String16Builder6appendERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L1063
	call	_ZdlPv@PLT
.L1063:
	movq	-144(%rbp), %rsi
	movq	-136(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%r13, -96(%rbp)
	leaq	(%rsi,%rdx,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-112(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rdx, -64(%rbp)
	call	_ZN12v8_inspector15String16Builder6appendERKNS_8String16E@PLT
	movl	-180(%rbp), %esi
	movq	%r12, %rdi
	call	_ZN12v8_inspector15String16Builder6appendEc@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L1064
	call	_ZdlPv@PLT
.L1064:
	movq	-144(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L1065
	call	_ZdlPv@PLT
.L1065:
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZN12v8_inspector15String16Builder8toStringEv@PLT
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1062
	call	_ZdlPv@PLT
.L1062:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1072
	addq	$152, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1072:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9440:
	.size	_ZN12v8_inspector8String166concatIJPKcS0_cEEES0_DpT_, .-_ZN12v8_inspector8String166concatIJPKcS0_cEEES0_DpT_
	.section	.text._ZN12v8_inspector8String166concatIJS0_cS0_cEEES0_DpT_,"axG",@progbits,_ZN12v8_inspector8String166concatIJS0_cS0_cEEES0_DpT_,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8String166concatIJS0_cS0_cEEES0_DpT_
	.type	_ZN12v8_inspector8String166concatIJS0_cS0_cEEES0_DpT_, @function
_ZN12v8_inspector8String166concatIJS0_cS0_cEEES0_DpT_:
.LFB9441:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-272(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	leaq	-176(%rbp), %rbx
	subq	$264, %rsp
	movl	%r8d, -284(%rbp)
	movl	%edx, -280(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector15String16BuilderC1Ev@PLT
	movq	(%r14), %rsi
	movq	8(%r14), %rax
	leaq	-192(%rbp), %rdi
	movq	%rbx, -192(%rbp)
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	32(%r14), %rax
	movq	(%r15), %rsi
	leaq	-240(%rbp), %r8
	movq	%r8, %rdi
	leaq	-224(%rbp), %r14
	movq	%r8, -296(%rbp)
	movq	%rax, -160(%rbp)
	movq	8(%r15), %rax
	movq	%r14, -240(%rbp)
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-296(%rbp), %r8
	movq	32(%r15), %rax
	movq	%r12, %rdi
	leaq	-128(%rbp), %r15
	movq	%r8, %rsi
	movq	%rax, -208(%rbp)
	call	_ZN12v8_inspector15String16Builder6appendERKNS_8String16E@PLT
	movq	-192(%rbp), %rsi
	movq	-184(%rbp), %rax
	leaq	-144(%rbp), %rdi
	movq	%r15, -144(%rbp)
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-160(%rbp), %rax
	movsbl	-280(%rbp), %esi
	movq	%r12, %rdi
	movq	%rax, -112(%rbp)
	call	_ZN12v8_inspector15String16Builder6appendEc@PLT
	movq	-144(%rbp), %rsi
	movq	-136(%rbp), %rdx
	leaq	-96(%rbp), %r8
	leaq	-80(%rbp), %rax
	movq	%r8, %rdi
	movq	%r8, -280(%rbp)
	leaq	(%rsi,%rdx,2), %rdx
	movq	%rax, -96(%rbp)
	movq	%rax, -296(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-280(%rbp), %r8
	movq	-112(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r8, %rsi
	movq	%rdx, -64(%rbp)
	call	_ZN12v8_inspector15String16Builder6appendERKNS_8String16E@PLT
	movsbl	-284(%rbp), %esi
	movq	%r12, %rdi
	call	_ZN12v8_inspector15String16Builder6appendEc@PLT
	movq	-96(%rbp), %rdi
	movq	-296(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1074
	call	_ZdlPv@PLT
.L1074:
	movq	-144(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L1075
	call	_ZdlPv@PLT
.L1075:
	movq	-240(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L1076
	call	_ZdlPv@PLT
.L1076:
	movq	-192(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1077
	call	_ZdlPv@PLT
.L1077:
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZN12v8_inspector15String16Builder8toStringEv@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1073
	call	_ZdlPv@PLT
.L1073:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1084
	addq	$264, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1084:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9441:
	.size	_ZN12v8_inspector8String166concatIJS0_cS0_cEEES0_DpT_, .-_ZN12v8_inspector8String166concatIJS0_cS0_cEEES0_DpT_
	.section	.text._ZN12v8_inspector12_GLOBAL__N_124descriptionForCollectionEPN2v87IsolateENS1_5LocalINS1_6ObjectEEEm,"ax",@progbits
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_124descriptionForCollectionEPN2v87IsolateENS1_5LocalINS1_6ObjectEEEm, @function
_ZN12v8_inspector12_GLOBAL__N_124descriptionForCollectionEPN2v87IsolateENS1_5LocalINS1_6ObjectEEEm:
.LFB7833:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	leaq	-128(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rdx, %rdi
	subq	$144, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v86Object18GetConstructorNameEv@PLT
	movq	%r14, %rsi
	leaq	-176(%rbp), %rdi
	leaq	-80(%rbp), %r14
	movq	%rax, %rdx
	call	_ZN12v8_inspector16toProtocolStringEPN2v87IsolateENS0_5LocalINS0_6StringEEE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	leaq	-64(%rbp), %r13
	call	_ZN12v8_inspector8String1611fromIntegerEm@PLT
	movq	-176(%rbp), %rsi
	movq	%r14, %rdi
	movq	%r13, -80(%rbp)
	movq	-168(%rbp), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	%r12, %rdi
	movq	%r15, %rcx
	movl	$40, %edx
	movq	-144(%rbp), %rax
	movl	$41, %r8d
	movq	%r14, %rsi
	movq	%rax, -48(%rbp)
	call	_ZN12v8_inspector8String166concatIJS0_cS0_cEEES0_DpT_
	movq	-80(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L1086
	call	_ZdlPv@PLT
.L1086:
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1087
	call	_ZdlPv@PLT
.L1087:
	movq	-176(%rbp), %rdi
	leaq	-160(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1085
	call	_ZdlPv@PLT
.L1085:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1091
	addq	$144, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1091:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7833:
	.size	_ZN12v8_inspector12_GLOBAL__N_124descriptionForCollectionEPN2v87IsolateENS1_5LocalINS1_6ObjectEEEm, .-_ZN12v8_inspector12_GLOBAL__N_124descriptionForCollectionEPN2v87IsolateENS1_5LocalINS1_6ObjectEEEm
	.section	.text._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_,"axG",@progbits,_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	.type	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_, @function
_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_:
.LFB9453:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rsi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	addq	$16, %rsi
	movq	(%rdi), %rdi
	cmpq	%rax, %rsi
	je	.L1108
	leaq	16(%r12), %rdx
	cmpq	%rdx, %rdi
	je	.L1109
	movq	%rax, (%r12)
	movq	8(%rbx), %rax
	movq	16(%r12), %rdx
	movq	%rax, 8(%r12)
	movq	16(%rbx), %rax
	movq	%rax, 16(%r12)
	testq	%rdi, %rdi
	je	.L1098
	movq	%rdi, (%rbx)
	movq	%rdx, 16(%rbx)
.L1096:
	xorl	%eax, %eax
	movq	$0, 8(%rbx)
	movw	%ax, (%rdi)
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1109:
	.cfi_restore_state
	movq	%rax, (%r12)
	movq	8(%rbx), %rax
	movq	%rax, 8(%r12)
	movq	16(%rbx), %rax
	movq	%rax, 16(%r12)
.L1098:
	movq	%rsi, (%rbx)
	movq	%rsi, %rdi
	jmp	.L1096
	.p2align 4,,10
	.p2align 3
.L1108:
	movq	8(%rbx), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.L1094
	cmpq	$1, %rax
	je	.L1110
	movq	%rax, %rdx
	addq	%rdx, %rdx
	je	.L1094
	call	memmove@PLT
	movq	8(%rbx), %rax
	movq	(%r12), %rdi
	leaq	(%rax,%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L1094:
	xorl	%ecx, %ecx
	movq	%rax, 8(%r12)
	movw	%cx, (%rdi,%rdx)
	movq	(%rbx), %rdi
	jmp	.L1096
	.p2align 4,,10
	.p2align 3
.L1110:
	movzwl	16(%rbx), %eax
	movw	%ax, (%rdi)
	movq	8(%rbx), %rax
	movq	(%r12), %rdi
	leaq	(%rax,%rax), %rdx
	jmp	.L1094
	.cfi_endproc
.LFE9453:
	.size	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_, .-_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	.section	.text._ZN12v8_inspector8String166concatIJcS0_cEEES0_DpT_,"axG",@progbits,_ZN12v8_inspector8String166concatIJcS0_cEEES0_DpT_,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8String166concatIJcS0_cEEES0_DpT_
	.type	_ZN12v8_inspector8String166concatIJcS0_cEEES0_DpT_, @function
_ZN12v8_inspector8String166concatIJcS0_cEEES0_DpT_:
.LFB9454:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	leaq	-128(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-176(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$152, %rsp
	movl	%ecx, -180(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector15String16BuilderC1Ev@PLT
	movq	(%rbx), %rsi
	movq	8(%rbx), %rdx
	leaq	-144(%rbp), %rdi
	movq	%r15, -144(%rbp)
	leaq	(%rsi,%rdx,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	32(%rbx), %rax
	movsbl	%r13b, %esi
	movq	%r12, %rdi
	leaq	-80(%rbp), %r13
	movq	%rax, -112(%rbp)
	call	_ZN12v8_inspector15String16Builder6appendEc@PLT
	movq	-144(%rbp), %rsi
	movq	-136(%rbp), %rax
	leaq	-96(%rbp), %r8
	movq	%r8, %rdi
	movq	%r8, -192(%rbp)
	leaq	(%rsi,%rax,2), %rdx
	movq	%r13, -96(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-192(%rbp), %r8
	movq	-112(%rbp), %rax
	movq	%r12, %rdi
	movq	%r8, %rsi
	movq	%rax, -64(%rbp)
	call	_ZN12v8_inspector15String16Builder6appendERKNS_8String16E@PLT
	movsbl	-180(%rbp), %esi
	movq	%r12, %rdi
	call	_ZN12v8_inspector15String16Builder6appendEc@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L1112
	call	_ZdlPv@PLT
.L1112:
	movq	-144(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L1113
	call	_ZdlPv@PLT
.L1113:
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZN12v8_inspector15String16Builder8toStringEv@PLT
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1111
	call	_ZdlPv@PLT
.L1111:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1120
	addq	$152, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1120:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9454:
	.size	_ZN12v8_inspector8String166concatIJcS0_cEEES0_DpT_, .-_ZN12v8_inspector8String166concatIJcS0_cEEES0_DpT_
	.section	.text._ZNSt6vectorIN12v8_inspector8String16ESaIS1_EED2Ev,"axG",@progbits,_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EED2Ev
	.type	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EED2Ev, @function
_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EED2Ev:
.LFB9560:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	8(%rdi), %rbx
	movq	(%rdi), %r12
	cmpq	%r12, %rbx
	je	.L1122
	.p2align 4,,10
	.p2align 3
.L1126:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1123
	call	_ZdlPv@PLT
	addq	$40, %r12
	cmpq	%r12, %rbx
	jne	.L1126
.L1124:
	movq	0(%r13), %r12
.L1122:
	testq	%r12, %r12
	je	.L1121
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L1123:
	.cfi_restore_state
	addq	$40, %r12
	cmpq	%r12, %rbx
	jne	.L1126
	jmp	.L1124
	.p2align 4,,10
	.p2align 3
.L1121:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9560:
	.size	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EED2Ev, .-_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EED2Ev
	.weak	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EED1Ev
	.set	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EED1Ev,_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EED2Ev
	.section	.rodata._ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE12emplace_backIJRA18_KcEEEvDpOT_.str1.1,"aMS",@progbits,1
.LC15:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE12emplace_backIJRA18_KcEEEvDpOT_,"axG",@progbits,_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE12emplace_backIJRA18_KcEEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE12emplace_backIJRA18_KcEEEvDpOT_
	.type	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE12emplace_backIJRA18_KcEEEvDpOT_, @function
_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE12emplace_backIJRA18_KcEEEvDpOT_:
.LFB9599:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	cmpq	16(%rdi), %r13
	je	.L1130
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	addq	$40, 8(%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1130:
	.cfi_restore_state
	movq	(%rdi), %r14
	movq	%r13, %rbx
	movabsq	$-3689348814741910323, %rdi
	subq	%r14, %rbx
	movq	%rbx, %rax
	sarq	$3, %rax
	imulq	%rdi, %rax
	movabsq	$230584300921369395, %rdi
	cmpq	%rdi, %rax
	je	.L1151
	testq	%rax, %rax
	je	.L1143
	movabsq	$9223372036854775800, %r15
	leaq	(%rax,%rax), %r8
	cmpq	%r8, %rax
	jbe	.L1152
.L1133:
	movq	%r15, %rdi
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, -64(%rbp)
	addq	%rax, %r15
	movq	%r15, -72(%rbp)
	leaq	40(%rax), %r15
.L1134:
	movq	-64(%rbp), %rax
	leaq	(%rax,%rbx), %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	cmpq	%r14, %r13
	je	.L1135
	leaq	-40(%r13), %rbx
	movq	-64(%rbp), %r15
	leaq	16(%r14), %rax
	movabsq	$922337203685477581, %r13
	movabsq	$2305843009213693951, %rdx
	subq	%r14, %rbx
	shrq	$3, %rbx
	imulq	%r13, %rbx
	andq	%rdx, %rbx
	leaq	(%rbx,%rbx,4), %rdx
	leaq	56(%r14,%rdx,8), %r13
	.p2align 4,,10
	.p2align 3
.L1141:
	leaq	16(%r15), %rsi
	movq	%rsi, (%r15)
	movq	-16(%rax), %rsi
	cmpq	%rax, %rsi
	je	.L1153
.L1136:
	movq	%rsi, (%r15)
	movq	(%rax), %rsi
	movq	%rsi, 16(%r15)
.L1137:
	movq	-8(%rax), %rsi
	xorl	%edx, %edx
	movq	%rsi, 8(%r15)
	movq	16(%rax), %rsi
	movq	%rax, -16(%rax)
	movq	$0, -8(%rax)
	movw	%dx, (%rax)
	movq	%rsi, 32(%r15)
	movq	-16(%rax), %rdi
	cmpq	%rax, %rdi
	je	.L1138
	movq	%rax, -56(%rbp)
	addq	$40, %r15
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
	addq	$40, %rax
	cmpq	%rax, %r13
	jne	.L1141
.L1139:
	movq	-64(%rbp), %rcx
	leaq	10(%rbx,%rbx,4), %rax
	leaq	(%rcx,%rax,8), %r15
.L1135:
	testq	%r14, %r14
	je	.L1142
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1142:
	movq	-64(%rbp), %xmm0
	movq	-72(%rbp), %rax
	movq	%r15, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movq	%rax, 16(%r12)
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1143:
	.cfi_restore_state
	movl	$40, %r15d
	jmp	.L1133
	.p2align 4,,10
	.p2align 3
.L1138:
	addq	$40, %rax
	addq	$40, %r15
	cmpq	%rax, %r13
	je	.L1139
	leaq	16(%r15), %rsi
	movq	%rsi, (%r15)
	movq	-16(%rax), %rsi
	cmpq	%rax, %rsi
	jne	.L1136
.L1153:
	movdqu	(%rax), %xmm1
	movups	%xmm1, 16(%r15)
	jmp	.L1137
	.p2align 4,,10
	.p2align 3
.L1152:
	testq	%r8, %r8
	jne	.L1154
	movq	$0, -72(%rbp)
	movl	$40, %r15d
	movq	$0, -64(%rbp)
	jmp	.L1134
.L1151:
	leaq	.LC15(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1154:
	cmpq	%rdi, %r8
	cmovbe	%r8, %rdi
	imulq	$40, %rdi, %r15
	jmp	.L1133
	.cfi_endproc
.LFE9599:
	.size	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE12emplace_backIJRA18_KcEEEvDpOT_, .-_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE12emplace_backIJRA18_KcEEEvDpOT_
	.section	.text._ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE12emplace_backIJRA17_KcEEEvDpOT_,"axG",@progbits,_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE12emplace_backIJRA17_KcEEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE12emplace_backIJRA17_KcEEEvDpOT_
	.type	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE12emplace_backIJRA17_KcEEEvDpOT_, @function
_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE12emplace_backIJRA17_KcEEEvDpOT_:
.LFB9601:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	cmpq	16(%rdi), %r13
	je	.L1156
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	addq	$40, 8(%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1156:
	.cfi_restore_state
	movq	(%rdi), %r14
	movq	%r13, %rbx
	movabsq	$-3689348814741910323, %rdi
	subq	%r14, %rbx
	movq	%rbx, %rax
	sarq	$3, %rax
	imulq	%rdi, %rax
	movabsq	$230584300921369395, %rdi
	cmpq	%rdi, %rax
	je	.L1177
	testq	%rax, %rax
	je	.L1169
	movabsq	$9223372036854775800, %r15
	leaq	(%rax,%rax), %r8
	cmpq	%r8, %rax
	jbe	.L1178
.L1159:
	movq	%r15, %rdi
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, -64(%rbp)
	addq	%rax, %r15
	movq	%r15, -72(%rbp)
	leaq	40(%rax), %r15
.L1160:
	movq	-64(%rbp), %rax
	leaq	(%rax,%rbx), %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	cmpq	%r14, %r13
	je	.L1161
	leaq	-40(%r13), %rbx
	movq	-64(%rbp), %r15
	leaq	16(%r14), %rax
	movabsq	$922337203685477581, %r13
	movabsq	$2305843009213693951, %rdx
	subq	%r14, %rbx
	shrq	$3, %rbx
	imulq	%r13, %rbx
	andq	%rdx, %rbx
	leaq	(%rbx,%rbx,4), %rdx
	leaq	56(%r14,%rdx,8), %r13
	.p2align 4,,10
	.p2align 3
.L1167:
	leaq	16(%r15), %rsi
	movq	%rsi, (%r15)
	movq	-16(%rax), %rsi
	cmpq	%rax, %rsi
	je	.L1179
.L1162:
	movq	%rsi, (%r15)
	movq	(%rax), %rsi
	movq	%rsi, 16(%r15)
.L1163:
	movq	-8(%rax), %rsi
	xorl	%edx, %edx
	movq	%rsi, 8(%r15)
	movq	16(%rax), %rsi
	movq	%rax, -16(%rax)
	movq	$0, -8(%rax)
	movw	%dx, (%rax)
	movq	%rsi, 32(%r15)
	movq	-16(%rax), %rdi
	cmpq	%rax, %rdi
	je	.L1164
	movq	%rax, -56(%rbp)
	addq	$40, %r15
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
	addq	$40, %rax
	cmpq	%rax, %r13
	jne	.L1167
.L1165:
	movq	-64(%rbp), %rcx
	leaq	10(%rbx,%rbx,4), %rax
	leaq	(%rcx,%rax,8), %r15
.L1161:
	testq	%r14, %r14
	je	.L1168
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1168:
	movq	-64(%rbp), %xmm0
	movq	-72(%rbp), %rax
	movq	%r15, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movq	%rax, 16(%r12)
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1169:
	.cfi_restore_state
	movl	$40, %r15d
	jmp	.L1159
	.p2align 4,,10
	.p2align 3
.L1164:
	addq	$40, %rax
	addq	$40, %r15
	cmpq	%rax, %r13
	je	.L1165
	leaq	16(%r15), %rsi
	movq	%rsi, (%r15)
	movq	-16(%rax), %rsi
	cmpq	%rax, %rsi
	jne	.L1162
.L1179:
	movdqu	(%rax), %xmm1
	movups	%xmm1, 16(%r15)
	jmp	.L1163
	.p2align 4,,10
	.p2align 3
.L1178:
	testq	%r8, %r8
	jne	.L1180
	movq	$0, -72(%rbp)
	movl	$40, %r15d
	movq	$0, -64(%rbp)
	jmp	.L1160
.L1177:
	leaq	.LC15(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1180:
	cmpq	%rdi, %r8
	cmovbe	%r8, %rdi
	imulq	$40, %rdi, %r15
	jmp	.L1159
	.cfi_endproc
.LFE9601:
	.size	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE12emplace_backIJRA17_KcEEEvDpOT_, .-_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE12emplace_backIJRA17_KcEEEvDpOT_
	.section	.text._ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE12emplace_backIJRA20_KcEEEvDpOT_,"axG",@progbits,_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE12emplace_backIJRA20_KcEEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE12emplace_backIJRA20_KcEEEvDpOT_
	.type	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE12emplace_backIJRA20_KcEEEvDpOT_, @function
_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE12emplace_backIJRA20_KcEEEvDpOT_:
.LFB9603:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	cmpq	16(%rdi), %r13
	je	.L1182
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	addq	$40, 8(%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1182:
	.cfi_restore_state
	movq	(%rdi), %r14
	movq	%r13, %rbx
	movabsq	$-3689348814741910323, %rdi
	subq	%r14, %rbx
	movq	%rbx, %rax
	sarq	$3, %rax
	imulq	%rdi, %rax
	movabsq	$230584300921369395, %rdi
	cmpq	%rdi, %rax
	je	.L1203
	testq	%rax, %rax
	je	.L1195
	movabsq	$9223372036854775800, %r15
	leaq	(%rax,%rax), %r8
	cmpq	%r8, %rax
	jbe	.L1204
.L1185:
	movq	%r15, %rdi
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, -64(%rbp)
	addq	%rax, %r15
	movq	%r15, -72(%rbp)
	leaq	40(%rax), %r15
.L1186:
	movq	-64(%rbp), %rax
	leaq	(%rax,%rbx), %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	cmpq	%r14, %r13
	je	.L1187
	leaq	-40(%r13), %rbx
	movq	-64(%rbp), %r15
	leaq	16(%r14), %rax
	movabsq	$922337203685477581, %r13
	movabsq	$2305843009213693951, %rdx
	subq	%r14, %rbx
	shrq	$3, %rbx
	imulq	%r13, %rbx
	andq	%rdx, %rbx
	leaq	(%rbx,%rbx,4), %rdx
	leaq	56(%r14,%rdx,8), %r13
	.p2align 4,,10
	.p2align 3
.L1193:
	leaq	16(%r15), %rsi
	movq	%rsi, (%r15)
	movq	-16(%rax), %rsi
	cmpq	%rax, %rsi
	je	.L1205
.L1188:
	movq	%rsi, (%r15)
	movq	(%rax), %rsi
	movq	%rsi, 16(%r15)
.L1189:
	movq	-8(%rax), %rsi
	xorl	%edx, %edx
	movq	%rsi, 8(%r15)
	movq	16(%rax), %rsi
	movq	%rax, -16(%rax)
	movq	$0, -8(%rax)
	movw	%dx, (%rax)
	movq	%rsi, 32(%r15)
	movq	-16(%rax), %rdi
	cmpq	%rax, %rdi
	je	.L1190
	movq	%rax, -56(%rbp)
	addq	$40, %r15
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
	addq	$40, %rax
	cmpq	%rax, %r13
	jne	.L1193
.L1191:
	movq	-64(%rbp), %rcx
	leaq	10(%rbx,%rbx,4), %rax
	leaq	(%rcx,%rax,8), %r15
.L1187:
	testq	%r14, %r14
	je	.L1194
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1194:
	movq	-64(%rbp), %xmm0
	movq	-72(%rbp), %rax
	movq	%r15, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movq	%rax, 16(%r12)
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1195:
	.cfi_restore_state
	movl	$40, %r15d
	jmp	.L1185
	.p2align 4,,10
	.p2align 3
.L1190:
	addq	$40, %rax
	addq	$40, %r15
	cmpq	%rax, %r13
	je	.L1191
	leaq	16(%r15), %rsi
	movq	%rsi, (%r15)
	movq	-16(%rax), %rsi
	cmpq	%rax, %rsi
	jne	.L1188
.L1205:
	movdqu	(%rax), %xmm1
	movups	%xmm1, 16(%r15)
	jmp	.L1189
	.p2align 4,,10
	.p2align 3
.L1204:
	testq	%r8, %r8
	jne	.L1206
	movq	$0, -72(%rbp)
	movl	$40, %r15d
	movq	$0, -64(%rbp)
	jmp	.L1186
.L1203:
	leaq	.LC15(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1206:
	cmpq	%rdi, %r8
	cmovbe	%r8, %rdi
	imulq	$40, %rdi, %r15
	jmp	.L1185
	.cfi_endproc
.LFE9603:
	.size	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE12emplace_backIJRA20_KcEEEvDpOT_, .-_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE12emplace_backIJRA20_KcEEEvDpOT_
	.section	.text._ZN12v8_inspector8String166concatIJS0_cS0_EEES0_DpT_,"axG",@progbits,_ZN12v8_inspector8String166concatIJS0_cS0_EEES0_DpT_,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8String166concatIJS0_cS0_EEES0_DpT_
	.type	_ZN12v8_inspector8String166concatIJS0_cS0_EEES0_DpT_, @function
_ZN12v8_inspector8String166concatIJS0_cS0_EEES0_DpT_:
.LFB9740:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-272(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	leaq	-176(%rbp), %rbx
	subq	$248, %rsp
	movl	%edx, -280(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector15String16BuilderC1Ev@PLT
	movq	(%r14), %rsi
	movq	8(%r14), %rax
	leaq	-192(%rbp), %rdi
	movq	%rbx, -192(%rbp)
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	32(%r14), %rax
	movq	(%r15), %rsi
	leaq	-240(%rbp), %r8
	movq	%r8, %rdi
	leaq	-224(%rbp), %r14
	movq	%r8, -288(%rbp)
	movq	%rax, -160(%rbp)
	movq	8(%r15), %rax
	movq	%r14, -240(%rbp)
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-288(%rbp), %r8
	movq	32(%r15), %rax
	movq	%r12, %rdi
	leaq	-128(%rbp), %r15
	movq	%r8, %rsi
	movq	%rax, -208(%rbp)
	call	_ZN12v8_inspector15String16Builder6appendERKNS_8String16E@PLT
	movq	-192(%rbp), %rsi
	movq	-184(%rbp), %rax
	leaq	-144(%rbp), %rdi
	movq	%r15, -144(%rbp)
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-160(%rbp), %rax
	movsbl	-280(%rbp), %esi
	movq	%r12, %rdi
	movq	%rax, -112(%rbp)
	call	_ZN12v8_inspector15String16Builder6appendEc@PLT
	movq	-144(%rbp), %rsi
	movq	-136(%rbp), %rdx
	leaq	-96(%rbp), %r8
	leaq	-80(%rbp), %rax
	movq	%r8, %rdi
	movq	%r8, -280(%rbp)
	leaq	(%rsi,%rdx,2), %rdx
	movq	%rax, -96(%rbp)
	movq	%rax, -288(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-280(%rbp), %r8
	movq	-112(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r8, %rsi
	movq	%rdx, -64(%rbp)
	call	_ZN12v8_inspector15String16Builder6appendERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	-288(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1208
	call	_ZdlPv@PLT
.L1208:
	movq	-144(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L1209
	call	_ZdlPv@PLT
.L1209:
	movq	-240(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L1210
	call	_ZdlPv@PLT
.L1210:
	movq	-192(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1211
	call	_ZdlPv@PLT
.L1211:
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZN12v8_inspector15String16Builder8toStringEv@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1207
	call	_ZdlPv@PLT
.L1207:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1218
	addq	$248, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1218:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9740:
	.size	_ZN12v8_inspector8String166concatIJS0_cS0_EEES0_DpT_, .-_ZN12v8_inspector8String166concatIJS0_cS0_EEES0_DpT_
	.section	.rodata._ZN12v8_inspector18descriptionForNodeEN2v85LocalINS0_7ContextEEENS1_INS0_5ValueEEE.str1.1,"aMS",@progbits,1
.LC16:
	.string	"nodeName"
.LC17:
	.string	"constructor"
.LC18:
	.string	"nodeType"
.LC19:
	.string	"id"
.LC20:
	.string	"className"
.LC21:
	.string	"<!DOCTYPE "
	.section	.text._ZN12v8_inspector18descriptionForNodeEN2v85LocalINS0_7ContextEEENS1_INS0_5ValueEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN12v8_inspector18descriptionForNodeEN2v85LocalINS0_7ContextEEENS1_INS0_5ValueEEE
	.type	_ZN12v8_inspector18descriptionForNodeEN2v85LocalINS0_7ContextEEENS1_INS0_5ValueEEE, @function
_ZN12v8_inspector18descriptionForNodeEN2v85LocalINS0_7ContextEEENS1_INS0_5ValueEEE:
.LFB8057:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%rdx, %rdi
	subq	$408, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	jne	.L1220
	leaq	16(%rbx), %rax
	movq	$0, 32(%rbx)
	pxor	%xmm0, %xmm0
	movq	%rax, (%rbx)
	movq	$0, 8(%rbx)
	movups	%xmm0, 16(%rbx)
.L1219:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1347
	addq	$408, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1220:
	.cfi_restore_state
	movq	%r13, %rdi
	leaq	-96(%rbp), %r15
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%rax, %r14
	leaq	-336(%rbp), %rax
	movq	%rax, %rdi
	movq	%r14, %rsi
	movq	%rax, -376(%rbp)
	call	_ZN2v88TryCatchC1EPNS_7IsolateE@PLT
	leaq	.LC16(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L1348
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	movq	%rax, -384(%rbp)
	cmpq	%rax, %rdi
	je	.L1312
	movq	%rdx, -392(%rbp)
	call	_ZdlPv@PLT
	movq	-392(%rbp), %rdx
.L1312:
	leaq	-288(%rbp), %rax
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%rdx, -400(%rbp)
	movq	%rax, -408(%rbp)
	leaq	-272(%rbp), %rax
	movq	%rax, -392(%rbp)
	movq	%rax, -288(%rbp)
	xorl	%eax, %eax
	movq	$0, -280(%rbp)
	movw	%ax, -272(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v85debug10GetBuiltinEPNS_7IsolateENS0_7BuiltinE@PLT
	movq	-400(%rbp), %rdx
	movq	(%rdx), %rcx
	movq	%rcx, %rsi
	andl	$3, %esi
	cmpq	$1, %rsi
	jne	.L1226
	movq	-1(%rcx), %rcx
	cmpw	$63, 11(%rcx)
	jbe	.L1349
.L1226:
	cmpq	$0, -280(%rbp)
	jne	.L1231
	leaq	.LC17(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	testq	%rax, %rax
	je	.L1236
	movq	%rax, %rdi
	movq	%rax, -400(%rbp)
	call	_ZNK2v85Value8IsObjectEv@PLT
	movq	-400(%rbp), %r8
	testb	%al, %al
	je	.L1236
	movq	-96(%rbp), %rdi
	cmpq	-384(%rbp), %rdi
	je	.L1234
	movq	%r8, -400(%rbp)
	call	_ZdlPv@PLT
	movq	-400(%rbp), %r8
.L1234:
	leaq	.LC0(%rip), %rsi
	movq	%r15, %rdi
	movq	%r8, -400(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	-400(%rbp), %r8
	movq	%r13, %rsi
	movq	%rax, %rdx
	movq	%r8, %rdi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L1236
	movq	(%rax), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L1239
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1239
	cmpq	-384(%rbp), %rdi
	je	.L1241
	movq	%rdx, -400(%rbp)
	call	_ZdlPv@PLT
	movq	-400(%rbp), %rdx
.L1241:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector16toProtocolStringEPN2v87IsolateENS0_5LocalINS0_6StringEEE@PLT
	movq	-408(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, -256(%rbp)
	cmpq	-384(%rbp), %rdi
	je	.L1231
	call	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L1231:
	leaq	.LC18(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	testq	%rax, %rax
	je	.L1243
	movq	%rax, %rdi
	movq	%rax, -400(%rbp)
	call	_ZNK2v85Value7IsInt32Ev@PLT
	movq	-400(%rbp), %r8
	testb	%al, %al
	movb	%al, -409(%rbp)
	je	.L1243
	movq	-96(%rbp), %rdi
	cmpq	-384(%rbp), %rdi
	je	.L1245
	movq	%r8, -400(%rbp)
	call	_ZdlPv@PLT
	movq	-400(%rbp), %r8
.L1245:
	movq	%r8, %rdi
	movq	%r8, -400(%rbp)
	call	_ZNK2v85Int325ValueEv@PLT
	movq	-400(%rbp), %r8
	cmpl	$1, %eax
	je	.L1350
	movq	%r8, %rdi
	call	_ZNK2v85Int325ValueEv@PLT
	cmpl	$1, %eax
	je	.L1351
.L1275:
	leaq	16(%rbx), %rax
	movq	%rax, (%rbx)
	movq	-288(%rbp), %rax
	cmpq	-392(%rbp), %rax
	jne	.L1277
	movdqa	-272(%rbp), %xmm2
	movups	%xmm2, 16(%rbx)
	jmp	.L1278
	.p2align 4,,10
	.p2align 3
.L1236:
	movq	-96(%rbp), %rdi
.L1239:
	cmpq	-384(%rbp), %rdi
	je	.L1228
	call	_ZdlPv@PLT
.L1228:
	leaq	16(%rbx), %rax
	movq	$0, 32(%rbx)
	pxor	%xmm0, %xmm0
	movq	%rax, (%rbx)
	movq	$0, 8(%rbx)
	movups	%xmm0, 16(%rbx)
.L1235:
	movq	-288(%rbp), %rdi
	cmpq	-392(%rbp), %rdi
	je	.L1224
	call	_ZdlPv@PLT
.L1224:
	movq	-376(%rbp), %rdi
	call	_ZN2v88TryCatchD1Ev@PLT
	jmp	.L1219
	.p2align 4,,10
	.p2align 3
.L1348:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1309
	call	_ZdlPv@PLT
.L1309:
	pxor	%xmm0, %xmm0
	leaq	16(%rbx), %rax
	movq	$0, 32(%rbx)
	movq	%rax, (%rbx)
	movq	$0, 8(%rbx)
	movups	%xmm0, 16(%rbx)
	jmp	.L1224
	.p2align 4,,10
	.p2align 3
.L1243:
	movq	-96(%rbp), %rdi
	cmpq	-384(%rbp), %rdi
	je	.L1293
	call	_ZdlPv@PLT
.L1293:
	leaq	16(%rbx), %rax
	movq	%rax, (%rbx)
	movq	-288(%rbp), %rax
	cmpq	-392(%rbp), %rax
	je	.L1352
.L1277:
	movq	%rax, (%rbx)
	movq	-272(%rbp), %rax
	movq	%rax, 16(%rbx)
.L1278:
	movq	-280(%rbp), %rax
	movq	%rax, 8(%rbx)
	movq	-256(%rbp), %rax
	movq	%rax, 32(%rbx)
	jmp	.L1224
	.p2align 4,,10
	.p2align 3
.L1352:
	movdqa	-272(%rbp), %xmm1
	movups	%xmm1, 16(%rbx)
	jmp	.L1278
	.p2align 4,,10
	.p2align 3
.L1349:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Function4CallENS_5LocalINS_7ContextEEENS1_INS_5ValueEEEiPS5_@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L1228
	movq	(%rax), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L1226
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1226
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector16toProtocolStringEPN2v87IsolateENS0_5LocalINS0_6StringEEE@PLT
	movq	-408(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, -256(%rbp)
	cmpq	-384(%rbp), %rdi
	je	.L1226
	call	_ZdlPv@PLT
	jmp	.L1226
	.p2align 4,,10
	.p2align 3
.L1350:
	leaq	.LC19(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	testq	%rax, %rax
	je	.L1353
	movq	-96(%rbp), %rdi
	cmpq	-384(%rbp), %rdi
	je	.L1285
	movq	%rax, -400(%rbp)
	call	_ZdlPv@PLT
	movq	-400(%rbp), %rax
.L1285:
	movq	(%rax), %rdx
	movq	%rdx, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L1254
	movq	-1(%rdx), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L1354
.L1254:
	leaq	.LC20(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	movq	%rax, %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1355
	movq	-96(%rbp), %rdi
	cmpq	-384(%rbp), %rdi
	je	.L1282
	call	_ZdlPv@PLT
.L1282:
	movq	(%r12), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L1275
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1275
	movq	%r12, %rdi
	call	_ZNK2v86String6LengthEv@PLT
	testl	%eax, %eax
	je	.L1275
	movq	%r12, %rdx
	movq	%r14, %rsi
	xorl	%r12d, %r12d
	leaq	-240(%rbp), %rdi
	leaq	-368(%rbp), %r13
	call	_ZN12v8_inspector16toProtocolStringEPN2v87IsolateENS0_5LocalINS0_6StringEEE@PLT
	movq	%r13, %rdi
	call	_ZN12v8_inspector15String16BuilderC1Ev@PLT
	xorl	%edx, %edx
	cmpq	$0, -232(%rbp)
	jne	.L1265
	jmp	.L1270
	.p2align 4,,10
	.p2align 3
.L1357:
	testb	%dl, %dl
	je	.L1356
.L1269:
	addq	$1, %r12
	cmpq	-232(%rbp), %r12
	jnb	.L1270
.L1265:
	movq	-240(%rbp), %rax
	movzwl	(%rax,%r12,2), %eax
	cmpw	$32, %ax
	je	.L1357
	movzwl	%ax, %esi
	movq	%r13, %rdi
	call	_ZN12v8_inspector15String16Builder6appendEt@PLT
	movq	-240(%rbp), %rax
	cmpw	$46, (%rax,%r12,2)
	sete	%dl
	jmp	.L1269
.L1351:
	movq	-288(%rbp), %rsi
	movq	-280(%rbp), %rax
	movq	%r15, %rdi
	movq	-384(%rbp), %r14
	leaq	(%rsi,%rax,2), %rdx
	movq	%r14, -96(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	%rbx, %rdi
	movl	$62, %ecx
	movq	%r15, %rdx
	movq	-256(%rbp), %rax
	leaq	.LC21(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN12v8_inspector8String166concatIJPKcS0_cEEES0_DpT_
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L1235
	call	_ZdlPv@PLT
	jmp	.L1235
.L1353:
	movq	-96(%rbp), %rdi
	cmpq	-384(%rbp), %rdi
	je	.L1288
	call	_ZdlPv@PLT
.L1288:
	leaq	16(%rbx), %rax
	movq	%rax, (%rbx)
	movq	-288(%rbp), %rax
	cmpq	-392(%rbp), %rax
	jne	.L1277
	movdqa	-272(%rbp), %xmm3
	movups	%xmm3, 16(%rbx)
	jmp	.L1278
.L1355:
	movq	-96(%rbp), %rdi
	cmpq	-384(%rbp), %rdi
	je	.L1279
	call	_ZdlPv@PLT
.L1279:
	leaq	16(%rbx), %rax
	movq	%rax, (%rbx)
	movq	-288(%rbp), %rax
	cmpq	-392(%rbp), %rax
	jne	.L1277
	movdqa	-272(%rbp), %xmm4
	movups	%xmm4, 16(%rbx)
	jmp	.L1278
.L1354:
	movq	%rax, %rdx
	leaq	-240(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN12v8_inspector16toProtocolStringEPN2v87IsolateENS0_5LocalINS0_6StringEEE@PLT
	movq	-232(%rbp), %rax
	testq	%rax, %rax
	jne	.L1358
.L1255:
	movq	-240(%rbp), %rdi
	leaq	-224(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1254
	call	_ZdlPv@PLT
	jmp	.L1254
.L1356:
	movl	$46, %esi
	movq	%r13, %rdi
	call	_ZN12v8_inspector15String16Builder6appendEc@PLT
	movzbl	-409(%rbp), %edx
	jmp	.L1269
.L1358:
	leaq	-128(%rbp), %rsi
	leaq	-144(%rbp), %rcx
	addq	%rax, %rax
	movq	%rsi, -400(%rbp)
	movq	%rcx, %rdi
	movq	%rsi, -144(%rbp)
	movq	-240(%rbp), %rsi
	movq	%rcx, -440(%rbp)
	leaq	(%rsi,%rax), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-208(%rbp), %rax
	movq	-288(%rbp), %rsi
	leaq	-192(%rbp), %r8
	movq	%r8, %rdi
	movq	%r8, -432(%rbp)
	movq	%rax, -112(%rbp)
	leaq	-176(%rbp), %rax
	movq	%rax, -424(%rbp)
	movq	%rax, -192(%rbp)
	movq	-280(%rbp), %rax
	addq	%rax, %rax
	leaq	(%rsi,%rax), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-432(%rbp), %r8
	movl	$35, %edx
	movq	%r15, %rdi
	movq	-256(%rbp), %rax
	movq	-440(%rbp), %rcx
	movq	%r8, %rsi
	movq	%rax, -160(%rbp)
	call	_ZN12v8_inspector8String166concatIJS0_cS0_EEES0_DpT_
	movq	-408(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, -256(%rbp)
	cmpq	-384(%rbp), %rdi
	je	.L1256
	call	_ZdlPv@PLT
.L1256:
	movq	-192(%rbp), %rdi
	cmpq	-424(%rbp), %rdi
	je	.L1257
	call	_ZdlPv@PLT
.L1257:
	movq	-144(%rbp), %rdi
	cmpq	-400(%rbp), %rdi
	je	.L1255
	call	_ZdlPv@PLT
	jmp	.L1255
.L1347:
	call	__stack_chk_fail@PLT
.L1270:
	leaq	-192(%rbp), %r12
	movq	%r13, %rsi
	leaq	-144(%rbp), %r13
	movq	%r12, %rdi
	call	_ZN12v8_inspector15String16Builder8toStringEv@PLT
	leaq	-128(%rbp), %rax
	movq	-288(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, -400(%rbp)
	movq	%rax, -144(%rbp)
	movq	-280(%rbp), %rax
	addq	%rax, %rax
	leaq	(%rsi,%rax), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	%r12, %rcx
	movl	$46, %edx
	movq	%r13, %rsi
	movq	-256(%rbp), %rax
	movq	%r15, %rdi
	movq	%rax, -112(%rbp)
	call	_ZN12v8_inspector8String166concatIJS0_cS0_EEES0_DpT_
	movq	-408(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, -256(%rbp)
	cmpq	-384(%rbp), %rdi
	je	.L1267
	call	_ZdlPv@PLT
.L1267:
	movq	-144(%rbp), %rdi
	cmpq	-400(%rbp), %rdi
	je	.L1271
	call	_ZdlPv@PLT
.L1271:
	movq	-192(%rbp), %rdi
	leaq	-176(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1272
	call	_ZdlPv@PLT
.L1272:
	movq	-368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1273
	call	_ZdlPv@PLT
.L1273:
	movq	-240(%rbp), %rdi
	leaq	-224(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1275
	call	_ZdlPv@PLT
	jmp	.L1275
	.cfi_endproc
.LFE8057:
	.size	_ZN12v8_inspector18descriptionForNodeEN2v85LocalINS0_7ContextEEENS1_INS0_5ValueEEE, .-_ZN12v8_inspector18descriptionForNodeEN2v85LocalINS0_7ContextEEENS1_INS0_5ValueEEE
	.section	.text._ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_,"axG",@progbits,_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_,comdat
	.align 2
	.p2align 4
	.weak	_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_
	.type	_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_, @function
_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_:
.LFB9985:
	.cfi_startproc
	endbr64
	movq	8(%rsi), %r9
	movq	8(%rdi), %r8
	movq	(%rsi), %rsi
	movq	(%rdi), %rcx
	cmpq	%r9, %r8
	movq	%r9, %rdx
	cmovbe	%r8, %rdx
	testq	%rdx, %rdx
	je	.L1360
	xorl	%eax, %eax
	jmp	.L1362
	.p2align 4,,10
	.p2align 3
.L1372:
	jb	.L1365
	addq	$1, %rax
	cmpq	%rdx, %rax
	je	.L1360
.L1362:
	movzwl	(%rcx,%rax,2), %edi
	cmpw	%di, (%rsi,%rax,2)
	jbe	.L1372
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1360:
	subq	%r9, %r8
	movl	$2147483647, %eax
	cmpq	$2147483647, %r8
	jg	.L1373
	cmpq	$-2147483648, %r8
	movl	$-2147483648, %eax
	cmovge	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1373:
	ret
	.p2align 4,,10
	.p2align 3
.L1365:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE9985:
	.size	_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_, .-_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_
	.section	.text._ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE12emplace_backIJS1_EEEvDpOT_,"axG",@progbits,_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE12emplace_backIJS1_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE12emplace_backIJS1_EEEvDpOT_
	.type	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE12emplace_backIJS1_EEEvDpOT_, @function
_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE12emplace_backIJS1_EEEvDpOT_:
.LFB10898:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	8(%rdi), %r13
	cmpq	16(%rdi), %r13
	je	.L1375
	leaq	16(%r13), %rax
	movq	%rax, 0(%r13)
	movq	(%rsi), %rcx
	leaq	16(%rsi), %rax
	cmpq	%rax, %rcx
	je	.L1400
	movq	%rcx, 0(%r13)
	movq	16(%rsi), %rcx
	movq	%rcx, 16(%r13)
.L1377:
	movq	8(%rbx), %rcx
	movq	%rax, (%rbx)
	movq	$0, 8(%rbx)
	movq	%rcx, 8(%r13)
	movq	32(%rbx), %rax
	xorl	%ecx, %ecx
	movw	%cx, 16(%rbx)
	movq	%rax, 32(%r13)
	addq	$40, 8(%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1400:
	.cfi_restore_state
	movdqu	16(%rsi), %xmm2
	movups	%xmm2, 16(%r13)
	jmp	.L1377
	.p2align 4,,10
	.p2align 3
.L1375:
	movabsq	$-3689348814741910323, %rsi
	movq	(%rdi), %rax
	movq	%r13, %rcx
	subq	%rax, %rcx
	movq	%rax, -64(%rbp)
	movq	%rcx, %rax
	sarq	$3, %rax
	imulq	%rsi, %rax
	movabsq	$230584300921369395, %rsi
	cmpq	%rsi, %rax
	je	.L1401
	testq	%rax, %rax
	je	.L1392
	movabsq	$9223372036854775800, %r15
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L1402
.L1380:
	movq	%r15, %rdi
	movq	%rcx, -80(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	addq	%rax, %r15
	movq	%rax, -56(%rbp)
	leaq	40(%rax), %rsi
	movq	%r15, -72(%rbp)
.L1381:
	movq	-56(%rbp), %rax
	movq	(%rbx), %rdi
	addq	%rcx, %rax
	leaq	16(%rax), %rcx
	movq	%rcx, (%rax)
	leaq	16(%rbx), %rcx
	cmpq	%rcx, %rdi
	je	.L1403
	movq	%rdi, (%rax)
	movq	16(%rbx), %rdi
	movq	%rdi, 16(%rax)
.L1383:
	movq	8(%rbx), %rdi
	movq	%rcx, (%rbx)
	xorl	%edx, %edx
	movq	32(%rbx), %rcx
	movq	$0, 8(%rbx)
	movq	%rdi, 8(%rax)
	movq	%rcx, 32(%rax)
	movq	-64(%rbp), %rax
	movw	%dx, 16(%rbx)
	cmpq	%rax, %r13
	je	.L1384
	leaq	-40(%r13), %rbx
	movq	-56(%rbp), %r15
	leaq	16(%rax), %r14
	movabsq	$922337203685477581, %r13
	movabsq	$2305843009213693951, %rcx
	subq	%rax, %rbx
	shrq	$3, %rbx
	imulq	%r13, %rbx
	andq	%rcx, %rbx
	leaq	(%rbx,%rbx,4), %rcx
	leaq	56(%rax,%rcx,8), %r13
	.p2align 4,,10
	.p2align 3
.L1390:
	leaq	16(%r15), %rcx
	movq	%rcx, (%r15)
	movq	-16(%r14), %rcx
	cmpq	%rcx, %r14
	je	.L1404
.L1385:
	movq	%rcx, (%r15)
	movq	(%r14), %rcx
	movq	%rcx, 16(%r15)
.L1386:
	movq	-8(%r14), %rcx
	xorl	%eax, %eax
	movq	%rcx, 8(%r15)
	movq	16(%r14), %rcx
	movq	%r14, -16(%r14)
	movq	$0, -8(%r14)
	movw	%ax, (%r14)
	movq	%rcx, 32(%r15)
	movq	-16(%r14), %rdi
	cmpq	%rdi, %r14
	je	.L1387
	call	_ZdlPv@PLT
	addq	$40, %r14
	addq	$40, %r15
	cmpq	%r14, %r13
	jne	.L1390
.L1388:
	movq	-56(%rbp), %rdx
	leaq	10(%rbx,%rbx,4), %rax
	leaq	(%rdx,%rax,8), %rsi
.L1384:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1391
	movq	%rsi, -64(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %rsi
.L1391:
	movq	-56(%rbp), %xmm0
	movq	-72(%rbp), %rax
	movq	%rsi, %xmm3
	punpcklqdq	%xmm3, %xmm0
	movq	%rax, 16(%r12)
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1387:
	.cfi_restore_state
	addq	$40, %r14
	addq	$40, %r15
	cmpq	%r14, %r13
	je	.L1388
	leaq	16(%r15), %rcx
	movq	%rcx, (%r15)
	movq	-16(%r14), %rcx
	cmpq	%rcx, %r14
	jne	.L1385
.L1404:
	movdqu	(%r14), %xmm1
	movups	%xmm1, 16(%r15)
	jmp	.L1386
	.p2align 4,,10
	.p2align 3
.L1402:
	testq	%rdi, %rdi
	jne	.L1405
	movq	$0, -72(%rbp)
	movl	$40, %esi
	movq	$0, -56(%rbp)
	jmp	.L1381
	.p2align 4,,10
	.p2align 3
.L1392:
	movl	$40, %r15d
	jmp	.L1380
	.p2align 4,,10
	.p2align 3
.L1403:
	movdqu	16(%rbx), %xmm4
	movups	%xmm4, 16(%rax)
	jmp	.L1383
.L1401:
	leaq	.LC15(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1405:
	cmpq	%rsi, %rdi
	cmovbe	%rdi, %rsi
	imulq	$40, %rsi, %r15
	jmp	.L1380
	.cfi_endproc
.LFE10898:
	.size	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE12emplace_backIJS1_EEEvDpOT_, .-_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE12emplace_backIJS1_EEEvDpOT_
	.section	.text._ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime15PropertyPreviewESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime15PropertyPreviewESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime15PropertyPreviewESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.type	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime15PropertyPreviewESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, @function
_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime15PropertyPreviewESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_:
.LFB10945:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	movabsq	$1152921504606846975, %rdx
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rcx
	movq	8(%rdi), %rax
	movq	%rsi, -64(%rbp)
	movq	%rdi, -136(%rbp)
	movq	%rax, -128(%rbp)
	subq	%rcx, %rax
	sarq	$3, %rax
	movq	%rsi, -72(%rbp)
	movq	%rcx, -88(%rbp)
	cmpq	%rdx, %rax
	je	.L1496
	movq	%rsi, %r13
	subq	-88(%rbp), %r13
	testq	%rax, %rax
	je	.L1451
	movabsq	$9223372036854775800, %rbx
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L1497
.L1408:
	movq	%rbx, %rdi
	call	_Znwm@PLT
	movq	%rax, -96(%rbp)
	addq	%rax, %rbx
	movq	%rbx, -120(%rbp)
	leaq	8(%rax), %rbx
.L1450:
	movq	(%r12), %rax
	movq	-96(%rbp), %rsi
	movq	$0, (%r12)
	movq	%rax, (%rsi,%r13)
	movq	-88(%rbp), %rax
	cmpq	%rax, -64(%rbp)
	je	.L1410
	movq	%rsi, -56(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L1444:
	movq	(%rbx), %rax
	movq	-56(%rbp), %rcx
	movq	$0, (%rbx)
	movq	%rax, (%rcx)
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L1411
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1412
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1413
	call	_ZdlPv@PLT
.L1413:
	movq	136(%r12), %r14
	testq	%r14, %r14
	je	.L1414
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1415
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r14)
	movq	160(%r14), %rax
	movq	%rax, -80(%rbp)
	testq	%rax, %rax
	je	.L1416
	movq	8(%rax), %rcx
	movq	(%rax), %r13
	cmpq	%r13, %rcx
	je	.L1417
	movq	%rbx, -112(%rbp)
	movq	%r13, %rbx
	movq	%rcx, %r13
	movq	%r12, -104(%rbp)
	jmp	.L1424
	.p2align 4,,10
	.p2align 3
.L1499:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L1420
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1421
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1420:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L1422
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1423
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1422:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1418:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	je	.L1498
.L1424:
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.L1418
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L1499
	addq	$8, %rbx
	movq	%r15, %rdi
	call	*%rdx
	cmpq	%rbx, %r13
	jne	.L1424
	.p2align 4,,10
	.p2align 3
.L1498:
	movq	-80(%rbp), %rax
	movq	-104(%rbp), %r12
	movq	-112(%rbp), %rbx
	movq	(%rax), %r13
.L1417:
	testq	%r13, %r13
	je	.L1425
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1425:
	movq	-80(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1416:
	movq	152(%r14), %rax
	movq	%rax, -80(%rbp)
	testq	%rax, %rax
	je	.L1426
	movq	8(%rax), %r15
	movq	(%rax), %r13
	cmpq	%r13, %r15
	je	.L1427
	movq	%r12, -104(%rbp)
	movq	%r14, -112(%rbp)
	jmp	.L1436
	.p2align 4,,10
	.p2align 3
.L1501:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1430
	call	_ZdlPv@PLT
.L1430:
	movq	136(%r12), %r14
	testq	%r14, %r14
	je	.L1431
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	%r14, %rdi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1432
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1431:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1433
	call	_ZdlPv@PLT
.L1433:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1434
	call	_ZdlPv@PLT
.L1434:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1435
	call	_ZdlPv@PLT
.L1435:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1428:
	addq	$8, %r13
	cmpq	%r13, %r15
	je	.L1500
.L1436:
	movq	0(%r13), %r12
	testq	%r12, %r12
	je	.L1428
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1501
	addq	$8, %r13
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r13, %r15
	jne	.L1436
	.p2align 4,,10
	.p2align 3
.L1500:
	movq	-80(%rbp), %rax
	movq	-104(%rbp), %r12
	movq	-112(%rbp), %r14
	movq	(%rax), %r13
.L1427:
	testq	%r13, %r13
	je	.L1437
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1437:
	movq	-80(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1426:
	movq	104(%r14), %rdi
	leaq	120(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1438
	call	_ZdlPv@PLT
.L1438:
	movq	56(%r14), %rdi
	leaq	72(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1439
	call	_ZdlPv@PLT
.L1439:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1440
	call	_ZdlPv@PLT
.L1440:
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1414:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1441
	call	_ZdlPv@PLT
.L1441:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1442
	call	_ZdlPv@PLT
.L1442:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1443
	call	_ZdlPv@PLT
.L1443:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1411:
	addq	$8, -56(%rbp)
	addq	$8, %rbx
	cmpq	%rbx, -64(%rbp)
	jne	.L1444
.L1502:
	movq	-96(%rbp), %rcx
	movq	-64(%rbp), %rax
	subq	-88(%rbp), %rax
	leaq	8(%rcx,%rax), %rbx
.L1410:
	movq	-64(%rbp), %rdi
	movq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1445
	subq	%rdi, %rax
	leaq	-8(%rax), %rsi
	movq	%rsi, %rcx
	shrq	$3, %rcx
	addq	$1, %rcx
	testq	%rsi, %rsi
	je	.L1453
	movq	%rcx, %rdx
	xorl	%eax, %eax
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L1447:
	movdqu	(%rdi,%rax), %xmm1
	movups	%xmm1, (%rbx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L1447
	movq	%rcx, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rdi
	leaq	(%rbx,%rdi), %rdx
	addq	-64(%rbp), %rdi
	movq	%rdi, -72(%rbp)
	cmpq	%rcx, %rax
	je	.L1448
.L1446:
	movq	-72(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L1448:
	leaq	8(%rbx,%rsi), %rbx
.L1445:
	movq	-88(%rbp), %rax
	testq	%rax, %rax
	je	.L1449
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L1449:
	movq	-96(%rbp), %xmm0
	movq	-120(%rbp), %rsi
	movq	%rbx, %xmm2
	movq	-136(%rbp), %rax
	punpcklqdq	%xmm2, %xmm0
	movq	%rsi, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1412:
	.cfi_restore_state
	movq	%r12, %rdi
	addq	$8, %rbx
	call	*%rax
	addq	$8, -56(%rbp)
	cmpq	%rbx, -64(%rbp)
	jne	.L1444
	jmp	.L1502
	.p2align 4,,10
	.p2align 3
.L1423:
	call	*%rdx
	jmp	.L1422
	.p2align 4,,10
	.p2align 3
.L1421:
	call	*%rdx
	jmp	.L1420
	.p2align 4,,10
	.p2align 3
.L1432:
	call	*%rax
	jmp	.L1431
	.p2align 4,,10
	.p2align 3
.L1415:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L1414
	.p2align 4,,10
	.p2align 3
.L1497:
	testq	%rcx, %rcx
	jne	.L1409
	movq	$0, -120(%rbp)
	movl	$8, %ebx
	movq	$0, -96(%rbp)
	jmp	.L1450
	.p2align 4,,10
	.p2align 3
.L1451:
	movl	$8, %ebx
	jmp	.L1408
.L1453:
	movq	%rbx, %rdx
	jmp	.L1446
.L1409:
	cmpq	%rdx, %rcx
	cmovbe	%rcx, %rdx
	leaq	0(,%rdx,8), %rbx
	jmp	.L1408
.L1496:
	leaq	.LC15(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE10945:
	.size	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime15PropertyPreviewESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, .-_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime15PropertyPreviewESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.section	.text._ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime12EntryPreviewESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime12EntryPreviewESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime12EntryPreviewESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.type	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime12EntryPreviewESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, @function
_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime12EntryPreviewESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_:
.LFB10994:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	movabsq	$1152921504606846975, %rdx
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rcx
	movq	8(%rdi), %rax
	movq	%rsi, -72(%rbp)
	movq	%rdi, -152(%rbp)
	movq	%rax, -144(%rbp)
	subq	%rcx, %rax
	sarq	$3, %rax
	movq	%rsi, -80(%rbp)
	movq	%rcx, -96(%rbp)
	cmpq	%rdx, %rax
	je	.L1648
	movq	%rsi, %r13
	subq	-96(%rbp), %r13
	testq	%rax, %rax
	je	.L1571
	movabsq	$9223372036854775800, %rbx
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L1649
.L1505:
	movq	%rbx, %rdi
	call	_Znwm@PLT
	movq	%rax, -104(%rbp)
	addq	%rax, %rbx
	movq	%rbx, -136(%rbp)
	leaq	8(%rax), %rbx
.L1570:
	movq	(%r12), %rax
	movq	-104(%rbp), %rsi
	movq	$0, (%r12)
	movq	%rax, (%rsi,%r13)
	movq	-96(%rbp), %rax
	cmpq	%rax, -72(%rbp)
	je	.L1507
	movq	%rsi, -56(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L1564:
	movq	(%rbx), %rax
	movq	-56(%rbp), %rcx
	movq	$0, (%rbx)
	movq	%rax, (%rcx)
	movq	(%rbx), %r13
	testq	%r13, %r13
	je	.L1508
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1509
	movq	16(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L1510
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%rcx, -64(%rbp)
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1511
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r14)
	movq	160(%r14), %rax
	movq	%rax, -88(%rbp)
	testq	%rax, %rax
	je	.L1512
	movq	8(%rax), %r15
	movq	(%rax), %r12
	cmpq	%r12, %r15
	je	.L1513
	movq	%rbx, -120(%rbp)
	movq	%r12, %rbx
	movq	%rcx, %r12
	movq	%r13, -112(%rbp)
	movq	%r14, -128(%rbp)
	jmp	.L1520
	.p2align 4,,10
	.p2align 3
.L1651:
	movq	16(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L1516
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%r12, %rdx
	jne	.L1517
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1516:
	movq	8(%r13), %r14
	testq	%r14, %r14
	je	.L1518
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%r12, %rdx
	jne	.L1519
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1518:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1514:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L1650
.L1520:
	movq	(%rbx), %r13
	testq	%r13, %r13
	je	.L1514
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L1651
	addq	$8, %rbx
	movq	%r13, %rdi
	call	*%rdx
	cmpq	%rbx, %r15
	jne	.L1520
	.p2align 4,,10
	.p2align 3
.L1650:
	movq	-88(%rbp), %rax
	movq	-112(%rbp), %r13
	movq	-120(%rbp), %rbx
	movq	-128(%rbp), %r14
	movq	(%rax), %r12
.L1513:
	testq	%r12, %r12
	je	.L1521
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1521:
	movq	-88(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1512:
	movq	152(%r14), %rax
	movq	%rax, -88(%rbp)
	testq	%rax, %rax
	je	.L1522
	movq	8(%rax), %r15
	movq	(%rax), %r12
	cmpq	%r12, %r15
	je	.L1523
	movq	%r13, -112(%rbp)
	movq	%r14, -120(%rbp)
	jmp	.L1532
	.p2align 4,,10
	.p2align 3
.L1653:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r13), %rdi
	movq	%rax, 0(%r13)
	leaq	168(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1526
	call	_ZdlPv@PLT
.L1526:
	movq	136(%r13), %r14
	testq	%r14, %r14
	je	.L1527
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	24(%rax), %rax
	cmpq	-64(%rbp), %rax
	jne	.L1528
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1527:
	movq	96(%r13), %rdi
	leaq	112(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1529
	call	_ZdlPv@PLT
.L1529:
	movq	48(%r13), %rdi
	leaq	64(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1530
	call	_ZdlPv@PLT
.L1530:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1531
	call	_ZdlPv@PLT
.L1531:
	movl	$192, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1524:
	addq	$8, %r12
	cmpq	%r12, %r15
	je	.L1652
.L1532:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L1524
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1653
	addq	$8, %r12
	movq	%r13, %rdi
	call	*%rax
	cmpq	%r12, %r15
	jne	.L1532
	.p2align 4,,10
	.p2align 3
.L1652:
	movq	-88(%rbp), %rax
	movq	-112(%rbp), %r13
	movq	-120(%rbp), %r14
	movq	(%rax), %r12
.L1523:
	testq	%r12, %r12
	je	.L1533
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1533:
	movq	-88(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1522:
	movq	104(%r14), %rdi
	leaq	120(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1534
	call	_ZdlPv@PLT
.L1534:
	movq	56(%r14), %rdi
	leaq	72(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1535
	call	_ZdlPv@PLT
.L1535:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1536
	call	_ZdlPv@PLT
.L1536:
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1510:
	movq	8(%r13), %r14
	testq	%r14, %r14
	je	.L1537
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	%rsi, -64(%rbp)
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1538
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r14)
	movq	160(%r14), %rax
	movq	%rax, -88(%rbp)
	testq	%rax, %rax
	je	.L1539
	movq	8(%rax), %r15
	movq	(%rax), %r12
	cmpq	%r12, %r15
	je	.L1540
	movq	%rbx, -120(%rbp)
	movq	%r12, %rbx
	movq	%rsi, %r12
	movq	%r13, -112(%rbp)
	movq	%r14, -128(%rbp)
	jmp	.L1547
	.p2align 4,,10
	.p2align 3
.L1655:
	movq	16(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L1543
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%r12, %rdx
	jne	.L1544
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1543:
	movq	8(%r13), %r14
	testq	%r14, %r14
	je	.L1545
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%r12, %rdx
	jne	.L1546
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1545:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1541:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L1654
.L1547:
	movq	(%rbx), %r13
	testq	%r13, %r13
	je	.L1541
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L1655
	addq	$8, %rbx
	movq	%r13, %rdi
	call	*%rdx
	cmpq	%rbx, %r15
	jne	.L1547
	.p2align 4,,10
	.p2align 3
.L1654:
	movq	-88(%rbp), %rax
	movq	-112(%rbp), %r13
	movq	-120(%rbp), %rbx
	movq	-128(%rbp), %r14
	movq	(%rax), %r12
.L1540:
	testq	%r12, %r12
	je	.L1548
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1548:
	movq	-88(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1539:
	movq	152(%r14), %rax
	movq	%rax, -88(%rbp)
	testq	%rax, %rax
	je	.L1549
	movq	8(%rax), %r15
	movq	(%rax), %r12
	cmpq	%r12, %r15
	je	.L1550
	movq	%r13, -112(%rbp)
	movq	%r14, -120(%rbp)
	jmp	.L1559
	.p2align 4,,10
	.p2align 3
.L1657:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r13), %rdi
	movq	%rax, 0(%r13)
	leaq	168(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1553
	call	_ZdlPv@PLT
.L1553:
	movq	136(%r13), %r14
	testq	%r14, %r14
	je	.L1554
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	24(%rax), %rax
	cmpq	-64(%rbp), %rax
	jne	.L1555
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1554:
	movq	96(%r13), %rdi
	leaq	112(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1556
	call	_ZdlPv@PLT
.L1556:
	movq	48(%r13), %rdi
	leaq	64(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1557
	call	_ZdlPv@PLT
.L1557:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1558
	call	_ZdlPv@PLT
.L1558:
	movl	$192, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1551:
	addq	$8, %r12
	cmpq	%r12, %r15
	je	.L1656
.L1559:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L1551
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1657
	addq	$8, %r12
	movq	%r13, %rdi
	call	*%rax
	cmpq	%r12, %r15
	jne	.L1559
	.p2align 4,,10
	.p2align 3
.L1656:
	movq	-88(%rbp), %rax
	movq	-112(%rbp), %r13
	movq	-120(%rbp), %r14
	movq	(%rax), %r12
.L1550:
	testq	%r12, %r12
	je	.L1560
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1560:
	movq	-88(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1549:
	movq	104(%r14), %rdi
	leaq	120(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1561
	call	_ZdlPv@PLT
.L1561:
	movq	56(%r14), %rdi
	leaq	72(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1562
	call	_ZdlPv@PLT
.L1562:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1563
	call	_ZdlPv@PLT
.L1563:
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1537:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1508:
	addq	$8, -56(%rbp)
	addq	$8, %rbx
	cmpq	%rbx, -72(%rbp)
	jne	.L1564
.L1658:
	movq	-104(%rbp), %rsi
	movq	-72(%rbp), %rax
	subq	-96(%rbp), %rax
	leaq	8(%rsi,%rax), %rbx
.L1507:
	movq	-72(%rbp), %rdi
	movq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1565
	subq	%rdi, %rax
	leaq	-8(%rax), %rsi
	movq	%rsi, %rcx
	shrq	$3, %rcx
	addq	$1, %rcx
	testq	%rsi, %rsi
	je	.L1573
	movq	%rcx, %rdx
	xorl	%eax, %eax
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L1567:
	movdqu	(%rdi,%rax), %xmm1
	movups	%xmm1, (%rbx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L1567
	movq	%rcx, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rdi
	leaq	(%rbx,%rdi), %rdx
	addq	-72(%rbp), %rdi
	movq	%rdi, -80(%rbp)
	cmpq	%rcx, %rax
	je	.L1568
.L1566:
	movq	-80(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L1568:
	leaq	8(%rbx,%rsi), %rbx
.L1565:
	movq	-96(%rbp), %rax
	testq	%rax, %rax
	je	.L1569
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L1569:
	movq	-104(%rbp), %xmm0
	movq	-152(%rbp), %rax
	movq	%rbx, %xmm2
	movq	-136(%rbp), %rsi
	punpcklqdq	%xmm2, %xmm0
	movq	%rsi, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1509:
	.cfi_restore_state
	movq	%r13, %rdi
	addq	$8, %rbx
	call	*%rax
	addq	$8, -56(%rbp)
	cmpq	%rbx, -72(%rbp)
	jne	.L1564
	jmp	.L1658
	.p2align 4,,10
	.p2align 3
.L1519:
	call	*%rdx
	jmp	.L1518
	.p2align 4,,10
	.p2align 3
.L1517:
	call	*%rdx
	jmp	.L1516
	.p2align 4,,10
	.p2align 3
.L1544:
	call	*%rdx
	jmp	.L1543
	.p2align 4,,10
	.p2align 3
.L1528:
	call	*%rax
	jmp	.L1527
	.p2align 4,,10
	.p2align 3
.L1555:
	call	*%rax
	jmp	.L1554
	.p2align 4,,10
	.p2align 3
.L1546:
	call	*%rdx
	jmp	.L1545
	.p2align 4,,10
	.p2align 3
.L1511:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L1510
	.p2align 4,,10
	.p2align 3
.L1538:
	movq	%r14, %rdi
	call	*%rax
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	jmp	.L1508
	.p2align 4,,10
	.p2align 3
.L1649:
	testq	%rcx, %rcx
	jne	.L1506
	movq	$0, -136(%rbp)
	movl	$8, %ebx
	movq	$0, -104(%rbp)
	jmp	.L1570
	.p2align 4,,10
	.p2align 3
.L1571:
	movl	$8, %ebx
	jmp	.L1505
.L1573:
	movq	%rbx, %rdx
	jmp	.L1566
.L1506:
	cmpq	%rdx, %rcx
	cmovbe	%rcx, %rdx
	leaq	0(,%rdx,8), %rbx
	jmp	.L1505
.L1648:
	leaq	.LC15(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE10994:
	.size	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime12EntryPreviewESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, .-_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime12EntryPreviewESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.section	.text._ZNSt6vectorIN12v8_inspector22InternalPropertyMirrorESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN12v8_inspector22InternalPropertyMirrorESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN12v8_inspector22InternalPropertyMirrorESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	.type	_ZNSt6vectorIN12v8_inspector22InternalPropertyMirrorESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_, @function
_ZNSt6vectorIN12v8_inspector22InternalPropertyMirrorESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_:
.LFB11041:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	movabsq	$-6148914691236517205, %rdx
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rbx
	movq	(%rdi), %r14
	movq	%rsi, -56(%rbp)
	movabsq	$192153584101141162, %rsi
	movq	%rdi, -80(%rbp)
	movq	%rbx, %rax
	subq	%r14, %rax
	sarq	$4, %rax
	imulq	%rdx, %rax
	cmpq	%rsi, %rax
	je	.L1691
	movq	-56(%rbp), %rdx
	subq	%r14, %rdx
	testq	%rax, %rax
	je	.L1680
	movabsq	$9223372036854775776, %rcx
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L1692
.L1661:
	movq	%rcx, %rdi
	movq	%rdx, -88(%rbp)
	movq	%rcx, -72(%rbp)
	call	_Znwm@PLT
	movq	-72(%rbp), %rcx
	movq	-88(%rbp), %rdx
	movq	%rax, -64(%rbp)
	leaq	48(%rax), %r8
	addq	%rax, %rcx
	movq	%rcx, -72(%rbp)
.L1679:
	movq	-64(%rbp), %rax
	movq	(%r12), %rcx
	addq	%rdx, %rax
	leaq	16(%rax), %rdx
	movq	%rdx, (%rax)
	leaq	16(%r12), %rdx
	cmpq	%rdx, %rcx
	je	.L1693
	movq	%rcx, (%rax)
	movq	16(%r12), %rcx
	movq	%rcx, 16(%rax)
.L1664:
	movq	%rdx, (%r12)
	xorl	%edx, %edx
	movq	8(%r12), %rcx
	movw	%dx, 16(%r12)
	movq	32(%r12), %rdx
	movq	%rcx, 8(%rax)
	movq	%rdx, 32(%rax)
	movq	40(%r12), %rdx
	movq	$0, 8(%r12)
	movq	%rdx, 40(%rax)
	movq	-56(%rbp), %rax
	movq	$0, 40(%r12)
	cmpq	%r14, %rax
	je	.L1665
	leaq	-48(%rax), %rdx
	movq	-64(%rbp), %r15
	leaq	16(%r14), %r13
	movabsq	$768614336404564651, %rcx
	subq	%r14, %rdx
	shrq	$4, %rdx
	imulq	%rcx, %rdx
	movabsq	$1152921504606846975, %rcx
	andq	%rcx, %rdx
	movq	%rdx, -88(%rbp)
	leaq	(%rdx,%rdx,2), %rdx
	salq	$4, %rdx
	leaq	64(%r14,%rdx), %r12
	.p2align 4,,10
	.p2align 3
.L1672:
	leaq	16(%r15), %rcx
	movq	%rcx, (%r15)
	movq	-16(%r13), %rcx
	cmpq	%r13, %rcx
	je	.L1694
.L1666:
	movq	%rcx, (%r15)
	movq	0(%r13), %rcx
	movq	%rcx, 16(%r15)
.L1667:
	movq	-8(%r13), %rcx
	xorl	%eax, %eax
	movq	%rcx, 8(%r15)
	movq	16(%r13), %rcx
	movq	%r13, -16(%r13)
	movq	$0, -8(%r13)
	movw	%ax, 0(%r13)
	movq	%rcx, 32(%r15)
	movq	24(%r13), %rcx
	movq	$0, 24(%r13)
	movq	%rcx, 40(%r15)
	movq	24(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1668
	movq	(%rdi), %rcx
	call	*8(%rcx)
.L1668:
	movq	-16(%r13), %rdi
	cmpq	%r13, %rdi
	je	.L1669
	call	_ZdlPv@PLT
	addq	$48, %r13
	addq	$48, %r15
	cmpq	%r13, %r12
	jne	.L1672
.L1670:
	movq	-88(%rbp), %rax
	leaq	6(%rax,%rax,2), %r8
	salq	$4, %r8
	addq	-64(%rbp), %r8
.L1665:
	movq	-56(%rbp), %rax
	cmpq	%rbx, %rax
	je	.L1673
	movq	%r8, %rdx
	.p2align 4,,10
	.p2align 3
.L1677:
	leaq	16(%rdx), %rcx
	leaq	16(%rax), %rsi
	movq	%rcx, (%rdx)
	movq	(%rax), %rcx
	cmpq	%rsi, %rcx
	je	.L1695
	movq	%rcx, (%rdx)
	movq	16(%rax), %rcx
	addq	$48, %rax
	addq	$48, %rdx
	movq	%rcx, -32(%rdx)
	movq	-40(%rax), %rcx
	movq	%rcx, -40(%rdx)
	movq	-16(%rax), %rcx
	movq	%rcx, -16(%rdx)
	movq	-8(%rax), %rcx
	movq	%rcx, -8(%rdx)
	cmpq	%rbx, %rax
	jne	.L1677
.L1675:
	movabsq	$768614336404564651, %rdx
	subq	-56(%rbp), %rbx
	leaq	-48(%rbx), %rax
	shrq	$4, %rax
	imulq	%rdx, %rax
	movabsq	$1152921504606846975, %rdx
	andq	%rdx, %rax
	leaq	3(%rax,%rax,2), %rax
	salq	$4, %rax
	addq	%rax, %r8
.L1673:
	testq	%r14, %r14
	je	.L1678
	movq	%r14, %rdi
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L1678:
	movq	-64(%rbp), %xmm0
	movq	-80(%rbp), %rax
	movq	%r8, %xmm3
	movq	-72(%rbp), %rbx
	punpcklqdq	%xmm3, %xmm0
	movq	%rbx, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1669:
	.cfi_restore_state
	addq	$48, %r13
	addq	$48, %r15
	cmpq	%r13, %r12
	je	.L1670
	leaq	16(%r15), %rcx
	movq	%rcx, (%r15)
	movq	-16(%r13), %rcx
	cmpq	%r13, %rcx
	jne	.L1666
.L1694:
	movdqu	0(%r13), %xmm1
	movups	%xmm1, 16(%r15)
	jmp	.L1667
	.p2align 4,,10
	.p2align 3
.L1695:
	movq	8(%rax), %rcx
	movdqu	16(%rax), %xmm2
	addq	$48, %rax
	addq	$48, %rdx
	movq	%rcx, -40(%rdx)
	movq	-16(%rax), %rcx
	movups	%xmm2, -32(%rdx)
	movq	%rcx, -16(%rdx)
	movq	-8(%rax), %rcx
	movq	%rcx, -8(%rdx)
	cmpq	%rax, %rbx
	jne	.L1677
	jmp	.L1675
	.p2align 4,,10
	.p2align 3
.L1692:
	testq	%rdi, %rdi
	jne	.L1662
	movq	$0, -72(%rbp)
	movl	$48, %r8d
	movq	$0, -64(%rbp)
	jmp	.L1679
	.p2align 4,,10
	.p2align 3
.L1680:
	movl	$48, %ecx
	jmp	.L1661
	.p2align 4,,10
	.p2align 3
.L1693:
	movdqu	16(%r12), %xmm4
	movups	%xmm4, 16(%rax)
	jmp	.L1664
.L1662:
	cmpq	%rsi, %rdi
	cmovbe	%rdi, %rsi
	imulq	$48, %rsi, %rcx
	jmp	.L1661
.L1691:
	leaq	.LC15(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE11041:
	.size	_ZNSt6vectorIN12v8_inspector22InternalPropertyMirrorESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_, .-_ZNSt6vectorIN12v8_inspector22InternalPropertyMirrorESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	.section	.text._ZNSt6vectorIN12v8_inspector22InternalPropertyMirrorESaIS1_EE12emplace_backIJS1_EEEvDpOT_,"axG",@progbits,_ZNSt6vectorIN12v8_inspector22InternalPropertyMirrorESaIS1_EE12emplace_backIJS1_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN12v8_inspector22InternalPropertyMirrorESaIS1_EE12emplace_backIJS1_EEEvDpOT_
	.type	_ZNSt6vectorIN12v8_inspector22InternalPropertyMirrorESaIS1_EE12emplace_backIJS1_EEEvDpOT_, @function
_ZNSt6vectorIN12v8_inspector22InternalPropertyMirrorESaIS1_EE12emplace_backIJS1_EEEvDpOT_:
.LFB9726:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	8(%rdi), %rsi
	cmpq	16(%rdi), %rsi
	je	.L1697
	leaq	16(%rsi), %rax
	movq	%rax, (%rsi)
	movq	(%rdx), %rcx
	leaq	16(%rdx), %rax
	cmpq	%rax, %rcx
	je	.L1702
	movq	%rcx, (%rsi)
	movq	16(%rdx), %rcx
	movq	%rcx, 16(%rsi)
.L1699:
	movq	8(%rdx), %rcx
	movq	%rax, (%rdx)
	xorl	%eax, %eax
	movq	$0, 8(%rdx)
	movq	%rcx, 8(%rsi)
	movw	%ax, 16(%rdx)
	movq	32(%rdx), %rax
	movq	%rax, 32(%rsi)
	movq	40(%rdx), %rax
	movq	$0, 40(%rdx)
	movq	%rax, 40(%rsi)
	addq	$48, 8(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L1702:
	movdqu	16(%rdx), %xmm0
	movups	%xmm0, 16(%rsi)
	jmp	.L1699
	.p2align 4,,10
	.p2align 3
.L1697:
	jmp	_ZNSt6vectorIN12v8_inspector22InternalPropertyMirrorESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	.cfi_endproc
.LFE9726:
	.size	_ZNSt6vectorIN12v8_inspector22InternalPropertyMirrorESaIS1_EE12emplace_backIJS1_EEEvDpOT_, .-_ZNSt6vectorIN12v8_inspector22InternalPropertyMirrorESaIS1_EE12emplace_backIJS1_EEEvDpOT_
	.section	.text._ZNSt6vectorIN12v8_inspector21PrivatePropertyMirrorESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN12v8_inspector21PrivatePropertyMirrorESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN12v8_inspector21PrivatePropertyMirrorESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	.type	_ZNSt6vectorIN12v8_inspector21PrivatePropertyMirrorESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_, @function
_ZNSt6vectorIN12v8_inspector21PrivatePropertyMirrorESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_:
.LFB11055:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	movabsq	$-6148914691236517205, %rdx
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rbx
	movq	(%rdi), %r14
	movq	%rsi, -56(%rbp)
	movabsq	$192153584101141162, %rsi
	movq	%rdi, -80(%rbp)
	movq	%rbx, %rax
	subq	%r14, %rax
	sarq	$4, %rax
	imulq	%rdx, %rax
	cmpq	%rsi, %rax
	je	.L1735
	movq	-56(%rbp), %rdx
	subq	%r14, %rdx
	testq	%rax, %rax
	je	.L1724
	movabsq	$9223372036854775776, %rcx
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L1736
.L1705:
	movq	%rcx, %rdi
	movq	%rdx, -88(%rbp)
	movq	%rcx, -72(%rbp)
	call	_Znwm@PLT
	movq	-72(%rbp), %rcx
	movq	-88(%rbp), %rdx
	movq	%rax, -64(%rbp)
	leaq	48(%rax), %r8
	addq	%rax, %rcx
	movq	%rcx, -72(%rbp)
.L1723:
	movq	-64(%rbp), %rax
	movq	(%r12), %rcx
	addq	%rdx, %rax
	leaq	16(%rax), %rdx
	movq	%rdx, (%rax)
	leaq	16(%r12), %rdx
	cmpq	%rdx, %rcx
	je	.L1737
	movq	%rcx, (%rax)
	movq	16(%r12), %rcx
	movq	%rcx, 16(%rax)
.L1708:
	movq	%rdx, (%r12)
	xorl	%edx, %edx
	movq	8(%r12), %rcx
	movw	%dx, 16(%r12)
	movq	32(%r12), %rdx
	movq	%rcx, 8(%rax)
	movq	%rdx, 32(%rax)
	movq	40(%r12), %rdx
	movq	$0, 8(%r12)
	movq	%rdx, 40(%rax)
	movq	-56(%rbp), %rax
	movq	$0, 40(%r12)
	cmpq	%r14, %rax
	je	.L1709
	leaq	-48(%rax), %rdx
	movq	-64(%rbp), %r15
	leaq	16(%r14), %r13
	movabsq	$768614336404564651, %rcx
	subq	%r14, %rdx
	shrq	$4, %rdx
	imulq	%rcx, %rdx
	movabsq	$1152921504606846975, %rcx
	andq	%rcx, %rdx
	movq	%rdx, -88(%rbp)
	leaq	(%rdx,%rdx,2), %rdx
	salq	$4, %rdx
	leaq	64(%r14,%rdx), %r12
	.p2align 4,,10
	.p2align 3
.L1716:
	leaq	16(%r15), %rcx
	movq	%rcx, (%r15)
	movq	-16(%r13), %rcx
	cmpq	%r13, %rcx
	je	.L1738
.L1710:
	movq	%rcx, (%r15)
	movq	0(%r13), %rcx
	movq	%rcx, 16(%r15)
.L1711:
	movq	-8(%r13), %rcx
	xorl	%eax, %eax
	movq	%rcx, 8(%r15)
	movq	16(%r13), %rcx
	movq	%r13, -16(%r13)
	movq	$0, -8(%r13)
	movw	%ax, 0(%r13)
	movq	%rcx, 32(%r15)
	movq	24(%r13), %rcx
	movq	$0, 24(%r13)
	movq	%rcx, 40(%r15)
	movq	24(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1712
	movq	(%rdi), %rcx
	call	*8(%rcx)
.L1712:
	movq	-16(%r13), %rdi
	cmpq	%r13, %rdi
	je	.L1713
	call	_ZdlPv@PLT
	addq	$48, %r13
	addq	$48, %r15
	cmpq	%r13, %r12
	jne	.L1716
.L1714:
	movq	-88(%rbp), %rax
	leaq	6(%rax,%rax,2), %r8
	salq	$4, %r8
	addq	-64(%rbp), %r8
.L1709:
	movq	-56(%rbp), %rax
	cmpq	%rbx, %rax
	je	.L1717
	movq	%r8, %rdx
	.p2align 4,,10
	.p2align 3
.L1721:
	leaq	16(%rdx), %rcx
	leaq	16(%rax), %rsi
	movq	%rcx, (%rdx)
	movq	(%rax), %rcx
	cmpq	%rsi, %rcx
	je	.L1739
	movq	%rcx, (%rdx)
	movq	16(%rax), %rcx
	addq	$48, %rax
	addq	$48, %rdx
	movq	%rcx, -32(%rdx)
	movq	-40(%rax), %rcx
	movq	%rcx, -40(%rdx)
	movq	-16(%rax), %rcx
	movq	%rcx, -16(%rdx)
	movq	-8(%rax), %rcx
	movq	%rcx, -8(%rdx)
	cmpq	%rbx, %rax
	jne	.L1721
.L1719:
	movabsq	$768614336404564651, %rdx
	subq	-56(%rbp), %rbx
	leaq	-48(%rbx), %rax
	shrq	$4, %rax
	imulq	%rdx, %rax
	movabsq	$1152921504606846975, %rdx
	andq	%rdx, %rax
	leaq	3(%rax,%rax,2), %rax
	salq	$4, %rax
	addq	%rax, %r8
.L1717:
	testq	%r14, %r14
	je	.L1722
	movq	%r14, %rdi
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L1722:
	movq	-64(%rbp), %xmm0
	movq	-80(%rbp), %rax
	movq	%r8, %xmm3
	movq	-72(%rbp), %rbx
	punpcklqdq	%xmm3, %xmm0
	movq	%rbx, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1713:
	.cfi_restore_state
	addq	$48, %r13
	addq	$48, %r15
	cmpq	%r13, %r12
	je	.L1714
	leaq	16(%r15), %rcx
	movq	%rcx, (%r15)
	movq	-16(%r13), %rcx
	cmpq	%r13, %rcx
	jne	.L1710
.L1738:
	movdqu	0(%r13), %xmm1
	movups	%xmm1, 16(%r15)
	jmp	.L1711
	.p2align 4,,10
	.p2align 3
.L1739:
	movq	8(%rax), %rcx
	movdqu	16(%rax), %xmm2
	addq	$48, %rax
	addq	$48, %rdx
	movq	%rcx, -40(%rdx)
	movq	-16(%rax), %rcx
	movups	%xmm2, -32(%rdx)
	movq	%rcx, -16(%rdx)
	movq	-8(%rax), %rcx
	movq	%rcx, -8(%rdx)
	cmpq	%rax, %rbx
	jne	.L1721
	jmp	.L1719
	.p2align 4,,10
	.p2align 3
.L1736:
	testq	%rdi, %rdi
	jne	.L1706
	movq	$0, -72(%rbp)
	movl	$48, %r8d
	movq	$0, -64(%rbp)
	jmp	.L1723
	.p2align 4,,10
	.p2align 3
.L1724:
	movl	$48, %ecx
	jmp	.L1705
	.p2align 4,,10
	.p2align 3
.L1737:
	movdqu	16(%r12), %xmm4
	movups	%xmm4, 16(%rax)
	jmp	.L1708
.L1706:
	cmpq	%rsi, %rdi
	cmovbe	%rdi, %rsi
	imulq	$48, %rsi, %rcx
	jmp	.L1705
.L1735:
	leaq	.LC15(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE11055:
	.size	_ZNSt6vectorIN12v8_inspector21PrivatePropertyMirrorESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_, .-_ZNSt6vectorIN12v8_inspector21PrivatePropertyMirrorESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	.section	.text._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_,"axG",@progbits,_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	.type	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_, @function
_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_:
.LFB11678:
	.cfi_startproc
	endbr64
	cmpq	%rsi, %rdi
	je	.L1759
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16(%rdi), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %r13
	movq	8(%rsi), %r15
	cmpq	%r13, %rdx
	je	.L1750
	movq	16(%rdi), %rax
.L1742:
	cmpq	%r15, %rax
	jb	.L1762
	leaq	(%r15,%r15), %rdx
	testq	%r15, %r15
	je	.L1746
.L1765:
	movq	(%r12), %rsi
	cmpq	$1, %r15
	je	.L1763
	testq	%rdx, %rdx
	je	.L1746
	movq	%r13, %rdi
	call	memmove@PLT
	movq	(%rbx), %r13
.L1746:
	xorl	%eax, %eax
	movq	%r15, 8(%rbx)
	movw	%ax, 0(%r13,%r15,2)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1762:
	.cfi_restore_state
	movabsq	$2305843009213693951, %rcx
	cmpq	%rcx, %r15
	ja	.L1764
	addq	%rax, %rax
	movq	%r15, %r14
	cmpq	%rax, %r15
	jnb	.L1745
	cmpq	%rcx, %rax
	cmovbe	%rax, %rcx
	movq	%rcx, %r14
.L1745:
	leaq	2(%r14,%r14), %rdi
	movq	%rdx, -56(%rbp)
	call	_Znwm@PLT
	movq	(%rbx), %rdi
	movq	-56(%rbp), %rdx
	movq	%rax, %r13
	cmpq	%rdi, %rdx
	je	.L1749
	call	_ZdlPv@PLT
.L1749:
	movq	%r13, (%rbx)
	leaq	(%r15,%r15), %rdx
	movq	%r14, 16(%rbx)
	testq	%r15, %r15
	je	.L1746
	jmp	.L1765
	.p2align 4,,10
	.p2align 3
.L1759:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L1750:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	$7, %eax
	jmp	.L1742
	.p2align 4,,10
	.p2align 3
.L1763:
	movzwl	(%rsi), %eax
	movw	%ax, 0(%r13)
	movq	(%rbx), %r13
	jmp	.L1746
.L1764:
	leaq	.LC8(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE11678:
	.size	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_, .-_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	.section	.text._ZNK12v8_inspector12_GLOBAL__N_112ObjectMirror20buildPropertyPreviewEN2v85LocalINS2_7ContextEEERKNS_8String16EPSt10unique_ptrINS_8protocol7Runtime15PropertyPreviewESt14default_deleteISC_EE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK12v8_inspector12_GLOBAL__N_112ObjectMirror20buildPropertyPreviewEN2v85LocalINS2_7ContextEEERKNS_8String16EPSt10unique_ptrINS_8protocol7Runtime15PropertyPreviewESt14default_deleteISC_EE, @function
_ZNK12v8_inspector12_GLOBAL__N_112ObjectMirror20buildPropertyPreviewEN2v85LocalINS2_7ContextEEERKNS_8String16EPSt10unique_ptrINS_8protocol7Runtime15PropertyPreviewESt14default_deleteISC_EE:
.LFB7995:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	-248(%rbp), %rdi
	subq	$216, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol7Runtime15PropertyPreview22PropertyPreviewBuilderILi0EEC1Ev
	movq	-248(%rbp), %r14
	movq	%r13, %rsi
	leaq	8(%r14), %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	32(%r13), %rax
	movq	_ZN12v8_inspector8protocol7Runtime12RemoteObject8TypeEnum6ObjectE(%rip), %rsi
	movq	%rax, 40(%r14)
	leaq	-240(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-248(%rbp), %r13
	movq	%r14, %rsi
	leaq	-80(%rbp), %r14
	leaq	48(%r13), %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-208(%rbp), %rax
	movq	_ZN12v8_inspector8protocol7Runtime12RemoteObject11SubtypeEnum6RegexpE(%rip), %rsi
	movq	%rax, 80(%r13)
	leaq	-192(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rsi
	leaq	64(%rbx), %rdi
	call	_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_
	xorl	%edx, %edx
	leaq	-144(%rbp), %rdi
	leaq	16(%rbx), %rsi
	testl	%eax, %eax
	setne	%dl
	call	_ZN12v8_inspector12_GLOBAL__N_116abbreviateStringERKNS_8String16ENS0_14AbbreviateModeE
	movq	-144(%rbp), %rsi
	movq	%r15, %rdi
	movq	%r14, -96(%rbp)
	movq	-136(%rbp), %rax
	movq	-248(%rbp), %r13
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-112(%rbp), %rax
	leaq	96(%r13), %rdi
	movq	%r15, %rsi
	movq	%rax, -64(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-64(%rbp), %rax
	movb	$1, 88(%r13)
	movq	%rax, 128(%r13)
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L1767
	call	_ZdlPv@PLT
.L1767:
	movq	-248(%rbp), %rax
	movq	(%r12), %rdi
	movq	$0, -248(%rbp)
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L1768
	movq	(%rdi), %rax
	call	*24(%rax)
.L1768:
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1769
	call	_ZdlPv@PLT
.L1769:
	movq	-192(%rbp), %rdi
	leaq	-176(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1770
	call	_ZdlPv@PLT
.L1770:
	movq	-240(%rbp), %rdi
	leaq	-224(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1771
	call	_ZdlPv@PLT
.L1771:
	movq	-248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1772
	movq	(%rdi), %rax
	call	*24(%rax)
.L1772:
	cmpb	$0, 56(%rbx)
	jne	.L1783
.L1766:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1784
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1783:
	.cfi_restore_state
	movq	64(%rbx), %rsi
	movq	72(%rbx), %rax
	movq	%r15, %rdi
	movq	%r14, -96(%rbp)
	movq	(%r12), %r12
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	96(%rbx), %rax
	leaq	152(%r12), %rdi
	movq	%r15, %rsi
	movq	%rax, -64(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-64(%rbp), %rax
	movb	$1, 144(%r12)
	movq	%rax, 184(%r12)
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L1766
	call	_ZdlPv@PLT
	jmp	.L1766
.L1784:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7995:
	.size	_ZNK12v8_inspector12_GLOBAL__N_112ObjectMirror20buildPropertyPreviewEN2v85LocalINS2_7ContextEEERKNS_8String16EPSt10unique_ptrINS_8protocol7Runtime15PropertyPreviewESt14default_deleteISC_EE, .-_ZNK12v8_inspector12_GLOBAL__N_112ObjectMirror20buildPropertyPreviewEN2v85LocalINS2_7ContextEEERKNS_8String16EPSt10unique_ptrINS_8protocol7Runtime15PropertyPreviewESt14default_deleteISC_EE
	.section	.text._ZNK12v8_inspector12_GLOBAL__N_114FunctionMirror17buildEntryPreviewEN2v85LocalINS2_7ContextEEEPiS6_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteISA_EE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK12v8_inspector12_GLOBAL__N_114FunctionMirror17buildEntryPreviewEN2v85LocalINS2_7ContextEEEPiS6_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteISA_EE, @function
_ZNK12v8_inspector12_GLOBAL__N_114FunctionMirror17buildEntryPreviewEN2v85LocalINS2_7ContextEEEPiS6_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteISA_EE:
.LFB7884:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movl	$168, %edi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-192(%rbp), %r12
	pushq	%rbx
	subq	$264, %rsp
	.cfi_offset 3, -56
	movq	%r8, -264(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	_ZN12v8_inspector8protocol7Runtime12RemoteObject8TypeEnum8FunctionE(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rbx
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rbx)
	leaq	24(%rbx), %rax
	leaq	8(%rbx), %r13
	movq	%rax, 8(%rbx)
	xorl	%eax, %eax
	movw	%ax, 24(%rbx)
	leaq	72(%rbx), %rax
	movq	%rax, 56(%rbx)
	leaq	120(%rbx), %rax
	movq	%rax, 104(%rbx)
	movq	$0, 16(%rbx)
	movq	$0, 40(%rbx)
	movb	$0, 48(%rbx)
	movq	$0, 88(%rbx)
	movq	$0, 64(%rbx)
	movb	$0, 96(%rbx)
	movq	$0, 136(%rbx)
	movq	$0, 112(%rbx)
	movb	$0, 144(%rbx)
	movups	%xmm0, 72(%rbx)
	movups	%xmm0, 120(%rbx)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 152(%rbx)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-160(%rbp), %rax
	movq	%r14, %rdi
	movq	8(%r15), %r13
	leaq	-240(%rbp), %r15
	movq	%rax, 40(%rbx)
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	movq	%rax, %r12
	call	_ZN2v88TryCatchC1EPNS_7IsolateE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNK2v85Value8ToStringENS_5LocalINS_7ContextEEE@PLT
	testq	%rax, %rax
	je	.L1945
	movq	%rax, %rdx
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN12v8_inspector16toProtocolStringEPN2v87IsolateENS0_5LocalINS0_6StringEEE@PLT
.L1858:
	movq	%r15, %rdi
	leaq	-96(%rbp), %r12
	leaq	-80(%rbp), %r13
	call	_ZN2v88TryCatchD1Ev@PLT
	movq	-144(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r13, -96(%rbp)
	movq	-136(%rbp), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-112(%rbp), %rax
	leaq	104(%rbx), %rdi
	movq	%r12, %rsi
	movq	%rax, -64(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movb	$1, 96(%rbx)
	movq	%rax, 136(%rbx)
	cmpq	%r13, %rdi
	je	.L1788
	call	_ZdlPv@PLT
.L1788:
	movb	$0, 144(%rbx)
	movl	$24, %edi
	call	_Znwm@PLT
	movq	152(%rbx), %rcx
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rax)
	movq	%rcx, -256(%rbp)
	movq	%rax, 152(%rbx)
	movups	%xmm0, (%rax)
	testq	%rcx, %rcx
	je	.L1789
	movq	8(%rcx), %rdx
	movq	(%rcx), %r15
	movq	%rdx, -288(%rbp)
	cmpq	%r15, %rdx
	je	.L1790
	movq	%rbx, -304(%rbp)
	movq	%r15, -248(%rbp)
	.p2align 4,,10
	.p2align 3
.L1813:
	movq	-248(%rbp), %rax
	movq	(%rax), %r12
	testq	%r12, %r12
	je	.L1791
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1792
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1793
	call	_ZdlPv@PLT
.L1793:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L1794
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rbx
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L1795
	movq	160(%r13), %r15
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r15, %r15
	je	.L1796
	movq	8(%r15), %rax
	movq	(%r15), %r14
	movq	%rax, -280(%rbp)
	cmpq	%r14, %rax
	je	.L1797
	movq	%r12, -296(%rbp)
	jmp	.L1804
	.p2align 4,,10
	.p2align 3
.L1947:
	movq	16(%r12), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L1800
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L1801
	movq	%rdi, -272(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-272(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L1800:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1802
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L1803
	movq	%rdi, -272(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-272(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L1802:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1798:
	addq	$8, %r14
	cmpq	%r14, -280(%rbp)
	je	.L1946
.L1804:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L1798
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1947
	movq	%r12, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -280(%rbp)
	jne	.L1804
	.p2align 4,,10
	.p2align 3
.L1946:
	movq	-296(%rbp), %r12
	movq	(%r15), %r14
.L1797:
	testq	%r14, %r14
	je	.L1805
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1805:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1796:
	movq	152(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1806
	call	_ZNKSt14default_deleteISt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime15PropertyPreviewES_IS5_EESaIS7_EEEclEPS9_.isra.0.part.0
.L1806:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1807
	call	_ZdlPv@PLT
.L1807:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1808
	call	_ZdlPv@PLT
.L1808:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1809
	call	_ZdlPv@PLT
.L1809:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1794:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1810
	call	_ZdlPv@PLT
.L1810:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1811
	call	_ZdlPv@PLT
.L1811:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1812
	call	_ZdlPv@PLT
.L1812:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1791:
	addq	$8, -248(%rbp)
	movq	-248(%rbp), %rax
	cmpq	%rax, -288(%rbp)
	jne	.L1813
	movq	-256(%rbp), %rax
	movq	-304(%rbp), %rbx
	movq	(%rax), %r15
.L1790:
	testq	%r15, %r15
	je	.L1814
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L1814:
	movq	-256(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1789:
	movq	-264(%rbp), %rax
	movq	(%rax), %r15
	movq	%rbx, (%rax)
	testq	%r15, %r15
	je	.L1815
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %r14
	movq	%r14, -248(%rbp)
	movq	24(%rax), %rax
	cmpq	%r14, %rax
	jne	.L1816
	movq	160(%r15), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r13, %r13
	je	.L1817
	movq	8(%r13), %rbx
	movq	0(%r13), %r12
	cmpq	%r12, %rbx
	je	.L1818
	movq	%r15, -256(%rbp)
	movq	%r13, -264(%rbp)
	jmp	.L1825
	.p2align 4,,10
	.p2align 3
.L1949:
	movq	16(%r13), %r15
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r15, %r15
	je	.L1821
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	24(%rax), %rax
	cmpq	%r14, %rax
	jne	.L1822
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1821:
	movq	8(%r13), %r15
	testq	%r15, %r15
	je	.L1823
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	24(%rax), %rax
	cmpq	%r14, %rax
	jne	.L1824
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1823:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1819:
	addq	$8, %r12
	cmpq	%r12, %rbx
	je	.L1948
.L1825:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L1819
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1949
	addq	$8, %r12
	movq	%r13, %rdi
	call	*%rax
	cmpq	%r12, %rbx
	jne	.L1825
	.p2align 4,,10
	.p2align 3
.L1948:
	movq	-264(%rbp), %r13
	movq	-256(%rbp), %r15
	movq	0(%r13), %r12
.L1818:
	testq	%r12, %r12
	je	.L1826
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1826:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1817:
	movq	152(%r15), %rax
	movq	%rax, -280(%rbp)
	testq	%rax, %rax
	je	.L1827
	movq	8(%rax), %rdx
	movq	(%rax), %rbx
	movq	%rdx, -272(%rbp)
	cmpq	%rbx, %rdx
	je	.L1828
	movq	%r15, -304(%rbp)
	.p2align 4,,10
	.p2align 3
.L1851:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L1829
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1830
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1831
	call	_ZdlPv@PLT
.L1831:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L1832
	movq	0(%r13), %rax
	movq	-248(%rbp), %r15
	movq	24(%rax), %rax
	cmpq	%r15, %rax
	jne	.L1833
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -256(%rbp)
	testq	%rax, %rax
	je	.L1834
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	movq	%rcx, -264(%rbp)
	cmpq	%r14, %rcx
	je	.L1835
	movq	%r12, -288(%rbp)
	movq	%r13, -296(%rbp)
	jmp	.L1842
	.p2align 4,,10
	.p2align 3
.L1951:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L1838
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%r15, %rax
	jne	.L1839
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1838:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L1840
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%r15, %rax
	jne	.L1841
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1840:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1836:
	addq	$8, %r14
	cmpq	%r14, -264(%rbp)
	je	.L1950
.L1842:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L1836
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1951
	movq	%r12, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -264(%rbp)
	jne	.L1842
	.p2align 4,,10
	.p2align 3
.L1950:
	movq	-256(%rbp), %rax
	movq	-288(%rbp), %r12
	movq	-296(%rbp), %r13
	movq	(%rax), %r14
.L1835:
	testq	%r14, %r14
	je	.L1843
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1843:
	movq	-256(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1834:
	movq	152(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1844
	call	_ZNKSt14default_deleteISt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime15PropertyPreviewES_IS5_EESaIS7_EEEclEPS9_.isra.0.part.0
.L1844:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1845
	call	_ZdlPv@PLT
.L1845:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1846
	call	_ZdlPv@PLT
.L1846:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1847
	call	_ZdlPv@PLT
.L1847:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1832:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1848
	call	_ZdlPv@PLT
.L1848:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1849
	call	_ZdlPv@PLT
.L1849:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1850
	call	_ZdlPv@PLT
.L1850:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1829:
	addq	$8, %rbx
	cmpq	%rbx, -272(%rbp)
	jne	.L1851
	movq	-280(%rbp), %rax
	movq	-304(%rbp), %r15
	movq	(%rax), %rbx
.L1828:
	testq	%rbx, %rbx
	je	.L1852
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L1852:
	movq	-280(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1827:
	movq	104(%r15), %rdi
	leaq	120(%r15), %rax
	cmpq	%rax, %rdi
	je	.L1853
	call	_ZdlPv@PLT
.L1853:
	movq	56(%r15), %rdi
	leaq	72(%r15), %rax
	cmpq	%rax, %rdi
	je	.L1854
	call	_ZdlPv@PLT
.L1854:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L1855
	call	_ZdlPv@PLT
.L1855:
	movl	$168, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1815:
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1856
	call	_ZdlPv@PLT
.L1856:
	movq	-192(%rbp), %rdi
	leaq	-176(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1785
	call	_ZdlPv@PLT
.L1785:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1952
	addq	$264, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1792:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1791
	.p2align 4,,10
	.p2align 3
.L1801:
	call	*%rax
	jmp	.L1800
	.p2align 4,,10
	.p2align 3
.L1803:
	call	*%rax
	jmp	.L1802
	.p2align 4,,10
	.p2align 3
.L1830:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1829
	.p2align 4,,10
	.p2align 3
.L1795:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1794
	.p2align 4,,10
	.p2align 3
.L1945:
	movq	%r13, %rdi
	call	_ZN2v86Object18GetConstructorNameEv@PLT
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rax, %rdx
	call	_ZN12v8_inspector16toProtocolStringEPN2v87IsolateENS0_5LocalINS0_6StringEEE@PLT
	jmp	.L1858
	.p2align 4,,10
	.p2align 3
.L1841:
	call	*%rax
	jmp	.L1840
	.p2align 4,,10
	.p2align 3
.L1839:
	call	*%rax
	jmp	.L1838
	.p2align 4,,10
	.p2align 3
.L1824:
	call	*%rax
	jmp	.L1823
	.p2align 4,,10
	.p2align 3
.L1822:
	call	*%rax
	jmp	.L1821
	.p2align 4,,10
	.p2align 3
.L1833:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1832
	.p2align 4,,10
	.p2align 3
.L1816:
	movq	%r15, %rdi
	call	*%rax
	jmp	.L1815
.L1952:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7884:
	.size	_ZNK12v8_inspector12_GLOBAL__N_114FunctionMirror17buildEntryPreviewEN2v85LocalINS2_7ContextEEEPiS6_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteISA_EE, .-_ZNK12v8_inspector12_GLOBAL__N_114FunctionMirror17buildEntryPreviewEN2v85LocalINS2_7ContextEEEPiS6_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteISA_EE
	.section	.rodata._ZNK12v8_inspector12_GLOBAL__N_112NumberMirror17buildEntryPreviewEN2v85LocalINS2_7ContextEEEPiS6_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteISA_EE.str1.1,"aMS",@progbits,1
.LC22:
	.string	"-Infinity"
.LC23:
	.string	"Infinity"
.LC24:
	.string	"NaN"
.LC26:
	.string	"-0"
	.section	.text._ZNK12v8_inspector12_GLOBAL__N_112NumberMirror17buildEntryPreviewEN2v85LocalINS2_7ContextEEEPiS6_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteISA_EE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK12v8_inspector12_GLOBAL__N_112NumberMirror17buildEntryPreviewEN2v85LocalINS2_7ContextEEEPiS6_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteISA_EE, @function
_ZNK12v8_inspector12_GLOBAL__N_112NumberMirror17buildEntryPreviewEN2v85LocalINS2_7ContextEEEPiS6_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteISA_EE:
.LFB7853:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-192(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	$168, %edi
	subq	$216, %rsp
	movq	%r8, -216(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	_ZN12v8_inspector8protocol7Runtime12RemoteObject8TypeEnum6NumberE(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %r15
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r15)
	leaq	24(%r15), %rax
	leaq	8(%r15), %r13
	movq	%rax, 8(%r15)
	xorl	%eax, %eax
	movw	%ax, 24(%r15)
	leaq	72(%r15), %rax
	movq	%rax, 56(%r15)
	leaq	120(%r15), %rax
	movq	%rax, 104(%r15)
	movq	$0, 16(%r15)
	movq	$0, 40(%r15)
	movb	$0, 48(%r15)
	movq	$0, 88(%r15)
	movq	$0, 64(%r15)
	movb	$0, 96(%r15)
	movq	$0, 136(%r15)
	movq	$0, 112(%r15)
	movb	$0, 144(%r15)
	movups	%xmm0, 72(%r15)
	movups	%xmm0, 120(%r15)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 152(%r15)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-160(%rbp), %rax
	movq	8(%rbx), %rdi
	movq	%rax, 40(%r15)
	call	_ZNK2v86Number5ValueEv@PLT
	ucomisd	%xmm0, %xmm0
	jp	.L2064
	ucomisd	.LC25(%rip), %xmm0
	jp	.L1956
	jne	.L1956
	movmskpd	%xmm0, %eax
	testb	$1, %al
	jne	.L2065
	movapd	%xmm0, %xmm1
	andpd	.LC27(%rip), %xmm1
	leaq	.LC23(%rip), %rsi
	ucomisd	.LC28(%rip), %xmm1
	jbe	.L1959
.L1960:
	leaq	-144(%rbp), %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
.L1955:
	movq	-144(%rbp), %rsi
	movq	-136(%rbp), %rax
	leaq	-96(%rbp), %r12
	leaq	-80(%rbp), %rbx
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-112(%rbp), %rax
	leaq	104(%r15), %rdi
	movq	%r12, %rsi
	movq	%rax, -64(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movb	$1, 96(%r15)
	movq	%rax, 136(%r15)
	cmpq	%rbx, %rdi
	je	.L1961
	call	_ZdlPv@PLT
.L1961:
	movb	$0, 144(%r15)
	movl	$24, %edi
	call	_Znwm@PLT
	movq	152(%r15), %rcx
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rax)
	movq	%rax, 152(%r15)
	movups	%xmm0, (%rax)
	movq	%rcx, %rax
	movq	%rcx, -208(%rbp)
	testq	%rcx, %rcx
	je	.L1962
	movq	8(%rcx), %rcx
	movq	(%rax), %rbx
	movq	%rcx, -240(%rbp)
	cmpq	%rbx, %rcx
	je	.L1963
	movq	%r15, -256(%rbp)
	movq	%rbx, -200(%rbp)
	.p2align 4,,10
	.p2align 3
.L1986:
	movq	-200(%rbp), %rax
	movq	(%rax), %r12
	testq	%r12, %r12
	je	.L1964
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1965
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1966
	call	_ZdlPv@PLT
.L1966:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L1967
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rbx
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L1968
	movq	160(%r13), %r15
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r15, %r15
	je	.L1969
	movq	8(%r15), %rax
	movq	(%r15), %r14
	movq	%rax, -232(%rbp)
	cmpq	%r14, %rax
	je	.L1970
	movq	%r12, -248(%rbp)
	jmp	.L1977
	.p2align 4,,10
	.p2align 3
.L2067:
	movq	16(%r12), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L1973
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L1974
	movq	%rdi, -224(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-224(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L1973:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1975
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L1976
	movq	%rdi, -224(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-224(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L1975:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1971:
	addq	$8, %r14
	cmpq	%r14, -232(%rbp)
	je	.L2066
.L1977:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L1971
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2067
	movq	%r12, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -232(%rbp)
	jne	.L1977
	.p2align 4,,10
	.p2align 3
.L2066:
	movq	-248(%rbp), %r12
	movq	(%r15), %r14
.L1970:
	testq	%r14, %r14
	je	.L1978
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1978:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1969:
	movq	152(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1979
	call	_ZNKSt14default_deleteISt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime15PropertyPreviewES_IS5_EESaIS7_EEEclEPS9_.isra.0.part.0
.L1979:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1980
	call	_ZdlPv@PLT
.L1980:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1981
	call	_ZdlPv@PLT
.L1981:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1982
	call	_ZdlPv@PLT
.L1982:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1967:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1983
	call	_ZdlPv@PLT
.L1983:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1984
	call	_ZdlPv@PLT
.L1984:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1985
	call	_ZdlPv@PLT
.L1985:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1964:
	addq	$8, -200(%rbp)
	movq	-200(%rbp), %rax
	cmpq	%rax, -240(%rbp)
	jne	.L1986
	movq	-208(%rbp), %rax
	movq	-256(%rbp), %r15
	movq	(%rax), %rbx
.L1963:
	testq	%rbx, %rbx
	je	.L1987
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L1987:
	movq	-208(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1962:
	movq	-216(%rbp), %rax
	movq	(%rax), %r12
	movq	%r15, (%rax)
	testq	%r12, %r12
	je	.L1988
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rbx
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L1989
	movq	160(%r12), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r14, %r14
	je	.L1990
	movq	8(%r14), %r15
	movq	(%r14), %r13
	cmpq	%r13, %r15
	je	.L1991
	movq	%r12, -200(%rbp)
	movq	%r14, -208(%rbp)
	jmp	.L1998
	.p2align 4,,10
	.p2align 3
.L2069:
	movq	16(%r12), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r14, %r14
	je	.L1994
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L1995
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1994:
	movq	8(%r12), %r14
	testq	%r14, %r14
	je	.L1996
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L1997
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1996:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1992:
	addq	$8, %r13
	cmpq	%r13, %r15
	je	.L2068
.L1998:
	movq	0(%r13), %r12
	testq	%r12, %r12
	je	.L1992
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2069
	addq	$8, %r13
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r13, %r15
	jne	.L1998
	.p2align 4,,10
	.p2align 3
.L2068:
	movq	-208(%rbp), %r14
	movq	-200(%rbp), %r12
	movq	(%r14), %r13
.L1991:
	testq	%r13, %r13
	je	.L1999
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1999:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1990:
	movq	152(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2000
	call	_ZNKSt14default_deleteISt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime15PropertyPreviewES_IS5_EESaIS7_EEEclEPS9_.isra.0.part.0
.L2000:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2001
	call	_ZdlPv@PLT
.L2001:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2002
	call	_ZdlPv@PLT
.L2002:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2003
	call	_ZdlPv@PLT
.L2003:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1988:
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2004
	call	_ZdlPv@PLT
.L2004:
	movq	-192(%rbp), %rdi
	leaq	-176(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1953
	call	_ZdlPv@PLT
.L1953:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2070
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1956:
	.cfi_restore_state
	movapd	%xmm0, %xmm1
	andpd	.LC27(%rip), %xmm1
	ucomisd	.LC28(%rip), %xmm1
	jbe	.L1959
	movmskpd	%xmm0, %eax
	leaq	.LC23(%rip), %rsi
	testb	$1, %al
	leaq	.LC22(%rip), %rax
	cmovne	%rax, %rsi
	jmp	.L1960
	.p2align 4,,10
	.p2align 3
.L1965:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1964
	.p2align 4,,10
	.p2align 3
.L1976:
	call	*%rax
	jmp	.L1975
	.p2align 4,,10
	.p2align 3
.L1974:
	call	*%rax
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L1968:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1967
	.p2align 4,,10
	.p2align 3
.L1959:
	leaq	-144(%rbp), %rdi
	call	_ZN12v8_inspector8String1610fromDoubleEd@PLT
	jmp	.L1955
	.p2align 4,,10
	.p2align 3
.L1997:
	call	*%rax
	jmp	.L1996
	.p2align 4,,10
	.p2align 3
.L1995:
	call	*%rax
	jmp	.L1994
	.p2align 4,,10
	.p2align 3
.L2065:
	leaq	-144(%rbp), %rdi
	leaq	.LC26(%rip), %rsi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	jmp	.L1955
	.p2align 4,,10
	.p2align 3
.L1989:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1988
	.p2align 4,,10
	.p2align 3
.L2064:
	leaq	-144(%rbp), %rdi
	leaq	.LC24(%rip), %rsi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	jmp	.L1955
.L2070:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7853:
	.size	_ZNK12v8_inspector12_GLOBAL__N_112NumberMirror17buildEntryPreviewEN2v85LocalINS2_7ContextEEEPiS6_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteISA_EE, .-_ZNK12v8_inspector12_GLOBAL__N_112NumberMirror17buildEntryPreviewEN2v85LocalINS2_7ContextEEEPiS6_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteISA_EE
	.section	.text._ZNK12v8_inspector12_GLOBAL__N_120PrimitiveValueMirror17buildEntryPreviewEN2v85LocalINS2_7ContextEEEPiS6_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteISA_EE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK12v8_inspector12_GLOBAL__N_120PrimitiveValueMirror17buildEntryPreviewEN2v85LocalINS2_7ContextEEEPiS6_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteISA_EE, @function
_ZNK12v8_inspector12_GLOBAL__N_120PrimitiveValueMirror17buildEntryPreviewEN2v85LocalINS2_7ContextEEEPiS6_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteISA_EE:
.LFB7845:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movl	$168, %edi
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 3, -56
	movq	%r8, -160(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	16(%r15), %rsi
	movq	%rax, %rbx
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rbx)
	leaq	24(%rbx), %rax
	leaq	8(%rbx), %rdi
	movq	%rax, 8(%rbx)
	xorl	%eax, %eax
	leaq	104(%rbx), %r13
	movw	%ax, 24(%rbx)
	leaq	72(%rbx), %rax
	movq	%rax, 56(%rbx)
	leaq	120(%rbx), %rax
	movq	%rax, 104(%rbx)
	movq	$0, 16(%rbx)
	movq	$0, 40(%rbx)
	movb	$0, 48(%rbx)
	movq	$0, 88(%rbx)
	movq	$0, 64(%rbx)
	movb	$0, 96(%rbx)
	movq	$0, 136(%rbx)
	movq	$0, 112(%rbx)
	movb	$0, 144(%rbx)
	movups	%xmm0, 72(%rbx)
	movups	%xmm0, 120(%rbx)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 152(%rbx)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	48(%r15), %rax
	movq	8(%r15), %rdx
	movq	%r12, %rsi
	leaq	-80(%rbp), %r12
	movq	%rax, 40(%rbx)
	leaq	-144(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -224(%rbp)
	call	_ZN12v8_inspector12_GLOBAL__N_127descriptionForPrimitiveTypeEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE
	movq	-144(%rbp), %rsi
	leaq	-96(%rbp), %rcx
	movq	-136(%rbp), %rax
	movq	%rcx, %rdi
	movq	%rcx, %r14
	movq	%r12, -96(%rbp)
	leaq	(%rsi,%rax,2), %rdx
	movq	%rcx, -208(%rbp)
	movq	%r12, -216(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-112(%rbp), %rax
	movq	%r13, %rdi
	movq	%r14, %rsi
	movq	%rax, -64(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movb	$1, 96(%rbx)
	movq	%rax, 136(%rbx)
	cmpq	%r12, %rdi
	je	.L2072
	call	_ZdlPv@PLT
.L2072:
	movb	$0, 144(%rbx)
	movl	$24, %edi
	call	_Znwm@PLT
	movq	152(%rbx), %rcx
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rax)
	movq	%rcx, -168(%rbp)
	movq	%rax, 152(%rbx)
	movups	%xmm0, (%rax)
	testq	%rcx, %rcx
	je	.L2073
	movq	8(%rcx), %rdx
	movq	(%rcx), %rax
	movq	%rdx, -192(%rbp)
	movq	%rax, -152(%rbp)
	cmpq	%rax, %rdx
	je	.L2074
	movq	%rbx, -232(%rbp)
	movq	%r15, -240(%rbp)
	.p2align 4,,10
	.p2align 3
.L2097:
	movq	-152(%rbp), %rax
	movq	(%rax), %r12
	testq	%r12, %r12
	je	.L2075
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2076
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2077
	call	_ZdlPv@PLT
.L2077:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2078
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rbx
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L2079
	movq	160(%r13), %r15
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r15, %r15
	je	.L2080
	movq	8(%r15), %rax
	movq	(%r15), %r14
	movq	%rax, -184(%rbp)
	cmpq	%r14, %rax
	je	.L2081
	movq	%r12, -200(%rbp)
	jmp	.L2088
	.p2align 4,,10
	.p2align 3
.L2176:
	movq	16(%r12), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L2084
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L2085
	movq	%rdi, -176(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-176(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L2084:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2086
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L2087
	movq	%rdi, -176(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-176(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L2086:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2082:
	addq	$8, %r14
	cmpq	%r14, -184(%rbp)
	je	.L2175
.L2088:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L2082
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L2176
	movq	%r12, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -184(%rbp)
	jne	.L2088
	.p2align 4,,10
	.p2align 3
.L2175:
	movq	-200(%rbp), %r12
	movq	(%r15), %r14
.L2081:
	testq	%r14, %r14
	je	.L2089
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2089:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L2080:
	movq	152(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2090
	call	_ZNKSt14default_deleteISt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime15PropertyPreviewES_IS5_EESaIS7_EEEclEPS9_.isra.0.part.0
.L2090:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2091
	call	_ZdlPv@PLT
.L2091:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2092
	call	_ZdlPv@PLT
.L2092:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2093
	call	_ZdlPv@PLT
.L2093:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2078:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2094
	call	_ZdlPv@PLT
.L2094:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2095
	call	_ZdlPv@PLT
.L2095:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2096
	call	_ZdlPv@PLT
.L2096:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2075:
	addq	$8, -152(%rbp)
	movq	-152(%rbp), %rax
	cmpq	%rax, -192(%rbp)
	jne	.L2097
	movq	-168(%rbp), %rax
	movq	-232(%rbp), %rbx
	movq	-240(%rbp), %r15
	movq	(%rax), %rax
	movq	%rax, -152(%rbp)
.L2074:
	movq	-152(%rbp), %rax
	testq	%rax, %rax
	je	.L2098
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L2098:
	movq	-168(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2073:
	movq	-160(%rbp), %rax
	movq	(%rax), %r12
	movq	%rbx, (%rax)
	testq	%r12, %r12
	je	.L2099
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rbx
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L2100
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	movq	160(%r12), %rax
	movq	%rax, -152(%rbp)
	testq	%rax, %rax
	je	.L2101
	movq	8(%rax), %rcx
	movq	(%rax), %r13
	cmpq	%r13, %rcx
	je	.L2102
	movq	%r12, -168(%rbp)
	movq	%rcx, %r14
	movq	%r15, -176(%rbp)
	jmp	.L2109
	.p2align 4,,10
	.p2align 3
.L2178:
	movq	16(%r12), %r15
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r15, %r15
	je	.L2105
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L2106
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L2105:
	movq	8(%r12), %r15
	testq	%r15, %r15
	je	.L2107
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L2108
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L2107:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2103:
	addq	$8, %r13
	cmpq	%r13, %r14
	je	.L2177
.L2109:
	movq	0(%r13), %r12
	testq	%r12, %r12
	je	.L2103
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L2178
	addq	$8, %r13
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r13, %r14
	jne	.L2109
	.p2align 4,,10
	.p2align 3
.L2177:
	movq	-152(%rbp), %rax
	movq	-168(%rbp), %r12
	movq	-176(%rbp), %r15
	movq	(%rax), %r13
.L2102:
	testq	%r13, %r13
	je	.L2110
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L2110:
	movq	-152(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2101:
	movq	152(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2111
	call	_ZNKSt14default_deleteISt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime15PropertyPreviewES_IS5_EESaIS7_EEEclEPS9_.isra.0.part.0
.L2111:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2112
	call	_ZdlPv@PLT
.L2112:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2113
	call	_ZdlPv@PLT
.L2113:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2114
	call	_ZdlPv@PLT
.L2114:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2099:
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rbx
	cmpq	%rbx, %rdi
	je	.L2115
	call	_ZdlPv@PLT
.L2115:
	movq	8(%r15), %rax
	movq	(%rax), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L2071
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	je	.L2179
.L2071:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2180
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2179:
	.cfi_restore_state
	cmpl	$3, 43(%rax)
	jne	.L2071
	movq	-160(%rbp), %rax
	movq	_ZN12v8_inspector8protocol7Runtime12RemoteObject11SubtypeEnum4NullE(%rip), %rsi
	movq	-224(%rbp), %rdi
	movq	(%rax), %r12
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-144(%rbp), %rsi
	movq	-136(%rbp), %rax
	movq	-208(%rbp), %r15
	movq	-216(%rbp), %r14
	leaq	(%rsi,%rax,2), %rdx
	movq	%r15, %rdi
	movq	%r14, -96(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-112(%rbp), %rax
	leaq	56(%r12), %rdi
	movq	%r15, %rsi
	movq	%rax, -64(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-64(%rbp), %rax
	movb	$1, 48(%r12)
	movq	%rax, 88(%r12)
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L2117
	call	_ZdlPv@PLT
.L2117:
	movq	-144(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L2071
	call	_ZdlPv@PLT
	jmp	.L2071
	.p2align 4,,10
	.p2align 3
.L2076:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2075
	.p2align 4,,10
	.p2align 3
.L2085:
	call	*%rax
	jmp	.L2084
	.p2align 4,,10
	.p2align 3
.L2087:
	call	*%rax
	jmp	.L2086
	.p2align 4,,10
	.p2align 3
.L2079:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2078
	.p2align 4,,10
	.p2align 3
.L2108:
	call	*%rax
	jmp	.L2107
	.p2align 4,,10
	.p2align 3
.L2106:
	call	*%rax
	jmp	.L2105
	.p2align 4,,10
	.p2align 3
.L2100:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2099
.L2180:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7845:
	.size	_ZNK12v8_inspector12_GLOBAL__N_120PrimitiveValueMirror17buildEntryPreviewEN2v85LocalINS2_7ContextEEEPiS6_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteISA_EE, .-_ZNK12v8_inspector12_GLOBAL__N_120PrimitiveValueMirror17buildEntryPreviewEN2v85LocalINS2_7ContextEEEPiS6_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteISA_EE
	.section	.text._ZNK12v8_inspector12_GLOBAL__N_114FunctionMirror20buildPropertyPreviewEN2v85LocalINS2_7ContextEEERKNS_8String16EPSt10unique_ptrINS_8protocol7Runtime15PropertyPreviewESt14default_deleteISC_EE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK12v8_inspector12_GLOBAL__N_114FunctionMirror20buildPropertyPreviewEN2v85LocalINS2_7ContextEEERKNS_8String16EPSt10unique_ptrINS_8protocol7Runtime15PropertyPreviewESt14default_deleteISC_EE, @function
_ZNK12v8_inspector12_GLOBAL__N_114FunctionMirror20buildPropertyPreviewEN2v85LocalINS2_7ContextEEERKNS_8String16EPSt10unique_ptrINS_8protocol7Runtime15PropertyPreviewESt14default_deleteISC_EE:
.LFB7883:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	leaq	-200(%rbp), %rdi
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r15
	pushq	%r13
	leaq	-80(%rbp), %r14
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol7Runtime15PropertyPreview22PropertyPreviewBuilderILi0EEC1Ev
	movq	-200(%rbp), %r13
	movq	%rbx, %rsi
	leaq	8(%r13), %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	32(%rbx), %rax
	movq	_ZN12v8_inspector8protocol7Runtime12RemoteObject8TypeEnum8FunctionE(%rip), %rsi
	movq	%rax, 40(%r13)
	leaq	-192(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-200(%rbp), %rbx
	movq	%r13, %rsi
	leaq	48(%rbx), %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-160(%rbp), %rax
	movq	%r15, %rdi
	movq	-200(%rbp), %r13
	movq	$0, -136(%rbp)
	movq	%rax, 80(%rbx)
	leaq	-128(%rbp), %rbx
	xorl	%eax, %eax
	movq	%rbx, %rdx
	movq	%rbx, %rsi
	movw	%ax, -128(%rbp)
	movq	%rbx, -144(%rbp)
	movq	$0, -112(%rbp)
	movq	%r14, -96(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-112(%rbp), %rax
	leaq	96(%r13), %rdi
	movq	%r15, %rsi
	movq	%rax, -64(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movb	$1, 88(%r13)
	movq	%rax, 128(%r13)
	cmpq	%r14, %rdi
	je	.L2182
	call	_ZdlPv@PLT
.L2182:
	movq	-200(%rbp), %rax
	movq	(%r12), %rdi
	movq	$0, -200(%rbp)
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L2183
	movq	(%rdi), %rax
	call	*24(%rax)
.L2183:
	movq	-144(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L2184
	call	_ZdlPv@PLT
.L2184:
	movq	-192(%rbp), %rdi
	leaq	-176(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2185
	call	_ZdlPv@PLT
.L2185:
	movq	-200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2181
	movq	(%rdi), %rax
	call	*24(%rax)
.L2181:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2195
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2195:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7883:
	.size	_ZNK12v8_inspector12_GLOBAL__N_114FunctionMirror20buildPropertyPreviewEN2v85LocalINS2_7ContextEEERKNS_8String16EPSt10unique_ptrINS_8protocol7Runtime15PropertyPreviewESt14default_deleteISC_EE, .-_ZNK12v8_inspector12_GLOBAL__N_114FunctionMirror20buildPropertyPreviewEN2v85LocalINS2_7ContextEEERKNS_8String16EPSt10unique_ptrINS_8protocol7Runtime15PropertyPreviewESt14default_deleteISC_EE
	.section	.text._ZNK12v8_inspector12_GLOBAL__N_112NumberMirror20buildPropertyPreviewEN2v85LocalINS2_7ContextEEERKNS_8String16EPSt10unique_ptrINS_8protocol7Runtime15PropertyPreviewESt14default_deleteISC_EE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK12v8_inspector12_GLOBAL__N_112NumberMirror20buildPropertyPreviewEN2v85LocalINS2_7ContextEEERKNS_8String16EPSt10unique_ptrINS_8protocol7Runtime15PropertyPreviewESt14default_deleteISC_EE, @function
_ZNK12v8_inspector12_GLOBAL__N_112NumberMirror20buildPropertyPreviewEN2v85LocalINS2_7ContextEEERKNS_8String16EPSt10unique_ptrINS_8protocol7Runtime15PropertyPreviewESt14default_deleteISC_EE:
.LFB7852:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	leaq	-184(%rbp), %rdi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rcx, %rbx
	subq	$160, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol7Runtime15PropertyPreview22PropertyPreviewBuilderILi0EEC1Ev
	movq	-184(%rbp), %r14
	movq	%r12, %rsi
	leaq	8(%r14), %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	32(%r12), %rax
	movq	_ZN12v8_inspector8protocol7Runtime12RemoteObject8TypeEnum6NumberE(%rip), %rsi
	movq	%rax, 40(%r14)
	leaq	-176(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-184(%rbp), %r12
	movq	%r14, %rsi
	leaq	48(%r12), %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-144(%rbp), %rax
	movq	%rax, 80(%r12)
	movq	8(%r13), %rdi
	call	_ZNK2v86Number5ValueEv@PLT
	ucomisd	%xmm0, %xmm0
	jp	.L2219
	ucomisd	.LC25(%rip), %xmm0
	jp	.L2199
	jne	.L2199
	movmskpd	%xmm0, %eax
	testb	$1, %al
	jne	.L2220
	movapd	%xmm0, %xmm1
	andpd	.LC27(%rip), %xmm1
	leaq	.LC23(%rip), %rsi
	ucomisd	.LC28(%rip), %xmm1
	jbe	.L2202
.L2203:
	leaq	-128(%rbp), %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
.L2198:
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rax
	leaq	-80(%rbp), %r14
	leaq	-64(%rbp), %r13
	movq	-184(%rbp), %r12
	movq	%r14, %rdi
	movq	%r13, -80(%rbp)
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-96(%rbp), %rax
	leaq	96(%r12), %rdi
	movq	%r14, %rsi
	movq	%rax, -48(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-48(%rbp), %rax
	movq	-80(%rbp), %rdi
	movb	$1, 88(%r12)
	movq	%rax, 128(%r12)
	cmpq	%r13, %rdi
	je	.L2204
	call	_ZdlPv@PLT
.L2204:
	movq	-184(%rbp), %rax
	movq	(%rbx), %rdi
	movq	$0, -184(%rbp)
	movq	%rax, (%rbx)
	testq	%rdi, %rdi
	je	.L2205
	movq	(%rdi), %rax
	call	*24(%rax)
.L2205:
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2206
	call	_ZdlPv@PLT
.L2206:
	movq	-176(%rbp), %rdi
	leaq	-160(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2207
	call	_ZdlPv@PLT
.L2207:
	movq	-184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2196
	movq	(%rdi), %rax
	call	*24(%rax)
.L2196:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2221
	addq	$160, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2199:
	.cfi_restore_state
	movapd	%xmm0, %xmm1
	andpd	.LC27(%rip), %xmm1
	ucomisd	.LC28(%rip), %xmm1
	jbe	.L2202
	movmskpd	%xmm0, %eax
	leaq	.LC23(%rip), %rsi
	testb	$1, %al
	leaq	.LC22(%rip), %rax
	cmovne	%rax, %rsi
	jmp	.L2203
	.p2align 4,,10
	.p2align 3
.L2202:
	leaq	-128(%rbp), %rdi
	call	_ZN12v8_inspector8String1610fromDoubleEd@PLT
	jmp	.L2198
	.p2align 4,,10
	.p2align 3
.L2220:
	leaq	-128(%rbp), %rdi
	leaq	.LC26(%rip), %rsi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	jmp	.L2198
	.p2align 4,,10
	.p2align 3
.L2219:
	leaq	-128(%rbp), %rdi
	leaq	.LC24(%rip), %rsi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	jmp	.L2198
.L2221:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7852:
	.size	_ZNK12v8_inspector12_GLOBAL__N_112NumberMirror20buildPropertyPreviewEN2v85LocalINS2_7ContextEEERKNS_8String16EPSt10unique_ptrINS_8protocol7Runtime15PropertyPreviewESt14default_deleteISC_EE, .-_ZNK12v8_inspector12_GLOBAL__N_112NumberMirror20buildPropertyPreviewEN2v85LocalINS2_7ContextEEERKNS_8String16EPSt10unique_ptrINS_8protocol7Runtime15PropertyPreviewESt14default_deleteISC_EE
	.section	.rodata._ZNK12v8_inspector12_GLOBAL__N_112SymbolMirror20buildPropertyPreviewEN2v85LocalINS2_7ContextEEERKNS_8String16EPSt10unique_ptrINS_8protocol7Runtime15PropertyPreviewESt14default_deleteISC_EE.str1.1,"aMS",@progbits,1
.LC29:
	.string	")"
.LC30:
	.string	"Symbol("
	.section	.text._ZNK12v8_inspector12_GLOBAL__N_112SymbolMirror20buildPropertyPreviewEN2v85LocalINS2_7ContextEEERKNS_8String16EPSt10unique_ptrINS_8protocol7Runtime15PropertyPreviewESt14default_deleteISC_EE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK12v8_inspector12_GLOBAL__N_112SymbolMirror20buildPropertyPreviewEN2v85LocalINS2_7ContextEEERKNS_8String16EPSt10unique_ptrINS_8protocol7Runtime15PropertyPreviewESt14default_deleteISC_EE, @function
_ZNK12v8_inspector12_GLOBAL__N_112SymbolMirror20buildPropertyPreviewEN2v85LocalINS2_7ContextEEERKNS_8String16EPSt10unique_ptrINS_8protocol7Runtime15PropertyPreviewESt14default_deleteISC_EE:
.LFB7866:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	leaq	-248(%rbp), %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$216, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol7Runtime15PropertyPreview22PropertyPreviewBuilderILi0EEC1Ev
	movq	-248(%rbp), %r15
	movq	%r12, %rsi
	leaq	8(%r15), %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	32(%r12), %rax
	movq	_ZN12v8_inspector8protocol7Runtime12RemoteObject8TypeEnum6SymbolE(%rip), %rsi
	movq	%rax, 40(%r15)
	leaq	-240(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-248(%rbp), %r12
	movq	%r15, %rsi
	leaq	48(%r12), %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-208(%rbp), %rax
	movq	%rax, 80(%r12)
	movq	8(%r13), %rdi
	leaq	-96(%rbp), %r12
	call	_ZNK2v86Symbol4NameEv@PLT
	movq	%r14, %rdi
	leaq	-192(%rbp), %r14
	movq	%rax, %r13
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	leaq	-80(%rbp), %r13
	movq	%rax, %rsi
	call	_ZN12v8_inspector29toProtocolStringWithTypeCheckEPN2v87IsolateENS0_5LocalINS0_5ValueEEE@PLT
	movq	%r14, %rdi
	movq	%r12, %rdx
	leaq	.LC29(%rip), %rcx
	leaq	.LC30(%rip), %rsi
	call	_ZN12v8_inspector8String166concatIJPKcS0_S3_EEES0_DpT_
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L2223
	call	_ZdlPv@PLT
.L2223:
	movq	%r14, %rsi
	leaq	-144(%rbp), %rdi
	movl	$1, %edx
	call	_ZN12v8_inspector12_GLOBAL__N_116abbreviateStringERKNS_8String16ENS0_14AbbreviateModeE
	movq	-144(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r13, -96(%rbp)
	movq	-136(%rbp), %rax
	movq	-248(%rbp), %r14
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-112(%rbp), %rax
	leaq	96(%r14), %rdi
	movq	%r12, %rsi
	movq	%rax, -64(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-64(%rbp), %rax
	movb	$1, 88(%r14)
	movq	%rax, 128(%r14)
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L2224
	call	_ZdlPv@PLT
.L2224:
	movq	-248(%rbp), %rax
	movq	(%rbx), %rdi
	movq	$0, -248(%rbp)
	movq	%rax, (%rbx)
	testq	%rdi, %rdi
	je	.L2225
	movq	(%rdi), %rax
	call	*24(%rax)
.L2225:
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2226
	call	_ZdlPv@PLT
.L2226:
	movq	-192(%rbp), %rdi
	leaq	-176(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2227
	call	_ZdlPv@PLT
.L2227:
	movq	-240(%rbp), %rdi
	leaq	-224(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2228
	call	_ZdlPv@PLT
.L2228:
	movq	-248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2222
	movq	(%rdi), %rax
	call	*24(%rax)
.L2222:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2238
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2238:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7866:
	.size	_ZNK12v8_inspector12_GLOBAL__N_112SymbolMirror20buildPropertyPreviewEN2v85LocalINS2_7ContextEEERKNS_8String16EPSt10unique_ptrINS_8protocol7Runtime15PropertyPreviewESt14default_deleteISC_EE, .-_ZNK12v8_inspector12_GLOBAL__N_112SymbolMirror20buildPropertyPreviewEN2v85LocalINS2_7ContextEEERKNS_8String16EPSt10unique_ptrINS_8protocol7Runtime15PropertyPreviewESt14default_deleteISC_EE
	.section	.text._ZNK12v8_inspector12_GLOBAL__N_112NumberMirror17buildRemoteObjectEN2v85LocalINS2_7ContextEEENS_8WrapModeEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISA_EE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK12v8_inspector12_GLOBAL__N_112NumberMirror17buildRemoteObjectEN2v85LocalINS2_7ContextEEENS_8WrapModeEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISA_EE, @function
_ZNK12v8_inspector12_GLOBAL__N_112NumberMirror17buildRemoteObjectEN2v85LocalINS2_7ContextEEENS_8WrapModeEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISA_EE:
.LFB7851:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -208(%rbp)
	movq	8(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v86Number5ValueEv@PLT
	ucomisd	%xmm0, %xmm0
	jp	.L2265
	ucomisd	.LC25(%rip), %xmm0
	jp	.L2242
	jne	.L2242
	movmskpd	%xmm0, %eax
	testb	$1, %al
	jne	.L2266
	movapd	%xmm0, %xmm1
	andpd	.LC27(%rip), %xmm1
	leaq	.LC23(%rip), %rsi
	ucomisd	.LC28(%rip), %xmm1
	jbe	.L2245
.L2246:
	leaq	-192(%rbp), %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movb	$1, -200(%rbp)
.L2241:
	movl	$320, %edi
	leaq	-144(%rbp), %r14
	leaq	-96(%rbp), %r15
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectC1Ev
	movq	_ZN12v8_inspector8protocol7Runtime12RemoteObject8TypeEnum6NumberE(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rsi
	leaq	16(%rbx), %rdi
	leaq	-80(%rbp), %r14
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-112(%rbp), %rax
	movq	%r15, %rdi
	movq	%r14, -96(%rbp)
	movq	-192(%rbp), %rsi
	movq	%rax, 48(%rbx)
	movq	-184(%rbp), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-160(%rbp), %rax
	leaq	216(%rbx), %rdi
	movq	%r15, %rsi
	movq	%rax, -64(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movb	$1, 208(%rbx)
	movq	%rax, 248(%rbx)
	cmpq	%r14, %rdi
	je	.L2247
	call	_ZdlPv@PLT
.L2247:
	movq	(%r12), %rdi
	movq	%rbx, (%r12)
	testq	%rdi, %rdi
	je	.L2248
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2249
	movq	%rdi, -216(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movq	-216(%rbp), %rdi
	movl	$320, %esi
	call	_ZdlPvm@PLT
.L2248:
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2250
	call	_ZdlPv@PLT
.L2250:
	cmpb	$0, -200(%rbp)
	movq	(%r12), %rbx
	je	.L2251
	movq	-192(%rbp), %rsi
	movq	-184(%rbp), %rax
	movq	%r15, %rdi
	movq	%r14, -96(%rbp)
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-160(%rbp), %rax
	leaq	168(%rbx), %rdi
	movq	%r15, %rsi
	movq	%rax, -64(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-64(%rbp), %rax
	movb	$1, 160(%rbx)
	movq	%rax, 200(%rbx)
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L2253
	call	_ZdlPv@PLT
.L2253:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	movq	-192(%rbp), %rdi
	leaq	-176(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2239
	call	_ZdlPv@PLT
.L2239:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2267
	addq	$184, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2242:
	.cfi_restore_state
	movapd	%xmm0, %xmm1
	andpd	.LC27(%rip), %xmm1
	ucomisd	.LC28(%rip), %xmm1
	jbe	.L2245
	movmskpd	%xmm0, %eax
	leaq	.LC23(%rip), %rsi
	testb	$1, %al
	leaq	.LC22(%rip), %rax
	cmovne	%rax, %rsi
	jmp	.L2246
	.p2align 4,,10
	.p2align 3
.L2251:
	movq	-208(%rbp), %rax
	movq	8(%rax), %rdi
	call	_ZNK2v86Number5ValueEv@PLT
	movl	$24, %edi
	movsd	%xmm0, -200(%rbp)
	call	_Znwm@PLT
	movsd	-200(%rbp), %xmm0
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rcx
	movl	$3, 8(%rax)
	movq	%rcx, (%rax)
	movsd	%xmm0, 16(%rax)
	movq	152(%rbx), %rdi
	movq	%rax, 152(%rbx)
	testq	%rdi, %rdi
	je	.L2253
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L2253
	.p2align 4,,10
	.p2align 3
.L2245:
	leaq	-192(%rbp), %rdi
	call	_ZN12v8_inspector8String1610fromDoubleEd@PLT
	movb	$0, -200(%rbp)
	jmp	.L2241
	.p2align 4,,10
	.p2align 3
.L2266:
	leaq	-192(%rbp), %rdi
	leaq	.LC26(%rip), %rsi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movb	$1, -200(%rbp)
	jmp	.L2241
	.p2align 4,,10
	.p2align 3
.L2249:
	call	*%rax
	jmp	.L2248
	.p2align 4,,10
	.p2align 3
.L2265:
	leaq	-192(%rbp), %rdi
	leaq	.LC24(%rip), %rsi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movb	$1, -200(%rbp)
	jmp	.L2241
.L2267:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7851:
	.size	_ZNK12v8_inspector12_GLOBAL__N_112NumberMirror17buildRemoteObjectEN2v85LocalINS2_7ContextEEENS_8WrapModeEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISA_EE, .-_ZNK12v8_inspector12_GLOBAL__N_112NumberMirror17buildRemoteObjectEN2v85LocalINS2_7ContextEEENS_8WrapModeEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISA_EE
	.section	.text._ZNK12v8_inspector12_GLOBAL__N_120PrimitiveValueMirror20buildPropertyPreviewEN2v85LocalINS2_7ContextEEERKNS_8String16EPSt10unique_ptrINS_8protocol7Runtime15PropertyPreviewESt14default_deleteISC_EE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK12v8_inspector12_GLOBAL__N_120PrimitiveValueMirror20buildPropertyPreviewEN2v85LocalINS2_7ContextEEERKNS_8String16EPSt10unique_ptrINS_8protocol7Runtime15PropertyPreviewESt14default_deleteISC_EE, @function
_ZNK12v8_inspector12_GLOBAL__N_120PrimitiveValueMirror20buildPropertyPreviewEN2v85LocalINS2_7ContextEEERKNS_8String16EPSt10unique_ptrINS_8protocol7Runtime15PropertyPreviewESt14default_deleteISC_EE:
.LFB7846:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	-200(%rbp), %rdi
	subq	$184, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol7Runtime15PropertyPreview22PropertyPreviewBuilderILi0EEC1Ev
	movq	-200(%rbp), %r15
	movq	%r13, %rsi
	leaq	8(%r15), %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	32(%r13), %rax
	leaq	-192(%rbp), %r13
	movq	%r14, %rsi
	movq	%r13, %rdi
	leaq	-96(%rbp), %r14
	movq	%rax, 40(%r15)
	movq	8(%rbx), %rdx
	call	_ZN12v8_inspector12_GLOBAL__N_127descriptionForPrimitiveTypeEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE
	leaq	-144(%rbp), %rax
	movq	%r13, %rsi
	xorl	%edx, %edx
	movq	%rax, %rdi
	movq	%rax, -224(%rbp)
	leaq	-80(%rbp), %r13
	call	_ZN12v8_inspector12_GLOBAL__N_116abbreviateStringERKNS_8String16ENS0_14AbbreviateModeE
	movq	-144(%rbp), %rsi
	movq	%r14, %rdi
	movq	%r13, -96(%rbp)
	movq	-136(%rbp), %rdx
	movq	-200(%rbp), %r15
	leaq	(%rsi,%rdx,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-112(%rbp), %rdx
	leaq	96(%r15), %rdi
	movq	%r14, %rsi
	movq	%rdx, -64(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-64(%rbp), %rdx
	movb	$1, 88(%r15)
	movq	%rdx, 128(%r15)
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L2269
	call	_ZdlPv@PLT
.L2269:
	movq	-200(%rbp), %rax
	leaq	16(%rbx), %rsi
	leaq	48(%rax), %rdi
	movq	%rax, -216(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	48(%rbx), %rdx
	movq	-216(%rbp), %rax
	movq	%rdx, 80(%rax)
	movq	-200(%rbp), %rax
	movq	$0, -200(%rbp)
	movq	(%r12), %rdi
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L2270
	movq	(%rdi), %rax
	call	*24(%rax)
.L2270:
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	movq	%rax, %r15
	cmpq	%rax, %rdi
	je	.L2271
	call	_ZdlPv@PLT
.L2271:
	movq	-192(%rbp), %rdi
	leaq	-176(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2272
	call	_ZdlPv@PLT
.L2272:
	movq	-200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2273
	movq	(%rdi), %rax
	call	*24(%rax)
.L2273:
	movq	8(%rbx), %rax
	movq	(%rax), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L2268
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	je	.L2285
.L2268:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2286
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2285:
	.cfi_restore_state
	cmpl	$3, 43(%rax)
	jne	.L2268
	movq	_ZN12v8_inspector8protocol7Runtime12RemoteObject11SubtypeEnum4NullE(%rip), %rsi
	movq	-224(%rbp), %rdi
	movq	(%r12), %rbx
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-144(%rbp), %rsi
	movq	%r14, %rdi
	movq	%r13, -96(%rbp)
	movq	-136(%rbp), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-112(%rbp), %rax
	leaq	152(%rbx), %rdi
	movq	%r14, %rsi
	movq	%rax, -64(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-64(%rbp), %rax
	movb	$1, 144(%rbx)
	movq	%rax, 184(%rbx)
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L2275
	call	_ZdlPv@PLT
.L2275:
	movq	-144(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L2268
	call	_ZdlPv@PLT
	jmp	.L2268
.L2286:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7846:
	.size	_ZNK12v8_inspector12_GLOBAL__N_120PrimitiveValueMirror20buildPropertyPreviewEN2v85LocalINS2_7ContextEEERKNS_8String16EPSt10unique_ptrINS_8protocol7Runtime15PropertyPreviewESt14default_deleteISC_EE, .-_ZNK12v8_inspector12_GLOBAL__N_120PrimitiveValueMirror20buildPropertyPreviewEN2v85LocalINS2_7ContextEEERKNS_8String16EPSt10unique_ptrINS_8protocol7Runtime15PropertyPreviewESt14default_deleteISC_EE
	.section	.rodata._ZNK12v8_inspector12_GLOBAL__N_114LocationMirror17buildRemoteObjectEN2v85LocalINS2_7ContextEEENS_8WrapModeEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISA_EE.str1.1,"aMS",@progbits,1
.LC31:
	.string	"scriptId"
.LC32:
	.string	"lineNumber"
.LC33:
	.string	"columnNumber"
.LC34:
	.string	"internal#location"
.LC35:
	.string	"Object"
	.section	.text._ZNK12v8_inspector12_GLOBAL__N_114LocationMirror17buildRemoteObjectEN2v85LocalINS2_7ContextEEENS_8WrapModeEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISA_EE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK12v8_inspector12_GLOBAL__N_114LocationMirror17buildRemoteObjectEN2v85LocalINS2_7ContextEEENS_8WrapModeEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISA_EE, @function
_ZNK12v8_inspector12_GLOBAL__N_114LocationMirror17buildRemoteObjectEN2v85LocalINS2_7ContextEEENS_8WrapModeEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISA_EE:
.LFB7872:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-144(%rbp), %r15
	leaq	-80(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-96(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$232, %rsp
	movq	%r8, -256(%rbp)
	movq	%rdi, -248(%rbp)
	movl	$96, %edi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movl	16(%rbx), %esi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String1611fromIntegerEi@PLT
	leaq	.LC31(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r15, %rdx
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue9setStringERKNS_8String16ES4_@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L2288
	call	_ZdlPv@PLT
.L2288:
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	movq	%rax, -264(%rbp)
	cmpq	%rax, %rdi
	je	.L2289
	call	_ZdlPv@PLT
.L2289:
	movl	20(%rbx), %edx
	leaq	.LC32(%rip), %rsi
	movq	%r12, %rdi
	movl	%edx, -272(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movl	-272(%rbp), %edx
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue10setIntegerERKNS_8String16Ei@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L2290
	call	_ZdlPv@PLT
.L2290:
	movl	24(%rbx), %ebx
	leaq	.LC33(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	movl	%ebx, %edx
	call	_ZN12v8_inspector8protocol15DictionaryValue10setIntegerERKNS_8String16Ei@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L2291
	call	_ZdlPv@PLT
.L2291:
	movl	$320, %edi
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectC1Ev
	leaq	-240(%rbp), %r8
	movq	_ZN12v8_inspector8protocol7Runtime12RemoteObject8TypeEnum6ObjectE(%rip), %rsi
	movq	%r8, %rdi
	movq	%r8, -272(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-272(%rbp), %r8
	leaq	16(%rbx), %rdi
	movq	%r8, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-208(%rbp), %rax
	leaq	-192(%rbp), %rdi
	leaq	.LC34(%rip), %rsi
	movq	%rax, 48(%rbx)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-192(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r14, -96(%rbp)
	movq	-184(%rbp), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-160(%rbp), %rax
	leaq	64(%rbx), %rdi
	movq	%r12, %rsi
	movq	%rax, -64(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movb	$1, 56(%rbx)
	movq	%rax, 96(%rbx)
	cmpq	%r14, %rdi
	je	.L2292
	call	_ZdlPv@PLT
.L2292:
	leaq	.LC35(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-144(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r14, -96(%rbp)
	movq	-136(%rbp), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-112(%rbp), %rax
	leaq	216(%rbx), %rdi
	movq	%r12, %rsi
	movq	%rax, -64(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movb	$1, 208(%rbx)
	movq	%rax, 248(%rbx)
	cmpq	%r14, %rdi
	je	.L2293
	call	_ZdlPv@PLT
.L2293:
	movq	152(%rbx), %rdi
	movq	%r13, 152(%rbx)
	testq	%rdi, %rdi
	je	.L2294
	movq	(%rdi), %rax
	call	*24(%rax)
.L2294:
	movq	-256(%rbp), %rax
	movq	(%rax), %r12
	movq	%rbx, (%rax)
	testq	%r12, %r12
	je	.L2295
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2296
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2295:
	movq	-144(%rbp), %rdi
	cmpq	-264(%rbp), %rdi
	je	.L2297
	call	_ZdlPv@PLT
.L2297:
	movq	-192(%rbp), %rdi
	leaq	-176(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2298
	call	_ZdlPv@PLT
.L2298:
	movq	-240(%rbp), %rdi
	leaq	-224(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2299
	call	_ZdlPv@PLT
.L2299:
	movq	-248(%rbp), %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2308
	movq	-248(%rbp), %rax
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2296:
	.cfi_restore_state
	call	*%rax
	jmp	.L2295
.L2308:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7872:
	.size	_ZNK12v8_inspector12_GLOBAL__N_114LocationMirror17buildRemoteObjectEN2v85LocalINS2_7ContextEEENS_8WrapModeEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISA_EE, .-_ZNK12v8_inspector12_GLOBAL__N_114LocationMirror17buildRemoteObjectEN2v85LocalINS2_7ContextEEENS_8WrapModeEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISA_EE
	.section	.text._ZNK12v8_inspector12_GLOBAL__N_112SymbolMirror17buildRemoteObjectEN2v85LocalINS2_7ContextEEENS_8WrapModeEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISA_EE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK12v8_inspector12_GLOBAL__N_112SymbolMirror17buildRemoteObjectEN2v85LocalINS2_7ContextEEENS_8WrapModeEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISA_EE, @function
_ZNK12v8_inspector12_GLOBAL__N_112SymbolMirror17buildRemoteObjectEN2v85LocalINS2_7ContextEEENS_8WrapModeEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISA_EE:
.LFB7865:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jne	.L2310
	leaq	-96(%rbp), %r13
	leaq	.LC12(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2309
	call	_ZdlPv@PLT
.L2309:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2324
	addq	$168, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2310:
	.cfi_restore_state
	movl	$320, %edi
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%r8, %r13
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectC1Ev
	leaq	-192(%rbp), %r8
	movq	_ZN12v8_inspector8protocol7Runtime12RemoteObject8TypeEnum6SymbolE(%rip), %rsi
	movq	%r8, %rdi
	movq	%r8, -200(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-200(%rbp), %r8
	leaq	16(%rbx), %rdi
	movq	%r8, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	8(%r15), %rdi
	movq	-160(%rbp), %rax
	movq	%rax, 48(%rbx)
	call	_ZNK2v86Symbol4NameEv@PLT
	movq	%r14, %rdi
	leaq	-96(%rbp), %r14
	movq	%rax, %r15
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%r15, %rdx
	movq	%r14, %rdi
	leaq	-80(%rbp), %r15
	movq	%rax, %rsi
	call	_ZN12v8_inspector29toProtocolStringWithTypeCheckEPN2v87IsolateENS0_5LocalINS0_5ValueEEE@PLT
	leaq	-144(%rbp), %rdi
	movq	%r14, %rdx
	leaq	.LC29(%rip), %rcx
	leaq	.LC30(%rip), %rsi
	call	_ZN12v8_inspector8String166concatIJPKcS0_S3_EEES0_DpT_
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L2313
	call	_ZdlPv@PLT
.L2313:
	movq	-144(%rbp), %rsi
	movq	-136(%rbp), %rax
	movq	%r14, %rdi
	movq	%r15, -96(%rbp)
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-112(%rbp), %rax
	leaq	216(%rbx), %rdi
	movq	%r14, %rsi
	movq	%rax, -64(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movb	$1, 208(%rbx)
	movq	%rax, 248(%rbx)
	cmpq	%r15, %rdi
	je	.L2314
	call	_ZdlPv@PLT
.L2314:
	movq	0(%r13), %r14
	movq	%rbx, 0(%r13)
	testq	%r14, %r14
	je	.L2315
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	%r14, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2316
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2315:
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2317
	call	_ZdlPv@PLT
.L2317:
	movq	-192(%rbp), %rdi
	leaq	-176(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2318
	call	_ZdlPv@PLT
.L2318:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	jmp	.L2309
	.p2align 4,,10
	.p2align 3
.L2316:
	call	*%rax
	jmp	.L2315
.L2324:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7865:
	.size	_ZNK12v8_inspector12_GLOBAL__N_112SymbolMirror17buildRemoteObjectEN2v85LocalINS2_7ContextEEENS_8WrapModeEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISA_EE, .-_ZNK12v8_inspector12_GLOBAL__N_112SymbolMirror17buildRemoteObjectEN2v85LocalINS2_7ContextEEENS_8WrapModeEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISA_EE
	.section	.text._ZNK12v8_inspector12_GLOBAL__N_120PrimitiveValueMirror17buildRemoteObjectEN2v85LocalINS2_7ContextEEENS_8WrapModeEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISA_EE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK12v8_inspector12_GLOBAL__N_120PrimitiveValueMirror17buildRemoteObjectEN2v85LocalINS2_7ContextEEENS_8WrapModeEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISA_EE, @function
_ZNK12v8_inspector12_GLOBAL__N_120PrimitiveValueMirror17buildRemoteObjectEN2v85LocalINS2_7ContextEEENS_8WrapModeEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISA_EE:
.LFB7844:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movq	%rdx, %rsi
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	8(%r12), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -168(%rbp)
	movq	(%rdx), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L2326
	movq	-1(%rax), %rcx
	cmpw	$67, 11(%rcx)
	je	.L2347
.L2326:
	leaq	-112(%rbp), %r15
	leaq	-168(%rbp), %r8
	movl	$1000, %ecx
	movq	%r15, %rdi
	call	_ZN12v8_inspector12_GLOBAL__N_115toProtocolValueEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEEiPSt10unique_ptrINS_8protocol5ValueESt14default_deleteIS9_EE.constprop.0
.L2327:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2328
	call	_ZdlPv@PLT
.L2328:
	movl	$320, %edi
	call	_Znwm@PLT
	movq	%rax, %rbx
	movq	%rax, %rdi
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectC1Ev
	leaq	16(%rbx), %rdi
	leaq	16(%r12), %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	48(%r12), %rax
	movq	152(%rbx), %rdi
	movq	%rax, 48(%rbx)
	movq	-168(%rbp), %rax
	movq	$0, -168(%rbp)
	movq	%rax, 152(%rbx)
	testq	%rdi, %rdi
	je	.L2329
	movq	(%rdi), %rax
	call	*24(%rax)
.L2329:
	movq	0(%r13), %rdi
	movq	%rbx, 0(%r13)
	testq	%rdi, %rdi
	je	.L2330
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2331
	movq	%rdi, -184(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movq	-184(%rbp), %rdi
	movl	$320, %esi
	call	_ZdlPvm@PLT
.L2330:
	movq	8(%r12), %rax
	movq	(%rax), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L2332
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	je	.L2348
.L2332:
	movq	%r14, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2325
	movq	(%rdi), %rax
	call	*24(%rax)
.L2325:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2349
	addq	$152, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2348:
	.cfi_restore_state
	cmpl	$3, 43(%rax)
	jne	.L2332
	movq	_ZN12v8_inspector8protocol7Runtime12RemoteObject11SubtypeEnum4NullE(%rip), %rsi
	leaq	-160(%rbp), %rdi
	movq	0(%r13), %rbx
	leaq	-96(%rbp), %r12
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-160(%rbp), %rsi
	movq	%r15, %rdi
	movq	%r12, -112(%rbp)
	movq	-152(%rbp), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-128(%rbp), %rax
	leaq	64(%rbx), %rdi
	movq	%r15, %rsi
	movq	%rax, -80(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-80(%rbp), %rax
	movb	$1, 56(%rbx)
	movq	%rax, 96(%rbx)
	movq	-112(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L2333
	call	_ZdlPv@PLT
.L2333:
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2332
	call	_ZdlPv@PLT
	jmp	.L2332
	.p2align 4,,10
	.p2align 3
.L2347:
	cmpl	$5, 43(%rax)
	jne	.L2326
	leaq	-112(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	jmp	.L2327
	.p2align 4,,10
	.p2align 3
.L2331:
	call	*%rax
	jmp	.L2330
.L2349:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7844:
	.size	_ZNK12v8_inspector12_GLOBAL__N_120PrimitiveValueMirror17buildRemoteObjectEN2v85LocalINS2_7ContextEEENS_8WrapModeEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISA_EE, .-_ZNK12v8_inspector12_GLOBAL__N_120PrimitiveValueMirror17buildRemoteObjectEN2v85LocalINS2_7ContextEEENS_8WrapModeEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISA_EE
	.section	.text._ZNK12v8_inspector12_GLOBAL__N_114FunctionMirror17buildRemoteObjectEN2v85LocalINS2_7ContextEEENS_8WrapModeEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISA_EE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK12v8_inspector12_GLOBAL__N_114FunctionMirror17buildRemoteObjectEN2v85LocalINS2_7ContextEEENS_8WrapModeEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISA_EE, @function
_ZNK12v8_inspector12_GLOBAL__N_114FunctionMirror17buildRemoteObjectEN2v85LocalINS2_7ContextEEENS_8WrapModeEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISA_EE:
.LFB7882:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$296, %rsp
	.cfi_offset 3, -56
	movq	%r8, -312(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jne	.L2351
	movq	$0, -304(%rbp)
	movq	8(%rsi), %rdx
	movq	(%rdx), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L2394
.L2352:
	leaq	-112(%rbp), %rdi
	leaq	-304(%rbp), %r8
	movl	$1000, %ecx
	movq	%r13, %rsi
	call	_ZN12v8_inspector12_GLOBAL__N_115toProtocolValueEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEEiPSt10unique_ptrINS_8protocol5ValueESt14default_deleteIS9_EE.constprop.0
.L2353:
	movl	-112(%rbp), %eax
	testl	%eax, %eax
	je	.L2354
	movl	%eax, (%r12)
	leaq	24(%r12), %rax
	leaq	-88(%rbp), %rdx
	movq	%rax, 8(%r12)
	movq	-104(%rbp), %rax
	cmpq	%rdx, %rax
	je	.L2395
	movq	%rax, 8(%r12)
	movq	-88(%rbp), %rax
	movq	%rax, 24(%r12)
.L2356:
	movq	-96(%rbp), %rax
	movq	-304(%rbp), %rdi
	movq	%rax, 16(%r12)
	movq	-72(%rbp), %rax
	movq	%rax, 40(%r12)
	movl	-64(%rbp), %eax
	movl	%eax, 48(%r12)
	testq	%rdi, %rdi
	je	.L2350
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L2350
	.p2align 4,,10
	.p2align 3
.L2351:
	movl	$320, %edi
	movq	%rsi, -320(%rbp)
	leaq	-256(%rbp), %r14
	leaq	-112(%rbp), %r15
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectC1Ev
	movq	_ZN12v8_inspector8protocol7Runtime12RemoteObject8TypeEnum8FunctionE(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rsi
	leaq	16(%rbx), %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-224(%rbp), %rax
	movq	-320(%rbp), %r10
	movq	%rax, 48(%rbx)
	movq	8(%r10), %rdi
	call	_ZN2v86Object18GetConstructorNameEv@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%r14, %rdx
	leaq	-208(%rbp), %rdi
	leaq	-96(%rbp), %r14
	movq	%rax, %rsi
	call	_ZN12v8_inspector29toProtocolStringWithTypeCheckEPN2v87IsolateENS0_5LocalINS0_5ValueEEE@PLT
	movq	-208(%rbp), %rsi
	movq	%r15, %rdi
	movq	%r14, -112(%rbp)
	movq	-200(%rbp), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-176(%rbp), %rax
	leaq	112(%rbx), %rdi
	movq	%r15, %rsi
	movq	%rax, -80(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-112(%rbp), %rdi
	movq	-80(%rbp), %rax
	movb	$1, 104(%rbx)
	movq	-320(%rbp), %r10
	cmpq	%r14, %rdi
	movq	%rax, 144(%rbx)
	je	.L2366
	call	_ZdlPv@PLT
	movq	-320(%rbp), %r10
.L2366:
	movq	8(%r10), %rax
	movq	%r13, %rdi
	movq	%rax, -320(%rbp)
	call	_ZN2v87Context10GetIsolateEv@PLT
	leaq	-304(%rbp), %r10
	movq	%r10, %rdi
	movq	%rax, %rsi
	movq	%r10, -328(%rbp)
	movq	%rax, -336(%rbp)
	call	_ZN2v88TryCatchC1EPNS_7IsolateE@PLT
	movq	-320(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNK2v85Value8ToStringENS_5LocalINS_7ContextEEE@PLT
	movq	-328(%rbp), %r10
	movq	-336(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L2396
	leaq	-160(%rbp), %rdi
	movq	%r8, %rsi
	movq	%r10, -320(%rbp)
	call	_ZN12v8_inspector16toProtocolStringEPN2v87IsolateENS0_5LocalINS0_6StringEEE@PLT
	movq	-320(%rbp), %r10
.L2375:
	movq	%r10, %rdi
	call	_ZN2v88TryCatchD1Ev@PLT
	movq	-160(%rbp), %rsi
	movq	%r15, %rdi
	movq	%r14, -112(%rbp)
	movq	-152(%rbp), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-128(%rbp), %rax
	leaq	216(%rbx), %rdi
	movq	%r15, %rsi
	movq	%rax, -80(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-80(%rbp), %rax
	movq	-112(%rbp), %rdi
	movb	$1, 208(%rbx)
	movq	%rax, 248(%rbx)
	cmpq	%r14, %rdi
	je	.L2369
	call	_ZdlPv@PLT
.L2369:
	movq	-312(%rbp), %rax
	movq	(%rax), %r14
	movq	%rbx, (%rax)
	testq	%r14, %r14
	je	.L2370
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	%r14, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2371
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2370:
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2372
	call	_ZdlPv@PLT
.L2372:
	movq	-208(%rbp), %rdi
	leaq	-192(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2373
	call	_ZdlPv@PLT
.L2373:
	movq	-256(%rbp), %rdi
	leaq	-240(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2365
	call	_ZdlPv@PLT
.L2365:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
.L2350:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2397
	addq	$296, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2394:
	.cfi_restore_state
	movq	-1(%rax), %rcx
	cmpw	$67, 11(%rcx)
	jne	.L2352
	cmpl	$5, 43(%rax)
	jne	.L2352
	leaq	-112(%rbp), %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	jmp	.L2353
	.p2align 4,,10
	.p2align 3
.L2396:
	movq	-320(%rbp), %rdi
	movq	%r10, -336(%rbp)
	movq	%r8, -328(%rbp)
	call	_ZN2v86Object18GetConstructorNameEv@PLT
	movq	-328(%rbp), %r8
	leaq	-160(%rbp), %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN12v8_inspector16toProtocolStringEPN2v87IsolateENS0_5LocalINS0_6StringEEE@PLT
	movq	-336(%rbp), %r10
	jmp	.L2375
	.p2align 4,,10
	.p2align 3
.L2354:
	movl	$320, %edi
	leaq	-160(%rbp), %r14
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectC1Ev
	movq	_ZN12v8_inspector8protocol7Runtime12RemoteObject8TypeEnum8FunctionE(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	leaq	16(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-128(%rbp), %rax
	movq	152(%rbx), %rdi
	movq	%rax, 48(%rbx)
	movq	-304(%rbp), %rax
	movq	$0, -304(%rbp)
	movq	%rax, 152(%rbx)
	testq	%rdi, %rdi
	je	.L2359
	movq	(%rdi), %rax
	call	*24(%rax)
.L2359:
	movq	-312(%rbp), %rax
	movq	(%rax), %r14
	movq	%rbx, (%rax)
	testq	%r14, %r14
	je	.L2360
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	%r14, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2361
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2360:
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2362
	call	_ZdlPv@PLT
.L2362:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2363
	call	_ZdlPv@PLT
.L2363:
	movq	-304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2365
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L2365
	.p2align 4,,10
	.p2align 3
.L2395:
	movdqu	-88(%rbp), %xmm0
	movups	%xmm0, 24(%r12)
	jmp	.L2356
	.p2align 4,,10
	.p2align 3
.L2371:
	call	*%rax
	jmp	.L2370
	.p2align 4,,10
	.p2align 3
.L2361:
	call	*%rax
	jmp	.L2360
.L2397:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7882:
	.size	_ZNK12v8_inspector12_GLOBAL__N_114FunctionMirror17buildRemoteObjectEN2v85LocalINS2_7ContextEEENS_8WrapModeEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISA_EE, .-_ZNK12v8_inspector12_GLOBAL__N_114FunctionMirror17buildRemoteObjectEN2v85LocalINS2_7ContextEEENS_8WrapModeEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISA_EE
	.section	.text._ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPN12v8_inspector8String16ESt6vectorIS3_SaIS3_EEEENS0_5__ops16_Iter_equals_valIKS3_EEET_SD_SD_T0_St26random_access_iterator_tag,"axG",@progbits,_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPN12v8_inspector8String16ESt6vectorIS3_SaIS3_EEEENS0_5__ops16_Iter_equals_valIKS3_EEET_SD_SD_T0_St26random_access_iterator_tag,comdat
	.p2align 4
	.weak	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPN12v8_inspector8String16ESt6vectorIS3_SaIS3_EEEENS0_5__ops16_Iter_equals_valIKS3_EEET_SD_SD_T0_St26random_access_iterator_tag
	.type	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPN12v8_inspector8String16ESt6vectorIS3_SaIS3_EEEENS0_5__ops16_Iter_equals_valIKS3_EEET_SD_SD_T0_St26random_access_iterator_tag, @function
_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPN12v8_inspector8String16ESt6vectorIS3_SaIS3_EEEENS0_5__ops16_Iter_equals_valIKS3_EEET_SD_SD_T0_St26random_access_iterator_tag:
.LFB12752:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	subq	%rdi, %rax
	sarq	$3, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	movabsq	$-3689348814741910323, %rdx
	imulq	%rdx, %rax
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rax, %rdx
	sarq	$2, %rdx
	testq	%rdx, %rdx
	jle	.L2399
	leaq	(%rdx,%rdx,4), %rsi
	movq	8(%r13), %rcx
	movq	0(%r13), %r9
	movl	$2147483648, %eax
	movabsq	$-2147483649, %rdx
	salq	$5, %rsi
	addq	%rdi, %rsi
	.p2align 4,,10
	.p2align 3
.L2413:
	movq	8(%rbx), %r8
	movq	%rcx, %r10
	movq	(%rbx), %r11
	cmpq	%rcx, %r8
	cmovbe	%r8, %r10
	testq	%r10, %r10
	je	.L2400
	xorl	%edi, %edi
	jmp	.L2402
	.p2align 4,,10
	.p2align 3
.L2448:
	addq	$1, %rdi
	cmpq	%r10, %rdi
	je	.L2400
.L2402:
	movzwl	(%r9,%rdi,2), %r14d
	cmpw	%r14w, (%r11,%rdi,2)
	je	.L2448
.L2401:
	movq	48(%rbx), %r8
	movq	40(%rbx), %r11
	cmpq	%r8, %rcx
	movq	%r8, %r10
	cmovbe	%rcx, %r10
	testq	%r10, %r10
	je	.L2404
	xorl	%edi, %edi
	jmp	.L2406
	.p2align 4,,10
	.p2align 3
.L2449:
	addq	$1, %rdi
	cmpq	%r10, %rdi
	je	.L2404
.L2406:
	movzwl	(%r9,%rdi,2), %r14d
	cmpw	%r14w, (%r11,%rdi,2)
	je	.L2449
.L2405:
	movq	88(%rbx), %r8
	movq	80(%rbx), %r11
	cmpq	%r8, %rcx
	movq	%r8, %r10
	cmovbe	%rcx, %r10
	testq	%r10, %r10
	je	.L2407
	xorl	%edi, %edi
	jmp	.L2409
	.p2align 4,,10
	.p2align 3
.L2450:
	addq	$1, %rdi
	cmpq	%r10, %rdi
	je	.L2407
.L2409:
	movzwl	(%r9,%rdi,2), %r14d
	cmpw	%r14w, (%r11,%rdi,2)
	je	.L2450
.L2408:
	movq	128(%rbx), %r8
	movq	120(%rbx), %r11
	cmpq	%r8, %rcx
	movq	%r8, %r10
	cmovbe	%rcx, %r10
	testq	%r10, %r10
	je	.L2410
	xorl	%edi, %edi
	jmp	.L2412
	.p2align 4,,10
	.p2align 3
.L2451:
	addq	$1, %rdi
	cmpq	%r10, %rdi
	je	.L2410
.L2412:
	movzwl	(%r9,%rdi,2), %r14d
	cmpw	%r14w, (%r11,%rdi,2)
	je	.L2451
.L2411:
	addq	$160, %rbx
	cmpq	%rbx, %rsi
	jne	.L2413
	movabsq	$-3689348814741910323, %rdx
	movq	%r12, %rax
	subq	%rbx, %rax
	sarq	$3, %rax
	imulq	%rdx, %rax
.L2399:
	cmpq	$2, %rax
	je	.L2418
	cmpq	$3, %rax
	je	.L2415
	cmpq	$1, %rax
	je	.L2419
.L2417:
	movq	%r12, %rax
	jmp	.L2441
	.p2align 4,,10
	.p2align 3
.L2400:
	subq	%rcx, %r8
	cmpq	%rax, %r8
	jge	.L2401
	cmpq	%rdx, %r8
	jle	.L2401
	testl	%r8d, %r8d
	jne	.L2401
.L2447:
	movq	%rbx, %rax
.L2441:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2404:
	.cfi_restore_state
	subq	%rcx, %r8
	cmpq	%rax, %r8
	jge	.L2405
	cmpq	%rdx, %r8
	jle	.L2405
	testl	%r8d, %r8d
	jne	.L2405
	leaq	40(%rbx), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2407:
	.cfi_restore_state
	subq	%rcx, %r8
	cmpq	%rax, %r8
	jge	.L2408
	cmpq	%rdx, %r8
	jle	.L2408
	testl	%r8d, %r8d
	jne	.L2408
	leaq	80(%rbx), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2410:
	.cfi_restore_state
	subq	%rcx, %r8
	cmpq	%rax, %r8
	jge	.L2411
	cmpq	%rdx, %r8
	jle	.L2411
	testl	%r8d, %r8d
	jne	.L2411
	leaq	120(%rbx), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2415:
	.cfi_restore_state
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_
	testl	%eax, %eax
	je	.L2447
	addq	$40, %rbx
.L2418:
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_
	testl	%eax, %eax
	je	.L2447
	addq	$40, %rbx
.L2419:
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_
	testl	%eax, %eax
	jne	.L2417
	jmp	.L2447
	.cfi_endproc
.LFE12752:
	.size	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPN12v8_inspector8String16ESt6vectorIS3_SaIS3_EEEENS0_5__ops16_Iter_equals_valIKS3_EEET_SD_SD_T0_St26random_access_iterator_tag, .-_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPN12v8_inspector8String16ESt6vectorIS3_SaIS3_EEEENS0_5__ops16_Iter_equals_valIKS3_EEET_SD_SD_T0_St26random_access_iterator_tag
	.section	.text._ZNSt6vectorIN12v8_inspector14PropertyMirrorESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN12v8_inspector14PropertyMirrorESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN12v8_inspector14PropertyMirrorESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	.type	_ZNSt6vectorIN12v8_inspector14PropertyMirrorESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_, @function
_ZNSt6vectorIN12v8_inspector14PropertyMirrorESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_:
.LFB12755:
	.cfi_startproc
	endbr64
	movabsq	$3353953467947191203, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movabsq	$104811045873349725, %rsi
	subq	$56, %rsp
	movq	8(%rdi), %r12
	movq	(%rdi), %r14
	movq	%rdi, -72(%rbp)
	movq	%r12, %rax
	subq	%r14, %rax
	sarq	$3, %rax
	imulq	%rcx, %rax
	cmpq	%rsi, %rax
	je	.L2499
	movq	%rbx, %r8
	subq	%r14, %r8
	testq	%rax, %rax
	je	.L2476
	movabsq	$9223372036854775800, %rcx
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L2500
.L2454:
	movq	%rcx, %rdi
	movq	%rdx, -88(%rbp)
	movq	%r8, -80(%rbp)
	movq	%rcx, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	-80(%rbp), %r8
	movq	%rax, -56(%rbp)
	movq	-88(%rbp), %rdx
	leaq	88(%rax), %rsi
	addq	%rax, %rcx
	movq	%rcx, -64(%rbp)
.L2475:
	movq	-56(%rbp), %rax
	movq	(%rdx), %rdi
	addq	%r8, %rax
	leaq	16(%rax), %rcx
	movq	%rcx, (%rax)
	leaq	16(%rdx), %rcx
	cmpq	%rcx, %rdi
	je	.L2501
	movq	%rdi, (%rax)
	movq	16(%rdx), %rdi
	movq	%rdi, 16(%rax)
.L2457:
	movq	%rcx, (%rdx)
	xorl	%ecx, %ecx
	movq	8(%rdx), %rdi
	pxor	%xmm0, %xmm0
	movw	%cx, 16(%rdx)
	movzbl	44(%rdx), %ecx
	movq	%rdi, 8(%rax)
	movq	32(%rdx), %rdi
	movb	%cl, 44(%rax)
	movq	48(%rdx), %rcx
	movq	%rdi, 32(%rax)
	movl	40(%rdx), %edi
	movq	%rcx, 48(%rax)
	movq	56(%rdx), %rcx
	movq	$0, 8(%rdx)
	movq	%rcx, 56(%rax)
	movq	64(%rdx), %rcx
	movl	%edi, 40(%rax)
	movq	%rcx, 64(%rax)
	movq	72(%rdx), %rcx
	movups	%xmm0, 48(%rdx)
	movq	%rcx, 72(%rax)
	movq	80(%rdx), %rcx
	movups	%xmm0, 64(%rdx)
	movq	$0, 80(%rdx)
	movq	%rcx, 80(%rax)
	cmpq	%r14, %rbx
	je	.L2458
	movq	-56(%rbp), %r13
	leaq	16(%r14), %r15
	jmp	.L2469
	.p2align 4,,10
	.p2align 3
.L2459:
	movq	%rcx, 0(%r13)
	movq	(%r15), %rcx
	movq	%rcx, 16(%r13)
.L2460:
	movq	-8(%r15), %rcx
	xorl	%eax, %eax
	movq	%rcx, 8(%r13)
	movq	16(%r15), %rcx
	movq	%r15, -16(%r15)
	movq	$0, -8(%r15)
	movw	%ax, (%r15)
	movq	%rcx, 32(%r13)
	movzbl	24(%r15), %ecx
	movb	%cl, 40(%r13)
	movzbl	25(%r15), %ecx
	movb	%cl, 41(%r13)
	movzbl	26(%r15), %ecx
	movb	%cl, 42(%r13)
	movzbl	27(%r15), %ecx
	movb	%cl, 43(%r13)
	movzbl	28(%r15), %ecx
	movb	%cl, 44(%r13)
	movq	32(%r15), %rcx
	movq	$0, 32(%r15)
	movq	%rcx, 48(%r13)
	movq	40(%r15), %rcx
	movq	$0, 40(%r15)
	movq	%rcx, 56(%r13)
	movq	48(%r15), %rcx
	movq	$0, 48(%r15)
	movq	%rcx, 64(%r13)
	movq	56(%r15), %rcx
	movq	$0, 56(%r15)
	movq	%rcx, 72(%r13)
	movq	64(%r15), %rcx
	movq	$0, 64(%r15)
	movq	%rcx, 80(%r13)
	movq	64(%r15), %rdi
	testq	%rdi, %rdi
	je	.L2461
	movq	(%rdi), %rcx
	call	*8(%rcx)
.L2461:
	movq	56(%r15), %rdi
	testq	%rdi, %rdi
	je	.L2462
	movq	(%rdi), %rcx
	call	*8(%rcx)
.L2462:
	movq	48(%r15), %rdi
	testq	%rdi, %rdi
	je	.L2463
	movq	(%rdi), %rcx
	call	*8(%rcx)
.L2463:
	movq	40(%r15), %rdi
	testq	%rdi, %rdi
	je	.L2464
	movq	(%rdi), %rcx
	call	*8(%rcx)
.L2464:
	movq	32(%r15), %rdi
	testq	%rdi, %rdi
	je	.L2465
	movq	(%rdi), %rcx
	call	*8(%rcx)
.L2465:
	movq	-16(%r15), %rdi
	cmpq	%rdi, %r15
	je	.L2466
	call	_ZdlPv@PLT
.L2466:
	leaq	72(%r15), %rax
	addq	$88, %r13
	leaq	88(%r15), %rcx
	cmpq	%rax, %rbx
	je	.L2467
	movq	%rcx, %r15
.L2469:
	leaq	16(%r13), %rcx
	movq	%rcx, 0(%r13)
	movq	-16(%r15), %rcx
	cmpq	%rcx, %r15
	jne	.L2459
	movdqu	(%r15), %xmm3
	movups	%xmm3, 16(%r13)
	jmp	.L2460
	.p2align 4,,10
	.p2align 3
.L2467:
	leaq	-88(%rbx), %rax
	movq	-56(%rbp), %rsi
	movabsq	$1048110458733497251, %rdx
	subq	%r14, %rax
	shrq	$3, %rax
	imulq	%rdx, %rax
	movabsq	$2305843009213693951, %rdx
	andq	%rdx, %rax
	addq	$2, %rax
	leaq	(%rax,%rax,4), %rdx
	leaq	(%rax,%rdx,2), %rax
	leaq	(%rsi,%rax,8), %rsi
.L2458:
	cmpq	%r12, %rbx
	je	.L2470
	movq	%rbx, %rax
	movq	%rsi, %rdx
	jmp	.L2473
	.p2align 4,,10
	.p2align 3
.L2471:
	movq	%rcx, (%rdx)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%rdx)
.L2472:
	movq	8(%rax), %rcx
	movdqu	48(%rax), %xmm1
	addq	$88, %rax
	addq	$88, %rdx
	movdqu	-24(%rax), %xmm2
	movq	%rcx, -80(%rdx)
	movq	-56(%rax), %rcx
	movups	%xmm1, -40(%rdx)
	movq	%rcx, -56(%rdx)
	movzbl	-48(%rax), %ecx
	movups	%xmm2, -24(%rdx)
	movb	%cl, -48(%rdx)
	movzbl	-47(%rax), %ecx
	movb	%cl, -47(%rdx)
	movzbl	-46(%rax), %ecx
	movb	%cl, -46(%rdx)
	movzbl	-45(%rax), %ecx
	movb	%cl, -45(%rdx)
	movzbl	-44(%rax), %ecx
	movb	%cl, -44(%rdx)
	movq	-8(%rax), %rcx
	movq	%rcx, -8(%rdx)
	cmpq	%r12, %rax
	je	.L2502
.L2473:
	leaq	16(%rdx), %rcx
	leaq	16(%rax), %rdi
	movq	%rcx, (%rdx)
	movq	(%rax), %rcx
	cmpq	%rdi, %rcx
	jne	.L2471
	movdqu	16(%rax), %xmm4
	movups	%xmm4, 16(%rdx)
	jmp	.L2472
	.p2align 4,,10
	.p2align 3
.L2502:
	movabsq	$1048110458733497251, %rdx
	subq	%rbx, %rax
	subq	$88, %rax
	shrq	$3, %rax
	imulq	%rdx, %rax
	movabsq	$2305843009213693951, %rdx
	andq	%rdx, %rax
	addq	$1, %rax
	leaq	(%rax,%rax,4), %rdx
	leaq	(%rax,%rdx,2), %rax
	leaq	(%rsi,%rax,8), %rsi
.L2470:
	testq	%r14, %r14
	je	.L2474
	movq	%r14, %rdi
	movq	%rsi, -80(%rbp)
	call	_ZdlPv@PLT
	movq	-80(%rbp), %rsi
.L2474:
	movq	-56(%rbp), %xmm0
	movq	-72(%rbp), %rax
	movq	%rsi, %xmm5
	movq	-64(%rbp), %rsi
	punpcklqdq	%xmm5, %xmm0
	movq	%rsi, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2500:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L2455
	movq	$0, -64(%rbp)
	movl	$88, %esi
	movq	$0, -56(%rbp)
	jmp	.L2475
	.p2align 4,,10
	.p2align 3
.L2476:
	movl	$88, %ecx
	jmp	.L2454
	.p2align 4,,10
	.p2align 3
.L2501:
	movdqu	16(%rdx), %xmm6
	movups	%xmm6, 16(%rax)
	jmp	.L2457
.L2455:
	cmpq	%rsi, %rdi
	cmovbe	%rdi, %rsi
	imulq	$88, %rsi, %rcx
	jmp	.L2454
.L2499:
	leaq	.LC15(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE12755:
	.size	_ZNSt6vectorIN12v8_inspector14PropertyMirrorESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_, .-_ZNSt6vectorIN12v8_inspector14PropertyMirrorESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	.section	.text._ZN12v8_inspector12_GLOBAL__N_126PreviewPropertyAccumulator3AddENS_14PropertyMirrorE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_126PreviewPropertyAccumulator3AddENS_14PropertyMirrorE, @function
_ZN12v8_inspector12_GLOBAL__N_126PreviewPropertyAccumulator3AddENS_14PropertyMirrorE:
.LFB7926:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	cmpq	$0, 80(%rsi)
	je	.L2504
.L2507:
	movl	$1, %r13d
.L2503:
	popq	%rbx
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2504:
	.cfi_restore_state
	movq	%rdi, %rbx
	movq	56(%rsi), %rdi
	movq	%rsi, %r12
	testq	%rdi, %rdi
	je	.L2509
	movq	(%rdi), %rax
	call	*48(%rax)
	movq	%rax, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L2508
.L2509:
	cmpq	$0, 48(%r12)
	je	.L2507
.L2508:
	movzbl	43(%r12), %r13d
	testb	%r13b, %r13b
	je	.L2507
	movq	16(%rbx), %r14
	movq	8(%rbx), %rdi
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPN12v8_inspector8String16ESt6vectorIS3_SaIS3_EEEENS0_5__ops16_Iter_equals_valIKS3_EEET_SD_SD_T0_St26random_access_iterator_tag
	cmpq	%rax, %r14
	jne	.L2507
	cmpb	$0, 44(%r12)
	je	.L2511
	movl	32(%rbx), %eax
	testl	%eax, %eax
	jle	.L2512
	subl	$1, %eax
	movl	%eax, 32(%rbx)
	jne	.L2507
.L2512:
	movq	48(%rbx), %rdx
.L2517:
	movl	(%rdx), %eax
	testl	%eax, %eax
	jne	.L2513
	movq	56(%rbx), %rax
	xorl	%r13d, %r13d
	movb	$1, (%rax)
	jmp	.L2503
	.p2align 4,,10
	.p2align 3
.L2511:
	movq	40(%rbx), %rdx
	jmp	.L2517
	.p2align 4,,10
	.p2align 3
.L2513:
	subl	$1, %eax
	movl	%eax, (%rdx)
	movq	64(%rbx), %rdi
	movq	8(%rdi), %rsi
	cmpq	16(%rdi), %rsi
	je	.L2514
	leaq	16(%rsi), %rax
	movq	%rax, (%rsi)
	movq	(%r12), %rdx
	leaq	16(%r12), %rax
	cmpq	%rax, %rdx
	je	.L2522
	movq	%rdx, (%rsi)
	movq	16(%r12), %rdx
	movq	%rdx, 16(%rsi)
.L2516:
	movq	8(%r12), %rdx
	movq	%rax, (%r12)
	xorl	%eax, %eax
	movq	$0, 8(%r12)
	movq	%rdx, 8(%rsi)
	movw	%ax, 16(%r12)
	movq	32(%r12), %rax
	movq	%rax, 32(%rsi)
	movzbl	44(%r12), %eax
	movl	40(%r12), %edx
	movb	%al, 44(%rsi)
	movl	%edx, 40(%rsi)
	movq	48(%r12), %rax
	movq	$0, 48(%r12)
	movq	%rax, 48(%rsi)
	movq	56(%r12), %rax
	movq	$0, 56(%r12)
	movq	%rax, 56(%rsi)
	movq	64(%r12), %rax
	movq	$0, 64(%r12)
	movq	%rax, 64(%rsi)
	movq	72(%r12), %rax
	movq	$0, 72(%r12)
	movq	%rax, 72(%rsi)
	movq	80(%r12), %rax
	movq	$0, 80(%r12)
	movq	%rax, 80(%rsi)
	addq	$88, 8(%rdi)
	jmp	.L2503
	.p2align 4,,10
	.p2align 3
.L2514:
	movq	%r12, %rdx
	call	_ZNSt6vectorIN12v8_inspector14PropertyMirrorESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	jmp	.L2503
.L2522:
	movdqu	16(%r12), %xmm0
	movups	%xmm0, 16(%rsi)
	jmp	.L2516
	.cfi_endproc
.LFE7926:
	.size	_ZN12v8_inspector12_GLOBAL__N_126PreviewPropertyAccumulator3AddENS_14PropertyMirrorE, .-_ZN12v8_inspector12_GLOBAL__N_126PreviewPropertyAccumulator3AddENS_14PropertyMirrorE
	.section	.text._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm,"axG",@progbits,_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm
	.type	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm, @function
_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm:
.LFB13150:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%r8, %r9
	subq	%rdx, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	16(%rdi), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	addq	%rdx, %rsi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	8(%rdi), %rax
	movq	%rsi, -72(%rbp)
	movq	%rax, %r13
	leaq	(%r9,%rax), %r15
	subq	%rsi, %r13
	cmpq	(%rdi), %r14
	je	.L2536
	movq	16(%rdi), %rax
.L2524:
	movabsq	$2305843009213693951, %rdx
	cmpq	%rdx, %r15
	ja	.L2557
	cmpq	%rax, %r15
	jbe	.L2526
	addq	%rax, %rax
	cmpq	%rax, %r15
	jnb	.L2526
	movabsq	$4611686018427387904, %rdi
	movq	%rdx, %r15
	cmpq	%rdx, %rax
	ja	.L2527
	movq	%rax, %r15
	.p2align 4,,10
	.p2align 3
.L2526:
	leaq	2(%r15,%r15), %rdi
.L2527:
	movq	%r8, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_Znwm@PLT
	testq	%r12, %r12
	movq	(%rbx), %r11
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %r8
	movq	%rax, %r10
	je	.L2529
	cmpq	$1, %r12
	je	.L2558
	movq	%r12, %rdx
	addq	%rdx, %rdx
	je	.L2529
	movq	%r11, %rsi
	movq	%rax, %rdi
	movq	%r8, -80(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%r11, -56(%rbp)
	call	memmove@PLT
	movq	-80(%rbp), %r8
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %r11
	movq	%rax, %r10
	.p2align 4,,10
	.p2align 3
.L2529:
	testq	%rcx, %rcx
	je	.L2531
	testq	%r8, %r8
	je	.L2531
	leaq	(%r10,%r12,2), %rdi
	cmpq	$1, %r8
	je	.L2559
	movq	%r8, %rdx
	addq	%rdx, %rdx
	je	.L2531
	movq	%rcx, %rsi
	movq	%r8, -80(%rbp)
	movq	%r10, -64(%rbp)
	movq	%r11, -56(%rbp)
	call	memcpy@PLT
	movq	-80(%rbp), %r8
	movq	-64(%rbp), %r10
	movq	-56(%rbp), %r11
.L2531:
	testq	%r13, %r13
	jne	.L2560
.L2533:
	cmpq	%r11, %r14
	je	.L2535
	movq	%r11, %rdi
	movq	%r10, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r10
.L2535:
	movq	%r15, 16(%rbx)
	movq	%r10, (%rbx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2560:
	.cfi_restore_state
	movq	-72(%rbp), %rax
	addq	%r12, %r8
	leaq	(%r10,%r8,2), %rdi
	leaq	(%r11,%rax,2), %rsi
	cmpq	$1, %r13
	je	.L2561
	addq	%r13, %r13
	je	.L2533
	movq	%r13, %rdx
	movq	%r10, -64(%rbp)
	movq	%r11, -56(%rbp)
	call	memmove@PLT
	movq	-64(%rbp), %r10
	movq	-56(%rbp), %r11
	jmp	.L2533
	.p2align 4,,10
	.p2align 3
.L2536:
	movl	$7, %eax
	jmp	.L2524
	.p2align 4,,10
	.p2align 3
.L2561:
	movzwl	(%rsi), %eax
	movw	%ax, (%rdi)
	jmp	.L2533
	.p2align 4,,10
	.p2align 3
.L2558:
	movzwl	(%r11), %eax
	movw	%ax, (%r10)
	jmp	.L2529
	.p2align 4,,10
	.p2align 3
.L2559:
	movzwl	(%rcx), %eax
	movw	%ax, (%rdi)
	testq	%r13, %r13
	je	.L2533
	jmp	.L2560
.L2557:
	leaq	.LC8(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE13150:
	.size	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm, .-_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm
	.section	.text._ZNK12v8_inspector8String16plERKS0_,"axG",@progbits,_ZNK12v8_inspector8String16plERKS0_,comdat
	.align 2
	.p2align 4
	.weak	_ZNK12v8_inspector8String16plERKS0_
	.type	_ZNK12v8_inspector8String16plERKS0_, @function
_ZNK12v8_inspector8String16plERKS0_:
.LFB3078:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-80(%rbp), %r14
	leaq	-64(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	subq	$48, %rsp
	movq	(%rsi), %r8
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	8(%rsi), %rax
	movq	%r13, -80(%rbp)
	movq	%r8, %rsi
	leaq	(%r8,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-80(%rbp), %rdx
	movq	8(%rbx), %r8
	movl	$7, %eax
	movq	-72(%rbp), %rsi
	movq	(%rbx), %rcx
	cmpq	%r13, %rdx
	cmovne	-64(%rbp), %rax
	leaq	(%r8,%rsi), %rbx
	cmpq	%rax, %rbx
	ja	.L2564
	testq	%r8, %r8
	jne	.L2577
.L2565:
	xorl	%eax, %eax
	movq	%rbx, -72(%rbp)
	movq	%r12, %rdi
	movq	%r14, %rsi
	movw	%ax, (%rdx,%rbx,2)
	call	_ZN12v8_inspector8String16C1EONSt7__cxx1112basic_stringItSt11char_traitsItESaItEEE@PLT
	movq	-80(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L2562
	call	_ZdlPv@PLT
.L2562:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2578
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2577:
	.cfi_restore_state
	leaq	(%rdx,%rsi,2), %rdi
	cmpq	$1, %r8
	je	.L2579
	addq	%r8, %r8
	je	.L2565
	movq	%r8, %rdx
	movq	%rcx, %rsi
	call	memmove@PLT
	movq	-80(%rbp), %rdx
	jmp	.L2565
	.p2align 4,,10
	.p2align 3
.L2564:
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm
	movq	-80(%rbp), %rdx
	jmp	.L2565
	.p2align 4,,10
	.p2align 3
.L2579:
	movzwl	(%rcx), %eax
	movw	%ax, (%rdi)
	movq	-80(%rbp), %rdx
	jmp	.L2565
.L2578:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3078:
	.size	_ZNK12v8_inspector8String16plERKS0_, .-_ZNK12v8_inspector8String16plERKS0_
	.section	.rodata._ZN12v8_inspector12_GLOBAL__N_119descriptionForErrorEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEENS0_9ErrorTypeE.str1.1,"aMS",@progbits,1
.LC36:
	.string	"stack"
.LC37:
	.string	"message"
.LC38:
	.string	": "
	.section	.text._ZN12v8_inspector12_GLOBAL__N_119descriptionForErrorEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEENS0_9ErrorTypeE,"ax",@progbits
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_119descriptionForErrorEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEENS0_9ErrorTypeE, @function
_ZN12v8_inspector12_GLOBAL__N_119descriptionForErrorEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEENS0_9ErrorTypeE:
.LFB7803:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-112(%rbp), %rbx
	subq	$424, %rsp
	movl	%ecx, -408(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%rax, %r13
	leaq	-400(%rbp), %rax
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -416(%rbp)
	call	_ZN2v88TryCatchC1EPNS_7IsolateE@PLT
	movq	%r15, %rdi
	call	_ZN2v86Object18GetConstructorNameEv@PLT
	movq	%r13, %rsi
	movq	%rax, %rdx
	leaq	-352(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -424(%rbp)
	call	_ZN12v8_inspector16toProtocolStringEPN2v87IsolateENS0_5LocalINS0_6StringEEE@PLT
	pxor	%xmm0, %xmm0
	leaq	.LC36(%rip), %rsi
	movq	%rbx, %rdi
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm0, -144(%rbp)
	movaps	%xmm0, -128(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	testq	%rax, %rax
	je	.L2691
	movq	%rax, %rdx
	movq	(%rax), %rax
	movq	-112(%rbp), %rdi
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L2692
.L2584:
	leaq	-96(%rbp), %rax
	movq	%rax, -432(%rbp)
	cmpq	%rax, %rdi
	je	.L2687
.L2587:
	call	_ZdlPv@PLT
.L2687:
	movl	-408(%rbp), %ecx
	movzbl	-160(%rbp), %eax
	testl	%ecx, %ecx
	jne	.L2593
	testb	%al, %al
	jne	.L2604
.L2688:
	leaq	-192(%rbp), %rax
	movq	%rax, -408(%rbp)
	leaq	-208(%rbp), %rax
	movq	%rax, -440(%rbp)
.L2605:
	movq	-440(%rbp), %rbx
	pxor	%xmm0, %xmm0
	leaq	.LC37(%rip), %rsi
	movaps	%xmm0, -112(%rbp)
	movq	%rbx, %rdi
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2612
	movq	(%rax), %rax
	movq	-208(%rbp), %rdi
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	je	.L2693
.L2690:
	cmpq	-408(%rbp), %rdi
	je	.L2611
	call	_ZdlPv@PLT
.L2611:
	cmpb	$0, -112(%rbp)
	jne	.L2615
	cmpb	$0, -160(%rbp)
	leaq	-152(%rbp), %rbx
	leaq	16(%r12), %rax
	movq	%r12, %rdi
	cmove	-424(%rbp), %rbx
	movq	%rax, (%r12)
	movq	(%rbx), %rsi
	movq	8(%rbx), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	32(%rbx), %rax
	movq	%rax, 32(%r12)
.L2617:
	cmpb	$0, -112(%rbp)
	je	.L2595
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2595
	call	_ZdlPv@PLT
	cmpb	$0, -160(%rbp)
	jne	.L2694
	.p2align 4,,10
	.p2align 3
.L2634:
	movq	-352(%rbp), %rdi
	leaq	-336(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2635
	call	_ZdlPv@PLT
.L2635:
	movq	-416(%rbp), %rdi
	call	_ZN2v88TryCatchD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2695
	addq	$424, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2692:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L2584
	leaq	-96(%rbp), %rax
	movq	%rax, -432(%rbp)
	cmpq	%rax, %rdi
	je	.L2642
	movq	%rdx, -440(%rbp)
	call	_ZdlPv@PLT
	movq	-440(%rbp), %rdx
.L2642:
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector16toProtocolStringEPN2v87IsolateENS0_5LocalINS0_6StringEEE@PLT
	cmpb	$0, -160(%rbp)
	jne	.L2696
	leaq	-136(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	-112(%rbp), %rax
	cmpq	-432(%rbp), %rax
	je	.L2697
	movq	%rax, -152(%rbp)
	movq	-96(%rbp), %rax
	movq	%rax, -136(%rbp)
.L2590:
	movq	-104(%rbp), %rax
	movl	-408(%rbp), %esi
	movb	$1, -160(%rbp)
	movq	%rax, -144(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -120(%rbp)
	testl	%esi, %esi
	je	.L2604
.L2592:
	movq	-144(%rbp), %rax
	movq	-344(%rbp), %rcx
	leaq	-192(%rbp), %rsi
	movq	%rsi, -408(%rbp)
	cmpq	%rax, %rcx
	movq	%rsi, -208(%rbp)
	movq	-152(%rbp), %rsi
	cmova	%rax, %rcx
	movq	%rsi, %rax
	leaq	(%rcx,%rcx), %rdx
	addq	%rdx, %rax
	je	.L2596
	testq	%rsi, %rsi
	je	.L2698
.L2596:
	movq	%rdx, %r8
	movq	-408(%rbp), %rdi
	sarq	%r8
	cmpq	$14, %rdx
	jbe	.L2597
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %r8
	ja	.L2699
	leaq	2(%rdx), %rdi
	movq	%r8, -464(%rbp)
	movq	%rcx, -456(%rbp)
	movq	%rsi, -448(%rbp)
	movq	%rdx, -440(%rbp)
	call	_Znwm@PLT
	movq	-464(%rbp), %r8
	movq	-456(%rbp), %rcx
	movq	%rax, -208(%rbp)
	movq	-448(%rbp), %rsi
	movq	%rax, %rdi
	movq	%r8, -192(%rbp)
	movq	-440(%rbp), %rdx
.L2597:
	cmpq	$2, %rdx
	je	.L2700
	testq	%rdx, %rdx
	je	.L2600
	movq	%r8, -448(%rbp)
	movq	%rcx, -440(%rbp)
	call	memmove@PLT
	movq	-208(%rbp), %rdi
	movq	-448(%rbp), %r8
	movq	-440(%rbp), %rcx
.L2600:
	xorl	%edx, %edx
	movq	%r8, -200(%rbp)
	leaq	-208(%rbp), %rax
	movw	%dx, (%rdi,%rcx,2)
	movq	%rax, %rsi
	movq	%rbx, %rdi
	movq	%rax, -440(%rbp)
	call	_ZN12v8_inspector8String16C1EONSt7__cxx1112basic_stringItSt11char_traitsItESaItEEE@PLT
	movq	-208(%rbp), %rdi
	cmpq	-408(%rbp), %rdi
	je	.L2601
	call	_ZdlPv@PLT
.L2601:
	movq	-424(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_
	movq	-112(%rbp), %rdi
	testl	%eax, %eax
	jne	.L2602
	cmpq	-432(%rbp), %rdi
	je	.L2604
	call	_ZdlPv@PLT
.L2604:
	leaq	16(%r12), %rax
	movq	-152(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, (%r12)
	movq	-144(%rbp), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-120(%rbp), %rax
	movq	%rax, 32(%r12)
.L2595:
	cmpb	$0, -160(%rbp)
	je	.L2634
.L2694:
	movq	-152(%rbp), %rdi
	leaq	-136(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2634
	call	_ZdlPv@PLT
	jmp	.L2634
	.p2align 4,,10
	.p2align 3
.L2693:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L2690
	cmpq	-408(%rbp), %rdi
	je	.L2610
	call	_ZdlPv@PLT
.L2610:
	movq	-440(%rbp), %rdi
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	_ZN12v8_inspector29toProtocolStringWithTypeCheckEPN2v87IsolateENS0_5LocalINS0_5ValueEEE@PLT
	movq	-200(%rbp), %rax
	testq	%rax, %rax
	je	.L2612
	cmpb	$0, -112(%rbp)
	je	.L2613
	movq	-440(%rbp), %rsi
	leaq	-104(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-176(%rbp), %rax
	movq	%rax, -72(%rbp)
	.p2align 4,,10
	.p2align 3
.L2612:
	movq	-208(%rbp), %rdi
	jmp	.L2690
	.p2align 4,,10
	.p2align 3
.L2593:
	testb	%al, %al
	je	.L2688
	jmp	.L2592
	.p2align 4,,10
	.p2align 3
.L2691:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	movq	%rax, -432(%rbp)
	cmpq	%rax, %rdi
	jne	.L2587
	jmp	.L2687
	.p2align 4,,10
	.p2align 3
.L2700:
	movzwl	(%rsi), %eax
	movw	%ax, (%rdi)
	movq	-208(%rbp), %rdi
	jmp	.L2600
	.p2align 4,,10
	.p2align 3
.L2696:
	leaq	-152(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	movq	-80(%rbp), %rax
	movq	-112(%rbp), %rdi
	movq	%rax, -120(%rbp)
	cmpq	-432(%rbp), %rdi
	jne	.L2587
	jmp	.L2687
	.p2align 4,,10
	.p2align 3
.L2615:
	leaq	-256(%rbp), %r13
	leaq	.LC38(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-440(%rbp), %rbx
	movq	-424(%rbp), %rsi
	movq	%r13, %rdx
	leaq	-304(%rbp), %r13
	movq	%rbx, %rdi
	call	_ZNK12v8_inspector8String16plERKS0_
	movq	%r13, %rdi
	leaq	-104(%rbp), %rdx
	movq	%rbx, %rsi
	call	_ZNK12v8_inspector8String16plERKS0_
	movq	-208(%rbp), %rdi
	cmpq	-408(%rbp), %rdi
	je	.L2618
	call	_ZdlPv@PLT
.L2618:
	movq	-256(%rbp), %rdi
	leaq	-240(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2619
	call	_ZdlPv@PLT
.L2619:
	cmpb	$0, -160(%rbp)
	jne	.L2620
	leaq	16(%r12), %rax
	leaq	-288(%rbp), %rdx
	movq	%rax, (%r12)
	movq	-304(%rbp), %rax
	cmpq	%rdx, %rax
	je	.L2701
	movq	%rax, (%r12)
	movq	-288(%rbp), %rax
	movq	%rax, 16(%r12)
.L2622:
	movq	-296(%rbp), %rax
	movq	%rax, 8(%r12)
	movq	-272(%rbp), %rax
	movq	%rax, 32(%r12)
	jmp	.L2617
	.p2align 4,,10
	.p2align 3
.L2602:
	cmpq	-432(%rbp), %rdi
	je	.L2605
	call	_ZdlPv@PLT
	jmp	.L2605
	.p2align 4,,10
	.p2align 3
.L2697:
	movdqa	-96(%rbp), %xmm1
	movups	%xmm1, -136(%rbp)
	jmp	.L2590
	.p2align 4,,10
	.p2align 3
.L2620:
	movq	-96(%rbp), %r8
	testq	%r8, %r8
	je	.L2624
	movq	-144(%rbp), %r11
	testq	%r11, %r11
	je	.L2625
	movq	-104(%rbp), %rcx
	movq	-152(%rbp), %r10
	movzwl	(%rcx), %esi
	leaq	(%r10,%r11,2), %r9
	cmpq	%r11, %r8
	ja	.L2625
	movl	$1, %edi
	movq	%r10, %rdx
	subq	%r8, %rdi
	.p2align 4,,10
	.p2align 3
.L2636:
	addq	%rdi, %r11
	je	.L2625
	xorl	%eax, %eax
	jmp	.L2627
	.p2align 4,,10
	.p2align 3
.L2702:
	addq	$1, %rax
	addq	$2, %rdx
	cmpq	%rax, %r11
	je	.L2625
.L2627:
	cmpw	(%rdx), %si
	jne	.L2702
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L2626:
	addq	$1, %rax
	cmpq	%rax, %r8
	je	.L2703
	movzwl	(%rcx,%rax,2), %ebx
	cmpw	%bx, (%rdx,%rax,2)
	je	.L2626
	addq	$2, %rdx
	movq	%r9, %r11
	subq	%rdx, %r11
	sarq	%r11
	cmpq	%r11, %r8
	jbe	.L2636
	.p2align 4,,10
	.p2align 3
.L2625:
	movq	-408(%rbp), %rax
	movq	$0, -200(%rbp)
	movq	$0, -176(%rbp)
	movq	%rax, -208(%rbp)
	xorl	%eax, %eax
	movw	%ax, -192(%rbp)
.L2631:
	movq	-440(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZNK12v8_inspector8String16plERKS0_
	movq	-208(%rbp), %rdi
	cmpq	-408(%rbp), %rdi
	je	.L2632
	call	_ZdlPv@PLT
.L2632:
	movq	-304(%rbp), %rdi
	leaq	-288(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2617
	call	_ZdlPv@PLT
	jmp	.L2617
	.p2align 4,,10
	.p2align 3
.L2701:
	movdqa	-288(%rbp), %xmm2
	movups	%xmm2, 16(%r12)
	jmp	.L2622
	.p2align 4,,10
	.p2align 3
.L2613:
	movq	-208(%rbp), %rsi
	leaq	-88(%rbp), %rdx
	leaq	-104(%rbp), %rdi
	movq	%rdx, -104(%rbp)
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-176(%rbp), %rax
	movb	$1, -112(%rbp)
	movq	%rax, -72(%rbp)
	jmp	.L2612
	.p2align 4,,10
	.p2align 3
.L2703:
	subq	%r10, %rdx
	sarq	%rdx
	cmpq	$-1, %rdx
	je	.L2625
	addq	%rdx, %r8
.L2624:
	movq	-440(%rbp), %rdi
	leaq	-152(%rbp), %rsi
	movl	$4294967295, %ecx
	movq	%r8, %rdx
	call	_ZNK12v8_inspector8String169substringEmm
	jmp	.L2631
.L2695:
	call	__stack_chk_fail@PLT
.L2699:
	leaq	.LC8(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L2698:
	leaq	.LC7(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.cfi_endproc
.LFE7803:
	.size	_ZN12v8_inspector12_GLOBAL__N_119descriptionForErrorEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEENS0_9ErrorTypeE, .-_ZN12v8_inspector12_GLOBAL__N_119descriptionForErrorEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEENS0_9ErrorTypeE
	.section	.rodata._ZN12v8_inspector12clientMirrorEN2v85LocalINS0_7ContextEEENS1_INS0_5ValueEEERKNS_8String16E.str1.1,"aMS",@progbits,1
.LC39:
	.string	"node"
.LC40:
	.string	"error"
.LC41:
	.string	"array"
	.section	.text._ZN12v8_inspector12clientMirrorEN2v85LocalINS0_7ContextEEENS1_INS0_5ValueEEERKNS_8String16E,"ax",@progbits
	.p2align 4
	.globl	_ZN12v8_inspector12clientMirrorEN2v85LocalINS0_7ContextEEENS1_INS0_5ValueEEERKNS_8String16E
	.type	_ZN12v8_inspector12clientMirrorEN2v85LocalINS0_7ContextEEENS1_INS0_5ValueEEERKNS_8String16E, @function
_ZN12v8_inspector12clientMirrorEN2v85LocalINS0_7ContextEEENS1_INS0_5ValueEEERKNS_8String16E:
.LFB8058:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-96(%rbp), %r13
	pushq	%r12
	movq	%r13, %rdi
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-80(%rbp), %rbx
	subq	$152, %rsp
	movq	%rsi, -168(%rbp)
	leaq	.LC39(%rip), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L2705
	movl	%eax, -176(%rbp)
	call	_ZdlPv@PLT
	movl	-176(%rbp), %eax
.L2705:
	testl	%eax, %eax
	jne	.L2706
	movq	-168(%rbp), %rsi
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZN12v8_inspector18descriptionForNodeEN2v85LocalINS0_7ContextEEENS1_INS0_5ValueEEE
	movl	$104, %edi
	call	_Znwm@PLT
	movq	-96(%rbp), %rsi
	movq	%rax, %r13
	leaq	16+_ZTVN12v8_inspector12_GLOBAL__N_112ObjectMirrorE(%rip), %rax
	movq	%rax, 0(%r13)
	leaq	32(%r13), %rax
	leaq	16(%r13), %rdi
	movq	%rax, 16(%r13)
	movq	-88(%rbp), %rax
	movq	%r14, 8(%r13)
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-64(%rbp), %rax
	movq	(%r12), %rsi
	movb	$1, 56(%r13)
	leaq	64(%r13), %rdi
	movq	%rax, 48(%r13)
	leaq	80(%r13), %rax
	movq	%rax, 64(%r13)
	movq	8(%r12), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	32(%r12), %rax
	movq	%r13, (%r15)
	movq	%rax, 96(%r13)
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L2704
.L2737:
	call	_ZdlPv@PLT
.L2704:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2738
	addq	$152, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2706:
	.cfi_restore_state
	leaq	.LC40(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L2709
	movl	%eax, -176(%rbp)
	call	_ZdlPv@PLT
	movl	-176(%rbp), %eax
.L2709:
	testl	%eax, %eax
	jne	.L2710
	movq	-168(%rbp), %rsi
	movl	$1, %ecx
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZN12v8_inspector12_GLOBAL__N_119descriptionForErrorEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEENS0_9ErrorTypeE
	movq	_ZN12v8_inspector8protocol7Runtime12RemoteObject11SubtypeEnum5ErrorE(%rip), %rdx
	movq	%r13, %rcx
	movq	%r14, %rsi
	leaq	-144(%rbp), %rdi
	call	_ZN2v84base11make_uniqueIN12v8_inspector12_GLOBAL__N_112ObjectMirrorEJRNS_5LocalINS_5ValueEEERPKcNS2_8String16EEEESt10unique_ptrIT_St14default_deleteISE_EEDpOT0_.isra.0
	movq	-144(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, (%r15)
	cmpq	%rbx, %rdi
	jne	.L2737
	jmp	.L2704
	.p2align 4,,10
	.p2align 3
.L2710:
	leaq	.LC41(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_
	testl	%eax, %eax
	je	.L2739
.L2712:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L2715
	call	_ZdlPv@PLT
.L2715:
	movq	-168(%rbp), %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v86Object18GetConstructorNameEv@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN12v8_inspector16toProtocolStringEPN2v87IsolateENS0_5LocalINS0_6StringEEE@PLT
	movl	$104, %edi
	call	_Znwm@PLT
	movq	-96(%rbp), %rsi
	movq	%rax, %r12
	leaq	16+_ZTVN12v8_inspector12_GLOBAL__N_112ObjectMirrorE(%rip), %rax
	movq	%rax, (%r12)
	leaq	32(%r12), %rax
	leaq	16(%r12), %rdi
	movq	%rax, 16(%r12)
	movq	-88(%rbp), %rax
	movq	%r14, 8(%r12)
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-64(%rbp), %rax
	movb	$0, 56(%r12)
	movq	$0, 72(%r12)
	movq	%rax, 48(%r12)
	leaq	80(%r12), %rax
	movq	%rax, 64(%r12)
	xorl	%eax, %eax
	movw	%ax, 80(%r12)
	movq	$0, 96(%r12)
	movq	-96(%rbp), %rdi
	movq	%r12, (%r15)
	cmpq	%rbx, %rdi
	jne	.L2737
	jmp	.L2704
	.p2align 4,,10
	.p2align 3
.L2739:
	movq	%r14, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L2712
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L2714
	call	_ZdlPv@PLT
.L2714:
	movq	-168(%rbp), %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%rax, %r12
	leaq	-144(%rbp), %rax
	movq	%rax, %rdi
	movq	%r12, %rsi
	movq	%rax, -176(%rbp)
	call	_ZN2v88TryCatchC1EPNS_7IsolateE@PLT
	leaq	.LC3(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	-168(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L2740
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L2723
	movq	%rax, -184(%rbp)
	call	_ZdlPv@PLT
	movq	-184(%rbp), %r8
.L2723:
	movq	%r8, %rdi
	movq	%r8, -184(%rbp)
	call	_ZNK2v85Value7IsInt32Ev@PLT
	movq	-184(%rbp), %r8
	testb	%al, %al
	jne	.L2741
.L2718:
	movq	-176(%rbp), %rdi
	call	_ZN2v88TryCatchD1Ev@PLT
	jmp	.L2715
	.p2align 4,,10
	.p2align 3
.L2740:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L2718
	call	_ZdlPv@PLT
	jmp	.L2718
	.p2align 4,,10
	.p2align 3
.L2741:
	movq	%r8, %rdi
	call	_ZNK2v85Int325ValueEv@PLT
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movslq	%eax, %rcx
	call	_ZN12v8_inspector12_GLOBAL__N_124descriptionForCollectionEPN2v87IsolateENS1_5LocalINS1_6ObjectEEEm
	movq	_ZN12v8_inspector8protocol7Runtime12RemoteObject11SubtypeEnum5ArrayE(%rip), %rdx
	movq	%r13, %rcx
	movq	%r14, %rsi
	leaq	-152(%rbp), %rdi
	call	_ZN2v84base11make_uniqueIN12v8_inspector12_GLOBAL__N_112ObjectMirrorEJRNS_5LocalINS_5ValueEEERPKcNS2_8String16EEEESt10unique_ptrIT_St14default_deleteISE_EEDpOT0_.isra.0
	movq	-152(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, (%r15)
	cmpq	%rbx, %rdi
	je	.L2719
	call	_ZdlPv@PLT
.L2719:
	movq	-176(%rbp), %rdi
	call	_ZN2v88TryCatchD1Ev@PLT
	jmp	.L2704
.L2738:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8058:
	.size	_ZN12v8_inspector12clientMirrorEN2v85LocalINS0_7ContextEEENS1_INS0_5ValueEEERKNS_8String16E, .-_ZN12v8_inspector12clientMirrorEN2v85LocalINS0_7ContextEEENS1_INS0_5ValueEEERKNS_8String16E
	.section	.rodata._ZN12v8_inspector12_GLOBAL__N_120descriptionForBigIntEN2v85LocalINS1_7ContextEEENS2_INS1_6BigIntEEE.str1.1,"aMS",@progbits,1
.LC42:
	.string	"n"
	.section	.text._ZN12v8_inspector12_GLOBAL__N_120descriptionForBigIntEN2v85LocalINS1_7ContextEEENS2_INS1_6BigIntEEE,"ax",@progbits
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_120descriptionForBigIntEN2v85LocalINS1_7ContextEEENS2_INS1_6BigIntEEE, @function
_ZN12v8_inspector12_GLOBAL__N_120descriptionForBigIntEN2v85LocalINS1_7ContextEEENS2_INS1_6BigIntEEE:
.LFB7797:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-224(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$184, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	movq	%rax, %r14
	call	_ZN2v88TryCatchC1EPNS_7IsolateE@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZNK2v85Value8ToStringENS_5LocalINS_7ContextEEE@PLT
	testq	%rax, %rax
	je	.L2766
	movq	%rax, %r13
	leaq	-96(%rbp), %rdi
	leaq	.LC42(%rip), %rsi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdx
	movq	%r14, %rsi
	leaq	-144(%rbp), %rdi
	call	_ZN12v8_inspector16toProtocolStringEPN2v87IsolateENS0_5LocalINS0_6StringEEE@PLT
	movq	-144(%rbp), %rsi
	movq	-136(%rbp), %rax
	leaq	-176(%rbp), %r14
	leaq	-160(%rbp), %rbx
	movq	%r14, %rdi
	leaq	(%rsi,%rax,2), %rdx
	movq	%rbx, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-176(%rbp), %rdx
	movq	-88(%rbp), %r8
	movl	$7, %eax
	movq	-168(%rbp), %rsi
	movq	-96(%rbp), %rcx
	cmpq	%rbx, %rdx
	cmovne	-160(%rbp), %rax
	leaq	(%r8,%rsi), %r13
	cmpq	%rax, %r13
	ja	.L2767
	testq	%r8, %r8
	jne	.L2768
.L2746:
	xorl	%eax, %eax
	movq	%r13, -168(%rbp)
	movq	%r12, %rdi
	movq	%r14, %rsi
	movw	%ax, (%rdx,%r13,2)
	call	_ZN12v8_inspector8String16C1EONSt7__cxx1112basic_stringItSt11char_traitsItESaItEEE@PLT
	movq	-176(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L2748
	call	_ZdlPv@PLT
.L2748:
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2749
	call	_ZdlPv@PLT
.L2749:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2751
	call	_ZdlPv@PLT
.L2751:
	movq	%r15, %rdi
	call	_ZN2v88TryCatchD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2769
	addq	$184, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2766:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	leaq	16(%r12), %rax
	movq	$0, 32(%r12)
	movq	%rax, (%r12)
	movq	$0, 8(%r12)
	movups	%xmm0, 16(%r12)
	jmp	.L2751
	.p2align 4,,10
	.p2align 3
.L2768:
	leaq	(%rdx,%rsi,2), %rdi
	cmpq	$1, %r8
	je	.L2770
	addq	%r8, %r8
	je	.L2746
	movq	%r8, %rdx
	movq	%rcx, %rsi
	call	memmove@PLT
	movq	-176(%rbp), %rdx
	jmp	.L2746
	.p2align 4,,10
	.p2align 3
.L2767:
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm
	movq	-176(%rbp), %rdx
	jmp	.L2746
	.p2align 4,,10
	.p2align 3
.L2770:
	movzwl	(%rcx), %eax
	movw	%ax, (%rdi)
	movq	-176(%rbp), %rdx
	jmp	.L2746
.L2769:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7797:
	.size	_ZN12v8_inspector12_GLOBAL__N_120descriptionForBigIntEN2v85LocalINS1_7ContextEEENS2_INS1_6BigIntEEE, .-_ZN12v8_inspector12_GLOBAL__N_120descriptionForBigIntEN2v85LocalINS1_7ContextEEENS2_INS1_6BigIntEEE
	.section	.text._ZNK12v8_inspector12_GLOBAL__N_112BigIntMirror20buildPropertyPreviewEN2v85LocalINS2_7ContextEEERKNS_8String16EPSt10unique_ptrINS_8protocol7Runtime15PropertyPreviewESt14default_deleteISC_EE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK12v8_inspector12_GLOBAL__N_112BigIntMirror20buildPropertyPreviewEN2v85LocalINS2_7ContextEEERKNS_8String16EPSt10unique_ptrINS_8protocol7Runtime15PropertyPreviewESt14default_deleteISC_EE, @function
_ZNK12v8_inspector12_GLOBAL__N_112BigIntMirror20buildPropertyPreviewEN2v85LocalINS2_7ContextEEERKNS_8String16EPSt10unique_ptrINS_8protocol7Runtime15PropertyPreviewESt14default_deleteISC_EE:
.LFB7859:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	leaq	-248(%rbp), %rdi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$216, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol7Runtime15PropertyPreview22PropertyPreviewBuilderILi0EEC1Ev
	movq	-248(%rbp), %r15
	movq	%r12, %rsi
	leaq	8(%r15), %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	32(%r12), %rax
	movq	_ZN12v8_inspector8protocol7Runtime12RemoteObject8TypeEnum6BigintE(%rip), %rsi
	movq	%rax, 40(%r15)
	leaq	-240(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-248(%rbp), %r12
	movq	%r15, %rsi
	leaq	48(%r12), %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-208(%rbp), %rax
	movq	%r13, %rsi
	leaq	-80(%rbp), %r13
	movq	%rax, 80(%r12)
	leaq	-192(%rbp), %r12
	movq	8(%r14), %rdx
	leaq	-96(%rbp), %r14
	movq	%r12, %rdi
	call	_ZN12v8_inspector12_GLOBAL__N_120descriptionForBigIntEN2v85LocalINS1_7ContextEEENS2_INS1_6BigIntEEE
	movq	%r12, %rsi
	leaq	-144(%rbp), %rdi
	xorl	%edx, %edx
	call	_ZN12v8_inspector12_GLOBAL__N_116abbreviateStringERKNS_8String16ENS0_14AbbreviateModeE
	movq	-144(%rbp), %rsi
	movq	%r14, %rdi
	movq	%r13, -96(%rbp)
	movq	-136(%rbp), %rax
	movq	-248(%rbp), %r12
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-112(%rbp), %rax
	leaq	96(%r12), %rdi
	movq	%r14, %rsi
	movq	%rax, -64(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movb	$1, 88(%r12)
	movq	%rax, 128(%r12)
	cmpq	%r13, %rdi
	je	.L2772
	call	_ZdlPv@PLT
.L2772:
	movq	-248(%rbp), %rax
	movq	(%rbx), %rdi
	movq	$0, -248(%rbp)
	movq	%rax, (%rbx)
	testq	%rdi, %rdi
	je	.L2773
	movq	(%rdi), %rax
	call	*24(%rax)
.L2773:
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2774
	call	_ZdlPv@PLT
.L2774:
	movq	-192(%rbp), %rdi
	leaq	-176(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2775
	call	_ZdlPv@PLT
.L2775:
	movq	-240(%rbp), %rdi
	leaq	-224(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2776
	call	_ZdlPv@PLT
.L2776:
	movq	-248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2771
	movq	(%rdi), %rax
	call	*24(%rax)
.L2771:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2786
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2786:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7859:
	.size	_ZNK12v8_inspector12_GLOBAL__N_112BigIntMirror20buildPropertyPreviewEN2v85LocalINS2_7ContextEEERKNS_8String16EPSt10unique_ptrINS_8protocol7Runtime15PropertyPreviewESt14default_deleteISC_EE, .-_ZNK12v8_inspector12_GLOBAL__N_112BigIntMirror20buildPropertyPreviewEN2v85LocalINS2_7ContextEEERKNS_8String16EPSt10unique_ptrINS_8protocol7Runtime15PropertyPreviewESt14default_deleteISC_EE
	.section	.text._ZNK12v8_inspector12_GLOBAL__N_112BigIntMirror17buildRemoteObjectEN2v85LocalINS2_7ContextEEENS_8WrapModeEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISA_EE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK12v8_inspector12_GLOBAL__N_112BigIntMirror17buildRemoteObjectEN2v85LocalINS2_7ContextEEENS_8WrapModeEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISA_EE, @function
_ZNK12v8_inspector12_GLOBAL__N_112BigIntMirror17buildRemoteObjectEN2v85LocalINS2_7ContextEEENS_8WrapModeEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISA_EE:
.LFB7858:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r9
	movq	%rdx, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	leaq	-192(%rbp), %rdi
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-144(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	8(%r9), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector12_GLOBAL__N_120descriptionForBigIntEN2v85LocalINS1_7ContextEEENS2_INS1_6BigIntEEE
	movl	$320, %edi
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectC1Ev
	movq	_ZN12v8_inspector8protocol7Runtime12RemoteObject8TypeEnum6BigintE(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rsi
	leaq	16(%rbx), %rdi
	leaq	-80(%rbp), %r13
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-112(%rbp), %rax
	movq	%r14, %rdi
	movq	%r13, -96(%rbp)
	movq	-192(%rbp), %rsi
	movq	%rax, 48(%rbx)
	movq	-184(%rbp), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-160(%rbp), %rax
	leaq	168(%rbx), %rdi
	movq	%r14, %rsi
	movq	%rax, -64(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movb	$1, 160(%rbx)
	movq	%rax, 200(%rbx)
	cmpq	%r13, %rdi
	je	.L2788
	call	_ZdlPv@PLT
.L2788:
	movq	-192(%rbp), %rsi
	movq	-184(%rbp), %rax
	movq	%r14, %rdi
	movq	%r13, -96(%rbp)
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-160(%rbp), %rax
	leaq	216(%rbx), %rdi
	movq	%r14, %rsi
	movq	%rax, -64(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movb	$1, 208(%rbx)
	movq	%rax, 248(%rbx)
	cmpq	%r13, %rdi
	je	.L2789
	call	_ZdlPv@PLT
.L2789:
	movq	(%r12), %r13
	movq	%rbx, (%r12)
	testq	%r13, %r13
	je	.L2790
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2791
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2790:
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2792
	call	_ZdlPv@PLT
.L2792:
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	movq	-192(%rbp), %rdi
	leaq	-176(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2787
	call	_ZdlPv@PLT
.L2787:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2799
	addq	$152, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2791:
	.cfi_restore_state
	call	*%rax
	jmp	.L2790
.L2799:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7858:
	.size	_ZNK12v8_inspector12_GLOBAL__N_112BigIntMirror17buildRemoteObjectEN2v85LocalINS2_7ContextEEENS_8WrapModeEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISA_EE, .-_ZNK12v8_inspector12_GLOBAL__N_112BigIntMirror17buildRemoteObjectEN2v85LocalINS2_7ContextEEENS_8WrapModeEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISA_EE
	.section	.text._ZNK12v8_inspector12_GLOBAL__N_112BigIntMirror17buildEntryPreviewEN2v85LocalINS2_7ContextEEEPiS6_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteISA_EE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK12v8_inspector12_GLOBAL__N_112BigIntMirror17buildEntryPreviewEN2v85LocalINS2_7ContextEEEPiS6_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteISA_EE, @function
_ZNK12v8_inspector12_GLOBAL__N_112BigIntMirror17buildEntryPreviewEN2v85LocalINS2_7ContextEEEPiS6_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteISA_EE:
.LFB7860:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-192(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	$168, %edi
	subq	$216, %rsp
	movq	%r8, -216(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	_ZN12v8_inspector8protocol7Runtime12RemoteObject8TypeEnum6BigintE(%rip), %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r15)
	leaq	24(%r15), %rax
	leaq	8(%r15), %r8
	movq	%rax, 8(%r15)
	xorl	%eax, %eax
	leaq	104(%r15), %r14
	movw	%ax, 24(%r15)
	leaq	72(%r15), %rax
	movq	%rax, 56(%r15)
	leaq	120(%r15), %rax
	movq	%rax, 104(%r15)
	movq	$0, 16(%r15)
	movq	$0, 40(%r15)
	movb	$0, 48(%r15)
	movq	$0, 88(%r15)
	movq	$0, 64(%r15)
	movb	$0, 96(%r15)
	movq	$0, 136(%r15)
	movq	$0, 112(%r15)
	movb	$0, 144(%r15)
	movups	%xmm0, 72(%r15)
	movups	%xmm0, 120(%r15)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 152(%r15)
	movq	%r8, -200(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-200(%rbp), %r8
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-160(%rbp), %rax
	movq	8(%rbx), %rdx
	movq	%r12, %rsi
	leaq	-144(%rbp), %rdi
	leaq	-96(%rbp), %r12
	movq	%rax, 40(%r15)
	leaq	-80(%rbp), %rbx
	call	_ZN12v8_inspector12_GLOBAL__N_120descriptionForBigIntEN2v85LocalINS1_7ContextEEENS2_INS1_6BigIntEEE
	movq	-144(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	movq	-136(%rbp), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-112(%rbp), %rax
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rax, -64(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movb	$1, 96(%r15)
	movq	%rax, 136(%r15)
	cmpq	%rbx, %rdi
	je	.L2801
	call	_ZdlPv@PLT
.L2801:
	movb	$0, 144(%r15)
	movl	$24, %edi
	call	_Znwm@PLT
	movq	152(%r15), %rcx
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rax)
	movq	%rax, 152(%r15)
	movups	%xmm0, (%rax)
	movq	%rcx, %rax
	movq	%rcx, -208(%rbp)
	testq	%rcx, %rcx
	je	.L2802
	movq	8(%rcx), %rcx
	movq	(%rax), %rbx
	movq	%rcx, -240(%rbp)
	cmpq	%rbx, %rcx
	je	.L2803
	movq	%r15, -256(%rbp)
	movq	%rbx, -200(%rbp)
	.p2align 4,,10
	.p2align 3
.L2826:
	movq	-200(%rbp), %rax
	movq	(%rax), %r12
	testq	%r12, %r12
	je	.L2804
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2805
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2806
	call	_ZdlPv@PLT
.L2806:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2807
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rbx
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L2808
	movq	160(%r13), %r15
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r15, %r15
	je	.L2809
	movq	8(%r15), %rax
	movq	(%r15), %r14
	movq	%rax, -232(%rbp)
	cmpq	%r14, %rax
	je	.L2810
	movq	%r12, -248(%rbp)
	jmp	.L2817
	.p2align 4,,10
	.p2align 3
.L2957:
	movq	16(%r12), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L2813
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L2814
	movq	%rdi, -224(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-224(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L2813:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2815
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L2816
	movq	%rdi, -224(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-224(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L2815:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2811:
	addq	$8, %r14
	cmpq	%r14, -232(%rbp)
	je	.L2956
.L2817:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L2811
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2957
	movq	%r12, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -232(%rbp)
	jne	.L2817
	.p2align 4,,10
	.p2align 3
.L2956:
	movq	-248(%rbp), %r12
	movq	(%r15), %r14
.L2810:
	testq	%r14, %r14
	je	.L2818
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2818:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L2809:
	movq	152(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2819
	call	_ZNKSt14default_deleteISt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime15PropertyPreviewES_IS5_EESaIS7_EEEclEPS9_.isra.0.part.0
.L2819:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2820
	call	_ZdlPv@PLT
.L2820:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2821
	call	_ZdlPv@PLT
.L2821:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2822
	call	_ZdlPv@PLT
.L2822:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2807:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2823
	call	_ZdlPv@PLT
.L2823:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2824
	call	_ZdlPv@PLT
.L2824:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2825
	call	_ZdlPv@PLT
.L2825:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2804:
	addq	$8, -200(%rbp)
	movq	-200(%rbp), %rax
	cmpq	%rax, -240(%rbp)
	jne	.L2826
	movq	-208(%rbp), %rax
	movq	-256(%rbp), %r15
	movq	(%rax), %rbx
.L2803:
	testq	%rbx, %rbx
	je	.L2827
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L2827:
	movq	-208(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2802:
	movq	-216(%rbp), %rax
	movq	(%rax), %rbx
	movq	%r15, (%rax)
	testq	%rbx, %rbx
	je	.L2828
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %r14
	movq	%r14, -200(%rbp)
	movq	24(%rax), %rax
	cmpq	%r14, %rax
	jne	.L2829
	movq	160(%rbx), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rbx)
	testq	%r13, %r13
	je	.L2830
	movq	8(%r13), %r15
	movq	0(%r13), %r12
	cmpq	%r12, %r15
	je	.L2831
	movq	%r13, -216(%rbp)
	jmp	.L2838
	.p2align 4,,10
	.p2align 3
.L2959:
	movq	16(%r13), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%rdi, %rdi
	je	.L2834
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%r14, %rax
	jne	.L2835
	movq	%rdi, -208(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-208(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L2834:
	movq	8(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2836
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%r14, %rax
	jne	.L2837
	movq	%rdi, -208(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-208(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L2836:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2832:
	addq	$8, %r12
	cmpq	%r12, %r15
	je	.L2958
.L2838:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L2832
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2959
	addq	$8, %r12
	movq	%r13, %rdi
	call	*%rax
	cmpq	%r12, %r15
	jne	.L2838
	.p2align 4,,10
	.p2align 3
.L2958:
	movq	-216(%rbp), %r13
	movq	0(%r13), %r12
.L2831:
	testq	%r12, %r12
	je	.L2839
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2839:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2830:
	movq	152(%rbx), %rax
	movq	%rax, -232(%rbp)
	testq	%rax, %rax
	je	.L2840
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -224(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rcx
	je	.L2841
	movq	%rbx, -256(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L2864:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L2842
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2843
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2844
	call	_ZdlPv@PLT
.L2844:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2845
	movq	0(%r13), %rax
	movq	-200(%rbp), %r15
	movq	24(%rax), %rax
	cmpq	%r15, %rax
	jne	.L2846
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -208(%rbp)
	testq	%rax, %rax
	je	.L2847
	movq	8(%rax), %rdx
	movq	(%rax), %r14
	movq	%rdx, -216(%rbp)
	cmpq	%r14, %rdx
	je	.L2848
	movq	%r12, -240(%rbp)
	movq	%r13, -248(%rbp)
	jmp	.L2855
	.p2align 4,,10
	.p2align 3
.L2961:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L2851
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%r15, %rax
	jne	.L2852
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2851:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L2853
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%r15, %rax
	jne	.L2854
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2853:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2849:
	addq	$8, %r14
	cmpq	%r14, -216(%rbp)
	je	.L2960
.L2855:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L2849
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2961
	movq	%r12, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -216(%rbp)
	jne	.L2855
	.p2align 4,,10
	.p2align 3
.L2960:
	movq	-208(%rbp), %rax
	movq	-240(%rbp), %r12
	movq	-248(%rbp), %r13
	movq	(%rax), %r14
.L2848:
	testq	%r14, %r14
	je	.L2856
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2856:
	movq	-208(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2847:
	movq	152(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2857
	call	_ZNKSt14default_deleteISt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime15PropertyPreviewES_IS5_EESaIS7_EEEclEPS9_.isra.0.part.0
.L2857:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2858
	call	_ZdlPv@PLT
.L2858:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2859
	call	_ZdlPv@PLT
.L2859:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2860
	call	_ZdlPv@PLT
.L2860:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2845:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2861
	call	_ZdlPv@PLT
.L2861:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2862
	call	_ZdlPv@PLT
.L2862:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2863
	call	_ZdlPv@PLT
.L2863:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2842:
	addq	$8, %rbx
	cmpq	%rbx, -224(%rbp)
	jne	.L2864
	movq	-232(%rbp), %rax
	movq	-256(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L2841:
	testq	%rdi, %rdi
	je	.L2865
	call	_ZdlPv@PLT
.L2865:
	movq	-232(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2840:
	movq	104(%rbx), %rdi
	leaq	120(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2866
	call	_ZdlPv@PLT
.L2866:
	movq	56(%rbx), %rdi
	leaq	72(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2867
	call	_ZdlPv@PLT
.L2867:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2868
	call	_ZdlPv@PLT
.L2868:
	movl	$168, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L2828:
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2869
	call	_ZdlPv@PLT
.L2869:
	movq	-192(%rbp), %rdi
	leaq	-176(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2800
	call	_ZdlPv@PLT
.L2800:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2962
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2805:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2804
	.p2align 4,,10
	.p2align 3
.L2816:
	call	*%rax
	jmp	.L2815
	.p2align 4,,10
	.p2align 3
.L2814:
	call	*%rax
	jmp	.L2813
	.p2align 4,,10
	.p2align 3
.L2808:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2807
	.p2align 4,,10
	.p2align 3
.L2843:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2842
	.p2align 4,,10
	.p2align 3
.L2852:
	call	*%rax
	jmp	.L2851
	.p2align 4,,10
	.p2align 3
.L2854:
	call	*%rax
	jmp	.L2853
	.p2align 4,,10
	.p2align 3
.L2837:
	call	*%rax
	jmp	.L2836
	.p2align 4,,10
	.p2align 3
.L2835:
	call	*%rax
	jmp	.L2834
	.p2align 4,,10
	.p2align 3
.L2846:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2845
	.p2align 4,,10
	.p2align 3
.L2829:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L2828
.L2962:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7860:
	.size	_ZNK12v8_inspector12_GLOBAL__N_112BigIntMirror17buildEntryPreviewEN2v85LocalINS2_7ContextEEEPiS6_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteISA_EE, .-_ZNK12v8_inspector12_GLOBAL__N_112BigIntMirror17buildEntryPreviewEN2v85LocalINS2_7ContextEEEPiS6_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteISA_EE
	.section	.rodata._ZN12v8_inspector11ValueMirror6createEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE.str1.1,"aMS",@progbits,1
.LC43:
	.string	"Proxy"
.LC44:
	.string	"Scopes["
.LC45:
	.string	"internal#scopeList"
.LC46:
	.string	"key"
.LC47:
	.string	"value"
.LC48:
	.string	"}"
.LC49:
	.string	" => "
.LC50:
	.string	"{"
.LC51:
	.string	"internal#entry"
.LC52:
	.string	"description"
.LC53:
	.string	"internal#scope"
	.section	.text._ZN12v8_inspector11ValueMirror6createEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector11ValueMirror6createEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE
	.type	_ZN12v8_inspector11ValueMirror6createEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE, @function
_ZN12v8_inspector11ValueMirror6createEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE:
.LFB8061:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$536, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L2964
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	je	.L3228
.L2964:
	movq	%r15, %rdi
	call	_ZNK2v85Value9IsBooleanEv@PLT
	testb	%al, %al
	jne	.L3229
	movq	%r15, %rdi
	call	_ZNK2v85Value8IsNumberEv@PLT
	testb	%al, %al
	je	.L2967
	movl	$16, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN12v8_inspector12_GLOBAL__N_112NumberMirrorE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	%r15, 8(%rax)
	movq	%rax, (%r12)
.L2963:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3230
	addq	$536, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3229:
	.cfi_restore_state
	movq	_ZN12v8_inspector8protocol7Runtime12RemoteObject8TypeEnum7BooleanE(%rip), %rdx
	leaq	-480(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v84base11make_uniqueIN12v8_inspector12_GLOBAL__N_120PrimitiveValueMirrorEJRNS_5LocalINS_5ValueEEERPKcEEESt10unique_ptrIT_St14default_deleteISD_EEDpOT0_.isra.0
	movq	-480(%rbp), %rax
	movq	%rax, (%r12)
	jmp	.L2963
	.p2align 4,,10
	.p2align 3
.L3228:
	cmpl	$3, 43(%rax)
	jne	.L2964
	movq	_ZN12v8_inspector8protocol7Runtime12RemoteObject8TypeEnum6ObjectE(%rip), %rdx
	leaq	-480(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v84base11make_uniqueIN12v8_inspector12_GLOBAL__N_120PrimitiveValueMirrorEJRNS_5LocalINS_5ValueEEERPKcEEESt10unique_ptrIT_St14default_deleteISD_EEDpOT0_.isra.0
	movq	-480(%rbp), %rax
	movq	%rax, (%r12)
	jmp	.L2963
	.p2align 4,,10
	.p2align 3
.L2967:
	movq	%r13, %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%rax, %rbx
	movq	(%r15), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	je	.L3231
.L2968:
	movq	%r15, %rdi
	call	_ZNK2v85Value8IsBigIntEv@PLT
	testb	%al, %al
	je	.L2969
	movl	$16, %edi
	leaq	16+_ZTVN12v8_inspector12_GLOBAL__N_112BigIntMirrorE(%rip), %rbx
	call	_Znwm@PLT
	movq	%rbx, (%rax)
	movq	%r15, 8(%rax)
	movq	%rax, (%r12)
	jmp	.L2963
	.p2align 4,,10
	.p2align 3
.L3231:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L2968
	movq	_ZN12v8_inspector8protocol7Runtime12RemoteObject8TypeEnum6StringE(%rip), %rdx
	leaq	-480(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v84base11make_uniqueIN12v8_inspector12_GLOBAL__N_120PrimitiveValueMirrorEJRNS_5LocalINS_5ValueEEERPKcEEESt10unique_ptrIT_St14default_deleteISD_EEDpOT0_.isra.0
	movq	-480(%rbp), %rax
	movq	%rax, (%r12)
	jmp	.L2963
	.p2align 4,,10
	.p2align 3
.L2969:
	movq	%r15, %rdi
	call	_ZNK2v85Value8IsSymbolEv@PLT
	testb	%al, %al
	jne	.L3232
	movq	(%r15), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L2971
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	jne	.L2971
	cmpl	$5, 43(%rax)
	jne	.L2971
.L2974:
	movq	%r13, %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%rax, %rdi
	call	_ZN2v85debug12GetInspectorEPNS_7IsolateE@PLT
	leaq	_ZN12v8_inspector17V8InspectorClient12valueSubtypeEN2v85LocalINS1_5ValueEEE(%rip), %rdx
	movq	16(%rax), %rsi
	movq	(%rsi), %rax
	movq	72(%rax), %rax
	cmpq	%rdx, %rax
	je	.L3195
	leaq	-496(%rbp), %rdi
	movq	%r15, %rdx
	call	*%rax
	movq	-496(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2976
	movq	(%rdi), %rax
	leaq	-96(%rbp), %r14
	call	*16(%rax)
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN12v8_inspector10toString16ERKNS_10StringViewE@PLT
	movq	%r12, %rdi
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	_ZN12v8_inspector12clientMirrorEN2v85LocalINS0_7ContextEEENS1_INS0_5ValueEEERKNS_8String16E
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2979
.L3207:
	call	_ZdlPv@PLT
.L2979:
	movq	-496(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2963
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L2963
	.p2align 4,,10
	.p2align 3
.L2971:
	movq	%r15, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	jne	.L2974
.L3195:
	movq	$0, -496(%rbp)
.L2976:
	movq	(%r15), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	je	.L3233
.L2980:
	movq	%r15, %rdi
	call	_ZNK2v85Value8IsRegExpEv@PLT
	testb	%al, %al
	je	.L2981
	leaq	-480(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN12v8_inspector15String16BuilderC1Ev@PLT
	movl	$47, %esi
	movq	%r14, %rdi
	call	_ZN12v8_inspector15String16Builder6appendEc@PLT
	movq	%r15, %rdi
	call	_ZNK2v86RegExp9GetSourceEv@PLT
	movq	%rbx, %rsi
	movq	%rax, %rdx
	leaq	-96(%rbp), %rax
	movq	%rax, %rbx
	movq	%rax, %rdi
	movq	%rax, -520(%rbp)
	call	_ZN12v8_inspector16toProtocolStringEPN2v87IsolateENS0_5LocalINS0_6StringEEE@PLT
	movq	%r14, %rdi
	movq	%rbx, %rsi
	call	_ZN12v8_inspector15String16Builder6appendERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	movq	%rax, -528(%rbp)
	cmpq	%rax, %rdi
	je	.L2982
	call	_ZdlPv@PLT
.L2982:
	movl	$47, %esi
	movq	%r14, %rdi
	call	_ZN12v8_inspector15String16Builder6appendEc@PLT
	movq	%r15, %rdi
	call	_ZNK2v86RegExp8GetFlagsEv@PLT
	movl	%eax, %ebx
	testb	$1, %al
	jne	.L3234
.L2983:
	testb	$2, %bl
	jne	.L3235
.L2984:
	testb	$4, %bl
	jne	.L3236
.L2985:
	testb	$32, %bl
	jne	.L3237
.L2986:
	testb	$16, %bl
	jne	.L3238
.L2987:
	andl	$8, %ebx
	jne	.L3239
.L2988:
	movq	-520(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN12v8_inspector15String16Builder8toStringEv@PLT
	movq	-480(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2989
	call	_ZdlPv@PLT
.L2989:
	movq	-520(%rbp), %rcx
	movq	_ZN12v8_inspector8protocol7Runtime12RemoteObject11SubtypeEnum6RegexpE(%rip), %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v84base11make_uniqueIN12v8_inspector12_GLOBAL__N_112ObjectMirrorEJRNS_5LocalINS_5ValueEEERPKcNS2_8String16EEEESt10unique_ptrIT_St14default_deleteISE_EEDpOT0_.isra.0
.L3209:
	movq	-480(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, (%r12)
	cmpq	-528(%rbp), %rdi
	jne	.L3207
	jmp	.L2979
.L3233:
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	jne	.L2980
	cmpl	$5, 43(%rax)
	jne	.L2980
	movq	_ZN12v8_inspector8protocol7Runtime12RemoteObject8TypeEnum9UndefinedE(%rip), %rdx
	leaq	-480(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v84base11make_uniqueIN12v8_inspector12_GLOBAL__N_120PrimitiveValueMirrorEJRNS_5LocalINS_5ValueEEERPKcEEESt10unique_ptrIT_St14default_deleteISD_EEDpOT0_.isra.0
	movq	-480(%rbp), %rax
	movq	%rax, (%r12)
	jmp	.L2979
	.p2align 4,,10
	.p2align 3
.L3232:
	movl	$16, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN12v8_inspector12_GLOBAL__N_112SymbolMirrorE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	%r15, 8(%rax)
	movq	%rax, (%r12)
	jmp	.L2963
.L2981:
	movq	%r15, %rdi
	call	_ZNK2v85Value7IsProxyEv@PLT
	testb	%al, %al
	je	.L2991
	movq	_ZN12v8_inspector8protocol7Runtime12RemoteObject11SubtypeEnum5ProxyE(%rip), %rsi
	leaq	-96(%rbp), %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	leaq	.LC43(%rip), %rsi
	leaq	-144(%rbp), %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movl	$104, %edi
	call	_Znwm@PLT
	movq	-144(%rbp), %rsi
	movq	%rax, %rbx
	leaq	16+_ZTVN12v8_inspector12_GLOBAL__N_112ObjectMirrorE(%rip), %rax
	movq	%rax, (%rbx)
	leaq	32(%rbx), %rax
	leaq	16(%rbx), %rdi
	movq	%rax, 16(%rbx)
	movq	-136(%rbp), %rax
	movq	%r15, 8(%rbx)
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-112(%rbp), %rax
	movq	-96(%rbp), %rsi
	movb	$1, 56(%rbx)
	leaq	64(%rbx), %rdi
	movq	%rax, 48(%rbx)
	leaq	80(%rbx), %rax
	movq	%rax, 64(%rbx)
	movq	-88(%rbp), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-64(%rbp), %rax
	movq	-144(%rbp), %rdi
	movq	%rax, 96(%rbx)
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2992
	call	_ZdlPv@PLT
.L2992:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2993
	call	_ZdlPv@PLT
.L2993:
	movq	%rbx, (%r12)
	jmp	.L2979
.L2991:
	movq	%r15, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L2994
	movl	$16, %edi
	leaq	16+_ZTVN12v8_inspector12_GLOBAL__N_114FunctionMirrorE(%rip), %rbx
	call	_Znwm@PLT
	movq	%rbx, (%rax)
	movq	%r15, 8(%rax)
	movq	%rax, (%r12)
	jmp	.L2979
.L3239:
	movl	$121, %esi
	movq	%r14, %rdi
	call	_ZN12v8_inspector15String16Builder6appendEc@PLT
	jmp	.L2988
.L3238:
	movl	$117, %esi
	movq	%r14, %rdi
	call	_ZN12v8_inspector15String16Builder6appendEc@PLT
	jmp	.L2987
.L3237:
	movl	$115, %esi
	movq	%r14, %rdi
	call	_ZN12v8_inspector15String16Builder6appendEc@PLT
	jmp	.L2986
.L3236:
	movl	$109, %esi
	movq	%r14, %rdi
	call	_ZN12v8_inspector15String16Builder6appendEc@PLT
	jmp	.L2985
.L3235:
	movl	$105, %esi
	movq	%r14, %rdi
	call	_ZN12v8_inspector15String16Builder6appendEc@PLT
	jmp	.L2984
.L3234:
	movl	$103, %esi
	movq	%r14, %rdi
	call	_ZN12v8_inspector15String16Builder6appendEc@PLT
	jmp	.L2983
.L2994:
	movq	%r15, %rdi
	call	_ZNK2v85Value6IsDateEv@PLT
	testb	%al, %al
	je	.L2995
	movq	%r13, %rdi
	leaq	-480(%rbp), %r14
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	movq	%rax, %rbx
	call	_ZN2v88TryCatchC1EPNS_7IsolateE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZNK2v85Value8ToStringENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L3240
.L3196:
	leaq	-96(%rbp), %rax
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, -520(%rbp)
	call	_ZN12v8_inspector16toProtocolStringEPN2v87IsolateENS0_5LocalINS0_6StringEEE@PLT
	movq	%r14, %rdi
	call	_ZN2v88TryCatchD1Ev@PLT
	movq	-520(%rbp), %rcx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	_ZN12v8_inspector8protocol7Runtime12RemoteObject11SubtypeEnum4DateE(%rip), %rdx
.L3211:
	call	_ZN2v84base11make_uniqueIN12v8_inspector12_GLOBAL__N_112ObjectMirrorEJRNS_5LocalINS_5ValueEEERPKcNS2_8String16EEEESt10unique_ptrIT_St14default_deleteISE_EEDpOT0_.isra.0
	movq	-480(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, (%r12)
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L3207
	jmp	.L2979
.L2995:
	movq	%r15, %rdi
	call	_ZNK2v85Value9IsPromiseEv@PLT
	movq	%r15, %rdi
	testb	%al, %al
	je	.L2999
	call	_ZN2v86Object18GetConstructorNameEv@PLT
	leaq	-144(%rbp), %rdi
	movq	%rbx, %rsi
	movq	%rax, %rdx
	call	_ZN12v8_inspector16toProtocolStringEPN2v87IsolateENS0_5LocalINS0_6StringEEE@PLT
	movq	_ZN12v8_inspector8protocol7Runtime12RemoteObject11SubtypeEnum7PromiseE(%rip), %rsi
	leaq	-96(%rbp), %rdi
.L3227:
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movl	$104, %edi
	call	_Znwm@PLT
	movq	%rax, %rbx
	leaq	16+_ZTVN12v8_inspector12_GLOBAL__N_112ObjectMirrorE(%rip), %rax
	movq	%rax, (%rbx)
	leaq	32(%rbx), %rax
	leaq	16(%rbx), %rdi
	movq	%r15, 8(%rbx)
	movq	%rax, 16(%rbx)
	movq	-136(%rbp), %rax
	movq	-144(%rbp), %rsi
	addq	%rax, %rax
	leaq	(%rsi,%rax), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-112(%rbp), %rax
	movb	$1, 56(%rbx)
	leaq	64(%rbx), %rdi
	movq	%rax, 48(%rbx)
	leaq	80(%rbx), %rax
	movq	%rax, 64(%rbx)
	movq	-88(%rbp), %rax
	movq	-96(%rbp), %rsi
	addq	%rax, %rax
	leaq	(%rsi,%rax), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-64(%rbp), %rax
	movq	%rax, 96(%rbx)
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3016
	call	_ZdlPv@PLT
.L3016:
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	movq	%rbx, (%r12)
	cmpq	%rax, %rdi
	jne	.L3207
	jmp	.L2979
.L2999:
	call	_ZNK2v85Value13IsNativeErrorEv@PLT
	testb	%al, %al
	je	.L3002
	leaq	-96(%rbp), %r14
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector12_GLOBAL__N_119descriptionForErrorEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEENS0_9ErrorTypeE
	movq	_ZN12v8_inspector8protocol7Runtime12RemoteObject11SubtypeEnum5ErrorE(%rip), %rdx
	leaq	-480(%rbp), %rdi
	movq	%r14, %rcx
.L3214:
	movq	%r15, %rsi
	jmp	.L3211
.L3230:
	call	__stack_chk_fail@PLT
.L3240:
	movq	%r15, %rdi
	call	_ZN2v86Object18GetConstructorNameEv@PLT
	movq	%rax, %rdx
	jmp	.L3196
.L3002:
	movq	%r15, %rdi
	call	_ZNK2v85Value5IsMapEv@PLT
	movq	%r15, %rdi
	testb	%al, %al
	je	.L3004
	call	_ZNK2v83Map4SizeEv@PLT
	leaq	-96(%rbp), %r13
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%rax, %rcx
	movq	%r13, %rdi
	call	_ZN12v8_inspector12_GLOBAL__N_124descriptionForCollectionEPN2v87IsolateENS1_5LocalINS1_6ObjectEEEm
	movq	_ZN12v8_inspector8protocol7Runtime12RemoteObject11SubtypeEnum3MapE(%rip), %rdx
	leaq	-480(%rbp), %rdi
	movq	%r13, %rcx
	jmp	.L3214
.L3004:
	call	_ZNK2v85Value5IsSetEv@PLT
	movq	%r15, %rdi
	testb	%al, %al
	je	.L3006
	call	_ZNK2v83Set4SizeEv@PLT
	leaq	-96(%rbp), %r13
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%rax, %rcx
	movq	%r13, %rdi
	call	_ZN12v8_inspector12_GLOBAL__N_124descriptionForCollectionEPN2v87IsolateENS1_5LocalINS1_6ObjectEEEm
	movq	_ZN12v8_inspector8protocol7Runtime12RemoteObject11SubtypeEnum3SetE(%rip), %rdx
	leaq	-480(%rbp), %rdi
	movq	%r13, %rcx
	jmp	.L3214
.L3006:
	call	_ZNK2v85Value9IsWeakMapEv@PLT
	movq	%r15, %rdi
	testb	%al, %al
	je	.L3008
	call	_ZN2v86Object18GetConstructorNameEv@PLT
	leaq	-96(%rbp), %r13
	movq	%rbx, %rsi
	movq	%rax, %rdx
	movq	%r13, %rdi
	call	_ZN12v8_inspector16toProtocolStringEPN2v87IsolateENS0_5LocalINS0_6StringEEE@PLT
	movq	_ZN12v8_inspector8protocol7Runtime12RemoteObject11SubtypeEnum7WeakmapE(%rip), %rdx
	leaq	-480(%rbp), %rdi
	movq	%r13, %rcx
	jmp	.L3214
.L3008:
	call	_ZNK2v85Value9IsWeakSetEv@PLT
	movq	%r15, %rdi
	testb	%al, %al
	je	.L3010
	call	_ZN2v86Object18GetConstructorNameEv@PLT
	leaq	-96(%rbp), %r13
	movq	%rbx, %rsi
	movq	%rax, %rdx
	movq	%r13, %rdi
	call	_ZN12v8_inspector16toProtocolStringEPN2v87IsolateENS0_5LocalINS0_6StringEEE@PLT
	movq	_ZN12v8_inspector8protocol7Runtime12RemoteObject11SubtypeEnum7WeaksetE(%rip), %rdx
	leaq	-480(%rbp), %rdi
	movq	%r13, %rcx
	jmp	.L3214
.L3010:
	call	_ZNK2v85Value13IsMapIteratorEv@PLT
	testb	%al, %al
	jne	.L3012
	movq	%r15, %rdi
	call	_ZNK2v85Value13IsSetIteratorEv@PLT
	testb	%al, %al
	je	.L3241
.L3012:
	movq	%r15, %rdi
	leaq	-96(%rbp), %r13
	call	_ZN2v86Object18GetConstructorNameEv@PLT
	movq	%r13, %rdi
	movq	%rbx, %rsi
	movq	%rax, %rdx
	call	_ZN12v8_inspector16toProtocolStringEPN2v87IsolateENS0_5LocalINS0_6StringEEE@PLT
	movq	_ZN12v8_inspector8protocol7Runtime12RemoteObject11SubtypeEnum8IteratorE(%rip), %rdx
	leaq	-480(%rbp), %rdi
	movq	%r13, %rcx
	jmp	.L3214
.L3241:
	movq	%r15, %rdi
	call	_ZNK2v85Value17IsGeneratorObjectEv@PLT
	testb	%al, %al
	je	.L3242
	movq	%r15, %rdi
	call	_ZN2v86Object18GetConstructorNameEv@PLT
	leaq	-144(%rbp), %rdi
	movq	%rbx, %rsi
	movq	%rax, %rdx
	call	_ZN12v8_inspector16toProtocolStringEPN2v87IsolateENS0_5LocalINS0_6StringEEE@PLT
	movq	_ZN12v8_inspector8protocol7Runtime12RemoteObject11SubtypeEnum9GeneratorE(%rip), %rsi
	leaq	-96(%rbp), %rdi
	jmp	.L3227
.L3242:
	movq	%r15, %rdi
	call	_ZNK2v85Value12IsTypedArrayEv@PLT
	movq	%r15, %rdi
	testb	%al, %al
	je	.L3018
	call	_ZN2v810TypedArray6LengthEv@PLT
	leaq	-96(%rbp), %r13
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%rax, %rcx
	movq	%r13, %rdi
	call	_ZN12v8_inspector12_GLOBAL__N_124descriptionForCollectionEPN2v87IsolateENS1_5LocalINS1_6ObjectEEEm
	movq	_ZN12v8_inspector8protocol7Runtime12RemoteObject11SubtypeEnum10TypedarrayE(%rip), %rdx
	leaq	-480(%rbp), %rdi
	movq	%r13, %rcx
	jmp	.L3214
.L3018:
	call	_ZNK2v85Value13IsArrayBufferEv@PLT
	movq	%r15, %rdi
	testb	%al, %al
	je	.L3020
	call	_ZNK2v811ArrayBuffer10ByteLengthEv@PLT
.L3224:
	leaq	-96(%rbp), %r13
	movq	%rax, %rcx
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector12_GLOBAL__N_124descriptionForCollectionEPN2v87IsolateENS1_5LocalINS1_6ObjectEEEm
	movq	_ZN12v8_inspector8protocol7Runtime12RemoteObject11SubtypeEnum11ArraybufferE(%rip), %rdx
	leaq	-480(%rbp), %rdi
	movq	%r13, %rcx
	jmp	.L3214
.L3020:
	call	_ZNK2v85Value19IsSharedArrayBufferEv@PLT
	movq	%r15, %rdi
	testb	%al, %al
	je	.L3022
	call	_ZNK2v817SharedArrayBuffer10ByteLengthEv@PLT
	jmp	.L3224
.L3022:
	call	_ZNK2v85Value10IsDataViewEv@PLT
	movq	%r15, %rdi
	testb	%al, %al
	je	.L3024
	call	_ZN2v815ArrayBufferView10ByteLengthEv@PLT
	leaq	-96(%rbp), %r13
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%rax, %rcx
	movq	%r13, %rdi
	call	_ZN12v8_inspector12_GLOBAL__N_124descriptionForCollectionEPN2v87IsolateENS1_5LocalINS1_6ObjectEEEm
	movq	_ZN12v8_inspector8protocol7Runtime12RemoteObject11SubtypeEnum8DataviewE(%rip), %rdx
	leaq	-480(%rbp), %rdi
	movq	%r13, %rcx
	jmp	.L3214
.L3024:
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L3026
	movq	%r13, %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%rax, %rdi
	call	_ZN2v85debug12GetInspectorEPNS_7IsolateE@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN12v8_inspector16InspectedContext9contextIdEN2v85LocalINS1_7ContextEEE@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZNK12v8_inspector15V8InspectorImpl10getContextEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3026
	movq	%r15, %rsi
	call	_ZN12v8_inspector16InspectedContext15getInternalTypeEN2v85LocalINS1_6ObjectEEE@PLT
	movq	%r15, %rdi
	movl	%eax, %r14d
	call	_ZNK2v85Value7IsArrayEv@PLT
	movq	%r15, %rdi
	testb	%al, %al
	je	.L3027
	cmpl	$3, %r14d
	jne	.L3027
	call	_ZNK2v85Array6LengthEv@PLT
	leaq	-96(%rbp), %r13
	movl	%eax, %esi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String1611fromIntegerEm@PLT
	leaq	-144(%rbp), %rax
	movq	%r13, %rdx
	movl	$93, %ecx
	movq	%rax, %rdi
	leaq	.LC44(%rip), %rsi
	movq	%rax, -536(%rbp)
	call	_ZN12v8_inspector8String166concatIJPKcS0_cEEES0_DpT_
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3032
	call	_ZdlPv@PLT
.L3032:
	movq	-536(%rbp), %rcx
	leaq	-480(%rbp), %rdi
	leaq	.LC45(%rip), %rdx
	movq	%r15, %rsi
	call	_ZN2v84base11make_uniqueIN12v8_inspector12_GLOBAL__N_112ObjectMirrorEJRNS_5LocalINS_5ValueEEERA19_KcNS2_8String16EEEESt10unique_ptrIT_St14default_deleteISE_EEDpOT0_.isra.0
	movq	-480(%rbp), %rax
	movq	-144(%rbp), %rdi
	movq	%rax, (%r12)
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L3207
	jmp	.L2979
.L3027:
	call	_ZNK2v85Value8IsObjectEv@PLT
	cmpl	$1, %r14d
	jne	.L3031
	testb	%al, %al
	je	.L3031
	movq	%r13, %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	leaq	-368(%rbp), %rcx
	leaq	.LC46(%rip), %rsi
	movq	$0, -376(%rbp)
	movq	%rcx, -568(%rbp)
	movq	%rax, %r14
	movq	%rcx, -384(%rbp)
	leaq	-96(%rbp), %rcx
	movq	%rcx, %rbx
	movq	%rcx, %rdi
	movq	%rcx, -520(%rbp)
	movq	%rax, -544(%rbp)
	movw	$0, -368(%rbp)
	movq	$0, -352(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%rbx, %rsi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN2v86Object20GetRealNamedPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L3243
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	movq	%rax, -528(%rbp)
	cmpq	%rax, %rdi
	je	.L3113
	call	_ZdlPv@PLT
.L3113:
	leaq	-488(%rbp), %rdi
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	_ZN12v8_inspector11ValueMirror6createEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE
	movq	-488(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3205
	movq	$0, -480(%rbp)
	leaq	-500(%rbp), %rdx
	movq	%r13, %rsi
	leaq	-480(%rbp), %r14
	movl	$5, -500(%rbp)
	movq	(%rdi), %rax
	movq	%r14, %r8
	movq	%rdx, %rcx
	call	*40(%rax)
	movq	-480(%rbp), %rax
	testq	%rax, %rax
	je	.L3042
	leaq	-144(%rbp), %rbx
	movw	$0, -128(%rbp)
	movq	%rbx, -536(%rbp)
	movq	%rbx, %rcx
	leaq	-128(%rbp), %rbx
	movq	%rbx, -144(%rbp)
	movq	$0, -136(%rbp)
	movq	$0, -112(%rbp)
	cmpb	$0, 96(%rax)
	je	.L3043
	leaq	104(%rax), %rcx
.L3043:
	movq	-528(%rbp), %rax
	movq	-520(%rbp), %rdi
	movq	%rcx, -552(%rbp)
	movq	%rax, -96(%rbp)
	movq	8(%rcx), %rax
	movq	(%rcx), %rsi
	addq	%rax, %rax
	leaq	(%rsi,%rax), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-552(%rbp), %rcx
	movq	-520(%rbp), %rsi
	movq	32(%rcx), %rax
	leaq	-384(%rbp), %rcx
	movq	%rcx, %rdi
	movq	%rcx, -560(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, -352(%rbp)
	cmpq	-528(%rbp), %rdi
	je	.L3044
	call	_ZdlPv@PLT
.L3044:
	movq	-144(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L3045
	call	_ZdlPv@PLT
.L3045:
	movq	_ZN12v8_inspector8protocol7Runtime12RemoteObject8TypeEnum6StringE(%rip), %rsi
	movq	-520(%rbp), %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-480(%rbp), %rcx
	movq	%rbx, -144(%rbp)
	movq	-536(%rbp), %rdi
	movq	16(%rcx), %rax
	movq	8(%rcx), %rsi
	movq	%rcx, -552(%rbp)
	addq	%rax, %rax
	leaq	(%rsi,%rax), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-552(%rbp), %rcx
	movq	-536(%rbp), %rdi
	movq	-520(%rbp), %rsi
	movq	40(%rcx), %rax
	movq	%rax, -112(%rbp)
	call	_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_
	movq	-144(%rbp), %rdi
	movl	%eax, -552(%rbp)
	cmpq	%rbx, %rdi
	je	.L3046
	call	_ZdlPv@PLT
.L3046:
	movq	-96(%rbp), %rdi
	cmpq	-528(%rbp), %rdi
	je	.L3047
	call	_ZdlPv@PLT
.L3047:
	cmpl	$0, -552(%rbp)
	je	.L3244
.L3048:
	movq	-480(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3042
	movq	(%rdi), %rax
	call	*24(%rax)
.L3042:
	movq	-488(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3116
	movq	(%rdi), %rax
	call	*8(%rax)
.L3116:
	movq	-520(%rbp), %rbx
	leaq	-320(%rbp), %rax
	leaq	.LC47(%rip), %rsi
	movq	$0, -328(%rbp)
	movq	%rax, -552(%rbp)
	movq	%rbx, %rdi
	movq	%rax, -336(%rbp)
	movw	$0, -320(%rbp)
	movq	$0, -304(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-544(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN2v86Object20GetRealNamedPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEE@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L3245
	movq	-96(%rbp), %rdi
	cmpq	-528(%rbp), %rdi
	je	.L3107
	call	_ZdlPv@PLT
.L3107:
	leaq	-488(%rbp), %rdi
	movq	%rbx, %rdx
	movq	%r13, %rsi
	call	_ZN12v8_inspector11ValueMirror6createEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE
	movq	-488(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3110
	movl	$5, -500(%rbp)
	leaq	-500(%rbp), %rdx
	movq	%r14, %r8
	movq	%r13, %rsi
	movq	$0, -480(%rbp)
	movq	(%rdi), %rax
	movq	%rdx, %rcx
	call	*40(%rax)
	movq	-480(%rbp), %rax
	testq	%rax, %rax
	je	.L3058
	leaq	-144(%rbp), %rbx
	movw	$0, -128(%rbp)
	movq	%rbx, -536(%rbp)
	movq	%rbx, %r13
	leaq	-128(%rbp), %rbx
	movq	%rbx, -144(%rbp)
	movq	$0, -136(%rbp)
	movq	$0, -112(%rbp)
	cmpb	$0, 96(%rax)
	je	.L3059
	leaq	104(%rax), %r13
.L3059:
	movq	-528(%rbp), %rax
	movq	-520(%rbp), %rdi
	movq	%rax, -96(%rbp)
	movq	8(%r13), %rax
	movq	0(%r13), %rsi
	addq	%rax, %rax
	leaq	(%rsi,%rax), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	32(%r13), %rax
	movq	-520(%rbp), %rsi
	leaq	-336(%rbp), %r13
	movq	%r13, %rdi
	movq	%rax, -64(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, -304(%rbp)
	cmpq	-528(%rbp), %rdi
	je	.L3060
	call	_ZdlPv@PLT
.L3060:
	movq	-144(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L3061
	call	_ZdlPv@PLT
.L3061:
	movq	_ZN12v8_inspector8protocol7Runtime12RemoteObject8TypeEnum6StringE(%rip), %rsi
	movq	-520(%rbp), %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-480(%rbp), %rcx
	movq	%rbx, -144(%rbp)
	movq	-536(%rbp), %rdi
	movq	16(%rcx), %rax
	movq	8(%rcx), %rsi
	movq	%rcx, -544(%rbp)
	addq	%rax, %rax
	leaq	(%rsi,%rax), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-544(%rbp), %rcx
	movq	-536(%rbp), %rdi
	movq	-520(%rbp), %rsi
	movq	40(%rcx), %rax
	movq	%rax, -112(%rbp)
	call	_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_
	movq	-144(%rbp), %rdi
	movl	%eax, -544(%rbp)
	cmpq	%rbx, %rdi
	je	.L3062
	call	_ZdlPv@PLT
.L3062:
	movq	-96(%rbp), %rdi
	cmpq	-528(%rbp), %rdi
	je	.L3063
	call	_ZdlPv@PLT
.L3063:
	cmpl	$0, -544(%rbp)
	je	.L3246
.L3064:
	movq	-480(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3058
	movq	(%rdi), %rax
	call	*24(%rax)
.L3058:
	movq	-488(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3110
	movq	(%rdi), %rax
	call	*8(%rax)
.L3110:
	cmpq	$0, -376(%rbp)
	je	.L3069
	leaq	-144(%rbp), %rax
	leaq	.LC48(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -536(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	leaq	-240(%rbp), %rdi
	leaq	.LC49(%rip), %rsi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-520(%rbp), %rbx
	leaq	.LC50(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	leaq	-288(%rbp), %rdi
	leaq	-384(%rbp), %rdx
	movq	%rbx, %rsi
	call	_ZNK12v8_inspector8String16plERKS0_
	movq	-96(%rbp), %rdi
	cmpq	-528(%rbp), %rdi
	je	.L3070
	call	_ZdlPv@PLT
.L3070:
	movq	-280(%rbp), %rax
	movq	-288(%rbp), %rsi
	movq	-528(%rbp), %r13
	movq	-520(%rbp), %rdi
	addq	%rax, %rax
	leaq	(%rsi,%rax), %rdx
	movq	%r13, -96(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-232(%rbp), %r8
	movq	-88(%rbp), %rsi
	movq	-96(%rbp), %rax
	movq	-240(%rbp), %rcx
	leaq	(%r8,%rsi), %rbx
	cmpq	%r13, %rax
	je	.L3129
	movq	-80(%rbp), %rdx
.L3071:
	cmpq	%rdx, %rbx
	ja	.L3072
	testq	%r8, %r8
	je	.L3073
	addq	%rsi, %rsi
	leaq	(%rax,%rsi), %rdi
	cmpq	$1, %r8
	je	.L3247
	addq	%r8, %r8
	je	.L3073
	movq	%r8, %rdx
	movq	%rcx, %rsi
	call	memmove@PLT
	movq	-96(%rbp), %rax
.L3073:
	movq	%rbx, -88(%rbp)
	movq	-520(%rbp), %rsi
	leaq	-192(%rbp), %rdi
	movw	$0, (%rax,%rbx,2)
	call	_ZN12v8_inspector8String16C1EONSt7__cxx1112basic_stringItSt11char_traitsItESaItEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	-528(%rbp), %rdi
	je	.L3075
	call	_ZdlPv@PLT
.L3075:
	movq	-184(%rbp), %rax
	movq	-192(%rbp), %rsi
	leaq	-432(%rbp), %r13
	leaq	-416(%rbp), %rbx
	movq	%r13, %rdi
	movq	%rbx, -432(%rbp)
	addq	%rax, %rax
	leaq	(%rsi,%rax), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-328(%rbp), %r8
	movq	-424(%rbp), %rsi
	movq	-336(%rbp), %rcx
	leaq	(%r8,%rsi), %rax
	movq	%rax, -544(%rbp)
	movq	-432(%rbp), %rax
	cmpq	%rbx, %rax
	je	.L3130
	movq	-416(%rbp), %rdx
.L3076:
	cmpq	%rdx, -544(%rbp)
	ja	.L3077
	testq	%r8, %r8
	je	.L3078
	addq	%rsi, %rsi
	leaq	(%rax,%rsi), %rdi
	cmpq	$1, %r8
	je	.L3248
	addq	%r8, %r8
	je	.L3078
	movq	%r8, %rdx
	movq	%rcx, %rsi
	call	memmove@PLT
	movq	-432(%rbp), %rax
.L3078:
	movq	-544(%rbp), %rcx
	movq	-520(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rcx, -424(%rbp)
	movw	$0, (%rax,%rcx,2)
	call	_ZN12v8_inspector8String16C1EONSt7__cxx1112basic_stringItSt11char_traitsItESaItEEE@PLT
	movq	-432(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L3080
	call	_ZdlPv@PLT
.L3080:
	movq	-536(%rbp), %rdx
	movq	-520(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNK12v8_inspector8String16plERKS0_
	movq	-96(%rbp), %rdi
	cmpq	-528(%rbp), %rdi
	je	.L3082
	call	_ZdlPv@PLT
.L3082:
	movq	-192(%rbp), %rdi
	leaq	-176(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3084
	call	_ZdlPv@PLT
.L3084:
	movq	-288(%rbp), %rdi
	leaq	-272(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3085
	call	_ZdlPv@PLT
.L3085:
	movq	-240(%rbp), %rdi
	leaq	-224(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3086
	call	_ZdlPv@PLT
.L3086:
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3083
	call	_ZdlPv@PLT
.L3083:
	movq	-336(%rbp), %rdi
	cmpq	-552(%rbp), %rdi
	je	.L3087
	call	_ZdlPv@PLT
.L3087:
	movq	-384(%rbp), %rdi
	cmpq	-568(%rbp), %rdi
	je	.L3088
	call	_ZdlPv@PLT
.L3088:
	movq	%r14, %rdi
	movq	%r13, %rcx
	leaq	.LC51(%rip), %rdx
	movq	%r15, %rsi
	call	_ZN2v84base11make_uniqueIN12v8_inspector12_GLOBAL__N_112ObjectMirrorEJRNS_5LocalINS_5ValueEEERA15_KcNS2_8String16EEEESt10unique_ptrIT_St14default_deleteISE_EEDpOT0_.isra.0
	movq	-480(%rbp), %rax
	movq	-432(%rbp), %rdi
	movq	%rax, (%r12)
	cmpq	%rbx, %rdi
	jne	.L3207
	jmp	.L2979
.L3077:
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm
	movq	-432(%rbp), %rax
	jmp	.L3078
.L3130:
	movl	$7, %edx
	jmp	.L3076
.L3072:
	movq	-520(%rbp), %rdi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm
	movq	-96(%rbp), %rax
	jmp	.L3073
.L3248:
	movzwl	(%rcx), %eax
	movw	%ax, (%rdi)
	movq	-432(%rbp), %rax
	jmp	.L3078
.L3129:
	movl	$7, %edx
	jmp	.L3071
.L3247:
	movzwl	(%rcx), %eax
	movw	%ax, (%rdi)
	movq	-96(%rbp), %rax
	jmp	.L3073
.L3069:
	movq	-328(%rbp), %rax
	movq	-336(%rbp), %rsi
	leaq	-432(%rbp), %r13
	leaq	-416(%rbp), %rbx
	movq	%r13, %rdi
	movq	%rbx, -432(%rbp)
	addq	%rax, %rax
	leaq	(%rsi,%rax), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-304(%rbp), %rax
	movq	%rax, -400(%rbp)
	jmp	.L3083
.L3246:
	movq	-328(%rbp), %rax
	movq	-336(%rbp), %rsi
	movq	%rbx, -144(%rbp)
	movq	-536(%rbp), %rdi
	addq	%rax, %rax
	leaq	(%rsi,%rax), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movl	$34, %ecx
	movl	$34, %esi
	movq	-304(%rbp), %rax
	movq	-536(%rbp), %rdx
	movq	-520(%rbp), %rdi
	movq	%rax, -112(%rbp)
	call	_ZN12v8_inspector8String166concatIJcS0_cEEES0_DpT_
	movq	-520(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, -304(%rbp)
	cmpq	-528(%rbp), %rdi
	je	.L3065
	call	_ZdlPv@PLT
.L3065:
	movq	-144(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L3064
	call	_ZdlPv@PLT
	jmp	.L3064
.L3243:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	movq	%rax, -528(%rbp)
	cmpq	%rax, %rdi
	je	.L3205
	call	_ZdlPv@PLT
.L3205:
	leaq	-480(%rbp), %r14
	jmp	.L3116
.L3245:
	movq	-96(%rbp), %rdi
	cmpq	-528(%rbp), %rdi
	je	.L3110
	call	_ZdlPv@PLT
	jmp	.L3110
.L3244:
	movq	-376(%rbp), %rax
	movq	-384(%rbp), %rsi
	movq	%rbx, -144(%rbp)
	movq	-536(%rbp), %rdi
	addq	%rax, %rax
	leaq	(%rsi,%rax), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movl	$34, %ecx
	movl	$34, %esi
	movq	-352(%rbp), %rax
	movq	-536(%rbp), %rdx
	movq	-520(%rbp), %rdi
	movq	%rax, -112(%rbp)
	call	_ZN12v8_inspector8String166concatIJcS0_cEEES0_DpT_
	movq	-560(%rbp), %rdi
	movq	-520(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, -352(%rbp)
	cmpq	-528(%rbp), %rdi
	je	.L3049
	call	_ZdlPv@PLT
.L3049:
	movq	-144(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L3048
	call	_ZdlPv@PLT
	jmp	.L3048
.L3031:
	movq	%r15, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	cmpl	$2, %r14d
	jne	.L3036
	testb	%al, %al
	je	.L3036
	movq	%r13, %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	leaq	.LC52(%rip), %rsi
	movq	%rax, %r14
	leaq	-96(%rbp), %rax
	movq	%rax, %rbx
	movq	%rax, %rdi
	movq	%rax, -520(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN2v86Object20GetRealNamedPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L3249
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	movq	%rax, -528(%rbp)
	cmpq	%rax, %rdi
	jne	.L3250
.L3122:
	movq	-520(%rbp), %rdi
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	_ZN12v8_inspector29toProtocolStringWithTypeCheckEPN2v87IsolateENS0_5LocalINS0_5ValueEEE@PLT
.L3094:
	movq	-520(%rbp), %rcx
	leaq	-480(%rbp), %rdi
	leaq	.LC53(%rip), %rdx
	movq	%r15, %rsi
	call	_ZN2v84base11make_uniqueIN12v8_inspector12_GLOBAL__N_112ObjectMirrorEJRNS_5LocalINS_5ValueEEERA15_KcNS2_8String16EEEESt10unique_ptrIT_St14default_deleteISE_EEDpOT0_.isra.0
	jmp	.L3209
.L3026:
	movq	%r15, %rdi
	call	_ZNK2v85Value7IsArrayEv@PLT
	movq	%r15, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	movq	%r15, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
.L3036:
	movq	$0, -488(%rbp)
	movq	%r15, %rdi
	call	_ZNK2v85Value7IsArrayEv@PLT
	testb	%al, %al
	jne	.L3090
	leaq	-488(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector12_GLOBAL__N_111isArrayLikeEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEEPm
	testb	%al, %al
	je	.L3251
.L3090:
	movq	%r15, %rdi
	call	_ZNK2v85Value7IsArrayEv@PLT
	testb	%al, %al
	jne	.L3252
	movq	-488(%rbp), %rcx
.L3099:
	leaq	-96(%rbp), %r13
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%rcx, -488(%rbp)
	movq	%r13, %rdi
	call	_ZN12v8_inspector12_GLOBAL__N_124descriptionForCollectionEPN2v87IsolateENS1_5LocalINS1_6ObjectEEEm
	movq	_ZN12v8_inspector8protocol7Runtime12RemoteObject11SubtypeEnum5ArrayE(%rip), %rdx
	leaq	-480(%rbp), %rdi
	movq	%r13, %rcx
	jmp	.L3214
.L3251:
	movq	%r15, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	jne	.L3253
	movq	$0, (%r12)
	jmp	.L2979
.L3252:
	movq	%r15, %rdi
	call	_ZNK2v85Array6LengthEv@PLT
	movl	%eax, %ecx
	jmp	.L3099
.L3253:
	movq	%r15, %rdi
	call	_ZN2v86Object18GetConstructorNameEv@PLT
	movq	%rbx, %rsi
	leaq	-96(%rbp), %rdi
	movq	%rax, %rdx
	call	_ZN12v8_inspector16toProtocolStringEPN2v87IsolateENS0_5LocalINS0_6StringEEE@PLT
	movl	$104, %edi
	call	_Znwm@PLT
	movq	-96(%rbp), %rsi
	movq	%rax, %rbx
	leaq	16+_ZTVN12v8_inspector12_GLOBAL__N_112ObjectMirrorE(%rip), %rax
	movq	%rax, (%rbx)
	leaq	32(%rbx), %rax
	leaq	16(%rbx), %rdi
	movq	%rax, 16(%rbx)
	movq	-88(%rbp), %rax
	movq	%r15, 8(%rbx)
	addq	%rax, %rax
	leaq	(%rsi,%rax), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-64(%rbp), %rax
	movb	$0, 56(%rbx)
	movq	$0, 72(%rbx)
	movq	%rax, 48(%rbx)
	leaq	80(%rbx), %rax
	movq	%rax, 64(%rbx)
	leaq	-80(%rbp), %rax
	movw	$0, 80(%rbx)
	movq	$0, 96(%rbx)
	movq	-96(%rbp), %rdi
	movq	%rbx, (%r12)
	cmpq	%rax, %rdi
	jne	.L3207
	jmp	.L2979
.L3249:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	movq	%rax, -528(%rbp)
	cmpq	%rax, %rdi
	je	.L3119
	call	_ZdlPv@PLT
.L3119:
	movq	-528(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -88(%rbp)
	movq	$0, -64(%rbp)
	movq	%rax, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	jmp	.L3094
.L3250:
	call	_ZdlPv@PLT
	jmp	.L3122
	.cfi_endproc
.LFE8061:
	.size	_ZN12v8_inspector11ValueMirror6createEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE, .-_ZN12v8_inspector11ValueMirror6createEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE
	.section	.rodata._ZN12v8_inspector11ValueMirror21getInternalPropertiesEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEEPSt6vectorINS_22InternalPropertyMirrorESaIS8_EE.str1.1,"aMS",@progbits,1
.LC54:
	.string	"[[FunctionLocation]]"
.LC55:
	.string	"[[IsGenerator]]"
.LC56:
	.string	"[[GeneratorLocation]]"
	.section	.text._ZN12v8_inspector11ValueMirror21getInternalPropertiesEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEEPSt6vectorINS_22InternalPropertyMirrorESaIS8_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector11ValueMirror21getInternalPropertiesEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEEPSt6vectorINS_22InternalPropertyMirrorESaIS8_EE
	.type	_ZN12v8_inspector11ValueMirror21getInternalPropertiesEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEEPSt6vectorINS_22InternalPropertyMirrorESaIS8_EE, @function
_ZN12v8_inspector11ValueMirror21getInternalPropertiesEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEEPSt6vectorINS_22InternalPropertyMirrorESaIS8_EE:
.LFB8042:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-160(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -216(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v87Context10GetIsolateEv@PLT
	movl	$1, %edx
	movq	%rax, %rbx
	leaq	-192(%rbp), %rax
	movq	%rax, %rdi
	movq	%rbx, %rsi
	movq	%rax, -224(%rbp)
	call	_ZN2v815MicrotasksScopeC1EPNS_7IsolateENS0_4TypeE@PLT
	movq	%r14, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88TryCatchC1EPNS_7IsolateE@PLT
	movq	%r12, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L3329
.L3255:
	movq	%r12, %rdi
	call	_ZNK2v85Value17IsGeneratorObjectEv@PLT
	testb	%al, %al
	jne	.L3330
.L3261:
	movq	%rbx, %rdi
	call	_ZN2v85debug12GetInspectorEPNS_7IsolateE@PLT
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	24(%rax), %rdi
	call	_ZN12v8_inspector10V8Debugger18internalPropertiesEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L3275
	leaq	-208(%rbp), %rax
	movq	%r12, %rdi
	xorl	%ebx, %ebx
	movq	%rax, -232(%rbp)
	call	_ZNK2v85Array6LengthEv@PLT
	cmpl	%ebx, %eax
	jbe	.L3275
	.p2align 4,,10
	.p2align 3
.L3331:
	movl	%ebx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L3276
	movq	(%rax), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L3276
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L3276
	leal	1(%rbx), %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L3276
	movq	-232(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector11ValueMirror6createEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE
	cmpq	$0, -208(%rbp)
	je	.L3279
	movq	%r13, %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	leaq	-112(%rbp), %r9
	movq	%r15, %rdx
	movq	%rax, %rsi
	movq	%r9, %rdi
	movq	%r9, -240(%rbp)
	call	_ZN12v8_inspector29toProtocolStringWithTypeCheckEPN2v87IsolateENS0_5LocalINS0_5ValueEEE@PLT
	movq	-240(%rbp), %r9
	movq	-208(%rbp), %rax
	movq	$0, -208(%rbp)
	movq	-216(%rbp), %rdi
	movq	%r9, %rsi
	movq	%rax, -72(%rbp)
	call	_ZNSt6vectorIN12v8_inspector22InternalPropertyMirrorESaIS1_EE12emplace_backIJS1_EEEvDpOT_
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3280
	movq	(%rdi), %rax
	call	*8(%rax)
.L3280:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3281
	call	_ZdlPv@PLT
.L3281:
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3279
	movq	(%rdi), %rax
	call	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L3279:
	movq	%r12, %rdi
	addl	$2, %ebx
	call	_ZNK2v85Array6LengthEv@PLT
	cmpl	%ebx, %eax
	ja	.L3331
.L3275:
	movq	%r14, %rdi
	call	_ZN2v88TryCatchD1Ev@PLT
	movq	-224(%rbp), %rdi
	call	_ZN2v815MicrotasksScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3332
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3276:
	.cfi_restore_state
	movq	%r14, %rdi
	call	_ZN2v88TryCatch5ResetEv@PLT
	jmp	.L3279
	.p2align 4,,10
	.p2align 3
.L3330:
	movq	%r12, %rdi
	call	_ZN2v85debug15GeneratorObject4CastENS_5LocalINS_5ValueEEE@PLT
	movq	%rax, %rdi
	movq	%rax, -232(%rbp)
	call	_ZN2v85debug15GeneratorObject11IsSuspendedEv@PLT
	movq	-232(%rbp), %rdi
	testb	%al, %al
	jne	.L3262
	call	_ZN2v85debug15GeneratorObject8FunctionEv@PLT
	movq	%rax, %r15
	movq	%rax, %rdi
	call	_ZNK2v88Function21GetScriptColumnNumberEv@PLT
	movq	%r15, %rdi
	movl	%eax, -240(%rbp)
	call	_ZNK2v88Function19GetScriptLineNumberEv@PLT
	movq	%r15, %rdi
	movl	%eax, -232(%rbp)
	call	_ZNK2v88Function8ScriptIdEv@PLT
	testl	%eax, %eax
	movl	%eax, -244(%rbp)
	je	.L3261
	movl	_ZN2v88Function19kLineOffsetNotFoundE(%rip), %eax
	cmpl	%eax, -232(%rbp)
	je	.L3261
	cmpl	%eax, -240(%rbp)
	je	.L3261
	movl	$32, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN12v8_inspector12_GLOBAL__N_114LocationMirrorE(%rip), %rdx
	movl	-232(%rbp), %ecx
	movl	-240(%rbp), %esi
	movq	%rdx, (%rax)
	movl	-244(%rbp), %edx
	movq	%r15, 8(%rax)
	movl	%edx, 16(%rax)
	movl	%ecx, 20(%rax)
	movl	%esi, 24(%rax)
	jmp	.L3266
	.p2align 4,,10
	.p2align 3
.L3329:
	movq	%r12, %rdi
	call	_ZNK2v88Function21GetScriptColumnNumberEv@PLT
	movq	%r12, %rdi
	movl	%eax, -232(%rbp)
	call	_ZNK2v88Function19GetScriptLineNumberEv@PLT
	movq	%r12, %rdi
	movl	%eax, %r15d
	call	_ZNK2v88Function8ScriptIdEv@PLT
	testl	%eax, %eax
	movl	%eax, -240(%rbp)
	je	.L3288
	movl	_ZN2v88Function19kLineOffsetNotFoundE(%rip), %eax
	cmpl	%eax, %r15d
	je	.L3288
	cmpl	%eax, -232(%rbp)
	je	.L3288
	movl	$32, %edi
	call	_Znwm@PLT
	movl	-240(%rbp), %edx
	movl	-232(%rbp), %ecx
	leaq	16+_ZTVN12v8_inspector12_GLOBAL__N_114LocationMirrorE(%rip), %rsi
	movq	%rsi, (%rax)
	leaq	-112(%rbp), %r8
	leaq	.LC54(%rip), %rsi
	movl	%edx, 16(%rax)
	movq	%r8, %rdi
	movl	%ecx, 24(%rax)
	movq	%r12, 8(%rax)
	movl	%r15d, 20(%rax)
	movq	%rax, -240(%rbp)
	movq	%r8, -232(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-232(%rbp), %r8
	movq	-240(%rbp), %rax
	movq	-216(%rbp), %rdi
	movq	%r8, %rsi
	movq	%rax, -72(%rbp)
	call	_ZNSt6vectorIN12v8_inspector22InternalPropertyMirrorESaIS1_EE12emplace_backIJS1_EEEvDpOT_
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3257
	movq	(%rdi), %rax
	call	*8(%rax)
.L3257:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3288
	call	_ZdlPv@PLT
.L3288:
	movq	%r12, %rdi
	call	_ZNK2v85Value19IsGeneratorFunctionEv@PLT
	testb	%al, %al
	je	.L3255
	leaq	-112(%rbp), %r8
	leaq	.LC55(%rip), %rsi
	movq	%r8, %rdi
	movq	%r8, -232(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	leaq	-72(%rbp), %rdi
	movq	%r13, %rsi
	leaq	112(%rax), %rdx
	call	_ZN12v8_inspector11ValueMirror6createEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE
	movq	-232(%rbp), %r8
	movq	-216(%rbp), %rdi
	movq	%r8, %rsi
	call	_ZNSt6vectorIN12v8_inspector22InternalPropertyMirrorESaIS1_EE12emplace_backIJS1_EEEvDpOT_
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3259
	movq	(%rdi), %rax
	call	*8(%rax)
.L3259:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3255
	call	_ZdlPv@PLT
	jmp	.L3255
	.p2align 4,,10
	.p2align 3
.L3262:
	movq	%rdi, -232(%rbp)
	call	_ZN2v85debug15GeneratorObject6ScriptEv@PLT
	movq	-232(%rbp), %rdi
	testq	%rax, %rax
	je	.L3261
	movq	%rax, -240(%rbp)
	call	_ZN2v85debug15GeneratorObject17SuspendedLocationEv@PLT
	leaq	-208(%rbp), %rdi
	movl	%edx, -200(%rbp)
	movq	%rdi, -232(%rbp)
	movq	%rax, -208(%rbp)
	call	_ZNK2v85debug8Location15GetColumnNumberEv@PLT
	movq	-232(%rbp), %rdi
	movl	%eax, %r15d
	call	_ZNK2v85debug8Location13GetLineNumberEv@PLT
	movq	-240(%rbp), %r8
	movl	%eax, -232(%rbp)
	movq	%r8, %rdi
	call	_ZNK2v85debug6Script2IdEv@PLT
	movl	%eax, %edx
	testl	%eax, %eax
	je	.L3261
	movl	_ZN2v88Function19kLineOffsetNotFoundE(%rip), %eax
	movl	%edx, -240(%rbp)
	cmpl	%eax, %r15d
	je	.L3261
	cmpl	%eax, -232(%rbp)
	je	.L3261
	movl	$32, %edi
	call	_Znwm@PLT
	movl	-240(%rbp), %edx
	movl	-232(%rbp), %ecx
	leaq	16+_ZTVN12v8_inspector12_GLOBAL__N_114LocationMirrorE(%rip), %rsi
	movq	%rsi, (%rax)
	movq	%r12, 8(%rax)
	movl	%edx, 16(%rax)
	movl	%ecx, 20(%rax)
	movl	%r15d, 24(%rax)
.L3266:
	leaq	-112(%rbp), %r8
	leaq	.LC56(%rip), %rsi
	movq	%rax, -240(%rbp)
	movq	%r8, %rdi
	movq	%r8, -232(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-232(%rbp), %r8
	movq	-240(%rbp), %rax
	movq	-216(%rbp), %rdi
	movq	%r8, %rsi
	movq	%rax, -72(%rbp)
	call	_ZNSt6vectorIN12v8_inspector22InternalPropertyMirrorESaIS1_EE12emplace_backIJS1_EEEvDpOT_
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3271
	movq	(%rdi), %rax
	call	*8(%rax)
.L3271:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3261
	call	_ZdlPv@PLT
	jmp	.L3261
.L3332:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8042:
	.size	_ZN12v8_inspector11ValueMirror21getInternalPropertiesEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEEPSt6vectorINS_22InternalPropertyMirrorESaIS8_EE, .-_ZN12v8_inspector11ValueMirror21getInternalPropertiesEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEEPSt6vectorINS_22InternalPropertyMirrorESaIS8_EE
	.section	.text._ZN12v8_inspector11ValueMirror20getPrivatePropertiesEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector11ValueMirror20getPrivatePropertiesEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEE
	.type	_ZN12v8_inspector11ValueMirror20getPrivatePropertiesEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEE, @function
_ZN12v8_inspector11ValueMirror20getPrivatePropertiesEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEE:
.LFB8047:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, 16(%rdi)
	movups	%xmm0, (%rdi)
	movq	%rsi, %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	movl	$1, %edx
	movq	%rax, %r13
	leaq	-192(%rbp), %rax
	movq	%rax, %rdi
	movq	%r13, %rsi
	movq	%rax, -240(%rbp)
	call	_ZN2v815MicrotasksScopeC1EPNS_7IsolateENS0_4TypeE@PLT
	leaq	-160(%rbp), %rax
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -232(%rbp)
	call	_ZN2v88TryCatchC1EPNS_7IsolateE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v85debug16GetPrivateFieldsENS_5LocalINS_7ContextEEENS1_INS_6ObjectEEE@PLT
	testq	%rax, %rax
	je	.L3334
	movq	%rax, %r13
	leaq	-200(%rbp), %rax
	xorl	%ebx, %ebx
	movq	%rax, -216(%rbp)
	jmp	.L3345
	.p2align 4,,10
	.p2align 3
.L3362:
	call	_ZNK2v87Private4NameEv@PLT
	leal	1(%rbx), %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L3335
	movq	-216(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN12v8_inspector11ValueMirror6createEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE
	cmpq	$0, -200(%rbp)
	je	.L3344
	movq	%r12, %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	leaq	-112(%rbp), %r8
	movq	%r14, %rdx
	movq	%rax, %rsi
	movq	%r8, %rdi
	movq	%r8, -224(%rbp)
	call	_ZN12v8_inspector29toProtocolStringWithTypeCheckEPN2v87IsolateENS0_5LocalINS0_5ValueEEE@PLT
	movq	-200(%rbp), %rax
	movq	8(%r15), %rsi
	movq	$0, -200(%rbp)
	cmpq	16(%r15), %rsi
	movq	-224(%rbp), %r8
	movq	%rax, -72(%rbp)
	je	.L3337
	leaq	16(%rsi), %rax
	leaq	-96(%rbp), %r14
	movq	%rax, (%rsi)
	movq	-112(%rbp), %rax
	cmpq	%r14, %rax
	je	.L3366
	movq	%rax, (%rsi)
	movq	-96(%rbp), %rax
	movq	%rax, 16(%rsi)
.L3339:
	movq	-104(%rbp), %rax
	movq	%r14, -112(%rbp)
	movq	%rax, 8(%rsi)
	xorl	%eax, %eax
	movw	%ax, -96(%rbp)
	movq	-80(%rbp), %rax
	movq	$0, -104(%rbp)
	movq	%rax, 32(%rsi)
	movq	-72(%rbp), %rax
	movq	$0, -72(%rbp)
	movq	%rax, 40(%rsi)
	addq	$48, 8(%r15)
.L3340:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3341
	movq	(%rdi), %rax
	call	*8(%rax)
.L3341:
	movq	-112(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L3342
	call	_ZdlPv@PLT
.L3342:
	movq	-200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3344
	movq	(%rdi), %rax
	call	*8(%rax)
.L3344:
	addl	$2, %ebx
.L3345:
	movq	%r13, %rdi
	call	_ZNK2v85Array6LengthEv@PLT
	cmpl	%ebx, %eax
	jbe	.L3334
	movq	%r13, %rdi
	movl	%ebx, %edx
	movq	%r12, %rsi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L3362
.L3335:
	movq	-232(%rbp), %rdi
	call	_ZN2v88TryCatch5ResetEv@PLT
	jmp	.L3344
	.p2align 4,,10
	.p2align 3
.L3334:
	movq	-232(%rbp), %rdi
	call	_ZN2v88TryCatchD1Ev@PLT
	movq	-240(%rbp), %rdi
	call	_ZN2v815MicrotasksScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3367
	addq	$200, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3366:
	.cfi_restore_state
	movdqa	-96(%rbp), %xmm1
	movups	%xmm1, 16(%rsi)
	jmp	.L3339
	.p2align 4,,10
	.p2align 3
.L3337:
	movq	%r8, %rdx
	movq	%r15, %rdi
	leaq	-96(%rbp), %r14
	call	_ZNSt6vectorIN12v8_inspector21PrivatePropertyMirrorESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	jmp	.L3340
.L3367:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8047:
	.size	_ZN12v8_inspector11ValueMirror20getPrivatePropertiesEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEE, .-_ZN12v8_inspector11ValueMirror20getPrivatePropertiesEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEE
	.section	.rodata._ZN12v8_inspector11ValueMirror13getPropertiesEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEEbbPNS0_19PropertyAccumulatorE.str1.1,"aMS",@progbits,1
.LC57:
	.string	"[[Int8Array]]"
.LC58:
	.string	"[[Uint8Array]]"
.LC59:
	.string	"[[Int16Array]]"
.LC60:
	.string	"[[Int32Array]]"
.LC61:
	.string	"__proto__"
.LC62:
	.string	"body"
.LC63:
	.string	"Request"
.LC64:
	.string	"Response"
	.section	.text._ZN12v8_inspector11ValueMirror13getPropertiesEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEEbbPNS0_19PropertyAccumulatorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector11ValueMirror13getPropertiesEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEEbbPNS0_19PropertyAccumulatorE
	.type	_ZN12v8_inspector11ValueMirror13getPropertiesEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEEbbPNS0_19PropertyAccumulatorE, @function
_ZN12v8_inspector11ValueMirror13getPropertiesEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEEbbPNS0_19PropertyAccumulatorE:
.LFB8027:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$696, %rsp
	movl	%edx, -704(%rbp)
	movq	%r8, -648(%rbp)
	movb	%dl, -572(%rbp)
	movl	%ecx, -724(%rbp)
	movb	%cl, -573(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -664(%rbp)
	call	_ZN2v87Context10GetIsolateEv@PLT
	leaq	-480(%rbp), %rcx
	movq	%rax, %r14
	movq	%rcx, %rdi
	movq	%rax, %rsi
	movq	%rcx, -696(%rbp)
	movq	%rax, -656(%rbp)
	call	_ZN2v88TryCatchC1EPNS_7IsolateE@PLT
	movq	%r14, %rdi
	call	_ZN2v83Set3NewEPNS_7IsolateE@PLT
	movl	$1, %edx
	movq	%r14, %rsi
	movq	%rax, %r15
	leaq	-544(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -688(%rbp)
	call	_ZN2v815MicrotasksScopeC1EPNS_7IsolateENS0_4TypeE@PLT
	movq	%r12, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	jne	.L3369
.L3607:
	movl	$0, -700(%rbp)
.L3370:
	movq	%rbx, %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%rax, %rdi
	call	_ZN2v85debug12GetInspectorEPNS_7IsolateE@PLT
	leaq	_ZN12v8_inspector17V8InspectorClient27formatAccessorsAsPropertiesEN2v85LocalINS1_5ValueEEE(%rip), %rdx
	movb	$0, -575(%rbp)
	movq	16(%rax), %rdi
	movq	(%rdi), %rax
	movq	80(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3615
.L3378:
	movq	-664(%rbp), %rdi
	call	_ZNK2v85Value13IsArrayBufferEv@PLT
	testb	%al, %al
	jne	.L3616
.L3379:
	movq	-664(%rbp), %rdi
	call	_ZNK2v85Value19IsSharedArrayBufferEv@PLT
	testb	%al, %al
	jne	.L3617
.L3382:
	movq	-664(%rbp), %rsi
	leaq	-560(%rbp), %rdi
	call	_ZN2v85debug16PropertyIterator6CreateENS_5LocalINS_6ObjectEEE@PLT
	leaq	-288(%rbp), %rax
	movq	%rax, -680(%rbp)
	.p2align 4,,10
	.p2align 3
.L3471:
	movq	-560(%rbp), %rdi
	movq	(%rdi), %rax
	call	*16(%rax)
	testb	%al, %al
	jne	.L3385
	movq	-560(%rbp), %rdi
	movq	(%rdi), %rax
	call	*80(%rax)
	movb	%al, -568(%rbp)
	cmpb	$1, %al
	je	.L3511
	cmpb	$0, -572(%rbp)
	jne	.L3385
.L3511:
	movq	-560(%rbp), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	movq	%rax, %r12
	call	_ZN2v83Set3HasENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movb	%al, -569(%rbp)
	testb	%al, %al
	je	.L3388
	shrw	$8, %ax
	jne	.L3469
	movq	%r15, %rdi
	movq	%r12, %rdx
	movq	%rbx, %rsi
	call	_ZN2v83Set3AddENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L3618
	leaq	-272(%rbp), %rax
	movq	$0, -280(%rbp)
	movq	%rax, -288(%rbp)
	movq	%rax, -640(%rbp)
	xorl	%eax, %eax
	movw	%ax, -272(%rbp)
	movq	$0, -256(%rbp)
	movq	(%r12), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L3392
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	jbe	.L3619
.L3392:
	movq	%r12, %rdi
	leaq	-144(%rbp), %r14
	call	_ZNK2v86Symbol4NameEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %r13
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%r13, %rdx
	movq	%r14, %rdi
	leaq	-240(%rbp), %r13
	movq	%rax, %rsi
	call	_ZN12v8_inspector29toProtocolStringWithTypeCheckEPN2v87IsolateENS0_5LocalINS0_5ValueEEE@PLT
	movq	%r13, %rdi
	movq	%r14, %rdx
	leaq	.LC29(%rip), %rcx
	leaq	.LC30(%rip), %rsi
	call	_ZN12v8_inspector8String166concatIJPKcS0_S3_EEES0_DpT_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	movq	%rax, -616(%rbp)
	cmpq	%rax, %rdi
	je	.L3395
	call	_ZdlPv@PLT
.L3395:
	movq	-680(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	movq	-208(%rbp), %rax
	movq	-240(%rbp), %rdi
	movq	%rax, -256(%rbp)
	leaq	-224(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3396
	call	_ZdlPv@PLT
.L3396:
	leaq	-336(%rbp), %r10
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r10, %rdi
	call	_ZN12v8_inspector11ValueMirror6createEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE
	movq	-336(%rbp), %rax
	movq	%rax, -632(%rbp)
.L3394:
	leaq	-432(%rbp), %rax
	movq	-656(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rax, -608(%rbp)
	call	_ZN2v88TryCatchC1EPNS_7IsolateE@PLT
	movq	-560(%rbp), %rdi
	movq	(%rdi), %rax
	call	*64(%rax)
	movb	%al, -592(%rbp)
	movq	%rax, %r13
	testb	%al, %al
	je	.L3397
	movq	-560(%rbp), %rdi
	shrq	$32, %r13
	movq	(%rdi), %rax
	call	*40(%rax)
	testb	%al, %al
	je	.L3398
	movq	-560(%rbp), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	testb	%al, %al
	jne	.L3620
	movq	-560(%rbp), %rdi
	movq	(%rdi), %rax
	call	*56(%rax)
	leaq	-336(%rbp), %r10
	movq	$0, -584(%rbp)
	movl	%eax, %r8d
	movl	%r13d, %eax
	notl	%eax
	andl	$1, %eax
	movb	%al, -592(%rbp)
	movl	%r13d, %eax
	shrl	$2, %r13d
	shrl	%eax
	xorl	$1, %r13d
	xorl	$1, %eax
	andl	$1, %eax
	movb	%al, -571(%rbp)
	movl	%r13d, %eax
	andl	$1, %eax
	movb	%al, -570(%rbp)
	testb	%r8b, %r8b
	je	.L3476
.L3503:
	movq	%rbx, %rdi
	movq	%r10, -600(%rbp)
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	-600(%rbp), %r10
	movq	%rax, %r13
	movq	%rax, %rsi
	movq	%r10, %rdi
	movq	%r10, -624(%rbp)
	call	_ZN2v88TryCatchC1EPNS_7IsolateE@PLT
	movq	%r13, %rdi
	call	_ZN2v86Object3NewEPNS_7IsolateE@PLT
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, -600(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	-600(%rbp), %rdi
	movq	%r12, %rcx
	movq	%rbx, %rsi
	movq	%rax, %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-144(%rbp), %rdi
	cmpq	-616(%rbp), %rdi
	movq	-624(%rbp), %r10
	movl	%eax, %r12d
	je	.L3408
	call	_ZdlPv@PLT
	movq	-624(%rbp), %r10
.L3408:
	testb	%r12b, %r12b
	jne	.L3409
.L3413:
	movq	$0, -384(%rbp)
.L3410:
	movq	%r10, %rdi
	call	_ZN2v88TryCatchD1Ev@PLT
	movq	-384(%rbp), %rax
	cmpq	$0, -584(%rbp)
	movq	%rax, -624(%rbp)
	jne	.L3507
	testq	%rax, %rax
	je	.L3476
.L3507:
	movq	$0, -600(%rbp)
	xorl	%r12d, %r12d
.L3415:
	movq	-288(%rbp), %rsi
	movq	-280(%rbp), %rax
	leaq	-240(%rbp), %r13
	leaq	-224(%rbp), %rcx
	movq	%r13, %rdi
	movq	%rcx, -240(%rbp)
	leaq	(%rsi,%rax,2), %rdx
	movq	%rcx, -672(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-256(%rbp), %rax
	movq	-560(%rbp), %rdi
	movq	%rax, -208(%rbp)
	movzbl	-592(%rbp), %eax
	movb	%al, -200(%rbp)
	movzbl	-570(%rbp), %eax
	movb	%al, -199(%rbp)
	movzbl	-571(%rbp), %eax
	movb	%al, -198(%rbp)
	movzbl	-568(%rbp), %eax
	movb	%al, -197(%rbp)
	movq	(%rdi), %rax
	call	*88(%rax)
	movq	-648(%rbp), %rsi
	movq	-624(%rbp), %xmm0
	movq	%r12, -160(%rbp)
	movq	-600(%rbp), %xmm1
	movb	%al, -196(%rbp)
	movq	(%rsi), %rdx
	movhps	-632(%rbp), %xmm0
	movq	-616(%rbp), %rsi
	movhps	-584(%rbp), %xmm1
	movaps	%xmm0, -176(%rbp)
	movq	-672(%rbp), %rcx
	movaps	%xmm1, -192(%rbp)
	movq	16(%rdx), %r9
	movq	-240(%rbp), %rdx
	movq	%rsi, -144(%rbp)
	cmpq	%rcx, %rdx
	je	.L3621
	movq	%rdx, -144(%rbp)
	movq	-224(%rbp), %rdx
	movq	%rdx, -128(%rbp)
.L3466:
	movq	-232(%rbp), %rdx
	pxor	%xmm2, %xmm2
	movq	%r12, -64(%rbp)
	movq	%r14, %rsi
	movq	%rcx, -240(%rbp)
	movq	-648(%rbp), %rdi
	movq	%rdx, -136(%rbp)
	xorl	%edx, %edx
	movw	%dx, -224(%rbp)
	movq	-208(%rbp), %rdx
	movaps	%xmm2, -192(%rbp)
	movq	%rdx, -112(%rbp)
	movl	-200(%rbp), %edx
	movaps	%xmm2, -176(%rbp)
	movl	%edx, -104(%rbp)
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	$0, -232(%rbp)
	movb	%al, -100(%rbp)
	movq	$0, -160(%rbp)
	call	*%r9
	movq	%r14, %rdi
	movl	%eax, %r12d
	call	_ZN12v8_inspector14PropertyMirrorD1Ev
	movq	%r13, %rdi
	testb	%r12b, %r12b
	je	.L3622
	call	_ZN12v8_inspector14PropertyMirrorD1Ev
	movq	-608(%rbp), %rdi
	call	_ZN2v88TryCatchD1Ev@PLT
.L3614:
	movq	-288(%rbp), %rdi
	cmpq	-640(%rbp), %rdi
	je	.L3469
	call	_ZdlPv@PLT
.L3469:
	movq	-560(%rbp), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L3471
	.p2align 4,,10
	.p2align 3
.L3397:
	movq	-608(%rbp), %rdi
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	leaq	-336(%rbp), %r10
	movq	%rbx, %rsi
	movq	%rax, %rdx
	movq	%r10, %rdi
	call	_ZN12v8_inspector11ValueMirror6createEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE
	movb	$0, -570(%rbp)
	movq	-336(%rbp), %r12
	movb	$0, -571(%rbp)
	movzbl	-573(%rbp), %ecx
	movq	$0, -624(%rbp)
	movq	$0, -584(%rbp)
	movq	$0, -600(%rbp)
.L3493:
	testb	%cl, %cl
	je	.L3415
	movq	-608(%rbp), %rdi
	call	_ZN2v88TryCatchD1Ev@PLT
	testq	%r12, %r12
	je	.L3460
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
.L3460:
	movq	-624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3461
	movq	(%rdi), %rax
	call	*8(%rax)
.L3461:
	movq	-584(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3462
	movq	(%rdi), %rax
	call	*8(%rax)
.L3462:
	movq	-600(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3463
	movq	(%rdi), %rax
	call	*8(%rax)
.L3463:
	movq	-632(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3614
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L3614
	.p2align 4,,10
	.p2align 3
.L3398:
	movq	-656(%rbp), %rsi
	leaq	-384(%rbp), %r13
	movb	%al, -624(%rbp)
	movq	%r13, %rdi
	call	_ZN2v88TryCatchC1EPNS_7IsolateE@PLT
	movq	-560(%rbp), %rsi
	leaq	-336(%rbp), %r10
	movq	%r10, -584(%rbp)
	movq	%r10, %rdi
	movq	(%rsi), %rax
	call	*72(%rax)
	movzbl	-336(%rbp), %eax
	movq	-584(%rbp), %r10
	testb	%al, %al
	movb	%al, -574(%rbp)
	je	.L3417
	movzbl	-328(%rbp), %eax
	movq	-320(%rbp), %r9
	movq	-312(%rbp), %r8
	movl	%eax, %edx
	movl	%eax, %esi
	movl	%eax, %edi
	movl	%eax, %ecx
	shrb	$2, %dl
	movl	%eax, %r11d
	shrb	$4, %sil
	andl	$1, %edi
	shrb	$5, %al
	shrb	%cl
	andl	$1, %edx
	andl	$1, %esi
	shrb	$3, %r11b
	andl	$1, %eax
	movq	%r9, -600(%rbp)
	movq	-304(%rbp), %r9
	cmovne	%esi, %eax
	andl	$1, %ecx
	cmovne	%edi, %ecx
	andl	$1, %r11d
	movb	%al, -592(%rbp)
	movq	-600(%rbp), %rax
	cmovne	%edx, %r11d
	movb	%cl, -571(%rbp)
	movzbl	-624(%rbp), %ecx
	testq	%rax, %rax
	movb	%r11b, -570(%rbp)
	je	.L3421
	movq	%rax, %rdx
	movq	%r10, %rdi
	movq	%rbx, %rsi
	movq	%r9, -712(%rbp)
	movq	%r8, -672(%rbp)
	movq	%rax, -504(%rbp)
	call	_ZN12v8_inspector11ValueMirror6createEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE
	movq	-336(%rbp), %rax
	movq	-712(%rbp), %r9
	movq	-672(%rbp), %r8
	movzbl	-624(%rbp), %ecx
	movq	%rax, -600(%rbp)
	movq	-584(%rbp), %r10
.L3421:
	testq	%r8, %r8
	je	.L3422
	movq	%r10, %rdi
	movq	%r8, %rdx
	movq	%rbx, %rsi
	movq	%r9, -736(%rbp)
	movb	%cl, -712(%rbp)
	movq	%r10, -672(%rbp)
	movq	%r8, -624(%rbp)
	call	_ZN12v8_inspector11ValueMirror6createEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE
	movq	-624(%rbp), %r8
	movq	-336(%rbp), %rax
	movq	%r8, %rdi
	movq	%rax, -584(%rbp)
	call	_ZNK2v85Value10IsFunctionEv@PLT
	movq	-624(%rbp), %r8
	movq	-672(%rbp), %r10
	testb	%al, %al
	movb	%al, -720(%rbp)
	movzbl	-712(%rbp), %ecx
	movq	-736(%rbp), %r9
	jne	.L3623
	testq	%r9, %r9
	je	.L3624
.L3424:
	movq	%r10, %rdi
	movq	%r9, %rdx
	movq	%rbx, %rsi
	movb	%cl, -712(%rbp)
	movq	%r10, -672(%rbp)
	movq	%r9, -488(%rbp)
	call	_ZN12v8_inspector11ValueMirror6createEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE
	movq	-336(%rbp), %rax
	cmpq	$0, -584(%rbp)
	movq	-672(%rbp), %r10
	movzbl	-712(%rbp), %ecx
	movq	%rax, -624(%rbp)
	je	.L3490
.L3425:
	movq	-664(%rbp), %rdi
	movq	%r10, -712(%rbp)
	movb	%cl, -672(%rbp)
	call	_ZNK2v85Value8IsSymbolEv@PLT
	movzbl	-672(%rbp), %ecx
	movq	-712(%rbp), %r10
	testb	%al, %al
	jne	.L3625
.L3431:
	leaq	-240(%rbp), %r8
	leaq	.LC61(%rip), %rsi
	movq	%r10, -736(%rbp)
	movq	%r8, %rdi
	movb	%cl, -712(%rbp)
	movq	%r8, -672(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-672(%rbp), %r8
	movq	-680(%rbp), %rdi
	movq	%r8, %rsi
	call	_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_
	movzbl	-712(%rbp), %ecx
	movq	-736(%rbp), %r10
	testl	%eax, %eax
	je	.L3446
	cmpb	$0, -575(%rbp)
	je	.L3446
	cmpb	$0, -720(%rbp)
	jne	.L3435
.L3446:
	movq	-240(%rbp), %rdi
	leaq	-224(%rbp), %rax
	xorl	%r12d, %r12d
	cmpq	%rax, %rdi
	je	.L3434
	movb	%cl, -672(%rbp)
	xorl	%r12d, %r12d
	call	_ZdlPv@PLT
	movzbl	-672(%rbp), %ecx
	.p2align 4,,10
	.p2align 3
.L3434:
	movq	%r13, %rdi
	movb	%cl, -672(%rbp)
	call	_ZN2v88TryCatchD1Ev@PLT
	movzbl	-672(%rbp), %ecx
	jmp	.L3493
	.p2align 4,,10
	.p2align 3
.L3621:
	movdqa	-224(%rbp), %xmm3
	movaps	%xmm3, -128(%rbp)
	jmp	.L3466
	.p2align 4,,10
	.p2align 3
.L3619:
	movq	-656(%rbp), %rsi
	leaq	-144(%rbp), %r14
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	_ZN12v8_inspector16toProtocolStringEPN2v87IsolateENS0_5LocalINS0_6StringEEE@PLT
	movq	-680(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	movq	-112(%rbp), %rax
	movq	-144(%rbp), %rdi
	movq	%rax, -256(%rbp)
	leaq	-128(%rbp), %rax
	movq	%rax, -616(%rbp)
	cmpq	%rax, %rdi
	je	.L3393
	call	_ZdlPv@PLT
.L3393:
	movq	$0, -632(%rbp)
	jmp	.L3394
	.p2align 4,,10
	.p2align 3
.L3620:
	movq	%rbx, %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	leaq	-336(%rbp), %r10
	movq	%r10, %rdi
	movq	%rax, %rsi
	movq	%r10, -600(%rbp)
	movq	%rax, -584(%rbp)
	call	_ZN2v88TryCatchC1EPNS_7IsolateE@PLT
	movq	-584(%rbp), %rdi
	call	_ZN2v86Object3NewEPNS_7IsolateE@PLT
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, -592(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-584(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	-592(%rbp), %rdi
	movq	%r12, %rcx
	movq	%rbx, %rsi
	movq	%rax, %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-144(%rbp), %rdi
	cmpq	-616(%rbp), %rdi
	movq	-600(%rbp), %r10
	je	.L3400
	movq	%r10, -624(%rbp)
	movb	%al, -600(%rbp)
	call	_ZdlPv@PLT
	movq	-624(%rbp), %r10
	movzbl	-600(%rbp), %eax
.L3400:
	testb	%al, %al
	jne	.L3401
.L3405:
	movq	$0, -384(%rbp)
.L3402:
	movq	%r10, %rdi
	movq	%r10, -600(%rbp)
	call	_ZN2v88TryCatchD1Ev@PLT
	movq	-384(%rbp), %rax
	movq	-560(%rbp), %rdi
	movq	%rax, -584(%rbp)
	movq	(%rdi), %rax
	call	*56(%rax)
	movq	-600(%rbp), %r10
	movl	%eax, %r8d
	movl	%r13d, %eax
	notl	%eax
	andl	$1, %eax
	movb	%al, -592(%rbp)
	movl	%r13d, %eax
	shrl	$2, %r13d
	shrl	%eax
	xorl	$1, %r13d
	xorl	$1, %eax
	andl	$1, %eax
	movb	%al, -571(%rbp)
	movl	%r13d, %eax
	andl	$1, %eax
	testb	%r8b, %r8b
	movb	%al, -570(%rbp)
	jne	.L3503
	cmpq	$0, -584(%rbp)
	je	.L3476
	movq	$0, -600(%rbp)
	xorl	%r12d, %r12d
	movq	$0, -624(%rbp)
	jmp	.L3415
	.p2align 4,,10
	.p2align 3
.L3422:
	testq	%r9, %r9
	jne	.L3491
	movzbl	-573(%rbp), %eax
.L3492:
	movb	%cl, -720(%rbp)
	movl	%eax, %ecx
	movq	%r9, -584(%rbp)
	movq	$0, -624(%rbp)
	jmp	.L3425
	.p2align 4,,10
	.p2align 3
.L3401:
	leaq	.LC1(%rip), %rsi
	movq	%r14, %rdi
	movq	%r10, -600(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-584(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	-592(%rbp), %rdi
	movq	-664(%rbp), %rcx
	movq	%rbx, %rsi
	movq	%rax, %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-144(%rbp), %rdi
	cmpq	-616(%rbp), %rdi
	movq	-600(%rbp), %r10
	je	.L3403
	movb	%al, -584(%rbp)
	call	_ZdlPv@PLT
	movq	-600(%rbp), %r10
	movzbl	-584(%rbp), %eax
.L3403:
	testb	%al, %al
	je	.L3405
	movq	-592(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	leaq	_ZN12v8_inspector12_GLOBAL__N_120nativeGetterCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rbx, %rdi
	movq	%r10, -584(%rbp)
	call	_ZN2v88Function3NewENS_5LocalINS_7ContextEEEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS1_IS5_EEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	-584(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L3405
	leaq	-384(%rbp), %rdi
	movq	%rbx, %rsi
	movq	%r10, -584(%rbp)
	call	_ZN12v8_inspector11ValueMirror6createEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE
	movq	-584(%rbp), %r10
	jmp	.L3402
	.p2align 4,,10
	.p2align 3
.L3409:
	leaq	.LC1(%rip), %rsi
	movq	%r14, %rdi
	movq	%r10, -624(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	-600(%rbp), %rdi
	movq	-664(%rbp), %rcx
	movq	%rbx, %rsi
	movq	%rax, %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-144(%rbp), %rdi
	cmpq	-616(%rbp), %rdi
	movq	-624(%rbp), %r10
	movl	%eax, %r12d
	je	.L3411
	call	_ZdlPv@PLT
	movq	-624(%rbp), %r10
.L3411:
	testb	%r12b, %r12b
	je	.L3413
	movq	-600(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%rbx, %rdi
	movl	$1, %ecx
	leaq	_ZN12v8_inspector12_GLOBAL__N_120nativeSetterCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%r10, -624(%rbp)
	call	_ZN2v88Function3NewENS_5LocalINS_7ContextEEEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS1_IS5_EEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	-624(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L3413
	leaq	-384(%rbp), %rdi
	movq	%rbx, %rsi
	movq	%r10, -600(%rbp)
	call	_ZN12v8_inspector11ValueMirror6createEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE
	movq	-600(%rbp), %r10
	jmp	.L3410
	.p2align 4,,10
	.p2align 3
.L3369:
	movq	%rbx, %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%rax, %rdi
	call	_ZN2v85debug12GetInspectorEPNS_7IsolateE@PLT
	movq	%rbx, %rdi
	movq	%rax, %r12
	call	_ZN12v8_inspector16InspectedContext9contextIdEN2v85LocalINS1_7ContextEEE@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZNK12v8_inspector15V8InspectorImpl10getContextEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3607
	movq	-664(%rbp), %r14
	movq	%r14, %rsi
	call	_ZN12v8_inspector16InspectedContext15getInternalTypeEN2v85LocalINS1_6ObjectEEE@PLT
	movl	%eax, -700(%rbp)
	cmpl	$2, %eax
	jne	.L3372
	leaq	-144(%rbp), %r12
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-656(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, -664(%rbp)
	testq	%rax, %rax
	je	.L3373
	movq	%rax, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	movq	-144(%rbp), %rdi
	movb	%al, -569(%rbp)
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3500
	call	_ZdlPv@PLT
.L3500:
	cmpb	$0, -569(%rbp)
	jne	.L3370
	jmp	.L3375
	.p2align 4,,10
	.p2align 3
.L3385:
	movq	-560(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3472
	movq	(%rdi), %rax
	call	*8(%rax)
.L3472:
	cmpl	$3, -700(%rbp)
	setne	%al
	andb	-704(%rbp), %al
	movb	%al, -569(%rbp)
	jne	.L3473
.L3474:
	movb	$1, -569(%rbp)
.L3375:
	movq	-688(%rbp), %rdi
	call	_ZN2v815MicrotasksScopeD1Ev@PLT
	movq	-696(%rbp), %rdi
	call	_ZN2v88TryCatchD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3626
	movzbl	-569(%rbp), %eax
	addq	$696, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3622:
	.cfi_restore_state
	call	_ZN12v8_inspector14PropertyMirrorD1Ev
	movq	-608(%rbp), %rdi
	call	_ZN2v88TryCatchD1Ev@PLT
	movq	-288(%rbp), %rdi
	cmpq	-640(%rbp), %rdi
	je	.L3388
	call	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L3388:
	movq	-560(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3375
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L3375
	.p2align 4,,10
	.p2align 3
.L3417:
	movq	%r13, %rdi
	movq	%r10, -584(%rbp)
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	movq	-584(%rbp), %r10
	movq	%rbx, %rsi
	movq	%rax, %rdx
	movq	%r10, %rdi
	call	_ZN12v8_inspector11ValueMirror6createEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE
	movb	$0, -570(%rbp)
	movq	-336(%rbp), %r12
	movb	$0, -571(%rbp)
	movzbl	-573(%rbp), %ecx
	movb	$0, -592(%rbp)
	movq	$0, -624(%rbp)
	movq	$0, -584(%rbp)
	movq	$0, -600(%rbp)
	jmp	.L3434
.L3372:
	cmpl	$3, -700(%rbp)
	jne	.L3370
	leaq	-144(%rbp), %r12
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-656(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movq	%rax, %rdx
	call	_ZN2v83Set3AddENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L3373
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3370
	call	_ZdlPv@PLT
	jmp	.L3370
	.p2align 4,,10
	.p2align 3
.L3617:
	movq	-664(%rbp), %r13
	movq	%r13, %rdi
	call	_ZNK2v817SharedArrayBuffer10ByteLengthEv@PLT
	movq	%rax, %r14
	cmpq	$2147483647, %rax
	ja	.L3382
	movq	-648(%rbp), %rax
	leaq	-144(%rbp), %r12
	leaq	.LC57(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rcx
	movq	%rcx, -568(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%r14, %rdx
	movq	%r13, -664(%rbp)
	leaq	-96(%rbp), %r13
	movl	$16777216, -104(%rbp)
	movb	$0, -100(%rbp)
	call	_ZN2v89Int8Array3NewENS_5LocalINS_17SharedArrayBufferEEEmm@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN12v8_inspector11ValueMirror6createEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	-568(%rbp), %rcx
	movups	%xmm0, -88(%rbp)
	movq	-648(%rbp), %rdi
	movups	%xmm0, -72(%rbp)
	call	*%rcx
	movq	%r12, %rdi
	call	_ZN12v8_inspector14PropertyMirrorD1Ev
	movq	-648(%rbp), %rax
	leaq	.LC58(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rcx
	movq	%rcx, -568(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	xorl	%esi, %esi
	movq	%r14, %rdx
	movb	$0, -100(%rbp)
	movq	-664(%rbp), %rdi
	movl	$16777216, -104(%rbp)
	call	_ZN2v810Uint8Array3NewENS_5LocalINS_17SharedArrayBufferEEEmm@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN12v8_inspector11ValueMirror6createEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE
	pxor	%xmm0, %xmm0
	movq	-648(%rbp), %rdi
	movq	%r12, %rsi
	movq	-568(%rbp), %rcx
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	call	*%rcx
	movq	%r12, %rdi
	call	_ZN12v8_inspector14PropertyMirrorD1Ev
	testb	$1, %r14b
	jne	.L3382
	movq	-648(%rbp), %rax
	leaq	.LC59(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rcx
	movq	%rcx, -568(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movb	$0, -100(%rbp)
	movq	-664(%rbp), %rdi
	shrq	%rdx
	movl	$16777216, -104(%rbp)
	call	_ZN2v810Int16Array3NewENS_5LocalINS_17SharedArrayBufferEEEmm@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN12v8_inspector11ValueMirror6createEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE
	pxor	%xmm0, %xmm0
	movq	-648(%rbp), %rdi
	movq	%r12, %rsi
	movq	-568(%rbp), %rcx
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	call	*%rcx
	movq	%r12, %rdi
	call	_ZN12v8_inspector14PropertyMirrorD1Ev
	testb	$3, %r14b
	jne	.L3382
	movq	-648(%rbp), %rax
	movq	%r12, %rdi
	leaq	.LC60(%rip), %rsi
	movq	(%rax), %rax
	movq	16(%rax), %rcx
	movq	%rcx, -568(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movb	$0, -100(%rbp)
	movq	-664(%rbp), %rdi
	shrq	$2, %rdx
	movl	$16777216, -104(%rbp)
	call	_ZN2v810Int32Array3NewENS_5LocalINS_17SharedArrayBufferEEEmm@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN12v8_inspector11ValueMirror6createEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE
	pxor	%xmm0, %xmm0
	movq	-648(%rbp), %rdi
	movq	%r12, %rsi
	movq	-568(%rbp), %rcx
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	call	*%rcx
	movq	%r12, %rdi
	call	_ZN12v8_inspector14PropertyMirrorD1Ev
	jmp	.L3382
	.p2align 4,,10
	.p2align 3
.L3616:
	movq	-664(%rbp), %r13
	movq	%r13, %rdi
	call	_ZNK2v811ArrayBuffer10ByteLengthEv@PLT
	movq	%rax, %r14
	cmpq	$2147483647, %rax
	ja	.L3379
	movq	-648(%rbp), %rax
	leaq	-144(%rbp), %r12
	leaq	.LC57(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rcx
	movq	%rcx, -568(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%r14, %rdx
	movq	%r13, -664(%rbp)
	leaq	-96(%rbp), %r13
	movl	$16777216, -104(%rbp)
	movb	$0, -100(%rbp)
	call	_ZN2v89Int8Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN12v8_inspector11ValueMirror6createEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	-568(%rbp), %rcx
	movups	%xmm0, -88(%rbp)
	movq	-648(%rbp), %rdi
	movups	%xmm0, -72(%rbp)
	call	*%rcx
	movq	%r12, %rdi
	call	_ZN12v8_inspector14PropertyMirrorD1Ev
	movq	-648(%rbp), %rax
	leaq	.LC58(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rcx
	movq	%rcx, -568(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	xorl	%esi, %esi
	movq	%r14, %rdx
	movb	$0, -100(%rbp)
	movq	-664(%rbp), %rdi
	movl	$16777216, -104(%rbp)
	call	_ZN2v810Uint8Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN12v8_inspector11ValueMirror6createEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE
	pxor	%xmm0, %xmm0
	movq	-648(%rbp), %rdi
	movq	%r12, %rsi
	movq	-568(%rbp), %rcx
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	call	*%rcx
	movq	%r12, %rdi
	call	_ZN12v8_inspector14PropertyMirrorD1Ev
	testb	$1, %r14b
	jne	.L3379
	movq	-648(%rbp), %rax
	leaq	.LC59(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rcx
	movq	%rcx, -568(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movb	$0, -100(%rbp)
	movq	-664(%rbp), %rdi
	shrq	%rdx
	movl	$16777216, -104(%rbp)
	call	_ZN2v810Int16Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN12v8_inspector11ValueMirror6createEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE
	pxor	%xmm0, %xmm0
	movq	-648(%rbp), %rdi
	movq	%r12, %rsi
	movq	-568(%rbp), %rcx
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	call	*%rcx
	movq	%r12, %rdi
	call	_ZN12v8_inspector14PropertyMirrorD1Ev
	testb	$3, %r14b
	jne	.L3379
	movq	-648(%rbp), %rax
	movq	%r12, %rdi
	leaq	.LC60(%rip), %rsi
	movq	(%rax), %rax
	movq	16(%rax), %rcx
	movq	%rcx, -568(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movb	$0, -100(%rbp)
	movq	-664(%rbp), %rdi
	shrq	$2, %rdx
	movl	$16777216, -104(%rbp)
	call	_ZN2v810Int32Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN12v8_inspector11ValueMirror6createEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE
	pxor	%xmm0, %xmm0
	movq	-648(%rbp), %rdi
	movq	%r12, %rsi
	movq	-568(%rbp), %rcx
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	call	*%rcx
	movq	%r12, %rdi
	call	_ZN12v8_inspector14PropertyMirrorD1Ev
	jmp	.L3379
	.p2align 4,,10
	.p2align 3
.L3625:
	leaq	.LC52(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-680(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_
	movq	-144(%rbp), %rdi
	movzbl	-672(%rbp), %ecx
	testl	%eax, %eax
	movq	-712(%rbp), %r10
	jne	.L3427
	cmpq	-616(%rbp), %rdi
	je	.L3453
	call	_ZdlPv@PLT
	movzbl	-672(%rbp), %ecx
	movq	-712(%rbp), %r10
	.p2align 4,,10
	.p2align 3
.L3453:
	movq	-656(%rbp), %rsi
	movq	%r10, %rdi
	movb	%cl, -712(%rbp)
	movq	%r10, -672(%rbp)
	call	_ZN2v88TryCatchC1EPNS_7IsolateE@PLT
	movq	-664(%rbp), %rdi
	movq	%r12, %rdx
	movq	%rbx, %rsi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	-672(%rbp), %r10
	movzbl	-712(%rbp), %ecx
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L3459
	leaq	-552(%rbp), %rdi
	movq	%rbx, %rsi
	movq	%r10, -672(%rbp)
	movb	%cl, -568(%rbp)
	call	_ZN12v8_inspector11ValueMirror6createEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE
	cmpq	$0, -600(%rbp)
	movq	-552(%rbp), %r12
	movq	$0, -552(%rbp)
	movzbl	-568(%rbp), %ecx
	movq	-672(%rbp), %r10
	je	.L3457
	movq	-600(%rbp), %rdi
	movq	%r10, -672(%rbp)
	movb	%cl, -568(%rbp)
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	-552(%rbp), %rdi
	movzbl	-568(%rbp), %ecx
	movq	-672(%rbp), %r10
	testq	%rdi, %rdi
	je	.L3457
	movq	(%rdi), %rax
	movq	%r10, -600(%rbp)
	call	*8(%rax)
	movq	-600(%rbp), %r10
	movzbl	-568(%rbp), %ecx
.L3457:
	movq	-624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3458
	movq	(%rdi), %rax
	movq	%r10, -600(%rbp)
	movb	%cl, -568(%rbp)
	call	*8(%rax)
	movq	-600(%rbp), %r10
	movzbl	-568(%rbp), %ecx
.L3458:
	movq	-584(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3508
	movq	(%rdi), %rax
	movq	%r10, -712(%rbp)
	movb	%cl, -672(%rbp)
	call	*8(%rax)
	movzbl	-574(%rbp), %eax
	movq	%r12, -600(%rbp)
	movzbl	-672(%rbp), %ecx
	movq	-712(%rbp), %r10
	movq	$0, -624(%rbp)
	movb	%al, -568(%rbp)
	movq	$0, -584(%rbp)
.L3459:
	movq	%r10, %rdi
	movb	%cl, -672(%rbp)
	xorl	%r12d, %r12d
	call	_ZN2v88TryCatchD1Ev@PLT
	movzbl	-672(%rbp), %ecx
	jmp	.L3434
	.p2align 4,,10
	.p2align 3
.L3427:
	cmpq	-616(%rbp), %rdi
	je	.L3431
	movq	%r10, -712(%rbp)
	movb	%cl, -672(%rbp)
	call	_ZdlPv@PLT
	movq	-712(%rbp), %r10
	movzbl	-672(%rbp), %ecx
	jmp	.L3431
	.p2align 4,,10
	.p2align 3
.L3373:
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3610
	call	_ZdlPv@PLT
.L3610:
	movb	$0, -569(%rbp)
	jmp	.L3375
	.p2align 4,,10
	.p2align 3
.L3476:
	cmpb	$0, -573(%rbp)
	je	.L3627
	movq	-608(%rbp), %rdi
	call	_ZN2v88TryCatchD1Ev@PLT
	jmp	.L3463
	.p2align 4,,10
	.p2align 3
.L3615:
	movq	-664(%rbp), %rsi
	call	*%rax
	movb	%al, -575(%rbp)
	jmp	.L3378
	.p2align 4,,10
	.p2align 3
.L3473:
	movq	-664(%rbp), %rdi
	call	_ZNK2v85Value7IsProxyEv@PLT
	testb	%al, %al
	jne	.L3474
	cmpb	$0, -724(%rbp)
	jne	.L3474
	movq	-664(%rbp), %rdi
	call	_ZN2v86Object12GetPrototypeEv@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L3375
	movq	-648(%rbp), %r15
	leaq	-144(%rbp), %r13
	leaq	.LC61(%rip), %rsi
	movq	%r13, %rdi
	movq	(%r15), %rax
	movq	16(%rax), %r14
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	leaq	-96(%rbp), %rdi
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movl	$16777473, -104(%rbp)
	movb	$0, -100(%rbp)
	call	_ZN12v8_inspector11ValueMirror6createEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	movq	%r13, %rsi
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	call	*%r14
	movq	%r13, %rdi
	call	_ZN12v8_inspector14PropertyMirrorD1Ev
	jmp	.L3375
	.p2align 4,,10
	.p2align 3
.L3623:
	movq	%r8, %rdi
	movq	%r10, -712(%rbp)
	movq	%r9, -672(%rbp)
	movb	%cl, -624(%rbp)
	call	_ZNK2v88Function8ScriptIdEv@PLT
	movq	-672(%rbp), %r9
	movq	-712(%rbp), %r10
	testl	%eax, %eax
	movzbl	-624(%rbp), %ecx
	sete	-720(%rbp)
	testq	%r9, %r9
	jne	.L3424
.L3624:
	movq	-584(%rbp), %r9
	movzbl	-720(%rbp), %ecx
	testq	%r9, %r9
	sete	%al
	andb	-573(%rbp), %al
	jmp	.L3492
	.p2align 4,,10
	.p2align 3
.L3491:
	movq	%r10, %rdi
	movq	%r9, %rdx
	movq	%rbx, %rsi
	movq	%r10, -584(%rbp)
	movq	%r9, -488(%rbp)
	call	_ZN12v8_inspector11ValueMirror6createEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE
	movq	-336(%rbp), %rax
	movb	$0, -720(%rbp)
	movq	-584(%rbp), %r10
	movq	%rax, -624(%rbp)
.L3490:
	cmpq	$0, -624(%rbp)
	movq	$0, -584(%rbp)
	sete	%cl
	andb	-573(%rbp), %cl
	jmp	.L3425
	.p2align 4,,10
	.p2align 3
.L3618:
	movb	$0, -569(%rbp)
	jmp	.L3388
.L3627:
	movq	$0, -600(%rbp)
	xorl	%r12d, %r12d
	movq	$0, -584(%rbp)
	movq	$0, -624(%rbp)
	jmp	.L3415
.L3435:
	movq	(%r12), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L3438
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L3438
	movq	%rbx, %rdi
	movq	%r10, -720(%rbp)
	movb	%cl, -712(%rbp)
	call	_ZN2v87Context10GetIsolateEv@PLT
	leaq	.LC62(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, -672(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-672(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v86String12StringEqualsENS_5LocalIS0_EE@PLT
	movq	-144(%rbp), %rdi
	cmpq	-616(%rbp), %rdi
	movzbl	-712(%rbp), %ecx
	movq	-720(%rbp), %r10
	je	.L3440
	movq	%r10, -736(%rbp)
	movb	%al, -720(%rbp)
	call	_ZdlPv@PLT
	movq	-736(%rbp), %r10
	movzbl	-720(%rbp), %eax
	movzbl	-712(%rbp), %ecx
.L3440:
	testb	%al, %al
	je	.L3438
	movq	-672(%rbp), %rsi
	movq	%r10, %rdi
	movb	%cl, -736(%rbp)
	movq	%r10, -720(%rbp)
	call	_ZN2v88TryCatchC1EPNS_7IsolateE@PLT
	movq	%rbx, %rdi
	call	_ZN2v87Context6GlobalEv@PLT
	leaq	.LC63(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, -712(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-672(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	-712(%rbp), %r8
	movq	%rbx, %rsi
	movq	%rax, %rdx
	movq	%r8, %rdi
	call	_ZN2v86Object20GetRealNamedPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEE@PLT
	movq	-720(%rbp), %r10
	movzbl	-736(%rbp), %ecx
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L3628
	movq	-144(%rbp), %rdi
	cmpq	-616(%rbp), %rdi
	je	.L3485
	movq	%r10, -736(%rbp)
	movb	%cl, -712(%rbp)
	movq	%rax, -720(%rbp)
	call	_ZdlPv@PLT
	movzbl	-712(%rbp), %ecx
	movq	-720(%rbp), %rdx
	movq	-736(%rbp), %r10
.L3485:
	movq	%rdx, %rdi
	movq	%r10, -736(%rbp)
	movb	%cl, -720(%rbp)
	movq	%rdx, -712(%rbp)
	call	_ZNK2v85Value8IsObjectEv@PLT
	movq	-712(%rbp), %rdx
	movzbl	-720(%rbp), %ecx
	testb	%al, %al
	movq	-736(%rbp), %r10
	jne	.L3629
.L3444:
	movq	%r10, %rdi
	movb	%cl, -720(%rbp)
	movq	%r10, -712(%rbp)
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	movq	-712(%rbp), %r10
	movzbl	-720(%rbp), %ecx
	testb	%al, %al
	jne	.L3630
.L3447:
	movq	%rbx, %rdi
	movq	%r10, -736(%rbp)
	movb	%cl, -720(%rbp)
	call	_ZN2v87Context6GlobalEv@PLT
	leaq	.LC64(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, -712(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-672(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	-712(%rbp), %r8
	movq	%rbx, %rsi
	movq	%rax, %rdx
	movq	%r8, %rdi
	call	_ZN2v86Object20GetRealNamedPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEE@PLT
	movzbl	-720(%rbp), %ecx
	movq	-736(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L3631
	movq	-144(%rbp), %rdi
	cmpq	-616(%rbp), %rdi
	je	.L3482
	movq	%r10, -720(%rbp)
	movb	%cl, -672(%rbp)
	movq	%rax, -712(%rbp)
	call	_ZdlPv@PLT
	movzbl	-672(%rbp), %ecx
	movq	-712(%rbp), %rdx
	movq	-720(%rbp), %r10
.L3482:
	movq	%rdx, %rdi
	movq	%r10, -720(%rbp)
	movb	%cl, -712(%rbp)
	movq	%rdx, -672(%rbp)
	call	_ZNK2v85Value8IsObjectEv@PLT
	movq	-672(%rbp), %rdx
	movzbl	-712(%rbp), %ecx
	testb	%al, %al
	movq	-720(%rbp), %r10
	jne	.L3450
.L3479:
	movq	%r10, %rdi
	movb	%cl, -712(%rbp)
	movq	%r10, -672(%rbp)
	call	_ZN2v88TryCatchD1Ev@PLT
	movq	-672(%rbp), %r10
	movzbl	-712(%rbp), %ecx
.L3438:
	movq	-240(%rbp), %rdi
	leaq	-224(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3453
	movq	%r10, -712(%rbp)
	movb	%cl, -672(%rbp)
	call	_ZdlPv@PLT
	movq	-712(%rbp), %r10
	movzbl	-672(%rbp), %ecx
	jmp	.L3453
.L3508:
	movzbl	-574(%rbp), %eax
	movq	%r12, -600(%rbp)
	movq	$0, -624(%rbp)
	movb	%al, -568(%rbp)
	jmp	.L3459
.L3450:
	movq	-664(%rbp), %rdi
	movq	%rbx, %rsi
	movq	%r10, -712(%rbp)
	movb	%cl, -672(%rbp)
	call	_ZN2v85Value10InstanceOfENS_5LocalINS_7ContextEEENS1_INS_6ObjectEEE@PLT
	movzbl	-672(%rbp), %ecx
	movq	-712(%rbp), %r10
	testb	%al, %al
	je	.L3479
	shrw	$8, %ax
	je	.L3479
	movq	%r10, %rdi
	call	_ZN2v88TryCatchD1Ev@PLT
	movzbl	-672(%rbp), %ecx
	jmp	.L3446
	.p2align 4,,10
	.p2align 3
.L3630:
	movq	%r10, %rdi
	call	_ZN2v88TryCatch5ResetEv@PLT
	movzbl	-720(%rbp), %ecx
	movq	-712(%rbp), %r10
	jmp	.L3447
.L3629:
	movq	-664(%rbp), %rdi
	movq	%rbx, %rsi
	movq	%r10, -720(%rbp)
	movb	%cl, -712(%rbp)
	call	_ZN2v85Value10InstanceOfENS_5LocalINS_7ContextEEENS1_INS_6ObjectEEE@PLT
	movzbl	-712(%rbp), %ecx
	movq	-720(%rbp), %r10
	testb	%al, %al
	je	.L3444
	shrw	$8, %ax
	je	.L3444
	movq	%r10, %rdi
	movb	%cl, -672(%rbp)
	call	_ZN2v88TryCatchD1Ev@PLT
	movzbl	-672(%rbp), %ecx
	jmp	.L3446
.L3631:
	movq	-144(%rbp), %rdi
	cmpq	-616(%rbp), %rdi
	je	.L3479
	movq	%r10, -712(%rbp)
	movb	%cl, -672(%rbp)
	call	_ZdlPv@PLT
	movzbl	-672(%rbp), %ecx
	movq	-712(%rbp), %r10
	jmp	.L3479
.L3628:
	movq	-144(%rbp), %rdi
	cmpq	-616(%rbp), %rdi
	je	.L3444
	movq	%r10, -720(%rbp)
	movb	%cl, -712(%rbp)
	call	_ZdlPv@PLT
	movzbl	-712(%rbp), %ecx
	movq	-720(%rbp), %r10
	jmp	.L3444
.L3626:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8027:
	.size	_ZN12v8_inspector11ValueMirror13getPropertiesEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEEbbPNS0_19PropertyAccumulatorE, .-_ZN12v8_inspector11ValueMirror13getPropertiesEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEEbbPNS0_19PropertyAccumulatorE
	.section	.rodata._ZNK12v8_inspector12_GLOBAL__N_112ObjectMirror26buildObjectPreviewInternalEN2v85LocalINS2_7ContextEEEbbPiS6_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteISA_EE.str1.1,"aMS",@progbits,1
.LC65:
	.string	"[[PrimitiveValue]]"
.LC66:
	.string	"[[PromiseStatus]]"
.LC67:
	.string	"[[PromiseValue]]"
.LC68:
	.string	"[[GeneratorStatus]]"
	.section	.text._ZNK12v8_inspector12_GLOBAL__N_112ObjectMirror26buildObjectPreviewInternalEN2v85LocalINS2_7ContextEEEbbPiS6_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteISA_EE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK12v8_inspector12_GLOBAL__N_112ObjectMirror26buildObjectPreviewInternalEN2v85LocalINS2_7ContextEEEbbPiS6_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteISA_EE, @function
_ZNK12v8_inspector12_GLOBAL__N_112ObjectMirror26buildObjectPreviewInternalEN2v85LocalINS2_7ContextEEEbbPiS6_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteISA_EE:
.LFB7996:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$424, %rsp
	movq	16(%rbp), %rax
	movq	%rsi, -360(%rbp)
	movl	%edx, -408(%rbp)
	movq	%r8, -384(%rbp)
	movq	%r9, -392(%rbp)
	movq	%rax, -352(%rbp)
	movb	%cl, -376(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movb	$0, -329(%rbp)
	movq	8(%rbx), %r12
	movq	%rax, -368(%rbp)
	movq	$0, 16(%rax)
	movups	%xmm0, (%rax)
	jmp	.L3634
	.p2align 4,,10
	.p2align 3
.L4039:
	call	_ZN2v85Proxy9GetTargetEv@PLT
	movq	%rax, %r12
.L3634:
	movq	%r12, %rdi
	call	_ZNK2v85Value7IsProxyEv@PLT
	movq	%r12, %rdi
	testb	%al, %al
	jne	.L4039
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	jne	.L3635
.L3637:
	leaq	-224(%rbp), %rax
	xorl	%r12d, %r12d
	movq	%rax, -344(%rbp)
.L3636:
	movq	-344(%rbp), %rdi
	leaq	-144(%rbp), %r14
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreview20ObjectPreviewBuilderILi0EEC1Ev
	movq	_ZN12v8_inspector8protocol7Runtime12RemoteObject8TypeEnum6ObjectE(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-224(%rbp), %r13
	movq	%r14, %rsi
	leaq	-80(%rbp), %r14
	leaq	8(%r13), %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-112(%rbp), %rax
	movq	%rax, 40(%r13)
	movq	16(%rbx), %rsi
	leaq	-96(%rbp), %r13
	movq	24(%rbx), %rax
	movq	-224(%rbp), %r15
	movq	%r13, %rdi
	movq	%r14, -96(%rbp)
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	48(%rbx), %rax
	leaq	104(%r15), %rdi
	movq	%r13, %rsi
	movq	%rax, -64(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-64(%rbp), %rax
	movb	$1, 96(%r15)
	movq	%rax, 136(%r15)
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L3832
	call	_ZdlPv@PLT
.L3832:
	movq	-224(%rbp), %rax
	movzbl	-329(%rbp), %edx
	movq	-368(%rbp), %rcx
	movq	152(%rax), %rdi
	movb	%dl, 144(%rax)
	movq	%rcx, 152(%rax)
	testq	%rdi, %rdi
	je	.L3833
	call	_ZNKSt14default_deleteISt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime15PropertyPreviewES_IS5_EESaIS7_EEEclEPS9_.isra.0.part.0
	movq	-224(%rbp), %rax
.L3833:
	movq	-352(%rbp), %rcx
	movq	$0, -224(%rbp)
	movq	(%rcx), %rdi
	movq	%rax, (%rcx)
	testq	%rdi, %rdi
	je	.L3834
	movq	(%rdi), %rax
	call	*24(%rax)
.L3834:
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3835
	call	_ZdlPv@PLT
.L3835:
	movq	-224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3836
	movq	(%rdi), %rax
	call	*24(%rax)
.L3836:
	cmpb	$0, 56(%rbx)
	jne	.L4040
.L3837:
	testq	%r12, %r12
	je	.L3632
	movq	-352(%rbp), %rax
	movq	(%rax), %rax
	movq	160(%rax), %r13
	movq	%r12, 160(%rax)
	testq	%r13, %r13
	je	.L3632
	movq	8(%r13), %rbx
	movq	0(%r13), %r15
	cmpq	%r15, %rbx
	je	.L3842
	movq	%r13, -344(%rbp)
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %r12
	jmp	.L3849
	.p2align 4,,10
	.p2align 3
.L4042:
	movq	16(%r14), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r14)
	testq	%r13, %r13
	je	.L3845
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%r12, %rax
	jne	.L3846
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3845:
	movq	8(%r14), %r13
	testq	%r13, %r13
	je	.L3847
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%r12, %rax
	jne	.L3848
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3847:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L3843:
	addq	$8, %r15
	cmpq	%r15, %rbx
	je	.L4041
.L3849:
	movq	(%r15), %r14
	testq	%r14, %r14
	je	.L3843
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L4042
	addq	$8, %r15
	movq	%r14, %rdi
	call	*%rax
	cmpq	%r15, %rbx
	jne	.L3849
	.p2align 4,,10
	.p2align 3
.L4041:
	movq	-344(%rbp), %r13
	movq	0(%r13), %r15
.L3842:
	testq	%r15, %r15
	je	.L3850
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L3850:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3632:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4043
	addq	$424, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3635:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v85Value7IsProxyEv@PLT
	testb	%al, %al
	jne	.L3637
	leaq	-256(%rbp), %rax
	movq	-360(%rbp), %rdi
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	%rax, %rdx
	movaps	%xmm0, -320(%rbp)
	movaps	%xmm0, -256(%rbp)
	movq	$0, -304(%rbp)
	movq	$0, -240(%rbp)
	movq	%rax, -416(%rbp)
	call	_ZN12v8_inspector11ValueMirror21getInternalPropertiesEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEEPSt6vectorINS_22InternalPropertyMirrorESaIS8_EE
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	$0, -208(%rbp)
	movaps	%xmm0, -224(%rbp)
	call	_ZNK2v85Value15IsBooleanObjectEv@PLT
	testb	%al, %al
	jne	.L3641
	movq	%r12, %rdi
	call	_ZNK2v85Value14IsNumberObjectEv@PLT
	testb	%al, %al
	je	.L4044
.L3641:
	movq	-216(%rbp), %r13
	cmpq	-208(%rbp), %r13
	je	.L4045
	leaq	.LC65(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-216(%rbp), %rax
	movq	-224(%rbp), %r8
	leaq	40(%rax), %r14
	leaq	-224(%rbp), %rax
	movq	%r14, -216(%rbp)
	movq	%rax, -344(%rbp)
.L3643:
	movq	-256(%rbp), %r15
	movq	-248(%rbp), %r13
	cmpq	%r13, %r15
	je	.L3657
	leaq	-320(%rbp), %rax
	movq	%rbx, -424(%rbp)
	movq	%r8, %rdi
	movq	%r15, %rbx
	movq	%rax, -400(%rbp)
	movq	%r13, %r15
	movq	-384(%rbp), %r13
	jmp	.L3679
	.p2align 4,,10
	.p2align 3
.L3677:
	movq	%rcx, (%rsi)
	movq	16(%rbx), %rcx
	movq	%rcx, 16(%rsi)
.L3678:
	movq	8(%rbx), %rcx
	movq	%rcx, 8(%rsi)
	movq	%rax, (%rbx)
	xorl	%eax, %eax
	movw	%ax, 16(%rbx)
	movq	32(%rbx), %rax
	movq	$0, 8(%rbx)
	movq	%rax, 32(%rsi)
	movq	40(%rbx), %rax
	movq	$0, 40(%rbx)
	addq	$48, -312(%rbp)
	movq	%rax, 40(%rsi)
.L3658:
	addq	$48, %rbx
	movq	-216(%rbp), %r14
	movq	-224(%rbp), %rdi
	cmpq	%rbx, %r15
	je	.L4046
.L3679:
	movq	%rbx, %rdx
	movq	%r14, %rsi
	call	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPN12v8_inspector8String16ESt6vectorIS3_SaIS3_EEEENS0_5__ops16_Iter_equals_valIKS3_EEET_SD_SD_T0_St26random_access_iterator_tag
	cmpq	%rax, %r14
	je	.L3658
	movl	0(%r13), %eax
	testl	%eax, %eax
	je	.L4047
	subl	$1, %eax
	movq	-312(%rbp), %rsi
	movl	%eax, 0(%r13)
	cmpq	-304(%rbp), %rsi
	je	.L3676
	leaq	16(%rsi), %rax
	movq	%rax, (%rsi)
	movq	(%rbx), %rcx
	leaq	16(%rbx), %rax
	cmpq	%rax, %rcx
	jne	.L3677
	movdqu	16(%rbx), %xmm2
	movups	%xmm2, 16(%rsi)
	jmp	.L3678
	.p2align 4,,10
	.p2align 3
.L4040:
	movq	-352(%rbp), %rax
	movq	64(%rbx), %rsi
	movq	%r13, %rdi
	movq	%r14, -96(%rbp)
	movq	(%rax), %r15
	movq	72(%rbx), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	96(%rbx), %rax
	leaq	56(%r15), %rdi
	movq	%r13, %rsi
	movq	%rax, -64(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-64(%rbp), %rax
	movb	$1, 48(%r15)
	movq	%rax, 88(%r15)
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L3837
	call	_ZdlPv@PLT
	jmp	.L3837
	.p2align 4,,10
	.p2align 3
.L3846:
	call	*%rax
	jmp	.L3845
	.p2align 4,,10
	.p2align 3
.L3848:
	call	*%rax
	jmp	.L3847
	.p2align 4,,10
	.p2align 3
.L3676:
	movq	-400(%rbp), %rdi
	movq	%rbx, %rdx
	call	_ZNSt6vectorIN12v8_inspector22InternalPropertyMirrorESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	jmp	.L3658
.L4046:
	movq	-424(%rbp), %rbx
	movq	%rdi, %r8
.L3657:
	cmpq	%r8, %r14
	je	.L3680
	movq	%r8, %r13
	.p2align 4,,10
	.p2align 3
.L3684:
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3681
	call	_ZdlPv@PLT
	addq	$40, %r13
	cmpq	%r14, %r13
	jne	.L3684
.L3682:
	movq	-224(%rbp), %r8
.L3680:
	testq	%r8, %r8
	je	.L3685
	movq	%r8, %rdi
	call	_ZdlPv@PLT
.L3685:
	movq	-248(%rbp), %r13
	movq	-256(%rbp), %r14
	cmpq	%r14, %r13
	je	.L3666
	.p2align 4,,10
	.p2align 3
.L3691:
	movq	40(%r14), %rdi
	testq	%rdi, %rdi
	je	.L3687
	movq	(%rdi), %rax
	call	*8(%rax)
.L3687:
	movq	(%r14), %rdi
	leaq	16(%r14), %rax
	cmpq	%rax, %rdi
	je	.L3688
	call	_ZdlPv@PLT
	addq	$48, %r14
	cmpq	%r14, %r13
	jne	.L3691
.L3669:
	movq	-256(%rbp), %r14
.L3666:
	testq	%r14, %r14
	je	.L3673
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3673:
	movq	-320(%rbp), %rax
	xorl	%r14d, %r14d
	movabsq	$-6148914691236517205, %r13
	cmpq	-312(%rbp), %rax
	je	.L3675
	movq	%r12, -400(%rbp)
	movq	-344(%rbp), %r15
	movq	%rbx, -424(%rbp)
	movq	-368(%rbp), %r12
	movq	%r14, %rbx
	movq	-360(%rbp), %r14
	.p2align 4,,10
	.p2align 3
.L3674:
	leaq	(%rbx,%rbx,2), %rdx
	movq	%r15, %rcx
	movq	%r14, %rsi
	movq	$0, -224(%rbp)
	salq	$4, %rdx
	addq	%rax, %rdx
	movq	40(%rdx), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	-224(%rbp), %rax
	testq	%rax, %rax
	je	.L3696
	movq	8(%r12), %rsi
	cmpq	16(%r12), %rsi
	je	.L3697
	movq	$0, -224(%rbp)
	movq	%rax, (%rsi)
	addq	$8, 8(%r12)
.L3698:
	movq	-224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3696
	movq	(%rdi), %rax
	call	*24(%rax)
.L3696:
	movq	-320(%rbp), %rax
	movq	-312(%rbp), %rcx
	addq	$1, %rbx
	subq	%rax, %rcx
	sarq	$4, %rcx
	imulq	%r13, %rcx
	cmpq	%rbx, %rcx
	ja	.L3674
	movq	-400(%rbp), %r12
	movq	-424(%rbp), %rbx
.L3675:
	movq	-360(%rbp), %rsi
	movq	-416(%rbp), %rdi
	movq	%r12, %rdx
	leaq	-288(%rbp), %r13
	call	_ZN12v8_inspector11ValueMirror20getPrivatePropertiesEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEE
	movq	-256(%rbp), %r15
	movq	-248(%rbp), %r14
	pxor	%xmm0, %xmm0
	movq	$0, -208(%rbp)
	movaps	%xmm0, -224(%rbp)
	cmpq	%r14, %r15
	je	.L3694
	movq	%r12, -424(%rbp)
	movq	-384(%rbp), %r12
	movq	%r14, -400(%rbp)
	movq	-360(%rbp), %r14
	movq	%rbx, -432(%rbp)
	movq	-368(%rbp), %rbx
	jmp	.L3693
	.p2align 4,,10
	.p2align 3
.L4050:
	movq	$0, -288(%rbp)
	movq	%rdi, (%rsi)
	addq	$8, 8(%rbx)
.L3715:
	movq	-288(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3716
	movq	(%rdi), %rdx
	call	*24(%rdx)
.L3716:
	addq	$48, %r15
	cmpq	%r15, -400(%rbp)
	je	.L4048
.L3693:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	$0, -288(%rbp)
	movq	40(%r15), %rdi
	movq	(%rdi), %r8
	call	*24(%r8)
	movq	-288(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3716
	movl	(%r12), %edx
	testl	%edx, %edx
	je	.L4049
	subl	$1, %edx
	movq	8(%rbx), %rsi
	movl	%edx, (%r12)
	cmpq	16(%rbx), %rsi
	jne	.L4050
	movq	%r13, %rdx
	movq	%rbx, %rdi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime15PropertyPreviewESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	jmp	.L3715
	.p2align 4,,10
	.p2align 3
.L3688:
	addq	$48, %r14
	cmpq	%r14, %r13
	jne	.L3691
	jmp	.L3669
	.p2align 4,,10
	.p2align 3
.L3681:
	addq	$40, %r13
	cmpq	%r14, %r13
	jne	.L3684
	jmp	.L3682
.L3697:
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime15PropertyPreviewESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	jmp	.L3698
.L4048:
	movq	-424(%rbp), %r12
	movq	-432(%rbp), %rbx
.L3694:
	movq	-344(%rbp), %rdi
	call	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EED1Ev
	movq	-248(%rbp), %r13
	movq	-256(%rbp), %r14
	cmpq	%r14, %r13
	je	.L3704
	.p2align 4,,10
	.p2align 3
.L3701:
	movq	40(%r14), %rdi
	testq	%rdi, %rdi
	je	.L3717
	movq	(%rdi), %rax
	call	*8(%rax)
.L3717:
	movq	(%r14), %rdi
	leaq	16(%r14), %rax
	cmpq	%rax, %rdi
	je	.L3718
	call	_ZdlPv@PLT
	addq	$48, %r14
	cmpq	%r14, %r13
	jne	.L3701
.L3707:
	movq	-256(%rbp), %r14
.L3704:
	testq	%r14, %r14
	je	.L3711
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3711:
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	$0, -272(%rbp)
	movq	$0, -240(%rbp)
	movq	$0, -328(%rbp)
	movaps	%xmm0, -288(%rbp)
	movaps	%xmm0, -256(%rbp)
	call	_ZNK2v85Value7IsArrayEv@PLT
	testb	%al, %al
	jne	.L3712
	leaq	-328(%rbp), %rax
	movq	-360(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rax, %rdx
	call	_ZN12v8_inspector12_GLOBAL__N_111isArrayLikeEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEEPm
	testb	%al, %al
	je	.L4051
.L3712:
	leaq	-96(%rbp), %r13
	leaq	.LC3(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-416(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE12emplace_backIJS1_EEEvDpOT_
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3727
	call	_ZdlPv@PLT
.L3727:
	movq	%r12, %rdi
	call	_ZNK2v85Value13IsArrayBufferEv@PLT
	testb	%al, %al
	jne	.L3736
	movq	%r12, %rdi
	call	_ZNK2v85Value19IsSharedArrayBufferEv@PLT
	testb	%al, %al
	jne	.L3736
.L3737:
	movq	%r12, %rdi
	call	_ZNK2v85Value14IsStringObjectEv@PLT
	movl	$-1, -432(%rbp)
	testb	%al, %al
	jne	.L4052
.L3741:
	leaq	16+_ZTVN12v8_inspector12_GLOBAL__N_126PreviewPropertyAccumulatorE(%rip), %rax
	movq	-248(%rbp), %r13
	movabsq	$-3689348814741910323, %rcx
	movq	$0, -216(%rbp)
	movq	%rax, -224(%rbp)
	movq	-256(%rbp), %rax
	movq	$0, -208(%rbp)
	movq	%r13, %r14
	subq	%rax, %r14
	movq	%rax, -424(%rbp)
	movq	$0, -200(%rbp)
	movq	%r14, %rax
	sarq	$3, %rax
	imulq	%rcx, %rax
	testq	%rax, %rax
	je	.L4053
	movabsq	$230584300921369395, %rcx
	cmpq	%rcx, %rax
	ja	.L4054
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	-248(%rbp), %r13
	movq	%rax, -400(%rbp)
	movq	-256(%rbp), %rax
	movq	%rax, -424(%rbp)
.L3743:
	movq	-400(%rbp), %r15
	movq	-424(%rbp), %rax
	movq	%r15, %xmm0
	leaq	(%r15,%r14), %rdx
	punpcklqdq	%xmm0, %xmm0
	movq	%rdx, -200(%rbp)
	movups	%xmm0, -216(%rbp)
	cmpq	%r13, %rax
	je	.L3745
	movq	%rax, %r14
	.p2align 4,,10
	.p2align 3
.L3746:
	leaq	16(%r15), %rax
	movq	%r15, %rdi
	addq	$40, %r14
	addq	$40, %r15
	movq	%rax, -40(%r15)
	movq	-32(%r14), %rax
	movq	-40(%r14), %rsi
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-8(%r14), %rax
	movq	%rax, -8(%r15)
	cmpq	%r14, %r13
	jne	.L3746
	movq	-400(%rbp), %rax
	subq	$40, %r13
	subq	-424(%rbp), %r13
	shrq	$3, %r13
	leaq	40(%rax,%r13,8), %rax
	movq	%rax, -400(%rbp)
.L3745:
	movq	-400(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	-384(%rbp), %xmm0
	movq	-344(%rbp), %r8
	movq	%rax, -208(%rbp)
	movl	-432(%rbp), %eax
	movhps	-392(%rbp), %xmm0
	movq	-360(%rbp), %rdi
	movl	%eax, -192(%rbp)
	leaq	-329(%rbp), %rax
	movups	%xmm0, -184(%rbp)
	movq	%rax, %xmm0
	leaq	-288(%rbp), %rax
	movq	%rax, %xmm3
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, -168(%rbp)
	call	_ZN12v8_inspector11ValueMirror13getPropertiesEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEEbbPNS0_19PropertyAccumulatorE
	movq	-208(%rbp), %r15
	movq	-216(%rbp), %r14
	movl	%eax, %r13d
	leaq	16+_ZTVN12v8_inspector12_GLOBAL__N_126PreviewPropertyAccumulatorE(%rip), %rax
	movq	%rax, -224(%rbp)
	cmpq	%r14, %r15
	je	.L3747
	.p2align 4,,10
	.p2align 3
.L3751:
	movq	(%r14), %rdi
	leaq	16(%r14), %rax
	cmpq	%rax, %rdi
	je	.L3748
	call	_ZdlPv@PLT
	addq	$40, %r14
	cmpq	%r14, %r15
	jne	.L3751
.L3749:
	movq	-216(%rbp), %r14
.L3747:
	testq	%r14, %r14
	je	.L3752
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3752:
	movq	-248(%rbp), %r15
	movq	-256(%rbp), %r14
	cmpq	%r14, %r15
	je	.L3753
	.p2align 4,,10
	.p2align 3
.L3757:
	movq	(%r14), %rdi
	leaq	16(%r14), %rax
	cmpq	%rax, %rdi
	je	.L3754
	call	_ZdlPv@PLT
	addq	$40, %r14
	cmpq	%r14, %r15
	jne	.L3757
.L3755:
	movq	-256(%rbp), %r14
.L3753:
	testq	%r14, %r14
	je	.L3758
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3758:
	testb	%r13b, %r13b
	je	.L4033
	movq	-288(%rbp), %rax
	cmpq	-280(%rbp), %rax
	je	.L4033
	leaq	-328(%rbp), %rcx
	movq	%rbx, -440(%rbp)
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
	movq	%rcx, -400(%rbp)
	movq	-368(%rbp), %rbx
	movabsq	$3353953467947191203, %r15
	jmp	.L3776
	.p2align 4,,10
	.p2align 3
.L4056:
	movq	(%rdi), %rax
	movq	-400(%rbp), %rcx
	movq	-360(%rbp), %rsi
	call	*24(%rax)
	cmpb	$0, -376(%rbp)
	je	.L3764
	movq	-288(%rbp), %rax
	movq	-344(%rbp), %rcx
	xorl	%edx, %edx
	movl	$1000, -224(%rbp)
	movq	-416(%rbp), %r9
	movq	-360(%rbp), %rsi
	movq	48(%rax,%r13), %rdi
	movq	%rcx, %r8
	movq	(%rdi), %rax
	call	*32(%rax)
.L3764:
	movq	-256(%rbp), %rax
	testq	%rax, %rax
	je	.L3769
	movq	-328(%rbp), %rdx
	movq	$0, -256(%rbp)
	movq	136(%rdx), %rdi
	movq	%rax, 136(%rdx)
	testq	%rdi, %rdi
	je	.L3769
	movq	(%rdi), %rax
	call	*24(%rax)
.L3769:
	movq	8(%rbx), %rsi
	cmpq	16(%rbx), %rsi
	je	.L3771
	movq	-328(%rbp), %rax
	movq	$0, -328(%rbp)
	movq	%rax, (%rsi)
	addq	$8, 8(%rbx)
.L3772:
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3773
	movq	(%rdi), %rax
	call	*24(%rax)
.L3773:
	movq	-328(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3774
	movq	(%rdi), %rax
	call	*24(%rax)
.L3774:
	movq	-288(%rbp), %rax
	movq	-280(%rbp), %rdx
	addq	$1, %r14
	addq	$88, %r13
	subq	%rax, %rdx
	sarq	$3, %rdx
	imulq	%r15, %rdx
	cmpq	%r14, %rdx
	jbe	.L4055
.L3776:
	movq	$0, -328(%rbp)
	leaq	(%rax,%r13), %rdx
	movq	$0, -256(%rbp)
	movq	48(%rdx), %rdi
	testq	%rdi, %rdi
	jne	.L4056
	movq	-344(%rbp), %rdi
	call	_ZN12v8_inspector8protocol7Runtime15PropertyPreview22PropertyPreviewBuilderILi0EEC1Ev
	movq	-288(%rbp), %rdx
	movq	-224(%rbp), %rax
	addq	%r13, %rdx
	leaq	8(%rax), %rdi
	movq	%rax, -432(%rbp)
	movq	%rdx, %rsi
	movq	%rdx, -424(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-424(%rbp), %rdx
	leaq	-96(%rbp), %r8
	movq	-432(%rbp), %rax
	movq	_ZN12v8_inspector8protocol7Runtime15PropertyPreview8TypeEnum8AccessorE(%rip), %rsi
	movq	%r8, %rdi
	movq	%r8, -432(%rbp)
	movq	32(%rdx), %rdx
	movq	%rdx, 40(%rax)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-224(%rbp), %rax
	movq	-432(%rbp), %r8
	leaq	48(%rax), %rdi
	movq	%r8, %rsi
	movq	%rax, -424(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-424(%rbp), %rax
	movq	-64(%rbp), %rdx
	movq	%rdx, 80(%rax)
	movq	-224(%rbp), %rax
	movq	-328(%rbp), %rdi
	movq	$0, -224(%rbp)
	movq	%rax, -328(%rbp)
	testq	%rdi, %rdi
	je	.L3765
	movq	(%rdi), %rax
	call	*24(%rax)
.L3765:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3766
	call	_ZdlPv@PLT
.L3766:
	movq	-224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3764
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L3764
.L3736:
	leaq	-96(%rbp), %r13
	leaq	.LC57(%rip), %rsi
	movq	%r13, %rdi
	leaq	-80(%rbp), %r14
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-416(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE12emplace_backIJS1_EEEvDpOT_
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L3735
	call	_ZdlPv@PLT
.L3735:
	leaq	.LC58(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-416(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE12emplace_backIJS1_EEEvDpOT_
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L3738
	call	_ZdlPv@PLT
.L3738:
	leaq	.LC59(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-416(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE12emplace_backIJS1_EEEvDpOT_
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L3739
	call	_ZdlPv@PLT
.L3739:
	leaq	.LC60(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-416(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE12emplace_backIJS1_EEEvDpOT_
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L3737
	call	_ZdlPv@PLT
	jmp	.L3737
.L4033:
	leaq	-328(%rbp), %rax
	movq	%rax, -400(%rbp)
.L3762:
	movq	-400(%rbp), %rsi
	movq	%r12, %rdi
	movb	$0, -328(%rbp)
	call	_ZN2v86Object14PreviewEntriesEPb@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L3821
	movq	%rbx, -448(%rbp)
	xorl	%r13d, %r13d
	movq	-360(%rbp), %rbx
	xorl	%r14d, %r14d
	movq	$0, -424(%rbp)
	movq	$0, -376(%rbp)
	jmp	.L3761
	.p2align 4,,10
	.p2align 3
.L3778:
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L3783
	xorl	%r15d, %r15d
.L3782:
	movq	-344(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN12v8_inspector11ValueMirror6createEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE
	movq	%r13, %rdx
	subq	-376(%rbp), %rdx
	movq	-224(%rbp), %r9
	cmpq	$80, %rdx
	je	.L4057
	cmpq	%r13, -424(%rbp)
	je	.L3788
	movq	%r15, %xmm0
	movq	%r9, %xmm1
	addq	$16, %r13
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -16(%r13)
.L3783:
	xorl	%eax, %eax
	cmpb	$0, -328(%rbp)
	setne	%al
	leal	1(%r14,%rax), %r14d
.L3761:
	movq	%r12, %rdi
	call	_ZNK2v85Array6LengthEv@PLT
	cmpl	%r14d, %eax
	jbe	.L3777
	cmpb	$0, -328(%rbp)
	movl	%r14d, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	je	.L3778
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L3856
	movq	-344(%rbp), %rdi
	movq	%rax, %rdx
	movq	%rbx, %rsi
	call	_ZN12v8_inspector11ValueMirror6createEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE
	movq	-224(%rbp), %r15
.L3856:
	cmpb	$0, -328(%rbp)
	je	.L3868
	leal	1(%r14), %edx
.L3780:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	jne	.L3782
	testq	%r15, %r15
	je	.L3783
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*8(%rax)
	jmp	.L3783
	.p2align 4,,10
	.p2align 3
.L3754:
	addq	$40, %r14
	cmpq	%r14, %r15
	jne	.L3757
	jmp	.L3755
	.p2align 4,,10
	.p2align 3
.L3748:
	addq	$40, %r14
	cmpq	%r14, %r15
	jne	.L3751
	jmp	.L3749
	.p2align 4,,10
	.p2align 3
.L3718:
	addq	$48, %r14
	cmpq	%r14, %r13
	jne	.L3701
	jmp	.L3707
.L3777:
	movq	-448(%rbp), %rbx
	cmpq	-376(%rbp), %r13
	jne	.L3802
.L3873:
	xorl	%r12d, %r12d
.L3803:
	movq	-376(%rbp), %rax
	testq	%rax, %rax
	je	.L3821
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L3821:
	movq	-280(%rbp), %r13
	movq	-288(%rbp), %r14
	cmpq	%r14, %r13
	je	.L3822
	.p2align 4,,10
	.p2align 3
.L3823:
	movq	%r14, %rdi
	addq	$88, %r14
	call	_ZN12v8_inspector14PropertyMirrorD1Ev
	cmpq	%r14, %r13
	jne	.L3823
	movq	-288(%rbp), %r14
.L3822:
	testq	%r14, %r14
	je	.L3824
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3824:
	movq	-312(%rbp), %r13
	movq	-320(%rbp), %r14
	cmpq	%r14, %r13
	je	.L3825
	.p2align 4,,10
	.p2align 3
.L3830:
	movq	40(%r14), %rdi
	testq	%rdi, %rdi
	je	.L3826
	movq	(%rdi), %rax
	call	*8(%rax)
.L3826:
	movq	(%r14), %rdi
	leaq	16(%r14), %rax
	cmpq	%rax, %rdi
	je	.L3827
	call	_ZdlPv@PLT
	addq	$48, %r14
	cmpq	%r14, %r13
	jne	.L3830
.L3828:
	movq	-320(%rbp), %r14
.L3825:
	testq	%r14, %r14
	je	.L3636
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	jmp	.L3636
	.p2align 4,,10
	.p2align 3
.L3827:
	addq	$48, %r14
	cmpq	%r14, %r13
	jne	.L3830
	jmp	.L3828
	.p2align 4,,10
	.p2align 3
.L3788:
	movabsq	$576460752303423487, %rcx
	movq	%rdx, %rax
	sarq	$4, %rax
	cmpq	%rcx, %rax
	je	.L3790
	testq	%rax, %rax
	je	.L4058
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	ja	.L4059
	testq	%rcx, %rcx
	jne	.L4060
	movq	$0, -424(%rbp)
	movl	$16, %eax
	movq	$0, -432(%rbp)
.L3793:
	movq	%r15, %xmm0
	movq	-432(%rbp), %rcx
	movq	%r9, %xmm4
	movq	-376(%rbp), %r15
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, (%rcx,%rdx)
	cmpq	%r15, %r13
	je	.L3871
	movq	%rbx, -440(%rbp)
	movq	%rcx, %rbx
	.p2align 4,,10
	.p2align 3
.L3799:
	movq	(%r15), %rax
	movq	$0, (%r15)
	movq	%rax, (%rbx)
	movq	8(%r15), %rax
	movq	$0, 8(%r15)
	movq	%rax, 8(%rbx)
	movq	8(%r15), %rdi
	testq	%rdi, %rdi
	je	.L3795
	movq	(%rdi), %rax
	call	*8(%rax)
.L3795:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L3796
	movq	(%rdi), %rax
	addq	$16, %r15
	addq	$16, %rbx
	call	*8(%rax)
	cmpq	%r13, %r15
	jne	.L3799
.L4035:
	movq	-432(%rbp), %rax
	subq	-376(%rbp), %r13
	movq	-440(%rbp), %rbx
	leaq	16(%rax,%r13), %r13
.L3794:
	movq	-376(%rbp), %rax
	testq	%rax, %rax
	je	.L3800
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L3800:
	movq	-432(%rbp), %rax
	movq	%rax, -376(%rbp)
	jmp	.L3783
.L4059:
	movabsq	$9223372036854775792, %rcx
.L3791:
	movq	%rcx, %rdi
	movq	%r9, -456(%rbp)
	movq	%rdx, -440(%rbp)
	movq	%rcx, -424(%rbp)
	call	_Znwm@PLT
	movq	-424(%rbp), %rcx
	movq	-440(%rbp), %rdx
	movq	%rax, -432(%rbp)
	movq	-456(%rbp), %r9
	addq	%rax, %rcx
	addq	$16, %rax
	movq	%rcx, -424(%rbp)
	jmp	.L3793
.L4044:
	movq	%r12, %rdi
	call	_ZNK2v85Value14IsStringObjectEv@PLT
	testb	%al, %al
	jne	.L3641
	movq	%r12, %rdi
	call	_ZNK2v85Value14IsSymbolObjectEv@PLT
	testb	%al, %al
	jne	.L3641
	movq	%r12, %rdi
	call	_ZNK2v85Value14IsBigIntObjectEv@PLT
	testb	%al, %al
	jne	.L3641
	movq	%r12, %rdi
	call	_ZNK2v85Value9IsPromiseEv@PLT
	testb	%al, %al
	je	.L3655
	leaq	-224(%rbp), %rax
	leaq	.LC66(%rip), %rsi
	movq	%rax, %r15
	movq	%rax, %rdi
	movq	%rax, -344(%rbp)
	call	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE12emplace_backIJRA18_KcEEEvDpOT_
	leaq	.LC67(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE12emplace_backIJRA17_KcEEEvDpOT_
	movq	-216(%rbp), %r14
	movq	-224(%rbp), %r8
	jmp	.L3643
	.p2align 4,,10
	.p2align 3
.L4051:
	movq	%r12, %rdi
	call	_ZNK2v85Value14IsStringObjectEv@PLT
	testb	%al, %al
	jne	.L3712
	movq	-360(%rbp), %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%rax, %rdi
	call	_ZN2v85debug12GetInspectorEPNS_7IsolateE@PLT
	leaq	_ZN12v8_inspector17V8InspectorClient12valueSubtypeEN2v85LocalINS1_5ValueEEE(%rip), %rdx
	movq	16(%rax), %rsi
	movq	(%rsi), %rax
	movq	72(%rax), %rax
	cmpq	%rdx, %rax
	je	.L3727
	movq	-344(%rbp), %rdi
	movq	%r12, %rdx
	call	*%rax
	cmpq	$0, -224(%rbp)
	je	.L3727
	leaq	-144(%rbp), %r14
	leaq	.LC41(%rip), %rsi
	movq	%r14, %rdi
	leaq	-96(%rbp), %r13
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-224(%rbp), %rdi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN12v8_inspector10toString16ERKNS_10StringViewE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	leaq	-80(%rbp), %r14
	call	_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_
	movq	-96(%rbp), %rdi
	movl	%eax, %r15d
	cmpq	%r14, %rdi
	je	.L3729
	call	_ZdlPv@PLT
.L3729:
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3858
	call	_ZdlPv@PLT
.L3858:
	testl	%r15d, %r15d
	jne	.L3731
	leaq	.LC3(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-416(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE12emplace_backIJS1_EEEvDpOT_
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L3731
	call	_ZdlPv@PLT
.L3731:
	movq	-224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3727
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L3727
	.p2align 4,,10
	.p2align 3
.L3796:
	addq	$16, %r15
	addq	$16, %rbx
	cmpq	%r13, %r15
	jne	.L3799
	jmp	.L4035
.L3771:
	movq	-400(%rbp), %rdx
	movq	%rbx, %rdi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime15PropertyPreviewESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	jmp	.L3772
.L4055:
	movq	-440(%rbp), %rbx
	jmp	.L3762
.L4047:
	movq	-216(%rbp), %r13
	movq	-224(%rbp), %r14
	movb	$1, -329(%rbp)
	movq	-424(%rbp), %rbx
	cmpq	%r14, %r13
	je	.L3660
	.p2align 4,,10
	.p2align 3
.L3664:
	movq	(%r14), %rdi
	leaq	16(%r14), %rax
	cmpq	%rax, %rdi
	je	.L3661
	call	_ZdlPv@PLT
	addq	$40, %r14
	cmpq	%r14, %r13
	jne	.L3664
.L3662:
	movq	-224(%rbp), %r14
.L3660:
	testq	%r14, %r14
	je	.L3665
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3665:
	movq	-248(%rbp), %r13
	movq	-256(%rbp), %r14
	cmpq	%r14, %r13
	je	.L3666
	.p2align 4,,10
	.p2align 3
.L3671:
	movq	40(%r14), %rdi
	testq	%rdi, %rdi
	je	.L3667
	movq	(%rdi), %rax
	call	*8(%rax)
.L3667:
	movq	(%r14), %rdi
	leaq	16(%r14), %rax
	cmpq	%rax, %rdi
	je	.L3668
	call	_ZdlPv@PLT
	addq	$48, %r14
	cmpq	%r14, %r13
	jne	.L3671
	jmp	.L3669
	.p2align 4,,10
	.p2align 3
.L3661:
	addq	$40, %r14
	cmpq	%r14, %r13
	jne	.L3664
	jmp	.L3662
	.p2align 4,,10
	.p2align 3
.L3668:
	addq	$48, %r14
	cmpq	%r14, %r13
	jne	.L3671
	jmp	.L3669
.L4049:
	movb	$1, -329(%rbp)
	movq	(%rdi), %rax
	movq	-424(%rbp), %r12
	movq	-432(%rbp), %rbx
	call	*24(%rax)
	movq	-344(%rbp), %rdi
	call	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EED1Ev
	movq	-248(%rbp), %r13
	movq	-256(%rbp), %r14
	cmpq	%r14, %r13
	je	.L3704
	.p2align 4,,10
	.p2align 3
.L3709:
	movq	40(%r14), %rdi
	testq	%rdi, %rdi
	je	.L3705
	movq	(%rdi), %rax
	call	*8(%rax)
.L3705:
	movq	(%r14), %rdi
	leaq	16(%r14), %rax
	cmpq	%rax, %rdi
	je	.L3706
	call	_ZdlPv@PLT
	addq	$48, %r14
	cmpq	%r14, %r13
	jne	.L3709
	jmp	.L3707
	.p2align 4,,10
	.p2align 3
.L3706:
	addq	$48, %r14
	cmpq	%r14, %r13
	jne	.L3709
	jmp	.L3707
.L4052:
	movq	%r12, %rdi
	call	_ZNK2v812StringObject7ValueOfEv@PLT
	movq	%rax, %rdi
	call	_ZNK2v86String6LengthEv@PLT
	addl	$1, %eax
	movl	%eax, -432(%rbp)
	jmp	.L3741
.L4058:
	movl	$16, %ecx
	jmp	.L3791
.L4057:
	movb	$1, -329(%rbp)
	movq	-448(%rbp), %rbx
	testq	%r9, %r9
	je	.L3784
	movq	(%r9), %rax
	movq	%r9, %rdi
	call	*8(%rax)
.L3784:
	testq	%r15, %r15
	je	.L3785
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*8(%rax)
.L3785:
	cmpb	$0, -408(%rbp)
	je	.L4061
	movb	$1, -329(%rbp)
	cmpq	-376(%rbp), %r13
	je	.L3873
.L3804:
	xorl	%r12d, %r12d
.L3816:
	movq	-376(%rbp), %r14
	.p2align 4,,10
	.p2align 3
.L3820:
	movq	8(%r14), %rdi
	testq	%rdi, %rdi
	je	.L3817
	movq	(%rdi), %rax
	call	*8(%rax)
.L3817:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L3818
	movq	(%rdi), %rax
	addq	$16, %r14
	call	*8(%rax)
	cmpq	%r13, %r14
	jne	.L3820
	jmp	.L3803
	.p2align 4,,10
	.p2align 3
.L3818:
	addq	$16, %r14
	cmpq	%r13, %r14
	jne	.L3820
	jmp	.L3803
.L4045:
	movq	-224(%rbp), %rax
	movq	%r13, %r15
	movabsq	$-3689348814741910323, %rdx
	movabsq	$230584300921369395, %rsi
	subq	%rax, %r15
	movq	%rax, -344(%rbp)
	movq	%r15, %rax
	sarq	$3, %rax
	imulq	%rdx, %rax
	cmpq	%rsi, %rax
	je	.L3790
	testq	%rax, %rax
	je	.L3863
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L4062
.L3645:
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	%rax, %r8
	leaq	(%rax,%r14), %rax
	movq	%rax, -424(%rbp)
	leaq	40(%r8), %r14
.L3646:
	leaq	(%r8,%r15), %rdi
	leaq	.LC65(%rip), %rsi
	movq	%r8, -400(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-344(%rbp), %rax
	movq	-400(%rbp), %r8
	cmpq	%rax, %r13
	je	.L3647
	leaq	-40(%r13), %r14
	movq	%rbx, -432(%rbp)
	leaq	16(%rax), %r15
	movabsq	$922337203685477581, %rdx
	subq	%rax, %r14
	movq	%r8, %rbx
	shrq	$3, %r14
	imulq	%rdx, %r14
	movabsq	$2305843009213693951, %rdx
	andq	%rdx, %r14
	leaq	(%r14,%r14,4), %rdx
	movq	%r14, -400(%rbp)
	movq	%r8, %r14
	leaq	56(%rax,%rdx,8), %r13
	.p2align 4,,10
	.p2align 3
.L3653:
	leaq	16(%r14), %rax
	movq	%rax, (%r14)
	movq	-16(%r15), %rax
	cmpq	%r15, %rax
	je	.L4063
	movq	%rax, (%r14)
	movq	(%r15), %rax
	movq	%rax, 16(%r14)
.L3649:
	movq	-8(%r15), %rax
	xorl	%edx, %edx
	movq	%rax, 8(%r14)
	movq	16(%r15), %rax
	movq	%r15, -16(%r15)
	movq	$0, -8(%r15)
	movw	%dx, (%r15)
	movq	%rax, 32(%r14)
	movq	-16(%r15), %rdi
	cmpq	%r15, %rdi
	je	.L3650
	call	_ZdlPv@PLT
	addq	$40, %r15
	addq	$40, %r14
	cmpq	%r13, %r15
	jne	.L3653
.L4030:
	movq	-400(%rbp), %rax
	movq	%rbx, %r8
	movq	-432(%rbp), %rbx
	leaq	10(%rax,%rax,4), %rax
	leaq	(%r8,%rax,8), %r14
.L3647:
	movq	-344(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3654
	movq	%r8, -344(%rbp)
	call	_ZdlPv@PLT
	movq	-344(%rbp), %r8
.L3654:
	movq	-424(%rbp), %rax
	movq	%r8, %xmm0
	movq	%r14, %xmm6
	punpcklqdq	%xmm6, %xmm0
	movq	%rax, -208(%rbp)
	leaq	-224(%rbp), %rax
	movq	%rax, -344(%rbp)
	movaps	%xmm0, -224(%rbp)
	jmp	.L3643
	.p2align 4,,10
	.p2align 3
.L3650:
	addq	$40, %r15
	addq	$40, %r14
	cmpq	%r15, %r13
	jne	.L3653
	jmp	.L4030
.L4063:
	movdqu	(%r15), %xmm5
	movups	%xmm5, 16(%r14)
	jmp	.L3649
.L4053:
	movq	$0, -400(%rbp)
	jmp	.L3743
.L4061:
	movl	$24, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rax)
	movq	%rax, %r12
	movups	%xmm0, (%rax)
	cmpq	-376(%rbp), %r13
	je	.L3803
.L3859:
	movq	%r13, -408(%rbp)
	movq	-376(%rbp), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %r15
	movq	%rbx, -424(%rbp)
	movq	-392(%rbp), %r13
	movq	-384(%rbp), %rbx
	jmp	.L3815
.L4065:
	movq	$0, -224(%rbp)
	movq	%rax, (%rsi)
	addq	$8, 8(%r12)
.L3812:
	movq	-224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3813
	movq	(%rdi), %rax
	call	*24(%rax)
.L3813:
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3814
	movq	(%rdi), %rax
	call	*24(%rax)
.L3814:
	movq	-328(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3806
	movq	(%rdi), %rax
	call	*24(%rax)
.L3806:
	addq	$16, %r14
	cmpq	%r14, -408(%rbp)
	je	.L4064
.L3815:
	movq	8(%r14), %rdi
	movq	%r13, %rcx
	movq	%rbx, %rdx
	movq	$0, -328(%rbp)
	movq	-400(%rbp), %r8
	movq	-360(%rbp), %rsi
	movq	(%rdi), %rax
	call	*40(%rax)
	cmpq	$0, -328(%rbp)
	je	.L3806
	movq	$0, -256(%rbp)
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L3807
	movq	(%rdi), %rax
	movq	-416(%rbp), %r8
	movq	%r13, %rcx
	movq	%rbx, %rdx
	movq	-360(%rbp), %rsi
	call	*40(%rax)
	cmpq	$0, -256(%rbp)
	je	.L3814
.L3807:
	movl	$24, %edi
	call	_Znwm@PLT
	movq	-328(%rbp), %rdx
	movq	$0, -328(%rbp)
	movq	%r15, (%rax)
	movq	%rdx, 16(%rax)
	movq	-256(%rbp), %rdx
	movq	$0, 8(%rax)
	movq	%rax, -224(%rbp)
	testq	%rdx, %rdx
	je	.L3810
	movq	$0, -256(%rbp)
	movq	%rdx, 8(%rax)
.L3810:
	movq	8(%r12), %rsi
	cmpq	16(%r12), %rsi
	jne	.L4065
	movq	-344(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol7Runtime12EntryPreviewESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	jmp	.L3812
.L4064:
	movq	-408(%rbp), %r13
	movq	-424(%rbp), %rbx
	jmp	.L3816
.L4062:
	testq	%rdx, %rdx
	jne	.L4066
	movq	$0, -424(%rbp)
	movl	$40, %r14d
	xorl	%r8d, %r8d
	jmp	.L3646
.L4060:
	movabsq	$576460752303423487, %rax
	cmpq	%rax, %rcx
	cmova	%rax, %rcx
	salq	$4, %rcx
	jmp	.L3791
.L3871:
	movq	%rax, %r13
	jmp	.L3794
.L3863:
	movl	$40, %r14d
	jmp	.L3645
.L3802:
	cmpb	$0, -408(%rbp)
	je	.L4067
	movb	$1, -329(%rbp)
	jmp	.L3804
.L4067:
	movl	$24, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rax)
	movq	%rax, %r12
	movups	%xmm0, (%rax)
	jmp	.L3859
.L3790:
	leaq	.LC15(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L3868:
	movl	%r14d, %edx
	jmp	.L3780
.L4066:
	cmpq	%rsi, %rdx
	cmova	%rsi, %rdx
	imulq	$40, %rdx, %r14
	jmp	.L3645
.L4043:
	call	__stack_chk_fail@PLT
.L3655:
	movq	%r12, %rdi
	call	_ZNK2v85Value17IsGeneratorObjectEv@PLT
	testb	%al, %al
	jne	.L3656
	leaq	-224(%rbp), %rax
	movq	-216(%rbp), %r14
	movq	-224(%rbp), %r8
	movq	%rax, -344(%rbp)
	jmp	.L3643
.L4054:
	call	_ZSt17__throw_bad_allocv@PLT
.L3656:
	leaq	-224(%rbp), %rax
	leaq	.LC68(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -344(%rbp)
	call	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE12emplace_backIJRA20_KcEEEvDpOT_
	movq	-216(%rbp), %r14
	movq	-224(%rbp), %r8
	jmp	.L3643
	.cfi_endproc
.LFE7996:
	.size	_ZNK12v8_inspector12_GLOBAL__N_112ObjectMirror26buildObjectPreviewInternalEN2v85LocalINS2_7ContextEEEbbPiS6_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteISA_EE, .-_ZNK12v8_inspector12_GLOBAL__N_112ObjectMirror26buildObjectPreviewInternalEN2v85LocalINS2_7ContextEEEbbPiS6_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteISA_EE
	.section	.text._ZNK12v8_inspector12_GLOBAL__N_112ObjectMirror17buildEntryPreviewEN2v85LocalINS2_7ContextEEEPiS6_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteISA_EE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK12v8_inspector12_GLOBAL__N_112ObjectMirror17buildEntryPreviewEN2v85LocalINS2_7ContextEEEPiS6_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteISA_EE, @function
_ZNK12v8_inspector12_GLOBAL__N_112ObjectMirror17buildEntryPreviewEN2v85LocalINS2_7ContextEEEPiS6_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteISA_EE:
.LFB7994:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r9
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	%r8
	movq	%rdx, %r8
	movl	$1, %edx
	call	_ZNK12v8_inspector12_GLOBAL__N_112ObjectMirror26buildObjectPreviewInternalEN2v85LocalINS2_7ContextEEEbbPiS6_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteISA_EE
	popq	%rax
	popq	%rdx
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7994:
	.size	_ZNK12v8_inspector12_GLOBAL__N_112ObjectMirror17buildEntryPreviewEN2v85LocalINS2_7ContextEEEPiS6_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteISA_EE, .-_ZNK12v8_inspector12_GLOBAL__N_112ObjectMirror17buildEntryPreviewEN2v85LocalINS2_7ContextEEEPiS6_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteISA_EE
	.section	.text._ZNK12v8_inspector12_GLOBAL__N_112ObjectMirror18buildObjectPreviewEN2v85LocalINS2_7ContextEEEbPiS6_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteISA_EE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK12v8_inspector12_GLOBAL__N_112ObjectMirror18buildObjectPreviewEN2v85LocalINS2_7ContextEEEbPiS6_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteISA_EE, @function
_ZNK12v8_inspector12_GLOBAL__N_112ObjectMirror18buildObjectPreviewEN2v85LocalINS2_7ContextEEEbPiS6_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteISA_EE:
.LFB7993:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r10
	movzbl	%dl, %ecx
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	%r9
	movq	%r8, %r9
	movq	%r10, %r8
	call	_ZNK12v8_inspector12_GLOBAL__N_112ObjectMirror26buildObjectPreviewInternalEN2v85LocalINS2_7ContextEEEbbPiS6_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteISA_EE
	popq	%rax
	popq	%rdx
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7993:
	.size	_ZNK12v8_inspector12_GLOBAL__N_112ObjectMirror18buildObjectPreviewEN2v85LocalINS2_7ContextEEEbPiS6_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteISA_EE, .-_ZNK12v8_inspector12_GLOBAL__N_112ObjectMirror18buildObjectPreviewEN2v85LocalINS2_7ContextEEEbPiS6_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteISA_EE
	.section	.text._ZNK12v8_inspector12_GLOBAL__N_112ObjectMirror17buildRemoteObjectEN2v85LocalINS2_7ContextEEENS_8WrapModeEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISA_EE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK12v8_inspector12_GLOBAL__N_112ObjectMirror17buildRemoteObjectEN2v85LocalINS2_7ContextEEENS_8WrapModeEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISA_EE, @function
_ZNK12v8_inspector12_GLOBAL__N_112ObjectMirror17buildRemoteObjectEN2v85LocalINS2_7ContextEEENS_8WrapModeEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISA_EE:
.LFB7992:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 3, -56
	movl	%ecx, -244(%rbp)
	movq	%r8, -240(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jne	.L4073
	movq	$0, -216(%rbp)
	movq	8(%rsi), %rdx
	movq	(%rdx), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L4122
.L4074:
	leaq	-112(%rbp), %rdi
	leaq	-216(%rbp), %r8
	movl	$1000, %ecx
	movq	%r14, %rsi
	call	_ZN12v8_inspector12_GLOBAL__N_115toProtocolValueEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEEiPSt10unique_ptrINS_8protocol5ValueESt14default_deleteIS9_EE.constprop.0
.L4075:
	movl	-112(%rbp), %eax
	testl	%eax, %eax
	je	.L4076
	movl	%eax, (%r12)
	leaq	24(%r12), %rax
	leaq	-88(%rbp), %rdx
	movq	%rax, 8(%r12)
	movq	-104(%rbp), %rax
	cmpq	%rdx, %rax
	je	.L4123
	movq	%rax, 8(%r12)
	movq	-88(%rbp), %rax
	movq	%rax, 24(%r12)
.L4078:
	movq	-96(%rbp), %rax
	movq	-216(%rbp), %rdi
	movq	%rax, 16(%r12)
	movq	-72(%rbp), %rax
	movq	%rax, 40(%r12)
	movl	-64(%rbp), %eax
	movl	%eax, 48(%r12)
	testq	%rdi, %rdi
	je	.L4072
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L4072
	.p2align 4,,10
	.p2align 3
.L4073:
	movq	%rdx, %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	movl	$320, %edi
	movq	%rax, %r15
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectC1Ev
	leaq	-208(%rbp), %r8
	movq	_ZN12v8_inspector8protocol7Runtime12RemoteObject8TypeEnum6ObjectE(%rip), %rsi
	movq	%r8, %rdi
	movq	%r8, -232(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-232(%rbp), %r8
	leaq	16(%rbx), %rdi
	movq	%r8, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-176(%rbp), %rax
	movq	8(%r13), %rdi
	movq	%rax, 48(%rbx)
	call	_ZN2v86Object18GetConstructorNameEv@PLT
	movq	%r15, %rsi
	leaq	-160(%rbp), %rdi
	leaq	-112(%rbp), %r15
	movq	%rax, %rdx
	call	_ZN12v8_inspector16toProtocolStringEPN2v87IsolateENS0_5LocalINS0_6StringEEE@PLT
	leaq	-96(%rbp), %rax
	movq	-160(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, -232(%rbp)
	movq	%rax, -112(%rbp)
	movq	-152(%rbp), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-128(%rbp), %rax
	leaq	112(%rbx), %rdi
	movq	%r15, %rsi
	movq	%rax, -80(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-80(%rbp), %rax
	movb	$1, 104(%rbx)
	movq	-112(%rbp), %rdi
	movq	%rax, 144(%rbx)
	cmpq	-232(%rbp), %rdi
	je	.L4088
	call	_ZdlPv@PLT
.L4088:
	movq	-232(%rbp), %rax
	movq	16(%r13), %rsi
	movq	%r15, %rdi
	movq	%rax, -112(%rbp)
	movq	24(%r13), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	48(%r13), %rax
	leaq	216(%rbx), %rdi
	movq	%r15, %rsi
	movq	%rax, -80(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-80(%rbp), %rax
	movb	$1, 208(%rbx)
	movq	-112(%rbp), %rdi
	movq	%rax, 248(%rbx)
	cmpq	-232(%rbp), %rdi
	je	.L4089
	call	_ZdlPv@PLT
.L4089:
	movq	-240(%rbp), %rax
	movq	(%rax), %rdi
	movq	%rbx, (%rax)
	testq	%rdi, %rdi
	je	.L4090
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4091
	movq	%rdi, -256(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movq	-256(%rbp), %rdi
	movl	$320, %esi
	call	_ZdlPvm@PLT
.L4090:
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4092
	call	_ZdlPv@PLT
.L4092:
	movq	-208(%rbp), %rdi
	leaq	-192(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4093
	call	_ZdlPv@PLT
.L4093:
	cmpb	$0, 56(%r13)
	jne	.L4124
.L4094:
	cmpl	$2, -244(%rbp)
	je	.L4125
.L4087:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
.L4072:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4126
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4122:
	.cfi_restore_state
	movq	-1(%rax), %rcx
	cmpw	$67, 11(%rcx)
	jne	.L4074
	cmpl	$5, 43(%rax)
	jne	.L4074
	leaq	-112(%rbp), %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	jmp	.L4075
	.p2align 4,,10
	.p2align 3
.L4076:
	movl	$320, %edi
	leaq	-160(%rbp), %r13
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectC1Ev
	movq	_ZN12v8_inspector8protocol7Runtime12RemoteObject8TypeEnum6ObjectE(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	leaq	16(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-128(%rbp), %rax
	movq	152(%rbx), %rdi
	movq	%rax, 48(%rbx)
	movq	-216(%rbp), %rax
	movq	$0, -216(%rbp)
	movq	%rax, 152(%rbx)
	testq	%rdi, %rdi
	je	.L4081
	movq	(%rdi), %rax
	call	*24(%rax)
.L4081:
	movq	-240(%rbp), %rax
	movq	(%rax), %r13
	movq	%rbx, (%rax)
	testq	%r13, %r13
	je	.L4082
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4083
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4082:
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4084
	call	_ZdlPv@PLT
.L4084:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4121
	call	_ZdlPv@PLT
.L4121:
	movq	-216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4087
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L4087
	.p2align 4,,10
	.p2align 3
.L4124:
	movq	-240(%rbp), %rax
	movq	64(%r13), %rsi
	movq	%r15, %rdi
	movq	(%rax), %rbx
	movq	-232(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	72(%r13), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	96(%r13), %rax
	leaq	64(%rbx), %rdi
	movq	%r15, %rsi
	movq	%rax, -80(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-80(%rbp), %rax
	movb	$1, 56(%rbx)
	movq	%rax, 96(%rbx)
	movq	-112(%rbp), %rdi
	cmpq	-232(%rbp), %rdi
	je	.L4094
	call	_ZdlPv@PLT
	jmp	.L4094
	.p2align 4,,10
	.p2align 3
.L4125:
	subq	$8, %rsp
	leaq	-216(%rbp), %rax
	xorl	%edx, %edx
	movq	%r13, %rdi
	pushq	%rax
	leaq	-220(%rbp), %r9
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	leaq	-224(%rbp), %r8
	movq	$0, -216(%rbp)
	movl	$5, -224(%rbp)
	movl	$100, -220(%rbp)
	call	_ZNK12v8_inspector12_GLOBAL__N_112ObjectMirror26buildObjectPreviewInternalEN2v85LocalINS2_7ContextEEEbbPiS6_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteISA_EE
	movq	-240(%rbp), %rax
	movq	-216(%rbp), %rdx
	movq	$0, -216(%rbp)
	movq	(%rax), %rax
	movq	304(%rax), %rdi
	movq	%rdx, 304(%rax)
	popq	%rax
	popq	%rdx
	testq	%rdi, %rdi
	je	.L4087
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L4121
	.p2align 4,,10
	.p2align 3
.L4123:
	movdqu	-88(%rbp), %xmm0
	movups	%xmm0, 24(%r12)
	jmp	.L4078
	.p2align 4,,10
	.p2align 3
.L4091:
	call	*%rax
	jmp	.L4090
	.p2align 4,,10
	.p2align 3
.L4083:
	call	*%rax
	jmp	.L4082
.L4126:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7992:
	.size	_ZNK12v8_inspector12_GLOBAL__N_112ObjectMirror17buildRemoteObjectEN2v85LocalINS2_7ContextEEENS_8WrapModeEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISA_EE, .-_ZNK12v8_inspector12_GLOBAL__N_112ObjectMirror17buildRemoteObjectEN2v85LocalINS2_7ContextEEENS_8WrapModeEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISA_EE
	.weak	_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE,"awG",@progbits,_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE, @object
	.size	_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE, 48
_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD1Ev
	.quad	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev
	.weak	_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE,"awG",@progbits,_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE, @object
	.size	_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE, 48
_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	.quad	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev
	.weak	_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE,"awG",@progbits,_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE, @object
	.size	_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE, 48
_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD1Ev
	.quad	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev
	.weak	_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE,"awG",@progbits,_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE, @object
	.size	_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE, 48
_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD1Ev
	.quad	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev
	.section	.data.rel.ro.local._ZTVN12v8_inspector12_GLOBAL__N_120PrimitiveValueMirrorE,"aw"
	.align 8
	.type	_ZTVN12v8_inspector12_GLOBAL__N_120PrimitiveValueMirrorE, @object
	.size	_ZTVN12v8_inspector12_GLOBAL__N_120PrimitiveValueMirrorE, 72
_ZTVN12v8_inspector12_GLOBAL__N_120PrimitiveValueMirrorE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector12_GLOBAL__N_120PrimitiveValueMirrorD1Ev
	.quad	_ZN12v8_inspector12_GLOBAL__N_120PrimitiveValueMirrorD0Ev
	.quad	_ZNK12v8_inspector12_GLOBAL__N_120PrimitiveValueMirror17buildRemoteObjectEN2v85LocalINS2_7ContextEEENS_8WrapModeEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISA_EE
	.quad	_ZNK12v8_inspector12_GLOBAL__N_120PrimitiveValueMirror20buildPropertyPreviewEN2v85LocalINS2_7ContextEEERKNS_8String16EPSt10unique_ptrINS_8protocol7Runtime15PropertyPreviewESt14default_deleteISC_EE
	.quad	_ZNK12v8_inspector11ValueMirror18buildObjectPreviewEN2v85LocalINS1_7ContextEEEbPiS5_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteIS9_EE
	.quad	_ZNK12v8_inspector12_GLOBAL__N_120PrimitiveValueMirror17buildEntryPreviewEN2v85LocalINS2_7ContextEEEPiS6_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteISA_EE
	.quad	_ZNK12v8_inspector12_GLOBAL__N_120PrimitiveValueMirror7v8ValueEv
	.section	.data.rel.ro.local._ZTVN12v8_inspector12_GLOBAL__N_112NumberMirrorE,"aw"
	.align 8
	.type	_ZTVN12v8_inspector12_GLOBAL__N_112NumberMirrorE, @object
	.size	_ZTVN12v8_inspector12_GLOBAL__N_112NumberMirrorE, 72
_ZTVN12v8_inspector12_GLOBAL__N_112NumberMirrorE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector12_GLOBAL__N_112NumberMirrorD1Ev
	.quad	_ZN12v8_inspector12_GLOBAL__N_112NumberMirrorD0Ev
	.quad	_ZNK12v8_inspector12_GLOBAL__N_112NumberMirror17buildRemoteObjectEN2v85LocalINS2_7ContextEEENS_8WrapModeEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISA_EE
	.quad	_ZNK12v8_inspector12_GLOBAL__N_112NumberMirror20buildPropertyPreviewEN2v85LocalINS2_7ContextEEERKNS_8String16EPSt10unique_ptrINS_8protocol7Runtime15PropertyPreviewESt14default_deleteISC_EE
	.quad	_ZNK12v8_inspector11ValueMirror18buildObjectPreviewEN2v85LocalINS1_7ContextEEEbPiS5_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteIS9_EE
	.quad	_ZNK12v8_inspector12_GLOBAL__N_112NumberMirror17buildEntryPreviewEN2v85LocalINS2_7ContextEEEPiS6_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteISA_EE
	.quad	_ZNK12v8_inspector12_GLOBAL__N_112NumberMirror7v8ValueEv
	.section	.data.rel.ro.local._ZTVN12v8_inspector12_GLOBAL__N_112BigIntMirrorE,"aw"
	.align 8
	.type	_ZTVN12v8_inspector12_GLOBAL__N_112BigIntMirrorE, @object
	.size	_ZTVN12v8_inspector12_GLOBAL__N_112BigIntMirrorE, 72
_ZTVN12v8_inspector12_GLOBAL__N_112BigIntMirrorE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector12_GLOBAL__N_112BigIntMirrorD1Ev
	.quad	_ZN12v8_inspector12_GLOBAL__N_112BigIntMirrorD0Ev
	.quad	_ZNK12v8_inspector12_GLOBAL__N_112BigIntMirror17buildRemoteObjectEN2v85LocalINS2_7ContextEEENS_8WrapModeEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISA_EE
	.quad	_ZNK12v8_inspector12_GLOBAL__N_112BigIntMirror20buildPropertyPreviewEN2v85LocalINS2_7ContextEEERKNS_8String16EPSt10unique_ptrINS_8protocol7Runtime15PropertyPreviewESt14default_deleteISC_EE
	.quad	_ZNK12v8_inspector11ValueMirror18buildObjectPreviewEN2v85LocalINS1_7ContextEEEbPiS5_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteIS9_EE
	.quad	_ZNK12v8_inspector12_GLOBAL__N_112BigIntMirror17buildEntryPreviewEN2v85LocalINS2_7ContextEEEPiS6_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteISA_EE
	.quad	_ZNK12v8_inspector12_GLOBAL__N_112BigIntMirror7v8ValueEv
	.section	.data.rel.ro.local._ZTVN12v8_inspector12_GLOBAL__N_112SymbolMirrorE,"aw"
	.align 8
	.type	_ZTVN12v8_inspector12_GLOBAL__N_112SymbolMirrorE, @object
	.size	_ZTVN12v8_inspector12_GLOBAL__N_112SymbolMirrorE, 72
_ZTVN12v8_inspector12_GLOBAL__N_112SymbolMirrorE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector12_GLOBAL__N_112SymbolMirrorD1Ev
	.quad	_ZN12v8_inspector12_GLOBAL__N_112SymbolMirrorD0Ev
	.quad	_ZNK12v8_inspector12_GLOBAL__N_112SymbolMirror17buildRemoteObjectEN2v85LocalINS2_7ContextEEENS_8WrapModeEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISA_EE
	.quad	_ZNK12v8_inspector12_GLOBAL__N_112SymbolMirror20buildPropertyPreviewEN2v85LocalINS2_7ContextEEERKNS_8String16EPSt10unique_ptrINS_8protocol7Runtime15PropertyPreviewESt14default_deleteISC_EE
	.quad	_ZNK12v8_inspector11ValueMirror18buildObjectPreviewEN2v85LocalINS1_7ContextEEEbPiS5_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteIS9_EE
	.quad	_ZNK12v8_inspector11ValueMirror17buildEntryPreviewEN2v85LocalINS1_7ContextEEEPiS5_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteIS9_EE
	.quad	_ZNK12v8_inspector12_GLOBAL__N_112SymbolMirror7v8ValueEv
	.section	.data.rel.ro.local._ZTVN12v8_inspector12_GLOBAL__N_114LocationMirrorE,"aw"
	.align 8
	.type	_ZTVN12v8_inspector12_GLOBAL__N_114LocationMirrorE, @object
	.size	_ZTVN12v8_inspector12_GLOBAL__N_114LocationMirrorE, 72
_ZTVN12v8_inspector12_GLOBAL__N_114LocationMirrorE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector12_GLOBAL__N_114LocationMirrorD1Ev
	.quad	_ZN12v8_inspector12_GLOBAL__N_114LocationMirrorD0Ev
	.quad	_ZNK12v8_inspector12_GLOBAL__N_114LocationMirror17buildRemoteObjectEN2v85LocalINS2_7ContextEEENS_8WrapModeEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISA_EE
	.quad	_ZNK12v8_inspector11ValueMirror20buildPropertyPreviewEN2v85LocalINS1_7ContextEEERKNS_8String16EPSt10unique_ptrINS_8protocol7Runtime15PropertyPreviewESt14default_deleteISB_EE
	.quad	_ZNK12v8_inspector11ValueMirror18buildObjectPreviewEN2v85LocalINS1_7ContextEEEbPiS5_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteIS9_EE
	.quad	_ZNK12v8_inspector11ValueMirror17buildEntryPreviewEN2v85LocalINS1_7ContextEEEPiS5_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteIS9_EE
	.quad	_ZNK12v8_inspector12_GLOBAL__N_114LocationMirror7v8ValueEv
	.section	.data.rel.ro.local._ZTVN12v8_inspector12_GLOBAL__N_114FunctionMirrorE,"aw"
	.align 8
	.type	_ZTVN12v8_inspector12_GLOBAL__N_114FunctionMirrorE, @object
	.size	_ZTVN12v8_inspector12_GLOBAL__N_114FunctionMirrorE, 72
_ZTVN12v8_inspector12_GLOBAL__N_114FunctionMirrorE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector12_GLOBAL__N_114FunctionMirrorD1Ev
	.quad	_ZN12v8_inspector12_GLOBAL__N_114FunctionMirrorD0Ev
	.quad	_ZNK12v8_inspector12_GLOBAL__N_114FunctionMirror17buildRemoteObjectEN2v85LocalINS2_7ContextEEENS_8WrapModeEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISA_EE
	.quad	_ZNK12v8_inspector12_GLOBAL__N_114FunctionMirror20buildPropertyPreviewEN2v85LocalINS2_7ContextEEERKNS_8String16EPSt10unique_ptrINS_8protocol7Runtime15PropertyPreviewESt14default_deleteISC_EE
	.quad	_ZNK12v8_inspector11ValueMirror18buildObjectPreviewEN2v85LocalINS1_7ContextEEEbPiS5_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteIS9_EE
	.quad	_ZNK12v8_inspector12_GLOBAL__N_114FunctionMirror17buildEntryPreviewEN2v85LocalINS2_7ContextEEEPiS6_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteISA_EE
	.quad	_ZNK12v8_inspector12_GLOBAL__N_114FunctionMirror7v8ValueEv
	.section	.data.rel.ro.local._ZTVN12v8_inspector12_GLOBAL__N_126PreviewPropertyAccumulatorE,"aw"
	.align 8
	.type	_ZTVN12v8_inspector12_GLOBAL__N_126PreviewPropertyAccumulatorE, @object
	.size	_ZTVN12v8_inspector12_GLOBAL__N_126PreviewPropertyAccumulatorE, 40
_ZTVN12v8_inspector12_GLOBAL__N_126PreviewPropertyAccumulatorE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector12_GLOBAL__N_126PreviewPropertyAccumulatorD1Ev
	.quad	_ZN12v8_inspector12_GLOBAL__N_126PreviewPropertyAccumulatorD0Ev
	.quad	_ZN12v8_inspector12_GLOBAL__N_126PreviewPropertyAccumulator3AddENS_14PropertyMirrorE
	.section	.data.rel.ro.local._ZTVN12v8_inspector12_GLOBAL__N_112ObjectMirrorE,"aw"
	.align 8
	.type	_ZTVN12v8_inspector12_GLOBAL__N_112ObjectMirrorE, @object
	.size	_ZTVN12v8_inspector12_GLOBAL__N_112ObjectMirrorE, 72
_ZTVN12v8_inspector12_GLOBAL__N_112ObjectMirrorE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector12_GLOBAL__N_112ObjectMirrorD1Ev
	.quad	_ZN12v8_inspector12_GLOBAL__N_112ObjectMirrorD0Ev
	.quad	_ZNK12v8_inspector12_GLOBAL__N_112ObjectMirror17buildRemoteObjectEN2v85LocalINS2_7ContextEEENS_8WrapModeEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISA_EE
	.quad	_ZNK12v8_inspector12_GLOBAL__N_112ObjectMirror20buildPropertyPreviewEN2v85LocalINS2_7ContextEEERKNS_8String16EPSt10unique_ptrINS_8protocol7Runtime15PropertyPreviewESt14default_deleteISC_EE
	.quad	_ZNK12v8_inspector12_GLOBAL__N_112ObjectMirror18buildObjectPreviewEN2v85LocalINS2_7ContextEEEbPiS6_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteISA_EE
	.quad	_ZNK12v8_inspector12_GLOBAL__N_112ObjectMirror17buildEntryPreviewEN2v85LocalINS2_7ContextEEEPiS6_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteISA_EE
	.quad	_ZNK12v8_inspector12_GLOBAL__N_112ObjectMirror7v8ValueEv
	.weak	_ZTVN12v8_inspector11ValueMirrorE
	.section	.data.rel.ro._ZTVN12v8_inspector11ValueMirrorE,"awG",@progbits,_ZTVN12v8_inspector11ValueMirrorE,comdat
	.align 8
	.type	_ZTVN12v8_inspector11ValueMirrorE, @object
	.size	_ZTVN12v8_inspector11ValueMirrorE, 72
_ZTVN12v8_inspector11ValueMirrorE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	_ZNK12v8_inspector11ValueMirror20buildPropertyPreviewEN2v85LocalINS1_7ContextEEERKNS_8String16EPSt10unique_ptrINS_8protocol7Runtime15PropertyPreviewESt14default_deleteISB_EE
	.quad	_ZNK12v8_inspector11ValueMirror18buildObjectPreviewEN2v85LocalINS1_7ContextEEEbPiS5_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteIS9_EE
	.quad	_ZNK12v8_inspector11ValueMirror17buildEntryPreviewEN2v85LocalINS1_7ContextEEEPiS5_PSt10unique_ptrINS_8protocol7Runtime13ObjectPreviewESt14default_deleteIS9_EE
	.quad	__cxa_pure_virtual
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC10:
	.long	0
	.long	-1042284544
	.align 8
.LC11:
	.long	4290772992
	.long	1105199103
	.align 8
.LC25:
	.long	0
	.long	0
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC27:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC28:
	.long	4294967295
	.long	2146435071
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
