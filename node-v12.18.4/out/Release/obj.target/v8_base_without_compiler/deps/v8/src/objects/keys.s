	.file	"keys.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB4770:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE4770:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB4771:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4771:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZNK2v84base19TemplateHashMapImplINS_8internal6HandleINS2_4NameEEEiNS2_12_GLOBAL__N_114NameComparatorENS2_20ZoneAllocationPolicyEE5ProbeERKS5_j.isra.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK2v84base19TemplateHashMapImplINS_8internal6HandleINS2_4NameEEEiNS2_12_GLOBAL__N_114NameComparatorENS2_20ZoneAllocationPolicyEE5ProbeERKS5_j.isra.0, @function
_ZNK2v84base19TemplateHashMapImplINS_8internal6HandleINS2_4NameEEEiNS2_12_GLOBAL__N_114NameComparatorENS2_20ZoneAllocationPolicyEE5ProbeERKS5_j.isra.0:
.LFB23797:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	movq	0(%r13), %rcx
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	8(%rdi), %edi
	leal	-1(%rdi), %ebx
	andl	%edx, %ebx
	leaq	(%rbx,%rbx,2), %r12
	salq	$3, %r12
	leaq	(%rcx,%r12), %rax
	cmpb	$0, 16(%rax)
	je	.L4
	movq	%rsi, %r14
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L7:
	movq	16(%r13), %r9
	movq	-1(%rax), %r8
	movzwl	11(%r8), %r8d
	andl	$65504, %r8d
	jne	.L11
	movq	(%rdx), %r8
	movq	-1(%r8), %r8
	movzwl	11(%r8), %r8d
	andl	$65504, %r8d
	je	.L12
.L11:
	movq	-1(%rax), %rax
	cmpw	$64, 11(%rax)
	je	.L12
	movq	(%rdx), %rax
	movq	-1(%rax), %rax
	cmpw	$64, 11(%rax)
	je	.L12
	movq	%r9, %rdi
	call	_ZN2v88internal6String10SlowEqualsEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	testb	%al, %al
	jne	.L21
	movl	8(%r13), %edi
	movq	0(%r13), %rcx
.L12:
	addq	$1, %rbx
	leal	-1(%rdi), %edx
	andq	%rdx, %rbx
	leaq	(%rbx,%rbx,2), %r12
	salq	$3, %r12
	leaq	(%rcx,%r12), %rax
	cmpb	$0, 16(%rax)
	je	.L4
.L13:
	movq	(%r14), %rsi
	movq	(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L23
	movq	(%rsi), %rax
	testq	%rdx, %rdx
	je	.L7
	testq	%rsi, %rsi
	je	.L7
	cmpq	%rax, (%rdx)
	jne	.L7
.L23:
	leaq	(%rcx,%r12), %rax
.L4:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L21:
	.cfi_restore_state
	movq	0(%r13), %rax
	addq	%r12, %rax
	jmp	.L4
	.cfi_endproc
.LFE23797:
	.size	_ZNK2v84base19TemplateHashMapImplINS_8internal6HandleINS2_4NameEEEiNS2_12_GLOBAL__N_114NameComparatorENS2_20ZoneAllocationPolicyEE5ProbeERKS5_j.isra.0, .-_ZNK2v84base19TemplateHashMapImplINS_8internal6HandleINS2_4NameEEEiNS2_12_GLOBAL__N_114NameComparatorENS2_20ZoneAllocationPolicyEE5ProbeERKS5_j.isra.0
	.section	.text._ZN2v88internal12_GLOBAL__N_132GetOwnEnumPropertyDictionaryKeysINS0_16GlobalDictionaryEEENS0_6HandleINS0_10FixedArrayEEEPNS0_7IsolateENS0_17KeyCollectionModeEPNS0_14KeyAccumulatorENS4_INS0_8JSObjectEEET_.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_132GetOwnEnumPropertyDictionaryKeysINS0_16GlobalDictionaryEEENS0_6HandleINS0_10FixedArrayEEEPNS0_7IsolateENS0_17KeyCollectionModeEPNS0_14KeyAccumulatorENS4_INS0_8JSObjectEEET_.isra.0, @function
_ZN2v88internal12_GLOBAL__N_132GetOwnEnumPropertyDictionaryKeysINS0_16GlobalDictionaryEEENS0_6HandleINS0_10FixedArrayEEEPNS0_7IsolateENS0_17KeyCollectionModeEPNS0_14KeyAccumulatorENS4_INS0_8JSObjectEEET_.isra.0:
.LFB23824:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%esi, %r14d
	movq	%rcx, %rsi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	41112(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L25
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r13
	movl	19(%rsi), %eax
	testl	%eax, %eax
	jne	.L28
.L34:
	leaq	288(%r12), %rax
.L29:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L32
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L33
.L27:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rcx, 0(%r13)
	movl	19(%rsi), %eax
	testl	%eax, %eax
	je	.L34
.L28:
	leaq	-64(%rbp), %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal10DictionaryINS0_16GlobalDictionaryENS0_21GlobalDictionaryShapeEE28NumberOfEnumerablePropertiesEv@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	%r15, %r8
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%rax, %rbx
	movq	%rax, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal18BaseNameDictionaryINS0_16GlobalDictionaryENS0_21GlobalDictionaryShapeEE14CopyEnumKeysToEPNS0_7IsolateENS0_6HandleIS2_EENS7_INS0_10FixedArrayEEENS0_17KeyCollectionModeEPNS0_14KeyAccumulatorE@PLT
	movq	%rbx, %rax
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L33:
	movq	%r12, %rdi
	movq	%rcx, -80(%rbp)
	movq	%rcx, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %rcx
	movq	-72(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L27
.L32:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23824:
	.size	_ZN2v88internal12_GLOBAL__N_132GetOwnEnumPropertyDictionaryKeysINS0_16GlobalDictionaryEEENS0_6HandleINS0_10FixedArrayEEEPNS0_7IsolateENS0_17KeyCollectionModeEPNS0_14KeyAccumulatorENS4_INS0_8JSObjectEEET_.isra.0, .-_ZN2v88internal12_GLOBAL__N_132GetOwnEnumPropertyDictionaryKeysINS0_16GlobalDictionaryEEENS0_6HandleINS0_10FixedArrayEEEPNS0_7IsolateENS0_17KeyCollectionModeEPNS0_14KeyAccumulatorENS4_INS0_8JSObjectEEET_.isra.0
	.section	.text._ZN2v88internal12_GLOBAL__N_132GetOwnEnumPropertyDictionaryKeysINS0_14NameDictionaryEEENS0_6HandleINS0_10FixedArrayEEEPNS0_7IsolateENS0_17KeyCollectionModeEPNS0_14KeyAccumulatorENS4_INS0_8JSObjectEEET_.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_132GetOwnEnumPropertyDictionaryKeysINS0_14NameDictionaryEEENS0_6HandleINS0_10FixedArrayEEEPNS0_7IsolateENS0_17KeyCollectionModeEPNS0_14KeyAccumulatorENS4_INS0_8JSObjectEEET_.isra.0, @function
_ZN2v88internal12_GLOBAL__N_132GetOwnEnumPropertyDictionaryKeysINS0_14NameDictionaryEEENS0_6HandleINS0_10FixedArrayEEEPNS0_7IsolateENS0_17KeyCollectionModeEPNS0_14KeyAccumulatorENS4_INS0_8JSObjectEEET_.isra.0:
.LFB23825:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%esi, %r14d
	movq	%rcx, %rsi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	41112(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L36
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r13
	movl	19(%rsi), %eax
	testl	%eax, %eax
	jne	.L39
.L45:
	leaq	288(%r12), %rax
.L40:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L43
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	.cfi_restore_state
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L44
.L38:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rcx, 0(%r13)
	movl	19(%rsi), %eax
	testl	%eax, %eax
	je	.L45
.L39:
	leaq	-64(%rbp), %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal10DictionaryINS0_14NameDictionaryENS0_19NameDictionaryShapeEE28NumberOfEnumerablePropertiesEv@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	%r15, %r8
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%rax, %rbx
	movq	%rax, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal18BaseNameDictionaryINS0_14NameDictionaryENS0_19NameDictionaryShapeEE14CopyEnumKeysToEPNS0_7IsolateENS0_6HandleIS2_EENS7_INS0_10FixedArrayEEENS0_17KeyCollectionModeEPNS0_14KeyAccumulatorE@PLT
	movq	%rbx, %rax
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L44:
	movq	%r12, %rdi
	movq	%rcx, -80(%rbp)
	movq	%rcx, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %rcx
	movq	-72(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L38
.L43:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23825:
	.size	_ZN2v88internal12_GLOBAL__N_132GetOwnEnumPropertyDictionaryKeysINS0_14NameDictionaryEEENS0_6HandleINS0_10FixedArrayEEEPNS0_7IsolateENS0_17KeyCollectionModeEPNS0_14KeyAccumulatorENS4_INS0_8JSObjectEEET_.isra.0, .-_ZN2v88internal12_GLOBAL__N_132GetOwnEnumPropertyDictionaryKeysINS0_14NameDictionaryEEENS0_6HandleINS0_10FixedArrayEEEPNS0_7IsolateENS0_17KeyCollectionModeEPNS0_14KeyAccumulatorENS4_INS0_8JSObjectEEET_.isra.0
	.section	.rodata._ZN2v84base19TemplateHashMapImplINS_8internal6HandleINS2_4NameEEEiNS2_12_GLOBAL__N_114NameComparatorENS2_20ZoneAllocationPolicyEE6ResizeES8_.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"Out of memory: HashMap::Initialize"
	.section	.text._ZN2v84base19TemplateHashMapImplINS_8internal6HandleINS2_4NameEEEiNS2_12_GLOBAL__N_114NameComparatorENS2_20ZoneAllocationPolicyEE6ResizeES8_,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v84base19TemplateHashMapImplINS_8internal6HandleINS2_4NameEEEiNS2_12_GLOBAL__N_114NameComparatorENS2_20ZoneAllocationPolicyEE6ResizeES8_, @function
_ZN2v84base19TemplateHashMapImplINS_8internal6HandleINS2_4NameEEEiNS2_12_GLOBAL__N_114NameComparatorENS2_20ZoneAllocationPolicyEE6ResizeES8_:
.LFB23371:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	8(%rdi), %eax
	movq	24(%r14), %rdx
	movq	(%rdi), %rbx
	movl	12(%rdi), %r13d
	addl	%eax, %eax
	leaq	(%rax,%rax,2), %rsi
	movq	%rax, %r15
	movq	16(%r14), %rax
	salq	$3, %rsi
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L68
	addq	%rax, %rsi
	movq	%rsi, 16(%r14)
.L48:
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L69
	movl	%r15d, 8(%r12)
	testl	%r15d, %r15d
	je	.L50
	movb	$0, 16(%rax)
	cmpl	$1, 8(%r12)
	movl	$24, %edx
	movl	$1, %eax
	jbe	.L50
	.p2align 4,,10
	.p2align 3
.L51:
	movq	(%r12), %rcx
	addq	$1, %rax
	movb	$0, 16(%rcx,%rdx)
	movl	8(%r12), %ecx
	addq	$24, %rdx
	cmpq	%rax, %rcx
	ja	.L51
.L50:
	movl	$0, 12(%r12)
	testl	%r13d, %r13d
	je	.L46
.L53:
	cmpb	$0, 16(%rbx)
	jne	.L70
.L54:
	addq	$24, %rbx
	cmpb	$0, 16(%rbx)
	je	.L54
.L70:
	movl	12(%rbx), %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZNK2v84base19TemplateHashMapImplINS_8internal6HandleINS2_4NameEEEiNS2_12_GLOBAL__N_114NameComparatorENS2_20ZoneAllocationPolicyEE5ProbeERKS5_j.isra.0
	movl	8(%rbx), %edx
	movl	12(%rbx), %r15d
	movq	(%rbx), %rcx
	movb	$1, 16(%rax)
	movl	%edx, 8(%rax)
	movq	%rcx, (%rax)
	movl	%r15d, 12(%rax)
	movl	12(%r12), %eax
	addl	$1, %eax
	movl	%eax, %edx
	movl	%eax, 12(%r12)
	shrl	$2, %edx
	addl	%edx, %eax
	cmpl	8(%r12), %eax
	jnb	.L71
.L55:
	addq	$24, %rbx
	subl	$1, %r13d
	jne	.L53
.L46:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	.cfi_restore_state
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v84base19TemplateHashMapImplINS_8internal6HandleINS2_4NameEEEiNS2_12_GLOBAL__N_114NameComparatorENS2_20ZoneAllocationPolicyEE6ResizeES8_
	movl	%r15d, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZNK2v84base19TemplateHashMapImplINS_8internal6HandleINS2_4NameEEEiNS2_12_GLOBAL__N_114NameComparatorENS2_20ZoneAllocationPolicyEE5ProbeERKS5_j.isra.0
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L68:
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L48
.L69:
	leaq	.LC0(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE23371:
	.size	_ZN2v84base19TemplateHashMapImplINS_8internal6HandleINS2_4NameEEEiNS2_12_GLOBAL__N_114NameComparatorENS2_20ZoneAllocationPolicyEE6ResizeES8_, .-_ZN2v84base19TemplateHashMapImplINS_8internal6HandleINS2_4NameEEEiNS2_12_GLOBAL__N_114NameComparatorENS2_20ZoneAllocationPolicyEE6ResizeES8_
	.section	.text._ZN2v88internal18FastKeyAccumulator7PrepareEv.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal18FastKeyAccumulator7PrepareEv.part.0, @function
_ZN2v88internal18FastKeyAccumulator7PrepareEv.part.0:
.LFB23815:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rdx
	movq	(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$256, %eax
	movw	%ax, 34(%rdi)
	movq	(%rdx), %rax
	leaq	-1(%rax), %rcx
	testb	$1, %al
	jne	.L111
.L74:
	movq	(%rcx), %rax
	leaq	-64(%rbp), %r15
	movq	$0, -72(%rbp)
	movq	23(%rax), %rbx
	cmpq	104(%r13), %rbx
	jne	.L79
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L113:
	movq	%r15, %rdi
	movq	%rbx, -64(%rbp)
	call	_ZN2v88internal8JSObject21HasEnumerableElementsEv@PLT
	testb	%al, %al
	jne	.L85
	andl	$1, %ebx
	jne	.L87
.L89:
	movq	(%r14), %rax
	movq	23(%rax), %rbx
	cmpq	104(%r13), %rbx
	je	.L88
.L79:
	movq	-1(%rbx), %rax
	leaq	-1(%rbx), %r14
	movl	15(%rax), %eax
	andl	$1023, %eax
	cmpl	$1023, %eax
	je	.L112
.L81:
	movq	(%r14), %rax
	movl	15(%rax), %eax
	testl	$1023, %eax
	je	.L113
.L85:
	movq	%rbx, -72(%rbp)
	andl	$1, %ebx
	movb	$0, 35(%r12)
	je	.L89
.L87:
	movq	(%r14), %rax
	cmpw	$1024, 11(%rax)
	jne	.L89
.L88:
	cmpb	$0, 35(%r12)
	jne	.L114
	movq	-72(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L72
	movq	(%r12), %rbx
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L91
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L92:
	movq	%rax, 16(%r12)
.L72:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L115
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	.cfi_restore_state
	movq	-1(%rbx), %rax
	movq	%r15, %rdi
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal3Map23OnlyHasSimplePropertiesEv@PLT
	testb	%al, %al
	je	.L81
	movq	-64(%rbp), %rax
	cmpw	$1024, 11(%rax)
	je	.L81
	movq	%r15, %rdi
	call	_ZNK2v88internal3Map28NumberOfEnumerablePropertiesEv@PLT
	testl	%eax, %eax
	jg	.L81
	movq	-64(%rbp), %rcx
	movl	15(%rcx), %eax
	andl	$-1024, %eax
	movl	%eax, 15(%rcx)
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L114:
	movq	8(%r12), %rdx
.L75:
	movq	(%rdx), %rdx
	movq	-1(%rdx), %rax
	movl	15(%rax), %eax
	andl	$1023, %eax
	cmpl	$1023, %eax
	jne	.L77
	xorl	%eax, %eax
.L78:
	movb	%al, 34(%r12)
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L77:
	leaq	-64(%rbp), %rdi
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal8JSObject21HasEnumerableElementsEv@PLT
	xorl	$1, %eax
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L91:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L116
.L93:
	movq	-72(%rbp), %rsi
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L111:
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	jne	.L74
.L110:
	cmpb	$0, 35(%r12)
	je	.L72
	jmp	.L75
.L116:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L93
.L115:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23815:
	.size	_ZN2v88internal18FastKeyAccumulator7PrepareEv.part.0, .-_ZN2v88internal18FastKeyAccumulator7PrepareEv.part.0
	.section	.rodata._ZN2v88internal12_GLOBAL__N_123GetFastEnumPropertyKeysEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"static_cast<unsigned>(length) <= static_cast<unsigned>(kMaxNumberOfDescriptors)"
	.section	.rodata._ZN2v88internal12_GLOBAL__N_123GetFastEnumPropertyKeysEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"Check failed: %s."
.LC3:
	.string	"unreachable code"
	.section	.text._ZN2v88internal12_GLOBAL__N_123GetFastEnumPropertyKeysEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_123GetFastEnumPropertyKeysEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEE, @function
_ZN2v88internal12_GLOBAL__N_123GetFastEnumPropertyKeysEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEE:
.LFB19015:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	-1(%rax), %rsi
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L118
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r13
.L119:
	movq	39(%rsi), %rax
	movq	41112(%r12), %rdi
	movq	15(%rax), %rax
	movq	7(%rax), %rsi
	testq	%rdi, %rdi
	je	.L121
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L122:
	movq	0(%r13), %rax
	movl	15(%rax), %r15d
	andl	$1023, %r15d
	cmpl	$1023, %r15d
	je	.L124
	movq	40960(%r12), %rbx
	cmpb	$0, 7008(%rbx)
	je	.L125
	movq	7000(%rbx), %rax
.L126:
	testq	%rax, %rax
	je	.L127
	addl	$1, (%rax)
.L127:
	movq	(%r14), %rax
	cmpl	%r15d, 11(%rax)
	je	.L136
	movq	%r14, %rsi
	xorl	%ecx, %ecx
	movl	%r15d, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory18CopyFixedArrayUpToENS0_6HandleINS0_10FixedArrayEEEiNS0_14AllocationTypeE@PLT
	movq	%rax, %r14
.L136:
	movq	%r14, %rax
.L129:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L211
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L125:
	.cfi_restore_state
	movb	$1, 7008(%rbx)
	leaq	6984(%rbx), %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 7000(%rbx)
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L118:
	movq	41088(%r12), %r13
	cmpq	%r13, 41096(%r12)
	je	.L212
.L120:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L121:
	movq	41088(%r12), %r14
	cmpq	%r14, 41096(%r12)
	je	.L213
.L123:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r14)
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L124:
	movq	%rax, -64(%rbp)
	leaq	-64(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -88(%rbp)
	call	_ZNK2v88internal3Map28NumberOfEnumerablePropertiesEv@PLT
	movl	%eax, %ebx
	movl	%eax, -76(%rbp)
	movq	(%r14), %rax
	cmpl	%ebx, 11(%rax)
	movq	0(%r13), %rax
	jge	.L214
	movq	41112(%r12), %rdi
	movq	39(%rax), %rsi
	testq	%rdi, %rdi
	je	.L137
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L138:
	movq	40960(%r12), %rbx
	cmpb	$0, 7040(%rbx)
	je	.L140
	movq	7032(%rbx), %rax
.L141:
	testq	%rax, %rax
	je	.L142
	addl	$1, (%rax)
.L142:
	movq	0(%r13), %rax
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	15(%rax), %ebx
	movl	-76(%rbp), %esi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	shrl	$10, %ebx
	andl	$1023, %ebx
	movq	%rax, -72(%rbp)
	testl	%ebx, %ebx
	je	.L143
	leal	-1(%rbx), %eax
	movl	$1, %r9d
	xorl	%ecx, %ecx
	movl	$31, %ebx
	leaq	(%rax,%rax,2), %rax
	movq	%r12, -144(%rbp)
	xorl	%r11d, %r11d
	movl	%ecx, %r12d
	leaq	55(,%rax,8), %r8
	movq	%r13, -136(%rbp)
	movq	%r15, %rax
	movl	%r9d, %r15d
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L147:
	movq	-72(%rbp), %rcx
	leal	16(,%r12,8), %edx
	movslq	%edx, %rdx
	movq	(%rcx), %rcx
	movq	%r13, -1(%rdx,%rcx)
.L172:
	andl	$2, %r14d
	cmovne	%r11d, %r15d
	addl	$1, %r12d
.L146:
	addq	$24, %rbx
	cmpq	%rbx, %r8
	je	.L145
.L144:
	movq	(%rax), %rcx
	movq	(%rcx,%rbx), %rdx
	movq	%rdx, %r14
	sarq	$32, %r14
	btq	$36, %rdx
	jc	.L146
	movq	-8(%rbx,%rcx), %r13
	testb	$1, %r13b
	je	.L147
	movq	-1(%r13), %rdx
	cmpw	$64, 11(%rdx)
	je	.L146
	movq	-72(%rbp), %rcx
	leal	16(,%r12,8), %edx
	movslq	%edx, %rdx
	movq	(%rcx), %rdi
	movq	%r13, %rcx
	andq	$-262144, %rcx
	leaq	-1(%rdi,%rdx), %rsi
	movq	%r13, (%rsi)
	movq	8(%rcx), %rdx
	testl	$262144, %edx
	je	.L170
	movq	%r13, %rdx
	movq	%r8, -128(%rbp)
	movq	%rax, -120(%rbp)
	movq	%rcx, -112(%rbp)
	movq	%rsi, -104(%rbp)
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-112(%rbp), %rcx
	movq	-128(%rbp), %r8
	xorl	%r11d, %r11d
	movq	-120(%rbp), %rax
	movq	-104(%rbp), %rsi
	movq	8(%rcx), %rdx
	movq	-96(%rbp), %rdi
.L170:
	andl	$24, %edx
	je	.L172
	movq	%rdi, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	jne	.L172
	movq	%r13, %rdx
	movq	%r8, -104(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-104(%rbp), %r8
	movq	-96(%rbp), %rax
	xorl	%r11d, %r11d
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L137:
	movq	41088(%r12), %r15
	cmpq	41096(%r12), %r15
	je	.L215
.L139:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r15)
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L145:
	movq	-144(%rbp), %r12
	movl	%r15d, %r9d
	movq	-136(%rbp), %r13
	movq	%rax, %r15
	leaq	288(%r12), %r11
	testb	%r9b, %r9b
	jne	.L216
.L151:
	movq	-72(%rbp), %rdx
	movq	%r15, %rdi
	movq	%r11, %rcx
	movq	%r12, %rsi
	call	_ZN2v88internal15DescriptorArray27InitializeOrChangeEnumCacheENS0_6HandleIS1_EEPNS0_7IsolateENS2_INS0_10FixedArrayEEES7_@PLT
	movq	0(%r13), %rax
	movq	-88(%rbp), %rdi
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal3Map23OnlyHasSimplePropertiesEv@PLT
	testb	%al, %al
	jne	.L217
.L166:
	movq	-72(%rbp), %rax
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L214:
	movq	-88(%rbp), %rdi
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal3Map23OnlyHasSimplePropertiesEv@PLT
	testb	%al, %al
	jne	.L218
.L131:
	movq	40960(%r12), %rbx
	cmpb	$0, 7008(%rbx)
	je	.L133
	movq	7000(%rbx), %rax
.L134:
	testq	%rax, %rax
	je	.L135
	addl	$1, (%rax)
.L135:
	movq	(%r14), %rax
	movl	-76(%rbp), %edx
	cmpl	%edx, 11(%rax)
	je	.L136
	movq	%r14, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory18CopyFixedArrayUpToENS0_6HandleINS0_10FixedArrayEEEiNS0_14AllocationTypeE@PLT
	movq	%rax, %r14
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L140:
	movb	$1, 7040(%rbx)
	leaq	7016(%rbx), %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 7032(%rbx)
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L213:
	movq	%r12, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L212:
	movq	%r12, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L217:
	movl	-76(%rbp), %eax
	movq	0(%r13), %rdx
	cmpl	$1023, %eax
	je	.L167
	cmpl	$1020, %eax
	ja	.L168
.L167:
	movl	15(%rdx), %eax
	andl	$-1024, %eax
	orl	-76(%rbp), %eax
	movl	%eax, 15(%rdx)
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L216:
	movl	-76(%rbp), %esi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r8, -96(%rbp)
	xorl	%ebx, %ebx
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	-96(%rbp), %r8
	movl	$31, %ecx
	movq	%rax, %r11
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L174:
	movl	$32768, %esi
.L159:
	movzbl	%al, %eax
	movslq	%edx, %rdx
	movslq	%r9d, %r9
	movslq	%edi, %rdi
	salq	$17, %rdx
	salq	$14, %rax
	orq	%rdx, %rax
	salq	$27, %rdi
	orq	%r9, %rax
	orq	%rdi, %rax
	movq	(%r11), %rdi
	orq	%rsi, %rax
	movl	%eax, %esi
	sarl	$3, %esi
	andl	$2047, %esi
	testb	$64, %ah
	leal	-2(%rsi), %edx
	leal	-3(%rsi), %r9d
	movl	%edx, %esi
	movl	%r9d, %edx
	notl	%esi
	cmove	%esi, %edx
	shrq	$15, %rax
	andl	$3, %eax
	addl	%edx, %edx
	movl	%edx, %esi
	orl	$1, %esi
	cmpl	$1, %eax
	leal	16(,%rbx,8), %eax
	cmove	%esi, %edx
	cltq
	addl	$1, %ebx
	salq	$32, %rdx
	movq	%rdx, -1(%rdi,%rax)
.L165:
	addq	$24, %rcx
	cmpq	%r8, %rcx
	je	.L151
.L169:
	movq	(%r15), %rax
	movq	(%rax,%rcx), %rdx
	btq	$36, %rdx
	jc	.L165
	movq	-8(%rcx,%rax), %rax
	testb	$1, %al
	jne	.L154
.L157:
	movq	0(%r13), %r10
	movq	39(%r10), %rax
	movq	(%rax,%rcx), %rsi
	movzbl	7(%r10), %edx
	movq	%rsi, %rax
	shrq	$38, %rsi
	shrq	$51, %rax
	andl	$7, %esi
	movq	%rax, %r9
	movzbl	8(%r10), %eax
	andl	$1023, %r9d
	subl	%eax, %edx
	cmpl	%edx, %r9d
	setl	%al
	jl	.L219
	subl	%edx, %r9d
	movl	$16, %edi
	leal	16(,%r9,8), %r9d
.L158:
	cmpl	$2, %esi
	je	.L174
	cmpb	$2, %sil
	jg	.L160
	je	.L161
.L175:
	xorl	%esi, %esi
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L160:
	subl	$3, %esi
	cmpb	$1, %sil
	jbe	.L175
.L161:
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L154:
	movq	-1(%rax), %rax
	cmpw	$64, 11(%rax)
	jne	.L157
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L219:
	movzbl	8(%r10), %edi
	movzbl	8(%r10), %r10d
	addl	%r10d, %r9d
	sall	$3, %edi
	sall	$3, %r9d
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L133:
	movb	$1, 7008(%rbx)
	leaq	6984(%rbx), %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 7000(%rbx)
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L218:
	movl	-76(%rbp), %eax
	movq	0(%r13), %rdx
	cmpl	$1023, %eax
	je	.L132
	cmpl	$1020, %eax
	ja	.L168
.L132:
	movl	15(%rdx), %eax
	andl	$-1024, %eax
	orl	-76(%rbp), %eax
	movl	%eax, 15(%rdx)
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L143:
	movl	-76(%rbp), %esi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	%rax, %r11
	jmp	.L151
.L215:
	movq	%r12, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L139
.L168:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L211:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19015:
	.size	_ZN2v88internal12_GLOBAL__N_123GetFastEnumPropertyKeysEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEE, .-_ZN2v88internal12_GLOBAL__N_123GetFastEnumPropertyKeysEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEE
	.section	.text._ZN2v88internal14KeyAccumulator7GetKeysENS0_17GetKeysConversionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14KeyAccumulator7GetKeysENS0_17GetKeysConversionE
	.type	_ZN2v88internal14KeyAccumulator7GetKeysENS0_17GetKeysConversionE, @function
_ZN2v88internal14KeyAccumulator7GetKeysENS0_17GetKeysConversionE:
.LFB18993:
	.cfi_startproc
	endbr64
	movl	%esi, %edx
	movq	8(%rdi), %rsi
	movq	(%rdi), %r8
	testq	%rsi, %rsi
	je	.L228
	movl	32(%rdi), %eax
	testl	%eax, %eax
	jne	.L223
	movq	(%rsi), %rax
	movq	-1(%rax), %rcx
	movq	%rsi, %rax
	cmpq	%rcx, 152(%r8)
	je	.L226
.L223:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%r8, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal14OrderedHashSet18ConvertToKeysArrayEPNS0_7IsolateENS0_6HandleIS1_EENS0_17GetKeysConversionE@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L226:
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L228:
	leaq	288(%r8), %rax
	ret
	.cfi_endproc
.LFE18993:
	.size	_ZN2v88internal14KeyAccumulator7GetKeysENS0_17GetKeysConversionE, .-_ZN2v88internal14KeyAccumulator7GetKeysENS0_17GetKeysConversionE
	.section	.text._ZN2v88internal14KeyAccumulator4keysEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14KeyAccumulator4keysEv
	.type	_ZN2v88internal14KeyAccumulator4keysEv, @function
_ZN2v88internal14KeyAccumulator4keysEv:
.LFB18994:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE18994:
	.size	_ZN2v88internal14KeyAccumulator4keysEv, .-_ZN2v88internal14KeyAccumulator4keysEv
	.section	.text._ZN2v88internal14KeyAccumulator7AddKeysENS0_6HandleINS0_8JSObjectEEENS0_16AddKeyConversionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14KeyAccumulator7AddKeysENS0_6HandleINS0_8JSObjectEEENS0_16AddKeyConversionE
	.type	_ZN2v88internal14KeyAccumulator7AddKeysENS0_6HandleINS0_8JSObjectEEENS0_16AddKeyConversionE, @function
_ZN2v88internal14KeyAccumulator7AddKeysENS0_6HandleINS0_8JSObjectEEENS0_16AddKeyConversionE:
.LFB19000:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	movl	%edx, %ecx
	movq	%rdi, %r8
	movq	-1(%rax), %rax
	movq	_ZN2v88internal16ElementsAccessor19elements_accessors_E(%rip), %rdx
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	movq	(%rdx,%rax,8), %rdi
	movq	%r8, %rdx
	movq	(%rdi), %rax
	movq	96(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE19000:
	.size	_ZN2v88internal14KeyAccumulator7AddKeysENS0_6HandleINS0_8JSObjectEEENS0_16AddKeyConversionE, .-_ZN2v88internal14KeyAccumulator7AddKeysENS0_6HandleINS0_8JSObjectEEENS0_16AddKeyConversionE
	.section	.text._ZN2v88internal15FilterProxyKeysEPNS0_14KeyAccumulatorENS0_6HandleINS0_7JSProxyEEENS3_INS0_10FixedArrayEEENS0_14PropertyFilterE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal15FilterProxyKeysEPNS0_14KeyAccumulatorENS0_6HandleINS0_7JSProxyEEENS3_INS0_10FixedArrayEEENS0_14PropertyFilterE
	.type	_ZN2v88internal15FilterProxyKeysEPNS0_14KeyAccumulatorENS0_6HandleINS0_7JSProxyEEENS3_INS0_10FixedArrayEEENS0_14PropertyFilterE, @function
_ZN2v88internal15FilterProxyKeysEPNS0_14KeyAccumulatorENS0_6HandleINS0_7JSProxyEEENS3_INS0_10FixedArrayEEENS0_14PropertyFilterE:
.LFB19001:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -128(%rbp)
	movq	%rsi, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdx, %rax
	testl	%ecx, %ecx
	je	.L233
	movq	(%rdi), %r15
	movq	(%rdx), %rdi
	movl	11(%rdi), %r10d
	testl	%r10d, %r10d
	jle	.L261
	movl	%ecx, %eax
	movl	%ecx, %ebx
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	andl	$8, %eax
	movl	%eax, -104(%rbp)
	movl	%ecx, %eax
	andl	$16, %eax
	movl	%eax, -156(%rbp)
	movl	%ecx, %eax
	andl	$2, %eax
	movl	%eax, -100(%rbp)
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L281:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r8
	movq	%rsi, %rax
	notq	%rax
	andl	$1, %eax
	cmpl	$64, %ebx
	je	.L279
.L238:
	testb	%al, %al
	je	.L280
.L243:
	movl	-104(%rbp), %r9d
	testl	%r9d, %r9d
	jne	.L250
.L246:
	movl	-100(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L248
.L251:
	movq	(%r12), %rdi
.L242:
	cmpl	%r14d, %r13d
	je	.L254
	leal	16(,%r13,8), %eax
	movq	(%r8), %rdx
	cltq
	leaq	-1(%rdi,%rax), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L259
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	8(%rax), %rcx
	movq	%rax, -112(%rbp)
	testl	$262144, %ecx
	je	.L256
	movq	%rdx, -152(%rbp)
	movq	%rsi, -144(%rbp)
	movq	%rdi, -136(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-112(%rbp), %rax
	movq	-152(%rbp), %rdx
	movq	-144(%rbp), %rsi
	movq	-136(%rbp), %rdi
	movq	8(%rax), %rcx
.L256:
	andl	$24, %ecx
	je	.L259
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L259
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L259:
	movq	(%r12), %rdi
.L254:
	addl	$1, %r13d
.L240:
	addq	$1, %r14
	cmpl	%r14d, 11(%rdi)
	jle	.L234
.L258:
	movq	15(%rdi,%r14,8), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	jne	.L281
	movq	41088(%r15), %r8
	cmpq	41096(%r15), %r8
	je	.L282
.L237:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, %rax
	notq	%rax
	movq	%rsi, (%r8)
	andl	$1, %eax
	cmpl	$64, %ebx
	jne	.L238
.L279:
	testb	%al, %al
	jne	.L250
	movq	-1(%rsi), %rax
	movq	(%r12), %rdi
	cmpw	$64, 11(%rax)
	jne	.L240
	testb	$16, 11(%rsi)
	jne	.L242
	jmp	.L240
.L261:
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L234:
	movl	%r13d, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal10FixedArray13ShrinkOrEmptyEPNS0_7IsolateENS0_6HandleIS1_EEi@PLT
.L233:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L283
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L248:
	.cfi_restore_state
	movq	-120(%rbp), %rsi
	pxor	%xmm0, %xmm0
	movq	%r8, %rdx
	leaq	-96(%rbp), %rcx
	movq	%r15, %rdi
	andb	$-64, -96(%rbp)
	movq	%r8, -112(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	call	_ZN2v88internal7JSProxy24GetOwnPropertyDescriptorEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEEPNS0_18PropertyDescriptorE@PLT
	movq	-112(%rbp), %r8
	testb	%al, %al
	je	.L284
	shrw	$8, %ax
	je	.L250
	testb	$1, -96(%rbp)
	jne	.L251
	movq	-128(%rbp), %rax
	movl	32(%rax), %edx
	testl	%edx, %edx
	je	.L250
	cmpq	$0, 24(%rax)
	movq	(%rax), %rdi
	je	.L285
.L253:
	movq	-128(%rbp), %rax
	movq	%r8, %rdx
	movq	24(%rax), %rsi
	call	_ZN2v88internal13ObjectHashSet3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE@PLT
	movq	-128(%rbp), %rcx
	movq	%rax, 24(%rcx)
	.p2align 4,,10
	.p2align 3
.L250:
	movq	(%r12), %rdi
	jmp	.L240
	.p2align 4,,10
	.p2align 3
.L280:
	movq	-1(%rsi), %rax
	cmpw	$64, 11(%rax)
	jne	.L243
	movl	-156(%rbp), %edi
	testl	%edi, %edi
	jne	.L250
	testb	$1, 11(%rsi)
	je	.L246
	movq	(%r12), %rdi
	jmp	.L240
	.p2align 4,,10
	.p2align 3
.L282:
	movq	%r15, %rdi
	movq	%rsi, -112(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-112(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L284:
	xorl	%eax, %eax
	jmp	.L233
.L285:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$16, %esi
	movq	%r8, -112(%rbp)
	call	_ZN2v88internal9HashTableINS0_13ObjectHashSetENS0_18ObjectHashSetShapeEE3NewEPNS0_7IsolateEiNS0_14AllocationTypeENS0_15MinimumCapacityE@PLT
	movq	-128(%rbp), %rcx
	movq	-112(%rbp), %r8
	movq	%rax, 24(%rcx)
	movq	(%rcx), %rdi
	jmp	.L253
.L283:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19001:
	.size	_ZN2v88internal15FilterProxyKeysEPNS0_14KeyAccumulatorENS0_6HandleINS0_7JSProxyEEENS3_INS0_10FixedArrayEEENS0_14PropertyFilterE, .-_ZN2v88internal15FilterProxyKeysEPNS0_14KeyAccumulatorENS0_6HandleINS0_7JSProxyEEENS3_INS0_10FixedArrayEEENS0_14PropertyFilterE
	.section	.text._ZN2v88internal14KeyAccumulator16HasShadowingKeysEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14KeyAccumulator16HasShadowingKeysEv
	.type	_ZN2v88internal14KeyAccumulator16HasShadowingKeysEv, @function
_ZN2v88internal14KeyAccumulator16HasShadowingKeysEv:
.LFB19007:
	.cfi_startproc
	endbr64
	cmpq	$0, 24(%rdi)
	setne	%al
	ret
	.cfi_endproc
.LFE19007:
	.size	_ZN2v88internal14KeyAccumulator16HasShadowingKeysEv, .-_ZN2v88internal14KeyAccumulator16HasShadowingKeysEv
	.section	.text._ZN2v88internal14KeyAccumulator10IsShadowedENS0_6HandleINS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14KeyAccumulator10IsShadowedENS0_6HandleINS0_6ObjectEEE
	.type	_ZN2v88internal14KeyAccumulator10IsShadowedENS0_6HandleINS0_6ObjectEEE, @function
_ZN2v88internal14KeyAccumulator10IsShadowedENS0_6HandleINS0_6ObjectEEE:
.LFB19008:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	testq	%rax, %rax
	je	.L319
	movzbl	42(%rdi), %ecx
	xorl	%r8d, %r8d
	movb	%cl, -69(%rbp)
	testb	%cl, %cl
	je	.L338
.L287:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L339
	addq	$40, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L338:
	.cfi_restore_state
	movq	(%rax), %rbx
	movq	(%rsi), %rax
	movq	%rsi, %r12
	movq	(%rdi), %r14
	testb	$1, %al
	jne	.L289
	sarq	$32, %rax
	movq	%rax, %rdx
	sall	$15, %eax
	subl	%edx, %eax
.L333:
	subl	$1, %eax
	movl	%eax, %edx
	shrl	$12, %edx
	xorl	%edx, %eax
	leal	(%rax,%rax,4), %edx
	movl	%edx, %eax
	shrl	$4, %eax
	xorl	%eax, %edx
	imull	$2057, %edx, %edx
	movl	%edx, %eax
	shrl	$16, %eax
	xorl	%eax, %edx
	andl	$1073741823, %edx
.L314:
	movslq	35(%rbx), %r13
	subq	$1, %rbx
	leal	-1(%r13), %eax
	movl	$1, %r13d
	andl	%eax, %edx
	movl	%eax, -68(%rbp)
	movq	88(%r14), %rax
	leaq	-64(%rbp), %r14
	movl	%edx, %r15d
	movq	%rax, -80(%rbp)
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L341:
	movq	(%r12), %rax
	movq	%r14, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6Object9SameValueES1_@PLT
	testb	%al, %al
	jne	.L340
	leal	(%r15,%r13), %edx
	andl	-68(%rbp), %edx
	addl	$1, %r13d
	movl	%edx, %r15d
.L317:
	leal	40(,%r15,8), %eax
	cltq
	movq	(%rax,%rbx), %rsi
	cmpq	%rsi, -80(%rbp)
	jne	.L341
.L315:
	movzbl	-69(%rbp), %r8d
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L319:
	xorl	%r8d, %r8d
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L289:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	je	.L291
	movq	-1(%rax), %rdx
	cmpw	$64, 11(%rdx)
	jbe	.L337
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	je	.L342
	movq	-1(%rax), %rdx
	cmpw	$66, 11(%rdx)
	je	.L343
	movq	-1(%rax), %rdx
	cmpw	$160, 11(%rdx)
	je	.L344
	leaq	-64(%rbp), %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal10JSReceiver15GetIdentityHashEv@PLT
	movq	%rax, %r8
	notq	%r8
	andl	$1, %r8d
	je	.L287
	shrq	$32, %rax
	movq	%rax, %rdx
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L342:
	movq	15(%rax), %rax
.L337:
	movl	7(%rax), %edx
	testb	$1, %dl
	jne	.L308
	shrl	$2, %edx
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L340:
	cmpl	$-1, %r15d
	setne	-69(%rbp)
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L308:
	leaq	-64(%rbp), %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6String17ComputeAndSetHashEv@PLT
	movl	%eax, %edx
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L291:
	movq	7(%rax), %rdx
	movq	%rdx, %xmm0
	ucomisd	%xmm0, %xmm0
	jp	.L345
	comisd	.LC4(%rip), %xmm0
	jb	.L335
	movsd	.LC5(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L335
	cvttsd2sil	%xmm0, %esi
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%esi, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L335
	jne	.L335
	movl	%esi, %eax
	sall	$15, %eax
	subl	%esi, %eax
	jmp	.L333
.L343:
	movl	7(%rax), %esi
	xorl	%edx, %edx
	andl	$2147483646, %esi
	je	.L314
	movq	15(%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L335:
	movq	%rdx, %rax
	salq	$18, %rax
	subq	%rdx, %rax
	subq	$1, %rax
	movq	%rax, %rdx
	shrq	$31, %rdx
	xorq	%rdx, %rax
	leaq	(%rax,%rax,4), %rdx
	leaq	(%rax,%rdx,4), %rdx
	movq	%rdx, %rax
	shrq	$11, %rax
	xorq	%rax, %rdx
	movq	%rdx, %rax
	salq	$6, %rax
	addq	%rax, %rdx
	movq	%rdx, %rax
	shrq	$22, %rax
	xorq	%rax, %rdx
	andl	$1073741823, %edx
	jmp	.L314
.L344:
	leaq	-64(%rbp), %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal18SharedFunctionInfo4HashEv@PLT
	andl	$2147483647, %eax
	movl	%eax, %edx
	jmp	.L314
.L345:
	movl	$2147483647, %edx
	jmp	.L314
.L339:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19008:
	.size	_ZN2v88internal14KeyAccumulator10IsShadowedENS0_6HandleINS0_6ObjectEEE, .-_ZN2v88internal14KeyAccumulator10IsShadowedENS0_6HandleINS0_6ObjectEEE
	.section	.rodata._ZN2v88internal14KeyAccumulator6AddKeyENS0_6HandleINS0_6ObjectEEENS0_16AddKeyConversionE.str1.1,"aMS",@progbits,1
.LC6:
	.string	"(location_) != nullptr"
	.section	.text._ZN2v88internal14KeyAccumulator6AddKeyENS0_6HandleINS0_6ObjectEEENS0_16AddKeyConversionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14KeyAccumulator6AddKeyENS0_6HandleINS0_6ObjectEEENS0_16AddKeyConversionE
	.type	_ZN2v88internal14KeyAccumulator6AddKeyENS0_6HandleINS0_6ObjectEEENS0_16AddKeyConversionE, @function
_ZN2v88internal14KeyAccumulator6AddKeyENS0_6HandleINS0_6ObjectEEENS0_16AddKeyConversionE:
.LFB18996:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	(%rsi), %rcx
	movl	36(%rdi), %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rcx, %rax
	notq	%rax
	andl	$1, %eax
	cmpl	$64, %edx
	je	.L389
	testb	%al, %al
	je	.L390
.L355:
	andl	$8, %edx
	jne	.L388
.L357:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal14KeyAccumulator10IsShadowedENS0_6HandleINS0_6ObjectEEE
	movl	%eax, %r13d
	testb	%al, %al
	jne	.L388
	movq	8(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L391
.L358:
	cmpl	$1, %r14d
	je	.L392
.L364:
	movq	(%rbx), %rdi
	movq	%r15, %rdx
	call	_ZN2v88internal14OrderedHashSet3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L393
	movq	8(%rbx), %rax
	movq	(%rax), %r13
	cmpq	%r13, (%r12)
	jne	.L394
	.p2align 4,,10
	.p2align 3
.L388:
	movl	$1, %r13d
.L346:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L395
	addq	$56, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L390:
	.cfi_restore_state
	movq	-1(%rcx), %rax
	cmpw	$64, 11(%rax)
	jne	.L355
	andl	$16, %edx
	jne	.L388
	testb	$1, 11(%rcx)
	je	.L357
	jmp	.L388
	.p2align 4,,10
	.p2align 3
.L389:
	testb	%al, %al
	jne	.L388
	movq	-1(%rcx), %rax
	cmpw	$64, 11(%rax)
	jne	.L388
	testb	$16, 11(%rcx)
	jne	.L357
	jmp	.L388
	.p2align 4,,10
	.p2align 3
.L394:
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %r14
	leaq	15(%r13), %rsi
	movq	%r14, 15(%r13)
	testb	$1, %r14b
	je	.L369
	movq	%r14, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L396
.L367:
	testb	$24, %al
	je	.L369
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L397
	.p2align 4,,10
	.p2align 3
.L369:
	movq	%r12, 8(%rbx)
	jmp	.L388
	.p2align 4,,10
	.p2align 3
.L392:
	movq	(%r12), %rax
	testb	$1, %al
	je	.L364
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L387
	movq	%rax, -64(%rbp)
	movl	7(%rax), %eax
	testb	$1, %al
	jne	.L362
	testb	$2, %al
	jne	.L387
.L362:
	leaq	-68(%rbp), %rsi
	leaq	-64(%rbp), %rdi
	call	_ZN2v88internal6String16SlowAsArrayIndexEPj@PLT
	testb	%al, %al
	je	.L387
	movl	-68(%rbp), %esi
	movq	(%rbx), %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory17NewNumberFromUintEjNS0_14AllocationTypeE@PLT
	movq	%rax, %r15
	.p2align 4,,10
	.p2align 3
.L387:
	movq	8(%rbx), %rsi
	jmp	.L364
	.p2align 4,,10
	.p2align 3
.L391:
	movq	(%rbx), %rdi
	movl	$16, %esi
	xorl	%edx, %edx
	call	_ZN2v88internal14OrderedHashSet8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L398
	movq	%rax, 8(%rbx)
	jmp	.L358
	.p2align 4,,10
	.p2align 3
.L393:
	movq	(%rbx), %r12
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$307, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory13NewRangeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L396:
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-88(%rbp), %rsi
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L397:
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L369
.L398:
	leaq	.LC6(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L395:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18996:
	.size	_ZN2v88internal14KeyAccumulator6AddKeyENS0_6HandleINS0_6ObjectEEENS0_16AddKeyConversionE, .-_ZN2v88internal14KeyAccumulator6AddKeyENS0_6HandleINS0_6ObjectEEENS0_16AddKeyConversionE
	.section	.rodata._ZN2v88internal12_GLOBAL__N_130CollectInterceptorKeysInternalENS0_6HandleINS0_10JSReceiverEEENS2_INS0_8JSObjectEEENS2_INS0_15InterceptorInfoEEEPNS0_14KeyAccumulatorENS1_14IndexedOrNamedE.str1.8,"aMS",@progbits,1
	.align 8
.LC7:
	.string	"interceptor-indexed-enumerator"
	.align 8
.LC8:
	.string	"disabled-by-default-v8.runtime"
	.section	.rodata._ZN2v88internal12_GLOBAL__N_130CollectInterceptorKeysInternalENS0_6HandleINS0_10JSReceiverEEENS2_INS0_8JSObjectEEENS2_INS0_15InterceptorInfoEEEPNS0_14KeyAccumulatorENS1_14IndexedOrNamedE.str1.1,"aMS",@progbits,1
.LC9:
	.string	"V8.ExternalCallback"
.LC10:
	.string	"interceptor-named-enumerator"
.LC12:
	.string	"element->ToUint32(&number)"
.LC13:
	.string	"interceptor-indexed-query"
.LC14:
	.string	"element->IsName()"
.LC15:
	.string	"interceptor-named-query"
.LC16:
	.string	"attributes->ToInt32(&value)"
	.section	.text._ZN2v88internal12_GLOBAL__N_130CollectInterceptorKeysInternalENS0_6HandleINS0_10JSReceiverEEENS2_INS0_8JSObjectEEENS2_INS0_15InterceptorInfoEEEPNS0_14KeyAccumulatorENS1_14IndexedOrNamedE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_130CollectInterceptorKeysInternalENS0_6HandleINS0_10JSReceiverEEENS2_INS0_8JSObjectEEENS2_INS0_15InterceptorInfoEEEPNS0_14KeyAccumulatorENS1_14IndexedOrNamedE, @function
_ZN2v88internal12_GLOBAL__N_130CollectInterceptorKeysInternalENS0_6HandleINS0_10JSReceiverEEENS2_INS0_8JSObjectEEENS2_INS0_15InterceptorInfoEEEPNS0_14KeyAccumulatorENS1_14IndexedOrNamedE:
.LFB19022:
	.cfi_startproc
	movabsq	$4294967297, %r9
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$376, %rsp
	movq	%rdi, -360(%rbp)
	movq	(%rcx), %r12
	movq	%rsi, -368(%rbp)
	movq	%rcx, -352(%rbp)
	movq	(%rdi), %rcx
	leaq	-224(%rbp), %rdi
	movl	%r8d, -340(%rbp)
	movq	(%rsi), %r8
	movq	%r12, %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	movq	63(%rax), %rdx
	call	_ZN2v88internal25PropertyCallbackArgumentsC1EPNS0_7IsolateENS0_6ObjectES4_NS0_8JSObjectENS_5MaybeINS0_11ShouldThrowEEE@PLT
	movq	(%rbx), %rax
	movq	47(%rax), %rax
	cmpq	%rax, 88(%r12)
	je	.L694
	movq	-184(%rbp), %rax
	movl	-340(%rbp), %ecx
	movq	41016(%rax), %r13
	movq	%r13, %rdi
	testl	%ecx, %ecx
	je	.L695
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	jne	.L696
.L427:
	pxor	%xmm0, %xmm0
	movq	-184(%rbp), %r15
	movq	$0, -256(%rbp)
	movaps	%xmm0, -288(%rbp)
	movaps	%xmm0, -272(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L697
.L428:
	movq	(%rbx), %rax
	movq	47(%rax), %rax
	cmpq	_ZN2v88internal3Smi5kZeroE(%rip), %rax
	je	.L518
	movq	7(%rax), %r8
	movq	%r8, %r14
.L429:
	cmpl	$32, 41828(%r15)
	je	.L698
.L430:
	movl	12616(%r15), %r13d
	movl	$6, 12616(%r15)
	movq	%r15, -320(%rbp)
	movq	%r8, -312(%rbp)
	movq	12608(%r15), %rax
	movq	%rax, -304(%rbp)
	leaq	-320(%rbp), %rax
	movq	%rax, 12608(%r15)
	movq	_ZZN2v88internal21ExternalCallbackScopeC4EPNS0_7IsolateEmE27trace_event_unique_atomic62(%rip), %rdx
	testq	%rdx, %rdx
	je	.L699
.L433:
	movzbl	(%rdx), %eax
	testb	$5, %al
	jne	.L700
.L435:
	leaq	-200(%rbp), %rax
	leaq	-328(%rbp), %rdi
	movq	%rax, -328(%rbp)
	call	*%r14
	movq	96(%r15), %rax
	xorl	%r14d, %r14d
	cmpq	%rax, -168(%rbp)
	je	.L439
	leaq	-168(%rbp), %r14
.L439:
	movq	-320(%rbp), %rax
	movq	-304(%rbp), %rdx
	movq	%rdx, 12608(%rax)
	movq	_ZZN2v88internal21ExternalCallbackScopeD4EvE27trace_event_unique_atomic68(%rip), %rdx
	movq	%rdx, %r11
	testq	%rdx, %rdx
	je	.L701
.L441:
	movzbl	(%r11), %eax
	testb	$5, %al
	jne	.L702
.L443:
	movl	%r13d, 12616(%r15)
.L431:
	movq	-288(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L703
.L426:
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	jne	.L401
	testq	%r14, %r14
	je	.L402
	movq	(%r14), %rsi
	movq	-352(%rbp), %rcx
	leaq	-1(%rsi), %rax
	testb	$2, 36(%rcx)
	je	.L450
	movq	(%rbx), %rdx
	movq	23(%rdx), %rcx
	cmpq	%rcx, 88(%r12)
	je	.L450
	movq	(%rax), %rax
	movq	_ZN2v88internal16ElementsAccessor19elements_accessors_E(%rip), %rdx
	xorl	%r12d, %r12d
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	movq	(%rdx,%rax,8), %r13
	movq	15(%rsi), %rdx
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*184(%rax)
	movl	%eax, -344(%rbp)
	testl	%eax, %eax
	je	.L402
	movq	%rbx, -384(%rbp)
	movq	-392(%rbp), %rbx
	.p2align 4,,10
	.p2align 3
.L451:
	movq	0(%r13), %rax
	movq	(%r14), %rsi
	movl	%r12d, %edx
	movq	%r13, %rdi
	call	*32(%rax)
	testb	%al, %al
	je	.L453
	movb	$1, %bl
	leaq	-144(%rbp), %rdi
	movabsq	$4294967296, %rax
	movl	%ebx, %ebx
	orq	%rax, %rbx
	movq	-360(%rbp), %rax
	movq	%rbx, %r9
	movq	(%rax), %rcx
	movq	-384(%rbp), %rax
	movq	(%rax), %rax
	movq	63(%rax), %rdx
	movq	-368(%rbp), %rax
	movq	(%rax), %r8
	movq	-352(%rbp), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal25PropertyCallbackArgumentsC1EPNS0_7IsolateENS0_6ObjectES4_NS0_8JSObjectENS_5MaybeINS0_11ShouldThrowEEE@PLT
	movq	0(%r13), %rax
	movl	%r12d, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	*40(%rax)
	movl	-340(%rbp), %edx
	movq	%rax, -376(%rbp)
	testl	%edx, %edx
	je	.L704
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L705
.L486:
	leaq	.LC14(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L695:
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	jne	.L706
.L405:
	pxor	%xmm0, %xmm0
	movq	-184(%rbp), %r15
	movq	$0, -256(%rbp)
	movaps	%xmm0, -288(%rbp)
	movaps	%xmm0, -272(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	je	.L428
	movq	40960(%r15), %rdi
	leaq	-280(%rbp), %rsi
	movl	$157, %edx
	addq	$23240, %rdi
	movq	%rdi, -288(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	movq	-184(%rbp), %r15
	jmp	.L428
	.p2align 4,,10
	.p2align 3
.L450:
	movq	(%rax), %rax
	movq	_ZN2v88internal16ElementsAccessor19elements_accessors_E(%rip), %rdx
	movl	$1, %ecx
	movq	%r14, %rsi
	subl	-340(%rbp), %ecx
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	movq	(%rdx,%rax,8), %rdi
	movq	-352(%rbp), %rdx
	movq	(%rdi), %rax
	call	*96(%rax)
	testb	%al, %al
	je	.L707
.L402:
	movl	$257, %eax
.L403:
	movq	-216(%rbp), %rdx
	movq	-208(%rbp), %rcx
	movq	%rcx, 41704(%rdx)
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L708
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L694:
	.cfi_restore_state
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	je	.L402
.L401:
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	xorl	%eax, %eax
	movb	$0, %ah
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L698:
	movq	%r8, -376(%rbp)
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	41472(%r15), %rdi
	call	_ZN2v88internal5Debug33PerformSideEffectCheckForCallbackENS0_6HandleINS0_6ObjectEEES4_NS1_12AccessorKindE@PLT
	movq	-376(%rbp), %r8
	testb	%al, %al
	jne	.L430
	xorl	%r14d, %r14d
	jmp	.L431
	.p2align 4,,10
	.p2align 3
.L707:
	xorl	%eax, %eax
	movb	$0, %ah
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L704:
	movq	(%rax), %r15
	testb	$1, %r15b
	jne	.L455
	sarq	$32, %r15
	js	.L457
	movl	%r15d, -412(%rbp)
.L458:
	pxor	%xmm0, %xmm0
	movq	-104(%rbp), %r15
	movq	$0, -256(%rbp)
	movaps	%xmm0, -288(%rbp)
	movaps	%xmm0, -272(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L709
.L464:
	movq	-384(%rbp), %rax
	movq	(%rax), %rax
	movq	23(%rax), %rax
	cmpq	_ZN2v88internal3Smi5kZeroE(%rip), %rax
	je	.L523
	movq	7(%rax), %r8
	movq	%r8, -392(%rbp)
.L465:
	cmpl	$32, 41828(%r15)
	je	.L710
.L466:
	movl	12616(%r15), %eax
	movl	$6, 12616(%r15)
	movq	%r15, -320(%rbp)
	movq	%r8, -312(%rbp)
	movl	%eax, -400(%rbp)
	movq	12608(%r15), %rax
	movq	%rax, -304(%rbp)
	leaq	-320(%rbp), %rax
	movq	%rax, 12608(%r15)
	movq	_ZZN2v88internal21ExternalCallbackScopeC4EPNS0_7IsolateEmE27trace_event_unique_atomic62(%rip), %rdx
	testq	%rdx, %rdx
	je	.L711
.L469:
	movzbl	(%rdx), %eax
	testb	$5, %al
	jne	.L712
.L471:
	leaq	-120(%rbp), %rax
	movq	%rax, -328(%rbp)
	movq	41016(%r15), %rdi
	movq	%rdi, -408(%rbp)
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	movq	-408(%rbp), %rdi
	testb	%al, %al
	jne	.L713
.L475:
	movq	-392(%rbp), %rax
	movl	-412(%rbp), %edi
	leaq	-328(%rbp), %rsi
	call	*%rax
	movq	96(%r15), %rax
	xorl	%r10d, %r10d
	cmpq	%rax, -88(%rbp)
	je	.L500
.L678:
	leaq	-88(%rbp), %r10
.L500:
	movq	-320(%rbp), %rax
	movq	-304(%rbp), %rdx
	movq	%rdx, 12608(%rax)
	movq	_ZZN2v88internal21ExternalCallbackScopeD4EvE27trace_event_unique_atomic68(%rip), %rdx
	testq	%rdx, %rdx
	je	.L714
.L502:
	movzbl	(%rdx), %eax
	testb	$5, %al
	jne	.L715
.L504:
	movl	-400(%rbp), %eax
	movl	%eax, 12616(%r15)
.L491:
	movq	-288(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L716
.L485:
	testq	%r10, %r10
	je	.L509
	movq	(%r10), %rax
	leaq	-320(%rbp), %rsi
	leaq	-288(%rbp), %rdi
	movq	%rax, -288(%rbp)
	call	_ZN2v88internal6Object7ToInt32EPi@PLT
	testb	%al, %al
	je	.L717
	testb	$2, -320(%rbp)
	je	.L718
.L509:
	movq	-136(%rbp), %rax
	movq	-128(%rbp), %rdx
	movq	%rdx, 41704(%rax)
.L453:
	addl	$1, %r12d
	cmpl	%r12d, -344(%rbp)
	jne	.L451
	jmp	.L402
	.p2align 4,,10
	.p2align 3
.L702:
	pxor	%xmm0, %xmm0
	movq	%r11, -376(%rbp)
	movaps	%xmm0, -144(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	-376(%rbp), %r11
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %r10
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rax
	cmpq	%rax, %r10
	jne	.L719
.L444:
	movq	-136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L445
	movq	(%rdi), %rax
	call	*8(%rax)
.L445:
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L443
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L443
	.p2align 4,,10
	.p2align 3
.L701:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r11
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L720
.L442:
	movq	%r11, _ZZN2v88internal21ExternalCallbackScopeD4EvE27trace_event_unique_atomic68(%rip)
	jmp	.L441
	.p2align 4,,10
	.p2align 3
.L700:
	pxor	%xmm0, %xmm0
	movq	%rdx, -376(%rbp)
	movaps	%xmm0, -144(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	-376(%rbp), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %r10
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rax
	cmpq	%rax, %r10
	jne	.L721
.L436:
	movq	-136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L437
	movq	(%rdi), %rax
	call	*8(%rax)
.L437:
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L435
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L699:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L722
.L434:
	movq	%rdx, _ZZN2v88internal21ExternalCallbackScopeC4EPNS0_7IsolateEmE27trace_event_unique_atomic62(%rip)
	jmp	.L433
	.p2align 4,,10
	.p2align 3
.L715:
	pxor	%xmm0, %xmm0
	movq	%rdx, -408(%rbp)
	movq	%r10, -392(%rbp)
	movaps	%xmm0, -240(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	-392(%rbp), %r10
	movq	-408(%rbp), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %r11
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rax
	cmpq	%rax, %r11
	jne	.L723
.L505:
	movq	-232(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L506
	movq	%r10, -392(%rbp)
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	-392(%rbp), %r10
.L506:
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L504
	movq	%r10, -392(%rbp)
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	-392(%rbp), %r10
	jmp	.L504
	.p2align 4,,10
	.p2align 3
.L714:
	movq	%r10, -392(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rsi
	movq	-392(%rbp), %r10
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L724
.L503:
	movq	%rdx, _ZZN2v88internal21ExternalCallbackScopeD4EvE27trace_event_unique_atomic68(%rip)
	jmp	.L502
	.p2align 4,,10
	.p2align 3
.L696:
	movq	-192(%rbp), %rdx
	leaq	.LC10(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal6Logger15ApiObjectAccessEPKcNS0_8JSObjectE@PLT
	jmp	.L427
	.p2align 4,,10
	.p2align 3
.L706:
	movq	-192(%rbp), %rdx
	leaq	.LC7(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal6Logger15ApiObjectAccessEPKcNS0_8JSObjectE@PLT
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L705:
	movq	-1(%rax), %rax
	cmpw	$64, 11(%rax)
	ja	.L486
	pxor	%xmm0, %xmm0
	movq	-104(%rbp), %r15
	movq	$0, -256(%rbp)
	movaps	%xmm0, -288(%rbp)
	movaps	%xmm0, -272(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L725
.L488:
	movq	-384(%rbp), %rax
	movq	(%rax), %rax
	movq	23(%rax), %rax
	cmpq	_ZN2v88internal3Smi5kZeroE(%rip), %rax
	je	.L527
	movq	7(%rax), %r8
	movq	%r8, -392(%rbp)
.L489:
	cmpl	$32, 41828(%r15)
	je	.L726
.L490:
	movl	12616(%r15), %eax
	movl	$6, 12616(%r15)
	movq	%r15, -320(%rbp)
	movq	%r8, -312(%rbp)
	movl	%eax, -400(%rbp)
	movq	12608(%r15), %rax
	movq	%rax, -304(%rbp)
	leaq	-320(%rbp), %rax
	movq	%rax, 12608(%r15)
	movq	_ZZN2v88internal21ExternalCallbackScopeC4EPNS0_7IsolateEmE27trace_event_unique_atomic62(%rip), %rdx
	testq	%rdx, %rdx
	je	.L727
.L493:
	movzbl	(%rdx), %eax
	testb	$5, %al
	jne	.L728
.L495:
	leaq	-120(%rbp), %rax
	movq	%rax, -328(%rbp)
	movq	41016(%r15), %rdi
	movq	%rdi, -408(%rbp)
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	movq	-408(%rbp), %rdi
	testb	%al, %al
	jne	.L729
.L499:
	movq	-392(%rbp), %rax
	movq	-376(%rbp), %rdi
	leaq	-328(%rbp), %rsi
	call	*%rax
	movq	96(%r15), %rax
	xorl	%r10d, %r10d
	cmpq	%rax, -88(%rbp)
	jne	.L678
	jmp	.L500
	.p2align 4,,10
	.p2align 3
.L726:
	movq	41472(%r15), %rdi
	movq	-384(%rbp), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r8, -400(%rbp)
	call	_ZN2v88internal5Debug33PerformSideEffectCheckForCallbackENS0_6HandleINS0_6ObjectEEES4_NS1_12AccessorKindE@PLT
	movq	-400(%rbp), %r8
	testb	%al, %al
	jne	.L490
.L656:
	xorl	%r10d, %r10d
	jmp	.L491
	.p2align 4,,10
	.p2align 3
.L729:
	movq	-376(%rbp), %rax
	movq	-112(%rbp), %rdx
	leaq	.LC15(%rip), %rsi
	movq	(%rax), %rcx
	call	_ZN2v88internal6Logger22ApiNamedPropertyAccessEPKcNS0_8JSObjectENS0_6ObjectE@PLT
	jmp	.L499
	.p2align 4,,10
	.p2align 3
.L728:
	pxor	%xmm0, %xmm0
	movq	%rdx, -408(%rbp)
	movaps	%xmm0, -240(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	-408(%rbp), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %r11
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rax
	cmpq	%rax, %r11
	jne	.L730
.L496:
	movq	-232(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L497
	movq	(%rdi), %rax
	call	*8(%rax)
.L497:
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L495
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L495
	.p2align 4,,10
	.p2align 3
.L727:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rsi
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L731
.L494:
	movq	%rdx, _ZZN2v88internal21ExternalCallbackScopeC4EPNS0_7IsolateEmE27trace_event_unique_atomic62(%rip)
	jmp	.L493
	.p2align 4,,10
	.p2align 3
.L703:
	leaq	-280(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L426
	.p2align 4,,10
	.p2align 3
.L518:
	xorl	%r8d, %r8d
	xorl	%r14d, %r14d
	jmp	.L429
	.p2align 4,,10
	.p2align 3
.L718:
	movq	-376(%rbp), %rsi
	movq	-352(%rbp), %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal14KeyAccumulator6AddKeyENS0_6HandleINS0_6ObjectEEENS0_16AddKeyConversionE
	testb	%al, %al
	jne	.L509
	movq	-136(%rbp), %rax
	movq	-128(%rbp), %rdx
	movq	%rdx, 41704(%rax)
	xorl	%eax, %eax
	movb	$0, %ah
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L455:
	movq	-1(%r15), %rax
	cmpw	$65, 11(%rax)
	je	.L732
.L457:
	leaq	.LC12(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L710:
	movq	41472(%r15), %rdi
	movq	-384(%rbp), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r8, -400(%rbp)
	call	_ZN2v88internal5Debug33PerformSideEffectCheckForCallbackENS0_6HandleINS0_6ObjectEEES4_NS1_12AccessorKindE@PLT
	movq	-400(%rbp), %r8
	testb	%al, %al
	jne	.L466
	jmp	.L656
	.p2align 4,,10
	.p2align 3
.L716:
	leaq	-280(%rbp), %rsi
	movq	%r10, -392(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	movq	-392(%rbp), %r10
	jmp	.L485
	.p2align 4,,10
	.p2align 3
.L711:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rsi
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L733
.L470:
	movq	%rdx, _ZZN2v88internal21ExternalCallbackScopeC4EPNS0_7IsolateEmE27trace_event_unique_atomic62(%rip)
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L713:
	movl	-412(%rbp), %ecx
	movq	-112(%rbp), %rdx
	leaq	.LC13(%rip), %rsi
	call	_ZN2v88internal6Logger24ApiIndexedPropertyAccessEPKcNS0_8JSObjectEj@PLT
	jmp	.L475
	.p2align 4,,10
	.p2align 3
.L712:
	pxor	%xmm0, %xmm0
	movq	%rdx, -408(%rbp)
	movaps	%xmm0, -240(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	-408(%rbp), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %r11
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rax
	cmpq	%rax, %r11
	jne	.L734
.L472:
	movq	-232(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L473
	movq	(%rdi), %rax
	call	*8(%rax)
.L473:
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L471
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L471
	.p2align 4,,10
	.p2align 3
.L722:
	leaq	.LC8(%rip), %rsi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L434
	.p2align 4,,10
	.p2align 3
.L721:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$66, %esi
	leaq	-144(%rbp), %rax
	pushq	$0
	leaq	.LC9(%rip), %rcx
	pushq	%rax
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%r10
	addq	$64, %rsp
	jmp	.L436
	.p2align 4,,10
	.p2align 3
.L720:
	leaq	.LC8(%rip), %rsi
	call	*%rax
	movq	%rax, %r11
	jmp	.L442
	.p2align 4,,10
	.p2align 3
.L719:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r11, %rdx
	leaq	-144(%rbp), %rax
	pushq	$0
	movl	$69, %esi
	leaq	.LC9(%rip), %rcx
	pushq	%rax
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%r10
	addq	$64, %rsp
	jmp	.L444
	.p2align 4,,10
	.p2align 3
.L697:
	movq	40960(%r15), %rdi
	leaq	-280(%rbp), %rsi
	movl	$172, %edx
	addq	$23240, %rdi
	movq	%rdi, -288(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	movq	-184(%rbp), %r15
	jmp	.L428
	.p2align 4,,10
	.p2align 3
.L725:
	movq	40960(%r15), %rax
	leaq	-280(%rbp), %rsi
	movl	$174, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -288(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L488
	.p2align 4,,10
	.p2align 3
.L527:
	movq	$0, -392(%rbp)
	xorl	%r8d, %r8d
	jmp	.L489
	.p2align 4,,10
	.p2align 3
.L724:
	leaq	.LC8(%rip), %rsi
	call	*%rax
	movq	-392(%rbp), %r10
	movq	%rax, %rdx
	jmp	.L503
	.p2align 4,,10
	.p2align 3
.L723:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$69, %esi
	leaq	-240(%rbp), %rax
	pushq	$0
	leaq	.LC9(%rip), %rcx
	pushq	%rax
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%r11
	movq	-392(%rbp), %r10
	addq	$64, %rsp
	jmp	.L505
	.p2align 4,,10
	.p2align 3
.L717:
	leaq	.LC16(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L732:
	movsd	7(%r15), %xmm1
	movsd	.LC11(%rip), %xmm2
	addsd	%xmm1, %xmm2
	movq	%xmm2, %rdx
	movq	%xmm2, %rax
	shrq	$32, %rdx
	cmpq	$1127219200, %rdx
	jne	.L457
	movl	%eax, %eax
	pxor	%xmm0, %xmm0
	movd	%xmm2, -412(%rbp)
	cvtsi2sdq	%rax, %xmm0
	ucomisd	%xmm0, %xmm1
	jp	.L457
	je	.L458
	jmp	.L457
	.p2align 4,,10
	.p2align 3
.L731:
	leaq	.LC8(%rip), %rsi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L494
	.p2align 4,,10
	.p2align 3
.L730:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$66, %esi
	leaq	-240(%rbp), %rax
	pushq	$0
	leaq	.LC9(%rip), %rcx
	pushq	%rax
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%r11
	addq	$64, %rsp
	jmp	.L496
	.p2align 4,,10
	.p2align 3
.L709:
	movq	40960(%r15), %rax
	leaq	-280(%rbp), %rsi
	movl	$159, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -288(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L464
	.p2align 4,,10
	.p2align 3
.L523:
	movq	$0, -392(%rbp)
	xorl	%r8d, %r8d
	jmp	.L465
.L734:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$66, %esi
	leaq	-240(%rbp), %rax
	pushq	$0
	leaq	.LC9(%rip), %rcx
	pushq	%rax
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%r11
	addq	$64, %rsp
	jmp	.L472
.L733:
	leaq	.LC8(%rip), %rsi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L470
.L708:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19022:
	.size	_ZN2v88internal12_GLOBAL__N_130CollectInterceptorKeysInternalENS0_6HandleINS0_10JSReceiverEEENS2_INS0_8JSObjectEEENS2_INS0_15InterceptorInfoEEEPNS0_14KeyAccumulatorENS1_14IndexedOrNamedE, .-_ZN2v88internal12_GLOBAL__N_130CollectInterceptorKeysInternalENS0_6HandleINS0_10JSReceiverEEENS2_INS0_8JSObjectEEENS2_INS0_15InterceptorInfoEEEPNS0_14KeyAccumulatorENS1_14IndexedOrNamedE
	.section	.text._ZN2v88internal12_GLOBAL__N_122CollectInterceptorKeysENS0_6HandleINS0_10JSReceiverEEENS2_INS0_8JSObjectEEEPNS0_14KeyAccumulatorENS1_14IndexedOrNamedE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_122CollectInterceptorKeysENS0_6HandleINS0_10JSReceiverEEENS2_INS0_8JSObjectEEEPNS0_14KeyAccumulatorENS1_14IndexedOrNamedE, @function
_ZN2v88internal12_GLOBAL__N_122CollectInterceptorKeysENS0_6HandleINS0_10JSReceiverEEENS2_INS0_8JSObjectEEEPNS0_14KeyAccumulatorENS1_14IndexedOrNamedE:
.LFB19023:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$24, %rsp
	movq	(%rsi), %rax
	movq	(%rdx), %r15
	movq	-1(%rax), %rdx
	testl	%ecx, %ecx
	jne	.L736
	testb	$8, 13(%rdx)
	je	.L761
	movq	-1(%rax), %rax
.L771:
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L774
.L744:
	movq	71(%rax), %rdx
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-37504(%rax), %rsi
	cmpq	%rsi, %rdx
	je	.L746
	movq	39(%rdx), %rsi
.L746:
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L752
.L776:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
	testb	$32, 36(%r12)
	je	.L755
.L778:
	movq	(%rdx), %rax
	testb	$2, 75(%rax)
	jne	.L755
.L761:
	addq	$24, %rsp
	movl	$257, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L736:
	.cfi_restore_state
	testb	$4, 13(%rdx)
	je	.L761
	movq	-1(%rax), %rax
.L773:
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L775
.L750:
	movq	71(%rax), %rdx
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-37504(%rax), %rsi
	cmpq	%rsi, %rdx
	je	.L746
	movq	41112(%r15), %rdi
	movq	31(%rdx), %rsi
	testq	%rdi, %rdi
	jne	.L776
.L752:
	movq	41088(%r15), %rdx
	cmpq	41096(%r15), %rdx
	je	.L777
.L754:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rdx)
	testb	$32, 36(%r12)
	jne	.L778
.L755:
	addq	$24, %rsp
	movl	%ebx, %r8d
	movq	%r12, %rcx
	movq	%r13, %rsi
	popq	%rbx
	movq	%r14, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12_GLOBAL__N_130CollectInterceptorKeysInternalENS0_6HandleINS0_10JSReceiverEEENS2_INS0_8JSObjectEEENS2_INS0_15InterceptorInfoEEEPNS0_14KeyAccumulatorENS1_14IndexedOrNamedE
	.p2align 4,,10
	.p2align 3
.L774:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	leaq	-1(%rax), %rcx
	cmpw	$68, 11(%rdx)
	je	.L771
	movq	(%rcx), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L744
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	jmp	.L744
	.p2align 4,,10
	.p2align 3
.L775:
	movq	-1(%rax), %rdx
	leaq	-1(%rax), %rcx
	cmpw	$68, 11(%rdx)
	je	.L773
	movq	(%rcx), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L750
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	jmp	.L750
	.p2align 4,,10
	.p2align 3
.L777:
	movq	%r15, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L754
	.cfi_endproc
.LFE19023:
	.size	_ZN2v88internal12_GLOBAL__N_122CollectInterceptorKeysENS0_6HandleINS0_10JSReceiverEEENS2_INS0_8JSObjectEEEPNS0_14KeyAccumulatorENS1_14IndexedOrNamedE, .-_ZN2v88internal12_GLOBAL__N_122CollectInterceptorKeysENS0_6HandleINS0_10JSReceiverEEENS2_INS0_8JSObjectEEEPNS0_14KeyAccumulatorENS1_14IndexedOrNamedE
	.section	.text._ZN2v88internal14KeyAccumulator7AddKeysENS0_6HandleINS0_10FixedArrayEEENS0_16AddKeyConversionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14KeyAccumulator7AddKeysENS0_6HandleINS0_10FixedArrayEEENS0_16AddKeyConversionE
	.type	_ZN2v88internal14KeyAccumulator7AddKeysENS0_6HandleINS0_10FixedArrayEEENS0_16AddKeyConversionE, @function
_ZN2v88internal14KeyAccumulator7AddKeysENS0_6HandleINS0_10FixedArrayEEENS0_16AddKeyConversionE:
.LFB18999:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, -92(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movslq	11(%rax), %rdx
	testq	%rdx, %rdx
	jle	.L780
	subl	$1, %edx
	movq	%rdi, %r14
	movq	%rsi, %rbx
	movl	$16, %r13d
	leaq	24(,%rdx,8), %rcx
	movq	%rcx, -88(%rbp)
	leaq	-68(%rbp), %rcx
	movq	%rcx, -112(%rbp)
	.p2align 4,,10
	.p2align 3
.L807:
	movq	(%r14), %r15
	movq	-1(%rax,%r13), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L781
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r12
.L782:
	movq	%rsi, %rax
	movl	36(%r14), %edx
	notq	%rax
	andl	$1, %eax
	cmpl	$64, %edx
	je	.L829
	testb	%al, %al
	je	.L830
.L792:
	andl	$8, %edx
	jne	.L786
.L794:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal14KeyAccumulator10IsShadowedENS0_6HandleINS0_6ObjectEEE
	movl	%eax, %r15d
	testb	%al, %al
	jne	.L786
	movq	8(%r14), %rsi
	testq	%rsi, %rsi
	je	.L831
.L795:
	cmpl	$1, -92(%rbp)
	je	.L832
.L801:
	movq	(%r14), %rdi
	movq	%r12, %rdx
	call	_ZN2v88internal14OrderedHashSet3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L833
	movq	8(%r14), %rax
	movq	(%rax), %r15
	cmpq	(%r12), %r15
	je	.L786
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %rdx
	leaq	15(%r15), %rsi
	movq	%rdx, 15(%r15)
	testb	$1, %dl
	je	.L808
	movq	%rdx, %r9
	andq	$-262144, %r9
	movq	8(%r9), %rax
	movq	%r9, -104(%rbp)
	testl	$262144, %eax
	jne	.L834
	testb	$24, %al
	je	.L808
.L839:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L835
	.p2align 4,,10
	.p2align 3
.L808:
	movq	%r12, 8(%r14)
	.p2align 4,,10
	.p2align 3
.L786:
	addq	$8, %r13
	cmpq	%r13, -88(%rbp)
	je	.L780
.L837:
	movq	(%rbx), %rax
	jmp	.L807
	.p2align 4,,10
	.p2align 3
.L781:
	movq	41088(%r15), %r12
	cmpq	41096(%r15), %r12
	je	.L836
.L783:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r12)
	jmp	.L782
	.p2align 4,,10
	.p2align 3
.L829:
	testb	%al, %al
	jne	.L786
	movq	-1(%rsi), %rax
	cmpw	$64, 11(%rax)
	jne	.L786
	movq	(%r12), %rax
	testb	$16, 11(%rax)
	jne	.L794
	addq	$8, %r13
	cmpq	%r13, -88(%rbp)
	jne	.L837
	.p2align 4,,10
	.p2align 3
.L780:
	movl	$1, %r15d
.L779:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L838
	addq	$88, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L830:
	.cfi_restore_state
	movq	-1(%rsi), %rax
	cmpw	$64, 11(%rax)
	jne	.L792
	andl	$16, %edx
	jne	.L786
	movq	(%r12), %rax
	testb	$1, 11(%rax)
	je	.L794
	jmp	.L786
	.p2align 4,,10
	.p2align 3
.L832:
	movq	(%r12), %rax
	testb	$1, %al
	je	.L801
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L827
	movq	%rax, -64(%rbp)
	movl	7(%rax), %eax
	testb	$1, %al
	jne	.L799
	testb	$2, %al
	jne	.L827
.L799:
	movq	-112(%rbp), %rsi
	leaq	-64(%rbp), %rdi
	call	_ZN2v88internal6String16SlowAsArrayIndexEPj@PLT
	testb	%al, %al
	jne	.L800
.L827:
	movq	8(%r14), %rsi
	jmp	.L801
	.p2align 4,,10
	.p2align 3
.L836:
	movq	%r15, %rdi
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L783
	.p2align 4,,10
	.p2align 3
.L834:
	movq	%r15, %rdi
	movq	%rdx, -128(%rbp)
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-104(%rbp), %r9
	movq	-128(%rbp), %rdx
	movq	-120(%rbp), %rsi
	movq	8(%r9), %rax
	testb	$24, %al
	jne	.L839
	jmp	.L808
	.p2align 4,,10
	.p2align 3
.L831:
	movq	(%r14), %rdi
	movl	$16, %esi
	xorl	%edx, %edx
	call	_ZN2v88internal14OrderedHashSet8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L840
	movq	%rax, 8(%r14)
	jmp	.L795
	.p2align 4,,10
	.p2align 3
.L833:
	movq	(%r14), %r12
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$307, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory13NewRangeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	jmp	.L779
	.p2align 4,,10
	.p2align 3
.L835:
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L808
.L800:
	movl	-68(%rbp), %esi
	movq	(%r14), %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory17NewNumberFromUintEjNS0_14AllocationTypeE@PLT
	movq	%rax, %r12
	jmp	.L827
.L840:
	leaq	.LC6(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L838:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18999:
	.size	_ZN2v88internal14KeyAccumulator7AddKeysENS0_6HandleINS0_10FixedArrayEEENS0_16AddKeyConversionE, .-_ZN2v88internal14KeyAccumulator7AddKeysENS0_6HandleINS0_10FixedArrayEEENS0_16AddKeyConversionE
	.section	.text._ZN2v88internal14KeyAccumulator18AddKeysFromJSProxyENS0_6HandleINS0_7JSProxyEEENS2_INS0_10FixedArrayEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14KeyAccumulator18AddKeysFromJSProxyENS0_6HandleINS0_7JSProxyEEENS2_INS0_10FixedArrayEEE
	.type	_ZN2v88internal14KeyAccumulator18AddKeysFromJSProxyENS0_6HandleINS0_7JSProxyEEENS2_INS0_10FixedArrayEEE, @function
_ZN2v88internal14KeyAccumulator18AddKeysFromJSProxyENS0_6HandleINS0_7JSProxyEEENS2_INS0_10FixedArrayEEE:
.LFB19005:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movzbl	40(%rdi), %ecx
	testb	%cl, %cl
	je	.L850
.L846:
	movq	%r8, %rsi
	movzbl	%cl, %edx
	call	_ZN2v88internal14KeyAccumulator7AddKeysENS0_6HandleINS0_10FixedArrayEEENS0_16AddKeyConversionE
	movl	%eax, %r8d
	movl	$257, %eax
	testb	%r8b, %r8b
	jne	.L844
	xorb	%al, %al
	movb	$0, %ah
.L844:
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L850:
	.cfi_restore_state
	movl	36(%rdi), %ecx
	movq	%rdi, -8(%rbp)
	call	_ZN2v88internal15FilterProxyKeysEPNS0_14KeyAccumulatorENS0_6HandleINS0_7JSProxyEEENS3_INS0_10FixedArrayEEENS0_14PropertyFilterE
	movq	-8(%rbp), %rdi
	movq	%rax, %r8
	xorl	%eax, %eax
	testq	%r8, %r8
	movb	$0, %ah
	je	.L844
	movl	32(%rdi), %eax
	testl	%eax, %eax
	jne	.L851
	movq	%r8, 8(%rdi)
	movl	$257, %eax
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L851:
	.cfi_restore_state
	movzbl	40(%rdi), %ecx
	jmp	.L846
	.cfi_endproc
.LFE19005:
	.size	_ZN2v88internal14KeyAccumulator18AddKeysFromJSProxyENS0_6HandleINS0_7JSProxyEEENS2_INS0_10FixedArrayEEE, .-_ZN2v88internal14KeyAccumulator18AddKeysFromJSProxyENS0_6HandleINS0_7JSProxyEEENS2_INS0_10FixedArrayEEE
	.section	.text._ZN2v88internal14KeyAccumulator6AddKeyENS0_6ObjectENS0_16AddKeyConversionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14KeyAccumulator6AddKeyENS0_6ObjectENS0_16AddKeyConversionE
	.type	_ZN2v88internal14KeyAccumulator6AddKeyENS0_6ObjectENS0_16AddKeyConversionE, @function
_ZN2v88internal14KeyAccumulator6AddKeyENS0_6ObjectENS0_16AddKeyConversionE:
.LFB18995:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L853
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r12
.L854:
	movq	%rsi, %rax
	movl	36(%rbx), %edx
	notq	%rax
	andl	$1, %eax
	cmpl	$64, %edx
	je	.L898
	testb	%al, %al
	je	.L899
.L864:
	andl	$8, %edx
	jne	.L897
.L866:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal14KeyAccumulator10IsShadowedENS0_6HandleINS0_6ObjectEEE
	movl	%eax, %r13d
	testb	%al, %al
	jne	.L897
	movq	8(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L900
.L867:
	cmpl	$1, %r14d
	je	.L901
.L873:
	movq	(%rbx), %rdi
	movq	%r12, %rdx
	call	_ZN2v88internal14OrderedHashSet3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L902
	movq	8(%rbx), %rax
	movq	(%rax), %r13
	cmpq	(%r12), %r13
	jne	.L903
	.p2align 4,,10
	.p2align 3
.L897:
	movl	$1, %r13d
.L852:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L904
	addq	$56, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L899:
	.cfi_restore_state
	movq	-1(%rsi), %rax
	cmpw	$64, 11(%rax)
	jne	.L864
	andl	$16, %edx
	jne	.L897
	movq	(%r12), %rax
	testb	$1, 11(%rax)
	je	.L866
	jmp	.L897
	.p2align 4,,10
	.p2align 3
.L853:
	movq	41088(%r15), %r12
	movq	%rsi, %r13
	cmpq	41096(%r15), %r12
	je	.L905
.L855:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r15)
	movq	%r13, (%r12)
	jmp	.L854
	.p2align 4,,10
	.p2align 3
.L898:
	testb	%al, %al
	jne	.L897
	movq	-1(%rsi), %rax
	cmpw	$64, 11(%rax)
	jne	.L897
	movq	(%r12), %rax
	testb	$16, 11(%rax)
	jne	.L866
	jmp	.L897
	.p2align 4,,10
	.p2align 3
.L903:
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %r14
	leaq	15(%r13), %rsi
	movq	%r14, 15(%r13)
	testb	$1, %r14b
	je	.L878
	movq	%r14, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L906
.L876:
	testb	$24, %al
	je	.L878
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L907
	.p2align 4,,10
	.p2align 3
.L878:
	movq	%r12, 8(%rbx)
	jmp	.L897
	.p2align 4,,10
	.p2align 3
.L901:
	movq	(%r12), %rax
	testb	$1, %al
	je	.L873
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L896
	movq	%rax, -64(%rbp)
	movl	7(%rax), %eax
	testb	$1, %al
	jne	.L871
	testb	$2, %al
	jne	.L896
.L871:
	leaq	-68(%rbp), %rsi
	leaq	-64(%rbp), %rdi
	call	_ZN2v88internal6String16SlowAsArrayIndexEPj@PLT
	testb	%al, %al
	je	.L896
	movl	-68(%rbp), %esi
	movq	(%rbx), %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory17NewNumberFromUintEjNS0_14AllocationTypeE@PLT
	movq	%rax, %r12
	.p2align 4,,10
	.p2align 3
.L896:
	movq	8(%rbx), %rsi
	jmp	.L873
	.p2align 4,,10
	.p2align 3
.L905:
	movq	%r15, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L855
	.p2align 4,,10
	.p2align 3
.L900:
	movq	(%rbx), %rdi
	movl	$16, %esi
	xorl	%edx, %edx
	call	_ZN2v88internal14OrderedHashSet8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L908
	movq	%rax, 8(%rbx)
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L902:
	movq	(%rbx), %r12
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$307, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory13NewRangeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	jmp	.L852
	.p2align 4,,10
	.p2align 3
.L906:
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-88(%rbp), %rsi
	jmp	.L876
	.p2align 4,,10
	.p2align 3
.L907:
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L878
.L908:
	leaq	.LC6(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L904:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18995:
	.size	_ZN2v88internal14KeyAccumulator6AddKeyENS0_6ObjectENS0_16AddKeyConversionE, .-_ZN2v88internal14KeyAccumulator6AddKeyENS0_6ObjectENS0_16AddKeyConversionE
	.section	.text._ZN2v88internal14KeyAccumulator15AddShadowingKeyENS0_6ObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14KeyAccumulator15AddShadowingKeyENS0_6ObjectE
	.type	_ZN2v88internal14KeyAccumulator15AddShadowingKeyENS0_6ObjectE, @function
_ZN2v88internal14KeyAccumulator15AddShadowingKeyENS0_6ObjectE:
.LFB19009:
	.cfi_startproc
	endbr64
	movl	32(%rdi), %edx
	testl	%edx, %edx
	je	.L917
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %r13
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L911
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r12
.L912:
	movl	32(%rbx), %eax
	testl	%eax, %eax
	je	.L909
	cmpq	$0, 24(%rbx)
	movq	(%rbx), %rdi
	je	.L920
.L915:
	movq	24(%rbx), %rsi
	movq	%r12, %rdx
	call	_ZN2v88internal13ObjectHashSet3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE@PLT
	movq	%rax, 24(%rbx)
.L909:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L911:
	.cfi_restore_state
	movq	41088(%r13), %r12
	cmpq	41096(%r13), %r12
	je	.L921
.L913:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r12)
	jmp	.L912
	.p2align 4,,10
	.p2align 3
.L917:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.p2align 4,,10
	.p2align 3
.L920:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$16, %esi
	call	_ZN2v88internal9HashTableINS0_13ObjectHashSetENS0_18ObjectHashSetShapeEE3NewEPNS0_7IsolateEiNS0_14AllocationTypeENS0_15MinimumCapacityE@PLT
	movq	(%rbx), %rdi
	movq	%rax, 24(%rbx)
	jmp	.L915
	.p2align 4,,10
	.p2align 3
.L921:
	movq	%r13, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L913
	.cfi_endproc
.LFE19009:
	.size	_ZN2v88internal14KeyAccumulator15AddShadowingKeyENS0_6ObjectE, .-_ZN2v88internal14KeyAccumulator15AddShadowingKeyENS0_6ObjectE
	.section	.text._ZN2v88internal12_GLOBAL__N_131CollectOwnPropertyNamesInternalILb0EEENS_4base8OptionalIiEENS0_6HandleINS0_8JSObjectEEEPNS0_14KeyAccumulatorENS6_INS0_15DescriptorArrayEEEii.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_131CollectOwnPropertyNamesInternalILb0EEENS_4base8OptionalIiEENS0_6HandleINS0_8JSObjectEEEPNS0_14KeyAccumulatorENS6_INS0_15DescriptorArrayEEEii.isra.0, @function
_ZN2v88internal12_GLOBAL__N_131CollectOwnPropertyNamesInternalILb0EEENS_4base8OptionalIiEENS0_6HandleINS0_8JSObjectEEEPNS0_14KeyAccumulatorENS6_INS0_15DescriptorArrayEEEii.isra.0:
.LFB23827:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	36(%rdi), %ecx
	movl	32(%rdi), %r10d
	movq	%rdi, -56(%rbp)
	cmpl	%edx, %r14d
	jle	.L945
	leal	1(%rdx), %ebx
	movl	%ecx, %r9d
	movl	%r14d, %eax
	movq	%rsi, %r15
	leal	(%rbx,%rbx,2), %r12d
	movl	%ecx, %r14d
	movl	%edx, %r13d
	andl	$32, %r9d
	sall	$3, %r12d
	movl	$-1, %r8d
	movl	%eax, %ecx
	movslq	%r12d, %r12
	jmp	.L924
	.p2align 4,,10
	.p2align 3
.L958:
	movl	$1, %esi
	cmpl	$1, %r10d
	jne	.L944
	testl	%r9d, %r9d
	je	.L930
.L927:
	andl	$1, %edi
	je	.L944
	movq	15(%r12,%rax), %rdi
	testb	$1, %dil
	jne	.L957
.L944:
	addq	$24, %r12
	cmpl	%ebx, %ecx
	je	.L923
.L960:
	addl	$1, %ebx
.L924:
	movq	(%r15), %rax
	movl	%r13d, %edx
	movl	%ebx, %r13d
	movq	7(%r12,%rax), %rsi
	movq	%rsi, %rdi
	shrq	$35, %rsi
	andl	$7, %esi
	sarq	$32, %rdi
	testl	%r14d, %esi
	jne	.L958
	xorl	%esi, %esi
	testl	%r9d, %r9d
	jne	.L927
.L930:
	movq	-1(%r12,%rax), %r11
	movq	-1(%r11), %rax
	cmpw	$64, 11(%rax)
	je	.L959
	cmpl	$-1, %r8d
	cmove	%edx, %r8d
	addq	$24, %r12
	cmpl	%ebx, %ecx
	jne	.L960
	.p2align 4,,10
	.p2align 3
.L923:
	movq	%r8, %rax
	salq	$32, %rax
	orq	$1, %rax
.L943:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L959:
	.cfi_restore_state
	movq	-56(%rbp), %rax
	movl	36(%rax), %edx
	movq	%r11, %rax
	notq	%rax
	andl	$1, %eax
	cmpl	$64, %edx
	je	.L961
	testb	%al, %al
	je	.L962
.L936:
	andl	$8, %edx
	jne	.L944
.L938:
	testb	%sil, %sil
	je	.L963
	movq	-56(%rbp), %rdi
	movq	%r11, %rsi
	movl	%ecx, -80(%rbp)
	movl	%r8d, -76(%rbp)
	movl	%r9d, -72(%rbp)
	movl	%r10d, -64(%rbp)
	call	_ZN2v88internal14KeyAccumulator15AddShadowingKeyENS0_6ObjectE
	movl	-64(%rbp), %r10d
	movl	-72(%rbp), %r9d
	movl	-76(%rbp), %r8d
	movl	-80(%rbp), %ecx
	jmp	.L944
	.p2align 4,,10
	.p2align 3
.L957:
	movq	-1(%rdi), %r11
	cmpw	$78, 11(%r11)
	jne	.L944
	testb	$1, 19(%rdi)
	jne	.L930
	jmp	.L944
	.p2align 4,,10
	.p2align 3
.L961:
	testb	%al, %al
	jne	.L944
	movq	-1(%r11), %rax
	cmpw	$64, 11(%rax)
	jne	.L944
	testb	$16, 11(%r11)
	jne	.L938
	jmp	.L944
	.p2align 4,,10
	.p2align 3
.L963:
	movq	-56(%rbp), %rax
	movq	(%rax), %rdx
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L940
	movq	%r11, %rsi
	movl	%ecx, -80(%rbp)
	movl	%r8d, -76(%rbp)
	movl	%r9d, -72(%rbp)
	movl	%r10d, -64(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-64(%rbp), %r10d
	movl	-72(%rbp), %r9d
	movl	-76(%rbp), %r8d
	movl	-80(%rbp), %ecx
	movq	%rax, %rsi
.L941:
	movq	-56(%rbp), %rdi
	xorl	%edx, %edx
	movl	%ecx, -80(%rbp)
	movl	%r8d, -76(%rbp)
	movl	%r9d, -72(%rbp)
	movl	%r10d, -64(%rbp)
	call	_ZN2v88internal14KeyAccumulator6AddKeyENS0_6HandleINS0_6ObjectEEENS0_16AddKeyConversionE
	movl	-64(%rbp), %r10d
	movl	-72(%rbp), %r9d
	testb	%al, %al
	movl	-76(%rbp), %r8d
	movl	-80(%rbp), %ecx
	jne	.L944
	xorl	%eax, %eax
	jmp	.L943
	.p2align 4,,10
	.p2align 3
.L940:
	movq	41088(%rdx), %rsi
	cmpq	41096(%rdx), %rsi
	je	.L964
.L942:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rdx)
	movq	%r11, (%rsi)
	jmp	.L941
	.p2align 4,,10
	.p2align 3
.L962:
	movq	-1(%r11), %rax
	cmpw	$64, 11(%rax)
	jne	.L936
	andl	$16, %edx
	jne	.L944
	testb	$1, 11(%r11)
	je	.L938
	jmp	.L944
.L945:
	movl	$-1, %r8d
	jmp	.L923
.L964:
	movq	%rdx, %rdi
	movl	%ecx, -88(%rbp)
	movl	%r8d, -84(%rbp)
	movl	%r9d, -80(%rbp)
	movl	%r10d, -76(%rbp)
	movq	%r11, -72(%rbp)
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movl	-88(%rbp), %ecx
	movl	-84(%rbp), %r8d
	movl	-80(%rbp), %r9d
	movl	-76(%rbp), %r10d
	movq	%rax, %rsi
	movq	-72(%rbp), %r11
	movq	-64(%rbp), %rdx
	jmp	.L942
	.cfi_endproc
.LFE23827:
	.size	_ZN2v88internal12_GLOBAL__N_131CollectOwnPropertyNamesInternalILb0EEENS_4base8OptionalIiEENS0_6HandleINS0_8JSObjectEEEPNS0_14KeyAccumulatorENS6_INS0_15DescriptorArrayEEEii.isra.0, .-_ZN2v88internal12_GLOBAL__N_131CollectOwnPropertyNamesInternalILb0EEENS_4base8OptionalIiEENS0_6HandleINS0_8JSObjectEEEPNS0_14KeyAccumulatorENS6_INS0_15DescriptorArrayEEEii.isra.0
	.section	.text._ZN2v88internal14KeyAccumulator15AddShadowingKeyENS0_6HandleINS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14KeyAccumulator15AddShadowingKeyENS0_6HandleINS0_6ObjectEEE
	.type	_ZN2v88internal14KeyAccumulator15AddShadowingKeyENS0_6HandleINS0_6ObjectEEE, @function
_ZN2v88internal14KeyAccumulator15AddShadowingKeyENS0_6HandleINS0_6ObjectEEE:
.LFB19010:
	.cfi_startproc
	endbr64
	movl	32(%rdi), %eax
	testl	%eax, %eax
	je	.L969
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	cmpq	$0, 24(%rbx)
	je	.L972
.L967:
	movq	24(%rbx), %rsi
	movq	%r12, %rdx
	call	_ZN2v88internal13ObjectHashSet3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE@PLT
	movq	%rax, 24(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L969:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.p2align 4,,10
	.p2align 3
.L972:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$16, %esi
	call	_ZN2v88internal9HashTableINS0_13ObjectHashSetENS0_18ObjectHashSetShapeEE3NewEPNS0_7IsolateEiNS0_14AllocationTypeENS0_15MinimumCapacityE@PLT
	movq	(%rbx), %rdi
	movq	%rax, 24(%rbx)
	jmp	.L967
	.cfi_endproc
.LFE19010:
	.size	_ZN2v88internal14KeyAccumulator15AddShadowingKeyENS0_6HandleINS0_6ObjectEEE, .-_ZN2v88internal14KeyAccumulator15AddShadowingKeyENS0_6HandleINS0_6ObjectEEE
	.section	.text._ZN2v88internal18FastKeyAccumulator7PrepareEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18FastKeyAccumulator7PrepareEv
	.type	_ZN2v88internal18FastKeyAccumulator7PrepareEv, @function
_ZN2v88internal18FastKeyAccumulator7PrepareEv:
.LFB19013:
	.cfi_startproc
	endbr64
	movl	24(%rdi), %eax
	testl	%eax, %eax
	jne	.L975
	ret
	.p2align 4,,10
	.p2align 3
.L975:
	jmp	_ZN2v88internal18FastKeyAccumulator7PrepareEv.part.0
	.cfi_endproc
.LFE19013:
	.size	_ZN2v88internal18FastKeyAccumulator7PrepareEv, .-_ZN2v88internal18FastKeyAccumulator7PrepareEv
	.section	.text._ZN2v88internal18FastKeyAccumulator36GetOwnKeysWithUninitializedEnumCacheEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18FastKeyAccumulator36GetOwnKeysWithUninitializedEnumCacheEv
	.type	_ZN2v88internal18FastKeyAccumulator36GetOwnKeysWithUninitializedEnumCacheEv, @function
_ZN2v88internal18FastKeyAccumulator36GetOwnKeysWithUninitializedEnumCacheEv:
.LFB19019:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rsi
	movq	(%rsi), %rax
	movq	-1(%rax), %rdx
	movq	(%rdi), %rdi
	movq	15(%rax), %rax
	cmpq	%rax, 288(%rdi)
	je	.L977
	cmpq	%rax, 1016(%rdi)
	je	.L977
	xorl	%eax, %eax
.L978:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L977:
	.cfi_restore_state
	movl	15(%rdx), %eax
	testl	$1047552, %eax
	jne	.L979
	movl	15(%rdx), %eax
	andl	$-1024, %eax
	movl	%eax, 15(%rdx)
	movq	(%rbx), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	addq	$288, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L979:
	.cfi_restore_state
	call	_ZN2v88internal12_GLOBAL__N_123GetFastEnumPropertyKeysEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEE
	cmpb	$0, 32(%rbx)
	jne	.L978
	movq	(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal7Factory14CopyFixedArrayENS0_6HandleINS0_10FixedArrayEEE@PLT
	jmp	.L978
	.cfi_endproc
.LFE19019:
	.size	_ZN2v88internal18FastKeyAccumulator36GetOwnKeysWithUninitializedEnumCacheEv, .-_ZN2v88internal18FastKeyAccumulator36GetOwnKeysWithUninitializedEnumCacheEv
	.section	.text._ZN2v88internal14KeyAccumulator24CollectOwnElementIndicesENS0_6HandleINS0_10JSReceiverEEENS2_INS0_8JSObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14KeyAccumulator24CollectOwnElementIndicesENS0_6HandleINS0_10JSReceiverEEENS2_INS0_8JSObjectEEE
	.type	_ZN2v88internal14KeyAccumulator24CollectOwnElementIndicesENS0_6HandleINS0_10JSReceiverEEENS2_INS0_8JSObjectEEE, @function
_ZN2v88internal14KeyAccumulator24CollectOwnElementIndicesENS0_6HandleINS0_10JSReceiverEEENS2_INS0_8JSObjectEEE:
.LFB19024:
	.cfi_startproc
	endbr64
	testb	$8, 36(%rdi)
	je	.L994
	movl	$257, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L994:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$257, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	cmpb	$0, 41(%rdi)
	je	.L995
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L995:
	.cfi_restore_state
	movq	%rdx, %r13
	movq	(%rdx), %rdx
	movq	%rsi, %r14
	movq	-1(%rdx), %rax
	movq	_ZN2v88internal16ElementsAccessor19elements_accessors_E(%rip), %rcx
	movq	(%rdi), %rbx
	movq	15(%rdx), %rsi
	movzbl	14(%rax), %eax
	movq	41112(%rbx), %rdi
	shrl	$3, %eax
	movq	(%rcx,%rax,8), %r15
	movq	(%r15), %rax
	movq	72(%rax), %r8
	testq	%rdi, %rdi
	je	.L986
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-56(%rbp), %r8
	movq	%rax, %rdx
.L987:
	movq	%r12, %rcx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	*%r8
	testb	%al, %al
	jne	.L989
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	movb	$0, %ah
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L986:
	.cfi_restore_state
	movq	41088(%rbx), %rdx
	cmpq	41096(%rbx), %rdx
	je	.L996
.L988:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rdx)
	jmp	.L987
	.p2align 4,,10
	.p2align 3
.L989:
	addq	$24, %rsp
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	xorl	%ecx, %ecx
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12_GLOBAL__N_122CollectInterceptorKeysENS0_6HandleINS0_10JSReceiverEEENS2_INS0_8JSObjectEEEPNS0_14KeyAccumulatorENS1_14IndexedOrNamedE
	.p2align 4,,10
	.p2align 3
.L996:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rsi, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %r8
	movq	%rax, %rdx
	jmp	.L988
	.cfi_endproc
.LFE19024:
	.size	_ZN2v88internal14KeyAccumulator24CollectOwnElementIndicesENS0_6HandleINS0_10JSReceiverEEENS2_INS0_8JSObjectEEE, .-_ZN2v88internal14KeyAccumulator24CollectOwnElementIndicesENS0_6HandleINS0_10JSReceiverEEENS2_INS0_8JSObjectEEE
	.section	.text._ZN2v88internal14KeyAccumulator19CollectPrivateNamesENS0_6HandleINS0_10JSReceiverEEENS2_INS0_8JSObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14KeyAccumulator19CollectPrivateNamesENS0_6HandleINS0_10JSReceiverEEENS2_INS0_8JSObjectEEE
	.type	_ZN2v88internal14KeyAccumulator19CollectPrivateNamesENS0_6HandleINS0_10JSReceiverEEENS2_INS0_8JSObjectEEE, @function
_ZN2v88internal14KeyAccumulator19CollectPrivateNamesENS0_6HandleINS0_10JSReceiverEEENS2_INS0_8JSObjectEEE:
.LFB19029:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	(%rdx), %rax
	movq	-1(%rax), %rdx
	movl	15(%rdx), %edx
	andl	$2097152, %edx
	movq	-1(%rax), %rdx
	jne	.L998
	movl	15(%rdx), %r12d
	movq	(%rdi), %r13
	movq	-1(%rax), %rax
	movq	41112(%r13), %rdi
	shrl	$10, %r12d
	movq	39(%rax), %r14
	andl	$1023, %r12d
	testq	%rdi, %rdi
	je	.L999
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1000:
	movl	%r12d, %ecx
	movq	%rbx, %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal12_GLOBAL__N_131CollectOwnPropertyNamesInternalILb0EEENS_4base8OptionalIiEENS0_6HandleINS0_8JSObjectEEEPNS0_14KeyAccumulatorENS6_INS0_15DescriptorArrayEEEii.isra.0
	addq	$16, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L998:
	.cfi_restore_state
	cmpw	$1025, 11(%rdx)
	movq	(%rdi), %r12
	movq	7(%rax), %rsi
	je	.L1017
	testb	$1, %sil
	jne	.L1008
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-36536(%rax), %rsi
.L1008:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1009
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdi
.L1010:
	addq	$16, %rsp
	movq	%rbx, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal18BaseNameDictionaryINS0_14NameDictionaryENS0_19NameDictionaryShapeEE13CollectKeysToENS0_6HandleIS2_EEPNS0_14KeyAccumulatorE@PLT
	.p2align 4,,10
	.p2align 3
.L999:
	.cfi_restore_state
	movq	41088(%r13), %rsi
	cmpq	41096(%r13), %rsi
	je	.L1018
.L1001:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r13)
	movq	%r14, (%rsi)
	jmp	.L1000
	.p2align 4,,10
	.p2align 3
.L1017:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1004
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdi
.L1005:
	addq	$16, %rsp
	movq	%rbx, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal18BaseNameDictionaryINS0_16GlobalDictionaryENS0_21GlobalDictionaryShapeEE13CollectKeysToENS0_6HandleIS2_EEPNS0_14KeyAccumulatorE@PLT
	.p2align 4,,10
	.p2align 3
.L1009:
	.cfi_restore_state
	movq	41088(%r12), %rdi
	cmpq	41096(%r12), %rdi
	je	.L1019
.L1011:
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdi)
	jmp	.L1010
	.p2align 4,,10
	.p2align 3
.L1004:
	movq	41088(%r12), %rdi
	cmpq	41096(%r12), %rdi
	je	.L1020
.L1006:
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdi)
	jmp	.L1005
	.p2align 4,,10
	.p2align 3
.L1018:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L1001
	.p2align 4,,10
	.p2align 3
.L1019:
	movq	%r12, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	movq	%rax, %rdi
	jmp	.L1011
	.p2align 4,,10
	.p2align 3
.L1020:
	movq	%r12, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	movq	%rax, %rdi
	jmp	.L1006
	.cfi_endproc
.LFE19029:
	.size	_ZN2v88internal14KeyAccumulator19CollectPrivateNamesENS0_6HandleINS0_10JSReceiverEEENS2_INS0_8JSObjectEEE, .-_ZN2v88internal14KeyAccumulator19CollectPrivateNamesENS0_6HandleINS0_10JSReceiverEEENS2_INS0_8JSObjectEEE
	.section	.text._ZN2v88internal14KeyAccumulator33CollectAccessCheckInterceptorKeysENS0_6HandleINS0_15AccessCheckInfoEEENS2_INS0_10JSReceiverEEENS2_INS0_8JSObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14KeyAccumulator33CollectAccessCheckInterceptorKeysENS0_6HandleINS0_15AccessCheckInfoEEENS2_INS0_10JSReceiverEEENS2_INS0_8JSObjectEEE
	.type	_ZN2v88internal14KeyAccumulator33CollectAccessCheckInterceptorKeysENS0_6HandleINS0_15AccessCheckInfoEEENS2_INS0_10JSReceiverEEENS2_INS0_8JSObjectEEE, @function
_ZN2v88internal14KeyAccumulator33CollectAccessCheckInterceptorKeysENS0_6HandleINS0_15AccessCheckInfoEEENS2_INS0_10JSReceiverEEENS2_INS0_8JSObjectEEE:
.LFB19030:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rbx
	cmpb	$0, 41(%r12)
	movq	(%rsi), %rax
	movq	41112(%rbx), %rdi
	jne	.L1022
	movq	%rsi, %r15
	movq	23(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1023
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L1024:
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal12_GLOBAL__N_130CollectInterceptorKeysInternalENS0_6HandleINS0_10JSReceiverEEENS2_INS0_8JSObjectEEENS2_INS0_15InterceptorInfoEEEPNS0_14KeyAccumulatorENS1_14IndexedOrNamedE
	testb	%al, %al
	je	.L1026
	movq	(%r12), %rbx
	movq	(%r15), %rax
	movq	41112(%rbx), %rdi
.L1022:
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1028
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L1029:
	movl	$1, %r8d
	movq	%r12, %rcx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal12_GLOBAL__N_130CollectInterceptorKeysInternalENS0_6HandleINS0_10JSReceiverEEENS2_INS0_8JSObjectEEENS2_INS0_15InterceptorInfoEEEPNS0_14KeyAccumulatorENS1_14IndexedOrNamedE
	movl	%eax, %r8d
	movl	$257, %eax
	testb	%r8b, %r8b
	jne	.L1027
	xorb	%al, %al
	movb	$0, %ah
.L1027:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1028:
	.cfi_restore_state
	movq	41088(%rbx), %rdx
	cmpq	41096(%rbx), %rdx
	je	.L1033
.L1030:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rdx)
	jmp	.L1029
	.p2align 4,,10
	.p2align 3
.L1023:
	movq	41088(%rbx), %rdx
	cmpq	41096(%rbx), %rdx
	je	.L1034
.L1025:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rdx)
	jmp	.L1024
	.p2align 4,,10
	.p2align 3
.L1026:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	movb	$0, %ah
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1033:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L1030
	.p2align 4,,10
	.p2align 3
.L1034:
	movq	%rbx, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L1025
	.cfi_endproc
.LFE19030:
	.size	_ZN2v88internal14KeyAccumulator33CollectAccessCheckInterceptorKeysENS0_6HandleINS0_15AccessCheckInfoEEENS2_INS0_10JSReceiverEEENS2_INS0_8JSObjectEEE, .-_ZN2v88internal14KeyAccumulator33CollectAccessCheckInterceptorKeysENS0_6HandleINS0_15AccessCheckInfoEEENS2_INS0_10JSReceiverEEENS2_INS0_8JSObjectEEE
	.section	.text._ZN2v88internal14KeyAccumulator22GetOwnEnumPropertyKeysEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14KeyAccumulator22GetOwnEnumPropertyKeysEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEE
	.type	_ZN2v88internal14KeyAccumulator22GetOwnEnumPropertyKeysEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEE, @function
_ZN2v88internal14KeyAccumulator22GetOwnEnumPropertyKeysEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEE:
.LFB19032:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	movq	-1(%rax), %rdx
	movl	15(%rdx), %edx
	andl	$2097152, %edx
	jne	.L1036
	jmp	_ZN2v88internal12_GLOBAL__N_123GetFastEnumPropertyKeysEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEE
	.p2align 4,,10
	.p2align 3
.L1036:
	movq	-1(%rax), %rdx
	movq	7(%rax), %rcx
	cmpw	$1025, 11(%rdx)
	je	.L1043
	testb	$1, %cl
	jne	.L1039
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-36536(%rax), %rcx
.L1039:
	xorl	%edx, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal12_GLOBAL__N_132GetOwnEnumPropertyDictionaryKeysINS0_14NameDictionaryEEENS0_6HandleINS0_10FixedArrayEEEPNS0_7IsolateENS0_17KeyCollectionModeEPNS0_14KeyAccumulatorENS4_INS0_8JSObjectEEET_.isra.0
	.p2align 4,,10
	.p2align 3
.L1043:
	xorl	%edx, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal12_GLOBAL__N_132GetOwnEnumPropertyDictionaryKeysINS0_16GlobalDictionaryEEENS0_6HandleINS0_10FixedArrayEEEPNS0_7IsolateENS0_17KeyCollectionModeEPNS0_14KeyAccumulatorENS4_INS0_8JSObjectEEET_.isra.0
	.cfi_endproc
.LFE19032:
	.size	_ZN2v88internal14KeyAccumulator22GetOwnEnumPropertyKeysEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEE, .-_ZN2v88internal14KeyAccumulator22GetOwnEnumPropertyKeysEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEE
	.section	.rodata._ZN2v88internal18FastKeyAccumulator11GetKeysFastENS0_17GetKeysConversionE.str1.8,"aMS",@progbits,1
	.align 8
.LC17:
	.string	"| strings=%d symbols=0 elements=%u || prototypes>=1 ||\n"
	.align 8
.LC18:
	.string	"| strings=%d symbols=0 elements=0 || prototypes>=1 ||\n"
	.section	.text._ZN2v88internal18FastKeyAccumulator11GetKeysFastENS0_17GetKeysConversionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18FastKeyAccumulator11GetKeysFastENS0_17GetKeysConversionE
	.type	_ZN2v88internal18FastKeyAccumulator11GetKeysFastENS0_17GetKeysConversionE, @function
_ZN2v88internal18FastKeyAccumulator11GetKeysFastENS0_17GetKeysConversionE:
.LFB19018:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %rax
	cmpb	$0, 35(%rdi)
	movq	(%rax), %rax
	jne	.L1045
	movl	24(%rdi), %edx
	testl	%edx, %edx
	jne	.L1084
.L1045:
	movq	-1(%rax), %rax
	cmpw	$1041, 11(%rax)
	ja	.L1085
.L1064:
	xorl	%eax, %eax
.L1072:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1084:
	.cfi_restore_state
	movq	-1(%rax), %rax
	jmp	.L1064
	.p2align 4,,10
	.p2align 3
.L1085:
	movq	8(%rbx), %r14
	movl	15(%rax), %eax
	testl	$2097152, %eax
	je	.L1048
	movzbl	33(%rbx), %edx
	movq	(%r14), %rax
	movq	%r14, %rsi
	movq	(%rbx), %rdi
	movb	%dl, -56(%rbp)
	movq	-1(%rax), %rax
	movq	_ZN2v88internal16ElementsAccessor19elements_accessors_E(%rip), %rcx
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	movq	(%rcx,%rax,8), %r15
	call	_ZN2v88internal14KeyAccumulator22GetOwnEnumPropertyKeysEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEE
.L1083:
	movzbl	-56(%rbp), %edx
	movq	%rax, %r12
	movq	%rax, %rbx
	testb	%dl, %dl
	je	.L1086
.L1049:
	cmpb	$0, _ZN2v88internal27FLAG_trace_for_in_enumerateE(%rip)
	je	.L1062
	testq	%rbx, %rbx
	je	.L1087
	movq	(%r12), %rax
	leaq	.LC17(%rip), %rdi
	movslq	11(%rax), %rsi
	movq	(%rbx), %rax
	movslq	11(%rax), %rdx
	xorl	%eax, %eax
	subl	%esi, %edx
	call	_ZN2v88internal6PrintFEPKcz@PLT
.L1062:
	addq	$24, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1048:
	.cfi_restore_state
	movq	(%r14), %rax
	movq	-1(%rax), %rax
	movl	15(%rax), %eax
	andl	$1023, %eax
	cmpl	$1023, %eax
	je	.L1088
.L1055:
	movzbl	33(%rbx), %edx
	movq	(%r14), %rax
	movq	%r14, %rsi
	movq	(%rbx), %rdi
	movb	%dl, -56(%rbp)
	movq	-1(%rax), %rax
	movq	_ZN2v88internal16ElementsAccessor19elements_accessors_E(%rip), %rcx
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	movq	(%rcx,%rax,8), %r15
	call	_ZN2v88internal12_GLOBAL__N_123GetFastEnumPropertyKeysEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEE
	jmp	.L1083
	.p2align 4,,10
	.p2align 3
.L1086:
	movq	(%r15), %rax
	movq	88(%rax), %r10
	movq	(%r14), %rax
	movq	%rax, %rdx
	movq	15(%rax), %rsi
	andq	$-262144, %rdx
	movq	24(%rdx), %rbx
	movq	3520(%rbx), %rdi
	subq	$37592, %rbx
	testq	%rdi, %rdi
	je	.L1050
	movq	%r10, -56(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-56(%rbp), %r10
	movq	%rax, %rdx
.L1051:
	movl	$2, %r9d
	movl	%r13d, %r8d
	movq	%r12, %rcx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	*%r10
	movq	%rax, %rbx
	jmp	.L1049
	.p2align 4,,10
	.p2align 3
.L1088:
	movq	%rbx, %rdi
	call	_ZN2v88internal18FastKeyAccumulator36GetOwnKeysWithUninitializedEnumCacheEv
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1055
	cmpb	$0, _ZN2v88internal27FLAG_trace_for_in_enumerateE(%rip)
	jne	.L1089
.L1057:
	movq	(%r14), %rax
	movq	-1(%rax), %rax
	movl	15(%rax), %eax
	andl	$1023, %eax
	cmpl	$1023, %eax
	movq	%r12, %rax
	setne	34(%rbx)
	jmp	.L1072
	.p2align 4,,10
	.p2align 3
.L1050:
	movq	41088(%rbx), %rdx
	cmpq	41096(%rbx), %rdx
	je	.L1090
.L1052:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rdx)
	jmp	.L1051
.L1089:
	movq	(%rax), %rax
	leaq	.LC18(%rip), %rdi
	movslq	11(%rax), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1057
.L1087:
	leaq	.LC6(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1090:
	movq	%rbx, %rdi
	movq	%rsi, -64(%rbp)
	movq	%r10, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %r10
	movq	%rax, %rdx
	jmp	.L1052
	.cfi_endproc
.LFE19018:
	.size	_ZN2v88internal18FastKeyAccumulator11GetKeysFastENS0_17GetKeysConversionE, .-_ZN2v88internal18FastKeyAccumulator11GetKeysFastENS0_17GetKeysConversionE
	.section	.text._ZN2v88internal14KeyAccumulator23CollectOwnPropertyNamesENS0_6HandleINS0_10JSReceiverEEENS2_INS0_8JSObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14KeyAccumulator23CollectOwnPropertyNamesENS0_6HandleINS0_10JSReceiverEEENS2_INS0_8JSObjectEEE
	.type	_ZN2v88internal14KeyAccumulator23CollectOwnPropertyNamesENS0_6HandleINS0_10JSReceiverEEENS2_INS0_8JSObjectEEE, @function
_ZN2v88internal14KeyAccumulator23CollectOwnPropertyNamesENS0_6HandleINS0_10JSReceiverEEENS2_INS0_8JSObjectEEE:
.LFB19027:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	cmpl	$18, 36(%rdi)
	movq	-1(%rax), %rdx
	movl	15(%rdx), %edx
	je	.L1178
	andl	$2097152, %edx
	movq	-1(%rax), %rdx
	je	.L1179
	cmpw	$1025, 11(%rdx)
	movq	(%rdi), %rbx
	movq	7(%rax), %rsi
	je	.L1180
	testb	$1, %sil
	jne	.L1146
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-36536(%rax), %rsi
.L1146:
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1147
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdi
.L1148:
	movq	%r12, %rsi
	call	_ZN2v88internal18BaseNameDictionaryINS0_14NameDictionaryENS0_19NameDictionaryShapeEE13CollectKeysToENS0_6HandleIS2_EEPNS0_14KeyAccumulatorE@PLT
	testb	%al, %al
	je	.L1177
.L1140:
	movq	-88(%rbp), %rdi
	movl	$1, %ecx
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	_ZN2v88internal12_GLOBAL__N_122CollectInterceptorKeysENS0_6HandleINS0_10JSReceiverEEENS2_INS0_8JSObjectEEEPNS0_14KeyAccumulatorENS1_14IndexedOrNamedE
.L1109:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1181
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1179:
	.cfi_restore_state
	movl	15(%rdx), %edx
	movq	(%rdi), %rbx
	shrl	$10, %edx
	andl	$1023, %edx
	movl	%edx, -96(%rbp)
	movq	-1(%rax), %rax
	movq	41112(%rbx), %rdi
	movq	39(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1113
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L1114:
	movl	32(%r12), %eax
	movl	36(%r12), %ecx
	movl	%eax, -72(%rbp)
	movl	-96(%rbp), %eax
	testl	%eax, %eax
	je	.L1140
	movl	%ecx, %r9d
	subl	$1, %eax
	movl	$-1, %r8d
	movq	%r13, -120(%rbp)
	movq	%rax, -80(%rbp)
	andl	$32, %r9d
	xorl	%ebx, %ebx
	movl	%r8d, %r13d
	jmp	.L1117
	.p2align 4,,10
	.p2align 3
.L1184:
	cmpl	$1, -72(%rbp)
	movl	$1, %esi
	jne	.L1139
	testl	%r9d, %r9d
	je	.L1125
.L1122:
	andl	$1, %edi
	je	.L1139
	movq	15(%rax,%rdx), %rdi
	testb	$1, %dil
	jne	.L1182
	.p2align 4,,10
	.p2align 3
.L1139:
	cmpq	%rbx, -80(%rbp)
	je	.L1183
.L1151:
	movq	%r14, %rbx
.L1117:
	leaq	1(%rbx), %r14
	movq	(%r15), %rdx
	movl	%ebx, %r10d
	leaq	(%r14,%r14,2), %rax
	salq	$3, %rax
	movq	7(%rax,%rdx), %rsi
	movq	%rsi, %rdi
	shrq	$35, %rsi
	andl	$7, %esi
	sarq	$32, %rdi
	testl	%ecx, %esi
	jne	.L1184
	xorl	%esi, %esi
	testl	%r9d, %r9d
	jne	.L1122
.L1125:
	movq	-1(%rax,%rdx), %r11
	movq	-1(%r11), %rax
	cmpw	$64, 11(%rax)
	je	.L1185
	movq	%r11, %rax
	movl	36(%r12), %edx
	notq	%rax
	andl	$1, %eax
	cmpl	$64, %edx
	je	.L1186
	testb	%al, %al
	je	.L1187
.L1131:
	andl	$8, %edx
	jne	.L1139
.L1133:
	testb	%sil, %sil
	je	.L1188
	movq	%r11, %rsi
	movq	%r12, %rdi
	movl	%r9d, -112(%rbp)
	movl	%ecx, -104(%rbp)
	call	_ZN2v88internal14KeyAccumulator15AddShadowingKeyENS0_6ObjectE
	movl	-104(%rbp), %ecx
	movl	-112(%rbp), %r9d
	cmpq	%rbx, -80(%rbp)
	jne	.L1151
	.p2align 4,,10
	.p2align 3
.L1183:
	movl	%r13d, %r8d
	movq	-120(%rbp), %r13
	cmpl	$-1, %r8d
	je	.L1140
	movl	-96(%rbp), %ecx
	movl	%r8d, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_131CollectOwnPropertyNamesInternalILb0EEENS_4base8OptionalIiEENS0_6HandleINS0_8JSObjectEEEPNS0_14KeyAccumulatorENS6_INS0_15DescriptorArrayEEEii.isra.0
	testb	%al, %al
	jne	.L1140
.L1177:
	xorl	%eax, %eax
.L1176:
	movb	$0, %ah
	jmp	.L1109
	.p2align 4,,10
	.p2align 3
.L1178:
	andl	$2097152, %edx
	je	.L1189
	movq	-1(%rax), %rdx
	movq	7(%rax), %rcx
	cmpw	$1025, 11(%rdx)
	je	.L1190
	testb	$1, %cl
	jne	.L1103
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-36536(%rax), %rcx
.L1103:
	movl	32(%r12), %esi
	movq	(%r12), %rdi
	movq	%r12, %rdx
	call	_ZN2v88internal12_GLOBAL__N_132GetOwnEnumPropertyDictionaryKeysINS0_14NameDictionaryEEENS0_6HandleINS0_10FixedArrayEEEPNS0_7IsolateENS0_17KeyCollectionModeEPNS0_14KeyAccumulatorENS4_INS0_8JSObjectEEET_.isra.0
	movq	%rax, %r14
.L1175:
	movq	0(%r13), %rax
.L1094:
	movq	-1(%rax), %rax
	cmpw	$1027, 11(%rax)
	jne	.L1104
	movq	(%r14), %rax
	movslq	11(%rax), %rdx
	testq	%rdx, %rdx
	jle	.L1104
	subl	$1, %edx
	movl	$16, %r15d
	leaq	-64(%rbp), %rbx
	leaq	24(,%rdx,8), %rcx
	movq	%rcx, -72(%rbp)
	jmp	.L1110
	.p2align 4,,10
	.p2align 3
.L1191:
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L1106:
	movq	0(%r13), %rax
	movq	(%r12), %rsi
	movq	%rbx, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal17JSModuleNamespace9GetExportEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	testq	%rax, %rax
	je	.L1176
	addq	$8, %r15
	cmpq	-72(%rbp), %r15
	je	.L1104
	movq	(%r14), %rax
.L1110:
	movq	(%r12), %rsi
	movq	-1(%rax,%r15), %r8
	movq	41112(%rsi), %rdi
	testq	%rdi, %rdi
	jne	.L1191
	movq	41088(%rsi), %rdx
	cmpq	41096(%rsi), %rdx
	je	.L1192
.L1107:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%rsi)
	movq	%r8, (%rdx)
	jmp	.L1106
	.p2align 4,,10
	.p2align 3
.L1189:
	movq	(%rdi), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal14KeyAccumulator22GetOwnEnumPropertyKeysEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEE
	movq	%rax, %r14
	movq	0(%r13), %rax
	movq	-1(%rax), %rdx
	movl	15(%rdx), %ebx
	movq	(%r14), %rcx
	shrl	$10, %ebx
	andl	$1023, %ebx
	cmpl	%ebx, 11(%rcx)
	je	.L1094
	movq	(%r12), %r15
	movq	39(%rdx), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1095
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L1096:
	testl	%ebx, %ebx
	jle	.L1175
	leal	-1(%rbx), %edx
	movq	%r13, -72(%rbp)
	movl	$31, %r15d
	movq	%r12, %r13
	leaq	(%rdx,%rdx,2), %rdx
	movq	%rax, %r12
	leaq	55(,%rdx,8), %rbx
	.p2align 4,,10
	.p2align 3
.L1098:
	movq	(%r12), %rdx
	movq	(%rdx,%r15), %rcx
	btq	$36, %rcx
	jnc	.L1100
	movq	-8(%r15,%rdx), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal14KeyAccumulator15AddShadowingKeyENS0_6ObjectE
.L1100:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L1098
	movq	%r13, %r12
	movq	-72(%rbp), %r13
	movq	0(%r13), %rax
	jmp	.L1094
	.p2align 4,,10
	.p2align 3
.L1113:
	movq	41088(%rbx), %r15
	cmpq	41096(%rbx), %r15
	je	.L1193
.L1115:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r15)
	jmp	.L1114
	.p2align 4,,10
	.p2align 3
.L1147:
	movq	41088(%rbx), %rdi
	cmpq	41096(%rbx), %rdi
	je	.L1194
.L1149:
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rdi)
	jmp	.L1148
	.p2align 4,,10
	.p2align 3
.L1185:
	cmpl	$-1, %r13d
	cmove	%r10d, %r13d
	jmp	.L1139
	.p2align 4,,10
	.p2align 3
.L1182:
	movq	-1(%rdi), %r11
	cmpw	$78, 11(%r11)
	jne	.L1139
	testb	$1, 19(%rdi)
	jne	.L1125
	jmp	.L1139
	.p2align 4,,10
	.p2align 3
.L1186:
	testb	%al, %al
	jne	.L1139
	movq	-1(%r11), %rax
	cmpw	$64, 11(%rax)
	jne	.L1139
	testb	$16, 11(%r11)
	jne	.L1133
	jmp	.L1139
	.p2align 4,,10
	.p2align 3
.L1188:
	movq	(%r12), %rdx
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L1135
	movq	%r11, %rsi
	movl	%r9d, -112(%rbp)
	movl	%ecx, -104(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-104(%rbp), %ecx
	movl	-112(%rbp), %r9d
	movq	%rax, %rsi
.L1136:
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	%r9d, -112(%rbp)
	movl	%ecx, -104(%rbp)
	call	_ZN2v88internal14KeyAccumulator6AddKeyENS0_6HandleINS0_6ObjectEEENS0_16AddKeyConversionE
	movl	-104(%rbp), %ecx
	movl	-112(%rbp), %r9d
	testb	%al, %al
	jne	.L1139
	xorl	%eax, %eax
	jmp	.L1176
	.p2align 4,,10
	.p2align 3
.L1104:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal14KeyAccumulator7AddKeysENS0_6HandleINS0_10FixedArrayEEENS0_16AddKeyConversionE
	testb	%al, %al
	jne	.L1140
	xorl	%eax, %eax
	jmp	.L1176
	.p2align 4,,10
	.p2align 3
.L1187:
	movq	-1(%r11), %rax
	cmpw	$64, 11(%rax)
	jne	.L1131
	andl	$16, %edx
	jne	.L1139
	testb	$1, 11(%r11)
	je	.L1133
	jmp	.L1139
	.p2align 4,,10
	.p2align 3
.L1135:
	movq	41088(%rdx), %rsi
	cmpq	41096(%rdx), %rsi
	je	.L1195
.L1137:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rdx)
	movq	%r11, (%rsi)
	jmp	.L1136
	.p2align 4,,10
	.p2align 3
.L1192:
	movq	%rsi, %rdi
	movq	%r8, -96(%rbp)
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %r8
	movq	-80(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L1107
	.p2align 4,,10
	.p2align 3
.L1180:
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1142
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdi
.L1143:
	movq	%r12, %rsi
	call	_ZN2v88internal18BaseNameDictionaryINS0_16GlobalDictionaryENS0_21GlobalDictionaryShapeEE13CollectKeysToENS0_6HandleIS2_EEPNS0_14KeyAccumulatorE@PLT
	testb	%al, %al
	jne	.L1140
	xorl	%eax, %eax
	jmp	.L1176
	.p2align 4,,10
	.p2align 3
.L1190:
	movl	32(%rdi), %esi
	movq	%rdi, %rdx
	movq	(%rdi), %rdi
	call	_ZN2v88internal12_GLOBAL__N_132GetOwnEnumPropertyDictionaryKeysINS0_16GlobalDictionaryEEENS0_6HandleINS0_10FixedArrayEEEPNS0_7IsolateENS0_17KeyCollectionModeEPNS0_14KeyAccumulatorENS4_INS0_8JSObjectEEET_.isra.0
	movq	%rax, %r14
	movq	0(%r13), %rax
	jmp	.L1094
	.p2align 4,,10
	.p2align 3
.L1095:
	movq	41088(%r15), %rax
	cmpq	%rax, 41096(%r15)
	je	.L1196
.L1097:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%rsi, (%rax)
	jmp	.L1096
	.p2align 4,,10
	.p2align 3
.L1142:
	movq	41088(%rbx), %rdi
	cmpq	41096(%rbx), %rdi
	je	.L1197
.L1144:
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rdi)
	jmp	.L1143
.L1197:
	movq	%rbx, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %rdi
	jmp	.L1144
.L1196:
	movq	%r15, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	jmp	.L1097
	.p2align 4,,10
	.p2align 3
.L1193:
	movq	%rbx, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L1115
	.p2align 4,,10
	.p2align 3
.L1195:
	movq	%rdx, %rdi
	movl	%r9d, -128(%rbp)
	movl	%ecx, -124(%rbp)
	movq	%r11, -112(%rbp)
	movq	%rdx, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movl	-128(%rbp), %r9d
	movl	-124(%rbp), %ecx
	movq	-112(%rbp), %r11
	movq	-104(%rbp), %rdx
	movq	%rax, %rsi
	jmp	.L1137
	.p2align 4,,10
	.p2align 3
.L1194:
	movq	%rbx, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %rdi
	jmp	.L1149
.L1181:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19027:
	.size	_ZN2v88internal14KeyAccumulator23CollectOwnPropertyNamesENS0_6HandleINS0_10JSReceiverEEENS2_INS0_8JSObjectEEE, .-_ZN2v88internal14KeyAccumulator23CollectOwnPropertyNamesENS0_6HandleINS0_10JSReceiverEEENS2_INS0_8JSObjectEEE
	.section	.text._ZN2v88internal14KeyAccumulator14CollectOwnKeysENS0_6HandleINS0_10JSReceiverEEENS2_INS0_8JSObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14KeyAccumulator14CollectOwnKeysENS0_6HandleINS0_10JSReceiverEEENS2_INS0_8JSObjectEEE
	.type	_ZN2v88internal14KeyAccumulator14CollectOwnKeysENS0_6HandleINS0_10JSReceiverEEENS2_INS0_8JSObjectEEE, @function
_ZN2v88internal14KeyAccumulator14CollectOwnKeysENS0_6HandleINS0_10JSReceiverEEENS2_INS0_8JSObjectEEE:
.LFB19031:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	-1(%rbx), %rax
	cmpw	$1026, 11(%rax)
	je	.L1229
	movq	-1(%rbx), %rax
	movzbl	13(%rax), %eax
	shrb	$5, %al
	andl	$1, %eax
.L1202:
	testb	%al, %al
	je	.L1203
	movq	(%r12), %r15
	movq	41112(%r15), %rdi
	movq	12464(%r15), %r8
	testq	%rdi, %rdi
	je	.L1204
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1205:
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal7Isolate9MayAccessENS0_6HandleINS0_7ContextEEENS2_INS0_8JSObjectEEE@PLT
	testb	%al, %al
	jne	.L1203
	cmpl	$1, 32(%r12)
	jne	.L1230
.L1207:
	movl	$1, %eax
	jmp	.L1210
	.p2align 4,,10
	.p2align 3
.L1203:
	movl	36(%r12), %eax
.L1209:
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	testb	$64, %al
	jne	.L1231
	call	_ZN2v88internal14KeyAccumulator24CollectOwnElementIndicesENS0_6HandleINS0_10JSReceiverEEENS2_INS0_8JSObjectEEE
	testb	%al, %al
	jne	.L1221
.L1228:
	xorl	%eax, %eax
	movb	$0, %ah
.L1210:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1232
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1231:
	.cfi_restore_state
	call	_ZN2v88internal14KeyAccumulator19CollectPrivateNamesENS0_6HandleINS0_10JSReceiverEEENS2_INS0_8JSObjectEEE
	testb	%al, %al
	je	.L1228
.L1220:
	movl	$257, %eax
	jmp	.L1210
	.p2align 4,,10
	.p2align 3
.L1204:
	movq	41088(%r15), %rsi
	cmpq	41096(%r15), %rsi
	je	.L1233
.L1206:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r15)
	movq	%r8, (%rsi)
	jmp	.L1205
	.p2align 4,,10
	.p2align 3
.L1229:
	movq	%rbx, %r15
	leaq	-64(%rbp), %rdi
	andq	$-262144, %r15
	movq	24(%r15), %rax
	movq	-25128(%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal7Context13global_objectEv@PLT
	movq	24(%r15), %rdx
	testb	$1, %bl
	jne	.L1234
.L1200:
	movq	-1(%rbx), %rdx
	movq	23(%rdx), %rdx
.L1201:
	cmpq	%rax, %rdx
	setne	%al
	jmp	.L1202
	.p2align 4,,10
	.p2align 3
.L1230:
	movq	(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal15AccessCheckInfo3GetEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1212
	movq	(%r12), %rbx
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1213
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L1212
	movq	(%rax), %rsi
.L1215:
	cmpq	$0, 15(%rsi)
	je	.L1212
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal14KeyAccumulator33CollectAccessCheckInterceptorKeysENS0_6HandleINS0_15AccessCheckInfoEEENS2_INS0_10JSReceiverEEENS2_INS0_8JSObjectEEE
	testb	%al, %al
	jne	.L1207
	jmp	.L1228
	.p2align 4,,10
	.p2align 3
.L1221:
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal14KeyAccumulator23CollectOwnPropertyNamesENS0_6HandleINS0_10JSReceiverEEENS2_INS0_8JSObjectEEE
	testb	%al, %al
	jne	.L1220
	jmp	.L1228
	.p2align 4,,10
	.p2align 3
.L1212:
	movl	36(%r12), %eax
	orl	$32, %eax
	movl	%eax, 36(%r12)
	jmp	.L1209
	.p2align 4,,10
	.p2align 3
.L1233:
	movq	%r15, %rdi
	movq	%r8, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L1206
	.p2align 4,,10
	.p2align 3
.L1213:
	movq	41088(%rbx), %r8
	cmpq	41096(%rbx), %r8
	je	.L1235
.L1216:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r8)
	jmp	.L1215
	.p2align 4,,10
	.p2align 3
.L1234:
	movq	-1(%rbx), %rcx
	cmpw	$1024, 11(%rcx)
	jne	.L1200
	movq	-37488(%rdx), %rdx
	jmp	.L1201
.L1235:
	movq	%rbx, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L1216
.L1232:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19031:
	.size	_ZN2v88internal14KeyAccumulator14CollectOwnKeysENS0_6HandleINS0_10JSReceiverEEENS2_INS0_8JSObjectEEE, .-_ZN2v88internal14KeyAccumulator14CollectOwnKeysENS0_6HandleINS0_10JSReceiverEEENS2_INS0_8JSObjectEEE
	.section	.text._ZN2v88internal14KeyAccumulator27CollectOwnJSProxyTargetKeysENS0_6HandleINS0_7JSProxyEEENS2_INS0_10JSReceiverEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14KeyAccumulator27CollectOwnJSProxyTargetKeysENS0_6HandleINS0_7JSProxyEEENS2_INS0_10JSReceiverEEE
	.type	_ZN2v88internal14KeyAccumulator27CollectOwnJSProxyTargetKeysENS0_6HandleINS0_7JSProxyEEENS2_INS0_10JSReceiverEEE, @function
_ZN2v88internal14KeyAccumulator27CollectOwnJSProxyTargetKeysENS0_6HandleINS0_7JSProxyEEENS2_INS0_10JSReceiverEEE:
.LFB19038:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	movq	%rdx, %rsi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$112, %rsp
	movzwl	40(%rdi), %edx
	leaq	-80(%rbp), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	%rsi, -120(%rbp)
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movw	%dx, -40(%rbp)
	movq	%rsi, %rdx
	movq	$0, -72(%rbp)
	subq	$37592, %rax
	movb	$1, -38(%rbp)
	movq	%rax, -80(%rbp)
	movq	$0, -64(%rbp)
	movq	$0, -56(%rbp)
	movq	$0, -48(%rbp)
	call	_ZN2v88internal14KeyAccumulator11CollectKeysENS0_6HandleINS0_10JSReceiverEEES4_
	testb	%al, %al
	jne	.L1251
.L1238:
	xorl	%eax, %eax
	movb	$0, %ah
	.p2align 4,,10
	.p2align 3
.L1244:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1252
	addq	$112, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1251:
	.cfi_restore_state
	movq	-80(%rbp), %rdi
	movq	-72(%rbp), %r8
	leaq	288(%rdi), %rsi
	testq	%r8, %r8
	je	.L1240
	movl	-48(%rbp), %edx
	testl	%edx, %edx
	jne	.L1241
	movq	(%r8), %rax
	movq	%r8, %rsi
	movq	-1(%rax), %rax
	cmpq	%rax, 152(%rdi)
	je	.L1243
.L1241:
	movq	%r8, %rsi
	xorl	%edx, %edx
	call	_ZN2v88internal14OrderedHashSet18ConvertToKeysArrayEPNS0_7IsolateENS0_6HandleIS1_EENS0_17GetKeysConversionE@PLT
	movq	%rax, %rsi
.L1243:
	testq	%rsi, %rsi
	je	.L1238
.L1240:
	movzbl	40(%r12), %edx
	testb	%dl, %dl
	je	.L1253
.L1245:
	movq	%r12, %rdi
	call	_ZN2v88internal14KeyAccumulator7AddKeysENS0_6HandleINS0_10FixedArrayEEENS0_16AddKeyConversionE
	movl	%eax, %edx
.L1247:
	xorl	%eax, %eax
	movb	%dl, %al
	movb	$1, %ah
	jmp	.L1244
	.p2align 4,,10
	.p2align 3
.L1253:
	movl	36(%r12), %ecx
	movq	%rsi, %rdx
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal15FilterProxyKeysEPNS0_14KeyAccumulatorENS0_6HandleINS0_7JSProxyEEENS3_INS0_10FixedArrayEEENS0_14PropertyFilterE
	xorl	%edx, %edx
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1247
	movl	32(%r12), %eax
	testl	%eax, %eax
	jne	.L1254
	movq	%rsi, 8(%r12)
	movl	$1, %edx
	jmp	.L1247
	.p2align 4,,10
	.p2align 3
.L1254:
	movzbl	40(%r12), %edx
	jmp	.L1245
.L1252:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19038:
	.size	_ZN2v88internal14KeyAccumulator27CollectOwnJSProxyTargetKeysENS0_6HandleINS0_7JSProxyEEENS2_INS0_10JSReceiverEEE, .-_ZN2v88internal14KeyAccumulator27CollectOwnJSProxyTargetKeysENS0_6HandleINS0_7JSProxyEEENS2_INS0_10JSReceiverEEE
	.section	.rodata._ZN2v88internal14KeyAccumulator21CollectOwnJSProxyKeysENS0_6HandleINS0_10JSReceiverEEENS2_INS0_7JSProxyEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC19:
	.string	"../deps/v8/src/objects/keys.cc:916"
	.section	.text._ZN2v88internal14KeyAccumulator21CollectOwnJSProxyKeysENS0_6HandleINS0_10JSReceiverEEENS2_INS0_7JSProxyEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14KeyAccumulator21CollectOwnJSProxyKeysENS0_6HandleINS0_10JSReceiverEEENS2_INS0_7JSProxyEEE
	.type	_ZN2v88internal14KeyAccumulator21CollectOwnJSProxyKeysENS0_6HandleINS0_10JSReceiverEEENS2_INS0_7JSProxyEEE, @function
_ZN2v88internal14KeyAccumulator21CollectOwnJSProxyKeysENS0_6HandleINS0_10JSReceiverEEENS2_INS0_7JSProxyEEE:
.LFB19037:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$280, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -232(%rbp)
	movq	(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	37528(%rbx), %rax
	jb	.L1386
	movq	(%r12), %rbx
	movq	-232(%rbp), %rax
	cmpl	$64, 36(%r12)
	movq	41112(%rbx), %rdi
	movq	(%rax), %rax
	je	.L1387
	movq	23(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1265
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L1266:
	movq	-232(%rbp), %rax
	movq	(%rax), %rax
	movq	23(%rax), %rdx
	movq	-1(%rdx), %rdx
	cmpw	$1023, 11(%rdx)
	jbe	.L1388
	movq	(%r12), %rbx
	movq	15(%rax), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1269
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -256(%rbp)
.L1270:
	movq	(%r12), %rax
	movq	%r13, %rdi
	leaq	3048(%rax), %rsi
	call	_ZN2v88internal6Object9GetMethodENS0_6HandleINS0_10JSReceiverEEENS2_INS0_4NameEEE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1385
	movq	(%r12), %rdi
	movq	(%rax), %rax
	cmpq	%rax, 88(%rdi)
	je	.L1389
	movq	-256(%rbp), %rax
	leaq	-64(%rbp), %r8
	movl	$1, %ecx
	movq	%r13, %rdx
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal9Execution4CallEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_iPS6_@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1385
	movq	(%r12), %rdi
	movl	$1, %edx
	call	_ZN2v88internal6Object23CreateListFromArrayLikeEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementTypesE@PLT
	movq	%rax, -248(%rbp)
	testq	%rax, %rax
	je	.L1390
	movq	(%r12), %rax
	leaq	.LC19(%rip), %rdx
	movq	41136(%rax), %rsi
	leaq	-128(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -272(%rbp)
	call	_ZN2v88internal4ZoneC1EPNS0_19AccountingAllocatorEPKc@PLT
	movq	(%r12), %rax
	movq	-104(%rbp), %rdx
	movq	%rax, -192(%rbp)
	movq	-112(%rbp), %rax
	subq	%rax, %rdx
	cmpq	$191, %rdx
	jbe	.L1391
	leaq	192(%rax), %rdx
	movq	%rdx, -112(%rbp)
.L1277:
	movq	%rax, -208(%rbp)
	testq	%rax, %rax
	je	.L1392
	movb	$0, 16(%rax)
	movb	$0, 40(%rax)
	movb	$0, 64(%rax)
	movb	$0, 88(%rax)
	movb	$0, 112(%rax)
	movb	$0, 136(%rax)
	movb	$0, 160(%rax)
	movb	$0, 184(%rax)
	movq	-248(%rbp), %rax
	movq	$8, -200(%rbp)
	movq	(%rax), %rax
	movl	11(%rax), %r10d
	testl	%r10d, %r10d
	jle	.L1341
	leaq	-176(%rbp), %rcx
	movq	%r12, -240(%rbp)
	movl	$15, %ebx
	xorl	%r15d, %r15d
	movq	%rcx, -264(%rbp)
	leaq	-216(%rbp), %r14
	leaq	-208(%rbp), %r13
	jmp	.L1288
	.p2align 4,,10
	.p2align 3
.L1395:
	shrl	$2, %edx
	movl	%edx, %r12d
.L1284:
	movl	%r12d, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNK2v84base19TemplateHashMapImplINS_8internal6HandleINS2_4NameEEEiNS2_12_GLOBAL__N_114NameComparatorENS2_20ZoneAllocationPolicyEE5ProbeERKS5_j.isra.0
	cmpb	$0, 16(%rax)
	jne	.L1285
	movq	-216(%rbp), %rcx
	movl	$0, 8(%rax)
	movl	%r12d, 12(%rax)
	movq	%rcx, (%rax)
	movl	-196(%rbp), %ecx
	movb	$1, 16(%rax)
	addl	$1, %ecx
	movl	%ecx, %esi
	movl	%ecx, -196(%rbp)
	shrl	$2, %esi
	addl	%esi, %ecx
	cmpl	-200(%rbp), %ecx
	jnb	.L1393
.L1285:
	cmpl	$1, 8(%rax)
	je	.L1287
	movl	$1, 8(%rax)
	addl	$1, %r15d
	addq	$8, %rbx
	movq	-248(%rbp), %rax
	movq	(%rax), %rax
	cmpl	%r15d, 11(%rax)
	jle	.L1394
.L1288:
	movq	-240(%rbp), %rcx
	movq	(%rcx), %r12
	movq	(%rax,%rbx), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1280
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L1281:
	movq	%rax, -216(%rbp)
	movq	(%rax), %rax
	movl	7(%rax), %edx
	testb	$1, %dl
	je	.L1395
	movq	-264(%rbp), %rdi
	movq	%rax, -176(%rbp)
	call	_ZN2v88internal6String17ComputeAndSetHashEv@PLT
	movl	%eax, %r12d
	jmp	.L1284
	.p2align 4,,10
	.p2align 3
.L1265:
	movq	41088(%rbx), %r13
	cmpq	%r13, 41096(%rbx)
	je	.L1396
.L1267:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, 0(%r13)
	jmp	.L1266
	.p2align 4,,10
	.p2align 3
.L1386:
	movq	(%r12), %rdi
	call	_ZN2v88internal7Isolate13StackOverflowEv@PLT
	xorl	%eax, %eax
	movb	$0, %ah
.L1257:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1397
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1269:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	movq	%rax, -256(%rbp)
	cmpq	41096(%rbx), %rax
	je	.L1398
.L1271:
	movq	-256(%rbp), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rcx)
	jmp	.L1270
	.p2align 4,,10
	.p2align 3
.L1280:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L1399
.L1282:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L1281
	.p2align 4,,10
	.p2align 3
.L1393:
	movq	-272(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v84base19TemplateHashMapImplINS_8internal6HandleINS2_4NameEEEiNS2_12_GLOBAL__N_114NameComparatorENS2_20ZoneAllocationPolicyEE6ResizeES8_
	movl	%r12d, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNK2v84base19TemplateHashMapImplINS_8internal6HandleINS2_4NameEEEiNS2_12_GLOBAL__N_114NameComparatorENS2_20ZoneAllocationPolicyEE5ProbeERKS5_j.isra.0
	jmp	.L1285
	.p2align 4,,10
	.p2align 3
.L1388:
	movq	(%r12), %r12
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$145, %esi
	leaq	3048(%r12), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	xorl	%eax, %eax
	movb	$0, %ah
	jmp	.L1257
	.p2align 4,,10
	.p2align 3
.L1387:
	movq	7(%rax), %rsi
	testb	$1, %sil
	jne	.L1260
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-36536(%rax), %rsi
.L1260:
	testq	%rdi, %rdi
	je	.L1261
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdi
.L1262:
	movq	%r12, %rsi
	call	_ZN2v88internal18BaseNameDictionaryINS0_14NameDictionaryENS0_19NameDictionaryShapeEE13CollectKeysToENS0_6HandleIS2_EEPNS0_14KeyAccumulatorE@PLT
	movl	%eax, %r8d
	movl	$257, %eax
	testb	%r8b, %r8b
	jne	.L1257
	xorb	%al, %al
	movb	$0, %ah
	jmp	.L1257
	.p2align 4,,10
	.p2align 3
.L1389:
	movq	-256(%rbp), %rdx
	movq	-232(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal14KeyAccumulator27CollectOwnJSProxyTargetKeysENS0_6HandleINS0_7JSProxyEEENS2_INS0_10JSReceiverEEE
	jmp	.L1257
	.p2align 4,,10
	.p2align 3
.L1385:
	xorl	%eax, %eax
	movb	$0, %ah
	jmp	.L1257
	.p2align 4,,10
	.p2align 3
.L1399:
	movq	%r12, %rdi
	movq	%rsi, -280(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-280(%rbp), %rsi
	jmp	.L1282
	.p2align 4,,10
	.p2align 3
.L1287:
	movq	-240(%rbp), %r12
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$142, %esi
	movq	(%r12), %r12
.L1382:
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
.L1384:
	xorl	%eax, %eax
	movb	$0, %ah
.L1289:
	movq	-272(%rbp), %rdi
	movl	%eax, -232(%rbp)
	call	_ZN2v88internal4ZoneD1Ev@PLT
	movl	-232(%rbp), %eax
	jmp	.L1257
	.p2align 4,,10
	.p2align 3
.L1261:
	movq	41088(%rbx), %rdi
	cmpq	%rdi, 41096(%rbx)
	je	.L1400
.L1263:
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rdi)
	jmp	.L1262
	.p2align 4,,10
	.p2align 3
.L1394:
	movq	-240(%rbp), %r12
.L1279:
	movq	-256(%rbp), %rdi
	call	_ZN2v88internal10JSReceiver12IsExtensibleENS0_6HandleIS1_EE@PLT
	testb	%al, %al
	je	.L1384
	movq	-256(%rbp), %rdi
	movzbl	%ah, %eax
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movw	%ax, -282(%rbp)
	call	_ZN2v88internal14KeyAccumulator7GetKeysENS0_6HandleINS0_10JSReceiverEEENS0_17KeyCollectionModeENS0_14PropertyFilterENS0_17GetKeysConversionEbb
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L1384
	movq	(%rax), %rax
	movq	(%r12), %rdi
	xorl	%edx, %edx
	movslq	11(%rax), %rsi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	%rax, -280(%rbp)
	movq	(%rbx), %rax
	movl	11(%rax), %r9d
	testl	%r9d, %r9d
	jle	.L1292
	xorl	%r14d, %r14d
	leaq	-176(%rbp), %rax
	movl	%r15d, -304(%rbp)
	movq	%rbx, %r15
	movl	$0, -264(%rbp)
	movq	%r14, %rbx
	movq	-256(%rbp), %r14
	movq	%rax, -240(%rbp)
	jmp	.L1304
	.p2align 4,,10
	.p2align 3
.L1403:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L1294:
	movq	(%r12), %rdi
	movq	-240(%rbp), %rcx
	movq	%r14, %rsi
	call	_ZN2v88internal10JSReceiver24GetOwnPropertyDescriptorEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEEPNS0_18PropertyDescriptorE@PLT
	testb	%al, %al
	je	.L1384
	shrw	$8, %ax
	je	.L1297
	testb	$4, -176(%rbp)
	je	.L1401
.L1297:
	movq	(%r15), %rax
	addq	$1, %rbx
	cmpl	%ebx, 11(%rax)
	jle	.L1402
.L1304:
	pxor	%xmm0, %xmm0
	andb	$-64, -176(%rbp)
	movq	(%r12), %rcx
	leaq	16(,%rbx,8), %r13
	movups	%xmm0, -168(%rbp)
	movups	%xmm0, -152(%rbp)
	movq	(%r15), %rax
	movq	-1(%r13,%rax), %rsi
	movq	41112(%rcx), %rdi
	testq	%rdi, %rdi
	jne	.L1403
	movq	41088(%rcx), %rdx
	cmpq	41096(%rcx), %rdx
	je	.L1404
.L1295:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%rcx)
	movq	%rsi, (%rdx)
	jmp	.L1294
	.p2align 4,,10
	.p2align 3
.L1396:
	movq	%rbx, %rdi
	movq	%rsi, -240(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-240(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L1267
	.p2align 4,,10
	.p2align 3
.L1401:
	movq	-280(%rbp), %rax
	movq	(%rax), %rdi
	movq	(%r15), %rax
	movq	-1(%r13,%rax), %rdx
	movl	-264(%rbp), %eax
	leal	16(,%rax,8), %eax
	cltq
	leaq	-1(%rdi,%rax), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L1339
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	8(%rax), %rcx
	movq	%rax, -256(%rbp)
	testl	$262144, %ecx
	je	.L1299
	movq	%rdx, -320(%rbp)
	movq	%rsi, -312(%rbp)
	movq	%rdi, -296(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-256(%rbp), %rax
	movq	-320(%rbp), %rdx
	movq	-312(%rbp), %rsi
	movq	-296(%rbp), %rdi
	movq	8(%rax), %rcx
.L1299:
	andl	$24, %ecx
	je	.L1339
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1339
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L1339:
	movq	(%r15), %rdi
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %rdx
	addl	$1, -264(%rbp)
	leaq	-1(%rdi,%r13), %r13
	movq	%rdx, 0(%r13)
	testb	$1, %dl
	je	.L1297
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	8(%rax), %rcx
	movq	%rax, -256(%rbp)
	testl	$262144, %ecx
	je	.L1302
	movq	%r13, %rsi
	movq	%rdx, -312(%rbp)
	movq	%rdi, -296(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-256(%rbp), %rax
	movq	-312(%rbp), %rdx
	movq	-296(%rbp), %rdi
	movq	8(%rax), %rcx
.L1302:
	andl	$24, %ecx
	je	.L1297
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1297
	movq	%r13, %rsi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1297
	.p2align 4,,10
	.p2align 3
.L1398:
	movq	%rbx, %rdi
	movq	%rsi, -240(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-240(%rbp), %rsi
	movq	%rax, -256(%rbp)
	jmp	.L1271
	.p2align 4,,10
	.p2align 3
.L1390:
	movb	$0, %ah
	jmp	.L1257
	.p2align 4,,10
	.p2align 3
.L1404:
	movq	%rcx, %rdi
	movq	%rsi, -296(%rbp)
	movq	%rcx, -256(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-296(%rbp), %rsi
	movq	-256(%rbp), %rcx
	movq	%rax, %rdx
	jmp	.L1295
	.p2align 4,,10
	.p2align 3
.L1391:
	movq	-272(%rbp), %rdi
	movl	$192, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1277
	.p2align 4,,10
	.p2align 3
.L1402:
	movl	-264(%rbp), %r8d
	movq	%r15, %rbx
	movl	-304(%rbp), %r15d
	testl	%r8d, %r8d
	jne	.L1375
	cmpb	$0, -282(%rbp)
	je	.L1378
.L1309:
	movzbl	40(%r12), %eax
	testb	%al, %al
	je	.L1405
.L1307:
	movq	-248(%rbp), %rsi
	movzbl	%al, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal14KeyAccumulator7AddKeysENS0_6HandleINS0_10FixedArrayEEENS0_16AddKeyConversionE
	movl	%eax, %edx
.L1313:
	xorl	%eax, %eax
	movb	%dl, %al
	movb	$1, %ah
	jmp	.L1289
.L1400:
	movq	%rbx, %rdi
	movq	%rsi, -232(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-232(%rbp), %rsi
	movq	%rax, %rdi
	jmp	.L1263
.L1378:
	movl	-264(%rbp), %esi
	testl	%esi, %esi
	je	.L1315
.L1375:
	movl	%r15d, %eax
	subl	-264(%rbp), %eax
	movq	%r12, -256(%rbp)
	leaq	-216(%rbp), %r14
	movl	%eax, -264(%rbp)
	movq	-280(%rbp), %r12
	movl	$16, %eax
	leaq	-208(%rbp), %r13
	movq	%rbx, -296(%rbp)
	movq	%rax, %rbx
.L1324:
	movq	(%r12), %rax
	movq	-1(%rbx,%rax), %rsi
	movq	-256(%rbp), %rax
	movq	(%rax), %rdx
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L1316
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L1317:
	movq	%rax, -216(%rbp)
	movq	(%rax), %rax
	movl	7(%rax), %edx
	testb	$1, %dl
	jne	.L1319
	shrl	$2, %edx
.L1320:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNK2v84base19TemplateHashMapImplINS_8internal6HandleINS2_4NameEEEiNS2_12_GLOBAL__N_114NameComparatorENS2_20ZoneAllocationPolicyEE5ProbeERKS5_j.isra.0
	cmpb	$0, 16(%rax)
	je	.L1322
	movl	8(%rax), %ecx
	testl	%ecx, %ecx
	je	.L1322
	movl	$0, 8(%rax)
	subl	$1, %r15d
	addq	$8, %rbx
	cmpl	-264(%rbp), %r15d
	jne	.L1324
	movq	-296(%rbp), %rbx
	movq	-256(%rbp), %r12
.L1315:
	cmpb	$0, -282(%rbp)
	je	.L1406
.L1337:
	movq	-248(%rbp), %rdx
	movq	-232(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal14KeyAccumulator18AddKeysFromJSProxyENS0_6HandleINS0_7JSProxyEEENS2_INS0_10FixedArrayEEE
	jmp	.L1289
	.p2align 4,,10
	.p2align 3
.L1319:
	movq	-240(%rbp), %rdi
	movq	%rax, -176(%rbp)
	call	_ZN2v88internal6String17ComputeAndSetHashEv@PLT
	movl	%eax, %edx
	jmp	.L1320
	.p2align 4,,10
	.p2align 3
.L1316:
	movq	41088(%rdx), %rax
	cmpq	41096(%rdx), %rax
	je	.L1407
.L1318:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%rdx)
	movq	%rsi, (%rax)
	jmp	.L1317
.L1322:
	movq	-256(%rbp), %r12
.L1383:
	movq	(%r12), %r12
	movq	-216(%rbp), %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$140, %esi
	jmp	.L1382
.L1341:
	xorl	%r15d, %r15d
	jmp	.L1279
.L1407:
	movq	%rdx, %rdi
	movq	%rsi, -304(%rbp)
	movq	%rdx, -280(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-304(%rbp), %rsi
	movq	-280(%rbp), %rdx
	jmp	.L1318
.L1405:
	movq	-248(%rbp), %rdx
	movl	36(%r12), %ecx
	movq	%r12, %rdi
	movq	-232(%rbp), %rsi
	call	_ZN2v88internal15FilterProxyKeysEPNS0_14KeyAccumulatorENS0_6HandleINS0_7JSProxyEEENS3_INS0_10FixedArrayEEENS0_14PropertyFilterE
	xorl	%edx, %edx
	movq	%rax, -248(%rbp)
	testq	%rax, %rax
	je	.L1313
	movl	32(%r12), %edi
	testl	%edi, %edi
	je	.L1314
	movzbl	40(%r12), %eax
	jmp	.L1307
.L1292:
	cmpb	$0, -282(%rbp)
	jne	.L1309
.L1306:
	testl	%r15d, %r15d
	je	.L1337
	movq	(%r12), %r12
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$141, %esi
	jmp	.L1382
.L1406:
	movq	(%rbx), %rax
	xorl	%ecx, %ecx
	leaq	-216(%rbp), %r13
	movq	%rcx, %r14
	cmpl	$0, 11(%rax)
	jle	.L1306
.L1326:
	movq	15(%rax,%r14,8), %rsi
	testb	$1, %sil
	je	.L1328
	movq	(%r12), %rdx
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L1329
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L1330:
	movq	%rax, -216(%rbp)
	movq	(%rax), %rax
	movl	7(%rax), %edx
	testb	$1, %dl
	jne	.L1332
	shrl	$2, %edx
.L1333:
	leaq	-208(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNK2v84base19TemplateHashMapImplINS_8internal6HandleINS2_4NameEEEiNS2_12_GLOBAL__N_114NameComparatorENS2_20ZoneAllocationPolicyEE5ProbeERKS5_j.isra.0
	cmpb	$0, 16(%rax)
	je	.L1383
	movl	8(%rax), %edx
	testl	%edx, %edx
	je	.L1383
	movl	$0, 8(%rax)
	movq	(%rbx), %rax
	subl	$1, %r15d
.L1328:
	addq	$1, %r14
	cmpl	%r14d, 11(%rax)
	jg	.L1326
	jmp	.L1306
.L1332:
	movq	-240(%rbp), %rdi
	movq	%rax, -176(%rbp)
	call	_ZN2v88internal6String17ComputeAndSetHashEv@PLT
	movl	%eax, %edx
	jmp	.L1333
.L1329:
	movq	41088(%rdx), %rax
	cmpq	41096(%rdx), %rax
	je	.L1408
.L1331:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%rdx)
	movq	%rsi, (%rax)
	jmp	.L1330
.L1314:
	movq	-248(%rbp), %rax
	movl	$1, %edx
	movq	%rax, 8(%r12)
	jmp	.L1313
.L1397:
	call	__stack_chk_fail@PLT
.L1408:
	movq	%rdx, %rdi
	movq	%rsi, -264(%rbp)
	movq	%rdx, -256(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-264(%rbp), %rsi
	movq	-256(%rbp), %rdx
	jmp	.L1331
.L1392:
	leaq	.LC0(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19037:
	.size	_ZN2v88internal14KeyAccumulator21CollectOwnJSProxyKeysENS0_6HandleINS0_10JSReceiverEEENS2_INS0_7JSProxyEEE, .-_ZN2v88internal14KeyAccumulator21CollectOwnJSProxyKeysENS0_6HandleINS0_10JSReceiverEEENS2_INS0_7JSProxyEEE
	.section	.rodata._ZN2v88internal14KeyAccumulator11CollectKeysENS0_6HandleINS0_10JSReceiverEEES4_.str1.1,"aMS",@progbits,1
.LC20:
	.string	"!handle_.is_null()"
	.section	.text._ZN2v88internal14KeyAccumulator11CollectKeysENS0_6HandleINS0_10JSReceiverEEES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14KeyAccumulator11CollectKeysENS0_6HandleINS0_10JSReceiverEEES4_
	.type	_ZN2v88internal14KeyAccumulator11CollectKeysENS0_6HandleINS0_10JSReceiverEEES4_, @function
_ZN2v88internal14KeyAccumulator11CollectKeysENS0_6HandleINS0_10JSReceiverEEES4_:
.LFB19006:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	32(%rdi), %eax
	movl	%eax, -56(%rbp)
	testl	%eax, %eax
	je	.L1454
	movq	(%rdi), %r12
	testq	%rdx, %rdx
	je	.L1455
.L1412:
	movl	$0, -52(%rbp)
	jmp	.L1418
	.p2align 4,,10
	.p2align 3
.L1458:
	addl	$1, -52(%rbp)
	movl	-52(%rbp), %eax
	cmpl	$102400, %eax
	jg	.L1456
	movq	%r15, %rdi
	call	_ZN2v88internal7JSProxy12GetPrototypeENS0_6HandleIS1_EE@PLT
	testq	%rax, %rax
	je	.L1431
	movl	-56(%rbp), %edx
	testl	%edx, %edx
	je	.L1429
	movq	(%rax), %rsi
	cmpq	%rsi, 104(%r12)
	sete	%bl
.L1429:
	movq	16(%r14), %rcx
	testq	%rcx, %rcx
	je	.L1434
	movq	(%r15), %rdx
	cmpq	%rdx, (%rcx)
	je	.L1417
.L1434:
	testb	%bl, %bl
	jne	.L1417
	movq	%rax, %r15
.L1418:
	cmpq	$0, 24(%r14)
	je	.L1419
	movb	$0, 42(%r14)
.L1419:
	movq	(%r15), %rax
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	je	.L1457
	call	_ZN2v88internal14KeyAccumulator14CollectOwnKeysENS0_6HandleINS0_10JSReceiverEEENS2_INS0_8JSObjectEEE
	movl	%eax, %ecx
	movzbl	%ah, %ebx
.L1421:
	testb	%cl, %cl
	je	.L1431
	testb	%bl, %bl
	je	.L1417
	movq	(%r15), %rax
	movq	-1(%rax), %rcx
	cmpw	$1024, 11(%rcx)
	je	.L1458
	movq	-1(%rax), %rax
	movq	23(%rax), %rsi
	cmpq	104(%r12), %rsi
	je	.L1426
	movl	-56(%rbp), %ecx
	xorl	%ebx, %ebx
	testl	%ecx, %ecx
	jne	.L1426
	cmpw	$1026, 11(%rax)
	setne	%bl
.L1426:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1459
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	jmp	.L1429
	.p2align 4,,10
	.p2align 3
.L1457:
	call	_ZN2v88internal14KeyAccumulator21CollectOwnJSProxyKeysENS0_6HandleINS0_10JSReceiverEEENS2_INS0_7JSProxyEEE
	movl	%eax, %ecx
	movzbl	%ah, %ebx
	jmp	.L1421
	.p2align 4,,10
	.p2align 3
.L1459:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L1460
.L1428:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L1429
	.p2align 4,,10
	.p2align 3
.L1456:
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate13StackOverflowEv@PLT
.L1431:
	xorl	%eax, %eax
	movb	$0, %ah
.L1445:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1417:
	.cfi_restore_state
	addq	$24, %rsp
	movl	$257, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1454:
	.cfi_restore_state
	movq	(%rdx), %rax
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	je	.L1411
	movq	(%rdi), %r12
	jmp	.L1412
	.p2align 4,,10
	.p2align 3
.L1460:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-64(%rbp), %rsi
	jmp	.L1428
	.p2align 4,,10
	.p2align 3
.L1455:
	leaq	.LC20(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1411:
	call	_ZN2v88internal14KeyAccumulator21CollectOwnJSProxyKeysENS0_6HandleINS0_10JSReceiverEEENS2_INS0_7JSProxyEEE
	movl	%eax, %r8d
	movl	$257, %eax
	testb	%r8b, %r8b
	jne	.L1445
	xorb	%al, %al
	movb	$0, %ah
	jmp	.L1445
	.cfi_endproc
.LFE19006:
	.size	_ZN2v88internal14KeyAccumulator11CollectKeysENS0_6HandleINS0_10JSReceiverEEES4_, .-_ZN2v88internal14KeyAccumulator11CollectKeysENS0_6HandleINS0_10JSReceiverEEES4_
	.section	.text._ZN2v88internal18FastKeyAccumulator11GetKeysSlowENS0_17GetKeysConversionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18FastKeyAccumulator11GetKeysSlowENS0_17GetKeysConversionE
	.type	_ZN2v88internal18FastKeyAccumulator11GetKeysSlowENS0_17GetKeysConversionE, @function
_ZN2v88internal18FastKeyAccumulator11GetKeysSlowENS0_17GetKeysConversionE:
.LFB19020:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	subq	$72, %rsp
	movq	(%rdi), %rdx
	movq	8(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	movq	$0, -72(%rbp)
	movq	%rdx, -80(%rbp)
	movq	%rsi, %rdx
	movq	%rax, -48(%rbp)
	movzwl	32(%rdi), %eax
	movq	$0, -56(%rbp)
	movw	%ax, -40(%rbp)
	movq	16(%rdi), %rax
	leaq	-80(%rbp), %rdi
	movb	$1, -38(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal14KeyAccumulator11CollectKeysENS0_6HandleINS0_10JSReceiverEEES4_
	testb	%al, %al
	jne	.L1462
	xorl	%eax, %eax
.L1463:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1470
	addq	$72, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1462:
	.cfi_restore_state
	movq	-80(%rbp), %rdi
	movq	-72(%rbp), %rsi
	leaq	288(%rdi), %rax
	testq	%rsi, %rsi
	je	.L1463
	movl	-48(%rbp), %eax
	testl	%eax, %eax
	jne	.L1466
	movq	(%rsi), %rax
	movq	-1(%rax), %rdx
	movq	%rsi, %rax
	cmpq	%rdx, 152(%rdi)
	je	.L1463
.L1466:
	movl	%r12d, %edx
	call	_ZN2v88internal14OrderedHashSet18ConvertToKeysArrayEPNS0_7IsolateENS0_6HandleIS1_EENS0_17GetKeysConversionE@PLT
	jmp	.L1463
.L1470:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19020:
	.size	_ZN2v88internal18FastKeyAccumulator11GetKeysSlowENS0_17GetKeysConversionE, .-_ZN2v88internal18FastKeyAccumulator11GetKeysSlowENS0_17GetKeysConversionE
	.section	.text._ZN2v88internal14KeyAccumulator7GetKeysENS0_6HandleINS0_10JSReceiverEEENS0_17KeyCollectionModeENS0_14PropertyFilterENS0_17GetKeysConversionEbb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14KeyAccumulator7GetKeysENS0_6HandleINS0_10JSReceiverEEENS0_17KeyCollectionModeENS0_14PropertyFilterENS0_17GetKeysConversionEbb
	.type	_ZN2v88internal14KeyAccumulator7GetKeysENS0_6HandleINS0_10JSReceiverEEENS0_17KeyCollectionModeENS0_14PropertyFilterENS0_17GetKeysConversionEbb, @function
_ZN2v88internal14KeyAccumulator7GetKeysENS0_6HandleINS0_10JSReceiverEEENS0_17KeyCollectionModeENS0_14PropertyFilterENS0_17GetKeysConversionEbb:
.LFB18992:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%ecx, %r12d
	xorl	%ecx, %ecx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	$0, -112(%rbp)
	movl	%esi, -104(%rbp)
	subq	$37592, %rax
	movl	%edx, -100(%rbp)
	movq	%rax, %xmm0
	movb	%r8b, -96(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movb	%r9b, -95(%rbp)
	movw	%cx, -94(%rbp)
	movaps	%xmm0, -128(%rbp)
	testl	%esi, %esi
	je	.L1472
	leaq	-128(%rbp), %rdi
	call	_ZN2v88internal18FastKeyAccumulator7PrepareEv.part.0
	movl	-100(%rbp), %edx
.L1472:
	movq	-128(%rbp), %rcx
	cmpl	$18, %edx
	je	.L1489
.L1476:
	movl	-104(%rbp), %eax
	movq	-120(%rbp), %rsi
	movl	%edx, -44(%rbp)
	leaq	-80(%rbp), %rdi
	movq	%rcx, -80(%rbp)
	movl	%eax, -48(%rbp)
	movzwl	-96(%rbp), %eax
	movq	%rsi, %rdx
	movq	$0, -72(%rbp)
	movw	%ax, -40(%rbp)
	movq	-112(%rbp), %rax
	movq	$0, -56(%rbp)
	movb	$1, -38(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal14KeyAccumulator11CollectKeysENS0_6HandleINS0_10JSReceiverEEES4_
	movl	%eax, %r8d
	xorl	%eax, %eax
	testb	%r8b, %r8b
	jne	.L1490
.L1475:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1491
	addq	$120, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1490:
	.cfi_restore_state
	movq	-80(%rbp), %rdi
	movq	-72(%rbp), %rsi
	leaq	288(%rdi), %rax
	testq	%rsi, %rsi
	je	.L1475
	movl	-48(%rbp), %eax
	testl	%eax, %eax
	jne	.L1480
	movq	(%rsi), %rax
	movq	-1(%rax), %rdx
	movq	%rsi, %rax
	cmpq	%rdx, 152(%rdi)
	je	.L1475
.L1480:
	movl	%r12d, %edx
	call	_ZN2v88internal14OrderedHashSet18ConvertToKeysArrayEPNS0_7IsolateENS0_6HandleIS1_EENS0_17GetKeysConversionE@PLT
	jmp	.L1475
	.p2align 4,,10
	.p2align 3
.L1489:
	leaq	-128(%rbp), %rdi
	movl	%r12d, %esi
	call	_ZN2v88internal18FastKeyAccumulator11GetKeysFastENS0_17GetKeysConversionE
	testq	%rax, %rax
	jne	.L1475
	movq	-128(%rbp), %rcx
	movq	12480(%rcx), %rdx
	cmpq	%rdx, 96(%rcx)
	jne	.L1475
	movl	-100(%rbp), %edx
	jmp	.L1476
.L1491:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18992:
	.size	_ZN2v88internal14KeyAccumulator7GetKeysENS0_6HandleINS0_10JSReceiverEEENS0_17KeyCollectionModeENS0_14PropertyFilterENS0_17GetKeysConversionEbb, .-_ZN2v88internal14KeyAccumulator7GetKeysENS0_6HandleINS0_10JSReceiverEEENS0_17KeyCollectionModeENS0_14PropertyFilterENS0_17GetKeysConversionEbb
	.section	.text._ZN2v88internal18FastKeyAccumulator7GetKeysENS0_17GetKeysConversionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18FastKeyAccumulator7GetKeysENS0_17GetKeysConversionE
	.type	_ZN2v88internal18FastKeyAccumulator7GetKeysENS0_17GetKeysConversionE, @function
_ZN2v88internal18FastKeyAccumulator7GetKeysENS0_17GetKeysConversionE:
.LFB19017:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	28(%rdi), %eax
	cmpl	$18, %eax
	je	.L1507
	movq	(%rdi), %rcx
.L1496:
	movl	24(%rbx), %edx
	movq	8(%rbx), %rsi
	movl	%eax, -44(%rbp)
	leaq	-80(%rbp), %rdi
	movzwl	32(%rbx), %eax
	movq	%rcx, -80(%rbp)
	movl	%edx, -48(%rbp)
	movq	%rsi, %rdx
	movw	%ax, -40(%rbp)
	movq	16(%rbx), %rax
	movq	$0, -72(%rbp)
	movq	$0, -56(%rbp)
	movb	$1, -38(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal14KeyAccumulator11CollectKeysENS0_6HandleINS0_10JSReceiverEEES4_
	movl	%eax, %r8d
	xorl	%eax, %eax
	testb	%r8b, %r8b
	jne	.L1508
.L1497:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1509
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1508:
	.cfi_restore_state
	movq	-80(%rbp), %rdi
	movq	-72(%rbp), %rsi
	leaq	288(%rdi), %rax
	testq	%rsi, %rsi
	je	.L1497
	movl	-48(%rbp), %eax
	testl	%eax, %eax
	jne	.L1501
	movq	(%rsi), %rax
	movq	-1(%rax), %rdx
	movq	%rsi, %rax
	cmpq	%rdx, 152(%rdi)
	je	.L1497
.L1501:
	movl	%r12d, %edx
	call	_ZN2v88internal14OrderedHashSet18ConvertToKeysArrayEPNS0_7IsolateENS0_6HandleIS1_EENS0_17GetKeysConversionE@PLT
	jmp	.L1497
	.p2align 4,,10
	.p2align 3
.L1507:
	call	_ZN2v88internal18FastKeyAccumulator11GetKeysFastENS0_17GetKeysConversionE
	testq	%rax, %rax
	jne	.L1497
	movq	(%rbx), %rcx
	movq	12480(%rcx), %rdx
	cmpq	%rdx, 96(%rcx)
	jne	.L1497
	movl	28(%rbx), %eax
	jmp	.L1496
.L1509:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19017:
	.size	_ZN2v88internal18FastKeyAccumulator7GetKeysENS0_17GetKeysConversionE, .-_ZN2v88internal18FastKeyAccumulator7GetKeysENS0_17GetKeysConversionE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal14KeyAccumulator7GetKeysENS0_6HandleINS0_10JSReceiverEEENS0_17KeyCollectionModeENS0_14PropertyFilterENS0_17GetKeysConversionEbb,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal14KeyAccumulator7GetKeysENS0_6HandleINS0_10JSReceiverEEENS0_17KeyCollectionModeENS0_14PropertyFilterENS0_17GetKeysConversionEbb, @function
_GLOBAL__sub_I__ZN2v88internal14KeyAccumulator7GetKeysENS0_6HandleINS0_10JSReceiverEEENS0_17KeyCollectionModeENS0_14PropertyFilterENS0_17GetKeysConversionEbb:
.LFB23700:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE23700:
	.size	_GLOBAL__sub_I__ZN2v88internal14KeyAccumulator7GetKeysENS0_6HandleINS0_10JSReceiverEEENS0_17KeyCollectionModeENS0_14PropertyFilterENS0_17GetKeysConversionEbb, .-_GLOBAL__sub_I__ZN2v88internal14KeyAccumulator7GetKeysENS0_6HandleINS0_10JSReceiverEEENS0_17KeyCollectionModeENS0_14PropertyFilterENS0_17GetKeysConversionEbb
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal14KeyAccumulator7GetKeysENS0_6HandleINS0_10JSReceiverEEENS0_17KeyCollectionModeENS0_14PropertyFilterENS0_17GetKeysConversionEbb
	.weak	_ZZN2v88internal21ExternalCallbackScopeD4EvE27trace_event_unique_atomic68
	.section	.bss._ZZN2v88internal21ExternalCallbackScopeD4EvE27trace_event_unique_atomic68,"awG",@nobits,_ZZN2v88internal21ExternalCallbackScopeD4EvE27trace_event_unique_atomic68,comdat
	.align 8
	.type	_ZZN2v88internal21ExternalCallbackScopeD4EvE27trace_event_unique_atomic68, @gnu_unique_object
	.size	_ZZN2v88internal21ExternalCallbackScopeD4EvE27trace_event_unique_atomic68, 8
_ZZN2v88internal21ExternalCallbackScopeD4EvE27trace_event_unique_atomic68:
	.zero	8
	.weak	_ZZN2v88internal21ExternalCallbackScopeC4EPNS0_7IsolateEmE27trace_event_unique_atomic62
	.section	.bss._ZZN2v88internal21ExternalCallbackScopeC4EPNS0_7IsolateEmE27trace_event_unique_atomic62,"awG",@nobits,_ZZN2v88internal21ExternalCallbackScopeC4EPNS0_7IsolateEmE27trace_event_unique_atomic62,comdat
	.align 8
	.type	_ZZN2v88internal21ExternalCallbackScopeC4EPNS0_7IsolateEmE27trace_event_unique_atomic62, @gnu_unique_object
	.size	_ZZN2v88internal21ExternalCallbackScopeC4EPNS0_7IsolateEmE27trace_event_unique_atomic62, 8
_ZZN2v88internal21ExternalCallbackScopeC4EPNS0_7IsolateEmE27trace_event_unique_atomic62:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC4:
	.long	0
	.long	-1042284544
	.align 8
.LC5:
	.long	4290772992
	.long	1105199103
	.align 8
.LC11:
	.long	0
	.long	1127219200
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
