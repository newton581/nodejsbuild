	.file	"wasm-module-sourcemap.cc"
	.text
	.section	.rodata._ZNK2v88internal4wasm19WasmModuleSourceMap13GetSourceLineEm.str1.1,"aMS",@progbits,1
.LC0:
	.string	"offsets.begin() != up"
.LC1:
	.string	"Check failed: %s."
	.section	.text._ZNK2v88internal4wasm19WasmModuleSourceMap13GetSourceLineEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4wasm19WasmModuleSourceMap13GetSourceLineEm
	.type	_ZNK2v88internal4wasm19WasmModuleSourceMap13GetSourceLineEm, @function
_ZNK2v88internal4wasm19WasmModuleSourceMap13GetSourceLineEm:
.LFB8837:
	.cfi_startproc
	endbr64
	movq	(%rdi), %r9
	movq	8(%rdi), %rax
	subq	%r9, %rax
	movq	%r9, %r8
	sarq	$3, %rax
	.p2align 4,,10
	.p2align 3
.L3:
	testq	%rax, %rax
	jle	.L2
.L11:
	movq	%rax, %rdx
	sarq	%rdx
	leaq	(%r8,%rdx,8), %rcx
	cmpq	%rsi, (%rcx)
	ja	.L6
	subq	%rdx, %rax
	leaq	8(%rcx), %r8
	subq	$1, %rax
	testq	%rax, %rax
	jg	.L11
.L2:
	cmpq	%r8, %r9
	je	.L12
	movq	72(%rdi), %rax
	subq	%r9, %r8
	movq	-8(%rax,%r8), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movq	%rdx, %rax
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L12:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8837:
	.size	_ZNK2v88internal4wasm19WasmModuleSourceMap13GetSourceLineEm, .-_ZNK2v88internal4wasm19WasmModuleSourceMap13GetSourceLineEm
	.section	.rodata._ZNK2v88internal4wasm19WasmModuleSourceMap11GetFilenameB5cxx11Em.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"basic_string::_M_construct null not valid"
	.section	.text._ZNK2v88internal4wasm19WasmModuleSourceMap11GetFilenameB5cxx11Em,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4wasm19WasmModuleSourceMap11GetFilenameB5cxx11Em
	.type	_ZNK2v88internal4wasm19WasmModuleSourceMap11GetFilenameB5cxx11Em, @function
_ZNK2v88internal4wasm19WasmModuleSourceMap11GetFilenameB5cxx11Em:
.LFB8838:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	(%rsi), %r9
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	8(%rsi), %rax
	movq	%r9, %r8
	subq	%r9, %rax
	sarq	$3, %rax
	.p2align 4,,10
	.p2align 3
.L15:
	testq	%rax, %rax
	jle	.L14
.L35:
	movq	%rax, %rcx
	sarq	%rcx
	leaq	(%r8,%rcx,8), %rdi
	cmpq	%rdx, (%rdi)
	ja	.L24
	subq	%rcx, %rax
	leaq	8(%rdi), %r8
	subq	$1, %rax
	testq	%rax, %rax
	jg	.L35
.L14:
	cmpq	%r8, %r9
	je	.L36
	movq	48(%rsi), %rax
	subq	%r9, %r8
	leaq	16(%r12), %rdi
	movq	-8(%rax,%r8), %rdx
	salq	$5, %rdx
	addq	24(%rsi), %rdx
	movq	%rdi, (%r12)
	movq	(%rdx), %r14
	movq	8(%rdx), %r13
	movq	%r14, %rax
	addq	%r13, %rax
	je	.L18
	testq	%r14, %r14
	je	.L37
.L18:
	movq	%r13, -48(%rbp)
	cmpq	$15, %r13
	ja	.L38
	cmpq	$1, %r13
	jne	.L21
	movzbl	(%r14), %eax
	movb	%al, 16(%r12)
.L22:
	movq	%r13, 8(%r12)
	movb	$0, (%rdi,%r13)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L39
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	movq	%rcx, %rax
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L21:
	testq	%r13, %r13
	je	.L22
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L38:
	movq	%r12, %rdi
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-48(%rbp), %rax
	movq	%rax, 16(%r12)
.L20:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %r13
	movq	(%r12), %rdi
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L36:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L39:
	call	__stack_chk_fail@PLT
.L37:
	leaq	.LC2(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.cfi_endproc
.LFE8838:
	.size	_ZNK2v88internal4wasm19WasmModuleSourceMap11GetFilenameB5cxx11Em, .-_ZNK2v88internal4wasm19WasmModuleSourceMap11GetFilenameB5cxx11Em
	.section	.text._ZNK2v88internal4wasm19WasmModuleSourceMap9HasSourceEmm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4wasm19WasmModuleSourceMap9HasSourceEmm
	.type	_ZNK2v88internal4wasm19WasmModuleSourceMap9HasSourceEmm, @function
_ZNK2v88internal4wasm19WasmModuleSourceMap9HasSourceEmm:
.LFB8839:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rcx
	xorl	%eax, %eax
	cmpq	%rsi, -8(%rcx)
	jb	.L40
	movq	(%rdi), %rax
	cmpq	%rdx, (%rax)
	setb	%al
.L40:
	ret
	.cfi_endproc
.LFE8839:
	.size	_ZNK2v88internal4wasm19WasmModuleSourceMap9HasSourceEmm, .-_ZNK2v88internal4wasm19WasmModuleSourceMap9HasSourceEmm
	.section	.text._ZNK2v88internal4wasm19WasmModuleSourceMap13HasValidEntryEmm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4wasm19WasmModuleSourceMap13HasValidEntryEmm
	.type	_ZNK2v88internal4wasm19WasmModuleSourceMap13HasValidEntryEmm, @function
_ZNK2v88internal4wasm19WasmModuleSourceMap13HasValidEntryEmm:
.LFB8840:
	.cfi_startproc
	endbr64
	movq	(%rdi), %r9
	movq	8(%rdi), %rax
	subq	%r9, %rax
	movq	%r9, %r8
	sarq	$3, %rax
	.p2align 4,,10
	.p2align 3
.L45:
	testq	%rax, %rax
	jle	.L44
.L50:
	movq	%rax, %rcx
	sarq	%rcx
	leaq	(%r8,%rcx,8), %rdi
	cmpq	%rdx, (%rdi)
	ja	.L48
	subq	%rcx, %rax
	leaq	8(%rdi), %r8
	subq	$1, %rax
	testq	%rax, %rax
	jg	.L50
.L44:
	cmpq	%r8, %r9
	je	.L49
	cmpq	%rsi, -8(%r8)
	setnb	%al
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	movq	%rcx, %rax
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L49:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE8840:
	.size	_ZNK2v88internal4wasm19WasmModuleSourceMap13HasValidEntryEmm, .-_ZNK2v88internal4wasm19WasmModuleSourceMap13HasValidEntryEmm
	.section	.rodata._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJPcEEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_.str1.1,"aMS",@progbits,1
.LC3:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJPcEEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJPcEEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJPcEEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.type	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJPcEEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, @function
_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJPcEEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_:
.LFB9899:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movabsq	$288230376151711743, %rsi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r15
	movq	(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r15, %rax
	subq	%rbx, %rax
	sarq	$5, %rax
	cmpq	%rsi, %rax
	je	.L87
	movq	%r14, %rcx
	movq	%rdi, %r12
	subq	%rbx, %rcx
	testq	%rax, %rax
	je	.L73
	leaq	(%rax,%rax), %r13
	cmpq	%r13, %rax
	jbe	.L88
	movabsq	$9223372036854775776, %r13
.L53:
	movq	%r13, %rdi
	movq	%rdx, -88(%rbp)
	movq	%rcx, -80(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	-88(%rbp), %rdx
	movq	%rax, -72(%rbp)
.L71:
	addq	-72(%rbp), %rcx
	movq	(%rdx), %r11
	leaq	16(%rcx), %r10
	movq	%r10, (%rcx)
	testq	%r11, %r11
	je	.L89
	movq	%r11, %rdi
	movq	%r10, -96(%rbp)
	movq	%rcx, -88(%rbp)
	movq	%r11, -80(%rbp)
	call	strlen@PLT
	movq	-80(%rbp), %r11
	movq	-88(%rbp), %rcx
	cmpq	$15, %rax
	movq	%rax, -64(%rbp)
	movq	-96(%rbp), %r10
	movq	%rax, %r9
	ja	.L90
	cmpq	$1, %rax
	jne	.L58
	movzbl	(%r11), %edx
	movb	%dl, 16(%rcx)
.L59:
	movq	%rax, 8(%rcx)
	movb	$0, (%r10,%rax)
	cmpq	%rbx, %r14
	je	.L75
.L94:
	movq	-72(%rbp), %rdx
	movq	%rbx, %rax
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L61:
	movq	%rcx, (%rdx)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%rdx)
.L85:
	movq	8(%rax), %rcx
	addq	$32, %rax
	addq	$32, %rdx
	movq	%rcx, -24(%rdx)
	cmpq	%rax, %r14
	je	.L91
.L64:
	leaq	16(%rdx), %rcx
	leaq	16(%rax), %rsi
	movq	%rcx, (%rdx)
	movq	(%rax), %rcx
	cmpq	%rsi, %rcx
	jne	.L61
	movdqu	16(%rax), %xmm1
	movups	%xmm1, 16(%rdx)
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L88:
	testq	%r13, %r13
	jne	.L54
	movq	$0, -72(%rbp)
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L91:
	movq	%r14, %r9
	subq	%rbx, %r9
	addq	-72(%rbp), %r9
.L60:
	addq	$32, %r9
	cmpq	%r15, %r14
	je	.L65
	movq	%r14, %rax
	movq	%r9, %rdx
	.p2align 4,,10
	.p2align 3
.L69:
	leaq	16(%rdx), %rcx
	leaq	16(%rax), %rsi
	movq	%rcx, (%rdx)
	movq	(%rax), %rcx
	cmpq	%rsi, %rcx
	je	.L92
	movq	%rcx, (%rdx)
	movq	16(%rax), %rcx
	addq	$32, %rax
	addq	$32, %rdx
	movq	%rcx, -16(%rdx)
	movq	-24(%rax), %rcx
	movq	%rcx, -24(%rdx)
	cmpq	%r15, %rax
	jne	.L69
.L67:
	subq	%r14, %r15
	addq	%r15, %r9
.L65:
	testq	%rbx, %rbx
	je	.L70
	movq	%rbx, %rdi
	movq	%r9, -80(%rbp)
	call	_ZdlPv@PLT
	movq	-80(%rbp), %r9
.L70:
	movq	-72(%rbp), %rax
	movq	%r9, %xmm3
	movq	%rax, %xmm0
	addq	%r13, %rax
	movq	%rax, 16(%r12)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, (%r12)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L93
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	.cfi_restore_state
	movdqu	16(%rax), %xmm2
	movq	8(%rax), %rcx
	addq	$32, %rax
	addq	$32, %rdx
	movups	%xmm2, -16(%rdx)
	movq	%rcx, -24(%rdx)
	cmpq	%rax, %r15
	jne	.L69
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L73:
	movl	$32, %r13d
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L58:
	testq	%rax, %rax
	jne	.L57
	movq	%rax, 8(%rcx)
	movb	$0, (%r10,%rax)
	cmpq	%rbx, %r14
	jne	.L94
.L75:
	movq	-72(%rbp), %r9
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L90:
	movq	%rcx, %rdi
	leaq	-64(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r11, -88(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-80(%rbp), %rcx
	movq	-88(%rbp), %r11
	movq	%rax, %r10
	movq	-96(%rbp), %r9
	movq	%rax, (%rcx)
	movq	-64(%rbp), %rax
	movq	%rax, 16(%rcx)
.L57:
	movq	%r10, %rdi
	movq	%r9, %rdx
	movq	%r11, %rsi
	movq	%rcx, -80(%rbp)
	call	memcpy@PLT
	movq	-80(%rbp), %rcx
	movq	-64(%rbp), %rax
	movq	(%rcx), %r10
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L89:
	leaq	.LC2(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L87:
	leaq	.LC3(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L54:
	cmpq	%rsi, %r13
	cmova	%rsi, %r13
	salq	$5, %r13
	jmp	.L53
.L93:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9899:
	.size	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJPcEEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, .-_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJPcEEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.section	.text._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE12emplace_backIJPcEEEvDpOT_,"axG",@progbits,_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE12emplace_backIJPcEEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE12emplace_backIJPcEEEvDpOT_
	.type	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE12emplace_backIJPcEEEvDpOT_, @function
_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE12emplace_backIJPcEEEvDpOT_:
.LFB9569:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$16, %rsp
	movq	8(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	16(%rdi), %r13
	je	.L96
	movq	(%rsi), %r15
	leaq	16(%r13), %rax
	movq	%rax, 0(%r13)
	testq	%r15, %r15
	je	.L108
	movq	%r15, %rdi
	call	strlen@PLT
	movq	%rax, -48(%rbp)
	movq	%rax, %r14
	movq	%rax, %rdx
	cmpq	$15, %rax
	ja	.L109
	movq	0(%r13), %rdi
	cmpq	$1, %rax
	jne	.L100
	movzbl	(%r15), %eax
	movb	%al, (%rdi)
	movq	-48(%rbp), %rdx
	movq	0(%r13), %rdi
.L101:
	movq	%rdx, 8(%r13)
	movb	$0, (%rdi,%rdx)
	addq	$32, 8(%r12)
.L95:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L110
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L109:
	.cfi_restore_state
	movq	%r13, %rdi
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, 0(%r13)
	movq	%rax, %rdi
	movq	-48(%rbp), %rax
	movq	%rax, 16(%r13)
.L99:
	movq	%r14, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %rdx
	movq	0(%r13), %rdi
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L108:
	leaq	.LC2(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L96:
	movq	%rsi, %rdx
	movq	%r13, %rsi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJPcEEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L100:
	testq	%rax, %rax
	je	.L101
	jmp	.L99
.L110:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9569:
	.size	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE12emplace_backIJPcEEEvDpOT_, .-_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE12emplace_backIJPcEEEvDpOT_
	.section	.text._ZNSt6vectorImSaImEE17_M_realloc_insertIJRKmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_,"axG",@progbits,_ZNSt6vectorImSaImEE17_M_realloc_insertIJRKmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorImSaImEE17_M_realloc_insertIJRKmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	.type	_ZNSt6vectorImSaImEE17_M_realloc_insertIJRKmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_, @function
_ZNSt6vectorImSaImEE17_M_realloc_insertIJRKmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_:
.LFB9911:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L125
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L121
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L126
.L113:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L120:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L127
	testq	%r13, %r13
	jg	.L116
	testq	%r9, %r9
	jne	.L119
.L117:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L127:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L116
.L119:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L126:
	testq	%rsi, %rsi
	jne	.L114
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L116:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L117
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L121:
	movl	$8, %r14d
	jmp	.L113
.L125:
	leaq	.LC3(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L114:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L113
	.cfi_endproc
.LFE9911:
	.size	_ZNSt6vectorImSaImEE17_M_realloc_insertIJRKmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_, .-_ZNSt6vectorImSaImEE17_M_realloc_insertIJRKmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	.section	.text._ZN2v88internal4wasm19WasmModuleSourceMap13DecodeMappingERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm19WasmModuleSourceMap13DecodeMappingERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN2v88internal4wasm19WasmModuleSourceMap13DecodeMappingERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN2v88internal4wasm19WasmModuleSourceMap13DecodeMappingERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB8841:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -40
	movq	8(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	movq	$0, -56(%rbp)
	movq	$0, -48(%rbp)
	testq	%rsi, %rsi
	je	.L130
	movq	%rdi, %rbx
	xorl	%eax, %eax
	leaq	-72(%rbp), %r13
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L131:
	movq	%r13, %rdx
	call	_ZN2v84base15VLQBase64DecodeEPKcmPm@PLT
	cmpl	$-2147483648, %eax
	je	.L135
	movq	8(%r12), %rsi
	movq	(%r12), %rdi
	movq	%r13, %rdx
	cltq
	addq	%rax, -64(%rbp)
	call	_ZN2v84base15VLQBase64DecodeEPKcmPm@PLT
	cmpl	$-2147483648, %eax
	je	.L135
	movq	8(%r12), %rsi
	movq	(%r12), %rdi
	movq	%r13, %rdx
	cltq
	addq	%rax, -56(%rbp)
	call	_ZN2v84base15VLQBase64DecodeEPKcmPm@PLT
	cmpl	$-2147483648, %eax
	je	.L135
	movq	8(%r12), %rsi
	movq	(%r12), %rdi
	movq	%r13, %rdx
	cltq
	addq	%rax, -48(%rbp)
	call	_ZN2v84base15VLQBase64DecodeEPKcmPm@PLT
	cmpl	$-2147483648, %eax
	je	.L135
	movq	-72(%rbp), %rax
	cmpq	8(%r12), %rax
	jnb	.L136
	movq	(%r12), %rdx
	cmpb	$44, (%rdx,%rax)
	jne	.L135
.L136:
	addq	$1, %rax
	movq	56(%rbx), %rsi
	movq	%rax, -72(%rbp)
	cmpq	64(%rbx), %rsi
	je	.L137
	movq	-56(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 56(%rbx)
.L138:
	movq	80(%rbx), %rsi
	cmpq	88(%rbx), %rsi
	je	.L139
	movq	-48(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 80(%rbx)
.L140:
	movq	8(%rbx), %rsi
	cmpq	16(%rbx), %rsi
	je	.L141
	movq	-64(%rbp), %rax
	movq	%rax, (%rsi)
	movq	-72(%rbp), %rax
	addq	$8, 8(%rbx)
	movq	8(%r12), %rsi
.L132:
	cmpq	%rsi, %rax
	jnb	.L130
.L129:
	movq	(%r12), %rdi
	cmpb	$44, (%rdi,%rax)
	jne	.L131
	addq	$1, %rax
	movq	%rax, -72(%rbp)
	cmpq	%rsi, %rax
	jb	.L129
.L130:
	movl	$1, %eax
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L141:
	leaq	-64(%rbp), %rdx
	movq	%rbx, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJRKmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	movq	-72(%rbp), %rax
	movq	8(%r12), %rsi
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L139:
	leaq	-48(%rbp), %rdx
	leaq	72(%rbx), %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJRKmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L137:
	leaq	-56(%rbp), %rdx
	leaq	48(%rbx), %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJRKmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L135:
	xorl	%eax, %eax
.L128:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L154
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L154:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8841:
	.size	_ZN2v88internal4wasm19WasmModuleSourceMap13DecodeMappingERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN2v88internal4wasm19WasmModuleSourceMap13DecodeMappingERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.rodata._ZN2v88internal4wasm19WasmModuleSourceMapC2EPNS_7IsolateENS_5LocalINS_6StringEEE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"version"
	.section	.text._ZN2v88internal4wasm19WasmModuleSourceMapC2EPNS_7IsolateENS_5LocalINS_6StringEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm19WasmModuleSourceMapC2EPNS_7IsolateENS_5LocalINS_6StringEEE
	.type	_ZN2v88internal4wasm19WasmModuleSourceMapC2EPNS_7IsolateENS_5LocalINS_6StringEEE, @function
_ZN2v88internal4wasm19WasmModuleSourceMapC2EPNS_7IsolateENS_5LocalINS_6StringEEE:
.LFB8835:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-128(%rbp), %r15
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movb	$0, 96(%rdi)
	movups	%xmm0, (%rdi)
	movups	%xmm0, 16(%rdi)
	movups	%xmm0, 32(%rdi)
	movups	%xmm0, 48(%rdi)
	movups	%xmm0, 64(%rdi)
	movups	%xmm0, 80(%rdi)
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	$0
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v87Context3NewEPNS_7IsolateEPNS_22ExtensionConfigurationENS_10MaybeLocalINS_14ObjectTemplateEEENS5_INS_5ValueEEENS_33DeserializeInternalFieldsCallbackEPNS_14MicrotaskQueueE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN2v84JSON5ParseENS_5LocalINS_7ContextEEENS1_INS_6StringEEE@PLT
	popq	%rdx
	popq	%rcx
	testq	%rax, %rax
	je	.L164
	movl	$1, %edx
	movl	$-1, %ecx
	movq	%r12, %rdi
	movq	%rax, %r14
	leaq	.LC4(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L225
.L192:
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L164
	movq	%rax, -152(%rbp)
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L164
	movq	-152(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNK2v85Value11Uint32ValueENS_5LocalINS_7ContextEEE@PLT
	testb	%al, %al
	je	.L164
	shrq	$32, %rax
	cmpl	$3, %eax
	je	.L226
	.p2align 4,,10
	.p2align 3
.L164:
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L155:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L227
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L226:
	.cfi_restore_state
	movl	$1, %edx
	movl	$-1, %ecx
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L228
.L163:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, -152(%rbp)
	testq	%rax, %rax
	je	.L164
	movq	%rax, %rdi
	call	_ZNK2v85Value7IsArrayEv@PLT
	testb	%al, %al
	je	.L164
	movl	$1, %edx
	movl	$-1, %ecx
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L229
.L166:
	movq	-152(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L164
	movq	%r13, %rsi
	call	_ZNK2v85Value11Uint32ValueENS_5LocalINS_7ContextEEE@PLT
	testb	%al, %al
	je	.L164
	leaq	24(%rbx), %rcx
	shrq	$32, %rax
	xorl	%r10d, %r10d
	movq	%rcx, -160(%rbp)
	leaq	-136(%rbp), %rcx
	movl	%eax, -164(%rbp)
	movq	%rcx, -176(%rbp)
	testl	%eax, %eax
	je	.L173
	movq	%rbx, -200(%rbp)
	movq	%r14, -192(%rbp)
	movl	%r10d, %r14d
.L174:
	movq	-152(%rbp), %rdi
	movl	%r14d, %edx
	movq	%r13, %rsi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %r11
	testq	%rax, %rax
	je	.L164
	movq	(%rax), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L164
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L164
	movq	%r11, %rdi
	movq	%r12, %rsi
	movq	%r11, -184(%rbp)
	addl	$1, %r14d
	call	_ZNK2v86String10Utf8LengthEPNS_7IsolateE@PLT
	leal	1(%rax), %edi
	movl	%eax, -168(%rbp)
	movslq	%edi, %rdi
	call	_Znam@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$-1, %ecx
	movq	-184(%rbp), %r11
	movq	%rax, %rdx
	movq	%r12, %rsi
	movq	%rax, %rbx
	movq	%r11, %rdi
	call	_ZNK2v86String9WriteUtf8EPNS_7IsolateEPciPii@PLT
	movslq	-168(%rbp), %rax
	movq	-176(%rbp), %rsi
	movq	%rbx, -136(%rbp)
	movq	-160(%rbp), %rdi
	movb	$0, (%rbx,%rax)
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE12emplace_backIJPcEEEvDpOT_
	movq	%rbx, %rdi
	call	_ZdaPv@PLT
	cmpl	-164(%rbp), %r14d
	jne	.L174
	movq	-192(%rbp), %r14
	movq	-200(%rbp), %rbx
.L173:
	movl	$1, %edx
	orl	$-1, %ecx
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L230
.L171:
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L164
	movq	(%rax), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	subq	$1, %rdx
	jne	.L164
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L164
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZNK2v86String10Utf8LengthEPNS_7IsolateE@PLT
	leal	1(%rax), %edi
	movl	%eax, -152(%rbp)
	movslq	%edi, %rdi
	call	_Znam@PLT
	movq	%r14, %rdi
	orl	$-1, %ecx
	movq	%r12, %rsi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%rax, %rdx
	movq	%rax, %r13
	call	_ZNK2v86String9WriteUtf8EPNS_7IsolateEPciPii@PLT
	leaq	-80(%rbp), %rax
	orq	$-1, %rcx
	movq	%r13, %rdi
	movslq	-152(%rbp), %r10
	movq	%rax, -96(%rbp)
	leaq	-96(%rbp), %r14
	movq	%rax, -152(%rbp)
	xorl	%eax, %eax
	movb	$0, 0(%r13,%r10)
	repnz scasb
	movq	%rcx, %rax
	notq	%rax
	leaq	-1(%rax), %r12
	movq	%r12, -136(%rbp)
	cmpq	$15, %r12
	ja	.L231
	cmpq	$1, %r12
	jne	.L180
	movzbl	0(%r13), %eax
	movb	%al, -80(%rbp)
.L181:
	movq	-136(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal4wasm19WasmModuleSourceMap13DecodeMappingERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-96(%rbp), %rdi
	movb	%al, 96(%rbx)
	cmpq	-152(%rbp), %rdi
	je	.L182
	call	_ZdlPv@PLT
.L182:
	movq	%r13, %rdi
	call	_ZdaPv@PLT
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L225:
	movq	%rdx, -152(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-152(%rbp), %rdx
	jmp	.L192
.L228:
	movq	%rax, -152(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-152(%rbp), %rdx
	jmp	.L163
.L229:
	movq	%rax, -160(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-160(%rbp), %rdx
	jmp	.L166
.L227:
	call	__stack_chk_fail@PLT
.L180:
	testq	%r12, %r12
	je	.L181
	movq	-152(%rbp), %rdi
.L179:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	jmp	.L181
.L231:
	movq	%r14, %rdi
	leaq	-136(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-136(%rbp), %rax
	movq	%rax, -80(%rbp)
	jmp	.L179
.L230:
	movq	%rax, -152(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-152(%rbp), %rdx
	jmp	.L171
	.cfi_endproc
.LFE8835:
	.size	_ZN2v88internal4wasm19WasmModuleSourceMapC2EPNS_7IsolateENS_5LocalINS_6StringEEE, .-_ZN2v88internal4wasm19WasmModuleSourceMapC2EPNS_7IsolateENS_5LocalINS_6StringEEE
	.globl	_ZN2v88internal4wasm19WasmModuleSourceMapC1EPNS_7IsolateENS_5LocalINS_6StringEEE
	.set	_ZN2v88internal4wasm19WasmModuleSourceMapC1EPNS_7IsolateENS_5LocalINS_6StringEEE,_ZN2v88internal4wasm19WasmModuleSourceMapC2EPNS_7IsolateENS_5LocalINS_6StringEEE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal4wasm19WasmModuleSourceMapC2EPNS_7IsolateENS_5LocalINS_6StringEEE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal4wasm19WasmModuleSourceMapC2EPNS_7IsolateENS_5LocalINS_6StringEEE, @function
_GLOBAL__sub_I__ZN2v88internal4wasm19WasmModuleSourceMapC2EPNS_7IsolateENS_5LocalINS_6StringEEE:
.LFB10447:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE10447:
	.size	_GLOBAL__sub_I__ZN2v88internal4wasm19WasmModuleSourceMapC2EPNS_7IsolateENS_5LocalINS_6StringEEE, .-_GLOBAL__sub_I__ZN2v88internal4wasm19WasmModuleSourceMapC2EPNS_7IsolateENS_5LocalINS_6StringEEE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal4wasm19WasmModuleSourceMapC2EPNS_7IsolateENS_5LocalINS_6StringEEE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
