	.file	"elements-kind.cc"
	.text
	.section	.rodata._ZN2v88internal23ElementsKindToShiftSizeENS0_12ElementsKindE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal23ElementsKindToShiftSizeENS0_12ElementsKindE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal23ElementsKindToShiftSizeENS0_12ElementsKindE
	.type	_ZN2v88internal23ElementsKindToShiftSizeENS0_12ElementsKindE, @function
_ZN2v88internal23ElementsKindToShiftSizeENS0_12ElementsKindE:
.LFB17782:
	.cfi_startproc
	endbr64
	cmpb	$27, %dil
	ja	.L2
	leaq	.L4(%rip), %rcx
	movzbl	%dil, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal23ElementsKindToShiftSizeENS0_12ElementsKindE,"a",@progbits
	.align 4
	.align 4
.L4:
	.long	.L3-.L4
	.long	.L3-.L4
	.long	.L3-.L4
	.long	.L3-.L4
	.long	.L3-.L4
	.long	.L3-.L4
	.long	.L3-.L4
	.long	.L3-.L4
	.long	.L3-.L4
	.long	.L3-.L4
	.long	.L3-.L4
	.long	.L3-.L4
	.long	.L3-.L4
	.long	.L3-.L4
	.long	.L3-.L4
	.long	.L3-.L4
	.long	.L3-.L4
	.long	.L5-.L4
	.long	.L5-.L4
	.long	.L7-.L4
	.long	.L7-.L4
	.long	.L8-.L4
	.long	.L8-.L4
	.long	.L8-.L4
	.long	.L3-.L4
	.long	.L5-.L4
	.long	.L3-.L4
	.long	.L3-.L4
	.section	.text._ZN2v88internal23ElementsKindToShiftSizeENS0_12ElementsKindE
	.p2align 4,,10
	.p2align 3
.L3:
	movl	$3, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	movl	$2, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movl	$1, %eax
	ret
.L2:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE17782:
	.size	_ZN2v88internal23ElementsKindToShiftSizeENS0_12ElementsKindE, .-_ZN2v88internal23ElementsKindToShiftSizeENS0_12ElementsKindE
	.section	.text._ZN2v88internal22ElementsKindToByteSizeENS0_12ElementsKindE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal22ElementsKindToByteSizeENS0_12ElementsKindE
	.type	_ZN2v88internal22ElementsKindToByteSizeENS0_12ElementsKindE, @function
_ZN2v88internal22ElementsKindToByteSizeENS0_12ElementsKindE:
.LFB17783:
	.cfi_startproc
	endbr64
	cmpb	$27, %dil
	ja	.L13
	leaq	.L15(%rip), %rcx
	movzbl	%dil, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal22ElementsKindToByteSizeENS0_12ElementsKindE,"a",@progbits
	.align 4
	.align 4
.L15:
	.long	.L14-.L15
	.long	.L14-.L15
	.long	.L14-.L15
	.long	.L14-.L15
	.long	.L14-.L15
	.long	.L14-.L15
	.long	.L14-.L15
	.long	.L14-.L15
	.long	.L14-.L15
	.long	.L14-.L15
	.long	.L14-.L15
	.long	.L14-.L15
	.long	.L14-.L15
	.long	.L14-.L15
	.long	.L14-.L15
	.long	.L14-.L15
	.long	.L14-.L15
	.long	.L16-.L15
	.long	.L16-.L15
	.long	.L18-.L15
	.long	.L18-.L15
	.long	.L19-.L15
	.long	.L19-.L15
	.long	.L19-.L15
	.long	.L14-.L15
	.long	.L16-.L15
	.long	.L14-.L15
	.long	.L14-.L15
	.section	.text._ZN2v88internal22ElementsKindToByteSizeENS0_12ElementsKindE
	.p2align 4,,10
	.p2align 3
.L14:
	movl	$8, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	movl	$4, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	movl	$2, %eax
	ret
.L13:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE17783:
	.size	_ZN2v88internal22ElementsKindToByteSizeENS0_12ElementsKindE, .-_ZN2v88internal22ElementsKindToByteSizeENS0_12ElementsKindE
	.section	.text._ZN2v88internal35GetDefaultHeaderSizeForElementsKindENS0_12ElementsKindE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal35GetDefaultHeaderSizeForElementsKindENS0_12ElementsKindE
	.type	_ZN2v88internal35GetDefaultHeaderSizeForElementsKindENS0_12ElementsKindE, @function
_ZN2v88internal35GetDefaultHeaderSizeForElementsKindENS0_12ElementsKindE:
.LFB17784:
	.cfi_startproc
	endbr64
	subl	$17, %edi
	cmpb	$11, %dil
	sbbl	%eax, %eax
	notl	%eax
	andl	$15, %eax
	ret
	.cfi_endproc
.LFE17784:
	.size	_ZN2v88internal35GetDefaultHeaderSizeForElementsKindENS0_12ElementsKindE, .-_ZN2v88internal35GetDefaultHeaderSizeForElementsKindENS0_12ElementsKindE
	.section	.rodata._ZN2v88internal20ElementsKindToStringENS0_12ElementsKindE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"NO_ELEMENTS"
.LC2:
	.string	"PACKED_SMI_ELEMENTS"
.LC3:
	.string	"PACKED_ELEMENTS"
.LC4:
	.string	"HOLEY_ELEMENTS"
.LC5:
	.string	"PACKED_DOUBLE_ELEMENTS"
.LC6:
	.string	"HOLEY_DOUBLE_ELEMENTS"
.LC7:
	.string	"PACKED_NONEXTENSIBLE_ELEMENTS"
.LC8:
	.string	"HOLEY_NONEXTENSIBLE_ELEMENTS"
.LC9:
	.string	"PACKED_SEALED_ELEMENTS"
.LC10:
	.string	"HOLEY_SEALED_ELEMENTS"
.LC11:
	.string	"PACKED_FROZEN_ELEMENTS"
.LC12:
	.string	"HOLEY_FROZEN_ELEMENTS"
.LC13:
	.string	"DICTIONARY_ELEMENTS"
	.section	.rodata._ZN2v88internal20ElementsKindToStringENS0_12ElementsKindE.str1.8,"aMS",@progbits,1
	.align 8
.LC14:
	.string	"FAST_SLOPPY_ARGUMENTS_ELEMENTS"
	.align 8
.LC15:
	.string	"SLOW_SLOPPY_ARGUMENTS_ELEMENTS"
	.section	.rodata._ZN2v88internal20ElementsKindToStringENS0_12ElementsKindE.str1.1
.LC16:
	.string	"FAST_STRING_WRAPPER_ELEMENTS"
.LC17:
	.string	"SLOW_STRING_WRAPPER_ELEMENTS"
.LC18:
	.string	"UINT8ELEMENTS"
.LC19:
	.string	"INT8ELEMENTS"
.LC20:
	.string	"UINT16ELEMENTS"
.LC21:
	.string	"INT16ELEMENTS"
.LC22:
	.string	"UINT32ELEMENTS"
.LC23:
	.string	"INT32ELEMENTS"
.LC24:
	.string	"FLOAT32ELEMENTS"
.LC25:
	.string	"FLOAT64ELEMENTS"
.LC26:
	.string	"UINT8_CLAMPEDELEMENTS"
.LC27:
	.string	"BIGUINT64ELEMENTS"
.LC28:
	.string	"BIGINT64ELEMENTS"
.LC29:
	.string	"HOLEY_SMI_ELEMENTS"
	.section	.text._ZN2v88internal20ElementsKindToStringENS0_12ElementsKindE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal20ElementsKindToStringENS0_12ElementsKindE
	.type	_ZN2v88internal20ElementsKindToStringENS0_12ElementsKindE, @function
_ZN2v88internal20ElementsKindToStringENS0_12ElementsKindE:
.LFB17785:
	.cfi_startproc
	endbr64
	leaq	.L28(%rip), %rdx
	movzbl	%dil, %edi
	movslq	(%rdx,%rdi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal20ElementsKindToStringENS0_12ElementsKindE,"a",@progbits
	.align 4
	.align 4
.L28:
	.long	.L56-.L28
	.long	.L55-.L28
	.long	.L57-.L28
	.long	.L53-.L28
	.long	.L52-.L28
	.long	.L51-.L28
	.long	.L50-.L28
	.long	.L49-.L28
	.long	.L48-.L28
	.long	.L47-.L28
	.long	.L46-.L28
	.long	.L45-.L28
	.long	.L44-.L28
	.long	.L43-.L28
	.long	.L42-.L28
	.long	.L41-.L28
	.long	.L40-.L28
	.long	.L39-.L28
	.long	.L38-.L28
	.long	.L37-.L28
	.long	.L36-.L28
	.long	.L35-.L28
	.long	.L34-.L28
	.long	.L33-.L28
	.long	.L32-.L28
	.long	.L31-.L28
	.long	.L30-.L28
	.long	.L29-.L28
	.long	.L27-.L28
	.section	.text._ZN2v88internal20ElementsKindToStringENS0_12ElementsKindE
	.p2align 4,,10
	.p2align 3
.L29:
	leaq	.LC28(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	leaq	.LC27(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	leaq	.LC26(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	leaq	.LC25(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	leaq	.LC24(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	leaq	.LC23(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	leaq	.LC22(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	leaq	.LC21(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	leaq	.LC20(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	leaq	.LC19(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	leaq	.LC18(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	leaq	.LC17(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	leaq	.LC16(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	leaq	.LC15(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	leaq	.LC14(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	leaq	.LC13(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	leaq	.LC12(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	leaq	.LC11(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	leaq	.LC10(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	leaq	.LC9(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	leaq	.LC8(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	leaq	.LC7(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	leaq	.LC6(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	leaq	.LC5(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	leaq	.LC4(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	leaq	.LC3(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	leaq	.LC29(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	leaq	.LC2(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	leaq	.LC1(%rip), %rax
	ret
	.cfi_endproc
.LFE17785:
	.size	_ZN2v88internal20ElementsKindToStringENS0_12ElementsKindE, .-_ZN2v88internal20ElementsKindToStringENS0_12ElementsKindE
	.section	.text._ZN2v88internal36GetFastElementsKindFromSequenceIndexEi,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal36GetFastElementsKindFromSequenceIndexEi
	.type	_ZN2v88internal36GetFastElementsKindFromSequenceIndexEi, @function
_ZN2v88internal36GetFastElementsKindFromSequenceIndexEi:
.LFB17786:
	.cfi_startproc
	endbr64
	movslq	%edi, %rdi
	leaq	_ZN2v88internal25kFastElementsKindSequenceE(%rip), %rax
	movzbl	(%rax,%rdi), %eax
	ret
	.cfi_endproc
.LFE17786:
	.size	_ZN2v88internal36GetFastElementsKindFromSequenceIndexEi, .-_ZN2v88internal36GetFastElementsKindFromSequenceIndexEi
	.section	.text._ZN2v88internal36GetSequenceIndexFromFastElementsKindENS0_12ElementsKindE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal36GetSequenceIndexFromFastElementsKindENS0_12ElementsKindE
	.type	_ZN2v88internal36GetSequenceIndexFromFastElementsKindENS0_12ElementsKindE, @function
_ZN2v88internal36GetSequenceIndexFromFastElementsKindENS0_12ElementsKindE:
.LFB17787:
	.cfi_startproc
	endbr64
	cmpb	_ZN2v88internal25kFastElementsKindSequenceE(%rip), %dil
	je	.L62
	cmpb	1+_ZN2v88internal25kFastElementsKindSequenceE(%rip), %dil
	je	.L63
	cmpb	2+_ZN2v88internal25kFastElementsKindSequenceE(%rip), %dil
	je	.L64
	cmpb	3+_ZN2v88internal25kFastElementsKindSequenceE(%rip), %dil
	je	.L65
	cmpb	4+_ZN2v88internal25kFastElementsKindSequenceE(%rip), %dil
	je	.L66
	cmpb	%dil, 5+_ZN2v88internal25kFastElementsKindSequenceE(%rip)
	je	.L69
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	xorl	%eax, %eax
	ret
.L63:
	movl	$1, %eax
	ret
.L64:
	movl	$2, %eax
	ret
.L65:
	movl	$3, %eax
	ret
.L66:
	movl	$4, %eax
	ret
.L69:
	movl	$5, %eax
	ret
	.cfi_endproc
.LFE17787:
	.size	_ZN2v88internal36GetSequenceIndexFromFastElementsKindENS0_12ElementsKindE, .-_ZN2v88internal36GetSequenceIndexFromFastElementsKindENS0_12ElementsKindE
	.section	.text._ZN2v88internal29GetNextTransitionElementsKindENS0_12ElementsKindE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal29GetNextTransitionElementsKindENS0_12ElementsKindE
	.type	_ZN2v88internal29GetNextTransitionElementsKindENS0_12ElementsKindE, @function
_ZN2v88internal29GetNextTransitionElementsKindENS0_12ElementsKindE:
.LFB17788:
	.cfi_startproc
	endbr64
	cmpb	_ZN2v88internal25kFastElementsKindSequenceE(%rip), %dil
	je	.L73
	cmpb	1+_ZN2v88internal25kFastElementsKindSequenceE(%rip), %dil
	je	.L74
	cmpb	2+_ZN2v88internal25kFastElementsKindSequenceE(%rip), %dil
	je	.L75
	cmpb	3+_ZN2v88internal25kFastElementsKindSequenceE(%rip), %dil
	je	.L76
	cmpb	4+_ZN2v88internal25kFastElementsKindSequenceE(%rip), %dil
	je	.L77
	cmpb	5+_ZN2v88internal25kFastElementsKindSequenceE(%rip), %dil
	movl	$6, %eax
	je	.L71
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
.L77:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movl	$5, %eax
.L71:
	leaq	_ZN2v88internal25kFastElementsKindSequenceE(%rip), %rdx
	movzbl	(%rdx,%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	movl	$1, %eax
	jmp	.L71
.L74:
	movl	$2, %eax
	jmp	.L71
.L75:
	movl	$3, %eax
	jmp	.L71
.L76:
	movl	$4, %eax
	jmp	.L71
	.cfi_endproc
.LFE17788:
	.size	_ZN2v88internal29GetNextTransitionElementsKindENS0_12ElementsKindE, .-_ZN2v88internal29GetNextTransitionElementsKindENS0_12ElementsKindE
	.section	.text._ZN2v88internal35IsMoreGeneralElementsKindTransitionENS0_12ElementsKindES1_,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal35IsMoreGeneralElementsKindTransitionENS0_12ElementsKindES1_
	.type	_ZN2v88internal35IsMoreGeneralElementsKindTransitionENS0_12ElementsKindES1_, @function
_ZN2v88internal35IsMoreGeneralElementsKindTransitionENS0_12ElementsKindES1_:
.LFB17790:
	.cfi_startproc
	endbr64
	movzbl	%dil, %edx
	xorl	%eax, %eax
	cmpb	$5, %dil
	ja	.L81
	cmpb	$5, %sil
	setbe	%al
	cmpb	$12, %sil
	sete	%cl
	orb	%cl, %al
	je	.L81
	leaq	.L84(%rip), %rcx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal35IsMoreGeneralElementsKindTransitionENS0_12ElementsKindES1_,"a",@progbits
	.align 4
	.align 4
.L84:
	.long	.L88-.L84
	.long	.L87-.L84
	.long	.L86-.L84
	.long	.L90-.L84
	.long	.L85-.L84
	.long	.L83-.L84
	.section	.text._ZN2v88internal35IsMoreGeneralElementsKindTransitionENS0_12ElementsKindES1_
.L90:
	xorl	%eax, %eax
.L81:
	ret
.L83:
	subl	$2, %esi
	cmpb	$1, %sil
	setbe	%al
	ret
.L88:
	testb	%sil, %sil
	setne	%al
	ret
.L87:
	cmpb	$1, %sil
	seta	%al
	ret
.L86:
	cmpb	$3, %sil
	sete	%al
	ret
.L85:
	cmpb	$1, %sil
	seta	%al
	cmpb	$4, %sil
	setne	%dl
	andl	%edx, %eax
	ret
	.cfi_endproc
.LFE17790:
	.size	_ZN2v88internal35IsMoreGeneralElementsKindTransitionENS0_12ElementsKindES1_, .-_ZN2v88internal35IsMoreGeneralElementsKindTransitionENS0_12ElementsKindES1_
	.section	.text.unlikely._ZN2v88internal25UnionElementsKindUptoSizeEPNS0_12ElementsKindES1_,"ax",@progbits
.LCOLDB30:
	.section	.text._ZN2v88internal25UnionElementsKindUptoSizeEPNS0_12ElementsKindES1_,"ax",@progbits
.LHOTB30:
	.p2align 4
	.globl	_ZN2v88internal25UnionElementsKindUptoSizeEPNS0_12ElementsKindES1_
	.type	_ZN2v88internal25UnionElementsKindUptoSizeEPNS0_12ElementsKindES1_, @function
_ZN2v88internal25UnionElementsKindUptoSizeEPNS0_12ElementsKindES1_:
.LFB17791:
	.cfi_startproc
	endbr64
	cmpb	$5, (%rdi)
	ja	.L106
	movzbl	(%rdi), %eax
	leaq	.L97(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal25UnionElementsKindUptoSizeEPNS0_12ElementsKindES1_,"a",@progbits
	.align 4
	.align 4
.L97:
	.long	.L102-.L97
	.long	.L101-.L97
	.long	.L100-.L97
	.long	.L99-.L97
	.long	.L98-.L97
	.long	.L96-.L97
	.section	.text._ZN2v88internal25UnionElementsKindUptoSizeEPNS0_12ElementsKindES1_
	.p2align 4,,10
	.p2align 3
.L98:
	movzbl	%sil, %eax
	subl	$4, %eax
	cmpl	$1, %eax
	jbe	.L105
.L112:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L96:
	movzbl	%sil, %esi
	subl	$4, %esi
	cmpl	$1, %esi
	setbe	%al
	ret
	.p2align 4,,10
	.p2align 3
.L102:
	xorl	%eax, %eax
	cmpb	$3, %sil
	ja	.L94
.L105:
	movb	%sil, (%rdi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	cmpb	$2, %sil
	je	.L110
	ja	.L103
	movl	$1, %eax
	testb	%sil, %sil
	je	.L113
.L104:
	movb	$3, (%rdi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L99:
	cmpb	$3, %sil
	setbe	%al
	ret
	.p2align 4,,10
	.p2align 3
.L101:
	movl	$1, %eax
	cmpb	$1, %sil
	jbe	.L94
	subl	$2, %esi
	xorl	%eax, %eax
	cmpb	$1, %sil
	jbe	.L104
.L94:
	ret
	.p2align 4,,10
	.p2align 3
.L103:
	cmpb	$3, %sil
	je	.L104
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L113:
	ret
	.p2align 4,,10
	.p2align 3
.L110:
	movl	$1, %eax
	ret
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal25UnionElementsKindUptoSizeEPNS0_12ElementsKindES1_
	.cfi_startproc
	.type	_ZN2v88internal25UnionElementsKindUptoSizeEPNS0_12ElementsKindES1_.cold, @function
_ZN2v88internal25UnionElementsKindUptoSizeEPNS0_12ElementsKindES1_.cold:
.LFSB17791:
.L106:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE17791:
	.section	.text._ZN2v88internal25UnionElementsKindUptoSizeEPNS0_12ElementsKindES1_
	.size	_ZN2v88internal25UnionElementsKindUptoSizeEPNS0_12ElementsKindES1_, .-_ZN2v88internal25UnionElementsKindUptoSizeEPNS0_12ElementsKindES1_
	.section	.text.unlikely._ZN2v88internal25UnionElementsKindUptoSizeEPNS0_12ElementsKindES1_
	.size	_ZN2v88internal25UnionElementsKindUptoSizeEPNS0_12ElementsKindES1_.cold, .-_ZN2v88internal25UnionElementsKindUptoSizeEPNS0_12ElementsKindES1_.cold
.LCOLDE30:
	.section	.text._ZN2v88internal25UnionElementsKindUptoSizeEPNS0_12ElementsKindES1_
.LHOTE30:
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal23ElementsKindToShiftSizeENS0_12ElementsKindE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal23ElementsKindToShiftSizeENS0_12ElementsKindE, @function
_GLOBAL__sub_I__ZN2v88internal23ElementsKindToShiftSizeENS0_12ElementsKindE:
.LFB21448:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE21448:
	.size	_GLOBAL__sub_I__ZN2v88internal23ElementsKindToShiftSizeENS0_12ElementsKindE, .-_GLOBAL__sub_I__ZN2v88internal23ElementsKindToShiftSizeENS0_12ElementsKindE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal23ElementsKindToShiftSizeENS0_12ElementsKindE
	.globl	_ZN2v88internal25kFastElementsKindSequenceE
	.section	.data._ZN2v88internal25kFastElementsKindSequenceE,"aw"
	.type	_ZN2v88internal25kFastElementsKindSequenceE, @object
	.size	_ZN2v88internal25kFastElementsKindSequenceE, 6
_ZN2v88internal25kFastElementsKindSequenceE:
	.byte	0
	.byte	1
	.byte	4
	.byte	5
	.byte	2
	.byte	3
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
