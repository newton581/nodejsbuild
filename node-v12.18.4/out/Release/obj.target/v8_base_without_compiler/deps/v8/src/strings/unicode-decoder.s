	.file	"unicode-decoder.cc"
	.text
	.section	.text._ZN2v88internal11Utf8DecoderC2ERKNS0_6VectorIKhEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11Utf8DecoderC2ERKNS0_6VectorIKhEE
	.type	_ZN2v88internal11Utf8DecoderC2ERKNS0_6VectorIKhEE, @function
_ZN2v88internal11Utf8DecoderC2ERKNS0_6VectorIKhEE:
.LFB5030:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movb	$0, (%rdi)
	movq	8(%rsi), %rdi
	movq	(%rsi), %rax
	movslq	%edi, %rdx
	leaq	(%rax,%rdx), %r8
	cmpq	$7, %rdx
	jbe	.L27
	movq	%rax, %rcx
	testb	$7, %al
	jne	.L6
	.p2align 4,,10
	.p2align 3
.L3:
	movabsq	$-9187201950435737472, %rsi
	.p2align 4,,10
	.p2align 3
.L10:
	movq	%rcx, %rdx
	addq	$8, %rcx
	cmpq	%rcx, %r8
	jb	.L2
	testq	%rsi, -8(%rcx)
	je	.L10
	.p2align 4,,10
	.p2align 3
.L8:
	subl	%eax, %edx
	movl	%edx, 4(%rbx)
	movl	%edx, 8(%rbx)
	cmpl	%edx, %edi
	je	.L1
.L38:
	movslq	%edx, %rdx
	movb	$12, -57(%rbp)
	xorl	%esi, %esi
	movl	$1, %r12d
	addq	%rdx, %rax
	leaq	_ZZN14Utf8DfaDecoderL6DecodeEhPNS_5StateEPjE11transitions(%rip), %r13
	leaq	_ZZN14Utf8DfaDecoderL6DecodeEhPNS_5StateEPjE6states(%rip), %r11
	movl	$127, %r10d
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L37:
	addl	$1, 8(%rbx)
.L14:
	cmpq	%rax, %r8
	jbe	.L15
	movzbl	-57(%rbp), %edi
	.p2align 4,,10
	.p2align 3
.L16:
	movzbl	(%rax), %ecx
	movq	%rax, %r9
	addq	$1, %rax
	movl	%ecx, %edx
	notl	%edx
	shrb	$7, %dl
	cmpb	$12, %dil
	sete	%r14b
	testb	%r14b, %dl
	jne	.L37
	movzbl	%cl, %edx
	movzbl	0(%r13,%rcx), %ecx
	movzbl	%dil, %r14d
	movl	%r10d, %r15d
	sall	$6, %esi
	addl	%ecx, %r14d
	sarl	%ecx
	movslq	%r14d, %r14
	sarl	%cl, %r15d
	movzbl	(%r11,%r14), %r14d
	andl	%r15d, %edx
	orl	%edx, %esi
	movb	%r14b, -57(%rbp)
	testb	%r14b, %r14b
	je	.L19
	cmpb	$12, %r14b
	je	.L20
	cmpq	%rax, %r8
	je	.L15
	.p2align 4,,10
	.p2align 3
.L42:
	movl	%r14d, %edi
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L4:
	addq	$1, %rcx
	testb	$7, %cl
	je	.L3
.L6:
	cmpb	$0, (%rcx)
	jns	.L4
	movl	%ecx, %edx
	subl	%eax, %edx
	movl	%edx, 4(%rbx)
	movl	%edx, 8(%rbx)
	cmpl	%edx, %edi
	jne	.L38
	.p2align 4,,10
	.p2align 3
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L39
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	.cfi_restore_state
	movq	%rax, %rdx
.L2:
	cmpq	%rdx, %r8
	ja	.L9
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L11:
	addq	$1, %rdx
	cmpq	%rdx, %r8
	je	.L40
.L9:
	cmpb	$0, (%rdx)
	jns	.L11
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L20:
	cmpl	$-4, %esi
	jne	.L41
	xorl	%esi, %esi
	cmpq	%rax, %r8
	jne	.L42
.L15:
	leaq	-57(%rbp), %rdi
	call	_ZN7unibrow4Utf824ValueOfIncrementalFinishEPN14Utf8DfaDecoder5StateE@PLT
	testl	%eax, %eax
	jne	.L43
	xorl	$1, %r12d
	addl	$1, %r12d
.L25:
	movb	%r12b, (%rbx)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L43:
	addl	$1, 8(%rbx)
	movl	$2, %r12d
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L19:
	cmpb	$12, %dil
	movb	$12, -57(%rbp)
	cmove	%rax, %r9
	addl	$1, 8(%rbx)
	xorl	%r12d, %r12d
	xorl	%esi, %esi
	movq	%r9, %rax
	jmp	.L14
.L41:
	cmpl	$255, %esi
	setbe	%dl
	andl	%edx, %r12d
	movl	8(%rbx), %edx
	cmpl	$65535, %esi
	jbe	.L23
	addl	$2, %edx
	xorl	%esi, %esi
	movl	%edx, 8(%rbx)
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L40:
	movq	%r8, %rdx
	jmp	.L8
.L23:
	addl	$1, %edx
	xorl	%esi, %esi
	movl	%edx, 8(%rbx)
	jmp	.L14
.L39:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5030:
	.size	_ZN2v88internal11Utf8DecoderC2ERKNS0_6VectorIKhEE, .-_ZN2v88internal11Utf8DecoderC2ERKNS0_6VectorIKhEE
	.globl	_ZN2v88internal11Utf8DecoderC1ERKNS0_6VectorIKhEE
	.set	_ZN2v88internal11Utf8DecoderC1ERKNS0_6VectorIKhEE,_ZN2v88internal11Utf8DecoderC2ERKNS0_6VectorIKhEE
	.section	.text._ZN2v88internal11Utf8Decoder6DecodeIhEEvPT_RKNS0_6VectorIKhEE,"axG",@progbits,_ZN2v88internal11Utf8Decoder6DecodeIhEEvPT_RKNS0_6VectorIKhEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11Utf8Decoder6DecodeIhEEvPT_RKNS0_6VectorIKhEE
	.type	_ZN2v88internal11Utf8Decoder6DecodeIhEEvPT_RKNS0_6VectorIKhEE, @function
_ZN2v88internal11Utf8Decoder6DecodeIhEEvPT_RKNS0_6VectorIKhEE:
.LFB5439:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movslq	4(%rdi), %rdx
	movq	(%r12), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	$7, %rdx
	ja	.L79
	leaq	(%rcx,%rdx), %rbx
	cmpq	%rbx, %rcx
	jnb	.L46
	leaq	15(%rcx), %rax
	subq	%rsi, %rax
	cmpq	$30, %rax
	jbe	.L61
	movq	%rcx, %rax
	notq	%rax
	addq	%rbx, %rax
	cmpq	$14, %rax
	jbe	.L61
	movq	%rdx, %rdi
	xorl	%eax, %eax
	andq	$-16, %rdi
	.p2align 4,,10
	.p2align 3
.L48:
	movdqu	(%rsi,%rax), %xmm0
	movups	%xmm0, (%rcx,%rax)
	addq	$16, %rax
	cmpq	%rdi, %rax
	jne	.L48
	movq	%rdx, %rdi
	andq	$-16, %rdi
	addq	%rdi, %rsi
	leaq	(%rcx,%rdi), %rax
	cmpq	%rdi, %rdx
	je	.L50
	movzbl	(%rsi), %edx
	movb	%dl, (%rax)
	leaq	1(%rax), %rdx
	cmpq	%rdx, %rbx
	jbe	.L50
	movzbl	1(%rsi), %edx
	movb	%dl, 1(%rax)
	leaq	2(%rax), %rdx
	cmpq	%rdx, %rbx
	jbe	.L50
	movzbl	2(%rsi), %edx
	movb	%dl, 2(%rax)
	leaq	3(%rax), %rdx
	cmpq	%rdx, %rbx
	jbe	.L50
	movzbl	3(%rsi), %edx
	movb	%dl, 3(%rax)
	leaq	4(%rax), %rdx
	cmpq	%rdx, %rbx
	jbe	.L50
	movzbl	4(%rsi), %edx
	movb	%dl, 4(%rax)
	leaq	5(%rax), %rdx
	cmpq	%rdx, %rbx
	jbe	.L50
	movzbl	5(%rsi), %edx
	movb	%dl, 5(%rax)
	leaq	6(%rax), %rdx
	cmpq	%rdx, %rbx
	jbe	.L50
	movzbl	6(%rsi), %edx
	movb	%dl, 6(%rax)
	leaq	7(%rax), %rdx
	cmpq	%rdx, %rbx
	jbe	.L50
	movzbl	7(%rsi), %edx
	movb	%dl, 7(%rax)
	leaq	8(%rax), %rdx
	cmpq	%rdx, %rbx
	jbe	.L50
	movzbl	8(%rsi), %edx
	movb	%dl, 8(%rax)
	leaq	9(%rax), %rdx
	cmpq	%rdx, %rbx
	jbe	.L50
	movzbl	9(%rsi), %edx
	movb	%dl, 9(%rax)
	leaq	10(%rax), %rdx
	cmpq	%rdx, %rbx
	jbe	.L50
	movzbl	10(%rsi), %edx
	movb	%dl, 10(%rax)
	leaq	11(%rax), %rdx
	cmpq	%rdx, %rbx
	jbe	.L50
	movzbl	11(%rsi), %edx
	movb	%dl, 11(%rax)
	leaq	12(%rax), %rdx
	cmpq	%rdx, %rbx
	jbe	.L50
	movzbl	12(%rsi), %edx
	movb	%dl, 12(%rax)
	leaq	13(%rax), %rdx
	cmpq	%rdx, %rbx
	jbe	.L50
	movzbl	13(%rsi), %edx
	movb	%dl, 13(%rax)
	leaq	14(%rax), %rdx
	cmpq	%rdx, %rbx
	jbe	.L50
	movzbl	14(%rsi), %edx
	movb	%dl, 14(%rax)
	.p2align 4,,10
	.p2align 3
.L50:
	movslq	4(%r13), %rdx
	movq	(%r12), %rsi
	leaq	(%rcx,%rdx), %rbx
.L46:
	movslq	8(%r12), %r8
	addq	%rsi, %rdx
	movb	$12, -41(%rbp)
	leaq	_ZZN14Utf8DfaDecoderL6DecodeEhPNS_5StateEPjE11transitions(%rip), %r13
	leaq	_ZZN14Utf8DfaDecoderL6DecodeEhPNS_5StateEPjE6states(%rip), %r12
	movl	$127, %r11d
	addq	%rsi, %r8
	xorl	%esi, %esi
	cmpq	%rdx, %r8
	jbe	.L51
.L80:
	movzbl	-41(%rbp), %edi
	.p2align 4,,10
	.p2align 3
.L52:
	movzbl	(%rdx), %eax
	movq	%rdx, %r9
	addq	$1, %rdx
	testb	%al, %al
	js	.L64
	cmpb	$12, %dil
	jne	.L64
.L53:
	movb	%al, (%rbx)
	addq	$1, %rbx
	cmpq	%rdx, %r8
	ja	.L80
	.p2align 4,,10
	.p2align 3
.L51:
	leaq	-41(%rbp), %rdi
	call	_ZN7unibrow4Utf824ValueOfIncrementalFinishEPN14Utf8DfaDecoder5StateE@PLT
	testl	%eax, %eax
	jne	.L81
.L44:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L82
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	.cfi_restore_state
	movzbl	0(%r13,%rax), %ecx
	movzbl	%al, %r10d
	movl	%r11d, %r14d
	sall	$6, %esi
	movzbl	%dil, %eax
	addl	%ecx, %eax
	sarl	%ecx
	cltq
	sarl	%cl, %r14d
	movzbl	(%r12,%rax), %eax
	andl	%r14d, %r10d
	orl	%r10d, %esi
	movb	%al, -41(%rbp)
	testb	%al, %al
	je	.L55
	cmpb	$12, %al
	je	.L56
	.p2align 4,,10
	.p2align 3
.L57:
	cmpq	%rdx, %r8
	je	.L51
	movl	%eax, %edi
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L56:
	cmpl	$-4, %esi
	jne	.L83
	xorl	%esi, %esi
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L55:
	xorl	%esi, %esi
	cmpb	$12, %dil
	movb	$12, -41(%rbp)
	movl	$-3, %eax
	cmovne	%r9, %rdx
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L81:
	movb	%al, (%rbx)
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L79:
	movq	%rcx, %rdi
	call	memcpy@PLT
	movslq	4(%r13), %rdx
	movq	(%r12), %rsi
	leaq	(%rax,%rdx), %rbx
	jmp	.L46
.L61:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L47:
	movzbl	(%rsi,%rax), %edi
	movb	%dil, (%rcx,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L47
	movslq	4(%r13), %rdx
	movq	(%r12), %rsi
	leaq	(%rcx,%rdx), %rbx
	jmp	.L46
.L82:
	call	__stack_chk_fail@PLT
.L83:
	movl	%esi, %eax
	xorl	%esi, %esi
	jmp	.L53
	.cfi_endproc
.LFE5439:
	.size	_ZN2v88internal11Utf8Decoder6DecodeIhEEvPT_RKNS0_6VectorIKhEE, .-_ZN2v88internal11Utf8Decoder6DecodeIhEEvPT_RKNS0_6VectorIKhEE
	.section	.text._ZN2v88internal11Utf8Decoder6DecodeItEEvPT_RKNS0_6VectorIKhEE,"axG",@progbits,_ZN2v88internal11Utf8Decoder6DecodeItEEvPT_RKNS0_6VectorIKhEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11Utf8Decoder6DecodeItEEvPT_RKNS0_6VectorIKhEE
	.type	_ZN2v88internal11Utf8Decoder6DecodeItEEvPT_RKNS0_6VectorIKhEE, @function
_ZN2v88internal11Utf8Decoder6DecodeItEEvPT_RKNS0_6VectorIKhEE:
.LFB5440:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movslq	4(%rdi), %rax
	movq	(%rdx), %rdi
	leaq	(%rsi,%rax,2), %rbx
	cmpq	%rbx, %rsi
	jnb	.L85
	movq	%rsi, %rax
	notq	%rax
	addq	%rbx, %rax
	movq	%rax, %rcx
	shrq	%rcx
	addq	$1, %rcx
	leaq	(%rsi,%rcx,2), %r8
	cmpq	%r8, %rdi
	leaq	(%rdi,%rcx), %r8
	setnb	%r10b
	cmpq	%r8, %rsi
	setnb	%r8b
	orb	%r8b, %r10b
	je	.L102
	cmpq	$29, %rax
	jbe	.L102
	movq	%rcx, %r8
	xorl	%eax, %eax
	pxor	%xmm1, %xmm1
	andq	$-16, %r8
	.p2align 4,,10
	.p2align 3
.L87:
	movdqu	(%rdi,%rax), %xmm0
	movdqa	%xmm0, %xmm2
	punpckhbw	%xmm1, %xmm0
	punpcklbw	%xmm1, %xmm2
	movups	%xmm0, 16(%rsi,%rax,2)
	movups	%xmm2, (%rsi,%rax,2)
	addq	$16, %rax
	cmpq	%r8, %rax
	jne	.L87
	movq	%rcx, %r8
	andq	$-16, %r8
	addq	%r8, %rdi
	leaq	(%rsi,%r8,2), %rax
	cmpq	%r8, %rcx
	je	.L89
	movzbl	(%rdi), %ecx
	movw	%cx, (%rax)
	leaq	2(%rax), %rcx
	cmpq	%rcx, %rbx
	jbe	.L89
	movzbl	1(%rdi), %ecx
	movw	%cx, 2(%rax)
	leaq	4(%rax), %rcx
	cmpq	%rcx, %rbx
	jbe	.L89
	movzbl	2(%rdi), %ecx
	movw	%cx, 4(%rax)
	leaq	6(%rax), %rcx
	cmpq	%rcx, %rbx
	jbe	.L89
	movzbl	3(%rdi), %ecx
	movw	%cx, 6(%rax)
	leaq	8(%rax), %rcx
	cmpq	%rcx, %rbx
	jbe	.L89
	movzbl	4(%rdi), %ecx
	movw	%cx, 8(%rax)
	leaq	10(%rax), %rcx
	cmpq	%rcx, %rbx
	jbe	.L89
	movzbl	5(%rdi), %ecx
	movw	%cx, 10(%rax)
	leaq	12(%rax), %rcx
	cmpq	%rcx, %rbx
	jbe	.L89
	movzbl	6(%rdi), %ecx
	movw	%cx, 12(%rax)
	leaq	14(%rax), %rcx
	cmpq	%rcx, %rbx
	jbe	.L89
	movzbl	7(%rdi), %ecx
	movw	%cx, 14(%rax)
	leaq	16(%rax), %rcx
	cmpq	%rcx, %rbx
	jbe	.L89
	movzbl	8(%rdi), %ecx
	movw	%cx, 16(%rax)
	leaq	18(%rax), %rcx
	cmpq	%rcx, %rbx
	jbe	.L89
	movzbl	9(%rdi), %ecx
	movw	%cx, 18(%rax)
	leaq	20(%rax), %rcx
	cmpq	%rcx, %rbx
	jbe	.L89
	movzbl	10(%rdi), %ecx
	movw	%cx, 20(%rax)
	leaq	22(%rax), %rcx
	cmpq	%rcx, %rbx
	jbe	.L89
	movzbl	11(%rdi), %ecx
	movw	%cx, 22(%rax)
	leaq	24(%rax), %rcx
	cmpq	%rcx, %rbx
	jbe	.L89
	movzbl	12(%rdi), %ecx
	movw	%cx, 24(%rax)
	leaq	26(%rax), %rcx
	cmpq	%rcx, %rbx
	jbe	.L89
	movzbl	13(%rdi), %ecx
	movw	%cx, 26(%rax)
	leaq	28(%rax), %rcx
	cmpq	%rcx, %rbx
	jbe	.L89
	movzbl	14(%rdi), %ecx
	movw	%cx, 28(%rax)
	.p2align 4,,10
	.p2align 3
.L89:
	movslq	4(%r9), %rax
	movq	(%rdx), %rdi
	leaq	(%rsi,%rax,2), %rbx
.L85:
	movslq	8(%rdx), %rdx
	movb	$12, -41(%rbp)
	addq	%rdi, %rax
	xorl	%esi, %esi
	leaq	_ZZN14Utf8DfaDecoderL6DecodeEhPNS_5StateEPjE11transitions(%rip), %r12
	leaq	_ZZN14Utf8DfaDecoderL6DecodeEhPNS_5StateEPjE6states(%rip), %r11
	movl	$127, %r10d
	addq	%rdx, %rdi
.L90:
	cmpq	%rax, %rdi
	jbe	.L91
.L122:
	movzbl	-41(%rbp), %r8d
	.p2align 4,,10
	.p2align 3
.L92:
	movzbl	(%rax), %edx
	movq	%rax, %r9
	addq	$1, %rax
	testb	%dl, %dl
	js	.L93
	cmpb	$12, %r8b
	jne	.L93
.L94:
	movw	%dx, (%rbx)
	addq	$2, %rbx
	cmpq	%rax, %rdi
	ja	.L122
	.p2align 4,,10
	.p2align 3
.L91:
	leaq	-41(%rbp), %rdi
	call	_ZN7unibrow4Utf824ValueOfIncrementalFinishEPN14Utf8DfaDecoder5StateE@PLT
	testl	%eax, %eax
	jne	.L123
.L84:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L124
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L93:
	.cfi_restore_state
	movzbl	(%r12,%rdx), %ecx
	movzbl	%dl, %r13d
	movzbl	%r8b, %edx
	movl	%r10d, %r14d
	sall	$6, %esi
	addl	%ecx, %edx
	sarl	%ecx
	movslq	%edx, %rdx
	sarl	%cl, %r14d
	movzbl	(%r11,%rdx), %edx
	andl	%r14d, %r13d
	orl	%r13d, %esi
	movb	%dl, -41(%rbp)
	testb	%dl, %dl
	je	.L95
	cmpb	$12, %dl
	je	.L96
	.p2align 4,,10
	.p2align 3
.L97:
	cmpq	%rax, %rdi
	je	.L91
	movl	%edx, %r8d
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L96:
	cmpl	$-4, %esi
	jne	.L125
	xorl	%esi, %esi
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L95:
	xorl	%esi, %esi
	cmpb	$12, %r8b
	movb	$12, -41(%rbp)
	movl	$-3, %edx
	cmovne	%r9, %rax
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L123:
	movw	%ax, (%rbx)
	jmp	.L84
.L125:
	movl	%esi, %edx
	cmpl	$65535, %esi
	jbe	.L126
	leal	-65536(%rsi), %edx
	andw	$1023, %si
	addq	$4, %rbx
	shrl	$10, %edx
	subw	$9216, %si
	andw	$1023, %dx
	movw	%si, -2(%rbx)
	xorl	%esi, %esi
	subw	$10240, %dx
	movw	%dx, -4(%rbx)
	jmp	.L90
.L102:
	movq	%rsi, %rax
	.p2align 4,,10
	.p2align 3
.L86:
	movzbl	(%rdi), %ecx
	addq	$2, %rax
	addq	$1, %rdi
	movw	%cx, -2(%rax)
	cmpq	%rbx, %rax
	jb	.L86
	jmp	.L89
.L124:
	call	__stack_chk_fail@PLT
.L126:
	xorl	%esi, %esi
	jmp	.L94
	.cfi_endproc
.LFE5440:
	.size	_ZN2v88internal11Utf8Decoder6DecodeItEEvPT_RKNS0_6VectorIKhEE, .-_ZN2v88internal11Utf8Decoder6DecodeItEEvPT_RKNS0_6VectorIKhEE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal11Utf8DecoderC2ERKNS0_6VectorIKhEE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal11Utf8DecoderC2ERKNS0_6VectorIKhEE, @function
_GLOBAL__sub_I__ZN2v88internal11Utf8DecoderC2ERKNS0_6VectorIKhEE:
.LFB5808:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE5808:
	.size	_GLOBAL__sub_I__ZN2v88internal11Utf8DecoderC2ERKNS0_6VectorIKhEE, .-_GLOBAL__sub_I__ZN2v88internal11Utf8DecoderC2ERKNS0_6VectorIKhEE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal11Utf8DecoderC2ERKNS0_6VectorIKhEE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata._ZZN14Utf8DfaDecoderL6DecodeEhPNS_5StateEPjE6states,"a"
	.align 32
	.type	_ZZN14Utf8DfaDecoderL6DecodeEhPNS_5StateEPjE6states, @object
	.size	_ZZN14Utf8DfaDecoderL6DecodeEhPNS_5StateEPjE6states, 108
_ZZN14Utf8DfaDecoderL6DecodeEhPNS_5StateEPjE6states:
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\f"
	.string	""
	.string	""
	.string	"\030$0<H"
	.string	"T`"
	.string	"\f\f\f"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\030\030\030"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\030\030"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"$$$"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"$"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\030"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"$$"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.section	.rodata._ZZN14Utf8DfaDecoderL6DecodeEhPNS_5StateEPjE11transitions,"a"
	.align 32
	.type	_ZZN14Utf8DfaDecoderL6DecodeEhPNS_5StateEPjE11transitions, @object
	.size	_ZZN14Utf8DfaDecoderL6DecodeEhPNS_5StateEPjE11transitions, 256
_ZZN14Utf8DfaDecoderL6DecodeEhPNS_5StateEPjE11transitions:
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.ascii	"\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001"
	.ascii	"\001\002\002\002\002\002\002\002\002\002\002\002\002\002\002"
	.ascii	"\002\002\003\003\003\003\003\003\003\003\003\003\003\003\003"
	.ascii	"\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"
	.ascii	"\003\003\003\003\t\t\004\004\004\004\004\004\004\004\004\004"
	.ascii	"\004\004\004\004\004\004\004\004\004\004\004\004\004\004\004"
	.ascii	"\004\004\004\004\004\n\005\005\005\005\005\005\005\005\005\005"
	.ascii	"\005\005\006\005\005\013\007\007\007\b\t\t\t\t\t\t\t\t\t\t\t"
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
