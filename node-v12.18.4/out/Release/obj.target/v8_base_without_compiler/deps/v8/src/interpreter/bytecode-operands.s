	.file	"bytecode-operands.cc"
	.text
	.section	.rodata._ZN2v88internal11interpreterlsERSoRKNS1_14AccumulatorUseE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"Read"
.LC1:
	.string	"Write"
.LC2:
	.string	"None"
.LC3:
	.string	"ReadWrite"
.LC4:
	.string	"unreachable code"
	.section	.text._ZN2v88internal11interpreterlsERSoRKNS1_14AccumulatorUseE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal11interpreterlsERSoRKNS1_14AccumulatorUseE
	.type	_ZN2v88internal11interpreterlsERSoRKNS1_14AccumulatorUseE, @function
_ZN2v88internal11interpreterlsERSoRKNS1_14AccumulatorUseE:
.LFB5218:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movzbl	(%rsi), %eax
	cmpb	$2, %al
	je	.L4
	ja	.L3
	testb	%al, %al
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rax
	movl	$4, %edx
	cmove	%rax, %rsi
.L2:
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	.cfi_restore_state
	cmpb	$3, %al
	jne	.L9
	movl	$9, %edx
	leaq	.LC3(%rip), %rsi
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L4:
	movl	$5, %edx
	leaq	.LC1(%rip), %rsi
	jmp	.L2
.L9:
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE5218:
	.size	_ZN2v88internal11interpreterlsERSoRKNS1_14AccumulatorUseE, .-_ZN2v88internal11interpreterlsERSoRKNS1_14AccumulatorUseE
	.section	.rodata._ZN2v88internal11interpreterlsERSoRKNS1_11OperandSizeE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"Byte"
.LC6:
	.string	"Short"
.LC7:
	.string	"Quad"
	.section	.text._ZN2v88internal11interpreterlsERSoRKNS1_11OperandSizeE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal11interpreterlsERSoRKNS1_11OperandSizeE
	.type	_ZN2v88internal11interpreterlsERSoRKNS1_11OperandSizeE, @function
_ZN2v88internal11interpreterlsERSoRKNS1_11OperandSizeE:
.LFB5219:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movzbl	(%rsi), %eax
	cmpb	$2, %al
	je	.L13
	ja	.L12
	testb	%al, %al
	leaq	.LC5(%rip), %rsi
	leaq	.LC2(%rip), %rax
	movl	$4, %edx
	cmove	%rax, %rsi
.L11:
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	cmpb	$4, %al
	jne	.L17
	movl	$4, %edx
	leaq	.LC7(%rip), %rsi
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L13:
	movl	$5, %edx
	leaq	.LC6(%rip), %rsi
	jmp	.L11
.L17:
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE5219:
	.size	_ZN2v88internal11interpreterlsERSoRKNS1_11OperandSizeE, .-_ZN2v88internal11interpreterlsERSoRKNS1_11OperandSizeE
	.section	.rodata._ZN2v88internal11interpreterlsERSoRKNS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC8:
	.string	"Double"
.LC9:
	.string	"Quadruple"
.LC10:
	.string	"Single"
	.section	.text._ZN2v88internal11interpreterlsERSoRKNS1_12OperandScaleE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal11interpreterlsERSoRKNS1_12OperandScaleE
	.type	_ZN2v88internal11interpreterlsERSoRKNS1_12OperandScaleE, @function
_ZN2v88internal11interpreterlsERSoRKNS1_12OperandScaleE:
.LFB5220:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movzbl	(%rsi), %eax
	cmpb	$2, %al
	je	.L20
	cmpb	$4, %al
	je	.L21
	cmpb	$1, %al
	je	.L22
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L21:
	movl	$9, %edx
	leaq	.LC9(%rip), %rsi
.L19:
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore_state
	movl	$6, %edx
	leaq	.LC10(%rip), %rsi
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L20:
	movl	$6, %edx
	leaq	.LC8(%rip), %rsi
	jmp	.L19
	.cfi_endproc
.LFE5220:
	.size	_ZN2v88internal11interpreterlsERSoRKNS1_12OperandScaleE, .-_ZN2v88internal11interpreterlsERSoRKNS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreterlsERSoRKNS1_11OperandTypeE.str1.1,"aMS",@progbits,1
.LC11:
	.string	"RegOutList"
.LC12:
	.string	"RegOutPair"
.LC13:
	.string	"RegOutTriple"
.LC14:
	.string	"Flag8"
.LC15:
	.string	"RuntimeId"
.LC16:
	.string	"NativeContextIndex"
.LC17:
	.string	"Idx"
.LC18:
	.string	"UImm"
.LC19:
	.string	"RegCount"
.LC20:
	.string	"Imm"
.LC21:
	.string	"Reg"
.LC22:
	.string	"RegList"
.LC23:
	.string	"RegPair"
.LC24:
	.string	"RegOut"
.LC25:
	.string	"IntrinsicId"
	.section	.text._ZN2v88internal11interpreterlsERSoRKNS1_11OperandTypeE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal11interpreterlsERSoRKNS1_11OperandTypeE
	.type	_ZN2v88internal11interpreterlsERSoRKNS1_11OperandTypeE, @function
_ZN2v88internal11interpreterlsERSoRKNS1_11OperandTypeE:
.LFB5221:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	cmpb	$15, (%rsi)
	ja	.L25
	movzbl	(%rsi), %eax
	leaq	.L27(%rip), %rdx
	movq	%rdi, %r12
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal11interpreterlsERSoRKNS1_11OperandTypeE,"a",@progbits
	.align 4
	.align 4
.L27:
	.long	.L42-.L27
	.long	.L41-.L27
	.long	.L40-.L27
	.long	.L43-.L27
	.long	.L38-.L27
	.long	.L37-.L27
	.long	.L36-.L27
	.long	.L35-.L27
	.long	.L34-.L27
	.long	.L33-.L27
	.long	.L32-.L27
	.long	.L31-.L27
	.long	.L30-.L27
	.long	.L29-.L27
	.long	.L28-.L27
	.long	.L26-.L27
	.section	.text._ZN2v88internal11interpreterlsERSoRKNS1_11OperandTypeE
	.p2align 4,,10
	.p2align 3
.L28:
	movl	$10, %edx
	leaq	.LC12(%rip), %rsi
.L39:
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	movl	$12, %edx
	leaq	.LC13(%rip), %rsi
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L42:
	movl	$4, %edx
	leaq	.LC2(%rip), %rsi
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L41:
	movl	$5, %edx
	leaq	.LC14(%rip), %rsi
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L40:
	movl	$11, %edx
	leaq	.LC25(%rip), %rsi
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L43:
	movl	$9, %edx
	leaq	.LC15(%rip), %rsi
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L38:
	movl	$18, %edx
	leaq	.LC16(%rip), %rsi
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L37:
	movl	$3, %edx
	leaq	.LC17(%rip), %rsi
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L36:
	movl	$4, %edx
	leaq	.LC18(%rip), %rsi
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L35:
	movl	$8, %edx
	leaq	.LC19(%rip), %rsi
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L34:
	movl	$3, %edx
	leaq	.LC20(%rip), %rsi
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L33:
	movl	$3, %edx
	leaq	.LC21(%rip), %rsi
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L32:
	movl	$7, %edx
	leaq	.LC22(%rip), %rsi
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L31:
	movl	$7, %edx
	leaq	.LC23(%rip), %rsi
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L30:
	movl	$6, %edx
	leaq	.LC24(%rip), %rsi
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L29:
	movl	$10, %edx
	leaq	.LC11(%rip), %rsi
	jmp	.L39
.L25:
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE5221:
	.size	_ZN2v88internal11interpreterlsERSoRKNS1_11OperandTypeE, .-_ZN2v88internal11interpreterlsERSoRKNS1_11OperandTypeE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal11interpreterlsERSoRKNS1_14AccumulatorUseE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal11interpreterlsERSoRKNS1_14AccumulatorUseE, @function
_GLOBAL__sub_I__ZN2v88internal11interpreterlsERSoRKNS1_14AccumulatorUseE:
.LFB6001:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE6001:
	.size	_GLOBAL__sub_I__ZN2v88internal11interpreterlsERSoRKNS1_14AccumulatorUseE, .-_GLOBAL__sub_I__ZN2v88internal11interpreterlsERSoRKNS1_14AccumulatorUseE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal11interpreterlsERSoRKNS1_14AccumulatorUseE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
