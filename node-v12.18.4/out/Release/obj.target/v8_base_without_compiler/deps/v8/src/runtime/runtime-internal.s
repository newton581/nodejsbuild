	.file	"runtime-internal.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB4860:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE4860:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB4861:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4861:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB4863:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4863:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.text._ZN2v88internal8OFStreamD1Ev,"axG",@progbits,_ZN2v88internal8OFStreamD1Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8OFStreamD1Ev
	.type	_ZN2v88internal8OFStreamD1Ev, @function
_ZN2v88internal8OFStreamD1Ev:
.LFB21122:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE21122:
	.size	_ZN2v88internal8OFStreamD1Ev, .-_ZN2v88internal8OFStreamD1Ev
	.section	.text._ZN2v88internal8OFStreamD0Ev,"axG",@progbits,_ZN2v88internal8OFStreamD0Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal8OFStreamD0Ev
	.type	_ZTv0_n24_N2v88internal8OFStreamD0Ev, @function
_ZTv0_n24_N2v88internal8OFStreamD0Ev:
.LFB26577:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %rax
	addq	-24(%rax), %rdi
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	movq	%rax, 80(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE26577:
	.size	_ZTv0_n24_N2v88internal8OFStreamD0Ev, .-_ZTv0_n24_N2v88internal8OFStreamD0Ev
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8OFStreamD0Ev
	.type	_ZN2v88internal8OFStreamD0Ev, @function
_ZN2v88internal8OFStreamD0Ev:
.LFB21123:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE21123:
	.size	_ZN2v88internal8OFStreamD0Ev, .-_ZN2v88internal8OFStreamD0Ev
	.section	.text._ZN2v88internal8OFStreamD1Ev,"axG",@progbits,_ZN2v88internal8OFStreamD1Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal8OFStreamD1Ev
	.type	_ZTv0_n24_N2v88internal8OFStreamD1Ev, @function
_ZTv0_n24_N2v88internal8OFStreamD1Ev:
.LFB26578:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	-24(%rax), %rbx
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	addq	%rdi, %rbx
	movq	%rax, 80(%rbx)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	64(%rbx), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE26578:
	.size	_ZTv0_n24_N2v88internal8OFStreamD1Ev, .-_ZTv0_n24_N2v88internal8OFStreamD1Ev
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB11068:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L19
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L22
.L13:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L13
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE11068:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.rodata._ZN2v88internalL19Stats_Runtime_ThrowEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"disabled-by-default-v8.runtime"
	.section	.rodata._ZN2v88internalL19Stats_Runtime_ThrowEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC1:
	.string	"V8.Runtime_Runtime_Throw"
	.section	.text._ZN2v88internalL19Stats_Runtime_ThrowEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL19Stats_Runtime_ThrowEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL19Stats_Runtime_ThrowEiPmPNS0_7IsolateE.isra.0:
.LFB26508:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L54
.L24:
	movq	_ZZN2v88internalL19Stats_Runtime_ThrowEiPmPNS0_7IsolateEE27trace_event_unique_atomic59(%rip), %rbx
	testq	%rbx, %rbx
	je	.L55
.L26:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L56
.L28:
	addl	$1, 41104(%r12)
	movq	0(%r13), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	41096(%r12), %rbx
	je	.L34
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L34:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L57
.L23:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L58
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L59
.L29:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L30
	movq	(%rdi), %rax
	call	*8(%rax)
.L30:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L31
	movq	(%rdi), %rax
	call	*8(%rax)
.L31:
	leaq	.LC1(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L55:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L60
.L27:
	movq	%rbx, _ZZN2v88internalL19Stats_Runtime_ThrowEiPmPNS0_7IsolateEE27trace_event_unique_atomic59(%rip)
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L54:
	movq	40960(%rsi), %rax
	movl	$362, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L57:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L60:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L59:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC1(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L29
.L58:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26508:
	.size	_ZN2v88internalL19Stats_Runtime_ThrowEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL19Stats_Runtime_ThrowEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL21Stats_Runtime_ReThrowEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC2:
	.string	"V8.Runtime_Runtime_ReThrow"
	.section	.text._ZN2v88internalL21Stats_Runtime_ReThrowEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL21Stats_Runtime_ReThrowEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL21Stats_Runtime_ReThrowEiPmPNS0_7IsolateE.isra.0:
.LFB26509:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L92
.L62:
	movq	_ZZN2v88internalL21Stats_Runtime_ReThrowEiPmPNS0_7IsolateEE27trace_event_unique_atomic65(%rip), %rbx
	testq	%rbx, %rbx
	je	.L93
.L64:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L94
.L66:
	addl	$1, 41104(%r12)
	movq	0(%r13), %rsi
	movq	%r12, %rdi
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	call	_ZN2v88internal7Isolate7ReThrowENS0_6ObjectE@PLT
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	41096(%r12), %rbx
	je	.L72
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L72:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L95
.L61:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L96
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L97
.L67:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L68
	movq	(%rdi), %rax
	call	*8(%rax)
.L68:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L69
	movq	(%rdi), %rax
	call	*8(%rax)
.L69:
	leaq	.LC2(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L93:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L98
.L65:
	movq	%rbx, _ZZN2v88internalL21Stats_Runtime_ReThrowEiPmPNS0_7IsolateEE27trace_event_unique_atomic65(%rip)
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L92:
	movq	40960(%rsi), %rax
	movl	$358, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L95:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L98:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L97:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC2(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L67
.L96:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26509:
	.size	_ZN2v88internalL21Stats_Runtime_ReThrowEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL21Stats_Runtime_ReThrowEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL32Stats_Runtime_ThrowStackOverflowEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"V8.Runtime_Runtime_ThrowStackOverflow"
	.section	.text._ZN2v88internalL32Stats_Runtime_ThrowStackOverflowEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL32Stats_Runtime_ThrowStackOverflowEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL32Stats_Runtime_ThrowStackOverflowEiPmPNS0_7IsolateE.isra.0:
.LFB26510:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L128
.L100:
	movq	_ZZN2v88internalL32Stats_Runtime_ThrowStackOverflowEiPmPNS0_7IsolateEE27trace_event_unique_atomic71(%rip), %rbx
	testq	%rbx, %rbx
	je	.L129
.L102:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L130
.L104:
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate13StackOverflowEv@PLT
	leaq	-144(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L131
.L99:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L132
	leaq	-24(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L129:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L133
.L103:
	movq	%rbx, _ZZN2v88internalL32Stats_Runtime_ThrowStackOverflowEiPmPNS0_7IsolateEE27trace_event_unique_atomic71(%rip)
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L130:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L134
.L105:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L106
	movq	(%rdi), %rax
	call	*8(%rax)
.L106:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L107
	movq	(%rdi), %rax
	call	*8(%rax)
.L107:
	leaq	.LC3(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L131:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L128:
	movq	40960(%rdi), %rax
	leaq	-104(%rbp), %rsi
	movl	$376, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L134:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC3(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L133:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L103
.L132:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26510:
	.size	_ZN2v88internalL32Stats_Runtime_ThrowStackOverflowEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL32Stats_Runtime_ThrowStackOverflowEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL43Stats_Runtime_UnwindAndFindExceptionHandlerEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"V8.Runtime_Runtime_UnwindAndFindExceptionHandler"
	.section	.text._ZN2v88internalL43Stats_Runtime_UnwindAndFindExceptionHandlerEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL43Stats_Runtime_UnwindAndFindExceptionHandlerEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL43Stats_Runtime_UnwindAndFindExceptionHandlerEiPmPNS0_7IsolateE.isra.0:
.LFB26511:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L164
.L136:
	movq	_ZZN2v88internalL43Stats_Runtime_UnwindAndFindExceptionHandlerEiPmPNS0_7IsolateEE28trace_event_unique_atomic171(%rip), %rbx
	testq	%rbx, %rbx
	je	.L165
.L138:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L166
.L140:
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate20UnwindAndFindHandlerEv@PLT
	leaq	-144(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L167
.L135:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L168
	leaq	-24(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L165:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L169
.L139:
	movq	%rbx, _ZZN2v88internalL43Stats_Runtime_UnwindAndFindExceptionHandlerEiPmPNS0_7IsolateEE28trace_event_unique_atomic171(%rip)
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L166:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L170
.L141:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L142
	movq	(%rdi), %rax
	call	*8(%rax)
.L142:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L143
	movq	(%rdi), %rax
	call	*8(%rax)
.L143:
	leaq	.LC4(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L167:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L164:
	movq	40960(%rdi), %rax
	leaq	-104(%rbp), %rsi
	movl	$383, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L170:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC4(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L169:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L139
.L168:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26511:
	.size	_ZN2v88internalL43Stats_Runtime_UnwindAndFindExceptionHandlerEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL43Stats_Runtime_UnwindAndFindExceptionHandlerEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL39Stats_Runtime_PromoteScheduledExceptionEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"V8.Runtime_Runtime_PromoteScheduledException"
	.section	.text._ZN2v88internalL39Stats_Runtime_PromoteScheduledExceptionEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL39Stats_Runtime_PromoteScheduledExceptionEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL39Stats_Runtime_PromoteScheduledExceptionEiPmPNS0_7IsolateE.isra.0:
.LFB26512:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L200
.L172:
	movq	_ZZN2v88internalL39Stats_Runtime_PromoteScheduledExceptionEiPmPNS0_7IsolateEE28trace_event_unique_atomic177(%rip), %rbx
	testq	%rbx, %rbx
	je	.L201
.L174:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L202
.L176:
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	leaq	-144(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L203
.L171:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L204
	leaq	-24(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L201:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L205
.L175:
	movq	%rbx, _ZZN2v88internalL39Stats_Runtime_PromoteScheduledExceptionEiPmPNS0_7IsolateEE28trace_event_unique_atomic177(%rip)
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L202:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L206
.L177:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L178
	movq	(%rdi), %rax
	call	*8(%rax)
.L178:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L179
	movq	(%rdi), %rax
	call	*8(%rax)
.L179:
	leaq	.LC5(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L203:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L200:
	movq	40960(%rdi), %rax
	leaq	-104(%rbp), %rsi
	movl	$356, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L206:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC5(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L205:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L175
.L204:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26512:
	.size	_ZN2v88internalL39Stats_Runtime_PromoteScheduledExceptionEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL39Stats_Runtime_PromoteScheduledExceptionEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL24Stats_Runtime_StackGuardEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC6:
	.string	"V8.Runtime_Runtime_StackGuard"
.LC7:
	.string	"v8.execute"
.LC8:
	.string	"V8.StackGuard"
	.section	.text._ZN2v88internalL24Stats_Runtime_StackGuardEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL24Stats_Runtime_StackGuardEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL24Stats_Runtime_StackGuardEiPmPNS0_7IsolateE.isra.0:
.LFB26513:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L256
.L208:
	movq	_ZZN2v88internalL24Stats_Runtime_StackGuardEiPmPNS0_7IsolateEE28trace_event_unique_atomic272(%rip), %rbx
	testq	%rbx, %rbx
	je	.L257
.L210:
	movq	$0, -176(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L258
.L212:
	movq	_ZZN2v88internalL28__RT_impl_Runtime_StackGuardENS0_9ArgumentsEPNS0_7IsolateEE28trace_event_unique_atomic275(%rip), %rbx
	testq	%rbx, %rbx
	je	.L259
.L217:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L260
.L219:
	xorl	%esi, %esi
	leaq	-184(%rbp), %rdi
	movq	%r12, -184(%rbp)
	call	_ZNK2v88internal15StackLimitCheck15JsHasOverflowedEm@PLT
	testb	%al, %al
	je	.L223
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate13StackOverflowEv@PLT
	movq	%rax, %r12
.L224:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	leaq	-176(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L261
.L207:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L262
	leaq	-24(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L223:
	.cfi_restore_state
	leaq	37512(%r12), %rdi
	call	_ZN2v88internal10StackGuard16HandleInterruptsEv@PLT
	movq	%rax, %r12
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L257:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L263
.L211:
	movq	%rbx, _ZZN2v88internalL24Stats_Runtime_StackGuardEiPmPNS0_7IsolateEE28trace_event_unique_atomic272(%rip)
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L260:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L264
.L220:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L221
	movq	(%rdi), %rax
	call	*8(%rax)
.L221:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L222
	movq	(%rdi), %rax
	call	*8(%rax)
.L222:
	leaq	.LC8(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L259:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L265
.L218:
	movq	%rbx, _ZZN2v88internalL28__RT_impl_Runtime_StackGuardENS0_9ArgumentsEPNS0_7IsolateEE28trace_event_unique_atomic275(%rip)
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L258:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L266
.L213:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L214
	movq	(%rdi), %rax
	call	*8(%rax)
.L214:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L215
	movq	(%rdi), %rax
	call	*8(%rax)
.L215:
	leaq	.LC6(%rip), %rax
	movq	%rbx, -168(%rbp)
	movq	%rax, -160(%rbp)
	leaq	-168(%rbp), %rax
	movq	%r13, -152(%rbp)
	movq	%rax, -176(%rbp)
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L261:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L256:
	movq	40960(%rdi), %rax
	leaq	-104(%rbp), %rsi
	movl	$361, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L266:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC6(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L265:
	leaq	.LC7(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L264:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC8(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L263:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L211
.L262:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26513:
	.size	_ZN2v88internalL24Stats_Runtime_StackGuardEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL24Stats_Runtime_StackGuardEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL48Stats_Runtime_ThrowPatternAssignmentNonCoercibleEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC9:
	.string	"V8.Runtime_Runtime_ThrowPatternAssignmentNonCoercible"
	.section	.text._ZN2v88internalL48Stats_Runtime_ThrowPatternAssignmentNonCoercibleEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL48Stats_Runtime_ThrowPatternAssignmentNonCoercibleEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL48Stats_Runtime_ThrowPatternAssignmentNonCoercibleEiPmPNS0_7IsolateE.isra.0:
.LFB26514:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L297
.L268:
	movq	_ZZN2v88internalL48Stats_Runtime_ThrowPatternAssignmentNonCoercibleEiPmPNS0_7IsolateEE28trace_event_unique_atomic402(%rip), %rbx
	testq	%rbx, %rbx
	je	.L298
.L270:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L299
.L272:
	movq	41088(%r12), %r14
	movq	%r13, %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	addl	$1, 41104(%r12)
	movq	41096(%r12), %rbx
	call	_ZN2v88internal10ErrorUtils28ThrowLoadFromNullOrUndefinedEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS0_11MaybeHandleIS5_EE@PLT
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	41096(%r12), %rbx
	je	.L276
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L276:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L300
.L267:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L301
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L298:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L302
.L271:
	movq	%rbx, _ZZN2v88internalL48Stats_Runtime_ThrowPatternAssignmentNonCoercibleEiPmPNS0_7IsolateEE28trace_event_unique_atomic402(%rip)
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L299:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L303
.L273:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L274
	movq	(%rdi), %rax
	call	*8(%rax)
.L274:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L275
	movq	(%rdi), %rax
	call	*8(%rax)
.L275:
	leaq	.LC9(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L297:
	movq	40960(%rsi), %rax
	movl	$372, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L300:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L267
	.p2align 4,,10
	.p2align 3
.L303:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC9(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L273
	.p2align 4,,10
	.p2align 3
.L302:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L271
.L301:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26514:
	.size	_ZN2v88internalL48Stats_Runtime_ThrowPatternAssignmentNonCoercibleEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL48Stats_Runtime_ThrowPatternAssignmentNonCoercibleEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL45Stats_Runtime_ThrowSymbolAsyncIteratorInvalidEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC10:
	.string	"V8.Runtime_Runtime_ThrowSymbolAsyncIteratorInvalid"
	.section	.text._ZN2v88internalL45Stats_Runtime_ThrowSymbolAsyncIteratorInvalidEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL45Stats_Runtime_ThrowSymbolAsyncIteratorInvalidEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL45Stats_Runtime_ThrowSymbolAsyncIteratorInvalidEiPmPNS0_7IsolateE.isra.0:
.LFB26518:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L334
.L305:
	movq	_ZZN2v88internalL45Stats_Runtime_ThrowSymbolAsyncIteratorInvalidEiPmPNS0_7IsolateEE27trace_event_unique_atomic77(%rip), %rbx
	testq	%rbx, %rbx
	je	.L335
.L307:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L336
.L309:
	addl	$1, 41104(%r12)
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$168, %esi
	movq	%r12, %rdi
	movq	41088(%r12), %r13
	movq	41096(%r12), %rbx
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r14
	cmpq	41096(%r12), %rbx
	je	.L313
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L313:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L337
.L304:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L338
	leaq	-32(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L335:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L339
.L308:
	movq	%rbx, _ZZN2v88internalL45Stats_Runtime_ThrowSymbolAsyncIteratorInvalidEiPmPNS0_7IsolateEE27trace_event_unique_atomic77(%rip)
	jmp	.L307
	.p2align 4,,10
	.p2align 3
.L336:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L340
.L310:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L311
	movq	(%rdi), %rax
	call	*8(%rax)
.L311:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L312
	movq	(%rdi), %rax
	call	*8(%rax)
.L312:
	leaq	.LC10(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L337:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L304
	.p2align 4,,10
	.p2align 3
.L334:
	movq	40960(%rdi), %rax
	leaq	-104(%rbp), %rsi
	movl	$377, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L340:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC10(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L339:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L308
.L338:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26518:
	.size	_ZN2v88internalL45Stats_Runtime_ThrowSymbolAsyncIteratorInvalidEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL45Stats_Runtime_ThrowSymbolAsyncIteratorInvalidEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL33Stats_Runtime_ThrowReferenceErrorEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC11:
	.string	"V8.Runtime_Runtime_ThrowReferenceError"
	.section	.text._ZN2v88internalL33Stats_Runtime_ThrowReferenceErrorEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL33Stats_Runtime_ThrowReferenceErrorEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL33Stats_Runtime_ThrowReferenceErrorEiPmPNS0_7IsolateE.isra.0:
.LFB26519:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L371
.L342:
	movq	_ZZN2v88internalL33Stats_Runtime_ThrowReferenceErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic183(%rip), %rbx
	testq	%rbx, %rbx
	je	.L372
.L344:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L373
.L346:
	movq	%r13, %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$177, %esi
	addl	$1, 41104(%r12)
	movq	%r12, %rdi
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	call	_ZN2v88internal7Factory17NewReferenceErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	41096(%r12), %rbx
	je	.L350
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L350:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L374
.L341:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L375
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L372:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L376
.L345:
	movq	%rbx, _ZZN2v88internalL33Stats_Runtime_ThrowReferenceErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic183(%rip)
	jmp	.L344
	.p2align 4,,10
	.p2align 3
.L373:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L377
.L347:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L348
	movq	(%rdi), %rax
	call	*8(%rax)
.L348:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L349
	movq	(%rdi), %rax
	call	*8(%rax)
.L349:
	leaq	.LC11(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L371:
	movq	40960(%rsi), %rax
	movl	$374, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L374:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L377:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC11(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L376:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L345
.L375:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26519:
	.size	_ZN2v88internalL33Stats_Runtime_ThrowReferenceErrorEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL33Stats_Runtime_ThrowReferenceErrorEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL48Stats_Runtime_ThrowAccessedUninitializedVariableEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC12:
	.string	"V8.Runtime_Runtime_ThrowAccessedUninitializedVariable"
	.section	.text._ZN2v88internalL48Stats_Runtime_ThrowAccessedUninitializedVariableEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL48Stats_Runtime_ThrowAccessedUninitializedVariableEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL48Stats_Runtime_ThrowAccessedUninitializedVariableEiPmPNS0_7IsolateE.isra.0:
.LFB26520:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L408
.L379:
	movq	_ZZN2v88internalL48Stats_Runtime_ThrowAccessedUninitializedVariableEiPmPNS0_7IsolateEE28trace_event_unique_atomic191(%rip), %rbx
	testq	%rbx, %rbx
	je	.L409
.L381:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L410
.L383:
	movq	%r13, %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$179, %esi
	addl	$1, 41104(%r12)
	movq	%r12, %rdi
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	call	_ZN2v88internal7Factory17NewReferenceErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	41096(%r12), %rbx
	je	.L387
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L387:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L411
.L378:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L412
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L409:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L413
.L382:
	movq	%rbx, _ZZN2v88internalL48Stats_Runtime_ThrowAccessedUninitializedVariableEiPmPNS0_7IsolateEE28trace_event_unique_atomic191(%rip)
	jmp	.L381
	.p2align 4,,10
	.p2align 3
.L410:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L414
.L384:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L385
	movq	(%rdi), %rax
	call	*8(%rax)
.L385:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L386
	movq	(%rdi), %rax
	call	*8(%rax)
.L386:
	leaq	.LC12(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L408:
	movq	40960(%rsi), %rax
	movl	$375, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L379
	.p2align 4,,10
	.p2align 3
.L411:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L414:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC12(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L384
	.p2align 4,,10
	.p2align 3
.L413:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L382
.L412:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26520:
	.size	_ZN2v88internalL48Stats_Runtime_ThrowAccessedUninitializedVariableEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL48Stats_Runtime_ThrowAccessedUninitializedVariableEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL26Stats_Runtime_NewTypeErrorEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC13:
	.string	"V8.Runtime_Runtime_NewTypeError"
	.section	.rodata._ZN2v88internalL26Stats_Runtime_NewTypeErrorEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC14:
	.string	"args[0].IsNumber()"
.LC15:
	.string	"Check failed: %s."
	.section	.rodata._ZN2v88internalL26Stats_Runtime_NewTypeErrorEiPmPNS0_7IsolateE.isra.0.str1.8
	.align 8
.LC16:
	.string	"args[0].ToInt32(&template_index)"
	.section	.text._ZN2v88internalL26Stats_Runtime_NewTypeErrorEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL26Stats_Runtime_NewTypeErrorEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL26Stats_Runtime_NewTypeErrorEiPmPNS0_7IsolateE.isra.0:
.LFB26521:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L449
.L416:
	movq	_ZZN2v88internalL26Stats_Runtime_NewTypeErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic200(%rip), %r13
	testq	%r13, %r13
	je	.L450
.L418:
	movq	$0, -160(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L451
.L420:
	movq	41088(%r12), %r14
	movq	41096(%r12), %r13
	addl	$1, 41104(%r12)
	movq	(%rbx), %rax
	testb	$1, %al
	jne	.L452
.L425:
	leaq	-172(%rbp), %rsi
	leaq	-168(%rbp), %rdi
	movl	$0, -172(%rbp)
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal6Object7ToInt32EPi@PLT
	testb	%al, %al
	je	.L453
	movl	-172(%rbp), %esi
	leaq	-8(%rbx), %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	movq	(%rax), %r15
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r13
	je	.L428
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L428:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L454
.L415:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L455
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L452:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	je	.L425
	leaq	.LC14(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L451:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L456
.L421:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L422
	movq	(%rdi), %rax
	call	*8(%rax)
.L422:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L423
	movq	(%rdi), %rax
	call	*8(%rax)
.L423:
	leaq	.LC13(%rip), %rax
	movq	%r13, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L450:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L457
.L419:
	movq	%r13, _ZZN2v88internalL26Stats_Runtime_NewTypeErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic200(%rip)
	jmp	.L418
	.p2align 4,,10
	.p2align 3
.L449:
	movq	40960(%rsi), %rax
	movl	$354, %edx
	leaq	-120(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L416
	.p2align 4,,10
	.p2align 3
.L453:
	leaq	.LC16(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L454:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L415
	.p2align 4,,10
	.p2align 3
.L457:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L419
	.p2align 4,,10
	.p2align 3
.L456:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC13(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L421
.L455:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26521:
	.size	_ZN2v88internalL26Stats_Runtime_NewTypeErrorEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL26Stats_Runtime_NewTypeErrorEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL31Stats_Runtime_NewReferenceErrorEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC17:
	.string	"V8.Runtime_Runtime_NewReferenceError"
	.section	.text._ZN2v88internalL31Stats_Runtime_NewReferenceErrorEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL31Stats_Runtime_NewReferenceErrorEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL31Stats_Runtime_NewReferenceErrorEiPmPNS0_7IsolateE.isra.0:
.LFB26522:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L492
.L459:
	movq	_ZZN2v88internalL31Stats_Runtime_NewReferenceErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic209(%rip), %r13
	testq	%r13, %r13
	je	.L493
.L461:
	movq	$0, -160(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L494
.L463:
	movq	41088(%r12), %r14
	movq	41096(%r12), %r13
	addl	$1, 41104(%r12)
	movq	(%rbx), %rax
	testb	$1, %al
	jne	.L495
.L468:
	leaq	-172(%rbp), %rsi
	leaq	-168(%rbp), %rdi
	movl	$0, -172(%rbp)
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal6Object7ToInt32EPi@PLT
	testb	%al, %al
	je	.L496
	movl	-172(%rbp), %esi
	leaq	-8(%rbx), %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory17NewReferenceErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	movq	(%rax), %r15
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r13
	je	.L471
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L471:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L497
.L458:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L498
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L495:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	je	.L468
	leaq	.LC14(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L494:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L499
.L464:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L465
	movq	(%rdi), %rax
	call	*8(%rax)
.L465:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L466
	movq	(%rdi), %rax
	call	*8(%rax)
.L466:
	leaq	.LC17(%rip), %rax
	movq	%r13, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L463
	.p2align 4,,10
	.p2align 3
.L493:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L500
.L462:
	movq	%r13, _ZZN2v88internalL31Stats_Runtime_NewReferenceErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic209(%rip)
	jmp	.L461
	.p2align 4,,10
	.p2align 3
.L492:
	movq	40960(%rsi), %rax
	movl	$352, %edx
	leaq	-120(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L459
	.p2align 4,,10
	.p2align 3
.L496:
	leaq	.LC16(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L497:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L458
	.p2align 4,,10
	.p2align 3
.L500:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L462
	.p2align 4,,10
	.p2align 3
.L499:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC17(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L464
.L498:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26522:
	.size	_ZN2v88internalL31Stats_Runtime_NewReferenceErrorEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL31Stats_Runtime_NewReferenceErrorEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL28Stats_Runtime_NewSyntaxErrorEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC18:
	.string	"V8.Runtime_Runtime_NewSyntaxError"
	.section	.text._ZN2v88internalL28Stats_Runtime_NewSyntaxErrorEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL28Stats_Runtime_NewSyntaxErrorEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL28Stats_Runtime_NewSyntaxErrorEiPmPNS0_7IsolateE.isra.0:
.LFB26523:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L535
.L502:
	movq	_ZZN2v88internalL28Stats_Runtime_NewSyntaxErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic218(%rip), %r13
	testq	%r13, %r13
	je	.L536
.L504:
	movq	$0, -160(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L537
.L506:
	movq	41088(%r12), %r14
	movq	41096(%r12), %r13
	addl	$1, 41104(%r12)
	movq	(%rbx), %rax
	testb	$1, %al
	jne	.L538
.L511:
	leaq	-172(%rbp), %rsi
	leaq	-168(%rbp), %rdi
	movl	$0, -172(%rbp)
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal6Object7ToInt32EPi@PLT
	testb	%al, %al
	je	.L539
	movl	-172(%rbp), %esi
	leaq	-8(%rbx), %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory14NewSyntaxErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	movq	(%rax), %r15
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r13
	je	.L514
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L514:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L540
.L501:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L541
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L538:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	je	.L511
	leaq	.LC14(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L537:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L542
.L507:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L508
	movq	(%rdi), %rax
	call	*8(%rax)
.L508:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L509
	movq	(%rdi), %rax
	call	*8(%rax)
.L509:
	leaq	.LC18(%rip), %rax
	movq	%r13, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L506
	.p2align 4,,10
	.p2align 3
.L536:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L543
.L505:
	movq	%r13, _ZZN2v88internalL28Stats_Runtime_NewSyntaxErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic218(%rip)
	jmp	.L504
	.p2align 4,,10
	.p2align 3
.L535:
	movq	40960(%rsi), %rax
	movl	$353, %edx
	leaq	-120(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L502
	.p2align 4,,10
	.p2align 3
.L539:
	leaq	.LC16(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L540:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L501
	.p2align 4,,10
	.p2align 3
.L543:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L505
	.p2align 4,,10
	.p2align 3
.L542:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC18(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L507
.L541:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26523:
	.size	_ZN2v88internalL28Stats_Runtime_NewSyntaxErrorEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL28Stats_Runtime_NewSyntaxErrorEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL38Stats_Runtime_ThrowInvalidStringLengthEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC19:
	.string	"V8.Runtime_Runtime_ThrowInvalidStringLength"
	.section	.text._ZN2v88internalL38Stats_Runtime_ThrowInvalidStringLengthEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL38Stats_Runtime_ThrowInvalidStringLengthEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL38Stats_Runtime_ThrowInvalidStringLengthEiPmPNS0_7IsolateE.isra.0:
.LFB26524:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L574
.L545:
	movq	_ZZN2v88internalL38Stats_Runtime_ThrowInvalidStringLengthEiPmPNS0_7IsolateEE28trace_event_unique_atomic227(%rip), %rbx
	testq	%rbx, %rbx
	je	.L575
.L547:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L576
.L549:
	addl	$1, 41104(%r12)
	movq	%r12, %rdi
	movq	41088(%r12), %r13
	movq	41096(%r12), %rbx
	call	_ZN2v88internal7Factory27NewInvalidStringLengthErrorEv@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r14
	cmpq	41096(%r12), %rbx
	je	.L553
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L553:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L577
.L544:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L578
	leaq	-32(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L575:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L579
.L548:
	movq	%rbx, _ZZN2v88internalL38Stats_Runtime_ThrowInvalidStringLengthEiPmPNS0_7IsolateEE28trace_event_unique_atomic227(%rip)
	jmp	.L547
	.p2align 4,,10
	.p2align 3
.L576:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L580
.L550:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L551
	movq	(%rdi), %rax
	call	*8(%rax)
.L551:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L552
	movq	(%rdi), %rax
	call	*8(%rax)
.L552:
	leaq	.LC19(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L549
	.p2align 4,,10
	.p2align 3
.L577:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L544
	.p2align 4,,10
	.p2align 3
.L574:
	movq	40960(%rdi), %rax
	leaq	-104(%rbp), %rsi
	movl	$367, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L545
	.p2align 4,,10
	.p2align 3
.L580:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC19(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L550
	.p2align 4,,10
	.p2align 3
.L579:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L548
.L578:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26524:
	.size	_ZN2v88internalL38Stats_Runtime_ThrowInvalidStringLengthEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL38Stats_Runtime_ThrowInvalidStringLengthEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL44Stats_Runtime_ThrowIteratorResultNotAnObjectEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC20:
	.string	"V8.Runtime_Runtime_ThrowIteratorResultNotAnObject"
	.section	.text._ZN2v88internalL44Stats_Runtime_ThrowIteratorResultNotAnObjectEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL44Stats_Runtime_ThrowIteratorResultNotAnObjectEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL44Stats_Runtime_ThrowIteratorResultNotAnObjectEiPmPNS0_7IsolateE.isra.0:
.LFB26525:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L611
.L582:
	movq	_ZZN2v88internalL44Stats_Runtime_ThrowIteratorResultNotAnObjectEiPmPNS0_7IsolateEE28trace_event_unique_atomic232(%rip), %rbx
	testq	%rbx, %rbx
	je	.L612
.L584:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L613
.L586:
	movq	%r13, %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$68, %esi
	addl	$1, 41104(%r12)
	movq	%r12, %rdi
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	41096(%r12), %rbx
	je	.L590
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L590:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L614
.L581:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L615
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L612:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L616
.L585:
	movq	%rbx, _ZZN2v88internalL44Stats_Runtime_ThrowIteratorResultNotAnObjectEiPmPNS0_7IsolateEE28trace_event_unique_atomic232(%rip)
	jmp	.L584
	.p2align 4,,10
	.p2align 3
.L613:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L617
.L587:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L588
	movq	(%rdi), %rax
	call	*8(%rax)
.L588:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L589
	movq	(%rdi), %rax
	call	*8(%rax)
.L589:
	leaq	.LC20(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L586
	.p2align 4,,10
	.p2align 3
.L611:
	movq	40960(%rsi), %rax
	movl	$370, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L582
	.p2align 4,,10
	.p2align 3
.L614:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L581
	.p2align 4,,10
	.p2align 3
.L617:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC20(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L616:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L585
.L615:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26525:
	.size	_ZN2v88internalL44Stats_Runtime_ThrowIteratorResultNotAnObjectEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL44Stats_Runtime_ThrowIteratorResultNotAnObjectEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL37Stats_Runtime_ThrowThrowMethodMissingEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC21:
	.string	"V8.Runtime_Runtime_ThrowThrowMethodMissing"
	.section	.text._ZN2v88internalL37Stats_Runtime_ThrowThrowMethodMissingEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL37Stats_Runtime_ThrowThrowMethodMissingEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL37Stats_Runtime_ThrowThrowMethodMissingEiPmPNS0_7IsolateE.isra.0:
.LFB26526:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L648
.L619:
	movq	_ZZN2v88internalL37Stats_Runtime_ThrowThrowMethodMissingEiPmPNS0_7IsolateEE28trace_event_unique_atomic241(%rip), %rbx
	testq	%rbx, %rbx
	je	.L649
.L621:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L650
.L623:
	addl	$1, 41104(%r12)
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$172, %esi
	movq	%r12, %rdi
	movq	41088(%r12), %r13
	movq	41096(%r12), %rbx
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r14
	cmpq	41096(%r12), %rbx
	je	.L627
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L627:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L651
.L618:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L652
	leaq	-32(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L649:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L653
.L622:
	movq	%rbx, _ZZN2v88internalL37Stats_Runtime_ThrowThrowMethodMissingEiPmPNS0_7IsolateEE28trace_event_unique_atomic241(%rip)
	jmp	.L621
	.p2align 4,,10
	.p2align 3
.L650:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L654
.L624:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L625
	movq	(%rdi), %rax
	call	*8(%rax)
.L625:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L626
	movq	(%rdi), %rax
	call	*8(%rax)
.L626:
	leaq	.LC21(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L623
	.p2align 4,,10
	.p2align 3
.L651:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L618
	.p2align 4,,10
	.p2align 3
.L648:
	movq	40960(%rdi), %rax
	leaq	-104(%rbp), %rsi
	movl	$379, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L619
	.p2align 4,,10
	.p2align 3
.L654:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC21(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L624
	.p2align 4,,10
	.p2align 3
.L653:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L622
.L652:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26526:
	.size	_ZN2v88internalL37Stats_Runtime_ThrowThrowMethodMissingEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL37Stats_Runtime_ThrowThrowMethodMissingEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL40Stats_Runtime_ThrowSymbolIteratorInvalidEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC22:
	.string	"V8.Runtime_Runtime_ThrowSymbolIteratorInvalid"
	.section	.text._ZN2v88internalL40Stats_Runtime_ThrowSymbolIteratorInvalidEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL40Stats_Runtime_ThrowSymbolIteratorInvalidEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL40Stats_Runtime_ThrowSymbolIteratorInvalidEiPmPNS0_7IsolateE.isra.0:
.LFB26527:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L685
.L656:
	movq	_ZZN2v88internalL40Stats_Runtime_ThrowSymbolIteratorInvalidEiPmPNS0_7IsolateEE28trace_event_unique_atomic248(%rip), %rbx
	testq	%rbx, %rbx
	je	.L686
.L658:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L687
.L660:
	addl	$1, 41104(%r12)
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$167, %esi
	movq	%r12, %rdi
	movq	41088(%r12), %r13
	movq	41096(%r12), %rbx
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r14
	cmpq	41096(%r12), %rbx
	je	.L664
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L664:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L688
.L655:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L689
	leaq	-32(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L686:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L690
.L659:
	movq	%rbx, _ZZN2v88internalL40Stats_Runtime_ThrowSymbolIteratorInvalidEiPmPNS0_7IsolateEE28trace_event_unique_atomic248(%rip)
	jmp	.L658
	.p2align 4,,10
	.p2align 3
.L687:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L691
.L661:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L662
	movq	(%rdi), %rax
	call	*8(%rax)
.L662:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L663
	movq	(%rdi), %rax
	call	*8(%rax)
.L663:
	leaq	.LC22(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L660
	.p2align 4,,10
	.p2align 3
.L688:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L655
	.p2align 4,,10
	.p2align 3
.L685:
	movq	40960(%rdi), %rax
	leaq	-104(%rbp), %rsi
	movl	$378, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L656
	.p2align 4,,10
	.p2align 3
.L691:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC22(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L661
	.p2align 4,,10
	.p2align 3
.L690:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L659
.L689:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26527:
	.size	_ZN2v88internalL40Stats_Runtime_ThrowSymbolIteratorInvalidEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL40Stats_Runtime_ThrowSymbolIteratorInvalidEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL33Stats_Runtime_ThrowNotConstructorEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC23:
	.string	"V8.Runtime_Runtime_ThrowNotConstructor"
	.section	.text._ZN2v88internalL33Stats_Runtime_ThrowNotConstructorEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL33Stats_Runtime_ThrowNotConstructorEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL33Stats_Runtime_ThrowNotConstructorEiPmPNS0_7IsolateE.isra.0:
.LFB26528:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L722
.L693:
	movq	_ZZN2v88internalL33Stats_Runtime_ThrowNotConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic255(%rip), %rbx
	testq	%rbx, %rbx
	je	.L723
.L695:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L724
.L697:
	movq	%r13, %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$90, %esi
	addl	$1, 41104(%r12)
	movq	%r12, %rdi
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	41096(%r12), %rbx
	je	.L701
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L701:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L725
.L692:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L726
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L723:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L727
.L696:
	movq	%rbx, _ZZN2v88internalL33Stats_Runtime_ThrowNotConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic255(%rip)
	jmp	.L695
	.p2align 4,,10
	.p2align 3
.L724:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L728
.L698:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L699
	movq	(%rdi), %rax
	call	*8(%rax)
.L699:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L700
	movq	(%rdi), %rax
	call	*8(%rax)
.L700:
	leaq	.LC23(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L697
	.p2align 4,,10
	.p2align 3
.L722:
	movq	40960(%rsi), %rax
	movl	$371, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L693
	.p2align 4,,10
	.p2align 3
.L725:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L692
	.p2align 4,,10
	.p2align 3
.L728:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC23(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L698
	.p2align 4,,10
	.p2align 3
.L727:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L696
.L726:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26528:
	.size	_ZN2v88internalL33Stats_Runtime_ThrowNotConstructorEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL33Stats_Runtime_ThrowNotConstructorEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL32Stats_Runtime_ThrowIteratorErrorEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC24:
	.string	"V8.Runtime_Runtime_ThrowIteratorError"
	.section	.text._ZN2v88internalL32Stats_Runtime_ThrowIteratorErrorEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL32Stats_Runtime_ThrowIteratorErrorEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL32Stats_Runtime_ThrowIteratorErrorEiPmPNS0_7IsolateE.isra.0:
.LFB26529:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L759
.L730:
	movq	_ZZN2v88internalL32Stats_Runtime_ThrowIteratorErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic379(%rip), %rbx
	testq	%rbx, %rbx
	je	.L760
.L732:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L761
.L734:
	addl	$1, 41104(%r12)
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	call	_ZN2v88internal10ErrorUtils16NewIteratorErrorEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	41096(%r12), %rbx
	je	.L738
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L738:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L762
.L729:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L763
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L760:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L764
.L733:
	movq	%rbx, _ZZN2v88internalL32Stats_Runtime_ThrowIteratorErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic379(%rip)
	jmp	.L732
	.p2align 4,,10
	.p2align 3
.L761:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L765
.L735:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L736
	movq	(%rdi), %rax
	call	*8(%rax)
.L736:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L737
	movq	(%rdi), %rax
	call	*8(%rax)
.L737:
	leaq	.LC24(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L734
	.p2align 4,,10
	.p2align 3
.L759:
	movq	40960(%rsi), %rax
	movl	$369, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L730
	.p2align 4,,10
	.p2align 3
.L762:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L729
	.p2align 4,,10
	.p2align 3
.L765:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC24(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L735
	.p2align 4,,10
	.p2align 3
.L764:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L733
.L763:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26529:
	.size	_ZN2v88internalL32Stats_Runtime_ThrowIteratorErrorEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL32Stats_Runtime_ThrowIteratorErrorEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL36Stats_Runtime_ThrowCalledNonCallableEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC25:
	.string	"V8.Runtime_Runtime_ThrowCalledNonCallable"
	.section	.text._ZN2v88internalL36Stats_Runtime_ThrowCalledNonCallableEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL36Stats_Runtime_ThrowCalledNonCallableEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL36Stats_Runtime_ThrowCalledNonCallableEiPmPNS0_7IsolateE.isra.0:
.LFB26530:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L796
.L767:
	movq	_ZZN2v88internalL36Stats_Runtime_ThrowCalledNonCallableEiPmPNS0_7IsolateEE28trace_event_unique_atomic386(%rip), %rbx
	testq	%rbx, %rbx
	je	.L797
.L769:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L798
.L771:
	addl	$1, 41104(%r12)
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	call	_ZN2v88internal10ErrorUtils25NewCalledNonCallableErrorEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	41096(%r12), %rbx
	je	.L775
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L775:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L799
.L766:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L800
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L797:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L801
.L770:
	movq	%rbx, _ZZN2v88internalL36Stats_Runtime_ThrowCalledNonCallableEiPmPNS0_7IsolateEE28trace_event_unique_atomic386(%rip)
	jmp	.L769
	.p2align 4,,10
	.p2align 3
.L798:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L802
.L772:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L773
	movq	(%rdi), %rax
	call	*8(%rax)
.L773:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L774
	movq	(%rdi), %rax
	call	*8(%rax)
.L774:
	leaq	.LC25(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L771
	.p2align 4,,10
	.p2align 3
.L796:
	movq	40960(%rsi), %rax
	movl	$364, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L767
	.p2align 4,,10
	.p2align 3
.L799:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L766
	.p2align 4,,10
	.p2align 3
.L802:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC25(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L772
	.p2align 4,,10
	.p2align 3
.L801:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L770
.L800:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26530:
	.size	_ZN2v88internalL36Stats_Runtime_ThrowCalledNonCallableEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL36Stats_Runtime_ThrowCalledNonCallableEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL46Stats_Runtime_ThrowConstructedNonConstructableEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC26:
	.string	"V8.Runtime_Runtime_ThrowConstructedNonConstructable"
	.section	.text._ZN2v88internalL46Stats_Runtime_ThrowConstructedNonConstructableEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL46Stats_Runtime_ThrowConstructedNonConstructableEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL46Stats_Runtime_ThrowConstructedNonConstructableEiPmPNS0_7IsolateE.isra.0:
.LFB26531:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L833
.L804:
	movq	_ZZN2v88internalL46Stats_Runtime_ThrowConstructedNonConstructableEiPmPNS0_7IsolateEE28trace_event_unique_atomic394(%rip), %rbx
	testq	%rbx, %rbx
	je	.L834
.L806:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L835
.L808:
	addl	$1, 41104(%r12)
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	call	_ZN2v88internal10ErrorUtils30NewConstructedNonConstructableEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	41096(%r12), %rbx
	je	.L812
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L812:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L836
.L803:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L837
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L834:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L838
.L807:
	movq	%rbx, _ZZN2v88internalL46Stats_Runtime_ThrowConstructedNonConstructableEiPmPNS0_7IsolateEE28trace_event_unique_atomic394(%rip)
	jmp	.L806
	.p2align 4,,10
	.p2align 3
.L835:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L839
.L809:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L810
	movq	(%rdi), %rax
	call	*8(%rax)
.L810:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L811
	movq	(%rdi), %rax
	call	*8(%rax)
.L811:
	leaq	.LC26(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L808
	.p2align 4,,10
	.p2align 3
.L833:
	movq	40960(%rsi), %rax
	movl	$365, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L804
	.p2align 4,,10
	.p2align 3
.L836:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L803
	.p2align 4,,10
	.p2align 3
.L839:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC26(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L809
	.p2align 4,,10
	.p2align 3
.L838:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L807
.L837:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26531:
	.size	_ZN2v88internalL46Stats_Runtime_ThrowConstructedNonConstructableEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL46Stats_Runtime_ThrowConstructedNonConstructableEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL47Stats_Runtime_ThrowConstructorReturnedNonObjectEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC27:
	.string	"V8.Runtime_Runtime_ThrowConstructorReturnedNonObject"
	.section	.text._ZN2v88internalL47Stats_Runtime_ThrowConstructorReturnedNonObjectEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL47Stats_Runtime_ThrowConstructorReturnedNonObjectEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL47Stats_Runtime_ThrowConstructorReturnedNonObjectEiPmPNS0_7IsolateE.isra.0:
.LFB26532:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L870
.L841:
	movq	_ZZN2v88internalL47Stats_Runtime_ThrowConstructorReturnedNonObjectEiPmPNS0_7IsolateEE28trace_event_unique_atomic410(%rip), %rbx
	testq	%rbx, %rbx
	je	.L871
.L843:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L872
.L845:
	addl	$1, 41104(%r12)
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$230, %esi
	movq	%r12, %rdi
	movq	41088(%r12), %r13
	movq	41096(%r12), %rbx
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r14
	cmpq	41096(%r12), %rbx
	je	.L849
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L849:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L873
.L840:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L874
	leaq	-32(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L871:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L875
.L844:
	movq	%rbx, _ZZN2v88internalL47Stats_Runtime_ThrowConstructorReturnedNonObjectEiPmPNS0_7IsolateEE28trace_event_unique_atomic410(%rip)
	jmp	.L843
	.p2align 4,,10
	.p2align 3
.L872:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L876
.L846:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L847
	movq	(%rdi), %rax
	call	*8(%rax)
.L847:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L848
	movq	(%rdi), %rax
	call	*8(%rax)
.L848:
	leaq	.LC27(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L845
	.p2align 4,,10
	.p2align 3
.L873:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L840
	.p2align 4,,10
	.p2align 3
.L870:
	movq	40960(%rdi), %rax
	leaq	-104(%rbp), %rsi
	movl	$366, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L841
	.p2align 4,,10
	.p2align 3
.L876:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC27(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L846
	.p2align 4,,10
	.p2align 3
.L875:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L844
.L874:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26532:
	.size	_ZN2v88internalL47Stats_Runtime_ThrowConstructorReturnedNonObjectEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL47Stats_Runtime_ThrowConstructorReturnedNonObjectEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL29Stats_Runtime_ThrowRangeErrorEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC28:
	.string	"V8.Runtime_Runtime_ThrowRangeError"
	.section	.rodata._ZN2v88internalL29Stats_Runtime_ThrowRangeErrorEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC29:
	.string	"args[0].IsSmi()"
	.section	.rodata._ZN2v88internalL29Stats_Runtime_ThrowRangeErrorEiPmPNS0_7IsolateE.str1.8
	.align 8
.LC30:
	.string	"Aborting on invalid BigInt length"
	.section	.text._ZN2v88internalL29Stats_Runtime_ThrowRangeErrorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL29Stats_Runtime_ThrowRangeErrorEiPmPNS0_7IsolateE, @function
_ZN2v88internalL29Stats_Runtime_ThrowRangeErrorEiPmPNS0_7IsolateE:
.LFB21169:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L919
.L878:
	movq	_ZZN2v88internalL29Stats_Runtime_ThrowRangeErrorEiPmPNS0_7IsolateEE27trace_event_unique_atomic98(%rip), %rbx
	testq	%rbx, %rbx
	je	.L920
.L880:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L921
.L882:
	cmpb	$0, _ZN2v88internal36FLAG_correctness_fuzzer_suppressionsE(%rip)
	je	.L886
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L888
	sarq	$32, %rax
	cmpq	$183, %rax
	je	.L922
.L886:
	movq	41088(%r12), %r15
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rsi
	testb	$1, %sil
	jne	.L888
	leaq	88(%r12), %r8
	sarq	$32, %rsi
	movq	%r8, %rdx
	cmpl	$1, %r14d
	jg	.L923
.L891:
	movq	%r8, %rcx
.L893:
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory13NewRangeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%r15, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	41096(%r12), %rbx
	je	.L896
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L896:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L924
.L877:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L925
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L923:
	.cfi_restore_state
	leaq	-8(%r13), %rdx
	cmpl	$2, %r14d
	je	.L891
	leaq	-16(%r13), %rcx
	cmpl	$3, %r14d
	je	.L893
	leaq	-24(%r13), %r8
	jmp	.L893
	.p2align 4,,10
	.p2align 3
.L921:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L926
.L883:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L884
	movq	(%rdi), %rax
	call	*8(%rax)
.L884:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L885
	movq	(%rdi), %rax
	call	*8(%rax)
.L885:
	leaq	.LC28(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L882
	.p2align 4,,10
	.p2align 3
.L920:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L927
.L881:
	movq	%rbx, _ZZN2v88internalL29Stats_Runtime_ThrowRangeErrorEiPmPNS0_7IsolateEE27trace_event_unique_atomic98(%rip)
	jmp	.L880
	.p2align 4,,10
	.p2align 3
.L888:
	leaq	.LC29(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L919:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$373, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L878
	.p2align 4,,10
	.p2align 3
.L924:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L877
	.p2align 4,,10
	.p2align 3
.L927:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L881
	.p2align 4,,10
	.p2align 3
.L926:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC28(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L883
.L922:
	leaq	.LC30(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L925:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21169:
	.size	_ZN2v88internalL29Stats_Runtime_ThrowRangeErrorEiPmPNS0_7IsolateE, .-_ZN2v88internalL29Stats_Runtime_ThrowRangeErrorEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL28Stats_Runtime_ThrowTypeErrorEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC31:
	.string	"V8.Runtime_Runtime_ThrowTypeError"
	.section	.text._ZN2v88internalL28Stats_Runtime_ThrowTypeErrorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL28Stats_Runtime_ThrowTypeErrorEiPmPNS0_7IsolateE, @function
_ZN2v88internalL28Stats_Runtime_ThrowTypeErrorEiPmPNS0_7IsolateE:
.LFB21172:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L965
.L929:
	movq	_ZZN2v88internalL28Stats_Runtime_ThrowTypeErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic118(%rip), %rbx
	testq	%rbx, %rbx
	je	.L966
.L931:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L967
.L933:
	movq	41088(%r12), %r15
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rsi
	testb	$1, %sil
	jne	.L968
	leaq	88(%r12), %r8
	sarq	$32, %rsi
	movq	%r8, %rdx
	cmpl	$1, %r14d
	jg	.L969
.L940:
	movq	%r8, %rcx
.L942:
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%r15, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	41096(%r12), %rbx
	je	.L945
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L945:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L970
.L928:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L971
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L969:
	.cfi_restore_state
	leaq	-8(%r13), %rdx
	cmpl	$2, %r14d
	je	.L940
	leaq	-16(%r13), %rcx
	cmpl	$3, %r14d
	je	.L942
	leaq	-24(%r13), %r8
	jmp	.L942
	.p2align 4,,10
	.p2align 3
.L967:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L972
.L934:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L935
	movq	(%rdi), %rax
	call	*8(%rax)
.L935:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L936
	movq	(%rdi), %rax
	call	*8(%rax)
.L936:
	leaq	.LC31(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L933
	.p2align 4,,10
	.p2align 3
.L966:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L973
.L932:
	movq	%rbx, _ZZN2v88internalL28Stats_Runtime_ThrowTypeErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic118(%rip)
	jmp	.L931
	.p2align 4,,10
	.p2align 3
.L968:
	leaq	.LC29(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L965:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$380, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L929
	.p2align 4,,10
	.p2align 3
.L970:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L928
	.p2align 4,,10
	.p2align 3
.L973:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L932
	.p2align 4,,10
	.p2align 3
.L972:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC31(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L934
.L971:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21172:
	.size	_ZN2v88internalL28Stats_Runtime_ThrowTypeErrorEiPmPNS0_7IsolateE, .-_ZN2v88internalL28Stats_Runtime_ThrowTypeErrorEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL35Stats_Runtime_ThrowApplyNonFunctionEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC32:
	.string	"V8.Runtime_Runtime_ThrowApplyNonFunction"
	.section	.text._ZN2v88internalL35Stats_Runtime_ThrowApplyNonFunctionEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL35Stats_Runtime_ThrowApplyNonFunctionEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL35Stats_Runtime_ThrowApplyNonFunctionEiPmPNS0_7IsolateE.isra.0:
.LFB26533:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1004
.L975:
	movq	_ZZN2v88internalL35Stats_Runtime_ThrowApplyNonFunctionEiPmPNS0_7IsolateEE28trace_event_unique_atomic263(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1005
.L977:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1006
.L979:
	addl	$1, 41104(%r12)
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	call	_ZN2v88internal6Object6TypeOfEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%r13, %rdx
	xorl	%r8d, %r8d
	movl	$11, %esi
	movq	%rax, %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	41096(%r12), %rbx
	je	.L983
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L983:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1007
.L974:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1008
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1005:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1009
.L978:
	movq	%rbx, _ZZN2v88internalL35Stats_Runtime_ThrowApplyNonFunctionEiPmPNS0_7IsolateEE28trace_event_unique_atomic263(%rip)
	jmp	.L977
	.p2align 4,,10
	.p2align 3
.L1006:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1010
.L980:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L981
	movq	(%rdi), %rax
	call	*8(%rax)
.L981:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L982
	movq	(%rdi), %rax
	call	*8(%rax)
.L982:
	leaq	.LC32(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L979
	.p2align 4,,10
	.p2align 3
.L1004:
	movq	40960(%rsi), %rax
	movl	$363, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L975
	.p2align 4,,10
	.p2align 3
.L1007:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L974
	.p2align 4,,10
	.p2align 3
.L1010:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC32(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L980
	.p2align 4,,10
	.p2align 3
.L1009:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L978
.L1008:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26533:
	.size	_ZN2v88internalL35Stats_Runtime_ThrowApplyNonFunctionEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL35Stats_Runtime_ThrowApplyNonFunctionEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL45Stats_Runtime_ThrowInvalidTypedArrayAlignmentEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC33:
	.string	"BigInt64Array"
.LC34:
	.string	"Uint8Array"
.LC35:
	.string	"Uint16Array"
.LC36:
	.string	"Int16Array"
.LC37:
	.string	"Uint32Array"
.LC38:
	.string	"Int32Array"
.LC39:
	.string	"Float32Array"
.LC40:
	.string	"Float64Array"
.LC41:
	.string	"Uint8ClampedArray"
.LC42:
	.string	"BigUint64Array"
.LC43:
	.string	"Int8Array"
	.section	.rodata._ZN2v88internalL45Stats_Runtime_ThrowInvalidTypedArrayAlignmentEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC44:
	.string	"V8.Runtime_Runtime_ThrowInvalidTypedArrayAlignment"
	.section	.rodata._ZN2v88internalL45Stats_Runtime_ThrowInvalidTypedArrayAlignmentEiPmPNS0_7IsolateE.isra.0.str1.1
.LC45:
	.string	"args[0].IsMap()"
.LC46:
	.string	"args[1].IsString()"
.LC47:
	.string	"unreachable code"
.LC48:
	.string	"(location_) != nullptr"
	.section	.text._ZN2v88internalL45Stats_Runtime_ThrowInvalidTypedArrayAlignmentEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL45Stats_Runtime_ThrowInvalidTypedArrayAlignmentEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL45Stats_Runtime_ThrowInvalidTypedArrayAlignmentEiPmPNS0_7IsolateE.isra.0:
.LFB26534:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1064
.L1012:
	movq	_ZZN2v88internalL45Stats_Runtime_ThrowInvalidTypedArrayAlignmentEiPmPNS0_7IsolateEE28trace_event_unique_atomic149(%rip), %r13
	testq	%r13, %r13
	je	.L1065
.L1014:
	movq	$0, -160(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L1066
.L1016:
	movq	41088(%r12), %rax
	movq	41096(%r12), %r13
	addl	$1, 41104(%r12)
	movq	%rax, -208(%rbp)
	movq	(%rbx), %rax
	testb	$1, %al
	jne	.L1020
.L1021:
	leaq	.LC45(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1065:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1067
.L1015:
	movq	%r13, _ZZN2v88internalL45Stats_Runtime_ThrowInvalidTypedArrayAlignmentEiPmPNS0_7IsolateEE28trace_event_unique_atomic149(%rip)
	jmp	.L1014
	.p2align 4,,10
	.p2align 3
.L1020:
	movq	-1(%rax), %rdx
	cmpw	$68, 11(%rdx)
	jne	.L1021
	movq	-8(%rbx), %rdx
	leaq	-8(%rbx), %r15
	testb	$1, %dl
	jne	.L1068
.L1022:
	leaq	.LC46(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1066:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1069
.L1017:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1018
	movq	(%rdi), %rax
	call	*8(%rax)
.L1018:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1019
	movq	(%rdi), %rax
	call	*8(%rax)
.L1019:
	leaq	.LC44(%rip), %rax
	movq	%r13, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1016
	.p2align 4,,10
	.p2align 3
.L1068:
	movq	-1(%rdx), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L1022
	movzbl	14(%rax), %r8d
	shrl	$3, %r8d
	leal	-17(%r8), %eax
	cmpb	$10, %al
	ja	.L1024
	leaq	.L1026(%rip), %rdx
	movzbl	%al, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internalL45Stats_Runtime_ThrowInvalidTypedArrayAlignmentEiPmPNS0_7IsolateE.isra.0,"a",@progbits
	.align 4
	.align 4
.L1026:
	.long	.L1036-.L1026
	.long	.L1035-.L1026
	.long	.L1047-.L1026
	.long	.L1033-.L1026
	.long	.L1032-.L1026
	.long	.L1031-.L1026
	.long	.L1030-.L1026
	.long	.L1029-.L1026
	.long	.L1028-.L1026
	.long	.L1027-.L1026
	.long	.L1025-.L1026
	.section	.text._ZN2v88internalL45Stats_Runtime_ThrowInvalidTypedArrayAlignmentEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L1027:
	movl	$14, %eax
	leaq	.LC42(%rip), %rdx
	.p2align 4,,10
	.p2align 3
.L1034:
	leaq	-176(%rbp), %r14
	movq	%rdx, -176(%rbp)
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r14, %rsi
	movl	%r8d, -200(%rbp)
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movl	-200(%rbp), %r8d
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L1070
	leaq	-180(%rbp), %rsi
	movl	%r8d, %edi
	movq	%r14, %rdx
	call	_ZN2v88internal7Factory26TypeAndSizeForElementsKindENS0_12ElementsKindEPNS0_17ExternalArrayTypeEPm@PLT
	movslq	-176(%rbp), %rsi
	movq	41112(%r12), %rdi
	salq	$32, %rsi
	testq	%rdi, %rdi
	je	.L1038
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r8
.L1039:
	movq	%r15, %rdx
	movq	%rbx, %rcx
	movl	$204, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory13NewRangeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	subl	$1, 41104(%r12)
	movq	%rax, %r15
	movq	-208(%rbp), %rax
	movq	%rax, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L1043
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1043:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1071
.L1011:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1072
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1028:
	.cfi_restore_state
	movl	$17, %eax
	leaq	.LC41(%rip), %rdx
	jmp	.L1034
	.p2align 4,,10
	.p2align 3
.L1047:
	movl	$11, %eax
	leaq	.LC35(%rip), %rdx
	jmp	.L1034
	.p2align 4,,10
	.p2align 3
.L1035:
	movl	$9, %eax
	leaq	.LC43(%rip), %rdx
	jmp	.L1034
	.p2align 4,,10
	.p2align 3
.L1036:
	movl	$10, %eax
	leaq	.LC34(%rip), %rdx
	jmp	.L1034
	.p2align 4,,10
	.p2align 3
.L1025:
	movl	$13, %eax
	leaq	.LC33(%rip), %rdx
	jmp	.L1034
	.p2align 4,,10
	.p2align 3
.L1030:
	movl	$12, %eax
	leaq	.LC39(%rip), %rdx
	jmp	.L1034
	.p2align 4,,10
	.p2align 3
.L1031:
	movl	$10, %eax
	leaq	.LC38(%rip), %rdx
	jmp	.L1034
	.p2align 4,,10
	.p2align 3
.L1032:
	movl	$11, %eax
	leaq	.LC37(%rip), %rdx
	jmp	.L1034
	.p2align 4,,10
	.p2align 3
.L1033:
	movl	$10, %eax
	leaq	.LC36(%rip), %rdx
	jmp	.L1034
	.p2align 4,,10
	.p2align 3
.L1029:
	movl	$12, %eax
	leaq	.LC40(%rip), %rdx
	jmp	.L1034
	.p2align 4,,10
	.p2align 3
.L1038:
	movq	41088(%r12), %r8
	cmpq	41096(%r12), %r8
	je	.L1073
.L1040:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r8)
	jmp	.L1039
	.p2align 4,,10
	.p2align 3
.L1064:
	movq	40960(%rsi), %rax
	movl	$368, %edx
	leaq	-120(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1012
	.p2align 4,,10
	.p2align 3
.L1070:
	leaq	.LC48(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1071:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1011
	.p2align 4,,10
	.p2align 3
.L1069:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC44(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1017
	.p2align 4,,10
	.p2align 3
.L1067:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L1015
	.p2align 4,,10
	.p2align 3
.L1073:
	movq	%r12, %rdi
	movq	%rsi, -200(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-200(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L1040
.L1024:
	leaq	.LC47(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1072:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26534:
	.size	_ZN2v88internalL45Stats_Runtime_ThrowInvalidTypedArrayAlignmentEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL45Stats_Runtime_ThrowInvalidTypedArrayAlignmentEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL39Stats_Runtime_AllocateInYoungGenerationEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC49:
	.string	"V8.Runtime_Runtime_AllocateInYoungGeneration"
	.section	.rodata._ZN2v88internalL39Stats_Runtime_AllocateInYoungGenerationEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC50:
	.string	"args[1].IsSmi()"
.LC51:
	.string	"IsAligned(size, kTaggedSize)"
.LC52:
	.string	"size > 0"
	.section	.rodata._ZN2v88internalL39Stats_Runtime_AllocateInYoungGenerationEiPmPNS0_7IsolateE.isra.0.str1.8
	.align 8
.LC53:
	.string	"FLAG_young_generation_large_objects || size <= kMaxRegularHeapObjectSize"
	.align 8
.LC54:
	.string	"size <= kMaxRegularHeapObjectSize"
	.section	.text._ZN2v88internalL39Stats_Runtime_AllocateInYoungGenerationEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL39Stats_Runtime_AllocateInYoungGenerationEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL39Stats_Runtime_AllocateInYoungGenerationEiPmPNS0_7IsolateE.isra.0:
.LFB26537:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1123
.L1075:
	movq	_ZZN2v88internalL39Stats_Runtime_AllocateInYoungGenerationEiPmPNS0_7IsolateEE28trace_event_unique_atomic306(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1124
.L1077:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1125
.L1079:
	movq	41088(%r12), %r13
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	(%r14), %rsi
	testb	$1, %sil
	jne	.L1126
	movq	-8(%r14), %rax
	sarq	$32, %rsi
	testb	$1, %al
	jne	.L1127
	sarq	$32, %rax
	andl	$2, %eax
	testb	$7, %sil
	jne	.L1128
	testl	%esi, %esi
	jle	.L1129
	cmpl	$131072, %esi
	setg	%dl
	cmpb	$1, _ZN2v88internal35FLAG_young_generation_large_objectsE(%rip)
	je	.L1087
	testb	%dl, %dl
	jne	.L1130
.L1087:
	testl	%eax, %eax
	jne	.L1088
	testb	%dl, %dl
	jne	.L1131
.L1088:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory15NewFillerObjectEibNS0_14AllocationTypeENS0_16AllocationOriginE@PLT
	movq	(%rax), %r14
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1091
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1091:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1132
.L1074:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1133
	leaq	-32(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1125:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1134
.L1080:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1081
	movq	(%rdi), %rax
	call	*8(%rax)
.L1081:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1082
	movq	(%rdi), %rax
	call	*8(%rax)
.L1082:
	leaq	.LC49(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1079
	.p2align 4,,10
	.p2align 3
.L1124:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1135
.L1078:
	movq	%rbx, _ZZN2v88internalL39Stats_Runtime_AllocateInYoungGenerationEiPmPNS0_7IsolateEE28trace_event_unique_atomic306(%rip)
	jmp	.L1077
	.p2align 4,,10
	.p2align 3
.L1126:
	leaq	.LC29(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1123:
	movq	40960(%rsi), %rax
	movl	$339, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1075
	.p2align 4,,10
	.p2align 3
.L1127:
	leaq	.LC50(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1128:
	leaq	.LC51(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1129:
	leaq	.LC52(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1135:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1078
	.p2align 4,,10
	.p2align 3
.L1134:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC49(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L1080
	.p2align 4,,10
	.p2align 3
.L1132:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1074
.L1130:
	leaq	.LC53(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1131:
	leaq	.LC54(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L1133:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26537:
	.size	_ZN2v88internalL39Stats_Runtime_AllocateInYoungGenerationEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL39Stats_Runtime_AllocateInYoungGenerationEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL37Stats_Runtime_AllocateInOldGenerationEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC55:
	.string	"V8.Runtime_Runtime_AllocateInOldGeneration"
	.section	.text._ZN2v88internalL37Stats_Runtime_AllocateInOldGenerationEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL37Stats_Runtime_AllocateInOldGenerationEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL37Stats_Runtime_AllocateInOldGenerationEiPmPNS0_7IsolateE.isra.0:
.LFB26538:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1178
.L1137:
	movq	_ZZN2v88internalL37Stats_Runtime_AllocateInOldGenerationEiPmPNS0_7IsolateEE28trace_event_unique_atomic331(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1179
.L1139:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1180
.L1141:
	movq	41088(%r12), %r13
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	(%r14), %rsi
	testb	$1, %sil
	jne	.L1181
	movq	-8(%r14), %rdx
	sarq	$32, %rsi
	testb	$1, %dl
	jne	.L1182
	sarq	$32, %rdx
	movl	%edx, %eax
	andl	$2, %eax
	testb	$7, %sil
	jne	.L1183
	testl	%esi, %esi
	jle	.L1184
	cmpl	$131072, %esi
	jle	.L1149
	testl	%eax, %eax
	je	.L1185
.L1149:
	andl	$1, %edx
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory15NewFillerObjectEibNS0_14AllocationTypeENS0_16AllocationOriginE@PLT
	movq	(%rax), %r14
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1152
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1152:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1186
.L1136:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1187
	leaq	-32(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1180:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1188
.L1142:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1143
	movq	(%rdi), %rax
	call	*8(%rax)
.L1143:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1144
	movq	(%rdi), %rax
	call	*8(%rax)
.L1144:
	leaq	.LC55(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1141
	.p2align 4,,10
	.p2align 3
.L1179:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1189
.L1140:
	movq	%rbx, _ZZN2v88internalL37Stats_Runtime_AllocateInOldGenerationEiPmPNS0_7IsolateEE28trace_event_unique_atomic331(%rip)
	jmp	.L1139
	.p2align 4,,10
	.p2align 3
.L1178:
	movq	40960(%rsi), %rax
	movl	$340, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1137
	.p2align 4,,10
	.p2align 3
.L1181:
	leaq	.LC29(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1182:
	leaq	.LC50(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1183:
	leaq	.LC51(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1184:
	leaq	.LC52(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1189:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1140
	.p2align 4,,10
	.p2align 3
.L1188:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC55(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L1142
	.p2align 4,,10
	.p2align 3
.L1186:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1136
	.p2align 4,,10
	.p2align 3
.L1185:
	leaq	.LC54(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L1187:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26538:
	.size	_ZN2v88internalL37Stats_Runtime_AllocateInOldGenerationEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL37Stats_Runtime_AllocateInOldGenerationEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL31Stats_Runtime_AllocateByteArrayEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC56:
	.string	"V8.Runtime_Runtime_AllocateByteArray"
	.section	.text._ZN2v88internalL31Stats_Runtime_AllocateByteArrayEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL31Stats_Runtime_AllocateByteArrayEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL31Stats_Runtime_AllocateByteArrayEiPmPNS0_7IsolateE.isra.0:
.LFB26539:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1221
.L1191:
	movq	_ZZN2v88internalL31Stats_Runtime_AllocateByteArrayEiPmPNS0_7IsolateEE28trace_event_unique_atomic349(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1222
.L1193:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1223
.L1195:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rsi
	testb	$1, %sil
	jne	.L1224
	sarq	$32, %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewByteArrayEiNS0_14AllocationTypeE@PLT
	movq	(%rax), %r13
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1200
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1200:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1225
.L1190:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1226
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1223:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1227
.L1196:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1197
	movq	(%rdi), %rax
	call	*8(%rax)
.L1197:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1198
	movq	(%rdi), %rax
	call	*8(%rax)
.L1198:
	leaq	.LC56(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1195
	.p2align 4,,10
	.p2align 3
.L1222:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1228
.L1194:
	movq	%rbx, _ZZN2v88internalL31Stats_Runtime_AllocateByteArrayEiPmPNS0_7IsolateEE28trace_event_unique_atomic349(%rip)
	jmp	.L1193
	.p2align 4,,10
	.p2align 3
.L1224:
	leaq	.LC29(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1221:
	movq	40960(%rsi), %rax
	movl	$338, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1191
	.p2align 4,,10
	.p2align 3
.L1225:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1190
	.p2align 4,,10
	.p2align 3
.L1228:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1194
	.p2align 4,,10
	.p2align 3
.L1227:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC56(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1196
.L1226:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26539:
	.size	_ZN2v88internalL31Stats_Runtime_AllocateByteArrayEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL31Stats_Runtime_AllocateByteArrayEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL31Stats_Runtime_GetTemplateObjectEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC57:
	.string	"V8.Runtime_Runtime_GetTemplateObject"
	.align 8
.LC58:
	.string	"args[0].IsTemplateObjectDescription()"
	.align 8
.LC59:
	.string	"args[1].IsSharedFunctionInfo()"
	.section	.rodata._ZN2v88internalL31Stats_Runtime_GetTemplateObjectEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC60:
	.string	"args[2].IsSmi()"
	.section	.text._ZN2v88internalL31Stats_Runtime_GetTemplateObjectEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL31Stats_Runtime_GetTemplateObjectEiPmPNS0_7IsolateE, @function
_ZN2v88internalL31Stats_Runtime_GetTemplateObjectEiPmPNS0_7IsolateE:
.LFB21278:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1268
.L1230:
	movq	_ZZN2v88internalL31Stats_Runtime_GetTemplateObjectEiPmPNS0_7IsolateEE28trace_event_unique_atomic535(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1269
.L1232:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1270
.L1234:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L1238
.L1239:
	leaq	.LC58(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1269:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1271
.L1233:
	movq	%rbx, _ZZN2v88internalL31Stats_Runtime_GetTemplateObjectEiPmPNS0_7IsolateEE28trace_event_unique_atomic535(%rip)
	jmp	.L1232
	.p2align 4,,10
	.p2align 3
.L1238:
	movq	-1(%rax), %rax
	cmpw	$101, 11(%rax)
	jne	.L1239
	movq	-8(%r13), %rax
	leaq	-8(%r13), %rcx
	testb	$1, %al
	jne	.L1272
.L1240:
	leaq	.LC59(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1270:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1273
.L1235:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1236
	movq	(%rdi), %rax
	call	*8(%rax)
.L1236:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1237
	movq	(%rdi), %rax
	call	*8(%rax)
.L1237:
	leaq	.LC57(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1234
	.p2align 4,,10
	.p2align 3
.L1272:
	movq	-1(%rax), %rax
	cmpw	$160, 11(%rax)
	jne	.L1240
	movq	-16(%r13), %r8
	testb	$1, %r8b
	jne	.L1274
	movq	12464(%r12), %rax
	movq	%r8, %r15
	sarq	$32, %r15
	movq	39(%rax), %r8
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1243
	movq	%r8, %rsi
	movq	%rcx, -168(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-168(%rbp), %rcx
	movq	%rax, %rsi
.L1244:
	movq	%r13, %rdx
	movl	%r15d, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal25TemplateObjectDescription17GetTemplateObjectEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEENS4_IS1_EENS4_INS0_18SharedFunctionInfoEEEi@PLT
	movq	(%rax), %r13
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1248
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1248:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1275
.L1229:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1276
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1243:
	.cfi_restore_state
	movq	%r14, %rsi
	cmpq	41096(%r12), %r14
	je	.L1277
.L1245:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r8, (%rsi)
	jmp	.L1244
	.p2align 4,,10
	.p2align 3
.L1268:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$349, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1230
	.p2align 4,,10
	.p2align 3
.L1274:
	leaq	.LC60(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1275:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1229
	.p2align 4,,10
	.p2align 3
.L1273:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC57(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1235
	.p2align 4,,10
	.p2align 3
.L1271:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1233
	.p2align 4,,10
	.p2align 3
.L1277:
	movq	%r12, %rdi
	movq	%r8, -176(%rbp)
	movq	%rcx, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-176(%rbp), %r8
	movq	-168(%rbp), %rcx
	movq	%rax, %rsi
	jmp	.L1245
.L1276:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21278:
	.size	_ZN2v88internalL31Stats_Runtime_GetTemplateObjectEiPmPNS0_7IsolateE, .-_ZN2v88internalL31Stats_Runtime_GetTemplateObjectEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL36Stats_Runtime_GetInitializerFunctionEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC61:
	.string	"V8.Runtime_Runtime_GetInitializerFunction"
	.section	.rodata._ZN2v88internalL36Stats_Runtime_GetInitializerFunctionEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC62:
	.string	"args[0].IsJSReceiver()"
	.section	.text._ZN2v88internalL36Stats_Runtime_GetInitializerFunctionEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL36Stats_Runtime_GetInitializerFunctionEiPmPNS0_7IsolateE, @function
_ZN2v88internalL36Stats_Runtime_GetInitializerFunctionEiPmPNS0_7IsolateE:
.LFB21284:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$200, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -208(%rbp)
	movq	$0, -176(%rbp)
	movaps	%xmm0, -192(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1317
.L1279:
	movq	_ZZN2v88internalL36Stats_Runtime_GetInitializerFunctionEiPmPNS0_7IsolateEE28trace_event_unique_atomic565(%rip), %r13
	testq	%r13, %r13
	je	.L1318
.L1281:
	movq	$0, -240(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L1319
.L1283:
	movq	41088(%r12), %r14
	movq	41096(%r12), %r13
	addl	$1, 41104(%r12)
	movq	(%rbx), %rdx
	testb	$1, %dl
	jne	.L1287
.L1288:
	leaq	.LC62(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1318:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1320
.L1282:
	movq	%r13, _ZZN2v88internalL36Stats_Runtime_GetInitializerFunctionEiPmPNS0_7IsolateEE28trace_event_unique_atomic565(%rip)
	jmp	.L1281
	.p2align 4,,10
	.p2align 3
.L1287:
	movq	-1(%rdx), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L1288
	movq	3648(%r12), %rax
	andq	$-262144, %rdx
	leaq	3648(%r12), %rsi
	movq	24(%rdx), %rdi
	movl	$2, %edx
	movq	-1(%rax), %rcx
	subq	$37592, %rdi
	cmpw	$64, 11(%rcx)
	jne	.L1289
	xorl	%edx, %edx
	testb	$1, 11(%rax)
	sete	%dl
	addl	%edx, %edx
.L1289:
	movabsq	$824633720832, %rax
	movl	%edx, -160(%rbp)
	movq	%rax, -148(%rbp)
	movq	3648(%r12), %rax
	movq	%rdi, -136(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %edx
	andl	$-32, %edx
	cmpl	$32, %edx
	je	.L1321
.L1290:
	leaq	-160(%rbp), %r15
	movq	%rsi, -128(%rbp)
	movq	%r15, %rdi
	movq	$0, -120(%rbp)
	movq	%rbx, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%rbx, -96(%rbp)
	movq	$-1, -88(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -156(%rbp)
	jne	.L1291
	movq	-136(%rbp), %rax
	addq	$88, %rax
.L1292:
	movq	(%rax), %r15
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r13
	je	.L1295
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1295:
	leaq	-240(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1322
.L1278:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1323
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1291:
	.cfi_restore_state
	movq	%r15, %rdi
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE@PLT
	jmp	.L1292
	.p2align 4,,10
	.p2align 3
.L1319:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1324
.L1284:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1285
	movq	(%rdi), %rax
	call	*8(%rax)
.L1285:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1286
	movq	(%rdi), %rax
	call	*8(%rax)
.L1286:
	leaq	.LC61(%rip), %rax
	movq	%r13, -232(%rbp)
	movq	%rax, -224(%rbp)
	leaq	-232(%rbp), %rax
	movq	%r14, -216(%rbp)
	movq	%rax, -240(%rbp)
	jmp	.L1283
	.p2align 4,,10
	.p2align 3
.L1321:
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rsi
	jmp	.L1290
	.p2align 4,,10
	.p2align 3
.L1317:
	movq	40960(%rdx), %rax
	leaq	-200(%rbp), %rsi
	movl	$573, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -208(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1279
	.p2align 4,,10
	.p2align 3
.L1322:
	leaq	-200(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1278
	.p2align 4,,10
	.p2align 3
.L1320:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L1282
	.p2align 4,,10
	.p2align 3
.L1324:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC61(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1284
.L1323:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21284:
	.size	_ZN2v88internalL36Stats_Runtime_GetInitializerFunctionEiPmPNS0_7IsolateE, .-_ZN2v88internalL36Stats_Runtime_GetInitializerFunctionEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL36Stats_Runtime_ThrowTypeErrorIfStrictEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC63:
	.string	"V8.Runtime_Runtime_ThrowTypeErrorIfStrict"
	.section	.text._ZN2v88internalL36Stats_Runtime_ThrowTypeErrorIfStrictEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL36Stats_Runtime_ThrowTypeErrorIfStrictEiPmPNS0_7IsolateE, @function
_ZN2v88internalL36Stats_Runtime_ThrowTypeErrorIfStrictEiPmPNS0_7IsolateE:
.LFB21175:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1364
.L1326:
	movq	_ZZN2v88internalL36Stats_Runtime_ThrowTypeErrorIfStrictEiPmPNS0_7IsolateEE28trace_event_unique_atomic122(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1365
.L1328:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1366
.L1330:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14GetShouldThrowEPNS0_7IsolateENS_5MaybeINS0_11ShouldThrowEEE@PLT
	cmpl	$1, %eax
	je	.L1367
	movq	41088(%r12), %r15
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rsi
	testb	$1, %sil
	jne	.L1368
	leaq	88(%r12), %r8
	sarq	$32, %rsi
	movq	%r8, %rdx
	cmpl	$1, %r14d
	jg	.L1369
.L1339:
	movq	%r8, %rcx
.L1341:
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%r15, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	41096(%r12), %rbx
	je	.L1335
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1335:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1370
.L1325:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1371
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1369:
	.cfi_restore_state
	leaq	-8(%r13), %rdx
	cmpl	$2, %r14d
	je	.L1339
	leaq	-16(%r13), %rcx
	cmpl	$3, %r14d
	je	.L1341
	leaq	-24(%r13), %r8
	jmp	.L1341
	.p2align 4,,10
	.p2align 3
.L1367:
	movq	88(%r12), %r13
	jmp	.L1335
	.p2align 4,,10
	.p2align 3
.L1366:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1372
.L1331:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1332
	movq	(%rdi), %rax
	call	*8(%rax)
.L1332:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1333
	movq	(%rdi), %rax
	call	*8(%rax)
.L1333:
	leaq	.LC63(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1330
	.p2align 4,,10
	.p2align 3
.L1365:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1373
.L1329:
	movq	%rbx, _ZZN2v88internalL36Stats_Runtime_ThrowTypeErrorIfStrictEiPmPNS0_7IsolateEE28trace_event_unique_atomic122(%rip)
	jmp	.L1328
	.p2align 4,,10
	.p2align 3
.L1364:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$381, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1326
	.p2align 4,,10
	.p2align 3
.L1370:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1325
	.p2align 4,,10
	.p2align 3
.L1368:
	leaq	.LC29(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1373:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1329
	.p2align 4,,10
	.p2align 3
.L1372:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC63(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L1331
.L1371:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21175:
	.size	_ZN2v88internalL36Stats_Runtime_ThrowTypeErrorIfStrictEiPmPNS0_7IsolateE, .-_ZN2v88internalL36Stats_Runtime_ThrowTypeErrorIfStrictEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL33Stats_Runtime_IncrementUseCounterEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC64:
	.string	"V8.Runtime_Runtime_IncrementUseCounter"
	.section	.text._ZN2v88internalL33Stats_Runtime_IncrementUseCounterEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL33Stats_Runtime_IncrementUseCounterEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL33Stats_Runtime_IncrementUseCounterEiPmPNS0_7IsolateE.isra.0:
.LFB26544:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1405
.L1375:
	movq	_ZZN2v88internalL33Stats_Runtime_IncrementUseCounterEiPmPNS0_7IsolateEE28trace_event_unique_atomic428(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1406
.L1377:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1407
.L1379:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rsi
	testb	$1, %sil
	jne	.L1408
	sarq	$32, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate10CountUsageENS_7Isolate17UseCounterFeatureE@PLT
	movq	%r14, 41088(%r12)
	movq	88(%r12), %r13
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1384
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1384:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1409
.L1374:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1410
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1407:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1411
.L1380:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1381
	movq	(%rdi), %rax
	call	*8(%rax)
.L1381:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1382
	movq	(%rdi), %rax
	call	*8(%rax)
.L1382:
	leaq	.LC64(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1379
	.p2align 4,,10
	.p2align 3
.L1406:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1412
.L1378:
	movq	%rbx, _ZZN2v88internalL33Stats_Runtime_IncrementUseCounterEiPmPNS0_7IsolateEE28trace_event_unique_atomic428(%rip)
	jmp	.L1377
	.p2align 4,,10
	.p2align 3
.L1408:
	leaq	.LC29(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1405:
	movq	40960(%rsi), %rax
	movl	$350, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1375
	.p2align 4,,10
	.p2align 3
.L1409:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1374
	.p2align 4,,10
	.p2align 3
.L1412:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1378
	.p2align 4,,10
	.p2align 3
.L1411:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC64(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1380
.L1410:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26544:
	.size	_ZN2v88internalL33Stats_Runtime_IncrementUseCounterEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL33Stats_Runtime_IncrementUseCounterEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL27Stats_Runtime_ReportMessageEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC65:
	.string	"V8.Runtime_Runtime_ReportMessage"
	.section	.text._ZN2v88internalL27Stats_Runtime_ReportMessageEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL27Stats_Runtime_ReportMessageEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL27Stats_Runtime_ReportMessageEiPmPNS0_7IsolateE.isra.0:
.LFB26545:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1443
.L1414:
	movq	_ZZN2v88internalL27Stats_Runtime_ReportMessageEiPmPNS0_7IsolateEE28trace_event_unique_atomic548(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1444
.L1416:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1445
.L1418:
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	movq	%r12, %rdi
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	movq	%rax, 12480(%r12)
	call	_ZN2v88internal7Isolate35ReportPendingMessagesFromJavaScriptEv@PLT
	movq	96(%r12), %rax
	movq	88(%r12), %r13
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, 12480(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1422
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1422:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1446
.L1413:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1447
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1444:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1448
.L1417:
	movq	%rbx, _ZZN2v88internalL27Stats_Runtime_ReportMessageEiPmPNS0_7IsolateEE28trace_event_unique_atomic548(%rip)
	jmp	.L1416
	.p2align 4,,10
	.p2align 3
.L1445:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1449
.L1419:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1420
	movq	(%rdi), %rax
	call	*8(%rax)
.L1420:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1421
	movq	(%rdi), %rax
	call	*8(%rax)
.L1421:
	leaq	.LC65(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1418
	.p2align 4,,10
	.p2align 3
.L1446:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1413
	.p2align 4,,10
	.p2align 3
.L1443:
	movq	40960(%rsi), %rax
	movl	$357, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1414
	.p2align 4,,10
	.p2align 3
.L1449:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC65(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1419
	.p2align 4,,10
	.p2align 3
.L1448:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1417
.L1447:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26545:
	.size	_ZN2v88internalL27Stats_Runtime_ReportMessageEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL27Stats_Runtime_ReportMessageEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL33Stats_Runtime_OrdinaryHasInstanceEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC66:
	.string	"V8.Runtime_Runtime_OrdinaryHasInstance"
	.section	.text._ZN2v88internalL33Stats_Runtime_OrdinaryHasInstanceEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL33Stats_Runtime_OrdinaryHasInstanceEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL33Stats_Runtime_OrdinaryHasInstanceEiPmPNS0_7IsolateE.isra.0:
.LFB26546:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1483
.L1451:
	movq	_ZZN2v88internalL33Stats_Runtime_OrdinaryHasInstanceEiPmPNS0_7IsolateEE28trace_event_unique_atomic489(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1484
.L1453:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1485
.L1455:
	addl	$1, 41104(%r12)
	leaq	-8(%r13), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	41088(%r12), %rbx
	movq	41096(%r12), %r14
	call	_ZN2v88internal6Object19OrdinaryHasInstanceEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	testq	%rax, %rax
	je	.L1486
	movq	(%rax), %r13
.L1460:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L1463
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1463:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1487
.L1450:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1488
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1485:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1489
.L1456:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1457
	movq	(%rdi), %rax
	call	*8(%rax)
.L1457:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1458
	movq	(%rdi), %rax
	call	*8(%rax)
.L1458:
	leaq	.LC66(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1455
	.p2align 4,,10
	.p2align 3
.L1484:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1490
.L1454:
	movq	%rbx, _ZZN2v88internalL33Stats_Runtime_OrdinaryHasInstanceEiPmPNS0_7IsolateEE28trace_event_unique_atomic489(%rip)
	jmp	.L1453
	.p2align 4,,10
	.p2align 3
.L1486:
	movq	312(%r12), %r13
	jmp	.L1460
	.p2align 4,,10
	.p2align 3
.L1487:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1450
	.p2align 4,,10
	.p2align 3
.L1483:
	movq	40960(%rsi), %rax
	movl	$355, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1451
	.p2align 4,,10
	.p2align 3
.L1490:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1454
	.p2align 4,,10
	.p2align 3
.L1489:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC66(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1456
.L1488:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26546:
	.size	_ZN2v88internalL33Stats_Runtime_OrdinaryHasInstanceEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL33Stats_Runtime_OrdinaryHasInstanceEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL41Stats_Runtime_GetAndResetRuntimeCallStatsEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC67:
	.string	"V8.Runtime_Runtime_GetAndResetRuntimeCallStats"
	.section	.rodata._ZN2v88internalL41Stats_Runtime_GetAndResetRuntimeCallStatsEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC68:
	.string	"args[0].IsString()"
.LC69:
	.string	"a"
	.section	.text._ZN2v88internalL41Stats_Runtime_GetAndResetRuntimeCallStatsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL41Stats_Runtime_GetAndResetRuntimeCallStatsEiPmPNS0_7IsolateE, @function
_ZN2v88internalL41Stats_Runtime_GetAndResetRuntimeCallStatsEiPmPNS0_7IsolateE:
.LFB21263:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$600, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -528(%rbp)
	movq	$0, -496(%rbp)
	movaps	%xmm0, -512(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1546
.L1492:
	movq	_ZZN2v88internalL41Stats_Runtime_GetAndResetRuntimeCallStatsEiPmPNS0_7IsolateEE28trace_event_unique_atomic436(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1547
.L1494:
	movq	$0, -560(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1548
.L1496:
	movq	40960(%r12), %rdi
	movq	41088(%r12), %rax
	addl	$1, 41104(%r12)
	movq	41096(%r12), %rbx
	leaq	23240(%rdi), %rsi
	addq	$50888, %rdi
	movq	%rax, -600(%rbp)
	call	_ZN2v88internal28WorkerThreadRuntimeCallStats14AddToMainTableEPNS0_16RuntimeCallStatsE@PLT
	testl	%r13d, %r13d
	jne	.L1500
	movq	.LC70(%rip), %xmm1
	leaq	-320(%rbp), %r13
	leaq	-448(%rbp), %r14
	movq	%r13, %rdi
	leaq	-368(%rbp), %r15
	movhps	.LC71(%rip), %xmm1
	movaps	%xmm1, -624(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movw	%ax, -96(%rbp)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	%rax, -448(%rbp)
	movq	$0, -104(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	-448(%rbp), %rax
	movq	$0, -440(%rbp)
	movq	-24(%rax), %rdi
	addq	%r14, %rdi
	leaq	-432(%rbp), %r14
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	xorl	%esi, %esi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r14, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movdqa	-624(%rbp), %xmm1
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r13, %rdi
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -608(%rbp)
	movq	%rax, -352(%rbp)
	movl	$24, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	40960(%r12), %rax
	movq	%r14, %rsi
	leaq	-464(%rbp), %r14
	leaq	23240(%rax), %rdi
	call	_ZN2v88internal16RuntimeCallStats5PrintERSo@PLT
	movq	-384(%rbp), %rax
	movq	%r14, -480(%rbp)
	leaq	-480(%rbp), %rdi
	movq	$0, -472(%rbp)
	movb	$0, -464(%rbp)
	testq	%rax, %rax
	je	.L1501
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L1502
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L1503:
	movq	-480(%rbp), %rdx
	movq	%rdx, %rdi
	movq	%rdx, -624(%rbp)
	call	strlen@PLT
	movq	-624(%rbp), %rdx
	leaq	-576(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -568(%rbp)
	movq	%rdx, -576(%rbp)
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	je	.L1549
	movq	-480(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L1505
	movq	%rax, -624(%rbp)
	call	_ZdlPv@PLT
	movq	-624(%rbp), %rax
.L1505:
	movq	.LC70(%rip), %xmm0
	movq	%rax, -632(%rbp)
	movq	40960(%r12), %rax
	movhps	.LC72(%rip), %xmm0
	leaq	23240(%rax), %rdi
	movaps	%xmm0, -624(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5ResetEv@PLT
	movq	-632(%rbp), %rax
	movdqa	-624(%rbp), %xmm0
	movq	-352(%rbp), %rdi
	movq	(%rax), %r14
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movaps	%xmm0, -432(%rbp)
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	cmpq	-608(%rbp), %rdi
	je	.L1506
	call	_ZdlPv@PLT
.L1506:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%r15, %rdi
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r13, %rdi
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	$0, -440(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
.L1507:
	subl	$1, 41104(%r12)
	movq	-600(%rbp), %rax
	movq	%rax, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1524
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1524:
	leaq	-560(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-528(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1550
.L1491:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1551
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1500:
	.cfi_restore_state
	movq	(%r14), %rax
	testb	$1, %al
	jne	.L1552
	sarq	$32, %rax
	movq	stdout(%rip), %r15
	cmpq	$1, %rax
	cmovne	stderr(%rip), %r15
.L1514:
	cmpl	$1, %r13d
	jle	.L1516
	movq	-8(%r14), %rax
	testb	$1, %al
	jne	.L1553
.L1517:
	leaq	.LC46(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1553:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L1517
	leaq	-576(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rax, -576(%rbp)
	call	_ZN2v88internal6String7PrintOnEP8_IO_FILE@PLT
	movl	$10, %edi
	movq	%r15, %rsi
	call	fputc@PLT
	movq	%r15, %rdi
	call	fflush@PLT
.L1516:
	leaq	-448(%rbp), %r13
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8OFStreamC1EP8_IO_FILE@PLT
	movq	40960(%r12), %rax
	movq	%r13, %rsi
	leaq	23240(%rax), %rdi
	call	_ZN2v88internal16RuntimeCallStats5PrintERSo@PLT
	movq	40960(%r12), %rax
	leaq	23240(%rax), %rdi
	call	_ZN2v88internal16RuntimeCallStats5ResetEv@PLT
	movq	(%r14), %rax
	movq	%r15, %rdi
	testb	$1, %al
	jne	.L1554
.L1519:
	call	fflush@PLT
.L1521:
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rcx
	movq	88(%r12), %r14
	movq	%rax, -368(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-384(%rbp), %rdi
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -448(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	-368(%rbp), %rdi
	movq	%rax, -448(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -368(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L1507
	.p2align 4,,10
	.p2align 3
.L1548:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -448(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1555
.L1497:
	movq	-440(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1498
	movq	(%rdi), %rax
	call	*8(%rax)
.L1498:
	movq	-448(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1499
	movq	(%rdi), %rax
	call	*8(%rax)
.L1499:
	leaq	.LC67(%rip), %rax
	movq	%rbx, -552(%rbp)
	movq	%rax, -544(%rbp)
	leaq	-552(%rbp), %rax
	movq	%r15, -536(%rbp)
	movq	%rax, -560(%rbp)
	jmp	.L1496
	.p2align 4,,10
	.p2align 3
.L1547:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1556
.L1495:
	movq	%rbx, _ZZN2v88internalL41Stats_Runtime_GetAndResetRuntimeCallStatsEiPmPNS0_7IsolateEE28trace_event_unique_atomic436(%rip)
	jmp	.L1494
	.p2align 4,,10
	.p2align 3
.L1502:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L1503
	.p2align 4,,10
	.p2align 3
.L1552:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L1510
	leaq	.LC29(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1501:
	leaq	-352(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L1503
	.p2align 4,,10
	.p2align 3
.L1554:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1519
	call	fclose@PLT
	jmp	.L1521
	.p2align 4,,10
	.p2align 3
.L1546:
	movq	40960(%rdx), %rax
	leaq	-520(%rbp), %rsi
	movl	$348, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -528(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1492
	.p2align 4,,10
	.p2align 3
.L1550:
	leaq	-520(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1491
	.p2align 4,,10
	.p2align 3
.L1510:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L1557
	leaq	-577(%rbp), %rsi
	leaq	-576(%rbp), %rdi
	movq	%rax, -576(%rbp)
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	leaq	.LC69(%rip), %rsi
	movq	%rax, %rdi
	call	fopen@PLT
	movq	%rax, %r15
	jmp	.L1514
	.p2align 4,,10
	.p2align 3
.L1557:
	leaq	.LC68(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1556:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1495
	.p2align 4,,10
	.p2align 3
.L1555:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$88, %esi
	leaq	-448(%rbp), %rdx
	pushq	$0
	leaq	.LC67(%rip), %rcx
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L1497
	.p2align 4,,10
	.p2align 3
.L1549:
	leaq	.LC48(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L1551:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21263:
	.size	_ZN2v88internalL41Stats_Runtime_GetAndResetRuntimeCallStatsEiPmPNS0_7IsolateE, .-_ZN2v88internalL41Stats_Runtime_GetAndResetRuntimeCallStatsEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL20Stats_Runtime_TypeofEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC73:
	.string	"V8.Runtime_Runtime_Typeof"
	.section	.text._ZN2v88internalL20Stats_Runtime_TypeofEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL20Stats_Runtime_TypeofEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL20Stats_Runtime_TypeofEiPmPNS0_7IsolateE.isra.0:
.LFB26547:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1589
.L1559:
	movq	_ZZN2v88internalL20Stats_Runtime_TypeofEiPmPNS0_7IsolateEE28trace_event_unique_atomic498(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1590
.L1561:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1591
.L1563:
	addl	$1, 41104(%r12)
	movq	41088(%r12), %r14
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	41096(%r12), %rbx
	call	_ZN2v88internal6Object6TypeOfEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%rax), %r13
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1569
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1569:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1592
.L1558:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1593
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1591:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1594
.L1564:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1565
	movq	(%rdi), %rax
	call	*8(%rax)
.L1565:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1566
	movq	(%rdi), %rax
	call	*8(%rax)
.L1566:
	leaq	.LC73(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1563
	.p2align 4,,10
	.p2align 3
.L1590:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1595
.L1562:
	movq	%rbx, _ZZN2v88internalL20Stats_Runtime_TypeofEiPmPNS0_7IsolateEE28trace_event_unique_atomic498(%rip)
	jmp	.L1561
	.p2align 4,,10
	.p2align 3
.L1589:
	movq	40960(%rsi), %rax
	movl	$382, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1559
	.p2align 4,,10
	.p2align 3
.L1592:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1558
	.p2align 4,,10
	.p2align 3
.L1595:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1562
	.p2align 4,,10
	.p2align 3
.L1594:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC73(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1564
.L1593:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26547:
	.size	_ZN2v88internalL20Stats_Runtime_TypeofEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL20Stats_Runtime_TypeofEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL25Stats_Runtime_AccessCheckEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC74:
	.string	"V8.Runtime_Runtime_AccessCheck"
	.section	.rodata._ZN2v88internalL25Stats_Runtime_AccessCheckEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC75:
	.string	"args[0].IsJSObject()"
	.section	.text._ZN2v88internalL25Stats_Runtime_AccessCheckEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL25Stats_Runtime_AccessCheckEiPmPNS0_7IsolateE, @function
_ZN2v88internalL25Stats_Runtime_AccessCheckEiPmPNS0_7IsolateE:
.LFB21148:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1634
.L1597:
	movq	_ZZN2v88internalL25Stats_Runtime_AccessCheckEiPmPNS0_7IsolateEE27trace_event_unique_atomic34(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1635
.L1599:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1636
.L1601:
	movq	41088(%r12), %rbx
	movq	41096(%r12), %r14
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L1605
.L1606:
	leaq	.LC75(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1635:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1637
.L1600:
	movq	%rbx, _ZZN2v88internalL25Stats_Runtime_AccessCheckEiPmPNS0_7IsolateEE27trace_event_unique_atomic34(%rip)
	jmp	.L1599
	.p2align 4,,10
	.p2align 3
.L1605:
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L1606
	movq	41112(%r12), %rdi
	movq	12464(%r12), %r15
	testq	%rdi, %rdi
	je	.L1607
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1608:
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate9MayAccessENS0_6HandleINS0_7ContextEEENS2_INS0_8JSObjectEEE@PLT
	testb	%al, %al
	jne	.L1610
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate23ReportFailedAccessCheckENS0_6HandleINS0_8JSObjectEEE@PLT
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	jne	.L1638
.L1610:
	movq	88(%r12), %r13
.L1611:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L1614
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1614:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1639
.L1596:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1640
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1607:
	.cfi_restore_state
	movq	%rbx, %rsi
	cmpq	41096(%r12), %rbx
	je	.L1641
.L1609:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r15, (%rsi)
	jmp	.L1608
	.p2align 4,,10
	.p2align 3
.L1636:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1642
.L1602:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1603
	movq	(%rdi), %rax
	call	*8(%rax)
.L1603:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1604
	movq	(%rdi), %rax
	call	*8(%rax)
.L1604:
	leaq	.LC74(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1601
	.p2align 4,,10
	.p2align 3
.L1638:
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	movq	%rax, %r13
	jmp	.L1611
	.p2align 4,,10
	.p2align 3
.L1634:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$337, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1597
	.p2align 4,,10
	.p2align 3
.L1639:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1596
	.p2align 4,,10
	.p2align 3
.L1641:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L1609
	.p2align 4,,10
	.p2align 3
.L1642:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC74(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1602
	.p2align 4,,10
	.p2align 3
.L1637:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1600
.L1640:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21148:
	.size	_ZN2v88internalL25Stats_Runtime_AccessCheckEiPmPNS0_7IsolateE, .-_ZN2v88internalL25Stats_Runtime_AccessCheckEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL37Stats_Runtime_BytecodeBudgetInterruptEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC76:
	.string	"V8.Runtime_Runtime_BytecodeBudgetInterrupt"
	.section	.rodata._ZN2v88internalL37Stats_Runtime_BytecodeBudgetInterruptEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC77:
	.string	"args[0].IsJSFunction()"
	.section	.text._ZN2v88internalL37Stats_Runtime_BytecodeBudgetInterruptEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL37Stats_Runtime_BytecodeBudgetInterruptEiPmPNS0_7IsolateE, @function
_ZN2v88internalL37Stats_Runtime_BytecodeBudgetInterruptEiPmPNS0_7IsolateE:
.LFB21224:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1682
.L1644:
	movq	_ZZN2v88internalL37Stats_Runtime_BytecodeBudgetInterruptEiPmPNS0_7IsolateEE28trace_event_unique_atomic286(%rip), %r13
	testq	%r13, %r13
	je	.L1683
.L1646:
	movq	$0, -160(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L1684
.L1648:
	movq	41088(%r12), %r14
	movq	41096(%r12), %r13
	addl	$1, 41104(%r12)
	movq	(%rbx), %rax
	testb	$1, %al
	jne	.L1685
.L1652:
	leaq	.LC77(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1683:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1686
.L1647:
	movq	%r13, _ZZN2v88internalL37Stats_Runtime_BytecodeBudgetInterruptEiPmPNS0_7IsolateEE28trace_event_unique_atomic286(%rip)
	jmp	.L1646
	.p2align 4,,10
	.p2align 3
.L1685:
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L1652
	movl	_ZN2v88internal21FLAG_interrupt_budgetE(%rip), %edx
	movq	39(%rax), %rax
	movabsq	$287762808832, %rcx
	movl	%edx, 15(%rax)
	movq	(%rbx), %rdx
	movq	23(%rdx), %rax
	movq	7(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1657
	testb	$1, %al
	jne	.L1655
.L1658:
	movq	39(%rdx), %rax
	movq	7(%rax), %rax
	movq	-1(%rax), %rax
	cmpw	$155, 11(%rax)
	je	.L1687
.L1657:
	movq	%rbx, %rdi
	call	_ZN2v88internal10JSFunction20EnsureFeedbackVectorENS0_6HandleIS1_EE@PLT
	movq	(%rbx), %rax
	movq	39(%rax), %rax
	movq	7(%rax), %rax
	movl	$1, 35(%rax)
	movq	88(%r12), %r15
.L1659:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L1662
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1662:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1688
.L1643:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1689
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1687:
	.cfi_restore_state
	leaq	37512(%r12), %rdi
	call	_ZN2v88internal10StackGuard16HandleInterruptsEv@PLT
	movq	%rax, %r15
	jmp	.L1659
	.p2align 4,,10
	.p2align 3
.L1684:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1690
.L1649:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1650
	movq	(%rdi), %rax
	call	*8(%rax)
.L1650:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1651
	movq	(%rdi), %rax
	call	*8(%rax)
.L1651:
	leaq	.LC76(%rip), %rax
	movq	%r13, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1648
	.p2align 4,,10
	.p2align 3
.L1655:
	movq	-1(%rax), %rcx
	cmpw	$165, 11(%rcx)
	je	.L1657
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L1658
	jmp	.L1657
	.p2align 4,,10
	.p2align 3
.L1682:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$351, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1644
	.p2align 4,,10
	.p2align 3
.L1688:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1643
	.p2align 4,,10
	.p2align 3
.L1686:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L1647
	.p2align 4,,10
	.p2align 3
.L1690:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC76(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1649
.L1689:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21224:
	.size	_ZN2v88internalL37Stats_Runtime_BytecodeBudgetInterruptEiPmPNS0_7IsolateE, .-_ZN2v88internalL37Stats_Runtime_BytecodeBudgetInterruptEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL34Stats_Runtime_AllowDynamicFunctionEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC78:
	.string	"V8.Runtime_Runtime_AllowDynamicFunction"
	.section	.text._ZN2v88internalL34Stats_Runtime_AllowDynamicFunctionEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL34Stats_Runtime_AllowDynamicFunctionEiPmPNS0_7IsolateE, @function
_ZN2v88internalL34Stats_Runtime_AllowDynamicFunctionEiPmPNS0_7IsolateE:
.LFB21272:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$144, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1728
.L1692:
	movq	_ZZN2v88internalL34Stats_Runtime_AllowDynamicFunctionEiPmPNS0_7IsolateEE28trace_event_unique_atomic505(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1729
.L1694:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1730
.L1696:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L1731
.L1700:
	leaq	.LC77(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1729:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1732
.L1695:
	movq	%rbx, _ZZN2v88internalL34Stats_Runtime_AllowDynamicFunctionEiPmPNS0_7IsolateEE28trace_event_unique_atomic505(%rip)
	jmp	.L1694
	.p2align 4,,10
	.p2align 3
.L1731:
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L1700
	movq	31(%rax), %rax
	leaq	-152(%rbp), %rdi
	movq	%rax, -152(%rbp)
	call	_ZN2v88internal7Context12global_proxyEv@PLT
	movq	41112(%r12), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L1733
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L1703:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8Builtins20AllowDynamicFunctionEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEENS4_INS0_8JSObjectEEE@PLT
	movq	%r12, %rdi
	movzbl	%al, %esi
	call	_ZN2v88internal7Factory9ToBooleanEb@PLT
	movq	(%rax), %r13
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1707
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1707:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1734
.L1691:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1735
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1733:
	.cfi_restore_state
	movq	41088(%r12), %rdx
	cmpq	41096(%r12), %rdx
	je	.L1736
.L1704:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdx)
	jmp	.L1703
	.p2align 4,,10
	.p2align 3
.L1730:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1737
.L1697:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1698
	movq	(%rdi), %rax
	call	*8(%rax)
.L1698:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1699
	movq	(%rdi), %rax
	call	*8(%rax)
.L1699:
	leaq	.LC78(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1696
	.p2align 4,,10
	.p2align 3
.L1728:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$343, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1692
	.p2align 4,,10
	.p2align 3
.L1734:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1691
	.p2align 4,,10
	.p2align 3
.L1736:
	movq	%r12, %rdi
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L1704
	.p2align 4,,10
	.p2align 3
.L1737:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC78(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1697
	.p2align 4,,10
	.p2align 3
.L1732:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1695
.L1735:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21272:
	.size	_ZN2v88internalL34Stats_Runtime_AllowDynamicFunctionEiPmPNS0_7IsolateE, .-_ZN2v88internalL34Stats_Runtime_AllowDynamicFunctionEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL37Stats_Runtime_CreateListFromArrayLikeEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC79:
	.string	"V8.Runtime_Runtime_CreateListFromArrayLike"
	.section	.text._ZN2v88internalL37Stats_Runtime_CreateListFromArrayLikeEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL37Stats_Runtime_CreateListFromArrayLikeEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL37Stats_Runtime_CreateListFromArrayLikeEiPmPNS0_7IsolateE.isra.0:
.LFB26558:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1771
.L1739:
	movq	_ZZN2v88internalL37Stats_Runtime_CreateListFromArrayLikeEiPmPNS0_7IsolateEE28trace_event_unique_atomic420(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1772
.L1741:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1773
.L1743:
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	41088(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	41096(%r12), %r14
	call	_ZN2v88internal6Object23CreateListFromArrayLikeEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementTypesE@PLT
	testq	%rax, %rax
	je	.L1774
	movq	(%rax), %r13
.L1748:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L1751
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1751:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1775
.L1738:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1776
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1773:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1777
.L1744:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1745
	movq	(%rdi), %rax
	call	*8(%rax)
.L1745:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1746
	movq	(%rdi), %rax
	call	*8(%rax)
.L1746:
	leaq	.LC79(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1743
	.p2align 4,,10
	.p2align 3
.L1772:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1778
.L1742:
	movq	%rbx, _ZZN2v88internalL37Stats_Runtime_CreateListFromArrayLikeEiPmPNS0_7IsolateEE28trace_event_unique_atomic420(%rip)
	jmp	.L1741
	.p2align 4,,10
	.p2align 3
.L1774:
	movq	312(%r12), %r13
	jmp	.L1748
	.p2align 4,,10
	.p2align 3
.L1775:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1738
	.p2align 4,,10
	.p2align 3
.L1771:
	movq	40960(%rsi), %rax
	movl	$345, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1739
	.p2align 4,,10
	.p2align 3
.L1778:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1742
	.p2align 4,,10
	.p2align 3
.L1777:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC79(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1744
.L1776:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26558:
	.size	_ZN2v88internalL37Stats_Runtime_CreateListFromArrayLikeEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL37Stats_Runtime_CreateListFromArrayLikeEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL38Stats_Runtime_AllocateSeqOneByteStringEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC80:
	.string	"V8.Runtime_Runtime_AllocateSeqOneByteString"
	.section	.text._ZN2v88internalL38Stats_Runtime_AllocateSeqOneByteStringEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL38Stats_Runtime_AllocateSeqOneByteStringEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL38Stats_Runtime_AllocateSeqOneByteStringEiPmPNS0_7IsolateE.isra.0:
.LFB26570:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1814
.L1780:
	movq	_ZZN2v88internalL38Stats_Runtime_AllocateSeqOneByteStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic357(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1815
.L1782:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1816
.L1784:
	movl	41104(%r12), %eax
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	leal	1(%rax), %edx
	movl	%edx, 41104(%r12)
	movq	0(%r13), %rsi
	testb	$1, %sil
	jne	.L1817
	sarq	$32, %rsi
	je	.L1818
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory19NewRawOneByteStringEiNS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	je	.L1819
	movq	(%rax), %r13
	movl	41104(%r12), %eax
	movq	41096(%r12), %rdx
	subl	$1, %eax
.L1792:
	movq	%r14, 41088(%r12)
	movl	%eax, 41104(%r12)
	cmpq	%rdx, %rbx
	je	.L1790
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1790:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1820
.L1779:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1821
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1818:
	.cfi_restore_state
	movl	%eax, 41104(%r12)
	movq	128(%r12), %r13
	jmp	.L1790
	.p2align 4,,10
	.p2align 3
.L1816:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1822
.L1785:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1786
	movq	(%rdi), %rax
	call	*8(%rax)
.L1786:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1787
	movq	(%rdi), %rax
	call	*8(%rax)
.L1787:
	leaq	.LC80(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1784
	.p2align 4,,10
	.p2align 3
.L1815:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1823
.L1783:
	movq	%rbx, _ZZN2v88internalL38Stats_Runtime_AllocateSeqOneByteStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic357(%rip)
	jmp	.L1782
	.p2align 4,,10
	.p2align 3
.L1817:
	leaq	.LC29(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1814:
	movq	40960(%rsi), %rax
	movl	$341, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1780
	.p2align 4,,10
	.p2align 3
.L1820:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1779
	.p2align 4,,10
	.p2align 3
.L1819:
	movl	41104(%r12), %eax
	movq	312(%r12), %r13
	movq	41096(%r12), %rdx
	subl	$1, %eax
	jmp	.L1792
	.p2align 4,,10
	.p2align 3
.L1823:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1783
	.p2align 4,,10
	.p2align 3
.L1822:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC80(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1785
.L1821:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26570:
	.size	_ZN2v88internalL38Stats_Runtime_AllocateSeqOneByteStringEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL38Stats_Runtime_AllocateSeqOneByteStringEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL38Stats_Runtime_AllocateSeqTwoByteStringEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC81:
	.string	"V8.Runtime_Runtime_AllocateSeqTwoByteString"
	.section	.text._ZN2v88internalL38Stats_Runtime_AllocateSeqTwoByteStringEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL38Stats_Runtime_AllocateSeqTwoByteStringEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL38Stats_Runtime_AllocateSeqTwoByteStringEiPmPNS0_7IsolateE.isra.0:
.LFB26571:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1859
.L1825:
	movq	_ZZN2v88internalL38Stats_Runtime_AllocateSeqTwoByteStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic368(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1860
.L1827:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1861
.L1829:
	movl	41104(%r12), %eax
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	leal	1(%rax), %edx
	movl	%edx, 41104(%r12)
	movq	0(%r13), %rsi
	testb	$1, %sil
	jne	.L1862
	sarq	$32, %rsi
	je	.L1863
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory19NewRawTwoByteStringEiNS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	je	.L1864
	movq	(%rax), %r13
	movl	41104(%r12), %eax
	movq	41096(%r12), %rdx
	subl	$1, %eax
.L1837:
	movq	%r14, 41088(%r12)
	movl	%eax, 41104(%r12)
	cmpq	%rdx, %rbx
	je	.L1835
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1835:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1865
.L1824:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1866
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1863:
	.cfi_restore_state
	movl	%eax, 41104(%r12)
	movq	128(%r12), %r13
	jmp	.L1835
	.p2align 4,,10
	.p2align 3
.L1861:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1867
.L1830:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1831
	movq	(%rdi), %rax
	call	*8(%rax)
.L1831:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1832
	movq	(%rdi), %rax
	call	*8(%rax)
.L1832:
	leaq	.LC81(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1829
	.p2align 4,,10
	.p2align 3
.L1860:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1868
.L1828:
	movq	%rbx, _ZZN2v88internalL38Stats_Runtime_AllocateSeqTwoByteStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic368(%rip)
	jmp	.L1827
	.p2align 4,,10
	.p2align 3
.L1862:
	leaq	.LC29(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1859:
	movq	40960(%rsi), %rax
	movl	$342, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1825
	.p2align 4,,10
	.p2align 3
.L1865:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1824
	.p2align 4,,10
	.p2align 3
.L1864:
	movl	41104(%r12), %eax
	movq	312(%r12), %r13
	movq	41096(%r12), %rdx
	subl	$1, %eax
	jmp	.L1837
	.p2align 4,,10
	.p2align 3
.L1868:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1828
	.p2align 4,,10
	.p2align 3
.L1867:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC81(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1830
.L1866:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26571:
	.size	_ZN2v88internalL38Stats_Runtime_AllocateSeqTwoByteStringEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL38Stats_Runtime_AllocateSeqTwoByteStringEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL41Stats_Runtime_CreateAsyncFromSyncIteratorEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC82:
	.string	"V8.Runtime_Runtime_CreateAsyncFromSyncIterator"
	.section	.text._ZN2v88internalL41Stats_Runtime_CreateAsyncFromSyncIteratorEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL41Stats_Runtime_CreateAsyncFromSyncIteratorEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL41Stats_Runtime_CreateAsyncFromSyncIteratorEiPmPNS0_7IsolateE.isra.0:
.LFB26572:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -208(%rbp)
	movq	$0, -176(%rbp)
	movaps	%xmm0, -192(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1913
.L1870:
	movq	_ZZN2v88internalL41Stats_Runtime_CreateAsyncFromSyncIteratorEiPmPNS0_7IsolateEE28trace_event_unique_atomic514(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1914
.L1872:
	movq	$0, -240(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1915
.L1874:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L1878
.L1880:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$167, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L1879:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1890
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1890:
	leaq	-240(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1916
.L1869:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1917
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1878:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$1023, 11(%rdx)
	jbe	.L1880
	movq	-1(%rax), %rax
	leaq	2912(%r12), %r8
	movq	%r13, %r15
	cmpw	$1023, 11(%rax)
	ja	.L1882
	movl	$-1, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r8, -248(%rbp)
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	-248(%rbp), %r8
	movq	%rax, %r15
.L1882:
	movq	2912(%r12), %rdx
	movl	$3, %eax
	movq	-1(%rdx), %rcx
	cmpw	$64, 11(%rcx)
	jne	.L1884
	movl	11(%rdx), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$3, %eax
.L1884:
	movl	%eax, -160(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -148(%rbp)
	movq	2912(%r12), %rax
	movq	%r12, -136(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L1918
.L1885:
	movq	%r15, -96(%rbp)
	leaq	-160(%rbp), %r15
	movq	%r15, %rdi
	movq	%r8, -128(%rbp)
	movq	$0, -120(%rbp)
	movq	%r13, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	$-1, -88(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -156(%rbp)
	jne	.L1886
	movq	-136(%rbp), %rax
	leaq	88(%rax), %rdx
.L1887:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory26NewJSAsyncFromSyncIteratorENS0_6HandleINS0_10JSReceiverEEENS2_INS0_6ObjectEEE@PLT
	movq	(%rax), %r13
	jmp	.L1879
	.p2align 4,,10
	.p2align 3
.L1915:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1919
.L1875:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1876
	movq	(%rdi), %rax
	call	*8(%rax)
.L1876:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1877
	movq	(%rdi), %rax
	call	*8(%rax)
.L1877:
	leaq	.LC82(%rip), %rax
	movq	%rbx, -232(%rbp)
	movq	%rax, -224(%rbp)
	leaq	-232(%rbp), %rax
	movq	%r14, -216(%rbp)
	movq	%rax, -240(%rbp)
	jmp	.L1874
	.p2align 4,,10
	.p2align 3
.L1914:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1920
.L1873:
	movq	%rbx, _ZZN2v88internalL41Stats_Runtime_CreateAsyncFromSyncIteratorEiPmPNS0_7IsolateEE28trace_event_unique_atomic514(%rip)
	jmp	.L1872
	.p2align 4,,10
	.p2align 3
.L1916:
	leaq	-200(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1869
	.p2align 4,,10
	.p2align 3
.L1913:
	movq	40960(%rsi), %rax
	movl	$344, %edx
	leaq	-200(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -208(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1870
	.p2align 4,,10
	.p2align 3
.L1886:
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	jne	.L1887
	movq	312(%r12), %r13
	jmp	.L1879
	.p2align 4,,10
	.p2align 3
.L1920:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1873
	.p2align 4,,10
	.p2align 3
.L1919:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC82(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1875
	.p2align 4,,10
	.p2align 3
.L1918:
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %r8
	jmp	.L1885
.L1917:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26572:
	.size	_ZN2v88internalL41Stats_Runtime_CreateAsyncFromSyncIteratorEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL41Stats_Runtime_CreateAsyncFromSyncIteratorEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal21RuntimeCallTimerScopeC2EPNS0_7IsolateENS0_20RuntimeCallCounterIdE,"axG",@progbits,_ZN2v88internal21RuntimeCallTimerScopeC5EPNS0_7IsolateENS0_20RuntimeCallCounterIdE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21RuntimeCallTimerScopeC2EPNS0_7IsolateENS0_20RuntimeCallCounterIdE
	.type	_ZN2v88internal21RuntimeCallTimerScopeC2EPNS0_7IsolateENS0_20RuntimeCallCounterIdE, @function
_ZN2v88internal21RuntimeCallTimerScopeC2EPNS0_7IsolateENS0_20RuntimeCallCounterIdE:
.LFB11445:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movq	$0, 32(%rdi)
	movups	%xmm0, (%rdi)
	movups	%xmm0, 16(%rdi)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1923
	ret
	.p2align 4,,10
	.p2align 3
.L1923:
	movq	40960(%rsi), %r8
	leaq	8(%rdi), %rsi
	addq	$23240, %r8
	movq	%r8, (%rdi)
	movq	%r8, %rdi
	jmp	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	.cfi_endproc
.LFE11445:
	.size	_ZN2v88internal21RuntimeCallTimerScopeC2EPNS0_7IsolateENS0_20RuntimeCallCounterIdE, .-_ZN2v88internal21RuntimeCallTimerScopeC2EPNS0_7IsolateENS0_20RuntimeCallCounterIdE
	.weak	_ZN2v88internal21RuntimeCallTimerScopeC1EPNS0_7IsolateENS0_20RuntimeCallCounterIdE
	.set	_ZN2v88internal21RuntimeCallTimerScopeC1EPNS0_7IsolateENS0_20RuntimeCallCounterIdE,_ZN2v88internal21RuntimeCallTimerScopeC2EPNS0_7IsolateENS0_20RuntimeCallCounterIdE
	.section	.text._ZN2v88internal19Runtime_AccessCheckEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal19Runtime_AccessCheckEiPmPNS0_7IsolateE
	.type	_ZN2v88internal19Runtime_AccessCheckEiPmPNS0_7IsolateE, @function
_ZN2v88internal19Runtime_AccessCheckEiPmPNS0_7IsolateE:
.LFB21149:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1936
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r14
	testb	$1, %al
	jne	.L1926
.L1927:
	leaq	.LC75(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1926:
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L1927
	movq	41112(%rdx), %rdi
	movq	12464(%rdx), %r15
	testq	%rdi, %rdi
	je	.L1928
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1929:
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate9MayAccessENS0_6HandleINS0_7ContextEEENS2_INS0_8JSObjectEEE@PLT
	testb	%al, %al
	jne	.L1931
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate23ReportFailedAccessCheckENS0_6HandleINS0_8JSObjectEEE@PLT
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	jne	.L1937
.L1931:
	movq	88(%r12), %r13
.L1932:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L1924
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1924:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1928:
	.cfi_restore_state
	movq	%rbx, %rsi
	cmpq	41096(%rdx), %rbx
	je	.L1938
.L1930:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r15, (%rsi)
	jmp	.L1929
	.p2align 4,,10
	.p2align 3
.L1937:
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	movq	%rax, %r13
	jmp	.L1932
	.p2align 4,,10
	.p2align 3
.L1936:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL25Stats_Runtime_AccessCheckEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L1938:
	.cfi_restore_state
	movq	%rdx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L1930
	.cfi_endproc
.LFE21149:
	.size	_ZN2v88internal19Runtime_AccessCheckEiPmPNS0_7IsolateE, .-_ZN2v88internal19Runtime_AccessCheckEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal13Runtime_ThrowEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal13Runtime_ThrowEiPmPNS0_7IsolateE
	.type	_ZN2v88internal13Runtime_ThrowEiPmPNS0_7IsolateE, @function
_ZN2v88internal13Runtime_ThrowEiPmPNS0_7IsolateE:
.LFB21158:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1943
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rsi
	movq	%r12, %rdi
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	xorl	%edx, %edx
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	41096(%r12), %rbx
	je	.L1941
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1941:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1943:
	.cfi_restore_state
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL19Stats_Runtime_ThrowEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE21158:
	.size	_ZN2v88internal13Runtime_ThrowEiPmPNS0_7IsolateE, .-_ZN2v88internal13Runtime_ThrowEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal15Runtime_ReThrowEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal15Runtime_ReThrowEiPmPNS0_7IsolateE
	.type	_ZN2v88internal15Runtime_ReThrowEiPmPNS0_7IsolateE, @function
_ZN2v88internal15Runtime_ReThrowEiPmPNS0_7IsolateE:
.LFB21161:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1948
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rsi
	movq	%rdx, %rdi
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	call	_ZN2v88internal7Isolate7ReThrowENS0_6ObjectE@PLT
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	41096(%r12), %rbx
	je	.L1946
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1946:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1948:
	.cfi_restore_state
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL21Stats_Runtime_ReThrowEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE21161:
	.size	_ZN2v88internal15Runtime_ReThrowEiPmPNS0_7IsolateE, .-_ZN2v88internal15Runtime_ReThrowEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal26Runtime_ThrowStackOverflowEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal26Runtime_ThrowStackOverflowEiPmPNS0_7IsolateE
	.type	_ZN2v88internal26Runtime_ThrowStackOverflowEiPmPNS0_7IsolateE, @function
_ZN2v88internal26Runtime_ThrowStackOverflowEiPmPNS0_7IsolateE:
.LFB21164:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rdx, %rdi
	testl	%eax, %eax
	jne	.L1953
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal7Isolate13StackOverflowEv@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1953:
	.cfi_restore 6
	jmp	_ZN2v88internalL32Stats_Runtime_ThrowStackOverflowEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE21164:
	.size	_ZN2v88internal26Runtime_ThrowStackOverflowEiPmPNS0_7IsolateE, .-_ZN2v88internal26Runtime_ThrowStackOverflowEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal39Runtime_ThrowSymbolAsyncIteratorInvalidEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal39Runtime_ThrowSymbolAsyncIteratorInvalidEiPmPNS0_7IsolateE
	.type	_ZN2v88internal39Runtime_ThrowSymbolAsyncIteratorInvalidEiPmPNS0_7IsolateE, @function
_ZN2v88internal39Runtime_ThrowSymbolAsyncIteratorInvalidEiPmPNS0_7IsolateE:
.LFB21167:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1958
	addl	$1, 41104(%rdx)
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movl	$168, %esi
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	41096(%r12), %rbx
	je	.L1956
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1956:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1958:
	.cfi_restore_state
	popq	%rbx
	movq	%rdx, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL45Stats_Runtime_ThrowSymbolAsyncIteratorInvalidEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE21167:
	.size	_ZN2v88internal39Runtime_ThrowSymbolAsyncIteratorInvalidEiPmPNS0_7IsolateE, .-_ZN2v88internal39Runtime_ThrowSymbolAsyncIteratorInvalidEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal23Runtime_ThrowRangeErrorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal23Runtime_ThrowRangeErrorEiPmPNS0_7IsolateE
	.type	_ZN2v88internal23Runtime_ThrowRangeErrorEiPmPNS0_7IsolateE, @function
_ZN2v88internal23Runtime_ThrowRangeErrorEiPmPNS0_7IsolateE:
.LFB21170:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1976
	cmpb	$0, _ZN2v88internal36FLAG_correctness_fuzzer_suppressionsE(%rip)
	je	.L1961
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L1963
	sarq	$32, %rax
	cmpq	$183, %rax
	je	.L1977
.L1961:
	movq	41088(%r12), %r13
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	(%r9), %rsi
	testb	$1, %sil
	jne	.L1963
	leaq	88(%r12), %r8
	sarq	$32, %rsi
	movq	%r8, %rdx
	cmpl	$1, %edi
	jg	.L1978
.L1966:
	movq	%r8, %rcx
.L1968:
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory13NewRangeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r14
	cmpq	41096(%r12), %rbx
	je	.L1974
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1974:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1978:
	.cfi_restore_state
	leaq	-8(%r9), %rdx
	cmpl	$2, %edi
	je	.L1966
	leaq	-16(%r9), %rcx
	cmpl	$3, %edi
	je	.L1968
	leaq	-24(%r9), %r8
	jmp	.L1968
	.p2align 4,,10
	.p2align 3
.L1963:
	leaq	.LC29(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1976:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL29Stats_Runtime_ThrowRangeErrorEiPmPNS0_7IsolateE
.L1977:
	.cfi_restore_state
	leaq	.LC30(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21170:
	.size	_ZN2v88internal23Runtime_ThrowRangeErrorEiPmPNS0_7IsolateE, .-_ZN2v88internal23Runtime_ThrowRangeErrorEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal22Runtime_ThrowTypeErrorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal22Runtime_ThrowTypeErrorEiPmPNS0_7IsolateE
	.type	_ZN2v88internal22Runtime_ThrowTypeErrorEiPmPNS0_7IsolateE, @function
_ZN2v88internal22Runtime_ThrowTypeErrorEiPmPNS0_7IsolateE:
.LFB21173:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1991
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rsi
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %sil
	jne	.L1992
	leaq	88(%rdx), %r8
	sarq	$32, %rsi
	movq	%r8, %rdx
	cmpl	$1, %edi
	jg	.L1993
.L1984:
	movq	%r8, %rcx
.L1986:
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r14
	cmpq	41096(%r12), %rbx
	je	.L1989
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1989:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1993:
	.cfi_restore_state
	leaq	-8(%r9), %rdx
	cmpl	$2, %edi
	je	.L1984
	leaq	-16(%r9), %rcx
	cmpl	$3, %edi
	je	.L1986
	leaq	-24(%r9), %r8
	jmp	.L1986
	.p2align 4,,10
	.p2align 3
.L1991:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL28Stats_Runtime_ThrowTypeErrorEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L1992:
	.cfi_restore_state
	leaq	.LC29(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21173:
	.size	_ZN2v88internal22Runtime_ThrowTypeErrorEiPmPNS0_7IsolateE, .-_ZN2v88internal22Runtime_ThrowTypeErrorEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal30Runtime_ThrowTypeErrorIfStrictEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal30Runtime_ThrowTypeErrorIfStrictEiPmPNS0_7IsolateE
	.type	_ZN2v88internal30Runtime_ThrowTypeErrorIfStrictEiPmPNS0_7IsolateE, @function
_ZN2v88internal30Runtime_ThrowTypeErrorIfStrictEiPmPNS0_7IsolateE:
.LFB21176:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2007
	xorl	%esi, %esi
	movq	%rdx, %rdi
	call	_ZN2v88internal14GetShouldThrowEPNS0_7IsolateENS_5MaybeINS0_11ShouldThrowEEE@PLT
	cmpl	$1, %eax
	je	.L2008
	movq	41088(%r12), %r15
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rsi
	testb	$1, %sil
	jne	.L2009
	leaq	88(%r12), %r8
	sarq	$32, %rsi
	movq	%r8, %rdx
	cmpl	$1, %r14d
	jg	.L2010
.L2001:
	movq	%r8, %rcx
.L2003:
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%r15, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1994
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rax
.L1994:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2010:
	.cfi_restore_state
	leaq	-8(%r13), %rdx
	cmpl	$2, %r14d
	je	.L2001
	leaq	-16(%r13), %rcx
	cmpl	$3, %r14d
	je	.L2003
	leaq	-24(%r13), %r8
	jmp	.L2003
	.p2align 4,,10
	.p2align 3
.L2008:
	movq	88(%r12), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2007:
	.cfi_restore_state
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL36Stats_Runtime_ThrowTypeErrorIfStrictEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L2009:
	.cfi_restore_state
	leaq	.LC29(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21176:
	.size	_ZN2v88internal30Runtime_ThrowTypeErrorIfStrictEiPmPNS0_7IsolateE, .-_ZN2v88internal30Runtime_ThrowTypeErrorIfStrictEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal39Runtime_ThrowInvalidTypedArrayAlignmentEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal39Runtime_ThrowInvalidTypedArrayAlignmentEiPmPNS0_7IsolateE
	.type	_ZN2v88internal39Runtime_ThrowInvalidTypedArrayAlignmentEiPmPNS0_7IsolateE, @function
_ZN2v88internal39Runtime_ThrowInvalidTypedArrayAlignmentEiPmPNS0_7IsolateE:
.LFB21180:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2040
	movq	41088(%rdx), %rax
	addl	$1, 41104(%rdx)
	movq	41096(%rdx), %r13
	movq	%rax, -112(%rbp)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L2014
.L2015:
	leaq	.LC45(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2014:
	movq	-1(%rax), %rdx
	cmpw	$68, 11(%rdx)
	jne	.L2015
	movq	-8(%rsi), %rdx
	leaq	-8(%rsi), %r15
	testb	$1, %dl
	jne	.L2041
.L2016:
	leaq	.LC46(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2041:
	movq	-1(%rdx), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L2016
	movzbl	14(%rax), %ebx
	shrl	$3, %ebx
	leal	-17(%rbx), %eax
	cmpb	$10, %al
	ja	.L2018
	leaq	.L2020(%rip), %rdx
	movzbl	%al, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal39Runtime_ThrowInvalidTypedArrayAlignmentEiPmPNS0_7IsolateE,"a",@progbits
	.align 4
	.align 4
.L2020:
	.long	.L2030-.L2020
	.long	.L2029-.L2020
	.long	.L2038-.L2020
	.long	.L2027-.L2020
	.long	.L2026-.L2020
	.long	.L2025-.L2020
	.long	.L2024-.L2020
	.long	.L2023-.L2020
	.long	.L2022-.L2020
	.long	.L2021-.L2020
	.long	.L2019-.L2020
	.section	.text._ZN2v88internal39Runtime_ThrowInvalidTypedArrayAlignmentEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L2021:
	movl	$14, %eax
	leaq	.LC42(%rip), %rdx
	.p2align 4,,10
	.p2align 3
.L2028:
	leaq	-80(%rbp), %r8
	movq	%rdx, -80(%rbp)
	movq	%r12, %rdi
	xorl	%edx, %edx
	movq	%r8, %rsi
	movq	%r8, -104(%rbp)
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	-104(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L2042
	leaq	-84(%rbp), %rsi
	movl	%ebx, %edi
	movq	%r8, %rdx
	call	_ZN2v88internal7Factory26TypeAndSizeForElementsKindENS0_12ElementsKindEPNS0_17ExternalArrayTypeEPm@PLT
	movslq	-80(%rbp), %rsi
	movq	41112(%r12), %rdi
	salq	$32, %rsi
	testq	%rdi, %rdi
	je	.L2032
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r8
.L2033:
	movq	%r14, %rcx
	movq	%r15, %rdx
	movl	$204, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory13NewRangeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	subl	$1, 41104(%r12)
	movq	%rax, %rbx
	movq	-112(%rbp), %rax
	movq	%rax, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L2036
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2036:
	movq	%rbx, %rax
.L2011:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2043
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2022:
	.cfi_restore_state
	movl	$17, %eax
	leaq	.LC41(%rip), %rdx
	jmp	.L2028
	.p2align 4,,10
	.p2align 3
.L2023:
	movl	$12, %eax
	leaq	.LC40(%rip), %rdx
	jmp	.L2028
	.p2align 4,,10
	.p2align 3
.L2024:
	movl	$12, %eax
	leaq	.LC39(%rip), %rdx
	jmp	.L2028
	.p2align 4,,10
	.p2align 3
.L2025:
	movl	$10, %eax
	leaq	.LC38(%rip), %rdx
	jmp	.L2028
	.p2align 4,,10
	.p2align 3
.L2026:
	movl	$11, %eax
	leaq	.LC37(%rip), %rdx
	jmp	.L2028
	.p2align 4,,10
	.p2align 3
.L2027:
	movl	$10, %eax
	leaq	.LC36(%rip), %rdx
	jmp	.L2028
	.p2align 4,,10
	.p2align 3
.L2038:
	movl	$11, %eax
	leaq	.LC35(%rip), %rdx
	jmp	.L2028
	.p2align 4,,10
	.p2align 3
.L2029:
	movl	$9, %eax
	leaq	.LC43(%rip), %rdx
	jmp	.L2028
	.p2align 4,,10
	.p2align 3
.L2019:
	movl	$13, %eax
	leaq	.LC33(%rip), %rdx
	jmp	.L2028
	.p2align 4,,10
	.p2align 3
.L2030:
	movl	$10, %eax
	leaq	.LC34(%rip), %rdx
	jmp	.L2028
	.p2align 4,,10
	.p2align 3
.L2032:
	movq	41088(%r12), %r8
	cmpq	41096(%r12), %r8
	je	.L2044
.L2034:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r8)
	jmp	.L2033
	.p2align 4,,10
	.p2align 3
.L2040:
	movq	%rdx, %rsi
	call	_ZN2v88internalL45Stats_Runtime_ThrowInvalidTypedArrayAlignmentEiPmPNS0_7IsolateE.isra.0
	jmp	.L2011
	.p2align 4,,10
	.p2align 3
.L2042:
	leaq	.LC48(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2044:
	movq	%r12, %rdi
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L2034
.L2018:
	leaq	.LC47(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2043:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21180:
	.size	_ZN2v88internal39Runtime_ThrowInvalidTypedArrayAlignmentEiPmPNS0_7IsolateE, .-_ZN2v88internal39Runtime_ThrowInvalidTypedArrayAlignmentEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal37Runtime_UnwindAndFindExceptionHandlerEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal37Runtime_UnwindAndFindExceptionHandlerEiPmPNS0_7IsolateE
	.type	_ZN2v88internal37Runtime_UnwindAndFindExceptionHandlerEiPmPNS0_7IsolateE, @function
_ZN2v88internal37Runtime_UnwindAndFindExceptionHandlerEiPmPNS0_7IsolateE:
.LFB21183:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rdx, %rdi
	testl	%eax, %eax
	jne	.L2049
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal7Isolate20UnwindAndFindHandlerEv@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2049:
	.cfi_restore 6
	jmp	_ZN2v88internalL43Stats_Runtime_UnwindAndFindExceptionHandlerEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE21183:
	.size	_ZN2v88internal37Runtime_UnwindAndFindExceptionHandlerEiPmPNS0_7IsolateE, .-_ZN2v88internal37Runtime_UnwindAndFindExceptionHandlerEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal33Runtime_PromoteScheduledExceptionEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal33Runtime_PromoteScheduledExceptionEiPmPNS0_7IsolateE
	.type	_ZN2v88internal33Runtime_PromoteScheduledExceptionEiPmPNS0_7IsolateE, @function
_ZN2v88internal33Runtime_PromoteScheduledExceptionEiPmPNS0_7IsolateE:
.LFB21186:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rdx, %rdi
	testl	%eax, %eax
	jne	.L2054
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2054:
	.cfi_restore 6
	jmp	_ZN2v88internalL39Stats_Runtime_PromoteScheduledExceptionEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE21186:
	.size	_ZN2v88internal33Runtime_PromoteScheduledExceptionEiPmPNS0_7IsolateE, .-_ZN2v88internal33Runtime_PromoteScheduledExceptionEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal27Runtime_ThrowReferenceErrorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal27Runtime_ThrowReferenceErrorEiPmPNS0_7IsolateE
	.type	_ZN2v88internal27Runtime_ThrowReferenceErrorEiPmPNS0_7IsolateE, @function
_ZN2v88internal27Runtime_ThrowReferenceErrorEiPmPNS0_7IsolateE:
.LFB21189:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2059
	addl	$1, 41104(%rdx)
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	movq	%rsi, %rdx
	movl	$177, %esi
	call	_ZN2v88internal7Factory17NewReferenceErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r14
	cmpq	41096(%r12), %rbx
	je	.L2057
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2057:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2059:
	.cfi_restore_state
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	movq	%r9, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL33Stats_Runtime_ThrowReferenceErrorEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE21189:
	.size	_ZN2v88internal27Runtime_ThrowReferenceErrorEiPmPNS0_7IsolateE, .-_ZN2v88internal27Runtime_ThrowReferenceErrorEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal42Runtime_ThrowAccessedUninitializedVariableEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal42Runtime_ThrowAccessedUninitializedVariableEiPmPNS0_7IsolateE
	.type	_ZN2v88internal42Runtime_ThrowAccessedUninitializedVariableEiPmPNS0_7IsolateE, @function
_ZN2v88internal42Runtime_ThrowAccessedUninitializedVariableEiPmPNS0_7IsolateE:
.LFB21192:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2064
	addl	$1, 41104(%rdx)
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	movq	%rsi, %rdx
	movl	$179, %esi
	call	_ZN2v88internal7Factory17NewReferenceErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r14
	cmpq	41096(%r12), %rbx
	je	.L2062
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2062:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2064:
	.cfi_restore_state
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	movq	%r9, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL48Stats_Runtime_ThrowAccessedUninitializedVariableEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE21192:
	.size	_ZN2v88internal42Runtime_ThrowAccessedUninitializedVariableEiPmPNS0_7IsolateE, .-_ZN2v88internal42Runtime_ThrowAccessedUninitializedVariableEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal20Runtime_NewTypeErrorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal20Runtime_NewTypeErrorEiPmPNS0_7IsolateE
	.type	_ZN2v88internal20Runtime_NewTypeErrorEiPmPNS0_7IsolateE, @function
_ZN2v88internal20Runtime_NewTypeErrorEiPmPNS0_7IsolateE:
.LFB21195:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2075
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L2076
.L2069:
	leaq	-52(%rbp), %rsi
	leaq	-48(%rbp), %rdi
	movl	$0, -52(%rbp)
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal6Object7ToInt32EPi@PLT
	testb	%al, %al
	je	.L2077
	movl	-52(%rbp), %esi
	leaq	-8(%r13), %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	movq	(%rax), %r13
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L2065
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2065:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2078
	addq	$32, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2076:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	je	.L2069
	leaq	.LC14(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2075:
	movq	%r13, %rdi
	movq	%rdx, %rsi
	call	_ZN2v88internalL26Stats_Runtime_NewTypeErrorEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r13
	jmp	.L2065
	.p2align 4,,10
	.p2align 3
.L2077:
	leaq	.LC16(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L2078:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21195:
	.size	_ZN2v88internal20Runtime_NewTypeErrorEiPmPNS0_7IsolateE, .-_ZN2v88internal20Runtime_NewTypeErrorEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal25Runtime_NewReferenceErrorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal25Runtime_NewReferenceErrorEiPmPNS0_7IsolateE
	.type	_ZN2v88internal25Runtime_NewReferenceErrorEiPmPNS0_7IsolateE, @function
_ZN2v88internal25Runtime_NewReferenceErrorEiPmPNS0_7IsolateE:
.LFB21198:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2089
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L2090
.L2083:
	leaq	-52(%rbp), %rsi
	leaq	-48(%rbp), %rdi
	movl	$0, -52(%rbp)
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal6Object7ToInt32EPi@PLT
	testb	%al, %al
	je	.L2091
	movl	-52(%rbp), %esi
	leaq	-8(%r13), %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory17NewReferenceErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	movq	(%rax), %r13
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L2079
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2079:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2092
	addq	$32, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2090:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	je	.L2083
	leaq	.LC14(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2089:
	movq	%r13, %rdi
	movq	%rdx, %rsi
	call	_ZN2v88internalL31Stats_Runtime_NewReferenceErrorEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r13
	jmp	.L2079
	.p2align 4,,10
	.p2align 3
.L2091:
	leaq	.LC16(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L2092:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21198:
	.size	_ZN2v88internal25Runtime_NewReferenceErrorEiPmPNS0_7IsolateE, .-_ZN2v88internal25Runtime_NewReferenceErrorEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal22Runtime_NewSyntaxErrorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal22Runtime_NewSyntaxErrorEiPmPNS0_7IsolateE
	.type	_ZN2v88internal22Runtime_NewSyntaxErrorEiPmPNS0_7IsolateE, @function
_ZN2v88internal22Runtime_NewSyntaxErrorEiPmPNS0_7IsolateE:
.LFB21201:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2103
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L2104
.L2097:
	leaq	-52(%rbp), %rsi
	leaq	-48(%rbp), %rdi
	movl	$0, -52(%rbp)
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal6Object7ToInt32EPi@PLT
	testb	%al, %al
	je	.L2105
	movl	-52(%rbp), %esi
	leaq	-8(%r13), %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory14NewSyntaxErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	movq	(%rax), %r13
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L2093
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2093:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2106
	addq	$32, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2104:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	je	.L2097
	leaq	.LC14(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2103:
	movq	%r13, %rdi
	movq	%rdx, %rsi
	call	_ZN2v88internalL28Stats_Runtime_NewSyntaxErrorEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r13
	jmp	.L2093
	.p2align 4,,10
	.p2align 3
.L2105:
	leaq	.LC16(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L2106:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21201:
	.size	_ZN2v88internal22Runtime_NewSyntaxErrorEiPmPNS0_7IsolateE, .-_ZN2v88internal22Runtime_NewSyntaxErrorEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal32Runtime_ThrowInvalidStringLengthEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal32Runtime_ThrowInvalidStringLengthEiPmPNS0_7IsolateE
	.type	_ZN2v88internal32Runtime_ThrowInvalidStringLengthEiPmPNS0_7IsolateE, @function
_ZN2v88internal32Runtime_ThrowInvalidStringLengthEiPmPNS0_7IsolateE:
.LFB21204:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2111
	addl	$1, 41104(%rdx)
	movq	%rdx, %rdi
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	call	_ZN2v88internal7Factory27NewInvalidStringLengthErrorEv@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	41096(%r12), %rbx
	je	.L2109
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2109:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2111:
	.cfi_restore_state
	popq	%rbx
	movq	%rdx, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL38Stats_Runtime_ThrowInvalidStringLengthEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE21204:
	.size	_ZN2v88internal32Runtime_ThrowInvalidStringLengthEiPmPNS0_7IsolateE, .-_ZN2v88internal32Runtime_ThrowInvalidStringLengthEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal38Runtime_ThrowIteratorResultNotAnObjectEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal38Runtime_ThrowIteratorResultNotAnObjectEiPmPNS0_7IsolateE
	.type	_ZN2v88internal38Runtime_ThrowIteratorResultNotAnObjectEiPmPNS0_7IsolateE, @function
_ZN2v88internal38Runtime_ThrowIteratorResultNotAnObjectEiPmPNS0_7IsolateE:
.LFB21207:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2116
	addl	$1, 41104(%rdx)
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	movq	%rsi, %rdx
	movl	$68, %esi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r14
	cmpq	41096(%r12), %rbx
	je	.L2114
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2114:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2116:
	.cfi_restore_state
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	movq	%r9, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL44Stats_Runtime_ThrowIteratorResultNotAnObjectEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE21207:
	.size	_ZN2v88internal38Runtime_ThrowIteratorResultNotAnObjectEiPmPNS0_7IsolateE, .-_ZN2v88internal38Runtime_ThrowIteratorResultNotAnObjectEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal31Runtime_ThrowThrowMethodMissingEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal31Runtime_ThrowThrowMethodMissingEiPmPNS0_7IsolateE
	.type	_ZN2v88internal31Runtime_ThrowThrowMethodMissingEiPmPNS0_7IsolateE, @function
_ZN2v88internal31Runtime_ThrowThrowMethodMissingEiPmPNS0_7IsolateE:
.LFB21210:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2121
	addl	$1, 41104(%rdx)
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movl	$172, %esi
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	41096(%r12), %rbx
	je	.L2119
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2119:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2121:
	.cfi_restore_state
	popq	%rbx
	movq	%rdx, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL37Stats_Runtime_ThrowThrowMethodMissingEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE21210:
	.size	_ZN2v88internal31Runtime_ThrowThrowMethodMissingEiPmPNS0_7IsolateE, .-_ZN2v88internal31Runtime_ThrowThrowMethodMissingEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal34Runtime_ThrowSymbolIteratorInvalidEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal34Runtime_ThrowSymbolIteratorInvalidEiPmPNS0_7IsolateE
	.type	_ZN2v88internal34Runtime_ThrowSymbolIteratorInvalidEiPmPNS0_7IsolateE, @function
_ZN2v88internal34Runtime_ThrowSymbolIteratorInvalidEiPmPNS0_7IsolateE:
.LFB21213:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2126
	addl	$1, 41104(%rdx)
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movl	$167, %esi
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	41096(%r12), %rbx
	je	.L2124
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2124:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2126:
	.cfi_restore_state
	popq	%rbx
	movq	%rdx, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL40Stats_Runtime_ThrowSymbolIteratorInvalidEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE21213:
	.size	_ZN2v88internal34Runtime_ThrowSymbolIteratorInvalidEiPmPNS0_7IsolateE, .-_ZN2v88internal34Runtime_ThrowSymbolIteratorInvalidEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal27Runtime_ThrowNotConstructorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal27Runtime_ThrowNotConstructorEiPmPNS0_7IsolateE
	.type	_ZN2v88internal27Runtime_ThrowNotConstructorEiPmPNS0_7IsolateE, @function
_ZN2v88internal27Runtime_ThrowNotConstructorEiPmPNS0_7IsolateE:
.LFB21216:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2131
	addl	$1, 41104(%rdx)
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	movq	%rsi, %rdx
	movl	$90, %esi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r14
	cmpq	41096(%r12), %rbx
	je	.L2129
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2129:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2131:
	.cfi_restore_state
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	movq	%r9, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL33Stats_Runtime_ThrowNotConstructorEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE21216:
	.size	_ZN2v88internal27Runtime_ThrowNotConstructorEiPmPNS0_7IsolateE, .-_ZN2v88internal27Runtime_ThrowNotConstructorEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal29Runtime_ThrowApplyNonFunctionEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal29Runtime_ThrowApplyNonFunctionEiPmPNS0_7IsolateE
	.type	_ZN2v88internal29Runtime_ThrowApplyNonFunctionEiPmPNS0_7IsolateE, @function
_ZN2v88internal29Runtime_ThrowApplyNonFunctionEiPmPNS0_7IsolateE:
.LFB21219:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2136
	addl	$1, 41104(%rdx)
	movq	%rdx, %rdi
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	call	_ZN2v88internal6Object6TypeOfEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%r13, %rdx
	xorl	%r8d, %r8d
	movl	$11, %esi
	movq	%rax, %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	41096(%r12), %rbx
	je	.L2134
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2134:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2136:
	.cfi_restore_state
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	movq	%rdx, %rsi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL35Stats_Runtime_ThrowApplyNonFunctionEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE21219:
	.size	_ZN2v88internal29Runtime_ThrowApplyNonFunctionEiPmPNS0_7IsolateE, .-_ZN2v88internal29Runtime_ThrowApplyNonFunctionEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal18Runtime_StackGuardEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal18Runtime_StackGuardEiPmPNS0_7IsolateE
	.type	_ZN2v88internal18Runtime_StackGuardEiPmPNS0_7IsolateE, @function
_ZN2v88internal18Runtime_StackGuardEiPmPNS0_7IsolateE:
.LFB21222:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2162
	movq	_ZZN2v88internalL28__RT_impl_Runtime_StackGuardENS0_9ArgumentsEPNS0_7IsolateEE28trace_event_unique_atomic275(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2163
.L2141:
	movq	$0, -96(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2164
.L2143:
	xorl	%esi, %esi
	leaq	-104(%rbp), %rdi
	movq	%r12, -104(%rbp)
	call	_ZNK2v88internal15StackLimitCheck15JsHasOverflowedEm@PLT
	testb	%al, %al
	jne	.L2165
	leaq	37512(%r12), %rdi
	call	_ZN2v88internal10StackGuard16HandleInterruptsEv@PLT
	movq	%rax, %r12
.L2148:
	leaq	-96(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	%r12, %rax
.L2137:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2166
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2165:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate13StackOverflowEv@PLT
	movq	%rax, %r12
	jmp	.L2148
	.p2align 4,,10
	.p2align 3
.L2164:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2167
.L2144:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2145
	movq	(%rdi), %rax
	call	*8(%rax)
.L2145:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2146
	movq	(%rdi), %rax
	call	*8(%rax)
.L2146:
	leaq	.LC8(%rip), %rax
	movq	%rbx, -88(%rbp)
	movq	%rax, -80(%rbp)
	leaq	-88(%rbp), %rax
	movq	%r13, -72(%rbp)
	movq	%rax, -96(%rbp)
	jmp	.L2143
	.p2align 4,,10
	.p2align 3
.L2163:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2168
.L2142:
	movq	%rbx, _ZZN2v88internalL28__RT_impl_Runtime_StackGuardENS0_9ArgumentsEPNS0_7IsolateEE28trace_event_unique_atomic275(%rip)
	jmp	.L2141
	.p2align 4,,10
	.p2align 3
.L2162:
	movq	%rdx, %rdi
	call	_ZN2v88internalL24Stats_Runtime_StackGuardEiPmPNS0_7IsolateE.isra.0
	jmp	.L2137
	.p2align 4,,10
	.p2align 3
.L2168:
	leaq	.LC7(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2142
	.p2align 4,,10
	.p2align 3
.L2167:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC8(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L2144
.L2166:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21222:
	.size	_ZN2v88internal18Runtime_StackGuardEiPmPNS0_7IsolateE, .-_ZN2v88internal18Runtime_StackGuardEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal31Runtime_BytecodeBudgetInterruptEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal31Runtime_BytecodeBudgetInterruptEiPmPNS0_7IsolateE
	.type	_ZN2v88internal31Runtime_BytecodeBudgetInterruptEiPmPNS0_7IsolateE, @function
_ZN2v88internal31Runtime_BytecodeBudgetInterruptEiPmPNS0_7IsolateE:
.LFB21225:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2182
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L2183
.L2171:
	leaq	.LC77(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2183:
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L2171
	movl	_ZN2v88internal21FLAG_interrupt_budgetE(%rip), %edx
	movq	39(%rax), %rax
	movabsq	$287762808832, %rcx
	movl	%edx, 15(%rax)
	movq	(%rsi), %rdx
	movq	23(%rdx), %rax
	movq	7(%rax), %rax
	cmpq	%rcx, %rax
	je	.L2176
	testb	$1, %al
	jne	.L2174
.L2177:
	movq	39(%rdx), %rax
	movq	7(%rax), %rax
	movq	-1(%rax), %rax
	cmpw	$155, 11(%rax)
	je	.L2184
.L2176:
	movq	%r13, %rdi
	call	_ZN2v88internal10JSFunction20EnsureFeedbackVectorENS0_6HandleIS1_EE@PLT
	movq	0(%r13), %rax
	movq	39(%rax), %rax
	movq	7(%rax), %rax
	movl	$1, 35(%rax)
	movq	88(%r12), %r13
.L2178:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L2169
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2169:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2184:
	.cfi_restore_state
	leaq	37512(%r12), %rdi
	call	_ZN2v88internal10StackGuard16HandleInterruptsEv@PLT
	movq	%rax, %r13
	jmp	.L2178
	.p2align 4,,10
	.p2align 3
.L2174:
	movq	-1(%rax), %rcx
	cmpw	$165, 11(%rcx)
	je	.L2176
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L2177
	jmp	.L2176
	.p2align 4,,10
	.p2align 3
.L2182:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL37Stats_Runtime_BytecodeBudgetInterruptEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE21225:
	.size	_ZN2v88internal31Runtime_BytecodeBudgetInterruptEiPmPNS0_7IsolateE, .-_ZN2v88internal31Runtime_BytecodeBudgetInterruptEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal33Runtime_AllocateInYoungGenerationEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal33Runtime_AllocateInYoungGenerationEiPmPNS0_7IsolateE
	.type	_ZN2v88internal33Runtime_AllocateInYoungGenerationEiPmPNS0_7IsolateE, @function
_ZN2v88internal33Runtime_AllocateInYoungGenerationEiPmPNS0_7IsolateE:
.LFB21228:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2208
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rsi
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %sil
	jne	.L2209
	movq	-8(%rdi), %rax
	sarq	$32, %rsi
	testb	$1, %al
	jne	.L2210
	sarq	$32, %rax
	andl	$2, %eax
	testb	$7, %sil
	jne	.L2211
	testl	%esi, %esi
	jle	.L2212
	cmpl	$131072, %esi
	setg	%dl
	cmpb	$1, _ZN2v88internal35FLAG_young_generation_large_objectsE(%rip)
	je	.L2191
	testb	%dl, %dl
	jne	.L2213
.L2191:
	testl	%eax, %eax
	jne	.L2192
	testb	%dl, %dl
	jne	.L2214
.L2192:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory15NewFillerObjectEibNS0_14AllocationTypeENS0_16AllocationOriginE@PLT
	movq	(%rax), %r14
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L2185
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2185:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2208:
	.cfi_restore_state
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL39Stats_Runtime_AllocateInYoungGenerationEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L2209:
	.cfi_restore_state
	leaq	.LC29(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2210:
	leaq	.LC50(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2211:
	leaq	.LC51(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2212:
	leaq	.LC52(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2213:
	leaq	.LC53(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2214:
	leaq	.LC54(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21228:
	.size	_ZN2v88internal33Runtime_AllocateInYoungGenerationEiPmPNS0_7IsolateE, .-_ZN2v88internal33Runtime_AllocateInYoungGenerationEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal31Runtime_AllocateInOldGenerationEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal31Runtime_AllocateInOldGenerationEiPmPNS0_7IsolateE
	.type	_ZN2v88internal31Runtime_AllocateInOldGenerationEiPmPNS0_7IsolateE, @function
_ZN2v88internal31Runtime_AllocateInOldGenerationEiPmPNS0_7IsolateE:
.LFB21231:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2231
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rsi
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %sil
	jne	.L2232
	movq	-8(%rdi), %rdx
	sarq	$32, %rsi
	testb	$1, %dl
	jne	.L2233
	sarq	$32, %rdx
	movl	%edx, %eax
	andl	$2, %eax
	testb	$7, %sil
	jne	.L2234
	testl	%esi, %esi
	jle	.L2235
	testl	%eax, %eax
	jne	.L2221
	cmpl	$131072, %esi
	jg	.L2236
.L2221:
	andl	$1, %edx
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory15NewFillerObjectEibNS0_14AllocationTypeENS0_16AllocationOriginE@PLT
	movq	(%rax), %r14
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L2215
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2215:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2231:
	.cfi_restore_state
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL37Stats_Runtime_AllocateInOldGenerationEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L2232:
	.cfi_restore_state
	leaq	.LC29(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2233:
	leaq	.LC50(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2234:
	leaq	.LC51(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2235:
	leaq	.LC52(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2236:
	leaq	.LC54(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21231:
	.size	_ZN2v88internal31Runtime_AllocateInOldGenerationEiPmPNS0_7IsolateE, .-_ZN2v88internal31Runtime_AllocateInOldGenerationEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal25Runtime_AllocateByteArrayEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal25Runtime_AllocateByteArrayEiPmPNS0_7IsolateE
	.type	_ZN2v88internal25Runtime_AllocateByteArrayEiPmPNS0_7IsolateE, @function
_ZN2v88internal25Runtime_AllocateByteArrayEiPmPNS0_7IsolateE:
.LFB21234:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2242
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rsi
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %sil
	jne	.L2243
	sarq	$32, %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewByteArrayEiNS0_14AllocationTypeE@PLT
	movq	(%rax), %r14
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L2237
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2237:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2242:
	.cfi_restore_state
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL31Stats_Runtime_AllocateByteArrayEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L2243:
	.cfi_restore_state
	leaq	.LC29(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21234:
	.size	_ZN2v88internal25Runtime_AllocateByteArrayEiPmPNS0_7IsolateE, .-_ZN2v88internal25Runtime_AllocateByteArrayEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal32Runtime_AllocateSeqOneByteStringEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal32Runtime_AllocateSeqOneByteStringEiPmPNS0_7IsolateE
	.type	_ZN2v88internal32Runtime_AllocateSeqOneByteStringEiPmPNS0_7IsolateE, @function
_ZN2v88internal32Runtime_AllocateSeqOneByteStringEiPmPNS0_7IsolateE:
.LFB21237:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2253
	movl	41104(%rdx), %eax
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	leal	1(%rax), %edx
	movl	%edx, 41104(%r12)
	movq	(%rsi), %rsi
	testb	$1, %sil
	jne	.L2254
	sarq	$32, %rsi
	je	.L2255
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory19NewRawOneByteStringEiNS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	je	.L2256
	movq	(%rax), %r14
	movl	41104(%r12), %eax
	movq	41096(%r12), %rdx
	subl	$1, %eax
.L2250:
	movq	%r13, 41088(%r12)
	movl	%eax, 41104(%r12)
	cmpq	%rdx, %rbx
	je	.L2244
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2244:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2255:
	.cfi_restore_state
	movq	128(%r12), %r14
	popq	%rbx
	movl	%eax, 41104(%r12)
	popq	%r12
	popq	%r13
	movq	%r14, %rax
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2253:
	.cfi_restore_state
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL38Stats_Runtime_AllocateSeqOneByteStringEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L2254:
	.cfi_restore_state
	leaq	.LC29(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2256:
	movl	41104(%r12), %eax
	movq	312(%r12), %r14
	movq	41096(%r12), %rdx
	subl	$1, %eax
	jmp	.L2250
	.cfi_endproc
.LFE21237:
	.size	_ZN2v88internal32Runtime_AllocateSeqOneByteStringEiPmPNS0_7IsolateE, .-_ZN2v88internal32Runtime_AllocateSeqOneByteStringEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal32Runtime_AllocateSeqTwoByteStringEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal32Runtime_AllocateSeqTwoByteStringEiPmPNS0_7IsolateE
	.type	_ZN2v88internal32Runtime_AllocateSeqTwoByteStringEiPmPNS0_7IsolateE, @function
_ZN2v88internal32Runtime_AllocateSeqTwoByteStringEiPmPNS0_7IsolateE:
.LFB21240:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2266
	movl	41104(%rdx), %eax
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	leal	1(%rax), %edx
	movl	%edx, 41104(%r12)
	movq	(%rsi), %rsi
	testb	$1, %sil
	jne	.L2267
	sarq	$32, %rsi
	je	.L2268
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory19NewRawTwoByteStringEiNS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	je	.L2269
	movq	(%rax), %r14
	movl	41104(%r12), %eax
	movq	41096(%r12), %rdx
	subl	$1, %eax
.L2263:
	movq	%r13, 41088(%r12)
	movl	%eax, 41104(%r12)
	cmpq	%rdx, %rbx
	je	.L2257
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2257:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2268:
	.cfi_restore_state
	movq	128(%r12), %r14
	popq	%rbx
	movl	%eax, 41104(%r12)
	popq	%r12
	popq	%r13
	movq	%r14, %rax
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2266:
	.cfi_restore_state
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL38Stats_Runtime_AllocateSeqTwoByteStringEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L2267:
	.cfi_restore_state
	leaq	.LC29(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2269:
	movl	41104(%r12), %eax
	movq	312(%r12), %r14
	movq	41096(%r12), %rdx
	subl	$1, %eax
	jmp	.L2263
	.cfi_endproc
.LFE21240:
	.size	_ZN2v88internal32Runtime_AllocateSeqTwoByteStringEiPmPNS0_7IsolateE, .-_ZN2v88internal32Runtime_AllocateSeqTwoByteStringEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal26Runtime_ThrowIteratorErrorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal26Runtime_ThrowIteratorErrorEiPmPNS0_7IsolateE
	.type	_ZN2v88internal26Runtime_ThrowIteratorErrorEiPmPNS0_7IsolateE, @function
_ZN2v88internal26Runtime_ThrowIteratorErrorEiPmPNS0_7IsolateE:
.LFB21243:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2274
	addl	$1, 41104(%rdx)
	movq	%rdx, %rdi
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	call	_ZN2v88internal10ErrorUtils16NewIteratorErrorEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r14
	cmpq	41096(%r12), %rbx
	je	.L2272
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2272:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2274:
	.cfi_restore_state
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	movq	%r8, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL32Stats_Runtime_ThrowIteratorErrorEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE21243:
	.size	_ZN2v88internal26Runtime_ThrowIteratorErrorEiPmPNS0_7IsolateE, .-_ZN2v88internal26Runtime_ThrowIteratorErrorEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal30Runtime_ThrowCalledNonCallableEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal30Runtime_ThrowCalledNonCallableEiPmPNS0_7IsolateE
	.type	_ZN2v88internal30Runtime_ThrowCalledNonCallableEiPmPNS0_7IsolateE, @function
_ZN2v88internal30Runtime_ThrowCalledNonCallableEiPmPNS0_7IsolateE:
.LFB21246:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2279
	addl	$1, 41104(%rdx)
	movq	%rdx, %rdi
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	call	_ZN2v88internal10ErrorUtils25NewCalledNonCallableErrorEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r14
	cmpq	41096(%r12), %rbx
	je	.L2277
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2277:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2279:
	.cfi_restore_state
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	movq	%r8, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL36Stats_Runtime_ThrowCalledNonCallableEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE21246:
	.size	_ZN2v88internal30Runtime_ThrowCalledNonCallableEiPmPNS0_7IsolateE, .-_ZN2v88internal30Runtime_ThrowCalledNonCallableEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal40Runtime_ThrowConstructedNonConstructableEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal40Runtime_ThrowConstructedNonConstructableEiPmPNS0_7IsolateE
	.type	_ZN2v88internal40Runtime_ThrowConstructedNonConstructableEiPmPNS0_7IsolateE, @function
_ZN2v88internal40Runtime_ThrowConstructedNonConstructableEiPmPNS0_7IsolateE:
.LFB21249:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2284
	addl	$1, 41104(%rdx)
	movq	%rdx, %rdi
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	call	_ZN2v88internal10ErrorUtils30NewConstructedNonConstructableEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r14
	cmpq	41096(%r12), %rbx
	je	.L2282
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2282:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2284:
	.cfi_restore_state
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	movq	%r8, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL46Stats_Runtime_ThrowConstructedNonConstructableEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE21249:
	.size	_ZN2v88internal40Runtime_ThrowConstructedNonConstructableEiPmPNS0_7IsolateE, .-_ZN2v88internal40Runtime_ThrowConstructedNonConstructableEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal42Runtime_ThrowPatternAssignmentNonCoercibleEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal42Runtime_ThrowPatternAssignmentNonCoercibleEiPmPNS0_7IsolateE
	.type	_ZN2v88internal42Runtime_ThrowPatternAssignmentNonCoercibleEiPmPNS0_7IsolateE, @function
_ZN2v88internal42Runtime_ThrowPatternAssignmentNonCoercibleEiPmPNS0_7IsolateE:
.LFB21252:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2289
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r13
	movq	%r12, %rdi
	movq	41096(%rdx), %rbx
	xorl	%edx, %edx
	call	_ZN2v88internal10ErrorUtils28ThrowLoadFromNullOrUndefinedEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS0_11MaybeHandleIS5_EE@PLT
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r14
	cmpq	41096(%r12), %rbx
	je	.L2287
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2287:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2289:
	.cfi_restore_state
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	movq	%r8, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL48Stats_Runtime_ThrowPatternAssignmentNonCoercibleEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE21252:
	.size	_ZN2v88internal42Runtime_ThrowPatternAssignmentNonCoercibleEiPmPNS0_7IsolateE, .-_ZN2v88internal42Runtime_ThrowPatternAssignmentNonCoercibleEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal41Runtime_ThrowConstructorReturnedNonObjectEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal41Runtime_ThrowConstructorReturnedNonObjectEiPmPNS0_7IsolateE
	.type	_ZN2v88internal41Runtime_ThrowConstructorReturnedNonObjectEiPmPNS0_7IsolateE, @function
_ZN2v88internal41Runtime_ThrowConstructorReturnedNonObjectEiPmPNS0_7IsolateE:
.LFB21255:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2294
	addl	$1, 41104(%rdx)
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movl	$230, %esi
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	41096(%r12), %rbx
	je	.L2292
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2292:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2294:
	.cfi_restore_state
	popq	%rbx
	movq	%rdx, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL47Stats_Runtime_ThrowConstructorReturnedNonObjectEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE21255:
	.size	_ZN2v88internal41Runtime_ThrowConstructorReturnedNonObjectEiPmPNS0_7IsolateE, .-_ZN2v88internal41Runtime_ThrowConstructorReturnedNonObjectEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal31Runtime_CreateListFromArrayLikeEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal31Runtime_CreateListFromArrayLikeEiPmPNS0_7IsolateE
	.type	_ZN2v88internal31Runtime_CreateListFromArrayLikeEiPmPNS0_7IsolateE, @function
_ZN2v88internal31Runtime_CreateListFromArrayLikeEiPmPNS0_7IsolateE:
.LFB21258:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2302
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %rbx
	movq	%r12, %rdi
	movq	41096(%rdx), %r13
	xorl	%edx, %edx
	call	_ZN2v88internal6Object23CreateListFromArrayLikeEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementTypesE@PLT
	testq	%rax, %rax
	je	.L2303
	movq	(%rax), %r14
.L2298:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L2295
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2295:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2303:
	.cfi_restore_state
	movq	312(%r12), %r14
	jmp	.L2298
	.p2align 4,,10
	.p2align 3
.L2302:
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	movq	%r8, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL37Stats_Runtime_CreateListFromArrayLikeEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE21258:
	.size	_ZN2v88internal31Runtime_CreateListFromArrayLikeEiPmPNS0_7IsolateE, .-_ZN2v88internal31Runtime_CreateListFromArrayLikeEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal27Runtime_IncrementUseCounterEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal27Runtime_IncrementUseCounterEiPmPNS0_7IsolateE
	.type	_ZN2v88internal27Runtime_IncrementUseCounterEiPmPNS0_7IsolateE, @function
_ZN2v88internal27Runtime_IncrementUseCounterEiPmPNS0_7IsolateE:
.LFB21261:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2309
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rsi
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %sil
	jne	.L2310
	sarq	$32, %rsi
	movq	%rdx, %rdi
	call	_ZN2v88internal7Isolate10CountUsageENS_7Isolate17UseCounterFeatureE@PLT
	movq	%r13, 41088(%r12)
	movq	88(%r12), %r14
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L2304
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2304:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2309:
	.cfi_restore_state
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL33Stats_Runtime_IncrementUseCounterEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L2310:
	.cfi_restore_state
	leaq	.LC29(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21261:
	.size	_ZN2v88internal27Runtime_IncrementUseCounterEiPmPNS0_7IsolateE, .-_ZN2v88internal27Runtime_IncrementUseCounterEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal35Runtime_GetAndResetRuntimeCallStatsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal35Runtime_GetAndResetRuntimeCallStatsEiPmPNS0_7IsolateE
	.type	_ZN2v88internal35Runtime_GetAndResetRuntimeCallStatsEiPmPNS0_7IsolateE, @function
_ZN2v88internal35Runtime_GetAndResetRuntimeCallStatsEiPmPNS0_7IsolateE:
.LFB21264:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$520, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2342
	movq	41088(%rdx), %rax
	addl	$1, 41104(%rdx)
	movq	40960(%rdx), %rdi
	movq	%rax, -528(%rbp)
	movq	41096(%rdx), %rax
	leaq	23240(%rdi), %rsi
	addq	$50888, %rdi
	movq	%rax, -520(%rbp)
	call	_ZN2v88internal28WorkerThreadRuntimeCallStats14AddToMainTableEPNS0_16RuntimeCallStatsE@PLT
	testl	%r14d, %r14d
	je	.L2343
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L2344
	sarq	$32, %rax
	movq	stdout(%rip), %r15
	cmpq	$1, %rax
	cmovne	stderr(%rip), %r15
.L2328:
	cmpl	$1, %r14d
	jle	.L2330
	movq	-8(%r13), %rax
	testb	$1, %al
	jne	.L2345
.L2331:
	leaq	.LC46(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2343:
	movq	.LC70(%rip), %xmm1
	leaq	-320(%rbp), %r13
	leaq	-448(%rbp), %rbx
	movq	%r13, %rdi
	leaq	-432(%rbp), %r15
	leaq	-368(%rbp), %r14
	movhps	.LC71(%rip), %xmm1
	movaps	%xmm1, -544(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movw	%ax, -96(%rbp)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	%rax, -448(%rbp)
	movq	$0, -104(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	-448(%rbp), %rax
	movq	$0, -440(%rbp)
	addq	-24(%rax), %rbx
	movq	%rbx, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	xorl	%esi, %esi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r15, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movdqa	-544(%rbp), %xmm1
	movq	%rbx, -448(%rbp)
	movq	-24(%rbx), %rax
	movq	%rcx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r13, %rdi
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -552(%rbp)
	movq	%rax, -352(%rbp)
	movl	$24, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	40960(%r12), %rax
	movq	%r15, %rsi
	leaq	-464(%rbp), %r15
	leaq	23240(%rax), %rdi
	call	_ZN2v88internal16RuntimeCallStats5PrintERSo@PLT
	movq	-384(%rbp), %rax
	movq	%r15, -480(%rbp)
	leaq	-480(%rbp), %rdi
	movq	$0, -472(%rbp)
	movb	$0, -464(%rbp)
	testq	%rax, %rax
	je	.L2315
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L2346
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L2317:
	movq	-480(%rbp), %rdx
	movq	%rdx, %rdi
	movq	%rdx, -544(%rbp)
	call	strlen@PLT
	movq	-544(%rbp), %rdx
	leaq	-496(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -488(%rbp)
	movq	%rdx, -496(%rbp)
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	je	.L2347
	movq	-480(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L2319
	movq	%rax, -544(%rbp)
	call	_ZdlPv@PLT
	movq	-544(%rbp), %rax
.L2319:
	movq	.LC70(%rip), %xmm0
	movq	%rax, -560(%rbp)
	movq	40960(%r12), %rax
	movhps	.LC72(%rip), %xmm0
	leaq	23240(%rax), %rdi
	movaps	%xmm0, -544(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5ResetEv@PLT
	movq	-560(%rbp), %rax
	movdqa	-544(%rbp), %xmm0
	movq	-352(%rbp), %rdi
	movq	(%rax), %r15
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movaps	%xmm0, -432(%rbp)
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	cmpq	-552(%rbp), %rdi
	je	.L2320
	call	_ZdlPv@PLT
.L2320:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	%rbx, -448(%rbp)
	movq	-24(%rbx), %rax
	movq	%r13, %rdi
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%rcx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rbx, -432(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	$0, -440(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L2321
	.p2align 4,,10
	.p2align 3
.L2345:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L2331
	leaq	-496(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rax, -496(%rbp)
	call	_ZN2v88internal6String7PrintOnEP8_IO_FILE@PLT
	movl	$10, %edi
	movq	%r15, %rsi
	call	fputc@PLT
	movq	%r15, %rdi
	call	fflush@PLT
.L2330:
	leaq	-448(%rbp), %r14
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8OFStreamC1EP8_IO_FILE@PLT
	movq	40960(%r12), %rax
	movq	%r14, %rsi
	leaq	23240(%rax), %rdi
	call	_ZN2v88internal16RuntimeCallStats5PrintERSo@PLT
	movq	40960(%r12), %rax
	leaq	23240(%rax), %rdi
	call	_ZN2v88internal16RuntimeCallStats5ResetEv@PLT
	movq	0(%r13), %rax
	movq	%r15, %rdi
	testb	$1, %al
	jne	.L2348
.L2333:
	call	fflush@PLT
.L2335:
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rbx
	movq	88(%r12), %r15
	movq	%rax, -368(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rbx, %xmm0
	leaq	-384(%rbp), %rdi
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -448(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	-368(%rbp), %rdi
	movq	%rax, -448(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -368(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
.L2321:
	subl	$1, 41104(%r12)
	movq	-528(%rbp), %rax
	movq	%rax, 41088(%r12)
	movq	-520(%rbp), %rax
	cmpq	41096(%r12), %rax
	je	.L2311
	movq	%rax, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2311:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2349
	addq	$520, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2346:
	.cfi_restore_state
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L2317
	.p2align 4,,10
	.p2align 3
.L2344:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L2324
	leaq	.LC29(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2348:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L2333
	call	fclose@PLT
	jmp	.L2335
	.p2align 4,,10
	.p2align 3
.L2342:
	call	_ZN2v88internalL41Stats_Runtime_GetAndResetRuntimeCallStatsEiPmPNS0_7IsolateE
	movq	%rax, %r15
	jmp	.L2311
	.p2align 4,,10
	.p2align 3
.L2315:
	leaq	-352(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L2317
	.p2align 4,,10
	.p2align 3
.L2324:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L2350
	leaq	-497(%rbp), %rsi
	leaq	-496(%rbp), %rdi
	movq	%rax, -496(%rbp)
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	leaq	.LC69(%rip), %rsi
	movq	%rax, %rdi
	call	fopen@PLT
	movq	%rax, %r15
	jmp	.L2328
	.p2align 4,,10
	.p2align 3
.L2350:
	leaq	.LC68(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2347:
	leaq	.LC48(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L2349:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21264:
	.size	_ZN2v88internal35Runtime_GetAndResetRuntimeCallStatsEiPmPNS0_7IsolateE, .-_ZN2v88internal35Runtime_GetAndResetRuntimeCallStatsEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal27Runtime_OrdinaryHasInstanceEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal27Runtime_OrdinaryHasInstanceEiPmPNS0_7IsolateE
	.type	_ZN2v88internal27Runtime_OrdinaryHasInstanceEiPmPNS0_7IsolateE, @function
_ZN2v88internal27Runtime_OrdinaryHasInstanceEiPmPNS0_7IsolateE:
.LFB21267:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2358
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %rbx
	movq	%r12, %rdi
	movq	41096(%rdx), %r13
	leaq	-8(%rsi), %rdx
	call	_ZN2v88internal6Object19OrdinaryHasInstanceEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	testq	%rax, %rax
	je	.L2359
	movq	(%rax), %r14
.L2354:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L2351
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2351:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2359:
	.cfi_restore_state
	movq	312(%r12), %r14
	jmp	.L2354
	.p2align 4,,10
	.p2align 3
.L2358:
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	movq	%r8, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL33Stats_Runtime_OrdinaryHasInstanceEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE21267:
	.size	_ZN2v88internal27Runtime_OrdinaryHasInstanceEiPmPNS0_7IsolateE, .-_ZN2v88internal27Runtime_OrdinaryHasInstanceEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal14Runtime_TypeofEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal14Runtime_TypeofEiPmPNS0_7IsolateE
	.type	_ZN2v88internal14Runtime_TypeofEiPmPNS0_7IsolateE, @function
_ZN2v88internal14Runtime_TypeofEiPmPNS0_7IsolateE:
.LFB21270:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2364
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r14
	movq	%rdx, %rdi
	movq	41096(%rdx), %rbx
	call	_ZN2v88internal6Object6TypeOfEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%rax), %r13
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L2360
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2360:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2364:
	.cfi_restore_state
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	movq	%r8, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL20Stats_Runtime_TypeofEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE21270:
	.size	_ZN2v88internal14Runtime_TypeofEiPmPNS0_7IsolateE, .-_ZN2v88internal14Runtime_TypeofEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal28Runtime_AllowDynamicFunctionEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal28Runtime_AllowDynamicFunctionEiPmPNS0_7IsolateE
	.type	_ZN2v88internal28Runtime_AllowDynamicFunctionEiPmPNS0_7IsolateE, @function
_ZN2v88internal28Runtime_AllowDynamicFunctionEiPmPNS0_7IsolateE:
.LFB21273:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2378
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L2379
.L2368:
	leaq	.LC77(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2379:
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L2368
	movq	31(%rax), %rax
	leaq	-48(%rbp), %rdi
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal7Context12global_proxyEv@PLT
	movq	41112(%r12), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L2380
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L2371:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8Builtins20AllowDynamicFunctionEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEENS4_INS0_8JSObjectEEE@PLT
	movq	%r12, %rdi
	movzbl	%al, %esi
	call	_ZN2v88internal7Factory9ToBooleanEb@PLT
	movq	(%rax), %r13
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L2365
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2365:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2381
	addq	$32, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2380:
	.cfi_restore_state
	movq	41088(%r12), %rdx
	cmpq	41096(%r12), %rdx
	je	.L2382
.L2372:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdx)
	jmp	.L2371
	.p2align 4,,10
	.p2align 3
.L2378:
	call	_ZN2v88internalL34Stats_Runtime_AllowDynamicFunctionEiPmPNS0_7IsolateE
	movq	%rax, %r13
	jmp	.L2365
	.p2align 4,,10
	.p2align 3
.L2382:
	movq	%r12, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L2372
.L2381:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21273:
	.size	_ZN2v88internal28Runtime_AllowDynamicFunctionEiPmPNS0_7IsolateE, .-_ZN2v88internal28Runtime_AllowDynamicFunctionEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal35Runtime_CreateAsyncFromSyncIteratorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal35Runtime_CreateAsyncFromSyncIteratorEiPmPNS0_7IsolateE
	.type	_ZN2v88internal35Runtime_CreateAsyncFromSyncIteratorEiPmPNS0_7IsolateE, @function
_ZN2v88internal35Runtime_CreateAsyncFromSyncIteratorEiPmPNS0_7IsolateE:
.LFB21276:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %r15d
	testl	%r15d, %r15d
	jne	.L2403
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L2386
.L2388:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$167, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L2387:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L2383
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2383:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2404
	addq	$120, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2386:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$1023, 11(%rdx)
	jbe	.L2388
	movq	-1(%rax), %rax
	leaq	2912(%r12), %r8
	movq	%rsi, %rdx
	cmpw	$1023, 11(%rax)
	ja	.L2390
	movl	$-1, %edx
	movq	%r12, %rdi
	movq	%r8, -152(%rbp)
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	-152(%rbp), %r8
	movq	%rax, %rdx
.L2390:
	movq	2912(%r12), %rax
	movq	-1(%rax), %rcx
	cmpw	$64, 11(%rcx)
	je	.L2405
	movl	$3, %r15d
.L2392:
	movabsq	$824633720832, %rax
	movl	%r15d, -144(%rbp)
	movq	%rax, -132(%rbp)
	movq	2912(%r12), %rax
	movq	%r12, -120(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L2406
.L2393:
	leaq	-144(%rbp), %r15
	movq	%r8, -112(%rbp)
	movq	%r15, %rdi
	movq	$0, -104(%rbp)
	movq	%r13, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rdx, -80(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -140(%rbp)
	jne	.L2394
	movq	-120(%rbp), %rax
	leaq	88(%rax), %rdx
.L2395:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory26NewJSAsyncFromSyncIteratorENS0_6HandleINS0_10JSReceiverEEENS2_INS0_6ObjectEEE@PLT
	movq	(%rax), %r13
	jmp	.L2387
	.p2align 4,,10
	.p2align 3
.L2403:
	movq	%r13, %rdi
	movq	%rdx, %rsi
	call	_ZN2v88internalL41Stats_Runtime_CreateAsyncFromSyncIteratorEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r13
	jmp	.L2383
	.p2align 4,,10
	.p2align 3
.L2394:
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	jne	.L2395
	movq	312(%r12), %r13
	jmp	.L2387
	.p2align 4,,10
	.p2align 3
.L2405:
	testb	$1, 11(%rax)
	movl	$3, %eax
	cmove	%eax, %r15d
	jmp	.L2392
	.p2align 4,,10
	.p2align 3
.L2406:
	movq	%r8, %rsi
	movq	%r12, %rdi
	movq	%rdx, -152(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-152(%rbp), %rdx
	movq	%rax, %r8
	jmp	.L2393
.L2404:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21276:
	.size	_ZN2v88internal35Runtime_CreateAsyncFromSyncIteratorEiPmPNS0_7IsolateE, .-_ZN2v88internal35Runtime_CreateAsyncFromSyncIteratorEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal25Runtime_GetTemplateObjectEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal25Runtime_GetTemplateObjectEiPmPNS0_7IsolateE
	.type	_ZN2v88internal25Runtime_GetTemplateObjectEiPmPNS0_7IsolateE, @function
_ZN2v88internal25Runtime_GetTemplateObjectEiPmPNS0_7IsolateE:
.LFB21279:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2420
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L2409
.L2410:
	leaq	.LC58(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2409:
	movq	-1(%rax), %rax
	cmpw	$101, 11(%rax)
	jne	.L2410
	movq	-8(%rsi), %rax
	leaq	-8(%rsi), %rcx
	testb	$1, %al
	jne	.L2421
.L2411:
	leaq	.LC59(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2421:
	movq	-1(%rax), %rax
	cmpw	$160, 11(%rax)
	jne	.L2411
	movq	-16(%rsi), %r8
	testb	$1, %r8b
	jne	.L2422
	movq	12464(%rdx), %rax
	movq	%r8, %r15
	sarq	$32, %r15
	movq	39(%rax), %r8
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L2414
	movq	%r8, %rsi
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-56(%rbp), %rcx
	movq	%rax, %rsi
.L2415:
	movq	%r13, %rdx
	movl	%r15d, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal25TemplateObjectDescription17GetTemplateObjectEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEENS4_IS1_EENS4_INS0_18SharedFunctionInfoEEEi@PLT
	movq	(%rax), %r13
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L2407
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2407:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2414:
	.cfi_restore_state
	movq	%r14, %rsi
	cmpq	41096(%rdx), %r14
	je	.L2423
.L2416:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r8, (%rsi)
	jmp	.L2415
	.p2align 4,,10
	.p2align 3
.L2420:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL31Stats_Runtime_GetTemplateObjectEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L2422:
	.cfi_restore_state
	leaq	.LC60(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2423:
	movq	%rdx, %rdi
	movq	%r8, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %rcx
	movq	%rax, %rsi
	jmp	.L2416
	.cfi_endproc
.LFE21279:
	.size	_ZN2v88internal25Runtime_GetTemplateObjectEiPmPNS0_7IsolateE, .-_ZN2v88internal25Runtime_GetTemplateObjectEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal21Runtime_ReportMessageEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal21Runtime_ReportMessageEiPmPNS0_7IsolateE
	.type	_ZN2v88internal21Runtime_ReportMessageEiPmPNS0_7IsolateE, @function
_ZN2v88internal21Runtime_ReportMessageEiPmPNS0_7IsolateE:
.LFB21282:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2428
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	%rdx, %rdi
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%rax, 12480(%rdx)
	call	_ZN2v88internal7Isolate35ReportPendingMessagesFromJavaScriptEv@PLT
	movq	96(%r12), %rax
	movq	88(%r12), %r13
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, 12480(%r12)
	cmpq	41096(%r12), %rbx
	je	.L2424
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2424:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2428:
	.cfi_restore_state
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL27Stats_Runtime_ReportMessageEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE21282:
	.size	_ZN2v88internal21Runtime_ReportMessageEiPmPNS0_7IsolateE, .-_ZN2v88internal21Runtime_ReportMessageEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal30Runtime_GetInitializerFunctionEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal30Runtime_GetInitializerFunctionEiPmPNS0_7IsolateE
	.type	_ZN2v88internal30Runtime_GetInitializerFunctionEiPmPNS0_7IsolateE, @function
_ZN2v88internal30Runtime_GetInitializerFunctionEiPmPNS0_7IsolateE:
.LFB21285:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$96, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %ecx
	testl	%ecx, %ecx
	jne	.L2444
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	(%rsi), %rdx
	testb	$1, %dl
	jne	.L2432
.L2433:
	leaq	.LC62(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2432:
	movq	-1(%rdx), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L2433
	movq	3648(%r12), %rax
	andq	$-262144, %rdx
	leaq	3648(%r12), %rsi
	movq	24(%rdx), %rdi
	movq	-1(%rax), %rdx
	subq	$37592, %rdi
	cmpw	$64, 11(%rdx)
	je	.L2445
	movl	$2, %ecx
.L2434:
	movabsq	$824633720832, %rax
	movl	%ecx, -128(%rbp)
	movq	%rax, -116(%rbp)
	movq	3648(%r12), %rax
	movq	%rdi, -104(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %edx
	andl	$-32, %edx
	cmpl	$32, %edx
	je	.L2446
.L2435:
	movq	%r13, -80(%rbp)
	movq	%r13, -64(%rbp)
	leaq	-128(%rbp), %r13
	movq	%r13, %rdi
	movq	%rsi, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	$0, -72(%rbp)
	movq	$-1, -56(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -124(%rbp)
	jne	.L2436
	movq	-104(%rbp), %rax
	addq	$88, %rax
.L2437:
	movq	(%rax), %r13
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L2429
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2429:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2447
	addq	$96, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2436:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE@PLT
	jmp	.L2437
	.p2align 4,,10
	.p2align 3
.L2445:
	testb	$1, 11(%rax)
	movl	$2, %eax
	cmove	%eax, %ecx
	jmp	.L2434
	.p2align 4,,10
	.p2align 3
.L2446:
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rsi
	jmp	.L2435
	.p2align 4,,10
	.p2align 3
.L2444:
	call	_ZN2v88internalL36Stats_Runtime_GetInitializerFunctionEiPmPNS0_7IsolateE
	movq	%rax, %r13
	jmp	.L2429
.L2447:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21285:
	.size	_ZN2v88internal30Runtime_GetInitializerFunctionEiPmPNS0_7IsolateE, .-_ZN2v88internal30Runtime_GetInitializerFunctionEiPmPNS0_7IsolateE
	.section	.text._ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED2Ev,"axG",@progbits,_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED2Ev
	.type	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED2Ev, @function
_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED2Ev:
.LFB22238:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2448
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L2448:
	ret
	.cfi_endproc
.LFE22238:
	.size	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED2Ev, .-_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED2Ev
	.weak	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED1Ev
	.set	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED1Ev,_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED2Ev
	.section	.rodata._ZN2v88internalL50Stats_Runtime_FatalProcessOutOfMemoryInAllocateRawEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC83:
	.string	"V8.Runtime_Runtime_FatalProcessOutOfMemoryInAllocateRaw"
	.align 8
.LC84:
	.string	"CodeStubAssembler::AllocateRaw"
	.section	.text._ZN2v88internalL50Stats_Runtime_FatalProcessOutOfMemoryInAllocateRawEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL50Stats_Runtime_FatalProcessOutOfMemoryInAllocateRawEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL50Stats_Runtime_FatalProcessOutOfMemoryInAllocateRawEiPmPNS0_7IsolateE.isra.0:
.LFB26506:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$346, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	-112(%rbp), %rdi
	movq	%rbx, %rsi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal21RuntimeCallTimerScopeC1EPNS0_7IsolateENS0_20RuntimeCallCounterIdE
	movq	_ZZN2v88internalL50Stats_Runtime_FatalProcessOutOfMemoryInAllocateRawEiPmPNS0_7IsolateEE27trace_event_unique_atomic45(%rip), %rdx
	movq	%rdx, %r12
	testq	%rdx, %rdx
	je	.L2459
.L2452:
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L2460
.L2453:
	addl	$1, 41104(%rbx)
	leaq	37592(%rbx), %rdi
	leaq	.LC84(%rip), %rsi
	call	_ZN2v88internal4Heap23FatalProcessOutOfMemoryEPKc@PLT
	leaq	.LC47(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2459:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	.LC0(%rip), %rsi
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*16(%rax)
	movq	%rax, _ZZN2v88internalL50Stats_Runtime_FatalProcessOutOfMemoryInAllocateRawEiPmPNS0_7IsolateEE27trace_event_unique_atomic45(%rip)
	movq	%rax, %r12
	jmp	.L2452
.L2460:
	pxor	%xmm0, %xmm0
	leaq	-64(%rbp), %r13
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	movq	%rax, %rdi
	pushq	%rax
	leaq	.LC83(%rip), %rcx
	movl	$88, %esi
	movq	(%rdi), %rax
	pushq	$0
	pushq	%r13
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*24(%rax)
	leaq	-56(%rbp), %rdi
	addq	$64, %rsp
	call	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED1Ev
	movq	%r13, %rdi
	call	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED1Ev
	jmp	.L2453
	.cfi_endproc
.LFE26506:
	.size	_ZN2v88internalL50Stats_Runtime_FatalProcessOutOfMemoryInAllocateRawEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL50Stats_Runtime_FatalProcessOutOfMemoryInAllocateRawEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal44Runtime_FatalProcessOutOfMemoryInAllocateRawEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal44Runtime_FatalProcessOutOfMemoryInAllocateRawEiPmPNS0_7IsolateE
	.type	_ZN2v88internal44Runtime_FatalProcessOutOfMemoryInAllocateRawEiPmPNS0_7IsolateE, @function
_ZN2v88internal44Runtime_FatalProcessOutOfMemoryInAllocateRawEiPmPNS0_7IsolateE:
.LFB21152:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testl	%eax, %eax
	jne	.L2464
	addl	$1, 41104(%rdx)
	leaq	37592(%rdx), %rdi
	leaq	.LC84(%rip), %rsi
	call	_ZN2v88internal4Heap23FatalProcessOutOfMemoryEPKc@PLT
	leaq	.LC47(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2464:
	movq	%rdx, %rdi
	call	_ZN2v88internalL50Stats_Runtime_FatalProcessOutOfMemoryInAllocateRawEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE21152:
	.size	_ZN2v88internal44Runtime_FatalProcessOutOfMemoryInAllocateRawEiPmPNS0_7IsolateE, .-_ZN2v88internal44Runtime_FatalProcessOutOfMemoryInAllocateRawEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL55Stats_Runtime_FatalProcessOutOfMemoryInvalidArrayLengthEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC85:
	.string	"V8.Runtime_Runtime_FatalProcessOutOfMemoryInvalidArrayLength"
	.section	.rodata._ZN2v88internalL55Stats_Runtime_FatalProcessOutOfMemoryInvalidArrayLengthEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC86:
	.string	"invalid array length"
	.section	.text._ZN2v88internalL55Stats_Runtime_FatalProcessOutOfMemoryInvalidArrayLengthEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL55Stats_Runtime_FatalProcessOutOfMemoryInvalidArrayLengthEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL55Stats_Runtime_FatalProcessOutOfMemoryInvalidArrayLengthEiPmPNS0_7IsolateE.isra.0:
.LFB26507:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$347, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	-112(%rbp), %rdi
	movq	%rbx, %rsi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal21RuntimeCallTimerScopeC1EPNS0_7IsolateENS0_20RuntimeCallCounterIdE
	movq	_ZZN2v88internalL55Stats_Runtime_FatalProcessOutOfMemoryInvalidArrayLengthEiPmPNS0_7IsolateEE27trace_event_unique_atomic52(%rip), %rdx
	movq	%rdx, %r12
	testq	%rdx, %rdx
	je	.L2474
.L2467:
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L2475
.L2468:
	addl	$1, 41104(%rbx)
	leaq	37592(%rbx), %rdi
	leaq	.LC86(%rip), %rsi
	call	_ZN2v88internal4Heap23FatalProcessOutOfMemoryEPKc@PLT
	leaq	.LC47(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2474:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	.LC0(%rip), %rsi
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*16(%rax)
	movq	%rax, _ZZN2v88internalL55Stats_Runtime_FatalProcessOutOfMemoryInvalidArrayLengthEiPmPNS0_7IsolateEE27trace_event_unique_atomic52(%rip)
	movq	%rax, %r12
	jmp	.L2467
.L2475:
	pxor	%xmm0, %xmm0
	leaq	-64(%rbp), %r13
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	movq	%rax, %rdi
	pushq	%rax
	leaq	.LC85(%rip), %rcx
	movl	$88, %esi
	movq	(%rdi), %rax
	pushq	$0
	pushq	%r13
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*24(%rax)
	leaq	-56(%rbp), %rdi
	addq	$64, %rsp
	call	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED1Ev
	movq	%r13, %rdi
	call	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED1Ev
	jmp	.L2468
	.cfi_endproc
.LFE26507:
	.size	_ZN2v88internalL55Stats_Runtime_FatalProcessOutOfMemoryInvalidArrayLengthEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL55Stats_Runtime_FatalProcessOutOfMemoryInvalidArrayLengthEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal49Runtime_FatalProcessOutOfMemoryInvalidArrayLengthEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal49Runtime_FatalProcessOutOfMemoryInvalidArrayLengthEiPmPNS0_7IsolateE
	.type	_ZN2v88internal49Runtime_FatalProcessOutOfMemoryInvalidArrayLengthEiPmPNS0_7IsolateE, @function
_ZN2v88internal49Runtime_FatalProcessOutOfMemoryInvalidArrayLengthEiPmPNS0_7IsolateE:
.LFB21155:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testl	%eax, %eax
	jne	.L2479
	addl	$1, 41104(%rdx)
	leaq	37592(%rdx), %rdi
	leaq	.LC86(%rip), %rsi
	call	_ZN2v88internal4Heap23FatalProcessOutOfMemoryEPKc@PLT
	leaq	.LC47(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2479:
	movq	%rdx, %rdi
	call	_ZN2v88internalL55Stats_Runtime_FatalProcessOutOfMemoryInvalidArrayLengthEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE21155:
	.size	_ZN2v88internal49Runtime_FatalProcessOutOfMemoryInvalidArrayLengthEiPmPNS0_7IsolateE, .-_ZN2v88internal49Runtime_FatalProcessOutOfMemoryInvalidArrayLengthEiPmPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal19Runtime_AccessCheckEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal19Runtime_AccessCheckEiPmPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal19Runtime_AccessCheckEiPmPNS0_7IsolateE:
.LFB26487:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE26487:
	.size	_GLOBAL__sub_I__ZN2v88internal19Runtime_AccessCheckEiPmPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal19Runtime_AccessCheckEiPmPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal19Runtime_AccessCheckEiPmPNS0_7IsolateE
	.hidden	_ZTCN2v88internal8OFStreamE0_So
	.weak	_ZTCN2v88internal8OFStreamE0_So
	.section	.rodata._ZTCN2v88internal8OFStreamE0_So,"aG",@progbits,_ZTVN2v88internal8OFStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal8OFStreamE0_So, @object
	.size	_ZTCN2v88internal8OFStreamE0_So, 80
_ZTCN2v88internal8OFStreamE0_So:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.weak	_ZTTN2v88internal8OFStreamE
	.section	.data.rel.ro.local._ZTTN2v88internal8OFStreamE,"awG",@progbits,_ZTVN2v88internal8OFStreamE,comdat
	.align 8
	.type	_ZTTN2v88internal8OFStreamE, @object
	.size	_ZTTN2v88internal8OFStreamE, 32
_ZTTN2v88internal8OFStreamE:
	.quad	_ZTVN2v88internal8OFStreamE+24
	.quad	_ZTCN2v88internal8OFStreamE0_So+24
	.quad	_ZTCN2v88internal8OFStreamE0_So+64
	.quad	_ZTVN2v88internal8OFStreamE+64
	.weak	_ZTVN2v88internal8OFStreamE
	.section	.data.rel.ro.local._ZTVN2v88internal8OFStreamE,"awG",@progbits,_ZTVN2v88internal8OFStreamE,comdat
	.align 8
	.type	_ZTVN2v88internal8OFStreamE, @object
	.size	_ZTVN2v88internal8OFStreamE, 80
_ZTVN2v88internal8OFStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	_ZN2v88internal8OFStreamD1Ev
	.quad	_ZN2v88internal8OFStreamD0Ev
	.quad	-80
	.quad	-80
	.quad	0
	.quad	_ZTv0_n24_N2v88internal8OFStreamD1Ev
	.quad	_ZTv0_n24_N2v88internal8OFStreamD0Ev
	.section	.bss._ZZN2v88internalL36Stats_Runtime_GetInitializerFunctionEiPmPNS0_7IsolateEE28trace_event_unique_atomic565,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL36Stats_Runtime_GetInitializerFunctionEiPmPNS0_7IsolateEE28trace_event_unique_atomic565, @object
	.size	_ZZN2v88internalL36Stats_Runtime_GetInitializerFunctionEiPmPNS0_7IsolateEE28trace_event_unique_atomic565, 8
_ZZN2v88internalL36Stats_Runtime_GetInitializerFunctionEiPmPNS0_7IsolateEE28trace_event_unique_atomic565:
	.zero	8
	.section	.bss._ZZN2v88internalL27Stats_Runtime_ReportMessageEiPmPNS0_7IsolateEE28trace_event_unique_atomic548,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL27Stats_Runtime_ReportMessageEiPmPNS0_7IsolateEE28trace_event_unique_atomic548, @object
	.size	_ZZN2v88internalL27Stats_Runtime_ReportMessageEiPmPNS0_7IsolateEE28trace_event_unique_atomic548, 8
_ZZN2v88internalL27Stats_Runtime_ReportMessageEiPmPNS0_7IsolateEE28trace_event_unique_atomic548:
	.zero	8
	.section	.bss._ZZN2v88internalL31Stats_Runtime_GetTemplateObjectEiPmPNS0_7IsolateEE28trace_event_unique_atomic535,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL31Stats_Runtime_GetTemplateObjectEiPmPNS0_7IsolateEE28trace_event_unique_atomic535, @object
	.size	_ZZN2v88internalL31Stats_Runtime_GetTemplateObjectEiPmPNS0_7IsolateEE28trace_event_unique_atomic535, 8
_ZZN2v88internalL31Stats_Runtime_GetTemplateObjectEiPmPNS0_7IsolateEE28trace_event_unique_atomic535:
	.zero	8
	.section	.bss._ZZN2v88internalL41Stats_Runtime_CreateAsyncFromSyncIteratorEiPmPNS0_7IsolateEE28trace_event_unique_atomic514,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL41Stats_Runtime_CreateAsyncFromSyncIteratorEiPmPNS0_7IsolateEE28trace_event_unique_atomic514, @object
	.size	_ZZN2v88internalL41Stats_Runtime_CreateAsyncFromSyncIteratorEiPmPNS0_7IsolateEE28trace_event_unique_atomic514, 8
_ZZN2v88internalL41Stats_Runtime_CreateAsyncFromSyncIteratorEiPmPNS0_7IsolateEE28trace_event_unique_atomic514:
	.zero	8
	.section	.bss._ZZN2v88internalL34Stats_Runtime_AllowDynamicFunctionEiPmPNS0_7IsolateEE28trace_event_unique_atomic505,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL34Stats_Runtime_AllowDynamicFunctionEiPmPNS0_7IsolateEE28trace_event_unique_atomic505, @object
	.size	_ZZN2v88internalL34Stats_Runtime_AllowDynamicFunctionEiPmPNS0_7IsolateEE28trace_event_unique_atomic505, 8
_ZZN2v88internalL34Stats_Runtime_AllowDynamicFunctionEiPmPNS0_7IsolateEE28trace_event_unique_atomic505:
	.zero	8
	.section	.bss._ZZN2v88internalL20Stats_Runtime_TypeofEiPmPNS0_7IsolateEE28trace_event_unique_atomic498,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL20Stats_Runtime_TypeofEiPmPNS0_7IsolateEE28trace_event_unique_atomic498, @object
	.size	_ZZN2v88internalL20Stats_Runtime_TypeofEiPmPNS0_7IsolateEE28trace_event_unique_atomic498, 8
_ZZN2v88internalL20Stats_Runtime_TypeofEiPmPNS0_7IsolateEE28trace_event_unique_atomic498:
	.zero	8
	.section	.bss._ZZN2v88internalL33Stats_Runtime_OrdinaryHasInstanceEiPmPNS0_7IsolateEE28trace_event_unique_atomic489,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL33Stats_Runtime_OrdinaryHasInstanceEiPmPNS0_7IsolateEE28trace_event_unique_atomic489, @object
	.size	_ZZN2v88internalL33Stats_Runtime_OrdinaryHasInstanceEiPmPNS0_7IsolateEE28trace_event_unique_atomic489, 8
_ZZN2v88internalL33Stats_Runtime_OrdinaryHasInstanceEiPmPNS0_7IsolateEE28trace_event_unique_atomic489:
	.zero	8
	.section	.bss._ZZN2v88internalL41Stats_Runtime_GetAndResetRuntimeCallStatsEiPmPNS0_7IsolateEE28trace_event_unique_atomic436,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL41Stats_Runtime_GetAndResetRuntimeCallStatsEiPmPNS0_7IsolateEE28trace_event_unique_atomic436, @object
	.size	_ZZN2v88internalL41Stats_Runtime_GetAndResetRuntimeCallStatsEiPmPNS0_7IsolateEE28trace_event_unique_atomic436, 8
_ZZN2v88internalL41Stats_Runtime_GetAndResetRuntimeCallStatsEiPmPNS0_7IsolateEE28trace_event_unique_atomic436:
	.zero	8
	.section	.bss._ZZN2v88internalL33Stats_Runtime_IncrementUseCounterEiPmPNS0_7IsolateEE28trace_event_unique_atomic428,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL33Stats_Runtime_IncrementUseCounterEiPmPNS0_7IsolateEE28trace_event_unique_atomic428, @object
	.size	_ZZN2v88internalL33Stats_Runtime_IncrementUseCounterEiPmPNS0_7IsolateEE28trace_event_unique_atomic428, 8
_ZZN2v88internalL33Stats_Runtime_IncrementUseCounterEiPmPNS0_7IsolateEE28trace_event_unique_atomic428:
	.zero	8
	.section	.bss._ZZN2v88internalL37Stats_Runtime_CreateListFromArrayLikeEiPmPNS0_7IsolateEE28trace_event_unique_atomic420,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL37Stats_Runtime_CreateListFromArrayLikeEiPmPNS0_7IsolateEE28trace_event_unique_atomic420, @object
	.size	_ZZN2v88internalL37Stats_Runtime_CreateListFromArrayLikeEiPmPNS0_7IsolateEE28trace_event_unique_atomic420, 8
_ZZN2v88internalL37Stats_Runtime_CreateListFromArrayLikeEiPmPNS0_7IsolateEE28trace_event_unique_atomic420:
	.zero	8
	.section	.bss._ZZN2v88internalL47Stats_Runtime_ThrowConstructorReturnedNonObjectEiPmPNS0_7IsolateEE28trace_event_unique_atomic410,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL47Stats_Runtime_ThrowConstructorReturnedNonObjectEiPmPNS0_7IsolateEE28trace_event_unique_atomic410, @object
	.size	_ZZN2v88internalL47Stats_Runtime_ThrowConstructorReturnedNonObjectEiPmPNS0_7IsolateEE28trace_event_unique_atomic410, 8
_ZZN2v88internalL47Stats_Runtime_ThrowConstructorReturnedNonObjectEiPmPNS0_7IsolateEE28trace_event_unique_atomic410:
	.zero	8
	.section	.bss._ZZN2v88internalL48Stats_Runtime_ThrowPatternAssignmentNonCoercibleEiPmPNS0_7IsolateEE28trace_event_unique_atomic402,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL48Stats_Runtime_ThrowPatternAssignmentNonCoercibleEiPmPNS0_7IsolateEE28trace_event_unique_atomic402, @object
	.size	_ZZN2v88internalL48Stats_Runtime_ThrowPatternAssignmentNonCoercibleEiPmPNS0_7IsolateEE28trace_event_unique_atomic402, 8
_ZZN2v88internalL48Stats_Runtime_ThrowPatternAssignmentNonCoercibleEiPmPNS0_7IsolateEE28trace_event_unique_atomic402:
	.zero	8
	.section	.bss._ZZN2v88internalL46Stats_Runtime_ThrowConstructedNonConstructableEiPmPNS0_7IsolateEE28trace_event_unique_atomic394,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL46Stats_Runtime_ThrowConstructedNonConstructableEiPmPNS0_7IsolateEE28trace_event_unique_atomic394, @object
	.size	_ZZN2v88internalL46Stats_Runtime_ThrowConstructedNonConstructableEiPmPNS0_7IsolateEE28trace_event_unique_atomic394, 8
_ZZN2v88internalL46Stats_Runtime_ThrowConstructedNonConstructableEiPmPNS0_7IsolateEE28trace_event_unique_atomic394:
	.zero	8
	.section	.bss._ZZN2v88internalL36Stats_Runtime_ThrowCalledNonCallableEiPmPNS0_7IsolateEE28trace_event_unique_atomic386,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL36Stats_Runtime_ThrowCalledNonCallableEiPmPNS0_7IsolateEE28trace_event_unique_atomic386, @object
	.size	_ZZN2v88internalL36Stats_Runtime_ThrowCalledNonCallableEiPmPNS0_7IsolateEE28trace_event_unique_atomic386, 8
_ZZN2v88internalL36Stats_Runtime_ThrowCalledNonCallableEiPmPNS0_7IsolateEE28trace_event_unique_atomic386:
	.zero	8
	.section	.bss._ZZN2v88internalL32Stats_Runtime_ThrowIteratorErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic379,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL32Stats_Runtime_ThrowIteratorErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic379, @object
	.size	_ZZN2v88internalL32Stats_Runtime_ThrowIteratorErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic379, 8
_ZZN2v88internalL32Stats_Runtime_ThrowIteratorErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic379:
	.zero	8
	.section	.bss._ZZN2v88internalL38Stats_Runtime_AllocateSeqTwoByteStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic368,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL38Stats_Runtime_AllocateSeqTwoByteStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic368, @object
	.size	_ZZN2v88internalL38Stats_Runtime_AllocateSeqTwoByteStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic368, 8
_ZZN2v88internalL38Stats_Runtime_AllocateSeqTwoByteStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic368:
	.zero	8
	.section	.bss._ZZN2v88internalL38Stats_Runtime_AllocateSeqOneByteStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic357,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL38Stats_Runtime_AllocateSeqOneByteStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic357, @object
	.size	_ZZN2v88internalL38Stats_Runtime_AllocateSeqOneByteStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic357, 8
_ZZN2v88internalL38Stats_Runtime_AllocateSeqOneByteStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic357:
	.zero	8
	.section	.bss._ZZN2v88internalL31Stats_Runtime_AllocateByteArrayEiPmPNS0_7IsolateEE28trace_event_unique_atomic349,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL31Stats_Runtime_AllocateByteArrayEiPmPNS0_7IsolateEE28trace_event_unique_atomic349, @object
	.size	_ZZN2v88internalL31Stats_Runtime_AllocateByteArrayEiPmPNS0_7IsolateEE28trace_event_unique_atomic349, 8
_ZZN2v88internalL31Stats_Runtime_AllocateByteArrayEiPmPNS0_7IsolateEE28trace_event_unique_atomic349:
	.zero	8
	.section	.bss._ZZN2v88internalL37Stats_Runtime_AllocateInOldGenerationEiPmPNS0_7IsolateEE28trace_event_unique_atomic331,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL37Stats_Runtime_AllocateInOldGenerationEiPmPNS0_7IsolateEE28trace_event_unique_atomic331, @object
	.size	_ZZN2v88internalL37Stats_Runtime_AllocateInOldGenerationEiPmPNS0_7IsolateEE28trace_event_unique_atomic331, 8
_ZZN2v88internalL37Stats_Runtime_AllocateInOldGenerationEiPmPNS0_7IsolateEE28trace_event_unique_atomic331:
	.zero	8
	.section	.bss._ZZN2v88internalL39Stats_Runtime_AllocateInYoungGenerationEiPmPNS0_7IsolateEE28trace_event_unique_atomic306,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL39Stats_Runtime_AllocateInYoungGenerationEiPmPNS0_7IsolateEE28trace_event_unique_atomic306, @object
	.size	_ZZN2v88internalL39Stats_Runtime_AllocateInYoungGenerationEiPmPNS0_7IsolateEE28trace_event_unique_atomic306, 8
_ZZN2v88internalL39Stats_Runtime_AllocateInYoungGenerationEiPmPNS0_7IsolateEE28trace_event_unique_atomic306:
	.zero	8
	.section	.bss._ZZN2v88internalL37Stats_Runtime_BytecodeBudgetInterruptEiPmPNS0_7IsolateEE28trace_event_unique_atomic286,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL37Stats_Runtime_BytecodeBudgetInterruptEiPmPNS0_7IsolateEE28trace_event_unique_atomic286, @object
	.size	_ZZN2v88internalL37Stats_Runtime_BytecodeBudgetInterruptEiPmPNS0_7IsolateEE28trace_event_unique_atomic286, 8
_ZZN2v88internalL37Stats_Runtime_BytecodeBudgetInterruptEiPmPNS0_7IsolateEE28trace_event_unique_atomic286:
	.zero	8
	.section	.bss._ZZN2v88internalL28__RT_impl_Runtime_StackGuardENS0_9ArgumentsEPNS0_7IsolateEE28trace_event_unique_atomic275,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL28__RT_impl_Runtime_StackGuardENS0_9ArgumentsEPNS0_7IsolateEE28trace_event_unique_atomic275, @object
	.size	_ZZN2v88internalL28__RT_impl_Runtime_StackGuardENS0_9ArgumentsEPNS0_7IsolateEE28trace_event_unique_atomic275, 8
_ZZN2v88internalL28__RT_impl_Runtime_StackGuardENS0_9ArgumentsEPNS0_7IsolateEE28trace_event_unique_atomic275:
	.zero	8
	.section	.bss._ZZN2v88internalL24Stats_Runtime_StackGuardEiPmPNS0_7IsolateEE28trace_event_unique_atomic272,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL24Stats_Runtime_StackGuardEiPmPNS0_7IsolateEE28trace_event_unique_atomic272, @object
	.size	_ZZN2v88internalL24Stats_Runtime_StackGuardEiPmPNS0_7IsolateEE28trace_event_unique_atomic272, 8
_ZZN2v88internalL24Stats_Runtime_StackGuardEiPmPNS0_7IsolateEE28trace_event_unique_atomic272:
	.zero	8
	.section	.bss._ZZN2v88internalL35Stats_Runtime_ThrowApplyNonFunctionEiPmPNS0_7IsolateEE28trace_event_unique_atomic263,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL35Stats_Runtime_ThrowApplyNonFunctionEiPmPNS0_7IsolateEE28trace_event_unique_atomic263, @object
	.size	_ZZN2v88internalL35Stats_Runtime_ThrowApplyNonFunctionEiPmPNS0_7IsolateEE28trace_event_unique_atomic263, 8
_ZZN2v88internalL35Stats_Runtime_ThrowApplyNonFunctionEiPmPNS0_7IsolateEE28trace_event_unique_atomic263:
	.zero	8
	.section	.bss._ZZN2v88internalL33Stats_Runtime_ThrowNotConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic255,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL33Stats_Runtime_ThrowNotConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic255, @object
	.size	_ZZN2v88internalL33Stats_Runtime_ThrowNotConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic255, 8
_ZZN2v88internalL33Stats_Runtime_ThrowNotConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic255:
	.zero	8
	.section	.bss._ZZN2v88internalL40Stats_Runtime_ThrowSymbolIteratorInvalidEiPmPNS0_7IsolateEE28trace_event_unique_atomic248,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL40Stats_Runtime_ThrowSymbolIteratorInvalidEiPmPNS0_7IsolateEE28trace_event_unique_atomic248, @object
	.size	_ZZN2v88internalL40Stats_Runtime_ThrowSymbolIteratorInvalidEiPmPNS0_7IsolateEE28trace_event_unique_atomic248, 8
_ZZN2v88internalL40Stats_Runtime_ThrowSymbolIteratorInvalidEiPmPNS0_7IsolateEE28trace_event_unique_atomic248:
	.zero	8
	.section	.bss._ZZN2v88internalL37Stats_Runtime_ThrowThrowMethodMissingEiPmPNS0_7IsolateEE28trace_event_unique_atomic241,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL37Stats_Runtime_ThrowThrowMethodMissingEiPmPNS0_7IsolateEE28trace_event_unique_atomic241, @object
	.size	_ZZN2v88internalL37Stats_Runtime_ThrowThrowMethodMissingEiPmPNS0_7IsolateEE28trace_event_unique_atomic241, 8
_ZZN2v88internalL37Stats_Runtime_ThrowThrowMethodMissingEiPmPNS0_7IsolateEE28trace_event_unique_atomic241:
	.zero	8
	.section	.bss._ZZN2v88internalL44Stats_Runtime_ThrowIteratorResultNotAnObjectEiPmPNS0_7IsolateEE28trace_event_unique_atomic232,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL44Stats_Runtime_ThrowIteratorResultNotAnObjectEiPmPNS0_7IsolateEE28trace_event_unique_atomic232, @object
	.size	_ZZN2v88internalL44Stats_Runtime_ThrowIteratorResultNotAnObjectEiPmPNS0_7IsolateEE28trace_event_unique_atomic232, 8
_ZZN2v88internalL44Stats_Runtime_ThrowIteratorResultNotAnObjectEiPmPNS0_7IsolateEE28trace_event_unique_atomic232:
	.zero	8
	.section	.bss._ZZN2v88internalL38Stats_Runtime_ThrowInvalidStringLengthEiPmPNS0_7IsolateEE28trace_event_unique_atomic227,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL38Stats_Runtime_ThrowInvalidStringLengthEiPmPNS0_7IsolateEE28trace_event_unique_atomic227, @object
	.size	_ZZN2v88internalL38Stats_Runtime_ThrowInvalidStringLengthEiPmPNS0_7IsolateEE28trace_event_unique_atomic227, 8
_ZZN2v88internalL38Stats_Runtime_ThrowInvalidStringLengthEiPmPNS0_7IsolateEE28trace_event_unique_atomic227:
	.zero	8
	.section	.bss._ZZN2v88internalL28Stats_Runtime_NewSyntaxErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic218,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL28Stats_Runtime_NewSyntaxErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic218, @object
	.size	_ZZN2v88internalL28Stats_Runtime_NewSyntaxErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic218, 8
_ZZN2v88internalL28Stats_Runtime_NewSyntaxErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic218:
	.zero	8
	.section	.bss._ZZN2v88internalL31Stats_Runtime_NewReferenceErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic209,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL31Stats_Runtime_NewReferenceErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic209, @object
	.size	_ZZN2v88internalL31Stats_Runtime_NewReferenceErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic209, 8
_ZZN2v88internalL31Stats_Runtime_NewReferenceErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic209:
	.zero	8
	.section	.bss._ZZN2v88internalL26Stats_Runtime_NewTypeErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic200,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL26Stats_Runtime_NewTypeErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic200, @object
	.size	_ZZN2v88internalL26Stats_Runtime_NewTypeErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic200, 8
_ZZN2v88internalL26Stats_Runtime_NewTypeErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic200:
	.zero	8
	.section	.bss._ZZN2v88internalL48Stats_Runtime_ThrowAccessedUninitializedVariableEiPmPNS0_7IsolateEE28trace_event_unique_atomic191,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL48Stats_Runtime_ThrowAccessedUninitializedVariableEiPmPNS0_7IsolateEE28trace_event_unique_atomic191, @object
	.size	_ZZN2v88internalL48Stats_Runtime_ThrowAccessedUninitializedVariableEiPmPNS0_7IsolateEE28trace_event_unique_atomic191, 8
_ZZN2v88internalL48Stats_Runtime_ThrowAccessedUninitializedVariableEiPmPNS0_7IsolateEE28trace_event_unique_atomic191:
	.zero	8
	.section	.bss._ZZN2v88internalL33Stats_Runtime_ThrowReferenceErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic183,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL33Stats_Runtime_ThrowReferenceErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic183, @object
	.size	_ZZN2v88internalL33Stats_Runtime_ThrowReferenceErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic183, 8
_ZZN2v88internalL33Stats_Runtime_ThrowReferenceErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic183:
	.zero	8
	.section	.bss._ZZN2v88internalL39Stats_Runtime_PromoteScheduledExceptionEiPmPNS0_7IsolateEE28trace_event_unique_atomic177,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL39Stats_Runtime_PromoteScheduledExceptionEiPmPNS0_7IsolateEE28trace_event_unique_atomic177, @object
	.size	_ZZN2v88internalL39Stats_Runtime_PromoteScheduledExceptionEiPmPNS0_7IsolateEE28trace_event_unique_atomic177, 8
_ZZN2v88internalL39Stats_Runtime_PromoteScheduledExceptionEiPmPNS0_7IsolateEE28trace_event_unique_atomic177:
	.zero	8
	.section	.bss._ZZN2v88internalL43Stats_Runtime_UnwindAndFindExceptionHandlerEiPmPNS0_7IsolateEE28trace_event_unique_atomic171,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL43Stats_Runtime_UnwindAndFindExceptionHandlerEiPmPNS0_7IsolateEE28trace_event_unique_atomic171, @object
	.size	_ZZN2v88internalL43Stats_Runtime_UnwindAndFindExceptionHandlerEiPmPNS0_7IsolateEE28trace_event_unique_atomic171, 8
_ZZN2v88internalL43Stats_Runtime_UnwindAndFindExceptionHandlerEiPmPNS0_7IsolateEE28trace_event_unique_atomic171:
	.zero	8
	.section	.bss._ZZN2v88internalL45Stats_Runtime_ThrowInvalidTypedArrayAlignmentEiPmPNS0_7IsolateEE28trace_event_unique_atomic149,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL45Stats_Runtime_ThrowInvalidTypedArrayAlignmentEiPmPNS0_7IsolateEE28trace_event_unique_atomic149, @object
	.size	_ZZN2v88internalL45Stats_Runtime_ThrowInvalidTypedArrayAlignmentEiPmPNS0_7IsolateEE28trace_event_unique_atomic149, 8
_ZZN2v88internalL45Stats_Runtime_ThrowInvalidTypedArrayAlignmentEiPmPNS0_7IsolateEE28trace_event_unique_atomic149:
	.zero	8
	.section	.bss._ZZN2v88internalL36Stats_Runtime_ThrowTypeErrorIfStrictEiPmPNS0_7IsolateEE28trace_event_unique_atomic122,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL36Stats_Runtime_ThrowTypeErrorIfStrictEiPmPNS0_7IsolateEE28trace_event_unique_atomic122, @object
	.size	_ZZN2v88internalL36Stats_Runtime_ThrowTypeErrorIfStrictEiPmPNS0_7IsolateEE28trace_event_unique_atomic122, 8
_ZZN2v88internalL36Stats_Runtime_ThrowTypeErrorIfStrictEiPmPNS0_7IsolateEE28trace_event_unique_atomic122:
	.zero	8
	.section	.bss._ZZN2v88internalL28Stats_Runtime_ThrowTypeErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic118,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL28Stats_Runtime_ThrowTypeErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic118, @object
	.size	_ZZN2v88internalL28Stats_Runtime_ThrowTypeErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic118, 8
_ZZN2v88internalL28Stats_Runtime_ThrowTypeErrorEiPmPNS0_7IsolateEE28trace_event_unique_atomic118:
	.zero	8
	.section	.bss._ZZN2v88internalL29Stats_Runtime_ThrowRangeErrorEiPmPNS0_7IsolateEE27trace_event_unique_atomic98,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL29Stats_Runtime_ThrowRangeErrorEiPmPNS0_7IsolateEE27trace_event_unique_atomic98, @object
	.size	_ZZN2v88internalL29Stats_Runtime_ThrowRangeErrorEiPmPNS0_7IsolateEE27trace_event_unique_atomic98, 8
_ZZN2v88internalL29Stats_Runtime_ThrowRangeErrorEiPmPNS0_7IsolateEE27trace_event_unique_atomic98:
	.zero	8
	.section	.bss._ZZN2v88internalL45Stats_Runtime_ThrowSymbolAsyncIteratorInvalidEiPmPNS0_7IsolateEE27trace_event_unique_atomic77,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL45Stats_Runtime_ThrowSymbolAsyncIteratorInvalidEiPmPNS0_7IsolateEE27trace_event_unique_atomic77, @object
	.size	_ZZN2v88internalL45Stats_Runtime_ThrowSymbolAsyncIteratorInvalidEiPmPNS0_7IsolateEE27trace_event_unique_atomic77, 8
_ZZN2v88internalL45Stats_Runtime_ThrowSymbolAsyncIteratorInvalidEiPmPNS0_7IsolateEE27trace_event_unique_atomic77:
	.zero	8
	.section	.bss._ZZN2v88internalL32Stats_Runtime_ThrowStackOverflowEiPmPNS0_7IsolateEE27trace_event_unique_atomic71,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL32Stats_Runtime_ThrowStackOverflowEiPmPNS0_7IsolateEE27trace_event_unique_atomic71, @object
	.size	_ZZN2v88internalL32Stats_Runtime_ThrowStackOverflowEiPmPNS0_7IsolateEE27trace_event_unique_atomic71, 8
_ZZN2v88internalL32Stats_Runtime_ThrowStackOverflowEiPmPNS0_7IsolateEE27trace_event_unique_atomic71:
	.zero	8
	.section	.bss._ZZN2v88internalL21Stats_Runtime_ReThrowEiPmPNS0_7IsolateEE27trace_event_unique_atomic65,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL21Stats_Runtime_ReThrowEiPmPNS0_7IsolateEE27trace_event_unique_atomic65, @object
	.size	_ZZN2v88internalL21Stats_Runtime_ReThrowEiPmPNS0_7IsolateEE27trace_event_unique_atomic65, 8
_ZZN2v88internalL21Stats_Runtime_ReThrowEiPmPNS0_7IsolateEE27trace_event_unique_atomic65:
	.zero	8
	.section	.bss._ZZN2v88internalL19Stats_Runtime_ThrowEiPmPNS0_7IsolateEE27trace_event_unique_atomic59,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL19Stats_Runtime_ThrowEiPmPNS0_7IsolateEE27trace_event_unique_atomic59, @object
	.size	_ZZN2v88internalL19Stats_Runtime_ThrowEiPmPNS0_7IsolateEE27trace_event_unique_atomic59, 8
_ZZN2v88internalL19Stats_Runtime_ThrowEiPmPNS0_7IsolateEE27trace_event_unique_atomic59:
	.zero	8
	.section	.bss._ZZN2v88internalL55Stats_Runtime_FatalProcessOutOfMemoryInvalidArrayLengthEiPmPNS0_7IsolateEE27trace_event_unique_atomic52,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL55Stats_Runtime_FatalProcessOutOfMemoryInvalidArrayLengthEiPmPNS0_7IsolateEE27trace_event_unique_atomic52, @object
	.size	_ZZN2v88internalL55Stats_Runtime_FatalProcessOutOfMemoryInvalidArrayLengthEiPmPNS0_7IsolateEE27trace_event_unique_atomic52, 8
_ZZN2v88internalL55Stats_Runtime_FatalProcessOutOfMemoryInvalidArrayLengthEiPmPNS0_7IsolateEE27trace_event_unique_atomic52:
	.zero	8
	.section	.bss._ZZN2v88internalL50Stats_Runtime_FatalProcessOutOfMemoryInAllocateRawEiPmPNS0_7IsolateEE27trace_event_unique_atomic45,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL50Stats_Runtime_FatalProcessOutOfMemoryInAllocateRawEiPmPNS0_7IsolateEE27trace_event_unique_atomic45, @object
	.size	_ZZN2v88internalL50Stats_Runtime_FatalProcessOutOfMemoryInAllocateRawEiPmPNS0_7IsolateEE27trace_event_unique_atomic45, 8
_ZZN2v88internalL50Stats_Runtime_FatalProcessOutOfMemoryInAllocateRawEiPmPNS0_7IsolateEE27trace_event_unique_atomic45:
	.zero	8
	.section	.bss._ZZN2v88internalL25Stats_Runtime_AccessCheckEiPmPNS0_7IsolateEE27trace_event_unique_atomic34,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL25Stats_Runtime_AccessCheckEiPmPNS0_7IsolateEE27trace_event_unique_atomic34, @object
	.size	_ZZN2v88internalL25Stats_Runtime_AccessCheckEiPmPNS0_7IsolateEE27trace_event_unique_atomic34, 8
_ZZN2v88internalL25Stats_Runtime_AccessCheckEiPmPNS0_7IsolateEE27trace_event_unique_atomic34:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.section	.data.rel.ro,"aw"
	.align 8
.LC70:
	.quad	_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE+64
	.align 8
.LC71:
	.quad	_ZTVSt15basic_streambufIcSt11char_traitsIcEE+16
	.align 8
.LC72:
	.quad	_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE+16
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
