	.file	"source-position-table.cc"
	.text
	.section	.text._ZN2v88internal26SourcePositionTableBuilderC2ENS1_13RecordingModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26SourcePositionTableBuilderC2ENS1_13RecordingModeE
	.type	_ZN2v88internal26SourcePositionTableBuilderC2ENS1_13RecordingModeE, @function
_ZN2v88internal26SourcePositionTableBuilderC2ENS1_13RecordingModeE:
.LFB17799:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movl	%esi, (%rdi)
	movq	$0, 24(%rdi)
	movl	$0, 32(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE17799:
	.size	_ZN2v88internal26SourcePositionTableBuilderC2ENS1_13RecordingModeE, .-_ZN2v88internal26SourcePositionTableBuilderC2ENS1_13RecordingModeE
	.globl	_ZN2v88internal26SourcePositionTableBuilderC1ENS1_13RecordingModeE
	.set	_ZN2v88internal26SourcePositionTableBuilderC1ENS1_13RecordingModeE,_ZN2v88internal26SourcePositionTableBuilderC2ENS1_13RecordingModeE
	.section	.text._ZN2v88internal26SourcePositionTableBuilder21ToSourcePositionTableEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26SourcePositionTableBuilder21ToSourcePositionTableEPNS0_7IsolateE
	.type	_ZN2v88internal26SourcePositionTableBuilder21ToSourcePositionTableEPNS0_7IsolateE, @function
_ZN2v88internal26SourcePositionTableBuilder21ToSourcePositionTableEPNS0_7IsolateE:
.LFB17803:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	movq	8(%r12), %rax
	movq	16(%r12), %rsi
	pushq	%rbx
	.cfi_offset 3, -32
	cmpq	%rsi, %rax
	je	.L7
	subq	%rax, %rsi
	movl	$1, %edx
	call	_ZN2v88internal7Factory12NewByteArrayEiNS0_14AllocationTypeE@PLT
	movq	8(%r12), %rsi
	movq	16(%r12), %rdx
	movq	%rax, %rbx
	movq	(%rax), %rax
	subq	%rsi, %rdx
	leaq	15(%rax), %rdi
	call	memcpy@PLT
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	popq	%rbx
	leaq	976(%rdi), %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17803:
	.size	_ZN2v88internal26SourcePositionTableBuilder21ToSourcePositionTableEPNS0_7IsolateE, .-_ZN2v88internal26SourcePositionTableBuilder21ToSourcePositionTableEPNS0_7IsolateE
	.section	.text._ZN2v88internal26SourcePositionTableBuilder27ToSourcePositionTableVectorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26SourcePositionTableBuilder27ToSourcePositionTableVectorEv
	.type	_ZN2v88internal26SourcePositionTableBuilder27ToSourcePositionTableVectorEv, @function
_ZN2v88internal26SourcePositionTableBuilder27ToSourcePositionTableVectorEv:
.LFB17804:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	16(%rsi), %rax
	movq	8(%rsi), %r13
	cmpq	%r13, %rax
	je	.L12
	subq	%r13, %rax
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_Znam@PLT
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	memmove@PLT
	movq	%rbx, 8(%r12)
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	movq	$0, (%rdi)
	movq	%r12, %rax
	movq	$0, 8(%rdi)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17804:
	.size	_ZN2v88internal26SourcePositionTableBuilder27ToSourcePositionTableVectorEv, .-_ZN2v88internal26SourcePositionTableBuilder27ToSourcePositionTableVectorEv
	.section	.text._ZN2v88internal27SourcePositionTableIterator7AdvanceEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal27SourcePositionTableIterator7AdvanceEv
	.type	_ZN2v88internal27SourcePositionTableIterator7AdvanceEv, @function
_ZN2v88internal27SourcePositionTableIterator7AdvanceEv:
.LFB17850:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	16(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	testq	%rax, %rax
	je	.L41
	movq	(%rax), %r8
	movslq	11(%r8), %r9
	addq	$15, %r8
.L15:
	movl	24(%rdi), %ebx
.L40:
	cmpl	$-1, %ebx
	je	.L13
	cmpl	%ebx, %r9d
	jg	.L38
	movl	$-1, 24(%rdi)
.L13:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_restore_state
	movl	24(%rdi), %eax
	xorl	%esi, %esi
	xorl	%ecx, %ecx
	leal	1(%rax), %edx
	movslq	%edx, %rdx
	.p2align 4,,10
	.p2align 3
.L21:
	movl	%edx, 24(%rdi)
	movzbl	-1(%r8,%rdx), %r11d
	movl	%edx, %eax
	addq	$1, %rdx
	movl	%r11d, %r10d
	andl	$127, %r10d
	sall	%cl, %r10d
	addl	$7, %ecx
	orl	%r10d, %esi
	testb	%r11b, %r11b
	js	.L21
	movl	%esi, %edx
	andl	$1, %esi
	movl	$1, %r11d
	sarl	%edx
	negl	%esi
	xorl	%edx, %esi
	jns	.L22
	notl	%esi
	xorl	%r11d, %r11d
.L22:
	addl	$1, %eax
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	cltq
	.p2align 4,,10
	.p2align 3
.L23:
	movl	%eax, 24(%rdi)
	movzbl	-1(%r8,%rax), %r12d
	movl	%eax, %ebx
	addq	$1, %rax
	movq	%r12, %r10
	andl	$127, %r10d
	salq	%cl, %r10
	addl	$7, %ecx
	orq	%r10, %rdx
	testb	%r12b, %r12b
	js	.L23
	movq	%rdx, %rax
	andl	$1, %edx
	addl	%esi, 32(%rdi)
	sarq	%rax
	negq	%rdx
	movb	%r11b, 48(%rdi)
	xorq	%rax, %rdx
	movl	56(%rdi), %eax
	addq	40(%rdi), %rdx
	movq	%rdx, 40(%rdi)
	cmpl	$2, %eax
	je	.L13
	testl	%eax, %eax
	jne	.L25
	andl	$1, %edx
	jne	.L40
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	cmpl	$1, %eax
	jne	.L40
	andl	$1, %edx
	je	.L40
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
	movq	(%rdi), %r8
	movq	8(%rdi), %r9
	jmp	.L15
	.cfi_endproc
.LFE17850:
	.size	_ZN2v88internal27SourcePositionTableIterator7AdvanceEv, .-_ZN2v88internal27SourcePositionTableIterator7AdvanceEv
	.section	.text._ZN2v88internal27SourcePositionTableIteratorC2ENS0_6HandleINS0_9ByteArrayEEENS1_15IterationFilterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal27SourcePositionTableIteratorC2ENS0_6HandleINS0_9ByteArrayEEENS1_15IterationFilterE
	.type	_ZN2v88internal27SourcePositionTableIteratorC2ENS0_6HandleINS0_9ByteArrayEEENS1_15IterationFilterE, @function
_ZN2v88internal27SourcePositionTableIteratorC2ENS0_6HandleINS0_9ByteArrayEEENS1_15IterationFilterE:
.LFB17845:
	.cfi_startproc
	endbr64
	movq	$0, (%rdi)
	movq	$0, 8(%rdi)
	movq	%rsi, 16(%rdi)
	movl	$0, 24(%rdi)
	movl	$0, 32(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movl	%edx, 56(%rdi)
	jmp	_ZN2v88internal27SourcePositionTableIterator7AdvanceEv
	.cfi_endproc
.LFE17845:
	.size	_ZN2v88internal27SourcePositionTableIteratorC2ENS0_6HandleINS0_9ByteArrayEEENS1_15IterationFilterE, .-_ZN2v88internal27SourcePositionTableIteratorC2ENS0_6HandleINS0_9ByteArrayEEENS1_15IterationFilterE
	.globl	_ZN2v88internal27SourcePositionTableIteratorC1ENS0_6HandleINS0_9ByteArrayEEENS1_15IterationFilterE
	.set	_ZN2v88internal27SourcePositionTableIteratorC1ENS0_6HandleINS0_9ByteArrayEEENS1_15IterationFilterE,_ZN2v88internal27SourcePositionTableIteratorC2ENS0_6HandleINS0_9ByteArrayEEENS1_15IterationFilterE
	.section	.text._ZN2v88internal27SourcePositionTableIteratorC2ENS0_6VectorIKhEENS1_15IterationFilterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal27SourcePositionTableIteratorC2ENS0_6VectorIKhEENS1_15IterationFilterE
	.type	_ZN2v88internal27SourcePositionTableIteratorC2ENS0_6VectorIKhEENS1_15IterationFilterE, @function
_ZN2v88internal27SourcePositionTableIteratorC2ENS0_6VectorIKhEENS1_15IterationFilterE:
.LFB17848:
	.cfi_startproc
	endbr64
	movq	%rsi, (%rdi)
	movq	%rdx, 8(%rdi)
	movq	$0, 16(%rdi)
	movl	$0, 24(%rdi)
	movl	$0, 32(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movl	%ecx, 56(%rdi)
	jmp	_ZN2v88internal27SourcePositionTableIterator7AdvanceEv
	.cfi_endproc
.LFE17848:
	.size	_ZN2v88internal27SourcePositionTableIteratorC2ENS0_6VectorIKhEENS1_15IterationFilterE, .-_ZN2v88internal27SourcePositionTableIteratorC2ENS0_6VectorIKhEENS1_15IterationFilterE
	.globl	_ZN2v88internal27SourcePositionTableIteratorC1ENS0_6VectorIKhEENS1_15IterationFilterE
	.set	_ZN2v88internal27SourcePositionTableIteratorC1ENS0_6VectorIKhEENS1_15IterationFilterE,_ZN2v88internal27SourcePositionTableIteratorC2ENS0_6VectorIKhEENS1_15IterationFilterE
	.section	.text._ZN2v88internal27SourcePositionTableIteratorC2ENS0_9ByteArrayENS1_15IterationFilterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal27SourcePositionTableIteratorC2ENS0_9ByteArrayENS1_15IterationFilterE
	.type	_ZN2v88internal27SourcePositionTableIteratorC2ENS0_9ByteArrayENS1_15IterationFilterE, @function
_ZN2v88internal27SourcePositionTableIteratorC2ENS0_9ByteArrayENS1_15IterationFilterE:
.LFB17842:
	.cfi_startproc
	endbr64
	movslq	11(%rsi), %rax
	addq	$15, %rsi
	movb	$0, 48(%rdi)
	movq	%rsi, (%rdi)
	movq	%rax, 8(%rdi)
	movq	$0, 16(%rdi)
	movl	$0, 24(%rdi)
	movl	$0, 32(%rdi)
	movq	$0, 40(%rdi)
	movl	%edx, 56(%rdi)
	jmp	_ZN2v88internal27SourcePositionTableIterator7AdvanceEv
	.cfi_endproc
.LFE17842:
	.size	_ZN2v88internal27SourcePositionTableIteratorC2ENS0_9ByteArrayENS1_15IterationFilterE, .-_ZN2v88internal27SourcePositionTableIteratorC2ENS0_9ByteArrayENS1_15IterationFilterE
	.globl	_ZN2v88internal27SourcePositionTableIteratorC1ENS0_9ByteArrayENS1_15IterationFilterE
	.set	_ZN2v88internal27SourcePositionTableIteratorC1ENS0_9ByteArrayENS1_15IterationFilterE,_ZN2v88internal27SourcePositionTableIteratorC2ENS0_9ByteArrayENS1_15IterationFilterE
	.section	.rodata._ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_.str1.1,"aMS",@progbits,1
.LC0:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_,"axG",@progbits,_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	.type	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_, @function
_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_:
.LFB21277:
	.cfi_startproc
	endbr64
	movabsq	$9223372036854775807, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rsi
	movq	(%rdi), %r15
	movq	%rsi, %rax
	subq	%r15, %rax
	cmpq	%rcx, %rax
	je	.L59
	movq	%rdx, %r13
	movq	%r8, %rdx
	movq	%rdi, %r12
	subq	%r15, %rdx
	testq	%rax, %rax
	je	.L54
	leaq	(%rax,%rax), %r14
	cmpq	%r14, %rax
	jbe	.L60
.L56:
	movq	%rcx, %r14
.L47:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L53:
	movzbl	0(%r13), %eax
	subq	%r8, %rsi
	leaq	1(%rbx,%rdx), %r10
	movq	%rsi, %r13
	movb	%al, (%rbx,%rdx)
	leaq	(%r10,%rsi), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L61
	testq	%rsi, %rsi
	jg	.L49
	testq	%r15, %r15
	jne	.L52
.L50:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r10, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r10
	movq	-72(%rbp), %r8
	jg	.L49
.L52:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L60:
	testq	%r14, %r14
	js	.L56
	jne	.L47
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L49:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r10, %rdi
	call	memcpy@PLT
	testq	%r15, %r15
	je	.L50
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L54:
	movl	$1, %r14d
	jmp	.L47
.L59:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE21277:
	.size	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_, .-_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	.section	.text._ZN2v88internal26SourcePositionTableBuilder8AddEntryERKNS0_18PositionTableEntryE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26SourcePositionTableBuilder8AddEntryERKNS0_18PositionTableEntryE
	.type	_ZN2v88internal26SourcePositionTableBuilder8AddEntryERKNS0_18PositionTableEntryE, @function
_ZN2v88internal26SourcePositionTableBuilder8AddEntryERKNS0_18PositionTableEntryE:
.LFB17802:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	8(%rdi), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	8(%rsi), %r15
	subq	40(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%rsi), %eax
	subl	32(%rdi), %eax
	cmpb	$0, 16(%rsi)
	jne	.L63
	notl	%eax
.L63:
	leal	(%rax,%rax), %r13d
	sarl	$31, %eax
	movq	16(%rbx), %rsi
	leaq	-57(%rbp), %r12
	xorl	%eax, %r13d
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L74:
	movb	%al, (%rsi)
	movq	16(%rbx), %rax
	leaq	1(%rax), %rsi
	movl	%r13d, %eax
	movq	%rsi, 16(%rbx)
	shrl	$7, %eax
	cmpl	$127, %r13d
	jbe	.L66
.L65:
	movl	%eax, %r13d
.L67:
	cmpl	$127, %r13d
	movl	%r13d, %edx
	seta	%al
	andl	$127, %edx
	sall	$7, %eax
	orl	%edx, %eax
	movb	%al, -57(%rbp)
	cmpq	%rsi, 24(%rbx)
	jne	.L74
	movq	%rcx, %rdi
	movq	%r12, %rdx
	movq	%rcx, -72(%rbp)
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movl	%r13d, %eax
	movq	16(%rbx), %rsi
	movq	-72(%rbp), %rcx
	shrl	$7, %eax
	cmpl	$127, %r13d
	ja	.L65
	.p2align 4,,10
	.p2align 3
.L66:
	leaq	(%r15,%r15), %r12
	sarq	$63, %r15
	leaq	-57(%rbp), %r13
	xorq	%r15, %r12
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L75:
	movb	%al, (%rsi)
	movq	%r12, %rax
	addq	$1, 16(%rbx)
	shrq	$7, %rax
	cmpq	$127, %r12
	jbe	.L70
.L69:
	movq	16(%rbx), %rsi
	movq	%rax, %r12
.L71:
	cmpq	$127, %r12
	movl	%r12d, %edx
	seta	%al
	andl	$127, %edx
	sall	$7, %eax
	orl	%edx, %eax
	movb	%al, -57(%rbp)
	cmpq	%rsi, 24(%rbx)
	jne	.L75
	movq	%rcx, %rdi
	movq	%r13, %rdx
	movq	%rcx, -72(%rbp)
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movq	%r12, %rax
	movq	-72(%rbp), %rcx
	shrq	$7, %rax
	cmpq	$127, %r12
	ja	.L69
	.p2align 4,,10
	.p2align 3
.L70:
	movdqu	(%r14), %xmm0
	movups	%xmm0, 32(%rbx)
	movzbl	16(%r14), %eax
	movb	%al, 48(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L76
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L76:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17802:
	.size	_ZN2v88internal26SourcePositionTableBuilder8AddEntryERKNS0_18PositionTableEntryE, .-_ZN2v88internal26SourcePositionTableBuilder8AddEntryERKNS0_18PositionTableEntryE
	.section	.text._ZN2v88internal26SourcePositionTableBuilder11AddPositionEmNS0_14SourcePositionEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26SourcePositionTableBuilder11AddPositionEmNS0_14SourcePositionEb
	.type	_ZN2v88internal26SourcePositionTableBuilder11AddPositionEmNS0_14SourcePositionEb, @function
_ZN2v88internal26SourcePositionTableBuilder11AddPositionEmNS0_14SourcePositionEb:
.LFB17801:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	cmpl	$2, (%rdi)
	je	.L81
.L77:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L82
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	.cfi_restore_state
	movl	%esi, -32(%rbp)
	leaq	-32(%rbp), %rsi
	movq	%rdx, -24(%rbp)
	movb	%cl, -16(%rbp)
	call	_ZN2v88internal26SourcePositionTableBuilder8AddEntryERKNS0_18PositionTableEntryE
	jmp	.L77
.L82:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17801:
	.size	_ZN2v88internal26SourcePositionTableBuilder11AddPositionEmNS0_14SourcePositionEb, .-_ZN2v88internal26SourcePositionTableBuilder11AddPositionEmNS0_14SourcePositionEb
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal26SourcePositionTableBuilderC2ENS1_13RecordingModeE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal26SourcePositionTableBuilderC2ENS1_13RecordingModeE, @function
_GLOBAL__sub_I__ZN2v88internal26SourcePositionTableBuilderC2ENS1_13RecordingModeE:
.LFB21665:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE21665:
	.size	_GLOBAL__sub_I__ZN2v88internal26SourcePositionTableBuilderC2ENS1_13RecordingModeE, .-_GLOBAL__sub_I__ZN2v88internal26SourcePositionTableBuilderC2ENS1_13RecordingModeE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal26SourcePositionTableBuilderC2ENS1_13RecordingModeE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
