	.file	"runtime-scopes.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB5066:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE5066:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB5067:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5067:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB5069:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5069:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.text._ZN2v88internal12_GLOBAL__N_114LoadLookupSlotEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS0_11ShouldThrowEPNS4_INS0_6ObjectEEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_114LoadLookupSlotEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS0_11ShouldThrowEPNS4_INS0_6ObjectEEE, @function
_ZN2v88internal12_GLOBAL__N_114LoadLookupSlotEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS0_11ShouldThrowEPNS4_INS0_6ObjectEEE:
.LFB22269:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$136, %rsp
	movq	12464(%rdi), %rsi
	movq	41112(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L6
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdi
.L7:
	leaq	-153(%rbp), %rax
	pushq	$0
	movl	$3, %edx
	movq	%r15, %rsi
	pushq	%rax
	leaq	-148(%rbp), %r8
	leaq	-152(%rbp), %rcx
	leaq	-154(%rbp), %r9
	call	_ZN2v88internal7Context6LookupENS0_6HandleIS1_EENS2_INS0_6StringEEENS0_18ContextLookupFlagsEPiPNS0_18PropertyAttributesEPNS0_18InitializationFlagEPNS0_12VariableModeEPb@PLT
	movq	%rax, %r8
	popq	%rax
	movq	12480(%r12), %rax
	popq	%rdx
	cmpq	%rax, 96(%r12)
	je	.L9
.L58:
	xorl	%eax, %eax
.L10:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L59
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	movq	%r8, -168(%rbp)
	testq	%r8, %r8
	je	.L11
	movq	(%r8), %rax
	testb	$1, %al
	jne	.L60
.L12:
	movl	-152(%rbp), %edx
	cmpl	$-1, %edx
	je	.L61
.L35:
	leal	16(,%rdx,8), %edx
	movslq	%edx, %rdx
	movq	-1(%rdx,%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L14
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L15:
	cmpb	$0, -154(%rbp)
	jne	.L17
	movq	(%rax), %rdx
	cmpq	%rdx, 96(%r12)
	je	.L18
.L17:
	testq	%rbx, %rbx
	je	.L10
	addq	$88, %r12
	movq	%r12, (%rbx)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L6:
	movq	41088(%r12), %rdi
	cmpq	%rdi, 41096(%r12)
	je	.L62
.L8:
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdi)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L61:
	testb	$1, %al
	jne	.L21
.L23:
	movq	%r8, %rsi
	movl	$-1, %edx
	movq	%r12, %rdi
	movq	%r8, -176(%rbp)
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	-176(%rbp), %r8
	movq	%rax, %r13
.L22:
	movq	(%r15), %rdx
	movl	$3, %eax
	movq	-1(%rdx), %rcx
	cmpw	$64, 11(%rcx)
	jne	.L24
	movl	11(%rdx), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$3, %eax
.L24:
	movl	%eax, -144(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -132(%rbp)
	movq	(%r15), %rax
	movq	%r12, -120(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L63
.L25:
	movq	%r13, -80(%rbp)
	leaq	-144(%rbp), %r13
	movq	%r13, %rdi
	movq	%r8, -96(%rbp)
	movq	%r8, -176(%rbp)
	movq	%r14, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	$0, -88(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -140(%rbp)
	movq	-176(%rbp), %r8
	jne	.L26
	movq	-120(%rbp), %rax
	addq	$88, %rax
.L27:
	testq	%rbx, %rbx
	je	.L10
	movq	(%r8), %rdx
	testb	$1, %dl
	jne	.L64
.L30:
	movq	-168(%rbp), %rcx
	movq	%rcx, (%rbx)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L60:
	movq	-1(%rax), %rdx
	cmpw	$119, 11(%rdx)
	jne	.L12
	testq	%rbx, %rbx
	je	.L13
	leaq	88(%r12), %rax
	movq	%rax, (%rbx)
.L13:
	movl	-152(%rbp), %edx
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal16SourceTextModule12LoadVariableEPNS0_7IsolateENS0_6HandleIS1_EEi@PLT
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L11:
	movl	-152(%rbp), %edx
	cmpl	$-1, %edx
	jne	.L65
	testl	%r13d, %r13d
	je	.L18
	leaq	88(%r12), %rax
	testq	%rbx, %rbx
	je	.L10
	movq	%rax, (%rbx)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L62:
	movq	%r12, %rdi
	movq	%rsi, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %rsi
	movq	%rax, %rdi
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L14:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L66
.L16:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L26:
	movl	$1, %esi
	movq	%r13, %rdi
	movq	%r8, -176(%rbp)
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	-176(%rbp), %r8
	testq	%rax, %rax
	jne	.L27
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L18:
	movq	%r15, %rdx
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$177, %esi
	call	_ZN2v88internal7Factory17NewReferenceErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L21:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L23
	movq	%r8, %r13
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L65:
	movq	0, %rax
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L63:
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%r8, -176(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-176(%rbp), %r8
	movq	%rax, %r14
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L64:
	movq	-1(%rdx), %rcx
	cmpw	$1025, 11(%rcx)
	je	.L32
	movq	-1(%rdx), %rdx
	cmpw	$1065, 11(%rdx)
	jne	.L30
.L32:
	leaq	88(%r12), %rcx
	movq	%rcx, -168(%rbp)
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L66:
	movq	%r12, %rdi
	movq	%rsi, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %rsi
	jmp	.L16
.L59:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22269:
	.size	_ZN2v88internal12_GLOBAL__N_114LoadLookupSlotEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS0_11ShouldThrowEPNS4_INS0_6ObjectEEE, .-_ZN2v88internal12_GLOBAL__N_114LoadLookupSlotEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS0_11ShouldThrowEPNS4_INS0_6ObjectEEE
	.section	.text._ZN2v88internal12_GLOBAL__N_123ThrowRedeclarationErrorEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS1_17RedeclarationTypeE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_123ThrowRedeclarationErrorEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS1_17RedeclarationTypeE, @function
_ZN2v88internal12_GLOBAL__N_123ThrowRedeclarationErrorEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS1_17RedeclarationTypeE:
.LFB22185:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	41088(%rdi), %rbx
	addl	$1, 41104(%rdi)
	testl	%edx, %edx
	movq	41096(%rdi), %r14
	movq	%rsi, %rdx
	movl	$175, %esi
	jne	.L68
	call	_ZN2v88internal7Factory14NewSyntaxErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
.L74:
	movq	(%rax), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rbx, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	41096(%r12), %r14
	je	.L72
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L72:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_restore_state
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	jmp	.L74
	.cfi_endproc
.LFE22185:
	.size	_ZN2v88internal12_GLOBAL__N_123ThrowRedeclarationErrorEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS1_17RedeclarationTypeE, .-_ZN2v88internal12_GLOBAL__N_123ThrowRedeclarationErrorEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS1_17RedeclarationTypeE
	.section	.text._ZN2v88internalL13FindNameClashEPNS0_7IsolateENS0_6HandleINS0_9ScopeInfoEEENS3_INS0_14JSGlobalObjectEEENS3_INS0_18ScriptContextTableEEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL13FindNameClashEPNS0_7IsolateENS0_6HandleINS0_9ScopeInfoEEENS3_INS0_14JSGlobalObjectEEENS3_INS0_18ScriptContextTableEEE, @function
_ZN2v88internalL13FindNameClashEPNS0_7IsolateENS0_6HandleINS0_9ScopeInfoEEENS3_INS0_14JSGlobalObjectEEENS3_INS0_18ScriptContextTableEEE:
.LFB22247:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%rcx, -176(%rbp)
	movq	%rdx, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movl	11(%rax), %ecx
	testl	%ecx, %ecx
	jle	.L77
	movq	%rsi, %r14
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L89:
	movq	31(%rax), %rdx
	sarq	$32, %rdx
	cmpl	%edx, %ebx
	jge	.L77
	leaq	-144(%rbp), %r13
	movl	%ebx, %esi
	movq	%rax, -144(%rbp)
	movq	%r13, %rdi
	call	_ZNK2v88internal9ScopeInfo16ContextLocalNameEi@PLT
	movq	41112(%r12), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L78
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L79:
	movq	(%r14), %rax
	movl	%ebx, %esi
	movq	%r13, %rdi
	movq	%rax, -144(%rbp)
	call	_ZNK2v88internal9ScopeInfo16ContextLocalModeEi@PLT
	movq	-176(%rbp), %rcx
	movq	(%r15), %rdx
	movq	%r12, %rdi
	movb	%al, -168(%rbp)
	movq	(%rcx), %rsi
	leaq	-156(%rbp), %rcx
	call	_ZN2v88internal18ScriptContextTable6LookupEPNS0_7IsolateES1_NS0_6StringEPNS1_12LookupResultE@PLT
	testb	%al, %al
	je	.L81
	cmpb	$1, -168(%rbp)
	jbe	.L95
	cmpb	$1, -148(%rbp)
	jbe	.L95
.L83:
	movq	(%r14), %rax
	addl	$1, %ebx
	movl	11(%rax), %edx
	testl	%edx, %edx
	jg	.L89
.L77:
	movq	88(%r12), %rax
.L90:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L96
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	.cfi_restore_state
	cmpb	$1, -168(%rbp)
	ja	.L83
	movq	-184(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -168(%rbp)
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	movq	(%r15), %rax
	subq	$37592, %rdi
	movq	-1(%rax), %rax
	movabsq	$824633720832, %rax
	movl	$0, -144(%rbp)
	movq	%rax, -132(%rbp)
	movq	%rdi, -120(%rbp)
	movq	(%r15), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %edx
	movq	%r15, %rax
	andl	$-32, %edx
	cmpl	$32, %edx
	je	.L97
.L85:
	movq	%rax, -112(%rbp)
	movq	-184(%rbp), %rax
	movq	%r13, %rdi
	movq	$0, -104(%rbp)
	movq	%rax, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rax, -80(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal10JSReceiver21GetPropertyAttributesEPNS0_14LookupIteratorE@PLT
	testb	%al, %al
	je	.L98
	btq	$34, %rax
	jc	.L95
	movq	-184(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal14JSGlobalObject22InvalidatePropertyCellENS0_6HandleIS1_EENS2_INS0_4NameEEE@PLT
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L78:
	movq	41088(%r12), %r15
	cmpq	41096(%r12), %r15
	je	.L99
.L80:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r15)
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L99:
	movq	%r12, %rdi
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L97:
	movq	%r15, %rsi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L95:
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_123ThrowRedeclarationErrorEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS1_17RedeclarationTypeE
	jmp	.L90
.L98:
	movq	312(%r12), %rax
	jmp	.L90
.L96:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22247:
	.size	_ZN2v88internalL13FindNameClashEPNS0_7IsolateENS0_6HandleINS0_9ScopeInfoEEENS3_INS0_14JSGlobalObjectEEENS3_INS0_18ScriptContextTableEEE, .-_ZN2v88internalL13FindNameClashEPNS0_7IsolateENS0_6HandleINS0_9ScopeInfoEEENS3_INS0_14JSGlobalObjectEEENS3_INS0_18ScriptContextTableEEE
	.section	.text._ZN2v88internal12_GLOBAL__N_113DeclareGlobalEPNS0_7IsolateENS0_6HandleINS0_14JSGlobalObjectEEENS4_INS0_6StringEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesEbbNS1_17RedeclarationTypeENS4_INS0_14FeedbackVectorEEENS0_12FeedbackSlotE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_113DeclareGlobalEPNS0_7IsolateENS0_6HandleINS0_14JSGlobalObjectEEENS4_INS0_6StringEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesEbbNS1_17RedeclarationTypeENS4_INS0_14FeedbackVectorEEENS0_12FeedbackSlotE, @function
_ZN2v88internal12_GLOBAL__N_113DeclareGlobalEPNS0_7IsolateENS0_6HandleINS0_14JSGlobalObjectEEENS4_INS0_6StringEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesEbbNS1_17RedeclarationTypeENS4_INS0_14FeedbackVectorEEENS0_12FeedbackSlotE:
.LFB22186:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$200, %rsp
	movq	%rcx, -224(%rbp)
	movl	16(%rbp), %r15d
	movl	%r8d, -212(%rbp)
	movl	%r9d, -216(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	23(%rax), %rax
	movq	1135(%rax), %rsi
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L101
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L102:
	movq	0(%r13), %rdx
	leaq	-188(%rbp), %rcx
	movq	%r12, %rdi
	movq	%r13, %r14
	call	_ZN2v88internal18ScriptContextTable6LookupEPNS0_7IsolateES1_NS0_6StringEPNS1_12LookupResultE@PLT
	testb	%al, %al
	je	.L104
	cmpb	$1, -180(%rbp)
	jbe	.L147
.L104:
	movq	(%rbx), %rax
	movzbl	%r15b, %edx
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	movq	0(%r13), %rax
	movq	-1(%rax), %rcx
	subq	$37592, %rdi
	cmpw	$64, 11(%rcx)
	jne	.L106
	testb	$1, 11(%rax)
	movl	$0, %eax
	cmovne	%eax, %edx
.L106:
	movabsq	$824633720832, %rax
	movl	%edx, -144(%rbp)
	movq	%rax, -132(%rbp)
	movq	0(%r13), %rax
	movq	%rdi, -120(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L148
.L107:
	movq	%r14, -112(%rbp)
	leaq	-144(%rbp), %r14
	movq	%r14, %rdi
	movq	$0, -104(%rbp)
	movq	%rbx, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rbx, -80(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal10JSReceiver21GetPropertyAttributesEPNS0_14LookupIteratorE@PLT
	testb	%al, %al
	jne	.L108
.L146:
	movq	312(%r12), %rax
.L105:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L149
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L108:
	.cfi_restore_state
	movl	-140(%rbp), %edx
	cmpl	$4, %edx
	je	.L109
	cmpb	$0, -216(%rbp)
	jne	.L116
	shrq	$32, %rax
	testb	$4, %al
	je	.L111
	testb	$3, %al
	jne	.L126
	movl	%eax, -212(%rbp)
	cmpl	$5, %edx
	je	.L126
	.p2align 4,,10
	.p2align 3
.L109:
	testb	%r15b, %r15b
	jne	.L150
.L113:
	movl	-212(%rbp), %edx
	movq	-224(%rbp), %rsi
	movl	$1, %ecx
	movq	%r14, %rdi
	call	_ZN2v88internal8JSObject33DefineOwnPropertyIgnoreAttributesEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS0_18PropertyAttributesENS1_20AccessorInfoHandlingE@PLT
	testq	%rax, %rax
	je	.L146
	cmpq	$0, 32(%rbp)
	je	.L116
	cmpl	$2, -140(%rbp)
	je	.L116
	movq	(%rbx), %rax
	movq	-1(%rax), %rdx
	testb	$4, 13(%rdx)
	je	.L117
	movq	-1(%rax), %rax
.L145:
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L151
.L121:
	movq	71(%rax), %rdx
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-37504(%rax), %rax
	cmpq	%rax, %rdx
	je	.L122
	movq	31(%rdx), %rax
.L122:
	testb	$4, 75(%rax)
	je	.L116
.L117:
	movq	32(%rbp), %rax
	movl	40(%rbp), %esi
	leaq	-200(%rbp), %rdi
	movq	$0, -168(%rbp)
	movq	%rax, -176(%rbp)
	movl	40(%rbp), %eax
	movl	%eax, -160(%rbp)
	movq	32(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -200(%rbp)
	call	_ZNK2v88internal14FeedbackVector7GetKindENS0_12FeedbackSlotE@PLT
	movq	%r14, %rdi
	movl	%eax, -156(%rbp)
	call	_ZNK2v88internal14LookupIterator15GetPropertyCellEv@PLT
	leaq	-176(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal13FeedbackNexus25ConfigurePropertyCellModeENS0_6HandleINS0_12PropertyCellEEE@PLT
	.p2align 4,,10
	.p2align 3
.L116:
	movq	88(%r12), %rax
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L101:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L152
.L103:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L147:
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_123ThrowRedeclarationErrorEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS1_17RedeclarationTypeE
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L150:
	xorl	%esi, %esi
	cmpl	$-1, -72(%rbp)
	movq	%r14, %rdi
	je	.L114
	call	_ZN2v88internal14LookupIterator15RestartInternalILb1EEEvNS1_16InterceptorStateE@PLT
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L148:
	movq	%r13, %rsi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %r14
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L152:
	movq	%r12, %rdi
	movq	%rsi, -232(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-232(%rbp), %rsi
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L111:
	cmpl	$5, %edx
	jne	.L109
	movq	%r14, %rdi
	call	_ZN2v88internal14LookupIterator6DeleteEv@PLT
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L126:
	movl	24(%rbp), %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_123ThrowRedeclarationErrorEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS1_17RedeclarationTypeE
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L114:
	call	_ZN2v88internal14LookupIterator15RestartInternalILb0EEEvNS1_16InterceptorStateE@PLT
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L151:
	movq	-1(%rax), %rdx
	leaq	-1(%rax), %rcx
	cmpw	$68, 11(%rdx)
	je	.L145
	movq	(%rcx), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L121
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	jmp	.L121
.L149:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22186:
	.size	_ZN2v88internal12_GLOBAL__N_113DeclareGlobalEPNS0_7IsolateENS0_6HandleINS0_14JSGlobalObjectEEENS4_INS0_6StringEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesEbbNS1_17RedeclarationTypeENS4_INS0_14FeedbackVectorEEENS0_12FeedbackSlotE, .-_ZN2v88internal12_GLOBAL__N_113DeclareGlobalEPNS0_7IsolateENS0_6HandleINS0_14JSGlobalObjectEEENS4_INS0_6StringEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesEbbNS1_17RedeclarationTypeENS4_INS0_14FeedbackVectorEEENS0_12FeedbackSlotE
	.section	.text._ZN2v88internal12_GLOBAL__N_115StoreLookupSlotEPNS0_7IsolateENS0_6HandleINS0_7ContextEEENS4_INS0_6StringEEENS4_INS0_6ObjectEEENS0_12LanguageModeENS0_18ContextLookupFlagsE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_115StoreLookupSlotEPNS0_7IsolateENS0_6HandleINS0_7ContextEEENS4_INS0_6StringEEENS4_INS0_6ObjectEEENS0_12LanguageModeENS0_18ContextLookupFlagsE, @function
_ZN2v88internal12_GLOBAL__N_115StoreLookupSlotEPNS0_7IsolateENS0_6HandleINS0_7ContextEEENS4_INS0_6StringEEENS4_INS0_6ObjectEEENS0_12LanguageModeENS0_18ContextLookupFlagsE:
.LFB22279:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	movl	%r9d, %edx
	leaq	-75(%rbp), %r9
	pushq	%r13
	movq	%r14, %rsi
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r15, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	leaq	-72(%rbp), %rcx
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	leaq	-68(%rbp), %r8
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-73(%rbp), %rax
	pushq	%rax
	leaq	-74(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal7Context6LookupENS0_6HandleIS1_EENS2_INS0_6StringEEENS0_18ContextLookupFlagsEPiPNS0_18PropertyAttributesEPNS0_18InitializationFlagEPNS0_12VariableModeEPb@PLT
	popq	%rdx
	popq	%rcx
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L189
	movq	(%rax), %rdx
	testb	$1, %dl
	jne	.L190
.L155:
	movl	-72(%rbp), %edx
	cmpl	$-1, %edx
	je	.L160
.L194:
	cmpb	$0, -75(%rbp)
	je	.L161
.L164:
	testb	$1, -68(%rbp)
	je	.L191
	cmpb	$1, -73(%rbp)
	jne	.L177
	testb	%bl, %bl
	jne	.L177
.L168:
	movq	%r12, %rax
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L160:
	cmpl	$64, -68(%rbp)
	jne	.L170
	testb	%bl, %bl
	jne	.L187
	movq	(%r15), %rax
	leaq	-64(%rbp), %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal7Context13global_objectEv@PLT
	movq	41112(%r13), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L172
	movq	%rax, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
	.p2align 4,,10
	.p2align 3
.L170:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal6Object11SetPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEES5_NS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE@PLT
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L190:
	movq	-1(%rdx), %rdx
	cmpw	$119, 11(%rdx)
	jne	.L155
	testb	$1, -68(%rbp)
	je	.L192
	.p2align 4,,10
	.p2align 3
.L177:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movl	$37, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
.L186:
	movq	(%rax), %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	xorl	%eax, %eax
.L156:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L193
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L189:
	.cfi_restore_state
	movq	12480(%r13), %rdi
	cmpq	%rdi, 96(%r13)
	jne	.L156
	movl	-72(%rbp), %edx
	cmpl	$-1, %edx
	jne	.L194
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L161:
	movq	(%rax), %rsi
	leal	16(,%rdx,8), %ecx
	movslq	%ecx, %rcx
	movq	-1(%rcx,%rsi), %rcx
	cmpq	%rcx, 96(%r13)
	jne	.L164
.L187:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movl	$177, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal7Factory17NewReferenceErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L191:
	movq	(%rax), %r14
	leal	16(,%rdx,8), %eax
	movq	(%r12), %r13
	cltq
	leaq	-1(%r14,%rax), %r15
	movq	%r13, (%r15)
	testb	$1, %r13b
	je	.L168
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L195
.L166:
	testb	$24, %al
	je	.L168
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L168
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L195:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L192:
	movl	-72(%rbp), %esi
	movq	%rax, %rdi
	movq	%r12, %rdx
	call	_ZN2v88internal16SourceTextModule13StoreVariableENS0_6HandleIS1_EEiNS2_INS0_6ObjectEEE@PLT
	movq	%r12, %rax
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L172:
	movq	41088(%r13), %rsi
	cmpq	41096(%r13), %rsi
	je	.L196
.L174:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r13)
	movq	%r15, (%rsi)
	jmp	.L170
.L196:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L174
.L193:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22279:
	.size	_ZN2v88internal12_GLOBAL__N_115StoreLookupSlotEPNS0_7IsolateENS0_6HandleINS0_7ContextEEENS4_INS0_6StringEEENS4_INS0_6ObjectEEENS0_12LanguageModeENS0_18ContextLookupFlagsE, .-_ZN2v88internal12_GLOBAL__N_115StoreLookupSlotEPNS0_7IsolateENS0_6HandleINS0_7ContextEEENS4_INS0_6StringEEENS4_INS0_6ObjectEEENS0_12LanguageModeENS0_18ContextLookupFlagsE
	.section	.text._ZN2v88internal12_GLOBAL__N_114DeclareGlobalsEPNS0_7IsolateENS0_6HandleINS0_10FixedArrayEEEiNS4_INS0_10JSFunctionEEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_114DeclareGlobalsEPNS0_7IsolateENS0_6HandleINS0_10FixedArrayEEEiNS4_INS0_10JSFunctionEEE, @function
_ZN2v88internal12_GLOBAL__N_114DeclareGlobalsEPNS0_7IsolateENS0_6HandleINS0_10FixedArrayEEEiNS4_INS0_10JSFunctionEEE:
.LFB22187:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$152, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	41088(%rdi), %rax
	addl	$1, 41104(%rdi)
	movq	%rax, -176(%rbp)
	movq	41096(%rdi), %rax
	movq	%rax, -168(%rbp)
	movq	12464(%rdi), %rax
	leaq	-64(%rbp), %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal7Context13global_objectEv@PLT
	movq	41112(%r12), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L198
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -88(%rbp)
.L199:
	movq	41112(%r12), %rdi
	movq	12464(%r12), %rsi
	testq	%rdi, %rdi
	je	.L201
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -144(%rbp)
.L202:
	movabsq	$287762808832, %rcx
	movq	0(%r13), %rdx
	movq	23(%rdx), %rax
	movq	7(%rax), %rax
	cmpq	%rcx, %rax
	je	.L209
	testb	$1, %al
	jne	.L207
.L211:
	movq	39(%rdx), %rax
	movq	7(%rax), %rax
	movq	-1(%rax), %rax
	cmpw	$155, 11(%rax)
	je	.L253
.L209:
	movq	0(%r13), %rax
	movq	39(%rax), %rax
	movq	7(%rax), %rsi
	movq	41112(%r12), %rax
	movq	%rax, -80(%rbp)
	testq	%rax, %rax
	je	.L254
	movq	%rax, %rdi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	$0, -80(%rbp)
	movq	%rax, -152(%rbp)
.L217:
	movq	(%r14), %rax
	movslq	11(%rax), %rdx
	movq	41096(%r12), %rax
	movl	%edx, -120(%rbp)
	movq	%rax, -128(%rbp)
	movl	41104(%r12), %eax
	testq	%rdx, %rdx
	jle	.L220
	andl	$1, %ebx
	movl	$0, -116(%rbp)
	cmpb	$1, %bl
	movl	$0, -72(%rbp)
	sbbl	%ebx, %ebx
	andl	$4, %ebx
	movl	%ebx, -104(%rbp)
	leaq	88(%r12), %rbx
	movq	%rbx, -112(%rbp)
.L244:
	leal	1(%rax), %edx
	addl	$1024, -116(%rbp)
	movl	-116(%rbp), %ebx
	movl	%edx, 41104(%r12)
	movl	-120(%rbp), %edx
	movq	41088(%r12), %rcx
	cmpl	%edx, %ebx
	cmovle	%ebx, %edx
	movl	-72(%rbp), %ebx
	movq	%rcx, -136(%rbp)
	movl	%edx, -100(%rbp)
	cmpl	%ebx, %edx
	jle	.L221
	leal	16(,%rbx,8), %ebx
	movslq	%ebx, %rbx
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L258:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L223:
	movq	(%r14), %rax
	movq	7(%rbx,%rax), %r15
	addl	$4, -72(%rbp)
	movq	15(%rbx,%rax), %rsi
	movq	41112(%r12), %rdi
	sarq	$32, %r15
	testq	%rdi, %rdi
	je	.L225
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L226:
	movq	(%r14), %rax
	movq	23(%rbx,%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L228
	movq	%rdx, -96(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-96(%rbp), %rdx
	movq	(%rax), %rsi
	cmpq	%rsi, 88(%r12)
	movq	%rax, %r10
	sete	%r9b
	testb	$1, %sil
	jne	.L255
.L232:
	movq	-112(%rbp), %rcx
	xorl	%eax, %eax
.L237:
	pushq	%r15
	movl	-104(%rbp), %r8d
	movq	%r13, %rdx
	andl	$1, %r9d
	pushq	-80(%rbp)
	movq	-88(%rbp), %rsi
	movq	%r12, %rdi
	pushq	$0
	pushq	%rax
	call	_ZN2v88internal12_GLOBAL__N_113DeclareGlobalEPNS0_7IsolateENS0_6HandleINS0_14JSGlobalObjectEEENS4_INS0_6StringEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesEbbNS1_17RedeclarationTypeENS4_INS0_14FeedbackVectorEEENS0_12FeedbackSlotE
	movq	12480(%r12), %rdx
	addq	$32, %rsp
	cmpq	%rdx, 96(%r12)
	jne	.L256
	movl	-100(%rbp), %ecx
	addq	$32, %rbx
	cmpl	%ecx, -72(%rbp)
	jge	.L257
.L241:
	movq	(%r14), %rax
	movq	-1(%rbx,%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	jne	.L258
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L259
.L224:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L254:
	movq	41088(%r12), %rax
	movq	%rax, -152(%rbp)
	cmpq	41096(%r12), %rax
	je	.L260
.L219:
	movq	-152(%rbp), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdx)
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L207:
	movq	-1(%rax), %rcx
	cmpw	$165, 11(%rcx)
	je	.L209
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L211
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L253:
	movq	0(%r13), %rax
	movq	41112(%r12), %rdi
	movq	39(%rax), %rax
	movq	7(%rax), %rsi
	testq	%rdi, %rdi
	je	.L250
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -80(%rbp)
	movq	(%rax), %rsi
.L214:
	movq	41112(%r12), %rdi
	movq	23(%rsi), %rsi
	testq	%rdi, %rdi
	je	.L216
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -152(%rbp)
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L228:
	movq	41088(%r12), %r10
	cmpq	41096(%r12), %r10
	je	.L261
.L230:
	leaq	8(%r10), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r10)
	cmpq	%rsi, 88(%r12)
	sete	%r9b
	testb	$1, %sil
	je	.L232
.L255:
	movq	-1(%rsi), %rax
	cmpw	$160, 11(%rax)
	jne	.L232
	movq	-152(%rbp), %rax
	movq	(%rax), %rcx
	movq	%rcx, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rsi
	movq	(%rdx), %rax
	sarq	$32, %rax
	subq	$37592, %rsi
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%rcx,%rax), %r8
	movq	41112(%rsi), %rdi
	testq	%rdi, %rdi
	je	.L234
	movq	%r8, %rsi
	movb	%r9b, -160(%rbp)
	movq	%r10, -96(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-96(%rbp), %r10
	movzbl	-160(%rbp), %r9d
	movq	%rax, %rcx
.L235:
	movl	$1, %r8d
	movq	%r10, %rsi
	movq	%r12, %rdi
	movb	%r9b, -96(%rbp)
	movq	-144(%rbp), %rdx
	call	_ZN2v88internal7Factory33NewFunctionFromSharedFunctionInfoENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS2_INS0_12FeedbackCellEEENS0_14AllocationTypeE@PLT
	movzbl	-96(%rbp), %r9d
	movq	%rax, %rcx
	movl	$1, %eax
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L225:
	movq	41088(%r12), %rdx
	cmpq	41096(%r12), %rdx
	je	.L262
.L227:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdx)
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L261:
	movq	%r12, %rdi
	movq	%rsi, -160(%rbp)
	movq	%rdx, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-160(%rbp), %rsi
	movq	-96(%rbp), %rdx
	movq	%rax, %r10
	jmp	.L230
	.p2align 4,,10
	.p2align 3
.L259:
	movq	%r12, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L262:
	movq	%r12, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L256:
	movq	%rax, %r13
	movq	-136(%rbp), %rax
	movq	-128(%rbp), %rbx
	movq	%rax, 41088(%r12)
	movl	41104(%r12), %eax
	subl	$1, %eax
	movl	%eax, 41104(%r12)
	cmpq	%rbx, 41096(%r12)
	je	.L240
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movq	41096(%r12), %rbx
	movl	41104(%r12), %eax
	movq	%rbx, -128(%rbp)
.L240:
	movq	-176(%rbp), %rdx
	subl	$1, %eax
	movq	-128(%rbp), %rbx
	movl	%eax, 41104(%r12)
	movq	%rdx, 41088(%r12)
	cmpq	%rbx, -168(%rbp)
	je	.L246
	movq	-168(%rbp), %rax
	movq	%r12, %rdi
	movq	%rax, 41096(%r12)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L246:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L263
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L234:
	.cfi_restore_state
	movq	41088(%rsi), %rcx
	cmpq	41096(%rsi), %rcx
	je	.L264
.L236:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rsi)
	movq	%r8, (%rcx)
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L257:
	movl	41104(%r12), %eax
	movq	-136(%rbp), %rbx
	subl	$1, %eax
	movq	%rbx, 41088(%r12)
	movq	-128(%rbp), %rbx
	movl	%eax, 41104(%r12)
	cmpq	%rbx, 41096(%r12)
	je	.L242
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movq	41096(%r12), %rbx
	movl	41104(%r12), %eax
	movl	-72(%rbp), %ecx
	movq	%rbx, -128(%rbp)
	cmpl	%ecx, -120(%rbp)
	jg	.L244
.L220:
	movq	88(%r12), %r13
	jmp	.L240
.L221:
	movl	%eax, 41104(%r12)
.L242:
	movl	-72(%rbp), %edx
	cmpl	%edx, -120(%rbp)
	jg	.L244
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L198:
	movq	41088(%r12), %rax
	movq	%rax, -88(%rbp)
	cmpq	41096(%r12), %rax
	je	.L265
.L200:
	movq	-88(%rbp), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdx)
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L201:
	movq	41088(%r12), %rax
	movq	%rax, -144(%rbp)
	cmpq	%rax, 41096(%r12)
	je	.L266
.L203:
	movq	-144(%rbp), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rcx)
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L216:
	movq	41088(%r12), %rax
	movq	%rax, -152(%rbp)
	cmpq	41096(%r12), %rax
	je	.L267
.L218:
	movq	-152(%rbp), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rcx)
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L250:
	movq	41088(%r12), %rax
	movq	%rax, -80(%rbp)
	cmpq	41096(%r12), %rax
	je	.L268
.L215:
	movq	-80(%rbp), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdx)
	jmp	.L214
.L264:
	movq	%rsi, %rdi
	movq	%r8, -192(%rbp)
	movb	%r9b, -177(%rbp)
	movq	%r10, -160(%rbp)
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-192(%rbp), %r8
	movq	-160(%rbp), %r10
	movzbl	-177(%rbp), %r9d
	movq	-96(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L236
.L266:
	movq	%r12, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, -144(%rbp)
	jmp	.L203
.L265:
	movq	%r12, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, -88(%rbp)
	jmp	.L200
.L260:
	movq	%r12, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, -152(%rbp)
	jmp	.L219
.L267:
	movq	%r12, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, -152(%rbp)
	jmp	.L218
.L268:
	movq	%r12, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, -80(%rbp)
	jmp	.L215
.L263:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22187:
	.size	_ZN2v88internal12_GLOBAL__N_114DeclareGlobalsEPNS0_7IsolateENS0_6HandleINS0_10FixedArrayEEEiNS4_INS0_10JSFunctionEEE, .-_ZN2v88internal12_GLOBAL__N_114DeclareGlobalsEPNS0_7IsolateENS0_6HandleINS0_10FixedArrayEEEiNS4_INS0_10JSFunctionEEE
	.section	.text._ZN2v88internal12_GLOBAL__N_117DeclareEvalHelperEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS4_INS0_6ObjectEEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_117DeclareEvalHelperEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS4_INS0_6ObjectEEE, @function
_ZN2v88internal12_GLOBAL__N_117DeclareEvalHelperEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS4_INS0_6ObjectEEE:
.LFB22192:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-64(%rbp), %r15
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	12464(%rdi), %rax
	movq	%r15, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal7Context19declaration_contextEv@PLT
	movq	41112(%r14), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L270
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r11
.L271:
	movq	(%r12), %rax
	movb	$0, -96(%rbp)
	movq	%rax, %rbx
	notq	%rbx
	andl	$1, %ebx
	je	.L324
.L273:
	leaq	-73(%rbp), %rax
	pushq	$0
	xorl	%edx, %edx
	leaq	-72(%rbp), %rcx
	pushq	%rax
	leaq	-68(%rbp), %r8
	movq	%r11, %rdi
	leaq	-74(%rbp), %r9
	movq	%r13, %rsi
	movq	%r11, -88(%rbp)
	call	_ZN2v88internal7Context6LookupENS0_6HandleIS1_EENS2_INS0_6StringEEENS0_18ContextLookupFlagsEPiPNS0_18PropertyAttributesEPNS0_18InitializationFlagEPNS0_12VariableModeEPb@PLT
	cmpl	$64, -68(%rbp)
	popq	%rdx
	movq	-88(%rbp), %r11
	movq	%rax, %r8
	popq	%rcx
	je	.L274
	movq	(%rax), %rdx
	testb	$1, %dl
	jne	.L325
.L274:
	movq	(%r11), %rdi
	movq	31(%rdi), %rax
	movq	-1(%rax), %rax
	cmpw	$1025, 11(%rax)
	je	.L326
	movq	-1(%rdi), %rax
	cmpw	$146, 11(%rax)
	je	.L327
	cmpl	$64, -68(%rbp)
	je	.L284
	testb	%bl, %bl
	je	.L285
.L300:
	movq	88(%r14), %rax
.L275:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L328
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L270:
	.cfi_restore_state
	movq	41088(%r14), %r11
	cmpq	41096(%r14), %r11
	je	.L329
.L272:
	leaq	8(%r11), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%r11)
	jmp	.L271
	.p2align 4,,10
	.p2align 3
.L324:
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	cmpw	$1105, %ax
	sete	-96(%rbp)
	setne	%bl
	jmp	.L273
	.p2align 4,,10
	.p2align 3
.L326:
	movq	31(%rdi), %r15
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L281
.L321:
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L282:
	movzbl	-96(%rbp), %eax
	pushq	$-1
	movl	%ebx, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	andl	$1, %r9d
	movq	%r12, %rcx
	movq	%r13, %rdx
	pushq	$1
	movq	%r14, %rdi
	pushq	%rax
	call	_ZN2v88internal12_GLOBAL__N_113DeclareGlobalEPNS0_7IsolateENS0_6HandleINS0_14JSGlobalObjectEEENS4_INS0_6StringEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesEbbNS1_17RedeclarationTypeENS4_INS0_14FeedbackVectorEEENS0_12FeedbackSlotE
	addq	$32, %rsp
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L327:
	movq	%rdi, -64(%rbp)
	movq	%r15, %rdi
	call	_ZN2v88internal7Context13global_objectEv@PLT
	movq	41112(%r14), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	jne	.L321
.L281:
	movq	41088(%r14), %rsi
	cmpq	41096(%r14), %rsi
	je	.L330
.L283:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r14)
	movq	%r15, (%rsi)
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L325:
	movq	-1(%rdx), %rdx
	cmpw	$1025, 11(%rdx)
	jne	.L274
	movzbl	-96(%rbp), %edx
	pushq	$-1
	movl	%ebx, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	andl	$1, %r9d
	movq	%r12, %rcx
	movq	%rax, %rsi
	pushq	$1
	movq	%r14, %rdi
	pushq	%rdx
	movq	%r13, %rdx
	call	_ZN2v88internal12_GLOBAL__N_113DeclareGlobalEPNS0_7IsolateENS0_6HandleINS0_14JSGlobalObjectEEENS4_INS0_6StringEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesEbbNS1_17RedeclarationTypeENS4_INS0_14FeedbackVectorEEENS0_12FeedbackSlotE
	addq	$32, %rsp
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L285:
	movl	-72(%rbp), %eax
	cmpl	$-1, %eax
	je	.L286
	leal	16(,%rax,8), %eax
	movq	(%r12), %r12
	cltq
	leaq	-1(%rdi,%rax), %r13
	movq	%r12, 0(%r13)
	testb	$1, %r12b
	je	.L300
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L331
.L288:
	testb	$24, %al
	je	.L300
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L300
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L284:
	movq	31(%rdi), %rax
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	%rax, -37496(%rdx)
	jne	.L332
	movq	12464(%r14), %rax
	movq	39(%rax), %rax
	movq	303(%rax), %r15
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L294
	movq	%r15, %rsi
	movq	%r11, -88(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-88(%rbp), %r11
	movq	%rax, %rsi
.L295:
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%r11, -88(%rbp)
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movq	-88(%rbp), %r11
	movq	(%rax), %rdx
	movq	%rax, %r8
	movq	(%r11), %r15
	movq	%rdx, 31(%r15)
	leaq	31(%r15), %rsi
	testb	$1, %dl
	je	.L286
	movq	%rdx, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L298
	movq	%r15, %rdi
	movq	%r8, -104(%rbp)
	movq	%rdx, -96(%rbp)
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-104(%rbp), %r8
	movq	-96(%rbp), %rdx
	movq	-88(%rbp), %rsi
.L298:
	testb	$24, %al
	je	.L286
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L286
	movq	%r15, %rdi
	movq	%r8, -88(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %r8
	.p2align 4,,10
	.p2align 3
.L286:
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8JSObject30SetOwnPropertyIgnoreAttributesENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	testq	%rax, %rax
	jne	.L300
	movq	312(%r14), %rax
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L329:
	movq	%r14, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movq	%rax, %r11
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L332:
	movq	%rdi, -64(%rbp)
	movq	%r15, %rdi
	call	_ZN2v88internal7Context16extension_objectEv@PLT
	movq	41112(%r14), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L291
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r8
	jmp	.L286
	.p2align 4,,10
	.p2align 3
.L330:
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L331:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%rdi, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-88(%rbp), %rdi
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L294:
	movq	41088(%r14), %rsi
	cmpq	41096(%r14), %rsi
	je	.L333
.L296:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r14)
	movq	%r15, (%rsi)
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L291:
	movq	41088(%r14), %r8
	cmpq	41096(%r14), %r8
	je	.L334
.L293:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%r8)
	jmp	.L286
.L333:
	movq	%r14, %rdi
	movq	%r11, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %r11
	movq	%rax, %rsi
	jmp	.L296
.L334:
	movq	%r14, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L293
.L328:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22192:
	.size	_ZN2v88internal12_GLOBAL__N_117DeclareEvalHelperEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS4_INS0_6ObjectEEE, .-_ZN2v88internal12_GLOBAL__N_117DeclareEvalHelperEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS4_INS0_6ObjectEEE
	.section	.rodata._ZN2v88internal12_GLOBAL__N_118NewSloppyArgumentsINS1_18ParameterArgumentsEEENS0_6HandleINS0_8JSObjectEEEPNS0_7IsolateENS4_INS0_10JSFunctionEEET_i.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"!IsDerivedConstructor(callee->shared().kind())"
	.section	.rodata._ZN2v88internal12_GLOBAL__N_118NewSloppyArgumentsINS1_18ParameterArgumentsEEENS0_6HandleINS0_8JSObjectEEEPNS0_7IsolateENS4_INS0_10JSFunctionEEET_i.str1.1,"aMS",@progbits,1
.LC1:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal12_GLOBAL__N_118NewSloppyArgumentsINS1_18ParameterArgumentsEEENS0_6HandleINS0_8JSObjectEEEPNS0_7IsolateENS4_INS0_10JSFunctionEEET_i,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_118NewSloppyArgumentsINS1_18ParameterArgumentsEEENS0_6HandleINS0_8JSObjectEEEPNS0_7IsolateENS4_INS0_10JSFunctionEEET_i, @function
_ZN2v88internal12_GLOBAL__N_118NewSloppyArgumentsINS1_18ParameterArgumentsEEENS0_6HandleINS0_8JSObjectEEEPNS0_7IsolateENS4_INS0_10JSFunctionEEET_i:
.LFB24768:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	23(%rax), %rax
	movl	47(%rax), %eax
	andl	$31, %eax
	subl	$4, %eax
	cmpb	$1, %al
	jbe	.L456
	movq	%rdx, %rbx
	movl	%ecx, %edx
	movq	%rsi, -80(%rbp)
	movq	%rdi, %r12
	movl	%ecx, %r15d
	call	_ZN2v88internal7Factory18NewArgumentsObjectENS0_6HandleINS0_10JSFunctionEEEi@PLT
	movq	-80(%rbp), %r8
	movq	%rax, -72(%rbp)
	movq	(%r8), %rax
	movq	23(%rax), %rax
	testl	%r15d, %r15d
	jle	.L338
	movzwl	41(%rax), %eax
	testl	%eax, %eax
	je	.L339
	cmpl	%eax, %r15d
	movq	%r12, %rdi
	cmovle	%r15d, %eax
	xorl	%edx, %edx
	leal	2(%rax), %esi
	movl	%eax, -96(%rbp)
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	624(%r12), %rdx
	movq	(%rax), %rdi
	movq	%rax, %r14
	testq	%rdx, %rdx
	movq	%rdx, -1(%rdi)
	movq	-80(%rbp), %r8
	jne	.L457
.L340:
	movq	-72(%rbp), %rax
	movq	(%rax), %r13
	movq	12464(%r12), %rax
	movq	39(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L343
	movq	%r8, -80(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-80(%rbp), %r8
	movq	(%rax), %rsi
.L344:
	movq	383(%rsi), %rdx
	movq	%rdx, -1(%r13)
	testq	%rdx, %rdx
	jne	.L458
.L346:
	movq	-72(%rbp), %rax
	movq	(%r14), %r13
	movq	(%rax), %rdi
	movq	%r13, 15(%rdi)
	leaq	15(%rdi), %rsi
	testb	$1, %r13b
	je	.L394
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -80(%rbp)
	testl	$262144, %eax
	je	.L350
	movq	%r13, %rdx
	movq	%r8, -112(%rbp)
	movq	%rsi, -104(%rbp)
	movq	%rdi, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %rcx
	movq	-112(%rbp), %r8
	movq	-104(%rbp), %rsi
	movq	-88(%rbp), %rdi
	movq	8(%rcx), %rax
.L350:
	testb	$24, %al
	je	.L394
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L459
	.p2align 4,,10
	.p2align 3
.L394:
	movq	41112(%r12), %rdi
	movq	12464(%r12), %rsi
	testq	%rdi, %rdi
	je	.L352
	movq	%r8, -80(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-80(%rbp), %r8
	movq	%rax, %rcx
.L353:
	xorl	%edx, %edx
	movl	%r15d, %esi
	movq	%r12, %rdi
	movq	%r8, -88(%rbp)
	movq	%rcx, -80(%rbp)
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	-80(%rbp), %rcx
	movq	(%r14), %rdi
	movq	%rax, %r13
	movq	(%rcx), %rdx
	leaq	15(%rdi), %rsi
	movq	%rdx, 15(%rdi)
	testb	$1, %dl
	movq	-88(%rbp), %r8
	je	.L393
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -80(%rbp)
	testl	$262144, %eax
	je	.L356
	movq	%r8, -120(%rbp)
	movq	%rdx, -112(%rbp)
	movq	%rsi, -104(%rbp)
	movq	%rdi, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %rcx
	movq	-120(%rbp), %r8
	movq	-112(%rbp), %rdx
	movq	-104(%rbp), %rsi
	movq	8(%rcx), %rax
	movq	-88(%rbp), %rdi
.L356:
	testb	$24, %al
	je	.L393
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L460
	.p2align 4,,10
	.p2align 3
.L393:
	movq	(%r14), %rdi
	movq	0(%r13), %rdx
	movq	%rdx, 23(%rdi)
	leaq	23(%rdi), %rsi
	testb	$1, %dl
	je	.L392
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -80(%rbp)
	testl	$262144, %eax
	je	.L359
	movq	%r8, -120(%rbp)
	movq	%rdx, -112(%rbp)
	movq	%rsi, -104(%rbp)
	movq	%rdi, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %rcx
	movq	-120(%rbp), %r8
	movq	-112(%rbp), %rdx
	movq	-104(%rbp), %rsi
	movq	8(%rcx), %rax
	movq	-88(%rbp), %rdi
.L359:
	testb	$24, %al
	je	.L392
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L461
	.p2align 4,,10
	.p2align 3
.L392:
	cmpl	-96(%rbp), %r15d
	jle	.L462
	leal	8(,%r15,8), %eax
	movslq	%r15d, %rdx
	subl	$1, %r15d
	subl	-96(%rbp), %r15d
	subq	%r15, %rdx
	movq	%r14, -112(%rbp)
	addq	$8, %rbx
	cltq
	movq	%rdx, %r15
	movq	%r8, -128(%rbp)
	salq	$3, %r15
	movq	%r12, -120(%rbp)
	movq	%r13, %r12
	movq	%rbx, %r13
	movq	%r15, %rbx
	movq	%rax, %r15
	.p2align 4,,10
	.p2align 3
.L368:
	movq	%r13, %rax
	movq	(%r12), %r14
	subq	%r15, %rax
	movq	(%rax), %rdx
	leaq	-1(%r14,%r15), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L391
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	8(%rax), %r9
	movq	%rax, -80(%rbp)
	testl	$262144, %r9d
	je	.L365
	movq	%r14, %rdi
	movq	%rdx, -104(%rbp)
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %rax
	movq	-104(%rbp), %rdx
	movq	-88(%rbp), %rsi
	movq	8(%rax), %r9
.L365:
	andl	$24, %r9d
	je	.L391
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L391
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L391:
	subq	$8, %r15
	cmpq	%r15, %rbx
	jne	.L368
	movq	%r13, %rbx
	movq	-112(%rbp), %r14
	movq	%r12, %r13
	movq	-128(%rbp), %r8
	movq	-120(%rbp), %r12
.L367:
	movq	(%r8), %rax
	movq	23(%rax), %rax
	movq	15(%rax), %rsi
	testb	$1, %sil
	jne	.L463
.L362:
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	subq	$37592, %rdi
	call	_ZN2v88internal9ScopeInfo5EmptyEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
.L369:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L370
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L371:
	movl	-96(%rbp), %eax
	movq	%r15, -120(%rbp)
	movl	$16, %r12d
	movq	%r14, %r15
	movq	%rbx, %r14
	movq	%r13, %rbx
	subl	$1, %eax
	leaq	24(,%rax,8), %rax
	movq	%rax, -80(%rbp)
	.p2align 4,,10
	.p2align 3
.L376:
	movq	%r14, %rax
	movq	(%rbx), %rdi
	subq	%r12, %rax
	movq	(%rax), %r13
	leaq	-1(%rdi,%r12), %rsi
	movq	%r13, (%rsi)
	testb	$1, %r13b
	je	.L390
	movq	%r13, %r9
	andq	$-262144, %r9
	movq	8(%r9), %rax
	movq	%r9, -88(%rbp)
	testl	$262144, %eax
	je	.L374
	movq	%r13, %rdx
	movq	%rsi, -112(%rbp)
	movq	%rdi, -104(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %r9
	movq	-112(%rbp), %rsi
	movq	-104(%rbp), %rdi
	movq	8(%r9), %rax
.L374:
	testb	$24, %al
	je	.L390
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L390
	movq	%r13, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L390:
	movq	(%r15), %rax
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	movq	-37496(%rdx), %rdx
	movq	%rdx, 15(%rax,%r12)
	addq	$8, %r12
	cmpq	%r12, -80(%rbp)
	jne	.L376
	movq	%r15, %r14
	movq	-120(%rbp), %r15
	movq	%rbx, %r13
	movq	(%r15), %rax
	movl	11(%rax), %ecx
	testl	%ecx, %ecx
	jle	.L338
	xorl	%r12d, %r12d
	leaq	-64(%rbp), %rbx
	.p2align 4,,10
	.p2align 3
.L382:
	movq	31(%rax), %rcx
	sarq	$32, %rcx
	cmpl	%ecx, %r12d
	jge	.L338
	movl	%r12d, %esi
	movq	%rbx, %rdi
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal9ScopeInfo23ContextLocalIsParameterEi@PLT
	testb	%al, %al
	je	.L380
	movq	(%r15), %rax
	movl	%r12d, %esi
	movq	%rbx, %rdi
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal9ScopeInfo27ContextLocalParameterNumberEi@PLT
	cmpl	-96(%rbp), %eax
	jge	.L380
	movq	0(%r13), %rcx
	leal	16(,%rax,8), %eax
	movslq	%eax, %rsi
	addl	$16, %eax
	movq	%rcx, %rdi
	cltq
	andq	$-262144, %rdi
	movq	24(%rdi), %rdi
	movq	-37496(%rdi), %rdi
	movq	%rdi, -1(%rcx,%rsi)
	leal	4(%r12), %ecx
	salq	$32, %rcx
	movq	(%r14), %rsi
	movq	%rcx, -1(%rax,%rsi)
.L380:
	movq	(%r15), %rax
	addl	$1, %r12d
	movl	11(%rax), %edx
	testl	%edx, %edx
	jg	.L382
.L338:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L464
	movq	-72(%rbp), %rax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L339:
	.cfi_restore_state
	movl	%r15d, %esi
	movq	%r12, %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	-72(%rbp), %rcx
	movq	(%rax), %r12
	movq	(%rcx), %r13
	movq	%r12, 15(%r13)
	leaq	15(%r13), %rsi
	testb	$1, %r12b
	je	.L396
	movq	%r12, %r14
	andq	$-262144, %r14
	movq	8(%r14), %rdx
	testl	$262144, %edx
	je	.L384
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rax, -88(%rbp)
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r14), %rdx
	movq	-88(%rbp), %rax
	movq	-80(%rbp), %rsi
.L384:
	andl	$24, %edx
	je	.L396
	movq	%r13, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	je	.L465
	.p2align 4,,10
	.p2align 3
.L396:
	leal	-1(%r15), %edx
	leaq	-16(%rbx), %r13
	leaq	-8(%rbx), %r12
	salq	$3, %rdx
	addq	$8, %rbx
	movq	%rbx, -80(%rbp)
	subq	%rdx, %r13
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L389:
	movq	-80(%rbp), %rdx
	movq	(%rbx), %r15
	movq	(%r12), %r14
	subq	%r12, %rdx
	leaq	-1(%r15,%rdx), %rsi
	movq	%r14, (%rsi)
	testb	$1, %r14b
	je	.L395
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rdx
	movq	%rcx, -88(%rbp)
	testl	$262144, %edx
	je	.L387
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rcx
	movq	-96(%rbp), %rsi
	movq	8(%rcx), %rdx
.L387:
	andl	$24, %edx
	je	.L395
	movq	%r15, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	jne	.L395
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L395:
	subq	$8, %r12
	cmpq	%r12, %r13
	jne	.L389
	jmp	.L338
	.p2align 4,,10
	.p2align 3
.L343:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L466
.L345:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L344
	.p2align 4,,10
	.p2align 3
.L457:
	testb	$1, %dl
	je	.L340
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L340
	xorl	%esi, %esi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %r8
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L370:
	movq	41088(%r12), %r15
	cmpq	41096(%r12), %r15
	je	.L467
.L372:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r15)
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L352:
	movq	41088(%r12), %rcx
	cmpq	%rcx, 41096(%r12)
	je	.L468
.L354:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rcx)
	jmp	.L353
	.p2align 4,,10
	.p2align 3
.L456:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L463:
	movq	-1(%rsi), %rdx
	cmpw	$136, 11(%rdx)
	jne	.L362
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L458:
	testb	$1, %dl
	je	.L346
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L346
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%r8, -80(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %r8
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L461:
	movq	%r8, -80(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %r8
	jmp	.L392
	.p2align 4,,10
	.p2align 3
.L460:
	movq	%r8, -80(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %r8
	jmp	.L393
	.p2align 4,,10
	.p2align 3
.L459:
	movq	%r13, %rdx
	movq	%r8, -80(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %r8
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L465:
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %rax
	jmp	.L396
	.p2align 4,,10
	.p2align 3
.L462:
	addq	$8, %rbx
	jmp	.L367
.L468:
	movq	%r12, %rdi
	movq	%r8, -88(%rbp)
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %r8
	movq	-80(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L354
.L467:
	movq	%r12, %rdi
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L372
.L466:
	movq	%r12, %rdi
	movq	%r8, -88(%rbp)
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %r8
	movq	-80(%rbp), %rsi
	jmp	.L345
.L464:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24768:
	.size	_ZN2v88internal12_GLOBAL__N_118NewSloppyArgumentsINS1_18ParameterArgumentsEEENS0_6HandleINS0_8JSObjectEEEPNS0_7IsolateENS4_INS0_10JSFunctionEEET_i, .-_ZN2v88internal12_GLOBAL__N_118NewSloppyArgumentsINS1_18ParameterArgumentsEEENS0_6HandleINS0_8JSObjectEEEPNS0_7IsolateENS4_INS0_10JSFunctionEEET_i
	.section	.text._ZN2v88internal12_GLOBAL__N_118NewSloppyArgumentsINS1_15HandleArgumentsEEENS0_6HandleINS0_8JSObjectEEEPNS0_7IsolateENS4_INS0_10JSFunctionEEET_i,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_118NewSloppyArgumentsINS1_15HandleArgumentsEEENS0_6HandleINS0_8JSObjectEEEPNS0_7IsolateENS4_INS0_10JSFunctionEEET_i, @function
_ZN2v88internal12_GLOBAL__N_118NewSloppyArgumentsINS1_15HandleArgumentsEEENS0_6HandleINS0_8JSObjectEEEPNS0_7IsolateENS4_INS0_10JSFunctionEEET_i:
.LFB24763:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	23(%rax), %rax
	movl	47(%rax), %eax
	andl	$31, %eax
	subl	$4, %eax
	cmpb	$1, %al
	jbe	.L590
	movq	%rdx, %r15
	movl	%ecx, %edx
	movq	%rsi, -80(%rbp)
	movq	%rdi, %r12
	movl	%ecx, %r14d
	call	_ZN2v88internal7Factory18NewArgumentsObjectENS0_6HandleINS0_10JSFunctionEEEi@PLT
	movq	-80(%rbp), %r9
	movq	%rax, -72(%rbp)
	movq	(%r9), %rax
	movq	23(%rax), %rax
	testl	%r14d, %r14d
	jle	.L472
	movzwl	41(%rax), %eax
	testl	%eax, %eax
	je	.L473
	cmpl	%eax, %r14d
	movq	%r12, %rdi
	cmovle	%r14d, %eax
	xorl	%edx, %edx
	leal	2(%rax), %esi
	movl	%eax, -96(%rbp)
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	624(%r12), %rdx
	movq	(%rax), %rdi
	movq	%rax, %r13
	testq	%rdx, %rdx
	movq	%rdx, -1(%rdi)
	movq	-80(%rbp), %r9
	jne	.L591
.L474:
	movq	-72(%rbp), %rax
	movq	(%rax), %rbx
	movq	12464(%r12), %rax
	movq	39(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L477
	movq	%r9, -80(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-80(%rbp), %r9
	movq	(%rax), %rsi
.L478:
	movq	383(%rsi), %rdx
	movq	%rdx, -1(%rbx)
	testq	%rdx, %rdx
	jne	.L592
.L480:
	movq	-72(%rbp), %rax
	movq	0(%r13), %rbx
	movq	(%rax), %rdi
	movq	%rbx, 15(%rdi)
	leaq	15(%rdi), %rsi
	testb	$1, %bl
	je	.L528
	movq	%rbx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -80(%rbp)
	testl	$262144, %eax
	je	.L484
	movq	%rbx, %rdx
	movq	%r9, -112(%rbp)
	movq	%rsi, -104(%rbp)
	movq	%rdi, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %rcx
	movq	-112(%rbp), %r9
	movq	-104(%rbp), %rsi
	movq	-88(%rbp), %rdi
	movq	8(%rcx), %rax
.L484:
	testb	$24, %al
	je	.L528
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L593
	.p2align 4,,10
	.p2align 3
.L528:
	movq	41112(%r12), %rdi
	movq	12464(%r12), %rsi
	testq	%rdi, %rdi
	je	.L486
	movq	%r9, -80(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-80(%rbp), %r9
	movq	%rax, %rcx
.L487:
	xorl	%edx, %edx
	movl	%r14d, %esi
	movq	%r12, %rdi
	movq	%r9, -88(%rbp)
	movq	%rcx, -80(%rbp)
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	-80(%rbp), %rcx
	movq	0(%r13), %rdi
	movq	%rax, %rbx
	movq	(%rcx), %rdx
	leaq	15(%rdi), %rsi
	movq	%rdx, 15(%rdi)
	testb	$1, %dl
	movq	-88(%rbp), %r9
	je	.L527
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	8(%rax), %rcx
	movq	%rax, -80(%rbp)
	testl	$262144, %ecx
	je	.L490
	movq	%r9, -120(%rbp)
	movq	%rdx, -112(%rbp)
	movq	%rsi, -104(%rbp)
	movq	%rdi, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %rax
	movq	-120(%rbp), %r9
	movq	-112(%rbp), %rdx
	movq	-104(%rbp), %rsi
	movq	8(%rax), %rcx
	movq	-88(%rbp), %rdi
.L490:
	andl	$24, %ecx
	je	.L527
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L594
	.p2align 4,,10
	.p2align 3
.L527:
	movq	0(%r13), %rdi
	movq	(%rbx), %rdx
	movq	%rdx, 23(%rdi)
	leaq	23(%rdi), %rsi
	testb	$1, %dl
	je	.L526
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	8(%rax), %rcx
	movq	%rax, -80(%rbp)
	testl	$262144, %ecx
	je	.L493
	movq	%r9, -120(%rbp)
	movq	%rdx, -112(%rbp)
	movq	%rsi, -104(%rbp)
	movq	%rdi, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %rax
	movq	-120(%rbp), %r9
	movq	-112(%rbp), %rdx
	movq	-104(%rbp), %rsi
	movq	8(%rax), %rcx
	movq	-88(%rbp), %rdi
.L493:
	andl	$24, %ecx
	je	.L526
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L595
	.p2align 4,,10
	.p2align 3
.L526:
	cmpl	-96(%rbp), %r14d
	jle	.L501
	leal	8(,%r14,8), %eax
	movslq	%r14d, %rdx
	subl	$1, %r14d
	subl	-96(%rbp), %r14d
	subq	%r14, %rdx
	movq	%r9, -128(%rbp)
	cltq
	movq	%rdx, %r14
	movq	%r13, -112(%rbp)
	movq	%r15, %r13
	salq	$3, %r14
	movq	%r12, -120(%rbp)
	movq	%rbx, %r12
	movq	%r14, %rbx
	movq	%rax, %r14
	.p2align 4,,10
	.p2align 3
.L502:
	movq	-16(%r13,%r14), %rax
	movq	(%r12), %r15
	movq	(%rax), %rdx
	leaq	-1(%r15,%r14), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L525
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	8(%rax), %r9
	movq	%rax, -80(%rbp)
	testl	$262144, %r9d
	je	.L499
	movq	%r15, %rdi
	movq	%rdx, -104(%rbp)
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %rax
	movq	-104(%rbp), %rdx
	movq	-88(%rbp), %rsi
	movq	8(%rax), %r9
.L499:
	andl	$24, %r9d
	je	.L525
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L525
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L525:
	subq	$8, %r14
	cmpq	%r14, %rbx
	jne	.L502
	movq	%r12, %rbx
	movq	%r13, %r15
	movq	-120(%rbp), %r12
	movq	-128(%rbp), %r9
	movq	-112(%rbp), %r13
.L501:
	movq	(%r9), %rax
	movq	23(%rax), %rax
	movq	15(%rax), %rsi
	testb	$1, %sil
	jne	.L596
.L496:
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	subq	$37592, %rdi
	call	_ZN2v88internal9ScopeInfo5EmptyEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
.L503:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L504
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L505:
	movl	-96(%rbp), %eax
	movq	%r14, -120(%rbp)
	movl	$16, %r12d
	movq	%r13, %r14
	movq	%r15, %r9
	subl	$1, %eax
	leaq	24(,%rax,8), %rax
	movq	%rax, -80(%rbp)
	.p2align 4,,10
	.p2align 3
.L510:
	movq	-16(%r9,%r12), %rax
	movq	(%rbx), %r13
	movq	(%rax), %r15
	leaq	-1(%r13,%r12), %rsi
	movq	%r15, (%rsi)
	testb	$1, %r15b
	je	.L524
	movq	%r15, %r10
	andq	$-262144, %r10
	movq	8(%r10), %rax
	movq	%r10, -88(%rbp)
	testl	$262144, %eax
	je	.L508
	movq	%r15, %rdx
	movq	%r13, %rdi
	movq	%r9, -112(%rbp)
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %r10
	movq	-112(%rbp), %r9
	movq	-104(%rbp), %rsi
	movq	8(%r10), %rax
.L508:
	testb	$24, %al
	je	.L524
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L524
	movq	%r15, %rdx
	movq	%r13, %rdi
	movq	%r9, -88(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %r9
	.p2align 4,,10
	.p2align 3
.L524:
	movq	(%r14), %rax
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	movq	-37496(%rdx), %rdx
	movq	%rdx, 15(%rax,%r12)
	addq	$8, %r12
	cmpq	%r12, -80(%rbp)
	jne	.L510
	movq	%r14, %r13
	movq	-120(%rbp), %r14
	movq	(%r14), %rax
	movl	11(%rax), %ecx
	testl	%ecx, %ecx
	jle	.L472
	xorl	%r15d, %r15d
	leaq	-64(%rbp), %r12
	movl	%r15d, %edi
	movq	%rbx, %r15
	movl	%edi, %ebx
	.p2align 4,,10
	.p2align 3
.L516:
	movq	31(%rax), %rcx
	sarq	$32, %rcx
	cmpl	%ecx, %ebx
	jge	.L472
	movl	%ebx, %esi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal9ScopeInfo23ContextLocalIsParameterEi@PLT
	testb	%al, %al
	je	.L514
	movq	(%r14), %rax
	movl	%ebx, %esi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal9ScopeInfo27ContextLocalParameterNumberEi@PLT
	cmpl	-96(%rbp), %eax
	jge	.L514
	movq	(%r15), %rcx
	leal	16(,%rax,8), %eax
	movslq	%eax, %rsi
	addl	$16, %eax
	movq	%rcx, %rdi
	cltq
	andq	$-262144, %rdi
	movq	24(%rdi), %rdi
	movq	-37496(%rdi), %rdi
	movq	%rdi, -1(%rcx,%rsi)
	leal	4(%rbx), %ecx
	salq	$32, %rcx
	movq	0(%r13), %rsi
	movq	%rcx, -1(%rax,%rsi)
.L514:
	movq	(%r14), %rax
	addl	$1, %ebx
	movl	11(%rax), %edx
	testl	%edx, %edx
	jg	.L516
.L472:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L597
	movq	-72(%rbp), %rax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L473:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	%r14d, %esi
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	-72(%rbp), %rdi
	movq	(%rax), %r12
	movq	(%rdi), %r13
	movq	%r12, 15(%r13)
	leaq	15(%r13), %rsi
	testb	$1, %r12b
	je	.L530
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rdx
	testl	$262144, %edx
	je	.L518
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rax, -88(%rbp)
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rdx
	movq	-88(%rbp), %rax
	movq	-80(%rbp), %rsi
.L518:
	andl	$24, %edx
	je	.L530
	movq	%r13, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	je	.L598
	.p2align 4,,10
	.p2align 3
.L530:
	leal	-1(%r14), %edx
	movq	%r15, %r8
	movl	$16, %ebx
	movq	%rax, %r15
	leaq	24(,%rdx,8), %r13
	movq	%r13, -80(%rbp)
	.p2align 4,,10
	.p2align 3
.L523:
	movq	-16(%r8,%rbx), %rdx
	movq	(%r15), %r14
	movq	(%rdx), %r12
	leaq	-1(%r14,%rbx), %r13
	movq	%r12, 0(%r13)
	testb	$1, %r12b
	je	.L529
	movq	%r12, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rdx
	movq	%rcx, -88(%rbp)
	testl	$262144, %edx
	je	.L521
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%r8, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rcx
	movq	-96(%rbp), %r8
	movq	8(%rcx), %rdx
.L521:
	andl	$24, %edx
	je	.L529
	movq	%r14, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	jne	.L529
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%r8, -88(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %r8
	.p2align 4,,10
	.p2align 3
.L529:
	addq	$8, %rbx
	cmpq	%rbx, -80(%rbp)
	jne	.L523
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L477:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L599
.L479:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L478
	.p2align 4,,10
	.p2align 3
.L591:
	testb	$1, %dl
	je	.L474
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L474
	xorl	%esi, %esi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %r9
	jmp	.L474
	.p2align 4,,10
	.p2align 3
.L504:
	movq	41088(%r12), %r14
	cmpq	41096(%r12), %r14
	je	.L600
.L506:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r14)
	jmp	.L505
	.p2align 4,,10
	.p2align 3
.L486:
	movq	41088(%r12), %rcx
	cmpq	%rcx, 41096(%r12)
	je	.L601
.L488:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rcx)
	jmp	.L487
	.p2align 4,,10
	.p2align 3
.L590:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L596:
	movq	-1(%rsi), %rdx
	cmpw	$136, 11(%rdx)
	jne	.L496
	jmp	.L503
	.p2align 4,,10
	.p2align 3
.L592:
	testb	$1, %dl
	je	.L480
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L480
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%r9, -80(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %r9
	jmp	.L480
	.p2align 4,,10
	.p2align 3
.L595:
	movq	%r9, -80(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %r9
	jmp	.L526
	.p2align 4,,10
	.p2align 3
.L594:
	movq	%r9, -80(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %r9
	jmp	.L527
	.p2align 4,,10
	.p2align 3
.L593:
	movq	%rbx, %rdx
	movq	%r9, -80(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %r9
	jmp	.L528
	.p2align 4,,10
	.p2align 3
.L598:
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %rax
	jmp	.L530
.L599:
	movq	%r12, %rdi
	movq	%r9, -88(%rbp)
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %r9
	movq	-80(%rbp), %rsi
	jmp	.L479
.L601:
	movq	%r12, %rdi
	movq	%r9, -88(%rbp)
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %r9
	movq	-80(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L488
.L600:
	movq	%r12, %rdi
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L506
.L597:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24763:
	.size	_ZN2v88internal12_GLOBAL__N_118NewSloppyArgumentsINS1_15HandleArgumentsEEENS0_6HandleINS0_8JSObjectEEEPNS0_7IsolateENS4_INS0_10JSFunctionEEET_i, .-_ZN2v88internal12_GLOBAL__N_118NewSloppyArgumentsINS1_15HandleArgumentsEEENS0_6HandleINS0_8JSObjectEEEPNS0_7IsolateENS4_INS0_10JSFunctionEEET_i
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB11067:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L608
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L611
.L602:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L608:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L611:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L602
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE11067:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.rodata._ZN2v88internalL28Stats_Runtime_DeclareGlobalsEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"disabled-by-default-v8.runtime"
	.align 8
.LC3:
	.string	"V8.Runtime_Runtime_DeclareGlobals"
	.section	.rodata._ZN2v88internalL28Stats_Runtime_DeclareGlobalsEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"args[0].IsFixedArray()"
.LC5:
	.string	"args[1].IsSmi()"
.LC6:
	.string	"args[2].IsJSFunction()"
	.section	.text._ZN2v88internalL28Stats_Runtime_DeclareGlobalsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL28Stats_Runtime_DeclareGlobalsEiPmPNS0_7IsolateE, @function
_ZN2v88internalL28Stats_Runtime_DeclareGlobalsEiPmPNS0_7IsolateE:
.LFB22189:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L647
.L613:
	movq	_ZZN2v88internalL28Stats_Runtime_DeclareGlobalsEiPmPNS0_7IsolateEE28trace_event_unique_atomic205(%rip), %rbx
	testq	%rbx, %rbx
	je	.L648
.L615:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L649
.L617:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L621
.L622:
	leaq	.LC4(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L648:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L650
.L616:
	movq	%rbx, _ZZN2v88internalL28Stats_Runtime_DeclareGlobalsEiPmPNS0_7IsolateEE28trace_event_unique_atomic205(%rip)
	jmp	.L615
	.p2align 4,,10
	.p2align 3
.L621:
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	subl	$123, %eax
	cmpw	$14, %ax
	ja	.L622
	movq	-8(%r13), %rdx
	testb	$1, %dl
	jne	.L651
	movq	-16(%r13), %rax
	sarq	$32, %rdx
	leaq	-16(%r13), %rcx
	testb	$1, %al
	jne	.L652
.L624:
	leaq	.LC6(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L649:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L653
.L618:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L619
	movq	(%rdi), %rax
	call	*8(%rax)
.L619:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L620
	movq	(%rdi), %rax
	call	*8(%rax)
.L620:
	leaq	.LC3(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L617
	.p2align 4,,10
	.p2align 3
.L652:
	movq	-1(%rax), %rax
	cmpw	$1105, 11(%rax)
	jne	.L624
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_114DeclareGlobalsEPNS0_7IsolateENS0_6HandleINS0_10FixedArrayEEEiNS4_INS0_10JSFunctionEEE
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	41096(%r12), %rbx
	je	.L626
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L626:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L654
.L612:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L655
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L647:
	.cfi_restore_state
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$503, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L613
	.p2align 4,,10
	.p2align 3
.L651:
	leaq	.LC5(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L654:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L653:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC3(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L618
	.p2align 4,,10
	.p2align 3
.L650:
	leaq	.LC2(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L616
.L655:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22189:
	.size	_ZN2v88internalL28Stats_Runtime_DeclareGlobalsEiPmPNS0_7IsolateE, .-_ZN2v88internalL28Stats_Runtime_DeclareGlobalsEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL33Stats_Runtime_DeclareEvalFunctionEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC7:
	.string	"V8.Runtime_Runtime_DeclareEvalFunction"
	.section	.rodata._ZN2v88internalL33Stats_Runtime_DeclareEvalFunctionEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC8:
	.string	"args[0].IsString()"
	.section	.text._ZN2v88internalL33Stats_Runtime_DeclareEvalFunctionEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL33Stats_Runtime_DeclareEvalFunctionEiPmPNS0_7IsolateE, @function
_ZN2v88internalL33Stats_Runtime_DeclareEvalFunctionEiPmPNS0_7IsolateE:
.LFB22193:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L688
.L657:
	movq	_ZZN2v88internalL33Stats_Runtime_DeclareEvalFunctionEiPmPNS0_7IsolateEE28trace_event_unique_atomic307(%rip), %rbx
	testq	%rbx, %rbx
	je	.L689
.L659:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L690
.L661:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L665
.L666:
	leaq	.LC8(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L689:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L691
.L660:
	movq	%rbx, _ZZN2v88internalL33Stats_Runtime_DeclareEvalFunctionEiPmPNS0_7IsolateEE28trace_event_unique_atomic307(%rip)
	jmp	.L659
	.p2align 4,,10
	.p2align 3
.L665:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L666
	leaq	-8(%r13), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_117DeclareEvalHelperEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS4_INS0_6ObjectEEE
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	41096(%r12), %rbx
	je	.L667
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L667:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L692
.L656:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L693
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L690:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L694
.L662:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L663
	movq	(%rdi), %rax
	call	*8(%rax)
.L663:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L664
	movq	(%rdi), %rax
	call	*8(%rax)
.L664:
	leaq	.LC7(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L661
	.p2align 4,,10
	.p2align 3
.L688:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$501, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L657
	.p2align 4,,10
	.p2align 3
.L692:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L656
	.p2align 4,,10
	.p2align 3
.L691:
	leaq	.LC2(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L660
	.p2align 4,,10
	.p2align 3
.L694:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC7(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L662
.L693:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22193:
	.size	_ZN2v88internalL33Stats_Runtime_DeclareEvalFunctionEiPmPNS0_7IsolateE, .-_ZN2v88internalL33Stats_Runtime_DeclareEvalFunctionEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL35Stats_Runtime_ThrowConstAssignErrorEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC9:
	.string	"V8.Runtime_Runtime_ThrowConstAssignError"
	.section	.text._ZN2v88internalL35Stats_Runtime_ThrowConstAssignErrorEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL35Stats_Runtime_ThrowConstAssignErrorEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL35Stats_Runtime_ThrowConstAssignErrorEiPmPNS0_7IsolateE.isra.0:
.LFB28538:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L725
.L696:
	movq	_ZZN2v88internalL35Stats_Runtime_ThrowConstAssignErrorEiPmPNS0_7IsolateEE27trace_event_unique_atomic25(%rip), %rbx
	testq	%rbx, %rbx
	je	.L726
.L698:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L727
.L700:
	addl	$1, 41104(%r12)
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$37, %esi
	movq	%r12, %rdi
	movq	41088(%r12), %r13
	movq	41096(%r12), %rbx
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r14
	cmpq	41096(%r12), %rbx
	je	.L704
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L704:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L728
.L695:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L729
	leaq	-32(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L726:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L730
.L699:
	movq	%rbx, _ZZN2v88internalL35Stats_Runtime_ThrowConstAssignErrorEiPmPNS0_7IsolateEE27trace_event_unique_atomic25(%rip)
	jmp	.L698
	.p2align 4,,10
	.p2align 3
.L727:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L731
.L701:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L702
	movq	(%rdi), %rax
	call	*8(%rax)
.L702:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L703
	movq	(%rdi), %rax
	call	*8(%rax)
.L703:
	leaq	.LC9(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L700
	.p2align 4,,10
	.p2align 3
.L728:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L695
	.p2align 4,,10
	.p2align 3
.L725:
	movq	40960(%rdi), %rax
	leaq	-104(%rbp), %rsi
	movl	$523, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L696
	.p2align 4,,10
	.p2align 3
.L731:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC9(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L701
	.p2align 4,,10
	.p2align 3
.L730:
	leaq	.LC2(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L699
.L729:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE28538:
	.size	_ZN2v88internalL35Stats_Runtime_ThrowConstAssignErrorEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL35Stats_Runtime_ThrowConstAssignErrorEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL28Stats_Runtime_DeclareEvalVarEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC10:
	.string	"V8.Runtime_Runtime_DeclareEvalVar"
	.section	.text._ZN2v88internalL28Stats_Runtime_DeclareEvalVarEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL28Stats_Runtime_DeclareEvalVarEiPmPNS0_7IsolateE, @function
_ZN2v88internalL28Stats_Runtime_DeclareEvalVarEiPmPNS0_7IsolateE:
.LFB22196:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L764
.L733:
	movq	_ZZN2v88internalL28Stats_Runtime_DeclareEvalVarEiPmPNS0_7IsolateEE28trace_event_unique_atomic315(%rip), %rbx
	testq	%rbx, %rbx
	je	.L765
.L735:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L766
.L737:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L741
.L742:
	leaq	.LC8(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L765:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L767
.L736:
	movq	%rbx, _ZZN2v88internalL28Stats_Runtime_DeclareEvalVarEiPmPNS0_7IsolateEE28trace_event_unique_atomic315(%rip)
	jmp	.L735
	.p2align 4,,10
	.p2align 3
.L741:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L742
	movq	%r13, %rsi
	leaq	88(%r12), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_117DeclareEvalHelperEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS4_INS0_6ObjectEEE
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	41096(%r12), %rbx
	je	.L743
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L743:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L768
.L732:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L769
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L766:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L770
.L738:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L739
	movq	(%rdi), %rax
	call	*8(%rax)
.L739:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L740
	movq	(%rdi), %rax
	call	*8(%rax)
.L740:
	leaq	.LC10(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L737
	.p2align 4,,10
	.p2align 3
.L764:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$502, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L733
	.p2align 4,,10
	.p2align 3
.L768:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L732
	.p2align 4,,10
	.p2align 3
.L767:
	leaq	.LC2(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L736
	.p2align 4,,10
	.p2align 3
.L770:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC10(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L738
.L769:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22196:
	.size	_ZN2v88internalL28Stats_Runtime_DeclareEvalVarEiPmPNS0_7IsolateE, .-_ZN2v88internalL28Stats_Runtime_DeclareEvalVarEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL34Stats_Runtime_NewArgumentsElementsEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC11:
	.string	"V8.Runtime_Runtime_NewArgumentsElements"
	.section	.rodata._ZN2v88internalL34Stats_Runtime_NewArgumentsElementsEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC12:
	.string	"args[2].IsSmi()"
	.section	.text._ZN2v88internalL34Stats_Runtime_NewArgumentsElementsEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL34Stats_Runtime_NewArgumentsElementsEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL34Stats_Runtime_NewArgumentsElementsEiPmPNS0_7IsolateE.isra.0:
.LFB28543:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L828
.L772:
	movq	_ZZN2v88internalL34Stats_Runtime_NewArgumentsElementsEiPmPNS0_7IsolateEE28trace_event_unique_atomic577(%rip), %r12
	testq	%r12, %r12
	je	.L829
.L774:
	movq	$0, -160(%rbp)
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L830
.L776:
	addl	$1, 41104(%r14)
	movq	41088(%r14), %rax
	movq	-8(%rbx), %r8
	movq	41096(%r14), %r13
	movq	%rax, -168(%rbp)
	testb	$1, %r8b
	jne	.L831
	movq	-16(%rbx), %rdi
	sarq	$32, %r8
	testb	$1, %dil
	jne	.L832
	sarq	$32, %rdi
	xorl	%edx, %edx
	movq	(%rbx), %r15
	movl	%r8d, %esi
	movq	%rdi, %rbx
	movq	%r14, %rdi
	movq	%r8, -176(%rbp)
	call	_ZN2v88internal7Factory26NewUninitializedFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	-176(%rbp), %r8
	movl	$4, %ecx
	movq	(%rax), %r12
	movq	%r12, %rdx
	andq	$-262144, %rdx
	movq	8(%rdx), %rdx
	testl	$262144, %edx
	jne	.L782
	xorl	%ecx, %ecx
	andl	$24, %edx
	sete	%cl
	sall	$2, %ecx
.L782:
	cmpl	%ebx, %r8d
	movl	%ebx, %edi
	cmovle	%r8d, %edi
	testl	%edi, %edi
	jle	.L783
	leal	-1(%rdi), %edx
	leaq	24(,%rdx,8), %r9
	movl	$16, %edx
	.p2align 4,,10
	.p2align 3
.L784:
	movq	96(%r14), %rsi
	addq	$8, %rdx
	movq	%rsi, -9(%rdx,%r12)
	movq	(%rax), %r12
	cmpq	%rdx, %r9
	jne	.L784
.L783:
	cmpl	%edi, %r8d
	jle	.L785
	leal	-1(%r8), %edx
	leal	16(,%rdi,8), %ebx
	subl	%edi, %edx
	movslq	%edi, %rdi
	leaq	24(%r15,%r8,8), %r8
	movslq	%ebx, %rbx
	leaq	3(%rdx,%rdi), %r9
	salq	$3, %r9
	.p2align 4,,10
	.p2align 3
.L793:
	movq	%r8, %rdx
	leaq	-1(%rbx,%r12), %rsi
	subq	%rbx, %rdx
	movq	(%rdx), %r15
	movq	%r15, (%rsi)
	testl	%ecx, %ecx
	je	.L788
	movq	%r15, %rdx
	notq	%rdx
	andl	$1, %edx
	cmpl	$4, %ecx
	je	.L833
	testb	%dl, %dl
	jne	.L788
.L816:
	movq	%r15, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	je	.L788
	movq	%r12, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	jne	.L788
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%r8, -200(%rbp)
	movq	%r9, -192(%rbp)
	movl	%ecx, -184(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-200(%rbp), %r8
	movq	-192(%rbp), %r9
	movl	-184(%rbp), %ecx
	movq	-176(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L788:
	addq	$8, %rbx
	movq	(%rax), %r12
	cmpq	%rbx, %r9
	jne	.L793
.L785:
	movq	-168(%rbp), %rax
	subl	$1, 41104(%r14)
	movq	%rax, 41088(%r14)
	cmpq	41096(%r14), %r13
	je	.L794
	movq	%r13, 41096(%r14)
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L794:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L834
.L771:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L835
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L833:
	.cfi_restore_state
	testb	%dl, %dl
	jne	.L788
	movq	%r15, %rdx
	andq	$-262144, %rdx
	testb	$4, 10(%rdx)
	je	.L816
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%r8, -208(%rbp)
	movq	%r9, -200(%rbp)
	movl	%ecx, -192(%rbp)
	movq	%rax, -184(%rbp)
	movq	%rsi, -176(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-176(%rbp), %rsi
	movq	-184(%rbp), %rax
	movl	-192(%rbp), %ecx
	movq	-200(%rbp), %r9
	movq	-208(%rbp), %r8
	jmp	.L816
	.p2align 4,,10
	.p2align 3
.L830:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L836
.L777:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L778
	movq	(%rdi), %rax
	call	*8(%rax)
.L778:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L779
	movq	(%rdi), %rax
	call	*8(%rax)
.L779:
	leaq	.LC11(%rip), %rax
	movq	%r12, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r13, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L776
	.p2align 4,,10
	.p2align 3
.L829:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L837
.L775:
	movq	%r12, _ZZN2v88internalL34Stats_Runtime_NewArgumentsElementsEiPmPNS0_7IsolateEE28trace_event_unique_atomic577(%rip)
	jmp	.L774
	.p2align 4,,10
	.p2align 3
.L828:
	movq	40960(%rsi), %rax
	movl	$507, %edx
	leaq	-120(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L772
	.p2align 4,,10
	.p2align 3
.L831:
	leaq	.LC5(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L832:
	leaq	.LC12(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L834:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L771
.L837:
	leaq	.LC2(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L775
.L836:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC11(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L777
.L835:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE28543:
	.size	_ZN2v88internalL34Stats_Runtime_NewArgumentsElementsEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL34Stats_Runtime_NewArgumentsElementsEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL24Stats_Runtime_NewClosureEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC13:
	.string	"V8.Runtime_Runtime_NewClosure"
	.section	.rodata._ZN2v88internalL24Stats_Runtime_NewClosureEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC14:
	.string	"args[0].IsSharedFunctionInfo()"
	.section	.rodata._ZN2v88internalL24Stats_Runtime_NewClosureEiPmPNS0_7IsolateE.str1.1
.LC15:
	.string	"args[1].IsFeedbackCell()"
	.section	.text._ZN2v88internalL24Stats_Runtime_NewClosureEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL24Stats_Runtime_NewClosureEiPmPNS0_7IsolateE, @function
_ZN2v88internalL24Stats_Runtime_NewClosureEiPmPNS0_7IsolateE:
.LFB22241:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L876
.L839:
	movq	_ZZN2v88internalL24Stats_Runtime_NewClosureEiPmPNS0_7IsolateEE28trace_event_unique_atomic601(%rip), %rbx
	testq	%rbx, %rbx
	je	.L877
.L841:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L878
.L843:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L847
.L848:
	leaq	.LC14(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L877:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L879
.L842:
	movq	%rbx, _ZZN2v88internalL24Stats_Runtime_NewClosureEiPmPNS0_7IsolateEE28trace_event_unique_atomic601(%rip)
	jmp	.L841
	.p2align 4,,10
	.p2align 3
.L847:
	movq	-1(%rax), %rax
	cmpw	$160, 11(%rax)
	jne	.L848
	movq	-8(%r13), %rax
	leaq	-8(%r13), %r15
	testb	$1, %al
	jne	.L880
.L849:
	leaq	.LC15(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L878:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L881
.L844:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L845
	movq	(%rdi), %rax
	call	*8(%rax)
.L845:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L846
	movq	(%rdi), %rax
	call	*8(%rax)
.L846:
	leaq	.LC13(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L843
	.p2align 4,,10
	.p2align 3
.L880:
	movq	-1(%rax), %rax
	cmpw	$154, 11(%rax)
	jne	.L849
	movq	41112(%r12), %rdi
	movq	12464(%r12), %rsi
	testq	%rdi, %rdi
	je	.L851
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L852:
	movq	%r13, %rsi
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory33NewFunctionFromSharedFunctionInfoENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS2_INS0_12FeedbackCellEEENS0_14AllocationTypeE@PLT
	movq	(%rax), %r13
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L856
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L856:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L882
.L838:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L883
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L851:
	.cfi_restore_state
	movq	%r14, %rdx
	cmpq	41096(%r12), %r14
	je	.L884
.L853:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdx)
	jmp	.L852
	.p2align 4,,10
	.p2align 3
.L876:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$508, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L839
	.p2align 4,,10
	.p2align 3
.L882:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L838
	.p2align 4,,10
	.p2align 3
.L881:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC13(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L844
	.p2align 4,,10
	.p2align 3
.L879:
	leaq	.LC2(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L842
	.p2align 4,,10
	.p2align 3
.L884:
	movq	%r12, %rdi
	movq	%rsi, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L853
.L883:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22241:
	.size	_ZN2v88internalL24Stats_Runtime_NewClosureEiPmPNS0_7IsolateE, .-_ZN2v88internalL24Stats_Runtime_NewClosureEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL32Stats_Runtime_NewClosure_TenuredEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC16:
	.string	"V8.Runtime_Runtime_NewClosure_Tenured"
	.section	.text._ZN2v88internalL32Stats_Runtime_NewClosure_TenuredEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL32Stats_Runtime_NewClosure_TenuredEiPmPNS0_7IsolateE, @function
_ZN2v88internalL32Stats_Runtime_NewClosure_TenuredEiPmPNS0_7IsolateE:
.LFB22244:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L923
.L886:
	movq	_ZZN2v88internalL32Stats_Runtime_NewClosure_TenuredEiPmPNS0_7IsolateEE28trace_event_unique_atomic613(%rip), %rbx
	testq	%rbx, %rbx
	je	.L924
.L888:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L925
.L890:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L894
.L895:
	leaq	.LC14(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L924:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L926
.L889:
	movq	%rbx, _ZZN2v88internalL32Stats_Runtime_NewClosure_TenuredEiPmPNS0_7IsolateEE28trace_event_unique_atomic613(%rip)
	jmp	.L888
	.p2align 4,,10
	.p2align 3
.L894:
	movq	-1(%rax), %rax
	cmpw	$160, 11(%rax)
	jne	.L895
	movq	-8(%r13), %rax
	leaq	-8(%r13), %r15
	testb	$1, %al
	jne	.L927
.L896:
	leaq	.LC15(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L925:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L928
.L891:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L892
	movq	(%rdi), %rax
	call	*8(%rax)
.L892:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L893
	movq	(%rdi), %rax
	call	*8(%rax)
.L893:
	leaq	.LC16(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L890
	.p2align 4,,10
	.p2align 3
.L927:
	movq	-1(%rax), %rax
	cmpw	$154, 11(%rax)
	jne	.L896
	movq	41112(%r12), %rdi
	movq	12464(%r12), %rsi
	testq	%rdi, %rdi
	je	.L898
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L899:
	movq	%r13, %rsi
	movl	$1, %r8d
	movq	%r15, %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory33NewFunctionFromSharedFunctionInfoENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS2_INS0_12FeedbackCellEEENS0_14AllocationTypeE@PLT
	movq	(%rax), %r13
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L903
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L903:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L929
.L885:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L930
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L898:
	.cfi_restore_state
	movq	%r14, %rdx
	cmpq	41096(%r12), %r14
	je	.L931
.L900:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdx)
	jmp	.L899
	.p2align 4,,10
	.p2align 3
.L923:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$509, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L886
	.p2align 4,,10
	.p2align 3
.L929:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L885
	.p2align 4,,10
	.p2align 3
.L928:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC16(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L891
	.p2align 4,,10
	.p2align 3
.L926:
	leaq	.LC2(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L889
	.p2align 4,,10
	.p2align 3
.L931:
	movq	%r12, %rdi
	movq	%rsi, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L900
.L930:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22244:
	.size	_ZN2v88internalL32Stats_Runtime_NewClosure_TenuredEiPmPNS0_7IsolateE, .-_ZN2v88internalL32Stats_Runtime_NewClosure_TenuredEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL32Stats_Runtime_NewFunctionContextEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC17:
	.string	"V8.Runtime_Runtime_NewFunctionContext"
	.section	.rodata._ZN2v88internalL32Stats_Runtime_NewFunctionContextEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC18:
	.string	"args[0].IsScopeInfo()"
	.section	.text._ZN2v88internalL32Stats_Runtime_NewFunctionContextEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL32Stats_Runtime_NewFunctionContextEiPmPNS0_7IsolateE, @function
_ZN2v88internalL32Stats_Runtime_NewFunctionContextEiPmPNS0_7IsolateE:
.LFB22251:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L968
.L933:
	movq	_ZZN2v88internalL32Stats_Runtime_NewFunctionContextEiPmPNS0_7IsolateEE28trace_event_unique_atomic690(%rip), %rbx
	testq	%rbx, %rbx
	je	.L969
.L935:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L970
.L937:
	movq	41088(%r12), %rbx
	movq	41096(%r12), %r14
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L941
.L942:
	leaq	.LC18(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L969:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L971
.L936:
	movq	%rbx, _ZZN2v88internalL32Stats_Runtime_NewFunctionContextEiPmPNS0_7IsolateEE28trace_event_unique_atomic690(%rip)
	jmp	.L935
	.p2align 4,,10
	.p2align 3
.L941:
	movq	-1(%rax), %rax
	cmpw	$136, 11(%rax)
	jne	.L942
	movq	41112(%r12), %rdi
	movq	12464(%r12), %r15
	testq	%rdi, %rdi
	je	.L943
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L944:
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory18NewFunctionContextENS0_6HandleINS0_7ContextEEENS2_INS0_9ScopeInfoEEE@PLT
	movq	(%rax), %r13
	movq	%rbx, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r14
	je	.L948
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L948:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L972
.L932:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L973
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L943:
	.cfi_restore_state
	movq	%rbx, %rsi
	cmpq	41096(%r12), %rbx
	je	.L974
.L945:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r15, (%rsi)
	jmp	.L944
	.p2align 4,,10
	.p2align 3
.L970:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L975
.L938:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L939
	movq	(%rdi), %rax
	call	*8(%rax)
.L939:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L940
	movq	(%rdi), %rax
	call	*8(%rax)
.L940:
	leaq	.LC17(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L937
	.p2align 4,,10
	.p2align 3
.L968:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$510, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L933
	.p2align 4,,10
	.p2align 3
.L972:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L932
	.p2align 4,,10
	.p2align 3
.L974:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L945
	.p2align 4,,10
	.p2align 3
.L975:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC17(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L938
	.p2align 4,,10
	.p2align 3
.L971:
	leaq	.LC2(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L936
.L973:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22251:
	.size	_ZN2v88internalL32Stats_Runtime_NewFunctionContextEiPmPNS0_7IsolateE, .-_ZN2v88internalL32Stats_Runtime_NewFunctionContextEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL29Stats_Runtime_PushWithContextEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC19:
	.string	"V8.Runtime_Runtime_PushWithContext"
	.section	.rodata._ZN2v88internalL29Stats_Runtime_PushWithContextEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC20:
	.string	"args[0].IsJSReceiver()"
.LC21:
	.string	"args[1].IsScopeInfo()"
	.section	.text._ZN2v88internalL29Stats_Runtime_PushWithContextEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL29Stats_Runtime_PushWithContextEiPmPNS0_7IsolateE, @function
_ZN2v88internalL29Stats_Runtime_PushWithContextEiPmPNS0_7IsolateE:
.LFB22254:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1014
.L977:
	movq	_ZZN2v88internalL29Stats_Runtime_PushWithContextEiPmPNS0_7IsolateEE28trace_event_unique_atomic700(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1015
.L979:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1016
.L981:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L985
.L986:
	leaq	.LC20(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1015:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1017
.L980:
	movq	%rbx, _ZZN2v88internalL29Stats_Runtime_PushWithContextEiPmPNS0_7IsolateEE28trace_event_unique_atomic700(%rip)
	jmp	.L979
	.p2align 4,,10
	.p2align 3
.L985:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L986
	movq	-8(%r13), %rax
	leaq	-8(%r13), %r15
	testb	$1, %al
	jne	.L1018
.L987:
	leaq	.LC21(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1016:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1019
.L982:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L983
	movq	(%rdi), %rax
	call	*8(%rax)
.L983:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L984
	movq	(%rdi), %rax
	call	*8(%rax)
.L984:
	leaq	.LC19(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L981
	.p2align 4,,10
	.p2align 3
.L1018:
	movq	-1(%rax), %rax
	cmpw	$136, 11(%rax)
	jne	.L987
	movq	41112(%r12), %rdi
	movq	12464(%r12), %r8
	testq	%rdi, %rdi
	je	.L989
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L990:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory14NewWithContextENS0_6HandleINS0_7ContextEEENS2_INS0_9ScopeInfoEEENS2_INS0_10JSReceiverEEE@PLT
	movq	(%rax), %r13
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%r13, 12464(%r12)
	cmpq	41096(%r12), %rbx
	je	.L994
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L994:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1020
.L976:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1021
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L989:
	.cfi_restore_state
	movq	%r14, %rsi
	cmpq	41096(%r12), %r14
	je	.L1022
.L991:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r8, (%rsi)
	jmp	.L990
	.p2align 4,,10
	.p2align 3
.L1014:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$519, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L977
	.p2align 4,,10
	.p2align 3
.L1020:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L976
	.p2align 4,,10
	.p2align 3
.L1019:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC19(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L982
	.p2align 4,,10
	.p2align 3
.L1017:
	leaq	.LC2(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L980
	.p2align 4,,10
	.p2align 3
.L1022:
	movq	%r12, %rdi
	movq	%r8, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L991
.L1021:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22254:
	.size	_ZN2v88internalL29Stats_Runtime_PushWithContextEiPmPNS0_7IsolateE, .-_ZN2v88internalL29Stats_Runtime_PushWithContextEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL31Stats_Runtime_PushModuleContextEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC22:
	.string	"V8.Runtime_Runtime_PushModuleContext"
	.section	.rodata._ZN2v88internalL31Stats_Runtime_PushModuleContextEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC23:
	.string	"args[0].IsSourceTextModule()"
	.section	.text._ZN2v88internalL31Stats_Runtime_PushModuleContextEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL31Stats_Runtime_PushModuleContextEiPmPNS0_7IsolateE, @function
_ZN2v88internalL31Stats_Runtime_PushModuleContextEiPmPNS0_7IsolateE:
.LFB22257:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1061
.L1024:
	movq	_ZZN2v88internalL31Stats_Runtime_PushModuleContextEiPmPNS0_7IsolateEE28trace_event_unique_atomic712(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1062
.L1026:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1063
.L1028:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L1032
.L1033:
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1062:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1064
.L1027:
	movq	%rbx, _ZZN2v88internalL31Stats_Runtime_PushModuleContextEiPmPNS0_7IsolateEE28trace_event_unique_atomic712(%rip)
	jmp	.L1026
	.p2align 4,,10
	.p2align 3
.L1032:
	movq	-1(%rax), %rax
	cmpw	$119, 11(%rax)
	jne	.L1033
	movq	-8(%r13), %rax
	leaq	-8(%r13), %r15
	testb	$1, %al
	jne	.L1065
.L1034:
	leaq	.LC21(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1063:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1066
.L1029:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1030
	movq	(%rdi), %rax
	call	*8(%rax)
.L1030:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1031
	movq	(%rdi), %rax
	call	*8(%rax)
.L1031:
	leaq	.LC22(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1028
	.p2align 4,,10
	.p2align 3
.L1065:
	movq	-1(%rax), %rax
	cmpw	$136, 11(%rax)
	jne	.L1034
	movq	41112(%r12), %rdi
	movq	12464(%r12), %rsi
	testq	%rdi, %rdi
	je	.L1036
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L1037:
	movq	%r13, %rsi
	movq	%r15, %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory16NewModuleContextENS0_6HandleINS0_16SourceTextModuleEEENS2_INS0_13NativeContextEEENS2_INS0_9ScopeInfoEEE@PLT
	movq	(%rax), %r13
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%r13, 12464(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1041
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1041:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1067
.L1023:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1068
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1036:
	.cfi_restore_state
	movq	%r14, %rdx
	cmpq	41096(%r12), %r14
	je	.L1069
.L1038:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdx)
	jmp	.L1037
	.p2align 4,,10
	.p2align 3
.L1061:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$518, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1024
	.p2align 4,,10
	.p2align 3
.L1067:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1023
	.p2align 4,,10
	.p2align 3
.L1066:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC22(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1029
	.p2align 4,,10
	.p2align 3
.L1064:
	leaq	.LC2(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1027
	.p2align 4,,10
	.p2align 3
.L1069:
	movq	%r12, %rdi
	movq	%rsi, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L1038
.L1068:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22257:
	.size	_ZN2v88internalL31Stats_Runtime_PushModuleContextEiPmPNS0_7IsolateE, .-_ZN2v88internalL31Stats_Runtime_PushModuleContextEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL30Stats_Runtime_PushCatchContextEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC24:
	.string	"V8.Runtime_Runtime_PushCatchContext"
	.section	.text._ZN2v88internalL30Stats_Runtime_PushCatchContextEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL30Stats_Runtime_PushCatchContextEiPmPNS0_7IsolateE, @function
_ZN2v88internalL30Stats_Runtime_PushCatchContextEiPmPNS0_7IsolateE:
.LFB22260:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1106
.L1071:
	movq	_ZZN2v88internalL30Stats_Runtime_PushCatchContextEiPmPNS0_7IsolateEE28trace_event_unique_atomic725(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1107
.L1073:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1108
.L1075:
	addl	$1, 41104(%r12)
	movq	-8(%r13), %rax
	leaq	-8(%r13), %r15
	movq	41088(%r12), %rbx
	movq	41096(%r12), %r14
	testb	$1, %al
	jne	.L1079
.L1080:
	leaq	.LC21(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1107:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1109
.L1074:
	movq	%rbx, _ZZN2v88internalL30Stats_Runtime_PushCatchContextEiPmPNS0_7IsolateEE28trace_event_unique_atomic725(%rip)
	jmp	.L1073
	.p2align 4,,10
	.p2align 3
.L1079:
	movq	-1(%rax), %rax
	cmpw	$136, 11(%rax)
	jne	.L1080
	movq	41112(%r12), %rdi
	movq	12464(%r12), %r8
	testq	%rdi, %rdi
	je	.L1081
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1082:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory15NewCatchContextENS0_6HandleINS0_7ContextEEENS2_INS0_9ScopeInfoEEENS2_INS0_6ObjectEEE@PLT
	movq	(%rax), %r13
	movq	%rbx, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%r13, 12464(%r12)
	cmpq	41096(%r12), %r14
	je	.L1086
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1086:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1110
.L1070:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1111
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1081:
	.cfi_restore_state
	movq	%rbx, %rsi
	cmpq	41096(%r12), %rbx
	je	.L1112
.L1083:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r8, (%rsi)
	jmp	.L1082
	.p2align 4,,10
	.p2align 3
.L1108:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1113
.L1076:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1077
	movq	(%rdi), %rax
	call	*8(%rax)
.L1077:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1078
	movq	(%rdi), %rax
	call	*8(%rax)
.L1078:
	leaq	.LC24(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1075
	.p2align 4,,10
	.p2align 3
.L1106:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$517, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1071
	.p2align 4,,10
	.p2align 3
.L1110:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1070
	.p2align 4,,10
	.p2align 3
.L1112:
	movq	%r12, %rdi
	movq	%r8, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L1083
	.p2align 4,,10
	.p2align 3
.L1113:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC24(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1076
	.p2align 4,,10
	.p2align 3
.L1109:
	leaq	.LC2(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1074
.L1111:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22260:
	.size	_ZN2v88internalL30Stats_Runtime_PushCatchContextEiPmPNS0_7IsolateE, .-_ZN2v88internalL30Stats_Runtime_PushCatchContextEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL30Stats_Runtime_PushBlockContextEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC25:
	.string	"V8.Runtime_Runtime_PushBlockContext"
	.section	.text._ZN2v88internalL30Stats_Runtime_PushBlockContextEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL30Stats_Runtime_PushBlockContextEiPmPNS0_7IsolateE, @function
_ZN2v88internalL30Stats_Runtime_PushBlockContextEiPmPNS0_7IsolateE:
.LFB22263:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1150
.L1115:
	movq	_ZZN2v88internalL30Stats_Runtime_PushBlockContextEiPmPNS0_7IsolateEE28trace_event_unique_atomic738(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1151
.L1117:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1152
.L1119:
	movq	41088(%r12), %rbx
	movq	41096(%r12), %r14
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L1123
.L1124:
	leaq	.LC18(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1151:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1153
.L1118:
	movq	%rbx, _ZZN2v88internalL30Stats_Runtime_PushBlockContextEiPmPNS0_7IsolateEE28trace_event_unique_atomic738(%rip)
	jmp	.L1117
	.p2align 4,,10
	.p2align 3
.L1123:
	movq	-1(%rax), %rax
	cmpw	$136, 11(%rax)
	jne	.L1124
	movq	41112(%r12), %rdi
	movq	12464(%r12), %r15
	testq	%rdi, %rdi
	je	.L1125
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1126:
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory15NewBlockContextENS0_6HandleINS0_7ContextEEENS2_INS0_9ScopeInfoEEE@PLT
	movq	(%rax), %r13
	movq	%rbx, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%r13, 12464(%r12)
	cmpq	41096(%r12), %r14
	je	.L1130
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1130:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1154
.L1114:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1155
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1125:
	.cfi_restore_state
	movq	%rbx, %rsi
	cmpq	41096(%r12), %rbx
	je	.L1156
.L1127:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r15, (%rsi)
	jmp	.L1126
	.p2align 4,,10
	.p2align 3
.L1152:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1157
.L1120:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1121
	movq	(%rdi), %rax
	call	*8(%rax)
.L1121:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1122
	movq	(%rdi), %rax
	call	*8(%rax)
.L1122:
	leaq	.LC25(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1119
	.p2align 4,,10
	.p2align 3
.L1150:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$516, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1115
	.p2align 4,,10
	.p2align 3
.L1154:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1114
	.p2align 4,,10
	.p2align 3
.L1156:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L1127
	.p2align 4,,10
	.p2align 3
.L1157:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC25(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1120
	.p2align 4,,10
	.p2align 3
.L1153:
	leaq	.LC2(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1118
.L1155:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22263:
	.size	_ZN2v88internalL30Stats_Runtime_PushBlockContextEiPmPNS0_7IsolateE, .-_ZN2v88internalL30Stats_Runtime_PushBlockContextEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL28Stats_Runtime_LoadLookupSlotEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC26:
	.string	"V8.Runtime_Runtime_LoadLookupSlot"
	.section	.text._ZN2v88internalL28Stats_Runtime_LoadLookupSlotEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL28Stats_Runtime_LoadLookupSlotEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL28Stats_Runtime_LoadLookupSlotEiPmPNS0_7IsolateE.isra.0:
.LFB28546:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1193
.L1159:
	movq	_ZZN2v88internalL28Stats_Runtime_LoadLookupSlotEiPmPNS0_7IsolateEE28trace_event_unique_atomic856(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1194
.L1161:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1195
.L1163:
	movq	41088(%r12), %rbx
	movq	41096(%r12), %r14
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L1167
.L1168:
	leaq	.LC8(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1194:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1196
.L1162:
	movq	%rbx, _ZZN2v88internalL28Stats_Runtime_LoadLookupSlotEiPmPNS0_7IsolateEE28trace_event_unique_atomic856(%rip)
	jmp	.L1161
	.p2align 4,,10
	.p2align 3
.L1167:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1168
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_114LoadLookupSlotEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS0_11ShouldThrowEPNS4_INS0_6ObjectEEE
	testq	%rax, %rax
	je	.L1197
	movq	(%rax), %r13
.L1170:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L1173
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1173:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1198
.L1158:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1199
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1195:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1200
.L1164:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1165
	movq	(%rdi), %rax
	call	*8(%rax)
.L1165:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1166
	movq	(%rdi), %rax
	call	*8(%rax)
.L1166:
	leaq	.LC26(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1163
	.p2align 4,,10
	.p2align 3
.L1197:
	movq	312(%r12), %r13
	jmp	.L1170
	.p2align 4,,10
	.p2align 3
.L1193:
	movq	40960(%rsi), %rax
	movl	$505, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1159
	.p2align 4,,10
	.p2align 3
.L1198:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1158
	.p2align 4,,10
	.p2align 3
.L1196:
	leaq	.LC2(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1162
	.p2align 4,,10
	.p2align 3
.L1200:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC26(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1164
.L1199:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE28546:
	.size	_ZN2v88internalL28Stats_Runtime_LoadLookupSlotEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL28Stats_Runtime_LoadLookupSlotEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL40Stats_Runtime_LoadLookupSlotInsideTypeofEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC27:
	.string	"V8.Runtime_Runtime_LoadLookupSlotInsideTypeof"
	.section	.text._ZN2v88internalL40Stats_Runtime_LoadLookupSlotInsideTypeofEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL40Stats_Runtime_LoadLookupSlotInsideTypeofEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL40Stats_Runtime_LoadLookupSlotInsideTypeofEiPmPNS0_7IsolateE.isra.0:
.LFB28547:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1236
.L1202:
	movq	_ZZN2v88internalL40Stats_Runtime_LoadLookupSlotInsideTypeofEiPmPNS0_7IsolateEE28trace_event_unique_atomic865(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1237
.L1204:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1238
.L1206:
	movq	41088(%r12), %rbx
	movq	41096(%r12), %r14
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L1210
.L1211:
	leaq	.LC8(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1237:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1239
.L1205:
	movq	%rbx, _ZZN2v88internalL40Stats_Runtime_LoadLookupSlotInsideTypeofEiPmPNS0_7IsolateEE28trace_event_unique_atomic865(%rip)
	jmp	.L1204
	.p2align 4,,10
	.p2align 3
.L1210:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1211
	xorl	%ecx, %ecx
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_114LoadLookupSlotEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS0_11ShouldThrowEPNS4_INS0_6ObjectEEE
	testq	%rax, %rax
	je	.L1240
	movq	(%rax), %r13
.L1213:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L1216
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1216:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1241
.L1201:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1242
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1238:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1243
.L1207:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1208
	movq	(%rdi), %rax
	call	*8(%rax)
.L1208:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1209
	movq	(%rdi), %rax
	call	*8(%rax)
.L1209:
	leaq	.LC27(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1206
	.p2align 4,,10
	.p2align 3
.L1240:
	movq	312(%r12), %r13
	jmp	.L1213
	.p2align 4,,10
	.p2align 3
.L1236:
	movq	40960(%rsi), %rax
	movl	$506, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1202
	.p2align 4,,10
	.p2align 3
.L1241:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1201
	.p2align 4,,10
	.p2align 3
.L1239:
	leaq	.LC2(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1205
	.p2align 4,,10
	.p2align 3
.L1243:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC27(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1207
.L1242:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE28547:
	.size	_ZN2v88internalL40Stats_Runtime_LoadLookupSlotInsideTypeofEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL40Stats_Runtime_LoadLookupSlotInsideTypeofEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL35Stats_Runtime_LoadLookupSlotForCallEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC28:
	.string	"V8.Runtime_Runtime_LoadLookupSlotForCall"
	.section	.text._ZN2v88internalL35Stats_Runtime_LoadLookupSlotForCallEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL35Stats_Runtime_LoadLookupSlotForCallEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL35Stats_Runtime_LoadLookupSlotForCallEiPmPNS0_7IsolateE.isra.0:
.LFB28548:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1278
.L1245:
	movq	_ZZN2v88internalL35Stats_Runtime_LoadLookupSlotForCallEiPmPNS0_7IsolateEE28trace_event_unique_atomic873(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1279
.L1247:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1280
.L1249:
	xorl	%edx, %edx
	leaq	-168(%rbp), %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	41088(%r12), %rbx
	movq	41096(%r12), %r14
	movq	$0, -168(%rbp)
	addl	$1, 41104(%r12)
	call	_ZN2v88internal12_GLOBAL__N_114LoadLookupSlotEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS0_11ShouldThrowEPNS4_INS0_6ObjectEEE
	testq	%rax, %rax
	je	.L1281
	movq	-168(%rbp), %rdx
	movq	(%rax), %r15
	movq	(%rdx), %r13
.L1254:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L1257
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1257:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1282
.L1255:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1283
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	movq	%r13, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1280:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1284
.L1250:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1251
	movq	(%rdi), %rax
	call	*8(%rax)
.L1251:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1252
	movq	(%rdi), %rax
	call	*8(%rax)
.L1252:
	leaq	.LC28(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1249
	.p2align 4,,10
	.p2align 3
.L1279:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1285
.L1248:
	movq	%rbx, _ZZN2v88internalL35Stats_Runtime_LoadLookupSlotForCallEiPmPNS0_7IsolateEE28trace_event_unique_atomic873(%rip)
	jmp	.L1247
	.p2align 4,,10
	.p2align 3
.L1281:
	movq	312(%r12), %r15
	xorl	%r13d, %r13d
	jmp	.L1254
	.p2align 4,,10
	.p2align 3
.L1282:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1255
	.p2align 4,,10
	.p2align 3
.L1278:
	movq	40960(%rsi), %rax
	movl	$203, %edx
	leaq	-120(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1245
	.p2align 4,,10
	.p2align 3
.L1285:
	leaq	.LC2(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1248
	.p2align 4,,10
	.p2align 3
.L1284:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC28(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1250
.L1283:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE28548:
	.size	_ZN2v88internalL35Stats_Runtime_LoadLookupSlotForCallEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL35Stats_Runtime_LoadLookupSlotForCallEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL36Stats_Runtime_StoreLookupSlot_SloppyEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC29:
	.string	"V8.Runtime_Runtime_StoreLookupSlot_Sloppy"
	.section	.text._ZN2v88internalL36Stats_Runtime_StoreLookupSlot_SloppyEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL36Stats_Runtime_StoreLookupSlot_SloppyEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL36Stats_Runtime_StoreLookupSlot_SloppyEiPmPNS0_7IsolateE.isra.0:
.LFB28549:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1324
.L1287:
	movq	_ZZN2v88internalL36Stats_Runtime_StoreLookupSlot_SloppyEiPmPNS0_7IsolateEE28trace_event_unique_atomic956(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1325
.L1289:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1326
.L1291:
	movq	41088(%r12), %rbx
	movq	41096(%r12), %r14
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L1295
.L1296:
	leaq	.LC8(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1325:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1327
.L1290:
	movq	%rbx, _ZZN2v88internalL36Stats_Runtime_StoreLookupSlot_SloppyEiPmPNS0_7IsolateEE28trace_event_unique_atomic956(%rip)
	jmp	.L1289
	.p2align 4,,10
	.p2align 3
.L1295:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1296
	movq	41112(%r12), %rdi
	movq	12464(%r12), %r8
	leaq	-8(%r13), %r15
	testq	%rdi, %rdi
	je	.L1297
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1298:
	xorl	%r8d, %r8d
	movl	$3, %r9d
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_115StoreLookupSlotEPNS0_7IsolateENS0_6HandleINS0_7ContextEEENS4_INS0_6StringEEENS4_INS0_6ObjectEEENS0_12LanguageModeENS0_18ContextLookupFlagsE
	testq	%rax, %rax
	je	.L1328
	movq	(%rax), %r13
.L1301:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L1304
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1304:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1329
.L1286:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1330
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1297:
	.cfi_restore_state
	movq	%rbx, %rsi
	cmpq	41096(%r12), %rbx
	je	.L1331
.L1299:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r8, (%rsi)
	jmp	.L1298
	.p2align 4,,10
	.p2align 3
.L1326:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1332
.L1292:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1293
	movq	(%rdi), %rax
	call	*8(%rax)
.L1293:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1294
	movq	(%rdi), %rax
	call	*8(%rax)
.L1294:
	leaq	.LC29(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1291
	.p2align 4,,10
	.p2align 3
.L1328:
	movq	312(%r12), %r13
	jmp	.L1301
	.p2align 4,,10
	.p2align 3
.L1324:
	movq	40960(%rsi), %rax
	movl	$520, %edx
	leaq	-120(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1287
	.p2align 4,,10
	.p2align 3
.L1329:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1286
	.p2align 4,,10
	.p2align 3
.L1331:
	movq	%r12, %rdi
	movq	%r8, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L1299
	.p2align 4,,10
	.p2align 3
.L1332:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC29(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1292
	.p2align 4,,10
	.p2align 3
.L1327:
	leaq	.LC2(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1290
.L1330:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE28549:
	.size	_ZN2v88internalL36Stats_Runtime_StoreLookupSlot_SloppyEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL36Stats_Runtime_StoreLookupSlot_SloppyEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL36Stats_Runtime_StoreLookupSlot_StrictEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC30:
	.string	"V8.Runtime_Runtime_StoreLookupSlot_Strict"
	.section	.text._ZN2v88internalL36Stats_Runtime_StoreLookupSlot_StrictEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL36Stats_Runtime_StoreLookupSlot_StrictEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL36Stats_Runtime_StoreLookupSlot_StrictEiPmPNS0_7IsolateE.isra.0:
.LFB28550:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1371
.L1334:
	movq	_ZZN2v88internalL36Stats_Runtime_StoreLookupSlot_StrictEiPmPNS0_7IsolateEE28trace_event_unique_atomic967(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1372
.L1336:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1373
.L1338:
	movq	41088(%r12), %rbx
	movq	41096(%r12), %r14
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L1342
.L1343:
	leaq	.LC8(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1372:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1374
.L1337:
	movq	%rbx, _ZZN2v88internalL36Stats_Runtime_StoreLookupSlot_StrictEiPmPNS0_7IsolateEE28trace_event_unique_atomic967(%rip)
	jmp	.L1336
	.p2align 4,,10
	.p2align 3
.L1342:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1343
	movq	41112(%r12), %rdi
	movq	12464(%r12), %r8
	leaq	-8(%r13), %r15
	testq	%rdi, %rdi
	je	.L1344
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1345:
	movl	$3, %r9d
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	$1, %r8d
	call	_ZN2v88internal12_GLOBAL__N_115StoreLookupSlotEPNS0_7IsolateENS0_6HandleINS0_7ContextEEENS4_INS0_6StringEEENS4_INS0_6ObjectEEENS0_12LanguageModeENS0_18ContextLookupFlagsE
	testq	%rax, %rax
	je	.L1375
	movq	(%rax), %r13
.L1348:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L1351
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1351:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1376
.L1333:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1377
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1344:
	.cfi_restore_state
	movq	%rbx, %rsi
	cmpq	41096(%r12), %rbx
	je	.L1378
.L1346:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r8, (%rsi)
	jmp	.L1345
	.p2align 4,,10
	.p2align 3
.L1373:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1379
.L1339:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1340
	movq	(%rdi), %rax
	call	*8(%rax)
.L1340:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1341
	movq	(%rdi), %rax
	call	*8(%rax)
.L1341:
	leaq	.LC30(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1338
	.p2align 4,,10
	.p2align 3
.L1375:
	movq	312(%r12), %r13
	jmp	.L1348
	.p2align 4,,10
	.p2align 3
.L1371:
	movq	40960(%rsi), %rax
	movl	$522, %edx
	leaq	-120(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1334
	.p2align 4,,10
	.p2align 3
.L1376:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1333
	.p2align 4,,10
	.p2align 3
.L1378:
	movq	%r12, %rdi
	movq	%r8, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L1346
	.p2align 4,,10
	.p2align 3
.L1379:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC30(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1339
	.p2align 4,,10
	.p2align 3
.L1374:
	leaq	.LC2(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1337
.L1377:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE28550:
	.size	_ZN2v88internalL36Stats_Runtime_StoreLookupSlot_StrictEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL36Stats_Runtime_StoreLookupSlot_StrictEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL44Stats_Runtime_StoreLookupSlot_SloppyHoistingEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC31:
	.string	"V8.Runtime_Runtime_StoreLookupSlot_SloppyHoisting"
	.section	.text._ZN2v88internalL44Stats_Runtime_StoreLookupSlot_SloppyHoistingEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL44Stats_Runtime_StoreLookupSlot_SloppyHoistingEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL44Stats_Runtime_StoreLookupSlot_SloppyHoistingEiPmPNS0_7IsolateE.isra.0:
.LFB28551:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1418
.L1381:
	movq	_ZZN2v88internalL44Stats_Runtime_StoreLookupSlot_SloppyHoistingEiPmPNS0_7IsolateEE28trace_event_unique_atomic980(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1419
.L1383:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1420
.L1385:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L1389
.L1390:
	leaq	.LC8(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1419:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1421
.L1384:
	movq	%rbx, _ZZN2v88internalL44Stats_Runtime_StoreLookupSlot_SloppyHoistingEiPmPNS0_7IsolateEE28trace_event_unique_atomic980(%rip)
	jmp	.L1383
	.p2align 4,,10
	.p2align 3
.L1389:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1390
	movq	12464(%r12), %rax
	leaq	-168(%rbp), %rdi
	leaq	-8(%r13), %r15
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal7Context19declaration_contextEv@PLT
	movq	41112(%r12), %rdi
	movq	%rax, %r8
	testq	%rdi, %rdi
	je	.L1391
	movq	%rax, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1392:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_115StoreLookupSlotEPNS0_7IsolateENS0_6HandleINS0_7ContextEEENS4_INS0_6StringEEENS4_INS0_6ObjectEEENS0_12LanguageModeENS0_18ContextLookupFlagsE
	testq	%rax, %rax
	je	.L1422
	movq	(%rax), %r13
.L1395:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1398
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1398:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1423
.L1380:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1424
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1391:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L1425
.L1393:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r8, (%rsi)
	jmp	.L1392
	.p2align 4,,10
	.p2align 3
.L1420:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1426
.L1386:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1387
	movq	(%rdi), %rax
	call	*8(%rax)
.L1387:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1388
	movq	(%rdi), %rax
	call	*8(%rax)
.L1388:
	leaq	.LC31(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1385
	.p2align 4,,10
	.p2align 3
.L1422:
	movq	312(%r12), %r13
	jmp	.L1395
	.p2align 4,,10
	.p2align 3
.L1418:
	movq	40960(%rsi), %rax
	movl	$521, %edx
	leaq	-120(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1381
	.p2align 4,,10
	.p2align 3
.L1423:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1380
	.p2align 4,,10
	.p2align 3
.L1425:
	movq	%r12, %rdi
	movq	%rax, -184(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-184(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L1393
	.p2align 4,,10
	.p2align 3
.L1426:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC31(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1386
	.p2align 4,,10
	.p2align 3
.L1421:
	leaq	.LC2(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1384
.L1424:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE28551:
	.size	_ZN2v88internalL44Stats_Runtime_StoreLookupSlot_SloppyHoistingEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL44Stats_Runtime_StoreLookupSlot_SloppyHoistingEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL30Stats_Runtime_NewScriptContextEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC32:
	.string	"V8.Runtime_Runtime_NewScriptContext"
	.section	.text._ZN2v88internalL30Stats_Runtime_NewScriptContextEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL30Stats_Runtime_NewScriptContextEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL30Stats_Runtime_NewScriptContextEiPmPNS0_7IsolateE.isra.0:
.LFB28570:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1482
.L1428:
	movq	_ZZN2v88internalL30Stats_Runtime_NewScriptContextEiPmPNS0_7IsolateEE28trace_event_unique_atomic665(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1483
.L1430:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1484
.L1432:
	movq	41096(%r12), %rax
	movq	41088(%r12), %r14
	addl	$1, 41104(%r12)
	movq	%rax, -184(%rbp)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L1485
.L1436:
	leaq	.LC18(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1483:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1486
.L1431:
	movq	%rbx, _ZZN2v88internalL30Stats_Runtime_NewScriptContextEiPmPNS0_7IsolateEE28trace_event_unique_atomic665(%rip)
	jmp	.L1430
	.p2align 4,,10
	.p2align 3
.L1485:
	movq	-1(%rax), %rax
	cmpw	$136, 11(%rax)
	jne	.L1436
	movq	41112(%r12), %rdi
	movq	12464(%r12), %rsi
	testq	%rdi, %rdi
	je	.L1487
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rbx
.L1439:
	leaq	-168(%rbp), %rdi
	movq	%rsi, -168(%rbp)
	call	_ZN2v88internal7Context13global_objectEv@PLT
	movq	41112(%r12), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L1441
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L1442:
	movq	(%rbx), %rax
	movq	1135(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1444
	movq	%rdx, -192(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-192(%rbp), %rdx
	movq	%rax, %r15
.L1445:
	movq	%r15, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internalL13FindNameClashEPNS0_7IsolateENS0_6HandleINS0_9ScopeInfoEEENS3_INS0_14JSGlobalObjectEEENS3_INS0_18ScriptContextTableEEE
	movq	12480(%r12), %rcx
	cmpq	%rcx, 96(%r12)
	je	.L1447
	movq	%rax, %r13
.L1448:
	subl	$1, 41104(%r12)
	movq	-184(%rbp), %rax
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rax
	je	.L1454
	movq	%rax, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1454:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1488
.L1427:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1489
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1444:
	.cfi_restore_state
	movq	41088(%r12), %r15
	cmpq	41096(%r12), %r15
	je	.L1490
.L1446:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r15)
	jmp	.L1445
	.p2align 4,,10
	.p2align 3
.L1441:
	movq	41088(%r12), %rdx
	cmpq	41096(%r12), %rdx
	je	.L1491
.L1443:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdx)
	jmp	.L1442
	.p2align 4,,10
	.p2align 3
.L1487:
	movq	%r14, %rbx
	cmpq	41096(%r12), %r14
	je	.L1492
.L1440:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rbx)
	jmp	.L1439
	.p2align 4,,10
	.p2align 3
.L1484:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1493
.L1433:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1434
	movq	(%rdi), %rax
	call	*8(%rax)
.L1434:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1435
	movq	(%rdi), %rax
	call	*8(%rax)
.L1435:
	leaq	.LC32(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1432
	.p2align 4,,10
	.p2align 3
.L1447:
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory16NewScriptContextENS0_6HandleINS0_13NativeContextEEENS2_INS0_9ScopeInfoEEE@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	movq	%rax, %r13
	call	_ZN2v88internal18ScriptContextTable6ExtendENS0_6HandleIS1_EENS2_INS0_7ContextEEE@PLT
	movq	(%rbx), %rdi
	movq	(%rax), %rbx
	leaq	1135(%rdi), %rsi
	movq	%rbx, 1135(%rdi)
	testb	$1, %bl
	je	.L1455
	movq	%rbx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -192(%rbp)
	testl	$262144, %eax
	je	.L1450
	movq	%rbx, %rdx
	movq	%rsi, -208(%rbp)
	movq	%rdi, -200(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-192(%rbp), %rcx
	movq	-208(%rbp), %rsi
	movq	-200(%rbp), %rdi
	movq	8(%rcx), %rax
.L1450:
	testb	$24, %al
	je	.L1455
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1494
	.p2align 4,,10
	.p2align 3
.L1455:
	movq	0(%r13), %r13
	jmp	.L1448
	.p2align 4,,10
	.p2align 3
.L1482:
	movq	40960(%rsi), %rax
	movl	$512, %edx
	leaq	-120(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1428
	.p2align 4,,10
	.p2align 3
.L1488:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1427
	.p2align 4,,10
	.p2align 3
.L1492:
	movq	%r12, %rdi
	movq	%rsi, -192(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-192(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L1440
	.p2align 4,,10
	.p2align 3
.L1491:
	movq	%r12, %rdi
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-192(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L1443
	.p2align 4,,10
	.p2align 3
.L1490:
	movq	%r12, %rdi
	movq	%rsi, -200(%rbp)
	movq	%rdx, -192(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-200(%rbp), %rsi
	movq	-192(%rbp), %rdx
	movq	%rax, %r15
	jmp	.L1446
	.p2align 4,,10
	.p2align 3
.L1493:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC32(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1433
	.p2align 4,,10
	.p2align 3
.L1486:
	leaq	.LC2(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1431
	.p2align 4,,10
	.p2align 3
.L1494:
	movq	%rbx, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1455
.L1489:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE28570:
	.size	_ZN2v88internalL30Stats_Runtime_NewScriptContextEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL30Stats_Runtime_NewScriptContextEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL30Stats_Runtime_DeleteLookupSlotEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC33:
	.string	"V8.Runtime_Runtime_DeleteLookupSlot"
	.section	.text._ZN2v88internalL30Stats_Runtime_DeleteLookupSlotEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL30Stats_Runtime_DeleteLookupSlotEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL30Stats_Runtime_DeleteLookupSlotEiPmPNS0_7IsolateE.isra.0:
.LFB28571:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$144, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1543
.L1496:
	movq	_ZZN2v88internalL30Stats_Runtime_DeleteLookupSlotEiPmPNS0_7IsolateEE28trace_event_unique_atomic750(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1544
.L1498:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1545
.L1500:
	movq	41088(%r12), %rbx
	movq	41096(%r12), %r14
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L1546
.L1504:
	leaq	.LC8(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1544:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1547
.L1499:
	movq	%rbx, _ZZN2v88internalL30Stats_Runtime_DeleteLookupSlotEiPmPNS0_7IsolateEE28trace_event_unique_atomic750(%rip)
	jmp	.L1498
	.p2align 4,,10
	.p2align 3
.L1546:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1504
	movq	41112(%r12), %rdi
	movq	12464(%r12), %rsi
	testq	%rdi, %rdi
	je	.L1548
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdi
.L1507:
	leaq	-153(%rbp), %rax
	pushq	$0
	movl	$3, %edx
	leaq	-152(%rbp), %rcx
	pushq	%rax
	leaq	-154(%rbp), %r9
	leaq	-148(%rbp), %r8
	movq	%r13, %rsi
	call	_ZN2v88internal7Context6LookupENS0_6HandleIS1_EENS2_INS0_6StringEEENS0_18ContextLookupFlagsEPiPNS0_18PropertyAttributesEPNS0_18InitializationFlagEPNS0_12VariableModeEPb@PLT
	movq	%rax, %rdi
	popq	%rax
	popq	%rdx
	testq	%rdi, %rdi
	je	.L1549
	movq	(%rdi), %rax
	testb	$1, %al
	jne	.L1512
.L1516:
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZN2v88internal10JSReceiver14DeletePropertyENS0_6HandleIS1_EENS2_INS0_4NameEEENS0_12LanguageModeE@PLT
	testb	%al, %al
	je	.L1540
	shrw	$8, %ax
	je	.L1517
.L1542:
	movq	112(%r12), %r13
.L1511:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L1520
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1520:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1550
.L1495:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1551
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1512:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	subw	$138, %dx
	cmpw	$9, %dx
	jbe	.L1517
	movq	-1(%rax), %rax
	cmpw	$119, 11(%rax)
	jne	.L1516
	.p2align 4,,10
	.p2align 3
.L1517:
	movq	120(%r12), %r13
	jmp	.L1511
	.p2align 4,,10
	.p2align 3
.L1548:
	movq	%rbx, %rdi
	cmpq	41096(%r12), %rbx
	je	.L1552
.L1508:
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdi)
	jmp	.L1507
	.p2align 4,,10
	.p2align 3
.L1549:
	movq	12480(%r12), %rax
	cmpq	%rax, 96(%r12)
	je	.L1542
.L1540:
	movq	312(%r12), %r13
	jmp	.L1511
	.p2align 4,,10
	.p2align 3
.L1545:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1553
.L1501:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1502
	movq	(%rdi), %rax
	call	*8(%rax)
.L1502:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1503
	movq	(%rdi), %rax
	call	*8(%rax)
.L1503:
	leaq	.LC33(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1500
	.p2align 4,,10
	.p2align 3
.L1543:
	movq	40960(%rsi), %rax
	movl	$504, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1496
	.p2align 4,,10
	.p2align 3
.L1550:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1495
	.p2align 4,,10
	.p2align 3
.L1552:
	movq	%r12, %rdi
	movq	%rsi, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %rsi
	movq	%rax, %rdi
	jmp	.L1508
	.p2align 4,,10
	.p2align 3
.L1553:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC33(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1501
	.p2align 4,,10
	.p2align 3
.L1547:
	leaq	.LC2(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1499
.L1551:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE28571:
	.size	_ZN2v88internalL30Stats_Runtime_DeleteLookupSlotEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL30Stats_Runtime_DeleteLookupSlotEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL32Stats_Runtime_NewSloppyArgumentsEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC34:
	.string	"V8.Runtime_Runtime_NewSloppyArguments"
	.section	.rodata._ZN2v88internalL32Stats_Runtime_NewSloppyArgumentsEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC35:
	.string	"args[0].IsJSFunction()"
	.section	.text._ZN2v88internalL32Stats_Runtime_NewSloppyArgumentsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL32Stats_Runtime_NewSloppyArgumentsEiPmPNS0_7IsolateE, @function
_ZN2v88internalL32Stats_Runtime_NewSloppyArgumentsEiPmPNS0_7IsolateE:
.LFB22235:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$1576, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -1568(%rbp)
	movq	$0, -1536(%rbp)
	movaps	%xmm0, -1552(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1588
.L1555:
	movq	_ZZN2v88internalL32Stats_Runtime_NewSloppyArgumentsEiPmPNS0_7IsolateEE28trace_event_unique_atomic547(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1589
.L1557:
	movq	$0, -1600(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1590
.L1559:
	movq	41088(%r12), %rax
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	%rax, -1608(%rbp)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L1591
.L1563:
	leaq	.LC35(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1589:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1592
.L1558:
	movq	%rbx, _ZZN2v88internalL32Stats_Runtime_NewSloppyArgumentsEiPmPNS0_7IsolateEE28trace_event_unique_atomic547(%rip)
	jmp	.L1557
	.p2align 4,,10
	.p2align 3
.L1591:
	movq	-1(%rax), %rax
	cmpw	$1105, 11(%rax)
	jne	.L1563
	leaq	-1520(%rbp), %r14
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal18StackFrameIterator7AdvanceEv@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal18StackFrameIterator7AdvanceEv@PLT
	movq	-104(%rbp), %r15
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*120(%rax)
	movq	32(%r15), %rdx
	movl	%eax, %ecx
	movq	(%rdx), %rax
	cmpq	$38, -8(%rax)
	je	.L1593
.L1565:
	leal	0(,%rcx,8), %eax
	movq	%r13, %rsi
	movq	%r12, %rdi
	cltq
	leaq	16(%rdx,%rax), %rdx
	call	_ZN2v88internal12_GLOBAL__N_118NewSloppyArgumentsINS1_18ParameterArgumentsEEENS0_6HandleINS0_8JSObjectEEEPNS0_7IsolateENS4_INS0_10JSFunctionEEET_i
	movq	(%rax), %r13
	movq	-1608(%rbp), %rax
	subl	$1, 41104(%r12)
	movq	%rax, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1568
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1568:
	leaq	-1600(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-1568(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1594
.L1554:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1595
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1590:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1596
.L1560:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1561
	movq	(%rdi), %rax
	call	*8(%rax)
.L1561:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1562
	movq	(%rdi), %rax
	call	*8(%rax)
.L1562:
	leaq	.LC34(%rip), %rax
	movq	%rbx, -1592(%rbp)
	movq	%rax, -1584(%rbp)
	leaq	-1592(%rbp), %rax
	movq	%r14, -1576(%rbp)
	movq	%rax, -1600(%rbp)
	jmp	.L1559
	.p2align 4,,10
	.p2align 3
.L1593:
	movq	%r14, %rdi
	call	_ZN2v88internal18StackFrameIterator7AdvanceEv@PLT
	movq	-104(%rbp), %r15
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*120(%rax)
	movq	32(%r15), %rdx
	movl	%eax, %ecx
	jmp	.L1565
	.p2align 4,,10
	.p2align 3
.L1588:
	movq	40960(%rdx), %rax
	leaq	-1560(%rbp), %rsi
	movl	$513, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -1568(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1555
	.p2align 4,,10
	.p2align 3
.L1594:
	leaq	-1560(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1554
	.p2align 4,,10
	.p2align 3
.L1592:
	leaq	.LC2(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1558
	.p2align 4,,10
	.p2align 3
.L1596:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC34(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1560
.L1595:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22235:
	.size	_ZN2v88internalL32Stats_Runtime_NewSloppyArgumentsEiPmPNS0_7IsolateE, .-_ZN2v88internalL32Stats_Runtime_NewSloppyArgumentsEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal29Runtime_ThrowConstAssignErrorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal29Runtime_ThrowConstAssignErrorEiPmPNS0_7IsolateE
	.type	_ZN2v88internal29Runtime_ThrowConstAssignErrorEiPmPNS0_7IsolateE, @function
_ZN2v88internal29Runtime_ThrowConstAssignErrorEiPmPNS0_7IsolateE:
.LFB22183:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1601
	addl	$1, 41104(%rdx)
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movl	$37, %esi
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	41096(%r12), %rbx
	je	.L1599
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1599:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1601:
	.cfi_restore_state
	popq	%rbx
	movq	%rdx, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL35Stats_Runtime_ThrowConstAssignErrorEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE22183:
	.size	_ZN2v88internal29Runtime_ThrowConstAssignErrorEiPmPNS0_7IsolateE, .-_ZN2v88internal29Runtime_ThrowConstAssignErrorEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal22Runtime_DeclareGlobalsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal22Runtime_DeclareGlobalsEiPmPNS0_7IsolateE
	.type	_ZN2v88internal22Runtime_DeclareGlobalsEiPmPNS0_7IsolateE, @function
_ZN2v88internal22Runtime_DeclareGlobalsEiPmPNS0_7IsolateE:
.LFB22190:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1611
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L1604
.L1605:
	leaq	.LC4(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1604:
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	subl	$123, %eax
	cmpw	$14, %ax
	ja	.L1605
	movq	-8(%rsi), %rdx
	testb	$1, %dl
	jne	.L1612
	movq	-16(%rsi), %rax
	sarq	$32, %rdx
	leaq	-16(%rsi), %rcx
	testb	$1, %al
	jne	.L1613
.L1607:
	leaq	.LC6(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1613:
	movq	-1(%rax), %rax
	cmpw	$1105, 11(%rax)
	jne	.L1607
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_114DeclareGlobalsEPNS0_7IsolateENS0_6HandleINS0_10FixedArrayEEEiNS4_INS0_10JSFunctionEEE
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	41096(%r12), %rbx
	je	.L1609
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1609:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1611:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL28Stats_Runtime_DeclareGlobalsEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L1612:
	.cfi_restore_state
	leaq	.LC5(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE22190:
	.size	_ZN2v88internal22Runtime_DeclareGlobalsEiPmPNS0_7IsolateE, .-_ZN2v88internal22Runtime_DeclareGlobalsEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal27Runtime_DeclareEvalFunctionEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal27Runtime_DeclareEvalFunctionEiPmPNS0_7IsolateE
	.type	_ZN2v88internal27Runtime_DeclareEvalFunctionEiPmPNS0_7IsolateE, @function
_ZN2v88internal27Runtime_DeclareEvalFunctionEiPmPNS0_7IsolateE:
.LFB22194:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1620
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L1616
.L1617:
	leaq	.LC8(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1616:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1617
	leaq	-8(%rsi), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_117DeclareEvalHelperEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS4_INS0_6ObjectEEE
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r14
	cmpq	41096(%r12), %rbx
	je	.L1618
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1618:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1620:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL33Stats_Runtime_DeclareEvalFunctionEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE22194:
	.size	_ZN2v88internal27Runtime_DeclareEvalFunctionEiPmPNS0_7IsolateE, .-_ZN2v88internal27Runtime_DeclareEvalFunctionEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal22Runtime_DeclareEvalVarEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal22Runtime_DeclareEvalVarEiPmPNS0_7IsolateE
	.type	_ZN2v88internal22Runtime_DeclareEvalVarEiPmPNS0_7IsolateE, @function
_ZN2v88internal22Runtime_DeclareEvalVarEiPmPNS0_7IsolateE:
.LFB22197:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1627
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L1623
.L1624:
	leaq	.LC8(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1623:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1624
	leaq	88(%rdx), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_117DeclareEvalHelperEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS4_INS0_6ObjectEEE
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	41096(%r12), %rbx
	je	.L1625
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1625:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1627:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL28Stats_Runtime_DeclareEvalVarEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE22197:
	.size	_ZN2v88internal22Runtime_DeclareEvalVarEiPmPNS0_7IsolateE, .-_ZN2v88internal22Runtime_DeclareEvalVarEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal26Runtime_NewSloppyArgumentsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal26Runtime_NewSloppyArgumentsEiPmPNS0_7IsolateE
	.type	_ZN2v88internal26Runtime_NewSloppyArgumentsEiPmPNS0_7IsolateE, @function
_ZN2v88internal26Runtime_NewSloppyArgumentsEiPmPNS0_7IsolateE:
.LFB22236:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$1480, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1638
	movq	41088(%rdx), %rax
	addl	$1, 41104(%rdx)
	movq	41096(%rdx), %rbx
	movq	%rax, -1512(%rbp)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L1639
.L1631:
	leaq	.LC35(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1639:
	movq	-1(%rax), %rax
	cmpw	$1105, 11(%rax)
	jne	.L1631
	leaq	-1504(%rbp), %r15
	movq	%rdx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal18StackFrameIterator7AdvanceEv@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal18StackFrameIterator7AdvanceEv@PLT
	movq	-88(%rbp), %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*120(%rax)
	movq	32(%r14), %rdx
	movl	%eax, %ecx
	movq	(%rdx), %rax
	cmpq	$38, -8(%rax)
	je	.L1640
.L1633:
	leal	0(,%rcx,8), %eax
	movq	%r13, %rsi
	movq	%r12, %rdi
	cltq
	leaq	16(%rdx,%rax), %rdx
	call	_ZN2v88internal12_GLOBAL__N_118NewSloppyArgumentsINS1_18ParameterArgumentsEEENS0_6HandleINS0_8JSObjectEEEPNS0_7IsolateENS4_INS0_10JSFunctionEEET_i
	movq	(%rax), %r13
	movq	-1512(%rbp), %rax
	subl	$1, 41104(%r12)
	movq	%rax, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1628
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1628:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1641
	addq	$1480, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1640:
	.cfi_restore_state
	movq	%r15, %rdi
	call	_ZN2v88internal18StackFrameIterator7AdvanceEv@PLT
	movq	-88(%rbp), %r15
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*120(%rax)
	movq	32(%r15), %rdx
	movl	%eax, %ecx
	jmp	.L1633
	.p2align 4,,10
	.p2align 3
.L1638:
	call	_ZN2v88internalL32Stats_Runtime_NewSloppyArgumentsEiPmPNS0_7IsolateE
	movq	%rax, %r13
	jmp	.L1628
.L1641:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22236:
	.size	_ZN2v88internal26Runtime_NewSloppyArgumentsEiPmPNS0_7IsolateE, .-_ZN2v88internal26Runtime_NewSloppyArgumentsEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal28Runtime_NewArgumentsElementsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal28Runtime_NewArgumentsElementsEiPmPNS0_7IsolateE
	.type	_ZN2v88internal28Runtime_NewArgumentsElementsEiPmPNS0_7IsolateE, @function
_ZN2v88internal28Runtime_NewArgumentsElementsEiPmPNS0_7IsolateE:
.LFB22239:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %r13d
	testl	%r13d, %r13d
	jne	.L1673
	movq	41088(%rdx), %rax
	addl	$1, 41104(%rdx)
	movq	-8(%rsi), %r9
	movq	%rax, -56(%rbp)
	movq	41096(%rdx), %rax
	movq	%rax, -64(%rbp)
	testb	$1, %r9b
	jne	.L1674
	movq	-16(%rsi), %rdi
	sarq	$32, %r9
	testb	$1, %dil
	jne	.L1675
	sarq	$32, %rdi
	xorl	%edx, %edx
	movq	(%rsi), %r15
	movl	%r9d, %esi
	movq	%rdi, %rbx
	movq	%r14, %rdi
	movq	%r9, -72(%rbp)
	call	_ZN2v88internal7Factory26NewUninitializedFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	-72(%rbp), %r9
	movq	(%rax), %r12
	movq	%r12, %rdx
	andq	$-262144, %rdx
	movq	8(%rdx), %rdx
	testl	$262144, %edx
	jne	.L1659
	andl	$24, %edx
	movl	$4, %edx
	cmove	%edx, %r13d
.L1646:
	cmpl	%ebx, %r9d
	movl	%ebx, %edi
	cmovle	%r9d, %edi
	testl	%edi, %edi
	jle	.L1647
	leal	-1(%rdi), %edx
	leaq	24(,%rdx,8), %r10
	movl	$16, %edx
	.p2align 4,,10
	.p2align 3
.L1648:
	movq	96(%r14), %rsi
	addq	$8, %rdx
	movq	%rsi, -9(%rdx,%r12)
	movq	(%rax), %r12
	cmpq	%rdx, %r10
	jne	.L1648
.L1647:
	cmpl	%edi, %r9d
	jle	.L1649
	leal	-1(%r9), %edx
	leal	16(,%rdi,8), %ebx
	subl	%edi, %edx
	movslq	%edi, %rdi
	leaq	24(%r15,%r9,8), %r9
	movslq	%ebx, %rbx
	leaq	3(%rdx,%rdi), %r10
	salq	$3, %r10
	.p2align 4,,10
	.p2align 3
.L1657:
	movq	%r9, %rdx
	leaq	-1(%rbx,%r12), %rsi
	subq	%rbx, %rdx
	movq	(%rdx), %r15
	movq	%r15, (%rsi)
	testl	%r13d, %r13d
	je	.L1652
	movq	%r15, %rdx
	notq	%rdx
	andl	$1, %edx
	cmpl	$4, %r13d
	je	.L1676
	testb	%dl, %dl
	jne	.L1652
.L1666:
	movq	%r15, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	je	.L1652
	movq	%r12, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	je	.L1677
	.p2align 4,,10
	.p2align 3
.L1652:
	addq	$8, %rbx
	movq	(%rax), %r12
	cmpq	%rbx, %r10
	jne	.L1657
.L1649:
	movq	-56(%rbp), %rax
	subl	$1, 41104(%r14)
	movq	%rax, 41088(%r14)
	movq	-64(%rbp), %rax
	cmpq	41096(%r14), %rax
	je	.L1642
	movq	%rax, 41096(%r14)
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1642:
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1676:
	.cfi_restore_state
	testb	%dl, %dl
	jne	.L1652
	movq	%r15, %rdx
	andq	$-262144, %rdx
	testb	$4, 10(%rdx)
	je	.L1666
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%r9, -96(%rbp)
	movq	%r10, -88(%rbp)
	movq	%rax, -80(%rbp)
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rsi
	movq	-80(%rbp), %rax
	movq	-88(%rbp), %r10
	movq	-96(%rbp), %r9
	jmp	.L1666
	.p2align 4,,10
	.p2align 3
.L1677:
	movq	%r12, %rdi
	movq	%r15, %rdx
	movq	%r9, -88(%rbp)
	addq	$8, %rbx
	movq	%r10, -80(%rbp)
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rax
	movq	-80(%rbp), %r10
	movq	-88(%rbp), %r9
	movq	(%rax), %r12
	cmpq	%rbx, %r10
	jne	.L1657
	jmp	.L1649
	.p2align 4,,10
	.p2align 3
.L1659:
	movl	$4, %r13d
	jmp	.L1646
	.p2align 4,,10
	.p2align 3
.L1673:
	addq	$56, %rsp
	movq	%rdx, %rsi
	movq	%r10, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL34Stats_Runtime_NewArgumentsElementsEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L1674:
	.cfi_restore_state
	leaq	.LC5(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1675:
	leaq	.LC12(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE22239:
	.size	_ZN2v88internal28Runtime_NewArgumentsElementsEiPmPNS0_7IsolateE, .-_ZN2v88internal28Runtime_NewArgumentsElementsEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal18Runtime_NewClosureEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal18Runtime_NewClosureEiPmPNS0_7IsolateE
	.type	_ZN2v88internal18Runtime_NewClosureEiPmPNS0_7IsolateE, @function
_ZN2v88internal18Runtime_NewClosureEiPmPNS0_7IsolateE:
.LFB22242:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1690
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L1680
.L1681:
	leaq	.LC14(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1680:
	movq	-1(%rax), %rax
	cmpw	$160, 11(%rax)
	jne	.L1681
	movq	-8(%rsi), %rax
	leaq	-8(%rsi), %r15
	testb	$1, %al
	jne	.L1691
.L1682:
	leaq	.LC15(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1691:
	movq	-1(%rax), %rax
	cmpw	$154, 11(%rax)
	jne	.L1682
	movq	41112(%rdx), %rdi
	movq	12464(%rdx), %rsi
	testq	%rdi, %rdi
	je	.L1684
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L1685:
	movq	%r13, %rsi
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory33NewFunctionFromSharedFunctionInfoENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS2_INS0_12FeedbackCellEEENS0_14AllocationTypeE@PLT
	movq	(%rax), %r13
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1678
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1678:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1684:
	.cfi_restore_state
	movq	%r14, %rdx
	cmpq	41096(%r12), %r14
	je	.L1692
.L1686:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdx)
	jmp	.L1685
	.p2align 4,,10
	.p2align 3
.L1690:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL24Stats_Runtime_NewClosureEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L1692:
	.cfi_restore_state
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L1686
	.cfi_endproc
.LFE22242:
	.size	_ZN2v88internal18Runtime_NewClosureEiPmPNS0_7IsolateE, .-_ZN2v88internal18Runtime_NewClosureEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal26Runtime_NewClosure_TenuredEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal26Runtime_NewClosure_TenuredEiPmPNS0_7IsolateE
	.type	_ZN2v88internal26Runtime_NewClosure_TenuredEiPmPNS0_7IsolateE, @function
_ZN2v88internal26Runtime_NewClosure_TenuredEiPmPNS0_7IsolateE:
.LFB22245:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1705
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L1695
.L1696:
	leaq	.LC14(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1695:
	movq	-1(%rax), %rax
	cmpw	$160, 11(%rax)
	jne	.L1696
	movq	-8(%rsi), %rax
	leaq	-8(%rsi), %r15
	testb	$1, %al
	jne	.L1706
.L1697:
	leaq	.LC15(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1706:
	movq	-1(%rax), %rax
	cmpw	$154, 11(%rax)
	jne	.L1697
	movq	41112(%rdx), %rdi
	movq	12464(%rdx), %rsi
	testq	%rdi, %rdi
	je	.L1699
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L1700:
	movq	%r13, %rsi
	movl	$1, %r8d
	movq	%r15, %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory33NewFunctionFromSharedFunctionInfoENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS2_INS0_12FeedbackCellEEENS0_14AllocationTypeE@PLT
	movq	(%rax), %r13
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1693
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1693:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1699:
	.cfi_restore_state
	movq	%r14, %rdx
	cmpq	41096(%r12), %r14
	je	.L1707
.L1701:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdx)
	jmp	.L1700
	.p2align 4,,10
	.p2align 3
.L1705:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL32Stats_Runtime_NewClosure_TenuredEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L1707:
	.cfi_restore_state
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L1701
	.cfi_endproc
.LFE22245:
	.size	_ZN2v88internal26Runtime_NewClosure_TenuredEiPmPNS0_7IsolateE, .-_ZN2v88internal26Runtime_NewClosure_TenuredEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal24Runtime_NewScriptContextEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal24Runtime_NewScriptContextEiPmPNS0_7IsolateE
	.type	_ZN2v88internal24Runtime_NewScriptContextEiPmPNS0_7IsolateE, @function
_ZN2v88internal24Runtime_NewScriptContextEiPmPNS0_7IsolateE:
.LFB22249:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1739
	movq	41096(%rdx), %rax
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %rbx
	movq	%rax, -72(%rbp)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L1740
.L1711:
	leaq	.LC18(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1740:
	movq	-1(%rax), %rax
	cmpw	$136, 11(%rax)
	jne	.L1711
	movq	41112(%rdx), %rdi
	movq	12464(%rdx), %rsi
	testq	%rdi, %rdi
	je	.L1741
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r15
.L1714:
	leaq	-64(%rbp), %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal7Context13global_objectEv@PLT
	movq	41112(%r12), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L1716
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L1717:
	movq	(%r15), %rax
	movq	1135(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1719
	movq	%rdx, -80(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-80(%rbp), %rdx
	movq	%rax, %r14
.L1720:
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internalL13FindNameClashEPNS0_7IsolateENS0_6HandleINS0_9ScopeInfoEEENS3_INS0_14JSGlobalObjectEEENS3_INS0_18ScriptContextTableEEE
	movq	12480(%r12), %rcx
	cmpq	%rcx, 96(%r12)
	je	.L1722
	movq	%rax, %r13
.L1723:
	subl	$1, 41104(%r12)
	movq	-72(%rbp), %rax
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %rax
	je	.L1708
	movq	%rax, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1708:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1742
	addq	$56, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1719:
	.cfi_restore_state
	movq	41088(%r12), %r14
	cmpq	41096(%r12), %r14
	je	.L1743
.L1721:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r14)
	jmp	.L1720
	.p2align 4,,10
	.p2align 3
.L1716:
	movq	41088(%r12), %rdx
	cmpq	41096(%r12), %rdx
	je	.L1744
.L1718:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdx)
	jmp	.L1717
	.p2align 4,,10
	.p2align 3
.L1741:
	movq	%rbx, %r15
	cmpq	41096(%rdx), %rbx
	je	.L1745
.L1715:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r15)
	jmp	.L1714
	.p2align 4,,10
	.p2align 3
.L1722:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory16NewScriptContextENS0_6HandleINS0_13NativeContextEEENS2_INS0_9ScopeInfoEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	movq	%rax, %r13
	call	_ZN2v88internal18ScriptContextTable6ExtendENS0_6HandleIS1_EENS2_INS0_7ContextEEE@PLT
	movq	(%r15), %rdi
	movq	(%rax), %r15
	leaq	1135(%rdi), %rsi
	movq	%r15, 1135(%rdi)
	testb	$1, %r15b
	je	.L1729
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -80(%rbp)
	testl	$262144, %eax
	je	.L1725
	movq	%r15, %rdx
	movq	%rsi, -96(%rbp)
	movq	%rdi, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %rcx
	movq	-96(%rbp), %rsi
	movq	-88(%rbp), %rdi
	movq	8(%rcx), %rax
.L1725:
	testb	$24, %al
	je	.L1729
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1746
	.p2align 4,,10
	.p2align 3
.L1729:
	movq	0(%r13), %r13
	jmp	.L1723
	.p2align 4,,10
	.p2align 3
.L1739:
	movq	%r13, %rdi
	movq	%rdx, %rsi
	call	_ZN2v88internalL30Stats_Runtime_NewScriptContextEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r13
	jmp	.L1708
	.p2align 4,,10
	.p2align 3
.L1745:
	movq	%rdx, %rdi
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L1715
	.p2align 4,,10
	.p2align 3
.L1744:
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L1718
	.p2align 4,,10
	.p2align 3
.L1743:
	movq	%r12, %rdi
	movq	%rsi, -88(%rbp)
	movq	%rdx, -80(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movq	-80(%rbp), %rdx
	movq	%rax, %r14
	jmp	.L1721
	.p2align 4,,10
	.p2align 3
.L1746:
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1729
.L1742:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22249:
	.size	_ZN2v88internal24Runtime_NewScriptContextEiPmPNS0_7IsolateE, .-_ZN2v88internal24Runtime_NewScriptContextEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal26Runtime_NewFunctionContextEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal26Runtime_NewFunctionContextEiPmPNS0_7IsolateE
	.type	_ZN2v88internal26Runtime_NewFunctionContextEiPmPNS0_7IsolateE, @function
_ZN2v88internal26Runtime_NewFunctionContextEiPmPNS0_7IsolateE:
.LFB22252:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1757
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r14
	testb	$1, %al
	jne	.L1749
.L1750:
	leaq	.LC18(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1749:
	movq	-1(%rax), %rax
	cmpw	$136, 11(%rax)
	jne	.L1750
	movq	41112(%rdx), %rdi
	movq	12464(%rdx), %r15
	testq	%rdi, %rdi
	je	.L1751
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1752:
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory18NewFunctionContextENS0_6HandleINS0_7ContextEEENS2_INS0_9ScopeInfoEEE@PLT
	movq	(%rax), %r13
	movq	%rbx, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r14
	je	.L1747
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1747:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1751:
	.cfi_restore_state
	movq	%rbx, %rsi
	cmpq	41096(%rdx), %rbx
	je	.L1758
.L1753:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r15, (%rsi)
	jmp	.L1752
	.p2align 4,,10
	.p2align 3
.L1757:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL32Stats_Runtime_NewFunctionContextEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L1758:
	.cfi_restore_state
	movq	%rdx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L1753
	.cfi_endproc
.LFE22252:
	.size	_ZN2v88internal26Runtime_NewFunctionContextEiPmPNS0_7IsolateE, .-_ZN2v88internal26Runtime_NewFunctionContextEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal23Runtime_PushWithContextEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal23Runtime_PushWithContextEiPmPNS0_7IsolateE
	.type	_ZN2v88internal23Runtime_PushWithContextEiPmPNS0_7IsolateE, @function
_ZN2v88internal23Runtime_PushWithContextEiPmPNS0_7IsolateE:
.LFB22255:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1771
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L1761
.L1762:
	leaq	.LC20(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1761:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L1762
	movq	-8(%rsi), %rax
	leaq	-8(%rsi), %r15
	testb	$1, %al
	jne	.L1772
.L1763:
	leaq	.LC21(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1772:
	movq	-1(%rax), %rax
	cmpw	$136, 11(%rax)
	jne	.L1763
	movq	41112(%rdx), %rdi
	movq	12464(%rdx), %r8
	testq	%rdi, %rdi
	je	.L1765
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1766:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory14NewWithContextENS0_6HandleINS0_7ContextEEENS2_INS0_9ScopeInfoEEENS2_INS0_10JSReceiverEEE@PLT
	movq	(%rax), %r13
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%r13, 12464(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1759
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1759:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1765:
	.cfi_restore_state
	movq	%r14, %rsi
	cmpq	41096(%rdx), %r14
	je	.L1773
.L1767:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r8, (%rsi)
	jmp	.L1766
	.p2align 4,,10
	.p2align 3
.L1771:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL29Stats_Runtime_PushWithContextEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L1773:
	.cfi_restore_state
	movq	%rdx, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L1767
	.cfi_endproc
.LFE22255:
	.size	_ZN2v88internal23Runtime_PushWithContextEiPmPNS0_7IsolateE, .-_ZN2v88internal23Runtime_PushWithContextEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal25Runtime_PushModuleContextEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal25Runtime_PushModuleContextEiPmPNS0_7IsolateE
	.type	_ZN2v88internal25Runtime_PushModuleContextEiPmPNS0_7IsolateE, @function
_ZN2v88internal25Runtime_PushModuleContextEiPmPNS0_7IsolateE:
.LFB22258:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1786
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L1776
.L1777:
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1776:
	movq	-1(%rax), %rax
	cmpw	$119, 11(%rax)
	jne	.L1777
	movq	-8(%rsi), %rax
	leaq	-8(%rsi), %r15
	testb	$1, %al
	jne	.L1787
.L1778:
	leaq	.LC21(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1787:
	movq	-1(%rax), %rax
	cmpw	$136, 11(%rax)
	jne	.L1778
	movq	41112(%rdx), %rdi
	movq	12464(%rdx), %rsi
	testq	%rdi, %rdi
	je	.L1780
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L1781:
	movq	%r13, %rsi
	movq	%r15, %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory16NewModuleContextENS0_6HandleINS0_16SourceTextModuleEEENS2_INS0_13NativeContextEEENS2_INS0_9ScopeInfoEEE@PLT
	movq	(%rax), %r13
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%r13, 12464(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1774
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1774:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1780:
	.cfi_restore_state
	movq	%r14, %rdx
	cmpq	41096(%r12), %r14
	je	.L1788
.L1782:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdx)
	jmp	.L1781
	.p2align 4,,10
	.p2align 3
.L1786:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL31Stats_Runtime_PushModuleContextEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L1788:
	.cfi_restore_state
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L1782
	.cfi_endproc
.LFE22258:
	.size	_ZN2v88internal25Runtime_PushModuleContextEiPmPNS0_7IsolateE, .-_ZN2v88internal25Runtime_PushModuleContextEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal24Runtime_PushCatchContextEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal24Runtime_PushCatchContextEiPmPNS0_7IsolateE
	.type	_ZN2v88internal24Runtime_PushCatchContextEiPmPNS0_7IsolateE, @function
_ZN2v88internal24Runtime_PushCatchContextEiPmPNS0_7IsolateE:
.LFB22261:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1799
	addl	$1, 41104(%rdx)
	movq	-8(%rsi), %rax
	leaq	-8(%rsi), %r15
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r14
	testb	$1, %al
	jne	.L1791
.L1792:
	leaq	.LC21(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1791:
	movq	-1(%rax), %rax
	cmpw	$136, 11(%rax)
	jne	.L1792
	movq	41112(%rdx), %rdi
	movq	12464(%rdx), %r8
	testq	%rdi, %rdi
	je	.L1793
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1794:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory15NewCatchContextENS0_6HandleINS0_7ContextEEENS2_INS0_9ScopeInfoEEENS2_INS0_6ObjectEEE@PLT
	movq	(%rax), %r13
	movq	%rbx, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%r13, 12464(%r12)
	cmpq	41096(%r12), %r14
	je	.L1789
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1789:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1793:
	.cfi_restore_state
	movq	%rbx, %rsi
	cmpq	41096(%rdx), %rbx
	je	.L1800
.L1795:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r8, (%rsi)
	jmp	.L1794
	.p2align 4,,10
	.p2align 3
.L1799:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL30Stats_Runtime_PushCatchContextEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L1800:
	.cfi_restore_state
	movq	%rdx, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L1795
	.cfi_endproc
.LFE22261:
	.size	_ZN2v88internal24Runtime_PushCatchContextEiPmPNS0_7IsolateE, .-_ZN2v88internal24Runtime_PushCatchContextEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal24Runtime_PushBlockContextEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal24Runtime_PushBlockContextEiPmPNS0_7IsolateE
	.type	_ZN2v88internal24Runtime_PushBlockContextEiPmPNS0_7IsolateE, @function
_ZN2v88internal24Runtime_PushBlockContextEiPmPNS0_7IsolateE:
.LFB22264:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1811
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r14
	testb	$1, %al
	jne	.L1803
.L1804:
	leaq	.LC18(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1803:
	movq	-1(%rax), %rax
	cmpw	$136, 11(%rax)
	jne	.L1804
	movq	41112(%rdx), %rdi
	movq	12464(%rdx), %r15
	testq	%rdi, %rdi
	je	.L1805
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1806:
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory15NewBlockContextENS0_6HandleINS0_7ContextEEENS2_INS0_9ScopeInfoEEE@PLT
	movq	(%rax), %r13
	movq	%rbx, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%r13, 12464(%r12)
	cmpq	41096(%r12), %r14
	je	.L1801
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1801:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1805:
	.cfi_restore_state
	movq	%rbx, %rsi
	cmpq	41096(%rdx), %rbx
	je	.L1812
.L1807:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r15, (%rsi)
	jmp	.L1806
	.p2align 4,,10
	.p2align 3
.L1811:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL30Stats_Runtime_PushBlockContextEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L1812:
	.cfi_restore_state
	movq	%rdx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L1807
	.cfi_endproc
.LFE22264:
	.size	_ZN2v88internal24Runtime_PushBlockContextEiPmPNS0_7IsolateE, .-_ZN2v88internal24Runtime_PushBlockContextEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal24Runtime_DeleteLookupSlotEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal24Runtime_DeleteLookupSlotEiPmPNS0_7IsolateE
	.type	_ZN2v88internal24Runtime_DeleteLookupSlotEiPmPNS0_7IsolateE, @function
_ZN2v88internal24Runtime_DeleteLookupSlotEiPmPNS0_7IsolateE:
.LFB22267:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1837
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r14
	testb	$1, %al
	jne	.L1838
.L1816:
	leaq	.LC8(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1838:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1816
	movq	41112(%rdx), %rdi
	movq	12464(%rdx), %rsi
	testq	%rdi, %rdi
	je	.L1839
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdi
.L1819:
	leaq	-49(%rbp), %rax
	pushq	$0
	movl	$3, %edx
	leaq	-48(%rbp), %rcx
	pushq	%rax
	leaq	-50(%rbp), %r9
	leaq	-44(%rbp), %r8
	movq	%r13, %rsi
	call	_ZN2v88internal7Context6LookupENS0_6HandleIS1_EENS2_INS0_6StringEEENS0_18ContextLookupFlagsEPiPNS0_18PropertyAttributesEPNS0_18InitializationFlagEPNS0_12VariableModeEPb@PLT
	movq	%rax, %rdi
	popq	%rax
	popq	%rdx
	testq	%rdi, %rdi
	je	.L1840
	movq	(%rdi), %rax
	testb	$1, %al
	jne	.L1824
.L1828:
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZN2v88internal10JSReceiver14DeletePropertyENS0_6HandleIS1_EENS2_INS0_4NameEEENS0_12LanguageModeE@PLT
	testb	%al, %al
	je	.L1834
	shrw	$8, %ax
	je	.L1829
.L1836:
	movq	112(%r12), %r13
.L1823:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L1813
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1813:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1841
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1824:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	subw	$138, %dx
	cmpw	$9, %dx
	jbe	.L1829
	movq	-1(%rax), %rax
	cmpw	$119, 11(%rax)
	jne	.L1828
	.p2align 4,,10
	.p2align 3
.L1829:
	movq	120(%r12), %r13
	jmp	.L1823
	.p2align 4,,10
	.p2align 3
.L1840:
	movq	12480(%r12), %rax
	cmpq	%rax, 96(%r12)
	je	.L1836
.L1834:
	movq	312(%r12), %r13
	jmp	.L1823
	.p2align 4,,10
	.p2align 3
.L1839:
	movq	%rbx, %rdi
	cmpq	41096(%rdx), %rbx
	je	.L1842
.L1820:
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdi)
	jmp	.L1819
	.p2align 4,,10
	.p2align 3
.L1837:
	movq	%r13, %rdi
	movq	%rdx, %rsi
	call	_ZN2v88internalL30Stats_Runtime_DeleteLookupSlotEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r13
	jmp	.L1813
	.p2align 4,,10
	.p2align 3
.L1842:
	movq	%rdx, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %rdi
	jmp	.L1820
.L1841:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22267:
	.size	_ZN2v88internal24Runtime_DeleteLookupSlotEiPmPNS0_7IsolateE, .-_ZN2v88internal24Runtime_DeleteLookupSlotEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal22Runtime_LoadLookupSlotEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal22Runtime_LoadLookupSlotEiPmPNS0_7IsolateE
	.type	_ZN2v88internal22Runtime_LoadLookupSlotEiPmPNS0_7IsolateE, @function
_ZN2v88internal22Runtime_LoadLookupSlotEiPmPNS0_7IsolateE:
.LFB22271:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1852
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r13
	testb	$1, %al
	jne	.L1845
.L1846:
	leaq	.LC8(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1845:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1846
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_114LoadLookupSlotEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS0_11ShouldThrowEPNS4_INS0_6ObjectEEE
	testq	%rax, %rax
	je	.L1853
	movq	(%rax), %r14
.L1848:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L1843
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1843:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1853:
	.cfi_restore_state
	movq	312(%r12), %r14
	jmp	.L1848
	.p2align 4,,10
	.p2align 3
.L1852:
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	movq	%r8, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL28Stats_Runtime_LoadLookupSlotEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE22271:
	.size	_ZN2v88internal22Runtime_LoadLookupSlotEiPmPNS0_7IsolateE, .-_ZN2v88internal22Runtime_LoadLookupSlotEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal34Runtime_LoadLookupSlotInsideTypeofEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal34Runtime_LoadLookupSlotInsideTypeofEiPmPNS0_7IsolateE
	.type	_ZN2v88internal34Runtime_LoadLookupSlotInsideTypeofEiPmPNS0_7IsolateE, @function
_ZN2v88internal34Runtime_LoadLookupSlotInsideTypeofEiPmPNS0_7IsolateE:
.LFB22274:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1863
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r13
	testb	$1, %al
	jne	.L1856
.L1857:
	leaq	.LC8(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1856:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1857
	xorl	%ecx, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_114LoadLookupSlotEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS0_11ShouldThrowEPNS4_INS0_6ObjectEEE
	testq	%rax, %rax
	je	.L1864
	movq	(%rax), %r14
.L1859:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L1854
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1854:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1864:
	.cfi_restore_state
	movq	312(%r12), %r14
	jmp	.L1859
	.p2align 4,,10
	.p2align 3
.L1863:
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	movq	%r8, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL40Stats_Runtime_LoadLookupSlotInsideTypeofEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE22274:
	.size	_ZN2v88internal34Runtime_LoadLookupSlotInsideTypeofEiPmPNS0_7IsolateE, .-_ZN2v88internal34Runtime_LoadLookupSlotInsideTypeofEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal29Runtime_LoadLookupSlotForCallEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal29Runtime_LoadLookupSlotForCallEiPmPNS0_7IsolateE
	.type	_ZN2v88internal29Runtime_LoadLookupSlotForCallEiPmPNS0_7IsolateE, @function
_ZN2v88internal29Runtime_LoadLookupSlotForCallEiPmPNS0_7IsolateE:
.LFB22277:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1875
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %rbx
	leaq	-64(%rbp), %rcx
	movq	%r12, %rdi
	movq	41096(%rdx), %r15
	xorl	%edx, %edx
	movq	$0, -64(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_114LoadLookupSlotEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS0_11ShouldThrowEPNS4_INS0_6ObjectEEE
	testq	%rax, %rax
	je	.L1876
	movq	-64(%rbp), %rdx
	movq	(%rax), %r14
	movq	(%rdx), %r13
.L1869:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r15
	je	.L1871
	movq	%r15, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1871:
	movq	%r14, %rax
	movq	%r13, %rdx
.L1867:
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L1877
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1876:
	.cfi_restore_state
	movq	312(%r12), %r14
	xorl	%r13d, %r13d
	jmp	.L1869
	.p2align 4,,10
	.p2align 3
.L1875:
	movq	%rdx, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internalL35Stats_Runtime_LoadLookupSlotForCallEiPmPNS0_7IsolateE.isra.0
	jmp	.L1867
.L1877:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22277:
	.size	_ZN2v88internal29Runtime_LoadLookupSlotForCallEiPmPNS0_7IsolateE, .-_ZN2v88internal29Runtime_LoadLookupSlotForCallEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal30Runtime_StoreLookupSlot_SloppyEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal30Runtime_StoreLookupSlot_SloppyEiPmPNS0_7IsolateE
	.type	_ZN2v88internal30Runtime_StoreLookupSlot_SloppyEiPmPNS0_7IsolateE, @function
_ZN2v88internal30Runtime_StoreLookupSlot_SloppyEiPmPNS0_7IsolateE:
.LFB22281:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1890
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r14
	testb	$1, %al
	jne	.L1880
.L1881:
	leaq	.LC8(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1880:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1881
	movq	41112(%rdx), %rdi
	movq	12464(%rdx), %r8
	leaq	-8(%rsi), %r15
	testq	%rdi, %rdi
	je	.L1882
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1883:
	xorl	%r8d, %r8d
	movl	$3, %r9d
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_115StoreLookupSlotEPNS0_7IsolateENS0_6HandleINS0_7ContextEEENS4_INS0_6StringEEENS4_INS0_6ObjectEEENS0_12LanguageModeENS0_18ContextLookupFlagsE
	testq	%rax, %rax
	je	.L1891
	movq	(%rax), %r13
.L1886:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L1878
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1878:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1882:
	.cfi_restore_state
	movq	%rbx, %rsi
	cmpq	41096(%rdx), %rbx
	je	.L1892
.L1884:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r8, (%rsi)
	jmp	.L1883
	.p2align 4,,10
	.p2align 3
.L1891:
	movq	312(%r12), %r13
	jmp	.L1886
	.p2align 4,,10
	.p2align 3
.L1890:
	addq	$24, %rsp
	movq	%r13, %rdi
	movq	%rdx, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL36Stats_Runtime_StoreLookupSlot_SloppyEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L1892:
	.cfi_restore_state
	movq	%rdx, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L1884
	.cfi_endproc
.LFE22281:
	.size	_ZN2v88internal30Runtime_StoreLookupSlot_SloppyEiPmPNS0_7IsolateE, .-_ZN2v88internal30Runtime_StoreLookupSlot_SloppyEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal30Runtime_StoreLookupSlot_StrictEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal30Runtime_StoreLookupSlot_StrictEiPmPNS0_7IsolateE
	.type	_ZN2v88internal30Runtime_StoreLookupSlot_StrictEiPmPNS0_7IsolateE, @function
_ZN2v88internal30Runtime_StoreLookupSlot_StrictEiPmPNS0_7IsolateE:
.LFB22284:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1905
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r14
	testb	$1, %al
	jne	.L1895
.L1896:
	leaq	.LC8(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1895:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1896
	movq	41112(%rdx), %rdi
	movq	12464(%rdx), %r8
	leaq	-8(%rsi), %r15
	testq	%rdi, %rdi
	je	.L1897
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1898:
	movl	$3, %r9d
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	$1, %r8d
	call	_ZN2v88internal12_GLOBAL__N_115StoreLookupSlotEPNS0_7IsolateENS0_6HandleINS0_7ContextEEENS4_INS0_6StringEEENS4_INS0_6ObjectEEENS0_12LanguageModeENS0_18ContextLookupFlagsE
	testq	%rax, %rax
	je	.L1906
	movq	(%rax), %r13
.L1901:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L1893
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1893:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1897:
	.cfi_restore_state
	movq	%rbx, %rsi
	cmpq	41096(%rdx), %rbx
	je	.L1907
.L1899:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r8, (%rsi)
	jmp	.L1898
	.p2align 4,,10
	.p2align 3
.L1906:
	movq	312(%r12), %r13
	jmp	.L1901
	.p2align 4,,10
	.p2align 3
.L1905:
	addq	$24, %rsp
	movq	%r13, %rdi
	movq	%rdx, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL36Stats_Runtime_StoreLookupSlot_StrictEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L1907:
	.cfi_restore_state
	movq	%rdx, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L1899
	.cfi_endproc
.LFE22284:
	.size	_ZN2v88internal30Runtime_StoreLookupSlot_StrictEiPmPNS0_7IsolateE, .-_ZN2v88internal30Runtime_StoreLookupSlot_StrictEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal38Runtime_StoreLookupSlot_SloppyHoistingEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal38Runtime_StoreLookupSlot_SloppyHoistingEiPmPNS0_7IsolateE
	.type	_ZN2v88internal38Runtime_StoreLookupSlot_SloppyHoistingEiPmPNS0_7IsolateE, @function
_ZN2v88internal38Runtime_StoreLookupSlot_SloppyHoistingEiPmPNS0_7IsolateE:
.LFB22287:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1922
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L1911
.L1912:
	leaq	.LC8(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1911:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1912
	movq	12464(%rdx), %rax
	leaq	-64(%rbp), %rdi
	leaq	-8(%rsi), %r15
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal7Context19declaration_contextEv@PLT
	movq	41112(%r12), %rdi
	movq	%rax, %r8
	testq	%rdi, %rdi
	je	.L1913
	movq	%rax, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1914:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_115StoreLookupSlotEPNS0_7IsolateENS0_6HandleINS0_7ContextEEENS4_INS0_6StringEEENS4_INS0_6ObjectEEENS0_12LanguageModeENS0_18ContextLookupFlagsE
	testq	%rax, %rax
	je	.L1923
	movq	(%rax), %r13
.L1917:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1908
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1908:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1924
	addq	$40, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1913:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L1925
.L1915:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r8, (%rsi)
	jmp	.L1914
	.p2align 4,,10
	.p2align 3
.L1923:
	movq	312(%r12), %r13
	jmp	.L1917
	.p2align 4,,10
	.p2align 3
.L1922:
	movq	%r13, %rdi
	movq	%rdx, %rsi
	call	_ZN2v88internalL44Stats_Runtime_StoreLookupSlot_SloppyHoistingEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r13
	jmp	.L1908
	.p2align 4,,10
	.p2align 3
.L1925:
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L1915
.L1924:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22287:
	.size	_ZN2v88internal38Runtime_StoreLookupSlot_SloppyHoistingEiPmPNS0_7IsolateE, .-_ZN2v88internal38Runtime_StoreLookupSlot_SloppyHoistingEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m.str1.1,"aMS",@progbits,1
.LC36:
	.string	"NewArray"
	.section	.text._ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m,"axG",@progbits,_ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m,comdat
	.p2align 4
	.weak	_ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m
	.type	_ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m, @function
_ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m:
.LFB24747:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZSt7nothrow(%rip), %rsi
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdi
	movq	$-1, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	0(,%rdi,8), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmova	%rax, %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	je	.L1928
	movq	%rbx, %rsi
	subq	$1, %rsi
	js	.L1926
	leaq	-2(%rbx), %rdx
	movl	$1, %edi
	cmpq	$-1, %rdx
	cmovge	%rbx, %rdi
	cmpq	$1, %rbx
	je	.L1941
	cmpq	$-1, %rdx
	jl	.L1941
	movq	%rdi, %rsi
	xorl	%edx, %edx
	pxor	%xmm0, %xmm0
	shrq	%rsi
	.p2align 4,,10
	.p2align 3
.L1932:
	movq	%rdx, %rcx
	addq	$1, %rdx
	salq	$4, %rcx
	movups	%xmm0, (%rax,%rcx)
	cmpq	%rdx, %rsi
	jne	.L1932
	movq	%rdi, %rcx
	andq	$-2, %rcx
	leaq	(%rax,%rcx,8), %rdx
	cmpq	%rcx, %rdi
	je	.L1926
.L1930:
	movq	$0, (%rdx)
.L1926:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1941:
	.cfi_restore_state
	movq	%rax, %rdx
	jmp	.L1930
.L1928:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%r12, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	je	.L1945
	movq	%rbx, %rdi
	subq	$1, %rdi
	js	.L1926
	leaq	-2(%rbx), %rcx
	movl	$1, %edx
	addq	$1, %rcx
	cmovge	%rbx, %rdx
	jl	.L1942
	subq	$1, %rbx
	je	.L1942
	movq	%rdx, %rsi
	xorl	%ecx, %ecx
	pxor	%xmm0, %xmm0
	shrq	%rsi
.L1936:
	movq	%rcx, %rdi
	addq	$1, %rcx
	salq	$4, %rdi
	movups	%xmm0, (%rax,%rdi)
	cmpq	%rcx, %rsi
	jne	.L1936
	movq	%rdx, %rsi
	andq	$-2, %rsi
	leaq	(%rax,%rsi,8), %rcx
	cmpq	%rsi, %rdx
	je	.L1926
.L1934:
	movq	$0, (%rcx)
	jmp	.L1926
.L1945:
	leaq	.LC36(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
.L1942:
	movq	%rax, %rcx
	jmp	.L1934
	.cfi_endproc
.LFE24747:
	.size	_ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m, .-_ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m
	.section	.text._ZN2v88internal12_GLOBAL__N_118GetCallerArgumentsEPNS0_7IsolateEPi.constprop.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_118GetCallerArgumentsEPNS0_7IsolateEPi.constprop.0, @function
_ZN2v88internal12_GLOBAL__N_118GetCallerArgumentsEPNS0_7IsolateEPi.constprop.0:
.LFB28627:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-1504(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$1736, %rsp
	movq	%rdi, -1768(%rbp)
	movq	%r15, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateE@PLT
	movq	-88(%rbp), %r12
	testq	%r12, %r12
	je	.L1947
	movq	%r15, %rdi
	call	_ZN2v88internal23JavaScriptFrameIterator7AdvanceEv@PLT
	movq	-88(%rbp), %r12
.L1947:
	pxor	%xmm0, %xmm0
	leaq	-1744(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -1728(%rbp)
	movaps	%xmm0, -1744(%rbp)
	movq	(%r12), %rax
	call	*160(%rax)
	movq	-1736(%rbp), %rax
	subq	-1744(%rbp), %rax
	movq	%rax, %rdx
	sarq	$3, %rdx
	movq	%rdx, -1776(%rbp)
	cmpq	$15, %rax
	ja	.L2000
	movq	-88(%rbp), %r14
	movq	32(%r14), %rax
	movq	(%rax), %rax
	cmpq	$38, -8(%rax)
	je	.L2001
.L1965:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*120(%rax)
	movslq	%eax, %rdi
	movl	%edi, (%rbx)
	xorl	%ebx, %ebx
	movl	%edi, -1776(%rbp)
	call	_ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m
	movl	-1776(%rbp), %ecx
	movq	%rax, %r12
	leal	-1(%rcx), %r15d
	testl	%ecx, %ecx
	jg	.L1971
	jmp	.L1970
	.p2align 4,,10
	.p2align 3
.L2002:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, (%r12,%rbx,8)
	leaq	1(%rbx), %rax
	cmpq	%r15, %rbx
	je	.L1970
.L2004:
	movq	%rax, %rbx
.L1971:
	movq	(%r14), %rax
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	*112(%rax)
	movq	41112(%r13), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	jne	.L2002
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L2003
.L1969:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%r13)
	movq	%rsi, (%rax)
	movq	%rax, (%r12,%rbx,8)
	leaq	1(%rbx), %rax
	cmpq	%r15, %rbx
	jne	.L2004
.L1970:
	movq	-1768(%rbp), %rax
	movq	%r12, (%rax)
.L1964:
	movq	-1744(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1946
	call	_ZdlPv@PLT
.L1946:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2005
	movq	-1768(%rbp), %rax
	addq	$1736, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2000:
	.cfi_restore_state
	leaq	-1664(%rbp), %r14
	movq	%r12, %rsi
	leaq	-1712(%rbp), %r15
	movq	%r14, %rdi
	call	_ZN2v88internal15TranslatedStateC1EPKNS0_15JavaScriptFrameE@PLT
	movq	32(%r12), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal15TranslatedState7PrepareEm@PLT
	movq	-1776(%rbp), %rdx
	leaq	-1748(%rbp), %r8
	movq	%r14, %rdi
	movl	$0, -1748(%rbp)
	leal	-1(%rdx), %esi
	movq	%r8, %rdx
	call	_ZN2v88internal15TranslatedState32GetArgumentsInfoFromJSFrameIndexEiPi@PLT
	movq	%r15, %rdi
	movl	$1, -1680(%rbp)
	movdqu	72(%rax), %xmm0
	movdqu	56(%rax), %xmm1
	movaps	%xmm0, -1696(%rbp)
	movaps	%xmm1, -1712(%rbp)
	call	_ZN2v88internal15TranslatedFrame15AdvanceIteratorEPSt15_Deque_iteratorINS0_15TranslatedValueERS3_PS3_E@PLT
	movq	%r15, %rdi
	addl	$1, -1680(%rbp)
	call	_ZN2v88internal15TranslatedFrame15AdvanceIteratorEPSt15_Deque_iteratorINS0_15TranslatedValueERS3_PS3_E@PLT
	movl	-1748(%rbp), %eax
	leal	-1(%rax), %edi
	movl	%edi, (%rbx)
	xorl	%ebx, %ebx
	movl	%edi, -1748(%rbp)
	movslq	%edi, %rdi
	call	_ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m
	xorl	%edx, %edx
	movq	%rax, %r13
	movl	-1748(%rbp), %eax
	testl	%eax, %eax
	jle	.L1950
.L1949:
	movq	-1712(%rbp), %rdi
	testb	%dl, %dl
	jne	.L1951
	call	_ZNK2v88internal15TranslatedValue20IsMaterializedObjectEv@PLT
	movq	-1712(%rbp), %rdi
	movb	%al, -1776(%rbp)
	call	_ZN2v88internal15TranslatedValue8GetValueEv@PLT
	movq	%r15, %rdi
	movq	%rax, 0(%r13,%rbx,8)
	addq	$1, %rbx
	addl	$1, -1680(%rbp)
	call	_ZN2v88internal15TranslatedFrame15AdvanceIteratorEPSt15_Deque_iteratorINS0_15TranslatedValueERS3_PS3_E@PLT
	cmpl	%ebx, -1748(%rbp)
	movzbl	-1776(%rbp), %edx
	jg	.L1949
	testb	%dl, %dl
	jne	.L1953
.L1950:
	movq	-1768(%rbp), %rax
	movq	-1616(%rbp), %rdi
	movq	%r13, (%rax)
	testq	%rdi, %rdi
	je	.L1955
	movq	-1544(%rbp), %rax
	movq	-1576(%rbp), %rbx
	leaq	8(%rax), %r12
	cmpq	%rbx, %r12
	jbe	.L1956
	.p2align 4,,10
	.p2align 3
.L1957:
	movq	(%rbx), %rdi
	addq	$8, %rbx
	call	_ZdlPv@PLT
	cmpq	%rbx, %r12
	ja	.L1957
	movq	-1616(%rbp), %rdi
.L1956:
	call	_ZdlPv@PLT
.L1955:
	movq	-1656(%rbp), %r15
	movq	-1664(%rbp), %r12
	cmpq	%r12, %r15
	je	.L1958
	.p2align 4,,10
	.p2align 3
.L1962:
	movq	40(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1959
	movq	112(%r12), %rax
	movq	80(%r12), %rbx
	leaq	8(%rax), %r13
	cmpq	%rbx, %r13
	jbe	.L1960
	.p2align 4,,10
	.p2align 3
.L1961:
	movq	(%rbx), %rdi
	addq	$8, %rbx
	call	_ZdlPv@PLT
	cmpq	%rbx, %r13
	ja	.L1961
	movq	40(%r12), %rdi
.L1960:
	call	_ZdlPv@PLT
.L1959:
	addq	$120, %r12
	cmpq	%r12, %r15
	jne	.L1962
	movq	-1664(%rbp), %r12
.L1958:
	testq	%r12, %r12
	je	.L1964
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	jmp	.L1964
	.p2align 4,,10
	.p2align 3
.L2006:
	movq	-1712(%rbp), %rdi
.L1951:
	call	_ZN2v88internal15TranslatedValue8GetValueEv@PLT
	movq	%r15, %rdi
	movq	%rax, 0(%r13,%rbx,8)
	addq	$1, %rbx
	addl	$1, -1680(%rbp)
	call	_ZN2v88internal15TranslatedFrame15AdvanceIteratorEPSt15_Deque_iteratorINS0_15TranslatedValueERS3_PS3_E@PLT
	cmpl	%ebx, -1748(%rbp)
	jg	.L2006
.L1953:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal15TranslatedState31StoreMaterializedValuesAndDeoptEPNS0_15JavaScriptFrameE@PLT
	jmp	.L1950
	.p2align 4,,10
	.p2align 3
.L2003:
	movq	%r13, %rdi
	movq	%rsi, -1776(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-1776(%rbp), %rsi
	jmp	.L1969
	.p2align 4,,10
	.p2align 3
.L2001:
	movq	%r15, %rdi
	call	_ZN2v88internal18StackFrameIterator7AdvanceEv@PLT
	movq	-88(%rbp), %r14
	jmp	.L1965
.L2005:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE28627:
	.size	_ZN2v88internal12_GLOBAL__N_118GetCallerArgumentsEPNS0_7IsolateEPi.constprop.0, .-_ZN2v88internal12_GLOBAL__N_118GetCallerArgumentsEPNS0_7IsolateEPi.constprop.0
	.section	.rodata._ZN2v88internalL30Stats_Runtime_NewRestParameterEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC37:
	.string	"V8.Runtime_Runtime_NewRestParameter"
	.section	.text._ZN2v88internalL30Stats_Runtime_NewRestParameterEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL30Stats_Runtime_NewRestParameterEiPmPNS0_7IsolateE, @function
_ZN2v88internalL30Stats_Runtime_NewRestParameterEiPmPNS0_7IsolateE:
.LFB22232:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2065
.L2008:
	movq	_ZZN2v88internalL30Stats_Runtime_NewRestParameterEiPmPNS0_7IsolateEE28trace_event_unique_atomic521(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2066
.L2010:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2067
.L2012:
	movq	41088(%r14), %rax
	addl	$1, 41104(%r14)
	movq	%rax, -200(%rbp)
	movq	41096(%r14), %rax
	movq	%rax, -184(%rbp)
	movq	(%r12), %rax
	testb	$1, %al
	jne	.L2068
.L2016:
	leaq	.LC35(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2066:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2069
.L2011:
	movq	%rbx, _ZZN2v88internalL30Stats_Runtime_NewRestParameterEiPmPNS0_7IsolateEE28trace_event_unique_atomic521(%rip)
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2068:
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L2016
	movq	23(%rax), %rax
	leaq	-168(%rbp), %rdi
	leaq	-172(%rbp), %rdx
	movq	%r14, %rsi
	movl	$4, %r13d
	movzwl	41(%rax), %r12d
	movl	$0, -172(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_118GetCallerArgumentsEPNS0_7IsolateEPi.constprop.0
	movl	-172(%rbp), %ebx
	movl	$0, %ecx
	movq	%r14, %rdi
	movzwl	%r12w, %eax
	movl	$2, %esi
	subl	%eax, %ebx
	cmovns	%ebx, %ecx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movl	%ecx, %edx
	movl	%ecx, -192(%rbp)
	call	_ZN2v88internal7Factory10NewJSArrayENS0_12ElementsKindEiiNS0_26ArrayStorageAllocationModeENS0_14AllocationTypeE@PLT
	movl	-192(%rbp), %ecx
	movq	(%rax), %r15
	movq	15(%r15), %rdi
	movq	%rdi, %rdx
	andq	$-262144, %rdx
	leaq	8(%rdx), %r8
	movq	8(%rdx), %rdx
	testl	$262144, %edx
	jne	.L2017
	xorl	%r13d, %r13d
	andl	$24, %edx
	sete	%r13b
	sall	$2, %r13d
.L2017:
	testl	%ebx, %ebx
	jle	.L2019
	movzwl	%r12w, %r9d
	movq	%rax, -192(%rbp)
	movl	%r13d, %r15d
	xorl	%r12d, %r12d
	salq	$3, %r9
	movq	%r14, -208(%rbp)
	leaq	15(%rdi), %rbx
	movq	%r8, %r13
	subq	%rdi, %r9
	movl	%ecx, %r14d
	movq	%r9, %rax
	.p2align 4,,10
	.p2align 3
.L2025:
	movq	-168(%rbp), %rdx
	addq	%rax, %rdx
	movq	-15(%rbx,%rdx), %rdx
	movq	(%rdx), %rdx
	movq	%rdx, (%rbx)
	testl	%r15d, %r15d
	je	.L2020
	movq	%rdx, %rsi
	notq	%rsi
	andl	$1, %esi
	cmpl	$4, %r15d
	je	.L2070
	testb	%sil, %sil
	jne	.L2020
.L2052:
	movq	%rdx, %rsi
	andq	$-262144, %rsi
	testb	$24, 8(%rsi)
	je	.L2020
	testb	$24, 0(%r13)
	jne	.L2020
	movq	%rbx, %rsi
	movq	%rax, -224(%rbp)
	movq	%rdi, -216(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-224(%rbp), %rax
	movq	-216(%rbp), %rdi
	.p2align 4,,10
	.p2align 3
.L2020:
	addl	$1, %r12d
	addq	$8, %rbx
	cmpl	%r14d, %r12d
	jl	.L2025
	movq	-192(%rbp), %rax
	movq	-208(%rbp), %r14
	movq	(%rax), %r15
.L2019:
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2026
	call	_ZdaPv@PLT
.L2026:
	movq	-200(%rbp), %rax
	subl	$1, 41104(%r14)
	movq	%rax, 41088(%r14)
	movq	-184(%rbp), %rax
	cmpq	41096(%r14), %rax
	je	.L2029
	movq	%rax, 41096(%r14)
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2029:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2071
.L2007:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2072
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2070:
	.cfi_restore_state
	testb	%sil, %sil
	jne	.L2020
	movq	%rdx, %rsi
	andq	$-262144, %rsi
	testb	$4, 10(%rsi)
	je	.L2052
	movq	%rbx, %rsi
	movq	%rax, -232(%rbp)
	movq	%rdx, -224(%rbp)
	movq	%rdi, -216(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-216(%rbp), %rdi
	movq	-224(%rbp), %rdx
	movq	-232(%rbp), %rax
	jmp	.L2052
	.p2align 4,,10
	.p2align 3
.L2067:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2073
.L2013:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2014
	movq	(%rdi), %rax
	call	*8(%rax)
.L2014:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2015
	movq	(%rdi), %rax
	call	*8(%rax)
.L2015:
	leaq	.LC37(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r13, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L2012
	.p2align 4,,10
	.p2align 3
.L2065:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$511, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2008
	.p2align 4,,10
	.p2align 3
.L2071:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2007
.L2069:
	leaq	.LC2(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2011
.L2073:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC37(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L2013
.L2072:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22232:
	.size	_ZN2v88internalL30Stats_Runtime_NewRestParameterEiPmPNS0_7IsolateE, .-_ZN2v88internalL30Stats_Runtime_NewRestParameterEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal24Runtime_NewRestParameterEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal24Runtime_NewRestParameterEiPmPNS0_7IsolateE
	.type	_ZN2v88internal24Runtime_NewRestParameterEiPmPNS0_7IsolateE, @function
_ZN2v88internal24Runtime_NewRestParameterEiPmPNS0_7IsolateE:
.LFB22233:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %r13d
	testl	%r13d, %r13d
	jne	.L2108
	movq	41088(%rdx), %rax
	addl	$1, 41104(%rdx)
	movq	%rax, -96(%rbp)
	movq	41096(%rdx), %rax
	movq	%rax, -88(%rbp)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L2109
.L2077:
	leaq	.LC35(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2109:
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L2077
	movq	23(%rax), %rax
	leaq	-64(%rbp), %rdi
	leaq	-68(%rbp), %rdx
	movq	%r15, %rsi
	movl	%r13d, %r14d
	movzwl	41(%rax), %r12d
	movl	$0, -68(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_118GetCallerArgumentsEPNS0_7IsolateEPi.constprop.0
	movl	-68(%rbp), %ebx
	movq	%r15, %rdi
	movl	$2, %esi
	movzwl	%r12w, %eax
	subl	%eax, %ebx
	cmovns	%ebx, %r14d
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	%r14d, %edx
	movl	%r14d, %ecx
	call	_ZN2v88internal7Factory10NewJSArrayENS0_12ElementsKindEiiNS0_26ArrayStorageAllocationModeENS0_14AllocationTypeE@PLT
	movq	(%rax), %r9
	movq	15(%r9), %rdi
	movq	%rdi, %rdx
	andq	$-262144, %rdx
	leaq	8(%rdx), %r8
	movq	8(%rdx), %rdx
	testl	$262144, %edx
	jne	.L2110
	andl	$24, %edx
	movl	$4, %edx
	cmove	%edx, %r13d
.L2078:
	testl	%ebx, %ebx
	jle	.L2080
	movzwl	%r12w, %r9d
	movq	%rax, -104(%rbp)
	movl	%r14d, %eax
	leaq	15(%rdi), %rbx
	salq	$3, %r9
	movq	%r15, -112(%rbp)
	movl	%r13d, %r14d
	xorl	%r12d, %r12d
	subq	%rdi, %r9
	movq	%r8, %r15
	movl	%eax, %r13d
	.p2align 4,,10
	.p2align 3
.L2086:
	movq	-64(%rbp), %rdx
	addq	%rbx, %rdx
	movq	-15(%r9,%rdx), %rdx
	movq	(%rdx), %rdx
	movq	%rdx, (%rbx)
	testl	%r14d, %r14d
	je	.L2081
	movq	%rdx, %rcx
	notq	%rcx
	andl	$1, %ecx
	cmpl	$4, %r14d
	je	.L2111
	testb	%cl, %cl
	jne	.L2081
.L2100:
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	testb	$24, 8(%rcx)
	je	.L2081
	testb	$24, (%r15)
	jne	.L2081
	movq	%rbx, %rsi
	movq	%r9, -128(%rbp)
	movq	%rdi, -120(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-128(%rbp), %r9
	movq	-120(%rbp), %rdi
	.p2align 4,,10
	.p2align 3
.L2081:
	addl	$1, %r12d
	addq	$8, %rbx
	cmpl	%r13d, %r12d
	jl	.L2086
	movq	-104(%rbp), %rax
	movq	-112(%rbp), %r15
	movq	(%rax), %r9
.L2080:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2087
	movq	%r9, -104(%rbp)
	call	_ZdaPv@PLT
	movq	-104(%rbp), %r9
.L2087:
	movq	-96(%rbp), %rax
	subl	$1, 41104(%r15)
	movq	%rax, 41088(%r15)
	movq	-88(%rbp), %rax
	cmpq	41096(%r15), %rax
	je	.L2074
	movq	%rax, 41096(%r15)
	movq	%r15, %rdi
	movq	%r9, -96(%rbp)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %r9
.L2074:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2112
	addq	$104, %rsp
	movq	%r9, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2111:
	.cfi_restore_state
	testb	%cl, %cl
	jne	.L2081
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	testb	$4, 10(%rcx)
	je	.L2100
	movq	%rbx, %rsi
	movq	%r9, -136(%rbp)
	movq	%rdx, -128(%rbp)
	movq	%rdi, -120(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-120(%rbp), %rdi
	movq	-128(%rbp), %rdx
	movq	-136(%rbp), %r9
	jmp	.L2100
	.p2align 4,,10
	.p2align 3
.L2110:
	movl	$4, %r13d
	jmp	.L2078
	.p2align 4,,10
	.p2align 3
.L2108:
	call	_ZN2v88internalL30Stats_Runtime_NewRestParameterEiPmPNS0_7IsolateE
	movq	%rax, %r9
	jmp	.L2074
.L2112:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22233:
	.size	_ZN2v88internal24Runtime_NewRestParameterEiPmPNS0_7IsolateE, .-_ZN2v88internal24Runtime_NewRestParameterEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL32Stats_Runtime_NewStrictArgumentsEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC38:
	.string	"V8.Runtime_Runtime_NewStrictArguments"
	.section	.text._ZN2v88internalL32Stats_Runtime_NewStrictArgumentsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL32Stats_Runtime_NewStrictArgumentsEiPmPNS0_7IsolateE, @function
_ZN2v88internalL32Stats_Runtime_NewStrictArgumentsEiPmPNS0_7IsolateE:
.LFB22229:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2181
.L2114:
	movq	_ZZN2v88internalL32Stats_Runtime_NewStrictArgumentsEiPmPNS0_7IsolateEE28trace_event_unique_atomic496(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2182
.L2116:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2183
.L2118:
	movq	41088(%r12), %r15
	movq	41096(%r12), %r14
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L2184
.L2122:
	leaq	.LC35(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2182:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2185
.L2117:
	movq	%rbx, _ZZN2v88internalL32Stats_Runtime_NewStrictArgumentsEiPmPNS0_7IsolateEE28trace_event_unique_atomic496(%rip)
	jmp	.L2116
	.p2align 4,,10
	.p2align 3
.L2184:
	movq	-1(%rax), %rax
	cmpw	$1105, 11(%rax)
	jne	.L2122
	leaq	-168(%rbp), %rdi
	movq	%r12, %rsi
	leaq	-172(%rbp), %rdx
	movl	$0, -172(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_118GetCallerArgumentsEPNS0_7IsolateEPi.constprop.0
	movl	-172(%rbp), %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory18NewArgumentsObjectENS0_6HandleINS0_10JSFunctionEEEi@PLT
	movl	-172(%rbp), %esi
	movq	%rax, %r9
	testl	%esi, %esi
	jne	.L2186
.L2124:
	movq	-168(%rbp), %rdi
	movq	(%r9), %r13
	testq	%rdi, %rdi
	je	.L2136
	call	_ZdaPv@PLT
.L2136:
	subl	$1, 41104(%r12)
	movq	%r15, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L2139
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2139:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2187
.L2113:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2188
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2183:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2189
.L2119:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2120
	movq	(%rdi), %rax
	call	*8(%rax)
.L2120:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2121
	movq	(%rdi), %rax
	call	*8(%rax)
.L2121:
	leaq	.LC38(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L2118
	.p2align 4,,10
	.p2align 3
.L2186:
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, -184(%rbp)
	call	_ZN2v88internal7Factory26NewUninitializedFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	-184(%rbp), %r9
	movl	$4, %ecx
	movq	(%rax), %r8
	movq	%r8, %rdx
	andq	$-262144, %rdx
	movq	8(%rdx), %rdx
	testl	$262144, %edx
	jne	.L2125
	xorl	%ecx, %ecx
	andl	$24, %edx
	sete	%cl
	sall	$2, %ecx
.L2125:
	movl	-172(%rbp), %edx
	testl	%edx, %edx
	jle	.L2126
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L2132:
	movq	-168(%rbp), %rdx
	leaq	15(%r8,%rbx,8), %rsi
	movq	(%rdx,%rbx,8), %rdx
	movq	(%rdx), %r13
	movq	%r13, (%rsi)
	testl	%ecx, %ecx
	je	.L2127
	movq	%r13, %rdx
	notq	%rdx
	andl	$1, %edx
	cmpl	$4, %ecx
	je	.L2190
	testb	%dl, %dl
	jne	.L2127
.L2167:
	movq	%r13, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	je	.L2127
	movq	%r8, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	je	.L2191
	.p2align 4,,10
	.p2align 3
.L2127:
	movq	(%rax), %r8
	addq	$1, %rbx
	cmpl	%ebx, -172(%rbp)
	jg	.L2132
.L2126:
	movq	(%r9), %r13
	movq	%r8, 15(%r13)
	leaq	15(%r13), %rsi
	testb	$1, %r8b
	je	.L2124
	movq	%r8, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L2134
	movq	%r8, %rdx
	movq	%r13, %rdi
	movq	%r9, -200(%rbp)
	movq	%r8, -192(%rbp)
	movq	%rsi, -184(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-200(%rbp), %r9
	movq	-192(%rbp), %r8
	movq	-184(%rbp), %rsi
.L2134:
	testb	$24, %al
	je	.L2124
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L2124
	movq	%r8, %rdx
	movq	%r13, %rdi
	movq	%r9, -184(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-184(%rbp), %r9
	jmp	.L2124
	.p2align 4,,10
	.p2align 3
.L2190:
	testb	%dl, %dl
	jne	.L2127
	movq	%r13, %rdx
	andq	$-262144, %rdx
	testb	$4, 10(%rdx)
	je	.L2167
	movq	%r8, %rdi
	movq	%r13, %rdx
	movl	%ecx, -212(%rbp)
	movq	%rax, -208(%rbp)
	movq	%r9, -200(%rbp)
	movq	%rsi, -192(%rbp)
	movq	%r8, -184(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-184(%rbp), %r8
	movq	-192(%rbp), %rsi
	movq	-200(%rbp), %r9
	movq	-208(%rbp), %rax
	movl	-212(%rbp), %ecx
	jmp	.L2167
	.p2align 4,,10
	.p2align 3
.L2191:
	movq	%r8, %rdi
	movq	%r13, %rdx
	movl	%ecx, -200(%rbp)
	addq	$1, %rbx
	movq	%rax, -192(%rbp)
	movq	%r9, -184(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-192(%rbp), %rax
	movl	-200(%rbp), %ecx
	movq	-184(%rbp), %r9
	movq	(%rax), %r8
	cmpl	%ebx, -172(%rbp)
	jg	.L2132
	jmp	.L2126
	.p2align 4,,10
	.p2align 3
.L2181:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$515, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2114
	.p2align 4,,10
	.p2align 3
.L2187:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2113
	.p2align 4,,10
	.p2align 3
.L2185:
	leaq	.LC2(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2117
	.p2align 4,,10
	.p2align 3
.L2189:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC38(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L2119
.L2188:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22229:
	.size	_ZN2v88internalL32Stats_Runtime_NewStrictArgumentsEiPmPNS0_7IsolateE, .-_ZN2v88internalL32Stats_Runtime_NewStrictArgumentsEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal26Runtime_NewStrictArgumentsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal26Runtime_NewStrictArgumentsEiPmPNS0_7IsolateE
	.type	_ZN2v88internal26Runtime_NewStrictArgumentsEiPmPNS0_7IsolateE, @function
_ZN2v88internal26Runtime_NewStrictArgumentsEiPmPNS0_7IsolateE:
.LFB22230:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %r15d
	testl	%r15d, %r15d
	jne	.L2236
	movq	41088(%rdx), %rax
	addl	$1, 41104(%rdx)
	movq	41096(%rdx), %r14
	movq	%rax, -88(%rbp)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L2237
.L2195:
	leaq	.LC35(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2237:
	movq	-1(%rax), %rax
	cmpw	$1105, 11(%rax)
	jne	.L2195
	leaq	-64(%rbp), %rdi
	leaq	-68(%rbp), %rdx
	movq	%r12, %rsi
	movl	$0, -68(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_118GetCallerArgumentsEPNS0_7IsolateEPi.constprop.0
	movl	-68(%rbp), %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory18NewArgumentsObjectENS0_6HandleINS0_10JSFunctionEEEi@PLT
	movl	-68(%rbp), %esi
	movq	%rax, %r9
	testl	%esi, %esi
	jne	.L2238
.L2197:
	movq	-64(%rbp), %rdi
	movq	(%r9), %r13
	testq	%rdi, %rdi
	je	.L2209
	call	_ZdaPv@PLT
.L2209:
	subl	$1, 41104(%r12)
	movq	-88(%rbp), %rax
	movq	%rax, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L2192
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2192:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2239
	addq	$88, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2238:
	.cfi_restore_state
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal7Factory26NewUninitializedFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	-96(%rbp), %r9
	movq	(%rax), %r8
	movq	%r8, %rdx
	andq	$-262144, %rdx
	movq	8(%rdx), %rdx
	testl	$262144, %edx
	jne	.L2214
	andl	$24, %edx
	movl	$4, %edx
	cmove	%edx, %r15d
.L2198:
	movl	-68(%rbp), %edx
	testl	%edx, %edx
	jle	.L2199
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L2205:
	movq	-64(%rbp), %rdx
	leaq	15(%r8,%rbx,8), %rsi
	movq	(%rdx,%rbx,8), %rdx
	movq	(%rdx), %r13
	movq	%r13, (%rsi)
	testl	%r15d, %r15d
	je	.L2200
	movq	%r13, %rdx
	notq	%rdx
	andl	$1, %edx
	cmpl	$4, %r15d
	je	.L2240
	testb	%dl, %dl
	jne	.L2200
.L2227:
	movq	%r13, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	je	.L2200
	movq	%r8, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	je	.L2241
	.p2align 4,,10
	.p2align 3
.L2200:
	movq	(%rax), %r8
	addq	$1, %rbx
	cmpl	%ebx, -68(%rbp)
	jg	.L2205
.L2199:
	movq	(%r9), %r13
	movq	%r8, 15(%r13)
	leaq	15(%r13), %rsi
	testb	$1, %r8b
	je	.L2197
	movq	%r8, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L2207
	movq	%r8, %rdx
	movq	%r13, %rdi
	movq	%r9, -112(%rbp)
	movq	%r8, -104(%rbp)
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-112(%rbp), %r9
	movq	-104(%rbp), %r8
	movq	-96(%rbp), %rsi
.L2207:
	testb	$24, %al
	je	.L2197
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L2197
	movq	%r8, %rdx
	movq	%r13, %rdi
	movq	%r9, -96(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-96(%rbp), %r9
	jmp	.L2197
	.p2align 4,,10
	.p2align 3
.L2240:
	testb	%dl, %dl
	jne	.L2200
	movq	%r13, %rdx
	andq	$-262144, %rdx
	testb	$4, 10(%rdx)
	je	.L2227
	movq	%r8, %rdi
	movq	%r13, %rdx
	movq	%rax, -120(%rbp)
	movq	%r9, -112(%rbp)
	movq	%rsi, -104(%rbp)
	movq	%r8, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-96(%rbp), %r8
	movq	-104(%rbp), %rsi
	movq	-112(%rbp), %r9
	movq	-120(%rbp), %rax
	jmp	.L2227
	.p2align 4,,10
	.p2align 3
.L2241:
	movq	%r8, %rdi
	movq	%r13, %rdx
	movq	%rax, -104(%rbp)
	addq	$1, %rbx
	movq	%r9, -96(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %r9
	movq	(%rax), %r8
	cmpl	%ebx, -68(%rbp)
	jg	.L2205
	jmp	.L2199
	.p2align 4,,10
	.p2align 3
.L2236:
	call	_ZN2v88internalL32Stats_Runtime_NewStrictArgumentsEiPmPNS0_7IsolateE
	movq	%rax, %r13
	jmp	.L2192
	.p2align 4,,10
	.p2align 3
.L2214:
	movl	$4, %r15d
	jmp	.L2198
.L2239:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22230:
	.size	_ZN2v88internal26Runtime_NewStrictArgumentsEiPmPNS0_7IsolateE, .-_ZN2v88internal26Runtime_NewStrictArgumentsEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL40Stats_Runtime_NewSloppyArguments_GenericEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC39:
	.string	"V8.Runtime_Runtime_NewSloppyArguments_Generic"
	.section	.text._ZN2v88internalL40Stats_Runtime_NewSloppyArguments_GenericEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL40Stats_Runtime_NewSloppyArguments_GenericEiPmPNS0_7IsolateE, @function
_ZN2v88internalL40Stats_Runtime_NewSloppyArguments_GenericEiPmPNS0_7IsolateE:
.LFB22226:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	addq	$-128, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2276
.L2243:
	movq	_ZZN2v88internalL40Stats_Runtime_NewSloppyArguments_GenericEiPmPNS0_7IsolateEE28trace_event_unique_atomic482(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2277
.L2245:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2278
.L2247:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L2279
.L2251:
	leaq	.LC35(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2277:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2280
.L2246:
	movq	%rbx, _ZZN2v88internalL40Stats_Runtime_NewSloppyArguments_GenericEiPmPNS0_7IsolateEE28trace_event_unique_atomic482(%rip)
	jmp	.L2245
	.p2align 4,,10
	.p2align 3
.L2279:
	movq	-1(%rax), %rax
	cmpw	$1105, 11(%rax)
	jne	.L2251
	leaq	-152(%rbp), %rdi
	movq	%r12, %rsi
	leaq	-156(%rbp), %rdx
	movl	$0, -156(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_118GetCallerArgumentsEPNS0_7IsolateEPi.constprop.0
	movl	-156(%rbp), %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-152(%rbp), %rdx
	call	_ZN2v88internal12_GLOBAL__N_118NewSloppyArgumentsINS1_15HandleArgumentsEEENS0_6HandleINS0_8JSObjectEEEPNS0_7IsolateENS4_INS0_10JSFunctionEEET_i
	movq	-152(%rbp), %rdi
	movq	(%rax), %r13
	testq	%rdi, %rdi
	je	.L2253
	call	_ZdaPv@PLT
.L2253:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L2256
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2256:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2281
.L2242:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2282
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2278:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2283
.L2248:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2249
	movq	(%rdi), %rax
	call	*8(%rax)
.L2249:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2250
	movq	(%rdi), %rax
	call	*8(%rax)
.L2250:
	leaq	.LC39(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L2247
	.p2align 4,,10
	.p2align 3
.L2276:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$514, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2243
	.p2align 4,,10
	.p2align 3
.L2281:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2242
	.p2align 4,,10
	.p2align 3
.L2280:
	leaq	.LC2(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2246
	.p2align 4,,10
	.p2align 3
.L2283:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC39(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L2248
.L2282:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22226:
	.size	_ZN2v88internalL40Stats_Runtime_NewSloppyArguments_GenericEiPmPNS0_7IsolateE, .-_ZN2v88internalL40Stats_Runtime_NewSloppyArguments_GenericEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal34Runtime_NewSloppyArguments_GenericEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal34Runtime_NewSloppyArguments_GenericEiPmPNS0_7IsolateE
	.type	_ZN2v88internal34Runtime_NewSloppyArguments_GenericEiPmPNS0_7IsolateE, @function
_ZN2v88internal34Runtime_NewSloppyArguments_GenericEiPmPNS0_7IsolateE:
.LFB22227:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2294
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L2295
.L2287:
	leaq	.LC35(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2295:
	movq	-1(%rax), %rax
	cmpw	$1105, 11(%rax)
	jne	.L2287
	leaq	-48(%rbp), %rdi
	leaq	-52(%rbp), %rdx
	movq	%r12, %rsi
	movl	$0, -52(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_118GetCallerArgumentsEPNS0_7IsolateEPi.constprop.0
	movl	-52(%rbp), %ecx
	movq	-48(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_118NewSloppyArgumentsINS1_15HandleArgumentsEEENS0_6HandleINS0_8JSObjectEEEPNS0_7IsolateENS4_INS0_10JSFunctionEEET_i
	movq	-48(%rbp), %rdi
	movq	(%rax), %r13
	testq	%rdi, %rdi
	je	.L2289
	call	_ZdaPv@PLT
.L2289:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L2284
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2284:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2296
	addq	$32, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2294:
	.cfi_restore_state
	call	_ZN2v88internalL40Stats_Runtime_NewSloppyArguments_GenericEiPmPNS0_7IsolateE
	movq	%rax, %r13
	jmp	.L2284
.L2296:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22227:
	.size	_ZN2v88internal34Runtime_NewSloppyArguments_GenericEiPmPNS0_7IsolateE, .-_ZN2v88internal34Runtime_NewSloppyArguments_GenericEiPmPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal29Runtime_ThrowConstAssignErrorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal29Runtime_ThrowConstAssignErrorEiPmPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal29Runtime_ThrowConstAssignErrorEiPmPNS0_7IsolateE:
.LFB28505:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE28505:
	.size	_GLOBAL__sub_I__ZN2v88internal29Runtime_ThrowConstAssignErrorEiPmPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal29Runtime_ThrowConstAssignErrorEiPmPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal29Runtime_ThrowConstAssignErrorEiPmPNS0_7IsolateE
	.section	.bss._ZZN2v88internalL44Stats_Runtime_StoreLookupSlot_SloppyHoistingEiPmPNS0_7IsolateEE28trace_event_unique_atomic980,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL44Stats_Runtime_StoreLookupSlot_SloppyHoistingEiPmPNS0_7IsolateEE28trace_event_unique_atomic980, @object
	.size	_ZZN2v88internalL44Stats_Runtime_StoreLookupSlot_SloppyHoistingEiPmPNS0_7IsolateEE28trace_event_unique_atomic980, 8
_ZZN2v88internalL44Stats_Runtime_StoreLookupSlot_SloppyHoistingEiPmPNS0_7IsolateEE28trace_event_unique_atomic980:
	.zero	8
	.section	.bss._ZZN2v88internalL36Stats_Runtime_StoreLookupSlot_StrictEiPmPNS0_7IsolateEE28trace_event_unique_atomic967,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL36Stats_Runtime_StoreLookupSlot_StrictEiPmPNS0_7IsolateEE28trace_event_unique_atomic967, @object
	.size	_ZZN2v88internalL36Stats_Runtime_StoreLookupSlot_StrictEiPmPNS0_7IsolateEE28trace_event_unique_atomic967, 8
_ZZN2v88internalL36Stats_Runtime_StoreLookupSlot_StrictEiPmPNS0_7IsolateEE28trace_event_unique_atomic967:
	.zero	8
	.section	.bss._ZZN2v88internalL36Stats_Runtime_StoreLookupSlot_SloppyEiPmPNS0_7IsolateEE28trace_event_unique_atomic956,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL36Stats_Runtime_StoreLookupSlot_SloppyEiPmPNS0_7IsolateEE28trace_event_unique_atomic956, @object
	.size	_ZZN2v88internalL36Stats_Runtime_StoreLookupSlot_SloppyEiPmPNS0_7IsolateEE28trace_event_unique_atomic956, 8
_ZZN2v88internalL36Stats_Runtime_StoreLookupSlot_SloppyEiPmPNS0_7IsolateEE28trace_event_unique_atomic956:
	.zero	8
	.section	.bss._ZZN2v88internalL35Stats_Runtime_LoadLookupSlotForCallEiPmPNS0_7IsolateEE28trace_event_unique_atomic873,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL35Stats_Runtime_LoadLookupSlotForCallEiPmPNS0_7IsolateEE28trace_event_unique_atomic873, @object
	.size	_ZZN2v88internalL35Stats_Runtime_LoadLookupSlotForCallEiPmPNS0_7IsolateEE28trace_event_unique_atomic873, 8
_ZZN2v88internalL35Stats_Runtime_LoadLookupSlotForCallEiPmPNS0_7IsolateEE28trace_event_unique_atomic873:
	.zero	8
	.section	.bss._ZZN2v88internalL40Stats_Runtime_LoadLookupSlotInsideTypeofEiPmPNS0_7IsolateEE28trace_event_unique_atomic865,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL40Stats_Runtime_LoadLookupSlotInsideTypeofEiPmPNS0_7IsolateEE28trace_event_unique_atomic865, @object
	.size	_ZZN2v88internalL40Stats_Runtime_LoadLookupSlotInsideTypeofEiPmPNS0_7IsolateEE28trace_event_unique_atomic865, 8
_ZZN2v88internalL40Stats_Runtime_LoadLookupSlotInsideTypeofEiPmPNS0_7IsolateEE28trace_event_unique_atomic865:
	.zero	8
	.section	.bss._ZZN2v88internalL28Stats_Runtime_LoadLookupSlotEiPmPNS0_7IsolateEE28trace_event_unique_atomic856,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL28Stats_Runtime_LoadLookupSlotEiPmPNS0_7IsolateEE28trace_event_unique_atomic856, @object
	.size	_ZZN2v88internalL28Stats_Runtime_LoadLookupSlotEiPmPNS0_7IsolateEE28trace_event_unique_atomic856, 8
_ZZN2v88internalL28Stats_Runtime_LoadLookupSlotEiPmPNS0_7IsolateEE28trace_event_unique_atomic856:
	.zero	8
	.section	.bss._ZZN2v88internalL30Stats_Runtime_DeleteLookupSlotEiPmPNS0_7IsolateEE28trace_event_unique_atomic750,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL30Stats_Runtime_DeleteLookupSlotEiPmPNS0_7IsolateEE28trace_event_unique_atomic750, @object
	.size	_ZZN2v88internalL30Stats_Runtime_DeleteLookupSlotEiPmPNS0_7IsolateEE28trace_event_unique_atomic750, 8
_ZZN2v88internalL30Stats_Runtime_DeleteLookupSlotEiPmPNS0_7IsolateEE28trace_event_unique_atomic750:
	.zero	8
	.section	.bss._ZZN2v88internalL30Stats_Runtime_PushBlockContextEiPmPNS0_7IsolateEE28trace_event_unique_atomic738,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL30Stats_Runtime_PushBlockContextEiPmPNS0_7IsolateEE28trace_event_unique_atomic738, @object
	.size	_ZZN2v88internalL30Stats_Runtime_PushBlockContextEiPmPNS0_7IsolateEE28trace_event_unique_atomic738, 8
_ZZN2v88internalL30Stats_Runtime_PushBlockContextEiPmPNS0_7IsolateEE28trace_event_unique_atomic738:
	.zero	8
	.section	.bss._ZZN2v88internalL30Stats_Runtime_PushCatchContextEiPmPNS0_7IsolateEE28trace_event_unique_atomic725,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL30Stats_Runtime_PushCatchContextEiPmPNS0_7IsolateEE28trace_event_unique_atomic725, @object
	.size	_ZZN2v88internalL30Stats_Runtime_PushCatchContextEiPmPNS0_7IsolateEE28trace_event_unique_atomic725, 8
_ZZN2v88internalL30Stats_Runtime_PushCatchContextEiPmPNS0_7IsolateEE28trace_event_unique_atomic725:
	.zero	8
	.section	.bss._ZZN2v88internalL31Stats_Runtime_PushModuleContextEiPmPNS0_7IsolateEE28trace_event_unique_atomic712,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL31Stats_Runtime_PushModuleContextEiPmPNS0_7IsolateEE28trace_event_unique_atomic712, @object
	.size	_ZZN2v88internalL31Stats_Runtime_PushModuleContextEiPmPNS0_7IsolateEE28trace_event_unique_atomic712, 8
_ZZN2v88internalL31Stats_Runtime_PushModuleContextEiPmPNS0_7IsolateEE28trace_event_unique_atomic712:
	.zero	8
	.section	.bss._ZZN2v88internalL29Stats_Runtime_PushWithContextEiPmPNS0_7IsolateEE28trace_event_unique_atomic700,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL29Stats_Runtime_PushWithContextEiPmPNS0_7IsolateEE28trace_event_unique_atomic700, @object
	.size	_ZZN2v88internalL29Stats_Runtime_PushWithContextEiPmPNS0_7IsolateEE28trace_event_unique_atomic700, 8
_ZZN2v88internalL29Stats_Runtime_PushWithContextEiPmPNS0_7IsolateEE28trace_event_unique_atomic700:
	.zero	8
	.section	.bss._ZZN2v88internalL32Stats_Runtime_NewFunctionContextEiPmPNS0_7IsolateEE28trace_event_unique_atomic690,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL32Stats_Runtime_NewFunctionContextEiPmPNS0_7IsolateEE28trace_event_unique_atomic690, @object
	.size	_ZZN2v88internalL32Stats_Runtime_NewFunctionContextEiPmPNS0_7IsolateEE28trace_event_unique_atomic690, 8
_ZZN2v88internalL32Stats_Runtime_NewFunctionContextEiPmPNS0_7IsolateEE28trace_event_unique_atomic690:
	.zero	8
	.section	.bss._ZZN2v88internalL30Stats_Runtime_NewScriptContextEiPmPNS0_7IsolateEE28trace_event_unique_atomic665,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL30Stats_Runtime_NewScriptContextEiPmPNS0_7IsolateEE28trace_event_unique_atomic665, @object
	.size	_ZZN2v88internalL30Stats_Runtime_NewScriptContextEiPmPNS0_7IsolateEE28trace_event_unique_atomic665, 8
_ZZN2v88internalL30Stats_Runtime_NewScriptContextEiPmPNS0_7IsolateEE28trace_event_unique_atomic665:
	.zero	8
	.section	.bss._ZZN2v88internalL32Stats_Runtime_NewClosure_TenuredEiPmPNS0_7IsolateEE28trace_event_unique_atomic613,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL32Stats_Runtime_NewClosure_TenuredEiPmPNS0_7IsolateEE28trace_event_unique_atomic613, @object
	.size	_ZZN2v88internalL32Stats_Runtime_NewClosure_TenuredEiPmPNS0_7IsolateEE28trace_event_unique_atomic613, 8
_ZZN2v88internalL32Stats_Runtime_NewClosure_TenuredEiPmPNS0_7IsolateEE28trace_event_unique_atomic613:
	.zero	8
	.section	.bss._ZZN2v88internalL24Stats_Runtime_NewClosureEiPmPNS0_7IsolateEE28trace_event_unique_atomic601,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL24Stats_Runtime_NewClosureEiPmPNS0_7IsolateEE28trace_event_unique_atomic601, @object
	.size	_ZZN2v88internalL24Stats_Runtime_NewClosureEiPmPNS0_7IsolateEE28trace_event_unique_atomic601, 8
_ZZN2v88internalL24Stats_Runtime_NewClosureEiPmPNS0_7IsolateEE28trace_event_unique_atomic601:
	.zero	8
	.section	.bss._ZZN2v88internalL34Stats_Runtime_NewArgumentsElementsEiPmPNS0_7IsolateEE28trace_event_unique_atomic577,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL34Stats_Runtime_NewArgumentsElementsEiPmPNS0_7IsolateEE28trace_event_unique_atomic577, @object
	.size	_ZZN2v88internalL34Stats_Runtime_NewArgumentsElementsEiPmPNS0_7IsolateEE28trace_event_unique_atomic577, 8
_ZZN2v88internalL34Stats_Runtime_NewArgumentsElementsEiPmPNS0_7IsolateEE28trace_event_unique_atomic577:
	.zero	8
	.section	.bss._ZZN2v88internalL32Stats_Runtime_NewSloppyArgumentsEiPmPNS0_7IsolateEE28trace_event_unique_atomic547,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL32Stats_Runtime_NewSloppyArgumentsEiPmPNS0_7IsolateEE28trace_event_unique_atomic547, @object
	.size	_ZZN2v88internalL32Stats_Runtime_NewSloppyArgumentsEiPmPNS0_7IsolateEE28trace_event_unique_atomic547, 8
_ZZN2v88internalL32Stats_Runtime_NewSloppyArgumentsEiPmPNS0_7IsolateEE28trace_event_unique_atomic547:
	.zero	8
	.section	.bss._ZZN2v88internalL30Stats_Runtime_NewRestParameterEiPmPNS0_7IsolateEE28trace_event_unique_atomic521,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL30Stats_Runtime_NewRestParameterEiPmPNS0_7IsolateEE28trace_event_unique_atomic521, @object
	.size	_ZZN2v88internalL30Stats_Runtime_NewRestParameterEiPmPNS0_7IsolateEE28trace_event_unique_atomic521, 8
_ZZN2v88internalL30Stats_Runtime_NewRestParameterEiPmPNS0_7IsolateEE28trace_event_unique_atomic521:
	.zero	8
	.section	.bss._ZZN2v88internalL32Stats_Runtime_NewStrictArgumentsEiPmPNS0_7IsolateEE28trace_event_unique_atomic496,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL32Stats_Runtime_NewStrictArgumentsEiPmPNS0_7IsolateEE28trace_event_unique_atomic496, @object
	.size	_ZZN2v88internalL32Stats_Runtime_NewStrictArgumentsEiPmPNS0_7IsolateEE28trace_event_unique_atomic496, 8
_ZZN2v88internalL32Stats_Runtime_NewStrictArgumentsEiPmPNS0_7IsolateEE28trace_event_unique_atomic496:
	.zero	8
	.section	.bss._ZZN2v88internalL40Stats_Runtime_NewSloppyArguments_GenericEiPmPNS0_7IsolateEE28trace_event_unique_atomic482,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL40Stats_Runtime_NewSloppyArguments_GenericEiPmPNS0_7IsolateEE28trace_event_unique_atomic482, @object
	.size	_ZZN2v88internalL40Stats_Runtime_NewSloppyArguments_GenericEiPmPNS0_7IsolateEE28trace_event_unique_atomic482, 8
_ZZN2v88internalL40Stats_Runtime_NewSloppyArguments_GenericEiPmPNS0_7IsolateEE28trace_event_unique_atomic482:
	.zero	8
	.section	.bss._ZZN2v88internalL28Stats_Runtime_DeclareEvalVarEiPmPNS0_7IsolateEE28trace_event_unique_atomic315,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL28Stats_Runtime_DeclareEvalVarEiPmPNS0_7IsolateEE28trace_event_unique_atomic315, @object
	.size	_ZZN2v88internalL28Stats_Runtime_DeclareEvalVarEiPmPNS0_7IsolateEE28trace_event_unique_atomic315, 8
_ZZN2v88internalL28Stats_Runtime_DeclareEvalVarEiPmPNS0_7IsolateEE28trace_event_unique_atomic315:
	.zero	8
	.section	.bss._ZZN2v88internalL33Stats_Runtime_DeclareEvalFunctionEiPmPNS0_7IsolateEE28trace_event_unique_atomic307,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL33Stats_Runtime_DeclareEvalFunctionEiPmPNS0_7IsolateEE28trace_event_unique_atomic307, @object
	.size	_ZZN2v88internalL33Stats_Runtime_DeclareEvalFunctionEiPmPNS0_7IsolateEE28trace_event_unique_atomic307, 8
_ZZN2v88internalL33Stats_Runtime_DeclareEvalFunctionEiPmPNS0_7IsolateEE28trace_event_unique_atomic307:
	.zero	8
	.section	.bss._ZZN2v88internalL28Stats_Runtime_DeclareGlobalsEiPmPNS0_7IsolateEE28trace_event_unique_atomic205,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL28Stats_Runtime_DeclareGlobalsEiPmPNS0_7IsolateEE28trace_event_unique_atomic205, @object
	.size	_ZZN2v88internalL28Stats_Runtime_DeclareGlobalsEiPmPNS0_7IsolateEE28trace_event_unique_atomic205, 8
_ZZN2v88internalL28Stats_Runtime_DeclareGlobalsEiPmPNS0_7IsolateEE28trace_event_unique_atomic205:
	.zero	8
	.section	.bss._ZZN2v88internalL35Stats_Runtime_ThrowConstAssignErrorEiPmPNS0_7IsolateEE27trace_event_unique_atomic25,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL35Stats_Runtime_ThrowConstAssignErrorEiPmPNS0_7IsolateEE27trace_event_unique_atomic25, @object
	.size	_ZZN2v88internalL35Stats_Runtime_ThrowConstAssignErrorEiPmPNS0_7IsolateEE27trace_event_unique_atomic25, 8
_ZZN2v88internalL35Stats_Runtime_ThrowConstAssignErrorEiPmPNS0_7IsolateEE27trace_event_unique_atomic25:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
