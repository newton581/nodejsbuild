	.file	"builtins-api.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB4860:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE4860:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB4861:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4861:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB4863:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4863:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.text._ZN2v88internal11Relocatable21PostGarbageCollectionEv,"axG",@progbits,_ZN2v88internal11Relocatable21PostGarbageCollectionEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11Relocatable21PostGarbageCollectionEv
	.type	_ZN2v88internal11Relocatable21PostGarbageCollectionEv, @function
_ZN2v88internal11Relocatable21PostGarbageCollectionEv:
.LFB6461:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6461:
	.size	_ZN2v88internal11Relocatable21PostGarbageCollectionEv, .-_ZN2v88internal11Relocatable21PostGarbageCollectionEv
	.section	.text._ZN2v88internal12_GLOBAL__N_120RelocatableArguments15IterateInstanceEPNS0_11RootVisitorE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_120RelocatableArguments15IterateInstanceEPNS0_11RootVisitorE, @function
_ZN2v88internal12_GLOBAL__N_120RelocatableArguments15IterateInstanceEPNS0_11RootVisitorE:
.LFB18736:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	movq	%rsi, %r9
	cmpl	$4, %eax
	je	.L6
	movq	(%rsi), %rdx
	leal	-8(,%rax,8), %eax
	movl	$7, %esi
	cltq
	movq	16(%rdx), %r10
	movq	32(%rdi), %rdx
	movq	%r9, %rdi
	movq	%rdx, %rcx
	leaq	8(%rdx), %r8
	xorl	%edx, %edx
	subq	%rax, %rcx
	jmp	*%r10
	.p2align 4,,10
	.p2align 3
.L6:
	ret
	.cfi_endproc
.LFE18736:
	.size	_ZN2v88internal12_GLOBAL__N_120RelocatableArguments15IterateInstanceEPNS0_11RootVisitorE, .-_ZN2v88internal12_GLOBAL__N_120RelocatableArguments15IterateInstanceEPNS0_11RootVisitorE
	.section	.text._ZN2v88internal12_GLOBAL__N_120RelocatableArgumentsD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_120RelocatableArgumentsD2Ev, @function
_ZN2v88internal12_GLOBAL__N_120RelocatableArgumentsD2Ev:
.LFB23039:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	16(%rdi), %rdx
	movq	%rdx, 41704(%rax)
	ret
	.cfi_endproc
.LFE23039:
	.size	_ZN2v88internal12_GLOBAL__N_120RelocatableArgumentsD2Ev, .-_ZN2v88internal12_GLOBAL__N_120RelocatableArgumentsD2Ev
	.set	_ZN2v88internal12_GLOBAL__N_120RelocatableArgumentsD1Ev,_ZN2v88internal12_GLOBAL__N_120RelocatableArgumentsD2Ev
	.section	.text._ZN2v88internal12_GLOBAL__N_120RelocatableArgumentsD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_120RelocatableArgumentsD0Ev, @function
_ZN2v88internal12_GLOBAL__N_120RelocatableArgumentsD0Ev:
.LFB23041:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	16(%rdi), %rdx
	movl	$40, %esi
	movq	%rdx, 41704(%rax)
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE23041:
	.size	_ZN2v88internal12_GLOBAL__N_120RelocatableArgumentsD0Ev, .-_ZN2v88internal12_GLOBAL__N_120RelocatableArgumentsD0Ev
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB18283:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L16
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L19
.L10:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L10
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE18283:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.rodata._ZN2v88internal25FunctionCallbackArguments4CallENS0_15CallHandlerInfoE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"call"
	.section	.rodata._ZN2v88internal25FunctionCallbackArguments4CallENS0_15CallHandlerInfoE.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"disabled-by-default-v8.runtime"
	.section	.rodata._ZN2v88internal25FunctionCallbackArguments4CallENS0_15CallHandlerInfoE.str1.1
.LC2:
	.string	"V8.ExternalCallback"
	.section	.text._ZN2v88internal25FunctionCallbackArguments4CallENS0_15CallHandlerInfoE,"axG",@progbits,_ZN2v88internal25FunctionCallbackArguments4CallENS0_15CallHandlerInfoE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal25FunctionCallbackArguments4CallENS0_15CallHandlerInfoE
	.type	_ZN2v88internal25FunctionCallbackArguments4CallENS0_15CallHandlerInfoE, @function
_ZN2v88internal25FunctionCallbackArguments4CallENS0_15CallHandlerInfoE:
.LFB18685:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 3, -56
	movq	32(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	41016(%rbx), %r14
	movq	%r14, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	jne	.L78
.L21:
	pxor	%xmm0, %xmm0
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L79
.L22:
	movq	7(%r13), %rax
	cmpq	%rax, _ZN2v88internal3Smi5kZeroE(%rip)
	je	.L46
	movq	7(%rax), %r15
	movq	%r15, %r14
.L23:
	cmpl	$32, 41828(%rbx)
	jne	.L24
	movq	41112(%rbx), %rdi
	movq	41472(%rbx), %r8
	testq	%rdi, %rdi
	je	.L25
	movq	%r13, %rsi
	movq	%r8, -200(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-200(%rbp), %r8
	movq	%rax, %rsi
.L26:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r8, %rdi
	call	_ZN2v88internal5Debug33PerformSideEffectCheckForCallbackENS0_6HandleINS0_6ObjectEEES4_NS1_12AccessorKindE@PLT
	testb	%al, %al
	jne	.L24
	movq	-128(%rbp), %rdi
	xorl	%r12d, %r12d
	testq	%rdi, %rdi
	je	.L44
.L84:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L24:
	movq	12608(%rbx), %rax
	movq	%r15, -184(%rbp)
	movl	12616(%rbx), %r13d
	movq	%rbx, -192(%rbp)
	movq	%rax, -176(%rbp)
	leaq	-192(%rbp), %rax
	movl	$6, 12616(%rbx)
	movq	%rax, 12608(%rbx)
	movq	_ZZN2v88internal21ExternalCallbackScopeC4EPNS0_7IsolateEmE27trace_event_unique_atomic62(%rip), %rdx
	movq	%rdx, %r15
	testq	%rdx, %rdx
	je	.L80
	movzbl	(%r15), %eax
	testb	$5, %al
	jne	.L81
.L32:
	leaq	24(%r12), %rcx
	movl	80(%r12), %eax
	leaq	-160(%rbp), %rdi
	movq	%rcx, %xmm0
	movhps	72(%r12), %xmm0
	movl	%eax, -144(%rbp)
	movaps	%xmm0, -160(%rbp)
	call	*%r14
	movq	96(%rbx), %rcx
	xorl	%eax, %eax
	cmpq	%rcx, 48(%r12)
	je	.L36
	leaq	48(%r12), %rax
.L36:
	movq	-176(%rbp), %rdx
	movq	%rax, %r12
	movq	-192(%rbp), %rax
	movq	%rdx, 12608(%rax)
	movq	_ZZN2v88internal21ExternalCallbackScopeD4EvE27trace_event_unique_atomic68(%rip), %rdx
	movq	%rdx, %r14
	testq	%rdx, %rdx
	je	.L82
	movzbl	(%r14), %eax
	testb	$5, %al
	jne	.L83
.L40:
	movq	-128(%rbp), %rdi
	movl	%r13d, 12616(%rbx)
	testq	%rdi, %rdi
	jne	.L84
.L44:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L85
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L80:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r15
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L86
.L31:
	movq	%r15, _ZZN2v88internal21ExternalCallbackScopeC4EPNS0_7IsolateEmE27trace_event_unique_atomic62(%rip)
	movzbl	(%r15), %eax
	testb	$5, %al
	je	.L32
.L81:
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L87
.L33:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L34
	movq	(%rdi), %rax
	call	*8(%rax)
.L34:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L32
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L83:
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L88
.L41:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L42
	movq	(%rdi), %rax
	call	*8(%rax)
.L42:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L40
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L82:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r14
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L89
.L39:
	movq	%r14, _ZZN2v88internal21ExternalCallbackScopeD4EvE27trace_event_unique_atomic68(%rip)
	movzbl	(%r14), %eax
	testb	$5, %al
	je	.L40
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L25:
	movq	41088(%rbx), %rsi
	cmpq	41096(%rbx), %rsi
	je	.L90
.L27:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rbx)
	movq	%r13, (%rsi)
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L78:
	movq	24(%r12), %rdx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal6Logger15ApiObjectAccessEPKcNS0_8JSObjectE@PLT
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L79:
	movq	40960(%rbx), %rax
	leaq	-120(%rbp), %rsi
	movl	$143, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L46:
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L89:
	leaq	.LC1(%rip), %rsi
	call	*%rax
	movq	%rax, %r14
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L86:
	leaq	.LC1(%rip), %rsi
	call	*%rax
	movq	%rax, %r15
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L87:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC2(%rip), %rcx
	movl	$66, %esi
	pushq	%rdx
	movq	%r15, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L88:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC2(%rip), %rcx
	movl	$69, %esi
	pushq	%rdx
	movq	%r14, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L90:
	movq	%rbx, %rdi
	movq	%r8, -200(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-200(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L27
.L85:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18685:
	.size	_ZN2v88internal25FunctionCallbackArguments4CallENS0_15CallHandlerInfoE, .-_ZN2v88internal25FunctionCallbackArguments4CallENS0_15CallHandlerInfoE
	.section	.text._ZN2v88internal12_GLOBAL__N_119HandleApiCallHelperILb0EEENS0_11MaybeHandleINS0_6ObjectEEEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEESA_NS8_INS0_20FunctionTemplateInfoEEENS8_IS4_EENS0_16BuiltinArgumentsE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_119HandleApiCallHelperILb0EEENS0_11MaybeHandleINS0_6ObjectEEEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEESA_NS8_INS0_20FunctionTemplateInfoEEENS8_IS4_EENS0_16BuiltinArgumentsE, @function
_ZN2v88internal12_GLOBAL__N_119HandleApiCallHelperILb0EEENS0_11MaybeHandleINS0_6ObjectEEEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEESA_NS8_INS0_20FunctionTemplateInfoEEENS8_IS4_EENS0_16BuiltinArgumentsE:
.LFB20884:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$168, %rsp
	movq	16(%rbp), %r8
	movq	24(%rbp), %rcx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rbx), %rax
	movq	(%r12), %r14
	testb	$32, 91(%rax)
	je	.L125
	movq	63(%rax), %rax
	testb	$1, %al
	jne	.L126
.L110:
	testq	%r14, %r14
	je	.L113
	movq	(%rbx), %rax
	movq	47(%rax), %rbx
	movq	%r12, %rax
	cmpq	%rbx, 88(%r13)
	jne	.L127
.L106:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L128
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L127:
	.cfi_restore_state
	subl	$5, %r8d
	subq	$8, %rcx
	movq	23(%rbx), %rdx
	movq	(%r9), %r10
	pushq	%r8
	movq	(%r15), %r9
	leaq	-144(%rbp), %r12
	movq	%r14, %r8
	pushq	%rcx
	movq	%r13, %rsi
	movq	%r10, %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal25FunctionCallbackArgumentsC1EPNS0_7IsolateENS0_6ObjectENS0_10HeapObjectES4_S5_Pmi@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal25FunctionCallbackArguments4CallENS0_15CallHandlerInfoE
	popq	%rdx
	movq	12552(%r13), %rbx
	popq	%rcx
	cmpq	%rbx, 96(%r13)
	jne	.L129
	testq	%rax, %rax
	je	.L130
	movq	41112(%r13), %rdi
	movq	(%rax), %rsi
	testq	%rdi, %rdi
	je	.L119
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	.p2align 4,,10
	.p2align 3
.L117:
	movq	-136(%rbp), %rdx
	movq	-128(%rbp), %rcx
	movq	%rcx, 41704(%rdx)
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L125:
	movq	-1(%r14), %rax
	leaq	-1(%r14), %rdx
	cmpw	$1026, 11(%rax)
	je	.L131
	movq	-1(%r14), %rax
	movzbl	13(%rax), %eax
	shrb	$5, %al
	andl	$1, %eax
.L96:
	testb	%al, %al
	je	.L103
	movq	41112(%r13), %rdi
	movq	12464(%r13), %r14
	testq	%rdi, %rdi
	je	.L123
	movq	%r14, %rsi
	movq	%rcx, -184(%rbp)
	movq	%r8, -176(%rbp)
	movq	%r9, -168(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-168(%rbp), %r9
	movq	-176(%rbp), %r8
	movq	-184(%rbp), %rcx
	movq	%rax, %rsi
.L101:
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rcx, -184(%rbp)
	movq	%r8, -176(%rbp)
	movq	%r9, -168(%rbp)
	call	_ZN2v88internal7Isolate9MayAccessENS0_6HandleINS0_7ContextEEENS2_INS0_8JSObjectEEE@PLT
	movq	-168(%rbp), %r9
	movq	-176(%rbp), %r8
	testb	%al, %al
	movq	-184(%rbp), %rcx
	jne	.L103
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal7Isolate23ReportFailedAccessCheckENS0_6HandleINS0_8JSObjectEEE@PLT
	movq	12552(%r13), %rdi
	leaq	88(%r13), %rax
	cmpq	%rdi, 96(%r13)
	je	.L106
	movq	%r13, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	xorl	%eax, %eax
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L103:
	movq	(%rbx), %rax
	movq	(%r12), %r14
	movq	63(%rax), %rax
	testb	$1, %al
	je	.L110
.L126:
	movq	-1(%rax), %rdx
	cmpw	$88, 11(%rdx)
	jne	.L110
	movq	-1(%r14), %rdx
	leaq	-1(%r14), %r10
	cmpw	$1024, 11(%rdx)
	ja	.L132
.L113:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$56, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	xorl	%eax, %eax
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L129:
	movq	%r13, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	xorl	%eax, %eax
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L131:
	movq	%rsi, -184(%rbp)
	movq	%r14, %rsi
	leaq	-152(%rbp), %rdi
	andq	$-262144, %rsi
	movq	%rcx, -200(%rbp)
	movq	24(%rsi), %rax
	movq	%r8, -192(%rbp)
	movq	%rdx, -176(%rbp)
	movq	-25128(%rax), %rax
	movq	%rsi, -168(%rbp)
	movq	%rax, -152(%rbp)
	call	_ZN2v88internal7Context13global_objectEv@PLT
	movq	-168(%rbp), %rsi
	andl	$1, %r14d
	movq	-176(%rbp), %rdx
	movq	-184(%rbp), %r9
	movq	-192(%rbp), %r8
	movq	24(%rsi), %rsi
	movq	-200(%rbp), %rcx
	jne	.L133
.L94:
	movq	(%rdx), %rdx
	movq	23(%rdx), %rdx
.L95:
	cmpq	%rdx, %rax
	setne	%al
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L123:
	movq	41088(%r13), %rsi
	cmpq	41096(%r13), %rsi
	je	.L134
.L102:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r13)
	movq	%r14, (%rsi)
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L119:
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L135
.L121:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%rsi, (%rax)
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L132:
	movq	%rcx, -200(%rbp)
	leaq	-152(%rbp), %rdi
	movq	%r8, -192(%rbp)
	movq	%r9, -184(%rbp)
	movq	%r10, -176(%rbp)
	movq	%rax, -152(%rbp)
	movq	-1(%r14), %rsi
	movq	%rdi, -168(%rbp)
	call	_ZN2v88internal20FunctionTemplateInfo13IsTemplateForENS0_3MapE@PLT
	movq	-168(%rbp), %rdi
	movq	-176(%rbp), %r10
	testb	%al, %al
	movq	-184(%rbp), %r9
	movq	-192(%rbp), %r8
	movq	-200(%rbp), %rcx
	jne	.L110
	movq	(%r10), %rax
	cmpw	$1026, 11(%rax)
	jne	.L113
	movq	%rcx, -184(%rbp)
	movq	%r8, -176(%rbp)
	movq	%r9, -168(%rbp)
	movq	(%r10), %rax
	movq	23(%rax), %r14
	cmpq	104(%r13), %r14
	je	.L113
	movq	-1(%r14), %rsi
	call	_ZN2v88internal20FunctionTemplateInfo13IsTemplateForENS0_3MapE@PLT
	movq	-168(%rbp), %r9
	movq	-176(%rbp), %r8
	testb	%al, %al
	movq	-184(%rbp), %rcx
	je	.L113
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L135:
	movq	%r13, %rdi
	movq	%rsi, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %rsi
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L130:
	leaq	88(%r13), %rax
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L134:
	movq	%r13, %rdi
	movq	%rcx, -184(%rbp)
	movq	%r8, -176(%rbp)
	movq	%r9, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-184(%rbp), %rcx
	movq	-176(%rbp), %r8
	movq	-168(%rbp), %r9
	movq	%rax, %rsi
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L133:
	movq	(%rdx), %rdi
	cmpw	$1024, 11(%rdi)
	jne	.L94
	movq	-37488(%rsi), %rdx
	jmp	.L95
.L128:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20884:
	.size	_ZN2v88internal12_GLOBAL__N_119HandleApiCallHelperILb0EEENS0_11MaybeHandleINS0_6ObjectEEEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEESA_NS8_INS0_20FunctionTemplateInfoEEENS8_IS4_EENS0_16BuiltinArgumentsE, .-_ZN2v88internal12_GLOBAL__N_119HandleApiCallHelperILb0EEENS0_11MaybeHandleINS0_6ObjectEEEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEESA_NS8_INS0_20FunctionTemplateInfoEEENS8_IS4_EENS0_16BuiltinArgumentsE
	.section	.text._ZN2v88internal12_GLOBAL__N_119HandleApiCallHelperILb1EEENS0_11MaybeHandleINS0_6ObjectEEEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEESA_NS8_INS0_20FunctionTemplateInfoEEENS8_IS4_EENS0_16BuiltinArgumentsE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_119HandleApiCallHelperILb1EEENS0_11MaybeHandleINS0_6ObjectEEEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEESA_NS8_INS0_20FunctionTemplateInfoEEENS8_IS4_EENS0_16BuiltinArgumentsE, @function
_ZN2v88internal12_GLOBAL__N_119HandleApiCallHelperILb1EEENS0_11MaybeHandleINS0_6ObjectEEEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEESA_NS8_INS0_20FunctionTemplateInfoEEENS8_IS4_EENS0_16BuiltinArgumentsE:
.LFB20883:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$152, %rsp
	movq	16(%rbp), %rax
	movq	(%rcx), %rdx
	movq	24(%rbp), %r14
	movq	%rax, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	71(%rdx), %rax
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	movq	-37504(%rdx), %rsi
	movq	%rax, %rdx
	cmpq	%rsi, %rax
	je	.L137
	movq	47(%rax), %rdx
.L137:
	cmpq	%rdx, 88(%r12)
	je	.L169
.L138:
	cmpq	%rsi, %rax
	je	.L143
	movq	47(%rax), %rsi
.L143:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L144
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r8
.L145:
	movq	%rbx, %rdx
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10ApiNatives17InstantiateObjectEPNS0_7IsolateENS0_6HandleINS0_18ObjectTemplateInfoEEENS4_INS0_10JSReceiverEEE@PLT
	movq	%rax, %r10
	xorl	%eax, %eax
	testq	%r10, %r10
	je	.L148
	movq	(%r10), %rax
	movq	%rax, (%r14)
	movq	(%r15), %rax
	movq	(%r10), %r8
	movq	47(%rax), %r11
	cmpq	%r11, 88(%r12)
	jne	.L170
.L149:
	movq	%r10, %rax
.L148:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L171
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L170:
	.cfi_restore_state
	subq	$8, %r14
	movq	23(%r11), %rdx
	movq	0(%r13), %rcx
	movq	%r12, %rsi
	movl	-152(%rbp), %r15d
	leaq	-144(%rbp), %r13
	movq	%r10, -168(%rbp)
	movq	%r13, %rdi
	movq	%r11, -160(%rbp)
	subl	$5, %r15d
	pushq	%r15
	pushq	%r14
	movq	(%rbx), %r9
	call	_ZN2v88internal25FunctionCallbackArgumentsC1EPNS0_7IsolateENS0_6ObjectENS0_10HeapObjectES4_S5_Pmi@PLT
	movq	-160(%rbp), %r11
	movq	%r13, %rdi
	movq	%r11, %rsi
	call	_ZN2v88internal25FunctionCallbackArguments4CallENS0_15CallHandlerInfoE
	movq	12552(%r12), %rbx
	popq	%rdx
	cmpq	%rbx, 96(%r12)
	movq	-168(%rbp), %r10
	popq	%rcx
	jne	.L172
	testq	%rax, %rax
	je	.L156
	movq	(%rax), %rsi
	testb	$1, %sil
	jne	.L173
.L153:
	movq	-136(%rbp), %rax
	movq	-128(%rbp), %rdx
	movq	%rdx, 41704(%rax)
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L144:
	movq	41088(%r12), %r8
	cmpq	41096(%r12), %r8
	je	.L174
.L146:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r8)
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L169:
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v814ObjectTemplate3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	movq	%rax, %rdx
	movq	(%r15), %rax
	movq	71(%rax), %rdi
	cmpq	88(%r12), %rdi
	je	.L175
.L139:
	movq	(%rdx), %rdx
	leaq	47(%rdi), %rsi
	movq	%rdx, 47(%rdi)
	testb	$1, %dl
	je	.L158
	movq	%rdx, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rax
	movq	%r8, -160(%rbp)
	testl	$262144, %eax
	je	.L141
	movq	%rdx, -184(%rbp)
	movq	%rsi, -176(%rbp)
	movq	%rdi, -168(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-160(%rbp), %r8
	movq	-184(%rbp), %rdx
	movq	-176(%rbp), %rsi
	movq	-168(%rbp), %rdi
	movq	8(%r8), %rax
.L141:
	testb	$24, %al
	je	.L158
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L176
	.p2align 4,,10
	.p2align 3
.L158:
	movq	(%r15), %rdx
	movq	71(%rdx), %rax
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	movq	-37504(%rdx), %rsi
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L173:
	movq	-1(%rsi), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L153
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L155
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r10
	.p2align 4,,10
	.p2align 3
.L156:
	movq	%r10, %rax
.L151:
	movq	-136(%rbp), %rdx
	movq	-128(%rbp), %rcx
	movq	%rcx, 41704(%rdx)
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L172:
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	xorl	%eax, %eax
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L174:
	movq	%r12, %rdi
	movq	%rsi, -160(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-160(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L175:
	movq	%r12, %rdi
	movq	%r15, %rsi
	movq	%rdx, -160(%rbp)
	call	_ZN2v88internal20FunctionTemplateInfo32AllocateFunctionTemplateRareDataEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	-160(%rbp), %rdx
	movq	%rax, %rdi
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L176:
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L155:
	movq	41088(%r12), %r10
	cmpq	41096(%r12), %r10
	je	.L177
.L157:
	leaq	8(%r10), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r10)
	jmp	.L156
.L177:
	movq	%r12, %rdi
	movq	%rsi, -152(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-152(%rbp), %rsi
	movq	%rax, %r10
	jmp	.L157
.L171:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20883:
	.size	_ZN2v88internal12_GLOBAL__N_119HandleApiCallHelperILb1EEENS0_11MaybeHandleINS0_6ObjectEEEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEESA_NS8_INS0_20FunctionTemplateInfoEEENS8_IS4_EENS0_16BuiltinArgumentsE, .-_ZN2v88internal12_GLOBAL__N_119HandleApiCallHelperILb1EEENS0_11MaybeHandleINS0_6ObjectEEEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEESA_NS8_INS0_20FunctionTemplateInfoEEENS8_IS4_EENS0_16BuiltinArgumentsE
	.section	.text._ZN2v88internalL26Builtin_Impl_HandleApiCallENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL26Builtin_Impl_HandleApiCallENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL26Builtin_Impl_HandleApiCallENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB18732:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	-16(,%rdi,8), %eax
	movq	%rsi, %r9
	movq	%rdi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %r13
	movslq	%eax, %rdx
	addl	$8, %eax
	subq	%rdx, %r15
	cltq
	movq	%rsi, %rdx
	subq	%rax, %rdx
	movq	(%r15), %rax
	movq	23(%rax), %rax
	movq	7(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L179
	movq	%r8, -80(%rbp)
	movq	%rdx, -56(%rbp)
	movq	%rbx, -72(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-56(%rbp), %rdx
	movq	-80(%rbp), %r8
	movq	-72(%rbp), %r9
	movq	%rax, %rcx
.L180:
	movq	(%rdx), %rax
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L182
	pushq	%r9
	movq	%r15, %rsi
	movq	%r12, %rdi
	pushq	%r8
	movq	%rbx, %r8
	call	_ZN2v88internal12_GLOBAL__N_119HandleApiCallHelperILb1EEENS0_11MaybeHandleINS0_6ObjectEEEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEESA_NS8_INS0_20FunctionTemplateInfoEEENS8_IS4_EENS0_16BuiltinArgumentsE
	popq	%rsi
	popq	%rdi
	testq	%rax, %rax
	je	.L190
.L185:
	movq	(%rax), %r15
.L184:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L188
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L188:
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L182:
	.cfi_restore_state
	pushq	%r9
	movq	%r15, %rsi
	movq	%r12, %rdi
	pushq	%r8
	movq	%rbx, %r8
	call	_ZN2v88internal12_GLOBAL__N_119HandleApiCallHelperILb0EEENS0_11MaybeHandleINS0_6ObjectEEEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEESA_NS8_INS0_20FunctionTemplateInfoEEENS8_IS4_EENS0_16BuiltinArgumentsE
	popq	%rdx
	popq	%rcx
	testq	%rax, %rax
	jne	.L185
.L190:
	movq	312(%r12), %r15
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L179:
	movq	41088(%r12), %rcx
	cmpq	41096(%r12), %rcx
	je	.L191
.L181:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rcx)
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L191:
	movq	%r12, %rdi
	movq	%r8, -96(%rbp)
	movq	%rsi, -80(%rbp)
	movq	%rdx, -56(%rbp)
	movq	%rbx, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %r8
	movq	-88(%rbp), %r9
	movq	-80(%rbp), %rsi
	movq	-56(%rbp), %rdx
	movq	%rax, %rcx
	jmp	.L181
	.cfi_endproc
.LFE18732:
	.size	_ZN2v88internalL26Builtin_Impl_HandleApiCallENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL26Builtin_Impl_HandleApiCallENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL32Builtin_Impl_Stats_HandleApiCallEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"V8.Builtin_HandleApiCall"
	.section	.text._ZN2v88internalL32Builtin_Impl_Stats_HandleApiCallEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL32Builtin_Impl_Stats_HandleApiCallEiPmPNS0_7IsolateE, @function
_ZN2v88internalL32Builtin_Impl_Stats_HandleApiCallEiPmPNS0_7IsolateE:
.LFB18730:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L221
.L193:
	movq	_ZZN2v88internalL32Builtin_Impl_Stats_HandleApiCallEiPmPNS0_7IsolateEE28trace_event_unique_atomic129(%rip), %rbx
	testq	%rbx, %rbx
	je	.L222
.L195:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L223
.L197:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL26Builtin_Impl_HandleApiCallENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L224
.L201:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L225
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L222:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L226
.L196:
	movq	%rbx, _ZZN2v88internalL32Builtin_Impl_Stats_HandleApiCallEiPmPNS0_7IsolateEE28trace_event_unique_atomic129(%rip)
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L223:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L227
.L198:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L199
	movq	(%rdi), %rax
	call	*8(%rax)
.L199:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L200
	movq	(%rdi), %rax
	call	*8(%rax)
.L200:
	leaq	.LC3(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L224:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L221:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$669, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L227:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC3(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L226:
	leaq	.LC1(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L196
.L225:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18730:
	.size	_ZN2v88internalL32Builtin_Impl_Stats_HandleApiCallEiPmPNS0_7IsolateE, .-_ZN2v88internalL32Builtin_Impl_Stats_HandleApiCallEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL36HandleApiCallAsFunctionOrConstructorEPNS0_7IsolateEbNS0_16BuiltinArgumentsE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"call non-function"
	.section	.text._ZN2v88internalL36HandleApiCallAsFunctionOrConstructorEPNS0_7IsolateEbNS0_16BuiltinArgumentsE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL36HandleApiCallAsFunctionOrConstructorEPNS0_7IsolateEbNS0_16BuiltinArgumentsE, @function
_ZN2v88internalL36HandleApiCallAsFunctionOrConstructorEPNS0_7IsolateEbNS0_16BuiltinArgumentsE:
.LFB18738:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$152, %rsp
	movq	%rdx, -152(%rbp)
	movq	(%rcx), %r8
	movq	%r8, %r9
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	%sil, %sil
	jne	.L229
	movq	88(%rdi), %r9
.L229:
	movq	-1(%r8), %rax
	movq	31(%rax), %rcx
	testb	$1, %cl
	jne	.L231
.L230:
	movq	23(%rcx), %rax
	movq	7(%rax), %rax
	movq	71(%rax), %rdx
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-37504(%rax), %r15
	cmpq	%r15, %rdx
	je	.L232
	movq	55(%rdx), %r15
.L232:
	movq	41016(%r12), %r13
	movq	41088(%r12), %rax
	movq	%rcx, -176(%rbp)
	movq	%r9, -168(%rbp)
	movq	41096(%r12), %r14
	addl	$1, 41104(%r12)
	movq	%r13, %rdi
	movq	%r8, -160(%rbp)
	movq	%rax, -184(%rbp)
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	movq	-160(%rbp), %r8
	movq	-168(%rbp), %r9
	testb	%al, %al
	movq	-176(%rbp), %rcx
	jne	.L250
.L233:
	movl	-152(%rbp), %eax
	subq	$8, %rbx
	movq	23(%r15), %rdx
	movq	%r12, %rsi
	leaq	-144(%rbp), %r13
	subl	$5, %eax
	movq	%r13, %rdi
	pushq	%rax
	pushq	%rbx
	call	_ZN2v88internal25FunctionCallbackArgumentsC1EPNS0_7IsolateENS0_6ObjectENS0_10HeapObjectES4_S5_Pmi@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal25FunctionCallbackArguments4CallENS0_15CallHandlerInfoE
	popq	%rdx
	popq	%rcx
	testq	%rax, %rax
	je	.L251
	movq	(%rax), %rbx
.L235:
	movq	-136(%rbp), %rax
	movq	-128(%rbp), %rdx
	movq	%rdx, 41704(%rax)
	movq	-184(%rbp), %rax
	subl	$1, 41104(%r12)
	movq	%rax, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L239
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L239:
	movq	12552(%r12), %rsi
	movq	%rbx, %rax
	cmpq	%rsi, 96(%r12)
	je	.L237
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
.L237:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L252
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L253:
	.cfi_restore_state
	movq	31(%rcx), %rcx
	testb	$1, %cl
	je	.L230
	.p2align 4,,10
	.p2align 3
.L231:
	movq	-1(%rcx), %rax
	cmpw	$68, 11(%rax)
	jne	.L230
	jmp	.L253
	.p2align 4,,10
	.p2align 3
.L250:
	movq	%r8, %rdx
	leaq	.LC4(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal6Logger15ApiObjectAccessEPKcNS0_8JSObjectE@PLT
	movq	-176(%rbp), %rcx
	movq	-168(%rbp), %r9
	movq	-160(%rbp), %r8
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L251:
	movq	88(%r12), %rbx
	jmp	.L235
.L252:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18738:
	.size	_ZN2v88internalL36HandleApiCallAsFunctionOrConstructorEPNS0_7IsolateEbNS0_16BuiltinArgumentsE, .-_ZN2v88internalL36HandleApiCallAsFunctionOrConstructorEPNS0_7IsolateEbNS0_16BuiltinArgumentsE
	.section	.rodata._ZN2v88internalL42Builtin_Impl_Stats_HandleApiCallAsFunctionEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"V8.Builtin_HandleApiCallAsFunction"
	.section	.text._ZN2v88internalL42Builtin_Impl_Stats_HandleApiCallAsFunctionEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL42Builtin_Impl_Stats_HandleApiCallAsFunctionEiPmPNS0_7IsolateE, @function
_ZN2v88internalL42Builtin_Impl_Stats_HandleApiCallAsFunctionEiPmPNS0_7IsolateE:
.LFB18739:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L283
.L255:
	movq	_ZZN2v88internalL42Builtin_Impl_Stats_HandleApiCallAsFunctionEiPmPNS0_7IsolateEE28trace_event_unique_atomic289(%rip), %rbx
	testq	%rbx, %rbx
	je	.L284
.L257:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L285
.L259:
	movq	%r12, %rdi
	movq	%r14, %rdx
	movq	%r13, %rcx
	xorl	%esi, %esi
	call	_ZN2v88internalL36HandleApiCallAsFunctionOrConstructorEPNS0_7IsolateEbNS0_16BuiltinArgumentsE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L286
.L254:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L287
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L284:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L288
.L258:
	movq	%rbx, _ZZN2v88internalL42Builtin_Impl_Stats_HandleApiCallAsFunctionEiPmPNS0_7IsolateEE28trace_event_unique_atomic289(%rip)
	jmp	.L257
	.p2align 4,,10
	.p2align 3
.L285:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L289
.L260:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L261
	movq	(%rdi), %rax
	call	*8(%rax)
.L261:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L262
	movq	(%rdi), %rax
	call	*8(%rax)
.L262:
	leaq	.LC5(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L286:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L283:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$670, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L289:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC5(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L288:
	leaq	.LC1(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L258
.L287:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18739:
	.size	_ZN2v88internalL42Builtin_Impl_Stats_HandleApiCallAsFunctionEiPmPNS0_7IsolateE, .-_ZN2v88internalL42Builtin_Impl_Stats_HandleApiCallAsFunctionEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL45Builtin_Impl_Stats_HandleApiCallAsConstructorEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"V8.Builtin_HandleApiCallAsConstructor"
	.section	.text._ZN2v88internalL45Builtin_Impl_Stats_HandleApiCallAsConstructorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL45Builtin_Impl_Stats_HandleApiCallAsConstructorEiPmPNS0_7IsolateE, @function
_ZN2v88internalL45Builtin_Impl_Stats_HandleApiCallAsConstructorEiPmPNS0_7IsolateE:
.LFB18742:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L319
.L291:
	movq	_ZZN2v88internalL45Builtin_Impl_Stats_HandleApiCallAsConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic295(%rip), %rbx
	testq	%rbx, %rbx
	je	.L320
.L293:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L321
.L295:
	movq	%r12, %rdi
	movq	%r14, %rdx
	movq	%r13, %rcx
	movl	$1, %esi
	call	_ZN2v88internalL36HandleApiCallAsFunctionOrConstructorEPNS0_7IsolateEbNS0_16BuiltinArgumentsE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L322
.L290:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L323
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L320:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L324
.L294:
	movq	%rbx, _ZZN2v88internalL45Builtin_Impl_Stats_HandleApiCallAsConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic295(%rip)
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L321:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L325
.L296:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L297
	movq	(%rdi), %rax
	call	*8(%rax)
.L297:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L298
	movq	(%rdi), %rax
	call	*8(%rax)
.L298:
	leaq	.LC6(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L322:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L319:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$671, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L325:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC6(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L324:
	leaq	.LC1(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L294
.L323:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18742:
	.size	_ZN2v88internalL45Builtin_Impl_Stats_HandleApiCallAsConstructorEiPmPNS0_7IsolateE, .-_ZN2v88internalL45Builtin_Impl_Stats_HandleApiCallAsConstructorEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal21Builtin_HandleApiCallEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal21Builtin_HandleApiCallEiPmPNS0_7IsolateE
	.type	_ZN2v88internal21Builtin_HandleApiCallEiPmPNS0_7IsolateE, @function
_ZN2v88internal21Builtin_HandleApiCallEiPmPNS0_7IsolateE:
.LFB18731:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L330
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL26Builtin_Impl_HandleApiCallENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L330:
	.cfi_restore 6
	jmp	_ZN2v88internalL32Builtin_Impl_Stats_HandleApiCallEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE18731:
	.size	_ZN2v88internal21Builtin_HandleApiCallEiPmPNS0_7IsolateE, .-_ZN2v88internal21Builtin_HandleApiCallEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal8Builtins17InvokeApiFunctionEPNS0_7IsolateEbNS0_6HandleINS0_10HeapObjectEEENS4_INS0_6ObjectEEEiPS8_S6_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins17InvokeApiFunctionEPNS0_7IsolateEbNS0_6HandleINS0_10HeapObjectEEENS4_INS0_6ObjectEEEiPS8_S6_
	.type	_ZN2v88internal8Builtins17InvokeApiFunctionEPNS0_7IsolateEbNS0_6HandleINS0_10HeapObjectEEENS4_INS0_6ObjectEEEiPS8_S6_, @function
_ZN2v88internal8Builtins17InvokeApiFunctionEPNS0_7IsolateEbNS0_6HandleINS0_10HeapObjectEEENS4_INS0_6ObjectEEEiPS8_S6_:
.LFB18737:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	movq	%rcx, %r8
	subq	$424, %rsp
	movq	%r9, -432(%rbp)
	movq	16(%rbp), %r10
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -416(%rbp)
	movq	$0, -384(%rbp)
	movaps	%xmm0, -400(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L373
.L332:
	testb	%r15b, %r15b
	jne	.L372
	movq	(%rcx), %rax
	testb	$1, %al
	jne	.L334
.L337:
	movq	(%r14), %rax
	movq	-1(%rax), %rdx
	cmpw	$88, 11(%rdx)
	je	.L335
	movq	23(%rax), %rdx
	movl	47(%rdx), %edx
	andl	$64, %edx
	jne	.L338
.L335:
	movq	%rcx, %rsi
	movq	%r12, %rdi
	movq	%r10, 16(%rbp)
	call	_ZN2v88internal6Object15ConvertReceiverEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L339
	movq	(%r14), %rax
	movq	16(%rbp), %r10
	movq	%r14, %r11
	movq	-1(%rax), %rdx
	cmpw	$88, 11(%rdx)
	jne	.L374
	.p2align 4,,10
	.p2align 3
.L343:
	leal	5(%rbx), %eax
	leaq	-320(%rbp), %rdx
	movslq	%eax, %rsi
	movq	%rdx, -424(%rbp)
	movq	%rdx, %r13
	leaq	0(,%rsi,8), %rdi
	cmpl	$32, %eax
	jg	.L375
.L347:
	movq	(%r8), %rax
	movq	%rax, -8(%r13,%rdi)
	testl	%ebx, %ebx
	jle	.L351
	movq	-432(%rbp), %r9
	leal	-1(%rbx), %ecx
	leaq	-16(%r13,%rdi), %rax
	leaq	8(%r9,%rcx,8), %rbx
	.p2align 4,,10
	.p2align 3
.L352:
	movq	(%r9), %rcx
	addq	$8, %r9
	subq	$8, %rax
	movq	(%rcx), %rcx
	movq	%rcx, 8(%rax)
	cmpq	%rbx, %r9
	jne	.L352
.L351:
	movq	%rsi, %rax
	movq	(%r10), %xmm1
	movq	%rsi, -344(%rbp)
	salq	$32, %rax
	movq	%r12, -360(%rbp)
	movq	%rax, %xmm0
	leaq	-8(%r13,%rdi), %rax
	movhps	(%r14), %xmm1
	movq	%rax, -336(%rbp)
	movq	41704(%r12), %rax
	movhps	96(%r12), %xmm0
	movups	%xmm1, 0(%r13)
	movq	%rax, -352(%rbp)
	leaq	-368(%rbp), %rax
	movq	%rax, 41704(%r12)
	leaq	16+_ZTVN2v88internal12_GLOBAL__N_120RelocatableArgumentsE(%rip), %rax
	movq	%rax, -368(%rbp)
	movups	%xmm0, 16(%r13)
	testb	%r15b, %r15b
	jne	.L349
	pushq	-336(%rbp)
	movq	%r10, %rdx
	movq	%r11, %rcx
	movq	%r14, %rsi
	pushq	-344(%rbp)
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_119HandleApiCallHelperILb0EEENS0_11MaybeHandleINS0_6ObjectEEEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEESA_NS8_INS0_20FunctionTemplateInfoEEENS8_IS4_EENS0_16BuiltinArgumentsE
	movq	%rax, %rbx
	popq	%rax
	popq	%rdx
.L353:
	movq	-360(%rbp), %rax
	movq	-352(%rbp), %rdx
	movq	%rdx, 41704(%rax)
	cmpq	-424(%rbp), %r13
	je	.L354
	movq	%r13, %rdi
	call	_ZdaPv@PLT
.L354:
	movq	%rbx, %rax
.L341:
	movq	-416(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L376
.L355:
	movq	-56(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L377
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L334:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L337
	.p2align 4,,10
	.p2align 3
.L372:
	movq	(%r14), %rax
.L338:
	movq	-1(%rax), %rdx
	movq	%r14, %r11
	cmpw	$88, 11(%rdx)
	je	.L343
.L374:
	movq	23(%rax), %rax
	movq	7(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L344
	movq	%r10, 16(%rbp)
	movq	%r8, -424(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-424(%rbp), %r8
	movq	16(%rbp), %r10
	movq	%rax, %rdx
.L345:
	movq	%rdx, %r11
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L349:
	pushq	-336(%rbp)
	movq	%r11, %rcx
	movq	%r14, %rsi
	movq	%r10, %rdx
	pushq	-344(%rbp)
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_119HandleApiCallHelperILb1EEENS0_11MaybeHandleINS0_6ObjectEEEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEESA_NS8_INS0_20FunctionTemplateInfoEEENS8_IS4_EENS0_16BuiltinArgumentsE
	popq	%rcx
	popq	%rsi
	movq	%rax, %rbx
	jmp	.L353
	.p2align 4,,10
	.p2align 3
.L375:
	movq	%rsi, -464(%rbp)
	movq	%r10, 16(%rbp)
	movq	%r11, -456(%rbp)
	movq	%r8, -448(%rbp)
	movq	%rdi, -440(%rbp)
	call	_Znam@PLT
	movq	-464(%rbp), %rsi
	movq	16(%rbp), %r10
	movq	-456(%rbp), %r11
	movq	-448(%rbp), %r8
	movq	%rax, %r13
	movq	-440(%rbp), %rdi
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L344:
	movq	41088(%r12), %rdx
	cmpq	41096(%r12), %rdx
	je	.L378
.L346:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdx)
	jmp	.L345
	.p2align 4,,10
	.p2align 3
.L376:
	leaq	-408(%rbp), %rsi
	movq	%rax, -424(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	movq	-424(%rbp), %rax
	jmp	.L355
	.p2align 4,,10
	.p2align 3
.L373:
	movq	40960(%rdi), %rax
	leaq	-408(%rbp), %rsi
	movl	$162, %edx
	movq	%rcx, -440(%rbp)
	movq	%rcx, -424(%rbp)
	leaq	23240(%rax), %rdi
	movq	%rdi, -416(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	movq	16(%rbp), %r10
	movq	-440(%rbp), %rcx
	movq	-424(%rbp), %r8
	jmp	.L332
.L378:
	movq	%r12, %rdi
	movq	%r10, 16(%rbp)
	movq	%rsi, -440(%rbp)
	movq	%r8, -424(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	16(%rbp), %r10
	movq	-440(%rbp), %rsi
	movq	-424(%rbp), %r8
	movq	%rax, %rdx
	jmp	.L346
.L339:
	xorl	%eax, %eax
	jmp	.L341
.L377:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18737:
	.size	_ZN2v88internal8Builtins17InvokeApiFunctionEPNS0_7IsolateEbNS0_6HandleINS0_10HeapObjectEEENS4_INS0_6ObjectEEEiPS8_S6_, .-_ZN2v88internal8Builtins17InvokeApiFunctionEPNS0_7IsolateEbNS0_6HandleINS0_10HeapObjectEEENS4_INS0_6ObjectEEEiPS8_S6_
	.section	.text._ZN2v88internal31Builtin_HandleApiCallAsFunctionEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal31Builtin_HandleApiCallAsFunctionEiPmPNS0_7IsolateE
	.type	_ZN2v88internal31Builtin_HandleApiCallAsFunctionEiPmPNS0_7IsolateE, @function
_ZN2v88internal31Builtin_HandleApiCallAsFunctionEiPmPNS0_7IsolateE:
.LFB18740:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsi, %rcx
	movq	%rdx, %r8
	testl	%eax, %eax
	jne	.L383
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdx
	xorl	%esi, %esi
	movq	%r8, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL36HandleApiCallAsFunctionOrConstructorEPNS0_7IsolateEbNS0_16BuiltinArgumentsE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L383:
	.cfi_restore 6
	jmp	_ZN2v88internalL42Builtin_Impl_Stats_HandleApiCallAsFunctionEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE18740:
	.size	_ZN2v88internal31Builtin_HandleApiCallAsFunctionEiPmPNS0_7IsolateE, .-_ZN2v88internal31Builtin_HandleApiCallAsFunctionEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal34Builtin_HandleApiCallAsConstructorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal34Builtin_HandleApiCallAsConstructorEiPmPNS0_7IsolateE
	.type	_ZN2v88internal34Builtin_HandleApiCallAsConstructorEiPmPNS0_7IsolateE, @function
_ZN2v88internal34Builtin_HandleApiCallAsConstructorEiPmPNS0_7IsolateE:
.LFB18743:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsi, %rcx
	movq	%rdx, %r8
	testl	%eax, %eax
	jne	.L388
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdx
	movl	$1, %esi
	movq	%r8, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL36HandleApiCallAsFunctionOrConstructorEPNS0_7IsolateEbNS0_16BuiltinArgumentsE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L388:
	.cfi_restore 6
	jmp	_ZN2v88internalL45Builtin_Impl_Stats_HandleApiCallAsConstructorEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE18743:
	.size	_ZN2v88internal34Builtin_HandleApiCallAsConstructorEiPmPNS0_7IsolateE, .-_ZN2v88internal34Builtin_HandleApiCallAsConstructorEiPmPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal21Builtin_HandleApiCallEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal21Builtin_HandleApiCallEiPmPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal21Builtin_HandleApiCallEiPmPNS0_7IsolateE:
.LFB23080:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE23080:
	.size	_GLOBAL__sub_I__ZN2v88internal21Builtin_HandleApiCallEiPmPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal21Builtin_HandleApiCallEiPmPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal21Builtin_HandleApiCallEiPmPNS0_7IsolateE
	.section	.data.rel.ro.local._ZTVN2v88internal12_GLOBAL__N_120RelocatableArgumentsE,"aw"
	.align 8
	.type	_ZTVN2v88internal12_GLOBAL__N_120RelocatableArgumentsE, @object
	.size	_ZTVN2v88internal12_GLOBAL__N_120RelocatableArgumentsE, 48
_ZTVN2v88internal12_GLOBAL__N_120RelocatableArgumentsE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal12_GLOBAL__N_120RelocatableArgumentsD1Ev
	.quad	_ZN2v88internal12_GLOBAL__N_120RelocatableArgumentsD0Ev
	.quad	_ZN2v88internal12_GLOBAL__N_120RelocatableArguments15IterateInstanceEPNS0_11RootVisitorE
	.quad	_ZN2v88internal11Relocatable21PostGarbageCollectionEv
	.section	.bss._ZZN2v88internalL45Builtin_Impl_Stats_HandleApiCallAsConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic295,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL45Builtin_Impl_Stats_HandleApiCallAsConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic295, @object
	.size	_ZZN2v88internalL45Builtin_Impl_Stats_HandleApiCallAsConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic295, 8
_ZZN2v88internalL45Builtin_Impl_Stats_HandleApiCallAsConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic295:
	.zero	8
	.section	.bss._ZZN2v88internalL42Builtin_Impl_Stats_HandleApiCallAsFunctionEiPmPNS0_7IsolateEE28trace_event_unique_atomic289,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL42Builtin_Impl_Stats_HandleApiCallAsFunctionEiPmPNS0_7IsolateEE28trace_event_unique_atomic289, @object
	.size	_ZZN2v88internalL42Builtin_Impl_Stats_HandleApiCallAsFunctionEiPmPNS0_7IsolateEE28trace_event_unique_atomic289, 8
_ZZN2v88internalL42Builtin_Impl_Stats_HandleApiCallAsFunctionEiPmPNS0_7IsolateEE28trace_event_unique_atomic289:
	.zero	8
	.section	.bss._ZZN2v88internalL32Builtin_Impl_Stats_HandleApiCallEiPmPNS0_7IsolateEE28trace_event_unique_atomic129,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL32Builtin_Impl_Stats_HandleApiCallEiPmPNS0_7IsolateEE28trace_event_unique_atomic129, @object
	.size	_ZZN2v88internalL32Builtin_Impl_Stats_HandleApiCallEiPmPNS0_7IsolateEE28trace_event_unique_atomic129, 8
_ZZN2v88internalL32Builtin_Impl_Stats_HandleApiCallEiPmPNS0_7IsolateEE28trace_event_unique_atomic129:
	.zero	8
	.weak	_ZZN2v88internal21ExternalCallbackScopeD4EvE27trace_event_unique_atomic68
	.section	.bss._ZZN2v88internal21ExternalCallbackScopeD4EvE27trace_event_unique_atomic68,"awG",@nobits,_ZZN2v88internal21ExternalCallbackScopeD4EvE27trace_event_unique_atomic68,comdat
	.align 8
	.type	_ZZN2v88internal21ExternalCallbackScopeD4EvE27trace_event_unique_atomic68, @gnu_unique_object
	.size	_ZZN2v88internal21ExternalCallbackScopeD4EvE27trace_event_unique_atomic68, 8
_ZZN2v88internal21ExternalCallbackScopeD4EvE27trace_event_unique_atomic68:
	.zero	8
	.weak	_ZZN2v88internal21ExternalCallbackScopeC4EPNS0_7IsolateEmE27trace_event_unique_atomic62
	.section	.bss._ZZN2v88internal21ExternalCallbackScopeC4EPNS0_7IsolateEmE27trace_event_unique_atomic62,"awG",@nobits,_ZZN2v88internal21ExternalCallbackScopeC4EPNS0_7IsolateEmE27trace_event_unique_atomic62,comdat
	.align 8
	.type	_ZZN2v88internal21ExternalCallbackScopeC4EPNS0_7IsolateEmE27trace_event_unique_atomic62, @gnu_unique_object
	.size	_ZZN2v88internal21ExternalCallbackScopeC4EPNS0_7IsolateEmE27trace_event_unique_atomic62, 8
_ZZN2v88internal21ExternalCallbackScopeC4EPNS0_7IsolateEmE27trace_event_unique_atomic62:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
