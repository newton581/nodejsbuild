	.file	"uri.cc"
	.text
	.section	.text._ZN2v88internal12StringSearchIhhE16SingleCharSearchEPS2_NS0_6VectorIKhEEi,"axG",@progbits,_ZN2v88internal12StringSearchIhhE16SingleCharSearchEPS2_NS0_6VectorIKhEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIhhE16SingleCharSearchEPS2_NS0_6VectorIKhEEi
	.type	_ZN2v88internal12StringSearchIhhE16SingleCharSearchEPS2_NS0_6VectorIKhEEi, @function
_ZN2v88internal12StringSearchIhhE16SingleCharSearchEPS2_NS0_6VectorIKhEEi:
.LFB21399:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	subl	16(%rdi), %r13d
	leal	1(%r13), %ebx
	movzbl	(%rax), %r15d
	movl	%r15d, %r14d
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L8:
	subq	%r12, %rax
	movslq	%eax, %rdx
	movl	%eax, %r8d
	cmpb	(%r12,%rdx), %r14b
	je	.L1
	leal	1(%rax), %ecx
	cmpl	%eax, %r13d
	jle	.L4
.L3:
	movl	%ebx, %edx
	movl	%r15d, %esi
	subl	%ecx, %edx
	movslq	%ecx, %rcx
	movslq	%edx, %rdx
	leaq	(%r12,%rcx), %rdi
	call	memchr@PLT
	testq	%rax, %rax
	jne	.L8
.L4:
	movl	$-1, %r8d
.L1:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21399:
	.size	_ZN2v88internal12StringSearchIhhE16SingleCharSearchEPS2_NS0_6VectorIKhEEi, .-_ZN2v88internal12StringSearchIhhE16SingleCharSearchEPS2_NS0_6VectorIKhEEi
	.section	.text._ZN2v88internal12StringSearchIhtE16SingleCharSearchEPS2_NS0_6VectorIKtEEi,"axG",@progbits,_ZN2v88internal12StringSearchIhtE16SingleCharSearchEPS2_NS0_6VectorIKtEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIhtE16SingleCharSearchEPS2_NS0_6VectorIKtEEi
	.type	_ZN2v88internal12StringSearchIhtE16SingleCharSearchEPS2_NS0_6VectorIKtEEi, @function
_ZN2v88internal12StringSearchIhtE16SingleCharSearchEPS2_NS0_6VectorIKtEEi:
.LFB21412:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	subl	16(%rdi), %r14d
	leal	1(%r14), %ebx
	movzbl	(%rax), %r15d
	movl	%r15d, %r13d
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L15:
	andq	$-2, %rax
	subq	%r12, %rax
	sarq	%rax
	movslq	%eax, %rdx
	movl	%eax, %r8d
	cmpw	(%r12,%rdx,2), %r13w
	je	.L9
	leal	1(%rax), %ecx
	cmpl	%eax, %r14d
	jle	.L12
.L11:
	movl	%ebx, %edx
	movl	%r15d, %esi
	subl	%ecx, %edx
	movslq	%ecx, %rcx
	movslq	%edx, %rdx
	leaq	(%r12,%rcx,2), %rdi
	addq	%rdx, %rdx
	call	memchr@PLT
	testq	%rax, %rax
	jne	.L15
.L12:
	movl	$-1, %r8d
.L9:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21412:
	.size	_ZN2v88internal12StringSearchIhtE16SingleCharSearchEPS2_NS0_6VectorIKtEEi, .-_ZN2v88internal12StringSearchIhtE16SingleCharSearchEPS2_NS0_6VectorIKtEEi
	.section	.text._ZN7unibrow4Utf86EncodeEPcjib.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN7unibrow4Utf86EncodeEPcjib.part.0, @function
_ZN7unibrow4Utf86EncodeEPcjib.part.0:
.LFB21845:
	.cfi_startproc
	sall	$10, %edx
	andl	$1023, %esi
	andl	$1047552, %edx
	orl	%esi, %edx
	leal	65536(%rdx), %esi
	movl	%esi, %eax
	shrl	$18, %eax
	orl	$-16, %eax
	movb	%al, -3(%rdi)
	movl	%esi, %eax
	shrl	$12, %eax
	andl	$63, %eax
	orl	$-128, %eax
	movb	%al, -2(%rdi)
	movl	%esi, %eax
	andl	$63, %esi
	shrl	$6, %eax
	orl	$-128, %esi
	andl	$63, %eax
	movb	%sil, (%rdi)
	orl	$-128, %eax
	movb	%al, -1(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE21845:
	.size	_ZN7unibrow4Utf86EncodeEPcjib.part.0, .-_ZN7unibrow4Utf86EncodeEPcjib.part.0
	.section	.text._ZN2v88internal12_GLOBAL__N_112UnescapeCharItEEiNS0_6VectorIKT_EEiiPi.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_112UnescapeCharItEEiNS0_6VectorIKT_EEiiPi.isra.0, @function
_ZN2v88internal12_GLOBAL__N_112UnescapeCharItEEiNS0_6VectorIKT_EEiiPi.isra.0:
.LFB21884:
	.cfi_startproc
	movslq	%esi, %rax
	movzwl	(%rdi,%rax,2), %r8d
	leaq	(%rax,%rax), %r10
	cmpw	$37, %r8w
	je	.L45
.L18:
	movl	$1, (%rcx)
	movzwl	%r8w, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	leal	-5(%rdx), %eax
	cmpl	%eax, %esi
	jge	.L19
	cmpw	$117, 2(%rdi,%r10)
	jne	.L19
	movzwl	6(%rdi,%r10), %edx
	movzwl	4(%rdi,%r10), %eax
	movl	%edx, %r9d
	cmpw	$102, %ax
	ja	.L18
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L20
	orl	$32, %eax
	movl	%eax, %esi
	leal	-49(%rax), %eax
	cmpl	$5, %eax
	ja	.L18
	leal	-39(%rsi), %eax
	cmpl	$38, %esi
	je	.L18
.L20:
	cmpw	$102, %r9w
	ja	.L18
	subl	$48, %edx
	cmpl	$9, %edx
	jbe	.L21
	movl	%edx, %esi
	orl	$32, %esi
	leal	-49(%rsi), %edx
	cmpl	$5, %edx
	ja	.L18
	leal	-39(%rsi), %edx
	cmpl	$38, %esi
	je	.L18
.L21:
	sall	$4, %eax
	addl	%edx, %eax
	js	.L18
	movzwl	10(%rdi,%r10), %esi
	movzwl	8(%rdi,%r10), %edx
	movl	%esi, %r9d
	cmpw	$102, %dx
	ja	.L18
	subl	$48, %edx
	cmpl	$9, %edx
	jbe	.L22
	movl	%edx, %edi
	orl	$32, %edi
	leal	-49(%rdi), %edx
	cmpl	$5, %edx
	ja	.L18
	leal	-39(%rdi), %edx
	cmpl	$38, %edi
	je	.L18
.L22:
	cmpw	$102, %r9w
	ja	.L18
	subl	$48, %esi
	cmpl	$9, %esi
	jbe	.L23
	movl	%esi, %edi
	orl	$32, %edi
	leal	-49(%rdi), %esi
	cmpl	$5, %esi
	ja	.L18
	leal	-39(%rdi), %esi
	cmpl	$38, %edi
	je	.L18
.L23:
	sall	$4, %edx
	addl	%esi, %edx
	js	.L18
	sall	$8, %eax
	movl	$6, (%rcx)
	addl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	subl	$2, %edx
	cmpl	%edx, %esi
	jge	.L18
	movzwl	4(%rdi,%r10), %edx
	movzwl	2(%rdi,%r10), %eax
	movl	%edx, %r9d
	cmpw	$102, %ax
	ja	.L18
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L25
	orl	$32, %eax
	movl	%eax, %esi
	leal	-49(%rax), %eax
	cmpl	$5, %eax
	ja	.L18
	leal	-39(%rsi), %eax
	cmpl	$38, %esi
	je	.L18
.L25:
	cmpw	$102, %r9w
	ja	.L18
	subl	$48, %edx
	cmpl	$9, %edx
	jbe	.L26
	orl	$32, %edx
	movl	%edx, %esi
	leal	-49(%rdx), %edx
	cmpl	$5, %edx
	ja	.L18
	leal	-39(%rsi), %edx
	cmpl	$38, %esi
	je	.L18
.L26:
	sall	$4, %eax
	addl	%edx, %eax
	js	.L18
	movl	$3, (%rcx)
	ret
	.cfi_endproc
.LFE21884:
	.size	_ZN2v88internal12_GLOBAL__N_112UnescapeCharItEEiNS0_6VectorIKT_EEiiPi.isra.0, .-_ZN2v88internal12_GLOBAL__N_112UnescapeCharItEEiNS0_6VectorIKT_EEiiPi.isra.0
	.section	.text._ZN2v88internal12_GLOBAL__N_112UnescapeCharIhEEiNS0_6VectorIKT_EEiiPi.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_112UnescapeCharIhEEiNS0_6VectorIKT_EEiiPi.isra.0, @function
_ZN2v88internal12_GLOBAL__N_112UnescapeCharIhEEiNS0_6VectorIKT_EEiiPi.isra.0:
.LFB21882:
	.cfi_startproc
	movslq	%esi, %rax
	movzbl	(%rdi,%rax), %r8d
	cmpb	$37, %r8b
	je	.L74
.L47:
	movl	$1, (%rcx)
	movzbl	%r8b, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	leal	-5(%rdx), %eax
	cmpl	%eax, %esi
	jge	.L48
	leal	1(%rsi), %eax
	cltq
	cmpb	$117, (%rdi,%rax)
	jne	.L48
	leal	3(%rsi), %eax
	cltq
	movzbl	(%rdi,%rax), %edx
	leal	2(%rsi), %eax
	cltq
	movzbl	(%rdi,%rax), %eax
	movl	%edx, %r10d
	cmpb	$102, %al
	ja	.L47
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L49
	orl	$32, %eax
	movl	%eax, %r9d
	leal	-49(%rax), %eax
	cmpl	$5, %eax
	ja	.L47
	leal	-39(%r9), %eax
	cmpl	$38, %r9d
	je	.L47
.L49:
	cmpb	$102, %r10b
	ja	.L47
	subl	$48, %edx
	cmpl	$9, %edx
	jbe	.L50
	orl	$32, %edx
	movl	%edx, %r9d
	leal	-49(%rdx), %edx
	cmpl	$5, %edx
	ja	.L47
	leal	-39(%r9), %edx
	cmpl	$38, %r9d
	je	.L47
.L50:
	sall	$4, %eax
	addl	%edx, %eax
	js	.L47
	leal	5(%rsi), %edx
	addl	$4, %esi
	movslq	%edx, %rdx
	movslq	%esi, %rsi
	movzbl	(%rdi,%rdx), %r9d
	movzbl	(%rdi,%rsi), %edx
	movl	%r9d, %r10d
	cmpb	$102, %dl
	ja	.L47
	subl	$48, %edx
	cmpl	$9, %edx
	jbe	.L51
	orl	$32, %edx
	movl	%edx, %esi
	leal	-49(%rdx), %edx
	cmpl	$5, %edx
	ja	.L47
	leal	-39(%rsi), %edx
	cmpl	$38, %esi
	je	.L47
.L51:
	cmpb	$102, %r10b
	ja	.L47
	leal	-48(%r9), %esi
	cmpl	$9, %esi
	jbe	.L52
	movl	%esi, %edi
	orl	$32, %edi
	leal	-49(%rdi), %esi
	cmpl	$5, %esi
	ja	.L47
	leal	-39(%rdi), %esi
	cmpl	$38, %edi
	je	.L47
.L52:
	sall	$4, %edx
	addl	%esi, %edx
	js	.L47
	sall	$8, %eax
	movl	$6, (%rcx)
	addl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	subl	$2, %edx
	cmpl	%edx, %esi
	jge	.L47
	leal	2(%rsi), %eax
	addl	$1, %esi
	cltq
	movslq	%esi, %rsi
	movzbl	(%rdi,%rax), %edx
	movzbl	(%rdi,%rsi), %eax
	movl	%edx, %r9d
	cmpb	$102, %al
	ja	.L47
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L54
	orl	$32, %eax
	movl	%eax, %esi
	leal	-49(%rax), %eax
	cmpl	$5, %eax
	ja	.L47
	leal	-39(%rsi), %eax
	cmpl	$38, %esi
	je	.L47
.L54:
	cmpb	$102, %r9b
	ja	.L47
	subl	$48, %edx
	cmpl	$9, %edx
	jbe	.L55
	orl	$32, %edx
	movl	%edx, %esi
	leal	-49(%rdx), %edx
	cmpl	$5, %edx
	ja	.L47
	leal	-39(%rsi), %edx
	cmpl	$38, %esi
	je	.L47
.L55:
	sall	$4, %eax
	addl	%edx, %eax
	js	.L47
	movl	$3, (%rcx)
	ret
	.cfi_endproc
.LFE21882:
	.size	_ZN2v88internal12_GLOBAL__N_112UnescapeCharIhEEiNS0_6VectorIKT_EEiiPi.isra.0, .-_ZN2v88internal12_GLOBAL__N_112UnescapeCharIhEEiNS0_6VectorIKT_EEiiPi.isra.0
	.section	.text._ZN2v88internal3Uri6EscapeEPNS0_7IsolateENS0_6HandleINS0_6StringEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Uri6EscapeEPNS0_7IsolateENS0_6HandleINS0_6StringEEE
	.type	_ZN2v88internal3Uri6EscapeEPNS0_7IsolateENS0_6HandleINS0_6StringEEE, @function
_ZN2v88internal3Uri6EscapeEPNS0_7IsolateENS0_6HandleINS0_6StringEEE:
.LFB17861:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	%rax, %rsi
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L152
.L77:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	jbe	.L86
.L150:
	movq	(%r12), %rsi
.L94:
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andw	$9, %ax
	je	.L92
.L153:
	cmpw	$8, %ax
	je	.L93
	movq	15(%rsi), %rsi
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andw	$9, %ax
	jne	.L153
.L92:
	movq	(%r12), %rax
	leaq	-65(%rbp), %r15
	leaq	-64(%rbp), %r14
	movq	%r15, %rsi
	movq	%r14, %rdi
	movl	11(%rax), %ebx
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	testl	%ebx, %ebx
	jle	.L154
	leal	-1(%rbx), %edx
	xorl	%esi, %esi
	movl	$1, %r8d
	movabsq	$9007199258935355, %rdi
	leaq	2(%rax,%rdx,2), %r9
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L155:
	addl	$6, %esi
.L117:
	cmpl	$1073741799, %esi
	jg	.L95
.L156:
	addq	$2, %rax
	cmpq	%r9, %rax
	je	.L95
.L120:
	movzwl	(%rax), %ecx
	cmpw	$255, %cx
	ja	.L155
	movl	%ecx, %edx
	orl	$32, %edx
	movzwl	%dx, %edx
	subl	$97, %edx
	cmpl	$25, %edx
	jbe	.L118
	movzwl	%cx, %edx
	subl	$48, %edx
	cmpl	$9, %edx
	jbe	.L118
	subl	$42, %ecx
	cmpw	$53, %cx
	ja	.L119
	movq	%r8, %rdx
	salq	%cl, %rdx
	testq	%rdi, %rdx
	jne	.L118
.L119:
	addl	$3, %esi
	cmpl	$1073741799, %esi
	jle	.L156
	.p2align 4,,10
	.p2align 3
.L95:
	cmpl	%esi, %ebx
	je	.L115
	movq	%r13, %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory19NewRawOneByteStringEiNS0_14AllocationTypeE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L151
	movq	(%r12), %rax
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	testl	%ebx, %ebx
	jle	.L141
	leal	-1(%rbx), %edx
	xorl	%edi, %edi
	movl	$1, %r9d
	movabsq	$9007199258935355, %r8
	leaq	2(%rax,%rdx,2), %r10
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L157:
	movb	$37, (%r11)
	leal	17(%rdi), %ecx
	movq	0(%r13), %r11
	movslq	%ecx, %rcx
	movb	$117, -1(%rcx,%r11)
	movl	%esi, %ecx
	movq	0(%r13), %rbx
	sarl	$12, %ecx
	cmpl	$9, %ecx
	leal	48(%rcx), %r12d
	leal	55(%rcx), %r11d
	cmovle	%r12d, %r11d
	leal	18(%rdi), %ecx
	movslq	%ecx, %rcx
	movb	%r11b, -1(%rbx,%rcx)
	movl	%esi, %ecx
	movq	0(%r13), %rbx
	sarl	$8, %ecx
	andl	$15, %ecx
	cmpl	$9, %ecx
	leal	48(%rcx), %r12d
	leal	55(%rcx), %r11d
	cmovle	%r12d, %r11d
	leal	19(%rdi), %ecx
	sarl	$4, %esi
	andl	$15, %esi
	movslq	%ecx, %rcx
	movb	%r11b, -1(%rbx,%rcx)
	cmpl	$9, %esi
	leal	55(%rsi), %ecx
	leal	48(%rsi), %ebx
	cmovg	%ecx, %ebx
	movq	0(%r13), %r11
	leal	20(%rdi), %ecx
	andl	$15, %edx
	movslq	%ecx, %rcx
	cmpw	$9, %dx
	movb	%bl, -1(%r11,%rcx)
	leal	48(%rdx), %r11d
	leal	55(%rdx), %ecx
	movq	0(%r13), %rsi
	cmovbe	%r11d, %ecx
	leal	21(%rdi), %edx
	addl	$6, %edi
	movslq	%edx, %rdx
	movb	%cl, -1(%rsi,%rdx)
.L133:
	addq	$2, %rax
	cmpq	%r10, %rax
	je	.L141
.L142:
	movzwl	(%rax), %esi
	movq	0(%r13), %r11
	leal	16(%rdi), %ecx
	movslq	%ecx, %rcx
	movl	%esi, %edx
	leaq	-1(%r11,%rcx), %r11
	cmpw	$255, %si
	ja	.L157
	movl	%esi, %ecx
	orl	$32, %ecx
	movzwl	%cx, %ecx
	subl	$97, %ecx
	cmpl	$25, %ecx
	jbe	.L134
	leal	-48(%rsi), %ecx
	cmpl	$9, %ecx
	jbe	.L134
	leal	-42(%rsi), %ecx
	cmpw	$53, %cx
	ja	.L135
	movq	%r9, %rbx
	salq	%cl, %rbx
	testq	%r8, %rbx
	jne	.L134
.L135:
	sarl	$4, %esi
	movb	$37, (%r11)
	movq	0(%r13), %r11
	cmpl	$10, %esi
	leal	48(%rsi), %ecx
	leal	55(%rsi), %ebx
	cmovl	%ecx, %ebx
	leal	17(%rdi), %ecx
	andl	$15, %edx
	movslq	%ecx, %rcx
	cmpw	$9, %dx
	movb	%bl, -1(%r11,%rcx)
	leal	48(%rdx), %r11d
	leal	55(%rdx), %ecx
	movq	0(%r13), %rsi
	cmovbe	%r11d, %ecx
	leal	18(%rdi), %edx
	addq	$2, %rax
	addl	$3, %edi
	movslq	%edx, %rdx
	movb	%cl, -1(%rsi,%rdx)
	cmpq	%r10, %rax
	jne	.L142
	.p2align 4,,10
	.p2align 3
.L141:
	movq	%r13, %r12
.L115:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L158
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L86:
	.cfi_restore_state
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	jne	.L150
	movq	(%r12), %rax
	movq	41112(%r13), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L89
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r12
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L93:
	movq	(%r12), %rax
	leaq	-65(%rbp), %r15
	leaq	-64(%rbp), %r14
	movq	%r15, %rsi
	movq	%r14, %rdi
	movl	11(%rax), %ebx
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	testl	%ebx, %ebx
	jle	.L145
	leal	-1(%rbx), %edx
	xorl	%esi, %esi
	movl	$1, %r8d
	movabsq	$9007199258935355, %rdi
	leaq	1(%rax,%rdx), %r9
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L159:
	movzbl	%cl, %edx
	subl	$48, %edx
	cmpl	$9, %edx
	jbe	.L98
	subl	$42, %ecx
	cmpb	$53, %cl
	ja	.L99
	movq	%r8, %rdx
	salq	%cl, %rdx
	testq	%rdi, %rdx
	jne	.L98
.L99:
	addl	$3, %esi
	cmpl	$1073741799, %esi
	jg	.L97
	.p2align 4,,10
	.p2align 3
.L160:
	addq	$1, %rax
	cmpq	%rax, %r9
	je	.L97
.L101:
	movzbl	(%rax), %ecx
	movl	%ecx, %edx
	orl	$32, %edx
	movzbl	%dl, %edx
	subl	$97, %edx
	cmpl	$25, %edx
	ja	.L159
.L98:
	addl	$1, %esi
	cmpl	$1073741799, %esi
	jle	.L160
.L97:
	cmpl	%esi, %ebx
	je	.L115
	movq	%r13, %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory19NewRawOneByteStringEiNS0_14AllocationTypeE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L151
	movq	(%r12), %rax
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	testl	%ebx, %ebx
	jle	.L141
	leal	-1(%rbx), %edx
	xorl	%edi, %edi
	movl	$1, %r9d
	movabsq	$9007199258935355, %r8
	leaq	1(%rax,%rdx), %r10
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L161:
	movzbl	%sil, %edx
	leal	-48(%rdx), %ecx
	cmpl	$9, %ecx
	jbe	.L105
	leal	-42(%rsi), %ecx
	cmpb	$53, %cl
	ja	.L106
	movq	%r9, %rbx
	salq	%cl, %rbx
	testq	%r8, %rbx
	jne	.L105
.L106:
	sarl	$4, %edx
	movb	$37, (%r11)
	movq	0(%r13), %r11
	cmpl	$10, %edx
	leal	55(%rdx), %ebx
	leal	48(%rdx), %ecx
	cmovge	%ebx, %ecx
	leal	17(%rdi), %edx
	andl	$15, %esi
	movslq	%edx, %rdx
	cmpb	$9, %sil
	movb	%cl, -1(%r11,%rdx)
	leal	48(%rsi), %edx
	leal	55(%rsi), %ecx
	movq	0(%r13), %r11
	cmovbe	%edx, %ecx
	leal	18(%rdi), %edx
	addl	$3, %edi
	movslq	%edx, %rdx
	movb	%cl, -1(%r11,%rdx)
	.p2align 4,,10
	.p2align 3
.L109:
	addq	$1, %rax
	cmpq	%r10, %rax
	je	.L141
.L114:
	movzbl	(%rax), %esi
	movq	0(%r13), %rcx
	leal	16(%rdi), %edx
	movslq	%edx, %rdx
	leaq	-1(%rcx,%rdx), %r11
	movl	%esi, %edx
	orl	$32, %edx
	movzbl	%dl, %edx
	subl	$97, %edx
	cmpl	$25, %edx
	ja	.L161
.L105:
	movb	%sil, (%r11)
	addl	$1, %edi
	jmp	.L109
.L118:
	addl	$1, %esi
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L152:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L77
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L80
	movq	23(%rax), %rdx
	movl	11(%rdx), %edx
	testl	%edx, %edx
	je	.L80
	movq	%r12, %rsi
	xorl	%edx, %edx
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	movq	(%rax), %rsi
	movq	%rax, %r12
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L89:
	movq	41088(%r13), %r12
	cmpq	41096(%r13), %r12
	je	.L162
.L91:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r12)
	jmp	.L94
.L134:
	movb	%dl, (%r11)
	addl	$1, %edi
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L80:
	movq	41112(%r13), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L82
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r12
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L151:
	xorl	%r12d, %r12d
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L82:
	movq	41088(%r13), %r12
	cmpq	41096(%r13), %r12
	je	.L163
.L84:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r12)
	jmp	.L77
.L163:
	movq	%r13, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L154:
	xorl	%esi, %esi
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L145:
	xorl	%esi, %esi
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L162:
	movq	%r13, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L91
.L158:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17861:
	.size	_ZN2v88internal3Uri6EscapeEPNS0_7IsolateENS0_6HandleINS0_6StringEEE, .-_ZN2v88internal3Uri6EscapeEPNS0_7IsolateENS0_6HandleINS0_6StringEEE
	.section	.rodata._ZN2v88internal3Uri8UnescapeEPNS0_7IsolateENS0_6HandleINS0_6StringEEE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%"
.LC1:
	.string	"(location_) != nullptr"
.LC2:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal3Uri8UnescapeEPNS0_7IsolateENS0_6HandleINS0_6StringEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Uri8UnescapeEPNS0_7IsolateENS0_6HandleINS0_6StringEEE
	.type	_ZN2v88internal3Uri8UnescapeEPNS0_7IsolateENS0_6HandleINS0_6StringEEE, @function
_ZN2v88internal3Uri8UnescapeEPNS0_7IsolateENS0_6HandleINS0_6StringEEE:
.LFB17862:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	%rax, %rsi
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L256
.L166:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	jbe	.L175
.L250:
	movq	(%r12), %rsi
.L183:
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andw	$9, %ax
	je	.L181
.L257:
	cmpw	$8, %ax
	je	.L182
	movq	15(%rsi), %rsi
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andw	$9, %ax
	jne	.L257
.L181:
	movq	-120(%rbp), %rax
	movq	$1, -80(%rbp)
	leaq	-104(%rbp), %r15
	leaq	-105(%rbp), %rsi
	movl	$0, -64(%rbp)
	movq	%r15, %rdi
	leaq	-96(%rbp), %r13
	movq	%rax, -96(%rbp)
	leaq	.LC0(%rip), %rax
	movq	%rax, -88(%rbp)
	leaq	_ZN2v88internal12StringSearchIhtE16SingleCharSearchEPS2_NS0_6VectorIKtEEi(%rip), %rax
	movq	%rax, -72(%rbp)
	movq	(%r12), %rax
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	movq	%rax, %rsi
	movslq	%edx, %rdx
	call	*-72(%rbp)
	movl	%eax, %ebx
	testl	%eax, %eax
	js	.L200
	movq	(%r12), %rax
	movq	%r15, %rsi
	movq	%r13, %rdi
	movl	11(%rax), %r14d
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	cmpl	%r14d, %ebx
	jge	.L202
	xorl	%ecx, %ecx
	movl	$1, %r10d
	movq	%r12, -128(%rbp)
	movl	%ebx, %r11d
	movl	%ebx, -136(%rbp)
	movq	%rax, %r12
	movl	%ecx, %ebx
	movq	%r13, %rcx
	movl	%r10d, %r13d
	.p2align 4,,10
	.p2align 3
.L205:
	movl	%r11d, %esi
	movl	%r14d, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_112UnescapeCharItEEiNS0_6VectorIKT_EEiiPi.isra.0
	cmpl	$255, %eax
	movl	$0, %eax
	cmovg	%eax, %r13d
	addl	-96(%rbp), %r11d
	addl	$1, %ebx
	cmpl	%r11d, %r14d
	jg	.L205
	movl	%ebx, -140(%rbp)
	movq	-128(%rbp), %r12
	movl	%r13d, %r10d
	xorl	%edx, %edx
	movl	-136(%rbp), %ebx
	movq	-120(%rbp), %rdi
	movq	%rcx, %r13
	movb	%r10b, -136(%rbp)
	movq	%r12, %rsi
	movl	%ebx, %ecx
	call	_ZN2v88internal7Factory18NewProperSubStringENS0_6HandleINS0_6StringEEEii@PLT
	movzbl	-136(%rbp), %r10d
	movq	%rax, -128(%rbp)
	testb	%r10b, %r10b
	jne	.L258
	movl	-140(%rbp), %esi
	movq	-120(%rbp), %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory19NewRawTwoByteStringEiNS0_14AllocationTypeE@PLT
	movq	%rax, -136(%rbp)
	testq	%rax, %rax
	je	.L198
	movq	(%r12), %rax
	movq	%r13, %rdi
	movq	%r15, %rsi
	movl	$16, %r12d
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	movq	-136(%rbp), %r11
	movq	%r13, %rcx
	movq	%rax, %r13
	.p2align 4,,10
	.p2align 3
.L211:
	movq	(%r11), %r15
	movl	%ebx, %esi
	movl	%r14d, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal12_GLOBAL__N_112UnescapeCharItEEiNS0_6VectorIKT_EEiiPi.isra.0
	movw	%ax, -1(%r15,%r12)
	addl	-96(%rbp), %ebx
	addq	$2, %r12
	cmpl	%ebx, %r14d
	jg	.L211
.L252:
	movq	-136(%rbp), %rdx
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdi
	call	_ZN2v88internal7Factory13NewConsStringENS0_6HandleINS0_6StringEEES4_@PLT
	movq	%rax, %r12
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L175:
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	jne	.L250
	movq	(%r12), %rax
	movq	15(%rax), %rsi
	movq	-120(%rbp), %rax
	movq	41112(%rax), %rdi
	testq	%rdi, %rdi
	je	.L178
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r12
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L182:
	movq	-120(%rbp), %rax
	movq	$1, -80(%rbp)
	leaq	-104(%rbp), %r15
	leaq	-105(%rbp), %rsi
	movl	$0, -64(%rbp)
	movq	%r15, %rdi
	leaq	-96(%rbp), %r13
	movq	%rax, -96(%rbp)
	leaq	.LC0(%rip), %rax
	movq	%rax, -88(%rbp)
	leaq	_ZN2v88internal12StringSearchIhhE16SingleCharSearchEPS2_NS0_6VectorIKhEEi(%rip), %rax
	movq	%rax, -72(%rbp)
	movq	(%r12), %rax
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	movq	%rax, %rsi
	movslq	%edx, %rdx
	call	*-72(%rbp)
	movl	%eax, %r14d
	testl	%eax, %eax
	jns	.L259
.L200:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L260
	addq	$104, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L256:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L166
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L169
	movq	23(%rax), %rdx
	movl	11(%rdx), %edx
	testl	%edx, %edx
	je	.L169
	movq	%r12, %rsi
	xorl	%edx, %edx
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	movq	(%rax), %rsi
	movq	%rax, %r12
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L259:
	movq	(%r12), %rax
	movq	%r15, %rsi
	movq	%r13, %rdi
	movl	11(%rax), %ebx
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	cmpl	%ebx, %r14d
	jge	.L188
	movl	$1, %r10d
	xorl	%edi, %edi
	movq	%r12, -128(%rbp)
	movl	%r14d, %r11d
	movl	%r14d, -136(%rbp)
	movq	%r13, %rcx
	movq	%rax, %r12
	movl	%ebx, %r13d
	movl	%r10d, %r14d
	movl	%edi, %ebx
	.p2align 4,,10
	.p2align 3
.L191:
	movl	%r11d, %esi
	movl	%r13d, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_112UnescapeCharIhEEiNS0_6VectorIKT_EEiiPi.isra.0
	cmpl	$255, %eax
	movl	$0, %eax
	cmovg	%eax, %r14d
	addl	-96(%rbp), %r11d
	addl	$1, %ebx
	cmpl	%r11d, %r13d
	jg	.L191
	movl	%r14d, %r10d
	movq	-128(%rbp), %r12
	movl	-136(%rbp), %r14d
	xorl	%edx, %edx
	movq	-120(%rbp), %rdi
	movl	%ebx, -140(%rbp)
	movl	%r13d, %ebx
	movq	%rcx, %r13
	movq	%r12, %rsi
	movl	%r14d, %ecx
	movb	%r10b, -136(%rbp)
	call	_ZN2v88internal7Factory18NewProperSubStringENS0_6HandleINS0_6StringEEEii@PLT
	movzbl	-136(%rbp), %r10d
	movq	%rax, -128(%rbp)
	testb	%r10b, %r10b
	jne	.L261
	movl	-140(%rbp), %esi
	movq	-120(%rbp), %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory19NewRawTwoByteStringEiNS0_14AllocationTypeE@PLT
	movq	%rax, -136(%rbp)
	testq	%rax, %rax
	je	.L198
	movq	(%r12), %rax
	movq	%r13, %rdi
	movq	%r15, %rsi
	movl	$16, %r12d
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	movq	-136(%rbp), %r11
	movq	%r13, %rcx
	movq	%rax, %r13
	.p2align 4,,10
	.p2align 3
.L199:
	movq	(%r11), %r15
	movl	%r14d, %esi
	movl	%ebx, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal12_GLOBAL__N_112UnescapeCharIhEEiNS0_6VectorIKT_EEiiPi.isra.0
	movw	%ax, -1(%r15,%r12)
	addl	-96(%rbp), %r14d
	addq	$2, %r12
	cmpl	%r14d, %ebx
	jg	.L199
	jmp	.L252
	.p2align 4,,10
	.p2align 3
.L178:
	movq	41088(%rax), %r12
	cmpq	41096(%rax), %r12
	je	.L262
.L180:
	movq	-120(%rbp), %rcx
	leaq	8(%r12), %rax
	movq	%rax, 41088(%rcx)
	movq	%rsi, (%r12)
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L169:
	movq	15(%rax), %rsi
	movq	-120(%rbp), %rax
	movq	41112(%rax), %rdi
	testq	%rdi, %rdi
	je	.L171
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r12
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L261:
	movl	-140(%rbp), %esi
	movq	-120(%rbp), %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory19NewRawOneByteStringEiNS0_14AllocationTypeE@PLT
	movq	%rax, -136(%rbp)
	testq	%rax, %rax
	je	.L198
	movq	(%r12), %rax
	movq	%r13, %rdi
	movq	%r15, %rsi
	movl	$16, %r12d
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	movq	%r13, %rcx
	movq	-136(%rbp), %r13
	movq	%rax, %r11
	.p2align 4,,10
	.p2align 3
.L196:
	movq	0(%r13), %r15
	movl	%r14d, %esi
	movl	%ebx, %edx
	movq	%r11, %rdi
	call	_ZN2v88internal12_GLOBAL__N_112UnescapeCharIhEEiNS0_6VectorIKT_EEiiPi.isra.0
	movb	%al, -1(%r15,%r12)
	addl	-96(%rbp), %r14d
	addq	$1, %r12
	cmpl	%r14d, %ebx
	jg	.L196
	jmp	.L252
	.p2align 4,,10
	.p2align 3
.L258:
	movl	-140(%rbp), %esi
	movq	-120(%rbp), %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory19NewRawOneByteStringEiNS0_14AllocationTypeE@PLT
	movq	%rax, -136(%rbp)
	testq	%rax, %rax
	je	.L198
	movq	(%r12), %rax
	movq	%r13, %rdi
	movq	%r15, %rsi
	movl	$16, %r12d
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	movq	-136(%rbp), %r11
	movq	%r13, %rcx
	movq	%rax, %r13
	.p2align 4,,10
	.p2align 3
.L209:
	movq	(%r11), %r15
	movl	%ebx, %esi
	movl	%r14d, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal12_GLOBAL__N_112UnescapeCharItEEiNS0_6VectorIKT_EEiiPi.isra.0
	movb	%al, -1(%r15,%r12)
	addl	-96(%rbp), %ebx
	addq	$1, %r12
	cmpl	%ebx, %r14d
	jg	.L209
	jmp	.L252
	.p2align 4,,10
	.p2align 3
.L171:
	movq	41088(%rax), %r12
	cmpq	41096(%rax), %r12
	je	.L263
.L173:
	movq	-120(%rbp), %rcx
	leaq	8(%r12), %rax
	movq	%rax, 41088(%rcx)
	movq	%rsi, (%r12)
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L202:
	movl	%ebx, %ecx
.L255:
	movq	-120(%rbp), %rbx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Factory18NewProperSubStringENS0_6HandleINS0_6StringEEEii@PLT
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal7Factory19NewRawOneByteStringEiNS0_14AllocationTypeE@PLT
	movq	%rax, -136(%rbp)
	testq	%rax, %rax
	jne	.L264
.L198:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L188:
	movl	%r14d, %ecx
	jmp	.L255
.L263:
	movq	%rax, %rdi
	movq	%rsi, -128(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-128(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L264:
	movq	(%r12), %rax
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	jmp	.L252
	.p2align 4,,10
	.p2align 3
.L262:
	movq	%rax, %rdi
	movq	%rsi, -128(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-128(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L180
.L260:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17862:
	.size	_ZN2v88internal3Uri8UnescapeEPNS0_7IsolateENS0_6HandleINS0_6StringEEE, .-_ZN2v88internal3Uri8UnescapeEPNS0_7IsolateENS0_6HandleINS0_6StringEEE
	.section	.rodata._ZNSt6vectorItSaItEE17_M_realloc_insertIJRKtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_.str1.1,"aMS",@progbits,1
.LC3:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorItSaItEE17_M_realloc_insertIJRKtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_,"axG",@progbits,_ZNSt6vectorItSaItEE17_M_realloc_insertIJRKtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorItSaItEE17_M_realloc_insertIJRKtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_
	.type	_ZNSt6vectorItSaItEE17_M_realloc_insertIJRKtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_, @function
_ZNSt6vectorItSaItEE17_M_realloc_insertIJRKtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_:
.LFB20768:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movabsq	$4611686018427387903, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r9
	movq	(%rdi), %r15
	movq	%r9, %rcx
	subq	%r15, %rcx
	movq	%rcx, %rax
	sarq	%rax
	cmpq	%rsi, %rax
	je	.L279
	movq	%rdx, %r13
	movq	%r8, %rdx
	movq	%rdi, %r12
	subq	%r15, %rdx
	testq	%rax, %rax
	je	.L275
	movabsq	$9223372036854775806, %r14
	cmpq	%rcx, %rax
	jbe	.L280
.L267:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L274:
	movzwl	0(%r13), %eax
	subq	%r8, %r9
	leaq	2(%rbx,%rdx), %r10
	movq	%r9, %r13
	movw	%ax, (%rbx,%rdx)
	leaq	(%r10,%r9), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L281
	testq	%r9, %r9
	jg	.L270
	testq	%r15, %r15
	jne	.L273
.L271:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L281:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r10, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r10
	movq	-72(%rbp), %r8
	jg	.L270
.L273:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	jmp	.L271
	.p2align 4,,10
	.p2align 3
.L280:
	testq	%rcx, %rcx
	jne	.L268
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L270:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r10, %rdi
	call	memcpy@PLT
	testq	%r15, %r15
	je	.L271
	jmp	.L273
	.p2align 4,,10
	.p2align 3
.L275:
	movl	$2, %r14d
	jmp	.L267
.L279:
	leaq	.LC3(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L268:
	cmpq	%rsi, %rcx
	cmovbe	%rcx, %rsi
	movq	%rsi, %r14
	addq	%rsi, %r14
	jmp	.L267
	.cfi_endproc
.LFE20768:
	.size	_ZNSt6vectorItSaItEE17_M_realloc_insertIJRKtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_, .-_ZNSt6vectorItSaItEE17_M_realloc_insertIJRKtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_
	.section	.text._ZNSt6vectorItSaItEE17_M_realloc_insertIJtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_,"axG",@progbits,_ZNSt6vectorItSaItEE17_M_realloc_insertIJtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorItSaItEE17_M_realloc_insertIJtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_
	.type	_ZNSt6vectorItSaItEE17_M_realloc_insertIJtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_, @function
_ZNSt6vectorItSaItEE17_M_realloc_insertIJtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_:
.LFB21353:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movabsq	$4611686018427387903, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r9
	movq	(%rdi), %r15
	movq	%r9, %rcx
	subq	%r15, %rcx
	movq	%rcx, %rax
	sarq	%rax
	cmpq	%rsi, %rax
	je	.L296
	movq	%rdx, %r13
	movq	%r8, %rdx
	movq	%rdi, %r12
	subq	%r15, %rdx
	testq	%rax, %rax
	je	.L292
	movabsq	$9223372036854775806, %r14
	cmpq	%rcx, %rax
	jbe	.L297
.L284:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L291:
	movzwl	0(%r13), %eax
	subq	%r8, %r9
	leaq	2(%rbx,%rdx), %r10
	movq	%r9, %r13
	movw	%ax, (%rbx,%rdx)
	leaq	(%r10,%r9), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L298
	testq	%r9, %r9
	jg	.L287
	testq	%r15, %r15
	jne	.L290
.L288:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L298:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r10, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r10
	movq	-72(%rbp), %r8
	jg	.L287
.L290:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L297:
	testq	%rcx, %rcx
	jne	.L285
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L287:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r10, %rdi
	call	memcpy@PLT
	testq	%r15, %r15
	je	.L288
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L292:
	movl	$2, %r14d
	jmp	.L284
.L296:
	leaq	.LC3(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L285:
	cmpq	%rsi, %rcx
	cmovbe	%rcx, %rsi
	movq	%rsi, %r14
	addq	%rsi, %r14
	jmp	.L284
	.cfi_endproc
.LFE21353:
	.size	_ZNSt6vectorItSaItEE17_M_realloc_insertIJtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_, .-_ZNSt6vectorItSaItEE17_M_realloc_insertIJtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_
	.section	.text._ZN2v88internal12_GLOBAL__N_111IntoTwoByteEibiPNS0_6String11FlatContentEPSt6vectorItSaItEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_111IntoTwoByteEibiPNS0_6String11FlatContentEPSt6vectorItSaItEE, @function
_ZN2v88internal12_GLOBAL__N_111IntoTwoByteEibiPNS0_6String11FlatContentEPSt6vectorItSaItEE:
.LFB17825:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movb	%sil, -81(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	%edx, %edi
	jge	.L373
	movq	%r8, %r12
	movl	%edi, %ebx
	movq	%rcx, %r13
	movl	%edx, %r8d
	leaq	.L360(%rip), %r15
.L300:
	movl	12(%r13), %edi
	movq	0(%r13), %rdx
	movslq	%ebx, %rax
	cmpl	$1, %edi
	je	.L403
	movzwl	(%rdx,%rax,2), %eax
.L303:
	movw	%ax, -78(%rbp)
	cmpw	$37, %ax
	jne	.L304
	leal	2(%rbx), %r14d
	cmpl	%r8d, %r14d
	jge	.L305
	leal	1(%rbx), %ecx
	movslq	%r14d, %r9
	movslq	%ecx, %rcx
	cmpl	$1, %edi
	je	.L404
	movzwl	(%rdx,%r9,2), %esi
	movzwl	-2(%rdx,%r9,2), %r11d
	movl	%esi, %r10d
.L307:
	movzwl	%r11w, %eax
	cmpw	$102, %r11w
	ja	.L305
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L308
	orl	$32, %eax
	movl	%eax, %r11d
	leal	-49(%rax), %eax
	cmpl	$5, %eax
	ja	.L305
	leal	-39(%r11), %eax
	cmpl	$38, %r11d
	je	.L305
.L308:
	cmpw	$102, %r10w
	ja	.L305
	subl	$48, %esi
	cmpl	$9, %esi
	jbe	.L309
	orl	$32, %esi
	movl	%esi, %r10d
	leal	-49(%rsi), %esi
	cmpl	$5, %esi
	ja	.L305
	leal	-39(%r10), %esi
	cmpl	$38, %r10d
	je	.L305
.L309:
	sall	$4, %eax
	addl	%esi, %eax
	js	.L305
	cmpw	$127, %ax
	ja	.L405
	cmpb	$0, -81(%rbp)
	movq	8(%r12), %rdi
	movw	%ax, -76(%rbp)
	movq	16(%r12), %rsi
	je	.L358
	leal	-35(%rax), %edx
	cmpw	$29, %dx
	ja	.L358
	movzwl	%dx, %edx
	movslq	(%r15,%rdx,4), %rdx
	addq	%r15, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal12_GLOBAL__N_111IntoTwoByteEibiPNS0_6String11FlatContentEPSt6vectorItSaItEE,"a",@progbits
	.align 4
	.align 4
.L360:
	.long	.L359-.L360
	.long	.L359-.L360
	.long	.L358-.L360
	.long	.L359-.L360
	.long	.L358-.L360
	.long	.L358-.L360
	.long	.L358-.L360
	.long	.L358-.L360
	.long	.L359-.L360
	.long	.L359-.L360
	.long	.L358-.L360
	.long	.L358-.L360
	.long	.L359-.L360
	.long	.L358-.L360
	.long	.L358-.L360
	.long	.L358-.L360
	.long	.L358-.L360
	.long	.L358-.L360
	.long	.L358-.L360
	.long	.L358-.L360
	.long	.L358-.L360
	.long	.L358-.L360
	.long	.L358-.L360
	.long	.L359-.L360
	.long	.L359-.L360
	.long	.L358-.L360
	.long	.L359-.L360
	.long	.L358-.L360
	.long	.L359-.L360
	.long	.L359-.L360
	.section	.text._ZN2v88internal12_GLOBAL__N_111IntoTwoByteEibiPNS0_6String11FlatContentEPSt6vectorItSaItEE
.L358:
	cmpq	%rdi, %rsi
	je	.L365
	movw	%ax, (%rdi)
	addq	$2, 8(%r12)
.L366:
	movl	%r14d, %ebx
.L357:
	addl	$1, %ebx
	cmpl	%ebx, %r8d
	jg	.L300
.L373:
	movl	$1, %eax
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L304:
	movq	8(%r12), %rsi
	cmpq	16(%r12), %rsi
	je	.L372
.L402:
	movw	%ax, (%rsi)
	addq	$2, 8(%r12)
	jmp	.L357
	.p2align 4,,10
	.p2align 3
.L405:
	movb	%al, -60(%rbp)
	movzwl	%ax, %esi
	testb	$64, %al
	je	.L375
	leal	5(%rbx), %r11d
	cmpl	%r11d, %r8d
	jle	.L305
	leal	3(%rbx), %r9d
	movslq	%r9d, %r9
	cmpl	$1, %edi
	je	.L313
	cmpw	$37, (%rdx,%r9,2)
	leaq	(%r9,%r9), %rcx
	jne	.L305
	movzwl	4(%rdx,%rcx), %r9d
	movzwl	2(%rdx,%rcx), %r14d
	movl	%r9d, %r10d
.L315:
	movzwl	%r14w, %ecx
	cmpw	$102, %r14w
	ja	.L305
	subl	$48, %ecx
	cmpl	$9, %ecx
	jbe	.L318
	orl	$32, %ecx
	movl	%ecx, %r14d
	leal	-49(%rcx), %ecx
	cmpl	$5, %ecx
	ja	.L305
	leal	-39(%r14), %ecx
	cmpl	$38, %r14d
	je	.L305
.L318:
	cmpw	$102, %r10w
	ja	.L305
	subl	$48, %r9d
	cmpl	$9, %r9d
	jbe	.L322
	movl	%r9d, %r10d
	orl	$32, %r10d
	leal	-49(%r10), %r9d
	cmpl	$5, %r9d
	ja	.L305
	leal	-39(%r10), %r9d
	cmpl	$38, %r10d
	je	.L305
.L322:
	sall	$4, %ecx
	addl	%r9d, %ecx
	js	.L305
	movb	%cl, -59(%rbp)
	testb	$32, %sil
	je	.L376
	leal	8(%rbx), %r11d
	cmpl	%r11d, %r8d
	jle	.L305
	leal	6(%rbx), %r9d
	movslq	%r9d, %r9
	cmpl	$1, %edi
	je	.L328
	cmpw	$37, (%rdx,%r9,2)
	leaq	(%r9,%r9), %rcx
	jne	.L305
	movzwl	4(%rdx,%rcx), %r9d
	movzwl	2(%rdx,%rcx), %r14d
	movl	%r9d, %r10d
.L329:
	movzwl	%r14w, %ecx
	cmpw	$102, %r14w
	ja	.L305
	subl	$48, %ecx
	cmpl	$9, %ecx
	jbe	.L330
	orl	$32, %ecx
	movl	%ecx, %r14d
	leal	-49(%rcx), %ecx
	cmpl	$5, %ecx
	ja	.L305
	leal	-39(%r14), %ecx
	cmpl	$38, %r14d
	je	.L305
.L330:
	cmpw	$102, %r10w
	ja	.L305
	subl	$48, %r9d
	cmpl	$9, %r9d
	jbe	.L331
	movl	%r9d, %r10d
	orl	$32, %r10d
	leal	-49(%r10), %r9d
	cmpl	$5, %r9d
	ja	.L305
	leal	-39(%r10), %r9d
	cmpl	$38, %r10d
	je	.L305
.L331:
	sall	$4, %ecx
	addl	%r9d, %ecx
	js	.L305
	movb	%cl, -58(%rbp)
	testb	$16, %sil
	je	.L377
	leal	11(%rbx), %ecx
	cmpl	%ecx, %r8d
	jle	.L305
	leal	9(%rbx), %r9d
	movslq	%r9d, %r9
	cmpl	$1, %edi
	je	.L406
	cmpw	$37, (%rdx,%r9,2)
	leaq	(%r9,%r9), %r11
	jne	.L305
	movzwl	4(%rdx,%r11), %edi
	movzwl	2(%rdx,%r11), %r9d
	movl	%edi, %r10d
.L336:
	movzwl	%r9w, %edx
	cmpw	$102, %r9w
	ja	.L305
	subl	$48, %edx
	cmpl	$9, %edx
	jbe	.L338
	orl	$32, %edx
	movl	%edx, %r9d
	leal	-49(%rdx), %edx
	cmpl	$5, %edx
	ja	.L305
	leal	-39(%r9), %edx
	cmpl	$38, %r9d
	je	.L305
.L338:
	cmpw	$102, %r10w
	ja	.L305
	subl	$48, %edi
	cmpl	$9, %edi
	jbe	.L341
	orl	$32, %edi
	movl	%edi, %r9d
	leal	-49(%rdi), %edi
	cmpl	$5, %edi
	ja	.L305
	leal	-39(%r9), %edi
	cmpl	$38, %r9d
	je	.L305
.L341:
	sall	$4, %edx
	addl	%edi, %edx
	js	.L305
	andl	$8, %esi
	movb	%dl, -57(%rbp)
	je	.L407
.L305:
	xorl	%eax, %eax
.L299:
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L408
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L403:
	.cfi_restore_state
	movzbl	(%rdx,%rax), %eax
	jmp	.L303
.L359:
	movl	$37, %edx
	movw	%dx, -72(%rbp)
	cmpq	%rdi, %rsi
	je	.L361
	movl	$37, %eax
	movw	%ax, (%rdi)
	movq	8(%r12), %rax
	leaq	2(%rax), %rsi
	movq	%rsi, 8(%r12)
.L362:
	cmpl	$1, 12(%r13)
	movq	0(%r13), %rax
	je	.L409
	movzwl	(%rax,%rcx,2), %edx
	movw	%dx, -74(%rbp)
	movzwl	2(%rax,%rcx,2), %eax
.L367:
	movw	%ax, -72(%rbp)
	cmpq	16(%r12), %rsi
	je	.L368
	movw	%dx, (%rsi)
	movq	8(%r12), %rax
	leaq	2(%rax), %rsi
	movq	%rsi, 8(%r12)
.L369:
	cmpq	%rsi, 16(%r12)
	je	.L370
	movzwl	-72(%rbp), %eax
	movw	%ax, (%rsi)
	addq	$2, 8(%r12)
	jmp	.L366
	.p2align 4,,10
	.p2align 3
.L404:
	movzbl	(%rdx,%r9), %r10d
	movzbl	(%rdx,%rcx), %r11d
	movl	%r10d, %esi
	jmp	.L307
	.p2align 4,,10
	.p2align 3
.L372:
	leaq	-78(%rbp), %rdx
	movq	%r12, %rdi
	movl	%r8d, -96(%rbp)
	call	_ZNSt6vectorItSaItEE17_M_realloc_insertIJRKtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_
	movl	-96(%rbp), %r8d
	jmp	.L357
	.p2align 4,,10
	.p2align 3
.L365:
	leaq	-76(%rbp), %rdx
	movq	%r12, %rdi
	movl	%r8d, -96(%rbp)
	call	_ZNSt6vectorItSaItEE17_M_realloc_insertIJRKtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_
	movl	-96(%rbp), %r8d
	jmp	.L366
	.p2align 4,,10
	.p2align 3
.L313:
	cmpb	$37, (%rdx,%r9)
	jne	.L305
	movslq	%r11d, %rcx
	movzbl	(%rdx,%rcx), %r10d
	leal	4(%rbx), %ecx
	movslq	%ecx, %rcx
	movzbl	(%rdx,%rcx), %r14d
	movl	%r10d, %r9d
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L328:
	cmpb	$37, (%rdx,%r9)
	jne	.L305
	movslq	%r11d, %rcx
	movzbl	(%rdx,%rcx), %r10d
	leal	7(%rbx), %ecx
	movslq	%ecx, %rcx
	movzbl	(%rdx,%rcx), %r14d
	movl	%r10d, %r9d
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L406:
	cmpb	$37, (%rdx,%r9)
	jne	.L305
	movslq	%ecx, %rdi
	addl	$10, %ebx
	movzbl	(%rdx,%rdi), %r10d
	movslq	%ebx, %rbx
	movzbl	(%rdx,%rbx), %r9d
	movl	%r10d, %edi
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L409:
	movzbl	(%rax,%rcx), %edx
	movw	%dx, -74(%rbp)
	movzbl	(%rax,%r9), %eax
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L361:
	leaq	-72(%rbp), %rdx
	movq	%r12, %rdi
	movl	%r8d, -88(%rbp)
	movq	%rcx, -104(%rbp)
	movq	%r9, -96(%rbp)
	call	_ZNSt6vectorItSaItEE17_M_realloc_insertIJtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_
	movq	8(%r12), %rsi
	movl	-88(%rbp), %r8d
	movq	-104(%rbp), %rcx
	movq	-96(%rbp), %r9
	jmp	.L362
	.p2align 4,,10
	.p2align 3
.L370:
	leaq	-72(%rbp), %rdx
	movq	%r12, %rdi
	movl	%r8d, -96(%rbp)
	call	_ZNSt6vectorItSaItEE17_M_realloc_insertIJRKtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_
	movl	-96(%rbp), %r8d
	jmp	.L366
	.p2align 4,,10
	.p2align 3
.L368:
	leaq	-74(%rbp), %rdx
	movq	%r12, %rdi
	movl	%r8d, -96(%rbp)
	call	_ZNSt6vectorItSaItEE17_M_realloc_insertIJRKtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_
	movq	8(%r12), %rsi
	movl	-96(%rbp), %r8d
	jmp	.L369
.L407:
	movl	%ecx, %ebx
	movl	$4, %ecx
.L326:
	movslq	%ecx, %rsi
.L311:
	movq	$0, -72(%rbp)
	testb	%al, %al
	js	.L344
	movq	$1, -72(%rbp)
	movq	8(%r12), %rsi
	movzbl	%al, %r14d
.L345:
	movw	%r14w, -74(%rbp)
	cmpq	16(%r12), %rsi
	je	.L354
	movw	%r14w, (%rsi)
	addq	$2, 8(%r12)
	jmp	.L357
.L344:
	leaq	-72(%rbp), %rdx
	leaq	-60(%rbp), %rdi
	movl	%r8d, -96(%rbp)
	movl	%ecx, -104(%rbp)
	call	_ZN7unibrow4Utf814CalculateValueEPKhmPm@PLT
	movl	-96(%rbp), %r8d
	cmpl	$65533, %eax
	movl	%eax, %r14d
	jne	.L346
	movl	-104(%rbp), %ecx
	cmpl	$3, %ecx
	jne	.L305
	cmpb	$-17, -60(%rbp)
	jne	.L305
	cmpb	$-65, -59(%rbp)
	jne	.L305
	cmpb	$-67, -58(%rbp)
	jne	.L305
	movq	8(%r12), %rsi
	jmp	.L345
	.p2align 4,,10
	.p2align 3
.L346:
	movq	8(%r12), %rsi
	movq	16(%r12), %rdx
	cmpl	$65535, %eax
	jle	.L345
	leal	-65536(%rax), %eax
	shrl	$10, %eax
	andw	$1023, %ax
	subw	$10240, %ax
	movw	%ax, -74(%rbp)
	cmpq	%rdx, %rsi
	je	.L352
	movw	%ax, (%rsi)
	movq	8(%r12), %rax
	leaq	2(%rax), %rsi
	movq	%rsi, 8(%r12)
.L353:
	movl	%r14d, %eax
	andw	$1023, %ax
	subw	$9216, %ax
	movw	%ax, -74(%rbp)
	cmpq	%rsi, 16(%r12)
	jne	.L402
.L354:
	leaq	-74(%rbp), %rdx
	movq	%r12, %rdi
	movl	%r8d, -96(%rbp)
	call	_ZNSt6vectorItSaItEE17_M_realloc_insertIJtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_
	movl	-96(%rbp), %r8d
	jmp	.L357
.L375:
	movl	%r14d, %ebx
	movl	$1, %esi
	movl	$1, %ecx
	jmp	.L311
.L376:
	movl	%r11d, %ebx
	movl	$2, %ecx
	jmp	.L326
.L377:
	movl	%r11d, %ebx
	movl	$3, %ecx
	jmp	.L326
.L352:
	leaq	-74(%rbp), %rdx
	movq	%r12, %rdi
	movl	%r8d, -96(%rbp)
	call	_ZNSt6vectorItSaItEE17_M_realloc_insertIJtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_
	movq	8(%r12), %rsi
	movl	-96(%rbp), %r8d
	jmp	.L353
.L408:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17825:
	.size	_ZN2v88internal12_GLOBAL__N_111IntoTwoByteEibiPNS0_6String11FlatContentEPSt6vectorItSaItEE, .-_ZN2v88internal12_GLOBAL__N_111IntoTwoByteEibiPNS0_6String11FlatContentEPSt6vectorItSaItEE
	.section	.text._ZNSt6vectorIhSaIhEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_,"axG",@progbits,_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	.type	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_, @function
_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_:
.LFB21368:
	.cfi_startproc
	endbr64
	movabsq	$9223372036854775807, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rsi
	movq	(%rdi), %r15
	movq	%rsi, %rax
	subq	%r15, %rax
	cmpq	%rcx, %rax
	je	.L424
	movq	%rdx, %r13
	movq	%r8, %rdx
	movq	%rdi, %r12
	subq	%r15, %rdx
	testq	%rax, %rax
	je	.L419
	leaq	(%rax,%rax), %r14
	cmpq	%r14, %rax
	jbe	.L425
.L421:
	movq	%rcx, %r14
.L412:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L418:
	movzbl	0(%r13), %eax
	subq	%r8, %rsi
	leaq	1(%rbx,%rdx), %r10
	movq	%rsi, %r13
	movb	%al, (%rbx,%rdx)
	leaq	(%r10,%rsi), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L426
	testq	%rsi, %rsi
	jg	.L414
	testq	%r15, %r15
	jne	.L417
.L415:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L426:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r10, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r10
	movq	-72(%rbp), %r8
	jg	.L414
.L417:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	jmp	.L415
	.p2align 4,,10
	.p2align 3
.L425:
	testq	%r14, %r14
	js	.L421
	jne	.L412
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L418
	.p2align 4,,10
	.p2align 3
.L414:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r10, %rdi
	call	memcpy@PLT
	testq	%r15, %r15
	je	.L415
	jmp	.L417
	.p2align 4,,10
	.p2align 3
.L419:
	movl	$1, %r14d
	jmp	.L412
.L424:
	leaq	.LC3(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE21368:
	.size	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_, .-_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	.section	.text._ZN2v88internal3Uri6DecodeEPNS0_7IsolateENS0_6HandleINS0_6StringEEEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Uri6DecodeEPNS0_7IsolateENS0_6HandleINS0_6StringEEEb
	.type	_ZN2v88internal3Uri6DecodeEPNS0_7IsolateENS0_6HandleINS0_6StringEEEb, @function
_ZN2v88internal3Uri6DecodeEPNS0_7IsolateENS0_6HandleINS0_6StringEEEb:
.LFB17827:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movb	%dl, -145(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	%rax, %rsi
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L526
.L429:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	jbe	.L527
.L437:
	pxor	%xmm0, %xmm0
	movzbl	%r12b, %eax
	movq	$0, -96(%rbp)
	leaq	-136(%rbp), %rbx
	movl	%eax, -160(%rbp)
	leaq	-137(%rbp), %rsi
	movq	%rbx, %rdi
	movq	$0, -64(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	(%r15), %rax
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	movq	%rax, -128(%rbp)
	movq	%rdx, -120(%rbp)
	movq	(%r15), %rax
	movl	11(%rax), %r12d
	testl	%r12d, %r12d
	jle	.L473
	xorl	%r15d, %r15d
	leaq	.L456(%rip), %r14
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L446:
	cmpw	$127, %ax
	ja	.L524
	movb	%al, -136(%rbp)
	movq	-104(%rbp), %rsi
	cmpq	-96(%rbp), %rsi
	je	.L470
	movb	%al, (%rsi)
	addq	$1, -104(%rbp)
.L471:
	movl	%r15d, %ecx
.L463:
	leal	1(%rcx), %r15d
	cmpl	%r15d, %r12d
	jle	.L473
.L472:
	movl	-116(%rbp), %edx
	movq	-128(%rbp), %rsi
	movslq	%r15d, %rax
	cmpl	$1, %edx
	je	.L528
	movzwl	(%rsi,%rax,2), %eax
.L445:
	cmpw	$37, %ax
	jne	.L446
	leal	2(%r15), %ecx
	cmpl	%ecx, %r12d
	jle	.L493
	leal	1(%r15), %r8d
	movslq	%ecx, %r10
	movslq	%r8d, %r8
	cmpl	$1, %edx
	je	.L529
	movzwl	(%rsi,%r10,2), %edx
	movzwl	-2(%rsi,%r10,2), %esi
	movl	%edx, %edi
.L449:
	movzwl	%si, %eax
	cmpw	$102, %si
	ja	.L493
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L450
	orl	$32, %eax
	movl	%eax, %esi
	leal	-49(%rax), %eax
	cmpl	$5, %eax
	ja	.L493
	leal	-39(%rsi), %eax
	cmpl	$38, %esi
	je	.L493
.L450:
	cmpw	$102, %di
	ja	.L493
	subl	$48, %edx
	cmpl	$9, %edx
	jbe	.L451
	orl	$32, %edx
	movl	%edx, %esi
	leal	-49(%rdx), %edx
	cmpl	$5, %edx
	ja	.L493
	leal	-39(%rsi), %edx
	cmpl	$38, %esi
	je	.L493
.L451:
	sall	$4, %eax
	addl	%edx, %eax
	js	.L493
	cmpw	$127, %ax
	ja	.L524
	cmpb	$0, -145(%rbp)
	movq	-104(%rbp), %rsi
	movq	-96(%rbp), %rdi
	je	.L454
	leal	-35(%rax), %edx
	cmpw	$29, %dx
	ja	.L454
	movzwl	%dx, %edx
	movslq	(%r14,%rdx,4), %rdx
	addq	%r14, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal3Uri6DecodeEPNS0_7IsolateENS0_6HandleINS0_6StringEEEb,"a",@progbits
	.align 4
	.align 4
.L456:
	.long	.L455-.L456
	.long	.L455-.L456
	.long	.L454-.L456
	.long	.L455-.L456
	.long	.L454-.L456
	.long	.L454-.L456
	.long	.L454-.L456
	.long	.L454-.L456
	.long	.L455-.L456
	.long	.L455-.L456
	.long	.L454-.L456
	.long	.L454-.L456
	.long	.L455-.L456
	.long	.L454-.L456
	.long	.L454-.L456
	.long	.L454-.L456
	.long	.L454-.L456
	.long	.L454-.L456
	.long	.L454-.L456
	.long	.L454-.L456
	.long	.L454-.L456
	.long	.L454-.L456
	.long	.L454-.L456
	.long	.L455-.L456
	.long	.L455-.L456
	.long	.L454-.L456
	.long	.L455-.L456
	.long	.L454-.L456
	.long	.L455-.L456
	.long	.L455-.L456
	.section	.text._ZN2v88internal3Uri6DecodeEPNS0_7IsolateENS0_6HandleINS0_6StringEEEb
.L454:
	movb	%al, -136(%rbp)
	cmpq	%rdi, %rsi
	je	.L467
	leal	1(%rcx), %r15d
	movb	%al, (%rsi)
	addq	$1, -104(%rbp)
	cmpl	%r15d, %r12d
	jg	.L472
	.p2align 4,,10
	.p2align 3
.L473:
	movq	-112(%rbp), %rcx
	movq	-104(%rbp), %rax
	movq	-72(%rbp), %rsi
	movq	-80(%rbp), %rdx
	subq	%rcx, %rax
	cmpq	%rdx, %rsi
	je	.L530
	subq	%rdx, %rsi
	movq	%r13, %rdi
	xorl	%edx, %edx
	sarq	%rsi
	addl	%eax, %esi
	call	_ZN2v88internal7Factory19NewRawTwoByteStringEiNS0_14AllocationTypeE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L490
	movq	(%rax), %rdx
	movq	-112(%rbp), %rcx
	movq	-104(%rbp), %rax
	leaq	15(%rdx), %rsi
	subq	%rcx, %rax
	leaq	(%rsi,%rax,2), %rdi
	cmpq	%rsi, %rdi
	jbe	.L480
	movq	%rdi, %rax
	leaq	16(%rdx), %r8
	movl	$1, %r9d
	movl	$2, %r10d
	subq	%rdx, %rax
	leaq	-16(%rax), %rdx
	movq	%rdx, %rax
	shrq	%rax
	addq	$1, %rax
	cmpq	%r8, %rdi
	cmovnb	%rax, %r9
	addq	%rax, %rax
	cmpq	%r8, %rdi
	cmovb	%r10, %rax
	addq	%rsi, %rax
	cmpq	%rax, %rcx
	leaq	(%rcx,%r9), %rax
	setnb	%r10b
	cmpq	%rax, %rsi
	setnb	%al
	orb	%al, %r10b
	je	.L511
	cmpq	$29, %rdx
	seta	%dl
	cmpq	%r8, %rdi
	setnb	%al
	testb	%al, %dl
	je	.L511
	movq	%r9, %r8
	movq	%rcx, %rdx
	pxor	%xmm1, %xmm1
	movq	%rsi, %rax
	andq	$-16, %r8
	addq	%rcx, %r8
	.p2align 4,,10
	.p2align 3
.L482:
	movdqu	(%rdx), %xmm0
	addq	$16, %rdx
	addq	$32, %rax
	movdqa	%xmm0, %xmm2
	punpckhbw	%xmm1, %xmm0
	punpcklbw	%xmm1, %xmm2
	movups	%xmm0, -16(%rax)
	movups	%xmm2, -32(%rax)
	cmpq	%r8, %rdx
	jne	.L482
	movq	%r9, %rdx
	andq	$-16, %rdx
	leaq	(%rsi,%rdx,2), %rax
	addq	%rdx, %rcx
	cmpq	%r9, %rdx
	je	.L484
	movzbl	(%rcx), %edx
	movw	%dx, (%rax)
	leaq	2(%rax), %rdx
	cmpq	%rdx, %rdi
	jbe	.L484
	movzbl	1(%rcx), %edx
	movw	%dx, 2(%rax)
	leaq	4(%rax), %rdx
	cmpq	%rdx, %rdi
	jbe	.L484
	movzbl	2(%rcx), %edx
	movw	%dx, 4(%rax)
	leaq	6(%rax), %rdx
	cmpq	%rdx, %rdi
	jbe	.L484
	movzbl	3(%rcx), %edx
	movw	%dx, 6(%rax)
	leaq	8(%rax), %rdx
	cmpq	%rdx, %rdi
	jbe	.L484
	movzbl	4(%rcx), %edx
	movw	%dx, 8(%rax)
	leaq	10(%rax), %rdx
	cmpq	%rdx, %rdi
	jbe	.L484
	movzbl	5(%rcx), %edx
	movw	%dx, 10(%rax)
	leaq	12(%rax), %rdx
	cmpq	%rdx, %rdi
	jbe	.L484
	movzbl	6(%rcx), %edx
	movw	%dx, 12(%rax)
	leaq	14(%rax), %rdx
	cmpq	%rdx, %rdi
	jbe	.L484
	movzbl	7(%rcx), %edx
	movw	%dx, 14(%rax)
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rdi
	jbe	.L484
	movzbl	8(%rcx), %edx
	movw	%dx, 16(%rax)
	leaq	18(%rax), %rdx
	cmpq	%rdx, %rdi
	jbe	.L484
	movzbl	9(%rcx), %edx
	movw	%dx, 18(%rax)
	leaq	20(%rax), %rdx
	cmpq	%rdx, %rdi
	jbe	.L484
	movzbl	10(%rcx), %edx
	movw	%dx, 20(%rax)
	leaq	22(%rax), %rdx
	cmpq	%rdx, %rdi
	jbe	.L484
	movzbl	11(%rcx), %edx
	movw	%dx, 22(%rax)
	leaq	24(%rax), %rdx
	cmpq	%rdx, %rdi
	jbe	.L484
	movzbl	12(%rcx), %edx
	movw	%dx, 24(%rax)
	leaq	26(%rax), %rdx
	cmpq	%rdx, %rdi
	jbe	.L484
	movzbl	13(%rcx), %edx
	movw	%dx, 26(%rax)
	leaq	28(%rax), %rdx
	cmpq	%rdx, %rdi
	jbe	.L484
	movzbl	14(%rcx), %edx
	movw	%dx, 28(%rax)
	.p2align 4,,10
	.p2align 3
.L484:
	movq	(%r12), %rdx
	movq	-104(%rbp), %rax
	subq	-112(%rbp), %rax
	leaq	15(%rdx,%rax,2), %rdi
.L480:
	movq	-80(%rbp), %rsi
	movq	-72(%rbp), %rdx
	subq	%rsi, %rdx
	cmpq	$7, %rdx
	ja	.L531
	leaq	(%rdi,%rdx), %rcx
	cmpq	%rdi, %rcx
	jbe	.L477
	leaq	15(%rsi), %rax
	subq	$1, %rdx
	subq	%rdi, %rax
	cmpq	$30, %rax
	jbe	.L512
	cmpq	$13, %rdx
	jbe	.L512
	shrq	%rdx
	xorl	%eax, %eax
	addq	$1, %rdx
	movq	%rdx, %r8
	shrq	$3, %r8
	salq	$4, %r8
	.p2align 4,,10
	.p2align 3
.L488:
	movdqu	(%rsi,%rax), %xmm3
	movups	%xmm3, (%rdi,%rax)
	addq	$16, %rax
	cmpq	%rax, %r8
	jne	.L488
	movq	%rdx, %rax
	andq	$-8, %rax
	leaq	(%rax,%rax), %r8
	addq	%r8, %rsi
	addq	%r8, %rdi
	cmpq	%rdx, %rax
	je	.L490
	movzwl	(%rsi), %eax
	movw	%ax, (%rdi)
	leaq	2(%rdi), %rax
	cmpq	%rax, %rcx
	jbe	.L490
	movzwl	2(%rsi), %eax
	movw	%ax, 2(%rdi)
	leaq	4(%rdi), %rax
	cmpq	%rax, %rcx
	jbe	.L490
	movzwl	4(%rsi), %eax
	movw	%ax, 4(%rdi)
	leaq	6(%rdi), %rax
	cmpq	%rax, %rcx
	jbe	.L490
	movzwl	6(%rsi), %eax
	movw	%ax, 6(%rdi)
	leaq	8(%rdi), %rax
	cmpq	%rax, %rcx
	jbe	.L490
	movzwl	8(%rsi), %eax
	movw	%ax, 8(%rdi)
	leaq	10(%rdi), %rax
	cmpq	%rax, %rcx
	jbe	.L490
	movzwl	10(%rsi), %eax
	movw	%ax, 10(%rdi)
	leaq	12(%rdi), %rax
	cmpq	%rax, %rcx
	jbe	.L490
	movzwl	12(%rsi), %eax
	movw	%ax, 12(%rdi)
.L490:
	movq	-80(%rbp), %rsi
	jmp	.L477
.L455:
	movb	$37, -136(%rbp)
	cmpq	%rdi, %rsi
	je	.L457
	movb	$37, (%rsi)
	movq	-104(%rbp), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, -104(%rbp)
.L458:
	cmpl	$1, -116(%rbp)
	movq	-128(%rbp), %rax
	je	.L532
	movzbl	(%rax,%r8,2), %edx
	movzbl	2(%rax,%r8,2), %r15d
	movb	%dl, -136(%rbp)
	cmpq	%rsi, -96(%rbp)
	je	.L465
.L534:
	movb	%dl, (%rsi)
	movq	-104(%rbp), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, -104(%rbp)
.L466:
	movb	%r15b, -136(%rbp)
	cmpq	%rsi, -96(%rbp)
	je	.L467
	movb	%r15b, (%rsi)
	addq	$1, -104(%rbp)
	jmp	.L463
	.p2align 4,,10
	.p2align 3
.L528:
	movzbl	(%rsi,%rax), %eax
	jmp	.L445
	.p2align 4,,10
	.p2align 3
.L470:
	leaq	-112(%rbp), %rdi
	movq	%rbx, %rdx
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	jmp	.L471
	.p2align 4,,10
	.p2align 3
.L529:
	movzbl	(%rsi,%r10), %edi
	movzbl	(%rsi,%r8), %esi
	movl	%edi, %edx
	jmp	.L449
	.p2align 4,,10
	.p2align 3
.L467:
	leaq	-112(%rbp), %rdi
	movq	%rbx, %rdx
	movl	%ecx, -152(%rbp)
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movl	-152(%rbp), %ecx
	jmp	.L463
	.p2align 4,,10
	.p2align 3
.L511:
	movzbl	(%rcx), %edx
	movq	%rsi, %rax
	addq	$2, %rsi
	addq	$1, %rcx
	movw	%dx, (%rax)
	cmpq	%rsi, %rdi
	ja	.L511
	jmp	.L484
	.p2align 4,,10
	.p2align 3
.L527:
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	jne	.L437
	movq	(%r15), %rax
	movq	41112(%r13), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L440
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
	jmp	.L437
	.p2align 4,,10
	.p2align 3
.L530:
	movslq	%eax, %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%rcx, -128(%rbp)
	movq	%rsi, -120(%rbp)
	leaq	-128(%rbp), %rsi
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	-80(%rbp), %rsi
	movq	%rax, %r12
.L477:
	testq	%rsi, %rsi
	je	.L491
	movq	%rsi, %rdi
	call	_ZdlPv@PLT
.L491:
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L492
	call	_ZdlPv@PLT
.L492:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L533
	addq	$136, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L512:
	.cfi_restore_state
	movsw
	cmpq	%rdi, %rcx
	jbe	.L490
	movsw
	cmpq	%rdi, %rcx
	ja	.L512
	jmp	.L490
	.p2align 4,,10
	.p2align 3
.L526:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L429
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L432
	movq	23(%rax), %rdx
	movl	11(%rdx), %edx
	testl	%edx, %edx
	je	.L432
	movq	%r15, %rsi
	xorl	%edx, %edx
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	movq	%rax, %r15
	jmp	.L437
	.p2align 4,,10
	.p2align 3
.L532:
	movzbl	(%rax,%r8), %edx
	movzbl	(%rax,%r10), %r15d
	movb	%dl, -136(%rbp)
	cmpq	%rsi, -96(%rbp)
	jne	.L534
.L465:
	leaq	-112(%rbp), %rdi
	movq	%rbx, %rdx
	movl	%ecx, -152(%rbp)
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movq	-104(%rbp), %rsi
	movl	-152(%rbp), %ecx
	jmp	.L466
	.p2align 4,,10
	.p2align 3
.L524:
	movl	-160(%rbp), %esi
	leaq	-128(%rbp), %rcx
	movl	%r12d, %edx
	movl	%r15d, %edi
	leaq	-80(%rbp), %r8
	call	_ZN2v88internal12_GLOBAL__N_111IntoTwoByteEibiPNS0_6String11FlatContentEPSt6vectorItSaItEE
	testb	%al, %al
	jne	.L473
	.p2align 4,,10
	.p2align 3
.L493:
	movq	12464(%r13), %rax
	movq	39(%rax), %rax
	movq	1775(%rax), %r12
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L474
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L475:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$339, %edx
	movq	%r13, %rdi
	xorl	%r12d, %r12d
	call	_ZN2v88internal7Factory8NewErrorENS0_6HandleINS0_10JSFunctionEEENS0_15MessageTemplateENS2_INS0_6ObjectEEES7_S7_@PLT
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	-80(%rbp), %rsi
	jmp	.L477
	.p2align 4,,10
	.p2align 3
.L457:
	leaq	-112(%rbp), %rdi
	movq	%rbx, %rdx
	movq	%r8, -176(%rbp)
	movq	%r10, -168(%rbp)
	movl	%ecx, -152(%rbp)
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movq	-104(%rbp), %rsi
	movq	-176(%rbp), %r8
	movq	-168(%rbp), %r10
	movl	-152(%rbp), %ecx
	jmp	.L458
	.p2align 4,,10
	.p2align 3
.L474:
	movq	41088(%r13), %rsi
	cmpq	41096(%r13), %rsi
	je	.L535
.L476:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r13)
	movq	%r12, (%rsi)
	jmp	.L475
	.p2align 4,,10
	.p2align 3
.L440:
	movq	41088(%r13), %r15
	cmpq	41096(%r13), %r15
	je	.L536
.L442:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r15)
	jmp	.L437
	.p2align 4,,10
	.p2align 3
.L531:
	call	memcpy@PLT
	movq	-80(%rbp), %rsi
	jmp	.L477
	.p2align 4,,10
	.p2align 3
.L432:
	movq	41112(%r13), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L434
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r15
	jmp	.L429
	.p2align 4,,10
	.p2align 3
.L434:
	movq	41088(%r13), %r15
	cmpq	41096(%r13), %r15
	je	.L537
.L436:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r15)
	jmp	.L429
.L537:
	movq	%r13, %rdi
	movq	%rsi, -160(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-160(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L436
.L535:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L476
.L536:
	movq	%r13, %rdi
	movq	%rsi, -160(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-160(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L442
.L533:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17827:
	.size	_ZN2v88internal3Uri6DecodeEPNS0_7IsolateENS0_6HandleINS0_6StringEEEb, .-_ZN2v88internal3Uri6DecodeEPNS0_7IsolateENS0_6HandleINS0_6StringEEEb
	.section	.rodata._ZN2v88internal3Uri6EncodeEPNS0_7IsolateENS0_6HandleINS0_6StringEEEb.str1.1,"aMS",@progbits,1
.LC4:
	.string	"vector::reserve"
.LC5:
	.string	"unreachable code"
	.section	.rodata
.LC6:
	.string	""
	.zero	3
	.section	.text._ZN2v88internal3Uri6EncodeEPNS0_7IsolateENS0_6HandleINS0_6StringEEEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Uri6EncodeEPNS0_7IsolateENS0_6HandleINS0_6StringEEEb
	.type	_ZN2v88internal3Uri6EncodeEPNS0_7IsolateENS0_6HandleINS0_6StringEEEb, @function
_ZN2v88internal3Uri6EncodeEPNS0_7IsolateENS0_6HandleINS0_6StringEEEb:
.LFB17854:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movb	%dl, -145(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	%rax, %rsi
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L693
.L540:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	jbe	.L549
.L692:
	movq	0(%r13), %rsi
.L548:
	movslq	11(%rsi), %r14
	pxor	%xmm0, %xmm0
	movq	$0, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	movl	%r14d, -120(%rbp)
	testl	%r14d, %r14d
	js	.L694
	testq	%r14, %r14
	jne	.L695
	movq	0(%r13), %rax
	leaq	-97(%rbp), %rsi
	movq	%rax, -96(%rbp)
	leaq	-96(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
.L659:
	movq	-80(%rbp), %rdx
	movq	-72(%rbp), %rax
	movq	%r12, %rdi
	movq	-136(%rbp), %rsi
	subq	%rdx, %rax
	movq	%rdx, -96(%rbp)
	xorl	%edx, %edx
	cltq
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %r12
	.p2align 4,,10
	.p2align 3
.L655:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L657
	call	_ZdlPv@PLT
.L657:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L696
	addq	$136, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L549:
	.cfi_restore_state
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	jne	.L692
	movq	0(%r13), %rax
	movq	41112(%r12), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L552
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
	jmp	.L692
	.p2align 4,,10
	.p2align 3
.L693:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L540
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L543
	movq	23(%rax), %rdx
	movl	11(%rdx), %edx
	testl	%edx, %edx
	je	.L543
	movq	%r13, %rsi
	xorl	%edx, %edx
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	movq	(%rax), %rsi
	movq	%rax, %r13
	jmp	.L548
	.p2align 4,,10
	.p2align 3
.L695:
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	-80(%rbp), %r15
	movq	-72(%rbp), %rdx
	movq	%rax, %rbx
	subq	%r15, %rdx
	testq	%rdx, %rdx
	jg	.L697
	testq	%r15, %r15
	jne	.L558
.L559:
	movq	%rbx, %xmm0
	addq	%r14, %rbx
	leaq	-97(%rbp), %rsi
	punpcklqdq	%xmm0, %xmm0
	movq	%rbx, -64(%rbp)
	xorl	%ebx, %ebx
	movaps	%xmm0, -80(%rbp)
	movq	0(%r13), %rax
	movq	%rax, -96(%rbp)
	leaq	-96(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	movq	%r12, -128(%rbp)
	leaq	.L615(%rip), %r11
	movq	%r13, %r12
	movq	%rax, %r8
	leaq	-80(%rbp), %rax
	shrq	$32, %rdx
	movabsq	$113808043409408, %r10
	movq	%rax, -144(%rbp)
	movl	%edx, %r14d
	movq	%r8, %r15
.L656:
	movslq	%ebx, %rax
	cmpl	$1, %r14d
	je	.L698
	movzwl	(%r15,%rax,2), %ecx
	leaq	(%rax,%rax), %rsi
	movl	%ecx, %eax
	andw	$-1024, %ax
	cmpw	$-10240, %ax
	je	.L699
	cmpw	$-9216, %ax
	jne	.L561
.L690:
	movq	-128(%rbp), %r12
.L658:
	movq	12464(%r12), %rax
	movq	39(%rax), %rax
	movq	1775(%rax), %r13
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L652
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L653:
	movq	%r12, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$339, %edx
	call	_ZN2v88internal7Factory8NewErrorENS0_6HandleINS0_10JSFunctionEEENS0_15MessageTemplateENS2_INS0_6ObjectEEES7_S7_@PLT
	movq	%r12, %rdi
	xorl	%edx, %edx
	xorl	%r12d, %r12d
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	jmp	.L655
	.p2align 4,,10
	.p2align 3
.L697:
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	memmove@PLT
.L558:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	jmp	.L559
	.p2align 4,,10
	.p2align 3
.L552:
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L700
.L554:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L548
	.p2align 4,,10
	.p2align 3
.L699:
	leal	1(%rbx), %r13d
	cmpl	%r13d, -120(%rbp)
	jle	.L690
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	andl	$15, %eax
	cmpw	$13, %ax
	ja	.L564
	leaq	.L566(%rip), %rdi
	movzwl	%ax, %eax
	movslq	(%rdi,%rax,4), %rax
	addq	%rdi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal3Uri6EncodeEPNS0_7IsolateENS0_6HandleINS0_6StringEEEb,"a",@progbits
	.align 4
	.align 4
.L566:
	.long	.L572-.L566
	.long	.L569-.L566
	.long	.L571-.L566
	.long	.L567-.L566
	.long	.L564-.L566
	.long	.L565-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L690-.L566
	.long	.L569-.L566
	.long	.L568-.L566
	.long	.L567-.L566
	.long	.L564-.L566
	.long	.L565-.L566
	.section	.text._ZN2v88internal3Uri6EncodeEPNS0_7IsolateENS0_6HandleINS0_6StringEEEb
	.p2align 4,,10
	.p2align 3
.L565:
	movq	-136(%rbp), %rdi
	movl	%r13d, %esi
	movl	%ecx, -160(%rbp)
	movq	%rdx, -96(%rbp)
	call	_ZN2v88internal10ThinString3GetEi@PLT
	movl	-160(%rbp), %ecx
	leaq	.L615(%rip), %r11
	movabsq	$113808043409408, %r10
.L573:
	movl	%eax, %edx
	andw	$-1024, %dx
	cmpw	$-9216, %dx
	jne	.L690
	movl	%ecx, %ebx
	andl	$1023, %eax
	movb	$37, -96(%rbp)
	movq	-72(%rbp), %rsi
	sall	$10, %ebx
	andl	$1047552, %ebx
	orl	%ebx, %eax
	leal	65536(%rax), %ebx
	movl	%ebx, %edi
	movl	%ebx, %ecx
	movl	%ebx, %r9d
	shrl	$6, %edi
	shrl	$12, %ecx
	movl	%edi, %edx
	movl	%ecx, %eax
	movl	%edi, -164(%rbp)
	shrl	$18, %r9d
	andl	$63, %edx
	andl	$63, %eax
	orl	$-128, %edx
	orl	$-128, %eax
	movb	%dl, -160(%rbp)
	movl	%ebx, %edx
	andl	$63, %edx
	orl	$-128, %edx
	movb	%dl, -152(%rbp)
	cmpq	%rsi, -64(%rbp)
	je	.L701
	movb	$37, (%rsi)
	movq	-72(%rbp), %rdi
	leaq	1(%rdi), %rsi
	movq	%rsi, -72(%rbp)
.L575:
	movb	$70, -96(%rbp)
	cmpq	%rsi, -64(%rbp)
	je	.L702
	movb	$70, (%rsi)
	movq	-72(%rbp), %rdi
	leaq	1(%rdi), %rsi
	movq	%rsi, -72(%rbp)
.L577:
	addl	$48, %r9d
	movb	%r9b, -96(%rbp)
	cmpq	%rsi, -64(%rbp)
	je	.L703
	movb	%r9b, (%rsi)
	addq	$1, -72(%rbp)
.L579:
	movb	$37, -96(%rbp)
	movq	-72(%rbp), %rsi
	movzbl	%al, %eax
	cmpq	%rsi, -64(%rbp)
	je	.L704
	movb	$37, (%rsi)
	movq	-72(%rbp), %rdi
	leaq	1(%rdi), %rsi
	movq	%rsi, -72(%rbp)
.L581:
	sarl	$4, %eax
	leal	55(%rax), %edi
	leal	48(%rax), %edx
	cmpl	$10, %eax
	cmovge	%edi, %edx
	movb	%dl, -96(%rbp)
	cmpq	%rsi, -64(%rbp)
	je	.L705
	movb	%dl, (%rsi)
	movq	-72(%rbp), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, -72(%rbp)
.L585:
	andl	$15, %ecx
	leal	55(%rcx), %edx
	leal	48(%rcx), %eax
	cmpb	$10, %cl
	cmovnb	%edx, %eax
	movb	%al, -96(%rbp)
	cmpq	%rsi, -64(%rbp)
	je	.L706
	movb	%al, (%rsi)
	addq	$1, -72(%rbp)
.L589:
	movzbl	-160(%rbp), %eax
	movq	-72(%rbp), %rsi
	movb	$37, -96(%rbp)
	cmpq	-64(%rbp), %rsi
	je	.L707
	movb	$37, (%rsi)
	movq	-72(%rbp), %rcx
	leaq	1(%rcx), %rsi
	movq	%rsi, -72(%rbp)
.L591:
	sarl	$4, %eax
	leal	55(%rax), %ecx
	leal	48(%rax), %edx
	cmpl	$10, %eax
	cmovge	%ecx, %edx
	movb	%dl, -96(%rbp)
	cmpq	%rsi, -64(%rbp)
	je	.L708
	movb	%dl, (%rsi)
	movq	-72(%rbp), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, -72(%rbp)
.L595:
	movzbl	-164(%rbp), %eax
	andl	$15, %eax
	leal	55(%rax), %ecx
	leal	48(%rax), %edx
	cmpb	$10, %al
	movl	%ecx, %eax
	cmovb	%edx, %eax
	movb	%al, -96(%rbp)
	cmpq	-64(%rbp), %rsi
	je	.L709
	movb	%al, (%rsi)
	addq	$1, -72(%rbp)
.L599:
	movzbl	-152(%rbp), %eax
	movq	-72(%rbp), %rsi
	movb	$37, -96(%rbp)
	cmpq	-64(%rbp), %rsi
	je	.L600
	movb	$37, (%rsi)
	movq	-72(%rbp), %rcx
	leaq	1(%rcx), %rsi
	movq	%rsi, -72(%rbp)
.L601:
	sarl	$4, %eax
	leal	48(%rax), %ecx
	leal	55(%rax), %edx
	cmpl	$9, %eax
	movl	%ecx, %eax
	cmovg	%edx, %eax
	movb	%al, -96(%rbp)
	cmpq	%rsi, -64(%rbp)
	je	.L604
	movb	%al, (%rsi)
	movq	-72(%rbp), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, -72(%rbp)
.L605:
	andl	$15, %ebx
	leal	48(%rbx), %edx
	leal	55(%rbx), %eax
	cmpb	$9, %bl
	cmovbe	%edx, %eax
	movb	%al, -96(%rbp)
	cmpq	%rsi, -64(%rbp)
	je	.L608
	movb	%al, (%rsi)
	addq	$1, -72(%rbp)
.L609:
	movl	%r13d, %ebx
	jmp	.L610
	.p2align 4,,10
	.p2align 3
.L567:
	movq	-136(%rbp), %rdi
	movl	%r13d, %esi
	movl	%ecx, -160(%rbp)
	movq	%rdx, -96(%rbp)
	call	_ZN2v88internal12SlicedString3GetEi@PLT
	movl	-160(%rbp), %ecx
	leaq	.L615(%rip), %r11
	movabsq	$113808043409408, %r10
	jmp	.L573
	.p2align 4,,10
	.p2align 3
.L569:
	movq	-136(%rbp), %rdi
	movl	%r13d, %esi
	movl	%ecx, -160(%rbp)
	movq	%rdx, -96(%rbp)
	call	_ZN2v88internal10ConsString3GetEi@PLT
	movl	-160(%rbp), %ecx
	leaq	.L615(%rip), %r11
	movabsq	$113808043409408, %r10
	jmp	.L573
	.p2align 4,,10
	.p2align 3
.L568:
	movq	15(%rdx), %rdi
	movq	-128(%rbp), %r12
	movq	(%rdi), %rax
	call	*48(%rax)
	jmp	.L658
	.p2align 4,,10
	.p2align 3
.L571:
	movq	15(%rdx), %rdi
	movl	%ecx, -152(%rbp)
	movq	%rsi, -160(%rbp)
	movq	(%rdi), %rax
	call	*48(%rax)
	movq	-160(%rbp), %rsi
	movl	-152(%rbp), %ecx
	leaq	.L615(%rip), %r11
	movabsq	$113808043409408, %r10
	movzwl	2(%rsi,%rax), %eax
	jmp	.L573
	.p2align 4,,10
	.p2align 3
.L572:
	leal	18(%rbx,%rbx), %eax
	cltq
	movzwl	-1(%rdx,%rax), %eax
	jmp	.L573
	.p2align 4,,10
	.p2align 3
.L698:
	movzbl	(%r15,%rax), %ecx
.L561:
	movl	%ecx, %eax
	orl	$32, %eax
	movzwl	%ax, %eax
	subl	$97, %eax
	cmpl	$25, %eax
	jbe	.L611
	movzwl	%cx, %edx
	leal	-48(%rdx), %eax
	cmpl	$9, %eax
	jbe	.L611
	cmpw	$46, %cx
	ja	.L612
	cmpw	$32, %cx
	ja	.L710
.L613:
	cmpb	$0, -145(%rbp)
	je	.L614
	leal	-35(%rcx), %eax
	cmpw	$29, %ax
	ja	.L614
	movzwl	%ax, %eax
	movslq	(%r11,%rax,4), %rax
	addq	%r11, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal3Uri6EncodeEPNS0_7IsolateENS0_6HandleINS0_6StringEEEb
	.align 4
	.align 4
.L615:
	.long	.L611-.L615
	.long	.L611-.L615
	.long	.L614-.L615
	.long	.L611-.L615
	.long	.L614-.L615
	.long	.L614-.L615
	.long	.L614-.L615
	.long	.L614-.L615
	.long	.L611-.L615
	.long	.L611-.L615
	.long	.L614-.L615
	.long	.L614-.L615
	.long	.L611-.L615
	.long	.L614-.L615
	.long	.L614-.L615
	.long	.L614-.L615
	.long	.L614-.L615
	.long	.L614-.L615
	.long	.L614-.L615
	.long	.L614-.L615
	.long	.L614-.L615
	.long	.L614-.L615
	.long	.L614-.L615
	.long	.L611-.L615
	.long	.L611-.L615
	.long	.L614-.L615
	.long	.L611-.L615
	.long	.L614-.L615
	.long	.L611-.L615
	.long	.L611-.L615
	.section	.text._ZN2v88internal3Uri6EncodeEPNS0_7IsolateENS0_6HandleINS0_6StringEEEb
	.p2align 4,,10
	.p2align 3
.L710:
	btq	%rdx, %r10
	jnc	.L613
.L611:
	movb	%cl, -96(%rbp)
	movq	-72(%rbp), %rsi
	cmpq	-64(%rbp), %rsi
	je	.L650
	movb	%cl, (%rsi)
	addq	$1, -72(%rbp)
.L610:
	addl	$1, %ebx
	cmpl	%ebx, -120(%rbp)
	jg	.L656
	movq	-128(%rbp), %r12
	jmp	.L659
.L614:
	movl	.LC6(%rip), %r13d
	cmpl	$127, %edx
	jbe	.L711
	movl	%edx, %eax
	andl	$63, %ecx
	shrl	$6, %eax
	orl	$-128, %ecx
	cmpl	$2047, %edx
	jbe	.L712
	shrl	$12, %edx
	andl	$63, %eax
	movzbl	%cl, %ecx
	orl	$-32, %edx
	orl	$-128, %eax
	sall	$16, %ecx
	movb	%dl, %r13b
	movl	%r13d, %edx
	movb	%al, %dh
	movl	%edx, %r13d
	andl	$-16711681, %r13d
	orl	%ecx, %r13d
	movl	$3, %ecx
.L619:
	movb	$37, -96(%rbp)
	movq	-72(%rbp), %rsi
	movl	%r13d, %eax
	movzbl	%r13b, %r9d
	cmpq	-64(%rbp), %rsi
	je	.L713
	movb	$37, (%rsi)
	movq	-72(%rbp), %rdi
	leaq	1(%rdi), %rsi
	movq	%rsi, -72(%rbp)
.L622:
	movl	%r9d, %edx
	sarl	$4, %edx
	leal	55(%rdx), %r9d
	leal	48(%rdx), %edi
	cmpl	$10, %edx
	cmovge	%r9d, %edi
	movb	%dil, -96(%rbp)
	cmpq	%rsi, -64(%rbp)
	je	.L714
	movb	%dil, (%rsi)
	movq	-72(%rbp), %rdi
	leaq	1(%rdi), %rsi
	movq	%rsi, -72(%rbp)
.L626:
	andl	$15, %eax
	leal	55(%rax), %edi
	leal	48(%rax), %edx
	cmpb	$10, %al
	movl	%edi, %eax
	cmovb	%edx, %eax
	movb	%al, -96(%rbp)
	cmpq	%rsi, -64(%rbp)
	je	.L715
	movb	%al, (%rsi)
	addq	$1, -72(%rbp)
.L630:
	cmpl	$1, %ecx
	je	.L610
	movl	%r13d, %edx
	movl	%r13d, %eax
	movb	$37, -96(%rbp)
	movq	-72(%rbp), %rsi
	movzbl	%dh, %edi
	movsbl	%ah, %eax
	movl	%edi, %r9d
	cmpq	%rsi, -64(%rbp)
	je	.L716
	movb	$37, (%rsi)
	movq	-72(%rbp), %rdi
	leaq	1(%rdi), %rsi
	movq	%rsi, -72(%rbp)
.L633:
	movl	%r9d, %edx
	sarl	$4, %edx
	leal	55(%rdx), %r9d
	leal	48(%rdx), %edi
	cmpl	$10, %edx
	cmovge	%r9d, %edi
	movb	%dil, -96(%rbp)
	cmpq	%rsi, -64(%rbp)
	je	.L717
	movb	%dil, (%rsi)
	movq	-72(%rbp), %rdi
	leaq	1(%rdi), %rsi
	movq	%rsi, -72(%rbp)
.L637:
	andl	$15, %eax
	leal	55(%rax), %edi
	leal	48(%rax), %edx
	cmpb	$10, %al
	movl	%edi, %eax
	cmovb	%edx, %eax
	movb	%al, -96(%rbp)
	cmpq	%rsi, -64(%rbp)
	je	.L718
	movb	%al, (%rsi)
	addq	$1, -72(%rbp)
.L641:
	cmpl	$2, %ecx
	je	.L610
	sall	$8, %r13d
	movb	$37, -96(%rbp)
	movq	-72(%rbp), %rsi
	movl	%r13d, %eax
	shrl	$24, %r13d
	sarl	$24, %eax
	cmpq	-64(%rbp), %rsi
	je	.L642
	movb	$37, (%rsi)
	movq	-72(%rbp), %rcx
	leaq	1(%rcx), %rsi
	movq	%rsi, -72(%rbp)
.L643:
	sarl	$4, %r13d
	leal	48(%r13), %ecx
	leal	55(%r13), %edx
	cmpl	$9, %r13d
	cmovle	%ecx, %edx
	movb	%dl, -96(%rbp)
	cmpq	-64(%rbp), %rsi
	je	.L646
	movb	%dl, (%rsi)
	movq	-72(%rbp), %rcx
	leaq	1(%rcx), %rsi
	movq	%rsi, -72(%rbp)
.L647:
	andl	$15, %eax
	leal	48(%rax), %ecx
	leal	55(%rax), %edx
	cmpb	$9, %al
	movl	%ecx, %eax
	cmova	%edx, %eax
	movb	%al, -96(%rbp)
	cmpq	-64(%rbp), %rsi
	je	.L650
	movb	%al, (%rsi)
	addq	$1, -72(%rbp)
	jmp	.L610
	.p2align 4,,10
	.p2align 3
.L652:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L719
.L654:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, (%rsi)
	jmp	.L653
	.p2align 4,,10
	.p2align 3
.L612:
	cmpw	$95, %cx
	je	.L611
	cmpw	$126, %cx
	jne	.L613
	jmp	.L611
	.p2align 4,,10
	.p2align 3
.L543:
	movq	41112(%r12), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L545
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r13
	jmp	.L540
	.p2align 4,,10
	.p2align 3
.L719:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L654
	.p2align 4,,10
	.p2align 3
.L711:
	movb	%cl, %r13b
	movl	$1, %ecx
	jmp	.L619
	.p2align 4,,10
	.p2align 3
.L650:
	movq	-136(%rbp), %rdx
	movq	-144(%rbp), %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	leaq	.L615(%rip), %r11
	movabsq	$113808043409408, %r10
	jmp	.L610
	.p2align 4,,10
	.p2align 3
.L545:
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L720
.L547:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L540
.L564:
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L701:
	movq	-136(%rbp), %rdx
	movq	-144(%rbp), %rdi
	movl	%ecx, -176(%rbp)
	movl	%r9d, -172(%rbp)
	movb	%al, -168(%rbp)
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movq	-72(%rbp), %rsi
	movzbl	-168(%rbp), %eax
	leaq	.L615(%rip), %r11
	movl	-172(%rbp), %r9d
	movl	-176(%rbp), %ecx
	movabsq	$113808043409408, %r10
	jmp	.L575
.L720:
	movq	%r12, %rdi
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L547
	.p2align 4,,10
	.p2align 3
.L714:
	movq	-136(%rbp), %rdx
	movq	-144(%rbp), %rdi
	movb	%al, -152(%rbp)
	movl	%ecx, -160(%rbp)
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movq	-72(%rbp), %rsi
	movl	-160(%rbp), %ecx
	leaq	.L615(%rip), %r11
	movzbl	-152(%rbp), %eax
	movabsq	$113808043409408, %r10
	jmp	.L626
	.p2align 4,,10
	.p2align 3
.L713:
	movq	-136(%rbp), %rdx
	movq	-144(%rbp), %rdi
	movl	%r9d, -152(%rbp)
	movl	%ecx, -160(%rbp)
	movb	%r13b, -164(%rbp)
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movq	-72(%rbp), %rsi
	movl	-160(%rbp), %ecx
	leaq	.L615(%rip), %r11
	movl	-152(%rbp), %r9d
	movzbl	-164(%rbp), %eax
	movabsq	$113808043409408, %r10
	jmp	.L622
	.p2align 4,,10
	.p2align 3
.L715:
	movq	-136(%rbp), %rdx
	movq	-144(%rbp), %rdi
	movl	%ecx, -160(%rbp)
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movl	-160(%rbp), %ecx
	leaq	.L615(%rip), %r11
	movabsq	$113808043409408, %r10
	jmp	.L630
	.p2align 4,,10
	.p2align 3
.L712:
	orl	$-64, %eax
	movb	%al, %r13b
	movl	%r13d, %eax
	movb	%cl, %ah
	movl	$2, %ecx
	movl	%eax, %r13d
	jmp	.L619
	.p2align 4,,10
	.p2align 3
.L716:
	movl	%edi, -152(%rbp)
	movq	-136(%rbp), %rdx
	movq	-144(%rbp), %rdi
	movb	%al, -164(%rbp)
	movl	%ecx, -160(%rbp)
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movq	-72(%rbp), %rsi
	movl	-160(%rbp), %ecx
	leaq	.L615(%rip), %r11
	movl	-152(%rbp), %r9d
	movzbl	-164(%rbp), %eax
	movabsq	$113808043409408, %r10
	jmp	.L633
	.p2align 4,,10
	.p2align 3
.L718:
	movq	-136(%rbp), %rdx
	movq	-144(%rbp), %rdi
	movl	%ecx, -160(%rbp)
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movl	-160(%rbp), %ecx
	leaq	.L615(%rip), %r11
	movabsq	$113808043409408, %r10
	jmp	.L641
	.p2align 4,,10
	.p2align 3
.L717:
	movq	-136(%rbp), %rdx
	movq	-144(%rbp), %rdi
	movb	%al, -152(%rbp)
	movl	%ecx, -160(%rbp)
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movq	-72(%rbp), %rsi
	movl	-160(%rbp), %ecx
	leaq	.L615(%rip), %r11
	movzbl	-152(%rbp), %eax
	movabsq	$113808043409408, %r10
	jmp	.L637
	.p2align 4,,10
	.p2align 3
.L646:
	movq	-136(%rbp), %rdx
	movq	-144(%rbp), %rdi
	movl	%eax, -160(%rbp)
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movq	-72(%rbp), %rsi
	movl	-160(%rbp), %eax
	movabsq	$113808043409408, %r10
	leaq	.L615(%rip), %r11
	jmp	.L647
	.p2align 4,,10
	.p2align 3
.L642:
	movq	-136(%rbp), %rdx
	movq	-144(%rbp), %rdi
	movl	%eax, -160(%rbp)
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movq	-72(%rbp), %rsi
	movl	-160(%rbp), %eax
	movabsq	$113808043409408, %r10
	leaq	.L615(%rip), %r11
	jmp	.L643
	.p2align 4,,10
	.p2align 3
.L700:
	movq	%r12, %rdi
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L554
.L604:
	movq	-136(%rbp), %rdx
	movq	-144(%rbp), %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movq	-72(%rbp), %rsi
	leaq	.L615(%rip), %r11
	movabsq	$113808043409408, %r10
	jmp	.L605
.L600:
	movq	-136(%rbp), %rdx
	movq	-144(%rbp), %rdi
	movl	%eax, -160(%rbp)
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movq	-72(%rbp), %rsi
	movl	-160(%rbp), %eax
	movabsq	$113808043409408, %r10
	leaq	.L615(%rip), %r11
	jmp	.L601
.L608:
	movq	-136(%rbp), %rdx
	movq	-144(%rbp), %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	leaq	.L615(%rip), %r11
	movabsq	$113808043409408, %r10
	jmp	.L609
.L709:
	movq	-136(%rbp), %rdx
	movq	-144(%rbp), %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	leaq	.L615(%rip), %r11
	movabsq	$113808043409408, %r10
	jmp	.L599
.L708:
	movq	-136(%rbp), %rdx
	movq	-144(%rbp), %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movq	-72(%rbp), %rsi
	leaq	.L615(%rip), %r11
	movabsq	$113808043409408, %r10
	jmp	.L595
.L707:
	movq	-136(%rbp), %rdx
	movq	-144(%rbp), %rdi
	movl	%eax, -160(%rbp)
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movq	-72(%rbp), %rsi
	movl	-160(%rbp), %eax
	leaq	.L615(%rip), %r11
	movabsq	$113808043409408, %r10
	jmp	.L591
.L706:
	movq	-136(%rbp), %rdx
	movq	-144(%rbp), %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	leaq	.L615(%rip), %r11
	movabsq	$113808043409408, %r10
	jmp	.L589
.L705:
	movq	-136(%rbp), %rdx
	movq	-144(%rbp), %rdi
	movl	%ecx, -168(%rbp)
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movq	-72(%rbp), %rsi
	movl	-168(%rbp), %ecx
	leaq	.L615(%rip), %r11
	movabsq	$113808043409408, %r10
	jmp	.L585
.L704:
	movq	-136(%rbp), %rdx
	movq	-144(%rbp), %rdi
	movl	%ecx, -172(%rbp)
	movl	%eax, -168(%rbp)
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movq	-72(%rbp), %rsi
	movl	-168(%rbp), %eax
	leaq	.L615(%rip), %r11
	movabsq	$113808043409408, %r10
	movl	-172(%rbp), %ecx
	jmp	.L581
.L703:
	movq	-136(%rbp), %rdx
	movq	-144(%rbp), %rdi
	movl	%ecx, -172(%rbp)
	movb	%al, -168(%rbp)
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movzbl	-168(%rbp), %eax
	movl	-172(%rbp), %ecx
	leaq	.L615(%rip), %r11
	movabsq	$113808043409408, %r10
	jmp	.L579
.L702:
	movq	-136(%rbp), %rdx
	movq	-144(%rbp), %rdi
	movl	%ecx, -176(%rbp)
	movl	%r9d, -172(%rbp)
	movb	%al, -168(%rbp)
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movq	-72(%rbp), %rsi
	movzbl	-168(%rbp), %eax
	leaq	.L615(%rip), %r11
	movl	-172(%rbp), %r9d
	movl	-176(%rbp), %ecx
	movabsq	$113808043409408, %r10
	jmp	.L577
.L696:
	call	__stack_chk_fail@PLT
.L694:
	leaq	.LC4(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE17854:
	.size	_ZN2v88internal3Uri6EncodeEPNS0_7IsolateENS0_6HandleINS0_6StringEEEb, .-_ZN2v88internal3Uri6EncodeEPNS0_7IsolateENS0_6HandleINS0_6StringEEEb
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal3Uri6DecodeEPNS0_7IsolateENS0_6HandleINS0_6StringEEEb,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal3Uri6DecodeEPNS0_7IsolateENS0_6HandleINS0_6StringEEEb, @function
_GLOBAL__sub_I__ZN2v88internal3Uri6DecodeEPNS0_7IsolateENS0_6HandleINS0_6StringEEEb:
.LFB21832:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE21832:
	.size	_GLOBAL__sub_I__ZN2v88internal3Uri6DecodeEPNS0_7IsolateENS0_6HandleINS0_6StringEEEb, .-_GLOBAL__sub_I__ZN2v88internal3Uri6DecodeEPNS0_7IsolateENS0_6HandleINS0_6StringEEEb
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal3Uri6DecodeEPNS0_7IsolateENS0_6HandleINS0_6StringEEEb
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
