	.file	"call-optimization.cc"
	.text
	.section	.text._ZNK2v88internal16CallOptimization18GetAccessorContextENS0_3MapE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal16CallOptimization18GetAccessorContextENS0_3MapE
	.type	_ZNK2v88internal16CallOptimization18GetAccessorContextENS0_3MapE, @function
_ZNK2v88internal16CallOptimization18GetAccessorContextENS0_3MapE:
.LFB17916:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L2
	movq	(%rax), %rax
	movq	31(%rax), %rax
	movq	39(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movq	31(%rsi), %rax
	leaq	31(%rax), %rdx
	testb	$1, %al
	jne	.L6
.L5:
	movq	(%rdx), %rax
	movq	39(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movq	-1(%rax), %rcx
	leaq	31(%rax), %rdx
	cmpw	$68, 11(%rcx)
	jne	.L5
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L6
	leaq	31(%rax), %rdx
	jmp	.L5
	.cfi_endproc
.LFE17916:
	.size	_ZNK2v88internal16CallOptimization18GetAccessorContextENS0_3MapE, .-_ZNK2v88internal16CallOptimization18GetAccessorContextENS0_3MapE
	.section	.text._ZNK2v88internal16CallOptimization30IsCrossContextLazyAccessorPairENS0_7ContextENS0_3MapE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal16CallOptimization30IsCrossContextLazyAccessorPairENS0_7ContextENS0_3MapE
	.type	_ZNK2v88internal16CallOptimization30IsCrossContextLazyAccessorPairENS0_7ContextENS0_3MapE, @function
_ZNK2v88internal16CallOptimization30IsCrossContextLazyAccessorPairENS0_7ContextENS0_3MapE:
.LFB17917:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpq	$0, (%rdi)
	je	.L18
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	movq	31(%rdx), %rax
	leaq	31(%rax), %rdx
	testb	$1, %al
	jne	.L14
.L13:
	movq	(%rdx), %rax
	movq	39(%rax), %rax
	cmpq	%rax, %rsi
	setne	%al
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movq	-1(%rax), %rcx
	leaq	31(%rax), %rdx
	cmpw	$68, 11(%rcx)
	jne	.L13
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L14
	leaq	31(%rax), %rdx
	jmp	.L13
	.cfi_endproc
.LFE17917:
	.size	_ZNK2v88internal16CallOptimization30IsCrossContextLazyAccessorPairENS0_7ContextENS0_3MapE, .-_ZNK2v88internal16CallOptimization30IsCrossContextLazyAccessorPairENS0_7ContextENS0_3MapE
	.section	.text._ZNK2v88internal16CallOptimization26LookupHolderOfExpectedTypeENS0_6HandleINS0_3MapEEEPNS1_12HolderLookupE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal16CallOptimization26LookupHolderOfExpectedTypeENS0_6HandleINS0_3MapEEEPNS1_12HolderLookupE
	.type	_ZNK2v88internal16CallOptimization26LookupHolderOfExpectedTypeENS0_6HandleINS0_3MapEEEPNS1_12HolderLookupE, @function
_ZNK2v88internal16CallOptimization26LookupHolderOfExpectedTypeENS0_6HandleINS0_3MapEEEPNS1_12HolderLookupE:
.LFB17918:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpw	$1024, 11(%rsi)
	jbe	.L31
	movq	16(%rdi), %rax
	movq	%rdi, %r13
	testq	%rax, %rax
	je	.L23
	movq	(%rax), %rax
	leaq	-64(%rbp), %r14
	movq	%r14, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal20FunctionTemplateInfo13IsTemplateForENS0_3MapE@PLT
	testb	%al, %al
	je	.L37
.L23:
	movl	$1, (%r12)
	xorl	%eax, %eax
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L37:
	movq	(%rbx), %rax
	cmpw	$1026, 11(%rax)
	je	.L38
	.p2align 4,,10
	.p2align 3
.L31:
	movl	$0, (%r12)
	xorl	%eax, %eax
.L21:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L39
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_restore_state
	movq	23(%rax), %rsi
	movq	%rsi, %rax
	andq	$-262144, %rax
	leaq	24(%rax), %r15
	movq	24(%rax), %rax
	leaq	-37592(%rax), %rdx
	cmpq	%rsi, -37488(%rax)
	je	.L31
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L25
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rbx
	movq	%rsi, %r15
	andq	$-262144, %r15
	addq	$24, %r15
.L26:
	movq	(%r15), %r15
	movq	-1(%rsi), %rsi
	movq	3520(%r15), %rdi
	subq	$37592, %r15
	testq	%rdi, %rdi
	je	.L28
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L29:
	movq	16(%r13), %rdx
	movq	%r14, %rdi
	movq	(%rdx), %rdx
	movq	%rdx, -64(%rbp)
	movq	(%rax), %rsi
	call	_ZN2v88internal20FunctionTemplateInfo13IsTemplateForENS0_3MapE@PLT
	testb	%al, %al
	je	.L31
	movl	$2, (%r12)
	movq	%rbx, %rax
	jmp	.L21
.L25:
	movq	41088(%rdx), %rbx
	cmpq	41096(%rdx), %rbx
	je	.L40
.L27:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%rbx)
	jmp	.L26
.L28:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L41
.L30:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%rsi, (%rax)
	jmp	.L29
.L40:
	movq	%rdx, %rdi
	movq	%rsi, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %rsi
	movq	-72(%rbp), %rdx
	movq	%rax, %rbx
	jmp	.L27
.L41:
	movq	%r15, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	jmp	.L30
.L39:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17918:
	.size	_ZNK2v88internal16CallOptimization26LookupHolderOfExpectedTypeENS0_6HandleINS0_3MapEEEPNS1_12HolderLookupE, .-_ZNK2v88internal16CallOptimization26LookupHolderOfExpectedTypeENS0_6HandleINS0_3MapEEEPNS1_12HolderLookupE
	.section	.text._ZNK2v88internal16CallOptimization20IsCompatibleReceiverENS0_6HandleINS0_6ObjectEEENS2_INS0_8JSObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal16CallOptimization20IsCompatibleReceiverENS0_6HandleINS0_6ObjectEEENS2_INS0_8JSObjectEEE
	.type	_ZNK2v88internal16CallOptimization20IsCompatibleReceiverENS0_6HandleINS0_6ObjectEEENS2_INS0_8JSObjectEEE, @function
_ZNK2v88internal16CallOptimization20IsCompatibleReceiverENS0_6HandleINS0_6ObjectEEENS2_INS0_8JSObjectEEE:
.LFB17919:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rsi), %rcx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rcx, %rax
	notq	%rax
	movl	%eax, %r12d
	andl	$1, %r12d
	je	.L71
.L65:
	xorl	%r12d, %r12d
.L42:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L72
	addq	$40, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	.cfi_restore_state
	movq	(%rdx), %rax
	movq	%rdi, %rbx
	movq	%rdx, %r15
	andq	$-262144, %rax
	movq	24(%rax), %r13
	movq	-1(%rcx), %rsi
	movq	3520(%r13), %rdi
	subq	$37592, %r13
	testq	%rdi, %rdi
	je	.L44
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r14
.L45:
	cmpw	$1024, 11(%rsi)
	jbe	.L42
	movq	16(%rbx), %rax
	testq	%rax, %rax
	je	.L50
	movq	(%rax), %rax
	leaq	-64(%rbp), %r13
	movq	%r13, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal20FunctionTemplateInfo13IsTemplateForENS0_3MapE@PLT
	testb	%al, %al
	jne	.L50
	movq	(%r14), %rax
	cmpw	$1026, 11(%rax)
	jne	.L42
	movq	23(%rax), %rsi
	movq	%rsi, %rax
	andq	$-262144, %rax
	leaq	24(%rax), %rdx
	movq	24(%rax), %rax
	leaq	-37592(%rax), %r14
	cmpq	-37488(%rax), %rsi
	je	.L42
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L52
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rcx
	movq	%rsi, %rdx
	andq	$-262144, %rdx
	addq	$24, %rdx
.L53:
	movq	(%rdx), %r14
	movq	-1(%rsi), %rsi
	movq	3520(%r14), %rdi
	subq	$37592, %r14
	testq	%rdi, %rdi
	je	.L55
	movq	%rcx, -72(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-72(%rbp), %rcx
.L56:
	movq	16(%rbx), %rdx
	movq	%r13, %rdi
	movq	%rcx, -72(%rbp)
	movq	(%rdx), %rdx
	movq	%rdx, -64(%rbp)
	movq	(%rax), %rsi
	call	_ZN2v88internal20FunctionTemplateInfo13IsTemplateForENS0_3MapE@PLT
	testb	%al, %al
	je	.L42
	movq	-72(%rbp), %rcx
	cmpq	%rcx, %r15
	je	.L60
	movq	(%rcx), %rdx
.L70:
	cmpq	(%r15), %rdx
	je	.L60
	movq	-1(%rdx), %rdx
	movq	23(%rdx), %rdx
	testb	$1, %dl
	je	.L65
	movq	-1(%rdx), %rcx
	cmpw	$1024, 11(%rcx)
	jbe	.L65
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L44:
	movq	41088(%r13), %r14
	cmpq	41096(%r13), %r14
	je	.L73
.L46:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r14)
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L50:
	movl	$1, %r12d
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L73:
	movq	%r13, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L60:
	movl	%eax, %r12d
	jmp	.L42
.L55:
	movq	41088(%r14), %rax
	cmpq	41096(%r14), %rax
	je	.L74
.L57:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r14)
	movq	%rsi, (%rax)
	jmp	.L56
.L52:
	movq	41088(%r14), %rcx
	cmpq	41096(%r14), %rcx
	je	.L75
.L54:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%rcx)
	jmp	.L53
.L74:
	movq	%r14, %rdi
	movq	%rsi, -80(%rbp)
	movq	%rcx, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %rsi
	movq	-72(%rbp), %rcx
	jmp	.L57
.L75:
	movq	%r14, %rdi
	movq	%rsi, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %rsi
	movq	-72(%rbp), %rdx
	movq	%rax, %rcx
	jmp	.L54
.L72:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17919:
	.size	_ZNK2v88internal16CallOptimization20IsCompatibleReceiverENS0_6HandleINS0_6ObjectEEENS2_INS0_8JSObjectEEE, .-_ZNK2v88internal16CallOptimization20IsCompatibleReceiverENS0_6HandleINS0_6ObjectEEENS2_INS0_8JSObjectEEE
	.section	.text._ZNK2v88internal16CallOptimization23IsCompatibleReceiverMapENS0_6HandleINS0_3MapEEENS2_INS0_8JSObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal16CallOptimization23IsCompatibleReceiverMapENS0_6HandleINS0_3MapEEENS2_INS0_8JSObjectEEE
	.type	_ZNK2v88internal16CallOptimization23IsCompatibleReceiverMapENS0_6HandleINS0_3MapEEENS2_INS0_8JSObjectEEE, @function
_ZNK2v88internal16CallOptimization23IsCompatibleReceiverMapENS0_6HandleINS0_3MapEEENS2_INS0_8JSObjectEEE:
.LFB17920:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpw	$1024, 11(%rsi)
	jbe	.L77
	movq	16(%rdi), %rax
	movq	%rdi, %r13
	testq	%rax, %rax
	je	.L80
	movq	(%rax), %rax
	leaq	-64(%rbp), %r14
	movq	%rdx, %r12
	movq	%r14, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal20FunctionTemplateInfo13IsTemplateForENS0_3MapE@PLT
	testb	%al, %al
	je	.L100
.L80:
	movl	$1, %eax
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L100:
	movq	(%rbx), %rax
	cmpw	$1026, 11(%rax)
	je	.L101
	.p2align 4,,10
	.p2align 3
.L77:
	xorl	%eax, %eax
.L76:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L102
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L101:
	.cfi_restore_state
	movq	23(%rax), %rsi
	movq	%rsi, %rax
	andq	$-262144, %rax
	leaq	24(%rax), %r15
	movq	24(%rax), %rax
	leaq	-37592(%rax), %rdx
	cmpq	-37488(%rax), %rsi
	je	.L77
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L82
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rbx
	movq	%rsi, %r15
	andq	$-262144, %r15
	addq	$24, %r15
.L83:
	movq	(%r15), %r15
	movq	-1(%rsi), %rsi
	movq	3520(%r15), %rdi
	subq	$37592, %r15
	testq	%rdi, %rdi
	je	.L85
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L86:
	movq	16(%r13), %rdx
	movq	%r14, %rdi
	movq	(%rdx), %rdx
	movq	%rdx, -64(%rbp)
	movq	(%rax), %rsi
	call	_ZN2v88internal20FunctionTemplateInfo13IsTemplateForENS0_3MapE@PLT
	testb	%al, %al
	je	.L77
	cmpq	%rbx, %r12
	je	.L76
	movq	(%rbx), %rdx
	testq	%r12, %r12
	je	.L95
.L99:
	cmpq	%rdx, (%r12)
	je	.L76
.L95:
	movq	-1(%rdx), %rdx
	movq	23(%rdx), %rdx
	testb	$1, %dl
	je	.L77
	movq	-1(%rdx), %rcx
	cmpw	$1024, 11(%rcx)
	jbe	.L77
	jmp	.L99
.L85:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L103
.L87:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%rsi, (%rax)
	jmp	.L86
.L82:
	movq	41088(%rdx), %rbx
	cmpq	41096(%rdx), %rbx
	je	.L104
.L84:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%rbx)
	jmp	.L83
.L103:
	movq	%r15, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	jmp	.L87
.L104:
	movq	%rdx, %rdi
	movq	%rsi, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %rsi
	movq	-72(%rbp), %rdx
	movq	%rax, %rbx
	jmp	.L84
.L102:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17920:
	.size	_ZNK2v88internal16CallOptimization23IsCompatibleReceiverMapENS0_6HandleINS0_3MapEEENS2_INS0_8JSObjectEEE, .-_ZNK2v88internal16CallOptimization23IsCompatibleReceiverMapENS0_6HandleINS0_3MapEEENS2_INS0_8JSObjectEEE
	.section	.text._ZN2v88internal16CallOptimization10InitializeEPNS0_7IsolateENS0_6HandleINS0_20FunctionTemplateInfoEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16CallOptimization10InitializeEPNS0_7IsolateENS0_6HandleINS0_20FunctionTemplateInfoEEE
	.type	_ZN2v88internal16CallOptimization10InitializeEPNS0_7IsolateENS0_6HandleINS0_20FunctionTemplateInfoEEE, @function
_ZN2v88internal16CallOptimization10InitializeEPNS0_7IsolateENS0_6HandleINS0_20FunctionTemplateInfoEEE:
.LFB17921:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	(%rdx), %rax
	movq	47(%rax), %rsi
	cmpq	%rsi, 88(%rbx)
	je	.L105
	movq	%rdi, %r12
	movq	41112(%rbx), %rdi
	movq	%rdx, %r13
	testq	%rdi, %rdi
	je	.L107
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L108:
	movq	%rax, 24(%r12)
	movq	0(%r13), %rax
	movq	63(%rax), %rsi
	cmpq	%rsi, 88(%rbx)
	je	.L110
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L111
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L112:
	movq	%rax, 16(%r12)
.L110:
	movb	$1, 8(%r12)
.L105:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L107:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L115
.L109:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L111:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L116
.L113:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L115:
	movq	%rbx, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L116:
	movq	%rbx, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L113
	.cfi_endproc
.LFE17921:
	.size	_ZN2v88internal16CallOptimization10InitializeEPNS0_7IsolateENS0_6HandleINS0_20FunctionTemplateInfoEEE, .-_ZN2v88internal16CallOptimization10InitializeEPNS0_7IsolateENS0_6HandleINS0_20FunctionTemplateInfoEEE
	.section	.text._ZN2v88internal16CallOptimization26AnalyzePossibleApiFunctionEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16CallOptimization26AnalyzePossibleApiFunctionEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE
	.type	_ZN2v88internal16CallOptimization26AnalyzePossibleApiFunctionEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE, @function
_ZN2v88internal16CallOptimization26AnalyzePossibleApiFunctionEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE:
.LFB17923:
	.cfi_startproc
	endbr64
	movq	(%rdx), %rax
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	testb	$1, %al
	jne	.L135
.L132:
	ret
	.p2align 4,,10
	.p2align 3
.L135:
	movq	-1(%rax), %rax
	cmpw	$88, 11(%rax)
	jne	.L132
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	(%rdx), %rax
	movq	23(%rax), %rax
	movq	7(%rax), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L121
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r13
.L122:
	movq	47(%rsi), %rsi
	cmpq	%rsi, 88(%rbx)
	je	.L117
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L124
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L125:
	movq	%rax, 24(%r12)
	movq	0(%r13), %rax
	movq	63(%rax), %rsi
	cmpq	%rsi, 88(%rbx)
	je	.L127
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L128
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L129:
	movq	%rax, 16(%r12)
.L127:
	movb	$1, 8(%r12)
.L117:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	.cfi_restore_state
	movq	41088(%rbx), %r13
	cmpq	41096(%rbx), %r13
	je	.L136
.L123:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, 0(%r13)
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L128:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L137
.L130:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L124:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L138
.L126:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L136:
	movq	%rbx, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L138:
	movq	%rbx, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L137:
	movq	%rbx, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L130
	.cfi_endproc
.LFE17923:
	.size	_ZN2v88internal16CallOptimization26AnalyzePossibleApiFunctionEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE, .-_ZN2v88internal16CallOptimization26AnalyzePossibleApiFunctionEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE
	.section	.text._ZN2v88internal16CallOptimization10InitializeEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16CallOptimization10InitializeEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE
	.type	_ZN2v88internal16CallOptimization10InitializeEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE, @function
_ZN2v88internal16CallOptimization10InitializeEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE:
.LFB17922:
	.cfi_startproc
	endbr64
	testq	%rdx, %rdx
	je	.L139
	movq	(%rdx), %rax
	movq	47(%rax), %rcx
	cmpl	$67, 59(%rcx)
	jne	.L148
.L139:
	ret
	.p2align 4,,10
	.p2align 3
.L148:
	movabsq	$287762808832, %rcx
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	cmpq	%rcx, %rax
	je	.L139
	testb	$1, %al
	jne	.L149
.L143:
	movq	%rdx, (%rdi)
	jmp	_ZN2v88internal16CallOptimization26AnalyzePossibleApiFunctionEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE
	.p2align 4,,10
	.p2align 3
.L149:
	movq	-1(%rax), %rcx
	cmpw	$165, 11(%rcx)
	je	.L139
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L143
	jmp	.L139
	.cfi_endproc
.LFE17922:
	.size	_ZN2v88internal16CallOptimization10InitializeEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE, .-_ZN2v88internal16CallOptimization10InitializeEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE
	.section	.text._ZN2v88internal16CallOptimizationC2EPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16CallOptimizationC2EPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	.type	_ZN2v88internal16CallOptimizationC2EPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE, @function
_ZN2v88internal16CallOptimizationC2EPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE:
.LFB17914:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movq	$0, (%rdi)
	movups	%xmm0, 16(%rdi)
	movq	(%rdx), %rax
	movb	$0, 8(%rdi)
	testb	$1, %al
	jne	.L157
.L150:
	ret
	.p2align 4,,10
	.p2align 3
.L157:
	movq	-1(%rax), %rcx
	cmpw	$1105, 11(%rcx)
	je	.L153
	movq	-1(%rax), %rax
	cmpw	$88, 11(%rax)
	jne	.L150
	jmp	_ZN2v88internal16CallOptimization10InitializeEPNS0_7IsolateENS0_6HandleINS0_20FunctionTemplateInfoEEE
	.p2align 4,,10
	.p2align 3
.L153:
	jmp	_ZN2v88internal16CallOptimization10InitializeEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE
	.cfi_endproc
.LFE17914:
	.size	_ZN2v88internal16CallOptimizationC2EPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE, .-_ZN2v88internal16CallOptimizationC2EPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	.globl	_ZN2v88internal16CallOptimizationC1EPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	.set	_ZN2v88internal16CallOptimizationC1EPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE,_ZN2v88internal16CallOptimizationC2EPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal16CallOptimizationC2EPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal16CallOptimizationC2EPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE, @function
_GLOBAL__sub_I__ZN2v88internal16CallOptimizationC2EPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE:
.LFB21698:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE21698:
	.size	_GLOBAL__sub_I__ZN2v88internal16CallOptimizationC2EPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE, .-_GLOBAL__sub_I__ZN2v88internal16CallOptimizationC2EPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal16CallOptimizationC2EPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
