	.file	"global-handles.cc"
	.text
	.section	.text._ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"axG",@progbits,_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.type	_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
.LFB1858:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE1858:
	.size	_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.section	.text._ZN2v823PersistentHandleVisitor21VisitPersistentHandleEPNS_10PersistentINS_5ValueENS_27NonCopyablePersistentTraitsIS2_EEEEt,"axG",@progbits,_ZN2v823PersistentHandleVisitor21VisitPersistentHandleEPNS_10PersistentINS_5ValueENS_27NonCopyablePersistentTraitsIS2_EEEEt,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v823PersistentHandleVisitor21VisitPersistentHandleEPNS_10PersistentINS_5ValueENS_27NonCopyablePersistentTraitsIS2_EEEEt
	.type	_ZN2v823PersistentHandleVisitor21VisitPersistentHandleEPNS_10PersistentINS_5ValueENS_27NonCopyablePersistentTraitsIS2_EEEEt, @function
_ZN2v823PersistentHandleVisitor21VisitPersistentHandleEPNS_10PersistentINS_5ValueENS_27NonCopyablePersistentTraitsIS2_EEEEt:
.LFB2545:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2545:
	.size	_ZN2v823PersistentHandleVisitor21VisitPersistentHandleEPNS_10PersistentINS_5ValueENS_27NonCopyablePersistentTraitsIS2_EEEEt, .-_ZN2v823PersistentHandleVisitor21VisitPersistentHandleEPNS_10PersistentINS_5ValueENS_27NonCopyablePersistentTraitsIS2_EEEEt
	.section	.text._ZN2v818EmbedderHeapTracer21IsRootForNonTracingGCERKNS_12TracedGlobalINS_5ValueEEE,"axG",@progbits,_ZN2v818EmbedderHeapTracer21IsRootForNonTracingGCERKNS_12TracedGlobalINS_5ValueEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v818EmbedderHeapTracer21IsRootForNonTracingGCERKNS_12TracedGlobalINS_5ValueEEE
	.type	_ZN2v818EmbedderHeapTracer21IsRootForNonTracingGCERKNS_12TracedGlobalINS_5ValueEEE, @function
_ZN2v818EmbedderHeapTracer21IsRootForNonTracingGCERKNS_12TracedGlobalINS_5ValueEEE:
.LFB2549:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE2549:
	.size	_ZN2v818EmbedderHeapTracer21IsRootForNonTracingGCERKNS_12TracedGlobalINS_5ValueEEE, .-_ZN2v818EmbedderHeapTracer21IsRootForNonTracingGCERKNS_12TracedGlobalINS_5ValueEEE
	.section	.text._ZN2v818EmbedderHeapTracer25ResetHandleInNonTracingGCERKNS_12TracedGlobalINS_5ValueEEE,"axG",@progbits,_ZN2v818EmbedderHeapTracer25ResetHandleInNonTracingGCERKNS_12TracedGlobalINS_5ValueEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v818EmbedderHeapTracer25ResetHandleInNonTracingGCERKNS_12TracedGlobalINS_5ValueEEE
	.type	_ZN2v818EmbedderHeapTracer25ResetHandleInNonTracingGCERKNS_12TracedGlobalINS_5ValueEEE, @function
_ZN2v818EmbedderHeapTracer25ResetHandleInNonTracingGCERKNS_12TracedGlobalINS_5ValueEEE:
.LFB2550:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2550:
	.size	_ZN2v818EmbedderHeapTracer25ResetHandleInNonTracingGCERKNS_12TracedGlobalINS_5ValueEEE, .-_ZN2v818EmbedderHeapTracer25ResetHandleInNonTracingGCERKNS_12TracedGlobalINS_5ValueEEE
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB4783:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE4783:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB4784:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4784:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB4786:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4786:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.text._ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE,"axG",@progbits,_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE
	.type	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE, @function
_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE:
.LFB7719:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	leaq	8(%rcx), %r8
	movq	16(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE7719:
	.size	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE, .-_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal13GlobalHandles42InvokeOrScheduleSecondPassPhantomCallbacksEbEUlvE_E10_M_managerERSt9_Any_dataRKS6_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal13GlobalHandles42InvokeOrScheduleSecondPassPhantomCallbacksEbEUlvE_E10_M_managerERSt9_Any_dataRKS6_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal13GlobalHandles42InvokeOrScheduleSecondPassPhantomCallbacksEbEUlvE_E10_M_managerERSt9_Any_dataRKS6_St18_Manager_operation:
.LFB22557:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L11
	cmpl	$3, %edx
	je	.L12
	cmpl	$1, %edx
	je	.L16
.L12:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE22557:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal13GlobalHandles42InvokeOrScheduleSecondPassPhantomCallbacksEbEUlvE_E10_M_managerERSt9_Any_dataRKS6_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal13GlobalHandles42InvokeOrScheduleSecondPassPhantomCallbacksEbEUlvE_E10_M_managerERSt9_Any_dataRKS6_St18_Manager_operation
	.section	.text._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB22653:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.cfi_endproc
.LFE22653:
	.size	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZN2v88internal13GlobalHandles32InvokeSecondPassPhantomCallbacksEv.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal13GlobalHandles32InvokeSecondPassPhantomCallbacksEv.part.0, @function
_ZN2v88internal13GlobalHandles32InvokeSecondPassPhantomCallbacksEv.part.0:
.LFB24164:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-96(%rbp), %r13
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movb	$1, 161(%rdi)
	movq	%r13, %rdi
	call	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb1EEC1EPNS0_7IsolateE@PLT
	movq	144(%rbx), %rax
	cmpq	%rax, 136(%rbx)
	je	.L19
	leaq	-80(%rbp), %r12
	.p2align 4,,10
	.p2align 3
.L20:
	movq	-24(%rax), %rdi
	movq	(%rbx), %xmm0
	subq	$32, %rax
	movq	16(%rax), %rsi
	movq	24(%rax), %rcx
	movq	%rdi, %xmm1
	movq	(%rax), %rdx
	movq	%r12, %rdi
	movq	%rax, 144(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movq	$0, -64(%rbp)
	movq	%rsi, -56(%rbp)
	movq	%rcx, -48(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	*%rdx
	movq	144(%rbx), %rax
	cmpq	136(%rbx), %rax
	jne	.L20
.L19:
	movb	$0, 161(%rbx)
	movq	%r13, %rdi
	call	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb1EED1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L24
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L24:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24164:
	.size	_ZN2v88internal13GlobalHandles32InvokeSecondPassPhantomCallbacksEv.part.0, .-_ZN2v88internal13GlobalHandles32InvokeSecondPassPhantomCallbacksEv.part.0
	.section	.text._ZN2v88internal12_GLOBAL__N_121ExtractInternalFieldsENS0_8JSObjectEPPvi.constprop.1,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_121ExtractInternalFieldsENS0_8JSObjectEPPvi.constprop.1, @function
_ZN2v88internal12_GLOBAL__N_121ExtractInternalFieldsENS0_8JSObjectEPPvi.constprop.1:
.LFB24354:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	-1(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	-1(%rdi), %rbx
	movzbl	7(%rbx), %r12d
	sall	$3, %r12d
	jne	.L47
.L26:
	movl	%r12d, %r12d
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L32:
	cmpq	%rbx, %r12
	je	.L25
	movq	(%r14), %rdx
	movl	$24, %eax
	movzwl	11(%rdx), %edi
	cmpw	$1057, %di
	je	.L29
	movsbl	13(%rdx), %esi
	shrl	$31, %esi
	call	_ZN2v88internal8JSObject13GetHeaderSizeENS0_12InstanceTypeEb@PLT
.L29:
	leal	(%rax,%rbx,8), %eax
	cltq
	movq	(%rax,%r14), %rax
	testb	$1, %al
	jne	.L30
	movq	%rax, 0(%r13,%rbx,8)
.L30:
	addq	$1, %rbx
	cmpq	$2, %rbx
	jne	.L32
.L25:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore_state
	movzwl	11(%rbx), %edi
	movl	$24, %ecx
	cmpw	$1057, %di
	je	.L27
	movsbl	13(%rbx), %esi
	shrl	$31, %esi
	call	_ZN2v88internal8JSObject13GetHeaderSizeENS0_12InstanceTypeEb@PLT
	movl	%eax, %ecx
.L27:
	movzbl	7(%rbx), %eax
	movzbl	8(%rbx), %edx
	subl	%ecx, %r12d
	sarl	$3, %r12d
	subl	%edx, %eax
	subl	%eax, %r12d
	jmp	.L26
	.cfi_endproc
.LFE24354:
	.size	_ZN2v88internal12_GLOBAL__N_121ExtractInternalFieldsENS0_8JSObjectEPPvi.constprop.1, .-_ZN2v88internal12_GLOBAL__N_121ExtractInternalFieldsENS0_8JSObjectEPPvi.constprop.1
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB18279:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L54
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L57
.L48:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L48
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE18279:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.text._ZN2v88internal13GlobalHandlesC2EPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13GlobalHandlesC2EPNS0_7IsolateE
	.type	_ZN2v88internal13GlobalHandlesC2EPNS0_7IsolateE, @function
_ZN2v88internal13GlobalHandlesC2EPNS0_7IsolateE:
.LFB18853:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rsi, (%rdi)
	movl	$32, %edi
	call	_Znwm@PLT
	movq	$0, 16(%rbx)
	movl	$32, %edi
	movq	%rbx, (%rax)
	movq	%rax, 8(%rbx)
	movq	$0, 24(%rbx)
	movq	$0, 32(%rbx)
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	$0, 48(%rbx)
	movq	%rbx, (%rax)
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	%rax, 40(%rbx)
	xorl	%eax, %eax
	movq	$0, 56(%rbx)
	movq	$0, 64(%rbx)
	movq	$0, 152(%rbx)
	movw	%ax, 160(%rbx)
	movl	$0, 164(%rbx)
	movups	%xmm0, 72(%rbx)
	movups	%xmm0, 88(%rbx)
	movups	%xmm0, 104(%rbx)
	movups	%xmm0, 120(%rbx)
	movups	%xmm0, 136(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18853:
	.size	_ZN2v88internal13GlobalHandlesC2EPNS0_7IsolateE, .-_ZN2v88internal13GlobalHandlesC2EPNS0_7IsolateE
	.globl	_ZN2v88internal13GlobalHandlesC1EPNS0_7IsolateE
	.set	_ZN2v88internal13GlobalHandlesC1EPNS0_7IsolateE,_ZN2v88internal13GlobalHandlesC2EPNS0_7IsolateE
	.section	.text._ZN2v88internal13GlobalHandlesD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13GlobalHandlesD2Ev
	.type	_ZN2v88internal13GlobalHandlesD2Ev, @function
_ZN2v88internal13GlobalHandlesD2Ev:
.LFB18856:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %r13
	movq	$0, 8(%rdi)
	testq	%r13, %r13
	je	.L61
	movq	8(%r13), %r12
	testq	%r12, %r12
	je	.L62
	.p2align 4,,10
	.p2align 3
.L63:
	movq	%r12, %rdi
	movq	8192(%r12), %r12
	movl	$8240, %esi
	call	_ZdlPvm@PLT
	testq	%r12, %r12
	jne	.L63
.L62:
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L61:
	movq	136(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L64
	call	_ZdlPv@PLT
.L64:
	movq	112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L65
	call	_ZdlPv@PLT
.L65:
	movq	88(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L66
	call	_ZdlPv@PLT
.L66:
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L67
	call	_ZdlPv@PLT
.L67:
	movq	40(%rbx), %r13
	testq	%r13, %r13
	je	.L68
	movq	8(%r13), %r12
	testq	%r12, %r12
	je	.L69
	.p2align 4,,10
	.p2align 3
.L70:
	movq	%r12, %rdi
	movq	8192(%r12), %r12
	movl	$8240, %esi
	call	_ZdlPvm@PLT
	testq	%r12, %r12
	jne	.L70
.L69:
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L68:
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L71
	call	_ZdlPv@PLT
.L71:
	movq	8(%rbx), %r12
	testq	%r12, %r12
	je	.L60
	movq	8(%r12), %rbx
	testq	%rbx, %rbx
	je	.L73
	.p2align 4,,10
	.p2align 3
.L74:
	movq	%rbx, %rdi
	movq	8192(%rbx), %rbx
	movl	$8240, %esi
	call	_ZdlPvm@PLT
	testq	%rbx, %rbx
	jne	.L74
.L73:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$32, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L60:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18856:
	.size	_ZN2v88internal13GlobalHandlesD2Ev, .-_ZN2v88internal13GlobalHandlesD2Ev
	.globl	_ZN2v88internal13GlobalHandlesD1Ev
	.set	_ZN2v88internal13GlobalHandlesD1Ev,_ZN2v88internal13GlobalHandlesD2Ev
	.section	.text._ZN2v88internal13GlobalHandles10MoveGlobalEPPmS3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13GlobalHandles10MoveGlobalEPPmS3_
	.type	_ZN2v88internal13GlobalHandles10MoveGlobalEPPmS3_, @function
_ZN2v88internal13GlobalHandles10MoveGlobalEPPmS3_:
.LFB18864:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movzbl	11(%rdx), %eax
	movl	%eax, %ecx
	andl	$7, %ecx
	cmpb	$2, %cl
	je	.L111
.L109:
	ret
	.p2align 4,,10
	.p2align 3
.L111:
	shrb	$6, %al
	cmpb	$3, %al
	jne	.L109
	movq	%rsi, 16(%rdx)
	ret
	.cfi_endproc
.LFE18864:
	.size	_ZN2v88internal13GlobalHandles10MoveGlobalEPPmS3_, .-_ZN2v88internal13GlobalHandles10MoveGlobalEPPmS3_
	.section	.text._ZN2v88internal13GlobalHandles16MoveTracedGlobalEPPmS3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13GlobalHandles16MoveTracedGlobalEPPmS3_
	.type	_ZN2v88internal13GlobalHandles16MoveTracedGlobalEPPmS3_, @function
_ZN2v88internal13GlobalHandles16MoveTracedGlobalEPPmS3_:
.LFB18865:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	cmpq	$0, 24(%rax)
	je	.L114
	ret
	.p2align 4,,10
	.p2align 3
.L114:
	movq	%rsi, 16(%rax)
	ret
	.cfi_endproc
.LFE18865:
	.size	_ZN2v88internal13GlobalHandles16MoveTracedGlobalEPPmS3_, .-_ZN2v88internal13GlobalHandles16MoveTracedGlobalEPPmS3_
	.section	.text._ZN2v88internal13GlobalHandles7DestroyEPm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13GlobalHandles7DestroyEPm
	.type	_ZN2v88internal13GlobalHandles7DestroyEPm, @function
_ZN2v88internal13GlobalHandles7DestroyEPm:
.LFB18866:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L138
	movabsq	$1995093329451155167, %rsi
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rcx
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movzbl	10(%rdi), %eax
	salq	$5, %rax
	subq	%rax, %rcx
	movq	8208(%rcx), %rbx
	movq	24(%rbx), %rax
	andb	$-32, 11(%rdi)
	movq	%rsi, (%rdi)
	movq	%rax, 16(%rdi)
	movw	%dx, 8(%rdi)
	movq	$0, 24(%rdi)
	movq	%rdi, 24(%rbx)
	movzbl	10(%rdi), %eax
	salq	$5, %rax
	subq	%rax, %rdi
	subl	$1, 8232(%rdi)
	jne	.L118
	movq	8216(%rdi), %rdx
	movq	8224(%rdi), %rax
	testq	%rdx, %rdx
	je	.L119
	movq	%rax, 8224(%rdx)
	movq	8224(%rdi), %rax
.L119:
	testq	%rax, %rax
	je	.L120
	movq	8216(%rdi), %rdx
	movq	%rdx, 8216(%rax)
.L120:
	cmpq	16(%rbx), %rdi
	je	.L141
.L118:
	movq	(%rbx), %rax
	movq	(%rax), %rax
	movq	40960(%rax), %r12
	cmpb	$0, 5920(%r12)
	je	.L122
	movq	5912(%r12), %rax
.L123:
	testq	%rax, %rax
	je	.L124
	subl	$1, (%rax)
.L124:
	movq	(%rbx), %rax
	subq	$1, 72(%rax)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L122:
	.cfi_restore_state
	movb	$1, 5920(%r12)
	leaq	5896(%r12), %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 5912(%r12)
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L138:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.p2align 4,,10
	.p2align 3
.L141:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movq	8216(%rdi), %rax
	movq	%rax, 16(%rbx)
	jmp	.L118
	.cfi_endproc
.LFE18866:
	.size	_ZN2v88internal13GlobalHandles7DestroyEPm, .-_ZN2v88internal13GlobalHandles7DestroyEPm
	.section	.text._ZN2v88internal13GlobalHandles13DestroyTracedEPm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13GlobalHandles13DestroyTracedEPm
	.type	_ZN2v88internal13GlobalHandles13DestroyTracedEPm, @function
_ZN2v88internal13GlobalHandles13DestroyTracedEPm:
.LFB18867:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L165
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movzbl	10(%rdi), %eax
	salq	$5, %rax
	subq	%rax, %rcx
	movabsq	$1995093329451155167, %rax
	movq	8208(%rcx), %rbx
	movq	24(%rbx), %rdx
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	movw	%ax, 8(%rdi)
	movzbl	11(%rdi), %eax
	movq	$0, 24(%rdi)
	andl	$-12, %eax
	movq	%rdx, 16(%rdi)
	orl	$8, %eax
	movb	%al, 11(%rdi)
	movq	%rdi, 24(%rbx)
	movzbl	10(%rdi), %eax
	salq	$5, %rax
	subq	%rax, %rdi
	subl	$1, 8232(%rdi)
	jne	.L145
	movq	8216(%rdi), %rdx
	movq	8224(%rdi), %rax
	testq	%rdx, %rdx
	je	.L146
	movq	%rax, 8224(%rdx)
	movq	8224(%rdi), %rax
.L146:
	testq	%rax, %rax
	je	.L147
	movq	8216(%rdi), %rdx
	movq	%rdx, 8216(%rax)
.L147:
	cmpq	16(%rbx), %rdi
	je	.L168
.L145:
	movq	(%rbx), %rax
	movq	(%rax), %rax
	movq	40960(%rax), %r12
	cmpb	$0, 5920(%r12)
	je	.L149
	movq	5912(%r12), %rax
.L150:
	testq	%rax, %rax
	je	.L151
	subl	$1, (%rax)
.L151:
	movq	(%rbx), %rax
	subq	$1, 72(%rax)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L149:
	.cfi_restore_state
	movb	$1, 5920(%r12)
	leaq	5896(%r12), %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 5912(%r12)
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L165:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.p2align 4,,10
	.p2align 3
.L168:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movq	8216(%rdi), %rax
	movq	%rax, 16(%rbx)
	jmp	.L145
	.cfi_endproc
.LFE18867:
	.size	_ZN2v88internal13GlobalHandles13DestroyTracedEPm, .-_ZN2v88internal13GlobalHandles13DestroyTracedEPm
	.section	.text._ZN2v88internal13GlobalHandles32SetFinalizationCallbackForTracedEPmPvPFvRKNS_16WeakCallbackInfoIvEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13GlobalHandles32SetFinalizationCallbackForTracedEPmPvPFvRKNS_16WeakCallbackInfoIvEEE
	.type	_ZN2v88internal13GlobalHandles32SetFinalizationCallbackForTracedEPmPvPFvRKNS_16WeakCallbackInfoIvEEE, @function
_ZN2v88internal13GlobalHandles32SetFinalizationCallbackForTracedEPmPvPFvRKNS_16WeakCallbackInfoIvEEE:
.LFB18868:
	.cfi_startproc
	endbr64
	movq	%rsi, 16(%rdi)
	movq	%rdx, 24(%rdi)
	ret
	.cfi_endproc
.LFE18868:
	.size	_ZN2v88internal13GlobalHandles32SetFinalizationCallbackForTracedEPmPvPFvRKNS_16WeakCallbackInfoIvEEE, .-_ZN2v88internal13GlobalHandles32SetFinalizationCallbackForTracedEPmPvPFvRKNS_16WeakCallbackInfoIvEEE
	.section	.rodata._ZN2v88internal13GlobalHandles8MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"object_ != kGlobalHandleZapValue"
	.section	.rodata._ZN2v88internal13GlobalHandles8MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal13GlobalHandles8MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13GlobalHandles8MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE
	.type	_ZN2v88internal13GlobalHandles8MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE, @function
_ZN2v88internal13GlobalHandles8MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE:
.LFB18869:
	.cfi_startproc
	endbr64
	movabsq	$1995093329451155167, %rax
	cmpq	%rax, (%rdi)
	je	.L179
	movzbl	11(%rdi), %eax
	cmpl	$1, %ecx
	je	.L172
	cmpl	$2, %ecx
	je	.L173
	testl	%ecx, %ecx
	je	.L174
	andl	$-8, %eax
	movq	%rsi, 16(%rdi)
	orl	$2, %eax
	movq	%rdx, 24(%rdi)
	movb	%al, 11(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L172:
	andl	$56, %eax
	movq	%rsi, 16(%rdi)
	orl	$-126, %eax
	movq	%rdx, 24(%rdi)
	movb	%al, 11(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L174:
	andl	$56, %eax
	movq	%rsi, 16(%rdi)
	orl	$66, %eax
	movq	%rdx, 24(%rdi)
	movb	%al, 11(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L173:
	andl	$56, %eax
	movq	%rsi, 16(%rdi)
	orl	$2, %eax
	movq	%rdx, 24(%rdi)
	movb	%al, 11(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L179:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18869:
	.size	_ZN2v88internal13GlobalHandles8MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE, .-_ZN2v88internal13GlobalHandles8MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE
	.section	.text._ZN2v88internal13GlobalHandles8MakeWeakEPPm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13GlobalHandles8MakeWeakEPPm
	.type	_ZN2v88internal13GlobalHandles8MakeWeakEPPm, @function
_ZN2v88internal13GlobalHandles8MakeWeakEPPm:
.LFB18870:
	.cfi_startproc
	endbr64
	movabsq	$1995093329451155167, %rdx
	movq	(%rdi), %rax
	cmpq	%rdx, (%rax)
	je	.L185
	movzbl	11(%rax), %edx
	movq	%rdi, 16(%rax)
	movq	$0, 24(%rax)
	andl	$56, %edx
	orl	$-62, %edx
	movb	%dl, 11(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L185:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18870:
	.size	_ZN2v88internal13GlobalHandles8MakeWeakEPPm, .-_ZN2v88internal13GlobalHandles8MakeWeakEPPm
	.section	.text._ZN2v88internal13GlobalHandles13ClearWeaknessEPm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13GlobalHandles13ClearWeaknessEPm
	.type	_ZN2v88internal13GlobalHandles13ClearWeaknessEPm, @function
_ZN2v88internal13GlobalHandles13ClearWeaknessEPm:
.LFB18871:
	.cfi_startproc
	endbr64
	movzbl	11(%rdi), %eax
	movq	16(%rdi), %r8
	movq	$0, 16(%rdi)
	andl	$-8, %eax
	orl	$1, %eax
	movb	%al, 11(%rdi)
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE18871:
	.size	_ZN2v88internal13GlobalHandles13ClearWeaknessEPm, .-_ZN2v88internal13GlobalHandles13ClearWeaknessEPm
	.section	.text._ZN2v88internal13GlobalHandles22AnnotateStrongRetainerEPmPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13GlobalHandles22AnnotateStrongRetainerEPmPKc
	.type	_ZN2v88internal13GlobalHandles22AnnotateStrongRetainerEPmPKc, @function
_ZN2v88internal13GlobalHandles22AnnotateStrongRetainerEPmPKc:
.LFB18872:
	.cfi_startproc
	endbr64
	movq	%rsi, 16(%rdi)
	ret
	.cfi_endproc
.LFE18872:
	.size	_ZN2v88internal13GlobalHandles22AnnotateStrongRetainerEPmPKc, .-_ZN2v88internal13GlobalHandles22AnnotateStrongRetainerEPmPKc
	.section	.text._ZN2v88internal13GlobalHandles6IsWeakEPm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13GlobalHandles6IsWeakEPm
	.type	_ZN2v88internal13GlobalHandles6IsWeakEPm, @function
_ZN2v88internal13GlobalHandles6IsWeakEPm:
.LFB18873:
	.cfi_startproc
	endbr64
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	sete	%al
	ret
	.cfi_endproc
.LFE18873:
	.size	_ZN2v88internal13GlobalHandles6IsWeakEPm, .-_ZN2v88internal13GlobalHandles6IsWeakEPm
	.section	.text._ZN2v88internal13GlobalHandles29IterateWeakRootsForFinalizersEPNS0_11RootVisitorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13GlobalHandles29IterateWeakRootsForFinalizersEPNS0_11RootVisitorE
	.type	_ZN2v88internal13GlobalHandles29IterateWeakRootsForFinalizersEPNS0_11RootVisitorE, @function
_ZN2v88internal13GlobalHandles29IterateWeakRootsForFinalizersEPNS0_11RootVisitorE:
.LFB18874:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	leaq	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE(%rip), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	8(%rdi), %rax
	xorl	%ebx, %ebx
	movq	16(%rax), %r13
	testq	%r13, %r13
	je	.L189
	.p2align 4,,10
	.p2align 3
.L190:
	movq	%rbx, %rcx
	salq	$5, %rcx
	addq	%r13, %rcx
	movzbl	11(%rcx), %eax
	andl	$7, %eax
	cmpb	$3, %al
	jne	.L192
	movq	(%r14), %rax
	movq	24(%rax), %r8
	cmpq	%r12, %r8
	jne	.L193
	leaq	8(%rcx), %r8
	xorl	%edx, %edx
	movl	$13, %esi
	movq	%r14, %rdi
	call	*16(%rax)
.L192:
	addq	$1, %rbx
	cmpq	$255, %rbx
	jbe	.L190
	movq	8216(%r13), %r13
	testq	%r13, %r13
	je	.L189
	xorl	%ebx, %ebx
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L189:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L193:
	.cfi_restore_state
	xorl	%edx, %edx
	movl	$13, %esi
	movq	%r14, %rdi
	call	*%r8
	jmp	.L192
	.cfi_endproc
.LFE18874:
	.size	_ZN2v88internal13GlobalHandles29IterateWeakRootsForFinalizersEPNS0_11RootVisitorE, .-_ZN2v88internal13GlobalHandles29IterateWeakRootsForFinalizersEPNS0_11RootVisitorE
	.section	.text._ZN2v88internal13GlobalHandles34IterateWeakRootsIdentifyFinalizersEPFbPNS0_4HeapENS0_14FullObjectSlotEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13GlobalHandles34IterateWeakRootsIdentifyFinalizersEPFbPNS0_4HeapENS0_14FullObjectSlotEE
	.type	_ZN2v88internal13GlobalHandles34IterateWeakRootsIdentifyFinalizersEPFbPNS0_4HeapENS0_14FullObjectSlotEE, @function
_ZN2v88internal13GlobalHandles34IterateWeakRootsIdentifyFinalizersEPFbPNS0_4HeapENS0_14FullObjectSlotEE:
.LFB18876:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movq	16(%rax), %r14
	testq	%r14, %r14
	je	.L199
	.p2align 4,,10
	.p2align 3
.L200:
	movq	%rbx, %r12
	salq	$5, %r12
	addq	%r14, %r12
	movzbl	11(%r12), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L213
.L203:
	addq	$1, %rbx
	cmpq	$255, %rbx
	jbe	.L200
	movq	8216(%r14), %r14
	testq	%r14, %r14
	je	.L199
	xorl	%ebx, %ebx
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L213:
	movq	(%r15), %rax
	movq	%r12, %rsi
	leaq	37592(%rax), %rdi
	call	*%r13
	testb	%al, %al
	je	.L203
	movzbl	11(%r12), %eax
	movl	%eax, %edx
	shrb	$6, %dl
	jne	.L203
	andl	$-8, %eax
	orl	$3, %eax
	movb	%al, 11(%r12)
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L199:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18876:
	.size	_ZN2v88internal13GlobalHandles34IterateWeakRootsIdentifyFinalizersEPFbPNS0_4HeapENS0_14FullObjectSlotEE, .-_ZN2v88internal13GlobalHandles34IterateWeakRootsIdentifyFinalizersEPFbPNS0_4HeapENS0_14FullObjectSlotEE
	.section	.text._ZN2v88internal13GlobalHandles29IdentifyWeakUnmodifiedObjectsEPFbNS0_14FullObjectSlotEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13GlobalHandles29IdentifyWeakUnmodifiedObjectsEPFbNS0_14FullObjectSlotEE
	.type	_ZN2v88internal13GlobalHandles29IdentifyWeakUnmodifiedObjectsEPFbNS0_14FullObjectSlotEE, @function
_ZN2v88internal13GlobalHandles29IdentifyWeakUnmodifiedObjectsEPFbNS0_14FullObjectSlotEE:
.LFB18877:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rdi), %r14
	movq	16(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%r14, %rbx
	jne	.L221
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L219:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L222
.L221:
	movq	(%rbx), %r12
	movzbl	11(%r12), %eax
	andl	$7, %eax
	cmpb	$2, %al
	jne	.L219
	movq	%r12, %rdi
	call	*%r13
	testb	%al, %al
	jne	.L219
	addq	$8, %rbx
	orb	$16, 11(%r12)
	cmpq	%rbx, %r14
	jne	.L221
	.p2align 4,,10
	.p2align 3
.L222:
	movq	(%r15), %rax
	movq	56(%r15), %r12
	movq	48(%r15), %rbx
	movq	39720(%rax), %r14
	cmpq	%rbx, %r12
	je	.L214
	.p2align 4,,10
	.p2align 3
.L227:
	movq	(%rbx), %r15
	testb	$3, 11(%r15)
	je	.L224
	movq	%r15, %rdi
	call	*%r13
	testb	%al, %al
	je	.L224
	movq	8(%r14), %rdi
	movq	%r15, -64(%rbp)
	movl	$8, %ecx
	testq	%rdi, %rdi
	je	.L226
	movq	(%rdi), %rax
	leaq	_ZN2v818EmbedderHeapTracer21IsRootForNonTracingGCERKNS_12TracedGlobalINS_5ValueEEE(%rip), %rdx
	movq	80(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L240
.L226:
	movzbl	11(%r15), %eax
	andl	$-9, %eax
	orl	%ecx, %eax
	movb	%al, 11(%r15)
.L224:
	addq	$8, %rbx
	cmpq	%rbx, %r12
	jne	.L227
.L214:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L241
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L240:
	.cfi_restore_state
	leaq	-64(%rbp), %rsi
	call	*%rax
	leal	0(,%rax,8), %ecx
	jmp	.L226
.L241:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18877:
	.size	_ZN2v88internal13GlobalHandles29IdentifyWeakUnmodifiedObjectsEPFbNS0_14FullObjectSlotEE, .-_ZN2v88internal13GlobalHandles29IdentifyWeakUnmodifiedObjectsEPFbNS0_14FullObjectSlotEE
	.section	.text._ZN2v88internal13GlobalHandles35IterateYoungStrongAndDependentRootsEPNS0_11RootVisitorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13GlobalHandles35IterateYoungStrongAndDependentRootsEPNS0_11RootVisitorE
	.type	_ZN2v88internal13GlobalHandles35IterateYoungStrongAndDependentRootsEPNS0_11RootVisitorE, @function
_ZN2v88internal13GlobalHandles35IterateYoungStrongAndDependentRootsEPNS0_11RootVisitorE:
.LFB18878:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rbx
	movq	24(%rdi), %r15
	cmpq	%r15, %rbx
	je	.L243
	leaq	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE(%rip), %r13
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L265:
	leal	-2(%rax), %esi
	cmpb	$1, %sil
	ja	.L264
.L245:
	testb	$8, %dl
	jne	.L246
	andl	$16, %edx
	je	.L246
	movq	(%r14), %rax
	xorl	%edx, %edx
	movq	24(%rax), %r8
	cmpq	%r13, %r8
	jne	.L248
.L266:
	leaq	8(%rcx), %r8
	movl	$13, %esi
	movq	%r14, %rdi
	call	*16(%rax)
.L246:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L243
.L249:
	movq	(%rbx), %rcx
	movzbl	11(%rcx), %edx
	movl	%edx, %eax
	andl	$7, %eax
	cmpb	$1, %al
	jne	.L265
	movq	(%r14), %rax
	movq	16(%rcx), %rdx
	movq	24(%rax), %r8
	cmpq	%r13, %r8
	je	.L266
.L248:
	addq	$8, %rbx
	movl	$13, %esi
	movq	%r14, %rdi
	call	*%r8
	cmpq	%rbx, %r15
	jne	.L249
	.p2align 4,,10
	.p2align 3
.L243:
	movq	48(%r12), %rbx
	movq	56(%r12), %r13
	cmpq	%rbx, %r13
	je	.L242
	leaq	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE(%rip), %r12
	.p2align 4,,10
	.p2align 3
.L253:
	movq	(%rbx), %rcx
	movzbl	11(%rcx), %eax
	testb	$3, %al
	je	.L251
	testb	$8, %al
	je	.L251
	movq	(%r14), %rax
	movq	24(%rax), %r8
	cmpq	%r12, %r8
	jne	.L252
	leaq	8(%rcx), %r8
	xorl	%edx, %edx
	movl	$13, %esi
	movq	%r14, %rdi
	call	*16(%rax)
.L251:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jne	.L253
.L242:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L264:
	.cfi_restore_state
	cmpb	$4, %al
	jne	.L246
	movl	%edx, %eax
	shrb	$6, %al
	jne	.L246
	jmp	.L245
	.p2align 4,,10
	.p2align 3
.L252:
	xorl	%edx, %edx
	movl	$13, %esi
	movq	%r14, %rdi
	call	*%r8
	jmp	.L251
	.cfi_endproc
.LFE18878:
	.size	_ZN2v88internal13GlobalHandles35IterateYoungStrongAndDependentRootsEPNS0_11RootVisitorE, .-_ZN2v88internal13GlobalHandles35IterateYoungStrongAndDependentRootsEPNS0_11RootVisitorE
	.section	.text._ZN2v88internal13GlobalHandles37MarkYoungWeakUnmodifiedObjectsPendingEPFbPNS0_4HeapENS0_14FullObjectSlotEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13GlobalHandles37MarkYoungWeakUnmodifiedObjectsPendingEPFbPNS0_4HeapENS0_14FullObjectSlotEE
	.type	_ZN2v88internal13GlobalHandles37MarkYoungWeakUnmodifiedObjectsPendingEPFbPNS0_4HeapENS0_14FullObjectSlotEE, @function
_ZN2v88internal13GlobalHandles37MarkYoungWeakUnmodifiedObjectsPendingEPFbPNS0_4HeapENS0_14FullObjectSlotEE:
.LFB18879:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %rbx
	movq	24(%rdi), %r14
	cmpq	%r14, %rbx
	je	.L267
	movq	%rdi, %r15
	movq	%rsi, %r13
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L270:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L267
.L272:
	movq	(%rbx), %r12
	movzbl	11(%r12), %eax
	testb	$8, %al
	jne	.L269
	testb	$16, %al
	jne	.L270
.L269:
	andl	$7, %eax
	cmpb	$2, %al
	jne	.L270
	movq	(%r15), %rax
	movq	%r12, %rsi
	leaq	37592(%rax), %rdi
	call	*%r13
	testb	%al, %al
	je	.L270
	movzbl	11(%r12), %edx
	movl	%edx, %eax
	shrb	$6, %al
	subl	$1, %eax
	cmpb	$2, %al
	jbe	.L270
	andl	$-8, %edx
	addq	$8, %rbx
	orl	$3, %edx
	movb	%dl, 11(%r12)
	cmpq	%rbx, %r14
	jne	.L272
.L267:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18879:
	.size	_ZN2v88internal13GlobalHandles37MarkYoungWeakUnmodifiedObjectsPendingEPFbPNS0_4HeapENS0_14FullObjectSlotEE, .-_ZN2v88internal13GlobalHandles37MarkYoungWeakUnmodifiedObjectsPendingEPFbPNS0_4HeapENS0_14FullObjectSlotEE
	.section	.text._ZN2v88internal13GlobalHandles44IterateYoungWeakUnmodifiedRootsForFinalizersEPNS0_11RootVisitorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13GlobalHandles44IterateYoungWeakUnmodifiedRootsForFinalizersEPNS0_11RootVisitorE
	.type	_ZN2v88internal13GlobalHandles44IterateYoungWeakUnmodifiedRootsForFinalizersEPNS0_11RootVisitorE, @function
_ZN2v88internal13GlobalHandles44IterateYoungWeakUnmodifiedRootsForFinalizersEPNS0_11RootVisitorE:
.LFB18880:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	16(%rdi), %rbx
	movq	24(%rdi), %r13
	cmpq	%r13, %rbx
	je	.L281
	movq	%rsi, %r14
	leaq	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE(%rip), %r12
	.p2align 4,,10
	.p2align 3
.L286:
	movq	(%rbx), %rcx
	movzbl	11(%rcx), %eax
	testb	$8, %al
	jne	.L283
	testb	$16, %al
	jne	.L284
.L283:
	andl	$7, %eax
	cmpb	$3, %al
	jne	.L284
	movq	(%r14), %rax
	movq	24(%rax), %r8
	cmpq	%r12, %r8
	jne	.L285
	leaq	8(%rcx), %r8
	xorl	%edx, %edx
	movl	$13, %esi
	movq	%r14, %rdi
	call	*16(%rax)
	.p2align 4,,10
	.p2align 3
.L284:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jne	.L286
.L281:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L285:
	.cfi_restore_state
	xorl	%edx, %edx
	movl	$13, %esi
	movq	%r14, %rdi
	call	*%r8
	jmp	.L284
	.cfi_endproc
.LFE18880:
	.size	_ZN2v88internal13GlobalHandles44IterateYoungWeakUnmodifiedRootsForFinalizersEPNS0_11RootVisitorE, .-_ZN2v88internal13GlobalHandles44IterateYoungWeakUnmodifiedRootsForFinalizersEPNS0_11RootVisitorE
	.section	.rodata._ZN2v88internal13GlobalHandles40InvokeSecondPassPhantomCallbacksFromTaskEv.str1.1,"aMS",@progbits,1
.LC2:
	.string	"v8"
	.section	.rodata._ZN2v88internal13GlobalHandles40InvokeSecondPassPhantomCallbacksFromTaskEv.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"V8.GCPhantomHandleProcessingCallback"
	.section	.text._ZN2v88internal13GlobalHandles40InvokeSecondPassPhantomCallbacksFromTaskEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13GlobalHandles40InvokeSecondPassPhantomCallbacksFromTaskEv
	.type	_ZN2v88internal13GlobalHandles40InvokeSecondPassPhantomCallbacksFromTaskEv, @function
_ZN2v88internal13GlobalHandles40InvokeSecondPassPhantomCallbacksFromTaskEv:
.LFB18882:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movb	$0, 160(%rdi)
	movq	_ZZN2v88internal13GlobalHandles40InvokeSecondPassPhantomCallbacksFromTaskEvE29trace_event_unique_atomic1050(%rip), %r12
	testq	%r12, %r12
	je	.L311
.L291:
	movq	$0, -96(%rbp)
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L312
.L293:
	movq	(%rbx), %rax
	xorl	%edx, %edx
	movl	$8, %esi
	leaq	37592(%rax), %rdi
	call	_ZN2v88internal4Heap23CallGCPrologueCallbacksENS_6GCTypeENS_15GCCallbackFlagsE@PLT
	cmpb	$0, 161(%rbx)
	jne	.L297
	movq	%rbx, %rdi
	call	_ZN2v88internal13GlobalHandles32InvokeSecondPassPhantomCallbacksEv.part.0
.L297:
	movq	(%rbx), %rdi
	xorl	%edx, %edx
	movl	$8, %esi
	addq	$37592, %rdi
	call	_ZN2v88internal4Heap23CallGCEpilogueCallbacksENS_6GCTypeENS_15GCCallbackFlagsE@PLT
	leaq	-96(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L313
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L312:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L314
.L294:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L295
	movq	(%rdi), %rax
	call	*8(%rax)
.L295:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L296
	movq	(%rdi), %rax
	call	*8(%rax)
.L296:
	leaq	.LC3(%rip), %rax
	movq	%r12, -88(%rbp)
	movq	%rax, -80(%rbp)
	leaq	-88(%rbp), %rax
	movq	%r13, -72(%rbp)
	movq	%rax, -96(%rbp)
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L311:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L315
.L292:
	movq	%r12, _ZZN2v88internal13GlobalHandles40InvokeSecondPassPhantomCallbacksFromTaskEvE29trace_event_unique_atomic1050(%rip)
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L314:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC3(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L315:
	leaq	.LC2(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L292
.L313:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18882:
	.size	_ZN2v88internal13GlobalHandles40InvokeSecondPassPhantomCallbacksFromTaskEv, .-_ZN2v88internal13GlobalHandles40InvokeSecondPassPhantomCallbacksFromTaskEv
	.section	.text._ZNSt17_Function_handlerIFvvEZN2v88internal13GlobalHandles42InvokeOrScheduleSecondPassPhantomCallbacksEbEUlvE_E9_M_invokeERKSt9_Any_data,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvvEZN2v88internal13GlobalHandles42InvokeOrScheduleSecondPassPhantomCallbacksEbEUlvE_E9_M_invokeERKSt9_Any_data, @function
_ZNSt17_Function_handlerIFvvEZN2v88internal13GlobalHandles42InvokeOrScheduleSecondPassPhantomCallbacksEbEUlvE_E9_M_invokeERKSt9_Any_data:
.LFB22556:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	jmp	_ZN2v88internal13GlobalHandles40InvokeSecondPassPhantomCallbacksFromTaskEv
	.cfi_endproc
.LFE22556:
	.size	_ZNSt17_Function_handlerIFvvEZN2v88internal13GlobalHandles42InvokeOrScheduleSecondPassPhantomCallbacksEbEUlvE_E9_M_invokeERKSt9_Any_data, .-_ZNSt17_Function_handlerIFvvEZN2v88internal13GlobalHandles42InvokeOrScheduleSecondPassPhantomCallbacksEbEUlvE_E9_M_invokeERKSt9_Any_data
	.section	.text._ZN2v88internal13GlobalHandles32InvokeSecondPassPhantomCallbacksEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13GlobalHandles32InvokeSecondPassPhantomCallbacksEv
	.type	_ZN2v88internal13GlobalHandles32InvokeSecondPassPhantomCallbacksEv, @function
_ZN2v88internal13GlobalHandles32InvokeSecondPassPhantomCallbacksEv:
.LFB18883:
	.cfi_startproc
	endbr64
	cmpb	$0, 161(%rdi)
	je	.L319
	ret
	.p2align 4,,10
	.p2align 3
.L319:
	jmp	_ZN2v88internal13GlobalHandles32InvokeSecondPassPhantomCallbacksEv.part.0
	.cfi_endproc
.LFE18883:
	.size	_ZN2v88internal13GlobalHandles32InvokeSecondPassPhantomCallbacksEv, .-_ZN2v88internal13GlobalHandles32InvokeSecondPassPhantomCallbacksEv
	.section	.rodata._ZN2v88internal13GlobalHandles22PostScavengeProcessingEj.str1.1,"aMS",@progbits,1
.LC4:
	.string	"IsPendingFinalizer()"
.LC5:
	.string	"NEAR_DEATH != state()"
	.section	.text._ZN2v88internal13GlobalHandles22PostScavengeProcessingEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13GlobalHandles22PostScavengeProcessingEj
	.type	_ZN2v88internal13GlobalHandles22PostScavengeProcessingEj, @function
_ZN2v88internal13GlobalHandles22PostScavengeProcessingEj:
.LFB18884:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %rbx
	movq	24(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%r13, %rbx
	je	.L331
	movq	%rdi, %r14
	xorl	%r15d, %r15d
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L346:
	cmpb	$4, %dl
	je	.L343
	.p2align 4,,10
	.p2align 3
.L322:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	je	.L320
.L329:
	movq	(%rbx), %r12
	movzbl	11(%r12), %eax
	movl	%eax, %edx
	andl	$7, %edx
	je	.L322
	cmpb	$4, %dl
	je	.L344
.L323:
	movl	%eax, %edx
	movl	%eax, %edi
	andl	$-17, %edx
	andl	$7, %edi
	movb	%dl, 11(%r12)
	cmpb	$3, %dil
	je	.L345
.L324:
	cmpl	164(%r14), %esi
	jne	.L320
	movzbl	11(%r12), %eax
	movl	%eax, %edx
	andl	$7, %edx
	jne	.L346
	addq	$1, %r15
.L350:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jne	.L329
	.p2align 4,,10
	.p2align 3
.L320:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L347
	addq	$88, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L344:
	.cfi_restore_state
	movl	%eax, %ecx
	shrb	$6, %cl
	jne	.L322
	jmp	.L323
	.p2align 4,,10
	.p2align 3
.L345:
	shrb	$6, %dl
	jne	.L348
	movq	(%r14), %rdx
	andl	$-24, %eax
	pxor	%xmm1, %xmm1
	movl	%esi, -116(%rbp)
	orl	$4, %eax
	leaq	-96(%rbp), %rdi
	movb	%al, 11(%r12)
	movl	12616(%rdx), %eax
	movq	%rdx, %xmm0
	addl	$1, 41104(%rdx)
	movq	41096(%rdx), %r9
	movl	$6, 12616(%rdx)
	movl	%eax, -120(%rbp)
	movq	41088(%rdx), %rax
	movq	%r9, -112(%rbp)
	movhps	16(%r12), %xmm0
	movq	%rax, -128(%rbp)
	movq	%rdx, -104(%rbp)
	movq	$0, -64(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm1, -80(%rbp)
	call	*24(%r12)
	movzbl	11(%r12), %eax
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %r9
	movl	-116(%rbp), %esi
	andl	$7, %eax
	cmpb	$4, %al
	je	.L349
	movq	-128(%rbp), %rax
	subl	$1, 41104(%rdx)
	movq	%rax, 41088(%rdx)
	cmpq	41096(%rdx), %r9
	je	.L327
	movq	%r9, 41096(%rdx)
	movq	%rdx, %rdi
	movl	%esi, -112(%rbp)
	movq	%rdx, -104(%rbp)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movl	-112(%rbp), %esi
	movq	-104(%rbp), %rdx
.L327:
	movl	-120(%rbp), %eax
	movl	%eax, 12616(%rdx)
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L343:
	shrb	$6, %al
	je	.L322
	addq	$1, %r15
	jmp	.L350
	.p2align 4,,10
	.p2align 3
.L348:
	leaq	.LC4(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L349:
	leaq	.LC5(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L331:
	xorl	%r15d, %r15d
	jmp	.L320
.L347:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18884:
	.size	_ZN2v88internal13GlobalHandles22PostScavengeProcessingEj, .-_ZN2v88internal13GlobalHandles22PostScavengeProcessingEj
	.section	.rodata._ZN2v88internal13GlobalHandles23PostMarkSweepProcessingEj.str1.1,"aMS",@progbits,1
.LC6:
	.string	"!is_active()"
	.section	.text._ZN2v88internal13GlobalHandles23PostMarkSweepProcessingEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13GlobalHandles23PostMarkSweepProcessingEj
	.type	_ZN2v88internal13GlobalHandles23PostMarkSweepProcessingEj, @function
_ZN2v88internal13GlobalHandles23PostMarkSweepProcessingEj:
.LFB18885:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	16(%rax), %r13
	testq	%r13, %r13
	je	.L351
	.p2align 4,,10
	.p2align 3
.L352:
	movq	%r12, %rbx
	salq	$5, %rbx
	addq	%r13, %rbx
	movzbl	11(%rbx), %eax
	movl	%eax, %edx
	andl	$7, %edx
	je	.L362
	cmpb	$4, %dl
	je	.L380
.L355:
	andb	$-17, 11(%rbx)
	movzbl	11(%rbx), %eax
	movl	%eax, %edx
	andl	$7, %edx
	cmpb	$3, %dl
	je	.L381
	cmpl	164(%r14), %esi
	jne	.L351
.L386:
	movzbl	11(%rbx), %eax
	movl	%eax, %edx
	andl	$7, %edx
	je	.L361
	cmpb	$4, %dl
	je	.L382
	.p2align 4,,10
	.p2align 3
.L362:
	addq	$1, %r12
	cmpq	$255, %r12
	jbe	.L352
	movq	8216(%r13), %r13
	testq	%r13, %r13
	je	.L351
	xorl	%r12d, %r12d
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L382:
	shrb	$6, %al
	je	.L362
	.p2align 4,,10
	.p2align 3
.L361:
	addq	$1, %r15
	jmp	.L362
	.p2align 4,,10
	.p2align 3
.L380:
	shrb	$6, %al
	jne	.L362
	jmp	.L355
	.p2align 4,,10
	.p2align 3
.L381:
	movl	%eax, %ecx
	shrb	$6, %cl
	jne	.L383
	testb	$16, %al
	jne	.L384
	movq	(%r14), %rdx
	andl	$-8, %eax
	pxor	%xmm1, %xmm1
	movl	%esi, -116(%rbp)
	orl	$4, %eax
	leaq	-96(%rbp), %rdi
	movb	%al, 11(%rbx)
	movl	12616(%rdx), %eax
	movq	%rdx, %xmm0
	addl	$1, 41104(%rdx)
	movq	41096(%rdx), %r9
	movl	$6, 12616(%rdx)
	movl	%eax, -120(%rbp)
	movq	41088(%rdx), %rax
	movq	%r9, -112(%rbp)
	movq	%rax, -128(%rbp)
	movq	%r12, %rax
	movhps	16(%rbx), %xmm0
	salq	$5, %rax
	movq	%rdx, -104(%rbp)
	movq	$0, -64(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm1, -80(%rbp)
	call	*24(%r13,%rax)
	movzbl	11(%rbx), %eax
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %r9
	movl	-116(%rbp), %esi
	andl	$7, %eax
	cmpb	$4, %al
	je	.L385
	movq	-128(%rbp), %rax
	subl	$1, 41104(%rdx)
	movq	%rax, 41088(%rdx)
	cmpq	41096(%rdx), %r9
	je	.L360
	movq	%r9, 41096(%rdx)
	movq	%rdx, %rdi
	movl	%esi, -112(%rbp)
	movq	%rdx, -104(%rbp)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movl	-112(%rbp), %esi
	movq	-104(%rbp), %rdx
.L360:
	movl	-120(%rbp), %eax
	movl	%eax, 12616(%rdx)
	cmpl	164(%r14), %esi
	je	.L386
	.p2align 4,,10
	.p2align 3
.L351:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L387
	addq	$88, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L383:
	.cfi_restore_state
	leaq	.LC4(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L384:
	leaq	.LC6(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L385:
	leaq	.LC5(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L387:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18885:
	.size	_ZN2v88internal13GlobalHandles23PostMarkSweepProcessingEj, .-_ZN2v88internal13GlobalHandles23PostMarkSweepProcessingEj
	.section	.text._ZN2v88internal13GlobalHandles42InvokeOrScheduleSecondPassPhantomCallbacksEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13GlobalHandles42InvokeOrScheduleSecondPassPhantomCallbacksEb
	.type	_ZN2v88internal13GlobalHandles42InvokeOrScheduleSecondPassPhantomCallbacksEb, @function
_ZN2v88internal13GlobalHandles42InvokeOrScheduleSecondPassPhantomCallbacksEb:
.LFB18890:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$80, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	144(%rdi), %rax
	cmpq	%rax, 136(%rdi)
	je	.L388
	cmpb	$0, _ZN2v88internal22FLAG_optimize_for_sizeE(%rip)
	movq	%rdi, %rbx
	jne	.L390
	cmpb	$0, _ZN2v88internal16FLAG_predictableE(%rip)
	jne	.L390
	testb	%sil, %sil
	je	.L391
.L390:
	movq	(%rbx), %rax
	xorl	%edx, %edx
	movl	$8, %esi
	leaq	37592(%rax), %rdi
	call	_ZN2v88internal4Heap23CallGCPrologueCallbacksENS_6GCTypeENS_15GCCallbackFlagsE@PLT
	cmpb	$0, 161(%rbx)
	je	.L420
.L393:
	movq	(%rbx), %rdi
	xorl	%edx, %edx
	movl	$8, %esi
	addq	$37592, %rdi
	call	_ZN2v88internal4Heap23CallGCEpilogueCallbacksENS_6GCTypeENS_15GCCallbackFlagsE@PLT
.L388:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L421
	addq	$80, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L420:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal13GlobalHandles32InvokeSecondPassPhantomCallbacksEv.part.0
	jmp	.L393
	.p2align 4,,10
	.p2align 3
.L391:
	cmpb	$0, 160(%rdi)
	jne	.L388
	movb	$1, 160(%rdi)
	leaq	-80(%rbp), %r12
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	(%rbx), %rdx
	leaq	-96(%rbp), %rdi
	movq	%rax, %rsi
	movq	(%rax), %rax
	call	*48(%rax)
	movq	-96(%rbp), %r13
	movq	(%rbx), %rsi
	leaq	-104(%rbp), %rdi
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal13GlobalHandles42InvokeOrScheduleSecondPassPhantomCallbacksEbEUlvE_E10_M_managerERSt9_Any_dataRKS6_St18_Manager_operation(%rip), %rcx
	movq	%r12, %rdx
	movq	0(%r13), %rax
	movq	%rcx, %xmm0
	movq	(%rax), %r14
	leaq	_ZNSt17_Function_handlerIFvvEZN2v88internal13GlobalHandles42InvokeOrScheduleSecondPassPhantomCallbacksEbEUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%rbx, -80(%rbp)
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal18MakeCancelableTaskEPNS0_7IsolateESt8functionIFvvEE@PLT
	movq	-104(%rbp), %rax
	movq	%r13, %rdi
	leaq	-112(%rbp), %rsi
	movq	$0, -104(%rbp)
	leaq	32(%rax), %rdx
	testq	%rax, %rax
	cmovne	%rdx, %rax
	movq	%rax, -112(%rbp)
	call	*%r14
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L395
	movq	(%rdi), %rax
	call	*8(%rax)
.L395:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L396
	movq	(%rdi), %rax
	call	*8(%rax)
.L396:
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L397
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	*%rax
.L397:
	movq	-88(%rbp), %r12
	testq	%r12, %r12
	je	.L388
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L400
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
.L401:
	cmpl	$1, %eax
	jne	.L388
	movq	(%r12), %rax
	leaq	_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv(%rip), %rdx
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L422
.L403:
	testq	%rbx, %rbx
	je	.L404
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L405:
	cmpl	$1, %eax
	jne	.L388
	movq	(%r12), %rdx
	leaq	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv(%rip), %rcx
	movq	%r12, %rdi
	movq	24(%rdx), %rax
	cmpq	%rcx, %rax
	jne	.L406
	call	*8(%rdx)
	jmp	.L388
.L400:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	jmp	.L401
.L404:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L405
.L422:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L403
.L406:
	call	*%rax
	jmp	.L388
.L421:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18890:
	.size	_ZN2v88internal13GlobalHandles42InvokeOrScheduleSecondPassPhantomCallbacksEb, .-_ZN2v88internal13GlobalHandles42InvokeOrScheduleSecondPassPhantomCallbacksEb
	.section	.text._ZN2v88internal13GlobalHandles22PendingPhantomCallback6InvokeEPNS0_7IsolateENS2_14InvocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13GlobalHandles22PendingPhantomCallback6InvokeEPNS0_7IsolateENS2_14InvocationTypeE
	.type	_ZN2v88internal13GlobalHandles22PendingPhantomCallback6InvokeEPNS0_7IsolateENS2_14InvocationTypeE, @function
_ZN2v88internal13GlobalHandles22PendingPhantomCallback6InvokeEPNS0_7IsolateENS2_14InvocationTypeE:
.LFB18900:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	movq	8(%rdi), %rdx
	movq	%rsi, -48(%rbp)
	movq	%rdx, -40(%rbp)
	cmove	%rdi, %rax
	movq	%rax, -32(%rbp)
	movq	16(%rdi), %rax
	movq	%rax, -24(%rbp)
	movq	24(%rdi), %rax
	movq	%rax, -16(%rbp)
	movq	(%rdi), %rax
	movq	$0, (%rdi)
	leaq	-48(%rbp), %rdi
	call	*%rax
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L428
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L428:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18900:
	.size	_ZN2v88internal13GlobalHandles22PendingPhantomCallback6InvokeEPNS0_7IsolateENS2_14InvocationTypeE, .-_ZN2v88internal13GlobalHandles22PendingPhantomCallback6InvokeEPNS0_7IsolateENS2_14InvocationTypeE
	.section	.text._ZN2v88internal13GlobalHandles13InRecursiveGCEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13GlobalHandles13InRecursiveGCEj
	.type	_ZN2v88internal13GlobalHandles13InRecursiveGCEj, @function
_ZN2v88internal13GlobalHandles13InRecursiveGCEj:
.LFB18901:
	.cfi_startproc
	endbr64
	cmpl	%esi, 164(%rdi)
	setne	%al
	ret
	.cfi_endproc
.LFE18901:
	.size	_ZN2v88internal13GlobalHandles13InRecursiveGCEj, .-_ZN2v88internal13GlobalHandles13InRecursiveGCEj
	.section	.text._ZN2v88internal13GlobalHandles18IterateStrongRootsEPNS0_11RootVisitorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13GlobalHandles18IterateStrongRootsEPNS0_11RootVisitorE
	.type	_ZN2v88internal13GlobalHandles18IterateStrongRootsEPNS0_11RootVisitorE, @function
_ZN2v88internal13GlobalHandles18IterateStrongRootsEPNS0_11RootVisitorE:
.LFB18903:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	leaq	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE(%rip), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	8(%rdi), %rax
	movq	16(%rax), %r13
	xorl	%eax, %eax
	testq	%r13, %r13
	je	.L430
	.p2align 4,,10
	.p2align 3
.L431:
	movq	%rax, %rcx
	leaq	1(%rax), %rbx
	salq	$5, %rcx
	addq	%r13, %rcx
	movzbl	11(%rcx), %eax
	andl	$7, %eax
	cmpb	$1, %al
	je	.L445
.L433:
	cmpq	$255, %rbx
	jbe	.L442
.L446:
	movq	8216(%r13), %r13
	testq	%r13, %r13
	je	.L430
	xorl	%eax, %eax
	jmp	.L431
	.p2align 4,,10
	.p2align 3
.L445:
	movq	(%r14), %rax
	movq	16(%rcx), %rdx
	movq	24(%rax), %r8
	cmpq	%r12, %r8
	jne	.L434
	leaq	8(%rcx), %r8
	movl	$13, %esi
	movq	%r14, %rdi
	call	*16(%rax)
	cmpq	$255, %rbx
	ja	.L446
.L442:
	movq	%rbx, %rax
	jmp	.L431
	.p2align 4,,10
	.p2align 3
.L430:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L434:
	.cfi_restore_state
	movl	$13, %esi
	movq	%r14, %rdi
	call	*%r8
	jmp	.L433
	.cfi_endproc
.LFE18903:
	.size	_ZN2v88internal13GlobalHandles18IterateStrongRootsEPNS0_11RootVisitorE, .-_ZN2v88internal13GlobalHandles18IterateStrongRootsEPNS0_11RootVisitorE
	.section	.text._ZN2v88internal13GlobalHandles16IterateWeakRootsEPNS0_11RootVisitorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13GlobalHandles16IterateWeakRootsEPNS0_11RootVisitorE
	.type	_ZN2v88internal13GlobalHandles16IterateWeakRootsEPNS0_11RootVisitorE, @function
_ZN2v88internal13GlobalHandles16IterateWeakRootsEPNS0_11RootVisitorE:
.LFB18904:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movq	16(%rax), %r15
	testq	%r15, %r15
	je	.L449
	.p2align 4,,10
	.p2align 3
.L448:
	movq	%rbx, %rcx
	salq	$5, %rcx
	addq	%r15, %rcx
	movzbl	11(%rcx), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L468
.L450:
	addq	$1, %rbx
	cmpq	$255, %rbx
	jbe	.L448
	movq	8216(%r15), %r15
	testq	%r15, %r15
	je	.L449
	xorl	%ebx, %ebx
	jmp	.L448
	.p2align 4,,10
	.p2align 3
.L468:
	movq	(%r14), %rax
	movq	24(%rax), %r8
	cmpq	%r13, %r8
	jne	.L451
	leaq	8(%rcx), %r8
	xorl	%edx, %edx
	movl	$13, %esi
	movq	%r14, %rdi
	call	*16(%rax)
	jmp	.L450
	.p2align 4,,10
	.p2align 3
.L449:
	movq	40(%r12), %rax
	xorl	%ebx, %ebx
	leaq	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE(%rip), %r12
	movq	16(%rax), %r13
	testq	%r13, %r13
	je	.L447
.L453:
	movq	%rbx, %rcx
	salq	$5, %rcx
	addq	%r13, %rcx
	testb	$3, 11(%rcx)
	je	.L455
	movq	(%r14), %rax
	movq	24(%rax), %r8
	cmpq	%r12, %r8
	jne	.L456
.L469:
	leaq	8(%rcx), %r8
	xorl	%edx, %edx
	movl	$13, %esi
	movq	%r14, %rdi
	call	*16(%rax)
.L455:
	addq	$1, %rbx
	cmpq	$255, %rbx
	jbe	.L453
	movq	8216(%r13), %r13
	testq	%r13, %r13
	je	.L447
	xorl	%ebx, %ebx
	movq	%rbx, %rcx
	salq	$5, %rcx
	addq	%r13, %rcx
	testb	$3, 11(%rcx)
	je	.L455
	movq	(%r14), %rax
	movq	24(%rax), %r8
	cmpq	%r12, %r8
	je	.L469
.L456:
	xorl	%edx, %edx
	movl	$13, %esi
	movq	%r14, %rdi
	call	*%r8
	jmp	.L455
	.p2align 4,,10
	.p2align 3
.L447:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L451:
	.cfi_restore_state
	xorl	%edx, %edx
	movl	$13, %esi
	movq	%r14, %rdi
	call	*%r8
	jmp	.L450
	.cfi_endproc
.LFE18904:
	.size	_ZN2v88internal13GlobalHandles16IterateWeakRootsEPNS0_11RootVisitorE, .-_ZN2v88internal13GlobalHandles16IterateWeakRootsEPNS0_11RootVisitorE
	.section	.text._ZN2v88internal13GlobalHandles15IterateAllRootsEPNS0_11RootVisitorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13GlobalHandles15IterateAllRootsEPNS0_11RootVisitorE
	.type	_ZN2v88internal13GlobalHandles15IterateAllRootsEPNS0_11RootVisitorE, @function
_ZN2v88internal13GlobalHandles15IterateAllRootsEPNS0_11RootVisitorE:
.LFB18905:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movq	16(%rax), %r15
	testq	%r15, %r15
	je	.L472
	.p2align 4,,10
	.p2align 3
.L471:
	movq	%rbx, %rcx
	salq	$5, %rcx
	addq	%r15, %rcx
	movzbl	11(%rcx), %eax
	movl	%eax, %edx
	andl	$7, %edx
	je	.L473
	cmpb	$4, %dl
	je	.L495
	movq	(%r14), %rax
	xorl	%r9d, %r9d
	movq	24(%rax), %r8
	cmpb	$1, %dl
	jne	.L476
	movq	16(%rcx), %r9
.L476:
	cmpq	%r13, %r8
	jne	.L477
.L496:
	leaq	8(%rcx), %r8
	movq	%r9, %rdx
	movl	$13, %esi
	movq	%r14, %rdi
	call	*16(%rax)
.L473:
	addq	$1, %rbx
	cmpq	$255, %rbx
	jbe	.L471
	movq	8216(%r15), %r15
	testq	%r15, %r15
	je	.L472
	xorl	%ebx, %ebx
	jmp	.L471
	.p2align 4,,10
	.p2align 3
.L495:
	shrb	$6, %al
	jne	.L473
	movq	(%r14), %rax
	xorl	%r9d, %r9d
	movq	24(%rax), %r8
	cmpq	%r13, %r8
	je	.L496
.L477:
	movq	%r9, %rdx
	movl	$13, %esi
	movq	%r14, %rdi
	call	*%r8
	jmp	.L473
	.p2align 4,,10
	.p2align 3
.L472:
	movq	40(%r12), %rax
	xorl	%ebx, %ebx
	leaq	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE(%rip), %r12
	movq	16(%rax), %r13
	testq	%r13, %r13
	je	.L470
	.p2align 4,,10
	.p2align 3
.L479:
	movq	%rbx, %rcx
	salq	$5, %rcx
	addq	%r13, %rcx
	movzbl	11(%rcx), %eax
	andl	$3, %eax
	cmpb	$1, %al
	je	.L497
.L481:
	addq	$1, %rbx
	cmpq	$255, %rbx
	jbe	.L479
	movq	8216(%r13), %r13
	testq	%r13, %r13
	je	.L470
	xorl	%ebx, %ebx
	jmp	.L479
	.p2align 4,,10
	.p2align 3
.L497:
	movq	(%r14), %rax
	movq	24(%rax), %r8
	cmpq	%r12, %r8
	jne	.L482
	leaq	8(%rcx), %r8
	xorl	%edx, %edx
	movl	$13, %esi
	movq	%r14, %rdi
	call	*16(%rax)
	jmp	.L481
	.p2align 4,,10
	.p2align 3
.L470:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L482:
	.cfi_restore_state
	xorl	%edx, %edx
	movl	$13, %esi
	movq	%r14, %rdi
	call	*%r8
	jmp	.L481
	.cfi_endproc
.LFE18905:
	.size	_ZN2v88internal13GlobalHandles15IterateAllRootsEPNS0_11RootVisitorE, .-_ZN2v88internal13GlobalHandles15IterateAllRootsEPNS0_11RootVisitorE
	.section	.text._ZN2v88internal13GlobalHandles20IterateAllYoungRootsEPNS0_11RootVisitorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13GlobalHandles20IterateAllYoungRootsEPNS0_11RootVisitorE
	.type	_ZN2v88internal13GlobalHandles20IterateAllYoungRootsEPNS0_11RootVisitorE, @function
_ZN2v88internal13GlobalHandles20IterateAllYoungRootsEPNS0_11RootVisitorE:
.LFB18906:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rbx
	movq	24(%rdi), %r15
	cmpq	%r15, %rbx
	je	.L499
	leaq	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE(%rip), %r13
	jmp	.L505
	.p2align 4,,10
	.p2align 3
.L501:
	movq	(%r14), %rax
	xorl	%r9d, %r9d
	movq	24(%rax), %r8
	cmpb	$1, %dl
	jne	.L503
	movq	16(%rcx), %r9
.L503:
	cmpq	%r13, %r8
	jne	.L504
.L517:
	leaq	8(%rcx), %r8
	movq	%r9, %rdx
	movl	$13, %esi
	movq	%r14, %rdi
	call	*16(%rax)
.L500:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L499
.L505:
	movq	(%rbx), %rcx
	movzbl	11(%rcx), %eax
	movl	%eax, %edx
	andl	$7, %edx
	je	.L500
	cmpb	$4, %dl
	jne	.L501
	shrb	$6, %al
	jne	.L500
	movq	(%r14), %rax
	xorl	%r9d, %r9d
	movq	24(%rax), %r8
	cmpq	%r13, %r8
	je	.L517
.L504:
	addq	$8, %rbx
	movq	%r9, %rdx
	movl	$13, %esi
	movq	%r14, %rdi
	call	*%r8
	cmpq	%rbx, %r15
	jne	.L505
	.p2align 4,,10
	.p2align 3
.L499:
	movq	48(%r12), %rbx
	movq	56(%r12), %r13
	cmpq	%rbx, %r13
	je	.L498
	leaq	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE(%rip), %r12
	jmp	.L509
	.p2align 4,,10
	.p2align 3
.L507:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	je	.L498
.L509:
	movq	(%rbx), %rcx
	movzbl	11(%rcx), %eax
	andl	$3, %eax
	cmpb	$1, %al
	jne	.L507
	movq	(%r14), %rax
	movq	24(%rax), %r8
	cmpq	%r12, %r8
	jne	.L508
	addq	$8, %rbx
	leaq	8(%rcx), %r8
	xorl	%edx, %edx
	movl	$13, %esi
	movq	%r14, %rdi
	call	*16(%rax)
	cmpq	%rbx, %r13
	jne	.L509
.L498:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L508:
	.cfi_restore_state
	xorl	%edx, %edx
	movl	$13, %esi
	movq	%r14, %rdi
	call	*%r8
	jmp	.L507
	.cfi_endproc
.LFE18906:
	.size	_ZN2v88internal13GlobalHandles20IterateAllYoungRootsEPNS0_11RootVisitorE, .-_ZN2v88internal13GlobalHandles20IterateAllYoungRootsEPNS0_11RootVisitorE
	.section	.text._ZN2v88internal13GlobalHandles28ApplyPersistentHandleVisitorEPNS_23PersistentHandleVisitorEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13GlobalHandles28ApplyPersistentHandleVisitorEPNS_23PersistentHandleVisitorEPNS1_4NodeE
	.type	_ZN2v88internal13GlobalHandles28ApplyPersistentHandleVisitorEPNS_23PersistentHandleVisitorEPNS1_4NodeE, @function
_ZN2v88internal13GlobalHandles28ApplyPersistentHandleVisitorEPNS_23PersistentHandleVisitorEPNS1_4NodeE:
.LFB18907:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN2v823PersistentHandleVisitor21VisitPersistentHandleEPNS_10PersistentINS_5ValueENS_27NonCopyablePersistentTraitsIS2_EEEEt(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	%rdx, -16(%rbp)
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L522
.L518:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L523
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L522:
	.cfi_restore_state
	movq	%rsi, %rdi
	movzwl	8(%rdx), %edx
	leaq	-16(%rbp), %rsi
	call	*%rax
	jmp	.L518
.L523:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18907:
	.size	_ZN2v88internal13GlobalHandles28ApplyPersistentHandleVisitorEPNS_23PersistentHandleVisitorEPNS1_4NodeE, .-_ZN2v88internal13GlobalHandles28ApplyPersistentHandleVisitorEPNS_23PersistentHandleVisitorEPNS1_4NodeE
	.section	.text._ZN2v88internal13GlobalHandles27IterateAllRootsWithClassIdsEPNS_23PersistentHandleVisitorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13GlobalHandles27IterateAllRootsWithClassIdsEPNS_23PersistentHandleVisitorE
	.type	_ZN2v88internal13GlobalHandles27IterateAllRootsWithClassIdsEPNS_23PersistentHandleVisitorE, @function
_ZN2v88internal13GlobalHandles27IterateAllRootsWithClassIdsEPNS_23PersistentHandleVisitorE:
.LFB18908:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	_ZN2v823PersistentHandleVisitor21VisitPersistentHandleEPNS_10PersistentINS_5ValueENS_27NonCopyablePersistentTraitsIS2_EEEEt(%rip), %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	xorl	%ebx, %ebx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	16(%rax), %r12
	testq	%r12, %r12
	je	.L524
	.p2align 4,,10
	.p2align 3
.L525:
	movq	%rbx, %rax
	salq	$5, %rax
	addq	%r12, %rax
	movzbl	11(%rax), %edx
	movl	%edx, %ecx
	andl	$7, %ecx
	je	.L527
	cmpb	$4, %cl
	je	.L542
.L528:
	movzwl	8(%rax), %edx
	testw	%dx, %dx
	je	.L527
	movq	%rax, -48(%rbp)
	movq	0(%r13), %rax
	movq	16(%rax), %rax
	cmpq	%r14, %rax
	jne	.L543
	.p2align 4,,10
	.p2align 3
.L527:
	addq	$1, %rbx
	cmpq	$255, %rbx
	jbe	.L525
	movq	8216(%r12), %r12
	testq	%r12, %r12
	je	.L524
	xorl	%ebx, %ebx
	jmp	.L525
	.p2align 4,,10
	.p2align 3
.L542:
	shrb	$6, %dl
	jne	.L527
	jmp	.L528
	.p2align 4,,10
	.p2align 3
.L524:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L544
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L543:
	.cfi_restore_state
	leaq	-48(%rbp), %rsi
	movq	%r13, %rdi
	call	*%rax
	jmp	.L527
.L544:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18908:
	.size	_ZN2v88internal13GlobalHandles27IterateAllRootsWithClassIdsEPNS_23PersistentHandleVisitorE, .-_ZN2v88internal13GlobalHandles27IterateAllRootsWithClassIdsEPNS_23PersistentHandleVisitorE
	.section	.text._ZN2v88internal13GlobalHandles18IterateTracedNodesEPNS_18EmbedderHeapTracer25TracedGlobalHandleVisitorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13GlobalHandles18IterateTracedNodesEPNS_18EmbedderHeapTracer25TracedGlobalHandleVisitorE
	.type	_ZN2v88internal13GlobalHandles18IterateTracedNodesEPNS_18EmbedderHeapTracer25TracedGlobalHandleVisitorE, @function
_ZN2v88internal13GlobalHandles18IterateTracedNodesEPNS_18EmbedderHeapTracer25TracedGlobalHandleVisitorE:
.LFB18909:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-48(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	xorl	%ebx, %ebx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	40(%rdi), %rax
	movq	16(%rax), %r12
	testq	%r12, %r12
	je	.L545
.L546:
	movq	%rbx, %rax
	salq	$5, %rax
	addq	%r12, %rax
	testb	$3, 11(%rax)
	je	.L548
.L558:
	movq	%rax, -48(%rbp)
	movq	(%r14), %rax
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	*16(%rax)
.L548:
	addq	$1, %rbx
	cmpq	$255, %rbx
	jbe	.L546
	movq	8216(%r12), %r12
	testq	%r12, %r12
	je	.L545
	xorl	%ebx, %ebx
	movq	%rbx, %rax
	salq	$5, %rax
	addq	%r12, %rax
	testb	$3, 11(%rax)
	je	.L548
	jmp	.L558
	.p2align 4,,10
	.p2align 3
.L545:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L559
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L559:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18909:
	.size	_ZN2v88internal13GlobalHandles18IterateTracedNodesEPNS_18EmbedderHeapTracer25TracedGlobalHandleVisitorE, .-_ZN2v88internal13GlobalHandles18IterateTracedNodesEPNS_18EmbedderHeapTracer25TracedGlobalHandleVisitorE
	.section	.text._ZN2v88internal13GlobalHandles32IterateAllYoungRootsWithClassIdsEPNS_23PersistentHandleVisitorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13GlobalHandles32IterateAllYoungRootsWithClassIdsEPNS_23PersistentHandleVisitorE
	.type	_ZN2v88internal13GlobalHandles32IterateAllYoungRootsWithClassIdsEPNS_23PersistentHandleVisitorE, @function
_ZN2v88internal13GlobalHandles32IterateAllYoungRootsWithClassIdsEPNS_23PersistentHandleVisitorE:
.LFB18910:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	16(%rdi), %rbx
	movq	24(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	%r12, %rbx
	je	.L560
	movq	%rsi, %r13
	leaq	_ZN2v823PersistentHandleVisitor21VisitPersistentHandleEPNS_10PersistentINS_5ValueENS_27NonCopyablePersistentTraitsIS2_EEEEt(%rip), %r14
	jmp	.L565
	.p2align 4,,10
	.p2align 3
.L563:
	movzwl	8(%rdx), %eax
	testw	%ax, %ax
	je	.L562
	movq	%rdx, -48(%rbp)
	movq	0(%r13), %rdx
	movq	16(%rdx), %rcx
	cmpq	%r14, %rcx
	jne	.L576
	.p2align 4,,10
	.p2align 3
.L562:
	addq	$8, %rbx
	cmpq	%rbx, %r12
	je	.L560
.L565:
	movq	(%rbx), %rdx
	movzbl	11(%rdx), %eax
	movl	%eax, %ecx
	andl	$7, %ecx
	je	.L562
	cmpb	$4, %cl
	jne	.L563
	shrb	$6, %al
	je	.L563
	addq	$8, %rbx
	cmpq	%rbx, %r12
	jne	.L565
	.p2align 4,,10
	.p2align 3
.L560:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L577
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L576:
	.cfi_restore_state
	movzwl	%ax, %edx
	leaq	-48(%rbp), %rsi
	movq	%r13, %rdi
	call	*%rcx
	jmp	.L562
.L577:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18910:
	.size	_ZN2v88internal13GlobalHandles32IterateAllYoungRootsWithClassIdsEPNS_23PersistentHandleVisitorE, .-_ZN2v88internal13GlobalHandles32IterateAllYoungRootsWithClassIdsEPNS_23PersistentHandleVisitorE
	.section	.text._ZN2v88internal13GlobalHandles33IterateYoungWeakRootsWithClassIdsEPNS_23PersistentHandleVisitorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13GlobalHandles33IterateYoungWeakRootsWithClassIdsEPNS_23PersistentHandleVisitorE
	.type	_ZN2v88internal13GlobalHandles33IterateYoungWeakRootsWithClassIdsEPNS_23PersistentHandleVisitorE, @function
_ZN2v88internal13GlobalHandles33IterateYoungWeakRootsWithClassIdsEPNS_23PersistentHandleVisitorE:
.LFB18911:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	16(%rdi), %rbx
	movq	24(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	%r12, %rbx
	je	.L578
	movq	%rsi, %r13
	leaq	_ZN2v823PersistentHandleVisitor21VisitPersistentHandleEPNS_10PersistentINS_5ValueENS_27NonCopyablePersistentTraitsIS2_EEEEt(%rip), %r14
	jmp	.L582
	.p2align 4,,10
	.p2align 3
.L580:
	addq	$8, %rbx
	cmpq	%rbx, %r12
	je	.L578
.L582:
	movq	(%rbx), %rax
	movzwl	8(%rax), %edx
	testw	%dx, %dx
	je	.L580
	movzbl	11(%rax), %ecx
	andl	$7, %ecx
	cmpb	$2, %cl
	jne	.L580
	movq	%rax, -48(%rbp)
	movq	0(%r13), %rax
	movq	16(%rax), %rax
	cmpq	%r14, %rax
	je	.L580
	addq	$8, %rbx
	leaq	-48(%rbp), %rsi
	movq	%r13, %rdi
	call	*%rax
	cmpq	%rbx, %r12
	jne	.L582
	.p2align 4,,10
	.p2align 3
.L578:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L589
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L589:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18911:
	.size	_ZN2v88internal13GlobalHandles33IterateYoungWeakRootsWithClassIdsEPNS_23PersistentHandleVisitorE, .-_ZN2v88internal13GlobalHandles33IterateYoungWeakRootsWithClassIdsEPNS_23PersistentHandleVisitorE
	.section	.text._ZN2v88internal13GlobalHandles11RecordStatsEPNS0_9HeapStatsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13GlobalHandles11RecordStatsEPNS0_9HeapStatsE
	.type	_ZN2v88internal13GlobalHandles11RecordStatsEPNS0_9HeapStatsE, @function
_ZN2v88internal13GlobalHandles11RecordStatsEPNS0_9HeapStatsE:
.LFB18912:
	.cfi_startproc
	endbr64
	movq	104(%rsi), %rax
	xorl	%edx, %edx
	movq	$0, (%rax)
	movq	112(%rsi), %rax
	movq	$0, (%rax)
	movq	120(%rsi), %rax
	movq	$0, (%rax)
	movq	128(%rsi), %rax
	movq	$0, (%rax)
	movq	136(%rsi), %rax
	movq	$0, (%rax)
	movq	8(%rdi), %rax
	movq	16(%rax), %rcx
	testq	%rcx, %rcx
	je	.L601
	.p2align 4,,10
	.p2align 3
.L591:
	movq	104(%rsi), %rax
	addq	$1, (%rax)
	movq	%rdx, %rax
	salq	$5, %rax
	movzbl	11(%rcx,%rax), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L602
	movzbl	%al, %edi
	cmpb	$3, %al
	je	.L603
	cmpl	$4, %edi
	je	.L604
	testl	%edi, %edi
	jne	.L594
	movq	136(%rsi), %rax
	addq	$1, (%rax)
	.p2align 4,,10
	.p2align 3
.L594:
	addq	$1, %rdx
	cmpq	$255, %rdx
	jbe	.L591
	movq	8216(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L605
	xorl	%edx, %edx
	jmp	.L591
	.p2align 4,,10
	.p2align 3
.L602:
	movq	112(%rsi), %rax
	addq	$1, (%rax)
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L603:
	movq	120(%rsi), %rax
	addq	$1, (%rax)
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L604:
	movq	128(%rsi), %rax
	addq	$1, (%rax)
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L605:
	ret
	.p2align 4,,10
	.p2align 3
.L601:
	ret
	.cfi_endproc
.LFE18912:
	.size	_ZN2v88internal13GlobalHandles11RecordStatsEPNS0_9HeapStatsE, .-_ZN2v88internal13GlobalHandles11RecordStatsEPNS0_9HeapStatsE
	.section	.text._ZN2v88internal14EternalHandlesD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14EternalHandlesD2Ev
	.type	_ZN2v88internal14EternalHandlesD2Ev, @function
_ZN2v88internal14EternalHandlesD2Ev:
.LFB18914:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	8(%rdi), %rbx
	movq	16(%rdi), %r12
	cmpq	%r12, %rbx
	je	.L607
	.p2align 4,,10
	.p2align 3
.L609:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L608
	call	_ZdaPv@PLT
.L608:
	addq	$8, %rbx
	cmpq	%rbx, %r12
	jne	.L609
.L607:
	movq	32(%r13), %rdi
	testq	%rdi, %rdi
	je	.L610
	call	_ZdlPv@PLT
.L610:
	movq	8(%r13), %rdi
	testq	%rdi, %rdi
	je	.L606
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L606:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18914:
	.size	_ZN2v88internal14EternalHandlesD2Ev, .-_ZN2v88internal14EternalHandlesD2Ev
	.globl	_ZN2v88internal14EternalHandlesD1Ev
	.set	_ZN2v88internal14EternalHandlesD1Ev,_ZN2v88internal14EternalHandlesD2Ev
	.section	.text._ZN2v88internal14EternalHandles15IterateAllRootsEPNS0_11RootVisitorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14EternalHandles15IterateAllRootsEPNS0_11RootVisitorE
	.type	_ZN2v88internal14EternalHandles15IterateAllRootsEPNS0_11RootVisitorE, @function
_ZN2v88internal14EternalHandles15IterateAllRootsEPNS0_11RootVisitorE:
.LFB18916:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rbx
	movq	16(%rdi), %r13
	movl	(%rdi), %r12d
	cmpq	%r13, %rbx
	je	.L620
	movq	%rsi, %r15
	movl	$256, %r14d
	.p2align 4,,10
	.p2align 3
.L622:
	cmpl	$256, %r12d
	movl	%r14d, %eax
	movq	(%rbx), %rcx
	movq	(%r15), %r9
	cmovle	%r12d, %eax
	addq	$8, %rbx
	xorl	%edx, %edx
	movl	$14, %esi
	movq	%r15, %rdi
	subl	$256, %r12d
	cltq
	leaq	(%rcx,%rax,8), %r8
	call	*16(%r9)
	cmpq	%rbx, %r13
	jne	.L622
.L620:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18916:
	.size	_ZN2v88internal14EternalHandles15IterateAllRootsEPNS0_11RootVisitorE, .-_ZN2v88internal14EternalHandles15IterateAllRootsEPNS0_11RootVisitorE
	.section	.text._ZN2v88internal14EternalHandles17IterateYoungRootsEPNS0_11RootVisitorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14EternalHandles17IterateYoungRootsEPNS0_11RootVisitorE
	.type	_ZN2v88internal14EternalHandles17IterateYoungRootsEPNS0_11RootVisitorE, @function
_ZN2v88internal14EternalHandles17IterateYoungRootsEPNS0_11RootVisitorE:
.LFB18917:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	32(%rdi), %rbx
	movq	40(%rdi), %r12
	cmpq	%r12, %rbx
	je	.L625
	movq	%rdi, %r15
	movq	%rsi, %r14
	leaq	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE(%rip), %r13
	jmp	.L629
	.p2align 4,,10
	.p2align 3
.L632:
	addq	$4, %rbx
	leaq	8(%rcx), %r8
	xorl	%edx, %edx
	movl	$14, %esi
	movq	%r14, %rdi
	call	*16(%r9)
	cmpq	%rbx, %r12
	je	.L625
.L629:
	movl	(%rbx), %eax
	movq	8(%r15), %rcx
	movq	(%r14), %r9
	movl	%eax, %edx
	movzbl	%al, %eax
	sarl	$8, %edx
	movq	24(%r9), %r8
	movslq	%edx, %rdx
	movq	(%rcx,%rdx,8), %rdx
	leaq	(%rdx,%rax,8), %rcx
	cmpq	%r13, %r8
	je	.L632
	addq	$4, %rbx
	xorl	%edx, %edx
	movl	$14, %esi
	movq	%r14, %rdi
	call	*%r8
	cmpq	%rbx, %r12
	jne	.L629
.L625:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18917:
	.size	_ZN2v88internal14EternalHandles17IterateYoungRootsEPNS0_11RootVisitorE, .-_ZN2v88internal14EternalHandles17IterateYoungRootsEPNS0_11RootVisitorE
	.section	.text._ZN2v88internal13GlobalHandles9NodeSpaceINS1_4NodeEE7ReleaseEPS3_,"axG",@progbits,_ZN2v88internal13GlobalHandles9NodeSpaceINS1_4NodeEE7ReleaseEPS3_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13GlobalHandles9NodeSpaceINS1_4NodeEE7ReleaseEPS3_
	.type	_ZN2v88internal13GlobalHandles9NodeSpaceINS1_4NodeEE7ReleaseEPS3_, @function
_ZN2v88internal13GlobalHandles9NodeSpaceINS1_4NodeEE7ReleaseEPS3_:
.LFB21017:
	.cfi_startproc
	endbr64
	movabsq	$1995093329451155167, %rsi
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rcx
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movzbl	10(%rdi), %eax
	salq	$5, %rax
	subq	%rax, %rcx
	movq	8208(%rcx), %rbx
	movq	24(%rbx), %rax
	andb	$-32, 11(%rdi)
	movq	%rsi, (%rdi)
	movq	%rax, 16(%rdi)
	movw	%dx, 8(%rdi)
	movq	$0, 24(%rdi)
	movq	%rdi, 24(%rbx)
	movzbl	10(%rdi), %eax
	salq	$5, %rax
	subq	%rax, %rdi
	subl	$1, 8232(%rdi)
	jne	.L635
	movq	8216(%rdi), %rdx
	movq	8224(%rdi), %rax
	testq	%rdx, %rdx
	je	.L636
	movq	%rax, 8224(%rdx)
	movq	8224(%rdi), %rax
.L636:
	testq	%rax, %rax
	je	.L637
	movq	8216(%rdi), %rdx
	movq	%rdx, 8216(%rax)
.L637:
	cmpq	16(%rbx), %rdi
	je	.L652
.L635:
	movq	(%rbx), %rax
	movq	(%rax), %rax
	movq	40960(%rax), %r12
	cmpb	$0, 5920(%r12)
	je	.L639
	movq	5912(%r12), %rax
.L640:
	testq	%rax, %rax
	je	.L641
	subl	$1, (%rax)
.L641:
	movq	(%rbx), %rax
	subq	$1, 72(%rax)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L639:
	.cfi_restore_state
	movb	$1, 5920(%r12)
	leaq	5896(%r12), %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 5912(%r12)
	jmp	.L640
	.p2align 4,,10
	.p2align 3
.L652:
	movq	8216(%rdi), %rax
	movq	%rax, 16(%rbx)
	jmp	.L635
	.cfi_endproc
.LFE21017:
	.size	_ZN2v88internal13GlobalHandles9NodeSpaceINS1_4NodeEE7ReleaseEPS3_, .-_ZN2v88internal13GlobalHandles9NodeSpaceINS1_4NodeEE7ReleaseEPS3_
	.section	.text._ZN2v88internal13GlobalHandles9NodeSpaceINS1_10TracedNodeEE7ReleaseEPS3_,"axG",@progbits,_ZN2v88internal13GlobalHandles9NodeSpaceINS1_10TracedNodeEE7ReleaseEPS3_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13GlobalHandles9NodeSpaceINS1_10TracedNodeEE7ReleaseEPS3_
	.type	_ZN2v88internal13GlobalHandles9NodeSpaceINS1_10TracedNodeEE7ReleaseEPS3_, @function
_ZN2v88internal13GlobalHandles9NodeSpaceINS1_10TracedNodeEE7ReleaseEPS3_:
.LFB21059:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movzbl	10(%rdi), %eax
	salq	$5, %rax
	subq	%rax, %rcx
	movabsq	$1995093329451155167, %rax
	movq	8208(%rcx), %rbx
	movq	24(%rbx), %rdx
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	movw	%ax, 8(%rdi)
	movzbl	11(%rdi), %eax
	movq	$0, 24(%rdi)
	andl	$-12, %eax
	movq	%rdx, 16(%rdi)
	orl	$8, %eax
	movb	%al, 11(%rdi)
	movq	%rdi, 24(%rbx)
	movzbl	10(%rdi), %eax
	salq	$5, %rax
	subq	%rax, %rdi
	subl	$1, 8232(%rdi)
	jne	.L655
	movq	8216(%rdi), %rdx
	movq	8224(%rdi), %rax
	testq	%rdx, %rdx
	je	.L656
	movq	%rax, 8224(%rdx)
	movq	8224(%rdi), %rax
.L656:
	testq	%rax, %rax
	je	.L657
	movq	8216(%rdi), %rdx
	movq	%rdx, 8216(%rax)
.L657:
	cmpq	16(%rbx), %rdi
	je	.L672
.L655:
	movq	(%rbx), %rax
	movq	(%rax), %rax
	movq	40960(%rax), %r12
	cmpb	$0, 5920(%r12)
	je	.L659
	movq	5912(%r12), %rax
.L660:
	testq	%rax, %rax
	je	.L661
	subl	$1, (%rax)
.L661:
	movq	(%rbx), %rax
	subq	$1, 72(%rax)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L659:
	.cfi_restore_state
	movb	$1, 5920(%r12)
	leaq	5896(%r12), %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 5912(%r12)
	jmp	.L660
	.p2align 4,,10
	.p2align 3
.L672:
	movq	8216(%rdi), %rax
	movq	%rax, 16(%rbx)
	jmp	.L655
	.cfi_endproc
.LFE21059:
	.size	_ZN2v88internal13GlobalHandles9NodeSpaceINS1_10TracedNodeEE7ReleaseEPS3_, .-_ZN2v88internal13GlobalHandles9NodeSpaceINS1_10TracedNodeEE7ReleaseEPS3_
	.section	.rodata._ZNSt6vectorIPN2v88internal13GlobalHandles4NodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_.str1.1,"aMS",@progbits,1
.LC7:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIPN2v88internal13GlobalHandles4NodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal13GlobalHandles4NodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal13GlobalHandles4NodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal13GlobalHandles4NodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal13GlobalHandles4NodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_:
.LFB22488:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L687
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L683
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L688
.L675:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L682:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L689
	testq	%r13, %r13
	jg	.L678
	testq	%r9, %r9
	jne	.L681
.L679:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L689:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L678
.L681:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L679
	.p2align 4,,10
	.p2align 3
.L688:
	testq	%rsi, %rsi
	jne	.L676
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L682
	.p2align 4,,10
	.p2align 3
.L678:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L679
	jmp	.L681
	.p2align 4,,10
	.p2align 3
.L683:
	movl	$8, %r14d
	jmp	.L675
.L687:
	leaq	.LC7(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L676:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L675
	.cfi_endproc
.LFE22488:
	.size	_ZNSt6vectorIPN2v88internal13GlobalHandles4NodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, .-_ZNSt6vectorIPN2v88internal13GlobalHandles4NodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.section	.text._ZN2v88internal13GlobalHandles6CreateENS0_6ObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13GlobalHandles6CreateENS0_6ObjectE
	.type	_ZN2v88internal13GlobalHandles6CreateENS0_6ObjectE, @function
_ZN2v88internal13GlobalHandles6CreateENS0_6ObjectE:
.LFB18858:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rbx), %r12
	testq	%r12, %r12
	je	.L716
.L691:
	movq	16(%r12), %rax
	movq	%r12, %rsi
	movq	%rax, 24(%rbx)
	movzbl	11(%r12), %eax
	movq	%r13, (%r12)
	movq	$0, 16(%r12)
	andl	$-8, %eax
	orl	$1, %eax
	movb	%al, 11(%r12)
	movzbl	10(%r12), %eax
	salq	$5, %rax
	subq	%rax, %rsi
	movl	8232(%rsi), %edx
	leal	1(%rdx), %ecx
	movl	%ecx, 8232(%rsi)
	testl	%edx, %edx
	jne	.L695
	movq	16(%rbx), %rdx
	movq	%rsi, 16(%rbx)
	movq	$0, 8224(%rsi)
	movq	%rdx, 8216(%rsi)
	testq	%rdx, %rdx
	je	.L695
	movq	%rsi, 8224(%rdx)
.L695:
	movq	(%rbx), %rax
	movq	(%rax), %rax
	movq	40960(%rax), %r15
	cmpb	$0, 5920(%r15)
	je	.L697
	movq	5912(%r15), %rax
.L698:
	testq	%rax, %rax
	je	.L699
	addl	$1, (%rax)
.L699:
	movq	(%rbx), %rax
	movq	%r12, -64(%rbp)
	addq	$1, 72(%rax)
	testb	$1, %r13b
	je	.L700
	andq	$-262144, %r13
	testb	$24, 8(%r13)
	jne	.L717
.L700:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L718
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L717:
	.cfi_restore_state
	testb	$32, 11(%r12)
	jne	.L700
	movq	24(%r14), %rsi
	cmpq	32(%r14), %rsi
	je	.L701
	movq	%r12, (%rsi)
	addq	$8, 24(%r14)
.L702:
	movq	-64(%rbp), %r12
	orb	$32, 11(%r12)
	jmp	.L700
	.p2align 4,,10
	.p2align 3
.L697:
	movb	$1, 5920(%r15)
	leaq	5896(%r15), %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 5912(%r15)
	jmp	.L698
.L716:
	movl	$8240, %edi
	call	_Znwm@PLT
	movq	8(%rbx), %xmm0
	movq	(%rbx), %rcx
	movq	%rax, %r12
	leaq	8192(%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L692:
	andb	$-33, 11(%rax)
	addq	$32, %rax
	cmpq	%rax, %rdx
	jne	.L692
	movq	%rcx, %xmm1
	movq	%rbx, 8208(%r12)
	leaq	8160(%r12), %rax
	movl	$255, %edx
	movq	$0, 8216(%r12)
	punpcklqdq	%xmm1, %xmm0
	movabsq	$1995093329451155167, %rdi
	movq	$0, 8224(%r12)
	movl	$0, 8232(%r12)
	movups	%xmm0, 8192(%r12)
	movq	%r12, 8(%rbx)
	.p2align 4,,10
	.p2align 3
.L693:
	movb	%dl, 10(%rax)
	movq	24(%rbx), %rcx
	xorl	%esi, %esi
	subl	$1, %edx
	andb	$-32, 11(%rax)
	movq	%rdi, (%rax)
	movw	%si, 8(%rax)
	movq	$0, 24(%rax)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rbx)
	subq	$32, %rax
	cmpl	$-1, %edx
	jne	.L693
	jmp	.L691
.L701:
	leaq	-64(%rbp), %rdx
	leaq	16(%r14), %rdi
	call	_ZNSt6vectorIPN2v88internal13GlobalHandles4NodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	jmp	.L702
.L718:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18858:
	.size	_ZN2v88internal13GlobalHandles6CreateENS0_6ObjectE, .-_ZN2v88internal13GlobalHandles6CreateENS0_6ObjectE
	.section	.text._ZN2v88internal13GlobalHandles6CreateEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13GlobalHandles6CreateEm
	.type	_ZN2v88internal13GlobalHandles6CreateEm, @function
_ZN2v88internal13GlobalHandles6CreateEm:
.LFB18859:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal13GlobalHandles6CreateENS0_6ObjectE
	.cfi_endproc
.LFE18859:
	.size	_ZN2v88internal13GlobalHandles6CreateEm, .-_ZN2v88internal13GlobalHandles6CreateEm
	.section	.text._ZN2v88internal13GlobalHandles10CopyGlobalEPm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13GlobalHandles10CopyGlobalEPm
	.type	_ZN2v88internal13GlobalHandles10CopyGlobalEPm, @function
_ZN2v88internal13GlobalHandles10CopyGlobalEPm:
.LFB18862:
	.cfi_startproc
	endbr64
	movzbl	10(%rdi), %eax
	movq	%rdi, %rdx
	movq	(%rdi), %rsi
	salq	$5, %rax
	subq	%rax, %rdx
	movq	8200(%rdx), %r8
	movq	%r8, %rdi
	jmp	_ZN2v88internal13GlobalHandles6CreateENS0_6ObjectE
	.cfi_endproc
.LFE18862:
	.size	_ZN2v88internal13GlobalHandles10CopyGlobalEPm, .-_ZN2v88internal13GlobalHandles10CopyGlobalEPm
	.section	.text._ZNSt6vectorIPN2v88internal13GlobalHandles10TracedNodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal13GlobalHandles10TracedNodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal13GlobalHandles10TracedNodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal13GlobalHandles10TracedNodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal13GlobalHandles10TracedNodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_:
.LFB22502:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L735
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L731
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L736
.L723:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L730:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L737
	testq	%r13, %r13
	jg	.L726
	testq	%r9, %r9
	jne	.L729
.L727:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L737:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L726
.L729:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L727
	.p2align 4,,10
	.p2align 3
.L736:
	testq	%rsi, %rsi
	jne	.L724
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L730
	.p2align 4,,10
	.p2align 3
.L726:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L727
	jmp	.L729
	.p2align 4,,10
	.p2align 3
.L731:
	movl	$8, %r14d
	jmp	.L723
.L735:
	leaq	.LC7(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L724:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L723
	.cfi_endproc
.LFE22502:
	.size	_ZNSt6vectorIPN2v88internal13GlobalHandles10TracedNodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, .-_ZNSt6vectorIPN2v88internal13GlobalHandles10TracedNodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.section	.text._ZN2v88internal13GlobalHandles12CreateTracedENS0_6ObjectEPmb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13GlobalHandles12CreateTracedENS0_6ObjectEPmb
	.type	_ZN2v88internal13GlobalHandles12CreateTracedENS0_6ObjectEPmb, @function
_ZN2v88internal13GlobalHandles12CreateTracedENS0_6ObjectEPmb:
.LFB18860:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	40(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rbx), %r8
	testq	%r8, %r8
	je	.L764
.L739:
	movq	16(%r8), %rax
	movq	%r8, %rsi
	movq	%rax, 24(%rbx)
	movzbl	11(%r8), %eax
	movq	%r12, (%r8)
	andl	$-4, %eax
	movq	$0, 16(%r8)
	orl	$1, %eax
	movb	%al, 11(%r8)
	movzbl	10(%r8), %eax
	salq	$5, %rax
	subq	%rax, %rsi
	movl	8232(%rsi), %edx
	leal	1(%rdx), %ecx
	movl	%ecx, 8232(%rsi)
	testl	%edx, %edx
	jne	.L743
	movq	16(%rbx), %rdx
	movq	%rsi, 16(%rbx)
	movq	$0, 8224(%rsi)
	movq	%rdx, 8216(%rsi)
	testq	%rdx, %rdx
	je	.L743
	movq	%rsi, 8224(%rdx)
.L743:
	movq	(%rbx), %rax
	movq	(%rax), %rax
	movq	40960(%rax), %rdx
	cmpb	$0, 5920(%rdx)
	je	.L745
	movq	5912(%rdx), %rax
.L746:
	testq	%rax, %rax
	je	.L747
	addl	$1, (%rax)
.L747:
	movq	(%rbx), %rax
	movq	%r8, -64(%rbp)
	addq	$1, 72(%rax)
	movzbl	11(%r8), %eax
	testb	$1, %r12b
	je	.L749
	andq	$-262144, %r12
	testb	$24, 8(%r12)
	jne	.L765
.L749:
	andl	$-17, %eax
	sall	$4, %r15d
	movq	%r14, 16(%r8)
	orl	%eax, %r15d
	movb	%r15b, 11(%r8)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L766
	addq	$40, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L765:
	.cfi_restore_state
	testb	$4, %al
	jne	.L749
	movq	56(%r13), %rsi
	cmpq	64(%r13), %rsi
	je	.L752
	movq	%r8, (%rsi)
	addq	$8, 56(%r13)
.L753:
	movq	-64(%rbp), %r8
	movzbl	11(%r8), %eax
	orl	$4, %eax
	movb	%al, 11(%r8)
	jmp	.L749
	.p2align 4,,10
	.p2align 3
.L745:
	movb	$1, 5920(%rdx)
	leaq	5896(%rdx), %rdi
	movq	%r8, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	-72(%rbp), %rdx
	movq	-80(%rbp), %r8
	movq	%rax, 5912(%rdx)
	jmp	.L746
.L764:
	movl	$8240, %edi
	call	_Znwm@PLT
	movq	8(%rbx), %xmm0
	movq	(%rbx), %rcx
	movq	%rax, %r8
	leaq	8192(%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L740:
	andb	$-5, 11(%rax)
	addq	$32, %rax
	cmpq	%rax, %rdx
	jne	.L740
	movq	%rcx, %xmm1
	movl	$255, %edi
	movq	%rbx, 8208(%r8)
	leaq	8160(%r8), %rax
	movq	$0, 8216(%r8)
	punpcklqdq	%xmm1, %xmm0
	movabsq	$1995093329451155167, %rsi
	movq	$0, 8224(%r8)
	movl	$0, 8232(%r8)
	movups	%xmm0, 8192(%r8)
	movq	%r8, 8(%rbx)
	.p2align 4,,10
	.p2align 3
.L741:
	xorl	%edx, %edx
	movb	%dil, 10(%rax)
	movq	24(%rbx), %rcx
	subl	$1, %edi
	movw	%dx, 8(%rax)
	movzbl	11(%rax), %edx
	movq	%rsi, (%rax)
	andl	$-12, %edx
	movq	$0, 24(%rax)
	orl	$8, %edx
	movq	%rcx, 16(%rax)
	movb	%dl, 11(%rax)
	movq	%rax, 24(%rbx)
	subq	$32, %rax
	cmpl	$-1, %edi
	jne	.L741
	jmp	.L739
.L752:
	leaq	-64(%rbp), %rdx
	leaq	48(%r13), %rdi
	call	_ZNSt6vectorIPN2v88internal13GlobalHandles10TracedNodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	jmp	.L753
.L766:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18860:
	.size	_ZN2v88internal13GlobalHandles12CreateTracedENS0_6ObjectEPmb, .-_ZN2v88internal13GlobalHandles12CreateTracedENS0_6ObjectEPmb
	.section	.text._ZN2v88internal13GlobalHandles12CreateTracedEmPmb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13GlobalHandles12CreateTracedEmPmb
	.type	_ZN2v88internal13GlobalHandles12CreateTracedEmPmb, @function
_ZN2v88internal13GlobalHandles12CreateTracedEmPmb:
.LFB18861:
	.cfi_startproc
	endbr64
	movzbl	%cl, %ecx
	jmp	_ZN2v88internal13GlobalHandles12CreateTracedENS0_6ObjectEPmb
	.cfi_endproc
.LFE18861:
	.size	_ZN2v88internal13GlobalHandles12CreateTracedEmPmb, .-_ZN2v88internal13GlobalHandles12CreateTracedEmPmb
	.section	.rodata._ZN2v88internal13GlobalHandles16CopyTracedGlobalEPKPKmPPm.str1.8,"aMS",@progbits,1
	.align 8
.LC8:
	.string	"!node->HasFinalizationCallback()"
	.section	.text._ZN2v88internal13GlobalHandles16CopyTracedGlobalEPKPKmPPm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13GlobalHandles16CopyTracedGlobalEPKPKmPPm
	.type	_ZN2v88internal13GlobalHandles16CopyTracedGlobalEPKPKmPPm, @function
_ZN2v88internal13GlobalHandles16CopyTracedGlobalEPKPKmPPm:
.LFB18863:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	cmpq	$0, 24(%rax)
	jne	.L771
	movzbl	10(%rax), %edx
	movq	%rax, %rcx
	movq	%rsi, %rbx
	movq	(%rax), %rsi
	salq	$5, %rdx
	subq	%rdx, %rcx
	movq	%rbx, %rdx
	movq	8200(%rcx), %rdi
	movzbl	11(%rax), %ecx
	shrb	$4, %cl
	andl	$1, %ecx
	call	_ZN2v88internal13GlobalHandles12CreateTracedENS0_6ObjectEPmb
	movq	%rax, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L771:
	.cfi_restore_state
	leaq	.LC8(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18863:
	.size	_ZN2v88internal13GlobalHandles16CopyTracedGlobalEPKPKmPPm, .-_ZN2v88internal13GlobalHandles16CopyTracedGlobalEPKPKmPPm
	.section	.rodata._ZNSt6vectorIiSaIiEE17_M_default_appendEm.str1.1,"aMS",@progbits,1
.LC9:
	.string	"vector::_M_default_append"
	.section	.text._ZNSt6vectorIiSaIiEE17_M_default_appendEm,"axG",@progbits,_ZNSt6vectorIiSaIiEE17_M_default_appendEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIiSaIiEE17_M_default_appendEm
	.type	_ZNSt6vectorIiSaIiEE17_M_default_appendEm, @function
_ZNSt6vectorIiSaIiEE17_M_default_appendEm:
.LFB22590:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L791
	movabsq	$2305843009213693951, %rdx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %rcx
	movq	16(%r12), %rax
	movq	%rcx, %rsi
	subq	(%rdi), %rsi
	subq	%rcx, %rax
	movq	%rdx, %rdi
	movq	%rsi, %r13
	sarq	$2, %rax
	sarq	$2, %r13
	subq	%r13, %rdi
	cmpq	%rbx, %rax
	jb	.L774
	salq	$2, %rbx
	movq	%rcx, %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	call	memset@PLT
	movq	%rax, %rcx
	addq	%rbx, %rcx
	movq	%rcx, 8(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L791:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L774:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	cmpq	%rbx, %rdi
	jb	.L794
	cmpq	%rbx, %r13
	movq	%rbx, %r14
	movq	%rsi, -56(%rbp)
	cmovnb	%r13, %r14
	addq	%r13, %r14
	cmpq	%rdx, %r14
	cmova	%rdx, %r14
	salq	$2, %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	leaq	0(,%rbx,4), %rdx
	movq	%rax, %r15
	leaq	(%rax,%rsi), %rdi
	xorl	%esi, %esi
	call	memset@PLT
	movq	(%r12), %r8
	movq	8(%r12), %rdx
	subq	%r8, %rdx
	testq	%rdx, %rdx
	jg	.L795
	testq	%r8, %r8
	jne	.L778
.L779:
	addq	%r13, %rbx
	addq	%r15, %r14
	movq	%r15, (%r12)
	leaq	(%r15,%rbx,4), %rax
	movq	%r14, 16(%r12)
	movq	%rax, 8(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L795:
	.cfi_restore_state
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r8, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %r8
.L778:
	movq	%r8, %rdi
	call	_ZdlPv@PLT
	jmp	.L779
.L794:
	leaq	.LC9(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE22590:
	.size	_ZNSt6vectorIiSaIiEE17_M_default_appendEm, .-_ZNSt6vectorIiSaIiEE17_M_default_appendEm
	.section	.text._ZN2v88internal14EternalHandles31PostGarbageCollectionProcessingEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14EternalHandles31PostGarbageCollectionProcessingEv
	.type	_ZN2v88internal14EternalHandles31PostGarbageCollectionProcessingEv, @function
_ZN2v88internal14EternalHandles31PostGarbageCollectionProcessingEv:
.LFB18918:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %r11
	movq	40(%rdi), %r10
	cmpq	%r10, %r11
	je	.L796
	movq	%r11, %rax
	xorl	%esi, %esi
	.p2align 4,,10
	.p2align 3
.L799:
	movl	(%rax), %ecx
	movq	8(%rdi), %r9
	movl	%ecx, %edx
	movzbl	%cl, %r8d
	sarl	$8, %edx
	movslq	%edx, %rdx
	movq	(%r9,%rdx,8), %rdx
	movq	(%rdx,%r8,8), %rdx
	testb	$1, %dl
	je	.L798
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	je	.L798
	movl	%ecx, (%r11,%rsi,4)
	movq	32(%rdi), %r11
	addq	$1, %rsi
.L798:
	addq	$4, %rax
	cmpq	%rax, %r10
	jne	.L799
	movq	40(%rdi), %rdx
	movq	%rdx, %rax
	subq	%r11, %rax
	sarq	$2, %rax
	cmpq	%rax, %rsi
	ja	.L805
	jnb	.L796
	leaq	(%r11,%rsi,4), %rax
	cmpq	%rax, %rdx
	je	.L796
	movq	%rax, 40(%rdi)
.L796:
	ret
	.p2align 4,,10
	.p2align 3
.L805:
	subq	%rax, %rsi
	addq	$32, %rdi
	jmp	_ZNSt6vectorIiSaIiEE17_M_default_appendEm
	.cfi_endproc
.LFE18918:
	.size	_ZN2v88internal14EternalHandles31PostGarbageCollectionProcessingEv, .-_ZN2v88internal14EternalHandles31PostGarbageCollectionProcessingEv
	.section	.text._ZNSt6vectorIPmSaIS0_EE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPmSaIS0_EE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPmSaIS0_EE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_
	.type	_ZNSt6vectorIPmSaIS0_EE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_, @function
_ZNSt6vectorIPmSaIS0_EE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_:
.LFB22596:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L820
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L816
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L821
.L808:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L815:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L822
	testq	%r13, %r13
	jg	.L811
	testq	%r9, %r9
	jne	.L814
.L812:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L822:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L811
.L814:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L812
	.p2align 4,,10
	.p2align 3
.L821:
	testq	%rsi, %rsi
	jne	.L809
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L815
	.p2align 4,,10
	.p2align 3
.L811:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L812
	jmp	.L814
	.p2align 4,,10
	.p2align 3
.L816:
	movl	$8, %r14d
	jmp	.L808
.L820:
	leaq	.LC7(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L809:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L808
	.cfi_endproc
.LFE22596:
	.size	_ZNSt6vectorIPmSaIS0_EE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_, .-_ZNSt6vectorIPmSaIS0_EE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_
	.section	.text._ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_,"axG",@progbits,_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	.type	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_, @function
_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_:
.LFB22601:
	.cfi_startproc
	endbr64
	movabsq	$2305843009213693951, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$2, %rax
	cmpq	%rcx, %rax
	je	.L837
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L833
	movabsq	$9223372036854775804, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L838
.L825:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L832:
	movl	(%r15), %eax
	subq	%r8, %r13
	leaq	4(%rbx,%rdx), %r15
	movl	%eax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L839
	testq	%r13, %r13
	jg	.L828
	testq	%r9, %r9
	jne	.L831
.L829:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L839:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L828
.L831:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L829
	.p2align 4,,10
	.p2align 3
.L838:
	testq	%rsi, %rsi
	jne	.L826
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L832
	.p2align 4,,10
	.p2align 3
.L828:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L829
	jmp	.L831
	.p2align 4,,10
	.p2align 3
.L833:
	movl	$4, %r14d
	jmp	.L825
.L837:
	leaq	.LC7(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L826:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$2, %r14
	jmp	.L825
	.cfi_endproc
.LFE22601:
	.size	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_, .-_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	.section	.text._ZN2v88internal14EternalHandles6CreateEPNS0_7IsolateENS0_6ObjectEPi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14EternalHandles6CreateEPNS0_7IsolateENS0_6ObjectEPi
	.type	_ZN2v88internal14EternalHandles6CreateEPNS0_7IsolateENS0_6ObjectEPi, @function
_ZN2v88internal14EternalHandles6CreateEPNS0_7IsolateENS0_6ObjectEPi:
.LFB18919:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L840
	movl	(%rdi), %eax
	movq	%rdi, %rbx
	movq	%rdx, %r12
	movq	%rcx, %r14
	movl	%eax, %r15d
	sarl	$8, %r15d
	andl	$255, %eax
	movl	%eax, %r13d
	je	.L855
.L842:
	movq	8(%rbx), %rcx
	movslq	%r15d, %rdx
	movslq	%r13d, %rax
	movq	(%rcx,%rdx,8), %rdx
	movq	%r12, (%rdx,%rax,8)
	testb	$1, %r12b
	je	.L845
	andq	$-262144, %r12
	testb	$24, 8(%r12)
	jne	.L856
.L845:
	movl	(%rbx), %eax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movl	%eax, (%r14)
.L840:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L857
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L855:
	.cfi_restore_state
	movq	96(%rsi), %rax
	movl	$2048, %edi
	movq	%rax, -72(%rbp)
	call	_Znam@PLT
	movl	$256, %ecx
	movq	%rax, -64(%rbp)
	movq	%rax, %rdi
	movq	-72(%rbp), %rax
#APP
# 185 "../deps/v8/src/utils/memcopy.h" 1
	cld;rep ; stosq
# 0 "" 2
#NO_APP
	movq	16(%rbx), %rsi
	cmpq	24(%rbx), %rsi
	je	.L843
	movq	-64(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 16(%rbx)
	jmp	.L842
	.p2align 4,,10
	.p2align 3
.L856:
	movq	40(%rbx), %rsi
	cmpq	48(%rbx), %rsi
	je	.L846
	movl	(%rbx), %eax
	movl	%eax, (%rsi)
	addq	$4, 40(%rbx)
	jmp	.L845
	.p2align 4,,10
	.p2align 3
.L843:
	leaq	-64(%rbp), %rdx
	leaq	8(%rbx), %rdi
	call	_ZNSt6vectorIPmSaIS0_EE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_
	jmp	.L842
	.p2align 4,,10
	.p2align 3
.L846:
	leaq	32(%rbx), %rdi
	movq	%rbx, %rdx
	call	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	jmp	.L845
.L857:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18919:
	.size	_ZN2v88internal14EternalHandles6CreateEPNS0_7IsolateENS0_6ObjectEPi, .-_ZN2v88internal14EternalHandles6CreateEPNS0_7IsolateENS0_6ObjectEPi
	.section	.text._ZNSt6vectorISt4pairIPN2v88internal13GlobalHandles4NodeENS3_22PendingPhantomCallbackEESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt4pairIPN2v88internal13GlobalHandles4NodeENS3_22PendingPhantomCallbackEESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt4pairIPN2v88internal13GlobalHandles4NodeENS3_22PendingPhantomCallbackEESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.type	_ZNSt6vectorISt4pairIPN2v88internal13GlobalHandles4NodeENS3_22PendingPhantomCallbackEESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, @function
_ZNSt6vectorISt4pairIPN2v88internal13GlobalHandles4NodeENS3_22PendingPhantomCallbackEESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_:
.LFB23158:
	.cfi_startproc
	endbr64
	movabsq	$-3689348814741910323, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rsi
	movq	(%rdi), %r14
	movq	%rsi, %rax
	subq	%r14, %rax
	sarq	$3, %rax
	imulq	%rcx, %rax
	movabsq	$230584300921369395, %rcx
	cmpq	%rcx, %rax
	je	.L875
	movq	%r12, %r8
	movq	%rdi, %r15
	subq	%r14, %r8
	testq	%rax, %rax
	je	.L867
	movabsq	$9223372036854775800, %rbx
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L876
.L860:
	movq	%rbx, %rdi
	movq	%rdx, -80(%rbp)
	movq	%r8, -72(%rbp)
	movq	%rsi, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rsi
	movq	-72(%rbp), %r8
	movq	%rax, %r13
	leaq	(%rax,%rbx), %rax
	movq	-80(%rbp), %rdx
	movq	%rax, -56(%rbp)
	leaq	40(%r13), %rbx
.L866:
	movdqu	(%rdx), %xmm3
	movdqu	16(%rdx), %xmm4
	movq	32(%rdx), %rax
	movups	%xmm3, 0(%r13,%r8)
	movq	%rax, 32(%r13,%r8)
	movups	%xmm4, 16(%r13,%r8)
	cmpq	%r14, %r12
	je	.L862
	movq	%r13, %rdx
	movq	%r14, %rax
	.p2align 4,,10
	.p2align 3
.L863:
	movdqu	(%rax), %xmm1
	addq	$40, %rax
	addq	$40, %rdx
	movups	%xmm1, -40(%rdx)
	movdqu	-24(%rax), %xmm2
	movups	%xmm2, -24(%rdx)
	movq	-8(%rax), %rcx
	movq	%rcx, -8(%rdx)
	cmpq	%rax, %r12
	jne	.L863
	leaq	-40(%r12), %rax
	subq	%r14, %rax
	shrq	$3, %rax
	leaq	80(%r13,%rax,8), %rbx
.L862:
	cmpq	%rsi, %r12
	je	.L864
	subq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	-40(%rsi), %rax
	movq	%r12, %rsi
	shrq	$3, %rax
	leaq	40(,%rax,8), %rdx
	movq	%rdx, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %rdx
	addq	%rdx, %rbx
.L864:
	testq	%r14, %r14
	je	.L865
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L865:
	movq	-56(%rbp), %rax
	movq	%r13, %xmm0
	movq	%rbx, %xmm5
	punpcklqdq	%xmm5, %xmm0
	movq	%rax, 16(%r15)
	movups	%xmm0, (%r15)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L876:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L861
	movq	$0, -56(%rbp)
	movl	$40, %ebx
	xorl	%r13d, %r13d
	jmp	.L866
	.p2align 4,,10
	.p2align 3
.L867:
	movl	$40, %ebx
	jmp	.L860
.L861:
	cmpq	%rcx, %rdi
	movq	%rcx, %rbx
	cmovbe	%rdi, %rbx
	imulq	$40, %rbx, %rbx
	jmp	.L860
.L875:
	leaq	.LC7(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE23158:
	.size	_ZNSt6vectorISt4pairIPN2v88internal13GlobalHandles4NodeENS3_22PendingPhantomCallbackEESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, .-_ZNSt6vectorISt4pairIPN2v88internal13GlobalHandles4NodeENS3_22PendingPhantomCallbackEESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.section	.text._ZNSt6vectorISt4pairIPN2v88internal13GlobalHandles10TracedNodeENS3_22PendingPhantomCallbackEESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt4pairIPN2v88internal13GlobalHandles10TracedNodeENS3_22PendingPhantomCallbackEESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt4pairIPN2v88internal13GlobalHandles10TracedNodeENS3_22PendingPhantomCallbackEESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.type	_ZNSt6vectorISt4pairIPN2v88internal13GlobalHandles10TracedNodeENS3_22PendingPhantomCallbackEESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, @function
_ZNSt6vectorISt4pairIPN2v88internal13GlobalHandles10TracedNodeENS3_22PendingPhantomCallbackEESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_:
.LFB23165:
	.cfi_startproc
	endbr64
	movabsq	$-3689348814741910323, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rsi
	movq	(%rdi), %r14
	movq	%rsi, %rax
	subq	%r14, %rax
	sarq	$3, %rax
	imulq	%rcx, %rax
	movabsq	$230584300921369395, %rcx
	cmpq	%rcx, %rax
	je	.L894
	movq	%r12, %r8
	movq	%rdi, %r15
	subq	%r14, %r8
	testq	%rax, %rax
	je	.L886
	movabsq	$9223372036854775800, %rbx
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L895
.L879:
	movq	%rbx, %rdi
	movq	%rdx, -80(%rbp)
	movq	%r8, -72(%rbp)
	movq	%rsi, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rsi
	movq	-72(%rbp), %r8
	movq	%rax, %r13
	leaq	(%rax,%rbx), %rax
	movq	-80(%rbp), %rdx
	movq	%rax, -56(%rbp)
	leaq	40(%r13), %rbx
.L885:
	movdqu	(%rdx), %xmm3
	movdqu	16(%rdx), %xmm4
	movq	32(%rdx), %rax
	movups	%xmm3, 0(%r13,%r8)
	movq	%rax, 32(%r13,%r8)
	movups	%xmm4, 16(%r13,%r8)
	cmpq	%r14, %r12
	je	.L881
	movq	%r13, %rdx
	movq	%r14, %rax
	.p2align 4,,10
	.p2align 3
.L882:
	movdqu	(%rax), %xmm1
	addq	$40, %rax
	addq	$40, %rdx
	movups	%xmm1, -40(%rdx)
	movdqu	-24(%rax), %xmm2
	movups	%xmm2, -24(%rdx)
	movq	-8(%rax), %rcx
	movq	%rcx, -8(%rdx)
	cmpq	%rax, %r12
	jne	.L882
	leaq	-40(%r12), %rax
	subq	%r14, %rax
	shrq	$3, %rax
	leaq	80(%r13,%rax,8), %rbx
.L881:
	cmpq	%rsi, %r12
	je	.L883
	subq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	-40(%rsi), %rax
	movq	%r12, %rsi
	shrq	$3, %rax
	leaq	40(,%rax,8), %rdx
	movq	%rdx, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %rdx
	addq	%rdx, %rbx
.L883:
	testq	%r14, %r14
	je	.L884
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L884:
	movq	-56(%rbp), %rax
	movq	%r13, %xmm0
	movq	%rbx, %xmm5
	punpcklqdq	%xmm5, %xmm0
	movq	%rax, 16(%r15)
	movups	%xmm0, (%r15)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L895:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L880
	movq	$0, -56(%rbp)
	movl	$40, %ebx
	xorl	%r13d, %r13d
	jmp	.L885
	.p2align 4,,10
	.p2align 3
.L886:
	movl	$40, %ebx
	jmp	.L879
.L880:
	cmpq	%rcx, %rdi
	movq	%rcx, %rbx
	cmovbe	%rdi, %rbx
	imulq	$40, %rbx, %rbx
	jmp	.L879
.L894:
	leaq	.LC7(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE23165:
	.size	_ZNSt6vectorISt4pairIPN2v88internal13GlobalHandles10TracedNodeENS3_22PendingPhantomCallbackEESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, .-_ZNSt6vectorISt4pairIPN2v88internal13GlobalHandles10TracedNodeENS3_22PendingPhantomCallbackEESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.section	.rodata._ZN2v88internal13GlobalHandles48IterateYoungWeakUnmodifiedRootsForPhantomHandlesEPNS0_11RootVisitorEPFbPNS0_4HeapENS0_14FullObjectSlotEE.str1.1,"aMS",@progbits,1
.LC10:
	.string	"unreachable code"
	.section	.text._ZN2v88internal13GlobalHandles48IterateYoungWeakUnmodifiedRootsForPhantomHandlesEPNS0_11RootVisitorEPFbPNS0_4HeapENS0_14FullObjectSlotEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13GlobalHandles48IterateYoungWeakUnmodifiedRootsForPhantomHandlesEPNS0_11RootVisitorEPFbPNS0_4HeapENS0_14FullObjectSlotEE
	.type	_ZN2v88internal13GlobalHandles48IterateYoungWeakUnmodifiedRootsForPhantomHandlesEPNS0_11RootVisitorEPFbPNS0_4HeapENS0_14FullObjectSlotEE, @function
_ZN2v88internal13GlobalHandles48IterateYoungWeakUnmodifiedRootsForPhantomHandlesEPNS0_11RootVisitorEPFbPNS0_4HeapENS0_14FullObjectSlotEE:
.LFB18881:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE(%rip), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$136, %rsp
	movq	24(%rdi), %r12
	movq	16(%rdi), %r13
	movq	%rsi, -152(%rbp)
	movq	%rdx, -136(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%r13, %r12
	je	.L914
	.p2align 4,,10
	.p2align 3
.L913:
	movq	0(%r13), %rbx
	movzbl	11(%rbx), %eax
	testb	$8, %al
	jne	.L900
	testb	$16, %al
	jne	.L901
.L900:
	movl	%eax, %edx
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L902
	cmpb	$4, %dl
	jne	.L901
	shrb	$6, %al
	jne	.L901
	.p2align 4,,10
	.p2align 3
.L902:
	movq	(%r14), %rax
	movq	%rbx, %rsi
	leaq	37592(%rax), %rdi
	movq	-136(%rbp), %rax
	call	*%rax
	testb	%al, %al
	je	.L903
	movzbl	11(%rbx), %eax
	movl	%eax, %edx
	shrb	$6, %dl
	cmpb	$3, %dl
	je	.L962
	subl	$1, %edx
	cmpb	$1, %dl
	ja	.L963
	andl	$-8, %eax
	pxor	%xmm0, %xmm0
	orl	$3, %eax
	movb	%al, 11(%rbx)
	movaps	%xmm0, -112(%rbp)
	movzbl	11(%rbx), %eax
	shrb	$6, %al
	cmpb	$1, %al
	je	.L907
	movq	(%rbx), %rdi
	testb	$1, %dil
	jne	.L964
.L907:
	movq	16(%rbx), %rax
	movq	24(%rbx), %rdx
	movq	$51729, (%rbx)
	movq	%rbx, -96(%rbp)
	movq	96(%r14), %rsi
	movq	%rax, -80(%rbp)
	movq	-112(%rbp), %rax
	movq	%rdx, -88(%rbp)
	movq	%rax, -72(%rbp)
	movq	-104(%rbp), %rax
	movq	%rax, -64(%rbp)
	cmpq	104(%r14), %rsi
	je	.L909
	movdqa	-96(%rbp), %xmm3
	movups	%xmm3, (%rsi)
	movdqa	-80(%rbp), %xmm4
	movups	%xmm4, 16(%rsi)
	movq	-64(%rbp), %rax
	movq	%rax, 32(%rsi)
	addq	$40, 96(%r14)
.L910:
	movzbl	11(%rbx), %eax
	andl	$-8, %eax
	orl	$4, %eax
	movb	%al, 11(%rbx)
	.p2align 4,,10
	.p2align 3
.L901:
	addq	$8, %r13
	cmpq	%r13, %r12
	jne	.L913
.L969:
	movq	%r14, %rbx
.L914:
	movq	(%rbx), %rax
	movq	48(%rbx), %r12
	movq	39720(%rax), %rax
	movq	%rax, -168(%rbp)
	movq	56(%rbx), %rax
	movq	%rax, -144(%rbp)
	cmpq	%r12, %rax
	je	.L896
	.p2align 4,,10
	.p2align 3
.L934:
	movq	(%r12), %r15
	testb	$3, 11(%r15)
	je	.L916
	movq	(%rbx), %rax
	movq	%r15, %rsi
	leaq	37592(%rax), %rdi
	movq	-136(%rbp), %rax
	call	*%rax
	testb	%al, %al
	je	.L917
	cmpq	$0, 24(%r15)
	je	.L965
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	(%r15), %rax
	movq	-1(%rax), %r14
	leaq	-1(%rax), %r8
	movzbl	7(%r14), %r13d
	sall	$3, %r13d
	jne	.L966
.L922:
	movq	%rbx, -160(%rbp)
	movl	%r13d, %r13d
	xorl	%r14d, %r14d
	movq	%r8, %rbx
	.p2align 4,,10
	.p2align 3
.L930:
	cmpq	%r13, %r14
	je	.L961
	movq	(%rbx), %rdx
	movl	$24, %eax
	movzwl	11(%rdx), %edi
	cmpw	$1057, %di
	je	.L927
	movsbl	13(%rdx), %esi
	shrl	$31, %esi
	call	_ZN2v88internal8JSObject13GetHeaderSizeENS0_12InstanceTypeEb@PLT
.L927:
	leal	(%rax,%r14,8), %eax
	cltq
	movq	(%rax,%rbx), %rax
	testb	$1, %al
	jne	.L928
	movq	%rax, -112(%rbp,%r14,8)
.L928:
	addq	$1, %r14
	cmpq	$2, %r14
	jne	.L930
.L961:
	movq	16(%r15), %rax
	movq	$51729, (%r15)
	movq	24(%r15), %rdx
	movq	-160(%rbp), %rbx
	movq	%r15, -96(%rbp)
	movq	%rax, -80(%rbp)
	movq	-112(%rbp), %rax
	movq	%rdx, -88(%rbp)
	movq	120(%rbx), %rsi
	movq	%rax, -72(%rbp)
	movq	-104(%rbp), %rax
	movq	%rax, -64(%rbp)
	cmpq	128(%rbx), %rsi
	je	.L967
	movdqa	-96(%rbp), %xmm1
	movups	%xmm1, (%rsi)
	movdqa	-80(%rbp), %xmm2
	movups	%xmm2, 16(%rsi)
	movq	-64(%rbp), %rax
	movq	%rax, 32(%rsi)
	addq	$40, 120(%rbx)
.L931:
	movzbl	11(%r15), %eax
	andl	$-4, %eax
	orl	$2, %eax
	movb	%al, 11(%r15)
	.p2align 4,,10
	.p2align 3
.L916:
	addq	$8, %r12
	cmpq	%r12, -144(%rbp)
	jne	.L934
.L896:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L968
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L903:
	.cfi_restore_state
	movq	-152(%rbp), %rax
	xorl	%edx, %edx
	movq	(%rax), %r10
	movzbl	11(%rbx), %eax
	andl	$7, %eax
	movq	24(%r10), %r8
	cmpb	$1, %al
	jne	.L911
	movq	16(%rbx), %rdx
.L911:
	cmpq	%r15, %r8
	jne	.L912
	addq	$8, %r13
	leaq	8(%rbx), %r8
	movq	%rbx, %rcx
	movl	$13, %esi
	movq	-152(%rbp), %rdi
	call	*16(%r10)
	cmpq	%r13, %r12
	jne	.L913
	jmp	.L969
	.p2align 4,,10
	.p2align 3
.L917:
	movzbl	11(%r15), %eax
	testb	$8, %al
	jne	.L916
	movq	-152(%rbp), %rdi
	orl	$8, %eax
	leaq	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE(%rip), %rcx
	movb	%al, 11(%r15)
	movq	(%rdi), %rax
	movq	24(%rax), %r8
	cmpq	%rcx, %r8
	jne	.L933
	leaq	8(%r15), %r8
	movq	%r15, %rcx
	xorl	%edx, %edx
	movl	$13, %esi
	call	*16(%rax)
	jmp	.L916
	.p2align 4,,10
	.p2align 3
.L965:
	testb	$16, 11(%r15)
	jne	.L970
	movq	-168(%rbp), %rax
	movq	%r15, -120(%rbp)
	leaq	_ZN2v818EmbedderHeapTracer25ResetHandleInNonTracingGCERKNS_12TracedGlobalINS_5ValueEEE(%rip), %rcx
	movq	8(%rax), %rdi
	movq	(%rdi), %rax
	movq	88(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L971
.L920:
	addq	$1, 80(%rbx)
	jmp	.L916
	.p2align 4,,10
	.p2align 3
.L966:
	movzwl	11(%r14), %edi
	movl	$24, %esi
	cmpw	$1057, %di
	je	.L923
	movq	%r8, -160(%rbp)
	movsbl	13(%r14), %esi
	shrl	$31, %esi
	call	_ZN2v88internal8JSObject13GetHeaderSizeENS0_12InstanceTypeEb@PLT
	movq	-160(%rbp), %r8
	movl	%eax, %esi
.L923:
	movzbl	7(%r14), %eax
	movzbl	8(%r14), %edx
	subl	%esi, %r13d
	sarl	$3, %r13d
	subl	%edx, %eax
	subl	%eax, %r13d
	jmp	.L922
	.p2align 4,,10
	.p2align 3
.L970:
	movq	16(%r15), %rax
	movq	%r15, %rdi
	movq	$0, (%rax)
	call	_ZN2v88internal13GlobalHandles9NodeSpaceINS1_10TracedNodeEE7ReleaseEPS3_
	jmp	.L920
	.p2align 4,,10
	.p2align 3
.L963:
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L962:
	andl	$-8, %eax
	movq	%rbx, %rdi
	addq	$8, %r13
	orl	$3, %eax
	movb	%al, 11(%rbx)
	movq	16(%rbx), %rax
	movq	$0, (%rax)
	call	_ZN2v88internal13GlobalHandles9NodeSpaceINS1_4NodeEE7ReleaseEPS3_
	addq	$1, 80(%r14)
	cmpq	%r13, %r12
	jne	.L913
	jmp	.L969
	.p2align 4,,10
	.p2align 3
.L912:
	addq	$8, %r13
	movq	-152(%rbp), %rdi
	movq	%rbx, %rcx
	movl	$13, %esi
	call	*%r8
	cmpq	%r13, %r12
	jne	.L913
	jmp	.L969
	.p2align 4,,10
	.p2align 3
.L964:
	movq	-1(%rdi), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L907
	leaq	-112(%rbp), %rsi
	call	_ZN2v88internal12_GLOBAL__N_121ExtractInternalFieldsENS0_8JSObjectEPPvi.constprop.1
	jmp	.L907
	.p2align 4,,10
	.p2align 3
.L933:
	movq	%r15, %rcx
	xorl	%edx, %edx
	movl	$13, %esi
	call	*%r8
	jmp	.L916
	.p2align 4,,10
	.p2align 3
.L967:
	leaq	-96(%rbp), %rdx
	leaq	112(%rbx), %rdi
	call	_ZNSt6vectorISt4pairIPN2v88internal13GlobalHandles10TracedNodeENS3_22PendingPhantomCallbackEESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	jmp	.L931
	.p2align 4,,10
	.p2align 3
.L909:
	leaq	-96(%rbp), %rdx
	leaq	88(%r14), %rdi
	call	_ZNSt6vectorISt4pairIPN2v88internal13GlobalHandles4NodeENS3_22PendingPhantomCallbackEESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	jmp	.L910
.L971:
	leaq	-120(%rbp), %rsi
	call	*%rax
	jmp	.L920
.L968:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18881:
	.size	_ZN2v88internal13GlobalHandles48IterateYoungWeakUnmodifiedRootsForPhantomHandlesEPNS0_11RootVisitorEPFbPNS0_4HeapENS0_14FullObjectSlotEE, .-_ZN2v88internal13GlobalHandles48IterateYoungWeakUnmodifiedRootsForPhantomHandlesEPNS0_11RootVisitorEPFbPNS0_4HeapENS0_14FullObjectSlotEE
	.section	.text._ZN2v88internal13GlobalHandles33IterateWeakRootsForPhantomHandlesEPFbPNS0_4HeapENS0_14FullObjectSlotEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13GlobalHandles33IterateWeakRootsForPhantomHandlesEPFbPNS0_4HeapENS0_14FullObjectSlotEE
	.type	_ZN2v88internal13GlobalHandles33IterateWeakRootsForPhantomHandlesEPFbPNS0_4HeapENS0_14FullObjectSlotEE, @function
_ZN2v88internal13GlobalHandles33IterateWeakRootsForPhantomHandlesEPFbPNS0_4HeapENS0_14FullObjectSlotEE:
.LFB18875:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$104, %rsp
	movq	%rsi, -136(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	16(%rax), %r13
	testq	%r13, %r13
	je	.L974
	.p2align 4,,10
	.p2align 3
.L973:
	movq	%rbx, %r12
	salq	$5, %r12
	addq	%r13, %r12
	movzbl	11(%r12), %edx
	movl	%edx, %eax
	andl	$7, %eax
	leal	-2(%rax), %ecx
	cmpb	$1, %cl
	ja	.L1042
.L975:
	movq	(%r15), %rax
	movq	%r12, %rsi
	leaq	37592(%rax), %rdi
	movq	-136(%rbp), %rax
	call	*%rax
	testb	%al, %al
	je	.L1040
	movzbl	11(%r12), %eax
	movl	%eax, %edx
	shrb	$6, %dl
	cmpb	$3, %dl
	je	.L1043
	subl	$1, %edx
	cmpb	$1, %dl
	ja	.L1040
	andl	$-8, %eax
	pxor	%xmm0, %xmm0
	orl	$3, %eax
	movb	%al, 11(%r12)
	movaps	%xmm0, -112(%rbp)
	movzbl	11(%r12), %eax
	shrb	$6, %al
	cmpb	$1, %al
	je	.L980
	movq	(%r12), %rdi
	testb	$1, %dil
	jne	.L1044
.L980:
	movq	16(%r12), %rax
	movq	%rbx, %rdx
	movq	$51729, (%r12)
	salq	$5, %rdx
	movq	96(%r15), %rsi
	movq	24(%r13,%rdx), %rdx
	movq	%rax, -80(%rbp)
	movq	-112(%rbp), %rax
	movq	%r12, -96(%rbp)
	movq	%rdx, -88(%rbp)
	movq	%rax, -72(%rbp)
	movq	-104(%rbp), %rax
	movq	%rax, -64(%rbp)
	cmpq	104(%r15), %rsi
	je	.L982
	movdqa	-96(%rbp), %xmm2
	movups	%xmm2, (%rsi)
	movdqa	-80(%rbp), %xmm3
	movups	%xmm3, 16(%rsi)
	movq	-64(%rbp), %rax
	movq	%rax, 32(%rsi)
	addq	$40, 96(%r15)
.L983:
	movzbl	11(%r12), %eax
	andl	$-8, %eax
	orl	$4, %eax
	movb	%al, 11(%r12)
	.p2align 4,,10
	.p2align 3
.L1040:
	addq	$1, %rbx
	cmpq	$255, %rbx
	jbe	.L973
	movq	8216(%r13), %r13
	testq	%r13, %r13
	je	.L974
	xorl	%ebx, %ebx
	jmp	.L973
	.p2align 4,,10
	.p2align 3
.L1042:
	cmpb	$4, %al
	jne	.L1040
	shrb	$6, %dl
	jne	.L1040
	jmp	.L975
	.p2align 4,,10
	.p2align 3
.L974:
	movq	40(%r15), %rax
	xorl	%r14d, %r14d
	movq	16(%rax), %rcx
	leaq	-96(%rbp), %rax
	movq	%rax, -144(%rbp)
	testq	%rcx, %rcx
	je	.L972
	.p2align 4,,10
	.p2align 3
.L987:
	movq	%r14, %r12
	salq	$5, %r12
	addq	%rcx, %r12
	testb	$3, 11(%r12)
	je	.L1041
	movq	(%r15), %rax
	movq	%rcx, -120(%rbp)
	movq	%r12, %rsi
	leaq	37592(%rax), %rdi
	movq	-136(%rbp), %rax
	call	*%rax
	movq	-120(%rbp), %rcx
	testb	%al, %al
	je	.L1041
	cmpq	$0, 24(%r12)
	je	.L1045
	pxor	%xmm1, %xmm1
	movaps	%xmm1, -112(%rbp)
	movq	(%r12), %rax
	movq	-1(%rax), %rbx
	leaq	-1(%rax), %rdx
	movzbl	7(%rbx), %r13d
	sall	$3, %r13d
	je	.L993
	movzwl	11(%rbx), %eax
	movl	$24, %edi
	cmpw	$1057, %ax
	je	.L994
	movq	%rdx, -128(%rbp)
	movzwl	%ax, %edi
	movq	%rcx, -120(%rbp)
	movsbl	13(%rbx), %esi
	shrl	$31, %esi
	call	_ZN2v88internal8JSObject13GetHeaderSizeENS0_12InstanceTypeEb@PLT
	movq	-128(%rbp), %rdx
	movq	-120(%rbp), %rcx
	movl	%eax, %edi
.L994:
	movzbl	7(%rbx), %eax
	movzbl	8(%rbx), %esi
	subl	%edi, %r13d
	sarl	$3, %r13d
	subl	%esi, %eax
	subl	%eax, %r13d
.L993:
	movl	%r13d, %r13d
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L1001:
	cmpq	%r13, %rbx
	je	.L1000
	movq	(%rdx), %rsi
	movl	$24, %eax
	movzwl	11(%rsi), %edi
	cmpw	$1057, %di
	je	.L998
	movq	%rdx, -128(%rbp)
	movq	%rcx, -120(%rbp)
	movsbl	13(%rsi), %esi
	shrl	$31, %esi
	call	_ZN2v88internal8JSObject13GetHeaderSizeENS0_12InstanceTypeEb@PLT
	movq	-128(%rbp), %rdx
	movq	-120(%rbp), %rcx
.L998:
	leal	(%rax,%rbx,8), %eax
	cltq
	movq	(%rax,%rdx), %rax
	testb	$1, %al
	jne	.L999
	movq	%rax, -112(%rbp,%rbx,8)
.L999:
	addq	$1, %rbx
	cmpq	$2, %rbx
	jne	.L1001
.L1000:
	movq	16(%r12), %rax
	movq	%r14, %rdx
	movq	$51729, (%r12)
	salq	$5, %rdx
	movq	120(%r15), %rsi
	movq	24(%rcx,%rdx), %rdx
	movq	%rax, -80(%rbp)
	movq	-112(%rbp), %rax
	movq	%r12, -96(%rbp)
	movq	%rdx, -88(%rbp)
	movq	%rax, -72(%rbp)
	movq	-104(%rbp), %rax
	movq	%rax, -64(%rbp)
	cmpq	128(%r15), %rsi
	je	.L1046
	movdqa	-96(%rbp), %xmm4
	movups	%xmm4, (%rsi)
	movdqa	-80(%rbp), %xmm5
	movups	%xmm5, 16(%rsi)
	movq	-64(%rbp), %rax
	movq	%rax, 32(%rsi)
	addq	$40, 120(%r15)
.L1002:
	movzbl	11(%r12), %eax
	andl	$-4, %eax
	orl	$2, %eax
	movb	%al, 11(%r12)
	.p2align 4,,10
	.p2align 3
.L1041:
	addq	$1, %r14
	cmpq	$255, %r14
	jbe	.L987
	movq	8216(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L972
	xorl	%r14d, %r14d
	jmp	.L987
	.p2align 4,,10
	.p2align 3
.L972:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1047
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1045:
	.cfi_restore_state
	testb	$16, 11(%r12)
	je	.L991
	movq	16(%r12), %rax
	movq	$0, (%rax)
.L991:
	movq	%r12, %rdi
	movq	%rcx, -120(%rbp)
	call	_ZN2v88internal13GlobalHandles9NodeSpaceINS1_10TracedNodeEE7ReleaseEPS3_
	addq	$1, 80(%r15)
	movq	-120(%rbp), %rcx
	jmp	.L1041
	.p2align 4,,10
	.p2align 3
.L1043:
	andl	$-8, %eax
	movq	%r12, %rdi
	orl	$3, %eax
	movb	%al, 11(%r12)
	movq	16(%r12), %rax
	movq	$0, (%rax)
	call	_ZN2v88internal13GlobalHandles9NodeSpaceINS1_4NodeEE7ReleaseEPS3_
	addq	$1, 80(%r15)
	jmp	.L1040
	.p2align 4,,10
	.p2align 3
.L1044:
	movq	-1(%rdi), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L980
	leaq	-112(%rbp), %rsi
	call	_ZN2v88internal12_GLOBAL__N_121ExtractInternalFieldsENS0_8JSObjectEPPvi.constprop.1
	jmp	.L980
	.p2align 4,,10
	.p2align 3
.L982:
	leaq	-96(%rbp), %rdx
	leaq	88(%r15), %rdi
	call	_ZNSt6vectorISt4pairIPN2v88internal13GlobalHandles4NodeENS3_22PendingPhantomCallbackEESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	jmp	.L983
	.p2align 4,,10
	.p2align 3
.L1046:
	movq	-144(%rbp), %rdx
	leaq	112(%r15), %rdi
	movq	%rcx, -120(%rbp)
	call	_ZNSt6vectorISt4pairIPN2v88internal13GlobalHandles10TracedNodeENS3_22PendingPhantomCallbackEESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	movq	-120(%rbp), %rcx
	jmp	.L1002
.L1047:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18875:
	.size	_ZN2v88internal13GlobalHandles33IterateWeakRootsForPhantomHandlesEPFbPNS0_4HeapENS0_14FullObjectSlotEE, .-_ZN2v88internal13GlobalHandles33IterateWeakRootsForPhantomHandlesEPFbPNS0_4HeapENS0_14FullObjectSlotEE
	.section	.text._ZNSt6vectorIPN2v88internal13GlobalHandles4NodeESaIS4_EE17_M_default_appendEm,"axG",@progbits,_ZNSt6vectorIPN2v88internal13GlobalHandles4NodeESaIS4_EE17_M_default_appendEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal13GlobalHandles4NodeESaIS4_EE17_M_default_appendEm
	.type	_ZNSt6vectorIPN2v88internal13GlobalHandles4NodeESaIS4_EE17_M_default_appendEm, @function
_ZNSt6vectorIPN2v88internal13GlobalHandles4NodeESaIS4_EE17_M_default_appendEm:
.LFB23275:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L1067
	movabsq	$1152921504606846975, %rdx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %rcx
	movq	16(%r12), %rax
	movq	%rcx, %rsi
	subq	(%rdi), %rsi
	subq	%rcx, %rax
	movq	%rdx, %rdi
	movq	%rsi, %r13
	sarq	$3, %rax
	sarq	$3, %r13
	subq	%r13, %rdi
	cmpq	%rbx, %rax
	jb	.L1050
	salq	$3, %rbx
	movq	%rcx, %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	call	memset@PLT
	movq	%rax, %rcx
	addq	%rbx, %rcx
	movq	%rcx, 8(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1067:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L1050:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	cmpq	%rbx, %rdi
	jb	.L1070
	cmpq	%rbx, %r13
	movq	%rbx, %r14
	movq	%rsi, -56(%rbp)
	cmovnb	%r13, %r14
	addq	%r13, %r14
	cmpq	%rdx, %r14
	cmova	%rdx, %r14
	salq	$3, %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	leaq	0(,%rbx,8), %rdx
	movq	%rax, %r15
	leaq	(%rax,%rsi), %rdi
	xorl	%esi, %esi
	call	memset@PLT
	movq	(%r12), %r8
	movq	8(%r12), %rdx
	subq	%r8, %rdx
	testq	%rdx, %rdx
	jg	.L1071
	testq	%r8, %r8
	jne	.L1054
.L1055:
	addq	%r13, %rbx
	addq	%r15, %r14
	movq	%r15, (%r12)
	leaq	(%r15,%rbx,8), %rax
	movq	%r14, 16(%r12)
	movq	%rax, 8(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1071:
	.cfi_restore_state
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r8, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %r8
.L1054:
	movq	%r8, %rdi
	call	_ZdlPv@PLT
	jmp	.L1055
.L1070:
	leaq	.LC9(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE23275:
	.size	_ZNSt6vectorIPN2v88internal13GlobalHandles4NodeESaIS4_EE17_M_default_appendEm, .-_ZNSt6vectorIPN2v88internal13GlobalHandles4NodeESaIS4_EE17_M_default_appendEm
	.section	.text._ZN2v88internal13GlobalHandles31UpdateAndCompactListOfYoungNodeINS1_4NodeEEEvPSt6vectorIPT_SaIS6_EE,"axG",@progbits,_ZN2v88internal13GlobalHandles31UpdateAndCompactListOfYoungNodeINS1_4NodeEEEvPSt6vectorIPT_SaIS6_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13GlobalHandles31UpdateAndCompactListOfYoungNodeINS1_4NodeEEEvPSt6vectorIPT_SaIS6_EE
	.type	_ZN2v88internal13GlobalHandles31UpdateAndCompactListOfYoungNodeINS1_4NodeEEEvPSt6vectorIPT_SaIS6_EE, @function
_ZN2v88internal13GlobalHandles31UpdateAndCompactListOfYoungNodeINS1_4NodeEEEvPSt6vectorIPT_SaIS6_EE:
.LFB21194:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rdx
	movq	8(%rsi), %r9
	movq	%rsi, %r10
	cmpq	%rdx, %r9
	je	.L1072
	xorl	%esi, %esi
	jmp	.L1077
	.p2align 4,,10
	.p2align 3
.L1084:
	movq	(%rcx), %r8
	testb	$1, %r8b
	je	.L1075
	andq	$-262144, %r8
	testb	$24, 8(%r8)
	je	.L1075
	movq	(%r10), %rax
	movq	%rcx, (%rax,%rsi,8)
	movq	(%rdi), %rax
	addq	$1, %rsi
	addl	$1, 39564(%rax)
.L1076:
	addq	$8, %rdx
	cmpq	%rdx, %r9
	je	.L1083
.L1077:
	movq	(%rdx), %rcx
	movzbl	11(%rcx), %eax
	testb	$7, %al
	jne	.L1084
	andl	$-33, %eax
	addq	$8, %rdx
	movb	%al, 11(%rcx)
	movq	(%rdi), %rax
	addl	$1, 39560(%rax)
	cmpq	%rdx, %r9
	jne	.L1077
.L1083:
	movq	8(%r10), %rdx
	movq	(%r10), %rcx
	movq	%rdx, %rax
	subq	%rcx, %rax
	sarq	$3, %rax
	cmpq	%rax, %rsi
	ja	.L1085
	jnb	.L1072
	leaq	(%rcx,%rsi,8), %rax
	cmpq	%rax, %rdx
	je	.L1072
	movq	%rax, 8(%r10)
.L1072:
	ret
	.p2align 4,,10
	.p2align 3
.L1075:
	andl	$-33, %eax
	movb	%al, 11(%rcx)
	movq	(%rdi), %rax
	addl	$1, 39568(%rax)
	jmp	.L1076
	.p2align 4,,10
	.p2align 3
.L1085:
	subq	%rax, %rsi
	movq	%r10, %rdi
	jmp	_ZNSt6vectorIPN2v88internal13GlobalHandles4NodeESaIS4_EE17_M_default_appendEm
	.cfi_endproc
.LFE21194:
	.size	_ZN2v88internal13GlobalHandles31UpdateAndCompactListOfYoungNodeINS1_4NodeEEEvPSt6vectorIPT_SaIS6_EE, .-_ZN2v88internal13GlobalHandles31UpdateAndCompactListOfYoungNodeINS1_4NodeEEEvPSt6vectorIPT_SaIS6_EE
	.section	.text._ZNSt6vectorIPN2v88internal13GlobalHandles10TracedNodeESaIS4_EE17_M_default_appendEm,"axG",@progbits,_ZNSt6vectorIPN2v88internal13GlobalHandles10TracedNodeESaIS4_EE17_M_default_appendEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal13GlobalHandles10TracedNodeESaIS4_EE17_M_default_appendEm
	.type	_ZNSt6vectorIPN2v88internal13GlobalHandles10TracedNodeESaIS4_EE17_M_default_appendEm, @function
_ZNSt6vectorIPN2v88internal13GlobalHandles10TracedNodeESaIS4_EE17_M_default_appendEm:
.LFB23279:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L1105
	movabsq	$1152921504606846975, %rdx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %rcx
	movq	16(%r12), %rax
	movq	%rcx, %rsi
	subq	(%rdi), %rsi
	subq	%rcx, %rax
	movq	%rdx, %rdi
	movq	%rsi, %r13
	sarq	$3, %rax
	sarq	$3, %r13
	subq	%r13, %rdi
	cmpq	%rbx, %rax
	jb	.L1088
	salq	$3, %rbx
	movq	%rcx, %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	call	memset@PLT
	movq	%rax, %rcx
	addq	%rbx, %rcx
	movq	%rcx, 8(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1105:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L1088:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	cmpq	%rbx, %rdi
	jb	.L1108
	cmpq	%rbx, %r13
	movq	%rbx, %r14
	movq	%rsi, -56(%rbp)
	cmovnb	%r13, %r14
	addq	%r13, %r14
	cmpq	%rdx, %r14
	cmova	%rdx, %r14
	salq	$3, %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	leaq	0(,%rbx,8), %rdx
	movq	%rax, %r15
	leaq	(%rax,%rsi), %rdi
	xorl	%esi, %esi
	call	memset@PLT
	movq	(%r12), %r8
	movq	8(%r12), %rdx
	subq	%r8, %rdx
	testq	%rdx, %rdx
	jg	.L1109
	testq	%r8, %r8
	jne	.L1092
.L1093:
	addq	%r13, %rbx
	addq	%r15, %r14
	movq	%r15, (%r12)
	leaq	(%r15,%rbx,8), %rax
	movq	%r14, 16(%r12)
	movq	%rax, 8(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1109:
	.cfi_restore_state
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r8, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %r8
.L1092:
	movq	%r8, %rdi
	call	_ZdlPv@PLT
	jmp	.L1093
.L1108:
	leaq	.LC9(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE23279:
	.size	_ZNSt6vectorIPN2v88internal13GlobalHandles10TracedNodeESaIS4_EE17_M_default_appendEm, .-_ZNSt6vectorIPN2v88internal13GlobalHandles10TracedNodeESaIS4_EE17_M_default_appendEm
	.section	.text._ZN2v88internal13GlobalHandles31UpdateAndCompactListOfYoungNodeINS1_10TracedNodeEEEvPSt6vectorIPT_SaIS6_EE,"axG",@progbits,_ZN2v88internal13GlobalHandles31UpdateAndCompactListOfYoungNodeINS1_10TracedNodeEEEvPSt6vectorIPT_SaIS6_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13GlobalHandles31UpdateAndCompactListOfYoungNodeINS1_10TracedNodeEEEvPSt6vectorIPT_SaIS6_EE
	.type	_ZN2v88internal13GlobalHandles31UpdateAndCompactListOfYoungNodeINS1_10TracedNodeEEEvPSt6vectorIPT_SaIS6_EE, @function
_ZN2v88internal13GlobalHandles31UpdateAndCompactListOfYoungNodeINS1_10TracedNodeEEEvPSt6vectorIPT_SaIS6_EE:
.LFB21195:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rdx
	movq	8(%rsi), %r9
	movq	%rsi, %r10
	cmpq	%rdx, %r9
	je	.L1110
	xorl	%esi, %esi
	jmp	.L1115
	.p2align 4,,10
	.p2align 3
.L1122:
	movq	(%rcx), %r8
	testb	$1, %r8b
	je	.L1113
	andq	$-262144, %r8
	testb	$24, 8(%r8)
	je	.L1113
	movq	(%r10), %rax
	movq	%rcx, (%rax,%rsi,8)
	movq	(%rdi), %rax
	addq	$1, %rsi
	addl	$1, 39564(%rax)
.L1114:
	addq	$8, %rdx
	cmpq	%rdx, %r9
	je	.L1121
.L1115:
	movq	(%rdx), %rcx
	movzbl	11(%rcx), %eax
	testb	$3, %al
	jne	.L1122
	andl	$-5, %eax
	addq	$8, %rdx
	movb	%al, 11(%rcx)
	movq	(%rdi), %rax
	addl	$1, 39560(%rax)
	cmpq	%rdx, %r9
	jne	.L1115
.L1121:
	movq	8(%r10), %rdx
	movq	(%r10), %rcx
	movq	%rdx, %rax
	subq	%rcx, %rax
	sarq	$3, %rax
	cmpq	%rax, %rsi
	ja	.L1123
	jnb	.L1110
	leaq	(%rcx,%rsi,8), %rax
	cmpq	%rax, %rdx
	je	.L1110
	movq	%rax, 8(%r10)
.L1110:
	ret
	.p2align 4,,10
	.p2align 3
.L1113:
	andl	$-5, %eax
	movb	%al, 11(%rcx)
	movq	(%rdi), %rax
	addl	$1, 39568(%rax)
	jmp	.L1114
	.p2align 4,,10
	.p2align 3
.L1123:
	subq	%rax, %rsi
	movq	%r10, %rdi
	jmp	_ZNSt6vectorIPN2v88internal13GlobalHandles10TracedNodeESaIS4_EE17_M_default_appendEm
	.cfi_endproc
.LFE21195:
	.size	_ZN2v88internal13GlobalHandles31UpdateAndCompactListOfYoungNodeINS1_10TracedNodeEEEvPSt6vectorIPT_SaIS6_EE, .-_ZN2v88internal13GlobalHandles31UpdateAndCompactListOfYoungNodeINS1_10TracedNodeEEEvPSt6vectorIPT_SaIS6_EE
	.section	.text._ZN2v88internal13GlobalHandles22UpdateListOfYoungNodesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13GlobalHandles22UpdateListOfYoungNodesEv
	.type	_ZN2v88internal13GlobalHandles22UpdateListOfYoungNodesEv, @function
_ZN2v88internal13GlobalHandles22UpdateListOfYoungNodesEv:
.LFB18887:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16(%rdi), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal13GlobalHandles31UpdateAndCompactListOfYoungNodeINS1_4NodeEEEvPSt6vectorIPT_SaIS6_EE
	addq	$8, %rsp
	leaq	48(%r12), %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal13GlobalHandles31UpdateAndCompactListOfYoungNodeINS1_10TracedNodeEEEvPSt6vectorIPT_SaIS6_EE
	.cfi_endproc
.LFE18887:
	.size	_ZN2v88internal13GlobalHandles22UpdateListOfYoungNodesEv, .-_ZN2v88internal13GlobalHandles22UpdateListOfYoungNodesEv
	.section	.text._ZN2v88internal13GlobalHandles31PostGarbageCollectionProcessingENS0_16GarbageCollectorENS_15GCCallbackFlagsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13GlobalHandles31PostGarbageCollectionProcessingENS0_16GarbageCollectorENS_15GCCallbackFlagsE
	.type	_ZN2v88internal13GlobalHandles31PostGarbageCollectionProcessingENS0_16GarbageCollectorENS_15GCCallbackFlagsE, @function
_ZN2v88internal13GlobalHandles31PostGarbageCollectionProcessingENS0_16GarbageCollectorENS_15GCCallbackFlagsE:
.LFB18902:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	164(%rdi), %eax
	leal	1(%rax), %ebx
	movl	$1, %eax
	movl	%ebx, 164(%rdi)
	movq	(%rdi), %rdi
	cmpl	$4, 37984(%rdi)
	je	.L1127
	andl	$28, %edx
	setne	%al
.L1127:
	movq	136(%r12), %rcx
	cmpq	%rcx, 144(%r12)
	je	.L1136
	cmpb	$0, _ZN2v88internal22FLAG_optimize_for_sizeE(%rip)
	jne	.L1131
	cmpb	$0, _ZN2v88internal16FLAG_predictableE(%rip)
	jne	.L1131
	testb	%al, %al
	je	.L1132
.L1131:
	addq	$37592, %rdi
	xorl	%edx, %edx
	movl	$8, %esi
	call	_ZN2v88internal4Heap23CallGCPrologueCallbacksENS_6GCTypeENS_15GCCallbackFlagsE@PLT
	cmpb	$0, 161(%r12)
	je	.L1169
.L1134:
	movq	(%r12), %rax
	xorl	%edx, %edx
	movl	$8, %esi
	leaq	37592(%rax), %rdi
	call	_ZN2v88internal4Heap23CallGCEpilogueCallbacksENS_6GCTypeENS_15GCCallbackFlagsE@PLT
.L1135:
	xorl	%eax, %eax
	cmpl	164(%r12), %ebx
	je	.L1136
.L1126:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1170
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1132:
	.cfi_restore_state
	cmpb	$0, 160(%r12)
	je	.L1171
	.p2align 4,,10
	.p2align 3
.L1136:
	andl	$-3, %r13d
	je	.L1129
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal13GlobalHandles23PostMarkSweepProcessingEj
.L1151:
	cmpl	164(%r12), %ebx
	jne	.L1126
	leaq	16(%r12), %rsi
	movq	%r12, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal13GlobalHandles31UpdateAndCompactListOfYoungNodeINS1_4NodeEEEvPSt6vectorIPT_SaIS6_EE
	leaq	48(%r12), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal13GlobalHandles31UpdateAndCompactListOfYoungNodeINS1_10TracedNodeEEEvPSt6vectorIPT_SaIS6_EE
	movq	-136(%rbp), %rax
	jmp	.L1126
	.p2align 4,,10
	.p2align 3
.L1169:
	movq	%r12, %rdi
	call	_ZN2v88internal13GlobalHandles32InvokeSecondPassPhantomCallbacksEv.part.0
	jmp	.L1134
	.p2align 4,,10
	.p2align 3
.L1129:
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal13GlobalHandles22PostScavengeProcessingEj
	jmp	.L1151
	.p2align 4,,10
	.p2align 3
.L1171:
	movb	$1, 160(%r12)
	leaq	-96(%rbp), %r14
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	(%r12), %rdx
	leaq	-112(%rbp), %rdi
	movq	%rax, %rsi
	movq	(%rax), %rax
	call	*48(%rax)
	movq	-112(%rbp), %r15
	movq	(%r12), %rsi
	leaq	-120(%rbp), %rdi
	movq	%r14, %rdx
	movq	(%r15), %rax
	movq	(%rax), %rcx
	leaq	_ZNSt17_Function_handlerIFvvEZN2v88internal13GlobalHandles42InvokeOrScheduleSecondPassPhantomCallbacksEbEUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%r12, -96(%rbp)
	movq	%rax, %xmm1
	movq	%rcx, -136(%rbp)
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal13GlobalHandles42InvokeOrScheduleSecondPassPhantomCallbacksEbEUlvE_E10_M_managerERSt9_Any_dataRKS6_St18_Manager_operation(%rip), %rcx
	movq	%rcx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal18MakeCancelableTaskEPNS0_7IsolateESt8functionIFvvEE@PLT
	movq	-120(%rbp), %rax
	movq	%r15, %rdi
	leaq	-128(%rbp), %rsi
	movq	$0, -120(%rbp)
	movq	-136(%rbp), %rcx
	leaq	32(%rax), %rdx
	testq	%rax, %rax
	cmovne	%rdx, %rax
	movq	%rax, -128(%rbp)
	call	*%rcx
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1138
	movq	(%rdi), %rax
	call	*8(%rax)
.L1138:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1139
	movq	(%rdi), %rax
	call	*8(%rax)
.L1139:
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L1140
	movl	$3, %edx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	*%rax
.L1140:
	movq	-104(%rbp), %r15
	testq	%r15, %r15
	je	.L1135
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r14
	testq	%r14, %r14
	je	.L1143
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r15)
.L1144:
	cmpl	$1, %eax
	jne	.L1135
	movq	(%r15), %rax
	leaq	_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv(%rip), %rdx
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1172
.L1146:
	testq	%r14, %r14
	je	.L1147
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r15)
.L1148:
	cmpl	$1, %eax
	jne	.L1135
	movq	(%r15), %rdx
	leaq	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv(%rip), %rcx
	movq	%r15, %rdi
	movq	24(%rdx), %rax
	cmpq	%rcx, %rax
	jne	.L1149
	call	*8(%rdx)
	jmp	.L1135
.L1143:
	movl	8(%r15), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r15)
	jmp	.L1144
.L1147:
	movl	12(%r15), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r15)
	jmp	.L1148
.L1172:
	movq	%r15, %rdi
	call	*%rax
	jmp	.L1146
.L1149:
	call	*%rax
	jmp	.L1135
.L1170:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18902:
	.size	_ZN2v88internal13GlobalHandles31PostGarbageCollectionProcessingENS0_16GarbageCollectorENS_15GCCallbackFlagsE, .-_ZN2v88internal13GlobalHandles31PostGarbageCollectionProcessingENS0_16GarbageCollectorENS_15GCCallbackFlagsE
	.section	.text._ZNSt6vectorIN2v88internal13GlobalHandles22PendingPhantomCallbackESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal13GlobalHandles22PendingPhantomCallbackESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal13GlobalHandles22PendingPhantomCallbackESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal13GlobalHandles22PendingPhantomCallbackESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, @function
_ZNSt6vectorIN2v88internal13GlobalHandles22PendingPhantomCallbackESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_:
.LFB23290:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rcx
	movq	(%rdi), %r14
	movabsq	$288230376151711743, %rdi
	movq	%rcx, %rax
	subq	%r14, %rax
	sarq	$5, %rax
	cmpq	%rdi, %rax
	je	.L1190
	movq	%rsi, %r12
	subq	%r14, %rsi
	testq	%rax, %rax
	je	.L1182
	movabsq	$9223372036854775776, %r13
	leaq	(%rax,%rax), %r8
	cmpq	%r8, %rax
	jbe	.L1191
.L1175:
	movq	%r13, %rdi
	movq	%rdx, -80(%rbp)
	movq	%rsi, -72(%rbp)
	movq	%rcx, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %rsi
	movq	%rax, %rbx
	leaq	(%rax,%r13), %rax
	movq	-80(%rbp), %rdx
	movq	%rax, -56(%rbp)
	leaq	32(%rbx), %r13
.L1181:
	movdqu	(%rdx), %xmm3
	movdqu	16(%rdx), %xmm4
	movups	%xmm3, (%rbx,%rsi)
	movups	%xmm4, 16(%rbx,%rsi)
	cmpq	%r14, %r12
	je	.L1177
	movq	%rbx, %rdx
	movq	%r14, %rax
	.p2align 4,,10
	.p2align 3
.L1178:
	movdqu	(%rax), %xmm1
	addq	$32, %rax
	addq	$32, %rdx
	movups	%xmm1, -32(%rdx)
	movdqu	-16(%rax), %xmm2
	movups	%xmm2, -16(%rdx)
	cmpq	%rax, %r12
	jne	.L1178
	movq	%r12, %rax
	subq	%r14, %rax
	leaq	32(%rbx,%rax), %r13
.L1177:
	cmpq	%rcx, %r12
	je	.L1179
	subq	%r12, %rcx
	movq	%r13, %rdi
	movq	%r12, %rsi
	movq	%rcx, %rdx
	movq	%rcx, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %rcx
	addq	%rcx, %r13
.L1179:
	testq	%r14, %r14
	je	.L1180
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1180:
	movq	-56(%rbp), %rax
	movq	%rbx, %xmm0
	movq	%r13, %xmm5
	punpcklqdq	%xmm5, %xmm0
	movq	%rax, 16(%r15)
	movups	%xmm0, (%r15)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1191:
	.cfi_restore_state
	testq	%r8, %r8
	jne	.L1176
	movq	$0, -56(%rbp)
	movl	$32, %r13d
	xorl	%ebx, %ebx
	jmp	.L1181
	.p2align 4,,10
	.p2align 3
.L1182:
	movl	$32, %r13d
	jmp	.L1175
.L1176:
	cmpq	%rdi, %r8
	cmovbe	%r8, %rdi
	movq	%rdi, %r13
	salq	$5, %r13
	jmp	.L1175
.L1190:
	leaq	.LC7(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE23290:
	.size	_ZNSt6vectorIN2v88internal13GlobalHandles22PendingPhantomCallbackESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, .-_ZNSt6vectorIN2v88internal13GlobalHandles22PendingPhantomCallbackESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.section	.rodata._ZN2v88internal13GlobalHandles28InvokeFirstPassWeakCallbacksEv.str1.8,"aMS",@progbits,1
	.align 8
.LC11:
	.string	"Handle not reset in first callback. See comments on |v8::WeakCallbackInfo|."
	.section	.text._ZN2v88internal13GlobalHandles28InvokeFirstPassWeakCallbacksEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13GlobalHandles28InvokeFirstPassWeakCallbacksEv
	.type	_ZN2v88internal13GlobalHandles28InvokeFirstPassWeakCallbacksEv, @function
_ZN2v88internal13GlobalHandles28InvokeFirstPassWeakCallbacksEv:
.LFB18889:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	88(%rdi), %r14
	movq	96(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, 104(%rdi)
	movups	%xmm0, 88(%rdi)
	cmpq	%r13, %r14
	je	.L1206
	movq	$0, -104(%rbp)
	leaq	136(%rdi), %rax
	leaq	8(%r14), %r15
	movq	%rax, -120(%rbp)
	leaq	-96(%rbp), %rbx
	jmp	.L1197
	.p2align 4,,10
	.p2align 3
.L1194:
	cmpq	$0, (%r15)
	je	.L1195
	movq	144(%r12), %rsi
	cmpq	152(%r12), %rsi
	je	.L1196
	movdqu	(%r15), %xmm1
	movups	%xmm1, (%rsi)
	movdqu	16(%r15), %xmm2
	movups	%xmm2, 16(%rsi)
	addq	$32, 144(%r12)
.L1195:
	leaq	40(%r15), %rdx
	addq	$32, %r15
	addq	$1, -104(%rbp)
	cmpq	%r15, %r13
	je	.L1193
	movq	%rdx, %r15
.L1197:
	movq	8(%r15), %rcx
	movq	(%r12), %rsi
	movq	%rbx, %rdi
	movq	-8(%r15), %rdx
	movq	%r15, -80(%rbp)
	movq	%rsi, -96(%rbp)
	movq	%rcx, -88(%rbp)
	movq	16(%r15), %rcx
	movq	%rdx, -112(%rbp)
	movq	%rcx, -72(%rbp)
	movq	24(%r15), %rcx
	movq	%rcx, -64(%rbp)
	movq	(%r15), %rcx
	movq	$0, (%r15)
	call	*%rcx
	movq	-112(%rbp), %rdx
	testb	$7, 11(%rdx)
	je	.L1194
.L1200:
	leaq	.LC11(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1196:
	movq	-120(%rbp), %rdi
	movq	%r15, %rdx
	call	_ZNSt6vectorIN2v88internal13GlobalHandles22PendingPhantomCallbackESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L1195
	.p2align 4,,10
	.p2align 3
.L1206:
	movq	$0, -104(%rbp)
	.p2align 4,,10
	.p2align 3
.L1193:
	testq	%r14, %r14
	je	.L1198
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1198:
	movq	112(%r12), %rax
	pxor	%xmm0, %xmm0
	movq	120(%r12), %rbx
	movq	$0, 128(%r12)
	movups	%xmm0, 112(%r12)
	movq	%rax, -128(%rbp)
	movq	%rbx, -112(%rbp)
	cmpq	%rbx, %rax
	je	.L1199
	leaq	8(%rax), %r15
	leaq	136(%r12), %rax
	xorl	%r13d, %r13d
	movq	%rax, -120(%rbp)
	leaq	-96(%rbp), %rbx
	jmp	.L1203
	.p2align 4,,10
	.p2align 3
.L1217:
	cmpq	$0, (%r15)
	je	.L1201
	movq	144(%r12), %rsi
	cmpq	152(%r12), %rsi
	je	.L1202
	movdqu	(%r15), %xmm3
	movups	%xmm3, (%rsi)
	movdqu	16(%r15), %xmm4
	movups	%xmm4, 16(%rsi)
	addq	$32, 144(%r12)
.L1201:
	addq	$1, %r13
	leaq	40(%r15), %rcx
	leaq	32(%r15), %rdx
	cmpq	%rdx, -112(%rbp)
	je	.L1216
	movq	%rcx, %r15
.L1203:
	movq	8(%r15), %rsi
	movq	(%r12), %rdi
	movq	-8(%r15), %r14
	movq	%r15, -80(%rbp)
	movq	%rdi, -96(%rbp)
	movq	%rbx, %rdi
	movq	%rsi, -88(%rbp)
	movq	16(%r15), %rsi
	movq	%rsi, -72(%rbp)
	movq	24(%r15), %rsi
	movq	%rsi, -64(%rbp)
	movq	(%r15), %rsi
	movq	$0, (%r15)
	call	*%rsi
	testb	$3, 11(%r14)
	je	.L1217
	jmp	.L1200
	.p2align 4,,10
	.p2align 3
.L1202:
	movq	-120(%rbp), %rdi
	movq	%r15, %rdx
	call	_ZNSt6vectorIN2v88internal13GlobalHandles22PendingPhantomCallbackESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L1201
	.p2align 4,,10
	.p2align 3
.L1216:
	addq	%r13, -104(%rbp)
.L1199:
	movq	-128(%rbp), %rax
	testq	%rax, %rax
	je	.L1192
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L1192:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1218
	movq	-104(%rbp), %rax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1218:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18889:
	.size	_ZN2v88internal13GlobalHandles28InvokeFirstPassWeakCallbacksEv, .-_ZN2v88internal13GlobalHandles28InvokeFirstPassWeakCallbacksEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal13GlobalHandlesC2EPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal13GlobalHandlesC2EPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal13GlobalHandlesC2EPNS0_7IsolateE:
.LFB24057:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE24057:
	.size	_GLOBAL__sub_I__ZN2v88internal13GlobalHandlesC2EPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal13GlobalHandlesC2EPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal13GlobalHandlesC2EPNS0_7IsolateE
	.section	.bss._ZZN2v88internal13GlobalHandles40InvokeSecondPassPhantomCallbacksFromTaskEvE29trace_event_unique_atomic1050,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal13GlobalHandles40InvokeSecondPassPhantomCallbacksFromTaskEvE29trace_event_unique_atomic1050, @object
	.size	_ZZN2v88internal13GlobalHandles40InvokeSecondPassPhantomCallbacksFromTaskEvE29trace_event_unique_atomic1050, 8
_ZZN2v88internal13GlobalHandles40InvokeSecondPassPhantomCallbacksFromTaskEvE29trace_event_unique_atomic1050:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
