	.file	"serializer-common.cc"
	.text
	.section	.text._ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE,"axG",@progbits,_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE
	.type	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE, @function
_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE:
.LFB6263:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	leaq	8(%rcx), %r8
	movq	16(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE6263:
	.size	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE, .-_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE
	.section	.text._ZN2v88internal24ExternalReferenceEncoderD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24ExternalReferenceEncoderD2Ev
	.type	_ZN2v88internal24ExternalReferenceEncoderD2Ev, @function
_ZN2v88internal24ExternalReferenceEncoderD2Ev:
.LFB17894:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE17894:
	.size	_ZN2v88internal24ExternalReferenceEncoderD2Ev, .-_ZN2v88internal24ExternalReferenceEncoderD2Ev
	.globl	_ZN2v88internal24ExternalReferenceEncoderD1Ev
	.set	_ZN2v88internal24ExternalReferenceEncoderD1Ev,_ZN2v88internal24ExternalReferenceEncoderD2Ev
	.section	.text._ZN2v88internal24ExternalReferenceEncoder9TryEncodeEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24ExternalReferenceEncoder9TryEncodeEm
	.type	_ZN2v88internal24ExternalReferenceEncoder9TryEncodeEm, @function
_ZN2v88internal24ExternalReferenceEncoder9TryEncodeEm:
.LFB17896:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movl	8(%rax), %edx
	movq	(%rax), %rdi
	subl	$1, %edx
	movl	%edx, %eax
	andl	%esi, %eax
	leaq	(%rax,%rax,2), %rcx
	leaq	(%rdi,%rcx,8), %rcx
	cmpb	$0, 16(%rcx)
	jne	.L7
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L11:
	addq	$1, %rax
	andq	%rdx, %rax
	leaq	(%rax,%rax,2), %rcx
	leaq	(%rdi,%rcx,8), %rcx
	cmpb	$0, 16(%rcx)
	je	.L8
.L7:
	cmpq	(%rcx), %rsi
	jne	.L11
	movl	8(%rcx), %eax
	movl	$1, %edx
	salq	$32, %rax
	movb	%dl, %al
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	xorl	%eax, %eax
	xorl	%edx, %edx
	salq	$32, %rax
	movb	%dl, %al
	ret
	.cfi_endproc
.LFE17896:
	.size	_ZN2v88internal24ExternalReferenceEncoder9TryEncodeEm, .-_ZN2v88internal24ExternalReferenceEncoder9TryEncodeEm
	.section	.rodata._ZN2v88internal24ExternalReferenceEncoder6EncodeEm.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"Unknown external reference %p.\n"
	.section	.rodata._ZN2v88internal24ExternalReferenceEncoder6EncodeEm.str1.1,"aMS",@progbits,1
.LC1:
	.string	"%s"
	.section	.text._ZN2v88internal24ExternalReferenceEncoder6EncodeEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24ExternalReferenceEncoder6EncodeEm
	.type	_ZN2v88internal24ExternalReferenceEncoder6EncodeEm, @function
_ZN2v88internal24ExternalReferenceEncoder6EncodeEm:
.LFB17897:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$8, %rsp
	movq	(%rdi), %rax
	movl	8(%rax), %edx
	movq	(%rax), %rsi
	subl	$1, %edx
	movl	%edx, %eax
	andl	%r12d, %eax
	leaq	(%rax,%rax,2), %rcx
	leaq	(%rsi,%rcx,8), %rcx
	cmpb	$0, 16(%rcx)
	jne	.L15
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L18:
	addq	$1, %rax
	andq	%rdx, %rax
	leaq	(%rax,%rax,2), %rcx
	leaq	(%rsi,%rcx,8), %rcx
	cmpb	$0, 16(%rcx)
	je	.L13
.L15:
	cmpq	(%rcx), %r12
	jne	.L18
	movl	8(%rcx), %eax
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	.cfi_restore_state
	movq	%r12, %rsi
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v84base2OS10PrintErrorEPKcz@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal22ExternalReferenceTable13ResolveSymbolEPv@PLT
	leaq	.LC1(%rip), %rdi
	movq	%rax, %rsi
	xorl	%eax, %eax
	call	_ZN2v84base2OS10PrintErrorEPKcz@PLT
	call	_ZN2v84base2OS5AbortEv@PLT
	.cfi_endproc
.LFE17897:
	.size	_ZN2v88internal24ExternalReferenceEncoder6EncodeEm, .-_ZN2v88internal24ExternalReferenceEncoder6EncodeEm
	.section	.rodata._ZNK2v88internal24ExternalReferenceEncoder13NameOfAddressEPNS0_7IsolateEm.str1.1,"aMS",@progbits,1
.LC2:
	.string	"<unknown>"
.LC3:
	.string	"<from api>"
	.section	.text._ZNK2v88internal24ExternalReferenceEncoder13NameOfAddressEPNS0_7IsolateEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal24ExternalReferenceEncoder13NameOfAddressEPNS0_7IsolateEm
	.type	_ZNK2v88internal24ExternalReferenceEncoder13NameOfAddressEPNS0_7IsolateEm, @function
_ZNK2v88internal24ExternalReferenceEncoder13NameOfAddressEPNS0_7IsolateEm:
.LFB17898:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	leaq	.LC2(%rip), %r8
	movl	8(%rax), %ecx
	movq	(%rax), %rdi
	subl	$1, %ecx
	movl	%ecx, %eax
	andl	%edx, %eax
	leaq	(%rax,%rax,2), %rsi
	leaq	(%rdi,%rsi,8), %rsi
	cmpb	$0, 16(%rsi)
	jne	.L22
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L29:
	addq	$1, %rax
	andq	%rcx, %rax
	leaq	(%rax,%rax,2), %rsi
	leaq	(%rdi,%rsi,8), %rsi
	cmpb	$0, 16(%rsi)
	je	.L28
.L22:
	cmpq	(%rsi), %rdx
	jne	.L29
	movl	8(%rsi), %eax
	leaq	.LC3(%rip), %r8
	testl	%eax, %eax
	js	.L19
	leaq	_ZN2v88internal22ExternalReferenceTable9ref_name_E(%rip), %rdx
	movq	(%rdx,%rax,8), %r8
.L19:
	movq	%r8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	leaq	.LC2(%rip), %r8
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE17898:
	.size	_ZNK2v88internal24ExternalReferenceEncoder13NameOfAddressEPNS0_7IsolateEm, .-_ZNK2v88internal24ExternalReferenceEncoder13NameOfAddressEPNS0_7IsolateEm
	.section	.rodata._ZN2v88internal14SerializedData12AllocateDataEj.str1.1,"aMS",@progbits,1
.LC4:
	.string	"NewArray"
	.section	.text._ZN2v88internal14SerializedData12AllocateDataEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14SerializedData12AllocateDataEj
	.type	_ZN2v88internal14SerializedData12AllocateDataEj, @function
_ZN2v88internal14SerializedData12AllocateDataEj:
.LFB17899:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	leaq	_ZSt7nothrow(%rip), %rsi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%r13, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$8, %rsp
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	je	.L33
.L31:
	movl	%r12d, 16(%rbx)
	movq	%rax, 8(%rbx)
	movb	$1, 20(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L33:
	.cfi_restore_state
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%r13, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	jne	.L31
	leaq	.LC4(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
	.cfi_endproc
.LFE17899:
	.size	_ZN2v88internal14SerializedData12AllocateDataEj, .-_ZN2v88internal14SerializedData12AllocateDataEj
	.section	.rodata._ZN2v88internal22SerializerDeserializer7IterateEPNS0_7IsolateEPNS0_11RootVisitorE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"vector::_M_realloc_insert"
	.section	.rodata._ZN2v88internal22SerializerDeserializer7IterateEPNS0_7IsolateEPNS0_11RootVisitorE.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"vector::_M_range_check: __n (which is %zu) >= this->size() (which is %zu)"
	.section	.text._ZN2v88internal22SerializerDeserializer7IterateEPNS0_7IsolateEPNS0_11RootVisitorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22SerializerDeserializer7IterateEPNS0_7IsolateEPNS0_11RootVisitorE
	.type	_ZN2v88internal22SerializerDeserializer7IterateEPNS0_7IsolateEPNS0_11RootVisitorE, @function
_ZN2v88internal22SerializerDeserializer7IterateEPNS0_7IsolateEPNS0_11RootVisitorE:
.LFB17900:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	45496(%rdi), %r12
	movq	45504(%rdi), %r15
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L35:
	movq	(%r9), %rax
	movq	24(%rax), %r8
.L49:
	leaq	0(,%r13,8), %rbx
	leaq	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE(%rip), %rdi
	leaq	(%r12,%rbx), %rcx
	cmpq	%rdi, %r8
	jne	.L50
	movq	%r9, -56(%rbp)
	movq	%r9, %rdi
	leaq	8(%rcx), %r8
	xorl	%edx, %edx
	movl	$19, %esi
	call	*16(%rax)
	movq	-56(%rbp), %r9
.L51:
	movq	45504(%r14), %r15
	movq	45496(%r14), %r12
	movq	%r15, %rdx
	subq	%r12, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %r13
	jnb	.L71
	movq	(%r12,%rbx), %rax
	cmpq	%rax, 88(%r14)
	je	.L34
	addq	$1, %r13
.L54:
	movq	%r15, %rcx
	subq	%r12, %rcx
	movq	%rcx, %rax
	sarq	$3, %rax
	cmpq	%rax, %r13
	jb	.L35
	cmpq	%r15, 45512(%r14)
	je	.L36
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %rax
	movq	%rax, (%r15)
	movq	45504(%r14), %rax
	movq	45496(%r14), %r12
	addq	$8, %rax
	movq	%rax, 45504(%r14)
	subq	%r12, %rax
	sarq	$3, %rax
	movq	%rax, %rbx
.L37:
	movq	(%r9), %rax
	movq	24(%rax), %r8
	cmpq	%rbx, %r13
	jb	.L49
	movq	%rbx, %rdx
	movq	%r13, %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L50:
	movq	%r9, -56(%rbp)
	movq	%r9, %rdi
	xorl	%edx, %edx
	movl	$19, %esi
	call	*%r8
	movq	-56(%rbp), %r9
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L36:
	movabsq	$1152921504606846975, %rsi
	cmpq	%rsi, %rax
	je	.L72
	testq	%rax, %rax
	movl	$1, %edx
	cmovne	%rax, %rdx
	addq	%rdx, %rax
	jc	.L40
	testq	%rax, %rax
	jne	.L73
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %rdi
	xorl	%eax, %eax
	movl	$8, %esi
	xorl	%edx, %edx
	movq	%rdi, (%rax,%rcx)
	cmpq	%r15, %r12
	je	.L57
.L74:
	leaq	-8(%r15), %rbx
	leaq	15(%rax), %rcx
	subq	%r12, %rbx
	subq	%r12, %rcx
	movq	%rbx, %rdi
	shrq	$3, %rdi
	cmpq	$30, %rcx
	jbe	.L58
	movabsq	$2305843009213693948, %rsi
	testq	%rsi, %rdi
	je	.L58
	addq	$1, %rdi
	xorl	%ecx, %ecx
	movq	%rdi, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L45:
	movdqu	(%r12,%rcx), %xmm1
	movups	%xmm1, (%rax,%rcx)
	addq	$16, %rcx
	cmpq	%rcx, %rsi
	jne	.L45
	movq	%rdi, %r8
	andq	$-2, %r8
	leaq	0(,%r8,8), %rsi
	leaq	(%r12,%rsi), %rcx
	addq	%rax, %rsi
	cmpq	%rdi, %r8
	je	.L47
	movq	(%rcx), %rcx
	movq	%rcx, (%rsi)
.L47:
	addq	$16, %rbx
	leaq	(%rax,%rbx), %rsi
	sarq	$3, %rbx
.L43:
	testq	%r12, %r12
	je	.L48
	movq	%r12, %rdi
	movq	%r9, -80(%rbp)
	movq	%rsi, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-80(%rbp), %r9
	movq	-72(%rbp), %rsi
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rax
.L48:
	movq	%rax, %xmm0
	movq	%rsi, %xmm2
	movq	%rdx, 45512(%r14)
	movq	%rax, %r12
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 45496(%r14)
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L34:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	.cfi_restore_state
	movabsq	$1152921504606846975, %rsi
	cmpq	%rsi, %rax
	cmova	%rsi, %rax
	leaq	0(,%rax,8), %rbx
.L41:
	movq	%rbx, %rdi
	movq	%r9, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rcx
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %rdi
	movq	-64(%rbp), %r9
	leaq	(%rax,%rbx), %rdx
	leaq	8(%rax), %rsi
	movq	%rdi, (%rax,%rcx)
	cmpq	%r15, %r12
	jne	.L74
.L57:
	movl	$1, %ebx
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L58:
	movq	%rax, %rsi
	movq	%r12, %rcx
	.p2align 4,,10
	.p2align 3
.L44:
	movq	(%rcx), %rdi
	addq	$8, %rcx
	addq	$8, %rsi
	movq	%rdi, -8(%rsi)
	cmpq	%r15, %rcx
	jne	.L44
	jmp	.L47
.L71:
	movq	%r13, %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L40:
	movabsq	$9223372036854775800, %rbx
	jmp	.L41
.L72:
	leaq	.LC5(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE17900:
	.size	_ZN2v88internal22SerializerDeserializer7IterateEPNS0_7IsolateEPNS0_11RootVisitorE, .-_ZN2v88internal22SerializerDeserializer7IterateEPNS0_7IsolateEPNS0_11RootVisitorE
	.section	.text._ZN2v88internal22SerializerDeserializer13CanBeDeferredENS0_10HeapObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22SerializerDeserializer13CanBeDeferredENS0_10HeapObjectE
	.type	_ZN2v88internal22SerializerDeserializer13CanBeDeferredENS0_10HeapObjectE, @function
_ZN2v88internal22SerializerDeserializer13CanBeDeferredENS0_10HeapObjectE:
.LFB17901:
	.cfi_startproc
	endbr64
	movq	-1(%rdi), %rax
	cmpw	$63, 11(%rax)
	ja	.L76
.L78:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	movq	-1(%rdi), %rax
	cmpw	$96, 11(%rax)
	je	.L78
	movq	-1(%rdi), %rax
	cmpw	$1086, 11(%rax)
	setne	%al
	ret
	.cfi_endproc
.LFE17901:
	.size	_ZN2v88internal22SerializerDeserializer13CanBeDeferredENS0_10HeapObjectE, .-_ZN2v88internal22SerializerDeserializer13CanBeDeferredENS0_10HeapObjectE
	.section	.text._ZN2v88internal22SerializerDeserializer35RestoreExternalReferenceRedirectorsERKSt6vectorINS0_12AccessorInfoESaIS3_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22SerializerDeserializer35RestoreExternalReferenceRedirectorsERKSt6vectorINS0_12AccessorInfoESaIS3_EE
	.type	_ZN2v88internal22SerializerDeserializer35RestoreExternalReferenceRedirectorsERKSt6vectorINS0_12AccessorInfoESaIS3_EE, @function
_ZN2v88internal22SerializerDeserializer35RestoreExternalReferenceRedirectorsERKSt6vectorINS0_12AccessorInfoESaIS3_EE:
.LFB17902:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rsi), %rbx
	movq	8(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	%r13, %rbx
	je	.L79
	leaq	-48(%rbp), %r14
	.p2align 4,,10
	.p2align 3
.L81:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	addq	$8, %rbx
	movq	%rax, -48(%rbp)
	movq	47(%rax), %r12
	call	_ZNK2v88internal12AccessorInfo17redirected_getterEv@PLT
	movq	%rax, 7(%r12)
	cmpq	%rbx, %r13
	jne	.L81
.L79:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L85
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L85:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17902:
	.size	_ZN2v88internal22SerializerDeserializer35RestoreExternalReferenceRedirectorsERKSt6vectorINS0_12AccessorInfoESaIS3_EE, .-_ZN2v88internal22SerializerDeserializer35RestoreExternalReferenceRedirectorsERKSt6vectorINS0_12AccessorInfoESaIS3_EE
	.section	.text._ZN2v88internal22SerializerDeserializer35RestoreExternalReferenceRedirectorsERKSt6vectorINS0_15CallHandlerInfoESaIS3_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22SerializerDeserializer35RestoreExternalReferenceRedirectorsERKSt6vectorINS0_15CallHandlerInfoESaIS3_EE
	.type	_ZN2v88internal22SerializerDeserializer35RestoreExternalReferenceRedirectorsERKSt6vectorINS0_15CallHandlerInfoESaIS3_EE, @function
_ZN2v88internal22SerializerDeserializer35RestoreExternalReferenceRedirectorsERKSt6vectorINS0_15CallHandlerInfoESaIS3_EE:
.LFB17903:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rsi), %rbx
	movq	8(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	%r13, %rbx
	je	.L86
	leaq	-48(%rbp), %r14
	.p2align 4,,10
	.p2align 3
.L88:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	addq	$8, %rbx
	movq	%rax, -48(%rbp)
	movq	15(%rax), %r12
	call	_ZNK2v88internal15CallHandlerInfo19redirected_callbackEv@PLT
	movq	%rax, 7(%r12)
	cmpq	%rbx, %r13
	jne	.L88
.L86:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L92
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L92:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17903:
	.size	_ZN2v88internal22SerializerDeserializer35RestoreExternalReferenceRedirectorsERKSt6vectorINS0_15CallHandlerInfoESaIS3_EE, .-_ZN2v88internal22SerializerDeserializer35RestoreExternalReferenceRedirectorsERKSt6vectorINS0_15CallHandlerInfoESaIS3_EE
	.section	.rodata._ZN2v84base19TemplateHashMapImplImjNS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE6ResizeES4_.str1.8,"aMS",@progbits,1
	.align 8
.LC7:
	.string	"Out of memory: HashMap::Initialize"
	.section	.text._ZN2v84base19TemplateHashMapImplImjNS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE6ResizeES4_,"axG",@progbits,_ZN2v84base19TemplateHashMapImplImjNS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE6ResizeES4_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base19TemplateHashMapImplImjNS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE6ResizeES4_
	.type	_ZN2v84base19TemplateHashMapImplImjNS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE6ResizeES4_, @function
_ZN2v84base19TemplateHashMapImplImjNS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE6ResizeES4_:
.LFB21596:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	8(%rdi), %eax
	movq	(%rdi), %r13
	movl	12(%rdi), %r15d
	addl	%eax, %eax
	leaq	(%rax,%rax,2), %rdi
	movq	%rax, %rbx
	salq	$3, %rdi
	call	malloc@PLT
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L118
	movl	%ebx, 8(%r12)
	testl	%ebx, %ebx
	je	.L95
	movb	$0, 16(%rax)
	movl	$24, %edx
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L96:
	movq	(%r12), %rcx
	addq	$1, %rax
	movb	$0, 16(%rcx,%rdx)
	movl	8(%r12), %ecx
	addq	$24, %rdx
	cmpq	%rax, %rcx
	ja	.L96
.L95:
	movl	$0, 12(%r12)
	movq	%r13, %r14
	testl	%r15d, %r15d
	je	.L102
.L97:
	cmpb	$0, 16(%r14)
	jne	.L119
.L98:
	addq	$24, %r14
	cmpb	$0, 16(%r14)
	je	.L98
.L119:
	movl	8(%r12), %eax
	movl	12(%r14), %ebx
	movq	(%r12), %rdi
	movq	(%r14), %rsi
	leal	-1(%rax), %ecx
	movl	%ebx, %eax
	andl	%ecx, %eax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rdi,%rdx,8), %rdx
	cmpb	$0, 16(%rdx)
	jne	.L100
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L120:
	addq	$1, %rax
	andq	%rcx, %rax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rdi,%rdx,8), %rdx
	cmpb	$0, 16(%rdx)
	je	.L99
.L100:
	cmpq	%rsi, (%rdx)
	jne	.L120
.L99:
	movl	8(%r14), %eax
	movq	%rsi, (%rdx)
	movl	%ebx, 12(%rdx)
	movl	%eax, 8(%rdx)
	movb	$1, 16(%rdx)
	movl	12(%r12), %eax
	addl	$1, %eax
	movl	%eax, %edx
	movl	%eax, 12(%r12)
	shrl	$2, %edx
	addl	%edx, %eax
	cmpl	8(%r12), %eax
	jnb	.L101
.L104:
	addq	$24, %r14
	subl	$1, %r15d
	jne	.L97
.L102:
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	free@PLT
	.p2align 4,,10
	.p2align 3
.L101:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v84base19TemplateHashMapImplImjNS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE6ResizeES4_
	movl	8(%r12), %eax
	movq	(%r12), %rsi
	leal	-1(%rax), %ecx
	movl	%ebx, %eax
	andl	%ecx, %eax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rsi,%rdx,8), %rdx
	cmpb	$0, 16(%rdx)
	je	.L104
	movq	(%r14), %rdi
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L121:
	addq	$1, %rax
	andq	%rcx, %rax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rsi,%rdx,8), %rdx
	cmpb	$0, 16(%rdx)
	je	.L104
.L105:
	cmpq	%rdi, (%rdx)
	jne	.L121
	jmp	.L104
.L118:
	leaq	.LC7(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21596:
	.size	_ZN2v84base19TemplateHashMapImplImjNS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE6ResizeES4_, .-_ZN2v84base19TemplateHashMapImplImjNS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE6ResizeES4_
	.section	.text._ZN2v88internal24ExternalReferenceEncoderC2EPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24ExternalReferenceEncoderC2EPNS0_7IsolateE
	.type	_ZN2v88internal24ExternalReferenceEncoderC2EPNS0_7IsolateE, @function
_ZN2v88internal24ExternalReferenceEncoderC2EPNS0_7IsolateE:
.LFB17891:
	.cfi_startproc
	endbr64
	movq	41736(%rsi), %rax
	movq	%rax, (%rdi)
	testq	%rax, %rax
	je	.L172
	ret
.L172:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movl	$24, %edi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	call	_Znwm@PLT
	movl	$192, %edi
	movq	%rax, %r13
	call	malloc@PLT
	movq	%rax, 0(%r13)
	testq	%rax, %rax
	je	.L173
	movl	$8, 8(%r13)
	movl	$24, %edx
	movb	$0, 16(%rax)
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L125:
	movq	0(%r13), %rcx
	addq	$1, %rax
	movb	$0, 16(%rcx,%rdx)
	movl	8(%r13), %esi
	addq	$24, %rdx
	movq	%rsi, %rcx
	cmpq	%rax, %rsi
	ja	.L125
	movl	$0, 12(%r13)
	xorl	%r12d, %r12d
	movq	%r13, (%r14)
	movq	%r13, 41736(%r15)
	.p2align 4,,10
	.p2align 3
.L135:
	subl	$1, %ecx
	movq	4856(%r15,%r12,8), %rbx
	movq	0(%r13), %r8
	movl	%r12d, %esi
	movl	%ecx, %eax
	andl	%ebx, %eax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%r8,%rdx,8), %rdx
	cmpb	$0, 16(%rdx)
	je	.L132
	movq	%rdx, %r9
	movq	%rax, %rdi
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L174:
	addq	$1, %rdi
	andq	%rcx, %rdi
	leaq	(%rdi,%rdi,2), %r9
	leaq	(%r8,%r9,8), %r9
	cmpb	$0, 16(%r9)
	je	.L131
.L128:
	cmpq	(%r9), %rbx
	jne	.L174
	addq	$1, %r12
	cmpq	$948, %r12
	je	.L134
.L176:
	movq	(%r14), %r13
	movl	8(%r13), %ecx
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L175:
	addq	$1, %rax
	andq	%rcx, %rax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%r8,%rdx,8), %rdx
	cmpb	$0, 16(%rdx)
	je	.L132
.L131:
	cmpq	(%rdx), %rbx
	jne	.L175
.L130:
	movl	%esi, 8(%rdx)
.L180:
	addq	$1, %r12
	cmpq	$948, %r12
	jne	.L176
.L134:
	movq	41728(%r15), %r15
	testq	%r15, %r15
	je	.L122
	movq	(%r15), %rbx
	testq	%rbx, %rbx
	je	.L122
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L145:
	movq	(%r14), %r9
	movl	%ebx, %r12d
	movl	8(%r9), %eax
	movq	(%r9), %r8
	leal	-1(%rax), %esi
	movl	%esi, %ecx
	andl	%ebx, %ecx
	leaq	(%rcx,%rcx,2), %rax
	leaq	(%r8,%rax,8), %rdi
	cmpb	$0, 16(%rdi)
	je	.L137
	movq	%rdi, %rdx
	movq	%rcx, %rax
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L178:
	addq	$1, %rax
	andq	%rsi, %rax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%r8,%rdx,8), %rdx
	cmpb	$0, 16(%rdx)
	je	.L177
.L139:
	cmpq	(%rdx), %rbx
	jne	.L178
	leal	1(%r13), %eax
	movq	(%r15,%rax,8), %rbx
	movq	%rax, %r13
	testq	%rbx, %rbx
	jne	.L145
.L122:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L132:
	.cfi_restore_state
	movq	%rbx, (%rdx)
	movl	$0, 8(%rdx)
	movl	%ebx, 12(%rdx)
	movb	$1, 16(%rdx)
	movl	12(%r13), %eax
	addl	$1, %eax
	movl	%eax, %ecx
	movl	%eax, 12(%r13)
	shrl	$2, %ecx
	addl	%ecx, %eax
	cmpl	8(%r13), %eax
	jb	.L130
	movq	%r13, %rdi
	movl	%esi, -56(%rbp)
	call	_ZN2v84base19TemplateHashMapImplImjNS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE6ResizeES4_
	movl	8(%r13), %eax
	movq	0(%r13), %rdi
	movl	-56(%rbp), %esi
	leal	-1(%rax), %ecx
	movl	%ecx, %eax
	andl	%ebx, %eax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rdi,%rdx,8), %rdx
	cmpb	$0, 16(%rdx)
	jne	.L133
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L179:
	addq	$1, %rax
	andq	%rcx, %rax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rdi,%rdx,8), %rdx
	cmpb	$0, 16(%rdx)
	je	.L130
.L133:
	cmpq	(%rdx), %rbx
	jne	.L179
	movl	%esi, 8(%rdx)
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L177:
	movl	%r13d, %edx
	orl	$-2147483648, %edx
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L181:
	addq	$1, %rcx
	andq	%rsi, %rcx
	leaq	(%rcx,%rcx,2), %rax
	leaq	(%r8,%rax,8), %rdi
	cmpb	$0, 16(%rdi)
	je	.L143
.L142:
	cmpq	(%rdi), %rbx
	jne	.L181
.L141:
	leal	1(%r13), %eax
	movl	%edx, 8(%rdi)
	movq	(%r15,%rax,8), %rbx
	movq	%rax, %r13
	testq	%rbx, %rbx
	jne	.L145
	jmp	.L122
.L137:
	movl	%r13d, %edx
	orl	$-2147483648, %edx
	.p2align 4,,10
	.p2align 3
.L143:
	movq	%rbx, (%rdi)
	movl	$0, 8(%rdi)
	movl	%r12d, 12(%rdi)
	movb	$1, 16(%rdi)
	movl	12(%r9), %eax
	addl	$1, %eax
	movl	%eax, %ecx
	movl	%eax, 12(%r9)
	shrl	$2, %ecx
	addl	%ecx, %eax
	cmpl	8(%r9), %eax
	jb	.L141
	movq	%r9, %rdi
	movl	%edx, -60(%rbp)
	movq	%r9, -56(%rbp)
	call	_ZN2v84base19TemplateHashMapImplImjNS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE6ResizeES4_
	movq	-56(%rbp), %r9
	movl	-60(%rbp), %edx
	movl	8(%r9), %eax
	movq	(%r9), %rsi
	leal	-1(%rax), %ecx
	andl	%ecx, %r12d
	leaq	(%r12,%r12,2), %rax
	leaq	(%rsi,%rax,8), %rdi
	cmpb	$0, 16(%rdi)
	jne	.L144
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L182:
	addq	$1, %r12
	andq	%rcx, %r12
	leaq	(%r12,%r12,2), %rax
	leaq	(%rsi,%rax,8), %rdi
	cmpb	$0, 16(%rdi)
	je	.L141
.L144:
	cmpq	(%rdi), %rbx
	jne	.L182
	jmp	.L141
.L173:
	leaq	.LC7(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE17891:
	.size	_ZN2v88internal24ExternalReferenceEncoderC2EPNS0_7IsolateE, .-_ZN2v88internal24ExternalReferenceEncoderC2EPNS0_7IsolateE
	.globl	_ZN2v88internal24ExternalReferenceEncoderC1EPNS0_7IsolateE
	.set	_ZN2v88internal24ExternalReferenceEncoderC1EPNS0_7IsolateE,_ZN2v88internal24ExternalReferenceEncoderC2EPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal24ExternalReferenceEncoderC2EPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal24ExternalReferenceEncoderC2EPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal24ExternalReferenceEncoderC2EPNS0_7IsolateE:
.LFB21710:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE21710:
	.size	_GLOBAL__sub_I__ZN2v88internal24ExternalReferenceEncoderC2EPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal24ExternalReferenceEncoderC2EPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal24ExternalReferenceEncoderC2EPNS0_7IsolateE
	.globl	_ZN2v88internal14SerializedData12kMagicNumberE
	.section	.rodata._ZN2v88internal14SerializedData12kMagicNumberE,"a"
	.align 4
	.type	_ZN2v88internal14SerializedData12kMagicNumberE, @object
	.size	_ZN2v88internal14SerializedData12kMagicNumberE, 4
_ZN2v88internal14SerializedData12kMagicNumberE:
	.long	-1059191884
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
