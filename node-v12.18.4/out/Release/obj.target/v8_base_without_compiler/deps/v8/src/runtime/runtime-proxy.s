	.file	"runtime-proxy.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB4479:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE4479:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB4480:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4480:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB4482:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4482:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB8772:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L11
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L14
.L5:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L5
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE8772:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.rodata._ZN2v88internalL37Stats_Runtime_SetPropertyWithReceiverEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"disabled-by-default-v8.runtime"
	.align 8
.LC1:
	.string	"V8.Runtime_Runtime_SetPropertyWithReceiver"
	.section	.rodata._ZN2v88internalL37Stats_Runtime_SetPropertyWithReceiverEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"args[0].IsJSReceiver()"
.LC3:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internalL37Stats_Runtime_SetPropertyWithReceiverEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL37Stats_Runtime_SetPropertyWithReceiverEiPmPNS0_7IsolateE, @function
_ZN2v88internalL37Stats_Runtime_SetPropertyWithReceiverEiPmPNS0_7IsolateE:
.LFB20034:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$232, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -208(%rbp)
	movq	$0, -176(%rbp)
	movaps	%xmm0, -192(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L52
.L16:
	movq	_ZZN2v88internalL37Stats_Runtime_SetPropertyWithReceiverEiPmPNS0_7IsolateEE27trace_event_unique_atomic41(%rip), %r13
	testq	%r13, %r13
	je	.L53
.L18:
	movq	$0, -240(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L54
.L20:
	movq	41088(%r12), %r14
	movq	41096(%r12), %r13
	addl	$1, 41104(%r12)
	movq	(%rbx), %rax
	testb	$1, %al
	jne	.L24
.L25:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L53:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L55
.L19:
	movq	%r13, _ZZN2v88internalL37Stats_Runtime_SetPropertyWithReceiverEiPmPNS0_7IsolateEE27trace_event_unique_atomic41(%rip)
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L24:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L25
	subq	$8, %rsp
	leaq	-24(%rbx), %rdx
	leaq	-8(%rbx), %rcx
	movq	%rbx, %r9
	pushq	$3
	leaq	-160(%rbp), %rdi
	movq	%r12, %rsi
	leaq	-16(%rbx), %r15
	leaq	-241(%rbp), %r8
	movq	%rdi, -264(%rbp)
	movb	$0, -241(%rbp)
	call	_ZN2v88internal14LookupIterator17PropertyOrElementEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_PbNS4_INS0_10JSReceiverEEENS1_13ConfigurationE@PLT
	cmpb	$0, -241(%rbp)
	popq	%rax
	movq	-264(%rbp), %rdi
	popq	%rdx
	je	.L51
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	call	_ZN2v88internal6Object16SetSuperPropertyEPNS0_14LookupIteratorENS0_6HandleIS1_EENS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testb	%al, %al
	je	.L51
	movzbl	%ah, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory9ToBooleanEb@PLT
	movq	(%rax), %r15
.L27:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L31
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L31:
	leaq	-240(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L56
.L15:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L57
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	.cfi_restore_state
	movq	312(%r12), %r15
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L54:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L58
.L21:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L22
	movq	(%rdi), %rax
	call	*8(%rax)
.L22:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L23
	movq	(%rdi), %rax
	call	*8(%rax)
.L23:
	leaq	.LC1(%rip), %rax
	movq	%r13, -232(%rbp)
	movq	%rax, -224(%rbp)
	leaq	-232(%rbp), %rax
	movq	%r14, -216(%rbp)
	movq	%rax, -240(%rbp)
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L52:
	movq	40960(%rdx), %rax
	leaq	-200(%rbp), %rsi
	movl	$492, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -208(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L56:
	leaq	-200(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L55:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L58:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC1(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L21
.L57:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20034:
	.size	_ZN2v88internalL37Stats_Runtime_SetPropertyWithReceiverEiPmPNS0_7IsolateE, .-_ZN2v88internalL37Stats_Runtime_SetPropertyWithReceiverEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL40Stats_Runtime_CheckProxyGetSetTrapResultEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"V8.Runtime_Runtime_CheckProxyGetSetTrapResult"
	.section	.rodata._ZN2v88internalL40Stats_Runtime_CheckProxyGetSetTrapResultEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"args[0].IsName()"
.LC6:
	.string	"args[1].IsJSReceiver()"
.LC7:
	.string	"args[3].IsNumber()"
	.section	.text._ZN2v88internalL40Stats_Runtime_CheckProxyGetSetTrapResultEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL40Stats_Runtime_CheckProxyGetSetTrapResultEiPmPNS0_7IsolateE, @function
_ZN2v88internalL40Stats_Runtime_CheckProxyGetSetTrapResultEiPmPNS0_7IsolateE:
.LFB20037:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L106
.L60:
	movq	_ZZN2v88internalL40Stats_Runtime_CheckProxyGetSetTrapResultEiPmPNS0_7IsolateEE27trace_event_unique_atomic63(%rip), %rbx
	testq	%rbx, %rbx
	je	.L107
.L62:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L108
.L64:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L68
.L69:
	leaq	.LC5(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L107:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L109
.L63:
	movq	%rbx, _ZZN2v88internalL40Stats_Runtime_CheckProxyGetSetTrapResultEiPmPNS0_7IsolateEE27trace_event_unique_atomic63(%rip)
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L68:
	movq	-1(%rax), %rax
	cmpw	$64, 11(%rax)
	ja	.L69
	movq	-8(%r13), %rax
	leaq	-8(%r13), %rdx
	testb	$1, %al
	jne	.L110
.L70:
	leaq	.LC6(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L108:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L111
.L65:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L66
	movq	(%rdi), %rax
	call	*8(%rax)
.L66:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L67
	movq	(%rdi), %rax
	call	*8(%rax)
.L67:
	leaq	.LC4(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L110:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L70
	movq	-24(%r13), %r8
	leaq	-16(%r13), %rcx
	testb	$1, %r8b
	je	.L103
	movq	-1(%r8), %rax
	cmpw	$65, 11(%rax)
	jne	.L112
	movsd	7(%r8), %xmm0
	ucomisd	%xmm0, %xmm0
	jp	.L87
	comisd	.LC8(%rip), %xmm0
	movl	$-1, %r8d
	jnb	.L76
	movsd	.LC9(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jnb	.L87
	cvttsd2siq	%xmm0, %r8
.L76:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7JSProxy21CheckGetSetTrapResultEPNS0_7IsolateENS0_6HandleINS0_4NameEEENS4_INS0_10JSReceiverEEENS4_INS0_6ObjectEEENS1_10AccessKindE@PLT
	testq	%rax, %rax
	je	.L113
.L77:
	movq	(%rax), %r13
.L78:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L81
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L81:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L114
.L59:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L115
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L103:
	.cfi_restore_state
	shrq	$32, %r8
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7JSProxy21CheckGetSetTrapResultEPNS0_7IsolateENS0_6HandleINS0_4NameEEENS4_INS0_10JSReceiverEEENS4_INS0_6ObjectEEENS1_10AccessKindE@PLT
	testq	%rax, %rax
	jne	.L77
.L113:
	movq	312(%r12), %r13
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L106:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$488, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L112:
	leaq	.LC7(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L114:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L111:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC4(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L109:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L87:
	xorl	%r8d, %r8d
	jmp	.L76
.L115:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20037:
	.size	_ZN2v88internalL40Stats_Runtime_CheckProxyGetSetTrapResultEiPmPNS0_7IsolateE, .-_ZN2v88internalL40Stats_Runtime_CheckProxyGetSetTrapResultEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL37Stats_Runtime_CheckProxyHasTrapResultEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC10:
	.string	"V8.Runtime_Runtime_CheckProxyHasTrapResult"
	.section	.text._ZN2v88internalL37Stats_Runtime_CheckProxyHasTrapResultEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL37Stats_Runtime_CheckProxyHasTrapResultEiPmPNS0_7IsolateE, @function
_ZN2v88internalL37Stats_Runtime_CheckProxyHasTrapResultEiPmPNS0_7IsolateE:
.LFB20040:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L155
.L117:
	movq	_ZZN2v88internalL37Stats_Runtime_CheckProxyHasTrapResultEiPmPNS0_7IsolateEE27trace_event_unique_atomic77(%rip), %rbx
	testq	%rbx, %rbx
	je	.L156
.L119:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L157
.L121:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L125
.L126:
	leaq	.LC5(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L156:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L158
.L120:
	movq	%rbx, _ZZN2v88internalL37Stats_Runtime_CheckProxyHasTrapResultEiPmPNS0_7IsolateEE27trace_event_unique_atomic77(%rip)
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L125:
	movq	-1(%rax), %rax
	cmpw	$64, 11(%rax)
	ja	.L126
	movq	-8(%r13), %rax
	leaq	-8(%r13), %rdx
	testb	$1, %al
	jne	.L159
.L127:
	leaq	.LC6(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L157:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L160
.L122:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L123
	movq	(%rdi), %rax
	call	*8(%rax)
.L123:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L124
	movq	(%rdi), %rax
	call	*8(%rax)
.L124:
	leaq	.LC10(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L159:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L127
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7JSProxy12CheckHasTrapEPNS0_7IsolateENS0_6HandleINS0_4NameEEENS4_INS0_10JSReceiverEEE@PLT
	testb	%al, %al
	je	.L161
	shrw	$8, %ax
	je	.L131
	movq	112(%r12), %r13
.L130:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L135
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L135:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L162
.L116:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L163
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L131:
	.cfi_restore_state
	movq	120(%r12), %r13
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L161:
	movq	312(%r12), %r13
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L155:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$489, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L162:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L160:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC10(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L158:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L120
.L163:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20040:
	.size	_ZN2v88internalL37Stats_Runtime_CheckProxyHasTrapResultEiPmPNS0_7IsolateE, .-_ZN2v88internalL37Stats_Runtime_CheckProxyHasTrapResultEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL40Stats_Runtime_CheckProxyDeleteTrapResultEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC11:
	.string	"V8.Runtime_Runtime_CheckProxyDeleteTrapResult"
	.section	.text._ZN2v88internalL40Stats_Runtime_CheckProxyDeleteTrapResultEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL40Stats_Runtime_CheckProxyDeleteTrapResultEiPmPNS0_7IsolateE, @function
_ZN2v88internalL40Stats_Runtime_CheckProxyDeleteTrapResultEiPmPNS0_7IsolateE:
.LFB20043:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L203
.L165:
	movq	_ZZN2v88internalL40Stats_Runtime_CheckProxyDeleteTrapResultEiPmPNS0_7IsolateEE27trace_event_unique_atomic89(%rip), %rbx
	testq	%rbx, %rbx
	je	.L204
.L167:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L205
.L169:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L173
.L174:
	leaq	.LC5(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L204:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L206
.L168:
	movq	%rbx, _ZZN2v88internalL40Stats_Runtime_CheckProxyDeleteTrapResultEiPmPNS0_7IsolateEE27trace_event_unique_atomic89(%rip)
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L173:
	movq	-1(%rax), %rax
	cmpw	$64, 11(%rax)
	ja	.L174
	movq	-8(%r13), %rax
	leaq	-8(%r13), %rdx
	testb	$1, %al
	jne	.L207
.L175:
	leaq	.LC6(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L205:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L208
.L170:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L171
	movq	(%rdi), %rax
	call	*8(%rax)
.L171:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L172
	movq	(%rdi), %rax
	call	*8(%rax)
.L172:
	leaq	.LC11(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L207:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L175
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7JSProxy15CheckDeleteTrapEPNS0_7IsolateENS0_6HandleINS0_4NameEEENS4_INS0_10JSReceiverEEE@PLT
	testb	%al, %al
	je	.L209
	shrw	$8, %ax
	je	.L179
	movq	112(%r12), %r13
.L178:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L183
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L183:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L210
.L164:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L211
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L179:
	.cfi_restore_state
	movq	120(%r12), %r13
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L209:
	movq	312(%r12), %r13
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L203:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$490, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L210:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L208:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC11(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L206:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L168
.L211:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20043:
	.size	_ZN2v88internalL40Stats_Runtime_CheckProxyDeleteTrapResultEiPmPNS0_7IsolateE, .-_ZN2v88internalL40Stats_Runtime_CheckProxyDeleteTrapResultEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL37Stats_Runtime_GetPropertyWithReceiverEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC12:
	.string	"V8.Runtime_Runtime_GetPropertyWithReceiver"
	.section	.rodata._ZN2v88internalL37Stats_Runtime_GetPropertyWithReceiverEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC13:
	.string	"args[3].IsSmi()"
	.section	.text._ZN2v88internalL37Stats_Runtime_GetPropertyWithReceiverEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL37Stats_Runtime_GetPropertyWithReceiverEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL37Stats_Runtime_GetPropertyWithReceiverEiPmPNS0_7IsolateE.isra.0:
.LFB24990:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$216, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -208(%rbp)
	movq	$0, -176(%rbp)
	movaps	%xmm0, -192(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L251
.L213:
	movq	_ZZN2v88internalL37Stats_Runtime_GetPropertyWithReceiverEiPmPNS0_7IsolateEE27trace_event_unique_atomic19(%rip), %r13
	testq	%r13, %r13
	je	.L252
.L215:
	movq	$0, -240(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L253
.L217:
	movq	41088(%r12), %r14
	movq	41096(%r12), %r13
	addl	$1, 41104(%r12)
	movq	(%rbx), %rax
	testb	$1, %al
	jne	.L254
.L221:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L252:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L255
.L216:
	movq	%r13, _ZZN2v88internalL37Stats_Runtime_GetPropertyWithReceiverEiPmPNS0_7IsolateEE27trace_event_unique_atomic19(%rip)
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L254:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L221
	leaq	-8(%rbx), %rcx
	leaq	-16(%rbx), %rdx
	testb	$1, -24(%rbx)
	jne	.L256
	subq	$8, %rsp
	movq	%rbx, %r9
	movq	%r12, %rsi
	movb	$0, -241(%rbp)
	pushq	$3
	leaq	-160(%rbp), %r15
	leaq	-241(%rbp), %r8
	movq	%r15, %rdi
	call	_ZN2v88internal14LookupIterator17PropertyOrElementEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_PbNS4_INS0_10JSReceiverEEENS1_13ConfigurationE@PLT
	cmpb	$0, -241(%rbp)
	popq	%rax
	popq	%rdx
	je	.L250
	movslq	-20(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	testq	%rax, %rax
	je	.L250
	movq	(%rax), %r15
.L225:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L229
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L229:
	leaq	-240(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L257
.L212:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L258
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L250:
	.cfi_restore_state
	movq	312(%r12), %r15
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L253:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L259
.L218:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L219
	movq	(%rdi), %rax
	call	*8(%rax)
.L219:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L220
	movq	(%rdi), %rax
	call	*8(%rax)
.L220:
	leaq	.LC12(%rip), %rax
	movq	%r13, -232(%rbp)
	movq	%rax, -224(%rbp)
	leaq	-232(%rbp), %rax
	movq	%r14, -216(%rbp)
	movq	%rax, -240(%rbp)
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L251:
	movq	40960(%rsi), %rax
	movl	$491, %edx
	leaq	-200(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -208(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L256:
	leaq	.LC13(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L257:
	leaq	-200(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L259:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC12(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L255:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L216
.L258:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24990:
	.size	_ZN2v88internalL37Stats_Runtime_GetPropertyWithReceiverEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL37Stats_Runtime_GetPropertyWithReceiverEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal31Runtime_GetPropertyWithReceiverEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal31Runtime_GetPropertyWithReceiverEiPmPNS0_7IsolateE
	.type	_ZN2v88internal31Runtime_GetPropertyWithReceiverEiPmPNS0_7IsolateE, @function
_ZN2v88internal31Runtime_GetPropertyWithReceiverEiPmPNS0_7IsolateE:
.LFB20032:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L275
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L276
.L263:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L276:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L263
	leaq	-8(%rsi), %rcx
	leaq	-16(%rsi), %rdx
	testb	$1, -24(%rsi)
	jne	.L277
	subq	$8, %rsp
	leaq	-144(%rbp), %r15
	movq	%rsi, %r9
	movq	%r12, %rsi
	pushq	$3
	leaq	-145(%rbp), %r8
	movq	%r15, %rdi
	movb	$0, -145(%rbp)
	call	_ZN2v88internal14LookupIterator17PropertyOrElementEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_PbNS4_INS0_10JSReceiverEEENS1_13ConfigurationE@PLT
	cmpb	$0, -145(%rbp)
	popq	%rax
	popq	%rdx
	je	.L274
	movslq	-20(%r13), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	testq	%rax, %rax
	je	.L274
	movq	(%rax), %r13
.L267:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L260
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L260:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L278
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L274:
	.cfi_restore_state
	movq	312(%r12), %r13
	jmp	.L267
	.p2align 4,,10
	.p2align 3
.L275:
	movq	%r13, %rdi
	movq	%rdx, %rsi
	call	_ZN2v88internalL37Stats_Runtime_GetPropertyWithReceiverEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r13
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L277:
	leaq	.LC13(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L278:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20032:
	.size	_ZN2v88internal31Runtime_GetPropertyWithReceiverEiPmPNS0_7IsolateE, .-_ZN2v88internal31Runtime_GetPropertyWithReceiverEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal31Runtime_SetPropertyWithReceiverEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal31Runtime_SetPropertyWithReceiverEiPmPNS0_7IsolateE
	.type	_ZN2v88internal31Runtime_SetPropertyWithReceiverEiPmPNS0_7IsolateE, @function
_ZN2v88internal31Runtime_SetPropertyWithReceiverEiPmPNS0_7IsolateE:
.LFB20035:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L292
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L282
.L283:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L282:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L283
	subq	$8, %rsp
	leaq	-144(%rbp), %r15
	leaq	-24(%rsi), %rdx
	movb	$0, -145(%rbp)
	pushq	$3
	leaq	-8(%rsi), %rcx
	leaq	-16(%rsi), %r14
	movq	%r15, %rdi
	leaq	-145(%rbp), %r8
	movq	%r12, %rsi
	call	_ZN2v88internal14LookupIterator17PropertyOrElementEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_PbNS4_INS0_10JSReceiverEEENS1_13ConfigurationE@PLT
	cmpb	$0, -145(%rbp)
	popq	%rax
	popq	%rdx
	je	.L291
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal6Object16SetSuperPropertyEPNS0_14LookupIteratorENS0_6HandleIS1_EENS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testb	%al, %al
	je	.L291
	movzbl	%ah, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory9ToBooleanEb@PLT
	movq	(%rax), %r14
.L285:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L279
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L279:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L293
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L291:
	.cfi_restore_state
	movq	312(%r12), %r14
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L292:
	call	_ZN2v88internalL37Stats_Runtime_SetPropertyWithReceiverEiPmPNS0_7IsolateE
	movq	%rax, %r14
	jmp	.L279
.L293:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20035:
	.size	_ZN2v88internal31Runtime_SetPropertyWithReceiverEiPmPNS0_7IsolateE, .-_ZN2v88internal31Runtime_SetPropertyWithReceiverEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal34Runtime_CheckProxyGetSetTrapResultEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal34Runtime_CheckProxyGetSetTrapResultEiPmPNS0_7IsolateE
	.type	_ZN2v88internal34Runtime_CheckProxyGetSetTrapResultEiPmPNS0_7IsolateE, @function
_ZN2v88internal34Runtime_CheckProxyGetSetTrapResultEiPmPNS0_7IsolateE:
.LFB20038:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %r8d
	testl	%r8d, %r8d
	jne	.L313
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L296
.L297:
	leaq	.LC5(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L296:
	movq	-1(%rax), %rax
	cmpw	$64, 11(%rax)
	ja	.L297
	movq	-8(%rsi), %rax
	leaq	-8(%rsi), %rdx
	testb	$1, %al
	jne	.L314
.L298:
	leaq	.LC6(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L314:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L298
	movq	-24(%rsi), %rax
	leaq	-16(%rsi), %rcx
	testb	$1, %al
	je	.L310
	movq	-1(%rax), %rdi
	cmpw	$65, 11(%rdi)
	jne	.L315
	movsd	7(%rax), %xmm0
	ucomisd	%xmm0, %xmm0
	jp	.L304
	comisd	.LC8(%rip), %xmm0
	jnb	.L309
	movsd	.LC9(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jnb	.L304
	cvttsd2siq	%xmm0, %r8
.L304:
	movq	%r12, %rdi
	call	_ZN2v88internal7JSProxy21CheckGetSetTrapResultEPNS0_7IsolateENS0_6HandleINS0_4NameEEENS4_INS0_10JSReceiverEEENS4_INS0_6ObjectEEENS1_10AccessKindE@PLT
	testq	%rax, %rax
	je	.L316
.L305:
	movq	(%rax), %r14
.L306:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L294
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L294:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L310:
	.cfi_restore_state
	shrq	$32, %rax
	movq	%r12, %rdi
	movq	%rax, %r8
	call	_ZN2v88internal7JSProxy21CheckGetSetTrapResultEPNS0_7IsolateENS0_6HandleINS0_4NameEEENS4_INS0_10JSReceiverEEENS4_INS0_6ObjectEEENS1_10AccessKindE@PLT
	testq	%rax, %rax
	jne	.L305
.L316:
	movq	312(%r12), %r14
	jmp	.L306
	.p2align 4,,10
	.p2align 3
.L309:
	movl	$-1, %r8d
	jmp	.L304
	.p2align 4,,10
	.p2align 3
.L313:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL40Stats_Runtime_CheckProxyGetSetTrapResultEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L315:
	.cfi_restore_state
	leaq	.LC7(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20038:
	.size	_ZN2v88internal34Runtime_CheckProxyGetSetTrapResultEiPmPNS0_7IsolateE, .-_ZN2v88internal34Runtime_CheckProxyGetSetTrapResultEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal31Runtime_CheckProxyHasTrapResultEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal31Runtime_CheckProxyHasTrapResultEiPmPNS0_7IsolateE
	.type	_ZN2v88internal31Runtime_CheckProxyHasTrapResultEiPmPNS0_7IsolateE, @function
_ZN2v88internal31Runtime_CheckProxyHasTrapResultEiPmPNS0_7IsolateE:
.LFB20041:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L330
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L319
.L320:
	leaq	.LC5(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L319:
	movq	-1(%rax), %rax
	cmpw	$64, 11(%rax)
	ja	.L320
	movq	-8(%rsi), %rax
	leaq	-8(%rsi), %rdx
	testb	$1, %al
	jne	.L331
.L321:
	leaq	.LC6(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L331:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L321
	movq	%r12, %rdi
	call	_ZN2v88internal7JSProxy12CheckHasTrapEPNS0_7IsolateENS0_6HandleINS0_4NameEEENS4_INS0_10JSReceiverEEE@PLT
	testb	%al, %al
	je	.L332
	shrw	$8, %ax
	je	.L325
	movq	112(%r12), %r14
.L324:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L317
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L317:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L325:
	.cfi_restore_state
	movq	120(%r12), %r14
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L332:
	movq	312(%r12), %r14
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L330:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL37Stats_Runtime_CheckProxyHasTrapResultEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE20041:
	.size	_ZN2v88internal31Runtime_CheckProxyHasTrapResultEiPmPNS0_7IsolateE, .-_ZN2v88internal31Runtime_CheckProxyHasTrapResultEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal34Runtime_CheckProxyDeleteTrapResultEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal34Runtime_CheckProxyDeleteTrapResultEiPmPNS0_7IsolateE
	.type	_ZN2v88internal34Runtime_CheckProxyDeleteTrapResultEiPmPNS0_7IsolateE, @function
_ZN2v88internal34Runtime_CheckProxyDeleteTrapResultEiPmPNS0_7IsolateE:
.LFB20044:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L346
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L335
.L336:
	leaq	.LC5(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L335:
	movq	-1(%rax), %rax
	cmpw	$64, 11(%rax)
	ja	.L336
	movq	-8(%rsi), %rax
	leaq	-8(%rsi), %rdx
	testb	$1, %al
	jne	.L347
.L337:
	leaq	.LC6(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L347:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L337
	movq	%r12, %rdi
	call	_ZN2v88internal7JSProxy15CheckDeleteTrapEPNS0_7IsolateENS0_6HandleINS0_4NameEEENS4_INS0_10JSReceiverEEE@PLT
	testb	%al, %al
	je	.L348
	shrw	$8, %ax
	je	.L341
	movq	112(%r12), %r14
.L340:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L333
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L333:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L341:
	.cfi_restore_state
	movq	120(%r12), %r14
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L348:
	movq	312(%r12), %r14
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L346:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL40Stats_Runtime_CheckProxyDeleteTrapResultEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE20044:
	.size	_ZN2v88internal34Runtime_CheckProxyDeleteTrapResultEiPmPNS0_7IsolateE, .-_ZN2v88internal34Runtime_CheckProxyDeleteTrapResultEiPmPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal31Runtime_GetPropertyWithReceiverEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal31Runtime_GetPropertyWithReceiverEiPmPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal31Runtime_GetPropertyWithReceiverEiPmPNS0_7IsolateE:
.LFB24974:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE24974:
	.size	_GLOBAL__sub_I__ZN2v88internal31Runtime_GetPropertyWithReceiverEiPmPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal31Runtime_GetPropertyWithReceiverEiPmPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal31Runtime_GetPropertyWithReceiverEiPmPNS0_7IsolateE
	.section	.bss._ZZN2v88internalL40Stats_Runtime_CheckProxyDeleteTrapResultEiPmPNS0_7IsolateEE27trace_event_unique_atomic89,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL40Stats_Runtime_CheckProxyDeleteTrapResultEiPmPNS0_7IsolateEE27trace_event_unique_atomic89, @object
	.size	_ZZN2v88internalL40Stats_Runtime_CheckProxyDeleteTrapResultEiPmPNS0_7IsolateEE27trace_event_unique_atomic89, 8
_ZZN2v88internalL40Stats_Runtime_CheckProxyDeleteTrapResultEiPmPNS0_7IsolateEE27trace_event_unique_atomic89:
	.zero	8
	.section	.bss._ZZN2v88internalL37Stats_Runtime_CheckProxyHasTrapResultEiPmPNS0_7IsolateEE27trace_event_unique_atomic77,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL37Stats_Runtime_CheckProxyHasTrapResultEiPmPNS0_7IsolateEE27trace_event_unique_atomic77, @object
	.size	_ZZN2v88internalL37Stats_Runtime_CheckProxyHasTrapResultEiPmPNS0_7IsolateEE27trace_event_unique_atomic77, 8
_ZZN2v88internalL37Stats_Runtime_CheckProxyHasTrapResultEiPmPNS0_7IsolateEE27trace_event_unique_atomic77:
	.zero	8
	.section	.bss._ZZN2v88internalL40Stats_Runtime_CheckProxyGetSetTrapResultEiPmPNS0_7IsolateEE27trace_event_unique_atomic63,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL40Stats_Runtime_CheckProxyGetSetTrapResultEiPmPNS0_7IsolateEE27trace_event_unique_atomic63, @object
	.size	_ZZN2v88internalL40Stats_Runtime_CheckProxyGetSetTrapResultEiPmPNS0_7IsolateEE27trace_event_unique_atomic63, 8
_ZZN2v88internalL40Stats_Runtime_CheckProxyGetSetTrapResultEiPmPNS0_7IsolateEE27trace_event_unique_atomic63:
	.zero	8
	.section	.bss._ZZN2v88internalL37Stats_Runtime_SetPropertyWithReceiverEiPmPNS0_7IsolateEE27trace_event_unique_atomic41,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL37Stats_Runtime_SetPropertyWithReceiverEiPmPNS0_7IsolateEE27trace_event_unique_atomic41, @object
	.size	_ZZN2v88internalL37Stats_Runtime_SetPropertyWithReceiverEiPmPNS0_7IsolateEE27trace_event_unique_atomic41, 8
_ZZN2v88internalL37Stats_Runtime_SetPropertyWithReceiverEiPmPNS0_7IsolateEE27trace_event_unique_atomic41:
	.zero	8
	.section	.bss._ZZN2v88internalL37Stats_Runtime_GetPropertyWithReceiverEiPmPNS0_7IsolateEE27trace_event_unique_atomic19,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL37Stats_Runtime_GetPropertyWithReceiverEiPmPNS0_7IsolateEE27trace_event_unique_atomic19, @object
	.size	_ZZN2v88internalL37Stats_Runtime_GetPropertyWithReceiverEiPmPNS0_7IsolateEE27trace_event_unique_atomic19, 8
_ZZN2v88internalL37Stats_Runtime_GetPropertyWithReceiverEiPmPNS0_7IsolateEE27trace_event_unique_atomic19:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC8:
	.long	0
	.long	1138753536
	.align 8
.LC9:
	.long	0
	.long	-1008730112
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
