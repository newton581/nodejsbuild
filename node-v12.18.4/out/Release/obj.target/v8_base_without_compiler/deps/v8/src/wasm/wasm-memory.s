	.file	"wasm-memory.cc"
	.text
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal4wasm12_GLOBAL__N_123TryAllocateBackingStoreEPNS3_17WasmMemoryTrackerEPNS2_4HeapEmmPPvPmEUlvE0_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal4wasm12_GLOBAL__N_123TryAllocateBackingStoreEPNS3_17WasmMemoryTrackerEPNS2_4HeapEmmPPvPmEUlvE0_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal4wasm12_GLOBAL__N_123TryAllocateBackingStoreEPNS3_17WasmMemoryTrackerEPNS2_4HeapEmmPPvPmEUlvE0_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation:
.LFB23531:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L2
	cmpl	$3, %edx
	je	.L3
	cmpl	$1, %edx
	je	.L8
.L3:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movdqu	(%rsi), %xmm0
	xorl	%eax, %eax
	movups	%xmm0, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE23531:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal4wasm12_GLOBAL__N_123TryAllocateBackingStoreEPNS3_17WasmMemoryTrackerEPNS2_4HeapEmmPPvPmEUlvE0_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal4wasm12_GLOBAL__N_123TryAllocateBackingStoreEPNS3_17WasmMemoryTrackerEPNS2_4HeapEmmPPvPmEUlvE0_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal4wasm12_GLOBAL__N_123TryAllocateBackingStoreEPNS3_17WasmMemoryTrackerEPNS2_4HeapEmmPPvPmEUlvE1_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal4wasm12_GLOBAL__N_123TryAllocateBackingStoreEPNS3_17WasmMemoryTrackerEPNS2_4HeapEmmPPvPmEUlvE1_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal4wasm12_GLOBAL__N_123TryAllocateBackingStoreEPNS3_17WasmMemoryTrackerEPNS2_4HeapEmmPPvPmEUlvE1_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation:
.LFB23535:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L10
	cmpl	$3, %edx
	je	.L11
	cmpl	$1, %edx
	je	.L15
.L11:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movdqu	(%rsi), %xmm0
	xorl	%eax, %eax
	movups	%xmm0, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE23535:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal4wasm12_GLOBAL__N_123TryAllocateBackingStoreEPNS3_17WasmMemoryTrackerEPNS2_4HeapEmmPPvPmEUlvE1_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal4wasm12_GLOBAL__N_123TryAllocateBackingStoreEPNS3_17WasmMemoryTrackerEPNS2_4HeapEmmPPvPmEUlvE1_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation
	.section	.text._ZNSt17_Function_handlerIFbvEZN2v88internal4wasm12_GLOBAL__N_123TryAllocateBackingStoreEPNS3_17WasmMemoryTrackerEPNS2_4HeapEmmPPvPmEUlvE0_E9_M_invokeERKSt9_Any_data,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFbvEZN2v88internal4wasm12_GLOBAL__N_123TryAllocateBackingStoreEPNS3_17WasmMemoryTrackerEPNS2_4HeapEmmPPvPmEUlvE0_E9_M_invokeERKSt9_Any_data, @function
_ZNSt17_Function_handlerIFbvEZN2v88internal4wasm12_GLOBAL__N_123TryAllocateBackingStoreEPNS3_17WasmMemoryTrackerEPNS2_4HeapEmmPPvPmEUlvE0_E9_M_invokeERKSt9_Any_data:
.LFB23530:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movq	(%rax), %rax
	movq	(%rax), %r13
	call	_ZN2v88internal24GetPlatformPageAllocatorEv@PLT
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movl	$65536, %ecx
	movq	%rax, %rdi
	movq	(%rbx), %rax
	movq	%r13, %rdx
	movq	(%rax), %r12
	call	_ZN2v88internal13AllocatePagesEPNS_13PageAllocatorEPvmmNS1_10PermissionE@PLT
	movq	%rax, (%r12)
	movq	(%rbx), %rax
	movq	(%rax), %rax
	cmpq	$0, (%rax)
	setne	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23530:
	.size	_ZNSt17_Function_handlerIFbvEZN2v88internal4wasm12_GLOBAL__N_123TryAllocateBackingStoreEPNS3_17WasmMemoryTrackerEPNS2_4HeapEmmPPvPmEUlvE0_E9_M_invokeERKSt9_Any_data, .-_ZNSt17_Function_handlerIFbvEZN2v88internal4wasm12_GLOBAL__N_123TryAllocateBackingStoreEPNS3_17WasmMemoryTrackerEPNS2_4HeapEmmPPvPmEUlvE0_E9_M_invokeERKSt9_Any_data
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal4wasm12_GLOBAL__N_123TryAllocateBackingStoreEPNS3_17WasmMemoryTrackerEPNS2_4HeapEmmPPvPmEUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal4wasm12_GLOBAL__N_123TryAllocateBackingStoreEPNS3_17WasmMemoryTrackerEPNS2_4HeapEmmPPvPmEUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal4wasm12_GLOBAL__N_123TryAllocateBackingStoreEPNS3_17WasmMemoryTrackerEPNS2_4HeapEmmPPvPmEUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation:
.LFB23527:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	cmpl	$2, %edx
	je	.L19
	cmpl	$3, %edx
	je	.L20
	cmpl	$1, %edx
	je	.L26
.L21:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	movq	(%rsi), %r12
	movl	$24, %edi
	call	_Znwm@PLT
	movdqu	(%r12), %xmm0
	movups	%xmm0, (%rax)
	movq	16(%r12), %rdx
	movq	%rax, (%rbx)
	movq	%rdx, 16(%rax)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L21
	movl	$24, %esi
	call	_ZdlPvm@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23527:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal4wasm12_GLOBAL__N_123TryAllocateBackingStoreEPNS3_17WasmMemoryTrackerEPNS2_4HeapEmmPPvPmEUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal4wasm12_GLOBAL__N_123TryAllocateBackingStoreEPNS3_17WasmMemoryTrackerEPNS2_4HeapEmmPPvPmEUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation
	.section	.text._ZNSt17_Function_handlerIFbvEZN2v88internal4wasm12_GLOBAL__N_123TryAllocateBackingStoreEPNS3_17WasmMemoryTrackerEPNS2_4HeapEmmPPvPmEUlvE1_E9_M_invokeERKSt9_Any_data,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFbvEZN2v88internal4wasm12_GLOBAL__N_123TryAllocateBackingStoreEPNS3_17WasmMemoryTrackerEPNS2_4HeapEmmPPvPmEUlvE1_E9_M_invokeERKSt9_Any_data, @function
_ZNSt17_Function_handlerIFbvEZN2v88internal4wasm12_GLOBAL__N_123TryAllocateBackingStoreEPNS3_17WasmMemoryTrackerEPNS2_4HeapEmmPPvPmEUlvE1_E9_M_invokeERKSt9_Any_data:
.LFB23534:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	jne	.L30
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	movq	8(%rdi), %rax
	movq	(%rax), %r12
	call	_ZN2v88internal24GetPlatformPageAllocatorEv@PLT
	leaq	65535(%rbx), %rdx
	movl	$2, %ecx
	popq	%rbx
	movq	%r12, %rsi
	movq	%rax, %rdi
	popq	%r12
	xorw	%dx, %dx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14SetPermissionsEPNS_13PageAllocatorEPvmNS1_10PermissionE@PLT
	.cfi_endproc
.LFE23534:
	.size	_ZNSt17_Function_handlerIFbvEZN2v88internal4wasm12_GLOBAL__N_123TryAllocateBackingStoreEPNS3_17WasmMemoryTrackerEPNS2_4HeapEmmPPvPmEUlvE1_E9_M_invokeERKSt9_Any_data, .-_ZNSt17_Function_handlerIFbvEZN2v88internal4wasm12_GLOBAL__N_123TryAllocateBackingStoreEPNS3_17WasmMemoryTrackerEPNS2_4HeapEmmPPvPmEUlvE1_E9_M_invokeERKSt9_Any_data
	.section	.text._ZNSt17_Function_handlerIFbvEZN2v88internal4wasm12_GLOBAL__N_123TryAllocateBackingStoreEPNS3_17WasmMemoryTrackerEPNS2_4HeapEmmPPvPmEUlvE_E9_M_invokeERKSt9_Any_data,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFbvEZN2v88internal4wasm12_GLOBAL__N_123TryAllocateBackingStoreEPNS3_17WasmMemoryTrackerEPNS2_4HeapEmmPPvPmEUlvE_E9_M_invokeERKSt9_Any_data, @function
_ZNSt17_Function_handlerIFbvEZN2v88internal4wasm12_GLOBAL__N_123TryAllocateBackingStoreEPNS3_17WasmMemoryTrackerEPNS2_4HeapEmmPPvPmEUlvE_E9_M_invokeERKSt9_Any_data:
.LFB23526:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rbx
	call	_ZN2v88internal14CommitPageSizeEv@PLT
	movq	(%rbx), %rdx
	movq	(%rdx), %rcx
	movabsq	$10737418237, %rdx
	addq	%rax, %rdx
	negq	%rax
	andq	%rdx, %rax
	movq	%rax, (%rcx)
	movq	16(%rbx), %rax
	movabsq	$1103806595072, %rcx
	movq	(%rax), %rdx
	movq	(%rbx), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
.L33:
	movq	(%rdx), %rax
	cmpq	%rcx, %rax
	ja	.L35
	movq	%rcx, %rdi
	subq	%rax, %rdi
	cmpq	%rdi, %rsi
	ja	.L35
	leaq	(%rsi,%rax), %rdi
	lock cmpxchgq	%rdi, (%rdx)
	jne	.L33
	movl	$1, %eax
.L31:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L31
	.cfi_endproc
.LFE23526:
	.size	_ZNSt17_Function_handlerIFbvEZN2v88internal4wasm12_GLOBAL__N_123TryAllocateBackingStoreEPNS3_17WasmMemoryTrackerEPNS2_4HeapEmmPPvPmEUlvE_E9_M_invokeERKSt9_Any_data, .-_ZNSt17_Function_handlerIFbvEZN2v88internal4wasm12_GLOBAL__N_123TryAllocateBackingStoreEPNS3_17WasmMemoryTrackerEPNS2_4HeapEmmPPvPmEUlvE_E9_M_invokeERKSt9_Any_data
	.section	.text._ZN2v88internal4wasm17WasmMemoryTracker19ReserveAddressSpaceEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm17WasmMemoryTracker19ReserveAddressSpaceEm
	.type	_ZN2v88internal4wasm17WasmMemoryTracker19ReserveAddressSpaceEm, @function
_ZN2v88internal4wasm17WasmMemoryTracker19ReserveAddressSpaceEm:
.LFB20043:
	.cfi_startproc
	endbr64
	movabsq	$1103806595072, %rdx
.L41:
	movq	(%rdi), %rax
	cmpq	%rdx, %rax
	ja	.L43
	movq	%rdx, %rcx
	subq	%rax, %rcx
	cmpq	%rsi, %rcx
	jb	.L43
	leaq	(%rsi,%rax), %rcx
	lock cmpxchgq	%rcx, (%rdi)
	jne	.L41
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE20043:
	.size	_ZN2v88internal4wasm17WasmMemoryTracker19ReserveAddressSpaceEm, .-_ZN2v88internal4wasm17WasmMemoryTracker19ReserveAddressSpaceEm
	.section	.text._ZN2v88internal4wasm17WasmMemoryTracker18ReleaseReservationEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm17WasmMemoryTracker18ReleaseReservationEm
	.type	_ZN2v88internal4wasm17WasmMemoryTracker18ReleaseReservationEm, @function
_ZN2v88internal4wasm17WasmMemoryTracker18ReleaseReservationEm:
.LFB20044:
	.cfi_startproc
	endbr64
	lock subq	%rsi, (%rdi)
	ret
	.cfi_endproc
.LFE20044:
	.size	_ZN2v88internal4wasm17WasmMemoryTracker18ReleaseReservationEm, .-_ZN2v88internal4wasm17WasmMemoryTracker18ReleaseReservationEm
	.section	.rodata._ZN2v88internal4wasm17WasmMemoryTracker24ReleaseAllocation_LockedEPNS0_7IsolateEPKv.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"find_result != allocations_.end()"
	.section	.rodata._ZN2v88internal4wasm17WasmMemoryTracker24ReleaseAllocation_LockedEPNS0_7IsolateEPKv.str1.1,"aMS",@progbits,1
.LC1:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal4wasm17WasmMemoryTracker24ReleaseAllocation_LockedEPNS0_7IsolateEPKv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm17WasmMemoryTracker24ReleaseAllocation_LockedEPNS0_7IsolateEPKv
	.type	_ZN2v88internal4wasm17WasmMemoryTracker24ReleaseAllocation_LockedEPNS0_7IsolateEPKv, @function
_ZN2v88internal4wasm17WasmMemoryTracker24ReleaseAllocation_LockedEPNS0_7IsolateEPKv:
.LFB20046:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %rax
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	movq	64(%rsi), %rsi
	divq	%rsi
	movq	56(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L48
	movq	(%rax), %r12
	movq	%rdi, %r13
	movq	%rdx, %r8
	movq	8(%r12), %rdi
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L81:
	movq	(%r12), %r12
	testq	%r12, %r12
	je	.L48
	movq	8(%r12), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rsi
	cmpq	%rdx, %r8
	jne	.L48
.L50:
	cmpq	%rcx, %rdi
	jne	.L81
	movq	24(%r12), %rax
	lock subq	%rax, (%rbx)
	pxor	%xmm0, %xmm0
	subq	%rax, 48(%rbx)
	movq	40(%r12), %rax
	movq	24(%r12), %rcx
	movq	32(%r12), %rdx
	movq	16(%r12), %rsi
	movq	%rax, 24(%r13)
	movzwl	48(%r12), %eax
	movq	%rsi, 0(%r13)
	movw	%ax, 32(%r13)
	movq	%rcx, 8(%r13)
	movq	%rdx, 16(%r13)
	movq	64(%r12), %r14
	subq	56(%r12), %r14
	movq	$0, 56(%r13)
	movq	%r14, %rax
	movups	%xmm0, 40(%r13)
	sarq	$4, %rax
	je	.L82
	movabsq	$576460752303423487, %rdx
	cmpq	%rdx, %rax
	ja	.L83
	movq	%r14, %rdi
	call	_Znwm@PLT
.L51:
	movq	%rax, %xmm0
	addq	%rax, %r14
	punpcklqdq	%xmm0, %xmm0
	movq	%r14, 56(%r13)
	movups	%xmm0, 40(%r13)
	movq	64(%r12), %rdx
	movq	56(%r12), %rdi
	cmpq	%rdi, %rdx
	je	.L53
	subq	$16, %rdx
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	subq	%rdi, %rdx
	movq	%rdx, %r8
	shrq	$4, %r8
	addq	$1, %r8
	.p2align 4,,10
	.p2align 3
.L54:
	movdqu	(%rdi,%rcx), %xmm1
	addq	$1, %rsi
	movups	%xmm1, (%rax,%rcx)
	addq	$16, %rcx
	cmpq	%r8, %rsi
	jb	.L54
	leaq	16(%rax,%rdx), %rax
.L53:
	movq	%rax, 48(%r13)
	movq	64(%rbx), %rcx
	xorl	%edx, %edx
	movq	8(%r12), %rax
	movq	56(%rbx), %r9
	divq	%rcx
	leaq	0(,%rdx,8), %r11
	movq	%rdx, %r8
	leaq	(%r9,%r11), %r10
	movq	(%r10), %rax
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L55:
	movq	%rdx, %rdi
	movq	(%rdx), %rdx
	cmpq	%rdx, %r12
	jne	.L55
	movq	(%r12), %rsi
	cmpq	%rdi, %rax
	je	.L84
	testq	%rsi, %rsi
	je	.L58
	movq	8(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r8
	je	.L58
	movq	%rdi, (%r9,%rdx,8)
	movq	(%r12), %rsi
.L58:
	movq	%rsi, (%rdi)
	movq	56(%r12), %rdi
	testq	%rdi, %rdi
	je	.L60
.L86:
	call	_ZdlPv@PLT
.L60:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	movq	%r13, %rax
	subq	$1, 80(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore_state
	testq	%rsi, %rsi
	je	.L63
	movq	8(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r8
	je	.L58
	movq	%rdi, (%r9,%rdx,8)
	addq	56(%rbx), %r11
	movq	(%r11), %rax
	movq	%r11, %r10
.L57:
	leaq	72(%rbx), %rdx
	cmpq	%rdx, %rax
	je	.L85
.L59:
	movq	$0, (%r10)
	movq	(%r12), %rsi
	movq	%rsi, (%rdi)
	movq	56(%r12), %rdi
	testq	%rdi, %rdi
	jne	.L86
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L82:
	xorl	%eax, %eax
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L63:
	movq	%rdi, %rax
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L48:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L85:
	movq	%rsi, 72(%rbx)
	jmp	.L59
.L83:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE20046:
	.size	_ZN2v88internal4wasm17WasmMemoryTracker24ReleaseAllocation_LockedEPNS0_7IsolateEPKv, .-_ZN2v88internal4wasm17WasmMemoryTracker24ReleaseAllocation_LockedEPNS0_7IsolateEPKv
	.section	.rodata._ZN2v88internal4wasm17WasmMemoryTracker26FreeBackingStoreForTestingENS_4base13AddressRegionEPv.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"FreePages(GetPlatformPageAllocator(), reinterpret_cast<void*>(memory.begin()), memory.size())"
	.section	.text._ZN2v88internal4wasm17WasmMemoryTracker26FreeBackingStoreForTestingENS_4base13AddressRegionEPv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm17WasmMemoryTracker26FreeBackingStoreForTestingENS_4base13AddressRegionEPv
	.type	_ZN2v88internal4wasm17WasmMemoryTracker26FreeBackingStoreForTestingENS_4base13AddressRegionEPv, @function
_ZN2v88internal4wasm17WasmMemoryTracker26FreeBackingStoreForTestingENS_4base13AddressRegionEPv:
.LFB20039:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	8(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r15, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v84base5Mutex4LockEv@PLT
	leaq	-128(%rbp), %rdi
	movq	%rbx, %rcx
	xorl	%edx, %edx
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm17WasmMemoryTracker24ReleaseAllocation_LockedEPNS0_7IsolateEPKv
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L88
	call	_ZdlPv@PLT
.L88:
	call	_ZN2v88internal24GetPlatformPageAllocatorEv@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal9FreePagesEPNS_13PageAllocatorEPvm@PLT
	testb	%al, %al
	je	.L95
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L96
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	.cfi_restore_state
	leaq	.LC2(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L96:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20039:
	.size	_ZN2v88internal4wasm17WasmMemoryTracker26FreeBackingStoreForTestingENS_4base13AddressRegionEPv, .-_ZN2v88internal4wasm17WasmMemoryTracker26FreeBackingStoreForTestingENS_4base13AddressRegionEPv
	.section	.text._ZN2v88internal4wasm17WasmMemoryTracker18FindAllocationDataEPKv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm17WasmMemoryTracker18FindAllocationDataEPKv
	.type	_ZN2v88internal4wasm17WasmMemoryTracker18FindAllocationDataEPKv, @function
_ZN2v88internal4wasm17WasmMemoryTracker18FindAllocationDataEPKv:
.LFB20059:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	8(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	64(%r12), %rcx
	movq	%rbx, %rax
	xorl	%edx, %edx
	divq	%rcx
	movq	56(%r12), %rax
	movq	(%rax,%rdx,8), %r12
	testq	%r12, %r12
	je	.L98
	movq	(%r12), %r12
	movq	%rdx, %rdi
	movq	8(%r12), %rsi
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L111:
	movq	(%r12), %r12
	testq	%r12, %r12
	je	.L98
	movq	8(%r12), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rcx
	cmpq	%rdx, %rdi
	jne	.L110
.L100:
	cmpq	%rbx, %rsi
	jne	.L111
	addq	$16, %r12
.L98:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L110:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L98
	.cfi_endproc
.LFE20059:
	.size	_ZN2v88internal4wasm17WasmMemoryTracker18FindAllocationDataEPKv, .-_ZN2v88internal4wasm17WasmMemoryTracker18FindAllocationDataEPKv
	.section	.text._ZN2v88internal4wasm17WasmMemoryTracker12IsWasmMemoryEPKv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm17WasmMemoryTracker12IsWasmMemoryEPKv
	.type	_ZN2v88internal4wasm17WasmMemoryTracker12IsWasmMemoryEPKv, @function
_ZN2v88internal4wasm17WasmMemoryTracker12IsWasmMemoryEPKv:
.LFB20060:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	8(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	64(%r12), %rsi
	movq	%rbx, %rax
	xorl	%edx, %edx
	divq	%rsi
	movq	56(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L117
	movq	(%rax), %rcx
	movq	%rdx, %r8
	movq	8(%rcx), %rdi
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L120:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L117
	movq	8(%rcx), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rsi
	cmpq	%rdx, %r8
	jne	.L117
.L114:
	cmpq	%rdi, %rbx
	jne	.L120
	movl	$1, %r12d
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L117:
	xorl	%r12d, %r12d
.L113:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20060:
	.size	_ZN2v88internal4wasm17WasmMemoryTracker12IsWasmMemoryEPKv, .-_ZN2v88internal4wasm17WasmMemoryTracker12IsWasmMemoryEPKv
	.section	.text._ZN2v88internal4wasm17WasmMemoryTracker18IsWasmSharedMemoryEPKv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm17WasmMemoryTracker18IsWasmSharedMemoryEPKv
	.type	_ZN2v88internal4wasm17WasmMemoryTracker18IsWasmSharedMemoryEPKv, @function
_ZN2v88internal4wasm17WasmMemoryTracker18IsWasmSharedMemoryEPKv:
.LFB20061:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	8(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	64(%r12), %rsi
	movq	%rbx, %rax
	xorl	%edx, %edx
	divq	%rsi
	movq	56(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L126
	movq	(%rax), %rcx
	movq	%rdx, %r8
	movq	8(%rcx), %rdi
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L130:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L126
	movq	8(%rcx), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rsi
	cmpq	%rdx, %r8
	jne	.L126
.L124:
	cmpq	%rbx, %rdi
	jne	.L130
	movzbl	48(%rcx), %r12d
.L122:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L126:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L122
	.cfi_endproc
.LFE20061:
	.size	_ZN2v88internal4wasm17WasmMemoryTracker18IsWasmSharedMemoryEPKv, .-_ZN2v88internal4wasm17WasmMemoryTracker18IsWasmSharedMemoryEPKv
	.section	.text._ZN2v88internal4wasm17WasmMemoryTracker25MarkWasmMemoryNotGrowableENS0_6HandleINS0_13JSArrayBufferEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm17WasmMemoryTracker25MarkWasmMemoryNotGrowableENS0_6HandleINS0_13JSArrayBufferEEE
	.type	_ZN2v88internal4wasm17WasmMemoryTracker25MarkWasmMemoryNotGrowableENS0_6HandleINS0_13JSArrayBufferEEE, @function
_ZN2v88internal4wasm17WasmMemoryTracker25MarkWasmMemoryNotGrowableENS0_6HandleINS0_13JSArrayBufferEEE:
.LFB20062:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	8(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$8, %rsp
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	(%r12), %rax
	movq	64(%rbx), %rsi
	xorl	%edx, %edx
	movq	31(%rax), %r8
	movq	%r8, %rax
	divq	%rsi
	movq	56(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L143
	movq	(%rax), %rcx
	movq	%rdx, %r9
	movq	8(%rcx), %rdi
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L144:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L143
	movq	8(%rcx), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rsi
	cmpq	%rdx, %r9
	jne	.L143
.L134:
	cmpq	%r8, %rdi
	jne	.L144
	movb	$0, 49(%rcx)
.L143:
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.cfi_endproc
.LFE20062:
	.size	_ZN2v88internal4wasm17WasmMemoryTracker25MarkWasmMemoryNotGrowableENS0_6HandleINS0_13JSArrayBufferEEE, .-_ZN2v88internal4wasm17WasmMemoryTracker25MarkWasmMemoryNotGrowableENS0_6HandleINS0_13JSArrayBufferEEE
	.section	.text._ZN2v88internal4wasm17WasmMemoryTracker20IsWasmMemoryGrowableENS0_6HandleINS0_13JSArrayBufferEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm17WasmMemoryTracker20IsWasmMemoryGrowableENS0_6HandleINS0_13JSArrayBufferEEE
	.type	_ZN2v88internal4wasm17WasmMemoryTracker20IsWasmMemoryGrowableENS0_6HandleINS0_13JSArrayBufferEEE, @function
_ZN2v88internal4wasm17WasmMemoryTracker20IsWasmMemoryGrowableENS0_6HandleINS0_13JSArrayBufferEEE:
.LFB20063:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	8(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$8, %rsp
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	(%r12), %rax
	movl	$1, %r12d
	movq	31(%rax), %rdi
	testq	%rdi, %rdi
	jne	.L156
.L146:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L156:
	.cfi_restore_state
	movq	64(%rbx), %r8
	movq	%rdi, %rax
	xorl	%edx, %edx
	divq	%r8
	movq	56(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L151
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L157:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L151
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%r8
	cmpq	%rdx, %r9
	jne	.L151
.L148:
	cmpq	%rsi, %rdi
	jne	.L157
	movzbl	49(%rcx), %r12d
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L151:
	xorl	%r12d, %r12d
	jmp	.L146
	.cfi_endproc
.LFE20063:
	.size	_ZN2v88internal4wasm17WasmMemoryTracker20IsWasmMemoryGrowableENS0_6HandleINS0_13JSArrayBufferEEE, .-_ZN2v88internal4wasm17WasmMemoryTracker20IsWasmMemoryGrowableENS0_6HandleINS0_13JSArrayBufferEEE
	.section	.text.unlikely._ZN2v88internal4wasm17WasmMemoryTracker46TriggerSharedGrowInterruptOnAllIsolates_LockedENS0_6HandleINS0_13JSArrayBufferEEE,"ax",@progbits
	.align 2
.LCOLDB3:
	.section	.text._ZN2v88internal4wasm17WasmMemoryTracker46TriggerSharedGrowInterruptOnAllIsolates_LockedENS0_6HandleINS0_13JSArrayBufferEEE,"ax",@progbits
.LHOTB3:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm17WasmMemoryTracker46TriggerSharedGrowInterruptOnAllIsolates_LockedENS0_6HandleINS0_13JSArrayBufferEEE
	.type	_ZN2v88internal4wasm17WasmMemoryTracker46TriggerSharedGrowInterruptOnAllIsolates_LockedENS0_6HandleINS0_13JSArrayBufferEEE, @function
_ZN2v88internal4wasm17WasmMemoryTracker46TriggerSharedGrowInterruptOnAllIsolates_LockedENS0_6HandleINS0_13JSArrayBufferEEE:
.LFB20070:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	movq	120(%rdi), %r8
	xorl	%edx, %edx
	movq	31(%rax), %r9
	movq	%r9, %rax
	divq	%r8
	movq	112(%rdi), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L159
	movq	(%rax), %rcx
	movq	%rdx, %r10
	movq	8(%rcx), %rsi
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L175:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L159
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%r8
	cmpq	%rdx, %r10
	jne	.L174
.L161:
	cmpq	%r9, %rsi
	jne	.L175
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	32(%rcx), %rbx
	testq	%rbx, %rbx
	je	.L158
	.p2align 4,,10
	.p2align 3
.L162:
	movq	8(%rbx), %rax
	movl	$32, %esi
	leaq	37512(%rax), %rdi
	call	_ZN2v88internal10StackGuard16RequestInterruptENS1_13InterruptFlagE@PLT
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L162
.L158:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
.L174:
	.cfi_restore 3
	.cfi_restore 6
	jmp	.L159
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal4wasm17WasmMemoryTracker46TriggerSharedGrowInterruptOnAllIsolates_LockedENS0_6HandleINS0_13JSArrayBufferEEE
	.cfi_startproc
	.type	_ZN2v88internal4wasm17WasmMemoryTracker46TriggerSharedGrowInterruptOnAllIsolates_LockedENS0_6HandleINS0_13JSArrayBufferEEE.cold, @function
_ZN2v88internal4wasm17WasmMemoryTracker46TriggerSharedGrowInterruptOnAllIsolates_LockedENS0_6HandleINS0_13JSArrayBufferEEE.cold:
.LFSB20070:
.L159:
	movq	32, %rax
	ud2
	.cfi_endproc
.LFE20070:
	.section	.text._ZN2v88internal4wasm17WasmMemoryTracker46TriggerSharedGrowInterruptOnAllIsolates_LockedENS0_6HandleINS0_13JSArrayBufferEEE
	.size	_ZN2v88internal4wasm17WasmMemoryTracker46TriggerSharedGrowInterruptOnAllIsolates_LockedENS0_6HandleINS0_13JSArrayBufferEEE, .-_ZN2v88internal4wasm17WasmMemoryTracker46TriggerSharedGrowInterruptOnAllIsolates_LockedENS0_6HandleINS0_13JSArrayBufferEEE
	.section	.text.unlikely._ZN2v88internal4wasm17WasmMemoryTracker46TriggerSharedGrowInterruptOnAllIsolates_LockedENS0_6HandleINS0_13JSArrayBufferEEE
	.size	_ZN2v88internal4wasm17WasmMemoryTracker46TriggerSharedGrowInterruptOnAllIsolates_LockedENS0_6HandleINS0_13JSArrayBufferEEE.cold, .-_ZN2v88internal4wasm17WasmMemoryTracker46TriggerSharedGrowInterruptOnAllIsolates_LockedENS0_6HandleINS0_13JSArrayBufferEEE.cold
.LCOLDE3:
	.section	.text._ZN2v88internal4wasm17WasmMemoryTracker46TriggerSharedGrowInterruptOnAllIsolates_LockedENS0_6HandleINS0_13JSArrayBufferEEE
.LHOTE3:
	.section	.text._ZN2v88internal4wasm17WasmMemoryTracker41ClearUpdatedInstancesOnPendingGrow_LockedEPKv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm17WasmMemoryTracker41ClearUpdatedInstancesOnPendingGrow_LockedEPKv
	.type	_ZN2v88internal4wasm17WasmMemoryTracker41ClearUpdatedInstancesOnPendingGrow_LockedEPKv, @function
_ZN2v88internal4wasm17WasmMemoryTracker41ClearUpdatedInstancesOnPendingGrow_LockedEPKv:
.LFB20073:
	.cfi_startproc
	endbr64
	movq	176(%rdi), %rcx
	movq	%rsi, %rax
	xorl	%edx, %edx
	divq	%rcx
	movq	168(%rdi), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L195
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rax), %rbx
	movq	8(%rbx), %rdi
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L198:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L176
	movq	8(%rbx), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rcx
	cmpq	%rdx, %r8
	jne	.L176
.L179:
	cmpq	%rdi, %rsi
	jne	.L198
	movq	32(%rbx), %r12
	testq	%r12, %r12
	je	.L180
	.p2align 4,,10
	.p2align 3
.L181:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L181
.L180:
	movq	24(%rbx), %rax
	movq	16(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	$0, 40(%rbx)
	movq	$0, 32(%rbx)
.L176:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L195:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE20073:
	.size	_ZN2v88internal4wasm17WasmMemoryTracker41ClearUpdatedInstancesOnPendingGrow_LockedEPKv, .-_ZN2v88internal4wasm17WasmMemoryTracker41ClearUpdatedInstancesOnPendingGrow_LockedEPKv
	.section	.text._ZN2v88internal4wasm17WasmMemoryTracker36UpdateMemoryObjectsForIsolate_LockedEPNS0_7IsolateEPvm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm17WasmMemoryTracker36UpdateMemoryObjectsForIsolate_LockedEPNS0_7IsolateEPvm
	.type	_ZN2v88internal4wasm17WasmMemoryTracker36UpdateMemoryObjectsForIsolate_LockedEPNS0_7IsolateEPvm, @function
_ZN2v88internal4wasm17WasmMemoryTracker36UpdateMemoryObjectsForIsolate_LockedEPNS0_7IsolateEPvm:
.LFB20074:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	64(%rdi), %r8
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdx, %rax
	xorl	%edx, %edx
	divq	%r8
	movq	56(%rdi), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L199
	movq	%rcx, %r15
	movq	(%rax), %rcx
	movq	%rsi, %r12
	movq	%rdx, %r9
	movq	8(%rcx), %rsi
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L214:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L199
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%r8
	cmpq	%rdx, %r9
	jne	.L199
.L203:
	cmpq	%r13, %rsi
	jne	.L214
	cmpb	$0, 48(%rcx)
	je	.L199
	movq	56(%rcx), %rbx
	movq	64(%rcx), %r14
	cmpq	%rbx, %r14
	je	.L199
	leaq	-64(%rbp), %rax
	movq	%rax, -96(%rbp)
	movq	%r13, %rax
	movq	%r14, %r13
	movq	%rax, %r14
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L204:
	addq	$16, %rbx
	cmpq	%rbx, %r13
	je	.L199
.L206:
	cmpq	%r12, 8(%rbx)
	jne	.L204
	movq	41088(%r12), %rax
	movq	41096(%r12), %r11
	movl	$1, %edx
	movq	%r12, %rdi
	addl	$1, 41104(%r12)
	movl	$1, %esi
	movq	%rax, -72(%rbp)
	movq	(%rbx), %rax
	movq	%r11, -88(%rbp)
	movq	(%rax), %rax
	movq	23(%rax), %rax
	movl	39(%rax), %eax
	movl	%eax, -80(%rbp)
	call	_ZN2v88internal7Factory16NewJSArrayBufferENS0_10SharedFlagENS0_14AllocationTypeE@PLT
	subq	$8, %rsp
	movl	-80(%rbp), %edx
	movq	%r15, %r8
	pushq	$1
	movq	%rax, %rdi
	movl	$1, %r9d
	movq	%r14, %rcx
	andl	$1, %edx
	movq	%r12, %rsi
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal13JSArrayBuffer5SetupENS0_6HandleIS1_EEPNS0_7IsolateEbPvmNS0_10SharedFlagEb@PLT
	movq	-80(%rbp), %r10
	movq	%r12, %rsi
	movq	(%r10), %rcx
	movl	39(%rcx), %edx
	andl	$-3, %edx
	movl	%edx, 39(%rcx)
	movq	(%rbx), %rdx
	movq	-96(%rbp), %rdi
	movq	(%rdx), %rdx
	movq	%rdx, -64(%rbp)
	movq	%r10, %rdx
	call	_ZN2v88internal16WasmMemoryObject16update_instancesEPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEE@PLT
	movq	-72(%rbp), %rax
	movq	-88(%rbp), %r11
	subl	$1, 41104(%r12)
	movq	%rax, 41088(%r12)
	popq	%rax
	popq	%rdx
	cmpq	41096(%r12), %r11
	je	.L204
	movq	%r11, 41096(%r12)
	movq	%r12, %rdi
	addq	$16, %rbx
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	cmpq	%rbx, %r13
	jne	.L206
	.p2align 4,,10
	.p2align 3
.L199:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L215
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L215:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20074:
	.size	_ZN2v88internal4wasm17WasmMemoryTracker36UpdateMemoryObjectsForIsolate_LockedEPNS0_7IsolateEPvm, .-_ZN2v88internal4wasm17WasmMemoryTracker36UpdateMemoryObjectsForIsolate_LockedEPNS0_7IsolateEPvm
	.section	.text._ZN2v88internal4wasm17WasmMemoryTracker30MemoryObjectsNeedUpdate_LockedEPNS0_7IsolateEPKv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm17WasmMemoryTracker30MemoryObjectsNeedUpdate_LockedEPNS0_7IsolateEPKv
	.type	_ZN2v88internal4wasm17WasmMemoryTracker30MemoryObjectsNeedUpdate_LockedEPNS0_7IsolateEPKv, @function
_ZN2v88internal4wasm17WasmMemoryTracker30MemoryObjectsNeedUpdate_LockedEPNS0_7IsolateEPKv:
.LFB20075:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %r9
	movq	%rdx, %rax
	movq	%rdx, %rcx
	xorl	%edx, %edx
	divq	%r9
	movq	56(%rdi), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r11
	testq	%rax, %rax
	je	.L240
	movq	(%rax), %r8
	movq	8(%r8), %r10
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L241:
	movq	(%r8), %r8
	testq	%r8, %r8
	je	.L240
	movq	8(%r8), %r10
	xorl	%edx, %edx
	movq	%r10, %rax
	divq	%r9
	cmpq	%rdx, %r11
	jne	.L240
.L219:
	cmpq	%rcx, %r10
	jne	.L241
	movzbl	48(%r8), %r9d
	testb	%r9b, %r9b
	je	.L216
	movq	56(%r8), %rax
	cmpq	%rax, 64(%r8)
	je	.L240
	movq	120(%rdi), %r10
	movq	%rcx, %rax
	xorl	%edx, %edx
	divq	%r10
	movq	112(%rdi), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r11
	testq	%rax, %rax
	je	.L240
	movq	(%rax), %rdi
	movq	8(%rdi), %r8
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L242:
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L240
	movq	8(%rdi), %r8
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	%r10
	cmpq	%rdx, %r11
	jne	.L240
.L221:
	cmpq	%r8, %rcx
	jne	.L242
	movq	24(%rdi), %r8
	movq	%rsi, %rax
	xorl	%edx, %edx
	divq	%r8
	movq	16(%rdi), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r11
	testq	%rax, %rax
	je	.L240
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	je	.L240
	movq	8(%rcx), %rdi
	xorl	%r10d, %r10d
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L222:
	testq	%r10, %r10
	jne	.L216
.L223:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L224
	movq	8(%rcx), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%r8
	cmpq	%rdx, %r11
	jne	.L224
.L225:
	cmpq	%rdi, %rsi
	jne	.L222
	addq	$1, %r10
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L240:
	xorl	%r9d, %r9d
.L216:
	movl	%r9d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L224:
	testq	%r10, %r10
	setne	%r9b
	movl	%r9d, %eax
	ret
	.cfi_endproc
.LFE20075:
	.size	_ZN2v88internal4wasm17WasmMemoryTracker30MemoryObjectsNeedUpdate_LockedEPNS0_7IsolateEPKv, .-_ZN2v88internal4wasm17WasmMemoryTracker30MemoryObjectsNeedUpdate_LockedEPNS0_7IsolateEPKv
	.section	.text._ZN2v88internal4wasm17WasmMemoryTracker26CanFreeSharedMemory_LockedEPKv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm17WasmMemoryTracker26CanFreeSharedMemory_LockedEPKv
	.type	_ZN2v88internal4wasm17WasmMemoryTracker26CanFreeSharedMemory_LockedEPKv, @function
_ZN2v88internal4wasm17WasmMemoryTracker26CanFreeSharedMemory_LockedEPKv:
.LFB20077:
	.cfi_startproc
	endbr64
	movq	120(%rdi), %r8
	movq	%rsi, %rax
	xorl	%edx, %edx
	divq	%r8
	movq	112(%rdi), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L248
	movq	(%rax), %rcx
	movq	8(%rcx), %rdi
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L250:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L248
	movq	8(%rcx), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%r8
	cmpq	%rdx, %r9
	jne	.L248
.L246:
	cmpq	%rsi, %rdi
	jne	.L250
	cmpq	$0, 40(%rcx)
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L248:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE20077:
	.size	_ZN2v88internal4wasm17WasmMemoryTracker26CanFreeSharedMemory_LockedEPKv, .-_ZN2v88internal4wasm17WasmMemoryTracker26CanFreeSharedMemory_LockedEPKv
	.section	.rodata._ZN2v88internal4wasm17WasmMemoryTracker48DestroyMemoryObjectsAndRemoveIsolateEntry_LockedEPNS0_7IsolateEPKv.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"result != allocations_.end() && result->second.is_shared"
	.section	.text._ZN2v88internal4wasm17WasmMemoryTracker48DestroyMemoryObjectsAndRemoveIsolateEntry_LockedEPNS0_7IsolateEPKv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm17WasmMemoryTracker48DestroyMemoryObjectsAndRemoveIsolateEntry_LockedEPNS0_7IsolateEPKv
	.type	_ZN2v88internal4wasm17WasmMemoryTracker48DestroyMemoryObjectsAndRemoveIsolateEntry_LockedEPNS0_7IsolateEPKv, @function
_ZN2v88internal4wasm17WasmMemoryTracker48DestroyMemoryObjectsAndRemoveIsolateEntry_LockedEPNS0_7IsolateEPKv:
.LFB20080:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rax
	movq	%rdx, %r8
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	64(%rdi), %rsi
	divq	%rsi
	movq	56(%rdi), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L252
	movq	(%rax), %rbx
	movq	%rdx, %r9
	movq	8(%rbx), %rcx
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L269:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L252
	movq	8(%rbx), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%rdx, %r9
	jne	.L252
.L254:
	cmpq	%r8, %rcx
	jne	.L269
	cmpb	$0, 48(%rbx)
	je	.L252
	movq	64(%rbx), %rax
	movq	56(%rbx), %r12
	cmpq	%r12, %rax
	jne	.L256
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L260:
	movq	%r13, %r12
	cmpq	%rax, %r12
	je	.L251
.L256:
	leaq	16(%r12), %r13
	cmpq	%r14, 8(%r12)
	jne	.L260
	movq	(%r12), %rdi
	call	_ZN2v88internal13GlobalHandles7DestroyEPm@PLT
	movq	64(%rbx), %rdx
	cmpq	%r13, %rdx
	je	.L258
	subq	%r13, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	memmove@PLT
	movq	64(%rbx), %r13
.L258:
	leaq	-16(%r13), %rax
	movq	%rax, 64(%rbx)
	cmpq	%rax, %r12
	jne	.L256
.L251:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L252:
	.cfi_restore_state
	leaq	.LC4(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20080:
	.size	_ZN2v88internal4wasm17WasmMemoryTracker48DestroyMemoryObjectsAndRemoveIsolateEntry_LockedEPNS0_7IsolateEPKv, .-_ZN2v88internal4wasm17WasmMemoryTracker48DestroyMemoryObjectsAndRemoveIsolateEntry_LockedEPNS0_7IsolateEPKv
	.section	.text._ZN2v88internal4wasm16SetupArrayBufferEPNS0_7IsolateEPvmbNS0_10SharedFlagE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm16SetupArrayBufferEPNS0_7IsolateEPvmbNS0_10SharedFlagE
	.type	_ZN2v88internal4wasm16SetupArrayBufferEPNS0_7IsolateEPvmbNS0_10SharedFlagE, @function
_ZN2v88internal4wasm16SetupArrayBufferEPNS0_7IsolateEPvmbNS0_10SharedFlagE:
.LFB20083:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movl	%r8d, %esi
	pushq	%r14
	.cfi_offset 14, -32
	movl	%r8d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	movl	$1, %edx
	subq	$24, %rsp
	movl	%ecx, -52(%rbp)
	call	_ZN2v88internal7Factory16NewJSArrayBufferENS0_10SharedFlagENS0_14AllocationTypeE@PLT
	subq	$8, %rsp
	movzbl	-52(%rbp), %edx
	movl	%r14d, %r9d
	pushq	$1
	movq	%rax, %r12
	movq	%rbx, %r8
	movq	%r15, %rcx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal13JSArrayBuffer5SetupENS0_6HandleIS1_EEPNS0_7IsolateEbPvmNS0_10SharedFlagEb@PLT
	movq	(%r12), %rdx
	movl	39(%rdx), %eax
	andl	$-3, %eax
	movl	%eax, 39(%rdx)
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20083:
	.size	_ZN2v88internal4wasm16SetupArrayBufferEPNS0_7IsolateEPvmbNS0_10SharedFlagE, .-_ZN2v88internal4wasm16SetupArrayBufferEPNS0_7IsolateEPvmbNS0_10SharedFlagE
	.section	.text._ZN2v88internal4wasm18DetachMemoryBufferEPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEb,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm18DetachMemoryBufferEPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEb
	.type	_ZN2v88internal4wasm18DetachMemoryBufferEPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEb, @function
_ZN2v88internal4wasm18DetachMemoryBufferEPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEb:
.LFB20090:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movl	39(%rax), %ecx
	andl	$8, %ecx
	je	.L282
.L272:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L283
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L282:
	.cfi_restore_state
	movl	39(%rax), %ecx
	movq	%rsi, %rbx
	andl	$1, %ecx
	je	.L274
.L281:
	leaq	-32(%rbp), %rdi
.L275:
	movl	39(%rax), %edx
	andl	$-17, %edx
	movl	%edx, 39(%rax)
	movq	(%rbx), %rdx
	movl	39(%rdx), %eax
	orl	$2, %eax
	movl	%eax, 39(%rdx)
	movq	(%rbx), %rax
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal13JSArrayBuffer6DetachEv@PLT
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L274:
	movl	%edx, -40(%rbp)
	movl	39(%rax), %ecx
	addq	$37592, %rdi
	orl	$1, %ecx
	movl	%ecx, 39(%rax)
	movq	(%rsi), %rsi
	call	_ZN2v88internal4Heap21UnregisterArrayBufferENS0_13JSArrayBufferE@PLT
	movl	-40(%rbp), %edx
	movq	(%rbx), %rax
	testb	%dl, %dl
	je	.L281
	leaq	-32(%rbp), %rdi
	movq	%rax, -32(%rbp)
	movq	%rdi, -40(%rbp)
	call	_ZN2v88internal13JSArrayBuffer30FreeBackingStoreFromMainThreadEv@PLT
	movq	(%rbx), %rax
	movq	-40(%rbp), %rdi
	jmp	.L275
.L283:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20090:
	.size	_ZN2v88internal4wasm18DetachMemoryBufferEPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEb, .-_ZN2v88internal4wasm18DetachMemoryBufferEPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEb
	.section	.text._ZNSt10_HashtableIPKvSt4pairIKS1_St13unordered_setIPN2v88internal7IsolateESt4hashIS8_ESt8equal_toIS8_ESaIS8_EEESaISF_ENSt8__detail10_Select1stESB_IS1_ES9_IS1_ENSH_18_Mod_range_hashingENSH_20_Default_ranged_hashENSH_20_Prime_rehash_policyENSH_17_Hashtable_traitsILb0ELb0ELb1EEEED2Ev,"axG",@progbits,_ZNSt10_HashtableIPKvSt4pairIKS1_St13unordered_setIPN2v88internal7IsolateESt4hashIS8_ESt8equal_toIS8_ESaIS8_EEESaISF_ENSt8__detail10_Select1stESB_IS1_ES9_IS1_ENSH_18_Mod_range_hashingENSH_20_Default_ranged_hashENSH_20_Prime_rehash_policyENSH_17_Hashtable_traitsILb0ELb0ELb1EEEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPKvSt4pairIKS1_St13unordered_setIPN2v88internal7IsolateESt4hashIS8_ESt8equal_toIS8_ESaIS8_EEESaISF_ENSt8__detail10_Select1stESB_IS1_ES9_IS1_ENSH_18_Mod_range_hashingENSH_20_Default_ranged_hashENSH_20_Prime_rehash_policyENSH_17_Hashtable_traitsILb0ELb0ELb1EEEED2Ev
	.type	_ZNSt10_HashtableIPKvSt4pairIKS1_St13unordered_setIPN2v88internal7IsolateESt4hashIS8_ESt8equal_toIS8_ESaIS8_EEESaISF_ENSt8__detail10_Select1stESB_IS1_ES9_IS1_ENSH_18_Mod_range_hashingENSH_20_Default_ranged_hashENSH_20_Prime_rehash_policyENSH_17_Hashtable_traitsILb0ELb0ELb1EEEED2Ev, @function
_ZNSt10_HashtableIPKvSt4pairIKS1_St13unordered_setIPN2v88internal7IsolateESt4hashIS8_ESt8equal_toIS8_ESaIS8_EEESaISF_ENSt8__detail10_Select1stESB_IS1_ES9_IS1_ENSH_18_Mod_range_hashingENSH_20_Default_ranged_hashENSH_20_Prime_rehash_policyENSH_17_Hashtable_traitsILb0ELb0ELb1EEEED2Ev:
.LFB22216:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	16(%rdi), %r12
	movq	%rdi, %rbx
	testq	%r12, %r12
	je	.L285
	.p2align 4,,10
	.p2align 3
.L290:
	movq	32(%r12), %r13
	movq	(%r12), %r14
	testq	%r13, %r13
	je	.L286
	.p2align 4,,10
	.p2align 3
.L287:
	movq	%r13, %rdi
	movq	0(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L287
.L286:
	movq	24(%r12), %rax
	movq	16(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	16(%r12), %rdi
	leaq	64(%r12), %rax
	movq	$0, 40(%r12)
	movq	$0, 32(%r12)
	cmpq	%rax, %rdi
	je	.L288
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r14, %r14
	je	.L285
.L289:
	movq	%r14, %r12
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L288:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r14, %r14
	jne	.L289
	.p2align 4,,10
	.p2align 3
.L285:
	movq	8(%rbx), %rax
	movq	(%rbx), %rdi
	xorl	%esi, %esi
	addq	$48, %rbx
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-48(%rbx), %rdi
	movq	$0, -24(%rbx)
	movq	$0, -32(%rbx)
	cmpq	%rbx, %rdi
	je	.L284
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L284:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22216:
	.size	_ZNSt10_HashtableIPKvSt4pairIKS1_St13unordered_setIPN2v88internal7IsolateESt4hashIS8_ESt8equal_toIS8_ESaIS8_EEESaISF_ENSt8__detail10_Select1stESB_IS1_ES9_IS1_ENSH_18_Mod_range_hashingENSH_20_Default_ranged_hashENSH_20_Prime_rehash_policyENSH_17_Hashtable_traitsILb0ELb0ELb1EEEED2Ev, .-_ZNSt10_HashtableIPKvSt4pairIKS1_St13unordered_setIPN2v88internal7IsolateESt4hashIS8_ESt8equal_toIS8_ESaIS8_EEESaISF_ENSt8__detail10_Select1stESB_IS1_ES9_IS1_ENSH_18_Mod_range_hashingENSH_20_Default_ranged_hashENSH_20_Prime_rehash_policyENSH_17_Hashtable_traitsILb0ELb0ELb1EEEED2Ev
	.weak	_ZNSt10_HashtableIPKvSt4pairIKS1_St13unordered_setIPN2v88internal7IsolateESt4hashIS8_ESt8equal_toIS8_ESaIS8_EEESaISF_ENSt8__detail10_Select1stESB_IS1_ES9_IS1_ENSH_18_Mod_range_hashingENSH_20_Default_ranged_hashENSH_20_Prime_rehash_policyENSH_17_Hashtable_traitsILb0ELb0ELb1EEEED1Ev
	.set	_ZNSt10_HashtableIPKvSt4pairIKS1_St13unordered_setIPN2v88internal7IsolateESt4hashIS8_ESt8equal_toIS8_ESaIS8_EEESaISF_ENSt8__detail10_Select1stESB_IS1_ES9_IS1_ENSH_18_Mod_range_hashingENSH_20_Default_ranged_hashENSH_20_Prime_rehash_policyENSH_17_Hashtable_traitsILb0ELb0ELb1EEEED1Ev,_ZNSt10_HashtableIPKvSt4pairIKS1_St13unordered_setIPN2v88internal7IsolateESt4hashIS8_ESt8equal_toIS8_ESaIS8_EEESaISF_ENSt8__detail10_Select1stESB_IS1_ES9_IS1_ENSH_18_Mod_range_hashingENSH_20_Default_ranged_hashENSH_20_Prime_rehash_policyENSH_17_Hashtable_traitsILb0ELb0ELb1EEEED2Ev
	.section	.text._ZN2v88internal4wasm17WasmMemoryTrackerD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm17WasmMemoryTrackerD2Ev
	.type	_ZN2v88internal4wasm17WasmMemoryTrackerD2Ev, @function
_ZN2v88internal4wasm17WasmMemoryTrackerD2Ev:
.LFB20036:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	240(%rdi), %r12
	testq	%r12, %r12
	je	.L307
	.p2align 4,,10
	.p2align 3
.L308:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L308
.L307:
	movq	232(%rbx), %rax
	movq	224(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	224(%rbx), %rdi
	leaq	272(%rbx), %rax
	movq	$0, 248(%rbx)
	movq	$0, 240(%rbx)
	cmpq	%rax, %rdi
	je	.L309
	call	_ZdlPv@PLT
.L309:
	leaq	168(%rbx), %rdi
	call	_ZNSt10_HashtableIPKvSt4pairIKS1_St13unordered_setIPN2v88internal7IsolateESt4hashIS8_ESt8equal_toIS8_ESaIS8_EEESaISF_ENSt8__detail10_Select1stESB_IS1_ES9_IS1_ENSH_18_Mod_range_hashingENSH_20_Default_ranged_hashENSH_20_Prime_rehash_policyENSH_17_Hashtable_traitsILb0ELb0ELb1EEEED1Ev
	leaq	112(%rbx), %rdi
	call	_ZNSt10_HashtableIPKvSt4pairIKS1_St13unordered_setIPN2v88internal7IsolateESt4hashIS8_ESt8equal_toIS8_ESaIS8_EEESaISF_ENSt8__detail10_Select1stESB_IS1_ES9_IS1_ENSH_18_Mod_range_hashingENSH_20_Default_ranged_hashENSH_20_Prime_rehash_policyENSH_17_Hashtable_traitsILb0ELb0ELb1EEEED1Ev
	movq	72(%rbx), %r12
	testq	%r12, %r12
	jne	.L313
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L329:
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L310
.L312:
	movq	%r13, %r12
.L313:
	movq	56(%r12), %rdi
	movq	(%r12), %r13
	testq	%rdi, %rdi
	jne	.L329
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L312
.L310:
	movq	64(%rbx), %rax
	movq	56(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	56(%rbx), %rdi
	leaq	104(%rbx), %rax
	movq	$0, 80(%rbx)
	movq	$0, 72(%rbx)
	cmpq	%rax, %rdi
	je	.L314
	call	_ZdlPv@PLT
.L314:
	addq	$8, %rsp
	leaq	8(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5MutexD1Ev@PLT
	.cfi_endproc
.LFE20036:
	.size	_ZN2v88internal4wasm17WasmMemoryTrackerD2Ev, .-_ZN2v88internal4wasm17WasmMemoryTrackerD2Ev
	.globl	_ZN2v88internal4wasm17WasmMemoryTrackerD1Ev
	.set	_ZN2v88internal4wasm17WasmMemoryTrackerD1Ev,_ZN2v88internal4wasm17WasmMemoryTrackerD2Ev
	.section	.text._ZNSt10_HashtableIPKvSt4pairIKS1_St13unordered_setIPN2v88internal7IsolateESt4hashIS8_ESt8equal_toIS8_ESaIS8_EEESaISF_ENSt8__detail10_Select1stESB_IS1_ES9_IS1_ENSH_18_Mod_range_hashingENSH_20_Default_ranged_hashENSH_20_Prime_rehash_policyENSH_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSH_10_Hash_nodeISF_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIPKvSt4pairIKS1_St13unordered_setIPN2v88internal7IsolateESt4hashIS8_ESt8equal_toIS8_ESaIS8_EEESaISF_ENSt8__detail10_Select1stESB_IS1_ES9_IS1_ENSH_18_Mod_range_hashingENSH_20_Default_ranged_hashENSH_20_Prime_rehash_policyENSH_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSH_10_Hash_nodeISF_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPKvSt4pairIKS1_St13unordered_setIPN2v88internal7IsolateESt4hashIS8_ESt8equal_toIS8_ESaIS8_EEESaISF_ENSt8__detail10_Select1stESB_IS1_ES9_IS1_ENSH_18_Mod_range_hashingENSH_20_Default_ranged_hashENSH_20_Prime_rehash_policyENSH_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSH_10_Hash_nodeISF_Lb0EEEm
	.type	_ZNSt10_HashtableIPKvSt4pairIKS1_St13unordered_setIPN2v88internal7IsolateESt4hashIS8_ESt8equal_toIS8_ESaIS8_EEESaISF_ENSt8__detail10_Select1stESB_IS1_ES9_IS1_ENSH_18_Mod_range_hashingENSH_20_Default_ranged_hashENSH_20_Prime_rehash_policyENSH_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSH_10_Hash_nodeISF_Lb0EEEm, @function
_ZNSt10_HashtableIPKvSt4pairIKS1_St13unordered_setIPN2v88internal7IsolateESt4hashIS8_ESt8equal_toIS8_ESaIS8_EEESaISF_ENSt8__detail10_Select1stESB_IS1_ES9_IS1_ENSH_18_Mod_range_hashingENSH_20_Default_ranged_hashENSH_20_Prime_rehash_policyENSH_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSH_10_Hash_nodeISF_Lb0EEEm:
.LFB24503:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L331
	movq	(%rbx), %r8
.L332:
	movq	(%r8,%r15,8), %rax
	leaq	0(,%r15,8), %rcx
	testq	%rax, %rax
	je	.L341
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r13, (%rax)
.L342:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L331:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L355
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L356
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%rbx), %r10
	movq	%rax, %r8
.L334:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L336
	xorl	%edi, %edi
	leaq	16(%rbx), %r9
	jmp	.L337
	.p2align 4,,10
	.p2align 3
.L338:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L339:
	testq	%rsi, %rsi
	je	.L336
.L337:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	8(%rcx), %rax
	divq	%r12
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L338
	movq	16(%rbx), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L344
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L337
	.p2align 4,,10
	.p2align 3
.L336:
	movq	(%rbx), %rdi
	cmpq	%r10, %rdi
	je	.L340
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L340:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	divq	%r12
	movq	%r8, (%rbx)
	movq	%rdx, %r15
	jmp	.L332
	.p2align 4,,10
	.p2align 3
.L341:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L343
	movq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r13, (%rax,%rdx,8)
.L343:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L344:
	movq	%rdx, %rdi
	jmp	.L339
	.p2align 4,,10
	.p2align 3
.L355:
	leaq	48(%rbx), %r8
	movq	$0, 48(%rbx)
	movq	%r8, %r10
	jmp	.L334
.L356:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE24503:
	.size	_ZNSt10_HashtableIPKvSt4pairIKS1_St13unordered_setIPN2v88internal7IsolateESt4hashIS8_ESt8equal_toIS8_ESaIS8_EEESaISF_ENSt8__detail10_Select1stESB_IS1_ES9_IS1_ENSH_18_Mod_range_hashingENSH_20_Default_ranged_hashENSH_20_Prime_rehash_policyENSH_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSH_10_Hash_nodeISF_Lb0EEEm, .-_ZNSt10_HashtableIPKvSt4pairIKS1_St13unordered_setIPN2v88internal7IsolateESt4hashIS8_ESt8equal_toIS8_ESaIS8_EEESaISF_ENSt8__detail10_Select1stESB_IS1_ES9_IS1_ENSH_18_Mod_range_hashingENSH_20_Default_ranged_hashENSH_20_Prime_rehash_policyENSH_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSH_10_Hash_nodeISF_Lb0EEEm
	.section	.rodata._ZNSt6vectorIN2v88internal4wasm17WasmMemoryTracker23SharedMemoryObjectStateESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_.str1.1,"aMS",@progbits,1
.LC5:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIN2v88internal4wasm17WasmMemoryTracker23SharedMemoryObjectStateESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal4wasm17WasmMemoryTracker23SharedMemoryObjectStateESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal4wasm17WasmMemoryTracker23SharedMemoryObjectStateESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal4wasm17WasmMemoryTracker23SharedMemoryObjectStateESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, @function
_ZNSt6vectorIN2v88internal4wasm17WasmMemoryTracker23SharedMemoryObjectStateESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_:
.LFB24518:
	.cfi_startproc
	endbr64
	movabsq	$576460752303423487, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r8
	movq	(%rdi), %r13
	movq	%r8, %rax
	subq	%r13, %rax
	sarq	$4, %rax
	cmpq	%rcx, %rax
	je	.L378
	movq	%rsi, %r9
	movq	%rdi, %r15
	movq	%rsi, %r12
	subq	%r13, %r9
	testq	%rax, %rax
	je	.L369
	movabsq	$9223372036854775792, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L379
.L359:
	movq	%r14, %rdi
	movq	%rdx, -72(%rbp)
	movq	%r9, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_Znwm@PLT
	movq	-72(%rbp), %rdx
	movq	-64(%rbp), %r9
	movq	%rax, %rbx
	leaq	(%rax,%r14), %rsi
	movq	-56(%rbp), %r8
	leaq	16(%rax), %r14
	movdqu	(%rdx), %xmm3
	movups	%xmm3, (%rbx,%r9)
	cmpq	%r13, %r12
	je	.L361
.L380:
	leaq	-16(%r12), %rdi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	subq	%r13, %rdi
	movq	%rdi, %rax
	shrq	$4, %rax
	addq	$1, %rax
	.p2align 4,,10
	.p2align 3
.L362:
	movdqu	0(%r13,%rdx), %xmm1
	addq	$1, %rcx
	movups	%xmm1, (%rbx,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rax
	ja	.L362
	leaq	32(%rbx,%rdi), %r14
	cmpq	%r8, %r12
	je	.L363
.L364:
	subq	%r12, %r8
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	leaq	-16(%r8), %rdi
	movq	%rdi, %rax
	shrq	$4, %rax
	addq	$1, %rax
	.p2align 4,,10
	.p2align 3
.L366:
	movdqu	(%r12,%rdx), %xmm2
	addq	$1, %rcx
	movups	%xmm2, (%r14,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rax
	ja	.L366
	leaq	16(%r14,%rdi), %r14
.L365:
	testq	%r13, %r13
	jne	.L363
.L367:
	movq	%rbx, %xmm0
	movq	%r14, %xmm4
	movq	%rsi, 16(%r15)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, (%r15)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L363:
	.cfi_restore_state
	movq	%r13, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L379:
	testq	%rsi, %rsi
	jne	.L360
	movdqu	(%rdx), %xmm3
	xorl	%ebx, %ebx
	movl	$16, %r14d
	movups	%xmm3, (%rbx,%r9)
	cmpq	%r13, %r12
	jne	.L380
.L361:
	cmpq	%r8, %r12
	jne	.L364
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L369:
	movl	$16, %r14d
	jmp	.L359
.L360:
	cmpq	%rcx, %rsi
	cmova	%rcx, %rsi
	salq	$4, %rsi
	movq	%rsi, %r14
	jmp	.L359
.L378:
	leaq	.LC5(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE24518:
	.size	_ZNSt6vectorIN2v88internal4wasm17WasmMemoryTracker23SharedMemoryObjectStateESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, .-_ZNSt6vectorIN2v88internal4wasm17WasmMemoryTracker23SharedMemoryObjectStateESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.section	.text._ZN2v88internal4wasm17WasmMemoryTracker31RegisterSharedWasmMemory_LockedENS0_6HandleINS0_16WasmMemoryObjectEEEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm17WasmMemoryTracker31RegisterSharedWasmMemory_LockedENS0_6HandleINS0_16WasmMemoryObjectEEEPNS0_7IsolateE
	.type	_ZN2v88internal4wasm17WasmMemoryTracker31RegisterSharedWasmMemory_LockedENS0_6HandleINS0_16WasmMemoryObjectEEEPNS0_7IsolateE, @function
_ZN2v88internal4wasm17WasmMemoryTracker31RegisterSharedWasmMemory_LockedENS0_6HandleINS0_16WasmMemoryObjectEEEPNS0_7IsolateE:
.LFB20068:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	xorl	%edx, %edx
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -32
	movq	(%rsi), %r9
	movq	64(%rdi), %rcx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	23(%r9), %rax
	movq	31(%rax), %r8
	movq	%r8, %rax
	divq	%rcx
	movq	56(%rdi), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L381
	movq	(%rax), %rbx
	movq	%rdx, %r10
	movq	8(%rbx), %rdi
	jmp	.L384
	.p2align 4,,10
	.p2align 3
.L398:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L381
	movq	8(%rbx), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rcx
	cmpq	%rdx, %r10
	jne	.L381
.L384:
	cmpq	%r8, %rdi
	jne	.L398
	cmpb	$0, 48(%rbx)
	je	.L399
.L387:
	movq	41152(%r12), %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal13GlobalHandles6CreateENS0_6ObjectE@PLT
	movq	%r12, -40(%rbp)
	movq	64(%rbx), %rsi
	movq	%rax, -48(%rbp)
	movq	%rax, %xmm0
	cmpq	72(%rbx), %rsi
	je	.L385
	movq	%r12, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rsi)
	addq	$16, 64(%rbx)
.L381:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L400
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L399:
	.cfi_restore_state
	movb	$1, 48(%rbx)
	movq	(%rsi), %r9
	jmp	.L387
	.p2align 4,,10
	.p2align 3
.L385:
	leaq	-48(%rbp), %rdx
	leaq	56(%rbx), %rdi
	call	_ZNSt6vectorIN2v88internal4wasm17WasmMemoryTracker23SharedMemoryObjectStateESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	jmp	.L381
.L400:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20068:
	.size	_ZN2v88internal4wasm17WasmMemoryTracker31RegisterSharedWasmMemory_LockedENS0_6HandleINS0_16WasmMemoryObjectEEEPNS0_7IsolateE, .-_ZN2v88internal4wasm17WasmMemoryTracker31RegisterSharedWasmMemory_LockedENS0_6HandleINS0_16WasmMemoryObjectEEEPNS0_7IsolateE
	.section	.text._ZNSt10_HashtableIPKvSt4pairIKS1_St13unordered_setIPN2v88internal7IsolateESt4hashIS8_ESt8equal_toIS8_ESaIS8_EEESaISF_ENSt8__detail10_Select1stESB_IS1_ES9_IS1_ENSH_18_Mod_range_hashingENSH_20_Default_ranged_hashENSH_20_Prime_rehash_policyENSH_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS3_,"axG",@progbits,_ZNSt10_HashtableIPKvSt4pairIKS1_St13unordered_setIPN2v88internal7IsolateESt4hashIS8_ESt8equal_toIS8_ESaIS8_EEESaISF_ENSt8__detail10_Select1stESB_IS1_ES9_IS1_ENSH_18_Mod_range_hashingENSH_20_Default_ranged_hashENSH_20_Prime_rehash_policyENSH_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS3_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPKvSt4pairIKS1_St13unordered_setIPN2v88internal7IsolateESt4hashIS8_ESt8equal_toIS8_ESaIS8_EEESaISF_ENSt8__detail10_Select1stESB_IS1_ES9_IS1_ENSH_18_Mod_range_hashingENSH_20_Default_ranged_hashENSH_20_Prime_rehash_policyENSH_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS3_
	.type	_ZNSt10_HashtableIPKvSt4pairIKS1_St13unordered_setIPN2v88internal7IsolateESt4hashIS8_ESt8equal_toIS8_ESaIS8_EEESaISF_ENSt8__detail10_Select1stESB_IS1_ES9_IS1_ENSH_18_Mod_range_hashingENSH_20_Default_ranged_hashENSH_20_Prime_rehash_policyENSH_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS3_, @function
_ZNSt10_HashtableIPKvSt4pairIKS1_St13unordered_setIPN2v88internal7IsolateESt4hashIS8_ESt8equal_toIS8_ESaIS8_EEESaISF_ENSt8__detail10_Select1stESB_IS1_ES9_IS1_ENSH_18_Mod_range_hashingENSH_20_Default_ranged_hashENSH_20_Prime_rehash_policyENSH_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS3_:
.LFB24573:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rsi), %r8
	movq	8(%rdi), %rdi
	movq	0(%r13), %rbx
	movq	%r8, %rax
	divq	%rdi
	leaq	0(,%rdx,8), %r15
	leaq	(%rbx,%r15), %r14
	movq	(%r14), %r11
	testq	%r11, %r11
	je	.L416
	movq	(%r11), %r12
	movq	%rdx, %r10
	movq	%r11, %r9
	movq	8(%r12), %rcx
	jmp	.L404
	.p2align 4,,10
	.p2align 3
.L428:
	movq	(%r12), %rsi
	testq	%rsi, %rsi
	je	.L416
	movq	8(%rsi), %rcx
	xorl	%edx, %edx
	movq	%r12, %r9
	movq	%rcx, %rax
	divq	%rdi
	cmpq	%r10, %rdx
	jne	.L416
	movq	%rsi, %r12
.L404:
	cmpq	%rcx, %r8
	jne	.L428
	movq	(%r12), %rcx
	cmpq	%r9, %r11
	je	.L429
	testq	%rcx, %rcx
	je	.L406
	movq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	%rdi
	cmpq	%rdx, %r10
	je	.L406
	movq	%r9, (%rbx,%rdx,8)
	movq	(%r12), %rcx
.L406:
	movq	%rcx, (%r9)
	movq	32(%r12), %rbx
	testq	%rbx, %rbx
	je	.L411
	.p2align 4,,10
	.p2align 3
.L408:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L408
.L411:
	movq	24(%r12), %rax
	movq	16(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	16(%r12), %rdi
	leaq	64(%r12), %rax
	movq	$0, 40(%r12)
	movq	$0, 32(%r12)
	cmpq	%rax, %rdi
	je	.L409
	call	_ZdlPv@PLT
.L409:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	subq	$1, 24(%r13)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L416:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L429:
	.cfi_restore_state
	testq	%rcx, %rcx
	je	.L417
	movq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	%rdi
	cmpq	%rdx, %r10
	je	.L406
	movq	%r9, (%rbx,%rdx,8)
	addq	0(%r13), %r15
	movq	(%r15), %rax
	movq	%r15, %r14
.L405:
	leaq	16(%r13), %rdx
	cmpq	%rdx, %rax
	je	.L430
.L407:
	movq	$0, (%r14)
	movq	(%r12), %rcx
	jmp	.L406
.L417:
	movq	%r9, %rax
	jmp	.L405
.L430:
	movq	%rcx, 16(%r13)
	jmp	.L407
	.cfi_endproc
.LFE24573:
	.size	_ZNSt10_HashtableIPKvSt4pairIKS1_St13unordered_setIPN2v88internal7IsolateESt4hashIS8_ESt8equal_toIS8_ESaIS8_EEESaISF_ENSt8__detail10_Select1stESB_IS1_ES9_IS1_ENSH_18_Mod_range_hashingENSH_20_Default_ranged_hashENSH_20_Prime_rehash_policyENSH_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS3_, .-_ZNSt10_HashtableIPKvSt4pairIKS1_St13unordered_setIPN2v88internal7IsolateESt4hashIS8_ESt8equal_toIS8_ESaIS8_EEESaISF_ENSt8__detail10_Select1stESB_IS1_ES9_IS1_ENSH_18_Mod_range_hashingENSH_20_Default_ranged_hashENSH_20_Prime_rehash_policyENSH_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS3_
	.section	.text._ZN2v88internal4wasm17WasmMemoryTracker28AreAllIsolatesUpdated_LockedEPKv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm17WasmMemoryTracker28AreAllIsolatesUpdated_LockedEPKv
	.type	_ZN2v88internal4wasm17WasmMemoryTracker28AreAllIsolatesUpdated_LockedEPKv, @function
_ZN2v88internal4wasm17WasmMemoryTracker28AreAllIsolatesUpdated_LockedEPKv:
.LFB20072:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	120(%rdi), %r8
	movq	%rsi, -24(%rbp)
	divq	%r8
	movq	112(%rdi), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L460
	movq	%rsi, %rcx
	movq	(%rax), %rsi
	movq	%rdx, %r10
	movq	8(%rsi), %r9
	jmp	.L434
	.p2align 4,,10
	.p2align 3
.L461:
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	je	.L460
	movq	8(%rsi), %r9
	xorl	%edx, %edx
	movq	%r9, %rax
	divq	%r8
	cmpq	%rdx, %r10
	jne	.L460
.L434:
	cmpq	%r9, %rcx
	jne	.L461
	movq	176(%rdi), %r10
	movq	%rcx, %rax
	xorl	%edx, %edx
	divq	%r10
	movq	168(%rdi), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r11
	testq	%rax, %rax
	je	.L452
	movq	(%rax), %r8
	movq	8(%r8), %r9
	jmp	.L436
	.p2align 4,,10
	.p2align 3
.L462:
	movq	(%r8), %r8
	testq	%r8, %r8
	je	.L452
	movq	8(%r8), %r9
	xorl	%edx, %edx
	movq	%r9, %rax
	divq	%r10
	cmpq	%rdx, %r11
	jne	.L452
.L436:
	cmpq	%r9, %rcx
	jne	.L462
	movq	40(%r8), %rbx
	xorl	%eax, %eax
	cmpq	%rbx, 40(%rsi)
	je	.L463
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L463:
	.cfi_restore_state
	movq	32(%rsi), %r11
	testq	%r11, %r11
	jne	.L464
.L437:
	leaq	-24(%rbp), %rsi
	addq	$168, %rdi
	call	_ZNSt10_HashtableIPKvSt4pairIKS1_St13unordered_setIPN2v88internal7IsolateESt4hashIS8_ESt8equal_toIS8_ESaIS8_EEESaISF_ENSt8__detail10_Select1stESB_IS1_ES9_IS1_ENSH_18_Mod_range_hashingENSH_20_Default_ranged_hashENSH_20_Prime_rehash_policyENSH_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS3_
.L460:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L452:
	.cfi_restore_state
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L464:
	.cfi_restore_state
	movq	24(%r8), %r9
	movq	16(%r8), %rbx
.L440:
	movq	8(%r11), %r8
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	%r9
	movq	(%rbx,%rdx,8), %rax
	movq	%rdx, %r10
	testq	%rax, %rax
	je	.L452
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L439
	.p2align 4,,10
	.p2align 3
.L465:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L452
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%r9
	cmpq	%rdx, %r10
	jne	.L452
.L439:
	cmpq	%rsi, %r8
	jne	.L465
	movq	(%r11), %r11
	testq	%r11, %r11
	je	.L437
	jmp	.L440
	.cfi_endproc
.LFE20072:
	.size	_ZN2v88internal4wasm17WasmMemoryTracker28AreAllIsolatesUpdated_LockedEPKv, .-_ZN2v88internal4wasm17WasmMemoryTracker28AreAllIsolatesUpdated_LockedEPKv
	.section	.text._ZN2v88internal4wasm17WasmMemoryTracker48DestroyMemoryObjectsAndRemoveIsolateEntry_LockedEPKv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm17WasmMemoryTracker48DestroyMemoryObjectsAndRemoveIsolateEntry_LockedEPKv
	.type	_ZN2v88internal4wasm17WasmMemoryTracker48DestroyMemoryObjectsAndRemoveIsolateEntry_LockedEPKv, @function
_ZN2v88internal4wasm17WasmMemoryTracker48DestroyMemoryObjectsAndRemoveIsolateEntry_LockedEPKv:
.LFB20079:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	movq	%rdi, %rax
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, -40(%rbp)
	movq	64(%r13), %rsi
	divq	%rsi
	movq	56(%r13), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L467
	movq	(%rax), %rbx
	movq	%rdx, %r8
	movq	8(%rbx), %rcx
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L483:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L467
	movq	8(%rbx), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%rdx, %r8
	jne	.L467
.L469:
	cmpq	%rcx, %rdi
	jne	.L483
	cmpb	$0, 48(%rbx)
	je	.L467
	movq	64(%rbx), %r14
	movq	56(%rbx), %r12
	cmpq	%r12, %r14
	je	.L466
	.p2align 4,,10
	.p2align 3
.L471:
	movq	(%r12), %rdi
	addq	$16, %r12
	call	_ZN2v88internal13GlobalHandles7DestroyEPm@PLT
	cmpq	%r12, %r14
	jne	.L471
	movq	56(%rbx), %rax
	cmpq	64(%rbx), %rax
	je	.L472
	movq	%rax, 64(%rbx)
.L472:
	leaq	-40(%rbp), %rsi
	leaq	112(%r13), %rdi
	call	_ZNSt10_HashtableIPKvSt4pairIKS1_St13unordered_setIPN2v88internal7IsolateESt4hashIS8_ESt8equal_toIS8_ESaIS8_EEESaISF_ENSt8__detail10_Select1stESB_IS1_ES9_IS1_ENSH_18_Mod_range_hashingENSH_20_Default_ranged_hashENSH_20_Prime_rehash_policyENSH_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS3_
.L466:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L467:
	.cfi_restore_state
	leaq	.LC4(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20079:
	.size	_ZN2v88internal4wasm17WasmMemoryTracker48DestroyMemoryObjectsAndRemoveIsolateEntry_LockedEPKv, .-_ZN2v88internal4wasm17WasmMemoryTracker48DestroyMemoryObjectsAndRemoveIsolateEntry_LockedEPKv
	.section	.text._ZNSt10_HashtableIPN2v88internal7IsolateES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS3_,"axG",@progbits,_ZNSt10_HashtableIPN2v88internal7IsolateES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS3_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPN2v88internal7IsolateES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS3_
	.type	_ZNSt10_HashtableIPN2v88internal7IsolateES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS3_, @function
_ZNSt10_HashtableIPN2v88internal7IsolateES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS3_:
.LFB24583:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rsi), %r9
	movq	8(%rdi), %r8
	movq	(%rdi), %r13
	movq	%r9, %rax
	divq	%r8
	leaq	0(,%rdx,8), %r15
	leaq	0(%r13,%r15), %r14
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L495
	movq	%rdi, %rbx
	movq	(%r12), %rdi
	movq	%rdx, %r11
	movq	%r12, %r10
	movq	8(%rdi), %rcx
	jmp	.L487
	.p2align 4,,10
	.p2align 3
.L502:
	movq	(%rdi), %rsi
	testq	%rsi, %rsi
	je	.L495
	movq	8(%rsi), %rcx
	xorl	%edx, %edx
	movq	%rdi, %r10
	movq	%rcx, %rax
	divq	%r8
	cmpq	%r11, %rdx
	jne	.L495
	movq	%rsi, %rdi
.L487:
	cmpq	%rcx, %r9
	jne	.L502
	movq	(%rdi), %rcx
	cmpq	%r10, %r12
	je	.L503
	testq	%rcx, %rcx
	je	.L489
	movq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	%r8
	cmpq	%rdx, %r11
	je	.L489
	movq	%r10, 0(%r13,%rdx,8)
	movq	(%rdi), %rcx
.L489:
	movq	%rcx, (%r10)
	call	_ZdlPv@PLT
	subq	$1, 24(%rbx)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L495:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L503:
	.cfi_restore_state
	testq	%rcx, %rcx
	je	.L496
	movq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	%r8
	cmpq	%rdx, %r11
	je	.L489
	movq	%r10, 0(%r13,%rdx,8)
	addq	(%rbx), %r15
	movq	(%r15), %rax
	movq	%r15, %r14
.L488:
	leaq	16(%rbx), %rdx
	cmpq	%rdx, %rax
	je	.L504
.L490:
	movq	$0, (%r14)
	movq	(%rdi), %rcx
	jmp	.L489
.L496:
	movq	%r10, %rax
	jmp	.L488
.L504:
	movq	%rcx, 16(%rbx)
	jmp	.L490
	.cfi_endproc
.LFE24583:
	.size	_ZNSt10_HashtableIPN2v88internal7IsolateES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS3_, .-_ZNSt10_HashtableIPN2v88internal7IsolateES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS3_
	.section	.text._ZN2v88internal4wasm17WasmMemoryTracker34DeleteSharedMemoryObjectsOnIsolateEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm17WasmMemoryTracker34DeleteSharedMemoryObjectsOnIsolateEPNS0_7IsolateE
	.type	_ZN2v88internal4wasm17WasmMemoryTracker34DeleteSharedMemoryObjectsOnIsolateEPNS0_7IsolateE, @function
_ZN2v88internal4wasm17WasmMemoryTracker34DeleteSharedMemoryObjectsOnIsolateEPNS0_7IsolateE:
.LFB20082:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	8(%rdi), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -56(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	cmpq	$0, 136(%r12)
	je	.L510
	movq	128(%r12), %rbx
	leaq	-56(%rbp), %r13
	testq	%rbx, %rbx
	je	.L514
	.p2align 4,,10
	.p2align 3
.L508:
	movq	-56(%rbp), %r8
	movq	24(%rbx), %rdi
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	%rdi
	movq	16(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L511
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L513
	.p2align 4,,10
	.p2align 3
.L528:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L511
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L511
.L513:
	cmpq	%rsi, %r8
	jne	.L528
	movq	8(%rbx), %r15
	leaq	16(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZNSt10_HashtableIPN2v88internal7IsolateES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS3_
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r15, %rdx
	call	_ZN2v88internal4wasm17WasmMemoryTracker48DestroyMemoryObjectsAndRemoveIsolateEntry_LockedEPNS0_7IsolateEPKv
.L511:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L508
.L514:
	movq	184(%r12), %rbx
	leaq	-56(%rbp), %r12
	testq	%rbx, %rbx
	je	.L510
	.p2align 4,,10
	.p2align 3
.L509:
	leaq	16(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZNSt10_HashtableIPN2v88internal7IsolateES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS3_
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L509
.L510:
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20082:
	.size	_ZN2v88internal4wasm17WasmMemoryTracker34DeleteSharedMemoryObjectsOnIsolateEPNS0_7IsolateE, .-_ZN2v88internal4wasm17WasmMemoryTracker34DeleteSharedMemoryObjectsOnIsolateEPNS0_7IsolateE
	.section	.text._ZN2v88internal4wasm17WasmMemoryTracker36RemoveIsolateFromBackingStore_LockedEPNS0_7IsolateEPKv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm17WasmMemoryTracker36RemoveIsolateFromBackingStore_LockedEPNS0_7IsolateEPKv
	.type	_ZN2v88internal4wasm17WasmMemoryTracker36RemoveIsolateFromBackingStore_LockedEPNS0_7IsolateEPKv, @function
_ZN2v88internal4wasm17WasmMemoryTracker36RemoveIsolateFromBackingStore_LockedEPNS0_7IsolateEPKv:
.LFB20081:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rax
	movq	%rdx, %rcx
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rsi, -8(%rbp)
	movq	120(%rdi), %rsi
	divq	%rsi
	movq	112(%rdi), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L529
	movq	(%rax), %rdi
	movq	%rdx, %r9
	movq	8(%rdi), %r8
	jmp	.L532
	.p2align 4,,10
	.p2align 3
.L542:
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L529
	movq	8(%rdi), %r8
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	%rsi
	cmpq	%rdx, %r9
	jne	.L529
.L532:
	cmpq	%rcx, %r8
	jne	.L542
	cmpq	$0, 40(%rdi)
	jne	.L540
.L529:
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L540:
	.cfi_restore_state
	leaq	-8(%rbp), %rsi
	addq	$16, %rdi
	call	_ZNSt10_HashtableIPN2v88internal7IsolateES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS3_
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20081:
	.size	_ZN2v88internal4wasm17WasmMemoryTracker36RemoveIsolateFromBackingStore_LockedEPNS0_7IsolateEPKv, .-_ZN2v88internal4wasm17WasmMemoryTracker36RemoveIsolateFromBackingStore_LockedEPNS0_7IsolateEPKv
	.section	.text._ZN2v88internal4wasm17WasmMemoryTracker30RemoveSharedBufferState_LockedEPNS0_7IsolateEPKv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm17WasmMemoryTracker30RemoveSharedBufferState_LockedEPNS0_7IsolateEPKv
	.type	_ZN2v88internal4wasm17WasmMemoryTracker30RemoveSharedBufferState_LockedEPNS0_7IsolateEPKv, @function
_ZN2v88internal4wasm17WasmMemoryTracker30RemoveSharedBufferState_LockedEPNS0_7IsolateEPKv:
.LFB20078:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L544
	movq	%rsi, %rbx
	call	_ZN2v88internal4wasm17WasmMemoryTracker48DestroyMemoryObjectsAndRemoveIsolateEntry_LockedEPNS0_7IsolateEPKv
	movq	120(%r13), %rsi
	movq	%r12, %rax
	xorl	%edx, %edx
	movq	%rbx, -48(%rbp)
	divq	%rsi
	movq	112(%r13), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r8
	testq	%rax, %rax
	je	.L543
	movq	(%rax), %rdi
	movq	8(%rdi), %rcx
	jmp	.L547
	.p2align 4,,10
	.p2align 3
.L559:
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L543
	movq	8(%rdi), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%rdx, %r8
	jne	.L543
.L547:
	cmpq	%rcx, %r12
	jne	.L559
	cmpq	$0, 40(%rdi)
	jne	.L557
.L543:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L560
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L557:
	.cfi_restore_state
	leaq	-48(%rbp), %rsi
	addq	$16, %rdi
	call	_ZNSt10_HashtableIPN2v88internal7IsolateES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS3_
	jmp	.L543
	.p2align 4,,10
	.p2align 3
.L544:
	movq	%rdx, %rsi
	call	_ZN2v88internal4wasm17WasmMemoryTracker48DestroyMemoryObjectsAndRemoveIsolateEntry_LockedEPKv
	jmp	.L543
.L560:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20078:
	.size	_ZN2v88internal4wasm17WasmMemoryTracker30RemoveSharedBufferState_LockedEPNS0_7IsolateEPKv, .-_ZN2v88internal4wasm17WasmMemoryTracker30RemoveSharedBufferState_LockedEPNS0_7IsolateEPKv
	.section	.rodata._ZN2v88internal4wasm17WasmMemoryTracker28FreeMemoryIfNotShared_LockedEPNS0_7IsolateEPKv.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"FreePages(GetPlatformPageAllocator(), allocation.allocation_base, allocation.allocation_length)"
	.section	.text._ZN2v88internal4wasm17WasmMemoryTracker28FreeMemoryIfNotShared_LockedEPNS0_7IsolateEPKv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm17WasmMemoryTracker28FreeMemoryIfNotShared_LockedEPNS0_7IsolateEPKv
	.type	_ZN2v88internal4wasm17WasmMemoryTracker28FreeMemoryIfNotShared_LockedEPNS0_7IsolateEPKv, @function
_ZN2v88internal4wasm17WasmMemoryTracker28FreeMemoryIfNotShared_LockedEPNS0_7IsolateEPKv:
.LFB20076:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L562
	call	_ZN2v88internal4wasm17WasmMemoryTracker48DestroyMemoryObjectsAndRemoveIsolateEntry_LockedEPNS0_7IsolateEPKv
	movq	120(%r13), %rcx
	movq	%r12, %rax
	xorl	%edx, %edx
	movq	%r14, -112(%rbp)
	divq	%rcx
	movq	112(%r13), %rax
	leaq	(%rax,%rdx,8), %r9
	movq	%rdx, %r8
	movq	%rdx, %r10
	movq	(%r9), %rax
	testq	%rax, %rax
	je	.L566
	movq	(%rax), %rdi
	movq	8(%rdi), %rsi
	jmp	.L565
	.p2align 4,,10
	.p2align 3
.L596:
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L566
	movq	8(%rdi), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rcx
	cmpq	%rdx, %r8
	jne	.L566
.L565:
	cmpq	%rsi, %r12
	jne	.L596
	cmpq	$0, 40(%rdi)
	jne	.L597
.L566:
	movq	(%r9), %rax
	testq	%rax, %rax
	je	.L568
	movq	(%rax), %rsi
	movq	8(%rsi), %rdi
	jmp	.L569
	.p2align 4,,10
	.p2align 3
.L598:
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	je	.L568
	movq	8(%rsi), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rcx
	cmpq	%rdx, %r10
	jne	.L568
.L569:
	cmpq	%rdi, %r12
	jne	.L598
	cmpq	$0, 40(%rsi)
	je	.L568
.L561:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L599
	addq	$88, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L597:
	.cfi_restore_state
	leaq	-112(%rbp), %rsi
	addq	$16, %rdi
	call	_ZNSt10_HashtableIPN2v88internal7IsolateES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS3_
.L595:
	movq	120(%r13), %rcx
	movq	%r12, %rax
	xorl	%edx, %edx
	divq	%rcx
	movq	112(%r13), %rax
	movq	%rdx, %r10
	leaq	(%rax,%rdx,8), %r9
	jmp	.L566
	.p2align 4,,10
	.p2align 3
.L568:
	movq	%r13, %rsi
	leaq	-112(%rbp), %rdi
	movq	%r12, %rcx
	movq	%r14, %rdx
	call	_ZN2v88internal4wasm17WasmMemoryTracker24ReleaseAllocation_LockedEPNS0_7IsolateEPKv
	movq	-104(%rbp), %r13
	movq	-112(%rbp), %r12
	call	_ZN2v88internal24GetPlatformPageAllocatorEv@PLT
	movq	%rax, %rdi
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal9FreePagesEPNS_13PageAllocatorEPvm@PLT
	testb	%al, %al
	je	.L600
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L561
	call	_ZdlPv@PLT
	jmp	.L561
	.p2align 4,,10
	.p2align 3
.L562:
	movq	%rdx, %rsi
	call	_ZN2v88internal4wasm17WasmMemoryTracker48DestroyMemoryObjectsAndRemoveIsolateEntry_LockedEPKv
	jmp	.L595
	.p2align 4,,10
	.p2align 3
.L600:
	leaq	.LC6(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L599:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20076:
	.size	_ZN2v88internal4wasm17WasmMemoryTracker28FreeMemoryIfNotShared_LockedEPNS0_7IsolateEPKv, .-_ZN2v88internal4wasm17WasmMemoryTracker28FreeMemoryIfNotShared_LockedEPNS0_7IsolateEPKv
	.section	.text._ZN2v88internal4wasm17WasmMemoryTracker14FreeWasmMemoryEPNS0_7IsolateEPKv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm17WasmMemoryTracker14FreeWasmMemoryEPNS0_7IsolateEPKv
	.type	_ZN2v88internal4wasm17WasmMemoryTracker14FreeWasmMemoryEPNS0_7IsolateEPKv, @function
_ZN2v88internal4wasm17WasmMemoryTracker14FreeWasmMemoryEPNS0_7IsolateEPKv:
.LFB20064:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	8(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r15, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	64(%r12), %rdi
	movq	%rbx, %rax
	xorl	%edx, %edx
	divq	%rdi
	movq	56(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L612
	movq	(%rax), %rcx
	movq	%rdx, %rsi
	movq	8(%rcx), %r8
	jmp	.L604
	.p2align 4,,10
	.p2align 3
.L619:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L612
	movq	8(%rcx), %r8
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	%rdi
	cmpq	%rdx, %rsi
	jne	.L612
.L604:
	cmpq	%rbx, %r8
	jne	.L619
	movzbl	48(%rcx), %r13d
	testb	%r13b, %r13b
	jne	.L620
	movq	%r12, %rsi
	leaq	-128(%rbp), %rdi
	movq	%r8, %rcx
	movq	%r14, %rdx
	call	_ZN2v88internal4wasm17WasmMemoryTracker24ReleaseAllocation_LockedEPNS0_7IsolateEPKv
	movq	-120(%rbp), %r13
	movq	-128(%rbp), %r12
	call	_ZN2v88internal24GetPlatformPageAllocatorEv@PLT
	movq	%rax, %rdi
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal9FreePagesEPNS_13PageAllocatorEPvm@PLT
	movl	%eax, %r13d
	testb	%al, %al
	je	.L621
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L602
	call	_ZdlPv@PLT
	jmp	.L602
	.p2align 4,,10
	.p2align 3
.L612:
	xorl	%r13d, %r13d
.L602:
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L622
	addq	$88, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L620:
	.cfi_restore_state
	movq	%r8, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm17WasmMemoryTracker28FreeMemoryIfNotShared_LockedEPNS0_7IsolateEPKv
	jmp	.L602
	.p2align 4,,10
	.p2align 3
.L621:
	leaq	.LC6(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L622:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20064:
	.size	_ZN2v88internal4wasm17WasmMemoryTracker14FreeWasmMemoryEPNS0_7IsolateEPKv, .-_ZN2v88internal4wasm17WasmMemoryTracker14FreeWasmMemoryEPNS0_7IsolateEPKv
	.section	.text._ZNSt10_HashtableIPKvSt4pairIKS1_N2v88internal4wasm17WasmMemoryTracker14AllocationDataEESaIS9_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIPKvSt4pairIKS1_N2v88internal4wasm17WasmMemoryTracker14AllocationDataEESaIS9_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPKvSt4pairIKS1_N2v88internal4wasm17WasmMemoryTracker14AllocationDataEESaIS9_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm
	.type	_ZNSt10_HashtableIPKvSt4pairIKS1_N2v88internal4wasm17WasmMemoryTracker14AllocationDataEESaIS9_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm, @function
_ZNSt10_HashtableIPKvSt4pairIKS1_N2v88internal4wasm17WasmMemoryTracker14AllocationDataEESaIS9_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm:
.LFB24952:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L624
	movq	(%rbx), %r8
.L625:
	movq	(%r8,%r15,8), %rax
	leaq	0(,%r15,8), %rcx
	testq	%rax, %rax
	je	.L634
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r13, (%rax)
.L635:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L624:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L648
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L649
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%rbx), %r10
	movq	%rax, %r8
.L627:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L629
	xorl	%edi, %edi
	leaq	16(%rbx), %r9
	jmp	.L630
	.p2align 4,,10
	.p2align 3
.L631:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L632:
	testq	%rsi, %rsi
	je	.L629
.L630:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	8(%rcx), %rax
	divq	%r12
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L631
	movq	16(%rbx), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L637
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L630
	.p2align 4,,10
	.p2align 3
.L629:
	movq	(%rbx), %rdi
	cmpq	%r10, %rdi
	je	.L633
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L633:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	divq	%r12
	movq	%r8, (%rbx)
	movq	%rdx, %r15
	jmp	.L625
	.p2align 4,,10
	.p2align 3
.L634:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L636
	movq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r13, (%rax,%rdx,8)
.L636:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L637:
	movq	%rdx, %rdi
	jmp	.L632
	.p2align 4,,10
	.p2align 3
.L648:
	leaq	48(%rbx), %r8
	movq	$0, 48(%rbx)
	movq	%r8, %r10
	jmp	.L627
.L649:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE24952:
	.size	_ZNSt10_HashtableIPKvSt4pairIKS1_N2v88internal4wasm17WasmMemoryTracker14AllocationDataEESaIS9_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm, .-_ZNSt10_HashtableIPKvSt4pairIKS1_N2v88internal4wasm17WasmMemoryTracker14AllocationDataEESaIS9_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm
	.section	.text._ZN2v88internal4wasm17WasmMemoryTracker18RegisterAllocationEPNS0_7IsolateEPvmS5_m,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm17WasmMemoryTracker18RegisterAllocationEPNS0_7IsolateEPvmS5_m
	.type	_ZN2v88internal4wasm17WasmMemoryTracker18RegisterAllocationEPNS0_7IsolateEPvmS5_m, @function
_ZN2v88internal4wasm17WasmMemoryTracker18RegisterAllocationEPNS0_7IsolateEPvmS5_m:
.LFB20045:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	8(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r14, %rdi
	subq	$24, %rsp
	movq	%r9, -56(%rbp)
	movq	%rsi, -64(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	48(%rbx), %rsi
	movq	-64(%rbp), %rax
	addq	%r13, %rsi
	movq	%rsi, 48(%rbx)
	movq	40960(%rax), %rdi
	shrq	$20, %rsi
	addq	$1808, %rdi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	movl	$80, %edi
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movl	$256, %edx
	movq	64(%rbx), %rsi
	pxor	%xmm0, %xmm0
	movw	%dx, 48(%rax)
	movq	%rax, %rdi
	xorl	%edx, %edx
	movq	$0, (%rax)
	movq	%r12, 8(%rax)
	movq	%r15, 16(%rax)
	movq	%r13, 24(%rax)
	movq	%r12, 32(%rax)
	movq	%r9, 40(%rax)
	movq	$0, 72(%rax)
	movups	%xmm0, 56(%rax)
	movq	%r12, %rax
	divq	%rsi
	movq	56(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	testq	%rax, %rax
	je	.L651
	movq	(%rax), %rcx
	movq	8(%rcx), %r8
	jmp	.L653
	.p2align 4,,10
	.p2align 3
.L663:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L651
	movq	8(%rcx), %r8
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	%rsi
	cmpq	%rdx, %r10
	jne	.L651
.L653:
	cmpq	%r12, %r8
	jne	.L663
	call	_ZdlPv@PLT
.L654:
	addq	$24, %rsp
	movq	%r14, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L651:
	.cfi_restore_state
	leaq	56(%rbx), %r9
	movq	%rdi, %rcx
	movl	$1, %r8d
	movq	%r12, %rdx
	movq	%r10, %rsi
	movq	%r9, %rdi
	call	_ZNSt10_HashtableIPKvSt4pairIKS1_N2v88internal4wasm17WasmMemoryTracker14AllocationDataEESaIS9_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm
	jmp	.L654
	.cfi_endproc
.LFE20045:
	.size	_ZN2v88internal4wasm17WasmMemoryTracker18RegisterAllocationEPNS0_7IsolateEPvmS5_m, .-_ZN2v88internal4wasm17WasmMemoryTracker18RegisterAllocationEPNS0_7IsolateEPvmS5_m
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_123TryAllocateBackingStoreEPNS1_17WasmMemoryTrackerEPNS0_4HeapEmmPPvPm.str1.8,"aMS",@progbits,1
	.align 8
.LC7:
	.string	"could not allocate wasm memory"
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_123TryAllocateBackingStoreEPNS1_17WasmMemoryTrackerEPNS0_4HeapEmmPPvPm.str1.1,"aMS",@progbits,1
.LC8:
	.string	"TryAllocateBackingStore"
	.section	.text._ZN2v88internal4wasm12_GLOBAL__N_123TryAllocateBackingStoreEPNS1_17WasmMemoryTrackerEPNS0_4HeapEmmPPvPm,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal4wasm12_GLOBAL__N_123TryAllocateBackingStoreEPNS1_17WasmMemoryTrackerEPNS0_4HeapEmmPPvPm, @function
_ZN2v88internal4wasm12_GLOBAL__N_123TryAllocateBackingStoreEPNS1_17WasmMemoryTrackerEPNS0_4HeapEmmPPvPm:
.LFB20018:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-160(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movl	$3, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	leaq	_ZNSt17_Function_handlerIFbvEZN2v88internal4wasm12_GLOBAL__N_123TryAllocateBackingStoreEPNS3_17WasmMemoryTrackerEPNS2_4HeapEmmPPvPmEUlvE_E9_M_invokeERKSt9_Any_data(%rip), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movq	%r12, %xmm3
	subq	$216, %rsp
	movq	%rdi, -184(%rbp)
	movq	%rdx, -192(%rbp)
	movq	%r8, -200(%rbp)
	movq	%r9, -208(%rbp)
	movq	%fs:40, %rsi
	movq	%rsi, -56(%rbp)
	xorl	%esi, %esi
	leaq	-208(%rbp), %rsi
	cmpq	%rcx, %rdx
	movq	$0, -144(%rbp)
	movq	%rsi, %rdi
	movq	%rsi, -248(%rbp)
	cmovnb	%rdx, %rcx
	leaq	-176(%rbp), %rsi
	movq	%rsi, %xmm2
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal4wasm12_GLOBAL__N_123TryAllocateBackingStoreEPNS3_17WasmMemoryTrackerEPNS2_4HeapEmmPPvPmEUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rsi
	xorl	%r14d, %r14d
	movq	%rsi, %xmm0
	movq	%rdi, %xmm1
	movl	$24, %edi
	movq	%rcx, -176(%rbp)
	punpcklqdq	%xmm2, %xmm1
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm1, -240(%rbp)
	movaps	%xmm0, -224(%rbp)
	call	_Znwm@PLT
	movdqa	-240(%rbp), %xmm1
	movdqa	-224(%rbp), %xmm0
	movq	%r15, %rdi
	leaq	-184(%rbp), %rdx
	movq	%rax, -160(%rbp)
	movq	%rdx, 16(%rax)
	movups	%xmm1, (%rax)
	movaps	%xmm0, -144(%rbp)
	call	*%r12
	movl	%eax, %r12d
	testb	%al, %al
	jne	.L666
.L703:
	subl	$1, %r13d
	je	.L667
	movl	$1, %edx
	movl	$2, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal4Heap26MemoryPressureNotificationENS_19MemoryPressureLevelEb@PLT
	cmpq	$0, -144(%rbp)
	je	.L668
	movq	-136(%rbp), %r12
	movq	%r15, %rdi
	movl	$1, %r14d
	call	*%r12
	movl	%eax, %r12d
	testb	%al, %al
	je	.L703
.L666:
	movq	-144(%rbp), %rax
	testq	%rax, %rax
	je	.L692
	movl	$3, %edx
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	*%rax
.L692:
	leaq	-200(%rbp), %rax
	leaq	-128(%rbp), %r15
	movl	$3, -224(%rbp)
	movq	%rax, %xmm0
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal4wasm12_GLOBAL__N_123TryAllocateBackingStoreEPNS3_17WasmMemoryTrackerEPNS2_4HeapEmmPPvPmEUlvE0_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rcx
	leaq	_ZNSt17_Function_handlerIFbvEZN2v88internal4wasm12_GLOBAL__N_123TryAllocateBackingStoreEPNS3_17WasmMemoryTrackerEPNS2_4HeapEmmPPvPmEUlvE0_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%r15, %rdi
	movhps	-248(%rbp), %xmm0
	movq	%rax, %xmm4
	movaps	%xmm0, -128(%rbp)
	movq	%rcx, %xmm0
	punpcklqdq	%xmm4, %xmm0
	movaps	%xmm0, -112(%rbp)
	call	*%rax
	movl	%eax, %r13d
	testb	%al, %al
	jne	.L673
.L704:
	subl	$1, -224(%rbp)
	je	.L674
	movl	$1, %edx
	movl	$2, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal4Heap26MemoryPressureNotificationENS_19MemoryPressureLevelEb@PLT
	cmpq	$0, -112(%rbp)
	je	.L668
	movq	-104(%rbp), %rax
	movq	%r15, %rdi
	movl	%r12d, %r14d
	call	*%rax
	movl	%eax, %r13d
	testb	%al, %al
	je	.L704
.L673:
	movq	-112(%rbp), %rax
	testq	%rax, %rax
	jne	.L705
.L689:
	movq	-200(%rbp), %rdx
	movl	$2147483648, %eax
	leaq	-96(%rbp), %r15
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal4wasm12_GLOBAL__N_123TryAllocateBackingStoreEPNS3_17WasmMemoryTrackerEPNS2_4HeapEmmPPvPmEUlvE1_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rcx
	movl	$3, %r12d
	movq	%r15, %rdi
	addq	(%rdx), %rax
	movq	%rax, -168(%rbp)
	leaq	-192(%rbp), %rax
	movq	%rax, %xmm0
	leaq	-168(%rbp), %rax
	movq	%rax, %xmm5
	leaq	_ZNSt17_Function_handlerIFbvEZN2v88internal4wasm12_GLOBAL__N_123TryAllocateBackingStoreEPNS3_17WasmMemoryTrackerEPNS2_4HeapEmmPPvPmEUlvE1_E9_M_invokeERKSt9_Any_data(%rip), %rax
	punpcklqdq	%xmm5, %xmm0
	movq	%rax, %xmm6
	movaps	%xmm0, -96(%rbp)
	movq	%rcx, %xmm0
	punpcklqdq	%xmm6, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	*%rax
	testb	%al, %al
	jne	.L677
.L706:
	subl	$1, %r12d
	je	.L678
	movl	$1, %edx
	movl	$2, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal4Heap26MemoryPressureNotificationENS_19MemoryPressureLevelEb@PLT
	cmpq	$0, -80(%rbp)
	je	.L668
	movq	-72(%rbp), %rax
	movl	%r13d, %r14d
	movq	%r15, %rdi
	call	*%rax
	testb	%al, %al
	je	.L706
.L677:
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	jne	.L707
.L683:
	movq	-208(%rbp), %rax
	movq	-192(%rbp), %r9
	subq	$37592, %rbx
	movq	-168(%rbp), %r8
	movq	-184(%rbp), %rdi
	movq	%rbx, %rsi
	movq	(%rax), %rcx
	movq	-200(%rbp), %rax
	movq	(%rax), %rdx
	call	_ZN2v88internal4wasm17WasmMemoryTracker18RegisterAllocationEPNS0_7IsolateEPvmS5_m
	movq	40960(%rbx), %rdi
	movzbl	%r14b, %esi
	addq	$1768, %rdi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	movq	-168(%rbp), %rax
	jmp	.L664
	.p2align 4,,10
	.p2align 3
.L667:
	movq	-144(%rbp), %rax
	testq	%rax, %rax
	je	.L695
	movl	$3, %edx
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	*%rax
.L695:
	movq	-192(%rbp), %rax
	cmpb	$0, _ZN2v88internal36FLAG_correctness_fuzzer_suppressionsE(%rip)
	movq	%rax, -176(%rbp)
	jne	.L708
	movq	3368(%rbx), %rdi
	movl	$2, %esi
	addq	$1768, %rdi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	xorl	%eax, %eax
.L664:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L709
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L674:
	.cfi_restore_state
	movq	-112(%rbp), %rax
	testq	%rax, %rax
	je	.L686
	movl	$3, %edx
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	*%rax
	.p2align 4,,10
	.p2align 3
.L686:
	movq	-208(%rbp), %rdx
	movq	-184(%rbp), %rax
	movq	(%rdx), %rdx
	lock subq	%rdx, (%rax)
	movl	$3, %esi
	movq	3368(%rbx), %rdi
	addq	$1768, %rdi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	xorl	%eax, %eax
	jmp	.L664
	.p2align 4,,10
	.p2align 3
.L678:
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L680
	movl	$3, %edx
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	*%rax
.L680:
	xorl	%edx, %edx
	leaq	.LC8(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal2V823FatalProcessOutOfMemoryEPNS0_7IsolateEPKcb@PLT
	.p2align 4,,10
	.p2align 3
.L668:
	call	_ZSt25__throw_bad_function_callv@PLT
.L709:
	call	__stack_chk_fail@PLT
.L708:
	leaq	.LC7(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L705:
	movl	$3, %edx
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	*%rax
	jmp	.L689
.L707:
	movl	$3, %edx
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	*%rax
	jmp	.L683
	.cfi_endproc
.LFE20018:
	.size	_ZN2v88internal4wasm12_GLOBAL__N_123TryAllocateBackingStoreEPNS1_17WasmMemoryTrackerEPNS0_4HeapEmmPPvPm, .-_ZN2v88internal4wasm12_GLOBAL__N_123TryAllocateBackingStoreEPNS1_17WasmMemoryTrackerEPNS0_4HeapEmmPPvPm
	.section	.text._ZN2v88internal4wasm17WasmMemoryTracker33TryAllocateBackingStoreForTestingEPNS0_4HeapEmPPvPm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm17WasmMemoryTracker33TryAllocateBackingStoreForTestingEPNS0_4HeapEmPPvPm
	.type	_ZN2v88internal4wasm17WasmMemoryTracker33TryAllocateBackingStoreForTestingEPNS0_4HeapEmPPvPm, @function
_ZN2v88internal4wasm17WasmMemoryTracker33TryAllocateBackingStoreForTestingEPNS0_4HeapEmPPvPm:
.LFB20038:
	.cfi_startproc
	endbr64
	movq	%r8, %r9
	movq	%rcx, %r8
	movq	%rdx, %rcx
	jmp	_ZN2v88internal4wasm12_GLOBAL__N_123TryAllocateBackingStoreEPNS1_17WasmMemoryTrackerEPNS0_4HeapEmmPPvPm
	.cfi_endproc
.LFE20038:
	.size	_ZN2v88internal4wasm17WasmMemoryTracker33TryAllocateBackingStoreForTestingEPNS0_4HeapEmPPvPm, .-_ZN2v88internal4wasm17WasmMemoryTracker33TryAllocateBackingStoreForTestingEPNS0_4HeapEmPPvPm
	.section	.text._ZN2v88internal4wasm27AllocateAndSetupArrayBufferEPNS0_7IsolateEmmNS0_10SharedFlagE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm27AllocateAndSetupArrayBufferEPNS0_7IsolateEmmNS0_10SharedFlagE
	.type	_ZN2v88internal4wasm27AllocateAndSetupArrayBufferEPNS0_7IsolateEmmNS0_10SharedFlagE, @function
_ZN2v88internal4wasm27AllocateAndSetupArrayBufferEPNS0_7IsolateEmmNS0_10SharedFlagE:
.LFB20084:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm13max_mem_pagesEv@PLT
	movl	%eax, %eax
	salq	$16, %rax
	cmpq	%rax, %rbx
	jbe	.L712
	xorl	%eax, %eax
.L713:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L725
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L712:
	.cfi_restore_state
	movq	%r13, %rcx
	leaq	-64(%rbp), %r9
	leaq	-72(%rbp), %r8
	movq	%rbx, %rdx
	movq	45752(%r14), %rdi
	leaq	37592(%r14), %rsi
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	call	_ZN2v88internal4wasm12_GLOBAL__N_123TryAllocateBackingStoreEPNS1_17WasmMemoryTrackerEPNS0_4HeapEmmPPvPm
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L714
	movq	32(%r14), %r15
	addq	%rbx, %r15
	movq	%r15, %rax
	subq	48(%r14), %rax
	movq	%r15, 32(%r14)
	cmpq	$33554432, %rax
	jg	.L726
.L715:
	testq	%rbx, %rbx
	jne	.L727
.L716:
	movl	$1, %edx
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal7Factory16NewJSArrayBufferENS0_10SharedFlagENS0_14AllocationTypeE@PLT
	subq	$8, %rsp
	movq	%r13, %rcx
	xorl	%edx, %edx
	pushq	$1
	movq	%rax, %r15
	movq	%rax, %rdi
	movl	%r12d, %r9d
	movq	%rbx, %r8
	movq	%r14, %rsi
	movq	%r15, %r13
	call	_ZN2v88internal13JSArrayBuffer5SetupENS0_6HandleIS1_EEPNS0_7IsolateEbPvmNS0_10SharedFlagEb@PLT
	movq	(%r15), %rdx
	movl	39(%rdx), %eax
	andl	$-3, %eax
	movl	%eax, 39(%rdx)
	popq	%rax
	popq	%rdx
.L714:
	movq	%r13, %rax
	jmp	.L713
	.p2align 4,,10
	.p2align 3
.L727:
	cmpq	40(%r14), %r15
	jle	.L716
	movq	%r14, %rdi
	call	_ZN2v87Isolate36ReportExternalAllocationLimitReachedEv@PLT
	jmp	.L716
	.p2align 4,,10
	.p2align 3
.L726:
	movq	%r14, %rdi
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	jmp	.L715
.L725:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20084:
	.size	_ZN2v88internal4wasm27AllocateAndSetupArrayBufferEPNS0_7IsolateEmmNS0_10SharedFlagE, .-_ZN2v88internal4wasm27AllocateAndSetupArrayBufferEPNS0_7IsolateEmmNS0_10SharedFlagE
	.section	.text._ZN2v88internal4wasm14NewArrayBufferEPNS0_7IsolateEm,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm14NewArrayBufferEPNS0_7IsolateEm
	.type	_ZN2v88internal4wasm14NewArrayBufferEPNS0_7IsolateEm, @function
_ZN2v88internal4wasm14NewArrayBufferEPNS0_7IsolateEm:
.LFB20088:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm13max_mem_pagesEv@PLT
	movl	%eax, %eax
	salq	$16, %rax
	cmpq	%rax, %rbx
	jbe	.L742
.L729:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L743
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L742:
	.cfi_restore_state
	leaq	-48(%rbp), %r9
	leaq	-56(%rbp), %r8
	movq	%rbx, %rcx
	movq	%rbx, %rdx
	movq	45752(%r13), %rdi
	leaq	37592(%r13), %rsi
	movq	$0, -56(%rbp)
	movq	$0, -48(%rbp)
	call	_ZN2v88internal4wasm12_GLOBAL__N_123TryAllocateBackingStoreEPNS1_17WasmMemoryTrackerEPNS0_4HeapEmmPPvPm
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L729
	movq	32(%r13), %r14
	addq	%rbx, %r14
	movq	%r14, %rax
	subq	48(%r13), %rax
	movq	%r14, 32(%r13)
	cmpq	$33554432, %rax
	jg	.L744
.L731:
	testq	%rbx, %rbx
	je	.L732
	cmpq	40(%r13), %r14
	jg	.L745
.L732:
	movl	$1, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal7Factory16NewJSArrayBufferENS0_10SharedFlagENS0_14AllocationTypeE@PLT
	subq	$8, %rsp
	movq	%r12, %rcx
	xorl	%edx, %edx
	pushq	$1
	movq	%rax, %r14
	movq	%rax, %rdi
	xorl	%r9d, %r9d
	movq	%rbx, %r8
	movq	%r13, %rsi
	movq	%r14, %r12
	call	_ZN2v88internal13JSArrayBuffer5SetupENS0_6HandleIS1_EEPNS0_7IsolateEbPvmNS0_10SharedFlagEb@PLT
	movq	(%r14), %rdx
	movl	39(%rdx), %eax
	andl	$-3, %eax
	movl	%eax, 39(%rdx)
	popq	%rax
	popq	%rdx
	jmp	.L729
	.p2align 4,,10
	.p2align 3
.L744:
	movq	%r13, %rdi
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	jmp	.L731
	.p2align 4,,10
	.p2align 3
.L745:
	movq	%r13, %rdi
	call	_ZN2v87Isolate36ReportExternalAllocationLimitReachedEv@PLT
	jmp	.L732
.L743:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20088:
	.size	_ZN2v88internal4wasm14NewArrayBufferEPNS0_7IsolateEm, .-_ZN2v88internal4wasm14NewArrayBufferEPNS0_7IsolateEm
	.section	.text._ZN2v88internal4wasm20NewSharedArrayBufferEPNS0_7IsolateEmm,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm20NewSharedArrayBufferEPNS0_7IsolateEmm
	.type	_ZN2v88internal4wasm20NewSharedArrayBufferEPNS0_7IsolateEmm, @function
_ZN2v88internal4wasm20NewSharedArrayBufferEPNS0_7IsolateEmm:
.LFB20089:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	xorl	%r14d, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm13max_mem_pagesEv@PLT
	movl	%eax, %eax
	salq	$16, %rax
	cmpq	%rax, %rbx
	jbe	.L760
.L747:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L761
	leaq	-32(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L760:
	.cfi_restore_state
	leaq	-48(%rbp), %r9
	leaq	-56(%rbp), %r8
	movq	%r12, %rcx
	movq	%rbx, %rdx
	movq	45752(%r13), %rdi
	leaq	37592(%r13), %rsi
	movq	$0, -56(%rbp)
	movq	$0, -48(%rbp)
	call	_ZN2v88internal4wasm12_GLOBAL__N_123TryAllocateBackingStoreEPNS1_17WasmMemoryTrackerEPNS0_4HeapEmmPPvPm
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L747
	movq	32(%r13), %r12
	addq	%rbx, %r12
	movq	%r12, %rax
	subq	48(%r13), %rax
	movq	%r12, 32(%r13)
	cmpq	$33554432, %rax
	jg	.L762
.L749:
	testq	%rbx, %rbx
	je	.L750
	cmpq	40(%r13), %r12
	jg	.L763
.L750:
	movl	$1, %edx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal7Factory16NewJSArrayBufferENS0_10SharedFlagENS0_14AllocationTypeE@PLT
	subq	$8, %rsp
	movq	%r14, %rcx
	xorl	%edx, %edx
	pushq	$1
	movq	%rax, %r12
	movq	%rax, %rdi
	movl	$1, %r9d
	movq	%rbx, %r8
	movq	%r13, %rsi
	movq	%r12, %r14
	call	_ZN2v88internal13JSArrayBuffer5SetupENS0_6HandleIS1_EEPNS0_7IsolateEbPvmNS0_10SharedFlagEb@PLT
	movq	(%r12), %rdx
	movl	39(%rdx), %eax
	andl	$-3, %eax
	movl	%eax, 39(%rdx)
	popq	%rax
	popq	%rdx
	jmp	.L747
	.p2align 4,,10
	.p2align 3
.L762:
	movq	%r13, %rdi
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	jmp	.L749
	.p2align 4,,10
	.p2align 3
.L763:
	movq	%r13, %rdi
	call	_ZN2v87Isolate36ReportExternalAllocationLimitReachedEv@PLT
	jmp	.L750
.L761:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20089:
	.size	_ZN2v88internal4wasm20NewSharedArrayBufferEPNS0_7IsolateEmm, .-_ZN2v88internal4wasm20NewSharedArrayBufferEPNS0_7IsolateEmm
	.section	.text._ZNSt10_HashtableIPN2v88internal7IsolateES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIPN2v88internal7IsolateES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPN2v88internal7IsolateES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm
	.type	_ZNSt10_HashtableIPN2v88internal7IsolateES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm, @function
_ZNSt10_HashtableIPN2v88internal7IsolateES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm:
.LFB25002:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L765
	movq	(%rbx), %r8
.L766:
	movq	(%r8,%r15,8), %rax
	leaq	0(,%r15,8), %rcx
	testq	%rax, %rax
	je	.L775
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r13, (%rax)
.L776:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L765:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L789
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L790
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%rbx), %r10
	movq	%rax, %r8
.L768:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L770
	xorl	%edi, %edi
	leaq	16(%rbx), %r9
	jmp	.L771
	.p2align 4,,10
	.p2align 3
.L772:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L773:
	testq	%rsi, %rsi
	je	.L770
.L771:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	8(%rcx), %rax
	divq	%r12
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L772
	movq	16(%rbx), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L778
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L771
	.p2align 4,,10
	.p2align 3
.L770:
	movq	(%rbx), %rdi
	cmpq	%r10, %rdi
	je	.L774
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L774:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	divq	%r12
	movq	%r8, (%rbx)
	movq	%rdx, %r15
	jmp	.L766
	.p2align 4,,10
	.p2align 3
.L775:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L777
	movq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r13, (%rax,%rdx,8)
.L777:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L776
	.p2align 4,,10
	.p2align 3
.L778:
	movq	%rdx, %rdi
	jmp	.L773
	.p2align 4,,10
	.p2align 3
.L789:
	leaq	48(%rbx), %r8
	movq	$0, 48(%rbx)
	movq	%r8, %r10
	jmp	.L768
.L790:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE25002:
	.size	_ZNSt10_HashtableIPN2v88internal7IsolateES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm, .-_ZNSt10_HashtableIPN2v88internal7IsolateES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm
	.section	.text._ZNSt10_HashtableIPN2v88internal7IsolateES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE10_M_emplaceIJRS3_EEESt4pairINS5_14_Node_iteratorIS3_Lb1ELb0EEEbESt17integral_constantIbLb1EEDpOT_,"axG",@progbits,_ZNSt10_HashtableIPN2v88internal7IsolateES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE10_M_emplaceIJRS3_EEESt4pairINS5_14_Node_iteratorIS3_Lb1ELb0EEEbESt17integral_constantIbLb1EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPN2v88internal7IsolateES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE10_M_emplaceIJRS3_EEESt4pairINS5_14_Node_iteratorIS3_Lb1ELb0EEEbESt17integral_constantIbLb1EEDpOT_
	.type	_ZNSt10_HashtableIPN2v88internal7IsolateES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE10_M_emplaceIJRS3_EEESt4pairINS5_14_Node_iteratorIS3_Lb1ELb0EEEbESt17integral_constantIbLb1EEDpOT_, @function
_ZNSt10_HashtableIPN2v88internal7IsolateES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE10_M_emplaceIJRS3_EEESt4pairINS5_14_Node_iteratorIS3_Lb1ELb0EEEbESt17integral_constantIbLb1EEDpOT_:
.LFB24504:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	$16, %edi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_Znwm@PLT
	movq	(%rbx), %r9
	movq	8(%r12), %rcx
	xorl	%edx, %edx
	movq	$0, (%rax)
	movq	%rax, %rdi
	movq	%r9, 8(%rax)
	movq	%r9, %rax
	divq	%rcx
	movq	(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	testq	%rax, %rax
	je	.L792
	movq	(%rax), %rbx
	movq	8(%rbx), %rsi
	jmp	.L794
	.p2align 4,,10
	.p2align 3
.L805:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L792
	movq	8(%rbx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rcx
	cmpq	%rdx, %r10
	jne	.L792
.L794:
	cmpq	%rsi, %r9
	jne	.L805
	call	_ZdlPv@PLT
	movq	%rbx, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L792:
	.cfi_restore_state
	movq	%rdi, %rcx
	movq	%r9, %rdx
	movq	%r12, %rdi
	movl	$1, %r8d
	movq	%r10, %rsi
	call	_ZNSt10_HashtableIPN2v88internal7IsolateES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm
	popq	%rbx
	movl	$1, %edx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24504:
	.size	_ZNSt10_HashtableIPN2v88internal7IsolateES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE10_M_emplaceIJRS3_EEESt4pairINS5_14_Node_iteratorIS3_Lb1ELb0EEEbESt17integral_constantIbLb1EEDpOT_, .-_ZNSt10_HashtableIPN2v88internal7IsolateES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE10_M_emplaceIJRS3_EEESt4pairINS5_14_Node_iteratorIS3_Lb1ELb0EEEbESt17integral_constantIbLb1EEDpOT_
	.section	.text._ZN2v88internal4wasm17WasmMemoryTracker26RegisterWasmMemoryAsSharedENS0_6HandleINS0_16WasmMemoryObjectEEEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm17WasmMemoryTracker26RegisterWasmMemoryAsSharedENS0_6HandleINS0_16WasmMemoryObjectEEEPNS0_7IsolateE
	.type	_ZN2v88internal4wasm17WasmMemoryTracker26RegisterWasmMemoryAsSharedENS0_6HandleINS0_16WasmMemoryObjectEEEPNS0_7IsolateE, @function
_ZN2v88internal4wasm17WasmMemoryTracker26RegisterWasmMemoryAsSharedENS0_6HandleINS0_16WasmMemoryObjectEEEPNS0_7IsolateE:
.LFB20065:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	cmpb	$0, _ZN2v88internal28FLAG_wasm_grow_shared_memoryE(%rip)
	movq	%rdx, -40(%rbp)
	jne	.L834
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L834:
	.cfi_restore_state
	movq	(%rsi), %rax
	leaq	8(%rdi), %r12
	movq	%rdi, %rbx
	movq	%rsi, %r14
	movq	%r12, %rdi
	movq	23(%rax), %rax
	movq	31(%rax), %r13
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	64(%rbx), %r8
	xorl	%edx, %edx
	movq	%r13, %rax
	divq	%r8
	movq	56(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %rsi
	testq	%rax, %rax
	je	.L833
	movq	(%rax), %rcx
	movq	8(%rcx), %rdi
	jmp	.L810
	.p2align 4,,10
	.p2align 3
.L835:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L833
	movq	8(%rcx), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%r8
	cmpq	%rdx, %rsi
	jne	.L833
.L810:
	cmpq	%rdi, %r13
	jne	.L835
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-40(%rbp), %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm17WasmMemoryTracker31RegisterSharedWasmMemory_LockedENS0_6HandleINS0_16WasmMemoryObjectEEEPNS0_7IsolateE
	movq	120(%rbx), %rsi
	movq	%r13, %rax
	xorl	%edx, %edx
	divq	%rsi
	movq	112(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r14
	testq	%rax, %rax
	je	.L812
	movq	(%rax), %rdi
	movq	8(%rdi), %rcx
	jmp	.L813
	.p2align 4,,10
	.p2align 3
.L836:
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L812
	movq	8(%rdi), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%rdx, %r14
	jne	.L812
.L813:
	cmpq	%rcx, %r13
	jne	.L836
	addq	$16, %rdi
.L815:
	leaq	-40(%rbp), %rsi
	call	_ZNSt10_HashtableIPN2v88internal7IsolateES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE10_M_emplaceIJRS3_EEESt4pairINS5_14_Node_iteratorIS3_Lb1ELb0EEEbESt17integral_constantIbLb1EEDpOT_
.L833:
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L812:
	.cfi_restore_state
	movl	$72, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	112(%rbx), %rdi
	movq	%r13, %rdx
	movq	%r13, 8(%rax)
	movq	%rax, %rcx
	leaq	64(%rax), %rax
	movq	%r14, %rsi
	movq	$0, -64(%rax)
	movl	$1, %r8d
	movups	%xmm0, -16(%rax)
	movq	$0, (%rax)
	movq	%rax, 16(%rcx)
	movq	$1, 24(%rcx)
	movq	$0, 32(%rcx)
	movq	$0, 40(%rcx)
	movl	$0x3f800000, 48(%rcx)
	movq	$0, 56(%rcx)
	call	_ZNSt10_HashtableIPKvSt4pairIKS1_St13unordered_setIPN2v88internal7IsolateESt4hashIS8_ESt8equal_toIS8_ESaIS8_EEESaISF_ENSt8__detail10_Select1stESB_IS1_ES9_IS1_ENSH_18_Mod_range_hashingENSH_20_Default_ranged_hashENSH_20_Prime_rehash_policyENSH_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSH_10_Hash_nodeISF_Lb0EEEm
	leaq	16(%rax), %rdi
	jmp	.L815
	.cfi_endproc
.LFE20065:
	.size	_ZN2v88internal4wasm17WasmMemoryTracker26RegisterWasmMemoryAsSharedENS0_6HandleINS0_16WasmMemoryObjectEEEPNS0_7IsolateE, .-_ZN2v88internal4wasm17WasmMemoryTracker26RegisterWasmMemoryAsSharedENS0_6HandleINS0_16WasmMemoryObjectEEEPNS0_7IsolateE
	.section	.text._ZN2v88internal4wasm17WasmMemoryTracker41UpdateSharedMemoryStateOnInterrupt_LockedEPNS0_7IsolateEPvm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm17WasmMemoryTracker41UpdateSharedMemoryStateOnInterrupt_LockedEPNS0_7IsolateEPvm
	.type	_ZN2v88internal4wasm17WasmMemoryTracker41UpdateSharedMemoryStateOnInterrupt_LockedEPNS0_7IsolateEPvm, @function
_ZN2v88internal4wasm17WasmMemoryTracker41UpdateSharedMemoryStateOnInterrupt_LockedEPNS0_7IsolateEPvm:
.LFB20071:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal4wasm17WasmMemoryTracker30MemoryObjectsNeedUpdate_LockedEPNS0_7IsolateEPKv
	testb	%al, %al
	jne	.L854
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L854:
	.cfi_restore_state
	movq	%r13, %rcx
	movq	%r12, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm17WasmMemoryTracker36UpdateMemoryObjectsForIsolate_LockedEPNS0_7IsolateEPvm
	movq	176(%rbx), %rsi
	movq	%r12, %rax
	xorl	%edx, %edx
	divq	%rsi
	movq	168(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r13
	testq	%rax, %rax
	je	.L839
	movq	(%rax), %rdi
	movq	8(%rdi), %rcx
	jmp	.L841
	.p2align 4,,10
	.p2align 3
.L855:
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L839
	movq	8(%rdi), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%rdx, %r13
	jne	.L839
.L841:
	cmpq	%rcx, %r12
	jne	.L855
	addq	$16, %rdi
.L842:
	leaq	-40(%rbp), %rsi
	call	_ZNSt10_HashtableIPN2v88internal7IsolateES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE10_M_emplaceIJRS3_EEESt4pairINS5_14_Node_iteratorIS3_Lb1ELb0EEEbESt17integral_constantIbLb1EEDpOT_
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L839:
	.cfi_restore_state
	movl	$72, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	$0, (%rax)
	movq	%rax, %rcx
	leaq	64(%rax), %rax
	leaq	168(%rbx), %rdi
	movq	%r12, -56(%rax)
	movl	$1, %r8d
	movups	%xmm0, -16(%rax)
	movq	$0, (%rax)
	movq	%rax, 16(%rcx)
	movq	$1, 24(%rcx)
	movq	$0, 32(%rcx)
	movq	$0, 40(%rcx)
	movl	$0x3f800000, 48(%rcx)
	movq	$0, 56(%rcx)
	call	_ZNSt10_HashtableIPKvSt4pairIKS1_St13unordered_setIPN2v88internal7IsolateESt4hashIS8_ESt8equal_toIS8_ESaIS8_EEESaISF_ENSt8__detail10_Select1stESB_IS1_ES9_IS1_ENSH_18_Mod_range_hashingENSH_20_Default_ranged_hashENSH_20_Prime_rehash_policyENSH_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSH_10_Hash_nodeISF_Lb0EEEm
	leaq	16(%rax), %rdi
	jmp	.L842
	.cfi_endproc
.LFE20071:
	.size	_ZN2v88internal4wasm17WasmMemoryTracker41UpdateSharedMemoryStateOnInterrupt_LockedEPNS0_7IsolateEPvm, .-_ZN2v88internal4wasm17WasmMemoryTracker41UpdateSharedMemoryStateOnInterrupt_LockedEPNS0_7IsolateEPvm
	.section	.text._ZN2v88internal4wasm17WasmMemoryTracker27UpdateSharedMemoryInstancesEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm17WasmMemoryTracker27UpdateSharedMemoryInstancesEPNS0_7IsolateE
	.type	_ZN2v88internal4wasm17WasmMemoryTracker27UpdateSharedMemoryInstancesEPNS0_7IsolateE, @function
_ZN2v88internal4wasm17WasmMemoryTracker27UpdateSharedMemoryInstancesEPNS0_7IsolateE:
.LFB20067:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	8(%rdi), %rax
	movq	%rax, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	240(%r14), %r12
	testq	%r12, %r12
	jne	.L857
	jmp	.L858
	.p2align 4,,10
	.p2align 3
.L859:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm17WasmMemoryTracker28AreAllIsolatesUpdated_LockedEPKv
	testb	%al, %al
	jne	.L891
.L863:
	movq	(%r12), %r12
	testq	%r12, %r12
	je	.L858
.L857:
	movq	8(%r12), %r13
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	%rbx, -64(%rbp)
	movq	16(%r12), %r15
	movq	%r13, %rdx
	call	_ZN2v88internal4wasm17WasmMemoryTracker30MemoryObjectsNeedUpdate_LockedEPNS0_7IsolateEPKv
	testb	%al, %al
	je	.L859
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm17WasmMemoryTracker36UpdateMemoryObjectsForIsolate_LockedEPNS0_7IsolateEPvm
	movq	176(%r14), %rsi
	movq	%r13, %rax
	xorl	%edx, %edx
	divq	%rsi
	movq	168(%r14), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r15
	testq	%rax, %rax
	je	.L860
	movq	(%rax), %rdi
	movq	8(%rdi), %rcx
	jmp	.L862
	.p2align 4,,10
	.p2align 3
.L892:
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L860
	movq	8(%rdi), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%rdx, %r15
	jne	.L860
.L862:
	cmpq	%rcx, %r13
	jne	.L892
	addq	$16, %rdi
.L871:
	leaq	-64(%rbp), %rsi
	call	_ZNSt10_HashtableIPN2v88internal7IsolateES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE10_M_emplaceIJRS3_EEESt4pairINS5_14_Node_iteratorIS3_Lb1ELb0EEEbESt17integral_constantIbLb1EEDpOT_
	movq	8(%r12), %r13
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal4wasm17WasmMemoryTracker28AreAllIsolatesUpdated_LockedEPKv
	testb	%al, %al
	je	.L863
.L891:
	movq	232(%r14), %rsi
	movq	8(%r12), %rax
	xorl	%edx, %edx
	movq	224(%r14), %r8
	divq	%rsi
	leaq	0(,%rdx,8), %r9
	movq	%rdx, %rdi
	leaq	(%r8,%r9), %r10
	movq	(%r10), %rax
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L864:
	movq	%rdx, %rcx
	movq	(%rdx), %rdx
	cmpq	%rdx, %r12
	jne	.L864
	movq	(%r12), %r15
	cmpq	%rcx, %rax
	je	.L893
	testq	%r15, %r15
	je	.L894
	movq	8(%r15), %rax
	xorl	%edx, %edx
	divq	%rsi
	cmpq	%rdx, %rdi
	je	.L867
	movq	%rcx, (%r8,%rdx,8)
	movq	(%r12), %r15
.L867:
	movq	%r15, (%rcx)
	movq	%r12, %rdi
	movq	%r15, %r12
	call	_ZdlPv@PLT
	subq	$1, 248(%r14)
	testq	%r12, %r12
	jne	.L857
	.p2align 4,,10
	.p2align 3
.L858:
	movq	-72(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L895
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L893:
	.cfi_restore_state
	testq	%r15, %r15
	je	.L873
	movq	8(%r15), %rax
	xorl	%edx, %edx
	divq	%rsi
	cmpq	%rdx, %rdi
	je	.L867
	movq	%rcx, (%r8,%rdx,8)
	addq	224(%r14), %r9
	leaq	240(%r14), %rdx
	movq	(%r9), %rax
	movq	%r9, %r10
	cmpq	%rdx, %rax
	je	.L896
.L868:
	movq	$0, (%r10)
	movq	(%r12), %r15
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L894:
	movq	$0, (%rcx)
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	subq	$1, 248(%r14)
	jmp	.L858
	.p2align 4,,10
	.p2align 3
.L860:
	movl	$72, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	$0, (%rax)
	movq	%rax, %rcx
	leaq	64(%rax), %rax
	movss	.LC9(%rip), %xmm1
	movq	%r13, -56(%rax)
	leaq	168(%r14), %rdi
	movl	$1, %r8d
	movups	%xmm0, -16(%rax)
	movq	$0, (%rax)
	movq	%rax, 16(%rcx)
	movq	$1, 24(%rcx)
	movq	$0, 32(%rcx)
	movq	$0, 40(%rcx)
	movq	$0, 56(%rcx)
	movss	%xmm1, 48(%rcx)
	call	_ZNSt10_HashtableIPKvSt4pairIKS1_St13unordered_setIPN2v88internal7IsolateESt4hashIS8_ESt8equal_toIS8_ESaIS8_EEESaISF_ENSt8__detail10_Select1stESB_IS1_ES9_IS1_ENSH_18_Mod_range_hashingENSH_20_Default_ranged_hashENSH_20_Prime_rehash_policyENSH_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSH_10_Hash_nodeISF_Lb0EEEm
	leaq	16(%rax), %rdi
	jmp	.L871
	.p2align 4,,10
	.p2align 3
.L873:
	movq	%rcx, %rax
	leaq	240(%r14), %rdx
	cmpq	%rdx, %rax
	jne	.L868
.L896:
	movq	%r15, 240(%r14)
	jmp	.L868
.L895:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20067:
	.size	_ZN2v88internal4wasm17WasmMemoryTracker27UpdateSharedMemoryInstancesEPNS0_7IsolateE, .-_ZN2v88internal4wasm17WasmMemoryTracker27UpdateSharedMemoryInstancesEPNS0_7IsolateE
	.section	.text._ZNSt10_HashtableIPvSt4pairIKS0_mESaIS3_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIPvSt4pairIKS0_mESaIS3_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPvSt4pairIKS0_mESaIS3_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm
	.type	_ZNSt10_HashtableIPvSt4pairIKS0_mESaIS3_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm, @function
_ZNSt10_HashtableIPvSt4pairIKS0_mESaIS3_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm:
.LFB25050:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L898
	movq	(%rbx), %r8
.L899:
	movq	(%r8,%r15,8), %rax
	leaq	0(,%r15,8), %rcx
	testq	%rax, %rax
	je	.L908
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r13, (%rax)
.L909:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L898:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L922
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L923
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%rbx), %r10
	movq	%rax, %r8
.L901:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L903
	xorl	%edi, %edi
	leaq	16(%rbx), %r9
	jmp	.L904
	.p2align 4,,10
	.p2align 3
.L905:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L906:
	testq	%rsi, %rsi
	je	.L903
.L904:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	8(%rcx), %rax
	divq	%r12
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L905
	movq	16(%rbx), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L911
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L904
	.p2align 4,,10
	.p2align 3
.L903:
	movq	(%rbx), %rdi
	cmpq	%r10, %rdi
	je	.L907
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L907:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	divq	%r12
	movq	%r8, (%rbx)
	movq	%rdx, %r15
	jmp	.L899
	.p2align 4,,10
	.p2align 3
.L908:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L910
	movq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r13, (%rax,%rdx,8)
.L910:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L909
	.p2align 4,,10
	.p2align 3
.L911:
	movq	%rdx, %rdi
	jmp	.L906
	.p2align 4,,10
	.p2align 3
.L922:
	leaq	48(%rbx), %r8
	movq	$0, 48(%rbx)
	movq	%r8, %r10
	jmp	.L901
.L923:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE25050:
	.size	_ZNSt10_HashtableIPvSt4pairIKS0_mESaIS3_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm, .-_ZNSt10_HashtableIPvSt4pairIKS0_mESaIS3_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm
	.section	.rodata._ZN2v88internal4wasm17WasmMemoryTracker25AddBufferToGrowMap_LockedENS0_6HandleINS0_13JSArrayBufferEEEm.str1.1,"aMS",@progbits,1
.LC10:
	.string	"entry->second <= new_size"
	.section	.text._ZN2v88internal4wasm17WasmMemoryTracker25AddBufferToGrowMap_LockedENS0_6HandleINS0_13JSArrayBufferEEEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm17WasmMemoryTracker25AddBufferToGrowMap_LockedENS0_6HandleINS0_13JSArrayBufferEEEm
	.type	_ZN2v88internal4wasm17WasmMemoryTracker25AddBufferToGrowMap_LockedENS0_6HandleINS0_13JSArrayBufferEEEm, @function
_ZN2v88internal4wasm17WasmMemoryTracker25AddBufferToGrowMap_LockedENS0_6HandleINS0_13JSArrayBufferEEEm:
.LFB20069:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	xorl	%edx, %edx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rsi), %rax
	movq	232(%rdi), %rsi
	movq	31(%rax), %r12
	movq	%r12, %rax
	divq	%rsi
	movq	224(%rdi), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L925
	movq	(%rax), %rcx
	movq	%rdx, %r8
	movq	8(%rcx), %rdi
	jmp	.L927
	.p2align 4,,10
	.p2align 3
.L966:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L925
	movq	8(%rcx), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rsi
	cmpq	%rdx, %r8
	jne	.L925
.L927:
	cmpq	%rdi, %r12
	jne	.L966
	cmpq	%r13, 16(%rcx)
	ja	.L967
	movq	%r13, 16(%rcx)
	movq	176(%rbx), %rsi
	movq	%r12, %rax
	xorl	%edx, %edx
	divq	%rsi
	movq	168(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %rdi
	testq	%rax, %rax
	je	.L924
	movq	(%rax), %rbx
	movq	8(%rbx), %rcx
	jmp	.L933
	.p2align 4,,10
	.p2align 3
.L968:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L924
	movq	8(%rbx), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%rdx, %rdi
	jne	.L924
.L933:
	cmpq	%rcx, %r12
	jne	.L968
	movq	32(%rbx), %r12
	testq	%r12, %r12
	je	.L934
	.p2align 4,,10
	.p2align 3
.L935:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L935
.L934:
	movq	24(%rbx), %rax
	movq	16(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	$0, 40(%rbx)
	movq	$0, 32(%rbx)
.L924:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L925:
	.cfi_restore_state
	movl	$24, %edi
	call	_Znwm@PLT
	movq	232(%rbx), %r8
	xorl	%edx, %edx
	movq	$0, (%rax)
	movq	%rax, %rdi
	movq	%r12, 8(%rax)
	movq	%r13, 16(%rax)
	movq	%r12, %rax
	divq	%r8
	movq	224(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L928
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L930
	.p2align 4,,10
	.p2align 3
.L969:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L928
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%r8
	cmpq	%rdx, %r9
	jne	.L928
.L930:
	cmpq	%rsi, %r12
	jne	.L969
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L967:
	.cfi_restore_state
	leaq	.LC10(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L928:
	addq	$8, %rsp
	movq	%rdi, %rcx
	movq	%r12, %rdx
	movl	$1, %r8d
	leaq	224(%rbx), %r10
	movq	%r9, %rsi
	popq	%rbx
	popq	%r12
	movq	%r10, %rdi
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt10_HashtableIPvSt4pairIKS0_mESaIS3_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm
	.cfi_endproc
.LFE20069:
	.size	_ZN2v88internal4wasm17WasmMemoryTracker25AddBufferToGrowMap_LockedENS0_6HandleINS0_13JSArrayBufferEEEm, .-_ZN2v88internal4wasm17WasmMemoryTracker25AddBufferToGrowMap_LockedENS0_6HandleINS0_13JSArrayBufferEEEm
	.section	.text.unlikely._ZN2v88internal4wasm17WasmMemoryTracker22SetPendingUpdateOnGrowENS0_6HandleINS0_13JSArrayBufferEEEm,"ax",@progbits
	.align 2
.LCOLDB11:
	.section	.text._ZN2v88internal4wasm17WasmMemoryTracker22SetPendingUpdateOnGrowENS0_6HandleINS0_13JSArrayBufferEEEm,"ax",@progbits
.LHOTB11:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm17WasmMemoryTracker22SetPendingUpdateOnGrowENS0_6HandleINS0_13JSArrayBufferEEEm
	.type	_ZN2v88internal4wasm17WasmMemoryTracker22SetPendingUpdateOnGrowENS0_6HandleINS0_13JSArrayBufferEEEm, @function
_ZN2v88internal4wasm17WasmMemoryTracker22SetPendingUpdateOnGrowENS0_6HandleINS0_13JSArrayBufferEEEm:
.LFB20066:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	8(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm17WasmMemoryTracker25AddBufferToGrowMap_LockedENS0_6HandleINS0_13JSArrayBufferEEEm
	movq	(%r12), %rax
	movq	120(%rbx), %rsi
	xorl	%edx, %edx
	movq	31(%rax), %r8
	movq	%r8, %rax
	divq	%rsi
	movq	112(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L971
	movq	(%rax), %rcx
	movq	%rdx, %r9
	movq	8(%rcx), %rdi
	jmp	.L973
	.p2align 4,,10
	.p2align 3
.L986:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L971
	movq	8(%rcx), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rsi
	cmpq	%rdx, %r9
	jne	.L985
.L973:
	cmpq	%rdi, %r8
	jne	.L986
	movq	32(%rcx), %rbx
	testq	%rbx, %rbx
	je	.L975
	.p2align 4,,10
	.p2align 3
.L974:
	movq	8(%rbx), %rax
	movl	$32, %esi
	leaq	37512(%rax), %rdi
	call	_ZN2v88internal10StackGuard16RequestInterruptENS1_13InterruptFlagE@PLT
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L974
.L975:
	popq	%rbx
	movq	%r14, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
.L985:
	.cfi_restore_state
	jmp	.L971
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal4wasm17WasmMemoryTracker22SetPendingUpdateOnGrowENS0_6HandleINS0_13JSArrayBufferEEEm
	.cfi_startproc
	.type	_ZN2v88internal4wasm17WasmMemoryTracker22SetPendingUpdateOnGrowENS0_6HandleINS0_13JSArrayBufferEEEm.cold, @function
_ZN2v88internal4wasm17WasmMemoryTracker22SetPendingUpdateOnGrowENS0_6HandleINS0_13JSArrayBufferEEEm.cold:
.LFSB20066:
.L971:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movq	32, %rax
	ud2
	.cfi_endproc
.LFE20066:
	.section	.text._ZN2v88internal4wasm17WasmMemoryTracker22SetPendingUpdateOnGrowENS0_6HandleINS0_13JSArrayBufferEEEm
	.size	_ZN2v88internal4wasm17WasmMemoryTracker22SetPendingUpdateOnGrowENS0_6HandleINS0_13JSArrayBufferEEEm, .-_ZN2v88internal4wasm17WasmMemoryTracker22SetPendingUpdateOnGrowENS0_6HandleINS0_13JSArrayBufferEEEm
	.section	.text.unlikely._ZN2v88internal4wasm17WasmMemoryTracker22SetPendingUpdateOnGrowENS0_6HandleINS0_13JSArrayBufferEEEm
	.size	_ZN2v88internal4wasm17WasmMemoryTracker22SetPendingUpdateOnGrowENS0_6HandleINS0_13JSArrayBufferEEEm.cold, .-_ZN2v88internal4wasm17WasmMemoryTracker22SetPendingUpdateOnGrowENS0_6HandleINS0_13JSArrayBufferEEEm.cold
.LCOLDE11:
	.section	.text._ZN2v88internal4wasm17WasmMemoryTracker22SetPendingUpdateOnGrowENS0_6HandleINS0_13JSArrayBufferEEEm
.LHOTE11:
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal4wasm17WasmMemoryTrackerD2Ev,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal4wasm17WasmMemoryTrackerD2Ev, @function
_GLOBAL__sub_I__ZN2v88internal4wasm17WasmMemoryTrackerD2Ev:
.LFB25865:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE25865:
	.size	_GLOBAL__sub_I__ZN2v88internal4wasm17WasmMemoryTrackerD2Ev, .-_GLOBAL__sub_I__ZN2v88internal4wasm17WasmMemoryTrackerD2Ev
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal4wasm17WasmMemoryTrackerD2Ev
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC9:
	.long	1065353216
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
