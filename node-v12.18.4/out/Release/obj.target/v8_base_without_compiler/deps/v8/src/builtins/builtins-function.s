	.file	"builtins-function.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB5450:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE5450:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB5451:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5451:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB5453:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5453:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.rodata._ZN2v88internalL38Builtin_Impl_FunctionPrototypeToStringENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"Function.prototype.toString"
.LC1:
	.string	"(location_) != nullptr"
.LC2:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internalL38Builtin_Impl_FunctionPrototypeToStringENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL38Builtin_Impl_FunctionPrototypeToStringENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL38Builtin_Impl_FunctionPrototypeToStringENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB20946:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -48
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L24
.L13:
	leaq	.LC0(%rip), %rax
	xorl	%edx, %edx
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	leaq	2560(%r12), %r14
	movq	$27, -56(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L25
	movq	%r14, %rcx
	xorl	%r8d, %r8d
	movl	$92, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r14
.L10:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L18
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L18:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L26
	addq	$32, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$1104, 11(%rdx)
	je	.L8
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	je	.L27
	movq	-1(%rax), %rdx
	cmpw	$1023, 11(%rdx)
	jbe	.L13
	movq	-1(%rax), %rax
	testb	$2, 13(%rax)
	je	.L13
	movq	2568(%r12), %r14
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L27:
	movq	%rsi, %rdi
	call	_ZN2v88internal10JSFunction8ToStringENS0_6HandleIS1_EE@PLT
	movq	(%rax), %r14
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L8:
	movq	%rsi, %rdi
	call	_ZN2v88internal15JSBoundFunction8ToStringENS0_6HandleIS1_EE@PLT
	movq	(%rax), %r14
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L25:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L26:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20946:
	.size	_ZN2v88internalL38Builtin_Impl_FunctionPrototypeToStringENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL38Builtin_Impl_FunctionPrototypeToStringENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internal12_GLOBAL__N_121CreateDynamicFunctionEPNS0_7IsolateENS0_16BuiltinArgumentsEPKc.str1.1,"aMS",@progbits,1
.LC3:
	.string	" anonymous("
.LC4:
	.string	") {\n"
.LC5:
	.string	"\n})"
	.section	.text._ZN2v88internal12_GLOBAL__N_121CreateDynamicFunctionEPNS0_7IsolateENS0_16BuiltinArgumentsEPKc,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_121CreateDynamicFunctionEPNS0_7IsolateENS0_16BuiltinArgumentsEPKc, @function
_ZN2v88internal12_GLOBAL__N_121CreateDynamicFunctionEPNS0_7IsolateENS0_16BuiltinArgumentsEPKc:
.LFB20927:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-96(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$104, %rsp
	movq	%rsi, -112(%rbp)
	movq	%rdx, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leal	-16(,%rsi,8), %eax
	movl	%eax, -116(%rbp)
	cltq
	subq	%rax, %rdx
	movq	(%rdx), %rax
	movq	%rdx, %r15
	movq	31(%rax), %rax
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal7Context12global_proxyEv@PLT
	movq	41112(%r12), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L29
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L30:
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8Builtins20AllowDynamicFunctionEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEENS4_INS0_8JSObjectEEE@PLT
	testb	%al, %al
	jne	.L32
	movl	$35, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate10CountUsageENS_7Isolate17UseCounterFeatureE@PLT
	leaq	88(%r12), %rax
.L33:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L184
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilderC1EPNS0_7IsolateE@PLT
	movq	-64(%rbp), %rax
	movl	-88(%rbp), %edi
	movq	(%rax), %rdx
	movl	-76(%rbp), %eax
	testl	%edi, %edi
	leal	1(%rax), %esi
	movl	%esi, -76(%rbp)
	jne	.L34
	addl	$16, %eax
	cltq
	movb	$40, -1(%rdx,%rax)
	movl	-76(%rbp), %eax
	cmpl	-80(%rbp), %eax
	je	.L177
.L35:
	movl	-88(%rbp), %ecx
	movzbl	(%rbx), %edx
	testl	%ecx, %ecx
	je	.L36
.L187:
	testb	%dl, %dl
	jne	.L38
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L178:
	movzbl	(%rbx), %edx
	testb	%dl, %dl
	je	.L41
.L38:
	movq	-64(%rbp), %rsi
	leal	1(%rax), %edi
	leal	16(%rax,%rax), %eax
	addq	$1, %rbx
	cltq
	movq	(%rsi), %rsi
	movl	%edi, -76(%rbp)
	movw	%dx, -1(%rsi,%rax)
	movl	-76(%rbp), %eax
	cmpl	-80(%rbp), %eax
	jne	.L178
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%rbx), %edx
	movl	-76(%rbp), %eax
	testb	%dl, %dl
	jne	.L38
	.p2align 4,,10
	.p2align 3
.L41:
	movl	-88(%rbp), %ebx
	testl	%ebx, %ebx
	je	.L39
.L37:
	movl	$32, %edx
	leaq	.LC3(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L46:
	movq	-64(%rbp), %rsi
	leal	1(%rax), %edi
	leal	16(%rax,%rax), %eax
	addq	$1, %rbx
	cltq
	movq	(%rsi), %rsi
	movl	%edi, -76(%rbp)
	movw	%dx, -1(%rsi,%rax)
	movl	-76(%rbp), %eax
	cmpl	-80(%rbp), %eax
	je	.L185
	movzbl	(%rbx), %edx
	testb	%dl, %dl
	jne	.L46
.L48:
	movq	-112(%rbp), %rcx
	movl	%ecx, %eax
	subl	$5, %eax
	movl	%eax, -128(%rbp)
	cmpl	$1, %eax
	jle	.L53
	movq	-104(%rbp), %rax
	leaq	-8(%rax), %rbx
	leal	-7(%rcx), %eax
	salq	$3, %rax
	movq	%rbx, %rcx
	movq	%rbx, %rsi
	subq	%rax, %rcx
	movq	(%rbx), %rax
	movq	%rcx, -112(%rbp)
	testb	$1, %al
	jne	.L61
	.p2align 4,,10
	.p2align 3
.L64:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L183
.L63:
	movq	(%rsi), %rax
	movq	-1(%rax), %rdx
	movq	%rax, %r8
	cmpw	$63, 11(%rdx)
	ja	.L67
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	je	.L186
.L67:
	movq	-1(%r8), %rax
	cmpw	$63, 11(%rax)
	ja	.L75
	movq	-1(%r8), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	jne	.L75
	movq	(%rsi), %rax
	movq	41112(%r12), %rdi
	movq	15(%rax), %r8
	testq	%rdi, %rdi
	je	.L78
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
	.p2align 4,,10
	.p2align 3
.L75:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	cmpq	-112(%rbp), %rbx
	je	.L53
	movl	-76(%rbp), %eax
	movl	-88(%rbp), %r10d
	movq	-64(%rbp), %rdx
	leal	1(%rax), %esi
	testl	%r10d, %r10d
	movq	(%rdx), %rdx
	movl	%esi, -76(%rbp)
	jne	.L57
	addl	$16, %eax
	cltq
	movb	$44, -1(%rdx,%rax)
	movl	-80(%rbp), %eax
	cmpl	%eax, -76(%rbp)
	je	.L58
.L59:
	movq	-8(%rbx), %rax
	subq	$8, %rbx
	movq	%rbx, %rsi
	testb	$1, %al
	je	.L64
.L61:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L64
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L34:
	leal	16(%rax,%rax), %eax
	movl	$40, %esi
	cltq
	movw	%si, -1(%rdx,%rax)
	movl	-76(%rbp), %eax
	cmpl	-80(%rbp), %eax
	jne	.L35
.L177:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movl	-88(%rbp), %ecx
	movl	-76(%rbp), %eax
	movzbl	(%rbx), %edx
	testl	%ecx, %ecx
	jne	.L187
.L36:
	testb	%dl, %dl
	jne	.L40
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L179:
	movzbl	(%rbx), %edx
	testb	%dl, %dl
	je	.L41
.L40:
	movq	-64(%rbp), %rsi
	leal	1(%rax), %edi
	addl	$16, %eax
	addq	$1, %rbx
	cltq
	movq	(%rsi), %rsi
	movl	%edi, -76(%rbp)
	movb	%dl, -1(%rsi,%rax)
	movl	-76(%rbp), %eax
	cmpl	-80(%rbp), %eax
	jne	.L179
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movl	-76(%rbp), %eax
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L29:
	movq	41088(%r12), %r14
	cmpq	%r14, 41096(%r12)
	je	.L188
.L31:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r14)
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L57:
	leal	16(%rax,%rax), %eax
	movl	$44, %r9d
	cltq
	movw	%r9w, -1(%rdx,%rax)
	movl	-80(%rbp), %eax
	cmpl	%eax, -76(%rbp)
	jne	.L59
.L58:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L185:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%rbx), %edx
	testb	%dl, %dl
	je	.L48
	movl	-76(%rbp), %eax
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L183:
	xorl	%eax, %eax
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L53:
	movl	-76(%rbp), %eax
	movq	-64(%rbp), %rdx
	movl	-88(%rbp), %r11d
	movq	(%rdx), %rdx
	leal	1(%rax), %ecx
	testl	%r11d, %r11d
	je	.L55
	leal	16(%rax,%rax), %eax
	movl	$10, %edi
	movl	%ecx, -76(%rbp)
	cltq
	movw	%di, -1(%rdx,%rax)
	movl	-80(%rbp), %eax
	cmpl	%eax, -76(%rbp)
	je	.L82
.L83:
	movq	%r13, %rdi
	call	_ZNK2v88internal24IncrementalStringBuilder6LengthEv@PLT
	movl	-88(%rbp), %esi
	movl	%eax, %ebx
	testl	%esi, %esi
	je	.L125
	movl	-76(%rbp), %eax
	movl	$41, %edx
	leaq	.LC4(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L86:
	movq	-64(%rbp), %rsi
	leal	1(%rax), %edi
	leal	16(%rax,%rax), %eax
	addq	$1, %rcx
	cltq
	movq	(%rsi), %rsi
	movl	%edi, -76(%rbp)
	movw	%dx, -1(%rsi,%rax)
	movl	-76(%rbp), %eax
	cmpl	-80(%rbp), %eax
	je	.L189
	movzbl	(%rcx), %edx
	testb	%dl, %dl
	jne	.L86
.L87:
	movl	-128(%rbp), %ecx
	testl	%ecx, %ecx
	jg	.L190
.L92:
	movl	-88(%rbp), %edx
	testl	%edx, %edx
	je	.L126
	movl	-76(%rbp), %eax
	movl	$10, %edx
	leaq	.LC5(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L98:
	movq	-64(%rbp), %rsi
	leal	1(%rax), %edi
	leal	16(%rax,%rax), %eax
	addq	$1, %rcx
	cltq
	movq	(%rsi), %rsi
	movl	%edi, -76(%rbp)
	movw	%dx, -1(%rsi,%rax)
	movl	-76(%rbp), %eax
	cmpl	-80(%rbp), %eax
	je	.L191
	movzbl	(%rcx), %edx
	testb	%dl, %dl
	jne	.L98
.L99:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6FinishEv@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L183
	movq	(%r15), %rax
	movq	31(%rax), %rax
	movq	39(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L105
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdi
.L106:
	movq	%r13, %rsi
	movl	%ebx, %ecx
	movl	$1, %edx
	call	_ZN2v88internal8Compiler21GetFunctionFromStringENS0_6HandleINS0_7ContextEEENS2_INS0_6ObjectEEENS0_16ParseRestrictionEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L183
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal9Execution4CallEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_iPS6_@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L183
	movq	(%rax), %rax
	movq	23(%rax), %rdx
	movl	47(%rdx), %eax
	orl	$67108864, %eax
	movl	%eax, 47(%rdx)
	movq	(%rbx), %rax
	movq	23(%rax), %rax
	movq	31(%rax), %rsi
	testb	$1, %sil
	jne	.L192
.L111:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L112
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L113:
	movl	27(%rsi), %eax
	testl	%eax, %eax
	je	.L193
.L115:
	movl	-116(%rbp), %eax
	movq	-104(%rbp), %rdx
	addl	$8, %eax
	cltq
	subq	%rax, %rdx
	movq	(%rdx), %rax
	cmpq	%rax, 88(%r12)
	je	.L116
	cmpq	%rdx, %r15
	je	.L116
	cmpq	(%r15), %rax
	jne	.L194
.L116:
	movq	%rbx, %rax
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L39:
	movl	$32, %edx
	leaq	.LC3(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L47:
	movq	-64(%rbp), %rsi
	leal	1(%rax), %edi
	addl	$16, %eax
	addq	$1, %rbx
	cltq
	movq	(%rsi), %rsi
	movl	%edi, -76(%rbp)
	movb	%dl, -1(%rsi,%rax)
	movl	-76(%rbp), %eax
	cmpl	-80(%rbp), %eax
	je	.L195
	movzbl	(%rbx), %edx
	testb	%dl, %dl
	jne	.L47
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L195:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%rbx), %edx
	testb	%dl, %dl
	je	.L48
	movl	-76(%rbp), %eax
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L55:
	addl	$16, %eax
	movl	%ecx, -76(%rbp)
	cltq
	movb	$10, -1(%rdx,%rax)
	movl	-80(%rbp), %eax
	cmpl	%eax, -76(%rbp)
	jne	.L83
.L82:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L78:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L196
.L80:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r8, (%rsi)
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L189:
	movq	%r13, %rdi
	movq	%rcx, -112(%rbp)
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movq	-112(%rbp), %rcx
	movzbl	(%rcx), %edx
	testb	%dl, %dl
	je	.L87
	movl	-76(%rbp), %eax
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L191:
	movq	%r13, %rdi
	movq	%rcx, -112(%rbp)
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movq	-112(%rbp), %rcx
	movzbl	(%rcx), %edx
	testb	%dl, %dl
	je	.L99
	movl	-76(%rbp), %eax
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L186:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L70
	movq	23(%rax), %rdx
	movl	11(%rdx), %r8d
	testl	%r8d, %r8d
	je	.L70
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	movq	%rax, %rsi
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L125:
	movl	$41, %ecx
	leaq	.LC4(%rip), %rdx
.L180:
	movl	-76(%rbp), %eax
.L85:
	movq	-64(%rbp), %rsi
	leal	1(%rax), %edi
	addl	$16, %eax
	addq	$1, %rdx
	cltq
	movq	(%rsi), %rsi
	movl	%edi, -76(%rbp)
	movb	%cl, -1(%rsi,%rax)
	movl	-76(%rbp), %eax
	cmpl	-80(%rbp), %eax
	je	.L197
	movzbl	(%rdx), %ecx
	testb	%cl, %cl
	jne	.L85
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L197:
	movq	%r13, %rdi
	movq	%rdx, -112(%rbp)
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movq	-112(%rbp), %rdx
	movzbl	(%rdx), %ecx
	testb	%cl, %cl
	jne	.L180
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L126:
	movl	$10, %ecx
	leaq	.LC5(%rip), %rdx
.L181:
	movl	-76(%rbp), %eax
.L97:
	movq	-64(%rbp), %rsi
	leal	1(%rax), %edi
	addl	$16, %eax
	addq	$1, %rdx
	cltq
	movq	(%rsi), %rsi
	movl	%edi, -76(%rbp)
	movb	%cl, -1(%rsi,%rax)
	movl	-76(%rbp), %eax
	cmpl	-80(%rbp), %eax
	je	.L198
	movzbl	(%rdx), %ecx
	testb	%cl, %cl
	jne	.L97
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L198:
	movq	%r13, %rdi
	movq	%rdx, -112(%rbp)
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movq	-112(%rbp), %rdx
	movzbl	(%rdx), %ecx
	testb	%cl, %cl
	jne	.L181
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L190:
	movl	-116(%rbp), %eax
	movq	-104(%rbp), %rsi
	subl	$24, %eax
	cltq
	subq	%rax, %rsi
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L93
.L96:
	movq	%r12, %rdi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rsi
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L33
.L95:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L70:
	movq	41112(%r12), %rdi
	movq	15(%rax), %r8
	testq	%rdi, %rdi
	je	.L72
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r8
	movq	%rax, %rsi
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L105:
	movq	41088(%r12), %rdi
	cmpq	41096(%r12), %rdi
	je	.L199
.L107:
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdi)
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L188:
	movq	%r12, %rdi
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-128(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L112:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L200
.L114:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L72:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L201
.L74:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r8, (%rsi)
	jmp	.L67
.L201:
	movq	%r12, %rdi
	movq	%r8, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L193:
	movabsq	$-8589934592, %rax
	movq	%rax, 23(%rsi)
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L192:
	movq	-1(%rsi), %rax
	cmpw	$86, 11(%rax)
	jne	.L111
	movq	23(%rsi), %rsi
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L93:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L96
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L194:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10JSFunction13GetDerivedMapEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10JSReceiverEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L183
	movq	(%rbx), %rax
	movq	23(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L118
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L119:
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r14, %rdx
	call	_ZN2v88internal3Map14AsLanguageModeEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_18SharedFunctionInfoEEE@PLT
	movq	41112(%r12), %rdi
	movq	%rax, %r13
	movq	(%rbx), %rax
	movq	31(%rax), %rsi
	testq	%rdi, %rdi
	je	.L121
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L122:
	xorl	%r8d, %r8d
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory33NewFunctionFromSharedFunctionInfoENS0_6HandleINS0_3MapEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_14AllocationTypeE@PLT
	movq	%rax, %rbx
	jmp	.L116
.L196:
	movq	%r12, %rdi
	movq	%r8, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L80
.L199:
	movq	%r12, %rdi
	movq	%rsi, -112(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-112(%rbp), %rsi
	movq	%rax, %rdi
	jmp	.L107
.L200:
	movq	%r12, %rdi
	movq	%rsi, -112(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-112(%rbp), %rsi
	jmp	.L114
.L121:
	movq	41088(%r12), %rcx
	cmpq	41096(%r12), %rcx
	je	.L202
.L123:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rcx)
	jmp	.L122
.L118:
	movq	41088(%r12), %r14
	cmpq	41096(%r12), %r14
	je	.L203
.L120:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r14)
	jmp	.L119
.L202:
	movq	%r12, %rdi
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L123
.L203:
	movq	%r12, %rdi
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L120
.L184:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20927:
	.size	_ZN2v88internal12_GLOBAL__N_121CreateDynamicFunctionEPNS0_7IsolateENS0_16BuiltinArgumentsEPKc, .-_ZN2v88internal12_GLOBAL__N_121CreateDynamicFunctionEPNS0_7IsolateENS0_16BuiltinArgumentsEPKc
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB8771:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L210
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L213
.L204:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L210:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L213:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L204
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE8771:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.rodata._ZN2v88internalL44Builtin_Impl_Stats_FunctionPrototypeToStringEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"disabled-by-default-v8.runtime"
	.align 8
.LC7:
	.string	"V8.Builtin_FunctionPrototypeToString"
	.section	.text._ZN2v88internalL44Builtin_Impl_Stats_FunctionPrototypeToStringEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL44Builtin_Impl_Stats_FunctionPrototypeToStringEiPmPNS0_7IsolateE, @function
_ZN2v88internalL44Builtin_Impl_Stats_FunctionPrototypeToStringEiPmPNS0_7IsolateE:
.LFB20944:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L243
.L215:
	movq	_ZZN2v88internalL44Builtin_Impl_Stats_FunctionPrototypeToStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic281(%rip), %rbx
	testq	%rbx, %rbx
	je	.L244
.L217:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L245
.L219:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL38Builtin_Impl_FunctionPrototypeToStringENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L246
.L223:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L247
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L244:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L248
.L218:
	movq	%rbx, _ZZN2v88internalL44Builtin_Impl_Stats_FunctionPrototypeToStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic281(%rip)
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L245:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L249
.L220:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L221
	movq	(%rdi), %rax
	call	*8(%rax)
.L221:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L222
	movq	(%rdi), %rax
	call	*8(%rax)
.L222:
	leaq	.LC7(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L246:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L243:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$775, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L249:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC7(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L248:
	leaq	.LC6(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L218
.L247:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20944:
	.size	_ZN2v88internalL44Builtin_Impl_Stats_FunctionPrototypeToStringEiPmPNS0_7IsolateE, .-_ZN2v88internalL44Builtin_Impl_Stats_FunctionPrototypeToStringEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL38Builtin_Impl_Stats_FunctionConstructorEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC8:
	.string	"V8.Builtin_FunctionConstructor"
	.section	.rodata._ZN2v88internalL38Builtin_Impl_Stats_FunctionConstructorEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC9:
	.string	"function"
	.section	.text._ZN2v88internalL38Builtin_Impl_Stats_FunctionConstructorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL38Builtin_Impl_Stats_FunctionConstructorEiPmPNS0_7IsolateE, @function
_ZN2v88internalL38Builtin_Impl_Stats_FunctionConstructorEiPmPNS0_7IsolateE:
.LFB20928:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L283
.L251:
	movq	_ZZN2v88internalL38Builtin_Impl_Stats_FunctionConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic136(%rip), %rbx
	testq	%rbx, %rbx
	je	.L284
.L253:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L285
.L255:
	leaq	.LC9(%rip), %rcx
	movq	%r14, %rsi
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	41088(%r12), %rbx
	movq	41096(%r12), %r15
	addl	$1, 41104(%r12)
	call	_ZN2v88internal12_GLOBAL__N_121CreateDynamicFunctionEPNS0_7IsolateENS0_16BuiltinArgumentsEPKc
	testq	%rax, %rax
	je	.L286
	movq	(%rax), %r13
.L260:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r15
	je	.L263
	movq	%r15, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L263:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L287
.L250:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L288
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L285:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L289
.L256:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L257
	movq	(%rdi), %rax
	call	*8(%rax)
.L257:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L258
	movq	(%rdi), %rax
	call	*8(%rax)
.L258:
	leaq	.LC8(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L284:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L290
.L254:
	movq	%rbx, _ZZN2v88internalL38Builtin_Impl_Stats_FunctionConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic136(%rip)
	jmp	.L253
	.p2align 4,,10
	.p2align 3
.L286:
	movq	312(%r12), %r13
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L287:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L283:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$773, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L290:
	leaq	.LC6(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L289:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC8(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L256
.L288:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20928:
	.size	_ZN2v88internalL38Builtin_Impl_Stats_FunctionConstructorEiPmPNS0_7IsolateE, .-_ZN2v88internalL38Builtin_Impl_Stats_FunctionConstructorEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL47Builtin_Impl_Stats_GeneratorFunctionConstructorEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC10:
	.string	"V8.Builtin_GeneratorFunctionConstructor"
	.section	.rodata._ZN2v88internalL47Builtin_Impl_Stats_GeneratorFunctionConstructorEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC11:
	.string	"function*"
	.section	.text._ZN2v88internalL47Builtin_Impl_Stats_GeneratorFunctionConstructorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL47Builtin_Impl_Stats_GeneratorFunctionConstructorEiPmPNS0_7IsolateE, @function
_ZN2v88internalL47Builtin_Impl_Stats_GeneratorFunctionConstructorEiPmPNS0_7IsolateE:
.LFB20931:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L324
.L292:
	movq	_ZZN2v88internalL47Builtin_Impl_Stats_GeneratorFunctionConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic145(%rip), %rbx
	testq	%rbx, %rbx
	je	.L325
.L294:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L326
.L296:
	leaq	.LC11(%rip), %rcx
	movq	%r14, %rsi
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	41088(%r12), %rbx
	movq	41096(%r12), %r15
	addl	$1, 41104(%r12)
	call	_ZN2v88internal12_GLOBAL__N_121CreateDynamicFunctionEPNS0_7IsolateENS0_16BuiltinArgumentsEPKc
	testq	%rax, %rax
	je	.L327
	movq	(%rax), %r13
.L301:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r15
	je	.L304
	movq	%r15, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L304:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L328
.L291:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L329
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L326:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L330
.L297:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L298
	movq	(%rdi), %rax
	call	*8(%rax)
.L298:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L299
	movq	(%rdi), %rax
	call	*8(%rax)
.L299:
	leaq	.LC10(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L325:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L331
.L295:
	movq	%rbx, _ZZN2v88internalL47Builtin_Impl_Stats_GeneratorFunctionConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic145(%rip)
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L327:
	movq	312(%r12), %r13
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L328:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L324:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$776, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L292
	.p2align 4,,10
	.p2align 3
.L331:
	leaq	.LC6(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L330:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC10(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L297
.L329:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20931:
	.size	_ZN2v88internalL47Builtin_Impl_Stats_GeneratorFunctionConstructorEiPmPNS0_7IsolateE, .-_ZN2v88internalL47Builtin_Impl_Stats_GeneratorFunctionConstructorEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL43Builtin_Impl_Stats_AsyncFunctionConstructorEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC12:
	.string	"V8.Builtin_AsyncFunctionConstructor"
	.section	.rodata._ZN2v88internalL43Builtin_Impl_Stats_AsyncFunctionConstructorEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC13:
	.string	"async function"
	.section	.text._ZN2v88internalL43Builtin_Impl_Stats_AsyncFunctionConstructorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL43Builtin_Impl_Stats_AsyncFunctionConstructorEiPmPNS0_7IsolateE, @function
_ZN2v88internalL43Builtin_Impl_Stats_AsyncFunctionConstructorEiPmPNS0_7IsolateE:
.LFB20934:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L374
.L333:
	movq	_ZZN2v88internalL43Builtin_Impl_Stats_AsyncFunctionConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic151(%rip), %rbx
	testq	%rbx, %rbx
	je	.L375
.L335:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L376
.L337:
	movq	%r13, %rdx
	leaq	.LC13(%rip), %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	41088(%r12), %r15
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	call	_ZN2v88internal12_GLOBAL__N_121CreateDynamicFunctionEPNS0_7IsolateENS0_16BuiltinArgumentsEPKc
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L377
	movq	(%rax), %r14
	testb	$1, %r14b
	jne	.L378
.L342:
	subl	$1, 41104(%r12)
	movq	%r15, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L353
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L353:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L379
.L332:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L380
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L376:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L381
.L338:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L339
	movq	(%rdi), %rax
	call	*8(%rax)
.L339:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L340
	movq	(%rdi), %rax
	call	*8(%rax)
.L340:
	leaq	.LC12(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L337
	.p2align 4,,10
	.p2align 3
.L375:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L382
.L336:
	movq	%rbx, _ZZN2v88internalL43Builtin_Impl_Stats_AsyncFunctionConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic151(%rip)
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L378:
	movq	-1(%r14), %rax
	cmpw	$1105, 11(%rax)
	jne	.L342
	movq	23(%r14), %rax
	movq	31(%rax), %r14
	testb	$1, %r14b
	jne	.L383
.L346:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L372
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L349:
	movq	%r12, %rdi
	call	_ZN2v88internal6Script15GetEvalPositionEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	0(%r13), %r14
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L377:
	movq	312(%r12), %r14
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L374:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$777, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L379:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L332
	.p2align 4,,10
	.p2align 3
.L382:
	leaq	.LC6(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L381:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC12(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L338
	.p2align 4,,10
	.p2align 3
.L372:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L384
.L350:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L383:
	movq	-1(%r14), %rax
	cmpw	$86, 11(%rax)
	jne	.L346
	movq	23(%r14), %r14
	jmp	.L346
.L384:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L350
.L380:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20934:
	.size	_ZN2v88internalL43Builtin_Impl_Stats_AsyncFunctionConstructorEiPmPNS0_7IsolateE, .-_ZN2v88internalL43Builtin_Impl_Stats_AsyncFunctionConstructorEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL52Builtin_Impl_Stats_AsyncGeneratorFunctionConstructorEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC14:
	.string	"V8.Builtin_AsyncGeneratorFunctionConstructor"
	.section	.rodata._ZN2v88internalL52Builtin_Impl_Stats_AsyncGeneratorFunctionConstructorEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC15:
	.string	"async function*"
	.section	.text._ZN2v88internalL52Builtin_Impl_Stats_AsyncGeneratorFunctionConstructorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL52Builtin_Impl_Stats_AsyncGeneratorFunctionConstructorEiPmPNS0_7IsolateE, @function
_ZN2v88internalL52Builtin_Impl_Stats_AsyncGeneratorFunctionConstructorEiPmPNS0_7IsolateE:
.LFB20937:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L427
.L386:
	movq	_ZZN2v88internalL52Builtin_Impl_Stats_AsyncGeneratorFunctionConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic170(%rip), %rbx
	testq	%rbx, %rbx
	je	.L428
.L388:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L429
.L390:
	movq	%r13, %rdx
	leaq	.LC15(%rip), %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	41088(%r12), %r15
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	call	_ZN2v88internal12_GLOBAL__N_121CreateDynamicFunctionEPNS0_7IsolateENS0_16BuiltinArgumentsEPKc
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L430
	movq	(%rax), %r14
	testb	$1, %r14b
	jne	.L431
.L395:
	subl	$1, 41104(%r12)
	movq	%r15, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L406
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L406:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L432
.L385:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L433
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L429:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L434
.L391:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L392
	movq	(%rdi), %rax
	call	*8(%rax)
.L392:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L393
	movq	(%rdi), %rax
	call	*8(%rax)
.L393:
	leaq	.LC14(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L390
	.p2align 4,,10
	.p2align 3
.L428:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L435
.L389:
	movq	%rbx, _ZZN2v88internalL52Builtin_Impl_Stats_AsyncGeneratorFunctionConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic170(%rip)
	jmp	.L388
	.p2align 4,,10
	.p2align 3
.L431:
	movq	-1(%r14), %rax
	cmpw	$1105, 11(%rax)
	jne	.L395
	movq	23(%r14), %rax
	movq	31(%rax), %r14
	testb	$1, %r14b
	jne	.L436
.L399:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L425
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L402:
	movq	%r12, %rdi
	call	_ZN2v88internal6Script15GetEvalPositionEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	0(%r13), %r14
	jmp	.L395
	.p2align 4,,10
	.p2align 3
.L430:
	movq	312(%r12), %r14
	jmp	.L395
	.p2align 4,,10
	.p2align 3
.L427:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$850, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L386
	.p2align 4,,10
	.p2align 3
.L432:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L385
	.p2align 4,,10
	.p2align 3
.L435:
	leaq	.LC6(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L389
	.p2align 4,,10
	.p2align 3
.L434:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC14(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L425:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L437
.L403:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L402
	.p2align 4,,10
	.p2align 3
.L436:
	movq	-1(%r14), %rax
	cmpw	$86, 11(%rax)
	jne	.L399
	movq	23(%r14), %r14
	jmp	.L399
.L437:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L403
.L433:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20937:
	.size	_ZN2v88internalL52Builtin_Impl_Stats_AsyncGeneratorFunctionConstructorEiPmPNS0_7IsolateE, .-_ZN2v88internalL52Builtin_Impl_Stats_AsyncGeneratorFunctionConstructorEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal27Builtin_FunctionConstructorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal27Builtin_FunctionConstructorEiPmPNS0_7IsolateE
	.type	_ZN2v88internal27Builtin_FunctionConstructorEiPmPNS0_7IsolateE, @function
_ZN2v88internal27Builtin_FunctionConstructorEiPmPNS0_7IsolateE:
.LFB20929:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L446
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %rbx
	movslq	%edi, %rsi
	movq	%r12, %rdi
	movq	41096(%rdx), %r13
	leaq	.LC9(%rip), %rcx
	movq	%r8, %rdx
	call	_ZN2v88internal12_GLOBAL__N_121CreateDynamicFunctionEPNS0_7IsolateENS0_16BuiltinArgumentsEPKc
	testq	%rax, %rax
	je	.L447
	movq	(%rax), %r14
.L441:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L444
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L444:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L447:
	.cfi_restore_state
	movq	312(%r12), %r14
	jmp	.L441
	.p2align 4,,10
	.p2align 3
.L446:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL38Builtin_Impl_Stats_FunctionConstructorEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE20929:
	.size	_ZN2v88internal27Builtin_FunctionConstructorEiPmPNS0_7IsolateE, .-_ZN2v88internal27Builtin_FunctionConstructorEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal36Builtin_GeneratorFunctionConstructorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal36Builtin_GeneratorFunctionConstructorEiPmPNS0_7IsolateE
	.type	_ZN2v88internal36Builtin_GeneratorFunctionConstructorEiPmPNS0_7IsolateE, @function
_ZN2v88internal36Builtin_GeneratorFunctionConstructorEiPmPNS0_7IsolateE:
.LFB20932:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L456
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %rbx
	movslq	%edi, %rsi
	movq	%r12, %rdi
	movq	41096(%rdx), %r13
	leaq	.LC11(%rip), %rcx
	movq	%r8, %rdx
	call	_ZN2v88internal12_GLOBAL__N_121CreateDynamicFunctionEPNS0_7IsolateENS0_16BuiltinArgumentsEPKc
	testq	%rax, %rax
	je	.L457
	movq	(%rax), %r14
.L451:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L454
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L454:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L457:
	.cfi_restore_state
	movq	312(%r12), %r14
	jmp	.L451
	.p2align 4,,10
	.p2align 3
.L456:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL47Builtin_Impl_Stats_GeneratorFunctionConstructorEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE20932:
	.size	_ZN2v88internal36Builtin_GeneratorFunctionConstructorEiPmPNS0_7IsolateE, .-_ZN2v88internal36Builtin_GeneratorFunctionConstructorEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal32Builtin_AsyncFunctionConstructorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal32Builtin_AsyncFunctionConstructorEiPmPNS0_7IsolateE
	.type	_ZN2v88internal32Builtin_AsyncFunctionConstructorEiPmPNS0_7IsolateE, @function
_ZN2v88internal32Builtin_AsyncFunctionConstructorEiPmPNS0_7IsolateE:
.LFB20935:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L475
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r14
	movslq	%edi, %rsi
	movq	%r12, %rdi
	movq	41096(%rdx), %r13
	leaq	.LC13(%rip), %rcx
	movq	%r8, %rdx
	call	_ZN2v88internal12_GLOBAL__N_121CreateDynamicFunctionEPNS0_7IsolateENS0_16BuiltinArgumentsEPKc
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L476
	movq	(%rax), %r15
	testb	$1, %r15b
	jne	.L477
.L461:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L473
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L473:
	addq	$8, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L477:
	.cfi_restore_state
	movq	-1(%r15), %rax
	cmpw	$1105, 11(%rax)
	jne	.L461
	movq	23(%r15), %rax
	movq	31(%rax), %r15
	testb	$1, %r15b
	jne	.L478
.L465:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L472
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L468:
	movq	%r12, %rdi
	call	_ZN2v88internal6Script15GetEvalPositionEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%rbx), %r15
	jmp	.L461
	.p2align 4,,10
	.p2align 3
.L476:
	movq	312(%r12), %r15
	jmp	.L461
	.p2align 4,,10
	.p2align 3
.L475:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL43Builtin_Impl_Stats_AsyncFunctionConstructorEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L472:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L479
.L469:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r15, (%rsi)
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L478:
	movq	-1(%r15), %rax
	cmpw	$86, 11(%rax)
	jne	.L465
	movq	23(%r15), %r15
	jmp	.L465
.L479:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L469
	.cfi_endproc
.LFE20935:
	.size	_ZN2v88internal32Builtin_AsyncFunctionConstructorEiPmPNS0_7IsolateE, .-_ZN2v88internal32Builtin_AsyncFunctionConstructorEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal41Builtin_AsyncGeneratorFunctionConstructorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal41Builtin_AsyncGeneratorFunctionConstructorEiPmPNS0_7IsolateE
	.type	_ZN2v88internal41Builtin_AsyncGeneratorFunctionConstructorEiPmPNS0_7IsolateE, @function
_ZN2v88internal41Builtin_AsyncGeneratorFunctionConstructorEiPmPNS0_7IsolateE:
.LFB20938:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L497
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r14
	movslq	%edi, %rsi
	movq	%r12, %rdi
	movq	41096(%rdx), %r13
	leaq	.LC15(%rip), %rcx
	movq	%r8, %rdx
	call	_ZN2v88internal12_GLOBAL__N_121CreateDynamicFunctionEPNS0_7IsolateENS0_16BuiltinArgumentsEPKc
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L498
	movq	(%rax), %r15
	testb	$1, %r15b
	jne	.L499
.L483:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L495
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L495:
	addq	$8, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L499:
	.cfi_restore_state
	movq	-1(%r15), %rax
	cmpw	$1105, 11(%rax)
	jne	.L483
	movq	23(%r15), %rax
	movq	31(%rax), %r15
	testb	$1, %r15b
	jne	.L500
.L487:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L494
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L490:
	movq	%r12, %rdi
	call	_ZN2v88internal6Script15GetEvalPositionEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%rbx), %r15
	jmp	.L483
	.p2align 4,,10
	.p2align 3
.L498:
	movq	312(%r12), %r15
	jmp	.L483
	.p2align 4,,10
	.p2align 3
.L497:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL52Builtin_Impl_Stats_AsyncGeneratorFunctionConstructorEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L494:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L501
.L491:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r15, (%rsi)
	jmp	.L490
	.p2align 4,,10
	.p2align 3
.L500:
	movq	-1(%r15), %rax
	cmpw	$86, 11(%rax)
	jne	.L487
	movq	23(%r15), %r15
	jmp	.L487
.L501:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L491
	.cfi_endproc
.LFE20938:
	.size	_ZN2v88internal41Builtin_AsyncGeneratorFunctionConstructorEiPmPNS0_7IsolateE, .-_ZN2v88internal41Builtin_AsyncGeneratorFunctionConstructorEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal33Builtin_FunctionPrototypeToStringEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal33Builtin_FunctionPrototypeToStringEiPmPNS0_7IsolateE
	.type	_ZN2v88internal33Builtin_FunctionPrototypeToStringEiPmPNS0_7IsolateE, @function
_ZN2v88internal33Builtin_FunctionPrototypeToStringEiPmPNS0_7IsolateE:
.LFB20945:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L506
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL38Builtin_Impl_FunctionPrototypeToStringENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L506:
	.cfi_restore 6
	jmp	_ZN2v88internalL44Builtin_Impl_Stats_FunctionPrototypeToStringEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE20945:
	.size	_ZN2v88internal33Builtin_FunctionPrototypeToStringEiPmPNS0_7IsolateE, .-_ZN2v88internal33Builtin_FunctionPrototypeToStringEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m.str1.1,"aMS",@progbits,1
.LC16:
	.string	"NewArray"
	.section	.text._ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m,"axG",@progbits,_ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m,comdat
	.p2align 4
	.weak	_ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m
	.type	_ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m, @function
_ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m:
.LFB23944:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZSt7nothrow(%rip), %rsi
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdi
	movq	$-1, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	0(,%rdi,8), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmova	%rax, %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	je	.L509
	movq	%rbx, %rsi
	subq	$1, %rsi
	js	.L507
	leaq	-2(%rbx), %rdx
	movl	$1, %edi
	cmpq	$-1, %rdx
	cmovge	%rbx, %rdi
	cmpq	$1, %rbx
	je	.L522
	cmpq	$-1, %rdx
	jl	.L522
	movq	%rdi, %rsi
	xorl	%edx, %edx
	pxor	%xmm0, %xmm0
	shrq	%rsi
	.p2align 4,,10
	.p2align 3
.L513:
	movq	%rdx, %rcx
	addq	$1, %rdx
	salq	$4, %rcx
	movups	%xmm0, (%rax,%rcx)
	cmpq	%rdx, %rsi
	jne	.L513
	movq	%rdi, %rcx
	andq	$-2, %rcx
	leaq	(%rax,%rcx,8), %rdx
	cmpq	%rcx, %rdi
	je	.L507
.L511:
	movq	$0, (%rdx)
.L507:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L522:
	.cfi_restore_state
	movq	%rax, %rdx
	jmp	.L511
.L509:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%r12, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	je	.L526
	movq	%rbx, %rdi
	subq	$1, %rdi
	js	.L507
	leaq	-2(%rbx), %rcx
	movl	$1, %edx
	addq	$1, %rcx
	cmovge	%rbx, %rdx
	jl	.L523
	subq	$1, %rbx
	je	.L523
	movq	%rdx, %rsi
	xorl	%ecx, %ecx
	pxor	%xmm0, %xmm0
	shrq	%rsi
.L517:
	movq	%rcx, %rdi
	addq	$1, %rcx
	salq	$4, %rdi
	movups	%xmm0, (%rax,%rdi)
	cmpq	%rcx, %rsi
	jne	.L517
	movq	%rdx, %rsi
	andq	$-2, %rsi
	leaq	(%rax,%rsi,8), %rcx
	cmpq	%rsi, %rdx
	je	.L507
.L515:
	movq	$0, (%rcx)
	jmp	.L507
.L526:
	leaq	.LC16(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
.L523:
	movq	%rax, %rcx
	jmp	.L515
	.cfi_endproc
.LFE23944:
	.size	_ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m, .-_ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m
	.section	.text._ZN2v88internal12_GLOBAL__N_114DoFunctionBindEPNS0_7IsolateENS0_16BuiltinArgumentsE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_114DoFunctionBindEPNS0_7IsolateENS0_16BuiltinArgumentsE, @function
_ZN2v88internal12_GLOBAL__N_114DoFunctionBindEPNS0_7IsolateENS0_16BuiltinArgumentsE:
.LFB20940:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$328, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	41088(%rdi), %r12
	movq	41096(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdi)
	movq	(%rdx), %rax
	testb	$1, %al
	jne	.L528
.L530:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$54, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r14
.L529:
	subl	$1, 41104(%r13)
	movq	%r12, 41088(%r13)
	cmpq	41096(%r13), %rbx
	je	.L586
	movq	%rbx, 41096(%r13)
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L586:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L621
	addq	$328, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L528:
	.cfi_restore_state
	movq	-1(%rax), %rax
	testb	$2, 13(%rax)
	je	.L530
	movq	%rdx, %r14
	movl	%esi, %edx
	movl	$0, %eax
	movq	%rsi, -344(%rbp)
	subl	$6, %edx
	leaq	88(%rdi), %r9
	cmovns	%edx, %eax
	movq	%r9, -352(%rbp)
	movl	%edx, -336(%rbp)
	movslq	%eax, %r15
	movl	%eax, -360(%rbp)
	movq	%r15, %rdi
	call	_ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m
	movq	-344(%rbp), %rsi
	movl	-336(%rbp), %edx
	movq	%rax, -328(%rbp)
	movq	-352(%rbp), %r9
	cmpl	$5, %esi
	jle	.L534
	leal	-4(%rsi), %edi
	leaq	-8(%r14), %r9
	cmpl	$6, %esi
	je	.L534
	subl	$7, %esi
	cmpl	$3, %esi
	jbe	.L588
	movl	%edx, %ecx
	movq	%r14, %xmm3
	pxor	%xmm5, %xmm5
	movdqa	.LC17(%rip), %xmm1
	shrl	$2, %ecx
	movdqa	.LC19(%rip), %xmm6
	punpcklqdq	%xmm3, %xmm3
	salq	$5, %rcx
	addq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L536:
	movdqa	%xmm1, %xmm0
	movdqa	%xmm5, %xmm2
	movdqa	%xmm3, %xmm7
	addq	$32, %rax
	pslld	$3, %xmm0
	paddd	%xmm6, %xmm1
	pcmpgtd	%xmm0, %xmm2
	movdqa	%xmm0, %xmm4
	punpckldq	%xmm2, %xmm4
	punpckhdq	%xmm2, %xmm0
	psubq	%xmm4, %xmm7
	movups	%xmm7, -32(%rax)
	movdqa	%xmm3, %xmm7
	psubq	%xmm0, %xmm7
	movups	%xmm7, -16(%rax)
	cmpq	%rcx, %rax
	jne	.L536
	movl	%edx, %ecx
	andl	$-4, %ecx
	leal	2(%rcx), %eax
	cmpl	%ecx, %edx
	je	.L534
.L535:
	leal	0(,%rax,8), %edx
	movq	-328(%rbp), %r10
	leal	-2(%rax), %ecx
	movq	%r14, %rsi
	movslq	%edx, %rdx
	movslq	%ecx, %rcx
	subq	%rdx, %rsi
	movq	%rsi, (%r10,%rcx,8)
	leal	1(%rax), %ecx
	cmpl	%ecx, %edi
	jle	.L534
	sall	$3, %ecx
	movq	%r14, %rsi
	movslq	%ecx, %rcx
	subq	%rcx, %rsi
	movq	%rsi, -8(%r10,%rdx)
	leal	2(%rax), %edx
	cmpl	%edi, %edx
	jge	.L534
	sall	$3, %edx
	movq	%r14, %rsi
	addl	$3, %eax
	movslq	%edx, %rdx
	subq	%rdx, %rsi
	movq	%rsi, -8(%r10,%rcx)
	cmpl	%eax, %edi
	jle	.L534
	sall	$3, %eax
	movq	%r14, %rcx
	cltq
	subq	%rax, %rcx
	movq	%rcx, -8(%r10,%rdx)
.L534:
	movq	-328(%rbp), %rax
	movq	%r15, %r8
	movq	%r9, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%r15, -312(%rbp)
	movq	%rax, %rcx
	movq	%rax, -320(%rbp)
	call	_ZN2v88internal7Factory18NewJSBoundFunctionENS0_6HandleINS0_10JSReceiverEEENS2_INS0_6ObjectEEENS0_6VectorIS6_EE@PLT
	movq	%rax, -336(%rbp)
	testq	%rax, %rax
	je	.L620
	leaq	2768(%r13), %rax
	movq	2768(%r13), %rdx
	movq	%rax, -352(%rbp)
	movq	(%r14), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	movq	-1(%rdx), %rcx
	movl	$1, %eax
	subq	$37592, %rdi
	cmpw	$64, 11(%rcx)
	jne	.L540
	movl	11(%rdx), %eax
	notl	%eax
	andl	$1, %eax
.L540:
	movl	%eax, -304(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -292(%rbp)
	movq	2768(%r13), %rax
	movq	%rdi, -280(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %edx
	leaq	2768(%r13), %rax
	andl	$-32, %edx
	cmpl	$32, %edx
	je	.L622
.L541:
	leaq	-304(%rbp), %r15
	movq	%rax, -272(%rbp)
	movq	%r15, %rdi
	movq	$0, -264(%rbp)
	movq	%r14, -256(%rbp)
	movq	$0, -248(%rbp)
	movq	%r14, -240(%rbp)
	movq	$-1, -232(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	movq	(%r14), %rax
	movq	-1(%rax), %rax
	cmpw	$1105, 11(%rax)
	je	.L623
.L542:
	movq	41112(%r13), %rdi
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %rsi
	testq	%rdi, %rdi
	je	.L544
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L545:
	movq	%r15, %rdi
	movq	%rax, -344(%rbp)
	call	_ZN2v88internal10JSReceiver21GetPropertyAttributesEPNS0_14LookupIteratorE@PLT
	testb	%al, %al
	je	.L620
	shrq	$32, %rax
	cmpl	$64, %eax
	je	.L550
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	testq	%rax, %rax
	je	.L620
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L624
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
.L555:
	ucomisd	%xmm0, %xmm0
	jp	.L591
	movq	.LC20(%rip), %xmm1
	movsd	.LC21(%rip), %xmm3
	movapd	%xmm0, %xmm2
	andpd	%xmm1, %xmm2
	ucomisd	%xmm2, %xmm3
	pxor	%xmm2, %xmm2
	jb	.L556
	ucomisd	%xmm2, %xmm0
	jnp	.L625
.L599:
	comisd	%xmm2, %xmm0
	movapd	%xmm0, %xmm4
	movsd	.LC22(%rip), %xmm5
	movapd	%xmm0, %xmm3
	andpd	%xmm1, %xmm4
	jb	.L616
	ucomisd	%xmm4, %xmm5
	jbe	.L561
	cvttsd2siq	%xmm0, %rax
	pxor	%xmm4, %xmm4
	movsd	.LC23(%rip), %xmm5
	andnpd	%xmm0, %xmm1
	cvtsi2sdq	%rax, %xmm4
	movapd	%xmm4, %xmm6
	cmpnlesd	%xmm0, %xmm6
	movapd	%xmm6, %xmm3
	andpd	%xmm5, %xmm3
	subsd	%xmm3, %xmm4
	movapd	%xmm4, %xmm3
	orpd	%xmm1, %xmm3
.L561:
	movapd	%xmm3, %xmm0
.L556:
	pxor	%xmm1, %xmm1
	xorl	%esi, %esi
	movq	%r13, %rdi
	cvtsi2sdl	-360(%rbp), %xmm1
	subsd	%xmm1, %xmm0
	maxsd	%xmm2, %xmm0
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	movq	%rax, -344(%rbp)
.L550:
	movq	-336(%rbp), %rax
	movq	2768(%r13), %rdx
	movq	(%rax), %rax
	movq	%rax, -360(%rbp)
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	movq	-1(%rdx), %rcx
	movl	$3, %eax
	subq	$37592, %rdi
	cmpw	$64, 11(%rcx)
	je	.L626
.L563:
	movl	%eax, -144(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -132(%rbp)
	movq	2768(%r13), %rax
	movq	%rdi, -120(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L627
.L564:
	movq	-352(%rbp), %rax
	leaq	-144(%rbp), %r15
	movq	$0, -104(%rbp)
	movq	%r15, %rdi
	movq	$0, -88(%rbp)
	movq	%rax, -112(%rbp)
	movq	-336(%rbp), %rax
	movq	$-1, -72(%rbp)
	movq	%rax, -96(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	movl	-128(%rbp), %edx
	movl	$1, %ecx
	movq	%r15, %rdi
	movq	-344(%rbp), %rsi
	shrl	$3, %edx
	andl	$7, %edx
	call	_ZN2v88internal8JSObject33DefineOwnPropertyIgnoreAttributesEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS0_18PropertyAttributesENS1_20AccessorInfoHandlingE@PLT
	testq	%rax, %rax
	je	.L620
.L543:
	movq	(%r14), %rax
	movq	2872(%r13), %rdx
	leaq	2872(%r13), %r15
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	movq	-1(%rdx), %rcx
	movl	$3, %eax
	subq	$37592, %rdi
	cmpw	$64, 11(%rcx)
	jne	.L566
	movl	11(%rdx), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$3, %eax
.L566:
	movl	%eax, -224(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -212(%rbp)
	movq	2872(%r13), %rax
	movq	%rdi, -200(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %edx
	movq	%r15, %rax
	andl	$-32, %edx
	cmpl	$32, %edx
	je	.L628
.L567:
	leaq	-224(%rbp), %rdi
	movq	%rax, -192(%rbp)
	movq	%rdi, -344(%rbp)
	movq	$0, -184(%rbp)
	movq	%r14, -176(%rbp)
	movq	$0, -168(%rbp)
	movq	%r14, -160(%rbp)
	movq	$-1, -152(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	movq	(%r14), %rax
	movq	-1(%rax), %rax
	movq	-344(%rbp), %rdi
	cmpw	$1105, 11(%rax)
	jne	.L571
	cmpl	$5, -220(%rbp)
	je	.L629
.L571:
	movl	$1, %esi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L620
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L630
.L575:
	leaq	2184(%r13), %rax
	movq	%rax, %r14
.L577:
	movq	-336(%rbp), %rax
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L580
.L582:
	movq	-336(%rbp), %rsi
	movl	$-1, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	%rax, %rdx
.L581:
	movq	2872(%r13), %rcx
	movl	$3, %eax
	movq	-1(%rcx), %rsi
	cmpw	$64, 11(%rsi)
	jne	.L583
	movl	11(%rcx), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$3, %eax
.L583:
	movl	%eax, -144(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -132(%rbp)
	movq	2872(%r13), %rax
	movq	%r13, -120(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L631
.L584:
	movq	%r15, -112(%rbp)
	movq	-336(%rbp), %rax
	leaq	-144(%rbp), %r15
	movq	%r15, %rdi
	movq	%rdx, -80(%rbp)
	movq	$0, -104(%rbp)
	movq	%rax, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	movl	-128(%rbp), %edx
	movl	$1, %ecx
	movq	%r14, %rsi
	movq	%r15, %rdi
	shrl	$3, %edx
	andl	$7, %edx
	call	_ZN2v88internal8JSObject33DefineOwnPropertyIgnoreAttributesEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS0_18PropertyAttributesENS1_20AccessorInfoHandlingE@PLT
	testq	%rax, %rax
	je	.L620
.L573:
	movq	-336(%rbp), %rax
	movq	(%rax), %r14
	jmp	.L539
	.p2align 4,,10
	.p2align 3
.L630:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L575
	movq	%r13, %rdi
	call	_ZN2v88internal4Name14ToFunctionNameEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L620
	leaq	2184(%r13), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal7Factory13NewConsStringENS0_6HandleINS0_6StringEEES4_@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	jne	.L577
	.p2align 4,,10
	.p2align 3
.L620:
	movq	312(%r13), %r14
.L539:
	movq	-328(%rbp), %rax
	testq	%rax, %rax
	je	.L529
	movq	%rax, %rdi
	call	_ZdaPv@PLT
	jmp	.L529
	.p2align 4,,10
	.p2align 3
.L544:
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L632
.L546:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%rsi, (%rax)
	jmp	.L545
	.p2align 4,,10
	.p2align 3
.L626:
	movl	11(%rdx), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$3, %eax
	jmp	.L563
	.p2align 4,,10
	.p2align 3
.L627:
	leaq	2768(%r13), %rsi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, -352(%rbp)
	jmp	.L564
	.p2align 4,,10
	.p2align 3
.L623:
	cmpl	$5, -300(%rbp)
	jne	.L542
	movq	%r15, %rdi
	call	_ZNK2v88internal14LookupIterator12GetAccessorsEv@PLT
	movq	(%rax), %rax
	testb	$1, %al
	je	.L542
	movq	-1(%rax), %rax
	cmpw	$78, 11(%rax)
	jne	.L542
	jmp	.L543
	.p2align 4,,10
	.p2align 3
.L622:
	movq	%rax, %rsi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	jmp	.L541
	.p2align 4,,10
	.p2align 3
.L624:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L550
	movsd	7(%rax), %xmm0
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L580:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L582
	movq	-336(%rbp), %rdx
	jmp	.L581
	.p2align 4,,10
	.p2align 3
.L632:
	movq	%r13, %rdi
	movq	%rsi, -344(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-344(%rbp), %rsi
	jmp	.L546
.L628:
	movq	%r15, %rsi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	jmp	.L567
.L588:
	movl	$2, %eax
	jmp	.L535
.L631:
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rdx, -344(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-344(%rbp), %rdx
	movq	%rax, %r15
	jmp	.L584
.L625:
	je	.L556
	jmp	.L599
	.p2align 4,,10
	.p2align 3
.L616:
	ucomisd	%xmm4, %xmm5
	jbe	.L561
	cvttsd2siq	%xmm0, %rax
	pxor	%xmm4, %xmm4
	movsd	.LC23(%rip), %xmm5
	andnpd	%xmm0, %xmm1
	cvtsi2sdq	%rax, %xmm4
	cmpnlesd	%xmm4, %xmm3
	andpd	%xmm5, %xmm3
	addsd	%xmm4, %xmm3
	orpd	%xmm1, %xmm3
	jmp	.L561
.L629:
	movq	%rdi, -344(%rbp)
	call	_ZNK2v88internal14LookupIterator12GetAccessorsEv@PLT
	movq	-344(%rbp), %rdi
	movq	(%rax), %rax
	testb	$1, %al
	je	.L571
	movq	-1(%rax), %rax
	cmpw	$78, 11(%rax)
	jne	.L571
	cmpl	$4, -220(%rbp)
	je	.L573
	call	_ZNK2v88internal14LookupIterator16HolderIsReceiverEv@PLT
	movq	-344(%rbp), %rdi
	testb	%al, %al
	je	.L571
	jmp	.L573
	.p2align 4,,10
	.p2align 3
.L591:
	pxor	%xmm0, %xmm0
	movapd	%xmm0, %xmm2
	jmp	.L556
.L621:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20940:
	.size	_ZN2v88internal12_GLOBAL__N_114DoFunctionBindEPNS0_7IsolateENS0_16BuiltinArgumentsE, .-_ZN2v88internal12_GLOBAL__N_114DoFunctionBindEPNS0_7IsolateENS0_16BuiltinArgumentsE
	.section	.rodata._ZN2v88internalL40Builtin_Impl_Stats_FunctionPrototypeBindEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC24:
	.string	"V8.Builtin_FunctionPrototypeBind"
	.section	.text._ZN2v88internalL40Builtin_Impl_Stats_FunctionPrototypeBindEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL40Builtin_Impl_Stats_FunctionPrototypeBindEiPmPNS0_7IsolateE, @function
_ZN2v88internalL40Builtin_Impl_Stats_FunctionPrototypeBindEiPmPNS0_7IsolateE:
.LFB20941:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L662
.L634:
	movq	_ZZN2v88internalL40Builtin_Impl_Stats_FunctionPrototypeBindEiPmPNS0_7IsolateEE28trace_event_unique_atomic278(%rip), %rbx
	testq	%rbx, %rbx
	je	.L663
.L636:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L664
.L638:
	movq	%r12, %rdi
	movq	%r14, %rsi
	movq	%r13, %rdx
	call	_ZN2v88internal12_GLOBAL__N_114DoFunctionBindEPNS0_7IsolateENS0_16BuiltinArgumentsE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L665
.L633:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L666
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L663:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L667
.L637:
	movq	%rbx, _ZZN2v88internalL40Builtin_Impl_Stats_FunctionPrototypeBindEiPmPNS0_7IsolateEE28trace_event_unique_atomic278(%rip)
	jmp	.L636
	.p2align 4,,10
	.p2align 3
.L664:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L668
.L639:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L640
	movq	(%rdi), %rax
	call	*8(%rax)
.L640:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L641
	movq	(%rdi), %rax
	call	*8(%rax)
.L641:
	leaq	.LC24(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L638
	.p2align 4,,10
	.p2align 3
.L665:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L633
	.p2align 4,,10
	.p2align 3
.L662:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$774, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L634
	.p2align 4,,10
	.p2align 3
.L668:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC24(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L639
	.p2align 4,,10
	.p2align 3
.L667:
	leaq	.LC6(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L637
.L666:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20941:
	.size	_ZN2v88internalL40Builtin_Impl_Stats_FunctionPrototypeBindEiPmPNS0_7IsolateE, .-_ZN2v88internalL40Builtin_Impl_Stats_FunctionPrototypeBindEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal29Builtin_FunctionPrototypeBindEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal29Builtin_FunctionPrototypeBindEiPmPNS0_7IsolateE
	.type	_ZN2v88internal29Builtin_FunctionPrototypeBindEiPmPNS0_7IsolateE, @function
_ZN2v88internal29Builtin_FunctionPrototypeBindEiPmPNS0_7IsolateE:
.LFB20942:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsi, %r9
	movq	%rdx, %r8
	testl	%eax, %eax
	jne	.L673
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rsi
	movq	%r9, %rdx
	movq	%r8, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal12_GLOBAL__N_114DoFunctionBindEPNS0_7IsolateENS0_16BuiltinArgumentsE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L673:
	.cfi_restore 6
	jmp	_ZN2v88internalL40Builtin_Impl_Stats_FunctionPrototypeBindEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE20942:
	.size	_ZN2v88internal29Builtin_FunctionPrototypeBindEiPmPNS0_7IsolateE, .-_ZN2v88internal29Builtin_FunctionPrototypeBindEiPmPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal27Builtin_FunctionConstructorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal27Builtin_FunctionConstructorEiPmPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal27Builtin_FunctionConstructorEiPmPNS0_7IsolateE:
.LFB25694:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE25694:
	.size	_GLOBAL__sub_I__ZN2v88internal27Builtin_FunctionConstructorEiPmPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal27Builtin_FunctionConstructorEiPmPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal27Builtin_FunctionConstructorEiPmPNS0_7IsolateE
	.section	.bss._ZZN2v88internalL44Builtin_Impl_Stats_FunctionPrototypeToStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic281,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL44Builtin_Impl_Stats_FunctionPrototypeToStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic281, @object
	.size	_ZZN2v88internalL44Builtin_Impl_Stats_FunctionPrototypeToStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic281, 8
_ZZN2v88internalL44Builtin_Impl_Stats_FunctionPrototypeToStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic281:
	.zero	8
	.section	.bss._ZZN2v88internalL40Builtin_Impl_Stats_FunctionPrototypeBindEiPmPNS0_7IsolateEE28trace_event_unique_atomic278,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL40Builtin_Impl_Stats_FunctionPrototypeBindEiPmPNS0_7IsolateEE28trace_event_unique_atomic278, @object
	.size	_ZZN2v88internalL40Builtin_Impl_Stats_FunctionPrototypeBindEiPmPNS0_7IsolateEE28trace_event_unique_atomic278, 8
_ZZN2v88internalL40Builtin_Impl_Stats_FunctionPrototypeBindEiPmPNS0_7IsolateEE28trace_event_unique_atomic278:
	.zero	8
	.section	.bss._ZZN2v88internalL52Builtin_Impl_Stats_AsyncGeneratorFunctionConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic170,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL52Builtin_Impl_Stats_AsyncGeneratorFunctionConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic170, @object
	.size	_ZZN2v88internalL52Builtin_Impl_Stats_AsyncGeneratorFunctionConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic170, 8
_ZZN2v88internalL52Builtin_Impl_Stats_AsyncGeneratorFunctionConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic170:
	.zero	8
	.section	.bss._ZZN2v88internalL43Builtin_Impl_Stats_AsyncFunctionConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic151,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL43Builtin_Impl_Stats_AsyncFunctionConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic151, @object
	.size	_ZZN2v88internalL43Builtin_Impl_Stats_AsyncFunctionConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic151, 8
_ZZN2v88internalL43Builtin_Impl_Stats_AsyncFunctionConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic151:
	.zero	8
	.section	.bss._ZZN2v88internalL47Builtin_Impl_Stats_GeneratorFunctionConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic145,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL47Builtin_Impl_Stats_GeneratorFunctionConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic145, @object
	.size	_ZZN2v88internalL47Builtin_Impl_Stats_GeneratorFunctionConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic145, 8
_ZZN2v88internalL47Builtin_Impl_Stats_GeneratorFunctionConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic145:
	.zero	8
	.section	.bss._ZZN2v88internalL38Builtin_Impl_Stats_FunctionConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic136,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL38Builtin_Impl_Stats_FunctionConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic136, @object
	.size	_ZZN2v88internalL38Builtin_Impl_Stats_FunctionConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic136, 8
_ZZN2v88internalL38Builtin_Impl_Stats_FunctionConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic136:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC17:
	.long	2
	.long	3
	.long	4
	.long	5
	.align 16
.LC19:
	.long	4
	.long	4
	.long	4
	.long	4
	.align 16
.LC20:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC21:
	.long	4294967295
	.long	2146435071
	.align 8
.LC22:
	.long	0
	.long	1127219200
	.align 8
.LC23:
	.long	0
	.long	1072693248
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
