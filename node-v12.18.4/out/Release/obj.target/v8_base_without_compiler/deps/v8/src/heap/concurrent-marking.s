	.file	"concurrent-marking.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB1935:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE1935:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB1936:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1936:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB1938:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE1938:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.text._ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE,"axG",@progbits,_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE
	.type	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE, @function
_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE:
.LFB7780:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	leaq	8(%rdx), %rcx
	movq	16(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE7780:
	.size	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE, .-_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE
	.section	.text._ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE,"axG",@progbits,_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE
	.type	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE, @function
_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE:
.LFB7781:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	leaq	8(%rdx), %rcx
	movq	24(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE7781:
	.size	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE, .-_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE
	.section	.text._ZN2v88internal13ObjectVisitor22VisitCustomWeakPointerENS0_10HeapObjectENS0_14FullObjectSlotE,"axG",@progbits,_ZN2v88internal13ObjectVisitor22VisitCustomWeakPointerENS0_10HeapObjectENS0_14FullObjectSlotE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ObjectVisitor22VisitCustomWeakPointerENS0_10HeapObjectENS0_14FullObjectSlotE
	.type	_ZN2v88internal13ObjectVisitor22VisitCustomWeakPointerENS0_10HeapObjectENS0_14FullObjectSlotE, @function
_ZN2v88internal13ObjectVisitor22VisitCustomWeakPointerENS0_10HeapObjectENS0_14FullObjectSlotE:
.LFB7782:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	leaq	8(%rdx), %rcx
	movq	32(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE7782:
	.size	_ZN2v88internal13ObjectVisitor22VisitCustomWeakPointerENS0_10HeapObjectENS0_14FullObjectSlotE, .-_ZN2v88internal13ObjectVisitor22VisitCustomWeakPointerENS0_10HeapObjectENS0_14FullObjectSlotE
	.section	.text._ZN2v88internal13ObjectVisitor17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE,"axG",@progbits,_ZN2v88internal13ObjectVisitor17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ObjectVisitor17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE
	.type	_ZN2v88internal13ObjectVisitor17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE, @function
_ZN2v88internal13ObjectVisitor17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE:
.LFB7784:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7784:
	.size	_ZN2v88internal13ObjectVisitor17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE, .-_ZN2v88internal13ObjectVisitor17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE
	.section	.text._ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE,"axG",@progbits,_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE
	.type	_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE, @function
_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE:
.LFB7785:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7785:
	.size	_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE, .-_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE
	.section	.text._ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_7ForeignEPm,"axG",@progbits,_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_7ForeignEPm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_7ForeignEPm
	.type	_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_7ForeignEPm, @function
_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_7ForeignEPm:
.LFB7786:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7786:
	.size	_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_7ForeignEPm, .-_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_7ForeignEPm
	.section	.text._ZN2v88internal13ObjectVisitor22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE,"axG",@progbits,_ZN2v88internal13ObjectVisitor22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ObjectVisitor22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE
	.type	_ZN2v88internal13ObjectVisitor22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE, @function
_ZN2v88internal13ObjectVisitor22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE:
.LFB7787:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7787:
	.size	_ZN2v88internal13ObjectVisitor22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE, .-_ZN2v88internal13ObjectVisitor22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE
	.section	.text._ZN2v88internal13ObjectVisitor18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE,"axG",@progbits,_ZN2v88internal13ObjectVisitor18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ObjectVisitor18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE
	.type	_ZN2v88internal13ObjectVisitor18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE, @function
_ZN2v88internal13ObjectVisitor18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE:
.LFB7788:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7788:
	.size	_ZN2v88internal13ObjectVisitor18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE, .-_ZN2v88internal13ObjectVisitor18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE
	.section	.text._ZN2v88internal24ConcurrentMarkingVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_,"axG",@progbits,_ZN2v88internal24ConcurrentMarkingVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal24ConcurrentMarkingVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	.type	_ZN2v88internal24ConcurrentMarkingVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_, @function
_ZN2v88internal24ConcurrentMarkingVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_:
.LFB22239:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE22239:
	.size	_ZN2v88internal24ConcurrentMarkingVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_, .-_ZN2v88internal24ConcurrentMarkingVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	.section	.text._ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES4_,"axG",@progbits,_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES4_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES4_
	.type	_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES4_, @function
_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES4_:
.LFB22279:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE22279:
	.size	_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES4_, .-_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES4_
	.section	.text._ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitorD2Ev,"axG",@progbits,_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitorD2Ev
	.type	_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitorD2Ev, @function
_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitorD2Ev:
.LFB27452:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE27452:
	.size	_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitorD2Ev, .-_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitorD2Ev
	.weak	_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitorD1Ev
	.set	_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitorD1Ev,_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitorD2Ev
	.section	.text._ZN2v88internal24ConcurrentMarkingVisitorD2Ev,"axG",@progbits,_ZN2v88internal24ConcurrentMarkingVisitorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal24ConcurrentMarkingVisitorD2Ev
	.type	_ZN2v88internal24ConcurrentMarkingVisitorD2Ev, @function
_ZN2v88internal24ConcurrentMarkingVisitorD2Ev:
.LFB28725:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE28725:
	.size	_ZN2v88internal24ConcurrentMarkingVisitorD2Ev, .-_ZN2v88internal24ConcurrentMarkingVisitorD2Ev
	.weak	_ZN2v88internal24ConcurrentMarkingVisitorD1Ev
	.set	_ZN2v88internal24ConcurrentMarkingVisitorD1Ev,_ZN2v88internal24ConcurrentMarkingVisitorD2Ev
	.section	.rodata._ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES4_.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES4_,"axG",@progbits,_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES4_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES4_
	.type	_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES4_, @function
_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES4_:
.LFB22276:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE22276:
	.size	_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES4_, .-_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES4_
	.section	.text._ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE,"axG",@progbits,_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE
	.type	_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE, @function
_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE:
.LFB22278:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE22278:
	.size	_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE, .-_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE
	.section	.text._ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE,"axG",@progbits,_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE
	.type	_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE, @function
_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE:
.LFB22277:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE22277:
	.size	_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE, .-_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE
	.section	.text._ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES4_,"axG",@progbits,_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES4_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES4_
	.type	_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES4_, @function
_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES4_:
.LFB22275:
	.cfi_startproc
	endbr64
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L24:
	movq	(%rdx), %r8
	movq	8(%rdi), %rsi
	movq	%rdx, %xmm0
	addq	$8, %rdx
	movslq	(%rsi), %rax
	movq	%r8, %xmm1
	punpcklqdq	%xmm1, %xmm0
	leal	1(%rax), %r9d
	salq	$4, %rax
	movl	%r9d, (%rsi)
	movups	%xmm0, 8(%rsi,%rax)
.L28:
	cmpq	%rdx, %rcx
	ja	.L24
	ret
	.cfi_endproc
.LFE22275:
	.size	_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES4_, .-_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES4_
	.section	.text._ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitorD0Ev,"axG",@progbits,_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitorD0Ev
	.type	_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitorD0Ev, @function
_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitorD0Ev:
.LFB27454:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE27454:
	.size	_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitorD0Ev, .-_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitorD0Ev
	.section	.text._ZN2v88internal24ConcurrentMarkingVisitorD0Ev,"axG",@progbits,_ZN2v88internal24ConcurrentMarkingVisitorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal24ConcurrentMarkingVisitorD0Ev
	.type	_ZN2v88internal24ConcurrentMarkingVisitorD0Ev, @function
_ZN2v88internal24ConcurrentMarkingVisitorD0Ev:
.LFB28727:
	.cfi_startproc
	endbr64
	movl	$4176, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE28727:
	.size	_ZN2v88internal24ConcurrentMarkingVisitorD0Ev, .-_ZN2v88internal24ConcurrentMarkingVisitorD0Ev
	.section	.text._ZN2v88internal17ConcurrentMarking4TaskD2Ev,"axG",@progbits,_ZN2v88internal17ConcurrentMarking4TaskD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal17ConcurrentMarking4TaskD2Ev
	.type	_ZN2v88internal17ConcurrentMarking4TaskD2Ev, @function
_ZN2v88internal17ConcurrentMarking4TaskD2Ev:
.LFB26538:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal14CancelableTaskE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN2v88internal10CancelableD2Ev@PLT
	.cfi_endproc
.LFE26538:
	.size	_ZN2v88internal17ConcurrentMarking4TaskD2Ev, .-_ZN2v88internal17ConcurrentMarking4TaskD2Ev
	.weak	_ZN2v88internal17ConcurrentMarking4TaskD1Ev
	.set	_ZN2v88internal17ConcurrentMarking4TaskD1Ev,_ZN2v88internal17ConcurrentMarking4TaskD2Ev
	.section	.text._ZN2v88internal17ConcurrentMarking4TaskD0Ev,"axG",@progbits,_ZN2v88internal17ConcurrentMarking4TaskD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal17ConcurrentMarking4TaskD0Ev
	.type	_ZN2v88internal17ConcurrentMarking4TaskD0Ev, @function
_ZN2v88internal17ConcurrentMarking4TaskD0Ev:
.LFB26540:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal14CancelableTaskE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN2v88internal10CancelableD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$64, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE26540:
	.size	_ZN2v88internal17ConcurrentMarking4TaskD0Ev, .-_ZN2v88internal17ConcurrentMarking4TaskD0Ev
	.section	.text._ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES4_.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES4_.constprop.0, @function
_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES4_.constprop.0:
.LFB29054:
	.cfi_startproc
	cmpq	%rdx, %rsi
	jnb	.L34
	.p2align 4,,10
	.p2align 3
.L35:
	movq	(%rsi), %r8
	movq	8(%rdi), %rcx
	movq	%rsi, %xmm0
	addq	$8, %rsi
	movslq	(%rcx), %rax
	movq	%r8, %xmm1
	punpcklqdq	%xmm1, %xmm0
	leal	1(%rax), %r9d
	salq	$4, %rax
	movl	%r9d, (%rcx)
	movups	%xmm0, 8(%rcx,%rax)
	cmpq	%rsi, %rdx
	ja	.L35
.L34:
	ret
	.cfi_endproc
.LFE29054:
	.size	_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES4_.constprop.0, .-_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES4_.constprop.0
	.section	.text._ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES4_.constprop.1,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES4_.constprop.1, @function
_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES4_.constprop.1:
.LFB29053:
	.cfi_startproc
	cmpq	%rdx, %rsi
	jnb	.L39
	.p2align 4,,10
	.p2align 3
.L40:
	movq	(%rsi), %r8
	movq	8(%rdi), %rcx
	movq	%rsi, %xmm0
	addq	$8, %rsi
	movslq	(%rcx), %rax
	movq	%r8, %xmm1
	punpcklqdq	%xmm1, %xmm0
	leal	1(%rax), %r9d
	salq	$4, %rax
	movl	%r9d, (%rcx)
	movups	%xmm0, 8(%rcx,%rax)
	cmpq	%rsi, %rdx
	ja	.L40
.L39:
	ret
	.cfi_endproc
.LFE29053:
	.size	_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES4_.constprop.1, .-_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES4_.constprop.1
	.section	.text._ZN2v88internal17ConcurrentMarking4TaskD2Ev,"axG",@progbits,_ZN2v88internal17ConcurrentMarking4TaskD5Ev,comdat
	.p2align 4
	.weak	_ZThn32_N2v88internal17ConcurrentMarking4TaskD1Ev
	.type	_ZThn32_N2v88internal17ConcurrentMarking4TaskD1Ev, @function
_ZThn32_N2v88internal17ConcurrentMarking4TaskD1Ev:
.LFB29043:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal14CancelableTaskE(%rip), %rax
	subq	$32, %rdi
	movq	%rax, (%rdi)
	jmp	_ZN2v88internal10CancelableD2Ev@PLT
	.cfi_endproc
.LFE29043:
	.size	_ZThn32_N2v88internal17ConcurrentMarking4TaskD1Ev, .-_ZThn32_N2v88internal17ConcurrentMarking4TaskD1Ev
	.section	.text._ZN2v88internal17ConcurrentMarking4TaskD0Ev,"axG",@progbits,_ZN2v88internal17ConcurrentMarking4TaskD5Ev,comdat
	.p2align 4
	.weak	_ZThn32_N2v88internal17ConcurrentMarking4TaskD0Ev
	.type	_ZThn32_N2v88internal17ConcurrentMarking4TaskD0Ev, @function
_ZThn32_N2v88internal17ConcurrentMarking4TaskD0Ev:
.LFB29044:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal14CancelableTaskE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-32(%rdi), %r12
	subq	$8, %rsp
	movq	%rax, -32(%rdi)
	movq	%r12, %rdi
	call	_ZN2v88internal10CancelableD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$64, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE29044:
	.size	_ZThn32_N2v88internal17ConcurrentMarking4TaskD0Ev, .-_ZThn32_N2v88internal17ConcurrentMarking4TaskD0Ev
	.section	.text._ZN2v88internal13ObjectVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_,"axG",@progbits,_ZN2v88internal13ObjectVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ObjectVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_
	.type	_ZN2v88internal13ObjectVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_, @function
_ZN2v88internal13ObjectVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_:
.LFB7783:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rdi), %rax
	leaq	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE(%rip), %rbx
	movq	40(%rax), %rcx
	cmpq	%rbx, %rcx
	jne	.L48
	leaq	8(%rdx), %rcx
	call	*16(%rax)
	movq	(%r12), %rax
	movq	40(%rax), %rcx
	cmpq	%rbx, %rcx
	jne	.L50
.L52:
	popq	%rbx
	leaq	8(%r14), %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	16(%rax), %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	call	*%rcx
	movq	(%r12), %rax
	movq	40(%rax), %rcx
	cmpq	%rbx, %rcx
	je	.L52
.L50:
	popq	%rbx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rcx
	.cfi_endproc
.LFE7783:
	.size	_ZN2v88internal13ObjectVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_, .-_ZN2v88internal13ObjectVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_
	.section	.rodata._ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_.str1.1,"aMS",@progbits,1
.LC1:
	.string	"NewArray"
	.section	.text._ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_,"axG",@progbits,_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	.type	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_, @function
_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_:
.LFB22236:
	.cfi_startproc
	endbr64
	cmpq	%rdx, %rcx
	jbe	.L93
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$56, %rsp
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L55:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jbe	.L97
.L68:
	movq	(%rbx), %r12
	testb	$1, %r12b
	je	.L55
	movq	%r12, %r9
	movl	%r12d, %eax
	movl	%r8d, %esi
	andl	$262143, %eax
	andq	$-262144, %r9
	movl	%eax, %ecx
	movq	16(%r9), %rdx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %esi
	leaq	(%rdx,%rax,4), %rdx
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L99:
	movl	%esi, %edi
	movl	%ecx, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rdx)
	cmpl	%eax, %ecx
	je	.L98
.L57:
	movl	(%rdx), %ecx
	movl	%esi, %eax
	andl	%ecx, %eax
	cmpl	%eax, %esi
	jne	.L99
.L56:
	movq	8(%r9), %rax
	testb	$64, %al
	je	.L55
.L104:
	movq	%r15, %r12
	andq	$-262144, %r12
	movq	8(%r12), %rax
	testb	$88, %al
	je	.L61
	testb	$-128, %ah
	je	.L55
.L61:
	movq	112(%r12), %rax
	testq	%rax, %rax
	je	.L100
.L63:
	movq	%rbx, %rcx
	movl	%ebx, %edx
	subq	%r12, %rcx
	andl	$262143, %edx
	shrq	$18, %rcx
	movl	%edx, %r12d
	movl	%edx, %r9d
	sarl	$13, %edx
	leaq	(%rcx,%rcx,2), %rcx
	sarl	$8, %r12d
	movslq	%edx, %rdx
	sarl	$3, %r9d
	salq	$7, %rcx
	andl	$31, %r12d
	andl	$31, %r9d
	leaq	(%rcx,%rdx,8), %rdx
	addq	%rax, %rdx
	movq	(%rdx), %r10
	testq	%r10, %r10
	je	.L101
.L65:
	movl	%r9d, %ecx
	movl	%r8d, %eax
	movslq	%r12d, %r12
	sall	%cl, %eax
	leaq	(%r10,%r12,4), %rsi
	movl	%eax, %ecx
	movl	(%rsi), %eax
	testl	%eax, %ecx
	je	.L67
	addq	$8, %rbx
	cmpq	%rbx, %r13
	ja	.L68
.L97:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L102:
	.cfi_restore_state
	movl	%ecx, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	je	.L55
.L67:
	movl	(%rsi), %edx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ecx
	jne	.L102
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L98:
	movslq	16(%r14), %rax
	movq	8(%r14), %rsi
	leaq	(%rax,%rax,4), %rdx
	salq	$4, %rdx
	addq	%rsi, %rdx
	movq	(%rdx), %rax
	movq	8(%rax), %rcx
	cmpq	$64, %rcx
	je	.L103
	leaq	1(%rcx), %rdx
	movq	%rdx, 8(%rax)
	movq	%r12, 16(%rax,%rcx,8)
	movq	8(%r9), %rax
	testb	$64, %al
	je	.L55
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L101:
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	movq	%rdx, -64(%rbp)
	movl	%r9d, -56(%rbp)
	call	_ZnamRKSt9nothrow_t@PLT
	movl	-56(%rbp), %r9d
	movq	-64(%rbp), %rdx
	movl	$1, %r8d
	testq	%rax, %rax
	movq	%rax, %r10
	je	.L105
.L66:
	leaq	8(%r10), %rdi
	movq	%r10, %rcx
	xorl	%eax, %eax
	movq	$0, (%r10)
	andq	$-8, %rdi
	movq	$0, 120(%r10)
	subq	%rdi, %rcx
	subl	$-128, %ecx
	shrl	$3, %ecx
	rep stosq
	lock cmpxchgq	%r10, (%rdx)
	movq	%rdx, -56(%rbp)
	testq	%rax, %rax
	je	.L65
	movq	%r10, %rdi
	movl	%r9d, -64(%rbp)
	call	_ZdaPv@PLT
	movq	-56(%rbp), %rdx
	movl	$1, %r8d
	movq	(%rdx), %r10
	movl	-64(%rbp), %r9d
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L100:
	movq	%r12, %rdi
	call	_ZN2v88internal11MemoryChunk15AllocateSlotSetILNS0_17RememberedSetTypeE1EEEPNS0_7SlotSetEv@PLT
	movl	$1, %r8d
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L103:
	leaq	640(%rsi), %rdi
	movq	%rdx, -96(%rbp)
	movq	%rcx, -88(%rbp)
	movq	%r9, -72(%rbp)
	movq	%rax, -80(%rbp)
	movq	%rsi, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-64(%rbp), %rsi
	movq	-80(%rbp), %rax
	movq	680(%rsi), %r10
	movq	%r10, (%rax)
	movq	%rax, 680(%rsi)
	movq	-56(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movq	-88(%rbp), %rcx
	movq	-96(%rbp), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	%rax, %rsi
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rsi, (%rdx)
	movq	-72(%rbp), %r9
	movq	8(%rsi), %rax
	cmpq	$64, %rax
	je	.L56
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%rsi)
	movq	%r12, 16(%rsi,%rax,8)
	movq	8(%r9), %rax
	testb	$64, %al
	je	.L55
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L93:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
.L105:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movl	-56(%rbp), %r9d
	movq	-64(%rbp), %rdx
	movl	$1, %r8d
	testq	%rax, %rax
	movq	%rax, %r10
	jne	.L66
	leaq	.LC1(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
	.cfi_endproc
.LFE22236:
	.size	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_, .-_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	.section	.text._ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES3_,"axG",@progbits,_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES3_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES3_
	.type	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES3_, @function
_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES3_:
.LFB22237:
	.cfi_startproc
	endbr64
	cmpq	%rcx, %rdx
	jnb	.L183
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L109:
	addq	$8, %r12
	cmpq	%r12, %r13
	jbe	.L187
.L107:
	movq	(%r12), %rbx
	movq	%rbx, %rax
	andl	$3, %eax
	cmpq	$1, %rax
	je	.L108
	cmpq	$3, %rax
	jne	.L109
	cmpl	$3, %ebx
	je	.L109
	movq	%rbx, %rdx
	andq	$-3, %rbx
	andq	$-262144, %rdx
	movq	%rbx, %rcx
	movl	%r8d, %ebx
	subl	%edx, %ecx
	movq	16(%rdx), %rsi
	movl	%ecx, %eax
	shrl	$3, %ecx
	shrl	$8, %eax
	sall	%cl, %ebx
	movl	(%rsi,%rax,4), %eax
	testl	%eax, %ebx
	je	.L188
	movq	8(%rdx), %rax
	testb	$64, %al
	je	.L109
	movq	%r15, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testb	$88, %al
	je	.L126
	testb	$-128, %ah
	je	.L109
.L126:
	movq	112(%rbx), %rax
	testq	%rax, %rax
	je	.L189
.L128:
	movq	%r12, %rcx
	movl	%r12d, %edx
	subq	%rbx, %rcx
	andl	$262143, %edx
	shrq	$18, %rcx
	movl	%edx, %ebx
	movl	%edx, %r9d
	sarl	$13, %edx
	leaq	(%rcx,%rcx,2), %rcx
	sarl	$8, %ebx
	movslq	%edx, %rdx
	sarl	$3, %r9d
	salq	$7, %rcx
	andl	$31, %ebx
	andl	$31, %r9d
	leaq	(%rcx,%rdx,8), %rdx
	addq	%rax, %rdx
	movq	(%rdx), %r10
	testq	%r10, %r10
	je	.L190
.L130:
	movl	%r9d, %ecx
	movl	%r8d, %esi
	movslq	%ebx, %rbx
	sall	%cl, %esi
	leaq	(%r10,%rbx,4), %rcx
	movl	(%rcx), %eax
	testl	%eax, %esi
	je	.L133
	addq	$8, %r12
	cmpq	%r12, %r13
	ja	.L107
.L187:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L108:
	.cfi_restore_state
	movq	%rbx, %r9
	movl	%ebx, %eax
	movl	%r8d, %esi
	andl	$262143, %eax
	andq	$-262144, %r9
	movl	%eax, %ecx
	movq	16(%r9), %rdx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %esi
	leaq	(%rdx,%rax,4), %rdx
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L192:
	movl	%esi, %edi
	movl	%ecx, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rdx)
	cmpl	%eax, %ecx
	je	.L191
.L113:
	movl	(%rdx), %ecx
	movl	%esi, %eax
	andl	%ecx, %eax
	cmpl	%eax, %esi
	jne	.L192
.L112:
	movq	8(%r9), %rax
	testb	$64, %al
	je	.L109
.L199:
	movq	%r15, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testb	$88, %al
	jne	.L193
	movq	112(%rbx), %rax
	testq	%rax, %rax
	je	.L194
.L120:
	movq	%r12, %rcx
	movl	%r12d, %edx
	subq	%rbx, %rcx
	andl	$262143, %edx
	shrq	$18, %rcx
	movl	%edx, %ebx
	movl	%edx, %r9d
	sarl	$13, %edx
	leaq	(%rcx,%rcx,2), %rcx
	sarl	$8, %ebx
	movslq	%edx, %rdx
	sarl	$3, %r9d
	salq	$7, %rcx
	andl	$31, %ebx
	andl	$31, %r9d
	leaq	(%rcx,%rdx,8), %rdx
	addq	%rax, %rdx
	movq	(%rdx), %r10
	testq	%r10, %r10
	je	.L195
.L122:
	movl	%r9d, %ecx
	movl	%r8d, %eax
	movslq	%ebx, %rbx
	sall	%cl, %eax
	leaq	(%r10,%rbx,4), %rsi
	movl	%eax, %ecx
	movl	(%rsi), %eax
	testl	%eax, %ecx
	jne	.L109
	movl	(%rsi), %edx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ecx
	je	.L109
	.p2align 4,,10
	.p2align 3
.L196:
	movl	%ecx, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	je	.L109
	movl	(%rsi), %edx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ecx
	jne	.L196
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L188:
	movslq	64(%r14), %rax
	movq	24(%r14), %rdx
	leaq	(%rax,%rax,4), %rsi
	salq	$4, %rsi
	addq	%rdx, %rsi
	movq	3480(%rsi), %rbx
	movq	8(%rbx), %rax
	cmpq	$64, %rax
	je	.L197
	addq	$1, %rax
	movq	%rax, 8(%rbx)
	salq	$4, %rax
	addq	%rbx, %rax
	movq	%r15, (%rax)
	movq	%r12, 8(%rax)
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L191:
	movslq	16(%r14), %rax
	movq	8(%r14), %rsi
	leaq	(%rax,%rax,4), %rdx
	salq	$4, %rdx
	addq	%rsi, %rdx
	movq	(%rdx), %rax
	movq	8(%rax), %rcx
	cmpq	$64, %rcx
	je	.L198
	leaq	1(%rcx), %rdx
	movq	%rdx, 8(%rax)
	movq	%rbx, 16(%rax,%rcx,8)
	movq	8(%r9), %rax
	testb	$64, %al
	je	.L109
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L193:
	testb	$-128, %ah
	je	.L109
	movq	112(%rbx), %rax
	testq	%rax, %rax
	jne	.L120
.L194:
	movq	%rbx, %rdi
	call	_ZN2v88internal11MemoryChunk15AllocateSlotSetILNS0_17RememberedSetTypeE1EEEPNS0_7SlotSetEv@PLT
	movl	$1, %r8d
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L200:
	movl	%esi, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rcx)
	cmpl	%eax, %edx
	je	.L109
.L133:
	movl	(%rcx), %edx
	movl	%esi, %eax
	andl	%edx, %eax
	cmpl	%eax, %esi
	jne	.L200
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L197:
	leaq	4120(%rdx), %rdi
	movq	%rsi, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-64(%rbp), %rdx
	movq	4160(%rdx), %rax
	movq	%rax, (%rbx)
	movq	%rbx, 4160(%rdx)
	movq	-56(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$1040, %edi
	call	_Znwm@PLT
	movq	-72(%rbp), %rsi
	pxor	%xmm0, %xmm0
	movl	$1, %r8d
	movq	$0, 8(%rax)
	leaq	16(%rax), %rdx
	leaq	1040(%rax), %rcx
	.p2align 4,,10
	.p2align 3
.L135:
	movups	%xmm0, (%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rcx
	jne	.L135
	movq	%rax, 3480(%rsi)
	movq	8(%rax), %rdx
	cmpq	$64, %rdx
	je	.L109
	addq	$1, %rdx
	movq	%rdx, 8(%rax)
	salq	$4, %rdx
	addq	%rdx, %rax
	movq	%r15, (%rax)
	movq	%r12, 8(%rax)
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L198:
	leaq	640(%rsi), %rdi
	movq	%rdx, -96(%rbp)
	movq	%rcx, -88(%rbp)
	movq	%r9, -72(%rbp)
	movq	%rax, -80(%rbp)
	movq	%rsi, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-64(%rbp), %rsi
	movq	-80(%rbp), %rax
	movq	680(%rsi), %r10
	movq	%r10, (%rax)
	movq	%rax, 680(%rsi)
	movq	-56(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movq	-88(%rbp), %rcx
	movq	-96(%rbp), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	%rax, %rsi
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rsi, (%rdx)
	movq	-72(%rbp), %r9
	movq	8(%rsi), %rax
	cmpq	$64, %rax
	je	.L112
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%rsi)
	movq	%rbx, 16(%rsi,%rax,8)
	jmp	.L112
.L190:
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	movq	%rdx, -64(%rbp)
	movl	%r9d, -56(%rbp)
	call	_ZnamRKSt9nothrow_t@PLT
	movl	-56(%rbp), %r9d
	movq	-64(%rbp), %rdx
	movl	$1, %r8d
	testq	%rax, %rax
	movq	%rax, %r10
	je	.L201
.L131:
	leaq	8(%r10), %rdi
	movq	%r10, %rcx
	xorl	%eax, %eax
	movq	$0, (%r10)
	andq	$-8, %rdi
	movq	$0, 120(%r10)
	subq	%rdi, %rcx
	subl	$-128, %ecx
	shrl	$3, %ecx
	rep stosq
	lock cmpxchgq	%r10, (%rdx)
	movq	%rdx, -56(%rbp)
	testq	%rax, %rax
	je	.L130
	movq	%r10, %rdi
	movl	%r9d, -64(%rbp)
	call	_ZdaPv@PLT
	movq	-56(%rbp), %rdx
	movl	$1, %r8d
	movq	(%rdx), %r10
	movl	-64(%rbp), %r9d
	jmp	.L130
.L189:
	movq	%rbx, %rdi
	call	_ZN2v88internal11MemoryChunk15AllocateSlotSetILNS0_17RememberedSetTypeE1EEEPNS0_7SlotSetEv@PLT
	movl	$1, %r8d
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L195:
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	movq	%rdx, -64(%rbp)
	movl	%r9d, -56(%rbp)
	call	_ZnamRKSt9nothrow_t@PLT
	movl	-56(%rbp), %r9d
	movq	-64(%rbp), %rdx
	movl	$1, %r8d
	testq	%rax, %rax
	movq	%rax, %r10
	je	.L202
.L123:
	leaq	8(%r10), %rdi
	movq	%r10, %rcx
	xorl	%eax, %eax
	movq	$0, (%r10)
	andq	$-8, %rdi
	movq	$0, 120(%r10)
	subq	%rdi, %rcx
	subl	$-128, %ecx
	shrl	$3, %ecx
	rep stosq
	lock cmpxchgq	%r10, (%rdx)
	movq	%rdx, -56(%rbp)
	testq	%rax, %rax
	je	.L122
	movq	%r10, %rdi
	movl	%r9d, -64(%rbp)
	call	_ZdaPv@PLT
	movq	-56(%rbp), %rdx
	movl	$1, %r8d
	movq	(%rdx), %r10
	movl	-64(%rbp), %r9d
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L183:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
.L201:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movl	-56(%rbp), %r9d
	movq	-64(%rbp), %rdx
	movl	$1, %r8d
	testq	%rax, %rax
	movq	%rax, %r10
	jne	.L131
.L132:
	leaq	.LC1(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
.L202:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movl	-56(%rbp), %r9d
	movq	-64(%rbp), %rdx
	movl	$1, %r8d
	testq	%rax, %rax
	movq	%rax, %r10
	jne	.L123
	jmp	.L132
	.cfi_endproc
.LFE22237:
	.size	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES3_, .-_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES3_
	.section	.text._ZN2v88internal19FixedBodyDescriptorILi8ELi16ELi16EE11IterateBodyINS0_24ConcurrentMarkingVisitorEEEvNS0_3MapENS0_10HeapObjectEiPT_.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal19FixedBodyDescriptorILi8ELi16ELi16EE11IterateBodyINS0_24ConcurrentMarkingVisitorEEEvNS0_3MapENS0_10HeapObjectEiPT_.isra.0, @function
_ZN2v88internal19FixedBodyDescriptorILi8ELi16ELi16EE11IterateBodyINS0_24ConcurrentMarkingVisitorEEEvNS0_3MapENS0_10HeapObjectEiPT_.isra.0:
.LFB28948:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	15(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	7(%rdi), %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	cmpq	%r12, %rax
	jbe	.L203
	movq	7(%rdi), %r14
	movq	%rdi, %rbx
	testb	$1, %r14b
	jne	.L241
.L203:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L241:
	.cfi_restore_state
	movl	%r14d, %eax
	movq	%r14, %r15
	movl	$1, %edx
	andl	$262143, %eax
	andq	$-262144, %r15
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %edx
	movq	16(%r15), %rcx
	leaq	(%rcx,%rax,4), %rdi
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L243:
	movl	%edx, %r8d
	movl	%ecx, %eax
	orl	%ecx, %r8d
	lock cmpxchgl	%r8d, (%rdi)
	cmpl	%eax, %ecx
	je	.L242
.L207:
	movl	(%rdi), %ecx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%eax, %edx
	jne	.L243
.L206:
	movq	8(%r15), %rax
	testb	$64, %al
	je	.L203
.L248:
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testb	$88, %al
	je	.L211
	testb	$-128, %ah
	je	.L203
.L211:
	movq	112(%rbx), %rax
	testq	%rax, %rax
	je	.L244
.L214:
	movq	%r12, %rdx
	andl	$262143, %r12d
	subq	%rbx, %rdx
	movl	%r12d, %r13d
	movl	%r12d, %ebx
	sarl	$13, %r12d
	shrq	$18, %rdx
	movslq	%r12d, %r12
	sarl	$8, %ebx
	leaq	(%rdx,%rdx,2), %rdx
	sarl	$3, %r13d
	andl	$31, %ebx
	salq	$7, %rdx
	andl	$31, %r13d
	leaq	(%rdx,%r12,8), %r12
	addq	%rax, %r12
	movq	(%r12), %r8
	testq	%r8, %r8
	je	.L245
.L216:
	movslq	%ebx, %rbx
	movl	$1, %edx
	movl	%r13d, %ecx
	leaq	(%r8,%rbx,4), %rsi
	sall	%cl, %edx
	movl	(%rsi), %eax
	testl	%eax, %edx
	je	.L218
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L246:
	.cfi_restore_state
	movl	%edx, %edi
	movl	%ecx, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %ecx
	je	.L203
.L218:
	movl	(%rsi), %ecx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%eax, %edx
	jne	.L246
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L242:
	.cfi_restore_state
	movslq	16(%rsi), %rax
	movq	8(%rsi), %rdx
	leaq	(%rax,%rax,4), %r13
	salq	$4, %r13
	addq	%rdx, %r13
	movq	0(%r13), %rax
	movq	8(%rax), %rcx
	cmpq	$64, %rcx
	je	.L247
	leaq	1(%rcx), %rdx
	movq	%rdx, 8(%rax)
	movq	%r14, 16(%rax,%rcx,8)
	movq	8(%r15), %rax
	testb	$64, %al
	je	.L203
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L245:
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L249
.L217:
	leaq	8(%r8), %rdi
	movq	%r8, %rcx
	xorl	%eax, %eax
	movq	$0, (%r8)
	andq	$-8, %rdi
	movq	$0, 120(%r8)
	subq	%rdi, %rcx
	subl	$-128, %ecx
	shrl	$3, %ecx
	rep stosq
	lock cmpxchgq	%r8, (%r12)
	testq	%rax, %rax
	je	.L216
	movq	%r8, %rdi
	call	_ZdaPv@PLT
	movq	(%r12), %r8
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L244:
	movq	%rbx, %rdi
	call	_ZN2v88internal11MemoryChunk15AllocateSlotSetILNS0_17RememberedSetTypeE1EEEPNS0_7SlotSetEv@PLT
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L247:
	leaq	640(%rdx), %rdi
	movq	%rcx, -80(%rbp)
	movq	%rax, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %rax
	movq	680(%rdx), %rsi
	movq	%rsi, (%rax)
	movq	%rax, 680(%rdx)
	movq	-56(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	$0, 8(%rax)
	movq	%rax, %rdx
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rdx, 0(%r13)
	movq	8(%rdx), %rax
	cmpq	$64, %rax
	je	.L206
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdx)
	movq	%r14, 16(%rdx,%rax,8)
	movq	8(%r15), %rax
	testb	$64, %al
	je	.L203
	jmp	.L248
.L249:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	jne	.L217
	leaq	.LC1(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
	.cfi_endproc
.LFE28948:
	.size	_ZN2v88internal19FixedBodyDescriptorILi8ELi16ELi16EE11IterateBodyINS0_24ConcurrentMarkingVisitorEEEvNS0_3MapENS0_10HeapObjectEiPT_.isra.0, .-_ZN2v88internal19FixedBodyDescriptorILi8ELi16ELi16EE11IterateBodyINS0_24ConcurrentMarkingVisitorEEEvNS0_3MapENS0_10HeapObjectEiPT_.isra.0
	.set	_ZN2v88internal19FixedBodyDescriptorILi8ELi16ELi24EE11IterateBodyINS0_24ConcurrentMarkingVisitorEEEvNS0_3MapENS0_10HeapObjectEiPT_.isra.0,_ZN2v88internal19FixedBodyDescriptorILi8ELi16ELi16EE11IterateBodyINS0_24ConcurrentMarkingVisitorEEEvNS0_3MapENS0_10HeapObjectEiPT_.isra.0
	.section	.text._ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE.constprop.1,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE.constprop.1, @function
_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE.constprop.1:
.LFB29049:
	.cfi_startproc
	cmpq	$-9, %rdx
	ja	.L286
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$40, %rsp
	movq	(%rdx), %r14
	testb	$1, %r14b
	jne	.L291
.L250:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L291:
	.cfi_restore_state
	movl	%r14d, %eax
	movq	%r14, %r15
	movl	$1, %edx
	movq	%rsi, %r13
	andl	$262143, %eax
	andq	$-262144, %r15
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %edx
	movq	16(%r15), %rcx
	leaq	(%rcx,%rax,4), %rsi
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L293:
	movl	%edx, %r8d
	movl	%ecx, %eax
	orl	%ecx, %r8d
	lock cmpxchgl	%r8d, (%rsi)
	cmpl	%eax, %ecx
	je	.L292
.L254:
	movl	(%rsi), %ecx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%eax, %edx
	jne	.L293
.L253:
	movq	8(%r15), %rax
	testb	$64, %al
	je	.L250
.L298:
	andq	$-262144, %r13
	movq	8(%r13), %rax
	testb	$88, %al
	je	.L258
	testb	$-128, %ah
	je	.L250
.L258:
	movq	112(%r13), %rax
	testq	%rax, %rax
	je	.L294
.L261:
	movq	%rbx, %rdx
	andl	$262143, %ebx
	subq	%r13, %rdx
	movl	%ebx, %r12d
	movl	%ebx, %r13d
	sarl	$13, %ebx
	shrq	$18, %rdx
	movslq	%ebx, %rbx
	sarl	$8, %r12d
	leaq	(%rdx,%rdx,2), %rdx
	sarl	$3, %r13d
	andl	$31, %r12d
	salq	$7, %rdx
	andl	$31, %r13d
	leaq	(%rdx,%rbx,8), %rbx
	addq	%rax, %rbx
	movq	(%rbx), %r8
	testq	%r8, %r8
	je	.L295
.L263:
	movslq	%r12d, %r12
	movl	$1, %edx
	movl	%r13d, %ecx
	leaq	(%r8,%r12,4), %rsi
	sall	%cl, %edx
	movl	(%rsi), %eax
	testl	%eax, %edx
	je	.L265
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L296:
	.cfi_restore_state
	movl	%edx, %edi
	movl	%ecx, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %ecx
	je	.L250
.L265:
	movl	(%rsi), %ecx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%eax, %edx
	jne	.L296
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L292:
	.cfi_restore_state
	movslq	16(%rdi), %rax
	movq	8(%rdi), %rdx
	leaq	(%rax,%rax,4), %r12
	salq	$4, %r12
	addq	%rdx, %r12
	movq	(%r12), %rax
	movq	8(%rax), %rcx
	cmpq	$64, %rcx
	je	.L297
	leaq	1(%rcx), %rdx
	movq	%rdx, 8(%rax)
	movq	%r14, 16(%rax,%rcx,8)
	movq	8(%r15), %rax
	testb	$64, %al
	je	.L250
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L295:
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L299
.L264:
	leaq	8(%r8), %rdi
	movq	%r8, %rcx
	xorl	%eax, %eax
	movq	$0, (%r8)
	andq	$-8, %rdi
	movq	$0, 120(%r8)
	subq	%rdi, %rcx
	subl	$-128, %ecx
	shrl	$3, %ecx
	rep stosq
	lock cmpxchgq	%r8, (%rbx)
	testq	%rax, %rax
	je	.L263
	movq	%r8, %rdi
	call	_ZdaPv@PLT
	movq	(%rbx), %r8
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L294:
	movq	%r13, %rdi
	call	_ZN2v88internal11MemoryChunk15AllocateSlotSetILNS0_17RememberedSetTypeE1EEEPNS0_7SlotSetEv@PLT
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L297:
	leaq	640(%rdx), %rdi
	movq	%rcx, -80(%rbp)
	movq	%rax, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %rax
	movq	680(%rdx), %rsi
	movq	%rsi, (%rax)
	movq	%rax, 680(%rdx)
	movq	-56(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	$0, 8(%rax)
	movq	%rax, %rdx
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rdx, (%r12)
	movq	8(%rdx), %rax
	cmpq	$64, %rax
	je	.L253
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdx)
	movq	%r14, 16(%rdx,%rax,8)
	movq	8(%r15), %rax
	testb	$64, %al
	je	.L250
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L286:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
.L299:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	jne	.L264
	leaq	.LC1(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
	.cfi_endproc
.LFE29049:
	.size	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE.constprop.1, .-_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE.constprop.1
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB9545:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L306
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L309
.L300:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L306:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L309:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L300
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE9545:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.text._ZN2v88internal10TimedScope11TimestampMsEv,"axG",@progbits,_ZN2v88internal10TimedScope11TimestampMsEv,comdat
	.p2align 4
	.weak	_ZN2v88internal10TimedScope11TimestampMsEv
	.type	_ZN2v88internal10TimedScope11TimestampMsEv, @function
_ZN2v88internal10TimedScope11TimestampMsEv:
.LFB12704:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*120(%rax)
	mulsd	.LC2(%rip), %xmm0
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE12704:
	.size	_ZN2v88internal10TimedScope11TimestampMsEv, .-_ZN2v88internal10TimedScope11TimestampMsEv
	.section	.text._ZN2v88internal10JSFunction30NeedsResetDueToFlushedBytecodeEv,"axG",@progbits,_ZN2v88internal10JSFunction30NeedsResetDueToFlushedBytecodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JSFunction30NeedsResetDueToFlushedBytecodeEv
	.type	_ZN2v88internal10JSFunction30NeedsResetDueToFlushedBytecodeEv, @function
_ZN2v88internal10JSFunction30NeedsResetDueToFlushedBytecodeEv:
.LFB15334:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	23(%rax), %rax
	movq	(%rdi), %rdx
	movq	47(%rdx), %rdx
	testb	$1, %al
	jne	.L313
.L315:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L313:
	movq	-1(%rax), %rcx
	cmpw	$160, 11(%rcx)
	jne	.L315
	testb	$1, %dl
	je	.L315
	movq	-1(%rdx), %rcx
	cmpw	$69, 11(%rcx)
	jne	.L315
	movabsq	$287762808832, %rcx
	movq	7(%rax), %rax
	cmpq	%rcx, %rax
	je	.L317
	testb	$1, %al
	je	.L315
	movq	-1(%rax), %rcx
	cmpw	$165, 11(%rcx)
	je	.L317
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L315
.L317:
	cmpl	$67, 59(%rdx)
	setne	%al
	ret
	.cfi_endproc
.LFE15334:
	.size	_ZN2v88internal10JSFunction30NeedsResetDueToFlushedBytecodeEv, .-_ZN2v88internal10JSFunction30NeedsResetDueToFlushedBytecodeEv
	.section	.text._ZNK2v88internal3Map16UsedInstanceSizeEv,"axG",@progbits,_ZNK2v88internal3Map16UsedInstanceSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal3Map16UsedInstanceSizeEv
	.type	_ZNK2v88internal3Map16UsedInstanceSizeEv, @function
_ZNK2v88internal3Map16UsedInstanceSizeEv:
.LFB18296:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movzbl	9(%rdx), %eax
	leal	0(,%rax,8), %r8d
	cmpl	$2, %eax
	jle	.L321
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L321:
	movzbl	7(%rdx), %r8d
	sall	$3, %r8d
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE18296:
	.size	_ZNK2v88internal3Map16UsedInstanceSizeEv, .-_ZNK2v88internal3Map16UsedInstanceSizeEv
	.section	.text._ZN2v88internal24ConcurrentMarkingVisitor23VisitPointersInSnapshotENS0_10HeapObjectERKNS0_12SlotSnapshotE,"axG",@progbits,_ZN2v88internal24ConcurrentMarkingVisitor23VisitPointersInSnapshotENS0_10HeapObjectERKNS0_12SlotSnapshotE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal24ConcurrentMarkingVisitor23VisitPointersInSnapshotENS0_10HeapObjectERKNS0_12SlotSnapshotE
	.type	_ZN2v88internal24ConcurrentMarkingVisitor23VisitPointersInSnapshotENS0_10HeapObjectERKNS0_12SlotSnapshotE, @function
_ZN2v88internal24ConcurrentMarkingVisitor23VisitPointersInSnapshotENS0_10HeapObjectERKNS0_12SlotSnapshotE:
.LFB22242:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rdx), %ecx
	movq	%rsi, -56(%rbp)
	testl	%ecx, %ecx
	jle	.L322
	movq	%rdi, %r8
	movq	%rdx, %r14
	xorl	%ebx, %ebx
	movl	$1, %r12d
	.p2align 4,,10
	.p2align 3
.L323:
	movq	%rbx, %rax
	salq	$4, %rax
	movq	16(%r14,%rax), %r13
	testb	$1, %r13b
	je	.L324
	movq	8(%r14,%rax), %r15
	movl	%r13d, %eax
	movq	%r13, %r9
	movl	%r12d, %edi
	andl	$262143, %eax
	andq	$-262144, %r9
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %edi
	movq	16(%r9), %rcx
	leaq	(%rcx,%rax,4), %rsi
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L364:
	movl	%edi, %r10d
	movl	%ecx, %eax
	orl	%ecx, %r10d
	lock cmpxchgl	%r10d, (%rsi)
	cmpl	%eax, %ecx
	je	.L363
.L326:
	movl	(%rsi), %ecx
	movl	%edi, %eax
	andl	%ecx, %eax
	cmpl	%eax, %edi
	jne	.L364
.L325:
	movq	8(%r9), %rax
	testb	$64, %al
	jne	.L329
.L361:
	movl	(%r14), %ecx
.L324:
	addq	$1, %rbx
	cmpl	%ebx, %ecx
	jg	.L323
.L322:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L363:
	.cfi_restore_state
	movslq	16(%r8), %rax
	movq	8(%r8), %r10
	leaq	(%rax,%rax,4), %rsi
	salq	$4, %rsi
	addq	%r10, %rsi
	movq	(%rsi), %rax
	movq	8(%rax), %rcx
	cmpq	$64, %rcx
	je	.L365
	leaq	1(%rcx), %rsi
	movq	%rsi, 8(%rax)
	movq	%r13, 16(%rax,%rcx,8)
	movq	8(%r9), %rax
	testb	$64, %al
	je	.L361
.L329:
	movq	-56(%rbp), %r13
	andq	$-262144, %r13
	movq	8(%r13), %rax
	testb	$88, %al
	je	.L330
	testb	$-128, %ah
	je	.L361
.L330:
	movq	112(%r13), %rax
	testq	%rax, %rax
	je	.L366
.L332:
	movq	%r15, %rcx
	andl	$262143, %r15d
	subq	%r13, %rcx
	movl	%r15d, %r9d
	movl	%r15d, %r13d
	sarl	$13, %r15d
	shrq	$18, %rcx
	movslq	%r15d, %r15
	sarl	$8, %r13d
	leaq	(%rcx,%rcx,2), %rcx
	sarl	$3, %r9d
	andl	$31, %r13d
	salq	$7, %rcx
	andl	$31, %r9d
	leaq	(%rcx,%r15,8), %r15
	addq	%rax, %r15
	movq	(%r15), %r10
	testq	%r10, %r10
	je	.L367
.L334:
	movslq	%r13d, %r13
	movl	%r12d, %esi
	movl	%r9d, %ecx
	leaq	(%r10,%r13,4), %rdi
	sall	%cl, %esi
	movl	(%rdi), %eax
	testl	%eax, %esi
	jne	.L361
	movl	(%rdi), %ecx
	movl	%esi, %eax
	andl	%ecx, %eax
	cmpl	%eax, %esi
	je	.L361
	.p2align 4,,10
	.p2align 3
.L337:
	movl	%esi, %r9d
	movl	%ecx, %eax
	orl	%ecx, %r9d
	lock cmpxchgl	%r9d, (%rdi)
	cmpl	%eax, %ecx
	je	.L361
	movl	(%rdi), %ecx
	movl	%esi, %eax
	andl	%ecx, %eax
	cmpl	%eax, %esi
	jne	.L337
	jmp	.L361
	.p2align 4,,10
	.p2align 3
.L366:
	movq	%r13, %rdi
	movq	%r8, -64(%rbp)
	call	_ZN2v88internal11MemoryChunk15AllocateSlotSetILNS0_17RememberedSetTypeE1EEEPNS0_7SlotSetEv@PLT
	movq	-64(%rbp), %r8
	jmp	.L332
	.p2align 4,,10
	.p2align 3
.L367:
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	movq	%r8, -72(%rbp)
	movl	%r9d, -64(%rbp)
	call	_ZnamRKSt9nothrow_t@PLT
	movl	-64(%rbp), %r9d
	movq	-72(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r10
	je	.L368
.L335:
	leaq	8(%r10), %rdi
	movq	%r10, %rcx
	xorl	%eax, %eax
	movq	$0, (%r10)
	andq	$-8, %rdi
	movq	$0, 120(%r10)
	subq	%rdi, %rcx
	subl	$-128, %ecx
	shrl	$3, %ecx
	rep stosq
	lock cmpxchgq	%r10, (%r15)
	testq	%rax, %rax
	je	.L334
	movq	%r10, %rdi
	movq	%r8, -72(%rbp)
	movl	%r9d, -64(%rbp)
	call	_ZdaPv@PLT
	movq	(%r15), %r10
	movq	-72(%rbp), %r8
	movl	-64(%rbp), %r9d
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L365:
	leaq	640(%r10), %rdi
	movq	%r8, -112(%rbp)
	movq	%rcx, -104(%rbp)
	movq	%r9, -88(%rbp)
	movq	%rsi, -80(%rbp)
	movq	%rax, -96(%rbp)
	movq	%r10, -72(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-72(%rbp), %r10
	movq	-96(%rbp), %rax
	movq	680(%r10), %r11
	movq	%r11, (%rax)
	movq	%rax, 680(%r10)
	movq	-64(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movq	-104(%rbp), %rcx
	movq	-80(%rbp), %rsi
	movq	%rax, %r10
	leaq	16(%rax), %rdi
	movq	-88(%rbp), %r9
	movq	-112(%rbp), %r8
	movq	$0, 8(%rax)
	xorl	%eax, %eax
	rep stosq
	movq	%r10, (%rsi)
	movq	8(%r10), %rax
	cmpq	$64, %rax
	je	.L325
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%r10)
	movq	%r13, 16(%r10,%rax,8)
	jmp	.L325
.L368:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movl	-64(%rbp), %r9d
	movq	-72(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r10
	jne	.L335
	leaq	.LC1(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
	.cfi_endproc
.LFE22242:
	.size	_ZN2v88internal24ConcurrentMarkingVisitor23VisitPointersInSnapshotENS0_10HeapObjectERKNS0_12SlotSnapshotE, .-_ZN2v88internal24ConcurrentMarkingVisitor23VisitPointersInSnapshotENS0_10HeapObjectERKNS0_12SlotSnapshotE
	.section	.text._ZN2v88internal24ConcurrentMarkingVisitor16VisitDescriptorsENS0_15DescriptorArrayEi,"axG",@progbits,_ZN2v88internal24ConcurrentMarkingVisitor16VisitDescriptorsENS0_15DescriptorArrayEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal24ConcurrentMarkingVisitor16VisitDescriptorsENS0_15DescriptorArrayEi
	.type	_ZN2v88internal24ConcurrentMarkingVisitor16VisitDescriptorsENS0_15DescriptorArrayEi, @function
_ZN2v88internal24ConcurrentMarkingVisitor16VisitDescriptorsENS0_15DescriptorArrayEi:
.LFB22264:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movswl	%dx, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%edx, %ebx
	movl	%r13d, %edx
	subq	$24, %rsp
	movq	%rsi, -40(%rbp)
	movl	4164(%rdi), %esi
	leaq	-40(%rbp), %rdi
	call	_ZN2v88internal15DescriptorArray31UpdateNumberOfMarkedDescriptorsEjs@PLT
	cmpw	%ax, %bx
	jg	.L372
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L372:
	.cfi_restore_state
	cwtl
	movq	-40(%rbp), %rsi
	leal	3(%r13,%r13,2), %ecx
	leal	3(%rax,%rax,2), %edx
	sall	$3, %ecx
	sall	$3, %edx
	leaq	-1(%rsi), %rdi
	movslq	%ecx, %rcx
	movslq	%edx, %rdx
	addq	%rdi, %rcx
	addq	%rdi, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES3_
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22264:
	.size	_ZN2v88internal24ConcurrentMarkingVisitor16VisitDescriptorsENS0_15DescriptorArrayEi, .-_ZN2v88internal24ConcurrentMarkingVisitor16VisitDescriptorsENS0_15DescriptorArrayEi
	.section	.text._ZN2v88internal24ConcurrentMarkingVisitor16ProcessEphemeronENS0_10HeapObjectES2_,"axG",@progbits,_ZN2v88internal24ConcurrentMarkingVisitor16ProcessEphemeronENS0_10HeapObjectES2_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal24ConcurrentMarkingVisitor16ProcessEphemeronENS0_10HeapObjectES2_
	.type	_ZN2v88internal24ConcurrentMarkingVisitor16ProcessEphemeronENS0_10HeapObjectES2_, @function
_ZN2v88internal24ConcurrentMarkingVisitor16ProcessEphemeronENS0_10HeapObjectES2_:
.LFB22269:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rcx
	movl	%esi, %r10d
	movl	$1, %r9d
	andl	$262143, %r10d
	andq	$-262144, %rcx
	movl	%r9d, %r8d
	movq	%rdx, %r11
	movl	%r10d, %eax
	andq	$-262144, %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	shrl	$8, %eax
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rcx), %rcx
	movl	(%rcx,%rax,4), %r12d
	movl	%edx, %eax
	andl	$262143, %eax
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %r8d
	movl	%r10d, %ecx
	shrl	$3, %ecx
	sall	%cl, %r9d
	testl	%r12d, %r9d
	je	.L374
	leaq	0(,%rax,4), %rsi
	addq	16(%r11), %rsi
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L398:
	movl	%ecx, %r9d
	movl	%ecx, %eax
	orl	%r8d, %r9d
	lock cmpxchgl	%r9d, (%rsi)
	cmpl	%eax, %ecx
	je	.L397
.L376:
	movl	(%rsi), %ecx
	movl	%ecx, %eax
	andl	%r8d, %eax
	cmpl	%r8d, %eax
	jne	.L398
.L375:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L374:
	.cfi_restore_state
	movq	16(%r11), %rcx
	movl	(%rcx,%rax,4), %eax
	testl	%r8d, %eax
	jne	.L375
	movslq	64(%rdi), %rax
	movq	24(%rdi), %r14
	movq	%rsi, %rbx
	leaq	(%rax,%rax,4), %r12
	salq	$4, %r12
	addq	%r14, %r12
	movq	2088(%r12), %r13
	movq	8(%r13), %rax
	cmpq	$64, %rax
	je	.L399
	addq	$1, %rax
	movq	%rsi, %xmm0
	movq	%rdx, %xmm1
	movq	%rax, 8(%r13)
	punpcklqdq	%xmm1, %xmm0
	salq	$4, %rax
	movups	%xmm0, 0(%r13,%rax)
	jmp	.L375
	.p2align 4,,10
	.p2align 3
.L397:
	movslq	16(%rdi), %rax
	movq	8(%rdi), %r14
	leaq	(%rax,%rax,4), %rbx
	salq	$4, %rbx
	addq	%r14, %rbx
	movq	(%rbx), %r13
	movq	8(%r13), %r12
	cmpq	$64, %r12
	je	.L400
	leaq	1(%r12), %rax
	movq	%rax, 8(%r13)
	movq	%rdx, 16(%r13,%r12,8)
.L378:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L400:
	.cfi_restore_state
	leaq	640(%r14), %r15
	movq	%rdx, -56(%rbp)
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	680(%r14), %rax
	movq	%r15, %rdi
	movq	%rax, 0(%r13)
	movq	%r13, 680(%r14)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movq	%r12, %rcx
	movq	-56(%rbp), %rdx
	movq	$0, 8(%rax)
	movq	%rax, %rsi
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rsi, (%rbx)
	movq	8(%rsi), %rax
	cmpq	$64, %rax
	je	.L378
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rsi)
	movq	%rdx, 16(%rsi,%rax,8)
	jmp	.L378
.L399:
	leaq	2728(%r14), %r15
	movq	%rdx, -56(%rbp)
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	2768(%r14), %rax
	movq	%r15, %rdi
	movq	%rax, 0(%r13)
	movq	%r13, 2768(%r14)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$1040, %edi
	call	_Znwm@PLT
	movl	$128, %ecx
	movq	-56(%rbp), %rdx
	movq	$0, 8(%rax)
	movq	%rax, %rsi
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rsi, 2088(%r12)
	movq	8(%rsi), %rax
	cmpq	$64, %rax
	je	.L375
	addq	$1, %rax
	movq	%rbx, %xmm0
	movq	%rdx, %xmm2
	movq	%rax, 8(%rsi)
	punpcklqdq	%xmm2, %xmm0
	salq	$4, %rax
	movups	%xmm0, (%rsi,%rax)
	jmp	.L375
	.cfi_endproc
.LFE22269:
	.size	_ZN2v88internal24ConcurrentMarkingVisitor16ProcessEphemeronENS0_10HeapObjectES2_, .-_ZN2v88internal24ConcurrentMarkingVisitor16ProcessEphemeronENS0_10HeapObjectES2_
	.section	.text._ZN2v88internal24ConcurrentMarkingVisitor4CastINS0_10ConsStringEEET_NS0_10HeapObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24ConcurrentMarkingVisitor4CastINS0_10ConsStringEEET_NS0_10HeapObjectE
	.type	_ZN2v88internal24ConcurrentMarkingVisitor4CastINS0_10ConsStringEEET_NS0_10HeapObjectE, @function
_ZN2v88internal24ConcurrentMarkingVisitor4CastINS0_10ConsStringEEET_NS0_10HeapObjectE:
.LFB22288:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE22288:
	.size	_ZN2v88internal24ConcurrentMarkingVisitor4CastINS0_10ConsStringEEET_NS0_10HeapObjectE, .-_ZN2v88internal24ConcurrentMarkingVisitor4CastINS0_10ConsStringEEET_NS0_10HeapObjectE
	.section	.text._ZN2v88internal24ConcurrentMarkingVisitor4CastINS0_12SlicedStringEEET_NS0_10HeapObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24ConcurrentMarkingVisitor4CastINS0_12SlicedStringEEET_NS0_10HeapObjectE
	.type	_ZN2v88internal24ConcurrentMarkingVisitor4CastINS0_12SlicedStringEEET_NS0_10HeapObjectE, @function
_ZN2v88internal24ConcurrentMarkingVisitor4CastINS0_12SlicedStringEEET_NS0_10HeapObjectE:
.LFB22289:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE22289:
	.size	_ZN2v88internal24ConcurrentMarkingVisitor4CastINS0_12SlicedStringEEET_NS0_10HeapObjectE, .-_ZN2v88internal24ConcurrentMarkingVisitor4CastINS0_12SlicedStringEEET_NS0_10HeapObjectE
	.section	.text._ZN2v88internal24ConcurrentMarkingVisitor4CastINS0_10ThinStringEEET_NS0_10HeapObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24ConcurrentMarkingVisitor4CastINS0_10ThinStringEEET_NS0_10HeapObjectE
	.type	_ZN2v88internal24ConcurrentMarkingVisitor4CastINS0_10ThinStringEEET_NS0_10HeapObjectE, @function
_ZN2v88internal24ConcurrentMarkingVisitor4CastINS0_10ThinStringEEET_NS0_10HeapObjectE:
.LFB22290:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE22290:
	.size	_ZN2v88internal24ConcurrentMarkingVisitor4CastINS0_10ThinStringEEET_NS0_10HeapObjectE, .-_ZN2v88internal24ConcurrentMarkingVisitor4CastINS0_10ThinStringEEET_NS0_10HeapObjectE
	.section	.text._ZN2v88internal24ConcurrentMarkingVisitor4CastINS0_16SeqOneByteStringEEET_NS0_10HeapObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24ConcurrentMarkingVisitor4CastINS0_16SeqOneByteStringEEET_NS0_10HeapObjectE
	.type	_ZN2v88internal24ConcurrentMarkingVisitor4CastINS0_16SeqOneByteStringEEET_NS0_10HeapObjectE, @function
_ZN2v88internal24ConcurrentMarkingVisitor4CastINS0_16SeqOneByteStringEEET_NS0_10HeapObjectE:
.LFB22291:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE22291:
	.size	_ZN2v88internal24ConcurrentMarkingVisitor4CastINS0_16SeqOneByteStringEEET_NS0_10HeapObjectE, .-_ZN2v88internal24ConcurrentMarkingVisitor4CastINS0_16SeqOneByteStringEEET_NS0_10HeapObjectE
	.section	.text._ZN2v88internal24ConcurrentMarkingVisitor4CastINS0_16SeqTwoByteStringEEET_NS0_10HeapObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24ConcurrentMarkingVisitor4CastINS0_16SeqTwoByteStringEEET_NS0_10HeapObjectE
	.type	_ZN2v88internal24ConcurrentMarkingVisitor4CastINS0_16SeqTwoByteStringEEET_NS0_10HeapObjectE, @function
_ZN2v88internal24ConcurrentMarkingVisitor4CastINS0_16SeqTwoByteStringEEET_NS0_10HeapObjectE:
.LFB22292:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE22292:
	.size	_ZN2v88internal24ConcurrentMarkingVisitor4CastINS0_16SeqTwoByteStringEEET_NS0_10HeapObjectE, .-_ZN2v88internal24ConcurrentMarkingVisitor4CastINS0_16SeqTwoByteStringEEET_NS0_10HeapObjectE
	.section	.text._ZN2v88internal24ConcurrentMarkingVisitor4CastINS0_10FixedArrayEEET_NS0_10HeapObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24ConcurrentMarkingVisitor4CastINS0_10FixedArrayEEET_NS0_10HeapObjectE
	.type	_ZN2v88internal24ConcurrentMarkingVisitor4CastINS0_10FixedArrayEEET_NS0_10HeapObjectE, @function
_ZN2v88internal24ConcurrentMarkingVisitor4CastINS0_10FixedArrayEEET_NS0_10HeapObjectE:
.LFB22293:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE22293:
	.size	_ZN2v88internal24ConcurrentMarkingVisitor4CastINS0_10FixedArrayEEET_NS0_10HeapObjectE, .-_ZN2v88internal24ConcurrentMarkingVisitor4CastINS0_10FixedArrayEEET_NS0_10HeapObjectE
	.section	.text._ZN2v88internal17ConcurrentMarkingC2EPNS0_4HeapEPNS0_8WorklistINS0_10HeapObjectELi64EEES7_PNS0_11WeakObjectsEPNS4_IS5_Li16EEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ConcurrentMarkingC2EPNS0_4HeapEPNS0_8WorklistINS0_10HeapObjectELi64EEES7_PNS0_11WeakObjectsEPNS4_IS5_Li16EEE
	.type	_ZN2v88internal17ConcurrentMarkingC2EPNS0_4HeapEPNS0_8WorklistINS0_10HeapObjectELi64EEES7_PNS0_11WeakObjectsEPNS4_IS5_Li16EEE, @function
_ZN2v88internal17ConcurrentMarkingC2EPNS0_4HeapEPNS0_8WorklistINS0_10HeapObjectELi64EEES7_PNS0_11WeakObjectsEPNS4_IS5_Li16EEE:
.LFB22334:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %xmm0
	movq	%rdx, %xmm1
	leaq	96(%rdi), %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%r8, -24(%rbp)
	movups	%xmm0, (%rdi)
	movq	%rcx, %xmm0
	movq	%rax, 48(%rdi)
	leaq	240(%rdi), %rax
	movhps	-24(%rbp), %xmm0
	movq	%rax, 192(%rdi)
	leaq	384(%rdi), %rax
	movups	%xmm0, 16(%rdi)
	movss	.LC3(%rip), %xmm0
	movq	%rax, 336(%rdi)
	leaq	528(%rdi), %rax
	movss	%xmm0, 80(%rdi)
	movss	%xmm0, 224(%rdi)
	movss	%xmm0, 368(%rdi)
	movq	%r9, 32(%rdi)
	movq	$1, 56(%rdi)
	movq	$0, 64(%rdi)
	movq	$0, 72(%rdi)
	movq	$0, 88(%rdi)
	movq	$0, 96(%rdi)
	movq	$0, 104(%rdi)
	movq	$1, 200(%rdi)
	movq	$0, 208(%rdi)
	movq	$0, 216(%rdi)
	movq	$0, 232(%rdi)
	movq	$0, 240(%rdi)
	movq	$0, 248(%rdi)
	movq	$1, 344(%rdi)
	movq	$0, 352(%rdi)
	movq	$0, 360(%rdi)
	movq	$0, 376(%rdi)
	movq	$0, 384(%rdi)
	movq	$0, 392(%rdi)
	movq	%rax, 480(%rdi)
	leaq	672(%rdi), %rax
	movq	%rax, 624(%rdi)
	leaq	816(%rdi), %rax
	movq	%rax, 768(%rdi)
	leaq	960(%rdi), %rax
	movq	%rax, 912(%rdi)
	leaq	1104(%rdi), %rax
	leaq	1208(%rdi), %rdi
	movss	%xmm0, -696(%rdi)
	movss	%xmm0, -552(%rdi)
	movss	%xmm0, -408(%rdi)
	movss	%xmm0, -264(%rdi)
	movq	$1, -720(%rdi)
	movq	$0, -712(%rdi)
	movq	$0, -704(%rdi)
	movq	$0, -688(%rdi)
	movq	$0, -680(%rdi)
	movq	$0, -672(%rdi)
	movq	$1, -576(%rdi)
	movq	$0, -568(%rdi)
	movq	$0, -560(%rdi)
	movq	$0, -544(%rdi)
	movq	$0, -536(%rdi)
	movq	$0, -528(%rdi)
	movq	$1, -432(%rdi)
	movq	$0, -424(%rdi)
	movq	$0, -416(%rdi)
	movq	$0, -400(%rdi)
	movq	$0, -392(%rdi)
	movq	$0, -384(%rdi)
	movq	$1, -288(%rdi)
	movq	$0, -280(%rdi)
	movq	$0, -272(%rdi)
	movq	$0, -256(%rdi)
	movq	$0, -248(%rdi)
	movq	$0, -240(%rdi)
	movq	%rax, -152(%rdi)
	movq	$1, -144(%rdi)
	movq	$0, -136(%rdi)
	movq	$0, -128(%rdi)
	movq	$0, -112(%rdi)
	movq	$0, -104(%rdi)
	movq	$0, -96(%rdi)
	movq	$0, -16(%rdi)
	movb	$0, -8(%rdi)
	movss	%xmm0, -120(%rdi)
	call	_ZN2v84base5MutexC1Ev@PLT
	leaq	1248(%rbx), %rdi
	call	_ZN2v84base17ConditionVariableC1Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$0, 1296(%rbx)
	movq	$0, 1300(%rbx)
	movl	$0, 1376(%rbx)
	movups	%xmm0, 1312(%rbx)
	movups	%xmm0, 1328(%rbx)
	movups	%xmm0, 1344(%rbx)
	movups	%xmm0, 1360(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22334:
	.size	_ZN2v88internal17ConcurrentMarkingC2EPNS0_4HeapEPNS0_8WorklistINS0_10HeapObjectELi64EEES7_PNS0_11WeakObjectsEPNS4_IS5_Li16EEE, .-_ZN2v88internal17ConcurrentMarkingC2EPNS0_4HeapEPNS0_8WorklistINS0_10HeapObjectELi64EEES7_PNS0_11WeakObjectsEPNS4_IS5_Li16EEE
	.globl	_ZN2v88internal17ConcurrentMarkingC1EPNS0_4HeapEPNS0_8WorklistINS0_10HeapObjectELi64EEES7_PNS0_11WeakObjectsEPNS4_IS5_Li16EEE
	.set	_ZN2v88internal17ConcurrentMarkingC1EPNS0_4HeapEPNS0_8WorklistINS0_10HeapObjectELi64EEES7_PNS0_11WeakObjectsEPNS4_IS5_Li16EEE,_ZN2v88internal17ConcurrentMarkingC2EPNS0_4HeapEPNS0_8WorklistINS0_10HeapObjectELi64EEES7_PNS0_11WeakObjectsEPNS4_IS5_Li16EEE
	.section	.rodata._ZN2v88internal17ConcurrentMarking13ScheduleTasksEv.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"Scheduling concurrent marking task %d\n"
	.section	.text._ZN2v88internal17ConcurrentMarking13ScheduleTasksEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ConcurrentMarking13ScheduleTasksEv
	.type	_ZN2v88internal17ConcurrentMarking13ScheduleTasksEv, @function
_ZN2v88internal17ConcurrentMarking13ScheduleTasksEv:
.LFB22340:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	1208(%rdi), %rax
	movq	%rax, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movl	1376(%r14), %eax
	testl	%eax, %eax
	je	.L429
	jle	.L418
.L414:
	leaq	-64(%rbp), %rax
	leaq	184(%r14), %r13
	movl	$1, %r15d
	movq	%rax, -80(%rbp)
	jmp	.L419
	.p2align 4,,10
	.p2align 3
.L416:
	movl	$64, %edi
	movb	$0, 0(%r13)
	mfence
	movq	(%r14), %rsi
	movq	2016(%rsi), %rax
	movq	%rsi, -72(%rbp)
	movl	9996(%rax), %eax
	movl	%eax, 72(%r13)
	movzbl	2764(%rsi), %eax
	movb	%al, 76(%r13)
	movb	$1, 1300(%r14,%r15)
	addl	$1, 1296(%r14)
	call	_Znwm@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rax, %rbx
	subq	$37592, %rsi
	addq	$32, %rbx
	call	_ZN2v88internal14CancelableTaskC2EPNS0_7IsolateE@PLT
	leaq	16+_ZTVN2v88internal17ConcurrentMarking4TaskE(%rip), %rax
	movq	%r14, 8(%rbx)
	movq	%rax, -32(%rbx)
	addq	$48, %rax
	movq	%rax, (%rbx)
	movq	-8(%rbx), %rax
	movq	%r13, 16(%rbx)
	movl	%r12d, 24(%rbx)
	movq	%rax, 1312(%r14,%r15,8)
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	-80(%rbp), %rsi
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	%rbx, -64(%rbp)
	call	*56(%rax)
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L415
	movq	(%rdi), %rax
	call	*8(%rax)
.L415:
	addq	$1, %r15
	addq	$144, %r13
	cmpl	%r15d, 1376(%r14)
	jl	.L418
.L419:
	cmpb	$0, 1300(%r14,%r15)
	movl	%r15d, %r12d
	jne	.L415
	cmpb	$0, _ZN2v88internal29FLAG_trace_concurrent_markingE(%rip)
	je	.L416
	movq	(%r14), %rax
	movl	%r15d, %edx
	leaq	.LC4(%rip), %rsi
	leaq	-37592(%rax), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal7Isolate18PrintWithTimestampEPKcz@PLT
	jmp	.L416
	.p2align 4,,10
	.p2align 3
.L418:
	movq	-88(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L430
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L429:
	.cfi_restore_state
	movzbl	_ZGVZN2v88internal17ConcurrentMarking13ScheduleTasksEvE9num_cores(%rip), %eax
	testb	%al, %al
	je	.L431
.L412:
	movl	_ZZN2v88internal17ConcurrentMarking13ScheduleTasksEvE9num_cores(%rip), %eax
	movl	$7, %edx
	subl	$1, %eax
	cmpl	$7, %eax
	cmovg	%edx, %eax
	movl	$1, %edx
	testl	%eax, %eax
	cmovle	%edx, %eax
	movl	%eax, 1376(%r14)
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L431:
	leaq	_ZGVZN2v88internal17ConcurrentMarking13ScheduleTasksEvE9num_cores(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L412
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*40(%rax)
	leaq	_ZGVZN2v88internal17ConcurrentMarking13ScheduleTasksEvE9num_cores(%rip), %rdi
	addl	$1, %eax
	movl	%eax, _ZZN2v88internal17ConcurrentMarking13ScheduleTasksEvE9num_cores(%rip)
	call	__cxa_guard_release@PLT
	jmp	.L412
.L430:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22340:
	.size	_ZN2v88internal17ConcurrentMarking13ScheduleTasksEv, .-_ZN2v88internal17ConcurrentMarking13ScheduleTasksEv
	.section	.text._ZN2v88internal17ConcurrentMarking23RescheduleTasksIfNeededEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ConcurrentMarking23RescheduleTasksIfNeededEv
	.type	_ZN2v88internal17ConcurrentMarking23RescheduleTasksIfNeededEv, @function
_ZN2v88internal17ConcurrentMarking23RescheduleTasksIfNeededEv:
.LFB22344:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	cmpl	$4, 392(%rax)
	je	.L487
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	1208(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movl	1296(%r12), %eax
	movq	%r13, %rdi
	testl	%eax, %eax
	jg	.L490
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	8(%r12), %rax
	movq	680(%rax), %rax
	testq	%rax, %rax
	je	.L491
.L436:
	movq	%r12, %rdi
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal17ConcurrentMarking13ScheduleTasksEv
	.p2align 4,,10
	.p2align 3
.L490:
	.cfi_restore_state
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L491:
	.cfi_restore_state
	movq	24(%r12), %rax
	movl	2080(%rax), %edx
	testl	%edx, %edx
	jle	.L437
	movq	1400(%rax), %rcx
	cmpq	$0, 8(%rcx)
	jne	.L436
	movq	1392(%rax), %rcx
	cmpq	$0, 8(%rcx)
	jne	.L436
	cmpl	$1, %edx
	je	.L437
	movq	1480(%rax), %rcx
	cmpq	$0, 8(%rcx)
	jne	.L436
	movq	1472(%rax), %rcx
	cmpq	$0, 8(%rcx)
	jne	.L436
	cmpl	$2, %edx
	je	.L437
	movq	1560(%rax), %rcx
	cmpq	$0, 8(%rcx)
	jne	.L436
	movq	1552(%rax), %rcx
	cmpq	$0, 8(%rcx)
	jne	.L436
	cmpl	$3, %edx
	je	.L437
	movq	1640(%rax), %rcx
	cmpq	$0, 8(%rcx)
	jne	.L436
	movq	1632(%rax), %rcx
	cmpq	$0, 8(%rcx)
	jne	.L436
	cmpl	$4, %edx
	je	.L437
	movq	1720(%rax), %rcx
	cmpq	$0, 8(%rcx)
	jne	.L436
	movq	1712(%rax), %rcx
	cmpq	$0, 8(%rcx)
	jne	.L436
	cmpl	$5, %edx
	je	.L437
	movq	1800(%rax), %rcx
	cmpq	$0, 8(%rcx)
	jne	.L436
	movq	1792(%rax), %rcx
	cmpq	$0, 8(%rcx)
	jne	.L436
	cmpl	$6, %edx
	je	.L437
	movq	1880(%rax), %rcx
	cmpq	$0, 8(%rcx)
	jne	.L436
	movq	1872(%rax), %rcx
	cmpq	$0, 8(%rcx)
	jne	.L436
	cmpl	$7, %edx
	je	.L437
	movq	1960(%rax), %rdx
	cmpq	$0, 8(%rdx)
	jne	.L436
	movq	1952(%rax), %rdx
	cmpq	$0, 8(%rdx)
	jne	.L436
.L437:
	movq	2072(%rax), %rdx
	testq	%rdx, %rdx
	jne	.L436
	movl	3472(%rax), %edx
	testl	%edx, %edx
	jle	.L438
	movq	2792(%rax), %rcx
	cmpq	$0, 8(%rcx)
	jne	.L436
	movq	2784(%rax), %rcx
	cmpq	$0, 8(%rcx)
	jne	.L436
	cmpl	$1, %edx
	je	.L438
	movq	2872(%rax), %rcx
	cmpq	$0, 8(%rcx)
	jne	.L436
	movq	2864(%rax), %rcx
	cmpq	$0, 8(%rcx)
	jne	.L436
	cmpl	$2, %edx
	je	.L438
	movq	2952(%rax), %rcx
	cmpq	$0, 8(%rcx)
	jne	.L436
	movq	2944(%rax), %rcx
	cmpq	$0, 8(%rcx)
	jne	.L436
	cmpl	$3, %edx
	je	.L438
	movq	3032(%rax), %rcx
	cmpq	$0, 8(%rcx)
	jne	.L436
	movq	3024(%rax), %rcx
	cmpq	$0, 8(%rcx)
	jne	.L436
	cmpl	$4, %edx
	je	.L438
	movq	3112(%rax), %rcx
	cmpq	$0, 8(%rcx)
	jne	.L436
	movq	3104(%rax), %rcx
	cmpq	$0, 8(%rcx)
	jne	.L436
	cmpl	$5, %edx
	je	.L438
	movq	3192(%rax), %rcx
	cmpq	$0, 8(%rcx)
	jne	.L436
	movq	3184(%rax), %rcx
	cmpq	$0, 8(%rcx)
	jne	.L436
	cmpl	$6, %edx
	je	.L438
	movq	3272(%rax), %rcx
	cmpq	$0, 8(%rcx)
	jne	.L436
	movq	3264(%rax), %rcx
	cmpq	$0, 8(%rcx)
	jne	.L436
	cmpl	$7, %edx
	je	.L438
	movq	3352(%rax), %rdx
	cmpq	$0, 8(%rdx)
	jne	.L436
	movq	3344(%rax), %rdx
	cmpq	$0, 8(%rdx)
	jne	.L436
.L438:
	movq	3464(%rax), %rax
	testq	%rax, %rax
	jne	.L436
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L487:
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE22344:
	.size	_ZN2v88internal17ConcurrentMarking23RescheduleTasksIfNeededEv, .-_ZN2v88internal17ConcurrentMarking23RescheduleTasksIfNeededEv
	.section	.text._ZN2v88internal17ConcurrentMarking4StopENS1_11StopRequestE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ConcurrentMarking4StopENS1_11StopRequestE
	.type	_ZN2v88internal17ConcurrentMarking4StopENS1_11StopRequestE, @function
_ZN2v88internal17ConcurrentMarking4StopENS1_11StopRequestE:
.LFB22345:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	xorl	%r14d, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	leaq	1208(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movl	1296(%rbx), %eax
	testl	%eax, %eax
	je	.L493
	cmpl	$2, %r13d
	je	.L494
	movq	(%rbx), %rdx
	movq	8048(%rdx), %r14
	movl	1376(%rbx), %edx
	testl	%edx, %edx
	jle	.L494
	testl	%r13d, %r13d
	je	.L497
	cmpb	$0, 1301(%rbx)
	jne	.L543
.L498:
	cmpl	$1, %edx
	jle	.L500
	cmpb	$0, 1302(%rbx)
	jne	.L544
.L501:
	cmpl	$2, %edx
	jle	.L500
	cmpb	$0, 1303(%rbx)
	jne	.L545
.L503:
	cmpl	$3, %edx
	jle	.L500
	cmpb	$0, 1304(%rbx)
	jne	.L546
.L505:
	cmpl	$4, %edx
	jle	.L500
	cmpb	$0, 1305(%rbx)
	jne	.L547
.L507:
	cmpl	$5, %edx
	jle	.L500
	cmpb	$0, 1306(%rbx)
	jne	.L548
.L509:
	cmpl	$6, %edx
	jle	.L500
	cmpb	$0, 1307(%rbx)
	jne	.L549
.L500:
	movl	1296(%rbx), %eax
.L494:
	leaq	1248(%rbx), %r13
	testl	%eax, %eax
	jle	.L526
	.p2align 4,,10
	.p2align 3
.L527:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v84base17ConditionVariable4WaitEPNS0_5MutexE@PLT
	movl	1296(%rbx), %eax
	testl	%eax, %eax
	jg	.L527
.L526:
	movl	$1, %r14d
.L493:
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	popq	%rbx
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L543:
	.cfi_restore_state
	movq	1320(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal21CancelableTaskManager8TryAbortEm@PLT
	cmpl	$2, %eax
	jne	.L536
	subl	$1, 1296(%rbx)
	movb	$0, 1301(%rbx)
.L536:
	movl	1376(%rbx), %edx
	jmp	.L498
	.p2align 4,,10
	.p2align 3
.L544:
	movq	1328(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal21CancelableTaskManager8TryAbortEm@PLT
	cmpl	$2, %eax
	je	.L502
.L537:
	movl	1376(%rbx), %edx
	jmp	.L501
	.p2align 4,,10
	.p2align 3
.L497:
	cmpb	$0, 1301(%rbx)
	jne	.L550
.L512:
	cmpl	$1, %edx
	jle	.L500
	cmpb	$0, 1302(%rbx)
	jne	.L551
.L514:
	cmpl	$2, %edx
	jle	.L500
	cmpb	$0, 1303(%rbx)
	jne	.L552
.L516:
	cmpl	$3, %edx
	jle	.L500
	cmpb	$0, 1304(%rbx)
	jne	.L553
.L518:
	cmpl	$4, %edx
	jle	.L500
	cmpb	$0, 1305(%rbx)
	jne	.L554
.L520:
	cmpl	$5, %edx
	jle	.L500
	cmpb	$0, 1306(%rbx)
	jne	.L555
.L522:
	cmpl	$6, %edx
	jle	.L500
	cmpb	$0, 1307(%rbx)
	je	.L500
	movq	1368(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal21CancelableTaskManager8TryAbortEm@PLT
	cmpl	$2, %eax
	je	.L542
	movb	$1, 1048(%rbx)
	mfence
	jmp	.L500
	.p2align 4,,10
	.p2align 3
.L545:
	movq	1336(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal21CancelableTaskManager8TryAbortEm@PLT
	cmpl	$2, %eax
	je	.L504
.L538:
	movl	1376(%rbx), %edx
	jmp	.L503
.L546:
	movq	1344(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal21CancelableTaskManager8TryAbortEm@PLT
	cmpl	$2, %eax
	je	.L506
.L539:
	movl	1376(%rbx), %edx
	jmp	.L505
.L549:
	movq	1368(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal21CancelableTaskManager8TryAbortEm@PLT
	cmpl	$2, %eax
	jne	.L500
.L542:
	subl	$1, 1296(%rbx)
	movb	$0, 1307(%rbx)
	jmp	.L500
.L502:
	subl	$1, 1296(%rbx)
	movb	$0, 1302(%rbx)
	jmp	.L537
.L554:
	movq	1352(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal21CancelableTaskManager8TryAbortEm@PLT
	cmpl	$2, %eax
	je	.L521
	movb	$1, 760(%rbx)
	mfence
	movl	1376(%rbx), %edx
	jmp	.L520
.L504:
	subl	$1, 1296(%rbx)
	movb	$0, 1303(%rbx)
	jmp	.L538
.L555:
	movq	1360(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal21CancelableTaskManager8TryAbortEm@PLT
	cmpl	$2, %eax
	je	.L523
	movb	$1, 904(%rbx)
	mfence
	movl	1376(%rbx), %edx
	jmp	.L522
.L506:
	subl	$1, 1296(%rbx)
	movb	$0, 1304(%rbx)
	jmp	.L539
.L547:
	movq	1352(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal21CancelableTaskManager8TryAbortEm@PLT
	cmpl	$2, %eax
	jne	.L540
	subl	$1, 1296(%rbx)
	movb	$0, 1305(%rbx)
.L540:
	movl	1376(%rbx), %edx
	jmp	.L507
.L548:
	movq	1360(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal21CancelableTaskManager8TryAbortEm@PLT
	cmpl	$2, %eax
	jne	.L541
	subl	$1, 1296(%rbx)
	movb	$0, 1306(%rbx)
.L541:
	movl	1376(%rbx), %edx
	jmp	.L509
.L550:
	movq	1320(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal21CancelableTaskManager8TryAbortEm@PLT
	cmpl	$2, %eax
	je	.L513
	movb	$1, 184(%rbx)
	mfence
	movl	1376(%rbx), %edx
	jmp	.L512
.L551:
	movq	1328(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal21CancelableTaskManager8TryAbortEm@PLT
	cmpl	$2, %eax
	je	.L515
	movb	$1, 328(%rbx)
	mfence
	movl	1376(%rbx), %edx
	jmp	.L514
.L552:
	movq	1336(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal21CancelableTaskManager8TryAbortEm@PLT
	cmpl	$2, %eax
	je	.L517
	movb	$1, 472(%rbx)
	mfence
	movl	1376(%rbx), %edx
	jmp	.L516
.L553:
	movq	1344(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal21CancelableTaskManager8TryAbortEm@PLT
	cmpl	$2, %eax
	je	.L519
	movb	$1, 616(%rbx)
	mfence
	movl	1376(%rbx), %edx
	jmp	.L518
.L513:
	subl	$1, 1296(%rbx)
	movl	1376(%rbx), %edx
	movb	$0, 1301(%rbx)
	jmp	.L512
.L515:
	subl	$1, 1296(%rbx)
	movl	1376(%rbx), %edx
	movb	$0, 1302(%rbx)
	jmp	.L514
.L517:
	subl	$1, 1296(%rbx)
	movl	1376(%rbx), %edx
	movb	$0, 1303(%rbx)
	jmp	.L516
.L519:
	subl	$1, 1296(%rbx)
	movl	1376(%rbx), %edx
	movb	$0, 1304(%rbx)
	jmp	.L518
.L521:
	subl	$1, 1296(%rbx)
	movl	1376(%rbx), %edx
	movb	$0, 1305(%rbx)
	jmp	.L520
.L523:
	subl	$1, 1296(%rbx)
	movl	1376(%rbx), %edx
	movb	$0, 1306(%rbx)
	jmp	.L522
	.cfi_endproc
.LFE22345:
	.size	_ZN2v88internal17ConcurrentMarking4StopENS1_11StopRequestE, .-_ZN2v88internal17ConcurrentMarking4StopENS1_11StopRequestE
	.section	.text._ZN2v88internal17ConcurrentMarking9IsStoppedEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ConcurrentMarking9IsStoppedEv
	.type	_ZN2v88internal17ConcurrentMarking9IsStoppedEv, @function
_ZN2v88internal17ConcurrentMarking9IsStoppedEv:
.LFB22346:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	$1, %r12d
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	cmpb	$0, _ZN2v88internal23FLAG_concurrent_markingE(%rip)
	je	.L556
	leaq	1208(%rdi), %r13
	movq	%rdi, %rbx
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movl	1296(%rbx), %eax
	movq	%r13, %rdi
	testl	%eax, %eax
	sete	%r12b
	call	_ZN2v84base5Mutex6UnlockEv@PLT
.L556:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22346:
	.size	_ZN2v88internal17ConcurrentMarking9IsStoppedEv, .-_ZN2v88internal17ConcurrentMarking9IsStoppedEv
	.section	.text._ZN2v88internal17ConcurrentMarking20FlushMemoryChunkDataEPNS0_26MajorNonAtomicMarkingStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ConcurrentMarking20FlushMemoryChunkDataEPNS0_26MajorNonAtomicMarkingStateE
	.type	_ZN2v88internal17ConcurrentMarking20FlushMemoryChunkDataEPNS0_26MajorNonAtomicMarkingStateE, @function
_ZN2v88internal17ConcurrentMarking20FlushMemoryChunkDataEPNS0_26MajorNonAtomicMarkingStateE:
.LFB22347:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	1376(%rdi), %eax
	testl	%eax, %eax
	jle	.L562
	leaq	192(%rdi), %r13
	movl	$1, %r14d
	.p2align 4,,10
	.p2align 3
.L572:
	movq	16(%r13), %rbx
	testq	%rbx, %rbx
	jne	.L568
	jmp	.L563
	.p2align 4,,10
	.p2align 3
.L567:
	movq	%r12, %rsi
	call	_ZN2v88internal10TypedSlots5MergeEPS1_@PLT
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
.L565:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L594
.L568:
	movq	16(%rbx), %rax
	movq	8(%rbx), %r8
	testq	%rax, %rax
	je	.L564
	addq	%rax, 96(%r8)
.L564:
	movq	24(%rbx), %r12
	testq	%r12, %r12
	je	.L565
	movq	$0, 24(%rbx)
	movq	128(%r8), %rdi
	testq	%rdi, %rdi
	jne	.L567
	movq	%r8, %rdi
	call	_ZN2v88internal11MemoryChunk20AllocateTypedSlotSetILNS0_17RememberedSetTypeE1EEEPNS0_12TypedSlotSetEv@PLT
	movq	%rax, %rdi
	jmp	.L567
	.p2align 4,,10
	.p2align 3
.L594:
	movq	16(%r13), %r12
	testq	%r12, %r12
	jne	.L571
	jmp	.L563
	.p2align 4,,10
	.p2align 3
.L595:
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L563
.L570:
	movq	%rbx, %r12
.L571:
	movq	24(%r12), %rdi
	movq	(%r12), %rbx
	testq	%rdi, %rdi
	jne	.L595
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L570
.L563:
	movq	8(%r13), %rax
	movq	0(%r13), %rdi
	xorl	%esi, %esi
	addl	$1, %r14d
	addq	$144, %r13
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	$0, -120(%r13)
	movq	$0, -128(%r13)
	movq	$0, -88(%r13)
	cmpl	%r14d, 1376(%r15)
	jge	.L572
.L562:
	movq	$0, 1192(%r15)
	mfence
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22347:
	.size	_ZN2v88internal17ConcurrentMarking20FlushMemoryChunkDataEPNS0_26MajorNonAtomicMarkingStateE, .-_ZN2v88internal17ConcurrentMarking20FlushMemoryChunkDataEPNS0_26MajorNonAtomicMarkingStateE
	.section	.text._ZN2v88internal17ConcurrentMarking20ClearMemoryChunkDataEPNS0_11MemoryChunkE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ConcurrentMarking20ClearMemoryChunkDataEPNS0_11MemoryChunkE
	.type	_ZN2v88internal17ConcurrentMarking20ClearMemoryChunkDataEPNS0_11MemoryChunkE, @function
_ZN2v88internal17ConcurrentMarking20ClearMemoryChunkDataEPNS0_11MemoryChunkE:
.LFB22348:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%r15, %r12
	pushq	%rbx
	shrq	$18, %r12
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	1376(%rdi), %esi
	testl	%esi, %esi
	jle	.L596
	movq	%rdi, %r14
	leaq	192(%rdi), %rbx
	movl	$1, %r13d
	.p2align 4,,10
	.p2align 3
.L603:
	movq	8(%rbx), %r8
	movq	%r12, %rax
	xorl	%edx, %edx
	divq	%r8
	movq	(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L598
	movq	(%rax), %rcx
	movq	32(%rcx), %rdi
	jmp	.L601
	.p2align 4,,10
	.p2align 3
.L599:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L598
	movq	32(%rcx), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%r8
	cmpq	%rdx, %r9
	jne	.L598
.L601:
	cmpq	%rdi, %r12
	jne	.L599
	cmpq	%r15, 8(%rcx)
	jne	.L599
	movq	24(%rcx), %rdi
	movq	$0, 16(%rcx)
	movq	$0, 24(%rcx)
	testq	%rdi, %rdi
	je	.L615
	movq	(%rdi), %rax
	call	*8(%rax)
.L615:
	movl	1376(%r14), %esi
.L598:
	addl	$1, %r13d
	addq	$144, %rbx
	cmpl	%esi, %r13d
	jle	.L603
.L596:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22348:
	.size	_ZN2v88internal17ConcurrentMarking20ClearMemoryChunkDataEPNS0_11MemoryChunkE, .-_ZN2v88internal17ConcurrentMarking20ClearMemoryChunkDataEPNS0_11MemoryChunkE
	.section	.text._ZN2v88internal17ConcurrentMarking16TotalMarkedBytesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ConcurrentMarking16TotalMarkedBytesEv
	.type	_ZN2v88internal17ConcurrentMarking16TotalMarkedBytesEv, @function
_ZN2v88internal17ConcurrentMarking16TotalMarkedBytesEv:
.LFB22349:
	.cfi_startproc
	endbr64
	movl	1376(%rdi), %eax
	testl	%eax, %eax
	jle	.L619
	leaq	248(%rdi), %rcx
	movl	$1, %edx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L618:
	movq	(%rcx), %rsi
	addl	$1, %edx
	addq	$144, %rcx
	addq	%rsi, %rax
	cmpl	%edx, 1376(%rdi)
	jge	.L618
	movq	1192(%rdi), %rdx
	addq	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L619:
	xorl	%eax, %eax
	movq	1192(%rdi), %rdx
	addq	%rdx, %rax
	ret
	.cfi_endproc
.LFE22349:
	.size	_ZN2v88internal17ConcurrentMarking16TotalMarkedBytesEv, .-_ZN2v88internal17ConcurrentMarking16TotalMarkedBytesEv
	.section	.text._ZN2v88internal17ConcurrentMarking10PauseScopeC2EPS1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ConcurrentMarking10PauseScopeC2EPS1_
	.type	_ZN2v88internal17ConcurrentMarking10PauseScopeC2EPS1_, @function
_ZN2v88internal17ConcurrentMarking10PauseScopeC2EPS1_:
.LFB22351:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movzbl	_ZN2v88internal23FLAG_concurrent_markingE(%rip), %eax
	movq	%rsi, (%rdi)
	testb	%al, %al
	jne	.L627
	movb	%al, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L627:
	.cfi_restore_state
	movq	%rsi, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal17ConcurrentMarking4StopENS1_11StopRequestE
	movb	%al, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22351:
	.size	_ZN2v88internal17ConcurrentMarking10PauseScopeC2EPS1_, .-_ZN2v88internal17ConcurrentMarking10PauseScopeC2EPS1_
	.globl	_ZN2v88internal17ConcurrentMarking10PauseScopeC1EPS1_
	.set	_ZN2v88internal17ConcurrentMarking10PauseScopeC1EPS1_,_ZN2v88internal17ConcurrentMarking10PauseScopeC2EPS1_
	.section	.text._ZN2v88internal17ConcurrentMarking10PauseScopeD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ConcurrentMarking10PauseScopeD2Ev
	.type	_ZN2v88internal17ConcurrentMarking10PauseScopeD2Ev, @function
_ZN2v88internal17ConcurrentMarking10PauseScopeD2Ev:
.LFB22354:
	.cfi_startproc
	endbr64
	cmpb	$0, 8(%rdi)
	jne	.L686
	ret
	.p2align 4,,10
	.p2align 3
.L686:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	(%rdi), %r12
	movq	(%r12), %rax
	cmpl	$4, 392(%rax)
	je	.L628
	leaq	1208(%r12), %r13
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movl	1296(%r12), %eax
	movq	%r13, %rdi
	testl	%eax, %eax
	jg	.L687
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	8(%r12), %rax
	movq	680(%rax), %rax
	testq	%rax, %rax
	je	.L688
.L632:
	movq	%r12, %rdi
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal17ConcurrentMarking13ScheduleTasksEv
	.p2align 4,,10
	.p2align 3
.L688:
	.cfi_restore_state
	movq	24(%r12), %rax
	movl	2080(%rax), %edx
	testl	%edx, %edx
	jle	.L633
	movq	1400(%rax), %rcx
	cmpq	$0, 8(%rcx)
	jne	.L632
	movq	1392(%rax), %rcx
	cmpq	$0, 8(%rcx)
	jne	.L632
	cmpl	$1, %edx
	je	.L633
	movq	1480(%rax), %rcx
	cmpq	$0, 8(%rcx)
	jne	.L632
	movq	1472(%rax), %rcx
	cmpq	$0, 8(%rcx)
	jne	.L632
	cmpl	$2, %edx
	je	.L633
	movq	1560(%rax), %rcx
	cmpq	$0, 8(%rcx)
	jne	.L632
	movq	1552(%rax), %rcx
	cmpq	$0, 8(%rcx)
	jne	.L632
	cmpl	$3, %edx
	je	.L633
	movq	1640(%rax), %rcx
	cmpq	$0, 8(%rcx)
	jne	.L632
	movq	1632(%rax), %rcx
	cmpq	$0, 8(%rcx)
	jne	.L632
	cmpl	$4, %edx
	je	.L633
	movq	1720(%rax), %rcx
	cmpq	$0, 8(%rcx)
	jne	.L632
	movq	1712(%rax), %rcx
	cmpq	$0, 8(%rcx)
	jne	.L632
	cmpl	$5, %edx
	je	.L633
	movq	1800(%rax), %rcx
	cmpq	$0, 8(%rcx)
	jne	.L632
	movq	1792(%rax), %rcx
	cmpq	$0, 8(%rcx)
	jne	.L632
	cmpl	$6, %edx
	je	.L633
	movq	1880(%rax), %rcx
	cmpq	$0, 8(%rcx)
	jne	.L632
	movq	1872(%rax), %rcx
	cmpq	$0, 8(%rcx)
	jne	.L632
	cmpl	$7, %edx
	je	.L633
	movq	1960(%rax), %rdx
	cmpq	$0, 8(%rdx)
	jne	.L632
	movq	1952(%rax), %rdx
	cmpq	$0, 8(%rdx)
	jne	.L632
.L633:
	movq	2072(%rax), %rdx
	testq	%rdx, %rdx
	jne	.L632
	movl	3472(%rax), %edx
	testl	%edx, %edx
	jle	.L634
	movq	2792(%rax), %rcx
	cmpq	$0, 8(%rcx)
	jne	.L632
	movq	2784(%rax), %rcx
	cmpq	$0, 8(%rcx)
	jne	.L632
	cmpl	$1, %edx
	je	.L634
	movq	2872(%rax), %rcx
	cmpq	$0, 8(%rcx)
	jne	.L632
	movq	2864(%rax), %rcx
	cmpq	$0, 8(%rcx)
	jne	.L632
	cmpl	$2, %edx
	je	.L634
	movq	2952(%rax), %rcx
	cmpq	$0, 8(%rcx)
	jne	.L632
	movq	2944(%rax), %rcx
	cmpq	$0, 8(%rcx)
	jne	.L632
	cmpl	$3, %edx
	je	.L634
	movq	3032(%rax), %rcx
	cmpq	$0, 8(%rcx)
	jne	.L632
	movq	3024(%rax), %rcx
	cmpq	$0, 8(%rcx)
	jne	.L632
	cmpl	$4, %edx
	je	.L634
	movq	3112(%rax), %rcx
	cmpq	$0, 8(%rcx)
	jne	.L632
	movq	3104(%rax), %rcx
	cmpq	$0, 8(%rcx)
	jne	.L632
	cmpl	$5, %edx
	je	.L634
	movq	3192(%rax), %rcx
	cmpq	$0, 8(%rcx)
	jne	.L632
	movq	3184(%rax), %rcx
	cmpq	$0, 8(%rcx)
	jne	.L632
	cmpl	$6, %edx
	je	.L634
	movq	3272(%rax), %rcx
	cmpq	$0, 8(%rcx)
	jne	.L632
	movq	3264(%rax), %rcx
	cmpq	$0, 8(%rcx)
	jne	.L632
	cmpl	$7, %edx
	je	.L634
	movq	3352(%rax), %rdx
	cmpq	$0, 8(%rdx)
	jne	.L632
	movq	3344(%rax), %rdx
	cmpq	$0, 8(%rdx)
	jne	.L632
.L634:
	movq	3464(%rax), %rax
	testq	%rax, %rax
	jne	.L632
.L628:
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L687:
	.cfi_restore_state
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.cfi_endproc
.LFE22354:
	.size	_ZN2v88internal17ConcurrentMarking10PauseScopeD2Ev, .-_ZN2v88internal17ConcurrentMarking10PauseScopeD2Ev
	.globl	_ZN2v88internal17ConcurrentMarking10PauseScopeD1Ev
	.set	_ZN2v88internal17ConcurrentMarking10PauseScopeD1Ev,_ZN2v88internal17ConcurrentMarking10PauseScopeD2Ev
	.section	.text._ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED2Ev,"axG",@progbits,_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED2Ev
	.type	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED2Ev, @function
_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED2Ev:
.LFB23130:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L689
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L689:
	ret
	.cfi_endproc
.LFE23130:
	.size	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED2Ev, .-_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED2Ev
	.weak	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED1Ev
	.set	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED1Ev,_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED2Ev
	.section	.text._ZN2v88internal8WorklistINS0_10HeapObjectELi64EE4PushEiS2_,"axG",@progbits,_ZN2v88internal8WorklistINS0_10HeapObjectELi64EE4PushEiS2_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8WorklistINS0_10HeapObjectELi64EE4PushEiS2_
	.type	_ZN2v88internal8WorklistINS0_10HeapObjectELi64EE4PushEiS2_, @function
_ZN2v88internal8WorklistINS0_10HeapObjectELi64EE4PushEiS2_:
.LFB23204:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	(%rsi,%rsi,4), %rbx
	salq	$4, %rbx
	addq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rbx), %r14
	movq	8(%r14), %r12
	cmpq	$64, %r12
	je	.L695
	leaq	1(%r12), %rax
	movq	%rax, 8(%r14)
	movq	%rdx, 16(%r14,%r12,8)
.L693:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L695:
	.cfi_restore_state
	leaq	640(%rdi), %r15
	movq	%rdi, %r13
	movq	%rdx, -56(%rbp)
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	680(%r13), %rax
	movq	%r15, %rdi
	movq	%rax, (%r14)
	movq	%r14, 680(%r13)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movq	-56(%rbp), %rdx
	movq	%r12, %rcx
	movq	%rax, %rsi
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rsi, (%rbx)
	movq	$1, 8(%rsi)
	movq	%rdx, 16(%rsi)
	jmp	.L693
	.cfi_endproc
.LFE23204:
	.size	_ZN2v88internal8WorklistINS0_10HeapObjectELi64EE4PushEiS2_, .-_ZN2v88internal8WorklistINS0_10HeapObjectELi64EE4PushEiS2_
	.section	.text._ZN2v88internal8WorklistINS0_10HeapObjectELi64EE13FlushToGlobalEi,"axG",@progbits,_ZN2v88internal8WorklistINS0_10HeapObjectELi64EE13FlushToGlobalEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8WorklistINS0_10HeapObjectELi64EE13FlushToGlobalEi
	.type	_ZN2v88internal8WorklistINS0_10HeapObjectELi64EE13FlushToGlobalEi, @function
_ZN2v88internal8WorklistINS0_10HeapObjectELi64EE13FlushToGlobalEi:
.LFB23213:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	leaq	(%rsi,%rsi,4), %rbx
	salq	$4, %rbx
	addq	%rdi, %rbx
	movq	(%rbx), %r13
	cmpq	$0, 8(%r13)
	jne	.L700
	movq	8(%rbx), %r13
	cmpq	$0, 8(%r13)
	jne	.L701
.L696:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L700:
	.cfi_restore_state
	leaq	640(%rdi), %r14
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	680(%r12), %rax
	movq	%r14, %rdi
	movq	%rax, 0(%r13)
	movq	%r13, 680(%r12)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movl	$64, %ecx
	movq	8(%rbx), %r13
	movq	$0, 8(%rax)
	movq	%rax, %rdx
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rdx, (%rbx)
	cmpq	$0, 8(%r13)
	je	.L696
.L701:
	leaq	640(%r12), %r14
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	680(%r12), %rax
	movq	%r14, %rdi
	movq	%rax, 0(%r13)
	movq	%r13, 680(%r12)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movl	$64, %ecx
	movq	$0, 8(%rax)
	movq	%rax, %rdx
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rdx, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23213:
	.size	_ZN2v88internal8WorklistINS0_10HeapObjectELi64EE13FlushToGlobalEi, .-_ZN2v88internal8WorklistINS0_10HeapObjectELi64EE13FlushToGlobalEi
	.section	.text._ZN2v88internal8WorklistINS0_9EphemeronELi64EE3PopEiPS2_,"axG",@progbits,_ZN2v88internal8WorklistINS0_9EphemeronELi64EE3PopEiPS2_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8WorklistINS0_9EphemeronELi64EE3PopEiPS2_
	.type	_ZN2v88internal8WorklistINS0_9EphemeronELi64EE3PopEiPS2_, @function
_ZN2v88internal8WorklistINS0_9EphemeronELi64EE3PopEiPS2_:
.LFB24880:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rsi
	leaq	(%rsi,%rsi,4), %rcx
	salq	$4, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	leaq	(%rdi,%rcx), %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	8(%r12), %rsi
	movq	8(%rsi), %rax
	testq	%rax, %rax
	je	.L719
	leaq	-1(%rax), %rcx
	salq	$4, %rax
	movq	%rcx, 8(%rsi)
	movdqu	(%rsi,%rax), %xmm1
	movl	$1, %eax
	movups	%xmm1, (%rdx)
.L702:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L719:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	%rdi, %rbx
	cmpq	$0, 8(%rax)
	je	.L720
	movq	%rsi, %xmm0
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, (%rdi,%rcx)
	movq	8(%r12), %r13
.L707:
	movq	8(%r13), %rcx
	movl	$1, %eax
	testq	%rcx, %rcx
	je	.L702
	leaq	-1(%rcx), %rsi
	salq	$4, %rcx
	movq	%rsi, 8(%r13)
	movdqu	0(%r13,%rcx), %xmm3
	movups	%xmm3, (%rdx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L720:
	.cfi_restore_state
	movq	%rdx, -40(%rbp)
	movq	680(%rdi), %rcx
	leaq	640(%rdi), %r14
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.L702
	movq	%r14, %rdi
	movb	%al, -41(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	680(%rbx), %r13
	movq	-40(%rbp), %rdx
	testq	%r13, %r13
	jne	.L708
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movzbl	-41(%rbp), %eax
	jmp	.L702
	.p2align 4,,10
	.p2align 3
.L708:
	movq	0(%r13), %rax
	movq	%rdx, -40(%rbp)
	movq	%r14, %rdi
	movq	%rax, 680(%rbx)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	8(%r12), %rdi
	movq	-40(%rbp), %rdx
	testq	%rdi, %rdi
	je	.L709
	movl	$1040, %esi
	call	_ZdlPvm@PLT
	movq	-40(%rbp), %rdx
.L709:
	movq	%r13, 8(%r12)
	jmp	.L707
	.cfi_endproc
.LFE24880:
	.size	_ZN2v88internal8WorklistINS0_9EphemeronELi64EE3PopEiPS2_, .-_ZN2v88internal8WorklistINS0_9EphemeronELi64EE3PopEiPS2_
	.section	.text._ZN2v88internal8WorklistINS0_9EphemeronELi64EE13FlushToGlobalEi,"axG",@progbits,_ZN2v88internal8WorklistINS0_9EphemeronELi64EE13FlushToGlobalEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8WorklistINS0_9EphemeronELi64EE13FlushToGlobalEi
	.type	_ZN2v88internal8WorklistINS0_9EphemeronELi64EE13FlushToGlobalEi, @function
_ZN2v88internal8WorklistINS0_9EphemeronELi64EE13FlushToGlobalEi:
.LFB24885:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	leaq	(%rsi,%rsi,4), %rbx
	salq	$4, %rbx
	addq	%rdi, %rbx
	movq	(%rbx), %r13
	cmpq	$0, 8(%r13)
	jne	.L725
	movq	8(%rbx), %r13
	cmpq	$0, 8(%r13)
	jne	.L726
.L721:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L725:
	.cfi_restore_state
	leaq	640(%rdi), %r14
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	680(%r12), %rax
	movq	%r14, %rdi
	movq	%rax, 0(%r13)
	movq	%r13, 680(%r12)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$1040, %edi
	call	_Znwm@PLT
	movl	$128, %ecx
	movq	8(%rbx), %r13
	movq	$0, 8(%rax)
	movq	%rax, %rdx
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rdx, (%rbx)
	cmpq	$0, 8(%r13)
	je	.L721
.L726:
	leaq	640(%r12), %r14
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	680(%r12), %rax
	movq	%r14, %rdi
	movq	%rax, 0(%r13)
	movq	%r13, 680(%r12)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$1040, %edi
	call	_Znwm@PLT
	movl	$128, %ecx
	movq	$0, 8(%rax)
	movq	%rax, %rdx
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rdx, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24885:
	.size	_ZN2v88internal8WorklistINS0_9EphemeronELi64EE13FlushToGlobalEi, .-_ZN2v88internal8WorklistINS0_9EphemeronELi64EE13FlushToGlobalEi
	.section	.text._ZNSt8__detail9_Map_baseIPN2v88internal11MemoryChunkESt4pairIKS4_NS2_15MemoryChunkDataEESaIS8_ENS_10_Select1stESt8equal_toIS4_ENS3_6HasherENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS6_,"axG",@progbits,_ZNSt8__detail9_Map_baseIPN2v88internal11MemoryChunkESt4pairIKS4_NS2_15MemoryChunkDataEESaIS8_ENS_10_Select1stESt8equal_toIS4_ENS3_6HasherENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS6_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8__detail9_Map_baseIPN2v88internal11MemoryChunkESt4pairIKS4_NS2_15MemoryChunkDataEESaIS8_ENS_10_Select1stESt8equal_toIS4_ENS3_6HasherENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS6_
	.type	_ZNSt8__detail9_Map_baseIPN2v88internal11MemoryChunkESt4pairIKS4_NS2_15MemoryChunkDataEESaIS8_ENS_10_Select1stESt8equal_toIS4_ENS3_6HasherENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS6_, @function
_ZNSt8__detail9_Map_baseIPN2v88internal11MemoryChunkESt4pairIKS4_NS2_15MemoryChunkDataEESaIS8_ENS_10_Select1stESt8equal_toIS4_ENS3_6HasherENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS6_:
.LFB26349:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %r9
	movq	8(%rdi), %rdi
	movq	%r9, %r13
	shrq	$18, %r13
	movq	%r13, %rax
	divq	%rdi
	movq	(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	leaq	0(,%rdx,8), %r15
	testq	%rax, %rax
	je	.L728
	movq	(%rax), %rcx
	movq	%rdx, %rsi
	movq	32(%rcx), %r8
	jmp	.L731
	.p2align 4,,10
	.p2align 3
.L729:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L728
	movq	32(%rcx), %r8
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	%rdi
	cmpq	%rdx, %rsi
	jne	.L728
.L731:
	cmpq	%r8, %r13
	jne	.L729
	cmpq	8(%rcx), %r9
	jne	.L729
	addq	$24, %rsp
	leaq	16(%rcx), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L728:
	.cfi_restore_state
	movl	$40, %edi
	call	_Znwm@PLT
	movq	24(%r12), %rdx
	movq	8(%r12), %rsi
	leaq	32(%r12), %rdi
	movq	$0, (%rax)
	movq	%rax, %rbx
	movq	(%r14), %rax
	movl	$1, %ecx
	movq	$0, 16(%rbx)
	movq	%rax, 8(%rbx)
	movq	$0, 24(%rbx)
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %r14
	testb	%al, %al
	jne	.L732
	movq	(%r12), %r8
	movq	%r13, 32(%rbx)
	leaq	(%r8,%r15), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L742
.L766:
	movq	(%rdx), %rdx
	movq	%rdx, (%rbx)
	movq	(%rax), %rax
	movq	%rbx, (%rax)
.L743:
	addq	$1, 24(%r12)
	addq	$24, %rsp
	leaq	16(%rbx), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L732:
	.cfi_restore_state
	cmpq	$1, %rdx
	je	.L764
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L765
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%r12), %r10
	movq	%rax, %r8
.L735:
	movq	16(%r12), %rsi
	movq	$0, 16(%r12)
	testq	%rsi, %rsi
	je	.L737
	xorl	%edi, %edi
	leaq	16(%r12), %r9
	jmp	.L738
	.p2align 4,,10
	.p2align 3
.L739:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L740:
	testq	%rsi, %rsi
	je	.L737
.L738:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	32(%rcx), %rax
	divq	%r14
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L739
	movq	16(%r12), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%r12)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L746
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L738
	.p2align 4,,10
	.p2align 3
.L737:
	movq	(%r12), %rdi
	cmpq	%r10, %rdi
	je	.L741
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L741:
	movq	%r13, %rax
	xorl	%edx, %edx
	movq	%r14, 8(%r12)
	divq	%r14
	movq	%r8, (%r12)
	movq	%r13, 32(%rbx)
	leaq	0(,%rdx,8), %r15
	leaq	(%r8,%r15), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	jne	.L766
.L742:
	movq	16(%r12), %rdx
	movq	%rbx, 16(%r12)
	movq	%rdx, (%rbx)
	testq	%rdx, %rdx
	je	.L744
	movq	32(%rdx), %rax
	xorl	%edx, %edx
	divq	8(%r12)
	movq	%rbx, (%r8,%rdx,8)
	movq	(%r12), %rax
	addq	%r15, %rax
.L744:
	leaq	16(%r12), %rdx
	movq	%rdx, (%rax)
	jmp	.L743
	.p2align 4,,10
	.p2align 3
.L746:
	movq	%rdx, %rdi
	jmp	.L740
	.p2align 4,,10
	.p2align 3
.L764:
	movq	$0, 48(%r12)
	leaq	48(%r12), %r8
	movq	%r8, %r10
	jmp	.L735
.L765:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE26349:
	.size	_ZNSt8__detail9_Map_baseIPN2v88internal11MemoryChunkESt4pairIKS4_NS2_15MemoryChunkDataEESaIS8_ENS_10_Select1stESt8equal_toIS4_ENS3_6HasherENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS6_, .-_ZNSt8__detail9_Map_baseIPN2v88internal11MemoryChunkESt4pairIKS4_NS2_15MemoryChunkDataEESaIS8_ENS_10_Select1stESt8equal_toIS4_ENS3_6HasherENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS6_
	.section	.text._ZN2v88internal24ConcurrentMarkingVisitor20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE,"axG",@progbits,_ZN2v88internal24ConcurrentMarkingVisitor20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal24ConcurrentMarkingVisitor20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE
	.type	_ZN2v88internal24ConcurrentMarkingVisitor20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE, @function
_ZN2v88internal24ConcurrentMarkingVisitor20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE:
.LFB22240:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	movq	(%rax), %r13
	movq	%r13, %rcx
	call	_ZN2v88internal20MarkCompactCollector22PrepareRecordRelocSlotENS0_4CodeEPNS0_9RelocInfoENS0_10HeapObjectE@PLT
	cmpb	$0, -68(%rbp)
	jne	.L805
.L768:
	movl	%r13d, %eax
	movq	%r13, %rdi
	movl	$1, %edx
	andl	$262143, %eax
	andq	$-262144, %rdi
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	leaq	0(,%rax,4), %rsi
	sall	%cl, %edx
	movq	16(%rdi), %rcx
	movl	(%rcx,%rax,4), %eax
	testl	%edx, %eax
	je	.L806
.L767:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L807
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L805:
	.cfi_restore_state
	movq	56(%r12), %rdi
	movq	%r14, %rsi
	call	_ZNSt8__detail9_Map_baseIPN2v88internal11MemoryChunkESt4pairIKS4_NS2_15MemoryChunkDataEESaIS8_ENS_10_Select1stESt8equal_toIS4_ENS3_6HasherENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS6_
	movq	8(%rax), %rdi
	movq	%rax, %r14
	testq	%rdi, %rdi
	je	.L808
.L769:
	movl	-64(%rbp), %edx
	movl	-72(%rbp), %esi
	call	_ZN2v88internal10TypedSlots6InsertENS0_8SlotTypeEj@PLT
	jmp	.L768
	.p2align 4,,10
	.p2align 3
.L806:
	testb	$62, 43(%rbx)
	je	.L809
.L771:
	addq	16(%rdi), %rsi
	.p2align 4,,10
	.p2align 3
.L778:
	movl	(%rsi), %ecx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%eax, %edx
	je	.L767
	movl	%edx, %edi
	movl	%ecx, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %ecx
	jne	.L778
	movslq	16(%r12), %rax
	movq	8(%r12), %r15
	leaq	(%rax,%rax,4), %rbx
	salq	$4, %rbx
	addq	%r15, %rbx
	movq	(%rbx), %r14
	movq	8(%r14), %r12
	cmpq	$64, %r12
	je	.L810
	leaq	1(%r12), %rax
	movq	%rax, 8(%r14)
	movq	%r13, 16(%r14,%r12,8)
	jmp	.L767
	.p2align 4,,10
	.p2align 3
.L809:
	movq	31(%rbx), %rax
	movl	15(%rax), %eax
	testb	$8, %al
	je	.L771
	movq	-1(%r13), %rax
	movzwl	11(%rax), %eax
	cmpw	$68, %ax
	je	.L811
	cmpw	$159, %ax
	je	.L773
	cmpw	$1023, %ax
	ja	.L773
	subw	$138, %ax
	cmpw	$9, %ax
	ja	.L771
	.p2align 4,,10
	.p2align 3
.L773:
	movslq	64(%r12), %rax
	movq	24(%r12), %r15
	leaq	(%rax,%rax,4), %r14
	salq	$4, %r14
	addq	%r15, %r14
	movq	4176(%r14), %r12
	movq	8(%r12), %rax
	cmpq	$64, %rax
	je	.L812
	addq	$1, %rax
	movq	%rax, 8(%r12)
	salq	$4, %rax
	addq	%r12, %rax
	movq	%r13, (%rax)
	movq	%rbx, 8(%rax)
	jmp	.L767
	.p2align 4,,10
	.p2align 3
.L808:
	movl	$24, %edi
	call	_Znwm@PLT
	movq	$0, 8(%rax)
	movq	%rax, %rdi
	movq	$0, 16(%rax)
	leaq	16+_ZTVN2v88internal10TypedSlotsE(%rip), %rax
	movq	%rax, (%rdi)
	movq	8(%r14), %r8
	movq	%rdi, 8(%r14)
	testq	%r8, %r8
	je	.L769
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
	movq	8(%r14), %rdi
	jmp	.L769
	.p2align 4,,10
	.p2align 3
.L810:
	leaq	640(%r15), %rdi
	movq	%rdi, -88(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	680(%r15), %rax
	movq	%rax, (%r14)
	movq	%r14, 680(%r15)
	movq	-88(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movq	%r12, %rcx
	movq	$0, 8(%rax)
	movq	%rax, %rdx
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rdx, (%rbx)
	movq	8(%rdx), %rax
	cmpq	$64, %rax
	je	.L767
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdx)
	movq	%r13, 16(%rdx,%rax,8)
	jmp	.L767
	.p2align 4,,10
	.p2align 3
.L811:
	cmpw	$1024, 11(%r13)
	jbe	.L771
	jmp	.L773
	.p2align 4,,10
	.p2align 3
.L812:
	leaq	4816(%r15), %rdi
	movq	%rdi, -88(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	4856(%r15), %rax
	movq	%rax, (%r12)
	movq	%r12, 4856(%r15)
	movq	-88(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$1040, %edi
	call	_Znwm@PLT
	movl	$128, %ecx
	movq	$0, 8(%rax)
	movq	%rax, %rdx
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rdx, 4176(%r14)
	movq	8(%rdx), %rax
	cmpq	$64, %rax
	je	.L767
	addq	$1, %rax
	movq	%rax, 8(%rdx)
	salq	$4, %rax
	addq	%rax, %rdx
	movq	%r13, (%rdx)
	movq	%rbx, 8(%rdx)
	jmp	.L767
.L807:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22240:
	.size	_ZN2v88internal24ConcurrentMarkingVisitor20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE, .-_ZN2v88internal24ConcurrentMarkingVisitor20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE
	.section	.rodata._ZN2v88internal24ConcurrentMarkingVisitor30VisitFixedArrayWithProgressBarENS0_3MapENS0_10FixedArrayEPNS0_11MemoryChunkE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"success"
.LC6:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal24ConcurrentMarkingVisitor30VisitFixedArrayWithProgressBarENS0_3MapENS0_10FixedArrayEPNS0_11MemoryChunkE,"axG",@progbits,_ZN2v88internal24ConcurrentMarkingVisitor30VisitFixedArrayWithProgressBarENS0_3MapENS0_10FixedArrayEPNS0_11MemoryChunkE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal24ConcurrentMarkingVisitor30VisitFixedArrayWithProgressBarENS0_3MapENS0_10FixedArrayEPNS0_11MemoryChunkE
	.type	_ZN2v88internal24ConcurrentMarkingVisitor30VisitFixedArrayWithProgressBarENS0_3MapENS0_10FixedArrayEPNS0_11MemoryChunkE, @function
_ZN2v88internal24ConcurrentMarkingVisitor30VisitFixedArrayWithProgressBarENS0_3MapENS0_10FixedArrayEPNS0_11MemoryChunkE:
.LFB22257:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movq	%rdx, %rsi
	pushq	%r14
	andq	$-262144, %rsi
	.cfi_offset 14, -32
	movq	%rdx, %r14
	leaq	-64(%rbp), %r9
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-1(%rdx), %rbx
	subq	$136, %rsp
	movq	%rdi, -96(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdx, -72(%rbp)
	movl	%ebx, %edx
	movl	$1, %eax
	subl	%esi, %edx
	movq	%rsi, -88(%rbp)
	movl	%edx, %ecx
	shrl	$8, %edx
	shrl	$3, %ecx
	sall	%cl, %eax
	movl	%eax, %ecx
	movq	16(%rsi), %rax
	leaq	(%rax,%rdx,4), %rsi
	movl	(%rsi), %eax
	testl	%ecx, %eax
	jne	.L886
.L845:
	movq	%r15, %rsi
	movq	%r9, %rdi
	movq	%r14, -64(%rbp)
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movl	%eax, %r15d
	leaq	88(%r13), %rax
	movq	%rax, -104(%rbp)
	movq	88(%r13), %rdx
	movl	%edx, %r9d
	leal	131072(%rdx), %r12d
	testl	%edx, %edx
	jne	.L818
	movl	$131088, %r12d
	movl	$16, %r9d
.L818:
	cmpl	%r12d, %r15d
	cmovle	%r15d, %r12d
	cmpl	%r12d, %r9d
	jl	.L887
.L820:
	movl	%r12d, %eax
	subl	%r9d, %eax
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L888
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L886:
	.cfi_restore_state
	addl	%ecx, %ecx
	jne	.L816
	movl	$1, %ecx
	movl	4(%rsi), %edx
	addq	$4, %rsi
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ecx
	je	.L889
	.p2align 4,,10
	.p2align 3
.L872:
	movl	%ecx, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	je	.L890
.L816:
	movl	(%rsi), %edx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ecx
	jne	.L872
.L889:
	leaq	-64(%rbp), %r9
	jmp	.L845
	.p2align 4,,10
	.p2align 3
.L887:
	movslq	%r12d, %rax
	leaq	(%rbx,%rax), %r8
	movq	%rax, -112(%rbp)
	movslq	%r9d, %rax
	addq	%rax, %rbx
	cmpq	%r8, %rbx
	jnb	.L838
	movl	%r15d, %esi
	movq	%r8, %rdi
	movq	%r14, %rax
	movq	%rdx, %r15
	movl	%r12d, %r8d
	movl	%r9d, %r14d
	movq	%rdi, %r12
	movq	%rax, %r9
	movl	%esi, %edx
	jmp	.L821
	.p2align 4,,10
	.p2align 3
.L824:
	addq	$8, %rbx
	cmpq	%rbx, %r12
	jbe	.L891
.L821:
	movq	(%rbx), %r13
	testb	$1, %r13b
	je	.L824
	movl	%r13d, %eax
	movq	%r13, %r10
	movl	$1, %edi
	andl	$262143, %eax
	andq	$-262144, %r10
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %edi
	movq	16(%r10), %rcx
	leaq	(%rcx,%rax,4), %rsi
	jmp	.L826
	.p2align 4,,10
	.p2align 3
.L893:
	movl	%edi, %r11d
	movl	%ecx, %eax
	orl	%ecx, %r11d
	lock cmpxchgl	%r11d, (%rsi)
	cmpl	%eax, %ecx
	je	.L892
.L826:
	movl	(%rsi), %ecx
	movl	%edi, %eax
	andl	%ecx, %eax
	cmpl	%eax, %edi
	jne	.L893
.L825:
	movq	8(%r10), %rax
	testb	$64, %al
	je	.L824
	movq	-88(%rbp), %rax
	movq	8(%rax), %rax
	testb	$88, %al
	je	.L830
	testb	$-128, %ah
	je	.L824
.L830:
	movq	-88(%rbp), %rax
	movq	112(%rax), %r13
	testq	%r13, %r13
	je	.L894
.L833:
	movl	%ebx, %ecx
	movq	%rbx, %rax
	subq	-88(%rbp), %rax
	andl	$262143, %ecx
	shrq	$18, %rax
	movl	%ecx, %r10d
	movl	%ecx, %r11d
	leaq	(%rax,%rax,2), %rax
	sarl	$13, %ecx
	salq	$7, %rax
	movslq	%ecx, %rcx
	sarl	$8, %r10d
	leaq	(%rax,%rcx,8), %rax
	sarl	$3, %r11d
	andl	$31, %r10d
	addq	%rax, %r13
	andl	$31, %r11d
	movq	0(%r13), %rsi
	testq	%rsi, %rsi
	je	.L895
.L835:
	movslq	%r10d, %r10
	movl	$1, %edi
	movl	%r11d, %ecx
	leaq	(%rsi,%r10,4), %rsi
	sall	%cl, %edi
	movl	(%rsi), %eax
	testl	%eax, %edi
	je	.L837
	addq	$8, %rbx
	cmpq	%rbx, %r12
	ja	.L821
	.p2align 4,,10
	.p2align 3
.L891:
	movl	%edx, %ebx
	movq	%r9, %rax
	movq	%r15, %rdx
	movl	%r14d, %r9d
	movl	%r8d, %r12d
	movq	%rax, %r14
	movl	%ebx, %r15d
.L838:
	movq	%rdx, %rax
	movq	-104(%rbp), %rbx
	movq	-112(%rbp), %rdx
	lock cmpxchgq	%rdx, (%rbx)
	jne	.L896
	cmpl	%r12d, %r15d
	jle	.L820
	movq	-96(%rbp), %rax
	movq	8(%rax), %rdx
	movslq	16(%rax), %rax
	leaq	(%rax,%rax,4), %rbx
	salq	$4, %rbx
	addq	%rdx, %rbx
	movq	(%rbx), %r15
	movq	8(%r15), %r13
	cmpq	$64, %r13
	je	.L897
	leaq	1(%r13), %rax
	movq	%rax, 8(%r15)
	movq	%r14, 16(%r15,%r13,8)
	jmp	.L820
	.p2align 4,,10
	.p2align 3
.L898:
	movl	%edi, %r10d
	movl	%ecx, %eax
	orl	%ecx, %r10d
	lock cmpxchgl	%r10d, (%rsi)
	cmpl	%eax, %ecx
	je	.L824
.L837:
	movl	(%rsi), %ecx
	movl	%edi, %eax
	andl	%ecx, %eax
	cmpl	%eax, %edi
	jne	.L898
	jmp	.L824
	.p2align 4,,10
	.p2align 3
.L892:
	movq	-96(%rbp), %rax
	movq	8(%rax), %r11
	movslq	16(%rax), %rax
	leaq	(%rax,%rax,4), %rsi
	salq	$4, %rsi
	addq	%r11, %rsi
	movq	(%rsi), %rax
	movq	8(%rax), %rcx
	cmpq	$64, %rcx
	je	.L899
	leaq	1(%rcx), %rsi
	movq	%rsi, 8(%rax)
	movq	%r13, 16(%rax,%rcx,8)
	jmp	.L825
	.p2align 4,,10
	.p2align 3
.L895:
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	movq	%r9, -152(%rbp)
	movl	%r8d, -144(%rbp)
	movl	%edx, -136(%rbp)
	movl	%r11d, -128(%rbp)
	movl	%r10d, -120(%rbp)
	call	_ZnamRKSt9nothrow_t@PLT
	movl	-120(%rbp), %r10d
	movl	-128(%rbp), %r11d
	testq	%rax, %rax
	movl	-136(%rbp), %edx
	movq	%rax, %rsi
	movl	-144(%rbp), %r8d
	movq	-152(%rbp), %r9
	je	.L900
.L836:
	leaq	8(%rsi), %rdi
	movq	%rsi, %rcx
	xorl	%eax, %eax
	movq	$0, (%rsi)
	andq	$-8, %rdi
	movq	$0, 120(%rsi)
	subq	%rdi, %rcx
	subl	$-128, %ecx
	shrl	$3, %ecx
	rep stosq
	lock cmpxchgq	%rsi, 0(%r13)
	testq	%rax, %rax
	je	.L835
	movq	%rsi, %rdi
	movq	%r9, -152(%rbp)
	movl	%r8d, -144(%rbp)
	movl	%edx, -136(%rbp)
	movl	%r11d, -128(%rbp)
	movl	%r10d, -120(%rbp)
	call	_ZdaPv@PLT
	movq	0(%r13), %rsi
	movq	-152(%rbp), %r9
	movl	-144(%rbp), %r8d
	movl	-136(%rbp), %edx
	movl	-128(%rbp), %r11d
	movl	-120(%rbp), %r10d
	jmp	.L835
	.p2align 4,,10
	.p2align 3
.L894:
	movq	-88(%rbp), %rdi
	movq	%r9, -136(%rbp)
	movl	%r8d, -128(%rbp)
	movl	%edx, -120(%rbp)
	call	_ZN2v88internal11MemoryChunk15AllocateSlotSetILNS0_17RememberedSetTypeE1EEEPNS0_7SlotSetEv@PLT
	movq	-136(%rbp), %r9
	movl	-128(%rbp), %r8d
	movl	-120(%rbp), %edx
	movq	%rax, %r13
	jmp	.L833
	.p2align 4,,10
	.p2align 3
.L890:
	movq	-72(%rbp), %rax
	leaq	-72(%rbp), %rdi
	movq	-1(%rax), %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	leaq	-64(%rbp), %r9
	movslq	%eax, %r12
	movq	-96(%rbp), %rax
	movq	%r9, %rsi
	movq	%r9, -104(%rbp)
	movq	48(%rax), %rdi
	movq	-88(%rbp), %rax
	movq	%rax, -64(%rbp)
	call	_ZNSt8__detail9_Map_baseIPN2v88internal11MemoryChunkESt4pairIKS4_NS2_15MemoryChunkDataEESaIS8_ENS_10_Select1stESt8equal_toIS4_ENS3_6HasherENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS6_
	movq	-104(%rbp), %r9
	addq	%r12, (%rax)
	jmp	.L845
	.p2align 4,,10
	.p2align 3
.L899:
	leaq	640(%r11), %rdi
	movq	%r9, -176(%rbp)
	movl	%r8d, -168(%rbp)
	movq	%rsi, -160(%rbp)
	movq	%rcx, -152(%rbp)
	movq	%r10, -136(%rbp)
	movl	%edx, -164(%rbp)
	movq	%rax, -144(%rbp)
	movq	%r11, -128(%rbp)
	movq	%rdi, -120(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-128(%rbp), %r11
	movq	-144(%rbp), %rax
	movq	680(%r11), %rdx
	movq	%rdx, (%rax)
	movq	%rax, 680(%r11)
	movq	-120(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movq	-152(%rbp), %rcx
	movq	-160(%rbp), %rsi
	movq	$0, 8(%rax)
	movq	%rax, %r11
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%r11, (%rsi)
	movq	-136(%rbp), %r10
	movl	-164(%rbp), %edx
	movl	-168(%rbp), %r8d
	movq	8(%r11), %rax
	movq	-176(%rbp), %r9
	cmpq	$64, %rax
	je	.L825
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%r11)
	movq	%r13, 16(%r11,%rax,8)
	jmp	.L825
	.p2align 4,,10
	.p2align 3
.L896:
	leaq	.LC5(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L897:
	leaq	640(%rdx), %rdi
	movl	%r9d, -104(%rbp)
	movq	%rdx, -96(%rbp)
	movq	%rdi, -88(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-96(%rbp), %rdx
	movq	680(%rdx), %rax
	movq	%rax, (%r15)
	movq	%r15, 680(%rdx)
	movq	-88(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movq	%r13, %rcx
	movl	-104(%rbp), %r9d
	movq	$0, 8(%rax)
	movq	%rax, %rdx
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rdx, (%rbx)
	movq	8(%rdx), %rax
	cmpq	$64, %rax
	je	.L820
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdx)
	movq	%r14, 16(%rdx,%rax,8)
	jmp	.L820
.L888:
	call	__stack_chk_fail@PLT
.L900:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movl	-120(%rbp), %r10d
	movl	-128(%rbp), %r11d
	testq	%rax, %rax
	movl	-136(%rbp), %edx
	movq	%rax, %rsi
	movl	-144(%rbp), %r8d
	movq	-152(%rbp), %r9
	jne	.L836
	leaq	.LC1(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
	.cfi_endproc
.LFE22257:
	.size	_ZN2v88internal24ConcurrentMarkingVisitor30VisitFixedArrayWithProgressBarENS0_3MapENS0_10FixedArrayEPNS0_11MemoryChunkE, .-_ZN2v88internal24ConcurrentMarkingVisitor30VisitFixedArrayWithProgressBarENS0_3MapENS0_10FixedArrayEPNS0_11MemoryChunkE
	.section	.text._ZN2v88internal24ConcurrentMarkingVisitor11ShouldVisitENS0_10HeapObjectE,"axG",@progbits,_ZN2v88internal24ConcurrentMarkingVisitor11ShouldVisitENS0_10HeapObjectE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal24ConcurrentMarkingVisitor11ShouldVisitENS0_10HeapObjectE
	.type	_ZN2v88internal24ConcurrentMarkingVisitor11ShouldVisitENS0_10HeapObjectE, @function
_ZN2v88internal24ConcurrentMarkingVisitor11ShouldVisitENS0_10HeapObjectE:
.LFB22232:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	andq	$-262144, %r12
	subq	$40, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -56(%rbp)
	subq	$1, %rsi
	movl	$1, %eax
	subl	%r12d, %esi
	movl	%esi, %ecx
	shrl	$8, %esi
	shrl	$3, %ecx
	sall	%cl, %eax
	movl	%eax, %ecx
	movq	16(%r12), %rax
	leaq	(%rax,%rsi,4), %rsi
	movl	(%rsi), %eax
	testl	%ecx, %eax
	jne	.L914
.L902:
	xorl	%eax, %eax
.L901:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L915
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L914:
	.cfi_restore_state
	movq	%rdi, %rbx
	addl	%ecx, %ecx
	jne	.L904
	movl	4(%rsi), %edx
	movl	$1, %ecx
	addq	$4, %rsi
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%ecx, %eax
	je	.L902
	.p2align 4,,10
	.p2align 3
.L917:
	movl	%edx, %edi
	movl	%edx, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	je	.L916
.L904:
	movl	(%rsi), %edx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%ecx, %eax
	jne	.L917
	jmp	.L902
	.p2align 4,,10
	.p2align 3
.L916:
	movq	-56(%rbp), %rax
	leaq	-56(%rbp), %rdi
	movq	-1(%rax), %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	48(%rbx), %rdi
	leaq	-48(%rbp), %rsi
	movq	%r12, -48(%rbp)
	movslq	%eax, %r13
	call	_ZNSt8__detail9_Map_baseIPN2v88internal11MemoryChunkESt4pairIKS4_NS2_15MemoryChunkDataEESaIS8_ENS_10_Select1stESt8equal_toIS4_ENS3_6HasherENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS6_
	addq	%r13, (%rax)
	movl	$1, %eax
	jmp	.L901
.L915:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22232:
	.size	_ZN2v88internal24ConcurrentMarkingVisitor11ShouldVisitENS0_10HeapObjectE, .-_ZN2v88internal24ConcurrentMarkingVisitor11ShouldVisitENS0_10HeapObjectE
	.section	.text._ZN2v88internal24ConcurrentMarkingVisitor24MarkDescriptorArrayBlackENS0_15DescriptorArrayE,"axG",@progbits,_ZN2v88internal24ConcurrentMarkingVisitor24MarkDescriptorArrayBlackENS0_15DescriptorArrayE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal24ConcurrentMarkingVisitor24MarkDescriptorArrayBlackENS0_15DescriptorArrayE
	.type	_ZN2v88internal24ConcurrentMarkingVisitor24MarkDescriptorArrayBlackENS0_15DescriptorArrayE, @function
_ZN2v88internal24ConcurrentMarkingVisitor24MarkDescriptorArrayBlackENS0_15DescriptorArrayE:
.LFB22271:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %edx
	andl	$262143, %edx
	movl	%edx, %ecx
	shrl	$8, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	shrl	$3, %ecx
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	andq	$-262144, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$1, %eax
	sall	%cl, %eax
	movl	%eax, %ecx
	movq	16(%rbx), %rax
	leaq	(%rax,%rdx,4), %rsi
	jmp	.L922
	.p2align 4,,10
	.p2align 3
.L919:
	movl	%edx, %edi
	movl	%edx, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	je	.L923
.L922:
	movl	(%rsi), %edx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%ecx, %eax
	jne	.L919
.L923:
	leaq	-1(%r13), %rdx
	movl	$1, %eax
	movq	%r13, -72(%rbp)
	subl	%ebx, %edx
	movl	%edx, %ecx
	shrl	$8, %edx
	shrl	$3, %ecx
	sall	%cl, %eax
	movl	%eax, %ecx
	movq	16(%rbx), %rax
	leaq	(%rax,%rdx,4), %rsi
	movl	(%rsi), %eax
	testl	%ecx, %eax
	jne	.L968
.L918:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L969
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L968:
	.cfi_restore_state
	addl	%ecx, %ecx
	je	.L970
	.p2align 4,,10
	.p2align 3
.L925:
	movl	(%rsi), %edx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ecx
	je	.L918
	movl	%ecx, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	jne	.L925
	movq	-72(%rbp), %rax
	leaq	-72(%rbp), %rdi
	movq	-1(%rax), %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	48(%r12), %rdi
	leaq	-64(%rbp), %rsi
	movq	%rbx, -64(%rbp)
	movslq	%eax, %r14
	call	_ZNSt8__detail9_Map_baseIPN2v88internal11MemoryChunkESt4pairIKS4_NS2_15MemoryChunkDataEESaIS8_ENS_10_Select1stESt8equal_toIS4_ENS3_6HasherENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS6_
	addq	%r14, (%rax)
	leaq	15(%r13), %r14
	leaq	23(%r13), %rax
	cmpq	%rax, %r14
	jnb	.L918
	movq	15(%r13), %r13
	testb	$1, %r13b
	je	.L918
	movl	%r13d, %eax
	movq	%r13, %r15
	movl	$1, %edx
	andl	$262143, %eax
	andq	$-262144, %r15
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %edx
	movl	%edx, %ecx
	movq	16(%r15), %rdx
	leaq	(%rdx,%rax,4), %rsi
	jmp	.L929
	.p2align 4,,10
	.p2align 3
.L972:
	movl	%ecx, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	je	.L971
.L929:
	movl	(%rsi), %edx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ecx
	jne	.L972
.L928:
	movq	8(%r15), %rax
	testb	$64, %al
	je	.L918
	movq	8(%rbx), %rax
	testb	$88, %al
	je	.L933
	testb	$-128, %ah
	je	.L918
.L933:
	movq	112(%rbx), %r12
	testq	%r12, %r12
	je	.L973
.L936:
	movq	%r14, %rax
	andl	$262143, %r14d
	subq	%rbx, %rax
	movl	%r14d, %r13d
	movl	%r14d, %ebx
	sarl	$13, %r14d
	shrq	$18, %rax
	sarl	$8, %ebx
	leaq	(%rax,%rax,2), %rdx
	movslq	%r14d, %rax
	sarl	$3, %r13d
	andl	$31, %ebx
	salq	$7, %rdx
	andl	$31, %r13d
	leaq	(%rdx,%rax,8), %rax
	addq	%rax, %r12
	movq	(%r12), %r8
	testq	%r8, %r8
	je	.L974
.L938:
	movl	%r13d, %ecx
	movl	$1, %eax
	movslq	%ebx, %rbx
	sall	%cl, %eax
	leaq	(%r8,%rbx,4), %rsi
	movl	%eax, %ecx
	movl	(%rsi), %eax
	testl	%eax, %ecx
	jne	.L918
	jmp	.L940
	.p2align 4,,10
	.p2align 3
.L970:
	addq	$4, %rsi
	movl	$1, %ecx
	jmp	.L925
	.p2align 4,,10
	.p2align 3
.L975:
	movl	%ecx, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	je	.L918
.L940:
	movl	(%rsi), %edx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ecx
	jne	.L975
	jmp	.L918
	.p2align 4,,10
	.p2align 3
.L971:
	movslq	16(%r12), %rax
	movq	8(%r12), %rdx
	leaq	(%rax,%rax,4), %r12
	salq	$4, %r12
	addq	%rdx, %r12
	movq	(%r12), %rax
	movq	8(%rax), %rcx
	cmpq	$64, %rcx
	je	.L976
	leaq	1(%rcx), %rdx
	movq	%rdx, 8(%rax)
	movq	%r13, 16(%rax,%rcx,8)
	jmp	.L928
.L973:
	movq	%rbx, %rdi
	call	_ZN2v88internal11MemoryChunk15AllocateSlotSetILNS0_17RememberedSetTypeE1EEEPNS0_7SlotSetEv@PLT
	movq	%rax, %r12
	jmp	.L936
.L974:
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L977
.L939:
	leaq	8(%r8), %rdi
	movq	%r8, %rcx
	xorl	%eax, %eax
	movq	$0, (%r8)
	andq	$-8, %rdi
	movq	$0, 120(%r8)
	subq	%rdi, %rcx
	subl	$-128, %ecx
	shrl	$3, %ecx
	rep stosq
	lock cmpxchgq	%r8, (%r12)
	testq	%rax, %rax
	je	.L938
	movq	%r8, %rdi
	call	_ZdaPv@PLT
	movq	(%r12), %r8
	jmp	.L938
.L976:
	leaq	640(%rdx), %rdi
	movq	%rcx, -112(%rbp)
	movq	%rax, -104(%rbp)
	movq	%rdx, -96(%rbp)
	movq	%rdi, -88(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-96(%rbp), %rdx
	movq	-104(%rbp), %rax
	movq	680(%rdx), %rsi
	movq	%rsi, (%rax)
	movq	%rax, 680(%rdx)
	movq	-88(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movq	-112(%rbp), %rcx
	movq	$0, 8(%rax)
	movq	%rax, %rdx
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rdx, (%r12)
	movq	8(%rdx), %rax
	cmpq	$64, %rax
	je	.L928
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdx)
	movq	%r13, 16(%rdx,%rax,8)
	jmp	.L928
.L969:
	call	__stack_chk_fail@PLT
.L977:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	jne	.L939
	leaq	.LC1(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
	.cfi_endproc
.LFE22271:
	.size	_ZN2v88internal24ConcurrentMarkingVisitor24MarkDescriptorArrayBlackENS0_15DescriptorArrayE, .-_ZN2v88internal24ConcurrentMarkingVisitor24MarkDescriptorArrayBlackENS0_15DescriptorArrayE
	.section	.rodata._ZN2v88internal24ConcurrentMarkingVisitor15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE.str1.8,"aMS",@progbits,1
	.align 8
.LC7:
	.string	"address < start || address >= end"
	.section	.text._ZN2v88internal24ConcurrentMarkingVisitor15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE,"axG",@progbits,_ZN2v88internal24ConcurrentMarkingVisitor15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal24ConcurrentMarkingVisitor15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE
	.type	_ZN2v88internal24ConcurrentMarkingVisitor15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE, @function
_ZN2v88internal24ConcurrentMarkingVisitor15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE:
.LFB22241:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	movslq	(%rax), %rbx
	addq	%rax, %rbx
	leaq	4(%rbx), %rdx
	movq	%rdx, -88(%rbp)
	call	_ZN2v88internal7Isolate19CurrentEmbeddedBlobEv@PLT
	movq	%rax, %r14
	call	_ZN2v88internal7Isolate23CurrentEmbeddedBlobSizeEv@PLT
	movq	-88(%rbp), %rdx
	movl	%eax, %eax
	addq	%r14, %rax
	cmpq	%rax, %rdx
	jnb	.L979
	cmpq	%rdx, %r14
	jbe	.L1006
.L979:
	subq	$59, %rbx
	leaq	-80(%rbp), %r14
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%rbx, %rcx
	movq	%r14, %rdi
	call	_ZN2v88internal20MarkCompactCollector22PrepareRecordRelocSlotENS0_4CodeEPNS0_9RelocInfoENS0_10HeapObjectE@PLT
	cmpb	$0, -68(%rbp)
	jne	.L1007
.L980:
	movl	%ebx, %eax
	movq	%rbx, %rsi
	movl	$1, %edx
	andl	$262143, %eax
	andq	$-262144, %rsi
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %edx
	movq	16(%rsi), %rcx
	leaq	(%rcx,%rax,4), %rsi
	jmp	.L983
	.p2align 4,,10
	.p2align 3
.L1009:
	movl	%edx, %edi
	movl	%ecx, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %ecx
	je	.L1008
.L983:
	movl	(%rsi), %ecx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%eax, %edx
	jne	.L1009
.L978:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1010
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1008:
	.cfi_restore_state
	movslq	16(%r12), %rax
	movq	8(%r12), %r15
	leaq	(%rax,%rax,4), %r12
	salq	$4, %r12
	addq	%r15, %r12
	movq	(%r12), %r14
	movq	8(%r14), %r13
	cmpq	$64, %r13
	je	.L1011
	leaq	1(%r13), %rax
	movq	%rax, 8(%r14)
	movq	%rbx, 16(%r14,%r13,8)
	jmp	.L978
	.p2align 4,,10
	.p2align 3
.L1007:
	movq	56(%r12), %rdi
	movq	%r14, %rsi
	call	_ZNSt8__detail9_Map_baseIPN2v88internal11MemoryChunkESt4pairIKS4_NS2_15MemoryChunkDataEESaIS8_ENS_10_Select1stESt8equal_toIS4_ENS3_6HasherENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS6_
	movq	8(%rax), %rdi
	movq	%rax, %r13
	testq	%rdi, %rdi
	je	.L1012
.L981:
	movl	-64(%rbp), %edx
	movl	-72(%rbp), %esi
	call	_ZN2v88internal10TypedSlots6InsertENS0_8SlotTypeEj@PLT
	jmp	.L980
	.p2align 4,,10
	.p2align 3
.L1006:
	leaq	.LC7(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1011:
	leaq	640(%r15), %rdi
	movq	%rdi, -88(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	680(%r15), %rax
	movq	%rax, (%r14)
	movq	%r14, 680(%r15)
	movq	-88(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movq	%r13, %rcx
	movq	$0, 8(%rax)
	movq	%rax, %rdx
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rdx, (%r12)
	movq	8(%rdx), %rax
	cmpq	$64, %rax
	je	.L978
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdx)
	movq	%rbx, 16(%rdx,%rax,8)
	jmp	.L978
	.p2align 4,,10
	.p2align 3
.L1012:
	movl	$24, %edi
	call	_Znwm@PLT
	movq	$0, 8(%rax)
	movq	%rax, %rdi
	movq	$0, 16(%rax)
	leaq	16+_ZTVN2v88internal10TypedSlotsE(%rip), %rax
	movq	%rax, (%rdi)
	movq	8(%r13), %r8
	movq	%rdi, 8(%r13)
	testq	%r8, %r8
	je	.L981
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
	movq	8(%r13), %rdi
	jmp	.L981
.L1010:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22241:
	.size	_ZN2v88internal24ConcurrentMarkingVisitor15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE, .-_ZN2v88internal24ConcurrentMarkingVisitor15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE
	.section	.text._ZN2v88internal24ConcurrentMarkingVisitor21VisitJSObjectSubclassINS0_8JSObjectENS3_14BodyDescriptorEEEiNS0_3MapET_,"axG",@progbits,_ZN2v88internal24ConcurrentMarkingVisitor21VisitJSObjectSubclassINS0_8JSObjectENS3_14BodyDescriptorEEEiNS0_3MapET_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal24ConcurrentMarkingVisitor21VisitJSObjectSubclassINS0_8JSObjectENS3_14BodyDescriptorEEEiNS0_3MapET_
	.type	_ZN2v88internal24ConcurrentMarkingVisitor21VisitJSObjectSubclassINS0_8JSObjectENS3_14BodyDescriptorEEEiNS0_3MapET_, @function
_ZN2v88internal24ConcurrentMarkingVisitor21VisitJSObjectSubclassINS0_8JSObjectENS3_14BodyDescriptorEEEiNS0_3MapET_:
.LFB24828:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	7(%rsi), %eax
	movb	%al, -121(%rbp)
	movzbl	9(%rsi), %eax
	cmpl	$2, %eax
	jle	.L1087
	leal	0(,%rax,8), %r12d
.L1015:
	leaq	-1(%r15), %r14
	leaq	7(%r15), %rax
	movl	$0, 72(%rbx)
	leaq	72(%rbx), %r8
	cmpq	%rax, %r14
	jnb	.L1016
	movq	-1(%r15), %rcx
	movq	%r14, %xmm0
	movl	$1, 72(%rbx)
	movq	%rcx, %xmm3
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 80(%rbx)
.L1016:
	movq	47(%rsi), %rcx
	testq	%rcx, %rcx
	jne	.L1017
	movslq	%r12d, %rdx
	addq	%r14, %rdx
	cmpq	%rdx, %rax
	jnb	.L1019
	.p2align 4,,10
	.p2align 3
.L1018:
	movq	(%rax), %rsi
	movslq	72(%rbx), %rcx
	movq	%rax, %xmm0
	addq	$8, %rax
	movq	%rsi, %xmm1
	leal	1(%rcx), %edi
	salq	$4, %rcx
	punpcklqdq	%xmm1, %xmm0
	movl	%edi, 72(%rbx)
	movups	%xmm0, 8(%r8,%rcx)
	cmpq	%rax, %rdx
	ja	.L1018
.L1019:
	movq	%r15, -88(%rbp)
	movl	%r14d, %eax
	andq	$-262144, %r15
	movl	$1, %esi
	movq	16(%r15), %rdx
	subl	%r15d, %eax
	movl	%eax, %ecx
	shrl	$8, %eax
	leaq	(%rdx,%rax,4), %rdx
	shrl	$3, %ecx
	movl	(%rdx), %eax
	sall	%cl, %esi
	movl	%esi, %ecx
	testl	%eax, %esi
	jne	.L1028
.L1031:
	xorl	%eax, %eax
.L1013:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1088
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1017:
	.cfi_restore_state
	movq	$0, -72(%rbp)
	movb	$1, -80(%rbp)
	movl	$0, -76(%rbp)
	movq	47(%rsi), %rax
	movq	%rax, -72(%rbp)
	testq	%rax, %rax
	jne	.L1089
.L1021:
	movl	$8, %r13d
	leaq	-88(%rbp), %rcx
	leaq	-80(%rbp), %rdi
	cmpl	$8, %r12d
	jg	.L1022
	jmp	.L1019
	.p2align 4,,10
	.p2align 3
.L1090:
	movl	-88(%rbp), %r13d
.L1024:
	cmpl	%r13d, %r12d
	jle	.L1019
.L1022:
	movl	%r12d, %edx
	movl	%r13d, %esi
	movq	%r8, -120(%rbp)
	movq	%rcx, -112(%rbp)
	movq	%rdi, -104(%rbp)
	call	_ZN2v88internal22LayoutDescriptorHelper8IsTaggedEiiPi@PLT
	movq	-104(%rbp), %rdi
	movq	-112(%rbp), %rcx
	testb	%al, %al
	movq	-120(%rbp), %r8
	je	.L1090
	movslq	-88(%rbp), %r9
	movslq	%r13d, %rax
	addq	%r14, %rax
	movq	%r9, %r10
	addq	%r14, %r9
	cmpq	%rax, %r9
	jbe	.L1054
	.p2align 4,,10
	.p2align 3
.L1026:
	movq	(%rax), %r11
	movslq	72(%rbx), %rsi
	movq	%rax, %xmm0
	addq	$8, %rax
	movq	%r11, %xmm2
	leal	1(%rsi), %r13d
	salq	$4, %rsi
	punpcklqdq	%xmm2, %xmm0
	movl	%r13d, 72(%rbx)
	movups	%xmm0, 8(%r8,%rsi)
	cmpq	%r9, %rax
	jb	.L1026
.L1054:
	movl	%r10d, %r13d
	jmp	.L1024
	.p2align 4,,10
	.p2align 3
.L1028:
	addl	%ecx, %ecx
	jne	.L1032
	movl	4(%rdx), %esi
	movl	$1, %ecx
	addq	$4, %rdx
	movl	%esi, %eax
	andl	%ecx, %eax
	cmpl	%eax, %ecx
	je	.L1031
	.p2align 4,,10
	.p2align 3
.L1092:
	movl	%esi, %edi
	movl	%esi, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rdx)
	cmpl	%eax, %esi
	je	.L1091
.L1032:
	movl	(%rdx), %esi
	movl	%esi, %eax
	andl	%ecx, %eax
	cmpl	%eax, %ecx
	jne	.L1092
	jmp	.L1031
	.p2align 4,,10
	.p2align 3
.L1089:
	movzbl	8(%rsi), %eax
	movb	$0, -80(%rbp)
	sall	$3, %eax
	movl	%eax, -76(%rbp)
	jmp	.L1021
	.p2align 4,,10
	.p2align 3
.L1091:
	movq	-88(%rbp), %rax
	leaq	-88(%rbp), %rdi
	xorl	%r14d, %r14d
	movq	-1(%rax), %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	48(%rbx), %rdi
	leaq	-80(%rbp), %rsi
	movq	%r15, -80(%rbp)
	movslq	%eax, %r13
	call	_ZNSt8__detail9_Map_baseIPN2v88internal11MemoryChunkESt4pairIKS4_NS2_15MemoryChunkDataEESaIS8_ENS_10_Select1stESt8equal_toIS4_ENS3_6HasherENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS6_
	movl	$1, %r8d
	addq	%r13, (%rax)
	movl	72(%rbx), %eax
	testl	%eax, %eax
	jle	.L1051
	.p2align 4,,10
	.p2align 3
.L1048:
	movq	%r14, %rcx
	salq	$4, %rcx
	movq	88(%rbx,%rcx), %r12
	testb	$1, %r12b
	je	.L1034
	movl	%r12d, %eax
	movq	80(%rbx,%rcx), %r13
	movq	%r12, %rsi
	movl	%r8d, %edi
	andl	$262143, %eax
	andq	$-262144, %rsi
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %edi
	movq	16(%rsi), %rcx
	leaq	(%rcx,%rax,4), %r9
	jmp	.L1036
	.p2align 4,,10
	.p2align 3
.L1094:
	movl	%edi, %r10d
	movl	%ecx, %eax
	orl	%ecx, %r10d
	lock cmpxchgl	%r10d, (%r9)
	cmpl	%eax, %ecx
	je	.L1093
.L1036:
	movl	(%r9), %ecx
	movl	%edi, %eax
	andl	%ecx, %eax
	cmpl	%eax, %edi
	jne	.L1094
.L1035:
	movq	8(%rsi), %rax
	testb	$64, %al
	jne	.L1039
.L1085:
	movl	72(%rbx), %eax
.L1034:
	addq	$1, %r14
	cmpl	%r14d, %eax
	jg	.L1048
.L1051:
	movzbl	-121(%rbp), %eax
	sall	$3, %eax
	jmp	.L1013
	.p2align 4,,10
	.p2align 3
.L1039:
	movq	8(%r15), %rax
	testb	$88, %al
	je	.L1040
	testb	$-128, %ah
	je	.L1085
.L1040:
	movq	112(%r15), %rax
	testq	%rax, %rax
	je	.L1095
.L1042:
	movq	%r13, %rcx
	andl	$262143, %r13d
	subq	%r15, %rcx
	movl	%r13d, %edx
	movl	%r13d, %r9d
	sarl	$13, %r13d
	shrq	$18, %rcx
	movslq	%r13d, %r13
	sarl	$8, %edx
	leaq	(%rcx,%rcx,2), %rcx
	andl	$31, %edx
	sarl	$3, %r9d
	salq	$7, %rcx
	movl	%edx, %r12d
	andl	$31, %r9d
	leaq	(%rcx,%r13,8), %r13
	addq	%rax, %r13
	movq	0(%r13), %r10
	testq	%r10, %r10
	je	.L1096
.L1044:
	movl	%r9d, %ecx
	movl	%r8d, %esi
	movslq	%r12d, %rdx
	sall	%cl, %esi
	leaq	(%r10,%rdx,4), %rcx
	movl	(%rcx), %eax
	testl	%eax, %esi
	jne	.L1085
	movl	(%rcx), %edx
	movl	%esi, %eax
	andl	%edx, %eax
	cmpl	%eax, %esi
	je	.L1085
	.p2align 4,,10
	.p2align 3
.L1047:
	movl	%esi, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rcx)
	cmpl	%eax, %edx
	je	.L1085
	movl	(%rcx), %edx
	movl	%esi, %eax
	andl	%edx, %eax
	cmpl	%eax, %esi
	jne	.L1047
	jmp	.L1085
	.p2align 4,,10
	.p2align 3
.L1093:
	movslq	16(%rbx), %rax
	movq	8(%rbx), %r10
	leaq	(%rax,%rax,4), %rax
	salq	$4, %rax
	leaq	(%r10,%rax), %r9
	movq	(%r9), %rax
	movq	8(%rax), %rcx
	cmpq	$64, %rcx
	je	.L1097
	leaq	1(%rcx), %rdi
	movq	%rdi, 8(%rax)
	movq	%r12, 16(%rax,%rcx,8)
	jmp	.L1035
	.p2align 4,,10
	.p2align 3
.L1087:
	movzbl	7(%rsi), %edx
	leal	0(,%rdx,8), %r12d
	jmp	.L1015
	.p2align 4,,10
	.p2align 3
.L1096:
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	movl	%r9d, -104(%rbp)
	call	_ZnamRKSt9nothrow_t@PLT
	movl	-104(%rbp), %r9d
	movl	$1, %r8d
	testq	%rax, %rax
	movq	%rax, %r10
	je	.L1098
.L1045:
	leaq	8(%r10), %rdi
	movq	%r10, %rcx
	xorl	%eax, %eax
	movq	$0, (%r10)
	andq	$-8, %rdi
	movq	$0, 120(%r10)
	subq	%rdi, %rcx
	subl	$-128, %ecx
	shrl	$3, %ecx
	rep stosq
	lock cmpxchgq	%r10, 0(%r13)
	testq	%rax, %rax
	je	.L1044
	movq	%r10, %rdi
	movl	%r9d, -104(%rbp)
	call	_ZdaPv@PLT
	movq	0(%r13), %r10
	movl	$1, %r8d
	movl	-104(%rbp), %r9d
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L1095:
	movq	%r15, %rdi
	call	_ZN2v88internal11MemoryChunk15AllocateSlotSetILNS0_17RememberedSetTypeE1EEEPNS0_7SlotSetEv@PLT
	movl	$1, %r8d
	jmp	.L1042
	.p2align 4,,10
	.p2align 3
.L1097:
	leaq	640(%r10), %rdi
	movq	%r9, -152(%rbp)
	movq	%rcx, -144(%rbp)
	movq	%rsi, -120(%rbp)
	movq	%rax, -136(%rbp)
	movq	%r10, -112(%rbp)
	movq	%rdi, -104(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-112(%rbp), %r10
	movq	-136(%rbp), %rax
	movq	680(%r10), %r11
	movq	%r11, (%rax)
	movq	%rax, 680(%r10)
	movq	-104(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movq	-144(%rbp), %rcx
	movq	-120(%rbp), %rsi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	%rax, %r10
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	-152(%rbp), %r9
	movq	%r10, (%r9)
	movq	8(%r10), %rax
	cmpq	$64, %rax
	je	.L1035
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%r10)
	movq	%r12, 16(%r10,%rax,8)
	jmp	.L1035
.L1088:
	call	__stack_chk_fail@PLT
.L1098:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movl	-104(%rbp), %r9d
	movl	$1, %r8d
	testq	%rax, %rax
	movq	%rax, %r10
	jne	.L1045
	leaq	.LC1(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
	.cfi_endproc
.LFE24828:
	.size	_ZN2v88internal24ConcurrentMarkingVisitor21VisitJSObjectSubclassINS0_8JSObjectENS3_14BodyDescriptorEEEiNS0_3MapET_, .-_ZN2v88internal24ConcurrentMarkingVisitor21VisitJSObjectSubclassINS0_8JSObjectENS3_14BodyDescriptorEEEiNS0_3MapET_
	.section	.text._ZN2v88internal24ConcurrentMarkingVisitor23VisitSharedFunctionInfoENS0_3MapENS0_18SharedFunctionInfoE.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal24ConcurrentMarkingVisitor23VisitSharedFunctionInfoENS0_3MapENS0_18SharedFunctionInfoE.constprop.0, @function
_ZN2v88internal24ConcurrentMarkingVisitor23VisitSharedFunctionInfoENS0_3MapENS0_18SharedFunctionInfoE.constprop.0:
.LFB29059:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	-1(%rdx), %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	andq	$-262144, %r13
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%r15d, %eax
	movq	%rdx, -72(%rbp)
	movl	$1, %edx
	subl	%r13d, %eax
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %edx
	movq	16(%r13), %rcx
	leaq	(%rcx,%rax,4), %rcx
	movl	(%rcx), %eax
	testl	%eax, %edx
	jne	.L1100
.L1103:
	xorl	%r8d, %r8d
.L1099:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1257
	addq	$136, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1100:
	.cfi_restore_state
	movq	%rdi, %r12
	movq	%rsi, %r8
	addl	%edx, %edx
	je	.L1258
	.p2align 4,,10
	.p2align 3
.L1104:
	movl	(%rcx), %esi
	movl	%esi, %eax
	andl	%edx, %eax
	cmpl	%eax, %edx
	je	.L1103
	movl	%esi, %edi
	movl	%esi, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rcx)
	cmpl	%eax, %esi
	jne	.L1104
	movq	-72(%rbp), %rax
	movq	%r8, -88(%rbp)
	leaq	-72(%rbp), %rdi
	movq	-1(%rax), %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	48(%r12), %rdi
	movq	%r13, -64(%rbp)
	movslq	%eax, %r14
	leaq	-64(%rbp), %rax
	movq	%rax, %rsi
	movq	%rax, -104(%rbp)
	call	_ZNSt8__detail9_Map_baseIPN2v88internal11MemoryChunkESt4pairIKS4_NS2_15MemoryChunkDataEESaIS8_ENS_10_Select1stESt8equal_toIS4_ENS3_6HasherENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS6_
	movq	-88(%rbp), %r8
	addq	%r14, (%rax)
	leaq	7(%rbx), %rax
	movzbl	7(%r8), %r8d
	movq	%rax, -88(%rbp)
	sall	$3, %r8d
	cmpq	%r15, %rax
	jbe	.L1169
	movq	-1(%rbx), %r14
	testb	$1, %r14b
	jne	.L1259
.L1169:
	leaq	15(%rbx), %rax
	leaq	39(%rbx), %r9
	movl	$1, %r10d
	movq	%rax, -96(%rbp)
	movq	%rax, %r14
	cmpq	%rax, %r9
	jbe	.L1138
	movq	%rbx, %rax
	movq	%r9, %rbx
	movq	%rax, %r9
	jmp	.L1120
	.p2align 4,,10
	.p2align 3
.L1123:
	addq	$8, %r14
	cmpq	%r14, %rbx
	jbe	.L1260
.L1120:
	movq	(%r14), %r15
	testb	$1, %r15b
	je	.L1123
	movl	%r15d, %eax
	movq	%r15, %rdx
	movl	%r10d, %edi
	andl	$262143, %eax
	andq	$-262144, %rdx
	movl	%eax, %ecx
	movq	16(%rdx), %rsi
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %edi
	movl	%edi, %ecx
	leaq	(%rsi,%rax,4), %rdi
	jmp	.L1125
	.p2align 4,,10
	.p2align 3
.L1262:
	movl	%ecx, %r11d
	movl	%esi, %eax
	orl	%esi, %r11d
	lock cmpxchgl	%r11d, (%rdi)
	cmpl	%eax, %esi
	je	.L1261
.L1125:
	movl	(%rdi), %esi
	movl	%ecx, %eax
	andl	%esi, %eax
	cmpl	%eax, %ecx
	jne	.L1262
.L1124:
	movq	8(%rdx), %rax
	testb	$64, %al
	je	.L1123
	movq	8(%r13), %rax
	testb	$88, %al
	je	.L1129
	testb	$-128, %ah
	je	.L1123
.L1129:
	movq	112(%r13), %r15
	testq	%r15, %r15
	je	.L1263
.L1132:
	movq	%r14, %rax
	movl	%r14d, %ecx
	subq	%r13, %rax
	andl	$262143, %ecx
	shrq	$18, %rax
	movl	%ecx, %edx
	movl	%ecx, %r11d
	sarl	$13, %ecx
	leaq	(%rax,%rax,2), %rax
	sarl	$8, %edx
	movslq	%ecx, %rcx
	sarl	$3, %r11d
	salq	$7, %rax
	andl	$31, %edx
	andl	$31, %r11d
	leaq	(%rax,%rcx,8), %rax
	addq	%rax, %r15
	movq	(%r15), %rsi
	testq	%rsi, %rsi
	je	.L1264
.L1134:
	movl	%r11d, %ecx
	movl	%r10d, %eax
	movslq	%edx, %rdx
	sall	%cl, %eax
	leaq	(%rsi,%rdx,4), %rsi
	movl	%eax, %ecx
	movl	(%rsi), %eax
	testl	%eax, %ecx
	je	.L1137
	addq	$8, %r14
	cmpq	%r14, %rbx
	ja	.L1120
	.p2align 4,,10
	.p2align 3
.L1260:
	movq	%r9, %rbx
.L1138:
	movl	4172(%r12), %edx
	testl	%edx, %edx
	jne	.L1265
.L1140:
	movq	-96(%rbp), %rdi
	cmpq	%rdi, -88(%rbp)
	jnb	.L1099
	movq	7(%rbx), %rbx
	testb	$1, %bl
	je	.L1099
	movl	%ebx, %eax
	movq	%rbx, %r14
	movl	$1, %edx
	andl	$262143, %eax
	andq	$-262144, %r14
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %edx
	movq	16(%r14), %rcx
	leaq	(%rcx,%rax,4), %rsi
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1267:
	movl	%edx, %edi
	movl	%ecx, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %ecx
	je	.L1266
.L1150:
	movl	(%rsi), %ecx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%eax, %edx
	jne	.L1267
.L1149:
	movq	8(%r14), %rax
	testb	$64, %al
	je	.L1099
	movq	8(%r13), %rax
	testb	$88, %al
	jne	.L1268
.L1154:
	movq	112(%r13), %rbx
	testq	%rbx, %rbx
	je	.L1269
.L1157:
	movq	-88(%rbp), %rax
	movq	%rax, %rcx
	movl	%eax, %edx
	subq	%r13, %rcx
	andl	$262143, %edx
	shrq	$18, %rcx
	movl	%edx, %r12d
	movl	%edx, %r13d
	sarl	$13, %edx
	leaq	(%rcx,%rcx,2), %rcx
	sarl	$8, %r12d
	movslq	%edx, %rdx
	sarl	$3, %r13d
	salq	$7, %rcx
	andl	$31, %r12d
	andl	$31, %r13d
	leaq	(%rcx,%rdx,8), %rax
	addq	%rax, %rbx
	movq	(%rbx), %r9
	testq	%r9, %r9
	je	.L1270
.L1159:
	movl	%r13d, %ecx
	movl	$1, %eax
	movslq	%r12d, %r12
	sall	%cl, %eax
	leaq	(%r9,%r12,4), %rsi
	movl	%eax, %ecx
	movl	(%rsi), %eax
	testl	%eax, %ecx
	jne	.L1099
	jmp	.L1161
	.p2align 4,,10
	.p2align 3
.L1271:
	movl	%ecx, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	je	.L1099
.L1161:
	movl	(%rsi), %edx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ecx
	jne	.L1271
	jmp	.L1099
	.p2align 4,,10
	.p2align 3
.L1272:
	movl	%ecx, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	je	.L1123
.L1137:
	movl	(%rsi), %edx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ecx
	jne	.L1272
	jmp	.L1123
	.p2align 4,,10
	.p2align 3
.L1259:
	movl	%r14d, %eax
	movq	%r14, %rdx
	movl	$1, %esi
	andl	$262143, %eax
	andq	$-262144, %rdx
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %esi
	movl	%esi, %ecx
	movq	16(%rdx), %rsi
	leaq	(%rsi,%rax,4), %rdi
	jmp	.L1108
	.p2align 4,,10
	.p2align 3
.L1274:
	movl	%ecx, %r9d
	movl	%esi, %eax
	orl	%esi, %r9d
	lock cmpxchgl	%r9d, (%rdi)
	cmpl	%eax, %esi
	je	.L1273
.L1108:
	movl	(%rdi), %esi
	movl	%ecx, %eax
	andl	%esi, %eax
	cmpl	%eax, %ecx
	jne	.L1274
.L1107:
	movq	8(%rdx), %rax
	testb	$64, %al
	je	.L1169
	movq	8(%r13), %rax
	testb	$88, %al
	je	.L1112
	testb	$-128, %ah
	je	.L1169
.L1112:
	movq	112(%r13), %rax
	testq	%rax, %rax
	je	.L1275
.L1115:
	movq	%r15, %rcx
	andl	$262143, %r15d
	subq	%r13, %rcx
	movl	%r15d, %r14d
	movl	%r15d, %edx
	sarl	$13, %r15d
	shrq	$18, %rcx
	movslq	%r15d, %r15
	sarl	$8, %r14d
	leaq	(%rcx,%rcx,2), %rcx
	sarl	$3, %edx
	andl	$31, %r14d
	salq	$7, %rcx
	andl	$31, %edx
	leaq	(%rcx,%r15,8), %r15
	addq	%rax, %r15
	movq	(%r15), %r9
	testq	%r9, %r9
	je	.L1276
.L1117:
	movl	%edx, %ecx
	movl	$1, %eax
	movslq	%r14d, %r14
	sall	%cl, %eax
	leaq	(%r9,%r14,4), %rsi
	movl	%eax, %ecx
	movl	(%rsi), %eax
	testl	%eax, %ecx
	jne	.L1169
	movl	(%rsi), %edx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ecx
	je	.L1169
	.p2align 4,,10
	.p2align 3
.L1277:
	movl	%ecx, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	je	.L1169
	movl	(%rsi), %edx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ecx
	jne	.L1277
	jmp	.L1169
	.p2align 4,,10
	.p2align 3
.L1273:
	movslq	16(%r12), %rax
	movq	8(%r12), %r9
	leaq	(%rax,%rax,4), %rsi
	salq	$4, %rsi
	addq	%r9, %rsi
	movq	(%rsi), %rax
	movq	8(%rax), %rcx
	cmpq	$64, %rcx
	je	.L1278
	leaq	1(%rcx), %rsi
	movq	%rsi, 8(%rax)
	movq	%r14, 16(%rax,%rcx,8)
	jmp	.L1107
	.p2align 4,,10
	.p2align 3
.L1261:
	movslq	16(%r12), %rax
	movq	8(%r12), %r11
	leaq	(%rax,%rax,4), %rsi
	salq	$4, %rsi
	addq	%r11, %rsi
	movq	(%rsi), %rax
	movq	8(%rax), %rcx
	cmpq	$64, %rcx
	je	.L1279
	leaq	1(%rcx), %rsi
	movq	%rsi, 8(%rax)
	movq	%r15, 16(%rax,%rcx,8)
	jmp	.L1124
	.p2align 4,,10
	.p2align 3
.L1265:
	movl	47(%rbx), %eax
	andl	$31, %eax
	leal	-9(%rax), %ecx
	cmpb	$6, %cl
	jbe	.L1140
	cmpb	$1, %al
	je	.L1140
	movl	47(%rbx), %eax
	testb	$16, %ah
	je	.L1140
	movq	7(%rbx), %rax
	testb	$1, %al
	je	.L1140
	movq	-1(%rax), %rcx
	cmpw	$72, 11(%rcx)
	jne	.L1140
	cmpl	$2, %edx
	je	.L1143
	movq	-104(%rbp), %rdi
	movl	%r8d, -112(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal13BytecodeArray5IsOldEv@PLT
	movl	-112(%rbp), %r8d
	testb	%al, %al
	je	.L1140
.L1143:
	movslq	64(%r12), %rax
	movq	24(%r12), %r15
	leaq	(%rax,%rax,4), %r12
	salq	$4, %r12
	addq	%r15, %r12
	movq	6264(%r12), %r14
	movq	8(%r14), %r13
	cmpq	$64, %r13
	je	.L1280
	leaq	1(%r13), %rax
	movq	%rax, 8(%r14)
	movq	%rbx, 16(%r14,%r13,8)
	jmp	.L1099
	.p2align 4,,10
	.p2align 3
.L1266:
	movslq	16(%r12), %rax
	movq	8(%r12), %rdx
	leaq	(%rax,%rax,4), %r12
	salq	$4, %r12
	addq	%rdx, %r12
	movq	(%r12), %r15
	movq	8(%r15), %rcx
	cmpq	$64, %rcx
	je	.L1281
	leaq	1(%rcx), %rax
	movq	%rax, 8(%r15)
	movq	%rbx, 16(%r15,%rcx,8)
	jmp	.L1149
	.p2align 4,,10
	.p2align 3
.L1258:
	addq	$4, %rcx
	movl	$1, %edx
	jmp	.L1104
	.p2align 4,,10
	.p2align 3
.L1268:
	testb	$-128, %ah
	je	.L1099
	jmp	.L1154
	.p2align 4,,10
	.p2align 3
.L1264:
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	movq	%r9, -136(%rbp)
	movl	%r8d, -128(%rbp)
	movl	%r11d, -120(%rbp)
	movl	%edx, -112(%rbp)
	call	_ZnamRKSt9nothrow_t@PLT
	movl	-112(%rbp), %edx
	movl	-120(%rbp), %r11d
	movl	$1, %r10d
	testq	%rax, %rax
	movl	-128(%rbp), %r8d
	movq	-136(%rbp), %r9
	movq	%rax, %rsi
	je	.L1282
.L1135:
	leaq	8(%rsi), %rdi
	movq	%rsi, %rcx
	xorl	%eax, %eax
	movq	$0, (%rsi)
	andq	$-8, %rdi
	movq	$0, 120(%rsi)
	subq	%rdi, %rcx
	subl	$-128, %ecx
	shrl	$3, %ecx
	rep stosq
	lock cmpxchgq	%rsi, (%r15)
	testq	%rax, %rax
	je	.L1134
	movq	%rsi, %rdi
	movq	%r9, -136(%rbp)
	movl	%r8d, -128(%rbp)
	movl	%r11d, -120(%rbp)
	movl	%edx, -112(%rbp)
	call	_ZdaPv@PLT
	movq	(%r15), %rsi
	movl	$1, %r10d
	movq	-136(%rbp), %r9
	movl	-128(%rbp), %r8d
	movl	-120(%rbp), %r11d
	movl	-112(%rbp), %edx
	jmp	.L1134
	.p2align 4,,10
	.p2align 3
.L1263:
	movq	%r13, %rdi
	movq	%r9, -120(%rbp)
	movl	%r8d, -112(%rbp)
	call	_ZN2v88internal11MemoryChunk15AllocateSlotSetILNS0_17RememberedSetTypeE1EEEPNS0_7SlotSetEv@PLT
	movq	-120(%rbp), %r9
	movl	-112(%rbp), %r8d
	movl	$1, %r10d
	movq	%rax, %r15
	jmp	.L1132
	.p2align 4,,10
	.p2align 3
.L1276:
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	movl	%r8d, -112(%rbp)
	movl	%edx, -96(%rbp)
	call	_ZnamRKSt9nothrow_t@PLT
	movl	-96(%rbp), %edx
	movl	-112(%rbp), %r8d
	testq	%rax, %rax
	movq	%rax, %r9
	je	.L1283
.L1118:
	leaq	8(%r9), %rdi
	movq	%r9, %rcx
	xorl	%eax, %eax
	movq	$0, (%r9)
	andq	$-8, %rdi
	movq	$0, 120(%r9)
	subq	%rdi, %rcx
	subl	$-128, %ecx
	shrl	$3, %ecx
	rep stosq
	lock cmpxchgq	%r9, (%r15)
	testq	%rax, %rax
	je	.L1117
	movq	%r9, %rdi
	movl	%r8d, -112(%rbp)
	movl	%edx, -96(%rbp)
	call	_ZdaPv@PLT
	movq	(%r15), %r9
	movl	-112(%rbp), %r8d
	movl	-96(%rbp), %edx
	jmp	.L1117
	.p2align 4,,10
	.p2align 3
.L1275:
	movq	%r13, %rdi
	movl	%r8d, -96(%rbp)
	call	_ZN2v88internal11MemoryChunk15AllocateSlotSetILNS0_17RememberedSetTypeE1EEEPNS0_7SlotSetEv@PLT
	movl	-96(%rbp), %r8d
	jmp	.L1115
	.p2align 4,,10
	.p2align 3
.L1279:
	leaq	640(%r11), %rdi
	movq	%r9, -168(%rbp)
	movl	%r8d, -156(%rbp)
	movq	%rsi, -152(%rbp)
	movq	%rcx, -144(%rbp)
	movq	%rdx, -128(%rbp)
	movq	%rax, -136(%rbp)
	movq	%r11, -120(%rbp)
	movq	%rdi, -112(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-120(%rbp), %r11
	movq	-136(%rbp), %rax
	movq	680(%r11), %rdi
	movq	%rdi, (%rax)
	movq	%rax, 680(%r11)
	movq	-112(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movq	-144(%rbp), %rcx
	movq	-128(%rbp), %rdx
	movl	$1, %r10d
	movq	$0, 8(%rax)
	movq	%rax, %r11
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	-152(%rbp), %rsi
	movl	-156(%rbp), %r8d
	movq	-168(%rbp), %r9
	movq	%r11, (%rsi)
	movq	8(%r11), %rax
	cmpq	$64, %rax
	je	.L1124
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%r11)
	movq	%r15, 16(%r11,%rax,8)
	jmp	.L1124
	.p2align 4,,10
	.p2align 3
.L1278:
	leaq	640(%r9), %rdi
	movl	%r8d, -152(%rbp)
	movq	%rsi, -144(%rbp)
	movq	%rcx, -136(%rbp)
	movq	%rdx, -120(%rbp)
	movq	%rax, -128(%rbp)
	movq	%r9, -112(%rbp)
	movq	%rdi, -96(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-112(%rbp), %r9
	movq	-128(%rbp), %rax
	movq	680(%r9), %r10
	movq	%r10, (%rax)
	movq	%rax, 680(%r9)
	movq	-96(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movq	-136(%rbp), %rcx
	movq	-120(%rbp), %rdx
	movq	$0, 8(%rax)
	movq	%rax, %r9
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	-144(%rbp), %rsi
	movl	-152(%rbp), %r8d
	movq	%r9, (%rsi)
	movq	8(%r9), %rax
	cmpq	$64, %rax
	je	.L1107
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%r9)
	movq	%r14, 16(%r9,%rax,8)
	jmp	.L1107
	.p2align 4,,10
	.p2align 3
.L1270:
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	movl	%r8d, -88(%rbp)
	call	_ZnamRKSt9nothrow_t@PLT
	movl	-88(%rbp), %r8d
	testq	%rax, %rax
	movq	%rax, %r9
	je	.L1284
.L1160:
	leaq	8(%r9), %rdi
	movq	%r9, %rcx
	xorl	%eax, %eax
	movq	$0, (%r9)
	andq	$-8, %rdi
	movq	$0, 120(%r9)
	subq	%rdi, %rcx
	subl	$-128, %ecx
	shrl	$3, %ecx
	rep stosq
	lock cmpxchgq	%r9, (%rbx)
	testq	%rax, %rax
	je	.L1159
	movq	%r9, %rdi
	movl	%r8d, -88(%rbp)
	call	_ZdaPv@PLT
	movq	(%rbx), %r9
	movl	-88(%rbp), %r8d
	jmp	.L1159
	.p2align 4,,10
	.p2align 3
.L1269:
	movq	%r13, %rdi
	movl	%r8d, -96(%rbp)
	call	_ZN2v88internal11MemoryChunk15AllocateSlotSetILNS0_17RememberedSetTypeE1EEEPNS0_7SlotSetEv@PLT
	movl	-96(%rbp), %r8d
	movq	%rax, %rbx
	jmp	.L1157
	.p2align 4,,10
	.p2align 3
.L1281:
	leaq	640(%rdx), %rdi
	movl	%r8d, -120(%rbp)
	movq	%rcx, -112(%rbp)
	movq	%rdx, -104(%rbp)
	movq	%rdi, -96(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-104(%rbp), %rdx
	movq	680(%rdx), %rax
	movq	%rax, (%r15)
	movq	%r15, 680(%rdx)
	movq	-96(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movq	-112(%rbp), %rcx
	movl	-120(%rbp), %r8d
	movq	$0, 8(%rax)
	movq	%rax, %rdx
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rdx, (%r12)
	movq	8(%rdx), %rax
	cmpq	$64, %rax
	je	.L1149
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdx)
	movq	%rbx, 16(%rdx,%rax,8)
	jmp	.L1149
	.p2align 4,,10
	.p2align 3
.L1280:
	leaq	6904(%r15), %rdi
	movl	%r8d, -96(%rbp)
	movq	%rdi, -88(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	6944(%r15), %rax
	movq	%rax, (%r14)
	movq	%r14, 6944(%r15)
	movq	-88(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movq	%r13, %rcx
	movl	-96(%rbp), %r8d
	movq	$0, 8(%rax)
	movq	%rax, %rdx
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rdx, 6264(%r12)
	movq	8(%rdx), %rax
	cmpq	$64, %rax
	je	.L1099
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdx)
	movq	%rbx, 16(%rdx,%rax,8)
	jmp	.L1099
.L1257:
	call	__stack_chk_fail@PLT
.L1284:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movl	-88(%rbp), %r8d
	testq	%rax, %rax
	movq	%rax, %r9
	jne	.L1160
.L1136:
	leaq	.LC1(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
.L1283:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movl	-96(%rbp), %edx
	movl	-112(%rbp), %r8d
	testq	%rax, %rax
	movq	%rax, %r9
	jne	.L1118
	jmp	.L1136
.L1282:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movl	-112(%rbp), %edx
	movl	-120(%rbp), %r11d
	movl	$1, %r10d
	testq	%rax, %rax
	movl	-128(%rbp), %r8d
	movq	-136(%rbp), %r9
	movq	%rax, %rsi
	jne	.L1135
	jmp	.L1136
	.cfi_endproc
.LFE29059:
	.size	_ZN2v88internal24ConcurrentMarkingVisitor23VisitSharedFunctionInfoENS0_3MapENS0_18SharedFunctionInfoE.constprop.0, .-_ZN2v88internal24ConcurrentMarkingVisitor23VisitSharedFunctionInfoENS0_3MapENS0_18SharedFunctionInfoE.constprop.0
	.section	.text._ZN2v88internal24ConcurrentMarkingVisitor14VisitJSWeakRefENS0_3MapENS0_9JSWeakRefE,"axG",@progbits,_ZN2v88internal24ConcurrentMarkingVisitor14VisitJSWeakRefENS0_3MapENS0_9JSWeakRefE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal24ConcurrentMarkingVisitor14VisitJSWeakRefENS0_3MapENS0_9JSWeakRefE
	.type	_ZN2v88internal24ConcurrentMarkingVisitor14VisitJSWeakRefENS0_3MapENS0_9JSWeakRefE, @function
_ZN2v88internal24ConcurrentMarkingVisitor14VisitJSWeakRefENS0_3MapENS0_9JSWeakRefE:
.LFB22246:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	7(%rsi), %eax
	movb	%al, -112(%rbp)
	movzbl	9(%rsi), %eax
	cmpl	$2, %eax
	jle	.L1405
	leal	0(,%rax,8), %r14d
.L1287:
	leaq	-1(%r12), %r8
	leaq	7(%r12), %rax
	movl	$0, 72(%rbx)
	leaq	72(%rbx), %r9
	cmpq	%rax, %r8
	jnb	.L1288
	movq	-1(%r12), %rdx
	movq	%r8, %xmm0
	movl	$1, 72(%rbx)
	movq	%rdx, %xmm4
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, 80(%rbx)
.L1288:
	leaq	23(%r12), %r15
	cmpq	%r15, %rax
	jnb	.L1293
	leaq	15(%r12), %rcx
	cmpq	%rcx, %r15
	sbbq	%rdx, %rdx
	notq	%rdx
	andl	$8, %edx
	addq	%rdx, %rcx
	.p2align 4,,10
	.p2align 3
.L1292:
	movq	(%rax), %rdi
	movslq	72(%rbx), %rdx
	movq	%rax, %xmm0
	addq	$8, %rax
	movq	%rdi, %xmm1
	leal	1(%rdx), %r10d
	salq	$4, %rdx
	punpcklqdq	%xmm1, %xmm0
	movl	%r10d, 72(%rbx)
	movups	%xmm0, 8(%r9,%rdx)
	cmpq	%rcx, %rax
	jne	.L1292
.L1293:
	movq	47(%rsi), %rax
	testq	%rax, %rax
	jne	.L1406
	movslq	%r14d, %r14
	leaq	31(%r12), %rdx
	addq	%r8, %r14
	cmpq	%rdx, %r14
	jbe	.L1295
	.p2align 4,,10
	.p2align 3
.L1294:
	movq	(%rdx), %rcx
	movslq	72(%rbx), %rax
	movq	%rdx, %xmm0
	addq	$8, %rdx
	movq	%rcx, %xmm2
	leal	1(%rax), %esi
	salq	$4, %rax
	punpcklqdq	%xmm2, %xmm0
	movl	%esi, 72(%rbx)
	movups	%xmm0, 8(%r9,%rax)
	cmpq	%rdx, %r14
	ja	.L1294
.L1295:
	movq	%r12, %r14
	movl	$1, %edx
	movq	%r12, -88(%rbp)
	andq	$-262144, %r14
	subl	%r14d, %r8d
	movq	16(%r14), %rax
	movl	%r8d, %ecx
	shrl	$8, %r8d
	shrl	$3, %ecx
	sall	%cl, %edx
	leaq	(%rax,%r8,4), %rcx
	movl	(%rcx), %eax
	testl	%eax, %edx
	jne	.L1304
.L1307:
	xorl	%r13d, %r13d
.L1285:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1407
	addq	$136, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1406:
	.cfi_restore_state
	movq	$0, -72(%rbp)
	movb	$1, -80(%rbp)
	movl	$0, -76(%rbp)
	movq	47(%rsi), %rax
	movq	%rax, -72(%rbp)
	testq	%rax, %rax
	jne	.L1408
.L1297:
	movl	$32, %r13d
	leaq	-88(%rbp), %rcx
	leaq	-80(%rbp), %rdi
	cmpl	$32, %r14d
	jle	.L1295
	movq	%r15, -128(%rbp)
	movq	%rbx, %r15
	movl	%r14d, %ebx
	movq	%rcx, %r14
	movq	%r12, -136(%rbp)
	movq	%r8, %r12
	movq	%r9, -120(%rbp)
	jmp	.L1298
	.p2align 4,,10
	.p2align 3
.L1410:
	movslq	-88(%rbp), %r13
	cmpl	%r13d, %ebx
	jle	.L1409
.L1298:
	movq	%r14, %rcx
	movl	%ebx, %edx
	movl	%r13d, %esi
	movq	%rdi, -104(%rbp)
	call	_ZN2v88internal22LayoutDescriptorHelper8IsTaggedEiiPi@PLT
	movq	-104(%rbp), %rdi
	testb	%al, %al
	je	.L1410
	movslq	-88(%rbp), %rdx
	addq	%r12, %r13
	movq	%rdx, %rsi
	addq	%r12, %rdx
	cmpq	%r13, %rdx
	jbe	.L1342
	.p2align 4,,10
	.p2align 3
.L1302:
	movq	0(%r13), %rcx
	movslq	72(%r15), %rax
	movq	%r13, %xmm0
	addq	$8, %r13
	movq	%rcx, %xmm3
	movq	-120(%rbp), %rcx
	leal	1(%rax), %r8d
	salq	$4, %rax
	punpcklqdq	%xmm3, %xmm0
	movl	%r8d, 72(%r15)
	movups	%xmm0, 8(%rcx,%rax)
	cmpq	%r13, %rdx
	ja	.L1302
.L1342:
	movslq	%esi, %r13
	cmpl	%r13d, %ebx
	jg	.L1298
.L1409:
	movq	%r12, %r8
	movq	%r15, %rbx
	movq	-136(%rbp), %r12
	movq	-128(%rbp), %r15
	jmp	.L1295
	.p2align 4,,10
	.p2align 3
.L1408:
	movzbl	8(%rsi), %eax
	movb	$0, -80(%rbp)
	sall	$3, %eax
	movl	%eax, -76(%rbp)
	jmp	.L1297
	.p2align 4,,10
	.p2align 3
.L1304:
	addl	%edx, %edx
	je	.L1411
	.p2align 4,,10
	.p2align 3
.L1308:
	movl	(%rcx), %esi
	movl	%esi, %eax
	andl	%edx, %eax
	cmpl	%eax, %edx
	je	.L1307
	movl	%esi, %edi
	movl	%esi, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rcx)
	cmpl	%eax, %esi
	jne	.L1308
	movq	-88(%rbp), %rax
	leaq	-88(%rbp), %rdi
	movq	-1(%rax), %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	48(%rbx), %rdi
	leaq	-80(%rbp), %rsi
	movq	%r14, -80(%rbp)
	movslq	%eax, %r13
	call	_ZNSt8__detail9_Map_baseIPN2v88internal11MemoryChunkESt4pairIKS4_NS2_15MemoryChunkDataEESaIS8_ENS_10_Select1stESt8equal_toIS4_ENS3_6HasherENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS6_
	xorl	%r8d, %r8d
	movl	$1, %r11d
	addq	%r13, (%rax)
	movl	72(%rbx), %eax
	testl	%eax, %eax
	jle	.L1339
	movq	%r12, -104(%rbp)
	movq	%r14, %r12
	movq	%r8, %r14
	.p2align 4,,10
	.p2align 3
.L1324:
	movq	%r14, %rcx
	salq	$4, %rcx
	movq	88(%rbx,%rcx), %rdx
	testb	$1, %dl
	je	.L1310
	movl	%edx, %eax
	movq	%rdx, %r8
	movq	80(%rbx,%rcx), %r13
	movl	%r11d, %edi
	andl	$262143, %eax
	andq	$-262144, %r8
	movl	%eax, %ecx
	movq	16(%r8), %rsi
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %edi
	movl	%edi, %ecx
	leaq	(%rsi,%rax,4), %rdi
	jmp	.L1312
	.p2align 4,,10
	.p2align 3
.L1413:
	movl	%ecx, %r9d
	movl	%esi, %eax
	orl	%esi, %r9d
	lock cmpxchgl	%r9d, (%rdi)
	cmpl	%eax, %esi
	je	.L1412
.L1312:
	movl	(%rdi), %esi
	movl	%ecx, %eax
	andl	%esi, %eax
	cmpl	%eax, %ecx
	jne	.L1413
.L1311:
	movq	8(%r8), %rax
	testb	$64, %al
	jne	.L1315
.L1403:
	movl	72(%rbx), %eax
.L1310:
	addq	$1, %r14
	cmpl	%r14d, %eax
	jg	.L1324
	movq	%r12, %r14
	movq	-104(%rbp), %r12
.L1339:
	movzbl	-112(%rbp), %r13d
	sall	$3, %r13d
	je	.L1285
	movq	23(%r12), %rax
	testb	$1, %al
	je	.L1285
	movq	%rax, %rcx
	andl	$262143, %eax
	andq	$-262144, %rcx
	movl	%eax, %edx
	shrl	$3, %eax
	movq	16(%rcx), %rsi
	shrl	$8, %edx
	movl	(%rsi,%rdx,4), %edx
	btl	%eax, %edx
	jc	.L1414
	movslq	64(%rbx), %rax
	movq	24(%rbx), %rdx
	leaq	(%rax,%rax,4), %rbx
	salq	$4, %rbx
	addq	%rdx, %rbx
	movq	4872(%rbx), %r15
	movq	8(%r15), %r14
	cmpq	$64, %r14
	je	.L1415
	leaq	1(%r14), %rax
	movq	%rax, 8(%r15)
	movq	%r12, 16(%r15,%r14,8)
	jmp	.L1285
	.p2align 4,,10
	.p2align 3
.L1315:
	movq	8(%r12), %rax
	testb	$88, %al
	je	.L1316
	testb	$-128, %ah
	je	.L1403
.L1316:
	movq	112(%r12), %rax
	testq	%rax, %rax
	je	.L1416
.L1318:
	movq	%r13, %rcx
	andl	$262143, %r13d
	subq	%r12, %rcx
	movl	%r13d, %edx
	movl	%r13d, %r8d
	sarl	$13, %r13d
	shrq	$18, %rcx
	movslq	%r13d, %r13
	sarl	$8, %edx
	leaq	(%rcx,%rcx,2), %rcx
	sarl	$3, %r8d
	andl	$31, %edx
	salq	$7, %rcx
	andl	$31, %r8d
	leaq	(%rcx,%r13,8), %r13
	addq	%rax, %r13
	movq	0(%r13), %r10
	testq	%r10, %r10
	je	.L1417
.L1320:
	movl	%r8d, %ecx
	movl	%r11d, %eax
	movslq	%edx, %rdx
	sall	%cl, %eax
	leaq	(%r10,%rdx,4), %rsi
	movl	%eax, %ecx
	movl	(%rsi), %eax
	testl	%eax, %ecx
	jne	.L1403
	movl	(%rsi), %edx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ecx
	je	.L1403
	.p2align 4,,10
	.p2align 3
.L1323:
	movl	%ecx, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	je	.L1403
	movl	(%rsi), %edx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ecx
	jne	.L1323
	jmp	.L1403
	.p2align 4,,10
	.p2align 3
.L1412:
	movslq	16(%rbx), %rax
	movq	8(%rbx), %r10
	leaq	(%rax,%rax,4), %rsi
	salq	$4, %rsi
	addq	%r10, %rsi
	movq	(%rsi), %rax
	movq	8(%rax), %rcx
	cmpq	$64, %rcx
	je	.L1418
	leaq	1(%rcx), %rsi
	movq	%rsi, 8(%rax)
	movq	%rdx, 16(%rax,%rcx,8)
	jmp	.L1311
	.p2align 4,,10
	.p2align 3
.L1405:
	movzbl	7(%rsi), %r14d
	sall	$3, %r14d
	jmp	.L1287
	.p2align 4,,10
	.p2align 3
.L1411:
	addq	$4, %rcx
	movl	$1, %edx
	jmp	.L1308
	.p2align 4,,10
	.p2align 3
.L1414:
	movq	8(%rcx), %rax
	testb	$64, %al
	je	.L1285
	movq	8(%r14), %rax
	testb	$88, %al
	je	.L1326
	testb	$-128, %ah
	je	.L1285
.L1326:
	movq	112(%r14), %rbx
	testq	%rbx, %rbx
	je	.L1419
.L1328:
	movq	%r15, %rdx
	andl	$262143, %r15d
	subq	%r14, %rdx
	movl	%r15d, %r12d
	movl	%r15d, %r14d
	sarl	$13, %r15d
	shrq	$18, %rdx
	movslq	%r15d, %r15
	sarl	$8, %r12d
	leaq	(%rdx,%rdx,2), %rdx
	sarl	$3, %r14d
	andl	$31, %r12d
	salq	$7, %rdx
	andl	$31, %r14d
	leaq	(%rdx,%r15,8), %rax
	addq	%rax, %rbx
	movq	(%rbx), %r8
	testq	%r8, %r8
	je	.L1420
.L1330:
	movl	%r14d, %ecx
	movl	$1, %eax
	movslq	%r12d, %r12
	sall	%cl, %eax
	leaq	(%r8,%r12,4), %rsi
	movl	%eax, %ecx
	movl	(%rsi), %eax
	testl	%eax, %ecx
	jne	.L1285
	jmp	.L1333
	.p2align 4,,10
	.p2align 3
.L1421:
	movl	%ecx, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	je	.L1285
.L1333:
	movl	(%rsi), %edx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ecx
	jne	.L1421
	jmp	.L1285
	.p2align 4,,10
	.p2align 3
.L1417:
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	movl	%r8d, -128(%rbp)
	movl	%edx, -120(%rbp)
	call	_ZnamRKSt9nothrow_t@PLT
	movl	-120(%rbp), %edx
	movl	-128(%rbp), %r8d
	movl	$1, %r11d
	testq	%rax, %rax
	movq	%rax, %r10
	je	.L1422
.L1321:
	leaq	8(%r10), %rdi
	movq	%r10, %rcx
	xorl	%eax, %eax
	movq	$0, (%r10)
	andq	$-8, %rdi
	movq	$0, 120(%r10)
	subq	%rdi, %rcx
	subl	$-128, %ecx
	shrl	$3, %ecx
	rep stosq
	lock cmpxchgq	%r10, 0(%r13)
	testq	%rax, %rax
	je	.L1320
	movq	%r10, %rdi
	movl	%r8d, -128(%rbp)
	movl	%edx, -120(%rbp)
	call	_ZdaPv@PLT
	movq	0(%r13), %r10
	movl	$1, %r11d
	movl	-128(%rbp), %r8d
	movl	-120(%rbp), %edx
	jmp	.L1320
	.p2align 4,,10
	.p2align 3
.L1416:
	movq	%r12, %rdi
	call	_ZN2v88internal11MemoryChunk15AllocateSlotSetILNS0_17RememberedSetTypeE1EEEPNS0_7SlotSetEv@PLT
	movl	$1, %r11d
	jmp	.L1318
	.p2align 4,,10
	.p2align 3
.L1418:
	leaq	640(%r10), %rdi
	movq	%rcx, -168(%rbp)
	movq	%r8, -152(%rbp)
	movq	%rdx, -144(%rbp)
	movq	%rsi, -136(%rbp)
	movq	%rax, -160(%rbp)
	movq	%r10, -128(%rbp)
	movq	%rdi, -120(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-128(%rbp), %r10
	movq	-160(%rbp), %rax
	movq	680(%r10), %rdi
	movq	%rdi, (%rax)
	movq	%rax, 680(%r10)
	movq	-120(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movq	-168(%rbp), %rcx
	movq	-136(%rbp), %rsi
	movl	$1, %r11d
	movq	$0, 8(%rax)
	movq	%rax, %r10
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%r10, (%rsi)
	movq	-144(%rbp), %rdx
	movq	-152(%rbp), %r8
	movq	8(%r10), %rax
	cmpq	$64, %rax
	je	.L1311
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%r10)
	movq	%rdx, 16(%r10,%rax,8)
	jmp	.L1311
	.p2align 4,,10
	.p2align 3
.L1415:
	leaq	5512(%rdx), %rdi
	movq	%rdx, -112(%rbp)
	movq	%rdi, -104(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-112(%rbp), %rdx
	movq	5552(%rdx), %rax
	movq	%rax, (%r15)
	movq	%r15, 5552(%rdx)
	movq	-104(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movq	%r14, %rcx
	movq	$0, 8(%rax)
	movq	%rax, %rdx
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rdx, 4872(%rbx)
	movq	8(%rdx), %rax
	cmpq	$64, %rax
	je	.L1285
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdx)
	movq	%r12, 16(%rdx,%rax,8)
	jmp	.L1285
.L1420:
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L1423
.L1331:
	leaq	8(%r8), %rdi
	movq	%r8, %rcx
	xorl	%eax, %eax
	movq	$0, (%r8)
	andq	$-8, %rdi
	movq	$0, 120(%r8)
	subq	%rdi, %rcx
	subl	$-128, %ecx
	shrl	$3, %ecx
	rep stosq
	lock cmpxchgq	%r8, (%rbx)
	testq	%rax, %rax
	je	.L1330
	movq	%r8, %rdi
	call	_ZdaPv@PLT
	movq	(%rbx), %r8
	jmp	.L1330
.L1419:
	movq	%r14, %rdi
	call	_ZN2v88internal11MemoryChunk15AllocateSlotSetILNS0_17RememberedSetTypeE1EEEPNS0_7SlotSetEv@PLT
	movq	%rax, %rbx
	jmp	.L1328
.L1407:
	call	__stack_chk_fail@PLT
.L1422:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movl	-120(%rbp), %edx
	movl	-128(%rbp), %r8d
	movl	$1, %r11d
	testq	%rax, %rax
	movq	%rax, %r10
	jne	.L1321
.L1332:
	leaq	.LC1(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
.L1423:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	jne	.L1331
	jmp	.L1332
	.cfi_endproc
.LFE22246:
	.size	_ZN2v88internal24ConcurrentMarkingVisitor14VisitJSWeakRefENS0_3MapENS0_9JSWeakRefE, .-_ZN2v88internal24ConcurrentMarkingVisitor14VisitJSWeakRefENS0_3MapENS0_9JSWeakRefE
	.section	.text._ZN2v88internal24ConcurrentMarkingVisitor13VisitWeakCellENS0_3MapENS0_8WeakCellE.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal24ConcurrentMarkingVisitor13VisitWeakCellENS0_3MapENS0_8WeakCellE.constprop.0, @function
_ZN2v88internal24ConcurrentMarkingVisitor13VisitWeakCellENS0_3MapENS0_8WeakCellE.constprop.0:
.LFB29061:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	-1(%rdx), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	andq	$-262144, %r12
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%r8d, %eax
	movq	%rdx, -72(%rbp)
	movl	$1, %edx
	subl	%r12d, %eax
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %edx
	movq	16(%r12), %rcx
	leaq	(%rcx,%rax,4), %rcx
	movl	(%rcx), %eax
	testl	%eax, %edx
	jne	.L1425
.L1428:
	xorl	%r15d, %r15d
.L1424:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1591
	addq	$120, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1425:
	.cfi_restore_state
	movq	%rdi, %r13
	movq	%rsi, %r15
	addl	%edx, %edx
	je	.L1592
	.p2align 4,,10
	.p2align 3
.L1429:
	movl	(%rcx), %esi
	movl	%esi, %eax
	andl	%edx, %eax
	cmpl	%eax, %edx
	je	.L1428
	movl	%esi, %edi
	movl	%esi, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rcx)
	cmpl	%eax, %esi
	jne	.L1429
	movq	-72(%rbp), %rax
	movq	%r8, -88(%rbp)
	leaq	-72(%rbp), %rdi
	movq	-1(%rax), %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	48(%r13), %rdi
	leaq	-64(%rbp), %rsi
	movq	%r12, -64(%rbp)
	movslq	%eax, %r14
	call	_ZNSt8__detail9_Map_baseIPN2v88internal11MemoryChunkESt4pairIKS4_NS2_15MemoryChunkDataEESaIS8_ENS_10_Select1stESt8equal_toIS4_ENS3_6HasherENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS6_
	addq	%r14, (%rax)
	movzbl	7(%r15), %r15d
	leaq	7(%rbx), %r14
	movq	-88(%rbp), %r8
	sall	$3, %r15d
	cmpq	%r8, %r14
	jbe	.L1496
	movq	-1(%rbx), %rdx
	testb	$1, %dl
	jne	.L1593
.L1496:
	leaq	15(%rbx), %rax
	movq	%rax, -88(%rbp)
	cmpq	%rax, %r14
	jnb	.L1445
	movq	7(%rbx), %rdx
	testb	$1, %dl
	jne	.L1594
.L1445:
	movslq	%r15d, %rax
	leaq	23(%rbx), %r14
	movl	$1, %r9d
	addq	%rax, %r8
	cmpq	%r14, %r8
	jbe	.L1477
	movq	%rbx, %rax
	movq	%r8, %rbx
	movq	%rax, %r8
	jmp	.L1461
	.p2align 4,,10
	.p2align 3
.L1463:
	addq	$8, %r14
	cmpq	%r14, %rbx
	jbe	.L1595
.L1461:
	movq	(%r14), %rdx
	testb	$1, %dl
	je	.L1463
	movl	%edx, %eax
	movq	%rdx, %rsi
	movl	%r9d, %edi
	andl	$262143, %eax
	andq	$-262144, %rsi
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %edi
	movl	%edi, %ecx
	movq	16(%rsi), %rdi
	leaq	(%rdi,%rax,4), %r10
	jmp	.L1465
	.p2align 4,,10
	.p2align 3
.L1597:
	movl	%ecx, %r11d
	movl	%edi, %eax
	orl	%edi, %r11d
	lock cmpxchgl	%r11d, (%r10)
	cmpl	%eax, %edi
	je	.L1596
.L1465:
	movl	(%r10), %edi
	movl	%ecx, %eax
	andl	%edi, %eax
	cmpl	%eax, %ecx
	jne	.L1597
.L1464:
	movq	8(%rsi), %rax
	testb	$64, %al
	je	.L1463
	movq	8(%r12), %rax
	testb	$88, %al
	je	.L1469
	testb	$-128, %ah
	je	.L1463
.L1469:
	movq	112(%r12), %rdx
	testq	%rdx, %rdx
	je	.L1598
.L1472:
	movq	%r14, %rax
	movl	%r14d, %ecx
	subq	%r12, %rax
	andl	$262143, %ecx
	shrq	$18, %rax
	movl	%ecx, %r10d
	movl	%ecx, %r11d
	sarl	$13, %ecx
	leaq	(%rax,%rax,2), %rax
	sarl	$8, %r10d
	movslq	%ecx, %rcx
	sarl	$3, %r11d
	salq	$7, %rax
	andl	$31, %r10d
	andl	$31, %r11d
	leaq	(%rax,%rcx,8), %rax
	addq	%rax, %rdx
	movq	(%rdx), %rsi
	testq	%rsi, %rsi
	je	.L1599
.L1474:
	movl	%r11d, %ecx
	movl	%r9d, %eax
	movslq	%r10d, %r10
	sall	%cl, %eax
	leaq	(%rsi,%r10,4), %rsi
	movl	%eax, %ecx
	movl	(%rsi), %eax
	testl	%eax, %ecx
	je	.L1476
	addq	$8, %r14
	cmpq	%r14, %rbx
	ja	.L1461
	.p2align 4,,10
	.p2align 3
.L1595:
	movq	%r8, %rbx
.L1477:
	movq	15(%rbx), %rax
	testb	$1, %al
	je	.L1424
	movq	%rax, %rcx
	andl	$262143, %eax
	andq	$-262144, %rcx
	movl	%eax, %edx
	shrl	$3, %eax
	movq	16(%rcx), %rsi
	shrl	$8, %edx
	movl	(%rsi,%rdx,4), %edx
	btl	%eax, %edx
	jnc	.L1478
	movq	8(%rcx), %rax
	testb	$64, %al
	je	.L1424
	movq	8(%r12), %rax
	testb	$88, %al
	je	.L1479
	testb	$-128, %ah
	je	.L1424
.L1479:
	movq	112(%r12), %rbx
	testq	%rbx, %rbx
	je	.L1600
.L1481:
	movq	-88(%rbp), %rax
	movq	%rax, %rdx
	andl	$262143, %eax
	subq	%r12, %rdx
	movl	%eax, %r13d
	movl	%eax, %r12d
	sarl	$13, %eax
	shrq	$18, %rdx
	cltq
	sarl	$8, %r12d
	leaq	(%rdx,%rdx,2), %rdx
	sarl	$3, %r13d
	andl	$31, %r12d
	salq	$7, %rdx
	andl	$31, %r13d
	leaq	(%rdx,%rax,8), %rax
	addq	%rax, %rbx
	movq	(%rbx), %r8
	testq	%r8, %r8
	je	.L1601
.L1483:
	movl	%r13d, %ecx
	movl	$1, %eax
	movslq	%r12d, %r12
	sall	%cl, %eax
	leaq	(%r8,%r12,4), %rsi
	movl	%eax, %ecx
	movl	(%rsi), %eax
	testl	%eax, %ecx
	jne	.L1424
	jmp	.L1485
	.p2align 4,,10
	.p2align 3
.L1602:
	movl	%ecx, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	je	.L1424
.L1485:
	movl	(%rsi), %edx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ecx
	jne	.L1602
	jmp	.L1424
	.p2align 4,,10
	.p2align 3
.L1603:
	movl	%ecx, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	je	.L1463
.L1476:
	movl	(%rsi), %edx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ecx
	jne	.L1603
	jmp	.L1463
	.p2align 4,,10
	.p2align 3
.L1593:
	movl	%edx, %eax
	movq	%rdx, %rsi
	movl	$1, %edi
	andl	$262143, %eax
	andq	$-262144, %rsi
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %edi
	movl	%edi, %ecx
	movq	16(%rsi), %rdi
	leaq	(%rdi,%rax,4), %r9
	jmp	.L1433
	.p2align 4,,10
	.p2align 3
.L1605:
	movl	%ecx, %r10d
	movl	%edi, %eax
	orl	%edi, %r10d
	lock cmpxchgl	%r10d, (%r9)
	cmpl	%eax, %edi
	je	.L1604
.L1433:
	movl	(%r9), %edi
	movl	%ecx, %eax
	andl	%edi, %eax
	cmpl	%eax, %ecx
	jne	.L1605
.L1432:
	movq	8(%rsi), %rax
	testb	$64, %al
	je	.L1496
	movq	8(%r12), %rax
	testb	$88, %al
	je	.L1437
	testb	$-128, %ah
	je	.L1496
.L1437:
	movq	112(%r12), %rax
	testq	%rax, %rax
	je	.L1606
.L1440:
	movq	%r8, %rcx
	movl	%r8d, %edx
	subq	%r12, %rcx
	andl	$262143, %edx
	shrq	$18, %rcx
	movl	%edx, %r9d
	movl	%edx, %r10d
	sarl	$13, %edx
	leaq	(%rcx,%rcx,2), %rcx
	sarl	$8, %r9d
	movslq	%edx, %rdx
	sarl	$3, %r10d
	salq	$7, %rcx
	andl	$31, %r9d
	andl	$31, %r10d
	leaq	(%rcx,%rdx,8), %rdx
	addq	%rax, %rdx
	movq	(%rdx), %r11
	testq	%r11, %r11
	je	.L1607
.L1442:
	movl	%r10d, %ecx
	movl	$1, %eax
	movslq	%r9d, %r9
	sall	%cl, %eax
	leaq	(%r11,%r9,4), %rsi
	movl	%eax, %ecx
	movl	(%rsi), %eax
	testl	%eax, %ecx
	jne	.L1496
	movl	(%rsi), %edx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ecx
	je	.L1496
	.p2align 4,,10
	.p2align 3
.L1608:
	movl	%ecx, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	je	.L1496
	movl	(%rsi), %edx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ecx
	jne	.L1608
	jmp	.L1496
	.p2align 4,,10
	.p2align 3
.L1594:
	movl	%edx, %eax
	movq	%rdx, %rsi
	movl	$1, %edi
	andl	$262143, %eax
	andq	$-262144, %rsi
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %edi
	movl	%edi, %ecx
	movq	16(%rsi), %rdi
	leaq	(%rdi,%rax,4), %r9
	jmp	.L1448
	.p2align 4,,10
	.p2align 3
.L1610:
	movl	%ecx, %r10d
	movl	%edi, %eax
	orl	%edi, %r10d
	lock cmpxchgl	%r10d, (%r9)
	cmpl	%eax, %edi
	je	.L1609
.L1448:
	movl	(%r9), %edi
	movl	%ecx, %eax
	andl	%edi, %eax
	cmpl	%eax, %ecx
	jne	.L1610
.L1447:
	movq	8(%rsi), %rax
	testb	$64, %al
	je	.L1445
	movq	8(%r12), %rax
	testb	$88, %al
	je	.L1452
	testb	$-128, %ah
	je	.L1445
.L1452:
	movq	112(%r12), %rax
	testq	%rax, %rax
	je	.L1611
.L1455:
	movq	%r14, %rcx
	andl	$262143, %r14d
	subq	%r12, %rcx
	movl	%r14d, %edx
	movl	%r14d, %r9d
	sarl	$13, %r14d
	shrq	$18, %rcx
	movslq	%r14d, %r14
	sarl	$8, %edx
	leaq	(%rcx,%rcx,2), %rcx
	sarl	$3, %r9d
	andl	$31, %edx
	salq	$7, %rcx
	andl	$31, %r9d
	leaq	(%rcx,%r14,8), %r14
	addq	%rax, %r14
	movq	(%r14), %r10
	testq	%r10, %r10
	je	.L1612
.L1457:
	movl	%r9d, %ecx
	movl	$1, %eax
	movslq	%edx, %rdx
	sall	%cl, %eax
	leaq	(%r10,%rdx,4), %rsi
	movl	%eax, %ecx
	movl	(%rsi), %eax
	testl	%eax, %ecx
	jne	.L1445
	movl	(%rsi), %edx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ecx
	je	.L1445
	.p2align 4,,10
	.p2align 3
.L1613:
	movl	%ecx, %edi
	movl	%edx, %eax
	orl	%edx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	je	.L1445
	movl	(%rsi), %edx
	movl	%ecx, %eax
	andl	%edx, %eax
	cmpl	%eax, %ecx
	jne	.L1613
	jmp	.L1445
	.p2align 4,,10
	.p2align 3
.L1596:
	movslq	16(%r13), %rax
	movq	8(%r13), %r11
	leaq	(%rax,%rax,4), %r10
	salq	$4, %r10
	addq	%r11, %r10
	movq	(%r10), %rax
	movq	8(%rax), %rcx
	cmpq	$64, %rcx
	je	.L1614
	leaq	1(%rcx), %rdi
	movq	%rdi, 8(%rax)
	movq	%rdx, 16(%rax,%rcx,8)
	jmp	.L1464
	.p2align 4,,10
	.p2align 3
.L1609:
	movslq	16(%r13), %rax
	movq	8(%r13), %r10
	leaq	(%rax,%rax,4), %r9
	salq	$4, %r9
	addq	%r10, %r9
	movq	(%r9), %rax
	movq	8(%rax), %rcx
	cmpq	$64, %rcx
	je	.L1615
	leaq	1(%rcx), %rdi
	movq	%rdi, 8(%rax)
	movq	%rdx, 16(%rax,%rcx,8)
	jmp	.L1447
	.p2align 4,,10
	.p2align 3
.L1604:
	movslq	16(%r13), %rax
	movq	8(%r13), %r10
	leaq	(%rax,%rax,4), %r9
	salq	$4, %r9
	addq	%r10, %r9
	movq	(%r9), %rax
	movq	8(%rax), %rcx
	cmpq	$64, %rcx
	je	.L1616
	leaq	1(%rcx), %rdi
	movq	%rdi, 8(%rax)
	movq	%rdx, 16(%rax,%rcx,8)
	jmp	.L1432
	.p2align 4,,10
	.p2align 3
.L1592:
	addq	$4, %rcx
	movl	$1, %edx
	jmp	.L1429
	.p2align 4,,10
	.p2align 3
.L1612:
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	movl	%r9d, -112(%rbp)
	movl	%edx, -104(%rbp)
	movq	%r8, -96(%rbp)
	call	_ZnamRKSt9nothrow_t@PLT
	movq	-96(%rbp), %r8
	movl	-104(%rbp), %edx
	testq	%rax, %rax
	movl	-112(%rbp), %r9d
	movq	%rax, %r10
	je	.L1617
.L1458:
	leaq	8(%r10), %rdi
	movq	%r10, %rcx
	xorl	%eax, %eax
	movq	$0, (%r10)
	andq	$-8, %rdi
	movq	$0, 120(%r10)
	subq	%rdi, %rcx
	subl	$-128, %ecx
	shrl	$3, %ecx
	rep stosq
	lock cmpxchgq	%r10, (%r14)
	testq	%rax, %rax
	je	.L1457
	movq	%r10, %rdi
	movl	%r9d, -112(%rbp)
	movl	%edx, -104(%rbp)
	movq	%r8, -96(%rbp)
	call	_ZdaPv@PLT
	movq	(%r14), %r10
	movl	-112(%rbp), %r9d
	movl	-104(%rbp), %edx
	movq	-96(%rbp), %r8
	jmp	.L1457
	.p2align 4,,10
	.p2align 3
.L1607:
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	movq	%rdx, -112(%rbp)
	movl	%r10d, -104(%rbp)
	movl	%r9d, -96(%rbp)
	movq	%r8, -88(%rbp)
	call	_ZnamRKSt9nothrow_t@PLT
	movq	-88(%rbp), %r8
	movl	-96(%rbp), %r9d
	testq	%rax, %rax
	movl	-104(%rbp), %r10d
	movq	-112(%rbp), %rdx
	movq	%rax, %r11
	je	.L1618
.L1443:
	leaq	8(%r11), %rdi
	movq	%r11, %rcx
	xorl	%eax, %eax
	movq	$0, (%r11)
	andq	$-8, %rdi
	movq	$0, 120(%r11)
	subq	%rdi, %rcx
	subl	$-128, %ecx
	shrl	$3, %ecx
	rep stosq
	lock cmpxchgq	%r11, (%rdx)
	movq	%rdx, -88(%rbp)
	testq	%rax, %rax
	je	.L1442
	movq	%r11, %rdi
	movl	%r10d, -112(%rbp)
	movl	%r9d, -104(%rbp)
	movq	%r8, -96(%rbp)
	call	_ZdaPv@PLT
	movq	-88(%rbp), %rdx
	movq	(%rdx), %r11
	movl	-112(%rbp), %r10d
	movl	-104(%rbp), %r9d
	movq	-96(%rbp), %r8
	jmp	.L1442
	.p2align 4,,10
	.p2align 3
.L1606:
	movq	%r12, %rdi
	movq	%r8, -88(%rbp)
	call	_ZN2v88internal11MemoryChunk15AllocateSlotSetILNS0_17RememberedSetTypeE1EEEPNS0_7SlotSetEv@PLT
	movq	-88(%rbp), %r8
	jmp	.L1440
	.p2align 4,,10
	.p2align 3
.L1599:
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	movq	%r8, -120(%rbp)
	movq	%rdx, -112(%rbp)
	movl	%r11d, -104(%rbp)
	movl	%r10d, -96(%rbp)
	call	_ZnamRKSt9nothrow_t@PLT
	movl	-96(%rbp), %r10d
	movl	-104(%rbp), %r11d
	movl	$1, %r9d
	testq	%rax, %rax
	movq	-112(%rbp), %rdx
	movq	-120(%rbp), %r8
	movq	%rax, %rsi
	je	.L1619
.L1475:
	leaq	8(%rsi), %rdi
	movq	%rsi, %rcx
	xorl	%eax, %eax
	movq	$0, (%rsi)
	andq	$-8, %rdi
	movq	$0, 120(%rsi)
	subq	%rdi, %rcx
	subl	$-128, %ecx
	shrl	$3, %ecx
	rep stosq
	lock cmpxchgq	%rsi, (%rdx)
	movq	%rdx, -96(%rbp)
	testq	%rax, %rax
	je	.L1474
	movq	%rsi, %rdi
	movq	%r8, -120(%rbp)
	movl	%r11d, -112(%rbp)
	movl	%r10d, -104(%rbp)
	call	_ZdaPv@PLT
	movq	-96(%rbp), %rdx
	movl	$1, %r9d
	movq	(%rdx), %rsi
	movq	-120(%rbp), %r8
	movl	-112(%rbp), %r11d
	movl	-104(%rbp), %r10d
	jmp	.L1474
	.p2align 4,,10
	.p2align 3
.L1598:
	movq	%r12, %rdi
	movq	%r8, -96(%rbp)
	call	_ZN2v88internal11MemoryChunk15AllocateSlotSetILNS0_17RememberedSetTypeE1EEEPNS0_7SlotSetEv@PLT
	movq	-96(%rbp), %r8
	movl	$1, %r9d
	movq	%rax, %rdx
	jmp	.L1472
	.p2align 4,,10
	.p2align 3
.L1611:
	movq	%r12, %rdi
	movq	%r8, -96(%rbp)
	call	_ZN2v88internal11MemoryChunk15AllocateSlotSetILNS0_17RememberedSetTypeE1EEEPNS0_7SlotSetEv@PLT
	movq	-96(%rbp), %r8
	jmp	.L1455
	.p2align 4,,10
	.p2align 3
.L1616:
	leaq	640(%r10), %rdi
	movq	%r9, -144(%rbp)
	movq	%rcx, -136(%rbp)
	movq	%rsi, -120(%rbp)
	movq	%rdx, -112(%rbp)
	movq	%r8, -104(%rbp)
	movq	%rax, -128(%rbp)
	movq	%r10, -96(%rbp)
	movq	%rdi, -88(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-96(%rbp), %r10
	movq	-128(%rbp), %rax
	movq	680(%r10), %r11
	movq	%r11, (%rax)
	movq	%rax, 680(%r10)
	movq	-88(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movq	-136(%rbp), %rcx
	movq	-144(%rbp), %r9
	movq	%rax, %r10
	leaq	16(%rax), %rdi
	movq	-104(%rbp), %r8
	movq	-112(%rbp), %rdx
	movq	$0, 8(%rax)
	xorl	%eax, %eax
	movq	-120(%rbp), %rsi
	rep stosq
	movq	%r10, (%r9)
	movq	8(%r10), %rax
	cmpq	$64, %rax
	je	.L1432
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%r10)
	movq	%rdx, 16(%r10,%rax,8)
	jmp	.L1432
	.p2align 4,,10
	.p2align 3
.L1615:
	leaq	640(%r10), %rdi
	movq	%r9, -152(%rbp)
	movq	%rcx, -144(%rbp)
	movq	%rsi, -128(%rbp)
	movq	%rdx, -120(%rbp)
	movq	%r8, -112(%rbp)
	movq	%rax, -136(%rbp)
	movq	%r10, -104(%rbp)
	movq	%rdi, -96(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-104(%rbp), %r10
	movq	-136(%rbp), %rax
	movq	680(%r10), %r11
	movq	%r11, (%rax)
	movq	%rax, 680(%r10)
	movq	-96(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movq	-144(%rbp), %rcx
	movq	-152(%rbp), %r9
	movq	%rax, %r10
	leaq	16(%rax), %rdi
	movq	-112(%rbp), %r8
	movq	-120(%rbp), %rdx
	movq	$0, 8(%rax)
	xorl	%eax, %eax
	movq	-128(%rbp), %rsi
	rep stosq
	movq	%r10, (%r9)
	movq	8(%r10), %rax
	cmpq	$64, %rax
	je	.L1447
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%r10)
	movq	%rdx, 16(%r10,%rax,8)
	jmp	.L1447
	.p2align 4,,10
	.p2align 3
.L1614:
	leaq	640(%r11), %rdi
	movq	%r8, -152(%rbp)
	movq	%rcx, -144(%rbp)
	movq	%rdx, -120(%rbp)
	movq	%r10, -112(%rbp)
	movq	%rax, -136(%rbp)
	movq	%rsi, -128(%rbp)
	movq	%r11, -104(%rbp)
	movq	%rdi, -96(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-104(%rbp), %r11
	movq	-136(%rbp), %rax
	movq	680(%r11), %rsi
	movq	%rsi, (%rax)
	movq	%rax, 680(%r11)
	movq	-96(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movq	-144(%rbp), %rcx
	movq	-112(%rbp), %r10
	movl	$1, %r9d
	movq	%rax, %r11
	leaq	16(%rax), %rdi
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %rsi
	movq	$0, 8(%rax)
	xorl	%eax, %eax
	movq	-152(%rbp), %r8
	rep stosq
	movq	%r11, (%r10)
	movq	8(%r11), %rax
	cmpq	$64, %rax
	je	.L1464
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%r11)
	movq	%rdx, 16(%r11,%rax,8)
	jmp	.L1464
	.p2align 4,,10
	.p2align 3
.L1478:
	movslq	64(%r13), %rax
	movq	24(%r13), %rdx
	leaq	(%rax,%rax,4), %r12
	salq	$4, %r12
	addq	%rdx, %r12
	movq	5568(%r12), %r14
	movq	8(%r14), %r13
	cmpq	$64, %r13
	je	.L1620
	leaq	1(%r13), %rax
	movq	%rax, 8(%r14)
	movq	%rbx, 16(%r14,%r13,8)
	jmp	.L1424
	.p2align 4,,10
	.p2align 3
.L1620:
	leaq	6208(%rdx), %rdi
	movq	%rdx, -96(%rbp)
	movq	%rdi, -88(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-96(%rbp), %rdx
	movq	6248(%rdx), %rax
	movq	%rax, (%r14)
	movq	%r14, 6248(%rdx)
	movq	-88(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movq	%r13, %rcx
	movq	$0, 8(%rax)
	movq	%rax, %rdx
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rdx, 5568(%r12)
	movq	8(%rdx), %rax
	cmpq	$64, %rax
	je	.L1424
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdx)
	movq	%rbx, 16(%rdx,%rax,8)
	jmp	.L1424
.L1601:
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L1621
.L1484:
	leaq	8(%r8), %rdi
	movq	%r8, %rcx
	xorl	%eax, %eax
	movq	$0, (%r8)
	andq	$-8, %rdi
	movq	$0, 120(%r8)
	subq	%rdi, %rcx
	subl	$-128, %ecx
	shrl	$3, %ecx
	rep stosq
	lock cmpxchgq	%r8, (%rbx)
	testq	%rax, %rax
	je	.L1483
	movq	%r8, %rdi
	call	_ZdaPv@PLT
	movq	(%rbx), %r8
	jmp	.L1483
.L1600:
	movq	%r12, %rdi
	call	_ZN2v88internal11MemoryChunk15AllocateSlotSetILNS0_17RememberedSetTypeE1EEEPNS0_7SlotSetEv@PLT
	movq	%rax, %rbx
	jmp	.L1481
.L1591:
	call	__stack_chk_fail@PLT
.L1621:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	jne	.L1484
.L1459:
	leaq	.LC1(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
.L1619:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movl	-96(%rbp), %r10d
	movl	-104(%rbp), %r11d
	movl	$1, %r9d
	testq	%rax, %rax
	movq	-112(%rbp), %rdx
	movq	-120(%rbp), %r8
	movq	%rax, %rsi
	jne	.L1475
	jmp	.L1459
.L1618:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	-88(%rbp), %r8
	movl	-96(%rbp), %r9d
	testq	%rax, %rax
	movl	-104(%rbp), %r10d
	movq	-112(%rbp), %rdx
	movq	%rax, %r11
	jne	.L1443
	jmp	.L1459
.L1617:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	-96(%rbp), %r8
	movl	-104(%rbp), %edx
	testq	%rax, %rax
	movl	-112(%rbp), %r9d
	movq	%rax, %r10
	jne	.L1458
	jmp	.L1459
	.cfi_endproc
.LFE29061:
	.size	_ZN2v88internal24ConcurrentMarkingVisitor13VisitWeakCellENS0_3MapENS0_8WeakCellE.constprop.0, .-_ZN2v88internal24ConcurrentMarkingVisitor13VisitWeakCellENS0_3MapENS0_8WeakCellE.constprop.0
	.section	.text._ZN2v88internal24ConcurrentMarkingVisitor23VisitEphemeronHashTableENS0_3MapENS0_18EphemeronHashTableE.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal24ConcurrentMarkingVisitor23VisitEphemeronHashTableENS0_3MapENS0_18EphemeronHashTableE.constprop.0, @function
_ZN2v88internal24ConcurrentMarkingVisitor23VisitEphemeronHashTableENS0_3MapENS0_18EphemeronHashTableE.constprop.0:
.LFB29064:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rax, %rbx
	andq	$-262144, %rbx
	subq	$120, %rsp
	movq	%rsi, -120(%rbp)
	movq	%rdx, -88(%rbp)
	movq	%fs:40, %rdx
	movq	%rdx, -56(%rbp)
	xorl	%edx, %edx
	movq	%rax, -72(%rbp)
	subq	$1, %rax
	movl	$1, %edx
	subl	%ebx, %eax
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %edx
	movl	%edx, %ecx
	movq	16(%rbx), %rdx
	leaq	(%rdx,%rax,4), %rsi
	movl	(%rsi), %eax
	testl	%eax, %ecx
	jne	.L1623
.L1626:
	xorl	%eax, %eax
.L1622:
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L1747
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1623:
	.cfi_restore_state
	movq	%rdi, %r15
	addl	%ecx, %ecx
	jne	.L1627
	movl	4(%rsi), %edx
	movl	$1, %ecx
	addq	$4, %rsi
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%eax, %ecx
	je	.L1626
	.p2align 4,,10
	.p2align 3
.L1749:
	movl	%edx, %edi
	movl	%edx, %eax
	orl	%ecx, %edi
	lock cmpxchgl	%edi, (%rsi)
	cmpl	%eax, %edx
	je	.L1748
.L1627:
	movl	(%rsi), %edx
	movl	%edx, %eax
	andl	%ecx, %eax
	cmpl	%eax, %ecx
	jne	.L1749
	jmp	.L1626
	.p2align 4,,10
	.p2align 3
.L1748:
	movq	-72(%rbp), %rax
	leaq	-72(%rbp), %rdi
	movq	-1(%rax), %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	48(%r15), %rdi
	leaq	-64(%rbp), %rsi
	movq	%rbx, -64(%rbp)
	movslq	%eax, %r12
	call	_ZNSt8__detail9_Map_baseIPN2v88internal11MemoryChunkESt4pairIKS4_NS2_15MemoryChunkDataEESaIS8_ENS_10_Select1stESt8equal_toIS4_ENS3_6HasherENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS6_
	addq	%r12, (%rax)
	movslq	64(%r15), %rax
	movq	24(%r15), %r14
	movq	-88(%rbp), %rdx
	leaq	(%rax,%rax,4), %rbx
	salq	$4, %rbx
	addq	%r14, %rbx
	movq	696(%rbx), %r13
	movq	8(%r13), %r12
	cmpq	$64, %r12
	je	.L1750
	leaq	1(%r12), %rax
	movq	%rax, 8(%r13)
	movq	%rdx, 16(%r13,%r12,8)
.L1629:
	movq	-88(%rbp), %rdx
	movl	$40, %ebx
	xorl	%r12d, %r12d
	movl	$1, %r14d
	movl	35(%rdx), %eax
	testl	%eax, %eax
	jle	.L1676
	movq	%r15, -112(%rbp)
	movq	%rdx, %r13
	movl	%r12d, %r15d
	movq	%rbx, %rdx
	jmp	.L1631
	.p2align 4,,10
	.p2align 3
.L1661:
	movq	-88(%rbp), %r13
.L1659:
	addl	$1, %r15d
	addq	$16, %rdx
	cmpl	%r15d, 35(%r13)
	jle	.L1676
.L1631:
	movq	-1(%rdx,%r13), %r9
	movq	-88(%rbp), %rax
	movq	%r9, %r12
	andq	$-262144, %r12
	movq	8(%r12), %rcx
	andl	$64, %ecx
	jne	.L1632
.L1636:
	movq	-88(%rbp), %rax
	movl	%r9d, %ecx
	movq	16(%r12), %rsi
	movl	%r14d, %edi
	subl	%r12d, %ecx
	leaq	7(%rdx,%rax), %rbx
	movl	%ecx, %eax
	shrl	$3, %ecx
	shrl	$8, %eax
	sall	%cl, %edi
	movl	(%rsi,%rax,4), %eax
	testl	%eax, %edi
	je	.L1751
	movq	-88(%rbp), %r13
	cmpq	$-9, %rbx
	ja	.L1659
	movq	(%rbx), %r12
	testb	$1, %r12b
	je	.L1661
	movl	%r12d, %eax
	movq	%r12, %rsi
	movl	%r14d, %edi
	andl	$262143, %eax
	andq	$-262144, %rsi
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %edi
	movq	16(%rsi), %rcx
	leaq	(%rcx,%rax,4), %r9
	jmp	.L1646
	.p2align 4,,10
	.p2align 3
.L1753:
	movl	%edi, %r8d
	movl	%ecx, %eax
	orl	%ecx, %r8d
	lock cmpxchgl	%r8d, (%r9)
	cmpl	%eax, %ecx
	je	.L1752
.L1646:
	movl	(%r9), %ecx
	movl	%edi, %eax
	andl	%ecx, %eax
	cmpl	%eax, %edi
	jne	.L1753
.L1645:
	movq	8(%rsi), %rax
	testb	$64, %al
	je	.L1661
	andq	$-262144, %r13
	movq	8(%r13), %rax
	testb	$88, %al
	je	.L1650
	testb	$-128, %ah
	je	.L1661
.L1650:
	movq	112(%r13), %rax
	testq	%rax, %rax
	je	.L1754
.L1653:
	movq	%rbx, %rcx
	andl	$262143, %ebx
	subq	%r13, %rcx
	movl	%ebx, %r12d
	movl	%ebx, %r13d
	sarl	$13, %ebx
	shrq	$18, %rcx
	movslq	%ebx, %rbx
	sarl	$8, %r12d
	leaq	(%rcx,%rcx,2), %rcx
	sarl	$3, %r13d
	andl	$31, %r12d
	salq	$7, %rcx
	andl	$31, %r13d
	leaq	(%rcx,%rbx,8), %rbx
	addq	%rax, %rbx
	movq	(%rbx), %r9
	testq	%r9, %r9
	je	.L1755
.L1655:
	movslq	%r12d, %r12
	movl	%r14d, %esi
	movl	%r13d, %ecx
	leaq	(%r9,%r12,4), %rdi
	sall	%cl, %esi
	movl	(%rdi), %eax
	testl	%eax, %esi
	jne	.L1661
	jmp	.L1658
	.p2align 4,,10
	.p2align 3
.L1756:
	movl	%esi, %r8d
	movl	%ecx, %eax
	orl	%ecx, %r8d
	lock cmpxchgl	%r8d, (%rdi)
	cmpl	%eax, %ecx
	je	.L1661
.L1658:
	movl	(%rdi), %ecx
	movl	%esi, %eax
	andl	%ecx, %eax
	cmpl	%eax, %esi
	jne	.L1756
	jmp	.L1661
	.p2align 4,,10
	.p2align 3
.L1751:
	leaq	-88(%rbp), %rdi
	movl	%r15d, %esi
	movq	%rdx, -104(%rbp)
	movq	%r9, -96(%rbp)
	call	_ZN2v88internal19ObjectHashTableBaseINS0_18EphemeronHashTableENS0_23EphemeronHashTableShapeEE7ValueAtEi@PLT
	movq	-96(%rbp), %r9
	movq	-104(%rbp), %rdx
	testb	$1, %al
	movq	%rax, %r12
	je	.L1661
	movq	%r12, %r13
	movq	-88(%rbp), %rax
	andq	$-262144, %r13
	movq	8(%r13), %rcx
	andl	$64, %ecx
	jne	.L1662
.L1666:
	movl	%r12d, %ecx
	movq	16(%r13), %rsi
	movl	%r14d, %ebx
	subl	%r13d, %ecx
	movl	%ecx, %eax
	shrl	$3, %ecx
	shrl	$8, %eax
	sall	%cl, %ebx
	movl	(%rsi,%rax,4), %eax
	testl	%eax, %ebx
	jne	.L1661
	movq	-112(%rbp), %rax
	movq	24(%rax), %rcx
	movslq	64(%rax), %rax
	leaq	(%rax,%rax,4), %rbx
	salq	$4, %rbx
	addq	%rcx, %rbx
	movq	2784(%rbx), %r13
	movq	8(%r13), %rax
	cmpq	$64, %rax
	je	.L1757
	addq	$1, %rax
	movq	%r9, %xmm0
	movq	%r12, %xmm1
	movq	%rax, 8(%r13)
	punpcklqdq	%xmm1, %xmm0
	salq	$4, %rax
	movups	%xmm0, 0(%r13,%rax)
	jmp	.L1661
	.p2align 4,,10
	.p2align 3
.L1752:
	movq	-112(%rbp), %rax
	movq	8(%rax), %r10
	movslq	16(%rax), %rax
	leaq	(%rax,%rax,4), %r9
	salq	$4, %r9
	addq	%r10, %r9
	movq	(%r9), %rax
	movq	8(%rax), %rcx
	cmpq	$64, %rcx
	je	.L1758
	leaq	1(%rcx), %rdi
	movq	%rdi, 8(%rax)
	movq	%r12, 16(%rax,%rcx,8)
	jmp	.L1645
	.p2align 4,,10
	.p2align 3
.L1632:
	andq	$-262144, %rax
	movq	%rax, %rbx
	movq	8(%rax), %rax
	testb	$88, %al
	je	.L1635
	testb	$-128, %ah
	je	.L1636
.L1635:
	movq	112(%rbx), %rcx
	testq	%rcx, %rcx
	je	.L1759
.L1638:
	movq	%rbx, %rax
	notq	%rax
	addq	%rax, %r13
	addq	%rdx, %r13
	movl	%r13d, %eax
	shrq	$18, %r13
	andl	$262143, %eax
	leaq	0(%r13,%r13,2), %rsi
	movl	%eax, %ebx
	movl	%eax, %r10d
	sarl	$13, %eax
	salq	$7, %rsi
	cltq
	sarl	$8, %ebx
	leaq	(%rsi,%rax,8), %r13
	sarl	$3, %r10d
	andl	$31, %ebx
	addq	%rcx, %r13
	andl	$31, %r10d
	movq	0(%r13), %r11
	testq	%r11, %r11
	je	.L1760
.L1640:
	movslq	%ebx, %rbx
	movl	%r14d, %esi
	movl	%r10d, %ecx
	leaq	(%r11,%rbx,4), %rdi
	sall	%cl, %esi
	movl	(%rdi), %eax
	testl	%eax, %esi
	jne	.L1636
	jmp	.L1642
	.p2align 4,,10
	.p2align 3
.L1761:
	movl	%esi, %r8d
	movl	%ecx, %eax
	orl	%ecx, %r8d
	lock cmpxchgl	%r8d, (%rdi)
	cmpl	%eax, %ecx
	je	.L1636
.L1642:
	movl	(%rdi), %ecx
	movl	%esi, %eax
	andl	%ecx, %eax
	cmpl	%eax, %esi
	jne	.L1761
	jmp	.L1636
	.p2align 4,,10
	.p2align 3
.L1676:
	movq	-120(%rbp), %rsi
	leaq	-88(%rbp), %rdi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	jmp	.L1622
	.p2align 4,,10
	.p2align 3
.L1755:
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	movq	%rdx, -96(%rbp)
	call	_ZnamRKSt9nothrow_t@PLT
	movq	-96(%rbp), %rdx
	testq	%rax, %rax
	movq	%rax, %r9
	je	.L1762
.L1656:
	leaq	8(%r9), %rdi
	movq	%r9, %rcx
	xorl	%eax, %eax
	movq	$0, (%r9)
	andq	$-8, %rdi
	movq	$0, 120(%r9)
	subq	%rdi, %rcx
	subl	$-128, %ecx
	shrl	$3, %ecx
	rep stosq
	lock cmpxchgq	%r9, (%rbx)
	testq	%rax, %rax
	je	.L1655
	movq	%r9, %rdi
	movq	%rdx, -96(%rbp)
	call	_ZdaPv@PLT
	movq	(%rbx), %r9
	movq	-96(%rbp), %rdx
	jmp	.L1655
	.p2align 4,,10
	.p2align 3
.L1754:
	movq	%r13, %rdi
	movq	%rdx, -96(%rbp)
	call	_ZN2v88internal11MemoryChunk15AllocateSlotSetILNS0_17RememberedSetTypeE1EEEPNS0_7SlotSetEv@PLT
	movq	-96(%rbp), %rdx
	jmp	.L1653
	.p2align 4,,10
	.p2align 3
.L1758:
	leaq	640(%r10), %rdi
	movq	%rdx, -160(%rbp)
	movq	%r9, -152(%rbp)
	movq	%rcx, -144(%rbp)
	movq	%rsi, -128(%rbp)
	movq	%rax, -136(%rbp)
	movq	%r10, -104(%rbp)
	movq	%rdi, -96(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-104(%rbp), %r10
	movq	-136(%rbp), %rax
	movq	680(%r10), %r11
	movq	%r11, (%rax)
	movq	%rax, 680(%r10)
	movq	-96(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movq	-144(%rbp), %rcx
	movq	-128(%rbp), %rsi
	movq	$0, 8(%rax)
	movq	%rax, %r10
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	-152(%rbp), %r9
	movq	-160(%rbp), %rdx
	movq	%r10, (%r9)
	movq	8(%r10), %rax
	cmpq	$64, %rax
	je	.L1645
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%r10)
	movq	%r12, 16(%r10,%rax,8)
	jmp	.L1645
	.p2align 4,,10
	.p2align 3
.L1760:
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	movq	%rdx, -128(%rbp)
	movl	%r10d, -104(%rbp)
	movq	%r9, -96(%rbp)
	call	_ZnamRKSt9nothrow_t@PLT
	movq	-96(%rbp), %r9
	movl	-104(%rbp), %r10d
	testq	%rax, %rax
	movq	-128(%rbp), %rdx
	movq	%rax, %r11
	je	.L1763
.L1641:
	leaq	8(%r11), %rdi
	movq	%r11, %rcx
	xorl	%eax, %eax
	movq	$0, (%r11)
	andq	$-8, %rdi
	movq	$0, 120(%r11)
	subq	%rdi, %rcx
	subl	$-128, %ecx
	shrl	$3, %ecx
	rep stosq
	lock cmpxchgq	%r11, 0(%r13)
	testq	%rax, %rax
	je	.L1640
	movq	%r11, %rdi
	movq	%rdx, -128(%rbp)
	movl	%r10d, -104(%rbp)
	movq	%r9, -96(%rbp)
	call	_ZdaPv@PLT
	movq	0(%r13), %r11
	movq	-128(%rbp), %rdx
	movl	-104(%rbp), %r10d
	movq	-96(%rbp), %r9
	jmp	.L1640
	.p2align 4,,10
	.p2align 3
.L1759:
	movq	%rbx, %rdi
	movq	%rdx, -104(%rbp)
	movq	%r9, -96(%rbp)
	call	_ZN2v88internal11MemoryChunk15AllocateSlotSetILNS0_17RememberedSetTypeE1EEEPNS0_7SlotSetEv@PLT
	movq	-104(%rbp), %rdx
	movq	-96(%rbp), %r9
	movq	%rax, %rcx
	jmp	.L1638
	.p2align 4,,10
	.p2align 3
.L1662:
	andq	$-262144, %rax
	movq	%rax, %rcx
	movq	8(%rax), %rax
	testb	$88, %al
	jne	.L1764
.L1665:
	movq	112(%rcx), %rax
	testq	%rax, %rax
	je	.L1765
.L1668:
	movq	%rbx, %rdi
	andl	$262143, %ebx
	subq	%rcx, %rdi
	movl	%ebx, %r10d
	movl	%ebx, %r11d
	sarl	$13, %ebx
	sarl	$8, %r10d
	movq	%rdi, %rcx
	movslq	%ebx, %rbx
	sarl	$3, %r11d
	shrq	$18, %rcx
	andl	$31, %r10d
	andl	$31, %r11d
	leaq	(%rcx,%rcx,2), %rcx
	salq	$7, %rcx
	leaq	(%rcx,%rbx,8), %rbx
	addq	%rax, %rbx
	movq	(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L1766
.L1670:
	movslq	%r10d, %r10
	movl	%r14d, %edi
	movl	%r11d, %ecx
	leaq	(%rsi,%r10,4), %rsi
	sall	%cl, %edi
	movl	(%rsi), %eax
	testl	%eax, %edi
	jne	.L1666
	jmp	.L1672
	.p2align 4,,10
	.p2align 3
.L1767:
	movl	%edi, %r8d
	movl	%ecx, %eax
	orl	%ecx, %r8d
	lock cmpxchgl	%r8d, (%rsi)
	cmpl	%eax, %ecx
	je	.L1666
.L1672:
	movl	(%rsi), %ecx
	movl	%edi, %eax
	andl	%ecx, %eax
	cmpl	%eax, %edi
	jne	.L1767
	jmp	.L1666
	.p2align 4,,10
	.p2align 3
.L1764:
	testb	$-128, %ah
	je	.L1666
	jmp	.L1665
	.p2align 4,,10
	.p2align 3
.L1750:
	leaq	1336(%r14), %rdi
	movq	%rdx, -104(%rbp)
	movq	%rdi, -96(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	1376(%r14), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 1376(%r14)
	movq	-96(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movq	%r12, %rcx
	movq	-104(%rbp), %rdx
	movq	$0, 8(%rax)
	movq	%rax, %rsi
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rsi, 696(%rbx)
	movq	8(%rsi), %rax
	cmpq	$64, %rax
	je	.L1629
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rsi)
	movq	%rdx, 16(%rsi,%rax,8)
	jmp	.L1629
	.p2align 4,,10
	.p2align 3
.L1766:
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	movq	%rdx, -136(%rbp)
	movl	%r11d, -128(%rbp)
	movl	%r10d, -104(%rbp)
	movq	%r9, -96(%rbp)
	call	_ZnamRKSt9nothrow_t@PLT
	movq	-96(%rbp), %r9
	movl	-104(%rbp), %r10d
	testq	%rax, %rax
	movl	-128(%rbp), %r11d
	movq	-136(%rbp), %rdx
	movq	%rax, %rsi
	je	.L1768
.L1671:
	leaq	8(%rsi), %rdi
	movq	%rsi, %rcx
	xorl	%eax, %eax
	movq	$0, (%rsi)
	andq	$-8, %rdi
	movq	$0, 120(%rsi)
	subq	%rdi, %rcx
	subl	$-128, %ecx
	shrl	$3, %ecx
	rep stosq
	lock cmpxchgq	%rsi, (%rbx)
	testq	%rax, %rax
	je	.L1670
	movq	%rsi, %rdi
	movq	%rdx, -136(%rbp)
	movl	%r11d, -128(%rbp)
	movl	%r10d, -104(%rbp)
	movq	%r9, -96(%rbp)
	call	_ZdaPv@PLT
	movq	(%rbx), %rsi
	movq	-136(%rbp), %rdx
	movl	-128(%rbp), %r11d
	movl	-104(%rbp), %r10d
	movq	-96(%rbp), %r9
	jmp	.L1670
	.p2align 4,,10
	.p2align 3
.L1765:
	movq	%rcx, %rdi
	movq	%rdx, -128(%rbp)
	movq	%r9, -104(%rbp)
	movq	%rcx, -96(%rbp)
	call	_ZN2v88internal11MemoryChunk15AllocateSlotSetILNS0_17RememberedSetTypeE1EEEPNS0_7SlotSetEv@PLT
	movq	-128(%rbp), %rdx
	movq	-104(%rbp), %r9
	movq	-96(%rbp), %rcx
	jmp	.L1668
	.p2align 4,,10
	.p2align 3
.L1757:
	leaq	3424(%rcx), %rdi
	movq	%rdx, -136(%rbp)
	movq	%r9, -128(%rbp)
	movq	%rcx, -104(%rbp)
	movq	%rdi, -96(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-104(%rbp), %rcx
	movq	3464(%rcx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 3464(%rcx)
	movq	-96(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$1040, %edi
	call	_Znwm@PLT
	movl	$128, %ecx
	movq	-128(%rbp), %r9
	movq	-136(%rbp), %rdx
	movq	$0, 8(%rax)
	movq	%rax, %rsi
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rsi, 2784(%rbx)
	movq	8(%rsi), %rax
	cmpq	$64, %rax
	je	.L1661
	addq	$1, %rax
	movq	%r9, %xmm0
	movq	%r12, %xmm2
	movq	%rax, 8(%rsi)
	punpcklqdq	%xmm2, %xmm0
	salq	$4, %rax
	movups	%xmm0, (%rsi,%rax)
	jmp	.L1661
.L1747:
	call	__stack_chk_fail@PLT
.L1763:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	-96(%rbp), %r9
	movl	-104(%rbp), %r10d
	testq	%rax, %rax
	movq	-128(%rbp), %rdx
	movq	%rax, %r11
	jne	.L1641
.L1657:
	leaq	.LC1(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
.L1762:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	-96(%rbp), %rdx
	testq	%rax, %rax
	movq	%rax, %r9
	jne	.L1656
	jmp	.L1657
.L1768:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$128, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	-96(%rbp), %r9
	movl	-104(%rbp), %r10d
	testq	%rax, %rax
	movl	-128(%rbp), %r11d
	movq	-136(%rbp), %rdx
	movq	%rax, %rsi
	jne	.L1671
	jmp	.L1657
	.cfi_endproc
.LFE29064:
	.size	_ZN2v88internal24ConcurrentMarkingVisitor23VisitEphemeronHashTableENS0_3MapENS0_18EphemeronHashTableE.constprop.0, .-_ZN2v88internal24ConcurrentMarkingVisitor23VisitEphemeronHashTableENS0_3MapENS0_18EphemeronHashTableE.constprop.0
	.section	.text._ZN2v88internal8WorklistINS0_10HeapObjectELi16EE4PushEiS2_,"axG",@progbits,_ZN2v88internal8WorklistINS0_10HeapObjectELi16EE4PushEiS2_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8WorklistINS0_10HeapObjectELi16EE4PushEiS2_
	.type	_ZN2v88internal8WorklistINS0_10HeapObjectELi16EE4PushEiS2_, @function
_ZN2v88internal8WorklistINS0_10HeapObjectELi16EE4PushEiS2_:
.LFB27458:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	(%rsi,%rsi,4), %rbx
	salq	$4, %rbx
	addq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rbx), %r14
	movq	8(%r14), %r12
	cmpq	$16, %r12
	je	.L1773
	leaq	1(%r12), %rax
	movq	%rax, 8(%r14)
	movq	%rdx, 16(%r14,%r12,8)
.L1771:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1773:
	.cfi_restore_state
	leaq	640(%rdi), %r15
	movq	%rdi, %r13
	movq	%rdx, -56(%rbp)
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	680(%r13), %rax
	movq	%r15, %rdi
	movq	%rax, (%r14)
	movq	%r14, 680(%r13)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$144, %edi
	call	_Znwm@PLT
	movq	-56(%rbp), %rdx
	movq	%r12, %rcx
	movq	%rax, %rsi
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rsi, (%rbx)
	movq	$1, 8(%rsi)
	movq	%rdx, 16(%rsi)
	jmp	.L1771
	.cfi_endproc
.LFE27458:
	.size	_ZN2v88internal8WorklistINS0_10HeapObjectELi16EE4PushEiS2_, .-_ZN2v88internal8WorklistINS0_10HeapObjectELi16EE4PushEiS2_
	.section	.text._ZN2v88internal8WorklistISt4pairINS0_10HeapObjectENS0_18FullHeapObjectSlotEELi64EE7SegmentC2Ev,"axG",@progbits,_ZN2v88internal8WorklistISt4pairINS0_10HeapObjectENS0_18FullHeapObjectSlotEELi64EE7SegmentC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8WorklistISt4pairINS0_10HeapObjectENS0_18FullHeapObjectSlotEELi64EE7SegmentC2Ev
	.type	_ZN2v88internal8WorklistISt4pairINS0_10HeapObjectENS0_18FullHeapObjectSlotEELi64EE7SegmentC2Ev, @function
_ZN2v88internal8WorklistISt4pairINS0_10HeapObjectENS0_18FullHeapObjectSlotEELi64EE7SegmentC2Ev:
.LFB27801:
	.cfi_startproc
	endbr64
	movq	$0, 8(%rdi)
	leaq	16(%rdi), %rax
	leaq	1040(%rdi), %rdx
	pxor	%xmm0, %xmm0
	.p2align 4,,10
	.p2align 3
.L1775:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L1775
	ret
	.cfi_endproc
.LFE27801:
	.size	_ZN2v88internal8WorklistISt4pairINS0_10HeapObjectENS0_18FullHeapObjectSlotEELi64EE7SegmentC2Ev, .-_ZN2v88internal8WorklistISt4pairINS0_10HeapObjectENS0_18FullHeapObjectSlotEELi64EE7SegmentC2Ev
	.weak	_ZN2v88internal8WorklistISt4pairINS0_10HeapObjectENS0_18FullHeapObjectSlotEELi64EE7SegmentC1Ev
	.set	_ZN2v88internal8WorklistISt4pairINS0_10HeapObjectENS0_18FullHeapObjectSlotEELi64EE7SegmentC1Ev,_ZN2v88internal8WorklistISt4pairINS0_10HeapObjectENS0_18FullHeapObjectSlotEELi64EE7SegmentC2Ev
	.section	.text._ZN2v88internal18BodyDescriptorBase23IterateJSObjectBodyImplINS0_24ConcurrentMarkingVisitor23SlotSnapshottingVisitorEEEvNS0_3MapENS0_10HeapObjectEiiPT_,"axG",@progbits,_ZN2v88internal18BodyDescriptorBase23IterateJSObjectBodyImplINS0_24ConcurrentMarkingVisitor23SlotSnapshottingVisitorEEEvNS0_3MapENS0_10HeapObjectEiiPT_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal18BodyDescriptorBase23IterateJSObjectBodyImplINS0_24ConcurrentMarkingVisitor23SlotSnapshottingVisitorEEEvNS0_3MapENS0_10HeapObjectEiiPT_
	.type	_ZN2v88internal18BodyDescriptorBase23IterateJSObjectBodyImplINS0_24ConcurrentMarkingVisitor23SlotSnapshottingVisitorEEEvNS0_3MapENS0_10HeapObjectEiiPT_, @function
_ZN2v88internal18BodyDescriptorBase23IterateJSObjectBodyImplINS0_24ConcurrentMarkingVisitor23SlotSnapshottingVisitorEEEvNS0_3MapENS0_10HeapObjectEiiPT_:
.LFB28327:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movslq	%ecx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	47(%rdi), %rax
	testq	%rax, %rax
	jne	.L1778
	subq	$1, %rsi
	movslq	%edx, %rdx
	addq	%rsi, %r15
	addq	%rsi, %rdx
	cmpq	%rdx, %r15
	jbe	.L1777
	.p2align 4,,10
	.p2align 3
.L1779:
	movq	(%rdx), %rdi
	movq	8(%r14), %rsi
	movq	%rdx, %xmm0
	addq	$8, %rdx
	movslq	(%rsi), %rax
	movq	%rdi, %xmm2
	punpcklqdq	%xmm2, %xmm0
	leal	1(%rax), %r8d
	salq	$4, %rax
	movl	%r8d, (%rsi)
	movups	%xmm0, 8(%rsi,%rax)
	cmpq	%r15, %rdx
	jb	.L1779
.L1777:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1801
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1778:
	.cfi_restore_state
	movq	$0, -72(%rbp)
	movb	$1, -80(%rbp)
	movl	$0, -76(%rbp)
	movq	47(%rdi), %rax
	movq	%rax, -72(%rbp)
	testq	%rax, %rax
	jne	.L1802
.L1782:
	leaq	-1(%rsi), %rax
	leaq	-84(%rbp), %r13
	movq	%rax, -104(%rbp)
	leaq	-80(%rbp), %r12
	cmpl	%r15d, %ebx
	jl	.L1783
	jmp	.L1777
	.p2align 4,,10
	.p2align 3
.L1784:
	movslq	-84(%rbp), %rsi
	movq	-104(%rbp), %rcx
	movslq	%ebx, %rdx
	movq	%rsi, %rax
	addq	%rcx, %rdx
	addq	%rcx, %rsi
	cmpq	%rdx, %rsi
	jbe	.L1790
	.p2align 4,,10
	.p2align 3
.L1787:
	movq	(%rdx), %rdi
	movq	8(%r14), %rcx
	movq	%rdx, %xmm0
	addq	$8, %rdx
	movslq	(%rcx), %rax
	movq	%rdi, %xmm1
	punpcklqdq	%xmm1, %xmm0
	leal	1(%rax), %r8d
	salq	$4, %rax
	movl	%r8d, (%rcx)
	movups	%xmm0, 8(%rcx,%rax)
	cmpq	%rdx, %rsi
	ja	.L1787
.L1800:
	movl	-84(%rbp), %ebx
.L1785:
	cmpl	%ebx, %r15d
	jle	.L1777
.L1783:
	movq	%r13, %rcx
	movl	%r15d, %edx
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal22LayoutDescriptorHelper8IsTaggedEiiPi@PLT
	testb	%al, %al
	je	.L1800
	jmp	.L1784
	.p2align 4,,10
	.p2align 3
.L1802:
	movzbl	8(%rdi), %eax
	movb	$0, -80(%rbp)
	sall	$3, %eax
	movl	%eax, -76(%rbp)
	jmp	.L1782
	.p2align 4,,10
	.p2align 3
.L1790:
	movl	%eax, %ebx
	jmp	.L1785
.L1801:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE28327:
	.size	_ZN2v88internal18BodyDescriptorBase23IterateJSObjectBodyImplINS0_24ConcurrentMarkingVisitor23SlotSnapshottingVisitorEEEvNS0_3MapENS0_10HeapObjectEiiPT_, .-_ZN2v88internal18BodyDescriptorBase23IterateJSObjectBodyImplINS0_24ConcurrentMarkingVisitor23SlotSnapshottingVisitorEEEvNS0_3MapENS0_10HeapObjectEiiPT_
	.section	.rodata._ZN2v88internal17ConcurrentMarking3RunEiPNS1_9TaskStateE.str1.1,"aMS",@progbits,1
.LC8:
	.string	"disabled-by-default-v8.gc"
	.section	.rodata._ZN2v88internal17ConcurrentMarking3RunEiPNS1_9TaskStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC9:
	.string	"Starting concurrent marking task %d\n"
	.align 8
.LC10:
	.string	"ConcurrentMarking::Run Preempted"
	.align 8
.LC11:
	.string	"Task %d concurrently marked %dKB in %.2fms\n"
	.section	.text._ZN2v88internal17ConcurrentMarking3RunEiPNS1_9TaskStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ConcurrentMarking3RunEiPNS1_9TaskStateE
	.type	_ZN2v88internal17ConcurrentMarking3RunEiPNS1_9TaskStateE, @function
_ZN2v88internal17ConcurrentMarking3RunEiPNS1_9TaskStateE:
.LFB22336:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$472, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -4520(%rbp)
	movq	%rdi, %rbx
	movl	%esi, -4500(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	2008(%rax), %rdi
	call	_ZN2v88internal8GCTracer32worker_thread_runtime_call_statsEv@PLT
	movq	%rax, %rsi
	leaq	-4440(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -4576(%rbp)
	call	_ZN2v88internal33WorkerThreadRuntimeCallStatsScopeC1EPNS0_28WorkerThreadRuntimeCallStatsE@PLT
	movq	(%rbx), %rax
	movl	$5, %edx
	movq	-4440(%rbp), %rcx
	movq	2008(%rax), %rsi
	leaq	-4384(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -4584(%rbp)
	call	_ZN2v88internal8GCTracer15BackgroundScopeC1EPS1_NS2_7ScopeIdEPNS0_16RuntimeCallStatsE@PLT
	movq	_ZZN2v88internal17ConcurrentMarking3RunEiPNS1_9TaskStateEE28trace_event_unique_atomic753(%rip), %r12
	testq	%r12, %r12
	je	.L2195
.L1805:
	movq	$0, -4416(%rbp)
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L2196
.L1807:
	movq	(%rbx), %rdi
	movq	-4520(%rbp), %rdx
	leaq	16+_ZTVN2v88internal24ConcurrentMarkingVisitorE(%rip), %r9
	movdqu	24(%rbx), %xmm6
	movq	2128(%rdi), %rax
	movzbl	76(%rdx), %ecx
	movl	72(%rdx), %esi
	cmpq	$0, 8(%rax)
	movq	8(%rbx), %rax
	movq	%r9, -4240(%rbp)
	setne	%r8b
	addq	$8, %rdx
	movl	$0, -4168(%rbp)
	movq	%rdx, %xmm0
	movq	%rax, -4232(%rbp)
	leaq	-80(%rbp), %rdx
	movl	-4500(%rbp), %eax
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm6, -4216(%rbp)
	movl	%eax, -4224(%rbp)
	movl	%eax, -4200(%rbp)
	movl	%eax, -4176(%rbp)
	leaq	-4160(%rbp), %rax
	movaps	%xmm0, -4192(%rbp)
	pxor	%xmm0, %xmm0
	.p2align 4,,10
	.p2align 3
.L1811:
	movaps	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L1811
	cmpb	$0, _ZN2v88internal26FLAG_stress_flush_bytecodeE(%rip)
	movb	%r8b, -80(%rbp)
	movl	$2, %eax
	movl	%esi, -76(%rbp)
	movb	%cl, -72(%rbp)
	jne	.L1812
	movzbl	_ZN2v88internal19FLAG_flush_bytecodeE(%rip), %eax
.L1812:
	cmpb	$0, _ZN2v88internal29FLAG_trace_concurrent_markingE(%rip)
	movl	%eax, -68(%rbp)
	jne	.L2197
.L1813:
	call	_ZN2v88internal10TimedScope11TimestampMsEv
	leaq	-4320(%rbp), %r15
	movl	-4500(%rbp), %r14d
	xorl	%r13d, %r13d
	movsd	%xmm0, -4592(%rbp)
	pxor	%xmm0, %xmm0
	leaq	-4240(%rbp), %r12
	movq	%r15, -4496(%rbp)
	movaps	%xmm0, -4320(%rbp)
	jmp	.L1814
	.p2align 4,,10
	.p2align 3
.L2198:
	movq	-4312(%rbp), %rdx
	movq	-4320(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal24ConcurrentMarkingVisitor16ProcessEphemeronENS0_10HeapObjectES2_
	testb	%al, %al
	cmovne	%eax, %r13d
.L1814:
	movq	24(%rbx), %rax
	movq	%r15, %rdx
	movl	%r14d, %esi
	leaq	1392(%rax), %rdi
	call	_ZN2v88internal8WorklistINS0_9EphemeronELi64EE3PopEiPS2_
	testb	%al, %al
	jne	.L2198
	movb	%al, -4521(%rbp)
	movslq	-4500(%rbp), %rax
	movb	%r13b, -4522(%rbp)
	movq	%rax, -4512(%rbp)
	leaq	-4168(%rbp), %rax
	movq	%rax, -4552(%rbp)
	leaq	16+_ZTVN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitorE(%rip), %rax
	movq	$0, -4480(%rbp)
	movq	%rax, %xmm3
	movhps	-4552(%rbp), %xmm3
	movaps	%xmm3, -4544(%rbp)
	.p2align 4,,10
	.p2align 3
.L1974:
	movq	-4512(%rbp), %rax
	xorl	%r15d, %r15d
	movq	$0, -4456(%rbp)
	movl	%r15d, %r14d
	movl	-4500(%rbp), %r15d
	leaq	(%rax,%rax,4), %rax
	movq	%rax, -4464(%rbp)
	.p2align 4,,10
	.p2align 3
.L1944:
	movq	-4464(%rbp), %r13
	movq	8(%rbx), %rcx
	salq	$4, %r13
	leaq	(%rcx,%r13), %r12
	movq	8(%r12), %rax
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2199
	leaq	-1(%rdx), %rcx
	movq	%rcx, 8(%rax)
	movq	8(%rax,%rdx,8), %r12
	leaq	-1(%r12), %r8
.L1820:
	movq	(%rbx), %rax
	addl	$1, %r14d
	movq	248(%rax), %rax
	movq	192(%rax), %rdx
	cmpq	%r8, %rdx
	movq	(%rbx), %rax
	setbe	%dl
	movq	248(%rax), %rcx
	movq	200(%rcx), %rcx
	movq	296(%rax), %rax
	cmpq	%r8, %rcx
	movq	128(%rax), %rsi
	seta	%al
	testb	%al, %dl
	jne	.L1985
	cmpq	%r8, %rsi
	je	.L1985
	movq	(%r8), %r10
	movzbl	10(%r10), %eax
	cmpb	$56, %al
	ja	.L1830
	leaq	.L1832(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal17ConcurrentMarking3RunEiPNS1_9TaskStateE,"a",@progbits
	.align 4
	.align 4
.L1832:
	.long	.L1887-.L1832
	.long	.L1886-.L1832
	.long	.L1885-.L1832
	.long	.L1884-.L1832
	.long	.L1883-.L1832
	.long	.L1882-.L1832
	.long	.L1830-.L1832
	.long	.L1881-.L1832
	.long	.L1880-.L1832
	.long	.L1879-.L1832
	.long	.L1878-.L1832
	.long	.L1877-.L1832
	.long	.L1876-.L1832
	.long	.L1875-.L1832
	.long	.L1874-.L1832
	.long	.L1873-.L1832
	.long	.L1872-.L1832
	.long	.L1871-.L1832
	.long	.L1870-.L1832
	.long	.L1869-.L1832
	.long	.L1868-.L1832
	.long	.L1867-.L1832
	.long	.L1866-.L1832
	.long	.L1865-.L1832
	.long	.L1864-.L1832
	.long	.L1863-.L1832
	.long	.L1862-.L1832
	.long	.L1861-.L1832
	.long	.L1860-.L1832
	.long	.L1859-.L1832
	.long	.L1858-.L1832
	.long	.L1857-.L1832
	.long	.L1856-.L1832
	.long	.L1855-.L1832
	.long	.L1854-.L1832
	.long	.L1853-.L1832
	.long	.L1852-.L1832
	.long	.L1851-.L1832
	.long	.L1850-.L1832
	.long	.L1849-.L1832
	.long	.L1848-.L1832
	.long	.L1847-.L1832
	.long	.L1846-.L1832
	.long	.L1845-.L1832
	.long	.L1844-.L1832
	.long	.L1843-.L1832
	.long	.L1842-.L1832
	.long	.L1841-.L1832
	.long	.L1840-.L1832
	.long	.L1839-.L1832
	.long	.L1838-.L1832
	.long	.L1837-.L1832
	.long	.L1836-.L1832
	.long	.L1835-.L1832
	.long	.L1834-.L1832
	.long	.L1833-.L1832
	.long	.L1831-.L1832
	.section	.text._ZN2v88internal17ConcurrentMarking3RunEiPNS1_9TaskStateE
.L1831:
	leaq	-4240(%rbp), %rdi
	movq	%r12, %rdx
	movq	%r10, %rsi
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitWeakCellENS0_3MapENS0_8WeakCellE.constprop.0
	cltq
	addq	%rax, -4456(%rbp)
	.p2align 4,,10
	.p2align 3
.L1829:
	cmpq	$65535, -4456(%rbp)
	ja	.L2192
	cmpl	$999, %r14d
	jle	.L1944
.L2192:
	movq	-4456(%rbp), %rsi
	addq	%rsi, -4480(%rbp)
	movq	-4520(%rbp), %rsi
	movq	-4480(%rbp), %rax
	movq	%r13, -4464(%rbp)
	movq	%rax, 64(%rsi)
	leaq	64(%rsi), %rdx
	movzbl	(%rsi), %eax
	testb	%al, %al
	je	.L1974
	movq	%rdx, -4456(%rbp)
	jmp	.L1825
	.p2align 4,,10
	.p2align 3
.L1833:
	leaq	-4240(%rbp), %r11
	movq	%r12, %rsi
	movq	%r8, -4560(%rbp)
	movq	%r11, %rdi
	movq	%r10, -4488(%rbp)
	movq	%r11, -4472(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor11ShouldVisitENS0_10HeapObjectE
	testb	%al, %al
	je	.L1829
	movq	-4488(%rbp), %r10
	movq	-4496(%rbp), %rdi
	movq	%r12, -4320(%rbp)
	movq	%r10, %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	-4560(%rbp), %r8
	leaq	7(%r12), %r10
	movq	%r12, %rsi
	movq	-4472(%rbp), %r11
	movq	%r10, %rcx
	movl	%eax, -4488(%rbp)
	movq	%r8, %rdx
	movq	%r10, -4600(%rbp)
	movq	%r11, %rdi
	movq	%r8, -4568(%rbp)
	movq	%r11, -4560(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	movslq	-4488(%rbp), %rax
	movq	%r12, %rsi
	movq	-4568(%rbp), %r8
	movq	-4600(%rbp), %r10
	movq	-4560(%rbp), %r11
	leaq	(%rax,%r8), %rcx
	movq	%rax, -4472(%rbp)
	movq	%r10, %rdx
	movq	%r11, %rdi
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES3_
	movq	-4472(%rbp), %rax
	addq	%rax, -4456(%rbp)
	jmp	.L1829
.L1834:
	movq	%r8, -4488(%rbp)
	leaq	-4424(%rbp), %rdi
	movq	%r10, -4424(%rbp)
	movzbl	7(%r10), %eax
	movb	%al, -4568(%rbp)
	call	_ZNK2v88internal3Map16UsedInstanceSizeEv
	movq	-4488(%rbp), %r8
	leaq	7(%r12), %r10
	movdqa	-4544(%rbp), %xmm3
	movl	%eax, -4560(%rbp)
	movq	%r10, %rdx
	movq	-4424(%rbp), %rax
	movq	-4496(%rbp), %rdi
	movq	%r8, %rsi
	movaps	%xmm3, -4320(%rbp)
	movq	%rax, -4472(%rbp)
	movl	$0, -4168(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES4_.constprop.0
	leaq	23(%r12), %rdx
	movq	%r10, %rsi
	call	_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES4_.constprop.1
	leaq	_ZN2v88internal18WasmInstanceObject19kTaggedFieldOffsetsE(%rip), %rcx
	movq	-4488(%rbp), %r8
	movl	-4560(%rbp), %r9d
	leaq	30(%rcx), %r11
	jmp	.L1938
	.p2align 4,,10
	.p2align 3
.L1936:
	movq	(%rdx), %rdi
	movq	-4312(%rbp), %rsi
	movq	%rdx, %xmm0
	addq	$2, %rcx
	movslq	(%rsi), %rax
	movq	%rdi, %xmm2
	punpcklqdq	%xmm2, %xmm0
	leal	1(%rax), %r10d
	salq	$4, %rax
	movl	%r10d, (%rsi)
	movups	%xmm0, 8(%rsi,%rax)
	cmpq	%r11, %rcx
	je	.L1937
.L1938:
	movzwl	(%rcx), %edx
	addq	%r8, %rdx
	cmpq	$-9, %rdx
	jbe	.L1936
	addq	$2, %rcx
	cmpq	%r11, %rcx
	jne	.L1938
.L1937:
	movq	-4472(%rbp), %rdi
	movq	-4496(%rbp), %r8
	movq	%r12, %rsi
	movl	%r9d, %ecx
	movl	$280, %edx
	call	_ZN2v88internal18BodyDescriptorBase23IterateJSObjectBodyImplINS0_24ConcurrentMarkingVisitor23SlotSnapshottingVisitorEEEvNS0_3MapENS0_10HeapObjectEiiPT_
	leaq	-4240(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rdi, -4472(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor11ShouldVisitENS0_10HeapObjectE
	testb	%al, %al
	je	.L1829
	movq	-4552(%rbp), %rdx
	movq	-4472(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal24ConcurrentMarkingVisitor23VisitPointersInSnapshotENS0_10HeapObjectERKNS0_12SlotSnapshotE
	movq	-4568(%rbp), %rax
	salq	$3, %rax
	andl	$2040, %eax
	addq	%rax, -4456(%rbp)
	jmp	.L1829
.L1838:
	leaq	-4240(%rbp), %r10
	movq	%r12, %rsi
	movq	%r8, -4488(%rbp)
	movq	%r10, %rdi
	movq	%r10, -4472(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor11ShouldVisitENS0_10HeapObjectE
	testb	%al, %al
	je	.L1829
	movq	-4488(%rbp), %r8
	movq	-4472(%rbp), %r10
	leaq	7(%r12), %rcx
	movq	%r12, %rsi
	movq	%r10, %rdi
	movq	%r8, %rdx
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	movq	-4472(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN2v88internal19FixedBodyDescriptorILi8ELi16ELi24EE11IterateBodyINS0_24ConcurrentMarkingVisitorEEEvNS0_3MapENS0_10HeapObjectEiPT_.isra.0
	addq	$24, -4456(%rbp)
	jmp	.L1829
.L1839:
	leaq	-4240(%rbp), %r11
	movq	%r12, %rsi
	movq	%r8, -4560(%rbp)
	movq	%r11, %rdi
	movq	%r10, -4488(%rbp)
	movq	%r11, -4472(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor11ShouldVisitENS0_10HeapObjectE
	testb	%al, %al
	je	.L1829
	movq	-4560(%rbp), %r8
	movq	-4472(%rbp), %r11
	leaq	7(%r12), %r9
	movq	%r12, %rsi
	movq	%r9, %rcx
	movq	%r9, -4600(%rbp)
	movq	%r8, %rdx
	movq	%r11, %rdi
	movq	%r8, -4568(%rbp)
	movq	%r11, -4560(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	movq	-4488(%rbp), %r10
	movq	-4496(%rbp), %rdi
	movq	%r12, -4320(%rbp)
	movq	%r10, %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	-4568(%rbp), %r8
	movq	-4600(%rbp), %r9
	movq	%r12, %rsi
	movq	-4560(%rbp), %r11
	cltq
	leaq	(%r8,%rax), %rcx
	movq	%r9, %rdx
	movq	%rax, -4472(%rbp)
	movq	%r11, %rdi
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES3_
	movslq	-4176(%rbp), %rax
	movq	-4216(%rbp), %rsi
	leaq	(%rax,%rax,4), %rax
	salq	$4, %rax
	leaq	(%rsi,%rax), %rdx
	movq	(%rdx), %rax
	movq	8(%rax), %rcx
	cmpq	$64, %rcx
	je	.L2200
	leaq	1(%rcx), %rdx
	movq	%rdx, 8(%rax)
	movq	%r12, 16(%rax,%rcx,8)
.L1932:
	movq	-4472(%rbp), %rsi
	addq	%rsi, -4456(%rbp)
	jmp	.L1829
.L1840:
	movdqa	-4544(%rbp), %xmm7
	movq	-4496(%rbp), %rdi
	movq	%r8, %rsi
	leaq	7(%r12), %rdx
	leaq	-4240(%rbp), %r11
	movl	$0, -4168(%rbp)
	movaps	%xmm7, -4320(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES4_.constprop.0
	leaq	15(%r12), %rsi
	leaq	23(%r12), %rdx
	call	_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES4_.constprop.1
	movq	%r12, %rsi
	movq	%r11, %rdi
	movq	%r11, -4472(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor11ShouldVisitENS0_10HeapObjectE
	testb	%al, %al
	je	.L1829
	movq	-4552(%rbp), %rdx
	movq	-4472(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal24ConcurrentMarkingVisitor23VisitPointersInSnapshotENS0_10HeapObjectERKNS0_12SlotSnapshotE
	addq	$24, -4456(%rbp)
	jmp	.L1829
.L1841:
	leaq	-4240(%rbp), %rdi
	movq	%r12, %rsi
	movq	%r8, -4488(%rbp)
	movq	%rdi, -4472(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor11ShouldVisitENS0_10HeapObjectE
	testb	%al, %al
	je	.L1829
	movq	-4488(%rbp), %r8
	leaq	7(%r12), %r10
	movq	-4472(%rbp), %rdi
	movq	%r12, %rsi
	movq	%r10, %rcx
	movq	%r10, -4560(%rbp)
	movq	%r8, %rdx
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	movq	-4560(%rbp), %r10
	leaq	47(%r12), %rcx
	movq	%r12, %rsi
	movq	-4472(%rbp), %rdi
	movq	%r10, %rdx
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	movq	-4472(%rbp), %rdi
	leaq	71(%r12), %rcx
	movq	%r12, %rsi
	leaq	55(%r12), %rdx
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	addq	$72, -4456(%rbp)
	jmp	.L1829
.L1836:
	leaq	-4240(%rbp), %r11
	movq	%r12, %rsi
	movq	%r8, -4560(%rbp)
	movq	%r11, %rdi
	movq	%r10, -4488(%rbp)
	movq	%r11, -4472(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor11ShouldVisitENS0_10HeapObjectE
	testb	%al, %al
	je	.L1829
	movq	-4488(%rbp), %r10
	movq	-4496(%rbp), %rdi
	movq	%r12, -4320(%rbp)
	movq	%r10, %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	-4560(%rbp), %r8
	leaq	7(%r12), %rcx
	movq	%r12, %rsi
	movq	-4472(%rbp), %r11
	movl	%eax, -4488(%rbp)
	movq	%r8, %rdx
	movq	%r8, -4568(%rbp)
	movq	%r11, %rdi
	movq	%r11, -4560(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	movslq	-4488(%rbp), %rax
	leaq	15(%r12), %rdx
	movq	%r12, %rsi
	movq	-4568(%rbp), %r8
	movq	-4560(%rbp), %r11
	movq	%rax, -4472(%rbp)
	leaq	(%rax,%r8), %rcx
	movq	%r11, %rdi
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	movq	-4472(%rbp), %rax
	addq	%rax, -4456(%rbp)
	jmp	.L1829
.L1837:
	leaq	-4240(%rbp), %rdi
	movq	%r12, %rsi
	movq	%r8, -4488(%rbp)
	movq	%rdi, -4472(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor11ShouldVisitENS0_10HeapObjectE
	testb	%al, %al
	je	.L1829
	movq	-4488(%rbp), %r8
	leaq	7(%r12), %r10
	movq	-4472(%rbp), %rdi
	movq	%r12, %rsi
	movq	%r10, %rcx
	movq	%r10, -4560(%rbp)
	movq	%r8, %rdx
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	movq	-4560(%rbp), %r10
	leaq	15(%r12), %rcx
	movq	%r12, %rsi
	movq	-4472(%rbp), %rdi
	movq	%r10, %rdx
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	movq	-4472(%rbp), %rdi
	leaq	31(%r12), %rcx
	movq	%r12, %rsi
	leaq	23(%r12), %rdx
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	addq	$32, -4456(%rbp)
	jmp	.L1829
.L1835:
	leaq	-4240(%rbp), %r11
	movq	%r12, %rsi
	movq	%r8, -4560(%rbp)
	movq	%r11, %rdi
	movq	%r10, -4488(%rbp)
	movq	%r11, -4472(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor11ShouldVisitENS0_10HeapObjectE
	testb	%al, %al
	je	.L1829
	movq	-4488(%rbp), %r10
	movq	-4496(%rbp), %rdi
	movq	%r12, -4320(%rbp)
	movq	%r10, %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	-4560(%rbp), %r8
	leaq	7(%r12), %rcx
	movq	%r12, %rsi
	movq	-4472(%rbp), %r11
	movl	%eax, -4488(%rbp)
	movq	%r8, %rdx
	movq	%r8, -4568(%rbp)
	movq	%r11, %rdi
	movq	%r11, -4560(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	movslq	-4488(%rbp), %rax
	leaq	31(%r12), %rdx
	movq	%r12, %rsi
	movq	-4568(%rbp), %r8
	movq	-4560(%rbp), %r11
	movq	%rax, -4472(%rbp)
	leaq	(%rax,%r8), %rcx
	movq	%r11, %rdi
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	movq	-4472(%rbp), %rax
	addq	%rax, -4456(%rbp)
	jmp	.L1829
.L1842:
	leaq	-4240(%rbp), %rdi
	movq	%r12, %rsi
	movq	%r8, -4488(%rbp)
	movq	%rdi, -4472(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor11ShouldVisitENS0_10HeapObjectE
	testb	%al, %al
	je	.L1829
	movq	-4488(%rbp), %r8
	movq	-4472(%rbp), %rdi
	leaq	7(%r12), %rcx
	movq	%r12, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	movq	-4472(%rbp), %rdi
	leaq	23(%r12), %rcx
	movq	%r12, %rsi
	leaq	15(%r12), %rdx
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	addq	$24, -4456(%rbp)
	jmp	.L1829
.L1843:
	leaq	-4240(%rbp), %rdi
	movq	%r12, %rsi
	movq	%r8, -4560(%rbp)
	movq	%r10, -4488(%rbp)
	movq	%rdi, -4472(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor11ShouldVisitENS0_10HeapObjectE
	testb	%al, %al
	je	.L1829
	movq	-4488(%rbp), %r10
	movq	%r12, %rsi
	movzbl	7(%r10), %eax
	movq	-4560(%rbp), %r8
	leaq	7(%r12), %r10
	movq	-4472(%rbp), %rdi
	movq	%r10, %rcx
	movq	%r10, -4600(%rbp)
	movq	%r8, %rdx
	movb	%al, -4488(%rbp)
	movq	%r8, -4568(%rbp)
	movq	%rdi, -4560(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	movq	-4488(%rbp), %rax
	movq	%r12, %rsi
	movq	-4568(%rbp), %r8
	movq	-4600(%rbp), %r10
	movq	-4560(%rbp), %rdi
	salq	$3, %rax
	andl	$2040, %eax
	movq	%r10, %rdx
	leaq	(%rax,%r8), %rcx
	movq	%rax, -4472(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	movq	-4472(%rbp), %rax
	addq	%rax, -4456(%rbp)
	jmp	.L1829
.L1844:
	leaq	-4240(%rbp), %rdi
	movq	%r12, %rsi
	movq	%r8, -4488(%rbp)
	movq	%rdi, -4472(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor11ShouldVisitENS0_10HeapObjectE
	testb	%al, %al
	je	.L1829
	movq	-4488(%rbp), %r8
	leaq	7(%r12), %r10
	movq	-4472(%rbp), %rdi
	movq	%r12, %rsi
	movq	%r10, %rcx
	movq	%r10, -4560(%rbp)
	movq	%r8, %rdx
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	movq	-4560(%rbp), %r10
	leaq	47(%r12), %r8
	movq	%r12, %rsi
	movq	-4472(%rbp), %rdi
	movq	%r8, %rcx
	movq	%r8, -4488(%rbp)
	movq	%r10, %rdx
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	movq	-4488(%rbp), %r8
	leaq	111(%r12), %rcx
	movq	%r12, %rsi
	movq	-4472(%rbp), %rdi
	movq	%r8, %rdx
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	addq	$112, -4456(%rbp)
	jmp	.L1829
.L1845:
	leaq	-4240(%rbp), %rdi
	movq	%r12, %rsi
	movq	%r8, -4488(%rbp)
	movq	%rdi, -4472(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor11ShouldVisitENS0_10HeapObjectE
	testb	%al, %al
	je	.L1829
	movzbl	17(%r12), %r10d
	movq	-4488(%rbp), %r8
	leaq	7(%r12), %rcx
	movq	%r12, %rsi
	movq	-4472(%rbp), %rdi
	leal	(%r10,%r10), %eax
	movq	%r8, %rdx
	movl	%r10d, -4568(%rbp)
	movl	%eax, -4560(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	movzbl	17(%r12), %edx
	movq	-4488(%rbp), %r8
	movq	%r12, %rsi
	movq	-4472(%rbp), %rdi
	leal	(%rdx,%rdx,2), %ecx
	leaq	23(%r12), %rdx
	sall	$4, %ecx
	addl	$24, %ecx
	movslq	%ecx, %rcx
	addq	%r8, %rcx
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	movl	-4560(%rbp), %eax
	movl	-4568(%rbp), %r10d
	leal	3(%rax,%rax,2), %edx
	leal	(%r10,%rdx,8), %edx
	leal	7(%rax,%rdx), %eax
	andl	$262136, %eax
	addq	%rax, -4456(%rbp)
	jmp	.L1829
.L1846:
	leaq	-4240(%rbp), %rdi
	movq	%r12, %rsi
	movq	%r8, -4488(%rbp)
	movq	%rdi, -4472(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor11ShouldVisitENS0_10HeapObjectE
	testb	%al, %al
	je	.L1829
	movq	-4488(%rbp), %r8
	movzbl	9(%r12), %eax
	leaq	7(%r12), %rcx
	movq	%r12, %rsi
	movq	-4472(%rbp), %rdi
	movq	%r8, %rdx
	movl	%eax, -4560(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	movzbl	9(%r12), %ecx
	leaq	15(%r12), %rdx
	movq	%r12, %rsi
	movq	-4488(%rbp), %r8
	movq	-4472(%rbp), %rdi
	addl	$1, %ecx
	sall	$4, %ecx
	movslq	%ecx, %rcx
	addq	%r8, %rcx
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	movl	-4560(%rbp), %eax
	leal	1(%rax), %edx
	sall	$4, %edx
	addl	%eax, %edx
	leal	7(%rdx,%rax,2), %eax
	andl	$65528, %eax
	addq	%rax, -4456(%rbp)
	jmp	.L1829
.L1847:
	leaq	-4240(%rbp), %rdi
	movq	%r12, %rsi
	movq	%r8, -4488(%rbp)
	movq	%rdi, -4472(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor11ShouldVisitENS0_10HeapObjectE
	testb	%al, %al
	je	.L1829
	movzbl	9(%r12), %r10d
	movq	-4488(%rbp), %r8
	leaq	7(%r12), %rcx
	movq	%r12, %rsi
	movq	-4472(%rbp), %rdi
	leal	(%r10,%r10), %eax
	movq	%r8, %rdx
	movl	%r10d, -4568(%rbp)
	movl	%eax, -4560(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	movzbl	9(%r12), %ecx
	leaq	15(%r12), %rdx
	movq	%r12, %rsi
	movq	-4488(%rbp), %r8
	movq	-4472(%rbp), %rdi
	sall	$5, %ecx
	addl	$16, %ecx
	movslq	%ecx, %rcx
	addq	%r8, %rcx
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	movl	-4560(%rbp), %eax
	movl	-4568(%rbp), %r10d
	leal	1(%rax), %edx
	sall	$4, %edx
	addl	%edx, %r10d
	leal	7(%rax,%r10), %eax
	andl	$131064, %eax
	addq	%rax, -4456(%rbp)
	jmp	.L1829
.L1848:
	movdqa	-4544(%rbp), %xmm6
	movq	-4496(%rbp), %rdi
	movq	%r8, %rsi
	leaq	7(%r12), %rdx
	leaq	-4240(%rbp), %r11
	movl	$0, -4168(%rbp)
	movaps	%xmm6, -4320(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES4_.constprop.0
	leaq	15(%r12), %rsi
	leaq	31(%r12), %rdx
	call	_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES4_.constprop.1
	movq	%r12, %rsi
	movq	%r11, %rdi
	movq	%r11, -4472(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor11ShouldVisitENS0_10HeapObjectE
	testb	%al, %al
	je	.L1829
	movq	-4552(%rbp), %rdx
	movq	-4472(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal24ConcurrentMarkingVisitor23VisitPointersInSnapshotENS0_10HeapObjectERKNS0_12SlotSnapshotE
	addq	$32, -4456(%rbp)
	jmp	.L1829
.L1849:
	movdqa	-4544(%rbp), %xmm4
	movq	-4496(%rbp), %rdi
	movq	%r8, %rsi
	leaq	7(%r12), %rdx
	leaq	-4240(%rbp), %r11
	movl	$0, -4168(%rbp)
	movaps	%xmm4, -4320(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES4_.constprop.0
	leaq	15(%r12), %rsi
	leaq	31(%r12), %rdx
	call	_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES4_.constprop.1
	movq	%r12, %rsi
	movq	%r11, %rdi
	movq	%r11, -4472(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor11ShouldVisitENS0_10HeapObjectE
	testb	%al, %al
	je	.L1829
	movq	-4552(%rbp), %rdx
	movq	-4472(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal24ConcurrentMarkingVisitor23VisitPointersInSnapshotENS0_10HeapObjectERKNS0_12SlotSnapshotE
	addq	$32, -4456(%rbp)
	jmp	.L1829
.L1850:
	leaq	-4240(%rbp), %rdi
	movq	%r12, %rdx
	movq	%r10, %rsi
	call	_ZN2v88internal24ConcurrentMarkingVisitor23VisitSharedFunctionInfoENS0_3MapENS0_18SharedFunctionInfoE.constprop.0
	cltq
	addq	%rax, -4456(%rbp)
	jmp	.L1829
.L1851:
	leaq	-4240(%rbp), %r11
	movq	%r12, %rsi
	movq	%r8, -4560(%rbp)
	movq	%r11, %rdi
	movq	%r10, -4488(%rbp)
	movq	%r11, -4472(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor11ShouldVisitENS0_10HeapObjectE
	testb	%al, %al
	je	.L1829
	movq	-4488(%rbp), %r10
	movq	-4496(%rbp), %rdi
	movq	%r12, -4320(%rbp)
	movq	%r10, %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	-4560(%rbp), %r8
	leaq	7(%r12), %r10
	movq	%r12, %rsi
	movq	-4472(%rbp), %r11
	movq	%r10, %rcx
	movl	%eax, -4488(%rbp)
	movq	%r8, %rdx
	movq	%r8, -4568(%rbp)
	movq	%r11, %rdi
	movq	%r10, -4600(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	movq	-4600(%rbp), %r10
	leaq	39(%r12), %r9
	movq	%r12, %rsi
	movq	-4472(%rbp), %r11
	movq	%r9, %rcx
	movq	%r9, -4560(%rbp)
	movq	%r10, %rdx
	movq	%r11, %rdi
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	movq	-4560(%rbp), %r9
	movq	-4472(%rbp), %r11
	movq	%r12, %rsi
	movq	%r9, %rdx
	movq	%r11, %rdi
	movq	%r11, -4560(%rbp)
	call	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE
	movslq	-4488(%rbp), %rax
	leaq	47(%r12), %rdx
	movq	%r12, %rsi
	movq	-4568(%rbp), %r8
	movq	-4560(%rbp), %r11
	movq	%rax, -4472(%rbp)
	leaq	(%r8,%rax), %rcx
	movq	%r11, %rdi
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	movq	-4472(%rbp), %rax
	addq	%rax, -4456(%rbp)
	jmp	.L1829
.L1852:
	leaq	-4240(%rbp), %rdi
	movq	%r12, %rsi
	movq	%r8, -4488(%rbp)
	movq	%rdi, -4472(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor11ShouldVisitENS0_10HeapObjectE
	testb	%al, %al
	je	.L1829
	movq	-4488(%rbp), %r8
	leaq	7(%r12), %r10
	movq	-4472(%rbp), %rdi
	movq	%r12, %rsi
	movq	%r10, %rcx
	movq	%r10, -4560(%rbp)
	movq	%r8, %rdx
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	movq	-4560(%rbp), %r10
	leaq	39(%r12), %rcx
	movq	%r12, %rsi
	movq	-4472(%rbp), %rdi
	movq	%r10, %rdx
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	addq	$40, -4456(%rbp)
	jmp	.L1829
.L1853:
	leaq	-4240(%rbp), %r11
	movq	%r12, %rsi
	movq	%r8, -4560(%rbp)
	movq	%r11, %rdi
	movq	%r10, -4488(%rbp)
	movq	%r11, -4472(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor11ShouldVisitENS0_10HeapObjectE
	testb	%al, %al
	je	.L1829
	movq	-4488(%rbp), %r10
	movq	-4496(%rbp), %rdi
	movq	%r12, -4320(%rbp)
	movq	%r10, %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	-4560(%rbp), %r8
	leaq	7(%r12), %rcx
	movq	%r12, %rsi
	movq	-4472(%rbp), %r11
	movl	%eax, -4488(%rbp)
	movq	%r8, %rdx
	movq	%r8, -4568(%rbp)
	movq	%r11, %rdi
	movq	%r11, -4560(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	movslq	-4488(%rbp), %rax
	leaq	15(%r12), %rdx
	movq	%r12, %rsi
	movq	-4568(%rbp), %r8
	movq	-4560(%rbp), %r11
	movq	%rax, -4472(%rbp)
	leaq	(%rax,%r8), %rcx
	movq	%r11, %rdi
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	movq	-4472(%rbp), %rax
	addq	%rax, -4456(%rbp)
	jmp	.L1829
.L1854:
	leaq	-4240(%rbp), %rdi
	movq	%r12, %rsi
	movq	%r8, -4488(%rbp)
	movq	%rdi, -4472(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor11ShouldVisitENS0_10HeapObjectE
	testb	%al, %al
	je	.L1829
	movl	7(%r12), %eax
	movl	11(%r12), %edx
	movq	%r12, %rsi
	leaq	7(%r12), %rcx
	movq	-4488(%rbp), %r8
	movq	-4472(%rbp), %rdi
	addl	$23, %eax
	andl	$-8, %eax
	leal	(%rax,%rdx,8), %eax
	movq	%r8, %rdx
	movl	%eax, -4560(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	movl	7(%r12), %esi
	movl	11(%r12), %edx
	movq	-4488(%rbp), %r8
	movq	-4472(%rbp), %rdi
	leal	23(%rsi), %eax
	movq	%r12, %rsi
	andl	$-8, %eax
	leal	(%rax,%rdx,8), %ecx
	cltq
	movslq	%ecx, %rcx
	leaq	(%rax,%r8), %rdx
	addq	%r8, %rcx
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	movslq	-4560(%rbp), %rax
	addq	%rax, -4456(%rbp)
	jmp	.L1829
.L1855:
	leaq	-4240(%rbp), %rdi
	movq	%r12, %rsi
	movq	%r8, -4488(%rbp)
	movq	%rdi, -4472(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor11ShouldVisitENS0_10HeapObjectE
	testb	%al, %al
	je	.L1829
	movq	-4488(%rbp), %r8
	movq	-4472(%rbp), %rdi
	leaq	7(%r12), %rcx
	movq	%r12, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	movq	-4472(%rbp), %rdi
	leaq	47(%r12), %rcx
	movq	%r12, %rsi
	leaq	15(%r12), %rdx
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	addq	$48, -4456(%rbp)
	jmp	.L1829
.L1856:
	leaq	-4240(%rbp), %rdi
	movq	%r12, %rsi
	movq	%r8, -4488(%rbp)
	movq	%rdi, -4472(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor11ShouldVisitENS0_10HeapObjectE
	testb	%al, %al
	je	.L1829
	movq	-4488(%rbp), %r8
	leaq	7(%r12), %r10
	movq	-4472(%rbp), %rdi
	movq	%r12, %rsi
	movq	%r10, %rcx
	movq	%r10, -4560(%rbp)
	movq	%r8, %rdx
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	movq	-4560(%rbp), %r10
	movq	-4472(%rbp), %rdi
	movq	%r12, %rsi
	leaq	1943(%r12), %rcx
	movq	%r10, %rdx
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	addq	$1976, -4456(%rbp)
	jmp	.L1829
.L1857:
	leaq	-4240(%rbp), %r10
	movq	%r12, %rsi
	movq	%r10, %rdi
	movq	%r10, -4472(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor11ShouldVisitENS0_10HeapObjectE
	testb	%al, %al
	je	.L1829
	cmpw	$1024, 11(%r12)
	movq	-4472(%rbp), %r10
	ja	.L2201
.L1916:
	leaq	71(%r12), %r8
	movq	%r10, %rdi
	leaq	23(%r12), %rdx
	movq	%r12, %rsi
	movq	%r8, %rcx
	movq	%r8, -4488(%rbp)
	movq	%r10, -4472(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	movq	-4488(%rbp), %r8
	movq	-4472(%rbp), %r10
	movq	%r12, %rsi
	movq	%r8, %rdx
	movq	%r10, %rdi
	call	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE
	addq	$80, -4456(%rbp)
	jmp	.L1829
.L1858:
	leaq	-4240(%rbp), %rax
	movq	%r8, -4488(%rbp)
	leaq	-4424(%rbp), %rdi
	movq	%r10, -4424(%rbp)
	movq	%rax, -4472(%rbp)
	movzbl	7(%r10), %esi
	movb	%sil, -4560(%rbp)
	call	_ZNK2v88internal3Map16UsedInstanceSizeEv
	movq	-4488(%rbp), %r8
	movdqa	-4544(%rbp), %xmm5
	leaq	7(%r12), %rdx
	movq	-4496(%rbp), %rdi
	movl	%eax, %r11d
	movq	-4424(%rbp), %r10
	movl	$0, -4168(%rbp)
	movq	%r8, %rsi
	movaps	%xmm5, -4320(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES4_.constprop.0
	movq	%r12, %rsi
	movl	$8, %edx
	movq	%rdi, %r8
	movl	%r11d, %ecx
	movq	%r10, %rdi
	call	_ZN2v88internal18BodyDescriptorBase23IterateJSObjectBodyImplINS0_24ConcurrentMarkingVisitor23SlotSnapshottingVisitorEEEvNS0_3MapENS0_10HeapObjectEiiPT_
	movq	-4472(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal24ConcurrentMarkingVisitor11ShouldVisitENS0_10HeapObjectE
	testb	%al, %al
	je	.L1829
	movq	-4552(%rbp), %rdx
	movq	-4472(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal24ConcurrentMarkingVisitor23VisitPointersInSnapshotENS0_10HeapObjectERKNS0_12SlotSnapshotE
	movq	-4560(%rbp), %rax
	salq	$3, %rax
	andl	$2040, %eax
	addq	%rax, -4456(%rbp)
	jmp	.L1829
.L1874:
	leaq	-4240(%rbp), %r11
	movq	%r12, %rsi
	movq	%r8, -4560(%rbp)
	movq	%r11, %rdi
	movq	%r10, -4488(%rbp)
	movq	%r11, -4472(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor11ShouldVisitENS0_10HeapObjectE
	testb	%al, %al
	je	.L1829
	movq	-4488(%rbp), %r10
	movq	-4496(%rbp), %rdi
	movq	%r12, -4320(%rbp)
	movq	%r10, %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	-4560(%rbp), %r8
	leaq	7(%r12), %r9
	movq	%r12, %rsi
	movq	-4472(%rbp), %r11
	movq	%r9, %rcx
	movl	%eax, -4488(%rbp)
	movq	%r8, %rdx
	movq	%r8, -4600(%rbp)
	movq	%r11, %rdi
	movq	%r9, -4608(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	movq	-4472(%rbp), %r11
	leaq	23(%r12), %r10
	movq	%r12, %rsi
	movq	-4608(%rbp), %r9
	movq	%r10, %rcx
	movq	%r10, -4568(%rbp)
	movq	%r11, %rdi
	movq	%r11, -4560(%rbp)
	movq	%r9, %rdx
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	movslq	-4488(%rbp), %rax
	movq	%r12, %rsi
	movq	-4600(%rbp), %r8
	movq	-4568(%rbp), %r10
	movq	-4560(%rbp), %r11
	leaq	(%r8,%rax), %rcx
	movq	%rax, -4472(%rbp)
	movq	%r10, %rdx
	movq	%r11, %rdi
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES3_
	movq	-4472(%rbp), %rax
	addq	%rax, -4456(%rbp)
	jmp	.L1829
.L1875:
	leaq	-4240(%rbp), %r11
	movq	%r12, %rsi
	movq	%r8, -4560(%rbp)
	movq	%r11, %rdi
	movq	%r10, -4488(%rbp)
	movq	%r11, -4472(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor11ShouldVisitENS0_10HeapObjectE
	testb	%al, %al
	je	.L1829
	movq	-4488(%rbp), %r10
	movq	-4496(%rbp), %rdi
	movq	%r12, -4320(%rbp)
	movq	%r10, %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	-4560(%rbp), %r8
	leaq	7(%r12), %rcx
	movq	%r12, %rsi
	movq	-4472(%rbp), %r11
	movl	%eax, -4488(%rbp)
	movq	%r8, %rdx
	movq	%r8, -4568(%rbp)
	movq	%r11, %rdi
	movq	%r11, -4560(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	movslq	-4488(%rbp), %rax
	leaq	15(%r12), %rdx
	movq	%r12, %rsi
	movq	-4568(%rbp), %r8
	movq	-4560(%rbp), %r11
	movq	%rax, -4472(%rbp)
	leaq	(%rax,%r8), %rcx
	movq	%r11, %rdi
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	movq	-4472(%rbp), %rax
	addq	%rax, -4456(%rbp)
	jmp	.L1829
.L1876:
	movdqa	-4544(%rbp), %xmm4
	movq	-4496(%rbp), %rdi
	movq	%r8, %rsi
	leaq	7(%r12), %rdx
	leaq	-4240(%rbp), %r11
	movl	$0, -4168(%rbp)
	movaps	%xmm4, -4320(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES4_.constprop.0
	leaq	15(%r12), %rsi
	leaq	31(%r12), %rdx
	call	_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES4_.constprop.1
	movq	%r12, %rsi
	movq	%r11, %rdi
	movq	%r11, -4472(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor11ShouldVisitENS0_10HeapObjectE
	testb	%al, %al
	je	.L1829
	movq	-4552(%rbp), %rdx
	movq	-4472(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal24ConcurrentMarkingVisitor23VisitPointersInSnapshotENS0_10HeapObjectERKNS0_12SlotSnapshotE
	addq	$32, -4456(%rbp)
	jmp	.L1829
.L1877:
	leaq	-4240(%rbp), %rdi
	movq	%r12, %rsi
	movq	%r8, -4488(%rbp)
	movq	%rdi, -4472(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor11ShouldVisitENS0_10HeapObjectE
	testb	%al, %al
	je	.L1829
	movq	-4488(%rbp), %r8
	leaq	7(%r12), %r10
	movq	-4472(%rbp), %rdi
	movq	%r12, %rsi
	movq	%r10, %rcx
	movq	%r10, -4560(%rbp)
	movq	%r8, %rdx
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	movq	-4560(%rbp), %r10
	movq	-4472(%rbp), %rdi
	movq	%r12, %rsi
	movq	%r10, %rcx
	movq	%r10, %rdx
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	addq	$24, -4456(%rbp)
	jmp	.L1829
.L1878:
	leaq	-4240(%rbp), %r10
	movq	%r12, %rsi
	movq	%r8, -4488(%rbp)
	movq	%r10, %rdi
	movq	%r10, -4472(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor11ShouldVisitENS0_10HeapObjectE
	testb	%al, %al
	je	.L1829
	testb	$1, 43(%r12)
	movl	39(%r12), %r9d
	leaq	39(%r12), %rax
	movq	-4472(%rbp), %r10
	movq	-4488(%rbp), %r8
	je	.L1893
	leal	71(%r9), %edx
	andl	$-8, %edx
	movslq	%edx, %rdx
	addq	%r12, %rdx
	movslq	-1(%rdx), %rcx
	leaq	7(%rdx,%rcx), %rcx
	leaq	63(%r12), %rdx
	movl	%ecx, %r9d
	subl	%edx, %r9d
.L1893:
	leaq	7(%r12), %r11
	movq	%r8, %rdx
	movq	%r12, %rsi
	movq	%r10, %rdi
	movq	%r11, %rcx
	movl	%r9d, -4568(%rbp)
	movq	%rax, -4560(%rbp)
	movq	%r11, -4488(%rbp)
	movq	%r10, -4472(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	movq	-4560(%rbp), %rax
	movq	-4488(%rbp), %r11
	movq	%r12, %rsi
	movq	-4472(%rbp), %r10
	movq	%rax, %rcx
	movq	%r11, %rdx
	movq	%r10, %rdi
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	movq	%r12, %rsi
	movq	-4496(%rbp), %r12
	movl	$1999, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal13RelocIteratorC1ENS0_4CodeEi@PLT
	movq	-4472(%rbp), %r10
	movq	%r12, %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal13ObjectVisitor14VisitRelocInfoEPNS0_13RelocIteratorE@PLT
	movl	-4568(%rbp), %r9d
	leal	7(%r9), %eax
	andl	$-8, %eax
	addl	$95, %eax
	andl	$-32, %eax
	cltq
	addq	%rax, -4456(%rbp)
	jmp	.L1829
.L1879:
	leaq	-4240(%rbp), %r10
	movq	%r12, %rsi
	movq	%r8, -4488(%rbp)
	movq	%r10, %rdi
	movq	%r10, -4472(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor11ShouldVisitENS0_10HeapObjectE
	testb	%al, %al
	je	.L1829
	movq	-4488(%rbp), %r8
	movq	-4472(%rbp), %r10
	leaq	7(%r12), %rcx
	movq	%r12, %rsi
	movq	%r10, %rdi
	movq	%r8, %rdx
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	movq	-4472(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN2v88internal19FixedBodyDescriptorILi8ELi16ELi16EE11IterateBodyINS0_24ConcurrentMarkingVisitorEEEvNS0_3MapENS0_10HeapObjectEiPT_.isra.0
	addq	$16, -4456(%rbp)
	jmp	.L1829
.L1880:
	leaq	-4240(%rbp), %rdi
	movq	%r12, %rsi
	movq	%r12, -4320(%rbp)
	movq	%rdi, -4472(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor11ShouldVisitENS0_10HeapObjectE
	testb	%al, %al
	je	.L1829
	movq	-4320(%rbp), %rax
	movq	7(%rax), %r12
	sarq	$32, %r12
	movq	-4320(%rbp), %rsi
	movq	-4472(%rbp), %rdi
	addl	$61, %r12d
	andl	$-8, %r12d
	leaq	7(%rsi), %rcx
	leaq	-1(%rsi), %rdx
	movl	%r12d, -4488(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	movq	-4320(%rbp), %r12
	movq	-4472(%rbp), %rdi
	leaq	15(%r12), %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE.constprop.1
	movq	-4472(%rbp), %rdi
	leaq	23(%r12), %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE.constprop.1
	movq	-4472(%rbp), %rdi
	leaq	31(%r12), %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE.constprop.1
	cmpb	$0, -72(%rbp)
	je	.L1890
	movslq	-4488(%rbp), %r12
	addq	%r12, -4456(%rbp)
	jmp	.L1829
.L1881:
	leaq	-4240(%rbp), %rdi
	movq	%r12, %rsi
	movq	%r8, -4560(%rbp)
	movq	%r10, -4488(%rbp)
	movq	%rdi, -4472(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor11ShouldVisitENS0_10HeapObjectE
	testb	%al, %al
	je	.L1829
	movq	-4488(%rbp), %r10
	movq	%r12, %rsi
	movzbl	7(%r10), %eax
	movq	-4560(%rbp), %r8
	leaq	7(%r12), %r10
	movq	-4472(%rbp), %rdi
	movq	%r10, %rcx
	movq	%r10, -4568(%rbp)
	movq	%r8, %rdx
	movb	%al, -4488(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	movq	-4568(%rbp), %r10
	leaq	31(%r12), %rcx
	movq	%r12, %rsi
	movq	-4472(%rbp), %rdi
	movq	%r10, %rdx
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	movq	-4488(%rbp), %rax
	salq	$3, %rax
	andl	$2040, %eax
	addq	%rax, -4456(%rbp)
	jmp	.L1829
.L1882:
	leaq	-4240(%rbp), %rdi
	movq	%r12, %rsi
	movq	%r8, -4488(%rbp)
	movq	%rdi, -4472(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor11ShouldVisitENS0_10HeapObjectE
	testb	%al, %al
	je	.L1829
	movq	-4488(%rbp), %r8
	movq	-4472(%rbp), %rdi
	leaq	7(%r12), %rcx
	movq	%r12, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	movl	11(%r12), %eax
	leal	23(%rax,%rax), %eax
	andl	$-8, %eax
	cltq
	addq	%rax, -4456(%rbp)
	jmp	.L1829
.L1883:
	leaq	-4240(%rbp), %rdi
	movq	%r12, %rsi
	movq	%r8, -4488(%rbp)
	movq	%rdi, -4472(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor11ShouldVisitENS0_10HeapObjectE
	testb	%al, %al
	je	.L1829
	movq	-4488(%rbp), %r8
	movq	-4472(%rbp), %rdi
	leaq	7(%r12), %rcx
	movq	%r12, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	movl	11(%r12), %eax
	addl	$23, %eax
	andl	$-8, %eax
	cltq
	addq	%rax, -4456(%rbp)
	jmp	.L1829
.L1884:
	leaq	7(%r12), %rax
	movq	%r8, -4560(%rbp)
	leaq	-4240(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rax, -4472(%rbp)
	movq	7(%r12), %rax
	movq	%rax, -4472(%rbp)
	movq	%rdi, -4488(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor11ShouldVisitENS0_10HeapObjectE
	testb	%al, %al
	je	.L1829
	movq	-4560(%rbp), %r8
	movq	-4488(%rbp), %rdi
	leaq	7(%r12), %rcx
	movq	%r12, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	movq	-4472(%rbp), %rax
	sarq	$32, %rax
	leal	16(,%rax,8), %eax
	cltq
	addq	%rax, -4456(%rbp)
	jmp	.L1829
.L1885:
	leaq	-4240(%rbp), %rdi
	movq	%r12, %rsi
	movq	%r8, -4560(%rbp)
	movq	%r10, -4488(%rbp)
	movq	%rdi, -4472(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor11ShouldVisitENS0_10HeapObjectE
	testb	%al, %al
	je	.L1829
	movq	-4488(%rbp), %r10
	leaq	7(%r12), %rcx
	movq	%r12, %rsi
	movzbl	7(%r10), %eax
	movq	-4560(%rbp), %r8
	movq	-4472(%rbp), %rdi
	movq	%r8, %rdx
	movb	%al, -4488(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	movq	-4488(%rbp), %rax
	salq	$3, %rax
	andl	$2040, %eax
	addq	%rax, -4456(%rbp)
	jmp	.L1829
.L1886:
	leaq	-4240(%rbp), %rdi
	movq	%r12, %rsi
	movq	%r8, -4488(%rbp)
	movq	%rdi, -4472(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor11ShouldVisitENS0_10HeapObjectE
	testb	%al, %al
	je	.L1829
	movq	7(%r12), %rax
	leaq	7(%r12), %rcx
	movq	%r12, %rsi
	movq	%rax, -4560(%rbp)
	movq	-4488(%rbp), %r8
	movq	-4472(%rbp), %rdi
	movq	%r8, %rdx
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	movq	-4560(%rbp), %rax
	sarq	$32, %rax
	addl	$23, %eax
	andl	$-8, %eax
	cltq
	addq	%rax, -4456(%rbp)
	jmp	.L1829
.L1887:
	leaq	-4240(%rbp), %rdi
	movq	%r12, %rsi
	movq	%r8, -4488(%rbp)
	movq	%rdi, -4472(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor11ShouldVisitENS0_10HeapObjectE
	testb	%al, %al
	je	.L1829
	movl	7(%r12), %eax
	leaq	7(%r12), %rcx
	movq	%r12, %rsi
	movl	%eax, -4560(%rbp)
	movq	-4488(%rbp), %r8
	movq	-4472(%rbp), %rdi
	movq	%r8, %rdx
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	movl	-4560(%rbp), %eax
	shrl	%eax
	leal	16(,%rax,8), %eax
	cltq
	addq	%rax, -4456(%rbp)
	jmp	.L1829
.L1866:
	leaq	-4240(%rbp), %rdi
	movq	%r12, %rdx
	movq	%r10, %rsi
	call	_ZN2v88internal24ConcurrentMarkingVisitor21VisitJSObjectSubclassINS0_8JSObjectENS3_14BodyDescriptorEEEiNS0_3MapET_
	movslq	%eax, %rcx
	testl	%ecx, %ecx
	je	.L1942
	cmpb	$0, -80(%rbp)
	jne	.L2202
.L1942:
	addq	%rcx, -4456(%rbp)
	jmp	.L1829
.L1867:
	leaq	-4240(%rbp), %rdi
	movq	%r12, %rsi
	movq	%r8, -4488(%rbp)
	movq	%rdi, -4472(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor11ShouldVisitENS0_10HeapObjectE
	testb	%al, %al
	je	.L1829
	movq	-4488(%rbp), %r8
	movq	-4472(%rbp), %rdi
	leaq	7(%r12), %rcx
	movq	%r12, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	movslq	11(%r12), %rax
	addq	%rax, -4456(%rbp)
	jmp	.L1829
.L1868:
	movq	%r12, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	testb	$1, %ah
	je	.L1899
	leaq	-4240(%rbp), %rdi
	movq	%r12, %rdx
	movq	%r10, %rsi
	call	_ZN2v88internal24ConcurrentMarkingVisitor30VisitFixedArrayWithProgressBarENS0_3MapENS0_10FixedArrayEPNS0_11MemoryChunkE
	cltq
	addq	%rax, -4456(%rbp)
	jmp	.L1829
.L1869:
	leaq	-4240(%rbp), %rdi
	movq	%r12, %rsi
	movq	%r8, -4488(%rbp)
	movq	%rdi, -4472(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor11ShouldVisitENS0_10HeapObjectE
	testb	%al, %al
	je	.L1829
	movq	-4488(%rbp), %r8
	movl	31(%r12), %eax
	leaq	7(%r12), %r10
	movq	%r12, %rsi
	movq	-4472(%rbp), %rdi
	movq	%r10, %rcx
	movq	%r10, -4600(%rbp)
	leal	48(,%rax,8), %eax
	movq	%r8, %rdx
	movq	%r8, -4568(%rbp)
	movl	%eax, -4560(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	movq	-4600(%rbp), %r10
	movq	-4472(%rbp), %rdi
	movq	%r12, %rsi
	movq	%r10, %rdx
	call	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE.constprop.1
	movq	-4472(%rbp), %rdi
	leaq	15(%r12), %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE
	movq	-4472(%rbp), %rdi
	leaq	23(%r12), %rdx
	movq	%r12, %rsi
	movq	%rdi, -4488(%rbp)
	call	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE.constprop.1
	movslq	-4560(%rbp), %rax
	leaq	47(%r12), %rdx
	movq	%r12, %rsi
	movq	-4568(%rbp), %r8
	movq	-4488(%rbp), %rdi
	movq	%rax, -4472(%rbp)
	leaq	(%r8,%rax), %rcx
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES3_
	movq	-4472(%rbp), %rax
	addq	%rax, -4456(%rbp)
	jmp	.L1829
.L1870:
	leaq	-4240(%rbp), %r10
	movq	%r12, %rsi
	movq	%r8, -4488(%rbp)
	movq	%r10, %rdi
	movq	%r10, -4472(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor11ShouldVisitENS0_10HeapObjectE
	testb	%al, %al
	je	.L1829
	movq	-4488(%rbp), %r8
	movq	-4472(%rbp), %r10
	leaq	7(%r12), %rcx
	movq	%r12, %rsi
	movq	%r10, %rdi
	movq	%r8, %rdx
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	movq	-4472(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN2v88internal19FixedBodyDescriptorILi8ELi16ELi24EE11IterateBodyINS0_24ConcurrentMarkingVisitorEEEvNS0_3MapENS0_10HeapObjectEiPT_.isra.0
	addq	$24, -4456(%rbp)
	jmp	.L1829
.L1871:
	leaq	-4240(%rbp), %rdi
	movq	%r12, %rdx
	movq	%r10, %rsi
	call	_ZN2v88internal24ConcurrentMarkingVisitor23VisitEphemeronHashTableENS0_3MapENS0_18EphemeronHashTableE.constprop.0
	cltq
	addq	%rax, -4456(%rbp)
	jmp	.L1829
.L1872:
	leaq	-4240(%rbp), %r11
	movq	%r12, %rsi
	movq	%r8, -4560(%rbp)
	movq	%r11, %rdi
	movq	%r10, -4488(%rbp)
	movq	%r11, -4472(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor11ShouldVisitENS0_10HeapObjectE
	testb	%al, %al
	je	.L1829
	movq	-4488(%rbp), %r10
	movq	-4496(%rbp), %rdi
	movq	%r12, -4320(%rbp)
	movq	%r10, %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	-4560(%rbp), %r8
	leaq	7(%r12), %rcx
	movq	%r12, %rsi
	movq	-4472(%rbp), %r11
	movl	%eax, -4488(%rbp)
	movq	%r8, %rdx
	movq	%r8, -4568(%rbp)
	movq	%r11, %rdi
	movq	%r11, -4560(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	movslq	-4488(%rbp), %rax
	leaq	15(%r12), %rdx
	movq	%r12, %rsi
	movq	-4568(%rbp), %r8
	movq	-4560(%rbp), %r11
	movq	%rax, -4472(%rbp)
	leaq	(%rax,%r8), %rcx
	movq	%r11, %rdi
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	movq	-4472(%rbp), %rax
	addq	%rax, -4456(%rbp)
	jmp	.L1829
.L1873:
	leaq	-4240(%rbp), %r11
	movq	%r12, %rsi
	movq	%r8, -4560(%rbp)
	movq	%r11, %rdi
	movq	%r10, -4488(%rbp)
	movq	%r11, -4472(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor11ShouldVisitENS0_10HeapObjectE
	testb	%al, %al
	je	.L1829
	movq	-4560(%rbp), %r8
	movq	-4472(%rbp), %r11
	leaq	7(%r12), %rcx
	movq	%r12, %rsi
	movq	%r8, %rdx
	movq	%r11, %rdi
	movq	%r11, -4560(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	movq	-4488(%rbp), %r10
	movq	-4496(%rbp), %rdi
	movq	%r12, -4320(%rbp)
	movq	%r10, %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	-4560(%rbp), %r11
	leaq	23(%r12), %rcx
	movq	%r12, %rsi
	leaq	15(%r12), %rdx
	movl	%eax, -4472(%rbp)
	movq	%r11, %rdi
	movq	%r11, -4488(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	movswl	9(%r12), %edx
	movq	%r12, %rsi
	movq	-4488(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN2v88internal24ConcurrentMarkingVisitor16VisitDescriptorsENS0_15DescriptorArrayEi
	movslq	-4472(%rbp), %rax
	addq	%rax, -4456(%rbp)
	jmp	.L1829
.L1862:
	leaq	-4240(%rbp), %rdi
	movq	%r12, %rdx
	movq	%r10, %rsi
	call	_ZN2v88internal24ConcurrentMarkingVisitor21VisitJSObjectSubclassINS0_8JSObjectENS3_14BodyDescriptorEEEiNS0_3MapET_
	cltq
	addq	%rax, -4456(%rbp)
	jmp	.L1829
.L1863:
	leaq	-4240(%rbp), %rax
	movq	%r8, -4488(%rbp)
	leaq	-4424(%rbp), %rdi
	movq	%r10, -4424(%rbp)
	movq	%rax, -4472(%rbp)
	movq	%r12, -4432(%rbp)
	movzbl	7(%r10), %esi
	movb	%sil, -4560(%rbp)
	call	_ZNK2v88internal3Map16UsedInstanceSizeEv
	movq	-4488(%rbp), %r8
	movdqa	-4544(%rbp), %xmm7
	leaq	7(%r12), %rdx
	movq	-4496(%rbp), %rdi
	movl	%eax, %r11d
	movq	-4424(%rbp), %r10
	movl	$0, -4168(%rbp)
	movq	%r8, %rsi
	movaps	%xmm7, -4320(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES4_.constprop.0
	movq	%r12, %rsi
	movl	$8, %edx
	movq	%rdi, %r8
	movl	%r11d, %ecx
	movq	%r10, %rdi
	call	_ZN2v88internal18BodyDescriptorBase23IterateJSObjectBodyImplINS0_24ConcurrentMarkingVisitor23SlotSnapshottingVisitorEEEvNS0_3MapENS0_10HeapObjectEiiPT_
	movq	-4472(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal24ConcurrentMarkingVisitor11ShouldVisitENS0_10HeapObjectE
	testb	%al, %al
	je	.L1907
	movq	-4552(%rbp), %rdx
	movq	-4472(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal24ConcurrentMarkingVisitor23VisitPointersInSnapshotENS0_10HeapObjectERKNS0_12SlotSnapshotE
	movq	-4560(%rbp), %rax
	salq	$3, %rax
	andl	$2040, %eax
	addq	%rax, -4456(%rbp)
.L1907:
	movl	-68(%rbp), %eax
	testl	%eax, %eax
	je	.L1829
	leaq	-4432(%rbp), %rdi
	call	_ZN2v88internal10JSFunction30NeedsResetDueToFlushedBytecodeEv
	testb	%al, %al
	je	.L1829
	movslq	-4176(%rbp), %rax
	movq	-4216(%rbp), %rdx
	movq	-4432(%rbp), %r8
	leaq	(%rax,%rax,4), %rsi
	salq	$4, %rsi
	addq	%rdx, %rsi
	movq	6960(%rsi), %rax
	movq	8(%rax), %r12
	cmpq	$64, %r12
	je	.L2203
	leaq	1(%r12), %rdx
	movq	%rdx, 8(%rax)
	movq	%r8, 16(%rax,%r12,8)
	jmp	.L1829
	.p2align 4,,10
	.p2align 3
.L1864:
	leaq	-4240(%rbp), %rax
	movq	%r8, -4560(%rbp)
	leaq	-4424(%rbp), %rdi
	movq	%r10, -4424(%rbp)
	movq	%rax, -4472(%rbp)
	movzbl	7(%r10), %esi
	movb	%sil, -4568(%rbp)
	call	_ZNK2v88internal3Map16UsedInstanceSizeEv
	movq	-4560(%rbp), %r8
	leaq	7(%r12), %r9
	movdqa	-4544(%rbp), %xmm6
	movq	-4496(%rbp), %rdi
	movq	%r9, %rdx
	movq	-4424(%rbp), %r10
	movl	%eax, %r11d
	movq	%r8, %rsi
	movaps	%xmm6, -4320(%rbp)
	movq	%r9, -4488(%rbp)
	movl	$0, -4168(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES4_.constprop.0
	movq	-4488(%rbp), %r9
	leaq	31(%r12), %rdx
	movq	%r9, %rsi
	call	_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES4_.constprop.1
	movq	%r12, %rsi
	movl	$56, %edx
	movq	%rdi, %r8
	movl	%r11d, %ecx
	movq	%r10, %rdi
	call	_ZN2v88internal18BodyDescriptorBase23IterateJSObjectBodyImplINS0_24ConcurrentMarkingVisitor23SlotSnapshottingVisitorEEEvNS0_3MapENS0_10HeapObjectEiiPT_
	movq	-4472(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal24ConcurrentMarkingVisitor11ShouldVisitENS0_10HeapObjectE
	testb	%al, %al
	je	.L1829
	movzbl	-4568(%rbp), %eax
	movq	-4552(%rbp), %rdx
	movq	%r12, %rsi
	movq	-4472(%rbp), %rdi
	sall	$3, %eax
	movl	%eax, -4488(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor23VisitPointersInSnapshotENS0_10HeapObjectERKNS0_12SlotSnapshotE
	movslq	-4488(%rbp), %rax
	testl	%eax, %eax
	je	.L1829
	addq	%rax, -4456(%rbp)
	cmpb	$0, -80(%rbp)
	je	.L1829
	movl	-4200(%rbp), %esi
	movq	-4208(%rbp), %rdi
	movq	%r12, %rdx
	call	_ZN2v88internal8WorklistINS0_10HeapObjectELi16EE4PushEiS2_
	jmp	.L1829
.L1865:
	leaq	-4240(%rbp), %rax
	movq	%r8, -4560(%rbp)
	leaq	-4424(%rbp), %rdi
	movq	%r10, -4424(%rbp)
	movq	%rax, -4472(%rbp)
	movzbl	7(%r10), %esi
	movb	%sil, -4568(%rbp)
	call	_ZNK2v88internal3Map16UsedInstanceSizeEv
	movq	-4560(%rbp), %r8
	leaq	7(%r12), %r9
	movdqa	-4544(%rbp), %xmm5
	movq	-4496(%rbp), %rdi
	movq	%r9, %rdx
	movq	-4424(%rbp), %r10
	movl	%eax, %r11d
	movq	%r8, %rsi
	movaps	%xmm5, -4320(%rbp)
	movq	%r9, -4488(%rbp)
	movl	$0, -4168(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES4_.constprop.0
	movq	-4488(%rbp), %r9
	leaq	23(%r12), %rdx
	movq	%r9, %rsi
	call	_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES4_.constprop.1
	movq	%r12, %rsi
	movl	$48, %edx
	movq	%rdi, %r8
	movl	%r11d, %ecx
	movq	%r10, %rdi
	call	_ZN2v88internal18BodyDescriptorBase23IterateJSObjectBodyImplINS0_24ConcurrentMarkingVisitor23SlotSnapshottingVisitorEEEvNS0_3MapENS0_10HeapObjectEiiPT_
	movq	-4472(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal24ConcurrentMarkingVisitor11ShouldVisitENS0_10HeapObjectE
	testb	%al, %al
	je	.L1829
	movzbl	-4568(%rbp), %eax
	movq	-4552(%rbp), %rdx
	movq	%r12, %rsi
	movq	-4472(%rbp), %rdi
	sall	$3, %eax
	movl	%eax, -4488(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor23VisitPointersInSnapshotENS0_10HeapObjectERKNS0_12SlotSnapshotE
	movslq	-4488(%rbp), %rax
	testl	%eax, %eax
	je	.L1829
	addq	%rax, -4456(%rbp)
	cmpb	$0, -80(%rbp)
	je	.L1829
	movl	-4200(%rbp), %esi
	movq	-4208(%rbp), %rdi
	movq	%r12, %rdx
	call	_ZN2v88internal8WorklistINS0_10HeapObjectELi16EE4PushEiS2_
	jmp	.L1829
.L1860:
	movq	%r8, -4488(%rbp)
	leaq	-4424(%rbp), %rdi
	movq	%r10, -4424(%rbp)
	movzbl	7(%r10), %eax
	movb	%al, -4560(%rbp)
	call	_ZNK2v88internal3Map16UsedInstanceSizeEv
	movq	-4488(%rbp), %r8
	leaq	7(%r12), %r9
	movdqa	-4544(%rbp), %xmm4
	movq	-4496(%rbp), %rdi
	movq	%r9, %rdx
	movl	%eax, %r11d
	movq	%r9, -4472(%rbp)
	movq	%r8, %rsi
	movq	-4424(%rbp), %r10
	movaps	%xmm4, -4320(%rbp)
	movl	$0, -4168(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES4_.constprop.0
	movq	-4472(%rbp), %r9
	leaq	31(%r12), %rdx
	movq	%r9, %rsi
	call	_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES4_.constprop.1
	leaq	63(%r12), %rdx
	leaq	71(%r12), %rax
	cmpq	%rax, %rdx
	jnb	.L1975
	movq	63(%r12), %rdi
	movq	-4312(%rbp), %rcx
	movq	%rdx, %xmm0
	movslq	(%rcx), %rax
	movq	%rdi, %xmm7
	punpcklqdq	%xmm7, %xmm0
	leal	1(%rax), %esi
	salq	$4, %rax
	movl	%esi, (%rcx)
	movups	%xmm0, 8(%rcx,%rax)
.L1975:
	movq	-4496(%rbp), %r8
	movq	%r12, %rsi
	movq	%r10, %rdi
	movl	%r11d, %ecx
	movl	$72, %edx
	call	_ZN2v88internal18BodyDescriptorBase23IterateJSObjectBodyImplINS0_24ConcurrentMarkingVisitor23SlotSnapshottingVisitorEEEvNS0_3MapENS0_10HeapObjectEiiPT_
	leaq	-4240(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rdi, -4472(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor11ShouldVisitENS0_10HeapObjectE
	testb	%al, %al
	je	.L1829
	movzbl	-4560(%rbp), %eax
	movq	-4552(%rbp), %rdx
	movq	%r12, %rsi
	movq	-4472(%rbp), %rdi
	sall	$3, %eax
	movl	%eax, -4488(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor23VisitPointersInSnapshotENS0_10HeapObjectERKNS0_12SlotSnapshotE
	movslq	-4488(%rbp), %rax
	testl	%eax, %eax
	je	.L1829
	addq	%rax, -4456(%rbp)
	cmpb	$0, -80(%rbp)
	je	.L1829
	movl	-4200(%rbp), %esi
	movq	-4208(%rbp), %rdi
	movq	%r12, %rdx
	call	_ZN2v88internal8WorklistINS0_10HeapObjectELi16EE4PushEiS2_
	jmp	.L1829
.L1861:
	leaq	-4240(%rbp), %rax
	movq	%r8, -4488(%rbp)
	leaq	-4424(%rbp), %rdi
	movq	%r10, -4424(%rbp)
	movq	%rax, -4472(%rbp)
	movzbl	7(%r10), %esi
	movb	%sil, -4560(%rbp)
	call	_ZNK2v88internal3Map16UsedInstanceSizeEv
	movq	-4488(%rbp), %r8
	leaq	7(%r12), %r11
	movdqa	-4544(%rbp), %xmm5
	movq	-4496(%rbp), %rdi
	movq	%r11, %rdx
	movl	%eax, %r10d
	movl	$0, -4168(%rbp)
	movq	%r8, %rsi
	movaps	%xmm5, -4320(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES4_.constprop.0
	movq	-4488(%rbp), %r8
	movslq	%r10d, %rdx
	movq	%r11, %rsi
	addq	%r8, %rdx
	call	_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES4_.constprop.1
	movq	-4472(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal24ConcurrentMarkingVisitor11ShouldVisitENS0_10HeapObjectE
	testb	%al, %al
	je	.L1829
	movq	-4552(%rbp), %rdx
	movq	-4472(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal24ConcurrentMarkingVisitor23VisitPointersInSnapshotENS0_10HeapObjectERKNS0_12SlotSnapshotE
	movq	-4560(%rbp), %rax
	salq	$3, %rax
	andl	$2040, %eax
	addq	%rax, -4456(%rbp)
	jmp	.L1829
.L1859:
	leaq	-4240(%rbp), %rdi
	movq	%r12, %rdx
	movq	%r10, %rsi
	call	_ZN2v88internal24ConcurrentMarkingVisitor14VisitJSWeakRefENS0_3MapENS0_9JSWeakRefE
	cltq
	addq	%rax, -4456(%rbp)
	jmp	.L1829
	.p2align 4,,10
	.p2align 3
.L1985:
	movq	16(%rbx), %rdi
	movq	%r12, %rdx
	movl	%r15d, %esi
	call	_ZN2v88internal8WorklistINS0_10HeapObjectELi64EE4PushEiS2_
	jmp	.L1829
	.p2align 4,,10
	.p2align 3
.L2199:
	movq	(%r12), %rdx
	cmpq	$0, 8(%rdx)
	je	.L2204
	movq	%rax, %xmm0
	movq	%rdx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rcx,%r13)
	movq	8(%r12), %rax
.L1821:
	movq	8(%rax), %r12
	movq	$-1, %r8
	testq	%r12, %r12
	je	.L1820
	leaq	-1(%r12), %rdx
	movq	%rdx, 8(%rax)
	movq	8(%rax,%r12,8), %r12
	leaq	-1(%r12), %r8
	jmp	.L1820
	.p2align 4,,10
	.p2align 3
.L2204:
	movq	680(%rcx), %rax
	leaq	640(%rcx), %rdi
	movq	%rcx, -4472(%rbp)
	testq	%rax, %rax
	je	.L2191
	movq	%rdi, -4488(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-4472(%rbp), %rcx
	movq	-4488(%rbp), %rdi
	movq	680(%rcx), %rax
	testq	%rax, %rax
	jne	.L1823
	movq	%r13, -4464(%rbp)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
.L1822:
	movq	-4456(%rbp), %rsi
	addq	%rsi, -4480(%rbp)
	movq	-4520(%rbp), %rsi
	movq	-4480(%rbp), %rax
	leaq	64(%rsi), %rdi
	movq	%rdi, -4456(%rbp)
	movq	%rax, 64(%rsi)
	movzbl	(%rsi), %eax
	movb	$1, -4521(%rbp)
	testb	%al, %al
	je	.L1826
.L1825:
	movq	_ZZN2v88internal17ConcurrentMarking3RunEiPNS1_9TaskStateEE28trace_event_unique_atomic811(%rip), %r13
	testq	%r13, %r13
	je	.L2205
.L1946:
	movq	$0, -4320(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L2206
.L1948:
	movq	-4496(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	cmpb	$0, -4521(%rbp)
	jne	.L1826
.L1950:
	movl	-4500(%rbp), %r15d
	movq	8(%rbx), %rdi
	movl	%r15d, %esi
	call	_ZN2v88internal8WorklistINS0_10HeapObjectELi64EE13FlushToGlobalEi
	movq	16(%rbx), %rdi
	movl	%r15d, %esi
	call	_ZN2v88internal8WorklistINS0_10HeapObjectELi64EE13FlushToGlobalEi
	movq	32(%rbx), %r13
	movq	-4464(%rbp), %rax
	leaq	0(%r13,%rax), %r14
	movq	(%r14), %r12
	cmpq	$0, 8(%r12)
	jne	.L2207
.L1954:
	movq	8(%r14), %r12
	cmpq	$0, 8(%r12)
	jne	.L2208
.L1955:
	movq	24(%rbx), %r13
	movq	-4464(%rbp), %rax
	leaq	0(%r13,%rax), %r14
	movq	(%r14), %r12
	cmpq	$0, 8(%r12)
	jne	.L2209
.L1956:
	movq	8(%r14), %r12
	cmpq	$0, 8(%r12)
	jne	.L2210
.L1957:
	movq	24(%rbx), %r13
	movq	-4464(%rbp), %rax
	leaq	0(%r13,%rax), %r14
	movq	696(%r14), %r12
	cmpq	$0, 8(%r12)
	jne	.L2211
.L1958:
	movq	704(%r14), %r12
	cmpq	$0, 8(%r12)
	jne	.L2212
.L1959:
	movl	-4500(%rbp), %r15d
	movq	24(%rbx), %rax
	movl	%r15d, %esi
	leaq	1392(%rax), %rdi
	call	_ZN2v88internal8WorklistINS0_9EphemeronELi64EE13FlushToGlobalEi
	movq	24(%rbx), %rax
	movl	%r15d, %esi
	leaq	2088(%rax), %rdi
	call	_ZN2v88internal8WorklistINS0_9EphemeronELi64EE13FlushToGlobalEi
	movq	24(%rbx), %rax
	movl	%r15d, %esi
	leaq	2784(%rax), %rdi
	call	_ZN2v88internal8WorklistINS0_9EphemeronELi64EE13FlushToGlobalEi
	movq	24(%rbx), %r12
	movq	-4464(%rbp), %rax
	leaq	(%r12,%rax), %r14
	movq	3480(%r14), %r13
	cmpq	$0, 8(%r13)
	jne	.L2213
.L1960:
	movq	3488(%r14), %r13
	cmpq	$0, 8(%r13)
	jne	.L2214
.L1961:
	movq	24(%rbx), %r13
	movq	-4464(%rbp), %rax
	leaq	0(%r13,%rax), %r14
	movq	4872(%r14), %r12
	cmpq	$0, 8(%r12)
	jne	.L2215
.L1962:
	movq	4880(%r14), %r12
	cmpq	$0, 8(%r12)
	jne	.L2216
.L1963:
	movq	24(%rbx), %r13
	movq	-4464(%rbp), %rax
	leaq	0(%r13,%rax), %r14
	movq	5568(%r14), %r12
	cmpq	$0, 8(%r12)
	jne	.L2217
.L1964:
	movq	5576(%r14), %r12
	cmpq	$0, 8(%r12)
	jne	.L2218
.L1965:
	movq	24(%rbx), %r13
	movq	-4464(%rbp), %rax
	leaq	0(%r13,%rax), %r14
	movq	4176(%r14), %r12
	cmpq	$0, 8(%r12)
	jne	.L2219
.L1966:
	movq	4184(%r14), %r12
	cmpq	$0, 8(%r12)
	jne	.L2220
.L1967:
	movq	24(%rbx), %r13
	movq	-4464(%rbp), %rax
	leaq	0(%r13,%rax), %r14
	movq	6264(%r14), %r12
	cmpq	$0, 8(%r12)
	jne	.L2221
.L1968:
	movq	6272(%r14), %r12
	cmpq	$0, 8(%r12)
	jne	.L2222
.L1969:
	movq	24(%rbx), %r13
	movq	-4464(%rbp), %r15
	addq	%r13, %r15
	movq	6960(%r15), %r12
	cmpq	$0, 8(%r12)
	jne	.L2223
.L1970:
	movq	6968(%r15), %r12
	cmpq	$0, 8(%r12)
	jne	.L2224
.L1971:
	movq	-4456(%rbp), %rax
	movq	$0, (%rax)
	movq	-4480(%rbp), %rax
	lock addq	%rax, 1192(%rbx)
	cmpb	$0, -4522(%rbp)
	jne	.L2225
.L1972:
	leaq	1208(%rbx), %r12
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-4512(%rbp), %rax
	leaq	1248(%rbx), %rdi
	movb	$0, 1300(%rbx,%rax)
	subl	$1, 1296(%rbx)
	call	_ZN2v84base17ConditionVariable9NotifyAllEv@PLT
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	call	_ZN2v88internal10TimedScope11TimestampMsEv
	cmpb	$0, _ZN2v88internal29FLAG_trace_concurrent_markingE(%rip)
	jne	.L2226
.L1973:
	leaq	-4416(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-4584(%rbp), %rdi
	call	_ZN2v88internal8GCTracer15BackgroundScopeD1Ev@PLT
	movq	-4576(%rbp), %rdi
	call	_ZN2v88internal33WorkerThreadRuntimeCallStatsScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2227
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2191:
	.cfi_restore_state
	movq	%r13, -4464(%rbp)
	jmp	.L1822
	.p2align 4,,10
	.p2align 3
.L1823:
	movq	(%rax), %rdx
	movq	%rax, -4472(%rbp)
	movq	%rdx, 680(%rcx)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	8(%r12), %rdi
	movq	-4472(%rbp), %rax
	testq	%rdi, %rdi
	je	.L1824
	movl	$528, %esi
	call	_ZdlPvm@PLT
	movq	-4472(%rbp), %rax
.L1824:
	movq	%rax, 8(%r12)
	jmp	.L1821
.L1899:
	leaq	7(%r12), %rcx
	movq	%r8, -4568(%rbp)
	leaq	-4240(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rcx, -4560(%rbp)
	movq	7(%r12), %rax
	movq	%rax, -4472(%rbp)
	movq	%rdi, -4488(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor11ShouldVisitENS0_10HeapObjectE
	testb	%al, %al
	je	.L1829
	movq	-4568(%rbp), %r8
	movq	-4560(%rbp), %rcx
	movq	%r12, %rsi
	movq	-4488(%rbp), %rdi
	movq	%r8, %rdx
	movq	%r8, -4560(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	movq	-4472(%rbp), %rax
	leaq	15(%r12), %rdx
	movq	%r12, %rsi
	movq	-4560(%rbp), %r8
	movq	-4488(%rbp), %rdi
	sarq	$32, %rax
	leal	16(,%rax,8), %eax
	cltq
	leaq	(%r8,%rax), %rcx
	movq	%rax, -4472(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	movq	-4472(%rbp), %rax
	addq	%rax, -4456(%rbp)
	jmp	.L1829
.L2202:
	movl	-4200(%rbp), %esi
	movq	-4208(%rbp), %rdi
	movq	%r12, %rdx
	movl	%ecx, -4472(%rbp)
	call	_ZN2v88internal8WorklistINS0_10HeapObjectELi16EE4PushEiS2_
	movslq	-4472(%rbp), %rcx
	addq	%rcx, -4456(%rbp)
	jmp	.L1829
.L1890:
	movq	-4496(%rbp), %rdi
	call	_ZN2v88internal13BytecodeArray9MakeOlderEv@PLT
	movslq	-4488(%rbp), %r12
	addq	%r12, -4456(%rbp)
	jmp	.L1829
.L2201:
	movq	39(%r12), %rsi
	movq	%r10, %rdi
	movq	%r10, -4472(%rbp)
	movq	%rsi, -4488(%rbp)
	call	_ZN2v88internal24ConcurrentMarkingVisitor24MarkDescriptorArrayBlackENS0_15DescriptorArrayE
	movl	15(%r12), %eax
	movq	-4472(%rbp), %r10
	movq	-4488(%rbp), %rsi
	shrl	$10, %eax
	andl	$1023, %eax
	je	.L1916
	movswl	9(%rsi), %edx
	movq	%r10, %rdi
	cmpl	%eax, %edx
	cmovg	%eax, %edx
	call	_ZN2v88internal24ConcurrentMarkingVisitor16VisitDescriptorsENS0_15DescriptorArrayEi
	movq	-4472(%rbp), %r10
	jmp	.L1916
.L1826:
	pxor	%xmm0, %xmm0
	movzbl	-4522(%rbp), %r13d
	movl	-4500(%rbp), %r14d
	leaq	-4240(%rbp), %r12
	movq	-4496(%rbp), %r15
	movaps	%xmm0, -4320(%rbp)
	jmp	.L1951
	.p2align 4,,10
	.p2align 3
.L2228:
	movq	-4312(%rbp), %rdx
	movq	-4320(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal24ConcurrentMarkingVisitor16ProcessEphemeronENS0_10HeapObjectES2_
	testb	%al, %al
	cmovne	%eax, %r13d
.L1951:
	movq	24(%rbx), %rax
	movq	%r15, %rdx
	movl	%r14d, %esi
	leaq	2784(%rax), %rdi
	call	_ZN2v88internal8WorklistINS0_9EphemeronELi64EE3PopEiPS2_
	testb	%al, %al
	jne	.L2228
	movb	%r13b, -4522(%rbp)
	jmp	.L1950
.L2195:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2229
.L1806:
	movq	%r12, _ZZN2v88internal17ConcurrentMarking3RunEiPNS1_9TaskStateEE28trace_event_unique_atomic753(%rip)
	jmp	.L1805
.L2197:
	movl	-4500(%rbp), %edx
	subq	$37592, %rdi
	leaq	.LC9(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal7Isolate18PrintWithTimestampEPKcz@PLT
	jmp	.L1813
.L2196:
	movl	$5, %edi
	xorl	%r14d, %r14d
	call	_ZN2v88internal8GCTracer15BackgroundScope4NameENS2_7ScopeIdE@PLT
	pxor	%xmm0, %xmm0
	movq	%rax, %r13
	movaps	%xmm0, -4240(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2230
.L1808:
	movq	-4232(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1809
	movq	(%rdi), %rax
	call	*8(%rax)
.L1809:
	movq	-4240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1810
	movq	(%rdi), %rax
	call	*8(%rax)
.L1810:
	movl	$5, %edi
	call	_ZN2v88internal8GCTracer15BackgroundScope4NameENS2_7ScopeIdE@PLT
	movq	%r12, -4408(%rbp)
	movq	%rax, -4400(%rbp)
	leaq	-4408(%rbp), %rax
	movq	%r14, -4392(%rbp)
	movq	%rax, -4416(%rbp)
	jmp	.L1807
.L2226:
	movq	-4480(%rbp), %rcx
	movq	(%rbx), %rdi
	leaq	.LC11(%rip), %rsi
	movl	$1, %eax
	subsd	-4592(%rbp), %xmm0
	movl	-4500(%rbp), %edx
	shrq	$10, %rcx
	subq	$37592, %rdi
	call	_ZN2v88internal7Isolate18PrintWithTimestampEPKcz@PLT
	jmp	.L1973
.L2225:
	movb	$1, 1200(%rbx)
	mfence
	jmp	.L1972
.L2224:
	leaq	7600(%r13), %r14
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	7640(%r13), %rax
	movq	%r14, %rdi
	movq	%rax, (%r12)
	movq	%r12, 7640(%r13)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movl	$64, %ecx
	movq	$0, 8(%rax)
	movq	%rax, %rdx
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rdx, 6968(%r15)
	jmp	.L1971
.L2223:
	leaq	7600(%r13), %r14
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	7640(%r13), %rax
	movq	%r14, %rdi
	movq	%rax, (%r12)
	movq	%r12, 7640(%r13)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movl	$64, %ecx
	movq	$0, 8(%rax)
	movq	%rax, %rdx
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rdx, 6960(%r15)
	jmp	.L1970
.L2222:
	leaq	6904(%r13), %r15
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	6944(%r13), %rax
	movq	%r15, %rdi
	movq	%rax, (%r12)
	movq	%r12, 6944(%r13)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movl	$64, %ecx
	movq	$0, 8(%rax)
	movq	%rax, %rdx
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rdx, 6272(%r14)
	jmp	.L1969
.L2221:
	leaq	6904(%r13), %r15
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	6944(%r13), %rax
	movq	%r15, %rdi
	movq	%rax, (%r12)
	movq	%r12, 6944(%r13)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movl	$64, %ecx
	movq	$0, 8(%rax)
	movq	%rax, %rdx
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rdx, 6264(%r14)
	jmp	.L1968
.L2220:
	leaq	4816(%r13), %r15
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	4856(%r13), %rax
	movq	%r15, %rdi
	movq	%rax, (%r12)
	movq	%r12, 4856(%r13)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$1040, %edi
	call	_Znwm@PLT
	movl	$128, %ecx
	movq	$0, 8(%rax)
	movq	%rax, %rdx
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rdx, 4184(%r14)
	jmp	.L1967
.L2219:
	leaq	4816(%r13), %r15
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	4856(%r13), %rax
	movq	%r15, %rdi
	movq	%rax, (%r12)
	movq	%r12, 4856(%r13)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$1040, %edi
	call	_Znwm@PLT
	movl	$128, %ecx
	movq	$0, 8(%rax)
	movq	%rax, %rdx
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rdx, 4176(%r14)
	jmp	.L1966
.L2218:
	leaq	6208(%r13), %r15
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	6248(%r13), %rax
	movq	%r15, %rdi
	movq	%rax, (%r12)
	movq	%r12, 6248(%r13)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movl	$64, %ecx
	movq	$0, 8(%rax)
	movq	%rax, %rdx
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rdx, 5576(%r14)
	jmp	.L1965
.L2217:
	leaq	6208(%r13), %r15
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	6248(%r13), %rax
	movq	%r15, %rdi
	movq	%rax, (%r12)
	movq	%r12, 6248(%r13)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movl	$64, %ecx
	movq	$0, 8(%rax)
	movq	%rax, %rdx
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rdx, 5568(%r14)
	jmp	.L1964
.L2216:
	leaq	5512(%r13), %r15
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	5552(%r13), %rax
	movq	%r15, %rdi
	movq	%rax, (%r12)
	movq	%r12, 5552(%r13)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movl	$64, %ecx
	movq	$0, 8(%rax)
	movq	%rax, %rdx
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rdx, 4880(%r14)
	jmp	.L1963
.L2215:
	leaq	5512(%r13), %r15
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	5552(%r13), %rax
	movq	%r15, %rdi
	movq	%rax, (%r12)
	movq	%r12, 5552(%r13)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movl	$64, %ecx
	movq	$0, 8(%rax)
	movq	%rax, %rdx
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rdx, 4872(%r14)
	jmp	.L1962
.L2214:
	leaq	4120(%r12), %r15
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	4160(%r12), %rax
	movq	%r15, %rdi
	movq	%rax, 0(%r13)
	movq	%r13, 4160(%r12)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$1040, %edi
	call	_Znwm@PLT
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZN2v88internal8WorklistISt4pairINS0_10HeapObjectENS0_18FullHeapObjectSlotEELi64EE7SegmentC1Ev
	movq	%r12, 3488(%r14)
	jmp	.L1961
.L2213:
	leaq	4120(%r12), %r15
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	4160(%r12), %rax
	movq	%r15, %rdi
	movq	%rax, 0(%r13)
	movq	%r13, 4160(%r12)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$1040, %edi
	call	_Znwm@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v88internal8WorklistISt4pairINS0_10HeapObjectENS0_18FullHeapObjectSlotEELi64EE7SegmentC1Ev
	movq	%r13, 3480(%r14)
	jmp	.L1960
.L2212:
	leaq	1336(%r13), %r15
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	1376(%r13), %rax
	movq	%r15, %rdi
	movq	%rax, (%r12)
	movq	%r12, 1376(%r13)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movl	$64, %ecx
	movq	$0, 8(%rax)
	movq	%rax, %rdx
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rdx, 704(%r14)
	jmp	.L1959
.L2211:
	leaq	1336(%r13), %r15
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	1376(%r13), %rax
	movq	%r15, %rdi
	movq	%rax, (%r12)
	movq	%r12, 1376(%r13)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movl	$64, %ecx
	movq	$0, 8(%rax)
	movq	%rax, %rdx
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rdx, 696(%r14)
	jmp	.L1958
.L2210:
	leaq	640(%r13), %r15
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	680(%r13), %rax
	movq	%r15, %rdi
	movq	%rax, (%r12)
	movq	%r12, 680(%r13)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movl	$64, %ecx
	movq	$0, 8(%rax)
	movq	%rax, %rdx
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rdx, 8(%r14)
	jmp	.L1957
.L2209:
	leaq	640(%r13), %r15
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	680(%r13), %rax
	movq	%r15, %rdi
	movq	%rax, (%r12)
	movq	%r12, 680(%r13)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movl	$64, %ecx
	movq	$0, 8(%rax)
	movq	%rax, %rdx
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rdx, (%r14)
	jmp	.L1956
.L2208:
	leaq	640(%r13), %r15
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	680(%r13), %rax
	movq	%r15, %rdi
	movq	%rax, (%r12)
	movq	%r12, 680(%r13)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$144, %edi
	call	_Znwm@PLT
	movl	$16, %ecx
	movq	$0, 8(%rax)
	movq	%rax, %rdx
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rdx, 8(%r14)
	jmp	.L1955
.L2207:
	leaq	640(%r13), %r15
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	680(%r13), %rax
	movq	%r15, %rdi
	movq	%rax, (%r12)
	movq	%r12, 680(%r13)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$144, %edi
	call	_Znwm@PLT
	movl	$16, %ecx
	movq	$0, 8(%rax)
	movq	%rax, %rdx
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rdx, (%r14)
	jmp	.L1954
.L1830:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2205:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2231
.L1947:
	movq	%r13, _ZZN2v88internal17ConcurrentMarking3RunEiPNS1_9TaskStateEE28trace_event_unique_atomic811(%rip)
	jmp	.L1946
.L2200:
	leaq	640(%rsi), %rdi
	movq	%rdx, -4608(%rbp)
	movq	%rcx, -4600(%rbp)
	movq	%rax, -4568(%rbp)
	movq	%rsi, -4560(%rbp)
	movq	%rdi, -4488(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-4560(%rbp), %rsi
	movq	-4568(%rbp), %rax
	movq	680(%rsi), %r8
	movq	%r8, (%rax)
	movq	%rax, 680(%rsi)
	movq	-4488(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movq	-4600(%rbp), %rcx
	movq	-4608(%rbp), %rdx
	movq	$0, 8(%rax)
	movq	%rax, %rsi
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rsi, (%rdx)
	movq	8(%rsi), %rax
	cmpq	$64, %rax
	je	.L1932
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%rsi)
	movq	%r12, 16(%rsi,%rax,8)
	jmp	.L1932
.L2206:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	leaq	-4256(%rbp), %r12
	movaps	%xmm0, -4256(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2232
.L1949:
	leaq	-4248(%rbp), %rdi
	call	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED1Ev
	movq	%r12, %rdi
	call	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED1Ev
	leaq	.LC10(%rip), %rax
	movq	%r13, -4312(%rbp)
	movq	%rax, -4304(%rbp)
	leaq	-4312(%rbp), %rax
	movq	%r14, -4296(%rbp)
	movq	%rax, -4320(%rbp)
	jmp	.L1948
.L2230:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	leaq	-4240(%rbp), %rdx
	pushq	$0
	movl	$88, %esi
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1808
.L2229:
	leaq	.LC8(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L1806
.L2203:
	leaq	7600(%rdx), %rdi
	movq	%rsi, -4600(%rbp)
	movq	%r8, -4560(%rbp)
	movq	%rax, -4568(%rbp)
	movq	%rdx, -4488(%rbp)
	movq	%rdi, -4472(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-4488(%rbp), %rdx
	movq	-4568(%rbp), %rax
	movq	7640(%rdx), %rcx
	movq	%rcx, (%rax)
	movq	%rax, 7640(%rdx)
	movq	-4472(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movq	%r12, %rcx
	movq	-4600(%rbp), %rsi
	movq	-4560(%rbp), %r8
	movq	$0, 8(%rax)
	movq	%rax, %rdx
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rdx, 6960(%rsi)
	movq	8(%rdx), %rax
	cmpq	$64, %rax
	je	.L1829
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdx)
	movq	%r8, 16(%rdx,%rax,8)
	jmp	.L1829
.L2232:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r13, %rdx
	pushq	$0
	leaq	.LC10(%rip), %rcx
	movl	$88, %esi
	pushq	%r12
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1949
.L2231:
	leaq	.LC8(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L1947
.L2227:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22336:
	.size	_ZN2v88internal17ConcurrentMarking3RunEiPNS1_9TaskStateE, .-_ZN2v88internal17ConcurrentMarking3RunEiPNS1_9TaskStateE
	.section	.text._ZN2v88internal17ConcurrentMarking4Task11RunInternalEv,"axG",@progbits,_ZN2v88internal17ConcurrentMarking4Task11RunInternalEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal17ConcurrentMarking4Task11RunInternalEv
	.type	_ZN2v88internal17ConcurrentMarking4Task11RunInternalEv, @function
_ZN2v88internal17ConcurrentMarking4Task11RunInternalEv:
.LFB22305:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rdx
	movl	56(%rdi), %esi
	movq	40(%rdi), %rdi
	jmp	_ZN2v88internal17ConcurrentMarking3RunEiPNS1_9TaskStateE
	.cfi_endproc
.LFE22305:
	.size	_ZN2v88internal17ConcurrentMarking4Task11RunInternalEv, .-_ZN2v88internal17ConcurrentMarking4Task11RunInternalEv
	.section	.text._ZN2v88internal14CancelableTask3RunEv,"axG",@progbits,_ZN2v88internal14CancelableTask3RunEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14CancelableTask3RunEv
	.type	_ZN2v88internal14CancelableTask3RunEv, @function
_ZN2v88internal14CancelableTask3RunEv:
.LFB7997:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	movl	$2, %edx
	lock cmpxchgl	%edx, 16(%rdi)
	jne	.L2234
	movq	(%rdi), %rax
	leaq	_ZN2v88internal17ConcurrentMarking4Task11RunInternalEv(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2236
	movq	48(%rdi), %rdx
	movl	56(%rdi), %esi
	movq	40(%rdi), %rdi
	jmp	_ZN2v88internal17ConcurrentMarking3RunEiPNS1_9TaskStateE
	.p2align 4,,10
	.p2align 3
.L2236:
	jmp	*%rax
.L2234:
	ret
	.cfi_endproc
.LFE7997:
	.size	_ZN2v88internal14CancelableTask3RunEv, .-_ZN2v88internal14CancelableTask3RunEv
	.p2align 4
	.weak	_ZThn32_N2v88internal14CancelableTask3RunEv
	.type	_ZThn32_N2v88internal14CancelableTask3RunEv, @function
_ZThn32_N2v88internal14CancelableTask3RunEv:
.LFB29045:
	.cfi_startproc
	endbr64
	leaq	-32(%rdi), %r8
	xorl	%eax, %eax
	movl	$2, %edx
	lock cmpxchgl	%edx, -16(%rdi)
	jne	.L2237
	movq	-32(%rdi), %rax
	leaq	_ZN2v88internal17ConcurrentMarking4Task11RunInternalEv(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2240
	movq	%r8, %rdi
	jmp	*%rax
.L2237:
	ret
.L2240:
	movq	16(%rdi), %rdx
	movl	24(%rdi), %esi
	movq	8(%rdi), %rdi
	jmp	_ZN2v88internal17ConcurrentMarking3RunEiPNS1_9TaskStateE
	.cfi_endproc
.LFE29045:
	.size	_ZThn32_N2v88internal14CancelableTask3RunEv, .-_ZThn32_N2v88internal14CancelableTask3RunEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal24ConcurrentMarkingVisitor4CastINS0_10ConsStringEEET_NS0_10HeapObjectE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal24ConcurrentMarkingVisitor4CastINS0_10ConsStringEEET_NS0_10HeapObjectE, @function
_GLOBAL__sub_I__ZN2v88internal24ConcurrentMarkingVisitor4CastINS0_10ConsStringEEET_NS0_10HeapObjectE:
.LFB28777:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE28777:
	.size	_GLOBAL__sub_I__ZN2v88internal24ConcurrentMarkingVisitor4CastINS0_10ConsStringEEET_NS0_10HeapObjectE, .-_GLOBAL__sub_I__ZN2v88internal24ConcurrentMarkingVisitor4CastINS0_10ConsStringEEET_NS0_10HeapObjectE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal24ConcurrentMarkingVisitor4CastINS0_10ConsStringEEET_NS0_10HeapObjectE
	.weak	_ZTVN2v88internal14CancelableTaskE
	.section	.data.rel.ro._ZTVN2v88internal14CancelableTaskE,"awG",@progbits,_ZTVN2v88internal14CancelableTaskE,comdat
	.align 8
	.type	_ZTVN2v88internal14CancelableTaskE, @object
	.size	_ZTVN2v88internal14CancelableTaskE, 88
_ZTVN2v88internal14CancelableTaskE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZN2v88internal14CancelableTask3RunEv
	.quad	__cxa_pure_virtual
	.quad	-32
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZThn32_N2v88internal14CancelableTask3RunEv
	.weak	_ZTVN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitorE
	.section	.data.rel.ro._ZTVN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitorE,"awG",@progbits,_ZTVN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitorE,comdat
	.align 8
	.type	_ZTVN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitorE, @object
	.size	_ZTVN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitorE, 152
_ZTVN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitorD1Ev
	.quad	_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitorD0Ev
	.quad	_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES4_
	.quad	_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES4_
	.quad	_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES4_
	.quad	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE
	.quad	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE
	.quad	_ZN2v88internal13ObjectVisitor22VisitCustomWeakPointerENS0_10HeapObjectENS0_14FullObjectSlotE
	.quad	_ZN2v88internal13ObjectVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_
	.quad	_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE
	.quad	_ZN2v88internal24ConcurrentMarkingVisitor23SlotSnapshottingVisitor20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE
	.quad	_ZN2v88internal13ObjectVisitor17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE
	.quad	_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE
	.quad	_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_7ForeignEPm
	.quad	_ZN2v88internal13ObjectVisitor22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE
	.quad	_ZN2v88internal13ObjectVisitor18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE
	.quad	_ZN2v88internal13ObjectVisitor14VisitRelocInfoEPNS0_13RelocIteratorE
	.weak	_ZTVN2v88internal24ConcurrentMarkingVisitorE
	.section	.data.rel.ro._ZTVN2v88internal24ConcurrentMarkingVisitorE,"awG",@progbits,_ZTVN2v88internal24ConcurrentMarkingVisitorE,comdat
	.align 8
	.type	_ZTVN2v88internal24ConcurrentMarkingVisitorE, @object
	.size	_ZTVN2v88internal24ConcurrentMarkingVisitorE, 152
_ZTVN2v88internal24ConcurrentMarkingVisitorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal24ConcurrentMarkingVisitorD1Ev
	.quad	_ZN2v88internal24ConcurrentMarkingVisitorD0Ev
	.quad	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	.quad	_ZN2v88internal24ConcurrentMarkingVisitor13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES3_
	.quad	_ZN2v88internal24ConcurrentMarkingVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	.quad	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE
	.quad	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE
	.quad	_ZN2v88internal13ObjectVisitor22VisitCustomWeakPointerENS0_10HeapObjectENS0_14FullObjectSlotE
	.quad	_ZN2v88internal13ObjectVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_
	.quad	_ZN2v88internal24ConcurrentMarkingVisitor15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE
	.quad	_ZN2v88internal24ConcurrentMarkingVisitor20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE
	.quad	_ZN2v88internal13ObjectVisitor17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE
	.quad	_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE
	.quad	_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_7ForeignEPm
	.quad	_ZN2v88internal13ObjectVisitor22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE
	.quad	_ZN2v88internal13ObjectVisitor18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE
	.quad	_ZN2v88internal13ObjectVisitor14VisitRelocInfoEPNS0_13RelocIteratorE
	.weak	_ZTVN2v88internal17ConcurrentMarking4TaskE
	.section	.data.rel.ro.local._ZTVN2v88internal17ConcurrentMarking4TaskE,"awG",@progbits,_ZTVN2v88internal17ConcurrentMarking4TaskE,comdat
	.align 8
	.type	_ZTVN2v88internal17ConcurrentMarking4TaskE, @object
	.size	_ZTVN2v88internal17ConcurrentMarking4TaskE, 88
_ZTVN2v88internal17ConcurrentMarking4TaskE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal17ConcurrentMarking4TaskD1Ev
	.quad	_ZN2v88internal17ConcurrentMarking4TaskD0Ev
	.quad	_ZN2v88internal14CancelableTask3RunEv
	.quad	_ZN2v88internal17ConcurrentMarking4Task11RunInternalEv
	.quad	-32
	.quad	0
	.quad	_ZThn32_N2v88internal17ConcurrentMarking4TaskD1Ev
	.quad	_ZThn32_N2v88internal17ConcurrentMarking4TaskD0Ev
	.quad	_ZThn32_N2v88internal14CancelableTask3RunEv
	.section	.bss._ZGVZN2v88internal17ConcurrentMarking13ScheduleTasksEvE9num_cores,"aw",@nobits
	.align 8
	.type	_ZGVZN2v88internal17ConcurrentMarking13ScheduleTasksEvE9num_cores, @object
	.size	_ZGVZN2v88internal17ConcurrentMarking13ScheduleTasksEvE9num_cores, 8
_ZGVZN2v88internal17ConcurrentMarking13ScheduleTasksEvE9num_cores:
	.zero	8
	.section	.bss._ZZN2v88internal17ConcurrentMarking13ScheduleTasksEvE9num_cores,"aw",@nobits
	.align 4
	.type	_ZZN2v88internal17ConcurrentMarking13ScheduleTasksEvE9num_cores, @object
	.size	_ZZN2v88internal17ConcurrentMarking13ScheduleTasksEvE9num_cores, 4
_ZZN2v88internal17ConcurrentMarking13ScheduleTasksEvE9num_cores:
	.zero	4
	.section	.bss._ZZN2v88internal17ConcurrentMarking3RunEiPNS1_9TaskStateEE28trace_event_unique_atomic811,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal17ConcurrentMarking3RunEiPNS1_9TaskStateEE28trace_event_unique_atomic811, @object
	.size	_ZZN2v88internal17ConcurrentMarking3RunEiPNS1_9TaskStateEE28trace_event_unique_atomic811, 8
_ZZN2v88internal17ConcurrentMarking3RunEiPNS1_9TaskStateEE28trace_event_unique_atomic811:
	.zero	8
	.section	.bss._ZZN2v88internal17ConcurrentMarking3RunEiPNS1_9TaskStateEE28trace_event_unique_atomic753,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal17ConcurrentMarking3RunEiPNS1_9TaskStateEE28trace_event_unique_atomic753, @object
	.size	_ZZN2v88internal17ConcurrentMarking3RunEiPNS1_9TaskStateEE28trace_event_unique_atomic753, 8
_ZZN2v88internal17ConcurrentMarking3RunEiPNS1_9TaskStateEE28trace_event_unique_atomic753:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC2:
	.long	0
	.long	1083129856
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC3:
	.long	1065353216
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
