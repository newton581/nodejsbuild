	.file	"debug-objects.cc"
	.text
	.section	.text._ZNKSt5ctypeIcE8do_widenEc,"axG",@progbits,_ZNKSt5ctypeIcE8do_widenEc,comdat
	.align 2
	.p2align 4
	.weak	_ZNKSt5ctypeIcE8do_widenEc
	.type	_ZNKSt5ctypeIcE8do_widenEc, @function
_ZNKSt5ctypeIcE8do_widenEc:
.LFB3330:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	ret
	.cfi_endproc
.LFE3330:
	.size	_ZNKSt5ctypeIcE8do_widenEc, .-_ZNKSt5ctypeIcE8do_widenEc
	.section	.text._ZN2v88internal12StdoutStreamD1Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD1Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StdoutStreamD1Ev
	.type	_ZN2v88internal12StdoutStreamD1Ev, @function
_ZN2v88internal12StdoutStreamD1Ev:
.LFB22405:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE22405:
	.size	_ZN2v88internal12StdoutStreamD1Ev, .-_ZN2v88internal12StdoutStreamD1Ev
	.section	.text._ZN2v88internal12StdoutStreamD0Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD0Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.type	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev, @function
_ZTv0_n24_N2v88internal12StdoutStreamD0Ev:
.LFB22486:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %rax
	addq	-24(%rax), %rdi
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	movq	%rax, 80(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE22486:
	.size	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev, .-_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StdoutStreamD0Ev
	.type	_ZN2v88internal12StdoutStreamD0Ev, @function
_ZN2v88internal12StdoutStreamD0Ev:
.LFB22406:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE22406:
	.size	_ZN2v88internal12StdoutStreamD0Ev, .-_ZN2v88internal12StdoutStreamD0Ev
	.section	.text._ZN2v88internal12StdoutStreamD1Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD1Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.type	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev, @function
_ZTv0_n24_N2v88internal12StdoutStreamD1Ev:
.LFB22487:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	-24(%rax), %rbx
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	addq	%rdi, %rbx
	movq	%rax, 80(%rbx)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	64(%rbx), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE22487:
	.size	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev, .-_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.section	.text._ZNK2v88internal9DebugInfo7IsEmptyEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9DebugInfo7IsEmptyEv
	.type	_ZNK2v88internal9DebugInfo7IsEmptyEv, @function
_ZNK2v88internal9DebugInfo7IsEmptyEv:
.LFB18436:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	xorl	%eax, %eax
	movl	59(%rdx), %ecx
	testl	%ecx, %ecx
	jne	.L11
	movl	19(%rdx), %eax
	testl	%eax, %eax
	sete	%al
.L11:
	ret
	.cfi_endproc
.LFE18436:
	.size	_ZNK2v88internal9DebugInfo7IsEmptyEv, .-_ZNK2v88internal9DebugInfo7IsEmptyEv
	.section	.text._ZNK2v88internal9DebugInfo12HasBreakInfoEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9DebugInfo12HasBreakInfoEv
	.type	_ZNK2v88internal9DebugInfo12HasBreakInfoEv, @function
_ZNK2v88internal9DebugInfo12HasBreakInfoEv:
.LFB18437:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movslq	59(%rax), %rax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE18437:
	.size	_ZNK2v88internal9DebugInfo12HasBreakInfoEv, .-_ZNK2v88internal9DebugInfo12HasBreakInfoEv
	.section	.text._ZNK2v88internal9DebugInfo18DebugExecutionModeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9DebugInfo18DebugExecutionModeEv
	.type	_ZNK2v88internal9DebugInfo18DebugExecutionModeEv, @function
_ZNK2v88internal9DebugInfo18DebugExecutionModeEv:
.LFB18438:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movslq	59(%rax), %rax
	andl	$32, %eax
	ret
	.cfi_endproc
.LFE18438:
	.size	_ZNK2v88internal9DebugInfo18DebugExecutionModeEv, .-_ZNK2v88internal9DebugInfo18DebugExecutionModeEv
	.section	.text._ZN2v88internal9DebugInfo21SetDebugExecutionModeENS1_13ExecutionModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9DebugInfo21SetDebugExecutionModeENS1_13ExecutionModeE
	.type	_ZN2v88internal9DebugInfo21SetDebugExecutionModeENS1_13ExecutionModeE, @function
_ZN2v88internal9DebugInfo21SetDebugExecutionModeENS1_13ExecutionModeE:
.LFB18439:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movslq	59(%rdx), %rax
	movl	%eax, %ecx
	andl	$-33, %eax
	orl	$32, %ecx
	cmpl	$32, %esi
	cmove	%ecx, %eax
	salq	$32, %rax
	movq	%rax, 55(%rdx)
	ret
	.cfi_endproc
.LFE18439:
	.size	_ZN2v88internal9DebugInfo21SetDebugExecutionModeENS1_13ExecutionModeE, .-_ZN2v88internal9DebugInfo21SetDebugExecutionModeENS1_13ExecutionModeE
	.section	.text._ZN2v88internal9DebugInfo14ClearBreakInfoEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9DebugInfo14ClearBreakInfoEPNS0_7IsolateE
	.type	_ZN2v88internal9DebugInfo14ClearBreakInfoEPNS0_7IsolateE, @function
_ZN2v88internal9DebugInfo14ClearBreakInfoEPNS0_7IsolateE:
.LFB18440:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	39(%rdi), %rax
	testb	$1, %al
	jne	.L76
.L20:
	movq	288(%r12), %r12
	movq	%r12, 47(%rdi)
	movq	(%rbx), %rdi
	testb	$1, %r12b
	je	.L39
	movq	%r12, %r13
	leaq	47(%rdi), %rsi
	andq	$-262144, %r13
	movq	8(%r13), %rax
	testl	$262144, %eax
	jne	.L77
	testb	$24, %al
	je	.L39
.L80:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L78
	.p2align 4,,10
	.p2align 3
.L39:
	movslq	59(%rdi), %rax
	andl	$-60, %eax
	salq	$32, %rax
	movq	%rax, 55(%rdi)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L79
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	.cfi_restore_state
	movq	%r12, %rdx
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	(%rbx), %rdi
	movq	8(%r13), %rax
	leaq	47(%rdi), %rsi
	testb	$24, %al
	jne	.L80
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L76:
	movq	-1(%rax), %rax
	cmpw	$72, 11(%rax)
	jne	.L20
	movq	7(%rdi), %r14
	movq	31(%rdi), %r13
	movq	7(%r14), %rdx
	movq	%r13, %rax
	leaq	7(%r14), %r15
	notq	%rax
	andl	$1, %eax
	testb	$1, %dl
	jne	.L81
.L22:
	movq	7(%r14), %r14
	movq	%r13, 7(%r14)
	leaq	7(%r14), %rsi
	testb	%al, %al
	jne	.L26
	movq	%r13, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	je	.L28
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-88(%rbp), %rsi
.L28:
	testb	$24, %al
	je	.L26
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L82
	.p2align 4,,10
	.p2align 3
.L26:
	movq	(%rbx), %rax
	leaq	-80(%rbp), %r13
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	7(%rax), %rsi
	call	_ZN2v88internal23RedirectActiveFunctionsC1ENS0_18SharedFunctionInfoENS1_4ModeE@PLT
	movq	%r13, %rdi
	leaq	12448(%r12), %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal23RedirectActiveFunctions11VisitThreadEPNS0_7IsolateEPNS0_14ThreadLocalTopE@PLT
	movq	41168(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal13ThreadManager22IterateArchivedThreadsEPNS0_13ThreadVisitorE@PLT
	movq	88(%r12), %r13
	movq	(%rbx), %rax
	movq	%r13, 31(%rax)
	movq	(%rbx), %rdi
	testb	$1, %r13b
	je	.L41
	movq	%r13, %r14
	leaq	31(%rdi), %rsi
	andq	$-262144, %r14
	movq	8(%r14), %rax
	testl	$262144, %eax
	jne	.L83
	testb	$24, %al
	je	.L41
.L86:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L41
	movq	%r13, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	(%rbx), %rdi
	.p2align 4,,10
	.p2align 3
.L41:
	movq	88(%r12), %r13
	movq	%r13, 39(%rdi)
	movq	(%rbx), %rdi
	testb	$1, %r13b
	je	.L20
	movq	%r13, %r14
	leaq	39(%rdi), %rsi
	andq	$-262144, %r14
	movq	8(%r14), %rax
	testl	$262144, %eax
	jne	.L84
	testb	$24, %al
	je	.L20
.L85:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L20
	movq	%r13, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	(%rbx), %rdi
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L78:
	movq	%r12, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	(%rbx), %rdi
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L84:
	movq	%r13, %rdx
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	(%rbx), %rdi
	movq	8(%r14), %rax
	leaq	39(%rdi), %rsi
	testb	$24, %al
	jne	.L85
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L83:
	movq	%r13, %rdx
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	(%rbx), %rdi
	movq	8(%r14), %rax
	leaq	31(%rdi), %rsi
	testb	$24, %al
	jne	.L86
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L81:
	movq	-1(%rdx), %rdx
	cmpw	$72, 11(%rdx)
	jne	.L22
	movq	%r13, 7(%r14)
	testb	%al, %al
	jne	.L26
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -88(%rbp)
	testl	$262144, %eax
	je	.L24
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rcx
	movq	8(%rcx), %rax
.L24:
	testb	$24, %al
	je	.L26
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L26
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L82:
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L26
.L79:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18440:
	.size	_ZN2v88internal9DebugInfo14ClearBreakInfoEPNS0_7IsolateE, .-_ZN2v88internal9DebugInfo14ClearBreakInfoEPNS0_7IsolateE
	.section	.text._ZN2v88internal9DebugInfo15SetBreakAtEntryEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9DebugInfo15SetBreakAtEntryEv
	.type	_ZN2v88internal9DebugInfo15SetBreakAtEntryEv, @function
_ZN2v88internal9DebugInfo15SetBreakAtEntryEv:
.LFB18441:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movslq	59(%rdx), %rax
	orl	$8, %eax
	salq	$32, %rax
	movq	%rax, 55(%rdx)
	ret
	.cfi_endproc
.LFE18441:
	.size	_ZN2v88internal9DebugInfo15SetBreakAtEntryEv, .-_ZN2v88internal9DebugInfo15SetBreakAtEntryEv
	.section	.text._ZN2v88internal9DebugInfo17ClearBreakAtEntryEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9DebugInfo17ClearBreakAtEntryEv
	.type	_ZN2v88internal9DebugInfo17ClearBreakAtEntryEv, @function
_ZN2v88internal9DebugInfo17ClearBreakAtEntryEv:
.LFB18442:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movslq	59(%rdx), %rax
	andl	$-9, %eax
	salq	$32, %rax
	movq	%rax, 55(%rdx)
	ret
	.cfi_endproc
.LFE18442:
	.size	_ZN2v88internal9DebugInfo17ClearBreakAtEntryEv, .-_ZN2v88internal9DebugInfo17ClearBreakAtEntryEv
	.section	.text._ZNK2v88internal9DebugInfo12BreakAtEntryEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9DebugInfo12BreakAtEntryEv
	.type	_ZNK2v88internal9DebugInfo12BreakAtEntryEv, @function
_ZNK2v88internal9DebugInfo12BreakAtEntryEv:
.LFB18443:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movslq	59(%rax), %rax
	shrl	$3, %eax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE18443:
	.size	_ZNK2v88internal9DebugInfo12BreakAtEntryEv, .-_ZNK2v88internal9DebugInfo12BreakAtEntryEv
	.section	.text._ZNK2v88internal9DebugInfo15CanBreakAtEntryEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9DebugInfo15CanBreakAtEntryEv
	.type	_ZNK2v88internal9DebugInfo15CanBreakAtEntryEv, @function
_ZNK2v88internal9DebugInfo15CanBreakAtEntryEv:
.LFB18444:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movslq	59(%rax), %rax
	shrl	$4, %eax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE18444:
	.size	_ZNK2v88internal9DebugInfo15CanBreakAtEntryEv, .-_ZNK2v88internal9DebugInfo15CanBreakAtEntryEv
	.section	.text._ZN2v88internal9DebugInfo17GetBreakPointInfoEPNS0_7IsolateEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9DebugInfo17GetBreakPointInfoEPNS0_7IsolateEi
	.type	_ZN2v88internal9DebugInfo17GetBreakPointInfoEPNS0_7IsolateEi, @function
_ZN2v88internal9DebugInfo17GetBreakPointInfoEPNS0_7IsolateEi:
.LFB18446:
	.cfi_startproc
	endbr64
	movq	(%rdi), %r8
	xorl	%ecx, %ecx
	movq	47(%r8), %rax
	movl	11(%rax), %edi
	testl	%edi, %edi
	jle	.L96
	.p2align 4,,10
	.p2align 3
.L95:
	leaq	0(,%rcx,8), %rdi
	movq	15(%rax,%rdi), %rax
	cmpq	%rax, 88(%rsi)
	je	.L94
	movq	47(%r8), %rax
	movq	15(%rax,%rdi), %rax
	cmpl	%edx, 11(%rax)
	je	.L93
.L94:
	movq	47(%r8), %rax
	addq	$1, %rcx
	cmpl	%ecx, 11(%rax)
	jg	.L95
.L96:
	movq	88(%rsi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L93:
	ret
	.cfi_endproc
.LFE18446:
	.size	_ZN2v88internal9DebugInfo17GetBreakPointInfoEPNS0_7IsolateEi, .-_ZN2v88internal9DebugInfo17GetBreakPointInfoEPNS0_7IsolateEi
	.section	.text._ZN2v88internal9DebugInfo13HasBreakPointEPNS0_7IsolateEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9DebugInfo13HasBreakPointEPNS0_7IsolateEi
	.type	_ZN2v88internal9DebugInfo13HasBreakPointEPNS0_7IsolateEi, @function
_ZN2v88internal9DebugInfo13HasBreakPointEPNS0_7IsolateEi:
.LFB18445:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal9DebugInfo17GetBreakPointInfoEPNS0_7IsolateEi
	xorl	%r8d, %r8d
	movq	88(%rsi), %rdx
	cmpq	%rax, %rdx
	je	.L99
	movq	15(%rax), %rax
	cmpq	%rax, %rdx
	je	.L99
	testb	$1, %al
	jne	.L102
.L103:
	movl	$1, %r8d
.L99:
	movl	%r8d, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L102:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	subl	$123, %edx
	cmpw	$14, %dx
	ja	.L103
	movl	11(%rax), %eax
	testl	%eax, %eax
	setg	%r8b
	jmp	.L99
	.cfi_endproc
.LFE18445:
	.size	_ZN2v88internal9DebugInfo13HasBreakPointEPNS0_7IsolateEi, .-_ZN2v88internal9DebugInfo13HasBreakPointEPNS0_7IsolateEi
	.section	.text._ZN2v88internal9DebugInfo14GetBreakPointsEPNS0_7IsolateEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9DebugInfo14GetBreakPointsEPNS0_7IsolateEi
	.type	_ZN2v88internal9DebugInfo14GetBreakPointsEPNS0_7IsolateEi, @function
_ZN2v88internal9DebugInfo14GetBreakPointsEPNS0_7IsolateEi:
.LFB18449:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	subq	$24, %rsp
	call	_ZN2v88internal9DebugInfo17GetBreakPointInfoEPNS0_7IsolateEi
	cmpq	%rax, 88(%rsi)
	je	.L114
	movq	41112(%rbx), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L110
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L110:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L115
.L112:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L114:
	.cfi_restore_state
	addq	$24, %rsp
	leaq	88(%rsi), %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L115:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L112
	.cfi_endproc
.LFE18449:
	.size	_ZN2v88internal9DebugInfo14GetBreakPointsEPNS0_7IsolateEi, .-_ZN2v88internal9DebugInfo14GetBreakPointsEPNS0_7IsolateEi
	.section	.text._ZN2v88internal9DebugInfo18GetBreakPointCountEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9DebugInfo18GetBreakPointCountEPNS0_7IsolateE
	.type	_ZN2v88internal9DebugInfo18GetBreakPointCountEPNS0_7IsolateE, @function
_ZN2v88internal9DebugInfo18GetBreakPointCountEPNS0_7IsolateE:
.LFB18450:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	movq	47(%rdi), %rdx
	movl	11(%rdx), %eax
	testl	%eax, %eax
	jle	.L123
	xorl	%eax, %eax
	xorl	%r9d, %r9d
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L125:
	addl	$1, %r9d
.L118:
	movq	47(%rdi), %rdx
	addq	$1, %rax
	cmpl	%eax, 11(%rdx)
	jle	.L116
.L122:
	leaq	0(,%rax,8), %rcx
	movq	15(%rdx,%rcx), %rdx
	cmpq	%rdx, 88(%rsi)
	je	.L118
	movq	47(%rdi), %rdx
	movq	15(%rdx,%rcx), %rdx
	movq	15(%rdx), %rdx
	cmpq	88(%rsi), %rdx
	je	.L118
	testb	$1, %dl
	je	.L125
	movq	-1(%rdx), %rcx
	movzwl	11(%rcx), %ecx
	subl	$123, %ecx
	cmpw	$14, %cx
	ja	.L125
	addl	11(%rdx), %r9d
	movq	47(%rdi), %rdx
	addq	$1, %rax
	cmpl	%eax, 11(%rdx)
	jg	.L122
.L116:
	movl	%r9d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L123:
	xorl	%r9d, %r9d
	movl	%r9d, %eax
	ret
	.cfi_endproc
.LFE18450:
	.size	_ZN2v88internal9DebugInfo18GetBreakPointCountEPNS0_7IsolateE, .-_ZN2v88internal9DebugInfo18GetBreakPointCountEPNS0_7IsolateE
	.section	.text._ZNK2v88internal9DebugInfo15HasCoverageInfoEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9DebugInfo15HasCoverageInfoEv
	.type	_ZNK2v88internal9DebugInfo15HasCoverageInfoEv, @function
_ZNK2v88internal9DebugInfo15HasCoverageInfoEv:
.LFB18452:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movslq	59(%rax), %rax
	shrl	$2, %eax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE18452:
	.size	_ZNK2v88internal9DebugInfo15HasCoverageInfoEv, .-_ZNK2v88internal9DebugInfo15HasCoverageInfoEv
	.section	.text._ZN2v88internal9DebugInfo17ClearCoverageInfoEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9DebugInfo17ClearCoverageInfoEPNS0_7IsolateE
	.type	_ZN2v88internal9DebugInfo17ClearCoverageInfoEPNS0_7IsolateE, @function
_ZN2v88internal9DebugInfo17ClearCoverageInfoEPNS0_7IsolateE:
.LFB18453:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	testb	$4, 59(%rax)
	jne	.L146
	ret
	.p2align 4,,10
	.p2align 3
.L146:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	88(%rsi), %r12
	movq	%r12, 63(%rax)
	movq	(%rdi), %rdi
	testb	$1, %r12b
	je	.L132
	movq	%r12, %r13
	leaq	63(%rdi), %rsi
	andq	$-262144, %r13
	movq	8(%r13), %rax
	testl	$262144, %eax
	jne	.L147
	testb	$24, %al
	je	.L132
.L148:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L132
	movq	%r12, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	(%rbx), %rdi
.L132:
	movslq	59(%rdi), %rax
	andl	$-5, %eax
	salq	$32, %rax
	movq	%rax, 55(%rdi)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L147:
	.cfi_restore_state
	movq	%r12, %rdx
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	(%rbx), %rdi
	movq	8(%r13), %rax
	leaq	63(%rdi), %rsi
	testb	$24, %al
	jne	.L148
	jmp	.L132
	.cfi_endproc
.LFE18453:
	.size	_ZN2v88internal9DebugInfo17ClearCoverageInfoEPNS0_7IsolateE, .-_ZN2v88internal9DebugInfo17ClearCoverageInfoEPNS0_7IsolateE
	.section	.text._ZN2v88internal9DebugInfo18GetSideEffectStateEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9DebugInfo18GetSideEffectStateEPNS0_7IsolateE
	.type	_ZN2v88internal9DebugInfo18GetSideEffectStateEPNS0_7IsolateE, @function
_ZN2v88internal9DebugInfo18GetSideEffectStateEPNS0_7IsolateE:
.LFB18454:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movslq	19(%rdx), %rax
	andl	$3, %eax
	jne	.L155
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	41112(%rsi), %rdi
	movq	7(%rdx), %r13
	testq	%rdi, %rdi
	je	.L151
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L152:
	movq	%r12, %rdi
	call	_ZN2v88internal13DebugEvaluate26FunctionGetSideEffectStateEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEE@PLT
	movq	(%rbx), %rdx
	movl	%eax, %r8d
	movslq	19(%rdx), %rax
	andl	$-4, %eax
	orl	%r8d, %eax
	salq	$32, %rax
	movq	%rax, 15(%rdx)
	movq	(%rbx), %rax
	movslq	19(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	andl	$3, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L155:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.p2align 4,,10
	.p2align 3
.L151:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	41088(%rsi), %rsi
	cmpq	41096(%r12), %rsi
	je	.L158
.L153:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, (%rsi)
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L158:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L153
	.cfi_endproc
.LFE18454:
	.size	_ZN2v88internal9DebugInfo18GetSideEffectStateEPNS0_7IsolateE, .-_ZN2v88internal9DebugInfo18GetSideEffectStateEPNS0_7IsolateE
	.section	.text._ZN2v88internal14BreakPointInfo15ClearBreakPointEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10BreakPointEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14BreakPointInfo15ClearBreakPointEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10BreakPointEEE
	.type	_ZN2v88internal14BreakPointInfo15ClearBreakPointEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10BreakPointEEE, @function
_ZN2v88internal14BreakPointInfo15ClearBreakPointEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10BreakPointEEE:
.LFB18456:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rsi), %r15
	movq	15(%r15), %rsi
	cmpq	%rsi, 88(%rdi)
	je	.L159
	movq	%rdi, %r13
	movq	%rdx, %r12
	leaq	15(%r15), %rbx
	testb	$1, %sil
	jne	.L214
.L162:
	movq	(%r12), %rax
	movl	11(%rsi), %edi
	cmpl	%edi, 11(%rax)
	je	.L215
.L159:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L214:
	.cfi_restore_state
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	subl	$123, %eax
	cmpw	$14, %ax
	ja	.L162
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L216
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r15
.L170:
	movslq	11(%rsi), %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	subl	$1, %esi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	(%r15), %rdx
	movl	11(%rdx), %ecx
	testl	%ecx, %ecx
	jle	.L159
	xorl	%ebx, %ebx
	xorl	%r8d, %r8d
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L218:
	addl	$1, %r8d
.L173:
	addq	$1, %rbx
	cmpl	%ebx, 11(%rdx)
	jle	.L217
.L177:
	leaq	0(,%rbx,8), %rcx
	movq	(%r12), %rsi
	leaq	16(%rcx), %rdi
	movq	15(%rdx,%rcx), %rcx
	movl	11(%rcx), %ecx
	cmpl	%ecx, 11(%rsi)
	je	.L218
	movq	(%rax), %r9
	movq	-1(%rdi,%rdx), %r13
	movl	%ebx, %edx
	subl	%r8d, %edx
	leal	16(,%rdx,8), %edx
	movslq	%edx, %rdx
	leaq	-1(%r9,%rdx), %rsi
	movq	%r13, (%rsi)
	testb	$1, %r13b
	je	.L212
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rdx
	movq	%rcx, -56(%rbp)
	testl	$262144, %edx
	je	.L175
	movq	%r13, %rdx
	movq	%r9, %rdi
	movq	%rax, -88(%rbp)
	movl	%r8d, -76(%rbp)
	movq	%rsi, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-88(%rbp), %rax
	movl	-76(%rbp), %r8d
	movq	-72(%rbp), %rsi
	movq	8(%rcx), %rdx
	movq	-64(%rbp), %r9
.L175:
	andl	$24, %edx
	je	.L212
	movq	%r9, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	jne	.L212
	movq	%r13, %rdx
	movq	%r9, %rdi
	movq	%rax, -64(%rbp)
	movl	%r8d, -56(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	(%r15), %rdx
	movq	-64(%rbp), %rax
	movl	-56(%rbp), %r8d
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L212:
	movq	(%r15), %rdx
	addq	$1, %rbx
	cmpl	%ebx, 11(%rdx)
	jg	.L177
.L217:
	testl	%r8d, %r8d
	je	.L159
	movq	(%r14), %r13
	movq	(%rax), %r12
	movq	%r12, 15(%r13)
	leaq	15(%r13), %r14
	testb	$1, %r12b
	je	.L159
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L179
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
.L179:
	testb	$24, %al
	je	.L159
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L159
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
.L213:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L215:
	.cfi_restore_state
	movq	88(%r13), %r12
	movq	%r12, (%rbx)
	testb	$1, %r12b
	je	.L159
	movq	%r12, %r13
	andq	$-262144, %r13
	movq	8(%r13), %rax
	testl	$262144, %eax
	jne	.L219
.L168:
	testb	$24, %al
	je	.L159
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L159
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L216:
	movq	41088(%r13), %r15
	cmpq	41096(%r13), %r15
	je	.L220
.L171:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r15)
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L219:
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r13), %rax
	jmp	.L168
.L220:
	movq	%r13, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L171
	.cfi_endproc
.LFE18456:
	.size	_ZN2v88internal14BreakPointInfo15ClearBreakPointEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10BreakPointEEE, .-_ZN2v88internal14BreakPointInfo15ClearBreakPointEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10BreakPointEEE
	.section	.text._ZN2v88internal14BreakPointInfo13SetBreakPointEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10BreakPointEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14BreakPointInfo13SetBreakPointEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10BreakPointEEE
	.type	_ZN2v88internal14BreakPointInfo13SetBreakPointEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10BreakPointEEE, @function
_ZN2v88internal14BreakPointInfo13SetBreakPointEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10BreakPointEEE:
.LFB18457:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %r14
	movq	(%rdx), %r15
	movq	15(%r14), %rsi
	cmpq	%rsi, 88(%rdi)
	je	.L307
	cmpq	%r15, %rsi
	je	.L221
	movq	%rdi, %r13
	movq	%rdx, %rbx
	testb	$1, %sil
	jne	.L308
.L228:
	movl	$2, %esi
	movq	%r13, %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	(%rax), %r15
	movq	%rax, %r13
	movq	(%r12), %rax
	movq	15(%rax), %r14
	leaq	15(%r15), %rsi
	movq	%r14, 15(%r15)
	testb	$1, %r14b
	je	.L260
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	jne	.L309
.L232:
	testb	$24, %al
	je	.L260
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L310
	.p2align 4,,10
	.p2align 3
.L260:
	movq	0(%r13), %r15
	movq	(%rbx), %r14
	movq	%r14, 23(%r15)
	leaq	23(%r15), %rsi
	testb	$1, %r14b
	je	.L259
	movq	%r14, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L311
.L235:
	testb	$24, %al
	je	.L259
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L312
	.p2align 4,,10
	.p2align 3
.L259:
	movq	(%r12), %r14
	movq	0(%r13), %r12
	movq	%r12, 15(%r14)
	leaq	15(%r14), %r13
	testb	$1, %r12b
	je	.L221
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L313
.L238:
	testb	$24, %al
	je	.L221
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L314
.L221:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L308:
	.cfi_restore_state
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	subl	$123, %eax
	cmpw	$14, %ax
	ja	.L228
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L315
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r15
.L240:
	movslq	11(%rsi), %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	addl	$1, %esi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	(%r15), %rdx
	movslq	11(%rdx), %rcx
	movl	%ecx, %esi
	testq	%rcx, %rcx
	jle	.L242
	xorl	%r13d, %r13d
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L316:
	movq	(%rax), %rdi
	movq	-1(%rsi,%rdx), %r14
	leaq	-1(%rdi,%rsi), %rsi
	movq	%r14, (%rsi)
	testb	$1, %r14b
	je	.L255
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rdx
	movq	%rcx, -56(%rbp)
	testl	$262144, %edx
	je	.L245
	movq	%r14, %rdx
	movq	%rax, -80(%rbp)
	movq	%rsi, -72(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-80(%rbp), %rax
	movq	-72(%rbp), %rsi
	movq	-64(%rbp), %rdi
	movq	8(%rcx), %rdx
.L245:
	andl	$24, %edx
	je	.L255
	movq	%rdi, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	jne	.L255
	movq	%r14, %rdx
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L255:
	movq	(%r15), %rdx
	addq	$1, %r13
	movslq	11(%rdx), %rcx
	movl	%ecx, %esi
	cmpl	%r13d, %ecx
	jle	.L242
.L247:
	leaq	0(,%r13,8), %rcx
	movq	(%rbx), %rdi
	leaq	16(%rcx), %rsi
	movq	15(%rdx,%rcx), %rcx
	movl	11(%rcx), %ecx
	cmpl	%ecx, 11(%rdi)
	jne	.L316
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L309:
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	8(%rcx), %rax
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L311:
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-56(%rbp), %rsi
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L313:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L314:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
.L306:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L310:
	.cfi_restore_state
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L312:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L307:
	movq	%r15, 15(%r14)
	testb	$1, %r15b
	je	.L221
	movq	%r15, %rbx
	leaq	15(%r14), %r8
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L317
.L224:
	testb	$24, %al
	je	.L221
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L221
	movq	%r15, %rdx
	movq	%r8, %rsi
	movq	%r14, %rdi
	jmp	.L306
	.p2align 4,,10
	.p2align 3
.L242:
	movq	(%rax), %r14
	leal	16(,%rsi,8), %edx
	movq	(%rbx), %r13
	movslq	%edx, %rdx
	leaq	-1(%r14,%rdx), %r15
	movq	%r13, (%r15)
	testb	$1, %r13b
	je	.L257
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rdx
	testl	$262144, %edx
	je	.L249
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rdx
	movq	-56(%rbp), %rax
.L249:
	andl	$24, %edx
	je	.L257
	movq	%r14, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	jne	.L257
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L257:
	movq	(%r12), %r13
	movq	(%rax), %r12
	movq	%r12, 15(%r13)
	leaq	15(%r13), %r14
	testb	$1, %r12b
	je	.L221
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L252
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
.L252:
	testb	$24, %al
	je	.L221
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L221
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	jmp	.L306
	.p2align 4,,10
	.p2align 3
.L315:
	movq	41088(%r13), %r15
	cmpq	41096(%r13), %r15
	je	.L318
.L241:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r15)
	jmp	.L240
	.p2align 4,,10
	.p2align 3
.L317:
	movq	%r8, %rsi
	movq	%r15, %rdx
	movq	%r14, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-56(%rbp), %r8
	jmp	.L224
.L318:
	movq	%r13, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L241
	.cfi_endproc
.LFE18457:
	.size	_ZN2v88internal14BreakPointInfo13SetBreakPointEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10BreakPointEEE, .-_ZN2v88internal14BreakPointInfo13SetBreakPointEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10BreakPointEEE
	.section	.text._ZN2v88internal9DebugInfo13SetBreakPointEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_10BreakPointEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9DebugInfo13SetBreakPointEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_10BreakPointEEE
	.type	_ZN2v88internal9DebugInfo13SetBreakPointEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_10BreakPointEEE, @function
_ZN2v88internal9DebugInfo13SetBreakPointEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_10BreakPointEEE:
.LFB18448:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	leaq	-64(%rbp), %rdi
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	%r15, %rsi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal9DebugInfo17GetBreakPointInfoEPNS0_7IsolateEi
	movq	41112(%r15), %rdi
	movq	%rax, %r8
	testq	%rdi, %rdi
	je	.L320
	movq	%rax, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
	movq	88(%r15), %rax
	cmpq	%rax, (%rsi)
	jne	.L372
.L323:
	movq	(%r12), %rcx
	movq	47(%rcx), %rsi
	movl	11(%rsi), %eax
	leaq	7(%rsi), %rdx
	testl	%eax, %eax
	jle	.L325
	movl	$16, %ebx
	xorl	%eax, %eax
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L373:
	movq	47(%rcx), %rsi
	addl	$1, %eax
	addq	$8, %rbx
	leaq	7(%rsi), %rdx
	cmpl	%eax, 11(%rsi)
	jle	.L325
.L327:
	movq	-1(%rsi,%rbx), %rdx
	cmpq	%rdx, 88(%r15)
	jne	.L373
.L326:
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal7Factory17NewBreakPointInfoEi@PLT
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	movq	%rax, %r13
	call	_ZN2v88internal14BreakPointInfo13SetBreakPointEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10BreakPointEEE
	movq	(%r12), %rax
	movq	0(%r13), %r12
	movq	47(%rax), %r14
	leaq	-1(%r14,%rbx), %r13
	movq	%r12, 0(%r13)
	testb	$1, %r12b
	je	.L319
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L338
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
.L338:
	testb	$24, %al
	je	.L319
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L374
	.p2align 4,,10
	.p2align 3
.L319:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L375
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L320:
	.cfi_restore_state
	movq	41088(%r15), %rsi
	cmpq	41096(%r15), %rsi
	je	.L376
.L322:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r15)
	movq	%r8, (%rsi)
	movq	88(%r15), %rax
	cmpq	%rax, (%rsi)
	je	.L323
.L372:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal14BreakPointInfo13SetBreakPointEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10BreakPointEEE
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L325:
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L377
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r8
	movq	(%rax), %rax
	leaq	7(%rax), %rdx
.L328:
	movq	(%rdx), %rsi
	movq	%r15, %rdi
	xorl	%edx, %edx
	movq	%r8, -72(%rbp)
	sarq	$32, %rsi
	addl	$4, %esi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	(%r12), %rdi
	movq	(%rax), %rbx
	leaq	47(%rdi), %rsi
	movq	%rbx, 47(%rdi)
	testb	$1, %bl
	movq	-72(%rbp), %r8
	je	.L342
	movq	%rbx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rdx
	movq	%rcx, -72(%rbp)
	testl	$262144, %edx
	je	.L331
	movq	%rbx, %rdx
	movq	%r8, -104(%rbp)
	movq	%rax, -96(%rbp)
	movq	%rsi, -88(%rbp)
	movq	%rdi, -80(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rcx
	movq	-104(%rbp), %r8
	movq	-96(%rbp), %rax
	movq	-88(%rbp), %rsi
	movq	8(%rcx), %rdx
	movq	-80(%rbp), %rdi
.L331:
	andl	$24, %edx
	je	.L342
	movq	%rdi, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	je	.L378
	.p2align 4,,10
	.p2align 3
.L342:
	movq	(%r8), %rdx
	movl	$15, %ecx
	xorl	%r9d, %r9d
	movslq	11(%rdx), %rsi
	leal	16(,%rsi,8), %ebx
	movslq	%ebx, %rbx
	testq	%rsi, %rsi
	jle	.L326
	movq	%r12, -104(%rbp)
	movq	%rax, %rbx
	movq	%r15, -96(%rbp)
	movq	%rcx, %r15
	movl	%r13d, -108(%rbp)
	movl	%r9d, %r13d
	movq	%r14, -120(%rbp)
	movq	%r8, %r14
	.p2align 4,,10
	.p2align 3
.L333:
	movq	(%rbx), %rdi
	movq	(%rdx,%r15), %r12
	leaq	(%rdi,%r15), %rsi
	movq	%r12, (%rsi)
	testb	$1, %r12b
	je	.L340
	movq	%r12, %r10
	andq	$-262144, %r10
	movq	8(%r10), %rdx
	movq	%r10, -72(%rbp)
	testl	$262144, %edx
	je	.L335
	movq	%r12, %rdx
	movq	%rsi, -88(%rbp)
	movq	%rdi, -80(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %r10
	movq	-88(%rbp), %rsi
	movq	-80(%rbp), %rdi
	movq	8(%r10), %rdx
.L335:
	andl	$24, %edx
	je	.L340
	movq	%rdi, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	jne	.L340
	movq	%r12, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L340:
	movq	(%r14), %rdx
	addl	$1, %r13d
	addq	$8, %r15
	movslq	11(%rdx), %rsi
	cmpl	%esi, %r13d
	jl	.L333
	leal	16(,%rsi,8), %ebx
	movq	-96(%rbp), %r15
	movq	-104(%rbp), %r12
	movl	-108(%rbp), %r13d
	movq	-120(%rbp), %r14
	movslq	%ebx, %rbx
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L374:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L377:
	movq	41088(%r15), %r8
	cmpq	41096(%r15), %r8
	je	.L379
.L329:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r8)
	jmp	.L328
	.p2align 4,,10
	.p2align 3
.L376:
	movq	%r15, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L322
.L378:
	movq	%rbx, %rdx
	movq	%r8, -80(%rbp)
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %r8
	movq	-72(%rbp), %rax
	jmp	.L342
.L379:
	movq	%r15, %rdi
	movq	%rsi, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %rsi
	movq	-72(%rbp), %rdx
	movq	%rax, %r8
	jmp	.L329
.L375:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18448:
	.size	_ZN2v88internal9DebugInfo13SetBreakPointEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_10BreakPointEEE, .-_ZN2v88internal9DebugInfo13SetBreakPointEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_10BreakPointEEE
	.section	.text._ZN2v88internal14BreakPointInfo13HasBreakPointEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10BreakPointEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14BreakPointInfo13HasBreakPointEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10BreakPointEEE
	.type	_ZN2v88internal14BreakPointInfo13HasBreakPointEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10BreakPointEEE, @function
_ZN2v88internal14BreakPointInfo13HasBreakPointEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10BreakPointEEE:
.LFB18458:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	movq	15(%rax), %rax
	cmpq	%rax, 88(%rdi)
	je	.L386
	testb	$1, %al
	jne	.L390
.L383:
	movq	(%rdx), %rdx
	movl	11(%rdx), %ecx
	cmpl	%ecx, 11(%rax)
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L386:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L390:
	movq	-1(%rax), %rcx
	movzwl	11(%rcx), %ecx
	subl	$123, %ecx
	cmpw	$14, %cx
	ja	.L383
	movl	11(%rax), %ecx
	leaq	7(%rax), %rdi
	testl	%ecx, %ecx
	jle	.L386
	movq	(%rdx), %rsi
	addq	$15, %rax
	xorl	%ecx, %ecx
	jmp	.L387
	.p2align 4,,10
	.p2align 3
.L391:
	addl	$1, %ecx
	addq	$8, %rax
	cmpl	%ecx, 4(%rdi)
	jle	.L386
.L387:
	movq	(%rax), %rdx
	movl	11(%rdx), %edx
	cmpl	%edx, 11(%rsi)
	jne	.L391
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE18458:
	.size	_ZN2v88internal14BreakPointInfo13HasBreakPointEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10BreakPointEEE, .-_ZN2v88internal14BreakPointInfo13HasBreakPointEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10BreakPointEEE
	.section	.text._ZN2v88internal9DebugInfo15ClearBreakPointEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10BreakPointEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9DebugInfo15ClearBreakPointEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10BreakPointEEE
	.type	_ZN2v88internal9DebugInfo15ClearBreakPointEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10BreakPointEEE, @function
_ZN2v88internal9DebugInfo15ClearBreakPointEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10BreakPointEEE:
.LFB18447:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rcx
	movq	47(%rcx), %rax
	movl	11(%rax), %r8d
	testl	%r8d, %r8d
	jle	.L411
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	xorl	%ebx, %ebx
	subq	$16, %rsp
	jmp	.L396
	.p2align 4,,10
	.p2align 3
.L413:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r8
.L399:
	movq	%r14, %rdx
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal14BreakPointInfo13HasBreakPointEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10BreakPointEEE
	testb	%al, %al
	jne	.L406
	movq	0(%r13), %rcx
.L401:
	movq	47(%rcx), %rax
	addq	$1, %rbx
	cmpl	%ebx, 11(%rax)
	jle	.L412
.L396:
	leaq	0(,%rbx,8), %rdx
	movq	15(%rax,%rdx), %rax
	leaq	16(%rdx), %rsi
	cmpq	%rax, 88(%r12)
	je	.L401
	movq	47(%rcx), %rax
	movq	-1(%rsi,%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	jne	.L413
	movq	41088(%r12), %r8
	cmpq	41096(%r12), %r8
	je	.L414
.L400:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r8)
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L412:
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L414:
	.cfi_restore_state
	movq	%r12, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L400
	.p2align 4,,10
	.p2align 3
.L406:
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%r8, %rsi
	movb	%al, -40(%rbp)
	call	_ZN2v88internal14BreakPointInfo15ClearBreakPointEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10BreakPointEEE
	movzbl	-40(%rbp), %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
.L411:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE18447:
	.size	_ZN2v88internal9DebugInfo15ClearBreakPointEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10BreakPointEEE, .-_ZN2v88internal9DebugInfo15ClearBreakPointEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10BreakPointEEE
	.section	.text._ZN2v88internal9DebugInfo18FindBreakPointInfoEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10BreakPointEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9DebugInfo18FindBreakPointInfoEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10BreakPointEEE
	.type	_ZN2v88internal9DebugInfo18FindBreakPointInfoEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10BreakPointEEE, @function
_ZN2v88internal9DebugInfo18FindBreakPointInfoEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10BreakPointEEE:
.LFB18451:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	xorl	%ebx, %ebx
	subq	$16, %rsp
	movq	(%rsi), %rcx
	movq	47(%rcx), %rax
	movl	11(%rax), %edi
	testl	%edi, %edi
	jg	.L422
	jmp	.L423
	.p2align 4,,10
	.p2align 3
.L431:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r8
.L420:
	movq	%r14, %rdx
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal14BreakPointInfo13HasBreakPointEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10BreakPointEEE
	testb	%al, %al
	jne	.L428
	movq	0(%r13), %rcx
.L418:
	movq	47(%rcx), %rax
	addq	$1, %rbx
	cmpl	%ebx, 11(%rax)
	jle	.L423
.L422:
	leaq	0(,%rbx,8), %rdx
	movq	15(%rax,%rdx), %rax
	cmpq	%rax, 88(%r12)
	je	.L418
	movq	47(%rcx), %rax
	movq	15(%rax,%rdx), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	jne	.L431
	movq	41088(%r12), %r8
	cmpq	41096(%r12), %r8
	je	.L432
.L421:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r8)
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L423:
	addq	$16, %rsp
	leaq	88(%r12), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L432:
	.cfi_restore_state
	movq	%r12, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L428:
	addq	$16, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18451:
	.size	_ZN2v88internal9DebugInfo18FindBreakPointInfoEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10BreakPointEEE, .-_ZN2v88internal9DebugInfo18FindBreakPointInfoEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10BreakPointEEE
	.section	.text._ZN2v88internal14BreakPointInfo18GetBreakPointCountEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14BreakPointInfo18GetBreakPointCountEPNS0_7IsolateE
	.type	_ZN2v88internal14BreakPointInfo18GetBreakPointCountEPNS0_7IsolateE, @function
_ZN2v88internal14BreakPointInfo18GetBreakPointCountEPNS0_7IsolateE:
.LFB18459:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	xorl	%r8d, %r8d
	movq	15(%rax), %rax
	cmpq	%rax, 88(%rsi)
	je	.L433
	testb	$1, %al
	jne	.L435
.L436:
	movl	$1, %r8d
.L433:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L435:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	subl	$123, %edx
	cmpw	$14, %dx
	ja	.L436
	movl	11(%rax), %r8d
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE18459:
	.size	_ZN2v88internal14BreakPointInfo18GetBreakPointCountEPNS0_7IsolateE, .-_ZN2v88internal14BreakPointInfo18GetBreakPointCountEPNS0_7IsolateE
	.section	.text._ZNK2v88internal12CoverageInfo9SlotCountEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal12CoverageInfo9SlotCountEv
	.type	_ZNK2v88internal12CoverageInfo9SlotCountEv, @function
_ZNK2v88internal12CoverageInfo9SlotCountEv:
.LFB18460:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movslq	11(%rax), %rdx
	testl	%edx, %edx
	leal	3(%rdx), %eax
	cmovns	%edx, %eax
	sarl	$2, %eax
	ret
	.cfi_endproc
.LFE18460:
	.size	_ZNK2v88internal12CoverageInfo9SlotCountEv, .-_ZNK2v88internal12CoverageInfo9SlotCountEv
	.section	.text._ZNK2v88internal12CoverageInfo19StartSourcePositionEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal12CoverageInfo19StartSourcePositionEi
	.type	_ZNK2v88internal12CoverageInfo19StartSourcePositionEi, @function
_ZNK2v88internal12CoverageInfo19StartSourcePositionEi:
.LFB18461:
	.cfi_startproc
	endbr64
	leal	2(,%rsi,4), %eax
	movq	(%rdi), %rdx
	sall	$3, %eax
	cltq
	movq	-1(%rax,%rdx), %rax
	sarq	$32, %rax
	ret
	.cfi_endproc
.LFE18461:
	.size	_ZNK2v88internal12CoverageInfo19StartSourcePositionEi, .-_ZNK2v88internal12CoverageInfo19StartSourcePositionEi
	.section	.text._ZNK2v88internal12CoverageInfo17EndSourcePositionEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal12CoverageInfo17EndSourcePositionEi
	.type	_ZNK2v88internal12CoverageInfo17EndSourcePositionEi, @function
_ZNK2v88internal12CoverageInfo17EndSourcePositionEi:
.LFB18462:
	.cfi_startproc
	endbr64
	leal	3(,%rsi,4), %eax
	movq	(%rdi), %rdx
	sall	$3, %eax
	cltq
	movq	-1(%rax,%rdx), %rax
	sarq	$32, %rax
	ret
	.cfi_endproc
.LFE18462:
	.size	_ZNK2v88internal12CoverageInfo17EndSourcePositionEi, .-_ZNK2v88internal12CoverageInfo17EndSourcePositionEi
	.section	.text._ZNK2v88internal12CoverageInfo10BlockCountEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal12CoverageInfo10BlockCountEi
	.type	_ZNK2v88internal12CoverageInfo10BlockCountEi, @function
_ZNK2v88internal12CoverageInfo10BlockCountEi:
.LFB18463:
	.cfi_startproc
	endbr64
	addl	$1, %esi
	movq	(%rdi), %rax
	sall	$5, %esi
	movslq	%esi, %rsi
	movq	-1(%rsi,%rax), %rax
	sarq	$32, %rax
	ret
	.cfi_endproc
.LFE18463:
	.size	_ZNK2v88internal12CoverageInfo10BlockCountEi, .-_ZNK2v88internal12CoverageInfo10BlockCountEi
	.section	.text._ZN2v88internal12CoverageInfo14InitializeSlotEiii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12CoverageInfo14InitializeSlotEiii
	.type	_ZN2v88internal12CoverageInfo14InitializeSlotEiii, @function
_ZN2v88internal12CoverageInfo14InitializeSlotEiii:
.LFB18464:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	sall	$2, %esi
	salq	$32, %rdx
	leal	16(,%rsi,8), %eax
	salq	$32, %rcx
	movslq	%eax, %r8
	addl	$8, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	cltq
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	leal	32(,%rsi,8), %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movslq	%r12d, %r12
	movq	%rdx, -1(%r8,%rdi)
	movq	(%rbx), %rdx
	movq	%rcx, -1(%rax,%rdx)
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %r13
	movq	(%rbx), %rax
	movq	%r13, -1(%r12,%rax)
	testb	$1, %r13b
	je	.L442
	movq	%r13, %r14
	movq	(%rbx), %rdi
	andq	$-262144, %r14
	movq	8(%r14), %rax
	leaq	-1(%r12,%rdi), %rsi
	testl	$262144, %eax
	jne	.L454
	testb	$24, %al
	je	.L442
.L456:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L455
.L442:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L454:
	.cfi_restore_state
	movq	%r13, %rdx
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	(%rbx), %rdi
	movq	8(%r14), %rax
	leaq	-1(%r12,%rdi), %rsi
	testb	$24, %al
	jne	.L456
	jmp	.L442
	.p2align 4,,10
	.p2align 3
.L455:
	popq	%rbx
	movq	%r13, %rdx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.cfi_endproc
.LFE18464:
	.size	_ZN2v88internal12CoverageInfo14InitializeSlotEiii, .-_ZN2v88internal12CoverageInfo14InitializeSlotEiii
	.section	.text._ZN2v88internal12CoverageInfo19IncrementBlockCountEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12CoverageInfo19IncrementBlockCountEi
	.type	_ZN2v88internal12CoverageInfo19IncrementBlockCountEi, @function
_ZN2v88internal12CoverageInfo19IncrementBlockCountEi:
.LFB18465:
	.cfi_startproc
	endbr64
	addl	$1, %esi
	movq	(%rdi), %rdx
	sall	$5, %esi
	movslq	%esi, %rsi
	movq	-1(%rsi,%rdx), %rax
	sarq	$32, %rax
	addl	$1, %eax
	salq	$32, %rax
	movq	%rax, -1(%rsi,%rdx)
	ret
	.cfi_endproc
.LFE18465:
	.size	_ZN2v88internal12CoverageInfo19IncrementBlockCountEi, .-_ZN2v88internal12CoverageInfo19IncrementBlockCountEi
	.section	.text._ZN2v88internal12CoverageInfo15ResetBlockCountEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12CoverageInfo15ResetBlockCountEi
	.type	_ZN2v88internal12CoverageInfo15ResetBlockCountEi, @function
_ZN2v88internal12CoverageInfo15ResetBlockCountEi:
.LFB18466:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	leal	1(%rsi), %ebx
	movq	(%rdi), %rax
	sall	$5, %ebx
	movslq	%ebx, %rbx
	movq	%r13, -1(%rbx,%rax)
	testb	$1, %r13b
	je	.L458
	movq	%r13, %r14
	movq	%rdi, %r12
	movq	(%rdi), %rdi
	andq	$-262144, %r14
	movq	8(%r14), %rax
	leaq	-1(%rbx,%rdi), %rsi
	testl	$262144, %eax
	jne	.L470
	testb	$24, %al
	je	.L458
.L472:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L471
.L458:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L470:
	.cfi_restore_state
	movq	%r13, %rdx
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	(%r12), %rdi
	movq	8(%r14), %rax
	leaq	-1(%rbx,%rdi), %rsi
	testb	$24, %al
	jne	.L472
	jmp	.L458
	.p2align 4,,10
	.p2align 3
.L471:
	popq	%rbx
	movq	%r13, %rdx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.cfi_endproc
.LFE18466:
	.size	_ZN2v88internal12CoverageInfo15ResetBlockCountEi, .-_ZN2v88internal12CoverageInfo15ResetBlockCountEi
	.section	.rodata._ZN2v88internal12CoverageInfo5PrintESt10unique_ptrIA_cSt14default_deleteIS3_EE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"Coverage info ("
.LC1:
	.string	"{anonymous}"
.LC2:
	.string	"):"
.LC3:
	.string	"{"
.LC4:
	.string	","
.LC5:
	.string	"}"
	.section	.text._ZN2v88internal12CoverageInfo5PrintESt10unique_ptrIA_cSt14default_deleteIS3_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12CoverageInfo5PrintESt10unique_ptrIA_cSt14default_deleteIS3_EE
	.type	_ZN2v88internal12CoverageInfo5PrintESt10unique_ptrIA_cSt14default_deleteIS3_EE, @function
_ZN2v88internal12CoverageInfo5PrintESt10unique_ptrIA_cSt14default_deleteIS3_EE:
.LFB18469:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-400(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$376, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-320(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -408(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	stdout(%rip), %rdx
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movq	%rax, -320(%rbp)
	movq	%r13, %rdi
	xorl	%eax, %eax
	movw	%ax, -96(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movl	$15, %edx
	movq	%r13, %rdi
	movq	%rax, -400(%rbp)
	leaq	.LC0(%rip), %rsi
	addq	$40, %rax
	movq	%rax, -320(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %r14
	cmpb	$0, (%r14)
	je	.L474
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L475:
	movl	$2, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-400(%rbp), %rax
	movq	-24(%rax), %rax
	movq	-160(%rbp,%rax), %r14
	testq	%r14, %r14
	je	.L480
	cmpb	$0, 56(%r14)
	je	.L477
	movsbl	67(%r14), %esi
.L478:
	movq	%r13, %rdi
	xorl	%ebx, %ebx
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movq	(%r12), %rax
	cmpl	$3, 11(%rax)
	jg	.L479
	jmp	.L483
	.p2align 4,,10
	.p2align 3
.L495:
	movsbl	67(%r15), %esi
.L482:
	movq	%r14, %rdi
	addq	$1, %rbx
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movq	(%r12), %rax
	movslq	11(%rax), %rdx
	testl	%edx, %edx
	leal	3(%rdx), %eax
	cmovns	%edx, %eax
	sarl	$2, %eax
	cmpl	%ebx, %eax
	jle	.L483
.L479:
	movl	$1, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r13, %rdi
	movq	%rbx, %r15
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r12), %rax
	salq	$5, %r15
	movq	%r13, %rdi
	movq	15(%r15,%rax), %rsi
	sarq	$32, %rsi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC4(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r12), %rax
	movq	%r14, %rdi
	movq	23(%r15,%rax), %rsi
	sarq	$32, %rsi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC5(%rip), %rsi
	movq	%rax, %r14
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rax
	movq	-24(%rax), %rax
	movq	240(%r14,%rax), %r15
	testq	%r15, %r15
	je	.L480
	cmpb	$0, 56(%r15)
	jne	.L495
	movq	%r15, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r15), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rcx
	movq	48(%rax), %rax
	cmpq	%rcx, %rax
	je	.L482
	movq	%r15, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L482
	.p2align 4,,10
	.p2align 3
.L483:
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	movq	%rax, -320(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-336(%rbp), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -400(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	-408(%rbp), %rdi
	movq	%rax, -400(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L496
	addq	$376, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L474:
	.cfi_restore_state
	movl	$11, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L475
	.p2align 4,,10
	.p2align 3
.L477:
	movq	%r14, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r14), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L478
	movq	%r14, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L478
.L480:
	call	_ZSt16__throw_bad_castv@PLT
.L496:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18469:
	.size	_ZN2v88internal12CoverageInfo5PrintESt10unique_ptrIA_cSt14default_deleteIS3_EE, .-_ZN2v88internal12CoverageInfo5PrintESt10unique_ptrIA_cSt14default_deleteIS3_EE
	.section	.text.startup._GLOBAL__sub_I__ZNK2v88internal9DebugInfo7IsEmptyEv,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZNK2v88internal9DebugInfo7IsEmptyEv, @function
_GLOBAL__sub_I__ZNK2v88internal9DebugInfo7IsEmptyEv:
.LFB22448:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE22448:
	.size	_GLOBAL__sub_I__ZNK2v88internal9DebugInfo7IsEmptyEv, .-_GLOBAL__sub_I__ZNK2v88internal9DebugInfo7IsEmptyEv
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZNK2v88internal9DebugInfo7IsEmptyEv
	.hidden	_ZTCN2v88internal12StdoutStreamE0_So
	.weak	_ZTCN2v88internal12StdoutStreamE0_So
	.section	.rodata._ZTCN2v88internal12StdoutStreamE0_So,"aG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal12StdoutStreamE0_So, @object
	.size	_ZTCN2v88internal12StdoutStreamE0_So, 80
_ZTCN2v88internal12StdoutStreamE0_So:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.hidden	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE
	.weak	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE
	.section	.rodata._ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE,"aG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE, @object
	.size	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE, 80
_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.weak	_ZTTN2v88internal12StdoutStreamE
	.section	.data.rel.ro.local._ZTTN2v88internal12StdoutStreamE,"awG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTTN2v88internal12StdoutStreamE, @object
	.size	_ZTTN2v88internal12StdoutStreamE, 48
_ZTTN2v88internal12StdoutStreamE:
	.quad	_ZTVN2v88internal12StdoutStreamE+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_So+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_So+64
	.quad	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE+64
	.quad	_ZTVN2v88internal12StdoutStreamE+64
	.weak	_ZTVN2v88internal12StdoutStreamE
	.section	.data.rel.ro.local._ZTVN2v88internal12StdoutStreamE,"awG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTVN2v88internal12StdoutStreamE, @object
	.size	_ZTVN2v88internal12StdoutStreamE, 80
_ZTVN2v88internal12StdoutStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	_ZN2v88internal12StdoutStreamD1Ev
	.quad	_ZN2v88internal12StdoutStreamD0Ev
	.quad	-80
	.quad	-80
	.quad	0
	.quad	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.quad	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
