	.file	"v8-debugger-script.cc"
	.text
	.section	.text._ZN12v8_inspector17V8InspectorClient17resourceNameToUrlERKNS_10StringViewE,"axG",@progbits,_ZN12v8_inspector17V8InspectorClient17resourceNameToUrlERKNS_10StringViewE,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector17V8InspectorClient17resourceNameToUrlERKNS_10StringViewE
	.type	_ZN12v8_inspector17V8InspectorClient17resourceNameToUrlERKNS_10StringViewE, @function
_ZN12v8_inspector17V8InspectorClient17resourceNameToUrlERKNS_10StringViewE:
.LFB3651:
	.cfi_startproc
	endbr64
	movq	$0, (%rdi)
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE3651:
	.size	_ZN12v8_inspector17V8InspectorClient17resourceNameToUrlERKNS_10StringViewE, .-_ZN12v8_inspector17V8InspectorClient17resourceNameToUrlERKNS_10StringViewE
	.section	.text._ZNK12v8_inspector12_GLOBAL__N_112ActualScript10isLiveEditEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK12v8_inspector12_GLOBAL__N_112ActualScript10isLiveEditEv, @function
_ZNK12v8_inspector12_GLOBAL__N_112ActualScript10isLiveEditEv:
.LFB7443:
	.cfi_startproc
	endbr64
	movzbl	152(%rdi), %eax
	ret
	.cfi_endproc
.LFE7443:
	.size	_ZNK12v8_inspector12_GLOBAL__N_112ActualScript10isLiveEditEv, .-_ZNK12v8_inspector12_GLOBAL__N_112ActualScript10isLiveEditEv
	.section	.text._ZNK12v8_inspector12_GLOBAL__N_112ActualScript8isModuleEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK12v8_inspector12_GLOBAL__N_112ActualScript8isModuleEv, @function
_ZNK12v8_inspector12_GLOBAL__N_112ActualScript8isModuleEv:
.LFB7444:
	.cfi_startproc
	endbr64
	movzbl	153(%rdi), %eax
	ret
	.cfi_endproc
.LFE7444:
	.size	_ZNK12v8_inspector12_GLOBAL__N_112ActualScript8isModuleEv, .-_ZNK12v8_inspector12_GLOBAL__N_112ActualScript8isModuleEv
	.section	.text._ZNK12v8_inspector12_GLOBAL__N_112ActualScript9startLineEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK12v8_inspector12_GLOBAL__N_112ActualScript9startLineEv, @function
_ZNK12v8_inspector12_GLOBAL__N_112ActualScript9startLineEv:
.LFB7446:
	.cfi_startproc
	endbr64
	movl	200(%rdi), %eax
	ret
	.cfi_endproc
.LFE7446:
	.size	_ZNK12v8_inspector12_GLOBAL__N_112ActualScript9startLineEv, .-_ZNK12v8_inspector12_GLOBAL__N_112ActualScript9startLineEv
	.section	.text._ZNK12v8_inspector12_GLOBAL__N_112ActualScript11startColumnEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK12v8_inspector12_GLOBAL__N_112ActualScript11startColumnEv, @function
_ZNK12v8_inspector12_GLOBAL__N_112ActualScript11startColumnEv:
.LFB7447:
	.cfi_startproc
	endbr64
	movl	204(%rdi), %eax
	ret
	.cfi_endproc
.LFE7447:
	.size	_ZNK12v8_inspector12_GLOBAL__N_112ActualScript11startColumnEv, .-_ZNK12v8_inspector12_GLOBAL__N_112ActualScript11startColumnEv
	.section	.text._ZNK12v8_inspector12_GLOBAL__N_112ActualScript7endLineEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK12v8_inspector12_GLOBAL__N_112ActualScript7endLineEv, @function
_ZNK12v8_inspector12_GLOBAL__N_112ActualScript7endLineEv:
.LFB7448:
	.cfi_startproc
	endbr64
	movl	208(%rdi), %eax
	ret
	.cfi_endproc
.LFE7448:
	.size	_ZNK12v8_inspector12_GLOBAL__N_112ActualScript7endLineEv, .-_ZNK12v8_inspector12_GLOBAL__N_112ActualScript7endLineEv
	.section	.text._ZNK12v8_inspector12_GLOBAL__N_112ActualScript9endColumnEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK12v8_inspector12_GLOBAL__N_112ActualScript9endColumnEv, @function
_ZNK12v8_inspector12_GLOBAL__N_112ActualScript9endColumnEv:
.LFB7449:
	.cfi_startproc
	endbr64
	movl	212(%rdi), %eax
	ret
	.cfi_endproc
.LFE7449:
	.size	_ZNK12v8_inspector12_GLOBAL__N_112ActualScript9endColumnEv, .-_ZNK12v8_inspector12_GLOBAL__N_112ActualScript9endColumnEv
	.section	.text._ZNK12v8_inspector12_GLOBAL__N_112ActualScript20isSourceLoadedLazilyEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK12v8_inspector12_GLOBAL__N_112ActualScript20isSourceLoadedLazilyEv, @function
_ZNK12v8_inspector12_GLOBAL__N_112ActualScript20isSourceLoadedLazilyEv:
.LFB7450:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE7450:
	.size	_ZNK12v8_inspector12_GLOBAL__N_112ActualScript20isSourceLoadedLazilyEv, .-_ZNK12v8_inspector12_GLOBAL__N_112ActualScript20isSourceLoadedLazilyEv
	.set	_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript8isModuleEv,_ZNK12v8_inspector12_GLOBAL__N_112ActualScript20isSourceLoadedLazilyEv
	.set	_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript10isLiveEditEv,_ZNK12v8_inspector12_GLOBAL__N_112ActualScript20isSourceLoadedLazilyEv
	.section	.text._ZNK12v8_inspector12_GLOBAL__N_112ActualScript16sourceMappingURLEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK12v8_inspector12_GLOBAL__N_112ActualScript16sourceMappingURLEv, @function
_ZNK12v8_inspector12_GLOBAL__N_112ActualScript16sourceMappingURLEv:
.LFB7452:
	.cfi_startproc
	endbr64
	leaq	112(%rdi), %rax
	ret
	.cfi_endproc
.LFE7452:
	.size	_ZNK12v8_inspector12_GLOBAL__N_112ActualScript16sourceMappingURLEv, .-_ZNK12v8_inspector12_GLOBAL__N_112ActualScript16sourceMappingURLEv
	.section	.text._ZN12v8_inspector12_GLOBAL__N_117WasmVirtualScript19setSourceMappingURLERKNS_8String16E,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_117WasmVirtualScript19setSourceMappingURLERKNS_8String16E, @function
_ZN12v8_inspector12_GLOBAL__N_117WasmVirtualScript19setSourceMappingURLERKNS_8String16E:
.LFB7501:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7501:
	.size	_ZN12v8_inspector12_GLOBAL__N_117WasmVirtualScript19setSourceMappingURLERKNS_8String16E, .-_ZN12v8_inspector12_GLOBAL__N_117WasmVirtualScript19setSourceMappingURLERKNS_8String16E
	.section	.text._ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript20isSourceLoadedLazilyEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript20isSourceLoadedLazilyEv, @function
_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript20isSourceLoadedLazilyEv:
.LFB7503:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7503:
	.size	_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript20isSourceLoadedLazilyEv, .-_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript20isSourceLoadedLazilyEv
	.section	.text._ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript9startLineEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript9startLineEv, @function
_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript9startLineEv:
.LFB7505:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE7505:
	.size	_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript9startLineEv, .-_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript9startLineEv
	.set	_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript11startColumnEv,_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript9startLineEv
	.section	.text._ZN12v8_inspector12_GLOBAL__N_117WasmVirtualScript25resetBlackboxedStateCacheEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_117WasmVirtualScript25resetBlackboxedStateCacheEv, @function
_ZN12v8_inspector12_GLOBAL__N_117WasmVirtualScript25resetBlackboxedStateCacheEv:
.LFB7511:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7511:
	.size	_ZN12v8_inspector12_GLOBAL__N_117WasmVirtualScript25resetBlackboxedStateCacheEv, .-_ZN12v8_inspector12_GLOBAL__N_117WasmVirtualScript25resetBlackboxedStateCacheEv
	.set	_ZN12v8_inspector12_GLOBAL__N_117WasmVirtualScript8MakeWeakEv,_ZN12v8_inspector12_GLOBAL__N_117WasmVirtualScript25resetBlackboxedStateCacheEv
	.section	.text._ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript6offsetEii,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript6offsetEii, @function
_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript6offsetEii:
.LFB7512:
	.cfi_startproc
	endbr64
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE7512:
	.size	_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript6offsetEii, .-_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript6offsetEii
	.section	.text._ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript18setBreakpointOnRunEPi,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript18setBreakpointOnRunEPi, @function
_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript18setBreakpointOnRunEPi:
.LFB7515:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE7515:
	.size	_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript18setBreakpointOnRunEPi, .-_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript18setBreakpointOnRunEPi
	.section	.text._ZNK12v8_inspector12_GLOBAL__N_112ActualScript6scriptEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK12v8_inspector12_GLOBAL__N_112ActualScript6scriptEv, @function
_ZNK12v8_inspector12_GLOBAL__N_112ActualScript6scriptEv:
.LFB7479:
	.cfi_startproc
	endbr64
	movq	216(%rdi), %rax
	testq	%rax, %rax
	je	.L23
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	96(%rdi), %rdi
	movq	(%rax), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE7479:
	.size	_ZNK12v8_inspector12_GLOBAL__N_112ActualScript6scriptEv, .-_ZNK12v8_inspector12_GLOBAL__N_112ActualScript6scriptEv
	.section	.text._ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript6scriptEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript6scriptEv, @function
_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript6scriptEv:
.LFB7519:
	.cfi_startproc
	endbr64
	movq	104(%rdi), %rax
	testq	%rax, %rax
	je	.L32
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	96(%rdi), %rdi
	movq	(%rax), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE7519:
	.size	_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript6scriptEv, .-_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript6scriptEv
	.section	.text._ZNK12v8_inspector12_GLOBAL__N_112ActualScript18setBreakpointOnRunEPi,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK12v8_inspector12_GLOBAL__N_112ActualScript18setBreakpointOnRunEPi, @function
_ZNK12v8_inspector12_GLOBAL__N_112ActualScript18setBreakpointOnRunEPi:
.LFB7476:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	96(%rdi), %rsi
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	216(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L36
	movq	96(%rbx), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
.L36:
	movq	%r12, %rsi
	call	_ZNK2v85debug6Script26SetBreakpointOnScriptEntryEPi@PLT
	movq	%r13, %rdi
	movl	%eax, %r12d
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L42
	addq	$40, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L42:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7476:
	.size	_ZNK12v8_inspector12_GLOBAL__N_112ActualScript18setBreakpointOnRunEPi, .-_ZNK12v8_inspector12_GLOBAL__N_112ActualScript18setBreakpointOnRunEPi
	.section	.text._ZZN12v8_inspector12_GLOBAL__N_112ActualScript8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS1_EEE_4_FUNES6_,"ax",@progbits
	.p2align 4
	.type	_ZZN12v8_inspector12_GLOBAL__N_112ActualScript8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS1_EEE_4_FUNES6_, @function
_ZZN12v8_inspector12_GLOBAL__N_112ActualScript8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS1_EEE_4_FUNES6_:
.LFB7492:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	8(%rdi), %r12
	movq	216(%r12), %rdi
	call	_ZN2v82V89ClearWeakEPm@PLT
	movq	104(%r12), %rdi
	addq	$8, %rsp
	movq	%r12, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN12v8_inspector19V8DebuggerAgentImpl15ScriptCollectedEPKNS_16V8DebuggerScriptE@PLT
	.cfi_endproc
.LFE7492:
	.size	_ZZN12v8_inspector12_GLOBAL__N_112ActualScript8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS1_EEE_4_FUNES6_, .-_ZZN12v8_inspector12_GLOBAL__N_112ActualScript8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS1_EEE_4_FUNES6_
	.section	.text._ZN12v8_inspector12_GLOBAL__N_112ActualScript8MakeWeakEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_112ActualScript8MakeWeakEv, @function
_ZN12v8_inspector12_GLOBAL__N_112ActualScript8MakeWeakEv:
.LFB7490:
	.cfi_startproc
	endbr64
	movq	%rdi, %rsi
	movq	216(%rdi), %rdi
	movl	$2, %ecx
	leaq	_ZZN12v8_inspector12_GLOBAL__N_112ActualScript8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS1_EEE_4_FUNES6_(%rip), %rdx
	jmp	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	.cfi_endproc
.LFE7490:
	.size	_ZN12v8_inspector12_GLOBAL__N_112ActualScript8MakeWeakEv, .-_ZN12v8_inspector12_GLOBAL__N_112ActualScript8MakeWeakEv
	.section	.text._ZNK12v8_inspector12_GLOBAL__N_112ActualScript13setBreakpointERKNS_8String16EPN2v85debug8LocationEPi,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK12v8_inspector12_GLOBAL__N_112ActualScript13setBreakpointERKNS_8String16EPN2v85debug8LocationEPi, @function
_ZNK12v8_inspector12_GLOBAL__N_112ActualScript13setBreakpointERKNS_8String16EPN2v85debug8LocationEPi:
.LFB7475:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r15
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	96(%rdi), %rsi
	movq	%r15, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	216(%rbx), %r8
	movq	96(%rbx), %rdi
	testq	%r8, %r8
	je	.L47
	movq	(%r8), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	96(%rbx), %rdi
	movq	%rax, %r8
.L47:
	movq	%r14, %rsi
	movq	%r8, -88(%rbp)
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	-88(%rbp), %r8
	movq	%r12, %rdx
	movq	%r13, %rcx
	movq	%rax, %rsi
	movq	%r8, %rdi
	call	_ZNK2v85debug6Script13SetBreakpointENS_5LocalINS_6StringEEEPNS0_8LocationEPi@PLT
	movq	%r15, %rdi
	movl	%eax, %r12d
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L53
	addq	$56, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L53:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7475:
	.size	_ZNK12v8_inspector12_GLOBAL__N_112ActualScript13setBreakpointERKNS_8String16EPN2v85debug8LocationEPi, .-_ZNK12v8_inspector12_GLOBAL__N_112ActualScript13setBreakpointERKNS_8String16EPN2v85debug8LocationEPi
	.section	.text._ZNK12v8_inspector12_GLOBAL__N_112ActualScript8locationEi,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK12v8_inspector12_GLOBAL__N_112ActualScript8locationEi, @function
_ZNK12v8_inspector12_GLOBAL__N_112ActualScript8locationEi:
.LFB7474:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	96(%rdi), %rsi
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	216(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L55
	movq	96(%rbx), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
.L55:
	movl	%r12d, %esi
	call	_ZNK2v85debug6Script17GetSourceLocationEi@PLT
	movq	%r13, %rdi
	movq	%rax, -76(%rbp)
	movl	%edx, -68(%rbp)
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-76(%rbp), %rax
	movl	-68(%rbp), %edx
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L61
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L61:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7474:
	.size	_ZNK12v8_inspector12_GLOBAL__N_112ActualScript8locationEi, .-_ZNK12v8_inspector12_GLOBAL__N_112ActualScript8locationEi
	.section	.text._ZNK12v8_inspector12_GLOBAL__N_112ActualScript6offsetEii,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK12v8_inspector12_GLOBAL__N_112ActualScript6offsetEii, @function
_ZNK12v8_inspector12_GLOBAL__N_112ActualScript6offsetEii:
.LFB7473:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	96(%rdi), %rsi
	movq	%r14, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	216(%rbx), %r12
	testq	%r12, %r12
	je	.L63
	movq	(%r12), %rsi
	movq	96(%rbx), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %r12
.L63:
	leaq	-92(%rbp), %rbx
	movl	%r15d, %edx
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	_ZN2v85debug8LocationC1Eii@PLT
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZNK2v85debug6Script15GetSourceOffsetERKNS0_8LocationE@PLT
	movq	%r14, %rdi
	movl	%eax, %r12d
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L69
	addq	$56, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L69:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7473:
	.size	_ZNK12v8_inspector12_GLOBAL__N_112ActualScript6offsetEii, .-_ZNK12v8_inspector12_GLOBAL__N_112ActualScript6offsetEii
	.section	.text._ZN12v8_inspector12_GLOBAL__N_112ActualScript25resetBlackboxedStateCacheEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_112ActualScript25resetBlackboxedStateCacheEv, @function
_ZN12v8_inspector12_GLOBAL__N_112ActualScript25resetBlackboxedStateCacheEv:
.LFB7472:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	leaq	-48(%rbp), %r12
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	96(%rdi), %rsi
	movq	%r12, %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	216(%rbx), %rsi
	movq	96(%rbx), %rdi
	testq	%rsi, %rsi
	je	.L71
	movq	(%rsi), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	96(%rbx), %rdi
	movq	%rax, %rsi
.L71:
	call	_ZN2v85debug25ResetBlackboxedStateCacheEPNS_7IsolateENS_5LocalINS0_6ScriptEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L77
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L77:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7472:
	.size	_ZN12v8_inspector12_GLOBAL__N_112ActualScript25resetBlackboxedStateCacheEv, .-_ZN12v8_inspector12_GLOBAL__N_112ActualScript25resetBlackboxedStateCacheEv
	.section	.text._ZNK12v8_inspector12_GLOBAL__N_112ActualScript6lengthEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK12v8_inspector12_GLOBAL__N_112ActualScript6lengthEv, @function
_ZNK12v8_inspector12_GLOBAL__N_112ActualScript6lengthEv:
.LFB7451:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	96(%rdi), %rsi
	movq	%r12, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	216(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L79
	movq	96(%rbx), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
.L79:
	call	_ZNK2v85debug6Script6SourceEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L88
	call	_ZNK2v86String6LengthEv@PLT
	movl	%eax, %r13d
.L81:
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L89
	addq	$40, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	.cfi_restore_state
	xorl	%r13d, %r13d
	jmp	.L81
.L89:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7451:
	.size	_ZNK12v8_inspector12_GLOBAL__N_112ActualScript6lengthEv, .-_ZNK12v8_inspector12_GLOBAL__N_112ActualScript6lengthEv
	.section	.text._ZNK12v8_inspector12_GLOBAL__N_112ActualScript6sourceEmm,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK12v8_inspector12_GLOBAL__N_112ActualScript6sourceEmm, @function
_ZNK12v8_inspector12_GLOBAL__N_112ActualScript6sourceEmm:
.LFB7445:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r15
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r15, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	96(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	216(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L91
	movq	96(%rbx), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
.L91:
	call	_ZNK2v85debug6Script6SourceEv@PLT
	testq	%rax, %rax
	je	.L92
	movq	%rax, %rdi
	movq	%rax, -88(%rbp)
	call	_ZNK2v86String6LengthEv@PLT
	movq	-88(%rbp), %r10
	cltq
	cmpq	%r13, %rax
	ja	.L107
.L92:
	leaq	16(%r12), %rax
	pxor	%xmm0, %xmm0
	movq	$0, 32(%r12)
	movq	%rax, (%r12)
	movq	$0, 8(%r12)
	movups	%xmm0, 16(%r12)
.L94:
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L108
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L107:
	.cfi_restore_state
	movq	%r10, %rdi
	movq	%r10, -96(%rbp)
	call	_ZNK2v86String6LengthEv@PLT
	cltq
	subq	%r13, %rax
	cmpq	%r14, %rax
	cmovbe	%rax, %r14
	movabsq	$4611686018427387900, %rax
	cmpq	%rax, %r14
	leaq	(%r14,%r14), %rdi
	movq	$-1, %rax
	cmova	%rax, %rdi
	call	_Znam@PLT
	movq	-96(%rbp), %r10
	movq	96(%rbx), %rsi
	xorl	%r9d, %r9d
	movl	%r14d, %r8d
	movl	%r13d, %ecx
	movq	%rax, %rdx
	movq	%rax, -88(%rbp)
	movq	%r10, %rdi
	call	_ZNK2v86String5WriteEPNS_7IsolateEPtiii@PLT
	movq	-88(%rbp), %r11
	movq	%r12, %rdi
	movq	%r14, %rdx
	movq	%r11, %rsi
	call	_ZN12v8_inspector8String16C1EPKtm@PLT
	movq	-88(%rbp), %r11
	movq	%r11, %rdi
	call	_ZdaPv@PLT
	jmp	.L94
.L108:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7445:
	.size	_ZNK12v8_inspector12_GLOBAL__N_112ActualScript6sourceEmm, .-_ZNK12v8_inspector12_GLOBAL__N_112ActualScript6sourceEmm
	.section	.rodata._ZN12v8_inspector12_GLOBAL__N_117WasmVirtualScript9setSourceERKNS_8String16EbPN2v85debug14LiveEditResultE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN12v8_inspector12_GLOBAL__N_117WasmVirtualScript9setSourceERKNS_8String16EbPN2v85debug14LiveEditResultE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_117WasmVirtualScript9setSourceERKNS_8String16EbPN2v85debug14LiveEditResultE, @function
_ZN12v8_inspector12_GLOBAL__N_117WasmVirtualScript9setSourceERKNS_8String16EbPN2v85debug14LiveEditResultE:
.LFB7502:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE7502:
	.size	_ZN12v8_inspector12_GLOBAL__N_117WasmVirtualScript9setSourceERKNS_8String16EbPN2v85debug14LiveEditResultE, .-_ZN12v8_inspector12_GLOBAL__N_117WasmVirtualScript9setSourceERKNS_8String16EbPN2v85debug14LiveEditResultE
	.section	.text._ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript8locationEi,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript8locationEi, @function
_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript8locationEi:
.LFB7513:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-32(%rbp), %rdi
	call	_ZN2v85debug8LocationC1Ev@PLT
	movq	-32(%rbp), %rax
	movl	-24(%rbp), %edx
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L114
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L114:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7513:
	.size	_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript8locationEi, .-_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript8locationEi
	.section	.text._ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript9endColumnEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript9endColumnEv, @function
_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript9endColumnEv:
.LFB7508:
	.cfi_startproc
	endbr64
	movl	120(%rdi), %edx
	leaq	8(%rdi), %rsi
	movq	112(%rdi), %rdi
	jmp	_ZN12v8_inspector15WasmTranslation12GetEndColumnERKNS_8String16Ei@PLT
	.cfi_endproc
.LFE7508:
	.size	_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript9endColumnEv, .-_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript9endColumnEv
	.section	.text._ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript7endLineEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript7endLineEv, @function
_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript7endLineEv:
.LFB7507:
	.cfi_startproc
	endbr64
	movl	120(%rdi), %edx
	leaq	8(%rdi), %rsi
	movq	112(%rdi), %rdi
	jmp	_ZN12v8_inspector15WasmTranslation10GetEndLineERKNS_8String16Ei@PLT
	.cfi_endproc
.LFE7507:
	.size	_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript7endLineEv, .-_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript7endLineEv
	.section	.text._ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript16sourceMappingURLEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript16sourceMappingURLEv, @function
_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript16sourceMappingURLEv:
.LFB7498:
	.cfi_startproc
	endbr64
	movzbl	_ZGVZN12v8_inspector12_GLOBAL__N_117WasmVirtualScript11emptyStringEvE17singleEmptyString(%rip), %eax
	testb	%al, %al
	je	.L128
	movq	_ZZN12v8_inspector12_GLOBAL__N_117WasmVirtualScript11emptyStringEvE17singleEmptyString(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L128:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZGVZN12v8_inspector12_GLOBAL__N_117WasmVirtualScript11emptyStringEvE17singleEmptyString(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	jne	.L129
	movq	_ZZN12v8_inspector12_GLOBAL__N_117WasmVirtualScript11emptyStringEvE17singleEmptyString(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L129:
	.cfi_restore_state
	movl	$40, %edi
	call	_Znwm@PLT
	leaq	_ZGVZN12v8_inspector12_GLOBAL__N_117WasmVirtualScript11emptyStringEvE17singleEmptyString(%rip), %rdi
	leaq	16(%rax), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	xorl	%edx, %edx
	movw	%dx, 16(%rax)
	movq	$0, 32(%rax)
	movq	%rax, _ZZN12v8_inspector12_GLOBAL__N_117WasmVirtualScript11emptyStringEvE17singleEmptyString(%rip)
	call	__cxa_guard_release@PLT
	movq	_ZZN12v8_inspector12_GLOBAL__N_117WasmVirtualScript11emptyStringEvE17singleEmptyString(%rip), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7498:
	.size	_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript16sourceMappingURLEv, .-_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript16sourceMappingURLEv
	.section	.text._ZN12v8_inspector12_GLOBAL__N_112ActualScriptD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_112ActualScriptD0Ev, @function
_ZN12v8_inspector12_GLOBAL__N_112ActualScriptD0Ev:
.LFB9983:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector12_GLOBAL__N_112ActualScriptE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	216(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L131
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L131:
	movq	160(%r12), %rdi
	leaq	176(%r12), %rax
	cmpq	%rax, %rdi
	je	.L132
	call	_ZdlPv@PLT
.L132:
	movq	112(%r12), %rdi
	leaq	128(%r12), %rax
	cmpq	%rax, %rdi
	je	.L133
	call	_ZdlPv@PLT
.L133:
	leaq	16+_ZTVN12v8_inspector16V8DebuggerScriptE(%rip), %rax
	movq	48(%r12), %rdi
	movq	%rax, (%r12)
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L134
	call	_ZdlPv@PLT
.L134:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L135
	call	_ZdlPv@PLT
.L135:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$224, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9983:
	.size	_ZN12v8_inspector12_GLOBAL__N_112ActualScriptD0Ev, .-_ZN12v8_inspector12_GLOBAL__N_112ActualScriptD0Ev
	.section	.text._ZN12v8_inspector12_GLOBAL__N_117WasmVirtualScriptD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_117WasmVirtualScriptD0Ev, @function
_ZN12v8_inspector12_GLOBAL__N_117WasmVirtualScriptD0Ev:
.LFB10017:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector12_GLOBAL__N_117WasmVirtualScriptE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	128(%rdi), %rdi
	leaq	144(%r12), %rax
	cmpq	%rax, %rdi
	je	.L141
	call	_ZdlPv@PLT
.L141:
	movq	104(%r12), %rdi
	testq	%rdi, %rdi
	je	.L142
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L142:
	leaq	16+_ZTVN12v8_inspector16V8DebuggerScriptE(%rip), %rax
	movq	48(%r12), %rdi
	movq	%rax, (%r12)
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L143
	call	_ZdlPv@PLT
.L143:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L144
	call	_ZdlPv@PLT
.L144:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$168, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10017:
	.size	_ZN12v8_inspector12_GLOBAL__N_117WasmVirtualScriptD0Ev, .-_ZN12v8_inspector12_GLOBAL__N_117WasmVirtualScriptD0Ev
	.section	.text._ZN12v8_inspector12_GLOBAL__N_117WasmVirtualScriptD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_117WasmVirtualScriptD2Ev, @function
_ZN12v8_inspector12_GLOBAL__N_117WasmVirtualScriptD2Ev:
.LFB10015:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector12_GLOBAL__N_117WasmVirtualScriptE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	128(%rdi), %rdi
	leaq	144(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L150
	call	_ZdlPv@PLT
.L150:
	movq	104(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L151
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L151:
	leaq	16+_ZTVN12v8_inspector16V8DebuggerScriptE(%rip), %rax
	movq	48(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L152
	call	_ZdlPv@PLT
.L152:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L149
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L149:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10015:
	.size	_ZN12v8_inspector12_GLOBAL__N_117WasmVirtualScriptD2Ev, .-_ZN12v8_inspector12_GLOBAL__N_117WasmVirtualScriptD2Ev
	.set	_ZN12v8_inspector12_GLOBAL__N_117WasmVirtualScriptD1Ev,_ZN12v8_inspector12_GLOBAL__N_117WasmVirtualScriptD2Ev
	.section	.text._ZN12v8_inspector12_GLOBAL__N_112ActualScriptD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_112ActualScriptD2Ev, @function
_ZN12v8_inspector12_GLOBAL__N_112ActualScriptD2Ev:
.LFB9981:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector12_GLOBAL__N_112ActualScriptE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	216(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L159
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L159:
	movq	160(%rbx), %rdi
	leaq	176(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L160
	call	_ZdlPv@PLT
.L160:
	movq	112(%rbx), %rdi
	leaq	128(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L161
	call	_ZdlPv@PLT
.L161:
	leaq	16+_ZTVN12v8_inspector16V8DebuggerScriptE(%rip), %rax
	movq	48(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L162
	call	_ZdlPv@PLT
.L162:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L158
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L158:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9981:
	.size	_ZN12v8_inspector12_GLOBAL__N_112ActualScriptD2Ev, .-_ZN12v8_inspector12_GLOBAL__N_112ActualScriptD2Ev
	.set	_ZN12v8_inspector12_GLOBAL__N_112ActualScriptD1Ev,_ZN12v8_inspector12_GLOBAL__N_112ActualScriptD2Ev
	.section	.rodata._ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript6lengthEv.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"basic_string::_M_construct null not valid"
	.section	.text._ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript6lengthEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript6lengthEv, @function
_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript6lengthEv:
.LFB7509:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	8(%rdi), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-112(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	120(%rdi), %edx
	movq	112(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector15WasmTranslation9GetSourceERKNS_8String16Ei@PLT
	movl	$4294967295, %edx
	movq	%r13, -128(%rbp)
	cmpq	%rdx, 8(%rax)
	movq	(%rax), %r14
	movq	%rdx, %rbx
	cmovbe	8(%rax), %rbx
	movq	%r14, %rax
	leaq	(%rbx,%rbx), %r12
	addq	%r12, %rax
	je	.L169
	testq	%r14, %r14
	je	.L188
.L169:
	movq	%r12, %r15
	sarq	%r15
	cmpq	$14, %r12
	ja	.L189
	cmpq	$2, %r12
	jne	.L171
	movzwl	(%r14), %eax
	movw	%ax, -112(%rbp)
	movq	-128(%rbp), %rax
.L172:
	xorl	%edx, %edx
	movq	%r15, -120(%rbp)
	leaq	-96(%rbp), %rdi
	leaq	-128(%rbp), %rsi
	movw	%dx, (%rax,%rbx,2)
	call	_ZN12v8_inspector8String16C1EONSt7__cxx1112basic_stringItSt11char_traitsItESaItEEE@PLT
	movq	-128(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L173
	call	_ZdlPv@PLT
.L173:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	movq	-88(%rbp), %rbx
	cmpq	%rax, %rdi
	je	.L168
	call	_ZdlPv@PLT
.L168:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L190
	addq	$88, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L171:
	.cfi_restore_state
	movq	%r13, %rax
	testq	%r12, %r12
	je	.L172
	movq	%r13, %rdi
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L189:
	leaq	2(%r12), %rdi
	call	_Znwm@PLT
	movq	%r15, -112(%rbp)
	movq	%rax, -128(%rbp)
	movq	%rax, %rdi
.L170:
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	memmove@PLT
	movq	-128(%rbp), %rax
	jmp	.L172
.L188:
	leaq	.LC1(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L190:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7509:
	.size	_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript6lengthEv, .-_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript6lengthEv
	.section	.rodata._ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript6sourceEmm.str1.1,"aMS",@progbits,1
.LC2:
	.string	"basic_string::substr"
	.section	.rodata._ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript6sourceEmm.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"%s: __pos (which is %zu) > this->size() (which is %zu)"
	.section	.rodata._ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript6sourceEmm.str1.1
.LC4:
	.string	"basic_string::basic_string"
.LC5:
	.string	"basic_string::_M_create"
	.section	.text._ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript6sourceEmm,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript6sourceEmm, @function
_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript6sourceEmm:
.LFB7504:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	8(%rsi), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$72, %rsp
	movl	120(%rsi), %edx
	movq	112(%rsi), %rdi
	movq	%r8, %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector15WasmTranslation9GetSourceERKNS_8String16Ei@PLT
	movq	8(%rax), %r8
	cmpq	%r8, %r12
	ja	.L212
	subq	%r12, %r8
	leaq	-80(%rbp), %r15
	leaq	(%r12,%r12), %r14
	movq	%r15, -96(%rbp)
	addq	(%rax), %r14
	cmpq	%rbx, %r8
	cmovbe	%r8, %rbx
	movq	%r14, %rax
	leaq	(%rbx,%rbx), %r12
	addq	%r12, %rax
	je	.L194
	testq	%r14, %r14
	je	.L213
.L194:
	movq	%r12, %rcx
	movq	%r15, %rdi
	sarq	%rcx
	cmpq	$14, %r12
	ja	.L214
.L195:
	cmpq	$2, %r12
	je	.L215
	testq	%r12, %r12
	je	.L198
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%rcx, -104(%rbp)
	call	memmove@PLT
	movq	-96(%rbp), %rdi
	movq	-104(%rbp), %rcx
.L198:
	xorl	%eax, %eax
	movq	%rcx, -88(%rbp)
	leaq	-96(%rbp), %rsi
	movw	%ax, (%rdi,%rbx,2)
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EONSt7__cxx1112basic_stringItSt11char_traitsItESaItEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L191
	call	_ZdlPv@PLT
.L191:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L216
	addq	$72, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L215:
	.cfi_restore_state
	movzwl	(%r14), %eax
	movw	%ax, (%rdi)
	movq	-96(%rbp), %rdi
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L214:
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %rcx
	ja	.L217
	leaq	2(%r12), %rdi
	movq	%rcx, -104(%rbp)
	call	_Znwm@PLT
	movq	-104(%rbp), %rcx
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	%rcx, -80(%rbp)
	jmp	.L195
.L213:
	leaq	.LC1(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L212:
	movq	%r8, %rcx
	movq	%r12, %rdx
	leaq	.LC2(%rip), %rsi
	xorl	%eax, %eax
	leaq	.LC3(%rip), %rdi
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L216:
	call	__stack_chk_fail@PLT
.L217:
	leaq	.LC5(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE7504:
	.size	_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript6sourceEmm, .-_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript6sourceEmm
	.section	.text._ZNK12v8_inspector12_GLOBAL__N_112ActualScript4hashEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK12v8_inspector12_GLOBAL__N_112ActualScript4hashEv, @function
_ZNK12v8_inspector12_GLOBAL__N_112ActualScript4hashEv:
.LFB7477:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	160(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 168(%rdi)
	je	.L272
.L218:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L273
	addq	$248, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L272:
	.cfi_restore_state
	movq	96(%rdi), %rsi
	leaq	-256(%rbp), %r14
	movq	%rdi, %rbx
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	216(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L220
	movq	96(%rbx), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
.L220:
	call	_ZNK2v85debug6Script6SourceEv@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L221
	pxor	%xmm0, %xmm0
	movq	96(%rbx), %rsi
	movq	%rax, %rdi
	movq	$0, -112(%rbp)
	movaps	%xmm0, -144(%rbp)
	movaps	%xmm0, -128(%rbp)
	movdqa	.LC6(%rip), %xmm0
	movq	%rsi, -264(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	$1, -64(%rbp)
	call	_ZNK2v86String6LengthEv@PLT
	movabsq	$4611686018427387900, %rdx
	cltq
	cmpq	%rdx, %rax
	leaq	(%rax,%rax), %rdi
	movq	$-1, %rax
	cmova	%rax, %rdi
	call	_Znam@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	movq	%rax, -272(%rbp)
	call	_ZNK2v86String6LengthEv@PLT
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	movq	%r12, %rdx
	movq	-264(%rbp), %rsi
	movl	%eax, %r8d
	movq	%r15, %rdi
	call	_ZNK2v86String5WriteEPNS_7IsolateEPtiii@PLT
	cltq
	addq	%rax, %rax
	movq	%rax, %rcx
	movq	%rax, -280(%rbp)
	shrq	$2, %rcx
	je	.L223
	xorl	%edx, %edx
	movl	$1732584193, %r9d
	movl	$1068978529, %edi
	xorl	%r12d, %r12d
	movq	-272(%rbp), %rax
	movl	$1, %esi
	leaq	_ZZN12v8_inspector12_GLOBAL__N_113calculateHashEPN2v87IsolateENS1_5LocalINS1_6StringEEEE9randomOdd(%rip), %r15
	leaq	_ZZN12v8_inspector12_GLOBAL__N_113calculateHashEPN2v87IsolateENS1_5LocalINS1_6StringEEEE5prime(%rip), %r11
	leaq	_ZZN12v8_inspector12_GLOBAL__N_113calculateHashEPN2v87IsolateENS1_5LocalINS1_6StringEEEE6random(%rip), %r10
	movq	%rax, %r8
	leaq	(%rax,%rcx,4), %rax
	xorl	%ecx, %ecx
	movq	%rax, -264(%rbp)
	movl	$-1268369401, %eax
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L274:
	movl	(%r15,%rax,4), %eax
	movq	-144(%rbp,%rcx,8), %rdx
	movq	-96(%rbp,%rcx,8), %rsi
	movq	(%r11,%rcx,8), %rdi
	movq	(%r10,%rcx,8), %r9
.L225:
	imull	(%r8), %eax
	andl	$2147483647, %eax
	imulq	%rsi, %rax
	addq	%rdx, %rax
	xorl	%edx, %edx
	divq	%rdi
	movq	%rsi, %rax
	imulq	%r9, %rax
	movq	%rdx, -144(%rbp,%rcx,8)
	xorl	%edx, %edx
	divq	%rdi
	cmpq	$4, %rcx
	leaq	1(%rcx), %rax
	cmove	%r12, %rax
	addq	$4, %r8
	movq	%rdx, -96(%rbp,%rcx,8)
	movq	%rax, %rcx
	cmpq	%r8, -264(%rbp)
	jne	.L274
.L223:
	movq	-280(%rbp), %rax
	testb	$2, %al
	je	.L226
	movq	%rax, %rdx
	andq	$-4, %rdx
	cmpq	%rdx, %rax
	jbe	.L242
	movq	-272(%rbp), %rsi
	addq	%rsi, %rax
	addq	%rsi, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	.p2align 4,,10
	.p2align 3
.L228:
	movzbl	(%rdx), %eax
	sall	$8, %esi
	addq	$1, %rdx
	orl	%eax, %esi
	cmpq	%rdi, %rdx
	jne	.L228
.L227:
	leaq	_ZZN12v8_inspector12_GLOBAL__N_113calculateHashEPN2v87IsolateENS1_5LocalINS1_6StringEEEE5prime(%rip), %rax
	movq	-96(%rbp,%rcx,8), %rdi
	xorl	%edx, %edx
	movq	(%rax,%rcx,8), %r8
	leaq	_ZZN12v8_inspector12_GLOBAL__N_113calculateHashEPN2v87IsolateENS1_5LocalINS1_6StringEEEE9randomOdd(%rip), %rax
	imull	(%rax,%rcx,4), %esi
	movq	-144(%rbp,%rcx,8), %rax
	andl	$2147483647, %esi
	imulq	%rdi, %rsi
	addq	%rsi, %rax
	divq	%r8
	leaq	_ZZN12v8_inspector12_GLOBAL__N_113calculateHashEPN2v87IsolateENS1_5LocalINS1_6StringEEEE6random(%rip), %rax
	imulq	(%rax,%rcx,8), %rdi
	movq	%rdi, %rax
	movq	%rdx, -144(%rbp,%rcx,8)
	xorl	%edx, %edx
	divq	%r8
	movq	%rdx, -96(%rbp,%rcx,8)
.L226:
	imulq	$1068978528, -96(%rbp), %rcx
	addq	-144(%rbp), %rcx
	movabsq	$-9182273323724036987, %rdx
	movq	%rcx, %rax
	leaq	-224(%rbp), %r15
	leaq	-144(%rbp), %r12
	mulq	%rdx
	movq	%r15, %rdi
	shrq	$29, %rdx
	imulq	$1068978529, %rdx, %rdx
	subq	%rdx, %rcx
	movabsq	$6899110747468611199, %rdx
	movq	%rcx, -144(%rbp)
	movl	$2870955598, %ecx
	imulq	-88(%rbp), %rcx
	addq	-136(%rbp), %rcx
	movq	%rcx, %rax
	mulq	%rdx
	movl	$2870955599, %eax
	shrq	$30, %rdx
	imulq	%rax, %rdx
	subq	%rdx, %rcx
	movabsq	$-340030506683481745, %rdx
	movq	%rcx, -136(%rbp)
	movl	$2187811780, %ecx
	imulq	-80(%rbp), %rcx
	addq	-128(%rbp), %rcx
	movq	%rcx, %rax
	mulq	%rdx
	movl	$2187811781, %eax
	shrq	$31, %rdx
	imulq	%rax, %rdx
	subq	%rdx, %rcx
	movabsq	$4525149885966559967, %rdx
	movq	%rcx, -128(%rbp)
	movl	$3448917300, %ecx
	imulq	-72(%rbp), %rcx
	addq	-120(%rbp), %rcx
	movq	%rcx, %rax
	mulq	%rdx
	movq	%rcx, %rax
	subq	%rdx, %rax
	shrq	%rax
	addq	%rax, %rdx
	movl	$3448917301, %eax
	shrq	$31, %rdx
	imulq	%rax, %rdx
	subq	%rdx, %rcx
	movabsq	$284515175986783729, %rdx
	movq	%rcx, -120(%rbp)
	movl	$2175525496, %ecx
	imulq	-64(%rbp), %rcx
	addq	-112(%rbp), %rcx
	movq	%rcx, %rax
	mulq	%rdx
	movl	$2175525497, %eax
	shrq	$25, %rdx
	imulq	%rax, %rdx
	subq	%rdx, %rcx
	movq	%rcx, -112(%rbp)
	call	_ZN12v8_inspector15String16BuilderC1Ev@PLT
	leaq	-104(%rbp), %rax
	movq	%r13, -264(%rbp)
	movq	%r15, %r13
	movq	%r14, %r15
	movq	%rbx, %r14
	movq	%rax, %rbx
.L229:
	movl	(%r12), %esi
	movq	%r13, %rdi
	addq	$8, %r12
	call	_ZN12v8_inspector15String16Builder19appendUnsignedAsHexEj@PLT
	cmpq	%r12, %rbx
	jne	.L229
	movq	%r14, %rbx
	movq	%r15, %r14
	leaq	-192(%rbp), %rdi
	movq	%r13, %r15
	movq	%r15, %rsi
	movq	-264(%rbp), %r13
	call	_ZN12v8_inspector15String16Builder8toStringEv@PLT
	movq	-224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L230
	call	_ZdlPv@PLT
.L230:
	movq	-272(%rbp), %rdi
	leaq	-176(%rbp), %r12
	call	_ZdaPv@PLT
	movq	-192(%rbp), %rdx
	movq	160(%rbx), %rdi
	movq	-184(%rbp), %rax
	cmpq	%r12, %rdx
	je	.L275
	leaq	176(%rbx), %rcx
	movq	-176(%rbp), %rsi
	cmpq	%rcx, %rdi
	je	.L276
	movq	%rax, %xmm0
	movq	%rsi, %xmm1
	movq	176(%rbx), %rcx
	movq	%rdx, 160(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 168(%rbx)
	testq	%rdi, %rdi
	je	.L236
	movq	%rdi, -192(%rbp)
	movq	%rcx, -176(%rbp)
.L234:
	movq	$0, -184(%rbp)
	xorl	%eax, %eax
	movw	%ax, (%rdi)
	movq	-160(%rbp), %rax
	movq	-192(%rbp), %rdi
	movq	%rax, 192(%rbx)
	cmpq	%r12, %rdi
	je	.L221
	call	_ZdlPv@PLT
.L221:
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L275:
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.L232
	cmpq	$1, %rax
	je	.L277
	movq	%rax, %rdx
	addq	%rdx, %rdx
	je	.L232
	movq	%r12, %rsi
	call	memmove@PLT
	movq	-184(%rbp), %rax
	movq	160(%rbx), %rdi
	leaq	(%rax,%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L232:
	xorl	%ecx, %ecx
	movq	%rax, 168(%rbx)
	movw	%cx, (%rdi,%rdx)
	movq	-192(%rbp), %rdi
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L276:
	movq	%rax, %xmm0
	movq	%rsi, %xmm2
	movq	%rdx, 160(%rbx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 168(%rbx)
.L236:
	movq	%r12, -192(%rbp)
	leaq	-176(%rbp), %r12
	movq	%r12, %rdi
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L242:
	xorl	%esi, %esi
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L277:
	movzwl	-176(%rbp), %eax
	movw	%ax, (%rdi)
	movq	-184(%rbp), %rax
	movq	160(%rbx), %rdi
	leaq	(%rax,%rax), %rdx
	jmp	.L232
.L273:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7477:
	.size	_ZNK12v8_inspector12_GLOBAL__N_112ActualScript4hashEv, .-_ZNK12v8_inspector12_GLOBAL__N_112ActualScript4hashEv
	.section	.text._ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript4hashEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript4hashEv, @function
_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript4hashEv:
.LFB7516:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 136(%rdi)
	je	.L297
.L279:
	movq	-24(%rbp), %rsi
	xorq	%fs:40, %rsi
	leaq	128(%rbx), %rax
	jne	.L298
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L297:
	.cfi_restore_state
	movl	120(%rbx), %ecx
	movq	112(%rbx), %rsi
	leaq	-64(%rbp), %rdi
	leaq	8(%rbx), %rdx
	leaq	-48(%rbp), %r12
	call	_ZN12v8_inspector15WasmTranslation7GetHashERKNS_8String16Ei@PLT
	movq	-64(%rbp), %rdx
	movq	128(%rbx), %rdi
	movq	-56(%rbp), %rax
	cmpq	%r12, %rdx
	je	.L299
	leaq	144(%rbx), %rcx
	movq	-48(%rbp), %rsi
	cmpq	%rcx, %rdi
	je	.L300
	movq	%rax, %xmm0
	movq	%rsi, %xmm1
	movq	144(%rbx), %rcx
	movq	%rdx, 128(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 136(%rbx)
	testq	%rdi, %rdi
	je	.L285
	movq	%rdi, -64(%rbp)
	movq	%rcx, -48(%rbp)
.L283:
	xorl	%eax, %eax
	movq	$0, -56(%rbp)
	movw	%ax, (%rdi)
	movq	-32(%rbp), %rax
	movq	-64(%rbp), %rdi
	movq	%rax, 160(%rbx)
	cmpq	%r12, %rdi
	je	.L279
	call	_ZdlPv@PLT
	jmp	.L279
	.p2align 4,,10
	.p2align 3
.L300:
	movq	%rax, %xmm0
	movq	%rsi, %xmm2
	movq	%rdx, 128(%rbx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 136(%rbx)
.L285:
	movq	%r12, -64(%rbp)
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L299:
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.L281
	cmpq	$1, %rax
	je	.L301
	movq	%rax, %rdx
	addq	%rdx, %rdx
	je	.L281
	movq	%r12, %rsi
	call	memmove@PLT
	movq	-56(%rbp), %rax
	movq	128(%rbx), %rdi
	leaq	(%rax,%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L281:
	xorl	%ecx, %ecx
	movq	%rax, 136(%rbx)
	movw	%cx, (%rdi,%rdx)
	movq	-64(%rbp), %rdi
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L301:
	movzwl	-48(%rbp), %eax
	movw	%ax, (%rdi)
	movq	-56(%rbp), %rax
	movq	128(%rbx), %rdi
	leaq	(%rax,%rax), %rdx
	jmp	.L281
.L298:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7516:
	.size	_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript4hashEv, .-_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript4hashEv
	.section	.text._ZN12v8_inspector12_GLOBAL__N_112ActualScript19setSourceMappingURLERKNS_8String16E,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_112ActualScript19setSourceMappingURLERKNS_8String16E, @function
_ZN12v8_inspector12_GLOBAL__N_112ActualScript19setSourceMappingURLERKNS_8String16E:
.LFB7453:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	112(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpq	%rax, %rsi
	je	.L303
	movq	112(%rdi), %r14
	leaq	128(%rdi), %rdx
	movq	8(%rsi), %r13
	cmpq	%rdx, %r14
	je	.L312
	movq	128(%rdi), %rax
.L304:
	cmpq	%rax, %r13
	ja	.L322
	leaq	(%r13,%r13), %rdx
	testq	%r13, %r13
	je	.L308
.L325:
	movq	(%r12), %rsi
	cmpq	$1, %r13
	je	.L323
	testq	%rdx, %rdx
	je	.L308
	movq	%r14, %rdi
	call	memmove@PLT
	movq	112(%rbx), %r14
	.p2align 4,,10
	.p2align 3
.L308:
	xorl	%eax, %eax
	movq	%r13, 120(%rbx)
	movw	%ax, (%r14,%r13,2)
.L303:
	movq	32(%r12), %rax
	movq	%rax, 144(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L322:
	.cfi_restore_state
	movabsq	$2305843009213693951, %rcx
	cmpq	%rcx, %r13
	ja	.L324
	addq	%rax, %rax
	movq	%r13, %r15
	cmpq	%rax, %r13
	jnb	.L307
	cmpq	%rcx, %rax
	cmovbe	%rax, %rcx
	movq	%rcx, %r15
.L307:
	leaq	2(%r15,%r15), %rdi
	movq	%rdx, -56(%rbp)
	call	_Znwm@PLT
	movq	112(%rbx), %rdi
	movq	-56(%rbp), %rdx
	movq	%rax, %r14
	cmpq	%rdi, %rdx
	je	.L311
	call	_ZdlPv@PLT
.L311:
	movq	%r14, 112(%rbx)
	leaq	(%r13,%r13), %rdx
	movq	%r15, 128(%rbx)
	testq	%r13, %r13
	je	.L308
	jmp	.L325
	.p2align 4,,10
	.p2align 3
.L312:
	movl	$7, %eax
	jmp	.L304
	.p2align 4,,10
	.p2align 3
.L323:
	movzwl	(%rsi), %eax
	movw	%ax, (%r14)
	movq	112(%rbx), %r14
	jmp	.L308
.L324:
	leaq	.LC5(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE7453:
	.size	_ZN12v8_inspector12_GLOBAL__N_112ActualScript19setSourceMappingURLERKNS_8String16E, .-_ZN12v8_inspector12_GLOBAL__N_112ActualScript19setSourceMappingURLERKNS_8String16E
	.section	.rodata._ZN12v8_inspector12_GLOBAL__N_112ActualScript9setSourceERKNS_8String16EbPN2v85debug14LiveEditResultE.str1.1,"aMS",@progbits,1
.LC7:
	.string	"lineEnds.size()"
.LC8:
	.string	"Check failed: %s."
	.section	.text._ZN12v8_inspector12_GLOBAL__N_112ActualScript9setSourceERKNS_8String16EbPN2v85debug14LiveEditResultE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_112ActualScript9setSourceERKNS_8String16EbPN2v85debug14LiveEditResultE, @function
_ZN12v8_inspector12_GLOBAL__N_112ActualScript9setSourceERKNS_8String16EbPN2v85debug14LiveEditResultE:
.LFB7454:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-128(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$136, %rsp
	movb	%dl, -161(%rbp)
	movq	96(%rdi), %rsi
	movq	%r14, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v820EscapableHandleScopeC1EPNS_7IsolateE@PLT
	movq	96(%rbx), %rdi
	movq	%r15, %rsi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	216(%rbx), %rdi
	movq	%rax, %r8
	testq	%rdi, %rdi
	je	.L327
	movq	96(%rbx), %r9
	movq	(%rdi), %rsi
	movq	%rax, -176(%rbp)
	movq	%r9, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	-176(%rbp), %r8
	movq	%rax, %rdi
.L327:
	movzbl	%r12b, %edx
	movq	%r13, %rcx
	movq	%r8, %rsi
	call	_ZNK2v85debug6Script15SetScriptSourceENS_5LocalINS_6StringEEEbPNS0_14LiveEditResultE@PLT
	testb	%al, %al
	je	.L378
	testb	%r12b, %r12b
	je	.L379
.L329:
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L380
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L379:
	.cfi_restore_state
	movq	160(%rbx), %rax
	xorl	%edi, %edi
	leaq	-80(%rbp), %r12
	xorl	%esi, %esi
	movq	%r12, -96(%rbp)
	xorl	%r8d, %r8d
	movq	$0, -64(%rbp)
	movw	%si, -80(%rbp)
	movq	$0, 168(%rbx)
	movw	%di, (%rax)
	movq	-96(%rbp), %rax
	movq	$0, -88(%rbp)
	movw	%r8w, (%rax)
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, 192(%rbx)
	cmpq	%r12, %rdi
	je	.L330
	call	_ZdlPv@PLT
.L330:
	movq	8(%r13), %rsi
	movq	%r14, %rdi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZNK2v85debug6Script9SourceURLEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L350
	call	_ZNK2v86String6LengthEv@PLT
	testl	%eax, %eax
	setg	-161(%rbp)
.L350:
	movzbl	-161(%rbp), %eax
	movq	%r13, %rdi
	movb	%al, 88(%rbx)
	call	_ZNK2v85debug6Script16SourceMappingURLEv@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L332
	movq	96(%rbx), %rsi
	leaq	-96(%rbp), %rdi
	call	_ZN12v8_inspector16toProtocolStringEPN2v87IsolateENS0_5LocalINS0_6StringEEE@PLT
	movq	-96(%rbp), %rdx
	movq	112(%rbx), %rdi
	cmpq	%r12, %rdx
	je	.L381
	leaq	128(%rbx), %rsi
	movq	-88(%rbp), %rax
	movq	-80(%rbp), %rcx
	cmpq	%rsi, %rdi
	je	.L382
	movq	%rax, %xmm0
	movq	%rcx, %xmm1
	movq	128(%rbx), %rsi
	movq	%rdx, 112(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 120(%rbx)
	testq	%rdi, %rdi
	je	.L338
	movq	%rdi, -96(%rbp)
	movq	%rsi, -80(%rbp)
.L336:
	xorl	%eax, %eax
	movq	$0, -88(%rbp)
	movw	%ax, (%rdi)
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, 144(%rbx)
	cmpq	%r12, %rdi
	je	.L332
	call	_ZdlPv@PLT
.L332:
	movq	%r13, %rdi
	call	_ZNK2v85debug6Script10LineOffsetEv@PLT
	movq	%r13, %rdi
	movl	%eax, 200(%rbx)
	call	_ZNK2v85debug6Script12ColumnOffsetEv@PLT
	leaq	-160(%rbp), %rdi
	movq	%r13, %rsi
	movl	%eax, 204(%rbx)
	call	_ZNK2v85debug6Script8LineEndsEv@PLT
	movq	-152(%rbp), %rdx
	movq	-160(%rbp), %rcx
	movq	%rdx, %rax
	subq	%rcx, %rax
	sarq	$2, %rax
	cmpq	%rdx, %rcx
	je	.L383
	movl	200(%rbx), %esi
	movl	-4(%rdx), %ecx
	addl	%eax, %esi
	subl	$1, %esi
	cmpq	$1, %rax
	movl	%ecx, %eax
	movl	%esi, 208(%rbx)
	jbe	.L341
	subl	-8(%rdx), %eax
	subl	$1, %eax
.L342:
	movl	%eax, 212(%rbx)
	movq	%r13, %rdi
	call	_ZNK2v85debug6Script9ContextIdEv@PLT
	testb	%al, %al
	je	.L343
	sarq	$32, %rax
	movl	%eax, 92(%rbx)
.L343:
	movq	%r13, %rdi
	call	_ZNK2v85debug6Script8IsModuleEv@PLT
	movq	216(%rbx), %rdi
	movq	96(%rbx), %r12
	movb	%al, 153(%rbx)
	testq	%rdi, %rdi
	je	.L344
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 216(%rbx)
.L344:
	testq	%r13, %r13
	je	.L345
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 216(%rbx)
	movq	%rax, %r13
.L345:
	movq	%r13, %rdi
	leaq	_ZN12v8_inspector12_GLOBAL__N_1L32kGlobalDebuggerScriptHandleLabelE(%rip), %rsi
	call	_ZN2v82V822AnnotateStrongRetainerEPmPKc@PLT
	movq	-160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L329
	call	_ZdlPv@PLT
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L378:
	movq	16(%r13), %rsi
	movq	%r14, %rdi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movq	%rax, 16(%r13)
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L381:
	movq	-88(%rbp), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.L334
	cmpq	$1, %rax
	je	.L384
	movq	%rax, %rdx
	addq	%rdx, %rdx
	je	.L334
	movq	%r12, %rsi
	call	memmove@PLT
	movq	-88(%rbp), %rax
	movq	112(%rbx), %rdi
	leaq	(%rax,%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L334:
	xorl	%ecx, %ecx
	movq	%rax, 120(%rbx)
	movw	%cx, (%rdi,%rdx)
	movq	-96(%rbp), %rdi
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L341:
	addl	204(%rbx), %eax
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L382:
	movq	%rax, %xmm0
	movq	%rcx, %xmm2
	movq	%rdx, 112(%rbx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 120(%rbx)
.L338:
	movq	%r12, -96(%rbp)
	leaq	-80(%rbp), %r12
	movq	%r12, %rdi
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L383:
	leaq	.LC7(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L384:
	movzwl	-80(%rbp), %eax
	movw	%ax, (%rdi)
	movq	-88(%rbp), %rax
	movq	112(%rbx), %rdi
	leaq	(%rax,%rax), %rdx
	jmp	.L334
.L380:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7454:
	.size	_ZN12v8_inspector12_GLOBAL__N_112ActualScript9setSourceERKNS_8String16EbPN2v85debug14LiveEditResultE, .-_ZN12v8_inspector12_GLOBAL__N_112ActualScript9setSourceERKNS_8String16EbPN2v85debug14LiveEditResultE
	.section	.text._ZN12v8_inspector16V8DebuggerScript10CreateWasmEPN2v87IsolateEPNS_15WasmTranslationENS1_5LocalINS1_5debug10WasmScriptEEENS_8String16ESA_i,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector16V8DebuggerScript10CreateWasmEPN2v87IsolateEPNS_15WasmTranslationENS1_5LocalINS1_5debug10WasmScriptEEENS_8String16ESA_i
	.type	_ZN12v8_inspector16V8DebuggerScript10CreateWasmEPN2v87IsolateEPNS_15WasmTranslationENS1_5LocalINS1_5debug10WasmScriptEEENS_8String16ESA_i, @function
_ZN12v8_inspector16V8DebuggerScript10CreateWasmEPN2v87IsolateEPNS_15WasmTranslationENS1_5LocalINS1_5debug10WasmScriptEEENS_8String16ESA_i:
.LFB7523:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-224(%rbp), %r15
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -256(%rbp)
	movq	(%r8), %rdx
	movq	%rsi, -248(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16(%r8), %rax
	movq	%r15, -240(%rbp)
	cmpq	%rax, %rdx
	je	.L405
	movq	%rdx, -240(%rbp)
	movq	16(%r8), %rdx
	movq	%rdx, -224(%rbp)
.L387:
	movq	8(%r8), %rdx
	movq	%rax, (%r8)
	xorl	%ebx, %ebx
	leaq	-176(%rbp), %r13
	movq	32(%r8), %rax
	movq	$0, 8(%r8)
	movq	%rdx, -232(%rbp)
	movq	(%r9), %rdx
	movq	%rax, -208(%rbp)
	leaq	16(%r9), %rax
	movw	%bx, 16(%r8)
	movq	%r13, -192(%rbp)
	cmpq	%rax, %rdx
	je	.L406
	movq	%rdx, -192(%rbp)
	movq	16(%r9), %rdx
	movq	%rdx, -176(%rbp)
.L389:
	xorl	%r11d, %r11d
	movq	8(%r9), %rdx
	movq	%rax, (%r9)
	movl	$168, %edi
	movq	$0, 8(%r9)
	movq	32(%r9), %rax
	movw	%r11w, 16(%r9)
	movq	%rdx, -184(%rbp)
	movq	%rax, -160(%rbp)
	call	_Znwm@PLT
	leaq	-80(%rbp), %rcx
	movq	%rax, %rbx
	movq	-192(%rbp), %rax
	movq	%rcx, -96(%rbp)
	cmpq	%r13, %rax
	je	.L407
	movq	-176(%rbp), %rdx
	movq	%rax, -96(%rbp)
	movq	%rdx, -80(%rbp)
.L391:
	movq	-184(%rbp), %rdi
	movq	-160(%rbp), %rsi
	xorl	%r10d, %r10d
	movq	%r13, -192(%rbp)
	movq	-240(%rbp), %rdx
	movw	%r10w, -176(%rbp)
	movq	%rdi, -88(%rbp)
	movq	$0, -184(%rbp)
	movq	%rsi, -64(%rbp)
	cmpq	%r15, %rdx
	je	.L408
	xorl	%r8d, %r8d
	leaq	16+_ZTVN12v8_inspector16V8DebuggerScriptE(%rip), %r9
	movq	-224(%rbp), %r11
	movq	-232(%rbp), %r10
	movw	%r8w, -224(%rbp)
	movq	-208(%rbp), %r8
	movq	%r9, (%rbx)
	leaq	24(%rbx), %r9
	movq	%r9, 8(%rbx)
	leaq	-128(%rbp), %r9
	movq	%r11, -128(%rbp)
	movq	%r15, -240(%rbp)
	movq	$0, -232(%rbp)
	movq	%r8, -112(%rbp)
	cmpq	%r9, %rdx
	je	.L393
	movq	%rdx, 8(%rbx)
	movq	%r11, 24(%rbx)
.L395:
	xorl	%edx, %edx
	movq	%r10, 16(%rbx)
	movw	%dx, -128(%rbp)
	leaq	64(%rbx), %rdx
	movq	%r9, -144(%rbp)
	movq	$0, -136(%rbp)
	movq	%r8, 40(%rbx)
	movq	%rdx, 48(%rbx)
	cmpq	%rcx, %rax
	je	.L409
	movq	%rax, 48(%rbx)
	movq	-80(%rbp), %rax
	movq	%rax, 64(%rbx)
.L397:
	movq	%rdi, 56(%rbx)
	movq	-248(%rbp), %rdi
	leaq	16+_ZTVN12v8_inspector12_GLOBAL__N_117WasmVirtualScriptE(%rip), %rax
	movq	%rsi, 80(%rbx)
	movb	$0, 88(%rbx)
	movl	$0, 92(%rbx)
	movq	%rdi, 96(%rbx)
	movq	%rax, (%rbx)
	testq	%r12, %r12
	je	.L403
	movq	%r12, %rsi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, %rdi
.L398:
	movl	16(%rbp), %eax
	movq	%rdi, %xmm0
	movq	$0, 136(%rbx)
	leaq	_ZN12v8_inspector12_GLOBAL__N_1L32kGlobalDebuggerScriptHandleLabelE(%rip), %rsi
	movq	$0, 160(%rbx)
	movhps	-256(%rbp), %xmm0
	movl	%eax, 120(%rbx)
	leaq	144(%rbx), %rax
	movq	%rax, 128(%rbx)
	xorl	%eax, %eax
	movw	%ax, 144(%rbx)
	movups	%xmm0, 104(%rbx)
	call	_ZN2v82V822AnnotateStrongRetainerEPmPKc@PLT
	movq	%r12, %rdi
	call	_ZNK2v85debug6Script9ContextIdEv@PLT
	movq	%rax, %r12
	testb	%al, %al
	je	.L410
.L399:
	movq	-192(%rbp), %rdi
	sarq	$32, %r12
	movl	%r12d, 92(%rbx)
	cmpq	%r13, %rdi
	je	.L400
	call	_ZdlPv@PLT
.L400:
	movq	-240(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L401
	call	_ZdlPv@PLT
.L401:
	movq	%rbx, (%r14)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L411
	addq	$216, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L405:
	.cfi_restore_state
	movdqu	16(%r8), %xmm1
	movaps	%xmm1, -224(%rbp)
	jmp	.L387
	.p2align 4,,10
	.p2align 3
.L409:
	movdqa	-80(%rbp), %xmm6
	movups	%xmm6, 64(%rbx)
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L408:
	leaq	16+_ZTVN12v8_inspector16V8DebuggerScriptE(%rip), %rdx
	movq	-208(%rbp), %r8
	xorl	%r9d, %r9d
	movdqa	-224(%rbp), %xmm4
	movq	%rdx, (%rbx)
	leaq	24(%rbx), %rdx
	movq	-232(%rbp), %r10
	movq	%r8, -112(%rbp)
	movq	$0, -232(%rbp)
	movq	%rdx, 8(%rbx)
	movw	%r9w, -224(%rbp)
	leaq	-128(%rbp), %r9
	movaps	%xmm4, -128(%rbp)
.L393:
	movdqa	-128(%rbp), %xmm5
	movups	%xmm5, 24(%rbx)
	jmp	.L395
	.p2align 4,,10
	.p2align 3
.L407:
	movdqa	-176(%rbp), %xmm3
	movq	%rcx, %rax
	movaps	%xmm3, -80(%rbp)
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L406:
	movdqu	16(%r9), %xmm2
	movaps	%xmm2, -176(%rbp)
	jmp	.L389
	.p2align 4,,10
	.p2align 3
.L403:
	xorl	%edi, %edi
	jmp	.L398
	.p2align 4,,10
	.p2align 3
.L410:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L399
.L411:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7523:
	.size	_ZN12v8_inspector16V8DebuggerScript10CreateWasmEPN2v87IsolateEPNS_15WasmTranslationENS1_5LocalINS1_5debug10WasmScriptEEENS_8String16ESA_i, .-_ZN12v8_inspector16V8DebuggerScript10CreateWasmEPN2v87IsolateEPNS_15WasmTranslationENS1_5LocalINS1_5debug10WasmScriptEEENS_8String16ESA_i
	.section	.text._ZN12v8_inspector16V8DebuggerScriptC2EPN2v87IsolateENS_8String16ES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector16V8DebuggerScriptC2EPN2v87IsolateENS_8String16ES4_
	.type	_ZN12v8_inspector16V8DebuggerScriptC2EPN2v87IsolateENS_8String16ES4_, @function
_ZN12v8_inspector16V8DebuggerScriptC2EPN2v87IsolateENS_8String16ES4_:
.LFB7527:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN12v8_inspector16V8DebuggerScriptE(%rip), %rax
	movq	(%rdx), %r8
	movq	%rax, (%rdi)
	leaq	24(%rdi), %rax
	movq	%rax, 8(%rdi)
	leaq	16(%rdx), %rax
	cmpq	%rax, %r8
	je	.L417
	movq	%r8, 8(%rdi)
	movq	16(%rdx), %r8
	movq	%r8, 24(%rdi)
.L414:
	movq	8(%rdx), %r8
	movq	%rax, (%rdx)
	movq	32(%rdx), %rax
	movq	$0, 8(%rdx)
	movq	%r8, 16(%rdi)
	xorl	%r8d, %r8d
	movw	%r8w, 16(%rdx)
	movq	(%rcx), %rdx
	movq	%rax, 40(%rdi)
	leaq	64(%rdi), %rax
	movq	%rax, 48(%rdi)
	leaq	16(%rcx), %rax
	cmpq	%rax, %rdx
	je	.L418
	movq	%rdx, 48(%rdi)
	movq	16(%rcx), %rdx
	movq	%rdx, 64(%rdi)
.L416:
	movq	%rax, (%rcx)
	xorl	%eax, %eax
	movq	8(%rcx), %rdx
	movw	%ax, 16(%rcx)
	movq	32(%rcx), %rax
	movq	%rdx, 56(%rdi)
	movq	$0, 8(%rcx)
	movq	%rax, 80(%rdi)
	movb	$0, 88(%rdi)
	movl	$0, 92(%rdi)
	movq	%rsi, 96(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L417:
	movdqu	16(%rdx), %xmm0
	movups	%xmm0, 24(%rdi)
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L418:
	movdqu	16(%rcx), %xmm1
	movups	%xmm1, 64(%rdi)
	jmp	.L416
	.cfi_endproc
.LFE7527:
	.size	_ZN12v8_inspector16V8DebuggerScriptC2EPN2v87IsolateENS_8String16ES4_, .-_ZN12v8_inspector16V8DebuggerScriptC2EPN2v87IsolateENS_8String16ES4_
	.globl	_ZN12v8_inspector16V8DebuggerScriptC1EPN2v87IsolateENS_8String16ES4_
	.set	_ZN12v8_inspector16V8DebuggerScriptC1EPN2v87IsolateENS_8String16ES4_,_ZN12v8_inspector16V8DebuggerScriptC2EPN2v87IsolateENS_8String16ES4_
	.section	.text._ZN12v8_inspector16V8DebuggerScriptD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector16V8DebuggerScriptD2Ev
	.type	_ZN12v8_inspector16V8DebuggerScriptD2Ev, @function
_ZN12v8_inspector16V8DebuggerScriptD2Ev:
.LFB7530:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector16V8DebuggerScriptE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	48(%rdi), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L420
	call	_ZdlPv@PLT
.L420:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L419
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L419:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7530:
	.size	_ZN12v8_inspector16V8DebuggerScriptD2Ev, .-_ZN12v8_inspector16V8DebuggerScriptD2Ev
	.globl	_ZN12v8_inspector16V8DebuggerScriptD1Ev
	.set	_ZN12v8_inspector16V8DebuggerScriptD1Ev,_ZN12v8_inspector16V8DebuggerScriptD2Ev
	.section	.text._ZN12v8_inspector16V8DebuggerScriptD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector16V8DebuggerScriptD0Ev
	.type	_ZN12v8_inspector16V8DebuggerScriptD0Ev, @function
_ZN12v8_inspector16V8DebuggerScriptD0Ev:
.LFB7532:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector16V8DebuggerScriptE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	48(%rdi), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L424
	call	_ZdlPv@PLT
.L424:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L425
	call	_ZdlPv@PLT
.L425:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$104, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE7532:
	.size	_ZN12v8_inspector16V8DebuggerScriptD0Ev, .-_ZN12v8_inspector16V8DebuggerScriptD0Ev
	.section	.text._ZN12v8_inspector16V8DebuggerScript12setSourceURLERKNS_8String16E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector16V8DebuggerScript12setSourceURLERKNS_8String16E
	.type	_ZN12v8_inspector16V8DebuggerScript12setSourceURLERKNS_8String16E, @function
_ZN12v8_inspector16V8DebuggerScript12setSourceURLERKNS_8String16E:
.LFB7533:
	.cfi_startproc
	endbr64
	cmpq	$0, 8(%rsi)
	jne	.L450
	ret
	.p2align 4,,10
	.p2align 3
.L450:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	48(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movb	$1, 88(%rdi)
	cmpq	%rax, %rsi
	je	.L429
	movq	48(%rdi), %r14
	leaq	64(%rdi), %rdx
	movq	8(%rsi), %r13
	cmpq	%rdx, %r14
	je	.L438
	movq	64(%rdi), %rax
.L430:
	cmpq	%rax, %r13
	ja	.L451
.L431:
	leaq	(%r13,%r13), %rdx
	testq	%r13, %r13
	je	.L434
	movq	(%r12), %rsi
	cmpq	$1, %r13
	je	.L452
	testq	%rdx, %rdx
	je	.L434
	movq	%r14, %rdi
	call	memmove@PLT
	movq	48(%rbx), %r14
	.p2align 4,,10
	.p2align 3
.L434:
	xorl	%eax, %eax
	movq	%r13, 56(%rbx)
	movw	%ax, (%r14,%r13,2)
.L429:
	movq	32(%r12), %rax
	movq	%rax, 80(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L451:
	.cfi_restore_state
	movabsq	$2305843009213693951, %rcx
	cmpq	%rcx, %r13
	ja	.L453
	addq	%rax, %rax
	movq	%r13, %r15
	cmpq	%rax, %r13
	jnb	.L433
	cmpq	%rcx, %rax
	cmovbe	%rax, %rcx
	movq	%rcx, %r15
.L433:
	leaq	2(%r15,%r15), %rdi
	movq	%rdx, -56(%rbp)
	call	_Znwm@PLT
	movq	48(%rbx), %rdi
	movq	-56(%rbp), %rdx
	movq	%rax, %r14
	cmpq	%rdi, %rdx
	je	.L437
	call	_ZdlPv@PLT
.L437:
	movq	%r14, 48(%rbx)
	movq	%r15, 64(%rbx)
	jmp	.L431
	.p2align 4,,10
	.p2align 3
.L438:
	movl	$7, %eax
	jmp	.L430
	.p2align 4,,10
	.p2align 3
.L452:
	movzwl	(%rsi), %eax
	movw	%ax, (%r14)
	movq	48(%rbx), %r14
	jmp	.L434
.L453:
	leaq	.LC5(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE7533:
	.size	_ZN12v8_inspector16V8DebuggerScript12setSourceURLERKNS_8String16E, .-_ZN12v8_inspector16V8DebuggerScript12setSourceURLERKNS_8String16E
	.section	.text._ZNK12v8_inspector16V8DebuggerScript13setBreakpointERKNS_8String16EPN2v85debug8LocationEPi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector16V8DebuggerScript13setBreakpointERKNS_8String16EPN2v85debug8LocationEPi
	.type	_ZNK12v8_inspector16V8DebuggerScript13setBreakpointERKNS_8String16EPN2v85debug8LocationEPi, @function
_ZNK12v8_inspector16V8DebuggerScript13setBreakpointERKNS_8String16EPN2v85debug8LocationEPi:
.LFB7534:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	leaq	-80(%rbp), %r8
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	96(%rdi), %rsi
	movq	%r8, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r8, -88(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*176(%rax)
	movq	96(%rbx), %rdi
	movq	%r15, %rsi
	movq	%rax, %r12
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	%r12, %rdi
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%rax, %rsi
	call	_ZNK2v85debug6Script13SetBreakpointENS_5LocalINS_6StringEEEPNS0_8LocationEPi@PLT
	movq	-88(%rbp), %r8
	movl	%eax, %r12d
	movq	%r8, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L457
	addq	$56, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L457:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7534:
	.size	_ZNK12v8_inspector16V8DebuggerScript13setBreakpointERKNS_8String16EPN2v85debug8LocationEPi, .-_ZNK12v8_inspector16V8DebuggerScript13setBreakpointERKNS_8String16EPN2v85debug8LocationEPi
	.section	.rodata._ZNSt6vectorIN2v85debug13BreakLocationESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_.str1.1,"aMS",@progbits,1
.LC9:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIN2v85debug13BreakLocationESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v85debug13BreakLocationESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v85debug13BreakLocationESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.type	_ZNSt6vectorIN2v85debug13BreakLocationESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, @function
_ZNSt6vectorIN2v85debug13BreakLocationESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_:
.LFB9935:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movabsq	$576460752303423487, %rsi
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rcx
	movq	(%rdi), %r14
	movq	%rcx, %rax
	subq	%r14, %rax
	sarq	$4, %rax
	cmpq	%rsi, %rax
	je	.L475
	movq	%r12, %r9
	movq	%rdi, %r15
	subq	%r14, %r9
	testq	%rax, %rax
	je	.L467
	movabsq	$9223372036854775792, %r13
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L476
.L460:
	movq	%r13, %rdi
	movq	%rdx, -80(%rbp)
	movq	%r9, -72(%rbp)
	movq	%rcx, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %r9
	movq	%rax, %rbx
	leaq	(%rax,%r13), %rax
	movq	-80(%rbp), %rdx
	movq	%rax, -56(%rbp)
	leaq	16(%rbx), %r13
.L466:
	movdqu	(%rdx), %xmm2
	movups	%xmm2, (%rbx,%r9)
	cmpq	%r14, %r12
	je	.L462
	movq	%rbx, %rdx
	movq	%r14, %rax
	.p2align 4,,10
	.p2align 3
.L463:
	movdqu	(%rax), %xmm1
	addq	$16, %rax
	addq	$16, %rdx
	movups	%xmm1, -16(%rdx)
	cmpq	%rax, %r12
	jne	.L463
	movq	%r12, %rax
	subq	%r14, %rax
	leaq	16(%rbx,%rax), %r13
.L462:
	cmpq	%rcx, %r12
	je	.L464
	subq	%r12, %rcx
	movq	%r13, %rdi
	movq	%r12, %rsi
	movq	%rcx, %rdx
	movq	%rcx, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %rcx
	addq	%rcx, %r13
.L464:
	testq	%r14, %r14
	je	.L465
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L465:
	movq	-56(%rbp), %rax
	movq	%rbx, %xmm0
	movq	%r13, %xmm3
	punpcklqdq	%xmm3, %xmm0
	movq	%rax, 16(%r15)
	movups	%xmm0, (%r15)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L476:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L461
	movq	$0, -56(%rbp)
	movl	$16, %r13d
	xorl	%ebx, %ebx
	jmp	.L466
	.p2align 4,,10
	.p2align 3
.L467:
	movl	$16, %r13d
	jmp	.L460
.L461:
	cmpq	%rsi, %rdi
	cmovbe	%rdi, %rsi
	movq	%rsi, %r13
	salq	$4, %r13
	jmp	.L460
.L475:
	leaq	.LC9(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE9935:
	.size	_ZNSt6vectorIN2v85debug13BreakLocationESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, .-_ZNSt6vectorIN2v85debug13BreakLocationESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.section	.text._ZN12v8_inspector12_GLOBAL__N_112ActualScript22getPossibleBreakpointsERKN2v85debug8LocationES6_bPSt6vectorINS3_13BreakLocationESaIS8_EE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_112ActualScript22getPossibleBreakpointsERKN2v85debug8LocationES6_bPSt6vectorINS3_13BreakLocationESaIS8_EE, @function
_ZN12v8_inspector12_GLOBAL__N_112ActualScript22getPossibleBreakpointsERKN2v85debug8LocationES6_bPSt6vectorINS3_13BreakLocationESaIS8_EE:
.LFB7462:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$104, %rsp
	movq	96(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-112(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -144(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	216(%r12), %rdi
	testq	%rdi, %rdi
	je	.L478
	movq	96(%r12), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
.L478:
	pxor	%xmm0, %xmm0
	movzbl	%r13b, %ecx
	leaq	-80(%rbp), %r8
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	$0, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZNK2v85debug6Script22GetPossibleBreakpointsERKNS0_8LocationES4_bPSt6vectorINS0_13BreakLocationESaIS6_EE@PLT
	movb	%al, -129(%rbp)
	testb	%al, %al
	je	.L503
	movq	-80(%rbp), %rdi
	movq	-72(%rbp), %r8
	cmpq	%r8, %rdi
	je	.L480
	movdqu	(%rdi), %xmm3
	subq	%rdi, %r8
	movl	$1, %r12d
	leaq	-128(%rbp), %rbx
	movaps	%xmm3, -128(%rbp)
	cmpq	$16, %r8
	jne	.L481
	.p2align 4,,10
	.p2align 3
.L489:
	movq	8(%r14), %rsi
	cmpq	16(%r14), %rsi
	je	.L505
	movdqa	-128(%rbp), %xmm4
	movups	%xmm4, (%rsi)
	movq	-80(%rbp), %r8
	addq	$16, 8(%r14)
.L480:
	testq	%r8, %r8
	je	.L490
	movq	%r8, %rdi
	call	_ZdlPv@PLT
.L490:
	movq	-144(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L506
	movzbl	-129(%rbp), %eax
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L508:
	.cfi_restore_state
	movq	-80(%rbp), %rdi
	addq	%r13, %rdi
	call	_ZNK2v85debug8Location15GetColumnNumberEv@PLT
	movq	%rbx, %rdi
	movl	%eax, %r15d
	call	_ZNK2v85debug8Location15GetColumnNumberEv@PLT
	cmpl	%eax, %r15d
	je	.L507
	movq	8(%r14), %rsi
	cmpq	16(%r14), %rsi
	je	.L487
.L509:
	movdqa	-128(%rbp), %xmm2
	movups	%xmm2, (%rsi)
	addq	$16, 8(%r14)
.L488:
	movq	-80(%rbp), %rdi
	movdqu	(%rdi,%r13), %xmm1
	movaps	%xmm1, -128(%rbp)
.L486:
	movq	-72(%rbp), %rax
	addq	$1, %r12
	subq	%rdi, %rax
	sarq	$4, %rax
	cmpq	%rax, %r12
	jnb	.L489
.L481:
	movq	%r12, %r13
	salq	$4, %r13
	addq	%r13, %rdi
	call	_ZNK2v85debug8Location13GetLineNumberEv@PLT
	movq	%rbx, %rdi
	movl	%eax, %r15d
	call	_ZNK2v85debug8Location13GetLineNumberEv@PLT
	cmpl	%eax, %r15d
	je	.L508
	movq	8(%r14), %rsi
	cmpq	16(%r14), %rsi
	jne	.L509
.L487:
	movq	%rbx, %rdx
	movq	%r14, %rdi
	call	_ZNSt6vectorIN2v85debug13BreakLocationESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	jmp	.L488
	.p2align 4,,10
	.p2align 3
.L507:
	movq	-80(%rbp), %rdi
	leaq	(%rdi,%r13), %r8
	cmpl	$3, 12(%r8)
	je	.L486
	movdqu	(%r8), %xmm5
	movaps	%xmm5, -128(%rbp)
	jmp	.L486
	.p2align 4,,10
	.p2align 3
.L505:
	leaq	-128(%rbp), %rdx
	movq	%r14, %rdi
	call	_ZNSt6vectorIN2v85debug13BreakLocationESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
.L503:
	movq	-80(%rbp), %r8
	jmp	.L480
.L506:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7462:
	.size	_ZN12v8_inspector12_GLOBAL__N_112ActualScript22getPossibleBreakpointsERKN2v85debug8LocationES6_bPSt6vectorINS3_13BreakLocationESaIS8_EE, .-_ZN12v8_inspector12_GLOBAL__N_112ActualScript22getPossibleBreakpointsERKN2v85debug8LocationES6_bPSt6vectorINS3_13BreakLocationESaIS8_EE
	.section	.text._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag,"axG",@progbits,_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	.type	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag, @function
_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag:
.LFB11618:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	testq	%rsi, %rsi
	jne	.L511
	testq	%rdx, %rdx
	jne	.L527
.L511:
	subq	%r13, %rbx
	movq	%rbx, %r14
	sarq	%r14
	cmpq	$15, %rbx
	ja	.L512
	movq	(%r12), %rdi
.L513:
	cmpq	$2, %rbx
	je	.L528
	testq	%rbx, %rbx
	je	.L516
	movq	%rbx, %rdx
	movq	%r13, %rsi
	call	memmove@PLT
	movq	(%r12), %rdi
.L516:
	xorl	%eax, %eax
	movq	%r14, 8(%r12)
	movw	%ax, (%rdi,%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L528:
	.cfi_restore_state
	movzwl	0(%r13), %eax
	movw	%ax, (%rdi)
	movq	(%r12), %rdi
	jmp	.L516
	.p2align 4,,10
	.p2align 3
.L512:
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %r14
	ja	.L529
	leaq	2(%rbx), %rdi
	call	_Znwm@PLT
	movq	%r14, 16(%r12)
	movq	%rax, (%r12)
	movq	%rax, %rdi
	jmp	.L513
.L527:
	leaq	.LC1(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L529:
	leaq	.LC5(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE11618:
	.size	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag, .-_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	.section	.text._ZN12v8_inspector16V8DebuggerScript6CreateEPN2v87IsolateENS1_5LocalINS1_5debug6ScriptEEEbPNS_19V8DebuggerAgentImplEPNS_17V8InspectorClientE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector16V8DebuggerScript6CreateEPN2v87IsolateENS1_5LocalINS1_5debug6ScriptEEEbPNS_19V8DebuggerAgentImplEPNS_17V8InspectorClientE
	.type	_ZN12v8_inspector16V8DebuggerScript6CreateEPN2v87IsolateENS1_5LocalINS1_5debug6ScriptEEEbPNS_19V8DebuggerAgentImplEPNS_17V8InspectorClientE, @function
_ZN12v8_inspector16V8DebuggerScript6CreateEPN2v87IsolateENS1_5LocalINS1_5debug6ScriptEEEbPNS_19V8DebuggerAgentImplEPNS_17V8InspectorClientE:
.LFB7520:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$200, %rsp
	movq	%rdi, -200(%rbp)
	movl	$224, %edi
	movq	%rsi, -208(%rbp)
	movl	%ecx, -220(%rbp)
	movq	%r8, -232(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZNK2v85debug6Script9SourceURLEv@PLT
	testq	%rax, %rax
	je	.L531
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZNK2v86String6LengthEv@PLT
	testl	%eax, %eax
	jg	.L602
.L531:
	movq	%r12, %rdi
	call	_ZNK2v85debug6Script4NameEv@PLT
	testq	%rax, %rax
	je	.L564
	movq	%rax, %rdi
	movq	%rax, -216(%rbp)
	call	_ZNK2v86String6LengthEv@PLT
	testl	%eax, %eax
	jg	.L603
.L564:
	leaq	-128(%rbp), %rax
	movq	$0, -112(%rbp)
	pxor	%xmm0, %xmm0
	leaq	-96(%rbp), %r15
	movq	%rax, -240(%rbp)
	leaq	-80(%rbp), %rbx
	movq	%rax, -144(%rbp)
	leaq	-176(%rbp), %rax
	movq	$0, -136(%rbp)
	movq	%rax, -216(%rbp)
	movaps	%xmm0, -128(%rbp)
.L533:
	movq	%r12, %rdi
	call	_ZNK2v85debug6Script2IdEv@PLT
	movq	%r15, %rdi
	movl	%eax, %esi
	call	_ZN12v8_inspector8String1611fromIntegerEi@PLT
	leaq	16+_ZTVN12v8_inspector16V8DebuggerScriptE(%rip), %rax
	movq	%rax, 0(%r13)
	leaq	24(%r13), %rax
	movq	%rax, 8(%r13)
	movq	-96(%rbp), %rax
	cmpq	%rbx, %rax
	je	.L604
	movq	%rax, 8(%r13)
	movq	-80(%rbp), %rax
	movq	%rax, 24(%r13)
.L541:
	movq	-88(%rbp), %rax
	xorl	%r8d, %r8d
	movq	%rbx, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rax, 16(%r13)
	movq	-64(%rbp), %rax
	movw	%r8w, -80(%rbp)
	movq	%rax, 40(%r13)
	leaq	64(%r13), %rax
	movq	%rax, 48(%r13)
	movq	-144(%rbp), %rax
	cmpq	-240(%rbp), %rax
	je	.L605
	movq	%rax, 48(%r13)
	movq	-128(%rbp), %rax
	movq	%rax, 64(%r13)
.L543:
	movq	-136(%rbp), %rax
	xorl	%edi, %edi
	xorl	%esi, %esi
	movb	$0, 88(%r13)
	movq	-208(%rbp), %xmm0
	movw	%di, 176(%r13)
	movq	%r12, %rdi
	movq	%rax, 56(%r13)
	movq	-112(%rbp), %rax
	movhps	-232(%rbp), %xmm0
	movl	$0, 92(%r13)
	movq	%rax, 80(%r13)
	leaq	16+_ZTVN12v8_inspector12_GLOBAL__N_112ActualScriptE(%rip), %rax
	movq	%rax, 0(%r13)
	leaq	128(%r13), %rax
	movq	%rax, -208(%rbp)
	movq	%rax, 112(%r13)
	movzbl	-220(%rbp), %eax
	movups	%xmm0, 96(%r13)
	pxor	%xmm0, %xmm0
	movb	%al, 152(%r13)
	leaq	176(%r13), %rax
	movq	$0, 120(%r13)
	movw	%si, 128(%r13)
	movq	$0, 144(%r13)
	movb	$0, 153(%r13)
	movq	%rax, 160(%r13)
	movq	$0, 168(%r13)
	movq	$0, 192(%r13)
	movq	$0, 216(%r13)
	movups	%xmm0, 200(%r13)
	call	_ZNK2v85debug6Script9SourceURLEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L606
	call	_ZNK2v86String6LengthEv@PLT
	testl	%eax, %eax
	setg	%al
.L562:
	movq	%r12, %rdi
	movb	%al, 88(%r13)
	call	_ZNK2v85debug6Script16SourceMappingURLEv@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L545
	movq	96(%r13), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector16toProtocolStringEPN2v87IsolateENS0_5LocalINS0_6StringEEE@PLT
	movq	-96(%rbp), %rdx
	movq	112(%r13), %rdi
	cmpq	%rbx, %rdx
	je	.L607
	movq	-88(%rbp), %rax
	movq	-80(%rbp), %rsi
	cmpq	%rdi, -208(%rbp)
	je	.L608
	movq	%rax, %xmm0
	movq	%rsi, %xmm3
	movq	128(%r13), %r8
	movq	%rdx, 112(%r13)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 120(%r13)
	testq	%rdi, %rdi
	je	.L551
	movq	%rdi, -96(%rbp)
	movq	%r8, -80(%rbp)
.L549:
	xorl	%eax, %eax
	movq	$0, -88(%rbp)
	movw	%ax, (%rdi)
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, 144(%r13)
	cmpq	%rbx, %rdi
	je	.L545
	call	_ZdlPv@PLT
.L545:
	movq	%r12, %rdi
	call	_ZNK2v85debug6Script10LineOffsetEv@PLT
	movq	%r12, %rdi
	movl	%eax, 200(%r13)
	call	_ZNK2v85debug6Script12ColumnOffsetEv@PLT
	movq	-216(%rbp), %rdi
	movq	%r12, %rsi
	movl	%eax, 204(%r13)
	call	_ZNK2v85debug6Script8LineEndsEv@PLT
	movq	-168(%rbp), %rdx
	movq	-176(%rbp), %rsi
	movq	%rdx, %rax
	subq	%rsi, %rax
	sarq	$2, %rax
	cmpq	%rdx, %rsi
	je	.L609
	movl	200(%r13), %edi
	movl	-4(%rdx), %esi
	addl	%eax, %edi
	subl	$1, %edi
	cmpq	$1, %rax
	movl	%esi, %eax
	movl	%edi, 208(%r13)
	jbe	.L554
	subl	-8(%rdx), %eax
	subl	$1, %eax
.L555:
	movq	%r12, %rdi
	movl	%eax, 212(%r13)
	call	_ZNK2v85debug6Script9ContextIdEv@PLT
	testb	%al, %al
	je	.L556
	sarq	$32, %rax
	movl	%eax, 92(%r13)
.L556:
	movq	%r12, %rdi
	call	_ZNK2v85debug6Script8IsModuleEv@PLT
	movq	216(%r13), %rdi
	movq	96(%r13), %r15
	movb	%al, 153(%r13)
	testq	%rdi, %rdi
	je	.L557
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 216(%r13)
.L557:
	testq	%r12, %r12
	je	.L558
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 216(%r13)
	movq	%rax, %r14
.L558:
	movq	%r14, %rdi
	leaq	_ZN12v8_inspector12_GLOBAL__N_1L32kGlobalDebuggerScriptHandleLabelE(%rip), %rsi
	call	_ZN2v82V822AnnotateStrongRetainerEPmPKc@PLT
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L559
	call	_ZdlPv@PLT
.L559:
	movq	-200(%rbp), %rax
	movq	%r13, (%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L610
	movq	-200(%rbp), %rax
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L602:
	.cfi_restore_state
	movq	-208(%rbp), %rsi
	movq	%r15, %rdx
	leaq	-80(%rbp), %rbx
	leaq	-96(%rbp), %r15
	leaq	-144(%rbp), %rdi
	call	_ZN12v8_inspector16toProtocolStringEPN2v87IsolateENS0_5LocalINS0_6StringEEE@PLT
	leaq	-128(%rbp), %rax
	movq	%rax, -240(%rbp)
	leaq	-176(%rbp), %rax
	movq	%rax, -216(%rbp)
	jmp	.L533
	.p2align 4,,10
	.p2align 3
.L606:
	xorl	%eax, %eax
	jmp	.L562
	.p2align 4,,10
	.p2align 3
.L605:
	movdqa	-128(%rbp), %xmm2
	movups	%xmm2, 64(%r13)
	jmp	.L543
	.p2align 4,,10
	.p2align 3
.L604:
	movdqa	-80(%rbp), %xmm1
	movups	%xmm1, 24(%r13)
	jmp	.L541
	.p2align 4,,10
	.p2align 3
.L554:
	addl	204(%r13), %eax
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L607:
	movq	-88(%rbp), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.L547
	cmpq	$1, %rax
	je	.L611
	movq	%rax, %rdx
	addq	%rdx, %rdx
	je	.L547
	movq	%rbx, %rsi
	call	memmove@PLT
	movq	-88(%rbp), %rax
	movq	112(%r13), %rdi
	leaq	(%rax,%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L547:
	xorl	%ecx, %ecx
	movq	%rax, 120(%r13)
	movw	%cx, (%rdi,%rdx)
	movq	-96(%rbp), %rdi
	jmp	.L549
	.p2align 4,,10
	.p2align 3
.L603:
	movq	-216(%rbp), %rdx
	movq	-208(%rbp), %rsi
	leaq	-96(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN12v8_inspector16toProtocolStringEPN2v87IsolateENS0_5LocalINS0_6StringEEE@PLT
	movq	(%rbx), %rax
	movq	%r15, %rsi
	movq	216(%rax), %rax
	movq	%rax, -240(%rbp)
	leaq	-176(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN12v8_inspector12toStringViewERKNS_8String16E@PLT
	movq	-240(%rbp), %rax
	leaq	_ZN12v8_inspector17V8InspectorClient17resourceNameToUrlERKNS_10StringViewE(%rip), %rdx
	cmpq	%rdx, %rax
	jne	.L535
	movq	$0, -184(%rbp)
.L536:
	leaq	-128(%rbp), %rax
	movq	-96(%rbp), %rsi
	leaq	-144(%rbp), %rdi
	movq	%rax, -240(%rbp)
	movq	%rax, -144(%rbp)
	movq	-88(%rbp), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	-64(%rbp), %rax
	movq	%rax, -112(%rbp)
	.p2align 4,,10
	.p2align 3
.L537:
	movq	-184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L538
	movq	(%rdi), %rax
	call	*8(%rax)
.L538:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rbx
	cmpq	%rbx, %rdi
	je	.L533
	call	_ZdlPv@PLT
	jmp	.L533
	.p2align 4,,10
	.p2align 3
.L608:
	movq	%rax, %xmm0
	movq	%rsi, %xmm4
	movq	%rdx, 112(%r13)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, 120(%r13)
.L551:
	movq	%rbx, -96(%rbp)
	leaq	-80(%rbp), %rbx
	movq	%rbx, %rdi
	jmp	.L549
	.p2align 4,,10
	.p2align 3
.L609:
	leaq	.LC7(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L535:
	leaq	-184(%rbp), %rdi
	movq	-216(%rbp), %rdx
	movq	%rbx, %rsi
	call	*%rax
	movq	-184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L536
	movq	(%rdi), %rax
	call	*16(%rax)
	leaq	-144(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN12v8_inspector10toString16ERKNS_10StringViewE@PLT
	leaq	-128(%rbp), %rax
	movq	%rax, -240(%rbp)
	jmp	.L537
	.p2align 4,,10
	.p2align 3
.L611:
	movzwl	-80(%rbp), %eax
	movw	%ax, (%rdi)
	movq	-88(%rbp), %rax
	movq	112(%r13), %rdi
	leaq	(%rax,%rax), %rdx
	jmp	.L547
.L610:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7520:
	.size	_ZN12v8_inspector16V8DebuggerScript6CreateEPN2v87IsolateENS1_5LocalINS1_5debug6ScriptEEEbPNS_19V8DebuggerAgentImplEPNS_17V8InspectorClientE, .-_ZN12v8_inspector16V8DebuggerScript6CreateEPN2v87IsolateENS1_5LocalINS1_5debug6ScriptEEEbPNS_19V8DebuggerAgentImplEPNS_17V8InspectorClientE
	.section	.text._ZN12v8_inspector12_GLOBAL__N_137TranslateProtocolLocationToV8LocationEPNS_15WasmTranslationEPN2v85debug8LocationERKNS_8String16ES9_.isra.0.part.0,"ax",@progbits
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_137TranslateProtocolLocationToV8LocationEPNS_15WasmTranslationEPN2v85debug8LocationERKNS_8String16ES9_.isra.0.part.0, @function
_ZN12v8_inspector12_GLOBAL__N_137TranslateProtocolLocationToV8LocationEPNS_15WasmTranslationEPN2v85debug8LocationERKNS_8String16ES9_.isra.0.part.0:
.LFB12864:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r15
	leaq	-80(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v85debug8Location13GetLineNumberEv@PLT
	movq	%rbx, %rdi
	movl	%eax, -116(%rbp)
	call	_ZNK2v85debug8Location15GetColumnNumberEv@PLT
	movq	(%r12), %rsi
	movq	%r15, %rdi
	movq	%r14, -96(%rbp)
	movl	%eax, -112(%rbp)
	movq	8(%r12), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	32(%r12), %rax
	movq	%r15, %rsi
	movq	%r13, %rdi
	leaq	-112(%rbp), %rcx
	leaq	-116(%rbp), %rdx
	movq	%rax, -64(%rbp)
	call	_ZN12v8_inspector15WasmTranslation45TranslateProtocolLocationToWasmScriptLocationEPNS_8String16EPiS3_@PLT
	movl	-112(%rbp), %edx
	movl	-116(%rbp), %esi
	leaq	-108(%rbp), %rdi
	call	_ZN2v85debug8LocationC1Eii@PLT
	movq	-108(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, (%rbx)
	movzbl	-100(%rbp), %eax
	movb	%al, 8(%rbx)
	cmpq	%r14, %rdi
	je	.L612
	call	_ZdlPv@PLT
.L612:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L616
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L616:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE12864:
	.size	_ZN12v8_inspector12_GLOBAL__N_137TranslateProtocolLocationToV8LocationEPNS_15WasmTranslationEPN2v85debug8LocationERKNS_8String16ES9_.isra.0.part.0, .-_ZN12v8_inspector12_GLOBAL__N_137TranslateProtocolLocationToV8LocationEPNS_15WasmTranslationEPN2v85debug8LocationERKNS_8String16ES9_.isra.0.part.0
	.section	.text._ZN12v8_inspector12_GLOBAL__N_137TranslateV8LocationToProtocolLocationEPNS_15WasmTranslationEPN2v85debug8LocationERKNS_8String16ES9_.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_137TranslateV8LocationToProtocolLocationEPNS_15WasmTranslationEPN2v85debug8LocationERKNS_8String16ES9_.isra.0, @function
_ZN12v8_inspector12_GLOBAL__N_137TranslateV8LocationToProtocolLocationEPNS_15WasmTranslationEPN2v85debug8LocationERKNS_8String16ES9_.isra.0:
.LFB12865:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r15
	leaq	-80(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v85debug8Location13GetLineNumberEv@PLT
	movq	%rbx, %rdi
	movl	%eax, -116(%rbp)
	call	_ZNK2v85debug8Location15GetColumnNumberEv@PLT
	movq	(%r12), %rsi
	movq	%r15, %rdi
	movq	%r14, -96(%rbp)
	movl	%eax, -112(%rbp)
	movq	8(%r12), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	32(%r12), %rax
	movq	%r15, %rsi
	movq	%r13, %rdi
	leaq	-112(%rbp), %rcx
	leaq	-116(%rbp), %rdx
	movq	%rax, -64(%rbp)
	call	_ZN12v8_inspector15WasmTranslation45TranslateWasmScriptLocationToProtocolLocationEPNS_8String16EPiS3_@PLT
	movl	-112(%rbp), %edx
	movl	-116(%rbp), %esi
	leaq	-108(%rbp), %rdi
	call	_ZN2v85debug8LocationC1Eii@PLT
	movq	-108(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, (%rbx)
	movzbl	-100(%rbp), %eax
	movb	%al, 8(%rbx)
	cmpq	%r14, %rdi
	je	.L617
	call	_ZdlPv@PLT
.L617:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L621
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L621:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE12865:
	.size	_ZN12v8_inspector12_GLOBAL__N_137TranslateV8LocationToProtocolLocationEPNS_15WasmTranslationEPN2v85debug8LocationERKNS_8String16ES9_.isra.0, .-_ZN12v8_inspector12_GLOBAL__N_137TranslateV8LocationToProtocolLocationEPNS_15WasmTranslationEPN2v85debug8LocationERKNS_8String16ES9_.isra.0
	.section	.text._ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript13setBreakpointERKNS_8String16EPN2v85debug8LocationEPi,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript13setBreakpointERKNS_8String16EPN2v85debug8LocationEPi, @function
_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript13setBreakpointERKNS_8String16EPN2v85debug8LocationEPi:
.LFB7514:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-128(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	%rsi, -144(%rbp)
	movq	96(%rdi), %rsi
	movq	%r14, %rdi
	movq	%rcx, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	104(%rbx), %r13
	testq	%r13, %r13
	je	.L623
	movq	0(%r13), %rsi
	movq	96(%rbx), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %r13
.L623:
	movq	%r13, %rdi
	leaq	-96(%rbp), %r15
	call	_ZNK2v85debug6Script2IdEv@PLT
	movq	%r15, %rdi
	movl	%eax, %esi
	call	_ZN12v8_inspector8String1611fromIntegerEi@PLT
	movq	112(%rbx), %rax
	movq	%r12, %rdi
	movq	%rax, -136(%rbp)
	call	_ZNK2v85debug8Location7IsEmptyEv@PLT
	testb	%al, %al
	jne	.L624
	movq	-136(%rbp), %rdi
	leaq	8(%rbx), %rdx
	movq	%r12, %rsi
	call	_ZN12v8_inspector12_GLOBAL__N_137TranslateProtocolLocationToV8LocationEPNS_15WasmTranslationEPN2v85debug8LocationERKNS_8String16ES9_.isra.0.part.0
.L624:
	movq	%r12, %rdi
	call	_ZNK2v85debug8Location7IsEmptyEv@PLT
	testb	%al, %al
	je	.L625
.L627:
	xorl	%r13d, %r13d
.L626:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L628
	call	_ZdlPv@PLT
.L628:
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L637
	addq	$120, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L625:
	.cfi_restore_state
	movq	96(%rbx), %rdi
	movq	-144(%rbp), %rsi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	-152(%rbp), %rcx
	movq	%r13, %rdi
	movq	%r12, %rdx
	movq	%rax, %rsi
	call	_ZNK2v85debug6Script13SetBreakpointENS_5LocalINS_6StringEEEPNS0_8LocationEPi@PLT
	movl	%eax, %r13d
	testb	%al, %al
	je	.L627
	movq	112(%rbx), %rdi
	movq	%r15, %rdx
	movq	%r12, %rsi
	call	_ZN12v8_inspector12_GLOBAL__N_137TranslateV8LocationToProtocolLocationEPNS_15WasmTranslationEPN2v85debug8LocationERKNS_8String16ES9_.isra.0
	jmp	.L626
.L637:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7514:
	.size	_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript13setBreakpointERKNS_8String16EPN2v85debug8LocationEPi, .-_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript13setBreakpointERKNS_8String16EPN2v85debug8LocationEPi
	.section	.text._ZN12v8_inspector12_GLOBAL__N_117WasmVirtualScript22getPossibleBreakpointsERKN2v85debug8LocationES6_bPSt6vectorINS3_13BreakLocationESaIS8_EE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_117WasmVirtualScript22getPossibleBreakpointsERKN2v85debug8LocationES6_bPSt6vectorINS3_13BreakLocationESaIS8_EE, @function
_ZN12v8_inspector12_GLOBAL__N_117WasmVirtualScript22getPossibleBreakpointsERKN2v85debug8LocationES6_bPSt6vectorINS3_13BreakLocationESaIS8_EE:
.LFB7510:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$168, %rsp
	movl	%ecx, -188(%rbp)
	movq	96(%rdi), %rsi
	movq	%r8, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-128(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -200(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	104(%rbx), %r13
	testq	%r13, %r13
	je	.L639
	movq	0(%r13), %rsi
	movq	96(%rbx), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %r13
.L639:
	movq	%r13, %rdi
	leaq	-96(%rbp), %r12
	call	_ZNK2v85debug6Script2IdEv@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN12v8_inspector8String1611fromIntegerEi@PLT
	movq	(%r15), %rax
	movq	112(%rbx), %rcx
	movq	%rax, -164(%rbp)
	movl	8(%r15), %eax
	leaq	-164(%rbp), %r15
	movq	%r15, %rdi
	movq	%rcx, -208(%rbp)
	movl	%eax, -156(%rbp)
	call	_ZNK2v85debug8Location7IsEmptyEv@PLT
	testb	%al, %al
	jne	.L640
	movq	-208(%rbp), %rdi
	leaq	8(%rbx), %rdx
	movq	%r15, %rsi
	call	_ZN12v8_inspector12_GLOBAL__N_137TranslateProtocolLocationToV8LocationEPNS_15WasmTranslationEPN2v85debug8LocationERKNS_8String16ES9_.isra.0.part.0
.L640:
	movq	(%r14), %rax
	movq	%rax, -152(%rbp)
	movl	8(%r14), %eax
	leaq	-152(%rbp), %r14
	movq	%r14, %rdi
	movl	%eax, -144(%rbp)
	call	_ZNK2v85debug8Location7IsEmptyEv@PLT
	testb	%al, %al
	je	.L641
	movq	%r15, %rdi
	call	_ZNK2v85debug8Location13GetLineNumberEv@PLT
	leaq	-140(%rbp), %rdi
	xorl	%edx, %edx
	leal	1(%rax), %esi
	call	_ZN2v85debug8LocationC1Eii@PLT
	movq	-140(%rbp), %rax
	movq	%rax, -152(%rbp)
	movzbl	-132(%rbp), %eax
	movb	%al, -144(%rbp)
.L642:
	movzbl	-188(%rbp), %ecx
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	-184(%rbp), %r8
	call	_ZNK2v85debug6Script22GetPossibleBreakpointsERKNS0_8LocationES4_bPSt6vectorINS0_13BreakLocationESaIS6_EE@PLT
	movl	%eax, %r13d
	movq	-184(%rbp), %rax
	movq	(%rax), %r15
	movq	8(%rax), %r14
	cmpq	%r15, %r14
	je	.L643
	.p2align 4,,10
	.p2align 3
.L644:
	movq	112(%rbx), %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	addq	$16, %r15
	call	_ZN12v8_inspector12_GLOBAL__N_137TranslateV8LocationToProtocolLocationEPNS_15WasmTranslationEPN2v85debug8LocationERKNS_8String16ES9_.isra.0
	cmpq	%r15, %r14
	jne	.L644
.L643:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L645
	call	_ZdlPv@PLT
.L645:
	movq	-200(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L652
	addq	$168, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L641:
	.cfi_restore_state
	movq	112(%rbx), %rax
	movq	%r14, %rdi
	movq	%rax, -208(%rbp)
	call	_ZNK2v85debug8Location7IsEmptyEv@PLT
	testb	%al, %al
	jne	.L642
	movq	-208(%rbp), %rdi
	leaq	8(%rbx), %rdx
	movq	%r14, %rsi
	call	_ZN12v8_inspector12_GLOBAL__N_137TranslateProtocolLocationToV8LocationEPNS_15WasmTranslationEPN2v85debug8LocationERKNS_8String16ES9_.isra.0.part.0
	jmp	.L642
.L652:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7510:
	.size	_ZN12v8_inspector12_GLOBAL__N_117WasmVirtualScript22getPossibleBreakpointsERKN2v85debug8LocationES6_bPSt6vectorINS3_13BreakLocationESaIS8_EE, .-_ZN12v8_inspector12_GLOBAL__N_117WasmVirtualScript22getPossibleBreakpointsERKN2v85debug8LocationES6_bPSt6vectorINS3_13BreakLocationESaIS8_EE
	.section	.data.rel.ro.local._ZTVN12v8_inspector12_GLOBAL__N_112ActualScriptE,"aw"
	.align 8
	.type	_ZTVN12v8_inspector12_GLOBAL__N_112ActualScriptE, @object
	.size	_ZTVN12v8_inspector12_GLOBAL__N_112ActualScriptE, 200
_ZTVN12v8_inspector12_GLOBAL__N_112ActualScriptE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector12_GLOBAL__N_112ActualScriptD1Ev
	.quad	_ZN12v8_inspector12_GLOBAL__N_112ActualScriptD0Ev
	.quad	_ZNK12v8_inspector12_GLOBAL__N_112ActualScript16sourceMappingURLEv
	.quad	_ZNK12v8_inspector12_GLOBAL__N_112ActualScript6sourceEmm
	.quad	_ZNK12v8_inspector12_GLOBAL__N_112ActualScript4hashEv
	.quad	_ZNK12v8_inspector12_GLOBAL__N_112ActualScript9startLineEv
	.quad	_ZNK12v8_inspector12_GLOBAL__N_112ActualScript11startColumnEv
	.quad	_ZNK12v8_inspector12_GLOBAL__N_112ActualScript7endLineEv
	.quad	_ZNK12v8_inspector12_GLOBAL__N_112ActualScript9endColumnEv
	.quad	_ZNK12v8_inspector12_GLOBAL__N_112ActualScript10isLiveEditEv
	.quad	_ZNK12v8_inspector12_GLOBAL__N_112ActualScript8isModuleEv
	.quad	_ZNK12v8_inspector12_GLOBAL__N_112ActualScript20isSourceLoadedLazilyEv
	.quad	_ZNK12v8_inspector12_GLOBAL__N_112ActualScript6lengthEv
	.quad	_ZN12v8_inspector12_GLOBAL__N_112ActualScript19setSourceMappingURLERKNS_8String16E
	.quad	_ZN12v8_inspector12_GLOBAL__N_112ActualScript9setSourceERKNS_8String16EbPN2v85debug14LiveEditResultE
	.quad	_ZN12v8_inspector12_GLOBAL__N_112ActualScript22getPossibleBreakpointsERKN2v85debug8LocationES6_bPSt6vectorINS3_13BreakLocationESaIS8_EE
	.quad	_ZN12v8_inspector12_GLOBAL__N_112ActualScript25resetBlackboxedStateCacheEv
	.quad	_ZNK12v8_inspector12_GLOBAL__N_112ActualScript6offsetEii
	.quad	_ZNK12v8_inspector12_GLOBAL__N_112ActualScript8locationEi
	.quad	_ZNK12v8_inspector12_GLOBAL__N_112ActualScript13setBreakpointERKNS_8String16EPN2v85debug8LocationEPi
	.quad	_ZN12v8_inspector12_GLOBAL__N_112ActualScript8MakeWeakEv
	.quad	_ZNK12v8_inspector12_GLOBAL__N_112ActualScript18setBreakpointOnRunEPi
	.quad	_ZNK12v8_inspector12_GLOBAL__N_112ActualScript6scriptEv
	.section	.data.rel.ro.local._ZTVN12v8_inspector12_GLOBAL__N_117WasmVirtualScriptE,"aw"
	.align 8
	.type	_ZTVN12v8_inspector12_GLOBAL__N_117WasmVirtualScriptE, @object
	.size	_ZTVN12v8_inspector12_GLOBAL__N_117WasmVirtualScriptE, 200
_ZTVN12v8_inspector12_GLOBAL__N_117WasmVirtualScriptE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector12_GLOBAL__N_117WasmVirtualScriptD1Ev
	.quad	_ZN12v8_inspector12_GLOBAL__N_117WasmVirtualScriptD0Ev
	.quad	_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript16sourceMappingURLEv
	.quad	_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript6sourceEmm
	.quad	_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript4hashEv
	.quad	_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript9startLineEv
	.quad	_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript11startColumnEv
	.quad	_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript7endLineEv
	.quad	_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript9endColumnEv
	.quad	_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript10isLiveEditEv
	.quad	_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript8isModuleEv
	.quad	_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript20isSourceLoadedLazilyEv
	.quad	_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript6lengthEv
	.quad	_ZN12v8_inspector12_GLOBAL__N_117WasmVirtualScript19setSourceMappingURLERKNS_8String16E
	.quad	_ZN12v8_inspector12_GLOBAL__N_117WasmVirtualScript9setSourceERKNS_8String16EbPN2v85debug14LiveEditResultE
	.quad	_ZN12v8_inspector12_GLOBAL__N_117WasmVirtualScript22getPossibleBreakpointsERKN2v85debug8LocationES6_bPSt6vectorINS3_13BreakLocationESaIS8_EE
	.quad	_ZN12v8_inspector12_GLOBAL__N_117WasmVirtualScript25resetBlackboxedStateCacheEv
	.quad	_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript6offsetEii
	.quad	_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript8locationEi
	.quad	_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript13setBreakpointERKNS_8String16EPN2v85debug8LocationEPi
	.quad	_ZN12v8_inspector12_GLOBAL__N_117WasmVirtualScript8MakeWeakEv
	.quad	_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript18setBreakpointOnRunEPi
	.quad	_ZNK12v8_inspector12_GLOBAL__N_117WasmVirtualScript6scriptEv
	.weak	_ZTVN12v8_inspector16V8DebuggerScriptE
	.section	.data.rel.ro._ZTVN12v8_inspector16V8DebuggerScriptE,"awG",@progbits,_ZTVN12v8_inspector16V8DebuggerScriptE,comdat
	.align 8
	.type	_ZTVN12v8_inspector16V8DebuggerScriptE, @object
	.size	_ZTVN12v8_inspector16V8DebuggerScriptE, 200
_ZTVN12v8_inspector16V8DebuggerScriptE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.section	.bss._ZGVZN12v8_inspector12_GLOBAL__N_117WasmVirtualScript11emptyStringEvE17singleEmptyString,"aw",@nobits
	.align 8
	.type	_ZGVZN12v8_inspector12_GLOBAL__N_117WasmVirtualScript11emptyStringEvE17singleEmptyString, @object
	.size	_ZGVZN12v8_inspector12_GLOBAL__N_117WasmVirtualScript11emptyStringEvE17singleEmptyString, 8
_ZGVZN12v8_inspector12_GLOBAL__N_117WasmVirtualScript11emptyStringEvE17singleEmptyString:
	.zero	8
	.section	.bss._ZZN12v8_inspector12_GLOBAL__N_117WasmVirtualScript11emptyStringEvE17singleEmptyString,"aw",@nobits
	.align 8
	.type	_ZZN12v8_inspector12_GLOBAL__N_117WasmVirtualScript11emptyStringEvE17singleEmptyString, @object
	.size	_ZZN12v8_inspector12_GLOBAL__N_117WasmVirtualScript11emptyStringEvE17singleEmptyString, 8
_ZZN12v8_inspector12_GLOBAL__N_117WasmVirtualScript11emptyStringEvE17singleEmptyString:
	.zero	8
	.section	.rodata._ZZN12v8_inspector12_GLOBAL__N_113calculateHashEPN2v87IsolateENS1_5LocalINS1_6StringEEEE9randomOdd,"a"
	.align 16
	.type	_ZZN12v8_inspector12_GLOBAL__N_113calculateHashEPN2v87IsolateENS1_5LocalINS1_6StringEEEE9randomOdd, @object
	.size	_ZZN12v8_inspector12_GLOBAL__N_113calculateHashEPN2v87IsolateENS1_5LocalINS1_6StringEEEE9randomOdd, 20
_ZZN12v8_inspector12_GLOBAL__N_113calculateHashEPN2v87IsolateENS1_5LocalINS1_6StringEEEE9randomOdd:
	.long	-1268369401
	.long	-869127179
	.long	-721871939
	.long	-1480679139
	.long	-1891227385
	.section	.rodata._ZZN12v8_inspector12_GLOBAL__N_113calculateHashEPN2v87IsolateENS1_5LocalINS1_6StringEEEE6random,"a"
	.align 32
	.type	_ZZN12v8_inspector12_GLOBAL__N_113calculateHashEPN2v87IsolateENS1_5LocalINS1_6StringEEEE6random, @object
	.size	_ZZN12v8_inspector12_GLOBAL__N_113calculateHashEPN2v87IsolateENS1_5LocalINS1_6StringEEEE6random, 40
_ZZN12v8_inspector12_GLOBAL__N_113calculateHashEPN2v87IsolateENS1_5LocalINS1_6StringEEEE6random:
	.quad	1732584193
	.quad	4023233417
	.quad	2562383102
	.quad	271733878
	.quad	3285377520
	.section	.rodata._ZZN12v8_inspector12_GLOBAL__N_113calculateHashEPN2v87IsolateENS1_5LocalINS1_6StringEEEE5prime,"a"
	.align 32
	.type	_ZZN12v8_inspector12_GLOBAL__N_113calculateHashEPN2v87IsolateENS1_5LocalINS1_6StringEEEE5prime, @object
	.size	_ZZN12v8_inspector12_GLOBAL__N_113calculateHashEPN2v87IsolateENS1_5LocalINS1_6StringEEEE5prime, 40
_ZZN12v8_inspector12_GLOBAL__N_113calculateHashEPN2v87IsolateENS1_5LocalINS1_6StringEEEE5prime:
	.quad	1068978529
	.quad	2870955599
	.quad	2187811781
	.quad	3448917301
	.quad	2175525497
	.section	.rodata._ZN12v8_inspector12_GLOBAL__N_1L32kGlobalDebuggerScriptHandleLabelE,"a"
	.align 16
	.type	_ZN12v8_inspector12_GLOBAL__N_1L32kGlobalDebuggerScriptHandleLabelE, @object
	.size	_ZN12v8_inspector12_GLOBAL__N_1L32kGlobalDebuggerScriptHandleLabelE, 18
_ZN12v8_inspector12_GLOBAL__N_1L32kGlobalDebuggerScriptHandleLabelE:
	.string	"DevTools debugger"
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC6:
	.quad	1
	.quad	1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
