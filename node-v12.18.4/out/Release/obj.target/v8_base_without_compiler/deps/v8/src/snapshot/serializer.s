	.file	"serializer.cc"
	.text
	.section	.text._ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE,"axG",@progbits,_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE
	.type	_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE, @function
_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE:
.LFB7690:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7690:
	.size	_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE, .-_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE
	.section	.text._ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE,"axG",@progbits,_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE
	.type	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE, @function
_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE:
.LFB7693:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	leaq	8(%rdx), %rcx
	movq	24(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE7693:
	.size	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE, .-_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE
	.section	.text._ZN2v88internal17CodeEventListener27is_listening_to_code_eventsEv,"axG",@progbits,_ZN2v88internal17CodeEventListener27is_listening_to_code_eventsEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal17CodeEventListener27is_listening_to_code_eventsEv
	.type	_ZN2v88internal17CodeEventListener27is_listening_to_code_eventsEv, @function
_ZN2v88internal17CodeEventListener27is_listening_to_code_eventsEv:
.LFB8684:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE8684:
	.size	_ZN2v88internal17CodeEventListener27is_listening_to_code_eventsEv, .-_ZN2v88internal17CodeEventListener27is_listening_to_code_eventsEv
	.section	.text._ZN2v88internal15CodeEventLogger13CallbackEventENS0_4NameEm,"axG",@progbits,_ZN2v88internal15CodeEventLogger13CallbackEventENS0_4NameEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15CodeEventLogger13CallbackEventENS0_4NameEm
	.type	_ZN2v88internal15CodeEventLogger13CallbackEventENS0_4NameEm, @function
_ZN2v88internal15CodeEventLogger13CallbackEventENS0_4NameEm:
.LFB8742:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8742:
	.size	_ZN2v88internal15CodeEventLogger13CallbackEventENS0_4NameEm, .-_ZN2v88internal15CodeEventLogger13CallbackEventENS0_4NameEm
	.section	.text._ZN2v88internal15CodeEventLogger19GetterCallbackEventENS0_4NameEm,"axG",@progbits,_ZN2v88internal15CodeEventLogger19GetterCallbackEventENS0_4NameEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15CodeEventLogger19GetterCallbackEventENS0_4NameEm
	.type	_ZN2v88internal15CodeEventLogger19GetterCallbackEventENS0_4NameEm, @function
_ZN2v88internal15CodeEventLogger19GetterCallbackEventENS0_4NameEm:
.LFB8743:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8743:
	.size	_ZN2v88internal15CodeEventLogger19GetterCallbackEventENS0_4NameEm, .-_ZN2v88internal15CodeEventLogger19GetterCallbackEventENS0_4NameEm
	.section	.text._ZN2v88internal15CodeEventLogger19SetterCallbackEventENS0_4NameEm,"axG",@progbits,_ZN2v88internal15CodeEventLogger19SetterCallbackEventENS0_4NameEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15CodeEventLogger19SetterCallbackEventENS0_4NameEm
	.type	_ZN2v88internal15CodeEventLogger19SetterCallbackEventENS0_4NameEm, @function
_ZN2v88internal15CodeEventLogger19SetterCallbackEventENS0_4NameEm:
.LFB8744:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8744:
	.size	_ZN2v88internal15CodeEventLogger19SetterCallbackEventENS0_4NameEm, .-_ZN2v88internal15CodeEventLogger19SetterCallbackEventENS0_4NameEm
	.section	.text._ZN2v88internal15CodeEventLogger27SharedFunctionInfoMoveEventEmm,"axG",@progbits,_ZN2v88internal15CodeEventLogger27SharedFunctionInfoMoveEventEmm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15CodeEventLogger27SharedFunctionInfoMoveEventEmm
	.type	_ZN2v88internal15CodeEventLogger27SharedFunctionInfoMoveEventEmm, @function
_ZN2v88internal15CodeEventLogger27SharedFunctionInfoMoveEventEmm:
.LFB8745:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8745:
	.size	_ZN2v88internal15CodeEventLogger27SharedFunctionInfoMoveEventEmm, .-_ZN2v88internal15CodeEventLogger27SharedFunctionInfoMoveEventEmm
	.section	.text._ZN2v88internal15CodeEventLogger22NativeContextMoveEventEmm,"axG",@progbits,_ZN2v88internal15CodeEventLogger22NativeContextMoveEventEmm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15CodeEventLogger22NativeContextMoveEventEmm
	.type	_ZN2v88internal15CodeEventLogger22NativeContextMoveEventEmm, @function
_ZN2v88internal15CodeEventLogger22NativeContextMoveEventEmm:
.LFB8746:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8746:
	.size	_ZN2v88internal15CodeEventLogger22NativeContextMoveEventEmm, .-_ZN2v88internal15CodeEventLogger22NativeContextMoveEventEmm
	.section	.text._ZN2v88internal15CodeEventLogger17CodeMovingGCEventEv,"axG",@progbits,_ZN2v88internal15CodeEventLogger17CodeMovingGCEventEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15CodeEventLogger17CodeMovingGCEventEv
	.type	_ZN2v88internal15CodeEventLogger17CodeMovingGCEventEv, @function
_ZN2v88internal15CodeEventLogger17CodeMovingGCEventEv:
.LFB8747:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8747:
	.size	_ZN2v88internal15CodeEventLogger17CodeMovingGCEventEv, .-_ZN2v88internal15CodeEventLogger17CodeMovingGCEventEv
	.section	.text._ZN2v88internal15CodeEventLogger14CodeDeoptEventENS0_4CodeENS0_14DeoptimizeKindEmi,"axG",@progbits,_ZN2v88internal15CodeEventLogger14CodeDeoptEventENS0_4CodeENS0_14DeoptimizeKindEmi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15CodeEventLogger14CodeDeoptEventENS0_4CodeENS0_14DeoptimizeKindEmi
	.type	_ZN2v88internal15CodeEventLogger14CodeDeoptEventENS0_4CodeENS0_14DeoptimizeKindEmi, @function
_ZN2v88internal15CodeEventLogger14CodeDeoptEventENS0_4CodeENS0_14DeoptimizeKindEmi:
.LFB8748:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8748:
	.size	_ZN2v88internal15CodeEventLogger14CodeDeoptEventENS0_4CodeENS0_14DeoptimizeKindEmi, .-_ZN2v88internal15CodeEventLogger14CodeDeoptEventENS0_4CodeENS0_14DeoptimizeKindEmi
	.section	.text._ZN2v88internal14CodeAddressMap19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE,"axG",@progbits,_ZN2v88internal14CodeAddressMap19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14CodeAddressMap19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE
	.type	_ZN2v88internal14CodeAddressMap19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE, @function
_ZN2v88internal14CodeAddressMap19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE:
.LFB8921:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8921:
	.size	_ZN2v88internal14CodeAddressMap19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE, .-_ZN2v88internal14CodeAddressMap19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE
	.section	.text._ZN2v88internal10Serializer16ObjectSerializerD2Ev,"axG",@progbits,_ZN2v88internal10Serializer16ObjectSerializerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10Serializer16ObjectSerializerD2Ev
	.type	_ZN2v88internal10Serializer16ObjectSerializerD2Ev, @function
_ZN2v88internal10Serializer16ObjectSerializerD2Ev:
.LFB8987:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8987:
	.size	_ZN2v88internal10Serializer16ObjectSerializerD2Ev, .-_ZN2v88internal10Serializer16ObjectSerializerD2Ev
	.weak	_ZN2v88internal10Serializer16ObjectSerializerD1Ev
	.set	_ZN2v88internal10Serializer16ObjectSerializerD1Ev,_ZN2v88internal10Serializer16ObjectSerializerD2Ev
	.section	.text._ZNK2v88internal29NativesExternalStringResource4dataEv,"axG",@progbits,_ZNK2v88internal29NativesExternalStringResource4dataEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal29NativesExternalStringResource4dataEv
	.type	_ZNK2v88internal29NativesExternalStringResource4dataEv, @function
_ZNK2v88internal29NativesExternalStringResource4dataEv:
.LFB21075:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE21075:
	.size	_ZNK2v88internal29NativesExternalStringResource4dataEv, .-_ZNK2v88internal29NativesExternalStringResource4dataEv
	.section	.text._ZN2v88internal10Serializer14MustBeDeferredENS0_10HeapObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10Serializer14MustBeDeferredENS0_10HeapObjectE
	.type	_ZN2v88internal10Serializer14MustBeDeferredENS0_10HeapObjectE, @function
_ZN2v88internal10Serializer14MustBeDeferredENS0_10HeapObjectE:
.LFB21299:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE21299:
	.size	_ZN2v88internal10Serializer14MustBeDeferredENS0_10HeapObjectE, .-_ZN2v88internal10Serializer14MustBeDeferredENS0_10HeapObjectE
	.section	.text._ZN2v88internal10Serializer16ObjectSerializer13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10Serializer16ObjectSerializer13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES4_
	.type	_ZN2v88internal10Serializer16ObjectSerializer13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES4_, @function
_ZN2v88internal10Serializer16ObjectSerializer13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES4_:
.LFB21332:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*24(%rax)
	.cfi_endproc
.LFE21332:
	.size	_ZN2v88internal10Serializer16ObjectSerializer13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES4_, .-_ZN2v88internal10Serializer16ObjectSerializer13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES4_
	.section	.rodata._ZN2v88internal14CodeAddressMap17LogRecordedBufferEPKNS0_4wasm8WasmCodeEPKci.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal14CodeAddressMap17LogRecordedBufferEPKNS0_4wasm8WasmCodeEPKci,"axG",@progbits,_ZN2v88internal14CodeAddressMap17LogRecordedBufferEPKNS0_4wasm8WasmCodeEPKci,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14CodeAddressMap17LogRecordedBufferEPKNS0_4wasm8WasmCodeEPKci
	.type	_ZN2v88internal14CodeAddressMap17LogRecordedBufferEPKNS0_4wasm8WasmCodeEPKci, @function
_ZN2v88internal14CodeAddressMap17LogRecordedBufferEPKNS0_4wasm8WasmCodeEPKci:
.LFB8941:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8941:
	.size	_ZN2v88internal14CodeAddressMap17LogRecordedBufferEPKNS0_4wasm8WasmCodeEPKci, .-_ZN2v88internal14CodeAddressMap17LogRecordedBufferEPKNS0_4wasm8WasmCodeEPKci
	.section	.text._ZN2v88internal10Serializer16ObjectSerializer17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10Serializer16ObjectSerializer17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE
	.type	_ZN2v88internal10Serializer16ObjectSerializer17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE, @function
_ZN2v88internal10Serializer16ObjectSerializer17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE:
.LFB21338:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21338:
	.size	_ZN2v88internal10Serializer16ObjectSerializer17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE, .-_ZN2v88internal10Serializer16ObjectSerializer17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE
	.section	.text._ZN2v88internal10Serializer16ObjectSerializerD0Ev,"axG",@progbits,_ZN2v88internal10Serializer16ObjectSerializerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10Serializer16ObjectSerializerD0Ev
	.type	_ZN2v88internal10Serializer16ObjectSerializerD0Ev, @function
_ZN2v88internal10Serializer16ObjectSerializerD0Ev:
.LFB8989:
	.cfi_startproc
	endbr64
	movl	$40, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8989:
	.size	_ZN2v88internal10Serializer16ObjectSerializerD0Ev, .-_ZN2v88internal10Serializer16ObjectSerializerD0Ev
	.section	.text._ZN2v88internal14CodeAddressMapD2Ev,"axG",@progbits,_ZN2v88internal14CodeAddressMapD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14CodeAddressMapD2Ev
	.type	_ZN2v88internal14CodeAddressMapD2Ev, @function
_ZN2v88internal14CodeAddressMapD2Ev:
.LFB8917:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal14CodeAddressMapE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%r12, %rsi
	movq	%rax, (%rdi)
	movq	8(%rdi), %rax
	movq	41016(%rax), %rdi
	call	_ZN2v88internal6Logger23RemoveCodeEventListenerEPNS0_17CodeEventListenerE@PLT
	movl	32(%r12), %edx
	movq	24(%r12), %rdi
	leaq	(%rdx,%rdx,2), %rax
	leaq	(%rdi,%rax,8), %rcx
	cmpq	%rcx, %rdi
	jnb	.L23
	movq	%rdi, %rax
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L36:
	cmpq	%rcx, %rax
	jnb	.L23
.L25:
	movq	(%rax), %rsi
	movq	%rax, %rbx
	addq	$24, %rax
	testq	%rsi, %rsi
	je	.L36
	.p2align 4,,10
	.p2align 3
.L24:
	movq	8(%rbx), %r8
	testq	%r8, %r8
	je	.L26
	movq	%r8, %rdi
	call	_ZdaPv@PLT
	movq	24(%r12), %rdi
	movl	32(%r12), %edx
.L26:
	leaq	(%rdx,%rdx,2), %rax
	leaq	(%rdi,%rax,8), %rcx
	leaq	24(%rbx), %rax
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L37:
	movq	(%rax), %rsi
	movq	%rax, %rbx
	addq	$24, %rax
	testq	%rsi, %rsi
	jne	.L24
.L35:
	cmpq	%rax, %rcx
	ja	.L37
.L23:
	call	free@PLT
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal15CodeEventLoggerD2Ev@PLT
	.cfi_endproc
.LFE8917:
	.size	_ZN2v88internal14CodeAddressMapD2Ev, .-_ZN2v88internal14CodeAddressMapD2Ev
	.weak	_ZN2v88internal14CodeAddressMapD1Ev
	.set	_ZN2v88internal14CodeAddressMapD1Ev,_ZN2v88internal14CodeAddressMapD2Ev
	.section	.text._ZN2v88internal10Serializer16ObjectSerializer20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10Serializer16ObjectSerializer20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE
	.type	_ZN2v88internal10Serializer16ObjectSerializer20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE, @function
_ZN2v88internal10Serializer16ObjectSerializer20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE:
.LFB21334:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	8(%rdi), %rdi
	movq	(%rdx), %rdx
	movq	(%rdi), %rax
	movq	(%rdx), %rsi
	call	*40(%rax)
	movq	%r12, %rdi
	call	_ZN2v88internal9RelocInfo16IsCodedSpeciallyEv@PLT
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$4, %eax
	addl	$4, %eax
	addl	%eax, 32(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21334:
	.size	_ZN2v88internal10Serializer16ObjectSerializer20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE, .-_ZN2v88internal10Serializer16ObjectSerializer20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE
	.section	.rodata._ZN2v88internal10Serializer16ObjectSerializer15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"address < start || address >= end"
	.section	.rodata._ZN2v88internal10Serializer16ObjectSerializer15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal10Serializer16ObjectSerializer15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10Serializer16ObjectSerializer15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE
	.type	_ZN2v88internal10Serializer16ObjectSerializer15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE, @function
_ZN2v88internal10Serializer16ObjectSerializer15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE:
.LFB21340:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	(%rdx), %rsi
	movslq	(%rsi), %rbx
	addq	%rsi, %rbx
	call	_ZN2v88internal7Isolate19CurrentEmbeddedBlobEv@PLT
	leaq	4(%rbx), %r15
	movq	%rax, %r14
	call	_ZN2v88internal7Isolate23CurrentEmbeddedBlobSizeEv@PLT
	movl	%eax, %eax
	addq	%r14, %rax
	cmpq	%rax, %r15
	jnb	.L43
	cmpq	%r15, %r14
	jbe	.L53
.L43:
	movq	8(%r12), %rdi
	leaq	-59(%rbx), %rsi
	movq	(%rdi), %rax
	call	*40(%rax)
	movq	%r13, %rdi
	call	_ZN2v88internal9RelocInfo16IsCodedSpeciallyEv@PLT
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$4, %eax
	addl	$4, %eax
	addl	%eax, 32(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore_state
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21340:
	.size	_ZN2v88internal10Serializer16ObjectSerializer15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE, .-_ZN2v88internal10Serializer16ObjectSerializer15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE
	.section	.text._ZN2v88internal14CodeAddressMapD0Ev,"axG",@progbits,_ZN2v88internal14CodeAddressMapD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14CodeAddressMapD0Ev
	.type	_ZN2v88internal14CodeAddressMapD0Ev, @function
_ZN2v88internal14CodeAddressMapD0Ev:
.LFB8919:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal14CodeAddressMapE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%r12, %rsi
	movq	%rax, (%rdi)
	movq	8(%rdi), %rax
	movq	41016(%rax), %rdi
	call	_ZN2v88internal6Logger23RemoveCodeEventListenerEPNS0_17CodeEventListenerE@PLT
	movl	32(%r12), %edx
	movq	24(%r12), %rdi
	leaq	(%rdx,%rdx,2), %rax
	leaq	(%rdi,%rax,8), %rcx
	cmpq	%rcx, %rdi
	jnb	.L55
	movq	%rdi, %rax
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L68:
	cmpq	%rcx, %rax
	jnb	.L55
.L57:
	movq	(%rax), %rsi
	movq	%rax, %rbx
	addq	$24, %rax
	testq	%rsi, %rsi
	je	.L68
	.p2align 4,,10
	.p2align 3
.L56:
	movq	8(%rbx), %r8
	testq	%r8, %r8
	je	.L58
	movq	%r8, %rdi
	call	_ZdaPv@PLT
	movq	24(%r12), %rdi
	movl	32(%r12), %edx
.L58:
	leaq	(%rdx,%rdx,2), %rax
	leaq	(%rdi,%rax,8), %rcx
	leaq	24(%rbx), %rax
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L69:
	movq	(%rax), %rsi
	movq	%rax, %rbx
	addq	$24, %rax
	testq	%rsi, %rsi
	jne	.L56
.L67:
	cmpq	%rax, %rcx
	ja	.L69
.L55:
	call	free@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal15CodeEventLoggerD2Ev@PLT
	popq	%rbx
	movq	%r12, %rdi
	movl	$48, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8919:
	.size	_ZN2v88internal14CodeAddressMapD0Ev, .-_ZN2v88internal14CodeAddressMapD0Ev
	.section	.text._ZN2v88internal13ObjectVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_,"axG",@progbits,_ZN2v88internal13ObjectVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ObjectVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	.type	_ZN2v88internal13ObjectVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_, @function
_ZN2v88internal13ObjectVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_:
.LFB7691:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	leaq	_ZN2v88internal10Serializer16ObjectSerializer13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES4_(%rip), %r9
	movq	16(%rax), %r8
	cmpq	%r9, %r8
	jne	.L71
	jmp	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L71:
	jmp	*%r8
	.cfi_endproc
.LFE7691:
	.size	_ZN2v88internal13ObjectVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_, .-_ZN2v88internal13ObjectVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	.section	.text._ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE,"axG",@progbits,_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE
	.type	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE, @function
_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE:
.LFB7692:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	leaq	_ZN2v88internal10Serializer16ObjectSerializer13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES4_(%rip), %r9
	leaq	8(%rdx), %rcx
	movq	16(%rax), %r8
	cmpq	%r9, %r8
	jne	.L73
	jmp	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L73:
	jmp	*%r8
	.cfi_endproc
.LFE7692:
	.size	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE, .-_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE
	.section	.text._ZN2v88internal13ObjectVisitor22VisitCustomWeakPointerENS0_10HeapObjectENS0_14FullObjectSlotE,"axG",@progbits,_ZN2v88internal13ObjectVisitor22VisitCustomWeakPointerENS0_10HeapObjectENS0_14FullObjectSlotE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ObjectVisitor22VisitCustomWeakPointerENS0_10HeapObjectENS0_14FullObjectSlotE
	.type	_ZN2v88internal13ObjectVisitor22VisitCustomWeakPointerENS0_10HeapObjectENS0_14FullObjectSlotE, @function
_ZN2v88internal13ObjectVisitor22VisitCustomWeakPointerENS0_10HeapObjectENS0_14FullObjectSlotE:
.LFB7694:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	leaq	_ZN2v88internal13ObjectVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_(%rip), %r9
	leaq	8(%rdx), %rcx
	movq	32(%rax), %r8
	cmpq	%r9, %r8
	jne	.L75
	movq	16(%rax), %r8
	leaq	_ZN2v88internal10Serializer16ObjectSerializer13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES4_(%rip), %r9
	cmpq	%r9, %r8
	jne	.L75
	jmp	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L75:
	jmp	*%r8
	.cfi_endproc
.LFE7694:
	.size	_ZN2v88internal13ObjectVisitor22VisitCustomWeakPointerENS0_10HeapObjectENS0_14FullObjectSlotE, .-_ZN2v88internal13ObjectVisitor22VisitCustomWeakPointerENS0_10HeapObjectENS0_14FullObjectSlotE
	.section	.text._ZN2v88internal13ObjectVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_,"axG",@progbits,_ZN2v88internal13ObjectVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ObjectVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_
	.type	_ZN2v88internal13ObjectVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_, @function
_ZN2v88internal13ObjectVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_:
.LFB7695:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rdi), %rax
	leaq	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE(%rip), %rbx
	movq	40(%rax), %rcx
	cmpq	%rbx, %rcx
	jne	.L79
	movq	16(%rax), %r8
	leaq	_ZN2v88internal10Serializer16ObjectSerializer13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES4_(%rip), %rsi
	leaq	8(%rdx), %rcx
	cmpq	%rsi, %r8
	movq	%r13, %rsi
	jne	.L80
	call	*24(%rax)
.L82:
	movq	(%r12), %rax
	movq	40(%rax), %rcx
	cmpq	%rbx, %rcx
	jne	.L83
.L87:
	movq	16(%rax), %r8
	leaq	_ZN2v88internal10Serializer16ObjectSerializer13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES4_(%rip), %rdx
	leaq	8(%r14), %rcx
	cmpq	%rdx, %r8
	jne	.L84
	popq	%rbx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	24(%rax), %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore_state
	call	*%rcx
	movq	(%r12), %rax
	movq	40(%rax), %rcx
	cmpq	%rbx, %rcx
	je	.L87
.L83:
	popq	%rbx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rcx
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore_state
	popq	%rbx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%r8
	.p2align 4,,10
	.p2align 3
.L80:
	.cfi_restore_state
	call	*%r8
	jmp	.L82
	.cfi_endproc
.LFE7695:
	.size	_ZN2v88internal13ObjectVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_, .-_ZN2v88internal13ObjectVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_
	.section	.rodata._ZN2v88internal10SerializerC2EPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"Out of memory: HashMap::Initialize"
	.section	.text._ZN2v88internal10SerializerC2EPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10SerializerC2EPNS0_7IsolateE
	.type	_ZN2v88internal10SerializerC2EPNS0_7IsolateE, @function
_ZN2v88internal10SerializerC2EPNS0_7IsolateE:
.LFB21291:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal10SerializerE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movl	$0, 72(%rdi)
	movq	%rax, (%rdi)
	movq	$0, 96(%rdi)
	movq	%rsi, 104(%rdi)
	movups	%xmm0, 8(%rdi)
	movups	%xmm0, 24(%rdi)
	movups	%xmm0, 40(%rdi)
	movups	%xmm0, 56(%rdi)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 80(%rdi)
	movl	$192, %edi
	call	malloc@PLT
	movq	%rax, 112(%r12)
	testq	%rax, %rax
	je	.L93
	movl	$8, 120(%r12)
	movl	$24, %edx
	movb	$0, 20(%rax)
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L90:
	movq	112(%r12), %rcx
	addq	$1, %rax
	movb	$0, 20(%rcx,%rdx)
	movl	120(%r12), %ecx
	addq	$24, %rdx
	cmpq	%rax, %rcx
	ja	.L90
	movl	$0, 124(%r12)
	leaq	136(%r12), %rdi
	movq	%r13, %rsi
	movl	$0, 132(%r12)
	call	_ZN2v88internal24ExternalReferenceEncoderC1EPNS0_7IsolateE@PLT
	leaq	144(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal12RootIndexMapC1EPNS0_7IsolateE@PLT
	pxor	%xmm0, %xmm0
	leaq	216(%r12), %rdi
	movq	%r12, %rsi
	movq	$0, 200(%r12)
	movl	$0, 208(%r12)
	movups	%xmm0, 152(%r12)
	movups	%xmm0, 168(%r12)
	movups	%xmm0, 184(%r12)
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal19SerializerAllocatorC1EPNS0_10SerializerE@PLT
.L93:
	.cfi_restore_state
	leaq	.LC3(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21291:
	.size	_ZN2v88internal10SerializerC2EPNS0_7IsolateE, .-_ZN2v88internal10SerializerC2EPNS0_7IsolateE
	.globl	_ZN2v88internal10SerializerC1EPNS0_7IsolateE
	.set	_ZN2v88internal10SerializerC1EPNS0_7IsolateE,_ZN2v88internal10SerializerC2EPNS0_7IsolateE
	.section	.text._ZN2v88internal10SerializerD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10SerializerD2Ev
	.type	_ZN2v88internal10SerializerD2Ev, @function
_ZN2v88internal10SerializerD2Ev:
.LFB21294:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal10SerializerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	152(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L95
	movq	0(%r13), %rax
	leaq	_ZN2v88internal14CodeAddressMapD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L96
	leaq	16+_ZTVN2v88internal14CodeAddressMapE(%rip), %rax
	movq	%r13, %rsi
	movq	%rax, 0(%r13)
	movq	8(%r13), %rax
	movq	41016(%rax), %rdi
	call	_ZN2v88internal6Logger23RemoveCodeEventListenerEPNS0_17CodeEventListenerE@PLT
	movl	32(%r13), %ecx
	movq	24(%r13), %r8
	leaq	(%rcx,%rcx,2), %rax
	leaq	(%r8,%rax,8), %rsi
	cmpq	%rsi, %r8
	jnb	.L97
	movq	%r8, %rax
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L126:
	cmpq	%rax, %rsi
	jbe	.L97
.L99:
	movq	(%rax), %rdx
	movq	%rax, %rbx
	addq	$24, %rax
	testq	%rdx, %rdx
	je	.L126
	.p2align 4,,10
	.p2align 3
.L98:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L100
	call	_ZdaPv@PLT
	movq	24(%r13), %r8
	movl	32(%r13), %ecx
.L100:
	leaq	(%rcx,%rcx,2), %rax
	leaq	(%r8,%rax,8), %rdx
	leaq	24(%rbx), %rax
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L127:
	movq	(%rax), %rsi
	movq	%rax, %rbx
	addq	$24, %rax
	testq	%rsi, %rsi
	jne	.L98
.L125:
	cmpq	%rax, %rdx
	ja	.L127
.L97:
	movq	%r8, %rdi
	call	free@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal15CodeEventLoggerD2Ev@PLT
	movl	$48, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L95:
	leaq	304(%r12), %rbx
	leaq	208(%r12), %r13
.L105:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L102
	call	_ZdlPv@PLT
	subq	$24, %rbx
	cmpq	%rbx, %r13
	jne	.L105
.L103:
	movq	184(%r12), %rdi
	testq	%rdi, %rdi
	je	.L106
	call	_ZdlPv@PLT
.L106:
	movq	160(%r12), %rdi
	testq	%rdi, %rdi
	je	.L107
	call	_ZdlPv@PLT
.L107:
	leaq	136(%r12), %rdi
	call	_ZN2v88internal24ExternalReferenceEncoderD1Ev@PLT
	movq	112(%r12), %rdi
	call	free@PLT
	movq	80(%r12), %rdi
	testq	%rdi, %rdi
	je	.L94
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L102:
	.cfi_restore_state
	subq	$24, %rbx
	cmpq	%rbx, %r13
	jne	.L105
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L94:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L96:
	.cfi_restore_state
	movq	%r13, %rdi
	call	*%rax
	jmp	.L95
	.cfi_endproc
.LFE21294:
	.size	_ZN2v88internal10SerializerD2Ev, .-_ZN2v88internal10SerializerD2Ev
	.globl	_ZN2v88internal10SerializerD1Ev
	.set	_ZN2v88internal10SerializerD1Ev,_ZN2v88internal10SerializerD2Ev
	.section	.text._ZN2v88internal10SerializerD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10SerializerD0Ev
	.type	_ZN2v88internal10SerializerD0Ev, @function
_ZN2v88internal10SerializerD0Ev:
.LFB21296:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal10SerializerD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$360, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE21296:
	.size	_ZN2v88internal10SerializerD0Ev, .-_ZN2v88internal10SerializerD0Ev
	.section	.rodata._ZN2v88internal10Serializer16OutputStatisticsEPKc.str1.1,"aMS",@progbits,1
.LC4:
	.string	"%s:\n"
	.section	.text._ZN2v88internal10Serializer16OutputStatisticsEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10Serializer16OutputStatisticsEPKc
	.type	_ZN2v88internal10Serializer16OutputStatisticsEPKc, @function
_ZN2v88internal10Serializer16OutputStatisticsEPKc:
.LFB21297:
	.cfi_startproc
	endbr64
	cmpb	$0, _ZN2v88internal29FLAG_serialization_statisticsE(%rip)
	jne	.L135
	ret
	.p2align 4,,10
	.p2align 3
.L135:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	.LC4(%rip), %rdi
	subq	$8, %rsp
	call	_ZN2v88internal6PrintFEPKcz@PLT
	addq	$8, %rsp
	leaq	216(%rbx), %rdi
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal19SerializerAllocator16OutputStatisticsEv@PLT
	.cfi_endproc
.LFE21297:
	.size	_ZN2v88internal10Serializer16OutputStatisticsEPKc, .-_ZN2v88internal10Serializer16OutputStatisticsEPKc
	.section	.text._ZNK2v88internal10Serializer23ObjectIsBytecodeHandlerENS0_10HeapObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal10Serializer23ObjectIsBytecodeHandlerENS0_10HeapObjectE
	.type	_ZNK2v88internal10Serializer23ObjectIsBytecodeHandlerENS0_10HeapObjectE, @function
_ZNK2v88internal10Serializer23ObjectIsBytecodeHandlerENS0_10HeapObjectE:
.LFB21305:
	.cfi_startproc
	endbr64
	movq	-1(%rsi), %rdx
	xorl	%eax, %eax
	cmpw	$69, 11(%rdx)
	jne	.L136
	movl	43(%rsi), %eax
	shrl	%eax
	andl	$31, %eax
	cmpl	$1, %eax
	sete	%al
.L136:
	ret
	.cfi_endproc
.LFE21305:
	.size	_ZNK2v88internal10Serializer23ObjectIsBytecodeHandlerENS0_10HeapObjectE, .-_ZNK2v88internal10Serializer23ObjectIsBytecodeHandlerENS0_10HeapObjectE
	.section	.rodata._ZN2v88internal10Serializer16PutBackReferenceENS0_10HeapObjectENS0_19SerializerReferenceE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"BackRefMapIndex"
.LC6:
	.string	"BackRefLargeObjectIndex"
.LC7:
	.string	"BackRefChunkIndex"
.LC8:
	.string	"BackRefChunkOffset"
	.section	.text._ZN2v88internal10Serializer16PutBackReferenceENS0_10HeapObjectENS0_19SerializerReferenceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10Serializer16PutBackReferenceENS0_10HeapObjectENS0_19SerializerReferenceE
	.type	_ZN2v88internal10Serializer16PutBackReferenceENS0_10HeapObjectENS0_19SerializerReferenceE, @function
_ZN2v88internal10Serializer16PutBackReferenceENS0_10HeapObjectENS0_19SerializerReferenceE:
.LFB21308:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %eax
	andl	$15, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	shrq	$32, %r14
	.cfi_offset 13, -32
	leaq	80(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	cmpl	$4, %eax
	je	.L140
	cmpl	$5, %eax
	je	.L141
	shrl	$4, %edx
	movq	%r13, %rdi
	movl	%edx, %esi
	leaq	.LC7(%rip), %rdx
	call	_ZN2v88internal16SnapshotByteSink6PutIntEmPKc@PLT
	leaq	.LC8(%rip), %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal16SnapshotByteSink6PutIntEmPKc@PLT
.L143:
	movslq	72(%rbx), %rdx
	movq	%rdx, %rax
	movq	%r12, 8(%rbx,%rdx,8)
	addl	$1, %eax
	andl	$7, %eax
	movl	%eax, 72(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L140:
	.cfi_restore_state
	leaq	.LC5(%rip), %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal16SnapshotByteSink6PutIntEmPKc@PLT
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L141:
	leaq	.LC6(%rip), %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal16SnapshotByteSink6PutIntEmPKc@PLT
	jmp	.L143
	.cfi_endproc
.LFE21308:
	.size	_ZN2v88internal10Serializer16PutBackReferenceENS0_10HeapObjectENS0_19SerializerReferenceE, .-_ZN2v88internal10Serializer16PutBackReferenceENS0_10HeapObjectENS0_19SerializerReferenceE
	.section	.text._ZN2v88internal10Serializer18PutAlignmentPrefixENS0_10HeapObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10Serializer18PutAlignmentPrefixENS0_10HeapObjectE
	.type	_ZN2v88internal10Serializer18PutAlignmentPrefixENS0_10HeapObjectE, @function
_ZN2v88internal10Serializer18PutAlignmentPrefixENS0_10HeapObjectE:
.LFB21310:
	.cfi_startproc
	endbr64
	movq	-1(%rsi), %rax
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE21310:
	.size	_ZN2v88internal10Serializer18PutAlignmentPrefixENS0_10HeapObjectE, .-_ZN2v88internal10Serializer18PutAlignmentPrefixENS0_10HeapObjectE
	.section	.text._ZN2v88internal10Serializer24InitializeCodeAddressMapEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10Serializer24InitializeCodeAddressMapEv
	.type	_ZN2v88internal10Serializer24InitializeCodeAddressMapEv, @function
_ZN2v88internal10Serializer24InitializeCodeAddressMapEv:
.LFB21314:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	104(%rdi), %rdi
	call	_ZN2v88internal7Isolate28InitializeLoggingAndCountersEv@PLT
	movl	$48, %edi
	call	_Znwm@PLT
	movq	104(%r12), %r13
	movq	%rax, %rdi
	movq	%rax, %rbx
	movq	%r13, %rsi
	call	_ZN2v88internal15CodeEventLoggerC2EPNS0_7IsolateE@PLT
	leaq	16+_ZTVN2v88internal14CodeAddressMapE(%rip), %rax
	movl	$192, %edi
	movq	%rax, (%rbx)
	call	malloc@PLT
	movq	%rax, 24(%rbx)
	testq	%rax, %rax
	je	.L152
	movl	$8, 32(%rbx)
	movl	$24, %edx
	movq	$0, (%rax)
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L149:
	movq	24(%rbx), %rcx
	addq	$1, %rax
	movq	$0, (%rcx,%rdx)
	movl	32(%rbx), %ecx
	addq	$24, %rdx
	cmpq	%rax, %rcx
	ja	.L149
	movl	$0, 36(%rbx)
	movq	41016(%r13), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal6Logger20AddCodeEventListenerEPNS0_17CodeEventListenerE@PLT
	movq	%rbx, 152(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L152:
	.cfi_restore_state
	leaq	.LC3(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21314:
	.size	_ZN2v88internal10Serializer24InitializeCodeAddressMapEv, .-_ZN2v88internal10Serializer24InitializeCodeAddressMapEv
	.section	.rodata._ZN2v88internal10Serializer8CopyCodeENS0_4CodeE.str1.1,"aMS",@progbits,1
.LC9:
	.string	"vector::_M_range_insert"
	.section	.text._ZN2v88internal10Serializer8CopyCodeENS0_4CodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10Serializer8CopyCodeENS0_4CodeE
	.type	_ZN2v88internal10Serializer8CopyCodeENS0_4CodeE, @function
_ZN2v88internal10Serializer8CopyCodeENS0_4CodeE:
.LFB21315:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	160(%rdi), %r13
	movq	%r13, %r8
	cmpq	168(%rdi), %r13
	je	.L154
	movq	%r13, 168(%rdi)
.L154:
	movl	39(%rsi), %eax
	testb	$1, 43(%rsi)
	je	.L155
	addl	$71, %eax
	andl	$-8, %eax
	cltq
	addq	%rsi, %rax
	movslq	-1(%rax), %rdx
	leaq	7(%rax,%rdx), %rax
	leaq	63(%rsi), %rdx
	subl	%edx, %eax
.L155:
	leal	7(%rax), %r12d
	leaq	-1(%rsi), %r9
	andl	$-8, %r12d
	leaq	1(%r13), %r15
	addl	$95, %r12d
	andl	$-32, %r12d
	movslq	%r12d, %r12
	addq	%r9, %r12
	cmpq	%r9, %r12
	je	.L181
	movq	176(%rbx), %rax
	subq	%r9, %r12
	subq	%r13, %rax
	cmpq	%rax, %r12
	ja	.L158
	movq	%r12, %rdx
	movq	%r9, %rsi
	movq	%r13, %rdi
	call	memmove@PLT
	addq	%r12, 168(%rbx)
	movq	160(%rbx), %r10
	leaq	1(%r10), %r15
.L181:
	addq	$56, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L158:
	.cfi_restore_state
	xorl	%edx, %edx
	testq	%r12, %r12
	js	.L183
	movq	%rdx, %r15
	addq	%r12, %r15
	testq	%r15, %r15
	js	.L171
	jne	.L162
	movq	$0, -56(%rbp)
	movl	$1, %r15d
	xorl	%r14d, %r14d
.L168:
	leaq	(%r14,%rdx), %r10
	leaq	(%r10,%r12), %r11
	testq	%rdx, %rdx
	jne	.L184
	movq	%r12, %rdx
	movq	%r9, %rsi
	movq	%r10, %rdi
	movq	%r11, -72(%rbp)
	movq	%r8, -64(%rbp)
	call	memcpy@PLT
	movq	168(%rbx), %rdx
	movq	-64(%rbp), %r8
	movq	-72(%rbp), %r11
	subq	%r8, %rdx
	leaq	(%r11,%rdx), %r12
	jne	.L164
.L166:
	testq	%r13, %r13
	jne	.L165
.L167:
	movq	-56(%rbp), %rax
	movq	%r14, %xmm0
	movq	%r12, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, 176(%rbx)
	movups	%xmm0, 160(%rbx)
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L171:
	movabsq	$9223372036854775807, %r15
.L162:
	movq	%r15, %rdi
	movq	%r8, -56(%rbp)
	movq	%r9, -64(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r8
	movq	160(%rbx), %r13
	movq	%rax, %r14
	leaq	(%rax,%r15), %rax
	movq	-64(%rbp), %r9
	movq	%r8, %rdx
	movq	%rax, -56(%rbp)
	leaq	1(%r14), %r15
	subq	%r13, %rdx
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L184:
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%r11, -88(%rbp)
	movq	%r8, -72(%rbp)
	movq	%r10, -80(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	movq	-64(%rbp), %r9
	movq	-80(%rbp), %r10
	movq	%r12, %rdx
	movq	%r9, %rsi
	movq	%r10, %rdi
	call	memcpy@PLT
	movq	168(%rbx), %rdx
	movq	-72(%rbp), %r8
	movq	-88(%rbp), %r11
	subq	%r8, %rdx
	leaq	(%r11,%rdx), %r12
	jne	.L164
.L165:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L164:
	movq	%r8, %rsi
	movq	%r11, %rdi
	call	memcpy@PLT
	jmp	.L166
.L183:
	leaq	.LC9(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE21315:
	.size	_ZN2v88internal10Serializer8CopyCodeENS0_4CodeE, .-_ZN2v88internal10Serializer8CopyCodeENS0_4CodeE
	.section	.text._ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS0_23DefaultAllocationPolicyEE6RemoveERKS2_j,"axG",@progbits,_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS0_23DefaultAllocationPolicyEE6RemoveERKS2_j,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS0_23DefaultAllocationPolicyEE6RemoveERKS2_j
	.type	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS0_23DefaultAllocationPolicyEE6RemoveERKS2_j, @function
_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS0_23DefaultAllocationPolicyEE6RemoveERKS2_j:
.LFB22058:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %r10d
	movq	%rsi, %r9
	movq	(%rdi), %rcx
	leal	-1(%r10), %esi
	andl	%esi, %edx
	leaq	(%rdx,%rdx,2), %rax
	leaq	(%rcx,%rax,8), %rax
	movq	(%rax), %r8
	testq	%r8, %r8
	je	.L185
	movq	(%r9), %r9
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L205:
	addq	$1, %rdx
	andq	%rsi, %rdx
	leaq	(%rdx,%rdx,2), %rax
	leaq	(%rcx,%rax,8), %rax
	movq	(%rax), %r8
	testq	%r8, %r8
	je	.L185
.L188:
	cmpq	%r8, %r9
	jne	.L205
	movq	8(%rax), %r8
	movq	%rax, %rsi
	movl	%r10d, %r9d
	.p2align 4,,10
	.p2align 3
.L194:
	leaq	(%r9,%r9,2), %rdx
	addq	$24, %rax
	leaq	(%rcx,%rdx,8), %rdx
	cmpq	%rdx, %rax
	cmove	%rcx, %rax
	cmpq	$0, (%rax)
	je	.L190
	leal	-1(%r10), %edx
	andl	16(%rax), %edx
	leaq	(%rdx,%rdx,2), %rdx
	leaq	(%rcx,%rdx,8), %rdx
	cmpq	%rax, %rsi
	jnb	.L191
	cmpq	%rdx, %rsi
	jnb	.L192
	cmpq	%rdx, %rax
	jnb	.L194
.L192:
	movdqu	(%rax), %xmm0
	movups	%xmm0, (%rsi)
	movl	16(%rax), %edx
	movl	%edx, 16(%rsi)
	movl	8(%rdi), %r9d
	movq	%rax, %rsi
	movq	(%rdi), %rcx
	movq	%r9, %r10
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L191:
	jbe	.L194
	cmpq	%rdx, %rsi
	jb	.L194
	cmpq	%rdx, %rax
	jb	.L192
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L190:
	movq	$0, (%rsi)
	subl	$1, 12(%rdi)
.L185:
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE22058:
	.size	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS0_23DefaultAllocationPolicyEE6RemoveERKS2_j, .-_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS0_23DefaultAllocationPolicyEE6RemoveERKS2_j
	.section	.rodata._ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_.str1.1,"aMS",@progbits,1
.LC10:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_,"axG",@progbits,_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	.type	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_, @function
_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_:
.LFB23869:
	.cfi_startproc
	endbr64
	movabsq	$9223372036854775807, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rsi
	movq	(%rdi), %r15
	movq	%rsi, %rax
	subq	%r15, %rax
	cmpq	%rcx, %rax
	je	.L220
	movq	%rdx, %r13
	movq	%r8, %rdx
	movq	%rdi, %r12
	subq	%r15, %rdx
	testq	%rax, %rax
	je	.L215
	leaq	(%rax,%rax), %r14
	cmpq	%r14, %rax
	jbe	.L221
.L217:
	movq	%rcx, %r14
.L208:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L214:
	movzbl	0(%r13), %eax
	subq	%r8, %rsi
	leaq	1(%rbx,%rdx), %r10
	movq	%rsi, %r13
	movb	%al, (%rbx,%rdx)
	leaq	(%r10,%rsi), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L222
	testq	%rsi, %rsi
	jg	.L210
	testq	%r15, %r15
	jne	.L213
.L211:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L222:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r10, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r10
	movq	-72(%rbp), %r8
	jg	.L210
.L213:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L221:
	testq	%r14, %r14
	js	.L217
	jne	.L208
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L210:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r10, %rdi
	call	memcpy@PLT
	testq	%r15, %r15
	je	.L211
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L215:
	movl	$1, %r14d
	jmp	.L208
.L220:
	leaq	.LC10(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE23869:
	.size	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_, .-_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	.section	.text._ZN2v88internal10Serializer17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10Serializer17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_
	.type	_ZN2v88internal10Serializer17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_, @function
_ZN2v88internal10Serializer17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_:
.LFB21300:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%r8, %rcx
	jnb	.L223
	movq	%rdi, %rbx
	movq	%r8, %r14
	leaq	80(%rdi), %r15
	movq	%rcx, %r13
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L225:
	movq	(%rbx), %rax
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	*40(%rax)
.L244:
	addq	$8, %r13
	cmpq	%r13, %r14
	jbe	.L223
.L224:
	movq	0(%r13), %r12
	testb	$1, %r12b
	jne	.L225
	movb	$96, -57(%rbp)
	movq	88(%rbx), %rsi
	cmpq	96(%rbx), %rsi
	je	.L226
	movb	$96, (%rsi)
	movq	88(%rbx), %rax
	movb	%r12b, -57(%rbp)
	leaq	1(%rax), %rsi
	movq	%rsi, 88(%rbx)
	cmpq	%rsi, 96(%rbx)
	je	.L250
.L228:
	movb	%r12b, (%rsi)
	movq	88(%rbx), %rax
	leaq	1(%rax), %rsi
	movq	%r12, %rax
	movzbl	%ah, %eax
	movq	%rsi, 88(%rbx)
	movb	%al, -57(%rbp)
	cmpq	96(%rbx), %rsi
	je	.L251
.L230:
	movb	%al, (%rsi)
	movq	88(%rbx), %rax
	leaq	1(%rax), %rsi
	movq	%r12, %rax
	shrq	$16, %rax
	movq	%rsi, 88(%rbx)
	movb	%al, -57(%rbp)
	cmpq	96(%rbx), %rsi
	je	.L252
.L232:
	movb	%al, (%rsi)
	movq	88(%rbx), %rax
	leaq	1(%rax), %rsi
	movq	%r12, %rax
	shrq	$24, %rax
	movq	%rsi, 88(%rbx)
	movb	%al, -57(%rbp)
	cmpq	%rsi, 96(%rbx)
	je	.L253
.L234:
	movb	%al, (%rsi)
	movq	88(%rbx), %rax
	leaq	1(%rax), %rsi
	movq	%r12, %rax
	shrq	$32, %rax
	movq	%rsi, 88(%rbx)
	movb	%al, -57(%rbp)
	cmpq	96(%rbx), %rsi
	je	.L254
.L236:
	movb	%al, (%rsi)
	movq	88(%rbx), %rax
	leaq	1(%rax), %rsi
	movq	%r12, %rax
	shrq	$40, %rax
	movq	%rsi, 88(%rbx)
	movb	%al, -57(%rbp)
	cmpq	96(%rbx), %rsi
	je	.L255
.L238:
	movb	%al, (%rsi)
	movq	88(%rbx), %rax
	leaq	1(%rax), %rsi
	movq	%r12, %rax
	shrq	$48, %rax
	movq	%rsi, 88(%rbx)
	movb	%al, -57(%rbp)
	cmpq	96(%rbx), %rsi
	je	.L256
.L240:
	movb	%al, (%rsi)
	movq	88(%rbx), %rax
	shrq	$56, %r12
	movb	%r12b, -57(%rbp)
	leaq	1(%rax), %rsi
	movq	%rsi, 88(%rbx)
	cmpq	96(%rbx), %rsi
	je	.L257
.L242:
	addq	$8, %r13
	movb	%r12b, (%rsi)
	addq	$1, 88(%rbx)
	cmpq	%r13, %r14
	ja	.L224
.L223:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L258
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L226:
	.cfi_restore_state
	leaq	-57(%rbp), %rdx
	movq	%r15, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movb	%r12b, -57(%rbp)
	movq	88(%rbx), %rsi
	cmpq	%rsi, 96(%rbx)
	jne	.L228
	.p2align 4,,10
	.p2align 3
.L250:
	leaq	-57(%rbp), %rdx
	movq	%r15, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movq	%r12, %rax
	movq	88(%rbx), %rsi
	movzbl	%ah, %eax
	movb	%al, -57(%rbp)
	cmpq	96(%rbx), %rsi
	jne	.L230
	.p2align 4,,10
	.p2align 3
.L251:
	leaq	-57(%rbp), %rdx
	movq	%r15, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movq	%r12, %rax
	movq	88(%rbx), %rsi
	shrq	$16, %rax
	movb	%al, -57(%rbp)
	cmpq	96(%rbx), %rsi
	jne	.L232
	.p2align 4,,10
	.p2align 3
.L252:
	leaq	-57(%rbp), %rdx
	movq	%r15, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movq	%r12, %rax
	movq	88(%rbx), %rsi
	shrq	$24, %rax
	movb	%al, -57(%rbp)
	cmpq	%rsi, 96(%rbx)
	jne	.L234
	.p2align 4,,10
	.p2align 3
.L253:
	leaq	-57(%rbp), %rdx
	movq	%r15, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movq	%r12, %rax
	movq	88(%rbx), %rsi
	shrq	$32, %rax
	movb	%al, -57(%rbp)
	cmpq	96(%rbx), %rsi
	jne	.L236
	.p2align 4,,10
	.p2align 3
.L254:
	leaq	-57(%rbp), %rdx
	movq	%r15, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movq	%r12, %rax
	movq	88(%rbx), %rsi
	shrq	$40, %rax
	movb	%al, -57(%rbp)
	cmpq	96(%rbx), %rsi
	jne	.L238
	.p2align 4,,10
	.p2align 3
.L255:
	leaq	-57(%rbp), %rdx
	movq	%r15, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movq	%r12, %rax
	movq	88(%rbx), %rsi
	shrq	$48, %rax
	movb	%al, -57(%rbp)
	cmpq	96(%rbx), %rsi
	jne	.L240
	.p2align 4,,10
	.p2align 3
.L256:
	leaq	-57(%rbp), %rdx
	movq	%r15, %rdi
	shrq	$56, %r12
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movb	%r12b, -57(%rbp)
	movq	88(%rbx), %rsi
	cmpq	96(%rbx), %rsi
	jne	.L242
	.p2align 4,,10
	.p2align 3
.L257:
	leaq	-57(%rbp), %rdx
	movq	%r15, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	jmp	.L244
.L258:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21300:
	.size	_ZN2v88internal10Serializer17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_, .-_ZN2v88internal10Serializer17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_
	.section	.text._ZN2v88internal10Serializer3PadEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10Serializer3PadEi
	.type	_ZN2v88internal10Serializer3PadEi, @function
_ZN2v88internal10Serializer3PadEi:
.LFB21313:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	80(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	88(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movb	$20, -41(%rbp)
	cmpq	%rsi, 96(%rdi)
	je	.L277
	movb	$20, (%rsi)
	movq	88(%rdi), %rax
	movb	$20, -41(%rbp)
	leaq	1(%rax), %rsi
	movq	%rsi, 88(%rdi)
	cmpq	%rsi, 96(%rbx)
	je	.L278
.L262:
	movb	$20, (%rsi)
	movq	88(%rbx), %rax
	movb	$20, -41(%rbp)
	leaq	1(%rax), %rsi
	movq	%rsi, 88(%rbx)
	cmpq	%rsi, 96(%rbx)
	je	.L264
.L281:
	movb	$20, (%rsi)
	movq	88(%rbx), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 88(%rbx)
.L265:
	movq	%rsi, %rax
	subq	80(%rbx), %rax
	leaq	-41(%rbp), %r14
	addl	%r12d, %eax
	testb	$7, %al
	jne	.L266
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L279:
	movb	$20, (%rsi)
	movq	88(%rbx), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, %rax
	subq	80(%rbx), %rax
	movq	%rsi, 88(%rbx)
	addl	%r12d, %eax
	testb	$7, %al
	je	.L259
.L266:
	movb	$20, -41(%rbp)
	cmpq	96(%rbx), %rsi
	jne	.L279
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movq	88(%rbx), %rsi
	movq	%rsi, %rax
	subq	80(%rbx), %rax
	addl	%r12d, %eax
	testb	$7, %al
	jne	.L266
	.p2align 4,,10
	.p2align 3
.L259:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L280
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L277:
	.cfi_restore_state
	leaq	-41(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movb	$20, -41(%rbp)
	movq	88(%rbx), %rsi
	cmpq	%rsi, 96(%rbx)
	jne	.L262
	.p2align 4,,10
	.p2align 3
.L278:
	leaq	-41(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movb	$20, -41(%rbp)
	movq	88(%rbx), %rsi
	cmpq	%rsi, 96(%rbx)
	jne	.L281
	.p2align 4,,10
	.p2align 3
.L264:
	leaq	-41(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movq	88(%rbx), %rsi
	jmp	.L265
.L280:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21313:
	.size	_ZN2v88internal10Serializer3PadEi, .-_ZN2v88internal10Serializer3PadEi
	.section	.text._ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE,"axG",@progbits,_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE
	.type	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE, @function
_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE:
.LFB7689:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	8(%rcx), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	leaq	_ZN2v88internal10Serializer17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_(%rip), %rdi
	movq	16(%rax), %r9
	cmpq	%rdi, %r9
	jne	.L283
	cmpq	%r8, %rcx
	jnb	.L282
	movq	(%rcx), %rbx
	testb	$1, %bl
	je	.L308
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	*40(%rax)
.L282:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L309
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L308:
	.cfi_restore_state
	movb	$96, -41(%rbp)
	movq	88(%r12), %rsi
	leaq	80(%r12), %r13
	cmpq	96(%r12), %rsi
	je	.L286
	movb	$96, (%rsi)
	movq	88(%r12), %rax
	movb	%bl, -41(%rbp)
	leaq	1(%rax), %rsi
	movq	%rsi, 88(%r12)
	cmpq	96(%r12), %rsi
	je	.L310
.L288:
	movb	%bl, (%rsi)
	movq	88(%r12), %rax
	leaq	1(%rax), %rsi
	movzbl	%bh, %eax
	movq	%rsi, 88(%r12)
	movb	%al, -41(%rbp)
	cmpq	%rsi, 96(%r12)
	je	.L311
.L290:
	movb	%al, (%rsi)
	movq	88(%r12), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 88(%r12)
.L291:
	movq	%rbx, %rax
	shrq	$16, %rax
	movb	%al, -41(%rbp)
	cmpq	%rsi, 96(%r12)
	je	.L312
	movb	%al, (%rsi)
	movq	88(%r12), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 88(%r12)
.L293:
	movq	%rbx, %rax
	shrq	$24, %rax
	movb	%al, -41(%rbp)
	cmpq	%rsi, 96(%r12)
	je	.L313
	movb	%al, (%rsi)
	movq	88(%r12), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 88(%r12)
.L295:
	movq	%rbx, %rax
	shrq	$32, %rax
	movb	%al, -41(%rbp)
	cmpq	96(%r12), %rsi
	je	.L314
	movb	%al, (%rsi)
	movq	88(%r12), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 88(%r12)
.L297:
	movq	%rbx, %rax
	shrq	$40, %rax
	movb	%al, -41(%rbp)
	cmpq	96(%r12), %rsi
	je	.L315
	movb	%al, (%rsi)
	movq	88(%r12), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 88(%r12)
.L299:
	movq	%rbx, %rax
	shrq	$48, %rax
	movb	%al, -41(%rbp)
	cmpq	96(%r12), %rsi
	je	.L316
	movb	%al, (%rsi)
	movq	88(%r12), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 88(%r12)
.L301:
	shrq	$56, %rbx
	movb	%bl, -41(%rbp)
	cmpq	96(%r12), %rsi
	je	.L317
	movb	%bl, (%rsi)
	addq	$1, 88(%r12)
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L286:
	leaq	-41(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movb	%bl, -41(%rbp)
	movq	88(%r12), %rsi
	cmpq	96(%r12), %rsi
	jne	.L288
	.p2align 4,,10
	.p2align 3
.L310:
	leaq	-41(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movzbl	%bh, %eax
	movq	88(%r12), %rsi
	movb	%al, -41(%rbp)
	cmpq	%rsi, 96(%r12)
	jne	.L290
	.p2align 4,,10
	.p2align 3
.L311:
	leaq	-41(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movq	88(%r12), %rsi
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L317:
	leaq	-41(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L316:
	leaq	-41(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movq	88(%r12), %rsi
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L315:
	leaq	-41(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movq	88(%r12), %rsi
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L314:
	leaq	-41(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movq	88(%r12), %rsi
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L313:
	leaq	-41(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movq	88(%r12), %rsi
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L312:
	leaq	-41(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movq	88(%r12), %rsi
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L283:
	movq	%r12, %rdi
	call	*%r9
	jmp	.L282
.L309:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7689:
	.size	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE, .-_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE
	.section	.rodata._ZN2v88internal10Serializer20PutAttachedReferenceENS0_19SerializerReferenceE.str1.1,"aMS",@progbits,1
.LC11:
	.string	"AttachedRefIndex"
	.section	.text._ZN2v88internal10Serializer20PutAttachedReferenceENS0_19SerializerReferenceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10Serializer20PutAttachedReferenceENS0_19SerializerReferenceE
	.type	_ZN2v88internal10Serializer20PutAttachedReferenceENS0_19SerializerReferenceE, @function
_ZN2v88internal10Serializer20PutAttachedReferenceENS0_19SerializerReferenceE:
.LFB21309:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	80(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	88(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movb	$18, -25(%rbp)
	cmpq	96(%rdi), %rsi
	je	.L319
	movb	$18, (%rsi)
	addq	$1, 88(%rdi)
.L320:
	shrq	$32, %rbx
	leaq	.LC11(%rip), %rdx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal16SnapshotByteSink6PutIntEmPKc@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L323
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L319:
	.cfi_restore_state
	leaq	-25(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	jmp	.L320
.L323:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21309:
	.size	_ZN2v88internal10Serializer20PutAttachedReferenceENS0_19SerializerReferenceE, .-_ZN2v88internal10Serializer20PutAttachedReferenceENS0_19SerializerReferenceE
	.section	.text._ZN2v88internal10Serializer12PutNextChunkENS0_13SnapshotSpaceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10Serializer12PutNextChunkENS0_13SnapshotSpaceE
	.type	_ZN2v88internal10Serializer12PutNextChunkENS0_13SnapshotSpaceE, @function
_ZN2v88internal10Serializer12PutNextChunkENS0_13SnapshotSpaceE:
.LFB21311:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	80(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	88(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movb	$21, -41(%rbp)
	cmpq	96(%rdi), %rsi
	je	.L325
	movb	$21, (%rsi)
	movq	88(%rdi), %rax
	movb	%r12b, -41(%rbp)
	leaq	1(%rax), %rsi
	movq	%rsi, 88(%rdi)
	cmpq	96(%rbx), %rsi
	je	.L327
.L332:
	movb	%r12b, (%rsi)
	addq	$1, 88(%rbx)
.L324:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L331
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L325:
	.cfi_restore_state
	leaq	-41(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movb	%r12b, -41(%rbp)
	movq	88(%rbx), %rsi
	cmpq	96(%rbx), %rsi
	jne	.L332
	.p2align 4,,10
	.p2align 3
.L327:
	leaq	-41(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	jmp	.L324
.L331:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21311:
	.size	_ZN2v88internal10Serializer12PutNextChunkENS0_13SnapshotSpaceE, .-_ZN2v88internal10Serializer12PutNextChunkENS0_13SnapshotSpaceE
	.section	.text._ZN2v88internal10Serializer6PutSmiENS0_3SmiE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10Serializer6PutSmiENS0_3SmiE
	.type	_ZN2v88internal10Serializer6PutSmiENS0_3SmiE, @function
_ZN2v88internal10Serializer6PutSmiENS0_3SmiE:
.LFB21307:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	88(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movb	$96, -65(%rbp)
	cmpq	96(%rdi), %rsi
	je	.L334
	movb	$96, (%rsi)
	movq	88(%rdi), %rax
	leaq	1(%rax), %rbx
	movq	%rbx, 88(%rdi)
.L335:
	movq	%r12, -64(%rbp)
	leaq	-56(%rbp), %r15
	leaq	-64(%rbp), %r12
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L357:
	movb	%dl, (%rbx)
	movq	88(%r13), %rax
	addq	$1, %r12
	leaq	1(%rax), %rbx
	movq	%rbx, 88(%r13)
	cmpq	%r15, %r12
	je	.L356
.L342:
	movzbl	(%r12), %edx
	cmpq	%rbx, 96(%r13)
	jne	.L357
	movabsq	$9223372036854775807, %rax
	movq	80(%r13), %r10
	subq	%r10, %rbx
	movq	%rbx, %r8
	cmpq	%rax, %rbx
	je	.L358
	movl	$1, %r14d
	testq	%rbx, %rbx
	je	.L339
	leaq	(%rbx,%rbx), %r14
	cmpq	%r14, %rbx
	jbe	.L359
	movabsq	$9223372036854775807, %r14
	jmp	.L339
	.p2align 4,,10
	.p2align 3
.L359:
	movabsq	$9223372036854775807, %rax
	testq	%r14, %r14
	cmovs	%rax, %r14
.L339:
	movq	%r14, %rdi
	movq	%r8, -104(%rbp)
	movq	%r10, -96(%rbp)
	movb	%dl, -88(%rbp)
	call	_Znwm@PLT
	movq	-104(%rbp), %r8
	movzbl	-88(%rbp), %edx
	addq	%rax, %r14
	movq	-96(%rbp), %r10
	movq	%rax, %rcx
	testq	%r8, %r8
	movb	%dl, (%rax,%r8)
	leaq	1(%rax,%r8), %rbx
	jg	.L360
	testq	%r10, %r10
	jne	.L340
.L341:
	movq	%rcx, %xmm0
	movq	%rbx, %xmm1
	addq	$1, %r12
	movq	%r14, 96(%r13)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 80(%r13)
	cmpq	%r15, %r12
	jne	.L342
	.p2align 4,,10
	.p2align 3
.L356:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L361
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L360:
	.cfi_restore_state
	movq	%r10, %rsi
	movq	%rcx, %rdi
	movq	%r8, %rdx
	movq	%r10, -88(%rbp)
	call	memmove@PLT
	movq	-88(%rbp), %r10
	movq	%rax, %rcx
.L340:
	movq	%r10, %rdi
	movq	%rcx, -88(%rbp)
	call	_ZdlPv@PLT
	movq	-88(%rbp), %rcx
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L334:
	leaq	-65(%rbp), %rdx
	leaq	80(%rdi), %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movq	88(%r13), %rbx
	jmp	.L335
.L358:
	leaq	.LC10(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L361:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21307:
	.size	_ZN2v88internal10Serializer6PutSmiENS0_3SmiE, .-_ZN2v88internal10Serializer6PutSmiENS0_3SmiE
	.section	.rodata._ZN2v88internal10Serializer16ObjectSerializer22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE.str1.1,"aMS",@progbits,1
.LC12:
	.string	"internal ref value"
	.section	.text._ZN2v88internal10Serializer16ObjectSerializer22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10Serializer16ObjectSerializer22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE
	.type	_ZN2v88internal10Serializer16ObjectSerializer22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE, @function
_ZN2v88internal10Serializer16ObjectSerializer22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE:
.LFB21337:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	movq	(%rax), %r12
	subq	16(%rdi), %r12
	movb	$34, -25(%rbp)
	movq	24(%rdi), %rdi
	subq	$63, %r12
	movq	8(%rdi), %rsi
	cmpq	16(%rdi), %rsi
	je	.L363
	movb	$34, (%rsi)
	addq	$1, 8(%rdi)
.L364:
	movq	24(%rbx), %rdi
	leaq	.LC12(%rip), %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal16SnapshotByteSink6PutIntEmPKc@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L367
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L363:
	.cfi_restore_state
	leaq	-25(%rbp), %rdx
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	jmp	.L364
.L367:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21337:
	.size	_ZN2v88internal10Serializer16ObjectSerializer22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE, .-_ZN2v88internal10Serializer16ObjectSerializer22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE
	.section	.rodata._ZN2v88internal10Serializer9PutRepeatEi.str1.1,"aMS",@progbits,1
.LC13:
	.string	"repeat count"
	.section	.text._ZN2v88internal10Serializer9PutRepeatEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10Serializer9PutRepeatEi
	.type	_ZN2v88internal10Serializer9PutRepeatEi, @function
_ZN2v88internal10Serializer9PutRepeatEi:
.LFB21312:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	80(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%esi, %ebx
	subq	$16, %rsp
	movq	96(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	88(%rdi), %rax
	cmpl	$17, %ebx
	jg	.L369
	addl	$126, %ebx
	movb	%bl, -25(%rbp)
	cmpq	%rax, %rsi
	je	.L370
	movb	%bl, (%rax)
	addq	$1, 88(%rdi)
	jmp	.L368
	.p2align 4,,10
	.p2align 3
.L369:
	movb	$27, -25(%rbp)
	cmpq	%rax, %rsi
	je	.L373
	movb	$27, (%rax)
	addq	$1, 88(%rdi)
.L374:
	leal	-18(%rbx), %esi
	leaq	.LC13(%rip), %rdx
	movq	%r12, %rdi
	movslq	%esi, %rsi
	call	_ZN2v88internal16SnapshotByteSink6PutIntEmPKc@PLT
.L368:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L377
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L370:
	.cfi_restore_state
	leaq	-25(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	jmp	.L368
	.p2align 4,,10
	.p2align 3
.L373:
	leaq	-25(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	jmp	.L374
.L377:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21312:
	.size	_ZN2v88internal10Serializer9PutRepeatEi, .-_ZN2v88internal10Serializer9PutRepeatEi
	.section	.rodata._ZN2v88internal10Serializer18SerializeHotObjectENS0_10HeapObjectE.str1.1,"aMS",@progbits,1
.LC14:
	.string	" Encoding hot object %d:"
.LC15:
	.string	"\n"
	.section	.text._ZN2v88internal10Serializer18SerializeHotObjectENS0_10HeapObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10Serializer18SerializeHotObjectENS0_10HeapObjectE
	.type	_ZN2v88internal10Serializer18SerializeHotObjectENS0_10HeapObjectE, @function
_ZN2v88internal10Serializer18SerializeHotObjectENS0_10HeapObjectE:
.LFB21303:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	%rsi, -40(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -24(%rbp)
	xorl	%ecx, %ecx
	cmpq	8(%rdi), %rsi
	je	.L386
	cmpq	16(%rdi), %rsi
	je	.L387
	cmpq	24(%rdi), %rsi
	je	.L388
	cmpq	32(%rdi), %rsi
	je	.L389
	cmpq	40(%rdi), %rsi
	je	.L390
	cmpq	48(%rdi), %rsi
	je	.L391
	cmpq	56(%rdi), %rsi
	je	.L392
	cmpq	64(%rdi), %rsi
	je	.L393
	xorl	%eax, %eax
.L378:
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L395
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L386:
	.cfi_restore_state
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L379:
	cmpb	$0, _ZN2v88internal21FLAG_trace_serializerE(%rip)
	jne	.L396
.L384:
	leal	-112(%r12), %eax
	movq	88(%rbx), %rsi
	movb	%al, -25(%rbp)
	cmpq	96(%rbx), %rsi
	je	.L381
	movb	%al, (%rsi)
	addq	$1, 88(%rbx)
.L382:
	movl	$1, %eax
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L396:
	xorl	%eax, %eax
	movl	%r12d, %esi
	leaq	.LC14(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	stdout(%rip), %rsi
	leaq	-40(%rbp), %rdi
	call	_ZNK2v88internal6Object10ShortPrintEP8_IO_FILE@PLT
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L384
	.p2align 4,,10
	.p2align 3
.L381:
	leaq	-25(%rbp), %rdx
	leaq	80(%rbx), %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	jmp	.L382
.L387:
	movl	$1, %r12d
	jmp	.L379
.L388:
	movl	$2, %r12d
	jmp	.L379
.L389:
	movl	$3, %r12d
	jmp	.L379
.L390:
	movl	$4, %r12d
	jmp	.L379
.L391:
	movl	$5, %r12d
	jmp	.L379
.L392:
	movl	$6, %r12d
	jmp	.L379
.L393:
	movl	$7, %r12d
	jmp	.L379
.L395:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21303:
	.size	_ZN2v88internal10Serializer18SerializeHotObjectENS0_10HeapObjectE, .-_ZN2v88internal10Serializer18SerializeHotObjectENS0_10HeapObjectE
	.section	.rodata._ZN2v88internal10Serializer16ObjectSerializer18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE.str1.1,"aMS",@progbits,1
.LC16:
	.string	"kNullAddress != addr"
	.section	.rodata._ZN2v88internal10Serializer16ObjectSerializer18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE.str1.8,"aMS",@progbits,1
	.align 8
.LC17:
	.string	"Builtins::IsIsolateIndependentBuiltin(target)"
	.section	.rodata._ZN2v88internal10Serializer16ObjectSerializer18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE.str1.1
.LC18:
	.string	"builtin index"
	.section	.text._ZN2v88internal10Serializer16ObjectSerializer18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10Serializer16ObjectSerializer18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE
	.type	_ZN2v88internal10Serializer16ObjectSerializer18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE, @function
_ZN2v88internal10Serializer16ObjectSerializer18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE:
.LFB21339:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	je	.L406
	movq	8(%rdi), %rax
	movq	%rdi, %rbx
	movq	%rdx, %r12
	movq	104(%rax), %rdi
	call	_ZN2v88internal17InstructionStream13TryLookupCodeEPNS0_7IsolateEm@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8Builtins27IsIsolateIndependentBuiltinENS0_4CodeE@PLT
	testb	%al, %al
	je	.L407
	movq	24(%rbx), %rdi
	movb	$37, -41(%rbp)
	movq	8(%rdi), %rsi
	cmpq	16(%rdi), %rsi
	je	.L400
	movb	$37, (%rsi)
	addq	$1, 8(%rdi)
.L401:
	movslq	59(%r13), %rsi
	movq	24(%rbx), %rdi
	leaq	.LC18(%rip), %rdx
	call	_ZN2v88internal16SnapshotByteSink6PutIntEmPKc@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal9RelocInfo16IsCodedSpeciallyEv@PLT
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$4, %eax
	addl	$4, %eax
	addl	%eax, 32(%rbx)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L408
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L400:
	.cfi_restore_state
	leaq	-41(%rbp), %rdx
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	jmp	.L401
	.p2align 4,,10
	.p2align 3
.L406:
	leaq	.LC16(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L407:
	leaq	.LC17(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L408:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21339:
	.size	_ZN2v88internal10Serializer16ObjectSerializer18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE, .-_ZN2v88internal10Serializer16ObjectSerializer18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE
	.section	.rodata._ZN2v88internal10Serializer16ObjectSerializer10OutputCodeEi.str1.1,"aMS",@progbits,1
.LC19:
	.string	"length"
.LC20:
	.string	"Code"
	.section	.text._ZN2v88internal10Serializer16ObjectSerializer10OutputCodeEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10Serializer16ObjectSerializer10OutputCodeEi
	.type	_ZN2v88internal10Serializer16ObjectSerializer10OutputCodeEi, @function
_ZN2v88internal10Serializer16ObjectSerializer10OutputCodeEi:
.LFB21343:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	$1416, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r12
	movq	8(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r12, %rsi
	call	_ZN2v88internal10Serializer8CopyCodeENS0_4CodeE
	movq	7(%r12), %rdx
	leaq	-128(%rbp), %r12
	movl	$1997, %ecx
	movq	%rax, %rsi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal13RelocIteratorC1ENS0_4CodeENS0_9ByteArrayEi@PLT
	cmpb	$0, -72(%rbp)
	jne	.L417
	.p2align 4,,10
	.p2align 3
.L410:
	movzbl	-104(%rbp), %eax
	cmpb	$10, %al
	jbe	.L424
.L413:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L424:
	btq	%rax, %r13
	jnc	.L414
	movq	-112(%rbp), %rax
	movq	$0, (%rax)
.L415:
	movq	%r12, %rdi
	call	_ZN2v88internal13RelocIterator4nextEv@PLT
	cmpb	$0, -72(%rbp)
	je	.L410
.L417:
	movq	$0, 7(%rbx)
	leaq	39(%rbx), %r13
	subl	$40, %r15d
	movq	$0, 15(%rbx)
	movq	$0, 23(%rbx)
	movq	$0, 31(%rbx)
	movb	$30, -128(%rbp)
	movq	24(%r14), %rdi
	movq	8(%rdi), %rsi
	cmpq	16(%rdi), %rsi
	je	.L425
	movb	$30, (%rsi)
	addq	$1, 8(%rdi)
.L418:
	movq	24(%r14), %rdi
	movslq	%r15d, %rsi
	leaq	.LC19(%rip), %rdx
	call	_ZN2v88internal16SnapshotByteSink6PutIntEmPKc@PLT
	movq	24(%r14), %rdi
	movl	%r15d, %edx
	movq	%r13, %rsi
	leaq	.LC20(%rip), %rcx
	call	_ZN2v88internal16SnapshotByteSink6PutRawEPKhiPKc@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L426
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L414:
	.cfi_restore_state
	testb	%al, %al
	je	.L416
	cmpb	$6, %al
	jne	.L413
.L416:
	movq	-112(%rbp), %rdi
	movl	$4, %esi
	movl	$0, (%rdi)
	call	_ZN2v88internal21FlushInstructionCacheEPvm@PLT
	jmp	.L415
.L425:
	movq	%r12, %rdx
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	jmp	.L418
.L426:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21343:
	.size	_ZN2v88internal10Serializer16ObjectSerializer10OutputCodeEi, .-_ZN2v88internal10Serializer16ObjectSerializer10OutputCodeEi
	.section	.text._ZN2v88internal10Serializer19SerializeRootObjectENS0_6ObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10Serializer19SerializeRootObjectENS0_6ObjectE
	.type	_ZN2v88internal10Serializer19SerializeRootObjectENS0_6ObjectE, @function
_ZN2v88internal10Serializer19SerializeRootObjectENS0_6ObjectE:
.LFB21301:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	andl	$1, %esi
	je	.L450
	movq	(%rdi), %rax
	movq	%r13, %rsi
	call	*40(%rax)
.L427:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L451
	addq	$24, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L450:
	.cfi_restore_state
	movb	$96, -41(%rbp)
	movq	88(%rdi), %rsi
	leaq	80(%rdi), %r14
	cmpq	96(%rdi), %rsi
	je	.L429
	movb	$96, (%rsi)
	movq	88(%rdi), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 88(%rdi)
.L430:
	movb	%r13b, -41(%rbp)
	cmpq	%rsi, 96(%r12)
	je	.L452
	movb	%r13b, (%rsi)
	addq	$1, 88(%r12)
.L432:
	movq	%r13, %rax
	movq	88(%r12), %rsi
	movzbl	%ah, %eax
	movb	%al, -41(%rbp)
	cmpq	96(%r12), %rsi
	je	.L453
	movb	%al, (%rsi)
	addq	$1, 88(%r12)
.L434:
	movq	%r13, %rax
	movq	88(%r12), %rsi
	shrq	$16, %rax
	movb	%al, -41(%rbp)
	cmpq	96(%r12), %rsi
	je	.L454
	movb	%al, (%rsi)
	addq	$1, 88(%r12)
.L436:
	movq	%r13, %rax
	movq	88(%r12), %rsi
	shrq	$24, %rax
	movb	%al, -41(%rbp)
	cmpq	96(%r12), %rsi
	je	.L455
	movb	%al, (%rsi)
	addq	$1, 88(%r12)
.L438:
	movq	%r13, %rax
	movq	88(%r12), %rsi
	shrq	$32, %rax
	movb	%al, -41(%rbp)
	cmpq	96(%r12), %rsi
	je	.L456
	movb	%al, (%rsi)
	addq	$1, 88(%r12)
.L440:
	movq	%r13, %rax
	movq	88(%r12), %rsi
	shrq	$40, %rax
	movb	%al, -41(%rbp)
	cmpq	96(%r12), %rsi
	je	.L457
	movb	%al, (%rsi)
	addq	$1, 88(%r12)
.L442:
	movq	%r13, %rax
	movq	88(%r12), %rsi
	shrq	$48, %rax
	movb	%al, -41(%rbp)
	cmpq	96(%r12), %rsi
	je	.L458
	movb	%al, (%rsi)
	addq	$1, 88(%r12)
.L444:
	shrq	$56, %r13
	movq	88(%r12), %rsi
	movb	%r13b, -41(%rbp)
	cmpq	%rsi, 96(%r12)
	je	.L445
	movb	%r13b, (%rsi)
	addq	$1, 88(%r12)
	jmp	.L427
	.p2align 4,,10
	.p2align 3
.L429:
	leaq	-41(%rbp), %rdx
	movq	%r14, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movq	88(%r12), %rsi
	jmp	.L430
	.p2align 4,,10
	.p2align 3
.L445:
	leaq	-41(%rbp), %rdx
	movq	%r14, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	jmp	.L427
	.p2align 4,,10
	.p2align 3
.L458:
	leaq	-41(%rbp), %rdx
	movq	%r14, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	jmp	.L444
	.p2align 4,,10
	.p2align 3
.L457:
	leaq	-41(%rbp), %rdx
	movq	%r14, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	jmp	.L442
	.p2align 4,,10
	.p2align 3
.L456:
	leaq	-41(%rbp), %rdx
	movq	%r14, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	jmp	.L440
	.p2align 4,,10
	.p2align 3
.L455:
	leaq	-41(%rbp), %rdx
	movq	%r14, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L454:
	leaq	-41(%rbp), %rdx
	movq	%r14, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	jmp	.L436
	.p2align 4,,10
	.p2align 3
.L453:
	leaq	-41(%rbp), %rdx
	movq	%r14, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	jmp	.L434
	.p2align 4,,10
	.p2align 3
.L452:
	leaq	-41(%rbp), %rdx
	movq	%r14, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	jmp	.L432
.L451:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21301:
	.size	_ZN2v88internal10Serializer19SerializeRootObjectENS0_6ObjectE, .-_ZN2v88internal10Serializer19SerializeRootObjectENS0_6ObjectE
	.section	.rodata._ZN2v88internal10Serializer16ObjectSerializer22VisitExternalReferenceENS0_7ForeignEPm.str1.1,"aMS",@progbits,1
.LC21:
	.string	"reference index"
	.section	.text._ZN2v88internal10Serializer16ObjectSerializer22VisitExternalReferenceENS0_7ForeignEPm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10Serializer16ObjectSerializer22VisitExternalReferenceENS0_7ForeignEPm
	.type	_ZN2v88internal10Serializer16ObjectSerializer22VisitExternalReferenceENS0_7ForeignEPm, @function
_ZN2v88internal10Serializer16ObjectSerializer22VisitExternalReferenceENS0_7ForeignEPm:
.LFB21335:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	7(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	leaq	136(%rax), %rdi
	call	_ZN2v88internal24ExternalReferenceEncoder6EncodeEm@PLT
	movq	24(%rbx), %rdi
	movl	%eax, %r12d
	testl	%eax, %eax
	js	.L468
	movb	$33, -25(%rbp)
	movq	8(%rdi), %rsi
	cmpq	16(%rdi), %rsi
	je	.L464
	movb	$33, (%rsi)
	addq	$1, 8(%rdi)
.L463:
	movq	24(%rbx), %rdi
	movl	%r12d, %esi
	leaq	.LC21(%rip), %rdx
	andl	$2147483647, %esi
	call	_ZN2v88internal16SnapshotByteSink6PutIntEmPKc@PLT
	addl	$8, 32(%rbx)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L469
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L468:
	.cfi_restore_state
	movb	$32, -25(%rbp)
	movq	8(%rdi), %rsi
	cmpq	16(%rdi), %rsi
	je	.L464
	movb	$32, (%rsi)
	addq	$1, 8(%rdi)
	jmp	.L463
	.p2align 4,,10
	.p2align 3
.L464:
	leaq	-25(%rbp), %rdx
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	jmp	.L463
.L469:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21335:
	.size	_ZN2v88internal10Serializer16ObjectSerializer22VisitExternalReferenceENS0_7ForeignEPm, .-_ZN2v88internal10Serializer16ObjectSerializer22VisitExternalReferenceENS0_7ForeignEPm
	.section	.text._ZN2v88internal10Serializer16ObjectSerializer22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10Serializer16ObjectSerializer22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE
	.type	_ZN2v88internal10Serializer16ObjectSerializer22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE, @function
_ZN2v88internal10Serializer16ObjectSerializer22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE:
.LFB21336:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %rcx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	leaq	136(%rcx), %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal24ExternalReferenceEncoder6EncodeEm@PLT
	movq	24(%rbx), %rdi
	movl	%eax, %r12d
	testl	%eax, %eax
	js	.L481
	movb	$33, -41(%rbp)
	movq	8(%rdi), %rsi
	cmpq	16(%rdi), %rsi
	je	.L475
	movb	$33, (%rsi)
	addq	$1, 8(%rdi)
.L474:
	movq	24(%rbx), %rdi
	movl	%r12d, %esi
	leaq	.LC21(%rip), %rdx
	andl	$2147483647, %esi
	call	_ZN2v88internal16SnapshotByteSink6PutIntEmPKc@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal9RelocInfo16IsCodedSpeciallyEv@PLT
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$4, %eax
	addl	$4, %eax
	addl	%eax, 32(%rbx)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L482
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L481:
	.cfi_restore_state
	movb	$32, -41(%rbp)
	movq	8(%rdi), %rsi
	cmpq	16(%rdi), %rsi
	je	.L475
	movb	$32, (%rsi)
	addq	$1, 8(%rdi)
	jmp	.L474
	.p2align 4,,10
	.p2align 3
.L475:
	leaq	-41(%rbp), %rdx
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	jmp	.L474
.L482:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21336:
	.size	_ZN2v88internal10Serializer16ObjectSerializer22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE, .-_ZN2v88internal10Serializer16ObjectSerializer22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE
	.section	.rodata._ZN2v88internal10Serializer7PutRootENS0_9RootIndexENS0_10HeapObjectE.str1.1,"aMS",@progbits,1
.LC22:
	.string	" Encoding root %d:"
.LC23:
	.string	"root_index"
	.section	.text._ZN2v88internal10Serializer7PutRootENS0_9RootIndexENS0_10HeapObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10Serializer7PutRootENS0_9RootIndexENS0_10HeapObjectE
	.type	_ZN2v88internal10Serializer7PutRootENS0_9RootIndexENS0_10HeapObjectE, @function
_ZN2v88internal10Serializer7PutRootENS0_9RootIndexENS0_10HeapObjectE:
.LFB21306:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movzwl	%si, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	%rdx, -56(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal21FLAG_trace_serializerE(%rip)
	jne	.L493
.L484:
	movq	88(%rbx), %rsi
	movq	96(%rbx), %rdx
	leaq	80(%rbx), %rdi
	cmpl	$31, %r14d
	jg	.L485
	movq	-56(%rbp), %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L494
.L485:
	movb	$17, -41(%rbp)
	cmpq	%rdx, %rsi
	je	.L489
	movb	$17, (%rsi)
	addq	$1, 88(%rbx)
.L490:
	leaq	.LC23(%rip), %rdx
	movzwl	%r13w, %esi
	call	_ZN2v88internal16SnapshotByteSink6PutIntEmPKc@PLT
	movslq	72(%rbx), %rdx
	movq	-56(%rbp), %rcx
	movq	%rdx, %rax
	movq	%rcx, 8(%rbx,%rdx,8)
	addl	$1, %eax
	andl	$7, %eax
	movl	%eax, 72(%rbx)
.L483:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L495
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L494:
	.cfi_restore_state
	addl	$64, %r12d
	movb	%r12b, -41(%rbp)
	cmpq	%rdx, %rsi
	je	.L486
	movb	%r12b, (%rsi)
	addq	$1, 88(%rbx)
	jmp	.L483
	.p2align 4,,10
	.p2align 3
.L493:
	movl	%r14d, %esi
	leaq	.LC22(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	stdout(%rip), %rsi
	leaq	-56(%rbp), %rdi
	call	_ZNK2v88internal6Object10ShortPrintEP8_IO_FILE@PLT
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L484
	.p2align 4,,10
	.p2align 3
.L489:
	leaq	-41(%rbp), %rdx
	movq	%rdi, -64(%rbp)
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movq	-64(%rbp), %rdi
	jmp	.L490
	.p2align 4,,10
	.p2align 3
.L486:
	leaq	-41(%rbp), %rdx
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	jmp	.L483
.L495:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21306:
	.size	_ZN2v88internal10Serializer7PutRootENS0_9RootIndexENS0_10HeapObjectE, .-_ZN2v88internal10Serializer7PutRootENS0_9RootIndexENS0_10HeapObjectE
	.section	.text._ZN2v88internal10Serializer13SerializeRootENS0_10HeapObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10Serializer13SerializeRootENS0_10HeapObjectE
	.type	_ZN2v88internal10Serializer13SerializeRootENS0_10HeapObjectE, @function
_ZN2v88internal10Serializer13SerializeRootENS0_10HeapObjectE:
.LFB21302:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	144(%rdi), %rax
	movl	8(%rax), %ecx
	movq	(%rax), %r8
	subl	$1, %ecx
	movl	%ecx, %eax
	andl	%esi, %eax
	leaq	(%rax,%rax,2), %rsi
	leaq	(%r8,%rsi,8), %rsi
	movzbl	16(%rsi), %r12d
	testb	%r12b, %r12b
	jne	.L499
	jmp	.L497
	.p2align 4,,10
	.p2align 3
.L506:
	addq	$1, %rax
	andq	%rcx, %rax
	leaq	(%rax,%rax,2), %rsi
	leaq	(%r8,%rsi,8), %rsi
	cmpb	$0, 16(%rsi)
	je	.L497
.L499:
	cmpq	%rdx, (%rsi)
	jne	.L506
	movl	8(%rsi), %esi
	call	_ZN2v88internal10Serializer7PutRootENS0_9RootIndexENS0_10HeapObjectE
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L497:
	.cfi_restore_state
	xorl	%r12d, %r12d
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21302:
	.size	_ZN2v88internal10Serializer13SerializeRootENS0_10HeapObjectE, .-_ZN2v88internal10Serializer13SerializeRootENS0_10HeapObjectE
	.section	.rodata._ZN2v88internal10Serializer22SerializeBackReferenceENS0_10HeapObjectE.str1.8,"aMS",@progbits,1
	.align 8
.LC24:
	.string	" Encoding attached reference %d\n"
	.section	.rodata._ZN2v88internal10Serializer22SerializeBackReferenceENS0_10HeapObjectE.str1.1,"aMS",@progbits,1
.LC25:
	.string	" Encoding back reference to: "
	.section	.text._ZN2v88internal10Serializer22SerializeBackReferenceENS0_10HeapObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10Serializer22SerializeBackReferenceENS0_10HeapObjectE
	.type	_ZN2v88internal10Serializer22SerializeBackReferenceENS0_10HeapObjectE, @function
_ZN2v88internal10Serializer22SerializeBackReferenceENS0_10HeapObjectE:
.LFB21304:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%rsi, -72(%rbp)
	movq	112(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	120(%rbx), %eax
	leal	-1(%rax), %edx
	movl	%edx, %eax
	andl	%esi, %eax
	leaq	(%rax,%rax,2), %rcx
	leaq	(%rdi,%rcx,8), %rcx
	movzbl	20(%rcx), %r12d
	testb	%r12b, %r12b
	jne	.L510
	jmp	.L507
	.p2align 4,,10
	.p2align 3
.L538:
	addq	$1, %rax
	andq	%rdx, %rax
	leaq	(%rax,%rax,2), %rcx
	leaq	(%rdi,%rcx,8), %rcx
	cmpb	$0, 20(%rcx)
	je	.L537
.L510:
	cmpq	(%rcx), %rsi
	jne	.L538
	movl	8(%rcx), %r15d
	movl	12(%rcx), %r14d
	movzbl	_ZN2v88internal21FLAG_trace_serializerE(%rip), %eax
	movl	%r15d, %r13d
	andl	$15, %r13d
	cmpl	$6, %r13d
	je	.L539
.L511:
	testb	%al, %al
	jne	.L540
.L515:
	movq	-1(%rsi), %rax
	leal	8(%r13), %eax
	movq	88(%rbx), %rsi
	leaq	80(%rbx), %rdi
	movb	%al, -57(%rbp)
	cmpq	96(%rbx), %rsi
	je	.L516
	movb	%al, (%rsi)
	addq	$1, 88(%rbx)
.L517:
	movq	-72(%rbp), %rcx
	cmpl	$4, %r13d
	je	.L518
	cmpl	$5, %r13d
	jne	.L541
	leaq	.LC6(%rip), %rdx
	movq	%r14, %rsi
	movq	%rcx, -80(%rbp)
	call	_ZN2v88internal16SnapshotByteSink6PutIntEmPKc@PLT
	movq	-80(%rbp), %rcx
.L521:
	movslq	72(%rbx), %rdx
	movq	%rdx, %rax
	movq	%rcx, 8(%rbx,%rdx,8)
	addl	$1, %eax
	andl	$7, %eax
	movl	%eax, 72(%rbx)
.L507:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L542
	addq	$56, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L541:
	.cfi_restore_state
	shrl	$4, %r15d
	leaq	.LC7(%rip), %rdx
	movq	%rcx, -88(%rbp)
	movl	%r15d, %esi
	movq	%rdi, -80(%rbp)
	call	_ZN2v88internal16SnapshotByteSink6PutIntEmPKc@PLT
	movq	-80(%rbp), %rdi
	leaq	.LC8(%rip), %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal16SnapshotByteSink6PutIntEmPKc@PLT
	movq	-88(%rbp), %rcx
	jmp	.L521
	.p2align 4,,10
	.p2align 3
.L518:
	leaq	.LC5(%rip), %rdx
	movq	%r14, %rsi
	movq	%rcx, -80(%rbp)
	call	_ZN2v88internal16SnapshotByteSink6PutIntEmPKc@PLT
	movq	-80(%rbp), %rcx
	jmp	.L521
	.p2align 4,,10
	.p2align 3
.L540:
	xorl	%eax, %eax
	leaq	.LC25(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	stdout(%rip), %rsi
	leaq	-72(%rbp), %rdi
	call	_ZNK2v88internal6Object10ShortPrintEP8_IO_FILE@PLT
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-72(%rbp), %rsi
	jmp	.L515
	.p2align 4,,10
	.p2align 3
.L537:
	xorl	%r12d, %r12d
	jmp	.L507
	.p2align 4,,10
	.p2align 3
.L539:
	movl	%r15d, %edx
	shrl	$4, %edx
	je	.L537
	cmpl	$1, %edx
	jne	.L511
	testb	%al, %al
	jne	.L543
.L512:
	movb	$18, -57(%rbp)
	movq	88(%rbx), %rsi
	leaq	80(%rbx), %rdi
	cmpq	96(%rbx), %rsi
	je	.L513
	movb	$18, (%rsi)
	addq	$1, 88(%rbx)
.L514:
	movl	%r14d, %esi
	leaq	.LC11(%rip), %rdx
	call	_ZN2v88internal16SnapshotByteSink6PutIntEmPKc@PLT
	jmp	.L507
	.p2align 4,,10
	.p2align 3
.L516:
	leaq	-57(%rbp), %rdx
	movq	%rdi, -80(%rbp)
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movq	-80(%rbp), %rdi
	jmp	.L517
	.p2align 4,,10
	.p2align 3
.L543:
	movl	%r14d, %esi
	leaq	.LC24(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L512
	.p2align 4,,10
	.p2align 3
.L513:
	leaq	-57(%rbp), %rdx
	movq	%rdi, -80(%rbp)
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movq	-80(%rbp), %rdi
	jmp	.L514
.L542:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21304:
	.size	_ZN2v88internal10Serializer22SerializeBackReferenceENS0_10HeapObjectE, .-_ZN2v88internal10Serializer22SerializeBackReferenceENS0_10HeapObjectE
	.section	.rodata._ZN2v88internal10Serializer16ObjectSerializer13OutputRawDataEm.str1.1,"aMS",@progbits,1
.LC26:
	.string	"Bytes"
	.section	.text._ZN2v88internal10Serializer16ObjectSerializer13OutputRawDataEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10Serializer16ObjectSerializer13OutputRawDataEm
	.type	_ZN2v88internal10Serializer16ObjectSerializer13OutputRawDataEm, @function
_ZN2v88internal10Serializer16ObjectSerializer13OutputRawDataEm:
.LFB21342:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	32(%rdi), %r14d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	leaq	-1(%rax), %r15
	subl	%r15d, %r13d
	movl	%r13d, %r12d
	movl	%r13d, 32(%rdi)
	subl	%r14d, %r12d
	je	.L544
	movq	%rdi, %rbx
	movq	24(%rdi), %rdi
	movq	8(%rdi), %rsi
	movq	16(%rdi), %rdx
	testb	$7, %r12b
	jne	.L546
	cmpl	$256, %r12d
	jle	.L575
.L546:
	movb	$31, -59(%rbp)
	cmpq	%rdx, %rsi
	je	.L550
	movb	$31, (%rsi)
	addq	$1, 8(%rdi)
.L551:
	movq	24(%rbx), %rdi
	movslq	%r12d, %rsi
	leaq	.LC19(%rip), %rdx
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal16SnapshotByteSink6PutIntEmPKc@PLT
	movq	-72(%rbp), %rax
.L549:
	movq	16(%rbx), %rdx
	movslq	%r14d, %rsi
	addq	%r15, %rsi
	movq	-1(%rdx), %rcx
	cmpw	$72, 11(%rcx)
	je	.L576
	movq	-1(%rdx), %rdx
	cmpw	$153, 11(%rdx)
	je	.L577
	movq	24(%rbx), %rdi
	leaq	.LC26(%rip), %rcx
	movl	%r12d, %edx
	call	_ZN2v88internal16SnapshotByteSink6PutRawEPKhiPKc@PLT
.L544:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L578
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L575:
	.cfi_restore_state
	movl	%r12d, %ecx
	sarl	$3, %ecx
	addl	$95, %ecx
	movb	%cl, -59(%rbp)
	cmpq	%rdx, %rsi
	je	.L547
	movb	%cl, (%rsi)
	addq	$1, 8(%rdi)
	jmp	.L549
	.p2align 4,,10
	.p2align 3
.L576:
	movl	$53, %edx
	movq	%rax, -72(%rbp)
	movq	24(%rbx), %r15
	leaq	.LC26(%rip), %rcx
	movb	$0, -59(%rbp)
	subl	%r14d, %edx
	js	.L556
	cmpl	$53, %r13d
	jle	.L556
	movq	%r15, %rdi
	call	_ZN2v88internal16SnapshotByteSink6PutRawEPKhiPKc@PLT
	leaq	-59(%rbp), %rsi
	movq	%r15, %rdi
	leaq	.LC26(%rip), %rcx
	movl	$1, %edx
	call	_ZN2v88internal16SnapshotByteSink6PutRawEPKhiPKc@PLT
	movq	-72(%rbp), %rax
	leal	-54(%r13), %edx
	movq	%r15, %rdi
	leaq	.LC26(%rip), %rcx
	leaq	53(%rax), %rsi
	call	_ZN2v88internal16SnapshotByteSink6PutRawEPKhiPKc@PLT
	jmp	.L544
	.p2align 4,,10
	.p2align 3
.L556:
	movl	%r12d, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal16SnapshotByteSink6PutRawEPKhiPKc@PLT
	jmp	.L544
	.p2align 4,,10
	.p2align 3
.L577:
	movq	%rax, -72(%rbp)
	movl	$12, %edx
	xorl	%eax, %eax
	movq	24(%rbx), %r15
	movw	%ax, -58(%rbp)
	leaq	.LC26(%rip), %rcx
	subl	%r14d, %edx
	js	.L556
	cmpl	$12, %r13d
	jle	.L556
	movq	%r15, %rdi
	call	_ZN2v88internal16SnapshotByteSink6PutRawEPKhiPKc@PLT
	leaq	-58(%rbp), %rsi
	movq	%r15, %rdi
	leaq	.LC26(%rip), %rcx
	movl	$2, %edx
	call	_ZN2v88internal16SnapshotByteSink6PutRawEPKhiPKc@PLT
	movq	-72(%rbp), %rax
	leal	-14(%r13), %edx
	movq	%r15, %rdi
	leaq	.LC26(%rip), %rcx
	leaq	13(%rax), %rsi
	call	_ZN2v88internal16SnapshotByteSink6PutRawEPKhiPKc@PLT
	jmp	.L544
	.p2align 4,,10
	.p2align 3
.L550:
	leaq	-59(%rbp), %rdx
	movq	%rax, -72(%rbp)
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movq	-72(%rbp), %rax
	jmp	.L551
	.p2align 4,,10
	.p2align 3
.L547:
	leaq	-59(%rbp), %rdx
	movq	%rax, -72(%rbp)
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movq	-72(%rbp), %rax
	jmp	.L549
.L578:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21342:
	.size	_ZN2v88internal10Serializer16ObjectSerializer13OutputRawDataEm, .-_ZN2v88internal10Serializer16ObjectSerializer13OutputRawDataEm
	.section	.text._ZN2v88internal10Serializer16ObjectSerializer16SerializeContentENS0_3MapEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10Serializer16ObjectSerializer16SerializeContentENS0_3MapEi
	.type	_ZN2v88internal10Serializer16ObjectSerializer16SerializeContentENS0_3MapEi, @function
_ZN2v88internal10Serializer16ObjectSerializer16SerializeContentENS0_3MapEi:
.LFB21331:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movslq	%edx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	8(%rdi), %rax
	movq	16(%rdi), %r15
	movq	104(%rax), %rdx
	movq	-1(%r15), %rax
	cmpw	$121, 11(%rax)
	je	.L603
.L580:
	movq	$0, -56(%rbp)
	xorl	%r15d, %r15d
.L584:
	movq	16(%r12), %rax
	leaq	16(%r12), %r14
	movq	-1(%rax), %rax
	cmpw	$69, 11(%rax)
	je	.L604
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	%r12, %rcx
	movl	%r13d, %edx
	call	_ZN2v88internal10HeapObject11IterateBodyENS0_3MapEiPNS0_13ObjectVisitorE@PLT
	movq	16(%r12), %rax
	movq	%r12, %rdi
	leaq	-1(%r13,%rax), %rsi
	call	_ZN2v88internal10Serializer16ObjectSerializer13OutputRawDataEm
	testq	%r15, %r15
	jne	.L605
.L579:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L604:
	.cfi_restore_state
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal10Serializer16ObjectSerializer10OutputCodeEi
	movq	%r12, %rcx
	movl	%r13d, %edx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal10HeapObject11IterateBodyENS0_3MapEiPNS0_13ObjectVisitorE@PLT
	testq	%r15, %r15
	je	.L579
.L605:
	movq	-56(%rbp), %rdx
	leaq	39(%r15), %rsi
	movq	%rdx, 39(%r15)
	testb	$1, %dl
	je	.L579
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L579
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L579
	addq	$40, %rsp
	movq	%r15, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L603:
	.cfi_restore_state
	movq	%r15, %r14
	andq	$-262144, %r14
	movq	24(%r14), %rcx
	movq	-1(%r15), %rax
	cmpq	%rax, -33296(%rcx)
	jne	.L580
	movq	39(%r15), %rax
	movq	88(%rdx), %rdx
	leaq	39(%r15), %rsi
	movq	%rax, -56(%rbp)
	movq	%rdx, 39(%r15)
	testb	$1, %dl
	je	.L584
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -64(%rbp)
	testl	$262144, %eax
	je	.L582
	movq	%r15, %rdi
	movq	%rdx, -80(%rbp)
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-64(%rbp), %rcx
	movq	-80(%rbp), %rdx
	movq	-72(%rbp), %rsi
	movq	8(%rcx), %rax
.L582:
	testb	$24, %al
	je	.L584
	testb	$24, 8(%r14)
	jne	.L584
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L584
	.cfi_endproc
.LFE21331:
	.size	_ZN2v88internal10Serializer16ObjectSerializer16SerializeContentENS0_3MapEi, .-_ZN2v88internal10Serializer16ObjectSerializer16SerializeContentENS0_3MapEi
	.section	.rodata._ZN2v88internal10Serializer16ObjectSerializer17SerializeDeferredEv.str1.8,"aMS",@progbits,1
	.align 8
.LC27:
	.string	" Encoding deferred heap object: "
	.section	.rodata._ZN2v88internal10Serializer16ObjectSerializer17SerializeDeferredEv.str1.1,"aMS",@progbits,1
.LC28:
	.string	"0 == bytes_processed_so_far_"
.LC29:
	.string	"deferred object size"
	.section	.text._ZN2v88internal10Serializer16ObjectSerializer17SerializeDeferredEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10Serializer16ObjectSerializer17SerializeDeferredEv
	.type	_ZN2v88internal10Serializer16ObjectSerializer17SerializeDeferredEv, @function
_ZN2v88internal10Serializer16ObjectSerializer17SerializeDeferredEv:
.LFB21330:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	16(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal21FLAG_trace_serializerE(%rip)
	jne	.L624
.L607:
	movq	16(%r12), %rax
	movq	%r13, %rdi
	movq	-1(%rax), %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	16(%r12), %rsi
	movl	%eax, %r13d
	movq	-1(%rsi), %r14
	movq	8(%r12), %rax
	movl	120(%rax), %edx
	movq	112(%rax), %rdi
	subl	$1, %edx
	movl	%edx, %eax
	andl	%esi, %eax
	leaq	(%rax,%rax,2), %rcx
	leaq	(%rdi,%rcx,8), %rcx
	cmpb	$0, 20(%rcx)
	jne	.L610
	jmp	.L619
	.p2align 4,,10
	.p2align 3
.L625:
	addq	$1, %rax
	andq	%rdx, %rax
	leaq	(%rax,%rax,2), %rcx
	leaq	(%rdi,%rcx,8), %rcx
	cmpb	$0, 20(%rcx)
	je	.L619
.L610:
	cmpq	(%rcx), %rsi
	jne	.L625
	movl	32(%r12), %eax
	movl	8(%rcx), %r8d
	movl	12(%rcx), %r9d
	testl	%eax, %eax
	jne	.L626
.L611:
	movl	$8, 32(%r12)
	movq	-1(%rsi), %rax
	movl	%r8d, %eax
	movq	24(%r12), %rdi
	andl	$15, %eax
	movb	%al, -57(%rbp)
	movq	8(%rdi), %rsi
	cmpq	16(%rdi), %rsi
	je	.L612
	movb	%al, (%rsi)
	addq	$1, 8(%rdi)
.L613:
	movq	8(%r12), %r15
	movq	16(%r12), %rbx
	leaq	80(%r15), %rdi
	cmpl	$4, %eax
	je	.L614
	cmpl	$5, %eax
	jne	.L627
	leaq	.LC6(%rip), %rdx
	movq	%r9, %rsi
	call	_ZN2v88internal16SnapshotByteSink6PutIntEmPKc@PLT
.L617:
	movslq	72(%r15), %rdx
	movl	%r13d, %esi
	sarl	$3, %esi
	movq	%rdx, %rax
	movq	%rbx, 8(%r15,%rdx,8)
	movslq	%esi, %rsi
	leaq	.LC29(%rip), %rdx
	addl	$1, %eax
	andl	$7, %eax
	movl	%eax, 72(%r15)
	movq	24(%r12), %rdi
	call	_ZN2v88internal16SnapshotByteSink6PutIntEmPKc@PLT
	movl	%r13d, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10Serializer16ObjectSerializer16SerializeContentENS0_3MapEi
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L628
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L619:
	.cfi_restore_state
	movl	32(%r12), %eax
	xorl	%r9d, %r9d
	movl	$6, %r8d
	testl	%eax, %eax
	je	.L611
.L626:
	leaq	.LC28(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L627:
	shrl	$4, %r8d
	leaq	.LC7(%rip), %rdx
	movq	%r9, -80(%rbp)
	movl	%r8d, %esi
	movq	%rdi, -72(%rbp)
	call	_ZN2v88internal16SnapshotByteSink6PutIntEmPKc@PLT
	movq	-80(%rbp), %r9
	movq	-72(%rbp), %rdi
	leaq	.LC8(%rip), %rdx
	movq	%r9, %rsi
	call	_ZN2v88internal16SnapshotByteSink6PutIntEmPKc@PLT
	jmp	.L617
	.p2align 4,,10
	.p2align 3
.L624:
	leaq	.LC27(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	stdout(%rip), %rsi
	movq	%r13, %rdi
	call	_ZNK2v88internal6Object10ShortPrintEP8_IO_FILE@PLT
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L607
	.p2align 4,,10
	.p2align 3
.L614:
	leaq	.LC5(%rip), %rdx
	movq	%r9, %rsi
	call	_ZN2v88internal16SnapshotByteSink6PutIntEmPKc@PLT
	jmp	.L617
	.p2align 4,,10
	.p2align 3
.L612:
	leaq	-57(%rbp), %rdx
	movl	%r9d, -84(%rbp)
	movl	%eax, -80(%rbp)
	movl	%r8d, -72(%rbp)
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movl	-84(%rbp), %r9d
	movl	-80(%rbp), %eax
	movl	-72(%rbp), %r8d
	jmp	.L613
.L628:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21330:
	.size	_ZN2v88internal10Serializer16ObjectSerializer17SerializeDeferredEv, .-_ZN2v88internal10Serializer16ObjectSerializer17SerializeDeferredEv
	.section	.text._ZN2v88internal10Serializer24SerializeDeferredObjectsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10Serializer24SerializeDeferredObjectsEv
	.type	_ZN2v88internal10Serializer24SerializeDeferredObjectsEv, @function
_ZN2v88internal10Serializer24SerializeDeferredObjectsEv:
.LFB21298:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	16+_ZTVN2v88internal10Serializer16ObjectSerializerE(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	80(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	leaq	-80(%rbp), %rbx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	192(%rdi), %rax
	cmpq	%rax, 184(%rdi)
	je	.L633
	.p2align 4,,10
	.p2align 3
.L630:
	movq	-8(%rax), %rdx
	subq	$8, %rax
	movq	%rbx, %rdi
	movq	%r13, -80(%rbp)
	movq	%rax, 192(%r14)
	movq	%r14, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r12, -56(%rbp)
	movl	$0, -48(%rbp)
	call	_ZN2v88internal10Serializer16ObjectSerializer17SerializeDeferredEv
	movq	192(%r14), %rax
	cmpq	%rax, 184(%r14)
	jne	.L630
.L633:
	movb	$26, -80(%rbp)
	movq	88(%r14), %rsi
	cmpq	96(%r14), %rsi
	je	.L642
	movb	$26, (%rsi)
	addq	$1, 88(%r14)
.L629:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L643
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L642:
	.cfi_restore_state
	leaq	-80(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	jmp	.L629
.L643:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21298:
	.size	_ZN2v88internal10Serializer24SerializeDeferredObjectsEv, .-_ZN2v88internal10Serializer24SerializeDeferredObjectsEv
	.section	.text._ZN2v88internal10Serializer16ObjectSerializer13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10Serializer16ObjectSerializer13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES4_
	.type	_ZN2v88internal10Serializer16ObjectSerializer13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES4_, @function
_ZN2v88internal10Serializer16ObjectSerializer13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES4_:
.LFB21333:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-57(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%rcx, %rdx
	jnb	.L644
	.p2align 4,,10
	.p2align 3
.L648:
	testb	$1, (%rbx)
	jne	.L647
	addq	$8, %rbx
	cmpq	%rbx, %r14
	ja	.L648
.L644:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L684
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L647:
	.cfi_restore_state
	cmpq	%rbx, %r14
	jbe	.L644
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10Serializer16ObjectSerializer13OutputRawDataEm
.L653:
	movq	(%rbx), %rax
	cmpl	$3, %eax
	jne	.L650
.L685:
	movq	24(%r12), %rdi
	movb	$35, -57(%rbp)
	movq	8(%rdi), %rsi
	cmpq	16(%rdi), %rsi
	je	.L651
	addq	$8, %rbx
	movb	$35, (%rsi)
	addq	$1, 8(%rdi)
	addl	$8, 32(%r12)
	cmpq	%rbx, %r14
	jbe	.L644
	movq	(%rbx), %rax
	cmpl	$3, %eax
	je	.L685
.L650:
	cmpq	%rbx, %r14
	jbe	.L644
	testb	$1, %al
	je	.L655
.L688:
	cmpl	$3, %eax
	je	.L655
	movq	%rax, %r9
	movq	%rax, %r15
	leaq	8(%rbx), %r8
	andl	$3, %r9d
	andq	$-3, %r15
	cmpq	%r8, %r14
	ja	.L686
.L657:
	addl	$8, 32(%r12)
.L663:
	cmpq	$3, %r9
	je	.L687
.L672:
	movq	8(%r12), %rdi
	movq	%r8, -72(%rbp)
	movq	%r15, %rsi
	movq	(%rdi), %rax
	call	*40(%rax)
	movq	-72(%rbp), %r8
	cmpq	%r8, %r14
	jbe	.L644
	movq	(%r8), %rax
	movq	%r8, %rbx
	testb	$1, %al
	jne	.L688
.L655:
	cmpq	%rbx, %r14
	ja	.L648
	jmp	.L644
	.p2align 4,,10
	.p2align 3
.L651:
	movq	%r13, %rdx
	addq	$8, %rbx
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	addl	$8, 32(%r12)
	cmpq	%rbx, %r14
	ja	.L653
	jmp	.L644
	.p2align 4,,10
	.p2align 3
.L687:
	movq	24(%r12), %rdi
	movb	$36, -57(%rbp)
	movq	8(%rdi), %rsi
	cmpq	16(%rdi), %rsi
	je	.L673
	movb	$36, (%rsi)
	addq	$1, 8(%rdi)
	jmp	.L672
	.p2align 4,,10
	.p2align 3
.L686:
	movq	8(%r12), %r10
	movq	144(%r10), %rdx
	movl	8(%rdx), %esi
	movq	(%rdx), %rdi
	subl	$1, %esi
	movl	%esi, %edx
	andl	%r15d, %edx
	leaq	(%rdx,%rdx,2), %rcx
	leaq	(%rdi,%rcx,8), %rcx
	cmpb	$0, 16(%rcx)
	jne	.L660
	jmp	.L657
	.p2align 4,,10
	.p2align 3
.L689:
	addq	$1, %rdx
	andq	%rsi, %rdx
	leaq	(%rdx,%rdx,2), %rcx
	leaq	(%rdi,%rcx,8), %rcx
	cmpb	$0, 16(%rcx)
	je	.L657
.L660:
	cmpq	(%rcx), %r15
	jne	.L689
	cmpw	$573, 8(%rcx)
	ja	.L657
	cmpq	%rax, (%r8)
	jne	.L657
	leaq	16(%rbx), %r8
	jmp	.L664
.L678:
	movq	%rdx, %r8
.L664:
	cmpq	%r8, %r14
	jbe	.L671
	leaq	8(%r8), %rdx
	cmpq	%rax, (%r8)
	je	.L678
.L671:
	movq	%r8, %rax
	leaq	80(%r10), %rdi
	subq	%rbx, %rax
	movq	%rax, %rbx
	shrq	$3, %rbx
	leal	0(,%rbx,8), %eax
	addl	%eax, 32(%r12)
	cmpl	$17, %ebx
	jg	.L665
	addl	$126, %ebx
	movb	%bl, -57(%rbp)
	movq	88(%r10), %rsi
	cmpq	96(%r10), %rsi
	je	.L666
	movb	%bl, (%rsi)
	addq	$1, 88(%r10)
	jmp	.L663
	.p2align 4,,10
	.p2align 3
.L673:
	movq	%r13, %rdx
	movq	%r8, -72(%rbp)
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movq	-72(%rbp), %r8
	jmp	.L672
.L665:
	movb	$27, -57(%rbp)
	movq	88(%r10), %rsi
	cmpq	96(%r10), %rsi
	je	.L668
	movb	$27, (%rsi)
	addq	$1, 88(%r10)
.L669:
	leal	-18(%rbx), %esi
	leaq	.LC13(%rip), %rdx
	movq	%r8, -80(%rbp)
	movslq	%esi, %rsi
	movq	%r9, -72(%rbp)
	call	_ZN2v88internal16SnapshotByteSink6PutIntEmPKc@PLT
	movq	-72(%rbp), %r9
	movq	-80(%rbp), %r8
	jmp	.L663
.L684:
	call	__stack_chk_fail@PLT
.L666:
	movq	%r13, %rdx
	movq	%r8, -80(%rbp)
	movq	%r9, -72(%rbp)
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movq	-72(%rbp), %r9
	movq	-80(%rbp), %r8
	jmp	.L663
.L668:
	movq	%r13, %rdx
	movq	%r8, -88(%rbp)
	movq	%r9, -80(%rbp)
	movq	%rdi, -72(%rbp)
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movq	-88(%rbp), %r8
	movq	-80(%rbp), %r9
	movq	-72(%rbp), %rdi
	jmp	.L669
	.cfi_endproc
.LFE21333:
	.size	_ZN2v88internal10Serializer16ObjectSerializer13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES4_, .-_ZN2v88internal10Serializer16ObjectSerializer13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES4_
	.section	.text._ZN2v84base19TemplateHashMapImplImNS_8internal19SerializerReferenceENS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE6ResizeES6_,"axG",@progbits,_ZN2v84base19TemplateHashMapImplImNS_8internal19SerializerReferenceENS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE6ResizeES6_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base19TemplateHashMapImplImNS_8internal19SerializerReferenceENS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE6ResizeES6_
	.type	_ZN2v84base19TemplateHashMapImplImNS_8internal19SerializerReferenceENS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE6ResizeES6_, @function
_ZN2v84base19TemplateHashMapImplImNS_8internal19SerializerReferenceENS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE6ResizeES6_:
.LFB25837:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	8(%rdi), %eax
	movq	(%rdi), %r13
	movl	12(%rdi), %r15d
	addl	%eax, %eax
	leaq	(%rax,%rax,2), %rdi
	movq	%rax, %rbx
	salq	$3, %rdi
	call	malloc@PLT
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L715
	movl	%ebx, 8(%r12)
	testl	%ebx, %ebx
	je	.L692
	movb	$0, 20(%rax)
	movl	$24, %edx
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L693:
	movq	(%r12), %rcx
	addq	$1, %rax
	movb	$0, 20(%rcx,%rdx)
	movl	8(%r12), %ecx
	addq	$24, %rdx
	cmpq	%rax, %rcx
	ja	.L693
.L692:
	movl	$0, 12(%r12)
	movq	%r13, %r14
	testl	%r15d, %r15d
	je	.L699
.L694:
	cmpb	$0, 20(%r14)
	jne	.L716
.L695:
	addq	$24, %r14
	cmpb	$0, 20(%r14)
	je	.L695
.L716:
	movl	8(%r12), %eax
	movl	16(%r14), %ebx
	movq	(%r12), %rdi
	movq	(%r14), %rsi
	leal	-1(%rax), %ecx
	movl	%ebx, %eax
	andl	%ecx, %eax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rdi,%rdx,8), %rdx
	cmpb	$0, 20(%rdx)
	jne	.L697
	jmp	.L696
	.p2align 4,,10
	.p2align 3
.L717:
	addq	$1, %rax
	andq	%rcx, %rax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rdi,%rdx,8), %rdx
	cmpb	$0, 20(%rdx)
	je	.L696
.L697:
	cmpq	%rsi, (%rdx)
	jne	.L717
.L696:
	movl	12(%r14), %eax
	movl	8(%r14), %ecx
	movq	%rsi, (%rdx)
	movl	%ebx, 16(%rdx)
	movl	%ecx, 8(%rdx)
	movl	%eax, 12(%rdx)
	movb	$1, 20(%rdx)
	movl	12(%r12), %eax
	addl	$1, %eax
	movl	%eax, %edx
	movl	%eax, 12(%r12)
	shrl	$2, %edx
	addl	%edx, %eax
	cmpl	8(%r12), %eax
	jnb	.L698
.L701:
	addq	$24, %r14
	subl	$1, %r15d
	jne	.L694
.L699:
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	free@PLT
	.p2align 4,,10
	.p2align 3
.L698:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v84base19TemplateHashMapImplImNS_8internal19SerializerReferenceENS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE6ResizeES6_
	movl	8(%r12), %eax
	movq	(%r12), %rsi
	leal	-1(%rax), %ecx
	movl	%ebx, %eax
	andl	%ecx, %eax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rsi,%rdx,8), %rdx
	cmpb	$0, 20(%rdx)
	je	.L701
	movq	(%r14), %rdi
	jmp	.L702
	.p2align 4,,10
	.p2align 3
.L718:
	addq	$1, %rax
	andq	%rcx, %rax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rsi,%rdx,8), %rdx
	cmpb	$0, 20(%rdx)
	je	.L701
.L702:
	cmpq	%rdi, (%rdx)
	jne	.L718
	jmp	.L701
.L715:
	leaq	.LC3(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE25837:
	.size	_ZN2v84base19TemplateHashMapImplImNS_8internal19SerializerReferenceENS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE6ResizeES6_, .-_ZN2v84base19TemplateHashMapImplImNS_8internal19SerializerReferenceENS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE6ResizeES6_
	.section	.text._ZN2v84base19TemplateHashMapImplImNS_8internal19SerializerReferenceENS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE14LookupOrInsertIZNS7_14LookupOrInsertERKmjS6_EUlvE_EEPNS0_20TemplateHashMapEntryImS3_EESA_jRKT_S6_,"axG",@progbits,_ZN2v84base19TemplateHashMapImplImNS_8internal19SerializerReferenceENS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE14LookupOrInsertIZNS7_14LookupOrInsertERKmjS6_EUlvE_EEPNS0_20TemplateHashMapEntryImS3_EESA_jRKT_S6_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base19TemplateHashMapImplImNS_8internal19SerializerReferenceENS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE14LookupOrInsertIZNS7_14LookupOrInsertERKmjS6_EUlvE_EEPNS0_20TemplateHashMapEntryImS3_EESA_jRKT_S6_
	.type	_ZN2v84base19TemplateHashMapImplImNS_8internal19SerializerReferenceENS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE14LookupOrInsertIZNS7_14LookupOrInsertERKmjS6_EUlvE_EEPNS0_20TemplateHashMapEntryImS3_EESA_jRKT_S6_, @function
_ZN2v84base19TemplateHashMapImplImNS_8internal19SerializerReferenceENS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE14LookupOrInsertIZNS7_14LookupOrInsertERKmjS6_EUlvE_EEPNS0_20TemplateHashMapEntryImS3_EESA_jRKT_S6_:
.LFB23849:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%edx, %ebx
	subq	$16, %rsp
	movl	8(%rdi), %eax
	movq	(%rdi), %r10
	movq	(%rsi), %r11
	leal	-1(%rax), %r9d
	movl	%edx, %eax
	andl	%r9d, %eax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%r10,%rdx,8), %r8
	cmpb	$0, 20(%r8)
	jne	.L722
	jmp	.L720
	.p2align 4,,10
	.p2align 3
.L729:
	addq	$1, %rax
	andq	%r9, %rax
	leaq	(%rax,%rax,2), %rcx
	leaq	(%r10,%rcx,8), %r8
	cmpb	$0, 20(%r8)
	je	.L720
.L722:
	cmpq	%r11, (%r8)
	jne	.L729
.L719:
	addq	$16, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L720:
	.cfi_restore_state
	movq	%r11, (%r8)
	movq	$6, 8(%r8)
	movl	%ebx, 16(%r8)
	movb	$1, 20(%r8)
	movl	12(%rdi), %eax
	addl	$1, %eax
	movl	%eax, %edx
	movl	%eax, 12(%rdi)
	shrl	$2, %edx
	addl	%edx, %eax
	cmpl	8(%rdi), %eax
	jb	.L719
	movq	%rdi, -24(%rbp)
	call	_ZN2v84base19TemplateHashMapImplImNS_8internal19SerializerReferenceENS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE6ResizeES6_
	movq	-24(%rbp), %rdi
	movl	%ebx, %edx
	movl	8(%rdi), %eax
	movq	(%rdi), %rsi
	leal	-1(%rax), %ecx
	andl	%ecx, %edx
	leaq	(%rdx,%rdx,2), %rax
	leaq	(%rsi,%rax,8), %r8
	cmpb	$0, 20(%r8)
	je	.L719
	movq	0(%r13), %rdi
	jmp	.L724
	.p2align 4,,10
	.p2align 3
.L730:
	addq	$1, %rdx
	andq	%rcx, %rdx
	leaq	(%rdx,%rdx,2), %rax
	leaq	(%rsi,%rax,8), %r8
	cmpb	$0, 20(%r8)
	je	.L719
.L724:
	cmpq	%rdi, (%r8)
	jne	.L730
	jmp	.L719
	.cfi_endproc
.LFE23849:
	.size	_ZN2v84base19TemplateHashMapImplImNS_8internal19SerializerReferenceENS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE14LookupOrInsertIZNS7_14LookupOrInsertERKmjS6_EUlvE_EEPNS0_20TemplateHashMapEntryImS3_EESA_jRKT_S6_, .-_ZN2v84base19TemplateHashMapImplImNS_8internal19SerializerReferenceENS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE14LookupOrInsertIZNS7_14LookupOrInsertERKmjS6_EUlvE_EEPNS0_20TemplateHashMapEntryImS3_EESA_jRKT_S6_
	.section	.rodata._ZN2v88internal10Serializer16ObjectSerializer17SerializePrologueENS0_13SnapshotSpaceEiNS0_3MapE.str1.1,"aMS",@progbits,1
.LC30:
	.string	"ObjectSizeInWords"
.LC31:
	.string	"!object_.IsCode()"
	.section	.text._ZN2v88internal10Serializer16ObjectSerializer17SerializePrologueENS0_13SnapshotSpaceEiNS0_3MapE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10Serializer16ObjectSerializer17SerializePrologueENS0_13SnapshotSpaceEiNS0_3MapE
	.type	_ZN2v88internal10Serializer16ObjectSerializer17SerializePrologueENS0_13SnapshotSpaceEiNS0_3MapE, @function
_ZN2v88internal10Serializer16ObjectSerializer17SerializePrologueENS0_13SnapshotSpaceEiNS0_3MapE:
.LFB21316:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	8(%rdi), %rcx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	152(%rcx), %rsi
	testq	%rsi, %rsi
	je	.L733
	movq	16(%rdi), %rax
	leaq	-1(%rax), %rdx
	movl	%edx, %eax
	sall	$15, %eax
	subl	%edx, %eax
	subl	$1, %eax
	movl	%eax, %edi
	shrl	$12, %edi
	xorl	%edi, %eax
	leal	(%rax,%rax,4), %eax
	movl	%eax, %edi
	shrl	$4, %edi
	xorl	%edi, %eax
	movq	24(%rsi), %rdi
	movl	32(%rsi), %esi
	imull	$2057, %eax, %eax
	subl	$1, %esi
	movl	%eax, %r9d
	shrl	$16, %r9d
	xorl	%r9d, %eax
	andl	%esi, %eax
	andl	$1073741823, %eax
	jmp	.L758
	.p2align 4,,10
	.p2align 3
.L759:
	cmpq	%r14, %rdx
	je	.L735
	addq	$1, %rax
	andq	%rsi, %rax
.L758:
	leaq	(%rax,%rax,2), %r9
	leaq	(%rdi,%r9,8), %r9
	movq	(%r9), %r14
	testq	%r14, %r14
	jne	.L759
.L734:
	movq	104(%rcx), %rax
	movl	%r8d, -88(%rbp)
	movq	41016(%rax), %r15
	movq	%r15, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	movl	-88(%rbp), %r8d
	testb	%al, %al
	jne	.L760
.L733:
	cmpl	$5, %r8d
	je	.L761
	cmpl	$4, %r8d
	je	.L762
	movq	16(%rbx), %rax
	movl	%r8d, %esi
	movl	%r12d, %edx
	movq	-1(%rax), %rax
	movq	8(%rbx), %rax
	movl	%r8d, -88(%rbp)
	leaq	216(%rax), %rdi
	call	_ZN2v88internal19SerializerAllocator8AllocateENS0_13SnapshotSpaceEj@PLT
	movq	24(%rbx), %rdi
	movl	-88(%rbp), %r8d
	movq	%rax, %r14
	movl	%eax, %r15d
	movb	%r8b, -64(%rbp)
	shrq	$32, %r14
	movq	8(%rdi), %rsi
	cmpq	16(%rdi), %rsi
	je	.L746
	movb	%r8b, (%rsi)
	leaq	-64(%rbp), %r8
	addq	$1, 8(%rdi)
.L747:
	movq	24(%rbx), %rdi
	sarl	$3, %r12d
	leaq	.LC30(%rip), %rdx
	movq	%r8, -88(%rbp)
	movslq	%r12d, %rsi
	call	_ZN2v88internal16SnapshotByteSink6PutIntEmPKc@PLT
	movq	-88(%rbp), %r8
.L742:
	movq	8(%rbx), %rax
	movq	16(%rbx), %rdx
	movq	%r8, %rsi
	leaq	-65(%rbp), %rcx
	leaq	112(%rax), %rdi
	movq	%rdx, -64(%rbp)
	call	_ZN2v84base19TemplateHashMapImplImNS_8internal19SerializerReferenceENS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE14LookupOrInsertIZNS7_14LookupOrInsertERKmjS6_EUlvE_EEPNS0_20TemplateHashMapEntryImS3_EESA_jRKT_S6_
	movq	%r13, %rsi
	movl	%r15d, 8(%rax)
	movl	%r14d, 12(%rax)
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	call	*40(%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L763
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L735:
	.cfi_restore_state
	movq	8(%r9), %r14
	jmp	.L734
	.p2align 4,,10
	.p2align 3
.L762:
	movq	8(%rbx), %rax
	leaq	216(%rax), %rdi
	call	_ZN2v88internal19SerializerAllocator11AllocateMapEv@PLT
	movq	24(%rbx), %rdi
	movb	$4, -64(%rbp)
	movq	%rax, %r14
	movl	%eax, %r15d
	movq	8(%rdi), %rsi
	shrq	$32, %r14
	cmpq	16(%rdi), %rsi
	je	.L746
	movb	$4, (%rsi)
	leaq	-64(%rbp), %r8
	addq	$1, 8(%rdi)
	jmp	.L747
	.p2align 4,,10
	.p2align 3
.L761:
	movq	24(%rbx), %rdi
	movb	$5, -64(%rbp)
	movq	8(%rdi), %rsi
	cmpq	16(%rdi), %rsi
	je	.L739
	movb	$5, (%rsi)
	addq	$1, 8(%rdi)
.L740:
	movl	%r12d, %esi
	movq	24(%rbx), %rdi
	leaq	.LC30(%rip), %rdx
	sarl	$3, %esi
	movslq	%esi, %rsi
	call	_ZN2v88internal16SnapshotByteSink6PutIntEmPKc@PLT
	movq	16(%rbx), %rax
	movq	-1(%rax), %rax
	cmpw	$69, 11(%rax)
	je	.L764
	movq	8(%rbx), %rax
	movl	%r12d, %esi
	leaq	216(%rax), %rdi
	call	_ZN2v88internal19SerializerAllocator19AllocateLargeObjectEj@PLT
	leaq	-64(%rbp), %r8
	movq	%rax, %r14
	movl	%eax, %r15d
	shrq	$32, %r14
	jmp	.L742
	.p2align 4,,10
	.p2align 3
.L760:
	movq	24(%rbx), %rax
	movq	%r14, %rcx
	movq	%r15, %rdi
	movq	8(%rax), %rdx
	subq	(%rax), %rdx
	movq	16(%rbx), %rax
	leaq	-1(%rax), %rsi
	call	_ZN2v88internal6Logger13CodeNameEventEmiPKc@PLT
	movl	-88(%rbp), %r8d
	jmp	.L733
	.p2align 4,,10
	.p2align 3
.L746:
	leaq	-64(%rbp), %r8
	movq	%r8, %rdx
	movq	%r8, -88(%rbp)
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movq	-88(%rbp), %r8
	jmp	.L747
	.p2align 4,,10
	.p2align 3
.L739:
	leaq	-64(%rbp), %r8
	movq	%r8, %rdx
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	jmp	.L740
	.p2align 4,,10
	.p2align 3
.L764:
	leaq	.LC31(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L763:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21316:
	.size	_ZN2v88internal10Serializer16ObjectSerializer17SerializePrologueENS0_13SnapshotSpaceEiNS0_3MapE, .-_ZN2v88internal10Serializer16ObjectSerializer17SerializePrologueENS0_13SnapshotSpaceEiNS0_3MapE
	.section	.rodata._ZN2v88internal10Serializer16ObjectSerializer41SerializeExternalStringAsSequentialStringEv.str1.1,"aMS",@progbits,1
.LC32:
	.string	"StringContent"
	.section	.text._ZN2v88internal10Serializer16ObjectSerializer41SerializeExternalStringAsSequentialStringEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10Serializer16ObjectSerializer41SerializeExternalStringAsSequentialStringEv
	.type	_ZN2v88internal10Serializer16ObjectSerializer41SerializeExternalStringAsSequentialStringEv, @function
_ZN2v88internal10Serializer16ObjectSerializer41SerializeExternalStringAsSequentialStringEv:
.LFB21321:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	104(%rax), %rsi
	movq	16(%rdi), %rax
	movl	11(%rax), %r14d
	leaq	-1(%rax), %rbx
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	movq	-1(%rax), %rcx
	andl	$-32, %edx
	cmpw	$63, 11(%rcx)
	jbe	.L802
.L766:
	testl	%edx, %edx
	je	.L803
	movq	744(%rsi), %rcx
.L772:
	movq	15(%rax), %rdi
	leal	16(%r14,%r14), %r14d
	movq	%rcx, -72(%rbp)
	leal	7(%r14), %r13d
	subl	$16, %r14d
	movq	(%rdi), %rax
	andl	$-8, %r13d
	call	*48(%rax)
	movq	-72(%rbp), %rcx
	movq	%rax, %r15
.L770:
	xorl	%esi, %esi
	cmpl	$131072, %r13d
	movq	%r12, %rdi
	movl	%r13d, %edx
	setg	%sil
	leal	2(%rsi,%rsi,2), %esi
	call	_ZN2v88internal10Serializer16ObjectSerializer17SerializePrologueENS0_13SnapshotSpaceEiNS0_3MapE
	movq	24(%r12), %rdi
	movb	$31, -57(%rbp)
	leal	-8(%r13), %eax
	movq	8(%rdi), %rsi
	cmpq	16(%rdi), %rsi
	je	.L774
	movb	$31, (%rsi)
	addq	$1, 8(%rdi)
.L775:
	movq	24(%r12), %rdi
	movslq	%eax, %rsi
	leaq	.LC19(%rip), %rdx
	call	_ZN2v88internal16SnapshotByteSink6PutIntEmPKc@PLT
	movq	24(%r12), %rdi
	movzbl	8(%rbx), %eax
	movb	%al, -57(%rbp)
	movq	8(%rdi), %rsi
	cmpq	%rsi, 16(%rdi)
	je	.L804
	movb	%al, (%rsi)
	addq	$1, 8(%rdi)
.L777:
	movq	24(%r12), %rdi
	movzbl	9(%rbx), %eax
	movb	%al, -57(%rbp)
	movq	8(%rdi), %rsi
	cmpq	16(%rdi), %rsi
	je	.L805
	movb	%al, (%rsi)
	addq	$1, 8(%rdi)
.L779:
	movq	24(%r12), %rdi
	movzbl	10(%rbx), %eax
	movb	%al, -57(%rbp)
	movq	8(%rdi), %rsi
	cmpq	16(%rdi), %rsi
	je	.L806
	movb	%al, (%rsi)
	addq	$1, 8(%rdi)
.L781:
	movq	24(%r12), %rdi
	movzbl	11(%rbx), %eax
	movb	%al, -57(%rbp)
	movq	8(%rdi), %rsi
	cmpq	16(%rdi), %rsi
	je	.L807
	movb	%al, (%rsi)
	addq	$1, 8(%rdi)
.L783:
	movq	24(%r12), %rdi
	movzbl	12(%rbx), %eax
	movb	%al, -57(%rbp)
	movq	8(%rdi), %rsi
	cmpq	16(%rdi), %rsi
	je	.L808
	movb	%al, (%rsi)
	addq	$1, 8(%rdi)
.L785:
	movq	24(%r12), %rdi
	movzbl	13(%rbx), %eax
	movb	%al, -57(%rbp)
	movq	8(%rdi), %rsi
	cmpq	16(%rdi), %rsi
	je	.L809
	movb	%al, (%rsi)
	addq	$1, 8(%rdi)
.L787:
	movq	24(%r12), %rdi
	movzbl	14(%rbx), %eax
	movb	%al, -57(%rbp)
	movq	8(%rdi), %rsi
	cmpq	16(%rdi), %rsi
	je	.L810
	movb	%al, (%rsi)
	addq	$1, 8(%rdi)
.L789:
	movq	24(%r12), %rdi
	movzbl	15(%rbx), %eax
	movb	%al, -57(%rbp)
	movq	8(%rdi), %rsi
	cmpq	16(%rdi), %rsi
	je	.L790
	movb	%al, (%rsi)
	addq	$1, 8(%rdi)
.L791:
	movq	24(%r12), %rdi
	subl	$16, %r13d
	movl	%r14d, %edx
	movq	%r15, %rsi
	leaq	.LC32(%rip), %rcx
	subl	%r14d, %r13d
	call	_ZN2v88internal16SnapshotByteSink6PutRawEPKhiPKc@PLT
	testl	%r13d, %r13d
	jle	.L765
	xorl	%ebx, %ebx
	leaq	-57(%rbp), %r14
	.p2align 4,,10
	.p2align 3
.L795:
	movq	24(%r12), %rdi
	movb	$0, -57(%rbp)
	movq	8(%rdi), %rsi
	cmpq	16(%rdi), %rsi
	je	.L793
	addl	$1, %ebx
	movb	$0, (%rsi)
	addq	$1, 8(%rdi)
	cmpl	%ebx, %r13d
	jne	.L795
.L765:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L811
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L803:
	.cfi_restore_state
	movq	824(%rsi), %rcx
	jmp	.L772
	.p2align 4,,10
	.p2align 3
.L793:
	movq	%r14, %rdx
	addl	$1, %ebx
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	cmpl	%ebx, %r13d
	jne	.L795
	jmp	.L765
	.p2align 4,,10
	.p2align 3
.L802:
	movq	-1(%rax), %rcx
	movzwl	11(%rcx), %ecx
	andl	$7, %ecx
	cmpw	$2, %cx
	jne	.L766
	movq	-1(%rax), %rcx
	testb	$8, 11(%rcx)
	je	.L766
	testl	%edx, %edx
	je	.L812
	movq	184(%rsi), %rcx
.L768:
	movq	15(%rax), %rdi
	leal	23(%r14), %r13d
	leaq	_ZNK2v88internal29NativesExternalStringResource4dataEv(%rip), %rdx
	andl	$-8, %r13d
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L769
	movq	8(%rdi), %r15
	jmp	.L770
	.p2align 4,,10
	.p2align 3
.L790:
	leaq	-57(%rbp), %rdx
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	jmp	.L791
	.p2align 4,,10
	.p2align 3
.L810:
	leaq	-57(%rbp), %rdx
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	jmp	.L789
	.p2align 4,,10
	.p2align 3
.L809:
	leaq	-57(%rbp), %rdx
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	jmp	.L787
	.p2align 4,,10
	.p2align 3
.L808:
	leaq	-57(%rbp), %rdx
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	jmp	.L785
	.p2align 4,,10
	.p2align 3
.L807:
	leaq	-57(%rbp), %rdx
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	jmp	.L783
	.p2align 4,,10
	.p2align 3
.L806:
	leaq	-57(%rbp), %rdx
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	jmp	.L781
	.p2align 4,,10
	.p2align 3
.L805:
	leaq	-57(%rbp), %rdx
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	jmp	.L779
	.p2align 4,,10
	.p2align 3
.L804:
	leaq	-57(%rbp), %rdx
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	jmp	.L777
	.p2align 4,,10
	.p2align 3
.L774:
	leaq	-57(%rbp), %rdx
	movl	%eax, -72(%rbp)
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movl	-72(%rbp), %eax
	jmp	.L775
.L812:
	movq	192(%rsi), %rcx
	jmp	.L768
.L769:
	movq	%rcx, -72(%rbp)
	call	*%rax
	movq	-72(%rbp), %rcx
	movq	%rax, %r15
	jmp	.L770
.L811:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21321:
	.size	_ZN2v88internal10Serializer16ObjectSerializer41SerializeExternalStringAsSequentialStringEv, .-_ZN2v88internal10Serializer16ObjectSerializer41SerializeExternalStringAsSequentialStringEv
	.section	.text._ZN2v88internal10Serializer16ObjectSerializer15SerializeObjectEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10Serializer16ObjectSerializer15SerializeObjectEv
	.type	_ZN2v88internal10Serializer16ObjectSerializer15SerializeObjectEv, @function
_ZN2v88internal10Serializer16ObjectSerializer15SerializeObjectEv:
.LFB21329:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	-1(%rax), %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	16(%r12), %rdi
	movl	%eax, %r13d
	movq	-1(%rdi), %r14
	call	_ZN2v88internal12ReadOnlyHeap8ContainsENS0_10HeapObjectE@PLT
	xorl	%esi, %esi
	testb	%al, %al
	jne	.L814
	movq	16(%r12), %rax
	andq	$-262144, %rax
	testb	$32, 10(%rax)
	je	.L863
.L814:
	movq	%r14, %rcx
	movl	%r13d, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal10Serializer16ObjectSerializer17SerializePrologueENS0_13SnapshotSpaceEiNS0_3MapE
	movl	32(%r12), %eax
	testl	%eax, %eax
	jne	.L864
	movl	$8, 32(%r12)
	movq	8(%r12), %rbx
	movl	208(%rbx), %eax
	addl	$1, %eax
	movl	%eax, 208(%rbx)
	cmpl	$31, %eax
	jg	.L817
.L821:
	movq	8(%r12), %rdi
	leaq	_ZN2v88internal10Serializer14MustBeDeferredENS0_10HeapObjectE(%rip), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L865
.L818:
	movl	%r13d, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10Serializer16ObjectSerializer16SerializeContentENS0_3MapEi
	subl	$1, 208(%rbx)
.L813:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L866
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L863:
	.cfi_restore_state
	movq	80(%rax), %rax
	movl	72(%rax), %esi
	movl	$5, %eax
	cmpl	$7, %esi
	cmove	%eax, %esi
	jmp	.L814
	.p2align 4,,10
	.p2align 3
.L817:
	movq	16(%r12), %rdi
	call	_ZN2v88internal22SerializerDeserializer13CanBeDeferredENS0_10HeapObjectE@PLT
	testb	%al, %al
	je	.L821
.L820:
	movq	8(%r12), %r13
	movq	16(%r12), %rdx
	movq	192(%r13), %r14
	cmpq	200(%r13), %r14
	je	.L823
	movq	%rdx, (%r14)
	addq	$8, 192(%r13)
.L824:
	movq	24(%r12), %rdi
	movb	$22, -57(%rbp)
	movq	8(%rdi), %rsi
	cmpq	16(%rdi), %rsi
	je	.L835
	movb	$22, (%rsi)
	addq	$1, 8(%rdi)
.L836:
	subl	$1, 208(%rbx)
	jmp	.L813
	.p2align 4,,10
	.p2align 3
.L865:
	movq	16(%r12), %rsi
	call	*%rax
	testb	%al, %al
	jne	.L820
	jmp	.L818
	.p2align 4,,10
	.p2align 3
.L864:
	leaq	.LC28(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L835:
	leaq	-57(%rbp), %rdx
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	jmp	.L836
	.p2align 4,,10
	.p2align 3
.L823:
	movq	184(%r13), %r8
	movq	%r14, %rcx
	movabsq	$1152921504606846975, %r15
	subq	%r8, %rcx
	movq	%rcx, %rax
	sarq	$3, %rax
	cmpq	%r15, %rax
	je	.L867
	testq	%rax, %rax
	je	.L868
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	ja	.L869
	testq	%rsi, %rsi
	jne	.L870
	movq	$8, -72(%rbp)
	xorl	%r15d, %r15d
	xorl	%eax, %eax
.L828:
	movq	%rdx, (%rax,%rcx)
	cmpq	%r8, %r14
	je	.L829
	leaq	-8(%r14), %rdi
	leaq	15(%rax), %rdx
	subq	%r8, %rdi
	subq	%r8, %rdx
	movq	%rdi, %r9
	shrq	$3, %r9
	cmpq	$30, %rdx
	jbe	.L844
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %r9
	je	.L844
	addq	$1, %r9
	xorl	%edx, %edx
	movq	%r9, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L831:
	movdqu	(%r8,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rcx
	jne	.L831
	movq	%r9, %r10
	andq	$-2, %r10
	leaq	0(,%r10,8), %rcx
	leaq	(%r8,%rcx), %rdx
	addq	%rax, %rcx
	cmpq	%r9, %r10
	je	.L833
	movq	(%rdx), %rdx
	movq	%rdx, (%rcx)
.L833:
	leaq	16(%rax,%rdi), %rcx
	movq	%rcx, -72(%rbp)
.L829:
	testq	%r8, %r8
	je	.L834
	movq	%r8, %rdi
	movq	%rax, -80(%rbp)
	call	_ZdlPv@PLT
	movq	-80(%rbp), %rax
.L834:
	movq	%rax, %xmm0
	movq	%r15, 200(%r13)
	movhps	-72(%rbp), %xmm0
	movups	%xmm0, 184(%r13)
	jmp	.L824
	.p2align 4,,10
	.p2align 3
.L869:
	movabsq	$9223372036854775800, %r15
.L826:
	movq	%r15, %rdi
	movq	%rdx, -96(%rbp)
	movq	%rcx, -88(%rbp)
	movq	%r8, -80(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %r8
	movq	-96(%rbp), %rdx
	leaq	8(%rax), %rcx
	addq	%rax, %r15
	movq	%rcx, -72(%rbp)
	movq	-88(%rbp), %rcx
	jmp	.L828
	.p2align 4,,10
	.p2align 3
.L868:
	movl	$8, %r15d
	jmp	.L826
	.p2align 4,,10
	.p2align 3
.L844:
	movq	%rax, %rcx
	movq	%r8, %rdx
	.p2align 4,,10
	.p2align 3
.L830:
	movq	(%rdx), %r9
	addq	$8, %rdx
	addq	$8, %rcx
	movq	%r9, -8(%rcx)
	cmpq	%r14, %rdx
	jne	.L830
	jmp	.L833
	.p2align 4,,10
	.p2align 3
.L870:
	cmpq	%r15, %rsi
	cmova	%r15, %rsi
	leaq	0(,%rsi,8), %r15
	jmp	.L826
.L866:
	call	__stack_chk_fail@PLT
.L867:
	leaq	.LC10(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE21329:
	.size	_ZN2v88internal10Serializer16ObjectSerializer15SerializeObjectEv, .-_ZN2v88internal10Serializer16ObjectSerializer15SerializeObjectEv
	.section	.text._ZN2v88internal10Serializer16ObjectSerializer23SerializeExternalStringEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10Serializer16ObjectSerializer23SerializeExternalStringEv
	.type	_ZN2v88internal10Serializer16ObjectSerializer23SerializeExternalStringEv, @function
_ZN2v88internal10Serializer16ObjectSerializer23SerializeExternalStringEv:
.LFB21320:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	8(%rdi), %rdi
	movq	16(%r12), %rbx
	movq	104(%rdi), %rdx
	movq	-1(%rbx), %rax
	movq	15(%rbx), %r13
	cmpq	%rax, 736(%rdx)
	je	.L872
	addq	$136, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal24ExternalReferenceEncoder9TryEncodeEm@PLT
	testb	%al, %al
	je	.L873
	shrq	$32, %rax
	andl	$2147483647, %eax
	movq	%rax, 15(%rbx)
	movq	-1(%rbx), %rax
	testb	$16, 11(%rax)
	jne	.L874
	movq	$0, 23(%rbx)
.L874:
	movq	%r12, %rdi
	call	_ZN2v88internal10Serializer16ObjectSerializer15SerializeObjectEv
	movq	%r13, 15(%rbx)
	movq	-1(%rbx), %rax
	cmpw	$63, 11(%rax)
	jbe	.L899
.L875:
	movq	-1(%rbx), %rax
	testb	$16, 11(%rax)
	jne	.L871
	movq	15(%rbx), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
.L893:
	movq	%rax, 23(%rbx)
.L871:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L872:
	.cfi_restore_state
	movl	28(%r13), %eax
	leal	1(%rax,%rax), %eax
	cltq
	leaq	0(,%rax,8), %rdi
	movq	%rdi, 15(%rbx)
	movq	-1(%rbx), %rdx
	testb	$16, 11(%rdx)
	jne	.L884
	movq	0(,%rax,8), %rdx
	leaq	_ZNK2v88internal29NativesExternalStringResource4dataEv(%rip), %rcx
	movq	48(%rdx), %rdx
	cmpq	%rcx, %rdx
	jne	.L885
	movq	8(,%rax,8), %rax
.L886:
	movq	%rax, 23(%rbx)
.L884:
	movq	%r12, %rdi
	call	_ZN2v88internal10Serializer16ObjectSerializer15SerializeObjectEv
	movq	%r13, 15(%rbx)
	testq	%r13, %r13
	je	.L871
.L898:
	movq	-1(%rbx), %rax
	testb	$16, 11(%rax)
	jne	.L871
	movq	0(%r13), %rax
	leaq	_ZNK2v88internal29NativesExternalStringResource4dataEv(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L890
	movq	8(%r13), %rax
	jmp	.L893
	.p2align 4,,10
	.p2align 3
.L873:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal10Serializer16ObjectSerializer41SerializeExternalStringAsSequentialStringEv
	.p2align 4,,10
	.p2align 3
.L899:
	.cfi_restore_state
	movq	-1(%rbx), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$2, %ax
	jne	.L875
	movq	-1(%rbx), %rax
	testb	$8, 11(%rax)
	je	.L875
	jmp	.L898
	.p2align 4,,10
	.p2align 3
.L885:
	call	*%rdx
	jmp	.L886
	.p2align 4,,10
	.p2align 3
.L890:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L893
	.cfi_endproc
.LFE21320:
	.size	_ZN2v88internal10Serializer16ObjectSerializer23SerializeExternalStringEv, .-_ZN2v88internal10Serializer16ObjectSerializer23SerializeExternalStringEv
	.section	.rodata._ZN2v88internal10Serializer16ObjectSerializer21SerializeBackingStoreEPvi.str1.1,"aMS",@progbits,1
.LC33:
	.string	"BackingStore"
	.section	.text._ZN2v88internal10Serializer16ObjectSerializer21SerializeBackingStoreEPvi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10Serializer16ObjectSerializer21SerializeBackingStoreEPvi
	.type	_ZN2v88internal10Serializer16ObjectSerializer21SerializeBackingStoreEPvi, @function
_ZN2v88internal10Serializer16ObjectSerializer21SerializeBackingStoreEPvi:
.LFB21317:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movl	120(%rax), %edx
	movq	112(%rax), %rsi
	subl	$1, %edx
	movl	%edx, %eax
	andl	%ebx, %eax
	leaq	(%rax,%rax,2), %rcx
	leaq	(%rsi,%rcx,8), %rcx
	cmpb	$0, 20(%rcx)
	jne	.L903
	jmp	.L901
	.p2align 4,,10
	.p2align 3
.L911:
	addq	$1, %rax
	andq	%rdx, %rax
	leaq	(%rax,%rax,2), %rcx
	leaq	(%rsi,%rcx,8), %rcx
	cmpb	$0, 20(%rcx)
	je	.L901
.L903:
	cmpq	(%rcx), %rbx
	jne	.L911
	movl	8(%rcx), %eax
	movl	12(%rcx), %r14d
	movl	%eax, %edx
	andl	$15, %edx
	cmpl	$6, %edx
	je	.L912
.L906:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L913
	addq	$40, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L912:
	.cfi_restore_state
	shrl	$4, %eax
	jne	.L906
	.p2align 4,,10
	.p2align 3
.L901:
	movq	24(%r12), %rdi
	movb	$28, -64(%rbp)
	movq	8(%rdi), %rsi
	cmpq	16(%rdi), %rsi
	je	.L904
	movb	$28, (%rsi)
	leaq	-64(%rbp), %r15
	addq	$1, 8(%rdi)
.L905:
	movq	24(%r12), %rdi
	movslq	%r13d, %rsi
	leaq	.LC19(%rip), %rdx
	call	_ZN2v88internal16SnapshotByteSink6PutIntEmPKc@PLT
	movq	24(%r12), %rdi
	movl	%r13d, %edx
	movq	%rbx, %rsi
	leaq	.LC33(%rip), %rcx
	call	_ZN2v88internal16SnapshotByteSink6PutRawEPKhiPKc@PLT
	movq	8(%r12), %rax
	leaq	216(%rax), %rdi
	call	_ZN2v88internal19SerializerAllocator27AllocateOffHeapBackingStoreEv@PLT
	movq	8(%r12), %rdi
	leaq	-65(%rbp), %rcx
	movl	%ebx, %edx
	movq	%rax, %r13
	movq	%r15, %rsi
	shrq	$32, %rax
	movq	%rbx, -64(%rbp)
	addq	$112, %rdi
	movq	%rax, %r14
	call	_ZN2v84base19TemplateHashMapImplImNS_8internal19SerializerReferenceENS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE14LookupOrInsertIZNS7_14LookupOrInsertERKmjS6_EUlvE_EEPNS0_20TemplateHashMapEntryImS3_EESA_jRKT_S6_
	movq	%r13, 8(%rax)
	jmp	.L906
	.p2align 4,,10
	.p2align 3
.L904:
	leaq	-64(%rbp), %r15
	movq	%r15, %rdx
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	jmp	.L905
.L913:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21317:
	.size	_ZN2v88internal10Serializer16ObjectSerializer21SerializeBackingStoreEPvi, .-_ZN2v88internal10Serializer16ObjectSerializer21SerializeBackingStoreEPvi
	.section	.rodata._ZN2v88internal10Serializer16ObjectSerializer21SerializeJSTypedArrayEv.str1.8,"aMS",@progbits,1
	.align 8
.LC34:
	.string	"buffer.byte_length() <= Smi::kMaxValue"
	.align 8
.LC35:
	.string	"typed_array.byte_offset() <= Smi::kMaxValue"
	.section	.text._ZN2v88internal10Serializer16ObjectSerializer21SerializeJSTypedArrayEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10Serializer16ObjectSerializer21SerializeJSTypedArrayEv
	.type	_ZN2v88internal10Serializer16ObjectSerializer21SerializeJSTypedArrayEv, @function
_ZN2v88internal10Serializer16ObjectSerializer21SerializeJSTypedArrayEv:
.LFB21318:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	16(%rdi), %rbx
	movq	23(%rbx), %rax
	movl	39(%rax), %edx
	andl	$4, %edx
	jne	.L915
	movq	15(%rbx), %rcx
	cmpq	%rcx, 63(%rbx)
	je	.L916
	movq	23(%rax), %rdx
	movl	$2147483648, %eax
	cmpq	%rax, %rdx
	jb	.L922
	leaq	.LC34(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L915:
	movq	$0, 55(%rbx)
.L916:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal10Serializer16ObjectSerializer15SerializeObjectEv
	.p2align 4,,10
	.p2align 3
.L922:
	.cfi_restore_state
	movq	31(%rbx), %rcx
	cmpq	%rax, %rcx
	jb	.L923
	leaq	.LC35(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L923:
	movq	55(%rbx), %rsi
	movq	%rdi, -24(%rbp)
	subq	%rcx, %rsi
	call	_ZN2v88internal10Serializer16ObjectSerializer21SerializeBackingStoreEPvi
	movq	-24(%rbp), %rdi
	salq	$32, %rax
	movq	%rax, 55(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal10Serializer16ObjectSerializer15SerializeObjectEv
	.cfi_endproc
.LFE21318:
	.size	_ZN2v88internal10Serializer16ObjectSerializer21SerializeJSTypedArrayEv, .-_ZN2v88internal10Serializer16ObjectSerializer21SerializeJSTypedArrayEv
	.section	.text._ZN2v88internal10Serializer16ObjectSerializer22SerializeJSArrayBufferEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10Serializer16ObjectSerializer22SerializeJSArrayBufferEv
	.type	_ZN2v88internal10Serializer16ObjectSerializer22SerializeJSArrayBufferEv, @function
_ZN2v88internal10Serializer16ObjectSerializer22SerializeJSArrayBufferEv:
.LFB21319:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 13, -24
	.cfi_offset 3, -32
	movq	16(%rdi), %rbx
	movq	23(%rbx), %rdx
	movq	31(%rbx), %r13
	cmpq	$2147483647, %rdx
	jbe	.L929
	leaq	.LC34(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L929:
	testq	%r13, %r13
	jne	.L930
.L927:
	call	_ZN2v88internal10Serializer16ObjectSerializer15SerializeObjectEv
	movq	%r13, 31(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L930:
	.cfi_restore_state
	movq	%r13, %rsi
	movq	%rdi, -24(%rbp)
	call	_ZN2v88internal10Serializer16ObjectSerializer21SerializeBackingStoreEPvi
	movq	-24(%rbp), %rdi
	salq	$32, %rax
	movq	%rax, 31(%rbx)
	jmp	.L927
	.cfi_endproc
.LFE21319:
	.size	_ZN2v88internal10Serializer16ObjectSerializer22SerializeJSArrayBufferEv, .-_ZN2v88internal10Serializer16ObjectSerializer22SerializeJSArrayBufferEv
	.section	.rodata._ZN2v88internal10Serializer16ObjectSerializer9SerializeEv.str1.1,"aMS",@progbits,1
.LC36:
	.string	" Encoding heap object: "
	.section	.text._ZN2v88internal10Serializer16ObjectSerializer9SerializeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10Serializer16ObjectSerializer9SerializeEv
	.type	_ZN2v88internal10Serializer16ObjectSerializer9SerializeEv, @function
_ZN2v88internal10Serializer16ObjectSerializer9SerializeEv:
.LFB21328:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal21FLAG_trace_serializerE(%rip)
	jne	.L962
.L932:
	movq	16(%r12), %rax
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L963
.L933:
	movq	16(%r12), %rdi
	call	_ZN2v88internal12ReadOnlyHeap8ContainsENS0_10HeapObjectE@PLT
	movq	16(%r12), %r13
	testb	%al, %al
	je	.L935
.L936:
	movq	-1(%r13), %rax
	cmpw	$1086, 11(%rax)
	je	.L964
	movq	-1(%r13), %rax
	cmpw	$1059, 11(%rax)
	je	.L965
	movq	-1(%r13), %rax
	cmpw	$96, 11(%rax)
	je	.L966
.L943:
	movq	%r12, %rdi
	call	_ZN2v88internal10Serializer16ObjectSerializer15SerializeObjectEv
.L931:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L967
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L963:
	.cfi_restore_state
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$2, %ax
	jne	.L933
	movq	%r12, %rdi
	call	_ZN2v88internal10Serializer16ObjectSerializer23SerializeExternalStringEv
	jmp	.L931
	.p2align 4,,10
	.p2align 3
.L962:
	leaq	.LC36(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	stdout(%rip), %rsi
	leaq	16(%r12), %rdi
	call	_ZNK2v88internal6Object10ShortPrintEP8_IO_FILE@PLT
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L932
	.p2align 4,,10
	.p2align 3
.L935:
	movq	-1(%r13), %rax
	cmpw	$63, 11(%rax)
	jbe	.L968
.L937:
	movq	-1(%r13), %rax
	cmpw	$63, 11(%rax)
	ja	.L936
	movq	-1(%r13), %rax
	testb	$7, 11(%rax)
	jne	.L936
	movq	-1(%r13), %rax
	testb	$8, 11(%rax)
	jne	.L936
	leaq	-64(%rbp), %rdi
	movq	%r13, -64(%rbp)
	call	_ZN2v88internal16SeqTwoByteString13clear_paddingEv@PLT
	movq	16(%r12), %r13
	jmp	.L936
	.p2align 4,,10
	.p2align 3
.L968:
	movq	-1(%r13), %rax
	testb	$7, 11(%rax)
	jne	.L937
	movq	-1(%r13), %rax
	testb	$8, 11(%rax)
	je	.L937
	leaq	-64(%rbp), %rdi
	movq	%r13, -64(%rbp)
	call	_ZN2v88internal16SeqOneByteString13clear_paddingEv@PLT
	movq	16(%r12), %r13
	jmp	.L936
	.p2align 4,,10
	.p2align 3
.L966:
	movq	8(%r12), %rax
	leaq	55(%r13), %r15
	movq	104(%rax), %rax
	movq	88(%rax), %r14
	movq	%r14, 55(%r13)
	testb	$1, %r14b
	je	.L943
	movq	%r14, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L969
.L945:
	testb	$24, %al
	je	.L943
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L943
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L943
	.p2align 4,,10
	.p2align 3
.L964:
	movq	%r12, %rdi
	call	_ZN2v88internal10Serializer16ObjectSerializer21SerializeJSTypedArrayEv
	jmp	.L931
	.p2align 4,,10
	.p2align 3
.L965:
	movq	23(%r13), %rdx
	movq	31(%r13), %r14
	cmpq	$2147483647, %rdx
	jbe	.L941
	leaq	.LC34(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L941:
	testq	%r14, %r14
	jne	.L970
.L942:
	movq	%r12, %rdi
	call	_ZN2v88internal10Serializer16ObjectSerializer15SerializeObjectEv
	movq	%r14, 31(%r13)
	jmp	.L931
	.p2align 4,,10
	.p2align 3
.L969:
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	jmp	.L945
	.p2align 4,,10
	.p2align 3
.L970:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10Serializer16ObjectSerializer21SerializeBackingStoreEPvi
	salq	$32, %rax
	movq	%rax, 31(%r13)
	jmp	.L942
.L967:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21328:
	.size	_ZN2v88internal10Serializer16ObjectSerializer9SerializeEv, .-_ZN2v88internal10Serializer16ObjectSerializer9SerializeEv
	.section	.text._ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS0_23DefaultAllocationPolicyEE6ResizeES5_,"axG",@progbits,_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS0_23DefaultAllocationPolicyEE6ResizeES5_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS0_23DefaultAllocationPolicyEE6ResizeES5_
	.type	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS0_23DefaultAllocationPolicyEE6ResizeES5_, @function
_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS0_23DefaultAllocationPolicyEE6ResizeES5_:
.LFB25853:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	8(%rdi), %eax
	movq	(%rdi), %r12
	movl	12(%rdi), %r14d
	addl	%eax, %eax
	leaq	(%rax,%rax,2), %rdi
	movq	%rax, %rbx
	salq	$3, %rdi
	call	malloc@PLT
	movq	%rax, 0(%r13)
	testq	%rax, %rax
	je	.L1011
	movl	%ebx, 8(%r13)
	testl	%ebx, %ebx
	je	.L973
	movq	$0, (%rax)
	movl	$24, %edx
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L974:
	movq	0(%r13), %rcx
	addq	$1, %rax
	movq	$0, (%rcx,%rdx)
	movl	8(%r13), %ecx
	addq	$24, %rdx
	cmpq	%rax, %rcx
	ja	.L974
.L973:
	movl	$0, 12(%r13)
	movq	%r12, %r15
	testl	%r14d, %r14d
	je	.L981
.L975:
	movq	(%r15), %rsi
	testq	%rsi, %rsi
	jne	.L1012
.L976:
	movq	24(%r15), %rsi
	addq	$24, %r15
	testq	%rsi, %rsi
	je	.L976
.L1012:
	movl	8(%r13), %eax
	movl	16(%r15), %ebx
	movq	0(%r13), %r8
	leal	-1(%rax), %edi
	movl	%ebx, %eax
	andl	%edi, %eax
	jmp	.L1010
	.p2align 4,,10
	.p2align 3
.L1013:
	testq	%rdx, %rdx
	je	.L977
	addq	$1, %rax
	andq	%rdi, %rax
.L1010:
	leaq	(%rax,%rax,2), %rdx
	leaq	(%r8,%rdx,8), %rcx
	movq	(%rcx), %rdx
	cmpq	%rdx, %rsi
	jne	.L1013
.L977:
	movq	8(%r15), %rax
	movq	%rsi, (%rcx)
	movl	%ebx, 16(%rcx)
	movq	%rax, 8(%rcx)
	movl	12(%r13), %eax
	addl	$1, %eax
	movl	%eax, %edx
	movl	%eax, 12(%r13)
	shrl	$2, %edx
	addl	%edx, %eax
	cmpl	8(%r13), %eax
	jnb	.L980
.L983:
	addq	$24, %r15
	subl	$1, %r14d
	jne	.L975
.L981:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	free@PLT
	.p2align 4,,10
	.p2align 3
.L980:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS0_23DefaultAllocationPolicyEE6ResizeES5_
	movl	8(%r13), %eax
	movq	0(%r13), %rsi
	leal	-1(%rax), %ecx
	movl	%ebx, %eax
	andl	%ecx, %eax
	leaq	(%rax,%rax,2), %rdx
	movq	(%rsi,%rdx,8), %rdx
	testq	%rdx, %rdx
	je	.L983
	movq	(%r15), %rdi
	jmp	.L984
	.p2align 4,,10
	.p2align 3
.L1014:
	addq	$1, %rax
	andq	%rcx, %rax
	leaq	(%rax,%rax,2), %rdx
	movq	(%rsi,%rdx,8), %rdx
	testq	%rdx, %rdx
	je	.L983
.L984:
	cmpq	%rdx, %rdi
	jne	.L1014
	jmp	.L983
.L1011:
	leaq	.LC3(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE25853:
	.size	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS0_23DefaultAllocationPolicyEE6ResizeES5_, .-_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS0_23DefaultAllocationPolicyEE6ResizeES5_
	.section	.text._ZN2v88internal14CodeAddressMap13CodeMoveEventENS0_12AbstractCodeES2_,"axG",@progbits,_ZN2v88internal14CodeAddressMap13CodeMoveEventENS0_12AbstractCodeES2_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14CodeAddressMap13CodeMoveEventENS0_12AbstractCodeES2_
	.type	_ZN2v88internal14CodeAddressMap13CodeMoveEventENS0_12AbstractCodeES2_, @function
_ZN2v88internal14CodeAddressMap13CodeMoveEventENS0_12AbstractCodeES2_:
.LFB8920:
	.cfi_startproc
	endbr64
	cmpq	%rdx, %rsi
	je	.L1045
	subq	$1, %rsi
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %eax
	sall	$15, %eax
	subl	%esi, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	subl	$1, %eax
	pushq	%r14
	movl	%eax, %ecx
	pushq	%r13
	shrl	$12, %ecx
	pushq	%r12
	xorl	%ecx, %eax
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leal	(%rax,%rax,4), %eax
	movl	%eax, %ecx
	shrl	$4, %ecx
	subq	$8, %rsp
	movq	24(%rbx), %r9
	xorl	%ecx, %eax
	movl	32(%rbx), %ecx
	imull	$2057, %eax, %eax
	subl	$1, %ecx
	movl	%eax, %edi
	shrl	$16, %edi
	xorl	%edi, %eax
	andl	%ecx, %eax
	andl	$1073741823, %eax
	jmp	.L1048
	.p2align 4,,10
	.p2align 3
.L1052:
	addq	$1, %rax
	andq	%rcx, %rax
.L1048:
	leaq	(%rax,%rax,2), %rdi
	leaq	(%r9,%rdi,8), %r8
	movq	(%r8), %rdi
	testq	%rdi, %rdi
	je	.L1017
	cmpq	%rdi, %rsi
	jne	.L1052
	leaq	-1(%rdx), %r13
	leaq	24(%rbx), %r15
	movl	16(%r8), %edx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	8(%r8), %r14
	call	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS0_23DefaultAllocationPolicyEE6RemoveERKS2_j
	movl	%r13d, %eax
	movq	24(%rbx), %rdi
	sall	$15, %eax
	subl	%r13d, %eax
	subl	$1, %eax
	movl	%eax, %edx
	shrl	$12, %edx
	xorl	%edx, %eax
	leal	(%rax,%rax,4), %r12d
	movl	%r12d, %eax
	shrl	$4, %eax
	xorl	%eax, %r12d
	imull	$2057, %r12d, %r12d
	movl	%r12d, %eax
	shrl	$16, %eax
	xorl	%eax, %r12d
	movl	32(%rbx), %eax
	andl	$1073741823, %r12d
	leal	-1(%rax), %esi
	movl	%r12d, %eax
	andl	%esi, %eax
	jmp	.L1049
	.p2align 4,,10
	.p2align 3
.L1053:
	cmpq	%rcx, %r13
	je	.L1021
	addq	$1, %rax
	andq	%rsi, %rax
.L1049:
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rdi,%rdx,8), %rdx
	movq	(%rdx), %rcx
	testq	%rcx, %rcx
	jne	.L1053
	movq	%r13, (%rdx)
	movq	$0, 8(%rdx)
	movl	%r12d, 16(%rdx)
	movl	36(%rbx), %eax
	addl	$1, %eax
	movl	%eax, %ecx
	movl	%eax, 36(%rbx)
	shrl	$2, %ecx
	addl	%ecx, %eax
	cmpl	32(%rbx), %eax
	jnb	.L1054
.L1021:
	movq	%r14, 8(%rdx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1045:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L1054:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	%r15, %rdi
	call	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS0_23DefaultAllocationPolicyEE6ResizeES5_
	movl	32(%rbx), %eax
	movq	24(%rbx), %rdi
	leal	-1(%rax), %esi
	andl	%esi, %r12d
	movl	%r12d, %eax
	jmp	.L1051
	.p2align 4,,10
	.p2align 3
.L1055:
	cmpq	%r13, %rcx
	je	.L1021
	addq	$1, %rax
	andq	%rsi, %rax
.L1051:
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rdi,%rdx,8), %rdx
	movq	(%rdx), %rcx
	testq	%rcx, %rcx
	jne	.L1055
	jmp	.L1021
.L1017:
	movq	8, %rax
	ud2
	.cfi_endproc
.LFE8920:
	.size	_ZN2v88internal14CodeAddressMap13CodeMoveEventENS0_12AbstractCodeES2_, .-_ZN2v88internal14CodeAddressMap13CodeMoveEventENS0_12AbstractCodeES2_
	.section	.rodata._ZN2v88internal14CodeAddressMap17LogRecordedBufferENS0_12AbstractCodeENS0_18SharedFunctionInfoEPKci.str1.1,"aMS",@progbits,1
.LC37:
	.string	"NewArray"
	.section	.text._ZN2v88internal14CodeAddressMap17LogRecordedBufferENS0_12AbstractCodeENS0_18SharedFunctionInfoEPKci,"axG",@progbits,_ZN2v88internal14CodeAddressMap17LogRecordedBufferENS0_12AbstractCodeENS0_18SharedFunctionInfoEPKci,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14CodeAddressMap17LogRecordedBufferENS0_12AbstractCodeENS0_18SharedFunctionInfoEPKci
	.type	_ZN2v88internal14CodeAddressMap17LogRecordedBufferENS0_12AbstractCodeENS0_18SharedFunctionInfoEPKci, @function
_ZN2v88internal14CodeAddressMap17LogRecordedBufferENS0_12AbstractCodeENS0_18SharedFunctionInfoEPKci:
.LFB8940:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-1(%rsi), %r14
	movl	%r14d, %eax
	pushq	%r13
	.cfi_offset 13, -40
	movslq	%r8d, %r13
	sall	$15, %eax
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	subl	%r14d, %eax
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subl	$1, %eax
	movl	%eax, %edx
	shrl	$12, %edx
	subq	$24, %rsp
	xorl	%edx, %eax
	leal	(%rax,%rax,4), %eax
	movl	%eax, %edx
	shrl	$4, %edx
	xorl	%edx, %eax
	imull	$2057, %eax, %eax
	movl	%eax, %edx
	shrl	$16, %edx
	xorl	%edx, %eax
	andl	$1073741823, %eax
	movl	%eax, %r15d
	movl	32(%rdi), %eax
	movq	24(%rdi), %rdi
	leal	-1(%rax), %edx
	movl	%r15d, %eax
	andl	%edx, %eax
	jmp	.L1117
	.p2align 4,,10
	.p2align 3
.L1118:
	cmpq	%rsi, %r14
	je	.L1058
	addq	$1, %rax
	andq	%rdx, %rax
.L1117:
	leaq	(%rax,%rax,2), %rcx
	leaq	(%rdi,%rcx,8), %rcx
	movq	(%rcx), %rsi
	testq	%rsi, %rsi
	jne	.L1118
	movq	%r14, (%rcx)
	movq	$0, 8(%rcx)
	movl	%r15d, 16(%rcx)
	movl	36(%rbx), %eax
	addl	$1, %eax
	movl	%eax, %edx
	movl	%eax, 36(%rbx)
	shrl	$2, %edx
	addl	%edx, %eax
	cmpl	32(%rbx), %eax
	jnb	.L1119
.L1058:
	cmpq	$0, 8(%rcx)
	je	.L1120
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1119:
	.cfi_restore_state
	leaq	24(%rbx), %rdi
	call	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS0_23DefaultAllocationPolicyEE6ResizeES5_
	movl	32(%rbx), %eax
	movq	24(%rbx), %rdi
	leal	-1(%rax), %esi
	movl	%r15d, %eax
	andl	%esi, %eax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rdi,%rdx,8), %rcx
	movq	(%rcx), %rdx
	cmpq	%r14, %rdx
	je	.L1058
	testq	%rdx, %rdx
	jne	.L1060
	jmp	.L1058
	.p2align 4,,10
	.p2align 3
.L1121:
	cmpq	%r14, %rdx
	je	.L1058
.L1060:
	addq	$1, %rax
	andq	%rsi, %rax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rdi,%rdx,8), %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.L1121
	jmp	.L1058
	.p2align 4,,10
	.p2align 3
.L1120:
	leal	1(%r13), %r14d
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%rcx, -56(%rbp)
	movslq	%r14d, %r14
	movq	%r14, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	-56(%rbp), %rcx
	testq	%rax, %rax
	je	.L1122
.L1063:
	testl	%r13d, %r13d
	jle	.L1068
	leal	-1(%r13), %edx
	cmpl	$14, %edx
	jbe	.L1086
	movl	%r13d, %esi
	movdqa	.LC38(%rip), %xmm3
	xorl	%edx, %edx
	pxor	%xmm2, %xmm2
	shrl	$4, %esi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L1067:
	movdqu	(%r12,%rdx), %xmm0
	movdqu	(%r12,%rdx), %xmm4
	movdqa	%xmm3, %xmm1
	pcmpeqb	%xmm2, %xmm0
	pand	%xmm0, %xmm1
	pandn	%xmm4, %xmm0
	por	%xmm1, %xmm0
	movups	%xmm0, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rsi, %rdx
	jne	.L1067
	movl	%r13d, %edx
	andl	$-16, %edx
	testb	$15, %r13b
	je	.L1068
.L1065:
	movslq	%edx, %r8
	movl	$32, %esi
	movzbl	(%r12,%r8), %edi
	testb	%dil, %dil
	cmove	%esi, %edi
	movb	%dil, (%rax,%r8)
	leal	1(%rdx), %edi
	cmpl	%edi, %r13d
	jle	.L1068
	movslq	%edi, %rdi
	movzbl	(%r12,%rdi), %r8d
	testb	%r8b, %r8b
	cmove	%esi, %r8d
	movb	%r8b, (%rax,%rdi)
	leal	2(%rdx), %edi
	cmpl	%edi, %r13d
	jle	.L1068
	movslq	%edi, %rdi
	movzbl	(%r12,%rdi), %r8d
	testb	%r8b, %r8b
	cmove	%esi, %r8d
	movb	%r8b, (%rax,%rdi)
	leal	3(%rdx), %edi
	cmpl	%edi, %r13d
	jle	.L1068
	movslq	%edi, %rdi
	movzbl	(%r12,%rdi), %r8d
	testb	%r8b, %r8b
	cmove	%esi, %r8d
	movb	%r8b, (%rax,%rdi)
	leal	4(%rdx), %edi
	cmpl	%edi, %r13d
	jle	.L1068
	movslq	%edi, %rdi
	movzbl	(%r12,%rdi), %r8d
	testb	%r8b, %r8b
	cmove	%esi, %r8d
	movb	%r8b, (%rax,%rdi)
	leal	5(%rdx), %edi
	cmpl	%edi, %r13d
	jle	.L1068
	movslq	%edi, %rdi
	movzbl	(%r12,%rdi), %r8d
	testb	%r8b, %r8b
	cmove	%esi, %r8d
	movb	%r8b, (%rax,%rdi)
	leal	6(%rdx), %edi
	cmpl	%edi, %r13d
	jle	.L1068
	movslq	%edi, %rdi
	movzbl	(%r12,%rdi), %r8d
	testb	%r8b, %r8b
	cmove	%esi, %r8d
	movb	%r8b, (%rax,%rdi)
	leal	7(%rdx), %edi
	cmpl	%edi, %r13d
	jle	.L1068
	movslq	%edi, %rdi
	movzbl	(%r12,%rdi), %r8d
	testb	%r8b, %r8b
	cmove	%esi, %r8d
	movb	%r8b, (%rax,%rdi)
	leal	8(%rdx), %edi
	cmpl	%edi, %r13d
	jle	.L1068
	movslq	%edi, %rdi
	movzbl	(%r12,%rdi), %r8d
	testb	%r8b, %r8b
	cmove	%esi, %r8d
	movb	%r8b, (%rax,%rdi)
	leal	9(%rdx), %edi
	cmpl	%edi, %r13d
	jle	.L1068
	movslq	%edi, %rdi
	movzbl	(%r12,%rdi), %r8d
	testb	%r8b, %r8b
	cmove	%esi, %r8d
	leal	10(%rdx), %esi
	movb	%r8b, (%rax,%rdi)
	cmpl	%esi, %r13d
	jle	.L1068
	movslq	%esi, %rsi
	movl	$32, %edi
	movzbl	(%r12,%rsi), %r8d
	testb	%r8b, %r8b
	cmove	%edi, %r8d
	movb	%r8b, (%rax,%rsi)
	leal	11(%rdx), %esi
	cmpl	%esi, %r13d
	jle	.L1068
	movslq	%esi, %rsi
	movzbl	(%r12,%rsi), %r8d
	testb	%r8b, %r8b
	cmove	%edi, %r8d
	movb	%r8b, (%rax,%rsi)
	leal	12(%rdx), %esi
	cmpl	%esi, %r13d
	jle	.L1068
	movslq	%esi, %rsi
	movzbl	(%r12,%rsi), %r8d
	testb	%r8b, %r8b
	cmove	%edi, %r8d
	movb	%r8b, (%rax,%rsi)
	leal	13(%rdx), %esi
	cmpl	%esi, %r13d
	jle	.L1068
	movslq	%esi, %rsi
	movzbl	(%r12,%rsi), %r8d
	testb	%r8b, %r8b
	cmove	%edi, %r8d
	addl	$14, %edx
	movb	%r8b, (%rax,%rsi)
	cmpl	%edx, %r13d
	jle	.L1068
	movslq	%edx, %rdx
	movzbl	(%r12,%rdx), %esi
	testb	%sil, %sil
	cmove	%edi, %esi
	movb	%sil, (%rax,%rdx)
.L1068:
	movb	$0, (%rax,%r13)
	movq	%rax, 8(%rcx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1086:
	.cfi_restore_state
	xorl	%edx, %edx
	jmp	.L1065
.L1122:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%r14, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	-56(%rbp), %rcx
	testq	%rax, %rax
	jne	.L1063
	leaq	.LC37(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
	.cfi_endproc
.LFE8940:
	.size	_ZN2v88internal14CodeAddressMap17LogRecordedBufferENS0_12AbstractCodeENS0_18SharedFunctionInfoEPKci, .-_ZN2v88internal14CodeAddressMap17LogRecordedBufferENS0_12AbstractCodeENS0_18SharedFunctionInfoEPKci
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal10SerializerC2EPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal10SerializerC2EPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal10SerializerC2EPNS0_7IsolateE:
.LFB26730:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE26730:
	.size	_GLOBAL__sub_I__ZN2v88internal10SerializerC2EPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal10SerializerC2EPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal10SerializerC2EPNS0_7IsolateE
	.weak	_ZTVN2v88internal14CodeAddressMapE
	.section	.data.rel.ro._ZTVN2v88internal14CodeAddressMapE,"awG",@progbits,_ZTVN2v88internal14CodeAddressMapE,comdat
	.align 8
	.type	_ZTVN2v88internal14CodeAddressMapE, @object
	.size	_ZTVN2v88internal14CodeAddressMapE, 176
_ZTVN2v88internal14CodeAddressMapE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal14CodeAddressMapD1Ev
	.quad	_ZN2v88internal14CodeAddressMapD0Ev
	.quad	_ZN2v88internal15CodeEventLogger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeEPKc
	.quad	_ZN2v88internal15CodeEventLogger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_4NameE
	.quad	_ZN2v88internal15CodeEventLogger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameE
	.quad	_ZN2v88internal15CodeEventLogger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameEii
	.quad	_ZN2v88internal15CodeEventLogger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsEPKNS0_4wasm8WasmCodeENS0_6VectorIKcEE
	.quad	_ZN2v88internal15CodeEventLogger13CallbackEventENS0_4NameEm
	.quad	_ZN2v88internal15CodeEventLogger19GetterCallbackEventENS0_4NameEm
	.quad	_ZN2v88internal15CodeEventLogger19SetterCallbackEventENS0_4NameEm
	.quad	_ZN2v88internal15CodeEventLogger21RegExpCodeCreateEventENS0_12AbstractCodeENS0_6StringE
	.quad	_ZN2v88internal14CodeAddressMap13CodeMoveEventENS0_12AbstractCodeES2_
	.quad	_ZN2v88internal15CodeEventLogger27SharedFunctionInfoMoveEventEmm
	.quad	_ZN2v88internal15CodeEventLogger22NativeContextMoveEventEmm
	.quad	_ZN2v88internal15CodeEventLogger17CodeMovingGCEventEv
	.quad	_ZN2v88internal14CodeAddressMap19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE
	.quad	_ZN2v88internal15CodeEventLogger14CodeDeoptEventENS0_4CodeENS0_14DeoptimizeKindEmi
	.quad	_ZN2v88internal17CodeEventListener27is_listening_to_code_eventsEv
	.quad	_ZN2v88internal14CodeAddressMap17LogRecordedBufferENS0_12AbstractCodeENS0_18SharedFunctionInfoEPKci
	.quad	_ZN2v88internal14CodeAddressMap17LogRecordedBufferEPKNS0_4wasm8WasmCodeEPKci
	.weak	_ZTVN2v88internal10SerializerE
	.section	.data.rel.ro._ZTVN2v88internal10SerializerE,"awG",@progbits,_ZTVN2v88internal10SerializerE,comdat
	.align 8
	.type	_ZTVN2v88internal10SerializerE, @object
	.size	_ZTVN2v88internal10SerializerE, 72
_ZTVN2v88internal10SerializerE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZN2v88internal10Serializer17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_
	.quad	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE
	.quad	_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE
	.quad	__cxa_pure_virtual
	.quad	_ZN2v88internal10Serializer14MustBeDeferredENS0_10HeapObjectE
	.weak	_ZTVN2v88internal10Serializer16ObjectSerializerE
	.section	.data.rel.ro._ZTVN2v88internal10Serializer16ObjectSerializerE,"awG",@progbits,_ZTVN2v88internal10Serializer16ObjectSerializerE,comdat
	.align 8
	.type	_ZTVN2v88internal10Serializer16ObjectSerializerE, @object
	.size	_ZTVN2v88internal10Serializer16ObjectSerializerE, 152
_ZTVN2v88internal10Serializer16ObjectSerializerE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal10Serializer16ObjectSerializerD1Ev
	.quad	_ZN2v88internal10Serializer16ObjectSerializerD0Ev
	.quad	_ZN2v88internal10Serializer16ObjectSerializer13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES4_
	.quad	_ZN2v88internal10Serializer16ObjectSerializer13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES4_
	.quad	_ZN2v88internal13ObjectVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	.quad	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE
	.quad	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE
	.quad	_ZN2v88internal13ObjectVisitor22VisitCustomWeakPointerENS0_10HeapObjectENS0_14FullObjectSlotE
	.quad	_ZN2v88internal13ObjectVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_
	.quad	_ZN2v88internal10Serializer16ObjectSerializer15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE
	.quad	_ZN2v88internal10Serializer16ObjectSerializer20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE
	.quad	_ZN2v88internal10Serializer16ObjectSerializer17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE
	.quad	_ZN2v88internal10Serializer16ObjectSerializer22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE
	.quad	_ZN2v88internal10Serializer16ObjectSerializer22VisitExternalReferenceENS0_7ForeignEPm
	.quad	_ZN2v88internal10Serializer16ObjectSerializer22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE
	.quad	_ZN2v88internal10Serializer16ObjectSerializer18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE
	.quad	_ZN2v88internal13ObjectVisitor14VisitRelocInfoEPNS0_13RelocIteratorE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC38:
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
