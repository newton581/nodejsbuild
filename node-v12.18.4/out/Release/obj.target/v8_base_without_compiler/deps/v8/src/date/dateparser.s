	.file	"dateparser.cc"
	.text
	.section	.text._ZN2v88internal10DateParser11DayComposer5WriteEPd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10DateParser11DayComposer5WriteEPd
	.type	_ZN2v88internal10DateParser11DayComposer5WriteEPd, @function
_ZN2v88internal10DateParser11DayComposer5WriteEPd:
.LFB17862:
	.cfi_startproc
	endbr64
	movslq	12(%rdi), %rax
	testl	%eax, %eax
	jle	.L14
	cmpl	$2, %eax
	jg	.L4
	movl	$1, (%rdi,%rax,4)
	je	.L5
	movl	$1, 8(%rdi)
.L5:
	movl	$3, 12(%rdi)
	movl	$3, %eax
.L4:
	movl	16(%rdi), %edx
	movzbl	20(%rdi), %r9d
	movl	(%rdi), %r8d
	movl	4(%rdi), %ecx
	cmpl	$2147483647, %edx
	je	.L20
	leal	-1(%r8), %eax
	cmpl	$30, %eax
	jbe	.L12
	movl	%ecx, %eax
	movl	%r8d, %ecx
	movl	%eax, %r8d
.L12:
	subl	$1, %edx
	testb	%r9b, %r9b
	jne	.L8
.L11:
	cmpl	$49, %ecx
	jbe	.L21
	leal	-50(%rcx), %edi
	leal	1900(%rcx), %eax
	cmpl	$50, %edi
	cmovb	%eax, %ecx
.L8:
	cmpl	$11, %edx
	ja	.L14
	leal	-1(%r8), %eax
	cmpl	$30, %eax
	ja	.L14
	pxor	%xmm0, %xmm0
	movl	$1, %eax
	cvtsi2sdl	%ecx, %xmm0
	movsd	%xmm0, (%rsi)
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%edx, %xmm0
	movsd	%xmm0, 8(%rsi)
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%r8d, %xmm0
	movsd	%xmm0, 16(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	testb	%r9b, %r9b
	jne	.L22
	leal	-1(%r8), %edx
	cmpl	$3, %eax
	je	.L23
	movl	%ecx, %r8d
	movl	$2000, %ecx
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L21:
	addl	$2000, %ecx
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L22:
	leal	-1(%rcx), %edx
	movl	%r8d, %ecx
	movl	8(%rdi), %r8d
	jmp	.L8
.L23:
	movl	8(%rdi), %eax
	cmpl	$30, %edx
	ja	.L10
	movl	%ecx, %r8d
	movl	%eax, %ecx
	jmp	.L11
.L10:
	leal	-1(%rcx), %edx
	movl	%r8d, %ecx
	movl	%eax, %r8d
	jmp	.L11
	.cfi_endproc
.LFE17862:
	.size	_ZN2v88internal10DateParser11DayComposer5WriteEPd, .-_ZN2v88internal10DateParser11DayComposer5WriteEPd
	.section	.text._ZN2v88internal10DateParser12TimeComposer5WriteEPd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10DateParser12TimeComposer5WriteEPd
	.type	_ZN2v88internal10DateParser12TimeComposer5WriteEPd, @function
_ZN2v88internal10DateParser12TimeComposer5WriteEPd:
.LFB17863:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movslq	16(%rdi), %rax
	movq	%rdi, %rbx
	cmpl	$3, %eax
	jg	.L25
	movl	$3, %edx
	leaq	(%rdi,%rax,4), %rdi
	xorl	%esi, %esi
	subl	%eax, %edx
	leaq	4(,%rdx,4), %rdx
	call	memset@PLT
	movl	$4, 16(%rbx)
.L25:
	movl	20(%rbx), %ecx
	movl	(%rbx), %eax
	cmpl	$2147483647, %ecx
	je	.L26
	cmpl	$12, %eax
	ja	.L30
	movslq	%eax, %rdx
	movl	%eax, %esi
	imulq	$715827883, %rdx, %rdx
	sarl	$31, %esi
	sarq	$33, %rdx
	subl	%esi, %edx
	leal	(%rdx,%rdx,2), %edx
	sall	$2, %edx
	subl	%edx, %eax
	addl	%ecx, %eax
	movl	%eax, (%rbx)
.L26:
	cmpl	$23, %eax
	jbe	.L36
	cmpl	$24, %eax
	jne	.L30
	movl	4(%rbx), %esi
	testl	%esi, %esi
	jne	.L30
	movl	8(%rbx), %ecx
	testl	%ecx, %ecx
	jne	.L30
	movl	12(%rbx), %edx
	testl	%edx, %edx
	jne	.L30
.L31:
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movl	$1, %eax
	movsd	%xmm0, 24(%r12)
	pxor	%xmm0, %xmm0
	cvtsi2sdl	4(%rbx), %xmm0
	movsd	%xmm0, 32(%r12)
	pxor	%xmm0, %xmm0
	cvtsi2sdl	8(%rbx), %xmm0
	movsd	%xmm0, 40(%r12)
	pxor	%xmm0, %xmm0
	cvtsi2sdl	12(%rbx), %xmm0
	popq	%rbx
	movsd	%xmm0, 48(%r12)
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	.cfi_restore_state
	cmpl	$59, 4(%rbx)
	ja	.L30
	cmpl	$59, 8(%rbx)
	ja	.L30
	cmpl	$999, 12(%rbx)
	jbe	.L31
.L30:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17863:
	.size	_ZN2v88internal10DateParser12TimeComposer5WriteEPd, .-_ZN2v88internal10DateParser12TimeComposer5WriteEPd
	.section	.text._ZN2v88internal10DateParser16TimeZoneComposer5WriteEPd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10DateParser16TimeZoneComposer5WriteEPd
	.type	_ZN2v88internal10DateParser16TimeZoneComposer5WriteEPd, @function
_ZN2v88internal10DateParser16TimeZoneComposer5WriteEPd:
.LFB17864:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	cmpl	$2147483647, %ecx
	je	.L38
	movl	4(%rdi), %eax
	cmpl	$2147483647, %eax
	je	.L39
	movl	8(%rdi), %edx
	imull	$3600, %eax, %eax
	cmpl	$2147483647, %edx
	je	.L41
.L46:
	imull	$60, %edx, %edx
	addl	%edx, %eax
.L42:
	xorl	%r8d, %r8d
	testl	%eax, %eax
	js	.L37
	movl	%eax, %edx
	pxor	%xmm0, %xmm0
	movl	$1, %r8d
	negl	%edx
	testl	%ecx, %ecx
	cmovs	%edx, %eax
	cvtsi2sdl	%eax, %xmm0
	movsd	%xmm0, 56(%rsi)
.L37:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	movl	8(%rdi), %edx
	movl	$0, 4(%rdi)
	xorl	%eax, %eax
	cmpl	$2147483647, %edx
	jne	.L46
.L41:
	movl	$0, 8(%rdi)
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L38:
	movq	.LC0(%rip), %rax
	movl	$1, %r8d
	movq	%rax, 56(%rsi)
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE17864:
	.size	_ZN2v88internal10DateParser16TimeZoneComposer5WriteEPd, .-_ZN2v88internal10DateParser16TimeZoneComposer5WriteEPd
	.section	.text._ZN2v88internal10DateParser12KeywordTable6LookupEPKji,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10DateParser12KeywordTable6LookupEPKji
	.type	_ZN2v88internal10DateParser12KeywordTable6LookupEPKji, @function
_ZN2v88internal10DateParser12KeywordTable6LookupEPKji:
.LFB17865:
	.cfi_startproc
	endbr64
	movl	(%rdi), %r9d
	leaq	_ZN2v88internal10DateParser12KeywordTable5arrayE(%rip), %rax
	xorl	%r8d, %r8d
	movl	$1, %edx
	.p2align 4,,10
	.p2align 3
.L50:
	movsbl	(%rax), %ecx
	cmpl	%ecx, %r9d
	jne	.L48
	movsbl	1(%rax), %ecx
	cmpl	%ecx, 4(%rdi)
	jne	.L48
	movsbl	2(%rax), %ecx
	cmpl	%ecx, 8(%rdi)
	jne	.L48
	cmpl	$3, %esi
	jle	.L47
	cmpb	$1, %dl
	je	.L47
.L48:
	movzbl	8(%rax), %edx
	addq	$5, %rax
	addl	$1, %r8d
	testb	%dl, %dl
	jne	.L50
.L47:
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE17865:
	.size	_ZN2v88internal10DateParser12KeywordTable6LookupEPKji, .-_ZN2v88internal10DateParser12KeywordTable6LookupEPKji
	.section	.text._ZN2v88internal10DateParser16ReadMillisecondsENS1_9DateTokenE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10DateParser16ReadMillisecondsENS1_9DateTokenE
	.type	_ZN2v88internal10DateParser16ReadMillisecondsENS1_9DateTokenE, @function
_ZN2v88internal10DateParser16ReadMillisecondsENS1_9DateTokenE:
.LFB17866:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	shrq	$32, %rdi
	movl	%esi, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpl	$2, %edi
	jg	.L53
	cmpl	$1, %edi
	je	.L66
	cmpl	$2, %edi
	je	.L67
.L52:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore_state
	cmpl	$3, %edi
	je	.L52
	cmpl	$9, %edi
	movl	$9, %eax
	cmovle	%edi, %eax
	cmpl	$4, %eax
	je	.L57
	cmpl	$5, %eax
	je	.L58
	cmpl	$6, %eax
	je	.L59
	cmpl	$7, %eax
	je	.L60
	cmpl	$8, %edi
	movl	$1000000, %ecx
	movl	$100000, %eax
	cmove	%eax, %ecx
.L56:
	movl	%esi, %eax
	cltd
	idivl	%ecx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_restore_state
	imull	$100, %esi, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_restore_state
	leal	(%rsi,%rsi,4), %eax
	addl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	.cfi_restore_state
	movl	$10, %ecx
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L58:
	movl	$100, %ecx
	jmp	.L56
.L59:
	movl	$1000, %ecx
	jmp	.L56
.L60:
	movl	$10000, %ecx
	jmp	.L56
	.cfi_endproc
.LFE17866:
	.size	_ZN2v88internal10DateParser16ReadMillisecondsENS1_9DateTokenE, .-_ZN2v88internal10DateParser16ReadMillisecondsENS1_9DateTokenE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal10DateParser11DayComposer5WriteEPd,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal10DateParser11DayComposer5WriteEPd, @function
_GLOBAL__sub_I__ZN2v88internal10DateParser11DayComposer5WriteEPd:
.LFB21523:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE21523:
	.size	_GLOBAL__sub_I__ZN2v88internal10DateParser11DayComposer5WriteEPd, .-_GLOBAL__sub_I__ZN2v88internal10DateParser11DayComposer5WriteEPd
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal10DateParser11DayComposer5WriteEPd
	.globl	_ZN2v88internal10DateParser12KeywordTable5arrayE
	.section	.rodata._ZN2v88internal10DateParser12KeywordTable5arrayE,"a"
	.align 32
	.type	_ZN2v88internal10DateParser12KeywordTable5arrayE, @object
	.size	_ZN2v88internal10DateParser12KeywordTable5arrayE, 140
_ZN2v88internal10DateParser12KeywordTable5arrayE:
	.ascii	"jan\001\001"
	.ascii	"feb\001\002"
	.ascii	"mar\001\003"
	.ascii	"apr\001\004"
	.ascii	"may\001\005"
	.ascii	"jun\001\006"
	.ascii	"jul\001\007"
	.ascii	"aug\001\b"
	.ascii	"sep\001\t"
	.ascii	"oct\001\n"
	.ascii	"nov\001\013"
	.ascii	"dec\001\f"
	.string	"am"
	.string	"\004"
	.string	"pm"
	.ascii	"\004\f"
	.string	"ut"
	.string	"\002"
	.string	"utc\002"
	.string	"z"
	.string	""
	.string	"\002"
	.string	"gmt\002"
	.ascii	"cdt\002\373"
	.ascii	"cst\002\372"
	.ascii	"edt\002\374"
	.ascii	"est\002\373"
	.ascii	"mdt\002\372"
	.ascii	"mst\002\371"
	.ascii	"pdt\002\371"
	.ascii	"pst\002\370"
	.string	"t"
	.string	""
	.string	"\003"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	2146959360
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
