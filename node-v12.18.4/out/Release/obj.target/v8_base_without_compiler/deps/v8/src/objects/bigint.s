	.file	"bigint.cc"
	.text
	.section	.text._ZNSt17_Function_handlerIFmmmEZN2v88internal13MutableBigInt11AbsoluteAndEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E9_M_invokeERKSt9_Any_dataOmSE_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFmmmEZN2v88internal13MutableBigInt11AbsoluteAndEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E9_M_invokeERKSt9_Any_dataOmSE_, @function
_ZNSt17_Function_handlerIFmmmEZN2v88internal13MutableBigInt11AbsoluteAndEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E9_M_invokeERKSt9_Any_dataOmSE_:
.LFB20883:
	.cfi_startproc
	endbr64
	movq	(%rdx), %rax
	andq	(%rsi), %rax
	ret
	.cfi_endproc
.LFE20883:
	.size	_ZNSt17_Function_handlerIFmmmEZN2v88internal13MutableBigInt11AbsoluteAndEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E9_M_invokeERKSt9_Any_dataOmSE_, .-_ZNSt17_Function_handlerIFmmmEZN2v88internal13MutableBigInt11AbsoluteAndEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E9_M_invokeERKSt9_Any_dataOmSE_
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal13MutableBigInt11AbsoluteAndEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal13MutableBigInt11AbsoluteAndEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal13MutableBigInt11AbsoluteAndEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation:
.LFB20884:
	.cfi_startproc
	endbr64
	cmpl	$1, %edx
	je	.L5
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE20884:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal13MutableBigInt11AbsoluteAndEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal13MutableBigInt11AbsoluteAndEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation
	.section	.text._ZNSt17_Function_handlerIFmmmEZN2v88internal13MutableBigInt14AbsoluteAndNotEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E9_M_invokeERKSt9_Any_dataOmSE_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFmmmEZN2v88internal13MutableBigInt14AbsoluteAndNotEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E9_M_invokeERKSt9_Any_dataOmSE_, @function
_ZNSt17_Function_handlerIFmmmEZN2v88internal13MutableBigInt14AbsoluteAndNotEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E9_M_invokeERKSt9_Any_dataOmSE_:
.LFB20887:
	.cfi_startproc
	endbr64
	movq	(%rdx), %rax
	notq	%rax
	andq	(%rsi), %rax
	ret
	.cfi_endproc
.LFE20887:
	.size	_ZNSt17_Function_handlerIFmmmEZN2v88internal13MutableBigInt14AbsoluteAndNotEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E9_M_invokeERKSt9_Any_dataOmSE_, .-_ZNSt17_Function_handlerIFmmmEZN2v88internal13MutableBigInt14AbsoluteAndNotEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E9_M_invokeERKSt9_Any_dataOmSE_
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal13MutableBigInt14AbsoluteAndNotEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal13MutableBigInt14AbsoluteAndNotEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal13MutableBigInt14AbsoluteAndNotEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation:
.LFB20888:
	.cfi_startproc
	endbr64
	cmpl	$1, %edx
	je	.L9
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE20888:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal13MutableBigInt14AbsoluteAndNotEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal13MutableBigInt14AbsoluteAndNotEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation
	.section	.text._ZNSt17_Function_handlerIFmmmEZN2v88internal13MutableBigInt10AbsoluteOrEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E9_M_invokeERKSt9_Any_dataOmSE_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFmmmEZN2v88internal13MutableBigInt10AbsoluteOrEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E9_M_invokeERKSt9_Any_dataOmSE_, @function
_ZNSt17_Function_handlerIFmmmEZN2v88internal13MutableBigInt10AbsoluteOrEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E9_M_invokeERKSt9_Any_dataOmSE_:
.LFB20891:
	.cfi_startproc
	endbr64
	movq	(%rdx), %rax
	orq	(%rsi), %rax
	ret
	.cfi_endproc
.LFE20891:
	.size	_ZNSt17_Function_handlerIFmmmEZN2v88internal13MutableBigInt10AbsoluteOrEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E9_M_invokeERKSt9_Any_dataOmSE_, .-_ZNSt17_Function_handlerIFmmmEZN2v88internal13MutableBigInt10AbsoluteOrEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E9_M_invokeERKSt9_Any_dataOmSE_
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal13MutableBigInt10AbsoluteOrEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal13MutableBigInt10AbsoluteOrEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal13MutableBigInt10AbsoluteOrEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation:
.LFB20892:
	.cfi_startproc
	endbr64
	cmpl	$1, %edx
	je	.L13
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE20892:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal13MutableBigInt10AbsoluteOrEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal13MutableBigInt10AbsoluteOrEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation
	.section	.text._ZNSt17_Function_handlerIFmmmEZN2v88internal13MutableBigInt11AbsoluteXorEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E9_M_invokeERKSt9_Any_dataOmSE_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFmmmEZN2v88internal13MutableBigInt11AbsoluteXorEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E9_M_invokeERKSt9_Any_dataOmSE_, @function
_ZNSt17_Function_handlerIFmmmEZN2v88internal13MutableBigInt11AbsoluteXorEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E9_M_invokeERKSt9_Any_dataOmSE_:
.LFB20895:
	.cfi_startproc
	endbr64
	movq	(%rdx), %rax
	xorq	(%rsi), %rax
	ret
	.cfi_endproc
.LFE20895:
	.size	_ZNSt17_Function_handlerIFmmmEZN2v88internal13MutableBigInt11AbsoluteXorEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E9_M_invokeERKSt9_Any_dataOmSE_, .-_ZNSt17_Function_handlerIFmmmEZN2v88internal13MutableBigInt11AbsoluteXorEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E9_M_invokeERKSt9_Any_dataOmSE_
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal13MutableBigInt11AbsoluteXorEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal13MutableBigInt11AbsoluteXorEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal13MutableBigInt11AbsoluteXorEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation:
.LFB20896:
	.cfi_startproc
	endbr64
	cmpl	$1, %edx
	je	.L17
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE20896:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal13MutableBigInt11AbsoluteXorEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal13MutableBigInt11AbsoluteXorEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation
	.section	.text._ZN2v88internal13MutableBigInt17InplaceRightShiftEi.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal13MutableBigInt17InplaceRightShiftEi.part.0, @function
_ZN2v88internal13MutableBigInt17InplaceRightShiftEi.part.0:
.LFB21823:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %r8
	movl	%esi, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	15(%r8), %r10
	movl	7(%r8), %eax
	shrq	%cl, %r10
	shrl	%eax
	movl	%eax, %r12d
	andl	$1073741823, %r12d
	testl	$1073741822, %eax
	je	.L19
	leal	-2(%r12), %eax
	movl	$64, %r11d
	leaq	32(,%rax,8), %rbx
	subl	%esi, %r11d
	movl	$24, %eax
	.p2align 4,,10
	.p2align 3
.L20:
	movq	-1(%rax,%r8), %r9
	movl	%r11d, %ecx
	addq	$8, %rax
	movq	%r9, %rdx
	salq	%cl, %rdx
	movl	%esi, %ecx
	orq	%r10, %rdx
	movq	%r9, %r10
	movq	%rdx, -17(%rax,%r8)
	shrq	%cl, %r10
	movq	(%rdi), %r8
	cmpq	%rax, %rbx
	jne	.L20
.L19:
	leal	8(,%r12,8), %eax
	cltq
	movq	%r10, -1(%rax,%r8)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21823:
	.size	_ZN2v88internal13MutableBigInt17InplaceRightShiftEi.part.0, .-_ZN2v88internal13MutableBigInt17InplaceRightShiftEi.part.0
	.section	.rodata._ZN2v88internal13MutableBigInt3NewEPNS0_7IsolateEiNS0_14AllocationTypeE.constprop.0.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"Aborting on invalid BigInt length"
	.section	.text._ZN2v88internal13MutableBigInt3NewEPNS0_7IsolateEiNS0_14AllocationTypeE.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal13MutableBigInt3NewEPNS0_7IsolateEiNS0_14AllocationTypeE.constprop.0, @function
_ZN2v88internal13MutableBigInt3NewEPNS0_7IsolateEiNS0_14AllocationTypeE.constprop.0:
.LFB21838:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpl	$16777216, %esi
	jle	.L28
	cmpb	$0, _ZN2v88internal36FLAG_correctness_fuzzer_suppressionsE(%rip)
	jne	.L32
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$183, %esi
	call	_ZN2v88internal7Factory13NewRangeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	movq	%r12, %rdi
	xorl	%edx, %edx
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_restore_state
	xorl	%edx, %edx
	movl	%esi, %ebx
	call	_ZN2v88internal7Factory9NewBigIntEiNS0_14AllocationTypeE@PLT
	addl	%ebx, %ebx
	movq	(%rax), %rdx
	movl	%ebx, 7(%rdx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L32:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21838:
	.size	_ZN2v88internal13MutableBigInt3NewEPNS0_7IsolateEiNS0_14AllocationTypeE.constprop.0, .-_ZN2v88internal13MutableBigInt3NewEPNS0_7IsolateEiNS0_14AllocationTypeE.constprop.0
	.section	.text._ZN2v88internal13MutableBigInt3NewEPNS0_7IsolateEiNS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13MutableBigInt3NewEPNS0_7IsolateEiNS0_14AllocationTypeE
	.type	_ZN2v88internal13MutableBigInt3NewEPNS0_7IsolateEiNS0_14AllocationTypeE, @function
_ZN2v88internal13MutableBigInt3NewEPNS0_7IsolateEiNS0_14AllocationTypeE:
.LFB17802:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpl	$16777216, %esi
	jle	.L34
	cmpb	$0, _ZN2v88internal36FLAG_correctness_fuzzer_suppressionsE(%rip)
	jne	.L38
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$183, %esi
	call	_ZN2v88internal7Factory13NewRangeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	movq	%r12, %rdi
	xorl	%edx, %edx
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	.cfi_restore_state
	movl	%esi, %ebx
	call	_ZN2v88internal7Factory9NewBigIntEiNS0_14AllocationTypeE@PLT
	addl	%ebx, %ebx
	movq	(%rax), %rdx
	movl	%ebx, 7(%rdx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L38:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE17802:
	.size	_ZN2v88internal13MutableBigInt3NewEPNS0_7IsolateEiNS0_14AllocationTypeE, .-_ZN2v88internal13MutableBigInt3NewEPNS0_7IsolateEiNS0_14AllocationTypeE
	.section	.rodata._ZN2v88internal13MutableBigInt4CopyEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"(location_) != nullptr"
.LC2:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal13MutableBigInt4CopyEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13MutableBigInt4CopyEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEE
	.type	_ZN2v88internal13MutableBigInt4CopyEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEE, @function
_ZN2v88internal13MutableBigInt4CopyEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEE:
.LFB17805:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	(%rsi), %rax
	movl	7(%rax), %ebx
	shrl	%ebx
	andl	$1073741823, %ebx
	cmpl	$16777216, %ebx
	jle	.L40
	cmpb	$0, _ZN2v88internal36FLAG_correctness_fuzzer_suppressionsE(%rip)
	jne	.L43
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$183, %esi
	movq	%rdi, -40(%rbp)
	call	_ZN2v88internal7Factory13NewRangeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	movq	-40(%rbp), %rdi
	xorl	%edx, %edx
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L40:
	movq	%rsi, %r13
	xorl	%edx, %edx
	movl	%ebx, %esi
	call	_ZN2v88internal7Factory9NewBigIntEiNS0_14AllocationTypeE@PLT
	leal	(%rbx,%rbx), %edx
	movq	%rax, %r12
	movq	(%rax), %rax
	movl	%edx, 7(%rax)
	movq	(%r12), %rax
	leal	0(,%rbx,8), %edx
	movq	0(%r13), %rsi
	movslq	%edx, %rdx
	leaq	15(%rax), %rdi
	addq	$15, %rsi
	call	memcpy@PLT
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L43:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE17805:
	.size	_ZN2v88internal13MutableBigInt4CopyEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEE, .-_ZN2v88internal13MutableBigInt4CopyEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEE
	.section	.text._ZN2v88internal13MutableBigInt16InitializeDigitsEih,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13MutableBigInt16InitializeDigitsEih
	.type	_ZN2v88internal13MutableBigInt16InitializeDigitsEih, @function
_ZN2v88internal13MutableBigInt16InitializeDigitsEih:
.LFB17806:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	movl	%edx, %r8d
	sall	$3, %esi
	movslq	%esi, %rdx
	movzbl	%r8b, %esi
	addq	$15, %rdi
	jmp	memset@PLT
	.cfi_endproc
.LFE17806:
	.size	_ZN2v88internal13MutableBigInt16InitializeDigitsEih, .-_ZN2v88internal13MutableBigInt16InitializeDigitsEih
	.section	.text._ZN2v88internal13MutableBigInt12CanonicalizeES1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13MutableBigInt12CanonicalizeES1_
	.type	_ZN2v88internal13MutableBigInt12CanonicalizeES1_, @function
_ZN2v88internal13MutableBigInt12CanonicalizeES1_:
.LFB17812:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	-1(%rdi), %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	7(%rdi), %eax
	shrl	%eax
	movl	%eax, %r13d
	leal	8(,%rax,8), %eax
	andl	$1073741823, %r13d
	cltq
	addq	%r15, %rax
	movl	%r13d, %ebx
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L58:
	movq	(%rax), %rdx
	leal	-1(%rbx), %ecx
	subq	$8, %rax
	testq	%rdx, %rdx
	jne	.L57
	movl	%ecx, %ebx
.L47:
	testl	%ebx, %ebx
	jne	.L58
	testl	%r13d, %r13d
	jne	.L59
.L45:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	.cfi_restore_state
	subl	%ebx, %r13d
	testl	%r13d, %r13d
	je	.L45
.L59:
	movq	%r12, %rax
	movq	%r12, %rdi
	andq	$-262144, %rax
	movq	24(%rax), %r14
	call	_ZN2v88internal4Heap13IsLargeObjectENS0_10HeapObjectE@PLT
	testb	%al, %al
	je	.L60
.L50:
	movl	7(%r12), %eax
	leal	(%rbx,%rbx), %edx
	andl	$-2147483647, %eax
	orl	%edx, %eax
	movl	%eax, 7(%r12)
	testl	%ebx, %ebx
	jne	.L45
	movl	7(%r12), %eax
	andl	$-2, %eax
	movl	%eax, 7(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	.cfi_restore_state
	leal	16(,%rbx,8), %esi
	leal	0(,%r13,8), %edx
	movl	$1, %ecx
	movq	%r14, %rdi
	movslq	%esi, %rsi
	movl	$1, %r8d
	addq	%r15, %rsi
	call	_ZN2v88internal4Heap20CreateFillerObjectAtEmiNS0_18ClearRecordedSlotsENS0_20ClearFreedMemoryModeE@PLT
	jmp	.L50
	.cfi_endproc
.LFE17812:
	.size	_ZN2v88internal13MutableBigInt12CanonicalizeES1_, .-_ZN2v88internal13MutableBigInt12CanonicalizeES1_
	.section	.text._ZN2v88internal13MutableBigInt13MakeImmutableENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13MutableBigInt13MakeImmutableENS0_6HandleIS1_EE
	.type	_ZN2v88internal13MutableBigInt13MakeImmutableENS0_6HandleIS1_EE, @function
_ZN2v88internal13MutableBigInt13MakeImmutableENS0_6HandleIS1_EE:
.LFB17811:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rdi), %rdi
	call	_ZN2v88internal13MutableBigInt12CanonicalizeES1_
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17811:
	.size	_ZN2v88internal13MutableBigInt13MakeImmutableENS0_6HandleIS1_EE, .-_ZN2v88internal13MutableBigInt13MakeImmutableENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal13MutableBigInt13MakeImmutableENS0_11MaybeHandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13MutableBigInt13MakeImmutableENS0_11MaybeHandleIS1_EE
	.type	_ZN2v88internal13MutableBigInt13MakeImmutableENS0_11MaybeHandleIS1_EE, @function
_ZN2v88internal13MutableBigInt13MakeImmutableENS0_11MaybeHandleIS1_EE:
.LFB17807:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L69
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	call	_ZN2v88internal13MutableBigInt12CanonicalizeES1_
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE17807:
	.size	_ZN2v88internal13MutableBigInt13MakeImmutableENS0_11MaybeHandleIS1_EE, .-_ZN2v88internal13MutableBigInt13MakeImmutableENS0_11MaybeHandleIS1_EE
	.section	.text._ZN2v88internal13MutableBigInt10NewFromIntEPNS0_7IsolateEi.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal13MutableBigInt10NewFromIntEPNS0_7IsolateEi.part.0, @function
_ZN2v88internal13MutableBigInt10NewFromIntEPNS0_7IsolateEi.part.0:
.LFB21832:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movslq	%esi, %rbx
	movl	$1, %esi
	call	_ZN2v88internal7Factory9NewBigIntEiNS0_14AllocationTypeE@PLT
	movq	(%rax), %rdx
	movq	%rax, %r12
	movl	%ebx, %eax
	shrl	$31, %eax
	orl	$2, %eax
	movl	%eax, 7(%rdx)
	movq	(%r12), %rax
	testl	%ebx, %ebx
	jns	.L75
	cmpl	$-2147483648, %ebx
	je	.L76
	negl	%ebx
	movslq	%ebx, %rbx
.L75:
	movq	%rbx, 15(%rax)
.L72:
	movq	(%r12), %rdi
	call	_ZN2v88internal13MutableBigInt12CanonicalizeES1_
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	.cfi_restore_state
	movl	$2147483648, %ecx
	movq	%rcx, 15(%rax)
	jmp	.L72
	.cfi_endproc
.LFE21832:
	.size	_ZN2v88internal13MutableBigInt10NewFromIntEPNS0_7IsolateEi.part.0, .-_ZN2v88internal13MutableBigInt10NewFromIntEPNS0_7IsolateEi.part.0
	.section	.text._ZN2v88internal13MutableBigInt10NewFromIntEPNS0_7IsolateEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13MutableBigInt10NewFromIntEPNS0_7IsolateEi
	.type	_ZN2v88internal13MutableBigInt10NewFromIntEPNS0_7IsolateEi, @function
_ZN2v88internal13MutableBigInt10NewFromIntEPNS0_7IsolateEi:
.LFB17803:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	je	.L81
	jmp	_ZN2v88internal13MutableBigInt10NewFromIntEPNS0_7IsolateEi.part.0
	.p2align 4,,10
	.p2align 3
.L81:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_ZN2v88internal7Factory9NewBigIntEiNS0_14AllocationTypeE@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	movl	$0, 7(%rax)
	movq	(%r12), %rdi
	call	_ZN2v88internal13MutableBigInt12CanonicalizeES1_
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17803:
	.size	_ZN2v88internal13MutableBigInt10NewFromIntEPNS0_7IsolateEi, .-_ZN2v88internal13MutableBigInt10NewFromIntEPNS0_7IsolateEi
	.section	.text._ZN2v88internal13MutableBigInt13NewFromDoubleEPNS0_7IsolateEd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13MutableBigInt13NewFromDoubleEPNS0_7IsolateEd
	.type	_ZN2v88internal13MutableBigInt13NewFromDoubleEPNS0_7IsolateEd, @function
_ZN2v88internal13MutableBigInt13NewFromDoubleEPNS0_7IsolateEd:
.LFB17804:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm1, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	ucomisd	%xmm1, %xmm0
	jp	.L85
	jne	.L85
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZN2v88internal7Factory9NewBigIntEiNS0_14AllocationTypeE@PLT
	movq	%rax, %rbx
	movq	(%rax), %rax
	movl	$0, 7(%rax)
	movq	(%rbx), %rdi
	call	_ZN2v88internal13MutableBigInt12CanonicalizeES1_
	addq	$24, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	.cfi_restore_state
	movq	%xmm0, %r12
	movq	%xmm0, %r14
	movsd	%xmm0, -56(%rbp)
	shrq	$52, %r12
	andl	$2047, %r12d
	leal	-960(%r12), %r13d
	subl	$1023, %r12d
	cmovns	%r12d, %r13d
	xorl	%edx, %edx
	sarl	$6, %r13d
	leal	1(%r13), %r15d
	movl	%r15d, %esi
	addl	%r15d, %r15d
	call	_ZN2v88internal7Factory9NewBigIntEiNS0_14AllocationTypeE@PLT
	movsd	-56(%rbp), %xmm0
	pxor	%xmm2, %xmm2
	xorl	%edx, %edx
	movq	%rax, %rbx
	movq	(%rax), %rax
	comisd	%xmm0, %xmm2
	seta	%dl
	orl	%edx, %r15d
	movl	%r12d, %edx
	sarl	$31, %edx
	movl	%r15d, 7(%rax)
	movabsq	$4503599627370495, %rax
	shrl	$26, %edx
	andq	%r14, %rax
	addl	%edx, %r12d
	btsq	$52, %rax
	andl	$63, %r12d
	subl	%edx, %r12d
	cmpl	$51, %r12d
	jle	.L142
	leal	-52(%r12), %ecx
	xorl	%esi, %esi
	salq	%cl, %rax
	movq	%rax, %r8
	xorl	%eax, %eax
.L89:
	movq	(%rbx), %rcx
	leal	16(,%r13,8), %edx
	movslq	%edx, %rdi
	movq	%r8, -1(%rdi,%rcx)
	movl	%r13d, %edi
	subl	$1, %edi
	js	.L90
	xorl	%edi, %edi
	testl	%esi, %esi
	je	.L91
	movq	%rax, %rdi
	subl	$64, %esi
	xorl	%eax, %eax
.L91:
	movq	(%rbx), %r8
	leal	-8(%rdx), %ecx
	movslq	%ecx, %rcx
	movq	%rdi, -1(%rcx,%r8)
	cmpl	$1, %r13d
	je	.L90
	testl	%esi, %esi
	movl	$0, %ecx
	movq	(%rbx), %rsi
	cmovle	%rcx, %rax
	leal	-16(%rdx), %ecx
	movslq	%ecx, %rcx
	movq	%rax, -1(%rcx,%rsi)
	cmpl	$2, %r13d
	je	.L90
	movq	(%rbx), %rcx
	leal	-24(%rdx), %eax
	cltq
	movq	$0, -1(%rax,%rcx)
	cmpl	$3, %r13d
	je	.L90
	movq	(%rbx), %rcx
	leal	-32(%rdx), %eax
	cltq
	movq	$0, -1(%rax,%rcx)
	cmpl	$4, %r13d
	je	.L90
	movq	(%rbx), %rcx
	leal	-40(%rdx), %eax
	cltq
	movq	$0, -1(%rax,%rcx)
	cmpl	$5, %r13d
	je	.L90
	movq	(%rbx), %rcx
	leal	-48(%rdx), %eax
	cltq
	movq	$0, -1(%rax,%rcx)
	cmpl	$6, %r13d
	je	.L90
	movq	(%rbx), %rcx
	leal	-56(%rdx), %eax
	cltq
	movq	$0, -1(%rax,%rcx)
	cmpl	$7, %r13d
	je	.L90
	movq	(%rbx), %rcx
	leal	-64(%rdx), %eax
	cltq
	movq	$0, -1(%rax,%rcx)
	cmpl	$8, %r13d
	je	.L90
	movq	(%rbx), %rcx
	leal	-72(%rdx), %eax
	cltq
	movq	$0, -1(%rax,%rcx)
	cmpl	$9, %r13d
	je	.L90
	movq	(%rbx), %rcx
	leal	-80(%rdx), %eax
	cltq
	movq	$0, -1(%rax,%rcx)
	cmpl	$10, %r13d
	je	.L90
	movq	(%rbx), %rcx
	leal	-88(%rdx), %eax
	cltq
	movq	$0, -1(%rax,%rcx)
	cmpl	$11, %r13d
	je	.L90
	movq	(%rbx), %rcx
	leal	-96(%rdx), %eax
	cltq
	movq	$0, -1(%rax,%rcx)
	cmpl	$12, %r13d
	je	.L90
	movq	(%rbx), %rcx
	leal	-104(%rdx), %eax
	cltq
	movq	$0, -1(%rax,%rcx)
	cmpl	$13, %r13d
	je	.L90
	movq	(%rbx), %rcx
	leal	-112(%rdx), %eax
	cltq
	movq	$0, -1(%rax,%rcx)
	cmpl	$14, %r13d
	je	.L90
	leal	-120(%rdx), %eax
	movq	(%rbx), %rdx
	cltq
	movq	$0, -1(%rax,%rdx)
	cmpl	$15, %r13d
	je	.L90
	movq	(%rbx), %rax
	movq	$0, 15(%rax)
.L90:
	movq	(%rbx), %rdi
	call	_ZN2v88internal13MutableBigInt12CanonicalizeES1_
	addq	$24, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L142:
	.cfi_restore_state
	movl	$52, %esi
	movq	%rax, %r8
	subl	%r12d, %esi
	movl	%esi, %ecx
	shrq	%cl, %r8
	leal	12(%r12), %ecx
	salq	%cl, %rax
	jmp	.L89
	.cfi_endproc
.LFE17804:
	.size	_ZN2v88internal13MutableBigInt13NewFromDoubleEPNS0_7IsolateEd, .-_ZN2v88internal13MutableBigInt13NewFromDoubleEPNS0_7IsolateEd
	.section	.text._ZN2v88internal6BigInt4ZeroEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6BigInt4ZeroEPNS0_7IsolateE
	.type	_ZN2v88internal6BigInt4ZeroEPNS0_7IsolateE, @function
_ZN2v88internal6BigInt4ZeroEPNS0_7IsolateE:
.LFB17813:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_ZN2v88internal7Factory9NewBigIntEiNS0_14AllocationTypeE@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	movl	$0, 7(%rax)
	movq	(%r12), %rdi
	call	_ZN2v88internal13MutableBigInt12CanonicalizeES1_
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17813:
	.size	_ZN2v88internal6BigInt4ZeroEPNS0_7IsolateE, .-_ZN2v88internal6BigInt4ZeroEPNS0_7IsolateE
	.section	.text._ZN2v88internal6BigInt10UnaryMinusEPNS0_7IsolateENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6BigInt10UnaryMinusEPNS0_7IsolateENS0_6HandleIS1_EE
	.type	_ZN2v88internal6BigInt10UnaryMinusEPNS0_7IsolateENS0_6HandleIS1_EE, @function
_ZN2v88internal6BigInt10UnaryMinusEPNS0_7IsolateENS0_6HandleIS1_EE:
.LFB17814:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	(%rsi), %rax
	movl	7(%rax), %edx
	andl	$2147483646, %edx
	jne	.L146
	addq	$8, %rsp
	movq	%rsi, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L146:
	.cfi_restore_state
	movl	7(%rax), %r12d
	shrl	%r12d
	andl	$1073741823, %r12d
	movl	%r12d, %esi
	call	_ZN2v88internal13MutableBigInt3NewEPNS0_7IsolateEiNS0_14AllocationTypeE.constprop.0
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L150
	movq	(%rax), %rax
	leal	0(,%r12,8), %edx
	movslq	%edx, %rdx
	leaq	15(%rax), %rdi
	movq	(%rbx), %rax
	leaq	15(%rax), %rsi
	call	memcpy@PLT
	movq	(%rbx), %rax
	movq	0(%r13), %rcx
	movl	7(%rax), %edx
	movl	7(%rcx), %eax
	notl	%edx
	andl	$-2, %eax
	andl	$1, %edx
	orl	%edx, %eax
	movl	%eax, 7(%rcx)
	movq	0(%r13), %rdi
	call	_ZN2v88internal13MutableBigInt12CanonicalizeES1_
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L150:
	.cfi_restore_state
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE17814:
	.size	_ZN2v88internal6BigInt10UnaryMinusEPNS0_7IsolateENS0_6HandleIS1_EE, .-_ZN2v88internal6BigInt10UnaryMinusEPNS0_7IsolateENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal6BigInt18UnsignedRightShiftEPNS0_7IsolateENS0_6HandleIS1_EES5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6BigInt18UnsignedRightShiftEPNS0_7IsolateENS0_6HandleIS1_EES5_
	.type	_ZN2v88internal6BigInt18UnsignedRightShiftEPNS0_7IsolateENS0_6HandleIS1_EES5_, @function
_ZN2v88internal6BigInt18UnsignedRightShiftEPNS0_7IsolateENS0_6HandleIS1_EES5_:
.LFB17827:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$23, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	movq	%r12, %rdi
	xorl	%edx, %edx
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17827:
	.size	_ZN2v88internal6BigInt18UnsignedRightShiftEPNS0_7IsolateENS0_6HandleIS1_EES5_, .-_ZN2v88internal6BigInt18UnsignedRightShiftEPNS0_7IsolateENS0_6HandleIS1_EES5_
	.section	.text._ZN2v88internal6BigInt13EqualToBigIntES1_S1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6BigInt13EqualToBigIntES1_S1_
	.type	_ZN2v88internal6BigInt13EqualToBigIntES1_S1_, @function
_ZN2v88internal6BigInt13EqualToBigIntES1_S1_:
.LFB17832:
	.cfi_startproc
	endbr64
	movl	7(%rdi), %eax
	movl	7(%rsi), %edx
	leaq	7(%rdi), %r8
	andl	$1, %edx
	andl	$1, %eax
	cmpb	%al, %dl
	je	.L158
.L154:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L158:
	movl	7(%rdi), %eax
	movl	7(%rsi), %edx
	shrl	%eax
	shrl	%edx
	andl	$1073741823, %edx
	andl	$1073741823, %eax
	cmpl	%eax, %edx
	jne	.L154
	leaq	15(%rdi), %rdx
	xorl	%ecx, %ecx
	subq	%rdi, %rsi
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L159:
	movq	(%rdx), %rax
	movq	(%rsi,%rdx), %rdi
	addq	$8, %rdx
	cmpq	%rax, %rdi
	jne	.L154
	addl	$1, %ecx
.L156:
	movl	(%r8), %eax
	shrl	%eax
	andl	$1073741823, %eax
	cmpl	%eax, %ecx
	jl	.L159
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE17832:
	.size	_ZN2v88internal6BigInt13EqualToBigIntES1_S1_, .-_ZN2v88internal6BigInt13EqualToBigIntES1_S1_
	.section	.text._ZN2v88internal6BigInt13EqualToStringEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6BigInt13EqualToStringEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEE
	.type	_ZN2v88internal6BigInt13EqualToStringEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEE, @function
_ZN2v88internal6BigInt13EqualToStringEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEE:
.LFB17842:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	movq	%rdx, %rsi
	subq	$8, %rsp
	call	_ZN2v88internal14StringToBigIntEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	testq	%rax, %rax
	je	.L161
	movq	(%rbx), %rdi
	movq	(%rax), %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6BigInt13EqualToBigIntES1_S1_
	.p2align 4,,10
	.p2align 3
.L161:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17842:
	.size	_ZN2v88internal6BigInt13EqualToStringEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEE, .-_ZN2v88internal6BigInt13EqualToStringEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEE
	.section	.text._ZN2v88internal6BigInt15CompareToNumberENS0_6HandleIS1_EENS2_INS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6BigInt15CompareToNumberENS0_6HandleIS1_EENS2_INS0_6ObjectEEE
	.type	_ZN2v88internal6BigInt15CompareToNumberENS0_6HandleIS1_EENS2_INS0_6ObjectEEE, @function
_ZN2v88internal6BigInt15CompareToNumberENS0_6HandleIS1_EENS2_INS0_6ObjectEEE:
.LFB17844:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L164
	movq	(%rdi), %rcx
	movq	%rax, %rsi
	shrq	$63, %rax
	sarq	$32, %rsi
	movl	7(%rcx), %edx
	andl	$1, %edx
	cmpb	%al, %dl
	je	.L165
.L230:
	testb	%dl, %dl
	je	.L228
.L227:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L164:
	movq	7(%rax), %rax
	movq	%rax, %xmm0
	ucomisd	%xmm0, %xmm0
	jp	.L196
	ucomisd	.LC4(%rip), %xmm0
	jnp	.L240
.L201:
	ucomisd	.LC5(%rip), %xmm0
	jnp	.L241
.L202:
	movq	(%rdi), %rdi
	pxor	%xmm1, %xmm1
	movl	7(%rdi), %esi
	andl	$1, %esi
	comisd	%xmm0, %xmm1
	seta	%dl
	cmpb	%dl, %sil
	je	.L177
.L237:
	testb	%sil, %sil
	jne	.L227
.L228:
	movl	$2, %eax
.L226:
	ret
	.p2align 4,,10
	.p2align 3
.L165:
	movl	7(%rcx), %eax
	testl	$2147483646, %eax
	jne	.L168
	xorl	%eax, %eax
	testq	%rsi, %rsi
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L177:
	ucomisd	%xmm1, %xmm0
	jp	.L178
	jne	.L178
	movl	7(%rdi), %eax
	testl	$2147483646, %eax
	setne	%al
	movzbl	%al, %eax
	addl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L240:
	jne	.L201
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L241:
	jne	.L202
	movl	$2, %eax
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L178:
	movl	7(%rdi), %edx
	andl	$2147483646, %edx
	je	.L227
	movq	%rax, %rdx
	shrq	$52, %rdx
	andl	$2047, %edx
	cmpl	$1022, %edx
	jle	.L237
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	-1(%rdi), %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	7(%rdi), %r8d
	shrl	%r8d
	leal	8(,%r8,8), %r10d
	movl	%r8d, %ebx
	movslq	%r10d, %rcx
	andl	$1073741823, %ebx
	movq	-1(%rdi,%rcx), %r11
	movl	$64, %ecx
	testq	%r11, %r11
	je	.L182
	bsrq	%r11, %rcx
	xorl	$63, %ecx
.L182:
	movl	%ebx, %r12d
	subl	$1022, %edx
	sall	$6, %r12d
	subl	%ecx, %r12d
	cmpl	%edx, %r12d
	jl	.L233
	jg	.L194
	movabsq	$4503599627370495, %rdx
	andq	%rax, %rdx
	movl	$63, %eax
	subl	%ecx, %eax
	btsq	$52, %rdx
	cmpl	$51, %eax
	jg	.L185
	leal	-11(%rcx), %r12d
	movq	%rdx, %rax
	movl	%r12d, %ecx
	shrq	%cl, %rax
	movl	$64, %ecx
	subl	%r12d, %ecx
	salq	%cl, %rdx
.L186:
	cmpq	%rax, %r11
	ja	.L194
	jb	.L233
	andl	$1073741822, %r8d
	je	.L189
	leal	-8(%r10), %eax
	leal	-2(%rbx), %ecx
	cltq
	salq	$3, %rcx
	addq	%rax, %r9
	leaq	-9(%rdi,%rax), %rax
	subq	%rcx, %rax
	jmp	.L193
.L242:
	subl	$64, %r12d
	cmpq	%rcx, %rdx
	jb	.L194
	ja	.L233
	xorl	%edx, %edx
.L192:
	subq	$8, %r9
	cmpq	%r9, %rax
	je	.L189
.L193:
	movq	(%r9), %rcx
	testl	%r12d, %r12d
	jg	.L242
	testq	%rcx, %rcx
	je	.L192
.L194:
	testb	%sil, %sil
	je	.L175
.L173:
	xorl	%eax, %eax
.L163:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L196:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	$3, %eax
	ret
.L189:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movl	$1, %eax
	testq	%rdx, %rdx
	je	.L163
.L233:
	testb	%sil, %sil
	je	.L173
.L175:
	movl	$2, %eax
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L168:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	7(%rcx), %eax
	shrl	%eax
	testl	$1073741822, %eax
	jne	.L230
	movl	%esi, %eax
	sarl	$31, %eax
	xorl	%eax, %esi
	subl	%eax, %esi
	cmpq	15(%rcx), %rsi
	jb	.L230
	movl	$1, %eax
	jbe	.L226
	testb	%dl, %dl
	je	.L227
	jmp	.L228
.L185:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movl	$11, %eax
	xorl	%r12d, %r12d
	subl	%ecx, %eax
	movl	%eax, %ecx
	movq	%rdx, %rax
	xorl	%edx, %edx
	salq	%cl, %rax
	jmp	.L186
	.cfi_endproc
.LFE17844:
	.size	_ZN2v88internal6BigInt15CompareToNumberENS0_6HandleIS1_EENS2_INS0_6ObjectEEE, .-_ZN2v88internal6BigInt15CompareToNumberENS0_6HandleIS1_EENS2_INS0_6ObjectEEE
	.section	.text._ZN2v88internal6BigInt15CompareToDoubleENS0_6HandleIS1_EEd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6BigInt15CompareToDoubleENS0_6HandleIS1_EEd
	.type	_ZN2v88internal6BigInt15CompareToDoubleENS0_6HandleIS1_EEd, @function
_ZN2v88internal6BigInt15CompareToDoubleENS0_6HandleIS1_EEd:
.LFB17845:
	.cfi_startproc
	endbr64
	ucomisd	%xmm0, %xmm0
	jp	.L267
	ucomisd	.LC4(%rip), %xmm0
	jnp	.L303
.L272:
	ucomisd	.LC5(%rip), %xmm0
	jnp	.L304
.L273:
	movq	(%rdi), %rdi
	pxor	%xmm1, %xmm1
	movl	7(%rdi), %eax
	movl	%eax, %esi
	andl	$1, %esi
	comisd	%xmm0, %xmm1
	seta	%al
	cmpb	%sil, %al
	je	.L249
.L301:
	testb	%sil, %sil
	je	.L292
.L291:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L304:
	jne	.L273
.L292:
	movl	$2, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L303:
	jne	.L272
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L249:
	ucomisd	%xmm1, %xmm0
	jp	.L250
	jne	.L250
	movl	7(%rdi), %eax
	testl	$2147483646, %eax
	setne	%al
	movzbl	%al, %eax
	addl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L250:
	movl	7(%rdi), %edx
	andl	$2147483646, %edx
	je	.L291
	movq	%xmm0, %rdx
	movq	%xmm0, %rax
	shrq	$52, %rdx
	andl	$2047, %edx
	cmpl	$1022, %edx
	jle	.L301
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	-1(%rdi), %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	7(%rdi), %r8d
	shrl	%r8d
	leal	8(,%r8,8), %r11d
	movl	%r8d, %ebx
	movslq	%r11d, %rcx
	andl	$1073741823, %ebx
	movq	-1(%rdi,%rcx), %r10
	movl	$64, %ecx
	testq	%r10, %r10
	je	.L254
	bsrq	%r10, %rcx
	xorl	$63, %ecx
.L254:
	movl	%ebx, %r12d
	subl	$1022, %edx
	sall	$6, %r12d
	subl	%ecx, %r12d
	cmpl	%edx, %r12d
	jl	.L297
	jg	.L266
	movabsq	$4503599627370495, %rdx
	andq	%rax, %rdx
	movl	$63, %eax
	subl	%ecx, %eax
	btsq	$52, %rdx
	cmpl	$51, %eax
	jg	.L257
	leal	-11(%rcx), %r12d
	movq	%rdx, %rax
	movl	%r12d, %ecx
	shrq	%cl, %rax
	movl	$64, %ecx
	subl	%r12d, %ecx
	salq	%cl, %rdx
.L258:
	cmpq	%r10, %rax
	jb	.L266
	ja	.L297
	andl	$1073741822, %r8d
	je	.L261
	leal	-8(%r11), %eax
	cltq
	leaq	-9(%rdi,%rax), %rcx
	addq	%rax, %r9
	leal	-2(%rbx), %eax
	salq	$3, %rax
	subq	%rax, %rcx
	jmp	.L265
.L305:
	subl	$64, %r12d
	cmpq	%rax, %rdx
	jb	.L266
	ja	.L297
	xorl	%edx, %edx
.L264:
	subq	$8, %r9
	cmpq	%r9, %rcx
	je	.L261
.L265:
	movq	(%r9), %rax
	testl	%r12d, %r12d
	jg	.L305
	testq	%rax, %rax
	je	.L264
.L266:
	testb	%sil, %sil
	je	.L247
.L245:
	xorl	%eax, %eax
.L243:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L261:
	.cfi_restore_state
	movl	$1, %eax
	testq	%rdx, %rdx
	je	.L243
.L297:
	testb	%sil, %sil
	je	.L245
.L247:
	popq	%rbx
	movl	$2, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L267:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	$3, %eax
	ret
.L257:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movl	$11, %eax
	xorl	%r12d, %r12d
	subl	%ecx, %eax
	movl	%eax, %ecx
	movq	%rdx, %rax
	xorl	%edx, %edx
	salq	%cl, %rax
	jmp	.L258
	.cfi_endproc
.LFE17845:
	.size	_ZN2v88internal6BigInt15CompareToDoubleENS0_6HandleIS1_EEd, .-_ZN2v88internal6BigInt15CompareToDoubleENS0_6HandleIS1_EEd
	.section	.text._ZN2v88internal6BigInt13EqualToNumberENS0_6HandleIS1_EENS2_INS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6BigInt13EqualToNumberENS0_6HandleIS1_EENS2_INS0_6ObjectEEE
	.type	_ZN2v88internal6BigInt13EqualToNumberENS0_6HandleIS1_EENS2_INS0_6ObjectEEE, @function
_ZN2v88internal6BigInt13EqualToNumberENS0_6HandleIS1_EENS2_INS0_6ObjectEEE:
.LFB17843:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L307
	sarq	$32, %rax
	movq	(%rdi), %rcx
	je	.L316
	movl	7(%rcx), %edx
	shrl	%edx
	andl	$1073741823, %edx
	cmpl	$1, %edx
	je	.L310
.L312:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L316:
	movl	7(%rcx), %eax
	testl	$2147483646, %eax
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L307:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	7(%rax), %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal6BigInt15CompareToDoubleENS0_6HandleIS1_EEd
	popq	%rbp
	.cfi_def_cfa 7, 8
	cmpl	$1, %eax
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L310:
	.cfi_restore 6
	movl	7(%rcx), %edx
	movl	%eax, %esi
	shrl	$31, %esi
	andl	$1, %edx
	cmpl	%edx, %esi
	jne	.L312
	cltd
	xorl	%edx, %eax
	subl	%edx, %eax
	cmpq	%rax, 15(%rcx)
	sete	%al
	ret
	.cfi_endproc
.LFE17843:
	.size	_ZN2v88internal6BigInt13EqualToNumberENS0_6HandleIS1_EENS2_INS0_6ObjectEEE, .-_ZN2v88internal6BigInt13EqualToNumberENS0_6HandleIS1_EENS2_INS0_6ObjectEEE
	.section	.text._ZN2v88internal6BigInt10FromNumberEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6BigInt10FromNumberEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	.type	_ZN2v88internal6BigInt10FromNumberEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE, @function
_ZN2v88internal6BigInt10FromNumberEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE:
.LFB17847:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rsi), %rsi
	testb	$1, %sil
	je	.L338
	movsd	7(%rsi), %xmm0
	movq	.LC6(%rip), %xmm1
	movsd	.LC7(%rip), %xmm3
	movapd	%xmm0, %xmm2
	andpd	%xmm1, %xmm2
	ucomisd	%xmm2, %xmm3
	jb	.L322
	ucomisd	%xmm0, %xmm0
	jp	.L331
	pxor	%xmm2, %xmm2
	ucomisd	%xmm2, %xmm0
	jnp	.L339
.L333:
	comisd	%xmm2, %xmm0
	movapd	%xmm0, %xmm3
	movsd	.LC8(%rip), %xmm4
	movapd	%xmm0, %xmm2
	andpd	%xmm1, %xmm3
	jb	.L335
	ucomisd	%xmm3, %xmm4
	jbe	.L328
	cvttsd2siq	%xmm0, %rax
	pxor	%xmm3, %xmm3
	movsd	.LC9(%rip), %xmm4
	andnpd	%xmm0, %xmm1
	cvtsi2sdq	%rax, %xmm3
	movapd	%xmm3, %xmm2
	cmpnlesd	%xmm0, %xmm2
	andpd	%xmm4, %xmm2
	subsd	%xmm2, %xmm3
	movapd	%xmm3, %xmm2
	orpd	%xmm1, %xmm2
.L328:
	movapd	%xmm2, %xmm1
.L323:
	ucomisd	%xmm1, %xmm0
	jp	.L322
	jne	.L322
	movq	%r12, %rdi
	call	_ZN2v88internal13MutableBigInt13NewFromDoubleEPNS0_7IsolateEd
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L322:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$19, %esi
	call	_ZN2v88internal7Factory13NewRangeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	movq	%r12, %rdi
	xorl	%edx, %edx
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L338:
	.cfi_restore_state
	sarq	$32, %rsi
	je	.L340
	call	_ZN2v88internal13MutableBigInt10NewFromIntEPNS0_7IsolateEi.part.0
	movq	%rax, %r12
.L320:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L340:
	.cfi_restore_state
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZN2v88internal7Factory9NewBigIntEiNS0_14AllocationTypeE@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	movl	$0, 7(%rax)
	movq	(%r12), %rdi
	call	_ZN2v88internal13MutableBigInt12CanonicalizeES1_
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L339:
	jne	.L333
	movapd	%xmm0, %xmm1
	jmp	.L323
	.p2align 4,,10
	.p2align 3
.L335:
	ucomisd	%xmm3, %xmm4
	jbe	.L328
	cvttsd2siq	%xmm0, %rax
	pxor	%xmm3, %xmm3
	movsd	.LC9(%rip), %xmm4
	andnpd	%xmm0, %xmm1
	cvtsi2sdq	%rax, %xmm3
	cmpnlesd	%xmm3, %xmm2
	andpd	%xmm4, %xmm2
	addsd	%xmm3, %xmm2
	orpd	%xmm1, %xmm2
	jmp	.L328
	.p2align 4,,10
	.p2align 3
.L331:
	pxor	%xmm1, %xmm1
	jmp	.L323
	.cfi_endproc
.LFE17847:
	.size	_ZN2v88internal6BigInt10FromNumberEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE, .-_ZN2v88internal6BigInt10FromNumberEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	.section	.text._ZN2v88internal6BigInt10FromObjectEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6BigInt10FromObjectEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	.type	_ZN2v88internal6BigInt10FromObjectEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE, @function
_ZN2v88internal6BigInt10FromObjectEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE:
.LFB17848:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L366
.L358:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$20, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
.L365:
	movq	(%rax), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
.L364:
	xorl	%r13d, %r13d
.L347:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L367
	addq	$16, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L366:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	ja	.L368
.L345:
	movq	(%rdx), %rax
	movq	%rax, %rsi
	notq	%rsi
	testb	$1, %al
	je	.L358
	movq	-1(%rax), %rcx
	cmpw	$67, 11(%rcx)
	je	.L369
.L349:
	andl	$1, %esi
	jne	.L358
	movq	-1(%rax), %rcx
	movq	%rdx, %r13
	cmpw	$66, 11(%rcx)
	je	.L347
	testb	$1, %al
	je	.L358
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L358
	movq	%rdx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal14StringToBigIntEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	testq	%rax, %rax
	je	.L370
	movq	%rax, %r13
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L368:
	movq	%rsi, %rdi
	movl	$1, %esi
	call	_ZN2v88internal10JSReceiver11ToPrimitiveENS0_6HandleIS1_EENS0_15ToPrimitiveHintE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	jne	.L345
	jmp	.L364
	.p2align 4,,10
	.p2align 3
.L369:
	testb	$-2, 43(%rax)
	jne	.L349
	leaq	-32(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal6Object12BooleanValueEPNS0_7IsolateE@PLT
	testb	%al, %al
	jne	.L351
	movq	%r12, %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZN2v88internal7Factory9NewBigIntEiNS0_14AllocationTypeE@PLT
	movq	%rax, %r13
	movq	(%rax), %rax
	movl	$0, 7(%rax)
	movq	0(%r13), %rdi
	call	_ZN2v88internal13MutableBigInt12CanonicalizeES1_
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L370:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movl	$20, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory14NewSyntaxErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L351:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal13MutableBigInt10NewFromIntEPNS0_7IsolateEi.part.0
	movq	%rax, %r13
	jmp	.L347
.L367:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17848:
	.size	_ZN2v88internal6BigInt10FromObjectEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE, .-_ZN2v88internal6BigInt10FromObjectEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	.section	.text._ZN2v88internal13MutableBigInt14DecideRoundingENS0_6HandleINS0_10BigIntBaseEEEiim,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13MutableBigInt14DecideRoundingENS0_6HandleINS0_10BigIntBaseEEEiim
	.type	_ZN2v88internal13MutableBigInt14DecideRoundingENS0_6HandleINS0_10BigIntBaseEEEiim, @function
_ZN2v88internal13MutableBigInt14DecideRoundingENS0_6HandleINS0_10BigIntBaseEEEiim:
.LFB17851:
	.cfi_startproc
	endbr64
	movq	%rcx, %rax
	xorl	%r8d, %r8d
	testl	%esi, %esi
	jg	.L371
	je	.L373
	notl	%esi
	movl	$1, %r8d
	movl	%esi, %ecx
	salq	%cl, %r8
	movq	%r8, %rcx
.L374:
	xorl	%r8d, %r8d
	testq	%rcx, %rax
	je	.L371
	subq	$1, %rcx
	movl	$2, %r8d
	testq	%rax, %rcx
	jne	.L371
	leal	8(,%rdx,8), %eax
	cltq
	jmp	.L375
	.p2align 4,,10
	.p2align 3
.L386:
	movq	(%rdi), %rcx
	subl	$1, %edx
	subq	$8, %rax
	movq	7(%rax,%rcx), %rcx
	testq	%rcx, %rcx
	jne	.L385
.L375:
	testl	%edx, %edx
	jg	.L386
	movl	$1, %r8d
.L371:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L373:
	movl	%esi, %r8d
	testl	%edx, %edx
	je	.L371
	movq	(%rdi), %rcx
	leal	8(,%rdx,8), %eax
	subl	$1, %edx
	cltq
	movq	-1(%rax,%rcx), %rax
	movabsq	$-9223372036854775808, %rcx
	jmp	.L374
	.p2align 4,,10
	.p2align 3
.L385:
	movl	$2, %r8d
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE17851:
	.size	_ZN2v88internal13MutableBigInt14DecideRoundingENS0_6HandleINS0_10BigIntBaseEEEiim, .-_ZN2v88internal13MutableBigInt14DecideRoundingENS0_6HandleINS0_10BigIntBaseEEEiim
	.section	.text._ZN2v88internal13MutableBigInt8ToDoubleENS0_6HandleINS0_10BigIntBaseEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13MutableBigInt8ToDoubleENS0_6HandleINS0_10BigIntBaseEEE
	.type	_ZN2v88internal13MutableBigInt8ToDoubleENS0_6HandleINS0_10BigIntBaseEEE, @function
_ZN2v88internal13MutableBigInt8ToDoubleENS0_6HandleINS0_10BigIntBaseEEE:
.LFB17850:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	pxor	%xmm0, %xmm0
	movl	7(%rdx), %eax
	testl	$2147483646, %eax
	jne	.L427
	ret
	.p2align 4,,10
	.p2align 3
.L427:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	7(%rdx), %eax
	shrl	%eax
	leal	8(,%rax,8), %esi
	movl	%eax, %ebx
	movslq	%esi, %rax
	andl	$1073741823, %ebx
	movq	-1(%rdx,%rax), %r8
	testq	%r8, %r8
	je	.L389
	bsrq	%r8, %rax
	movl	%ebx, %r9d
	leaq	7(%rdx), %r11
	xorq	$63, %rax
	sall	$6, %r9d
	subl	%eax, %r9d
	movl	%eax, %r12d
	cmpl	$1024, %r9d
	jg	.L399
	subl	$1, %r9d
	leal	1(%rax), %ecx
	movslq	%r9d, %r9
	cmpl	$63, %eax
	jne	.L398
	movl	$63, %r12d
	xorl	%r10d, %r10d
	leal	-1(%rbx), %r14d
	leal	-11(%r12), %r13d
	testl	%r13d, %r13d
	jg	.L428
	.p2align 4,,10
	.p2align 3
.L393:
	movq	%r8, %rcx
	movl	%r14d, %edx
	movl	%r13d, %esi
	call	_ZN2v88internal13MutableBigInt14DecideRoundingENS0_6HandleINS0_10BigIntBaseEEEiim
	cmpl	$2, %eax
	je	.L394
	cmpl	$1, %eax
	je	.L429
.L395:
	movl	(%r11), %eax
	addq	$1023, %r9
	salq	$52, %r9
	salq	$63, %rax
	orq	%r10, %r9
	orq	%rax, %r9
	movq	%r9, %xmm0
.L387:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L389:
	.cfi_restore_state
	movl	%ebx, %r9d
	leaq	7(%rdx), %r11
	sall	$6, %r9d
	leal	-64(%r9), %eax
	cmpl	$1024, %eax
	jg	.L399
	subl	$65, %r9d
	movl	$64, %r12d
	movl	$65, %ecx
	movslq	%r9d, %r9
.L398:
	movq	%r8, %r10
	leal	-11(%r12), %r13d
	leal	-1(%rbx), %r14d
	salq	%cl, %r10
	shrq	$12, %r10
	testl	%r13d, %r13d
	jle	.L393
.L428:
	testl	%r14d, %r14d
	jle	.L393
	subl	$8, %esi
	movl	$64, %ecx
	leal	-2(%rbx), %r14d
	movslq	%esi, %rsi
	subl	%r13d, %ecx
	leal	-75(%r12), %r13d
	movq	-1(%rdx,%rsi), %r8
	movq	%r8, %rax
	shrq	%cl, %rax
	orq	%rax, %r10
	jmp	.L393
	.p2align 4,,10
	.p2align 3
.L429:
	testb	$1, %r10b
	je	.L395
.L394:
	addq	$1, %r10
	movq	%r10, %rax
	shrq	$52, %rax
	je	.L395
	addq	$1, %r9
	xorl	%r10d, %r10d
	cmpq	$1023, %r9
	jbe	.L395
	.p2align 4,,10
	.p2align 3
.L399:
	movl	(%r11), %eax
	movsd	.LC4(%rip), %xmm0
	testb	$1, %al
	je	.L387
	movsd	.LC5(%rip), %xmm0
	jmp	.L387
	.cfi_endproc
.LFE17850:
	.size	_ZN2v88internal13MutableBigInt8ToDoubleENS0_6HandleINS0_10BigIntBaseEEE, .-_ZN2v88internal13MutableBigInt8ToDoubleENS0_6HandleINS0_10BigIntBaseEEE
	.section	.text._ZN2v88internal6BigInt8ToNumberEPNS0_7IsolateENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6BigInt8ToNumberEPNS0_7IsolateENS0_6HandleIS1_EE
	.type	_ZN2v88internal6BigInt8ToNumberEPNS0_7IsolateENS0_6HandleIS1_EE, @function
_ZN2v88internal6BigInt8ToNumberEPNS0_7IsolateENS0_6HandleIS1_EE:
.LFB17849:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -32
	movq	(%rsi), %rax
	movl	7(%rax), %edx
	andl	$2147483646, %edx
	jne	.L431
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L432
	xorl	%esi, %esi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L435:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L432:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L444
.L434:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	$0, (%rax)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L431:
	.cfi_restore_state
	movl	7(%rax), %edx
	shrl	%edx
	andl	$1073741823, %edx
	cmpl	$1, %edx
	je	.L445
.L436:
	movq	%rsi, %rdi
	call	_ZN2v88internal13MutableBigInt8ToDoubleENS0_6HandleINS0_10BigIntBaseEEE
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%xmm0, %rbx
	call	_ZN2v88internal7Factory13NewHeapNumberENS0_14AllocationTypeE@PLT
	movq	(%rax), %rdx
	movq	%rbx, 7(%rdx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L445:
	.cfi_restore_state
	movq	15(%rax), %rdx
	cmpq	$2147483646, %rdx
	ja	.L436
	movl	7(%rax), %eax
	movl	%edx, %esi
	movq	41112(%rdi), %rdi
	negl	%esi
	testb	$1, %al
	cmove	%edx, %esi
	salq	$32, %rsi
	testq	%rdi, %rdi
	je	.L446
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L444:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L434
	.p2align 4,,10
	.p2align 3
.L446:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L447
.L441:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L435
.L447:
	movq	%r12, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L441
	.cfi_endproc
.LFE17849:
	.size	_ZN2v88internal6BigInt8ToNumberEPNS0_7IsolateENS0_6HandleIS1_EE, .-_ZN2v88internal6BigInt8ToNumberEPNS0_7IsolateENS0_6HandleIS1_EE
	.section	.rodata._ZN2v88internal6BigInt16BigIntShortPrintERSo.str1.1,"aMS",@progbits,1
.LC10:
	.string	"-"
.LC11:
	.string	"0"
.LC12:
	.string	"..."
	.section	.text._ZN2v88internal6BigInt16BigIntShortPrintERSo,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6BigInt16BigIntShortPrintERSo
	.type	_ZN2v88internal6BigInt16BigIntShortPrintERSo, @function
_ZN2v88internal6BigInt16BigIntShortPrintERSo:
.LFB17852:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	movl	7(%rax), %edx
	andl	$1, %edx
	jne	.L459
	movl	7(%rax), %edx
	movl	%edx, %ecx
	shrl	%ecx
	andl	$2147483646, %edx
	je	.L460
.L450:
	andl	$1073741822, %ecx
	jne	.L461
	movq	15(%rax), %rsi
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNSo9_M_insertImEERSoT_@PLT
	.p2align 4,,10
	.p2align 3
.L459:
	.cfi_restore_state
	movl	$1, %edx
	leaq	.LC10(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movl	7(%rax), %edx
	movl	%edx, %ecx
	shrl	%ecx
	andl	$2147483646, %edx
	jne	.L450
.L460:
	popq	%rbx
	movq	%r12, %rdi
	movl	$1, %edx
	popq	%r12
	leaq	.LC11(%rip), %rsi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	.p2align 4,,10
	.p2align 3
.L461:
	.cfi_restore_state
	movq	%r12, %rdi
	leaq	.LC12(%rip), %rsi
	movl	$3, %edx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	15(%rax), %rsi
	jmp	_ZNSo9_M_insertImEERSoT_@PLT
	.cfi_endproc
.LFE17852:
	.size	_ZN2v88internal6BigInt16BigIntShortPrintERSo, .-_ZN2v88internal6BigInt16BigIntShortPrintERSo
	.section	.text._ZN2v88internal13MutableBigInt11AbsoluteAddES1_NS0_6BigIntES2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13MutableBigInt11AbsoluteAddES1_NS0_6BigIntES2_
	.type	_ZN2v88internal13MutableBigInt11AbsoluteAddES1_NS0_6BigIntES2_, @function
_ZN2v88internal13MutableBigInt11AbsoluteAddES1_NS0_6BigIntES2_:
.LFB17853:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r9
	leaq	7(%rdx), %r11
	leaq	-1(%rdi), %rax
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-8(%rdx), %rbx
	leaq	-8(%r9), %rdx
	subq	%rdi, %rdx
	subq	%rdi, %rbx
	movq	%rdx, %r8
	subq	$16, %rsp
	movq	%rsi, -136(%rbp)
	leaq	15(%rdi), %rsi
	movq	%rax, -120(%rbp)
	xorl	%eax, %eax
	movq	%r13, -168(%rbp)
	movq	%r11, %r13
	movq	%rdi, -128(%rbp)
	movq	%r14, -160(%rbp)
	movq	%r15, -152(%rbp)
	movq	%r12, -176(%rbp)
	jmp	.L465
	.p2align 4,,10
	.p2align 3
.L463:
	movq	(%rbx,%rsi), %rdi
	movq	(%r8,%rsi), %rdx
	movq	%rax, -64(%rbp)
	movq	(%r8,%rsi), %r14
	movq	-64(%rbp), %r10
	movq	$0, -56(%rbp)
	addq	%rdi, %rdx
	movq	-56(%rbp), %r11
	movq	%rdi, -112(%rbp)
	movq	%rdx, -80(%rbp)
	addq	-80(%rbp), %r10
	movq	$0, -72(%rbp)
	adcq	-72(%rbp), %r11
	movq	%r11, %r15
	movq	%r14, -96(%rbp)
	movq	-96(%rbp), %r10
	movq	$0, -88(%rbp)
	addq	-112(%rbp), %r10
	movq	$0, -104(%rbp)
	movq	-88(%rbp), %r11
	adcq	-104(%rbp), %r11
	addq	%rax, %rdx
	addl	$1, %ecx
	movq	%rdx, (%r9)
	leaq	(%r15,%r11), %rax
.L465:
	movl	0(%r13), %edx
	movq	%rsi, %r9
	addq	$8, %rsi
	shrl	%edx
	andl	$1073741823, %edx
	cmpl	%edx, %ecx
	jl	.L463
	movq	-136(%rbp), %rbx
	leal	16(,%rcx,8), %r10d
	movslq	%r10d, %r10
	addq	-120(%rbp), %r10
	leaq	-8(%rbx), %rsi
	leaq	7(%rbx), %rdi
	subq	-128(%rbp), %rsi
	jmp	.L464
	.p2align 4,,10
	.p2align 3
.L468:
	movq	(%rsi,%r10), %rdx
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
	movq	%r15, %r9
	movq	%rdx, %r8
	addq	%rax, %r8
	adcq	%r13, %r9
	addq	%rax, %rdx
	addl	$1, %ecx
	movq	%rdx, (%r11)
	movq	%r9, %rax
.L464:
	movl	(%rdi), %edx
	movq	%r10, %r11
	addq	$8, %r10
	shrl	%edx
	andl	$1073741823, %edx
	cmpl	%edx, %ecx
	jl	.L468
	movq	%rax, (%r11)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17853:
	.size	_ZN2v88internal13MutableBigInt11AbsoluteAddES1_NS0_6BigIntES2_, .-_ZN2v88internal13MutableBigInt11AbsoluteAddES1_NS0_6BigIntES2_
	.section	.text._ZN2v88internal13MutableBigInt11AbsoluteAddEPNS0_7IsolateENS0_6HandleINS0_6BigIntEEES6_b,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13MutableBigInt11AbsoluteAddEPNS0_7IsolateENS0_6HandleINS0_6BigIntEEES6_b
	.type	_ZN2v88internal13MutableBigInt11AbsoluteAddEPNS0_7IsolateENS0_6HandleINS0_6BigIntEEES6_b, @function
_ZN2v88internal13MutableBigInt11AbsoluteAddEPNS0_7IsolateENS0_6HandleINS0_6BigIntEEES6_b:
.LFB17854:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%ecx, %ebx
	movq	(%rsi), %rcx
	movl	7(%rcx), %r8d
	movq	(%rdx), %r9
	movl	7(%r9), %edx
	shrl	%r8d
	andl	$1073741823, %r8d
	shrl	%edx
	andl	$1073741823, %edx
	cmpl	%edx, %r8d
	jge	.L470
	movzbl	%bl, %ecx
	movq	%rsi, %rdx
	popq	%rbx
	movq	%r13, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal13MutableBigInt11AbsoluteAddEPNS0_7IsolateENS0_6HandleINS0_6BigIntEEES6_b
	.p2align 4,,10
	.p2align 3
.L470:
	.cfi_restore_state
	movl	7(%rcx), %eax
	testl	$2147483646, %eax
	jne	.L471
	movq	%rsi, %rax
.L472:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L471:
	.cfi_restore_state
	movl	7(%r9), %eax
	testl	$2147483646, %eax
	jne	.L473
	movl	7(%rcx), %eax
	andl	$1, %eax
	cmpb	%al, %bl
	jne	.L477
.L474:
	movq	%rsi, %rax
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L473:
	movl	7(%rcx), %esi
	shrl	%esi
	andl	$1073741823, %esi
	addl	$1, %esi
	call	_ZN2v88internal13MutableBigInt3NewEPNS0_7IsolateEiNS0_14AllocationTypeE.constprop.0
	movq	%rax, %r14
	xorl	%eax, %eax
	testq	%r14, %r14
	je	.L472
	movq	0(%r13), %rdx
	movq	(%r12), %rsi
	movq	(%r14), %rdi
	call	_ZN2v88internal13MutableBigInt11AbsoluteAddES1_NS0_6BigIntES2_
	movq	(%r14), %rdx
	movzbl	%bl, %ecx
	movl	7(%rdx), %eax
	andl	$-2, %eax
	orl	%ecx, %eax
	movl	%eax, 7(%rdx)
	movq	(%r14), %rdi
	call	_ZN2v88internal13MutableBigInt12CanonicalizeES1_
	movq	%r14, %rax
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L477:
	call	_ZN2v88internal6BigInt10UnaryMinusEPNS0_7IsolateENS0_6HandleIS1_EE
	movq	%rax, %rsi
	jmp	.L474
	.cfi_endproc
.LFE17854:
	.size	_ZN2v88internal13MutableBigInt11AbsoluteAddEPNS0_7IsolateENS0_6HandleINS0_6BigIntEEES6_b, .-_ZN2v88internal13MutableBigInt11AbsoluteAddEPNS0_7IsolateENS0_6HandleINS0_6BigIntEEES6_b
	.section	.text._ZN2v88internal13MutableBigInt11AbsoluteSubES1_NS0_6BigIntES2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13MutableBigInt11AbsoluteSubES1_NS0_6BigIntES2_
	.type	_ZN2v88internal13MutableBigInt11AbsoluteSubES1_NS0_6BigIntES2_, @function
_ZN2v88internal13MutableBigInt11AbsoluteSubES1_NS0_6BigIntES2_:
.LFB17856:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r11
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	subq	%rdx, %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	7(%rdx), %rbx
	movq	%rdi, -96(%rbp)
	subq	%rdx, %rdi
	movq	%rsi, -104(%rbp)
	leaq	15(%rdx), %rsi
	movq	%r9, -120(%rbp)
	movq	%r11, %r9
	movq	%rdi, -88(%rbp)
	movq	%r8, -128(%rbp)
	jmp	.L481
	.p2align 4,,10
	.p2align 3
.L479:
	xorl	%r13d, %r13d
	movq	(%r9,%rsi), %rdx
	movq	(%rsi), %r8
	movq	%rcx, -64(%rbp)
	subq	(%rsi), %rdx
	movq	$0, -56(%rbp)
	movq	%r13, %r11
	cmpq	-64(%rbp), %rdx
	sbbq	-56(%rbp), %r11
	xorl	%r15d, %r15d
	movq	%r8, -80(%rbp)
	movq	%r11, %rdi
	movq	(%r9,%rsi), %r14
	movq	$0, -72(%rbp)
	movq	%r15, %r11
	andl	$1, %edi
	cmpq	-80(%rbp), %r14
	sbbq	-72(%rbp), %r11
	subq	%rcx, %rdx
	movq	%r11, %r10
	movq	-88(%rbp), %rcx
	addl	$1, %eax
	andl	$1, %r10d
	movq	%rdx, (%rcx,%rsi)
	addq	$8, %rsi
	leaq	(%rdi,%r10), %rcx
.L481:
	movl	(%rbx), %edx
	shrl	%edx
	andl	$1073741823, %edx
	cmpl	%edx, %eax
	jl	.L479
	movq	-104(%rbp), %r15
	movq	-96(%rbp), %rdi
	leal	16(,%rax,8), %edx
	movslq	%edx, %rdx
	leaq	7(%r15), %rbx
	leaq	-1(%r15,%rdx), %rsi
	subq	%r15, %rdi
	jmp	.L480
	.p2align 4,,10
	.p2align 3
.L484:
	movq	(%rsi), %rdx
	xorl	%r9d, %r9d
	xorl	%r11d, %r11d
	movq	%r9, %r13
	cmpq	%rcx, %rdx
	sbbq	%r11, %r13
	subq	%rcx, %rdx
	addl	$1, %eax
	movq	%r13, %rcx
	movq	%rdx, (%rdi,%rsi)
	addq	$8, %rsi
	andl	$1, %ecx
.L480:
	movl	(%rbx), %edx
	shrl	%edx
	andl	$1073741823, %edx
	cmpl	%edx, %eax
	jl	.L484
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17856:
	.size	_ZN2v88internal13MutableBigInt11AbsoluteSubES1_NS0_6BigIntES2_, .-_ZN2v88internal13MutableBigInt11AbsoluteSubES1_NS0_6BigIntES2_
	.section	.text._ZN2v88internal13MutableBigInt11AbsoluteSubEPNS0_7IsolateENS0_6HandleINS0_6BigIntEEES6_b,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13MutableBigInt11AbsoluteSubEPNS0_7IsolateENS0_6HandleINS0_6BigIntEEES6_b
	.type	_ZN2v88internal13MutableBigInt11AbsoluteSubEPNS0_7IsolateENS0_6HandleINS0_6BigIntEEES6_b, @function
_ZN2v88internal13MutableBigInt11AbsoluteSubEPNS0_7IsolateENS0_6HandleINS0_6BigIntEEES6_b:
.LFB17855:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	(%rsi), %rax
	movl	7(%rax), %esi
	andl	$2147483646, %esi
	jne	.L486
.L493:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L486:
	.cfi_restore_state
	movl	%ecx, %ebx
	movq	(%rdx), %rcx
	movl	7(%rcx), %ecx
	andl	$2147483646, %ecx
	jne	.L488
	movl	7(%rax), %eax
	andl	$1, %eax
	cmpb	%al, %bl
	je	.L493
	addq	$24, %rsp
	movq	%r12, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6BigInt10UnaryMinusEPNS0_7IsolateENS0_6HandleIS1_EE
	.p2align 4,,10
	.p2align 3
.L488:
	.cfi_restore_state
	movq	%rdx, -40(%rbp)
	movl	7(%rax), %esi
	shrl	%esi
	andl	$1073741823, %esi
	call	_ZN2v88internal13MutableBigInt3NewEPNS0_7IsolateEiNS0_14AllocationTypeE.constprop.0
	movq	-40(%rbp), %rdx
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L494
	movq	(%r12), %rsi
	movq	(%rax), %rdi
	movq	(%rdx), %rdx
	call	_ZN2v88internal13MutableBigInt11AbsoluteSubES1_NS0_6BigIntES2_
	movq	0(%r13), %rdx
	movl	7(%rdx), %ecx
	movl	%ecx, %eax
	movzbl	%bl, %ecx
	andl	$-2, %eax
	orl	%eax, %ecx
	movl	%ecx, 7(%rdx)
	movq	0(%r13), %rdi
	call	_ZN2v88internal13MutableBigInt12CanonicalizeES1_
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L494:
	.cfi_restore_state
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE17855:
	.size	_ZN2v88internal13MutableBigInt11AbsoluteSubEPNS0_7IsolateENS0_6HandleINS0_6BigIntEEES6_b, .-_ZN2v88internal13MutableBigInt11AbsoluteSubEPNS0_7IsolateENS0_6HandleINS0_6BigIntEEES6_b
	.section	.text._ZN2v88internal13MutableBigInt14AbsoluteAddOneEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEEbS1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13MutableBigInt14AbsoluteAddOneEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEEbS1_
	.type	_ZN2v88internal13MutableBigInt14AbsoluteAddOneEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEEbS1_, @function
_ZN2v88internal13MutableBigInt14AbsoluteAddOneEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEEbS1_:
.LFB17857:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movl	%edx, -52(%rbp)
	movq	(%rsi), %rdx
	movl	7(%rdx), %r8d
	shrl	%r8d
	andl	$1073741823, %r8d
	je	.L508
	leal	-1(%r8), %esi
	leaq	15(%rdx), %rax
	leaq	23(%rdx,%rsi,8), %rdx
	jmp	.L497
	.p2align 4,,10
	.p2align 3
.L515:
	addq	$8, %rax
	cmpq	%rax, %rdx
	je	.L514
.L497:
	cmpq	$-1, (%rax)
	je	.L515
	movl	%r8d, %r9d
.L496:
	movq	41112(%r10), %rdi
	testq	%rdi, %rdi
	je	.L498
	movq	%rcx, %rsi
	movq	%r10, -80(%rbp)
	movl	%r9d, -68(%rbp)
	movl	%r8d, -56(%rbp)
	movq	%rcx, -64(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-64(%rbp), %rcx
	movl	-56(%rbp), %r8d
	movl	-68(%rbp), %r9d
	movq	-80(%rbp), %r10
	testq	%rcx, %rcx
	je	.L516
.L501:
	testl	%r8d, %r8d
	je	.L504
.L518:
	leal	-1(%r8), %edx
	movl	%r9d, -64(%rbp)
	movl	$1, %esi
	movl	%r8d, %r9d
	leaq	24(,%rdx,8), %r11
	movq	%rbx, %r8
	movl	$16, %edx
	movq	%r11, %rbx
	.p2align 4,,10
	.p2align 3
.L505:
	movq	(%r8), %rcx
	xorl	%r13d, %r13d
	movq	%rsi, %rdi
	xorl	%r15d, %r15d
	movq	%r13, %r11
	movq	-1(%rdx,%rcx), %rcx
	movq	%rcx, %r10
	addq	%rsi, %r10
	movq	(%rax), %r10
	adcq	%r15, %r11
	addq	%rdi, %rcx
	addq	$8, %rdx
	movq	%rcx, -9(%rdx,%r10)
	movq	%r11, %rsi
	cmpq	%rdx, %rbx
	jne	.L505
	movl	%r9d, %r8d
	movl	-64(%rbp), %r9d
	movq	(%rax), %rcx
	cmpl	%r9d, %r8d
	jl	.L507
.L506:
	movl	7(%rcx), %edx
	movzbl	-52(%rbp), %esi
	andl	$-2, %edx
	orl	%esi, %edx
	movl	%edx, 7(%rcx)
.L512:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L498:
	.cfi_restore_state
	movq	41088(%r10), %rax
	cmpq	41096(%r10), %rax
	je	.L517
.L500:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r10)
	movq	%rcx, (%rax)
	testq	%rcx, %rcx
	jne	.L501
.L516:
	movl	%r9d, %esi
	movq	%r10, %rdi
	movl	%r8d, -56(%rbp)
	movl	%r9d, -64(%rbp)
	call	_ZN2v88internal13MutableBigInt3NewEPNS0_7IsolateEiNS0_14AllocationTypeE.constprop.0
	movl	-64(%rbp), %r9d
	movl	-56(%rbp), %r8d
	testq	%rax, %rax
	je	.L512
	testl	%r8d, %r8d
	jne	.L518
.L504:
	movq	(%rax), %rcx
	movl	$1, %esi
.L507:
	leal	16(,%r8,8), %edx
	movslq	%edx, %rdx
	movq	%rsi, -1(%rcx,%rdx)
	movq	(%rax), %rcx
	jmp	.L506
	.p2align 4,,10
	.p2align 3
.L514:
	leal	1(%r8), %r9d
	jmp	.L496
.L517:
	movq	%r10, %rdi
	movq	%rcx, -80(%rbp)
	movl	%r9d, -68(%rbp)
	movl	%r8d, -56(%rbp)
	movq	%r10, -64(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %rcx
	movl	-68(%rbp), %r9d
	movl	-56(%rbp), %r8d
	movq	-64(%rbp), %r10
	jmp	.L500
.L508:
	movl	$1, %r9d
	jmp	.L496
	.cfi_endproc
.LFE17857:
	.size	_ZN2v88internal13MutableBigInt14AbsoluteAddOneEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEEbS1_, .-_ZN2v88internal13MutableBigInt14AbsoluteAddOneEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEEbS1_
	.section	.text._ZN2v88internal13MutableBigInt14AbsoluteSubOneEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13MutableBigInt14AbsoluteSubOneEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEEi
	.type	_ZN2v88internal13MutableBigInt14AbsoluteSubOneEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEEi, @function
_ZN2v88internal13MutableBigInt14AbsoluteSubOneEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEEi:
.LFB17859:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movl	%edx, %esi
	subq	$24, %rsp
	movl	%edx, -52(%rbp)
	call	_ZN2v88internal13MutableBigInt3NewEPNS0_7IsolateEiNS0_14AllocationTypeE.constprop.0
	movl	-52(%rbp), %edx
	testq	%rax, %rax
	je	.L521
	movq	(%rbx), %rsi
	movl	7(%rsi), %r10d
	shrl	%r10d
	andl	$1073741823, %r10d
	je	.L531
	leal	-1(%r10), %ecx
	movl	$1, %r8d
	leaq	24(,%rcx,8), %r11
	movl	$16, %ecx
	jmp	.L526
	.p2align 4,,10
	.p2align 3
.L532:
	movq	(%rbx), %rsi
.L526:
	movq	-1(%rcx,%rsi), %rsi
	xorl	%r13d, %r13d
	xorl	%r15d, %r15d
	movq	%r8, %rdi
	movq	%r13, %r9
	cmpq	%r8, %rsi
	sbbq	%r15, %r9
	subq	%rdi, %rsi
	addq	$8, %rcx
	movq	%r9, %r8
	movq	(%rax), %r9
	andl	$1, %r8d
	movq	%rsi, -9(%rcx,%r9)
	cmpq	%rcx, %r11
	jne	.L532
.L525:
	cmpl	%r10d, %edx
	jle	.L521
	subl	$1, %edx
	leal	16(,%r10,8), %ecx
	subl	%r10d, %edx
	movslq	%r10d, %r10
	movslq	%ecx, %rcx
	leaq	3(%rdx,%r10), %rsi
	salq	$3, %rsi
	.p2align 4,,10
	.p2align 3
.L527:
	movq	(%rax), %rdx
	addq	$8, %rcx
	movq	%r8, -9(%rcx,%rdx)
	cmpq	%rcx, %rsi
	jne	.L527
.L521:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L531:
	.cfi_restore_state
	movl	$1, %r8d
	jmp	.L525
	.cfi_endproc
.LFE17859:
	.size	_ZN2v88internal13MutableBigInt14AbsoluteSubOneEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEEi, .-_ZN2v88internal13MutableBigInt14AbsoluteSubOneEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEEi
	.section	.text._ZN2v88internal6BigInt10BitwiseNotEPNS0_7IsolateENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6BigInt10BitwiseNotEPNS0_7IsolateENS0_6HandleIS1_EE
	.type	_ZN2v88internal6BigInt10BitwiseNotEPNS0_7IsolateENS0_6HandleIS1_EE, @function
_ZN2v88internal6BigInt10BitwiseNotEPNS0_7IsolateENS0_6HandleIS1_EE:
.LFB17815:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rsi), %rax
	movl	7(%rax), %edx
	andl	$1, %edx
	je	.L534
	movl	7(%rax), %edx
	shrl	%edx
	andl	$1073741823, %edx
	call	_ZN2v88internal13MutableBigInt14AbsoluteSubOneEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEEi
	movq	%rax, %r12
.L535:
	testq	%r12, %r12
	je	.L536
	movq	(%r12), %rdi
	call	_ZN2v88internal13MutableBigInt12CanonicalizeES1_
.L536:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L534:
	.cfi_restore_state
	xorl	%ecx, %ecx
	movl	$1, %edx
	call	_ZN2v88internal13MutableBigInt14AbsoluteAddOneEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEEbS1_
	movq	%rax, %r12
	jmp	.L535
	.cfi_endproc
.LFE17815:
	.size	_ZN2v88internal6BigInt10BitwiseNotEPNS0_7IsolateENS0_6HandleIS1_EE, .-_ZN2v88internal6BigInt10BitwiseNotEPNS0_7IsolateENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal13MutableBigInt14AbsoluteSubOneEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13MutableBigInt14AbsoluteSubOneEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEE
	.type	_ZN2v88internal13MutableBigInt14AbsoluteSubOneEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEE, @function
_ZN2v88internal13MutableBigInt14AbsoluteSubOneEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEE:
.LFB17858:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rsi), %rax
	movl	7(%rax), %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	shrl	%edx
	andl	$1073741823, %edx
	call	_ZN2v88internal13MutableBigInt14AbsoluteSubOneEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEEi
	testq	%rax, %rax
	je	.L544
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L544:
	.cfi_restore_state
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE17858:
	.size	_ZN2v88internal13MutableBigInt14AbsoluteSubOneEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEE, .-_ZN2v88internal13MutableBigInt14AbsoluteSubOneEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEE
	.section	.text._ZN2v88internal6BigInt9IncrementEPNS0_7IsolateENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6BigInt9IncrementEPNS0_7IsolateENS0_6HandleIS1_EE
	.type	_ZN2v88internal6BigInt9IncrementEPNS0_7IsolateENS0_6HandleIS1_EE, @function
_ZN2v88internal6BigInt9IncrementEPNS0_7IsolateENS0_6HandleIS1_EE:
.LFB17839:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rsi), %rax
	movl	7(%rax), %edx
	andl	$1, %edx
	je	.L546
	movl	7(%rax), %edx
	shrl	%edx
	andl	$1073741823, %edx
	call	_ZN2v88internal13MutableBigInt14AbsoluteSubOneEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEEi
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L554
	movq	(%rax), %rdx
	movl	7(%rdx), %eax
	orl	$1, %eax
	movl	%eax, 7(%rdx)
	movq	(%rbx), %rdi
	call	_ZN2v88internal13MutableBigInt12CanonicalizeES1_
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L546:
	.cfi_restore_state
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	_ZN2v88internal13MutableBigInt14AbsoluteAddOneEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEEbS1_
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L549
	movq	(%rax), %rdi
	call	_ZN2v88internal13MutableBigInt12CanonicalizeES1_
.L549:
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L554:
	.cfi_restore_state
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE17839:
	.size	_ZN2v88internal6BigInt9IncrementEPNS0_7IsolateENS0_6HandleIS1_EE, .-_ZN2v88internal6BigInt9IncrementEPNS0_7IsolateENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal6BigInt9DecrementEPNS0_7IsolateENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6BigInt9DecrementEPNS0_7IsolateENS0_6HandleIS1_EE
	.type	_ZN2v88internal6BigInt9DecrementEPNS0_7IsolateENS0_6HandleIS1_EE, @function
_ZN2v88internal6BigInt9DecrementEPNS0_7IsolateENS0_6HandleIS1_EE:
.LFB17840:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rsi), %rax
	movl	7(%rax), %edx
	andl	$1, %edx
	je	.L556
	xorl	%ecx, %ecx
	movl	$1, %edx
	call	_ZN2v88internal13MutableBigInt14AbsoluteAddOneEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEEbS1_
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L557
.L558:
	movq	(%rbx), %rdi
	call	_ZN2v88internal13MutableBigInt12CanonicalizeES1_
.L557:
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L556:
	.cfi_restore_state
	movl	7(%rax), %edx
	andl	$2147483646, %edx
	jne	.L559
	xorl	%edx, %edx
	movl	$1, %esi
	call	_ZN2v88internal7Factory9NewBigIntEiNS0_14AllocationTypeE@PLT
	movq	%rax, %rbx
	movq	(%rax), %rax
	movl	$3, 7(%rax)
	movq	(%rbx), %rax
	movq	$1, 15(%rax)
	movq	(%rbx), %rdi
	call	_ZN2v88internal13MutableBigInt12CanonicalizeES1_
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L559:
	.cfi_restore_state
	movl	7(%rax), %edx
	shrl	%edx
	andl	$1073741823, %edx
	call	_ZN2v88internal13MutableBigInt14AbsoluteSubOneEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEEi
	movq	%rax, %rbx
	testq	%rax, %rax
	jne	.L558
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE17840:
	.size	_ZN2v88internal6BigInt9DecrementEPNS0_7IsolateENS0_6HandleIS1_EE, .-_ZN2v88internal6BigInt9DecrementEPNS0_7IsolateENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal13MutableBigInt9BitwiseOrEPNS0_7IsolateENS0_6HandleINS0_6BigIntEEES6_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13MutableBigInt9BitwiseOrEPNS0_7IsolateENS0_6HandleINS0_6BigIntEEES6_
	.type	_ZN2v88internal13MutableBigInt9BitwiseOrEPNS0_7IsolateENS0_6HandleINS0_6BigIntEEES6_, @function
_ZN2v88internal13MutableBigInt9BitwiseOrEPNS0_7IsolateENS0_6HandleINS0_6BigIntEEES6_:
.LFB17838:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 3, -56
	movq	(%rdx), %rcx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	7(%rcx), %edx
	movq	(%rsi), %rdi
	movl	7(%rdi), %eax
	shrl	%edx
	andl	$1073741823, %edx
	shrl	%eax
	andl	$1073741823, %eax
	cmpl	%eax, %edx
	cmovl	%eax, %edx
	movl	7(%rdi), %eax
	testb	$1, %al
	jne	.L567
	movl	7(%rcx), %eax
	testb	$1, %al
	je	.L658
.L567:
	movl	7(%rdi), %eax
	testb	$1, %al
	jne	.L581
.L584:
	movl	7(%rdi), %eax
	movq	%r12, %r14
	movq	%r13, %rdi
	andl	$1, %eax
	cmove	%rsi, %r14
	cmove	%r12, %rsi
	call	_ZN2v88internal13MutableBigInt14AbsoluteSubOneEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEEi
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L585
	movq	(%rax), %r15
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal13MutableBigInt14AbsoluteAndNotEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation(%rip), %rbx
	leaq	_ZNSt17_Function_handlerIFmmmEZN2v88internal13MutableBigInt14AbsoluteAndNotEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E9_M_invokeERKSt9_Any_dataOmSE_(%rip), %rax
	movq	%rbx, %xmm0
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	movq	(%r9), %rax
	movl	7(%rax), %r12d
	movq	(%r14), %rax
	movl	7(%rax), %ebx
	shrl	%r12d
	movq	41112(%r13), %rdi
	andl	$1073741823, %r12d
	shrl	%ebx
	andl	$1073741823, %ebx
	cmpl	%r12d, %ebx
	cmovg	%r12d, %ebx
	testq	%rdi, %rdi
	je	.L599
	movq	%r15, %rsi
	movq	%r9, -216(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-216(%rbp), %r9
	movq	%rax, %r8
	testq	%r15, %r15
	jne	.L602
.L662:
	movl	%r12d, %esi
	movq	%r13, %rdi
	movq	%r9, -216(%rbp)
	call	_ZN2v88internal13MutableBigInt3NewEPNS0_7IsolateEiNS0_14AllocationTypeE.constprop.0
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L585
	movl	%r12d, -248(%rbp)
	movq	-216(%rbp), %r9
.L603:
	movl	$16, %ecx
	xorl	%r10d, %r10d
	leaq	-176(%rbp), %r15
	testl	%ebx, %ebx
	je	.L607
	.p2align 4,,10
	.p2align 3
.L604:
	movq	(%r9), %rsi
	movq	(%r14), %rax
	movl	%r10d, -244(%rbp)
	movq	(%r8), %r11
	cmpq	$0, -80(%rbp)
	movq	%r8, -240(%rbp)
	movq	-1(%rcx,%rax), %rax
	movq	-1(%rcx,%rsi), %rsi
	movq	%r9, -224(%rbp)
	movq	%r11, -232(%rbp)
	movq	%rcx, -216(%rbp)
	movq	%rsi, -168(%rbp)
	movq	%rax, -176(%rbp)
	je	.L594
	leaq	-168(%rbp), %rsi
	leaq	-96(%rbp), %rdi
	movq	%r15, %rdx
	call	*-72(%rbp)
	movl	-244(%rbp), %r10d
	movq	-216(%rbp), %rcx
	movq	-232(%rbp), %r11
	movq	-224(%rbp), %r9
	addl	$1, %r10d
	movq	-240(%rbp), %r8
	movq	%rax, -1(%r11,%rcx)
	addq	$8, %rcx
	cmpl	%r10d, %ebx
	jne	.L604
.L607:
	cmpl	%r12d, %ebx
	jge	.L659
	leal	-1(%r12), %edx
	leal	16(,%rbx,8), %eax
	subl	%ebx, %edx
	movslq	%ebx, %rbx
	cltq
	leaq	3(%rdx,%rbx), %rdx
	salq	$3, %rdx
	.p2align 4,,10
	.p2align 3
.L610:
	movq	(%r9), %rcx
	addq	$8, %rax
	movq	-9(%rax,%rcx), %rsi
	movq	(%r8), %rcx
	movq	%rsi, -9(%rax,%rcx)
	cmpq	%rax, %rdx
	jne	.L610
.L605:
	cmpl	-248(%rbp), %r12d
	jge	.L608
	movl	-248(%rbp), %r15d
	leal	16(,%r12,8), %eax
	cltq
	subl	$1, %r15d
	subl	%r12d, %r15d
	movslq	%r12d, %r12
	leaq	3(%r15,%r12), %rdx
	salq	$3, %rdx
	.p2align 4,,10
	.p2align 3
.L611:
	movq	(%r8), %rcx
	addq	$8, %rax
	movq	$0, -9(%rax,%rcx)
	cmpq	%rax, %rdx
	jne	.L611
.L608:
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L612
	leaq	-96(%rbp), %rdi
	movq	%r8, -216(%rbp)
	movl	$3, %edx
	movq	%rdi, %rsi
	call	*%rax
	movq	-216(%rbp), %r8
.L612:
	movq	(%r8), %rcx
	movl	$1, %edx
	movq	%r8, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal13MutableBigInt14AbsoluteAddOneEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEEbS1_
.L580:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L660
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L581:
	.cfi_restore_state
	movl	7(%rcx), %eax
	testb	$1, %al
	je	.L584
	movq	%r13, %rdi
	call	_ZN2v88internal13MutableBigInt14AbsoluteSubOneEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEEi
	movq	%rax, %r14
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L585
	movq	(%r12), %rax
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	7(%rax), %edx
	shrl	%edx
	andl	$1073741823, %edx
	call	_ZN2v88internal13MutableBigInt14AbsoluteSubOneEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEEi
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L585
	leaq	_ZNSt17_Function_handlerIFmmmEZN2v88internal13MutableBigInt11AbsoluteAndEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E9_M_invokeERKSt9_Any_dataOmSE_(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal13MutableBigInt11AbsoluteAndEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation(%rip), %rdi
	movq	(%r14), %rdx
	movq	%rax, %xmm3
	movq	%rdi, %xmm0
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	(%r14), %rax
	movl	7(%rax), %eax
	movq	(%r12), %rcx
	movl	7(%rcx), %ecx
	shrl	%eax
	andl	$1073741823, %eax
	shrl	%ecx
	andl	$1073741823, %ecx
	movl	%ecx, -232(%rbp)
	cmpl	%ecx, %eax
	jge	.L586
	movl	%eax, -232(%rbp)
	movq	%r12, %r14
	movq	%rbx, %r12
.L586:
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L587
	movq	%rdx, %rsi
	movq	%rdx, -216(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-216(%rbp), %rdx
	movq	%rax, %r15
.L588:
	testq	%rdx, %rdx
	jne	.L590
	movl	-232(%rbp), %ebx
	movq	%r13, %rdi
	movl	%ebx, %esi
	call	_ZN2v88internal13MutableBigInt3NewEPNS0_7IsolateEiNS0_14AllocationTypeE.constprop.0
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L585
	testl	%ebx, %ebx
	je	.L591
	movl	%ebx, -244(%rbp)
	jmp	.L592
	.p2align 4,,10
	.p2align 3
.L599:
	movq	41088(%r13), %r8
	cmpq	41096(%r13), %r8
	je	.L661
.L601:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r13)
	movq	%r15, (%r8)
	testq	%r15, %r15
	je	.L662
.L602:
	movl	7(%r15), %r15d
	shrl	%r15d
	andl	$1073741823, %r15d
	movl	%r15d, -248(%rbp)
	jmp	.L603
	.p2align 4,,10
	.p2align 3
.L658:
	leaq	_ZNSt17_Function_handlerIFmmmEZN2v88internal13MutableBigInt10AbsoluteOrEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E9_M_invokeERKSt9_Any_dataOmSE_(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal13MutableBigInt10AbsoluteOrEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation(%rip), %rbx
	movq	%rsi, %r14
	movq	%rbx, %xmm0
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -144(%rbp)
	movl	7(%rdi), %r12d
	shrl	%r12d
	movl	%r12d, %eax
	andl	$1073741823, %eax
	movl	%eax, -240(%rbp)
	movl	7(%rcx), %ebx
	shrl	%ebx
	andl	$1073741823, %ebx
	movl	%ebx, %edi
	movl	%ebx, -224(%rbp)
	cmpl	%edi, %eax
	jge	.L568
	movl	%eax, -224(%rbp)
	movq	%r15, %r14
	movq	%rsi, %r15
	movl	%edi, -240(%rbp)
.L568:
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L569
	xorl	%esi, %esi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L570:
	movl	-240(%rbp), %esi
	movq	%r13, %rdi
	call	_ZN2v88internal13MutableBigInt3NewEPNS0_7IsolateEiNS0_14AllocationTypeE.constprop.0
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L585
	movl	-224(%rbp), %edx
	leaq	-208(%rbp), %rax
	movl	$16, %ebx
	xorl	%r13d, %r13d
	movq	%rax, -232(%rbp)
	testl	%edx, %edx
	je	.L577
	.p2align 4,,10
	.p2align 3
.L573:
	movq	(%r15), %rax
	movq	(%r14), %rdx
	cmpq	$0, -144(%rbp)
	movq	(%r12), %r10
	movq	-1(%rbx,%rax), %rax
	movq	-1(%rbx,%rdx), %rdx
	movq	%rax, -208(%rbp)
	movq	%rdx, -200(%rbp)
	je	.L594
	movq	%r10, -216(%rbp)
	movq	-232(%rbp), %rdx
	addl	$1, %r13d
	leaq	-200(%rbp), %rsi
	leaq	-160(%rbp), %rdi
	call	*-136(%rbp)
	movq	-216(%rbp), %r10
	movq	%rax, -1(%r10,%rbx)
	addq	$8, %rbx
	cmpl	%r13d, -224(%rbp)
	jne	.L573
.L577:
	movslq	-224(%rbp), %rbx
	cmpl	%ebx, -240(%rbp)
	jle	.L574
	movl	-240(%rbp), %edx
	leal	16(,%rbx,8), %eax
	cltq
	subl	$1, %edx
	subl	%ebx, %edx
	leaq	3(%rdx,%rbx), %rdi
	salq	$3, %rdi
	.p2align 4,,10
	.p2align 3
.L578:
	movq	(%r14), %rdx
	addq	$8, %rax
	movq	-9(%rax,%rdx), %rsi
	movq	(%r12), %rdx
	movq	%rsi, -9(%rax,%rdx)
	cmpq	%rdi, %rax
	jne	.L578
.L574:
	movq	-144(%rbp), %rax
	testq	%rax, %rax
	je	.L579
	leaq	-160(%rbp), %rdi
	movl	$3, %edx
	movq	%rdi, %rsi
	call	*%rax
.L579:
	movq	%r12, %rax
	jmp	.L580
.L664:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L595:
	cmpl	%edx, -244(%rbp)
	jle	.L591
	movl	-244(%rbp), %ecx
	leal	16(,%rdx,8), %eax
	cltq
	subl	$1, %ecx
	subl	%edx, %ecx
	movslq	%edx, %rdx
	leaq	3(%rcx,%rdx), %rcx
	salq	$3, %rcx
	.p2align 4,,10
	.p2align 3
.L597:
	movq	(%r15), %rdx
	addq	$8, %rax
	movq	$0, -9(%rax,%rdx)
	cmpq	%rax, %rcx
	jne	.L597
.L591:
	movq	-112(%rbp), %rax
	testq	%rax, %rax
	je	.L598
	leaq	-128(%rbp), %rdi
	movl	$3, %edx
	movq	%rdi, %rsi
	call	*%rax
.L598:
	movq	(%r15), %rcx
	movl	$1, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal13MutableBigInt14AbsoluteAddOneEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEEbS1_
	jmp	.L580
	.p2align 4,,10
	.p2align 3
.L585:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L569:
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L663
.L571:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	$0, (%rax)
	jmp	.L570
	.p2align 4,,10
	.p2align 3
.L659:
	movl	%ebx, %r12d
	jmp	.L605
	.p2align 4,,10
	.p2align 3
.L590:
	movl	7(%rdx), %eax
	shrl	%eax
	andl	$1073741823, %eax
	movl	%eax, -244(%rbp)
	movl	-232(%rbp), %eax
	testl	%eax, %eax
	je	.L664
.L592:
	leaq	-192(%rbp), %rax
	xorl	%ebx, %ebx
	movq	%rax, -240(%rbp)
	.p2align 4,,10
	.p2align 3
.L596:
	movq	(%r12), %rax
	movq	(%r14), %rdx
	leaq	16(,%rbx,8), %rcx
	movq	(%r15), %r11
	cmpq	$0, -112(%rbp)
	movq	%rcx, -216(%rbp)
	movq	-1(%rcx,%rax), %rax
	movq	-1(%rcx,%rdx), %rdx
	movq	%r11, -224(%rbp)
	movq	%rdx, -184(%rbp)
	movq	%rax, -192(%rbp)
	je	.L594
	movq	-240(%rbp), %rdx
	leaq	-184(%rbp), %rsi
	leaq	-128(%rbp), %rdi
	call	*-104(%rbp)
	movq	-216(%rbp), %rcx
	movq	-224(%rbp), %r11
	leal	1(%rbx), %edx
	addq	$1, %rbx
	movq	%rax, -1(%r11,%rcx)
	cmpl	%ebx, -232(%rbp)
	jg	.L596
	jmp	.L595
	.p2align 4,,10
	.p2align 3
.L587:
	movq	41088(%r13), %r15
	cmpq	41096(%r13), %r15
	je	.L665
.L589:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r13)
	movq	%rdx, (%r15)
	jmp	.L588
	.p2align 4,,10
	.p2align 3
.L661:
	movq	%r13, %rdi
	movq	%r9, -216(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-216(%rbp), %r9
	movq	%rax, %r8
	jmp	.L601
.L665:
	movq	%r13, %rdi
	movq	%rdx, -216(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-216(%rbp), %rdx
	movq	%rax, %r15
	jmp	.L589
	.p2align 4,,10
	.p2align 3
.L663:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L571
.L594:
	call	_ZSt25__throw_bad_function_callv@PLT
.L660:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17838:
	.size	_ZN2v88internal13MutableBigInt9BitwiseOrEPNS0_7IsolateENS0_6HandleINS0_6BigIntEEES6_, .-_ZN2v88internal13MutableBigInt9BitwiseOrEPNS0_7IsolateENS0_6HandleINS0_6BigIntEEES6_
	.section	.text._ZN2v88internal6BigInt9BitwiseOrEPNS0_7IsolateENS0_6HandleIS1_EES5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6BigInt9BitwiseOrEPNS0_7IsolateENS0_6HandleIS1_EES5_
	.type	_ZN2v88internal6BigInt9BitwiseOrEPNS0_7IsolateENS0_6HandleIS1_EES5_, @function
_ZN2v88internal6BigInt9BitwiseOrEPNS0_7IsolateENS0_6HandleIS1_EES5_:
.LFB17837:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_ZN2v88internal13MutableBigInt9BitwiseOrEPNS0_7IsolateENS0_6HandleINS0_6BigIntEEES6_
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L667
	movq	(%rax), %rdi
	call	_ZN2v88internal13MutableBigInt12CanonicalizeES1_
.L667:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17837:
	.size	_ZN2v88internal6BigInt9BitwiseOrEPNS0_7IsolateENS0_6HandleIS1_EES5_, .-_ZN2v88internal6BigInt9BitwiseOrEPNS0_7IsolateENS0_6HandleIS1_EES5_
	.section	.text._ZN2v88internal13MutableBigInt10BitwiseAndEPNS0_7IsolateENS0_6HandleINS0_6BigIntEEES6_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13MutableBigInt10BitwiseAndEPNS0_7IsolateENS0_6HandleINS0_6BigIntEEES6_
	.type	_ZN2v88internal13MutableBigInt10BitwiseAndEPNS0_7IsolateENS0_6HandleINS0_6BigIntEEES6_, @function
_ZN2v88internal13MutableBigInt10BitwiseAndEPNS0_7IsolateENS0_6HandleINS0_6BigIntEEES6_:
.LFB17834:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movl	7(%rax), %edx
	andl	$1, %edx
	jne	.L673
	movq	0(%r13), %rdx
	movl	7(%rdx), %ecx
	andl	$1, %ecx
	je	.L757
.L673:
	movl	7(%rax), %edx
	andl	$1, %edx
	jne	.L684
.L687:
	movl	7(%rax), %eax
	movq	%r13, %r14
	movq	%r12, %rdi
	andl	$1, %eax
	cmove	%rsi, %r14
	cmove	%r13, %rsi
	movq	(%rsi), %rax
	movl	7(%rax), %edx
	shrl	%edx
	andl	$1073741823, %edx
	call	_ZN2v88internal13MutableBigInt14AbsoluteSubOneEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEEi
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L690
	leaq	_ZNSt17_Function_handlerIFmmmEZN2v88internal13MutableBigInt14AbsoluteAndNotEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E9_M_invokeERKSt9_Any_dataOmSE_(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal13MutableBigInt14AbsoluteAndNotEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation(%rip), %rcx
	movq	%rax, %xmm1
	movq	%rcx, %xmm0
	movq	(%r14), %rax
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	movl	7(%rax), %r13d
	movq	(%r15), %rax
	movl	7(%rax), %ebx
	shrl	%r13d
	movq	41112(%r12), %rdi
	andl	$1073741823, %r13d
	shrl	%ebx
	andl	$1073741823, %ebx
	cmpl	%r13d, %ebx
	cmovg	%r13d, %ebx
	testq	%rdi, %rdi
	je	.L707
	xorl	%esi, %esi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L708:
	movq	%r12, %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal13MutableBigInt3NewEPNS0_7IsolateEiNS0_14AllocationTypeE.constprop.0
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L690
	movl	$16, %ecx
	xorl	%r8d, %r8d
	leaq	-176(%rbp), %rdx
	testl	%ebx, %ebx
	je	.L713
	.p2align 4,,10
	.p2align 3
.L710:
	movq	(%r15), %rax
	movq	(%r14), %rsi
	movl	%r8d, -240(%rbp)
	movq	(%r12), %r9
	cmpq	$0, -80(%rbp)
	movq	%rcx, -224(%rbp)
	movq	-1(%rcx,%rax), %rax
	movq	-1(%rcx,%rsi), %rsi
	movq	%r9, -232(%rbp)
	movq	%rsi, -168(%rbp)
	movq	%rax, -176(%rbp)
	je	.L700
	movq	%rdx, -216(%rbp)
	leaq	-168(%rbp), %rsi
	leaq	-96(%rbp), %rdi
	call	*-72(%rbp)
	movl	-240(%rbp), %r8d
	movq	-224(%rbp), %rcx
	movq	-232(%rbp), %r9
	movq	-216(%rbp), %rdx
	addl	$1, %r8d
	movq	%rax, -1(%r9,%rcx)
	addq	$8, %rcx
	cmpl	%ebx, %r8d
	jne	.L710
.L713:
	cmpl	%r13d, %ebx
	jge	.L711
	subl	$1, %r13d
	leal	16(,%rbx,8), %eax
	subl	%ebx, %r13d
	movslq	%ebx, %rbx
	cltq
	leaq	3(%r13,%rbx), %rdx
	salq	$3, %rdx
	.p2align 4,,10
	.p2align 3
.L714:
	movq	(%r14), %rcx
	addq	$8, %rax
	movq	-9(%rax,%rcx), %rsi
	movq	(%r12), %rcx
	movq	%rsi, -9(%rax,%rcx)
	cmpq	%rax, %rdx
	jne	.L714
.L711:
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L715
	leaq	-96(%rbp), %rdi
	movl	$3, %edx
	movq	%rdi, %rsi
	call	*%rax
.L715:
	movq	%r12, %rax
.L683:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L758
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L684:
	.cfi_restore_state
	movq	0(%r13), %rdx
	movl	7(%rdx), %ecx
	andl	$1, %ecx
	je	.L687
	movl	7(%rdx), %edx
	movl	7(%rax), %eax
	movq	%r12, %rdi
	shrl	%edx
	shrl	%eax
	andl	$1073741823, %eax
	andl	$1073741823, %edx
	cmpl	%eax, %edx
	cmovl	%eax, %edx
	addl	$1, %edx
	call	_ZN2v88internal13MutableBigInt14AbsoluteSubOneEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEEi
	movq	%rax, %r15
	movq	%rax, %rbx
	xorl	%eax, %eax
	testq	%r15, %r15
	je	.L683
	movq	0(%r13), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	7(%rax), %edx
	shrl	%edx
	andl	$1073741823, %edx
	call	_ZN2v88internal13MutableBigInt14AbsoluteSubOneEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEEi
	movq	%rax, %r11
	testq	%rax, %rax
	je	.L690
	leaq	_ZNSt17_Function_handlerIFmmmEZN2v88internal13MutableBigInt10AbsoluteOrEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E9_M_invokeERKSt9_Any_dataOmSE_(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal13MutableBigInt10AbsoluteOrEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation(%rip), %rcx
	movq	(%r15), %rdx
	movq	%rcx, %xmm0
	movq	%rax, %xmm3
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	(%r15), %rax
	movl	7(%rax), %r14d
	movq	(%r11), %rax
	shrl	%r14d
	movl	%r14d, %ecx
	andl	$1073741823, %ecx
	movl	%ecx, -244(%rbp)
	movl	7(%rax), %r13d
	shrl	%r13d
	movl	%r13d, %eax
	andl	$1073741823, %eax
	movl	%eax, -232(%rbp)
	cmpl	%eax, %ecx
	jge	.L691
	movl	%ecx, -232(%rbp)
	movq	%r11, %r15
	movq	%rbx, %r11
	movl	%eax, -244(%rbp)
.L691:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L692
	movq	%rdx, %rsi
	movq	%r11, -224(%rbp)
	movq	%rdx, -216(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-216(%rbp), %rdx
	movq	-224(%rbp), %r11
	movq	%rax, %r14
.L693:
	testq	%rdx, %rdx
	jne	.L695
	movl	-244(%rbp), %ebx
	movq	%r12, %rdi
	movq	%r11, -216(%rbp)
	movl	%ebx, %esi
	call	_ZN2v88internal13MutableBigInt3NewEPNS0_7IsolateEiNS0_14AllocationTypeE.constprop.0
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L690
	movl	%ebx, -248(%rbp)
	movq	-216(%rbp), %r11
	jmp	.L696
	.p2align 4,,10
	.p2align 3
.L707:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L759
.L709:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	$0, (%rax)
	jmp	.L708
	.p2align 4,,10
	.p2align 3
.L757:
	leaq	_ZNSt17_Function_handlerIFmmmEZN2v88internal13MutableBigInt11AbsoluteAndEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E9_M_invokeERKSt9_Any_dataOmSE_(%rip), %rcx
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal13MutableBigInt11AbsoluteAndEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation(%rip), %rdi
	movq	%rsi, %r14
	movq	%rdi, %xmm0
	movq	%rcx, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -144(%rbp)
	movl	7(%rax), %eax
	movl	7(%rdx), %ebx
	shrl	%eax
	shrl	%ebx
	andl	$1073741823, %eax
	andl	$1073741823, %ebx
	cmpl	%ebx, %eax
	jge	.L674
	movq	%r13, %r14
	movq	%rsi, %r15
	movl	%eax, %ebx
.L674:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L675
	xorl	%esi, %esi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L676:
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal13MutableBigInt3NewEPNS0_7IsolateEiNS0_14AllocationTypeE.constprop.0
	movq	%rax, -216(%rbp)
	testq	%rax, %rax
	je	.L690
	testl	%ebx, %ebx
	je	.L679
	leal	-1(%rbx), %eax
	leaq	-208(%rbp), %r13
	movl	$16, %ebx
	leaq	24(,%rax,8), %rax
	movq	%rax, -224(%rbp)
	.p2align 4,,10
	.p2align 3
.L681:
	movq	-216(%rbp), %rax
	movq	(%r14), %rdx
	cmpq	$0, -144(%rbp)
	movq	(%rax), %r12
	movq	(%r15), %rax
	movq	-1(%rbx,%rdx), %rdx
	movq	-1(%rbx,%rax), %rax
	movq	%rdx, -200(%rbp)
	movq	%rax, -208(%rbp)
	je	.L700
	leaq	-200(%rbp), %rsi
	leaq	-160(%rbp), %rdi
	movq	%r13, %rdx
	call	*-136(%rbp)
	movq	%rax, -1(%r12,%rbx)
	addq	$8, %rbx
	cmpq	%rbx, -224(%rbp)
	jne	.L681
.L679:
	movq	-144(%rbp), %rax
	testq	%rax, %rax
	je	.L682
	leaq	-160(%rbp), %rdi
	movl	$3, %edx
	movq	%rdi, %rsi
	call	*%rax
.L682:
	movq	-216(%rbp), %rax
	jmp	.L683
	.p2align 4,,10
	.p2align 3
.L690:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L675:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L760
.L677:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	$0, (%rax)
	jmp	.L676
	.p2align 4,,10
	.p2align 3
.L695:
	movl	7(%rdx), %eax
	shrl	%eax
	andl	$1073741823, %eax
	movl	%eax, -248(%rbp)
.L696:
	leaq	-192(%rbp), %rax
	movl	$16, %ebx
	xorl	%r13d, %r13d
	movq	%rax, -240(%rbp)
	movl	-232(%rbp), %eax
	testl	%eax, %eax
	je	.L701
	.p2align 4,,10
	.p2align 3
.L697:
	movq	(%r11), %rax
	movq	(%r15), %rdx
	movq	%r11, -216(%rbp)
	movq	(%r14), %r10
	cmpq	$0, -112(%rbp)
	movq	-1(%rbx,%rax), %rax
	movq	-1(%rbx,%rdx), %rdx
	movq	%r10, -224(%rbp)
	movq	%rdx, -184(%rbp)
	movq	%rax, -192(%rbp)
	je	.L700
	movq	-240(%rbp), %rdx
	leaq	-184(%rbp), %rsi
	leaq	-128(%rbp), %rdi
	addl	$1, %r13d
	call	*-104(%rbp)
	movq	-224(%rbp), %r10
	movq	-216(%rbp), %r11
	movq	%rax, -1(%r10,%rbx)
	addq	$8, %rbx
	cmpl	%r13d, -232(%rbp)
	jne	.L697
.L701:
	movl	-232(%rbp), %ecx
	cmpl	%ecx, -244(%rbp)
	jle	.L761
	movl	-244(%rbp), %edi
	movslq	%ecx, %r13
	leal	16(,%rcx,8), %eax
	cltq
	leal	-1(%rdi), %edx
	subl	%ecx, %edx
	leaq	3(%rdx,%r13), %rsi
	salq	$3, %rsi
	.p2align 4,,10
	.p2align 3
.L704:
	movq	(%r15), %rdx
	addq	$8, %rax
	movq	-9(%rax,%rdx), %rcx
	movq	(%r14), %rdx
	movq	%rcx, -9(%rax,%rdx)
	cmpq	%rax, %rsi
	jne	.L704
.L698:
	movl	-244(%rbp), %ecx
	cmpl	%ecx, -248(%rbp)
	jle	.L702
	movl	-248(%rbp), %edx
	movslq	%ecx, %rcx
	leal	16(,%rcx,8), %eax
	subl	$1, %edx
	cltq
	subl	%ecx, %edx
	leaq	3(%rdx,%rcx), %rcx
	salq	$3, %rcx
	.p2align 4,,10
	.p2align 3
.L705:
	movq	(%r14), %rdx
	addq	$8, %rax
	movq	$0, -9(%rax,%rdx)
	cmpq	%rax, %rcx
	jne	.L705
.L702:
	movq	-112(%rbp), %rax
	testq	%rax, %rax
	je	.L706
	leaq	-128(%rbp), %rdi
	movl	$3, %edx
	movq	%rdi, %rsi
	call	*%rax
.L706:
	movq	(%r14), %rcx
	movl	$1, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal13MutableBigInt14AbsoluteAddOneEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEEbS1_
	jmp	.L683
	.p2align 4,,10
	.p2align 3
.L759:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L709
	.p2align 4,,10
	.p2align 3
.L692:
	movq	41088(%r12), %r14
	cmpq	41096(%r12), %r14
	je	.L762
.L694:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r12)
	movq	%rdx, (%r14)
	jmp	.L693
	.p2align 4,,10
	.p2align 3
.L760:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L677
	.p2align 4,,10
	.p2align 3
.L761:
	movl	%ecx, -244(%rbp)
	jmp	.L698
.L762:
	movq	%r12, %rdi
	movq	%r11, -224(%rbp)
	movq	%rdx, -216(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-224(%rbp), %r11
	movq	-216(%rbp), %rdx
	movq	%rax, %r14
	jmp	.L694
.L700:
	call	_ZSt25__throw_bad_function_callv@PLT
.L758:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17834:
	.size	_ZN2v88internal13MutableBigInt10BitwiseAndEPNS0_7IsolateENS0_6HandleINS0_6BigIntEEES6_, .-_ZN2v88internal13MutableBigInt10BitwiseAndEPNS0_7IsolateENS0_6HandleINS0_6BigIntEEES6_
	.section	.text._ZN2v88internal6BigInt10BitwiseAndEPNS0_7IsolateENS0_6HandleIS1_EES5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6BigInt10BitwiseAndEPNS0_7IsolateENS0_6HandleIS1_EES5_
	.type	_ZN2v88internal6BigInt10BitwiseAndEPNS0_7IsolateENS0_6HandleIS1_EES5_, @function
_ZN2v88internal6BigInt10BitwiseAndEPNS0_7IsolateENS0_6HandleIS1_EES5_:
.LFB17833:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_ZN2v88internal13MutableBigInt10BitwiseAndEPNS0_7IsolateENS0_6HandleINS0_6BigIntEEES6_
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L764
	movq	(%rax), %rdi
	call	_ZN2v88internal13MutableBigInt12CanonicalizeES1_
.L764:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17833:
	.size	_ZN2v88internal6BigInt10BitwiseAndEPNS0_7IsolateENS0_6HandleIS1_EES5_, .-_ZN2v88internal6BigInt10BitwiseAndEPNS0_7IsolateENS0_6HandleIS1_EES5_
	.section	.text._ZN2v88internal13MutableBigInt11AbsoluteAndEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEES6_S1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13MutableBigInt11AbsoluteAndEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEES6_S1_
	.type	_ZN2v88internal13MutableBigInt11AbsoluteAndEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEES6_S1_, @function
_ZN2v88internal13MutableBigInt11AbsoluteAndEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEES6_S1_:
.LFB17861:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal13MutableBigInt11AbsoluteAndEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation(%rip), %rbx
	movq	%rbx, %xmm0
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	_ZNSt17_Function_handlerIFmmmEZN2v88internal13MutableBigInt11AbsoluteAndEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E9_M_invokeERKSt9_Any_dataOmSE_(%rip), %rax
	movq	%rax, %xmm1
	movq	(%rsi), %rax
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	movl	7(%rax), %eax
	movq	(%rdx), %rdx
	movl	7(%rdx), %ebx
	shrl	%eax
	andl	$1073741823, %eax
	shrl	%ebx
	andl	$1073741823, %ebx
	cmpl	%ebx, %eax
	jge	.L770
	movq	%r15, %r8
	movl	%eax, %ebx
	movq	%rsi, %r15
.L770:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L771
	movq	%r14, %rsi
	movq	%r8, -120(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-120(%rbp), %r8
	movq	%rax, %r13
.L772:
	testq	%r14, %r14
	jne	.L774
	movl	%ebx, %esi
	movq	%r12, %rdi
	movq	%r8, -120(%rbp)
	call	_ZN2v88internal13MutableBigInt3NewEPNS0_7IsolateEiNS0_14AllocationTypeE.constprop.0
	movq	-120(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L795
	testl	%ebx, %ebx
	je	.L776
	movl	%ebx, %r14d
.L777:
	movl	$16, %ecx
	xorl	%r12d, %r12d
	leaq	-112(%rbp), %rdx
	.p2align 4,,10
	.p2align 3
.L781:
	movq	(%r15), %rax
	movq	(%r8), %rsi
	cmpq	$0, -80(%rbp)
	movq	0(%r13), %r9
	movq	-1(%rcx,%rax), %rax
	movq	-1(%rcx,%rsi), %rsi
	movq	%rax, -112(%rbp)
	movq	%rsi, -104(%rbp)
	je	.L796
	movq	%r9, -128(%rbp)
	leaq	-104(%rbp), %rsi
	leaq	-96(%rbp), %rdi
	addl	$1, %r12d
	movq	%rcx, -144(%rbp)
	movq	%r8, -136(%rbp)
	movq	%rdx, -120(%rbp)
	call	*-72(%rbp)
	movq	-144(%rbp), %rcx
	movq	-128(%rbp), %r9
	movq	-120(%rbp), %rdx
	movq	-136(%rbp), %r8
	movq	%rax, -1(%r9,%rcx)
	addq	$8, %rcx
	cmpl	%r12d, %ebx
	jg	.L781
.L780:
	cmpl	%r12d, %r14d
	jle	.L776
	subl	$1, %r14d
	leal	16(,%r12,8), %eax
	subl	%r12d, %r14d
	movslq	%r12d, %r12
	cltq
	leaq	3(%r14,%r12), %rdx
	salq	$3, %rdx
	.p2align 4,,10
	.p2align 3
.L782:
	movq	0(%r13), %rcx
	addq	$8, %rax
	movq	$0, -9(%rax,%rcx)
	cmpq	%rdx, %rax
	jne	.L782
.L776:
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L783
	leaq	-96(%rbp), %rdi
	movl	$3, %edx
	movq	%rdi, %rsi
	call	*%rax
.L783:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L797
	addq	$104, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L774:
	.cfi_restore_state
	movl	7(%r14), %r14d
	shrl	%r14d
	andl	$1073741823, %r14d
	testl	%ebx, %ebx
	jne	.L777
	xorl	%r12d, %r12d
	jmp	.L780
	.p2align 4,,10
	.p2align 3
.L771:
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L798
.L773:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, 0(%r13)
	jmp	.L772
	.p2align 4,,10
	.p2align 3
.L795:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L798:
	movq	%r12, %rdi
	movq	%r8, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %r8
	movq	%rax, %r13
	jmp	.L773
.L796:
	call	_ZSt25__throw_bad_function_callv@PLT
.L797:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17861:
	.size	_ZN2v88internal13MutableBigInt11AbsoluteAndEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEES6_S1_, .-_ZN2v88internal13MutableBigInt11AbsoluteAndEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEES6_S1_
	.section	.text._ZN2v88internal13MutableBigInt14AbsoluteAndNotEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEES6_S1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13MutableBigInt14AbsoluteAndNotEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEES6_S1_
	.type	_ZN2v88internal13MutableBigInt14AbsoluteAndNotEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEES6_S1_, @function
_ZN2v88internal13MutableBigInt14AbsoluteAndNotEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEES6_S1_:
.LFB17868:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal13MutableBigInt14AbsoluteAndNotEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation(%rip), %rdi
	pushq	%r14
	movq	%rdi, %xmm0
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	_ZNSt17_Function_handlerIFmmmEZN2v88internal13MutableBigInt14AbsoluteAndNotEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E9_M_invokeERKSt9_Any_dataOmSE_(%rip), %rax
	movq	%rax, %xmm1
	movq	(%rsi), %rax
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	movl	7(%rax), %edx
	movq	(%r8), %rax
	movl	7(%rax), %ebx
	shrl	%edx
	movq	41112(%r15), %rdi
	andl	$1073741823, %edx
	shrl	%ebx
	movl	%edx, %r12d
	andl	$1073741823, %ebx
	cmpl	%edx, %ebx
	cmovg	%edx, %ebx
	testq	%rdi, %rdi
	je	.L800
	movq	%rcx, %rsi
	movq	%r8, -128(%rbp)
	movq	%rcx, -120(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-120(%rbp), %rcx
	movq	-128(%rbp), %r8
	movq	%rax, %r14
.L801:
	testq	%rcx, %rcx
	jne	.L803
	movq	%r15, %rdi
	movl	%r12d, %esi
	movq	%r8, -120(%rbp)
	movl	%r12d, %r15d
	call	_ZN2v88internal13MutableBigInt3NewEPNS0_7IsolateEiNS0_14AllocationTypeE.constprop.0
	movq	-120(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L827
.L805:
	movl	$16, %ecx
	xorl	%r9d, %r9d
	leaq	-112(%rbp), %rdx
	testl	%ebx, %ebx
	je	.L810
	.p2align 4,,10
	.p2align 3
.L806:
	movq	(%r8), %rax
	movq	0(%r13), %rsi
	cmpq	$0, -80(%rbp)
	movq	(%r14), %r10
	movq	-1(%rcx,%rax), %rax
	movq	-1(%rcx,%rsi), %rsi
	movq	%rax, -112(%rbp)
	movq	%rsi, -104(%rbp)
	je	.L828
	movq	%r8, -152(%rbp)
	leaq	-104(%rbp), %rsi
	leaq	-96(%rbp), %rdi
	movq	%rcx, -144(%rbp)
	movl	%r9d, -132(%rbp)
	movq	%r10, -128(%rbp)
	movq	%rdx, -120(%rbp)
	call	*-72(%rbp)
	movl	-132(%rbp), %r9d
	movq	-144(%rbp), %rcx
	movq	-128(%rbp), %r10
	movq	-120(%rbp), %rdx
	addl	$1, %r9d
	movq	-152(%rbp), %r8
	movq	%rax, -1(%r10,%rcx)
	addq	$8, %rcx
	cmpl	%ebx, %r9d
	jne	.L806
.L810:
	cmpl	%ebx, %r12d
	jle	.L829
	leal	-1(%r12), %eax
	leal	16(,%rbx,8), %ecx
	subl	%ebx, %eax
	movslq	%ebx, %rbx
	movslq	%ecx, %rcx
	leaq	3(%rax,%rbx), %rax
	salq	$3, %rax
	.p2align 4,,10
	.p2align 3
.L813:
	movq	0(%r13), %rdx
	addq	$8, %rcx
	movq	-9(%rcx,%rdx), %rsi
	movq	(%r14), %rdx
	movq	%rsi, -9(%rcx,%rdx)
	cmpq	%rcx, %rax
	jne	.L813
.L807:
	cmpl	%r12d, %r15d
	jle	.L811
	subl	$1, %r15d
	movslq	%r12d, %rdx
	leal	16(,%r12,8), %eax
	subl	%r12d, %r15d
	cltq
	leaq	3(%r15,%rdx), %rdx
	salq	$3, %rdx
	.p2align 4,,10
	.p2align 3
.L814:
	movq	(%r14), %rcx
	addq	$8, %rax
	movq	$0, -9(%rax,%rcx)
	cmpq	%rdx, %rax
	jne	.L814
.L811:
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L815
	leaq	-96(%rbp), %rdi
	movl	$3, %edx
	movq	%rdi, %rsi
	call	*%rax
.L815:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L830
	addq	$120, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L803:
	.cfi_restore_state
	movl	7(%rcx), %r15d
	shrl	%r15d
	andl	$1073741823, %r15d
	jmp	.L805
	.p2align 4,,10
	.p2align 3
.L800:
	movq	41088(%r15), %r14
	cmpq	41096(%r15), %r14
	je	.L831
.L802:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r15)
	movq	%rcx, (%r14)
	jmp	.L801
	.p2align 4,,10
	.p2align 3
.L827:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L829:
	movl	%ebx, %r12d
	jmp	.L807
	.p2align 4,,10
	.p2align 3
.L831:
	movq	%r15, %rdi
	movq	%rcx, -128(%rbp)
	movq	%r8, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-128(%rbp), %rcx
	movq	-120(%rbp), %r8
	movq	%rax, %r14
	jmp	.L802
.L828:
	call	_ZSt25__throw_bad_function_callv@PLT
.L830:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17868:
	.size	_ZN2v88internal13MutableBigInt14AbsoluteAndNotEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEES6_S1_, .-_ZN2v88internal13MutableBigInt14AbsoluteAndNotEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEES6_S1_
	.section	.text._ZN2v88internal13MutableBigInt10AbsoluteOrEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEES6_S1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13MutableBigInt10AbsoluteOrEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEES6_S1_
	.type	_ZN2v88internal13MutableBigInt10AbsoluteOrEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEES6_S1_, @function
_ZN2v88internal13MutableBigInt10AbsoluteOrEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEES6_S1_:
.LFB17872:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal13MutableBigInt10AbsoluteOrEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation(%rip), %rdi
	pushq	%r13
	movq	%rdi, %xmm0
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	_ZNSt17_Function_handlerIFmmmEZN2v88internal13MutableBigInt10AbsoluteOrEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E9_M_invokeERKSt9_Any_dataOmSE_(%rip), %rax
	movq	%rax, %xmm1
	movq	(%rsi), %rax
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	movl	7(%rax), %ebx
	movq	(%rdx), %rax
	movl	7(%rax), %r12d
	shrl	%ebx
	andl	$1073741823, %ebx
	shrl	%r12d
	andl	$1073741823, %r12d
	cmpl	%r12d, %ebx
	jge	.L833
	movl	%r12d, %eax
	movq	%rdx, %r8
	movl	%ebx, %r12d
	movq	%rsi, %r15
	movl	%eax, %ebx
.L833:
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L834
	movq	%rcx, %rsi
	movq	%r8, -128(%rbp)
	movq	%rcx, -120(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-120(%rbp), %rcx
	movq	-128(%rbp), %r8
	movq	%rax, %r13
.L835:
	testq	%rcx, %rcx
	jne	.L837
	movq	%r14, %rdi
	movl	%ebx, %esi
	movq	%r8, -120(%rbp)
	movl	%ebx, %r14d
	call	_ZN2v88internal13MutableBigInt3NewEPNS0_7IsolateEiNS0_14AllocationTypeE.constprop.0
	movq	-120(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L862
.L839:
	movl	$16, %ecx
	xorl	%r9d, %r9d
	leaq	-112(%rbp), %rdx
	testl	%r12d, %r12d
	je	.L844
	.p2align 4,,10
	.p2align 3
.L840:
	movq	(%r15), %rax
	movq	(%r8), %rsi
	cmpq	$0, -80(%rbp)
	movq	0(%r13), %r10
	movq	-1(%rcx,%rax), %rax
	movq	-1(%rcx,%rsi), %rsi
	movq	%rax, -112(%rbp)
	movq	%rsi, -104(%rbp)
	je	.L863
	movq	%rcx, -152(%rbp)
	leaq	-104(%rbp), %rsi
	leaq	-96(%rbp), %rdi
	movl	%r9d, -140(%rbp)
	movq	%r10, -136(%rbp)
	movq	%r8, -128(%rbp)
	movq	%rdx, -120(%rbp)
	call	*-72(%rbp)
	movl	-140(%rbp), %r9d
	movq	-152(%rbp), %rcx
	movq	-136(%rbp), %r10
	movq	-120(%rbp), %rdx
	addl	$1, %r9d
	movq	-128(%rbp), %r8
	movq	%rax, -1(%r10,%rcx)
	addq	$8, %rcx
	cmpl	%r9d, %r12d
	jne	.L840
.L844:
	cmpl	%r12d, %ebx
	jle	.L864
	leal	-1(%rbx), %edx
	leal	16(,%r12,8), %eax
	subl	%r12d, %edx
	movslq	%r12d, %r12
	cltq
	leaq	3(%rdx,%r12), %rdx
	salq	$3, %rdx
	.p2align 4,,10
	.p2align 3
.L847:
	movq	(%r8), %rcx
	addq	$8, %rax
	movq	-9(%rax,%rcx), %rsi
	movq	0(%r13), %rcx
	movq	%rsi, -9(%rax,%rcx)
	cmpq	%rax, %rdx
	jne	.L847
.L841:
	cmpl	%ebx, %r14d
	jle	.L845
	subl	$1, %r14d
	leal	16(,%rbx,8), %eax
	subl	%ebx, %r14d
	movslq	%ebx, %rbx
	cltq
	leaq	3(%r14,%rbx), %rdx
	salq	$3, %rdx
	.p2align 4,,10
	.p2align 3
.L848:
	movq	0(%r13), %rcx
	addq	$8, %rax
	movq	$0, -9(%rax,%rcx)
	cmpq	%rdx, %rax
	jne	.L848
.L845:
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L849
	leaq	-96(%rbp), %rdi
	movl	$3, %edx
	movq	%rdi, %rsi
	call	*%rax
.L849:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L865
	addq	$120, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L837:
	.cfi_restore_state
	movl	7(%rcx), %r14d
	shrl	%r14d
	andl	$1073741823, %r14d
	jmp	.L839
	.p2align 4,,10
	.p2align 3
.L834:
	movq	41088(%r14), %r13
	cmpq	41096(%r14), %r13
	je	.L866
.L836:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r14)
	movq	%rcx, 0(%r13)
	jmp	.L835
	.p2align 4,,10
	.p2align 3
.L862:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L864:
	movl	%r12d, %ebx
	jmp	.L841
	.p2align 4,,10
	.p2align 3
.L866:
	movq	%r14, %rdi
	movq	%rcx, -128(%rbp)
	movq	%r8, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-128(%rbp), %rcx
	movq	-120(%rbp), %r8
	movq	%rax, %r13
	jmp	.L836
.L863:
	call	_ZSt25__throw_bad_function_callv@PLT
.L865:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17872:
	.size	_ZN2v88internal13MutableBigInt10AbsoluteOrEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEES6_S1_, .-_ZN2v88internal13MutableBigInt10AbsoluteOrEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEES6_S1_
	.section	.text._ZN2v88internal13MutableBigInt11AbsoluteXorEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEES6_S1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13MutableBigInt11AbsoluteXorEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEES6_S1_
	.type	_ZN2v88internal13MutableBigInt11AbsoluteXorEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEES6_S1_, @function
_ZN2v88internal13MutableBigInt11AbsoluteXorEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEES6_S1_:
.LFB17876:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal13MutableBigInt11AbsoluteXorEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation(%rip), %rdi
	pushq	%r13
	movq	%rdi, %xmm0
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	_ZNSt17_Function_handlerIFmmmEZN2v88internal13MutableBigInt11AbsoluteXorEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E9_M_invokeERKSt9_Any_dataOmSE_(%rip), %rax
	movq	%rax, %xmm1
	movq	(%rsi), %rax
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	movl	7(%rax), %ebx
	movq	(%rdx), %rax
	movl	7(%rax), %r12d
	shrl	%ebx
	andl	$1073741823, %ebx
	shrl	%r12d
	andl	$1073741823, %r12d
	cmpl	%r12d, %ebx
	jge	.L868
	movl	%r12d, %eax
	movq	%rdx, %r8
	movl	%ebx, %r12d
	movq	%rsi, %r15
	movl	%eax, %ebx
.L868:
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L869
	movq	%rcx, %rsi
	movq	%r8, -128(%rbp)
	movq	%rcx, -120(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-120(%rbp), %rcx
	movq	-128(%rbp), %r8
	movq	%rax, %r13
.L870:
	testq	%rcx, %rcx
	jne	.L872
	movq	%r14, %rdi
	movl	%ebx, %esi
	movq	%r8, -120(%rbp)
	movl	%ebx, %r14d
	call	_ZN2v88internal13MutableBigInt3NewEPNS0_7IsolateEiNS0_14AllocationTypeE.constprop.0
	movq	-120(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L897
.L874:
	movl	$16, %ecx
	xorl	%r9d, %r9d
	leaq	-112(%rbp), %rdx
	testl	%r12d, %r12d
	je	.L879
	.p2align 4,,10
	.p2align 3
.L875:
	movq	(%r15), %rax
	movq	(%r8), %rsi
	cmpq	$0, -80(%rbp)
	movq	0(%r13), %r10
	movq	-1(%rcx,%rax), %rax
	movq	-1(%rcx,%rsi), %rsi
	movq	%rax, -112(%rbp)
	movq	%rsi, -104(%rbp)
	je	.L898
	movq	%rcx, -152(%rbp)
	leaq	-104(%rbp), %rsi
	leaq	-96(%rbp), %rdi
	movl	%r9d, -140(%rbp)
	movq	%r10, -136(%rbp)
	movq	%r8, -128(%rbp)
	movq	%rdx, -120(%rbp)
	call	*-72(%rbp)
	movl	-140(%rbp), %r9d
	movq	-152(%rbp), %rcx
	movq	-136(%rbp), %r10
	movq	-120(%rbp), %rdx
	addl	$1, %r9d
	movq	-128(%rbp), %r8
	movq	%rax, -1(%r10,%rcx)
	addq	$8, %rcx
	cmpl	%r9d, %r12d
	jne	.L875
.L879:
	cmpl	%r12d, %ebx
	jle	.L899
	leal	-1(%rbx), %edx
	leal	16(,%r12,8), %eax
	subl	%r12d, %edx
	movslq	%r12d, %r12
	cltq
	leaq	3(%rdx,%r12), %rdx
	salq	$3, %rdx
	.p2align 4,,10
	.p2align 3
.L882:
	movq	(%r8), %rcx
	addq	$8, %rax
	movq	-9(%rax,%rcx), %rsi
	movq	0(%r13), %rcx
	movq	%rsi, -9(%rax,%rcx)
	cmpq	%rax, %rdx
	jne	.L882
.L876:
	cmpl	%ebx, %r14d
	jle	.L880
	subl	$1, %r14d
	leal	16(,%rbx,8), %eax
	subl	%ebx, %r14d
	movslq	%ebx, %rbx
	cltq
	leaq	3(%r14,%rbx), %rdx
	salq	$3, %rdx
	.p2align 4,,10
	.p2align 3
.L883:
	movq	0(%r13), %rcx
	addq	$8, %rax
	movq	$0, -9(%rax,%rcx)
	cmpq	%rdx, %rax
	jne	.L883
.L880:
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L884
	leaq	-96(%rbp), %rdi
	movl	$3, %edx
	movq	%rdi, %rsi
	call	*%rax
.L884:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L900
	addq	$120, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L872:
	.cfi_restore_state
	movl	7(%rcx), %r14d
	shrl	%r14d
	andl	$1073741823, %r14d
	jmp	.L874
	.p2align 4,,10
	.p2align 3
.L869:
	movq	41088(%r14), %r13
	cmpq	41096(%r14), %r13
	je	.L901
.L871:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r14)
	movq	%rcx, 0(%r13)
	jmp	.L870
	.p2align 4,,10
	.p2align 3
.L897:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L899:
	movl	%r12d, %ebx
	jmp	.L876
	.p2align 4,,10
	.p2align 3
.L901:
	movq	%r14, %rdi
	movq	%rcx, -128(%rbp)
	movq	%r8, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-128(%rbp), %rcx
	movq	-120(%rbp), %r8
	movq	%rax, %r13
	jmp	.L871
.L898:
	call	_ZSt25__throw_bad_function_callv@PLT
.L900:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17876:
	.size	_ZN2v88internal13MutableBigInt11AbsoluteXorEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEES6_S1_, .-_ZN2v88internal13MutableBigInt11AbsoluteXorEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEES6_S1_
	.section	.text._ZN2v88internal13MutableBigInt10BitwiseXorEPNS0_7IsolateENS0_6HandleINS0_6BigIntEEES6_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13MutableBigInt10BitwiseXorEPNS0_7IsolateENS0_6HandleINS0_6BigIntEEES6_
	.type	_ZN2v88internal13MutableBigInt10BitwiseXorEPNS0_7IsolateENS0_6HandleINS0_6BigIntEEES6_, @function
_ZN2v88internal13MutableBigInt10BitwiseXorEPNS0_7IsolateENS0_6HandleINS0_6BigIntEEES6_:
.LFB17836:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	7(%rdi), %eax
	testb	$1, %al
	je	.L966
	movq	(%rdx), %rax
.L903:
	movl	7(%rdi), %edx
	andl	$1, %edx
	jne	.L917
.L920:
	movl	7(%rax), %edx
	movl	7(%rdi), %eax
	movq	%r12, %rcx
	shrl	%edx
	shrl	%eax
	andl	$1073741823, %eax
	andl	$1073741823, %edx
	cmpl	%eax, %edx
	cmovl	%eax, %edx
	movl	7(%rdi), %eax
	movq	%r13, %rdi
	addl	$1, %edx
	andl	$1, %eax
	cmove	%rsi, %rcx
	cmove	%r12, %rsi
	movq	%rcx, -168(%rbp)
	call	_ZN2v88internal13MutableBigInt14AbsoluteSubOneEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEEi
	movq	-168(%rbp), %rcx
	testq	%rax, %rax
	movq	%rax, %r8
	movq	%rax, %rdx
	je	.L972
	movq	(%rax), %r14
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal13MutableBigInt11AbsoluteXorEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation(%rip), %rbx
	leaq	_ZNSt17_Function_handlerIFmmmEZN2v88internal13MutableBigInt11AbsoluteXorEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E9_M_invokeERKSt9_Any_dataOmSE_(%rip), %rax
	movq	%rbx, %xmm0
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	movq	(%r8), %rax
	movl	7(%rax), %ebx
	movq	(%rcx), %rax
	movl	7(%rax), %r12d
	shrl	%ebx
	andl	$1073741823, %ebx
	shrl	%r12d
	andl	$1073741823, %r12d
	cmpl	%r12d, %ebx
	jge	.L923
	movl	%r12d, %eax
	movq	%rcx, %r8
	movl	%ebx, %r12d
	movq	%rdx, %rcx
	movl	%eax, %ebx
.L923:
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L924
	movq	%r14, %rsi
	movq	%r8, -176(%rbp)
	movq	%rcx, -168(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-168(%rbp), %rcx
	movq	-176(%rbp), %r8
	movq	%rax, %r15
	testq	%r14, %r14
	jne	.L927
.L976:
	movl	%ebx, %esi
	movq	%r13, %rdi
	movq	%r8, -176(%rbp)
	movq	%rcx, -168(%rbp)
	call	_ZN2v88internal13MutableBigInt3NewEPNS0_7IsolateEiNS0_14AllocationTypeE.constprop.0
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L921
	movl	%ebx, -200(%rbp)
	movq	-168(%rbp), %rcx
	movq	-176(%rbp), %r8
.L928:
	movl	$16, %r9d
	xorl	%r10d, %r10d
	leaq	-144(%rbp), %r14
	testl	%r12d, %r12d
	je	.L933
	.p2align 4,,10
	.p2align 3
.L929:
	movq	(%rcx), %rax
	movq	(%r8), %rsi
	movl	%r10d, -196(%rbp)
	movq	(%r15), %r11
	cmpq	$0, -80(%rbp)
	movq	%rcx, -184(%rbp)
	movq	-1(%r9,%rax), %rax
	movq	-1(%r9,%rsi), %rsi
	movq	%r8, -176(%rbp)
	movq	%r11, -192(%rbp)
	movq	%r9, -168(%rbp)
	movq	%rsi, -136(%rbp)
	movq	%rax, -144(%rbp)
	je	.L932
	leaq	-136(%rbp), %rsi
	leaq	-96(%rbp), %rdi
	movq	%r14, %rdx
	call	*-72(%rbp)
	movl	-196(%rbp), %r10d
	movq	-168(%rbp), %r9
	movq	-192(%rbp), %r11
	movq	-176(%rbp), %r8
	addl	$1, %r10d
	movq	-184(%rbp), %rcx
	movq	%rax, -1(%r11,%r9)
	addq	$8, %r9
	cmpl	%r10d, %r12d
	jne	.L929
.L933:
	cmpl	%r12d, %ebx
	jle	.L973
	leal	-1(%rbx), %eax
	leal	16(,%r12,8), %edx
	subl	%r12d, %eax
	movslq	%r12d, %r12
	movslq	%edx, %rdx
	leaq	3(%rax,%r12), %rax
	salq	$3, %rax
	.p2align 4,,10
	.p2align 3
.L936:
	movq	(%r8), %rcx
	addq	$8, %rdx
	movq	-9(%rdx,%rcx), %rsi
	movq	(%r15), %rcx
	movq	%rsi, -9(%rdx,%rcx)
	cmpq	%rdx, %rax
	jne	.L936
.L930:
	cmpl	%ebx, -200(%rbp)
	jle	.L934
	movl	-200(%rbp), %r14d
	leal	16(,%rbx,8), %eax
	cltq
	subl	$1, %r14d
	subl	%ebx, %r14d
	movslq	%ebx, %rbx
	leaq	3(%r14,%rbx), %rdx
	salq	$3, %rdx
	.p2align 4,,10
	.p2align 3
.L937:
	movq	(%r15), %rcx
	addq	$8, %rax
	movq	$0, -9(%rax,%rcx)
	cmpq	%rax, %rdx
	jne	.L937
.L934:
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L938
	leaq	-96(%rbp), %rdi
	movl	$3, %edx
	movq	%rdi, %rsi
	call	*%rax
.L938:
	movq	(%r15), %rcx
	movl	$1, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal13MutableBigInt14AbsoluteAddOneEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEEbS1_
.L916:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L974
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L917:
	.cfi_restore_state
	movl	7(%rax), %edx
	andl	$1, %edx
	je	.L920
	movl	7(%rax), %edx
	movl	7(%rdi), %eax
	movq	%r13, %rdi
	shrl	%edx
	shrl	%eax
	andl	$1073741823, %eax
	andl	$1073741823, %edx
	cmpl	%eax, %edx
	cmovl	%eax, %edx
	call	_ZN2v88internal13MutableBigInt14AbsoluteSubOneEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEEi
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L921
	movq	(%r12), %rax
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	7(%rax), %edx
	shrl	%edx
	andl	$1073741823, %edx
	call	_ZN2v88internal13MutableBigInt14AbsoluteSubOneEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEEi
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L921
	movq	(%r14), %rcx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal13MutableBigInt11AbsoluteXorEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEES6_S1_
	jmp	.L916
	.p2align 4,,10
	.p2align 3
.L924:
	movq	41088(%r13), %r15
	cmpq	41096(%r13), %r15
	je	.L975
.L926:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r13)
	movq	%r14, (%r15)
	testq	%r14, %r14
	je	.L976
.L927:
	movl	7(%r14), %r14d
	shrl	%r14d
	andl	$1073741823, %r14d
	movl	%r14d, -200(%rbp)
	jmp	.L928
	.p2align 4,,10
	.p2align 3
.L966:
	movq	(%rdx), %rax
	movq	%rdx, %r15
	movl	7(%rax), %edx
	andl	$1, %edx
	jne	.L903
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal13MutableBigInt11AbsoluteXorEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation(%rip), %rbx
	leaq	_ZNSt17_Function_handlerIFmmmEZN2v88internal13MutableBigInt11AbsoluteXorEPNS2_7IsolateENS2_6HandleINS2_10BigIntBaseEEES8_S3_EUlmmE_E9_M_invokeERKSt9_Any_dataOmSE_(%rip), %rdx
	movq	%rsi, %rcx
	movq	%rbx, %xmm0
	movq	%rdx, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -112(%rbp)
	movl	7(%rdi), %r14d
	shrl	%r14d
	movl	%r14d, %ebx
	andl	$1073741823, %ebx
	movl	%ebx, -192(%rbp)
	movl	7(%rax), %r12d
	shrl	%r12d
	movl	%r12d, %eax
	andl	$1073741823, %eax
	movl	%eax, -176(%rbp)
	cmpl	%eax, %ebx
	jge	.L904
	movl	%ebx, -176(%rbp)
	movq	%r15, %rcx
	movq	%rsi, %r15
	movl	%eax, -192(%rbp)
.L904:
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L905
	xorl	%esi, %esi
	movq	%rcx, -168(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-168(%rbp), %rcx
.L906:
	movl	-192(%rbp), %esi
	movq	%r13, %rdi
	movq	%rcx, -168(%rbp)
	call	_ZN2v88internal13MutableBigInt3NewEPNS0_7IsolateEiNS0_14AllocationTypeE.constprop.0
	movq	-168(%rbp), %rcx
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L921
	leaq	-160(%rbp), %rax
	movl	$16, %r13d
	xorl	%r14d, %r14d
	movq	%rax, -184(%rbp)
	movl	-176(%rbp), %eax
	testl	%eax, %eax
	je	.L913
	.p2align 4,,10
	.p2align 3
.L909:
	movq	(%r15), %rax
	movq	(%rcx), %rdx
	cmpq	$0, -112(%rbp)
	movq	(%rbx), %r12
	movq	-1(%r13,%rax), %rax
	movq	-1(%r13,%rdx), %rdx
	movq	%rax, -160(%rbp)
	movq	%rdx, -152(%rbp)
	je	.L932
	movq	%rcx, -168(%rbp)
	movq	-184(%rbp), %rdx
	leaq	-128(%rbp), %rdi
	addl	$1, %r14d
	leaq	-152(%rbp), %rsi
	call	*-104(%rbp)
	movq	-168(%rbp), %rcx
	movq	%rax, -1(%r12,%r13)
	addq	$8, %r13
	cmpl	%r14d, -176(%rbp)
	jne	.L909
.L913:
	movl	-192(%rbp), %edi
	cmpl	%edi, -176(%rbp)
	jge	.L910
	movslq	-176(%rbp), %r12
	movl	%edi, %r14d
	subl	$1, %r14d
	subl	%r12d, %r14d
	leal	16(,%r12,8), %eax
	leaq	3(%r14,%r12), %rdi
	cltq
	salq	$3, %rdi
	.p2align 4,,10
	.p2align 3
.L914:
	movq	(%rcx), %rdx
	addq	$8, %rax
	movq	-9(%rax,%rdx), %rsi
	movq	(%rbx), %rdx
	movq	%rsi, -9(%rax,%rdx)
	cmpq	%rax, %rdi
	jne	.L914
.L910:
	movq	-112(%rbp), %rax
	testq	%rax, %rax
	je	.L915
	leaq	-128(%rbp), %rdi
	movl	$3, %edx
	movq	%rdi, %rsi
	call	*%rax
.L915:
	movq	%rbx, %rax
	jmp	.L916
	.p2align 4,,10
	.p2align 3
.L972:
	xorl	%eax, %eax
	jmp	.L916
	.p2align 4,,10
	.p2align 3
.L905:
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L977
.L907:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	$0, (%rax)
	jmp	.L906
	.p2align 4,,10
	.p2align 3
.L921:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L973:
	movl	%r12d, %ebx
	jmp	.L930
	.p2align 4,,10
	.p2align 3
.L975:
	movq	%r13, %rdi
	movq	%r8, -176(%rbp)
	movq	%rcx, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-176(%rbp), %r8
	movq	-168(%rbp), %rcx
	movq	%rax, %r15
	jmp	.L926
	.p2align 4,,10
	.p2align 3
.L977:
	movq	%r13, %rdi
	movq	%rcx, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %rcx
	jmp	.L907
.L932:
	call	_ZSt25__throw_bad_function_callv@PLT
.L974:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17836:
	.size	_ZN2v88internal13MutableBigInt10BitwiseXorEPNS0_7IsolateENS0_6HandleINS0_6BigIntEEES6_, .-_ZN2v88internal13MutableBigInt10BitwiseXorEPNS0_7IsolateENS0_6HandleINS0_6BigIntEEES6_
	.section	.text._ZN2v88internal6BigInt10BitwiseXorEPNS0_7IsolateENS0_6HandleIS1_EES5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6BigInt10BitwiseXorEPNS0_7IsolateENS0_6HandleIS1_EES5_
	.type	_ZN2v88internal6BigInt10BitwiseXorEPNS0_7IsolateENS0_6HandleIS1_EES5_, @function
_ZN2v88internal6BigInt10BitwiseXorEPNS0_7IsolateENS0_6HandleIS1_EES5_:
.LFB17835:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_ZN2v88internal13MutableBigInt10BitwiseXorEPNS0_7IsolateENS0_6HandleINS0_6BigIntEEES6_
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L979
	movq	(%rax), %rdi
	call	_ZN2v88internal13MutableBigInt12CanonicalizeES1_
.L979:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17835:
	.size	_ZN2v88internal6BigInt10BitwiseXorEPNS0_7IsolateENS0_6HandleIS1_EES5_, .-_ZN2v88internal6BigInt10BitwiseXorEPNS0_7IsolateENS0_6HandleIS1_EES5_
	.section	.text._ZN2v88internal13MutableBigInt15AbsoluteCompareENS0_10BigIntBaseES2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13MutableBigInt15AbsoluteCompareENS0_10BigIntBaseES2_
	.type	_ZN2v88internal13MutableBigInt15AbsoluteCompareENS0_10BigIntBaseES2_, @function
_ZN2v88internal13MutableBigInt15AbsoluteCompareENS0_10BigIntBaseES2_:
.LFB17881:
	.cfi_startproc
	endbr64
	movl	7(%rdi), %eax
	movl	7(%rsi), %edx
	shrl	%eax
	shrl	%edx
	andl	$1073741823, %eax
	andl	$1073741823, %edx
	subl	%edx, %eax
	je	.L993
.L984:
	ret
	.p2align 4,,10
	.p2align 3
.L993:
	movl	7(%rdi), %edx
	shrl	%edx
	andl	$1073741823, %edx
	movl	%edx, %r8d
	je	.L984
	leal	8(,%rdx,8), %ecx
	leaq	-1(%rdi), %r9
	subl	$1, %r8d
	subq	%rdi, %rsi
	movslq	%ecx, %rcx
	salq	$3, %r8
	leaq	(%rcx,%r9), %rdx
	leaq	-8(%rcx,%r9), %rcx
	subq	%r8, %rcx
	jmp	.L987
	.p2align 4,,10
	.p2align 3
.L995:
	subq	$8, %rdx
	cmpq	%rdx, %rcx
	je	.L994
.L987:
	movq	(%rdx), %rdi
	cmpq	%rdi, (%rsi,%rdx)
	je	.L995
	sbbl	%eax, %eax
	andl	$2, %eax
	subl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L994:
	ret
	.cfi_endproc
.LFE17881:
	.size	_ZN2v88internal13MutableBigInt15AbsoluteCompareENS0_10BigIntBaseES2_, .-_ZN2v88internal13MutableBigInt15AbsoluteCompareENS0_10BigIntBaseES2_
	.section	.text._ZN2v88internal13MutableBigInt15AbsoluteCompareENS0_6HandleINS0_10BigIntBaseEEES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13MutableBigInt15AbsoluteCompareENS0_6HandleINS0_10BigIntBaseEEES4_
	.type	_ZN2v88internal13MutableBigInt15AbsoluteCompareENS0_6HandleINS0_10BigIntBaseEEES4_, @function
_ZN2v88internal13MutableBigInt15AbsoluteCompareENS0_6HandleINS0_10BigIntBaseEEES4_:
.LFB17880:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rsi
	movq	(%rdi), %rdi
	jmp	_ZN2v88internal13MutableBigInt15AbsoluteCompareENS0_10BigIntBaseES2_
	.cfi_endproc
.LFE17880:
	.size	_ZN2v88internal13MutableBigInt15AbsoluteCompareENS0_6HandleINS0_10BigIntBaseEEES4_, .-_ZN2v88internal13MutableBigInt15AbsoluteCompareENS0_6HandleINS0_10BigIntBaseEEES4_
	.section	.text._ZN2v88internal6BigInt15CompareToBigIntENS0_6HandleIS1_EES3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6BigInt15CompareToBigIntENS0_6HandleIS1_EES3_
	.type	_ZN2v88internal6BigInt15CompareToBigIntENS0_6HandleIS1_EES3_, @function
_ZN2v88internal6BigInt15CompareToBigIntENS0_6HandleIS1_EES3_:
.LFB17831:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	movl	7(%rdi), %r10d
	movq	(%rsi), %rsi
	movl	7(%rsi), %eax
	andl	$1, %r10d
	andl	$1, %eax
	cmpb	%al, %r10b
	je	.L998
	testb	%r10b, %r10b
	je	.L1010
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1010:
	movl	$2, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L998:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal13MutableBigInt15AbsoluteCompareENS0_10BigIntBaseES2_
	testl	%eax, %eax
	jle	.L1001
	testb	%r10b, %r10b
	je	.L1002
.L999:
	xorl	%eax, %eax
.L997:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1001:
	.cfi_restore_state
	movl	$1, %eax
	je	.L997
	testb	%r10b, %r10b
	je	.L999
.L1002:
	movl	$2, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17831:
	.size	_ZN2v88internal6BigInt15CompareToBigIntENS0_6HandleIS1_EES3_, .-_ZN2v88internal6BigInt15CompareToBigIntENS0_6HandleIS1_EES3_
	.section	.text._ZN2v88internal6BigInt3AddEPNS0_7IsolateENS0_6HandleIS1_EES5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6BigInt3AddEPNS0_7IsolateENS0_6HandleIS1_EES5_
	.type	_ZN2v88internal6BigInt3AddEPNS0_7IsolateENS0_6HandleIS1_EES5_, @function
_ZN2v88internal6BigInt3AddEPNS0_7IsolateENS0_6HandleIS1_EES5_:
.LFB17823:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r11
	movq	%rdx, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	(%rsi), %rdi
	movl	7(%rdi), %ebx
	movq	(%rdx), %rsi
	movl	7(%rsi), %eax
	movl	%ebx, %r13d
	andl	$1, %r13d
	andl	$1, %eax
	cmpb	%al, %r13b
	je	.L1016
	call	_ZN2v88internal13MutableBigInt15AbsoluteCompareENS0_10BigIntBaseES2_
	testl	%eax, %eax
	js	.L1013
	andl	$1, %ebx
	movq	%r12, %rdi
	movq	%r10, %rdx
	movq	%r11, %rsi
	movl	%ebx, %ecx
	call	_ZN2v88internal13MutableBigInt11AbsoluteSubEPNS0_7IsolateENS0_6HandleINS0_6BigIntEEES6_b
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1013:
	.cfi_restore_state
	xorl	$1, %r13d
	movq	%r12, %rdi
	movq	%r11, %rdx
	movq	%r10, %rsi
	movzbl	%r13b, %ecx
	call	_ZN2v88internal13MutableBigInt11AbsoluteSubEPNS0_7IsolateENS0_6HandleINS0_6BigIntEEES6_b
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1016:
	.cfi_restore_state
	addq	$8, %rsp
	movl	%ebx, %ecx
	movq	%r12, %rdi
	movq	%r11, %rsi
	popq	%rbx
	andl	$1, %ecx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal13MutableBigInt11AbsoluteAddEPNS0_7IsolateENS0_6HandleINS0_6BigIntEEES6_b
	.cfi_endproc
.LFE17823:
	.size	_ZN2v88internal6BigInt3AddEPNS0_7IsolateENS0_6HandleIS1_EES5_, .-_ZN2v88internal6BigInt3AddEPNS0_7IsolateENS0_6HandleIS1_EES5_
	.section	.text._ZN2v88internal6BigInt8SubtractEPNS0_7IsolateENS0_6HandleIS1_EES5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6BigInt8SubtractEPNS0_7IsolateENS0_6HandleIS1_EES5_
	.type	_ZN2v88internal6BigInt8SubtractEPNS0_7IsolateENS0_6HandleIS1_EES5_, @function
_ZN2v88internal6BigInt8SubtractEPNS0_7IsolateENS0_6HandleIS1_EES5_:
.LFB17824:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r11
	movq	%rdx, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	(%rsi), %rdi
	movl	7(%rdi), %ebx
	movq	(%rdx), %rsi
	movl	7(%rsi), %eax
	movl	%ebx, %r13d
	andl	$1, %r13d
	andl	$1, %eax
	cmpb	%al, %r13b
	je	.L1018
	addq	$8, %rsp
	andl	$1, %ebx
	movq	%r12, %rdi
	movq	%r11, %rsi
	movl	%ebx, %ecx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal13MutableBigInt11AbsoluteAddEPNS0_7IsolateENS0_6HandleINS0_6BigIntEEES6_b
	.p2align 4,,10
	.p2align 3
.L1018:
	.cfi_restore_state
	call	_ZN2v88internal13MutableBigInt15AbsoluteCompareENS0_10BigIntBaseES2_
	testl	%eax, %eax
	js	.L1019
	movl	%ebx, %ecx
	movq	%r12, %rdi
	movq	%r10, %rdx
	movq	%r11, %rsi
	andl	$1, %ecx
	call	_ZN2v88internal13MutableBigInt11AbsoluteSubEPNS0_7IsolateENS0_6HandleINS0_6BigIntEEES6_b
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1019:
	.cfi_restore_state
	xorl	$1, %r13d
	movq	%r12, %rdi
	movq	%r11, %rdx
	movq	%r10, %rsi
	movzbl	%r13b, %ecx
	call	_ZN2v88internal13MutableBigInt11AbsoluteSubEPNS0_7IsolateENS0_6HandleINS0_6BigIntEEES6_b
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17824:
	.size	_ZN2v88internal6BigInt8SubtractEPNS0_7IsolateENS0_6HandleIS1_EES5_, .-_ZN2v88internal6BigInt8SubtractEPNS0_7IsolateENS0_6HandleIS1_EES5_
	.section	.text._ZN2v88internal6BigInt15CompareToStringEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6BigInt15CompareToStringEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEE
	.type	_ZN2v88internal6BigInt15CompareToStringEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEE, @function
_ZN2v88internal6BigInt15CompareToStringEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEE:
.LFB17841:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	movq	%rdx, %rsi
	subq	$8, %rsp
	call	_ZN2v88internal14StringToBigIntEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	testq	%rax, %rax
	je	.L1029
	movq	(%rbx), %rdi
	movl	7(%rdi), %r10d
	movq	(%rax), %rsi
	movl	7(%rsi), %eax
	andl	$1, %r10d
	andl	$1, %eax
	cmpb	%al, %r10b
	je	.L1024
.L1034:
	testb	%r10b, %r10b
	je	.L1028
.L1025:
	xorl	%eax, %eax
.L1022:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1024:
	.cfi_restore_state
	call	_ZN2v88internal13MutableBigInt15AbsoluteCompareENS0_10BigIntBaseES2_
	testl	%eax, %eax
	jg	.L1034
	movl	$1, %eax
	je	.L1022
	testb	%r10b, %r10b
	je	.L1025
.L1028:
	addq	$8, %rsp
	movl	$2, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1029:
	.cfi_restore_state
	addq	$8, %rsp
	movl	$3, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17841:
	.size	_ZN2v88internal6BigInt15CompareToStringEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEE, .-_ZN2v88internal6BigInt15CompareToStringEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEE
	.section	.text._ZN2v88internal13MutableBigInt18MultiplyAccumulateENS0_6HandleINS0_10BigIntBaseEEEmNS2_IS1_EEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13MutableBigInt18MultiplyAccumulateENS0_6HandleINS0_10BigIntBaseEEEmNS2_IS1_EEi
	.type	_ZN2v88internal13MutableBigInt18MultiplyAccumulateENS0_6HandleINS0_10BigIntBaseEEEmNS2_IS1_EEi, @function
_ZN2v88internal13MutableBigInt18MultiplyAccumulateENS0_6HandleINS0_10BigIntBaseEEEmNS2_IS1_EEi:
.LFB17882:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$80, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -152(%rbp)
	movq	%rsi, -160(%rbp)
	testq	%rsi, %rsi
	jne	.L1051
.L1035:
	addq	$80, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1051:
	.cfi_restore_state
	movq	%rdx, %r11
	leal	16(,%rcx,8), %r9d
	xorl	%edi, %edi
	xorl	%ebx, %ebx
	movl	%ecx, -164(%rbp)
	movslq	%r9d, %r9
	xorl	%edx, %edx
	movq	%r11, %r12
	jmp	.L1037
	.p2align 4,,10
	.p2align 3
.L1052:
	movq	(%r12), %rax
	movq	$0, -56(%rbp)
	xorl	%r15d, %r15d
	movq	-56(%rbp), %r11
	movq	%rbx, -96(%rbp)
	leaq	-1(%r9,%rax), %r8
	movq	$0, -72(%rbp)
	movq	(%r8), %rax
	movq	$0, -88(%rbp)
	movq	$0, -104(%rbp)
	leaq	(%rax,%rbx), %rcx
	movq	%rax, -80(%rbp)
	movq	-160(%rbp), %rax
	movq	%rcx, -64(%rbp)
	movq	-64(%rbp), %r10
	movq	$0, -120(%rbp)
	addq	%rdx, %r10
	movq	-80(%rbp), %r10
	adcq	%r15, %r11
	addq	-96(%rbp), %r10
	movq	%r11, %rsi
	movq	-72(%rbp), %r11
	adcq	-88(%rbp), %r11
	addq	%rdx, %rcx
	mulq	15(%r13,%rdi,8)
	addq	%r11, %rsi
	movq	-104(%rbp), %r11
	movq	%rcx, -128(%rbp)
	movq	%rax, -112(%rbp)
	movq	-112(%rbp), %r10
	movq	%rdx, %rbx
	movq	%rax, -144(%rbp)
	movq	-144(%rbp), %rax
	addq	-128(%rbp), %r10
	adcq	-120(%rbp), %r11
	addq	$1, %rdi
	addq	$8, %r9
	addq	%rcx, %rax
	movq	%rdx, -136(%rbp)
	movq	%r11, %rdx
	movq	%rax, (%r8)
	addq	%rsi, %rdx
.L1037:
	movl	-164(%rbp), %eax
	leal	(%rax,%rdi), %r8d
	movq	-152(%rbp), %rax
	movq	(%rax), %r13
	movl	7(%r13), %eax
	shrl	%eax
	andl	$1073741823, %eax
	cmpl	%edi, %eax
	jg	.L1052
	movq	%r12, %r11
	testq	%rdx, %rdx
	jne	.L1039
	testq	%rbx, %rbx
	je	.L1035
.L1039:
	leal	16(,%r8,8), %r8d
	movq	%r11, %r14
	movslq	%r8d, %r8
.L1040:
	movq	(%r14), %rax
	movq	%rdx, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	-192(%rbp), %r12
	leaq	-1(%r8,%rax), %r9
	movq	-184(%rbp), %r13
	movq	$0, -200(%rbp)
	movq	(%r9), %r15
	leaq	(%rbx,%r15), %rax
	movq	%rax, -208(%rbp)
	addq	-208(%rbp), %r12
	movq	%r15, %r12
	adcq	-200(%rbp), %r13
	xorl	%edi, %edi
	xorl	%r11d, %r11d
	addq	%rbx, %r12
	movq	%r13, %rcx
	movq	%rdi, %r13
	adcq	%r11, %r13
	addq	%rdx, %rax
	addq	$8, %r8
	xorl	%ebx, %ebx
	addq	%r13, %rcx
	movq	%rax, (%r9)
	movq	%rcx, %rdx
	jne	.L1040
	addq	$80, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17882:
	.size	_ZN2v88internal13MutableBigInt18MultiplyAccumulateENS0_6HandleINS0_10BigIntBaseEEEmNS2_IS1_EEi, .-_ZN2v88internal13MutableBigInt18MultiplyAccumulateENS0_6HandleINS0_10BigIntBaseEEEmNS2_IS1_EEi
	.section	.text._ZN2v88internal6BigInt8MultiplyEPNS0_7IsolateENS0_6HandleIS1_EES5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6BigInt8MultiplyEPNS0_7IsolateENS0_6HandleIS1_EES5_
	.type	_ZN2v88internal6BigInt8MultiplyEPNS0_7IsolateENS0_6HandleIS1_EES5_, @function
_ZN2v88internal6BigInt8MultiplyEPNS0_7IsolateENS0_6HandleIS1_EES5_:
.LFB17820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	(%rsi), %rax
	movq	%rdi, -56(%rbp)
	movl	7(%rax), %edx
	andl	$2147483646, %edx
	jne	.L1054
	movq	%rsi, %rax
.L1055:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1054:
	.cfi_restore_state
	movq	(%r14), %rdx
	movl	7(%rdx), %ecx
	andl	$2147483646, %ecx
	jne	.L1056
	addq	$24, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1056:
	.cfi_restore_state
	movl	7(%rax), %eax
	movl	7(%rdx), %edx
	shrl	%eax
	shrl	%edx
	andl	$1073741823, %eax
	andl	$1073741823, %edx
	leal	(%rax,%rdx), %r12d
	movl	%r12d, %esi
	call	_ZN2v88internal13MutableBigInt3NewEPNS0_7IsolateEiNS0_14AllocationTypeE.constprop.0
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1064
	movq	(%rax), %rax
	leal	0(,%r12,8), %edx
	xorl	%esi, %esi
	xorl	%r15d, %r15d
	movslq	%edx, %rdx
	xorl	%r12d, %r12d
	leaq	15(%rax), %rdi
	call	memset@PLT
	movq	-56(%rbp), %rax
	addq	$37512, %rax
	movq	%rax, -64(%rbp)
	jmp	.L1062
	.p2align 4,,10
	.p2align 3
.L1059:
	addq	$1, %r15
.L1062:
	movq	(%rbx), %rcx
	movl	7(%rcx), %edx
	shrl	%edx
	andl	$1073741823, %edx
	cmpl	%r15d, %edx
	jle	.L1058
	movq	15(%rcx,%r15,8), %rsi
	movq	%r13, %rdx
	movl	%r15d, %ecx
	movq	%r14, %rdi
	call	_ZN2v88internal13MutableBigInt18MultiplyAccumulateENS0_6HandleINS0_10BigIntBaseEEEmNS2_IS1_EEi
	movq	(%r14), %rdx
	movl	7(%rdx), %edx
	shrl	%edx
	andl	$1073741823, %edx
	addq	%rdx, %r12
	cmpq	$5000000, %r12
	jbe	.L1059
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	movq	%rax, %r9
	movq	-56(%rbp), %rax
	movq	37544(%rax), %rax
	cmpq	%rax, %r9
	jb	.L1060
.L1061:
	xorl	%r12d, %r12d
	jmp	.L1059
	.p2align 4,,10
	.p2align 3
.L1060:
	movq	-64(%rbp), %rdi
	call	_ZN2v88internal10StackGuard16HandleInterruptsEv@PLT
	movq	-56(%rbp), %rdi
	cmpq	%rax, 312(%rdi)
	jne	.L1061
.L1064:
	xorl	%eax, %eax
	jmp	.L1055
	.p2align 4,,10
	.p2align 3
.L1058:
	movq	0(%r13), %rsi
	movl	7(%rcx), %eax
	movq	(%r14), %rdx
	movl	7(%rdx), %ecx
	movl	7(%rsi), %edx
	xorl	%ecx, %eax
	andl	$-2, %edx
	andl	$1, %eax
	orl	%edx, %eax
	movl	%eax, 7(%rsi)
	movq	0(%r13), %rdi
	call	_ZN2v88internal13MutableBigInt12CanonicalizeES1_
	movq	%r13, %rax
	jmp	.L1055
	.cfi_endproc
.LFE17820:
	.size	_ZN2v88internal6BigInt8MultiplyEPNS0_7IsolateENS0_6HandleIS1_EES5_, .-_ZN2v88internal6BigInt8MultiplyEPNS0_7IsolateENS0_6HandleIS1_EES5_
	.section	.text._ZN2v88internal6BigInt12ExponentiateEPNS0_7IsolateENS0_6HandleIS1_EES5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6BigInt12ExponentiateEPNS0_7IsolateENS0_6HandleIS1_EES5_
	.type	_ZN2v88internal6BigInt12ExponentiateEPNS0_7IsolateENS0_6HandleIS1_EES5_, @function
_ZN2v88internal6BigInt12ExponentiateEPNS0_7IsolateENS0_6HandleIS1_EES5_:
.LFB17819:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdx), %rax
	movl	7(%rax), %edx
	andl	$1, %edx
	je	.L1066
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$182, %esi
.L1107:
	movq	%r13, %rdi
	call	_ZN2v88internal7Factory13NewRangeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
.L1108:
	xorl	%eax, %eax
.L1067:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1066:
	.cfi_restore_state
	movl	7(%rax), %edx
	andl	$2147483646, %edx
	jne	.L1068
	xorl	%edx, %edx
	movl	$1, %esi
	call	_ZN2v88internal7Factory9NewBigIntEiNS0_14AllocationTypeE@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	movl	$2, 7(%rax)
	movq	(%r12), %rax
	movq	$1, 15(%rax)
	movq	(%r12), %rdi
	call	_ZN2v88internal13MutableBigInt12CanonicalizeES1_
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1068:
	.cfi_restore_state
	movq	(%rsi), %rdx
	movq	%rsi, %r12
	movl	7(%rdx), %ecx
	andl	$2147483646, %ecx
	jne	.L1069
.L1086:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1069:
	.cfi_restore_state
	movl	7(%rdx), %ecx
	movq	%rsi, %r14
	shrl	%ecx
	andl	$1073741823, %ecx
	cmpl	$1, %ecx
	je	.L1109
.L1070:
	movl	7(%rax), %ecx
	shrl	%ecx
	andl	$1073741822, %ecx
	jne	.L1073
	movq	15(%rax), %rbx
	cmpq	$1, %rbx
	je	.L1086
	cmpq	$1073741823, %rbx
	ja	.L1073
	movl	7(%rdx), %eax
	shrl	%eax
	andl	$1073741823, %eax
	cmpl	$1, %eax
	je	.L1110
.L1080:
	testb	$1, %bl
	movl	$0, %eax
	cmove	%rax, %r12
	sarl	%ebx
	je	.L1086
.L1090:
	movq	%r14, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal6BigInt8MultiplyEPNS0_7IsolateENS0_6HandleIS1_EES5_
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1108
	testb	$1, %bl
	je	.L1088
	testq	%r12, %r12
	je	.L1091
	movq	%r12, %rsi
	movq	%rax, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal6BigInt8MultiplyEPNS0_7IsolateENS0_6HandleIS1_EES5_
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1108
.L1088:
	sarl	%ebx
	jne	.L1090
	jmp	.L1086
	.p2align 4,,10
	.p2align 3
.L1073:
	cmpb	$0, _ZN2v88internal36FLAG_correctness_fuzzer_suppressionsE(%rip)
	jne	.L1111
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$183, %esi
	jmp	.L1107
	.p2align 4,,10
	.p2align 3
.L1109:
	cmpq	$1, 15(%rdx)
	jne	.L1070
	movl	7(%rdx), %edx
	andl	$1, %edx
	je	.L1086
	testb	$1, 15(%rax)
	jne	.L1086
	call	_ZN2v88internal6BigInt10UnaryMinusEPNS0_7IsolateENS0_6HandleIS1_EE
	jmp	.L1067
	.p2align 4,,10
	.p2align 3
.L1091:
	movq	%rax, %r12
	jmp	.L1088
.L1110:
	cmpq	$2, 15(%rdx)
	jne	.L1080
	movl	%ebx, %r14d
	movq	%r13, %rdi
	sarl	$6, %r14d
	addl	$1, %r14d
	movl	%r14d, %esi
	call	_ZN2v88internal13MutableBigInt3NewEPNS0_7IsolateEiNS0_14AllocationTypeE.constprop.0
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1108
	movq	(%rax), %rax
	sall	$3, %r14d
	xorl	%esi, %esi
	movslq	%r14d, %rdx
	leaq	15(%rax), %rdi
	call	memset@PLT
	movq	(%r15), %rsi
	leal	8(%r14), %eax
	movl	%ebx, %ecx
	movl	$1, %edx
	cltq
	salq	%cl, %rdx
	movq	%rdx, -1(%rax,%rsi)
	movq	(%r12), %rax
	movl	7(%rax), %eax
	testb	$1, %al
	jne	.L1112
.L1085:
	movq	(%r15), %rdi
	call	_ZN2v88internal13MutableBigInt12CanonicalizeES1_
	movq	%r15, %rax
	jmp	.L1067
.L1112:
	movq	(%r15), %rdx
	andl	$1, %ebx
	movl	7(%rdx), %eax
	andl	$-2, %eax
	orl	%eax, %ebx
	movl	%ebx, 7(%rdx)
	jmp	.L1085
.L1111:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE17819:
	.size	_ZN2v88internal6BigInt12ExponentiateEPNS0_7IsolateENS0_6HandleIS1_EES5_, .-_ZN2v88internal6BigInt12ExponentiateEPNS0_7IsolateENS0_6HandleIS1_EES5_
	.section	.rodata._ZN2v88internal13MutableBigInt19InternalMultiplyAddENS0_10BigIntBaseEmmiS1_.str1.1,"aMS",@progbits,1
.LC13:
	.string	"carry + high == 0"
	.section	.text._ZN2v88internal13MutableBigInt19InternalMultiplyAddENS0_10BigIntBaseEmmiS1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13MutableBigInt19InternalMultiplyAddENS0_10BigIntBaseEmmiS1_
	.type	_ZN2v88internal13MutableBigInt19InternalMultiplyAddENS0_10BigIntBaseEmmiS1_, @function
_ZN2v88internal13MutableBigInt19InternalMultiplyAddENS0_10BigIntBaseEmmiS1_:
.LFB17883:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -72(%rbp)
	movl	%ecx, -100(%rbp)
	movq	%r8, -96(%rbp)
	testl	%ecx, %ecx
	jle	.L1114
	movl	%ecx, %eax
	movq	%r8, %rsi
	xorl	%r8d, %r8d
	movq	%rdx, -64(%rbp)
	subl	$1, %eax
	subq	%rdi, %rsi
	movq	%r8, -56(%rbp)
	leaq	15(%rdi), %rcx
	leaq	23(%rdi,%rax,8), %rax
	movq	%rsi, -88(%rbp)
	movq	%rax, -80(%rbp)
	.p2align 4,,10
	.p2align 3
.L1115:
	movq	-72(%rbp), %rax
	mulq	(%rcx)
	xorl	%r11d, %r11d
	xorl	%r15d, %r15d
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %r9
	leaq	(%r8,%rax), %rbx
	movq	%rdx, -56(%rbp)
	addq	%r8, %rax
	movq	%r11, %rdx
	adcq	%r15, %rdx
	movq	%rbx, %rax
	xorl	%edi, %edi
	xorl	%r13d, %r13d
	movq	%rdx, %r8
	addq	%r9, %rax
	movq	%rdi, %rdx
	adcq	%r13, %rdx
	addq	%r9, %rbx
	movq	%rdx, %rax
	addq	%r8, %rax
	movq	%rax, -64(%rbp)
	movq	-88(%rbp), %rax
	movq	%rbx, (%rax,%rcx)
	addq	$8, %rcx
	cmpq	%rcx, -80(%rbp)
	jne	.L1115
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %r9
	addq	%r8, %r9
.L1114:
	movq	-96(%rbp), %rdi
	movl	7(%rdi), %eax
	movl	-100(%rbp), %ebx
	leaq	7(%rdi), %rsi
	shrl	%eax
	andl	$1073741823, %eax
	cmpl	%eax, %ebx
	jge	.L1116
	leal	16(,%rbx,8), %eax
	leal	1(%rbx), %edx
	movslq	%eax, %rcx
	addl	$8, %eax
	cltq
	movq	%r9, -1(%rdi,%rcx)
	leaq	-1(%rdi,%rax), %rcx
	jmp	.L1118
	.p2align 4,,10
	.p2align 3
.L1124:
	movq	$0, (%rcx)
	addl	$1, %edx
	addq	$8, %rcx
.L1118:
	movl	(%rsi), %eax
	shrl	%eax
	andl	$1073741823, %eax
	cmpl	%eax, %edx
	jl	.L1124
.L1113:
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1116:
	.cfi_restore_state
	testq	%r9, %r9
	je	.L1113
	leaq	.LC13(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE17883:
	.size	_ZN2v88internal13MutableBigInt19InternalMultiplyAddENS0_10BigIntBaseEmmiS1_, .-_ZN2v88internal13MutableBigInt19InternalMultiplyAddENS0_10BigIntBaseEmmiS1_
	.section	.text._ZN2v88internal6BigInt18InplaceMultiplyAddENS0_6HandleINS0_22FreshlyAllocatedBigIntEEEmm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6BigInt18InplaceMultiplyAddENS0_6HandleINS0_22FreshlyAllocatedBigIntEEEmm
	.type	_ZN2v88internal6BigInt18InplaceMultiplyAddENS0_6HandleINS0_22FreshlyAllocatedBigIntEEEmm, @function
_ZN2v88internal6BigInt18InplaceMultiplyAddENS0_6HandleINS0_22FreshlyAllocatedBigIntEEEmm:
.LFB17884:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	movl	7(%rdi), %ecx
	movq	%rdi, %r8
	shrl	%ecx
	andl	$1073741823, %ecx
	jmp	_ZN2v88internal13MutableBigInt19InternalMultiplyAddENS0_10BigIntBaseEmmiS1_
	.cfi_endproc
.LFE17884:
	.size	_ZN2v88internal6BigInt18InplaceMultiplyAddENS0_6HandleINS0_22FreshlyAllocatedBigIntEEEmm, .-_ZN2v88internal6BigInt18InplaceMultiplyAddENS0_6HandleINS0_22FreshlyAllocatedBigIntEEEmm
	.section	.text._ZN2v88internal13MutableBigInt16AbsoluteDivSmallEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEEmPNS4_IS1_EEPm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13MutableBigInt16AbsoluteDivSmallEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEEmPNS4_IS1_EEPm
	.type	_ZN2v88internal13MutableBigInt16AbsoluteDivSmallEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEEmPNS4_IS1_EEPm, @function
_ZN2v88internal13MutableBigInt16AbsoluteDivSmallEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEEmPNS4_IS1_EEPm:
.LFB17885:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	subq	$16, %rsp
	movq	$0, (%r8)
	movq	(%rsi), %rax
	movl	7(%rax), %r15d
	shrl	%r15d
	andl	$1073741823, %r15d
	testq	%rcx, %rcx
	je	.L1127
	cmpq	$0, (%rcx)
	movq	%rcx, %r14
	je	.L1139
.L1128:
	testl	%r15d, %r15d
	je	.L1126
	movslq	%r15d, %rcx
	leal	-1(%r15), %eax
	leal	8(,%r15,8), %esi
	subq	%rax, %rcx
	movslq	%esi, %rsi
	salq	$3, %rcx
	.p2align 4,,10
	.p2align 3
.L1132:
	movq	0(%r13), %rax
	movq	(%r8), %rdx
	subq	$8, %rsi
	movq	7(%rsi,%rax), %rax
#APP
# 2614 "../deps/v8/src/objects/bigint.cc" 1
	divq  %rbx
# 0 "" 2
#NO_APP
	movq	%rdx, (%r8)
	movq	(%r14), %rdx
	movq	(%rdx), %rdx
	movq	%rax, 7(%rsi,%rdx)
	cmpq	%rsi, %rcx
	jne	.L1132
.L1126:
	addq	$16, %rsp
	popq	%rbx
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1127:
	.cfi_restore_state
	testl	%r15d, %r15d
	je	.L1126
	movslq	%r15d, %rsi
	leal	-1(%r15), %edi
	leal	8(,%r15,8), %ecx
	xorl	%edx, %edx
	subq	%rdi, %rsi
	movslq	%ecx, %rcx
	salq	$3, %rsi
	jmp	.L1133
	.p2align 4,,10
	.p2align 3
.L1140:
	movq	0(%r13), %rax
.L1133:
	movq	-1(%rcx,%rax), %rax
	subq	$8, %rcx
#APP
# 2614 "../deps/v8/src/objects/bigint.cc" 1
	divq  %rbx
# 0 "" 2
#NO_APP
	movq	%rdx, (%r8)
	cmpq	%rcx, %rsi
	jne	.L1140
	addq	$16, %rsp
	popq	%rbx
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1139:
	.cfi_restore_state
	movl	%r15d, %esi
	movq	%r8, -40(%rbp)
	call	_ZN2v88internal13MutableBigInt3NewEPNS0_7IsolateEiNS0_14AllocationTypeE.constprop.0
	movq	-40(%rbp), %r8
	testq	%rax, %rax
	je	.L1141
	movq	%rax, (%r14)
	jmp	.L1128
.L1141:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE17885:
	.size	_ZN2v88internal13MutableBigInt16AbsoluteDivSmallEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEEmPNS4_IS1_EEPm, .-_ZN2v88internal13MutableBigInt16AbsoluteDivSmallEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEEmPNS4_IS1_EEPm
	.section	.text._ZN2v88internal13MutableBigInt18ProductGreaterThanEmmmm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13MutableBigInt18ProductGreaterThanEmmmm
	.type	_ZN2v88internal13MutableBigInt18ProductGreaterThanEmmmm, @function
_ZN2v88internal13MutableBigInt18ProductGreaterThanEmmmm:
.LFB17887:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	movq	%rdx, %r9
	movl	$1, %r8d
	mulq	%rsi
	cmpq	%rdx, %r9
	jb	.L1142
	sete	%r8b
	cmpq	%rax, %rcx
	setb	%al
	andl	%eax, %r8d
.L1142:
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE17887:
	.size	_ZN2v88internal13MutableBigInt18ProductGreaterThanEmmmm, .-_ZN2v88internal13MutableBigInt18ProductGreaterThanEmmmm
	.section	.text._ZN2v88internal13MutableBigInt10InplaceAddENS0_6HandleINS0_10BigIntBaseEEEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13MutableBigInt10InplaceAddENS0_6HandleINS0_10BigIntBaseEEEi
	.type	_ZN2v88internal13MutableBigInt10InplaceAddENS0_6HandleINS0_10BigIntBaseEEEi, @function
_ZN2v88internal13MutableBigInt10InplaceAddENS0_6HandleINS0_10BigIntBaseEEEi:
.LFB17888:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -96(%rbp)
	movq	(%rsi), %rcx
	movl	7(%rcx), %eax
	shrl	%eax
	andl	$1073741823, %eax
	je	.L1148
	subl	$1, %eax
	movslq	%edx, %rdx
	leaq	24(,%rax,8), %rax
	leaq	0(,%rdx,8), %rbx
	movl	$16, %edx
	movq	%rax, -88(%rbp)
	xorl	%eax, %eax
	movq	%rax, %r8
	movq	%rdi, %rax
	jmp	.L1147
	.p2align 4,,10
	.p2align 3
.L1152:
	movq	-96(%rbp), %rdi
	movq	(%rdi), %rcx
.L1147:
	movq	(%rax), %rsi
	movq	-1(%rdx,%rcx), %r10
	leaq	(%rbx,%rdx), %rcx
	xorl	%r15d, %r15d
	movq	%r8, %rdi
	xorl	%r13d, %r13d
	movq	%r15, %r9
	movq	$0, -56(%rbp)
	leaq	-1(%rsi,%rcx), %rsi
	movq	%r10, -80(%rbp)
	movq	(%rsi), %rcx
	movq	$0, -72(%rbp)
	addq	%r10, %rcx
	addq	%rcx, %r8
	adcq	%r13, %r9
	movq	%r9, %r11
	movq	(%rsi), %r9
	movq	%r9, -64(%rbp)
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %r8
	addq	-80(%rbp), %r8
	adcq	-72(%rbp), %r9
	addq	%rdi, %rcx
	addq	$8, %rdx
	movq	%r9, %r8
	movq	%rcx, (%rsi)
	addq	%r11, %r8
	cmpq	%rdx, -88(%rbp)
	jne	.L1152
	movq	%r8, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1148:
	.cfi_restore_state
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17888:
	.size	_ZN2v88internal13MutableBigInt10InplaceAddENS0_6HandleINS0_10BigIntBaseEEEi, .-_ZN2v88internal13MutableBigInt10InplaceAddENS0_6HandleINS0_10BigIntBaseEEEi
	.section	.text._ZN2v88internal13MutableBigInt10InplaceSubENS0_6HandleINS0_10BigIntBaseEEEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13MutableBigInt10InplaceSubENS0_6HandleINS0_10BigIntBaseEEEi
	.type	_ZN2v88internal13MutableBigInt10InplaceSubENS0_6HandleINS0_10BigIntBaseEEEi, @function
_ZN2v88internal13MutableBigInt10InplaceSubENS0_6HandleINS0_10BigIntBaseEEEi:
.LFB17889:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -96(%rbp)
	movq	(%rsi), %rcx
	movl	7(%rcx), %eax
	shrl	%eax
	andl	$1073741823, %eax
	je	.L1156
	subl	$1, %eax
	movslq	%edx, %rdx
	movq	%rdi, %r11
	leaq	24(,%rax,8), %rax
	leaq	0(,%rdx,8), %rbx
	movl	$16, %edx
	movq	%rax, -88(%rbp)
	xorl	%eax, %eax
	jmp	.L1155
	.p2align 4,,10
	.p2align 3
.L1160:
	movq	-96(%rbp), %rdi
	movq	(%rdi), %rcx
.L1155:
	movq	(%r11), %rsi
	movq	-1(%rdx,%rcx), %r10
	leaq	(%rbx,%rdx), %rcx
	xorl	%r15d, %r15d
	movq	%rax, -64(%rbp)
	movq	%r15, %r9
	movq	%rax, %rdi
	leaq	-1(%rsi,%rcx), %rsi
	movq	$0, -56(%rbp)
	movq	(%rsi), %rcx
	movq	(%rsi), %r12
	movq	%r10, -80(%rbp)
	movq	$0, -72(%rbp)
	subq	%r10, %rcx
	cmpq	-64(%rbp), %rcx
	sbbq	-56(%rbp), %r9
	xorl	%r13d, %r13d
	movq	%r9, %rax
	movq	%r13, %r9
	andl	$1, %eax
	cmpq	-80(%rbp), %r12
	sbbq	-72(%rbp), %r9
	subq	%rdi, %rcx
	movq	%r9, %r8
	movq	%rcx, (%rsi)
	addq	$8, %rdx
	andl	$1, %r8d
	addq	%r8, %rax
	cmpq	%rdx, -88(%rbp)
	jne	.L1160
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1156:
	.cfi_restore_state
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17889:
	.size	_ZN2v88internal13MutableBigInt10InplaceSubENS0_6HandleINS0_10BigIntBaseEEEi, .-_ZN2v88internal13MutableBigInt10InplaceSubENS0_6HandleINS0_10BigIntBaseEEEi
	.section	.text._ZN2v88internal13MutableBigInt17InplaceRightShiftEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13MutableBigInt17InplaceRightShiftEi
	.type	_ZN2v88internal13MutableBigInt17InplaceRightShiftEi, @function
_ZN2v88internal13MutableBigInt17InplaceRightShiftEi:
.LFB17890:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	jne	.L1163
	ret
	.p2align 4,,10
	.p2align 3
.L1163:
	jmp	_ZN2v88internal13MutableBigInt17InplaceRightShiftEi.part.0
	.cfi_endproc
.LFE17890:
	.size	_ZN2v88internal13MutableBigInt17InplaceRightShiftEi, .-_ZN2v88internal13MutableBigInt17InplaceRightShiftEi
	.section	.text._ZN2v88internal13MutableBigInt16SpecialLeftShiftEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEEiNS1_20SpecialLeftShiftModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13MutableBigInt16SpecialLeftShiftEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEEiNS1_20SpecialLeftShiftModeE
	.type	_ZN2v88internal13MutableBigInt16SpecialLeftShiftEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEEiNS1_20SpecialLeftShiftModeE, @function
_ZN2v88internal13MutableBigInt16SpecialLeftShiftEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEEiNS1_20SpecialLeftShiftModeE:
.LFB17891:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rsi), %rax
	movq	%rsi, %rbx
	xorl	%esi, %esi
	movl	7(%rax), %r12d
	shrl	%r12d
	andl	$1073741823, %r12d
	cmpl	$1, %ecx
	sete	%sil
	addl	%r12d, %esi
	call	_ZN2v88internal13MutableBigInt3NewEPNS0_7IsolateEiNS0_14AllocationTypeE.constprop.0
	testq	%rax, %rax
	je	.L1167
	testl	%r13d, %r13d
	jne	.L1168
	testl	%r12d, %r12d
	je	.L1172
	leal	-1(%r12), %edx
	leaq	24(,%rdx,8), %rdi
	movl	$16, %edx
	.p2align 4,,10
	.p2align 3
.L1173:
	movq	(%rbx), %rcx
	addq	$8, %rdx
	movq	-9(%rdx,%rcx), %rsi
	movq	(%rax), %rcx
	movq	%rsi, -9(%rdx,%rcx)
	cmpq	%rdx, %rdi
	jne	.L1173
.L1172:
	cmpl	$1, %r14d
	je	.L1181
.L1167:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1168:
	.cfi_restore_state
	testl	%r12d, %r12d
	je	.L1178
	leal	-1(%r12), %edx
	movl	$64, %r9d
	movl	$16, %esi
	leaq	24(,%rdx,8), %r10
	subl	%r13d, %r9d
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L1175:
	movq	(%rbx), %rcx
	movq	(%rax), %r8
	addq	$8, %rsi
	movq	-9(%rsi,%rcx), %rdi
	movl	%r13d, %ecx
	movq	%rdi, %r11
	salq	%cl, %r11
	movq	%r11, %rcx
	orq	%rdx, %rcx
	movq	%rcx, -9(%rsi,%r8)
	movl	%r9d, %ecx
	shrq	%cl, %rdi
	movq	%rdi, %rdx
	cmpq	%rsi, %r10
	jne	.L1175
.L1174:
	cmpl	$1, %r14d
	jne	.L1167
	movq	(%rax), %rsi
	leal	16(,%r12,8), %ecx
	movslq	%ecx, %rcx
	movq	%rdx, -1(%rcx,%rsi)
	jmp	.L1167
	.p2align 4,,10
	.p2align 3
.L1181:
	movq	(%rax), %rcx
	leal	16(,%r12,8), %edx
	movslq	%edx, %rdx
	movq	$0, -1(%rdx,%rcx)
	jmp	.L1167
	.p2align 4,,10
	.p2align 3
.L1178:
	xorl	%edx, %edx
	jmp	.L1174
	.cfi_endproc
.LFE17891:
	.size	_ZN2v88internal13MutableBigInt16SpecialLeftShiftEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEEiNS1_20SpecialLeftShiftModeE, .-_ZN2v88internal13MutableBigInt16SpecialLeftShiftEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEEiNS1_20SpecialLeftShiftModeE
	.section	.text._ZN2v88internal13MutableBigInt16AbsoluteDivLargeEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEES6_PNS4_IS1_EES8_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13MutableBigInt16AbsoluteDivLargeEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEES6_PNS4_IS1_EES8_
	.type	_ZN2v88internal13MutableBigInt16AbsoluteDivLargeEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEES6_PNS4_IS1_EES8_, @function
_ZN2v88internal13MutableBigInt16AbsoluteDivLargeEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEES6_PNS4_IS1_EES8_:
.LFB17886:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -136(%rbp)
	movq	%rcx, -112(%rbp)
	movq	%r8, -160(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	movq	%rdx, -88(%rbp)
	movl	7(%rax), %eax
	shrl	%eax
	andl	$1073741823, %eax
	movl	%eax, -96(%rbp)
	movl	%eax, %esi
	movq	(%r9), %rax
	movl	7(%rax), %r13d
	shrl	%r13d
	andl	$1073741823, %r13d
	movl	%r13d, %ebx
	subl	%esi, %ebx
	testq	%rcx, %rcx
	je	.L1206
	leal	1(%rbx), %esi
	movq	%r9, -72(%rbp)
	call	_ZN2v88internal13MutableBigInt3NewEPNS0_7IsolateEiNS0_14AllocationTypeE.constprop.0
	movq	-72(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, -128(%rbp)
	je	.L1189
.L1183:
	movl	-96(%rbp), %eax
	movq	-136(%rbp), %rdi
	movq	%r9, -72(%rbp)
	leal	1(%rax), %r12d
	movl	%r12d, %esi
	call	_ZN2v88internal13MutableBigInt3NewEPNS0_7IsolateEiNS0_14AllocationTypeE.constprop.0
	movq	-72(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L1243
	movq	(%r14), %rax
	sall	$3, %r12d
	movslq	%r12d, %r12
	movq	-1(%r12,%rax), %rax
	testq	%rax, %rax
	je	.L1207
	bsrq	%rax, %rax
	xorq	$63, %rax
	movl	%eax, -148(%rbp)
	movl	%eax, -152(%rbp)
	testl	%eax, %eax
	jne	.L1187
	movl	%eax, -152(%rbp)
.L1188:
	movl	-148(%rbp), %edx
	movq	-136(%rbp), %rdi
	movl	$1, %ecx
	movq	%r9, %rsi
	call	_ZN2v88internal13MutableBigInt16SpecialLeftShiftEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEEiNS1_20SpecialLeftShiftModeE
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1243
	movq	-88(%rbp), %rax
	movq	(%rax), %rdi
	movq	-1(%r12,%rdi), %rax
	movq	%rax, -80(%rbp)
	testl	%ebx, %ebx
	js	.L1191
	movl	-96(%rbp), %esi
	leal	16(,%r13,8), %r12d
	movslq	%r12d, %r12
	leal	0(,%rsi,8), %eax
	cltq
	subq	$1, %rax
	movq	%rax, -144(%rbp)
	movslq	%esi, %rax
	movq	%rax, -120(%rbp)
	leal	0(,%r13,8), %eax
	xorl	%r13d, %r13d
	movl	%eax, -92(%rbp)
	leaq	-64(%rbp), %rax
	movq	%rax, -104(%rbp)
	jmp	.L1202
	.p2align 4,,10
	.p2align 3
.L1192:
	movq	(%r15), %r8
	movl	-96(%rbp), %ecx
	movq	%r9, %rsi
	xorl	%edx, %edx
	movq	%r9, -72(%rbp)
	call	_ZN2v88internal13MutableBigInt19InternalMultiplyAddENS0_10BigIntBaseEmmiS1_
	movq	(%r14), %rax
	movq	-104(%rbp), %rdi
	movl	%ebx, %edx
	movq	%r15, %rsi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal13MutableBigInt10InplaceSubENS0_6HandleINS0_10BigIntBaseEEEi
	movq	-72(%rbp), %r9
	testq	%rax, %rax
	jne	.L1244
.L1197:
	cmpq	$0, -112(%rbp)
	je	.L1198
	movq	-128(%rbp), %rsi
	leal	16(,%rbx,8), %eax
	cltq
	movq	(%rsi), %rdx
	movq	%r9, -1(%rax,%rdx)
.L1198:
	addq	-120(%rbp), %r13
	cmpq	$5000000, %r13
	ja	.L1245
	subl	$1, %ebx
	subl	$8, -92(%rbp)
	subq	$8, %r12
	cmpl	$-1, %ebx
	je	.L1191
.L1246:
	movq	-88(%rbp), %rax
	movq	(%rax), %rdi
.L1202:
	movq	(%r14), %rsi
	movq	-80(%rbp), %rcx
	movq	$-1, %r9
	movq	-1(%r12,%rsi), %rdx
	cmpq	%rdx, %rcx
	je	.L1192
	movl	-92(%rbp), %r11d
	leal	8(%r11), %eax
	cltq
	movq	-1(%rsi,%rax), %rax
#APP
# 2614 "../deps/v8/src/objects/bigint.cc" 1
	divq  %rcx
# 0 "" 2
#NO_APP
	movq	%rax, %r9
	movq	-144(%rbp), %rax
	movq	%rdx, %rcx
	movq	(%rax,%rdi), %r8
	movslq	%r11d, %rax
	movq	-1(%rsi,%rax), %rsi
.L1196:
	movq	%r9, %rax
	mulq	%r8
	cmpq	%rcx, %rdx
	ja	.L1193
	jne	.L1192
	cmpq	%rax, %rsi
	jnb	.L1192
	.p2align 4,,10
	.p2align 3
.L1193:
	subq	$1, %r9
	addq	-80(%rbp), %rcx
	jnc	.L1196
	jmp	.L1192
	.p2align 4,,10
	.p2align 3
.L1207:
	movl	$64, -152(%rbp)
	movl	$64, -148(%rbp)
.L1187:
	movl	-148(%rbp), %edx
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%r9, -72(%rbp)
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal13MutableBigInt16SpecialLeftShiftEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEEiNS1_20SpecialLeftShiftModeE
	movq	-72(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, -88(%rbp)
	jne	.L1188
.L1189:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1245:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	movq	%rax, %r8
	movq	-136(%rbp), %rax
	movq	37544(%rax), %rax
	cmpq	%rax, %r8
	jb	.L1200
.L1201:
	subl	$1, %ebx
	subl	$8, -92(%rbp)
	xorl	%r13d, %r13d
	subq	$8, %r12
	cmpl	$-1, %ebx
	jne	.L1246
.L1191:
	movq	-112(%rbp), %rax
	testq	%rax, %rax
	je	.L1203
	movq	-128(%rbp), %rbx
	movq	%rbx, (%rax)
.L1203:
	cmpq	$0, -160(%rbp)
	movl	$1, %eax
	je	.L1182
	movq	(%r14), %rax
	movq	%rax, -64(%rbp)
	movl	-152(%rbp), %eax
	testl	%eax, %eax
	jne	.L1247
	movq	-160(%rbp), %rax
	movq	%r14, (%rax)
	movl	$1, %eax
	jmp	.L1182
	.p2align 4,,10
	.p2align 3
.L1244:
	movq	(%r14), %rax
	movq	-88(%rbp), %rsi
	movl	%ebx, %edx
	movq	-104(%rbp), %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal13MutableBigInt10InplaceAddENS0_6HandleINS0_10BigIntBaseEEEi
	movq	-72(%rbp), %r9
	movq	%rax, %r8
	movq	(%r14), %rax
	subq	$1, %r9
	addq	%r8, -1(%r12,%rax)
	jmp	.L1197
	.p2align 4,,10
	.p2align 3
.L1200:
	movq	-136(%rbp), %r13
	leaq	37512(%r13), %rdi
	call	_ZN2v88internal10StackGuard16HandleInterruptsEv@PLT
	cmpq	%rax, 312(%r13)
	jne	.L1201
	.p2align 4,,10
	.p2align 3
.L1243:
	xorl	%eax, %eax
.L1182:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1248
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1206:
	.cfi_restore_state
	movq	$0, -128(%rbp)
	jmp	.L1183
	.p2align 4,,10
	.p2align 3
.L1247:
	movl	-148(%rbp), %esi
	leaq	-64(%rbp), %rdi
	call	_ZN2v88internal13MutableBigInt17InplaceRightShiftEi.part.0
	movq	-160(%rbp), %rax
	movq	%r14, (%rax)
	movl	$1, %eax
	jmp	.L1182
.L1248:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17886:
	.size	_ZN2v88internal13MutableBigInt16AbsoluteDivLargeEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEES6_PNS4_IS1_EES8_, .-_ZN2v88internal13MutableBigInt16AbsoluteDivLargeEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEES6_PNS4_IS1_EES8_
	.section	.text._ZN2v88internal6BigInt6DivideEPNS0_7IsolateENS0_6HandleIS1_EES5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6BigInt6DivideEPNS0_7IsolateENS0_6HandleIS1_EES5_
	.type	_ZN2v88internal6BigInt6DivideEPNS0_7IsolateENS0_6HandleIS1_EES5_, @function
_ZN2v88internal6BigInt6DivideEPNS0_7IsolateENS0_6HandleIS1_EES5_:
.LFB17821:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	(%rdx), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	7(%rsi), %eax
	testl	$2147483646, %eax
	jne	.L1250
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$181, %esi
	call	_ZN2v88internal7Factory13NewRangeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	xorl	%eax, %eax
.L1251:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1259
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1250:
	.cfi_restore_state
	movq	(%rbx), %rdi
	movq	%rdx, %r12
	call	_ZN2v88internal13MutableBigInt15AbsoluteCompareENS0_10BigIntBaseES2_
	testl	%eax, %eax
	js	.L1260
	movq	(%rbx), %rcx
	movq	$0, -56(%rbp)
	movl	7(%rcx), %esi
	movq	(%r12), %rdx
	movl	7(%rdx), %edi
	movl	7(%rdx), %eax
	shrl	%eax
	andl	$1073741823, %eax
	cmpl	$1, %eax
	jne	.L1253
	movq	15(%rdx), %rdx
	cmpq	$1, %rdx
	je	.L1261
	leaq	-56(%rbp), %rcx
	leaq	-48(%rbp), %r8
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal13MutableBigInt16AbsoluteDivSmallEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEEmPNS4_IS1_EEPm
.L1256:
	movq	-56(%rbp), %rax
	movq	(%rax), %rcx
	movq	(%rbx), %rax
	movl	7(%rax), %eax
	movq	(%r12), %rdx
	movl	7(%rdx), %esi
	movl	7(%rcx), %edx
	xorl	%esi, %eax
	andl	$-2, %edx
	andl	$1, %eax
	orl	%edx, %eax
	movl	%eax, 7(%rcx)
	movq	-56(%rbp), %rbx
	movq	(%rbx), %rdi
	call	_ZN2v88internal13MutableBigInt12CanonicalizeES1_
	movq	%rbx, %rax
	jmp	.L1251
	.p2align 4,,10
	.p2align 3
.L1260:
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal7Factory9NewBigIntEiNS0_14AllocationTypeE@PLT
	movq	%rax, %rbx
	movq	(%rax), %rax
	movl	$0, 7(%rax)
	movq	(%rbx), %rdi
	call	_ZN2v88internal13MutableBigInt12CanonicalizeES1_
	movq	%rbx, %rax
	jmp	.L1251
	.p2align 4,,10
	.p2align 3
.L1253:
	xorl	%r8d, %r8d
	leaq	-56(%rbp), %rcx
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal13MutableBigInt16AbsoluteDivLargeEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEES6_PNS4_IS1_EES8_
	testb	%al, %al
	jne	.L1256
	xorl	%eax, %eax
	jmp	.L1251
	.p2align 4,,10
	.p2align 3
.L1261:
	movl	7(%rcx), %eax
	xorl	%edi, %esi
	andl	$1, %esi
	andl	$1, %eax
	cmpb	%al, %sil
	jne	.L1262
.L1255:
	movq	%r10, %rax
	jmp	.L1251
	.p2align 4,,10
	.p2align 3
.L1262:
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal6BigInt10UnaryMinusEPNS0_7IsolateENS0_6HandleIS1_EE
	movq	%rax, %r10
	jmp	.L1255
.L1259:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17821:
	.size	_ZN2v88internal6BigInt6DivideEPNS0_7IsolateENS0_6HandleIS1_EES5_, .-_ZN2v88internal6BigInt6DivideEPNS0_7IsolateENS0_6HandleIS1_EES5_
	.section	.text._ZN2v88internal6BigInt9RemainderEPNS0_7IsolateENS0_6HandleIS1_EES5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6BigInt9RemainderEPNS0_7IsolateENS0_6HandleIS1_EES5_
	.type	_ZN2v88internal6BigInt9RemainderEPNS0_7IsolateENS0_6HandleIS1_EES5_, @function
_ZN2v88internal6BigInt9RemainderEPNS0_7IsolateENS0_6HandleIS1_EES5_:
.LFB17822:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	(%rdx), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	7(%rsi), %eax
	testl	$2147483646, %eax
	jne	.L1264
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$181, %esi
	call	_ZN2v88internal7Factory13NewRangeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	xorl	%eax, %eax
.L1265:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1281
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1264:
	.cfi_restore_state
	movq	(%rbx), %rdi
	movq	%rdx, %r10
	call	_ZN2v88internal13MutableBigInt15AbsoluteCompareENS0_10BigIntBaseES2_
	testl	%eax, %eax
	jns	.L1266
.L1280:
	movq	%rbx, %rax
	jmp	.L1265
	.p2align 4,,10
	.p2align 3
.L1266:
	movq	(%r10), %rdx
	movq	$0, -48(%rbp)
	movl	7(%rdx), %eax
	shrl	%eax
	andl	$1073741823, %eax
	cmpl	$1, %eax
	jne	.L1267
	movq	15(%rdx), %rsi
	cmpq	$1, %rsi
	je	.L1272
	movq	(%rbx), %rdx
	movl	7(%rdx), %eax
	shrl	%eax
	andl	$1073741823, %eax
	je	.L1272
	leal	8(,%rax,8), %edi
	subq	$1, %rdx
	subl	$1, %eax
	xorl	%r13d, %r13d
	movslq	%edi, %rdi
	salq	$3, %rax
	leaq	(%rdx,%rdi), %rcx
	leaq	-8(%rdx,%rdi), %rdi
	subq	%rax, %rdi
	.p2align 4,,10
	.p2align 3
.L1271:
	movq	(%rcx), %rax
	subq	$8, %rcx
	movq	%r13, %rdx
#APP
# 2614 "../deps/v8/src/objects/bigint.cc" 1
	divq  %rsi
# 0 "" 2
#NO_APP
	movq	%rdx, %r13
	cmpq	%rcx, %rdi
	jne	.L1271
	testq	%rdx, %rdx
	je	.L1272
	xorl	%edx, %edx
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory9NewBigIntEiNS0_14AllocationTypeE@PLT
	movq	(%rax), %rdx
	movl	$2, 7(%rdx)
	movq	%rax, -48(%rbp)
	movq	(%rax), %rax
	movq	%r13, 15(%rax)
.L1274:
	movq	-48(%rbp), %rax
	movq	(%rax), %rcx
	movq	(%rbx), %rax
	movl	7(%rax), %eax
	movl	7(%rcx), %edx
	andl	$1, %eax
	andl	$-2, %edx
	orl	%edx, %eax
	movl	%eax, 7(%rcx)
	movq	-48(%rbp), %rbx
	movq	(%rbx), %rdi
	call	_ZN2v88internal13MutableBigInt12CanonicalizeES1_
	jmp	.L1280
	.p2align 4,,10
	.p2align 3
.L1267:
	xorl	%ecx, %ecx
	leaq	-48(%rbp), %r8
	movq	%r10, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal13MutableBigInt16AbsoluteDivLargeEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEES6_PNS4_IS1_EES8_
	testb	%al, %al
	jne	.L1274
	xorl	%eax, %eax
	jmp	.L1265
	.p2align 4,,10
	.p2align 3
.L1272:
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory9NewBigIntEiNS0_14AllocationTypeE@PLT
	movq	%rax, %rbx
	movq	(%rax), %rax
	movl	$0, 7(%rax)
	movq	(%rbx), %rdi
	call	_ZN2v88internal13MutableBigInt12CanonicalizeES1_
	movq	%rbx, %rax
	jmp	.L1265
.L1281:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17822:
	.size	_ZN2v88internal6BigInt9RemainderEPNS0_7IsolateENS0_6HandleIS1_EES5_, .-_ZN2v88internal6BigInt9RemainderEPNS0_7IsolateENS0_6HandleIS1_EES5_
	.section	.text._ZN2v88internal13MutableBigInt19LeftShiftByAbsoluteEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEES6_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13MutableBigInt19LeftShiftByAbsoluteEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEES6_
	.type	_ZN2v88internal13MutableBigInt19LeftShiftByAbsoluteEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEES6_, @function
_ZN2v88internal13MutableBigInt19LeftShiftByAbsoluteEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEES6_:
.LFB17892:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdx), %rdx
	movl	7(%rdx), %eax
	shrl	%eax
	testl	$1073741822, %eax
	jne	.L1319
	movq	15(%rdx), %r12
	cmpq	$1073741824, %r12
	jbe	.L1320
.L1319:
	cmpb	$0, _ZN2v88internal36FLAG_correctness_fuzzer_suppressionsE(%rip)
	jne	.L1288
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$183, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal7Factory13NewRangeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	xorl	%eax, %eax
.L1312:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1320:
	.cfi_restore_state
	movq	(%rsi), %rax
	movq	%r12, %rdx
	movq	%rsi, %r13
	shrq	$6, %rdx
	movl	7(%rax), %r15d
	shrl	%r15d
	andl	$1073741823, %r15d
	andl	$63, %r12d
	jne	.L1284
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
.L1285:
	leal	(%r15,%rdx), %r8d
	addl	%r8d, %ecx
	cmpl	$16777216, %ecx
	jg	.L1319
	movl	%ecx, %esi
	movq	%r14, %rdi
	movb	%r9b, -65(%rbp)
	movl	%r8d, -56(%rbp)
	movq	%rdx, -64(%rbp)
	movl	%ecx, -52(%rbp)
	call	_ZN2v88internal13MutableBigInt3NewEPNS0_7IsolateEiNS0_14AllocationTypeE.constprop.0
	movl	-52(%rbp), %ecx
	movq	-64(%rbp), %rdx
	movq	%rax, %rbx
	xorl	%eax, %eax
	movl	-56(%rbp), %r8d
	movzbl	-65(%rbp), %r9d
	testq	%rbx, %rbx
	je	.L1312
	testl	%r12d, %r12d
	jne	.L1290
	testq	%rdx, %rdx
	je	.L1294
	leal	-1(%rdx), %eax
	leaq	24(,%rax,8), %rdi
	movl	$16, %eax
	.p2align 4,,10
	.p2align 3
.L1295:
	movq	(%rbx), %rsi
	addq	$8, %rax
	movq	$0, -9(%rax,%rsi)
	cmpq	%rax, %rdi
	jne	.L1295
	movl	%edx, %r12d
.L1294:
	movq	(%rbx), %rsi
	cmpl	%r12d, %ecx
	jle	.L1292
	movl	%r12d, %eax
	movq	0(%r13), %rdi
	subl	%edx, %eax
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%rax,%rdi), %rdi
	leal	16(,%r12,8), %eax
	cltq
	movq	%rdi, -1(%rsi,%rax)
	leal	1(%r12), %esi
	cmpl	%esi, %ecx
	jle	.L1317
	subl	$2, %ecx
	movslq	%r12d, %r8
	leal	24(,%r12,8), %eax
	subl	%edx, %esi
	subl	%r12d, %ecx
	cltq
	leaq	4(%r8,%rcx), %rdi
	movslq	%esi, %rcx
	subq	%r8, %rcx
	salq	$3, %rdi
	salq	$3, %rcx
	.p2align 4,,10
	.p2align 3
.L1298:
	leaq	(%rcx,%rax), %rdx
	addq	0(%r13), %rdx
	addq	$8, %rax
	movq	-9(%rdx), %rsi
	movq	(%rbx), %rdx
	movq	%rsi, -9(%rax,%rdx)
	cmpq	%rax, %rdi
	jne	.L1298
	jmp	.L1317
	.p2align 4,,10
	.p2align 3
.L1284:
	leal	8(,%r15,8), %esi
	movl	$64, %ecx
	movslq	%esi, %rsi
	subl	%r12d, %ecx
	movq	-1(%rsi,%rax), %rax
	shrq	%cl, %rax
	testq	%rax, %rax
	setne	%cl
	setne	%r9b
	movzbl	%cl, %ecx
	jmp	.L1285
	.p2align 4,,10
	.p2align 3
.L1290:
	testq	%rdx, %rdx
	je	.L1302
	leal	-1(%rdx), %eax
	leaq	24(,%rax,8), %rsi
	movl	$16, %eax
	.p2align 4,,10
	.p2align 3
.L1303:
	movq	(%rbx), %rcx
	addq	$8, %rax
	movq	$0, -9(%rax,%rcx)
	cmpq	%rax, %rsi
	jne	.L1303
.L1302:
	testl	%r15d, %r15d
	je	.L1321
	leal	-1(%r15), %eax
	movl	$64, %r10d
	salq	$3, %rdx
	xorl	%r14d, %r14d
	leaq	24(,%rax,8), %r11
	subl	%r12d, %r10d
	movl	$16, %eax
	.p2align 4,,10
	.p2align 3
.L1304:
	movq	0(%r13), %rcx
	leaq	(%rdx,%rax), %rsi
	addq	(%rbx), %rsi
	addq	$8, %rax
	movq	-9(%rax,%rcx), %rdi
	movl	%r12d, %ecx
	movq	%rdi, %r15
	salq	%cl, %r15
	movq	%r15, %rcx
	orq	%r14, %rcx
	movq	%rcx, -1(%rsi)
	movl	%r10d, %ecx
	shrq	%cl, %rdi
	movq	%rdi, %r14
	cmpq	%rax, %r11
	jne	.L1304
.L1300:
	movq	(%rbx), %rsi
	testb	%r9b, %r9b
	je	.L1292
	leal	16(,%r8,8), %eax
	cltq
	movq	%r14, -1(%rsi,%rax)
.L1317:
	movq	(%rbx), %rsi
.L1292:
	movq	0(%r13), %rax
	movl	7(%rax), %eax
	movl	7(%rsi), %edx
	andl	$1, %eax
	andl	$-2, %edx
	orl	%edx, %eax
	movl	%eax, 7(%rsi)
	movq	(%rbx), %rdi
	call	_ZN2v88internal13MutableBigInt12CanonicalizeES1_
	addq	$40, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1321:
	.cfi_restore_state
	xorl	%r14d, %r14d
	jmp	.L1300
.L1288:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE17892:
	.size	_ZN2v88internal13MutableBigInt19LeftShiftByAbsoluteEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEES6_, .-_ZN2v88internal13MutableBigInt19LeftShiftByAbsoluteEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEES6_
	.section	.text._ZN2v88internal13MutableBigInt20RightShiftByAbsoluteEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEES6_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13MutableBigInt20RightShiftByAbsoluteEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEES6_
	.type	_ZN2v88internal13MutableBigInt20RightShiftByAbsoluteEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEES6_, @function
_ZN2v88internal13MutableBigInt20RightShiftByAbsoluteEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEES6_:
.LFB17893:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rsi), %rdi
	movl	7(%rdi), %r13d
	movl	7(%rdi), %r14d
	movq	(%rdx), %rdx
	andl	$1, %r14d
	movl	7(%rdx), %eax
	shrl	%eax
	testl	$1073741822, %eax
	jne	.L1323
	movq	15(%rdx), %r8
	cmpq	$1073741824, %r8
	jbe	.L1384
.L1323:
	testb	%r14b, %r14b
	jne	.L1348
	xorl	%edx, %edx
.L1328:
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal7Factory9NewBigIntEiNS0_14AllocationTypeE@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	movl	$0, 7(%rax)
.L1382:
	movq	(%r12), %rdi
	call	_ZN2v88internal13MutableBigInt12CanonicalizeES1_
.L1329:
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1384:
	.cfi_restore_state
	shrl	%r13d
	movq	%r8, %r9
	movq	%rsi, %rbx
	andl	$1073741823, %r13d
	shrq	$6, %r9
	movl	%r13d, %esi
	movl	%r9d, %edx
	subl	%r9d, %esi
	testl	%esi, %esi
	jle	.L1385
	andl	$63, %r8d
	testb	%r14b, %r14b
	jne	.L1386
.L1330:
	movq	%r15, %rdi
	movl	%edx, -72(%rbp)
	movl	%r8d, -68(%rbp)
	movq	%r9, -64(%rbp)
	movl	%esi, -56(%rbp)
	call	_ZN2v88internal13MutableBigInt3NewEPNS0_7IsolateEiNS0_14AllocationTypeE.constprop.0
	movl	-56(%rbp), %esi
	movq	-64(%rbp), %r9
	testq	%rax, %rax
	movl	-68(%rbp), %r8d
	movl	-72(%rbp), %edx
	movq	%rax, %r12
	je	.L1344
	xorl	%edi, %edi
	testl	%r8d, %r8d
	je	.L1387
	leal	2(%r9), %r10d
.L1336:
	movq	(%rbx), %rdx
	sall	$3, %r10d
	movl	%r8d, %ecx
	movl	%esi, %r13d
	movslq	%r10d, %rax
	movq	-1(%rdx,%rax), %rax
	shrq	%cl, %rax
	subl	$1, %r13d
	je	.L1339
	addl	$8, %r10d
	movq	(%r12), %rcx
	movl	$64, %r11d
	movslq	%r10d, %r10
	subl	%r8d, %r11d
	movq	-1(%rdx,%r10), %rdx
	movq	%rcx, -56(%rbp)
	movl	%r11d, %ecx
	movq	%rdx, %r10
	salq	%cl, %r10
	movq	-56(%rbp), %rcx
	orq	%r10, %rax
	movq	%rax, 15(%rcx)
	movq	%rdx, %rax
	movl	%r8d, %ecx
	shrq	%cl, %rax
	cmpl	$1, %r13d
	je	.L1339
	movl	%esi, -56(%rbp)
	leal	-3(%rsi), %edx
	leaq	8(,%r9,8), %r13
	leaq	32(,%rdx,8), %r10
	movl	$24, %edx
	.p2align 4,,10
	.p2align 3
.L1342:
	leaq	0(%r13,%rdx), %rcx
	addq	(%rbx), %rcx
	addq	$8, %rdx
	movq	-1(%rcx), %r9
	movl	%r11d, %ecx
	movq	%r9, %rsi
	salq	%cl, %rsi
	movl	%r8d, %ecx
	orq	%rsi, %rax
	movq	(%r12), %rsi
	movq	%rax, -9(%rdx,%rsi)
	movq	%r9, %rax
	shrq	%cl, %rax
	cmpq	%rdx, %r10
	jne	.L1342
	movl	-56(%rbp), %esi
.L1339:
	movq	(%r12), %rcx
	leal	8(,%rsi,8), %edx
	movslq	%edx, %rdx
	movq	%rax, -1(%rdx,%rcx)
.L1337:
	testb	%r14b, %r14b
	je	.L1382
	movq	(%r12), %rdx
	movl	7(%rdx), %eax
	orl	$1, %eax
	movl	%eax, 7(%rdx)
	testb	%dil, %dil
	je	.L1382
.L1350:
	movq	(%r12), %rcx
	movq	%r12, %rsi
	movl	$1, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal13MutableBigInt14AbsoluteAddOneEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEEbS1_
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L1382
	.p2align 4,,10
	.p2align 3
.L1344:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1348:
	xorl	%edx, %edx
.L1383:
	movq	%r15, %rdi
	movl	$1, %esi
	call	_ZN2v88internal7Factory9NewBigIntEiNS0_14AllocationTypeE@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	movl	$3, 7(%rax)
	movq	(%r12), %rax
	movq	$1, 15(%rax)
	movq	(%r12), %rdi
	call	_ZN2v88internal13MutableBigInt12CanonicalizeES1_
	jmp	.L1329
	.p2align 4,,10
	.p2align 3
.L1385:
	xorl	%edx, %edx
	testb	%r14b, %r14b
	je	.L1328
	jmp	.L1383
	.p2align 4,,10
	.p2align 3
.L1386:
	leal	2(%r9), %r10d
	movq	$-1, %rax
	movl	%r8d, %ecx
	leal	0(,%r10,8), %r11d
	salq	%cl, %rax
	movslq	%r11d, %r11
	notq	%rax
	testq	%rax, -1(%rdi,%r11)
	jne	.L1331
	testq	%r9, %r9
	je	.L1330
	leal	-1(%r9), %ecx
	leaq	15(%rdi), %rax
	leaq	23(%rdi,%rcx,8), %rcx
	jmp	.L1332
	.p2align 4,,10
	.p2align 3
.L1388:
	addq	$8, %rax
	cmpq	%rax, %rcx
	je	.L1330
.L1332:
	cmpq	$0, (%rax)
	je	.L1388
.L1331:
	testl	%r8d, %r8d
	jne	.L1333
	leal	8(,%r13,8), %eax
	cltq
	cmpq	$-1, -1(%rdi,%rax)
	jne	.L1334
	addl	$1, %esi
.L1334:
	movq	%r15, %rdi
	movl	%edx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_ZN2v88internal13MutableBigInt3NewEPNS0_7IsolateEiNS0_14AllocationTypeE.constprop.0
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1344
	movq	-56(%rbp), %r9
	cmpl	%r13d, %r9d
	jge	.L1346
	movl	-64(%rbp), %edx
	movl	%r14d, %edi
.L1347:
	movslq	%r9d, %r9
	movl	$16, %eax
	salq	$3, %r9
	.p2align 4,,10
	.p2align 3
.L1338:
	leaq	(%rax,%r9), %rcx
	addq	(%rbx), %rcx
	addl	$1, %edx
	addq	$8, %rax
	movq	-1(%rcx), %rsi
	movq	(%r12), %rcx
	movq	%rsi, -9(%rax,%rcx)
	cmpl	%r13d, %edx
	jl	.L1338
	jmp	.L1337
	.p2align 4,,10
	.p2align 3
.L1346:
	movq	(%rax), %rdx
	movl	7(%rdx), %eax
	orl	$1, %eax
	movl	%eax, 7(%rdx)
	jmp	.L1350
.L1387:
	cmpl	%r13d, %r9d
	jl	.L1347
	jmp	.L1337
.L1333:
	movq	%r15, %rdi
	movl	%r10d, -72(%rbp)
	movl	%r8d, -68(%rbp)
	movq	%r9, -64(%rbp)
	movl	%esi, -56(%rbp)
	call	_ZN2v88internal13MutableBigInt3NewEPNS0_7IsolateEiNS0_14AllocationTypeE.constprop.0
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1344
	movl	-56(%rbp), %esi
	movq	-64(%rbp), %r9
	movl	%r14d, %edi
	movl	-68(%rbp), %r8d
	movl	-72(%rbp), %r10d
	jmp	.L1336
	.cfi_endproc
.LFE17893:
	.size	_ZN2v88internal13MutableBigInt20RightShiftByAbsoluteEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEES6_, .-_ZN2v88internal13MutableBigInt20RightShiftByAbsoluteEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEES6_
	.section	.text._ZN2v88internal6BigInt9LeftShiftEPNS0_7IsolateENS0_6HandleIS1_EES5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6BigInt9LeftShiftEPNS0_7IsolateENS0_6HandleIS1_EES5_
	.type	_ZN2v88internal6BigInt9LeftShiftEPNS0_7IsolateENS0_6HandleIS1_EES5_, @function
_ZN2v88internal6BigInt9LeftShiftEPNS0_7IsolateENS0_6HandleIS1_EES5_:
.LFB17825:
	.cfi_startproc
	endbr64
	movq	(%rdx), %rax
	movl	7(%rax), %ecx
	andl	$2147483646, %ecx
	jne	.L1390
.L1392:
	movq	%rsi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1390:
	movq	(%rsi), %rcx
	movl	7(%rcx), %ecx
	andl	$2147483646, %ecx
	je	.L1392
	movl	7(%rax), %eax
	testb	$1, %al
	je	.L1393
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal13MutableBigInt20RightShiftByAbsoluteEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEES6_
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1393:
	.cfi_restore 6
	jmp	_ZN2v88internal13MutableBigInt19LeftShiftByAbsoluteEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEES6_
	.cfi_endproc
.LFE17825:
	.size	_ZN2v88internal6BigInt9LeftShiftEPNS0_7IsolateENS0_6HandleIS1_EES5_, .-_ZN2v88internal6BigInt9LeftShiftEPNS0_7IsolateENS0_6HandleIS1_EES5_
	.section	.text._ZN2v88internal6BigInt16SignedRightShiftEPNS0_7IsolateENS0_6HandleIS1_EES5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6BigInt16SignedRightShiftEPNS0_7IsolateENS0_6HandleIS1_EES5_
	.type	_ZN2v88internal6BigInt16SignedRightShiftEPNS0_7IsolateENS0_6HandleIS1_EES5_, @function
_ZN2v88internal6BigInt16SignedRightShiftEPNS0_7IsolateENS0_6HandleIS1_EES5_:
.LFB17826:
	.cfi_startproc
	endbr64
	movq	(%rdx), %rax
	movl	7(%rax), %ecx
	andl	$2147483646, %ecx
	jne	.L1402
.L1404:
	movq	%rsi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1402:
	movq	(%rsi), %rcx
	movl	7(%rcx), %ecx
	andl	$2147483646, %ecx
	je	.L1404
	movl	7(%rax), %eax
	testb	$1, %al
	je	.L1405
	jmp	_ZN2v88internal13MutableBigInt19LeftShiftByAbsoluteEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEES6_
	.p2align 4,,10
	.p2align 3
.L1405:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal13MutableBigInt20RightShiftByAbsoluteEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEES6_
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17826:
	.size	_ZN2v88internal6BigInt16SignedRightShiftEPNS0_7IsolateENS0_6HandleIS1_EES5_, .-_ZN2v88internal6BigInt16SignedRightShiftEPNS0_7IsolateENS0_6HandleIS1_EES5_
	.section	.text._ZN2v88internal13MutableBigInt19RightShiftByMaximumEPNS0_7IsolateEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13MutableBigInt19RightShiftByMaximumEPNS0_7IsolateEb
	.type	_ZN2v88internal13MutableBigInt19RightShiftByMaximumEPNS0_7IsolateEb, @function
_ZN2v88internal13MutableBigInt19RightShiftByMaximumEPNS0_7IsolateEb:
.LFB17894:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	testb	%sil, %sil
	je	.L1413
	movl	$1, %esi
	call	_ZN2v88internal7Factory9NewBigIntEiNS0_14AllocationTypeE@PLT
	movq	%rax, %rbx
	movq	(%rax), %rax
	movl	$3, 7(%rax)
	movq	(%rbx), %rax
	movq	$1, 15(%rax)
	movq	(%rbx), %rdi
	call	_ZN2v88internal13MutableBigInt12CanonicalizeES1_
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1413:
	.cfi_restore_state
	xorl	%esi, %esi
	call	_ZN2v88internal7Factory9NewBigIntEiNS0_14AllocationTypeE@PLT
	movq	%rax, %rbx
	movq	(%rax), %rax
	movl	$0, 7(%rax)
	movq	(%rbx), %rdi
	call	_ZN2v88internal13MutableBigInt12CanonicalizeES1_
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17894:
	.size	_ZN2v88internal13MutableBigInt19RightShiftByMaximumEPNS0_7IsolateEb, .-_ZN2v88internal13MutableBigInt19RightShiftByMaximumEPNS0_7IsolateEb
	.section	.text._ZN2v88internal13MutableBigInt13ToShiftAmountENS0_6HandleINS0_10BigIntBaseEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13MutableBigInt13ToShiftAmountENS0_6HandleINS0_10BigIntBaseEEE
	.type	_ZN2v88internal13MutableBigInt13ToShiftAmountENS0_6HandleINS0_10BigIntBaseEEE, @function
_ZN2v88internal13MutableBigInt13ToShiftAmountENS0_6HandleINS0_10BigIntBaseEEE:
.LFB17895:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movl	7(%rdx), %eax
	shrl	%eax
	testl	$1073741822, %eax
	je	.L1417
	xorl	%edx, %edx
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1417:
	movq	15(%rdx), %rdx
	movl	$0, %ecx
	cmpq	$1073741825, %rdx
	sbbq	%rax, %rax
	andl	$1, %eax
	cmpq	$1073741825, %rdx
	cmovnb	%rcx, %rdx
	ret
	.cfi_endproc
.LFE17895:
	.size	_ZN2v88internal13MutableBigInt13ToShiftAmountENS0_6HandleINS0_10BigIntBaseEEE, .-_ZN2v88internal13MutableBigInt13ToShiftAmountENS0_6HandleINS0_10BigIntBaseEEE
	.section	.text._ZN2v88internal6BigInt11AllocateForEPNS0_7IsolateEiiNS0_11ShouldThrowENS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6BigInt11AllocateForEPNS0_7IsolateEiiNS0_11ShouldThrowENS0_14AllocationTypeE
	.type	_ZN2v88internal6BigInt11AllocateForEPNS0_7IsolateEiiNS0_11ShouldThrowENS0_14AllocationTypeE, @function
_ZN2v88internal6BigInt11AllocateForEPNS0_7IsolateEiiNS0_11ShouldThrowENS0_14AllocationTypeE:
.LFB17896:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN2v88internalL15kMaxBitsPerCharE(%rip), %rax
	movslq	%esi, %rsi
	movslq	%edx, %r9
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movzbl	(%rax,%rsi), %esi
	movq	$-32, %rax
	divq	%rsi
	cmpq	%r9, %rax
	jb	.L1421
	imulq	%r9, %rsi
	addq	$31, %rsi
	shrq	$5, %rsi
	cmpq	$2147483647, %rsi
	jbe	.L1426
.L1421:
	xorl	%r13d, %r13d
	testl	%ecx, %ecx
	je	.L1427
.L1422:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1426:
	.cfi_restore_state
	addq	$63, %rsi
	movq	%rsi, %rbx
	shrq	$6, %rbx
	cmpq	$1073741887, %rsi
	ja	.L1421
	movl	%r8d, %edx
	movl	%ebx, %esi
	call	_ZN2v88internal7Factory9NewBigIntEiNS0_14AllocationTypeE@PLT
	leal	(%rbx,%rbx), %edx
	xorl	%esi, %esi
	movq	%rax, %r13
	movq	(%rax), %rax
	movl	%edx, 7(%rax)
	movq	0(%r13), %rax
	leal	0(,%rbx,8), %edx
	movslq	%edx, %rdx
	leaq	15(%rax), %rdi
	call	memset@PLT
	jmp	.L1422
	.p2align 4,,10
	.p2align 3
.L1427:
	cmpb	$0, _ZN2v88internal36FLAG_correctness_fuzzer_suppressionsE(%rip)
	jne	.L1428
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$183, %esi
	call	_ZN2v88internal7Factory13NewRangeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	movq	%r12, %rdi
	xorl	%edx, %edx
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1428:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE17896:
	.size	_ZN2v88internal6BigInt11AllocateForEPNS0_7IsolateEiiNS0_11ShouldThrowENS0_14AllocationTypeE, .-_ZN2v88internal6BigInt11AllocateForEPNS0_7IsolateEiiNS0_11ShouldThrowENS0_14AllocationTypeE
	.section	.text._ZN2v88internal6BigInt8FinalizeENS0_6HandleINS0_22FreshlyAllocatedBigIntEEEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6BigInt8FinalizeENS0_6HandleINS0_22FreshlyAllocatedBigIntEEEb
	.type	_ZN2v88internal6BigInt8FinalizeENS0_6HandleINS0_22FreshlyAllocatedBigIntEEEb, @function
_ZN2v88internal6BigInt8FinalizeENS0_6HandleINS0_22FreshlyAllocatedBigIntEEEb:
.LFB17900:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %r8d
	movzbl	%r8b, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rdi), %rdx
	movl	7(%rdx), %esi
	andl	$-2, %esi
	orl	%eax, %esi
	movl	%esi, 7(%rdx)
	movq	(%rdi), %rdi
	call	_ZN2v88internal13MutableBigInt12CanonicalizeES1_
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17900:
	.size	_ZN2v88internal6BigInt8FinalizeENS0_6HandleINS0_22FreshlyAllocatedBigIntEEEb, .-_ZN2v88internal6BigInt8FinalizeENS0_6HandleINS0_22FreshlyAllocatedBigIntEEEb
	.section	.text._ZNK2v88internal6BigInt27GetBitfieldForSerializationEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6BigInt27GetBitfieldForSerializationEv
	.type	_ZNK2v88internal6BigInt27GetBitfieldForSerializationEv, @function
_ZNK2v88internal6BigInt27GetBitfieldForSerializationEv:
.LFB17901:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movl	7(%rdx), %eax
	movl	7(%rdx), %edx
	shrl	%eax
	andl	$1, %edx
	sall	$4, %eax
	orl	%edx, %eax
	ret
	.cfi_endproc
.LFE17901:
	.size	_ZNK2v88internal6BigInt27GetBitfieldForSerializationEv, .-_ZNK2v88internal6BigInt27GetBitfieldForSerializationEv
	.section	.text._ZN2v88internal6BigInt27DigitsByteLengthForBitfieldEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6BigInt27DigitsByteLengthForBitfieldEj
	.type	_ZN2v88internal6BigInt27DigitsByteLengthForBitfieldEj, @function
_ZN2v88internal6BigInt27DigitsByteLengthForBitfieldEj:
.LFB17902:
	.cfi_startproc
	endbr64
	movl	%edi, %eax
	shrl	%eax
	andl	$1073741823, %eax
	ret
	.cfi_endproc
.LFE17902:
	.size	_ZN2v88internal6BigInt27DigitsByteLengthForBitfieldEj, .-_ZN2v88internal6BigInt27DigitsByteLengthForBitfieldEj
	.section	.text._ZN2v88internal6BigInt15SerializeDigitsEPh,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6BigInt15SerializeDigitsEPh
	.type	_ZN2v88internal6BigInt15SerializeDigitsEPh, @function
_ZN2v88internal6BigInt15SerializeDigitsEPh:
.LFB17904:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movq	(%r8), %rsi
	movl	7(%rsi), %edx
	addq	$15, %rsi
	shrl	%edx
	sall	$3, %edx
	movslq	%edx, %rdx
	jmp	memcpy@PLT
	.cfi_endproc
.LFE17904:
	.size	_ZN2v88internal6BigInt15SerializeDigitsEPh, .-_ZN2v88internal6BigInt15SerializeDigitsEPh
	.section	.text._ZN2v88internal6BigInt20FromSerializedDigitsEPNS0_7IsolateEjNS0_6VectorIKhEENS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6BigInt20FromSerializedDigitsEPNS0_7IsolateEjNS0_6VectorIKhEENS0_14AllocationTypeE
	.type	_ZN2v88internal6BigInt20FromSerializedDigitsEPNS0_7IsolateEjNS0_6VectorIKhEENS0_14AllocationTypeE, @function
_ZN2v88internal6BigInt20FromSerializedDigitsEPNS0_7IsolateEjNS0_6VectorIKhEENS0_14AllocationTypeE:
.LFB17905:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	movl	%r8d, %edx
	pushq	%r14
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	andl	$1, %r14d
	shrl	%r13d
	pushq	%r12
	andl	$1073741823, %r13d
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leal	7(%r13), %ebx
	sarl	$3, %ebx
	subq	$8, %rsp
	movl	%ebx, %esi
	call	_ZN2v88internal7Factory9NewBigIntEiNS0_14AllocationTypeE@PLT
	leal	(%rbx,%rbx), %esi
	movq	%rax, %r12
	movq	(%rax), %rax
	orl	%esi, %r14d
	movq	%r15, %rsi
	movl	%r14d, 7(%rax)
	movq	(%r12), %rax
	movslq	%r13d, %r14
	movq	%r14, %rdx
	leaq	15(%rax), %rdi
	call	memcpy@PLT
	leal	0(,%rbx,8), %edx
	xorl	%esi, %esi
	movl	%edx, %ebx
	movq	%rax, %rdi
	subl	%r13d, %ebx
	addq	%r14, %rdi
	movslq	%ebx, %rdx
	call	memset@PLT
	movq	(%r12), %rdi
	call	_ZN2v88internal13MutableBigInt12CanonicalizeES1_
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17905:
	.size	_ZN2v88internal6BigInt20FromSerializedDigitsEPNS0_7IsolateEjNS0_6VectorIKhEENS0_14AllocationTypeE, .-_ZN2v88internal6BigInt20FromSerializedDigitsEPNS0_7IsolateEjNS0_6VectorIKhEENS0_14AllocationTypeE
	.section	.text._ZN2v88internal13MutableBigInt22ToStringBasePowerOfTwoEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEEiNS0_11ShouldThrowE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13MutableBigInt22ToStringBasePowerOfTwoEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEEiNS0_11ShouldThrowE
	.type	_ZN2v88internal13MutableBigInt22ToStringBasePowerOfTwoEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEEiNS0_11ShouldThrowE, @function
_ZN2v88internal13MutableBigInt22ToStringBasePowerOfTwoEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEEiNS0_11ShouldThrowE:
.LFB17908:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rsi), %rax
	movl	7(%rax), %r14d
	movl	7(%rax), %edx
	shrl	%r14d
	movl	%edx, %esi
	movl	%r14d, %r15d
	andl	$1, %esi
	movb	%sil, -61(%rbp)
	andl	$1073741823, %r15d
	testl	%r8d, %r8d
	je	.L1450
	xorl	%ebx, %ebx
	rep bsfl	%r8d, %ebx
	movslq	%ebx, %rsi
	leaq	-1(%rsi), %rdx
.L1437:
	leal	8(,%r15,8), %r9d
	movslq	%r9d, %r9
	movq	-1(%r9,%rax), %r12
	movl	$64, %r9d
	testq	%r12, %r12
	je	.L1438
	bsrq	%r12, %r9
	xorl	$63, %r9d
.L1438:
	movl	%r15d, %eax
	sall	$6, %eax
	subl	%r9d, %eax
	movzbl	-61(%rbp), %r9d
	cltq
	addq	%rdx, %rax
	xorl	%edx, %edx
	divq	%rsi
	addq	%rax, %r9
	cmpq	$1073741799, %r9
	jbe	.L1439
	xorl	%r12d, %r12d
	testl	%ecx, %ecx
	je	.L1465
.L1441:
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1465:
	.cfi_restore_state
	movq	%rdi, -56(%rbp)
	call	_ZN2v88internal7Factory27NewInvalidStringLengthErrorEv@PLT
	movq	-56(%rbp), %rdi
	xorl	%edx, %edx
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	jmp	.L1441
	.p2align 4,,10
	.p2align 3
.L1450:
	movl	$32, %ebx
	movl	$31, %edx
	movl	$32, %esi
	jmp	.L1437
	.p2align 4,,10
	.p2align 3
.L1439:
	xorl	%edx, %edx
	movl	%r9d, %esi
	movl	%r8d, -60(%rbp)
	movq	%r9, -56(%rbp)
	call	_ZN2v88internal7Factory19NewRawOneByteStringEiNS0_14AllocationTypeE@PLT
	movq	-56(%rbp), %r9
	movl	-60(%rbp), %r8d
	testq	%rax, %rax
	movq	%rax, -80(%rbp)
	je	.L1466
	leal	-1(%r8), %ecx
	movq	(%rax), %r8
	andl	$1073741822, %r14d
	leal	-1(%r9), %eax
	movl	%ecx, -56(%rbp)
	leaq	15(%r8), %r11
	je	.L1452
	leal	-2(%r15), %edx
	movq	%r12, -88(%rbp)
	movl	$16, %r10d
	xorl	%esi, %esi
	leaq	24(,%rdx,8), %rdi
	movslq	%ecx, %r9
	xorl	%edx, %edx
	movq	%r11, %r15
	movq	%rdi, -72(%rbp)
	leaq	_ZN2v88internalL16kConversionCharsE(%rip), %rdi
	.p2align 4,,10
	.p2align 3
.L1446:
	movq	0(%r13), %rcx
	movslq	%eax, %r12
	movq	-1(%r10,%rcx), %r11
	leal	-1(%rax), %ecx
	movl	%ecx, -60(%rbp)
	movl	%esi, %ecx
	movq	%r11, %r14
	salq	%cl, %r14
	movl	%ebx, %ecx
	orl	%r14d, %edx
	andl	-56(%rbp), %edx
	subl	%esi, %ecx
	movl	$64, %esi
	movslq	%edx, %rdx
	subl	%ecx, %esi
	movzbl	(%rdi,%rdx), %edx
	movb	%dl, (%r15,%r12)
	movq	%r11, %rdx
	shrq	%cl, %rdx
	cmpl	%ebx, %esi
	jl	.L1453
	subl	$2, %eax
	movl	%ebx, %ecx
	movslq	%eax, %r11
	.p2align 4,,10
	.p2align 3
.L1445:
	movq	%rdx, %rbx
	subl	%ecx, %esi
	movl	%r11d, %eax
	shrq	%cl, %rdx
	andq	%r9, %rbx
	movzbl	(%rdi,%rbx), %ebx
	movb	%bl, 16(%r8,%r11)
	subq	$1, %r11
	cmpl	%ecx, %esi
	jge	.L1445
	movl	%ecx, %ebx
.L1444:
	addq	$8, %r10
	cmpq	%r10, -72(%rbp)
	jne	.L1446
	movq	-88(%rbp), %r12
	movl	%ebx, %r9d
	movl	%esi, %ecx
	subl	%esi, %r9d
	movq	%r12, %r10
	salq	%cl, %r10
.L1443:
	movl	-56(%rbp), %r14d
	orl	%r10d, %edx
	movslq	%eax, %rcx
	leal	-1(%rax), %esi
	andl	%r14d, %edx
	movslq	%edx, %rdx
	movzbl	(%rdi,%rdx), %edx
	movb	%dl, 15(%r8,%rcx)
	movl	%r9d, %ecx
	shrq	%cl, %r12
	testq	%r12, %r12
	je	.L1447
	subl	$2, %eax
	movslq	%r14d, %rdx
	movl	%ebx, %ecx
	cltq
	.p2align 4,,10
	.p2align 3
.L1448:
	movq	%r12, %r9
	shrq	%cl, %r12
	movl	%eax, %esi
	andq	%rdx, %r9
	movzbl	(%rdi,%r9), %r9d
	movb	%r9b, 16(%r8,%rax)
	subq	$1, %rax
	testq	%r12, %r12
	jne	.L1448
.L1447:
	cmpb	$0, -61(%rbp)
	je	.L1449
	movslq	%esi, %rsi
	movb	$45, 15(%r8,%rsi)
.L1449:
	movq	-80(%rbp), %r12
	jmp	.L1441
	.p2align 4,,10
	.p2align 3
.L1453:
	movl	-60(%rbp), %eax
	jmp	.L1444
.L1452:
	movl	%ebx, %r9d
	movq	%r12, %r10
	leaq	_ZN2v88internalL16kConversionCharsE(%rip), %rdi
	xorl	%edx, %edx
	jmp	.L1443
.L1466:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE17908:
	.size	_ZN2v88internal13MutableBigInt22ToStringBasePowerOfTwoEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEEiNS0_11ShouldThrowE, .-_ZN2v88internal13MutableBigInt22ToStringBasePowerOfTwoEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEEiNS0_11ShouldThrowE
	.section	.text._ZN2v88internal13MutableBigInt15ToStringGenericEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEEiNS0_11ShouldThrowE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13MutableBigInt15ToStringGenericEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEEiNS0_11ShouldThrowE
	.type	_ZN2v88internal13MutableBigInt15ToStringGenericEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEEiNS0_11ShouldThrowE, @function
_ZN2v88internal13MutableBigInt15ToStringGenericEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEEiNS0_11ShouldThrowE:
.LFB17913:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -88(%rbp)
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	7(%rdi), %esi
	movl	7(%rdi), %eax
	shrl	%esi
	andl	$1, %eax
	movl	%esi, %r13d
	movb	%al, -149(%rbp)
	movl	%esi, %eax
	leal	8(,%rsi,8), %esi
	movslq	%esi, %rsi
	andl	$1073741823, %r13d
	sall	$6, %eax
	movq	-1(%rsi,%rdi), %rsi
	movl	$64, %edi
	testq	%rsi, %rsi
	je	.L1468
	bsrq	%rsi, %rdi
	xorl	$63, %edi
.L1468:
	movslq	%edx, %rbx
	leaq	_ZN2v88internalL15kMaxBitsPerCharE(%rip), %rdx
	subl	%edi, %eax
	movzbl	(%rdx,%rbx), %r14d
	salq	$5, %rax
	movq	%rax, %rdx
	leal	-1(%r14), %esi
	movzbl	%sil, %eax
	movzbl	%sil, %esi
	subl	$1, %eax
	cltq
	addq	%rdx, %rax
	xorl	%edx, %edx
	divq	%rsi
	movzbl	-149(%rbp), %edx
	addq	%rdx, %rax
	movq	%rax, -168(%rbp)
	cmpq	$1073741799, %rax
	jbe	.L1469
	xorl	%r12d, %r12d
	testl	%ecx, %ecx
	je	.L1511
.L1471:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1512
	addq	$136, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1511:
	.cfi_restore_state
	movq	%r11, %rdi
	movq	%r11, -96(%rbp)
	call	_ZN2v88internal7Factory27NewInvalidStringLengthErrorEv@PLT
	movq	-96(%rbp), %r11
	xorl	%edx, %edx
	movq	(%rax), %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	jmp	.L1471
	.p2align 4,,10
	.p2align 3
.L1469:
	movl	-168(%rbp), %esi
	xorl	%edx, %edx
	movq	%r11, %rdi
	movq	%r11, -96(%rbp)
	call	_ZN2v88internal7Factory19NewRawOneByteStringEiNS0_14AllocationTypeE@PLT
	movq	-96(%rbp), %r11
	testq	%rax, %rax
	movq	%rax, -128(%rbp)
	je	.L1513
	leaq	37592(%r11), %rax
	movq	%rax, -176(%rbp)
	cmpl	$1, %r13d
	je	.L1514
	movl	$2048, %eax
	xorl	%edx, %edx
	movl	$1, %ecx
	divl	%r14d
	movq	%rbx, %rdx
	movl	%eax, %eax
	movq	%rax, %r12
	.p2align 4,,10
	.p2align 3
.L1476:
	testb	$1, %al
	je	.L1475
	imulq	%rdx, %rcx
.L1475:
	imulq	%rdx, %rdx
	shrq	%rax
	jne	.L1476
	leaq	-64(%rbp), %rdi
	movq	%r11, -120(%rbp)
	movslq	%r13d, %rax
	leal	-1(%r13), %r15d
	movq	%rdi, -160(%rbp)
	leaq	-72(%rbp), %rdi
	xorl	%r10d, %r10d
	xorl	%r14d, %r14d
	movq	%rdi, -96(%rbp)
	leal	-1(%r12), %edi
	leaq	_ZN2v88internalL16kConversionCharsE(%rip), %r13
	movq	%rcx, -136(%rbp)
	movq	$0, -72(%rbp)
	movl	%edi, -148(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-88(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L1482:
	movq	-160(%rbp), %r8
	movq	-96(%rbp), %rcx
	movq	%r10, -112(%rbp)
	movq	(%rax), %rsi
	movq	-120(%rbp), %rdi
	movq	-136(%rbp), %rdx
	call	_ZN2v88internal13MutableBigInt16AbsoluteDivSmallEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEEmPNS4_IS1_EEPm
	movq	-128(%rbp), %rax
	movq	-112(%rbp), %r10
	xorl	%ecx, %ecx
	movslq	%r14d, %r8
	movq	(%rax), %rax
	movq	%rax, -104(%rbp)
	leaq	15(%rax), %rdi
	movq	-64(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L1477:
	xorl	%edx, %edx
	leaq	(%rdi,%rcx), %rsi
	addq	$1, %rcx
	divq	%rbx
	movzbl	0(%r13,%rdx), %eax
	xorl	%edx, %edx
	movb	%al, (%rsi,%r8)
	movq	-64(%rbp), %rax
	divq	%rbx
	movq	%rax, -64(%rbp)
	cmpl	%ecx, %r12d
	jg	.L1477
	testl	%r12d, %r12d
	movq	-72(%rbp), %rdx
	movl	$0, %eax
	cmovg	-148(%rbp), %eax
	movq	(%rdx), %rdx
	leal	1(%r14,%rax), %r14d
	leal	16(,%r15,8), %eax
	cltq
	cmpq	$1, -1(%rax,%rdx)
	sbbl	$0, %r15d
	addq	-144(%rbp), %r10
	cmpq	$500000, %r10
	ja	.L1515
.L1479:
	movq	-96(%rbp), %rax
	testl	%r15d, %r15d
	jg	.L1482
	movq	-72(%rbp), %rax
	movq	(%rax), %rax
	movq	15(%rax), %rsi
.L1474:
	movq	-128(%rbp), %rax
	leal	1(%r14), %ecx
	movslq	%ecx, %rcx
	movq	(%rax), %r8
	leaq	15(%r8), %r12
	leaq	14(%r8), %rdi
	.p2align 4,,10
	.p2align 3
.L1483:
	movq	%rsi, %rax
	xorl	%edx, %edx
	movl	%ecx, %r9d
	addq	$1, %rcx
	divq	%rbx
	movzbl	0(%r13,%rdx), %edx
	movb	%dl, -1(%rcx,%rdi)
	movq	%rsi, %rdx
	movq	%rax, %rsi
	cmpq	%rbx, %rdx
	jnb	.L1483
	cmpl	$1, %r9d
	je	.L1495
	movslq	%r9d, %rax
	jmp	.L1485
	.p2align 4,,10
	.p2align 3
.L1516:
	subq	$1, %rax
	movl	%eax, %r9d
	cmpl	$1, %eax
	je	.L1495
.L1485:
	cmpb	$48, (%rax,%rdi)
	movl	%eax, %r9d
	je	.L1516
	cmpb	$0, -149(%rbp)
	jne	.L1486
.L1518:
	leal	-1(%r9), %ebx
	movl	%r9d, %edx
.L1487:
	cmpl	%edx, -168(%rbp)
	jg	.L1517
.L1489:
	movslq	%ebx, %r8
	xorl	%eax, %eax
	addq	%r12, %r8
	testl	%ebx, %ebx
	je	.L1492
	.p2align 4,,10
	.p2align 3
.L1491:
	movq	%r8, %rdx
	leaq	(%r12,%rax), %rcx
	subq	%rax, %rdx
	movzbl	(%rcx), %esi
	addq	$1, %rax
	movzbl	(%rdx), %edi
	movb	%dil, (%rcx)
	movb	%sil, (%rdx)
	movl	%ebx, %edx
	subl	%eax, %edx
	cmpl	%eax, %edx
	jg	.L1491
.L1492:
	movq	-128(%rbp), %r12
	jmp	.L1471
.L1514:
	movq	(%r12), %rax
	xorl	%r14d, %r14d
	leaq	_ZN2v88internalL16kConversionCharsE(%rip), %r13
	movq	15(%rax), %rsi
	jmp	.L1474
	.p2align 4,,10
	.p2align 3
.L1515:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	movq	%rax, %r8
	movq	-120(%rbp), %rax
	movq	37544(%rax), %rax
	cmpq	%rax, %r8
	jb	.L1480
.L1481:
	xorl	%r10d, %r10d
	jmp	.L1479
	.p2align 4,,10
	.p2align 3
.L1480:
	movq	-120(%rbp), %rax
	leaq	37512(%rax), %rdi
	call	_ZN2v88internal10StackGuard16HandleInterruptsEv@PLT
	movq	-176(%rbp), %rdi
	cmpq	-37280(%rdi), %rax
	jne	.L1481
	xorl	%r12d, %r12d
	jmp	.L1471
.L1495:
	cmpb	$0, -149(%rbp)
	movl	$1, %eax
	je	.L1518
.L1486:
	movb	$45, 15(%r8,%rax)
	leal	1(%r9), %edx
	movl	%r9d, %ebx
	jmp	.L1487
.L1517:
	movq	-128(%rbp), %rax
	leal	23(%rdx), %ecx
	andl	$-8, %ecx
	movq	(%rax), %rax
	movl	%edx, 11(%rax)
	movl	-168(%rbp), %eax
	addl	$23, %eax
	andl	$-8, %eax
	cmpl	%eax, %ecx
	jge	.L1489
	subl	%ecx, %eax
	movq	-176(%rbp), %rdi
	movslq	%ecx, %rcx
	movl	$1, %r8d
	movl	%eax, %edx
	movq	-128(%rbp), %rax
	movq	(%rax), %rax
	leaq	-1(%rax,%rcx), %rsi
	movl	$1, %ecx
	call	_ZN2v88internal4Heap20CreateFillerObjectAtEmiNS0_18ClearRecordedSlotsENS0_20ClearFreedMemoryModeE@PLT
	jmp	.L1489
.L1513:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L1512:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17913:
	.size	_ZN2v88internal13MutableBigInt15ToStringGenericEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEEiNS0_11ShouldThrowE, .-_ZN2v88internal13MutableBigInt15ToStringGenericEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEEiNS0_11ShouldThrowE
	.section	.text._ZN2v88internal6BigInt8ToStringEPNS0_7IsolateENS0_6HandleIS1_EEiNS0_11ShouldThrowE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6BigInt8ToStringEPNS0_7IsolateENS0_6HandleIS1_EEiNS0_11ShouldThrowE
	.type	_ZN2v88internal6BigInt8ToStringEPNS0_7IsolateENS0_6HandleIS1_EEiNS0_11ShouldThrowE, @function
_ZN2v88internal6BigInt8ToStringEPNS0_7IsolateENS0_6HandleIS1_EEiNS0_11ShouldThrowE:
.LFB17846:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	movl	7(%rax), %eax
	testl	$2147483646, %eax
	jne	.L1520
	leaq	3608(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1520:
	testl	%edx, %edx
	jle	.L1522
	leal	-1(%rdx), %eax
	testl	%edx, %eax
	jne	.L1522
	jmp	_ZN2v88internal13MutableBigInt22ToStringBasePowerOfTwoEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEEiNS0_11ShouldThrowE
	.p2align 4,,10
	.p2align 3
.L1522:
	jmp	_ZN2v88internal13MutableBigInt15ToStringGenericEPNS0_7IsolateENS0_6HandleINS0_10BigIntBaseEEEiNS0_11ShouldThrowE
	.cfi_endproc
.LFE17846:
	.size	_ZN2v88internal6BigInt8ToStringEPNS0_7IsolateENS0_6HandleIS1_EEiNS0_11ShouldThrowE, .-_ZN2v88internal6BigInt8ToStringEPNS0_7IsolateENS0_6HandleIS1_EEiNS0_11ShouldThrowE
	.section	.text._ZN2v88internal13MutableBigInt15TruncateToNBitsEPNS0_7IsolateEiNS0_6HandleINS0_6BigIntEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13MutableBigInt15TruncateToNBitsEPNS0_7IsolateEiNS0_6HandleINS0_6BigIntEEE
	.type	_ZN2v88internal13MutableBigInt15TruncateToNBitsEPNS0_7IsolateEiNS0_6HandleINS0_6BigIntEEE, @function
_ZN2v88internal13MutableBigInt15TruncateToNBitsEPNS0_7IsolateEiNS0_6HandleINS0_6BigIntEEE:
.LFB17916:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %eax
	addl	$63, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leal	126(%rsi), %r14d
	cmovns	%eax, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	sarl	$6, %r14d
	movq	%rdx, %rbx
	movl	%r14d, %esi
	call	_ZN2v88internal13MutableBigInt3NewEPNS0_7IsolateEiNS0_14AllocationTypeE.constprop.0
	testq	%rax, %rax
	je	.L1534
	movq	%rax, %r12
	cmpl	$64, %r13d
	jle	.L1526
	leal	-2(%r14), %eax
	movl	$16, %ecx
	leaq	24(,%rax,8), %rdx
	.p2align 4,,10
	.p2align 3
.L1527:
	movq	(%rbx), %rax
	addq	$8, %rcx
	movq	-9(%rcx,%rax), %rsi
	movq	(%r12), %rax
	movq	%rsi, -9(%rcx,%rax)
	cmpq	%rcx, %rdx
	jne	.L1527
.L1526:
	movq	(%rbx), %rdx
	leal	8(,%r14,8), %eax
	cltq
	movq	-1(%rax,%rdx), %rdx
	testb	$63, %r13b
	je	.L1528
	movl	%r13d, %ecx
	sarl	$31, %ecx
	shrl	$26, %ecx
	addl	%ecx, %r13d
	andl	$63, %r13d
	subl	%r13d, %ecx
	addl	$64, %ecx
	salq	%cl, %rdx
	shrq	%cl, %rdx
.L1528:
	movq	(%r12), %rcx
	movq	%rdx, -1(%rax,%rcx)
	movq	(%r12), %rcx
	movq	(%rbx), %rax
	movl	7(%rax), %eax
	movl	7(%rcx), %edx
	andl	$1, %eax
	andl	$-2, %edx
	orl	%edx, %eax
	movl	%eax, 7(%rcx)
	movq	(%r12), %rdi
	call	_ZN2v88internal13MutableBigInt12CanonicalizeES1_
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1534:
	.cfi_restore_state
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE17916:
	.size	_ZN2v88internal13MutableBigInt15TruncateToNBitsEPNS0_7IsolateEiNS0_6HandleINS0_6BigIntEEE, .-_ZN2v88internal13MutableBigInt15TruncateToNBitsEPNS0_7IsolateEiNS0_6HandleINS0_6BigIntEEE
	.section	.text._ZN2v88internal13MutableBigInt28TruncateAndSubFromPowerOfTwoEPNS0_7IsolateEiNS0_6HandleINS0_6BigIntEEEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13MutableBigInt28TruncateAndSubFromPowerOfTwoEPNS0_7IsolateEiNS0_6HandleINS0_6BigIntEEEb
	.type	_ZN2v88internal13MutableBigInt28TruncateAndSubFromPowerOfTwoEPNS0_7IsolateEiNS0_6HandleINS0_6BigIntEEEb, @function
_ZN2v88internal13MutableBigInt28TruncateAndSubFromPowerOfTwoEPNS0_7IsolateEiNS0_6HandleINS0_6BigIntEEEb:
.LFB17917:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leal	126(%rsi), %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$88, %rsp
	movl	%esi, -84(%rbp)
	addl	$63, %esi
	cmovns	%esi, %r15d
	movq	%r9, -64(%rbp)
	movq	%r10, -56(%rbp)
	sarl	$6, %r15d
	movl	%ecx, -88(%rbp)
	movl	%r15d, %esi
	call	_ZN2v88internal13MutableBigInt3NewEPNS0_7IsolateEiNS0_14AllocationTypeE.constprop.0
	testq	%rax, %rax
	je	.L1551
	movq	%rax, %r14
	movq	(%rbx), %rax
	leal	-1(%r15), %esi
	movl	%esi, -80(%rbp)
	movl	7(%rax), %edx
	shrl	%edx
	movl	%edx, %edi
	andl	$1073741823, %edi
	cmpl	%edi, %esi
	movl	%edi, -92(%rbp)
	cmovle	%esi, %edi
	movl	%edi, -76(%rbp)
	testl	%edi, %edi
	jle	.L1552
	movl	%edi, %edx
	movq	%r14, %r8
	movq	%rbx, -72(%rbp)
	movl	$16, %ecx
	subl	$1, %edx
	movl	%r15d, -96(%rbp)
	leaq	24(,%rdx,8), %rdi
	movq	%r12, -112(%rbp)
	movq	%rdi, -64(%rbp)
	xorl	%edi, %edi
	movq	%r13, -104(%rbp)
	movq	%rdi, %r14
	jmp	.L1541
	.p2align 4,,10
	.p2align 3
.L1553:
	movq	-72(%rbp), %rax
	movq	(%rax), %rax
.L1541:
	movq	-1(%rcx,%rax), %rbx
	xorl	%r10d, %r10d
	xorl	%r12d, %r12d
	movq	%r14, %rdx
	movq	%r10, %r15
	movq	%rbx, %rax
	negq	%rax
	cmpq	%r14, %rax
	sbbq	%r12, %r15
	xorl	%edi, %edi
	movq	%r15, %r14
	movq	%rdi, %r15
	andl	$1, %r14d
	movq	%r14, %r13
	movq	%rbx, %r14
	negq	%r14
	adcq	$0, %r15
	subq	%rdx, %rax
	addq	$8, %rcx
	negq	%r15
	movq	%r15, %r14
	movq	(%r8), %r15
	andl	$1, %r14d
	movq	%rax, -9(%rcx,%r15)
	addq	%r13, %r14
	cmpq	%rcx, -64(%rbp)
	jne	.L1553
	movq	-72(%rbp), %rbx
	movl	-96(%rbp), %r15d
	movq	%r14, %rdi
	movq	%r8, %r14
.L1540:
	movl	-80(%rbp), %edx
	cmpl	%edx, -76(%rbp)
	jge	.L1538
	movslq	-76(%rbp), %r10
	leal	-2(%r15), %eax
	movq	%rbx, %r9
	subl	%r10d, %eax
	leal	16(,%r10,8), %esi
	leaq	3(%rax,%r10), %r8
	movslq	%esi, %rsi
	salq	$3, %r8
	.p2align 4,,10
	.p2align 3
.L1542:
	xorl	%r13d, %r13d
	movq	%rdi, %rcx
	movq	%rdi, %rax
	movq	(%r14), %rdx
	negq	%rcx
	movq	%r13, %rbx
	adcq	$0, %rbx
	negq	%rax
	addq	$8, %rsi
	negq	%rbx
	movq	%rax, -9(%rsi,%rdx)
	movq	%rbx, %rdi
	andl	$1, %edi
	cmpq	%rsi, %r8
	jne	.L1542
	movq	%r9, %rbx
.L1538:
	leal	8(,%r15,8), %eax
	movl	-92(%rbp), %ecx
	xorl	%edx, %edx
	cltq
	cmpl	%ecx, -80(%rbp)
	jl	.L1554
.L1543:
	testb	$63, -84(%rbp)
	je	.L1555
	movl	-84(%rbp), %ebx
	movl	$64, %r8d
	movl	%ebx, %esi
	sarl	$31, %esi
	shrl	$26, %esi
	addl	%esi, %ebx
	movl	%ebx, %ecx
	andl	$63, %ecx
	subl	%esi, %ecx
	movl	$1, %esi
	salq	%cl, %rsi
	subl	%ecx, %r8d
	movq	%rsi, %rbx
	movl	%r8d, %ecx
	subq	$1, %rsi
	subq	%rdi, %rbx
	salq	%cl, %rdx
	movq	%rbx, %rdi
	shrq	%cl, %rdx
	subq	%rdx, %rdi
	andq	%rsi, %rdi
.L1545:
	movq	(%r14), %rdx
	movq	%rdi, -1(%rax,%rdx)
	movq	(%r14), %rdx
	movl	7(%rdx), %ecx
	movzbl	-88(%rbp), %eax
	andl	$-2, %ecx
	orl	%eax, %ecx
	movl	%ecx, 7(%rdx)
	movq	(%r14), %rdi
	call	_ZN2v88internal13MutableBigInt12CanonicalizeES1_
	addq	$88, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1555:
	.cfi_restore_state
	addq	%rdx, %rdi
	negq	%rdi
	jmp	.L1545
	.p2align 4,,10
	.p2align 3
.L1554:
	movq	(%rbx), %rdx
	movq	-1(%rax,%rdx), %rdx
	jmp	.L1543
	.p2align 4,,10
	.p2align 3
.L1551:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1552:
	movl	$0, -76(%rbp)
	xorl	%edi, %edi
	jmp	.L1540
	.cfi_endproc
.LFE17917:
	.size	_ZN2v88internal13MutableBigInt28TruncateAndSubFromPowerOfTwoEPNS0_7IsolateEiNS0_6HandleINS0_6BigIntEEEb, .-_ZN2v88internal13MutableBigInt28TruncateAndSubFromPowerOfTwoEPNS0_7IsolateEiNS0_6HandleINS0_6BigIntEEEb
	.section	.text._ZN2v88internal6BigInt6AsIntNEPNS0_7IsolateEmNS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6BigInt6AsIntNEPNS0_7IsolateEmNS0_6HandleIS1_EE
	.type	_ZN2v88internal6BigInt6AsIntNEPNS0_7IsolateEmNS0_6HandleIS1_EE, @function
_ZN2v88internal6BigInt6AsIntNEPNS0_7IsolateEmNS0_6HandleIS1_EE:
.LFB17914:
	.cfi_startproc
	endbr64
	movq	(%rdx), %r8
	movl	7(%r8), %eax
	testl	$2147483646, %eax
	je	.L1586
	testq	%rsi, %rsi
	jne	.L1559
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	call	_ZN2v88internal7Factory9NewBigIntEiNS0_14AllocationTypeE@PLT
	movq	%rax, %rbx
	movq	(%rax), %rax
	movl	$0, 7(%rax)
	movq	(%rbx), %rdi
	call	_ZN2v88internal13MutableBigInt12CanonicalizeES1_
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
.L1565:
	.cfi_restore 3
	.cfi_restore 6
	cmpq	%r11, %rcx
	jne	.L1568
	testb	%r10b, %r10b
	je	.L1568
.L1586:
	movq	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1559:
	movl	7(%r8), %eax
	leaq	63(%rsi), %r9
	shrq	$6, %r9
	shrl	%eax
	andl	$1073741823, %eax
	cmpq	%rax, %r9
	ja	.L1586
	leal	8(,%r9,8), %ecx
	movl	$1, %r10d
	movslq	%ecx, %rcx
	movq	-1(%r8,%rcx), %r11
	leal	-1(%rsi), %ecx
	salq	%cl, %r10
	cmpq	%rax, %r9
	movq	%r10, %rcx
	sete	%r10b
	cmpq	%r11, %rcx
	jbe	.L1561
	testb	%r10b, %r10b
	jne	.L1586
.L1561:
	movq	%rcx, %rax
	andq	%r11, %rax
	cmpq	%rcx, %rax
	je	.L1562
.L1568:
	jmp	_ZN2v88internal13MutableBigInt15TruncateToNBitsEPNS0_7IsolateEiNS0_6HandleINS0_6BigIntEEE
	.p2align 4,,10
	.p2align 3
.L1562:
	movl	7(%r8), %eax
	testb	$1, %al
	je	.L1587
	leaq	-1(%rcx), %rax
	testq	%r11, %rax
	jne	.L1564
	cmpl	$1, %r9d
	jle	.L1565
	leal	0(,%r9,8), %eax
	cltq
	leaq	-1(%rax,%r8), %rax
	leaq	-9(%r8,%r9,8), %r8
	subl	$2, %r9d
	salq	$3, %r9
	subq	%r9, %r8
	jmp	.L1567
	.p2align 4,,10
	.p2align 3
.L1566:
	subq	$8, %rax
	cmpq	%rax, %r8
	je	.L1565
.L1567:
	cmpq	$0, (%rax)
	je	.L1566
.L1564:
	xorl	%ecx, %ecx
	jmp	_ZN2v88internal13MutableBigInt28TruncateAndSubFromPowerOfTwoEPNS0_7IsolateEiNS0_6HandleINS0_6BigIntEEEb
.L1587:
	movl	$1, %ecx
	jmp	_ZN2v88internal13MutableBigInt28TruncateAndSubFromPowerOfTwoEPNS0_7IsolateEiNS0_6HandleINS0_6BigIntEEEb
	.cfi_endproc
.LFE17914:
	.size	_ZN2v88internal6BigInt6AsIntNEPNS0_7IsolateEmNS0_6HandleIS1_EE, .-_ZN2v88internal6BigInt6AsIntNEPNS0_7IsolateEmNS0_6HandleIS1_EE
	.section	.text._ZN2v88internal6BigInt7AsUintNEPNS0_7IsolateEmNS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6BigInt7AsUintNEPNS0_7IsolateEmNS0_6HandleIS1_EE
	.type	_ZN2v88internal6BigInt7AsUintNEPNS0_7IsolateEmNS0_6HandleIS1_EE, @function
_ZN2v88internal6BigInt7AsUintNEPNS0_7IsolateEmNS0_6HandleIS1_EE:
.LFB17915:
	.cfi_startproc
	endbr64
	movq	(%rdx), %rax
	movl	7(%rax), %ecx
	andl	$2147483646, %ecx
	jne	.L1589
	movq	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1589:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	testq	%rsi, %rsi
	jne	.L1591
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory9NewBigIntEiNS0_14AllocationTypeE@PLT
	movq	%rax, %rbx
	movq	(%rax), %rax
	movl	$0, 7(%rax)
	movq	(%rbx), %rdi
	call	_ZN2v88internal13MutableBigInt12CanonicalizeES1_
	movq	%rbx, %rax
.L1590:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1591:
	.cfi_restore_state
	movl	7(%rax), %ecx
	andl	$1, %ecx
	je	.L1592
	cmpq	$1073741824, %rsi
	jbe	.L1593
	cmpb	$0, _ZN2v88internal36FLAG_correctness_fuzzer_suppressionsE(%rip)
	jne	.L1603
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$183, %esi
	movq	%rdi, -24(%rbp)
	call	_ZN2v88internal7Factory13NewRangeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	movq	-24(%rbp), %rdi
	xorl	%edx, %edx
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	xorl	%eax, %eax
	jmp	.L1590
	.p2align 4,,10
	.p2align 3
.L1592:
	cmpq	$1073741823, %rsi
	jbe	.L1595
.L1602:
	addq	$24, %rsp
	movq	%rdx, %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1593:
	.cfi_restore_state
	xorl	%ecx, %ecx
	call	_ZN2v88internal13MutableBigInt28TruncateAndSubFromPowerOfTwoEPNS0_7IsolateEiNS0_6HandleINS0_6BigIntEEEb
	jmp	.L1590
	.p2align 4,,10
	.p2align 3
.L1595:
	movl	7(%rax), %ecx
	leaq	63(%rsi), %r8
	shrq	$6, %r8
	shrl	%ecx
	andl	$1073741823, %ecx
	cmpl	%ecx, %r8d
	jg	.L1602
	movl	7(%rax), %ecx
	shrl	%ecx
	andl	$1073741823, %ecx
	cmpl	%ecx, %r8d
	jne	.L1597
	movl	%esi, %ecx
	andl	$63, %ecx
	je	.L1602
	leal	8(,%r8,8), %r8d
	movslq	%r8d, %r8
	movq	-1(%r8,%rax), %rax
	shrq	%cl, %rax
	testq	%rax, %rax
	je	.L1602
.L1597:
	call	_ZN2v88internal13MutableBigInt15TruncateToNBitsEPNS0_7IsolateEiNS0_6HandleINS0_6BigIntEEE
	jmp	.L1590
.L1603:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE17915:
	.size	_ZN2v88internal6BigInt7AsUintNEPNS0_7IsolateEmNS0_6HandleIS1_EE, .-_ZN2v88internal6BigInt7AsUintNEPNS0_7IsolateEmNS0_6HandleIS1_EE
	.section	.text._ZN2v88internal6BigInt9FromInt64EPNS0_7IsolateEl,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6BigInt9FromInt64EPNS0_7IsolateEl
	.type	_ZN2v88internal6BigInt9FromInt64EPNS0_7IsolateEl, @function
_ZN2v88internal6BigInt9FromInt64EPNS0_7IsolateEl:
.LFB17918:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	testq	%rsi, %rsi
	jne	.L1605
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZN2v88internal7Factory9NewBigIntEiNS0_14AllocationTypeE@PLT
	movq	%rax, %rbx
	movq	(%rax), %rax
	movl	$0, 7(%rax)
	movq	(%rbx), %rdi
	call	_ZN2v88internal13MutableBigInt12CanonicalizeES1_
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1605:
	.cfi_restore_state
	movq	%rsi, %r12
	xorl	%edx, %edx
	movl	$1, %esi
	call	_ZN2v88internal7Factory9NewBigIntEiNS0_14AllocationTypeE@PLT
	movq	(%rax), %rdx
	movq	%rax, %rbx
	movq	%r12, %rax
	shrq	$63, %rax
	orl	$2, %eax
	movl	%eax, 7(%rdx)
	testq	%r12, %r12
	js	.L1611
.L1608:
	movq	(%rbx), %rax
	movq	%r12, 15(%rax)
	movq	(%rbx), %rdi
	call	_ZN2v88internal13MutableBigInt12CanonicalizeES1_
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1611:
	.cfi_restore_state
	movabsq	$-9223372036854775808, %rax
	movq	%r12, %rsi
	negq	%rsi
	cmpq	%rax, %r12
	cmovne	%rsi, %r12
	jmp	.L1608
	.cfi_endproc
.LFE17918:
	.size	_ZN2v88internal6BigInt9FromInt64EPNS0_7IsolateEl, .-_ZN2v88internal6BigInt9FromInt64EPNS0_7IsolateEl
	.section	.text._ZN2v88internal6BigInt10FromUint64EPNS0_7IsolateEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6BigInt10FromUint64EPNS0_7IsolateEm
	.type	_ZN2v88internal6BigInt10FromUint64EPNS0_7IsolateEm, @function
_ZN2v88internal6BigInt10FromUint64EPNS0_7IsolateEm:
.LFB17919:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	testq	%rsi, %rsi
	jne	.L1613
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZN2v88internal7Factory9NewBigIntEiNS0_14AllocationTypeE@PLT
	movq	%rax, %rbx
	movq	(%rax), %rax
	movl	$0, 7(%rax)
	movq	(%rbx), %rdi
	call	_ZN2v88internal13MutableBigInt12CanonicalizeES1_
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1613:
	.cfi_restore_state
	movq	%rsi, %r12
	xorl	%edx, %edx
	movl	$1, %esi
	call	_ZN2v88internal7Factory9NewBigIntEiNS0_14AllocationTypeE@PLT
	movq	%rax, %rbx
	movq	(%rax), %rax
	movl	$2, 7(%rax)
	movq	(%rbx), %rax
	movq	%r12, 15(%rax)
	movq	(%rbx), %rdi
	call	_ZN2v88internal13MutableBigInt12CanonicalizeES1_
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17919:
	.size	_ZN2v88internal6BigInt10FromUint64EPNS0_7IsolateEm, .-_ZN2v88internal6BigInt10FromUint64EPNS0_7IsolateEm
	.section	.text._ZN2v88internal6BigInt11FromWords64EPNS0_7IsolateEiiPKm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6BigInt11FromWords64EPNS0_7IsolateEiiPKm
	.type	_ZN2v88internal6BigInt11FromWords64EPNS0_7IsolateEiiPKm, @function
_ZN2v88internal6BigInt11FromWords64EPNS0_7IsolateEiiPKm:
.LFB17920:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 15, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	cmpl	$16777216, %edx
	ja	.L1625
	movl	%edx, %r12d
	testl	%edx, %edx
	je	.L1626
	movl	%esi, %r15d
	movl	%edx, %esi
	movq	%rcx, %r13
	call	_ZN2v88internal13MutableBigInt3NewEPNS0_7IsolateEiNS0_14AllocationTypeE.constprop.0
	movq	%rax, %rbx
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.L1619
	movq	(%rbx), %rdx
	xorl	%ecx, %ecx
	movl	$16, %esi
	movl	7(%rdx), %eax
	andl	$-2, %eax
	testl	%r15d, %r15d
	setne	%cl
	orl	%ecx, %eax
	movl	%eax, 7(%rdx)
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1622:
	movq	-16(%r13,%rsi), %rcx
	movq	(%rbx), %rdx
	addl	$1, %eax
	addq	$8, %rsi
	movq	%rcx, -9(%rsi,%rdx)
	cmpl	%eax, %r12d
	jg	.L1622
	movq	(%rbx), %rdi
	call	_ZN2v88internal13MutableBigInt12CanonicalizeES1_
	addq	$16, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1626:
	.cfi_restore_state
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZN2v88internal7Factory9NewBigIntEiNS0_14AllocationTypeE@PLT
	movq	%rax, %rbx
	movq	(%rax), %rax
	movl	$0, 7(%rax)
	movq	(%rbx), %rdi
	call	_ZN2v88internal13MutableBigInt12CanonicalizeES1_
	movq	%rbx, %rax
.L1619:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1625:
	.cfi_restore_state
	cmpb	$0, _ZN2v88internal36FLAG_correctness_fuzzer_suppressionsE(%rip)
	jne	.L1627
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$183, %esi
	movq	%rdi, -40(%rbp)
	call	_ZN2v88internal7Factory13NewRangeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	movq	-40(%rbp), %rdi
	xorl	%edx, %edx
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1627:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE17920:
	.size	_ZN2v88internal6BigInt11FromWords64EPNS0_7IsolateEiiPKm, .-_ZN2v88internal6BigInt11FromWords64EPNS0_7IsolateEiiPKm
	.section	.text._ZN2v88internal6BigInt12Words64CountEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6BigInt12Words64CountEv
	.type	_ZN2v88internal6BigInt12Words64CountEv, @function
_ZN2v88internal6BigInt12Words64CountEv:
.LFB17921:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movl	7(%rax), %eax
	shrl	%eax
	andl	$1073741823, %eax
	ret
	.cfi_endproc
.LFE17921:
	.size	_ZN2v88internal6BigInt12Words64CountEv, .-_ZN2v88internal6BigInt12Words64CountEv
	.section	.text._ZN2v88internal6BigInt14ToWordsArray64EPiS2_Pm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6BigInt14ToWordsArray64EPiS2_Pm
	.type	_ZN2v88internal6BigInt14ToWordsArray64EPiS2_Pm, @function
_ZN2v88internal6BigInt14ToWordsArray64EPiS2_Pm:
.LFB17922:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movl	7(%rax), %eax
	andl	$1, %eax
	movl	%eax, (%rsi)
	movq	(%rdi), %rax
	movl	(%rdx), %esi
	movl	7(%rax), %eax
	shrl	%eax
	andl	$1073741823, %eax
	movl	%eax, (%rdx)
	testl	%esi, %esi
	jne	.L1637
.L1629:
	ret
	.p2align 4,,10
	.p2align 3
.L1637:
	movq	(%rdi), %rdx
	movl	7(%rdx), %eax
	shrl	%eax
	andl	$1073741823, %eax
	cmpl	%esi, %eax
	cmovg	%esi, %eax
	testl	%eax, %eax
	jle	.L1629
	subl	$1, %eax
	leaq	24(,%rax,8), %rsi
	movl	$16, %eax
	jmp	.L1631
	.p2align 4,,10
	.p2align 3
.L1638:
	movq	(%rdi), %rdx
.L1631:
	movq	-1(%rax,%rdx), %rdx
	movq	%rdx, -16(%rcx,%rax)
	addq	$8, %rax
	cmpq	%rax, %rsi
	jne	.L1638
	ret
	.cfi_endproc
.LFE17922:
	.size	_ZN2v88internal6BigInt14ToWordsArray64EPiS2_Pm, .-_ZN2v88internal6BigInt14ToWordsArray64EPiS2_Pm
	.section	.text._ZN2v88internal13MutableBigInt10GetRawBitsENS0_10BigIntBaseEPb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13MutableBigInt10GetRawBitsENS0_10BigIntBaseEPb
	.type	_ZN2v88internal13MutableBigInt10GetRawBitsENS0_10BigIntBaseEPb, @function
_ZN2v88internal13MutableBigInt10GetRawBitsENS0_10BigIntBaseEPb:
.LFB17923:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L1640
	movb	$1, (%rsi)
	movl	7(%rdi), %eax
	testl	$2147483646, %eax
	jne	.L1641
.L1644:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1641:
	movl	7(%rdi), %eax
	shrl	%eax
	testl	$1073741822, %eax
	je	.L1643
	movb	$0, (%rsi)
.L1643:
	movq	15(%rdi), %rax
	movl	7(%rdi), %ecx
	movq	%rax, %rdx
	negq	%rdx
	andl	$1, %ecx
	cmovne	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1640:
	movl	7(%rdi), %eax
	testl	$2147483646, %eax
	je	.L1644
	movl	7(%rdi), %eax
	jmp	.L1643
	.cfi_endproc
.LFE17923:
	.size	_ZN2v88internal13MutableBigInt10GetRawBitsENS0_10BigIntBaseEPb, .-_ZN2v88internal13MutableBigInt10GetRawBitsENS0_10BigIntBaseEPb
	.section	.text._ZN2v88internal6BigInt7AsInt64EPb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6BigInt7AsInt64EPb
	.type	_ZN2v88internal6BigInt7AsInt64EPb, @function
_ZN2v88internal6BigInt7AsInt64EPb:
.LFB17924:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rcx
	testq	%rsi, %rsi
	je	.L1656
	movb	$1, (%rsi)
	movl	7(%rcx), %edx
	xorl	%eax, %eax
	shrl	%edx
	andl	$1073741823, %edx
	jne	.L1670
	movq	(%rdi), %rcx
	movl	7(%rcx), %ecx
	andl	$1, %ecx
	cmpl	%edx, %ecx
	je	.L1655
.L1672:
	movb	$0, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L1656:
	movl	7(%rcx), %edx
	xorl	%eax, %eax
	andl	$2147483646, %edx
	jne	.L1671
.L1655:
	ret
	.p2align 4,,10
	.p2align 3
.L1670:
	movl	7(%rcx), %eax
	shrl	%eax
	testl	$1073741822, %eax
	je	.L1658
	movb	$0, (%rsi)
	movq	15(%rcx), %rax
	movl	7(%rcx), %edx
	andl	$1, %edx
	je	.L1660
	negq	%rax
.L1660:
	movq	(%rdi), %rcx
	movq	%rax, %rdx
	shrq	$63, %rdx
	movl	7(%rcx), %ecx
	andl	$1, %ecx
	cmpl	%edx, %ecx
	jne	.L1672
	jmp	.L1655
	.p2align 4,,10
	.p2align 3
.L1671:
	movl	7(%rcx), %eax
	movq	15(%rcx), %rdx
	movl	7(%rcx), %ecx
	movq	%rdx, %rax
	negq	%rax
	andl	$1, %ecx
	cmove	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1658:
	movq	15(%rcx), %rdx
	movl	7(%rcx), %ecx
	movq	%rdx, %rax
	negq	%rax
	andl	$1, %ecx
	cmove	%rdx, %rax
	jmp	.L1660
	.cfi_endproc
.LFE17924:
	.size	_ZN2v88internal6BigInt7AsInt64EPb, .-_ZN2v88internal6BigInt7AsInt64EPb
	.section	.text._ZN2v88internal6BigInt8AsUint64EPb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6BigInt8AsUint64EPb
	.type	_ZN2v88internal6BigInt8AsUint64EPb, @function
_ZN2v88internal6BigInt8AsUint64EPb:
.LFB17925:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	testq	%rsi, %rsi
	je	.L1674
	movb	$1, (%rsi)
	movl	7(%rdx), %ecx
	xorl	%eax, %eax
	andl	$2147483646, %ecx
	jne	.L1690
	movq	(%rdi), %rdx
	movl	7(%rdx), %edx
	andl	$1, %edx
	je	.L1673
.L1692:
	movb	$0, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L1674:
	movl	7(%rdx), %ecx
	xorl	%eax, %eax
	andl	$2147483646, %ecx
	jne	.L1691
.L1673:
	ret
	.p2align 4,,10
	.p2align 3
.L1690:
	movl	7(%rdx), %eax
	shrl	%eax
	testl	$1073741822, %eax
	je	.L1689
	movb	$0, (%rsi)
.L1689:
	movq	15(%rdx), %rax
	movl	7(%rdx), %ecx
	movq	%rax, %rdx
	negq	%rdx
	andl	$1, %ecx
	cmovne	%rdx, %rax
	movq	(%rdi), %rdx
	movl	7(%rdx), %edx
	andl	$1, %edx
	je	.L1673
	jmp	.L1692
	.p2align 4,,10
	.p2align 3
.L1691:
	movl	7(%rdx), %eax
	movq	15(%rdx), %rax
	movl	7(%rdx), %ecx
	movq	%rax, %rdx
	negq	%rdx
	andl	$1, %ecx
	cmovne	%rdx, %rax
	ret
	.cfi_endproc
.LFE17925:
	.size	_ZN2v88internal6BigInt8AsUint64EPb, .-_ZN2v88internal6BigInt8AsUint64EPb
	.section	.text._ZN2v88internal13MutableBigInt9digit_powEmm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13MutableBigInt9digit_powEmm
	.type	_ZN2v88internal13MutableBigInt9digit_powEmm, @function
_ZN2v88internal13MutableBigInt9digit_powEmm:
.LFB17930:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	testq	%rsi, %rsi
	je	.L1697
	.p2align 4,,10
	.p2align 3
.L1696:
	testb	$1, %sil
	je	.L1695
	imulq	%rdi, %rax
.L1695:
	imulq	%rdi, %rdi
	shrq	%rsi
	jne	.L1696
	ret
	.p2align 4,,10
	.p2align 3
.L1697:
	ret
	.cfi_endproc
.LFE17930:
	.size	_ZN2v88internal13MutableBigInt9digit_powEmm, .-_ZN2v88internal13MutableBigInt9digit_powEmm
	.section	.text._ZN2v88internal13MutableBigInt11set_64_bitsEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13MutableBigInt11set_64_bitsEm
	.type	_ZN2v88internal13MutableBigInt11set_64_bitsEm, @function
_ZN2v88internal13MutableBigInt11set_64_bitsEm:
.LFB17931:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	%rsi, 15(%rax)
	ret
	.cfi_endproc
.LFE17931:
	.size	_ZN2v88internal13MutableBigInt11set_64_bitsEm, .-_ZN2v88internal13MutableBigInt11set_64_bitsEm
	.section	.text._ZN2v88internal40MutableBigInt_AbsoluteAddAndCanonicalizeEmmm,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal40MutableBigInt_AbsoluteAddAndCanonicalizeEmmm
	.type	_ZN2v88internal40MutableBigInt_AbsoluteAddAndCanonicalizeEmmm, @function
_ZN2v88internal40MutableBigInt_AbsoluteAddAndCanonicalizeEmmm:
.LFB17932:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal13MutableBigInt11AbsoluteAddES1_NS0_6BigIntES2_
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal13MutableBigInt12CanonicalizeES1_
	.cfi_endproc
.LFE17932:
	.size	_ZN2v88internal40MutableBigInt_AbsoluteAddAndCanonicalizeEmmm, .-_ZN2v88internal40MutableBigInt_AbsoluteAddAndCanonicalizeEmmm
	.section	.text._ZN2v88internal29MutableBigInt_AbsoluteCompareEmm,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal29MutableBigInt_AbsoluteCompareEmm
	.type	_ZN2v88internal29MutableBigInt_AbsoluteCompareEmm, @function
_ZN2v88internal29MutableBigInt_AbsoluteCompareEmm:
.LFB17933:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal13MutableBigInt15AbsoluteCompareENS0_10BigIntBaseES2_
	.cfi_endproc
.LFE17933:
	.size	_ZN2v88internal29MutableBigInt_AbsoluteCompareEmm, .-_ZN2v88internal29MutableBigInt_AbsoluteCompareEmm
	.section	.text._ZN2v88internal40MutableBigInt_AbsoluteSubAndCanonicalizeEmmm,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal40MutableBigInt_AbsoluteSubAndCanonicalizeEmmm
	.type	_ZN2v88internal40MutableBigInt_AbsoluteSubAndCanonicalizeEmmm, @function
_ZN2v88internal40MutableBigInt_AbsoluteSubAndCanonicalizeEmmm:
.LFB17934:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal13MutableBigInt11AbsoluteSubES1_NS0_6BigIntES2_
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal13MutableBigInt12CanonicalizeES1_
	.cfi_endproc
.LFE17934:
	.size	_ZN2v88internal40MutableBigInt_AbsoluteSubAndCanonicalizeEmmm, .-_ZN2v88internal40MutableBigInt_AbsoluteSubAndCanonicalizeEmmm
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal13MutableBigInt3NewEPNS0_7IsolateEiNS0_14AllocationTypeE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal13MutableBigInt3NewEPNS0_7IsolateEiNS0_14AllocationTypeE, @function
_GLOBAL__sub_I__ZN2v88internal13MutableBigInt3NewEPNS0_7IsolateEiNS0_14AllocationTypeE:
.LFB21781:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE21781:
	.size	_GLOBAL__sub_I__ZN2v88internal13MutableBigInt3NewEPNS0_7IsolateEiNS0_14AllocationTypeE, .-_GLOBAL__sub_I__ZN2v88internal13MutableBigInt3NewEPNS0_7IsolateEiNS0_14AllocationTypeE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal13MutableBigInt3NewEPNS0_7IsolateEiNS0_14AllocationTypeE
	.section	.rodata._ZN2v88internalL16kConversionCharsE,"a"
	.align 32
	.type	_ZN2v88internalL16kConversionCharsE, @object
	.size	_ZN2v88internalL16kConversionCharsE, 37
_ZN2v88internalL16kConversionCharsE:
	.string	"0123456789abcdefghijklmnopqrstuvwxyz"
	.section	.rodata._ZN2v88internalL15kMaxBitsPerCharE,"a"
	.align 32
	.type	_ZN2v88internalL15kMaxBitsPerCharE, @object
	.size	_ZN2v88internalL15kMaxBitsPerCharE, 37
_ZN2v88internalL15kMaxBitsPerCharE:
	.string	""
	.string	""
	.ascii	" 3@KSZ`fkoswz~\200\203\206\210\213\215\217\221\223\225\227\231"
	.ascii	"\232\234\236\237\240\242\243\245\246"
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC4:
	.long	0
	.long	2146435072
	.align 8
.LC5:
	.long	0
	.long	-1048576
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC6:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC7:
	.long	4294967295
	.long	2146435071
	.align 8
.LC8:
	.long	0
	.long	1127219200
	.align 8
.LC9:
	.long	0
	.long	1072693248
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
