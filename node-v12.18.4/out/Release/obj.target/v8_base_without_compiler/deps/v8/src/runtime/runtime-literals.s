	.file	"runtime-literals.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB5053:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE5053:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB5054:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5054:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB5056:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5056:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.rodata._ZN2v88internal12_GLOBAL__N_119JSObjectWalkVisitorINS1_24DeprecationUpdateContextEE13StructureWalkENS0_6HandleINS0_8JSObjectEEE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.rodata._ZN2v88internal12_GLOBAL__N_119JSObjectWalkVisitorINS1_24DeprecationUpdateContextEE13StructureWalkENS0_6HandleINS0_8JSObjectEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"(!IsSmi() && (*layout_word_index < length())) || (IsSmi() && (*layout_word_index < 1))"
	.section	.rodata._ZN2v88internal12_GLOBAL__N_119JSObjectWalkVisitorINS1_24DeprecationUpdateContextEE13StructureWalkENS0_6HandleINS0_8JSObjectEEE.str1.1
.LC2:
	.string	"Check failed: %s."
.LC3:
	.string	"unimplemented code"
	.section	.text._ZN2v88internal12_GLOBAL__N_119JSObjectWalkVisitorINS1_24DeprecationUpdateContextEE13StructureWalkENS0_6HandleINS0_8JSObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_119JSObjectWalkVisitorINS1_24DeprecationUpdateContextEE13StructureWalkENS0_6HandleINS0_8JSObjectEEE, @function
_ZN2v88internal12_GLOBAL__N_119JSObjectWalkVisitorINS1_24DeprecationUpdateContextEE13StructureWalkENS0_6HandleINS0_8JSObjectEEE:
.LFB21870:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	(%rdi), %rax
	cmpl	$1, 8(%rdi)
	movq	(%rax), %r13
	je	.L6
	movq	%rdi, %r15
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	37528(%r13), %rax
	jb	.L137
	movq	(%rbx), %rax
	movq	-1(%rax), %rax
	movl	15(%rax), %eax
	testl	$16777216, %eax
	jne	.L138
.L96:
	addl	$1, 41104(%r13)
	movq	41088(%r13), %rax
	movq	(%rbx), %r11
	movq	41096(%r13), %r12
	movq	%rax, -56(%rbp)
	movq	-1(%r11), %rax
	cmpw	$1061, 11(%rax)
	je	.L58
	movq	-1(%r11), %rax
	movl	15(%rax), %eax
	testl	$2097152, %eax
	jne	.L10
	movq	-1(%r11), %rax
	movq	41112(%r13), %rdi
	movq	39(%rax), %rsi
	testq	%rdi, %rdi
	je	.L11
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r8
.L12:
	movq	(%rbx), %r11
	movq	-1(%r11), %rax
	movl	15(%rax), %eax
	shrl	$10, %eax
	andl	$1023, %eax
	je	.L14
	subl	$1, %eax
	movq	%r12, -72(%rbp)
	movq	%r13, %r12
	movl	$31, %r14d
	leaq	(%rax,%rax,2), %rax
	movq	%r15, -64(%rbp)
	movq	%rbx, %r13
	movq	%r8, %rbx
	leaq	55(,%rax,8), %r9
	movq	%r9, %r15
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L97:
	movl	$32768, %esi
.L17:
	movzbl	%al, %eax
	movslq	%ecx, %rcx
	movslq	%r9d, %r9
	movslq	%edi, %rdi
	salq	$17, %rcx
	salq	$14, %rax
	orq	%rcx, %rax
	salq	$27, %rdi
	orq	%r9, %rax
	orq	%rdi, %rax
	movq	-1(%r11), %rdi
	orq	%rax, %rsi
	shrq	$30, %rax
	movl	%esi, %ecx
	andl	$15, %eax
	sarl	$3, %ecx
	andl	$2047, %ecx
	subl	%eax, %ecx
	testl	$16384, %esi
	jne	.L20
	movq	%r11, %rax
	movq	7(%r11), %rsi
	andq	$-262144, %rax
	movq	24(%rax), %rax
	subq	$37592, %rax
	testb	$1, %sil
	je	.L34
	cmpq	288(%rax), %rsi
	je	.L34
	.p2align 4,,10
	.p2align 3
.L22:
	leal	16(,%rcx,8), %eax
	cltq
	movq	-1(%rsi,%rax), %rsi
	testb	$1, %sil
	jne	.L139
	.p2align 4,,10
	.p2align 3
.L42:
	addq	$24, %r14
	cmpq	%r14, %r15
	je	.L140
.L33:
	movq	(%rbx), %rax
	movq	(%rax,%r14), %rsi
	movq	-1(%r11), %r9
	movq	%rsi, %rax
	movzbl	7(%r9), %ecx
	movzbl	8(%r9), %edi
	shrq	$51, %rsi
	shrq	$38, %rax
	andl	$1023, %esi
	movq	%rax, %r10
	subl	%edi, %ecx
	andl	$7, %r10d
	cmpl	%ecx, %esi
	setl	%al
	jl	.L141
	subl	%ecx, %esi
	movl	$16, %edi
	leal	16(,%rsi,8), %r9d
.L16:
	cmpl	$2, %r10d
	je	.L97
	cmpb	$2, %r10b
	jg	.L18
	je	.L19
.L98:
	xorl	%esi, %esi
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L137:
	movq	%r13, %rdi
	call	_ZN2v88internal7Isolate13StackOverflowEv@PLT
	xorl	%eax, %eax
.L106:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	movq	(%rsi), %rax
	movq	-1(%rax), %rax
	movl	15(%rax), %eax
	testl	$16777216, %eax
	jne	.L142
.L95:
	addq	$56, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L143:
	.cfi_restore_state
	movq	%r12, %r13
	movq	-72(%rbp), %rbx
	movq	-64(%rbp), %r12
.L136:
	movq	(%rbx), %r11
.L14:
	movq	15(%r11), %rax
	movl	11(%rax), %esi
	testl	%esi, %esi
	je	.L59
.L58:
	movq	-1(%r11), %rax
	movzbl	14(%rax), %edx
	movl	%edx, %eax
	shrl	$3, %eax
	cmpl	$223, %edx
	ja	.L59
	leaq	.L60(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal12_GLOBAL__N_119JSObjectWalkVisitorINS1_24DeprecationUpdateContextEE13StructureWalkENS0_6HandleINS0_8JSObjectEEE,"a",@progbits
	.align 4
	.align 4
.L60:
	.long	.L59-.L60
	.long	.L59-.L60
	.long	.L63-.L60
	.long	.L63-.L60
	.long	.L59-.L60
	.long	.L59-.L60
	.long	.L63-.L60
	.long	.L63-.L60
	.long	.L63-.L60
	.long	.L63-.L60
	.long	.L63-.L60
	.long	.L63-.L60
	.long	.L62-.L60
	.long	.L61-.L60
	.long	.L61-.L60
	.long	.L19-.L60
	.long	.L19-.L60
	.long	.L19-.L60
	.long	.L19-.L60
	.long	.L19-.L60
	.long	.L19-.L60
	.long	.L19-.L60
	.long	.L19-.L60
	.long	.L19-.L60
	.long	.L19-.L60
	.long	.L19-.L60
	.long	.L19-.L60
	.long	.L19-.L60
	.section	.text._ZN2v88internal12_GLOBAL__N_119JSObjectWalkVisitorINS1_24DeprecationUpdateContextEE13StructureWalkENS0_6HandleINS0_8JSObjectEEE
	.p2align 4,,10
	.p2align 3
.L145:
	movq	%r13, %rax
	movq	-64(%rbp), %rbx
	movq	%r12, %r13
	movq	%rax, %r12
	.p2align 4,,10
	.p2align 3
.L59:
	movq	%rbx, %rax
.L41:
	movq	-56(%rbp), %rdx
	subl	$1, 41104(%r13)
	movq	%rdx, 41088(%r13)
	cmpq	41096(%r13), %r12
	je	.L106
	movq	%r12, 41096(%r13)
	movq	%r13, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	movq	7(%r11), %rsi
	testb	$1, %sil
	jne	.L44
	andq	$-262144, %r11
	movq	24(%r11), %rax
	movq	-36536(%rax), %rsi
.L44:
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L45
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rdx
.L46:
	movslq	35(%rsi), %rax
	testq	%rax, %rax
	jle	.L136
	subl	$1, %eax
	movq	%rbx, -72(%rbp)
	movq	%rdx, %rbx
	movl	$64, %r14d
	leaq	(%rax,%rax,2), %rax
	movq	%r12, -64(%rbp)
	movq	%r13, %r12
	leaq	88(,%rax,8), %rcx
	movq	%rcx, %r13
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L56:
	addq	$24, %r14
	cmpq	%r14, %r13
	je	.L143
	movq	(%rbx), %rsi
.L57:
	movq	-1(%rsi,%r14), %rsi
	testb	$1, %sil
	je	.L56
	movq	-1(%rsi), %rax
	leaq	-1(%rsi), %r9
	cmpw	$1024, 11(%rax)
	jbe	.L56
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L144
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r8
	movq	(%rax), %rax
	leaq	-1(%rax), %r9
.L52:
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	(%r9), %rax
	call	_ZN2v88internal12_GLOBAL__N_119JSObjectWalkVisitorINS1_24DeprecationUpdateContextEE13StructureWalkENS0_6HandleINS0_8JSObjectEEE
	testq	%rax, %rax
	jne	.L56
.L135:
	movq	%r12, %r13
	xorl	%eax, %eax
	movq	-64(%rbp), %r12
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L63:
	movq	41112(%r13), %rdi
	movq	15(%r11), %rsi
	testq	%rdi, %rdi
	je	.L64
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rdx
.L65:
	movq	-1(%rsi), %rax
	cmpq	%rax, 160(%r13)
	je	.L59
	movq	(%rdx), %rax
	movl	11(%rax), %ecx
	testl	%ecx, %ecx
	jle	.L59
	movq	%rbx, -64(%rbp)
	movq	%rdx, %rbx
	movq	%r12, %rdx
	xorl	%r14d, %r14d
	movq	%r13, %r12
	movq	%rdx, %r13
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L78:
	addq	$1, %r14
	cmpl	%r14d, 11(%rax)
	jle	.L145
.L72:
	movq	15(%rax,%r14,8), %rsi
	testb	$1, %sil
	je	.L78
	movq	-1(%rsi), %rdi
	leaq	-1(%rsi), %rcx
	cmpw	$1024, 11(%rdi)
	jbe	.L78
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L146
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r8
	movq	(%rax), %rax
	leaq	-1(%rax), %rcx
.L73:
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	(%rcx), %rax
	call	_ZN2v88internal12_GLOBAL__N_119JSObjectWalkVisitorINS1_24DeprecationUpdateContextEE13StructureWalkENS0_6HandleINS0_8JSObjectEEE
	testq	%rax, %rax
	je	.L132
	movq	(%rbx), %rax
	jmp	.L78
.L62:
	movq	41112(%r13), %rdi
	movq	15(%r11), %rsi
	testq	%rdi, %rdi
	je	.L79
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rdx
.L80:
	movslq	35(%rsi), %rax
	testq	%rax, %rax
	jle	.L59
	subl	$1, %eax
	movq	%rbx, -72(%rbp)
	movq	%rdx, %rbx
	movl	$56, %r14d
	leaq	(%rax,%rax,2), %rax
	movq	%r12, -64(%rbp)
	movq	%r13, %r12
	leaq	80(,%rax,8), %rcx
	movq	%rcx, %r13
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L90:
	addq	$24, %r14
	cmpq	%r13, %r14
	je	.L147
	movq	(%rbx), %rsi
.L91:
	movq	-1(%r14,%rsi), %rsi
	testb	$1, %sil
	je	.L90
	movq	-1(%rsi), %rax
	leaq	-1(%rsi), %r9
	cmpw	$1024, 11(%rax)
	jbe	.L90
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L148
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r8
	movq	(%rax), %rax
	leaq	-1(%rax), %r9
.L86:
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	(%r9), %rax
	call	_ZN2v88internal12_GLOBAL__N_119JSObjectWalkVisitorINS1_24DeprecationUpdateContextEE13StructureWalkENS0_6HandleINS0_8JSObjectEEE
	testq	%rax, %rax
	jne	.L90
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L18:
	subl	$3, %r10d
	cmpb	$1, %r10b
	jbe	.L98
	.p2align 4,,10
	.p2align 3
.L19:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L20:
	movq	47(%rdi), %rax
	testq	%rax, %rax
	je	.L32
	movq	%rax, %r9
	movl	$32, %edi
	notq	%r9
	movl	%r9d, %r10d
	andl	$1, %r10d
	jne	.L24
	movslq	11(%rax), %rdi
	sall	$3, %edi
.L24:
	cmpl	%edi, %ecx
	jnb	.L32
	testl	%ecx, %ecx
	leal	31(%rcx), %edi
	cmovns	%ecx, %edi
	sarl	$5, %edi
	andl	$1, %r9d
	jne	.L25
	cmpl	%edi, 11(%rax)
	jle	.L25
	movl	%ecx, %r9d
	sarl	$31, %r9d
	shrl	$27, %r9d
	addl	%r9d, %ecx
	andl	$31, %ecx
	subl	%r9d, %ecx
	movl	$1, %r9d
	sall	%cl, %r9d
	movl	%r9d, %ecx
	testb	%r10b, %r10b
	je	.L149
.L29:
	sarq	$32, %rax
	testl	%eax, %ecx
	sete	%al
.L31:
	testb	%al, %al
	jne	.L32
.L134:
	movq	0(%r13), %r11
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L32:
	movq	0(%r13), %r11
	andl	$16376, %esi
	movq	-1(%r11,%rsi), %rsi
	testb	$1, %sil
	je	.L42
.L139:
	movq	-1(%rsi), %rax
	leaq	-1(%rsi), %rcx
	cmpw	$1024, 11(%rax)
	jbe	.L42
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L36
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r9
	movq	(%rax), %rax
	leaq	-1(%rax), %rcx
.L37:
	movq	(%rcx), %rax
	movq	-64(%rbp), %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal12_GLOBAL__N_119JSObjectWalkVisitorINS1_24DeprecationUpdateContextEE13StructureWalkENS0_6HandleINS0_8JSObjectEEE
	testq	%rax, %rax
	jne	.L134
	movq	%r12, %r13
	xorl	%eax, %eax
	movq	-72(%rbp), %r12
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L141:
	movzbl	8(%r9), %edi
	movzbl	8(%r9), %r9d
	addl	%r9d, %esi
	sall	$3, %edi
	leal	0(,%rsi,8), %r9d
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L34:
	movq	968(%rax), %rsi
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L142:
	movq	%r13, %rdi
	call	_ZN2v88internal8JSObject15MigrateInstanceEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L144:
	movq	41088(%r12), %r8
	cmpq	41096(%r12), %r8
	je	.L150
.L53:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r8)
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L149:
	sall	$2, %edi
	movslq	%edi, %rdi
	movl	15(%rax,%rdi), %eax
	testl	%eax, %r9d
	sete	%al
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L138:
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8JSObject15MigrateInstanceEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L140:
	movq	%r13, %rbx
	movq	-64(%rbp), %r15
	movq	%r12, %r13
	movq	-72(%rbp), %r12
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L25:
	cmpl	$31, %ecx
	jg	.L27
	testb	%r10b, %r10b
	je	.L27
	movl	$1, %edi
	sall	%cl, %edi
	movl	%edi, %ecx
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L36:
	movq	41088(%r12), %r9
	cmpq	41096(%r12), %r9
	je	.L151
.L38:
	leaq	8(%r9), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r9)
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L11:
	movq	-56(%rbp), %rax
	movq	%rax, %r8
	cmpq	%rax, 41096(%r13)
	je	.L152
.L13:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r8)
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L45:
	movq	41088(%r13), %rdx
	cmpq	41096(%r13), %rdx
	je	.L153
.L47:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%rdx)
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L79:
	movq	41088(%r13), %rdx
	cmpq	41096(%r13), %rdx
	je	.L154
.L81:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%rdx)
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L64:
	movq	41088(%r13), %rdx
	cmpq	41096(%r13), %rdx
	je	.L155
.L66:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%rdx)
	jmp	.L65
.L61:
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L151:
	movq	%r12, %rdi
	movq	%rcx, -88(%rbp)
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rcx
	movq	-80(%rbp), %rsi
	movq	%rax, %r9
	jmp	.L38
.L152:
	movq	%r13, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-64(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L13
.L153:
	movq	%r13, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-64(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L148:
	movq	41088(%r12), %r8
	cmpq	41096(%r12), %r8
	je	.L156
.L87:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r8)
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L147:
	movq	%r12, %r13
	movq	-72(%rbp), %rbx
	movq	-64(%rbp), %r12
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L146:
	movq	41088(%r12), %r8
	cmpq	41096(%r12), %r8
	je	.L157
.L74:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r8)
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L150:
	movq	%r12, %rdi
	movq	%r9, -88(%rbp)
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %r9
	movq	-80(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L53
.L132:
	movq	%r13, %rax
	movq	%r12, %r13
	movq	%rax, %r12
	xorl	%eax, %eax
	jmp	.L41
.L27:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L156:
	movq	%r12, %rdi
	movq	%r9, -88(%rbp)
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %r9
	movq	-80(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L87
.L157:
	movq	%r12, %rdi
	movq	%rcx, -80(%rbp)
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %rcx
	movq	-72(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L74
.L155:
	movq	%r13, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-64(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L66
.L154:
	movq	%r13, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-64(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L81
	.cfi_endproc
.LFE21870:
	.size	_ZN2v88internal12_GLOBAL__N_119JSObjectWalkVisitorINS1_24DeprecationUpdateContextEE13StructureWalkENS0_6HandleINS0_8JSObjectEEE, .-_ZN2v88internal12_GLOBAL__N_119JSObjectWalkVisitorINS1_24DeprecationUpdateContextEE13StructureWalkENS0_6HandleINS0_8JSObjectEEE
	.section	.rodata._ZN2v88internal12_GLOBAL__N_119JSObjectWalkVisitorINS1_29AllocationSiteCreationContextEE22VisitElementOrPropertyENS0_6HandleINS0_8JSObjectEEES7_.str1.1,"aMS",@progbits,1
.LC4:
	.string	"Fat"
	.section	.rodata._ZN2v88internal12_GLOBAL__N_119JSObjectWalkVisitorINS1_29AllocationSiteCreationContextEE22VisitElementOrPropertyENS0_6HandleINS0_8JSObjectEEES7_.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"*** Creating top level %s AllocationSite %p\n"
	.section	.rodata._ZN2v88internal12_GLOBAL__N_119JSObjectWalkVisitorINS1_29AllocationSiteCreationContextEE22VisitElementOrPropertyENS0_6HandleINS0_8JSObjectEEES7_.str1.1
.LC6:
	.string	"Slim"
	.section	.rodata._ZN2v88internal12_GLOBAL__N_119JSObjectWalkVisitorINS1_29AllocationSiteCreationContextEE22VisitElementOrPropertyENS0_6HandleINS0_8JSObjectEEES7_.str1.8
	.align 8
.LC7:
	.string	"*** Creating nested %s AllocationSite (top, current, new) (%p, %p, %p)\n"
	.align 8
.LC8:
	.string	"*** Setting AllocationSite (%p, %p) transition_info %p\n"
	.align 8
.LC9:
	.string	"*** Setting AllocationSite %p transition_info %p\n"
	.section	.text._ZN2v88internal12_GLOBAL__N_119JSObjectWalkVisitorINS1_29AllocationSiteCreationContextEE22VisitElementOrPropertyENS0_6HandleINS0_8JSObjectEEES7_,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_119JSObjectWalkVisitorINS1_29AllocationSiteCreationContextEE22VisitElementOrPropertyENS0_6HandleINS0_8JSObjectEEES7_, @function
_ZN2v88internal12_GLOBAL__N_119JSObjectWalkVisitorINS1_29AllocationSiteCreationContextEE22VisitElementOrPropertyENS0_6HandleINS0_8JSObjectEEES7_:
.LFB23102:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	(%rdx), %rax
	movq	-1(%rax), %rax
	cmpw	$1061, 11(%rax)
	je	.L159
	addq	$40, %rsp
	movq	%rdx, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12_GLOBAL__N_119JSObjectWalkVisitorINS1_29AllocationSiteCreationContextEE13StructureWalkENS0_6HandleINS0_8JSObjectEEE
	.p2align 4,,10
	.p2align 3
.L159:
	.cfi_restore_state
	movq	(%rdi), %r14
	cmpq	$0, 8(%r14)
	movq	(%r14), %rdi
	je	.L190
	xorl	%esi, %esi
	call	_ZN2v88internal7Factory17NewAllocationSiteEb@PLT
	cmpb	$0, _ZN2v88internal36FLAG_trace_creation_allocation_sitesE(%rip)
	movq	%rax, %rbx
	jne	.L191
.L166:
	movq	16(%r14), %rax
	movq	(%rbx), %r15
	movq	(%rax), %rdi
	movq	%r15, 15(%rdi)
	leaq	15(%rdi), %rsi
	testb	$1, %r15b
	je	.L176
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	je	.L168
	movq	%r15, %rdx
	movq	%rsi, -72(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-72(%rbp), %rsi
	movq	-64(%rbp), %rdi
	movq	8(%rcx), %rax
.L168:
	testb	$24, %al
	je	.L176
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L192
	.p2align 4,,10
	.p2align 3
.L176:
	movq	(%rbx), %rdx
	movq	16(%r14), %rax
	movq	%rdx, (%rax)
.L165:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal12_GLOBAL__N_119JSObjectWalkVisitorINS1_29AllocationSiteCreationContextEE13StructureWalkENS0_6HandleINS0_8JSObjectEEE
	movq	0(%r13), %rcx
	movq	(%rbx), %r15
	movq	(%r12), %r13
	movq	%rax, %r14
	leaq	7(%r15), %rsi
	movq	%r13, 7(%r15)
	testb	$1, %r13b
	je	.L175
	movq	%r13, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rax
	movq	%r8, -56(%rbp)
	testl	$262144, %eax
	jne	.L193
.L171:
	testb	$24, %al
	je	.L175
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L194
	.p2align 4,,10
	.p2align 3
.L175:
	cmpb	$0, _ZN2v88internal36FLAG_trace_creation_allocation_sitesE(%rip)
	je	.L173
	movq	8(%rcx), %rax
	movq	(%rbx), %rsi
	cmpq	%rbx, %rax
	je	.L174
	movq	(%rax), %r8
	cmpq	%r8, %rsi
	jne	.L195
.L174:
	movq	(%r12), %rdx
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
.L173:
	addq	$40, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L191:
	.cfi_restore_state
	movq	16(%r14), %rdx
	movq	8(%r14), %rax
	leaq	.LC6(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movq	(%rbx), %r8
	movq	(%rdx), %rcx
	movq	(%rax), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L193:
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rcx, -72(%rbp)
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %r8
	movq	-72(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	8(%r8), %rax
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L195:
	movq	(%r12), %rcx
	movq	%rsi, %rdx
	leaq	.LC8(%rip), %rdi
	xorl	%eax, %eax
	movq	%r8, %rsi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L190:
	movl	$1, %esi
	call	_ZN2v88internal7Factory17NewAllocationSiteEb@PLT
	movq	(%r14), %rbx
	movq	%rax, 8(%r14)
	movq	(%rax), %r15
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L196
.L161:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%r15, (%rax)
	movq	(%r14), %r15
	movq	%rax, 16(%r14)
	movq	8(%r14), %rax
	movq	41112(%r15), %rdi
	movq	(%rax), %rsi
	testq	%rdi, %rdi
	je	.L162
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rbx
.L163:
	cmpb	$0, _ZN2v88internal36FLAG_trace_creation_allocation_sitesE(%rip)
	je	.L165
	movq	(%rbx), %rdx
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L192:
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L194:
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L162:
	movq	41088(%r15), %rbx
	cmpq	41096(%r15), %rbx
	je	.L197
.L164:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rbx)
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L196:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L161
.L197:
	movq	%r15, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L164
	.cfi_endproc
.LFE23102:
	.size	_ZN2v88internal12_GLOBAL__N_119JSObjectWalkVisitorINS1_29AllocationSiteCreationContextEE22VisitElementOrPropertyENS0_6HandleINS0_8JSObjectEEES7_, .-_ZN2v88internal12_GLOBAL__N_119JSObjectWalkVisitorINS1_29AllocationSiteCreationContextEE22VisitElementOrPropertyENS0_6HandleINS0_8JSObjectEEES7_
	.section	.text._ZN2v88internal12_GLOBAL__N_119JSObjectWalkVisitorINS1_29AllocationSiteCreationContextEE13StructureWalkENS0_6HandleINS0_8JSObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_119JSObjectWalkVisitorINS1_29AllocationSiteCreationContextEE13StructureWalkENS0_6HandleINS0_8JSObjectEEE, @function
_ZN2v88internal12_GLOBAL__N_119JSObjectWalkVisitorINS1_29AllocationSiteCreationContextEE13StructureWalkENS0_6HandleINS0_8JSObjectEEE:
.LFB21880:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	(%rdi), %rax
	cmpl	$1, 8(%rdi)
	movq	(%rax), %r12
	je	.L199
	movq	%rdi, %r15
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	37528(%r12), %rax
	jb	.L318
	movq	(%rbx), %rax
	movq	-1(%rax), %rax
	movl	15(%rax), %eax
	testl	$16777216, %eax
	jne	.L319
.L283:
	movq	41088(%r12), %rax
	movq	41096(%r12), %r13
	addl	$1, 41104(%r12)
	movq	(%rbx), %r11
	movq	%rax, -56(%rbp)
	movq	-1(%r11), %rax
	cmpw	$1061, 11(%rax)
	je	.L249
	movq	-1(%r11), %rax
	movl	15(%rax), %eax
	testl	$2097152, %eax
	jne	.L203
	movq	-1(%r11), %rax
	movq	41112(%r12), %rdi
	movq	39(%rax), %rsi
	testq	%rdi, %rdi
	je	.L204
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r8
.L205:
	movq	(%rbx), %r11
	movq	-1(%r11), %rax
	movl	15(%rax), %eax
	shrl	$10, %eax
	andl	$1023, %eax
	je	.L207
	subl	$1, %eax
	movq	%r13, -72(%rbp)
	movq	%r12, %r13
	movl	$31, %r14d
	leaq	(%rax,%rax,2), %rax
	movq	%r15, -64(%rbp)
	movq	%rbx, %r12
	movq	%r8, %rbx
	leaq	55(,%rax,8), %r9
	movq	%r9, %r15
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L284:
	movl	$32768, %ecx
.L210:
	movzbl	%al, %eax
	movslq	%edx, %rdx
	movslq	%edi, %rdi
	movslq	%esi, %rsi
	salq	$17, %rdx
	salq	$14, %rax
	orq	%rdx, %rax
	salq	$27, %rsi
	movq	%rcx, %rdx
	orq	%rdi, %rax
	orq	%rsi, %rax
	movq	-1(%r11), %rsi
	orq	%rax, %rdx
	shrq	$30, %rax
	movl	%edx, %ecx
	andl	$15, %eax
	sarl	$3, %ecx
	andl	$2047, %ecx
	subl	%eax, %ecx
	testb	$64, %dh
	jne	.L213
	movq	%r11, %rax
	movq	7(%r11), %rdx
	andq	$-262144, %rax
	movq	24(%rax), %rax
	subq	$37592, %rax
	testb	$1, %dl
	je	.L227
	cmpq	288(%rax), %rdx
	je	.L227
	.p2align 4,,10
	.p2align 3
.L215:
	leal	16(,%rcx,8), %eax
	cltq
	movq	-1(%rdx,%rax), %rsi
	testb	$1, %sil
	jne	.L320
	.p2align 4,,10
	.p2align 3
.L229:
	addq	$24, %r14
	cmpq	%r14, %r15
	je	.L321
.L226:
	movq	(%rbx), %rax
	movq	(%rax,%r14), %rcx
	movq	-1(%r11), %rdi
	movq	%rcx, %rax
	movzbl	7(%rdi), %edx
	movzbl	8(%rdi), %esi
	shrq	$51, %rcx
	shrq	$38, %rax
	andl	$1023, %ecx
	movq	%rax, %r10
	subl	%esi, %edx
	andl	$7, %r10d
	cmpl	%edx, %ecx
	setl	%al
	jl	.L322
	subl	%edx, %ecx
	movl	$16, %esi
	leal	16(,%rcx,8), %edi
.L209:
	cmpl	$2, %r10d
	je	.L284
	cmpb	$2, %r10b
	jg	.L211
	je	.L212
.L285:
	xorl	%ecx, %ecx
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L318:
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate13StackOverflowEv@PLT
	xorl	%eax, %eax
.L291:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L199:
	.cfi_restore_state
	movq	(%rsi), %rax
	movq	-1(%rax), %rax
	movl	15(%rax), %eax
	testl	$16777216, %eax
	jne	.L323
.L282:
	addq	$40, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L324:
	.cfi_restore_state
	movq	%rbx, %r12
	movq	-72(%rbp), %r13
	movq	-64(%rbp), %rbx
.L317:
	movq	(%rbx), %r11
.L207:
	movq	15(%r11), %rax
	movl	11(%rax), %ecx
	testl	%ecx, %ecx
	je	.L250
.L249:
	movq	-1(%r11), %rax
	movzbl	14(%rax), %edx
	movl	%edx, %eax
	shrl	$3, %eax
	cmpl	$223, %edx
	ja	.L250
	leaq	.L251(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal12_GLOBAL__N_119JSObjectWalkVisitorINS1_29AllocationSiteCreationContextEE13StructureWalkENS0_6HandleINS0_8JSObjectEEE,"a",@progbits
	.align 4
	.align 4
.L251:
	.long	.L250-.L251
	.long	.L250-.L251
	.long	.L254-.L251
	.long	.L254-.L251
	.long	.L250-.L251
	.long	.L250-.L251
	.long	.L254-.L251
	.long	.L254-.L251
	.long	.L254-.L251
	.long	.L254-.L251
	.long	.L254-.L251
	.long	.L254-.L251
	.long	.L253-.L251
	.long	.L252-.L251
	.long	.L252-.L251
	.long	.L212-.L251
	.long	.L212-.L251
	.long	.L212-.L251
	.long	.L212-.L251
	.long	.L212-.L251
	.long	.L212-.L251
	.long	.L212-.L251
	.long	.L212-.L251
	.long	.L212-.L251
	.long	.L212-.L251
	.long	.L212-.L251
	.long	.L212-.L251
	.long	.L212-.L251
	.section	.text._ZN2v88internal12_GLOBAL__N_119JSObjectWalkVisitorINS1_29AllocationSiteCreationContextEE13StructureWalkENS0_6HandleINS0_8JSObjectEEE
	.p2align 4,,10
	.p2align 3
.L314:
	movq	-72(%rbp), %r13
	movq	%r15, %r12
.L250:
	movq	%rbx, %rax
.L235:
	subl	$1, 41104(%r12)
	movq	-56(%rbp), %rbx
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L291
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L203:
	.cfi_restore_state
	movq	7(%r11), %rsi
	testb	$1, %sil
	jne	.L237
	andq	$-262144, %r11
	movq	24(%r11), %rax
	movq	-36536(%rax), %rsi
.L237:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L238
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rcx
.L239:
	movslq	35(%rsi), %rax
	testq	%rax, %rax
	jle	.L317
	subl	$1, %eax
	movq	%rbx, -64(%rbp)
	movq	%r12, %rbx
	movl	$64, %r14d
	leaq	(%rax,%rax,2), %rax
	movq	%r13, -72(%rbp)
	movq	%rcx, %r13
	leaq	88(,%rax,8), %r8
	movq	%r8, %r12
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L244:
	addq	$24, %r14
	cmpq	%r14, %r12
	je	.L324
	movq	0(%r13), %rsi
.L248:
	movq	-1(%rsi,%r14), %rsi
	testb	$1, %sil
	je	.L244
	movq	-1(%rsi), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L244
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L309
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L245:
	movq	-64(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal12_GLOBAL__N_119JSObjectWalkVisitorINS1_29AllocationSiteCreationContextEE22VisitElementOrPropertyENS0_6HandleINS0_8JSObjectEEES7_
	testq	%rax, %rax
	jne	.L244
	movq	-72(%rbp), %r13
	movq	%rbx, %r12
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L254:
	movq	41112(%r12), %rdi
	movq	15(%r11), %rsi
	testq	%rdi, %rdi
	je	.L255
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rcx
.L256:
	movq	-1(%rsi), %rax
	cmpq	%rax, 160(%r12)
	je	.L250
	movq	(%rcx), %rax
	movl	11(%rax), %edx
	testl	%edx, %edx
	jle	.L250
	movq	%r13, -64(%rbp)
	xorl	%r14d, %r14d
	movq	%r12, %r13
	movq	%rcx, %r12
	jmp	.L267
	.p2align 4,,10
	.p2align 3
.L263:
	addq	$1, %r14
	cmpl	%r14d, 11(%rax)
	jle	.L325
.L267:
	movq	15(%rax,%r14,8), %rsi
	testb	$1, %sil
	je	.L263
	movq	-1(%rsi), %rdx
	cmpw	$1024, 11(%rdx)
	jbe	.L263
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L311
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L264:
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal12_GLOBAL__N_119JSObjectWalkVisitorINS1_29AllocationSiteCreationContextEE22VisitElementOrPropertyENS0_6HandleINS0_8JSObjectEEES7_
	testq	%rax, %rax
	je	.L312
	movq	(%r12), %rax
	jmp	.L263
.L253:
	movq	41112(%r12), %rdi
	movq	15(%r11), %rsi
	testq	%rdi, %rdi
	je	.L268
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rcx
.L269:
	movslq	35(%rsi), %rax
	testq	%rax, %rax
	jle	.L250
	subl	$1, %eax
	movq	%r13, -72(%rbp)
	movq	%rcx, %r13
	movl	$56, %r14d
	leaq	(%rax,%rax,2), %rax
	movq	%r15, -64(%rbp)
	movq	%r12, %r15
	leaq	80(,%rax,8), %r8
	movq	%r8, %r12
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L274:
	addq	$24, %r14
	cmpq	%r12, %r14
	je	.L314
	movq	0(%r13), %rsi
.L278:
	movq	-1(%r14,%rsi), %rsi
	testb	$1, %sil
	je	.L274
	movq	-1(%rsi), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L274
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L313
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L275:
	movq	-64(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal12_GLOBAL__N_119JSObjectWalkVisitorINS1_29AllocationSiteCreationContextEE22VisitElementOrPropertyENS0_6HandleINS0_8JSObjectEEES7_
	testq	%rax, %rax
	jne	.L274
	movq	-72(%rbp), %r13
	movq	%r15, %r12
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L211:
	subl	$3, %r10d
	cmpb	$1, %r10b
	jbe	.L285
	.p2align 4,,10
	.p2align 3
.L212:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L213:
	movq	47(%rsi), %rax
	testq	%rax, %rax
	je	.L225
	movq	%rax, %rdi
	movl	$32, %esi
	notq	%rdi
	movl	%edi, %r10d
	andl	$1, %r10d
	jne	.L217
	movslq	11(%rax), %rsi
	sall	$3, %esi
.L217:
	cmpl	%esi, %ecx
	jnb	.L225
	testl	%ecx, %ecx
	leal	31(%rcx), %esi
	cmovns	%ecx, %esi
	sarl	$5, %esi
	andl	$1, %edi
	jne	.L218
	cmpl	%esi, 11(%rax)
	jle	.L218
	movl	%ecx, %edi
	sarl	$31, %edi
	shrl	$27, %edi
	addl	%edi, %ecx
	andl	$31, %ecx
	subl	%edi, %ecx
	movl	$1, %edi
	sall	%cl, %edi
	movl	%edi, %ecx
	testb	%r10b, %r10b
	je	.L326
.L222:
	sarq	$32, %rax
	testl	%eax, %ecx
	sete	%al
.L224:
	testb	%al, %al
	jne	.L225
.L316:
	movq	(%r12), %r11
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L225:
	movq	(%r12), %r11
	andl	$16376, %edx
	movq	-1(%r11,%rdx), %rsi
	testb	$1, %sil
	je	.L229
.L320:
	movq	-1(%rsi), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L229
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L231
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L232:
	movq	-64(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal12_GLOBAL__N_119JSObjectWalkVisitorINS1_29AllocationSiteCreationContextEE22VisitElementOrPropertyENS0_6HandleINS0_8JSObjectEEES7_
	testq	%rax, %rax
	jne	.L316
	movq	%r13, %r12
	xorl	%eax, %eax
	movq	-72(%rbp), %r13
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L322:
	movzbl	8(%rdi), %esi
	movzbl	8(%rdi), %edi
	addl	%edi, %ecx
	sall	$3, %esi
	leal	0(,%rcx,8), %edi
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L227:
	movq	968(%rax), %rdx
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L323:
	movq	%r12, %rdi
	call	_ZN2v88internal8JSObject15MigrateInstanceEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L309:
	movq	41088(%rbx), %rdx
	cmpq	41096(%rbx), %rdx
	je	.L327
.L246:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rdx)
	jmp	.L245
	.p2align 4,,10
	.p2align 3
.L326:
	sall	$2, %esi
	movslq	%esi, %rsi
	movl	15(%rax,%rsi), %eax
	testl	%eax, %edi
	sete	%al
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L319:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8JSObject15MigrateInstanceEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L321:
	movq	%r12, %rbx
	movq	-64(%rbp), %r15
	movq	%r13, %r12
	movq	-72(%rbp), %r13
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L218:
	cmpl	$31, %ecx
	jg	.L220
	testb	%r10b, %r10b
	je	.L220
	movl	$1, %esi
	sall	%cl, %esi
	movl	%esi, %ecx
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L231:
	movq	41088(%r13), %rdx
	cmpq	41096(%r13), %rdx
	je	.L328
.L233:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%rdx)
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L238:
	movq	41088(%r12), %rcx
	cmpq	41096(%r12), %rcx
	je	.L329
.L240:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rcx)
	jmp	.L239
	.p2align 4,,10
	.p2align 3
.L204:
	movq	-56(%rbp), %rax
	movq	%rax, %r8
	cmpq	%rax, 41096(%r12)
	je	.L330
.L206:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r8)
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L268:
	movq	41088(%r12), %rcx
	cmpq	41096(%r12), %rcx
	je	.L331
.L270:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rcx)
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L255:
	movq	41088(%r12), %rcx
	cmpq	41096(%r12), %rcx
	je	.L332
.L257:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rcx)
	jmp	.L256
.L252:
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L328:
	movq	%r13, %rdi
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L233
.L329:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-64(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L240
.L330:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-64(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L313:
	movq	41088(%r15), %rdx
	cmpq	41096(%r15), %rdx
	je	.L333
.L276:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rdx)
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L311:
	movq	41088(%r13), %rdx
	cmpq	41096(%r13), %rdx
	je	.L334
.L265:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%rdx)
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L325:
	movq	%r13, %r12
	movq	-64(%rbp), %r13
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L327:
	movq	%rbx, %rdi
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L246
.L312:
	movq	%r13, %r12
	xorl	%eax, %eax
	movq	-64(%rbp), %r13
	jmp	.L235
.L220:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L333:
	movq	%r15, %rdi
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L276
.L334:
	movq	%r13, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L265
.L332:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-64(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L257
.L331:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-64(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L270
	.cfi_endproc
.LFE21880:
	.size	_ZN2v88internal12_GLOBAL__N_119JSObjectWalkVisitorINS1_29AllocationSiteCreationContextEE13StructureWalkENS0_6HandleINS0_8JSObjectEEE, .-_ZN2v88internal12_GLOBAL__N_119JSObjectWalkVisitorINS1_29AllocationSiteCreationContextEE13StructureWalkENS0_6HandleINS0_8JSObjectEEE
	.section	.text._ZN2v88internal12_GLOBAL__N_118CreateArrayLiteralEPNS0_7IsolateENS0_6HandleINS0_27ArrayBoilerplateDescriptionEEENS0_14AllocationTypeE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_118CreateArrayLiteralEPNS0_7IsolateENS0_6HandleINS0_27ArrayBoilerplateDescriptionEEENS0_14AllocationTypeE, @function
_ZN2v88internal12_GLOBAL__N_118CreateArrayLiteralEPNS0_7IsolateENS0_6HandleINS0_27ArrayBoilerplateDescriptionEEENS0_14AllocationTypeE:
.LFB19791:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$72, %rsp
	movq	(%rsi), %rax
	movq	41112(%rdi), %rdi
	movb	%dl, -49(%rbp)
	movslq	11(%rax), %r14
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L336
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movzbl	%r14b, %r13d
	subl	$4, %r14d
	movq	%rax, %r15
	cmpb	$1, %r14b
	jbe	.L383
.L339:
	movq	(%r15), %rax
	movq	-1(%rax), %rax
	cmpq	%rax, 160(%r12)
	jne	.L384
.L340:
	movq	(%r15), %rax
	movl	%ebx, %r8d
	movl	%r13d, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movslq	11(%rax), %rcx
	call	_ZN2v88internal7Factory22NewJSArrayWithElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE@PLT
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L384:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory14CopyFixedArrayENS0_6HandleINS0_10FixedArrayEEE@PLT
	movq	(%r15), %rdx
	movq	%rax, %r14
	movl	11(%rdx), %eax
	testl	%eax, %eax
	jle	.L364
	movq	%r15, -72(%rbp)
	xorl	%r8d, %r8d
	movl	%ebx, -76(%rbp)
	movl	%r13d, -56(%rbp)
	movq	%r14, %r13
	movq	%r8, %r14
.L359:
	movq	0(%r13), %rax
	leaq	16(,%r14,8), %r15
	movq	-1(%r15,%rax), %rsi
	testb	$1, %sil
	je	.L342
	movq	-1(%rsi), %rax
	cmpw	$82, 11(%rax)
	jne	.L343
	movq	41088(%r12), %rax
	movq	41112(%r12), %rdi
	addl	$1, 41104(%r12)
	movq	41096(%r12), %rbx
	movq	%rax, -64(%rbp)
	testq	%rdi, %rdi
	je	.L344
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r8
.L345:
	movzbl	-49(%rbp), %edx
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal12_GLOBAL__N_118CreateArrayLiteralEPNS0_7IsolateENS0_6HandleINS0_27ArrayBoilerplateDescriptionEEENS0_14AllocationTypeE
	movq	0(%r13), %rdi
	movq	(%rax), %rdx
	leaq	-1(%rdi,%r15), %r15
	movq	%rdx, (%r15)
	testb	$1, %dl
	je	.L350
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	8(%rax), %rcx
	movq	%rax, -88(%rbp)
	testl	$262144, %ecx
	je	.L348
	movq	%r15, %rsi
	movq	%rdx, -104(%rbp)
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rax
	movq	-104(%rbp), %rdx
	movq	-96(%rbp), %rdi
	movq	8(%rax), %rcx
.L348:
	andl	$24, %ecx
	je	.L350
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L385
	.p2align 4,,10
	.p2align 3
.L350:
	subl	$1, 41104(%r12)
	movq	-64(%rbp), %rax
	movq	%rax, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L382
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rax
	movq	(%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L342:
	addq	$1, %r14
	cmpl	%r14d, 11(%rdx)
	jg	.L359
	movq	%r13, %r14
	movl	-76(%rbp), %ebx
	movl	-56(%rbp), %r13d
	movq	%r14, %r15
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L336:
	movq	41088(%r12), %r15
	cmpq	%r15, 41096(%r12)
	je	.L386
.L338:
	leaq	8(%r15), %rax
	movzbl	%r14b, %r13d
	subl	$4, %r14d
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r15)
	cmpb	$1, %r14b
	ja	.L339
.L383:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory20CopyFixedDoubleArrayENS0_6HandleINS0_16FixedDoubleArrayEEE@PLT
	movq	%rax, %r15
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L343:
	movq	-1(%rsi), %rax
	cmpw	$124, 11(%rax)
	jne	.L342
	movq	41096(%r12), %rax
	movq	41112(%r12), %rdi
	addl	$1, 41104(%r12)
	movq	41088(%r12), %rbx
	movq	%rax, -64(%rbp)
	testq	%rdi, %rdi
	je	.L352
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r8
.L353:
	movslq	19(%rsi), %rdx
	movzbl	-49(%rbp), %ecx
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_119CreateObjectLiteralEPNS0_7IsolateENS0_6HandleINS0_28ObjectBoilerplateDescriptionEEEiNS0_14AllocationTypeE
	movq	0(%r13), %rdi
	movq	(%rax), %rdx
	leaq	-1(%rdi,%r15), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L358
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	8(%rax), %rcx
	movq	%rax, -88(%rbp)
	testl	$262144, %ecx
	je	.L356
	movq	%rdx, -112(%rbp)
	movq	%rsi, -104(%rbp)
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rax
	movq	-112(%rbp), %rdx
	movq	-104(%rbp), %rsi
	movq	-96(%rbp), %rdi
	movq	8(%rax), %rcx
.L356:
	andl	$24, %ecx
	je	.L358
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L358
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L358:
	subl	$1, 41104(%r12)
	movq	-64(%rbp), %rax
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %rax
	je	.L382
	movq	%rax, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L382:
	movq	-72(%rbp), %rax
	movq	(%rax), %rdx
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L344:
	movq	%rax, %r8
	cmpq	%rbx, %rax
	je	.L387
.L346:
	leaq	8(%r8), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%r8)
	jmp	.L345
	.p2align 4,,10
	.p2align 3
.L386:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-64(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L338
	.p2align 4,,10
	.p2align 3
.L364:
	movq	%r14, %r15
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L385:
	movq	%r15, %rsi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L350
	.p2align 4,,10
	.p2align 3
.L352:
	movq	%rbx, %r8
	cmpq	-64(%rbp), %rbx
	je	.L388
.L354:
	leaq	8(%r8), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%r8)
	jmp	.L353
	.p2align 4,,10
	.p2align 3
.L387:
	movq	%r12, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L346
.L388:
	movq	%r12, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L354
	.cfi_endproc
.LFE19791:
	.size	_ZN2v88internal12_GLOBAL__N_118CreateArrayLiteralEPNS0_7IsolateENS0_6HandleINS0_27ArrayBoilerplateDescriptionEEENS0_14AllocationTypeE, .-_ZN2v88internal12_GLOBAL__N_118CreateArrayLiteralEPNS0_7IsolateENS0_6HandleINS0_27ArrayBoilerplateDescriptionEEENS0_14AllocationTypeE
	.section	.rodata._ZN2v88internal12_GLOBAL__N_119CreateObjectLiteralEPNS0_7IsolateENS0_6HandleINS0_28ObjectBoilerplateDescriptionEEEiNS0_14AllocationTypeE.str1.1,"aMS",@progbits,1
.LC11:
	.string	"(location_) != nullptr"
.LC12:
	.string	"FastLiteral"
	.section	.text._ZN2v88internal12_GLOBAL__N_119CreateObjectLiteralEPNS0_7IsolateENS0_6HandleINS0_28ObjectBoilerplateDescriptionEEEiNS0_14AllocationTypeE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_119CreateObjectLiteralEPNS0_7IsolateENS0_6HandleINS0_28ObjectBoilerplateDescriptionEEEiNS0_14AllocationTypeE, @function
_ZN2v88internal12_GLOBAL__N_119CreateObjectLiteralEPNS0_7IsolateENS0_6HandleINS0_28ObjectBoilerplateDescriptionEEEiNS0_14AllocationTypeE:
.LFB19790:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	12464(%rdi), %rax
	movb	%cl, -77(%rbp)
	movq	39(%rax), %r8
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L390
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L391:
	movq	(%r12), %rcx
	movl	%r13d, %ebx
	andl	$16, %r13d
	movl	%r13d, -76(%rbp)
	andl	$8, %ebx
	movslq	11(%rcx), %rax
	leal	-1(%rax), %edx
	testb	$1, %al
	je	.L445
	movl	%edx, %r13d
	shrl	$31, %r13d
	addl	%edx, %r13d
	sarl	%r13d
.L394:
	movl	-76(%rbp), %edx
	testl	%edx, %edx
	je	.L395
	movq	(%rsi), %rax
	movq	1215(%rax), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L396
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -72(%rbp)
	movq	(%rax), %rsi
	movl	15(%rsi), %eax
	testl	$2097152, %eax
	jne	.L446
.L399:
	movq	-72(%rbp), %rsi
	movl	%r14d, %edx
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal7Factory18NewJSObjectFromMapENS0_6HandleINS0_3MapEEENS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE@PLT
	movq	%rax, %r14
	testl	%ebx, %ebx
	jne	.L401
.L447:
	movq	%r14, %rdi
	call	_ZN2v88internal8JSObject17NormalizeElementsENS0_6HandleIS1_EE@PLT
	jmp	.L401
	.p2align 4,,10
	.p2align 3
.L395:
	movl	%r13d, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal7Factory25ObjectLiteralMapFromCacheENS0_6HandleINS0_13NativeContextEEEi@PLT
	movq	%rax, -72(%rbp)
	movq	(%rax), %rsi
.L397:
	movl	15(%rsi), %eax
	testl	$2097152, %eax
	je	.L399
.L446:
	movq	-72(%rbp), %rsi
	movl	%r14d, %ecx
	xorl	%r8d, %r8d
	movl	%r13d, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal7Factory22NewSlowJSObjectFromMapENS0_6HandleINS0_3MapEEEiNS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE@PLT
	movq	%rax, %r14
	testl	%ebx, %ebx
	je	.L447
.L401:
	movq	(%r12), %rax
	movslq	11(%rax), %rdx
	leal	-1(%rdx), %ecx
	movl	%ecx, %r13d
	shrl	$31, %r13d
	addl	%ecx, %r13d
	sarl	%r13d
	movl	%r13d, -56(%rbp)
	cmpq	$2, %rdx
	jle	.L402
	xorl	%ebx, %ebx
	jmp	.L426
	.p2align 4,,10
	.p2align 3
.L450:
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-64(%rbp), %rdx
	movq	%rax, %r13
.L404:
	movq	(%r12), %rax
	movq	31(%rdx,%rax), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L406
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r8
	testb	$1, %sil
	jne	.L448
.L409:
	movq	0(%r13), %rdx
	movq	%rdx, %rax
	notq	%rax
	andl	$1, %eax
	je	.L411
	sarq	$32, %rdx
	movl	%edx, %r11d
	js	.L415
.L413:
	movq	(%r8), %rax
	cmpq	%rax, 80(%r15)
	je	.L449
.L419:
	xorl	%ecx, %ecx
	movq	%r8, %rdx
	movl	%r11d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8JSObject29SetOwnElementIgnoreAttributesENS0_6HandleIS1_EEjNS2_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	testq	%rax, %rax
	je	.L420
.L425:
	addq	$1, %rbx
	cmpl	%ebx, -56(%rbp)
	jle	.L402
	movq	(%r12), %rax
.L426:
	movq	%rbx, %rdx
	salq	$4, %rdx
	movq	23(%rax,%rdx), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	jne	.L450
	movq	41088(%r15), %r13
	cmpq	41096(%r15), %r13
	je	.L451
.L405:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, 0(%r13)
	jmp	.L404
	.p2align 4,,10
	.p2align 3
.L411:
	movq	-1(%rdx), %rcx
	cmpw	$65, 11(%rcx)
	jne	.L415
	movsd	7(%rdx), %xmm0
	movsd	.LC10(%rip), %xmm2
	addsd	%xmm0, %xmm2
	movq	%xmm2, %rcx
	movq	%xmm2, %rdx
	shrq	$32, %rcx
	cmpq	$1127219200, %rcx
	jne	.L415
	movl	%edx, %ecx
	pxor	%xmm1, %xmm1
	movd	%xmm2, %r11d
	cvtsi2sdq	%rcx, %xmm1
	ucomisd	%xmm0, %xmm1
	setnp	%cl
	cmove	%ecx, %eax
	testb	%al, %al
	je	.L415
	cmpl	$-1, %edx
	jne	.L413
	.p2align 4,,10
	.p2align 3
.L415:
	xorl	%ecx, %ecx
	movq	%r8, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8JSObject30SetOwnPropertyIgnoreAttributesENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	testq	%rax, %rax
	jne	.L425
.L420:
	leaq	.LC11(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L406:
	movq	41088(%r15), %r8
	cmpq	41096(%r15), %r8
	je	.L452
.L408:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r8)
	testb	$1, %sil
	je	.L409
.L448:
	movq	-1(%rsi), %rax
	cmpw	$82, 11(%rax)
	je	.L453
	movq	(%r8), %rax
	movq	-1(%rax), %rdx
	cmpw	$124, 11(%rdx)
	jne	.L409
	movslq	19(%rax), %rdx
	movzbl	-77(%rbp), %ecx
	movq	%r8, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal12_GLOBAL__N_119CreateObjectLiteralEPNS0_7IsolateENS0_6HandleINS0_28ObjectBoilerplateDescriptionEEEiNS0_14AllocationTypeE
	movq	%rax, %r8
	jmp	.L409
	.p2align 4,,10
	.p2align 3
.L449:
	movq	41112(%r15), %rdi
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %rsi
	testq	%rdi, %rdi
	je	.L422
	movl	%r11d, -64(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-64(%rbp), %r11d
	movq	%rax, %r8
	jmp	.L419
	.p2align 4,,10
	.p2align 3
.L453:
	movzbl	-77(%rbp), %edx
	movq	%r8, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal12_GLOBAL__N_118CreateArrayLiteralEPNS0_7IsolateENS0_6HandleINS0_27ArrayBoilerplateDescriptionEEENS0_14AllocationTypeE
	movq	%rax, %r8
	jmp	.L409
	.p2align 4,,10
	.p2align 3
.L422:
	movq	41088(%r15), %r8
	cmpq	41096(%r15), %r8
	je	.L454
.L423:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r8)
	jmp	.L419
	.p2align 4,,10
	.p2align 3
.L396:
	movq	41088(%r15), %rax
	movq	%rax, -72(%rbp)
	cmpq	%rax, 41096(%r15)
	je	.L455
.L398:
	movq	-72(%rbp), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rcx)
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L390:
	movq	41088(%r15), %rsi
	cmpq	41096(%r15), %rsi
	je	.L456
.L392:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r15)
	movq	%r8, (%rsi)
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L451:
	movq	%r15, %rdi
	movq	%rdx, -88(%rbp)
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rdx
	movq	-64(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L452:
	movq	%r15, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-64(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L402:
	movq	-72(%rbp), %rax
	movq	(%rax), %rax
	movl	15(%rax), %eax
	testl	$2097152, %eax
	je	.L427
	movl	-76(%rbp), %eax
	testl	%eax, %eax
	je	.L457
.L427:
	addq	$56, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L445:
	.cfi_restore_state
	leal	8(,%rax,8), %eax
	cltq
	movq	-1(%rcx,%rax), %r13
	shrq	$32, %r13
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L457:
	movq	(%r14), %rax
	movq	-1(%rax), %rax
	movzbl	9(%rax), %esi
	cmpl	$2, %esi
	jg	.L458
.L428:
	leaq	.LC12(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal8JSObject17MigrateSlowToFastENS0_6HandleIS1_EEiPKc@PLT
	jmp	.L427
	.p2align 4,,10
	.p2align 3
.L456:
	movq	%r15, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L392
.L454:
	movq	%r15, %rdi
	movl	%r11d, -88(%rbp)
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movl	-88(%rbp), %r11d
	movq	-64(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L423
.L455:
	movq	%r15, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, -72(%rbp)
	jmp	.L398
.L458:
	movzbl	7(%rax), %eax
	subl	%esi, %eax
	movl	%eax, %esi
	jmp	.L428
	.cfi_endproc
.LFE19790:
	.size	_ZN2v88internal12_GLOBAL__N_119CreateObjectLiteralEPNS0_7IsolateENS0_6HandleINS0_28ObjectBoilerplateDescriptionEEEiNS0_14AllocationTypeE, .-_ZN2v88internal12_GLOBAL__N_119CreateObjectLiteralEPNS0_7IsolateENS0_6HandleINS0_28ObjectBoilerplateDescriptionEEEiNS0_14AllocationTypeE
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB10407:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L465
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L468
.L459:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L465:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L468:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L459
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE10407:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.rodata._ZN2v88internalL54Stats_Runtime_CreateObjectLiteralWithoutAllocationSiteEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC13:
	.string	"disabled-by-default-v8.runtime"
	.align 8
.LC14:
	.string	"V8.Runtime_Runtime_CreateObjectLiteralWithoutAllocationSite"
	.align 8
.LC15:
	.string	"args[0].IsObjectBoilerplateDescription()"
	.section	.rodata._ZN2v88internalL54Stats_Runtime_CreateObjectLiteralWithoutAllocationSiteEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC16:
	.string	"args[1].IsSmi()"
	.section	.text._ZN2v88internalL54Stats_Runtime_CreateObjectLiteralWithoutAllocationSiteEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL54Stats_Runtime_CreateObjectLiteralWithoutAllocationSiteEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL54Stats_Runtime_CreateObjectLiteralWithoutAllocationSiteEiPmPNS0_7IsolateE.isra.0:
.LFB24302:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L508
.L470:
	movq	_ZZN2v88internalL54Stats_Runtime_CreateObjectLiteralWithoutAllocationSiteEiPmPNS0_7IsolateEE28trace_event_unique_atomic608(%rip), %rbx
	testq	%rbx, %rbx
	je	.L509
.L472:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L510
.L474:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L478
.L479:
	leaq	.LC15(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L509:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L511
.L473:
	movq	%rbx, _ZZN2v88internalL54Stats_Runtime_CreateObjectLiteralWithoutAllocationSiteEiPmPNS0_7IsolateEE28trace_event_unique_atomic608(%rip)
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L478:
	movq	-1(%rax), %rax
	cmpw	$124, 11(%rax)
	jne	.L479
	movq	-8(%r13), %rax
	testb	$1, %al
	jne	.L512
	sarq	$32, %rax
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %r15
	movl	%eax, %edx
	call	_ZN2v88internal12_GLOBAL__N_119CreateObjectLiteralEPNS0_7IsolateENS0_6HandleINS0_28ObjectBoilerplateDescriptionEEEiNS0_14AllocationTypeE
	andl	$1, %r15d
	movq	%rax, %r13
	je	.L513
.L481:
	testq	%r13, %r13
	je	.L483
	movq	0(%r13), %r13
.L485:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L488
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L488:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L514
.L469:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L515
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L510:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L516
.L475:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L476
	movq	(%rdi), %rax
	call	*8(%rax)
.L476:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L477
	movq	(%rdi), %rax
	call	*8(%rax)
.L477:
	leaq	.LC14(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L474
	.p2align 4,,10
	.p2align 3
.L513:
	leaq	-184(%rbp), %rax
	leaq	-176(%rbp), %rdi
	movq	%r13, %rsi
	movq	%r12, -184(%rbp)
	movq	%rax, -176(%rbp)
	movl	$0, -168(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_119JSObjectWalkVisitorINS1_24DeprecationUpdateContextEE13StructureWalkENS0_6HandleINS0_8JSObjectEEE
	testq	%rax, %rax
	jne	.L481
.L483:
	movq	312(%r12), %r13
	jmp	.L485
	.p2align 4,,10
	.p2align 3
.L508:
	movq	40960(%rsi), %rax
	movl	$391, %edx
	leaq	-120(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L512:
	leaq	.LC16(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L514:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L516:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC14(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L475
	.p2align 4,,10
	.p2align 3
.L511:
	leaq	.LC13(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L473
.L515:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24302:
	.size	_ZN2v88internalL54Stats_Runtime_CreateObjectLiteralWithoutAllocationSiteEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL54Stats_Runtime_CreateObjectLiteralWithoutAllocationSiteEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL53Stats_Runtime_CreateArrayLiteralWithoutAllocationSiteEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC17:
	.string	"V8.Runtime_Runtime_CreateArrayLiteralWithoutAllocationSite"
	.align 8
.LC18:
	.string	"args[0].IsArrayBoilerplateDescription()"
	.section	.text._ZN2v88internalL53Stats_Runtime_CreateArrayLiteralWithoutAllocationSiteEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL53Stats_Runtime_CreateArrayLiteralWithoutAllocationSiteEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL53Stats_Runtime_CreateArrayLiteralWithoutAllocationSiteEiPmPNS0_7IsolateE.isra.0:
.LFB24303:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L556
.L518:
	movq	_ZZN2v88internalL53Stats_Runtime_CreateArrayLiteralWithoutAllocationSiteEiPmPNS0_7IsolateEE28trace_event_unique_atomic618(%rip), %rbx
	testq	%rbx, %rbx
	je	.L557
.L520:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L558
.L522:
	movq	41088(%r12), %r15
	movq	41096(%r12), %r14
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L526
.L527:
	leaq	.LC18(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L557:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L559
.L521:
	movq	%rbx, _ZZN2v88internalL53Stats_Runtime_CreateArrayLiteralWithoutAllocationSiteEiPmPNS0_7IsolateEE28trace_event_unique_atomic618(%rip)
	jmp	.L520
	.p2align 4,,10
	.p2align 3
.L526:
	movq	-1(%rax), %rax
	cmpw	$82, 11(%rax)
	jne	.L527
	movq	-8(%r13), %rcx
	testb	$1, %cl
	jne	.L560
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rcx, -200(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_118CreateArrayLiteralEPNS0_7IsolateENS0_6HandleINS0_27ArrayBoilerplateDescriptionEEENS0_14AllocationTypeE
	movq	-200(%rbp), %rcx
	movq	%rax, %rbx
	btq	$32, %rcx
	jnc	.L561
.L529:
	testq	%rbx, %rbx
	je	.L531
	movq	(%rbx), %r13
.L533:
	subl	$1, 41104(%r12)
	movq	%r15, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L536
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L536:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L562
.L517:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L563
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L558:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L564
.L523:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L524
	movq	(%rdi), %rax
	call	*8(%rax)
.L524:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L525
	movq	(%rdi), %rax
	call	*8(%rax)
.L525:
	leaq	.LC17(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L522
	.p2align 4,,10
	.p2align 3
.L561:
	leaq	-184(%rbp), %rax
	leaq	-176(%rbp), %rdi
	movq	%rbx, %rsi
	movq	%r12, -184(%rbp)
	movq	%rax, -176(%rbp)
	movl	$0, -168(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_119JSObjectWalkVisitorINS1_24DeprecationUpdateContextEE13StructureWalkENS0_6HandleINS0_8JSObjectEEE
	testq	%rax, %rax
	jne	.L529
.L531:
	movq	312(%r12), %r13
	jmp	.L533
	.p2align 4,,10
	.p2align 3
.L556:
	movq	40960(%rsi), %rax
	movl	$389, %edx
	leaq	-120(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L518
	.p2align 4,,10
	.p2align 3
.L560:
	leaq	.LC16(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L562:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L517
	.p2align 4,,10
	.p2align 3
.L564:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC17(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L523
	.p2align 4,,10
	.p2align 3
.L559:
	leaq	.LC13(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L521
.L563:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24303:
	.size	_ZN2v88internalL53Stats_Runtime_CreateArrayLiteralWithoutAllocationSiteEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL53Stats_Runtime_CreateArrayLiteralWithoutAllocationSiteEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL33Stats_Runtime_CreateRegExpLiteralEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC19:
	.string	"V8.Runtime_Runtime_CreateRegExpLiteral"
	.section	.rodata._ZN2v88internalL33Stats_Runtime_CreateRegExpLiteralEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC20:
	.string	"args[0].IsHeapObject()"
.LC21:
	.string	"args[2].IsString()"
.LC22:
	.string	"args[3].IsSmi()"
	.section	.text._ZN2v88internalL33Stats_Runtime_CreateRegExpLiteralEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL33Stats_Runtime_CreateRegExpLiteralEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL33Stats_Runtime_CreateRegExpLiteralEiPmPNS0_7IsolateE.isra.0:
.LFB24340:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$152, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L624
.L566:
	movq	_ZZN2v88internalL33Stats_Runtime_CreateRegExpLiteralEiPmPNS0_7IsolateEE28trace_event_unique_atomic645(%rip), %r13
	testq	%r13, %r13
	je	.L625
.L568:
	movq	$0, -160(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L626
.L570:
	movq	41088(%r12), %r14
	movq	41096(%r12), %r13
	addl	$1, 41104(%r12)
	movq	(%rbx), %rax
	testb	$1, %al
	je	.L627
	movq	-8(%rbx), %rcx
	testb	$1, %cl
	jne	.L628
	movq	-16(%rbx), %rdx
	leaq	-16(%rbx), %r15
	testb	$1, %dl
	jne	.L629
.L576:
	leaq	.LC21(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L625:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L630
.L569:
	movq	%r13, _ZZN2v88internalL33Stats_Runtime_CreateRegExpLiteralEiPmPNS0_7IsolateEE28trace_event_unique_atomic645(%rip)
	jmp	.L568
	.p2align 4,,10
	.p2align 3
.L626:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L631
.L571:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L572
	movq	(%rdi), %rax
	call	*8(%rax)
.L572:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L573
	movq	(%rdi), %rax
	call	*8(%rax)
.L573:
	leaq	.LC19(%rip), %rax
	movq	%r13, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L570
	.p2align 4,,10
	.p2align 3
.L629:
	movq	-1(%rdx), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L576
	movq	-24(%rbx), %rdx
	testb	$1, %dl
	jne	.L632
	movq	%rax, %rsi
	sarq	$32, %rdx
	andq	$-262144, %rsi
	movq	24(%rsi), %rsi
	cmpq	-37504(%rsi), %rax
	jne	.L579
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8JSRegExp3NewEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS_4base5FlagsINS1_4FlagEiEE@PLT
	testq	%rax, %rax
	je	.L623
.L622:
	movq	(%rax), %r15
.L581:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L593
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L593:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L633
.L565:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L634
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L579:
	.cfi_restore_state
	sarq	$32, %rcx
	leal	48(,%rcx,8), %ecx
	movslq	%ecx, %rcx
	movq	-1(%rax,%rcx), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L582
	movq	%rdx, -176(%rbp)
	movq	%rcx, -168(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-168(%rbp), %rcx
	movq	-176(%rbp), %rdx
	movq	(%rax), %rsi
	movq	%rax, %r8
.L583:
	andl	$1, %esi
	movq	%r8, -176(%rbp)
	movq	%rcx, -168(%rbp)
	jne	.L598
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8JSRegExp3NewEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS_4base5FlagsINS1_4FlagEiEE@PLT
	movq	-168(%rbp), %rcx
	movq	-176(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L623
	movq	(%rbx), %rdi
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %rax
	leaq	-1(%rcx,%rdi), %rsi
	cmpq	%rax, (%r8)
	je	.L635
	movq	(%r15), %rax
	movq	%rax, (%rsi)
	testb	$1, %al
	je	.L585
	cmpl	$3, %eax
	je	.L585
	movq	%rax, %rdx
	andq	$-262144, %rax
	movq	%rax, %rbx
	movq	8(%rax), %rax
	andq	$-3, %rdx
	testl	$262144, %eax
	je	.L589
	movq	%rdx, -184(%rbp)
	movq	%rsi, -176(%rbp)
	movq	%rdi, -168(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-184(%rbp), %rdx
	movq	-176(%rbp), %rsi
	movq	-168(%rbp), %rdi
.L589:
	testb	$24, %al
	je	.L585
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L585
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L585
	.p2align 4,,10
	.p2align 3
.L582:
	movq	%r14, %r8
	cmpq	41096(%r12), %r14
	je	.L636
.L584:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r8)
	jmp	.L583
	.p2align 4,,10
	.p2align 3
.L598:
	xorl	%r15d, %r15d
.L585:
	movq	%r15, %rdi
	call	_ZN2v88internal8JSRegExp4CopyENS0_6HandleIS1_EE@PLT
	jmp	.L622
	.p2align 4,,10
	.p2align 3
.L624:
	movq	40960(%rsi), %rax
	movl	$392, %edx
	leaq	-120(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L566
	.p2align 4,,10
	.p2align 3
.L627:
	leaq	.LC20(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L623:
	movq	312(%r12), %r15
	jmp	.L581
	.p2align 4,,10
	.p2align 3
.L628:
	leaq	.LC16(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L632:
	leaq	.LC22(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L631:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC19(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L571
	.p2align 4,,10
	.p2align 3
.L630:
	leaq	.LC13(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L569
	.p2align 4,,10
	.p2align 3
.L633:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L565
	.p2align 4,,10
	.p2align 3
.L635:
	movabsq	$4294967296, %rax
	movq	%rax, (%rsi)
	movq	(%r15), %r15
	jmp	.L581
	.p2align 4,,10
	.p2align 3
.L636:
	movq	%r12, %rdi
	movq	%rdx, -184(%rbp)
	movq	%rsi, -176(%rbp)
	movq	%rcx, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-184(%rbp), %rdx
	movq	-176(%rbp), %rsi
	movq	-168(%rbp), %rcx
	movq	%rax, %r8
	jmp	.L584
.L634:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24340:
	.size	_ZN2v88internalL33Stats_Runtime_CreateRegExpLiteralEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL33Stats_Runtime_CreateRegExpLiteralEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal8JSObject17FastPropertyAtPutENS0_10FieldIndexENS0_6ObjectE,"axG",@progbits,_ZN2v88internal8JSObject17FastPropertyAtPutENS0_10FieldIndexENS0_6ObjectE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8JSObject17FastPropertyAtPutENS0_10FieldIndexENS0_6ObjectE
	.type	_ZN2v88internal8JSObject17FastPropertyAtPutENS0_10FieldIndexENS0_6ObjectE, @function
_ZN2v88internal8JSObject17FastPropertyAtPutENS0_10FieldIndexENS0_6ObjectE:
.LFB13926:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	sarl	$3, %ecx
	andl	$2047, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	(%rdi), %rax
	movq	%rsi, %rdi
	shrq	$30, %rdi
	andl	$15, %edi
	movq	-1(%rax), %rdx
	subl	%edi, %ecx
	testl	$16384, %esi
	jne	.L638
	movq	7(%rax), %r13
	andq	$-262144, %rax
	movq	24(%rax), %rax
	subq	$37592, %rax
	testb	$1, %r13b
	jne	.L639
.L655:
	movq	968(%rax), %r13
.L640:
	leal	16(,%rcx,8), %eax
	cltq
	leaq	-1(%r13,%rax), %r14
	movq	%r12, (%r14)
	testb	$1, %r12b
	je	.L637
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L657
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
.L657:
	testb	$24, %al
	je	.L637
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L684
.L637:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L638:
	.cfi_restore_state
	movq	47(%rdx), %rax
	testq	%rax, %rax
	je	.L650
	movq	%rax, %rdi
	movl	$32, %edx
	notq	%rdi
	movl	%edi, %r8d
	andl	$1, %r8d
	je	.L685
.L642:
	cmpl	%edx, %ecx
	jnb	.L650
	testl	%ecx, %ecx
	leal	31(%rcx), %edx
	cmovns	%ecx, %edx
	sarl	$5, %edx
	andl	$1, %edi
	jne	.L643
	cmpl	%edx, 11(%rax)
	jle	.L643
	movl	%ecx, %edi
	sarl	$31, %edi
	shrl	$27, %edi
	addl	%edi, %ecx
	andl	$31, %ecx
	subl	%edi, %ecx
	movl	$1, %edi
	sall	%cl, %edi
	movl	%edi, %ecx
	testb	%r8b, %r8b
	je	.L686
.L647:
	sarq	$32, %rax
	testl	%eax, %ecx
	sete	%cl
.L649:
	movq	(%rbx), %rax
	movq	%rsi, %rdx
	andl	$16383, %edx
	leaq	-1(%rdx,%rax), %rdx
	testb	%cl, %cl
	jne	.L650
	movq	7(%r12), %rax
	movq	%rax, (%rdx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L650:
	.cfi_restore_state
	movq	(%rbx), %rax
	andl	$16383, %esi
	leaq	-1(%rsi), %r13
	movq	%r12, -1(%rsi,%rax)
	testb	$1, %r12b
	je	.L637
	movq	%r12, %r14
	movq	(%rbx), %rdi
	andq	$-262144, %r14
	movq	8(%r14), %rax
	leaq	0(%r13,%rdi), %rsi
	testl	$262144, %eax
	je	.L652
	movq	%r12, %rdx
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	(%rbx), %rdi
	movq	8(%r14), %rax
	leaq	0(%r13,%rdi), %rsi
.L652:
	testb	$24, %al
	je	.L637
	movq	%rdi, %rax
	movq	%r12, %rdx
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L637
.L683:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L639:
	.cfi_restore_state
	cmpq	288(%rax), %r13
	jne	.L640
	jmp	.L655
	.p2align 4,,10
	.p2align 3
.L685:
	movslq	11(%rax), %rdx
	sall	$3, %edx
	jmp	.L642
	.p2align 4,,10
	.p2align 3
.L686:
	sall	$2, %edx
	movslq	%edx, %rdx
	movl	15(%rax,%rdx), %eax
	testl	%eax, %edi
	sete	%cl
	jmp	.L649
	.p2align 4,,10
	.p2align 3
.L684:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	jmp	.L683
	.p2align 4,,10
	.p2align 3
.L643:
	testb	%r8b, %r8b
	je	.L645
	cmpl	$31, %ecx
	jg	.L645
	movl	$1, %edx
	sall	%cl, %edx
	movl	%edx, %ecx
	jmp	.L647
.L645:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE13926:
	.size	_ZN2v88internal8JSObject17FastPropertyAtPutENS0_10FieldIndexENS0_6ObjectE, .-_ZN2v88internal8JSObject17FastPropertyAtPutENS0_10FieldIndexENS0_6ObjectE
	.section	.rodata._ZN2v88internal12_GLOBAL__N_119JSObjectWalkVisitorINS0_26AllocationSiteUsageContextEE13StructureWalkENS0_6HandleINS0_8JSObjectEEE.str1.1,"aMS",@progbits,1
.LC23:
	.string	"JSArray"
.LC24:
	.string	"JSObject"
	.section	.rodata._ZN2v88internal12_GLOBAL__N_119JSObjectWalkVisitorINS0_26AllocationSiteUsageContextEE13StructureWalkENS0_6HandleINS0_8JSObjectEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC25:
	.string	"*** Creating Memento for %s %p\n"
	.section	.text._ZN2v88internal12_GLOBAL__N_119JSObjectWalkVisitorINS0_26AllocationSiteUsageContextEE13StructureWalkENS0_6HandleINS0_8JSObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_119JSObjectWalkVisitorINS0_26AllocationSiteUsageContextEE13StructureWalkENS0_6HandleINS0_8JSObjectEEE, @function
_ZN2v88internal12_GLOBAL__N_119JSObjectWalkVisitorINS0_26AllocationSiteUsageContextEE13StructureWalkENS0_6HandleINS0_8JSObjectEEE:
.LFB21884:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movl	8(%rdi), %r15d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	(%rax), %r12
	cmpl	$1, %r15d
	je	.L688
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	37528(%r12), %rax
	jb	.L890
.L688:
	movq	0(%r13), %rax
	movq	-1(%rax), %rax
	movl	15(%rax), %eax
	testl	$16777216, %eax
	jne	.L891
.L690:
	movq	(%rbx), %rax
	xorl	%edx, %edx
	cmpb	$0, 32(%rax)
	jne	.L892
.L692:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory30CopyJSObjectWithAllocationSiteENS0_6HandleINS0_8JSObjectEEENS2_INS0_14AllocationSiteEEE@PLT
	movq	%rax, %r14
	cmpl	$1, %r15d
	jne	.L893
.L689:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L894
	addq	$88, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L893:
	.cfi_restore_state
	movq	41088(%r12), %rax
	addl	$1, 41104(%r12)
	movq	(%r14), %r11
	movq	%rax, -72(%rbp)
	movq	41096(%r12), %rax
	movq	%rax, -80(%rbp)
	movq	-1(%r11), %rax
	cmpw	$1061, 11(%rax)
	je	.L766
	movq	-1(%r11), %rax
	movl	15(%rax), %eax
	testl	$2097152, %eax
	jne	.L702
	movq	-1(%r11), %rax
	movq	41112(%r12), %rdi
	movq	39(%rax), %rsi
	testq	%rdi, %rdi
	je	.L703
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r8
.L704:
	movq	(%r14), %r11
	movq	-1(%r11), %rax
	movl	15(%rax), %eax
	shrl	$10, %eax
	andl	$1023, %eax
	je	.L706
	subl	$1, %eax
	movq	%r12, -88(%rbp)
	movl	$31, %r15d
	leaq	(%rax,%rax,2), %rax
	movq	%rbx, -96(%rbp)
	movq	%r8, %rbx
	leaq	55(,%rax,8), %r13
	movq	%r13, %r12
	jmp	.L725
	.p2align 4,,10
	.p2align 3
.L826:
	movl	$32768, %r13d
.L709:
	movzbl	%al, %eax
	movslq	%ecx, %rcx
	movslq	%edi, %rdi
	movslq	%esi, %rsi
	salq	$17, %rcx
	salq	$14, %rax
	orq	%rcx, %rax
	salq	$27, %rsi
	orq	%rdi, %rax
	orq	%rsi, %rax
	movq	-1(%r11), %rsi
	orq	%rax, %r13
	shrq	$30, %rax
	movl	%r13d, %ecx
	andl	$15, %eax
	sarl	$3, %ecx
	andl	$2047, %ecx
	subl	%eax, %ecx
	testl	$16384, %r13d
	jne	.L712
	movq	%r11, %rax
	movq	7(%r11), %rsi
	andq	$-262144, %rax
	movq	24(%rax), %rax
	subq	$37592, %rax
	testb	$1, %sil
	je	.L726
	cmpq	288(%rax), %rsi
	je	.L726
	.p2align 4,,10
	.p2align 3
.L714:
	leal	16(,%rcx,8), %eax
	cltq
	movq	-1(%rsi,%rax), %rsi
	testb	$1, %sil
	jne	.L895
.L727:
	cmpl	$2, %edx
	je	.L896
.L741:
	addq	$24, %r15
	cmpq	%r15, %r12
	je	.L897
.L725:
	movq	(%rbx), %rax
	movq	(%rax,%r15), %rdi
	movq	-1(%r11), %r10
	movq	%rdi, %rax
	movzbl	7(%r10), %ecx
	movzbl	8(%r10), %esi
	shrq	$51, %rdi
	shrq	$38, %rax
	andl	$1023, %edi
	movq	%rax, %rdx
	subl	%esi, %ecx
	andl	$7, %edx
	cmpl	%ecx, %edi
	setl	%al
	jl	.L898
	subl	%ecx, %edi
	movl	$16, %esi
	leal	16(,%rdi,8), %edi
.L708:
	cmpl	$2, %edx
	je	.L826
	cmpb	$2, %dl
	jg	.L710
	je	.L711
.L827:
	xorl	%r13d, %r13d
	jmp	.L709
	.p2align 4,,10
	.p2align 3
.L890:
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	call	_ZN2v88internal7Isolate13StackOverflowEv@PLT
	jmp	.L689
	.p2align 4,,10
	.p2align 3
.L892:
	movq	0(%r13), %r8
	movq	-1(%r8), %rcx
	cmpb	$0, _ZN2v88internal32FLAG_allocation_site_pretenuringE(%rip)
	movzwl	11(%rcx), %ecx
	jne	.L899
	cmpw	$1061, %cx
	jne	.L692
	movq	-1(%r8), %rcx
	cmpb	$15, 14(%rcx)
	ja	.L692
	jmp	.L822
	.p2align 4,,10
	.p2align 3
.L891:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8JSObject15MigrateInstanceEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	jmp	.L690
	.p2align 4,,10
	.p2align 3
.L900:
	movq	-88(%rbp), %r14
.L888:
	movq	(%r14), %r11
.L706:
	movq	15(%r11), %rax
	movl	11(%rax), %edx
	testl	%edx, %edx
	je	.L740
.L766:
	movq	-1(%r11), %rax
	movzbl	14(%rax), %edx
	movl	%edx, %eax
	shrl	$3, %eax
	cmpl	$223, %edx
	ja	.L740
	leaq	.L768(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal12_GLOBAL__N_119JSObjectWalkVisitorINS0_26AllocationSiteUsageContextEE13StructureWalkENS0_6HandleINS0_8JSObjectEEE,"a",@progbits
	.align 4
	.align 4
.L768:
	.long	.L740-.L768
	.long	.L740-.L768
	.long	.L771-.L768
	.long	.L771-.L768
	.long	.L740-.L768
	.long	.L740-.L768
	.long	.L771-.L768
	.long	.L771-.L768
	.long	.L771-.L768
	.long	.L771-.L768
	.long	.L771-.L768
	.long	.L771-.L768
	.long	.L770-.L768
	.long	.L769-.L768
	.long	.L769-.L768
	.long	.L711-.L768
	.long	.L711-.L768
	.long	.L711-.L768
	.long	.L711-.L768
	.long	.L711-.L768
	.long	.L711-.L768
	.long	.L711-.L768
	.long	.L711-.L768
	.long	.L711-.L768
	.long	.L711-.L768
	.long	.L711-.L768
	.long	.L711-.L768
	.long	.L711-.L768
	.section	.text._ZN2v88internal12_GLOBAL__N_119JSObjectWalkVisitorINS0_26AllocationSiteUsageContextEE13StructureWalkENS0_6HandleINS0_8JSObjectEEE
	.p2align 4,,10
	.p2align 3
.L885:
	movq	-88(%rbp), %r14
.L740:
	subl	$1, 41104(%r12)
	movq	-72(%rbp), %rax
	movq	%rax, 41088(%r12)
	movq	-80(%rbp), %rax
	cmpq	41096(%r12), %rax
	je	.L689
	movq	%rax, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	jmp	.L689
	.p2align 4,,10
	.p2align 3
.L702:
	movq	7(%r11), %rsi
	testb	$1, %sil
	jne	.L743
	andq	$-262144, %r11
	movq	24(%r11), %rax
	movq	-36536(%rax), %rsi
.L743:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L744
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rcx
.L745:
	movslq	35(%rsi), %rax
	testq	%rax, %rax
	jle	.L888
	subl	$1, %eax
	movq	%r14, -88(%rbp)
	movl	$64, %r15d
	leaq	(%rax,%rax,2), %rax
	leaq	88(,%rax,8), %r13
	movq	%r13, %r14
	movq	%rcx, %r13
	jmp	.L765
	.p2align 4,,10
	.p2align 3
.L748:
	addq	$24, %r15
	cmpq	%r15, %r14
	je	.L900
	movq	0(%r13), %rsi
.L765:
	movq	-1(%rsi,%r15), %rsi
	testb	$1, %sil
	je	.L748
	movq	-1(%rsi), %rax
	leaq	-1(%rsi), %rdx
	cmpw	$1024, 11(%rax)
	jbe	.L748
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L750
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r8
	movq	(%rax), %rax
	leaq	-1(%rax), %rdx
.L751:
	movq	(%rdx), %rax
	cmpw	$1061, 11(%rax)
	je	.L753
.L759:
	movq	%r8, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal12_GLOBAL__N_119JSObjectWalkVisitorINS0_26AllocationSiteUsageContextEE13StructureWalkENS0_6HandleINS0_8JSObjectEEE
	testq	%rax, %rax
	je	.L791
	movq	0(%r13), %rdi
	movq	(%rax), %rdx
	leaq	-1(%rdi,%r15), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L748
	movq	%rdx, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rax
	movq	%r8, -96(%rbp)
	testl	$262144, %eax
	je	.L763
	movq	%rdx, -120(%rbp)
	movq	%rsi, -112(%rbp)
	movq	%rdi, -104(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-96(%rbp), %r8
	movq	-120(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	-104(%rbp), %rdi
	movq	8(%r8), %rax
.L763:
	testb	$24, %al
	je	.L748
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L748
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L748
	.p2align 4,,10
	.p2align 3
.L899:
	andl	$-5, %ecx
	cmpw	$1057, %cx
	jne	.L692
.L822:
	cmpb	$0, _ZN2v88internal36FLAG_trace_creation_allocation_sitesE(%rip)
	jne	.L901
.L698:
	movq	16(%rax), %rdx
	jmp	.L692
	.p2align 4,,10
	.p2align 3
.L771:
	movq	41112(%r12), %rdi
	movq	15(%r11), %rsi
	testq	%rdi, %rdi
	je	.L772
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rcx
.L773:
	movq	-1(%rsi), %rax
	cmpq	%rax, 160(%r12)
	je	.L740
	movq	(%rcx), %rdx
	movl	11(%rdx), %eax
	testl	%eax, %eax
	jle	.L740
	movq	%r14, -88(%rbp)
	xorl	%r15d, %r15d
	movq	%rcx, %r13
	jmp	.L795
	.p2align 4,,10
	.p2align 3
.L778:
	addq	$1, %r15
	cmpl	%r15d, 11(%rdx)
	jle	.L885
.L795:
	leaq	0(,%r15,8), %rax
	movq	15(%rdx,%rax), %rsi
	leaq	16(%rax), %r14
	testb	$1, %sil
	je	.L778
	movq	-1(%rsi), %rax
	leaq	-1(%rsi), %r9
	cmpw	$1024, 11(%rax)
	jbe	.L778
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L780
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r8
	movq	(%rax), %rax
	leaq	-1(%rax), %r9
.L781:
	movq	(%r9), %rax
	cmpw	$1061, 11(%rax)
	je	.L783
.L789:
	movq	%r8, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal12_GLOBAL__N_119JSObjectWalkVisitorINS0_26AllocationSiteUsageContextEE13StructureWalkENS0_6HandleINS0_8JSObjectEEE
	testq	%rax, %rax
	je	.L791
	movq	0(%r13), %rdi
	movq	(%rax), %rdx
	leaq	-1(%rdi,%r14), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L889
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -96(%rbp)
	testl	$262144, %eax
	je	.L793
	movq	%rdx, -120(%rbp)
	movq	%rsi, -112(%rbp)
	movq	%rdi, -104(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-96(%rbp), %rcx
	movq	-120(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	-104(%rbp), %rdi
	movq	8(%rcx), %rax
.L793:
	testb	$24, %al
	je	.L889
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L889
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L889:
	movq	0(%r13), %rdx
	jmp	.L778
.L770:
	movq	41112(%r12), %rdi
	movq	15(%r11), %rsi
	testq	%rdi, %rdi
	je	.L796
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rcx
.L797:
	movslq	35(%rsi), %rax
	testq	%rax, %rax
	jle	.L740
	subl	$1, %eax
	movq	%r14, -88(%rbp)
	movl	$56, %r15d
	movq	%rcx, %r13
	leaq	(%rax,%rax,2), %rax
	leaq	80(,%rax,8), %r8
	movq	%r8, %r14
	jmp	.L816
	.p2align 4,,10
	.p2align 3
.L800:
	addq	$24, %r15
	cmpq	%r14, %r15
	je	.L885
	movq	0(%r13), %rsi
.L816:
	movq	-1(%r15,%rsi), %rsi
	testb	$1, %sil
	je	.L800
	movq	-1(%rsi), %rax
	leaq	-1(%rsi), %rdx
	cmpw	$1024, 11(%rax)
	jbe	.L800
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L802
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r9
	movq	(%rax), %rax
	leaq	-1(%rax), %rdx
.L803:
	movq	(%rdx), %rax
	cmpw	$1061, 11(%rax)
	je	.L805
.L811:
	movq	%r9, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal12_GLOBAL__N_119JSObjectWalkVisitorINS0_26AllocationSiteUsageContextEE13StructureWalkENS0_6HandleINS0_8JSObjectEEE
	testq	%rax, %rax
	je	.L791
	movq	0(%r13), %rdi
	movq	(%rax), %rdx
	leaq	-1(%r15,%rdi), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L800
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -96(%rbp)
	testl	$262144, %eax
	je	.L814
	movq	%rdx, -120(%rbp)
	movq	%rsi, -112(%rbp)
	movq	%rdi, -104(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-96(%rbp), %rcx
	movq	-120(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	-104(%rbp), %rdi
	movq	8(%rcx), %rax
.L814:
	testb	$24, %al
	je	.L800
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L800
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L800
	.p2align 4,,10
	.p2align 3
.L710:
	leal	-3(%rdx), %r10d
	cmpb	$1, %r10b
	jbe	.L827
	.p2align 4,,10
	.p2align 3
.L711:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L712:
	movq	47(%rsi), %rax
	testq	%rax, %rax
	je	.L724
	movq	%rax, %rdi
	movl	$32, %esi
	notq	%rdi
	movl	%edi, %r10d
	andl	$1, %r10d
	jne	.L716
	movslq	11(%rax), %rsi
	sall	$3, %esi
.L716:
	cmpl	%esi, %ecx
	jnb	.L724
	testl	%ecx, %ecx
	leal	31(%rcx), %esi
	cmovns	%ecx, %esi
	sarl	$5, %esi
	andl	$1, %edi
	jne	.L717
	cmpl	%esi, 11(%rax)
	jle	.L717
	movl	%ecx, %edi
	sarl	$31, %edi
	shrl	$27, %edi
	addl	%edi, %ecx
	andl	$31, %ecx
	subl	%edi, %ecx
	movl	$1, %edi
	sall	%cl, %edi
	movl	%edi, %ecx
	testb	%r10b, %r10b
	je	.L902
.L721:
	sarq	$32, %rax
	testl	%eax, %ecx
	sete	%al
.L723:
	testb	%al, %al
	jne	.L724
	movq	(%r14), %r11
	jmp	.L741
	.p2align 4,,10
	.p2align 3
.L724:
	movq	(%r14), %r11
	movq	%r13, %rax
	andl	$16376, %eax
	movq	-1(%r11,%rax), %rsi
	testb	$1, %sil
	je	.L727
.L895:
	movq	-1(%rsi), %rax
	leaq	-1(%rsi), %rcx
	cmpw	$1024, 11(%rax)
	jbe	.L727
	movq	-88(%rbp), %rax
	movq	41112(%rax), %rdi
	testq	%rdi, %rdi
	je	.L728
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r10
	movq	(%rax), %rax
	leaq	-1(%rax), %rcx
.L729:
	movq	(%rcx), %rax
	cmpw	$1061, 11(%rax)
	je	.L731
.L737:
	movq	-96(%rbp), %rdi
	movq	%r10, %rsi
	call	_ZN2v88internal12_GLOBAL__N_119JSObjectWalkVisitorINS0_26AllocationSiteUsageContextEE13StructureWalkENS0_6HandleINS0_8JSObjectEEE
	testq	%rax, %rax
	je	.L903
.L887:
	movq	(%r14), %rdx
	leaq	-64(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rdx, -64(%rbp)
	movq	(%rax), %rdx
	call	_ZN2v88internal8JSObject17FastPropertyAtPutENS0_10FieldIndexENS0_6ObjectE
	movq	(%r14), %r11
	jmp	.L741
	.p2align 4,,10
	.p2align 3
.L898:
	movzbl	8(%r10), %esi
	movzbl	8(%r10), %r10d
	addl	%r10d, %edi
	sall	$3, %esi
	sall	$3, %edi
	jmp	.L708
	.p2align 4,,10
	.p2align 3
.L726:
	movq	968(%rax), %rsi
	jmp	.L714
	.p2align 4,,10
	.p2align 3
.L896:
	movq	7(%rsi), %rcx
	movq	-88(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rcx, -104(%rbp)
	call	_ZN2v88internal7Factory13NewHeapNumberENS0_14AllocationTypeE@PLT
	movq	-104(%rbp), %rcx
	movq	(%rax), %rdx
	movq	%rcx, 7(%rdx)
	jmp	.L887
	.p2align 4,,10
	.p2align 3
.L750:
	movq	41088(%r12), %r8
	cmpq	41096(%r12), %r8
	je	.L904
.L752:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r8)
	jmp	.L751
	.p2align 4,,10
	.p2align 3
.L902:
	sall	$2, %esi
	movslq	%esi, %rsi
	movl	15(%rax,%rsi), %eax
	testl	%eax, %edi
	sete	%al
	jmp	.L723
	.p2align 4,,10
	.p2align 3
.L897:
	movq	-88(%rbp), %r12
	movq	-96(%rbp), %rbx
	jmp	.L706
	.p2align 4,,10
	.p2align 3
.L791:
	xorl	%r14d, %r14d
	jmp	.L740
	.p2align 4,,10
	.p2align 3
.L753:
	movq	(%rbx), %rdx
	cmpq	$0, 8(%rdx)
	je	.L905
	movq	16(%rdx), %rax
	movq	(%rax), %rsi
	movq	15(%rsi), %rsi
	movq	%rsi, (%rax)
	movq	16(%rdx), %rax
.L757:
	movq	(%rdx), %rdx
	movq	(%rax), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L758
	movq	%r8, -96(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-96(%rbp), %r8
	jmp	.L759
	.p2align 4,,10
	.p2align 3
.L717:
	testb	%r10b, %r10b
	je	.L719
	cmpl	$31, %ecx
	jg	.L719
	movl	$1, %esi
	sall	%cl, %esi
	movl	%esi, %ecx
	jmp	.L721
	.p2align 4,,10
	.p2align 3
.L728:
	movq	41088(%rax), %r10
	cmpq	41096(%rax), %r10
	je	.L906
.L730:
	movq	-88(%rbp), %rdx
	leaq	8(%r10), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%r10)
	jmp	.L729
	.p2align 4,,10
	.p2align 3
.L703:
	movq	-72(%rbp), %rax
	movq	%rax, %r8
	cmpq	%rax, 41096(%r12)
	je	.L907
.L705:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r8)
	jmp	.L704
	.p2align 4,,10
	.p2align 3
.L744:
	movq	41088(%r12), %rcx
	cmpq	41096(%r12), %rcx
	je	.L908
.L746:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rcx)
	jmp	.L745
	.p2align 4,,10
	.p2align 3
.L758:
	movq	41088(%rdx), %rax
	cmpq	41096(%rdx), %rax
	je	.L909
.L760:
	leaq	8(%rax), %rdi
	movq	%rdi, 41088(%rdx)
	movq	%rsi, (%rax)
	jmp	.L759
	.p2align 4,,10
	.p2align 3
.L796:
	movq	41088(%r12), %rcx
	cmpq	41096(%r12), %rcx
	je	.L910
.L798:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rcx)
	jmp	.L797
	.p2align 4,,10
	.p2align 3
.L772:
	movq	41088(%r12), %rcx
	cmpq	41096(%r12), %rcx
	je	.L911
.L774:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rcx)
	jmp	.L773
.L769:
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L906:
	movq	%rax, %rdi
	movq	%rcx, -112(%rbp)
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-112(%rbp), %rcx
	movq	-104(%rbp), %rsi
	movq	%rax, %r10
	jmp	.L730
.L908:
	movq	%r12, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L746
.L907:
	movq	%r12, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L705
	.p2align 4,,10
	.p2align 3
.L802:
	movq	41088(%r12), %r9
	cmpq	41096(%r12), %r9
	je	.L912
.L804:
	leaq	8(%r9), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r9)
	jmp	.L803
	.p2align 4,,10
	.p2align 3
.L805:
	movq	(%rbx), %rdx
	cmpq	$0, 8(%rdx)
	je	.L913
	movq	16(%rdx), %rax
	movq	(%rax), %rcx
	movq	15(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	16(%rdx), %rax
.L809:
	movq	(%rdx), %rdx
	movq	(%rax), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L810
	movq	%r9, -96(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-96(%rbp), %r9
	jmp	.L811
	.p2align 4,,10
	.p2align 3
.L731:
	movq	-96(%rbp), %rax
	movq	(%rax), %rdx
	cmpq	$0, 8(%rdx)
	je	.L914
	movq	16(%rdx), %rax
	movq	(%rax), %rcx
	movq	15(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	16(%rdx), %rax
.L735:
	movq	(%rdx), %rdx
	movq	(%rax), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L736
	movq	%r10, -104(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-104(%rbp), %r10
	jmp	.L737
	.p2align 4,,10
	.p2align 3
.L780:
	movq	41088(%r12), %r8
	cmpq	41096(%r12), %r8
	je	.L915
.L782:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r8)
	jmp	.L781
	.p2align 4,,10
	.p2align 3
.L783:
	movq	(%rbx), %rdx
	cmpq	$0, 8(%rdx)
	je	.L916
	movq	16(%rdx), %rax
	movq	(%rax), %rsi
	movq	15(%rsi), %rsi
	movq	%rsi, (%rax)
	movq	16(%rdx), %rax
.L787:
	movq	(%rdx), %rdx
	movq	(%rax), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L788
	movq	%r8, -96(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-96(%rbp), %r8
	jmp	.L789
	.p2align 4,,10
	.p2align 3
.L901:
	movq	-1(%r8), %rax
	leaq	.LC23(%rip), %rsi
	movq	%r8, %rdx
	leaq	.LC25(%rip), %rdi
	cmpw	$1061, 11(%rax)
	leaq	.LC24(%rip), %rax
	cmovne	%rax, %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	(%rbx), %rax
	jmp	.L698
	.p2align 4,,10
	.p2align 3
.L903:
	movq	-88(%rbp), %r12
	xorl	%r14d, %r14d
	jmp	.L740
	.p2align 4,,10
	.p2align 3
.L810:
	movq	41088(%rdx), %rax
	cmpq	41096(%rdx), %rax
	je	.L917
.L812:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%rdx)
	movq	%rsi, (%rax)
	jmp	.L811
	.p2align 4,,10
	.p2align 3
.L736:
	movq	41088(%rdx), %rax
	cmpq	41096(%rdx), %rax
	je	.L918
.L738:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%rdx)
	movq	%rsi, (%rax)
	jmp	.L737
	.p2align 4,,10
	.p2align 3
.L904:
	movq	%r12, %rdi
	movq	%rdx, -104(%rbp)
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L752
.L788:
	movq	41088(%rdx), %rax
	cmpq	41096(%rdx), %rax
	je	.L919
.L790:
	leaq	8(%rax), %rdi
	movq	%rdi, 41088(%rdx)
	movq	%rsi, (%rax)
	jmp	.L789
.L905:
	movq	24(%rdx), %rax
	movq	(%rdx), %rsi
	movq	%rax, 8(%rdx)
	movq	(%rax), %r9
	movq	41088(%rsi), %rax
	cmpq	41096(%rsi), %rax
	je	.L920
.L756:
	leaq	8(%rax), %rdi
	movq	%rdi, 41088(%rsi)
	movq	%r9, (%rax)
	movq	%rax, 16(%rdx)
	jmp	.L757
.L719:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L912:
	movq	%r12, %rdi
	movq	%rdx, -104(%rbp)
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%rax, %r9
	jmp	.L804
.L913:
	movq	24(%rdx), %rax
	movq	(%rdx), %rcx
	movq	%rax, 8(%rdx)
	movq	(%rax), %rsi
	movq	41088(%rcx), %rax
	cmpq	41096(%rcx), %rax
	je	.L921
.L808:
	leaq	8(%rax), %rdi
	movq	%rdi, 41088(%rcx)
	movq	%rsi, (%rax)
	movq	%rax, 16(%rdx)
	jmp	.L809
.L914:
	movq	24(%rdx), %rax
	movq	(%rdx), %rsi
	movq	%rax, 8(%rdx)
	movq	(%rax), %rcx
	movq	41088(%rsi), %rax
	cmpq	41096(%rsi), %rax
	je	.L922
.L734:
	leaq	8(%rax), %rdi
	movq	%rdi, 41088(%rsi)
	movq	%rcx, (%rax)
	movq	%rax, 16(%rdx)
	jmp	.L735
.L915:
	movq	%r12, %rdi
	movq	%r9, -104(%rbp)
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %r9
	movq	-96(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L782
.L909:
	movq	%rdx, %rdi
	movq	%rsi, -112(%rbp)
	movq	%r8, -104(%rbp)
	movq	%rdx, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-112(%rbp), %rsi
	movq	-104(%rbp), %r8
	movq	-96(%rbp), %rdx
	jmp	.L760
.L910:
	movq	%r12, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L798
.L911:
	movq	%r12, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L774
.L916:
	movq	24(%rdx), %rax
	movq	(%rdx), %rsi
	movq	%rax, 8(%rdx)
	movq	(%rax), %r9
	movq	41088(%rsi), %rax
	cmpq	41096(%rsi), %rax
	je	.L923
.L786:
	leaq	8(%rax), %rdi
	movq	%rdi, 41088(%rsi)
	movq	%r9, (%rax)
	movq	%rax, 16(%rdx)
	jmp	.L787
.L917:
	movq	%rdx, %rdi
	movq	%rsi, -112(%rbp)
	movq	%r9, -104(%rbp)
	movq	%rdx, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-112(%rbp), %rsi
	movq	-104(%rbp), %r9
	movq	-96(%rbp), %rdx
	jmp	.L812
.L918:
	movq	%rdx, %rdi
	movq	%rsi, -120(%rbp)
	movq	%r10, -112(%rbp)
	movq	%rdx, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %rsi
	movq	-112(%rbp), %r10
	movq	-104(%rbp), %rdx
	jmp	.L738
.L919:
	movq	%rdx, %rdi
	movq	%rsi, -112(%rbp)
	movq	%r8, -104(%rbp)
	movq	%rdx, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-112(%rbp), %rsi
	movq	-104(%rbp), %r8
	movq	-96(%rbp), %rdx
	jmp	.L790
.L920:
	movq	%rsi, %rdi
	movq	%r9, -120(%rbp)
	movq	%rdx, -112(%rbp)
	movq	%r8, -104(%rbp)
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %r9
	movq	-112(%rbp), %rdx
	movq	-104(%rbp), %r8
	movq	-96(%rbp), %rsi
	jmp	.L756
.L921:
	movq	%rcx, %rdi
	movq	%rsi, -120(%rbp)
	movq	%rdx, -112(%rbp)
	movq	%r9, -104(%rbp)
	movq	%rcx, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %rsi
	movq	-112(%rbp), %rdx
	movq	-104(%rbp), %r9
	movq	-96(%rbp), %rcx
	jmp	.L808
.L922:
	movq	%rsi, %rdi
	movq	%rcx, -128(%rbp)
	movq	%rdx, -120(%rbp)
	movq	%r10, -112(%rbp)
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-128(%rbp), %rcx
	movq	-120(%rbp), %rdx
	movq	-112(%rbp), %r10
	movq	-104(%rbp), %rsi
	jmp	.L734
.L923:
	movq	%rsi, %rdi
	movq	%r9, -120(%rbp)
	movq	%rdx, -112(%rbp)
	movq	%r8, -104(%rbp)
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %r9
	movq	-112(%rbp), %rdx
	movq	-104(%rbp), %r8
	movq	-96(%rbp), %rsi
	jmp	.L786
.L894:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21884:
	.size	_ZN2v88internal12_GLOBAL__N_119JSObjectWalkVisitorINS0_26AllocationSiteUsageContextEE13StructureWalkENS0_6HandleINS0_8JSObjectEEE, .-_ZN2v88internal12_GLOBAL__N_119JSObjectWalkVisitorINS0_26AllocationSiteUsageContextEE13StructureWalkENS0_6HandleINS0_8JSObjectEEE
	.section	.rodata._ZN2v88internal12_GLOBAL__N_113CreateLiteralINS1_19ObjectLiteralHelperEEENS0_11MaybeHandleINS0_8JSObjectEEEPNS0_7IsolateENS4_INS0_14FeedbackVectorEEEiNS0_6HandleINS0_10HeapObjectEEEi.str1.8,"aMS",@progbits,1
	.align 8
.LC26:
	.string	"literals_slot.ToInt() < vector->length()"
	.section	.text._ZN2v88internal12_GLOBAL__N_113CreateLiteralINS1_19ObjectLiteralHelperEEENS0_11MaybeHandleINS0_8JSObjectEEEPNS0_7IsolateENS4_INS0_14FeedbackVectorEEEiNS0_6HandleINS0_10HeapObjectEEEi,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_113CreateLiteralINS1_19ObjectLiteralHelperEEENS0_11MaybeHandleINS0_8JSObjectEEEPNS0_7IsolateENS4_INS0_14FeedbackVectorEEEiNS0_6HandleINS0_10HeapObjectEEEi, @function
_ZN2v88internal12_GLOBAL__N_113CreateLiteralINS1_19ObjectLiteralHelperEEENS0_11MaybeHandleINS0_8JSObjectEEEPNS0_7IsolateENS4_INS0_14FeedbackVectorEEEiNS0_6HandleINS0_10HeapObjectEEEi:
.LFB21931:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%r8d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L980
	movq	(%rsi), %rax
	cmpl	31(%rax), %edx
	jge	.L981
	leal	48(,%rdx,8), %r13d
	movslq	%r13d, %r13
	movq	-1(%rax,%r13), %rsi
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L931
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r10
	movl	%r12d, %eax
	andl	$1, %eax
	movl	%eax, -120(%rbp)
	testb	$1, %sil
	je	.L934
.L985:
	movq	41112(%r15), %rdi
	movq	7(%rsi), %rsi
	testq	%rdi, %rdi
	je	.L935
	movq	%r10, -128(%rbp)
	leaq	-96(%rbp), %r14
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-128(%rbp), %r10
	leaq	-112(%rbp), %r9
	movq	%rax, %r11
.L936:
	shrl	%r12d
	movq	%r15, %xmm0
	movq	%r10, -72(%rbp)
	movq	%r10, %xmm1
	xorl	$1, %r12d
	punpcklqdq	%xmm1, %xmm0
	movq	$0, -80(%rbp)
	movq	41088(%r15), %rax
	andl	$1, %r12d
	movaps	%xmm0, -96(%rbp)
	movb	%r12b, -64(%rbp)
	movq	(%r10), %rbx
	cmpq	41096(%r15), %rax
	je	.L982
.L957:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%rbx, (%rax)
	movq	-96(%rbp), %rbx
	movq	%rax, -80(%rbp)
	movq	(%rax), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L958
	movq	%r9, -136(%rbp)
	movq	%r11, -128(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-128(%rbp), %r11
	movq	-136(%rbp), %r9
.L959:
	movl	-120(%rbp), %eax
	movq	%r11, %rsi
	movq	%r9, %rdi
	movq	%r14, -112(%rbp)
	movl	%eax, -104(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_119JSObjectWalkVisitorINS0_26AllocationSiteUsageContextEE13StructureWalkENS0_6HandleINS0_8JSObjectEEE
.L929:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L983
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L931:
	.cfi_restore_state
	movq	41088(%r15), %r10
	cmpq	41096(%r15), %r10
	je	.L984
.L933:
	leaq	8(%r10), %rax
	movq	%rax, 41088(%r15)
	movl	%r12d, %eax
	andl	$1, %eax
	movq	%rsi, (%r10)
	movl	%eax, -120(%rbp)
	testb	$1, %sil
	jne	.L985
.L934:
	testb	$4, %r12b
	jne	.L938
	cmpq	%rsi, _ZN2v88internal3Smi5kZeroE(%rip)
	je	.L986
.L938:
	movl	%r12d, %edx
	movq	%r14, %rsi
	movl	$1, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal12_GLOBAL__N_119CreateObjectLiteralEPNS0_7IsolateENS0_6HandleINS0_28ObjectBoilerplateDescriptionEEEiNS0_14AllocationTypeE
	movl	$1, %esi
	movq	%r15, %rdi
	movq	$0, -88(%rbp)
	movq	%rax, -128(%rbp)
	movq	$0, -80(%rbp)
	movq	%r15, -96(%rbp)
	call	_ZN2v88internal7Factory17NewAllocationSiteEb@PLT
	movq	-96(%rbp), %r14
	movq	-128(%rbp), %r11
	movq	%rax, -88(%rbp)
	movq	(%rax), %rdx
	movq	41088(%r14), %rax
	cmpq	41096(%r14), %rax
	je	.L987
.L943:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%r14)
	movq	%rdx, (%rax)
	movq	-96(%rbp), %r14
	movq	%rax, -80(%rbp)
	movq	-88(%rbp), %rax
	movq	41112(%r14), %rdi
	movq	(%rax), %rsi
	testq	%rdi, %rdi
	je	.L944
	movq	%r11, -128(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-128(%rbp), %r11
	movq	%rax, %r10
.L945:
	cmpb	$0, _ZN2v88internal36FLAG_trace_creation_allocation_sitesE(%rip)
	jne	.L988
.L947:
	leaq	-112(%rbp), %r9
	movq	%r11, %rsi
	leaq	-96(%rbp), %r14
	movq	%r10, -144(%rbp)
	movq	%r9, %rdi
	movq	%r11, -136(%rbp)
	movq	%r9, -128(%rbp)
	movq	%r14, -112(%rbp)
	movl	$0, -104(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_119JSObjectWalkVisitorINS1_29AllocationSiteCreationContextEE13StructureWalkENS0_6HandleINS0_8JSObjectEEE
	movq	-128(%rbp), %r9
	movq	-136(%rbp), %r11
	testq	%rax, %rax
	movq	-144(%rbp), %r10
	je	.L929
	movq	(%r10), %r8
	testq	%r11, %r11
	je	.L949
	movq	(%r11), %rdx
	leaq	7(%r8), %rsi
	movq	%rdx, 7(%r8)
	testb	$1, %dl
	je	.L962
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	8(%rax), %rcx
	movq	%rax, -128(%rbp)
	testl	$262144, %ecx
	jne	.L989
.L951:
	andl	$24, %ecx
	je	.L962
	movq	%r8, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L990
	.p2align 4,,10
	.p2align 3
.L962:
	cmpb	$0, _ZN2v88internal36FLAG_trace_creation_allocation_sitesE(%rip)
	movq	(%r10), %r8
	je	.L949
	movq	-88(%rbp), %rax
	cmpq	%rax, %r10
	je	.L953
	movq	(%rax), %rsi
	cmpq	%rsi, %r8
	je	.L953
	movq	(%r11), %rcx
	movq	%r8, %rdx
	leaq	.LC8(%rip), %rdi
	xorl	%eax, %eax
	movq	%r9, -144(%rbp)
	movq	%r10, -136(%rbp)
	movq	%r11, -128(%rbp)
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-136(%rbp), %r10
	movq	-128(%rbp), %r11
	movq	-144(%rbp), %r9
	movq	(%r10), %r8
	.p2align 4,,10
	.p2align 3
.L949:
	movq	(%rbx), %rbx
	leaq	-1(%rbx,%r13), %r13
	movq	%r8, 0(%r13)
	testb	$1, %r8b
	je	.L936
	cmpl	$3, %r8d
	je	.L936
	movq	%r8, %rax
	movq	%r8, %rdx
	andq	$-262144, %rax
	andq	$-3, %rdx
	movq	8(%rax), %rcx
	movq	%rax, -128(%rbp)
	testl	$262144, %ecx
	jne	.L991
.L955:
	andl	$24, %ecx
	je	.L936
	movq	%rbx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L936
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%r11, -128(%rbp)
	movq	%r9, -144(%rbp)
	movq	%r10, -136(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-144(%rbp), %r9
	movq	-136(%rbp), %r10
	movq	-128(%rbp), %r11
	jmp	.L936
	.p2align 4,,10
	.p2align 3
.L935:
	movq	41088(%r15), %r11
	cmpq	41096(%r15), %r11
	je	.L992
.L937:
	leaq	8(%r11), %rax
	leaq	-112(%rbp), %r9
	movq	%rax, 41088(%r15)
	leaq	-96(%rbp), %r14
	movq	%rsi, (%r11)
	jmp	.L936
	.p2align 4,,10
	.p2align 3
.L986:
	movq	(%rbx), %rax
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%r15, %rdi
	movabsq	$4294967296, %rdx
	movq	%rdx, -1(%r13,%rax)
	movl	%r12d, %edx
	call	_ZN2v88internal12_GLOBAL__N_119CreateObjectLiteralEPNS0_7IsolateENS0_6HandleINS0_28ObjectBoilerplateDescriptionEEEiNS0_14AllocationTypeE
	movq	%rax, %rbx
	movl	-120(%rbp), %eax
	testl	%eax, %eax
	je	.L993
.L941:
	movq	%rbx, %rax
	jmp	.L929
	.p2align 4,,10
	.p2align 3
.L958:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L994
.L960:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L959
	.p2align 4,,10
	.p2align 3
.L980:
	xorl	%ecx, %ecx
	movl	%r8d, %edx
	movq	%r14, %rsi
	call	_ZN2v88internal12_GLOBAL__N_119CreateObjectLiteralEPNS0_7IsolateENS0_6HandleINS0_28ObjectBoilerplateDescriptionEEEiNS0_14AllocationTypeE
	andl	$1, %r12d
	movq	%rax, %r13
	je	.L995
.L928:
	movq	%r13, %rax
	jmp	.L929
	.p2align 4,,10
	.p2align 3
.L981:
	leaq	.LC26(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L984:
	movq	%r15, %rdi
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %rsi
	movq	%rax, %r10
	jmp	.L933
	.p2align 4,,10
	.p2align 3
.L995:
	leaq	-112(%rbp), %rax
	movq	%r13, %rsi
	leaq	-96(%rbp), %rdi
	movq	%r15, -112(%rbp)
	movq	%rax, -96(%rbp)
	movl	$0, -88(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_119JSObjectWalkVisitorINS1_24DeprecationUpdateContextEE13StructureWalkENS0_6HandleINS0_8JSObjectEEE
	testq	%rax, %rax
	cmove	%rbx, %r13
	jmp	.L928
	.p2align 4,,10
	.p2align 3
.L944:
	movq	41088(%r14), %r10
	cmpq	41096(%r14), %r10
	je	.L996
.L946:
	leaq	8(%r10), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%r10)
	jmp	.L945
	.p2align 4,,10
	.p2align 3
.L989:
	movq	%r8, %rdi
	movq	%r9, -176(%rbp)
	movq	%r10, -168(%rbp)
	movq	%r11, -160(%rbp)
	movq	%rdx, -152(%rbp)
	movq	%rsi, -144(%rbp)
	movq	%r8, -136(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-128(%rbp), %rax
	movq	-176(%rbp), %r9
	movq	-168(%rbp), %r10
	movq	-160(%rbp), %r11
	movq	8(%rax), %rcx
	movq	-152(%rbp), %rdx
	movq	-144(%rbp), %rsi
	movq	-136(%rbp), %r8
	jmp	.L951
	.p2align 4,,10
	.p2align 3
.L991:
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%r9, -160(%rbp)
	movq	%r10, -152(%rbp)
	movq	%r11, -144(%rbp)
	movq	%rdx, -136(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-128(%rbp), %rax
	movq	-160(%rbp), %r9
	movq	-152(%rbp), %r10
	movq	-144(%rbp), %r11
	movq	8(%rax), %rcx
	movq	-136(%rbp), %rdx
	jmp	.L955
	.p2align 4,,10
	.p2align 3
.L990:
	movq	%r8, %rdi
	movq	%r9, -144(%rbp)
	movq	%r10, -136(%rbp)
	movq	%r11, -128(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-144(%rbp), %r9
	movq	-136(%rbp), %r10
	movq	-128(%rbp), %r11
	jmp	.L962
	.p2align 4,,10
	.p2align 3
.L993:
	leaq	-112(%rbp), %rax
	movq	%rbx, %rsi
	leaq	-96(%rbp), %rdi
	movq	%r15, -112(%rbp)
	movq	%rax, -96(%rbp)
	movl	$0, -88(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_119JSObjectWalkVisitorINS1_24DeprecationUpdateContextEE13StructureWalkENS0_6HandleINS0_8JSObjectEEE
	testq	%rax, %rax
	movl	$0, %eax
	cmove	%rax, %rbx
	jmp	.L941
	.p2align 4,,10
	.p2align 3
.L953:
	movq	(%r11), %rdx
	movq	%r8, %rsi
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	movq	%r9, -144(%rbp)
	movq	%r10, -136(%rbp)
	movq	%r11, -128(%rbp)
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-136(%rbp), %r10
	movq	-144(%rbp), %r9
	movq	-128(%rbp), %r11
	movq	(%r10), %r8
	jmp	.L949
	.p2align 4,,10
	.p2align 3
.L994:
	movq	%rbx, %rdi
	movq	%r9, -144(%rbp)
	movq	%rsi, -136(%rbp)
	movq	%r11, -128(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-144(%rbp), %r9
	movq	-136(%rbp), %rsi
	movq	-128(%rbp), %r11
	jmp	.L960
	.p2align 4,,10
	.p2align 3
.L988:
	movq	(%r10), %rdx
	leaq	.LC4(%rip), %rsi
	xorl	%eax, %eax
	movq	%r10, -128(%rbp)
	leaq	.LC5(%rip), %rdi
	movq	%r11, -136(%rbp)
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-136(%rbp), %r11
	movq	-128(%rbp), %r10
	jmp	.L947
	.p2align 4,,10
	.p2align 3
.L992:
	movq	%r15, %rdi
	movq	%rsi, -136(%rbp)
	movq	%r10, -128(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rsi
	movq	-128(%rbp), %r10
	movq	%rax, %r11
	jmp	.L937
	.p2align 4,,10
	.p2align 3
.L987:
	movq	%r14, %rdi
	movq	%rdx, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rdx
	movq	-128(%rbp), %r11
	jmp	.L943
	.p2align 4,,10
	.p2align 3
.L982:
	movq	%r15, %rdi
	movq	%r9, -136(%rbp)
	movq	%r11, -128(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %r9
	movq	-128(%rbp), %r11
	jmp	.L957
.L996:
	movq	%r14, %rdi
	movq	%rsi, -136(%rbp)
	movq	%r11, -128(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rsi
	movq	-128(%rbp), %r11
	movq	%rax, %r10
	jmp	.L946
.L983:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21931:
	.size	_ZN2v88internal12_GLOBAL__N_113CreateLiteralINS1_19ObjectLiteralHelperEEENS0_11MaybeHandleINS0_8JSObjectEEEPNS0_7IsolateENS4_INS0_14FeedbackVectorEEEiNS0_6HandleINS0_10HeapObjectEEEi, .-_ZN2v88internal12_GLOBAL__N_113CreateLiteralINS1_19ObjectLiteralHelperEEENS0_11MaybeHandleINS0_8JSObjectEEEPNS0_7IsolateENS4_INS0_14FeedbackVectorEEEiNS0_6HandleINS0_10HeapObjectEEEi
	.section	.rodata._ZN2v88internalL33Stats_Runtime_CreateObjectLiteralEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC27:
	.string	"V8.Runtime_Runtime_CreateObjectLiteral"
	.align 8
.LC28:
	.string	"args[2].IsObjectBoilerplateDescription()"
	.section	.text._ZN2v88internalL33Stats_Runtime_CreateObjectLiteralEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL33Stats_Runtime_CreateObjectLiteralEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL33Stats_Runtime_CreateObjectLiteralEiPmPNS0_7IsolateE.isra.0:
.LFB24301:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1037
.L998:
	movq	_ZZN2v88internalL33Stats_Runtime_CreateObjectLiteralEiPmPNS0_7IsolateEE28trace_event_unique_atomic591(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1038
.L1000:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1039
.L1002:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rcx
	movq	%rcx, %rax
	notq	%rax
	andl	$1, %eax
	jne	.L1040
	movq	-8(%r13), %rdx
	testb	$1, %dl
	jne	.L1041
	movq	-16(%r13), %rsi
	sarq	$32, %rdx
	leaq	-16(%r13), %r9
	testb	$1, %sil
	jne	.L1008
.L1009:
	leaq	.LC28(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1038:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1042
.L1001:
	movq	%rbx, _ZZN2v88internalL33Stats_Runtime_CreateObjectLiteralEiPmPNS0_7IsolateEE28trace_event_unique_atomic591(%rip)
	jmp	.L1000
	.p2align 4,,10
	.p2align 3
.L1039:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1043
.L1003:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1004
	movq	(%rdi), %rax
	call	*8(%rax)
.L1004:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1005
	movq	(%rdi), %rax
	call	*8(%rax)
.L1005:
	leaq	.LC27(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1002
	.p2align 4,,10
	.p2align 3
.L1008:
	movq	-1(%rsi), %rsi
	cmpw	$124, 11(%rsi)
	jne	.L1009
	movq	-24(%r13), %r8
	testb	$1, %r8b
	jne	.L1044
	movq	%rcx, %rsi
	movq	%r12, %rdi
	andq	$-262144, %rsi
	movq	24(%rsi), %rsi
	cmpq	-37504(%rsi), %rcx
	movq	%r9, %rcx
	cmove	%rax, %r13
	shrq	$32, %r8
	movq	%r13, %rsi
	call	_ZN2v88internal12_GLOBAL__N_113CreateLiteralINS1_19ObjectLiteralHelperEEENS0_11MaybeHandleINS0_8JSObjectEEEPNS0_7IsolateENS4_INS0_14FeedbackVectorEEEiNS0_6HandleINS0_10HeapObjectEEEi
	testq	%rax, %rax
	je	.L1045
	movq	(%rax), %r13
.L1013:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1016
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1016:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1046
.L997:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1047
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1045:
	.cfi_restore_state
	movq	312(%r12), %r13
	jmp	.L1013
	.p2align 4,,10
	.p2align 3
.L1040:
	leaq	.LC20(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1037:
	movq	40960(%rsi), %rax
	movl	$390, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L998
	.p2align 4,,10
	.p2align 3
.L1041:
	leaq	.LC16(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1044:
	leaq	.LC22(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1042:
	leaq	.LC13(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1001
	.p2align 4,,10
	.p2align 3
.L1043:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC27(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1003
	.p2align 4,,10
	.p2align 3
.L1046:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L997
.L1047:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24301:
	.size	_ZN2v88internalL33Stats_Runtime_CreateObjectLiteralEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL33Stats_Runtime_CreateObjectLiteralEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal12_GLOBAL__N_113CreateLiteralINS1_18ArrayLiteralHelperEEENS0_11MaybeHandleINS0_8JSObjectEEEPNS0_7IsolateENS4_INS0_14FeedbackVectorEEEiNS0_6HandleINS0_10HeapObjectEEEi,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_113CreateLiteralINS1_18ArrayLiteralHelperEEENS0_11MaybeHandleINS0_8JSObjectEEEPNS0_7IsolateENS4_INS0_14FeedbackVectorEEEiNS0_6HandleINS0_10HeapObjectEEEi, @function
_ZN2v88internal12_GLOBAL__N_113CreateLiteralINS1_18ArrayLiteralHelperEEENS0_11MaybeHandleINS0_8JSObjectEEEPNS0_7IsolateENS4_INS0_14FeedbackVectorEEEiNS0_6HandleINS0_10HeapObjectEEEi:
.LFB21939:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L1104
	movq	(%rsi), %rax
	cmpl	31(%rax), %edx
	jge	.L1105
	leal	48(,%rdx,8), %r13d
	movslq	%r13d, %r13
	movq	-1(%rax,%r13), %rsi
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1055
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r10
	movl	%ebx, %eax
	andl	$1, %eax
	movl	%eax, -120(%rbp)
	testb	$1, %sil
	je	.L1058
.L1109:
	movq	41112(%r15), %rdi
	movq	7(%rsi), %rsi
	testq	%rdi, %rdi
	je	.L1059
	movq	%r10, -128(%rbp)
	leaq	-96(%rbp), %r14
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-128(%rbp), %r10
	leaq	-112(%rbp), %r9
	movq	%rax, %r11
.L1060:
	shrl	%ebx
	movq	%r15, %xmm0
	movq	%r10, %xmm1
	movq	%r10, -72(%rbp)
	xorl	$1, %ebx
	punpcklqdq	%xmm1, %xmm0
	movq	$0, -80(%rbp)
	movq	41088(%r15), %rax
	andl	$1, %ebx
	movaps	%xmm0, -96(%rbp)
	movb	%bl, -64(%rbp)
	movq	(%r10), %rbx
	cmpq	41096(%r15), %rax
	je	.L1106
.L1081:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%rbx, (%rax)
	movq	-96(%rbp), %rbx
	movq	%rax, -80(%rbp)
	movq	(%rax), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1082
	movq	%r9, -136(%rbp)
	movq	%r11, -128(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-128(%rbp), %r11
	movq	-136(%rbp), %r9
.L1083:
	movl	-120(%rbp), %eax
	movq	%r11, %rsi
	movq	%r9, %rdi
	movq	%r14, -112(%rbp)
	movl	%eax, -104(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_119JSObjectWalkVisitorINS0_26AllocationSiteUsageContextEE13StructureWalkENS0_6HandleINS0_8JSObjectEEE
.L1053:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1107
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1055:
	.cfi_restore_state
	movq	41088(%r15), %r10
	cmpq	41096(%r15), %r10
	je	.L1108
.L1057:
	leaq	8(%r10), %rax
	movq	%rax, 41088(%r15)
	movl	%ebx, %eax
	andl	$1, %eax
	movq	%rsi, (%r10)
	movl	%eax, -120(%rbp)
	testb	$1, %sil
	jne	.L1109
.L1058:
	testb	$4, %bl
	jne	.L1062
	cmpq	%rsi, _ZN2v88internal3Smi5kZeroE(%rip)
	je	.L1110
.L1062:
	movl	$1, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal12_GLOBAL__N_118CreateArrayLiteralEPNS0_7IsolateENS0_6HandleINS0_27ArrayBoilerplateDescriptionEEENS0_14AllocationTypeE
	movl	$1, %esi
	movq	%r15, %rdi
	movq	$0, -88(%rbp)
	movq	%rax, -128(%rbp)
	movq	$0, -80(%rbp)
	movq	%r15, -96(%rbp)
	call	_ZN2v88internal7Factory17NewAllocationSiteEb@PLT
	movq	-96(%rbp), %r14
	movq	-128(%rbp), %r11
	movq	%rax, -88(%rbp)
	movq	(%rax), %rdx
	movq	41088(%r14), %rax
	cmpq	41096(%r14), %rax
	je	.L1111
.L1067:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%r14)
	movq	%rdx, (%rax)
	movq	-96(%rbp), %r14
	movq	%rax, -80(%rbp)
	movq	-88(%rbp), %rax
	movq	41112(%r14), %rdi
	movq	(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1068
	movq	%r11, -128(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-128(%rbp), %r11
	movq	%rax, %r10
.L1069:
	cmpb	$0, _ZN2v88internal36FLAG_trace_creation_allocation_sitesE(%rip)
	jne	.L1112
.L1071:
	leaq	-112(%rbp), %r9
	movq	%r11, %rsi
	leaq	-96(%rbp), %r14
	movq	%r10, -144(%rbp)
	movq	%r9, %rdi
	movq	%r11, -136(%rbp)
	movq	%r9, -128(%rbp)
	movq	%r14, -112(%rbp)
	movl	$0, -104(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_119JSObjectWalkVisitorINS1_29AllocationSiteCreationContextEE13StructureWalkENS0_6HandleINS0_8JSObjectEEE
	movq	-128(%rbp), %r9
	movq	-136(%rbp), %r11
	testq	%rax, %rax
	movq	-144(%rbp), %r10
	je	.L1053
	movq	(%r10), %r8
	testq	%r11, %r11
	je	.L1073
	movq	(%r11), %rdx
	leaq	7(%r8), %rsi
	movq	%rdx, 7(%r8)
	testb	$1, %dl
	je	.L1086
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	8(%rax), %rcx
	movq	%rax, -128(%rbp)
	testl	$262144, %ecx
	jne	.L1113
.L1075:
	andl	$24, %ecx
	je	.L1086
	movq	%r8, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1114
	.p2align 4,,10
	.p2align 3
.L1086:
	cmpb	$0, _ZN2v88internal36FLAG_trace_creation_allocation_sitesE(%rip)
	movq	(%r10), %r8
	je	.L1073
	movq	-88(%rbp), %rax
	cmpq	%rax, %r10
	je	.L1077
	movq	(%rax), %rsi
	cmpq	%rsi, %r8
	je	.L1077
	movq	(%r11), %rcx
	movq	%r8, %rdx
	leaq	.LC8(%rip), %rdi
	xorl	%eax, %eax
	movq	%r9, -144(%rbp)
	movq	%r10, -136(%rbp)
	movq	%r11, -128(%rbp)
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-136(%rbp), %r10
	movq	-128(%rbp), %r11
	movq	-144(%rbp), %r9
	movq	(%r10), %r8
	.p2align 4,,10
	.p2align 3
.L1073:
	movq	(%r12), %r12
	leaq	-1(%r12,%r13), %r13
	movq	%r8, 0(%r13)
	testb	$1, %r8b
	je	.L1060
	cmpl	$3, %r8d
	je	.L1060
	movq	%r8, %rax
	movq	%r8, %rdx
	andq	$-262144, %rax
	andq	$-3, %rdx
	movq	8(%rax), %rcx
	movq	%rax, -128(%rbp)
	testl	$262144, %ecx
	jne	.L1115
.L1079:
	andl	$24, %ecx
	je	.L1060
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1060
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r11, -128(%rbp)
	movq	%r9, -144(%rbp)
	movq	%r10, -136(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-144(%rbp), %r9
	movq	-136(%rbp), %r10
	movq	-128(%rbp), %r11
	jmp	.L1060
	.p2align 4,,10
	.p2align 3
.L1059:
	movq	41088(%r15), %r11
	cmpq	41096(%r15), %r11
	je	.L1116
.L1061:
	leaq	8(%r11), %rax
	leaq	-112(%rbp), %r9
	movq	%rax, 41088(%r15)
	leaq	-96(%rbp), %r14
	movq	%rsi, (%r11)
	jmp	.L1060
	.p2align 4,,10
	.p2align 3
.L1110:
	movq	(%r12), %rax
	movq	%r14, %rsi
	movq	%r15, %rdi
	movabsq	$4294967296, %rdx
	movq	%rdx, -1(%r13,%rax)
	xorl	%edx, %edx
	call	_ZN2v88internal12_GLOBAL__N_118CreateArrayLiteralEPNS0_7IsolateENS0_6HandleINS0_27ArrayBoilerplateDescriptionEEENS0_14AllocationTypeE
	movq	%rax, %rbx
	movl	-120(%rbp), %eax
	testl	%eax, %eax
	je	.L1117
.L1065:
	movq	%rbx, %rax
	jmp	.L1053
	.p2align 4,,10
	.p2align 3
.L1082:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L1118
.L1084:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L1083
	.p2align 4,,10
	.p2align 3
.L1104:
	xorl	%edx, %edx
	movq	%rcx, %rsi
	call	_ZN2v88internal12_GLOBAL__N_118CreateArrayLiteralEPNS0_7IsolateENS0_6HandleINS0_27ArrayBoilerplateDescriptionEEENS0_14AllocationTypeE
	andl	$1, %ebx
	movq	%rax, %r13
	je	.L1119
.L1052:
	movq	%r13, %rax
	jmp	.L1053
	.p2align 4,,10
	.p2align 3
.L1105:
	leaq	.LC26(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1108:
	movq	%r15, %rdi
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %rsi
	movq	%rax, %r10
	jmp	.L1057
	.p2align 4,,10
	.p2align 3
.L1119:
	leaq	-112(%rbp), %rax
	movq	%r13, %rsi
	leaq	-96(%rbp), %rdi
	movq	%r15, -112(%rbp)
	movq	%rax, -96(%rbp)
	movl	$0, -88(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_119JSObjectWalkVisitorINS1_24DeprecationUpdateContextEE13StructureWalkENS0_6HandleINS0_8JSObjectEEE
	testq	%rax, %rax
	cmove	%r12, %r13
	jmp	.L1052
	.p2align 4,,10
	.p2align 3
.L1068:
	movq	41088(%r14), %r10
	cmpq	41096(%r14), %r10
	je	.L1120
.L1070:
	leaq	8(%r10), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%r10)
	jmp	.L1069
	.p2align 4,,10
	.p2align 3
.L1113:
	movq	%r8, %rdi
	movq	%r9, -176(%rbp)
	movq	%r10, -168(%rbp)
	movq	%r11, -160(%rbp)
	movq	%rdx, -152(%rbp)
	movq	%rsi, -144(%rbp)
	movq	%r8, -136(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-128(%rbp), %rax
	movq	-176(%rbp), %r9
	movq	-168(%rbp), %r10
	movq	-160(%rbp), %r11
	movq	8(%rax), %rcx
	movq	-152(%rbp), %rdx
	movq	-144(%rbp), %rsi
	movq	-136(%rbp), %r8
	jmp	.L1075
	.p2align 4,,10
	.p2align 3
.L1115:
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r9, -160(%rbp)
	movq	%r10, -152(%rbp)
	movq	%r11, -144(%rbp)
	movq	%rdx, -136(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-128(%rbp), %rax
	movq	-160(%rbp), %r9
	movq	-152(%rbp), %r10
	movq	-144(%rbp), %r11
	movq	8(%rax), %rcx
	movq	-136(%rbp), %rdx
	jmp	.L1079
	.p2align 4,,10
	.p2align 3
.L1114:
	movq	%r8, %rdi
	movq	%r9, -144(%rbp)
	movq	%r10, -136(%rbp)
	movq	%r11, -128(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-144(%rbp), %r9
	movq	-136(%rbp), %r10
	movq	-128(%rbp), %r11
	jmp	.L1086
	.p2align 4,,10
	.p2align 3
.L1117:
	leaq	-112(%rbp), %rax
	movq	%rbx, %rsi
	leaq	-96(%rbp), %rdi
	movq	%r15, -112(%rbp)
	movq	%rax, -96(%rbp)
	movl	$0, -88(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_119JSObjectWalkVisitorINS1_24DeprecationUpdateContextEE13StructureWalkENS0_6HandleINS0_8JSObjectEEE
	testq	%rax, %rax
	movl	$0, %eax
	cmove	%rax, %rbx
	jmp	.L1065
	.p2align 4,,10
	.p2align 3
.L1077:
	movq	(%r11), %rdx
	movq	%r8, %rsi
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	movq	%r9, -144(%rbp)
	movq	%r10, -136(%rbp)
	movq	%r11, -128(%rbp)
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-136(%rbp), %r10
	movq	-144(%rbp), %r9
	movq	-128(%rbp), %r11
	movq	(%r10), %r8
	jmp	.L1073
	.p2align 4,,10
	.p2align 3
.L1118:
	movq	%rbx, %rdi
	movq	%r9, -144(%rbp)
	movq	%rsi, -136(%rbp)
	movq	%r11, -128(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-144(%rbp), %r9
	movq	-136(%rbp), %rsi
	movq	-128(%rbp), %r11
	jmp	.L1084
	.p2align 4,,10
	.p2align 3
.L1112:
	movq	(%r10), %rdx
	leaq	.LC4(%rip), %rsi
	xorl	%eax, %eax
	movq	%r10, -128(%rbp)
	leaq	.LC5(%rip), %rdi
	movq	%r11, -136(%rbp)
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-136(%rbp), %r11
	movq	-128(%rbp), %r10
	jmp	.L1071
	.p2align 4,,10
	.p2align 3
.L1116:
	movq	%r15, %rdi
	movq	%rsi, -136(%rbp)
	movq	%r10, -128(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rsi
	movq	-128(%rbp), %r10
	movq	%rax, %r11
	jmp	.L1061
	.p2align 4,,10
	.p2align 3
.L1111:
	movq	%r14, %rdi
	movq	%rdx, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rdx
	movq	-128(%rbp), %r11
	jmp	.L1067
	.p2align 4,,10
	.p2align 3
.L1106:
	movq	%r15, %rdi
	movq	%r9, -136(%rbp)
	movq	%r11, -128(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %r9
	movq	-128(%rbp), %r11
	jmp	.L1081
.L1120:
	movq	%r14, %rdi
	movq	%rsi, -136(%rbp)
	movq	%r11, -128(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rsi
	movq	-128(%rbp), %r11
	movq	%rax, %r10
	jmp	.L1070
.L1107:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21939:
	.size	_ZN2v88internal12_GLOBAL__N_113CreateLiteralINS1_18ArrayLiteralHelperEEENS0_11MaybeHandleINS0_8JSObjectEEEPNS0_7IsolateENS4_INS0_14FeedbackVectorEEEiNS0_6HandleINS0_10HeapObjectEEEi, .-_ZN2v88internal12_GLOBAL__N_113CreateLiteralINS1_18ArrayLiteralHelperEEENS0_11MaybeHandleINS0_8JSObjectEEEPNS0_7IsolateENS4_INS0_14FeedbackVectorEEEiNS0_6HandleINS0_10HeapObjectEEEi
	.section	.rodata._ZN2v88internalL32Stats_Runtime_CreateArrayLiteralEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC29:
	.string	"V8.Runtime_Runtime_CreateArrayLiteral"
	.align 8
.LC30:
	.string	"args[2].IsArrayBoilerplateDescription()"
	.section	.text._ZN2v88internalL32Stats_Runtime_CreateArrayLiteralEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL32Stats_Runtime_CreateArrayLiteralEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL32Stats_Runtime_CreateArrayLiteralEiPmPNS0_7IsolateE.isra.0:
.LFB24304:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1161
.L1122:
	movq	_ZZN2v88internalL32Stats_Runtime_CreateArrayLiteralEiPmPNS0_7IsolateEE28trace_event_unique_atomic628(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1162
.L1124:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1163
.L1126:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rcx
	movq	%rcx, %rax
	notq	%rax
	andl	$1, %eax
	jne	.L1164
	movq	-8(%r13), %rdx
	testb	$1, %dl
	jne	.L1165
	movq	-16(%r13), %rsi
	sarq	$32, %rdx
	leaq	-16(%r13), %r9
	testb	$1, %sil
	jne	.L1132
.L1133:
	leaq	.LC30(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1162:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1166
.L1125:
	movq	%rbx, _ZZN2v88internalL32Stats_Runtime_CreateArrayLiteralEiPmPNS0_7IsolateEE28trace_event_unique_atomic628(%rip)
	jmp	.L1124
	.p2align 4,,10
	.p2align 3
.L1163:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1167
.L1127:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1128
	movq	(%rdi), %rax
	call	*8(%rax)
.L1128:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1129
	movq	(%rdi), %rax
	call	*8(%rax)
.L1129:
	leaq	.LC29(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1126
	.p2align 4,,10
	.p2align 3
.L1132:
	movq	-1(%rsi), %rsi
	cmpw	$82, 11(%rsi)
	jne	.L1133
	movq	-24(%r13), %r8
	testb	$1, %r8b
	jne	.L1168
	movq	%rcx, %rsi
	movq	%r12, %rdi
	andq	$-262144, %rsi
	movq	24(%rsi), %rsi
	cmpq	-37504(%rsi), %rcx
	movq	%r9, %rcx
	cmove	%rax, %r13
	shrq	$32, %r8
	movq	%r13, %rsi
	call	_ZN2v88internal12_GLOBAL__N_113CreateLiteralINS1_18ArrayLiteralHelperEEENS0_11MaybeHandleINS0_8JSObjectEEEPNS0_7IsolateENS4_INS0_14FeedbackVectorEEEiNS0_6HandleINS0_10HeapObjectEEEi
	testq	%rax, %rax
	je	.L1169
	movq	(%rax), %r13
.L1137:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1140
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1140:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1170
.L1121:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1171
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1169:
	.cfi_restore_state
	movq	312(%r12), %r13
	jmp	.L1137
	.p2align 4,,10
	.p2align 3
.L1164:
	leaq	.LC20(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1161:
	movq	40960(%rsi), %rax
	movl	$388, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1122
	.p2align 4,,10
	.p2align 3
.L1165:
	leaq	.LC16(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1168:
	leaq	.LC22(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1166:
	leaq	.LC13(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1125
	.p2align 4,,10
	.p2align 3
.L1167:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC29(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1127
	.p2align 4,,10
	.p2align 3
.L1170:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1121
.L1171:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24304:
	.size	_ZN2v88internalL32Stats_Runtime_CreateArrayLiteralEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL32Stats_Runtime_CreateArrayLiteralEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal27Runtime_CreateObjectLiteralEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal27Runtime_CreateObjectLiteralEiPmPNS0_7IsolateE
	.type	_ZN2v88internal27Runtime_CreateObjectLiteralEiPmPNS0_7IsolateE, @function
_ZN2v88internal27Runtime_CreateObjectLiteralEiPmPNS0_7IsolateE:
.LFB19796:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1186
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rcx
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	movq	%rcx, %rax
	notq	%rax
	andl	$1, %eax
	jne	.L1187
	movq	-8(%rsi), %rdx
	testb	$1, %dl
	jne	.L1188
	leaq	-16(%rsi), %r10
	movq	-16(%rsi), %rsi
	sarq	$32, %rdx
	testb	$1, %sil
	jne	.L1176
.L1177:
	leaq	.LC28(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1176:
	movq	-1(%rsi), %rsi
	cmpw	$124, 11(%rsi)
	jne	.L1177
	movq	-24(%r9), %r8
	testb	$1, %r8b
	jne	.L1189
	movq	%rcx, %rsi
	movq	%r12, %rdi
	andq	$-262144, %rsi
	movq	24(%rsi), %rsi
	cmpq	-37504(%rsi), %rcx
	movq	%r10, %rcx
	cmove	%rax, %r9
	shrq	$32, %r8
	movq	%r9, %rsi
	call	_ZN2v88internal12_GLOBAL__N_113CreateLiteralINS1_19ObjectLiteralHelperEEENS0_11MaybeHandleINS0_8JSObjectEEEPNS0_7IsolateENS4_INS0_14FeedbackVectorEEEiNS0_6HandleINS0_10HeapObjectEEEi
	testq	%rax, %rax
	je	.L1190
	movq	(%rax), %r14
.L1181:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1172
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1172:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1190:
	.cfi_restore_state
	movq	312(%r12), %r14
	jmp	.L1181
	.p2align 4,,10
	.p2align 3
.L1186:
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	movq	%r9, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL33Stats_Runtime_CreateObjectLiteralEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L1187:
	.cfi_restore_state
	leaq	.LC20(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1188:
	leaq	.LC16(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1189:
	leaq	.LC22(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19796:
	.size	_ZN2v88internal27Runtime_CreateObjectLiteralEiPmPNS0_7IsolateE, .-_ZN2v88internal27Runtime_CreateObjectLiteralEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal48Runtime_CreateObjectLiteralWithoutAllocationSiteEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal48Runtime_CreateObjectLiteralWithoutAllocationSiteEiPmPNS0_7IsolateE
	.type	_ZN2v88internal48Runtime_CreateObjectLiteralWithoutAllocationSiteEiPmPNS0_7IsolateE, @function
_ZN2v88internal48Runtime_CreateObjectLiteralWithoutAllocationSiteEiPmPNS0_7IsolateE:
.LFB19799:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1206
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L1194
.L1195:
	leaq	.LC15(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1194:
	movq	-1(%rax), %rax
	cmpw	$124, 11(%rax)
	jne	.L1195
	movq	-8(%rsi), %rax
	testb	$1, %al
	jne	.L1207
	sarq	$32, %rax
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	%rax, %r14
	movl	%eax, %edx
	call	_ZN2v88internal12_GLOBAL__N_119CreateObjectLiteralEPNS0_7IsolateENS0_6HandleINS0_28ObjectBoilerplateDescriptionEEEiNS0_14AllocationTypeE
	andl	$1, %r14d
	movq	%rax, %r15
	je	.L1208
.L1197:
	testq	%r15, %r15
	je	.L1199
	movq	(%r15), %r14
.L1201:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1191
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1191:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1209
	addq	$56, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1208:
	.cfi_restore_state
	leaq	-88(%rbp), %rax
	leaq	-80(%rbp), %rdi
	movq	%r15, %rsi
	movq	%r12, -88(%rbp)
	movq	%rax, -80(%rbp)
	movl	$0, -72(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_119JSObjectWalkVisitorINS1_24DeprecationUpdateContextEE13StructureWalkENS0_6HandleINS0_8JSObjectEEE
	testq	%rax, %rax
	jne	.L1197
.L1199:
	movq	312(%r12), %r14
	jmp	.L1201
	.p2align 4,,10
	.p2align 3
.L1206:
	movq	%rdx, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internalL54Stats_Runtime_CreateObjectLiteralWithoutAllocationSiteEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r14
	jmp	.L1191
	.p2align 4,,10
	.p2align 3
.L1207:
	leaq	.LC16(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1209:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19799:
	.size	_ZN2v88internal48Runtime_CreateObjectLiteralWithoutAllocationSiteEiPmPNS0_7IsolateE, .-_ZN2v88internal48Runtime_CreateObjectLiteralWithoutAllocationSiteEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal47Runtime_CreateArrayLiteralWithoutAllocationSiteEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal47Runtime_CreateArrayLiteralWithoutAllocationSiteEiPmPNS0_7IsolateE
	.type	_ZN2v88internal47Runtime_CreateArrayLiteralWithoutAllocationSiteEiPmPNS0_7IsolateE, @function
_ZN2v88internal47Runtime_CreateArrayLiteralWithoutAllocationSiteEiPmPNS0_7IsolateE:
.LFB19802:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1225
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %r13
	testb	$1, %al
	jne	.L1213
.L1214:
	leaq	.LC18(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1213:
	movq	-1(%rax), %rax
	cmpw	$82, 11(%rax)
	jne	.L1214
	movq	-8(%rsi), %r15
	testb	$1, %r15b
	jne	.L1226
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_118CreateArrayLiteralEPNS0_7IsolateENS0_6HandleINS0_27ArrayBoilerplateDescriptionEEENS0_14AllocationTypeE
	btq	$32, %r15
	movq	%rax, %rbx
	jnc	.L1227
.L1216:
	testq	%rbx, %rbx
	je	.L1218
	movq	(%rbx), %r15
.L1220:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L1210
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1210:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1228
	addq	$56, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1227:
	.cfi_restore_state
	leaq	-88(%rbp), %rax
	leaq	-80(%rbp), %rdi
	movq	%rbx, %rsi
	movq	%r12, -88(%rbp)
	movq	%rax, -80(%rbp)
	movl	$0, -72(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_119JSObjectWalkVisitorINS1_24DeprecationUpdateContextEE13StructureWalkENS0_6HandleINS0_8JSObjectEEE
	testq	%rax, %rax
	jne	.L1216
.L1218:
	movq	312(%r12), %r15
	jmp	.L1220
	.p2align 4,,10
	.p2align 3
.L1225:
	movq	%rdx, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internalL53Stats_Runtime_CreateArrayLiteralWithoutAllocationSiteEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r15
	jmp	.L1210
	.p2align 4,,10
	.p2align 3
.L1226:
	leaq	.LC16(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1228:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19802:
	.size	_ZN2v88internal47Runtime_CreateArrayLiteralWithoutAllocationSiteEiPmPNS0_7IsolateE, .-_ZN2v88internal47Runtime_CreateArrayLiteralWithoutAllocationSiteEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal26Runtime_CreateArrayLiteralEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal26Runtime_CreateArrayLiteralEiPmPNS0_7IsolateE
	.type	_ZN2v88internal26Runtime_CreateArrayLiteralEiPmPNS0_7IsolateE, @function
_ZN2v88internal26Runtime_CreateArrayLiteralEiPmPNS0_7IsolateE:
.LFB19805:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1243
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rcx
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	movq	%rcx, %rax
	notq	%rax
	andl	$1, %eax
	jne	.L1244
	movq	-8(%rsi), %rdx
	testb	$1, %dl
	jne	.L1245
	leaq	-16(%rsi), %r10
	movq	-16(%rsi), %rsi
	sarq	$32, %rdx
	testb	$1, %sil
	jne	.L1233
.L1234:
	leaq	.LC30(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1233:
	movq	-1(%rsi), %rsi
	cmpw	$82, 11(%rsi)
	jne	.L1234
	movq	-24(%r9), %r8
	testb	$1, %r8b
	jne	.L1246
	movq	%rcx, %rsi
	movq	%r12, %rdi
	andq	$-262144, %rsi
	movq	24(%rsi), %rsi
	cmpq	-37504(%rsi), %rcx
	movq	%r10, %rcx
	cmove	%rax, %r9
	shrq	$32, %r8
	movq	%r9, %rsi
	call	_ZN2v88internal12_GLOBAL__N_113CreateLiteralINS1_18ArrayLiteralHelperEEENS0_11MaybeHandleINS0_8JSObjectEEEPNS0_7IsolateENS4_INS0_14FeedbackVectorEEEiNS0_6HandleINS0_10HeapObjectEEEi
	testq	%rax, %rax
	je	.L1247
	movq	(%rax), %r14
.L1238:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1229
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1229:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1247:
	.cfi_restore_state
	movq	312(%r12), %r14
	jmp	.L1238
	.p2align 4,,10
	.p2align 3
.L1243:
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	movq	%r9, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL32Stats_Runtime_CreateArrayLiteralEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L1244:
	.cfi_restore_state
	leaq	.LC20(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1245:
	leaq	.LC16(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1246:
	leaq	.LC22(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19805:
	.size	_ZN2v88internal26Runtime_CreateArrayLiteralEiPmPNS0_7IsolateE, .-_ZN2v88internal26Runtime_CreateArrayLiteralEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal27Runtime_CreateRegExpLiteralEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal27Runtime_CreateRegExpLiteralEiPmPNS0_7IsolateE
	.type	_ZN2v88internal27Runtime_CreateRegExpLiteralEiPmPNS0_7IsolateE, @function
_ZN2v88internal27Runtime_CreateRegExpLiteralEiPmPNS0_7IsolateE:
.LFB19808:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1281
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	je	.L1282
	movq	-8(%rsi), %rcx
	testb	$1, %cl
	jne	.L1283
	movq	-16(%rsi), %rdx
	leaq	-16(%rsi), %r15
	testb	$1, %dl
	jne	.L1284
.L1252:
	leaq	.LC21(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1284:
	movq	-1(%rdx), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L1252
	movq	-24(%rsi), %rdx
	testb	$1, %dl
	jne	.L1285
	movq	%rax, %rsi
	sarq	$32, %rdx
	andq	$-262144, %rsi
	movq	24(%rsi), %rsi
	cmpq	-37504(%rsi), %rax
	jne	.L1255
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8JSRegExp3NewEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS_4base5FlagsINS1_4FlagEiEE@PLT
	testq	%rax, %rax
	je	.L1280
.L1279:
	movq	(%rax), %r13
.L1257:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1248
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1248:
	addq	$40, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1255:
	.cfi_restore_state
	sarq	$32, %rcx
	leal	48(,%rcx,8), %ecx
	movslq	%ecx, %rcx
	movq	-1(%rax,%rcx), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1258
	movq	%rdx, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rdx
	movq	(%rax), %rsi
	movq	%rax, %r8
.L1259:
	andl	$1, %esi
	movq	%rcx, -64(%rbp)
	movq	%r8, -56(%rbp)
	jne	.L1270
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8JSRegExp3NewEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS_4base5FlagsINS1_4FlagEiEE@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %rcx
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L1280
	movq	0(%r13), %rdi
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %rax
	leaq	-1(%rcx,%rdi), %rsi
	cmpq	%rax, (%r8)
	je	.L1286
	movq	(%r15), %rax
	movq	%rax, (%rsi)
	testb	$1, %al
	je	.L1261
	cmpl	$3, %eax
	je	.L1261
	movq	%rax, %rdx
	andq	$-262144, %rax
	movq	%rax, %r13
	movq	8(%rax), %rax
	andq	$-3, %rdx
	testl	$262144, %eax
	je	.L1265
	movq	%rdx, -72(%rbp)
	movq	%rsi, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r13), %rax
	movq	-72(%rbp), %rdx
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
.L1265:
	testb	$24, %al
	je	.L1261
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1261
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1261
	.p2align 4,,10
	.p2align 3
.L1281:
	addq	$40, %rsp
	movq	%r13, %rdi
	movq	%rdx, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL33Stats_Runtime_CreateRegExpLiteralEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L1270:
	.cfi_restore_state
	xorl	%r15d, %r15d
.L1261:
	movq	%r15, %rdi
	call	_ZN2v88internal8JSRegExp4CopyENS0_6HandleIS1_EE@PLT
	jmp	.L1279
	.p2align 4,,10
	.p2align 3
.L1258:
	movq	%r14, %r8
	cmpq	41096(%r12), %r14
	je	.L1287
.L1260:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r8)
	jmp	.L1259
	.p2align 4,,10
	.p2align 3
.L1282:
	leaq	.LC20(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1280:
	movq	312(%r12), %r13
	jmp	.L1257
	.p2align 4,,10
	.p2align 3
.L1283:
	leaq	.LC16(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1285:
	leaq	.LC22(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1286:
	movabsq	$4294967296, %rax
	movq	%rax, (%rsi)
	movq	(%r15), %r13
	jmp	.L1257
	.p2align 4,,10
	.p2align 3
.L1287:
	movq	%r12, %rdi
	movq	%rdx, -72(%rbp)
	movq	%rsi, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rdx
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rcx
	movq	%rax, %r8
	jmp	.L1260
	.cfi_endproc
.LFE19808:
	.size	_ZN2v88internal27Runtime_CreateRegExpLiteralEiPmPNS0_7IsolateE, .-_ZN2v88internal27Runtime_CreateRegExpLiteralEiPmPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal27Runtime_CreateObjectLiteralEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal27Runtime_CreateObjectLiteralEiPmPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal27Runtime_CreateObjectLiteralEiPmPNS0_7IsolateE:
.LFB24248:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE24248:
	.size	_GLOBAL__sub_I__ZN2v88internal27Runtime_CreateObjectLiteralEiPmPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal27Runtime_CreateObjectLiteralEiPmPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal27Runtime_CreateObjectLiteralEiPmPNS0_7IsolateE
	.section	.bss._ZZN2v88internalL33Stats_Runtime_CreateRegExpLiteralEiPmPNS0_7IsolateEE28trace_event_unique_atomic645,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL33Stats_Runtime_CreateRegExpLiteralEiPmPNS0_7IsolateEE28trace_event_unique_atomic645, @object
	.size	_ZZN2v88internalL33Stats_Runtime_CreateRegExpLiteralEiPmPNS0_7IsolateEE28trace_event_unique_atomic645, 8
_ZZN2v88internalL33Stats_Runtime_CreateRegExpLiteralEiPmPNS0_7IsolateEE28trace_event_unique_atomic645:
	.zero	8
	.section	.bss._ZZN2v88internalL32Stats_Runtime_CreateArrayLiteralEiPmPNS0_7IsolateEE28trace_event_unique_atomic628,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL32Stats_Runtime_CreateArrayLiteralEiPmPNS0_7IsolateEE28trace_event_unique_atomic628, @object
	.size	_ZZN2v88internalL32Stats_Runtime_CreateArrayLiteralEiPmPNS0_7IsolateEE28trace_event_unique_atomic628, 8
_ZZN2v88internalL32Stats_Runtime_CreateArrayLiteralEiPmPNS0_7IsolateEE28trace_event_unique_atomic628:
	.zero	8
	.section	.bss._ZZN2v88internalL53Stats_Runtime_CreateArrayLiteralWithoutAllocationSiteEiPmPNS0_7IsolateEE28trace_event_unique_atomic618,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL53Stats_Runtime_CreateArrayLiteralWithoutAllocationSiteEiPmPNS0_7IsolateEE28trace_event_unique_atomic618, @object
	.size	_ZZN2v88internalL53Stats_Runtime_CreateArrayLiteralWithoutAllocationSiteEiPmPNS0_7IsolateEE28trace_event_unique_atomic618, 8
_ZZN2v88internalL53Stats_Runtime_CreateArrayLiteralWithoutAllocationSiteEiPmPNS0_7IsolateEE28trace_event_unique_atomic618:
	.zero	8
	.section	.bss._ZZN2v88internalL54Stats_Runtime_CreateObjectLiteralWithoutAllocationSiteEiPmPNS0_7IsolateEE28trace_event_unique_atomic608,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL54Stats_Runtime_CreateObjectLiteralWithoutAllocationSiteEiPmPNS0_7IsolateEE28trace_event_unique_atomic608, @object
	.size	_ZZN2v88internalL54Stats_Runtime_CreateObjectLiteralWithoutAllocationSiteEiPmPNS0_7IsolateEE28trace_event_unique_atomic608, 8
_ZZN2v88internalL54Stats_Runtime_CreateObjectLiteralWithoutAllocationSiteEiPmPNS0_7IsolateEE28trace_event_unique_atomic608:
	.zero	8
	.section	.bss._ZZN2v88internalL33Stats_Runtime_CreateObjectLiteralEiPmPNS0_7IsolateEE28trace_event_unique_atomic591,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL33Stats_Runtime_CreateObjectLiteralEiPmPNS0_7IsolateEE28trace_event_unique_atomic591, @object
	.size	_ZZN2v88internalL33Stats_Runtime_CreateObjectLiteralEiPmPNS0_7IsolateEE28trace_event_unique_atomic591, 8
_ZZN2v88internalL33Stats_Runtime_CreateObjectLiteralEiPmPNS0_7IsolateEE28trace_event_unique_atomic591:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC10:
	.long	0
	.long	1127219200
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
