	.file	"wasm-interpreter.cc"
	.text
	.section	.text._ZN2v88internal4wasm7Decoder12onFirstErrorEv,"axG",@progbits,_ZN2v88internal4wasm7Decoder12onFirstErrorEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm7Decoder12onFirstErrorEv
	.type	_ZN2v88internal4wasm7Decoder12onFirstErrorEv, @function
_ZN2v88internal4wasm7Decoder12onFirstErrorEv:
.LFB19254:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE19254:
	.size	_ZN2v88internal4wasm7Decoder12onFirstErrorEv, .-_ZN2v88internal4wasm7Decoder12onFirstErrorEv
	.section	.text._ZN2v88internal4wasm7DecoderD2Ev,"axG",@progbits,_ZN2v88internal4wasm7DecoderD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm7DecoderD2Ev
	.type	_ZN2v88internal4wasm7DecoderD2Ev, @function
_ZN2v88internal4wasm7DecoderD2Ev:
.LFB25818:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %r8
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %rax
	addq	$64, %rdi
	movq	%rax, -64(%rdi)
	cmpq	%rdi, %r8
	je	.L3
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L3:
	ret
	.cfi_endproc
.LFE25818:
	.size	_ZN2v88internal4wasm7DecoderD2Ev, .-_ZN2v88internal4wasm7DecoderD2Ev
	.weak	_ZN2v88internal4wasm7DecoderD1Ev
	.set	_ZN2v88internal4wasm7DecoderD1Ev,_ZN2v88internal4wasm7DecoderD2Ev
	.section	.text._ZN2v88internal4wasm7DecoderD0Ev,"axG",@progbits,_ZN2v88internal4wasm7DecoderD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm7DecoderD0Ev
	.type	_ZN2v88internal4wasm7DecoderD0Ev, @function
_ZN2v88internal4wasm7DecoderD0Ev:
.LFB25820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	48(%rdi), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L6
	call	_ZdlPv@PLT
.L6:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$80, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE25820:
	.size	_ZN2v88internal4wasm7DecoderD0Ev, .-_ZN2v88internal4wasm7DecoderD0Ev
	.section	.text._ZN2v88internal4wasm12_GLOBAL__N_112NopFinalizerERKNS_16WeakCallbackInfoIvEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal4wasm12_GLOBAL__N_112NopFinalizerERKNS_16WeakCallbackInfoIvEE, @function
_ZN2v88internal4wasm12_GLOBAL__N_112NopFinalizerERKNS_16WeakCallbackInfoIvEE:
.LFB20338:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	jmp	_ZN2v88internal13GlobalHandles7DestroyEPm@PLT
	.cfi_endproc
.LFE20338:
	.size	_ZN2v88internal4wasm12_GLOBAL__N_112NopFinalizerERKNS_16WeakCallbackInfoIvEE, .-_ZN2v88internal4wasm12_GLOBAL__N_112NopFinalizerERKNS_16WeakCallbackInfoIvEE
	.section	.text._ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE0ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi1EEET_PKhPjPKcS7_.isra.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE0ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi1EEET_PKhPjPKcS7_.isra.0, @function
_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE0ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi1EEET_PKhPjPKcS7_.isra.0:
.LFB26337:
	.cfi_startproc
	movzbl	(%rdi), %ecx
	movl	%ecx, %eax
	sall	$7, %eax
	andl	$16256, %eax
	orl	%edx, %eax
	testb	%cl, %cl
	js	.L14
	movl	$2, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movzbl	1(%rdi), %ecx
	movl	%ecx, %edx
	sall	$14, %edx
	andl	$2080768, %edx
	orl	%edx, %eax
	testb	%cl, %cl
	js	.L15
	movl	$3, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	movzbl	2(%rdi), %ecx
	movl	%ecx, %edx
	sall	$21, %edx
	andl	$266338304, %edx
	orl	%edx, %eax
	testb	%cl, %cl
	js	.L16
	movl	$4, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	movzbl	3(%rdi), %edx
	movl	$5, (%rsi)
	sall	$28, %edx
	orl	%edx, %eax
	ret
	.cfi_endproc
.LFE26337:
	.size	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE0ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi1EEET_PKhPjPKcS7_.isra.0, .-_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE0ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi1EEET_PKhPjPKcS7_.isra.0
	.section	.text._ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE0EEC2EPS3_PKhj.constprop.0,"axG",@progbits,_ZN2v88internal4wasm10ThreadImpl7ExecuteEPNS1_15InterpreterCodeEmi,comdat
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE0EEC2EPS3_PKhj.constprop.0, @function
_ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE0EEC2EPS3_PKhj.constprop.0:
.LFB26411:
	.cfi_startproc
	movzbl	1(%rsi), %eax
	movl	$0, 8(%rdi)
	movl	%eax, %edx
	andl	$127, %edx
	testb	%al, %al
	js	.L31
	movl	$4, %r9d
	movl	$3, %r8d
	movl	$2, %ecx
	movl	$6, %r11d
	movl	$5, %r10d
	movl	$2, %eax
.L18:
	addq	%rax, %rsi
	movl	%edx, (%rdi)
	movzbl	(%rsi), %eax
	movl	%eax, %edx
	andl	$127, %edx
	testb	%al, %al
	js	.L32
	movl	%edx, 4(%rdi)
	movl	%ecx, 8(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	movzbl	2(%rsi), %ecx
	movl	%ecx, %eax
	sall	$7, %eax
	andl	$16256, %eax
	orl	%eax, %edx
	testb	%cl, %cl
	js	.L33
	movl	$5, %r9d
	movl	$4, %r8d
	movl	$3, %ecx
	movl	$7, %r11d
	movl	$6, %r10d
	movl	$3, %eax
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L32:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%r8d, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movzbl	1(%rsi), %ebx
	movl	%ebx, %eax
	sall	$7, %eax
	andl	$16256, %eax
	orl	%eax, %edx
	testb	%bl, %bl
	js	.L34
.L19:
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	%edx, 4(%rdi)
	movl	%ecx, 8(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore 3
	.cfi_restore 6
	movzbl	3(%rsi), %ecx
	movl	%ecx, %eax
	sall	$14, %eax
	andl	$2080768, %eax
	orl	%eax, %edx
	testb	%cl, %cl
	js	.L35
	movl	$6, %r9d
	movl	$5, %r8d
	movl	$4, %ecx
	movl	$8, %r11d
	movl	$7, %r10d
	movl	$4, %eax
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L34:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movzbl	2(%rsi), %r8d
	movl	%r9d, %ecx
	movl	%r8d, %eax
	sall	$14, %eax
	andl	$2080768, %eax
	orl	%eax, %edx
	testb	%r8b, %r8b
	jns	.L19
	movzbl	3(%rsi), %r8d
	movl	%r10d, %ecx
	movl	%r8d, %eax
	sall	$21, %eax
	andl	$266338304, %eax
	orl	%eax, %edx
	testb	%r8b, %r8b
	jns	.L19
	movzbl	4(%rsi), %eax
	movl	%r11d, %ecx
	sall	$28, %eax
	orl	%eax, %edx
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	movzbl	4(%rsi), %ecx
	movl	%ecx, %eax
	sall	$21, %eax
	andl	$266338304, %eax
	orl	%eax, %edx
	testb	%cl, %cl
	js	.L36
	movl	$7, %r9d
	movl	$6, %r8d
	movl	$5, %ecx
	movl	$9, %r11d
	movl	$8, %r10d
	movl	$5, %eax
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L36:
	movzbl	5(%rsi), %eax
	movl	$8, %r9d
	movl	$6, %ecx
	movl	$7, %r8d
	movl	$10, %r11d
	movl	$9, %r10d
	sall	$28, %eax
	orl	%eax, %edx
	movl	$6, %eax
	jmp	.L18
	.cfi_endproc
.LFE26411:
	.size	_ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE0EEC2EPS3_PKhj.constprop.0, .-_ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE0EEC2EPS3_PKhj.constprop.0
	.section	.text._ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE0EEC2EPS3_PKhj.constprop.1,"axG",@progbits,_ZN2v88internal4wasm10ThreadImpl7ExecuteEPNS1_15InterpreterCodeEmi,comdat
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE0EEC2EPS3_PKhj.constprop.1, @function
_ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE0EEC2EPS3_PKhj.constprop.1:
.LFB26410:
	.cfi_startproc
	movzbl	1(%rsi), %eax
	movl	$0, 8(%rdi)
	movl	%eax, %edx
	andl	$127, %edx
	testb	%al, %al
	js	.L51
	movl	$4, %r9d
	movl	$3, %r8d
	movl	$2, %ecx
	movl	$6, %r11d
	movl	$5, %r10d
	movl	$2, %eax
.L38:
	addq	%rax, %rsi
	movl	%edx, (%rdi)
	movzbl	(%rsi), %eax
	movl	%eax, %edx
	andl	$127, %edx
	testb	%al, %al
	js	.L52
	movl	%edx, 4(%rdi)
	movl	%ecx, 8(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	movzbl	2(%rsi), %ecx
	movl	%ecx, %eax
	sall	$7, %eax
	andl	$16256, %eax
	orl	%eax, %edx
	testb	%cl, %cl
	js	.L53
	movl	$5, %r9d
	movl	$4, %r8d
	movl	$3, %ecx
	movl	$7, %r11d
	movl	$6, %r10d
	movl	$3, %eax
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L52:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%r8d, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movzbl	1(%rsi), %ebx
	movl	%ebx, %eax
	sall	$7, %eax
	andl	$16256, %eax
	orl	%eax, %edx
	testb	%bl, %bl
	js	.L54
.L39:
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	%edx, 4(%rdi)
	movl	%ecx, 8(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore 3
	.cfi_restore 6
	movzbl	3(%rsi), %ecx
	movl	%ecx, %eax
	sall	$14, %eax
	andl	$2080768, %eax
	orl	%eax, %edx
	testb	%cl, %cl
	js	.L55
	movl	$6, %r9d
	movl	$5, %r8d
	movl	$4, %ecx
	movl	$8, %r11d
	movl	$7, %r10d
	movl	$4, %eax
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L54:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movzbl	2(%rsi), %r8d
	movl	%r9d, %ecx
	movl	%r8d, %eax
	sall	$14, %eax
	andl	$2080768, %eax
	orl	%eax, %edx
	testb	%r8b, %r8b
	jns	.L39
	movzbl	3(%rsi), %r8d
	movl	%r10d, %ecx
	movl	%r8d, %eax
	sall	$21, %eax
	andl	$266338304, %eax
	orl	%eax, %edx
	testb	%r8b, %r8b
	jns	.L39
	movzbl	4(%rsi), %eax
	movl	%r11d, %ecx
	sall	$28, %eax
	orl	%eax, %edx
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L55:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	movzbl	4(%rsi), %ecx
	movl	%ecx, %eax
	sall	$21, %eax
	andl	$266338304, %eax
	orl	%eax, %edx
	testb	%cl, %cl
	js	.L56
	movl	$7, %r9d
	movl	$6, %r8d
	movl	$5, %ecx
	movl	$9, %r11d
	movl	$8, %r10d
	movl	$5, %eax
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L56:
	movzbl	5(%rsi), %eax
	movl	$8, %r9d
	movl	$6, %ecx
	movl	$7, %r8d
	movl	$10, %r11d
	movl	$9, %r10d
	sall	$28, %eax
	orl	%eax, %edx
	movl	$6, %eax
	jmp	.L38
	.cfi_endproc
.LFE26410:
	.size	_ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE0EEC2EPS3_PKhj.constprop.1, .-_ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE0EEC2EPS3_PKhj.constprop.1
	.section	.text._ZN2v88internal11HandleScopeD2Ev,"axG",@progbits,_ZN2v88internal11HandleScopeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11HandleScopeD2Ev
	.type	_ZN2v88internal11HandleScopeD2Ev, @function
_ZN2v88internal11HandleScopeD2Ev:
.LFB10656:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L57
	movq	16(%rax), %rdx
	movq	8(%rax), %rax
	subl	$1, 41104(%rdi)
	movq	%rax, 41088(%rdi)
	cmpq	41096(%rdi), %rdx
	je	.L57
	movq	%rdx, 41096(%rdi)
	jmp	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	.p2align 4,,10
	.p2align 3
.L57:
	ret
	.cfi_endproc
.LFE10656:
	.size	_ZN2v88internal11HandleScopeD2Ev, .-_ZN2v88internal11HandleScopeD2Ev
	.weak	_ZN2v88internal11HandleScopeD1Ev
	.set	_ZN2v88internal11HandleScopeD1Ev,_ZN2v88internal11HandleScopeD2Ev
	.section	.rodata._ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag.str1.1,"aMS",@progbits,1
.LC0:
	.string	"0 < len"
.LC1:
	.string	"Check failed: %s."
	.section	.rodata._ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"basic_string::_M_construct null not valid"
	.section	.text._ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag,"axG",@progbits,_ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag
	.type	_ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag, @function
_ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag:
.LFB19274:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$392, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 56(%rdi)
	je	.L87
.L60:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L88
	addq	$392, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	.cfi_restore_state
	movq	$256, -328(%rbp)
	movq	%rdi, %r12
	movl	%esi, %ebx
	leaq	-320(%rbp), %rdi
	movl	$256, %esi
	movq	%rdi, -336(%rbp)
	call	_ZN2v88internal9VSNPrintFENS0_6VectorIcEEPKcP13__va_list_tag@PLT
	testl	%eax, %eax
	jle	.L89
	movq	-336(%rbp), %r14
	leaq	-400(%rbp), %r13
	movslq	%eax, %r15
	leaq	-416(%rbp), %rdi
	movq	%r13, -416(%rbp)
	testq	%r14, %r14
	je	.L90
	movq	%r15, -424(%rbp)
	cmpl	$15, %eax
	jg	.L91
	movq	%r13, %rdi
	cmpl	$1, %eax
	jne	.L65
	movzbl	(%r14), %eax
	movq	%r13, %rdx
	movb	%al, -400(%rbp)
	movl	$1, %eax
.L66:
	movq	%rax, -408(%rbp)
	leaq	-360(%rbp), %r14
	movb	$0, (%rdx,%rax)
	movq	-416(%rbp), %rax
	movl	%ebx, -384(%rbp)
	movq	%r14, -376(%rbp)
	cmpq	%r13, %rax
	je	.L92
	movq	-400(%rbp), %rcx
	movq	-408(%rbp), %rdx
	movq	%rax, -376(%rbp)
	movq	%r13, -416(%rbp)
	movq	48(%r12), %rdi
	movq	%rcx, -360(%rbp)
	movq	%rdx, -368(%rbp)
	movq	$0, -408(%rbp)
	movb	$0, -400(%rbp)
	movl	%ebx, 40(%r12)
	cmpq	%r14, %rax
	je	.L68
	leaq	64(%r12), %rsi
	cmpq	%rsi, %rdi
	je	.L93
	movq	%rdx, %xmm0
	movq	%rcx, %xmm1
	movq	64(%r12), %rsi
	movq	%rax, 48(%r12)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 56(%r12)
	testq	%rdi, %rdi
	je	.L74
	movq	%rdi, -376(%rbp)
	movq	%rsi, -360(%rbp)
.L72:
	movq	$0, -368(%rbp)
	movb	$0, (%rdi)
	movq	-376(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L75
	call	_ZdlPv@PLT
.L75:
	movq	-416(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L76
	call	_ZdlPv@PLT
.L76:
	movq	(%r12), %rax
	leaq	_ZN2v88internal4wasm7Decoder12onFirstErrorEv(%rip), %rdx
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	je	.L60
	movq	%r12, %rdi
	call	*%rax
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L91:
	leaq	-424(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -416(%rbp)
	movq	%rax, %rdi
	movq	-424(%rbp), %rax
	movq	%rax, -400(%rbp)
.L65:
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-424(%rbp), %rax
	movq	-416(%rbp), %rdx
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L92:
	movq	-408(%rbp), %rdx
	movq	48(%r12), %rdi
	movl	%ebx, 40(%r12)
	movdqa	-400(%rbp), %xmm2
	movq	$0, -408(%rbp)
	movq	%rdx, -368(%rbp)
	movb	$0, -400(%rbp)
	movups	%xmm2, -360(%rbp)
.L68:
	testq	%rdx, %rdx
	je	.L70
	cmpq	$1, %rdx
	je	.L94
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-368(%rbp), %rdx
	movq	48(%r12), %rdi
.L70:
	movq	%rdx, 56(%r12)
	movb	$0, (%rdi,%rdx)
	movq	-376(%rbp), %rdi
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L93:
	movq	%rdx, %xmm0
	movq	%rcx, %xmm3
	movq	%rax, 48(%r12)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 56(%r12)
.L74:
	movq	%r14, -376(%rbp)
	leaq	-360(%rbp), %r14
	movq	%r14, %rdi
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L94:
	movzbl	-360(%rbp), %eax
	movb	%al, (%rdi)
	movq	-368(%rbp), %rdx
	movq	48(%r12), %rdi
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L89:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L88:
	call	__stack_chk_fail@PLT
.L90:
	leaq	.LC2(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.cfi_endproc
.LFE19274:
	.size	_ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag, .-_ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag
	.section	.text._ZN2v88internal4wasm7Decoder6errorfEjPKcz,"axG",@progbits,_ZN2v88internal4wasm7Decoder6errorfEjPKcz,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm7Decoder6errorfEjPKcz
	.type	_ZN2v88internal4wasm7Decoder6errorfEjPKcz, @function
_ZN2v88internal4wasm7Decoder6errorfEjPKcz:
.LFB19252:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$208, %rsp
	movq	%rcx, -152(%rbp)
	movq	%r8, -144(%rbp)
	movq	%r9, -136(%rbp)
	testb	%al, %al
	je	.L96
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm3, -80(%rbp)
	movaps	%xmm4, -64(%rbp)
	movaps	%xmm5, -48(%rbp)
	movaps	%xmm6, -32(%rbp)
	movaps	%xmm7, -16(%rbp)
.L96:
	movq	%fs:40, %rax
	movq	%rax, -184(%rbp)
	xorl	%eax, %eax
	leaq	16(%rbp), %rax
	leaq	-208(%rbp), %rcx
	movl	$24, -208(%rbp)
	movq	%rax, -200(%rbp)
	leaq	-176(%rbp), %rax
	movq	%rax, -192(%rbp)
	movl	$48, -204(%rbp)
	call	_ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag
	movq	-184(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L99
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L99:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19252:
	.size	_ZN2v88internal4wasm7Decoder6errorfEjPKcz, .-_ZN2v88internal4wasm7Decoder6errorfEjPKcz
	.section	.rodata._ZN2v88internal4wasm7Decoder5errorEPKhPKc.str1.1,"aMS",@progbits,1
.LC3:
	.string	"%s"
	.section	.text._ZN2v88internal4wasm7Decoder5errorEPKhPKc,"axG",@progbits,_ZN2v88internal4wasm7Decoder5errorEPKhPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	.type	_ZN2v88internal4wasm7Decoder5errorEPKhPKc, @function
_ZN2v88internal4wasm7Decoder5errorEPKhPKc:
.LFB19250:
	.cfi_startproc
	endbr64
	movq	%rdx, %rcx
	subq	8(%rdi), %rsi
	leaq	.LC3(%rip), %rdx
	xorl	%eax, %eax
	addl	32(%rdi), %esi
	jmp	_ZN2v88internal4wasm7Decoder6errorfEjPKcz
	.cfi_endproc
.LFE19250:
	.size	_ZN2v88internal4wasm7Decoder5errorEPKhPKc, .-_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	.section	.rodata._ZN2v88internal4wasm7CodeMapC2EPKNS1_10WasmModuleEPKhPNS0_4ZoneE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"vector::reserve"
.LC5:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZN2v88internal4wasm7CodeMapC2EPKNS1_10WasmModuleEPKhPNS0_4ZoneE,"axG",@progbits,_ZN2v88internal4wasm7CodeMapC5EPKNS1_10WasmModuleEPKhPNS0_4ZoneE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm7CodeMapC2EPKNS1_10WasmModuleEPKhPNS0_4ZoneE
	.type	_ZN2v88internal4wasm7CodeMapC2EPKNS1_10WasmModuleEPKhPNS0_4ZoneE, @function
_ZN2v88internal4wasm7CodeMapC2EPKNS1_10WasmModuleEPKhPNS0_4ZoneE:
.LFB20007:
	.cfi_startproc
	endbr64
	movq	%rcx, %xmm0
	movq	%rsi, %xmm2
	movq	%rcx, 16(%rdi)
	punpcklqdq	%xmm2, %xmm0
	movq	$0, 24(%rdi)
	movups	%xmm0, (%rdi)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 32(%rdi)
	testq	%rsi, %rsi
	je	.L145
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	144(%rsi), %r8
	movq	136(%rsi), %r13
	movq	%r8, %rax
	subq	%r13, %rax
	movq	%rax, %rdx
	sarq	$5, %rdx
	cmpq	$780903167, %rax
	ja	.L148
	movq	%rdi, %r12
	xorl	%eax, %eax
	xorl	%ebx, %ebx
	testq	%rdx, %rdx
	jne	.L149
.L104:
	cmpq	%r13, %r8
	je	.L101
	pxor	%xmm0, %xmm0
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L150:
	cmpq	%rbx, %rax
	je	.L108
	movq	%r13, (%rbx)
	movl	$0, 8(%rbx)
	movq	$0, 24(%rbx)
	movq	$0, 40(%rbx)
	movq	$0, 32(%rbx)
	movq	$0, 48(%rbx)
	movq	$0, 56(%rbx)
	movq	$0, 64(%rbx)
	movq	$0, 72(%rbx)
	movq	$0, 80(%rbx)
	addq	$88, 32(%r12)
	movq	%rcx, 16(%rbx)
.L117:
	addq	$32, %r13
	cmpq	%r13, %r8
	je	.L101
.L151:
	movq	32(%r12), %rbx
	movq	40(%r12), %rax
.L127:
	cmpb	$0, 24(%r13)
	movq	(%r12), %rcx
	jne	.L150
	movl	16(%r13), %r14d
	movl	%r14d, %r15d
	addl	20(%r13), %r15d
	addq	%r11, %r14
	addq	%r11, %r15
	cmpq	%rbx, %rax
	je	.L118
	movq	%r13, (%rbx)
	addq	$32, %r13
	movl	$0, 8(%rbx)
	movq	$0, 24(%rbx)
	movq	$0, 32(%rbx)
	movq	$0, 40(%rbx)
	movq	%r14, 48(%rbx)
	movq	%r15, 56(%rbx)
	movq	%r14, 64(%rbx)
	movq	%r15, 72(%rbx)
	movq	$0, 80(%rbx)
	movq	%rcx, 16(%rbx)
	addq	$88, 32(%r12)
	cmpq	%r13, %r8
	jne	.L151
.L101:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L108:
	.cfi_restore_state
	movq	24(%r12), %r14
	movq	%rbx, %r15
	movabsq	$3353953467947191203, %rdi
	subq	%r14, %r15
	movq	%r15, %rax
	sarq	$3, %rax
	imulq	%rdi, %rax
	cmpq	$24403223, %rax
	je	.L120
	testq	%rax, %rax
	je	.L129
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L152
	movl	$2147483624, %esi
	movl	$2147483624, %edx
.L111:
	movq	16(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %r9
	subq	%rax, %r9
	cmpq	%rsi, %r9
	jb	.L153
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L114:
	leaq	(%rax,%rdx), %rdi
	leaq	88(%rax), %rsi
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L152:
	testq	%rdx, %rdx
	jne	.L154
	movl	$88, %esi
	xorl	%edi, %edi
	xorl	%eax, %eax
.L112:
	leaq	(%rax,%r15), %rdx
	movq	%r13, (%rdx)
	movl	$0, 8(%rdx)
	movq	%rcx, 16(%rdx)
	movq	$0, 24(%rdx)
	movq	$0, 32(%rdx)
	movq	$0, 40(%rdx)
	movq	$0, 48(%rdx)
	movq	$0, 56(%rdx)
	movq	$0, 64(%rdx)
	movq	$0, 72(%rdx)
	movq	$0, 80(%rdx)
	cmpq	%rbx, %r14
	je	.L115
	movq	%r14, %rdx
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L116:
	movq	(%rdx), %rsi
	addq	$88, %rdx
	addq	$88, %rcx
	movq	%rsi, -88(%rcx)
	movl	-80(%rdx), %esi
	movl	%esi, -80(%rcx)
	movq	-72(%rdx), %rsi
	movq	%rsi, -72(%rcx)
	movq	-64(%rdx), %rsi
	movq	%rsi, -64(%rcx)
	movq	-56(%rdx), %rsi
	movq	%rsi, -56(%rcx)
	movq	-48(%rdx), %rsi
	movq	%rsi, -48(%rcx)
	movq	-40(%rdx), %rsi
	movq	$0, -48(%rdx)
	movups	%xmm0, -64(%rdx)
	movq	%rsi, -40(%rcx)
	movq	-32(%rdx), %rsi
	movq	%rsi, -32(%rcx)
	movq	-24(%rdx), %rsi
	movq	%rsi, -24(%rcx)
	movq	-16(%rdx), %rsi
	movq	%rsi, -16(%rcx)
	movq	-8(%rdx), %rsi
	movq	%rsi, -8(%rcx)
	cmpq	%rdx, %rbx
	jne	.L116
	subq	%r14, %rbx
	leaq	-88(%rbx), %rdx
	movabsq	$1048110458733497251, %rbx
	shrq	$3, %rdx
	imulq	%rbx, %rdx
	movabsq	$2305843009213693951, %rbx
	andq	%rbx, %rdx
	addq	$2, %rdx
	leaq	(%rdx,%rdx,4), %rcx
	leaq	(%rdx,%rcx,2), %rdx
	leaq	(%rax,%rdx,8), %rsi
.L115:
	movq	%rax, %xmm1
	movq	%rsi, %xmm3
	movq	%rdi, 40(%r12)
	punpcklqdq	%xmm3, %xmm1
	movups	%xmm1, 24(%r12)
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L118:
	movq	24(%r12), %r9
	movq	%rbx, %rdx
	movabsq	$3353953467947191203, %rdi
	subq	%r9, %rdx
	movq	%rdx, %rax
	sarq	$3, %rax
	imulq	%rdi, %rax
	cmpq	$24403223, %rax
	je	.L120
	testq	%rax, %rax
	je	.L132
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L155
	movq	$2147483624, -56(%rbp)
	movl	$2147483624, %esi
.L121:
	movq	16(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %r10
	subq	%rax, %r10
	cmpq	%rsi, %r10
	jb	.L156
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L124:
	movq	-56(%rbp), %rsi
	leaq	88(%rax), %rdi
	addq	%rax, %rsi
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L155:
	testq	%rsi, %rsi
	jne	.L157
	movl	$88, %edi
	xorl	%esi, %esi
	xorl	%eax, %eax
.L122:
	addq	%rax, %rdx
	movq	%r13, (%rdx)
	movl	$0, 8(%rdx)
	movq	%rcx, 16(%rdx)
	movq	$0, 24(%rdx)
	movq	$0, 32(%rdx)
	movq	$0, 40(%rdx)
	movq	%r14, 48(%rdx)
	movq	%r15, 56(%rdx)
	movq	%r14, 64(%rdx)
	movq	%r15, 72(%rdx)
	movq	$0, 80(%rdx)
	cmpq	%rbx, %r9
	je	.L125
	movq	%r9, %rdx
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L126:
	movq	(%rdx), %rdi
	addq	$88, %rdx
	addq	$88, %rcx
	movq	%rdi, -88(%rcx)
	movl	-80(%rdx), %edi
	movl	%edi, -80(%rcx)
	movq	-72(%rdx), %rdi
	movq	%rdi, -72(%rcx)
	movq	-64(%rdx), %rdi
	movq	%rdi, -64(%rcx)
	movq	-56(%rdx), %rdi
	movq	%rdi, -56(%rcx)
	movq	-48(%rdx), %rdi
	movq	%rdi, -48(%rcx)
	movq	-40(%rdx), %rdi
	movq	$0, -48(%rdx)
	movups	%xmm0, -64(%rdx)
	movq	%rdi, -40(%rcx)
	movq	-32(%rdx), %rdi
	movq	%rdi, -32(%rcx)
	movq	-24(%rdx), %rdi
	movq	%rdi, -24(%rcx)
	movq	-16(%rdx), %rdi
	movq	%rdi, -16(%rcx)
	movq	-8(%rdx), %rdi
	movq	%rdi, -8(%rcx)
	cmpq	%rdx, %rbx
	jne	.L126
	subq	%r9, %rbx
	leaq	-88(%rbx), %rdx
	movabsq	$1048110458733497251, %rbx
	shrq	$3, %rdx
	imulq	%rbx, %rdx
	movabsq	$2305843009213693951, %rbx
	andq	%rbx, %rdx
	addq	$2, %rdx
	leaq	(%rdx,%rdx,4), %rcx
	leaq	(%rdx,%rcx,2), %rdx
	leaq	(%rax,%rdx,8), %rdi
.L125:
	movq	%rax, %xmm1
	movq	%rdi, %xmm4
	movq	%rsi, 40(%r12)
	punpcklqdq	%xmm4, %xmm1
	movups	%xmm1, 24(%r12)
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L149:
	leaq	(%rdx,%rdx,4), %rax
	movq	16(%rcx), %rbx
	leaq	(%rdx,%rax,2), %rax
	movq	24(%rcx), %rdx
	leaq	0(,%rax,8), %r13
	subq	%rbx, %rdx
	movq	%r13, %rsi
	cmpq	%rdx, %r13
	ja	.L158
	addq	%rbx, %rsi
	movq	%rsi, 16(%rcx)
.L106:
	leaq	(%rbx,%r13), %rax
	movq	%rbx, 24(%r12)
	movq	%rbx, 32(%r12)
	movq	%rax, 40(%r12)
	movq	136(%r14), %r13
	movq	144(%r14), %r8
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L145:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L132:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	$88, -56(%rbp)
	movl	$88, %esi
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L129:
	movl	$88, %esi
	movl	$88, %edx
	jmp	.L111
.L156:
	movq	%r11, -96(%rbp)
	movq	%r8, -88(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%rdx, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	-80(%rbp), %rcx
	movq	-88(%rbp), %r8
	movq	-96(%rbp), %r11
	jmp	.L124
.L153:
	movq	%r11, -80(%rbp)
	movq	%r8, -72(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rcx
	pxor	%xmm0, %xmm0
	movq	-72(%rbp), %r8
	movq	-80(%rbp), %r11
	jmp	.L114
.L158:
	movq	%rcx, %rdi
	movq	%r11, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r11
	movq	%rax, %rbx
	jmp	.L106
.L120:
	leaq	.LC5(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L154:
	cmpq	$24403223, %rdx
	movl	$24403223, %eax
	cmova	%rax, %rdx
	imulq	$88, %rdx, %rdx
	movq	%rdx, %rsi
	jmp	.L111
.L148:
	leaq	.LC4(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L157:
	cmpq	$24403223, %rsi
	movl	$24403223, %eax
	cmova	%rax, %rsi
	imulq	$88, %rsi, %rax
	movq	%rax, -56(%rbp)
	movq	%rax, %rsi
	jmp	.L121
	.cfi_endproc
.LFE20007:
	.size	_ZN2v88internal4wasm7CodeMapC2EPKNS1_10WasmModuleEPKhPNS0_4ZoneE, .-_ZN2v88internal4wasm7CodeMapC2EPKNS1_10WasmModuleEPKhPNS0_4ZoneE
	.weak	_ZN2v88internal4wasm7CodeMapC1EPKNS1_10WasmModuleEPKhPNS0_4ZoneE
	.set	_ZN2v88internal4wasm7CodeMapC1EPKNS1_10WasmModuleEPKhPNS0_4ZoneE,_ZN2v88internal4wasm7CodeMapC2EPKNS1_10WasmModuleEPKhPNS0_4ZoneE
	.section	.rodata._ZN2v88internal4wasm10ThreadImpl14GetGlobalValueEj.str1.1,"aMS",@progbits,1
.LC6:
	.string	"unreachable code"
	.section	.text._ZN2v88internal4wasm10ThreadImpl14GetGlobalValueEj,"axG",@progbits,_ZN2v88internal4wasm10ThreadImpl14GetGlobalValueEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm10ThreadImpl14GetGlobalValueEj
	.type	_ZN2v88internal4wasm10ThreadImpl14GetGlobalValueEj, @function
_ZN2v88internal4wasm10ThreadImpl14GetGlobalValueEj:
.LFB20083:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	salq	$5, %rbx
	subq	$40, %rsp
	movq	(%rsi), %rax
	movq	8(%rax), %rax
	addq	24(%rax), %rbx
	cmpb	$9, (%rbx)
	ja	.L160
	movzbl	(%rbx), %eax
	leaq	.L162(%rip), %rdx
	movq	%rdi, %r12
	movq	%rsi, %r13
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm10ThreadImpl14GetGlobalValueEj,"aG",@progbits,_ZN2v88internal4wasm10ThreadImpl14GetGlobalValueEj,comdat
	.align 4
	.align 4
.L162:
	.long	.L160-.L162
	.long	.L167-.L162
	.long	.L166-.L162
	.long	.L165-.L162
	.long	.L164-.L162
	.long	.L163-.L162
	.long	.L161-.L162
	.long	.L161-.L162
	.long	.L160-.L162
	.long	.L161-.L162
	.section	.text._ZN2v88internal4wasm10ThreadImpl14GetGlobalValueEj,"axG",@progbits,_ZN2v88internal4wasm10ThreadImpl14GetGlobalValueEj,comdat
	.p2align 4,,10
	.p2align 3
.L161:
	movq	8(%rsi), %r14
	movq	41088(%r14), %rax
	addl	$1, 41104(%r14)
	cmpb	$0, 1(%rbx)
	movq	41096(%r14), %r15
	movq	%rax, -56(%rbp)
	movq	16(%rsi), %rax
	movq	8(%rsi), %rcx
	movq	(%rax), %rax
	je	.L180
	cmpb	$0, 28(%rbx)
	jne	.L196
.L180:
	movq	41112(%rcx), %rdi
	movq	175(%rax), %rsi
	testq	%rdi, %rdi
	je	.L185
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L186:
	movl	24(%rbx), %eax
.L184:
	movq	8(%r13), %rbx
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%rsi,%rax), %r13
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L188
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r13
.L189:
	movq	-56(%rbp), %rax
	subl	$1, 41104(%r14)
	movq	%rax, 41088(%r14)
	cmpq	41096(%r14), %r15
	je	.L191
	movq	%r15, 41096(%r14)
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L191:
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L192
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L193:
	movb	$6, (%r12)
	movq	$0, 9(%r12)
	movq	%rax, 1(%r12)
.L159:
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L167:
	.cfi_restore_state
	movq	16(%rsi), %rax
	cmpb	$0, 1(%rbx)
	movq	(%rax), %rdx
	movl	24(%rbx), %eax
	je	.L168
	cmpb	$0, 28(%rbx)
	jne	.L197
.L168:
	addq	103(%rdx), %rax
.L169:
	movl	(%rax), %eax
	pxor	%xmm0, %xmm0
	movb	$1, (%r12)
	movups	%xmm0, 1(%r12)
	movl	%eax, 1(%r12)
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L166:
	movq	16(%rsi), %rax
	cmpb	$0, 1(%rbx)
	movq	(%rax), %rdx
	movl	24(%rbx), %eax
	je	.L171
	cmpb	$0, 28(%rbx)
	jne	.L198
.L171:
	addq	103(%rdx), %rax
.L172:
	movq	(%rax), %rax
	movb	$2, (%r12)
	movq	$0, 9(%r12)
	movq	%rax, 1(%r12)
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L165:
	movq	16(%rsi), %rax
	cmpb	$0, 1(%rbx)
	movq	(%rax), %rdx
	movl	24(%rbx), %eax
	je	.L173
	cmpb	$0, 28(%rbx)
	jne	.L199
.L173:
	addq	103(%rdx), %rax
.L174:
	movl	(%rax), %eax
	pxor	%xmm0, %xmm0
	movb	$3, (%r12)
	movups	%xmm0, 1(%r12)
	movl	%eax, 1(%r12)
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L164:
	movq	16(%rsi), %rax
	cmpb	$0, 1(%rbx)
	movq	(%rax), %rdx
	movl	24(%rbx), %eax
	je	.L175
	cmpb	$0, 28(%rbx)
	jne	.L200
.L175:
	addq	103(%rdx), %rax
.L176:
	movq	(%rax), %rax
	movb	$4, (%r12)
	movq	$0, 9(%r12)
	movq	%rax, 1(%r12)
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L163:
	movq	16(%rsi), %rax
	cmpb	$0, 1(%rbx)
	movq	(%rax), %rdx
	movl	24(%rbx), %eax
	je	.L177
	cmpb	$0, 28(%rbx)
	jne	.L201
.L177:
	addq	103(%rdx), %rax
.L178:
	movdqu	(%rax), %xmm1
	movb	$5, (%r12)
	movups	%xmm1, 1(%r12)
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L192:
	movq	41088(%r14), %rax
	cmpq	41096(%r14), %rax
	je	.L202
.L194:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r14)
	movq	%r13, (%rax)
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L188:
	movq	41088(%rbx), %rax
	cmpq	%rax, 41096(%rbx)
	je	.L203
.L190:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%rbx)
	movq	%r13, (%rax)
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L185:
	movq	41088(%rcx), %rax
	cmpq	41096(%rcx), %rax
	je	.L204
.L187:
	leaq	8(%rax), %rdi
	movq	%rdi, 41088(%rcx)
	movq	%rsi, (%rax)
	jmp	.L186
.L160:
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L201:
	movq	111(%rdx), %rdx
	movq	(%rdx,%rax,8), %rax
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L200:
	movq	111(%rdx), %rdx
	movq	(%rdx,%rax,8), %rax
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L199:
	movq	111(%rdx), %rdx
	movq	(%rdx,%rax,8), %rax
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L198:
	movq	111(%rdx), %rdx
	movq	(%rdx,%rax,8), %rax
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L197:
	movq	111(%rdx), %rdx
	movq	(%rdx,%rax,8), %rax
	jmp	.L169
.L196:
	movl	24(%rbx), %esi
	movq	183(%rax), %rax
	leal	16(,%rsi,8), %esi
	movslq	%esi, %rsi
	movq	-1(%rsi,%rax), %rsi
	movq	41112(%rcx), %rdi
	testq	%rdi, %rdi
	je	.L181
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L182:
	movq	16(%r13), %rax
	movl	24(%rbx), %ecx
	movq	(%rax), %rax
	movq	111(%rax), %rax
	movl	(%rax,%rcx,8), %eax
	jmp	.L184
.L203:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L190
.L202:
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L194
.L181:
	movq	41088(%rcx), %rax
	cmpq	41096(%rcx), %rax
	je	.L205
.L183:
	leaq	8(%rax), %rdi
	movq	%rdi, 41088(%rcx)
	movq	%rsi, (%rax)
	jmp	.L182
.L204:
	movq	%rcx, %rdi
	movq	%rsi, -72(%rbp)
	movq	%rcx, -64(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	-64(%rbp), %rcx
	jmp	.L187
.L205:
	movq	%rcx, %rdi
	movq	%rsi, -72(%rbp)
	movq	%rcx, -64(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	-64(%rbp), %rcx
	jmp	.L183
	.cfi_endproc
.LFE20083:
	.size	_ZN2v88internal4wasm10ThreadImpl14GetGlobalValueEj, .-_ZN2v88internal4wasm10ThreadImpl14GetGlobalValueEj
	.section	.text._ZN2v88internal4wasm10ThreadImpl15HandleExceptionEPNS0_7IsolateE,"axG",@progbits,_ZN2v88internal4wasm10ThreadImpl15HandleExceptionEPNS0_7IsolateE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm10ThreadImpl15HandleExceptionEPNS0_7IsolateE
	.type	_ZN2v88internal4wasm10ThreadImpl15HandleExceptionEPNS0_7IsolateE, @function
_ZN2v88internal4wasm10ThreadImpl15HandleExceptionEPNS0_7IsolateE:
.LFB20084:
	.cfi_startproc
	endbr64
	movabsq	$-1085102592571150095, %r9
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	72(%rdi), %r12
	movq	136(%rdi), %r8
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r12, %rax
	subq	64(%rdi), %rax
	movl	-16(%r8), %edx
	movabsq	$-6148914691236517205, %rdi
	sarq	$3, %rax
	imulq	%rdi, %rax
	cmpq	%rdx, %rax
	jbe	.L230
	.p2align 4,,10
	.p2align 3
.L207:
	movq	-24(%r12), %r14
	movq	-16(%r12), %rdx
	movq	80(%r14), %rax
	leaq	16(%rax), %rsi
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L209
	movq	%rsi, %rcx
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L258:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L211
.L210:
	cmpq	32(%rax), %rdx
	jbe	.L258
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L210
.L211:
	cmpq	%rcx, %rsi
	je	.L209
	cmpq	32(%rcx), %rdx
	jnb	.L259
.L209:
	movq	48(%rbx), %rdx
	movq	24(%rbx), %rax
	movq	-8(%r12), %r10
	movq	(%rdx), %rdx
	movq	7(%rdx), %rsi
	movq	40(%rbx), %rdx
	subq	%rax, %rdx
	imull	%r9d, %edx
	cmpl	%edx, %r10d
	jge	.L228
	movl	%r10d, %r11d
	movq	%rsi, %rcx
	leal	16(,%r10,8), %eax
	notl	%r11d
	andq	$-262144, %rcx
	cltq
	addl	%r11d, %edx
	movslq	%r10d, %r11
	leaq	-1(%rsi,%rax), %rax
	addq	$24, %rcx
	addq	%r11, %rdx
	leaq	23(%rsi,%rdx,8), %rsi
	.p2align 4,,10
	.p2align 3
.L229:
	movq	(%rcx), %rdx
	addq	$8, %rax
	movq	-37496(%rdx), %rdx
	movq	%rdx, -8(%rax)
	cmpq	%rax, %rsi
	jne	.L229
	movq	24(%rbx), %rax
.L228:
	movq	%r10, %rdx
	salq	$4, %rdx
	addq	%rdx, %r10
	addq	%r10, %rax
	movq	%rax, 40(%rbx)
	movq	72(%rbx), %rax
	leaq	-24(%rax), %r12
	movq	%r12, %rax
	subq	64(%rbx), %rax
	movq	%r12, 72(%rbx)
	sarq	$3, %rax
	movl	-16(%r8), %edx
	imulq	%rdi, %rax
	cmpq	%rax, %rdx
	jb	.L207
.L230:
	movl	$0, 88(%rbx)
	movl	$1, %eax
.L206:
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L260
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L259:
	.cfi_restore_state
	movq	41112(%r13), %rdi
	movq	12480(%r13), %r15
	testq	%rdi, %rdi
	je	.L214
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r15
.L215:
	movq	$0, -71(%rbp)
	movzbl	-64(%rbp), %edx
	movq	40(%rbx), %rax
	subq	24(%rbx), %rax
	movb	$6, -80(%rbp)
	movb	%dl, -96(%rbp)
	movq	48(%rbx), %rdx
	imull	$-252645135, %eax, %eax
	movq	$0, -79(%rbp)
	movdqa	-80(%rbp), %xmm0
	movq	(%rdx), %rdx
	leal	16(,%rax,8), %eax
	movaps	%xmm0, -112(%rbp)
	movq	7(%rdx), %rdi
	cltq
	leaq	-1(%rdi,%rax), %rsi
	movq	%r15, (%rsi)
	testb	$1, %r15b
	je	.L231
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -136(%rbp)
	testl	$262144, %eax
	jne	.L261
.L218:
	testb	$24, %al
	je	.L231
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L231
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
.L231:
	movq	40(%rbx), %rax
	movb	$6, -112(%rbp)
	movq	$0, -111(%rbp)
	movdqa	-112(%rbp), %xmm1
	leaq	17(%rax), %rdx
	movq	%rdx, 40(%rbx)
	movzbl	-96(%rbp), %edx
	movups	%xmm1, (%rax)
	movb	%dl, 16(%rax)
	movq	96(%r13), %rax
	movq	%rax, 12480(%r13)
	movq	80(%r14), %rax
	movq	-16(%r12), %rdx
	leaq	16(%rax), %r13
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L220
	movq	%r13, %rcx
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L262:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L222
.L221:
	cmpq	32(%rax), %rdx
	jbe	.L262
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L221
.L222:
	cmpq	%rcx, %r13
	je	.L220
	cmpq	32(%rcx), %rdx
	cmovnb	%rcx, %r13
.L220:
	movq	24(%rbx), %rsi
	movq	40(%rbx), %rax
	movabsq	$-1085102592571150095, %r15
	movl	44(%r13), %edx
	movl	48(%r13), %r14d
	subq	%rsi, %rax
	imulq	%r15, %rax
	addl	$1, %edx
	movl	%r14d, -120(%rbp)
	movq	%rax, %r10
	subq	%rdx, %r10
	testq	%r14, %r14
	je	.L225
	cmpq	%rdx, %r14
	jne	.L263
.L225:
	movq	48(%rbx), %rdx
	addq	%r14, %r10
	movq	(%rdx), %rdx
	movq	7(%rdx), %rcx
	movl	%eax, %edx
	cmpl	%eax, %r10d
	jge	.L226
	movl	%r10d, %edi
	movq	%rcx, %rsi
	leal	16(,%r10,8), %eax
	notl	%edi
	andq	$-262144, %rsi
	cltq
	addl	%edi, %edx
	movslq	%r10d, %rdi
	leaq	-1(%rcx,%rax), %rax
	addq	$24, %rsi
	addq	%rdi, %rdx
	leaq	23(%rcx,%rdx,8), %rcx
	.p2align 4,,10
	.p2align 3
.L227:
	movq	(%rsi), %rdx
	addq	$8, %rax
	movq	-37496(%rdx), %rdx
	movq	%rdx, -8(%rax)
	cmpq	%rax, %rcx
	jne	.L227
	movq	24(%rbx), %rsi
.L226:
	movq	%r10, %rax
	salq	$4, %rax
	addq	%r10, %rax
	addq	%rsi, %rax
	movq	%rax, 40(%rbx)
	movslq	40(%r13), %rax
	addq	%rax, -16(%r12)
	xorl	%eax, %eax
	jmp	.L206
.L214:
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L264
.L216:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%r15, (%rax)
	jmp	.L215
.L263:
	subq	%r14, %rax
	movq	%r10, %rdi
	movq	%r14, %rdx
	movq	%r10, -128(%rbp)
	movq	%rax, %r9
	salq	$4, %rdi
	movq	%rax, -136(%rbp)
	salq	$4, %r9
	addq	%r10, %rdi
	salq	$4, %rdx
	addq	%rax, %r9
	addq	%rsi, %rdi
	addq	%r14, %rdx
	addq	%rsi, %r9
	movq	%r9, %rsi
	call	memmove@PLT
	movq	48(%rbx), %rax
	movq	-128(%rbp), %r10
	movl	$4, %r9d
	movq	-136(%rbp), %rcx
	movl	-120(%rbp), %r8d
	movq	(%rax), %rax
	leal	16(,%r10,8), %edx
	leal	16(,%rcx,8), %ecx
	movslq	%edx, %rdx
	movq	7(%rax), %rsi
	movslq	%ecx, %rcx
	leaq	-1(%rsi), %rax
	addq	%rax, %rdx
	addq	%rax, %rcx
	movq	8(%rbx), %rax
	leaq	37592(%rax), %rdi
	call	_ZN2v88internal4Heap9MoveRangeENS0_10HeapObjectENS0_14FullObjectSlotES3_iNS0_16WriteBarrierModeE@PLT
	movq	24(%rbx), %rsi
	movq	40(%rbx), %rax
	movq	-128(%rbp), %r10
	subq	%rsi, %rax
	imulq	%r15, %rax
	jmp	.L225
.L264:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L216
.L261:
	movq	%r15, %rdx
	movq	%rsi, -128(%rbp)
	movq	%rdi, -120(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-136(%rbp), %rcx
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdi
	movq	8(%rcx), %rax
	jmp	.L218
.L260:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20084:
	.size	_ZN2v88internal4wasm10ThreadImpl15HandleExceptionEPNS0_7IsolateE, .-_ZN2v88internal4wasm10ThreadImpl15HandleExceptionEPNS0_7IsolateE
	.section	.rodata._ZN2v88internal4wasm10ThreadImpl24CallExternalWasmFunctionEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEPKNS1_8WasmCodeEPNS0_9SignatureINS1_9ValueTypeEEE.str1.1,"aMS",@progbits,1
.LC7:
	.string	"unimplemented code"
	.section	.text._ZN2v88internal4wasm10ThreadImpl24CallExternalWasmFunctionEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEPKNS1_8WasmCodeEPNS0_9SignatureINS1_9ValueTypeEEE,"axG",@progbits,_ZN2v88internal4wasm10ThreadImpl7ExecuteEPNS1_15InterpreterCodeEmi,comdat
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm10ThreadImpl24CallExternalWasmFunctionEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEPKNS1_8WasmCodeEPNS0_9SignatureINS1_9ValueTypeEEE, @function
_ZN2v88internal4wasm10ThreadImpl24CallExternalWasmFunctionEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEPKNS1_8WasmCodeEPNS0_9SignatureINS1_9ValueTypeEEE:
.LFB20280:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%rsi, %rdi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$360, %rsp
	movq	%rdx, -344(%rbp)
	movq	%rcx, -352(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%r8), %rax
	movq	%rax, -336(%rbp)
	call	_ZN2v88internal4wasm23WasmFeaturesFromIsolateEPNS0_7IsolateE@PLT
	movq	%rax, -317(%rbp)
	movzbl	%dh, %eax
	movb	%al, -308(%rbp)
	movq	%rdx, %rax
	shrq	$16, %rax
	movb	%dl, -309(%rbp)
	movb	%al, -307(%rbp)
	movq	%rdx, %rax
	shrq	$32, %rdx
	shrq	$24, %rax
	cmpl	$2, 60(%rbx)
	movb	%dl, -305(%rbp)
	movb	%al, -306(%rbp)
	je	.L358
.L266:
	movq	16(%r14), %rax
	movq	41112(%r13), %rdi
	movq	(%rax), %rax
	movq	191(%rax), %rsi
	testq	%rdi, %rdi
	je	.L352
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdi
.L274:
	movq	%r12, %rsi
	call	_ZN2v88internal13WasmDebugInfo13GetCWasmEntryENS0_6HandleIS1_EEPNS0_9SignatureINS0_4wasm9ValueTypeEEE@PLT
	movq	(%r12), %r8
	movq	16(%r12), %r10
	movq	%rax, -360(%rbp)
	leaq	(%r10,%r8), %rax
	cmpq	%rax, %r10
	je	.L276
	movq	%r10, %rdx
	xorl	%esi, %esi
	leaq	.L279(%rip), %rdi
	.p2align 4,,10
	.p2align 3
.L282:
	cmpb	$9, (%rdx)
	ja	.L277
	movzbl	(%rdx), %ecx
	movslq	(%rdi,%rcx,4), %rcx
	addq	%rdi, %rcx
	notrack jmp	*%rcx
	.section	.rodata._ZN2v88internal4wasm10ThreadImpl24CallExternalWasmFunctionEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEPKNS1_8WasmCodeEPNS0_9SignatureINS1_9ValueTypeEEE,"aG",@progbits,_ZN2v88internal4wasm10ThreadImpl7ExecuteEPNS1_15InterpreterCodeEmi,comdat
	.align 4
	.align 4
.L279:
	.long	.L277-.L279
	.long	.L281-.L279
	.long	.L278-.L279
	.long	.L281-.L279
	.long	.L278-.L279
	.long	.L333-.L279
	.long	.L278-.L279
	.long	.L278-.L279
	.long	.L277-.L279
	.long	.L278-.L279
	.section	.text._ZN2v88internal4wasm10ThreadImpl24CallExternalWasmFunctionEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEPKNS1_8WasmCodeEPNS0_9SignatureINS1_9ValueTypeEEE,"axG",@progbits,_ZN2v88internal4wasm10ThreadImpl7ExecuteEPNS1_15InterpreterCodeEmi,comdat
	.p2align 4,,10
	.p2align 3
.L278:
	movl	$8, %ecx
.L280:
	addq	$1, %rdx
	addl	%ecx, %esi
	cmpq	%rdx, %rax
	jne	.L282
	addq	8(%r12), %r8
	addq	%r8, %r10
	cmpq	%r10, %rax
	je	.L357
.L330:
	xorl	%ecx, %ecx
	leaq	.L285(%rip), %rdi
	.p2align 4,,10
	.p2align 3
.L288:
	cmpb	$9, (%rax)
	ja	.L277
	movzbl	(%rax), %edx
	movslq	(%rdi,%rdx,4), %rdx
	addq	%rdi, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal4wasm10ThreadImpl24CallExternalWasmFunctionEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEPKNS1_8WasmCodeEPNS0_9SignatureINS1_9ValueTypeEEE,"aG",@progbits,_ZN2v88internal4wasm10ThreadImpl7ExecuteEPNS1_15InterpreterCodeEmi,comdat
	.align 4
	.align 4
.L285:
	.long	.L277-.L285
	.long	.L287-.L285
	.long	.L284-.L285
	.long	.L287-.L285
	.long	.L284-.L285
	.long	.L334-.L285
	.long	.L284-.L285
	.long	.L284-.L285
	.long	.L277-.L285
	.long	.L284-.L285
	.section	.text._ZN2v88internal4wasm10ThreadImpl24CallExternalWasmFunctionEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEPKNS1_8WasmCodeEPNS0_9SignatureINS1_9ValueTypeEEE,"axG",@progbits,_ZN2v88internal4wasm10ThreadImpl7ExecuteEPNS1_15InterpreterCodeEmi,comdat
	.p2align 4,,10
	.p2align 3
.L281:
	movl	$4, %ecx
	jmp	.L280
	.p2align 4,,10
	.p2align 3
.L333:
	movl	$16, %ecx
	jmp	.L280
	.p2align 4,,10
	.p2align 3
.L284:
	movl	$8, %edx
.L286:
	addq	$1, %rax
	addl	%edx, %ecx
	cmpq	%rax, %r10
	jne	.L288
	cmpl	%ecx, %esi
	cmovl	%ecx, %esi
.L357:
	movslq	%esi, %r15
	cmpl	$80, %esi
	jg	.L289
.L328:
	movq	$0, -80(%rbp)
	pxor	%xmm0, %xmm0
	leaq	-176(%rbp), %r8
	movaps	%xmm0, -96(%rbp)
.L290:
	movq	24(%r14), %rax
	movq	40(%r14), %r10
	movabsq	$-1085102592571150095, %rdx
	movq	%r8, -72(%rbp)
	movq	-336(%rbp), %rdi
	movq	$0, -64(%rbp)
	subq	%rax, %r10
	imulq	%rdx, %r10
	movslq	%edi, %r15
	subq	%r15, %r10
	testl	%edi, %edi
	jle	.L291
	movq	%r10, %rdx
	leal	-1(%rdi), %r8d
	movq	%r12, %rdi
	xorl	%ebx, %ebx
	salq	$4, %rdx
	movq	%r15, -368(%rbp)
	leaq	.L299(%rip), %rcx
	movq	%r8, %r15
	addq	%r10, %rdx
	sall	$3, %r10d
	movq	%r13, -376(%rbp)
	movq	%rdx, %r12
	movl	%r10d, %r13d
	movq	%r14, %rdx
	movq	%rdi, %r14
	addq	%r12, %rax
	cmpb	$6, (%rax)
	je	.L292
	.p2align 4,,10
	.p2align 3
.L360:
	movdqu	(%rax), %xmm1
	movzbl	16(%rax), %eax
	movb	%al, -192(%rbp)
	movq	16(%r14), %rax
	movaps	%xmm1, -208(%rbp)
	addq	%rbx, %rax
	addq	(%r14), %rax
	cmpb	$9, (%rax)
	ja	.L297
.L361:
	movzbl	(%rax), %eax
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm10ThreadImpl24CallExternalWasmFunctionEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEPKNS1_8WasmCodeEPNS0_9SignatureINS1_9ValueTypeEEE,"aG",@progbits,_ZN2v88internal4wasm10ThreadImpl7ExecuteEPNS1_15InterpreterCodeEmi,comdat
	.align 4
	.align 4
.L299:
	.long	.L297-.L299
	.long	.L301-.L299
	.long	.L300-.L299
	.long	.L301-.L299
	.long	.L300-.L299
	.long	.L297-.L299
	.long	.L298-.L299
	.long	.L298-.L299
	.long	.L297-.L299
	.long	.L298-.L299
	.section	.text._ZN2v88internal4wasm10ThreadImpl24CallExternalWasmFunctionEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEPKNS1_8WasmCodeEPNS0_9SignatureINS1_9ValueTypeEEE,"axG",@progbits,_ZN2v88internal4wasm10ThreadImpl7ExecuteEPNS1_15InterpreterCodeEmi,comdat
	.p2align 4,,10
	.p2align 3
.L287:
	movl	$4, %edx
	jmp	.L286
	.p2align 4,,10
	.p2align 3
.L334:
	movl	$16, %edx
	jmp	.L286
	.p2align 4,,10
	.p2align 3
.L298:
	movq	-207(%rbp), %rax
	movq	-72(%rbp), %rsi
	movq	(%rax), %rdi
	movq	-64(%rbp), %rax
	addq	%rax, %rsi
	addq	$8, %rax
	movq	%rax, -64(%rbp)
	movq	%rdi, (%rsi)
.L304:
	leaq	1(%rbx), %rsi
	addq	$17, %r12
	cmpq	%rbx, %r15
	je	.L359
.L305:
	movq	24(%rdx), %rax
	movq	%rsi, %rbx
	addq	%r12, %rax
	cmpb	$6, (%rax)
	jne	.L360
.L292:
	movq	48(%rdx), %rax
	movq	8(%rdx), %rdi
	movq	(%rax), %rsi
	leal	16(%r13,%rbx,8), %eax
	cltq
	movq	7(%rsi), %rsi
	movq	-1(%rax,%rsi), %rsi
	movq	41112(%rdi), %r9
	testq	%r9, %r9
	je	.L294
	movq	%r9, %rdi
	movq	%rdx, -328(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-328(%rbp), %rdx
	leaq	.L299(%rip), %rcx
.L295:
	movq	%rax, -207(%rbp)
	movq	16(%r14), %rax
	addq	%rbx, %rax
	addq	(%r14), %rax
	cmpb	$9, (%rax)
	jbe	.L361
	.p2align 4,,10
	.p2align 3
.L297:
	leaq	.LC7(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L301:
	movq	-64(%rbp), %rax
	movq	-72(%rbp), %rsi
	addq	$17, %r12
	addq	%rax, %rsi
	addq	$4, %rax
	movq	%rax, -64(%rbp)
	movl	-207(%rbp), %eax
	movl	%eax, (%rsi)
	leaq	1(%rbx), %rsi
	cmpq	%rbx, %r15
	jne	.L305
.L359:
	movq	-368(%rbp), %r15
	movq	-376(%rbp), %r13
	movq	%r14, %r12
	movq	%rdx, %r14
	movq	-72(%rbp), %r8
.L291:
	movq	-352(%rbp), %rax
	movq	-344(%rbp), %rcx
	movq	%r13, %rdi
	movq	-360(%rbp), %rsi
	movq	(%rax), %rdx
	call	_ZN2v88internal9Execution8CallWasmEPNS0_7IsolateENS0_6HandleINS0_4CodeEEEmNS4_INS0_6ObjectEEEm@PLT
	movq	40(%r14), %rax
	movl	-336(%rbp), %r8d
	movabsq	$-1085102592571150095, %rcx
	movq	%rax, %rdx
	subq	24(%r14), %rdx
	imulq	%rcx, %rdx
	movq	48(%r14), %rcx
	movq	(%rcx), %rcx
	subq	%r15, %rdx
	addl	%edx, %r8d
	movq	7(%rcx), %rcx
	cmpl	%r8d, %edx
	jge	.L307
	movl	%edx, %edi
	movq	%rcx, %rsi
	leal	16(,%rdx,8), %eax
	movslq	%edx, %rdx
	notl	%edi
	andq	$-262144, %rsi
	cltq
	addl	%r8d, %edi
	leaq	-1(%rcx,%rax), %rax
	addq	$24, %rsi
	addq	%rdi, %rdx
	leaq	23(%rcx,%rdx,8), %rcx
	.p2align 4,,10
	.p2align 3
.L308:
	movq	(%rsi), %rdx
	addq	$8, %rax
	movq	-37496(%rdx), %rdx
	movq	%rdx, -8(%rax)
	cmpq	%rax, %rcx
	jne	.L308
	movq	40(%r14), %rax
.L307:
	movq	%r15, %rdx
	salq	$4, %rdx
	addq	%rdx, %r15
	subq	%r15, %rax
	movq	%rax, 40(%r14)
	movq	12480(%r13), %rax
	cmpq	%rax, 96(%r13)
	je	.L309
	movq	%r13, %rsi
	movq	%r14, %rdi
	xorl	%ebx, %ebx
	call	_ZN2v88internal4wasm10ThreadImpl15HandleExceptionEPNS0_7IsolateE
	cmpl	$1, %eax
	setne	%bl
	xorl	%r12d, %r12d
	addl	$4, %ebx
	andl	$15, %ebx
.L311:
	movq	-96(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L273
	call	_ZdlPv@PLT
.L273:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L362
	addq	$360, %rsp
	movl	%ebx, %eax
	movq	%r12, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L300:
	.cfi_restore_state
	movq	-64(%rbp), %rax
	movq	-72(%rbp), %rsi
	addq	%rax, %rsi
	addq	$8, %rax
	movq	%rax, -64(%rbp)
	movq	-207(%rbp), %rax
	movq	%rax, (%rsi)
	jmp	.L304
	.p2align 4,,10
	.p2align 3
.L352:
	movq	41088(%r13), %rdi
	cmpq	41096(%r13), %rdi
	je	.L363
.L275:
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%rdi)
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L294:
	movq	41088(%rdi), %rax
	cmpq	41096(%rdi), %rax
	je	.L364
.L296:
	leaq	8(%rax), %r9
	movq	%r9, 41088(%rdi)
	movq	%rsi, (%rax)
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L289:
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	movq	$0, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	leaq	(%rax,%r15), %rbx
	movq	%rax, %rdi
	movq	%rax, -96(%rbp)
	movq	%rbx, -80(%rbp)
	call	memset@PLT
	movq	%rbx, -88(%rbp)
	movq	%rax, %r8
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L309:
	xorl	%ebx, %ebx
	cmpq	$0, (%r12)
	movq	$0, -64(%rbp)
	leaq	.L314(%rip), %r15
	je	.L326
	.p2align 4,,10
	.p2align 3
.L312:
	movq	16(%r12), %rax
	cmpb	$9, (%rax,%rbx)
	ja	.L297
	movzbl	(%rax,%rbx), %eax
	movslq	(%r15,%rax,4), %rax
	addq	%r15, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm10ThreadImpl24CallExternalWasmFunctionEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEPKNS1_8WasmCodeEPNS0_9SignatureINS1_9ValueTypeEEE,"aG",@progbits,_ZN2v88internal4wasm10ThreadImpl7ExecuteEPNS1_15InterpreterCodeEmi,comdat
	.align 4
	.align 4
.L314:
	.long	.L297-.L314
	.long	.L318-.L314
	.long	.L317-.L314
	.long	.L316-.L314
	.long	.L315-.L314
	.long	.L297-.L314
	.long	.L313-.L314
	.long	.L313-.L314
	.long	.L297-.L314
	.long	.L313-.L314
	.section	.text._ZN2v88internal4wasm10ThreadImpl24CallExternalWasmFunctionEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEPKNS1_8WasmCodeEPNS0_9SignatureINS1_9ValueTypeEEE,"axG",@progbits,_ZN2v88internal4wasm10ThreadImpl7ExecuteEPNS1_15InterpreterCodeEmi,comdat
	.p2align 4,,10
	.p2align 3
.L313:
	movq	-64(%rbp), %rax
	movq	-72(%rbp), %rdx
	movq	41112(%r13), %rdi
	addq	%rax, %rdx
	addq	$8, %rax
	movq	%rax, -64(%rbp)
	movq	(%rdx), %r8
	testq	%rdi, %rdi
	je	.L320
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r8
.L321:
	movq	$0, -199(%rbp)
	movzbl	-192(%rbp), %edx
	movabsq	$-1085102592571150095, %rdi
	movq	40(%r14), %rax
	subq	24(%r14), %rax
	movb	$6, -208(%rbp)
	movb	%dl, -224(%rbp)
	movq	48(%r14), %rdx
	imulq	%rdi, %rax
	movq	$0, -207(%rbp)
	movdqa	-208(%rbp), %xmm6
	movq	(%rdx), %rdx
	leal	16(,%rax,8), %eax
	movaps	%xmm6, -240(%rbp)
	movq	7(%rdx), %rdi
	cltq
	leaq	-1(%rdi,%rax), %rsi
	movq	%r8, (%rsi)
	testb	$1, %r8b
	je	.L327
	movq	%r8, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -328(%rbp)
	testl	$262144, %eax
	jne	.L365
	testb	$24, %al
	je	.L327
.L367:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L327
	movq	%r8, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L327:
	movq	40(%r14), %rax
	movb	$6, -240(%rbp)
	addq	$1, %rbx
	movq	$0, -239(%rbp)
	movdqa	-240(%rbp), %xmm7
	leaq	17(%rax), %rdx
	movq	%rdx, 40(%r14)
	movzbl	-224(%rbp), %edx
	movups	%xmm7, (%rax)
	movb	%dl, 16(%rax)
	cmpq	(%r12), %rbx
	jb	.L312
.L326:
	movl	$3, %ebx
	xorl	%r12d, %r12d
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L315:
	movq	-64(%rbp), %rax
	movq	-72(%rbp), %rdx
	movq	$0, -295(%rbp)
	movb	$4, -304(%rbp)
	movq	40(%r14), %rcx
	addq	%rax, %rdx
	addq	$8, %rax
	movq	%rax, -64(%rbp)
	movq	(%rdx), %rdx
	leaq	17(%rcx), %rsi
	movzbl	-288(%rbp), %eax
	movq	%rsi, 40(%r14)
	movq	%rdx, -303(%rbp)
	movdqa	-304(%rbp), %xmm2
	movb	%al, -256(%rbp)
	movaps	%xmm2, -272(%rbp)
	movb	$4, -272(%rbp)
	movq	%rdx, -271(%rbp)
	movdqa	-272(%rbp), %xmm3
	movb	%al, -192(%rbp)
	movaps	%xmm3, -208(%rbp)
	movb	$4, -208(%rbp)
	movq	%rdx, -207(%rbp)
	movdqa	-208(%rbp), %xmm4
	movb	%al, -224(%rbp)
	movaps	%xmm4, -240(%rbp)
	movb	$4, -240(%rbp)
	movq	%rdx, -239(%rbp)
	movdqa	-240(%rbp), %xmm5
	movb	%al, 16(%rcx)
	movups	%xmm5, (%rcx)
.L319:
	addq	$1, %rbx
	cmpq	(%r12), %rbx
	jb	.L312
	movl	$3, %ebx
	xorl	%r12d, %r12d
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L316:
	movq	-64(%rbp), %rax
	movq	-72(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movb	$3, -304(%rbp)
	movups	%xmm0, -303(%rbp)
	addq	%rax, %rdx
	addq	$4, %rax
	movq	%rax, -64(%rbp)
	movl	(%rdx), %ecx
	movq	40(%r14), %rdx
	movzbl	-288(%rbp), %eax
	movl	%ecx, -303(%rbp)
	movdqa	-304(%rbp), %xmm4
	leaq	17(%rdx), %rsi
	movb	%al, -256(%rbp)
	movaps	%xmm4, -272(%rbp)
	movl	%ecx, -271(%rbp)
	movq	-271(%rbp), %rcx
	movb	$3, -272(%rbp)
	movdqa	-272(%rbp), %xmm5
	movq	%rsi, 40(%r14)
	movaps	%xmm5, -208(%rbp)
	movb	$3, -208(%rbp)
	movq	%rcx, -207(%rbp)
	movdqa	-208(%rbp), %xmm6
	movb	%al, -192(%rbp)
	movaps	%xmm6, -240(%rbp)
	movb	$3, -240(%rbp)
	movq	%rcx, -239(%rbp)
	movdqa	-240(%rbp), %xmm7
	movb	%al, -224(%rbp)
	movups	%xmm7, (%rdx)
	movb	%al, 16(%rdx)
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L317:
	movq	-64(%rbp), %rax
	movq	-72(%rbp), %rdx
	movq	$0, -295(%rbp)
	movb	$2, -304(%rbp)
	movq	40(%r14), %rcx
	addq	%rax, %rdx
	addq	$8, %rax
	movq	%rax, -64(%rbp)
	movq	(%rdx), %rdx
	leaq	17(%rcx), %rsi
	movzbl	-288(%rbp), %eax
	movq	%rsi, 40(%r14)
	movq	%rdx, -303(%rbp)
	movdqa	-304(%rbp), %xmm6
	movb	%al, -256(%rbp)
	movaps	%xmm6, -272(%rbp)
	movb	$2, -272(%rbp)
	movq	%rdx, -271(%rbp)
	movdqa	-272(%rbp), %xmm7
	movb	%al, -192(%rbp)
	movaps	%xmm7, -208(%rbp)
	movb	$2, -208(%rbp)
	movq	%rdx, -207(%rbp)
	movdqa	-208(%rbp), %xmm2
	movb	%al, -224(%rbp)
	movaps	%xmm2, -240(%rbp)
	movb	$2, -240(%rbp)
	movq	%rdx, -239(%rbp)
	movdqa	-240(%rbp), %xmm3
	movb	%al, 16(%rcx)
	movups	%xmm3, (%rcx)
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L318:
	movq	-64(%rbp), %rax
	movq	-72(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movb	$1, -304(%rbp)
	movups	%xmm0, -303(%rbp)
	addq	%rax, %rdx
	addq	$4, %rax
	movq	%rax, -64(%rbp)
	movl	(%rdx), %ecx
	movq	40(%r14), %rdx
	movzbl	-288(%rbp), %eax
	movl	%ecx, -303(%rbp)
	movdqa	-304(%rbp), %xmm2
	leaq	17(%rdx), %rsi
	movb	%al, -256(%rbp)
	movaps	%xmm2, -272(%rbp)
	movl	%ecx, -271(%rbp)
	movq	-271(%rbp), %rcx
	movb	$1, -272(%rbp)
	movdqa	-272(%rbp), %xmm3
	movq	%rsi, 40(%r14)
	movaps	%xmm3, -208(%rbp)
	movb	$1, -208(%rbp)
	movq	%rcx, -207(%rbp)
	movdqa	-208(%rbp), %xmm4
	movb	%al, -192(%rbp)
	movaps	%xmm4, -240(%rbp)
	movb	$1, -240(%rbp)
	movq	%rcx, -239(%rbp)
	movdqa	-240(%rbp), %xmm5
	movb	%al, -224(%rbp)
	movups	%xmm5, (%rdx)
	movb	%al, 16(%rdx)
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L320:
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L366
.L322:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%r8, (%rax)
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L364:
	movq	%rdx, -392(%rbp)
	movq	%rsi, -384(%rbp)
	movq	%rdi, -328(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-392(%rbp), %rdx
	movq	-384(%rbp), %rsi
	leaq	.L299(%rip), %rcx
	movq	-328(%rbp), %rdi
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L358:
	leaq	-317(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm23IsJSCompatibleSignatureEPKNS0_9SignatureINS1_9ValueTypeEEERKNS1_12WasmFeaturesE@PLT
	testb	%al, %al
	jne	.L266
	movq	40(%r14), %rax
	movq	-336(%rbp), %rdi
	movabsq	$-1085102592571150095, %rcx
	movq	%rax, %rdx
	subq	24(%r14), %rdx
	movslq	%edi, %rsi
	movl	%edi, %r9d
	imulq	%rcx, %rdx
	movq	48(%r14), %rcx
	movq	(%rcx), %rcx
	subq	%rsi, %rdx
	addl	%edx, %r9d
	movq	7(%rcx), %rcx
	cmpl	%r9d, %edx
	jge	.L267
	movl	%edx, %r8d
	movq	%rcx, %rdi
	leal	16(,%rdx,8), %eax
	movslq	%edx, %rdx
	notl	%r8d
	andq	$-262144, %rdi
	cltq
	addl	%r9d, %r8d
	leaq	-1(%rcx,%rax), %rax
	addq	$24, %rdi
	addq	%r8, %rdx
	leaq	23(%rcx,%rdx,8), %rcx
	.p2align 4,,10
	.p2align 3
.L268:
	movq	(%rdi), %rdx
	addq	$8, %rax
	movq	-37496(%rdx), %rdx
	movq	%rdx, -8(%rax)
	cmpq	%rcx, %rax
	jne	.L268
	movq	40(%r14), %rax
.L267:
	movq	%rsi, %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	salq	$4, %rdx
	xorl	%ebx, %ebx
	addq	%rsi, %rdx
	movl	$349, %esi
	subq	%rdx, %rax
	xorl	%edx, %edx
	movq	%rax, 40(%r14)
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl15HandleExceptionEPNS0_7IsolateE
	cmpl	$1, %eax
	setne	%bl
	xorl	%r12d, %r12d
	addl	$4, %ebx
	andl	$15, %ebx
	jmp	.L273
.L365:
	movq	%r8, %rdx
	movq	%r8, -352(%rbp)
	movq	%rsi, -344(%rbp)
	movq	%rdi, -336(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-328(%rbp), %rcx
	movq	-352(%rbp), %r8
	movq	-344(%rbp), %rsi
	movq	-336(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L367
	jmp	.L327
.L366:
	movq	%r13, %rdi
	movq	%r8, -328(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-328(%rbp), %r8
	jmp	.L322
.L276:
	movq	8(%r12), %r10
	addq	%r8, %r10
	addq	%rax, %r10
	cmpq	%r10, %rax
	je	.L328
	xorl	%esi, %esi
	jmp	.L330
.L363:
	movq	%r13, %rdi
	movq	%rsi, -328(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-328(%rbp), %rsi
	movq	%rax, %rdi
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L277:
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L362:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20280:
	.size	_ZN2v88internal4wasm10ThreadImpl24CallExternalWasmFunctionEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEPKNS1_8WasmCodeEPNS0_9SignatureINS1_9ValueTypeEEE, .-_ZN2v88internal4wasm10ThreadImpl24CallExternalWasmFunctionEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEPKNS1_8WasmCodeEPNS0_9SignatureINS1_9ValueTypeEEE
	.section	.text._ZN2v88internal4wasm10ThreadImpl8DoReturnEPNS1_7DecoderEPPNS1_15InterpreterCodeEPmS8_m,"axG",@progbits,_ZN2v88internal4wasm10ThreadImpl8DoReturnEPNS1_7DecoderEPPNS1_15InterpreterCodeEPmS8_m,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm10ThreadImpl8DoReturnEPNS1_7DecoderEPPNS1_15InterpreterCodeEPmS8_m
	.type	_ZN2v88internal4wasm10ThreadImpl8DoReturnEPNS1_7DecoderEPPNS1_15InterpreterCodeEPmS8_m, @function
_ZN2v88internal4wasm10ThreadImpl8DoReturnEPNS1_7DecoderEPPNS1_15InterpreterCodeEPmS8_m:
.LFB20109:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%r9, %r12
	movabsq	$-6148914691236517205, %r9
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	24(%rdi), %rdi
	movq	40(%rbx), %r10
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	72(%rbx), %r13
	movq	136(%rbx), %r11
	movabsq	$-1085102592571150095, %rax
	subq	%rdi, %r10
	imulq	%rax, %r10
	leaq	-24(%r13), %rax
	movl	%r10d, %r14d
	subl	-8(%r13), %r14d
	movq	%rax, 72(%rbx)
	movl	%r10d, %edx
	subq	64(%rbx), %rax
	sarq	$3, %rax
	imulq	%r9, %rax
	xorl	%r9d, %r9d
	cmpq	%r11, 128(%rbx)
	je	.L369
	movl	-16(%r11), %r9d
.L369:
	cmpq	%r9, %rax
	je	.L418
	movq	-48(%r13), %rax
	movl	$0, -92(%rbp)
	movl	$0, -96(%rbp)
	movq	%rax, (%r15)
	movq	72(%rax), %rdx
	movdqu	64(%rax), %xmm0
	leaq	-72(%rbp), %rax
	movb	$0, -72(%rbp)
	movq	%rdx, 24(%rsi)
	movq	48(%rsi), %rdx
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, -88(%rbp)
	movl	$0, 32(%rsi)
	movl	$0, 40(%rsi)
	movq	$0, 56(%rsi)
	movups	%xmm0, 8(%rsi)
	movb	$0, (%rdx)
	movq	-88(%rbp), %rdx
	movq	$0, -80(%rbp)
	movb	$0, (%rdx)
	movq	-88(%rbp), %rdi
	cmpq	%rax, %rdi
	je	.L378
	movq	%r8, -112(%rbp)
	movq	%rcx, -104(%rbp)
	call	_ZdlPv@PLT
	movq	-112(%rbp), %r8
	movq	-104(%rbp), %rcx
.L378:
	movq	(%r15), %rsi
	movq	-40(%r13), %rax
	movq	48(%rsi), %rdx
	movzbl	(%rdx,%rax), %edx
	cmpb	$16, %dl
	je	.L379
	cmpb	$17, %dl
	jne	.L419
	movq	64(%rsi), %rdx
	addq	%rax, %rdx
	cmpb	$0, 1(%rdx)
	js	.L420
	movl	$4, %r9d
	movl	$5, %r11d
	movl	$6, %r10d
	movl	$3, %edi
	movl	$2, %esi
	movl	$1, %r15d
.L384:
	addq	%r15, %rdx
	cmpb	$0, 1(%rdx)
	js	.L421
.L385:
	leaq	1(%rax,%rsi), %rax
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L379:
	movq	64(%rsi), %rdi
	movl	$1, %edx
	addq	%rax, %rdi
	cmpb	$0, 1(%rdi)
	js	.L422
.L382:
	leaq	1(%rax,%rdx), %rax
.L383:
	movq	%rax, (%rcx)
	movq	-48(%r13), %rdx
	movq	72(%rdx), %rax
	subq	64(%rdx), %rax
	movabsq	$-1085102592571150095, %rdx
	movq	%rax, (%r8)
	movq	24(%rbx), %rdi
	movq	40(%rbx), %rax
	subq	%rdi, %rax
	imulq	%rdx, %rax
	movq	%rax, %r15
	movq	%rax, %rdx
	subq	%r14, %r15
	testq	%r12, %r12
	je	.L406
	cmpq	%r14, %r12
	je	.L406
	subq	%r12, %rax
	movq	%r12, %rdx
	movq	%rax, %rsi
	movq	%rax, %r13
	salq	$4, %rdx
	salq	$4, %rsi
	addq	%r12, %rdx
	addq	%rax, %rsi
	movq	%r15, %rax
	salq	$4, %rax
	addq	%rdi, %rsi
	addq	%r15, %rax
	addq	%rax, %rdi
	call	memmove@PLT
	movq	48(%rbx), %rax
	movq	(%rax), %rax
	movq	7(%rax), %rsi
	testl	%r12d, %r12d
	jne	.L423
.L389:
	movq	24(%rbx), %rdi
	movq	40(%rbx), %rdx
	movabsq	$-1085102592571150095, %rax
	subq	%rdi, %rdx
	imulq	%rax, %rdx
.L388:
	addq	%r15, %r12
	cmpl	%edx, %r12d
	jge	.L390
	movl	%r12d, %edi
	movq	%rsi, %rcx
	leal	16(,%r12,8), %eax
	notl	%edi
	andq	$-262144, %rcx
	cltq
	addl	%edi, %edx
	movslq	%r12d, %rdi
	leaq	-1(%rsi,%rax), %rax
	addq	$24, %rcx
	addq	%rdi, %rdx
	leaq	23(%rsi,%rdx,8), %rsi
	.p2align 4,,10
	.p2align 3
.L391:
	movq	(%rcx), %rdx
	addq	$8, %rax
	movq	-37496(%rdx), %rdx
	movq	%rdx, -8(%rax)
	cmpq	%rax, %rsi
	jne	.L391
	movq	24(%rbx), %rdi
.L390:
	movq	%r12, %r9
	movl	$1, %eax
	salq	$4, %r9
	addq	%r9, %r12
	addq	%r12, %rdi
	movq	%rdi, 40(%rbx)
.L368:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L424
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L406:
	.cfi_restore_state
	movq	48(%rbx), %rax
	movq	(%rax), %rax
	movq	7(%rax), %rsi
	jmp	.L388
	.p2align 4,,10
	.p2align 3
.L418:
	movq	%r10, %r13
	movl	$3, 88(%rbx)
	subq	%r14, %r13
	testq	%r12, %r12
	je	.L405
	cmpq	%r14, %r12
	jne	.L371
.L405:
	movq	48(%rbx), %rax
	movq	(%rax), %rax
	movq	7(%rax), %rsi
.L373:
	addq	%r13, %r12
	cmpl	%edx, %r12d
	jge	.L375
	movl	%r12d, %edi
	movq	%rsi, %rcx
	leal	16(,%r12,8), %eax
	notl	%edi
	andq	$-262144, %rcx
	cltq
	addl	%edi, %edx
	movslq	%r12d, %rdi
	leaq	-1(%rsi,%rax), %rax
	addq	$24, %rcx
	addq	%rdi, %rdx
	leaq	23(%rsi,%rdx,8), %rsi
	.p2align 4,,10
	.p2align 3
.L376:
	movq	(%rcx), %rdx
	addq	$8, %rax
	movq	-37496(%rdx), %rdx
	movq	%rdx, -8(%rax)
	cmpq	%rsi, %rax
	jne	.L376
	movq	24(%rbx), %rdi
.L375:
	movq	%r12, %r9
	xorl	%eax, %eax
	salq	$4, %r9
	addq	%r9, %r12
	addq	%r12, %rdi
	movq	%rdi, 40(%rbx)
	jmp	.L368
	.p2align 4,,10
	.p2align 3
.L371:
	movq	%r10, %r14
	movq	%r13, %rax
	movq	%r12, %rdx
	salq	$4, %rax
	subq	%r12, %r14
	salq	$4, %rdx
	movq	%r14, %rsi
	addq	%r13, %rax
	addq	%r12, %rdx
	salq	$4, %rsi
	addq	%r14, %rsi
	addq	%rdi, %rsi
	addq	%rax, %rdi
	call	memmove@PLT
	movq	48(%rbx), %rax
	movq	(%rax), %rax
	movq	7(%rax), %rsi
	testl	%r12d, %r12d
	je	.L374
	leal	16(,%r13,8), %edx
	leaq	-1(%rsi), %rax
	movl	$4, %r9d
	movl	%r12d, %r8d
	leal	16(,%r14,8), %ecx
	movslq	%edx, %rdx
	movslq	%ecx, %rcx
	addq	%rax, %rdx
	addq	%rax, %rcx
	movq	8(%rbx), %rax
	leaq	37592(%rax), %rdi
	call	_ZN2v88internal4Heap9MoveRangeENS0_10HeapObjectENS0_14FullObjectSlotES3_iNS0_16WriteBarrierModeE@PLT
	movq	48(%rbx), %rax
	movq	(%rax), %rax
	movq	7(%rax), %rsi
.L374:
	movq	24(%rbx), %rdi
	movq	40(%rbx), %rdx
	subq	%rdi, %rdx
	imull	$-252645135, %edx, %edx
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L423:
	leal	16(,%r15,8), %edx
	leaq	-1(%rsi), %rax
	movl	$4, %r9d
	movl	%r12d, %r8d
	leal	16(,%r13,8), %ecx
	movslq	%edx, %rdx
	movslq	%ecx, %rcx
	addq	%rax, %rdx
	addq	%rax, %rcx
	movq	8(%rbx), %rax
	leaq	37592(%rax), %rdi
	call	_ZN2v88internal4Heap9MoveRangeENS0_10HeapObjectENS0_14FullObjectSlotES3_iNS0_16WriteBarrierModeE@PLT
	movq	48(%rbx), %rax
	movq	(%rax), %rax
	movq	7(%rax), %rsi
	jmp	.L389
	.p2align 4,,10
	.p2align 3
.L420:
	cmpb	$0, 2(%rdx)
	js	.L425
	movl	$5, %r9d
	movl	$6, %r11d
	movl	$7, %r10d
	movl	$4, %edi
	movl	$3, %esi
	movl	$2, %r15d
	jmp	.L384
	.p2align 4,,10
	.p2align 3
.L421:
	cmpb	$0, 2(%rdx)
	movq	%rdi, %rsi
	jns	.L385
	cmpb	$0, 3(%rdx)
	movq	%r9, %rsi
	jns	.L385
	cmpb	$0, 4(%rdx)
	movq	%r10, %rsi
	cmovns	%r11, %rsi
	jmp	.L385
	.p2align 4,,10
	.p2align 3
.L422:
	cmpb	$0, 2(%rdi)
	movl	$2, %edx
	jns	.L382
	cmpb	$0, 3(%rdi)
	movl	$3, %edx
	jns	.L382
	movsbq	4(%rdi), %rdx
	shrq	$63, %rdx
	addq	$4, %rdx
	jmp	.L382
	.p2align 4,,10
	.p2align 3
.L425:
	cmpb	$0, 3(%rdx)
	js	.L426
	movl	$6, %r9d
	movl	$7, %r11d
	movl	$8, %r10d
	movl	$5, %edi
	movl	$4, %esi
	movl	$3, %r15d
	jmp	.L384
	.p2align 4,,10
	.p2align 3
.L426:
	movsbq	4(%rdx), %r15
	movq	%r15, %r9
	movq	%r15, %r11
	movq	%r15, %r10
	movq	%r15, %rdi
	sarq	$63, %r9
	movq	%r15, %rsi
	sarq	$63, %r11
	sarq	$63, %r10
	sarq	$63, %rdi
	notq	%r9
	notq	%r11
	sarq	$63, %rsi
	sarq	$63, %r15
	notq	%r10
	notq	%rdi
	notq	%rsi
	notq	%r15
	addq	$8, %r9
	addq	$9, %r11
	addq	$10, %r10
	addq	$7, %rdi
	addq	$6, %rsi
	addq	$5, %r15
	jmp	.L384
.L424:
	call	__stack_chk_fail@PLT
.L419:
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20109:
	.size	_ZN2v88internal4wasm10ThreadImpl8DoReturnEPNS1_7DecoderEPPNS1_15InterpreterCodeEPmS8_m, .-_ZN2v88internal4wasm10ThreadImpl8DoReturnEPNS1_7DecoderEPPNS1_15InterpreterCodeEPmS8_m
	.section	.text._ZN2v88internal4wasm10ThreadImpl12DoReturnCallEPNS1_7DecoderEPNS1_15InterpreterCodeEPmS7_,"axG",@progbits,_ZN2v88internal4wasm10ThreadImpl12DoReturnCallEPNS1_7DecoderEPNS1_15InterpreterCodeEPmS7_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm10ThreadImpl12DoReturnCallEPNS1_7DecoderEPNS1_15InterpreterCodeEPmS7_
	.type	_ZN2v88internal4wasm10ThreadImpl12DoReturnCallEPNS1_7DecoderEPNS1_15InterpreterCodeEPmS7_, @function
_ZN2v88internal4wasm10ThreadImpl12DoReturnCallEPNS1_7DecoderEPNS1_15InterpreterCodeEPmS7_:
.LFB20111:
	.cfi_startproc
	endbr64
	movabsq	$-1085102592571150095, %r9
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$152, %rsp
	movq	32(%rbx), %r13
	movq	40(%rbx), %rsi
	movq	%rcx, -168(%rbp)
	movq	%r8, -136(%rbp)
	movq	32(%rdx), %rdx
	movq	%r13, %r10
	subq	24(%r12), %rdx
	subq	%rsi, %r10
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	80(%r12), %rax
	movl	56(%rax), %edi
	movq	24(%rbx), %rax
	addq	%rdi, %rdx
	movq	%rsi, %rdi
	movq	%r10, %rsi
	subq	%rax, %rdi
	imulq	%r9, %rsi
	imulq	%r9, %rdi
	movq	%rdi, %rcx
	movq	%rdi, %r8
	cmpq	%rsi, %rdx
	ja	.L510
.L429:
	addq	$1, 112(%rbx)
	movq	(%r12), %rsi
	movl	%ecx, %edx
	movq	%r8, %r13
	movq	72(%rbx), %r14
	movq	(%rsi), %rsi
	subl	-8(%r14), %ecx
	movq	8(%rsi), %r11
	subq	%rcx, %r13
	testq	%r11, %r11
	je	.L476
	cmpq	%rcx, %r11
	jne	.L448
.L476:
	movq	48(%rbx), %rcx
	movq	(%rcx), %rcx
	movq	7(%rcx), %rsi
.L450:
	addq	%r11, %r13
	cmpl	%edx, %r13d
	jge	.L452
	movl	%r13d, %edi
	movq	%rsi, %rcx
	leal	16(,%r13,8), %eax
	notl	%edi
	andq	$-262144, %rcx
	cltq
	addl	%edi, %edx
	movslq	%r13d, %rdi
	leaq	-1(%rsi,%rax), %rax
	addq	$24, %rcx
	addq	%rdi, %rdx
	leaq	23(%rsi,%rdx,8), %rsi
	.p2align 4,,10
	.p2align 3
.L453:
	movq	(%rcx), %rdx
	addq	$8, %rax
	movq	-37496(%rdx), %rdx
	movq	%rdx, -8(%rax)
	cmpq	%rsi, %rax
	jne	.L453
	movq	24(%rbx), %rax
.L452:
	movq	%r13, %rdi
	movq	-136(%rbp), %rcx
	movb	$0, -72(%rbp)
	salq	$4, %rdi
	movl	$0, -92(%rbp)
	addq	%rdi, %r13
	movl	$0, -96(%rbp)
	addq	%r13, %rax
	movq	%rax, 40(%rbx)
	movq	72(%r12), %rax
	subq	64(%r12), %rax
	movq	%rax, (%rcx)
	movq	72(%r12), %rax
	movdqu	64(%r12), %xmm0
	movq	48(%r15), %rdx
	movl	$0, 32(%r15)
	movq	%rax, 24(%r15)
	leaq	-72(%rbp), %rax
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, -88(%rbp)
	movl	$0, 40(%r15)
	movq	$0, 56(%r15)
	movups	%xmm0, 8(%r15)
	movb	$0, (%rdx)
	movq	-88(%rbp), %rdx
	movq	$0, -80(%rbp)
	movb	$0, (%rdx)
	movq	-88(%rbp), %rdi
	cmpq	%rax, %rdi
	je	.L454
	movq	%r11, -136(%rbp)
	call	_ZdlPv@PLT
	movq	-136(%rbp), %r11
.L454:
	movabsq	$-1085102592571150095, %r13
	movq	%r12, -24(%r14)
	movq	$0, -16(%r14)
	movq	40(%rbx), %rax
	subq	24(%rbx), %rax
	imulq	%r13, %rax
	subq	%r11, %rax
	movq	%rax, -8(%r14)
	movq	32(%r12), %rax
	movq	24(%r12), %r15
	movq	%rax, -136(%rbp)
	cmpq	%rax, %r15
	je	.L470
	movq	%r13, -176(%rbp)
	.p2align 4,,10
	.p2align 3
.L469:
	movzbl	(%r15), %ecx
	cmpb	$9, %cl
	ja	.L456
	leaq	.L458(%rip), %rdi
	movzbl	%cl, %eax
	movslq	(%rdi,%rax,4), %rax
	addq	%rdi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm10ThreadImpl12DoReturnCallEPNS1_7DecoderEPNS1_15InterpreterCodeEPmS7_,"aG",@progbits,_ZN2v88internal4wasm10ThreadImpl12DoReturnCallEPNS1_7DecoderEPNS1_15InterpreterCodeEPmS7_,comdat
	.align 4
	.align 4
.L458:
	.long	.L456-.L458
	.long	.L461-.L458
	.long	.L459-.L458
	.long	.L461-.L458
	.long	.L459-.L458
	.long	.L459-.L458
	.long	.L457-.L458
	.long	.L457-.L458
	.long	.L456-.L458
	.long	.L457-.L458
	.section	.text._ZN2v88internal4wasm10ThreadImpl12DoReturnCallEPNS1_7DecoderEPNS1_15InterpreterCodeEPmS7_,"axG",@progbits,_ZN2v88internal4wasm10ThreadImpl12DoReturnCallEPNS1_7DecoderEPNS1_15InterpreterCodeEPmS7_,comdat
	.p2align 4,,10
	.p2align 3
.L461:
	pxor	%xmm0, %xmm0
	movups	%xmm0, -95(%rbp)
	movq	-95(%rbp), %rax
	movq	-87(%rbp), %rdx
.L464:
	movq	%rdx, -87(%rbp)
	movzbl	-80(%rbp), %edx
	movb	%cl, -96(%rbp)
	movq	40(%rbx), %rsi
	movq	%rax, -95(%rbp)
	movdqa	-96(%rbp), %xmm4
	movb	%dl, -112(%rbp)
	movaps	%xmm4, -128(%rbp)
.L474:
	movq	%rax, -127(%rbp)
	movzbl	-112(%rbp), %eax
	leaq	17(%rsi), %rdx
	addq	$1, %r15
	movb	%cl, -128(%rbp)
	movdqa	-128(%rbp), %xmm3
	movq	%rdx, 40(%rbx)
	movb	%al, 16(%rsi)
	movups	%xmm3, (%rsi)
	cmpq	%r15, -136(%rbp)
	jne	.L469
.L470:
	movl	8(%r12), %eax
	movq	-168(%rbp), %rbx
	movq	%rax, -16(%r14)
	movq	%rax, (%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L511
	addq	$152, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L459:
	.cfi_restore_state
	xorl	%eax, %eax
	xorl	%edx, %edx
	jmp	.L464
	.p2align 4,,10
	.p2align 3
.L457:
	movq	40(%rbx), %rax
	subq	24(%rbx), %rax
	movq	$0, -87(%rbp)
	movzbl	-80(%rbp), %ecx
	movq	8(%rbx), %rdx
	movb	$6, -96(%rbp)
	imulq	-176(%rbp), %rax
	movq	$0, -95(%rbp)
	movdqa	-96(%rbp), %xmm2
	movq	104(%rdx), %rdx
	movb	%cl, -112(%rbp)
	movq	48(%rbx), %rcx
	leal	16(,%rax,8), %eax
	movaps	%xmm2, -128(%rbp)
	movq	(%rcx), %rcx
	cltq
	movq	7(%rcx), %rdi
	leaq	-1(%rdi,%rax), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L471
	movq	%rdx, %r13
	andq	$-262144, %r13
	movq	8(%r13), %rax
	testl	$262144, %eax
	je	.L467
	movq	%rdx, -160(%rbp)
	movq	%rsi, -152(%rbp)
	movq	%rdi, -144(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r13), %rax
	movq	-160(%rbp), %rdx
	movq	-152(%rbp), %rsi
	movq	-144(%rbp), %rdi
.L467:
	testb	$24, %al
	je	.L471
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L471
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L471:
	movq	40(%rbx), %rsi
	xorl	%eax, %eax
	movl	$6, %ecx
	jmp	.L474
	.p2align 4,,10
	.p2align 3
.L448:
	subq	%r11, %r8
	movq	%r13, %rdi
	movq	%r11, %rdx
	movq	%r11, -144(%rbp)
	movq	%r8, %rsi
	salq	$4, %rdi
	movq	%r8, -152(%rbp)
	salq	$4, %rsi
	salq	$4, %rdx
	addq	%r13, %rdi
	addq	%r8, %rsi
	addq	%r11, %rdx
	addq	%rax, %rdi
	addq	%rax, %rsi
	call	memmove@PLT
	movq	48(%rbx), %rax
	movq	-144(%rbp), %r11
	movq	(%rax), %rax
	movq	7(%rax), %rsi
	testl	%r11d, %r11d
	jne	.L512
.L451:
	movq	24(%rbx), %rax
	movq	40(%rbx), %rdx
	subq	%rax, %rdx
	imull	$-252645135, %edx, %edx
	jmp	.L450
	.p2align 4,,10
	.p2align 3
.L512:
	movq	-152(%rbp), %r8
	leaq	-1(%rsi), %rax
	leal	16(,%r13,8), %edx
	movl	$4, %r9d
	movslq	%edx, %rdx
	leal	16(,%r8,8), %ecx
	addq	%rax, %rdx
	movl	%r11d, %r8d
	movslq	%ecx, %rcx
	addq	%rax, %rcx
	movq	8(%rbx), %rax
	leaq	37592(%rax), %rdi
	call	_ZN2v88internal4Heap9MoveRangeENS0_10HeapObjectENS0_14FullObjectSlotES3_iNS0_16WriteBarrierModeE@PLT
	movq	48(%rbx), %rax
	movq	-144(%rbp), %r11
	movq	(%rax), %rax
	movq	7(%rax), %rsi
	jmp	.L451
	.p2align 4,,10
	.p2align 3
.L510:
	subq	%rax, %r13
	addq	%rdx, %rdi
	imulq	%r13, %r9
	movq	%r9, %r14
	call	_ZN2v84base4bits21RoundUpToPowerOfTwo64Em@PLT
	movq	%rax, %rdx
	leaq	(%r14,%r14), %rax
	cmpq	%rdx, %rax
	cmovb	%rdx, %rax
	movl	$8, %edx
	movq	%rdx, %rcx
	movabsq	$542551296285575047, %rdx
	cmpq	$8, %rax
	cmovnb	%rax, %rcx
	cmpq	%rdx, %rax
	movq	%rcx, -144(%rbp)
	movq	%rcx, %rax
	jbe	.L430
	salq	$4, %rax
	movq	$-1, %rdi
	addq	%rcx, %rax
	movq	%rax, -152(%rbp)
.L431:
	call	_Znam@PLT
	movq	-144(%rbp), %rdx
	movq	%rax, %rcx
	subq	$1, %rdx
	js	.L435
	.p2align 4,,10
	.p2align 3
.L436:
	pxor	%xmm0, %xmm0
	subq	$1, %rdx
	movb	$0, (%rax)
	addq	$17, %rax
	movups	%xmm0, -16(%rax)
	cmpq	$-1, %rdx
	jne	.L436
.L435:
	movq	24(%rbx), %r8
	testq	%r14, %r14
	jne	.L513
	movq	40(%rbx), %rax
	movq	%rcx, 24(%rbx)
	addq	%rcx, %rax
	subq	%r8, %rax
	movq	%rax, 40(%rbx)
	testq	%r8, %r8
	jne	.L437
.L438:
	movq	8(%rbx), %r13
	addq	-152(%rbp), %rcx
	movq	%rcx, 32(%rbx)
	movl	-144(%rbp), %edx
	movq	41088(%r13), %rax
	addl	$1, 41104(%r13)
	movq	8(%rbx), %rcx
	subl	%r14d, %edx
	movq	%rax, -160(%rbp)
	movq	41096(%r13), %rax
	movq	41112(%rcx), %rdi
	movq	%rax, -152(%rbp)
	movq	48(%rbx), %rax
	movq	(%rax), %rax
	movq	7(%rax), %r9
	testq	%rdi, %rdi
	je	.L439
	movq	%r9, %rsi
	movl	%edx, -176(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-176(%rbp), %edx
	movq	%rax, %rsi
.L440:
	movq	8(%rbx), %rdi
	xorl	%ecx, %ecx
	call	_ZN2v88internal7Factory21CopyFixedArrayAndGrowENS0_6HandleINS0_10FixedArrayEEEiNS0_14AllocationTypeE@PLT
	movq	-144(%rbp), %rdi
	movq	(%rax), %r9
	cmpl	%edi, %r14d
	jge	.L442
	movl	%r14d, %ecx
	movq	%r9, %rsi
	leal	16(,%r14,8), %edx
	notl	%ecx
	andq	$-262144, %rsi
	movslq	%edx, %rdx
	addl	%ecx, %edi
	movslq	%r14d, %rcx
	leaq	-1(%r9,%rdx), %rdx
	addq	$24, %rsi
	addq	%rdi, %rcx
	leaq	23(%r9,%rcx,8), %rdi
	.p2align 4,,10
	.p2align 3
.L443:
	movq	(%rsi), %rcx
	addq	$8, %rdx
	movq	-37496(%rcx), %rcx
	movq	%rcx, -8(%rdx)
	cmpq	%rdx, %rdi
	jne	.L443
	movq	(%rax), %r9
.L442:
	movq	48(%rbx), %rax
	movq	(%rax), %r14
	movq	%r9, 7(%r14)
	leaq	7(%r14), %rsi
	testb	$1, %r9b
	je	.L447
	movq	%r9, %rax
	andq	$-262144, %rax
	movq	8(%rax), %rdx
	movq	%rax, -144(%rbp)
	testl	$262144, %edx
	jne	.L514
.L445:
	andl	$24, %edx
	je	.L447
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L447
	movq	%r9, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L447:
	movq	-160(%rbp), %rax
	subl	$1, 41104(%r13)
	movq	%rax, 41088(%r13)
	movq	-152(%rbp), %rax
	cmpq	41096(%r13), %rax
	je	.L473
	movq	%rax, 41096(%r13)
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L473:
	movq	24(%rbx), %rax
	movq	40(%rbx), %rcx
	movabsq	$-1085102592571150095, %rdx
	subq	%rax, %rcx
	imulq	%rdx, %rcx
	movq	%rcx, %r8
	jmp	.L429
	.p2align 4,,10
	.p2align 3
.L513:
	movq	%r8, %rsi
	movq	%rcx, %rdi
	movq	%r13, %rdx
	movq	%r8, -160(%rbp)
	call	memcpy@PLT
	movq	-160(%rbp), %r8
	movq	%rax, %rcx
	movq	40(%rbx), %rax
	movq	%rcx, 24(%rbx)
	addq	%rcx, %rax
	subq	%r8, %rax
	movq	%rax, 40(%rbx)
.L437:
	movq	%r8, %rdi
	call	_ZdaPv@PLT
	movq	24(%rbx), %rcx
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L456:
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L514:
	movq	%r9, %rdx
	movq	%r14, %rdi
	movq	%r9, -184(%rbp)
	movq	%rsi, -176(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-144(%rbp), %rax
	movq	-184(%rbp), %r9
	movq	-176(%rbp), %rsi
	movq	8(%rax), %rdx
	jmp	.L445
	.p2align 4,,10
	.p2align 3
.L430:
	movq	%rcx, %rdi
	salq	$4, %rdi
	addq	%rcx, %rdi
	movq	%rdi, -152(%rbp)
	jmp	.L431
	.p2align 4,,10
	.p2align 3
.L439:
	movq	41088(%rcx), %rsi
	cmpq	41096(%rcx), %rsi
	je	.L515
.L441:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rcx)
	movq	%r9, (%rsi)
	jmp	.L440
.L515:
	movq	%rcx, %rdi
	movq	%r9, -192(%rbp)
	movl	%edx, -184(%rbp)
	movq	%rcx, -176(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-192(%rbp), %r9
	movl	-184(%rbp), %edx
	movq	-176(%rbp), %rcx
	movq	%rax, %rsi
	jmp	.L441
.L511:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20111:
	.size	_ZN2v88internal4wasm10ThreadImpl12DoReturnCallEPNS1_7DecoderEPNS1_15InterpreterCodeEPmS7_, .-_ZN2v88internal4wasm10ThreadImpl12DoReturnCallEPNS1_7DecoderEPNS1_15InterpreterCodeEPmS7_
	.section	.text._ZN2v88internal4wasm10ThreadImpl16EffectiveAddressEj,"axG",@progbits,_ZN2v88internal4wasm10ThreadImpl16EffectiveAddressEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm10ThreadImpl16EffectiveAddressEj
	.type	_ZN2v88internal4wasm10ThreadImpl16EffectiveAddressEj, @function
_ZN2v88internal4wasm10ThreadImpl16EffectiveAddressEj:
.LFB20113:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	(%rax), %rax
	andl	39(%rax), %esi
	addq	23(%rax), %rsi
	movq	%rsi, %rax
	ret
	.cfi_endproc
.LFE20113:
	.size	_ZN2v88internal4wasm10ThreadImpl16EffectiveAddressEj, .-_ZN2v88internal4wasm10ThreadImpl16EffectiveAddressEj
	.section	.text._ZN2v88internal4wasm10ThreadImpl12GetGlobalPtrEPKNS1_10WasmGlobalE,"axG",@progbits,_ZN2v88internal4wasm10ThreadImpl12GetGlobalPtrEPKNS1_10WasmGlobalE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm10ThreadImpl12GetGlobalPtrEPKNS1_10WasmGlobalE
	.type	_ZN2v88internal4wasm10ThreadImpl12GetGlobalPtrEPKNS1_10WasmGlobalE, @function
_ZN2v88internal4wasm10ThreadImpl12GetGlobalPtrEPKNS1_10WasmGlobalE:
.LFB20251:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdx
	cmpb	$0, 1(%rsi)
	movl	24(%rsi), %eax
	movq	(%rdx), %rdx
	je	.L518
	cmpb	$0, 28(%rsi)
	jne	.L520
.L518:
	addq	103(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L520:
	movq	111(%rdx), %rdx
	movq	(%rdx,%rax,8), %rax
	ret
	.cfi_endproc
.LFE20251:
	.size	_ZN2v88internal4wasm10ThreadImpl12GetGlobalPtrEPKNS1_10WasmGlobalE, .-_ZN2v88internal4wasm10ThreadImpl12GetGlobalPtrEPKNS1_10WasmGlobalE
	.section	.text._ZN2v88internal4wasm10ThreadImpl16DoThrowExceptionEPKNS1_13WasmExceptionEj,"axG",@progbits,_ZN2v88internal4wasm10ThreadImpl16DoThrowExceptionEPKNS1_13WasmExceptionEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm10ThreadImpl16DoThrowExceptionEPKNS1_13WasmExceptionEj
	.type	_ZN2v88internal4wasm10ThreadImpl16DoThrowExceptionEPKNS1_13WasmExceptionEj, @function
_ZN2v88internal4wasm10ThreadImpl16DoThrowExceptionEPKNS1_13WasmExceptionEj:
.LFB20259:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	41088(%rax), %rcx
	addl	$1, 41104(%rax)
	movq	%rax, -104(%rbp)
	movq	8(%rdi), %r13
	movq	%rcx, -112(%rbp)
	movq	41096(%rax), %rcx
	movq	16(%rdi), %rax
	movq	%rcx, -120(%rbp)
	movq	(%rax), %rdx
	leal	16(,%r9,8), %eax
	cltq
	movq	223(%rdx), %rdx
	movq	-1(%rax,%rdx), %rsi
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L522
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r12
.L523:
	movq	%rbx, %rdi
	call	_ZN2v88internal20WasmExceptionPackage14GetEncodedSizeEPKNS0_4wasm13WasmExceptionE@PLT
	movq	8(%r15), %rdi
	movq	%r12, %rsi
	movl	%eax, %edx
	call	_ZN2v88internal20WasmExceptionPackage3NewEPNS0_7IsolateENS0_6HandleINS0_16WasmExceptionTagEEEi@PLT
	movq	8(%r15), %rdi
	movq	%rax, %rsi
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal20WasmExceptionPackage18GetExceptionValuesEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE@PLT
	movq	40(%r15), %rdi
	movq	(%rbx), %rcx
	movabsq	$-1085102592571150095, %r9
	movq	%rax, %r13
	movq	24(%r15), %rax
	movq	%rdi, %rdx
	movq	8(%rcx), %rsi
	subq	%rax, %rdx
	imulq	%r9, %rdx
	subq	%rsi, %rdx
	testq	%rsi, %rsi
	je	.L525
	movq	%rdx, %r14
	leal	0(,%rdx,8), %ebx
	movq	%r15, %rsi
	xorl	%r12d, %r12d
	salq	$4, %r14
	movl	%ebx, -88(%rbp)
	leaq	.L533(%rip), %r9
	xorl	%ebx, %ebx
	addq	%rdx, %r14
	movq	%r14, %r15
	movq	%rcx, %r14
	movq	%rsi, %rcx
	.p2align 4,,10
	.p2align 3
.L543:
	addq	%r15, %rax
	cmpb	$6, (%rax)
	je	.L526
	movdqu	(%rax), %xmm0
	movzbl	16(%rax), %eax
	movb	%al, -64(%rbp)
	movq	16(%r14), %rax
	movaps	%xmm0, -80(%rbp)
	addq	%r12, %rax
	addq	(%r14), %rax
	cmpb	$9, (%rax)
	ja	.L531
.L563:
	movzbl	(%rax), %eax
	movslq	(%r9,%rax,4), %rax
	addq	%r9, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm10ThreadImpl16DoThrowExceptionEPKNS1_13WasmExceptionEj,"aG",@progbits,_ZN2v88internal4wasm10ThreadImpl16DoThrowExceptionEPKNS1_13WasmExceptionEj,comdat
	.align 4
	.align 4
.L533:
	.long	.L531-.L533
	.long	.L536-.L533
	.long	.L537-.L533
	.long	.L536-.L533
	.long	.L537-.L533
	.long	.L534-.L533
	.long	.L532-.L533
	.long	.L532-.L533
	.long	.L531-.L533
	.long	.L532-.L533
	.section	.text._ZN2v88internal4wasm10ThreadImpl16DoThrowExceptionEPKNS1_13WasmExceptionEj,"axG",@progbits,_ZN2v88internal4wasm10ThreadImpl16DoThrowExceptionEPKNS1_13WasmExceptionEj,comdat
	.p2align 4,,10
	.p2align 3
.L532:
	movq	-79(%rbp), %rax
	movq	0(%r13), %rdi
	leal	1(%rbx), %r10d
	movq	(%rax), %rdx
	leal	16(,%rbx,8), %eax
	cltq
	leaq	-1(%rdi,%rax), %rbx
	movq	%rdx, (%rbx)
	testb	$1, %dl
	je	.L546
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	8(%rax), %r8
	movq	%rax, -96(%rbp)
	testl	$262144, %r8d
	je	.L541
	movq	%rbx, %rsi
	movq	%rcx, -160(%rbp)
	movl	%r10d, -148(%rbp)
	movq	%rdx, -144(%rbp)
	movq	%rdi, -136(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-96(%rbp), %rax
	movq	-160(%rbp), %rcx
	leaq	.L533(%rip), %r9
	movl	-148(%rbp), %r10d
	movq	-144(%rbp), %rdx
	movq	8(%rax), %r8
	movq	-136(%rbp), %rdi
.L541:
	andl	$24, %r8d
	je	.L546
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L561
.L546:
	movl	%r10d, %ebx
	jmp	.L539
	.p2align 4,,10
	.p2align 3
.L536:
	movl	-79(%rbp), %eax
	leal	2(%rbx), %esi
	movq	0(%r13), %r10
	leal	0(,%rsi,8), %edx
	movl	%eax, %edi
	movslq	%edx, %rdx
	shrl	$16, %edi
	salq	$32, %rdi
	movq	%rdi, -1(%rdx,%r10)
	leal	24(,%rbx,8), %edx
.L560:
	movq	0(%r13), %rdi
	movzwl	%ax, %eax
	movslq	%edx, %rdx
	movl	%esi, %ebx
	salq	$32, %rax
	movq	%rax, -1(%rdx,%rdi)
.L539:
	movq	8(%r14), %rdx
	addq	$1, %r12
	movq	24(%rcx), %rax
	addq	$17, %r15
	cmpq	%rdx, %r12
	jb	.L543
	movq	40(%rcx), %rdi
	movq	%rcx, %r15
	movslq	%edx, %rsi
	movq	%rdi, %rcx
	subq	%rax, %rcx
	movabsq	$-1085102592571150095, %rax
	imulq	%rax, %rcx
	movq	48(%r15), %rax
	movq	(%rax), %rax
	subq	%rsi, %rcx
	addl	%ecx, %edx
	movq	7(%rax), %r9
	cmpl	%edx, %ecx
	jge	.L525
	movl	%ecx, %r10d
	leal	16(,%rcx,8), %eax
	movq	%r9, %rdi
	movslq	%ecx, %rcx
	notl	%r10d
	andq	$-262144, %rdi
	cltq
	addl	%r10d, %edx
	addq	$24, %rdi
	leaq	-1(%r9,%rax), %rax
	addq	%rdx, %rcx
	leaq	23(%r9,%rcx,8), %rcx
	.p2align 4,,10
	.p2align 3
.L544:
	movq	(%rdi), %rdx
	addq	$8, %rax
	movq	-37496(%rdx), %rdx
	movq	%rdx, -8(%rax)
	cmpq	%rax, %rcx
	jne	.L544
	movq	40(%r15), %rdi
.L525:
	movq	%rsi, %rax
	xorl	%edx, %edx
	salq	$4, %rax
	addq	%rax, %rsi
	movq	-128(%rbp), %rax
	subq	%rsi, %rdi
	movq	%rdi, 40(%r15)
	movq	8(%r15), %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	8(%r15), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl15HandleExceptionEPNS0_7IsolateE
	movq	-112(%rbp), %rcx
	testl	%eax, %eax
	movq	-104(%rbp), %rax
	sete	%r12b
	movq	%rcx, 41088(%rax)
	movq	-120(%rbp), %rcx
	subl	$1, 41104(%rax)
	cmpq	41096(%rax), %rcx
	je	.L521
	movq	%rcx, 41096(%rax)
	movq	%rax, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L521:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L562
	addq	$120, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L537:
	.cfi_restore_state
	movq	-79(%rbp), %rax
	movq	0(%r13), %rdi
	leal	16(,%rbx,8), %edx
	movslq	%edx, %rdx
	movq	%rax, %rsi
	shrq	$48, %rsi
	salq	$32, %rsi
	movq	%rsi, -1(%rdx,%rdi)
	leal	24(,%rbx,8), %edx
	movabsq	$281470681743360, %rsi
	movslq	%edx, %rdx
	andq	%rax, %rsi
	movq	0(%r13), %rdi
	movq	%rsi, -1(%rdx,%rdi)
	leal	4(%rbx), %esi
	movl	%eax, %edi
	leal	0(,%rsi,8), %edx
	shrl	$16, %edi
	movq	0(%r13), %r10
	movslq	%edx, %rdx
	salq	$32, %rdi
	movq	%rdi, -1(%rdx,%r10)
	leal	40(,%rbx,8), %edx
	jmp	.L560
	.p2align 4,,10
	.p2align 3
.L534:
	movq	-79(%rbp), %rdx
	movq	0(%r13), %r10
	leal	16(,%rbx,8), %esi
	movabsq	$281470681743360, %r8
	movslq	%esi, %rsi
	movq	-71(%rbp), %rax
	movl	%edx, %edi
	shrl	$16, %edi
	salq	$32, %rdi
	movq	%rdi, -1(%rsi,%r10)
	leal	24(,%rbx,8), %esi
	movzwl	%dx, %edi
	movslq	%esi, %rsi
	salq	$32, %rdi
	movq	0(%r13), %r10
	movq	%rdi, -1(%rsi,%r10)
	movq	%rdx, %rdi
	leal	32(,%rbx,8), %esi
	shrq	$48, %rdi
	movslq	%esi, %rsi
	movq	0(%r13), %r10
	salq	$32, %rdi
	movq	%rdi, -1(%rsi,%r10)
	leal	40(,%rbx,8), %esi
	movabsq	$281470681743360, %r10
	andq	%r10, %rdx
	movslq	%esi, %rsi
	movq	0(%r13), %rdi
	movq	%rdx, -1(%rsi,%rdi)
	movl	%eax, %esi
	leal	48(,%rbx,8), %edx
	shrl	$16, %esi
	movslq	%edx, %rdx
	movq	0(%r13), %rdi
	salq	$32, %rsi
	movq	%rsi, -1(%rdx,%rdi)
	leal	56(,%rbx,8), %edx
	movzwl	%ax, %esi
	movslq	%edx, %rdx
	salq	$32, %rsi
	movq	0(%r13), %rdi
	movq	%rsi, -1(%rdx,%rdi)
	leal	8(%rbx), %esi
	movq	%rax, %rdi
	andq	%r8, %rax
	leal	0(,%rsi,8), %edx
	shrq	$48, %rdi
	movq	0(%r13), %r10
	movslq	%edx, %rdx
	salq	$32, %rdi
	movq	%rdi, -1(%rdx,%r10)
	leal	72(,%rbx,8), %edx
	movl	%esi, %ebx
	movslq	%edx, %rdx
	movq	0(%r13), %rdi
	movq	%rax, -1(%rdx,%rdi)
	jmp	.L539
	.p2align 4,,10
	.p2align 3
.L526:
	movq	48(%rcx), %rax
	movq	8(%rcx), %rdx
	movq	(%rax), %rsi
	movl	-88(%rbp), %eax
	movq	7(%rsi), %rsi
	leal	16(%rax,%r12,8), %eax
	cltq
	movq	-1(%rax,%rsi), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L528
	movq	%rcx, -96(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-96(%rbp), %rcx
	leaq	.L533(%rip), %r9
.L529:
	movq	%rax, -79(%rbp)
	movq	16(%r14), %rax
	movq	$0, -71(%rbp)
	addq	%r12, %rax
	addq	(%r14), %rax
	cmpb	$9, (%rax)
	jbe	.L563
	.p2align 4,,10
	.p2align 3
.L531:
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L528:
	movq	41088(%rdx), %rax
	cmpq	41096(%rdx), %rax
	je	.L564
.L530:
	leaq	8(%rax), %rdi
	movq	%rdi, 41088(%rdx)
	movq	%rsi, (%rax)
	jmp	.L529
.L561:
	movq	%rbx, %rsi
	movq	%rcx, -136(%rbp)
	movl	%r10d, -96(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movl	-96(%rbp), %r10d
	movq	-136(%rbp), %rcx
	leaq	.L533(%rip), %r9
	movl	%r10d, %ebx
	jmp	.L539
.L522:
	movq	41088(%r13), %r12
	cmpq	%r12, 41096(%r13)
	je	.L565
.L524:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r12)
	jmp	.L523
.L564:
	movq	%rdx, %rdi
	movq	%rcx, -144(%rbp)
	movq	%rsi, -136(%rbp)
	movq	%rdx, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-144(%rbp), %rcx
	movq	-96(%rbp), %rdx
	leaq	.L533(%rip), %r9
	movq	-136(%rbp), %rsi
	jmp	.L530
.L565:
	movq	%r13, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L524
.L562:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20259:
	.size	_ZN2v88internal4wasm10ThreadImpl16DoThrowExceptionEPKNS1_13WasmExceptionEj, .-_ZN2v88internal4wasm10ThreadImpl16DoThrowExceptionEPKNS1_13WasmExceptionEj
	.section	.text._ZN2v88internal4wasm10ThreadImpl17DoUnpackExceptionEPKNS1_13WasmExceptionENS0_6HandleINS0_6ObjectEEE,"axG",@progbits,_ZN2v88internal4wasm10ThreadImpl17DoUnpackExceptionEPKNS1_13WasmExceptionENS0_6HandleINS0_6ObjectEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm10ThreadImpl17DoUnpackExceptionEPKNS1_13WasmExceptionENS0_6HandleINS0_6ObjectEEE
	.type	_ZN2v88internal4wasm10ThreadImpl17DoUnpackExceptionEPKNS1_13WasmExceptionENS0_6HandleINS0_6ObjectEEE, @function
_ZN2v88internal4wasm10ThreadImpl17DoUnpackExceptionEPKNS1_13WasmExceptionENS0_6HandleINS0_6ObjectEEE:
.LFB20264:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movq	%rdx, %rsi
	subq	$120, %rsp
	movq	8(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal20WasmExceptionPackage18GetExceptionValuesEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE@PLT
	movq	(%rbx), %r12
	cmpq	$0, 8(%r12)
	je	.L566
	movq	%rax, %r10
	xorl	%ecx, %ecx
	leaq	.L570(%rip), %r14
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L567:
	movq	16(%r12), %rax
	addq	%rbx, %rax
	addq	(%r12), %rax
	movzbl	(%rax), %edx
	cmpb	$9, %dl
	ja	.L568
	movzbl	%dl, %eax
	movslq	(%r14,%rax,4), %rax
	addq	%r14, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm10ThreadImpl17DoUnpackExceptionEPKNS1_13WasmExceptionENS0_6HandleINS0_6ObjectEEE,"aG",@progbits,_ZN2v88internal4wasm10ThreadImpl17DoUnpackExceptionEPKNS1_13WasmExceptionENS0_6HandleINS0_6ObjectEEE,comdat
	.align 4
	.align 4
.L570:
	.long	.L568-.L570
	.long	.L573-.L570
	.long	.L572-.L570
	.long	.L573-.L570
	.long	.L572-.L570
	.long	.L571-.L570
	.long	.L569-.L570
	.long	.L569-.L570
	.long	.L568-.L570
	.long	.L569-.L570
	.section	.text._ZN2v88internal4wasm10ThreadImpl17DoUnpackExceptionEPKNS1_13WasmExceptionENS0_6HandleINS0_6ObjectEEE,"axG",@progbits,_ZN2v88internal4wasm10ThreadImpl17DoUnpackExceptionEPKNS1_13WasmExceptionENS0_6HandleINS0_6ObjectEEE,comdat
	.p2align 4,,10
	.p2align 3
.L569:
	movq	8(%r13), %rsi
	movq	(%r10), %rdx
	leal	16(,%rcx,8), %eax
	leal	1(%rcx), %r15d
	cltq
	movq	-1(%rax,%rdx), %r8
	movq	41112(%rsi), %rdi
	testq	%rdi, %rdi
	je	.L577
	movq	%r8, %rsi
	movq	%r10, -120(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-120(%rbp), %r10
	movq	%rax, %rdx
.L578:
	movq	40(%r13), %rax
	subq	24(%r13), %rax
	movb	$6, -80(%rbp)
	movabsq	$-1085102592571150095, %rsi
	movq	$0, -71(%rbp)
	movzbl	-64(%rbp), %ecx
	imulq	%rsi, %rax
	movq	$0, -79(%rbp)
	movq	(%rdx), %rdx
	movb	%cl, -96(%rbp)
	movq	48(%r13), %rcx
	leal	16(,%rax,8), %eax
	movdqa	-80(%rbp), %xmm3
	movq	(%rcx), %rcx
	cltq
	movaps	%xmm3, -112(%rbp)
	movq	7(%rcx), %rdi
	leaq	-1(%rdi,%rax), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L585
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -120(%rbp)
	testl	$262144, %eax
	je	.L582
	movq	%r10, -152(%rbp)
	movq	%rdx, -144(%rbp)
	movq	%rsi, -136(%rbp)
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-120(%rbp), %rcx
	movq	-128(%rbp), %rdi
	movq	-152(%rbp), %r10
	movq	-144(%rbp), %rdx
	movq	8(%rcx), %rax
	movq	-136(%rbp), %rsi
.L582:
	testb	$24, %al
	je	.L585
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L585
	movq	%r10, -120(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-120(%rbp), %r10
	.p2align 4,,10
	.p2align 3
.L585:
	movq	40(%r13), %rsi
	movl	%r15d, %ecx
	xorl	%eax, %eax
	movl	$6, %edx
.L586:
	movq	%rax, -111(%rbp)
	movzbl	-96(%rbp), %eax
	leaq	17(%rsi), %rdi
	addq	$1, %rbx
	movb	%dl, -112(%rbp)
	movdqa	-112(%rbp), %xmm1
	movq	%rdi, 40(%r13)
	movb	%al, 16(%rsi)
	movups	%xmm1, (%rsi)
	cmpq	8(%r12), %rbx
	jb	.L567
.L566:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L599
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L572:
	.cfi_restore_state
	movq	(%r10), %r8
	leal	16(,%rcx,8), %eax
	leal	4(%rcx), %esi
	cltq
	movq	-1(%rax,%r8), %r9
	leal	24(,%rcx,8), %eax
	leal	40(,%rcx,8), %ecx
	cltq
	movslq	%ecx, %rcx
	movq	-1(%rax,%r8), %rdi
	leal	0(,%rsi,8), %eax
	sarq	$32, %r9
	cltq
	sall	$16, %r9d
	movq	-1(%rax,%r8), %rax
	movq	-1(%rcx,%r8), %rcx
	sarq	$32, %rdi
	movzwl	%di, %edi
	sarq	$32, %rax
	sarq	$32, %rcx
	orl	%edi, %r9d
	xorl	%edi, %edi
	sall	$16, %eax
	movzwl	%cx, %ecx
	salq	$32, %r9
	orl	%ecx, %eax
	movl	%esi, %ecx
	orq	%r9, %rax
.L576:
	movq	%rdi, -71(%rbp)
	movzbl	-64(%rbp), %edi
	movb	%dl, -80(%rbp)
	movq	40(%r13), %rsi
	movq	%rax, -79(%rbp)
	movdqa	-80(%rbp), %xmm2
	movb	%dil, -96(%rbp)
	movaps	%xmm2, -112(%rbp)
	jmp	.L586
	.p2align 4,,10
	.p2align 3
.L573:
	leal	2(%rcx), %esi
	movq	(%r10), %rdi
	leal	24(,%rcx,8), %ecx
	pxor	%xmm0, %xmm0
	leal	0(,%rsi,8), %eax
	movslq	%ecx, %rcx
	cltq
	movq	-1(%rax,%rdi), %rax
	movq	-1(%rcx,%rdi), %rcx
	movups	%xmm0, -79(%rbp)
	movq	-71(%rbp), %rdi
	sarq	$32, %rax
	sarq	$32, %rcx
	movzwl	%cx, %ecx
	sall	$16, %eax
	orl	%ecx, %eax
	movl	%esi, %ecx
	movl	%eax, -79(%rbp)
	movq	-79(%rbp), %rax
	jmp	.L576
	.p2align 4,,10
	.p2align 3
.L571:
	movq	(%r10), %r8
	leal	16(,%rcx,8), %eax
	leal	24(,%rcx,8), %esi
	movslq	%esi, %rsi
	cltq
	movq	-1(%rax,%r8), %rax
	movq	-1(%rsi,%r8), %r9
	leal	32(,%rcx,8), %esi
	movslq	%esi, %rsi
	movq	-1(%rsi,%r8), %rdi
	leal	40(,%rcx,8), %esi
	sarq	$32, %rax
	sarq	$32, %r9
	movslq	%esi, %rsi
	sall	$16, %eax
	movzwl	%r9w, %r9d
	movq	-1(%rsi,%r8), %r11
	leal	48(,%rcx,8), %esi
	sarq	$32, %rdi
	orl	%r9d, %eax
	movslq	%esi, %rsi
	sall	$16, %edi
	movq	-1(%rsi,%r8), %rsi
	sarq	$32, %r11
	movzwl	%r11w, %r11d
	movq	%rsi, -120(%rbp)
	leal	56(,%rcx,8), %esi
	orl	%r11d, %edi
	movslq	%esi, %rsi
	salq	$32, %rdi
	movq	-1(%rsi,%r8), %r15
	orq	%rdi, %rax
	movq	%r15, -128(%rbp)
	leal	8(%rcx), %r15d
	leal	72(,%rcx,8), %ecx
	leal	0(,%r15,8), %esi
	movslq	%ecx, %rcx
	movslq	%esi, %rsi
	movq	-1(%rsi,%r8), %rsi
	movq	-1(%rcx,%r8), %rcx
	movq	-120(%rbp), %r8
	movq	-128(%rbp), %rdi
	sarq	$32, %rsi
	sarq	$32, %rcx
	sarq	$32, %r8
	movzwl	%cx, %ecx
	sarq	$32, %rdi
	sall	$16, %esi
	sall	$16, %r8d
	movzwl	%di, %edi
	orl	%ecx, %esi
	orl	%r8d, %edi
	movl	%r15d, %ecx
	salq	$32, %rsi
	orq	%rsi, %rdi
	jmp	.L576
	.p2align 4,,10
	.p2align 3
.L577:
	movq	41088(%rsi), %rdx
	cmpq	41096(%rsi), %rdx
	je	.L600
.L579:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%rsi)
	movq	%r8, (%rdx)
	jmp	.L578
	.p2align 4,,10
	.p2align 3
.L600:
	movq	%rsi, %rdi
	movq	%r10, -136(%rbp)
	movq	%r8, -128(%rbp)
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %r10
	movq	-128(%rbp), %r8
	movq	-120(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L579
	.p2align 4,,10
	.p2align 3
.L568:
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L599:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20264:
	.size	_ZN2v88internal4wasm10ThreadImpl17DoUnpackExceptionEPKNS1_13WasmExceptionENS0_6HandleINS0_6ObjectEEE, .-_ZN2v88internal4wasm10ThreadImpl17DoUnpackExceptionEPKNS1_13WasmExceptionENS0_6HandleINS0_6ObjectEEE
	.section	.text._ZN2v88internal4wasm10ThreadImpl3PopEv,"axG",@progbits,_ZN2v88internal4wasm10ThreadImpl3PopEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm10ThreadImpl3PopEv
	.type	_ZN2v88internal4wasm10ThreadImpl3PopEv, @function
_ZN2v88internal4wasm10ThreadImpl3PopEv:
.LFB20266:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	40(%rsi), %rax
	leaq	-17(%rax), %rdx
	movq	%rdx, 40(%rsi)
	movdqu	-17(%rax), %xmm0
	movzbl	-1(%rax), %ecx
	movzbl	-17(%rax), %eax
	movaps	%xmm0, -64(%rbp)
	movb	%cl, -48(%rbp)
	cmpb	$6, %al
	je	.L602
	movb	%al, -64(%rbp)
	movdqa	-64(%rbp), %xmm1
	movb	%cl, 16(%rdi)
	movups	%xmm1, (%rdi)
.L601:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L609
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L602:
	.cfi_restore_state
	movq	48(%rsi), %rax
	subq	24(%rsi), %rdx
	movq	%rsi, %rbx
	imull	$-252645135, %edx, %edx
	movq	8(%rsi), %r13
	movq	(%rax), %rcx
	leal	16(,%rdx,8), %eax
	movq	7(%rcx), %rdx
	cltq
	movq	-1(%rax,%rdx), %rsi
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L604
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L605:
	movq	48(%rbx), %rdx
	movq	8(%rbx), %rcx
	movq	%rax, 1(%r12)
	movq	40(%rbx), %rax
	subq	24(%rbx), %rax
	movb	$6, (%r12)
	imull	$-252645135, %eax, %eax
	movq	(%rdx), %rdx
	movq	96(%rcx), %rcx
	movq	$0, 9(%r12)
	movq	7(%rdx), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	%rcx, -1(%rax,%rdx)
	jmp	.L601
	.p2align 4,,10
	.p2align 3
.L604:
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L610
.L606:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%rsi, (%rax)
	jmp	.L605
	.p2align 4,,10
	.p2align 3
.L610:
	movq	%r13, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	jmp	.L606
.L609:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20266:
	.size	_ZN2v88internal4wasm10ThreadImpl3PopEv, .-_ZN2v88internal4wasm10ThreadImpl3PopEv
	.section	.text._ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE,"axG",@progbits,_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	.type	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE, @function
_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE:
.LFB20269:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movdqu	16(%rbp), %xmm0
	movq	17(%rbp), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	32(%rbp), %ecx
	movq	40(%rdi), %rax
	movaps	%xmm0, -112(%rbp)
	movzbl	-112(%rbp), %r12d
	movq	24(%rdi), %rsi
	movq	%rdx, -111(%rbp)
	movdqa	-112(%rbp), %xmm1
	movb	%cl, -96(%rbp)
	movb	%cl, -128(%rbp)
	movaps	%xmm1, -144(%rbp)
	cmpb	$6, %r12b
	je	.L625
.L612:
	movq	%rdx, -143(%rbp)
	movzbl	-128(%rbp), %edx
	leaq	17(%rax), %rcx
	movq	%rcx, 40(%rbx)
	movb	%r12b, -144(%rbp)
	movdqa	-144(%rbp), %xmm2
	movb	%dl, 16(%rax)
	movups	%xmm2, (%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L626
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L625:
	.cfi_restore_state
	movq	$0, -71(%rbp)
	movzbl	-64(%rbp), %ecx
	subq	%rsi, %rax
	imull	$-252645135, %eax, %eax
	movb	$6, -80(%rbp)
	movq	(%rdx), %r13
	movb	%cl, -128(%rbp)
	movq	48(%rdi), %rcx
	movq	$0, -79(%rbp)
	movdqa	-80(%rbp), %xmm3
	movq	(%rcx), %rcx
	leal	16(,%rax,8), %eax
	cltq
	movaps	%xmm3, -144(%rbp)
	movq	7(%rcx), %r14
	leaq	-1(%r14,%rax), %r15
	movq	%r13, (%r15)
	testb	$1, %r13b
	je	.L616
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -152(%rbp)
	testl	$262144, %eax
	je	.L614
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-152(%rbp), %rcx
	movq	8(%rcx), %rax
.L614:
	testb	$24, %al
	je	.L616
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L627
	.p2align 4,,10
	.p2align 3
.L616:
	movq	40(%rbx), %rax
	xorl	%edx, %edx
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L627:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L616
.L626:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20269:
	.size	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE, .-_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	.section	.rodata._ZN2v88internal4wasm10ThreadImpl16ExecuteNumericOpENS1_10WasmOpcodeEPNS1_7DecoderEPNS1_15InterpreterCodeEmPi.str1.1,"aMS",@progbits,1
.LC8:
	.string	"InternalBreakpoint"
	.section	.rodata._ZN2v88internal4wasm10ThreadImpl16ExecuteNumericOpENS1_10WasmOpcodeEPNS1_7DecoderEPNS1_15InterpreterCodeEmPi.str1.8,"aMS",@progbits,1
	.align 8
.LC19:
	.string	"Unknown or unimplemented opcode #%d:%s"
	.section	.text._ZN2v88internal4wasm10ThreadImpl16ExecuteNumericOpENS1_10WasmOpcodeEPNS1_7DecoderEPNS1_15InterpreterCodeEmPi,"axG",@progbits,_ZN2v88internal4wasm10ThreadImpl16ExecuteNumericOpENS1_10WasmOpcodeEPNS1_7DecoderEPNS1_15InterpreterCodeEmPi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm10ThreadImpl16ExecuteNumericOpENS1_10WasmOpcodeEPNS1_7DecoderEPNS1_15InterpreterCodeEmPi
	.type	_ZN2v88internal4wasm10ThreadImpl16ExecuteNumericOpENS1_10WasmOpcodeEPNS1_7DecoderEPNS1_15InterpreterCodeEmPi, @function
_ZN2v88internal4wasm10ThreadImpl16ExecuteNumericOpENS1_10WasmOpcodeEPNS1_7DecoderEPNS1_15InterpreterCodeEmPi:
.LFB20121:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	subl	$64512, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$17, %esi
	ja	.L629
	leaq	.L631(%rip), %rdx
	movq	%rdi, %r12
	movq	%r9, %rbx
	movslq	(%rdx,%rsi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm10ThreadImpl16ExecuteNumericOpENS1_10WasmOpcodeEPNS1_7DecoderEPNS1_15InterpreterCodeEmPi,"aG",@progbits,_ZN2v88internal4wasm10ThreadImpl16ExecuteNumericOpENS1_10WasmOpcodeEPNS1_7DecoderEPNS1_15InterpreterCodeEmPi,comdat
	.align 4
	.align 4
.L631:
	.long	.L648-.L631
	.long	.L647-.L631
	.long	.L646-.L631
	.long	.L645-.L631
	.long	.L644-.L631
	.long	.L643-.L631
	.long	.L642-.L631
	.long	.L641-.L631
	.long	.L640-.L631
	.long	.L639-.L631
	.long	.L638-.L631
	.long	.L637-.L631
	.long	.L636-.L631
	.long	.L635-.L631
	.long	.L634-.L631
	.long	.L633-.L631
	.long	.L632-.L631
	.long	.L630-.L631
	.section	.text._ZN2v88internal4wasm10ThreadImpl16ExecuteNumericOpENS1_10WasmOpcodeEPNS1_7DecoderEPNS1_15InterpreterCodeEmPi,"axG",@progbits,_ZN2v88internal4wasm10ThreadImpl16ExecuteNumericOpENS1_10WasmOpcodeEPNS1_7DecoderEPNS1_15InterpreterCodeEmPi,comdat
	.p2align 4,,10
	.p2align 3
.L632:
	movq	64(%rcx), %rax
	movl	$1, %r14d
	leaq	1(%rax,%r8), %rdx
	movzbl	1(%rdx), %eax
	movl	%eax, %esi
	andl	$127, %esi
	testb	%al, %al
	js	.L921
.L817:
	movq	8(%r12), %r15
	movq	41088(%r15), %rax
	addl	$1, 41104(%r15)
	movq	8(%r12), %rdx
	movq	41096(%r15), %r13
	movq	%rax, -136(%rbp)
	movq	16(%r12), %rax
	movq	(%rax), %rcx
	leal	16(,%rsi,8), %eax
	cltq
	movq	199(%rcx), %rcx
	movq	-1(%rax,%rcx), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L818
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L819:
	movq	23(%rsi), %rax
	pxor	%xmm0, %xmm0
	movb	$1, -96(%rbp)
	subq	$32, %rsp
	movups	%xmm0, -95(%rbp)
	movslq	11(%rax), %rax
	movl	%eax, -95(%rbp)
	movdqa	-96(%rbp), %xmm6
	movups	%xmm6, (%rsp)
	jmp	.L920
	.p2align 4,,10
	.p2align 3
.L630:
	movl	$1, -136(%rbp)
	movq	64(%rcx), %rax
	leaq	1(%rax,%r8), %rdx
	movzbl	1(%rdx), %eax
	movl	%eax, %r10d
	andl	$127, %r10d
	testb	%al, %al
	js	.L922
.L822:
	movq	8(%r12), %r15
	movq	41088(%r15), %rax
	addl	$1, 41104(%r15)
	movq	41096(%r15), %r14
	movq	24(%r12), %r11
	movq	%rax, -144(%rbp)
	movq	40(%r12), %rax
	leaq	-17(%rax), %rdx
	movq	%rdx, 40(%r12)
	movdqu	-17(%rax), %xmm7
	movzbl	-1(%rax), %ecx
	movzbl	-17(%rax), %eax
	movaps	%xmm7, -96(%rbp)
	movb	%cl, -80(%rbp)
	cmpb	$6, %al
	je	.L823
	movb	%al, -96(%rbp)
	movdqa	-96(%rbp), %xmm7
	movb	%cl, -112(%rbp)
	movaps	%xmm7, -128(%rbp)
.L824:
	leaq	-17(%rdx), %rax
	movl	-127(%rbp), %r8d
	movq	8(%r12), %rdi
	movq	%rax, 40(%r12)
	movdqu	-17(%rdx), %xmm2
	movzbl	-1(%rdx), %ecx
	cmpb	$6, -17(%rdx)
	movaps	%xmm2, -96(%rbp)
	movb	%cl, -80(%rbp)
	je	.L828
	movq	-95(%rbp), %rcx
.L829:
	leaq	-17(%rax), %rdx
	movq	%rdx, 40(%r12)
	movdqu	-17(%rax), %xmm3
	movzbl	-1(%rax), %esi
	movzbl	-17(%rax), %eax
	movaps	%xmm3, -96(%rbp)
	movb	%sil, -80(%rbp)
	cmpb	$6, %al
	je	.L833
	movb	%al, -96(%rbp)
	movdqa	-96(%rbp), %xmm2
	movb	%sil, -112(%rbp)
	movaps	%xmm2, -128(%rbp)
.L834:
	movq	16(%r12), %rax
	movl	-127(%rbp), %edx
	movq	(%rax), %rsi
	leal	16(,%r10,8), %eax
	cltq
	movq	199(%rsi), %rsi
	movq	-1(%rax,%rsi), %r10
	movq	41112(%rdi), %r11
	testq	%r11, %r11
	je	.L838
	movq	%r10, %rsi
	movq	%r11, %rdi
	movq	%rcx, -168(%rbp)
	movl	%r8d, -160(%rbp)
	movl	%edx, -152(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-152(%rbp), %edx
	movl	-160(%rbp), %r8d
	movq	(%rax), %r10
	movq	-168(%rbp), %rcx
	movq	%rax, %rsi
.L839:
	movq	23(%r10), %rdi
	movslq	11(%rdi), %rax
	cmpl	%edx, 11(%rdi)
	jb	.L911
	subl	%edx, %eax
	movq	8(%r12), %rdi
	cmpl	%r8d, %eax
	jb	.L923
	call	_ZN2v88internal15WasmTableObject4FillEPNS0_7IsolateENS0_6HandleIS1_EEjNS4_INS0_6ObjectEEEj@PLT
	movl	-136(%rbp), %eax
	addl	%eax, (%rbx)
	movl	$1, %eax
.L842:
	movq	-144(%rbp), %rbx
	subl	$1, 41104(%r15)
	movq	%rbx, 41088(%r15)
	cmpq	41096(%r15), %r14
	je	.L628
	movq	%r14, 41096(%r15)
	movq	%r15, %rdi
	movb	%al, -136(%rbp)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movzbl	-136(%rbp), %eax
	.p2align 4,,10
	.p2align 3
.L628:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L924
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L648:
	.cfi_restore_state
	movq	40(%rdi), %rax
	leaq	-17(%rax), %rdx
	movq	%rdx, 40(%rdi)
	movdqu	-17(%rax), %xmm2
	movzbl	-1(%rax), %ecx
	movzbl	-17(%rax), %eax
	movaps	%xmm2, -96(%rbp)
	movb	%cl, -80(%rbp)
	cmpb	$6, %al
	je	.L649
	movb	%al, -96(%rbp)
	movdqa	-96(%rbp), %xmm4
	movaps	%xmm4, -128(%rbp)
.L650:
	movss	-127(%rbp), %xmm0
	comiss	.LC9(%rip), %xmm0
	jb	.L654
	movss	.LC10(%rip), %xmm1
	comiss	%xmm0, %xmm1
	jbe	.L654
	cvttss2sil	%xmm0, %eax
	.p2align 4,,10
	.p2align 3
.L685:
	pxor	%xmm0, %xmm0
	movb	$1, -96(%rbp)
	subq	$32, %rsp
	movups	%xmm0, -95(%rbp)
	movl	%eax, -95(%rbp)
	movdqa	-96(%rbp), %xmm3
	movups	%xmm3, (%rsp)
	jmp	.L912
	.p2align 4,,10
	.p2align 3
.L640:
	movq	64(%rcx), %rsi
	addq	%r8, %rsi
	movzbl	2(%rsi), %edx
	movl	%edx, %eax
	andl	$127, %eax
	testb	%dl, %dl
	js	.L925
	sall	$25, %eax
	movl	$1, %ecx
	sarl	$25, %eax
.L718:
	movl	(%rbx), %edx
	leal	1(%rcx,%rdx), %edx
	movl	%edx, (%rbx)
	movl	%eax, %ebx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	263(%rax), %rax
	cmpb	$0, (%rax,%rbx)
	jne	.L916
	leaq	-96(%rbp), %r14
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	%r12, %rsi
	movq	%r14, %rdi
	movl	-95(%rbp), %r15d
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-95(%rbp), %ecx
	movq	%r14, %rdi
	movq	%r12, %rsi
	movl	%ecx, -136(%rbp)
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	testl	%r15d, %r15d
	movl	-95(%rbp), %edi
	movl	-136(%rbp), %ecx
	je	.L913
	movq	16(%r12), %rax
	movq	(%rax), %rdx
	movq	31(%rdx), %rax
	cmpl	%eax, %edi
	ja	.L867
	subl	%edi, %eax
	cmpl	%eax, %r15d
	movl	%eax, %esi
	cmovbe	%r15d, %esi
	setbe	%r9b
.L722:
	movq	255(%rdx), %rax
	andl	39(%rdx), %edi
	movl	%ecx, %r8d
	addq	23(%rdx), %rdi
	movq	247(%rdx), %rdx
	movl	(%rax,%rbx,4), %eax
	addq	(%rdx,%rbx,8), %r8
	cmpl	%ecx, %eax
	jb	.L723
	subl	%ecx, %eax
	movl	%esi, %edx
	cmpl	%esi, %eax
	setnb	%bl
	andl	%r9d, %ebx
	cmpl	%esi, %eax
	movq	%r8, %rsi
	cmovbe	%eax, %edx
	call	_ZN2v88internal4wasm19memory_copy_wrapperEmmj@PLT
	testb	%bl, %bl
	jne	.L913
	jmp	.L853
	.p2align 4,,10
	.p2align 3
.L639:
	movq	64(%rcx), %rax
	leaq	2(%rax,%r8), %rsi
	movzbl	(%rsi), %edx
	movl	%edx, %eax
	andl	$127, %eax
	testb	%dl, %dl
	js	.L926
	sall	$25, %eax
	movl	$1, %edx
	sarl	$25, %eax
.L729:
	addl	%edx, (%rbx)
	movq	16(%r12), %rdx
	movq	(%rdx), %rdx
	addq	263(%rdx), %rax
	cmpb	$0, (%rax)
	jne	.L916
.L782:
	movb	$1, (%rax)
	movl	$1, %eax
	jmp	.L628
	.p2align 4,,10
	.p2align 3
.L638:
	addl	$2, (%r9)
	movq	40(%rdi), %rax
	movq	24(%rdi), %rsi
	leaq	-17(%rax), %rdx
	movq	%rdx, 40(%rdi)
	movdqu	-17(%rax), %xmm6
	movzbl	-1(%rax), %ecx
	movzbl	-17(%rax), %eax
	movaps	%xmm6, -96(%rbp)
	movb	%cl, -80(%rbp)
	cmpb	$6, %al
	je	.L732
	movb	%al, -96(%rbp)
	movdqa	-96(%rbp), %xmm4
	movb	%cl, -112(%rbp)
	movaps	%xmm4, -128(%rbp)
.L733:
	leaq	-17(%rdx), %rax
	movl	-127(%rbp), %r14d
	movq	%rax, 40(%r12)
	movdqu	-17(%rdx), %xmm7
	movzbl	-1(%rdx), %ecx
	movzbl	-17(%rdx), %edx
	movaps	%xmm7, -96(%rbp)
	movb	%cl, -80(%rbp)
	cmpb	$6, %dl
	je	.L737
	movb	%dl, -96(%rbp)
	movdqa	-96(%rbp), %xmm5
	movb	%cl, -112(%rbp)
	movaps	%xmm5, -128(%rbp)
.L738:
	leaq	-17(%rax), %rdx
	movl	-127(%rbp), %ebx
	movq	%rdx, 40(%r12)
	movdqu	-17(%rax), %xmm2
	movzbl	-1(%rax), %ecx
	movzbl	-17(%rax), %eax
	movaps	%xmm2, -96(%rbp)
	movb	%cl, -80(%rbp)
	cmpb	$6, %al
	je	.L742
	movb	%al, -96(%rbp)
	movdqa	-96(%rbp), %xmm6
	movb	%cl, -112(%rbp)
	movaps	%xmm6, -128(%rbp)
.L743:
	movl	-127(%rbp), %edi
	testl	%r14d, %r14d
	je	.L913
	movq	16(%r12), %rax
	movq	(%rax), %rdx
	movq	31(%rdx), %rax
	cmpl	%eax, %edi
	ja	.L868
	movl	%eax, %ecx
	subl	%edi, %ecx
	cmpl	%ecx, %r14d
	movl	%ecx, %esi
	cmovbe	%r14d, %esi
	setbe	%cl
.L748:
	cmpl	%ebx, %edi
	jbe	.L749
	testb	%cl, %cl
	je	.L853
.L749:
	cmpl	%eax, %ebx
	ja	.L750
	subl	%ebx, %eax
	movq	23(%rdx), %r8
	cmpl	%esi, %eax
	setnb	%r14b
	andl	%ecx, %r14d
	movq	39(%rdx), %rcx
	cmpl	%esi, %eax
	movl	%esi, %edx
	cmovbe	%eax, %edx
	andl	%ecx, %ebx
	andl	%ecx, %edi
	addq	%r8, %rdi
	leaq	(%rbx,%r8), %rsi
	call	_ZN2v88internal4wasm19memory_copy_wrapperEmmj@PLT
	testb	%r14b, %r14b
	jne	.L913
	jmp	.L853
	.p2align 4,,10
	.p2align 3
.L637:
	addl	$1, (%r9)
	movq	40(%rdi), %rax
	movq	24(%rdi), %rsi
	leaq	-17(%rax), %rdx
	movq	%rdx, 40(%rdi)
	movdqu	-17(%rax), %xmm3
	movzbl	-1(%rax), %ecx
	movzbl	-17(%rax), %eax
	movaps	%xmm3, -96(%rbp)
	movb	%cl, -80(%rbp)
	cmpb	$6, %al
	je	.L752
	movb	%al, -96(%rbp)
	movdqa	-96(%rbp), %xmm7
	movb	%cl, -112(%rbp)
	movaps	%xmm7, -128(%rbp)
.L753:
	leaq	-17(%rdx), %rax
	movl	-127(%rbp), %ebx
	movq	%rax, 40(%r12)
	movdqu	-17(%rdx), %xmm4
	movzbl	-1(%rdx), %ecx
	movzbl	-17(%rdx), %edx
	movaps	%xmm4, -96(%rbp)
	movb	%cl, -80(%rbp)
	cmpb	$6, %dl
	je	.L757
	movb	%dl, -96(%rbp)
	movdqa	-96(%rbp), %xmm2
	movb	%cl, -112(%rbp)
	movaps	%xmm2, -128(%rbp)
.L758:
	leaq	-17(%rax), %rdx
	movl	-127(%rbp), %r14d
	movq	%rdx, 40(%r12)
	movdqu	-17(%rax), %xmm5
	movzbl	-1(%rax), %ecx
	movzbl	-17(%rax), %eax
	movaps	%xmm5, -96(%rbp)
	movb	%cl, -80(%rbp)
	cmpb	$6, %al
	je	.L762
	movb	%al, -96(%rbp)
	movdqa	-96(%rbp), %xmm3
	movb	%cl, -112(%rbp)
	movaps	%xmm3, -128(%rbp)
.L763:
	movl	-127(%rbp), %edi
	testl	%ebx, %ebx
	je	.L913
	movq	16(%r12), %rax
	movq	(%rax), %rdx
	movq	31(%rdx), %rax
	cmpl	%eax, %edi
	ja	.L767
	movl	%eax, %r15d
	movl	%r14d, %esi
	subl	%edi, %r15d
	cmpl	%r15d, %ebx
	movl	%r15d, %r8d
	cmovbe	%ebx, %r8d
	andl	39(%rdx), %edi
	addq	23(%rdx), %rdi
	movl	%r8d, %edx
	call	_ZN2v88internal4wasm19memory_fill_wrapperEmjj@PLT
	cmpl	%r15d, %ebx
	jbe	.L913
	.p2align 4,,10
	.p2align 3
.L853:
	movl	$4, 88(%r12)
	movq	72(%r12), %rax
	movl	$1, 104(%r12)
	movq	%r13, -16(%rax)
	xorl	%eax, %eax
	jmp	.L628
	.p2align 4,,10
	.p2align 3
.L636:
	movq	64(%rcx), %rdx
	addq	%r8, %rdx
	movzbl	2(%rdx), %eax
	movl	%eax, %ecx
	andl	$127, %ecx
	testb	%al, %al
	js	.L927
	sall	$25, %ecx
	movl	$2, %eax
	movl	$1, %edi
	sarl	$25, %ecx
.L772:
	leaq	(%rdx,%rax), %r9
	movl	$1, %esi
	movzbl	1(%r9), %eax
	movl	%eax, %edx
	andl	$127, %edx
	testb	%al, %al
	js	.L928
.L773:
	leal	(%rsi,%rdi), %eax
	addl	%eax, (%rbx)
	movq	16(%r12), %rax
	movq	(%rax), %rsi
	movl	%ecx, %eax
	movq	271(%rsi), %rsi
	cmpb	$0, (%rsi,%rax)
	jne	.L915
	leaq	-96(%rbp), %r14
	movq	%r12, %rsi
	movl	%ecx, -152(%rbp)
	movq	%r14, %rdi
	movl	%edx, -144(%rbp)
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	%r14, %rdi
	movq	%r12, %rsi
	movl	-95(%rbp), %ebx
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-95(%rbp), %r9d
	movq	%r14, %rdi
	movq	%r12, %rsi
	movl	%r9d, -136(%rbp)
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	8(%r12), %r14
	subq	$8, %rsp
	movl	-136(%rbp), %r9d
	movl	-95(%rbp), %r8d
	movl	-152(%rbp), %ecx
	addl	$1, 41104(%r14)
	movq	41088(%r14), %rax
	movq	16(%r12), %rsi
	movq	41096(%r14), %r15
	movq	%rax, -160(%rbp)
	movl	-144(%rbp), %edx
	movq	(%rsi), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	pushq	%rbx
	subq	$37592, %rdi
	call	_ZN2v88internal18WasmInstanceObject16InitTableEntriesEPNS0_7IsolateENS0_6HandleIS1_EEjjjjj@PLT
	popq	%rsi
	popq	%rdi
	testb	%al, %al
	jne	.L776
	movl	$4, 88(%r12)
	movq	72(%r12), %rdx
	movl	$11, 104(%r12)
	movq	%r13, -16(%rdx)
.L776:
	movq	-160(%rbp), %rbx
	subl	$1, 41104(%r14)
	movq	%rbx, 41088(%r14)
	cmpq	41096(%r14), %r15
	je	.L628
	movq	%r15, 41096(%r14)
	movq	%r14, %rdi
	movb	%al, -136(%rbp)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movzbl	-136(%rbp), %eax
	jmp	.L628
	.p2align 4,,10
	.p2align 3
.L635:
	movq	64(%rcx), %rax
	leaq	2(%rax,%r8), %rsi
	movzbl	(%rsi), %edx
	movl	%edx, %eax
	andl	$127, %eax
	testb	%dl, %dl
	js	.L929
	sall	$25, %eax
	movl	$1, %edx
	sarl	$25, %eax
.L781:
	addl	%edx, (%rbx)
	movq	16(%r12), %rdx
	movq	(%rdx), %rdx
	addq	271(%rdx), %rax
	cmpb	$0, (%rax)
	je	.L782
.L915:
	movl	$4, 88(%r12)
	movq	72(%r12), %rax
	movl	$10, 104(%r12)
	movq	%r13, -16(%rax)
	xorl	%eax, %eax
	jmp	.L628
	.p2align 4,,10
	.p2align 3
.L634:
	movq	64(%rcx), %rax
	movl	$2, %esi
	movl	$1, %r15d
	addq	%r8, %rax
	movzbl	2(%rax), %ecx
	movl	%ecx, %edx
	andl	$127, %edx
	testb	%cl, %cl
	js	.L930
.L784:
	addq	%rsi, %rax
	movl	$1, %r14d
	movzbl	1(%rax), %esi
	movl	%esi, %ecx
	andl	$127, %ecx
	testb	%sil, %sil
	js	.L931
.L785:
	movq	40(%r12), %rax
	movq	24(%r12), %rdi
	movq	8(%r12), %r10
	leaq	-17(%rax), %rsi
	movq	%rsi, 40(%r12)
	movdqu	-17(%rax), %xmm6
	movzbl	-1(%rax), %r8d
	movzbl	-17(%rax), %eax
	movaps	%xmm6, -96(%rbp)
	movb	%r8b, -80(%rbp)
	cmpb	$6, %al
	je	.L786
	movb	%al, -96(%rbp)
	movdqa	-96(%rbp), %xmm1
	movb	%r8b, -112(%rbp)
	movaps	%xmm1, -128(%rbp)
.L787:
	leaq	-17(%rsi), %rax
	movl	-127(%rbp), %r8d
	movq	%rax, 40(%r12)
	movdqu	-17(%rsi), %xmm7
	movzbl	-1(%rsi), %r9d
	movzbl	-17(%rsi), %esi
	movaps	%xmm7, -96(%rbp)
	movb	%r9b, -80(%rbp)
	cmpb	$6, %sil
	je	.L791
	movb	%sil, -96(%rbp)
	movdqa	-96(%rbp), %xmm4
	movb	%r9b, -112(%rbp)
	movaps	%xmm4, -128(%rbp)
.L792:
	leaq	-17(%rax), %rsi
	movl	-127(%rbp), %r9d
	movq	%rsi, 40(%r12)
	movdqu	-17(%rax), %xmm2
	movzbl	-1(%rax), %r11d
	movzbl	-17(%rax), %eax
	movaps	%xmm2, -96(%rbp)
	movb	%r11b, -80(%rbp)
	cmpb	$6, %al
	je	.L796
	movb	%al, -96(%rbp)
	movdqa	-96(%rbp), %xmm5
	movb	%r11b, -112(%rbp)
	movaps	%xmm5, -128(%rbp)
.L797:
	addl	$1, 41104(%r10)
	subq	$8, %rsp
	movq	41096(%r10), %r11
	movq	41088(%r10), %rax
	movq	16(%r12), %rsi
	movq	%r10, -136(%rbp)
	movq	8(%r12), %rdi
	pushq	%r8
	movl	-127(%rbp), %r8d
	movq	%r11, -144(%rbp)
	movq	%rax, -152(%rbp)
	call	_ZN2v88internal18WasmInstanceObject16CopyTableEntriesEPNS0_7IsolateENS0_6HandleIS1_EEjjjjj@PLT
	popq	%rdx
	popq	%rcx
	testb	%al, %al
	movq	-136(%rbp), %r10
	movq	-144(%rbp), %r11
	jne	.L801
	movl	$4, 88(%r12)
	movq	72(%r12), %rdx
	movl	$11, 104(%r12)
	movq	%r13, -16(%rdx)
.L801:
	leal	(%r15,%r14), %edx
	addl	%edx, (%rbx)
	movq	-152(%rbp), %rbx
	subl	$1, 41104(%r10)
	movq	%rbx, 41088(%r10)
	cmpq	41096(%r10), %r11
	je	.L628
	movq	%r11, 41096(%r10)
	movq	%r10, %rdi
	movb	%al, -136(%rbp)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movzbl	-136(%rbp), %eax
	jmp	.L628
	.p2align 4,,10
	.p2align 3
.L633:
	movq	64(%rcx), %rax
	movl	$1, %r14d
	leaq	1(%rax,%r8), %rdx
	movzbl	1(%rdx), %eax
	movl	%eax, %esi
	andl	$127, %esi
	testb	%al, %al
	js	.L932
.L802:
	movq	8(%r12), %r15
	movq	41088(%r15), %rax
	addl	$1, 41104(%r15)
	movq	8(%r12), %rdx
	movq	41096(%r15), %r13
	movq	%rax, -136(%rbp)
	movq	16(%r12), %rax
	movq	(%rax), %rcx
	leal	16(,%rsi,8), %eax
	cltq
	movq	199(%rcx), %rcx
	movq	-1(%rax,%rcx), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L803
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r9
.L804:
	movq	40(%r12), %rdx
	movq	24(%r12), %rsi
	movq	8(%r12), %rdi
	leaq	-17(%rdx), %rax
	movq	%rax, 40(%r12)
	movdqu	-17(%rdx), %xmm3
	movzbl	-1(%rdx), %ecx
	movzbl	-17(%rdx), %edx
	movaps	%xmm3, -96(%rbp)
	movb	%cl, -80(%rbp)
	cmpb	$6, %dl
	je	.L806
	movb	%dl, -96(%rbp)
	movdqa	-96(%rbp), %xmm6
	movb	%cl, -112(%rbp)
	movaps	%xmm6, -128(%rbp)
.L807:
	leaq	-17(%rax), %r10
	movl	-127(%rbp), %edx
	movq	%r10, 40(%r12)
	movdqu	-17(%rax), %xmm4
	movzbl	-1(%rax), %ecx
	cmpb	$6, -17(%rax)
	movaps	%xmm4, -96(%rbp)
	movb	%cl, -80(%rbp)
	je	.L811
	movq	-95(%rbp), %rcx
.L812:
	movq	%r9, %rsi
	call	_ZN2v88internal15WasmTableObject4GrowEPNS0_7IsolateENS0_6HandleIS1_EEjNS4_INS0_6ObjectEEE@PLT
	pxor	%xmm0, %xmm0
	movb	$1, -96(%rbp)
	subq	$32, %rsp
	movups	%xmm0, -95(%rbp)
	movl	%eax, -95(%rbp)
	movdqa	-96(%rbp), %xmm5
	movups	%xmm5, (%rsp)
.L920:
	movzbl	-80(%rbp), %eax
	movq	%r12, %rdi
	movb	%al, 16(%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movq	-136(%rbp), %rax
	addl	%r14d, (%rbx)
	addq	$32, %rsp
	subl	$1, 41104(%r15)
	movq	%rax, 41088(%r15)
	cmpq	41096(%r15), %r13
	je	.L913
	movq	%r13, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	jmp	.L913
	.p2align 4,,10
	.p2align 3
.L644:
	movq	40(%rdi), %rax
	leaq	-17(%rax), %rdx
	movq	%rdx, 40(%rdi)
	movdqu	-17(%rax), %xmm4
	movzbl	-1(%rax), %ecx
	movzbl	-17(%rax), %eax
	movaps	%xmm4, -96(%rbp)
	movb	%cl, -80(%rbp)
	cmpb	$6, %al
	je	.L686
	movb	%al, -96(%rbp)
	movdqa	-96(%rbp), %xmm6
	movaps	%xmm6, -128(%rbp)
.L687:
	movl	-127(%rbp), %ebx
	leaq	-64(%rbp), %rdi
	movl	%ebx, -64(%rbp)
	call	_ZN2v88internal4wasm24float32_to_int64_wrapperEm@PLT
	testl	%eax, %eax
	jne	.L917
	movd	%ebx, %xmm0
	ucomiss	%xmm0, %xmm0
	jp	.L866
	movabsq	$-9223372036854775808, %rax
	pxor	%xmm1, %xmm1
	movabsq	$9223372036854775807, %rdx
	comiss	%xmm0, %xmm1
	cmovbe	%rdx, %rax
	jmp	.L713
	.p2align 4,,10
	.p2align 3
.L646:
	movq	40(%rdi), %rax
	leaq	-17(%rax), %rdx
	movq	%rdx, 40(%rdi)
	movdqu	-17(%rax), %xmm6
	movzbl	-1(%rax), %ecx
	cmpb	$6, -17(%rax)
	movaps	%xmm6, -96(%rbp)
	movb	%cl, -80(%rbp)
	je	.L668
	movq	-95(%rbp), %rax
.L669:
	movq	%rax, %xmm0
	comisd	.LC14(%rip), %xmm0
	jbe	.L673
	movsd	.LC15(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jbe	.L673
	cvttsd2sil	%xmm0, %eax
.L676:
	pxor	%xmm0, %xmm0
	movb	$1, -96(%rbp)
	subq	$32, %rsp
	movups	%xmm0, -95(%rbp)
	movl	%eax, -95(%rbp)
	movdqa	-96(%rbp), %xmm7
	movups	%xmm7, (%rsp)
	jmp	.L912
	.p2align 4,,10
	.p2align 3
.L645:
	movq	40(%rdi), %rax
	leaq	-17(%rax), %rdx
	movq	%rdx, 40(%rdi)
	movdqu	-17(%rax), %xmm2
	movzbl	-1(%rax), %ecx
	cmpb	$6, -17(%rax)
	movaps	%xmm2, -96(%rbp)
	movb	%cl, -80(%rbp)
	je	.L677
	movq	-95(%rbp), %rdi
.L678:
	movq	%rdi, %xmm0
	comisd	.LC17(%rip), %xmm0
	jbe	.L682
	movsd	.LC18(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jbe	.L682
	cvttsd2siq	%xmm0, %rax
	jmp	.L685
	.p2align 4,,10
	.p2align 3
.L647:
	movq	40(%rdi), %rax
	leaq	-17(%rax), %rdx
	movq	%rdx, 40(%rdi)
	movdqu	-17(%rax), %xmm4
	movzbl	-1(%rax), %ecx
	movzbl	-17(%rax), %eax
	movaps	%xmm4, -96(%rbp)
	movb	%cl, -80(%rbp)
	cmpb	$6, %al
	je	.L659
	movb	%al, -96(%rbp)
	movdqa	-96(%rbp), %xmm5
	movaps	%xmm5, -128(%rbp)
.L660:
	movss	-127(%rbp), %xmm0
	comiss	.LC12(%rip), %xmm0
	jbe	.L664
	movss	.LC13(%rip), %xmm1
	comiss	%xmm0, %xmm1
	jbe	.L664
	cvttss2siq	%xmm0, %rax
.L667:
	pxor	%xmm0, %xmm0
	movb	$1, -96(%rbp)
	movups	%xmm0, -95(%rbp)
	movl	%eax, -95(%rbp)
	jmp	.L914
	.p2align 4,,10
	.p2align 3
.L642:
	movq	40(%rdi), %rax
	leaq	-17(%rax), %rdx
	movq	%rdx, 40(%rdi)
	movdqu	-17(%rax), %xmm2
	movzbl	-1(%rax), %ecx
	cmpb	$6, -17(%rax)
	movaps	%xmm2, -96(%rbp)
	movb	%cl, -80(%rbp)
	je	.L700
	movq	-95(%rbp), %rbx
.L701:
	leaq	-64(%rbp), %rdi
	movq	%rbx, -64(%rbp)
	call	_ZN2v88internal4wasm24float64_to_int64_wrapperEm@PLT
	testl	%eax, %eax
	je	.L705
	movq	-64(%rbp), %rax
.L706:
	movb	$2, -96(%rbp)
	subq	$32, %rsp
	movq	$0, -87(%rbp)
	movq	%rax, -95(%rbp)
	movdqa	-96(%rbp), %xmm3
	movups	%xmm3, (%rsp)
	jmp	.L912
	.p2align 4,,10
	.p2align 3
.L641:
	movq	40(%rdi), %rax
	leaq	-17(%rax), %rdx
	movq	%rdx, 40(%rdi)
	movdqu	-17(%rax), %xmm4
	movzbl	-1(%rax), %ecx
	cmpb	$6, -17(%rax)
	movaps	%xmm4, -96(%rbp)
	movb	%cl, -80(%rbp)
	je	.L707
	movq	-95(%rbp), %rbx
.L708:
	leaq	-64(%rbp), %rdi
	movq	%rbx, -64(%rbp)
	call	_ZN2v88internal4wasm25float64_to_uint64_wrapperEm@PLT
	testl	%eax, %eax
	jne	.L917
	movq	%rbx, %xmm0
	ucomisd	%xmm0, %xmm0
	jp	.L866
	pxor	%xmm1, %xmm1
	xorl	%eax, %eax
	comisd	%xmm0, %xmm1
	setbe	%al
	negq	%rax
.L713:
	movb	$2, -96(%rbp)
	movq	$0, -87(%rbp)
	movq	%rax, -95(%rbp)
.L914:
	movdqa	-96(%rbp), %xmm5
	subq	$32, %rsp
	movups	%xmm5, (%rsp)
	jmp	.L912
	.p2align 4,,10
	.p2align 3
.L643:
	movq	40(%rdi), %rax
	leaq	-17(%rax), %rdx
	movq	%rdx, 40(%rdi)
	movdqu	-17(%rax), %xmm6
	movzbl	-1(%rax), %ecx
	movzbl	-17(%rax), %eax
	movaps	%xmm6, -96(%rbp)
	movb	%cl, -80(%rbp)
	cmpb	$6, %al
	je	.L693
	movb	%al, -96(%rbp)
	movdqa	-96(%rbp), %xmm7
	movaps	%xmm7, -128(%rbp)
.L694:
	movl	-127(%rbp), %ebx
	leaq	-64(%rbp), %rdi
	movl	%ebx, -64(%rbp)
	call	_ZN2v88internal4wasm25float32_to_uint64_wrapperEm@PLT
	testl	%eax, %eax
	je	.L698
	movq	-64(%rbp), %rax
.L699:
	movb	$2, -96(%rbp)
	subq	$32, %rsp
	movq	$0, -87(%rbp)
	movq	%rax, -95(%rbp)
	movdqa	-96(%rbp), %xmm7
	movups	%xmm7, (%rsp)
	.p2align 4,,10
	.p2align 3
.L912:
	movzbl	-80(%rbp), %eax
	movq	%r12, %rdi
	movb	%al, 16(%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
.L913:
	movl	$1, %eax
	jmp	.L628
	.p2align 4,,10
	.p2align 3
.L803:
	movq	41088(%rdx), %r9
	cmpq	41096(%rdx), %r9
	je	.L933
.L805:
	leaq	8(%r9), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%r9)
	jmp	.L804
	.p2align 4,,10
	.p2align 3
.L818:
	movq	41088(%rdx), %rax
	cmpq	%rax, 41096(%rdx)
	je	.L934
.L820:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%rdx)
	movq	%rsi, (%rax)
	jmp	.L819
	.p2align 4,,10
	.p2align 3
.L926:
	movzbl	1(%rsi), %ecx
	movzbl	%al, %eax
	movl	%ecx, %edx
	sall	$7, %edx
	andl	$16256, %edx
	orl	%edx, %eax
	testb	%cl, %cl
	js	.L935
	sall	$18, %eax
	movl	$2, %edx
	sarl	$18, %eax
	jmp	.L729
	.p2align 4,,10
	.p2align 3
.L925:
	movzbl	3(%rsi), %ecx
	movzbl	%al, %eax
	movl	%ecx, %edx
	sall	$7, %edx
	andl	$16256, %edx
	orl	%edx, %eax
	testb	%cl, %cl
	js	.L936
	sall	$18, %eax
	movl	$2, %ecx
	sarl	$18, %eax
	jmp	.L718
	.p2align 4,,10
	.p2align 3
.L927:
	movzbl	3(%rdx), %esi
	movzbl	%cl, %ecx
	movl	%esi, %eax
	sall	$7, %eax
	andl	$16256, %eax
	orl	%eax, %ecx
	testb	%sil, %sil
	js	.L937
	sall	$18, %ecx
	movl	$3, %eax
	movl	$2, %edi
	sarl	$18, %ecx
	jmp	.L772
	.p2align 4,,10
	.p2align 3
.L931:
	movzbl	2(%rax), %r8d
	movl	$2, %r14d
	movl	%r8d, %edi
	sall	$7, %edi
	andl	$16256, %edi
	orl	%edi, %ecx
	testb	%r8b, %r8b
	jns	.L785
	movzbl	3(%rax), %esi
	movl	$3, %r14d
	movl	%esi, %edi
	sall	$14, %edi
	andl	$2080768, %edi
	orl	%edi, %ecx
	testb	%sil, %sil
	jns	.L785
	movzbl	4(%rax), %edi
	movl	$4, %r14d
	movl	%edi, %esi
	sall	$21, %esi
	andl	$266338304, %esi
	orl	%esi, %ecx
	testb	%dil, %dil
	jns	.L785
	movzbl	5(%rax), %eax
	movl	$5, %r14d
	sall	$28, %eax
	orl	%eax, %ecx
	jmp	.L785
	.p2align 4,,10
	.p2align 3
.L929:
	movzbl	1(%rsi), %ecx
	movzbl	%al, %eax
	movl	%ecx, %edx
	sall	$7, %edx
	andl	$16256, %edx
	orl	%edx, %eax
	testb	%cl, %cl
	js	.L938
	sall	$18, %eax
	movl	$2, %edx
	sarl	$18, %eax
	jmp	.L781
	.p2align 4,,10
	.p2align 3
.L932:
	movzbl	2(%rdx), %ecx
	movl	$2, %r14d
	movl	%ecx, %eax
	sall	$7, %eax
	andl	$16256, %eax
	orl	%eax, %esi
	testb	%cl, %cl
	jns	.L802
	movzbl	3(%rdx), %ecx
	movl	$3, %r14d
	movl	%ecx, %eax
	sall	$14, %eax
	andl	$2080768, %eax
	orl	%eax, %esi
	testb	%cl, %cl
	jns	.L802
	movzbl	4(%rdx), %ecx
	movl	$4, %r14d
	movl	%ecx, %eax
	sall	$21, %eax
	andl	$266338304, %eax
	orl	%eax, %esi
	testb	%cl, %cl
	jns	.L802
	movzbl	5(%rdx), %eax
	movl	$5, %r14d
	sall	$28, %eax
	orl	%eax, %esi
	jmp	.L802
	.p2align 4,,10
	.p2align 3
.L922:
	movl	$2, -136(%rbp)
	movzbl	2(%rdx), %ecx
	movl	%ecx, %eax
	sall	$7, %eax
	andl	$16256, %eax
	orl	%eax, %r10d
	testb	%cl, %cl
	jns	.L822
	movl	$3, -136(%rbp)
	movzbl	3(%rdx), %ecx
	movl	%ecx, %eax
	sall	$14, %eax
	andl	$2080768, %eax
	orl	%eax, %r10d
	testb	%cl, %cl
	jns	.L822
	movl	$4, -136(%rbp)
	movzbl	4(%rdx), %ecx
	movl	%ecx, %eax
	sall	$21, %eax
	andl	$266338304, %eax
	orl	%eax, %r10d
	testb	%cl, %cl
	jns	.L822
	movl	$5, -136(%rbp)
	movzbl	5(%rdx), %eax
	sall	$28, %eax
	orl	%eax, %r10d
	jmp	.L822
	.p2align 4,,10
	.p2align 3
.L921:
	movzbl	2(%rdx), %ecx
	movl	$2, %r14d
	movl	%ecx, %eax
	sall	$7, %eax
	andl	$16256, %eax
	orl	%eax, %esi
	testb	%cl, %cl
	jns	.L817
	movzbl	3(%rdx), %ecx
	movl	$3, %r14d
	movl	%ecx, %eax
	sall	$14, %eax
	andl	$2080768, %eax
	orl	%eax, %esi
	testb	%cl, %cl
	jns	.L817
	movzbl	4(%rdx), %ecx
	movl	$4, %r14d
	movl	%ecx, %eax
	sall	$21, %eax
	andl	$266338304, %eax
	orl	%eax, %esi
	testb	%cl, %cl
	jns	.L817
	movzbl	5(%rdx), %eax
	movl	$5, %r14d
	sall	$28, %eax
	orl	%eax, %esi
	jmp	.L817
	.p2align 4,,10
	.p2align 3
.L930:
	movzbl	3(%rax), %edi
	movl	$3, %esi
	movl	$2, %r15d
	movl	%edi, %ecx
	sall	$7, %ecx
	andl	$16256, %ecx
	orl	%ecx, %edx
	testb	%dil, %dil
	jns	.L784
	movzbl	4(%rax), %edi
	movl	$4, %esi
	movl	$3, %r15d
	movl	%edi, %ecx
	sall	$14, %ecx
	andl	$2080768, %ecx
	orl	%ecx, %edx
	testb	%dil, %dil
	jns	.L784
	movzbl	5(%rax), %edi
	movl	$5, %esi
	movl	$4, %r15d
	movl	%edi, %ecx
	sall	$21, %ecx
	andl	$266338304, %ecx
	orl	%ecx, %edx
	testb	%dil, %dil
	jns	.L784
	movzbl	6(%rax), %ecx
	movl	$6, %esi
	movl	$5, %r15d
	sall	$28, %ecx
	orl	%ecx, %edx
	jmp	.L784
	.p2align 4,,10
	.p2align 3
.L786:
	movq	48(%r12), %rax
	subq	%rdi, %rsi
	movq	(%rax), %r8
	imull	$-252645135, %esi, %eax
	movq	7(%r8), %rsi
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%rax,%rsi), %rsi
	movq	41112(%r10), %rdi
	testq	%rdi, %rdi
	je	.L788
	movl	%ecx, -144(%rbp)
	movl	%edx, -136(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-136(%rbp), %edx
	movl	-144(%rbp), %ecx
.L789:
	movq	48(%r12), %rsi
	movq	8(%r12), %rdi
	movq	%rax, -127(%rbp)
	movq	40(%r12), %rax
	subq	24(%r12), %rax
	imull	$-252645135, %eax, %eax
	movq	(%rsi), %rsi
	movq	96(%rdi), %rdi
	movq	7(%rsi), %rsi
	leal	16(,%rax,8), %eax
	cltq
	movq	%rdi, -1(%rax,%rsi)
	movq	40(%r12), %rsi
	movq	24(%r12), %rdi
	movq	8(%r12), %r10
	jmp	.L787
	.p2align 4,,10
	.p2align 3
.L707:
	movq	48(%rdi), %rax
	subq	24(%rdi), %rdx
	movq	8(%rdi), %r13
	movq	(%rax), %rcx
	imull	$-252645135, %edx, %eax
	movq	7(%rcx), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%rax,%rdx), %rsi
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L709
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rbx
.L710:
	movq	40(%r12), %rax
	movq	48(%r12), %rdx
	subq	24(%r12), %rax
	movq	8(%r12), %rcx
	imull	$-252645135, %eax, %eax
	movq	(%rdx), %rdx
	movq	96(%rcx), %rcx
	movq	7(%rdx), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	%rcx, -1(%rax,%rdx)
	jmp	.L708
	.p2align 4,,10
	.p2align 3
.L823:
	movq	48(%r12), %rax
	subq	%r11, %rdx
	movq	8(%r12), %rcx
	movq	(%rax), %rsi
	imull	$-252645135, %edx, %eax
	movq	7(%rsi), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%rax,%rdx), %rsi
	movq	41112(%rcx), %rdi
	testq	%rdi, %rdi
	je	.L825
	movl	%r10d, -152(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-152(%rbp), %r10d
.L826:
	movq	48(%r12), %rdx
	movq	8(%r12), %rcx
	movq	%rax, -127(%rbp)
	movq	40(%r12), %rax
	subq	24(%r12), %rax
	imull	$-252645135, %eax, %eax
	movq	(%rdx), %rdx
	movq	96(%rcx), %rcx
	movq	7(%rdx), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	%rcx, -1(%rax,%rdx)
	movq	40(%r12), %rdx
	movq	24(%r12), %r11
	jmp	.L824
	.p2align 4,,10
	.p2align 3
.L693:
	movq	48(%rdi), %rax
	subq	24(%rdi), %rdx
	movq	8(%rdi), %rbx
	movq	(%rax), %rcx
	imull	$-252645135, %edx, %eax
	movq	7(%rcx), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%rax,%rdx), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L695
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L696:
	movq	48(%r12), %rdx
	movq	8(%r12), %rcx
	movq	%rax, -127(%rbp)
	movq	40(%r12), %rax
	subq	24(%r12), %rax
	imull	$-252645135, %eax, %eax
	movq	(%rdx), %rdx
	movq	96(%rcx), %rcx
	movq	7(%rdx), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	%rcx, -1(%rax,%rdx)
	jmp	.L694
	.p2align 4,,10
	.p2align 3
.L700:
	movq	48(%rdi), %rax
	subq	24(%rdi), %rdx
	movq	8(%rdi), %r13
	movq	(%rax), %rcx
	imull	$-252645135, %edx, %eax
	movq	7(%rcx), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%rax,%rdx), %rsi
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L702
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rbx
.L703:
	movq	40(%r12), %rax
	movq	48(%r12), %rdx
	subq	24(%r12), %rax
	movq	8(%r12), %rcx
	imull	$-252645135, %eax, %eax
	movq	(%rdx), %rdx
	movq	96(%rcx), %rcx
	movq	7(%rdx), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	%rcx, -1(%rax,%rdx)
	jmp	.L701
	.p2align 4,,10
	.p2align 3
.L649:
	movq	48(%rdi), %rax
	subq	24(%rdi), %rdx
	movq	8(%rdi), %rbx
	movq	(%rax), %rcx
	imull	$-252645135, %edx, %eax
	movq	7(%rcx), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%rax,%rdx), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L651
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L652:
	movq	48(%r12), %rdx
	movq	8(%r12), %rcx
	movq	%rax, -127(%rbp)
	movq	40(%r12), %rax
	subq	24(%r12), %rax
	imull	$-252645135, %eax, %eax
	movq	(%rdx), %rdx
	movq	96(%rcx), %rcx
	movq	7(%rdx), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	%rcx, -1(%rax,%rdx)
	jmp	.L650
	.p2align 4,,10
	.p2align 3
.L686:
	movq	48(%rdi), %rax
	subq	24(%rdi), %rdx
	movq	8(%rdi), %rbx
	movq	(%rax), %rcx
	imull	$-252645135, %edx, %eax
	movq	7(%rcx), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%rax,%rdx), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L688
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L689:
	movq	48(%r12), %rdx
	movq	8(%r12), %rcx
	movq	%rax, -127(%rbp)
	movq	40(%r12), %rax
	subq	24(%r12), %rax
	imull	$-252645135, %eax, %eax
	movq	(%rdx), %rdx
	movq	96(%rcx), %rcx
	movq	7(%rdx), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	%rcx, -1(%rax,%rdx)
	jmp	.L687
	.p2align 4,,10
	.p2align 3
.L752:
	movq	48(%rdi), %rax
	subq	%rsi, %rdx
	movq	8(%rdi), %rbx
	movq	(%rax), %rcx
	imull	$-252645135, %edx, %eax
	movq	7(%rcx), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%rax,%rdx), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L754
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L755:
	movq	48(%r12), %rdx
	movq	8(%r12), %rcx
	movq	%rax, -127(%rbp)
	movq	40(%r12), %rax
	subq	24(%r12), %rax
	imull	$-252645135, %eax, %eax
	movq	(%rdx), %rdx
	movq	96(%rcx), %rcx
	movq	7(%rdx), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	%rcx, -1(%rax,%rdx)
	movq	40(%r12), %rdx
	movq	24(%r12), %rsi
	jmp	.L753
	.p2align 4,,10
	.p2align 3
.L659:
	movq	48(%rdi), %rax
	subq	24(%rdi), %rdx
	movq	8(%rdi), %rbx
	movq	(%rax), %rcx
	imull	$-252645135, %edx, %eax
	movq	7(%rcx), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%rax,%rdx), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L661
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L662:
	movq	48(%r12), %rdx
	movq	8(%r12), %rcx
	movq	%rax, -127(%rbp)
	movq	40(%r12), %rax
	subq	24(%r12), %rax
	imull	$-252645135, %eax, %eax
	movq	(%rdx), %rdx
	movq	96(%rcx), %rcx
	movq	7(%rdx), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	%rcx, -1(%rax,%rdx)
	jmp	.L660
	.p2align 4,,10
	.p2align 3
.L732:
	movq	48(%rdi), %rax
	subq	%rsi, %rdx
	movq	8(%rdi), %rbx
	movq	(%rax), %rcx
	imull	$-252645135, %edx, %eax
	movq	7(%rcx), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%rax,%rdx), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L734
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L735:
	movq	48(%r12), %rdx
	movq	8(%r12), %rcx
	movq	%rax, -127(%rbp)
	movq	40(%r12), %rax
	subq	24(%r12), %rax
	imull	$-252645135, %eax, %eax
	movq	(%rdx), %rdx
	movq	96(%rcx), %rcx
	movq	7(%rdx), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	%rcx, -1(%rax,%rdx)
	movq	40(%r12), %rdx
	movq	24(%r12), %rsi
	jmp	.L733
	.p2align 4,,10
	.p2align 3
.L677:
	movq	48(%rdi), %rax
	subq	24(%rdi), %rdx
	movq	8(%rdi), %rbx
	movq	(%rax), %rcx
	imull	$-252645135, %edx, %eax
	movq	7(%rcx), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%rax,%rdx), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L679
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdi
.L680:
	movq	40(%r12), %rax
	movq	48(%r12), %rdx
	subq	24(%r12), %rax
	movq	8(%r12), %rcx
	imull	$-252645135, %eax, %eax
	movq	(%rdx), %rdx
	movq	96(%rcx), %rcx
	movq	7(%rdx), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	%rcx, -1(%rax,%rdx)
	jmp	.L678
	.p2align 4,,10
	.p2align 3
.L668:
	movq	48(%rdi), %rax
	subq	24(%rdi), %rdx
	movq	8(%rdi), %rbx
	movq	(%rax), %rcx
	imull	$-252645135, %edx, %eax
	movq	7(%rcx), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%rax,%rdx), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L670
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L671:
	movq	40(%r12), %rdx
	movq	48(%r12), %rcx
	subq	24(%r12), %rdx
	movq	8(%r12), %rsi
	imull	$-252645135, %edx, %edx
	movq	(%rcx), %rcx
	movq	96(%rsi), %rsi
	movq	7(%rcx), %rcx
	leal	16(,%rdx,8), %edx
	movslq	%edx, %rdx
	movq	%rsi, -1(%rdx,%rcx)
	jmp	.L669
	.p2align 4,,10
	.p2align 3
.L917:
	movq	-64(%rbp), %rax
	jmp	.L713
	.p2align 4,,10
	.p2align 3
.L654:
	ucomiss	%xmm0, %xmm0
	jp	.L860
	pxor	%xmm1, %xmm1
	xorl	%eax, %eax
	comiss	%xmm0, %xmm1
	seta	%al
	addl	$2147483647, %eax
	jmp	.L685
	.p2align 4,,10
	.p2align 3
.L673:
	ucomisd	%xmm0, %xmm0
	jp	.L858
	pxor	%xmm1, %xmm1
	xorl	%eax, %eax
	comisd	%xmm0, %xmm1
	seta	%al
	addl	$2147483647, %eax
	jmp	.L676
	.p2align 4,,10
	.p2align 3
.L664:
	ucomiss	%xmm0, %xmm0
	jp	.L857
	pxor	%xmm1, %xmm1
	xorl	%eax, %eax
	comiss	%xmm0, %xmm1
	setbe	%al
	negl	%eax
	jmp	.L667
	.p2align 4,,10
	.p2align 3
.L682:
	ucomisd	%xmm0, %xmm0
	jp	.L860
	pxor	%xmm1, %xmm1
	xorl	%eax, %eax
	comisd	%xmm0, %xmm1
	setbe	%al
	negl	%eax
	jmp	.L685
	.p2align 4,,10
	.p2align 3
.L916:
	movl	$4, 88(%r12)
	movq	72(%r12), %rax
	movl	$9, 104(%r12)
	movq	%r13, -16(%rax)
	xorl	%eax, %eax
	jmp	.L628
	.p2align 4,,10
	.p2align 3
.L923:
	movl	%eax, %r8d
	call	_ZN2v88internal15WasmTableObject4FillEPNS0_7IsolateENS0_6HandleIS1_EEjNS4_INS0_6ObjectEEEj@PLT
.L911:
	movl	$4, 88(%r12)
	movq	72(%r12), %rax
	movl	$11, 104(%r12)
	movq	%r13, -16(%rax)
	xorl	%eax, %eax
	jmp	.L842
	.p2align 4,,10
	.p2align 3
.L698:
	movd	%ebx, %xmm0
	ucomiss	%xmm0, %xmm0
	jp	.L863
	pxor	%xmm1, %xmm1
	xorl	%eax, %eax
	comiss	%xmm0, %xmm1
	setbe	%al
	negq	%rax
	jmp	.L699
	.p2align 4,,10
	.p2align 3
.L705:
	movq	%rbx, %xmm0
	ucomisd	%xmm0, %xmm0
	jp	.L864
	movabsq	$-9223372036854775808, %rax
	pxor	%xmm1, %xmm1
	movabsq	$9223372036854775807, %rdx
	comisd	%xmm0, %xmm1
	cmovbe	%rdx, %rax
	jmp	.L706
	.p2align 4,,10
	.p2align 3
.L838:
	movq	41088(%rdi), %rsi
	cmpq	41096(%rdi), %rsi
	je	.L939
.L840:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rdi)
	movq	%r10, (%rsi)
	jmp	.L839
	.p2align 4,,10
	.p2align 3
.L928:
	movzbl	2(%r9), %r8d
	movl	%r8d, %esi
	sall	$7, %esi
	andl	$16256, %esi
	orl	%esi, %edx
	movl	$2, %esi
	testb	%r8b, %r8b
	jns	.L773
	movzbl	3(%r9), %eax
	movl	%eax, %esi
	sall	$14, %esi
	andl	$2080768, %esi
	orl	%esi, %edx
	movl	$3, %esi
	testb	%al, %al
	jns	.L773
	movzbl	4(%r9), %r8d
	movl	$4, %esi
	movl	%r8d, %eax
	sall	$21, %eax
	andl	$266338304, %eax
	orl	%eax, %edx
	testb	%r8b, %r8b
	jns	.L773
	movzbl	5(%r9), %eax
	movl	$5, %esi
	sall	$28, %eax
	orl	%eax, %edx
	jmp	.L773
	.p2align 4,,10
	.p2align 3
.L796:
	movq	48(%r12), %rax
	subq	%rdi, %rsi
	movq	(%rax), %r11
	imull	$-252645135, %esi, %eax
	movq	7(%r11), %rsi
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%rax,%rsi), %rsi
	movq	41112(%r10), %rdi
	testq	%rdi, %rdi
	je	.L798
	movl	%ecx, -160(%rbp)
	movl	%edx, -152(%rbp)
	movl	%r8d, -144(%rbp)
	movl	%r9d, -136(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-136(%rbp), %r9d
	movl	-144(%rbp), %r8d
	movl	-152(%rbp), %edx
	movl	-160(%rbp), %ecx
.L799:
	movq	48(%r12), %rsi
	movq	8(%r12), %rdi
	movq	%rax, -127(%rbp)
	movq	40(%r12), %rax
	subq	24(%r12), %rax
	imull	$-252645135, %eax, %eax
	movq	(%rsi), %rsi
	movq	96(%rdi), %rdi
	movq	7(%rsi), %rsi
	leal	16(,%rax,8), %eax
	cltq
	movq	%rdi, -1(%rax,%rsi)
	movq	8(%r12), %r10
	jmp	.L797
	.p2align 4,,10
	.p2align 3
.L791:
	movq	48(%r12), %rsi
	subq	%rdi, %rax
	imull	$-252645135, %eax, %eax
	movq	(%rsi), %rsi
	leal	16(,%rax,8), %eax
	movq	7(%rsi), %rsi
	cltq
	movq	-1(%rax,%rsi), %rsi
	movq	41112(%r10), %rdi
	testq	%rdi, %rdi
	je	.L793
	movl	%ecx, -152(%rbp)
	movl	%edx, -144(%rbp)
	movl	%r8d, -136(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-136(%rbp), %r8d
	movl	-144(%rbp), %edx
	movl	-152(%rbp), %ecx
.L794:
	movq	48(%r12), %rsi
	movq	8(%r12), %rdi
	movq	%rax, -127(%rbp)
	movq	40(%r12), %rax
	subq	24(%r12), %rax
	imull	$-252645135, %eax, %eax
	movq	(%rsi), %rsi
	movq	96(%rdi), %rdi
	movq	7(%rsi), %rsi
	leal	16(,%rax,8), %eax
	cltq
	movq	%rdi, -1(%rax,%rsi)
	movq	40(%r12), %rax
	movq	24(%r12), %rdi
	movq	8(%r12), %r10
	jmp	.L792
	.p2align 4,,10
	.p2align 3
.L762:
	movq	48(%r12), %rax
	subq	%rsi, %rdx
	movq	8(%r12), %r15
	movq	(%rax), %rcx
	imull	$-252645135, %edx, %eax
	movq	7(%rcx), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%rax,%rdx), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L764
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L765:
	movq	48(%r12), %rdx
	movq	8(%r12), %rcx
	movq	%rax, -127(%rbp)
	movq	40(%r12), %rax
	subq	24(%r12), %rax
	imull	$-252645135, %eax, %eax
	movq	(%rdx), %rdx
	movq	96(%rcx), %rcx
	movq	7(%rdx), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	%rcx, -1(%rax,%rdx)
	jmp	.L763
	.p2align 4,,10
	.p2align 3
.L742:
	movq	48(%r12), %rax
	subq	%rsi, %rdx
	movq	8(%r12), %r15
	movq	(%rax), %rcx
	imull	$-252645135, %edx, %eax
	movq	7(%rcx), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%rax,%rdx), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L744
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L745:
	movq	48(%r12), %rdx
	movq	8(%r12), %rcx
	movq	%rax, -127(%rbp)
	movq	40(%r12), %rax
	subq	24(%r12), %rax
	imull	$-252645135, %eax, %eax
	movq	(%rdx), %rdx
	movq	96(%rcx), %rcx
	movq	7(%rdx), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	%rcx, -1(%rax,%rdx)
	jmp	.L743
	.p2align 4,,10
	.p2align 3
.L757:
	movq	48(%r12), %rdx
	subq	%rsi, %rax
	movq	8(%r12), %r14
	imull	$-252645135, %eax, %eax
	movq	(%rdx), %rdx
	leal	16(,%rax,8), %eax
	movq	7(%rdx), %rdx
	cltq
	movq	-1(%rax,%rdx), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L759
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L760:
	movq	48(%r12), %rdx
	movq	8(%r12), %rcx
	movq	%rax, -127(%rbp)
	movq	40(%r12), %rax
	subq	24(%r12), %rax
	imull	$-252645135, %eax, %eax
	movq	(%rdx), %rdx
	movq	96(%rcx), %rcx
	movq	7(%rdx), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	%rcx, -1(%rax,%rdx)
	movq	40(%r12), %rax
	movq	24(%r12), %rsi
	jmp	.L758
	.p2align 4,,10
	.p2align 3
.L811:
	movq	48(%r12), %rax
	movq	(%rax), %rcx
	movq	%r10, %rax
	subq	%rsi, %rax
	imull	$-252645135, %eax, %eax
	movq	7(%rcx), %rcx
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%rax,%rcx), %rsi
	movq	41112(%rdi), %r10
	testq	%r10, %r10
	je	.L813
	movq	%r10, %rdi
	movl	%edx, -152(%rbp)
	movq	%r9, -144(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-144(%rbp), %r9
	movl	-152(%rbp), %edx
	movq	%rax, %rcx
.L814:
	movq	40(%r12), %rax
	movq	48(%r12), %rsi
	subq	24(%r12), %rax
	movq	8(%r12), %rdi
	imull	$-252645135, %eax, %eax
	movq	(%rsi), %rsi
	movq	96(%rdi), %rdi
	movq	7(%rsi), %rsi
	leal	16(,%rax,8), %eax
	cltq
	movq	%rdi, -1(%rax,%rsi)
	movq	8(%r12), %rdi
	jmp	.L812
	.p2align 4,,10
	.p2align 3
.L828:
	movq	48(%r12), %rdx
	subq	%r11, %rax
	imull	$-252645135, %eax, %eax
	movq	(%rdx), %rdx
	leal	16(,%rax,8), %eax
	movq	7(%rdx), %rdx
	cltq
	movq	-1(%rax,%rdx), %rsi
	movq	41112(%rdi), %r11
	testq	%r11, %r11
	je	.L830
	movq	%r11, %rdi
	movl	%r10d, -160(%rbp)
	movl	%r8d, -152(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-152(%rbp), %r8d
	movl	-160(%rbp), %r10d
	movq	%rax, %rcx
.L831:
	movq	40(%r12), %rax
	movq	48(%r12), %rdx
	subq	24(%r12), %rax
	movq	8(%r12), %rsi
	imull	$-252645135, %eax, %eax
	movq	(%rdx), %rdx
	movq	96(%rsi), %rsi
	movq	7(%rdx), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	%rsi, -1(%rax,%rdx)
	movq	40(%r12), %rax
	movq	24(%r12), %r11
	movq	8(%r12), %rdi
	jmp	.L829
	.p2align 4,,10
	.p2align 3
.L833:
	movq	48(%r12), %rax
	subq	%r11, %rdx
	imull	$-252645135, %edx, %edx
	movq	(%rax), %rax
	leal	16(,%rdx,8), %edx
	movq	7(%rax), %rax
	movslq	%edx, %rdx
	movq	-1(%rdx,%rax), %rsi
	movq	41112(%rdi), %r11
	testq	%r11, %r11
	je	.L835
	movq	%r11, %rdi
	movq	%rcx, -168(%rbp)
	movl	%r10d, -160(%rbp)
	movl	%r8d, -152(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-152(%rbp), %r8d
	movl	-160(%rbp), %r10d
	movq	-168(%rbp), %rcx
.L836:
	movq	48(%r12), %rdx
	movq	8(%r12), %rsi
	movq	%rax, -127(%rbp)
	movq	40(%r12), %rax
	subq	24(%r12), %rax
	imull	$-252645135, %eax, %eax
	movq	(%rdx), %rdx
	movq	96(%rsi), %rsi
	movq	7(%rdx), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	%rsi, -1(%rax,%rdx)
	movq	8(%r12), %rdi
	jmp	.L834
	.p2align 4,,10
	.p2align 3
.L737:
	movq	48(%r12), %rdx
	subq	%rsi, %rax
	movq	8(%r12), %rbx
	imull	$-252645135, %eax, %eax
	movq	(%rdx), %rdx
	leal	16(,%rax,8), %eax
	movq	7(%rdx), %rdx
	cltq
	movq	-1(%rax,%rdx), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L739
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L740:
	movq	48(%r12), %rdx
	movq	8(%r12), %rcx
	movq	%rax, -127(%rbp)
	movq	40(%r12), %rax
	subq	24(%r12), %rax
	imull	$-252645135, %eax, %eax
	movq	(%rdx), %rdx
	movq	96(%rcx), %rcx
	movq	7(%rdx), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	%rcx, -1(%rax,%rdx)
	movq	40(%r12), %rax
	movq	24(%r12), %rsi
	jmp	.L738
	.p2align 4,,10
	.p2align 3
.L806:
	movq	48(%r12), %rdx
	subq	%rsi, %rax
	imull	$-252645135, %eax, %eax
	movq	(%rdx), %rdx
	leal	16(,%rax,8), %eax
	movq	7(%rdx), %rdx
	cltq
	movq	-1(%rax,%rdx), %rsi
	movq	41112(%rdi), %r10
	testq	%r10, %r10
	je	.L808
	movq	%r10, %rdi
	movq	%r9, -144(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-144(%rbp), %r9
.L809:
	movq	48(%r12), %rdx
	movq	8(%r12), %rcx
	movq	%rax, -127(%rbp)
	movq	40(%r12), %rax
	subq	24(%r12), %rax
	imull	$-252645135, %eax, %eax
	movq	(%rdx), %rdx
	movq	96(%rcx), %rcx
	movq	7(%rdx), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	%rcx, -1(%rax,%rdx)
	movq	40(%r12), %rax
	movq	24(%r12), %rsi
	movq	8(%r12), %rdi
	jmp	.L807
.L767:
	andl	39(%rdx), %edi
	movl	%r14d, %esi
	addq	23(%rdx), %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal4wasm19memory_fill_wrapperEmjj@PLT
	jmp	.L853
.L868:
	xorl	%esi, %esi
	xorl	%ecx, %ecx
	jmp	.L748
.L860:
	xorl	%eax, %eax
	jmp	.L685
.L723:
	xorl	%edx, %edx
	movq	%r8, %rsi
	call	_ZN2v88internal4wasm19memory_copy_wrapperEmmj@PLT
	jmp	.L853
.L867:
	xorl	%esi, %esi
	xorl	%r9d, %r9d
	jmp	.L722
.L935:
	movzbl	2(%rsi), %ecx
	movl	%ecx, %edx
	sall	$14, %edx
	andl	$2080768, %edx
	orl	%edx, %eax
	testb	%cl, %cl
	js	.L940
	sall	$11, %eax
	movl	$3, %edx
	sarl	$11, %eax
	jmp	.L729
.L936:
	movzbl	4(%rsi), %ecx
	movl	%ecx, %edx
	sall	$14, %edx
	andl	$2080768, %edx
	orl	%edx, %eax
	testb	%cl, %cl
	js	.L941
	sall	$11, %eax
	movl	$3, %ecx
	sarl	$11, %eax
	jmp	.L718
.L938:
	movzbl	2(%rsi), %ecx
	movl	%ecx, %edx
	sall	$14, %edx
	andl	$2080768, %edx
	orl	%edx, %eax
	testb	%cl, %cl
	js	.L942
	sall	$11, %eax
	movl	$3, %edx
	sarl	$11, %eax
	jmp	.L781
.L937:
	movzbl	4(%rdx), %esi
	movl	%esi, %eax
	sall	$14, %eax
	andl	$2080768, %eax
	orl	%eax, %ecx
	testb	%sil, %sil
	js	.L943
	sall	$11, %ecx
	movl	$4, %eax
	movl	$3, %edi
	sarl	$11, %ecx
	jmp	.L772
.L866:
	xorl	%eax, %eax
	jmp	.L713
.L793:
	movq	41088(%r10), %rax
	cmpq	41096(%r10), %rax
	je	.L944
.L795:
	leaq	8(%rax), %rdi
	movq	%rdi, 41088(%r10)
	movq	%rsi, (%rax)
	jmp	.L794
.L744:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L945
.L746:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%rsi, (%rax)
	jmp	.L745
.L651:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L946
.L653:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L652
.L808:
	movq	41088(%rdi), %rax
	cmpq	41096(%rdi), %rax
	je	.L947
.L810:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rdi)
	movq	%rsi, (%rax)
	jmp	.L809
.L798:
	movq	41088(%r10), %rax
	cmpq	41096(%r10), %rax
	je	.L948
.L800:
	leaq	8(%rax), %rdi
	movq	%rdi, 41088(%r10)
	movq	%rsi, (%rax)
	jmp	.L799
.L825:
	movq	41088(%rcx), %rax
	cmpq	41096(%rcx), %rax
	je	.L949
.L827:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rcx)
	movq	%rsi, (%rax)
	jmp	.L826
.L759:
	movq	41088(%r14), %rax
	cmpq	41096(%r14), %rax
	je	.L950
.L761:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r14)
	movq	%rsi, (%rax)
	jmp	.L760
.L734:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L951
.L736:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L735
.L688:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L952
.L690:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L689
.L709:
	movq	41088(%r13), %rbx
	cmpq	41096(%r13), %rbx
	je	.L953
.L711:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%rbx)
	jmp	.L710
.L830:
	movq	41088(%rdi), %rcx
	cmpq	41096(%rdi), %rcx
	je	.L954
.L832:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rdi)
	movq	%rsi, (%rcx)
	jmp	.L831
.L754:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L955
.L756:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L755
.L702:
	movq	41088(%r13), %rbx
	cmpq	41096(%r13), %rbx
	je	.L956
.L704:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%rbx)
	jmp	.L703
.L788:
	movq	41088(%r10), %rax
	cmpq	41096(%r10), %rax
	je	.L957
.L790:
	leaq	8(%rax), %rdi
	movq	%rdi, 41088(%r10)
	movq	%rsi, (%rax)
	jmp	.L789
.L739:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L958
.L741:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L740
.L679:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L959
.L681:
	movq	%rax, %rdi
	leaq	8(%rax), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rdi)
	jmp	.L680
.L661:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L960
.L663:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L662
.L695:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L961
.L697:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L696
.L764:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L962
.L766:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%rsi, (%rax)
	jmp	.L765
.L670:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L963
.L672:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L671
.L813:
	movq	41088(%rdi), %rcx
	cmpq	41096(%rdi), %rcx
	je	.L964
.L815:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rdi)
	movq	%rsi, (%rcx)
	jmp	.L814
.L835:
	movq	41088(%rdi), %rax
	cmpq	41096(%rdi), %rax
	je	.L965
.L837:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rdi)
	movq	%rsi, (%rax)
	jmp	.L836
.L939:
	movq	%r10, -184(%rbp)
	movq	%rcx, -176(%rbp)
	movl	%r8d, -168(%rbp)
	movl	%edx, -160(%rbp)
	movq	%rdi, -152(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-184(%rbp), %r10
	movq	-176(%rbp), %rcx
	movl	-168(%rbp), %r8d
	movl	-160(%rbp), %edx
	movq	%rax, %rsi
	movq	-152(%rbp), %rdi
	jmp	.L840
.L934:
	movq	%rdx, %rdi
	movq	%rsi, -152(%rbp)
	movq	%rdx, -144(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-152(%rbp), %rsi
	movq	-144(%rbp), %rdx
	jmp	.L820
.L933:
	movq	%rdx, %rdi
	movq	%rsi, -152(%rbp)
	movq	%rdx, -144(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-152(%rbp), %rsi
	movq	-144(%rbp), %rdx
	movq	%rax, %r9
	jmp	.L805
.L858:
	xorl	%eax, %eax
	jmp	.L676
.L857:
	xorl	%eax, %eax
	jmp	.L667
.L864:
	xorl	%eax, %eax
	jmp	.L706
.L863:
	xorl	%eax, %eax
	jmp	.L699
.L941:
	movzbl	5(%rsi), %ecx
	movl	%ecx, %edx
	sall	$21, %edx
	andl	$266338304, %edx
	orl	%eax, %edx
	testb	%cl, %cl
	js	.L966
	movl	%edx, %eax
	movl	$4, %ecx
	sall	$4, %eax
	sarl	$4, %eax
	jmp	.L718
.L942:
	movzbl	3(%rsi), %ecx
	movl	%ecx, %edx
	sall	$21, %edx
	andl	$266338304, %edx
	orl	%eax, %edx
	testb	%cl, %cl
	js	.L967
	movl	%edx, %eax
	movl	$4, %edx
	sall	$4, %eax
	sarl	$4, %eax
	jmp	.L781
.L943:
	movzbl	5(%rdx), %esi
	movl	%esi, %eax
	sall	$21, %eax
	andl	$266338304, %eax
	orl	%eax, %ecx
	testb	%sil, %sil
	js	.L968
	sall	$4, %ecx
	movl	$5, %eax
	movl	$4, %edi
	sarl	$4, %ecx
	jmp	.L772
.L940:
	movzbl	3(%rsi), %ecx
	movl	%ecx, %edx
	sall	$21, %edx
	andl	$266338304, %edx
	orl	%eax, %edx
	testb	%cl, %cl
	js	.L969
	movl	%edx, %eax
	movl	$4, %edx
	sall	$4, %eax
	sarl	$4, %eax
	jmp	.L729
.L966:
	movzbl	6(%rsi), %eax
	movl	$5, %ecx
	sall	$28, %eax
	orl	%edx, %eax
	jmp	.L718
.L969:
	movzbl	4(%rsi), %eax
	sall	$28, %eax
	orl	%edx, %eax
	movl	$5, %edx
	jmp	.L729
.L967:
	movzbl	4(%rsi), %eax
	sall	$28, %eax
	orl	%edx, %eax
	movl	$5, %edx
	jmp	.L781
.L968:
	movzbl	6(%rdx), %eax
	movl	$5, %edi
	sall	$28, %eax
	orl	%eax, %ecx
	movl	$6, %eax
	jmp	.L772
.L944:
	movq	%r10, %rdi
	movq	%rsi, -168(%rbp)
	movl	%ecx, -160(%rbp)
	movl	%edx, -152(%rbp)
	movl	%r8d, -144(%rbp)
	movq	%r10, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %rsi
	movl	-160(%rbp), %ecx
	movl	-152(%rbp), %edx
	movl	-144(%rbp), %r8d
	movq	-136(%rbp), %r10
	jmp	.L795
.L958:
	movq	%rbx, %rdi
	movq	%rsi, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rsi
	jmp	.L741
.L950:
	movq	%r14, %rdi
	movq	%rsi, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rsi
	jmp	.L761
.L951:
	movq	%rbx, %rdi
	movq	%rsi, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rsi
	jmp	.L736
.L956:
	movq	%r13, %rdi
	movq	%rsi, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L704
.L948:
	movq	%r10, %rdi
	movq	%rsi, -176(%rbp)
	movl	%ecx, -168(%rbp)
	movl	%edx, -160(%rbp)
	movl	%r8d, -152(%rbp)
	movl	%r9d, -144(%rbp)
	movq	%r10, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-176(%rbp), %rsi
	movl	-168(%rbp), %ecx
	movl	-160(%rbp), %edx
	movl	-152(%rbp), %r8d
	movl	-144(%rbp), %r9d
	movq	-136(%rbp), %r10
	jmp	.L800
.L960:
	movq	%rbx, %rdi
	movq	%rsi, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rsi
	jmp	.L663
.L952:
	movq	%rbx, %rdi
	movq	%rsi, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rsi
	jmp	.L690
.L965:
	movq	%rcx, -184(%rbp)
	movq	%rsi, -176(%rbp)
	movl	%r10d, -168(%rbp)
	movl	%r8d, -160(%rbp)
	movq	%rdi, -152(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-184(%rbp), %rcx
	movq	-176(%rbp), %rsi
	movl	-168(%rbp), %r10d
	movl	-160(%rbp), %r8d
	movq	-152(%rbp), %rdi
	jmp	.L837
.L954:
	movq	%rsi, -176(%rbp)
	movl	%r10d, -168(%rbp)
	movl	%r8d, -160(%rbp)
	movq	%rdi, -152(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-176(%rbp), %rsi
	movl	-168(%rbp), %r10d
	movl	-160(%rbp), %r8d
	movq	-152(%rbp), %rdi
	movq	%rax, %rcx
	jmp	.L832
.L953:
	movq	%r13, %rdi
	movq	%rsi, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L711
.L945:
	movq	%r15, %rdi
	movq	%rsi, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rsi
	jmp	.L746
.L946:
	movq	%rbx, %rdi
	movq	%rsi, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rsi
	jmp	.L653
.L949:
	movq	%rcx, %rdi
	movq	%rsi, -168(%rbp)
	movl	%r10d, -160(%rbp)
	movq	%rcx, -152(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %rsi
	movl	-160(%rbp), %r10d
	movq	-152(%rbp), %rcx
	jmp	.L827
.L957:
	movq	%r10, %rdi
	movq	%rsi, -160(%rbp)
	movl	%ecx, -152(%rbp)
	movl	%edx, -144(%rbp)
	movq	%r10, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-160(%rbp), %rsi
	movl	-152(%rbp), %ecx
	movl	-144(%rbp), %edx
	movq	-136(%rbp), %r10
	jmp	.L790
.L962:
	movq	%r15, %rdi
	movq	%rsi, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rsi
	jmp	.L766
.L959:
	movq	%rbx, %rdi
	movq	%rsi, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rsi
	jmp	.L681
.L963:
	movq	%rbx, %rdi
	movq	%rsi, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rsi
	jmp	.L672
.L947:
	movq	%rsi, -160(%rbp)
	movq	%r9, -152(%rbp)
	movq	%rdi, -144(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-160(%rbp), %rsi
	movq	-152(%rbp), %r9
	movq	-144(%rbp), %rdi
	jmp	.L810
.L961:
	movq	%rbx, %rdi
	movq	%rsi, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rsi
	jmp	.L697
.L955:
	movq	%rbx, %rdi
	movq	%rsi, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rsi
	jmp	.L756
.L964:
	movq	%rsi, -168(%rbp)
	movl	%edx, -160(%rbp)
	movq	%r9, -152(%rbp)
	movq	%rdi, -144(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %rsi
	movl	-160(%rbp), %edx
	movq	-152(%rbp), %r9
	movq	-144(%rbp), %rdi
	movq	%rax, %rcx
	jmp	.L815
.L629:
	movq	64(%rcx), %rax
	movq	%rcx, -136(%rbp)
	leaq	.LC8(%rip), %rdx
	movzbl	(%rax,%r8), %edi
	cmpl	$255, %edi
	je	.L845
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	-136(%rbp), %rcx
	movq	%rax, %rdx
	movq	64(%rcx), %rax
.L845:
	movzbl	(%rax,%r13), %esi
	leaq	.LC19(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L924:
	call	__stack_chk_fail@PLT
.L750:
	movq	23(%rdx), %rax
	movq	39(%rdx), %rdx
	movl	%ebx, %esi
	andl	%edx, %esi
	andl	%edx, %edi
	xorl	%edx, %edx
	addq	%rax, %rsi
	addq	%rax, %rdi
	call	_ZN2v88internal4wasm19memory_copy_wrapperEmmj@PLT
	jmp	.L853
	.cfi_endproc
.LFE20121:
	.size	_ZN2v88internal4wasm10ThreadImpl16ExecuteNumericOpENS1_10WasmOpcodeEPNS1_7DecoderEPNS1_15InterpreterCodeEmPi, .-_ZN2v88internal4wasm10ThreadImpl16ExecuteNumericOpENS1_10WasmOpcodeEPNS1_7DecoderEPNS1_15InterpreterCodeEmPi
	.section	.text._ZN2v88internal4wasm10ThreadImpl13ExecuteSimdOpENS1_10WasmOpcodeEPNS1_7DecoderEPNS1_15InterpreterCodeEmPi,"axG",@progbits,_ZN2v88internal4wasm10ThreadImpl13ExecuteSimdOpENS1_10WasmOpcodeEPNS1_7DecoderEPNS1_15InterpreterCodeEmPi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm10ThreadImpl13ExecuteSimdOpENS1_10WasmOpcodeEPNS1_7DecoderEPNS1_15InterpreterCodeEmPi
	.type	_ZN2v88internal4wasm10ThreadImpl13ExecuteSimdOpENS1_10WasmOpcodeEPNS1_7DecoderEPNS1_15InterpreterCodeEmPi, @function
_ZN2v88internal4wasm10ThreadImpl13ExecuteSimdOpENS1_10WasmOpcodeEPNS1_7DecoderEPNS1_15InterpreterCodeEmPi:
.LFB20253:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	subl	$64768, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$296, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$209, %esi
	ja	.L1327
	leaq	.L973(%rip), %rdx
	movq	%rdi, %r12
	movq	%r8, %rbx
	movslq	(%rdx,%rsi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm10ThreadImpl13ExecuteSimdOpENS1_10WasmOpcodeEPNS1_7DecoderEPNS1_15InterpreterCodeEmPi,"aG",@progbits,_ZN2v88internal4wasm10ThreadImpl13ExecuteSimdOpENS1_10WasmOpcodeEPNS1_7DecoderEPNS1_15InterpreterCodeEmPi,comdat
	.align 4
	.align 4
.L973:
	.long	.L1144-.L973
	.long	.L1143-.L973
	.long	.L1327-.L973
	.long	.L1142-.L973
	.long	.L1141-.L973
	.long	.L1140-.L973
	.long	.L1327-.L973
	.long	.L1139-.L973
	.long	.L1138-.L973
	.long	.L1137-.L973
	.long	.L1327-.L973
	.long	.L1136-.L973
	.long	.L1135-.L973
	.long	.L1134-.L973
	.long	.L1133-.L973
	.long	.L1132-.L973
	.long	.L1131-.L973
	.long	.L1130-.L973
	.long	.L1129-.L973
	.long	.L1128-.L973
	.long	.L1127-.L973
	.long	.L1126-.L973
	.long	.L1125-.L973
	.long	.L1124-.L973
	.long	.L1123-.L973
	.long	.L1122-.L973
	.long	.L1121-.L973
	.long	.L1120-.L973
	.long	.L1119-.L973
	.long	.L1118-.L973
	.long	.L1117-.L973
	.long	.L1116-.L973
	.long	.L1115-.L973
	.long	.L1114-.L973
	.long	.L1113-.L973
	.long	.L1112-.L973
	.long	.L1111-.L973
	.long	.L1110-.L973
	.long	.L1109-.L973
	.long	.L1108-.L973
	.long	.L1107-.L973
	.long	.L1106-.L973
	.long	.L1105-.L973
	.long	.L1104-.L973
	.long	.L1103-.L973
	.long	.L1102-.L973
	.long	.L1101-.L973
	.long	.L1100-.L973
	.long	.L1099-.L973
	.long	.L1098-.L973
	.long	.L1097-.L973
	.long	.L1096-.L973
	.long	.L1095-.L973
	.long	.L1094-.L973
	.long	.L1093-.L973
	.long	.L1092-.L973
	.long	.L1091-.L973
	.long	.L1090-.L973
	.long	.L1089-.L973
	.long	.L1088-.L973
	.long	.L1087-.L973
	.long	.L1086-.L973
	.long	.L1085-.L973
	.long	.L1084-.L973
	.long	.L1083-.L973
	.long	.L1082-.L973
	.long	.L1081-.L973
	.long	.L1080-.L973
	.long	.L1079-.L973
	.long	.L1078-.L973
	.long	.L1077-.L973
	.long	.L1076-.L973
	.long	.L1075-.L973
	.long	.L1074-.L973
	.long	.L1073-.L973
	.long	.L1072-.L973
	.long	.L1071-.L973
	.long	.L1070-.L973
	.long	.L1069-.L973
	.long	.L1068-.L973
	.long	.L1067-.L973
	.long	.L1066-.L973
	.long	.L1021-.L973
	.long	.L1065-.L973
	.long	.L1064-.L973
	.long	.L1063-.L973
	.long	.L1062-.L973
	.long	.L1061-.L973
	.long	.L1060-.L973
	.long	.L1059-.L973
	.long	.L1058-.L973
	.long	.L1057-.L973
	.long	.L1056-.L973
	.long	.L1055-.L973
	.long	.L1054-.L973
	.long	.L1053-.L973
	.long	.L1052-.L973
	.long	.L1051-.L973
	.long	.L1050-.L973
	.long	.L1021-.L973
	.long	.L1049-.L973
	.long	.L1048-.L973
	.long	.L1047-.L973
	.long	.L1046-.L973
	.long	.L1045-.L973
	.long	.L1044-.L973
	.long	.L1043-.L973
	.long	.L1042-.L973
	.long	.L1041-.L973
	.long	.L1040-.L973
	.long	.L1039-.L973
	.long	.L1038-.L973
	.long	.L1037-.L973
	.long	.L1036-.L973
	.long	.L1035-.L973
	.long	.L1034-.L973
	.long	.L1021-.L973
	.long	.L1033-.L973
	.long	.L1032-.L973
	.long	.L1031-.L973
	.long	.L1030-.L973
	.long	.L1029-.L973
	.long	.L1327-.L973
	.long	.L1327-.L973
	.long	.L1028-.L973
	.long	.L1327-.L973
	.long	.L1327-.L973
	.long	.L1027-.L973
	.long	.L1026-.L973
	.long	.L1025-.L973
	.long	.L1024-.L973
	.long	.L1023-.L973
	.long	.L1022-.L973
	.long	.L1021-.L973
	.long	.L1020-.L973
	.long	.L1019-.L973
	.long	.L1018-.L973
	.long	.L1017-.L973
	.long	.L1016-.L973
	.long	.L1327-.L973
	.long	.L1015-.L973
	.long	.L1014-.L973
	.long	.L1013-.L973
	.long	.L1012-.L973
	.long	.L1011-.L973
	.long	.L1010-.L973
	.long	.L1327-.L973
	.long	.L1327-.L973
	.long	.L1327-.L973
	.long	.L1009-.L973
	.long	.L1008-.L973
	.long	.L1327-.L973
	.long	.L1007-.L973
	.long	.L1006-.L973
	.long	.L1005-.L973
	.long	.L1004-.L973
	.long	.L1003-.L973
	.long	.L1002-.L973
	.long	.L1001-.L973
	.long	.L1000-.L973
	.long	.L999-.L973
	.long	.L998-.L973
	.long	.L1327-.L973
	.long	.L1327-.L973
	.long	.L1327-.L973
	.long	.L997-.L973
	.long	.L996-.L973
	.long	.L995-.L973
	.long	.L994-.L973
	.long	.L993-.L973
	.long	.L992-.L973
	.long	.L991-.L973
	.long	.L990-.L973
	.long	.L1327-.L973
	.long	.L1327-.L973
	.long	.L989-.L973
	.long	.L988-.L973
	.long	.L1327-.L973
	.long	.L1327-.L973
	.long	.L1327-.L973
	.long	.L1327-.L973
	.long	.L1327-.L973
	.long	.L1327-.L973
	.long	.L1327-.L973
	.long	.L1327-.L973
	.long	.L1327-.L973
	.long	.L1327-.L973
	.long	.L1327-.L973
	.long	.L1327-.L973
	.long	.L987-.L973
	.long	.L986-.L973
	.long	.L985-.L973
	.long	.L1327-.L973
	.long	.L1327-.L973
	.long	.L1327-.L973
	.long	.L1327-.L973
	.long	.L1327-.L973
	.long	.L1327-.L973
	.long	.L984-.L973
	.long	.L983-.L973
	.long	.L982-.L973
	.long	.L981-.L973
	.long	.L980-.L973
	.long	.L979-.L973
	.long	.L978-.L973
	.long	.L977-.L973
	.long	.L976-.L973
	.long	.L975-.L973
	.long	.L974-.L973
	.long	.L972-.L973
	.section	.text._ZN2v88internal4wasm10ThreadImpl13ExecuteSimdOpENS1_10WasmOpcodeEPNS1_7DecoderEPNS1_15InterpreterCodeEmPi,"axG",@progbits,_ZN2v88internal4wasm10ThreadImpl13ExecuteSimdOpENS1_10WasmOpcodeEPNS1_7DecoderEPNS1_15InterpreterCodeEmPi,comdat
	.p2align 4,,10
	.p2align 3
.L1327:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L970:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1377
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1021:
	.cfi_restore_state
	leaq	-80(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-79(%rbp), %rax
	movq	-71(%rbp), %rdx
	movq	%r12, %rdi
	pxor	%xmm0, %xmm0
	movb	$1, -80(%rbp)
	movq	%rax, %rcx
	movups	%xmm0, -79(%rbp)
	sarq	$32, %rcx
	orl	%ecx, %eax
	orl	%edx, %eax
	sarq	$32, %rdx
	orl	%edx, %eax
	setne	%al
	subq	$32, %rsp
	movzbl	%al, %eax
	movl	%eax, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm5
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1054:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movdqu	-143(%rbp), %xmm3
	movdqu	-111(%rbp), %xmm0
	movdqu	-111(%rbp), %xmm7
	movdqu	-143(%rbp), %xmm4
	movdqa	%xmm3, %xmm1
	movdqa	%xmm0, %xmm2
	pcmpgtb	%xmm0, %xmm1
	movaps	%xmm7, -192(%rbp)
	movaps	%xmm4, -176(%rbp)
	pand	%xmm1, %xmm2
	pandn	%xmm3, %xmm1
	movdqa	%xmm1, %xmm0
	por	%xmm2, %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm6
	movaps	%xmm0, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1055:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movdqu	-143(%rbp), %xmm1
	movdqu	-111(%rbp), %xmm0
	movdqu	-111(%rbp), %xmm4
	movdqu	-143(%rbp), %xmm6
	movdqa	%xmm0, %xmm2
	movdqa	%xmm1, %xmm3
	punpcklbw	%xmm1, %xmm3
	punpcklbw	%xmm0, %xmm2
	punpckhbw	%xmm1, %xmm1
	movaps	%xmm4, -192(%rbp)
	pmullw	%xmm3, %xmm2
	punpckhbw	%xmm0, %xmm0
	movdqa	.LC31(%rip), %xmm3
	movaps	%xmm6, -176(%rbp)
	pmullw	%xmm1, %xmm0
	pand	%xmm3, %xmm2
	pand	%xmm3, %xmm0
	movdqa	%xmm2, %xmm5
	packuswb	%xmm0, %xmm5
	movups	%xmm5, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movaps	%xmm5, -160(%rbp)
	movdqa	-80(%rbp), %xmm5
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1056:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movdqu	-111(%rbp), %xmm2
	movdqu	-143(%rbp), %xmm1
	movdqu	-111(%rbp), %xmm4
	movdqu	-143(%rbp), %xmm6
	movdqa	%xmm2, %xmm3
	psubb	%xmm1, %xmm3
	psubusb	%xmm2, %xmm1
	movaps	%xmm4, -192(%rbp)
	movdqa	%xmm1, %xmm0
	pxor	%xmm1, %xmm1
	movaps	%xmm6, -176(%rbp)
	pcmpeqb	%xmm1, %xmm0
	pand	%xmm3, %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm5
	movaps	%xmm0, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1057:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movdqu	-143(%rbp), %xmm7
	movq	%r12, %rdi
	pxor	%xmm13, %xmm13
	movdqa	%xmm13, %xmm11
	movdqu	-111(%rbp), %xmm6
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	pcmpgtb	%xmm7, %xmm11
	movdqa	%xmm7, %xmm2
	movdqa	%xmm7, %xmm0
	movdqu	-143(%rbp), %xmm5
	movaps	%xmm6, -192(%rbp)
	pxor	%xmm6, %xmm6
	movdqa	%xmm6, %xmm3
	movdqa	%xmm6, %xmm4
	movdqa	%xmm6, %xmm10
	movaps	%xmm5, -176(%rbp)
	punpcklbw	%xmm11, %xmm2
	punpckhbw	%xmm11, %xmm0
	movdqu	-111(%rbp), %xmm5
	pcmpgtw	%xmm2, %xmm3
	pcmpgtw	%xmm0, %xmm4
	movdqa	%xmm2, %xmm1
	movdqa	%xmm5, %xmm8
	movdqa	%xmm5, %xmm9
	movaps	%xmm5, -304(%rbp)
	punpcklwd	%xmm3, %xmm1
	punpckhwd	%xmm3, %xmm2
	movdqa	%xmm0, %xmm3
	punpcklwd	%xmm4, %xmm3
	punpckhwd	%xmm4, %xmm0
	movdqa	%xmm13, %xmm4
	pcmpgtb	%xmm5, %xmm4
	punpckhbw	%xmm4, %xmm8
	punpcklbw	%xmm4, %xmm9
	pcmpgtw	%xmm8, %xmm6
	movdqa	%xmm8, %xmm15
	movdqa	%xmm9, %xmm14
	pcmpgtw	%xmm9, %xmm10
	punpckhwd	%xmm6, %xmm15
	movdqa	%xmm15, %xmm5
	punpcklwd	%xmm10, %xmm14
	punpckhwd	%xmm10, %xmm9
	movdqa	.LC32(%rip), %xmm15
	movdqa	%xmm8, %xmm10
	movdqa	%xmm15, %xmm8
	punpcklwd	%xmm6, %xmm10
	movdqa	%xmm4, %xmm6
	paddd	%xmm3, %xmm8
	pcmpeqb	%xmm13, %xmm6
	movdqa	%xmm15, %xmm12
	movaps	%xmm8, -240(%rbp)
	movdqa	%xmm7, %xmm8
	paddd	%xmm1, %xmm12
	pcmpgtb	%xmm13, %xmm8
	pand	%xmm6, %xmm11
	movdqa	%xmm15, %xmm6
	paddd	%xmm0, %xmm15
	paddd	%xmm2, %xmm6
	movdqa	%xmm8, %xmm13
	pand	%xmm4, %xmm13
	movdqa	.LC33(%rip), %xmm4
	movaps	%xmm13, -272(%rbp)
	paddd	%xmm4, %xmm2
	paddd	%xmm4, %xmm3
	paddd	%xmm4, %xmm1
	paddd	%xmm0, %xmm4
	movdqa	%xmm11, %xmm0
	movaps	%xmm3, -288(%rbp)
	pandn	%xmm13, %xmm0
	pcmpgtd	%xmm14, %xmm1
	movdqa	%xmm2, %xmm8
	movaps	%xmm4, -256(%rbp)
	movaps	%xmm0, -224(%rbp)
	movdqa	%xmm14, %xmm0
	movdqa	-256(%rbp), %xmm14
	pcmpgtd	%xmm12, %xmm0
	movdqa	%xmm9, %xmm12
	pcmpgtd	%xmm6, %xmm12
	pcmpgtd	%xmm5, %xmm14
	movdqa	%xmm0, %xmm6
	punpckhwd	%xmm12, %xmm6
	movdqa	%xmm12, %xmm13
	movdqa	%xmm6, %xmm3
	movdqa	%xmm0, %xmm6
	punpcklwd	%xmm12, %xmm6
	movdqa	%xmm5, %xmm12
	movdqa	%xmm6, %xmm2
	pcmpgtd	%xmm15, %xmm12
	punpckhwd	%xmm3, %xmm6
	punpcklwd	%xmm3, %xmm2
	punpcklwd	%xmm6, %xmm2
	movdqa	%xmm10, %xmm6
	pcmpgtd	-240(%rbp), %xmm6
	movdqa	%xmm6, %xmm4
	punpckhwd	%xmm12, %xmm6
	movdqa	%xmm4, %xmm15
	punpcklwd	%xmm12, %xmm15
	movdqa	%xmm15, %xmm3
	punpckhwd	%xmm6, %xmm15
	punpcklwd	%xmm6, %xmm3
	movdqa	.LC31(%rip), %xmm6
	punpcklwd	%xmm15, %xmm3
	pand	%xmm6, %xmm3
	pand	%xmm6, %xmm2
	packuswb	%xmm3, %xmm2
	movdqa	%xmm2, %xmm15
	movdqa	%xmm8, %xmm2
	pcmpgtd	%xmm9, %xmm2
	pand	%xmm11, %xmm15
	movdqa	%xmm2, %xmm9
	movdqa	%xmm1, %xmm2
	punpckhwd	%xmm9, %xmm2
	movdqa	%xmm2, %xmm3
	movdqa	%xmm1, %xmm2
	punpcklwd	%xmm9, %xmm2
	movdqa	%xmm2, %xmm8
	punpcklwd	%xmm3, %xmm2
	punpckhwd	%xmm3, %xmm8
	movdqa	-288(%rbp), %xmm3
	punpcklwd	%xmm8, %xmm2
	movdqa	%xmm14, %xmm8
	pcmpgtd	%xmm10, %xmm3
	pand	%xmm6, %xmm2
	movdqa	%xmm3, %xmm10
	movdqa	%xmm3, %xmm14
	punpcklwd	%xmm8, %xmm10
	punpckhwd	%xmm8, %xmm14
	movdqa	%xmm10, %xmm5
	punpcklwd	%xmm14, %xmm10
	punpckhwd	%xmm14, %xmm5
	punpcklwd	%xmm5, %xmm10
	pand	%xmm6, %xmm10
	packuswb	%xmm10, %xmm2
	pxor	%xmm10, %xmm10
	pand	-224(%rbp), %xmm2
	pcmpeqd	%xmm10, %xmm0
	pcmpeqd	%xmm10, %xmm13
	pcmpeqd	%xmm10, %xmm12
	pcmpeqd	%xmm10, %xmm4
	pcmpeqd	%xmm10, %xmm9
	pcmpeqd	%xmm10, %xmm1
	pcmpeqd	%xmm10, %xmm8
	pcmpeqd	%xmm10, %xmm3
	movdqa	%xmm0, %xmm5
	punpckhwd	%xmm13, %xmm5
	punpcklwd	%xmm13, %xmm0
	movdqa	-272(%rbp), %xmm13
	movdqa	%xmm5, %xmm14
	movdqa	%xmm0, %xmm5
	punpckhwd	%xmm14, %xmm5
	punpcklwd	%xmm14, %xmm0
	por	%xmm11, %xmm13
	punpcklwd	%xmm5, %xmm0
	movdqa	%xmm4, %xmm5
	punpcklwd	%xmm12, %xmm4
	punpckhwd	%xmm12, %xmm5
	movdqa	%xmm4, %xmm12
	pand	%xmm6, %xmm0
	punpckhwd	%xmm5, %xmm12
	punpcklwd	%xmm5, %xmm4
	movdqa	-304(%rbp), %xmm5
	punpcklwd	%xmm12, %xmm4
	pand	%xmm6, %xmm4
	psubb	%xmm7, %xmm5
	packuswb	%xmm4, %xmm0
	movdqa	%xmm1, %xmm4
	punpcklwd	%xmm9, %xmm1
	punpckhwd	%xmm9, %xmm4
	movdqa	%xmm1, %xmm9
	pand	%xmm11, %xmm0
	punpckhwd	%xmm4, %xmm9
	punpcklwd	%xmm4, %xmm1
	movdqa	%xmm3, %xmm4
	punpcklwd	%xmm8, %xmm3
	punpckhwd	%xmm8, %xmm4
	punpcklwd	%xmm9, %xmm1
	movdqa	%xmm3, %xmm8
	punpcklwd	%xmm4, %xmm3
	pand	%xmm6, %xmm1
	punpckhwd	%xmm4, %xmm8
	pcmpeqd	%xmm11, %xmm11
	punpcklwd	%xmm8, %xmm3
	pxor	%xmm11, %xmm13
	pand	%xmm6, %xmm3
	packuswb	%xmm3, %xmm1
	pand	-224(%rbp), %xmm1
	por	%xmm1, %xmm0
	movdqa	.LC34(%rip), %xmm1
	por	%xmm13, %xmm0
	pand	%xmm0, %xmm5
	pandn	%xmm1, %xmm0
	pand	%xmm2, %xmm1
	por	%xmm5, %xmm0
	pandn	%xmm0, %xmm2
	movdqa	%xmm15, %xmm0
	por	%xmm1, %xmm2
	movdqa	.LC35(%rip), %xmm1
	pandn	%xmm2, %xmm0
	pand	%xmm15, %xmm1
	por	%xmm1, %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm7
	movaps	%xmm0, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1058:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movdqu	-111(%rbp), %xmm0
	movdqu	-143(%rbp), %xmm5
	movdqu	-143(%rbp), %xmm6
	movaps	%xmm0, -192(%rbp)
	psubb	%xmm5, %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm7
	movaps	%xmm6, -176(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	movaps	%xmm0, -160(%rbp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1059:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	pxor	%xmm1, %xmm1
	movb	$5, -80(%rbp)
	movq	%r12, %rdi
	movdqu	-111(%rbp), %xmm3
	movdqu	-143(%rbp), %xmm4
	subq	$32, %rsp
	movdqu	-111(%rbp), %xmm5
	movdqu	-143(%rbp), %xmm7
	movdqa	%xmm4, %xmm9
	movdqa	%xmm3, %xmm0
	movdqa	%xmm4, %xmm8
	punpcklbw	%xmm1, %xmm9
	movaps	%xmm5, -192(%rbp)
	punpcklbw	%xmm1, %xmm0
	movdqa	%xmm3, %xmm2
	movdqa	.LC36(%rip), %xmm5
	movaps	%xmm7, -176(%rbp)
	movdqa	%xmm9, %xmm6
	pxor	%xmm7, %xmm7
	punpckhbw	%xmm1, %xmm8
	punpckhbw	%xmm1, %xmm2
	punpcklwd	%xmm7, %xmm6
	movdqa	%xmm0, %xmm1
	movdqa	%xmm5, %xmm15
	punpckhwd	%xmm7, %xmm9
	psubd	%xmm6, %xmm15
	punpcklwd	%xmm7, %xmm1
	punpckhwd	%xmm7, %xmm0
	pcmpgtd	%xmm15, %xmm1
	movdqa	%xmm5, %xmm15
	pxor	%xmm6, %xmm6
	psubd	%xmm9, %xmm15
	paddb	%xmm4, %xmm3
	pcmpgtd	%xmm15, %xmm0
	pcmpeqd	%xmm6, %xmm1
	pcmpeqd	%xmm6, %xmm0
	movdqa	%xmm1, %xmm9
	punpcklwd	%xmm0, %xmm1
	punpckhwd	%xmm0, %xmm9
	movdqa	%xmm1, %xmm0
	punpcklwd	%xmm9, %xmm1
	punpckhwd	%xmm9, %xmm0
	movdqa	%xmm5, %xmm9
	punpcklwd	%xmm0, %xmm1
	movdqa	%xmm8, %xmm0
	punpckhwd	%xmm7, %xmm8
	punpcklwd	%xmm7, %xmm0
	psubd	%xmm8, %xmm5
	psubd	%xmm0, %xmm9
	movdqa	%xmm2, %xmm0
	punpckhwd	%xmm7, %xmm2
	punpcklwd	%xmm7, %xmm0
	pcmpgtd	%xmm5, %xmm2
	pcmpgtd	%xmm9, %xmm0
	pcmpeqd	%xmm6, %xmm2
	pcmpeqd	%xmm6, %xmm0
	movdqa	%xmm0, %xmm5
	punpcklwd	%xmm2, %xmm0
	punpckhwd	%xmm2, %xmm5
	movdqa	%xmm0, %xmm2
	punpckhwd	%xmm5, %xmm2
	punpcklwd	%xmm5, %xmm0
	punpcklwd	%xmm2, %xmm0
	movdqa	.LC31(%rip), %xmm2
	pand	%xmm2, %xmm0
	pand	%xmm2, %xmm1
	movdqa	%xmm3, %xmm2
	packuswb	%xmm0, %xmm1
	pcmpeqd	%xmm0, %xmm0
	pand	%xmm1, %xmm2
	pandn	%xmm0, %xmm1
	movdqa	%xmm1, %xmm0
	por	%xmm2, %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm4
	movaps	%xmm0, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm4, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1060:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	pxor	%xmm1, %xmm1
	movb	$5, -80(%rbp)
	movq	%r12, %rdi
	movdqu	-143(%rbp), %xmm4
	movdqa	%xmm1, %xmm6
	movdqu	-111(%rbp), %xmm7
	subq	$32, %rsp
	movdqu	-143(%rbp), %xmm5
	movdqu	-111(%rbp), %xmm8
	movaps	%xmm4, -176(%rbp)
	pxor	%xmm4, %xmm4
	movdqa	%xmm4, %xmm3
	movdqa	%xmm5, %xmm2
	movdqa	%xmm5, %xmm0
	movaps	%xmm7, -192(%rbp)
	pcmpgtb	%xmm5, %xmm3
	movdqa	%xmm4, %xmm15
	movdqa	%xmm8, %xmm10
	pcmpgtb	%xmm8, %xmm15
	movdqa	%xmm8, %xmm9
	punpcklbw	%xmm3, %xmm2
	punpckhbw	%xmm3, %xmm0
	pcmpgtw	%xmm2, %xmm6
	movdqa	%xmm2, %xmm13
	movdqa	%xmm0, %xmm12
	punpckhbw	%xmm15, %xmm9
	punpcklbw	%xmm15, %xmm10
	pand	%xmm3, %xmm15
	movdqa	.LC33(%rip), %xmm3
	movdqa	%xmm10, %xmm14
	movdqa	%xmm9, %xmm11
	movdqa	%xmm9, %xmm7
	movaps	%xmm15, -304(%rbp)
	punpcklwd	%xmm6, %xmm13
	punpckhwd	%xmm6, %xmm2
	movdqa	%xmm1, %xmm6
	pcmpgtw	%xmm0, %xmm6
	punpcklwd	%xmm6, %xmm12
	punpckhwd	%xmm6, %xmm0
	movdqa	%xmm1, %xmm6
	pcmpgtw	%xmm10, %xmm6
	pcmpgtw	%xmm9, %xmm1
	punpcklwd	%xmm6, %xmm14
	punpckhwd	%xmm6, %xmm10
	punpcklwd	%xmm1, %xmm11
	punpckhwd	%xmm1, %xmm7
	movdqa	%xmm8, %xmm6
	movdqa	%xmm5, %xmm1
	pcmpgtb	%xmm4, %xmm1
	pcmpgtb	%xmm4, %xmm6
	paddb	%xmm8, %xmm5
	movaps	%xmm7, -256(%rbp)
	pand	%xmm1, %xmm6
	movdqa	.LC32(%rip), %xmm1
	movaps	%xmm6, -224(%rbp)
	movdqa	%xmm1, %xmm7
	movdqa	%xmm1, %xmm4
	movdqa	%xmm1, %xmm9
	psubd	%xmm0, %xmm1
	psubd	%xmm13, %xmm7
	movaps	%xmm1, -272(%rbp)
	movdqa	%xmm3, %xmm1
	psubd	%xmm2, %xmm4
	psubd	%xmm12, %xmm9
	psubd	%xmm13, %xmm1
	movdqa	%xmm3, %xmm13
	psubd	%xmm2, %xmm13
	pcmpgtd	%xmm14, %xmm1
	movdqa	%xmm3, %xmm2
	psubd	%xmm12, %xmm2
	movdqa	%xmm3, %xmm12
	movaps	%xmm13, -320(%rbp)
	movdqa	%xmm10, %xmm13
	psubd	%xmm0, %xmm12
	movdqa	-224(%rbp), %xmm0
	pcmpgtd	%xmm4, %xmm13
	movaps	%xmm2, -288(%rbp)
	movdqa	%xmm12, %xmm6
	movdqa	-256(%rbp), %xmm12
	pandn	%xmm15, %xmm0
	pcmpgtd	-272(%rbp), %xmm12
	movaps	%xmm0, -240(%rbp)
	movdqa	%xmm14, %xmm0
	movdqa	%xmm6, %xmm14
	pcmpgtd	-256(%rbp), %xmm14
	pcmpgtd	%xmm7, %xmm0
	movdqa	%xmm0, %xmm4
	punpckhwd	%xmm13, %xmm4
	movdqa	%xmm4, %xmm3
	movdqa	%xmm0, %xmm4
	punpcklwd	%xmm13, %xmm4
	movdqa	%xmm4, %xmm2
	punpckhwd	%xmm3, %xmm4
	punpcklwd	%xmm3, %xmm2
	punpcklwd	%xmm4, %xmm2
	movdqa	%xmm11, %xmm4
	pcmpgtd	%xmm9, %xmm4
	movdqa	%xmm4, %xmm9
	movdqa	%xmm4, %xmm7
	punpcklwd	%xmm12, %xmm9
	punpckhwd	%xmm12, %xmm7
	movdqa	%xmm9, %xmm3
	punpckhwd	%xmm7, %xmm9
	punpcklwd	%xmm7, %xmm3
	movdqa	.LC31(%rip), %xmm7
	punpcklwd	%xmm9, %xmm3
	pand	%xmm7, %xmm3
	pand	%xmm7, %xmm2
	packuswb	%xmm3, %xmm2
	movdqa	-320(%rbp), %xmm3
	pand	-224(%rbp), %xmm2
	pcmpgtd	%xmm10, %xmm3
	movdqa	%xmm2, %xmm15
	movdqa	%xmm1, %xmm2
	movdqa	%xmm3, %xmm10
	movdqa	%xmm1, %xmm3
	punpcklwd	%xmm10, %xmm2
	punpckhwd	%xmm10, %xmm3
	movdqa	%xmm2, %xmm9
	punpcklwd	%xmm3, %xmm2
	punpckhwd	%xmm3, %xmm9
	movdqa	-288(%rbp), %xmm3
	punpcklwd	%xmm9, %xmm2
	movdqa	%xmm14, %xmm9
	pcmpgtd	%xmm11, %xmm3
	pand	%xmm7, %xmm2
	movdqa	%xmm3, %xmm11
	movdqa	%xmm3, %xmm14
	punpcklwd	%xmm9, %xmm11
	punpckhwd	%xmm9, %xmm14
	movdqa	%xmm11, %xmm6
	punpcklwd	%xmm14, %xmm11
	punpckhwd	%xmm14, %xmm6
	punpcklwd	%xmm6, %xmm11
	pand	%xmm7, %xmm11
	packuswb	%xmm11, %xmm2
	pxor	%xmm11, %xmm11
	pand	-240(%rbp), %xmm2
	pcmpeqd	%xmm11, %xmm13
	pcmpeqd	%xmm11, %xmm0
	pcmpeqd	%xmm11, %xmm12
	pcmpeqd	%xmm11, %xmm4
	pcmpeqd	%xmm11, %xmm10
	pcmpeqd	%xmm11, %xmm1
	pcmpeqd	%xmm11, %xmm9
	pcmpeqd	%xmm11, %xmm3
	movdqa	%xmm0, %xmm6
	punpcklwd	%xmm13, %xmm0
	punpckhwd	%xmm13, %xmm6
	movdqa	%xmm0, %xmm13
	punpcklwd	%xmm6, %xmm0
	punpckhwd	%xmm6, %xmm13
	movdqa	-224(%rbp), %xmm6
	punpcklwd	%xmm13, %xmm0
	movdqa	%xmm4, %xmm13
	punpcklwd	%xmm12, %xmm4
	punpckhwd	%xmm12, %xmm13
	movdqa	%xmm4, %xmm12
	pand	%xmm7, %xmm0
	punpckhwd	%xmm13, %xmm12
	punpcklwd	%xmm13, %xmm4
	punpcklwd	%xmm12, %xmm4
	pand	%xmm7, %xmm4
	packuswb	%xmm4, %xmm0
	movdqa	%xmm1, %xmm4
	punpcklwd	%xmm10, %xmm1
	punpckhwd	%xmm10, %xmm4
	movdqa	%xmm1, %xmm10
	pand	%xmm6, %xmm0
	punpckhwd	%xmm4, %xmm10
	punpcklwd	%xmm4, %xmm1
	movdqa	%xmm3, %xmm4
	punpcklwd	%xmm9, %xmm3
	punpckhwd	%xmm9, %xmm4
	punpcklwd	%xmm10, %xmm1
	movdqa	%xmm3, %xmm9
	punpcklwd	%xmm4, %xmm3
	pand	%xmm7, %xmm1
	punpckhwd	%xmm4, %xmm9
	por	-304(%rbp), %xmm6
	punpcklwd	%xmm9, %xmm3
	pand	%xmm7, %xmm3
	packuswb	%xmm3, %xmm1
	pand	-240(%rbp), %xmm1
	por	%xmm1, %xmm0
	pcmpeqd	%xmm1, %xmm1
	pxor	%xmm1, %xmm6
	movdqa	.LC34(%rip), %xmm1
	por	%xmm6, %xmm0
	pand	%xmm0, %xmm5
	pandn	%xmm1, %xmm0
	pand	%xmm2, %xmm1
	por	%xmm5, %xmm0
	pandn	%xmm0, %xmm2
	movdqa	%xmm15, %xmm0
	por	%xmm1, %xmm2
	movdqa	.LC35(%rip), %xmm1
	pandn	%xmm2, %xmm0
	pand	%xmm15, %xmm1
	por	%xmm1, %xmm0
	movaps	%xmm0, -160(%rbp)
	movups	%xmm0, -79(%rbp)
	movdqa	-80(%rbp), %xmm6
	movzbl	-64(%rbp), %eax
	movups	%xmm6, (%rsp)
	movb	%al, 16(%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1061:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movdqu	-143(%rbp), %xmm0
	movdqu	-111(%rbp), %xmm7
	movdqu	-111(%rbp), %xmm6
	movdqu	-143(%rbp), %xmm5
	paddb	%xmm7, %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm4
	movaps	%xmm6, -192(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm4, (%rsp)
	movaps	%xmm5, -176(%rbp)
	movaps	%xmm0, -160(%rbp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1062:
	leaq	-80(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	movl	-79(%rbp), %ebx
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	pxor	%xmm0, %xmm0
	pxor	%xmm4, %xmm4
	movq	%r12, %rdi
	movdqu	-111(%rbp), %xmm1
	movslq	%ebx, %rax
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movq	%rax, %xmm3
	movdqu	-111(%rbp), %xmm6
	movdqa	%xmm1, %xmm2
	punpckhbw	%xmm0, %xmm1
	punpcklbw	%xmm0, %xmm2
	movaps	%xmm6, -160(%rbp)
	movdqa	%xmm2, %xmm0
	punpckhwd	%xmm4, %xmm2
	punpcklwd	%xmm4, %xmm0
	psrad	%xmm3, %xmm2
	psrad	%xmm3, %xmm0
	movdqa	%xmm0, %xmm5
	punpcklwd	%xmm2, %xmm0
	punpckhwd	%xmm2, %xmm5
	movdqa	%xmm0, %xmm2
	punpckhwd	%xmm5, %xmm2
	punpcklwd	%xmm5, %xmm0
	punpcklwd	%xmm2, %xmm0
	movdqa	%xmm1, %xmm2
	punpckhwd	%xmm4, %xmm1
	punpcklwd	%xmm4, %xmm2
	psrad	%xmm3, %xmm1
	psrad	%xmm3, %xmm2
	movdqa	%xmm2, %xmm7
	movdqa	%xmm2, %xmm3
	punpcklwd	%xmm1, %xmm7
	punpckhwd	%xmm1, %xmm3
	movdqa	%xmm7, %xmm2
	movdqa	%xmm7, %xmm1
	punpckhwd	%xmm3, %xmm2
	punpcklwd	%xmm3, %xmm1
	punpcklwd	%xmm2, %xmm1
	movdqa	.LC31(%rip), %xmm2
	pand	%xmm2, %xmm0
	pand	%xmm2, %xmm1
	packuswb	%xmm1, %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm5
	movaps	%xmm0, -144(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1063:
	leaq	-80(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	movl	-79(%rbp), %ebx
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	pxor	%xmm0, %xmm0
	pxor	%xmm4, %xmm4
	movq	%r12, %rdi
	movdqu	-111(%rbp), %xmm1
	movdqa	%xmm4, %xmm5
	movslq	%ebx, %rax
	movb	$5, -80(%rbp)
	movdqu	-111(%rbp), %xmm3
	subq	$32, %rsp
	pcmpgtb	%xmm1, %xmm0
	movdqa	%xmm1, %xmm2
	movaps	%xmm3, -160(%rbp)
	movq	%rax, %xmm3
	punpcklbw	%xmm0, %xmm2
	punpckhbw	%xmm0, %xmm1
	pcmpgtw	%xmm2, %xmm5
	pcmpgtw	%xmm1, %xmm4
	movdqa	%xmm2, %xmm0
	punpcklwd	%xmm5, %xmm0
	punpckhwd	%xmm5, %xmm2
	psrad	%xmm3, %xmm2
	psrad	%xmm3, %xmm0
	movdqa	%xmm0, %xmm5
	punpcklwd	%xmm2, %xmm0
	punpckhwd	%xmm2, %xmm5
	movdqa	%xmm0, %xmm2
	punpckhwd	%xmm5, %xmm2
	punpcklwd	%xmm5, %xmm0
	punpcklwd	%xmm2, %xmm0
	movdqa	%xmm1, %xmm2
	punpckhwd	%xmm4, %xmm1
	punpcklwd	%xmm4, %xmm2
	psrad	%xmm3, %xmm1
	psrad	%xmm3, %xmm2
	movdqa	%xmm2, %xmm7
	movdqa	%xmm2, %xmm3
	punpcklwd	%xmm1, %xmm7
	punpckhwd	%xmm1, %xmm3
	movdqa	%xmm7, %xmm2
	movdqa	%xmm7, %xmm1
	punpckhwd	%xmm3, %xmm2
	punpcklwd	%xmm3, %xmm1
	punpcklwd	%xmm2, %xmm1
	movdqa	.LC31(%rip), %xmm2
	pand	%xmm2, %xmm0
	pand	%xmm2, %xmm1
	packuswb	%xmm1, %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm4
	movaps	%xmm0, -144(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm4, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1064:
	leaq	-80(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	movl	-79(%rbp), %ebx
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	pxor	%xmm0, %xmm0
	pxor	%xmm4, %xmm4
	movq	%r12, %rdi
	movdqu	-111(%rbp), %xmm1
	movslq	%ebx, %rax
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movq	%rax, %xmm3
	movdqu	-111(%rbp), %xmm5
	movdqa	%xmm1, %xmm2
	punpckhbw	%xmm0, %xmm1
	punpcklbw	%xmm0, %xmm2
	movaps	%xmm5, -160(%rbp)
	movdqa	%xmm2, %xmm0
	punpckhwd	%xmm4, %xmm2
	punpcklwd	%xmm4, %xmm0
	pslld	%xmm3, %xmm2
	pslld	%xmm3, %xmm0
	movdqa	%xmm0, %xmm5
	punpcklwd	%xmm2, %xmm0
	punpckhwd	%xmm2, %xmm5
	movdqa	%xmm0, %xmm2
	punpckhwd	%xmm5, %xmm2
	punpcklwd	%xmm5, %xmm0
	punpcklwd	%xmm2, %xmm0
	movdqa	%xmm1, %xmm2
	punpckhwd	%xmm4, %xmm1
	punpcklwd	%xmm4, %xmm2
	pslld	%xmm3, %xmm1
	pslld	%xmm3, %xmm2
	movdqa	%xmm2, %xmm7
	movdqa	%xmm2, %xmm3
	punpcklwd	%xmm1, %xmm7
	punpckhwd	%xmm1, %xmm3
	movdqa	%xmm7, %xmm2
	movdqa	%xmm7, %xmm1
	punpckhwd	%xmm3, %xmm2
	punpcklwd	%xmm3, %xmm1
	punpcklwd	%xmm2, %xmm1
	movdqa	.LC31(%rip), %xmm2
	pand	%xmm2, %xmm0
	pand	%xmm2, %xmm1
	packuswb	%xmm1, %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm7
	movaps	%xmm0, -144(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1065:
	leaq	-80(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-79(%rbp), %rcx
	movq	-71(%rbp), %rdx
	movq	%r12, %rdi
	pxor	%xmm0, %xmm0
	movb	$1, -80(%rbp)
	testb	%cl, %cl
	movl	%ecx, %esi
	movups	%xmm0, -79(%rbp)
	setne	%al
	sarw	$8, %si
	testb	%sil, %sil
	setne	%sil
	andl	%esi, %eax
	movl	%ecx, %esi
	sall	$8, %esi
	sarl	$24, %esi
	testb	%sil, %sil
	setne	%sil
	andl	%esi, %eax
	movl	%ecx, %esi
	sarl	$24, %esi
	testb	%sil, %sil
	setne	%sil
	andl	%esi, %eax
	movq	%rcx, %rsi
	salq	$24, %rsi
	sarq	$56, %rsi
	testb	%sil, %sil
	setne	%sil
	andl	%esi, %eax
	movq	%rcx, %rsi
	salq	$16, %rsi
	sarq	$56, %rsi
	testb	%sil, %sil
	setne	%sil
	andl	%esi, %eax
	movq	%rcx, %rsi
	salq	$8, %rsi
	sarq	$56, %rsi
	testb	%sil, %sil
	setne	%sil
	sarq	$56, %rcx
	andl	%esi, %eax
	testb	%cl, %cl
	setne	%cl
	andl	%ecx, %eax
	testb	%dl, %dl
	setne	%cl
	andl	%ecx, %eax
	movl	%edx, %ecx
	sarw	$8, %cx
	testb	%cl, %cl
	setne	%cl
	andl	%ecx, %eax
	movl	%edx, %ecx
	sall	$8, %ecx
	sarl	$24, %ecx
	testb	%cl, %cl
	setne	%cl
	andl	%ecx, %eax
	movl	%edx, %ecx
	sarl	$24, %ecx
	testb	%cl, %cl
	setne	%cl
	andl	%ecx, %eax
	movq	%rdx, %rcx
	salq	$24, %rcx
	sarq	$56, %rcx
	testb	%cl, %cl
	setne	%cl
	andl	%ecx, %eax
	movq	%rdx, %rcx
	salq	$16, %rcx
	sarq	$56, %rcx
	testb	%cl, %cl
	setne	%cl
	andl	%ecx, %eax
	movq	%rdx, %rcx
	salq	$8, %rcx
	sarq	$56, %rcx
	testb	%cl, %cl
	setne	%cl
	sarq	$56, %rdx
	andl	%ecx, %eax
	testb	%dl, %dl
	setne	%dl
	subq	$32, %rsp
	andl	%edx, %eax
	movzbl	%al, %eax
	movl	%eax, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm6
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1017:
	leaq	-80(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-79(%rbp), %ebx
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rdx
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	movl	%ebx, %ecx
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	shrq	%cl, %rax
	shrq	%cl, %rdx
	movq	%rax, -71(%rbp)
	movzbl	-64(%rbp), %eax
	movq	%rdx, -79(%rbp)
	movdqa	-80(%rbp), %xmm3
	movb	%al, 16(%rsp)
	movups	%xmm3, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1018:
	leaq	-80(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-79(%rbp), %ebx
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rax
	movb	$5, -80(%rbp)
	movq	%r12, %rdi
	movl	%ebx, %ecx
	subq	$32, %rsp
	sarq	%cl, %rax
	movq	%rax, %xmm0
	movq	-103(%rbp), %rax
	sarq	%cl, %rax
	movq	%rax, %xmm5
	punpcklqdq	%xmm5, %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm7
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1019:
	leaq	-80(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-79(%rbp), %ebx
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rdx
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	movl	%ebx, %ecx
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	salq	%cl, %rax
	salq	%cl, %rdx
	movq	%rax, -71(%rbp)
	movzbl	-64(%rbp), %eax
	movq	%rdx, -79(%rbp)
	movdqa	-80(%rbp), %xmm6
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1020:
	leaq	-80(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-79(%rbp), %rdx
	movq	-71(%rbp), %rax
	movq	%r12, %rdi
	pxor	%xmm0, %xmm0
	movb	$1, -80(%rbp)
	testq	%rdx, %rdx
	movups	%xmm0, -79(%rbp)
	setne	%dl
	testq	%rax, %rax
	setne	%al
	subq	$32, %rsp
	movzbl	%al, %eax
	andl	%edx, %eax
	movl	%eax, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm7
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1022:
	movq	%r12, %rsi
	leaq	-112(%rbp), %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-103(%rbp), %rdx
	movq	-111(%rbp), %rax
	movabsq	$-9223372036854775808, %rsi
	movq	%rdx, %rcx
	cmpq	%rsi, %rax
	je	.L1199
	negq	%rax
.L1199:
	movabsq	$-9223372036854775808, %rsi
	cmpq	%rsi, %rdx
	je	.L1200
	movq	%rdx, %rcx
	negq	%rcx
.L1200:
	movq	%rax, %xmm0
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	%rcx, %xmm7
	punpcklqdq	%xmm7, %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm4
	movb	%al, 16(%rsp)
	movups	%xmm4, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1023:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rdx
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	movdqa	.LC25(%rip), %xmm2
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movl	%edx, -192(%rbp)
	sarq	$32, %rdx
	movl	%eax, -184(%rbp)
	sarq	$32, %rax
	movl	%edx, -188(%rbp)
	movq	-143(%rbp), %rdx
	movl	%eax, -180(%rbp)
	movq	-135(%rbp), %rax
	movl	%edx, -176(%rbp)
	movdqa	-192(%rbp), %xmm0
	sarq	$32, %rdx
	movl	%eax, -168(%rbp)
	sarq	$32, %rax
	movl	%eax, -164(%rbp)
	movdqa	%xmm0, %xmm1
	movl	%edx, -172(%rbp)
	movdqa	-176(%rbp), %xmm5
	psubd	%xmm2, %xmm1
	psubd	%xmm2, %xmm5
	movdqa	%xmm0, %xmm2
	pcmpgtd	%xmm5, %xmm1
	pand	%xmm1, %xmm2
	pandn	-176(%rbp), %xmm1
	movdqa	%xmm1, %xmm0
	por	%xmm2, %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm7
	movaps	%xmm0, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1024:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rdx
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movl	%edx, -192(%rbp)
	sarq	$32, %rdx
	movl	%eax, -184(%rbp)
	sarq	$32, %rax
	movl	%edx, -188(%rbp)
	movq	-143(%rbp), %rdx
	movl	%eax, -180(%rbp)
	movq	-135(%rbp), %rax
	movl	%edx, -176(%rbp)
	sarq	$32, %rdx
	movl	%eax, -168(%rbp)
	sarq	$32, %rax
	movl	%eax, -164(%rbp)
	movl	%edx, -172(%rbp)
	movdqa	-176(%rbp), %xmm0
	movdqa	%xmm0, %xmm1
	movdqa	%xmm0, %xmm2
	pcmpgtd	-192(%rbp), %xmm1
	pand	%xmm1, %xmm2
	pandn	-192(%rbp), %xmm1
	movdqa	%xmm1, %xmm0
	por	%xmm2, %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm5
	movaps	%xmm0, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1025:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rdx
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	movdqa	.LC25(%rip), %xmm2
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movl	%edx, -192(%rbp)
	sarq	$32, %rdx
	movl	%eax, -184(%rbp)
	sarq	$32, %rax
	movl	%edx, -188(%rbp)
	movq	-143(%rbp), %rdx
	movl	%eax, -180(%rbp)
	movq	-135(%rbp), %rax
	movl	%edx, -176(%rbp)
	movdqa	-192(%rbp), %xmm0
	sarq	$32, %rdx
	movl	%eax, -168(%rbp)
	sarq	$32, %rax
	movl	%eax, -164(%rbp)
	movdqa	%xmm0, %xmm5
	movl	%edx, -172(%rbp)
	movdqa	-176(%rbp), %xmm1
	psubd	%xmm2, %xmm5
	psubd	%xmm2, %xmm1
	movdqa	%xmm0, %xmm2
	pcmpgtd	%xmm5, %xmm1
	pand	%xmm1, %xmm2
	pandn	-176(%rbp), %xmm1
	movdqa	%xmm1, %xmm0
	por	%xmm2, %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm6
	movaps	%xmm0, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1026:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rdx
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movl	%edx, -192(%rbp)
	sarq	$32, %rdx
	movl	%eax, -184(%rbp)
	sarq	$32, %rax
	movl	%edx, -188(%rbp)
	movq	-143(%rbp), %rdx
	movl	%eax, -180(%rbp)
	movq	-135(%rbp), %rax
	movl	%edx, -176(%rbp)
	sarq	$32, %rdx
	movdqa	-192(%rbp), %xmm1
	movl	%eax, -168(%rbp)
	sarq	$32, %rax
	movl	%eax, -164(%rbp)
	movl	%edx, -172(%rbp)
	movdqa	-176(%rbp), %xmm0
	pcmpgtd	-192(%rbp), %xmm0
	pand	%xmm0, %xmm1
	pandn	-176(%rbp), %xmm0
	por	%xmm1, %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm7
	movaps	%xmm0, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1027:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rdx
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movl	%edx, -192(%rbp)
	sarq	$32, %rdx
	movl	%eax, -184(%rbp)
	sarq	$32, %rax
	movl	%edx, -188(%rbp)
	movq	-143(%rbp), %rdx
	movl	%eax, -180(%rbp)
	movq	-135(%rbp), %rax
	movl	%edx, -176(%rbp)
	sarq	$32, %rdx
	movdqa	-192(%rbp), %xmm1
	movl	%eax, -168(%rbp)
	sarq	$32, %rax
	movdqa	-192(%rbp), %xmm0
	movl	%eax, -164(%rbp)
	psrlq	$32, %xmm1
	movl	%edx, -172(%rbp)
	movdqa	-176(%rbp), %xmm2
	pmuludq	-176(%rbp), %xmm0
	pshufd	$8, %xmm0, %xmm0
	psrlq	$32, %xmm2
	pmuludq	%xmm2, %xmm1
	pshufd	$8, %xmm1, %xmm1
	punpckldq	%xmm1, %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm5
	movaps	%xmm0, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1028:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rdx
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movl	%edx, -192(%rbp)
	sarq	$32, %rdx
	movl	%eax, -184(%rbp)
	sarq	$32, %rax
	movl	%edx, -188(%rbp)
	movq	-143(%rbp), %rdx
	movl	%eax, -180(%rbp)
	movq	-135(%rbp), %rax
	movl	%edx, -176(%rbp)
	sarq	$32, %rdx
	movdqa	-192(%rbp), %xmm0
	movl	%eax, -168(%rbp)
	sarq	$32, %rax
	movl	%eax, -164(%rbp)
	movl	%edx, -172(%rbp)
	psubd	-176(%rbp), %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm6
	movaps	%xmm0, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1029:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rdx
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movl	%edx, -192(%rbp)
	sarq	$32, %rdx
	movl	%eax, -184(%rbp)
	sarq	$32, %rax
	movl	%edx, -188(%rbp)
	movq	-143(%rbp), %rdx
	movl	%eax, -180(%rbp)
	movq	-135(%rbp), %rax
	movl	%edx, -176(%rbp)
	sarq	$32, %rdx
	movl	%eax, -168(%rbp)
	sarq	$32, %rax
	movl	%eax, -164(%rbp)
	movl	%edx, -172(%rbp)
	movdqa	-176(%rbp), %xmm0
	paddd	-192(%rbp), %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm7
	movaps	%xmm0, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1030:
	leaq	-80(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	movl	-79(%rbp), %ebx
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rdx
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	movd	%ebx, %xmm0
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movl	%edx, -160(%rbp)
	sarq	$32, %rdx
	movl	%eax, -152(%rbp)
	sarq	$32, %rax
	movl	%eax, -148(%rbp)
	movl	%edx, -156(%rbp)
	movdqa	-160(%rbp), %xmm5
	psrld	%xmm0, %xmm5
	movups	%xmm5, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm7
	movaps	%xmm5, -144(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1031:
	leaq	-80(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	movslq	-79(%rbp), %rbx
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rdx
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	movq	%rbx, %xmm6
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movl	%edx, -160(%rbp)
	sarq	$32, %rdx
	movl	%eax, -152(%rbp)
	sarq	$32, %rax
	movl	%eax, -148(%rbp)
	movl	%edx, -156(%rbp)
	movdqa	-160(%rbp), %xmm0
	psrad	%xmm6, %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm5
	movaps	%xmm0, -144(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1032:
	leaq	-80(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	movl	-79(%rbp), %ebx
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rdx
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	movd	%ebx, %xmm0
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movl	%edx, -160(%rbp)
	sarq	$32, %rdx
	movl	%eax, -152(%rbp)
	sarq	$32, %rax
	movl	%eax, -148(%rbp)
	movl	%edx, -156(%rbp)
	movdqa	-160(%rbp), %xmm7
	pslld	%xmm0, %xmm7
	movups	%xmm7, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm4
	movaps	%xmm7, -144(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm4, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1033:
	leaq	-80(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-79(%rbp), %rcx
	movq	-71(%rbp), %rdx
	movq	%r12, %rdi
	pxor	%xmm0, %xmm0
	movb	$1, -80(%rbp)
	testl	%ecx, %ecx
	movups	%xmm0, -79(%rbp)
	setne	%al
	sarq	$32, %rcx
	testl	%ecx, %ecx
	setne	%cl
	andl	%ecx, %eax
	testl	%edx, %edx
	setne	%cl
	sarq	$32, %rdx
	andl	%ecx, %eax
	testl	%edx, %edx
	setne	%dl
	subq	$32, %rsp
	movzbl	%dl, %edx
	andl	%edx, %eax
	movl	%eax, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm3
	movb	%al, 16(%rsp)
	movups	%xmm3, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1034:
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rdx
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	movdqa	.LC25(%rip), %xmm0
	pxor	%xmm3, %xmm3
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movl	%edx, -160(%rbp)
	sarq	$32, %rdx
	movl	%eax, -152(%rbp)
	sarq	$32, %rax
	movdqa	%xmm0, %xmm1
	movdqa	%xmm0, %xmm2
	movl	%eax, -148(%rbp)
	movl	%edx, -156(%rbp)
	psubd	-160(%rbp), %xmm3
	pcmpeqd	-160(%rbp), %xmm1
	pand	%xmm1, %xmm2
	pandn	%xmm3, %xmm1
	movdqa	%xmm1, %xmm0
	por	%xmm2, %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm6
	movaps	%xmm0, -144(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1035:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rdx
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	pxor	%xmm1, %xmm1
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movl	%edx, %ecx
	movw	%dx, -192(%rbp)
	sarl	$16, %ecx
	movw	%ax, -184(%rbp)
	movw	%cx, -190(%rbp)
	movq	%rdx, %rcx
	sarq	$48, %rdx
	movw	%dx, -186(%rbp)
	movl	%eax, %edx
	salq	$16, %rcx
	sarl	$16, %edx
	sarq	$48, %rcx
	movw	%dx, -182(%rbp)
	movq	%rax, %rdx
	sarq	$48, %rax
	salq	$16, %rdx
	movw	%cx, -188(%rbp)
	sarq	$48, %rdx
	movw	%ax, -178(%rbp)
	movq	-135(%rbp), %rax
	movw	%dx, -180(%rbp)
	movq	-143(%rbp), %rdx
	movw	%ax, -168(%rbp)
	movdqa	-192(%rbp), %xmm0
	movl	%edx, %ecx
	movw	%dx, -176(%rbp)
	sarl	$16, %ecx
	movw	%cx, -174(%rbp)
	movq	%rdx, %rcx
	sarq	$48, %rdx
	movw	%dx, -170(%rbp)
	movl	%eax, %edx
	salq	$16, %rcx
	sarl	$16, %edx
	sarq	$48, %rcx
	movw	%dx, -166(%rbp)
	movq	%rax, %rdx
	sarq	$48, %rax
	salq	$16, %rdx
	movw	%ax, -162(%rbp)
	sarq	$48, %rdx
	movw	%cx, -172(%rbp)
	movw	%dx, -164(%rbp)
	psubusw	-176(%rbp), %xmm0
	pcmpeqw	%xmm1, %xmm0
	movdqa	-176(%rbp), %xmm1
	pand	%xmm0, %xmm1
	pandn	-192(%rbp), %xmm0
	por	%xmm1, %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm4
	movaps	%xmm0, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm4, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1036:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rdx
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movl	%edx, %ecx
	movw	%dx, -192(%rbp)
	sarl	$16, %ecx
	movw	%ax, -184(%rbp)
	movw	%cx, -190(%rbp)
	movq	%rdx, %rcx
	sarq	$48, %rdx
	movw	%dx, -186(%rbp)
	movl	%eax, %edx
	salq	$16, %rcx
	sarl	$16, %edx
	sarq	$48, %rcx
	movw	%dx, -182(%rbp)
	movq	%rax, %rdx
	sarq	$48, %rax
	salq	$16, %rdx
	movw	%cx, -188(%rbp)
	sarq	$48, %rdx
	movw	%ax, -178(%rbp)
	movq	-135(%rbp), %rax
	movw	%dx, -180(%rbp)
	movq	-143(%rbp), %rdx
	movw	%ax, -168(%rbp)
	movdqa	-192(%rbp), %xmm0
	movl	%edx, %ecx
	movw	%dx, -176(%rbp)
	sarl	$16, %ecx
	movw	%cx, -174(%rbp)
	movq	%rdx, %rcx
	sarq	$48, %rdx
	movw	%dx, -170(%rbp)
	movl	%eax, %edx
	salq	$16, %rcx
	sarl	$16, %edx
	sarq	$48, %rcx
	movw	%dx, -166(%rbp)
	movq	%rax, %rdx
	sarq	$48, %rax
	salq	$16, %rdx
	movw	%ax, -162(%rbp)
	sarq	$48, %rdx
	movw	%cx, -172(%rbp)
	movw	%dx, -164(%rbp)
	pmaxsw	-176(%rbp), %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm7
	movaps	%xmm0, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1037:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rdx
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	pxor	%xmm2, %xmm2
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movl	%edx, %ecx
	movw	%dx, -192(%rbp)
	sarl	$16, %ecx
	movw	%ax, -184(%rbp)
	movw	%cx, -190(%rbp)
	movq	%rdx, %rcx
	sarq	$48, %rdx
	movw	%dx, -186(%rbp)
	movl	%eax, %edx
	salq	$16, %rcx
	sarl	$16, %edx
	sarq	$48, %rcx
	movw	%dx, -182(%rbp)
	movq	%rax, %rdx
	sarq	$48, %rax
	salq	$16, %rdx
	movw	%cx, -188(%rbp)
	sarq	$48, %rdx
	movw	%ax, -178(%rbp)
	movq	-135(%rbp), %rax
	movw	%dx, -180(%rbp)
	movq	-143(%rbp), %rdx
	movw	%ax, -168(%rbp)
	movl	%edx, %ecx
	movw	%dx, -176(%rbp)
	sarl	$16, %ecx
	movw	%cx, -174(%rbp)
	movq	%rdx, %rcx
	sarq	$48, %rdx
	movw	%dx, -170(%rbp)
	movl	%eax, %edx
	salq	$16, %rcx
	sarl	$16, %edx
	sarq	$48, %rcx
	movw	%dx, -166(%rbp)
	movq	%rax, %rdx
	sarq	$48, %rax
	salq	$16, %rdx
	movw	%ax, -162(%rbp)
	sarq	$48, %rdx
	movw	%cx, -172(%rbp)
	movw	%dx, -164(%rbp)
	movdqa	-176(%rbp), %xmm0
	movdqa	%xmm0, %xmm1
	psubusw	-192(%rbp), %xmm1
	pcmpeqw	%xmm2, %xmm1
	movdqa	%xmm0, %xmm2
	pand	%xmm1, %xmm2
	pandn	-192(%rbp), %xmm1
	movdqa	%xmm1, %xmm0
	por	%xmm2, %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm5
	movaps	%xmm0, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1038:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rdx
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movl	%edx, %ecx
	movw	%dx, -192(%rbp)
	sarl	$16, %ecx
	movw	%ax, -184(%rbp)
	movw	%cx, -190(%rbp)
	movq	%rdx, %rcx
	sarq	$48, %rdx
	movw	%dx, -186(%rbp)
	movl	%eax, %edx
	salq	$16, %rcx
	sarl	$16, %edx
	sarq	$48, %rcx
	movw	%dx, -182(%rbp)
	movq	%rax, %rdx
	sarq	$48, %rax
	salq	$16, %rdx
	movw	%cx, -188(%rbp)
	sarq	$48, %rdx
	movw	%ax, -178(%rbp)
	movq	-135(%rbp), %rax
	movw	%dx, -180(%rbp)
	movq	-143(%rbp), %rdx
	movw	%ax, -168(%rbp)
	movdqa	-192(%rbp), %xmm0
	movl	%edx, %ecx
	movw	%dx, -176(%rbp)
	sarl	$16, %ecx
	movw	%cx, -174(%rbp)
	movq	%rdx, %rcx
	sarq	$48, %rdx
	movw	%dx, -170(%rbp)
	movl	%eax, %edx
	salq	$16, %rcx
	sarl	$16, %edx
	sarq	$48, %rcx
	movw	%dx, -166(%rbp)
	movq	%rax, %rdx
	sarq	$48, %rax
	salq	$16, %rdx
	movw	%ax, -162(%rbp)
	sarq	$48, %rdx
	movw	%cx, -172(%rbp)
	movw	%dx, -164(%rbp)
	pminsw	-176(%rbp), %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm6
	movaps	%xmm0, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1039:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rdx
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movl	%edx, %ecx
	movw	%dx, -192(%rbp)
	sarl	$16, %ecx
	movw	%ax, -184(%rbp)
	movw	%cx, -190(%rbp)
	movq	%rdx, %rcx
	sarq	$48, %rdx
	movw	%dx, -186(%rbp)
	movl	%eax, %edx
	salq	$16, %rcx
	sarl	$16, %edx
	sarq	$48, %rcx
	movw	%dx, -182(%rbp)
	movq	%rax, %rdx
	sarq	$48, %rax
	salq	$16, %rdx
	movw	%cx, -188(%rbp)
	sarq	$48, %rdx
	movw	%ax, -178(%rbp)
	movq	-135(%rbp), %rax
	movw	%dx, -180(%rbp)
	movq	-143(%rbp), %rdx
	movw	%ax, -168(%rbp)
	movdqa	-192(%rbp), %xmm0
	movl	%edx, %ecx
	movw	%dx, -176(%rbp)
	sarl	$16, %ecx
	movw	%cx, -174(%rbp)
	movq	%rdx, %rcx
	sarq	$48, %rdx
	movw	%dx, -170(%rbp)
	movl	%eax, %edx
	salq	$16, %rcx
	sarl	$16, %edx
	sarq	$48, %rcx
	movw	%dx, -166(%rbp)
	movq	%rax, %rdx
	sarq	$48, %rax
	salq	$16, %rdx
	movw	%ax, -162(%rbp)
	sarq	$48, %rdx
	movw	%cx, -172(%rbp)
	movw	%dx, -164(%rbp)
	pmullw	-176(%rbp), %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm7
	movaps	%xmm0, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1040:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rdx
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	pxor	%xmm2, %xmm2
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movl	%edx, %ecx
	movw	%dx, -192(%rbp)
	sarl	$16, %ecx
	movw	%ax, -184(%rbp)
	movw	%cx, -190(%rbp)
	movq	%rdx, %rcx
	sarq	$48, %rdx
	movw	%dx, -186(%rbp)
	movl	%eax, %edx
	salq	$16, %rcx
	sarl	$16, %edx
	sarq	$48, %rcx
	movw	%dx, -182(%rbp)
	movq	%rax, %rdx
	sarq	$48, %rax
	salq	$16, %rdx
	movw	%cx, -188(%rbp)
	sarq	$48, %rdx
	movw	%ax, -178(%rbp)
	movq	-135(%rbp), %rax
	movw	%dx, -180(%rbp)
	movq	-143(%rbp), %rdx
	movw	%ax, -168(%rbp)
	movdqa	-192(%rbp), %xmm1
	movl	%edx, %ecx
	movw	%dx, -176(%rbp)
	sarl	$16, %ecx
	movw	%cx, -174(%rbp)
	movq	%rdx, %rcx
	sarq	$48, %rdx
	movw	%dx, -170(%rbp)
	movl	%eax, %edx
	salq	$16, %rcx
	sarl	$16, %edx
	sarq	$48, %rcx
	movw	%dx, -166(%rbp)
	movq	%rax, %rdx
	sarq	$48, %rax
	salq	$16, %rdx
	movw	%ax, -162(%rbp)
	sarq	$48, %rdx
	movw	%cx, -172(%rbp)
	movw	%dx, -164(%rbp)
	movdqa	-176(%rbp), %xmm0
	psubusw	-192(%rbp), %xmm0
	psubw	-176(%rbp), %xmm1
	pcmpeqw	%xmm2, %xmm0
	pand	%xmm1, %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm4
	movaps	%xmm0, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm4, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1041:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rdx
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	pxor	%xmm1, %xmm1
	movdqa	.LC27(%rip), %xmm7
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movdqa	%xmm1, %xmm3
	movdqa	%xmm1, %xmm4
	movdqa	%xmm1, %xmm2
	movl	%edx, %ecx
	sarl	$16, %ecx
	movw	%dx, -192(%rbp)
	movw	%cx, -190(%rbp)
	movq	%rdx, %rcx
	sarq	$48, %rdx
	movw	%dx, -186(%rbp)
	movl	%eax, %edx
	salq	$16, %rcx
	sarl	$16, %edx
	sarq	$48, %rcx
	movw	%ax, -184(%rbp)
	movw	%dx, -182(%rbp)
	movq	%rax, %rdx
	sarq	$48, %rax
	salq	$16, %rdx
	movw	%cx, -188(%rbp)
	sarq	$48, %rdx
	movw	%ax, -178(%rbp)
	movq	-135(%rbp), %rax
	movw	%dx, -180(%rbp)
	movq	-143(%rbp), %rdx
	movdqa	-192(%rbp), %xmm5
	movw	%ax, -168(%rbp)
	movl	%edx, %ecx
	movw	%dx, -176(%rbp)
	pminsw	%xmm5, %xmm3
	pcmpgtw	%xmm5, %xmm2
	sarl	$16, %ecx
	movdqa	%xmm5, %xmm12
	movw	%cx, -174(%rbp)
	movq	%rdx, %rcx
	sarq	$48, %rdx
	pcmpeqw	%xmm1, %xmm3
	movw	%dx, -170(%rbp)
	movl	%eax, %edx
	salq	$16, %rcx
	movdqa	%xmm5, %xmm11
	sarl	$16, %edx
	sarq	$48, %rcx
	punpcklwd	%xmm2, %xmm12
	punpckhwd	%xmm2, %xmm11
	movw	%dx, -166(%rbp)
	movq	%rax, %rdx
	sarq	$48, %rax
	salq	$16, %rdx
	movw	%cx, -172(%rbp)
	sarq	$48, %rdx
	movw	%ax, -162(%rbp)
	movw	%dx, -164(%rbp)
	movdqa	-176(%rbp), %xmm6
	pcmpgtw	%xmm6, %xmm4
	movdqa	%xmm6, %xmm0
	movdqa	%xmm6, %xmm9
	movdqa	%xmm6, %xmm8
	psubw	%xmm6, %xmm5
	pcmpgtw	%xmm1, %xmm8
	movdqa	%xmm12, %xmm1
	punpckhwd	%xmm4, %xmm9
	punpcklwd	%xmm4, %xmm0
	pand	%xmm3, %xmm4
	movdqa	.LC26(%rip), %xmm3
	movdqa	%xmm4, %xmm10
	movdqa	%xmm3, %xmm13
	paddd	%xmm9, %xmm3
	pand	%xmm2, %xmm8
	paddd	%xmm0, %xmm13
	paddd	%xmm7, %xmm0
	pandn	%xmm8, %xmm10
	pcmpgtd	%xmm13, %xmm1
	pcmpgtd	%xmm12, %xmm0
	paddd	%xmm9, %xmm7
	movdqa	%xmm11, %xmm9
	pcmpgtd	%xmm11, %xmm7
	pcmpgtd	%xmm3, %xmm9
	movdqa	%xmm1, %xmm2
	movdqa	%xmm1, %xmm3
	movdqa	%xmm0, %xmm11
	punpckhwd	%xmm7, %xmm11
	punpcklwd	%xmm9, %xmm2
	punpckhwd	%xmm9, %xmm3
	movdqa	%xmm2, %xmm13
	punpcklwd	%xmm3, %xmm2
	punpckhwd	%xmm3, %xmm13
	movdqa	%xmm0, %xmm3
	punpcklwd	%xmm7, %xmm3
	punpcklwd	%xmm13, %xmm2
	movdqa	%xmm3, %xmm12
	punpcklwd	%xmm11, %xmm3
	pand	%xmm4, %xmm2
	punpckhwd	%xmm11, %xmm12
	pxor	%xmm11, %xmm11
	pcmpeqd	%xmm11, %xmm9
	pcmpeqd	%xmm11, %xmm1
	punpcklwd	%xmm12, %xmm3
	pcmpeqd	%xmm11, %xmm7
	pcmpeqd	%xmm11, %xmm0
	pand	%xmm10, %xmm3
	movdqa	%xmm1, %xmm12
	punpcklwd	%xmm9, %xmm1
	punpckhwd	%xmm9, %xmm12
	movdqa	%xmm1, %xmm9
	punpckhwd	%xmm12, %xmm9
	punpcklwd	%xmm12, %xmm1
	punpcklwd	%xmm9, %xmm1
	movdqa	%xmm0, %xmm9
	punpcklwd	%xmm7, %xmm0
	punpckhwd	%xmm7, %xmm9
	movdqa	%xmm0, %xmm7
	pand	%xmm4, %xmm1
	punpckhwd	%xmm9, %xmm7
	punpcklwd	%xmm9, %xmm0
	por	%xmm8, %xmm4
	punpcklwd	%xmm7, %xmm0
	pand	%xmm10, %xmm0
	por	%xmm1, %xmm0
	pcmpeqd	%xmm1, %xmm1
	pxor	%xmm1, %xmm4
	movdqa	%xmm5, %xmm1
	por	%xmm4, %xmm0
	movdqa	.LC28(%rip), %xmm4
	pand	%xmm0, %xmm1
	pandn	%xmm4, %xmm0
	pand	%xmm3, %xmm4
	por	%xmm1, %xmm0
	pandn	%xmm0, %xmm3
	movdqa	.LC29(%rip), %xmm0
	por	%xmm4, %xmm3
	pand	%xmm2, %xmm0
	pandn	%xmm3, %xmm2
	por	%xmm0, %xmm2
	movups	%xmm2, -79(%rbp)
	movdqa	-80(%rbp), %xmm7
	movzbl	-64(%rbp), %eax
	movaps	%xmm2, -160(%rbp)
	movups	%xmm7, (%rsp)
	movb	%al, 16(%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1042:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rdx
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movl	%edx, %ecx
	movw	%dx, -192(%rbp)
	sarl	$16, %ecx
	movw	%ax, -184(%rbp)
	movw	%cx, -190(%rbp)
	movq	%rdx, %rcx
	sarq	$48, %rdx
	movw	%dx, -186(%rbp)
	movl	%eax, %edx
	salq	$16, %rcx
	sarl	$16, %edx
	sarq	$48, %rcx
	movw	%dx, -182(%rbp)
	movq	%rax, %rdx
	sarq	$48, %rax
	salq	$16, %rdx
	movw	%cx, -188(%rbp)
	sarq	$48, %rdx
	movw	%ax, -178(%rbp)
	movq	-135(%rbp), %rax
	movw	%dx, -180(%rbp)
	movq	-143(%rbp), %rdx
	movw	%ax, -168(%rbp)
	movdqa	-192(%rbp), %xmm0
	movl	%edx, %ecx
	movw	%dx, -176(%rbp)
	sarl	$16, %ecx
	movw	%cx, -174(%rbp)
	movq	%rdx, %rcx
	sarq	$48, %rdx
	movw	%dx, -170(%rbp)
	movl	%eax, %edx
	salq	$16, %rcx
	sarl	$16, %edx
	sarq	$48, %rcx
	movw	%dx, -166(%rbp)
	movq	%rax, %rdx
	sarq	$48, %rax
	salq	$16, %rdx
	movw	%ax, -162(%rbp)
	sarq	$48, %rdx
	movw	%cx, -172(%rbp)
	movw	%dx, -164(%rbp)
	psubw	-176(%rbp), %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm5
	movaps	%xmm0, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1043:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rdx
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	movdqa	.LC30(%rip), %xmm3
	pxor	%xmm4, %xmm4
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movl	%edx, %ecx
	movw	%dx, -192(%rbp)
	sarl	$16, %ecx
	movw	%ax, -184(%rbp)
	movdqa	%xmm3, %xmm6
	movw	%cx, -190(%rbp)
	movq	%rdx, %rcx
	sarq	$48, %rdx
	movw	%dx, -186(%rbp)
	movl	%eax, %edx
	salq	$16, %rcx
	sarl	$16, %edx
	sarq	$48, %rcx
	movw	%dx, -182(%rbp)
	movq	%rax, %rdx
	sarq	$48, %rax
	salq	$16, %rdx
	movw	%cx, -188(%rbp)
	sarq	$48, %rdx
	movw	%ax, -178(%rbp)
	movq	-135(%rbp), %rax
	movw	%dx, -180(%rbp)
	movq	-143(%rbp), %rdx
	movw	%ax, -168(%rbp)
	movdqa	-192(%rbp), %xmm1
	movl	%edx, %ecx
	movw	%dx, -176(%rbp)
	sarl	$16, %ecx
	movdqa	%xmm1, %xmm0
	movw	%cx, -174(%rbp)
	movq	%rdx, %rcx
	sarq	$48, %rdx
	punpcklwd	%xmm4, %xmm0
	movw	%dx, -170(%rbp)
	movl	%eax, %edx
	salq	$16, %rcx
	sarl	$16, %edx
	sarq	$48, %rcx
	movw	%dx, -166(%rbp)
	movq	%rax, %rdx
	sarq	$48, %rax
	salq	$16, %rdx
	movw	%ax, -162(%rbp)
	sarq	$48, %rdx
	movw	%cx, -172(%rbp)
	movw	%dx, -164(%rbp)
	movdqa	-176(%rbp), %xmm5
	movdqa	%xmm5, %xmm2
	movdqa	%xmm5, %xmm7
	punpcklwd	%xmm4, %xmm2
	punpckhwd	%xmm4, %xmm7
	psubd	%xmm2, %xmm6
	movdqa	%xmm1, %xmm2
	psubd	%xmm7, %xmm3
	punpckhwd	%xmm4, %xmm2
	pcmpgtd	%xmm6, %xmm0
	pxor	%xmm6, %xmm6
	pcmpgtd	%xmm3, %xmm2
	paddw	%xmm5, %xmm1
	pcmpeqd	%xmm6, %xmm0
	pcmpeqd	%xmm6, %xmm2
	movdqa	%xmm0, %xmm3
	punpcklwd	%xmm2, %xmm0
	punpckhwd	%xmm2, %xmm3
	movdqa	%xmm0, %xmm2
	punpcklwd	%xmm3, %xmm0
	punpckhwd	%xmm3, %xmm2
	punpcklwd	%xmm2, %xmm0
	pcmpeqd	%xmm2, %xmm2
	pand	%xmm0, %xmm1
	pandn	%xmm2, %xmm0
	por	%xmm1, %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm5
	movaps	%xmm0, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1044:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rdx
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	pxor	%xmm0, %xmm0
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movl	%edx, %ecx
	movw	%dx, -192(%rbp)
	movdqa	%xmm0, %xmm8
	movdqa	%xmm0, %xmm1
	sarl	$16, %ecx
	movw	%ax, -184(%rbp)
	movw	%cx, -190(%rbp)
	movq	%rdx, %rcx
	sarq	$48, %rdx
	movw	%dx, -186(%rbp)
	movl	%eax, %edx
	salq	$16, %rcx
	sarl	$16, %edx
	sarq	$48, %rcx
	movw	%dx, -182(%rbp)
	movq	%rax, %rdx
	sarq	$48, %rax
	salq	$16, %rdx
	movw	%cx, -188(%rbp)
	sarq	$48, %rdx
	movw	%ax, -178(%rbp)
	movq	-135(%rbp), %rax
	movw	%dx, -180(%rbp)
	movq	-143(%rbp), %rdx
	movw	%ax, -168(%rbp)
	movdqa	-192(%rbp), %xmm6
	movl	%edx, %ecx
	movw	%dx, -176(%rbp)
	movdqa	%xmm6, %xmm2
	sarl	$16, %ecx
	pcmpgtw	%xmm6, %xmm1
	movdqa	%xmm6, %xmm12
	movw	%cx, -174(%rbp)
	movq	%rdx, %rcx
	sarq	$48, %rdx
	pcmpgtw	%xmm0, %xmm2
	movw	%dx, -170(%rbp)
	movl	%eax, %edx
	salq	$16, %rcx
	movdqa	%xmm6, %xmm4
	sarl	$16, %edx
	sarq	$48, %rcx
	punpcklwd	%xmm1, %xmm12
	punpckhwd	%xmm1, %xmm4
	movw	%dx, -166(%rbp)
	movq	%rax, %rdx
	sarq	$48, %rax
	salq	$16, %rdx
	movw	%cx, -172(%rbp)
	sarq	$48, %rdx
	movw	%ax, -162(%rbp)
	movw	%dx, -164(%rbp)
	movdqa	-176(%rbp), %xmm5
	movdqa	%xmm5, %xmm7
	pcmpgtw	%xmm5, %xmm8
	movdqa	%xmm5, %xmm10
	pcmpgtw	%xmm0, %xmm7
	movdqa	%xmm5, %xmm3
	paddw	%xmm6, %xmm5
	punpcklwd	%xmm8, %xmm10
	punpckhwd	%xmm8, %xmm3
	pand	%xmm1, %xmm8
	pand	%xmm7, %xmm2
	movdqa	.LC27(%rip), %xmm7
	movdqa	%xmm12, %xmm1
	movdqa	%xmm2, %xmm9
	movdqa	.LC26(%rip), %xmm2
	movdqa	%xmm7, %xmm0
	psubd	%xmm3, %xmm7
	movdqa	%xmm9, %xmm11
	movdqa	%xmm2, %xmm13
	psubd	%xmm10, %xmm0
	psubd	%xmm3, %xmm2
	psubd	%xmm10, %xmm13
	movdqa	%xmm4, %xmm10
	pandn	%xmm8, %xmm11
	pcmpgtd	%xmm13, %xmm1
	pcmpgtd	%xmm2, %xmm10
	pcmpgtd	%xmm4, %xmm7
	movdqa	%xmm1, %xmm3
	movdqa	%xmm1, %xmm13
	punpcklwd	%xmm10, %xmm3
	punpckhwd	%xmm10, %xmm13
	movdqa	%xmm3, %xmm2
	punpcklwd	%xmm13, %xmm3
	punpckhwd	%xmm13, %xmm2
	punpcklwd	%xmm2, %xmm3
	movdqa	%xmm0, %xmm2
	pcmpgtd	%xmm12, %xmm2
	pand	%xmm9, %xmm3
	movdqa	%xmm2, %xmm4
	movdqa	%xmm2, %xmm0
	punpcklwd	%xmm7, %xmm4
	punpckhwd	%xmm7, %xmm0
	movdqa	%xmm4, %xmm12
	punpcklwd	%xmm0, %xmm4
	punpckhwd	%xmm0, %xmm12
	punpcklwd	%xmm12, %xmm4
	pxor	%xmm12, %xmm12
	pcmpeqd	%xmm12, %xmm10
	pcmpeqd	%xmm12, %xmm1
	pand	%xmm11, %xmm4
	pcmpeqd	%xmm12, %xmm2
	pcmpeqd	%xmm12, %xmm7
	movdqa	%xmm1, %xmm0
	punpcklwd	%xmm10, %xmm1
	punpckhwd	%xmm10, %xmm0
	movdqa	%xmm1, %xmm10
	punpckhwd	%xmm0, %xmm10
	punpcklwd	%xmm0, %xmm1
	movdqa	%xmm2, %xmm0
	punpcklwd	%xmm7, %xmm0
	punpckhwd	%xmm7, %xmm2
	punpcklwd	%xmm10, %xmm1
	movdqa	%xmm0, %xmm7
	punpcklwd	%xmm2, %xmm0
	pand	%xmm9, %xmm1
	punpckhwd	%xmm2, %xmm7
	movdqa	%xmm8, %xmm2
	punpcklwd	%xmm7, %xmm0
	por	%xmm9, %xmm2
	pand	%xmm11, %xmm0
	por	%xmm1, %xmm0
	pcmpeqd	%xmm1, %xmm1
	pxor	%xmm1, %xmm2
	movdqa	%xmm5, %xmm1
	por	%xmm2, %xmm0
	movdqa	.LC28(%rip), %xmm2
	pand	%xmm0, %xmm1
	pandn	%xmm2, %xmm0
	pand	%xmm4, %xmm2
	por	%xmm1, %xmm0
	movdqa	.LC29(%rip), %xmm1
	pandn	%xmm0, %xmm4
	por	%xmm2, %xmm4
	pand	%xmm3, %xmm1
	pandn	%xmm4, %xmm3
	movdqa	%xmm3, %xmm0
	por	%xmm1, %xmm0
	movups	%xmm0, -79(%rbp)
	movdqa	-80(%rbp), %xmm6
	movzbl	-64(%rbp), %eax
	movaps	%xmm0, -160(%rbp)
	movups	%xmm6, (%rsp)
	movb	%al, 16(%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1045:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rdx
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movl	%edx, %ecx
	movw	%dx, -192(%rbp)
	sarl	$16, %ecx
	movw	%ax, -184(%rbp)
	movw	%cx, -190(%rbp)
	movq	%rdx, %rcx
	sarq	$48, %rdx
	movw	%dx, -186(%rbp)
	movl	%eax, %edx
	salq	$16, %rcx
	sarl	$16, %edx
	sarq	$48, %rcx
	movw	%dx, -182(%rbp)
	movq	%rax, %rdx
	sarq	$48, %rax
	salq	$16, %rdx
	movw	%cx, -188(%rbp)
	sarq	$48, %rdx
	movw	%ax, -178(%rbp)
	movq	-135(%rbp), %rax
	movw	%dx, -180(%rbp)
	movq	-143(%rbp), %rdx
	movw	%ax, -168(%rbp)
	movdqa	-192(%rbp), %xmm0
	movl	%edx, %ecx
	movw	%dx, -176(%rbp)
	sarl	$16, %ecx
	movw	%cx, -174(%rbp)
	movq	%rdx, %rcx
	sarq	$48, %rdx
	movw	%dx, -170(%rbp)
	movl	%eax, %edx
	salq	$16, %rcx
	sarl	$16, %edx
	sarq	$48, %rcx
	movw	%dx, -166(%rbp)
	movq	%rax, %rdx
	sarq	$48, %rax
	salq	$16, %rdx
	movw	%ax, -162(%rbp)
	sarq	$48, %rdx
	movw	%cx, -172(%rbp)
	movw	%dx, -164(%rbp)
	paddw	-176(%rbp), %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm6
	movaps	%xmm0, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1046:
	leaq	-80(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	movl	-79(%rbp), %ebx
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rdx
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	pxor	%xmm3, %xmm3
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movl	%edx, %ecx
	movw	%dx, -160(%rbp)
	sarl	$16, %ecx
	movw	%ax, -152(%rbp)
	movw	%cx, -158(%rbp)
	movq	%rdx, %rcx
	sarq	$48, %rdx
	movw	%dx, -154(%rbp)
	movl	%eax, %edx
	salq	$16, %rcx
	sarl	$16, %edx
	sarq	$48, %rcx
	movw	%dx, -150(%rbp)
	movq	%rax, %rdx
	sarq	$48, %rax
	salq	$16, %rdx
	movw	%ax, -146(%rbp)
	movslq	%ebx, %rax
	sarq	$48, %rdx
	movw	%cx, -156(%rbp)
	movq	%rax, %xmm2
	movw	%dx, -148(%rbp)
	movdqa	-160(%rbp), %xmm1
	movdqa	%xmm1, %xmm0
	punpckhwd	%xmm3, %xmm1
	punpcklwd	%xmm3, %xmm0
	psrad	%xmm2, %xmm1
	psrad	%xmm2, %xmm0
	movdqa	%xmm0, %xmm2
	punpcklwd	%xmm1, %xmm0
	punpckhwd	%xmm1, %xmm2
	movdqa	%xmm0, %xmm1
	punpckhwd	%xmm2, %xmm1
	punpcklwd	%xmm2, %xmm0
	punpcklwd	%xmm1, %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm6
	movaps	%xmm0, -144(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1047:
	leaq	-80(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	movl	-79(%rbp), %ebx
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rdx
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	pxor	%xmm3, %xmm3
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movl	%edx, %ecx
	movw	%dx, -160(%rbp)
	sarl	$16, %ecx
	movw	%ax, -152(%rbp)
	movw	%cx, -158(%rbp)
	movq	%rdx, %rcx
	sarq	$48, %rdx
	movw	%dx, -154(%rbp)
	movl	%eax, %edx
	salq	$16, %rcx
	sarl	$16, %edx
	sarq	$48, %rcx
	movw	%dx, -150(%rbp)
	movq	%rax, %rdx
	sarq	$48, %rax
	salq	$16, %rdx
	movw	%ax, -146(%rbp)
	movslq	%ebx, %rax
	sarq	$48, %rdx
	movw	%cx, -156(%rbp)
	movq	%rax, %xmm2
	movw	%dx, -148(%rbp)
	movdqa	-160(%rbp), %xmm1
	pcmpgtw	%xmm1, %xmm3
	movdqa	%xmm1, %xmm0
	punpcklwd	%xmm3, %xmm0
	punpckhwd	%xmm3, %xmm1
	psrad	%xmm2, %xmm1
	psrad	%xmm2, %xmm0
	movdqa	%xmm0, %xmm2
	punpcklwd	%xmm1, %xmm0
	punpckhwd	%xmm1, %xmm2
	movdqa	%xmm0, %xmm1
	punpckhwd	%xmm2, %xmm1
	punpcklwd	%xmm2, %xmm0
	punpcklwd	%xmm1, %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm4
	movaps	%xmm0, -144(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm4, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1048:
	leaq	-80(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	movl	-79(%rbp), %ebx
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rdx
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	pxor	%xmm3, %xmm3
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movl	%edx, %ecx
	movw	%dx, -160(%rbp)
	sarl	$16, %ecx
	movw	%ax, -152(%rbp)
	movw	%cx, -158(%rbp)
	movq	%rdx, %rcx
	sarq	$48, %rdx
	movw	%dx, -154(%rbp)
	movl	%eax, %edx
	salq	$16, %rcx
	sarl	$16, %edx
	sarq	$48, %rcx
	movw	%dx, -150(%rbp)
	movq	%rax, %rdx
	sarq	$48, %rax
	salq	$16, %rdx
	movw	%ax, -146(%rbp)
	movslq	%ebx, %rax
	sarq	$48, %rdx
	movw	%cx, -156(%rbp)
	movq	%rax, %xmm2
	movw	%dx, -148(%rbp)
	movdqa	-160(%rbp), %xmm1
	movdqa	%xmm1, %xmm0
	punpckhwd	%xmm3, %xmm1
	punpcklwd	%xmm3, %xmm0
	pslld	%xmm2, %xmm1
	pslld	%xmm2, %xmm0
	movdqa	%xmm0, %xmm2
	punpcklwd	%xmm1, %xmm0
	punpckhwd	%xmm1, %xmm2
	movdqa	%xmm0, %xmm1
	punpckhwd	%xmm2, %xmm1
	punpcklwd	%xmm2, %xmm0
	punpcklwd	%xmm1, %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm3
	movaps	%xmm0, -144(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm3, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1049:
	leaq	-80(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-79(%rbp), %rcx
	movq	-71(%rbp), %rdx
	movq	%r12, %rdi
	pxor	%xmm0, %xmm0
	movb	$1, -80(%rbp)
	testw	%cx, %cx
	movl	%ecx, %esi
	movups	%xmm0, -79(%rbp)
	setne	%al
	sarl	$16, %esi
	testw	%si, %si
	setne	%sil
	andl	%esi, %eax
	movq	%rcx, %rsi
	salq	$16, %rsi
	sarq	$48, %rsi
	testw	%si, %si
	setne	%sil
	sarq	$48, %rcx
	andl	%esi, %eax
	testw	%cx, %cx
	setne	%cl
	andl	%ecx, %eax
	testw	%dx, %dx
	setne	%cl
	andl	%ecx, %eax
	movl	%edx, %ecx
	sarl	$16, %ecx
	testw	%cx, %cx
	setne	%cl
	andl	%ecx, %eax
	movq	%rdx, %rcx
	salq	$16, %rcx
	sarq	$48, %rcx
	testw	%cx, %cx
	setne	%cl
	sarq	$48, %rdx
	andl	%ecx, %eax
	testw	%dx, %dx
	setne	%dl
	subq	$32, %rsp
	movzbl	%dl, %edx
	andl	%edx, %eax
	movl	%eax, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm4
	movb	%al, 16(%rsp)
	movups	%xmm4, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1081:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rax
	movq	-103(%rbp), %rdx
	movq	%r12, %rdi
	movq	-143(%rbp), %rcx
	movq	-135(%rbp), %rsi
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movd	%edx, %xmm2
	shrq	$32, %rdx
	movd	%ecx, %xmm1
	movd	%esi, %xmm0
	shrq	$32, %rcx
	movd	%edx, %xmm5
	shrq	$32, %rsi
	movd	%ecx, %xmm6
	unpcklps	%xmm5, %xmm2
	movd	%esi, %xmm4
	unpcklps	%xmm6, %xmm1
	unpcklps	%xmm4, %xmm0
	movlhps	%xmm0, %xmm1
	movd	%eax, %xmm0
	shrq	$32, %rax
	movd	%eax, %xmm7
	unpcklps	%xmm7, %xmm0
	movlhps	%xmm2, %xmm0
	cmpltps	%xmm1, %xmm0
	pand	.LC42(%rip), %xmm0
	pxor	%xmm1, %xmm1
	psubd	%xmm0, %xmm1
	movups	%xmm1, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm4
	movaps	%xmm1, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm4, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1082:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rax
	movq	-103(%rbp), %rdx
	movq	%r12, %rdi
	movq	-143(%rbp), %rcx
	movq	-135(%rbp), %rsi
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movd	%edx, %xmm2
	shrq	$32, %rdx
	movd	%ecx, %xmm0
	movd	%esi, %xmm1
	shrq	$32, %rcx
	movd	%edx, %xmm7
	shrq	$32, %rsi
	movd	%ecx, %xmm5
	unpcklps	%xmm7, %xmm2
	movd	%esi, %xmm6
	unpcklps	%xmm5, %xmm0
	unpcklps	%xmm6, %xmm1
	movlhps	%xmm1, %xmm0
	movd	%eax, %xmm1
	shrq	$32, %rax
	movd	%eax, %xmm4
	unpcklps	%xmm4, %xmm1
	movlhps	%xmm2, %xmm1
	cmpneqps	%xmm1, %xmm0
	pand	.LC42(%rip), %xmm0
	pxor	%xmm1, %xmm1
	psubd	%xmm0, %xmm1
	movups	%xmm1, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm6
	movaps	%xmm1, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1083:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rcx
	movq	-103(%rbp), %rsi
	movq	%r12, %rdi
	movq	-143(%rbp), %rax
	movq	-135(%rbp), %rdx
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movd	%ecx, %xmm0
	movd	%esi, %xmm1
	shrq	$32, %rcx
	shrq	$32, %rsi
	movd	%ecx, %xmm6
	movd	%edx, %xmm2
	shrq	$32, %rdx
	movd	%esi, %xmm4
	unpcklps	%xmm6, %xmm0
	movd	%edx, %xmm5
	unpcklps	%xmm4, %xmm1
	unpcklps	%xmm5, %xmm2
	movlhps	%xmm1, %xmm0
	movd	%eax, %xmm1
	shrq	$32, %rax
	movd	%eax, %xmm7
	unpcklps	%xmm7, %xmm1
	movlhps	%xmm2, %xmm1
	cmpeqps	%xmm1, %xmm0
	pand	.LC42(%rip), %xmm0
	pxor	%xmm1, %xmm1
	psubd	%xmm0, %xmm1
	movups	%xmm1, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm4
	movaps	%xmm1, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm4, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1084:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	xorl	%edx, %edx
	movb	$5, -80(%rbp)
	movq	%r12, %rdi
	movq	-143(%rbp), %rax
	cmpq	%rax, -111(%rbp)
	setnb	%dl
	movq	-135(%rbp), %rax
	negq	%rdx
	cmpq	%rax, -103(%rbp)
	setnb	%al
	movq	%rdx, -79(%rbp)
	subq	$32, %rsp
	movzbl	%al, %eax
	negq	%rax
	movq	%rax, -71(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm6
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1085:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rax
	xorl	%edx, %edx
	movb	$5, -80(%rbp)
	cmpq	%rax, -143(%rbp)
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	setle	%dl
	negq	%rdx
	cmpq	%rax, -135(%rbp)
	setle	%al
	movq	%rdx, -79(%rbp)
	subq	$32, %rsp
	movzbl	%al, %eax
	negq	%rax
	movq	%rax, -71(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm6
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1086:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	xorl	%edx, %edx
	movb	$5, -80(%rbp)
	movq	%r12, %rdi
	movq	-143(%rbp), %rax
	cmpq	%rax, -111(%rbp)
	setbe	%dl
	movq	-135(%rbp), %rax
	negq	%rdx
	cmpq	%rax, -103(%rbp)
	setbe	%al
	movq	%rdx, -79(%rbp)
	subq	$32, %rsp
	movzbl	%al, %eax
	negq	%rax
	movq	%rax, -71(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm7
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1087:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rax
	xorl	%edx, %edx
	movb	$5, -80(%rbp)
	cmpq	%rax, -143(%rbp)
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	setge	%dl
	negq	%rdx
	cmpq	%rax, -135(%rbp)
	setge	%al
	movq	%rdx, -79(%rbp)
	subq	$32, %rsp
	movzbl	%al, %eax
	negq	%rax
	movq	%rax, -71(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm7
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1088:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	xorl	%edx, %edx
	movb	$5, -80(%rbp)
	movq	%r12, %rdi
	movq	-143(%rbp), %rax
	cmpq	%rax, -111(%rbp)
	seta	%dl
	movq	-135(%rbp), %rax
	negq	%rdx
	cmpq	%rax, -103(%rbp)
	seta	%al
	movq	%rdx, -79(%rbp)
	subq	$32, %rsp
	movzbl	%al, %eax
	negq	%rax
	movq	%rax, -71(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm4
	movb	%al, 16(%rsp)
	movups	%xmm4, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1089:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rax
	xorl	%edx, %edx
	movb	$5, -80(%rbp)
	cmpq	%rax, -143(%rbp)
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	setl	%dl
	negq	%rdx
	cmpq	%rax, -135(%rbp)
	setl	%al
	movq	%rdx, -79(%rbp)
	subq	$32, %rsp
	movzbl	%al, %eax
	negq	%rax
	movq	%rax, -71(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm4
	movb	%al, 16(%rsp)
	movups	%xmm4, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1090:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-143(%rbp), %rax
	cmpq	%rax, -111(%rbp)
	movb	$5, -80(%rbp)
	movq	-135(%rbp), %rax
	sbbq	%rdx, %rdx
	cmpq	%rax, -103(%rbp)
	movq	%r12, %rdi
	sbbq	%rax, %rax
	movq	%rdx, -79(%rbp)
	subq	$32, %rsp
	movq	%rax, -71(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm5
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1091:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rax
	xorl	%edx, %edx
	movb	$5, -80(%rbp)
	cmpq	%rax, -143(%rbp)
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	setg	%dl
	negq	%rdx
	cmpq	%rax, -135(%rbp)
	setg	%al
	movq	%rdx, -79(%rbp)
	subq	$32, %rsp
	movzbl	%al, %eax
	negq	%rax
	movq	%rax, -71(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm5
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1092:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rax
	xorl	%edx, %edx
	movb	$5, -80(%rbp)
	cmpq	%rax, -143(%rbp)
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	setne	%dl
	negq	%rdx
	cmpq	%rax, -135(%rbp)
	setne	%al
	movq	%rdx, -79(%rbp)
	subq	$32, %rsp
	movzbl	%al, %eax
	negq	%rax
	movq	%rax, -71(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm7
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1093:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rax
	xorl	%edx, %edx
	movb	$5, -80(%rbp)
	cmpq	%rax, -143(%rbp)
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	sete	%dl
	negq	%rdx
	cmpq	%rax, -135(%rbp)
	sete	%al
	movq	%rdx, -79(%rbp)
	subq	$32, %rsp
	movzbl	%al, %eax
	negq	%rax
	movq	%rax, -71(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm5
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1094:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rdx
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	movdqa	.LC25(%rip), %xmm1
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movl	%edx, -192(%rbp)
	sarq	$32, %rdx
	movl	%eax, -184(%rbp)
	sarq	$32, %rax
	movl	%edx, -188(%rbp)
	movq	-143(%rbp), %rdx
	movl	%eax, -180(%rbp)
	movq	-135(%rbp), %rax
	movl	%edx, -176(%rbp)
	sarq	$32, %rdx
	movdqa	-192(%rbp), %xmm5
	movl	%eax, -168(%rbp)
	sarq	$32, %rax
	movl	%eax, -164(%rbp)
	psubd	%xmm1, %xmm5
	movl	%edx, -172(%rbp)
	movdqa	-176(%rbp), %xmm0
	psubd	%xmm1, %xmm0
	pxor	%xmm1, %xmm1
	pcmpgtd	%xmm5, %xmm0
	pandn	.LC42(%rip), %xmm0
	psubd	%xmm0, %xmm1
	movups	%xmm1, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm7
	movaps	%xmm1, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1095:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rdx
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	pxor	%xmm1, %xmm1
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movl	%edx, -192(%rbp)
	sarq	$32, %rdx
	movl	%eax, -184(%rbp)
	sarq	$32, %rax
	movl	%edx, -188(%rbp)
	movq	-143(%rbp), %rdx
	movl	%eax, -180(%rbp)
	movq	-135(%rbp), %rax
	movl	%edx, -176(%rbp)
	sarq	$32, %rdx
	movl	%eax, -168(%rbp)
	sarq	$32, %rax
	movl	%eax, -164(%rbp)
	movl	%edx, -172(%rbp)
	movdqa	-176(%rbp), %xmm0
	pcmpgtd	-192(%rbp), %xmm0
	pandn	.LC42(%rip), %xmm0
	psubd	%xmm0, %xmm1
	movups	%xmm1, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm7
	movaps	%xmm1, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1096:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rdx
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	movdqa	.LC25(%rip), %xmm1
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movl	%edx, -192(%rbp)
	sarq	$32, %rdx
	movl	%eax, -184(%rbp)
	sarq	$32, %rax
	movl	%edx, -188(%rbp)
	movq	-143(%rbp), %rdx
	movl	%eax, -180(%rbp)
	movq	-135(%rbp), %rax
	movl	%edx, -176(%rbp)
	sarq	$32, %rdx
	movdqa	-192(%rbp), %xmm0
	movl	%eax, -168(%rbp)
	sarq	$32, %rax
	movl	%eax, -164(%rbp)
	psubd	%xmm1, %xmm0
	movl	%edx, -172(%rbp)
	movdqa	-176(%rbp), %xmm5
	psubd	%xmm1, %xmm5
	pxor	%xmm1, %xmm1
	pcmpgtd	%xmm5, %xmm0
	pandn	.LC42(%rip), %xmm0
	psubd	%xmm0, %xmm1
	movups	%xmm1, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm6
	movaps	%xmm1, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1097:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rdx
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	pxor	%xmm1, %xmm1
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movl	%edx, -192(%rbp)
	sarq	$32, %rdx
	movl	%eax, -184(%rbp)
	sarq	$32, %rax
	movl	%edx, -188(%rbp)
	movq	-143(%rbp), %rdx
	movl	%eax, -180(%rbp)
	movq	-135(%rbp), %rax
	movl	%edx, -176(%rbp)
	sarq	$32, %rdx
	movdqa	-192(%rbp), %xmm0
	movl	%eax, -168(%rbp)
	sarq	$32, %rax
	movl	%eax, -164(%rbp)
	movl	%edx, -172(%rbp)
	pcmpgtd	-176(%rbp), %xmm0
	pandn	.LC42(%rip), %xmm0
	psubd	%xmm0, %xmm1
	movups	%xmm1, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm6
	movaps	%xmm1, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1098:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rdx
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	movdqa	.LC25(%rip), %xmm1
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movl	%edx, -192(%rbp)
	sarq	$32, %rdx
	movl	%eax, -184(%rbp)
	sarq	$32, %rax
	movl	%edx, -188(%rbp)
	movq	-143(%rbp), %rdx
	movl	%eax, -180(%rbp)
	movq	-135(%rbp), %rax
	movl	%edx, -176(%rbp)
	sarq	$32, %rdx
	movdqa	-192(%rbp), %xmm0
	movl	%eax, -168(%rbp)
	sarq	$32, %rax
	movl	%eax, -164(%rbp)
	psubd	%xmm1, %xmm0
	movl	%edx, -172(%rbp)
	movdqa	-176(%rbp), %xmm7
	psubd	%xmm1, %xmm7
	pxor	%xmm1, %xmm1
	pcmpgtd	%xmm7, %xmm0
	pand	.LC42(%rip), %xmm0
	psubd	%xmm0, %xmm1
	movups	%xmm1, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm5
	movaps	%xmm1, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1099:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rdx
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	pxor	%xmm1, %xmm1
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movl	%edx, -192(%rbp)
	sarq	$32, %rdx
	movl	%eax, -184(%rbp)
	sarq	$32, %rax
	movl	%edx, -188(%rbp)
	movq	-143(%rbp), %rdx
	movl	%eax, -180(%rbp)
	movq	-135(%rbp), %rax
	movl	%edx, -176(%rbp)
	sarq	$32, %rdx
	movdqa	-192(%rbp), %xmm0
	movl	%eax, -168(%rbp)
	sarq	$32, %rax
	movl	%eax, -164(%rbp)
	movl	%edx, -172(%rbp)
	pcmpgtd	-176(%rbp), %xmm0
	pand	.LC42(%rip), %xmm0
	psubd	%xmm0, %xmm1
	movups	%xmm1, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm5
	movaps	%xmm1, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1100:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rdx
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	movdqa	.LC25(%rip), %xmm1
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movl	%edx, -192(%rbp)
	sarq	$32, %rdx
	movl	%eax, -184(%rbp)
	sarq	$32, %rax
	movl	%edx, -188(%rbp)
	movq	-143(%rbp), %rdx
	movl	%eax, -180(%rbp)
	movq	-135(%rbp), %rax
	movl	%edx, -176(%rbp)
	sarq	$32, %rdx
	movdqa	-192(%rbp), %xmm7
	movl	%eax, -168(%rbp)
	sarq	$32, %rax
	movl	%eax, -164(%rbp)
	psubd	%xmm1, %xmm7
	movl	%edx, -172(%rbp)
	movdqa	-176(%rbp), %xmm0
	psubd	%xmm1, %xmm0
	pxor	%xmm1, %xmm1
	pcmpgtd	%xmm7, %xmm0
	pand	.LC42(%rip), %xmm0
	psubd	%xmm0, %xmm1
	movups	%xmm1, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm4
	movaps	%xmm1, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm4, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1005:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rax
	movq	-103(%rbp), %rdx
	movq	%r12, %rdi
	movq	-143(%rbp), %rcx
	movq	-135(%rbp), %rsi
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movd	%edx, %xmm2
	shrq	$32, %rdx
	movd	%ecx, %xmm0
	movd	%esi, %xmm1
	shrq	$32, %rcx
	movd	%edx, %xmm5
	shrq	$32, %rsi
	movd	%ecx, %xmm6
	unpcklps	%xmm5, %xmm2
	movd	%esi, %xmm7
	unpcklps	%xmm6, %xmm0
	unpcklps	%xmm7, %xmm1
	movlhps	%xmm1, %xmm0
	movd	%eax, %xmm1
	shrq	$32, %rax
	movd	%eax, %xmm7
	unpcklps	%xmm7, %xmm1
	movlhps	%xmm2, %xmm1
	addps	%xmm1, %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm6
	movaps	%xmm0, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1006:
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rax
	movq	-103(%rbp), %rdx
	movd	%eax, %xmm1
	shrq	$32, %rax
	movd	%edx, %xmm0
	movd	%eax, %xmm6
	shrq	$32, %rdx
	unpcklps	%xmm6, %xmm1
	movd	%edx, %xmm4
	unpcklps	%xmm4, %xmm0
	movaps	%xmm1, %xmm7
	pxor	%xmm1, %xmm1
	movlhps	%xmm0, %xmm7
	movaps	%xmm7, -224(%rbp)
	movq	-224(%rbp), %rbx
	movq	-216(%rbp), %r14
	movd	%ebx, %xmm0
	ucomiss	%xmm1, %xmm0
	jp	.L1177
	jne	.L1177
	movss	.LC24(%rip), %xmm0
	testl	%ebx, %ebx
	jns	.L1179
	movss	.LC23(%rip), %xmm0
.L1179:
	sarq	$32, %rbx
	movd	%xmm0, %r13d
	movd	%ebx, %xmm0
	ucomiss	%xmm1, %xmm0
	jp	.L1183
	jne	.L1183
	movss	.LC24(%rip), %xmm0
	testl	%ebx, %ebx
	jns	.L1185
	movss	.LC23(%rip), %xmm0
.L1185:
	movd	%xmm0, %eax
	movd	%r14d, %xmm0
	salq	$32, %rax
	orq	%rax, %r13
	ucomiss	%xmm1, %xmm0
	jp	.L1189
	jne	.L1189
	movss	.LC24(%rip), %xmm0
	testl	%r14d, %r14d
	jns	.L1191
	movss	.LC23(%rip), %xmm0
.L1191:
	sarq	$32, %r14
	movd	%xmm0, %ebx
	movd	%r14d, %xmm0
	ucomiss	%xmm1, %xmm0
	jp	.L1362
	jne	.L1362
	movss	.LC23(%rip), %xmm0
	testl	%r14d, %r14d
	js	.L1198
	movss	.LC24(%rip), %xmm0
.L1198:
	movd	%xmm0, %eax
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	salq	$32, %rax
	movq	%r13, -79(%rbp)
	orq	%rbx, %rax
	movq	%rax, -71(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm5
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1007:
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rax
	movq	-103(%rbp), %rdx
	movd	%eax, %xmm1
	shrq	$32, %rax
	movd	%edx, %xmm0
	movd	%eax, %xmm5
	shrq	$32, %rdx
	movd	%edx, %xmm6
	unpcklps	%xmm5, %xmm1
	unpcklps	%xmm6, %xmm0
	movaps	%xmm1, %xmm6
	pxor	%xmm1, %xmm1
	movlhps	%xmm0, %xmm6
	movaps	%xmm6, -224(%rbp)
	movq	-224(%rbp), %rax
	movq	-216(%rbp), %rcx
	movd	%eax, %xmm2
	ucomiss	%xmm1, %xmm2
	jp	.L1168
	jne	.L1168
	movss	.LC23(%rip), %xmm0
	testl	%eax, %eax
	js	.L1169
	movss	.LC24(%rip), %xmm0
.L1169:
	sarq	$32, %rax
	movd	%xmm0, %edx
	movd	%eax, %xmm2
	ucomiss	%xmm1, %xmm2
	jp	.L1170
	jne	.L1170
	movss	.LC23(%rip), %xmm0
	testl	%eax, %eax
	js	.L1171
	movss	.LC24(%rip), %xmm0
.L1171:
	movd	%xmm0, %eax
	movd	%ecx, %xmm2
	salq	$32, %rax
	orq	%rax, %rdx
	ucomiss	%xmm1, %xmm2
	jp	.L1172
	jne	.L1172
	movss	.LC23(%rip), %xmm0
	testl	%ecx, %ecx
	js	.L1173
	movss	.LC24(%rip), %xmm0
.L1173:
	sarq	$32, %rcx
	movd	%xmm0, %eax
	movd	%ecx, %xmm2
	ucomiss	%xmm1, %xmm2
	jp	.L1361
	jne	.L1361
	movss	.LC23(%rip), %xmm0
	testl	%ecx, %ecx
	js	.L1176
	movss	.LC24(%rip), %xmm0
.L1176:
	movd	%xmm0, %ecx
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	salq	$32, %rcx
	movq	%rdx, -79(%rbp)
	orq	%rcx, %rax
	movq	%rax, -71(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm7
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1008:
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rax
	movq	-103(%rbp), %rdx
	movq	%r12, %rdi
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movd	%eax, %xmm0
	movd	%edx, %xmm1
	shrq	$32, %rax
	shrq	$32, %rdx
	movd	%eax, %xmm7
	movd	%edx, %xmm5
	unpcklps	%xmm7, %xmm0
	unpcklps	%xmm5, %xmm1
	movlhps	%xmm1, %xmm0
	xorps	.LC40(%rip), %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm4
	movaps	%xmm0, -144(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm4, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1009:
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rax
	movq	-103(%rbp), %rdx
	movq	%r12, %rdi
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movd	%eax, %xmm0
	movd	%edx, %xmm1
	shrq	$32, %rax
	shrq	$32, %rdx
	movd	%eax, %xmm4
	movd	%edx, %xmm7
	unpcklps	%xmm4, %xmm0
	unpcklps	%xmm7, %xmm1
	movlhps	%xmm1, %xmm0
	andps	.LC39(%rip), %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm6
	movaps	%xmm0, -144(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1010:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rax
	movq	-103(%rbp), %rdx
	movq	%r12, %rdi
	movq	-143(%rbp), %rsi
	movq	-135(%rbp), %rcx
	movb	$5, -80(%rbp)
	cmpq	%rsi, %rax
	cmovbe	%rsi, %rax
	cmpq	%rcx, %rdx
	cmovbe	%rcx, %rdx
	subq	$32, %rsp
	movq	%rax, %xmm0
	movq	%rdx, %xmm6
	punpcklqdq	%xmm6, %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm5
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1011:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rdx
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	movq	-143(%rbp), %rsi
	movq	-135(%rbp), %rcx
	movb	$5, -80(%rbp)
	cmpq	%rsi, %rdx
	cmovle	%rsi, %rdx
	cmpq	%rcx, %rax
	cmovle	%rcx, %rax
	subq	$32, %rsp
	movq	%rdx, -79(%rbp)
	movq	%rax, -71(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm7
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1012:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rax
	movq	-103(%rbp), %rdx
	movq	%r12, %rdi
	movq	-143(%rbp), %rsi
	movq	-135(%rbp), %rcx
	movb	$5, -80(%rbp)
	cmpq	%rsi, %rax
	cmovnb	%rsi, %rax
	cmpq	%rcx, %rdx
	cmovnb	%rcx, %rdx
	subq	$32, %rsp
	movq	%rax, %xmm0
	movq	%rdx, %xmm6
	punpcklqdq	%xmm6, %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm5
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1013:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rdx
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	movq	-143(%rbp), %rsi
	movq	-135(%rbp), %rcx
	movb	$5, -80(%rbp)
	cmpq	%rsi, %rdx
	cmovge	%rsi, %rdx
	cmpq	%rcx, %rax
	cmovge	%rcx, %rax
	subq	$32, %rsp
	movq	%rdx, -79(%rbp)
	movq	%rax, -71(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm7
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1014:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rdx
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	subq	-135(%rbp), %rax
	subq	-143(%rbp), %rdx
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movq	%rax, -71(%rbp)
	movzbl	-64(%rbp), %eax
	movq	%rdx, -79(%rbp)
	movdqa	-80(%rbp), %xmm6
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1015:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	-143(%rbp), %rdx
	movq	-135(%rbp), %rax
	imulq	-111(%rbp), %rdx
	imulq	-103(%rbp), %rax
	movq	%rdx, -79(%rbp)
	movq	%rax, -71(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm5
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1016:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	-143(%rbp), %rdx
	movq	-135(%rbp), %rax
	addq	-111(%rbp), %rdx
	addq	-103(%rbp), %rax
	movq	%rax, -71(%rbp)
	movzbl	-64(%rbp), %eax
	movq	%rdx, -79(%rbp)
	movdqa	-80(%rbp), %xmm7
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L972:
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-103(%rbp), %rax
	movb	$5, -80(%rbp)
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%rax, %rdx
	movl	%eax, %ecx
	movzwl	%ax, %ebx
	shrq	$48, %rax
	salq	$16, %rdx
	shrl	$16, %ecx
	movd	%eax, %xmm3
	movd	%ebx, %xmm0
	shrq	$48, %rdx
	movd	%ecx, %xmm4
	movd	%edx, %xmm1
	punpckldq	%xmm4, %xmm0
	punpckldq	%xmm3, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm6
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L974:
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rax
	movb	$5, -80(%rbp)
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%rax, %rdx
	movl	%eax, %ecx
	movzwl	%ax, %ebx
	shrq	$48, %rax
	salq	$16, %rdx
	shrl	$16, %ecx
	movd	%eax, %xmm4
	movd	%ebx, %xmm0
	shrq	$48, %rdx
	movd	%ecx, %xmm6
	movd	%edx, %xmm1
	punpckldq	%xmm6, %xmm0
	punpckldq	%xmm4, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm5
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L975:
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-103(%rbp), %rax
	movb	$5, -80(%rbp)
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%rax, %rdx
	movl	%eax, %ecx
	movswl	%ax, %ebx
	sarq	$48, %rax
	salq	$16, %rdx
	sarl	$16, %ecx
	movd	%eax, %xmm6
	movd	%ebx, %xmm0
	sarq	$48, %rdx
	movd	%ecx, %xmm5
	movd	%edx, %xmm1
	punpckldq	%xmm5, %xmm0
	punpckldq	%xmm6, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm7
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L976:
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rax
	movb	$5, -80(%rbp)
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%rax, %rdx
	movl	%eax, %ecx
	movswl	%ax, %ebx
	sarq	$48, %rax
	salq	$16, %rdx
	sarl	$16, %ecx
	movd	%eax, %xmm5
	movd	%ebx, %xmm0
	sarq	$48, %rdx
	movd	%ecx, %xmm7
	movd	%edx, %xmm1
	punpckldq	%xmm7, %xmm0
	punpckldq	%xmm5, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm3
	movb	%al, 16(%rsp)
	movups	%xmm3, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1069:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rdx
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movl	%edx, -192(%rbp)
	sarq	$32, %rdx
	movl	%eax, -184(%rbp)
	sarq	$32, %rax
	movl	%edx, -188(%rbp)
	movq	-143(%rbp), %rdx
	movl	%eax, -180(%rbp)
	movq	-135(%rbp), %rax
	movl	%edx, -176(%rbp)
	sarq	$32, %rdx
	movl	%eax, -168(%rbp)
	sarq	$32, %rax
	movl	%eax, -164(%rbp)
	movl	%edx, -172(%rbp)
	movdqa	-176(%rbp), %xmm0
	por	-192(%rbp), %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm5
	movaps	%xmm0, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1070:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rdx
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movl	%edx, -192(%rbp)
	sarq	$32, %rdx
	movl	%eax, -184(%rbp)
	sarq	$32, %rax
	movl	%edx, -188(%rbp)
	movq	-143(%rbp), %rdx
	movl	%eax, -180(%rbp)
	movq	-135(%rbp), %rax
	movl	%edx, -176(%rbp)
	sarq	$32, %rdx
	movl	%eax, -168(%rbp)
	sarq	$32, %rax
	movl	%eax, -164(%rbp)
	movl	%edx, -172(%rbp)
	movdqa	-176(%rbp), %xmm0
	pand	-192(%rbp), %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm6
	movaps	%xmm0, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1071:
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rdx
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	pcmpeqd	%xmm0, %xmm0
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movl	%edx, -160(%rbp)
	sarq	$32, %rdx
	movl	%eax, -152(%rbp)
	sarq	$32, %rax
	movl	%eax, -148(%rbp)
	movl	%edx, -156(%rbp)
	pxor	-160(%rbp), %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm5
	movaps	%xmm0, -144(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1072:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movsd	-111(%rbp), %xmm0
	xorl	%edx, %edx
	movb	$5, -80(%rbp)
	comisd	-143(%rbp), %xmm0
	movsd	-103(%rbp), %xmm0
	movq	%r12, %rdi
	setnb	%dl
	xorl	%eax, %eax
	negq	%rdx
	comisd	-135(%rbp), %xmm0
	movq	%rdx, -79(%rbp)
	setnb	%al
	subq	$32, %rsp
	negq	%rax
	movq	%rax, -71(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm6
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1073:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	xorl	%edx, %edx
	movb	$5, -80(%rbp)
	movq	%r12, %rdi
	movsd	-143(%rbp), %xmm0
	comisd	-111(%rbp), %xmm0
	movsd	-135(%rbp), %xmm0
	setnb	%dl
	xorl	%eax, %eax
	negq	%rdx
	comisd	-103(%rbp), %xmm0
	movq	%rdx, -79(%rbp)
	setnb	%al
	subq	$32, %rsp
	negq	%rax
	movq	%rax, -71(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm7
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1074:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movsd	-111(%rbp), %xmm0
	xorl	%edx, %edx
	movb	$5, -80(%rbp)
	comisd	-143(%rbp), %xmm0
	movsd	-103(%rbp), %xmm0
	movq	%r12, %rdi
	seta	%dl
	xorl	%eax, %eax
	negq	%rdx
	comisd	-135(%rbp), %xmm0
	movq	%rdx, -79(%rbp)
	seta	%al
	subq	$32, %rsp
	negq	%rax
	movq	%rax, -71(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm4
	movb	%al, 16(%rsp)
	movups	%xmm4, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1075:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	xorl	%edx, %edx
	movb	$5, -80(%rbp)
	movq	%r12, %rdi
	movsd	-143(%rbp), %xmm0
	comisd	-111(%rbp), %xmm0
	movsd	-135(%rbp), %xmm0
	seta	%dl
	xorl	%eax, %eax
	negq	%rdx
	comisd	-103(%rbp), %xmm0
	movq	%rdx, -79(%rbp)
	seta	%al
	subq	$32, %rsp
	negq	%rax
	movq	%rax, -71(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm5
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1076:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	xorl	%edx, %edx
	movl	$1, %ecx
	movb	$5, -80(%rbp)
	movsd	-143(%rbp), %xmm0
	ucomisd	-111(%rbp), %xmm0
	movq	%r12, %rdi
	movsd	-135(%rbp), %xmm0
	setp	%dl
	cmovne	%rcx, %rdx
	xorl	%eax, %eax
	negq	%rdx
	ucomisd	-103(%rbp), %xmm0
	movq	%rdx, -79(%rbp)
	setp	%al
	cmovne	%rcx, %rax
	subq	$32, %rsp
	negq	%rax
	movq	%rax, -71(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm7
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1077:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	xorl	%edx, %edx
	movl	$0, %ecx
	movb	$5, -80(%rbp)
	movsd	-143(%rbp), %xmm0
	ucomisd	-111(%rbp), %xmm0
	movq	%r12, %rdi
	movsd	-135(%rbp), %xmm0
	setnp	%dl
	cmovne	%rcx, %rdx
	xorl	%eax, %eax
	negq	%rdx
	ucomisd	-103(%rbp), %xmm0
	movq	%rdx, -79(%rbp)
	setnp	%al
	cmovne	%rcx, %rax
	subq	$32, %rsp
	negq	%rax
	movq	%rax, -71(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm5
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1078:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rax
	movq	-103(%rbp), %rdx
	movq	%r12, %rdi
	movq	-143(%rbp), %rcx
	movq	-135(%rbp), %rsi
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movd	%edx, %xmm2
	shrq	$32, %rdx
	movd	%ecx, %xmm0
	movd	%esi, %xmm1
	shrq	$32, %rcx
	movd	%edx, %xmm6
	shrq	$32, %rsi
	movd	%ecx, %xmm4
	unpcklps	%xmm6, %xmm2
	movd	%esi, %xmm7
	unpcklps	%xmm4, %xmm0
	unpcklps	%xmm7, %xmm1
	movlhps	%xmm1, %xmm0
	movd	%eax, %xmm1
	shrq	$32, %rax
	movd	%eax, %xmm5
	unpcklps	%xmm5, %xmm1
	movlhps	%xmm2, %xmm1
	cmpleps	%xmm1, %xmm0
	pand	.LC42(%rip), %xmm0
	pxor	%xmm1, %xmm1
	psubd	%xmm0, %xmm1
	movups	%xmm1, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm7
	movaps	%xmm1, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1079:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rax
	movq	-103(%rbp), %rdx
	movq	%r12, %rdi
	movq	-143(%rbp), %rcx
	movq	-135(%rbp), %rsi
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movd	%edx, %xmm2
	shrq	$32, %rdx
	movd	%ecx, %xmm1
	movd	%esi, %xmm0
	shrq	$32, %rcx
	movd	%edx, %xmm7
	shrq	$32, %rsi
	movd	%ecx, %xmm5
	unpcklps	%xmm7, %xmm2
	movd	%esi, %xmm6
	unpcklps	%xmm5, %xmm1
	unpcklps	%xmm6, %xmm0
	movlhps	%xmm0, %xmm1
	movd	%eax, %xmm0
	shrq	$32, %rax
	movd	%eax, %xmm4
	unpcklps	%xmm4, %xmm0
	movlhps	%xmm2, %xmm0
	cmpleps	%xmm1, %xmm0
	pand	.LC42(%rip), %xmm0
	pxor	%xmm1, %xmm1
	psubd	%xmm0, %xmm1
	movups	%xmm1, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm6
	movaps	%xmm1, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1080:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rax
	movq	-103(%rbp), %rdx
	movq	%r12, %rdi
	movq	-143(%rbp), %rcx
	movq	-135(%rbp), %rsi
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movd	%edx, %xmm2
	shrq	$32, %rdx
	movd	%ecx, %xmm0
	movd	%esi, %xmm1
	shrq	$32, %rcx
	movd	%edx, %xmm4
	shrq	$32, %rsi
	movd	%ecx, %xmm7
	unpcklps	%xmm4, %xmm2
	movd	%esi, %xmm5
	unpcklps	%xmm7, %xmm0
	unpcklps	%xmm5, %xmm1
	movlhps	%xmm1, %xmm0
	movd	%eax, %xmm1
	shrq	$32, %rax
	movd	%eax, %xmm6
	unpcklps	%xmm6, %xmm1
	movlhps	%xmm2, %xmm1
	cmpltps	%xmm1, %xmm0
	pand	.LC42(%rip), %xmm0
	pxor	%xmm1, %xmm1
	psubd	%xmm0, %xmm1
	movups	%xmm1, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm5
	movaps	%xmm1, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1050:
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rdx
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	movdqa	.LC28(%rip), %xmm0
	pxor	%xmm3, %xmm3
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movl	%edx, %ecx
	movw	%dx, -160(%rbp)
	sarl	$16, %ecx
	movw	%ax, -152(%rbp)
	movdqa	%xmm0, %xmm1
	movdqa	%xmm0, %xmm2
	movw	%cx, -158(%rbp)
	movq	%rdx, %rcx
	sarq	$48, %rdx
	movw	%dx, -154(%rbp)
	movl	%eax, %edx
	salq	$16, %rcx
	sarl	$16, %edx
	sarq	$48, %rcx
	movw	%dx, -150(%rbp)
	movq	%rax, %rdx
	sarq	$48, %rax
	salq	$16, %rdx
	movw	%ax, -146(%rbp)
	sarq	$48, %rdx
	movw	%cx, -156(%rbp)
	movw	%dx, -148(%rbp)
	psubw	-160(%rbp), %xmm3
	pcmpeqw	-160(%rbp), %xmm1
	pand	%xmm1, %xmm2
	pandn	%xmm3, %xmm1
	movdqa	%xmm1, %xmm0
	por	%xmm2, %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm7
	movaps	%xmm0, -144(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1051:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	pxor	%xmm2, %xmm2
	movb	$5, -80(%rbp)
	movq	%r12, %rdi
	movdqu	-111(%rbp), %xmm3
	movdqu	-143(%rbp), %xmm0
	subq	$32, %rsp
	movdqu	-111(%rbp), %xmm4
	movdqu	-143(%rbp), %xmm6
	movdqa	%xmm3, %xmm1
	psubusb	%xmm0, %xmm1
	movaps	%xmm4, -192(%rbp)
	pcmpeqb	%xmm2, %xmm1
	movdqa	%xmm0, %xmm2
	movaps	%xmm6, -176(%rbp)
	pand	%xmm1, %xmm2
	pandn	%xmm3, %xmm1
	movdqa	%xmm1, %xmm0
	por	%xmm2, %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm5
	movaps	%xmm0, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1052:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movdqu	-143(%rbp), %xmm0
	movdqu	-111(%rbp), %xmm3
	movdqu	-111(%rbp), %xmm6
	movdqu	-143(%rbp), %xmm5
	movdqa	%xmm0, %xmm1
	movdqa	%xmm0, %xmm2
	pcmpgtb	%xmm3, %xmm1
	movaps	%xmm6, -192(%rbp)
	movaps	%xmm5, -176(%rbp)
	pand	%xmm1, %xmm2
	pandn	%xmm3, %xmm1
	movdqa	%xmm1, %xmm0
	por	%xmm2, %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm7
	movaps	%xmm0, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1053:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	pxor	%xmm2, %xmm2
	movb	$5, -80(%rbp)
	movq	%r12, %rdi
	movdqu	-111(%rbp), %xmm3
	movdqu	-143(%rbp), %xmm0
	subq	$32, %rsp
	movdqu	-111(%rbp), %xmm5
	movdqu	-143(%rbp), %xmm7
	movdqa	%xmm0, %xmm1
	psubusb	%xmm3, %xmm1
	movaps	%xmm5, -192(%rbp)
	pcmpeqb	%xmm2, %xmm1
	movdqa	%xmm0, %xmm2
	movaps	%xmm7, -176(%rbp)
	pand	%xmm1, %xmm2
	pandn	%xmm3, %xmm1
	movdqa	%xmm1, %xmm0
	por	%xmm2, %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm4
	movaps	%xmm0, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm4, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L989:
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rdx
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movl	%edx, -160(%rbp)
	sarq	$32, %rdx
	movl	%eax, -152(%rbp)
	sarq	$32, %rax
	movl	%eax, -148(%rbp)
	movl	%edx, -156(%rbp)
	cvtdq2ps	-160(%rbp), %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm7
	movaps	%xmm0, -144(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L990:
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rax
	movq	-103(%rbp), %rdx
	movq	%r12, %rdi
	pxor	%xmm2, %xmm2
	movapd	.LC49(%rip), %xmm5
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movd	%eax, %xmm0
	movd	%edx, %xmm1
	shrq	$32, %rax
	shrq	$32, %rdx
	movd	%eax, %xmm3
	movd	%edx, %xmm7
	unpcklps	%xmm3, %xmm0
	unpcklps	%xmm7, %xmm1
	movaps	.LC50(%rip), %xmm7
	movlhps	%xmm1, %xmm0
	movhlps	%xmm0, %xmm2
	movaps	%xmm0, %xmm9
	movaps	%xmm7, %xmm4
	cvtps2pd	%xmm0, %xmm1
	cvtps2pd	%xmm2, %xmm3
	pxor	%xmm2, %xmm2
	cmpleps	%xmm0, %xmm4
	movapd	%xmm1, %xmm8
	cmpltps	%xmm2, %xmm9
	cmpordpd	%xmm1, %xmm8
	movapd	%xmm3, %xmm2
	cmpordpd	%xmm3, %xmm2
	andps	%xmm4, %xmm7
	pslld	$31, %xmm4
	subps	%xmm7, %xmm0
	movdqa	%xmm9, %xmm6
	shufps	$136, %xmm2, %xmm8
	movapd	%xmm5, %xmm2
	cmpltpd	%xmm3, %xmm5
	pandn	%xmm8, %xmm6
	cmpltpd	%xmm1, %xmm2
	cmpunordpd	%xmm3, %xmm3
	cmpunordpd	%xmm1, %xmm1
	cvttps2dq	%xmm0, %xmm0
	pxor	%xmm0, %xmm4
	movdqa	%xmm8, %xmm0
	pand	%xmm9, %xmm0
	shufps	$136, %xmm3, %xmm1
	pcmpeqd	%xmm3, %xmm3
	por	%xmm0, %xmm1
	movdqa	%xmm3, %xmm0
	pxor	%xmm5, %xmm3
	pxor	%xmm2, %xmm0
	shufps	$136, %xmm5, %xmm2
	pand	%xmm6, %xmm2
	shufps	$136, %xmm3, %xmm0
	movdqa	%xmm2, %xmm3
	pand	%xmm6, %xmm0
	pandn	%xmm4, %xmm3
	pand	%xmm0, %xmm4
	por	%xmm3, %xmm2
	pandn	%xmm2, %xmm0
	por	%xmm4, %xmm0
	pandn	%xmm0, %xmm1
	movups	%xmm1, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm4
	movaps	%xmm1, -144(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm4, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L991:
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rax
	movq	-103(%rbp), %rdx
	movq	%r12, %rdi
	pxor	%xmm1, %xmm1
	movapd	.LC47(%rip), %xmm5
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movd	%eax, %xmm2
	movd	%edx, %xmm0
	shrq	$32, %rax
	shrq	$32, %rdx
	movd	%eax, %xmm6
	movd	%edx, %xmm4
	unpcklps	%xmm6, %xmm2
	unpcklps	%xmm4, %xmm0
	movlhps	%xmm0, %xmm2
	movhlps	%xmm2, %xmm1
	cvtps2pd	%xmm2, %xmm3
	movapd	%xmm3, %xmm4
	movaps	%xmm2, %xmm6
	cvtps2pd	%xmm1, %xmm1
	movapd	%xmm1, %xmm0
	cmpordpd	%xmm3, %xmm4
	cvttps2dq	%xmm2, %xmm2
	cmpordpd	%xmm1, %xmm0
	cmpltps	.LC46(%rip), %xmm6
	shufps	$136, %xmm0, %xmm4
	movapd	%xmm5, %xmm0
	cmpltpd	%xmm1, %xmm5
	movdqa	%xmm6, %xmm7
	cmpltpd	%xmm3, %xmm0
	cmpunordpd	%xmm1, %xmm1
	pandn	%xmm4, %xmm7
	cmpunordpd	%xmm3, %xmm3
	pand	%xmm6, %xmm4
	pcmpeqd	%xmm6, %xmm6
	shufps	$136, %xmm1, %xmm3
	movdqa	%xmm6, %xmm1
	pxor	%xmm5, %xmm6
	pxor	%xmm0, %xmm1
	shufps	$136, %xmm5, %xmm0
	movdqa	.LC48(%rip), %xmm5
	pand	%xmm7, %xmm0
	shufps	$136, %xmm6, %xmm1
	pand	%xmm7, %xmm1
	pand	%xmm0, %xmm5
	pandn	%xmm2, %xmm0
	pand	%xmm1, %xmm2
	por	%xmm5, %xmm0
	pandn	%xmm0, %xmm1
	por	%xmm2, %xmm1
	movdqa	.LC25(%rip), %xmm2
	pand	%xmm4, %xmm2
	pandn	%xmm1, %xmm4
	movdqa	%xmm4, %xmm0
	por	%xmm2, %xmm0
	pandn	%xmm0, %xmm3
	movups	%xmm3, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm5
	movaps	%xmm3, -144(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L992:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movupd	-111(%rbp), %xmm5
	movupd	-143(%rbp), %xmm6
	movaps	%xmm5, -224(%rbp)
	movsd	-224(%rbp), %xmm0
	movaps	%xmm6, -240(%rbp)
	movsd	-240(%rbp), %xmm1
	ucomisd	%xmm0, %xmm0
	jp	.L1154
	ucomisd	%xmm1, %xmm1
	jp	.L1334
	movmskpd	%xmm0, %edx
	movmskpd	%xmm1, %eax
	andl	$1, %edx
	andl	$1, %eax
	cmpb	%al, %dl
	jb	.L1154
	maxsd	%xmm1, %xmm0
.L1154:
	movq	%xmm0, %rax
	movsd	-216(%rbp), %xmm0
	movsd	-232(%rbp), %xmm1
	ucomisd	%xmm0, %xmm0
	jp	.L1155
	ucomisd	%xmm1, %xmm1
	jp	.L1335
	movmskpd	%xmm0, %ecx
	movmskpd	%xmm1, %edx
	andl	$1, %ecx
	andl	$1, %edx
	cmpb	%dl, %cl
	jb	.L1155
	maxsd	%xmm1, %xmm0
.L1155:
	movq	%rax, -79(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	%xmm0, -71(%rbp)
	movzbl	-64(%rbp), %eax
	movb	$5, -80(%rbp)
	movdqa	-80(%rbp), %xmm5
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L993:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movupd	-111(%rbp), %xmm5
	movupd	-143(%rbp), %xmm7
	movaps	%xmm5, -224(%rbp)
	movsd	-224(%rbp), %xmm1
	movaps	%xmm7, -240(%rbp)
	movsd	-240(%rbp), %xmm0
	ucomisd	%xmm1, %xmm1
	jp	.L1332
	ucomisd	%xmm0, %xmm0
	jp	.L1152
	movmskpd	%xmm1, %eax
	movmskpd	%xmm0, %edx
	andl	$1, %edx
	andl	$1, %eax
	cmpb	%al, %dl
	ja	.L1152
	minsd	%xmm1, %xmm0
.L1152:
	movsd	-216(%rbp), %xmm1
	movq	%xmm0, %rax
	movsd	-232(%rbp), %xmm0
	ucomisd	%xmm1, %xmm1
	jp	.L1333
	ucomisd	%xmm0, %xmm0
	jp	.L1153
	movmskpd	%xmm1, %ecx
	movmskpd	%xmm0, %edx
	andl	$1, %ecx
	andl	$1, %edx
	cmpb	%dl, %cl
	jb	.L1153
	minsd	%xmm1, %xmm0
.L1153:
	movq	%rax, -79(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	%xmm0, -71(%rbp)
	movzbl	-64(%rbp), %eax
	movb	$5, -80(%rbp)
	movdqa	-80(%rbp), %xmm6
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L994:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movupd	-111(%rbp), %xmm7
	pxor	%xmm1, %xmm1
	movaps	%xmm7, -224(%rbp)
	movupd	-143(%rbp), %xmm7
	movsd	-224(%rbp), %xmm0
	movaps	%xmm7, -240(%rbp)
	movsd	-240(%rbp), %xmm2
	ucomisd	%xmm1, %xmm2
	jp	.L1145
	jne	.L1145
	ucomisd	%xmm0, %xmm0
	jp	.L1328
	jne	.L1328
	ucomisd	%xmm1, %xmm0
	movl	$0, %edx
	setnp	%al
	cmovne	%edx, %eax
	testb	%al, %al
	jne	.L1328
	movmskpd	%xmm2, %eax
	testb	$1, %al
	sete	%dl
	comisd	%xmm1, %xmm0
	movsd	.LC21(%rip), %xmm0
	setnb	%al
	cmpb	%al, %dl
	jne	.L1146
	movsd	.LC22(%rip), %xmm0
.L1146:
	movsd	-232(%rbp), %xmm2
	movq	%xmm0, %rax
	movsd	-216(%rbp), %xmm0
	ucomisd	%xmm1, %xmm2
	jp	.L1360
	jne	.L1360
	ucomisd	%xmm1, %xmm0
	movl	$0, %ecx
	setnp	%dl
	cmovne	%ecx, %edx
	testb	%dl, %dl
	jne	.L1330
	ucomisd	%xmm0, %xmm0
	jp	.L1330
	jne	.L1330
	comisd	%xmm1, %xmm0
	movmskpd	%xmm2, %edx
	movsd	.LC21(%rip), %xmm0
	setnb	%cl
	andb	$1, %dl
	sete	%dl
	cmpb	%dl, %cl
	jne	.L1150
	movsd	.LC22(%rip), %xmm0
.L1150:
	movq	%rax, -79(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	%xmm0, -71(%rbp)
	movzbl	-64(%rbp), %eax
	movb	$5, -80(%rbp)
	movdqa	-80(%rbp), %xmm7
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L995:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movsd	-111(%rbp), %xmm0
	movsd	-103(%rbp), %xmm1
	mulsd	-143(%rbp), %xmm0
	mulsd	-135(%rbp), %xmm1
	unpcklpd	%xmm1, %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm5
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L996:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movsd	-111(%rbp), %xmm0
	movsd	-103(%rbp), %xmm1
	subsd	-143(%rbp), %xmm0
	subsd	-135(%rbp), %xmm1
	unpcklpd	%xmm1, %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm6
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L997:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movsd	-111(%rbp), %xmm0
	movsd	-103(%rbp), %xmm1
	addsd	-143(%rbp), %xmm0
	addsd	-135(%rbp), %xmm1
	unpcklpd	%xmm1, %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm7
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L998:
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	.LC38(%rip), %xmm1
	movsd	-111(%rbp), %xmm0
	movsd	-103(%rbp), %xmm6
	xorpd	%xmm1, %xmm0
	xorpd	%xmm6, %xmm1
	unpcklpd	%xmm1, %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm5
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L999:
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	.LC37(%rip), %xmm1
	movsd	-111(%rbp), %xmm0
	movsd	-103(%rbp), %xmm7
	andpd	%xmm1, %xmm0
	andpd	%xmm7, %xmm1
	unpcklpd	%xmm1, %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm4
	movb	%al, 16(%rsp)
	movups	%xmm4, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1000:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rax
	movq	-103(%rbp), %rdx
	movd	%eax, %xmm1
	shrq	$32, %rax
	movd	%edx, %xmm0
	movd	%eax, %xmm5
	shrq	$32, %rdx
	movq	-143(%rbp), %rax
	movd	%edx, %xmm6
	unpcklps	%xmm5, %xmm1
	movq	-135(%rbp), %rdx
	unpcklps	%xmm6, %xmm0
	movaps	%xmm1, %xmm6
	movd	%eax, %xmm1
	shrq	$32, %rax
	movlhps	%xmm0, %xmm6
	movd	%edx, %xmm0
	shrq	$32, %rdx
	movaps	%xmm6, -224(%rbp)
	movd	%eax, %xmm6
	movq	-224(%rbp), %rdi
	movd	%edx, %xmm7
	unpcklps	%xmm6, %xmm1
	unpcklps	%xmm7, %xmm0
	movq	-216(%rbp), %rsi
	movaps	%xmm1, %xmm7
	movlhps	%xmm0, %xmm7
	movd	%edi, %xmm0
	ucomiss	%xmm0, %xmm0
	movaps	%xmm7, -224(%rbp)
	movq	-224(%rbp), %rax
	movq	-216(%rbp), %rcx
	movd	%eax, %xmm1
	jp	.L1160
	ucomiss	%xmm1, %xmm1
	jp	.L1340
	testl	%edi, %edi
	sets	%r8b
	testl	%eax, %eax
	sets	%dl
	cmpb	%dl, %r8b
	jb	.L1160
	maxss	%xmm1, %xmm0
.L1160:
	sarq	$32, %rdi
	movd	%xmm0, %edx
	sarq	$32, %rax
	movd	%edi, %xmm0
	movd	%eax, %xmm1
	ucomiss	%xmm0, %xmm0
	jp	.L1161
	ucomiss	%xmm1, %xmm1
	jp	.L1341
	testl	%edi, %edi
	sets	%dil
	testl	%eax, %eax
	sets	%al
	cmpb	%al, %dil
	jb	.L1161
	maxss	%xmm1, %xmm0
.L1161:
	movd	%xmm0, %eax
	movd	%esi, %xmm0
	movd	%ecx, %xmm1
	salq	$32, %rax
	orq	%rax, %rdx
	ucomiss	%xmm0, %xmm0
	jp	.L1162
	ucomiss	%xmm1, %xmm1
	jp	.L1342
	testl	%esi, %esi
	sets	%dil
	testl	%ecx, %ecx
	sets	%al
	cmpb	%al, %dil
	jb	.L1162
	maxss	%xmm1, %xmm0
.L1162:
	sarq	$32, %rsi
	movd	%xmm0, %eax
	sarq	$32, %rcx
	movd	%esi, %xmm0
	movd	%ecx, %xmm1
	ucomiss	%xmm0, %xmm0
	jp	.L1163
	ucomiss	%xmm1, %xmm1
	jp	.L1343
	testl	%esi, %esi
	sets	%sil
	testl	%ecx, %ecx
	sets	%cl
	cmpb	%cl, %sil
	jb	.L1163
	maxss	%xmm1, %xmm0
.L1163:
	movd	%xmm0, %ecx
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	salq	$32, %rcx
	movq	%rdx, -79(%rbp)
	orq	%rcx, %rax
	movq	%rax, -71(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm5
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1001:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rax
	movq	-103(%rbp), %rdx
	movq	-135(%rbp), %rcx
	movd	%eax, %xmm1
	shrq	$32, %rax
	movd	%edx, %xmm0
	movd	%eax, %xmm7
	shrq	$32, %rdx
	movd	%edx, %xmm5
	unpcklps	%xmm7, %xmm1
	movq	-143(%rbp), %rdx
	unpcklps	%xmm5, %xmm0
	movaps	%xmm1, %xmm7
	movlhps	%xmm0, %xmm7
	movd	%edx, %xmm1
	shrq	$32, %rdx
	movd	%ecx, %xmm0
	movaps	%xmm7, -224(%rbp)
	shrq	$32, %rcx
	movq	-224(%rbp), %rax
	movd	%edx, %xmm5
	movd	%ecx, %xmm6
	unpcklps	%xmm5, %xmm1
	movq	-216(%rbp), %rsi
	unpcklps	%xmm6, %xmm0
	movaps	%xmm1, %xmm5
	movd	%eax, %xmm1
	movlhps	%xmm0, %xmm5
	ucomiss	%xmm1, %xmm1
	movaps	%xmm5, -224(%rbp)
	movq	-224(%rbp), %rdi
	movq	-216(%rbp), %rcx
	movd	%edi, %xmm0
	jp	.L1336
	ucomiss	%xmm0, %xmm0
	jp	.L1156
	testl	%edi, %edi
	sets	%r8b
	testl	%eax, %eax
	sets	%dl
	cmpb	%dl, %r8b
	ja	.L1156
	minss	%xmm1, %xmm0
.L1156:
	sarq	$32, %rax
	sarq	$32, %rdi
	movd	%xmm0, %edx
	movd	%eax, %xmm1
	movd	%edi, %xmm0
	ucomiss	%xmm1, %xmm1
	jp	.L1337
	ucomiss	%xmm0, %xmm0
	jp	.L1157
	testl	%edi, %edi
	sets	%dil
	testl	%eax, %eax
	sets	%al
	cmpb	%al, %dil
	ja	.L1157
	minss	%xmm1, %xmm0
.L1157:
	movd	%xmm0, %eax
	movd	%esi, %xmm1
	movd	%ecx, %xmm0
	salq	$32, %rax
	orq	%rax, %rdx
	ucomiss	%xmm1, %xmm1
	jp	.L1338
	ucomiss	%xmm0, %xmm0
	jp	.L1158
	testl	%ecx, %ecx
	sets	%dil
	testl	%esi, %esi
	sets	%al
	cmpb	%al, %dil
	ja	.L1158
	minss	%xmm1, %xmm0
.L1158:
	sarq	$32, %rsi
	sarq	$32, %rcx
	movd	%xmm0, %eax
	movd	%esi, %xmm1
	movd	%ecx, %xmm0
	ucomiss	%xmm1, %xmm1
	jp	.L1339
	ucomiss	%xmm0, %xmm0
	jp	.L1159
	testl	%esi, %esi
	sets	%sil
	testl	%ecx, %ecx
	sets	%cl
	cmpb	%cl, %sil
	jb	.L1159
	minss	%xmm1, %xmm0
.L1159:
	movd	%xmm0, %ecx
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	salq	$32, %rcx
	movq	%rdx, -79(%rbp)
	orq	%rcx, %rax
	movq	%rax, -71(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm7
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1002:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rcx
	movq	-103(%rbp), %rsi
	movq	%r12, %rdi
	movq	-143(%rbp), %rax
	movq	-135(%rbp), %rdx
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movd	%ecx, %xmm0
	movd	%esi, %xmm1
	shrq	$32, %rcx
	shrq	$32, %rsi
	movd	%ecx, %xmm6
	movd	%edx, %xmm2
	shrq	$32, %rdx
	movd	%esi, %xmm7
	unpcklps	%xmm6, %xmm0
	movd	%edx, %xmm5
	unpcklps	%xmm7, %xmm1
	unpcklps	%xmm5, %xmm2
	movlhps	%xmm1, %xmm0
	movd	%eax, %xmm1
	shrq	$32, %rax
	movd	%eax, %xmm7
	unpcklps	%xmm7, %xmm1
	movlhps	%xmm2, %xmm1
	divps	%xmm1, %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm6
	movaps	%xmm0, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1003:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rcx
	movq	-103(%rbp), %rsi
	movq	%r12, %rdi
	movq	-143(%rbp), %rax
	movq	-135(%rbp), %rdx
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movd	%ecx, %xmm0
	movd	%esi, %xmm1
	shrq	$32, %rcx
	shrq	$32, %rsi
	movd	%ecx, %xmm5
	movd	%edx, %xmm2
	shrq	$32, %rdx
	movd	%esi, %xmm6
	unpcklps	%xmm5, %xmm0
	movd	%edx, %xmm7
	unpcklps	%xmm6, %xmm1
	unpcklps	%xmm7, %xmm2
	movlhps	%xmm1, %xmm0
	movd	%eax, %xmm1
	shrq	$32, %rax
	movd	%eax, %xmm6
	unpcklps	%xmm6, %xmm1
	movlhps	%xmm2, %xmm1
	mulps	%xmm1, %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm5
	movaps	%xmm0, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1004:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rcx
	movq	-103(%rbp), %rsi
	movq	%r12, %rdi
	movq	-143(%rbp), %rax
	movq	-135(%rbp), %rdx
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movd	%ecx, %xmm0
	movd	%esi, %xmm1
	shrq	$32, %rcx
	shrq	$32, %rsi
	movd	%ecx, %xmm7
	movd	%edx, %xmm2
	shrq	$32, %rdx
	movd	%esi, %xmm5
	unpcklps	%xmm7, %xmm0
	movd	%edx, %xmm6
	unpcklps	%xmm5, %xmm1
	unpcklps	%xmm6, %xmm2
	movlhps	%xmm1, %xmm0
	movd	%eax, %xmm1
	shrq	$32, %rax
	movd	%eax, %xmm5
	unpcklps	%xmm5, %xmm1
	movlhps	%xmm2, %xmm1
	subps	%xmm1, %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm7
	movaps	%xmm0, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L981:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	movl	$-1, %r14d
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rax
	movq	-103(%rbp), %rcx
	movq	$0, -224(%rbp)
	movq	-135(%rbp), %rsi
	movq	-143(%rbp), %rdx
	movq	$0, -216(%rbp)
	movq	%rax, %rdi
	movq	%rcx, %r13
	movl	%ecx, -240(%rbp)
	movq	%rdx, %rbx
	movq	%rsi, %r11
	sarq	$32, %r13
	movl	%esi, %r9d
	sarq	$32, %rbx
	sarq	$32, %r11
	movl	%r13d, -256(%rbp)
	sarq	$32, %rdi
	cmpl	$65535, %eax
	movl	%ebx, %r10d
	movl	%r11d, %r8d
	cmovnb	%r14d, %eax
	movw	%ax, -224(%rbp)
	cmpl	$65534, %edi
	jbe	.L1218
	movdqa	-224(%rbp), %xmm5
	movq	$-1, %rax
	pinsrw	$1, %eax, %xmm5
	movaps	%xmm5, -224(%rbp)
.L1219:
	cmpl	$65534, %ecx
	jbe	.L1220
	movdqa	-224(%rbp), %xmm5
	movq	$-1, %rax
	pinsrw	$2, %eax, %xmm5
	movaps	%xmm5, -224(%rbp)
.L1221:
	cmpl	$65534, %r13d
	jbe	.L1222
	movdqa	-224(%rbp), %xmm7
	movq	$-1, %rax
	pinsrw	$3, %eax, %xmm7
	movaps	%xmm7, -224(%rbp)
.L1223:
	cmpl	$65535, %edx
	movl	$-1, %eax
	movb	$5, -80(%rbp)
	movq	%r12, %rdi
	cmovnb	%eax, %edx
	movl	$-1, %eax
	cmpl	$65535, %ebx
	cmovnb	%eax, %r10d
	cmpl	$65535, %esi
	movw	%dx, -216(%rbp)
	movdqa	-224(%rbp), %xmm0
	cmovnb	%eax, %r9d
	cmpl	$65535, %r11d
	cmovnb	%eax, %r8d
	subq	$32, %rsp
	pinsrw	$5, %r10d, %xmm0
	pinsrw	$6, %r9d, %xmm0
	pinsrw	$7, %r8d, %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm6
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L982:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rsi
	movq	$-32768, %r8
	movl	$32767, %edi
	movq	-103(%rbp), %rcx
	movq	-143(%rbp), %rdx
	movb	$5, -80(%rbp)
	cmpl	$-32768, %esi
	movslq	%esi, %r9
	movq	$0, -224(%rbp)
	movq	-135(%rbp), %rax
	movq	$0, -216(%rbp)
	cmovl	%r8, %r9
	cmpq	$32767, %r9
	cmovg	%rdi, %r9
	sarq	$32, %rsi
	cmpq	$-32768, %rsi
	cmovl	%r8, %rsi
	movw	%r9w, -224(%rbp)
	movdqa	-224(%rbp), %xmm0
	cmpq	$32767, %rsi
	cmovg	%rdi, %rsi
	cmpl	$-32768, %ecx
	pinsrw	$1, %esi, %xmm0
	movslq	%ecx, %rsi
	cmovl	%r8, %rsi
	cmpq	$32767, %rsi
	cmovg	%rdi, %rsi
	sarq	$32, %rcx
	cmpq	$-32768, %rcx
	cmovl	%r8, %rcx
	pinsrw	$2, %esi, %xmm0
	cmpq	$32767, %rcx
	cmovg	%rdi, %rcx
	cmpl	$-32768, %edx
	pinsrw	$3, %ecx, %xmm0
	movslq	%edx, %rcx
	cmovl	%r8, %rcx
	movaps	%xmm0, -224(%rbp)
	cmpq	$32767, %rcx
	cmovg	%rdi, %rcx
	sarq	$32, %rdx
	cmpq	$-32768, %rdx
	cmovl	%r8, %rdx
	movw	%cx, -216(%rbp)
	movdqa	-224(%rbp), %xmm0
	cmpq	$32767, %rdx
	cmovg	%rdi, %rdx
	cmpl	$-32768, %eax
	pinsrw	$5, %edx, %xmm0
	movslq	%eax, %rdx
	cmovl	%r8, %rdx
	cmpq	$32767, %rdx
	cmovg	%rdi, %rdx
	sarq	$32, %rax
	cmpq	$-32768, %rax
	cmovl	%r8, %rax
	pinsrw	$6, %edx, %xmm0
	cmpq	$32767, %rax
	cmovg	%rdi, %rax
	subq	$32, %rsp
	movq	%r12, %rdi
	pinsrw	$7, %eax, %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm5
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L983:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	movl	$-1, %ebx
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-103(%rbp), %rdi
	movq	-135(%rbp), %rdx
	movq	-143(%rbp), %rsi
	movq	-111(%rbp), %r8
	movq	%rdi, %rax
	movl	%edi, %r14d
	movl	%edx, %r9d
	salq	$16, %rax
	movq	%rsi, %rcx
	movq	%r8, %r11
	movq	%r8, %r15
	sarq	$48, %rax
	salq	$16, %rcx
	movl	%esi, %r13d
	movq	%rsi, %r10
	movq	%rax, -224(%rbp)
	salq	$16, %r11
	movq	%rdi, %rax
	sarq	$48, %rcx
	sarq	$48, %rax
	sarq	$48, %r15
	movq	%rcx, -256(%rbp)
	movq	%rax, -240(%rbp)
	sarl	$16, %r14d
	movq	%rdx, %rax
	sarl	$16, %r13d
	salq	$16, %rax
	sarq	$48, %r10
	sarq	$48, %rax
	sarl	$16, %r9d
	movq	%rax, -272(%rbp)
	movq	%rdx, %rax
	sarq	$48, %rax
	movq	%rax, -288(%rbp)
	movswl	%r8w, %eax
	cmpl	$255, %eax
	cmovb	%eax, %ebx
	xorl	%eax, %eax
	movb	%bl, %al
	movl	%r8d, %ebx
	sarl	$16, %ebx
	cmpl	$16711679, %r8d
	movq	$-1, %r8
	cmova	%r8, %rbx
	sarq	$48, %r11
	movb	%bl, %ah
	cmpl	$254, %r11d
	jbe	.L1266
	orq	$16711680, %rax
.L1267:
	movswl	%r15w, %r8d
	cmpl	$254, %r15d
	jbe	.L1268
	movl	$4278190080, %r8d
	orq	%r8, %rax
.L1269:
	movswl	%di, %edi
	cmpl	$254, %edi
	jbe	.L1270
	movabsq	$1095216660480, %rdi
	orq	%rdi, %rax
.L1271:
	movswl	%r14w, %edi
	cmpl	$254, %r14d
	jbe	.L1272
	movabsq	$280375465082880, %rdi
	orq	%rdi, %rax
.L1273:
	movq	-224(%rbp), %rbx
	movswl	%bx, %edi
	cmpl	$254, %ebx
	jbe	.L1274
	movabsq	$71776119061217280, %rdi
	orq	%rdi, %rax
.L1275:
	movq	-240(%rbp), %rbx
	movswl	%bx, %edi
	cmpl	$254, %ebx
	jbe	.L1276
	movabsq	$-72057594037927936, %rdi
	orq	%rdi, %rax
.L1277:
	movswl	%si, %r8d
	movl	$255, %edi
	movq	%rax, -79(%rbp)
	cmpq	$255, %r8
	movq	%r8, %rsi
	movb	$5, -80(%rbp)
	movabsq	$-4278190081, %r8
	cmovge	%edi, %esi
	xorl	%ebx, %ebx
	movb	%sil, %bl
	movl	%r13d, %esi
	cmpq	$255, %rsi
	cmovge	%edi, %r13d
	movq	%r13, %rcx
	movb	%cl, %bh
	movq	-256(%rbp), %rcx
	movl	%ecx, %esi
	cmpq	$255, %rsi
	cmovge	%edi, %ecx
	andq	$-16711681, %rbx
	movzbl	%cl, %ecx
	salq	$16, %rcx
	movq	%rcx, %rsi
	movq	%rbx, %rcx
	movq	-272(%rbp), %rbx
	orq	%rsi, %rcx
	movl	%r10d, %esi
	cmpq	$255, %rsi
	movzbl	%r10b, %esi
	cmovge	%rdi, %rsi
	andq	%r8, %rcx
	salq	$24, %rsi
	orq	%rsi, %rcx
	movswl	%dx, %esi
	cmpq	$255, %rsi
	movq	%rsi, %rdx
	cmovge	%edi, %edx
	movzbl	%dl, %edx
	movq	%rdx, %rsi
	movabsq	$-1095216660481, %rdx
	salq	$32, %rsi
	andq	%rcx, %rdx
	movl	%r9d, %ecx
	orq	%rsi, %rdx
	cmpq	$255, %rcx
	movzbl	%r9b, %ecx
	movabsq	$-280375465082881, %rsi
	cmovge	%rdi, %rcx
	andq	%rsi, %rdx
	movabsq	$-71776119061217281, %rsi
	salq	$40, %rcx
	orq	%rcx, %rdx
	movl	%ebx, %ecx
	cmpq	$255, %rcx
	movzbl	%bl, %ecx
	movq	-288(%rbp), %rbx
	cmovge	%rdi, %rcx
	andq	%rsi, %rdx
	salq	$48, %rcx
	orq	%rcx, %rdx
	movl	%ebx, %ecx
	cmpq	$255, %rcx
	movabsq	$72057594037927935, %rcx
	cmovl	%ebx, %edi
	andq	%rcx, %rdx
	subq	$32, %rsp
	salq	$56, %rdi
	orq	%rdi, %rdx
	movq	%r12, %rdi
	movq	%rdx, -71(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm5
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L984:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-103(%rbp), %rdi
	movq	-143(%rbp), %r8
	movq	-135(%rbp), %r9
	movq	-111(%rbp), %rax
	movq	%rdi, %rcx
	movq	%r8, %r15
	movl	%edi, %ebx
	movl	%r8d, %r13d
	salq	$16, %rcx
	salq	$16, %r15
	movl	%eax, %r11d
	movq	%rax, %rdx
	sarq	$48, %rcx
	sarq	$48, %r15
	movq	%rax, %rsi
	movq	%rdi, %r10
	movq	%rcx, -224(%rbp)
	sarl	$16, %r11d
	movq	%r8, %rcx
	salq	$16, %rdx
	sarq	$48, %rcx
	movq	%r15, -240(%rbp)
	movq	%r9, %r15
	sarl	$16, %ebx
	movq	%rcx, -256(%rbp)
	movl	%r9d, %ecx
	salq	$16, %r15
	movl	%r11d, %r14d
	sarl	$16, %ecx
	sarl	$16, %r13d
	movw	%bx, -304(%rbp)
	movl	%ecx, -320(%rbp)
	sarq	$48, %r15
	sarq	$48, %rdx
	movw	%cx, -324(%rbp)
	sarq	$48, %rsi
	movq	%r9, %rcx
	sarq	$48, %r10
	sarq	$48, %rcx
	movw	%r13w, -322(%rbp)
	movq	%r15, -272(%rbp)
	movq	%rcx, -288(%rbp)
	cmpw	$126, %ax
	jle	.L1230
	movl	$127, %eax
.L1231:
	cmpw	$126, %r11w
	jle	.L1232
	movb	$127, %ah
.L1233:
	cmpw	$126, %dx
	jle	.L1234
	andq	$-16711681, %rax
	orq	$8323072, %rax
.L1235:
	cmpw	$126, %si
	jle	.L1236
	movabsq	$-4278190081, %rdx
	andq	%rdx, %rax
	orq	$2130706432, %rax
.L1237:
	cmpw	$126, %di
	jle	.L1238
	movabsq	$-1095216660481, %rdx
	andq	%rdx, %rax
	movabsq	$545460846592, %rdx
	orq	%rdx, %rax
.L1239:
	cmpw	$126, %bx
	jle	.L1240
	movabsq	$-280375465082881, %rdx
	andq	%rdx, %rax
	movabsq	$139637976727552, %rdx
	orq	%rdx, %rax
.L1241:
	cmpw	$126, -224(%rbp)
	jle	.L1242
	movabsq	$-71776119061217281, %rdx
	andq	%rdx, %rax
	movabsq	$35747322042253312, %rdx
	orq	%rdx, %rax
.L1243:
	cmpw	$126, %r10w
	jle	.L1244
	movabsq	$72057594037927935, %rdx
	andq	%rdx, %rax
	movabsq	$9151314442816847872, %rdx
	orq	%rdx, %rax
.L1245:
	cmpw	$126, %r8w
	jle	.L1246
	movl	$127, %ecx
.L1249:
	xorl	%edx, %edx
	movb	%cl, %dl
	movl	$127, %ecx
	cmpw	$126, %r13w
	jg	.L1248
	movswq	-322(%rbp), %rcx
	movq	$-128, %rsi
	cmpw	$-128, %cx
	cmovl	%rsi, %rcx
.L1248:
	cmpw	$126, -240(%rbp)
	movb	%cl, %dh
	movl	$127, %r15d
	jg	.L1251
	movq	-240(%rbp), %rbx
	movq	$-128, %r15
	cmpq	$-128, %rbx
	cmovge	%rbx, %r15
.L1251:
	movzbl	%r15b, %ecx
	andq	$-16711681, %rdx
	salq	$16, %rcx
	orq	%rcx, %rdx
	cmpw	$126, -256(%rbp)
	movl	$127, %ecx
	jg	.L1253
	movq	-256(%rbp), %rbx
	movq	$-128, %rcx
	cmpq	$-128, %rbx
	cmovge	%rbx, %rcx
.L1253:
	movabsq	$-4278190081, %rsi
	movzbl	%cl, %ecx
	andq	%rsi, %rdx
	salq	$24, %rcx
	orq	%rdx, %rcx
	movl	$127, %edx
	cmpw	$126, %r9w
	jg	.L1255
	movswq	%r9w, %rdx
	cmpw	$-128, %r9w
	movq	$-128, %rsi
	cmovl	%rsi, %rdx
.L1255:
	movabsq	$-1095216660481, %rsi
	movzbl	%dl, %edx
	andq	%rsi, %rcx
	salq	$32, %rdx
	orq	%rcx, %rdx
	cmpw	$126, -320(%rbp)
	movl	$127, %ecx
	jg	.L1257
	movswq	-324(%rbp), %rcx
	movq	$-128, %rsi
	cmpw	$-128, %cx
	cmovl	%rsi, %rcx
.L1257:
	movzbl	%cl, %ecx
	movl	$127, %r15d
	movabsq	$-280375465082881, %rsi
	salq	$40, %rcx
	andq	%rsi, %rdx
	orq	%rdx, %rcx
	cmpw	$126, -272(%rbp)
	jg	.L1259
	movq	-272(%rbp), %rbx
	movq	$-128, %r15
	cmpq	$-128, %rbx
	cmovge	%rbx, %r15
.L1259:
	movzbl	%r15b, %edx
	salq	$48, %rdx
	movq	%rdx, %r15
	movabsq	$-71776119061217281, %rdx
	andq	%rcx, %rdx
	movl	$127, %ecx
	orq	%r15, %rdx
	cmpw	$126, -288(%rbp)
	jg	.L1261
	movq	-288(%rbp), %rbx
	movq	$-128, %rcx
	cmpq	$-128, %rbx
	cmovge	%rbx, %rcx
.L1261:
	salq	$56, %rcx
	movq	%rax, -79(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movabsq	$72057594037927935, %rsi
	movb	$5, -80(%rbp)
	andq	%rsi, %rdx
	orq	%rcx, %rdx
	movq	%rdx, -71(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm7
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L985:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rsi
	movq	-103(%rbp), %rdx
	movq	%r12, %rdi
	movq	-143(%rbp), %rcx
	movq	-135(%rbp), %rax
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movd	%esi, %xmm1
	shrq	$32, %rsi
	movd	%esi, %xmm7
	addss	%xmm1, %xmm7
	movd	%edx, %xmm1
	shrq	$32, %rdx
	movaps	%xmm7, %xmm0
	movd	%edx, %xmm7
	addss	%xmm1, %xmm7
	movd	%ecx, %xmm1
	shrq	$32, %rcx
	movd	%ecx, %xmm5
	addss	%xmm1, %xmm5
	movd	%eax, %xmm1
	shrq	$32, %rax
	movd	%xmm7, %edx
	movd	%eax, %xmm7
	addss	%xmm1, %xmm7
	movq	%rdx, %rsi
	movd	%xmm0, %edx
	salq	$32, %rsi
	orq	%rsi, %rdx
	movd	%xmm7, %eax
	movq	%rdx, -79(%rbp)
	movq	%rax, %rcx
	movd	%xmm5, %eax
	salq	$32, %rcx
	orq	%rcx, %rax
	movq	%rax, -71(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm6
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L988:
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rdx
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	movdqa	.LC30(%rip), %xmm1
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movl	%edx, -160(%rbp)
	sarq	$32, %rdx
	movl	%eax, -152(%rbp)
	sarq	$32, %rax
	movl	%eax, -148(%rbp)
	movl	%edx, -156(%rbp)
	movdqa	-160(%rbp), %xmm0
	pand	%xmm0, %xmm1
	psrld	$16, %xmm0
	cvtdq2ps	%xmm0, %xmm0
	mulps	.LC45(%rip), %xmm0
	cvtdq2ps	%xmm1, %xmm1
	addps	%xmm1, %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm3
	movaps	%xmm0, -144(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm3, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L977:
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-103(%rbp), %rax
	movb	$5, -80(%rbp)
	movq	%r12, %rdi
	movq	$0, -224(%rbp)
	subq	$32, %rsp
	movq	$0, -216(%rbp)
	movzbl	%al, %edx
	movw	%dx, -224(%rbp)
	movl	%eax, %edx
	movdqa	-224(%rbp), %xmm0
	shrw	$8, %dx
	pinsrw	$1, %edx, %xmm0
	movl	%eax, %edx
	sall	$8, %edx
	shrl	$24, %edx
	pinsrw	$2, %edx, %xmm0
	movl	%eax, %edx
	sarl	$24, %edx
	movzbl	%dl, %edx
	pinsrw	$3, %edx, %xmm0
	movq	%rax, %rdx
	salq	$24, %rdx
	movaps	%xmm0, -224(%rbp)
	shrq	$56, %rdx
	movw	%dx, -216(%rbp)
	movq	%rax, %rdx
	movdqa	-224(%rbp), %xmm0
	salq	$16, %rdx
	shrq	$56, %rdx
	pinsrw	$5, %edx, %xmm0
	movq	%rax, %rdx
	shrq	$56, %rax
	salq	$8, %rdx
	shrq	$56, %rdx
	pinsrw	$6, %edx, %xmm0
	pinsrw	$7, %eax, %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm3
	movb	%al, 16(%rsp)
	movups	%xmm3, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L978:
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rax
	movb	$5, -80(%rbp)
	movq	%r12, %rdi
	movq	$0, -224(%rbp)
	subq	$32, %rsp
	movq	$0, -216(%rbp)
	movzbl	%al, %edx
	movw	%dx, -224(%rbp)
	movl	%eax, %edx
	movdqa	-224(%rbp), %xmm0
	shrw	$8, %dx
	pinsrw	$1, %edx, %xmm0
	movl	%eax, %edx
	sall	$8, %edx
	shrl	$24, %edx
	pinsrw	$2, %edx, %xmm0
	movl	%eax, %edx
	sarl	$24, %edx
	movzbl	%dl, %edx
	pinsrw	$3, %edx, %xmm0
	movq	%rax, %rdx
	salq	$24, %rdx
	movaps	%xmm0, -224(%rbp)
	shrq	$56, %rdx
	movw	%dx, -216(%rbp)
	movq	%rax, %rdx
	movdqa	-224(%rbp), %xmm0
	salq	$16, %rdx
	shrq	$56, %rdx
	pinsrw	$5, %edx, %xmm0
	movq	%rax, %rdx
	shrq	$56, %rax
	salq	$8, %rdx
	shrq	$56, %rdx
	pinsrw	$6, %edx, %xmm0
	pinsrw	$7, %eax, %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm6
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L979:
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-103(%rbp), %rax
	movb	$5, -80(%rbp)
	movq	%r12, %rdi
	movq	$0, -224(%rbp)
	subq	$32, %rsp
	movsbw	%al, %dx
	movl	%eax, %ebx
	movq	$0, -216(%rbp)
	sarl	$24, %ebx
	movw	%dx, -224(%rbp)
	movl	%eax, %edx
	movdqa	-224(%rbp), %xmm0
	sarw	$8, %dx
	pinsrw	$1, %edx, %xmm0
	movl	%eax, %edx
	sall	$8, %edx
	sarl	$24, %edx
	pinsrw	$2, %edx, %xmm0
	movq	%rax, %rdx
	pinsrw	$3, %ebx, %xmm0
	salq	$24, %rdx
	movaps	%xmm0, -224(%rbp)
	sarq	$56, %rdx
	movw	%dx, -216(%rbp)
	movq	%rax, %rdx
	movdqa	-224(%rbp), %xmm0
	salq	$16, %rdx
	sarq	$56, %rdx
	pinsrw	$5, %edx, %xmm0
	movq	%rax, %rdx
	sarq	$56, %rax
	salq	$8, %rdx
	sarq	$56, %rdx
	pinsrw	$6, %edx, %xmm0
	pinsrw	$7, %eax, %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm7
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1112:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rdx
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	pxor	%xmm1, %xmm1
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movl	%edx, %ecx
	movw	%dx, -192(%rbp)
	sarl	$16, %ecx
	movw	%ax, -184(%rbp)
	movw	%cx, -190(%rbp)
	movq	%rdx, %rcx
	sarq	$48, %rdx
	movw	%dx, -186(%rbp)
	movl	%eax, %edx
	salq	$16, %rcx
	sarl	$16, %edx
	sarq	$48, %rcx
	movw	%dx, -182(%rbp)
	movq	%rax, %rdx
	sarq	$48, %rax
	salq	$16, %rdx
	movw	%cx, -188(%rbp)
	sarq	$48, %rdx
	movw	%ax, -178(%rbp)
	movq	-135(%rbp), %rax
	movw	%dx, -180(%rbp)
	movq	-143(%rbp), %rdx
	movw	%ax, -168(%rbp)
	movl	%edx, %ecx
	movw	%dx, -176(%rbp)
	sarl	$16, %ecx
	movw	%cx, -174(%rbp)
	movq	%rdx, %rcx
	sarq	$48, %rdx
	movw	%dx, -170(%rbp)
	movl	%eax, %edx
	salq	$16, %rcx
	sarl	$16, %edx
	sarq	$48, %rcx
	movw	%dx, -166(%rbp)
	movq	%rax, %rdx
	sarq	$48, %rax
	salq	$16, %rdx
	movw	%ax, -162(%rbp)
	sarq	$48, %rdx
	movw	%cx, -172(%rbp)
	movw	%dx, -164(%rbp)
	movdqa	-176(%rbp), %xmm0
	pcmpeqw	-192(%rbp), %xmm0
	pandn	.LC43(%rip), %xmm0
	psubw	%xmm0, %xmm1
	movups	%xmm1, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm7
	movaps	%xmm1, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1113:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rdx
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	pxor	%xmm1, %xmm1
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movl	%edx, %ecx
	movw	%dx, -192(%rbp)
	sarl	$16, %ecx
	movw	%ax, -184(%rbp)
	movw	%cx, -190(%rbp)
	movq	%rdx, %rcx
	sarq	$48, %rdx
	movw	%dx, -186(%rbp)
	movl	%eax, %edx
	salq	$16, %rcx
	sarl	$16, %edx
	sarq	$48, %rcx
	movw	%dx, -182(%rbp)
	movq	%rax, %rdx
	sarq	$48, %rax
	salq	$16, %rdx
	movw	%cx, -188(%rbp)
	sarq	$48, %rdx
	movw	%ax, -178(%rbp)
	movq	-135(%rbp), %rax
	movw	%dx, -180(%rbp)
	movq	-143(%rbp), %rdx
	movw	%ax, -168(%rbp)
	movdqa	-192(%rbp), %xmm0
	movl	%edx, %ecx
	movw	%dx, -176(%rbp)
	sarl	$16, %ecx
	movw	%cx, -174(%rbp)
	movq	%rdx, %rcx
	sarq	$48, %rdx
	movw	%dx, -170(%rbp)
	movl	%eax, %edx
	salq	$16, %rcx
	sarl	$16, %edx
	sarq	$48, %rcx
	movw	%dx, -166(%rbp)
	movq	%rax, %rdx
	sarq	$48, %rax
	salq	$16, %rdx
	movw	%ax, -162(%rbp)
	sarq	$48, %rdx
	movw	%cx, -172(%rbp)
	movw	%dx, -164(%rbp)
	pcmpeqw	-176(%rbp), %xmm0
	pand	.LC43(%rip), %xmm0
	psubw	%xmm0, %xmm1
	movups	%xmm1, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm5
	movaps	%xmm1, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1114:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	pxor	%xmm1, %xmm1
	movb	$5, -80(%rbp)
	movq	%r12, %rdi
	movdqu	-111(%rbp), %xmm4
	movdqu	-143(%rbp), %xmm0
	subq	$32, %rsp
	movdqu	-111(%rbp), %xmm7
	movaps	%xmm0, -176(%rbp)
	psubusb	%xmm4, %xmm0
	pcmpeqb	%xmm1, %xmm0
	pand	.LC44(%rip), %xmm0
	movaps	%xmm7, -192(%rbp)
	psubb	%xmm0, %xmm1
	movups	%xmm1, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm6
	movaps	%xmm1, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1115:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	pxor	%xmm1, %xmm1
	movb	$5, -80(%rbp)
	movq	%r12, %rdi
	movdqu	-111(%rbp), %xmm4
	movdqu	-143(%rbp), %xmm0
	subq	$32, %rsp
	movdqu	-111(%rbp), %xmm7
	movaps	%xmm0, -176(%rbp)
	pcmpgtb	%xmm4, %xmm0
	pandn	.LC44(%rip), %xmm0
	movaps	%xmm7, -192(%rbp)
	psubb	%xmm0, %xmm1
	movups	%xmm1, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm6
	movaps	%xmm1, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1116:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	pxor	%xmm1, %xmm1
	movb	$5, -80(%rbp)
	movq	%r12, %rdi
	movdqu	-111(%rbp), %xmm0
	movdqu	-143(%rbp), %xmm6
	subq	$32, %rsp
	movdqu	-143(%rbp), %xmm4
	movaps	%xmm0, -192(%rbp)
	psubusb	%xmm6, %xmm0
	pcmpeqb	%xmm1, %xmm0
	pand	.LC44(%rip), %xmm0
	movaps	%xmm4, -176(%rbp)
	psubb	%xmm0, %xmm1
	movups	%xmm1, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm5
	movaps	%xmm1, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1117:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	pxor	%xmm1, %xmm1
	movb	$5, -80(%rbp)
	movq	%r12, %rdi
	movdqu	-111(%rbp), %xmm0
	movdqu	-143(%rbp), %xmm5
	subq	$32, %rsp
	movdqu	-143(%rbp), %xmm6
	movaps	%xmm0, -192(%rbp)
	pcmpgtb	%xmm5, %xmm0
	pandn	.LC44(%rip), %xmm0
	movaps	%xmm6, -176(%rbp)
	psubb	%xmm0, %xmm1
	movups	%xmm1, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm7
	movaps	%xmm1, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1118:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	pxor	%xmm1, %xmm1
	movb	$5, -80(%rbp)
	movq	%r12, %rdi
	movdqu	-111(%rbp), %xmm0
	movdqu	-143(%rbp), %xmm6
	subq	$32, %rsp
	movdqu	-143(%rbp), %xmm4
	movaps	%xmm0, -192(%rbp)
	psubusb	%xmm6, %xmm0
	pcmpeqb	%xmm1, %xmm0
	pandn	.LC44(%rip), %xmm0
	movaps	%xmm4, -176(%rbp)
	psubb	%xmm0, %xmm1
	movups	%xmm1, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm5
	movaps	%xmm1, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1119:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	pxor	%xmm1, %xmm1
	movb	$5, -80(%rbp)
	movq	%r12, %rdi
	movdqu	-111(%rbp), %xmm0
	movdqu	-143(%rbp), %xmm6
	subq	$32, %rsp
	movdqu	-143(%rbp), %xmm4
	movaps	%xmm0, -192(%rbp)
	pcmpgtb	%xmm6, %xmm0
	pand	.LC44(%rip), %xmm0
	movaps	%xmm4, -176(%rbp)
	psubb	%xmm0, %xmm1
	movups	%xmm1, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm5
	movaps	%xmm1, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1120:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	pxor	%xmm1, %xmm1
	movb	$5, -80(%rbp)
	movq	%r12, %rdi
	movdqu	-111(%rbp), %xmm7
	movdqu	-143(%rbp), %xmm0
	subq	$32, %rsp
	movdqu	-111(%rbp), %xmm5
	movaps	%xmm0, -176(%rbp)
	psubusb	%xmm7, %xmm0
	pcmpeqb	%xmm1, %xmm0
	pandn	.LC44(%rip), %xmm0
	movaps	%xmm5, -192(%rbp)
	psubb	%xmm0, %xmm1
	movups	%xmm1, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm3
	movaps	%xmm1, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm3, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1121:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	pxor	%xmm1, %xmm1
	movb	$5, -80(%rbp)
	movq	%r12, %rdi
	movdqu	-111(%rbp), %xmm7
	movdqu	-143(%rbp), %xmm0
	subq	$32, %rsp
	movdqu	-111(%rbp), %xmm5
	movaps	%xmm0, -176(%rbp)
	pcmpgtb	%xmm7, %xmm0
	pand	.LC44(%rip), %xmm0
	movaps	%xmm5, -192(%rbp)
	psubb	%xmm0, %xmm1
	movups	%xmm1, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm4
	movaps	%xmm1, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm4, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1122:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	pxor	%xmm1, %xmm1
	movb	$5, -80(%rbp)
	movq	%r12, %rdi
	movdqu	-111(%rbp), %xmm0
	movdqu	-143(%rbp), %xmm5
	subq	$32, %rsp
	movdqu	-111(%rbp), %xmm4
	movdqu	-143(%rbp), %xmm6
	pcmpeqb	%xmm5, %xmm0
	pandn	.LC44(%rip), %xmm0
	movaps	%xmm4, -192(%rbp)
	psubb	%xmm0, %xmm1
	movaps	%xmm6, -176(%rbp)
	movups	%xmm1, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm7
	movaps	%xmm1, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1123:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	pxor	%xmm1, %xmm1
	movb	$5, -80(%rbp)
	movq	%r12, %rdi
	movdqu	-111(%rbp), %xmm5
	movdqu	-143(%rbp), %xmm0
	subq	$32, %rsp
	movdqu	-111(%rbp), %xmm4
	movdqu	-143(%rbp), %xmm6
	pcmpeqb	%xmm5, %xmm0
	pand	.LC44(%rip), %xmm0
	movaps	%xmm4, -192(%rbp)
	psubb	%xmm0, %xmm1
	movaps	%xmm6, -176(%rbp)
	movups	%xmm1, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm7
	movaps	%xmm1, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1124:
	movq	64(%rcx), %rax
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	movzbl	2(%rax,%r8), %ebx
	addl	$1, (%r9)
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movupd	-111(%rbp), %xmm0
	movsd	-143(%rbp), %xmm7
	movaps	%xmm0, -160(%rbp)
	movsd	%xmm7, -160(%rbp,%rbx,8)
	movdqa	-160(%rbp), %xmm3
	movups	%xmm3, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm4
	movb	%al, 16(%rsp)
	movups	%xmm4, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1125:
	movq	64(%rcx), %rax
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	movzbl	2(%rax,%r8), %ebx
	addl	$1, (%r9)
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movb	$4, -80(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movupd	-111(%rbp), %xmm0
	movq	$0, -71(%rbp)
	movzbl	-64(%rbp), %eax
	movaps	%xmm0, -144(%rbp)
	movsd	-144(%rbp,%rbx,8), %xmm0
	movb	%al, 16(%rsp)
	movq	%xmm0, -79(%rbp)
	movdqa	-80(%rbp), %xmm7
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1126:
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movsd	-111(%rbp), %xmm0
	unpcklpd	%xmm0, %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm6
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1127:
	movq	64(%rcx), %rax
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	movzbl	2(%rax,%r8), %ebx
	addl	$1, (%r9)
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rax
	movq	-103(%rbp), %rdx
	movq	%r12, %rdi
	movss	-143(%rbp), %xmm7
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movd	%eax, %xmm0
	movd	%edx, %xmm1
	shrq	$32, %rax
	shrq	$32, %rdx
	movd	%eax, %xmm5
	movd	%edx, %xmm6
	unpcklps	%xmm5, %xmm0
	unpcklps	%xmm6, %xmm1
	movlhps	%xmm1, %xmm0
	movaps	%xmm0, -160(%rbp)
	movss	%xmm7, -160(%rbp,%rbx,4)
	movdqa	-160(%rbp), %xmm3
	movups	%xmm3, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm4
	movb	%al, 16(%rsp)
	movups	%xmm4, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1128:
	movq	64(%rcx), %rax
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	movzbl	2(%rax,%r8), %ebx
	addl	$1, (%r9)
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rax
	movq	-103(%rbp), %rdx
	movq	%r12, %rdi
	movb	$3, -80(%rbp)
	subq	$32, %rsp
	movd	%eax, %xmm0
	movd	%edx, %xmm1
	shrq	$32, %rax
	shrq	$32, %rdx
	movd	%eax, %xmm5
	movd	%edx, %xmm6
	unpcklps	%xmm5, %xmm0
	unpcklps	%xmm6, %xmm1
	movlhps	%xmm1, %xmm0
	movaps	%xmm0, -144(%rbp)
	movl	-144(%rbp,%rbx,4), %eax
	pxor	%xmm0, %xmm0
	movups	%xmm0, -79(%rbp)
	movl	%eax, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm7
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1129:
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movss	-111(%rbp), %xmm0
	shufps	$0, %xmm0, %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm7
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1130:
	movq	64(%rcx), %rax
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	movzbl	2(%rax,%r8), %ebx
	addl	$1, (%r9)
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rax
	movb	$5, -80(%rbp)
	movq	%r12, %rdi
	movq	-143(%rbp), %rdx
	subq	$32, %rsp
	movq	%rax, -160(%rbp)
	movq	-103(%rbp), %rax
	movq	%rax, -152(%rbp)
	movzbl	%bl, %eax
	movq	%rdx, -160(%rbp,%rax,8)
	movdqa	-160(%rbp), %xmm6
	movups	%xmm6, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm5
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1131:
	movq	64(%rcx), %rax
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	movzbl	2(%rax,%r8), %ebx
	addl	$1, (%r9)
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rax
	movb	$2, -80(%rbp)
	movq	%r12, %rdi
	movq	$0, -71(%rbp)
	subq	$32, %rsp
	movq	%rax, -144(%rbp)
	movq	-103(%rbp), %rax
	movq	%rax, -136(%rbp)
	movzbl	%bl, %eax
	movq	-144(%rbp,%rax,8), %rax
	movq	%rax, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm6
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1132:
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	-111(%rbp), %xmm0
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm6
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1133:
	movq	64(%rcx), %rax
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	movzbl	2(%rax,%r8), %ebx
	addl	$1, (%r9)
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-103(%rbp), %rax
	movq	-111(%rbp), %rdx
	movq	%r12, %rdi
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movl	%eax, -152(%rbp)
	sarq	$32, %rax
	movl	%eax, -148(%rbp)
	movl	-143(%rbp), %eax
	movl	%edx, -160(%rbp)
	sarq	$32, %rdx
	movl	%edx, -156(%rbp)
	movl	%eax, -160(%rbp,%rbx,4)
	movdqa	-160(%rbp), %xmm7
	movups	%xmm7, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm3
	movb	%al, 16(%rsp)
	movups	%xmm3, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1134:
	movq	64(%rcx), %rax
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	movzbl	2(%rax,%r8), %ebx
	addl	$1, (%r9)
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rdx
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	pxor	%xmm0, %xmm0
	movb	$1, -80(%rbp)
	subq	$32, %rsp
	movl	%edx, -144(%rbp)
	sarq	$32, %rdx
	movl	%eax, -136(%rbp)
	sarq	$32, %rax
	movl	%edx, -140(%rbp)
	movl	%eax, -132(%rbp)
	movl	-144(%rbp,%rbx,4), %eax
	movups	%xmm0, -79(%rbp)
	movl	%eax, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm5
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1135:
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movd	-111(%rbp), %xmm5
	pshufd	$0, %xmm5, %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm7
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1136:
	movq	64(%rcx), %rax
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	movzbl	2(%rax,%r8), %ebx
	addl	$1, (%r9)
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rdx
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movl	%edx, %ecx
	movw	%dx, -160(%rbp)
	sarl	$16, %ecx
	movw	%ax, -152(%rbp)
	movw	%cx, -158(%rbp)
	movq	%rdx, %rcx
	sarq	$48, %rdx
	movw	%dx, -154(%rbp)
	movl	%eax, %edx
	salq	$16, %rcx
	sarl	$16, %edx
	sarq	$48, %rcx
	movw	%dx, -150(%rbp)
	movq	%rax, %rdx
	sarq	$48, %rax
	movw	%ax, -146(%rbp)
	salq	$16, %rdx
	movl	-143(%rbp), %eax
	sarq	$48, %rdx
	movw	%cx, -156(%rbp)
	movw	%dx, -148(%rbp)
	movw	%ax, -160(%rbp,%rbx,2)
	movdqa	-160(%rbp), %xmm4
	movups	%xmm4, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm6
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1137:
	movq	64(%rcx), %rax
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	movzbl	2(%rax,%r8), %ebx
	addl	$1, (%r9)
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rdx
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	pxor	%xmm0, %xmm0
	movb	$1, -80(%rbp)
	subq	$32, %rsp
	movl	%edx, %ecx
	movw	%dx, -144(%rbp)
	sarl	$16, %ecx
	movw	%ax, -136(%rbp)
	movw	%cx, -142(%rbp)
	movq	%rdx, %rcx
	sarq	$48, %rdx
	movw	%dx, -138(%rbp)
	movl	%eax, %edx
	salq	$16, %rcx
	sarl	$16, %edx
	sarq	$48, %rcx
	movups	%xmm0, -79(%rbp)
	movw	%dx, -134(%rbp)
	movq	%rax, %rdx
	sarq	$48, %rax
	salq	$16, %rdx
	movw	%cx, -140(%rbp)
	sarq	$48, %rdx
	movw	%ax, -130(%rbp)
	movw	%dx, -132(%rbp)
	movswl	-144(%rbp,%rbx,2), %eax
	movl	%eax, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm7
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1138:
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movd	-111(%rbp), %xmm0
	movb	$5, -80(%rbp)
	movq	%r12, %rdi
	punpcklwd	%xmm0, %xmm0
	subq	$32, %rsp
	pshufd	$0, %xmm0, %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm6
	movaps	%xmm0, -144(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1139:
	movq	64(%rcx), %rax
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	movzbl	2(%rax,%r8), %ebx
	addl	$1, (%r9)
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movl	-143(%rbp), %eax
	movdqu	-111(%rbp), %xmm5
	movaps	%xmm5, -160(%rbp)
	movb	%al, -160(%rbp,%rbx)
	movdqa	-160(%rbp), %xmm7
	movups	%xmm7, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm3
	movb	%al, 16(%rsp)
	movups	%xmm3, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1140:
	movq	64(%rcx), %rax
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	movzbl	2(%rax,%r8), %ebx
	addl	$1, (%r9)
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	pxor	%xmm0, %xmm0
	movb	$1, -80(%rbp)
	movq	%r12, %rdi
	movdqu	-111(%rbp), %xmm6
	movups	%xmm0, -79(%rbp)
	subq	$32, %rsp
	movaps	%xmm6, -144(%rbp)
	movsbl	-144(%rbp,%rbx), %eax
	movl	%eax, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm5
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1067:
	leaq	-80(%rbp), %r13
	movq	%rdi, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-79(%rbp), %rdx
	movq	-71(%rbp), %rax
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	%edx, -176(%rbp)
	sarq	$32, %rdx
	movl	%eax, -168(%rbp)
	sarq	$32, %rax
	movl	%edx, -172(%rbp)
	movl	%eax, -164(%rbp)
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-79(%rbp), %rdx
	movq	-71(%rbp), %rax
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	%edx, -160(%rbp)
	sarq	$32, %rdx
	movl	%eax, -152(%rbp)
	sarq	$32, %rax
	movl	%edx, -156(%rbp)
	movl	%eax, -148(%rbp)
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-79(%rbp), %rdx
	movq	-71(%rbp), %rax
	movq	%r12, %rdi
	movdqa	-160(%rbp), %xmm0
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movl	%edx, -144(%rbp)
	sarq	$32, %rdx
	movl	%eax, -136(%rbp)
	sarq	$32, %rax
	movl	%eax, -132(%rbp)
	movl	%edx, -140(%rbp)
	pxor	-144(%rbp), %xmm0
	pand	-176(%rbp), %xmm0
	pxor	-160(%rbp), %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm3
	movaps	%xmm0, -112(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm3, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1068:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rdx
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movl	%edx, -192(%rbp)
	sarq	$32, %rdx
	movl	%eax, -184(%rbp)
	sarq	$32, %rax
	movl	%edx, -188(%rbp)
	movq	-143(%rbp), %rdx
	movl	%eax, -180(%rbp)
	movq	-135(%rbp), %rax
	movl	%edx, -176(%rbp)
	sarq	$32, %rdx
	movdqa	-192(%rbp), %xmm0
	movl	%eax, -168(%rbp)
	sarq	$32, %rax
	movl	%eax, -164(%rbp)
	movl	%edx, -172(%rbp)
	pxor	-176(%rbp), %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm7
	movaps	%xmm0, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1066:
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	pxor	%xmm3, %xmm3
	movb	$5, -80(%rbp)
	movq	%r12, %rdi
	movdqu	-111(%rbp), %xmm1
	movdqa	.LC34(%rip), %xmm0
	subq	$32, %rsp
	movdqu	-111(%rbp), %xmm4
	psubb	%xmm1, %xmm3
	pcmpeqb	%xmm0, %xmm1
	movdqa	%xmm0, %xmm2
	movaps	%xmm4, -160(%rbp)
	pand	%xmm1, %xmm2
	pandn	%xmm3, %xmm1
	movdqa	%xmm1, %xmm0
	por	%xmm2, %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm6
	movaps	%xmm0, -144(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L980:
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rax
	movb	$5, -80(%rbp)
	movq	%r12, %rdi
	movq	$0, -224(%rbp)
	subq	$32, %rsp
	movsbw	%al, %dx
	movl	%eax, %ebx
	movq	$0, -216(%rbp)
	sarl	$24, %ebx
	movw	%dx, -224(%rbp)
	movl	%eax, %edx
	movdqa	-224(%rbp), %xmm0
	sarw	$8, %dx
	pinsrw	$1, %edx, %xmm0
	movl	%eax, %edx
	sall	$8, %edx
	sarl	$24, %edx
	pinsrw	$2, %edx, %xmm0
	movq	%rax, %rdx
	pinsrw	$3, %ebx, %xmm0
	salq	$24, %rdx
	movaps	%xmm0, -224(%rbp)
	sarq	$56, %rdx
	movw	%dx, -216(%rbp)
	movq	%rax, %rdx
	movdqa	-224(%rbp), %xmm0
	salq	$16, %rdx
	sarq	$56, %rdx
	pinsrw	$5, %edx, %xmm0
	movq	%rax, %rdx
	sarq	$56, %rax
	salq	$8, %rdx
	sarq	$56, %rdx
	pinsrw	$6, %edx, %xmm0
	pinsrw	$7, %eax, %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm4
	movb	%al, 16(%rsp)
	movups	%xmm4, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1141:
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movzbl	-111(%rbp), %eax
	movb	$5, -80(%rbp)
	movq	%r12, %rdi
	movabsq	$72340172838076673, %rdx
	subq	$32, %rsp
	movq	%rax, %rcx
	imulq	%rdx, %rcx
	mulq	%rdx
	addq	%rcx, %rdx
	movq	%rax, -79(%rbp)
	movq	%rdx, -71(%rbp)
	movdqa	-80(%rbp), %xmm5
	movq	%rax, -144(%rbp)
	movzbl	-64(%rbp), %eax
	movups	%xmm5, (%rsp)
	movb	%al, 16(%rsp)
	movq	%rdx, -136(%rbp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1142:
	movq	64(%rcx), %rax
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %r13
	movq	%rdi, %rsi
	movaps	%xmm0, -160(%rbp)
	movq	%r13, %rdi
	movdqu	2(%rax,%r8), %xmm7
	addl	$16, (%r9)
	movaps	%xmm7, -160(%rbp)
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movdqu	-79(%rbp), %xmm3
	movq	%r12, %rsi
	movq	%r13, %rdi
	movaps	%xmm3, -144(%rbp)
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movdqu	-79(%rbp), %xmm4
	movzbl	-160(%rbp), %eax
	movaps	%xmm4, -112(%rbp)
	cmpl	$15, %eax
	jle	.L1294
	subl	$16, %eax
	cltq
	movzbl	-144(%rbp,%rax), %edx
.L1295:
	xorl	%eax, %eax
	movb	%dl, %al
	movzbl	-159(%rbp), %edx
	cmpl	$15, %edx
	jle	.L1296
	subl	$16, %edx
	movslq	%edx, %rdx
	movzbl	-144(%rbp,%rdx), %edx
.L1297:
	movb	%dl, %ah
	movzbl	-158(%rbp), %edx
	cmpl	$15, %edx
	jle	.L1298
	subl	$16, %edx
	movslq	%edx, %rdx
	movzbl	-144(%rbp,%rdx), %edx
.L1299:
	andq	$-16711681, %rax
	movq	%rdx, %rcx
	movq	%rax, %rdx
	movzbl	-157(%rbp), %eax
	salq	$16, %rcx
	orq	%rcx, %rdx
	cmpl	$15, %eax
	jle	.L1300
	subl	$16, %eax
	cltq
	movzbl	-144(%rbp,%rax), %eax
.L1301:
	movabsq	$-4278190081, %rcx
	salq	$24, %rax
	andq	%rcx, %rdx
	orq	%rax, %rdx
	movzbl	-156(%rbp), %eax
	cmpl	$15, %eax
	jle	.L1302
	subl	$16, %eax
	cltq
	movzbl	-144(%rbp,%rax), %eax
.L1303:
	movabsq	$-1095216660481, %rcx
	salq	$32, %rax
	andq	%rcx, %rdx
	orq	%rax, %rdx
	movzbl	-155(%rbp), %eax
	cmpl	$15, %eax
	jle	.L1304
	subl	$16, %eax
	cltq
	movzbl	-144(%rbp,%rax), %eax
.L1305:
	movabsq	$-280375465082881, %rcx
	salq	$40, %rax
	andq	%rcx, %rdx
	orq	%rax, %rdx
	movzbl	-154(%rbp), %eax
	cmpl	$15, %eax
	jle	.L1306
	subl	$16, %eax
	cltq
	movzbl	-144(%rbp,%rax), %eax
.L1307:
	movabsq	$-71776119061217281, %rcx
	salq	$48, %rax
	andq	%rcx, %rdx
	orq	%rax, %rdx
	movzbl	-153(%rbp), %eax
	cmpl	$15, %eax
	jle	.L1308
	subl	$16, %eax
	cltq
	movzbl	-144(%rbp,%rax), %eax
.L1309:
	movabsq	$72057594037927935, %rcx
	salq	$56, %rax
	andq	%rcx, %rdx
	orq	%rax, %rdx
	movzbl	-152(%rbp), %eax
	cmpl	$15, %eax
	jle	.L1310
	subl	$16, %eax
	cltq
	movzbl	-144(%rbp,%rax), %ecx
.L1311:
	xorl	%eax, %eax
	movb	%cl, %al
	movzbl	-151(%rbp), %ecx
	cmpl	$15, %ecx
	jle	.L1312
	subl	$16, %ecx
	movslq	%ecx, %rcx
	movzbl	-144(%rbp,%rcx), %ecx
.L1313:
	movb	%cl, %ah
	movzbl	-150(%rbp), %ecx
	cmpl	$15, %ecx
	jle	.L1314
	subl	$16, %ecx
	movslq	%ecx, %rcx
	movzbl	-144(%rbp,%rcx), %ecx
.L1315:
	salq	$16, %rcx
	andq	$-16711681, %rax
	orq	%rcx, %rax
	movzbl	-149(%rbp), %ecx
	cmpl	$15, %ecx
	jle	.L1316
	subl	$16, %ecx
	movslq	%ecx, %rcx
	movzbl	-144(%rbp,%rcx), %ecx
.L1317:
	movabsq	$-4278190081, %rsi
	salq	$24, %rcx
	andq	%rsi, %rax
	orq	%rcx, %rax
	movzbl	-148(%rbp), %ecx
	cmpl	$15, %ecx
	jle	.L1318
	subl	$16, %ecx
	movslq	%ecx, %rcx
	movzbl	-144(%rbp,%rcx), %ecx
.L1319:
	movabsq	$-1095216660481, %rsi
	salq	$32, %rcx
	andq	%rsi, %rax
	orq	%rcx, %rax
	movzbl	-147(%rbp), %ecx
	cmpl	$15, %ecx
	jle	.L1320
	subl	$16, %ecx
	movslq	%ecx, %rcx
	movzbl	-144(%rbp,%rcx), %ecx
.L1321:
	movabsq	$-280375465082881, %rsi
	salq	$40, %rcx
	andq	%rsi, %rax
	orq	%rcx, %rax
	movzbl	-146(%rbp), %ecx
	cmpl	$15, %ecx
	jle	.L1322
	subl	$16, %ecx
	movslq	%ecx, %rcx
	movzbl	-144(%rbp,%rcx), %ecx
.L1323:
	movabsq	$-71776119061217281, %rsi
	salq	$48, %rcx
	andq	%rsi, %rax
	orq	%rcx, %rax
	movzbl	-145(%rbp), %ecx
	cmpl	$15, %ecx
	jg	.L1324
	movzbl	-112(%rbp,%rcx), %ecx
.L1325:
	salq	$56, %rcx
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movabsq	$72057594037927935, %rsi
	movq	%rdx, -79(%rbp)
	andq	%rsi, %rax
	orq	%rcx, %rax
	movq	%rax, -71(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm6
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1143:
	movq	64(%rcx), %r8
	addq	%rbx, %r8
	movzbl	1(%r8), %eax
	testb	%al, %al
	js	.L1378
	movl	$1, -204(%rbp)
	movl	$2, %eax
	movl	$1, %r14d
.L1209:
	addq	%rax, %r8
	movl	$1, %r13d
	movzbl	(%r8), %eax
	movl	%eax, %r15d
	andl	$127, %r15d
	testb	%al, %al
	js	.L1379
.L1210:
	leaq	-80(%rbp), %rdi
	movq	%r12, %rsi
	movq	%r9, -272(%rbp)
	movq	%rdi, -224(%rbp)
	movq	%rcx, -256(%rbp)
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movdqu	-79(%rbp), %xmm4
	movq	-224(%rbp), %rdi
	movq	%r12, %rsi
	movaps	%xmm4, -240(%rbp)
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	addl	-79(%rbp), %r15d
	jc	.L1213
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	31(%rax), %rax
	cmpq	$15, %rax
	jbe	.L1213
	movl	%r15d, %edx
	subq	$16, %rax
	cmpq	%rax, %rdx
	ja	.L1213
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl16EffectiveAddressEj
	movq	-256(%rbp), %rcx
	movq	-272(%rbp), %r9
	testq	%rax, %rax
	je	.L1213
	movdqa	-240(%rbp), %xmm7
	addl	%r14d, %r13d
	movups	%xmm7, (%rax)
	addl	%r13d, (%r9)
	movzbl	_ZN2v88internal22FLAG_trace_wasm_memoryE(%rip), %eax
	testb	%al, %al
	jne	.L1380
	movl	$1, %eax
	jmp	.L970
.L1144:
	movq	64(%rcx), %rdx
	movl	$2, %eax
	movl	$1, %r13d
	addq	%r8, %rdx
	cmpb	$0, 1(%rdx)
	js	.L1381
.L1201:
	addq	%rdx, %rax
	movl	$1, %r14d
	movzbl	(%rax), %edx
	movl	%edx, %r15d
	andl	$127, %r15d
	testb	%dl, %dl
	js	.L1382
.L1202:
	leaq	-80(%rbp), %rdi
	movq	%r12, %rsi
	movq	%r9, -240(%rbp)
	movq	%rcx, -224(%rbp)
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	addl	-79(%rbp), %r15d
	jc	.L1205
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	31(%rax), %rax
	cmpq	$15, %rax
	jbe	.L1205
	movl	%r15d, %edx
	subq	$16, %rax
	cmpq	%rax, %rdx
	ja	.L1205
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl16EffectiveAddressEj
	movq	-224(%rbp), %rcx
	movq	-240(%rbp), %r9
	testq	%rax, %rax
	je	.L1205
	movdqu	(%rax), %xmm3
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	%r9, -240(%rbp)
	movups	%xmm3, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm4
	movq	%rcx, -224(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm4, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movq	-240(%rbp), %r9
	addq	$32, %rsp
	leal	0(%r13,%r14), %eax
	movq	-224(%rbp), %rcx
	addl	%eax, (%r9)
	movzbl	_ZN2v88internal22FLAG_trace_wasm_memoryE(%rip), %eax
	testb	%al, %al
	jne	.L1383
	movl	$1, %eax
	jmp	.L970
.L1101:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rdx
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	pxor	%xmm1, %xmm1
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movl	%edx, -192(%rbp)
	sarq	$32, %rdx
	movl	%eax, -184(%rbp)
	sarq	$32, %rax
	movl	%edx, -188(%rbp)
	movq	-143(%rbp), %rdx
	movl	%eax, -180(%rbp)
	movq	-135(%rbp), %rax
	movl	%edx, -176(%rbp)
	sarq	$32, %rdx
	movl	%eax, -168(%rbp)
	sarq	$32, %rax
	movl	%eax, -164(%rbp)
	movl	%edx, -172(%rbp)
	movdqa	-176(%rbp), %xmm0
	pcmpgtd	-192(%rbp), %xmm0
	pand	.LC42(%rip), %xmm0
	psubd	%xmm0, %xmm1
	movups	%xmm1, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm4
	movaps	%xmm1, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm4, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1102:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rdx
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	pxor	%xmm1, %xmm1
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movl	%edx, -192(%rbp)
	sarq	$32, %rdx
	movl	%eax, -184(%rbp)
	sarq	$32, %rax
	movl	%edx, -188(%rbp)
	movq	-143(%rbp), %rdx
	movl	%eax, -180(%rbp)
	movq	-135(%rbp), %rax
	movl	%edx, -176(%rbp)
	sarq	$32, %rdx
	movdqa	-192(%rbp), %xmm0
	movl	%eax, -168(%rbp)
	sarq	$32, %rax
	movl	%eax, -164(%rbp)
	movl	%edx, -172(%rbp)
	pcmpeqd	-176(%rbp), %xmm0
	pandn	.LC42(%rip), %xmm0
	psubd	%xmm0, %xmm1
	movups	%xmm1, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm6
	movaps	%xmm1, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1103:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rdx
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	pxor	%xmm1, %xmm1
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movl	%edx, -192(%rbp)
	sarq	$32, %rdx
	movl	%eax, -184(%rbp)
	sarq	$32, %rax
	movl	%edx, -188(%rbp)
	movq	-143(%rbp), %rdx
	movl	%eax, -180(%rbp)
	movq	-135(%rbp), %rax
	movl	%edx, -176(%rbp)
	sarq	$32, %rdx
	movl	%eax, -168(%rbp)
	sarq	$32, %rax
	movl	%eax, -164(%rbp)
	movl	%edx, -172(%rbp)
	movdqa	-176(%rbp), %xmm0
	pcmpeqd	-192(%rbp), %xmm0
	pand	.LC42(%rip), %xmm0
	psubd	%xmm0, %xmm1
	movups	%xmm1, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm4
	movaps	%xmm1, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm4, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L986:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-103(%rbp), %rax
	movq	-111(%rbp), %rdi
	movb	$5, -80(%rbp)
	movq	-135(%rbp), %rcx
	movq	-143(%rbp), %rsi
	subq	$32, %rsp
	movq	%rax, %rdx
	movq	%rdi, %r8
	sarq	$32, %rdx
	sarq	$32, %r8
	addl	%eax, %edx
	movq	%rdx, %rax
	leal	(%rdi,%r8), %edx
	movq	%rsi, %rdi
	salq	$32, %rax
	sarq	$32, %rdi
	orq	%rax, %rdx
	movq	%rcx, %rax
	sarq	$32, %rax
	movq	%rdx, -79(%rbp)
	addl	%ecx, %eax
	salq	$32, %rax
	movq	%rax, %rcx
	leal	(%rsi,%rdi), %eax
	movq	%r12, %rdi
	orq	%rcx, %rax
	movq	%rax, -71(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm4
	movb	%al, 16(%rsp)
	movups	%xmm4, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L987:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rax
	movq	-103(%rbp), %rdx
	movq	$0, -224(%rbp)
	movq	-143(%rbp), %rcx
	movq	-135(%rbp), %rsi
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movq	$0, -216(%rbp)
	movl	%eax, %edi
	sarl	$16, %edi
	addl	%eax, %edi
	movw	%di, -224(%rbp)
	movl	%ecx, %edi
	sarl	$16, %edi
	addl	%ecx, %edi
	movw	%di, -216(%rbp)
	movq	%rax, %rdi
	salq	$16, %rax
	movdqa	-224(%rbp), %xmm0
	sarq	$48, %rdi
	sarq	$48, %rax
	addl	%edi, %eax
	movq	%r12, %rdi
	pinsrw	$1, %eax, %xmm0
	movq	%rcx, %rax
	sarq	$48, %rcx
	salq	$16, %rax
	sarq	$48, %rax
	addl	%eax, %ecx
	movl	%edx, %eax
	sarl	$16, %eax
	pinsrw	$5, %ecx, %xmm0
	addl	%edx, %eax
	pinsrw	$2, %eax, %xmm0
	movl	%esi, %eax
	sarl	$16, %eax
	addl	%esi, %eax
	pinsrw	$6, %eax, %xmm0
	movq	%rdx, %rax
	sarq	$48, %rdx
	salq	$16, %rax
	sarq	$48, %rax
	addl	%eax, %edx
	movq	%rsi, %rax
	salq	$16, %rax
	pinsrw	$3, %edx, %xmm0
	sarq	$48, %rax
	movq	%rax, %rdx
	movq	%rsi, %rax
	sarq	$48, %rax
	addl	%edx, %eax
	pinsrw	$7, %eax, %xmm0
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm5
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1104:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rdx
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	pxor	%xmm1, %xmm1
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movl	%edx, %ecx
	movw	%dx, -192(%rbp)
	sarl	$16, %ecx
	movw	%ax, -184(%rbp)
	movw	%cx, -190(%rbp)
	movq	%rdx, %rcx
	sarq	$48, %rdx
	movw	%dx, -186(%rbp)
	movl	%eax, %edx
	salq	$16, %rcx
	sarl	$16, %edx
	sarq	$48, %rcx
	movw	%dx, -182(%rbp)
	movq	%rax, %rdx
	sarq	$48, %rax
	salq	$16, %rdx
	movw	%cx, -188(%rbp)
	sarq	$48, %rdx
	movw	%ax, -178(%rbp)
	movq	-135(%rbp), %rax
	movw	%dx, -180(%rbp)
	movq	-143(%rbp), %rdx
	movw	%ax, -168(%rbp)
	movl	%edx, %ecx
	movw	%dx, -176(%rbp)
	sarl	$16, %ecx
	movw	%cx, -174(%rbp)
	movq	%rdx, %rcx
	sarq	$48, %rdx
	movw	%dx, -170(%rbp)
	movl	%eax, %edx
	salq	$16, %rcx
	sarl	$16, %edx
	sarq	$48, %rcx
	movw	%dx, -166(%rbp)
	movq	%rax, %rdx
	sarq	$48, %rax
	salq	$16, %rdx
	movw	%ax, -162(%rbp)
	sarq	$48, %rdx
	movw	%cx, -172(%rbp)
	movw	%dx, -164(%rbp)
	movdqa	-176(%rbp), %xmm0
	psubusw	-192(%rbp), %xmm0
	pcmpeqw	%xmm1, %xmm0
	pand	.LC43(%rip), %xmm0
	psubw	%xmm0, %xmm1
	movups	%xmm1, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm6
	movaps	%xmm1, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1105:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rdx
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	pxor	%xmm1, %xmm1
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movl	%edx, %ecx
	movw	%dx, -192(%rbp)
	sarl	$16, %ecx
	movw	%ax, -184(%rbp)
	movw	%cx, -190(%rbp)
	movq	%rdx, %rcx
	sarq	$48, %rdx
	movw	%dx, -186(%rbp)
	movl	%eax, %edx
	salq	$16, %rcx
	sarl	$16, %edx
	sarq	$48, %rcx
	movw	%dx, -182(%rbp)
	movq	%rax, %rdx
	sarq	$48, %rax
	salq	$16, %rdx
	movw	%cx, -188(%rbp)
	sarq	$48, %rdx
	movw	%ax, -178(%rbp)
	movq	-135(%rbp), %rax
	movw	%dx, -180(%rbp)
	movq	-143(%rbp), %rdx
	movw	%ax, -168(%rbp)
	movl	%edx, %ecx
	movw	%dx, -176(%rbp)
	sarl	$16, %ecx
	movw	%cx, -174(%rbp)
	movq	%rdx, %rcx
	sarq	$48, %rdx
	movw	%dx, -170(%rbp)
	movl	%eax, %edx
	salq	$16, %rcx
	sarl	$16, %edx
	sarq	$48, %rcx
	movw	%dx, -166(%rbp)
	movq	%rax, %rdx
	sarq	$48, %rax
	salq	$16, %rdx
	movw	%ax, -162(%rbp)
	sarq	$48, %rdx
	movw	%cx, -172(%rbp)
	movw	%dx, -164(%rbp)
	movdqa	-176(%rbp), %xmm0
	pcmpgtw	-192(%rbp), %xmm0
	pandn	.LC43(%rip), %xmm0
	psubw	%xmm0, %xmm1
	movups	%xmm1, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm6
	movaps	%xmm1, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1106:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rdx
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	pxor	%xmm1, %xmm1
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movl	%edx, %ecx
	movw	%dx, -192(%rbp)
	sarl	$16, %ecx
	movw	%ax, -184(%rbp)
	movw	%cx, -190(%rbp)
	movq	%rdx, %rcx
	sarq	$48, %rdx
	movw	%dx, -186(%rbp)
	movl	%eax, %edx
	salq	$16, %rcx
	sarl	$16, %edx
	sarq	$48, %rcx
	movw	%dx, -182(%rbp)
	movq	%rax, %rdx
	sarq	$48, %rax
	salq	$16, %rdx
	movw	%cx, -188(%rbp)
	sarq	$48, %rdx
	movw	%ax, -178(%rbp)
	movq	-135(%rbp), %rax
	movw	%dx, -180(%rbp)
	movq	-143(%rbp), %rdx
	movw	%ax, -168(%rbp)
	movdqa	-192(%rbp), %xmm0
	movl	%edx, %ecx
	movw	%dx, -176(%rbp)
	sarl	$16, %ecx
	movw	%cx, -174(%rbp)
	movq	%rdx, %rcx
	sarq	$48, %rdx
	movw	%dx, -170(%rbp)
	movl	%eax, %edx
	salq	$16, %rcx
	sarl	$16, %edx
	sarq	$48, %rcx
	movw	%dx, -166(%rbp)
	movq	%rax, %rdx
	sarq	$48, %rax
	salq	$16, %rdx
	movw	%ax, -162(%rbp)
	sarq	$48, %rdx
	movw	%cx, -172(%rbp)
	movw	%dx, -164(%rbp)
	psubusw	-176(%rbp), %xmm0
	pcmpeqw	%xmm1, %xmm0
	pand	.LC43(%rip), %xmm0
	psubw	%xmm0, %xmm1
	movups	%xmm1, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm7
	movaps	%xmm1, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1107:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rdx
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	pxor	%xmm1, %xmm1
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movl	%edx, %ecx
	movw	%dx, -192(%rbp)
	sarl	$16, %ecx
	movw	%ax, -184(%rbp)
	movw	%cx, -190(%rbp)
	movq	%rdx, %rcx
	sarq	$48, %rdx
	movw	%dx, -186(%rbp)
	movl	%eax, %edx
	salq	$16, %rcx
	sarl	$16, %edx
	sarq	$48, %rcx
	movw	%dx, -182(%rbp)
	movq	%rax, %rdx
	sarq	$48, %rax
	salq	$16, %rdx
	movw	%cx, -188(%rbp)
	sarq	$48, %rdx
	movw	%ax, -178(%rbp)
	movq	-135(%rbp), %rax
	movw	%dx, -180(%rbp)
	movq	-143(%rbp), %rdx
	movw	%ax, -168(%rbp)
	movdqa	-192(%rbp), %xmm0
	movl	%edx, %ecx
	movw	%dx, -176(%rbp)
	sarl	$16, %ecx
	movw	%cx, -174(%rbp)
	movq	%rdx, %rcx
	sarq	$48, %rdx
	movw	%dx, -170(%rbp)
	movl	%eax, %edx
	salq	$16, %rcx
	sarl	$16, %edx
	sarq	$48, %rcx
	movw	%dx, -166(%rbp)
	movq	%rax, %rdx
	sarq	$48, %rax
	salq	$16, %rdx
	movw	%ax, -162(%rbp)
	sarq	$48, %rdx
	movw	%cx, -172(%rbp)
	movw	%dx, -164(%rbp)
	pcmpgtw	-176(%rbp), %xmm0
	pandn	.LC43(%rip), %xmm0
	psubw	%xmm0, %xmm1
	movups	%xmm1, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm7
	movaps	%xmm1, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1108:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rdx
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	pxor	%xmm1, %xmm1
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movl	%edx, %ecx
	movw	%dx, -192(%rbp)
	sarl	$16, %ecx
	movw	%ax, -184(%rbp)
	movw	%cx, -190(%rbp)
	movq	%rdx, %rcx
	sarq	$48, %rdx
	movw	%dx, -186(%rbp)
	movl	%eax, %edx
	salq	$16, %rcx
	sarl	$16, %edx
	sarq	$48, %rcx
	movw	%dx, -182(%rbp)
	movq	%rax, %rdx
	sarq	$48, %rax
	salq	$16, %rdx
	movw	%cx, -188(%rbp)
	sarq	$48, %rdx
	movw	%ax, -178(%rbp)
	movq	-135(%rbp), %rax
	movw	%dx, -180(%rbp)
	movq	-143(%rbp), %rdx
	movw	%ax, -168(%rbp)
	movdqa	-192(%rbp), %xmm0
	movl	%edx, %ecx
	movw	%dx, -176(%rbp)
	sarl	$16, %ecx
	movw	%cx, -174(%rbp)
	movq	%rdx, %rcx
	sarq	$48, %rdx
	movw	%dx, -170(%rbp)
	movl	%eax, %edx
	salq	$16, %rcx
	sarl	$16, %edx
	sarq	$48, %rcx
	movw	%dx, -166(%rbp)
	movq	%rax, %rdx
	sarq	$48, %rax
	salq	$16, %rdx
	movw	%ax, -162(%rbp)
	sarq	$48, %rdx
	movw	%cx, -172(%rbp)
	movw	%dx, -164(%rbp)
	psubusw	-176(%rbp), %xmm0
	pcmpeqw	%xmm1, %xmm0
	pandn	.LC43(%rip), %xmm0
	psubw	%xmm0, %xmm1
	movups	%xmm1, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm4
	movaps	%xmm1, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm4, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1109:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rdx
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	pxor	%xmm1, %xmm1
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movl	%edx, %ecx
	movw	%dx, -192(%rbp)
	sarl	$16, %ecx
	movw	%ax, -184(%rbp)
	movw	%cx, -190(%rbp)
	movq	%rdx, %rcx
	sarq	$48, %rdx
	movw	%dx, -186(%rbp)
	movl	%eax, %edx
	salq	$16, %rcx
	sarl	$16, %edx
	sarq	$48, %rcx
	movw	%dx, -182(%rbp)
	movq	%rax, %rdx
	sarq	$48, %rax
	salq	$16, %rdx
	movw	%cx, -188(%rbp)
	sarq	$48, %rdx
	movw	%ax, -178(%rbp)
	movq	-135(%rbp), %rax
	movw	%dx, -180(%rbp)
	movq	-143(%rbp), %rdx
	movw	%ax, -168(%rbp)
	movdqa	-192(%rbp), %xmm0
	movl	%edx, %ecx
	movw	%dx, -176(%rbp)
	sarl	$16, %ecx
	movw	%cx, -174(%rbp)
	movq	%rdx, %rcx
	sarq	$48, %rdx
	movw	%dx, -170(%rbp)
	movl	%eax, %edx
	salq	$16, %rcx
	sarl	$16, %edx
	sarq	$48, %rcx
	movw	%dx, -166(%rbp)
	movq	%rax, %rdx
	sarq	$48, %rax
	salq	$16, %rdx
	movw	%ax, -162(%rbp)
	sarq	$48, %rdx
	movw	%cx, -172(%rbp)
	movw	%dx, -164(%rbp)
	pcmpgtw	-176(%rbp), %xmm0
	pand	.LC43(%rip), %xmm0
	psubw	%xmm0, %xmm1
	movups	%xmm1, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm4
	movaps	%xmm1, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm4, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1110:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rdx
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	pxor	%xmm1, %xmm1
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movl	%edx, %ecx
	movw	%dx, -192(%rbp)
	sarl	$16, %ecx
	movw	%ax, -184(%rbp)
	movw	%cx, -190(%rbp)
	movq	%rdx, %rcx
	sarq	$48, %rdx
	movw	%dx, -186(%rbp)
	movl	%eax, %edx
	salq	$16, %rcx
	sarl	$16, %edx
	sarq	$48, %rcx
	movw	%dx, -182(%rbp)
	movq	%rax, %rdx
	sarq	$48, %rax
	salq	$16, %rdx
	movw	%cx, -188(%rbp)
	sarq	$48, %rdx
	movw	%ax, -178(%rbp)
	movq	-135(%rbp), %rax
	movw	%dx, -180(%rbp)
	movq	-143(%rbp), %rdx
	movw	%ax, -168(%rbp)
	movl	%edx, %ecx
	movw	%dx, -176(%rbp)
	sarl	$16, %ecx
	movw	%cx, -174(%rbp)
	movq	%rdx, %rcx
	sarq	$48, %rdx
	movw	%dx, -170(%rbp)
	movl	%eax, %edx
	salq	$16, %rcx
	sarl	$16, %edx
	sarq	$48, %rcx
	movw	%dx, -166(%rbp)
	movq	%rax, %rdx
	sarq	$48, %rax
	salq	$16, %rdx
	movw	%ax, -162(%rbp)
	sarq	$48, %rdx
	movw	%cx, -172(%rbp)
	movw	%dx, -164(%rbp)
	movdqa	-176(%rbp), %xmm0
	psubusw	-192(%rbp), %xmm0
	pcmpeqw	%xmm1, %xmm0
	pandn	.LC43(%rip), %xmm0
	psubw	%xmm0, %xmm1
	movups	%xmm1, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm5
	movaps	%xmm1, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1111:
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-111(%rbp), %rdx
	movq	-103(%rbp), %rax
	movq	%r12, %rdi
	pxor	%xmm1, %xmm1
	movb	$5, -80(%rbp)
	subq	$32, %rsp
	movl	%edx, %ecx
	movw	%dx, -192(%rbp)
	sarl	$16, %ecx
	movw	%ax, -184(%rbp)
	movw	%cx, -190(%rbp)
	movq	%rdx, %rcx
	sarq	$48, %rdx
	movw	%dx, -186(%rbp)
	movl	%eax, %edx
	salq	$16, %rcx
	sarl	$16, %edx
	sarq	$48, %rcx
	movw	%dx, -182(%rbp)
	movq	%rax, %rdx
	sarq	$48, %rax
	salq	$16, %rdx
	movw	%cx, -188(%rbp)
	sarq	$48, %rdx
	movw	%ax, -178(%rbp)
	movq	-135(%rbp), %rax
	movw	%dx, -180(%rbp)
	movq	-143(%rbp), %rdx
	movw	%ax, -168(%rbp)
	movl	%edx, %ecx
	movw	%dx, -176(%rbp)
	sarl	$16, %ecx
	movw	%cx, -174(%rbp)
	movq	%rdx, %rcx
	sarq	$48, %rdx
	movw	%dx, -170(%rbp)
	movl	%eax, %edx
	salq	$16, %rcx
	sarl	$16, %edx
	sarq	$48, %rcx
	movw	%dx, -166(%rbp)
	movq	%rax, %rdx
	sarq	$48, %rax
	salq	$16, %rdx
	movw	%ax, -162(%rbp)
	sarq	$48, %rdx
	movw	%cx, -172(%rbp)
	movw	%dx, -164(%rbp)
	movdqa	-176(%rbp), %xmm0
	pcmpgtw	-192(%rbp), %xmm0
	pand	.LC43(%rip), %xmm0
	psubw	%xmm0, %xmm1
	movups	%xmm1, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm5
	movaps	%xmm1, -160(%rbp)
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L970
.L1232:
	movswq	%r14w, %r11
	cmpw	$-128, %r14w
	movq	$-128, %r14
	cmovl	%r14, %r11
	movq	%r11, %rcx
	movb	%cl, %ah
	jmp	.L1233
.L1234:
	cmpq	$-128, %rdx
	movq	$-128, %r11
	cmovl	%r11, %rdx
	andq	$-16711681, %rax
	movzbl	%dl, %edx
	salq	$16, %rdx
	orq	%rdx, %rax
	jmp	.L1235
.L1236:
	cmpq	$-128, %rsi
	movq	$-128, %rdx
	cmovge	%rsi, %rdx
	movabsq	$-4278190081, %rsi
	andq	%rsi, %rax
	movzbl	%dl, %edx
	salq	$24, %rdx
	orq	%rdx, %rax
	jmp	.L1237
.L1238:
	cmpw	$-128, %di
	movq	$-128, %rsi
	movswq	%di, %rdx
	cmovl	%rsi, %rdx
	movabsq	$-1095216660481, %rsi
	andq	%rsi, %rax
	movzbl	%dl, %edx
	salq	$32, %rdx
	orq	%rdx, %rax
	jmp	.L1239
.L1213:
	movl	$4, 88(%r12)
	movq	72(%r12), %rax
	movl	$1, 104(%r12)
	movq	%rbx, -16(%rax)
	xorl	%eax, %eax
	jmp	.L970
.L1308:
	movzbl	-112(%rbp,%rax), %eax
	jmp	.L1309
.L1306:
	movzbl	-112(%rbp,%rax), %eax
	jmp	.L1307
.L1304:
	movzbl	-112(%rbp,%rax), %eax
	jmp	.L1305
.L1302:
	movzbl	-112(%rbp,%rax), %eax
	jmp	.L1303
.L1300:
	movzbl	-112(%rbp,%rax), %eax
	jmp	.L1301
.L1298:
	movzbl	-112(%rbp,%rdx), %edx
	jmp	.L1299
.L1296:
	movzbl	-112(%rbp,%rdx), %edx
	jmp	.L1297
.L1294:
	movzbl	-112(%rbp,%rax), %edx
	jmp	.L1295
.L1324:
	subl	$16, %ecx
	movslq	%ecx, %rcx
	movzbl	-144(%rbp,%rcx), %ecx
	jmp	.L1325
.L1322:
	movzbl	-112(%rbp,%rcx), %ecx
	jmp	.L1323
.L1320:
	movzbl	-112(%rbp,%rcx), %ecx
	jmp	.L1321
.L1318:
	movzbl	-112(%rbp,%rcx), %ecx
	jmp	.L1319
.L1316:
	movzbl	-112(%rbp,%rcx), %ecx
	jmp	.L1317
.L1314:
	movzbl	-112(%rbp,%rcx), %ecx
	jmp	.L1315
.L1312:
	movzbl	-112(%rbp,%rcx), %ecx
	jmp	.L1313
.L1310:
	movzbl	-112(%rbp,%rax), %ecx
	jmp	.L1311
.L1240:
	movswq	-304(%rbp), %rdx
	movq	$-128, %rsi
	cmpw	$-128, %dx
	cmovl	%rsi, %rdx
	movabsq	$-280375465082881, %rsi
	andq	%rsi, %rax
	movzbl	%dl, %edx
	salq	$40, %rdx
	orq	%rdx, %rax
	jmp	.L1241
.L1242:
	movq	-224(%rbp), %rbx
	movq	$-128, %rdx
	movabsq	$-71776119061217281, %rcx
	cmpq	$-128, %rbx
	cmovge	%rbx, %rdx
	andq	%rcx, %rax
	movzbl	%dl, %edx
	salq	$48, %rdx
	orq	%rdx, %rax
	jmp	.L1243
.L1244:
	cmpq	$-128, %r10
	movq	$-128, %rdx
	cmovl	%rdx, %r10
	movabsq	$72057594037927935, %rdx
	andq	%rdx, %rax
	salq	$56, %r10
	orq	%r10, %rax
	jmp	.L1245
.L1230:
	cmpw	$-128, %ax
	movswq	%ax, %r15
	movq	$-128, %rax
	cmovl	%rax, %r15
	xorl	%eax, %eax
	movb	%r15b, %al
	jmp	.L1231
.L1145:
	divsd	%xmm2, %xmm0
	jmp	.L1146
.L1361:
	movss	.LC41(%rip), %xmm0
	divss	%xmm2, %xmm0
	jmp	.L1176
.L1360:
	divsd	%xmm2, %xmm0
	jmp	.L1150
.L1172:
	movss	.LC41(%rip), %xmm0
	divss	%xmm2, %xmm0
	jmp	.L1173
.L1170:
	movss	.LC41(%rip), %xmm0
	divss	%xmm2, %xmm0
	jmp	.L1171
.L1168:
	movss	.LC41(%rip), %xmm0
	divss	%xmm2, %xmm0
	jmp	.L1169
.L1205:
	movl	$4, 88(%r12)
	movq	72(%r12), %rax
	movl	$1, 104(%r12)
	movq	%rbx, -16(%rax)
	xorl	%eax, %eax
	jmp	.L970
.L1328:
	movsd	.LC20(%rip), %xmm0
	jmp	.L1146
.L1330:
	movsd	.LC20(%rip), %xmm0
	jmp	.L1150
.L1362:
	movaps	%xmm0, %xmm5
	ucomiss	%xmm0, %xmm1
	sqrtss	%xmm5, %xmm5
	movss	%xmm5, -224(%rbp)
	ja	.L1384
.L1197:
	movss	.LC41(%rip), %xmm0
	divss	-224(%rbp), %xmm0
	jmp	.L1198
.L1189:
	movaps	%xmm0, %xmm6
	ucomiss	%xmm0, %xmm1
	sqrtss	%xmm6, %xmm6
	movss	%xmm6, -224(%rbp)
	ja	.L1374
.L1194:
	movss	.LC41(%rip), %xmm0
	divss	-224(%rbp), %xmm0
	jmp	.L1191
.L1183:
	movaps	%xmm0, %xmm4
	ucomiss	%xmm0, %xmm1
	sqrtss	%xmm4, %xmm4
	movss	%xmm4, -224(%rbp)
	ja	.L1373
.L1188:
	movss	.LC41(%rip), %xmm0
	divss	-224(%rbp), %xmm0
	jmp	.L1185
.L1177:
	movaps	%xmm0, %xmm3
	ucomiss	%xmm0, %xmm1
	sqrtss	%xmm3, %xmm3
	movss	%xmm3, -224(%rbp)
	ja	.L1372
.L1182:
	movss	.LC41(%rip), %xmm0
	divss	-224(%rbp), %xmm0
	jmp	.L1179
.L1379:
	movl	%r15d, %edx
	leaq	-200(%rbp), %rsi
	leaq	1(%r8), %rdi
	movq	%r9, -240(%rbp)
	movq	%rcx, -224(%rbp)
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE0ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi1EEET_PKhPjPKcS7_.isra.0
	movl	-204(%rbp), %r14d
	movl	-200(%rbp), %r13d
	movq	-240(%rbp), %r9
	movq	-224(%rbp), %rcx
	movl	%eax, %r15d
	jmp	.L1210
.L1382:
	movzbl	1(%rax), %esi
	movl	$2, %r14d
	movl	%esi, %edx
	sall	$7, %edx
	andl	$16256, %edx
	orl	%edx, %r15d
	testb	%sil, %sil
	jns	.L1202
	movzbl	2(%rax), %esi
	movl	$3, %r14d
	movl	%esi, %edx
	sall	$14, %edx
	andl	$2080768, %edx
	orl	%edx, %r15d
	testb	%sil, %sil
	jns	.L1202
	movzbl	3(%rax), %esi
	movl	$4, %r14d
	movl	%esi, %edx
	sall	$21, %edx
	andl	$266338304, %edx
	orl	%edx, %r15d
	testb	%sil, %sil
	jns	.L1202
	movzbl	4(%rax), %eax
	movl	$5, %r14d
	sall	$28, %eax
	orl	%eax, %r15d
	jmp	.L1202
	.p2align 4,,10
	.p2align 3
.L1381:
	cmpb	$0, 2(%rdx)
	movl	$3, %eax
	movl	$2, %r13d
	jns	.L1201
	cmpb	$0, 3(%rdx)
	js	.L1385
	movl	$4, %eax
	movl	$3, %r13d
	jmp	.L1201
.L1378:
	movl	%eax, %edx
	leaq	-204(%rbp), %rsi
	leaq	2(%r8), %rdi
	movq	%r9, -240(%rbp)
	andl	$127, %edx
	movq	%rcx, -224(%rbp)
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE0ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi1EEET_PKhPjPKcS7_.isra.0
	movl	-204(%rbp), %eax
	movq	-224(%rbp), %rcx
	movq	-240(%rbp), %r9
	movq	%rax, %r14
	addq	$1, %rax
	jmp	.L1209
.L1383:
	movq	16(%r12), %rdx
	movb	%al, -224(%rbp)
	movl	$3584, %eax
	leaq	-200(%rbp), %rsi
	movw	%ax, -196(%rbp)
	movl	%r15d, -200(%rbp)
	movq	(%rdx), %rdi
	movq	(%rcx), %rdx
	movl	%ebx, %ecx
	movq	23(%rdi), %r8
	movl	$1, %edi
	movl	8(%rdx), %edx
	call	_ZN2v88internal4wasm20TraceMemoryOperationENS1_13ExecutionTierEPKNS1_17MemoryTracingInfoEiiPh@PLT
	movzbl	-224(%rbp), %eax
	jmp	.L970
.L1246:
	movswq	%r8w, %rcx
	cmpw	$-128, %r8w
	movq	$-128, %rdx
	cmovl	%rdx, %rcx
	jmp	.L1249
.L1380:
	movq	16(%r12), %rdx
	movl	%r15d, -200(%rbp)
	leaq	-200(%rbp), %rsi
	movw	$3585, -196(%rbp)
	movq	(%rdx), %rdi
	movq	(%rcx), %rdx
	movl	%ebx, %ecx
	movb	%al, -224(%rbp)
	movq	23(%rdi), %r8
	movl	8(%rdx), %edx
	movl	$1, %edi
	call	_ZN2v88internal4wasm20TraceMemoryOperationENS1_13ExecutionTierEPKNS1_17MemoryTracingInfoEiiPh@PLT
	movzbl	-224(%rbp), %eax
	jmp	.L970
.L1218:
	movdqa	-224(%rbp), %xmm7
	pinsrw	$1, %edi, %xmm7
	movaps	%xmm7, -224(%rbp)
	jmp	.L1219
.L1266:
	movzbl	%r11b, %r11d
	andq	$-16711681, %rax
	salq	$16, %r11
	orq	%r11, %rax
	jmp	.L1267
.L1268:
	movabsq	$-4278190081, %r11
	movzbl	%r8b, %r8d
	salq	$24, %r8
	andq	%r11, %rax
	orq	%r8, %rax
	jmp	.L1269
.L1220:
	movdqa	-224(%rbp), %xmm3
	pinsrw	$2, -240(%rbp), %xmm3
	movaps	%xmm3, -224(%rbp)
	jmp	.L1221
.L1270:
	movabsq	$-1095216660481, %r8
	movzbl	%dil, %edi
	salq	$32, %rdi
	andq	%r8, %rax
	orq	%rdi, %rax
	jmp	.L1271
.L1222:
	movdqa	-224(%rbp), %xmm4
	pinsrw	$3, -256(%rbp), %xmm4
	movaps	%xmm4, -224(%rbp)
	jmp	.L1223
.L1272:
	movabsq	$-280375465082881, %r8
	movzbl	%dil, %edi
	salq	$40, %rdi
	andq	%r8, %rax
	orq	%rdi, %rax
	jmp	.L1273
.L1274:
	movabsq	$-71776119061217281, %r8
	movzbl	%dil, %edi
	salq	$48, %rdi
	andq	%r8, %rax
	orq	%rdi, %rax
	jmp	.L1275
.L1276:
	movabsq	$72057594037927935, %r8
	salq	$56, %rdi
	andq	%r8, %rax
	orq	%rdi, %rax
	jmp	.L1277
.L1377:
	call	__stack_chk_fail@PLT
.L1334:
	movapd	%xmm1, %xmm0
	jmp	.L1154
.L1335:
	movapd	%xmm1, %xmm0
	jmp	.L1155
.L1339:
	movd	%esi, %xmm0
	jmp	.L1159
.L1332:
	movapd	%xmm1, %xmm0
	jmp	.L1152
.L1338:
	movd	%esi, %xmm0
	jmp	.L1158
.L1337:
	movd	%eax, %xmm0
	jmp	.L1157
.L1336:
	movd	%eax, %xmm0
	jmp	.L1156
.L1333:
	movapd	%xmm1, %xmm0
	jmp	.L1153
.L1340:
	movd	%eax, %xmm0
	jmp	.L1160
.L1341:
	movd	%eax, %xmm0
	jmp	.L1161
.L1342:
	movd	%ecx, %xmm0
	jmp	.L1162
.L1343:
	movd	%ecx, %xmm0
	jmp	.L1163
.L1385:
	movsbq	4(%rdx), %rax
	movsbl	4(%rdx), %r13d
	sarq	$63, %rax
	sarl	$31, %r13d
	notq	%rax
	notl	%r13d
	addq	$6, %rax
	addl	$5, %r13d
	jmp	.L1201
.L1384:
	call	sqrtf@PLT
	jmp	.L1197
.L1372:
	movss	%xmm1, -240(%rbp)
	call	sqrtf@PLT
	movss	-240(%rbp), %xmm1
	jmp	.L1182
.L1374:
	movss	%xmm1, -240(%rbp)
	call	sqrtf@PLT
	movss	-240(%rbp), %xmm1
	jmp	.L1194
.L1373:
	movss	%xmm1, -240(%rbp)
	call	sqrtf@PLT
	movss	-240(%rbp), %xmm1
	jmp	.L1188
	.cfi_endproc
.LFE20253:
	.size	_ZN2v88internal4wasm10ThreadImpl13ExecuteSimdOpENS1_10WasmOpcodeEPNS1_7DecoderEPNS1_15InterpreterCodeEmPi, .-_ZN2v88internal4wasm10ThreadImpl13ExecuteSimdOpENS1_10WasmOpcodeEPNS1_7DecoderEPNS1_15InterpreterCodeEmPi
	.section	.text._ZN2v88internal4wasm15WasmInterpreter6Thread5stateEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15WasmInterpreter6Thread5stateEv
	.type	_ZN2v88internal4wasm15WasmInterpreter6Thread5stateEv, @function
_ZN2v88internal4wasm15WasmInterpreter6Thread5stateEv:
.LFB20303:
	.cfi_startproc
	endbr64
	movl	88(%rdi), %eax
	ret
	.cfi_endproc
.LFE20303:
	.size	_ZN2v88internal4wasm15WasmInterpreter6Thread5stateEv, .-_ZN2v88internal4wasm15WasmInterpreter6Thread5stateEv
	.section	.text._ZN2v88internal4wasm15WasmInterpreter6Thread5PauseEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15WasmInterpreter6Thread5PauseEv
	.type	_ZN2v88internal4wasm15WasmInterpreter6Thread5PauseEv, @function
_ZN2v88internal4wasm15WasmInterpreter6Thread5PauseEv:
.LFB20306:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC7(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20306:
	.size	_ZN2v88internal4wasm15WasmInterpreter6Thread5PauseEv, .-_ZN2v88internal4wasm15WasmInterpreter6Thread5PauseEv
	.section	.text._ZN2v88internal4wasm15WasmInterpreter6Thread5ResetEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15WasmInterpreter6Thread5ResetEv
	.type	_ZN2v88internal4wasm15WasmInterpreter6Thread5ResetEv, @function
_ZN2v88internal4wasm15WasmInterpreter6Thread5ResetEv:
.LFB20307:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdx
	movq	40(%rdi), %rax
	movabsq	$-1085102592571150095, %rcx
	subq	%rdx, %rax
	imulq	%rcx, %rax
	movq	48(%rdi), %rcx
	movq	(%rcx), %rcx
	movq	7(%rcx), %rcx
	testl	%eax, %eax
	jle	.L1390
	movq	%rcx, %rsi
	subl	$1, %eax
	leaq	15(%rcx), %rdx
	andq	$-262144, %rsi
	leaq	23(%rcx,%rax,8), %rcx
	addq	$24, %rsi
	.p2align 4,,10
	.p2align 3
.L1391:
	movq	(%rsi), %rax
	addq	$8, %rdx
	movq	-37496(%rax), %rax
	movq	%rax, -8(%rdx)
	cmpq	%rdx, %rcx
	jne	.L1391
	movq	24(%rdi), %rdx
.L1390:
	movq	%rdx, 40(%rdi)
	movq	64(%rdi), %rax
	cmpq	72(%rdi), %rax
	je	.L1392
	movq	%rax, 72(%rdi)
.L1392:
	movl	$0, 88(%rdi)
	movl	$12, 104(%rdi)
	movb	$0, 108(%rdi)
	ret
	.cfi_endproc
.LFE20307:
	.size	_ZN2v88internal4wasm15WasmInterpreter6Thread5ResetEv, .-_ZN2v88internal4wasm15WasmInterpreter6Thread5ResetEv
	.section	.text._ZN2v88internal4wasm15WasmInterpreter6Thread14RaiseExceptionEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15WasmInterpreter6Thread14RaiseExceptionEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	.type	_ZN2v88internal4wasm15WasmInterpreter6Thread14RaiseExceptionEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE, @function
_ZN2v88internal4wasm15WasmInterpreter6Thread14RaiseExceptionEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE:
.LFB20308:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$104, %rsp
	movq	(%rdx), %rsi
	xorl	%edx, %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	72(%rbx), %r12
	movq	64(%rbx), %rcx
	movabsq	$-6148914691236517205, %rdi
	movq	136(%rbx), %rsi
	movabsq	$-1085102592571150095, %r8
	movq	%r12, %rax
	subq	%rcx, %rax
	movl	-16(%rsi), %edx
	sarq	$3, %rax
	imulq	%rdi, %rax
	cmpq	%rdx, %rax
	jbe	.L1418
	.p2align 4,,10
	.p2align 3
.L1395:
	movq	-24(%r12), %r14
	movq	-16(%r12), %r9
	movq	80(%r14), %rax
	leaq	16(%rax), %r10
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L1397
	movq	%r10, %rdx
	jmp	.L1398
	.p2align 4,,10
	.p2align 3
.L1446:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L1399
.L1398:
	cmpq	32(%rax), %r9
	jbe	.L1446
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L1398
.L1399:
	cmpq	%rdx, %r10
	je	.L1397
	cmpq	32(%rdx), %r9
	jnb	.L1447
.L1397:
	movq	48(%rbx), %rdx
	movq	24(%rbx), %r10
	movq	-8(%r12), %rax
	movq	(%rdx), %rdx
	movq	7(%rdx), %r9
	movq	40(%rbx), %rdx
	subq	%r10, %rdx
	imull	%r8d, %edx
	cmpl	%edx, %eax
	jge	.L1416
	movl	%eax, %r11d
	movq	%r9, %r10
	leal	16(,%rax,8), %ecx
	notl	%r11d
	andq	$-262144, %r10
	movslq	%ecx, %rcx
	addl	%r11d, %edx
	movslq	%eax, %r11
	leaq	-1(%r9,%rcx), %rcx
	addq	$24, %r10
	addq	%r11, %rdx
	leaq	23(%r9,%rdx,8), %rdx
	.p2align 4,,10
	.p2align 3
.L1417:
	movq	(%r10), %r9
	addq	$8, %rcx
	movq	-37496(%r9), %r9
	movq	%r9, -8(%rcx)
	cmpq	%rcx, %rdx
	jne	.L1417
	movq	64(%rbx), %rcx
	movq	72(%rbx), %r12
	movq	24(%rbx), %r10
.L1416:
	movq	%rax, %rdx
	subq	$24, %r12
	salq	$4, %rdx
	movq	%r12, 72(%rbx)
	addq	%rdx, %rax
	addq	%r10, %rax
	movq	%rax, 40(%rbx)
	movq	%r12, %rax
	movl	-16(%rsi), %edx
	subq	%rcx, %rax
	sarq	$3, %rax
	imulq	%rdi, %rax
	cmpq	%rdx, %rax
	ja	.L1395
.L1418:
	movl	$0, 88(%rbx)
	movl	$1, %eax
.L1394:
	movq	-56(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L1448
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1447:
	.cfi_restore_state
	movq	41112(%r13), %rdi
	movq	12480(%r13), %r15
	testq	%rdi, %rdi
	je	.L1402
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r15
.L1403:
	movq	$0, -71(%rbp)
	movzbl	-64(%rbp), %edx
	movq	40(%rbx), %rax
	subq	24(%rbx), %rax
	movb	$6, -80(%rbp)
	movb	%dl, -96(%rbp)
	movq	48(%rbx), %rdx
	imull	$-252645135, %eax, %eax
	movq	$0, -79(%rbp)
	movdqa	-80(%rbp), %xmm0
	movq	(%rdx), %rdx
	leal	16(,%rax,8), %eax
	movaps	%xmm0, -112(%rbp)
	movq	7(%rdx), %rdi
	cltq
	leaq	-1(%rdi,%rax), %rsi
	movq	%r15, (%rsi)
	testb	$1, %r15b
	je	.L1419
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -120(%rbp)
	testl	$262144, %eax
	jne	.L1449
.L1406:
	testb	$24, %al
	je	.L1419
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1419
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L1419:
	movq	40(%rbx), %rax
	movb	$6, -112(%rbp)
	movq	$0, -111(%rbp)
	movdqa	-112(%rbp), %xmm1
	leaq	17(%rax), %rdx
	movq	%rdx, 40(%rbx)
	movzbl	-96(%rbp), %edx
	movups	%xmm1, (%rax)
	movb	%dl, 16(%rax)
	movq	96(%r13), %rax
	movq	%rax, 12480(%r13)
	movq	80(%r14), %rax
	movq	-16(%r12), %rcx
	leaq	16(%rax), %r13
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L1408
	movq	%r13, %rdx
	jmp	.L1409
	.p2align 4,,10
	.p2align 3
.L1450:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L1410
.L1409:
	cmpq	32(%rax), %rcx
	jbe	.L1450
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L1409
.L1410:
	cmpq	%rdx, %r13
	je	.L1408
	cmpq	32(%rdx), %rcx
	cmovnb	%rdx, %r13
.L1408:
	movq	24(%rbx), %rcx
	movq	40(%rbx), %rax
	movabsq	$-1085102592571150095, %r15
	movl	44(%r13), %edx
	movl	48(%r13), %r14d
	subq	%rcx, %rax
	imulq	%r15, %rax
	addl	$1, %edx
	movl	%r14d, -120(%rbp)
	movq	%rax, %r10
	subq	%rdx, %r10
	testq	%r14, %r14
	je	.L1413
	cmpq	%rdx, %r14
	jne	.L1451
.L1413:
	movq	48(%rbx), %rdx
	addq	%r10, %r14
	movq	(%rdx), %rdx
	movq	7(%rdx), %rsi
	cmpl	%eax, %r14d
	jge	.L1414
	movl	%r14d, %edi
	movq	%rsi, %rcx
	leal	16(,%r14,8), %edx
	notl	%edi
	andq	$-262144, %rcx
	movslq	%edx, %rdx
	addl	%edi, %eax
	movslq	%r14d, %rdi
	leaq	-1(%rsi,%rdx), %rdx
	addq	$24, %rcx
	addq	%rdi, %rax
	leaq	23(%rsi,%rax,8), %rsi
	.p2align 4,,10
	.p2align 3
.L1415:
	movq	(%rcx), %rax
	addq	$8, %rdx
	movq	-37496(%rax), %rax
	movq	%rax, -8(%rdx)
	cmpq	%rdx, %rsi
	jne	.L1415
	movq	24(%rbx), %rcx
.L1414:
	movq	%r14, %rdi
	salq	$4, %rdi
	addq	%r14, %rdi
	addq	%rcx, %rdi
	movq	%rdi, 40(%rbx)
	movslq	40(%r13), %rax
	addq	%rax, -16(%r12)
	xorl	%eax, %eax
	movl	$2, 88(%rbx)
	jmp	.L1394
	.p2align 4,,10
	.p2align 3
.L1402:
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L1452
.L1404:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%r15, (%rax)
	jmp	.L1403
	.p2align 4,,10
	.p2align 3
.L1451:
	subq	%r14, %rax
	movq	%r10, %rdi
	movq	%r14, %rdx
	movq	%r10, -128(%rbp)
	movq	%rax, %rsi
	salq	$4, %rdi
	movq	%rax, -136(%rbp)
	salq	$4, %rsi
	addq	%r10, %rdi
	salq	$4, %rdx
	addq	%rax, %rsi
	addq	%rcx, %rdi
	addq	%r14, %rdx
	addq	%rcx, %rsi
	call	memmove@PLT
	movq	48(%rbx), %rax
	movq	-128(%rbp), %r10
	movq	-136(%rbp), %r9
	movl	-120(%rbp), %r8d
	movq	(%rax), %rax
	leal	16(,%r10,8), %edx
	leal	16(,%r9,8), %ecx
	movslq	%edx, %rdx
	movl	$4, %r9d
	movq	7(%rax), %rsi
	movslq	%ecx, %rcx
	leaq	-1(%rsi), %rax
	addq	%rax, %rcx
	addq	%rax, %rdx
	movq	8(%rbx), %rax
	leaq	37592(%rax), %rdi
	call	_ZN2v88internal4Heap9MoveRangeENS0_10HeapObjectENS0_14FullObjectSlotES3_iNS0_16WriteBarrierModeE@PLT
	movq	24(%rbx), %rcx
	movq	40(%rbx), %rax
	movq	-128(%rbp), %r10
	subq	%rcx, %rax
	imulq	%r15, %rax
	jmp	.L1413
	.p2align 4,,10
	.p2align 3
.L1452:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L1404
	.p2align 4,,10
	.p2align 3
.L1449:
	movq	%r15, %rdx
	movq	%rsi, -136(%rbp)
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-120(%rbp), %rcx
	movq	-136(%rbp), %rsi
	movq	-128(%rbp), %rdi
	movq	8(%rcx), %rax
	jmp	.L1406
.L1448:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20308:
	.size	_ZN2v88internal4wasm15WasmInterpreter6Thread14RaiseExceptionEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE, .-_ZN2v88internal4wasm15WasmInterpreter6Thread14RaiseExceptionEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	.section	.text._ZN2v88internal4wasm15WasmInterpreter6Thread15GetBreakpointPcEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15WasmInterpreter6Thread15GetBreakpointPcEv
	.type	_ZN2v88internal4wasm15WasmInterpreter6Thread15GetBreakpointPcEv, @function
_ZN2v88internal4wasm15WasmInterpreter6Thread15GetBreakpointPcEv:
.LFB20309:
	.cfi_startproc
	endbr64
	movq	96(%rdi), %rax
	ret
	.cfi_endproc
.LFE20309:
	.size	_ZN2v88internal4wasm15WasmInterpreter6Thread15GetBreakpointPcEv, .-_ZN2v88internal4wasm15WasmInterpreter6Thread15GetBreakpointPcEv
	.section	.text._ZN2v88internal4wasm15WasmInterpreter6Thread13GetFrameCountEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15WasmInterpreter6Thread13GetFrameCountEv
	.type	_ZN2v88internal4wasm15WasmInterpreter6Thread13GetFrameCountEv, @function
_ZN2v88internal4wasm15WasmInterpreter6Thread13GetFrameCountEv:
.LFB20310:
	.cfi_startproc
	endbr64
	movabsq	$-6148914691236517205, %rdx
	movq	72(%rdi), %rax
	subq	64(%rdi), %rax
	sarq	$3, %rax
	imulq	%rdx, %rax
	ret
	.cfi_endproc
.LFE20310:
	.size	_ZN2v88internal4wasm15WasmInterpreter6Thread13GetFrameCountEv, .-_ZN2v88internal4wasm15WasmInterpreter6Thread13GetFrameCountEv
	.section	.text._ZN2v88internal4wasm15WasmInterpreter6Thread8GetFrameEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15WasmInterpreter6Thread8GetFrameEi
	.type	_ZN2v88internal4wasm15WasmInterpreter6Thread8GetFrameEi, @function
_ZN2v88internal4wasm15WasmInterpreter6Thread8GetFrameEi:
.LFB20313:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movl	$16, %edi
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%edx, %ebx
	subq	$8, %rsp
	call	_Znwm@PLT
	movq	%r13, (%rax)
	movl	%ebx, 8(%rax)
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20313:
	.size	_ZN2v88internal4wasm15WasmInterpreter6Thread8GetFrameEi, .-_ZN2v88internal4wasm15WasmInterpreter6Thread8GetFrameEi
	.section	.text._ZN2v88internal4wasm15WasmInterpreter6Thread14GetReturnValueEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15WasmInterpreter6Thread14GetReturnValueEi
	.type	_ZN2v88internal4wasm15WasmInterpreter6Thread14GetReturnValueEi, @function
_ZN2v88internal4wasm15WasmInterpreter6Thread14GetReturnValueEi:
.LFB20314:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -32
	cmpl	$4, 88(%rsi)
	je	.L1467
	movq	136(%rsi), %rcx
	xorl	%eax, %eax
	cmpq	128(%rsi), %rcx
	je	.L1460
	movq	-8(%rcx), %rax
.L1460:
	movl	%edx, %edx
	addq	%rax, %rdx
	movq	%rdx, %rax
	salq	$4, %rax
	addq	%rdx, %rax
	addq	24(%rsi), %rax
	cmpb	$6, (%rax)
	je	.L1461
	movdqu	(%rax), %xmm1
	movzbl	16(%rax), %eax
	movups	%xmm1, (%r12)
	movb	%al, 16(%r12)
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1467:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	movb	$1, (%rdi)
	movq	%r12, %rax
	movups	%xmm0, 1(%rdi)
	movl	$-559038737, 1(%rdi)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1461:
	.cfi_restore_state
	movq	48(%rsi), %rax
	movq	8(%rsi), %rbx
	movq	(%rax), %rcx
	leal	16(,%rdx,8), %eax
	cltq
	movq	7(%rcx), %rdx
	movq	-1(%rax,%rdx), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1462
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L1463:
	movq	%rax, 1(%r12)
	movq	%r12, %rax
	movb	$6, (%r12)
	movq	$0, 9(%r12)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1462:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L1468
.L1464:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L1463
	.p2align 4,,10
	.p2align 3
.L1468:
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L1464
	.cfi_endproc
.LFE20314:
	.size	_ZN2v88internal4wasm15WasmInterpreter6Thread14GetReturnValueEi, .-_ZN2v88internal4wasm15WasmInterpreter6Thread14GetReturnValueEi
	.section	.text._ZN2v88internal4wasm15WasmInterpreter6Thread13GetTrapReasonEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15WasmInterpreter6Thread13GetTrapReasonEv
	.type	_ZN2v88internal4wasm15WasmInterpreter6Thread13GetTrapReasonEv, @function
_ZN2v88internal4wasm15WasmInterpreter6Thread13GetTrapReasonEv:
.LFB20315:
	.cfi_startproc
	endbr64
	movl	104(%rdi), %eax
	ret
	.cfi_endproc
.LFE20315:
	.size	_ZN2v88internal4wasm15WasmInterpreter6Thread13GetTrapReasonEv, .-_ZN2v88internal4wasm15WasmInterpreter6Thread13GetTrapReasonEv
	.section	.text._ZN2v88internal4wasm15WasmInterpreter6Thread14GetGlobalCountEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15WasmInterpreter6Thread14GetGlobalCountEv
	.type	_ZN2v88internal4wasm15WasmInterpreter6Thread14GetGlobalCountEv, @function
_ZN2v88internal4wasm15WasmInterpreter6Thread14GetGlobalCountEv:
.LFB20316:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	8(%rax), %rdx
	movq	32(%rdx), %rax
	subq	24(%rdx), %rax
	sarq	$5, %rax
	ret
	.cfi_endproc
.LFE20316:
	.size	_ZN2v88internal4wasm15WasmInterpreter6Thread14GetGlobalCountEv, .-_ZN2v88internal4wasm15WasmInterpreter6Thread14GetGlobalCountEv
	.section	.text._ZN2v88internal4wasm15WasmInterpreter6Thread14GetGlobalValueEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15WasmInterpreter6Thread14GetGlobalValueEj
	.type	_ZN2v88internal4wasm15WasmInterpreter6Thread14GetGlobalValueEj, @function
_ZN2v88internal4wasm15WasmInterpreter6Thread14GetGlobalValueEj:
.LFB20317:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	salq	$5, %rbx
	subq	$40, %rsp
	movq	(%rsi), %rax
	movq	8(%rax), %rax
	addq	24(%rax), %rbx
	cmpb	$9, (%rbx)
	ja	.L1472
	movzbl	(%rbx), %eax
	leaq	.L1474(%rip), %rdx
	movq	%rdi, %r12
	movq	%rsi, %r13
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmInterpreter6Thread14GetGlobalValueEj,"a",@progbits
	.align 4
	.align 4
.L1474:
	.long	.L1472-.L1474
	.long	.L1479-.L1474
	.long	.L1478-.L1474
	.long	.L1477-.L1474
	.long	.L1476-.L1474
	.long	.L1475-.L1474
	.long	.L1473-.L1474
	.long	.L1473-.L1474
	.long	.L1472-.L1474
	.long	.L1473-.L1474
	.section	.text._ZN2v88internal4wasm15WasmInterpreter6Thread14GetGlobalValueEj
	.p2align 4,,10
	.p2align 3
.L1473:
	movq	8(%rsi), %r14
	movq	41088(%r14), %rax
	addl	$1, 41104(%r14)
	cmpb	$0, 1(%rbx)
	movq	41096(%r14), %r15
	movq	%rax, -56(%rbp)
	movq	16(%rsi), %rax
	movq	8(%rsi), %rcx
	movq	(%rax), %rax
	je	.L1492
	cmpb	$0, 28(%rbx)
	jne	.L1508
.L1492:
	movq	41112(%rcx), %rdi
	movq	175(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1497
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L1498:
	movl	24(%rbx), %eax
.L1496:
	movq	8(%r13), %rbx
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%rsi,%rax), %r13
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1500
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r13
.L1501:
	movq	-56(%rbp), %rax
	subl	$1, 41104(%r14)
	movq	%rax, 41088(%r14)
	cmpq	41096(%r14), %r15
	je	.L1503
	movq	%r15, 41096(%r14)
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1503:
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1504
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L1505:
	movb	$6, (%r12)
	movq	$0, 9(%r12)
	movq	%rax, 1(%r12)
.L1471:
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1479:
	.cfi_restore_state
	movq	16(%rsi), %rax
	cmpb	$0, 1(%rbx)
	movq	(%rax), %rdx
	movl	24(%rbx), %eax
	je	.L1480
	cmpb	$0, 28(%rbx)
	jne	.L1509
.L1480:
	addq	103(%rdx), %rax
.L1481:
	movl	(%rax), %eax
	pxor	%xmm0, %xmm0
	movb	$1, (%r12)
	movups	%xmm0, 1(%r12)
	movl	%eax, 1(%r12)
	jmp	.L1471
	.p2align 4,,10
	.p2align 3
.L1478:
	movq	16(%rsi), %rax
	cmpb	$0, 1(%rbx)
	movq	(%rax), %rdx
	movl	24(%rbx), %eax
	je	.L1483
	cmpb	$0, 28(%rbx)
	jne	.L1510
.L1483:
	addq	103(%rdx), %rax
.L1484:
	movq	(%rax), %rax
	movb	$2, (%r12)
	movq	$0, 9(%r12)
	movq	%rax, 1(%r12)
	jmp	.L1471
	.p2align 4,,10
	.p2align 3
.L1477:
	movq	16(%rsi), %rax
	cmpb	$0, 1(%rbx)
	movq	(%rax), %rdx
	movl	24(%rbx), %eax
	je	.L1485
	cmpb	$0, 28(%rbx)
	jne	.L1511
.L1485:
	addq	103(%rdx), %rax
.L1486:
	movl	(%rax), %eax
	pxor	%xmm0, %xmm0
	movb	$3, (%r12)
	movups	%xmm0, 1(%r12)
	movl	%eax, 1(%r12)
	jmp	.L1471
	.p2align 4,,10
	.p2align 3
.L1476:
	movq	16(%rsi), %rax
	cmpb	$0, 1(%rbx)
	movq	(%rax), %rdx
	movl	24(%rbx), %eax
	je	.L1487
	cmpb	$0, 28(%rbx)
	jne	.L1512
.L1487:
	addq	103(%rdx), %rax
.L1488:
	movq	(%rax), %rax
	movb	$4, (%r12)
	movq	$0, 9(%r12)
	movq	%rax, 1(%r12)
	jmp	.L1471
	.p2align 4,,10
	.p2align 3
.L1475:
	movq	16(%rsi), %rax
	cmpb	$0, 1(%rbx)
	movq	(%rax), %rdx
	movl	24(%rbx), %eax
	je	.L1489
	cmpb	$0, 28(%rbx)
	jne	.L1513
.L1489:
	addq	103(%rdx), %rax
.L1490:
	movdqu	(%rax), %xmm1
	movb	$5, (%r12)
	movups	%xmm1, 1(%r12)
	jmp	.L1471
	.p2align 4,,10
	.p2align 3
.L1504:
	movq	41088(%r14), %rax
	cmpq	41096(%r14), %rax
	je	.L1514
.L1506:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r14)
	movq	%r13, (%rax)
	jmp	.L1505
	.p2align 4,,10
	.p2align 3
.L1500:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L1515
.L1502:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%rbx)
	movq	%r13, (%rax)
	jmp	.L1501
	.p2align 4,,10
	.p2align 3
.L1497:
	movq	41088(%rcx), %rax
	cmpq	41096(%rcx), %rax
	je	.L1516
.L1499:
	leaq	8(%rax), %rdi
	movq	%rdi, 41088(%rcx)
	movq	%rsi, (%rax)
	jmp	.L1498
.L1472:
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1513:
	movq	111(%rdx), %rdx
	movq	(%rdx,%rax,8), %rax
	jmp	.L1490
	.p2align 4,,10
	.p2align 3
.L1512:
	movq	111(%rdx), %rdx
	movq	(%rdx,%rax,8), %rax
	jmp	.L1488
	.p2align 4,,10
	.p2align 3
.L1511:
	movq	111(%rdx), %rdx
	movq	(%rdx,%rax,8), %rax
	jmp	.L1486
	.p2align 4,,10
	.p2align 3
.L1510:
	movq	111(%rdx), %rdx
	movq	(%rdx,%rax,8), %rax
	jmp	.L1484
	.p2align 4,,10
	.p2align 3
.L1509:
	movq	111(%rdx), %rdx
	movq	(%rdx,%rax,8), %rax
	jmp	.L1481
	.p2align 4,,10
	.p2align 3
.L1508:
	movl	24(%rbx), %esi
	movq	183(%rax), %rax
	leal	16(,%rsi,8), %esi
	movslq	%esi, %rsi
	movq	-1(%rsi,%rax), %rsi
	movq	41112(%rcx), %rdi
	testq	%rdi, %rdi
	je	.L1493
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L1494:
	movq	16(%r13), %rax
	movl	24(%rbx), %ecx
	movq	(%rax), %rax
	movq	111(%rax), %rax
	movl	(%rax,%rcx,8), %eax
	jmp	.L1496
	.p2align 4,,10
	.p2align 3
.L1515:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L1502
	.p2align 4,,10
	.p2align 3
.L1514:
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L1506
	.p2align 4,,10
	.p2align 3
.L1493:
	movq	41088(%rcx), %rax
	cmpq	41096(%rcx), %rax
	je	.L1517
.L1495:
	leaq	8(%rax), %rdi
	movq	%rdi, 41088(%rcx)
	movq	%rsi, (%rax)
	jmp	.L1494
	.p2align 4,,10
	.p2align 3
.L1516:
	movq	%rcx, %rdi
	movq	%rsi, -72(%rbp)
	movq	%rcx, -64(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	-64(%rbp), %rcx
	jmp	.L1499
.L1517:
	movq	%rcx, %rdi
	movq	%rsi, -72(%rbp)
	movq	%rcx, -64(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	-64(%rbp), %rcx
	jmp	.L1495
	.cfi_endproc
.LFE20317:
	.size	_ZN2v88internal4wasm15WasmInterpreter6Thread14GetGlobalValueEj, .-_ZN2v88internal4wasm15WasmInterpreter6Thread14GetGlobalValueEj
	.section	.text._ZN2v88internal4wasm15WasmInterpreter6Thread22PossibleNondeterminismEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15WasmInterpreter6Thread22PossibleNondeterminismEv
	.type	_ZN2v88internal4wasm15WasmInterpreter6Thread22PossibleNondeterminismEv, @function
_ZN2v88internal4wasm15WasmInterpreter6Thread22PossibleNondeterminismEv:
.LFB20318:
	.cfi_startproc
	endbr64
	movzbl	108(%rdi), %eax
	ret
	.cfi_endproc
.LFE20318:
	.size	_ZN2v88internal4wasm15WasmInterpreter6Thread22PossibleNondeterminismEv, .-_ZN2v88internal4wasm15WasmInterpreter6Thread22PossibleNondeterminismEv
	.section	.text._ZN2v88internal4wasm15WasmInterpreter6Thread19NumInterpretedCallsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15WasmInterpreter6Thread19NumInterpretedCallsEv
	.type	_ZN2v88internal4wasm15WasmInterpreter6Thread19NumInterpretedCallsEv, @function
_ZN2v88internal4wasm15WasmInterpreter6Thread19NumInterpretedCallsEv:
.LFB20319:
	.cfi_startproc
	endbr64
	movq	112(%rdi), %rax
	ret
	.cfi_endproc
.LFE20319:
	.size	_ZN2v88internal4wasm15WasmInterpreter6Thread19NumInterpretedCallsEv, .-_ZN2v88internal4wasm15WasmInterpreter6Thread19NumInterpretedCallsEv
	.section	.text._ZN2v88internal4wasm15WasmInterpreter6Thread13AddBreakFlagsEh,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15WasmInterpreter6Thread13AddBreakFlagsEh
	.type	_ZN2v88internal4wasm15WasmInterpreter6Thread13AddBreakFlagsEh, @function
_ZN2v88internal4wasm15WasmInterpreter6Thread13AddBreakFlagsEh:
.LFB20320:
	.cfi_startproc
	endbr64
	orb	%sil, 109(%rdi)
	ret
	.cfi_endproc
.LFE20320:
	.size	_ZN2v88internal4wasm15WasmInterpreter6Thread13AddBreakFlagsEh, .-_ZN2v88internal4wasm15WasmInterpreter6Thread13AddBreakFlagsEh
	.section	.text._ZN2v88internal4wasm15WasmInterpreter6Thread15ClearBreakFlagsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15WasmInterpreter6Thread15ClearBreakFlagsEv
	.type	_ZN2v88internal4wasm15WasmInterpreter6Thread15ClearBreakFlagsEv, @function
_ZN2v88internal4wasm15WasmInterpreter6Thread15ClearBreakFlagsEv:
.LFB20321:
	.cfi_startproc
	endbr64
	movb	$0, 109(%rdi)
	ret
	.cfi_endproc
.LFE20321:
	.size	_ZN2v88internal4wasm15WasmInterpreter6Thread15ClearBreakFlagsEv, .-_ZN2v88internal4wasm15WasmInterpreter6Thread15ClearBreakFlagsEv
	.section	.text._ZN2v88internal4wasm15WasmInterpreter6Thread14NumActivationsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15WasmInterpreter6Thread14NumActivationsEv
	.type	_ZN2v88internal4wasm15WasmInterpreter6Thread14NumActivationsEv, @function
_ZN2v88internal4wasm15WasmInterpreter6Thread14NumActivationsEv:
.LFB20322:
	.cfi_startproc
	endbr64
	movq	136(%rdi), %rax
	subq	128(%rdi), %rax
	sarq	$4, %rax
	ret
	.cfi_endproc
.LFE20322:
	.size	_ZN2v88internal4wasm15WasmInterpreter6Thread14NumActivationsEv, .-_ZN2v88internal4wasm15WasmInterpreter6Thread14NumActivationsEv
	.section	.text._ZN2v88internal4wasm15WasmInterpreter6Thread15StartActivationEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15WasmInterpreter6Thread15StartActivationEv
	.type	_ZN2v88internal4wasm15WasmInterpreter6Thread15StartActivationEv, @function
_ZN2v88internal4wasm15WasmInterpreter6Thread15StartActivationEv:
.LFB20323:
	.cfi_startproc
	endbr64
	movabsq	$-1085102592571150095, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movabsq	$-6148914691236517205, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	136(%rdi), %rdx
	movq	128(%rdi), %r14
	movq	40(%rdi), %rax
	subq	24(%rdi), %rax
	movq	%rdx, %r15
	imulq	%rax, %rcx
	movq	72(%rdi), %rax
	subq	64(%rdi), %rax
	subq	%r14, %r15
	sarq	$3, %rax
	movq	%r15, %r13
	imulq	%rax, %r12
	sarq	$4, %r13
	cmpq	144(%rdi), %rdx
	je	.L1524
	movl	%r12d, (%rdx)
	movq	%rcx, 8(%rdx)
	addq	$16, 136(%rdi)
.L1525:
	movl	$0, 88(%rbx)
	addq	$40, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1524:
	.cfi_restore_state
	cmpq	$134217727, %r13
	je	.L1538
	testq	%r13, %r13
	je	.L1533
	leaq	(%r13,%r13), %rax
	cmpq	%rax, %r13
	jbe	.L1539
	movl	$2147483632, %esi
	movl	$2147483632, %r8d
.L1527:
	movq	120(%rbx), %r9
	movq	16(%r9), %rax
	movq	24(%r9), %rdi
	subq	%rax, %rdi
	cmpq	%rdi, %rsi
	ja	.L1540
	addq	%rax, %rsi
	movq	%rsi, 16(%r9)
.L1530:
	addq	%rax, %r8
	leaq	16(%rax), %rsi
.L1528:
	addq	%rax, %r15
	movl	%r12d, (%r15)
	movq	%rcx, 8(%r15)
	cmpq	%rdx, %r14
	je	.L1531
	movq	%r14, %rcx
	movq	%rax, %rsi
	.p2align 4,,10
	.p2align 3
.L1532:
	movl	(%rcx), %r9d
	movq	8(%rcx), %rdi
	addq	$16, %rcx
	addq	$16, %rsi
	movl	%r9d, -16(%rsi)
	movq	%rdi, -8(%rsi)
	cmpq	%rcx, %rdx
	jne	.L1532
	subq	%r14, %rdx
	leaq	16(%rax,%rdx), %rsi
.L1531:
	movq	%rax, %xmm0
	movq	%rsi, %xmm1
	movq	%r8, 144(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 128(%rbx)
	jmp	.L1525
	.p2align 4,,10
	.p2align 3
.L1539:
	testq	%rax, %rax
	jne	.L1541
	movl	$16, %esi
	xorl	%r8d, %r8d
	xorl	%eax, %eax
	jmp	.L1528
	.p2align 4,,10
	.p2align 3
.L1533:
	movl	$16, %esi
	movl	$16, %r8d
	jmp	.L1527
.L1540:
	movq	%r9, %rdi
	movq	%rcx, -72(%rbp)
	movq	%r8, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %r8
	movq	-72(%rbp), %rcx
	jmp	.L1530
.L1538:
	leaq	.LC5(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1541:
	cmpq	$134217727, %rax
	movl	$134217727, %r8d
	cmovbe	%rax, %r8
	salq	$4, %r8
	movq	%r8, %rsi
	jmp	.L1527
	.cfi_endproc
.LFE20323:
	.size	_ZN2v88internal4wasm15WasmInterpreter6Thread15StartActivationEv, .-_ZN2v88internal4wasm15WasmInterpreter6Thread15StartActivationEv
	.section	.text._ZN2v88internal4wasm15WasmInterpreter6Thread16FinishActivationEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15WasmInterpreter6Thread16FinishActivationEj
	.type	_ZN2v88internal4wasm15WasmInterpreter6Thread16FinishActivationEj, @function
_ZN2v88internal4wasm15WasmInterpreter6Thread16FinishActivationEj:
.LFB20324:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rax
	movq	24(%rdi), %rsi
	movq	136(%rdi), %rcx
	movq	(%rax), %rax
	movq	-8(%rcx), %rdx
	movq	7(%rax), %r8
	movq	40(%rdi), %rax
	subq	%rsi, %rax
	imull	$-252645135, %eax, %eax
	cmpl	%eax, %edx
	jge	.L1543
	movl	%edx, %r9d
	movq	%r8, %rsi
	leal	16(,%rdx,8), %ecx
	notl	%r9d
	andq	$-262144, %rsi
	movslq	%ecx, %rcx
	addl	%r9d, %eax
	movslq	%edx, %r9
	leaq	-1(%r8,%rcx), %rcx
	addq	$24, %rsi
	addq	%r9, %rax
	leaq	23(%r8,%rax,8), %r8
	.p2align 4,,10
	.p2align 3
.L1544:
	movq	(%rsi), %rax
	addq	$8, %rcx
	movq	-37496(%rax), %rax
	movq	%rax, -8(%rcx)
	cmpq	%rcx, %r8
	jne	.L1544
	movq	136(%rdi), %rcx
	movq	24(%rdi), %rsi
.L1543:
	movq	%rdx, %rax
	subq	$16, %rcx
	salq	$4, %rax
	movq	%rcx, 136(%rdi)
	addq	%rax, %rdx
	addq	%rdx, %rsi
	movq	%rsi, 40(%rdi)
	ret
	.cfi_endproc
.LFE20324:
	.size	_ZN2v88internal4wasm15WasmInterpreter6Thread16FinishActivationEj, .-_ZN2v88internal4wasm15WasmInterpreter6Thread16FinishActivationEj
	.section	.text._ZN2v88internal4wasm15WasmInterpreter6Thread19ActivationFrameBaseEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15WasmInterpreter6Thread19ActivationFrameBaseEj
	.type	_ZN2v88internal4wasm15WasmInterpreter6Thread19ActivationFrameBaseEj, @function
_ZN2v88internal4wasm15WasmInterpreter6Thread19ActivationFrameBaseEj:
.LFB20325:
	.cfi_startproc
	endbr64
	movl	%esi, %esi
	salq	$4, %rsi
	addq	128(%rdi), %rsi
	movl	(%rsi), %eax
	ret
	.cfi_endproc
.LFE20325:
	.size	_ZN2v88internal4wasm15WasmInterpreter6Thread19ActivationFrameBaseEj, .-_ZN2v88internal4wasm15WasmInterpreter6Thread19ActivationFrameBaseEj
	.section	.text._ZN2v88internal4wasm15WasmInterpreterD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15WasmInterpreterD2Ev
	.type	_ZN2v88internal4wasm15WasmInterpreterD2Ev, @function
_ZN2v88internal4wasm15WasmInterpreterD2Ev:
.LFB20344:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	64(%rdi), %rbx
	movq	88(%rbx), %rax
	movq	48(%rax), %rdi
	call	_ZN2v88internal13GlobalHandles7DestroyEPm@PLT
	movq	96(%rbx), %r13
	movq	88(%rbx), %rbx
	cmpq	%rbx, %r13
	je	.L1548
	.p2align 4,,10
	.p2align 3
.L1551:
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1549
	call	_ZdaPv@PLT
	addq	$152, %rbx
	cmpq	%rbx, %r13
	jne	.L1551
.L1548:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal4ZoneD1Ev@PLT
	.p2align 4,,10
	.p2align 3
.L1549:
	.cfi_restore_state
	addq	$152, %rbx
	cmpq	%rbx, %r13
	jne	.L1551
	jmp	.L1548
	.cfi_endproc
.LFE20344:
	.size	_ZN2v88internal4wasm15WasmInterpreterD2Ev, .-_ZN2v88internal4wasm15WasmInterpreterD2Ev
	.globl	_ZN2v88internal4wasm15WasmInterpreterD1Ev
	.set	_ZN2v88internal4wasm15WasmInterpreterD1Ev,_ZN2v88internal4wasm15WasmInterpreterD2Ev
	.section	.text._ZN2v88internal4wasm15WasmInterpreter5PauseEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15WasmInterpreter5PauseEv
	.type	_ZN2v88internal4wasm15WasmInterpreter5PauseEv, @function
_ZN2v88internal4wasm15WasmInterpreter5PauseEv:
.LFB26406:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC7(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE26406:
	.size	_ZN2v88internal4wasm15WasmInterpreter5PauseEv, .-_ZN2v88internal4wasm15WasmInterpreter5PauseEv
	.section	.text._ZN2v88internal4wasm15WasmInterpreter10SetTracingEPKNS1_12WasmFunctionEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15WasmInterpreter10SetTracingEPKNS1_12WasmFunctionEb
	.type	_ZN2v88internal4wasm15WasmInterpreter10SetTracingEPKNS1_12WasmFunctionEb, @function
_ZN2v88internal4wasm15WasmInterpreter10SetTracingEPKNS1_12WasmFunctionEb:
.LFB20350:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC7(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20350:
	.size	_ZN2v88internal4wasm15WasmInterpreter10SetTracingEPKNS1_12WasmFunctionEb, .-_ZN2v88internal4wasm15WasmInterpreter10SetTracingEPKNS1_12WasmFunctionEb
	.section	.text._ZN2v88internal4wasm15WasmInterpreter14GetThreadCountEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15WasmInterpreter14GetThreadCountEv
	.type	_ZN2v88internal4wasm15WasmInterpreter14GetThreadCountEv, @function
_ZN2v88internal4wasm15WasmInterpreter14GetThreadCountEv:
.LFB20351:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE20351:
	.size	_ZN2v88internal4wasm15WasmInterpreter14GetThreadCountEv, .-_ZN2v88internal4wasm15WasmInterpreter14GetThreadCountEv
	.section	.rodata._ZN2v88internal4wasm15WasmInterpreter9GetThreadEi.str1.1,"aMS",@progbits,1
.LC51:
	.string	"0 == id"
	.section	.text._ZN2v88internal4wasm15WasmInterpreter9GetThreadEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15WasmInterpreter9GetThreadEi
	.type	_ZN2v88internal4wasm15WasmInterpreter9GetThreadEi, @function
_ZN2v88internal4wasm15WasmInterpreter9GetThreadEi:
.LFB20352:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	jne	.L1563
	movq	64(%rdi), %rax
	movq	88(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1563:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC51(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20352:
	.size	_ZN2v88internal4wasm15WasmInterpreter9GetThreadEi, .-_ZN2v88internal4wasm15WasmInterpreter9GetThreadEi
	.section	.text._ZN2v88internal4wasm15WasmInterpreter21AddFunctionForTestingEPKNS1_12WasmFunctionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15WasmInterpreter21AddFunctionForTestingEPKNS1_12WasmFunctionE
	.type	_ZN2v88internal4wasm15WasmInterpreter21AddFunctionForTestingEPKNS1_12WasmFunctionE, @function
_ZN2v88internal4wasm15WasmInterpreter21AddFunctionForTestingEPKNS1_12WasmFunctionE:
.LFB20353:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	64(%rdi), %r12
	movq	32(%r12), %rcx
	movq	64(%r12), %rbx
	cmpq	72(%r12), %rbx
	je	.L1565
	movq	%rsi, (%rbx)
	movl	$0, 8(%rbx)
	movq	%rcx, 16(%rbx)
	movq	$0, 24(%rbx)
	movq	$0, 32(%rbx)
	movq	$0, 40(%rbx)
	movq	$0, 48(%rbx)
	movq	$0, 56(%rbx)
	movq	$0, 64(%rbx)
	movq	$0, 72(%rbx)
	movq	$0, 80(%rbx)
	addq	$88, 64(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1565:
	.cfi_restore_state
	movq	56(%r12), %r15
	movq	%rbx, %r13
	movabsq	$3353953467947191203, %rdx
	subq	%r15, %r13
	movq	%r13, %rax
	sarq	$3, %rax
	imulq	%rdx, %rax
	cmpq	$24403223, %rax
	je	.L1579
	testq	%rax, %rax
	je	.L1574
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L1580
	movl	$2147483624, %esi
	movl	$2147483624, %edx
.L1568:
	movq	48(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %r8
	subq	%rax, %r8
	cmpq	%rsi, %r8
	jb	.L1581
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1571:
	leaq	(%rax,%rdx), %rsi
	leaq	88(%rax), %rdi
.L1569:
	leaq	(%rax,%r13), %rdx
	movq	%r14, (%rdx)
	movl	$0, 8(%rdx)
	movq	%rcx, 16(%rdx)
	movq	$0, 24(%rdx)
	movq	$0, 32(%rdx)
	movq	$0, 40(%rdx)
	movq	$0, 48(%rdx)
	movq	$0, 56(%rdx)
	movq	$0, 64(%rdx)
	movq	$0, 72(%rdx)
	movq	$0, 80(%rdx)
	cmpq	%r15, %rbx
	je	.L1572
	movq	%r15, %rdx
	movq	%rax, %rcx
	pxor	%xmm0, %xmm0
	.p2align 4,,10
	.p2align 3
.L1573:
	movq	(%rdx), %rdi
	addq	$88, %rdx
	addq	$88, %rcx
	movq	%rdi, -88(%rcx)
	movl	-80(%rdx), %edi
	movl	%edi, -80(%rcx)
	movq	-72(%rdx), %rdi
	movq	%rdi, -72(%rcx)
	movq	-64(%rdx), %rdi
	movq	%rdi, -64(%rcx)
	movq	-56(%rdx), %rdi
	movq	%rdi, -56(%rcx)
	movq	-48(%rdx), %rdi
	movq	%rdi, -48(%rcx)
	movq	-40(%rdx), %rdi
	movq	$0, -48(%rdx)
	movups	%xmm0, -64(%rdx)
	movq	%rdi, -40(%rcx)
	movq	-32(%rdx), %rdi
	movq	%rdi, -32(%rcx)
	movq	-24(%rdx), %rdi
	movq	%rdi, -24(%rcx)
	movq	-16(%rdx), %rdi
	movq	%rdi, -16(%rcx)
	movq	-8(%rdx), %rdi
	movq	%rdi, -8(%rcx)
	cmpq	%rdx, %rbx
	jne	.L1573
	leaq	-88(%rbx), %rdx
	movabsq	$1048110458733497251, %rbx
	subq	%r15, %rdx
	shrq	$3, %rdx
	imulq	%rbx, %rdx
	movabsq	$2305843009213693951, %rbx
	andq	%rbx, %rdx
	addq	$2, %rdx
	leaq	(%rdx,%rdx,4), %rcx
	leaq	(%rdx,%rcx,2), %rdx
	leaq	(%rax,%rdx,8), %rdi
.L1572:
	movq	%rax, %xmm0
	movq	%rdi, %xmm1
	movq	%rsi, 72(%r12)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 56(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1574:
	.cfi_restore_state
	movl	$88, %esi
	movl	$88, %edx
	jmp	.L1568
	.p2align 4,,10
	.p2align 3
.L1580:
	testq	%rdx, %rdx
	jne	.L1582
	movl	$88, %edi
	xorl	%esi, %esi
	xorl	%eax, %eax
	jmp	.L1569
	.p2align 4,,10
	.p2align 3
.L1581:
	movq	%rdx, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rdx
	jmp	.L1571
.L1579:
	leaq	.LC5(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1582:
	cmpq	$24403223, %rdx
	movl	$24403223, %eax
	cmova	%rax, %rdx
	imulq	$88, %rdx, %rdx
	movq	%rdx, %rsi
	jmp	.L1568
	.cfi_endproc
.LFE20353:
	.size	_ZN2v88internal4wasm15WasmInterpreter21AddFunctionForTestingEPKNS1_12WasmFunctionE, .-_ZN2v88internal4wasm15WasmInterpreter21AddFunctionForTestingEPKNS1_12WasmFunctionE
	.section	.text._ZNK2v88internal4wasm16InterpretedFrame8functionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4wasm16InterpretedFrame8functionEv
	.type	_ZNK2v88internal4wasm16InterpretedFrame8functionEv, @function
_ZNK2v88internal4wasm16InterpretedFrame8functionEv:
.LFB20368:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movslq	8(%rdi), %rax
	movq	64(%rdx), %rcx
	leaq	(%rax,%rax,2), %rax
	leaq	(%rcx,%rax,8), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	ret
	.cfi_endproc
.LFE20368:
	.size	_ZNK2v88internal4wasm16InterpretedFrame8functionEv, .-_ZNK2v88internal4wasm16InterpretedFrame8functionEv
	.section	.text._ZNK2v88internal4wasm16InterpretedFrame2pcEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4wasm16InterpretedFrame2pcEv
	.type	_ZNK2v88internal4wasm16InterpretedFrame2pcEv, @function
_ZNK2v88internal4wasm16InterpretedFrame2pcEv:
.LFB20369:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movslq	8(%rdi), %rax
	movq	64(%rdx), %rcx
	leaq	(%rax,%rax,2), %rax
	leaq	(%rcx,%rax,8), %rax
	movq	8(%rax), %rax
	ret
	.cfi_endproc
.LFE20369:
	.size	_ZNK2v88internal4wasm16InterpretedFrame2pcEv, .-_ZNK2v88internal4wasm16InterpretedFrame2pcEv
	.section	.text._ZNK2v88internal4wasm16InterpretedFrame17GetParameterCountEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4wasm16InterpretedFrame17GetParameterCountEv
	.type	_ZNK2v88internal4wasm16InterpretedFrame17GetParameterCountEv, @function
_ZNK2v88internal4wasm16InterpretedFrame17GetParameterCountEv:
.LFB20370:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movslq	8(%rdi), %rax
	movq	64(%rdx), %rcx
	leaq	(%rax,%rax,2), %rax
	leaq	(%rcx,%rax,8), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rax
	ret
	.cfi_endproc
.LFE20370:
	.size	_ZNK2v88internal4wasm16InterpretedFrame17GetParameterCountEv, .-_ZNK2v88internal4wasm16InterpretedFrame17GetParameterCountEv
	.section	.text._ZNK2v88internal4wasm16InterpretedFrame13GetLocalCountEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4wasm16InterpretedFrame13GetLocalCountEv
	.type	_ZNK2v88internal4wasm16InterpretedFrame13GetLocalCountEv, @function
_ZNK2v88internal4wasm16InterpretedFrame13GetLocalCountEv:
.LFB20371:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movslq	8(%rdi), %rax
	movq	64(%rdx), %rcx
	leaq	(%rax,%rax,2), %rax
	leaq	(%rcx,%rax,8), %rax
	movq	(%rax), %rdx
	movq	(%rdx), %rax
	movq	(%rax), %rcx
	movq	32(%rdx), %rax
	subq	24(%rdx), %rax
	addq	8(%rcx), %rax
	ret
	.cfi_endproc
.LFE20371:
	.size	_ZNK2v88internal4wasm16InterpretedFrame13GetLocalCountEv, .-_ZNK2v88internal4wasm16InterpretedFrame13GetLocalCountEv
	.section	.text._ZNK2v88internal4wasm16InterpretedFrame14GetStackHeightEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4wasm16InterpretedFrame14GetStackHeightEv
	.type	_ZNK2v88internal4wasm16InterpretedFrame14GetStackHeightEv, @function
_ZNK2v88internal4wasm16InterpretedFrame14GetStackHeightEv:
.LFB20372:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rsi
	movslq	8(%rdi), %rcx
	movabsq	$-6148914691236517205, %r9
	movq	64(%rsi), %rdi
	movq	72(%rsi), %rax
	leaq	1(%rcx), %r8
	movq	%rcx, %rdx
	subq	%rdi, %rax
	sarq	$3, %rax
	imulq	%r9, %rax
	cmpq	%rax, %r8
	je	.L1590
	addl	$1, %edx
	movslq	%edx, %rdx
	leaq	(%rdx,%rdx,2), %rax
	movq	16(%rdi,%rax,8), %rax
.L1589:
	leaq	(%rcx,%rcx,2), %rdx
	leaq	(%rdi,%rdx,8), %rdx
	movq	(%rdx), %rcx
	subq	16(%rdx), %rax
	movq	(%rcx), %rdx
	movq	(%rdx), %rsi
	movq	32(%rcx), %rdx
	subq	24(%rcx), %rdx
	addq	8(%rsi), %rdx
	subl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1590:
	movabsq	$-1085102592571150095, %rdx
	movq	40(%rsi), %rax
	subq	24(%rsi), %rax
	imulq	%rdx, %rax
	jmp	.L1589
	.cfi_endproc
.LFE20372:
	.size	_ZNK2v88internal4wasm16InterpretedFrame14GetStackHeightEv, .-_ZNK2v88internal4wasm16InterpretedFrame14GetStackHeightEv
	.section	.text._ZNK2v88internal4wasm16InterpretedFrame13GetLocalValueEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4wasm16InterpretedFrame13GetLocalValueEi
	.type	_ZNK2v88internal4wasm16InterpretedFrame13GetLocalValueEi, @function
_ZNK2v88internal4wasm16InterpretedFrame13GetLocalValueEi:
.LFB20373:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -32
	movq	(%rsi), %rcx
	movslq	8(%rsi), %rax
	movq	64(%rcx), %rsi
	leaq	(%rax,%rax,2), %rax
	leaq	(%rsi,%rax,8), %rax
	addl	16(%rax), %edx
	movslq	%edx, %rsi
	movq	%rsi, %rax
	salq	$4, %rax
	addq	%rsi, %rax
	addq	24(%rcx), %rax
	cmpb	$6, (%rax)
	je	.L1592
	movdqu	(%rax), %xmm0
	movzbl	16(%rax), %eax
	movups	%xmm0, (%rdi)
	movb	%al, 16(%rdi)
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1592:
	.cfi_restore_state
	movq	48(%rcx), %rax
	movq	8(%rcx), %rbx
	movq	(%rax), %rcx
	leal	16(,%rdx,8), %eax
	cltq
	movq	7(%rcx), %rdx
	movq	-1(%rax,%rdx), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1594
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L1595:
	movq	%rax, 1(%r12)
	movq	%r12, %rax
	movb	$6, (%r12)
	movq	$0, 9(%r12)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1594:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L1598
.L1596:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L1595
	.p2align 4,,10
	.p2align 3
.L1598:
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L1596
	.cfi_endproc
.LFE20373:
	.size	_ZNK2v88internal4wasm16InterpretedFrame13GetLocalValueEi, .-_ZNK2v88internal4wasm16InterpretedFrame13GetLocalValueEi
	.section	.text._ZNK2v88internal4wasm16InterpretedFrame13GetStackValueEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4wasm16InterpretedFrame13GetStackValueEi
	.type	_ZNK2v88internal4wasm16InterpretedFrame13GetStackValueEi, @function
_ZNK2v88internal4wasm16InterpretedFrame13GetStackValueEi:
.LFB20374:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -32
	movq	(%rsi), %rcx
	movslq	8(%rsi), %rax
	leaq	(%rax,%rax,2), %rdx
	movq	64(%rcx), %rax
	leaq	(%rax,%rdx,8), %rsi
	movq	(%rsi), %rax
	movq	(%rax), %rdx
	movq	(%rdx), %rdi
	movq	32(%rax), %rdx
	subq	24(%rax), %rdx
	addq	8(%rdi), %rdx
	addl	16(%rsi), %edx
	addl	%r8d, %edx
	movslq	%edx, %rsi
	movq	%rsi, %rax
	salq	$4, %rax
	addq	%rsi, %rax
	addq	24(%rcx), %rax
	cmpb	$6, (%rax)
	je	.L1600
	movdqu	(%rax), %xmm0
	movzbl	16(%rax), %eax
	movups	%xmm0, (%r12)
	movb	%al, 16(%r12)
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1600:
	.cfi_restore_state
	movq	48(%rcx), %rax
	movq	8(%rcx), %rbx
	movq	(%rax), %rcx
	leal	16(,%rdx,8), %eax
	cltq
	movq	7(%rcx), %rdx
	movq	-1(%rax,%rdx), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1602
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L1603:
	movq	%rax, 1(%r12)
	movq	%r12, %rax
	movb	$6, (%r12)
	movq	$0, 9(%r12)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1602:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L1606
.L1604:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L1603
	.p2align 4,,10
	.p2align 3
.L1606:
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L1604
	.cfi_endproc
.LFE20374:
	.size	_ZNK2v88internal4wasm16InterpretedFrame13GetStackValueEi, .-_ZNK2v88internal4wasm16InterpretedFrame13GetStackValueEi
	.section	.text._ZN2v88internal4wasm23InterpretedFrameDeleterclEPNS1_16InterpretedFrameE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm23InterpretedFrameDeleterclEPNS1_16InterpretedFrameE
	.type	_ZN2v88internal4wasm23InterpretedFrameDeleterclEPNS1_16InterpretedFrameE, @function
_ZN2v88internal4wasm23InterpretedFrameDeleterclEPNS1_16InterpretedFrameE:
.LFB20375:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdi
	testq	%rsi, %rsi
	je	.L1607
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L1607:
	ret
	.cfi_endproc
.LFE20375:
	.size	_ZN2v88internal4wasm23InterpretedFrameDeleterclEPNS1_16InterpretedFrameE, .-_ZN2v88internal4wasm23InterpretedFrameDeleterclEPNS1_16InterpretedFrameE
	.section	.text._ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE0EEC2ERKNS1_12WasmFeaturesEPS3_PKh,"axG",@progbits,_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE0EEC5ERKNS1_12WasmFeaturesEPS3_PKh,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE0EEC2ERKNS1_12WasmFeaturesEPS3_PKh
	.type	_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE0EEC2ERKNS1_12WasmFeaturesEPS3_PKh, @function
_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE0EEC2ERKNS1_12WasmFeaturesEPS3_PKh:
.LFB22398:
	.cfi_startproc
	endbr64
	movzbl	1(%rcx), %edx
	movl	$1, (%rdi)
	movb	$0, 4(%rdi)
	leal	-64(%rdx), %eax
	movl	$0, 8(%rdi)
	movq	$0, 16(%rdi)
	cmpb	$63, %al
	ja	.L1610
	leaq	.L1612(%rip), %rsi
	movzbl	%al, %eax
	movslq	(%rsi,%rax,4), %rax
	addq	%rsi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE0EEC2ERKNS1_12WasmFeaturesEPS3_PKh,"aG",@progbits,_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE0EEC5ERKNS1_12WasmFeaturesEPS3_PKh,comdat
	.align 4
	.align 4
.L1612:
	.long	.L1609-.L1612
	.long	.L1610-.L1612
	.long	.L1610-.L1612
	.long	.L1610-.L1612
	.long	.L1610-.L1612
	.long	.L1610-.L1612
	.long	.L1610-.L1612
	.long	.L1610-.L1612
	.long	.L1610-.L1612
	.long	.L1610-.L1612
	.long	.L1610-.L1612
	.long	.L1610-.L1612
	.long	.L1610-.L1612
	.long	.L1610-.L1612
	.long	.L1610-.L1612
	.long	.L1610-.L1612
	.long	.L1610-.L1612
	.long	.L1610-.L1612
	.long	.L1610-.L1612
	.long	.L1610-.L1612
	.long	.L1610-.L1612
	.long	.L1610-.L1612
	.long	.L1610-.L1612
	.long	.L1610-.L1612
	.long	.L1610-.L1612
	.long	.L1610-.L1612
	.long	.L1610-.L1612
	.long	.L1610-.L1612
	.long	.L1610-.L1612
	.long	.L1610-.L1612
	.long	.L1610-.L1612
	.long	.L1610-.L1612
	.long	.L1610-.L1612
	.long	.L1610-.L1612
	.long	.L1610-.L1612
	.long	.L1610-.L1612
	.long	.L1610-.L1612
	.long	.L1610-.L1612
	.long	.L1610-.L1612
	.long	.L1610-.L1612
	.long	.L1619-.L1612
	.long	.L1610-.L1612
	.long	.L1610-.L1612
	.long	.L1610-.L1612
	.long	.L1610-.L1612
	.long	.L1610-.L1612
	.long	.L1610-.L1612
	.long	.L1618-.L1612
	.long	.L1617-.L1612
	.long	.L1610-.L1612
	.long	.L1610-.L1612
	.long	.L1610-.L1612
	.long	.L1610-.L1612
	.long	.L1610-.L1612
	.long	.L1610-.L1612
	.long	.L1610-.L1612
	.long	.L1610-.L1612
	.long	.L1610-.L1612
	.long	.L1610-.L1612
	.long	.L1616-.L1612
	.long	.L1615-.L1612
	.long	.L1614-.L1612
	.long	.L1613-.L1612
	.long	.L1611-.L1612
	.section	.text._ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE0EEC2ERKNS1_12WasmFeaturesEPS3_PKh,"axG",@progbits,_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE0EEC5ERKNS1_12WasmFeaturesEPS3_PKh,comdat
	.p2align 4,,10
	.p2align 3
.L1610:
	movl	%edx, %esi
	movb	$10, 4(%rdi)
	andl	$127, %esi
	movl	%esi, %eax
	sall	$25, %eax
	sarl	$25, %eax
	testb	%dl, %dl
	js	.L1626
.L1625:
	movl	%eax, 8(%rdi)
.L1609:
	ret
	.p2align 4,,10
	.p2align 3
.L1611:
	movb	$1, 4(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L1619:
	movb	$9, 4(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L1618:
	movb	$6, 4(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L1617:
	movb	$7, 4(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L1616:
	movb	$5, 4(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L1615:
	movb	$4, 4(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L1614:
	movb	$3, 4(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L1613:
	movb	$2, 4(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L1626:
	movzbl	2(%rcx), %edx
	movzbl	%sil, %esi
	movl	%edx, %eax
	sall	$7, %eax
	andl	$16256, %eax
	orl	%esi, %eax
	testb	%dl, %dl
	js	.L1627
	sall	$18, %eax
	movl	$2, (%rdi)
	sarl	$18, %eax
	jmp	.L1625
	.p2align 4,,10
	.p2align 3
.L1627:
	movzbl	3(%rcx), %esi
	movl	%esi, %edx
	sall	$14, %edx
	andl	$2080768, %edx
	orl	%edx, %eax
	testb	%sil, %sil
	js	.L1628
	sall	$11, %eax
	movl	$3, (%rdi)
	sarl	$11, %eax
	jmp	.L1625
	.p2align 4,,10
	.p2align 3
.L1628:
	movzbl	4(%rcx), %esi
	movl	%esi, %edx
	sall	$21, %edx
	andl	$266338304, %edx
	orl	%edx, %eax
	testb	%sil, %sil
	js	.L1629
	sall	$4, %eax
	movl	$4, (%rdi)
	sarl	$4, %eax
	jmp	.L1625
.L1629:
	movzbl	5(%rcx), %edx
	movl	$5, (%rdi)
	sall	$28, %edx
	orl	%edx, %eax
	jmp	.L1625
	.cfi_endproc
.LFE22398:
	.size	_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE0EEC2ERKNS1_12WasmFeaturesEPS3_PKh, .-_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE0EEC2ERKNS1_12WasmFeaturesEPS3_PKh
	.weak	_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE0EEC1ERKNS1_12WasmFeaturesEPS3_PKh
	.set	_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE0EEC1ERKNS1_12WasmFeaturesEPS3_PKh,_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE0EEC2ERKNS1_12WasmFeaturesEPS3_PKh
	.section	.text._ZNSt6vectorIZN2v88internal4wasm9SideTableC4EPNS1_4ZoneEPKNS2_10WasmModuleEPNS2_15InterpreterCodeEE7ControlNS1_13ZoneAllocatorISB_EEE12emplace_backIJPKhRPZNS3_C4ES5_S8_SA_E6CLabelSK_jEEEvDpOT_,"axG",@progbits,_ZNSt6vectorIZN2v88internal4wasm9SideTableC4EPNS1_4ZoneEPKNS2_10WasmModuleEPNS2_15InterpreterCodeEE7ControlNS1_13ZoneAllocatorISB_EEE12emplace_backIJPKhRPZNS3_C4ES5_S8_SA_E6CLabelSK_jEEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIZN2v88internal4wasm9SideTableC4EPNS1_4ZoneEPKNS2_10WasmModuleEPNS2_15InterpreterCodeEE7ControlNS1_13ZoneAllocatorISB_EEE12emplace_backIJPKhRPZNS3_C4ES5_S8_SA_E6CLabelSK_jEEEvDpOT_
	.type	_ZNSt6vectorIZN2v88internal4wasm9SideTableC4EPNS1_4ZoneEPKNS2_10WasmModuleEPNS2_15InterpreterCodeEE7ControlNS1_13ZoneAllocatorISB_EEE12emplace_backIJPKhRPZNS3_C4ES5_S8_SA_E6CLabelSK_jEEEvDpOT_, @function
_ZNSt6vectorIZN2v88internal4wasm9SideTableC4EPNS1_4ZoneEPKNS2_10WasmModuleEPNS2_15InterpreterCodeEE7ControlNS1_13ZoneAllocatorISB_EEE12emplace_backIJPKhRPZNS3_C4ES5_S8_SA_E6CLabelSK_jEEEvDpOT_:
.LFB22406:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rbx
	cmpq	24(%rdi), %rbx
	je	.L1631
	movq	(%rsi), %rdi
	movl	(%r8), %eax
	movq	(%rdx), %rsi
	movq	(%rcx), %rdx
	movb	$0, 28(%rbx)
	movq	%rdi, (%rbx)
	movq	%rsi, 8(%rbx)
	movq	%rdx, 16(%rbx)
	movl	%eax, 24(%rbx)
	addq	$32, 16(%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1631:
	.cfi_restore_state
	movq	8(%rdi), %r14
	movq	%rbx, %r15
	subq	%r14, %r15
	movq	%r15, %rax
	sarq	$5, %rax
	cmpq	$67108863, %rax
	je	.L1645
	testq	%rax, %rax
	je	.L1640
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L1646
	movl	$2147483616, %esi
	movl	$2147483616, %r9d
.L1634:
	movq	(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %r10
	subq	%rax, %r10
	cmpq	%r10, %rsi
	ja	.L1647
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1637:
	leaq	(%rax,%r9), %rdi
	leaq	32(%rax), %r9
.L1635:
	movq	(%rdx), %r10
	movq	0(%r13), %r11
	leaq	(%rax,%r15), %rsi
	movq	(%rcx), %rcx
	movl	(%r8), %edx
	movb	$0, 28(%rsi)
	movq	%r11, (%rsi)
	movq	%r10, 8(%rsi)
	movq	%rcx, 16(%rsi)
	movl	%edx, 24(%rsi)
	cmpq	%r14, %rbx
	je	.L1638
	movq	%r14, %rdx
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L1639:
	movdqu	(%rdx), %xmm1
	addq	$32, %rdx
	addq	$32, %rcx
	movups	%xmm1, -32(%rcx)
	movdqu	-16(%rdx), %xmm2
	movups	%xmm2, -16(%rcx)
	cmpq	%rdx, %rbx
	jne	.L1639
	subq	%r14, %rbx
	leaq	32(%rax,%rbx), %r9
.L1638:
	movq	%rax, %xmm0
	movq	%r9, %xmm3
	movq	%rdi, 24(%r12)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 8(%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1646:
	.cfi_restore_state
	testq	%rsi, %rsi
	jne	.L1648
	movl	$32, %r9d
	xorl	%edi, %edi
	xorl	%eax, %eax
	jmp	.L1635
	.p2align 4,,10
	.p2align 3
.L1640:
	movl	$32, %esi
	movl	$32, %r9d
	jmp	.L1634
.L1647:
	movq	%r8, -80(%rbp)
	movq	%rcx, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %r8
	jmp	.L1637
.L1645:
	leaq	.LC5(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1648:
	cmpq	$67108863, %rsi
	movl	$67108863, %r9d
	cmova	%r9, %rsi
	salq	$5, %rsi
	movq	%rsi, %r9
	jmp	.L1634
	.cfi_endproc
.LFE22406:
	.size	_ZNSt6vectorIZN2v88internal4wasm9SideTableC4EPNS1_4ZoneEPKNS2_10WasmModuleEPNS2_15InterpreterCodeEE7ControlNS1_13ZoneAllocatorISB_EEE12emplace_backIJPKhRPZNS3_C4ES5_S8_SA_E6CLabelSK_jEEEvDpOT_, .-_ZNSt6vectorIZN2v88internal4wasm9SideTableC4EPNS1_4ZoneEPKNS2_10WasmModuleEPNS2_15InterpreterCodeEE7ControlNS1_13ZoneAllocatorISB_EEE12emplace_backIJPKhRPZNS3_C4ES5_S8_SA_E6CLabelSK_jEEEvDpOT_
	.section	.text._ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsIjjEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_,"axG",@progbits,_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsIjjEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsIjjEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	.type	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsIjjEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_, @function
_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsIjjEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_:
.LFB22570:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r9, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	16(%rbp), %rcx
	movq	24(%rbp), %r9
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	64(%rdx), %rax
	leaq	1(%rax,%r8), %rax
	cmpb	$0, 1(%rax)
	js	.L1691
	movl	$6, %r8d
	movl	$5, %edi
	movl	$7, %esi
	movl	$8, %r10d
	movl	$4, %r15d
	movl	$2, %edx
.L1650:
	addq	%rdx, %rax
	movzbl	(%rax), %edx
	movl	%edx, %r12d
	andl	$127, %r12d
	testb	%dl, %dl
	js	.L1692
.L1651:
	movabsq	$-1085102592571150095, %rsi
	movq	40(%rbx), %rax
	leaq	-17(%rax), %rdx
	movq	%rdx, %rdi
	subq	24(%rbx), %rdi
	imulq	%rdi, %rsi
	movq	%rsi, %rdi
	testq	%r9, %r9
	je	.L1652
	movq	%rdx, 40(%rbx)
	movdqu	-17(%rax), %xmm2
	movzbl	-1(%rax), %edx
	movzbl	-17(%rax), %eax
	movaps	%xmm2, -80(%rbp)
	movb	%dl, -64(%rbp)
	cmpb	$6, %al
	je	.L1653
	movb	%al, -80(%rbp)
	movdqa	-80(%rbp), %xmm4
	movb	%dl, -96(%rbp)
	movaps	%xmm4, -112(%rbp)
.L1654:
	movabsq	$-1085102592571150095, %rsi
	movl	-111(%rbp), %eax
	movl	%eax, (%r9)
	movq	40(%rbx), %rax
	leaq	-17(%rax), %rdx
	movq	%rdx, %rdi
	subq	24(%rbx), %rdi
	imulq	%rsi, %rdi
.L1652:
	testq	%rcx, %rcx
	je	.L1658
	movq	%rdx, 40(%rbx)
	movdqu	-17(%rax), %xmm3
	movzbl	-1(%rax), %edx
	movzbl	-17(%rax), %eax
	movaps	%xmm3, -80(%rbp)
	movb	%dl, -64(%rbp)
	cmpb	$6, %al
	je	.L1659
	movb	%al, -80(%rbp)
	movdqa	-80(%rbp), %xmm5
	movb	%dl, -96(%rbp)
	movaps	%xmm5, -112(%rbp)
.L1660:
	movl	-111(%rbp), %eax
	movl	%eax, (%rcx)
	movq	40(%rbx), %rax
	movabsq	$-1085102592571150095, %rcx
	leaq	-17(%rax), %rdx
	movq	%rdx, %rdi
	subq	24(%rbx), %rdi
	imulq	%rcx, %rdi
.L1658:
	movq	%rdx, 40(%rbx)
	movdqu	-17(%rax), %xmm0
	movzbl	-1(%rax), %edx
	movzbl	-17(%rax), %eax
	movaps	%xmm0, -80(%rbp)
	movb	%dl, -64(%rbp)
	cmpb	$6, %al
	je	.L1664
	movb	%al, -80(%rbp)
	movdqa	-80(%rbp), %xmm1
	movb	%dl, -96(%rbp)
	movaps	%xmm1, -112(%rbp)
.L1665:
	addl	-111(%rbp), %r12d
	jc	.L1670
	movq	16(%rbx), %rax
	xorl	%ecx, %ecx
	movq	(%rax), %rdx
	movq	31(%rdx), %rax
	cmpq	$3, %rax
	ja	.L1693
.L1671:
	movq	%rcx, 0(%r13)
	movl	%r15d, (%r14)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1694
	addq	$104, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1693:
	.cfi_restore_state
	movl	%r12d, %r12d
	subq	$4, %rax
	cmpq	%rax, %r12
	ja	.L1671
	movq	23(%rdx), %rcx
	andq	39(%rdx), %r12
	addq	%r12, %rcx
	jmp	.L1671
	.p2align 4,,10
	.p2align 3
.L1691:
	cmpb	$0, 2(%rax)
	js	.L1695
	movl	$7, %r8d
	movl	$6, %edi
	movl	$8, %esi
	movl	$9, %r10d
	movl	$5, %r15d
	movl	$3, %edx
	jmp	.L1650
	.p2align 4,,10
	.p2align 3
.L1692:
	movzbl	1(%rax), %r11d
	movl	%edi, %r15d
	movl	%r11d, %edx
	sall	$7, %edx
	andl	$16256, %edx
	orl	%edx, %r12d
	testb	%r11b, %r11b
	jns	.L1651
	movzbl	2(%rax), %edi
	movl	%r8d, %r15d
	movl	%edi, %edx
	sall	$14, %edx
	andl	$2080768, %edx
	orl	%edx, %r12d
	testb	%dil, %dil
	jns	.L1651
	movzbl	3(%rax), %edi
	movl	%esi, %r15d
	movl	%edi, %edx
	sall	$21, %edx
	andl	$266338304, %edx
	orl	%edx, %r12d
	testb	%dil, %dil
	jns	.L1651
	movzbl	4(%rax), %eax
	movl	%r10d, %r15d
	sall	$28, %eax
	orl	%eax, %r12d
	jmp	.L1651
	.p2align 4,,10
	.p2align 3
.L1664:
	movq	48(%rbx), %rax
	movq	8(%rbx), %rdx
	movq	(%rax), %rcx
	leal	16(,%rdi,8), %eax
	cltq
	movq	7(%rcx), %rcx
	movq	-1(%rax,%rcx), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L1666
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L1667:
	movq	48(%rbx), %rdx
	movq	8(%rbx), %rcx
	movq	%rax, -111(%rbp)
	movq	40(%rbx), %rax
	subq	24(%rbx), %rax
	imull	$-252645135, %eax, %eax
	movq	(%rdx), %rdx
	movq	96(%rcx), %rcx
	movq	7(%rdx), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	%rcx, -1(%rax,%rdx)
	jmp	.L1665
	.p2align 4,,10
	.p2align 3
.L1659:
	movq	48(%rbx), %rax
	movq	8(%rbx), %rdx
	movq	(%rax), %rsi
	leal	16(,%rdi,8), %eax
	cltq
	movq	7(%rsi), %rsi
	movq	-1(%rax,%rsi), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L1661
	movq	%rcx, -120(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-120(%rbp), %rcx
.L1662:
	movq	48(%rbx), %rdx
	movq	8(%rbx), %rsi
	movq	%rax, -111(%rbp)
	movq	40(%rbx), %rax
	subq	24(%rbx), %rax
	imull	$-252645135, %eax, %eax
	movq	(%rdx), %rdx
	movq	96(%rsi), %rsi
	movq	7(%rdx), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	%rsi, -1(%rax,%rdx)
	jmp	.L1660
	.p2align 4,,10
	.p2align 3
.L1653:
	movq	48(%rbx), %rax
	movq	8(%rbx), %rdx
	movq	(%rax), %rdi
	leal	16(,%rsi,8), %eax
	cltq
	movq	7(%rdi), %rsi
	movq	-1(%rax,%rsi), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L1655
	movq	%r9, -128(%rbp)
	movq	%rcx, -120(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-120(%rbp), %rcx
	movq	-128(%rbp), %r9
.L1656:
	movq	48(%rbx), %rdx
	movq	8(%rbx), %rsi
	movq	%rax, -111(%rbp)
	movq	40(%rbx), %rax
	subq	24(%rbx), %rax
	imull	$-252645135, %eax, %eax
	movq	(%rdx), %rdx
	movq	96(%rsi), %rsi
	movq	7(%rdx), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	%rsi, -1(%rax,%rdx)
	jmp	.L1654
	.p2align 4,,10
	.p2align 3
.L1695:
	cmpb	$0, 3(%rax)
	js	.L1696
	movl	$8, %r8d
	movl	$7, %edi
	movl	$9, %esi
	movl	$10, %r10d
	movl	$6, %r15d
	movl	$4, %edx
	jmp	.L1650
	.p2align 4,,10
	.p2align 3
.L1666:
	movq	41088(%rdx), %rax
	cmpq	41096(%rdx), %rax
	je	.L1697
.L1668:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%rdx)
	movq	%rsi, (%rax)
	jmp	.L1667
	.p2align 4,,10
	.p2align 3
.L1655:
	movq	41088(%rdx), %rax
	cmpq	41096(%rdx), %rax
	je	.L1698
.L1657:
	leaq	8(%rax), %rdi
	movq	%rdi, 41088(%rdx)
	movq	%rsi, (%rax)
	jmp	.L1656
	.p2align 4,,10
	.p2align 3
.L1661:
	movq	41088(%rdx), %rax
	cmpq	41096(%rdx), %rax
	je	.L1699
.L1663:
	leaq	8(%rax), %rdi
	movq	%rdi, 41088(%rdx)
	movq	%rsi, (%rax)
	jmp	.L1662
.L1699:
	movq	%rdx, %rdi
	movq	%rcx, -136(%rbp)
	movq	%rsi, -128(%rbp)
	movq	%rdx, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rcx
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	jmp	.L1663
.L1698:
	movq	%rdx, %rdi
	movq	%r9, -144(%rbp)
	movq	%rcx, -136(%rbp)
	movq	%rsi, -128(%rbp)
	movq	%rdx, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-144(%rbp), %r9
	movq	-136(%rbp), %rcx
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	jmp	.L1657
	.p2align 4,,10
	.p2align 3
.L1696:
	movsbl	4(%rax), %esi
	movsbq	4(%rax), %rdx
	movl	%esi, %r8d
	movl	%esi, %edi
	movl	%esi, %r10d
	movl	%esi, %r15d
	sarl	$31, %r8d
	sarl	$31, %edi
	sarl	$31, %r10d
	sarl	$31, %r15d
	notl	%r8d
	notl	%edi
	sarl	$31, %esi
	sarq	$63, %rdx
	notl	%r10d
	notl	%r15d
	notl	%esi
	notq	%rdx
	addl	$10, %r8d
	addl	$9, %edi
	addl	$12, %r10d
	addl	$8, %r15d
	addl	$11, %esi
	addq	$6, %rdx
	jmp	.L1650
	.p2align 4,,10
	.p2align 3
.L1697:
	movq	%rdx, %rdi
	movq	%rsi, -128(%rbp)
	movq	%rdx, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	jmp	.L1668
.L1670:
	xorl	%ecx, %ecx
	jmp	.L1671
.L1694:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22570:
	.size	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsIjjEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_, .-_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsIjjEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	.section	.text._ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsIhjEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_,"axG",@progbits,_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsIhjEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsIhjEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	.type	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsIhjEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_, @function
_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsIhjEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_:
.LFB22573:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r9, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	16(%rbp), %rcx
	movq	24(%rbp), %r9
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	64(%rdx), %rax
	leaq	1(%rax,%r8), %rax
	cmpb	$0, 1(%rax)
	js	.L1744
	movl	$6, %r8d
	movl	$5, %edi
	movl	$7, %esi
	movl	$8, %r10d
	movl	$4, %r15d
	movl	$2, %edx
.L1701:
	addq	%rdx, %rax
	movzbl	(%rax), %edx
	movl	%edx, %r12d
	andl	$127, %r12d
	testb	%dl, %dl
	js	.L1745
.L1702:
	movabsq	$-1085102592571150095, %rsi
	movq	40(%rbx), %rax
	leaq	-17(%rax), %rdx
	movq	%rdx, %rdi
	subq	24(%rbx), %rdi
	imulq	%rdi, %rsi
	movq	%rsi, %rdi
	testq	%r9, %r9
	je	.L1703
	movq	%rdx, 40(%rbx)
	movdqu	-17(%rax), %xmm2
	movzbl	-1(%rax), %edx
	movzbl	-17(%rax), %eax
	movaps	%xmm2, -80(%rbp)
	movb	%dl, -64(%rbp)
	cmpb	$6, %al
	je	.L1704
	movb	%al, -80(%rbp)
	movdqa	-80(%rbp), %xmm4
	movb	%dl, -96(%rbp)
	movaps	%xmm4, -112(%rbp)
.L1705:
	movabsq	$-1085102592571150095, %rsi
	movl	-111(%rbp), %eax
	movb	%al, (%r9)
	movq	40(%rbx), %rax
	leaq	-17(%rax), %rdx
	movq	%rdx, %rdi
	subq	24(%rbx), %rdi
	imulq	%rsi, %rdi
.L1703:
	testq	%rcx, %rcx
	je	.L1709
	movq	%rdx, 40(%rbx)
	movdqu	-17(%rax), %xmm3
	movzbl	-1(%rax), %edx
	movzbl	-17(%rax), %eax
	movaps	%xmm3, -80(%rbp)
	movb	%dl, -64(%rbp)
	cmpb	$6, %al
	je	.L1710
	movb	%al, -80(%rbp)
	movdqa	-80(%rbp), %xmm5
	movb	%dl, -96(%rbp)
	movaps	%xmm5, -112(%rbp)
.L1711:
	movl	-111(%rbp), %eax
	movb	%al, (%rcx)
	movq	40(%rbx), %rax
	movabsq	$-1085102592571150095, %rcx
	leaq	-17(%rax), %rdx
	movq	%rdx, %rdi
	subq	24(%rbx), %rdi
	imulq	%rcx, %rdi
.L1709:
	movq	%rdx, 40(%rbx)
	movdqu	-17(%rax), %xmm0
	movzbl	-1(%rax), %edx
	movzbl	-17(%rax), %eax
	movaps	%xmm0, -80(%rbp)
	movb	%dl, -64(%rbp)
	cmpb	$6, %al
	je	.L1715
	movb	%al, -80(%rbp)
	movdqa	-80(%rbp), %xmm1
	movb	%dl, -96(%rbp)
	movaps	%xmm1, -112(%rbp)
.L1716:
	addl	-111(%rbp), %r12d
	jc	.L1721
	movq	16(%rbx), %rax
	movq	(%rax), %rdx
	movq	31(%rdx), %rax
	testq	%rax, %rax
	jne	.L1746
.L1722:
	movq	%rax, 0(%r13)
	movl	%r15d, (%r14)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1747
	addq	$104, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1746:
	.cfi_restore_state
	leaq	-1(%rax), %rcx
	movl	%r12d, %r12d
	xorl	%eax, %eax
	cmpq	%rcx, %r12
	ja	.L1722
	movq	23(%rdx), %rax
	andq	39(%rdx), %r12
	addq	%r12, %rax
	jmp	.L1722
	.p2align 4,,10
	.p2align 3
.L1744:
	cmpb	$0, 2(%rax)
	js	.L1748
	movl	$7, %r8d
	movl	$6, %edi
	movl	$8, %esi
	movl	$9, %r10d
	movl	$5, %r15d
	movl	$3, %edx
	jmp	.L1701
	.p2align 4,,10
	.p2align 3
.L1745:
	movzbl	1(%rax), %r11d
	movl	%edi, %r15d
	movl	%r11d, %edx
	sall	$7, %edx
	andl	$16256, %edx
	orl	%edx, %r12d
	testb	%r11b, %r11b
	jns	.L1702
	movzbl	2(%rax), %edi
	movl	%r8d, %r15d
	movl	%edi, %edx
	sall	$14, %edx
	andl	$2080768, %edx
	orl	%edx, %r12d
	testb	%dil, %dil
	jns	.L1702
	movzbl	3(%rax), %edi
	movl	%esi, %r15d
	movl	%edi, %edx
	sall	$21, %edx
	andl	$266338304, %edx
	orl	%edx, %r12d
	testb	%dil, %dil
	jns	.L1702
	movzbl	4(%rax), %eax
	movl	%r10d, %r15d
	sall	$28, %eax
	orl	%eax, %r12d
	jmp	.L1702
	.p2align 4,,10
	.p2align 3
.L1715:
	movq	48(%rbx), %rax
	movq	8(%rbx), %rdx
	movq	(%rax), %rcx
	leal	16(,%rdi,8), %eax
	cltq
	movq	7(%rcx), %rcx
	movq	-1(%rax,%rcx), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L1717
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L1718:
	movq	48(%rbx), %rdx
	movq	8(%rbx), %rcx
	movq	%rax, -111(%rbp)
	movq	40(%rbx), %rax
	subq	24(%rbx), %rax
	imull	$-252645135, %eax, %eax
	movq	(%rdx), %rdx
	movq	96(%rcx), %rcx
	movq	7(%rdx), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	%rcx, -1(%rax,%rdx)
	jmp	.L1716
	.p2align 4,,10
	.p2align 3
.L1710:
	movq	48(%rbx), %rax
	movq	8(%rbx), %rdx
	movq	(%rax), %rsi
	leal	16(,%rdi,8), %eax
	cltq
	movq	7(%rsi), %rsi
	movq	-1(%rax,%rsi), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L1712
	movq	%rcx, -120(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-120(%rbp), %rcx
.L1713:
	movq	48(%rbx), %rdx
	movq	8(%rbx), %rsi
	movq	%rax, -111(%rbp)
	movq	40(%rbx), %rax
	subq	24(%rbx), %rax
	imull	$-252645135, %eax, %eax
	movq	(%rdx), %rdx
	movq	96(%rsi), %rsi
	movq	7(%rdx), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	%rsi, -1(%rax,%rdx)
	jmp	.L1711
	.p2align 4,,10
	.p2align 3
.L1704:
	movq	48(%rbx), %rax
	movq	8(%rbx), %rdx
	movq	(%rax), %rdi
	leal	16(,%rsi,8), %eax
	cltq
	movq	7(%rdi), %rsi
	movq	-1(%rax,%rsi), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L1706
	movq	%r9, -128(%rbp)
	movq	%rcx, -120(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-120(%rbp), %rcx
	movq	-128(%rbp), %r9
.L1707:
	movq	48(%rbx), %rdx
	movq	8(%rbx), %rsi
	movq	%rax, -111(%rbp)
	movq	40(%rbx), %rax
	subq	24(%rbx), %rax
	imull	$-252645135, %eax, %eax
	movq	(%rdx), %rdx
	movq	96(%rsi), %rsi
	movq	7(%rdx), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	%rsi, -1(%rax,%rdx)
	jmp	.L1705
	.p2align 4,,10
	.p2align 3
.L1748:
	cmpb	$0, 3(%rax)
	js	.L1749
	movl	$8, %r8d
	movl	$7, %edi
	movl	$9, %esi
	movl	$10, %r10d
	movl	$6, %r15d
	movl	$4, %edx
	jmp	.L1701
	.p2align 4,,10
	.p2align 3
.L1717:
	movq	41088(%rdx), %rax
	cmpq	41096(%rdx), %rax
	je	.L1750
.L1719:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%rdx)
	movq	%rsi, (%rax)
	jmp	.L1718
	.p2align 4,,10
	.p2align 3
.L1706:
	movq	41088(%rdx), %rax
	cmpq	41096(%rdx), %rax
	je	.L1751
.L1708:
	leaq	8(%rax), %rdi
	movq	%rdi, 41088(%rdx)
	movq	%rsi, (%rax)
	jmp	.L1707
	.p2align 4,,10
	.p2align 3
.L1712:
	movq	41088(%rdx), %rax
	cmpq	41096(%rdx), %rax
	je	.L1752
.L1714:
	leaq	8(%rax), %rdi
	movq	%rdi, 41088(%rdx)
	movq	%rsi, (%rax)
	jmp	.L1713
.L1752:
	movq	%rdx, %rdi
	movq	%rcx, -136(%rbp)
	movq	%rsi, -128(%rbp)
	movq	%rdx, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rcx
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	jmp	.L1714
.L1751:
	movq	%rdx, %rdi
	movq	%r9, -144(%rbp)
	movq	%rcx, -136(%rbp)
	movq	%rsi, -128(%rbp)
	movq	%rdx, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-144(%rbp), %r9
	movq	-136(%rbp), %rcx
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	jmp	.L1708
	.p2align 4,,10
	.p2align 3
.L1749:
	movsbl	4(%rax), %esi
	movsbq	4(%rax), %rdx
	movl	%esi, %r8d
	movl	%esi, %edi
	movl	%esi, %r10d
	movl	%esi, %r15d
	sarl	$31, %r8d
	sarl	$31, %edi
	sarl	$31, %r10d
	sarl	$31, %r15d
	notl	%r8d
	notl	%edi
	sarl	$31, %esi
	sarq	$63, %rdx
	notl	%r10d
	notl	%r15d
	notl	%esi
	notq	%rdx
	addl	$10, %r8d
	addl	$9, %edi
	addl	$12, %r10d
	addl	$8, %r15d
	addl	$11, %esi
	addq	$6, %rdx
	jmp	.L1701
	.p2align 4,,10
	.p2align 3
.L1750:
	movq	%rdx, %rdi
	movq	%rsi, -128(%rbp)
	movq	%rdx, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	jmp	.L1719
.L1721:
	xorl	%eax, %eax
	jmp	.L1722
.L1747:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22573:
	.size	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsIhjEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_, .-_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsIhjEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	.section	.text._ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsItjEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_,"axG",@progbits,_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsItjEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsItjEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	.type	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsItjEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_, @function
_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsItjEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_:
.LFB22576:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r9, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	16(%rbp), %rcx
	movq	24(%rbp), %r9
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	64(%rdx), %rax
	leaq	1(%rax,%r8), %rax
	cmpb	$0, 1(%rax)
	js	.L1795
	movl	$6, %r8d
	movl	$5, %edi
	movl	$7, %esi
	movl	$8, %r10d
	movl	$4, %r15d
	movl	$2, %edx
.L1754:
	addq	%rdx, %rax
	movzbl	(%rax), %edx
	movl	%edx, %r12d
	andl	$127, %r12d
	testb	%dl, %dl
	js	.L1796
.L1755:
	movabsq	$-1085102592571150095, %rsi
	movq	40(%rbx), %rax
	leaq	-17(%rax), %rdx
	movq	%rdx, %rdi
	subq	24(%rbx), %rdi
	imulq	%rdi, %rsi
	movq	%rsi, %rdi
	testq	%r9, %r9
	je	.L1756
	movq	%rdx, 40(%rbx)
	movdqu	-17(%rax), %xmm2
	movzbl	-1(%rax), %edx
	movzbl	-17(%rax), %eax
	movaps	%xmm2, -80(%rbp)
	movb	%dl, -64(%rbp)
	cmpb	$6, %al
	je	.L1757
	movb	%al, -80(%rbp)
	movdqa	-80(%rbp), %xmm4
	movb	%dl, -96(%rbp)
	movaps	%xmm4, -112(%rbp)
.L1758:
	movabsq	$-1085102592571150095, %rsi
	movl	-111(%rbp), %eax
	movw	%ax, (%r9)
	movq	40(%rbx), %rax
	leaq	-17(%rax), %rdx
	movq	%rdx, %rdi
	subq	24(%rbx), %rdi
	imulq	%rsi, %rdi
.L1756:
	testq	%rcx, %rcx
	je	.L1762
	movq	%rdx, 40(%rbx)
	movdqu	-17(%rax), %xmm3
	movzbl	-1(%rax), %edx
	movzbl	-17(%rax), %eax
	movaps	%xmm3, -80(%rbp)
	movb	%dl, -64(%rbp)
	cmpb	$6, %al
	je	.L1763
	movb	%al, -80(%rbp)
	movdqa	-80(%rbp), %xmm5
	movb	%dl, -96(%rbp)
	movaps	%xmm5, -112(%rbp)
.L1764:
	movl	-111(%rbp), %eax
	movw	%ax, (%rcx)
	movq	40(%rbx), %rax
	movabsq	$-1085102592571150095, %rcx
	leaq	-17(%rax), %rdx
	movq	%rdx, %rdi
	subq	24(%rbx), %rdi
	imulq	%rcx, %rdi
.L1762:
	movq	%rdx, 40(%rbx)
	movdqu	-17(%rax), %xmm0
	movzbl	-1(%rax), %edx
	movzbl	-17(%rax), %eax
	movaps	%xmm0, -80(%rbp)
	movb	%dl, -64(%rbp)
	cmpb	$6, %al
	je	.L1768
	movb	%al, -80(%rbp)
	movdqa	-80(%rbp), %xmm1
	movb	%dl, -96(%rbp)
	movaps	%xmm1, -112(%rbp)
.L1769:
	addl	-111(%rbp), %r12d
	jc	.L1774
	movq	16(%rbx), %rax
	xorl	%ecx, %ecx
	movq	(%rax), %rdx
	movq	31(%rdx), %rax
	cmpq	$1, %rax
	ja	.L1797
.L1775:
	movq	%rcx, 0(%r13)
	movl	%r15d, (%r14)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1798
	addq	$104, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1797:
	.cfi_restore_state
	movl	%r12d, %r12d
	subq	$2, %rax
	cmpq	%rax, %r12
	ja	.L1775
	movq	23(%rdx), %rcx
	andq	39(%rdx), %r12
	addq	%r12, %rcx
	jmp	.L1775
	.p2align 4,,10
	.p2align 3
.L1795:
	cmpb	$0, 2(%rax)
	js	.L1799
	movl	$7, %r8d
	movl	$6, %edi
	movl	$8, %esi
	movl	$9, %r10d
	movl	$5, %r15d
	movl	$3, %edx
	jmp	.L1754
	.p2align 4,,10
	.p2align 3
.L1796:
	movzbl	1(%rax), %r11d
	movl	%edi, %r15d
	movl	%r11d, %edx
	sall	$7, %edx
	andl	$16256, %edx
	orl	%edx, %r12d
	testb	%r11b, %r11b
	jns	.L1755
	movzbl	2(%rax), %edi
	movl	%r8d, %r15d
	movl	%edi, %edx
	sall	$14, %edx
	andl	$2080768, %edx
	orl	%edx, %r12d
	testb	%dil, %dil
	jns	.L1755
	movzbl	3(%rax), %edi
	movl	%esi, %r15d
	movl	%edi, %edx
	sall	$21, %edx
	andl	$266338304, %edx
	orl	%edx, %r12d
	testb	%dil, %dil
	jns	.L1755
	movzbl	4(%rax), %eax
	movl	%r10d, %r15d
	sall	$28, %eax
	orl	%eax, %r12d
	jmp	.L1755
	.p2align 4,,10
	.p2align 3
.L1768:
	movq	48(%rbx), %rax
	movq	8(%rbx), %rdx
	movq	(%rax), %rcx
	leal	16(,%rdi,8), %eax
	cltq
	movq	7(%rcx), %rcx
	movq	-1(%rax,%rcx), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L1770
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L1771:
	movq	48(%rbx), %rdx
	movq	8(%rbx), %rcx
	movq	%rax, -111(%rbp)
	movq	40(%rbx), %rax
	subq	24(%rbx), %rax
	imull	$-252645135, %eax, %eax
	movq	(%rdx), %rdx
	movq	96(%rcx), %rcx
	movq	7(%rdx), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	%rcx, -1(%rax,%rdx)
	jmp	.L1769
	.p2align 4,,10
	.p2align 3
.L1763:
	movq	48(%rbx), %rax
	movq	8(%rbx), %rdx
	movq	(%rax), %rsi
	leal	16(,%rdi,8), %eax
	cltq
	movq	7(%rsi), %rsi
	movq	-1(%rax,%rsi), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L1765
	movq	%rcx, -120(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-120(%rbp), %rcx
.L1766:
	movq	48(%rbx), %rdx
	movq	8(%rbx), %rsi
	movq	%rax, -111(%rbp)
	movq	40(%rbx), %rax
	subq	24(%rbx), %rax
	imull	$-252645135, %eax, %eax
	movq	(%rdx), %rdx
	movq	96(%rsi), %rsi
	movq	7(%rdx), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	%rsi, -1(%rax,%rdx)
	jmp	.L1764
	.p2align 4,,10
	.p2align 3
.L1757:
	movq	48(%rbx), %rax
	movq	8(%rbx), %rdx
	movq	(%rax), %rdi
	leal	16(,%rsi,8), %eax
	cltq
	movq	7(%rdi), %rsi
	movq	-1(%rax,%rsi), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L1759
	movq	%r9, -128(%rbp)
	movq	%rcx, -120(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-120(%rbp), %rcx
	movq	-128(%rbp), %r9
.L1760:
	movq	48(%rbx), %rdx
	movq	8(%rbx), %rsi
	movq	%rax, -111(%rbp)
	movq	40(%rbx), %rax
	subq	24(%rbx), %rax
	imull	$-252645135, %eax, %eax
	movq	(%rdx), %rdx
	movq	96(%rsi), %rsi
	movq	7(%rdx), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	%rsi, -1(%rax,%rdx)
	jmp	.L1758
	.p2align 4,,10
	.p2align 3
.L1799:
	cmpb	$0, 3(%rax)
	js	.L1800
	movl	$8, %r8d
	movl	$7, %edi
	movl	$9, %esi
	movl	$10, %r10d
	movl	$6, %r15d
	movl	$4, %edx
	jmp	.L1754
	.p2align 4,,10
	.p2align 3
.L1770:
	movq	41088(%rdx), %rax
	cmpq	41096(%rdx), %rax
	je	.L1801
.L1772:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%rdx)
	movq	%rsi, (%rax)
	jmp	.L1771
	.p2align 4,,10
	.p2align 3
.L1759:
	movq	41088(%rdx), %rax
	cmpq	41096(%rdx), %rax
	je	.L1802
.L1761:
	leaq	8(%rax), %rdi
	movq	%rdi, 41088(%rdx)
	movq	%rsi, (%rax)
	jmp	.L1760
	.p2align 4,,10
	.p2align 3
.L1765:
	movq	41088(%rdx), %rax
	cmpq	41096(%rdx), %rax
	je	.L1803
.L1767:
	leaq	8(%rax), %rdi
	movq	%rdi, 41088(%rdx)
	movq	%rsi, (%rax)
	jmp	.L1766
.L1803:
	movq	%rdx, %rdi
	movq	%rcx, -136(%rbp)
	movq	%rsi, -128(%rbp)
	movq	%rdx, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rcx
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	jmp	.L1767
.L1802:
	movq	%rdx, %rdi
	movq	%r9, -144(%rbp)
	movq	%rcx, -136(%rbp)
	movq	%rsi, -128(%rbp)
	movq	%rdx, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-144(%rbp), %r9
	movq	-136(%rbp), %rcx
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	jmp	.L1761
	.p2align 4,,10
	.p2align 3
.L1800:
	movsbl	4(%rax), %esi
	movsbq	4(%rax), %rdx
	movl	%esi, %r8d
	movl	%esi, %edi
	movl	%esi, %r10d
	movl	%esi, %r15d
	sarl	$31, %r8d
	sarl	$31, %edi
	sarl	$31, %r10d
	sarl	$31, %r15d
	notl	%r8d
	notl	%edi
	sarl	$31, %esi
	sarq	$63, %rdx
	notl	%r10d
	notl	%r15d
	notl	%esi
	notq	%rdx
	addl	$10, %r8d
	addl	$9, %edi
	addl	$12, %r10d
	addl	$8, %r15d
	addl	$11, %esi
	addq	$6, %rdx
	jmp	.L1754
	.p2align 4,,10
	.p2align 3
.L1801:
	movq	%rdx, %rdi
	movq	%rsi, -128(%rbp)
	movq	%rdx, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	jmp	.L1772
.L1774:
	xorl	%ecx, %ecx
	jmp	.L1775
.L1798:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22576:
	.size	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsItjEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_, .-_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsItjEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	.section	.text._ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsImmEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_,"axG",@progbits,_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsImmEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsImmEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	.type	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsImmEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_, @function
_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsImmEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_:
.LFB22609:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r9, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	16(%rbp), %r9
	movq	24(%rbp), %r10
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	64(%rdx), %rax
	leaq	1(%rax,%r8), %rax
	cmpb	$0, 1(%rax)
	js	.L1846
	movl	$6, %edi
	movl	$5, %esi
	movl	$7, %ecx
	movl	$8, %r8d
	movl	$4, %r15d
	movl	$2, %edx
.L1805:
	addq	%rdx, %rax
	movzbl	(%rax), %edx
	movl	%edx, %r12d
	andl	$127, %r12d
	testb	%dl, %dl
	js	.L1847
.L1806:
	movabsq	$-1085102592571150095, %rcx
	movq	40(%rbx), %rax
	leaq	-17(%rax), %rdx
	movq	%rdx, %rsi
	subq	24(%rbx), %rsi
	imulq	%rsi, %rcx
	movq	%rcx, %rsi
	testq	%r10, %r10
	je	.L1807
	movq	%rdx, 40(%rbx)
	movdqu	-17(%rax), %xmm2
	movzbl	-1(%rax), %edx
	cmpb	$6, -17(%rax)
	movaps	%xmm2, -80(%rbp)
	movb	%dl, -64(%rbp)
	je	.L1808
	movq	-79(%rbp), %rax
.L1809:
	movabsq	$-1085102592571150095, %rcx
	movq	%rax, (%r10)
	movq	40(%rbx), %rax
	leaq	-17(%rax), %rdx
	movq	%rdx, %rsi
	subq	24(%rbx), %rsi
	imulq	%rcx, %rsi
.L1807:
	testq	%r9, %r9
	je	.L1813
	movq	%rdx, 40(%rbx)
	movdqu	-17(%rax), %xmm3
	movzbl	-1(%rax), %edx
	cmpb	$6, -17(%rax)
	movaps	%xmm3, -80(%rbp)
	movb	%dl, -64(%rbp)
	je	.L1814
	movq	-79(%rbp), %rax
.L1815:
	movabsq	$-1085102592571150095, %rcx
	movq	%rax, (%r9)
	movq	40(%rbx), %rax
	leaq	-17(%rax), %rdx
	movq	%rdx, %rsi
	subq	24(%rbx), %rsi
	imulq	%rcx, %rsi
.L1813:
	movq	%rdx, 40(%rbx)
	movdqu	-17(%rax), %xmm0
	movzbl	-1(%rax), %edx
	movzbl	-17(%rax), %eax
	movaps	%xmm0, -80(%rbp)
	movb	%dl, -64(%rbp)
	cmpb	$6, %al
	je	.L1819
	movb	%al, -80(%rbp)
	movdqa	-80(%rbp), %xmm1
	movb	%dl, -96(%rbp)
	movaps	%xmm1, -112(%rbp)
.L1820:
	addl	-111(%rbp), %r12d
	jc	.L1825
	movq	16(%rbx), %rax
	xorl	%ecx, %ecx
	movq	(%rax), %rdx
	movq	31(%rdx), %rax
	cmpq	$7, %rax
	ja	.L1848
.L1826:
	movq	%rcx, 0(%r13)
	movl	%r15d, (%r14)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1849
	addq	$104, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1848:
	.cfi_restore_state
	movl	%r12d, %r12d
	subq	$8, %rax
	cmpq	%rax, %r12
	ja	.L1826
	movq	23(%rdx), %rcx
	andq	39(%rdx), %r12
	addq	%r12, %rcx
	jmp	.L1826
	.p2align 4,,10
	.p2align 3
.L1846:
	cmpb	$0, 2(%rax)
	js	.L1850
	movl	$7, %edi
	movl	$6, %esi
	movl	$8, %ecx
	movl	$9, %r8d
	movl	$5, %r15d
	movl	$3, %edx
	jmp	.L1805
	.p2align 4,,10
	.p2align 3
.L1847:
	movzbl	1(%rax), %r11d
	movl	%esi, %r15d
	movl	%r11d, %edx
	sall	$7, %edx
	andl	$16256, %edx
	orl	%edx, %r12d
	testb	%r11b, %r11b
	jns	.L1806
	movzbl	2(%rax), %esi
	movl	%edi, %r15d
	movl	%esi, %edx
	sall	$14, %edx
	andl	$2080768, %edx
	orl	%edx, %r12d
	testb	%sil, %sil
	jns	.L1806
	movzbl	3(%rax), %esi
	movl	%ecx, %r15d
	movl	%esi, %edx
	sall	$21, %edx
	andl	$266338304, %edx
	orl	%edx, %r12d
	testb	%sil, %sil
	jns	.L1806
	movzbl	4(%rax), %eax
	movl	%r8d, %r15d
	sall	$28, %eax
	orl	%eax, %r12d
	jmp	.L1806
	.p2align 4,,10
	.p2align 3
.L1819:
	movq	48(%rbx), %rax
	movq	8(%rbx), %rdx
	movq	(%rax), %rcx
	leal	16(,%rsi,8), %eax
	cltq
	movq	7(%rcx), %rcx
	movq	-1(%rax,%rcx), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L1821
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L1822:
	movq	48(%rbx), %rdx
	movq	8(%rbx), %rcx
	movq	%rax, -111(%rbp)
	movq	40(%rbx), %rax
	subq	24(%rbx), %rax
	imull	$-252645135, %eax, %eax
	movq	(%rdx), %rdx
	movq	96(%rcx), %rcx
	movq	7(%rdx), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	%rcx, -1(%rax,%rdx)
	jmp	.L1820
	.p2align 4,,10
	.p2align 3
.L1814:
	movq	48(%rbx), %rax
	movq	8(%rbx), %rdx
	movq	(%rax), %rcx
	leal	16(,%rsi,8), %eax
	cltq
	movq	7(%rcx), %rcx
	movq	-1(%rax,%rcx), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L1816
	movq	%r9, -120(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-120(%rbp), %r9
.L1817:
	movq	40(%rbx), %rdx
	movq	48(%rbx), %rcx
	subq	24(%rbx), %rdx
	movq	8(%rbx), %rsi
	imull	$-252645135, %edx, %edx
	movq	(%rcx), %rcx
	movq	96(%rsi), %rsi
	movq	7(%rcx), %rcx
	leal	16(,%rdx,8), %edx
	movslq	%edx, %rdx
	movq	%rsi, -1(%rdx,%rcx)
	jmp	.L1815
	.p2align 4,,10
	.p2align 3
.L1808:
	movq	48(%rbx), %rax
	movq	8(%rbx), %rdx
	movq	(%rax), %rsi
	leal	16(,%rcx,8), %eax
	cltq
	movq	7(%rsi), %rcx
	movq	-1(%rax,%rcx), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L1810
	movq	%r10, -128(%rbp)
	movq	%r9, -120(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-120(%rbp), %r9
	movq	-128(%rbp), %r10
.L1811:
	movq	40(%rbx), %rdx
	movq	48(%rbx), %rcx
	subq	24(%rbx), %rdx
	movq	8(%rbx), %rsi
	imull	$-252645135, %edx, %edx
	movq	(%rcx), %rcx
	movq	96(%rsi), %rsi
	movq	7(%rcx), %rcx
	leal	16(,%rdx,8), %edx
	movslq	%edx, %rdx
	movq	%rsi, -1(%rdx,%rcx)
	jmp	.L1809
	.p2align 4,,10
	.p2align 3
.L1850:
	cmpb	$0, 3(%rax)
	js	.L1851
	movl	$8, %edi
	movl	$7, %esi
	movl	$9, %ecx
	movl	$10, %r8d
	movl	$6, %r15d
	movl	$4, %edx
	jmp	.L1805
	.p2align 4,,10
	.p2align 3
.L1821:
	movq	41088(%rdx), %rax
	cmpq	41096(%rdx), %rax
	je	.L1852
.L1823:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%rdx)
	movq	%rsi, (%rax)
	jmp	.L1822
	.p2align 4,,10
	.p2align 3
.L1810:
	movq	41088(%rdx), %rax
	cmpq	41096(%rdx), %rax
	je	.L1853
.L1812:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%rdx)
	movq	%rsi, (%rax)
	jmp	.L1811
	.p2align 4,,10
	.p2align 3
.L1816:
	movq	41088(%rdx), %rax
	cmpq	41096(%rdx), %rax
	je	.L1854
.L1818:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%rdx)
	movq	%rsi, (%rax)
	jmp	.L1817
.L1854:
	movq	%rdx, %rdi
	movq	%r9, -136(%rbp)
	movq	%rsi, -128(%rbp)
	movq	%rdx, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %r9
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	jmp	.L1818
.L1853:
	movq	%rdx, %rdi
	movq	%r10, -144(%rbp)
	movq	%r9, -136(%rbp)
	movq	%rsi, -128(%rbp)
	movq	%rdx, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-144(%rbp), %r10
	movq	-136(%rbp), %r9
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	jmp	.L1812
	.p2align 4,,10
	.p2align 3
.L1851:
	movsbl	4(%rax), %ecx
	movsbq	4(%rax), %rdx
	movl	%ecx, %edi
	movl	%ecx, %esi
	movl	%ecx, %r8d
	movl	%ecx, %r15d
	sarl	$31, %edi
	sarl	$31, %esi
	sarl	$31, %r8d
	sarl	$31, %r15d
	notl	%edi
	notl	%esi
	sarl	$31, %ecx
	sarq	$63, %rdx
	notl	%r8d
	notl	%r15d
	notl	%ecx
	notq	%rdx
	addl	$10, %edi
	addl	$9, %esi
	addl	$12, %r8d
	addl	$8, %r15d
	addl	$11, %ecx
	addq	$6, %rdx
	jmp	.L1805
	.p2align 4,,10
	.p2align 3
.L1852:
	movq	%rdx, %rdi
	movq	%rsi, -128(%rbp)
	movq	%rdx, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	jmp	.L1823
.L1825:
	xorl	%ecx, %ecx
	jmp	.L1826
.L1849:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22609:
	.size	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsImmEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_, .-_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsImmEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	.section	.text._ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsIhmEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_,"axG",@progbits,_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsIhmEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsIhmEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	.type	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsIhmEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_, @function
_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsIhmEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_:
.LFB22612:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r9, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	16(%rbp), %r9
	movq	24(%rbp), %r10
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	64(%rdx), %rax
	leaq	1(%rax,%r8), %rax
	cmpb	$0, 1(%rax)
	js	.L1899
	movl	$6, %edi
	movl	$5, %esi
	movl	$7, %ecx
	movl	$8, %r8d
	movl	$4, %r15d
	movl	$2, %edx
.L1856:
	addq	%rdx, %rax
	movzbl	(%rax), %edx
	movl	%edx, %r12d
	andl	$127, %r12d
	testb	%dl, %dl
	js	.L1900
.L1857:
	movabsq	$-1085102592571150095, %rcx
	movq	40(%rbx), %rax
	leaq	-17(%rax), %rdx
	movq	%rdx, %rsi
	subq	24(%rbx), %rsi
	imulq	%rsi, %rcx
	movq	%rcx, %rsi
	testq	%r10, %r10
	je	.L1858
	movq	%rdx, 40(%rbx)
	movdqu	-17(%rax), %xmm2
	movzbl	-1(%rax), %edx
	cmpb	$6, -17(%rax)
	movaps	%xmm2, -80(%rbp)
	movb	%dl, -64(%rbp)
	je	.L1859
	movq	-79(%rbp), %rax
.L1860:
	movabsq	$-1085102592571150095, %rcx
	movb	%al, (%r10)
	movq	40(%rbx), %rax
	leaq	-17(%rax), %rdx
	movq	%rdx, %rsi
	subq	24(%rbx), %rsi
	imulq	%rcx, %rsi
.L1858:
	testq	%r9, %r9
	je	.L1864
	movq	%rdx, 40(%rbx)
	movdqu	-17(%rax), %xmm3
	movzbl	-1(%rax), %edx
	cmpb	$6, -17(%rax)
	movaps	%xmm3, -80(%rbp)
	movb	%dl, -64(%rbp)
	je	.L1865
	movq	-79(%rbp), %rax
.L1866:
	movabsq	$-1085102592571150095, %rcx
	movb	%al, (%r9)
	movq	40(%rbx), %rax
	leaq	-17(%rax), %rdx
	movq	%rdx, %rsi
	subq	24(%rbx), %rsi
	imulq	%rcx, %rsi
.L1864:
	movq	%rdx, 40(%rbx)
	movdqu	-17(%rax), %xmm0
	movzbl	-1(%rax), %edx
	movzbl	-17(%rax), %eax
	movaps	%xmm0, -80(%rbp)
	movb	%dl, -64(%rbp)
	cmpb	$6, %al
	je	.L1870
	movb	%al, -80(%rbp)
	movdqa	-80(%rbp), %xmm1
	movb	%dl, -96(%rbp)
	movaps	%xmm1, -112(%rbp)
.L1871:
	addl	-111(%rbp), %r12d
	jc	.L1876
	movq	16(%rbx), %rax
	movq	(%rax), %rdx
	movq	31(%rdx), %rax
	testq	%rax, %rax
	jne	.L1901
.L1877:
	movq	%rax, 0(%r13)
	movl	%r15d, (%r14)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1902
	addq	$104, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1901:
	.cfi_restore_state
	leaq	-1(%rax), %rcx
	movl	%r12d, %r12d
	xorl	%eax, %eax
	cmpq	%rcx, %r12
	ja	.L1877
	movq	23(%rdx), %rax
	andq	39(%rdx), %r12
	addq	%r12, %rax
	jmp	.L1877
	.p2align 4,,10
	.p2align 3
.L1899:
	cmpb	$0, 2(%rax)
	js	.L1903
	movl	$7, %edi
	movl	$6, %esi
	movl	$8, %ecx
	movl	$9, %r8d
	movl	$5, %r15d
	movl	$3, %edx
	jmp	.L1856
	.p2align 4,,10
	.p2align 3
.L1900:
	movzbl	1(%rax), %r11d
	movl	%esi, %r15d
	movl	%r11d, %edx
	sall	$7, %edx
	andl	$16256, %edx
	orl	%edx, %r12d
	testb	%r11b, %r11b
	jns	.L1857
	movzbl	2(%rax), %esi
	movl	%edi, %r15d
	movl	%esi, %edx
	sall	$14, %edx
	andl	$2080768, %edx
	orl	%edx, %r12d
	testb	%sil, %sil
	jns	.L1857
	movzbl	3(%rax), %esi
	movl	%ecx, %r15d
	movl	%esi, %edx
	sall	$21, %edx
	andl	$266338304, %edx
	orl	%edx, %r12d
	testb	%sil, %sil
	jns	.L1857
	movzbl	4(%rax), %eax
	movl	%r8d, %r15d
	sall	$28, %eax
	orl	%eax, %r12d
	jmp	.L1857
	.p2align 4,,10
	.p2align 3
.L1870:
	movq	48(%rbx), %rax
	movq	8(%rbx), %rdx
	movq	(%rax), %rcx
	leal	16(,%rsi,8), %eax
	cltq
	movq	7(%rcx), %rcx
	movq	-1(%rax,%rcx), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L1872
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L1873:
	movq	48(%rbx), %rdx
	movq	8(%rbx), %rcx
	movq	%rax, -111(%rbp)
	movq	40(%rbx), %rax
	subq	24(%rbx), %rax
	imull	$-252645135, %eax, %eax
	movq	(%rdx), %rdx
	movq	96(%rcx), %rcx
	movq	7(%rdx), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	%rcx, -1(%rax,%rdx)
	jmp	.L1871
	.p2align 4,,10
	.p2align 3
.L1865:
	movq	48(%rbx), %rax
	movq	8(%rbx), %rdx
	movq	(%rax), %rcx
	leal	16(,%rsi,8), %eax
	cltq
	movq	7(%rcx), %rcx
	movq	-1(%rax,%rcx), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L1867
	movq	%r9, -120(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-120(%rbp), %r9
.L1868:
	movq	40(%rbx), %rdx
	movq	48(%rbx), %rcx
	subq	24(%rbx), %rdx
	movq	8(%rbx), %rsi
	imull	$-252645135, %edx, %edx
	movq	(%rcx), %rcx
	movq	96(%rsi), %rsi
	movq	7(%rcx), %rcx
	leal	16(,%rdx,8), %edx
	movslq	%edx, %rdx
	movq	%rsi, -1(%rdx,%rcx)
	jmp	.L1866
	.p2align 4,,10
	.p2align 3
.L1859:
	movq	48(%rbx), %rax
	movq	8(%rbx), %rdx
	movq	(%rax), %rsi
	leal	16(,%rcx,8), %eax
	cltq
	movq	7(%rsi), %rcx
	movq	-1(%rax,%rcx), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L1861
	movq	%r10, -128(%rbp)
	movq	%r9, -120(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-120(%rbp), %r9
	movq	-128(%rbp), %r10
.L1862:
	movq	40(%rbx), %rdx
	movq	48(%rbx), %rcx
	subq	24(%rbx), %rdx
	movq	8(%rbx), %rsi
	imull	$-252645135, %edx, %edx
	movq	(%rcx), %rcx
	movq	96(%rsi), %rsi
	movq	7(%rcx), %rcx
	leal	16(,%rdx,8), %edx
	movslq	%edx, %rdx
	movq	%rsi, -1(%rdx,%rcx)
	jmp	.L1860
	.p2align 4,,10
	.p2align 3
.L1903:
	cmpb	$0, 3(%rax)
	js	.L1904
	movl	$8, %edi
	movl	$7, %esi
	movl	$9, %ecx
	movl	$10, %r8d
	movl	$6, %r15d
	movl	$4, %edx
	jmp	.L1856
	.p2align 4,,10
	.p2align 3
.L1872:
	movq	41088(%rdx), %rax
	cmpq	41096(%rdx), %rax
	je	.L1905
.L1874:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%rdx)
	movq	%rsi, (%rax)
	jmp	.L1873
	.p2align 4,,10
	.p2align 3
.L1861:
	movq	41088(%rdx), %rax
	cmpq	41096(%rdx), %rax
	je	.L1906
.L1863:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%rdx)
	movq	%rsi, (%rax)
	jmp	.L1862
	.p2align 4,,10
	.p2align 3
.L1867:
	movq	41088(%rdx), %rax
	cmpq	41096(%rdx), %rax
	je	.L1907
.L1869:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%rdx)
	movq	%rsi, (%rax)
	jmp	.L1868
.L1907:
	movq	%rdx, %rdi
	movq	%r9, -136(%rbp)
	movq	%rsi, -128(%rbp)
	movq	%rdx, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %r9
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	jmp	.L1869
.L1906:
	movq	%rdx, %rdi
	movq	%r10, -144(%rbp)
	movq	%r9, -136(%rbp)
	movq	%rsi, -128(%rbp)
	movq	%rdx, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-144(%rbp), %r10
	movq	-136(%rbp), %r9
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	jmp	.L1863
	.p2align 4,,10
	.p2align 3
.L1904:
	movsbl	4(%rax), %ecx
	movsbq	4(%rax), %rdx
	movl	%ecx, %edi
	movl	%ecx, %esi
	movl	%ecx, %r8d
	movl	%ecx, %r15d
	sarl	$31, %edi
	sarl	$31, %esi
	sarl	$31, %r8d
	sarl	$31, %r15d
	notl	%edi
	notl	%esi
	sarl	$31, %ecx
	sarq	$63, %rdx
	notl	%r8d
	notl	%r15d
	notl	%ecx
	notq	%rdx
	addl	$10, %edi
	addl	$9, %esi
	addl	$12, %r8d
	addl	$8, %r15d
	addl	$11, %ecx
	addq	$6, %rdx
	jmp	.L1856
	.p2align 4,,10
	.p2align 3
.L1905:
	movq	%rdx, %rdi
	movq	%rsi, -128(%rbp)
	movq	%rdx, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	jmp	.L1874
.L1876:
	xorl	%eax, %eax
	jmp	.L1877
.L1902:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22612:
	.size	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsIhmEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_, .-_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsIhmEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	.section	.text._ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsItmEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_,"axG",@progbits,_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsItmEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsItmEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	.type	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsItmEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_, @function
_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsItmEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_:
.LFB22614:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r9, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	16(%rbp), %r9
	movq	24(%rbp), %r10
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	64(%rdx), %rax
	leaq	1(%rax,%r8), %rax
	cmpb	$0, 1(%rax)
	js	.L1950
	movl	$6, %edi
	movl	$5, %esi
	movl	$7, %ecx
	movl	$8, %r8d
	movl	$4, %r15d
	movl	$2, %edx
.L1909:
	addq	%rdx, %rax
	movzbl	(%rax), %edx
	movl	%edx, %r12d
	andl	$127, %r12d
	testb	%dl, %dl
	js	.L1951
.L1910:
	movabsq	$-1085102592571150095, %rcx
	movq	40(%rbx), %rax
	leaq	-17(%rax), %rdx
	movq	%rdx, %rsi
	subq	24(%rbx), %rsi
	imulq	%rsi, %rcx
	movq	%rcx, %rsi
	testq	%r10, %r10
	je	.L1911
	movq	%rdx, 40(%rbx)
	movdqu	-17(%rax), %xmm2
	movzbl	-1(%rax), %edx
	cmpb	$6, -17(%rax)
	movaps	%xmm2, -80(%rbp)
	movb	%dl, -64(%rbp)
	je	.L1912
	movq	-79(%rbp), %rax
.L1913:
	movabsq	$-1085102592571150095, %rcx
	movw	%ax, (%r10)
	movq	40(%rbx), %rax
	leaq	-17(%rax), %rdx
	movq	%rdx, %rsi
	subq	24(%rbx), %rsi
	imulq	%rcx, %rsi
.L1911:
	testq	%r9, %r9
	je	.L1917
	movq	%rdx, 40(%rbx)
	movdqu	-17(%rax), %xmm3
	movzbl	-1(%rax), %edx
	cmpb	$6, -17(%rax)
	movaps	%xmm3, -80(%rbp)
	movb	%dl, -64(%rbp)
	je	.L1918
	movq	-79(%rbp), %rax
.L1919:
	movabsq	$-1085102592571150095, %rcx
	movw	%ax, (%r9)
	movq	40(%rbx), %rax
	leaq	-17(%rax), %rdx
	movq	%rdx, %rsi
	subq	24(%rbx), %rsi
	imulq	%rcx, %rsi
.L1917:
	movq	%rdx, 40(%rbx)
	movdqu	-17(%rax), %xmm0
	movzbl	-1(%rax), %edx
	movzbl	-17(%rax), %eax
	movaps	%xmm0, -80(%rbp)
	movb	%dl, -64(%rbp)
	cmpb	$6, %al
	je	.L1923
	movb	%al, -80(%rbp)
	movdqa	-80(%rbp), %xmm1
	movb	%dl, -96(%rbp)
	movaps	%xmm1, -112(%rbp)
.L1924:
	addl	-111(%rbp), %r12d
	jc	.L1929
	movq	16(%rbx), %rax
	xorl	%ecx, %ecx
	movq	(%rax), %rdx
	movq	31(%rdx), %rax
	cmpq	$1, %rax
	ja	.L1952
.L1930:
	movq	%rcx, 0(%r13)
	movl	%r15d, (%r14)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1953
	addq	$104, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1952:
	.cfi_restore_state
	movl	%r12d, %r12d
	subq	$2, %rax
	cmpq	%rax, %r12
	ja	.L1930
	movq	23(%rdx), %rcx
	andq	39(%rdx), %r12
	addq	%r12, %rcx
	jmp	.L1930
	.p2align 4,,10
	.p2align 3
.L1950:
	cmpb	$0, 2(%rax)
	js	.L1954
	movl	$7, %edi
	movl	$6, %esi
	movl	$8, %ecx
	movl	$9, %r8d
	movl	$5, %r15d
	movl	$3, %edx
	jmp	.L1909
	.p2align 4,,10
	.p2align 3
.L1951:
	movzbl	1(%rax), %r11d
	movl	%esi, %r15d
	movl	%r11d, %edx
	sall	$7, %edx
	andl	$16256, %edx
	orl	%edx, %r12d
	testb	%r11b, %r11b
	jns	.L1910
	movzbl	2(%rax), %esi
	movl	%edi, %r15d
	movl	%esi, %edx
	sall	$14, %edx
	andl	$2080768, %edx
	orl	%edx, %r12d
	testb	%sil, %sil
	jns	.L1910
	movzbl	3(%rax), %esi
	movl	%ecx, %r15d
	movl	%esi, %edx
	sall	$21, %edx
	andl	$266338304, %edx
	orl	%edx, %r12d
	testb	%sil, %sil
	jns	.L1910
	movzbl	4(%rax), %eax
	movl	%r8d, %r15d
	sall	$28, %eax
	orl	%eax, %r12d
	jmp	.L1910
	.p2align 4,,10
	.p2align 3
.L1923:
	movq	48(%rbx), %rax
	movq	8(%rbx), %rdx
	movq	(%rax), %rcx
	leal	16(,%rsi,8), %eax
	cltq
	movq	7(%rcx), %rcx
	movq	-1(%rax,%rcx), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L1925
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L1926:
	movq	48(%rbx), %rdx
	movq	8(%rbx), %rcx
	movq	%rax, -111(%rbp)
	movq	40(%rbx), %rax
	subq	24(%rbx), %rax
	imull	$-252645135, %eax, %eax
	movq	(%rdx), %rdx
	movq	96(%rcx), %rcx
	movq	7(%rdx), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	%rcx, -1(%rax,%rdx)
	jmp	.L1924
	.p2align 4,,10
	.p2align 3
.L1918:
	movq	48(%rbx), %rax
	movq	8(%rbx), %rdx
	movq	(%rax), %rcx
	leal	16(,%rsi,8), %eax
	cltq
	movq	7(%rcx), %rcx
	movq	-1(%rax,%rcx), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L1920
	movq	%r9, -120(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-120(%rbp), %r9
.L1921:
	movq	40(%rbx), %rdx
	movq	48(%rbx), %rcx
	subq	24(%rbx), %rdx
	movq	8(%rbx), %rsi
	imull	$-252645135, %edx, %edx
	movq	(%rcx), %rcx
	movq	96(%rsi), %rsi
	movq	7(%rcx), %rcx
	leal	16(,%rdx,8), %edx
	movslq	%edx, %rdx
	movq	%rsi, -1(%rdx,%rcx)
	jmp	.L1919
	.p2align 4,,10
	.p2align 3
.L1912:
	movq	48(%rbx), %rax
	movq	8(%rbx), %rdx
	movq	(%rax), %rsi
	leal	16(,%rcx,8), %eax
	cltq
	movq	7(%rsi), %rcx
	movq	-1(%rax,%rcx), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L1914
	movq	%r10, -128(%rbp)
	movq	%r9, -120(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-120(%rbp), %r9
	movq	-128(%rbp), %r10
.L1915:
	movq	40(%rbx), %rdx
	movq	48(%rbx), %rcx
	subq	24(%rbx), %rdx
	movq	8(%rbx), %rsi
	imull	$-252645135, %edx, %edx
	movq	(%rcx), %rcx
	movq	96(%rsi), %rsi
	movq	7(%rcx), %rcx
	leal	16(,%rdx,8), %edx
	movslq	%edx, %rdx
	movq	%rsi, -1(%rdx,%rcx)
	jmp	.L1913
	.p2align 4,,10
	.p2align 3
.L1954:
	cmpb	$0, 3(%rax)
	js	.L1955
	movl	$8, %edi
	movl	$7, %esi
	movl	$9, %ecx
	movl	$10, %r8d
	movl	$6, %r15d
	movl	$4, %edx
	jmp	.L1909
	.p2align 4,,10
	.p2align 3
.L1925:
	movq	41088(%rdx), %rax
	cmpq	41096(%rdx), %rax
	je	.L1956
.L1927:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%rdx)
	movq	%rsi, (%rax)
	jmp	.L1926
	.p2align 4,,10
	.p2align 3
.L1914:
	movq	41088(%rdx), %rax
	cmpq	41096(%rdx), %rax
	je	.L1957
.L1916:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%rdx)
	movq	%rsi, (%rax)
	jmp	.L1915
	.p2align 4,,10
	.p2align 3
.L1920:
	movq	41088(%rdx), %rax
	cmpq	41096(%rdx), %rax
	je	.L1958
.L1922:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%rdx)
	movq	%rsi, (%rax)
	jmp	.L1921
.L1958:
	movq	%rdx, %rdi
	movq	%r9, -136(%rbp)
	movq	%rsi, -128(%rbp)
	movq	%rdx, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %r9
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	jmp	.L1922
.L1957:
	movq	%rdx, %rdi
	movq	%r10, -144(%rbp)
	movq	%r9, -136(%rbp)
	movq	%rsi, -128(%rbp)
	movq	%rdx, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-144(%rbp), %r10
	movq	-136(%rbp), %r9
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	jmp	.L1916
	.p2align 4,,10
	.p2align 3
.L1955:
	movsbl	4(%rax), %ecx
	movsbq	4(%rax), %rdx
	movl	%ecx, %edi
	movl	%ecx, %esi
	movl	%ecx, %r8d
	movl	%ecx, %r15d
	sarl	$31, %edi
	sarl	$31, %esi
	sarl	$31, %r8d
	sarl	$31, %r15d
	notl	%edi
	notl	%esi
	sarl	$31, %ecx
	sarq	$63, %rdx
	notl	%r8d
	notl	%r15d
	notl	%ecx
	notq	%rdx
	addl	$10, %edi
	addl	$9, %esi
	addl	$12, %r8d
	addl	$8, %r15d
	addl	$11, %ecx
	addq	$6, %rdx
	jmp	.L1909
	.p2align 4,,10
	.p2align 3
.L1956:
	movq	%rdx, %rdi
	movq	%rsi, -128(%rbp)
	movq	%rdx, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	jmp	.L1927
.L1929:
	xorl	%ecx, %ecx
	jmp	.L1930
.L1953:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22614:
	.size	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsItmEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_, .-_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsItmEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	.section	.text._ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsIjmEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_,"axG",@progbits,_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsIjmEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsIjmEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	.type	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsIjmEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_, @function
_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsIjmEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_:
.LFB22616:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r9, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	16(%rbp), %r9
	movq	24(%rbp), %r10
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	64(%rdx), %rax
	leaq	1(%rax,%r8), %rax
	cmpb	$0, 1(%rax)
	js	.L2001
	movl	$6, %edi
	movl	$5, %esi
	movl	$7, %ecx
	movl	$8, %r8d
	movl	$4, %r15d
	movl	$2, %edx
.L1960:
	addq	%rdx, %rax
	movzbl	(%rax), %edx
	movl	%edx, %r12d
	andl	$127, %r12d
	testb	%dl, %dl
	js	.L2002
.L1961:
	movabsq	$-1085102592571150095, %rcx
	movq	40(%rbx), %rax
	leaq	-17(%rax), %rdx
	movq	%rdx, %rsi
	subq	24(%rbx), %rsi
	imulq	%rsi, %rcx
	movq	%rcx, %rsi
	testq	%r10, %r10
	je	.L1962
	movq	%rdx, 40(%rbx)
	movdqu	-17(%rax), %xmm2
	movzbl	-1(%rax), %edx
	cmpb	$6, -17(%rax)
	movaps	%xmm2, -80(%rbp)
	movb	%dl, -64(%rbp)
	je	.L1963
	movq	-79(%rbp), %rax
.L1964:
	movabsq	$-1085102592571150095, %rcx
	movl	%eax, (%r10)
	movq	40(%rbx), %rax
	leaq	-17(%rax), %rdx
	movq	%rdx, %rsi
	subq	24(%rbx), %rsi
	imulq	%rcx, %rsi
.L1962:
	testq	%r9, %r9
	je	.L1968
	movq	%rdx, 40(%rbx)
	movdqu	-17(%rax), %xmm3
	movzbl	-1(%rax), %edx
	cmpb	$6, -17(%rax)
	movaps	%xmm3, -80(%rbp)
	movb	%dl, -64(%rbp)
	je	.L1969
	movq	-79(%rbp), %rax
.L1970:
	movabsq	$-1085102592571150095, %rcx
	movl	%eax, (%r9)
	movq	40(%rbx), %rax
	leaq	-17(%rax), %rdx
	movq	%rdx, %rsi
	subq	24(%rbx), %rsi
	imulq	%rcx, %rsi
.L1968:
	movq	%rdx, 40(%rbx)
	movdqu	-17(%rax), %xmm0
	movzbl	-1(%rax), %edx
	movzbl	-17(%rax), %eax
	movaps	%xmm0, -80(%rbp)
	movb	%dl, -64(%rbp)
	cmpb	$6, %al
	je	.L1974
	movb	%al, -80(%rbp)
	movdqa	-80(%rbp), %xmm1
	movb	%dl, -96(%rbp)
	movaps	%xmm1, -112(%rbp)
.L1975:
	addl	-111(%rbp), %r12d
	jc	.L1980
	movq	16(%rbx), %rax
	xorl	%ecx, %ecx
	movq	(%rax), %rdx
	movq	31(%rdx), %rax
	cmpq	$3, %rax
	ja	.L2003
.L1981:
	movq	%rcx, 0(%r13)
	movl	%r15d, (%r14)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2004
	addq	$104, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2003:
	.cfi_restore_state
	movl	%r12d, %r12d
	subq	$4, %rax
	cmpq	%rax, %r12
	ja	.L1981
	movq	23(%rdx), %rcx
	andq	39(%rdx), %r12
	addq	%r12, %rcx
	jmp	.L1981
	.p2align 4,,10
	.p2align 3
.L2001:
	cmpb	$0, 2(%rax)
	js	.L2005
	movl	$7, %edi
	movl	$6, %esi
	movl	$8, %ecx
	movl	$9, %r8d
	movl	$5, %r15d
	movl	$3, %edx
	jmp	.L1960
	.p2align 4,,10
	.p2align 3
.L2002:
	movzbl	1(%rax), %r11d
	movl	%esi, %r15d
	movl	%r11d, %edx
	sall	$7, %edx
	andl	$16256, %edx
	orl	%edx, %r12d
	testb	%r11b, %r11b
	jns	.L1961
	movzbl	2(%rax), %esi
	movl	%edi, %r15d
	movl	%esi, %edx
	sall	$14, %edx
	andl	$2080768, %edx
	orl	%edx, %r12d
	testb	%sil, %sil
	jns	.L1961
	movzbl	3(%rax), %esi
	movl	%ecx, %r15d
	movl	%esi, %edx
	sall	$21, %edx
	andl	$266338304, %edx
	orl	%edx, %r12d
	testb	%sil, %sil
	jns	.L1961
	movzbl	4(%rax), %eax
	movl	%r8d, %r15d
	sall	$28, %eax
	orl	%eax, %r12d
	jmp	.L1961
	.p2align 4,,10
	.p2align 3
.L1974:
	movq	48(%rbx), %rax
	movq	8(%rbx), %rdx
	movq	(%rax), %rcx
	leal	16(,%rsi,8), %eax
	cltq
	movq	7(%rcx), %rcx
	movq	-1(%rax,%rcx), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L1976
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L1977:
	movq	48(%rbx), %rdx
	movq	8(%rbx), %rcx
	movq	%rax, -111(%rbp)
	movq	40(%rbx), %rax
	subq	24(%rbx), %rax
	imull	$-252645135, %eax, %eax
	movq	(%rdx), %rdx
	movq	96(%rcx), %rcx
	movq	7(%rdx), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	%rcx, -1(%rax,%rdx)
	jmp	.L1975
	.p2align 4,,10
	.p2align 3
.L1969:
	movq	48(%rbx), %rax
	movq	8(%rbx), %rdx
	movq	(%rax), %rcx
	leal	16(,%rsi,8), %eax
	cltq
	movq	7(%rcx), %rcx
	movq	-1(%rax,%rcx), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L1971
	movq	%r9, -120(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-120(%rbp), %r9
.L1972:
	movq	40(%rbx), %rdx
	movq	48(%rbx), %rcx
	subq	24(%rbx), %rdx
	movq	8(%rbx), %rsi
	imull	$-252645135, %edx, %edx
	movq	(%rcx), %rcx
	movq	96(%rsi), %rsi
	movq	7(%rcx), %rcx
	leal	16(,%rdx,8), %edx
	movslq	%edx, %rdx
	movq	%rsi, -1(%rdx,%rcx)
	jmp	.L1970
	.p2align 4,,10
	.p2align 3
.L1963:
	movq	48(%rbx), %rax
	movq	8(%rbx), %rdx
	movq	(%rax), %rsi
	leal	16(,%rcx,8), %eax
	cltq
	movq	7(%rsi), %rcx
	movq	-1(%rax,%rcx), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L1965
	movq	%r10, -128(%rbp)
	movq	%r9, -120(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-120(%rbp), %r9
	movq	-128(%rbp), %r10
.L1966:
	movq	40(%rbx), %rdx
	movq	48(%rbx), %rcx
	subq	24(%rbx), %rdx
	movq	8(%rbx), %rsi
	imull	$-252645135, %edx, %edx
	movq	(%rcx), %rcx
	movq	96(%rsi), %rsi
	movq	7(%rcx), %rcx
	leal	16(,%rdx,8), %edx
	movslq	%edx, %rdx
	movq	%rsi, -1(%rdx,%rcx)
	jmp	.L1964
	.p2align 4,,10
	.p2align 3
.L2005:
	cmpb	$0, 3(%rax)
	js	.L2006
	movl	$8, %edi
	movl	$7, %esi
	movl	$9, %ecx
	movl	$10, %r8d
	movl	$6, %r15d
	movl	$4, %edx
	jmp	.L1960
	.p2align 4,,10
	.p2align 3
.L1976:
	movq	41088(%rdx), %rax
	cmpq	41096(%rdx), %rax
	je	.L2007
.L1978:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%rdx)
	movq	%rsi, (%rax)
	jmp	.L1977
	.p2align 4,,10
	.p2align 3
.L1965:
	movq	41088(%rdx), %rax
	cmpq	41096(%rdx), %rax
	je	.L2008
.L1967:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%rdx)
	movq	%rsi, (%rax)
	jmp	.L1966
	.p2align 4,,10
	.p2align 3
.L1971:
	movq	41088(%rdx), %rax
	cmpq	41096(%rdx), %rax
	je	.L2009
.L1973:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%rdx)
	movq	%rsi, (%rax)
	jmp	.L1972
.L2009:
	movq	%rdx, %rdi
	movq	%r9, -136(%rbp)
	movq	%rsi, -128(%rbp)
	movq	%rdx, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %r9
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	jmp	.L1973
.L2008:
	movq	%rdx, %rdi
	movq	%r10, -144(%rbp)
	movq	%r9, -136(%rbp)
	movq	%rsi, -128(%rbp)
	movq	%rdx, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-144(%rbp), %r10
	movq	-136(%rbp), %r9
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	jmp	.L1967
	.p2align 4,,10
	.p2align 3
.L2006:
	movsbl	4(%rax), %ecx
	movsbq	4(%rax), %rdx
	movl	%ecx, %edi
	movl	%ecx, %esi
	movl	%ecx, %r8d
	movl	%ecx, %r15d
	sarl	$31, %edi
	sarl	$31, %esi
	sarl	$31, %r8d
	sarl	$31, %r15d
	notl	%edi
	notl	%esi
	sarl	$31, %ecx
	sarq	$63, %rdx
	notl	%r8d
	notl	%r15d
	notl	%ecx
	notq	%rdx
	addl	$10, %edi
	addl	$9, %esi
	addl	$12, %r8d
	addl	$8, %r15d
	addl	$11, %ecx
	addq	$6, %rdx
	jmp	.L1960
	.p2align 4,,10
	.p2align 3
.L2007:
	movq	%rdx, %rdi
	movq	%rsi, -128(%rbp)
	movq	%rdx, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	jmp	.L1978
.L1980:
	xorl	%ecx, %ecx
	jmp	.L1981
.L2004:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22616:
	.size	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsIjmEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_, .-_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsIjmEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	.section	.text._ZN2v88internal4wasm10ThreadImpl15ExecuteAtomicOpENS1_10WasmOpcodeEPNS1_7DecoderEPNS1_15InterpreterCodeEmPi,"axG",@progbits,_ZN2v88internal4wasm10ThreadImpl15ExecuteAtomicOpENS1_10WasmOpcodeEPNS1_7DecoderEPNS1_15InterpreterCodeEmPi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm10ThreadImpl15ExecuteAtomicOpENS1_10WasmOpcodeEPNS1_7DecoderEPNS1_15InterpreterCodeEmPi
	.type	_ZN2v88internal4wasm10ThreadImpl15ExecuteAtomicOpENS1_10WasmOpcodeEPNS1_7DecoderEPNS1_15InterpreterCodeEmPi, @function
_ZN2v88internal4wasm10ThreadImpl15ExecuteAtomicOpENS1_10WasmOpcodeEPNS1_7DecoderEPNS1_15InterpreterCodeEmPi:
.LFB20124:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	subl	$65027, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$75, %esi
	ja	.L2011
	movq	%rdx, %r10
	movq	%rcx, %rdx
	leaq	.L2013(%rip), %rcx
	movq	%rdi, %r12
	movslq	(%rcx,%rsi,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm10ThreadImpl15ExecuteAtomicOpENS1_10WasmOpcodeEPNS1_7DecoderEPNS1_15InterpreterCodeEmPi,"aG",@progbits,_ZN2v88internal4wasm10ThreadImpl15ExecuteAtomicOpENS1_10WasmOpcodeEPNS1_7DecoderEPNS1_15InterpreterCodeEmPi,comdat
	.align 4
	.align 4
.L2013:
	.long	.L2076-.L2013
	.long	.L2011-.L2013
	.long	.L2011-.L2013
	.long	.L2011-.L2013
	.long	.L2011-.L2013
	.long	.L2011-.L2013
	.long	.L2011-.L2013
	.long	.L2011-.L2013
	.long	.L2011-.L2013
	.long	.L2011-.L2013
	.long	.L2011-.L2013
	.long	.L2011-.L2013
	.long	.L2011-.L2013
	.long	.L2075-.L2013
	.long	.L2074-.L2013
	.long	.L2073-.L2013
	.long	.L2072-.L2013
	.long	.L2071-.L2013
	.long	.L2070-.L2013
	.long	.L2069-.L2013
	.long	.L2068-.L2013
	.long	.L2067-.L2013
	.long	.L2066-.L2013
	.long	.L2065-.L2013
	.long	.L2064-.L2013
	.long	.L2063-.L2013
	.long	.L2062-.L2013
	.long	.L2061-.L2013
	.long	.L2060-.L2013
	.long	.L2059-.L2013
	.long	.L2058-.L2013
	.long	.L2057-.L2013
	.long	.L2056-.L2013
	.long	.L2055-.L2013
	.long	.L2054-.L2013
	.long	.L2053-.L2013
	.long	.L2052-.L2013
	.long	.L2051-.L2013
	.long	.L2050-.L2013
	.long	.L2049-.L2013
	.long	.L2048-.L2013
	.long	.L2047-.L2013
	.long	.L2046-.L2013
	.long	.L2045-.L2013
	.long	.L2044-.L2013
	.long	.L2043-.L2013
	.long	.L2042-.L2013
	.long	.L2041-.L2013
	.long	.L2040-.L2013
	.long	.L2039-.L2013
	.long	.L2038-.L2013
	.long	.L2037-.L2013
	.long	.L2036-.L2013
	.long	.L2035-.L2013
	.long	.L2034-.L2013
	.long	.L2033-.L2013
	.long	.L2032-.L2013
	.long	.L2031-.L2013
	.long	.L2030-.L2013
	.long	.L2029-.L2013
	.long	.L2028-.L2013
	.long	.L2027-.L2013
	.long	.L2026-.L2013
	.long	.L2025-.L2013
	.long	.L2024-.L2013
	.long	.L2023-.L2013
	.long	.L2022-.L2013
	.long	.L2021-.L2013
	.long	.L2020-.L2013
	.long	.L2019-.L2013
	.long	.L2018-.L2013
	.long	.L2017-.L2013
	.long	.L2016-.L2013
	.long	.L2015-.L2013
	.long	.L2014-.L2013
	.long	.L2012-.L2013
	.section	.text._ZN2v88internal4wasm10ThreadImpl15ExecuteAtomicOpENS1_10WasmOpcodeEPNS1_7DecoderEPNS1_15InterpreterCodeEmPi,"axG",@progbits,_ZN2v88internal4wasm10ThreadImpl15ExecuteAtomicOpENS1_10WasmOpcodeEPNS1_7DecoderEPNS1_15InterpreterCodeEmPi,comdat
.L2011:
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2012:
	leaq	-128(%rbp), %rax
	leaq	-136(%rbp), %rbx
	movq	%r10, %rsi
	pushq	%rax
	leaq	-120(%rbp), %rcx
	pushq	%rbx
	call	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsIjmEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	popq	%r11
	popq	%r14
	movl	%eax, %r13d
	testb	%al, %al
	je	.L2010
	movq	-120(%rbp), %rdx
	movl	-136(%rbp), %eax
	movl	-128(%rbp), %ecx
	lock cmpxchgl	%ecx, (%rdx)
	je	.L2154
	movl	%eax, (%rbx)
.L2154:
	movl	-136(%rbp), %eax
	movb	$2, -80(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	$0, -71(%rbp)
	movq	%rax, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm3
	movb	%al, 16(%rsp)
	movups	%xmm3, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	.p2align 4,,10
	.p2align 3
.L2010:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2273
	leaq	-40(%rbp), %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2014:
	.cfi_restore_state
	leaq	-128(%rbp), %rax
	leaq	-136(%rbp), %rbx
	movq	%r10, %rsi
	pushq	%rax
	leaq	-120(%rbp), %rcx
	pushq	%rbx
	call	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsItmEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	popq	%r15
	movl	%eax, %r13d
	popq	%rax
	testb	%r13b, %r13b
	je	.L2010
	movq	-120(%rbp), %rdx
	movzwl	-128(%rbp), %ecx
	movzwl	-136(%rbp), %eax
	lock cmpxchgw	%cx, (%rdx)
	je	.L2152
	movw	%ax, (%rbx)
.L2152:
	movzwl	-136(%rbp), %eax
	movb	$2, -80(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	$0, -71(%rbp)
	movq	%rax, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm2
	movb	%al, 16(%rsp)
	movups	%xmm2, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2015:
	leaq	-128(%rbp), %rax
	leaq	-136(%rbp), %rbx
	movq	%r10, %rsi
	pushq	%rax
	leaq	-120(%rbp), %rcx
	pushq	%rbx
	call	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsIhmEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	movl	%eax, %r13d
	popq	%rax
	popq	%rdx
	testb	%r13b, %r13b
	je	.L2010
	movq	-120(%rbp), %rdx
	movzbl	-128(%rbp), %ecx
	movzbl	-136(%rbp), %eax
	lock cmpxchgb	%cl, (%rdx)
	je	.L2150
	movb	%al, (%rbx)
.L2150:
	movzbl	-136(%rbp), %eax
	movb	$2, -80(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	$0, -71(%rbp)
	movq	%rax, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm1
	movb	%al, 16(%rsp)
	movups	%xmm1, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2016:
	leaq	-128(%rbp), %rax
	leaq	-136(%rbp), %rbx
	movq	%r10, %rsi
	pushq	%rax
	leaq	-120(%rbp), %rcx
	pushq	%rbx
	call	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsItjEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	popq	%rdi
	popq	%r8
	movl	%eax, %r13d
	testb	%al, %al
	je	.L2010
	movq	-120(%rbp), %rdx
	movzwl	-128(%rbp), %ecx
	movzwl	-136(%rbp), %eax
	lock cmpxchgw	%cx, (%rdx)
	je	.L2146
	movw	%ax, (%rbx)
.L2146:
	pxor	%xmm0, %xmm0
	movb	$1, -80(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movzwl	-136(%rbp), %eax
	movups	%xmm0, -79(%rbp)
	movl	%eax, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm6
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2017:
	leaq	-128(%rbp), %rax
	leaq	-136(%rbp), %rbx
	movq	%r10, %rsi
	pushq	%rax
	leaq	-120(%rbp), %rcx
	pushq	%rbx
	call	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsIhjEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	popq	%r9
	popq	%r10
	movl	%eax, %r13d
	testb	%al, %al
	je	.L2010
	movq	-120(%rbp), %rdx
	movzbl	-128(%rbp), %ecx
	movzbl	-136(%rbp), %eax
	lock cmpxchgb	%cl, (%rdx)
	je	.L2144
	movb	%al, (%rbx)
.L2144:
	pxor	%xmm0, %xmm0
	movb	$1, -80(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movzbl	-136(%rbp), %eax
	movups	%xmm0, -79(%rbp)
	movl	%eax, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm5
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2018:
	leaq	-128(%rbp), %rax
	leaq	-136(%rbp), %rbx
	movq	%r10, %rsi
	pushq	%rax
	leaq	-120(%rbp), %rcx
	pushq	%rbx
	call	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsImmEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	popq	%rcx
	popq	%rsi
	movl	%eax, %r13d
	testb	%al, %al
	je	.L2010
	movq	-120(%rbp), %rdx
	movq	-136(%rbp), %rax
	movq	-128(%rbp), %rcx
	lock cmpxchgq	%rcx, (%rdx)
	je	.L2148
	movq	%rax, (%rbx)
.L2148:
	movq	-136(%rbp), %rax
	movb	$2, -80(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	$0, -71(%rbp)
	movq	%rax, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm7
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2019:
	leaq	-128(%rbp), %rax
	leaq	-136(%rbp), %rbx
	movq	%r10, %rsi
	pushq	%rax
	leaq	-120(%rbp), %rcx
	pushq	%rbx
	call	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsIjjEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	popq	%r11
	popq	%r14
	movl	%eax, %r13d
	testb	%al, %al
	je	.L2010
	movq	-120(%rbp), %rdx
	movl	-136(%rbp), %eax
	movl	-128(%rbp), %ecx
	lock cmpxchgl	%ecx, (%rdx)
	je	.L2142
	movl	%eax, (%rbx)
.L2142:
	pxor	%xmm0, %xmm0
	movb	$1, -80(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movl	-136(%rbp), %eax
	movups	%xmm0, -79(%rbp)
	movl	%eax, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm4
	movb	%al, 16(%rsp)
	movups	%xmm4, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2020:
	leaq	-128(%rbp), %rax
	pushq	$0
	leaq	-120(%rbp), %rcx
	movq	%r10, %rsi
	pushq	%rax
	call	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsIjmEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	popq	%r15
	movl	%eax, %r13d
	popq	%rax
	testb	%r13b, %r13b
	je	.L2010
	movq	-120(%rbp), %rdx
	movl	-128(%rbp), %eax
	xchgl	(%rdx), %eax
	movl	%eax, %eax
	movb	$2, -80(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	$0, -71(%rbp)
	movq	%rax, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm3
	movb	%al, 16(%rsp)
	movups	%xmm3, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2021:
	leaq	-128(%rbp), %rax
	pushq	$0
	leaq	-120(%rbp), %rcx
	movq	%r10, %rsi
	pushq	%rax
	call	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsItmEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	movl	%eax, %r13d
	popq	%rax
	popq	%rdx
	testb	%r13b, %r13b
	je	.L2010
	movq	-120(%rbp), %rdx
	movzwl	-128(%rbp), %eax
	xchgw	(%rdx), %ax
	movzwl	%ax, %eax
	movb	$2, -80(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	$0, -71(%rbp)
	movq	%rax, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm2
	movb	%al, 16(%rsp)
	movups	%xmm2, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2022:
	leaq	-128(%rbp), %rax
	pushq	$0
	leaq	-120(%rbp), %rcx
	movq	%r10, %rsi
	pushq	%rax
	call	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsIhmEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	popq	%rcx
	popq	%rsi
	movl	%eax, %r13d
	testb	%al, %al
	je	.L2010
	movq	-120(%rbp), %rdx
	movzbl	-128(%rbp), %eax
	xchgb	(%rdx), %al
	movzbl	%al, %eax
	movb	$2, -80(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	$0, -71(%rbp)
	movq	%rax, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm1
	movb	%al, 16(%rsp)
	movups	%xmm1, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2023:
	leaq	-128(%rbp), %rax
	pushq	$0
	leaq	-120(%rbp), %rcx
	movq	%r10, %rsi
	pushq	%rax
	call	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsItjEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	popq	%r14
	popq	%r15
	movl	%eax, %r13d
	testb	%al, %al
	je	.L2010
	movq	-120(%rbp), %rdx
	movzwl	-128(%rbp), %eax
	xchgw	(%rdx), %ax
	pxor	%xmm0, %xmm0
	movzwl	%ax, %eax
	movups	%xmm0, -79(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movl	%eax, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movb	$1, -80(%rbp)
	movdqa	-80(%rbp), %xmm5
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2024:
	leaq	-128(%rbp), %rax
	pushq	$0
	leaq	-120(%rbp), %rcx
	movq	%r10, %rsi
	pushq	%rax
	call	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsIhjEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	movl	%eax, %r13d
	popq	%rax
	popq	%rdx
	testb	%r13b, %r13b
	je	.L2010
	movq	-120(%rbp), %rdx
	movzbl	-128(%rbp), %eax
	xchgb	(%rdx), %al
	pxor	%xmm0, %xmm0
	movzbl	%al, %eax
	movups	%xmm0, -79(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movl	%eax, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movb	$1, -80(%rbp)
	movdqa	-80(%rbp), %xmm4
	movb	%al, 16(%rsp)
	movups	%xmm4, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2025:
	leaq	-128(%rbp), %rax
	pushq	$0
	leaq	-120(%rbp), %rcx
	movq	%r10, %rsi
	pushq	%rax
	call	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsImmEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	popq	%rdi
	popq	%r8
	movl	%eax, %r13d
	testb	%al, %al
	je	.L2010
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %rax
	xchgq	(%rdx), %rax
	movq	$0, -71(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	%rax, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movb	$2, -80(%rbp)
	movdqa	-80(%rbp), %xmm7
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2026:
	leaq	-128(%rbp), %rax
	pushq	$0
	leaq	-120(%rbp), %rcx
	movq	%r10, %rsi
	pushq	%rax
	call	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsIjjEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	popq	%rcx
	popq	%rsi
	movl	%eax, %r13d
	testb	%al, %al
	je	.L2010
	movq	-120(%rbp), %rdx
	movl	-128(%rbp), %eax
	xchgl	(%rdx), %eax
	pxor	%xmm0, %xmm0
	movups	%xmm0, -79(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movl	%eax, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movb	$1, -80(%rbp)
	movdqa	-80(%rbp), %xmm3
	movb	%al, 16(%rsp)
	movups	%xmm3, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2027:
	leaq	-128(%rbp), %rax
	pushq	$0
	movq	%r10, %rsi
	leaq	-120(%rbp), %rcx
	pushq	%rax
	call	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsIjmEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	popq	%r9
	popq	%r10
	movl	%eax, %r13d
	testb	%al, %al
	je	.L2010
	movq	-120(%rbp), %rdx
	movl	(%rdx), %eax
.L2136:
	movl	-128(%rbp), %esi
	movl	%eax, %ecx
	xorl	%eax, %esi
	lock cmpxchgl	%esi, (%rdx)
	jne	.L2136
	movl	%ecx, %eax
	movb	$2, -80(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	$0, -71(%rbp)
	movq	%rax, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm7
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2028:
	leaq	-128(%rbp), %rax
	pushq	$0
	leaq	-120(%rbp), %rcx
	movq	%r10, %rsi
	pushq	%rax
	call	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsItmEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	popq	%r11
	popq	%rbx
	movl	%eax, %r13d
	testb	%al, %al
	je	.L2010
	movq	-120(%rbp), %rcx
	movzwl	-128(%rbp), %edi
	movzwl	(%rcx), %eax
.L2134:
	movl	%eax, %esi
	movzwl	%ax, %edx
	xorl	%edi, %esi
	lock cmpxchgw	%si, (%rcx)
	jne	.L2134
	movq	$0, -71(%rbp)
	movzbl	-64(%rbp), %eax
	subq	$32, %rsp
	movq	%r12, %rdi
	movb	$2, -80(%rbp)
	movq	%rdx, -79(%rbp)
	movdqa	-80(%rbp), %xmm6
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2029:
	leaq	-128(%rbp), %rax
	pushq	$0
	leaq	-120(%rbp), %rcx
	movq	%r10, %rsi
	pushq	%rax
	call	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsIhmEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	popq	%r14
	popq	%r15
	movl	%eax, %r13d
	testb	%al, %al
	je	.L2010
	movq	-120(%rbp), %rcx
	movzbl	-128(%rbp), %edi
	movzbl	(%rcx), %eax
.L2132:
	movl	%eax, %esi
	movzbl	%al, %edx
	xorl	%edi, %esi
	lock cmpxchgb	%sil, (%rcx)
	jne	.L2132
	movq	$0, -71(%rbp)
	movzbl	-64(%rbp), %eax
	subq	$32, %rsp
	movq	%r12, %rdi
	movb	$2, -80(%rbp)
	movq	%rdx, -79(%rbp)
	movdqa	-80(%rbp), %xmm5
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2030:
	leaq	-128(%rbp), %rax
	pushq	$0
	leaq	-120(%rbp), %rcx
	movq	%r10, %rsi
	pushq	%rax
	call	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsItjEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	popq	%rdi
	popq	%r8
	movl	%eax, %r13d
	testb	%al, %al
	je	.L2010
	movq	-120(%rbp), %rcx
	movzwl	-128(%rbp), %edi
	movzwl	(%rcx), %eax
.L2101:
	movl	%eax, %esi
	movzwl	%ax, %edx
	xorl	%edi, %esi
	lock cmpxchgw	%si, (%rcx)
	jne	.L2101
	pxor	%xmm0, %xmm0
	movb	$1, -80(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movl	%edx, -79(%rbp)
	movdqa	-80(%rbp), %xmm2
	movb	%al, 16(%rsp)
	movups	%xmm2, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2031:
	leaq	-128(%rbp), %rax
	pushq	$0
	movq	%r10, %rsi
	leaq	-120(%rbp), %rcx
	pushq	%rax
	call	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsIhjEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	popq	%r9
	popq	%r10
	movl	%eax, %r13d
	testb	%al, %al
	je	.L2010
	movq	-120(%rbp), %rcx
	movzbl	-128(%rbp), %edi
	movzbl	(%rcx), %eax
.L2099:
	movl	%eax, %esi
	movzbl	%al, %edx
	xorl	%edi, %esi
	lock cmpxchgb	%sil, (%rcx)
	jne	.L2099
	pxor	%xmm0, %xmm0
	movb	$1, -80(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movl	%edx, -79(%rbp)
	movdqa	-80(%rbp), %xmm1
	movb	%al, 16(%rsp)
	movups	%xmm1, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2032:
	leaq	-128(%rbp), %rax
	pushq	$0
	leaq	-120(%rbp), %rcx
	movq	%r10, %rsi
	pushq	%rax
	call	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsImmEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	movl	%eax, %r13d
	popq	%rax
	popq	%rdx
	testb	%r13b, %r13b
	je	.L2010
	movq	-120(%rbp), %rdx
	movq	(%rdx), %rax
.L2130:
	movq	-128(%rbp), %rsi
	movq	%rax, %rcx
	xorq	%rax, %rsi
	lock cmpxchgq	%rsi, (%rdx)
	jne	.L2130
	movq	$0, -71(%rbp)
	movzbl	-64(%rbp), %eax
	subq	$32, %rsp
	movq	%r12, %rdi
	movb	$2, -80(%rbp)
	movq	%rcx, -79(%rbp)
	movdqa	-80(%rbp), %xmm4
	movb	%al, 16(%rsp)
	movups	%xmm4, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2033:
	leaq	-128(%rbp), %rax
	pushq	$0
	leaq	-120(%rbp), %rcx
	movq	%r10, %rsi
	pushq	%rax
	call	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsIjjEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	popq	%r11
	popq	%rbx
	movl	%eax, %r13d
	testb	%al, %al
	je	.L2010
	movq	-120(%rbp), %rdx
	movl	(%rdx), %eax
.L2097:
	movl	-128(%rbp), %esi
	movl	%eax, %ecx
	xorl	%eax, %esi
	lock cmpxchgl	%esi, (%rdx)
	jne	.L2097
	pxor	%xmm0, %xmm0
	movb	$1, -80(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movl	%ecx, -79(%rbp)
	movdqa	-80(%rbp), %xmm7
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2034:
	leaq	-128(%rbp), %rax
	pushq	$0
	leaq	-120(%rbp), %rcx
	movq	%r10, %rsi
	pushq	%rax
	call	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsIjmEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	popq	%rcx
	popq	%rsi
	movl	%eax, %r13d
	testb	%al, %al
	je	.L2010
	movq	-120(%rbp), %rdx
	movl	(%rdx), %eax
.L2128:
	movl	-128(%rbp), %esi
	movl	%eax, %ecx
	orl	%eax, %esi
	lock cmpxchgl	%esi, (%rdx)
	jne	.L2128
	movl	%ecx, %eax
	movb	$2, -80(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	$0, -71(%rbp)
	movq	%rax, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm3
	movb	%al, 16(%rsp)
	movups	%xmm3, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2035:
	leaq	-128(%rbp), %rax
	pushq	$0
	leaq	-120(%rbp), %rcx
	movq	%r10, %rsi
	pushq	%rax
	call	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsItmEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	popq	%rdi
	popq	%r8
	movl	%eax, %r13d
	testb	%al, %al
	je	.L2010
	movq	-120(%rbp), %rcx
	movzwl	-128(%rbp), %edi
	movzwl	(%rcx), %eax
.L2126:
	movl	%eax, %esi
	movzwl	%ax, %edx
	orl	%edi, %esi
	lock cmpxchgw	%si, (%rcx)
	jne	.L2126
	movq	$0, -71(%rbp)
	movzbl	-64(%rbp), %eax
	subq	$32, %rsp
	movq	%r12, %rdi
	movb	$2, -80(%rbp)
	movq	%rdx, -79(%rbp)
	movdqa	-80(%rbp), %xmm2
	movb	%al, 16(%rsp)
	movups	%xmm2, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2036:
	leaq	-128(%rbp), %rax
	pushq	$0
	movq	%r10, %rsi
	leaq	-120(%rbp), %rcx
	pushq	%rax
	call	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsIhmEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	popq	%r9
	popq	%r10
	movl	%eax, %r13d
	testb	%al, %al
	je	.L2010
	movq	-120(%rbp), %rcx
	movzbl	-128(%rbp), %edi
	movzbl	(%rcx), %eax
.L2124:
	movl	%eax, %esi
	movzbl	%al, %edx
	orl	%edi, %esi
	lock cmpxchgb	%sil, (%rcx)
	jne	.L2124
	movq	$0, -71(%rbp)
	movzbl	-64(%rbp), %eax
	subq	$32, %rsp
	movq	%r12, %rdi
	movb	$2, -80(%rbp)
	movq	%rdx, -79(%rbp)
	movdqa	-80(%rbp), %xmm1
	movb	%al, 16(%rsp)
	movups	%xmm1, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2037:
	leaq	-128(%rbp), %rax
	pushq	$0
	leaq	-120(%rbp), %rcx
	movq	%r10, %rsi
	pushq	%rax
	call	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsItjEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	popq	%r14
	popq	%r15
	movl	%eax, %r13d
	testb	%al, %al
	je	.L2010
	movq	-120(%rbp), %rcx
	movzwl	-128(%rbp), %edi
	movzwl	(%rcx), %eax
.L2095:
	movl	%eax, %esi
	movzwl	%ax, %edx
	orl	%edi, %esi
	lock cmpxchgw	%si, (%rcx)
	jne	.L2095
	pxor	%xmm0, %xmm0
	movb	$1, -80(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movl	%edx, -79(%rbp)
	movdqa	-80(%rbp), %xmm6
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2038:
	leaq	-128(%rbp), %rax
	pushq	$0
	leaq	-120(%rbp), %rcx
	movq	%r10, %rsi
	pushq	%rax
	call	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsIhjEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	movl	%eax, %r13d
	popq	%rax
	popq	%rdx
	testb	%r13b, %r13b
	je	.L2010
	movq	-120(%rbp), %rcx
	movzbl	-128(%rbp), %edi
	movzbl	(%rcx), %eax
.L2093:
	movl	%eax, %esi
	movzbl	%al, %edx
	orl	%edi, %esi
	lock cmpxchgb	%sil, (%rcx)
	jne	.L2093
	pxor	%xmm0, %xmm0
	movb	$1, -80(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movl	%edx, -79(%rbp)
	movdqa	-80(%rbp), %xmm5
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2039:
	leaq	-128(%rbp), %rax
	pushq	$0
	leaq	-120(%rbp), %rcx
	movq	%r10, %rsi
	pushq	%rax
	call	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsImmEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	popq	%r11
	popq	%rbx
	movl	%eax, %r13d
	testb	%al, %al
	je	.L2010
	movq	-120(%rbp), %rdx
	movq	(%rdx), %rax
.L2122:
	movq	-128(%rbp), %rsi
	movq	%rax, %rcx
	orq	%rax, %rsi
	lock cmpxchgq	%rsi, (%rdx)
	jne	.L2122
	movq	$0, -71(%rbp)
	movzbl	-64(%rbp), %eax
	subq	$32, %rsp
	movq	%r12, %rdi
	movb	$2, -80(%rbp)
	movq	%rcx, -79(%rbp)
	movdqa	-80(%rbp), %xmm7
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2040:
	leaq	-128(%rbp), %rax
	pushq	$0
	leaq	-120(%rbp), %rcx
	movq	%r10, %rsi
	pushq	%rax
	call	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsIjjEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	popq	%rcx
	popq	%rsi
	movl	%eax, %r13d
	testb	%al, %al
	je	.L2010
	movq	-120(%rbp), %rdx
	movl	(%rdx), %eax
.L2091:
	movl	-128(%rbp), %esi
	movl	%eax, %ecx
	orl	%eax, %esi
	lock cmpxchgl	%esi, (%rdx)
	jne	.L2091
	pxor	%xmm0, %xmm0
	movb	$1, -80(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movl	%ecx, -79(%rbp)
	movdqa	-80(%rbp), %xmm4
	movb	%al, 16(%rsp)
	movups	%xmm4, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2041:
	leaq	-128(%rbp), %rax
	pushq	$0
	leaq	-120(%rbp), %rcx
	movq	%r10, %rsi
	pushq	%rax
	call	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsIjmEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	popq	%r14
	popq	%r15
	movl	%eax, %r13d
	testb	%al, %al
	je	.L2010
	movq	-120(%rbp), %rdx
	movl	(%rdx), %eax
.L2120:
	movl	-128(%rbp), %esi
	movl	%eax, %ecx
	andl	%eax, %esi
	lock cmpxchgl	%esi, (%rdx)
	jne	.L2120
	movl	%ecx, %eax
	movb	$2, -80(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	$0, -71(%rbp)
	movq	%rax, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm6
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2042:
	leaq	-128(%rbp), %rax
	pushq	$0
	leaq	-120(%rbp), %rcx
	movq	%r10, %rsi
	pushq	%rax
	call	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsItmEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	movl	%eax, %r13d
	popq	%rax
	popq	%rdx
	testb	%r13b, %r13b
	je	.L2010
	movq	-120(%rbp), %rcx
	movzwl	-128(%rbp), %edi
	movzwl	(%rcx), %eax
.L2118:
	movl	%eax, %esi
	movzwl	%ax, %edx
	andl	%edi, %esi
	lock cmpxchgw	%si, (%rcx)
	jne	.L2118
	movq	$0, -71(%rbp)
	movzbl	-64(%rbp), %eax
	subq	$32, %rsp
	movq	%r12, %rdi
	movb	$2, -80(%rbp)
	movq	%rdx, -79(%rbp)
	movdqa	-80(%rbp), %xmm5
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2043:
	leaq	-128(%rbp), %rax
	pushq	$0
	leaq	-120(%rbp), %rcx
	movq	%r10, %rsi
	pushq	%rax
	call	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsIhmEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	popq	%rcx
	popq	%rsi
	movl	%eax, %r13d
	testb	%al, %al
	je	.L2010
	movq	-120(%rbp), %rcx
	movzbl	-128(%rbp), %edi
	movzbl	(%rcx), %eax
.L2116:
	movl	%eax, %esi
	movzbl	%al, %edx
	andl	%edi, %esi
	lock cmpxchgb	%sil, (%rcx)
	jne	.L2116
	movq	$0, -71(%rbp)
	movzbl	-64(%rbp), %eax
	subq	$32, %rsp
	movq	%r12, %rdi
	movb	$2, -80(%rbp)
	movq	%rdx, -79(%rbp)
	movdqa	-80(%rbp), %xmm4
	movb	%al, 16(%rsp)
	movups	%xmm4, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2044:
	leaq	-128(%rbp), %rax
	pushq	$0
	leaq	-120(%rbp), %rcx
	movq	%r10, %rsi
	pushq	%rax
	call	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsItjEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	popq	%rdi
	popq	%r8
	movl	%eax, %r13d
	testb	%al, %al
	je	.L2010
	movq	-120(%rbp), %rcx
	movzwl	-128(%rbp), %edi
	movzwl	(%rcx), %eax
.L2089:
	movl	%eax, %esi
	movzwl	%ax, %edx
	andl	%edi, %esi
	lock cmpxchgw	%si, (%rcx)
	jne	.L2089
	pxor	%xmm0, %xmm0
	movb	$1, -80(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movl	%edx, -79(%rbp)
	movdqa	-80(%rbp), %xmm3
	movb	%al, 16(%rsp)
	movups	%xmm3, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2045:
	leaq	-128(%rbp), %rax
	pushq	$0
	movq	%r10, %rsi
	leaq	-120(%rbp), %rcx
	pushq	%rax
	call	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsIhjEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	popq	%r9
	popq	%r10
	movl	%eax, %r13d
	testb	%al, %al
	je	.L2010
	movq	-120(%rbp), %rcx
	movzbl	-128(%rbp), %edi
	movzbl	(%rcx), %eax
.L2087:
	movl	%eax, %esi
	movzbl	%al, %edx
	andl	%edi, %esi
	lock cmpxchgb	%sil, (%rcx)
	jne	.L2087
	pxor	%xmm0, %xmm0
	movb	$1, -80(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movl	%edx, -79(%rbp)
	movdqa	-80(%rbp), %xmm2
	movb	%al, 16(%rsp)
	movups	%xmm2, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2046:
	leaq	-128(%rbp), %rax
	pushq	$0
	leaq	-120(%rbp), %rcx
	movq	%r10, %rsi
	pushq	%rax
	call	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsImmEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	popq	%rdi
	popq	%r8
	movl	%eax, %r13d
	testb	%al, %al
	je	.L2010
	movq	-120(%rbp), %rdx
	movq	(%rdx), %rax
.L2114:
	movq	-128(%rbp), %rsi
	movq	%rax, %rcx
	andq	%rax, %rsi
	lock cmpxchgq	%rsi, (%rdx)
	jne	.L2114
	movq	$0, -71(%rbp)
	movzbl	-64(%rbp), %eax
	subq	$32, %rsp
	movq	%r12, %rdi
	movb	$2, -80(%rbp)
	movq	%rcx, -79(%rbp)
	movdqa	-80(%rbp), %xmm3
	movb	%al, 16(%rsp)
	movups	%xmm3, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2047:
	leaq	-128(%rbp), %rax
	pushq	$0
	leaq	-120(%rbp), %rcx
	movq	%r10, %rsi
	pushq	%rax
	call	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsIjjEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	popq	%r11
	popq	%rbx
	movl	%eax, %r13d
	testb	%al, %al
	je	.L2010
	movq	-120(%rbp), %rdx
	movl	(%rdx), %eax
.L2085:
	movl	-128(%rbp), %esi
	movl	%eax, %ecx
	andl	%eax, %esi
	lock cmpxchgl	%esi, (%rdx)
	jne	.L2085
	pxor	%xmm0, %xmm0
	movb	$1, -80(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movups	%xmm0, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movl	%ecx, -79(%rbp)
	movdqa	-80(%rbp), %xmm1
	movb	%al, 16(%rsp)
	movups	%xmm1, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2048:
	leaq	-128(%rbp), %rax
	pushq	$0
	movq	%r10, %rsi
	leaq	-120(%rbp), %rcx
	pushq	%rax
	call	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsIjmEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	popq	%r9
	popq	%r10
	movl	%eax, %r13d
	testb	%al, %al
	je	.L2010
	movl	-128(%rbp), %eax
	movq	-120(%rbp), %rdx
	negl	%eax
	lock xaddl	%eax, (%rdx)
	movl	%eax, %eax
	movb	$2, -80(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	$0, -71(%rbp)
	movq	%rax, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm6
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2049:
	leaq	-128(%rbp), %rax
	pushq	$0
	leaq	-120(%rbp), %rcx
	movq	%r10, %rsi
	pushq	%rax
	call	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsItmEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	popq	%r11
	popq	%rbx
	movl	%eax, %r13d
	testb	%al, %al
	je	.L2010
	movzwl	-128(%rbp), %eax
	movq	-120(%rbp), %rdx
	negl	%eax
	lock xaddw	%ax, (%rdx)
	movzwl	%ax, %eax
	movb	$2, -80(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	$0, -71(%rbp)
	movq	%rax, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm5
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2050:
	leaq	-128(%rbp), %rax
	pushq	$0
	leaq	-120(%rbp), %rcx
	movq	%r10, %rsi
	pushq	%rax
	call	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsIhmEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	popq	%r14
	popq	%r15
	movl	%eax, %r13d
	testb	%al, %al
	je	.L2010
	movzbl	-128(%rbp), %eax
	movq	-120(%rbp), %rdx
	negl	%eax
	lock xaddb	%al, (%rdx)
	movzbl	%al, %eax
	movb	$2, -80(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	$0, -71(%rbp)
	movq	%rax, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm4
	movb	%al, 16(%rsp)
	movups	%xmm4, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2051:
	leaq	-128(%rbp), %rax
	pushq	$0
	leaq	-120(%rbp), %rcx
	movq	%r10, %rsi
	pushq	%rax
	call	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsItjEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	popq	%r14
	popq	%r15
	movl	%eax, %r13d
	testb	%al, %al
	je	.L2010
	movzwl	-128(%rbp), %eax
	movq	-120(%rbp), %rdx
	negl	%eax
	lock xaddw	%ax, (%rdx)
	pxor	%xmm0, %xmm0
	movzwl	%ax, %eax
	movups	%xmm0, -79(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movl	%eax, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movb	$1, -80(%rbp)
	movdqa	-80(%rbp), %xmm2
	movb	%al, 16(%rsp)
	movups	%xmm2, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2052:
	leaq	-128(%rbp), %rax
	pushq	$0
	leaq	-120(%rbp), %rcx
	movq	%r10, %rsi
	pushq	%rax
	call	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsIhjEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	movl	%eax, %r13d
	popq	%rax
	popq	%rdx
	testb	%r13b, %r13b
	je	.L2010
	movzbl	-128(%rbp), %eax
	movq	-120(%rbp), %rdx
	negl	%eax
	lock xaddb	%al, (%rdx)
	pxor	%xmm0, %xmm0
	movzbl	%al, %eax
	movups	%xmm0, -79(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movl	%eax, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movb	$1, -80(%rbp)
	movdqa	-80(%rbp), %xmm6
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2053:
	leaq	-128(%rbp), %rax
	pushq	$0
	leaq	-120(%rbp), %rcx
	movq	%r10, %rsi
	pushq	%rax
	call	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsImmEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	movl	%eax, %r13d
	popq	%rax
	popq	%rdx
	testb	%r13b, %r13b
	je	.L2010
	movq	-128(%rbp), %rax
	movq	-120(%rbp), %rdx
	negq	%rax
	lock xaddq	%rax, (%rdx)
	movq	$0, -71(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	%rax, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movb	$2, -80(%rbp)
	movdqa	-80(%rbp), %xmm3
	movb	%al, 16(%rsp)
	movups	%xmm3, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2054:
	leaq	-128(%rbp), %rax
	pushq	$0
	leaq	-120(%rbp), %rcx
	movq	%r10, %rsi
	pushq	%rax
	call	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsIjjEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	popq	%rcx
	popq	%rsi
	movl	%eax, %r13d
	testb	%al, %al
	je	.L2010
	movl	-128(%rbp), %eax
	movq	-120(%rbp), %rdx
	negl	%eax
	lock xaddl	%eax, (%rdx)
	pxor	%xmm0, %xmm0
	movups	%xmm0, -79(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movl	%eax, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movb	$1, -80(%rbp)
	movdqa	-80(%rbp), %xmm5
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2055:
	leaq	-128(%rbp), %rax
	pushq	$0
	leaq	-120(%rbp), %rcx
	movq	%r10, %rsi
	pushq	%rax
	call	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsIjmEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	popq	%rcx
	popq	%rsi
	movl	%eax, %r13d
	testb	%al, %al
	je	.L2010
	movq	-120(%rbp), %rdx
	movl	-128(%rbp), %eax
	lock xaddl	%eax, (%rdx)
	movl	%eax, %eax
	movb	$2, -80(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	$0, -71(%rbp)
	movq	%rax, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm2
	movb	%al, 16(%rsp)
	movups	%xmm2, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2056:
	leaq	-128(%rbp), %rax
	pushq	$0
	leaq	-120(%rbp), %rcx
	movq	%r10, %rsi
	pushq	%rax
	call	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsItmEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	popq	%rdi
	popq	%r8
	movl	%eax, %r13d
	testb	%al, %al
	je	.L2010
	movq	-120(%rbp), %rdx
	movzwl	-128(%rbp), %eax
	lock xaddw	%ax, (%rdx)
	movzwl	%ax, %eax
	movb	$2, -80(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	$0, -71(%rbp)
	movq	%rax, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm1
	movb	%al, 16(%rsp)
	movups	%xmm1, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2057:
	leaq	-128(%rbp), %rax
	pushq	$0
	movq	%r10, %rsi
	leaq	-120(%rbp), %rcx
	pushq	%rax
	call	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsIhmEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	popq	%r9
	popq	%r10
	movl	%eax, %r13d
	testb	%al, %al
	je	.L2010
	movq	-120(%rbp), %rdx
	movzbl	-128(%rbp), %eax
	lock xaddb	%al, (%rdx)
	movzbl	%al, %eax
	movb	$2, -80(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	$0, -71(%rbp)
	movq	%rax, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm7
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2058:
	leaq	-128(%rbp), %rax
	pushq	$0
	leaq	-120(%rbp), %rcx
	movq	%r10, %rsi
	pushq	%rax
	call	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsItjEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	popq	%rdi
	popq	%r8
	movl	%eax, %r13d
	testb	%al, %al
	je	.L2010
	movq	-120(%rbp), %rdx
	movzwl	-128(%rbp), %eax
	lock xaddw	%ax, (%rdx)
	pxor	%xmm0, %xmm0
	movzwl	%ax, %eax
	movups	%xmm0, -79(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movl	%eax, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movb	$1, -80(%rbp)
	movdqa	-80(%rbp), %xmm4
	movb	%al, 16(%rsp)
	movups	%xmm4, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2059:
	leaq	-128(%rbp), %rax
	pushq	$0
	movq	%r10, %rsi
	leaq	-120(%rbp), %rcx
	pushq	%rax
	call	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsIhjEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	popq	%r9
	popq	%r10
	movl	%eax, %r13d
	testb	%al, %al
	je	.L2010
	movq	-120(%rbp), %rdx
	movzbl	-128(%rbp), %eax
	lock xaddb	%al, (%rdx)
	pxor	%xmm0, %xmm0
	movzbl	%al, %eax
	movups	%xmm0, -79(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movl	%eax, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movb	$1, -80(%rbp)
	movdqa	-80(%rbp), %xmm3
	movb	%al, 16(%rsp)
	movups	%xmm3, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2060:
	leaq	-128(%rbp), %rax
	pushq	$0
	leaq	-120(%rbp), %rcx
	movq	%r10, %rsi
	pushq	%rax
	call	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsImmEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	popq	%r11
	popq	%rbx
	movl	%eax, %r13d
	testb	%al, %al
	je	.L2010
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %rax
	lock xaddq	%rax, (%rdx)
	movq	$0, -71(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	%rax, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movb	$2, -80(%rbp)
	movdqa	-80(%rbp), %xmm6
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2061:
	leaq	-128(%rbp), %rax
	pushq	$0
	leaq	-120(%rbp), %rcx
	movq	%r10, %rsi
	pushq	%rax
	call	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsIjjEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	popq	%r11
	popq	%rbx
	movl	%eax, %r13d
	testb	%al, %al
	je	.L2010
	movq	-120(%rbp), %rdx
	movl	-128(%rbp), %eax
	lock xaddl	%eax, (%rdx)
	pxor	%xmm0, %xmm0
	movups	%xmm0, -79(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movl	%eax, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movb	$1, -80(%rbp)
	movdqa	-80(%rbp), %xmm2
	movb	%al, 16(%rsp)
	movups	%xmm2, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2062:
	leaq	-128(%rbp), %rax
	pushq	$0
	leaq	-120(%rbp), %rcx
	movq	%r10, %rsi
	pushq	%rax
	call	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsIjmEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	movl	%eax, %r13d
	popq	%rax
	popq	%rdx
	testb	%r13b, %r13b
	je	.L2010
	movq	-120(%rbp), %rax
	movl	-128(%rbp), %edx
	movl	%edx, (%rax)
	mfence
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2063:
	leaq	-128(%rbp), %rax
	pushq	$0
	leaq	-120(%rbp), %rcx
	movq	%r10, %rsi
	pushq	%rax
	call	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsItmEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	popq	%rcx
	popq	%rsi
	movl	%eax, %r13d
	testb	%al, %al
	je	.L2010
	movq	-120(%rbp), %rdx
	movzwl	-128(%rbp), %eax
	movw	%ax, (%rdx)
	mfence
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2064:
	leaq	-128(%rbp), %rax
	pushq	$0
	leaq	-120(%rbp), %rcx
	movq	%r10, %rsi
	pushq	%rax
	call	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsIhmEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	popq	%rdi
	popq	%r8
	movl	%eax, %r13d
	testb	%al, %al
	je	.L2010
	movq	-120(%rbp), %rdx
	movzbl	-128(%rbp), %eax
	movb	%al, (%rdx)
	mfence
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2065:
	leaq	-128(%rbp), %rax
	pushq	$0
	leaq	-120(%rbp), %rcx
	movq	%r10, %rsi
	pushq	%rax
	call	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsItjEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	popq	%r11
	popq	%rbx
	movl	%eax, %r13d
	testb	%al, %al
	je	.L2010
	movq	-120(%rbp), %rdx
	movzwl	-128(%rbp), %eax
	movw	%ax, (%rdx)
	mfence
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2066:
	leaq	-128(%rbp), %rax
	pushq	$0
	leaq	-120(%rbp), %rcx
	movq	%r10, %rsi
	pushq	%rax
	call	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsIhjEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	popq	%r12
	popq	%r14
	movl	%eax, %r13d
	testb	%al, %al
	je	.L2010
	movq	-120(%rbp), %rdx
	movzbl	-128(%rbp), %eax
	movb	%al, (%rdx)
	mfence
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2067:
	leaq	-128(%rbp), %rax
	pushq	$0
	movq	%r10, %rsi
	leaq	-120(%rbp), %rcx
	pushq	%rax
	call	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsImmEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	popq	%r9
	popq	%r10
	movl	%eax, %r13d
	testb	%al, %al
	je	.L2010
	movq	-120(%rbp), %rax
	movq	-128(%rbp), %rdx
	movq	%rdx, (%rax)
	mfence
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2068:
	leaq	-128(%rbp), %rax
	pushq	$0
	leaq	-120(%rbp), %rcx
	movq	%r10, %rsi
	pushq	%rax
	call	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsIjjEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	popq	%r15
	movl	%eax, %r13d
	popq	%rax
	testb	%r13b, %r13b
	je	.L2010
	movq	-120(%rbp), %rax
	movl	-128(%rbp), %edx
	movl	%edx, (%rax)
	mfence
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2069:
	movq	64(%rdx), %rax
	leaq	1(%rax,%r8), %rax
	cmpb	$0, 1(%rax)
	js	.L2274
	movl	$6, %r8d
	movl	$5, %esi
	movl	$8, %edi
	movl	$4, %ebx
	movl	$7, %ecx
	movl	$2, %edx
.L2179:
	addq	%rdx, %rax
	movzbl	(%rax), %edx
	movl	%edx, %r13d
	andl	$127, %r13d
	testb	%dl, %dl
	js	.L2275
.L2180:
	movq	40(%r12), %rax
	leaq	-17(%rax), %rdx
	movq	%rdx, 40(%r12)
	movdqu	-17(%rax), %xmm5
	movzbl	-1(%rax), %ecx
	movzbl	-17(%rax), %eax
	movaps	%xmm5, -80(%rbp)
	movb	%cl, -64(%rbp)
	cmpb	$6, %al
	je	.L2181
	movb	%al, -80(%rbp)
	movdqa	-80(%rbp), %xmm1
	movb	%cl, -96(%rbp)
	movaps	%xmm1, -112(%rbp)
.L2182:
	addl	-111(%rbp), %r13d
	jc	.L2187
	movq	16(%r12), %rax
	xorl	%ecx, %ecx
	movq	(%rax), %rdx
	movq	31(%rdx), %rax
	cmpq	$3, %rax
	jbe	.L2188
	movl	%r13d, %r13d
	subq	$4, %rax
	cmpq	%rax, %r13
	ja	.L2188
	movq	23(%rdx), %rcx
	andq	39(%rdx), %r13
	addq	%r13, %rcx
	.p2align 4,,10
	.p2align 3
.L2188:
	movl	%ebx, (%r9)
	subq	$32, %rsp
	movq	%r12, %rdi
	movl	$1, %r13d
	movl	(%rcx), %eax
	movq	$0, -71(%rbp)
	movq	%rax, -79(%rbp)
	movb	$2, -80(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm6
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2070:
	pushq	$0
	leaq	-120(%rbp), %rcx
	movq	%r10, %rsi
	pushq	$0
	call	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsItmEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	movl	%eax, %r13d
	popq	%rax
	popq	%rdx
	testb	%r13b, %r13b
	je	.L2010
	movq	-120(%rbp), %rax
	subq	$32, %rsp
	movq	%r12, %rdi
	movzwl	(%rax), %eax
	movq	$0, -71(%rbp)
	movq	%rax, -79(%rbp)
	movb	$2, -80(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm7
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2071:
	pushq	$0
	leaq	-120(%rbp), %rcx
	movq	%r10, %rsi
	pushq	$0
	call	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsIhmEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	popq	%rcx
	popq	%rsi
	movl	%eax, %r13d
	testb	%al, %al
	je	.L2010
	movq	-120(%rbp), %rax
	subq	$32, %rsp
	movq	%r12, %rdi
	movzbl	(%rax), %eax
	movq	$0, -71(%rbp)
	movq	%rax, -79(%rbp)
	movb	$2, -80(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm6
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2072:
	pushq	$0
	leaq	-120(%rbp), %rcx
	movq	%r10, %rsi
	pushq	$0
	call	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsItjEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	popq	%rdi
	popq	%r8
	movl	%eax, %r13d
	testb	%al, %al
	je	.L2010
	movq	-120(%rbp), %rax
	pxor	%xmm0, %xmm0
	subq	$32, %rsp
	movq	%r12, %rdi
	movzwl	(%rax), %eax
	movups	%xmm0, -79(%rbp)
	movb	$1, -80(%rbp)
	movl	%eax, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm5
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2073:
	pushq	$0
	movq	%r10, %rsi
	leaq	-120(%rbp), %rcx
	pushq	$0
	call	_ZN2v88internal4wasm10ThreadImpl21ExtractAtomicOpParamsIhjEEbPNS1_7DecoderEPNS1_15InterpreterCodeEPmmPiPT_SB_
	popq	%r9
	popq	%r10
	movl	%eax, %r13d
	testb	%al, %al
	je	.L2010
	movq	-120(%rbp), %rax
	pxor	%xmm0, %xmm0
	subq	$32, %rsp
	movq	%r12, %rdi
	movzbl	(%rax), %eax
	movups	%xmm0, -79(%rbp)
	movb	$1, -80(%rbp)
	movl	%eax, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm4
	movb	%al, 16(%rsp)
	movups	%xmm4, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2074:
	movq	64(%rdx), %rax
	leaq	1(%rax,%r8), %rax
	cmpb	$0, 1(%rax)
	js	.L2276
	movl	$6, %r8d
	movl	$5, %esi
	movl	$8, %edi
	movl	$4, %ebx
	movl	$7, %ecx
	movl	$2, %edx
.L2167:
	addq	%rdx, %rax
	movzbl	(%rax), %edx
	movl	%edx, %r13d
	andl	$127, %r13d
	testb	%dl, %dl
	js	.L2277
.L2168:
	movq	40(%r12), %rax
	leaq	-17(%rax), %rdx
	movq	%rdx, 40(%r12)
	movdqu	-17(%rax), %xmm3
	movzbl	-1(%rax), %ecx
	movzbl	-17(%rax), %eax
	movaps	%xmm3, -80(%rbp)
	movb	%cl, -64(%rbp)
	cmpb	$6, %al
	je	.L2169
	movb	%al, -80(%rbp)
	movdqa	-80(%rbp), %xmm7
	movb	%cl, -96(%rbp)
	movaps	%xmm7, -112(%rbp)
.L2170:
	addl	-111(%rbp), %r13d
	jc	.L2175
	movq	16(%r12), %rax
	xorl	%ecx, %ecx
	movq	(%rax), %rdx
	movq	31(%rdx), %rax
	cmpq	$7, %rax
	jbe	.L2176
	movl	%r13d, %r13d
	subq	$8, %rax
	cmpq	%rax, %r13
	ja	.L2176
	movq	23(%rdx), %rcx
	andq	39(%rdx), %r13
	addq	%r13, %rcx
	.p2align 4,,10
	.p2align 3
.L2176:
	movl	%ebx, (%r9)
	subq	$32, %rsp
	movq	%r12, %rdi
	movl	$1, %r13d
	movq	(%rcx), %rax
	movq	$0, -71(%rbp)
	movq	%rax, -79(%rbp)
	movb	$2, -80(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm4
	movb	%al, 16(%rsp)
	movups	%xmm4, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2075:
	movq	64(%rdx), %rax
	leaq	1(%rax,%r8), %rax
	cmpb	$0, 1(%rax)
	js	.L2278
	movl	$6, %r8d
	movl	$5, %esi
	movl	$8, %edi
	movl	$4, %ebx
	movl	$7, %ecx
	movl	$2, %edx
.L2155:
	addq	%rdx, %rax
	movzbl	(%rax), %edx
	movl	%edx, %r13d
	andl	$127, %r13d
	testb	%dl, %dl
	js	.L2279
.L2156:
	movq	40(%r12), %rax
	leaq	-17(%rax), %rdx
	movq	%rdx, 40(%r12)
	movdqu	-17(%rax), %xmm1
	movzbl	-1(%rax), %ecx
	movzbl	-17(%rax), %eax
	movaps	%xmm1, -80(%rbp)
	movb	%cl, -64(%rbp)
	cmpb	$6, %al
	je	.L2157
	movb	%al, -80(%rbp)
	movdqa	-80(%rbp), %xmm7
	movb	%cl, -96(%rbp)
	movaps	%xmm7, -112(%rbp)
.L2158:
	addl	-111(%rbp), %r13d
	jc	.L2163
	movq	16(%r12), %rax
	xorl	%ecx, %ecx
	movq	(%rax), %rdx
	movq	31(%rdx), %rax
	cmpq	$3, %rax
	jbe	.L2164
	movl	%r13d, %r13d
	subq	$4, %rax
	cmpq	%rax, %r13
	ja	.L2164
	movq	23(%rdx), %rcx
	andq	39(%rdx), %r13
	addq	%r13, %rcx
	.p2align 4,,10
	.p2align 3
.L2164:
	movl	%ebx, (%r9)
	pxor	%xmm0, %xmm0
	subq	$32, %rsp
	movq	%r12, %rdi
	movl	$1, %r13d
	movl	(%rcx), %eax
	movups	%xmm0, -79(%rbp)
	movb	$1, -80(%rbp)
	movl	%eax, -79(%rbp)
	movzbl	-64(%rbp), %eax
	movdqa	-80(%rbp), %xmm2
	movb	%al, 16(%rsp)
	movups	%xmm2, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2076:
	mfence
	movl	$1, %r13d
	addl	$2, (%r9)
	jmp	.L2010
.L2279:
	movzbl	1(%rax), %r10d
	movl	%esi, %ebx
	movl	%r10d, %edx
	sall	$7, %edx
	andl	$16256, %edx
	orl	%edx, %r13d
	testb	%r10b, %r10b
	jns	.L2156
	movzbl	2(%rax), %esi
	movl	%r8d, %ebx
	movl	%esi, %edx
	sall	$14, %edx
	andl	$2080768, %edx
	orl	%edx, %r13d
	testb	%sil, %sil
	jns	.L2156
	movzbl	3(%rax), %esi
	movl	%ecx, %ebx
	movl	%esi, %edx
	sall	$21, %edx
	andl	$266338304, %edx
	orl	%edx, %r13d
	testb	%sil, %sil
	jns	.L2156
	movzbl	4(%rax), %eax
	movl	%edi, %ebx
	sall	$28, %eax
	orl	%eax, %r13d
	jmp	.L2156
	.p2align 4,,10
	.p2align 3
.L2278:
	cmpb	$0, 2(%rax)
	js	.L2280
	movl	$7, %r8d
	movl	$6, %esi
	movl	$9, %edi
	movl	$5, %ebx
	movl	$8, %ecx
	movl	$3, %edx
	jmp	.L2155
.L2277:
	movzbl	1(%rax), %r10d
	movl	%esi, %ebx
	movl	%r10d, %edx
	sall	$7, %edx
	andl	$16256, %edx
	orl	%edx, %r13d
	testb	%r10b, %r10b
	jns	.L2168
	movzbl	2(%rax), %esi
	movl	%r8d, %ebx
	movl	%esi, %edx
	sall	$14, %edx
	andl	$2080768, %edx
	orl	%edx, %r13d
	testb	%sil, %sil
	jns	.L2168
	movzbl	3(%rax), %esi
	movl	%ecx, %ebx
	movl	%esi, %edx
	sall	$21, %edx
	andl	$266338304, %edx
	orl	%edx, %r13d
	testb	%sil, %sil
	jns	.L2168
	movzbl	4(%rax), %eax
	movl	%edi, %ebx
	sall	$28, %eax
	orl	%eax, %r13d
	jmp	.L2168
	.p2align 4,,10
	.p2align 3
.L2276:
	cmpb	$0, 2(%rax)
	js	.L2281
	movl	$7, %r8d
	movl	$6, %esi
	movl	$9, %edi
	movl	$5, %ebx
	movl	$8, %ecx
	movl	$3, %edx
	jmp	.L2167
.L2275:
	movzbl	1(%rax), %r10d
	movl	%esi, %ebx
	movl	%r10d, %edx
	sall	$7, %edx
	andl	$16256, %edx
	orl	%edx, %r13d
	testb	%r10b, %r10b
	jns	.L2180
	movzbl	2(%rax), %esi
	movl	%r8d, %ebx
	movl	%esi, %edx
	sall	$14, %edx
	andl	$2080768, %edx
	orl	%edx, %r13d
	testb	%sil, %sil
	jns	.L2180
	movzbl	3(%rax), %esi
	movl	%ecx, %ebx
	movl	%esi, %edx
	sall	$21, %edx
	andl	$266338304, %edx
	orl	%edx, %r13d
	testb	%sil, %sil
	jns	.L2180
	movzbl	4(%rax), %eax
	movl	%edi, %ebx
	sall	$28, %eax
	orl	%eax, %r13d
	jmp	.L2180
	.p2align 4,,10
	.p2align 3
.L2274:
	cmpb	$0, 2(%rax)
	js	.L2282
	movl	$7, %r8d
	movl	$6, %esi
	movl	$9, %edi
	movl	$5, %ebx
	movl	$8, %ecx
	movl	$3, %edx
	jmp	.L2179
.L2169:
	movq	48(%r12), %rax
	subq	24(%r12), %rdx
	movq	8(%r12), %r14
	movq	(%rax), %rcx
	imull	$-252645135, %edx, %eax
	movq	7(%rcx), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%rax,%rdx), %r15
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L2171
	movq	%r15, %rsi
	movq	%r9, -152(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-152(%rbp), %r9
.L2172:
	movq	48(%r12), %rdx
	movq	8(%r12), %rcx
	movq	%rax, -111(%rbp)
	movq	40(%r12), %rax
	subq	24(%r12), %rax
	imull	$-252645135, %eax, %eax
	movq	(%rdx), %rdx
	movq	96(%rcx), %rcx
	movq	7(%rdx), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	%rcx, -1(%rax,%rdx)
	jmp	.L2170
.L2157:
	movq	48(%r12), %rax
	subq	24(%r12), %rdx
	movq	8(%r12), %r14
	movq	(%rax), %rcx
	imull	$-252645135, %edx, %eax
	movq	7(%rcx), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%rax,%rdx), %r15
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L2159
	movq	%r15, %rsi
	movq	%r9, -152(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-152(%rbp), %r9
.L2160:
	movq	48(%r12), %rdx
	movq	8(%r12), %rcx
	movq	%rax, -111(%rbp)
	movq	40(%r12), %rax
	subq	24(%r12), %rax
	imull	$-252645135, %eax, %eax
	movq	(%rdx), %rdx
	movq	96(%rcx), %rcx
	movq	7(%rdx), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	%rcx, -1(%rax,%rdx)
	jmp	.L2158
.L2181:
	movq	48(%r12), %rax
	subq	24(%r12), %rdx
	movq	8(%r12), %r14
	movq	(%rax), %rcx
	imull	$-252645135, %edx, %eax
	movq	7(%rcx), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%rax,%rdx), %r15
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L2183
	movq	%r15, %rsi
	movq	%r9, -152(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-152(%rbp), %r9
.L2184:
	movq	48(%r12), %rdx
	movq	8(%r12), %rcx
	movq	%rax, -111(%rbp)
	movq	40(%r12), %rax
	subq	24(%r12), %rax
	imull	$-252645135, %eax, %eax
	movq	(%rdx), %rdx
	movq	96(%rcx), %rcx
	movq	7(%rdx), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	%rcx, -1(%rax,%rdx)
	jmp	.L2182
.L2281:
	cmpb	$0, 3(%rax)
	js	.L2283
	movl	$8, %r8d
	movl	$7, %esi
	movl	$10, %edi
	movl	$6, %ebx
	movl	$9, %ecx
	movl	$4, %edx
	jmp	.L2167
.L2280:
	cmpb	$0, 3(%rax)
	js	.L2284
	movl	$8, %r8d
	movl	$7, %esi
	movl	$10, %edi
	movl	$6, %ebx
	movl	$9, %ecx
	movl	$4, %edx
	jmp	.L2155
.L2282:
	cmpb	$0, 3(%rax)
	js	.L2285
	movl	$8, %r8d
	movl	$7, %esi
	movl	$10, %edi
	movl	$6, %ebx
	movl	$9, %ecx
	movl	$4, %edx
	jmp	.L2179
.L2183:
	movq	41088(%r14), %rax
	cmpq	41096(%r14), %rax
	je	.L2286
.L2185:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r14)
	movq	%r15, (%rax)
	jmp	.L2184
.L2171:
	movq	41088(%r14), %rax
	cmpq	41096(%r14), %rax
	je	.L2287
.L2173:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r14)
	movq	%r15, (%rax)
	jmp	.L2172
.L2159:
	movq	41088(%r14), %rax
	cmpq	41096(%r14), %rax
	je	.L2288
.L2161:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r14)
	movq	%r15, (%rax)
	jmp	.L2160
.L2285:
	movsbl	4(%rax), %ecx
	movsbq	4(%rax), %rdx
	movl	%ecx, %r8d
	movl	%ecx, %esi
	movl	%ecx, %edi
	movl	%ecx, %ebx
	sarl	$31, %r8d
	sarl	$31, %esi
	sarl	$31, %edi
	sarl	$31, %ebx
	notl	%r8d
	notl	%esi
	sarl	$31, %ecx
	sarq	$63, %rdx
	notl	%edi
	notl	%ebx
	notl	%ecx
	notq	%rdx
	addl	$10, %r8d
	addl	$9, %esi
	addl	$12, %edi
	addl	$8, %ebx
	addl	$11, %ecx
	addq	$6, %rdx
	jmp	.L2179
.L2283:
	movsbl	4(%rax), %ecx
	movsbq	4(%rax), %rdx
	movl	%ecx, %r8d
	movl	%ecx, %esi
	movl	%ecx, %edi
	movl	%ecx, %ebx
	sarl	$31, %r8d
	sarl	$31, %esi
	sarl	$31, %edi
	sarl	$31, %ebx
	notl	%r8d
	notl	%esi
	sarl	$31, %ecx
	sarq	$63, %rdx
	notl	%edi
	notl	%ebx
	notl	%ecx
	notq	%rdx
	addl	$10, %r8d
	addl	$9, %esi
	addl	$12, %edi
	addl	$8, %ebx
	addl	$11, %ecx
	addq	$6, %rdx
	jmp	.L2167
.L2284:
	movsbl	4(%rax), %ecx
	movsbq	4(%rax), %rdx
	movl	%ecx, %r8d
	movl	%ecx, %esi
	movl	%ecx, %edi
	movl	%ecx, %ebx
	sarl	$31, %r8d
	sarl	$31, %esi
	sarl	$31, %edi
	sarl	$31, %ebx
	notl	%r8d
	notl	%esi
	sarl	$31, %ecx
	sarq	$63, %rdx
	notl	%edi
	notl	%ebx
	notl	%ecx
	notq	%rdx
	addl	$10, %r8d
	addl	$9, %esi
	addl	$12, %edi
	addl	$8, %ebx
	addl	$11, %ecx
	addq	$6, %rdx
	jmp	.L2155
.L2273:
	call	__stack_chk_fail@PLT
.L2287:
	movq	%r14, %rdi
	movq	%r9, -152(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-152(%rbp), %r9
	jmp	.L2173
.L2288:
	movq	%r14, %rdi
	movq	%r9, -152(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-152(%rbp), %r9
	jmp	.L2161
.L2286:
	movq	%r14, %rdi
	movq	%r9, -152(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-152(%rbp), %r9
	jmp	.L2185
.L2163:
	xorl	%ecx, %ecx
	jmp	.L2164
.L2187:
	xorl	%ecx, %ecx
	jmp	.L2188
.L2175:
	xorl	%ecx, %ecx
	jmp	.L2176
	.cfi_endproc
.LFE20124:
	.size	_ZN2v88internal4wasm10ThreadImpl15ExecuteAtomicOpENS1_10WasmOpcodeEPNS1_7DecoderEPNS1_15InterpreterCodeEmPi, .-_ZN2v88internal4wasm10ThreadImpl15ExecuteAtomicOpENS1_10WasmOpcodeEPNS1_7DecoderEPNS1_15InterpreterCodeEmPi
	.section	.text._ZNSt8_Rb_treeImSt4pairIKmN2v88internal4wasm20ControlTransferEntryEESt10_Select1stIS6_ESt4lessImENS3_13ZoneAllocatorIS6_EEE8_M_eraseEPSt13_Rb_tree_nodeIS6_E,"axG",@progbits,_ZNSt8_Rb_treeImSt4pairIKmN2v88internal4wasm20ControlTransferEntryEESt10_Select1stIS6_ESt4lessImENS3_13ZoneAllocatorIS6_EEE8_M_eraseEPSt13_Rb_tree_nodeIS6_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeImSt4pairIKmN2v88internal4wasm20ControlTransferEntryEESt10_Select1stIS6_ESt4lessImENS3_13ZoneAllocatorIS6_EEE8_M_eraseEPSt13_Rb_tree_nodeIS6_E
	.type	_ZNSt8_Rb_treeImSt4pairIKmN2v88internal4wasm20ControlTransferEntryEESt10_Select1stIS6_ESt4lessImENS3_13ZoneAllocatorIS6_EEE8_M_eraseEPSt13_Rb_tree_nodeIS6_E, @function
_ZNSt8_Rb_treeImSt4pairIKmN2v88internal4wasm20ControlTransferEntryEESt10_Select1stIS6_ESt4lessImENS3_13ZoneAllocatorIS6_EEE8_M_eraseEPSt13_Rb_tree_nodeIS6_E:
.LFB23888:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L2297
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L2291:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmN2v88internal4wasm20ControlTransferEntryEESt10_Select1stIS6_ESt4lessImENS3_13ZoneAllocatorIS6_EEE8_M_eraseEPSt13_Rb_tree_nodeIS6_E
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L2291
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2297:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE23888:
	.size	_ZNSt8_Rb_treeImSt4pairIKmN2v88internal4wasm20ControlTransferEntryEESt10_Select1stIS6_ESt4lessImENS3_13ZoneAllocatorIS6_EEE8_M_eraseEPSt13_Rb_tree_nodeIS6_E, .-_ZNSt8_Rb_treeImSt4pairIKmN2v88internal4wasm20ControlTransferEntryEESt10_Select1stIS6_ESt4lessImENS3_13ZoneAllocatorIS6_EEE8_M_eraseEPSt13_Rb_tree_nodeIS6_E
	.section	.text._ZNSt6vectorIZN2v88internal4wasm9SideTableC4EPNS1_4ZoneEPKNS2_10WasmModuleEPNS2_15InterpreterCodeEEN6CLabel3RefENS1_13ZoneAllocatorISC_EEE12emplace_backIJSC_EEEvDpOT_,"axG",@progbits,_ZNSt6vectorIZN2v88internal4wasm9SideTableC4EPNS1_4ZoneEPKNS2_10WasmModuleEPNS2_15InterpreterCodeEEN6CLabel3RefENS1_13ZoneAllocatorISC_EEE12emplace_backIJSC_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIZN2v88internal4wasm9SideTableC4EPNS1_4ZoneEPKNS2_10WasmModuleEPNS2_15InterpreterCodeEEN6CLabel3RefENS1_13ZoneAllocatorISC_EEE12emplace_backIJSC_EEEvDpOT_
	.type	_ZNSt6vectorIZN2v88internal4wasm9SideTableC4EPNS1_4ZoneEPKNS2_10WasmModuleEPNS2_15InterpreterCodeEEN6CLabel3RefENS1_13ZoneAllocatorISC_EEE12emplace_backIJSC_EEEvDpOT_, @function
_ZNSt6vectorIZN2v88internal4wasm9SideTableC4EPNS1_4ZoneEPKNS2_10WasmModuleEPNS2_15InterpreterCodeEEN6CLabel3RefENS1_13ZoneAllocatorISC_EEE12emplace_backIJSC_EEEvDpOT_:
.LFB23902:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	16(%rdi), %r12
	cmpq	24(%rdi), %r12
	je	.L2301
	movq	(%rsi), %rdx
	movl	8(%rsi), %eax
	movq	%rdx, (%r12)
	movl	%eax, 8(%r12)
	addq	$16, 16(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2301:
	.cfi_restore_state
	movq	8(%rdi), %r14
	movq	%r12, %r15
	subq	%r14, %r15
	movq	%r15, %rax
	sarq	$4, %rax
	cmpq	$134217727, %rax
	je	.L2315
	testq	%rax, %rax
	je	.L2310
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L2316
	movl	$2147483632, %esi
	movl	$2147483632, %r8d
.L2304:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rsi, %rdx
	jb	.L2317
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L2307:
	addq	%rax, %r8
	leaq	16(%rax), %rcx
.L2305:
	movq	0(%r13), %rdi
	movl	8(%r13), %esi
	leaq	(%rax,%r15), %rdx
	movq	%rdi, (%rdx)
	movl	%esi, 8(%rdx)
	cmpq	%r14, %r12
	je	.L2308
	movq	%r14, %rdx
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L2309:
	movq	(%rdx), %rdi
	movl	8(%rdx), %esi
	addq	$16, %rdx
	addq	$16, %rcx
	movq	%rdi, -16(%rcx)
	movl	%esi, -8(%rcx)
	cmpq	%rdx, %r12
	jne	.L2309
	subq	%r14, %r12
	leaq	16(%rax,%r12), %rcx
.L2308:
	movq	%rax, %xmm0
	movq	%rcx, %xmm1
	movq	%r8, 24(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2316:
	.cfi_restore_state
	testq	%rdx, %rdx
	jne	.L2318
	movl	$16, %ecx
	xorl	%r8d, %r8d
	xorl	%eax, %eax
	jmp	.L2305
	.p2align 4,,10
	.p2align 3
.L2310:
	movl	$16, %esi
	movl	$16, %r8d
	jmp	.L2304
.L2317:
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r8
	jmp	.L2307
.L2315:
	leaq	.LC5(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L2318:
	cmpq	$134217727, %rdx
	movl	$134217727, %r8d
	cmovbe	%rdx, %r8
	salq	$4, %r8
	movq	%r8, %rsi
	jmp	.L2304
	.cfi_endproc
.LFE23902:
	.size	_ZNSt6vectorIZN2v88internal4wasm9SideTableC4EPNS1_4ZoneEPKNS2_10WasmModuleEPNS2_15InterpreterCodeEEN6CLabel3RefENS1_13ZoneAllocatorISC_EEE12emplace_backIJSC_EEEvDpOT_, .-_ZNSt6vectorIZN2v88internal4wasm9SideTableC4EPNS1_4ZoneEPKNS2_10WasmModuleEPNS2_15InterpreterCodeEEN6CLabel3RefENS1_13ZoneAllocatorISC_EEE12emplace_backIJSC_EEEvDpOT_
	.section	.text._ZNSt6vectorIN2v88internal4wasm10ThreadImplENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRPNS1_4ZoneEPNS2_7CodeMapERNS1_6HandleINS1_18WasmInstanceObjectEEERNSD_INS1_4CellEEEEEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal4wasm10ThreadImplENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRPNS1_4ZoneEPNS2_7CodeMapERNS1_6HandleINS1_18WasmInstanceObjectEEERNSD_INS1_4CellEEEEEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal4wasm10ThreadImplENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRPNS1_4ZoneEPNS2_7CodeMapERNS1_6HandleINS1_18WasmInstanceObjectEEERNSD_INS1_4CellEEEEEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal4wasm10ThreadImplENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRPNS1_4ZoneEPNS2_7CodeMapERNS1_6HandleINS1_18WasmInstanceObjectEEERNSD_INS1_4CellEEEEEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_, @function
_ZNSt6vectorIN2v88internal4wasm10ThreadImplENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRPNS1_4ZoneEPNS2_7CodeMapERNS1_6HandleINS1_18WasmInstanceObjectEEERNSD_INS1_4CellEEEEEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_:
.LFB24196:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r11
	movabsq	$-8737931403336103397, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r15
	movq	8(%rdi), %rbx
	movq	%r15, %rax
	subq	%rbx, %rax
	sarq	$3, %rax
	imulq	%rsi, %rax
	cmpq	$14128181, %rax
	je	.L2343
	movq	%r11, %r10
	movq	%rdi, %r13
	subq	%rbx, %r10
	testq	%rax, %rax
	je	.L2334
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L2344
	movl	$2147483512, %esi
	movl	$2147483512, %r14d
.L2321:
	movq	0(%r13), %rdi
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	%rsi, %rax
	jb	.L2345
	addq	%r12, %rsi
	movq	%rsi, 16(%rdi)
.L2324:
	leaq	(%r12,%r14), %rax
	leaq	152(%r12), %r14
	movq	%rax, -56(%rbp)
	jmp	.L2322
	.p2align 4,,10
	.p2align 3
.L2344:
	testq	%rsi, %rsi
	jne	.L2346
	movq	$0, -56(%rbp)
	movl	$152, %r14d
	xorl	%r12d, %r12d
.L2322:
	movq	(%rdx), %rsi
	movq	(%rcx), %rdx
	leaq	(%r12,%r10), %rax
	pxor	%xmm1, %xmm1
	movq	(%r8), %rcx
	movq	(%r9), %xmm0
	movl	$2147483648, %edi
	movl	$0, 88(%rax)
	movq	%rdx, (%rax)
	movq	%rsi, %xmm2
	movq	(%rcx), %rdx
	punpcklqdq	%xmm2, %xmm0
	movq	%rdi, 96(%rax)
	movl	$12, 104(%rax)
	andq	$-262144, %rdx
	movq	$0, 112(%rax)
	movq	24(%rdx), %rdx
	movups	%xmm1, 32(%rax)
	movq	%rcx, 16(%rax)
	subq	$37592, %rdx
	movq	$0, 24(%rax)
	movq	%rdx, 8(%rax)
	xorl	%edx, %edx
	movq	$0, 80(%rax)
	movw	%dx, 108(%rax)
	movq	%rsi, 120(%rax)
	movq	$0, 128(%rax)
	movq	$0, 136(%rax)
	movq	$0, 144(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm1, 64(%rax)
	cmpq	%rbx, %r11
	je	.L2325
	movq	%rbx, %rax
	movq	%r12, %rdx
	movdqa	%xmm1, %xmm0
	.p2align 4,,10
	.p2align 3
.L2326:
	movdqu	(%rax), %xmm2
	addq	$152, %rax
	addq	$152, %rdx
	movups	%xmm2, -152(%rdx)
	movq	-136(%rax), %rcx
	movq	%rcx, -136(%rdx)
	movq	-128(%rax), %rcx
	movq	$0, -128(%rax)
	movq	%rcx, -128(%rdx)
	movq	-120(%rax), %rcx
	movq	%rcx, -120(%rdx)
	movq	-112(%rax), %rcx
	movq	%rcx, -112(%rdx)
	movq	-104(%rax), %rcx
	movq	%rcx, -104(%rdx)
	movq	-96(%rax), %rcx
	movq	%rcx, -96(%rdx)
	movdqu	-88(%rax), %xmm3
	movups	%xmm3, -88(%rdx)
	movq	-72(%rax), %rcx
	movq	%rcx, -72(%rdx)
	movl	-64(%rax), %ecx
	movq	$0, -72(%rax)
	movups	%xmm0, -88(%rax)
	movl	%ecx, -64(%rdx)
	movq	-56(%rax), %rcx
	movq	%rcx, -56(%rdx)
	movl	-48(%rax), %ecx
	movl	%ecx, -48(%rdx)
	movzbl	-44(%rax), %ecx
	movb	%cl, -44(%rdx)
	movzbl	-43(%rax), %ecx
	movb	%cl, -43(%rdx)
	movq	-40(%rax), %rcx
	movq	%rcx, -40(%rdx)
	movq	-32(%rax), %rcx
	movq	%rcx, -32(%rdx)
	movdqu	-24(%rax), %xmm4
	movups	%xmm4, -24(%rdx)
	movq	-8(%rax), %rcx
	movq	%rcx, -8(%rdx)
	movq	$0, -8(%rax)
	movups	%xmm0, -24(%rax)
	cmpq	%rax, %r11
	jne	.L2326
	leaq	-152(%r11), %rax
	movabsq	$485440633518672411, %rdx
	subq	%rbx, %rax
	shrq	$3, %rax
	imulq	%rdx, %rax
	movabsq	$2305843009213693951, %rdx
	andq	%rdx, %rax
	addq	$2, %rax
	leaq	(%rax,%rax,8), %rdx
	leaq	(%rax,%rdx,2), %rax
	leaq	(%r12,%rax,8), %r14
.L2325:
	movq	%r11, %rax
	movq	%r14, %rdx
	pxor	%xmm0, %xmm0
	cmpq	%r15, %r11
	je	.L2330
	.p2align 4,,10
	.p2align 3
.L2329:
	movdqu	(%rax), %xmm5
	addq	$152, %rax
	addq	$152, %rdx
	movups	%xmm5, -152(%rdx)
	movq	-136(%rax), %rcx
	movq	%rcx, -136(%rdx)
	movq	-128(%rax), %rcx
	movq	$0, -128(%rax)
	movq	%rcx, -128(%rdx)
	movq	-120(%rax), %rcx
	movq	%rcx, -120(%rdx)
	movq	-112(%rax), %rcx
	movq	%rcx, -112(%rdx)
	movq	-104(%rax), %rcx
	movq	%rcx, -104(%rdx)
	movq	-96(%rax), %rcx
	movq	%rcx, -96(%rdx)
	movdqu	-88(%rax), %xmm6
	movups	%xmm6, -88(%rdx)
	movq	-72(%rax), %rcx
	movq	%rcx, -72(%rdx)
	movl	-64(%rax), %ecx
	movq	$0, -72(%rax)
	movups	%xmm0, -88(%rax)
	movl	%ecx, -64(%rdx)
	movq	-56(%rax), %rcx
	movq	%rcx, -56(%rdx)
	movl	-48(%rax), %ecx
	movl	%ecx, -48(%rdx)
	movzbl	-44(%rax), %ecx
	movb	%cl, -44(%rdx)
	movzbl	-43(%rax), %ecx
	movb	%cl, -43(%rdx)
	movq	-40(%rax), %rcx
	movq	%rcx, -40(%rdx)
	movq	-32(%rax), %rcx
	movq	%rcx, -32(%rdx)
	movdqu	-24(%rax), %xmm7
	movups	%xmm7, -24(%rdx)
	movq	-8(%rax), %rcx
	movq	%rcx, -8(%rdx)
	movq	$0, -8(%rax)
	movups	%xmm0, -24(%rax)
	cmpq	%rax, %r15
	jne	.L2329
	movabsq	$485440633518672411, %rdx
	movq	%r15, %rax
	subq	%r11, %rax
	subq	$152, %rax
	shrq	$3, %rax
	imulq	%rdx, %rax
	movabsq	$2305843009213693951, %rdx
	andq	%rdx, %rax
	addq	$1, %rax
	leaq	(%rax,%rax,8), %rdx
	leaq	(%rax,%rdx,2), %rax
	leaq	(%r14,%rax,8), %r14
.L2330:
	cmpq	%r15, %rbx
	je	.L2332
	.p2align 4,,10
	.p2align 3
.L2328:
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2331
	call	_ZdaPv@PLT
	addq	$152, %rbx
	cmpq	%r15, %rbx
	jne	.L2328
.L2332:
	movq	-56(%rbp), %rax
	movq	%r12, %xmm0
	movq	%r14, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, 24(%r13)
	movups	%xmm0, 8(%r13)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2331:
	.cfi_restore_state
	addq	$152, %rbx
	cmpq	%r15, %rbx
	jne	.L2328
	jmp	.L2332
	.p2align 4,,10
	.p2align 3
.L2334:
	movl	$152, %esi
	movl	$152, %r14d
	jmp	.L2321
.L2345:
	movq	%r9, -96(%rbp)
	movq	%r8, -88(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%rdx, -72(%rbp)
	movq	%r11, -64(%rbp)
	movq	%r10, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r10
	movq	-64(%rbp), %r11
	movq	-72(%rbp), %rdx
	movq	-80(%rbp), %rcx
	movq	%rax, %r12
	movq	-88(%rbp), %r8
	movq	-96(%rbp), %r9
	jmp	.L2324
.L2346:
	cmpq	$14128181, %rsi
	movl	$14128181, %eax
	cmovbe	%rsi, %rax
	imulq	$152, %rax, %r14
	movq	%r14, %rsi
	jmp	.L2321
.L2343:
	leaq	.LC5(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE24196:
	.size	_ZNSt6vectorIN2v88internal4wasm10ThreadImplENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRPNS1_4ZoneEPNS2_7CodeMapERNS1_6HandleINS1_18WasmInstanceObjectEEERNSD_INS1_4CellEEEEEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_, .-_ZNSt6vectorIN2v88internal4wasm10ThreadImplENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRPNS1_4ZoneEPNS2_7CodeMapERNS1_6HandleINS1_18WasmInstanceObjectEEERNSD_INS1_4CellEEEEEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	.section	.rodata._ZN2v88internal4wasm15WasmInterpreterC2EPNS0_7IsolateEPKNS1_10WasmModuleERKNS1_15ModuleWireBytesENS0_6HandleINS0_18WasmInstanceObjectEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC52:
	.string	"../deps/v8/src/wasm/wasm-interpreter.cc:4061"
	.align 8
.LC53:
	.string	"cannot create std::vector larger than max_size()"
	.section	.text._ZN2v88internal4wasm15WasmInterpreterC2EPNS0_7IsolateEPKNS1_10WasmModuleERKNS1_15ModuleWireBytesENS0_6HandleINS0_18WasmInstanceObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15WasmInterpreterC2EPNS0_7IsolateEPKNS1_10WasmModuleERKNS1_15ModuleWireBytesENS0_6HandleINS0_18WasmInstanceObjectEEE
	.type	_ZN2v88internal4wasm15WasmInterpreterC2EPNS0_7IsolateEPKNS1_10WasmModuleERKNS1_15ModuleWireBytesENS0_6HandleINS0_18WasmInstanceObjectEEE, @function
_ZN2v88internal4wasm15WasmInterpreterC2EPNS0_7IsolateEPKNS1_10WasmModuleERKNS1_15ModuleWireBytesENS0_6HandleINS0_18WasmInstanceObjectEEE:
.LFB20341:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	leaq	.LC52(%rip), %rdx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movq	41136(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal4ZoneC1EPNS0_19AccountingAllocatorEPKc@PLT
	movq	41152(%rbx), %rdi
	movq	0(%r13), %rsi
	call	_ZN2v88internal13GlobalHandles6CreateENS0_6ObjectE@PLT
	xorl	%ecx, %ecx
	leaq	_ZN2v88internal4wasm12_GLOBAL__N_112NopFinalizerERKNS_16WeakCallbackInfoIvEE(%rip), %rdx
	movq	%rax, %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal13GlobalHandles8MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	movq	16(%r12), %rbx
	movq	24(%r12), %rax
	subq	%rbx, %rax
	cmpq	$111, %rax
	jbe	.L2373
	leaq	112(%rbx), %rax
	movq	%rax, 16(%r12)
.L2349:
	movq	%r13, -80(%rbp)
	movq	(%r15), %r13
	movq	8(%r15), %r15
	movq	%r12, -88(%rbp)
	movq	$0, 8(%rbx)
	movq	%r12, (%rbx)
	leaq	0(%r13,%r15), %rcx
	movq	$0, 16(%rbx)
	movq	$0, 24(%rbx)
	cmpq	$2147483647, %r15
	ja	.L2374
	testq	%r15, %r15
	je	.L2351
	movq	16(%r12), %rax
	movq	24(%r12), %rdx
	leaq	7(%r15), %rsi
	andq	$-8, %rsi
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L2375
	addq	%rax, %rsi
	movq	%rsi, 16(%r12)
.L2353:
	leaq	(%rax,%r15), %rsi
	movq	%rax, 8(%rbx)
	movq	%rax, %rdx
	movq	%rsi, 24(%rbx)
	cmpq	%rcx, %r13
	je	.L2363
	leaq	15(%r13), %rdx
	subq	%rax, %rdx
	cmpq	$30, %rdx
	jbe	.L2355
	leaq	-1(%r15), %rdx
	cmpq	$14, %rdx
	jbe	.L2355
	movq	%r15, %rdi
	movq	%r13, %r8
	movq	%rax, %rdx
	andq	$-16, %rdi
	subq	%rax, %r8
	addq	%rax, %rdi
	.p2align 4,,10
	.p2align 3
.L2356:
	movdqu	(%rdx,%r8), %xmm1
	addq	$16, %rdx
	movups	%xmm1, -16(%rdx)
	cmpq	%rdi, %rdx
	jne	.L2356
	movq	%r15, %rdi
	andq	$-16, %rdi
	leaq	0(%r13,%rdi), %rdx
	addq	%rdi, %rax
	cmpq	%rdi, %r15
	je	.L2358
	movzbl	(%rdx), %edi
	movb	%dil, (%rax)
	leaq	1(%rdx), %rdi
	cmpq	%rdi, %rcx
	je	.L2358
	movzbl	1(%rdx), %edi
	movb	%dil, 1(%rax)
	leaq	2(%rdx), %rdi
	cmpq	%rdi, %rcx
	je	.L2358
	movzbl	2(%rdx), %edi
	movb	%dil, 2(%rax)
	leaq	3(%rdx), %rdi
	cmpq	%rdi, %rcx
	je	.L2358
	movzbl	3(%rdx), %edi
	movb	%dil, 3(%rax)
	leaq	4(%rdx), %rdi
	cmpq	%rdi, %rcx
	je	.L2358
	movzbl	4(%rdx), %edi
	movb	%dil, 4(%rax)
	leaq	5(%rdx), %rdi
	cmpq	%rdi, %rcx
	je	.L2358
	movzbl	5(%rdx), %edi
	movb	%dil, 5(%rax)
	leaq	6(%rdx), %rdi
	cmpq	%rdi, %rcx
	je	.L2358
	movzbl	6(%rdx), %edi
	movb	%dil, 6(%rax)
	leaq	7(%rdx), %rdi
	cmpq	%rdi, %rcx
	je	.L2358
	movzbl	7(%rdx), %edi
	movb	%dil, 7(%rax)
	leaq	8(%rdx), %rdi
	cmpq	%rdi, %rcx
	je	.L2358
	movzbl	8(%rdx), %edi
	movb	%dil, 8(%rax)
	leaq	9(%rdx), %rdi
	cmpq	%rdi, %rcx
	je	.L2358
	movzbl	9(%rdx), %edi
	movb	%dil, 9(%rax)
	leaq	10(%rdx), %rdi
	cmpq	%rdi, %rcx
	je	.L2358
	movzbl	10(%rdx), %edi
	movb	%dil, 10(%rax)
	leaq	11(%rdx), %rdi
	cmpq	%rdi, %rcx
	je	.L2358
	movzbl	11(%rdx), %edi
	movb	%dil, 11(%rax)
	leaq	12(%rdx), %rdi
	cmpq	%rdi, %rcx
	je	.L2358
	movzbl	12(%rdx), %edi
	movb	%dil, 12(%rax)
	leaq	13(%rdx), %rdi
	cmpq	%rdi, %rcx
	je	.L2358
	movzbl	13(%rdx), %edi
	movb	%dil, 13(%rax)
	leaq	14(%rdx), %rdi
	cmpq	%rdi, %rcx
	je	.L2358
	movzbl	14(%rdx), %edx
	movb	%dl, 14(%rax)
	.p2align 4,,10
	.p2align 3
.L2358:
	movq	8(%rbx), %r8
	movq	%rsi, %rdx
.L2354:
	movq	%rdx, 16(%rbx)
	movq	-88(%rbp), %rcx
	leaq	32(%rbx), %r13
	movq	%r8, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm7CodeMapC1EPKNS1_10WasmModuleEPKhPNS0_4ZoneE
	movq	-88(%rbp), %rax
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	movq	%rax, 80(%rbx)
	movq	-80(%rbp), %rax
	movq	$0, 104(%rbx)
	movq	(%rax), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rsi
	leaq	-37592(%rsi), %rdi
	subq	$37304, %rsi
	movq	41152(%rdi), %r14
	call	_ZN2v88internal7Factory7NewCellENS0_6HandleINS0_6ObjectEEE@PLT
	movq	(%rax), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal13GlobalHandles6CreateENS0_6ObjectE@PLT
	movq	%r13, -64(%rbp)
	movq	96(%rbx), %rsi
	movq	%rax, -72(%rbp)
	cmpq	104(%rbx), %rsi
	je	.L2360
	movq	-80(%rbp), %rdi
	movq	-88(%rbp), %rcx
	pxor	%xmm0, %xmm0
	movq	%r13, (%rsi)
	movl	$0, 88(%rsi)
	movq	(%rdi), %rdx
	movl	$12, 104(%rsi)
	movq	$0, 112(%rsi)
	andq	$-262144, %rdx
	movups	%xmm0, 32(%rsi)
	movq	24(%rdx), %rdx
	movq	%rax, 48(%rsi)
	movl	$2147483648, %eax
	movq	%rax, 96(%rsi)
	xorl	%eax, %eax
	subq	$37592, %rdx
	movq	%rdi, 16(%rsi)
	movq	%rdx, 8(%rsi)
	movq	$0, 24(%rsi)
	movq	%rcx, 56(%rsi)
	movq	$0, 80(%rsi)
	movw	%ax, 108(%rsi)
	movq	%rcx, 120(%rsi)
	movq	$0, 128(%rsi)
	movq	$0, 136(%rsi)
	movq	$0, 144(%rsi)
	movups	%xmm0, 64(%rsi)
	addq	$152, 96(%rbx)
.L2361:
	movq	%rbx, 64(%r12)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2376
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2375:
	.cfi_restore_state
	movq	%r12, %rdi
	movq	%rcx, -104(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-104(%rbp), %rcx
	jmp	.L2353
	.p2align 4,,10
	.p2align 3
.L2355:
	movq	%r13, %rdx
	subq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L2359:
	movzbl	(%rax,%rdx), %ecx
	addq	$1, %rax
	movb	%cl, -1(%rax)
	cmpq	%rsi, %rax
	jne	.L2359
	jmp	.L2358
	.p2align 4,,10
	.p2align 3
.L2360:
	leaq	-64(%rbp), %rcx
	leaq	-88(%rbp), %rdx
	leaq	80(%rbx), %rdi
	leaq	-72(%rbp), %r9
	leaq	-80(%rbp), %r8
	call	_ZNSt6vectorIN2v88internal4wasm10ThreadImplENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRPNS1_4ZoneEPNS2_7CodeMapERNS1_6HandleINS1_18WasmInstanceObjectEEERNSD_INS1_4CellEEEEEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	jmp	.L2361
	.p2align 4,,10
	.p2align 3
.L2351:
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	jmp	.L2354
	.p2align 4,,10
	.p2align 3
.L2373:
	movl	$112, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rbx
	jmp	.L2349
.L2374:
	leaq	.LC53(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L2376:
	call	__stack_chk_fail@PLT
.L2363:
	movq	%rax, %r8
	jmp	.L2354
	.cfi_endproc
.LFE20341:
	.size	_ZN2v88internal4wasm15WasmInterpreterC2EPNS0_7IsolateEPKNS1_10WasmModuleERKNS1_15ModuleWireBytesENS0_6HandleINS0_18WasmInstanceObjectEEE, .-_ZN2v88internal4wasm15WasmInterpreterC2EPNS0_7IsolateEPKNS1_10WasmModuleERKNS1_15ModuleWireBytesENS0_6HandleINS0_18WasmInstanceObjectEEE
	.globl	_ZN2v88internal4wasm15WasmInterpreterC1EPNS0_7IsolateEPKNS1_10WasmModuleERKNS1_15ModuleWireBytesENS0_6HandleINS0_18WasmInstanceObjectEEE
	.set	_ZN2v88internal4wasm15WasmInterpreterC1EPNS0_7IsolateEPKNS1_10WasmModuleERKNS1_15ModuleWireBytesENS0_6HandleINS0_18WasmInstanceObjectEEE,_ZN2v88internal4wasm15WasmInterpreterC2EPNS0_7IsolateEPKNS1_10WasmModuleERKNS1_15ModuleWireBytesENS0_6HandleINS0_18WasmInstanceObjectEEE
	.section	.text._ZNSt8_Rb_treeImSt4pairIKmN2v88internal4wasm20ControlTransferEntryEESt10_Select1stIS6_ESt4lessImENS3_13ZoneAllocatorIS6_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS6_ERS1_,"axG",@progbits,_ZNSt8_Rb_treeImSt4pairIKmN2v88internal4wasm20ControlTransferEntryEESt10_Select1stIS6_ESt4lessImENS3_13ZoneAllocatorIS6_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS6_ERS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeImSt4pairIKmN2v88internal4wasm20ControlTransferEntryEESt10_Select1stIS6_ESt4lessImENS3_13ZoneAllocatorIS6_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS6_ERS1_
	.type	_ZNSt8_Rb_treeImSt4pairIKmN2v88internal4wasm20ControlTransferEntryEESt10_Select1stIS6_ESt4lessImENS3_13ZoneAllocatorIS6_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS6_ERS1_, @function
_ZNSt8_Rb_treeImSt4pairIKmN2v88internal4wasm20ControlTransferEntryEESt10_Select1stIS6_ESt4lessImENS3_13ZoneAllocatorIS6_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS6_ERS1_:
.LFB24758:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	cmpq	%rsi, %r15
	je	.L2427
	movq	(%rdx), %r14
	cmpq	32(%rsi), %r14
	jnb	.L2388
	movq	32(%rdi), %rbx
	movq	%rbx, %rax
	movq	%rbx, %rdx
	cmpq	%rsi, %rbx
	je	.L2380
	movq	%rsi, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %rdx
	cmpq	32(%rax), %r14
	jbe	.L2390
	movl	$0, %ebx
	cmpq	$0, 24(%rax)
	movq	%rbx, %rax
	cmovne	%r12, %rdx
	cmovne	%r12, %rax
.L2380:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2388:
	.cfi_restore_state
	jbe	.L2399
	movq	40(%rdi), %rdx
	cmpq	%rsi, %rdx
	je	.L2426
	movq	%rsi, %rdi
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %rdx
	cmpq	32(%rax), %r14
	jnb	.L2401
	movl	$0, %ebx
	cmpq	$0, 24(%r12)
	movq	%rbx, %rax
	cmovne	%rdx, %rax
	cmove	%r12, %rdx
	jmp	.L2380
	.p2align 4,,10
	.p2align 3
.L2427:
	cmpq	$0, 48(%rdi)
	je	.L2379
	movq	40(%rdi), %rdx
	movq	(%r14), %rax
	cmpq	%rax, 32(%rdx)
	jb	.L2426
.L2379:
	movq	24(%r13), %rbx
	testq	%rbx, %rbx
	je	.L2410
	movq	(%r14), %rsi
	jmp	.L2382
	.p2align 4,,10
	.p2align 3
.L2428:
	movq	16(%rbx), %rax
	movl	$1, %ecx
	testq	%rax, %rax
	je	.L2383
.L2429:
	movq	%rax, %rbx
.L2382:
	movq	32(%rbx), %rdx
	cmpq	%rdx, %rsi
	jb	.L2428
	movq	24(%rbx), %rax
	xorl	%ecx, %ecx
	testq	%rax, %rax
	jne	.L2429
.L2383:
	movq	%rbx, %r12
	testb	%cl, %cl
	jne	.L2381
.L2386:
	xorl	%eax, %eax
	cmpq	%rsi, %rdx
	cmovb	%rax, %rbx
	cmovnb	%rax, %r12
.L2387:
	addq	$8, %rsp
	movq	%rbx, %rax
	movq	%r12, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2399:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%rsi, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2426:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2390:
	.cfi_restore_state
	movq	24(%r13), %r12
	testq	%r12, %r12
	jne	.L2393
	jmp	.L2430
	.p2align 4,,10
	.p2align 3
.L2431:
	movq	16(%r12), %rax
	movl	$1, %esi
.L2396:
	testq	%rax, %rax
	je	.L2394
	movq	%rax, %r12
.L2393:
	movq	32(%r12), %rcx
	cmpq	%rcx, %r14
	jb	.L2431
	movq	24(%r12), %rax
	xorl	%esi, %esi
	jmp	.L2396
	.p2align 4,,10
	.p2align 3
.L2410:
	movq	%r12, %rbx
.L2381:
	cmpq	%rbx, 32(%r13)
	je	.L2412
	movq	%rbx, %rdi
	movq	%rbx, %r12
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	(%r14), %rsi
	movq	32(%rax), %rdx
	movq	%rax, %rbx
	jmp	.L2386
	.p2align 4,,10
	.p2align 3
.L2394:
	movq	%r12, %rdx
	testb	%sil, %sil
	jne	.L2392
.L2397:
	xorl	%eax, %eax
	cmpq	%rcx, %r14
	cmova	%rax, %r12
	cmovbe	%rax, %rdx
.L2398:
	movq	%r12, %rax
	jmp	.L2380
.L2430:
	movq	%r15, %r12
.L2392:
	cmpq	%r12, %rbx
	je	.L2416
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%r12, %rdx
	movq	32(%rax), %rcx
	movq	%rax, %r12
	jmp	.L2397
	.p2align 4,,10
	.p2align 3
.L2401:
	movq	24(%r13), %rbx
	testq	%rbx, %rbx
	jne	.L2404
	jmp	.L2432
	.p2align 4,,10
	.p2align 3
.L2433:
	movq	16(%rbx), %rax
	movl	$1, %esi
.L2407:
	testq	%rax, %rax
	je	.L2405
	movq	%rax, %rbx
.L2404:
	movq	32(%rbx), %rcx
	cmpq	%rcx, %r14
	jb	.L2433
	movq	24(%rbx), %rax
	xorl	%esi, %esi
	jmp	.L2407
	.p2align 4,,10
	.p2align 3
.L2405:
	movq	%rbx, %rdx
	testb	%sil, %sil
	jne	.L2403
.L2408:
	xorl	%eax, %eax
	cmpq	%rcx, %r14
	cmova	%rax, %rbx
	cmovbe	%rax, %rdx
.L2409:
	movq	%rbx, %rax
	jmp	.L2380
	.p2align 4,,10
	.p2align 3
.L2412:
	movq	%rbx, %r12
	xorl	%ebx, %ebx
	jmp	.L2387
.L2432:
	movq	%r15, %rbx
.L2403:
	cmpq	%rbx, 32(%r13)
	je	.L2420
	movq	%rbx, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%rbx, %rdx
	movq	32(%rax), %rcx
	movq	%rax, %rbx
	jmp	.L2408
	.p2align 4,,10
	.p2align 3
.L2416:
	movq	%r12, %rdx
	xorl	%r12d, %r12d
	jmp	.L2398
.L2420:
	movq	%rbx, %rdx
	xorl	%ebx, %ebx
	jmp	.L2409
	.cfi_endproc
.LFE24758:
	.size	_ZNSt8_Rb_treeImSt4pairIKmN2v88internal4wasm20ControlTransferEntryEESt10_Select1stIS6_ESt4lessImENS3_13ZoneAllocatorIS6_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS6_ERS1_, .-_ZNSt8_Rb_treeImSt4pairIKmN2v88internal4wasm20ControlTransferEntryEESt10_Select1stIS6_ESt4lessImENS3_13ZoneAllocatorIS6_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS6_ERS1_
	.section	.rodata._ZN2v88internal4wasm9SideTableC2EPNS0_4ZoneEPKNS1_10WasmModuleEPNS1_15InterpreterCodeE.str1.8,"aMS",@progbits,1
	.align 8
.LC54:
	.string	"../deps/v8/src/wasm/wasm-interpreter.cc:676"
	.section	.text._ZN2v88internal4wasm9SideTableC2EPNS0_4ZoneEPKNS1_10WasmModuleEPNS1_15InterpreterCodeE,"axG",@progbits,_ZN2v88internal4wasm9SideTableC5EPNS0_4ZoneEPKNS1_10WasmModuleEPNS1_15InterpreterCodeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm9SideTableC2EPNS0_4ZoneEPKNS1_10WasmModuleEPNS1_15InterpreterCodeE
	.type	_ZN2v88internal4wasm9SideTableC2EPNS0_4ZoneEPKNS1_10WasmModuleEPNS1_15InterpreterCodeE, @function
_ZN2v88internal4wasm9SideTableC2EPNS0_4ZoneEPKNS1_10WasmModuleEPNS1_15InterpreterCodeE:
.LFB19999:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$424, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -464(%rbp)
	movq	%rdx, -416(%rbp)
	leaq	.LC54(%rip), %rdx
	movq	%rcx, -376(%rbp)
	movq	%fs:40, %rdi
	movq	%rdi, -56(%rbp)
	xorl	%edi, %edi
	movq	%rsi, (%r15)
	leaq	16(%r15), %rsi
	movl	$0, 16(%r15)
	movq	$0, 24(%r15)
	movq	%rsi, 32(%r15)
	movq	%rsi, 40(%r15)
	movq	$0, 48(%r15)
	movl	$0, 56(%r15)
	movq	%rsi, -424(%rbp)
	movq	32(%rax), %rsi
	leaq	-208(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, %rbx
	movq	%rax, -384(%rbp)
	call	_ZN2v88internal4ZoneC1EPNS0_19AccountingAllocatorEPKc@PLT
	movq	(%r14), %rax
	movq	%rbx, -240(%rbp)
	movq	$0, -232(%rbp)
	movq	-192(%rbp), %rbx
	movq	$0, -224(%rbp)
	movq	$0, -216(%rbp)
	movq	(%rax), %rax
	movq	(%rax), %r13
	movq	-184(%rbp), %rax
	subq	%rbx, %rax
	cmpq	$47, %rax
	jbe	.L2690
	leaq	48(%rbx), %rax
	movq	%rax, -192(%rbp)
.L2436:
	movq	-384(%rbp), %rax
	movq	$0, (%rbx)
	movl	$0, 8(%rbx)
	movl	%r13d, 12(%rbx)
	movq	%rax, 16(%rbx)
	movq	-224(%rbp), %r12
	movq	$0, 24(%rbx)
	movq	$0, 32(%rbx)
	movq	$0, 40(%rbx)
	cmpq	-216(%rbp), %r12
	je	.L2437
	movq	-376(%rbp), %rax
	movq	48(%rax), %rax
	movq	%rbx, 8(%r12)
	movq	$0, 16(%r12)
	movq	%rax, (%r12)
	addq	$32, -224(%rbp)
	movl	%r13d, 24(%r12)
	movb	$0, 28(%r12)
.L2438:
	movq	-376(%rbp), %rax
	movq	56(%rax), %rdx
	movq	48(%rax), %rsi
	leaq	8(%rax), %rcx
	leaq	-144(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -432(%rbp)
	call	_ZN2v88internal4wasm16BytecodeIteratorC1EPKhS4_PNS1_14BodyLocalDeclsE@PLT
	movq	-128(%rbp), %rax
	cmpq	-120(%rbp), %rax
	jnb	.L2446
	movq	$0, -448(%rbp)
	xorl	%ebx, %ebx
	movq	$0, -344(%rbp)
	movq	$0, -368(%rbp)
.L2576:
	movzbl	(%rax), %r14d
	movl	%r14d, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	testb	%al, %al
	je	.L2447
	movq	-128(%rbp), %rax
	movzwl	(%rax), %eax
	rolw	$8, %ax
	movzwl	%ax, %r14d
.L2447:
	movq	-224(%rbp), %rax
	xorl	%r13d, %r13d
	movzbl	-4(%rax), %r12d
	testb	%r12b, %r12b
	je	.L2691
.L2448:
	movq	-368(%rbp), %rcx
	cmpq	%rcx, -344(%rbp)
	je	.L2451
	movl	%r14d, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes16IsThrowingOpcodeENS1_10WasmOpcodeE@PLT
	testb	%al, %al
	je	.L2451
	testb	%r12b, %r12b
	je	.L2692
.L2453:
	addl	$1, %r13d
	cmpl	56(%r15), %r13d
	jbe	.L2451
	movl	%r13d, 56(%r15)
	.p2align 4,,10
	.p2align 3
.L2451:
	cmpl	$14, %r14d
	ja	.L2454
	leaq	.L2456(%rip), %rcx
	movl	%r14d, %eax
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm9SideTableC2EPNS0_4ZoneEPKNS1_10WasmModuleEPNS1_15InterpreterCodeE,"aG",@progbits,_ZN2v88internal4wasm9SideTableC5EPNS0_4ZoneEPKNS1_10WasmModuleEPNS1_15InterpreterCodeE,comdat
	.align 4
	.align 4
.L2456:
	.long	.L2454-.L2456
	.long	.L2454-.L2456
	.long	.L2465-.L2456
	.long	.L2465-.L2456
	.long	.L2464-.L2456
	.long	.L2463-.L2456
	.long	.L2462-.L2456
	.long	.L2461-.L2456
	.long	.L2454-.L2456
	.long	.L2454-.L2456
	.long	.L2460-.L2456
	.long	.L2459-.L2456
	.long	.L2457-.L2456
	.long	.L2457-.L2456
	.long	.L2455-.L2456
	.section	.text._ZN2v88internal4wasm9SideTableC2EPNS0_4ZoneEPKNS1_10WasmModuleEPNS1_15InterpreterCodeE,"axG",@progbits,_ZN2v88internal4wasm9SideTableC5EPNS0_4ZoneEPKNS1_10WasmModuleEPNS1_15InterpreterCodeE,comdat
	.p2align 4,,10
	.p2align 3
.L2457:
	movq	-128(%rbp), %rcx
	movzbl	1(%rcx), %eax
	movl	%eax, %edx
	andl	$127, %edx
	testb	%al, %al
	js	.L2693
.L2569:
	testb	%r12b, %r12b
	je	.L2694
	.p2align 4,,10
	.p2align 3
.L2454:
	movl	%r14d, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes19IsUnconditionalJumpENS1_10WasmOpcodeE@PLT
	testb	%al, %al
	je	.L2575
	movq	-224(%rbp), %rax
	movb	$1, -4(%rax)
.L2575:
	movq	-128(%rbp), %rdi
	movq	-120(%rbp), %rsi
	cmpq	%rsi, %rdi
	jb	.L2695
.L2446:
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, -144(%rbp)
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2579
	call	_ZdlPv@PLT
.L2579:
	movq	-384(%rbp), %rdi
	call	_ZN2v88internal4ZoneD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2696
	addq	$424, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2465:
	.cfi_restore_state
	movq	-128(%rbp), %rcx
	movq	-432(%rbp), %rdx
	leaq	-304(%rbp), %rdi
	leaq	_ZN2v88internal4wasmL16kAllWasmFeaturesE(%rip), %rsi
	call	_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE0EEC1ERKNS1_12WasmFeaturesEPS3_PKh
	movzbl	-300(%rbp), %eax
	cmpb	$10, %al
	je	.L2697
	xorl	%r13d, %r13d
	cmpl	$3, %r14d
	je	.L2468
	xorl	%r13d, %r13d
	testb	%al, %al
	setne	%r13b
.L2468:
	movq	-192(%rbp), %r12
	movq	-184(%rbp), %rax
	subq	%r12, %rax
	cmpq	$47, %rax
	jbe	.L2698
	leaq	48(%r12), %rax
	movq	%rax, -192(%rbp)
.L2470:
	movq	-384(%rbp), %rax
	movq	$0, (%r12)
	xorl	%r10d, %r10d
	movl	%ebx, 8(%r12)
	movl	%r13d, 12(%r12)
	movq	%rax, 16(%r12)
	movq	$0, 24(%r12)
	movq	$0, 32(%r12)
	movq	$0, 40(%r12)
	movzbl	-300(%rbp), %eax
	testb	%al, %al
	je	.L2471
	movl	$1, %r10d
	cmpb	$10, %al
	jne	.L2471
	movq	-288(%rbp), %rax
	movl	(%rax), %r10d
.L2471:
	movq	-128(%rbp), %xmm0
	movq	-224(%rbp), %r13
	cmpq	-216(%rbp), %r13
	je	.L2472
	movq	%r12, %xmm5
	movq	$0, 16(%r13)
	punpcklqdq	%xmm5, %xmm0
	movl	%r10d, 24(%r13)
	movups	%xmm0, 0(%r13)
	movq	-224(%rbp), %rax
	movb	$0, 28(%r13)
	leaq	32(%rax), %rsi
	movq	-232(%rbp), %rax
	movq	%rsi, %r13
	movq	%rsi, -224(%rbp)
	subq	%rax, %r13
	subq	$64, %r13
.L2473:
	movzbl	28(%rax,%r13), %eax
	movb	%al, -4(%rsi)
	cmpl	$3, %r14d
	jne	.L2454
	movq	-128(%rbp), %rax
	movq	%rax, (%r12)
	jmp	.L2454
	.p2align 4,,10
	.p2align 3
.L2464:
	leaq	-272(%rbp), %r8
	movq	-128(%rbp), %rcx
	xorl	%r13d, %r13d
	movq	-432(%rbp), %rdx
	leaq	_ZN2v88internal4wasmL16kAllWasmFeaturesE(%rip), %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE0EEC1ERKNS1_12WasmFeaturesEPS3_PKh
	movzbl	-268(%rbp), %eax
	testb	%al, %al
	setne	%r13b
	cmpb	$10, %al
	je	.L2699
.L2483:
	movq	-192(%rbp), %rax
	movq	-184(%rbp), %rdx
	subq	%rax, %rdx
	cmpq	$47, %rdx
	jbe	.L2700
	leaq	48(%rax), %rdx
	movq	%rdx, -192(%rbp)
.L2485:
	movq	-384(%rbp), %rsi
	movq	$0, (%rax)
	movl	%ebx, 8(%rax)
	movl	%r13d, 12(%rax)
	movq	%rsi, 16(%rax)
	movq	$0, 24(%rax)
	movq	-184(%rbp), %rdx
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	%rax, -320(%rbp)
	movq	-192(%rbp), %rax
	subq	%rax, %rdx
	cmpq	$47, %rdx
	jbe	.L2701
	leaq	48(%rax), %rdx
	movq	%rsi, %rcx
	movq	%rdx, -192(%rbp)
.L2487:
	movq	$0, (%rax)
	movl	%ebx, 8(%rax)
	movl	$0, 12(%rax)
	movq	%rcx, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movzbl	-268(%rbp), %edx
	movq	%rax, -312(%rbp)
	xorl	%eax, %eax
	testb	%dl, %dl
	je	.L2488
	movl	$1, %eax
	cmpb	$10, %dl
	jne	.L2488
	movq	-256(%rbp), %rax
	movl	(%rax), %eax
.L2488:
	movl	%eax, -324(%rbp)
	leaq	-304(%rbp), %r13
	movq	-128(%rbp), %rax
	leaq	-320(%rbp), %rdx
	leaq	-312(%rbp), %rcx
	leaq	-240(%rbp), %rdi
	movq	%r13, %rsi
	leaq	-324(%rbp), %r8
	movq	%rax, -304(%rbp)
	call	_ZNSt6vectorIZN2v88internal4wasm9SideTableC4EPNS1_4ZoneEPKNS2_10WasmModuleEPNS2_15InterpreterCodeEE7ControlNS1_13ZoneAllocatorISB_EEE12emplace_backIJPKhRPZNS3_C4ES5_S8_SA_E6CLabelSK_jEEEvDpOT_
	movq	-224(%rbp), %rax
	movzbl	-36(%rax), %edx
	movb	%dl, -4(%rax)
	testb	%r12b, %r12b
	jne	.L2454
	movq	-312(%rbp), %rax
	movq	%r13, %rsi
	movl	%ebx, -296(%rbp)
	leaq	16(%rax), %rdi
	movq	-128(%rbp), %rax
	movq	%rax, -304(%rbp)
	call	_ZNSt6vectorIZN2v88internal4wasm9SideTableC4EPNS1_4ZoneEPKNS2_10WasmModuleEPNS2_15InterpreterCodeEEN6CLabel3RefENS1_13ZoneAllocatorISC_EEE12emplace_backIJSC_EEEvDpOT_
	jmp	.L2454
	.p2align 4,,10
	.p2align 3
.L2462:
	leaq	-272(%rbp), %r8
	movq	-128(%rbp), %rcx
	xorl	%r12d, %r12d
	movq	-432(%rbp), %rdx
	leaq	_ZN2v88internal4wasmL16kAllWasmFeaturesE(%rip), %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE0EEC1ERKNS1_12WasmFeaturesEPS3_PKh
	movzbl	-268(%rbp), %eax
	testb	%al, %al
	setne	%r12b
	cmpb	$10, %al
	je	.L2702
.L2505:
	movq	-192(%rbp), %rax
	movq	-184(%rbp), %rdx
	subq	%rax, %rdx
	cmpq	$47, %rdx
	jbe	.L2703
	leaq	48(%rax), %rdx
	movq	%rdx, -192(%rbp)
.L2507:
	movq	-384(%rbp), %rsi
	movq	$0, (%rax)
	movl	%ebx, 8(%rax)
	movl	%r12d, 12(%rax)
	movq	%rsi, 16(%rax)
	movq	$0, 24(%rax)
	movq	-184(%rbp), %rdx
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	%rax, -320(%rbp)
	movq	-192(%rbp), %rax
	subq	%rax, %rdx
	cmpq	$47, %rdx
	jbe	.L2704
	leaq	48(%rax), %rdx
	movq	%rsi, %rcx
	movq	%rdx, -192(%rbp)
.L2509:
	movq	$0, (%rax)
	movl	%ebx, 8(%rax)
	movl	$1, 12(%rax)
	movq	%rcx, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movzbl	-268(%rbp), %edx
	movq	%rax, -312(%rbp)
	xorl	%eax, %eax
	testb	%dl, %dl
	je	.L2510
	movl	$1, %eax
	cmpb	$10, %dl
	jne	.L2510
	movq	-256(%rbp), %rax
	movl	(%rax), %eax
.L2510:
	movl	%eax, -324(%rbp)
	movq	-128(%rbp), %rax
	leaq	-304(%rbp), %rsi
	leaq	-312(%rbp), %rcx
	leaq	-320(%rbp), %rdx
	leaq	-240(%rbp), %rdi
	leaq	-324(%rbp), %r8
	movq	%rax, -304(%rbp)
	call	_ZNSt6vectorIZN2v88internal4wasm9SideTableC4EPNS1_4ZoneEPKNS2_10WasmModuleEPNS2_15InterpreterCodeEE7ControlNS1_13ZoneAllocatorISB_EEE12emplace_backIJPKhRPZNS3_C4ES5_S8_SA_E6CLabelSK_jEEEvDpOT_
	movq	-224(%rbp), %r12
	movq	-344(%rbp), %rax
	subq	-232(%rbp), %r12
	movq	-448(%rbp), %rsi
	sarq	$5, %r12
	subq	$1, %r12
	cmpq	%rsi, %rax
	je	.L2511
	movq	%r12, (%rax)
	addq	$8, %rax
	movq	%rax, -344(%rbp)
.L2512:
	movq	-224(%rbp), %rax
	movzbl	-36(%rax), %edx
	movb	%dl, -4(%rax)
	jmp	.L2454
	.p2align 4,,10
	.p2align 3
.L2455:
	movq	-128(%rbp), %rax
	movl	$2, %r13d
	movzbl	1(%rax), %edx
	movl	%edx, %r9d
	andl	$127, %r9d
	testb	%dl, %dl
	js	.L2705
.L2571:
	testb	%r12b, %r12b
	jne	.L2454
	addq	%rax, %r13
	xorl	%r12d, %r12d
	leaq	-272(%rbp), %r8
	movl	%r14d, -360(%rbp)
	movq	%r15, -392(%rbp)
	movl	%ebx, %r14d
	movq	%r13, %r15
	movl	%r12d, %ebx
	movl	%r9d, %r13d
	movq	%r8, %r12
.L2574:
	movq	$1, -352(%rbp)
	movzbl	(%r15), %edx
	movl	%edx, %esi
	andl	$127, %esi
	testb	%dl, %dl
	js	.L2706
.L2573:
	movq	-232(%rbp), %rcx
	movq	-224(%rbp), %rdx
	subq	%rcx, %rdx
	sarq	$5, %rdx
	subq	$1, %rdx
	subq	%rsi, %rdx
	movq	%r12, %rsi
	salq	$5, %rdx
	movq	8(%rcx,%rdx), %rdi
	movl	%ebx, %edx
	addl	$1, %ebx
	movl	%r14d, -264(%rbp)
	addq	%rdx, %rax
	addq	$16, %rdi
	movq	%rax, -272(%rbp)
	call	_ZNSt6vectorIZN2v88internal4wasm9SideTableC4EPNS1_4ZoneEPKNS2_10WasmModuleEPNS2_15InterpreterCodeEEN6CLabel3RefENS1_13ZoneAllocatorISC_EEE12emplace_backIJSC_EEEvDpOT_
	cmpl	%ebx, %r13d
	jb	.L2678
	movq	-128(%rbp), %rax
	addq	-352(%rbp), %r15
	jmp	.L2574
	.p2align 4,,10
	.p2align 3
.L2463:
	movq	-224(%rbp), %rcx
	movzbl	-36(%rcx), %eax
	movq	%rcx, -440(%rbp)
	movb	%al, -4(%rcx)
	movq	-224(%rbp), %rax
	cmpb	$0, -36(%rax)
	je	.L2707
.L2490:
	movq	-440(%rbp), %rdi
	movq	-128(%rbp), %rax
	movq	-16(%rdi), %rdx
	addq	$1, %rax
	movq	%rax, (%rdx)
	movq	-376(%rbp), %rax
	movq	-16(%rdi), %r13
	movq	48(%rax), %rax
	movq	24(%r13), %r12
	movq	%rax, -392(%rbp)
	movq	32(%r13), %rax
	movq	%rax, -400(%rbp)
	cmpq	%rax, %r12
	je	.L2503
	movl	%r14d, -452(%rbp)
	movq	%r15, %rbx
	movq	-424(%rbp), %r15
	.p2align 4,,10
	.p2align 3
.L2502:
	movq	(%r12), %rax
	movq	0(%r13), %rsi
	movq	%r15, %r14
	movl	8(%r12), %r9d
	subl	8(%r13), %r9d
	movq	%rax, %rdx
	subq	%rax, %rsi
	movq	24(%rbx), %rax
	subq	-392(%rbp), %rdx
	movq	%rsi, -352(%rbp)
	testq	%rax, %rax
	jne	.L2493
	jmp	.L2492
	.p2align 4,,10
	.p2align 3
.L2708:
	movq	%rax, %r14
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L2494
.L2493:
	cmpq	32(%rax), %rdx
	jbe	.L2708
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L2493
.L2494:
	cmpq	%r14, %r15
	je	.L2492
	cmpq	32(%r14), %rdx
	jnb	.L2497
.L2492:
	movq	(%rbx), %rdi
	movq	16(%rdi), %r8
	movq	24(%rdi), %rax
	subq	%r8, %rax
	cmpq	$55, %rax
	jbe	.L2709
	leaq	56(%r8), %rax
	movq	%rax, 16(%rdi)
.L2499:
	movq	%rdx, 32(%r8)
	movq	%r14, %rsi
	leaq	32(%r8), %rdx
	movq	%rbx, %rdi
	movq	$0, 40(%r8)
	movl	$0, 48(%r8)
	movl	%r9d, -360(%rbp)
	movq	%r8, -408(%rbp)
	call	_ZNSt8_Rb_treeImSt4pairIKmN2v88internal4wasm20ControlTransferEntryEESt10_Select1stIS6_ESt4lessImENS3_13ZoneAllocatorIS6_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS6_ERS1_
	movl	-360(%rbp), %r9d
	testq	%rdx, %rdx
	movq	%rax, %r14
	je	.L2497
	cmpq	%rdx, %r15
	movq	-408(%rbp), %r8
	je	.L2594
	testq	%rax, %rax
	je	.L2710
.L2594:
	movl	$1, %edi
.L2501:
	movq	%r8, %rsi
	movq	%r15, %rcx
	movl	%r9d, -408(%rbp)
	movq	%r8, -360(%rbp)
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 48(%rbx)
	movq	-360(%rbp), %r8
	movl	-408(%rbp), %r9d
	movq	%r8, %r14
.L2497:
	movl	-352(%rbp), %eax
	movl	%r9d, 44(%r14)
	addq	$16, %r12
	movl	%eax, 40(%r14)
	movl	12(%r13), %eax
	movl	%eax, 48(%r14)
	cmpq	%r12, -400(%rbp)
	jne	.L2502
	movl	-452(%rbp), %r14d
	movq	%rbx, %r15
.L2503:
	movq	-440(%rbp), %rax
	movq	$0, -16(%rax)
	movq	-24(%rax), %rax
	movl	8(%rax), %ebx
	jmp	.L2454
	.p2align 4,,10
	.p2align 3
.L2460:
	movq	-128(%rbp), %rdx
	movl	$1, %eax
	movzbl	1(%rdx), %ecx
	movl	%ecx, %esi
	andl	$127, %esi
	testb	%cl, %cl
	js	.L2711
.L2536:
	addq	%rdx, %rax
	movzbl	1(%rax), %ecx
	movl	%ecx, %edi
	andl	$127, %edi
	testb	%cl, %cl
	js	.L2712
.L2537:
	testb	%r12b, %r12b
	jne	.L2454
	movq	-416(%rbp), %rax
	movq	-232(%rbp), %rcx
	leaq	-272(%rbp), %r8
	movq	256(%rax), %rax
	movq	(%rax,%rdi,8), %rax
	movq	8(%rax), %rdi
	movq	-224(%rbp), %rax
	subq	%rcx, %rax
	sarq	$5, %rax
	subq	$1, %rax
	subq	%rsi, %rax
	movq	%r8, %rsi
	salq	$5, %rax
	movq	8(%rcx,%rax), %r9
	leal	-1(%rbx,%rdi), %eax
	movq	%rdx, -272(%rbp)
	movl	%eax, -264(%rbp)
	addq	$16, %r9
	movq	%r9, %rdi
	call	_ZNSt6vectorIZN2v88internal4wasm9SideTableC4EPNS1_4ZoneEPKNS2_10WasmModuleEPNS2_15InterpreterCodeEEN6CLabel3RefENS1_13ZoneAllocatorISC_EEE12emplace_backIJSC_EEEvDpOT_
	jmp	.L2454
	.p2align 4,,10
	.p2align 3
.L2459:
	movq	-224(%rbp), %rax
	movq	-24(%rax), %r12
	movq	%rax, -440(%rbp)
	cmpq	$0, (%r12)
	je	.L2713
.L2539:
	movq	-376(%rbp), %rax
	movq	24(%r12), %r13
	movq	48(%rax), %rax
	movq	%rax, -360(%rbp)
	movq	32(%r12), %rax
	movq	%rax, -400(%rbp)
	cmpq	%rax, %r13
	je	.L2555
	movl	%r14d, -452(%rbp)
	movq	-424(%rbp), %r14
	movq	%r15, %rbx
	movq	%r12, %r15
	.p2align 4,,10
	.p2align 3
.L2554:
	movq	0(%r13), %rax
	movq	(%r15), %rsi
	movq	%r14, %r12
	movl	8(%r13), %r9d
	subl	8(%r15), %r9d
	movq	%rax, %rdx
	subq	%rax, %rsi
	movq	24(%rbx), %rax
	subq	-360(%rbp), %rdx
	movq	%rsi, -352(%rbp)
	testq	%rax, %rax
	jne	.L2545
	jmp	.L2544
	.p2align 4,,10
	.p2align 3
.L2714:
	movq	%rax, %r12
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L2546
.L2545:
	cmpq	32(%rax), %rdx
	jbe	.L2714
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L2545
.L2546:
	cmpq	%r12, %r14
	je	.L2544
	cmpq	32(%r12), %rdx
	jnb	.L2549
.L2544:
	movq	(%rbx), %rdi
	movq	16(%rdi), %r8
	movq	24(%rdi), %rax
	subq	%r8, %rax
	cmpq	$55, %rax
	jbe	.L2715
	leaq	56(%r8), %rax
	movq	%rax, 16(%rdi)
.L2551:
	movq	%rdx, 32(%r8)
	movq	%r12, %rsi
	leaq	32(%r8), %rdx
	movq	%rbx, %rdi
	movq	$0, 40(%r8)
	movl	$0, 48(%r8)
	movl	%r9d, -392(%rbp)
	movq	%r8, -408(%rbp)
	call	_ZNSt8_Rb_treeImSt4pairIKmN2v88internal4wasm20ControlTransferEntryEESt10_Select1stIS6_ESt4lessImENS3_13ZoneAllocatorIS6_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS6_ERS1_
	movl	-392(%rbp), %r9d
	testq	%rdx, %rdx
	movq	%rax, %r12
	je	.L2549
	testq	%rax, %rax
	movq	-408(%rbp), %r8
	jne	.L2609
	cmpq	%rdx, %r14
	jne	.L2716
.L2609:
	movl	$1, %edi
.L2553:
	movq	%r8, %rsi
	movq	%r14, %rcx
	movl	%r9d, -408(%rbp)
	movq	%r8, -392(%rbp)
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 48(%rbx)
	movq	-392(%rbp), %r8
	movl	-408(%rbp), %r9d
	movq	%r8, %r12
.L2549:
	movl	-352(%rbp), %eax
	movl	%r9d, 44(%r12)
	addq	$16, %r13
	movl	%eax, 40(%r12)
	movl	12(%r15), %eax
	movl	%eax, 48(%r12)
	cmpq	%r13, -400(%rbp)
	jne	.L2554
	movl	-452(%rbp), %r14d
	movq	%rbx, %r15
.L2555:
	movq	-440(%rbp), %rax
	movq	-16(%rax), %rbx
	testq	%rbx, %rbx
	je	.L2543
	movq	32(%rbx), %rax
	movq	24(%rbx), %r8
	movq	%rax, -400(%rbp)
	cmpq	%rax, %r8
	je	.L2543
	movl	%r14d, -452(%rbp)
	movq	%r15, %r14
	movq	%rbx, %r15
	movq	%r8, %r13
	movq	-424(%rbp), %rbx
	.p2align 4,,10
	.p2align 3
.L2566:
	movq	0(%r13), %rax
	movq	(%r15), %rsi
	movq	%rbx, %r12
	movl	8(%r13), %r9d
	subl	8(%r15), %r9d
	movq	%rax, %rdx
	subq	%rax, %rsi
	movq	24(%r14), %rax
	subq	-360(%rbp), %rdx
	movq	%rsi, -352(%rbp)
	testq	%rax, %rax
	jne	.L2557
	jmp	.L2556
	.p2align 4,,10
	.p2align 3
.L2717:
	movq	%rax, %r12
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L2558
.L2557:
	cmpq	32(%rax), %rdx
	jbe	.L2717
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L2557
.L2558:
	cmpq	%r12, %rbx
	je	.L2556
	cmpq	32(%r12), %rdx
	jnb	.L2561
.L2556:
	movq	(%r14), %rdi
	movq	16(%rdi), %r8
	movq	24(%rdi), %rax
	subq	%r8, %rax
	cmpq	$55, %rax
	jbe	.L2718
	leaq	56(%r8), %rax
	movq	%rax, 16(%rdi)
.L2563:
	movq	%rdx, 32(%r8)
	movq	%r12, %rsi
	leaq	32(%r8), %rdx
	movq	%r14, %rdi
	movq	$0, 40(%r8)
	movl	$0, 48(%r8)
	movl	%r9d, -392(%rbp)
	movq	%r8, -408(%rbp)
	call	_ZNSt8_Rb_treeImSt4pairIKmN2v88internal4wasm20ControlTransferEntryEESt10_Select1stIS6_ESt4lessImENS3_13ZoneAllocatorIS6_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS6_ERS1_
	movl	-392(%rbp), %r9d
	testq	%rdx, %rdx
	movq	%rax, %r12
	je	.L2561
	cmpq	%rdx, %rbx
	movq	-408(%rbp), %r8
	je	.L2611
	testq	%rax, %rax
	je	.L2719
.L2611:
	movl	$1, %edi
.L2565:
	movq	%r8, %rsi
	movq	%rbx, %rcx
	movl	%r9d, -408(%rbp)
	movq	%r8, -392(%rbp)
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 48(%r14)
	movq	-392(%rbp), %r8
	movl	-408(%rbp), %r9d
	movq	%r8, %r12
.L2561:
	movl	-352(%rbp), %eax
	movl	%r9d, 44(%r12)
	addq	$16, %r13
	movl	%eax, 40(%r12)
	movl	12(%r15), %eax
	movl	%eax, 48(%r12)
	cmpq	%r13, -400(%rbp)
	jne	.L2566
	movq	%r14, %r15
	movl	-452(%rbp), %r14d
.L2543:
	movq	-440(%rbp), %rdi
	movq	-24(%rdi), %rax
	movl	-8(%rdi), %ebx
	addl	8(%rax), %ebx
	subq	$32, -224(%rbp)
	jmp	.L2454
	.p2align 4,,10
	.p2align 3
.L2461:
	movq	-224(%rbp), %rax
	subq	$8, -344(%rbp)
	movq	%rax, %rcx
	movq	%rax, -440(%rbp)
	movzbl	-36(%rax), %eax
	movb	%al, -4(%rcx)
	movq	-224(%rbp), %rax
	cmpb	$0, -36(%rax)
	je	.L2720
.L2522:
	movq	-440(%rbp), %rcx
	movq	-128(%rbp), %rax
	movq	-16(%rcx), %rdx
	addq	$1, %rax
	movq	%rax, (%rdx)
	movq	-376(%rbp), %rax
	movq	-16(%rcx), %r13
	movq	48(%rax), %rax
	movq	24(%r13), %r12
	movq	%rax, -392(%rbp)
	movq	32(%r13), %rax
	movq	%rax, -400(%rbp)
	cmpq	%rax, %r12
	je	.L2535
	movl	%r14d, -452(%rbp)
	movq	%r15, %rbx
	movq	-424(%rbp), %r15
	.p2align 4,,10
	.p2align 3
.L2534:
	movq	(%r12), %rax
	movq	0(%r13), %rcx
	movq	%r15, %r14
	movl	8(%r12), %r9d
	subl	8(%r13), %r9d
	movq	%rax, %rdx
	subq	%rax, %rcx
	movq	24(%rbx), %rax
	subq	-392(%rbp), %rdx
	movq	%rcx, -352(%rbp)
	testq	%rax, %rax
	jne	.L2525
	jmp	.L2524
	.p2align 4,,10
	.p2align 3
.L2721:
	movq	%rax, %r14
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L2526
.L2525:
	cmpq	32(%rax), %rdx
	jbe	.L2721
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L2525
.L2526:
	cmpq	%r14, %r15
	je	.L2524
	cmpq	32(%r14), %rdx
	jnb	.L2529
.L2524:
	movq	(%rbx), %rdi
	movq	16(%rdi), %r8
	movq	24(%rdi), %rax
	subq	%r8, %rax
	cmpq	$55, %rax
	jbe	.L2722
	leaq	56(%r8), %rax
	movq	%rax, 16(%rdi)
.L2531:
	movq	%rdx, 32(%r8)
	movq	%r14, %rsi
	leaq	32(%r8), %rdx
	movq	%rbx, %rdi
	movq	$0, 40(%r8)
	movl	$0, 48(%r8)
	movl	%r9d, -360(%rbp)
	movq	%r8, -408(%rbp)
	call	_ZNSt8_Rb_treeImSt4pairIKmN2v88internal4wasm20ControlTransferEntryEESt10_Select1stIS6_ESt4lessImENS3_13ZoneAllocatorIS6_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS6_ERS1_
	movl	-360(%rbp), %r9d
	testq	%rdx, %rdx
	movq	%rax, %r14
	je	.L2529
	cmpq	%rdx, %r15
	movq	-408(%rbp), %r8
	je	.L2603
	testq	%rax, %rax
	je	.L2723
.L2603:
	movl	$1, %edi
.L2533:
	movq	%r8, %rsi
	movq	%r15, %rcx
	movl	%r9d, -408(%rbp)
	movq	%r8, -360(%rbp)
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 48(%rbx)
	movq	-360(%rbp), %r8
	movl	-408(%rbp), %r9d
	movq	%r8, %r14
.L2529:
	movl	-352(%rbp), %eax
	movl	%r9d, 44(%r14)
	addq	$16, %r12
	movl	%eax, 40(%r14)
	movl	12(%r13), %eax
	movl	%eax, 48(%r14)
	cmpq	%r12, -400(%rbp)
	jne	.L2534
	movl	-452(%rbp), %r14d
	movq	%rbx, %r15
.L2535:
	movq	-440(%rbp), %rax
	movq	$0, -16(%rax)
	movq	-24(%rax), %rax
	movl	8(%rax), %ebx
	addl	$1, %ebx
	jmp	.L2454
	.p2align 4,,10
	.p2align 3
.L2691:
	movq	-376(%rbp), %rax
	movq	-120(%rbp), %rcx
	movq	-128(%rbp), %rdx
	movq	-416(%rbp), %rdi
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal4wasm11StackEffectEPKNS1_10WasmModuleEPNS0_9SignatureINS1_9ValueTypeEEEPKhSA_@PLT
	subl	%eax, %ebx
	shrq	$32, %rax
	movl	%ebx, %r13d
	leal	(%rbx,%rax), %ebx
	cmpl	%ebx, 56(%r15)
	jnb	.L2448
	movl	%ebx, 56(%r15)
	jmp	.L2448
	.p2align 4,,10
	.p2align 3
.L2695:
	call	_ZN2v88internal4wasm12OpcodeLengthEPKhS3_@PLT
	movq	-120(%rbp), %rdx
	movl	%eax, %eax
	addq	-128(%rbp), %rax
	movq	%rax, -128(%rbp)
	cmpq	%rdx, %rax
	jb	.L2576
	movq	%rdx, -128(%rbp)
	jmp	.L2446
	.p2align 4,,10
	.p2align 3
.L2692:
	movq	-344(%rbp), %rax
	leaq	-272(%rbp), %r8
	movq	%r8, %rsi
	movq	-8(%rax), %rax
	movq	%rax, -352(%rbp)
	salq	$5, %rax
	addq	-232(%rbp), %rax
	movq	16(%rax), %rdi
	movq	-128(%rbp), %rax
	movl	%r13d, -264(%rbp)
	addq	$16, %rdi
	movq	%rax, -272(%rbp)
	call	_ZNSt6vectorIZN2v88internal4wasm9SideTableC4EPNS1_4ZoneEPKNS2_10WasmModuleEPNS2_15InterpreterCodeEEN6CLabel3RefENS1_13ZoneAllocatorISC_EEE12emplace_backIJSC_EEEvDpOT_
	jmp	.L2453
	.p2align 4,,10
	.p2align 3
.L2702:
	movq	-416(%rbp), %rax
	movl	-264(%rbp), %edx
	movq	88(%rax), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rax, -256(%rbp)
	movl	(%rax), %r12d
	jmp	.L2505
	.p2align 4,,10
	.p2align 3
.L2699:
	movq	-416(%rbp), %rax
	movl	-264(%rbp), %edx
	movq	88(%rax), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rax, -256(%rbp)
	movl	(%rax), %r13d
	jmp	.L2483
	.p2align 4,,10
	.p2align 3
.L2697:
	movq	-416(%rbp), %rax
	movl	-296(%rbp), %edx
	movq	88(%rax), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rax, -288(%rbp)
	cmpl	$3, %r14d
	jne	.L2467
	movl	8(%rax), %r13d
	jmp	.L2468
	.p2align 4,,10
	.p2align 3
.L2707:
	movq	-24(%rcx), %rax
	leaq	-272(%rbp), %r8
	movl	%ebx, -264(%rbp)
	movq	%r8, %rsi
	movq	%rax, -352(%rbp)
	leaq	16(%rax), %rdi
	movq	-128(%rbp), %rax
	movq	%rax, -272(%rbp)
	call	_ZNSt6vectorIZN2v88internal4wasm9SideTableC4EPNS1_4ZoneEPKNS2_10WasmModuleEPNS2_15InterpreterCodeEEN6CLabel3RefENS1_13ZoneAllocatorISC_EEE12emplace_backIJSC_EEEvDpOT_
	jmp	.L2490
	.p2align 4,,10
	.p2align 3
.L2720:
	movq	-24(%rcx), %rax
	leaq	-272(%rbp), %r8
	movl	%ebx, -264(%rbp)
	movq	%r8, %rsi
	movq	%rax, -352(%rbp)
	leaq	16(%rax), %rdi
	movq	-128(%rbp), %rax
	movq	%rax, -272(%rbp)
	call	_ZNSt6vectorIZN2v88internal4wasm9SideTableC4EPNS1_4ZoneEPKNS2_10WasmModuleEPNS2_15InterpreterCodeEEN6CLabel3RefENS1_13ZoneAllocatorISC_EEE12emplace_backIJSC_EEEvDpOT_
	jmp	.L2522
	.p2align 4,,10
	.p2align 3
.L2694:
	movq	-232(%rbp), %rsi
	movq	-224(%rbp), %rax
	leaq	-272(%rbp), %r8
	subq	%rsi, %rax
	sarq	$5, %rax
	subq	$1, %rax
	subq	%rdx, %rax
	salq	$5, %rax
	movq	8(%rsi,%rax), %rdi
	movq	%r8, %rsi
	movq	%rcx, -272(%rbp)
	movl	%ebx, -264(%rbp)
	addq	$16, %rdi
	call	_ZNSt6vectorIZN2v88internal4wasm9SideTableC4EPNS1_4ZoneEPKNS2_10WasmModuleEPNS2_15InterpreterCodeEEN6CLabel3RefENS1_13ZoneAllocatorISC_EEE12emplace_backIJSC_EEEvDpOT_
	jmp	.L2454
	.p2align 4,,10
	.p2align 3
.L2716:
	xorl	%edi, %edi
	movq	32(%rdx), %rax
	cmpq	%rax, 32(%r8)
	setb	%dil
	jmp	.L2553
	.p2align 4,,10
	.p2align 3
.L2710:
	xorl	%edi, %edi
	movq	32(%rdx), %rax
	cmpq	%rax, 32(%r8)
	setb	%dil
	jmp	.L2501
	.p2align 4,,10
	.p2align 3
.L2723:
	xorl	%edi, %edi
	movq	32(%rdx), %rax
	cmpq	%rax, 32(%r8)
	setb	%dil
	jmp	.L2533
	.p2align 4,,10
	.p2align 3
.L2693:
	movzbl	2(%rcx), %esi
	movl	%esi, %eax
	sall	$7, %eax
	andl	$16256, %eax
	orl	%eax, %edx
	testb	%sil, %sil
	jns	.L2569
	movzbl	3(%rcx), %esi
	movl	%esi, %eax
	sall	$14, %eax
	andl	$2080768, %eax
	orl	%eax, %edx
	testb	%sil, %sil
	jns	.L2569
	movzbl	4(%rcx), %esi
	movl	%esi, %eax
	sall	$21, %eax
	andl	$266338304, %eax
	orl	%eax, %edx
	testb	%sil, %sil
	jns	.L2569
	movzbl	5(%rcx), %eax
	sall	$28, %eax
	orl	%eax, %edx
	jmp	.L2569
	.p2align 4,,10
	.p2align 3
.L2722:
	movl	$56, %esi
	movl	%r9d, -408(%rbp)
	movq	%rdx, -360(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-360(%rbp), %rdx
	movl	-408(%rbp), %r9d
	movq	%rax, %r8
	jmp	.L2531
	.p2align 4,,10
	.p2align 3
.L2715:
	movl	$56, %esi
	movl	%r9d, -408(%rbp)
	movq	%rdx, -392(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-392(%rbp), %rdx
	movl	-408(%rbp), %r9d
	movq	%rax, %r8
	jmp	.L2551
	.p2align 4,,10
	.p2align 3
.L2709:
	movl	$56, %esi
	movl	%r9d, -408(%rbp)
	movq	%rdx, -360(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-360(%rbp), %rdx
	movl	-408(%rbp), %r9d
	movq	%rax, %r8
	jmp	.L2499
	.p2align 4,,10
	.p2align 3
.L2713:
	movq	-16(%rax), %rdx
	movq	%rax, %rcx
	movq	-128(%rbp), %rax
	testq	%rdx, %rdx
	je	.L2540
	movq	%rax, (%rdx)
	movq	-24(%rcx), %r12
	movq	-128(%rbp), %rax
.L2540:
	addq	$1, %rax
	movq	%rax, (%r12)
	movq	-440(%rbp), %rax
	movq	-24(%rax), %r12
	jmp	.L2539
	.p2align 4,,10
	.p2align 3
.L2719:
	xorl	%edi, %edi
	movq	32(%rdx), %rax
	cmpq	%rax, 32(%r8)
	setb	%dil
	jmp	.L2565
	.p2align 4,,10
	.p2align 3
.L2467:
	movl	(%rax), %r13d
	jmp	.L2468
	.p2align 4,,10
	.p2align 3
.L2712:
	movzbl	2(%rax), %r8d
	movl	%r8d, %ecx
	sall	$7, %ecx
	andl	$16256, %ecx
	orl	%ecx, %edi
	testb	%r8b, %r8b
	jns	.L2537
	movzbl	3(%rax), %r8d
	movl	%r8d, %ecx
	sall	$14, %ecx
	andl	$2080768, %ecx
	orl	%ecx, %edi
	testb	%r8b, %r8b
	jns	.L2537
	movzbl	4(%rax), %r8d
	movl	%r8d, %ecx
	sall	$21, %ecx
	andl	$266338304, %ecx
	orl	%ecx, %edi
	testb	%r8b, %r8b
	jns	.L2537
	movzbl	5(%rax), %eax
	sall	$28, %eax
	orl	%eax, %edi
	jmp	.L2537
	.p2align 4,,10
	.p2align 3
.L2711:
	movzbl	2(%rdx), %ecx
	movl	%ecx, %eax
	sall	$7, %eax
	andl	$16256, %eax
	orl	%eax, %esi
	movl	$2, %eax
	testb	%cl, %cl
	jns	.L2536
	movzbl	3(%rdx), %ecx
	movl	%ecx, %eax
	sall	$14, %eax
	andl	$2080768, %eax
	orl	%eax, %esi
	movl	$3, %eax
	testb	%cl, %cl
	jns	.L2536
	movzbl	4(%rdx), %ecx
	movl	%ecx, %eax
	sall	$21, %eax
	andl	$266338304, %eax
	orl	%eax, %esi
	movl	$4, %eax
	testb	%cl, %cl
	jns	.L2536
	movzbl	5(%rdx), %eax
	sall	$28, %eax
	orl	%eax, %esi
	movl	$5, %eax
	jmp	.L2536
	.p2align 4,,10
	.p2align 3
.L2705:
	movzbl	2(%rax), %ecx
	movl	$3, %r13d
	movl	%ecx, %edx
	sall	$7, %edx
	andl	$16256, %edx
	orl	%edx, %r9d
	testb	%cl, %cl
	jns	.L2571
	movzbl	3(%rax), %ecx
	movl	$4, %r13d
	movl	%ecx, %edx
	sall	$14, %edx
	andl	$2080768, %edx
	orl	%edx, %r9d
	testb	%cl, %cl
	jns	.L2571
	movzbl	4(%rax), %ecx
	movl	$5, %r13d
	movl	%ecx, %edx
	sall	$21, %edx
	andl	$266338304, %edx
	orl	%edx, %r9d
	testb	%cl, %cl
	jns	.L2571
	movzbl	5(%rax), %edx
	movl	$6, %r13d
	sall	$28, %edx
	orl	%edx, %r9d
	jmp	.L2571
	.p2align 4,,10
	.p2align 3
.L2718:
	movl	$56, %esi
	movl	%r9d, -408(%rbp)
	movq	%rdx, -392(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-392(%rbp), %rdx
	movl	-408(%rbp), %r9d
	movq	%rax, %r8
	jmp	.L2563
	.p2align 4,,10
	.p2align 3
.L2437:
	movq	-232(%rbp), %rdx
	movq	%r12, %r14
	subq	%rdx, %r14
	movq	%r14, %rax
	sarq	$5, %rax
	cmpq	$67108863, %rax
	je	.L2474
	testq	%rax, %rax
	je	.L2581
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L2724
	movl	$2147483616, %esi
	movl	$2147483616, %r8d
.L2440:
	movq	-240(%rbp), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	%rsi, %rcx
	jb	.L2725
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L2443:
	leaq	(%rax,%r8), %rdi
	leaq	32(%rax), %rcx
.L2441:
	movq	-376(%rbp), %rsi
	addq	%rax, %r14
	movq	48(%rsi), %rsi
	movq	%rbx, 8(%r14)
	movq	$0, 16(%r14)
	movq	%rsi, (%r14)
	movl	%r13d, 24(%r14)
	movb	$0, 28(%r14)
	cmpq	%rdx, %r12
	je	.L2444
	movq	%rdx, %rcx
	movq	%rax, %rsi
	.p2align 4,,10
	.p2align 3
.L2445:
	movdqu	(%rcx), %xmm1
	addq	$32, %rcx
	addq	$32, %rsi
	movups	%xmm1, -32(%rsi)
	movdqu	-16(%rcx), %xmm2
	movups	%xmm2, -16(%rsi)
	cmpq	%rcx, %r12
	jne	.L2445
	subq	%rdx, %r12
	leaq	32(%rax,%r12), %rcx
.L2444:
	movq	%rax, %xmm0
	movq	%rcx, %xmm7
	movq	%rdi, -216(%rbp)
	punpcklqdq	%xmm7, %xmm0
	movups	%xmm0, -232(%rbp)
	jmp	.L2438
	.p2align 4,,10
	.p2align 3
.L2511:
	movq	%rax, %r13
	subq	-368(%rbp), %r13
	movq	%r13, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L2474
	testq	%rax, %rax
	je	.L2597
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L2726
	movl	$2147483640, %esi
	movl	$2147483640, %ecx
.L2513:
	movq	-464(%rbp), %rax
	movq	16(%rax), %rdx
	movq	24(%rax), %rax
	movq	%rax, -352(%rbp)
	subq	%rdx, %rax
	cmpq	%rsi, %rax
	jb	.L2727
	movq	-464(%rbp), %rax
	addq	%rdx, %rsi
	movq	%rsi, 16(%rax)
.L2516:
	leaq	(%rdx,%rcx), %rax
	movq	%rax, -448(%rbp)
	leaq	8(%rdx), %rax
	jmp	.L2514
.L2726:
	testq	%rdx, %rdx
	jne	.L2728
	movq	$0, -448(%rbp)
	movl	$8, %eax
	xorl	%edx, %edx
.L2514:
	movq	-344(%rbp), %rsi
	movq	-368(%rbp), %r11
	movq	%r12, (%rdx,%r13)
	cmpq	%r11, %rsi
	je	.L2600
	movq	%rsi, %rdi
	leaq	15(%rdx), %rax
	subq	$8, %rdi
	subq	%r11, %rax
	subq	%r11, %rdi
	movq	%rdi, %rcx
	shrq	$3, %rcx
	cmpq	$30, %rax
	jbe	.L2601
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rcx
	je	.L2601
	leaq	1(%rcx), %r8
	xorl	%eax, %eax
	movq	%r11, %rcx
	movq	%r8, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L2519:
	movdqu	(%rcx,%rax), %xmm6
	movups	%xmm6, (%rdx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rsi
	jne	.L2519
	movq	%r8, %rsi
	movq	-368(%rbp), %rax
	andq	$-2, %rsi
	leaq	0(,%rsi,8), %rcx
	addq	%rcx, %rax
	addq	%rdx, %rcx
	cmpq	%r8, %rsi
	je	.L2521
	movq	(%rax), %rax
	movq	%rax, (%rcx)
.L2521:
	leaq	16(%rdx,%rdi), %rax
	movq	%rax, -344(%rbp)
.L2517:
	movq	%rdx, -368(%rbp)
	jmp	.L2512
	.p2align 4,,10
	.p2align 3
.L2472:
	movq	-232(%rbp), %rcx
	movq	%r13, %rdx
	subq	%rcx, %rdx
	movq	%rdx, %rax
	sarq	$5, %rax
	cmpq	$67108863, %rax
	je	.L2474
	testq	%rax, %rax
	je	.L2587
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L2729
	movl	$2147483616, %esi
	movl	$2147483616, %r9d
.L2475:
	movq	-240(%rbp), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %r8
	subq	%rax, %r8
	cmpq	%rsi, %r8
	jb	.L2730
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L2478:
	addq	%rax, %r9
	leaq	32(%rax), %rsi
	jmp	.L2476
.L2729:
	testq	%rsi, %rsi
	jne	.L2731
	movl	$32, %esi
	xorl	%r9d, %r9d
	xorl	%eax, %eax
.L2476:
	movq	%r12, %xmm7
	addq	%rax, %rdx
	punpcklqdq	%xmm7, %xmm0
	movq	$0, 16(%rdx)
	movl	%r10d, 24(%rdx)
	movb	$0, 28(%rdx)
	movups	%xmm0, (%rdx)
	cmpq	%rcx, %r13
	je	.L2590
	movq	%rcx, %rdx
	movq	%rax, %rsi
	.p2align 4,,10
	.p2align 3
.L2480:
	movdqu	(%rdx), %xmm3
	addq	$32, %rdx
	addq	$32, %rsi
	movups	%xmm3, -32(%rsi)
	movdqu	-16(%rdx), %xmm4
	movups	%xmm4, -16(%rsi)
	cmpq	%rdx, %r13
	jne	.L2480
	subq	$32, %r13
	subq	%rcx, %r13
	leaq	64(%rax,%r13), %rsi
.L2479:
	movq	%rax, %xmm0
	movq	%rsi, %xmm7
	movq	%r9, -216(%rbp)
	punpcklqdq	%xmm7, %xmm0
	movups	%xmm0, -232(%rbp)
	jmp	.L2473
.L2724:
	testq	%rcx, %rcx
	jne	.L2732
	movl	$32, %ecx
	xorl	%edi, %edi
	xorl	%eax, %eax
	jmp	.L2441
	.p2align 4,,10
	.p2align 3
.L2690:
	movq	-384(%rbp), %rdi
	movl	$48, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rbx
	jmp	.L2436
	.p2align 4,,10
	.p2align 3
.L2678:
	movl	%r14d, %ebx
	movq	-392(%rbp), %r15
	movl	-360(%rbp), %r14d
	jmp	.L2454
.L2704:
	movq	%rsi, %rdi
	movl	$48, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-384(%rbp), %rcx
	jmp	.L2509
.L2698:
	movq	-384(%rbp), %rdi
	movl	$48, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L2470
.L2700:
	movq	-384(%rbp), %rdi
	movl	$48, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L2485
.L2701:
	movq	%rsi, %rdi
	movl	$48, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-384(%rbp), %rcx
	jmp	.L2487
.L2703:
	movq	-384(%rbp), %rdi
	movl	$48, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L2507
.L2581:
	movl	$32, %esi
	movl	$32, %r8d
	jmp	.L2440
.L2706:
	movq	$2, -352(%rbp)
	movzbl	1(%r15), %ecx
	movl	%ecx, %edx
	sall	$7, %edx
	andl	$16256, %edx
	orl	%edx, %esi
	testb	%cl, %cl
	jns	.L2573
	movq	$3, -352(%rbp)
	movzbl	2(%r15), %ecx
	movl	%ecx, %edx
	sall	$14, %edx
	andl	$2080768, %edx
	orl	%edx, %esi
	testb	%cl, %cl
	jns	.L2573
	movq	$4, -352(%rbp)
	movzbl	3(%r15), %ecx
	movl	%ecx, %edx
	sall	$21, %edx
	andl	$266338304, %edx
	orl	%edx, %esi
	testb	%cl, %cl
	jns	.L2573
	movq	$5, -352(%rbp)
	movzbl	4(%r15), %edx
	sall	$28, %edx
	orl	%edx, %esi
	jmp	.L2573
	.p2align 4,,10
	.p2align 3
.L2587:
	movl	$32, %esi
	movl	$32, %r9d
	jmp	.L2475
.L2597:
	movl	$8, %esi
	movl	$8, %ecx
	jmp	.L2513
.L2601:
	movq	-368(%rbp), %r8
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L2518:
	movq	(%r8,%rax,8), %rsi
	movq	%rsi, (%rdx,%rax,8)
	movq	%rax, %rsi
	addq	$1, %rax
	cmpq	%rsi, %rcx
	jne	.L2518
	jmp	.L2521
.L2600:
	movq	%rax, -344(%rbp)
	jmp	.L2517
.L2590:
	movq	$-32, %r13
	jmp	.L2479
.L2725:
	movq	%r8, -352(%rbp)
	movq	%rdx, -344(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-344(%rbp), %rdx
	movq	-352(%rbp), %r8
	jmp	.L2443
.L2727:
	movq	-464(%rbp), %rdi
	movq	%rcx, -352(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-352(%rbp), %rcx
	movq	%rax, %rdx
	jmp	.L2516
.L2730:
	movq	%r9, -408(%rbp)
	movq	%rdx, -400(%rbp)
	movq	%rcx, -392(%rbp)
	movl	%r10d, -352(%rbp)
	movq	%xmm0, -360(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-352(%rbp), %r10d
	movq	-360(%rbp), %xmm0
	movq	-392(%rbp), %rcx
	movq	-400(%rbp), %rdx
	movq	-408(%rbp), %r9
	jmp	.L2478
.L2728:
	cmpq	$268435455, %rdx
	movl	$268435455, %eax
	cmova	%rax, %rdx
	leaq	0(,%rdx,8), %rcx
	movq	%rcx, %rsi
	jmp	.L2513
.L2731:
	cmpq	$67108863, %rsi
	movl	$67108863, %r9d
	cmova	%r9, %rsi
	salq	$5, %rsi
	movq	%rsi, %r9
	jmp	.L2475
.L2696:
	call	__stack_chk_fail@PLT
.L2732:
	cmpq	$67108863, %rcx
	movl	$67108863, %eax
	cmova	%rax, %rcx
	salq	$5, %rcx
	movq	%rcx, %r8
	movq	%rcx, %rsi
	jmp	.L2440
.L2474:
	leaq	.LC5(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE19999:
	.size	_ZN2v88internal4wasm9SideTableC2EPNS0_4ZoneEPKNS1_10WasmModuleEPNS1_15InterpreterCodeE, .-_ZN2v88internal4wasm9SideTableC2EPNS0_4ZoneEPKNS1_10WasmModuleEPNS1_15InterpreterCodeE
	.weak	_ZN2v88internal4wasm9SideTableC1EPNS0_4ZoneEPKNS1_10WasmModuleEPNS1_15InterpreterCodeE
	.set	_ZN2v88internal4wasm9SideTableC1EPNS0_4ZoneEPKNS1_10WasmModuleEPNS1_15InterpreterCodeE,_ZN2v88internal4wasm9SideTableC2EPNS0_4ZoneEPKNS1_10WasmModuleEPNS1_15InterpreterCodeE
	.section	.text._ZN2v88internal4wasm15WasmInterpreter25SetFunctionCodeForTestingEPKNS1_12WasmFunctionEPKhS7_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15WasmInterpreter25SetFunctionCodeForTestingEPKNS1_12WasmFunctionEPKhS7_
	.type	_ZN2v88internal4wasm15WasmInterpreter25SetFunctionCodeForTestingEPKNS1_12WasmFunctionEPKhS7_, @function
_ZN2v88internal4wasm15WasmInterpreter25SetFunctionCodeForTestingEPKNS1_12WasmFunctionEPKhS7_:
.LFB20354:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %xmm1
	movq	%rdx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	8(%rsi), %eax
	movq	64(%rdi), %r12
	leaq	(%rax,%rax,4), %rcx
	leaq	(%rax,%rcx,2), %rcx
	movq	56(%r12), %rax
	leaq	(%rax,%rcx,8), %rbx
	movq	$0, 80(%rbx)
	movups	%xmm0, 48(%rbx)
	movups	%xmm0, 64(%rbx)
	testq	%rdx, %rdx
	je	.L2733
	movq	32(%r12), %rdi
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	subq	%r13, %rax
	cmpq	$63, %rax
	jbe	.L2741
	leaq	64(%r13), %rax
	movq	%rax, 16(%rdi)
.L2736:
	movq	40(%r12), %rdx
	movq	32(%r12), %rsi
	movq	%rbx, %rcx
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm9SideTableC1EPNS0_4ZoneEPKNS1_10WasmModuleEPNS1_15InterpreterCodeE
	movq	%r13, 80(%rbx)
.L2733:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2741:
	.cfi_restore_state
	movl	$64, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r13
	jmp	.L2736
	.cfi_endproc
.LFE20354:
	.size	_ZN2v88internal4wasm15WasmInterpreter25SetFunctionCodeForTestingEPKNS1_12WasmFunctionEPKhS7_, .-_ZN2v88internal4wasm15WasmInterpreter25SetFunctionCodeForTestingEPKNS1_12WasmFunctionEPKhS7_
	.section	.text._ZN2v88internal4wasm15WasmInterpreter13GetBreakpointEPKNS1_12WasmFunctionEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15WasmInterpreter13GetBreakpointEPKNS1_12WasmFunctionEm
	.type	_ZN2v88internal4wasm15WasmInterpreter13GetBreakpointEPKNS1_12WasmFunctionEm, @function
_ZN2v88internal4wasm15WasmInterpreter13GetBreakpointEPKNS1_12WasmFunctionEm:
.LFB20349:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	8(%rsi), %eax
	movq	64(%rdi), %r13
	leaq	(%rax,%rax,4), %rdx
	leaq	(%rax,%rdx,2), %rdx
	movq	56(%r13), %rax
	leaq	(%rax,%rdx,8), %rbx
	cmpq	$0, 80(%rbx)
	movq	64(%rbx), %rdx
	je	.L2752
.L2743:
	movq	72(%rbx), %rax
	subq	%rdx, %rax
	cmpq	%r12, %rax
	jbe	.L2747
	movl	8(%rbx), %eax
	cmpq	%r12, %rax
	ja	.L2747
	cmpb	$-1, (%rdx,%r12)
	popq	%rbx
	sete	%al
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2747:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2752:
	.cfi_restore_state
	testq	%rdx, %rdx
	je	.L2743
	movq	32(%r13), %rdi
	movq	16(%rdi), %r14
	movq	24(%rdi), %rax
	subq	%r14, %rax
	cmpq	$63, %rax
	jbe	.L2753
	leaq	64(%r14), %rax
	movq	%rax, 16(%rdi)
.L2745:
	movq	40(%r13), %rdx
	movq	32(%r13), %rsi
	movq	%rbx, %rcx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm9SideTableC1EPNS0_4ZoneEPKNS1_10WasmModuleEPNS1_15InterpreterCodeE
	movq	%r14, 80(%rbx)
	movq	64(%rbx), %rdx
	jmp	.L2743
	.p2align 4,,10
	.p2align 3
.L2753:
	movl	$64, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r14
	jmp	.L2745
	.cfi_endproc
.LFE20349:
	.size	_ZN2v88internal4wasm15WasmInterpreter13GetBreakpointEPKNS1_12WasmFunctionEm, .-_ZN2v88internal4wasm15WasmInterpreter13GetBreakpointEPKNS1_12WasmFunctionEm
	.section	.text._ZN2v88internal4wasm15WasmInterpreter13SetBreakpointEPKNS1_12WasmFunctionEmb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15WasmInterpreter13SetBreakpointEPKNS1_12WasmFunctionEmb
	.type	_ZN2v88internal4wasm15WasmInterpreter13SetBreakpointEPKNS1_12WasmFunctionEmb, @function
_ZN2v88internal4wasm15WasmInterpreter13SetBreakpointEPKNS1_12WasmFunctionEmb:
.LFB20348:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	8(%rsi), %eax
	movq	64(%rdi), %r14
	leaq	(%rax,%rax,4), %rdx
	leaq	(%rax,%rdx,2), %rdx
	movq	56(%r14), %rax
	leaq	(%rax,%rdx,8), %rbx
	cmpq	$0, 80(%rbx)
	movq	64(%rbx), %rdx
	je	.L2768
.L2755:
	movq	72(%rbx), %r14
	movl	8(%rbx), %eax
	subq	%rdx, %r14
	cmpq	%r12, %rax
	ja	.L2763
	cmpq	%r12, %r14
	jbe	.L2763
	movq	48(%rbx), %rcx
	testb	%r15b, %r15b
	je	.L2759
	cmpq	%rcx, %rdx
	je	.L2769
.L2760:
	addq	%r12, %rdx
	cmpb	$-1, (%rdx)
	movb	$-1, (%rdx)
	sete	%al
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2763:
	.cfi_restore_state
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2768:
	.cfi_restore_state
	testq	%rdx, %rdx
	je	.L2755
	movq	32(%r14), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$63, %rdx
	jbe	.L2770
	leaq	64(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L2757:
	movq	40(%r14), %rdx
	movq	32(%r14), %rsi
	movq	%rax, %rdi
	movq	%rbx, %rcx
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal4wasm9SideTableC1EPNS0_4ZoneEPKNS1_10WasmModuleEPNS1_15InterpreterCodeE
	movq	-56(%rbp), %rax
	movq	64(%rbx), %rdx
	movq	%rax, 80(%rbx)
	jmp	.L2755
	.p2align 4,,10
	.p2align 3
.L2759:
	movzbl	(%rcx,%r12), %ecx
	addq	%r12, %rdx
	cmpb	$-1, (%rdx)
	sete	%al
	movb	%cl, (%rdx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2769:
	.cfi_restore_state
	movq	16(%r13), %rdi
	movq	24(%r13), %rax
	leaq	7(%r14), %rsi
	andq	$-8, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L2771
	addq	%rdi, %rsi
	movq	%rsi, 16(%r13)
.L2762:
	movq	%rdi, 64(%rbx)
	movq	48(%rbx), %rsi
	movq	%r14, %rdx
	call	memcpy@PLT
	movq	64(%rbx), %rdx
	addq	%rdx, %r14
	movq	%r14, 72(%rbx)
	jmp	.L2760
	.p2align 4,,10
	.p2align 3
.L2770:
	movl	$64, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L2757
.L2771:
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdi
	jmp	.L2762
	.cfi_endproc
.LFE20348:
	.size	_ZN2v88internal4wasm15WasmInterpreter13SetBreakpointEPKNS1_12WasmFunctionEmb, .-_ZN2v88internal4wasm15WasmInterpreter13SetBreakpointEPKNS1_12WasmFunctionEmb
	.section	.text._ZN2v88internal4wasm10ThreadImpl20CallIndirectFunctionEjjj,"axG",@progbits,_ZN2v88internal4wasm10ThreadImpl7ExecuteEPNS1_15InterpreterCodeEmi,comdat
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm10ThreadImpl20CallIndirectFunctionEjjj, @function
_ZN2v88internal4wasm10ThreadImpl20CallIndirectFunctionEjjj:
.LFB20286:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	movl	%r8d, %edx
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$72, %rsp
	movq	8(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%r8d, -88(%rbp)
	movq	41088(%r12), %rax
	movq	41096(%r12), %r13
	addl	$1, 41104(%r12)
	movq	16(%rdi), %rsi
	movq	%rax, -96(%rbp)
	movq	(%rdi), %rax
	movq	8(%rdi), %rdi
	movq	8(%rax), %rax
	movq	112(%rax), %rax
	movl	(%rax,%rbx,4), %eax
	movl	%eax, -104(%rbp)
	call	_ZN2v88internal18WasmInstanceObject25IndirectFunctionTableSizeEPNS0_7IsolateENS0_6HandleIS1_EEj@PLT
	movl	-88(%rbp), %r8d
	cmpl	%r15d, %eax
	ja	.L2773
	movl	$1, %ebx
	xorl	%r15d, %r15d
.L2774:
	subl	$1, 41104(%r12)
	movq	-96(%rbp), %rax
	movq	%rax, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L2797
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2797:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2808
	addq	$72, %rsp
	movl	%ebx, %eax
	movq	%r15, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2773:
	.cfi_restore_state
	movq	16(%r14), %rax
	testl	%r8d, %r8d
	jne	.L2775
	movq	%rax, -80(%rbp)
	movq	$0, -72(%rbp)
.L2776:
	leaq	-80(%rbp), %r8
	movl	%r15d, -64(%rbp)
	movq	%r8, %rdi
	movq	%r8, -88(%rbp)
	call	_ZNK2v88internal26IndirectFunctionTableEntry6sig_idEv@PLT
	cmpl	-104(%rbp), %eax
	movq	-88(%rbp), %r8
	je	.L2780
	movl	$2, %ebx
	xorl	%r15d, %r15d
	jmp	.L2774
	.p2align 4,,10
	.p2align 3
.L2775:
	movq	$0, -80(%rbp)
	movq	(%rax), %rcx
	movq	%rcx, %rax
	movq	207(%rcx), %rcx
	andq	$-262144, %rax
	movq	24(%rax), %rdx
	leal	16(,%r8,8), %eax
	cltq
	subq	$37592, %rdx
	movq	-1(%rax,%rcx), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L2777
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L2778:
	movq	%rax, -72(%rbp)
	jmp	.L2776
	.p2align 4,,10
	.p2align 3
.L2780:
	movq	(%r14), %rax
	movq	8(%r14), %r15
	movq	%r8, %rdi
	movq	%r8, -88(%rbp)
	movq	8(%rax), %rax
	movq	88(%rax), %rax
	movq	(%rax,%rbx,8), %rax
	movq	%rax, -104(%rbp)
	call	_ZNK2v88internal26IndirectFunctionTableEntry10object_refEv@PLT
	movq	41112(%r15), %rdi
	movq	-88(%rbp), %r8
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L2781
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-88(%rbp), %r8
	movq	%rax, %rbx
.L2782:
	movq	%r8, %rdi
	call	_ZNK2v88internal26IndirectFunctionTableEntry6targetEv@PLT
	movq	8(%r14), %r11
	movq	%rax, %r15
	movq	45752(%r11), %rax
	movq	%r15, %rsi
	movq	%r11, -88(%rbp)
	leaq	280(%rax), %rdi
	call	_ZNK2v88internal4wasm15WasmCodeManager18LookupNativeModuleEm@PLT
	movq	%r15, %rsi
	movq	504(%rax), %rdx
	movq	%rax, %r8
	movq	%r8, %rdi
	movq	(%rdx), %rax
	cmpq	%r15, %rax
	ja	.L2784
	addq	8(%rdx), %rax
	cmpq	%rax, %r15
	jnb	.L2784
	movq	%r8, -112(%rbp)
	call	_ZNK2v88internal4wasm12NativeModule33GetFunctionIndexFromJumpTableSlotEm@PLT
	movq	-112(%rbp), %r8
	movl	%eax, %esi
	movl	%eax, %r15d
	movq	%r8, %rdi
	call	_ZNK2v88internal4wasm12NativeModule7HasCodeEj@PLT
	movq	-112(%rbp), %r8
	movq	-88(%rbp), %r11
	testb	%al, %al
	jne	.L2787
	movq	%r8, %rsi
	movl	%r15d, %edx
	movq	%r11, %rdi
	movq	%r8, -88(%rbp)
	call	_ZN2v88internal4wasm11CompileLazyEPNS0_7IsolateEPNS1_12NativeModuleEi@PLT
	movq	-88(%rbp), %r8
	testb	%al, %al
	je	.L2789
.L2787:
	movl	%r15d, %esi
	movq	%r8, %rdi
	call	_ZNK2v88internal4wasm12NativeModule7GetCodeEj@PLT
	movq	%rax, %rcx
.L2786:
	testq	%rcx, %rcx
	je	.L2789
	movq	(%rbx), %rax
	testb	$1, %al
	jne	.L2809
.L2791:
	movq	8(%r14), %rsi
	movq	-104(%rbp), %r8
	movq	%rbx, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl24CallExternalWasmFunctionEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEPKNS1_8WasmCodeEPNS0_9SignatureINS1_9ValueTypeEEE
	movq	%rax, %rbx
	movq	%rdx, %r15
	jmp	.L2774
	.p2align 4,,10
	.p2align 3
.L2777:
	movq	41088(%rdx), %rax
	cmpq	41096(%rdx), %rax
	je	.L2810
.L2779:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%rdx)
	movq	%rsi, (%rax)
	jmp	.L2778
	.p2align 4,,10
	.p2align 3
.L2781:
	movq	41088(%r15), %rbx
	cmpq	%rbx, 41096(%r15)
	je	.L2811
.L2783:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rbx)
	jmp	.L2782
.L2810:
	movq	%rdx, %rdi
	movq	%rsi, -112(%rbp)
	movq	%rdx, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-112(%rbp), %rsi
	movq	-88(%rbp), %rdx
	jmp	.L2779
	.p2align 4,,10
	.p2align 3
.L2784:
	call	_ZNK2v88internal4wasm12NativeModule6LookupEm@PLT
	movq	%rax, %rcx
	jmp	.L2786
	.p2align 4,,10
	.p2align 3
.L2809:
	movq	-1(%rax), %rax
	cmpw	$1100, 11(%rax)
	jne	.L2791
	movq	16(%r14), %rax
	cmpq	%rax, %rbx
	je	.L2792
	testq	%rax, %rax
	je	.L2791
	movq	(%rbx), %rsi
	cmpq	%rsi, (%rax)
	jne	.L2791
.L2792:
	movl	56(%rcx), %eax
	movq	(%r14), %rbx
	leaq	(%rax,%rax,4), %rdx
	leaq	(%rax,%rdx,2), %rdx
	movq	24(%rbx), %rax
	leaq	(%rax,%rdx,8), %r15
	cmpq	$0, 80(%r15)
	je	.L2812
.L2793:
	xorl	%ebx, %ebx
	jmp	.L2774
.L2812:
	cmpq	$0, 64(%r15)
	je	.L2793
	movq	(%rbx), %rdi
	movq	16(%rdi), %r14
	movq	24(%rdi), %rdx
	subq	%r14, %rdx
	cmpq	$63, %rdx
	jbe	.L2813
	leaq	64(%r14), %rdx
	movq	%rdx, 16(%rdi)
.L2795:
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	movq	%r15, %rcx
	movq	%r14, %rdi
	xorl	%ebx, %ebx
	call	_ZN2v88internal4wasm9SideTableC1EPNS0_4ZoneEPKNS1_10WasmModuleEPNS1_15InterpreterCodeE
	movq	%r14, 80(%r15)
	jmp	.L2774
	.p2align 4,,10
	.p2align 3
.L2789:
	movq	8(%r14), %rsi
	movq	%r14, %rdi
	xorl	%ebx, %ebx
	call	_ZN2v88internal4wasm10ThreadImpl15HandleExceptionEPNS0_7IsolateE
	cmpl	$1, %eax
	setne	%bl
	xorl	%r15d, %r15d
	addl	$4, %ebx
	andl	$15, %ebx
	jmp	.L2774
.L2811:
	movq	%r15, %rdi
	movq	%r8, -112(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-112(%rbp), %r8
	movq	-88(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L2783
.L2808:
	call	__stack_chk_fail@PLT
.L2813:
	movl	$64, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r14
	jmp	.L2795
	.cfi_endproc
.LFE20286:
	.size	_ZN2v88internal4wasm10ThreadImpl20CallIndirectFunctionEjjj, .-_ZN2v88internal4wasm10ThreadImpl20CallIndirectFunctionEjjj
	.section	.text._ZNSt6vectorIN2v88internal4wasm10ThreadImpl5FrameENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal4wasm10ThreadImpl5FrameENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal4wasm10ThreadImpl5FrameENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal4wasm10ThreadImpl5FrameENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, @function
_ZNSt6vectorIN2v88internal4wasm10ThreadImpl5FrameENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_:
.LFB24874:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	movabsq	$-6148914691236517205, %rdx
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r12
	movq	8(%rdi), %r14
	movq	%r12, %rax
	subq	%r14, %rax
	sarq	$3, %rax
	imulq	%rdx, %rax
	cmpq	$89478485, %rax
	je	.L2830
	movq	%rsi, %rdx
	movq	%rdi, %r13
	movq	%rsi, %rbx
	subq	%r14, %rdx
	testq	%rax, %rax
	je	.L2824
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L2831
	movl	$2147483640, %esi
	movl	$2147483640, %r8d
.L2816:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	%rsi, %rcx
	jb	.L2832
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L2819:
	addq	%rax, %r8
	leaq	24(%rax), %rdi
	jmp	.L2817
	.p2align 4,,10
	.p2align 3
.L2831:
	testq	%rcx, %rcx
	jne	.L2833
	movl	$24, %edi
	xorl	%r8d, %r8d
	xorl	%eax, %eax
.L2817:
	movdqu	(%r15), %xmm3
	movups	%xmm3, (%rax,%rdx)
	movq	16(%r15), %rcx
	movq	%rcx, 16(%rax,%rdx)
	cmpq	%r14, %rbx
	je	.L2820
	movq	%r14, %rdx
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L2821:
	movdqu	(%rdx), %xmm1
	addq	$24, %rdx
	addq	$24, %rcx
	movups	%xmm1, -24(%rcx)
	movq	-8(%rdx), %rsi
	movq	%rsi, -8(%rcx)
	cmpq	%rdx, %rbx
	jne	.L2821
	leaq	-24(%rbx), %rdx
	subq	%r14, %rdx
	shrq	$3, %rdx
	leaq	48(%rax,%rdx,8), %rdi
.L2820:
	cmpq	%r12, %rbx
	je	.L2822
	movq	%rbx, %rdx
	movq	%rdi, %rcx
	.p2align 4,,10
	.p2align 3
.L2823:
	movdqu	(%rdx), %xmm2
	addq	$24, %rdx
	addq	$24, %rcx
	movups	%xmm2, -24(%rcx)
	movq	-8(%rdx), %rsi
	movq	%rsi, -8(%rcx)
	cmpq	%rdx, %r12
	jne	.L2823
	subq	%rbx, %r12
	leaq	-24(%r12), %rdx
	shrq	$3, %rdx
	leaq	24(%rdi,%rdx,8), %rdi
.L2822:
	movq	%rax, %xmm0
	movq	%rdi, %xmm4
	movq	%r8, 24(%r13)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, 8(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2824:
	.cfi_restore_state
	movl	$24, %esi
	movl	$24, %r8d
	jmp	.L2816
.L2832:
	movq	%rdx, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %rdx
	jmp	.L2819
.L2833:
	cmpq	$89478485, %rcx
	movl	$89478485, %r8d
	cmovbe	%rcx, %r8
	imulq	$24, %r8, %r8
	movq	%r8, %rsi
	jmp	.L2816
.L2830:
	leaq	.LC5(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE24874:
	.size	_ZNSt6vectorIN2v88internal4wasm10ThreadImpl5FrameENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, .-_ZNSt6vectorIN2v88internal4wasm10ThreadImpl5FrameENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.section	.text._ZN2v88internal4wasm10ThreadImpl6DoCallEPNS1_7DecoderEPNS1_15InterpreterCodeEPmS7_,"axG",@progbits,_ZN2v88internal4wasm10ThreadImpl6DoCallEPNS1_7DecoderEPNS1_15InterpreterCodeEPmS7_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm10ThreadImpl6DoCallEPNS1_7DecoderEPNS1_15InterpreterCodeEPmS7_
	.type	_ZN2v88internal4wasm10ThreadImpl6DoCallEPNS1_7DecoderEPNS1_15InterpreterCodeEPmS7_, @function
_ZN2v88internal4wasm10ThreadImpl6DoCallEPNS1_7DecoderEPNS1_15InterpreterCodeEPmS7_:
.LFB20110:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$184, %rsp
	movq	%rcx, -216(%rbp)
	movq	(%rcx), %rdx
	movq	%r8, -224(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	72(%rdi), %rax
	movq	%rdx, -16(%rax)
	movq	80(%r12), %rax
	movq	32(%r15), %r13
	movq	32(%r12), %rdx
	movl	56(%rax), %edi
	movq	40(%r15), %rax
	subq	24(%r12), %rdx
	movq	%r13, %rcx
	addq	%rdi, %rdx
	subq	%rax, %rcx
	movabsq	$-1085102592571150095, %rdi
	imulq	%rdi, %rcx
	cmpq	%rcx, %rdx
	ja	.L2911
.L2836:
	addq	$1, 112(%r15)
	movq	(%r12), %rdx
	movabsq	$-1085102592571150095, %rdi
	subq	24(%r15), %rax
	movq	72(%r15), %rsi
	movq	(%rdx), %rdx
	imulq	%rdi, %rax
	movq	%r12, -160(%rbp)
	movq	8(%rdx), %rdx
	movq	$0, -152(%rbp)
	subq	%rdx, %rax
	movq	%rax, -144(%rbp)
	cmpq	80(%r15), %rsi
	je	.L2855
	movdqa	-160(%rbp), %xmm4
	movups	%xmm4, (%rsi)
	movq	-144(%rbp), %rax
	movq	%rax, 16(%rsi)
	movq	72(%r15), %rax
	addq	$24, %rax
	movq	%rax, -200(%rbp)
	movq	%rax, 72(%r15)
.L2856:
	movq	32(%r12), %rax
	movq	24(%r12), %r14
	leaq	.L2862(%rip), %r13
	movq	%rax, -168(%rbp)
	cmpq	%rax, %r14
	je	.L2874
	movq	%rbx, -208(%rbp)
	.p2align 4,,10
	.p2align 3
.L2873:
	movzbl	(%r14), %ecx
	cmpb	$9, %cl
	ja	.L2860
	movzbl	%cl, %eax
	movslq	0(%r13,%rax,4), %rax
	addq	%r13, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm10ThreadImpl6DoCallEPNS1_7DecoderEPNS1_15InterpreterCodeEPmS7_,"aG",@progbits,_ZN2v88internal4wasm10ThreadImpl6DoCallEPNS1_7DecoderEPNS1_15InterpreterCodeEPmS7_,comdat
	.align 4
	.align 4
.L2862:
	.long	.L2860-.L2862
	.long	.L2865-.L2862
	.long	.L2863-.L2862
	.long	.L2865-.L2862
	.long	.L2863-.L2862
	.long	.L2863-.L2862
	.long	.L2861-.L2862
	.long	.L2861-.L2862
	.long	.L2860-.L2862
	.long	.L2861-.L2862
	.section	.text._ZN2v88internal4wasm10ThreadImpl6DoCallEPNS1_7DecoderEPNS1_15InterpreterCodeEPmS7_,"axG",@progbits,_ZN2v88internal4wasm10ThreadImpl6DoCallEPNS1_7DecoderEPNS1_15InterpreterCodeEPmS7_,comdat
	.p2align 4,,10
	.p2align 3
.L2865:
	pxor	%xmm0, %xmm0
	movups	%xmm0, -95(%rbp)
	movq	-95(%rbp), %rax
	movq	-87(%rbp), %rdx
.L2868:
	movq	%rdx, -87(%rbp)
	movzbl	-80(%rbp), %edx
	movb	%cl, -96(%rbp)
	movq	40(%r15), %rsi
	movq	%rax, -95(%rbp)
	movdqa	-96(%rbp), %xmm3
	movb	%dl, -112(%rbp)
	movaps	%xmm3, -128(%rbp)
.L2880:
	movq	%rax, -127(%rbp)
	movzbl	-112(%rbp), %eax
	leaq	17(%rsi), %rdx
	addq	$1, %r14
	movb	%cl, -128(%rbp)
	movdqa	-128(%rbp), %xmm2
	movq	%rdx, 40(%r15)
	movb	%al, 16(%rsi)
	movups	%xmm2, (%rsi)
	cmpq	%r14, -168(%rbp)
	jne	.L2873
	movq	-208(%rbp), %rbx
.L2874:
	movq	-200(%rbp), %rax
	movl	8(%r12), %edi
	movq	%rdi, -16(%rax)
	movq	72(%r15), %rcx
	movl	_ZN2v88internal15FLAG_stack_sizeE(%rip), %edx
	movq	40(%r15), %rax
	movq	%rcx, %rsi
	subq	24(%r15), %rax
	subq	64(%r15), %rsi
	sall	$10, %edx
	addq	%rsi, %rax
	movslq	%edx, %rdx
	cmpq	%rax, %rdx
	jb	.L2912
.L2858:
	movq	-16(%rcx), %rax
	movq	-216(%rbp), %rdi
	movl	$0, -92(%rbp)
	movl	$0, -96(%rbp)
	movq	%rax, (%rdi)
	movq	-224(%rbp), %rdi
	movq	72(%r12), %rax
	subq	64(%r12), %rax
	movb	$0, -72(%rbp)
	movq	%rax, (%rdi)
	movq	72(%r12), %rax
	movdqu	64(%r12), %xmm0
	movq	48(%rbx), %rdx
	movl	$0, 32(%rbx)
	movq	%rax, 24(%rbx)
	leaq	-72(%rbp), %rax
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, -88(%rbp)
	movl	$0, 40(%rbx)
	movq	$0, 56(%rbx)
	movups	%xmm0, 8(%rbx)
	movb	$0, (%rdx)
	movq	-88(%rbp), %rdx
	movq	$0, -80(%rbp)
	movb	$0, (%rdx)
	movq	-88(%rbp), %rdi
	cmpq	%rax, %rdi
	je	.L2876
	call	_ZdlPv@PLT
.L2876:
	movl	$1, %eax
.L2834:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L2913
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2863:
	.cfi_restore_state
	xorl	%eax, %eax
	xorl	%edx, %edx
	jmp	.L2868
	.p2align 4,,10
	.p2align 3
.L2861:
	movq	40(%r15), %rax
	subq	24(%r15), %rax
	movb	$6, -96(%rbp)
	movabsq	$-1085102592571150095, %rbx
	movq	$0, -87(%rbp)
	movzbl	-80(%rbp), %ecx
	imulq	%rbx, %rax
	movq	$0, -95(%rbp)
	movq	8(%r15), %rdx
	movb	%cl, -112(%rbp)
	movq	48(%r15), %rcx
	leal	16(,%rax,8), %eax
	movdqa	-96(%rbp), %xmm1
	movq	104(%rdx), %rdx
	movq	(%rcx), %rcx
	cltq
	movaps	%xmm1, -128(%rbp)
	movq	7(%rcx), %rdi
	leaq	-1(%rdi,%rax), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L2877
	movq	%rdx, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L2871
	movq	%rdx, -192(%rbp)
	movq	%rsi, -184(%rbp)
	movq	%rdi, -176(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-192(%rbp), %rdx
	movq	-184(%rbp), %rsi
	movq	-176(%rbp), %rdi
.L2871:
	testb	$24, %al
	je	.L2877
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L2877
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L2877:
	movq	40(%r15), %rsi
	xorl	%eax, %eax
	movl	$6, %ecx
	jmp	.L2880
	.p2align 4,,10
	.p2align 3
.L2855:
	leaq	-160(%rbp), %rdx
	leaq	56(%r15), %rdi
	call	_ZNSt6vectorIN2v88internal4wasm10ThreadImpl5FrameENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	movq	72(%r15), %rax
	movq	%rax, -200(%rbp)
	jmp	.L2856
	.p2align 4,,10
	.p2align 3
.L2911:
	movq	24(%r15), %rcx
	subq	%rcx, %r13
	subq	%rcx, %rax
	movq	%r13, %r14
	imulq	%rdi, %r14
	imulq	%rax, %rdi
	addq	%rdx, %rdi
	call	_ZN2v84base4bits21RoundUpToPowerOfTwo64Em@PLT
	movq	%rax, %rdx
	leaq	(%r14,%r14), %rax
	cmpq	%rdx, %rax
	cmovb	%rdx, %rax
	movl	$8, %edx
	movq	%rdx, %rsi
	movabsq	$542551296285575047, %rdx
	cmpq	$8, %rax
	cmovnb	%rax, %rsi
	cmpq	%rdx, %rax
	movq	%rsi, -168(%rbp)
	movq	%rsi, %rax
	jbe	.L2837
	salq	$4, %rax
	movq	$-1, %rdi
	addq	%rsi, %rax
	movq	%rax, -176(%rbp)
.L2838:
	call	_Znam@PLT
	movq	-168(%rbp), %rdx
	movq	%rax, %rcx
	subq	$1, %rdx
	js	.L2842
	.p2align 4,,10
	.p2align 3
.L2843:
	pxor	%xmm0, %xmm0
	subq	$1, %rdx
	movb	$0, (%rax)
	addq	$17, %rax
	movups	%xmm0, -16(%rax)
	cmpq	$-1, %rdx
	jne	.L2843
.L2842:
	movq	24(%r15), %r8
	testq	%r14, %r14
	jne	.L2914
	movq	40(%r15), %rax
	movq	%rcx, 24(%r15)
	addq	%rcx, %rax
	subq	%r8, %rax
	movq	%rax, 40(%r15)
	testq	%r8, %r8
	jne	.L2844
.L2845:
	movq	-176(%rbp), %rax
	movq	8(%r15), %r13
	movl	-168(%rbp), %edx
	addq	%rcx, %rax
	movq	%rax, 32(%r15)
	movq	41088(%r13), %rax
	subl	%r14d, %edx
	addl	$1, 41104(%r13)
	movq	8(%r15), %rcx
	movq	%rax, -184(%rbp)
	movq	41096(%r13), %rax
	movq	41112(%rcx), %rdi
	movq	%rax, -176(%rbp)
	movq	48(%r15), %rax
	movq	(%rax), %rax
	movq	7(%rax), %r11
	testq	%rdi, %rdi
	je	.L2846
	movq	%r11, %rsi
	movl	%edx, -192(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-192(%rbp), %edx
	movq	%rax, %rsi
.L2847:
	movq	8(%r15), %rdi
	xorl	%ecx, %ecx
	call	_ZN2v88internal7Factory21CopyFixedArrayAndGrowENS0_6HandleINS0_10FixedArrayEEEiNS0_14AllocationTypeE@PLT
	movq	-168(%rbp), %rdi
	movq	(%rax), %r11
	cmpl	%edi, %r14d
	jge	.L2849
	movl	%r14d, %ecx
	leal	16(,%r14,8), %edx
	movq	%r11, %rsi
	movslq	%r14d, %r14
	notl	%ecx
	andq	$-262144, %rsi
	movslq	%edx, %rdx
	addl	%edi, %ecx
	addq	$24, %rsi
	leaq	-1(%r11,%rdx), %rdx
	addq	%rcx, %r14
	leaq	23(%r11,%r14,8), %rdi
	.p2align 4,,10
	.p2align 3
.L2850:
	movq	(%rsi), %rcx
	addq	$8, %rdx
	movq	-37496(%rcx), %rcx
	movq	%rcx, -8(%rdx)
	cmpq	%rdi, %rdx
	jne	.L2850
	movq	(%rax), %r11
.L2849:
	movq	48(%r15), %rax
	movq	(%rax), %r14
	movq	%r11, 7(%r14)
	leaq	7(%r14), %rsi
	testb	$1, %r11b
	je	.L2854
	movq	%r11, %rax
	andq	$-262144, %rax
	movq	8(%rax), %rdx
	movq	%rax, -168(%rbp)
	testl	$262144, %edx
	jne	.L2915
.L2852:
	andl	$24, %edx
	je	.L2854
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L2854
	movq	%r11, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L2854:
	movq	-184(%rbp), %rax
	subl	$1, 41104(%r13)
	movq	%rax, 41088(%r13)
	movq	-176(%rbp), %rax
	cmpq	41096(%r13), %rax
	je	.L2879
	movq	%rax, 41096(%r13)
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2879:
	movq	40(%r15), %rax
	jmp	.L2836
	.p2align 4,,10
	.p2align 3
.L2914:
	movq	%r8, %rsi
	movq	%rcx, %rdi
	movq	%r13, %rdx
	movq	%r8, -184(%rbp)
	call	memcpy@PLT
	movq	-184(%rbp), %r8
	movq	%rax, %rcx
	movq	40(%r15), %rax
	movq	%rcx, 24(%r15)
	addq	%rcx, %rax
	subq	%r8, %rax
	movq	%rax, 40(%r15)
.L2844:
	movq	%r8, %rdi
	call	_ZdaPv@PLT
	movq	24(%r15), %rcx
	jmp	.L2845
	.p2align 4,,10
	.p2align 3
.L2912:
	movq	$0, -16(%rcx)
	movq	8(%r15), %rdi
	call	_ZN2v88internal7Isolate13StackOverflowEv@PLT
	movq	8(%r15), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl15HandleExceptionEPNS0_7IsolateE
	movl	%eax, %r8d
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jne	.L2834
	movq	72(%r15), %rcx
	jmp	.L2858
	.p2align 4,,10
	.p2align 3
.L2860:
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2915:
	movq	%r11, %rdx
	movq	%r14, %rdi
	movq	%r11, -200(%rbp)
	movq	%rsi, -192(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-168(%rbp), %rax
	movq	-200(%rbp), %r11
	movq	-192(%rbp), %rsi
	movq	8(%rax), %rdx
	jmp	.L2852
	.p2align 4,,10
	.p2align 3
.L2837:
	movq	%rsi, %rdi
	salq	$4, %rdi
	addq	%rsi, %rdi
	movq	%rdi, -176(%rbp)
	jmp	.L2838
	.p2align 4,,10
	.p2align 3
.L2846:
	movq	41088(%rcx), %rsi
	cmpq	41096(%rcx), %rsi
	je	.L2916
.L2848:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rcx)
	movq	%r11, (%rsi)
	jmp	.L2847
.L2916:
	movq	%rcx, %rdi
	movq	%r11, -208(%rbp)
	movl	%edx, -200(%rbp)
	movq	%rcx, -192(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-208(%rbp), %r11
	movl	-200(%rbp), %edx
	movq	-192(%rbp), %rcx
	movq	%rax, %rsi
	jmp	.L2848
.L2913:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20110:
	.size	_ZN2v88internal4wasm10ThreadImpl6DoCallEPNS1_7DecoderEPNS1_15InterpreterCodeEPmS7_, .-_ZN2v88internal4wasm10ThreadImpl6DoCallEPNS1_7DecoderEPNS1_15InterpreterCodeEPmS7_
	.section	.rodata._ZN2v88internal4wasm10ThreadImpl7ExecuteEPNS1_15InterpreterCodeEmi.str1.1,"aMS",@progbits,1
.LC55:
	.string	"invalid select type"
	.globl	__popcountdi2
	.section	.text._ZN2v88internal4wasm10ThreadImpl7ExecuteEPNS1_15InterpreterCodeEmi,"axG",@progbits,_ZN2v88internal4wasm10ThreadImpl7ExecuteEPNS1_15InterpreterCodeEmi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm10ThreadImpl7ExecuteEPNS1_15InterpreterCodeEmi
	.type	_ZN2v88internal4wasm10ThreadImpl7ExecuteEPNS1_15InterpreterCodeEmi, @function
_ZN2v88internal4wasm10ThreadImpl7ExecuteEPNS1_15InterpreterCodeEmi:
.LFB20265:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %rdi
	pushq	%rbx
	subq	$552, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -504(%rbp)
	movq	%rdx, -512(%rbp)
	movq	%rsi, %rdx
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	movq	72(%rsi), %rcx
	movq	64(%rsi), %rsi
	movb	$0, -80(%rbp)
	movq	64(%rdx), %rdx
	movq	%rdi, -144(%rbp)
	movq	%rsi, -136(%rbp)
	movq	%rsi, -128(%rbp)
	leaq	-80(%rbp), %rsi
	movq	%rcx, -120(%rbp)
	subq	%rdx, %rcx
	movl	$0, -112(%rbp)
	movl	$0, -104(%rbp)
	movq	%rsi, -528(%rbp)
	movq	%rsi, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rcx, -488(%rbp)
	movb	$0, -513(%rbp)
	.p2align 4,,10
	.p2align 3
.L2918:
	movl	$1, -492(%rbp)
	movzbl	(%rdx,%rax), %r14d
	movl	%r14d, %edi
	movq	%r14, %rbx
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	testb	%al, %al
	je	.L2919
	movq	-504(%rbp), %rax
	sall	$8, %r14d
	movq	64(%rax), %rdx
	movq	-512(%rbp), %rax
	movzbl	1(%rdx,%rax), %eax
	orl	%eax, %r14d
.L2919:
	cmpb	$-1, %bl
	je	.L3951
	testl	%r13d, %r13d
	je	.L2924
.L3954:
	setg	%al
	movzbl	%al, %eax
	subl	%eax, %r13d
	cmpb	$-2, %bl
	ja	.L2926
	leaq	.L2928(%rip), %rdx
	movslq	(%rdx,%rbx,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm10ThreadImpl7ExecuteEPNS1_15InterpreterCodeEmi,"aG",@progbits,_ZN2v88internal4wasm10ThreadImpl7ExecuteEPNS1_15InterpreterCodeEmi,comdat
	.align 4
	.align 4
.L2928:
	.long	.L3136-.L2928
	.long	.L3129-.L2928
	.long	.L3134-.L2928
	.long	.L3134-.L2928
	.long	.L3135-.L2928
	.long	.L3133-.L2928
	.long	.L3134-.L2928
	.long	.L3133-.L2928
	.long	.L3132-.L2928
	.long	.L3131-.L2928
	.long	.L3130-.L2928
	.long	.L3129-.L2928
	.long	.L3128-.L2928
	.long	.L3127-.L2928
	.long	.L3126-.L2928
	.long	.L3125-.L2928
	.long	.L3124-.L2928
	.long	.L3123-.L2928
	.long	.L3122-.L2928
	.long	.L3121-.L2928
	.long	.L2926-.L2928
	.long	.L2926-.L2928
	.long	.L2926-.L2928
	.long	.L2926-.L2928
	.long	.L2926-.L2928
	.long	.L2926-.L2928
	.long	.L3120-.L2928
	.long	.L3119-.L2928
	.long	.L3118-.L2928
	.long	.L2926-.L2928
	.long	.L2926-.L2928
	.long	.L2926-.L2928
	.long	.L3117-.L2928
	.long	.L3116-.L2928
	.long	.L3115-.L2928
	.long	.L3114-.L2928
	.long	.L3113-.L2928
	.long	.L3112-.L2928
	.long	.L3111-.L2928
	.long	.L2926-.L2928
	.long	.L3110-.L2928
	.long	.L3109-.L2928
	.long	.L3108-.L2928
	.long	.L3107-.L2928
	.long	.L3106-.L2928
	.long	.L3105-.L2928
	.long	.L3104-.L2928
	.long	.L3103-.L2928
	.long	.L3102-.L2928
	.long	.L3101-.L2928
	.long	.L3100-.L2928
	.long	.L3099-.L2928
	.long	.L3098-.L2928
	.long	.L3097-.L2928
	.long	.L3096-.L2928
	.long	.L3095-.L2928
	.long	.L3094-.L2928
	.long	.L3093-.L2928
	.long	.L3092-.L2928
	.long	.L3091-.L2928
	.long	.L3090-.L2928
	.long	.L3089-.L2928
	.long	.L3088-.L2928
	.long	.L3087-.L2928
	.long	.L3086-.L2928
	.long	.L3085-.L2928
	.long	.L3084-.L2928
	.long	.L3083-.L2928
	.long	.L3082-.L2928
	.long	.L3081-.L2928
	.long	.L3080-.L2928
	.long	.L3079-.L2928
	.long	.L3078-.L2928
	.long	.L3077-.L2928
	.long	.L3076-.L2928
	.long	.L3075-.L2928
	.long	.L3074-.L2928
	.long	.L3073-.L2928
	.long	.L3072-.L2928
	.long	.L3071-.L2928
	.long	.L3070-.L2928
	.long	.L3069-.L2928
	.long	.L3068-.L2928
	.long	.L3067-.L2928
	.long	.L3066-.L2928
	.long	.L3065-.L2928
	.long	.L3064-.L2928
	.long	.L3063-.L2928
	.long	.L3062-.L2928
	.long	.L3061-.L2928
	.long	.L3060-.L2928
	.long	.L3059-.L2928
	.long	.L3058-.L2928
	.long	.L3057-.L2928
	.long	.L3056-.L2928
	.long	.L3055-.L2928
	.long	.L3054-.L2928
	.long	.L3053-.L2928
	.long	.L3052-.L2928
	.long	.L3051-.L2928
	.long	.L3050-.L2928
	.long	.L3049-.L2928
	.long	.L3048-.L2928
	.long	.L3047-.L2928
	.long	.L3046-.L2928
	.long	.L3045-.L2928
	.long	.L3044-.L2928
	.long	.L3043-.L2928
	.long	.L3042-.L2928
	.long	.L3041-.L2928
	.long	.L3040-.L2928
	.long	.L3039-.L2928
	.long	.L3038-.L2928
	.long	.L3037-.L2928
	.long	.L3036-.L2928
	.long	.L3035-.L2928
	.long	.L3034-.L2928
	.long	.L3033-.L2928
	.long	.L3032-.L2928
	.long	.L3031-.L2928
	.long	.L3030-.L2928
	.long	.L3029-.L2928
	.long	.L3028-.L2928
	.long	.L3027-.L2928
	.long	.L3026-.L2928
	.long	.L3025-.L2928
	.long	.L3024-.L2928
	.long	.L3023-.L2928
	.long	.L3022-.L2928
	.long	.L3021-.L2928
	.long	.L3020-.L2928
	.long	.L3019-.L2928
	.long	.L3018-.L2928
	.long	.L3017-.L2928
	.long	.L3016-.L2928
	.long	.L3015-.L2928
	.long	.L3014-.L2928
	.long	.L3013-.L2928
	.long	.L3012-.L2928
	.long	.L3011-.L2928
	.long	.L3010-.L2928
	.long	.L3009-.L2928
	.long	.L3008-.L2928
	.long	.L3007-.L2928
	.long	.L3006-.L2928
	.long	.L3005-.L2928
	.long	.L3004-.L2928
	.long	.L3003-.L2928
	.long	.L3002-.L2928
	.long	.L3001-.L2928
	.long	.L3000-.L2928
	.long	.L2999-.L2928
	.long	.L2998-.L2928
	.long	.L2997-.L2928
	.long	.L2996-.L2928
	.long	.L2995-.L2928
	.long	.L2994-.L2928
	.long	.L2993-.L2928
	.long	.L2992-.L2928
	.long	.L2991-.L2928
	.long	.L2990-.L2928
	.long	.L2989-.L2928
	.long	.L2988-.L2928
	.long	.L2987-.L2928
	.long	.L2986-.L2928
	.long	.L2985-.L2928
	.long	.L2984-.L2928
	.long	.L2983-.L2928
	.long	.L2982-.L2928
	.long	.L2981-.L2928
	.long	.L2980-.L2928
	.long	.L2979-.L2928
	.long	.L2978-.L2928
	.long	.L2977-.L2928
	.long	.L2976-.L2928
	.long	.L2975-.L2928
	.long	.L2974-.L2928
	.long	.L2973-.L2928
	.long	.L2972-.L2928
	.long	.L2971-.L2928
	.long	.L2970-.L2928
	.long	.L2969-.L2928
	.long	.L2968-.L2928
	.long	.L2967-.L2928
	.long	.L2966-.L2928
	.long	.L2965-.L2928
	.long	.L2964-.L2928
	.long	.L2963-.L2928
	.long	.L2962-.L2928
	.long	.L2961-.L2928
	.long	.L2960-.L2928
	.long	.L2959-.L2928
	.long	.L2958-.L2928
	.long	.L2957-.L2928
	.long	.L2956-.L2928
	.long	.L2955-.L2928
	.long	.L2954-.L2928
	.long	.L2926-.L2928
	.long	.L2926-.L2928
	.long	.L2926-.L2928
	.long	.L2926-.L2928
	.long	.L2926-.L2928
	.long	.L2926-.L2928
	.long	.L2926-.L2928
	.long	.L2926-.L2928
	.long	.L2926-.L2928
	.long	.L2926-.L2928
	.long	.L2926-.L2928
	.long	.L2953-.L2928
	.long	.L2952-.L2928
	.long	.L2951-.L2928
	.long	.L2950-.L2928
	.long	.L2949-.L2928
	.long	.L2948-.L2928
	.long	.L2947-.L2928
	.long	.L2946-.L2928
	.long	.L2945-.L2928
	.long	.L2944-.L2928
	.long	.L2943-.L2928
	.long	.L2942-.L2928
	.long	.L2941-.L2928
	.long	.L2940-.L2928
	.long	.L2939-.L2928
	.long	.L2938-.L2928
	.long	.L2937-.L2928
	.long	.L2936-.L2928
	.long	.L2935-.L2928
	.long	.L2934-.L2928
	.long	.L2933-.L2928
	.long	.L2932-.L2928
	.long	.L2931-.L2928
	.long	.L2926-.L2928
	.long	.L2926-.L2928
	.long	.L2926-.L2928
	.long	.L2926-.L2928
	.long	.L2926-.L2928
	.long	.L2926-.L2928
	.long	.L2926-.L2928
	.long	.L2926-.L2928
	.long	.L2926-.L2928
	.long	.L2926-.L2928
	.long	.L2926-.L2928
	.long	.L2926-.L2928
	.long	.L2926-.L2928
	.long	.L2926-.L2928
	.long	.L2926-.L2928
	.long	.L2926-.L2928
	.long	.L2926-.L2928
	.long	.L2926-.L2928
	.long	.L2926-.L2928
	.long	.L2926-.L2928
	.long	.L2926-.L2928
	.long	.L2930-.L2928
	.long	.L2929-.L2928
	.long	.L2927-.L2928
	.section	.text._ZN2v88internal4wasm10ThreadImpl7ExecuteEPNS1_15InterpreterCodeEmi,"axG",@progbits,_ZN2v88internal4wasm10ThreadImpl7ExecuteEPNS1_15InterpreterCodeEmi,comdat
	.p2align 4,,10
	.p2align 3
.L3951:
	movq	-504(%rbp), %rax
	movq	48(%rax), %rdx
	movq	-512(%rbp), %rax
	movzbl	(%rdx,%rax), %r15d
	movl	%r15d, %edi
	movq	%r15, %rbx
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	testb	%al, %al
	movq	-512(%rbp), %rax
	jne	.L2921
	cmpq	%rax, 96(%r12)
	je	.L3952
.L2923:
	movl	$2, 88(%r12)
.L3613:
	movq	%rax, %rdx
.L3614:
	movq	%rax, 96(%r12)
	movq	72(%r12), %rax
	movq	%rdx, -16(%rax)
.L3947:
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, -144(%rbp)
	cmpq	-528(%rbp), %rdi
	je	.L2917
	call	_ZdlPv@PLT
.L2917:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3953
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2921:
	.cfi_restore_state
	movq	-504(%rbp), %rdx
	sall	$8, %r15d
	movq	48(%rdx), %rdx
	movzbl	1(%rdx,%rax), %r14d
	orl	%r15d, %r14d
	cmpq	%rax, 96(%r12)
	jne	.L2923
.L3952:
	movl	$2147483648, %eax
	movq	%rax, 96(%r12)
	testl	%r13d, %r13d
	jne	.L3954
	.p2align 4,,10
	.p2align 3
.L2924:
	cmpb	$0, -513(%rbp)
	movq	-512(%rbp), %rdx
	movl	$2, 88(%r12)
	jne	.L3719
	movl	$2147483648, %eax
	jmp	.L3614
.L3184:
	movb	$10, -412(%rbp)
.L3188:
	leaq	1(%r8), %rsi
	leaq	-144(%rbp), %rdi
	leaq	.LC55(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movl	-416(%rbp), %eax
.L3185:
	addl	$1, %eax
	movl	%eax, -492(%rbp)
.L3119:
	leaq	-256(%rbp), %rdi
	movq	%r12, %rsi
	leaq	-224(%rbp), %r14
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-192(%rbp), %rbx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	%rbx, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-255(%rbp), %r15d
	movq	%r12, %rdi
	testl	%r15d, %r15d
	cmove	%r14, %rbx
	subq	$32, %rsp
	movdqa	(%rbx), %xmm5
	movups	%xmm5, (%rsp)
	movzbl	16(%rbx), %eax
	movb	%al, 16(%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	.p2align 4,,10
	.p2align 3
.L3137:
	addq	%rcx, %rax
	movq	%rax, -512(%rbp)
	cmpq	-488(%rbp), %rax
	je	.L3955
.L3612:
	movq	64(%r15), %rdx
	jmp	.L2918
	.p2align 4,,10
	.p2align 3
.L3955:
	movq	(%r15), %rax
	leaq	-504(%rbp), %rdx
	leaq	-512(%rbp), %rcx
	movq	%r12, %rdi
	leaq	-144(%rbp), %rsi
	leaq	-488(%rbp), %r8
	movq	(%rax), %rax
	movq	(%rax), %r9
	call	_ZN2v88internal4wasm10ThreadImpl8DoReturnEPNS1_7DecoderEPPNS1_15InterpreterCodeEPmS8_m
	movl	%eax, %edx
	testb	%al, %al
	je	.L3947
	movzbl	109(%r12), %ecx
	movzbl	-513(%rbp), %esi
	movq	-504(%rbp), %r15
	movq	-512(%rbp), %rax
	andl	$1, %ecx
	cmovne	%edx, %esi
	movl	$0, %edx
	cmovne	%edx, %r13d
	movb	%sil, -513(%rbp)
	jmp	.L3612
	.p2align 4,,10
	.p2align 3
.L2926:
	movq	-504(%rbp), %rax
	movq	64(%rax), %rdx
	movq	-512(%rbp), %rax
	movzbl	(%rdx,%rax), %edi
	movl	$-1, %eax
	leaq	.LC8(%rip), %rdx
	cmpl	$255, %edi
	je	.L3610
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	%rax, %rdx
	movq	-504(%rbp), %rax
	movq	64(%rax), %rcx
	movq	-512(%rbp), %rax
	movzbl	(%rcx,%rax), %eax
.L3610:
	movzbl	%al, %esi
	leaq	.LC19(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3134:
	movq	-504(%rbp), %rax
	movq	-512(%rbp), %rcx
	leaq	-144(%rbp), %rdx
	leaq	-416(%rbp), %rdi
	leaq	_ZN2v88internal4wasmL16kAllWasmFeaturesE(%rip), %rsi
	addq	64(%rax), %rcx
	call	_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE0EEC1ERKNS1_12WasmFeaturesEPS3_PKh
	movl	-416(%rbp), %eax
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	addl	$1, %eax
	movl	%eax, -492(%rbp)
	cltq
	jmp	.L3137
.L3133:
	movq	-504(%rbp), %r15
	movq	-512(%rbp), %rcx
	movq	80(%r15), %rax
	leaq	16(%rax), %rsi
	movq	24(%rax), %rax
	movq	%rsi, %rdx
	testq	%rax, %rax
	jne	.L3146
	jmp	.L3145
	.p2align 4,,10
	.p2align 3
.L3956:
	movq	%rax, %rdx
	movq	16(%rax), %rax
.L3149:
	testq	%rax, %rax
	je	.L3147
.L3146:
	cmpq	32(%rax), %rcx
	jbe	.L3956
	movq	24(%rax), %rax
	jmp	.L3149
.L3129:
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3070:
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-191(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movups	%xmm0, -191(%rbp)
	testq	%rax, %rax
	movb	$1, -192(%rbp)
	sete	%al
	subq	$32, %rsp
	movzbl	%al, %eax
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm5
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L2989:
	leaq	-256(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-224(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movsd	-223(%rbp), %xmm0
	subsd	-255(%rbp), %xmm0
	movq	%r12, %rdi
	movq	$0, -183(%rbp)
	movb	$4, -192(%rbp)
	movq	%xmm0, -191(%rbp)
	ucomisd	%xmm0, %xmm0
	movdqa	-192(%rbp), %xmm5
	setp	%al
	orb	%al, 108(%r12)
	movzbl	-176(%rbp), %eax
	subq	$32, %rsp
	movups	%xmm5, (%rsp)
	movb	%al, 16(%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L2990:
	leaq	-256(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-224(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movsd	-255(%rbp), %xmm0
	addsd	-223(%rbp), %xmm0
	movq	%r12, %rdi
	movq	$0, -183(%rbp)
	movb	$4, -192(%rbp)
	movq	%xmm0, -191(%rbp)
	ucomisd	%xmm0, %xmm0
	movdqa	-192(%rbp), %xmm4
	setp	%al
	orb	%al, 108(%r12)
	movzbl	-176(%rbp), %eax
	subq	$32, %rsp
	movups	%xmm4, (%rsp)
	movb	%al, 16(%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L2991:
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movsd	-191(%rbp), %xmm0
	pxor	%xmm2, %xmm2
	ucomisd	%xmm0, %xmm2
	movapd	%xmm0, %xmm1
	sqrtsd	%xmm1, %xmm1
	ja	.L3957
.L3597:
	ucomisd	%xmm1, %xmm1
	movb	$4, -192(%rbp)
	movq	%r12, %rdi
	movq	$0, -183(%rbp)
	movq	%xmm1, -191(%rbp)
	movdqa	-192(%rbp), %xmm6
	setp	%al
	orb	%al, 108(%r12)
	movzbl	-176(%rbp), %eax
	subq	$32, %rsp
	movups	%xmm6, (%rsp)
	movb	%al, 16(%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L2992:
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movsd	-191(%rbp), %xmm0
	call	nearbyint@PLT
	movb	$4, -192(%rbp)
	movq	%r12, %rdi
	movq	$0, -183(%rbp)
	ucomisd	%xmm0, %xmm0
	movq	%xmm0, -191(%rbp)
	movdqa	-192(%rbp), %xmm5
	setp	%al
	orb	%al, 108(%r12)
	movzbl	-176(%rbp), %eax
	subq	$32, %rsp
	movups	%xmm5, (%rsp)
	movb	%al, 16(%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L2993:
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movsd	-191(%rbp), %xmm1
	movsd	.LC67(%rip), %xmm2
	movsd	.LC58(%rip), %xmm3
	andpd	%xmm1, %xmm2
	movapd	%xmm1, %xmm0
	ucomisd	%xmm2, %xmm3
	jbe	.L3550
	cvttsd2siq	%xmm1, %rax
	pxor	%xmm0, %xmm0
	movsd	.LC67(%rip), %xmm3
	andnpd	%xmm1, %xmm3
	cvtsi2sdq	%rax, %xmm0
	orpd	%xmm3, %xmm0
.L3550:
	ucomisd	%xmm0, %xmm0
	movb	$4, -192(%rbp)
	movq	%r12, %rdi
	movq	$0, -183(%rbp)
	movq	%xmm0, -191(%rbp)
	movdqa	-192(%rbp), %xmm4
	setp	%al
	orb	%al, 108(%r12)
	movzbl	-176(%rbp), %eax
	subq	$32, %rsp
	movups	%xmm4, (%rsp)
	movb	%al, 16(%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L2994:
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movsd	-191(%rbp), %xmm3
	movsd	.LC37(%rip), %xmm2
	movsd	.LC58(%rip), %xmm4
	movapd	%xmm3, %xmm1
	movapd	%xmm3, %xmm0
	andpd	%xmm2, %xmm1
	ucomisd	%xmm1, %xmm4
	jbe	.L3549
	cvttsd2siq	%xmm3, %rax
	pxor	%xmm1, %xmm1
	movsd	.LC59(%rip), %xmm4
	andnpd	%xmm3, %xmm2
	cvtsi2sdq	%rax, %xmm1
	movapd	%xmm1, %xmm7
	cmpnlesd	%xmm3, %xmm7
	movapd	%xmm7, %xmm0
	andpd	%xmm4, %xmm0
	subsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	orpd	%xmm2, %xmm0
.L3549:
	ucomisd	%xmm0, %xmm0
	movb	$4, -192(%rbp)
	movq	%r12, %rdi
	movq	$0, -183(%rbp)
	movq	%xmm0, -191(%rbp)
	movdqa	-192(%rbp), %xmm2
	setp	%al
	orb	%al, 108(%r12)
	movzbl	-176(%rbp), %eax
	subq	$32, %rsp
	movups	%xmm2, (%rsp)
	movb	%al, 16(%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L2995:
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movsd	-191(%rbp), %xmm3
	movsd	.LC37(%rip), %xmm2
	movsd	.LC58(%rip), %xmm4
	movapd	%xmm3, %xmm1
	movapd	%xmm3, %xmm0
	andpd	%xmm2, %xmm1
	ucomisd	%xmm1, %xmm4
	jbe	.L3548
	cvttsd2siq	%xmm3, %rax
	pxor	%xmm1, %xmm1
	movsd	.LC59(%rip), %xmm4
	andnpd	%xmm3, %xmm2
	cvtsi2sdq	%rax, %xmm1
	cmpnlesd	%xmm1, %xmm0
	andpd	%xmm4, %xmm0
	addsd	%xmm1, %xmm0
	orpd	%xmm2, %xmm0
.L3548:
	ucomisd	%xmm0, %xmm0
	movb	$4, -192(%rbp)
	movq	%r12, %rdi
	movq	$0, -183(%rbp)
	movq	%xmm0, -191(%rbp)
	movdqa	-192(%rbp), %xmm3
	setp	%al
	orb	%al, 108(%r12)
	movzbl	-176(%rbp), %eax
	subq	$32, %rsp
	movups	%xmm3, (%rsp)
	movb	%al, 16(%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L2996:
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movb	$4, -192(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	$0, -183(%rbp)
	movzbl	-176(%rbp), %eax
	btcq	$63, -191(%rbp)
	movdqa	-192(%rbp), %xmm7
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L2997:
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movb	$4, -192(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	$0, -183(%rbp)
	movzbl	-176(%rbp), %eax
	btrq	$63, -191(%rbp)
	movdqa	-192(%rbp), %xmm6
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L2998:
	leaq	-192(%rbp), %r14
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	%r12, %rsi
	movq	%r14, %rdi
	movl	-191(%rbp), %ebx
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	pxor	%xmm0, %xmm0
	subq	$32, %rsp
	movq	%r12, %rdi
	movl	-191(%rbp), %eax
	andl	$-2147483648, %ebx
	movups	%xmm0, -191(%rbp)
	movb	$3, -192(%rbp)
	andl	$2147483647, %eax
	orl	%ebx, %eax
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm3
	movb	%al, 16(%rsp)
	movups	%xmm3, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L2999:
	leaq	-192(%rbp), %r14
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-191(%rbp), %ebx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movl	%ebx, -536(%rbp)
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-191(%rbp), %eax
	movd	%eax, %xmm0
	ucomiss	%xmm0, %xmm0
	jp	.L3533
	movss	-536(%rbp), %xmm1
	ucomiss	%xmm1, %xmm1
	jp	.L3690
	testl	%eax, %eax
	sets	%cl
	testl	%ebx, %ebx
	sets	%dl
	cmpb	%dl, %cl
	jb	.L3533
	comiss	%xmm1, %xmm0
	maxss	%xmm1, %xmm0
	cmovbe	%ebx, %eax
.L3533:
	ucomiss	%xmm0, %xmm0
	pxor	%xmm0, %xmm0
	movb	$3, -192(%rbp)
	movq	%r12, %rdi
	movups	%xmm0, -191(%rbp)
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	setp	%dl
	orb	%dl, 108(%r12)
	movdqa	-192(%rbp), %xmm7
	subq	$32, %rsp
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3000:
	leaq	-192(%rbp), %r14
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-191(%rbp), %ebx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movl	%ebx, -536(%rbp)
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-191(%rbp), %eax
	movd	%eax, %xmm1
	ucomiss	%xmm1, %xmm1
	jp	.L3689
	movss	-536(%rbp), %xmm0
	ucomiss	%xmm0, %xmm0
	jp	.L3532
	testl	%eax, %eax
	movd	%xmm0, %edx
	sets	%cl
	testl	%edx, %edx
	sets	%dl
	cmpb	%dl, %cl
	jb	.L3532
	comiss	%xmm0, %xmm1
	minss	%xmm1, %xmm0
	cmovbe	%eax, %ebx
.L3532:
	ucomiss	%xmm0, %xmm0
	pxor	%xmm0, %xmm0
	movb	$3, -192(%rbp)
	movq	%r12, %rdi
	movups	%xmm0, -191(%rbp)
	movl	%ebx, -191(%rbp)
	movdqa	-192(%rbp), %xmm6
	setp	%al
	orb	%al, 108(%r12)
	movzbl	-176(%rbp), %eax
	subq	$32, %rsp
	movups	%xmm6, (%rsp)
	movb	%al, 16(%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3001:
	leaq	-256(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-224(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movss	-223(%rbp), %xmm0
	pxor	%xmm1, %xmm1
	movq	%r12, %rdi
	divss	-255(%rbp), %xmm0
	ucomiss	%xmm0, %xmm0
	movups	%xmm1, -191(%rbp)
	movb	$3, -192(%rbp)
	movd	%xmm0, -191(%rbp)
	movdqa	-192(%rbp), %xmm4
	setp	%al
	orb	%al, 108(%r12)
	movzbl	-176(%rbp), %eax
	subq	$32, %rsp
	movups	%xmm4, (%rsp)
	movb	%al, 16(%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3002:
	leaq	-256(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-224(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movss	-255(%rbp), %xmm0
	pxor	%xmm1, %xmm1
	movq	%r12, %rdi
	mulss	-223(%rbp), %xmm0
	movups	%xmm1, -191(%rbp)
	movb	$3, -192(%rbp)
	ucomiss	%xmm0, %xmm0
	movd	%xmm0, -191(%rbp)
	movdqa	-192(%rbp), %xmm7
	setp	%al
	orb	%al, 108(%r12)
	movzbl	-176(%rbp), %eax
	subq	$32, %rsp
	movups	%xmm7, (%rsp)
	movb	%al, 16(%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3003:
	leaq	-256(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-224(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movss	-223(%rbp), %xmm0
	pxor	%xmm1, %xmm1
	movq	%r12, %rdi
	subss	-255(%rbp), %xmm0
	movups	%xmm1, -191(%rbp)
	movb	$3, -192(%rbp)
	ucomiss	%xmm0, %xmm0
	movd	%xmm0, -191(%rbp)
	movdqa	-192(%rbp), %xmm7
	setp	%al
	orb	%al, 108(%r12)
	movzbl	-176(%rbp), %eax
	subq	$32, %rsp
	movups	%xmm7, (%rsp)
	movb	%al, 16(%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3004:
	leaq	-256(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-224(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movss	-255(%rbp), %xmm0
	pxor	%xmm1, %xmm1
	movq	%r12, %rdi
	addss	-223(%rbp), %xmm0
	movups	%xmm1, -191(%rbp)
	movb	$3, -192(%rbp)
	ucomiss	%xmm0, %xmm0
	movd	%xmm0, -191(%rbp)
	movdqa	-192(%rbp), %xmm6
	setp	%al
	orb	%al, 108(%r12)
	movzbl	-176(%rbp), %eax
	subq	$32, %rsp
	movups	%xmm6, (%rsp)
	movb	%al, 16(%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3005:
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movss	-191(%rbp), %xmm0
	pxor	%xmm2, %xmm2
	ucomiss	%xmm0, %xmm2
	movaps	%xmm0, %xmm1
	sqrtss	%xmm1, %xmm1
	ja	.L3958
.L3596:
	ucomiss	%xmm1, %xmm1
	pxor	%xmm0, %xmm0
	movb	$3, -192(%rbp)
	movq	%r12, %rdi
	movups	%xmm0, -191(%rbp)
	movd	%xmm1, -191(%rbp)
	movdqa	-192(%rbp), %xmm5
	setp	%al
	orb	%al, 108(%r12)
	movzbl	-176(%rbp), %eax
	subq	$32, %rsp
	movups	%xmm5, (%rsp)
	movb	%al, 16(%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3006:
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movss	-191(%rbp), %xmm0
	call	nearbyintf@PLT
	pxor	%xmm1, %xmm1
	movb	$3, -192(%rbp)
	movq	%r12, %rdi
	ucomiss	%xmm0, %xmm0
	movups	%xmm1, -191(%rbp)
	movd	%xmm0, -191(%rbp)
	movdqa	-192(%rbp), %xmm5
	setp	%al
	orb	%al, 108(%r12)
	movzbl	-176(%rbp), %eax
	subq	$32, %rsp
	movups	%xmm5, (%rsp)
	movb	%al, 16(%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3127:
	movq	-504(%rbp), %rdx
	movq	-512(%rbp), %rax
	addq	64(%rdx), %rax
	movzbl	1(%rax), %ecx
	movl	%ecx, %edx
	andl	$127, %edx
	testb	%cl, %cl
	js	.L3959
	movl	$1, -412(%rbp)
.L3201:
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	movl	%edx, -416(%rbp)
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-191(%rbp), %ebx
	testl	%ebx, %ebx
	jne	.L3960
	movl	-412(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -492(%rbp)
.L3211:
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	cltq
	jmp	.L3137
.L3128:
	movq	-504(%rbp), %rax
	movq	-512(%rbp), %rdx
	movq	64(%rax), %rsi
	addq	%rdx, %rsi
	movzbl	1(%rsi), %ecx
	movl	%ecx, %r8d
	andl	$127, %r8d
	testb	%cl, %cl
	js	.L3961
	movl	$1, -412(%rbp)
.L3191:
	movl	%r8d, -416(%rbp)
	movq	80(%rax), %rax
	leaq	16(%rax), %rbx
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L3192
	movq	%rbx, %rcx
	jmp	.L3193
	.p2align 4,,10
	.p2align 3
.L3962:
	movq	%rax, %rcx
	movq	16(%rax), %rax
.L3196:
	testq	%rax, %rax
	je	.L3194
.L3193:
	cmpq	%rdx, 32(%rax)
	jnb	.L3962
	movq	24(%rax), %rax
	jmp	.L3196
.L3073:
	leaq	-256(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-224(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-223(%rbp), %eax
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	cmpl	%eax, -255(%rbp)
	movups	%xmm0, -191(%rbp)
	setnb	%al
	movb	$1, -192(%rbp)
	subq	$32, %rsp
	movzbl	%al, %eax
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm3
	movb	%al, 16(%rsp)
	movups	%xmm3, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3074:
	leaq	-256(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-224(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-223(%rbp), %eax
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	cmpl	%eax, -255(%rbp)
	movups	%xmm0, -191(%rbp)
	setge	%al
	movb	$1, -192(%rbp)
	subq	$32, %rsp
	movzbl	%al, %eax
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm7
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3075:
	leaq	-256(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-224(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-223(%rbp), %eax
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	cmpl	%eax, -255(%rbp)
	movups	%xmm0, -191(%rbp)
	setb	%al
	movb	$1, -192(%rbp)
	subq	$32, %rsp
	movzbl	%al, %eax
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm4
	movb	%al, 16(%rsp)
	movups	%xmm4, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3076:
	leaq	-256(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-224(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-223(%rbp), %eax
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	cmpl	%eax, -255(%rbp)
	movups	%xmm0, -191(%rbp)
	setl	%al
	movb	$1, -192(%rbp)
	subq	$32, %rsp
	movzbl	%al, %eax
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm3
	movb	%al, 16(%rsp)
	movups	%xmm3, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3077:
	leaq	-256(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-224(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-223(%rbp), %eax
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	cmpl	%eax, -255(%rbp)
	movups	%xmm0, -191(%rbp)
	seta	%al
	movb	$1, -192(%rbp)
	subq	$32, %rsp
	movzbl	%al, %eax
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm7
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3078:
	leaq	-256(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-224(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-223(%rbp), %eax
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	cmpl	%eax, -255(%rbp)
	movups	%xmm0, -191(%rbp)
	setg	%al
	movb	$1, -192(%rbp)
	subq	$32, %rsp
	movzbl	%al, %eax
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm6
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3079:
	leaq	-256(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-224(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-223(%rbp), %eax
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	cmpl	%eax, -255(%rbp)
	movups	%xmm0, -191(%rbp)
	setne	%al
	movb	$1, -192(%rbp)
	subq	$32, %rsp
	movzbl	%al, %eax
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm6
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3080:
	leaq	-256(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-224(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-223(%rbp), %eax
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	cmpl	%eax, -255(%rbp)
	movups	%xmm0, -191(%rbp)
	sete	%al
	movb	$1, -192(%rbp)
	subq	$32, %rsp
	movzbl	%al, %eax
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm5
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3081:
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-191(%rbp), %eax
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movups	%xmm0, -191(%rbp)
	testl	%eax, %eax
	movb	$1, -192(%rbp)
	sete	%al
	subq	$32, %rsp
	movzbl	%al, %eax
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm7
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3082:
	movq	-504(%rbp), %rax
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	64(%rax), %rdx
	movq	-512(%rbp), %rax
	movq	1(%rdx,%rax), %rax
	movb	$4, -192(%rbp)
	movq	$0, -183(%rbp)
	movq	%rax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm7
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movl	$9, -492(%rbp)
	movq	-504(%rbp), %r15
	movl	$9, %eax
	jmp	.L3137
.L3083:
	movq	-504(%rbp), %rax
	pxor	%xmm0, %xmm0
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	64(%rax), %rdx
	movq	-512(%rbp), %rax
	movl	1(%rdx,%rax), %eax
	movups	%xmm0, -191(%rbp)
	movb	$3, -192(%rbp)
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm6
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movl	$5, -492(%rbp)
	movq	-504(%rbp), %r15
	movl	$5, %eax
	jmp	.L3137
.L3084:
	movq	-504(%rbp), %rax
	movq	64(%rax), %rdx
	movq	-512(%rbp), %rax
	leaq	1(%rdx,%rax), %rcx
	movzbl	(%rcx), %eax
	movl	%eax, %edx
	andl	$127, %edx
	testb	%al, %al
	js	.L3963
	salq	$57, %rdx
	movl	$2, %eax
	movl	$2, %ebx
	sarq	$57, %rdx
.L3237:
	movq	%rdx, -191(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	$0, -183(%rbp)
	movzbl	-176(%rbp), %edx
	movb	$2, -192(%rbp)
	movdqa	-192(%rbp), %xmm7
	movq	%rax, -536(%rbp)
	movb	%dl, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movl	%ebx, -492(%rbp)
	addq	$32, %rsp
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	movq	-536(%rbp), %rax
	jmp	.L3137
.L3085:
	movq	-504(%rbp), %rax
	movq	64(%rax), %rdx
	movq	-512(%rbp), %rax
	leaq	1(%rdx,%rax), %rcx
	movzbl	(%rcx), %eax
	movl	%eax, %edx
	andl	$127, %edx
	testb	%al, %al
	js	.L3964
	sall	$25, %edx
	movl	$2, %eax
	movl	$2, %ebx
	sarl	$25, %edx
.L3227:
	pxor	%xmm0, %xmm0
	movb	$1, -192(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movups	%xmm0, -191(%rbp)
	movl	%edx, -191(%rbp)
	movzbl	-176(%rbp), %edx
	movdqa	-192(%rbp), %xmm6
	movq	%rax, -536(%rbp)
	movb	%dl, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movl	%ebx, -492(%rbp)
	addq	$32, %rsp
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	movq	-536(%rbp), %rax
	jmp	.L3137
.L3086:
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	8(%r12), %r15
	movl	-191(%rbp), %edx
	addl	$1, 41104(%r15)
	movq	8(%r12), %rcx
	movq	16(%r12), %rax
	movq	41088(%r15), %r14
	movq	41112(%rcx), %rdi
	movq	41096(%r15), %rbx
	movq	(%rax), %rax
	movq	159(%rax), %r8
	testq	%rdi, %rdi
	je	.L3515
	movq	%r8, %rsi
	movl	%edx, -536(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-536(%rbp), %edx
	movq	%rax, %rsi
.L3516:
	movq	8(%r12), %rdi
	call	_ZN2v88internal16WasmMemoryObject4GrowEPNS0_7IsolateENS0_6HandleIS1_EEj@PLT
	pxor	%xmm0, %xmm0
	subq	$32, %rsp
	movq	%r12, %rdi
	movups	%xmm0, -191(%rbp)
	movb	$1, -192(%rbp)
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm7
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	addq	$32, %rsp
	movl	$2, -492(%rbp)
	testl	%r13d, %r13d
	jle	.L3518
	subl	$1000, %r13d
	movl	$0, %eax
	cmovs	%eax, %r13d
.L3518:
	subl	$1, 41104(%r15)
	movl	$2, %eax
	movq	%r14, 41088(%r15)
	cmpq	41096(%r15), %rbx
	je	.L3636
	movq	%rbx, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movslq	-492(%rbp), %rax
.L3636:
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	jmp	.L3137
.L2957:
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	pxor	%xmm0, %xmm0
	subq	$32, %rsp
	movq	%r12, %rdi
	movl	-191(%rbp), %eax
	movb	$1, -192(%rbp)
	movups	%xmm0, -191(%rbp)
	cwtl
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm4
	movb	%al, 16(%rsp)
	movups	%xmm4, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L2958:
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	pxor	%xmm0, %xmm0
	subq	$32, %rsp
	movq	%r12, %rdi
	movl	-191(%rbp), %eax
	movb	$1, -192(%rbp)
	movups	%xmm0, -191(%rbp)
	movsbl	%al, %eax
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm7
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L2959:
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movb	$4, -192(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	$0, -183(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm6
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L2960:
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	pxor	%xmm0, %xmm0
	subq	$32, %rsp
	movq	%r12, %rdi
	movl	-191(%rbp), %eax
	movb	$3, -192(%rbp)
	movups	%xmm0, -191(%rbp)
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm6
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L2961:
	leaq	-224(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-223(%rbp), %rax
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	$0, -183(%rbp)
	movq	%rax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movb	$2, -192(%rbp)
	movdqa	-192(%rbp), %xmm6
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L2962:
	leaq	-224(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	pxor	%xmm0, %xmm0
	subq	$32, %rsp
	movq	%r12, %rdi
	movl	-223(%rbp), %eax
	movups	%xmm0, -191(%rbp)
	movb	$1, -192(%rbp)
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm5
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L2963:
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	pxor	%xmm0, %xmm0
	movb	$4, -192(%rbp)
	movq	%r12, %rdi
	cvtss2sd	-191(%rbp), %xmm0
	ucomisd	%xmm0, %xmm0
	movq	$0, -183(%rbp)
	movq	%xmm0, -191(%rbp)
	movdqa	-192(%rbp), %xmm5
	setp	%al
	orb	%al, 108(%r12)
	movzbl	-176(%rbp), %eax
	subq	$32, %rsp
	movups	%xmm5, (%rsp)
	movb	%al, 16(%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L2964:
	movq	%r12, %rsi
	leaq	-192(%rbp), %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-191(%rbp), %rax
	leaq	-64(%rbp), %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal4wasm25uint64_to_float64_wrapperEm@PLT
	movq	-64(%rbp), %rax
	movb	$4, -192(%rbp)
	movq	%r12, %rdi
	movq	$0, -183(%rbp)
	movq	%rax, %xmm0
	movq	%rax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	ucomisd	%xmm0, %xmm0
	movdqa	-192(%rbp), %xmm4
	setp	%dl
	orb	%dl, 108(%r12)
	subq	$32, %rsp
	movb	%al, 16(%rsp)
	movups	%xmm4, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L2965:
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	pxor	%xmm0, %xmm0
	movb	$4, -192(%rbp)
	movq	%r12, %rdi
	cvtsi2sdq	-191(%rbp), %xmm0
	movq	$0, -183(%rbp)
	movq	%xmm0, -191(%rbp)
	ucomisd	%xmm0, %xmm0
	movdqa	-192(%rbp), %xmm2
	setp	%al
	orb	%al, 108(%r12)
	movzbl	-176(%rbp), %eax
	subq	$32, %rsp
	movups	%xmm2, (%rsp)
	movb	%al, 16(%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L2966:
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	pxor	%xmm0, %xmm0
	subq	$32, %rsp
	movq	%r12, %rdi
	movl	-191(%rbp), %eax
	movb	$4, -192(%rbp)
	movq	$0, -183(%rbp)
	cvtsi2sdq	%rax, %xmm0
	movzbl	-176(%rbp), %eax
	movb	%al, 16(%rsp)
	movq	%xmm0, -191(%rbp)
	movdqa	-192(%rbp), %xmm3
	movups	%xmm3, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L2967:
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	pxor	%xmm0, %xmm0
	subq	$32, %rsp
	movq	%r12, %rdi
	cvtsi2sdl	-191(%rbp), %xmm0
	movb	$4, -192(%rbp)
	movq	$0, -183(%rbp)
	movzbl	-176(%rbp), %eax
	movb	%al, 16(%rsp)
	movq	%xmm0, -191(%rbp)
	movdqa	-192(%rbp), %xmm7
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L2968:
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movsd	-191(%rbp), %xmm0
	comisd	.LC60(%rip), %xmm0
	jbe	.L3936
	movsd	.LC61(%rip), %xmm1
	xorl	%edx, %edx
	comisd	%xmm0, %xmm1
	sbbl	%eax, %eax
	notl	%eax
	addl	$2139095040, %eax
.L3559:
	orb	%dl, 108(%r12)
	pxor	%xmm0, %xmm0
	subq	$32, %rsp
	movq	%r12, %rdi
	movups	%xmm0, -191(%rbp)
	movb	$3, -192(%rbp)
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm5
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L2969:
	movq	%r12, %rsi
	leaq	-192(%rbp), %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-191(%rbp), %rax
	leaq	-64(%rbp), %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal4wasm25uint64_to_float32_wrapperEm@PLT
	movss	-64(%rbp), %xmm0
	movb	$3, -192(%rbp)
	movq	%r12, %rdi
	ucomiss	%xmm0, %xmm0
	pxor	%xmm0, %xmm0
	movups	%xmm0, -191(%rbp)
	setp	%al
	orb	%al, 108(%r12)
	movl	-64(%rbp), %eax
	subq	$32, %rsp
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm4
	movb	%al, 16(%rsp)
	movups	%xmm4, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L2971:
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	pxor	%xmm0, %xmm0
	pxor	%xmm1, %xmm1
	movq	%r12, %rdi
	movl	-191(%rbp), %eax
	movb	$3, -192(%rbp)
	movups	%xmm1, -191(%rbp)
	cvtsi2ssq	%rax, %xmm0
	ucomiss	%xmm0, %xmm0
	movd	%xmm0, -191(%rbp)
	movdqa	-192(%rbp), %xmm3
	setp	%al
	orb	%al, 108(%r12)
	movzbl	-176(%rbp), %eax
	subq	$32, %rsp
	movups	%xmm3, (%rsp)
	movb	%al, 16(%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L2972:
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	pxor	%xmm0, %xmm0
	pxor	%xmm1, %xmm1
	movq	%r12, %rdi
	cvtsi2ssl	-191(%rbp), %xmm0
	movb	$3, -192(%rbp)
	movups	%xmm1, -191(%rbp)
	ucomiss	%xmm0, %xmm0
	movd	%xmm0, -191(%rbp)
	movdqa	-192(%rbp), %xmm7
	setp	%al
	orb	%al, 108(%r12)
	movzbl	-176(%rbp), %eax
	subq	$32, %rsp
	movups	%xmm7, (%rsp)
	movb	%al, 16(%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L2970:
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	pxor	%xmm0, %xmm0
	pxor	%xmm1, %xmm1
	movq	%r12, %rdi
	cvtsi2ssq	-191(%rbp), %xmm0
	movb	$3, -192(%rbp)
	movups	%xmm1, -191(%rbp)
	ucomiss	%xmm0, %xmm0
	movd	%xmm0, -191(%rbp)
	movdqa	-192(%rbp), %xmm2
	setp	%al
	orb	%al, 108(%r12)
	movzbl	-176(%rbp), %eax
	subq	$32, %rsp
	movups	%xmm2, (%rsp)
	movb	%al, 16(%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L2941:
	movq	%r12, %rsi
	leaq	-192(%rbp), %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	16(%r12), %rdx
	movl	-191(%rbp), %eax
	movl	$2143289344, %esi
	movq	(%rdx), %rdx
	movq	31(%rdx), %rcx
	cmpq	$3, %rcx
	jbe	.L3508
	subq	$4, %rcx
	cmpq	%rcx, %rax
	ja	.L3508
	andq	39(%rdx), %rax
	addq	23(%rdx), %rax
	je	.L3508
	movl	(%rax), %esi
.L3508:
	pxor	%xmm0, %xmm0
	movb	$3, -192(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movups	%xmm0, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movl	%esi, -191(%rbp)
	movdqa	-192(%rbp), %xmm7
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L2942:
	movq	%r12, %rsi
	leaq	-192(%rbp), %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	16(%r12), %rdx
	movl	-191(%rbp), %eax
	xorl	%esi, %esi
	movq	(%rdx), %rdx
	movq	31(%rdx), %rcx
	cmpq	$3, %rcx
	jbe	.L3507
	subq	$4, %rcx
	cmpq	%rcx, %rax
	ja	.L3507
	andq	39(%rdx), %rax
	addq	23(%rdx), %rax
	je	.L3507
	movl	(%rax), %esi
.L3507:
	pxor	%xmm0, %xmm0
	movb	$1, -192(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movups	%xmm0, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movl	%esi, -191(%rbp)
	movdqa	-192(%rbp), %xmm6
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L2943:
	movq	%r12, %rsi
	leaq	-192(%rbp), %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	16(%r12), %rdx
	movl	-191(%rbp), %eax
	xorl	%esi, %esi
	movq	(%rdx), %rdx
	movq	31(%rdx), %rcx
	cmpq	$1, %rcx
	jbe	.L3506
	subq	$2, %rcx
	cmpq	%rcx, %rax
	ja	.L3506
	andq	39(%rdx), %rax
	addq	23(%rdx), %rax
	je	.L3506
	movzwl	(%rax), %esi
.L3506:
	pxor	%xmm0, %xmm0
	movb	$1, -192(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movups	%xmm0, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movl	%esi, -191(%rbp)
	movdqa	-192(%rbp), %xmm5
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L2944:
	movq	%r12, %rsi
	leaq	-192(%rbp), %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	16(%r12), %rdx
	movl	-191(%rbp), %eax
	xorl	%esi, %esi
	movq	(%rdx), %rdx
	movq	31(%rdx), %rcx
	cmpq	$1, %rcx
	jbe	.L3505
	subq	$2, %rcx
	cmpq	%rcx, %rax
	ja	.L3505
	andq	39(%rdx), %rax
	addq	23(%rdx), %rax
	je	.L3505
	movswl	(%rax), %esi
.L3505:
	pxor	%xmm0, %xmm0
	movb	$1, -192(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movups	%xmm0, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movl	%esi, -191(%rbp)
	movdqa	-192(%rbp), %xmm7
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3136:
	movq	72(%r12), %rax
	movq	-512(%rbp), %rdx
	movl	$4, 88(%r12)
	movl	$0, 104(%r12)
	movq	%rdx, -16(%rax)
	jmp	.L3947
.L3132:
	movq	-504(%rbp), %rdx
	movq	-512(%rbp), %rax
	movq	64(%rdx), %rsi
	movq	$0, -408(%rbp)
	addq	%rax, %rsi
	movzbl	1(%rsi), %ecx
	movl	%ecx, %edx
	andl	$127, %edx
	testb	%cl, %cl
	js	.L3965
	movl	$1, -400(%rbp)
.L3151:
	movl	%edx, -416(%rbp)
	movq	72(%r12), %rdx
	movq	%r12, %rdi
	movq	%rax, -16(%rdx)
	movq	(%r12), %rax
	movl	-416(%rbp), %ecx
	movq	8(%rax), %rax
	movq	%rcx, %rdx
	movq	256(%rax), %rax
	leaq	(%rax,%rcx,8), %rsi
	call	_ZN2v88internal4wasm10ThreadImpl16DoThrowExceptionEPKNS1_13WasmExceptionEj
	testb	%al, %al
	je	.L3947
.L3326:
	movq	72(%r12), %rax
	movq	-24(%rax), %rdx
	movq	%rdx, -504(%rbp)
	movq	-16(%rax), %rdx
	movq	%rdx, -512(%rbp)
	movq	-24(%rax), %rcx
	movq	72(%rcx), %rdx
	subq	64(%rcx), %rdx
	movq	%rdx, -488(%rbp)
	movq	-24(%rax), %rax
	movq	72(%rax), %rdx
	movdqu	64(%rax), %xmm0
	leaq	-168(%rbp), %rax
	movl	$0, -112(%rbp)
	movq	%rax, -184(%rbp)
	movq	%rdx, -120(%rbp)
	movq	-96(%rbp), %rdx
	punpcklqdq	%xmm0, %xmm0
	movl	$0, -188(%rbp)
	movl	$0, -192(%rbp)
	movb	$0, -168(%rbp)
	movl	$0, -104(%rbp)
	movq	$0, -88(%rbp)
	movups	%xmm0, -136(%rbp)
	movb	$0, (%rdx)
	movq	-184(%rbp), %rdx
	movq	$0, -176(%rbp)
	movb	$0, (%rdx)
	movq	-184(%rbp), %rdi
	cmpq	%rax, %rdi
	je	.L3328
	call	_ZdlPv@PLT
.L3328:
	movq	-504(%rbp), %r15
.L3155:
	movq	-512(%rbp), %rax
	jmp	.L3612
.L3130:
	movq	-504(%rbp), %rdx
	movq	-512(%rbp), %rax
	addq	64(%rdx), %rax
	cmpb	$0, 1(%rax)
	movzbl	2(%rax), %ecx
	js	.L3159
	movl	$2, -544(%rbp)
	leaq	1(%rax), %rsi
.L3160:
	movl	$1, -536(%rbp)
	movl	%ecx, %ebx
	andl	$127, %ebx
	testb	%cl, %cl
	js	.L3966
.L3164:
	movq	8(%r12), %r15
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	movq	41088(%r15), %rax
	addl	$1, 41104(%r15)
	movq	%rax, -560(%rbp)
	movq	41096(%r15), %rax
	movq	%rax, -552(%rbp)
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-191(%rbp), %r14
	movq	8(%r12), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal20WasmExceptionPackage15GetExceptionTagEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE@PLT
	movq	8(%r12), %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rsi
	leal	16(,%rbx,8), %eax
	cltq
	movq	223(%rsi), %rsi
	movq	-1(%rax,%rsi), %rsi
	movq	41112(%rdi), %r9
	testq	%r9, %r9
	je	.L3165
	movq	%r9, %rdi
	movq	%rcx, -568(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-568(%rbp), %rcx
	cmpq	%rcx, %rax
	je	.L3167
	testq	%rax, %rax
	je	.L3171
.L3170:
	testq	%rcx, %rcx
	je	.L3171
	movq	(%rcx), %rsi
	cmpq	%rsi, (%rax)
	jne	.L3171
.L3167:
	movq	(%r12), %rax
	movl	%ebx, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rax
	movq	256(%rax), %rax
	leaq	(%rax,%rdx,8), %rsi
	movq	%r14, %rdx
	call	_ZN2v88internal4wasm10ThreadImpl17DoUnpackExceptionEPKNS1_13WasmExceptionENS0_6HandleINS0_6ObjectEEE
	movq	-504(%rbp), %rax
	movq	-512(%rbp), %rdx
	movq	80(%rax), %rax
	leaq	16(%rax), %r14
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L3173
	movq	%r14, %rcx
	jmp	.L3174
.L3967:
	movq	%rax, %rcx
	movq	16(%rax), %rax
.L3177:
	testq	%rax, %rax
	je	.L3175
.L3174:
	cmpq	32(%rax), %rdx
	jbe	.L3967
	movq	24(%rax), %rax
	jmp	.L3177
.L3131:
	movq	8(%r12), %r15
	leaq	-224(%rbp), %rdi
	movq	%r12, %rsi
	addl	$1, 41104(%r15)
	movq	41088(%r15), %r14
	movq	41096(%r15), %rbx
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	72(%r12), %rax
	movq	-512(%rbp), %rdx
	movq	%rdx, -16(%rax)
	movq	-223(%rbp), %rax
	movq	8(%r12), %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate7ReThrowENS0_6ObjectE@PLT
	movq	8(%r12), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl15HandleExceptionEPNS0_7IsolateE
	testl	%eax, %eax
	je	.L3156
	subl	$1, 41104(%r15)
	movq	%r14, 41088(%r15)
	cmpq	41096(%r15), %rbx
	je	.L3947
	movq	%rbx, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	jmp	.L3947
.L2927:
	movq	-512(%rbp), %r8
	movl	%r14d, %esi
	movq	%r12, %rdi
	movq	-504(%rbp), %rcx
	leaq	-144(%rbp), %rdx
	leaq	-492(%rbp), %r9
	call	_ZN2v88internal4wasm10ThreadImpl15ExecuteAtomicOpENS1_10WasmOpcodeEPNS1_7DecoderEPNS1_15InterpreterCodeEmPi
	testb	%al, %al
	je	.L3947
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	jmp	.L3137
.L2933:
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movss	-191(%rbp), %xmm0
	pxor	%xmm1, %xmm1
	movsd	.LC64(%rip), %xmm3
	cvtss2sd	%xmm0, %xmm1
	movapd	%xmm1, %xmm2
	andpd	.LC37(%rip), %xmm2
	ucomisd	%xmm2, %xmm3
	jb	.L3572
	movsd	.LC65(%rip), %xmm2
	comisd	%xmm1, %xmm2
	jb	.L3572
	comiss	.LC9(%rip), %xmm0
	jb	.L3572
	cvttss2sil	%xmm0, %eax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	ucomisd	%xmm0, %xmm1
	jnp	.L3968
.L3572:
	movabsq	$9218868437227405312, %rax
	movq	%xmm1, %rdx
	testq	%rax, %rdx
	je	.L3706
	movq	%xmm1, %rsi
	xorl	%eax, %eax
	shrq	$52, %rsi
	andl	$2047, %esi
	movl	%esi, %ecx
	subl	$1075, %ecx
	js	.L3969
	cmpl	$31, %ecx
	jg	.L3576
	movabsq	$4503599627370495, %rax
	movabsq	$4503599627370496, %rsi
	andq	%rdx, %rax
	addq	%rsi, %rax
	salq	%cl, %rax
	movl	%eax, %eax
.L3578:
	sarq	$63, %rdx
	orl	$1, %edx
	imull	%edx, %eax
.L3576:
	pxor	%xmm0, %xmm0
	movb	$1, -192(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movups	%xmm0, -191(%rbp)
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm3
	movb	%al, 16(%rsp)
	movups	%xmm3, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L2934:
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movss	-191(%rbp), %xmm0
	pxor	%xmm1, %xmm1
	movsd	.LC64(%rip), %xmm3
	cvtss2sd	%xmm0, %xmm1
	movapd	%xmm1, %xmm2
	andpd	.LC37(%rip), %xmm2
	ucomisd	%xmm2, %xmm3
	jb	.L3564
	movsd	.LC65(%rip), %xmm2
	comisd	%xmm1, %xmm2
	jb	.L3564
	comiss	.LC9(%rip), %xmm0
	jb	.L3564
	cvttss2sil	%xmm0, %eax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	ucomisd	%xmm0, %xmm1
	jnp	.L3970
.L3564:
	movabsq	$9218868437227405312, %rax
	movq	%xmm1, %rdx
	testq	%rax, %rdx
	je	.L3702
	movq	%xmm1, %rsi
	xorl	%eax, %eax
	shrq	$52, %rsi
	andl	$2047, %esi
	movl	%esi, %ecx
	subl	$1075, %ecx
	js	.L3971
	cmpl	$31, %ecx
	jg	.L3568
	movabsq	$4503599627370495, %rax
	movabsq	$4503599627370496, %rsi
	andq	%rdx, %rax
	addq	%rsi, %rax
	salq	%cl, %rax
	movl	%eax, %eax
.L3570:
	sarq	$63, %rdx
	orl	$1, %edx
	imull	%edx, %eax
.L3568:
	pxor	%xmm0, %xmm0
	movb	$1, -192(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movups	%xmm0, -191(%rbp)
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm7
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L2935:
	leaq	-224(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	16(%r12), %rdx
	movl	-191(%rbp), %eax
	movq	(%rdx), %rdx
	movq	31(%rdx), %rcx
	cmpq	$7, %rcx
	jbe	.L3514
	subq	$8, %rcx
	cmpq	%rcx, %rax
	ja	.L3514
	andq	39(%rdx), %rax
	addq	23(%rdx), %rax
	je	.L3514
	movsd	-223(%rbp), %xmm5
	movsd	%xmm5, (%rax)
.L3514:
	movzbl	-208(%rbp), %eax
	movdqa	-224(%rbp), %xmm6
	subq	$32, %rsp
	movq	%r12, %rdi
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L2936:
	leaq	-224(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	16(%r12), %rdx
	movl	-191(%rbp), %eax
	movq	(%rdx), %rdx
	movq	31(%rdx), %rcx
	cmpq	$3, %rcx
	jbe	.L3513
	subq	$4, %rcx
	cmpq	%rcx, %rax
	ja	.L3513
	andq	39(%rdx), %rax
	addq	23(%rdx), %rax
	je	.L3513
	movss	-223(%rbp), %xmm4
	movss	%xmm4, (%rax)
.L3513:
	movzbl	-208(%rbp), %eax
	movdqa	-224(%rbp), %xmm5
	subq	$32, %rsp
	movq	%r12, %rdi
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L2937:
	leaq	-224(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	16(%r12), %rdx
	movl	-191(%rbp), %eax
	movq	(%rdx), %rdx
	movq	31(%rdx), %rcx
	cmpq	$3, %rcx
	jbe	.L3512
	subq	$4, %rcx
	cmpq	%rcx, %rax
	ja	.L3512
	andq	39(%rdx), %rax
	addq	23(%rdx), %rax
	je	.L3512
	movl	-223(%rbp), %edx
	movl	%edx, (%rax)
.L3512:
	movzbl	-208(%rbp), %eax
	movdqa	-224(%rbp), %xmm4
	subq	$32, %rsp
	movq	%r12, %rdi
	movb	%al, 16(%rsp)
	movups	%xmm4, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L2938:
	leaq	-224(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	16(%r12), %rdx
	movl	-191(%rbp), %eax
	movq	(%rdx), %rdx
	movq	31(%rdx), %rcx
	cmpq	$1, %rcx
	jbe	.L3511
	subq	$2, %rcx
	cmpq	%rcx, %rax
	ja	.L3511
	andq	39(%rdx), %rax
	addq	23(%rdx), %rax
	je	.L3511
	movl	-223(%rbp), %edx
	movw	%dx, (%rax)
.L3511:
	movzbl	-208(%rbp), %eax
	movdqa	-224(%rbp), %xmm7
	subq	$32, %rsp
	movq	%r12, %rdi
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L2939:
	leaq	-224(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	16(%r12), %rdx
	movl	-191(%rbp), %eax
	movq	(%rdx), %rdx
	movq	31(%rdx), %rcx
	testq	%rcx, %rcx
	je	.L3510
	subq	$1, %rcx
	cmpq	%rcx, %rax
	ja	.L3510
	andq	39(%rdx), %rax
	addq	23(%rdx), %rax
	je	.L3510
	movl	-223(%rbp), %edx
	movb	%dl, (%rax)
.L3510:
	movzbl	-208(%rbp), %eax
	movdqa	-224(%rbp), %xmm6
	subq	$32, %rsp
	movq	%r12, %rdi
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L2940:
	movq	%r12, %rsi
	leaq	-192(%rbp), %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	16(%r12), %rdx
	movl	-191(%rbp), %eax
	movabsq	$9221120237041090560, %rsi
	movq	(%rdx), %rdx
	movq	31(%rdx), %rcx
	cmpq	$7, %rcx
	jbe	.L3509
	subq	$8, %rcx
	cmpq	%rcx, %rax
	ja	.L3509
	andq	39(%rdx), %rax
	addq	23(%rdx), %rax
	je	.L3509
	movq	(%rax), %rsi
.L3509:
	movb	$4, -192(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	$0, -183(%rbp)
	movzbl	-176(%rbp), %eax
	movq	%rsi, -191(%rbp)
	movdqa	-192(%rbp), %xmm5
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L2929:
	movq	-512(%rbp), %r8
	movl	%r14d, %esi
	movq	%r12, %rdi
	movq	-504(%rbp), %rcx
	leaq	-144(%rbp), %rdx
	leaq	-492(%rbp), %r9
	addl	$1, -492(%rbp)
	call	_ZN2v88internal4wasm10ThreadImpl13ExecuteSimdOpENS1_10WasmOpcodeEPNS1_7DecoderEPNS1_15InterpreterCodeEmPi
	testb	%al, %al
	je	.L3947
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	jmp	.L3137
.L2930:
	movq	-512(%rbp), %r8
	movl	%r14d, %esi
	movq	%r12, %rdi
	movq	-504(%rbp), %rcx
	leaq	-144(%rbp), %rdx
	leaq	-492(%rbp), %r9
	addl	$1, -492(%rbp)
	call	_ZN2v88internal4wasm10ThreadImpl16ExecuteNumericOpENS1_10WasmOpcodeEPNS1_7DecoderEPNS1_15InterpreterCodeEmPi
	testb	%al, %al
	je	.L3947
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	jmp	.L3137
.L2931:
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-191(%rbp), %rdx
	movsd	.LC64(%rip), %xmm2
	movq	%rdx, %xmm1
	andpd	.LC37(%rip), %xmm1
	movq	%rdx, %xmm0
	ucomisd	%xmm1, %xmm2
	jb	.L3588
	movsd	.LC65(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L3588
	comisd	.LC66(%rip), %xmm0
	jb	.L3588
	cvttsd2sil	%xmm0, %eax
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	ucomisd	%xmm1, %xmm0
	jnp	.L3972
.L3588:
	movabsq	$9218868437227405312, %rax
	testq	%rax, %rdx
	je	.L3714
	movq	%rdx, %rsi
	xorl	%eax, %eax
	shrq	$52, %rsi
	andl	$2047, %esi
	movl	%esi, %ecx
	subl	$1075, %ecx
	js	.L3973
	cmpl	$31, %ecx
	jg	.L3592
	movabsq	$4503599627370495, %rax
	movabsq	$4503599627370496, %rsi
	andq	%rdx, %rax
	addq	%rsi, %rax
	salq	%cl, %rax
	movl	%eax, %eax
.L3594:
	sarq	$63, %rdx
	orl	$1, %edx
	imull	%edx, %eax
.L3592:
	pxor	%xmm0, %xmm0
	movb	$1, -192(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movups	%xmm0, -191(%rbp)
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm4
	movb	%al, 16(%rsp)
	movups	%xmm4, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L2932:
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-191(%rbp), %rdx
	movsd	.LC64(%rip), %xmm2
	movq	%rdx, %xmm1
	andpd	.LC37(%rip), %xmm1
	movq	%rdx, %xmm0
	ucomisd	%xmm1, %xmm2
	jb	.L3580
	movsd	.LC65(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L3580
	comisd	.LC66(%rip), %xmm0
	jb	.L3580
	cvttsd2sil	%xmm0, %eax
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	ucomisd	%xmm1, %xmm0
	jnp	.L3974
.L3580:
	movabsq	$9218868437227405312, %rax
	testq	%rax, %rdx
	je	.L3710
	movq	%rdx, %rsi
	xorl	%eax, %eax
	shrq	$52, %rsi
	andl	$2047, %esi
	movl	%esi, %ecx
	subl	$1075, %ecx
	js	.L3975
	cmpl	$31, %ecx
	jg	.L3584
	movabsq	$4503599627370495, %rax
	movabsq	$4503599627370496, %rsi
	andq	%rdx, %rax
	addq	%rsi, %rax
	salq	%cl, %rax
	movl	%eax, %eax
.L3586:
	sarq	$63, %rdx
	orl	$1, %edx
	imull	%edx, %eax
.L3584:
	pxor	%xmm0, %xmm0
	movb	$1, -192(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movups	%xmm0, -191(%rbp)
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm2
	movb	%al, 16(%rsp)
	movups	%xmm2, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3007:
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movss	-191(%rbp), %xmm2
	movss	.LC57(%rip), %xmm1
	movss	.LC56(%rip), %xmm4
	movaps	%xmm2, %xmm3
	movaps	%xmm2, %xmm0
	andps	%xmm1, %xmm3
	ucomiss	%xmm3, %xmm4
	jbe	.L3547
	cvttss2sil	%xmm2, %eax
	pxor	%xmm0, %xmm0
	andnps	%xmm2, %xmm1
	cvtsi2ssl	%eax, %xmm0
	orps	%xmm1, %xmm0
.L3547:
	ucomiss	%xmm0, %xmm0
	pxor	%xmm1, %xmm1
	movb	$3, -192(%rbp)
	movq	%r12, %rdi
	movups	%xmm1, -191(%rbp)
	movd	%xmm0, -191(%rbp)
	movdqa	-192(%rbp), %xmm4
	setp	%al
	orb	%al, 108(%r12)
	movzbl	-176(%rbp), %eax
	subq	$32, %rsp
	movups	%xmm4, (%rsp)
	movb	%al, 16(%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3008:
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movss	-191(%rbp), %xmm3
	movss	.LC57(%rip), %xmm2
	movss	.LC56(%rip), %xmm4
	movaps	%xmm3, %xmm1
	movaps	%xmm3, %xmm0
	andps	%xmm2, %xmm1
	ucomiss	%xmm1, %xmm4
	jbe	.L3546
	cvttss2sil	%xmm3, %eax
	pxor	%xmm1, %xmm1
	movss	.LC41(%rip), %xmm4
	andnps	%xmm3, %xmm2
	cvtsi2ssl	%eax, %xmm1
	movaps	%xmm1, %xmm6
	cmpnless	%xmm3, %xmm6
	movaps	%xmm6, %xmm0
	andps	%xmm4, %xmm0
	subss	%xmm0, %xmm1
	movaps	%xmm1, %xmm0
	orps	%xmm2, %xmm0
.L3546:
	ucomiss	%xmm0, %xmm0
	pxor	%xmm1, %xmm1
	movb	$3, -192(%rbp)
	movq	%r12, %rdi
	movups	%xmm1, -191(%rbp)
	movd	%xmm0, -191(%rbp)
	movdqa	-192(%rbp), %xmm2
	setp	%al
	orb	%al, 108(%r12)
	movzbl	-176(%rbp), %eax
	subq	$32, %rsp
	movups	%xmm2, (%rsp)
	movb	%al, 16(%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3123:
	movq	-504(%rbp), %rdx
	movq	-512(%rbp), %rax
	addq	64(%rdx), %rax
	movzbl	1(%rax), %ecx
	movzbl	2(%rax), %edx
	movl	%ecx, %r14d
	andl	$127, %r14d
	testb	%cl, %cl
	js	.L3287
	movl	$2, -536(%rbp)
	leaq	1(%rax), %rcx
.L3288:
	movl	%edx, %r15d
	movl	$1, %ebx
	andl	$127, %r15d
	testb	%dl, %dl
	js	.L3976
.L3292:
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	72(%r12), %rax
	movl	%r15d, %esi
	movq	%r12, %rdi
	movq	-512(%rbp), %rcx
	movl	-191(%rbp), %edx
	movq	%rcx, -16(%rax)
	movl	%r14d, %ecx
	call	_ZN2v88internal4wasm10ThreadImpl20CallIndirectFunctionEjjj
	movq	%rdx, %r9
	cmpl	$5, %eax
	ja	.L3293
	leaq	.L3295(%rip), %rdx
	movl	%eax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm10ThreadImpl7ExecuteEPNS1_15InterpreterCodeEmi,"aG",@progbits,_ZN2v88internal4wasm10ThreadImpl7ExecuteEPNS1_15InterpreterCodeEmi,comdat
	.align 4
	.align 4
.L3295:
	.long	.L3300-.L3295
	.long	.L3341-.L3295
	.long	.L3340-.L3295
	.long	.L3297-.L3295
	.long	.L3947-.L3295
	.long	.L3326-.L3295
	.section	.text._ZN2v88internal4wasm10ThreadImpl7ExecuteEPNS1_15InterpreterCodeEmi,"axG",@progbits,_ZN2v88internal4wasm10ThreadImpl7ExecuteEPNS1_15InterpreterCodeEmi,comdat
.L3124:
	movq	-504(%rbp), %rdx
	movq	-512(%rbp), %rax
	movq	$0, -472(%rbp)
	addq	64(%rdx), %rax
	movzbl	1(%rax), %ecx
	movl	%ecx, %edx
	andl	$127, %edx
	testb	%cl, %cl
	js	.L3977
	movl	$1, -464(%rbp)
.L3260:
	movl	%edx, %eax
	movq	(%r12), %rbx
	movl	%edx, -480(%rbp)
	leaq	(%rax,%rax,4), %rdx
	leaq	(%rax,%rdx,2), %rdx
	movq	24(%rbx), %rax
	leaq	(%rax,%rdx,8), %r15
	cmpq	$0, 80(%r15)
	je	.L3978
.L3261:
	movq	(%r15), %rax
	movzbl	24(%rax), %ebx
	testb	%bl, %bl
	je	.L3264
	movq	72(%r12), %rax
	movq	-512(%rbp), %rdx
	leaq	-416(%rbp), %r8
	movq	%r8, %rdi
	movq	%r8, -536(%rbp)
	movq	%rdx, -16(%rax)
	movq	(%r15), %rax
	movq	8(%r12), %r14
	movl	8(%rax), %esi
	movq	41088(%r14), %rax
	addl	$1, 41104(%r14)
	movq	8(%r12), %rdx
	movl	%esi, -560(%rbp)
	movq	%rax, -568(%rbp)
	movq	41096(%r14), %rax
	movq	%rdx, -552(%rbp)
	movq	%rax, -544(%rbp)
	movq	16(%r12), %rax
	movl	%esi, -408(%rbp)
	movq	%rax, -416(%rbp)
	call	_ZN2v88internal21ImportedFunctionEntry10object_refEv@PLT
	movq	-552(%rbp), %rdx
	movq	-536(%rbp), %r8
	movq	%rax, %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L3265
	movq	%r8, -552(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-552(%rbp), %r8
	movq	%rax, -536(%rbp)
.L3266:
	movq	%r8, %rdi
	call	_ZN2v88internal21ImportedFunctionEntry6targetEv@PLT
	movq	8(%r12), %r10
	movq	%rax, %rsi
	movq	45752(%r10), %rax
	movq	%rsi, -552(%rbp)
	movq	%r10, -576(%rbp)
	leaq	280(%rax), %rdi
	call	_ZNK2v88internal4wasm15WasmCodeManager18LookupNativeModuleEm@PLT
	movq	-552(%rbp), %rsi
	movq	504(%rax), %rdx
	movq	%rax, %r8
	movq	%r8, %rdi
	movq	(%rdx), %rax
	cmpq	%rsi, %rax
	ja	.L3268
	addq	8(%rdx), %rax
	cmpq	%rax, %rsi
	jnb	.L3268
	movq	%r8, -552(%rbp)
	call	_ZNK2v88internal4wasm12NativeModule33GetFunctionIndexFromJumpTableSlotEm@PLT
	movq	-552(%rbp), %r8
	movl	%eax, %esi
	movl	%eax, -584(%rbp)
	movq	%r8, %rdi
	call	_ZNK2v88internal4wasm12NativeModule7HasCodeEj@PLT
	movq	-552(%rbp), %r8
	movl	-584(%rbp), %r11d
	testb	%al, %al
	jne	.L3269
	movq	-576(%rbp), %r10
	movl	%r11d, %edx
	movq	%r8, %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal4wasm11CompileLazyEPNS0_7IsolateEPNS1_12NativeModuleEi@PLT
	movq	-552(%rbp), %r8
	movl	-584(%rbp), %r11d
	testb	%al, %al
	je	.L3979
.L3269:
	movl	%r11d, %esi
	movq	%r8, %rdi
	call	_ZNK2v88internal4wasm12NativeModule7GetCodeEj@PLT
	movq	%rax, %rcx
.L3271:
	movq	8(%r12), %rsi
	testq	%rcx, %rcx
	je	.L3270
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	8(%rax), %rdx
	movl	-560(%rbp), %eax
	salq	$5, %rax
	addq	136(%rdx), %rax
	movq	-536(%rbp), %rdx
	movq	(%rax), %r8
	call	_ZN2v88internal4wasm10ThreadImpl24CallExternalWasmFunctionEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEPKNS1_8WasmCodeEPNS0_9SignatureINS1_9ValueTypeEEE
.L3274:
	movq	-568(%rbp), %rsi
	subl	$1, 41104(%r14)
	movq	%rsi, 41088(%r14)
	movq	-544(%rbp), %rsi
	cmpq	41096(%r14), %rsi
	je	.L3623
	movl	%eax, -536(%rbp)
	movq	-544(%rbp), %rax
	movq	%r14, %rdi
	movq	%rax, 41096(%r14)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movl	-536(%rbp), %eax
.L3623:
	cmpl	$4, %eax
	je	.L3947
	ja	.L3276
	cmpl	$3, %eax
	jne	.L3980
	movzbl	109(%r12), %eax
	movzbl	-513(%rbp), %esi
	andl	$2, %eax
	movl	$0, %eax
	cmovne	%eax, %r13d
	movl	-464(%rbp), %eax
	cmovne	%ebx, %esi
	addl	$1, %eax
	movb	%sil, -513(%rbp)
	movl	%eax, -492(%rbp)
.L3283:
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	cltq
	jmp	.L3137
.L3125:
	movq	-504(%rbp), %rax
	leaq	-512(%rbp), %rcx
	leaq	-504(%rbp), %rdx
	movq	%r12, %rdi
	leaq	-144(%rbp), %rsi
	leaq	-488(%rbp), %r8
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax), %r9
	call	_ZN2v88internal4wasm10ThreadImpl8DoReturnEPNS1_7DecoderEPPNS1_15InterpreterCodeEPmS8_m
	testb	%al, %al
	je	.L3947
	movzbl	109(%r12), %edx
	movq	-504(%rbp), %r15
	andl	$1, %edx
.L3941:
	movzbl	-513(%rbp), %esi
	cmovne	%eax, %esi
	movl	$0, %eax
	cmovne	%eax, %r13d
	movb	%sil, -513(%rbp)
	jmp	.L3155
.L3126:
	movq	-504(%rbp), %rdx
	movq	-512(%rbp), %rax
	addq	64(%rdx), %rax
	movl	$0, -416(%rbp)
	movzbl	1(%rax), %edx
	movl	%edx, %r14d
	andl	$127, %r14d
	testb	%dl, %dl
	js	.L3981
.L3212:
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-191(%rbp), %ebx
	cmpl	%r14d, %ebx
	cmova	%r14d, %ebx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L3213:
	addl	$1, %eax
	cmpl	%eax, %ebx
	jnb	.L3213
	movq	-504(%rbp), %rax
	movl	%ebx, %edx
	addq	-512(%rbp), %rdx
	movq	80(%rax), %rax
	leaq	16(%rax), %r15
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L3214
	movq	%r15, %rcx
	jmp	.L3215
	.p2align 4,,10
	.p2align 3
.L3982:
	movq	%rax, %rcx
	movq	16(%rax), %rax
.L3218:
	testq	%rax, %rax
	je	.L3216
.L3215:
	cmpq	32(%rax), %rdx
	jbe	.L3982
	movq	24(%rax), %rax
	jmp	.L3218
.L3121:
	movq	-504(%rbp), %rdx
	movq	-512(%rbp), %rax
	addq	64(%rdx), %rax
	movzbl	1(%rax), %esi
	movzbl	2(%rax), %edx
	movl	%esi, %ecx
	andl	$127, %ecx
	testb	%sil, %sil
	js	.L3329
	leaq	1(%rax), %rsi
	movl	$2, %ebx
.L3330:
	movl	%edx, %r15d
	movl	$1, %r14d
	andl	$127, %r15d
	testb	%dl, %dl
	js	.L3983
.L3334:
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	movl	%ecx, -536(%rbp)
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-512(%rbp), %rsi
	movq	72(%r12), %rax
	movq	%r12, %rdi
	movl	-191(%rbp), %edx
	movl	-536(%rbp), %ecx
	movq	%rsi, -16(%rax)
	movl	%r15d, %esi
	call	_ZN2v88internal4wasm10ThreadImpl20CallIndirectFunctionEjjj
	movq	%rdx, %r9
	cmpl	$5, %eax
	ja	.L3335
	leaq	.L3337(%rip), %rdx
	movl	%eax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm10ThreadImpl7ExecuteEPNS1_15InterpreterCodeEmi,"aG",@progbits,_ZN2v88internal4wasm10ThreadImpl7ExecuteEPNS1_15InterpreterCodeEmi,comdat
	.align 4
	.align 4
.L3337:
	.long	.L3342-.L3337
	.long	.L3341-.L3337
	.long	.L3340-.L3337
	.long	.L3339-.L3337
	.long	.L3947-.L3337
	.long	.L3336-.L3337
	.section	.text._ZN2v88internal4wasm10ThreadImpl7ExecuteEPNS1_15InterpreterCodeEmi,"axG",@progbits,_ZN2v88internal4wasm10ThreadImpl7ExecuteEPNS1_15InterpreterCodeEmi,comdat
.L3122:
	movq	-504(%rbp), %rdx
	movq	-512(%rbp), %rax
	addq	64(%rdx), %rax
	movq	$0, -408(%rbp)
	movzbl	1(%rax), %ecx
	movl	%ecx, %edx
	andl	$127, %edx
	testb	%cl, %cl
	js	.L3984
	movl	$1, -400(%rbp)
.L3306:
	movl	%edx, %eax
	movq	(%r12), %rbx
	movl	%edx, -416(%rbp)
	leaq	(%rax,%rax,4), %rdx
	leaq	(%rax,%rdx,2), %rdx
	movq	24(%rbx), %rax
	leaq	(%rax,%rdx,8), %r15
	cmpq	$0, 80(%r15)
	je	.L3985
.L3307:
	movq	(%r15), %rax
	cmpb	$0, 24(%rax)
	jne	.L3310
	leaq	-512(%rbp), %rcx
	leaq	-144(%rbp), %rsi
	movq	%r15, %rdx
	movq	%r12, %rdi
	leaq	-488(%rbp), %r8
	call	_ZN2v88internal4wasm10ThreadImpl12DoReturnCallEPNS1_7DecoderEPNS1_15InterpreterCodeEPmS7_
	testb	%al, %al
	je	.L3947
.L3942:
	movzbl	109(%r12), %edx
	movq	%r15, -504(%rbp)
	andl	$2, %edx
	jmp	.L3941
.L3135:
	movq	-504(%rbp), %rax
	movq	-512(%rbp), %rcx
	leaq	-144(%rbp), %rdx
	leaq	-416(%rbp), %rdi
	leaq	_ZN2v88internal4wasmL16kAllWasmFeaturesE(%rip), %rsi
	addq	64(%rax), %rcx
	call	_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE0EEC1ERKNS1_12WasmFeaturesEPS3_PKh
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-191(%rbp), %eax
	testl	%eax, %eax
	je	.L3138
	movl	-416(%rbp), %eax
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	leal	1(%rax), %edx
	movl	%edx, -492(%rbp)
.L3139:
	movslq	%edx, %rax
	jmp	.L3137
.L3059:
	leaq	-256(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-224(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movss	-255(%rbp), %xmm0
	movl	$0, %edx
	movq	%r12, %rdi
	ucomiss	-223(%rbp), %xmm0
	pxor	%xmm0, %xmm0
	movb	$1, -192(%rbp)
	movups	%xmm0, -191(%rbp)
	setnp	%al
	cmovne	%edx, %eax
	subq	$32, %rsp
	movzbl	%al, %eax
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm3
	movb	%al, 16(%rsp)
	movups	%xmm3, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L2973:
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-191(%rbp), %rax
	leaq	-64(%rbp), %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal4wasm25float64_to_uint64_wrapperEm@PLT
	testl	%eax, %eax
	je	.L3607
	movq	-64(%rbp), %rax
	movb	$2, -192(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	$0, -183(%rbp)
	movq	%rax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm4
	movb	%al, 16(%rsp)
	movups	%xmm4, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L2974:
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-191(%rbp), %rax
	leaq	-64(%rbp), %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal4wasm24float64_to_int64_wrapperEm@PLT
	testl	%eax, %eax
	je	.L3607
	movq	-64(%rbp), %rax
	movb	$2, -192(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	$0, -183(%rbp)
	movq	%rax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm3
	movb	%al, 16(%rsp)
	movups	%xmm3, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3010:
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	pxor	%xmm0, %xmm0
	subq	$32, %rsp
	movq	%r12, %rdi
	movl	-191(%rbp), %eax
	movb	$3, -192(%rbp)
	movups	%xmm0, -191(%rbp)
	addl	$-2147483648, %eax
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm7
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3011:
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	pxor	%xmm0, %xmm0
	subq	$32, %rsp
	movq	%r12, %rdi
	movl	-191(%rbp), %eax
	movb	$3, -192(%rbp)
	movups	%xmm0, -191(%rbp)
	andl	$2147483647, %eax
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm6
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3012:
	leaq	-192(%rbp), %r14
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-191(%rbp), %rbx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movb	$2, -192(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	$0, -183(%rbp)
	movl	%ebx, %ecx
	movzbl	-176(%rbp), %eax
	rorq	%cl, -191(%rbp)
	movdqa	-192(%rbp), %xmm4
	movb	%al, 16(%rsp)
	movups	%xmm4, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3013:
	leaq	-192(%rbp), %r14
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-191(%rbp), %rbx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movb	$2, -192(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	$0, -183(%rbp)
	movl	%ebx, %ecx
	movzbl	-176(%rbp), %eax
	rolq	%cl, -191(%rbp)
	movdqa	-192(%rbp), %xmm5
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3014:
	leaq	-192(%rbp), %r14
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-191(%rbp), %rbx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movb	$2, -192(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	$0, -183(%rbp)
	movl	%ebx, %ecx
	movzbl	-176(%rbp), %eax
	shrq	%cl, -191(%rbp)
	movdqa	-192(%rbp), %xmm6
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3015:
	leaq	-192(%rbp), %r14
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-191(%rbp), %rbx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movb	$2, -192(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	$0, -183(%rbp)
	movl	%ebx, %ecx
	movzbl	-176(%rbp), %eax
	sarq	%cl, -191(%rbp)
	movdqa	-192(%rbp), %xmm7
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3016:
	leaq	-192(%rbp), %r14
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-191(%rbp), %rbx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movb	$2, -192(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	$0, -183(%rbp)
	movl	%ebx, %ecx
	movzbl	-176(%rbp), %eax
	salq	%cl, -191(%rbp)
	movdqa	-192(%rbp), %xmm5
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3017:
	leaq	-256(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-224(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-255(%rbp), %rax
	subq	$32, %rsp
	movq	%r12, %rdi
	xorq	-223(%rbp), %rax
	movb	$2, -192(%rbp)
	movq	$0, -183(%rbp)
	movq	%rax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm5
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3018:
	leaq	-256(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-224(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-255(%rbp), %rax
	subq	$32, %rsp
	movq	%r12, %rdi
	orq	-223(%rbp), %rax
	movb	$2, -192(%rbp)
	movq	$0, -183(%rbp)
	movq	%rax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm4
	movb	%al, 16(%rsp)
	movups	%xmm4, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3019:
	leaq	-256(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-224(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-255(%rbp), %rax
	subq	$32, %rsp
	movq	%r12, %rdi
	andq	-223(%rbp), %rax
	movb	$2, -192(%rbp)
	movq	$0, -183(%rbp)
	movq	%rax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm3
	movb	%al, 16(%rsp)
	movups	%xmm3, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3020:
	leaq	-192(%rbp), %r14
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-191(%rbp), %rbx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-191(%rbp), %rax
	testq	%rbx, %rbx
	je	.L3531
	xorl	%edx, %edx
	movb	$2, -192(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	divq	%rbx
	movq	$0, -183(%rbp)
	movzbl	-176(%rbp), %eax
	movb	%al, 16(%rsp)
	movq	%rdx, -191(%rbp)
	movdqa	-192(%rbp), %xmm4
	movups	%xmm4, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3021:
	leaq	-192(%rbp), %r14
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-191(%rbp), %rbx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-191(%rbp), %rax
	testq	%rbx, %rbx
	je	.L3531
	xorl	%edx, %edx
	cmpq	$-1, %rbx
	je	.L3530
	cqto
	idivq	%rbx
.L3530:
	movq	$0, -183(%rbp)
	movzbl	-176(%rbp), %eax
	movb	$2, -192(%rbp)
	movq	%rdx, -191(%rbp)
	movdqa	-192(%rbp), %xmm2
	subq	$32, %rsp
	movq	%r12, %rdi
	movb	%al, 16(%rsp)
	movups	%xmm2, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3022:
	leaq	-192(%rbp), %r14
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-191(%rbp), %rbx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-191(%rbp), %rax
	testq	%rbx, %rbx
	je	.L3528
	xorl	%edx, %edx
	movb	$2, -192(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	divq	%rbx
	movq	$0, -183(%rbp)
	movq	%rax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm3
	movb	%al, 16(%rsp)
	movups	%xmm3, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3023:
	leaq	-192(%rbp), %r14
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-191(%rbp), %rbx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-191(%rbp), %rax
	testq	%rbx, %rbx
	je	.L3686
	cmpq	$-1, %rbx
	jne	.L3721
	movabsq	$-9223372036854775808, %rdx
	cmpq	%rdx, %rax
	jne	.L3721
.L3687:
	movl	$4, %eax
.L3526:
	movl	%eax, 104(%r12)
	movq	-512(%rbp), %rdx
	movl	$4, 88(%r12)
	movq	72(%r12), %rax
	movq	%rdx, -16(%rax)
	jmp	.L3947
	.p2align 4,,10
	.p2align 3
.L3113:
	movq	-504(%rbp), %rdx
	movq	-512(%rbp), %rax
	addq	64(%rdx), %rax
	movb	$0, -444(%rbp)
	movq	$0, -440(%rbp)
	movzbl	1(%rax), %ecx
	movl	%ecx, %edx
	andl	$127, %edx
	testb	%cl, %cl
	js	.L3986
	movl	$1, -432(%rbp)
.L3351:
	movq	(%r12), %rax
	movl	%edx, -448(%rbp)
	salq	$5, %rdx
	movq	8(%rax), %rax
	addq	24(%rax), %rdx
	cmpb	$9, (%rdx)
	movq	%rdx, %r14
	ja	.L3278
	movzbl	(%rdx), %eax
	leaq	.L3353(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm10ThreadImpl7ExecuteEPNS1_15InterpreterCodeEmi,"aG",@progbits,_ZN2v88internal4wasm10ThreadImpl7ExecuteEPNS1_15InterpreterCodeEmi,comdat
	.align 4
	.align 4
.L3353:
	.long	.L3278-.L3353
	.long	.L3358-.L3353
	.long	.L3355-.L3353
	.long	.L3356-.L3353
	.long	.L3355-.L3353
	.long	.L3354-.L3353
	.long	.L3352-.L3353
	.long	.L3352-.L3353
	.long	.L3278-.L3353
	.long	.L3352-.L3353
	.section	.text._ZN2v88internal4wasm10ThreadImpl7ExecuteEPNS1_15InterpreterCodeEmi,"axG",@progbits,_ZN2v88internal4wasm10ThreadImpl7ExecuteEPNS1_15InterpreterCodeEmi,comdat
.L3114:
	movq	-504(%rbp), %rdx
	movq	-512(%rbp), %rax
	addq	64(%rdx), %rax
	movb	$0, -412(%rbp)
	movq	$0, -408(%rbp)
	movzbl	1(%rax), %ecx
	movl	%ecx, %edx
	andl	$127, %edx
	testb	%cl, %cl
	js	.L3987
	movl	$1, -400(%rbp)
.L3348:
	movq	8(%r12), %r14
	movl	%edx, -416(%rbp)
	leaq	-320(%rbp), %rdi
	movq	%r12, %rsi
	addl	$1, 41104(%r14)
	movq	41088(%r14), %r15
	movq	41096(%r14), %rbx
	call	_ZN2v88internal4wasm10ThreadImpl14GetGlobalValueEj
	movzbl	-304(%rbp), %eax
	subq	$32, %rsp
	movq	%r12, %rdi
	movdqa	-320(%rbp), %xmm6
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movl	-400(%rbp), %eax
	addq	$32, %rsp
	subl	$1, 41104(%r14)
	movq	%r15, 41088(%r14)
	addl	$1, %eax
	movl	%eax, -492(%rbp)
	cmpq	41096(%r14), %rbx
	je	.L3349
	movq	%rbx, 41096(%r14)
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movl	-492(%rbp), %eax
.L3349:
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	cltq
	jmp	.L3137
.L3042:
	leaq	-256(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-224(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	pxor	%xmm0, %xmm0
	subq	$32, %rsp
	movq	%r12, %rdi
	movl	-255(%rbp), %eax
	imull	-223(%rbp), %eax
	movups	%xmm0, -191(%rbp)
	movb	$1, -192(%rbp)
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm6
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3043:
	leaq	-256(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-224(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	pxor	%xmm0, %xmm0
	subq	$32, %rsp
	movq	%r12, %rdi
	movl	-223(%rbp), %eax
	subl	-255(%rbp), %eax
	movups	%xmm0, -191(%rbp)
	movb	$1, -192(%rbp)
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm5
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3044:
	leaq	-256(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-224(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	pxor	%xmm0, %xmm0
	subq	$32, %rsp
	movq	%r12, %rdi
	movl	-223(%rbp), %eax
	addl	-255(%rbp), %eax
	movups	%xmm0, -191(%rbp)
	movb	$1, -192(%rbp)
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm4
	movb	%al, 16(%rsp)
	movups	%xmm4, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3045:
	movq	%r12, %rsi
	leaq	-192(%rbp), %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-191(%rbp), %edi
	call	__popcountdi2@PLT
	pxor	%xmm0, %xmm0
	subq	$32, %rsp
	movq	%r12, %rdi
	movups	%xmm0, -191(%rbp)
	movb	$1, -192(%rbp)
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm6
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3046:
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-191(%rbp), %edx
	xorl	%eax, %eax
	movq	%r12, %rdi
	pxor	%xmm0, %xmm0
	movb	$1, -192(%rbp)
	rep bsfl	%edx, %eax
	testl	%edx, %edx
	movl	$32, %edx
	movups	%xmm0, -191(%rbp)
	cmove	%edx, %eax
	subq	$32, %rsp
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm5
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3047:
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-191(%rbp), %edx
	movl	$32, %eax
	testl	%edx, %edx
	je	.L3541
	bsrl	%edx, %eax
	xorl	$31, %eax
.L3541:
	pxor	%xmm0, %xmm0
	movb	$1, -192(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movups	%xmm0, -191(%rbp)
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm4
	movb	%al, 16(%rsp)
	movups	%xmm4, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3048:
	leaq	-256(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-224(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movsd	-223(%rbp), %xmm0
	comisd	-255(%rbp), %xmm0
	pxor	%xmm0, %xmm0
	movups	%xmm0, -191(%rbp)
	movq	%r12, %rdi
	movb	$1, -192(%rbp)
	setnb	%al
	subq	$32, %rsp
	movzbl	%al, %eax
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm6
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3049:
	leaq	-256(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-224(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movsd	-255(%rbp), %xmm0
	comisd	-223(%rbp), %xmm0
	pxor	%xmm0, %xmm0
	movups	%xmm0, -191(%rbp)
	movq	%r12, %rdi
	movb	$1, -192(%rbp)
	setnb	%al
	subq	$32, %rsp
	movzbl	%al, %eax
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm4
	movb	%al, 16(%rsp)
	movups	%xmm4, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3050:
	leaq	-256(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-224(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movsd	-223(%rbp), %xmm0
	comisd	-255(%rbp), %xmm0
	pxor	%xmm0, %xmm0
	movups	%xmm0, -191(%rbp)
	movq	%r12, %rdi
	movb	$1, -192(%rbp)
	seta	%al
	subq	$32, %rsp
	movzbl	%al, %eax
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm5
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3051:
	leaq	-256(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-224(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movsd	-255(%rbp), %xmm0
	comisd	-223(%rbp), %xmm0
	pxor	%xmm0, %xmm0
	movups	%xmm0, -191(%rbp)
	movq	%r12, %rdi
	movb	$1, -192(%rbp)
	seta	%al
	subq	$32, %rsp
	movzbl	%al, %eax
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm3
	movb	%al, 16(%rsp)
	movups	%xmm3, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3052:
	leaq	-256(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-224(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movsd	-255(%rbp), %xmm0
	movl	$1, %edx
	movq	%r12, %rdi
	ucomisd	-223(%rbp), %xmm0
	pxor	%xmm0, %xmm0
	movb	$1, -192(%rbp)
	movups	%xmm0, -191(%rbp)
	setp	%al
	cmovne	%edx, %eax
	subq	$32, %rsp
	movzbl	%al, %eax
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm7
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3053:
	leaq	-256(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-224(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movsd	-255(%rbp), %xmm0
	movl	$0, %edx
	movq	%r12, %rdi
	ucomisd	-223(%rbp), %xmm0
	pxor	%xmm0, %xmm0
	movb	$1, -192(%rbp)
	movups	%xmm0, -191(%rbp)
	setnp	%al
	cmovne	%edx, %eax
	subq	$32, %rsp
	movzbl	%al, %eax
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm6
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3054:
	leaq	-256(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-224(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movss	-223(%rbp), %xmm0
	comiss	-255(%rbp), %xmm0
	pxor	%xmm0, %xmm0
	movups	%xmm0, -191(%rbp)
	movq	%r12, %rdi
	movb	$1, -192(%rbp)
	setnb	%al
	subq	$32, %rsp
	movzbl	%al, %eax
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm3
	movb	%al, 16(%rsp)
	movups	%xmm3, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3069:
	leaq	-256(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-224(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-223(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	cmpq	%rax, -255(%rbp)
	movups	%xmm0, -191(%rbp)
	sete	%al
	movb	$1, -192(%rbp)
	subq	$32, %rsp
	movzbl	%al, %eax
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm6
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3067:
	leaq	-256(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-224(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-223(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	cmpq	%rax, -255(%rbp)
	movups	%xmm0, -191(%rbp)
	setg	%al
	movb	$1, -192(%rbp)
	subq	$32, %rsp
	movzbl	%al, %eax
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm7
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3068:
	leaq	-256(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-224(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-223(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	cmpq	%rax, -255(%rbp)
	movups	%xmm0, -191(%rbp)
	setne	%al
	movb	$1, -192(%rbp)
	subq	$32, %rsp
	movzbl	%al, %eax
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm7
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3063:
	leaq	-256(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-224(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-223(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	cmpq	%rax, -255(%rbp)
	movups	%xmm0, -191(%rbp)
	setge	%al
	movb	$1, -192(%rbp)
	subq	$32, %rsp
	movzbl	%al, %eax
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm3
	movb	%al, 16(%rsp)
	movups	%xmm3, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3064:
	leaq	-256(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-224(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-223(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	cmpq	%rax, -255(%rbp)
	movups	%xmm0, -191(%rbp)
	setb	%al
	movb	$1, -192(%rbp)
	subq	$32, %rsp
	movzbl	%al, %eax
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm5
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3065:
	leaq	-256(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-224(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-223(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	cmpq	%rax, -255(%rbp)
	movups	%xmm0, -191(%rbp)
	setl	%al
	movb	$1, -192(%rbp)
	subq	$32, %rsp
	movzbl	%al, %eax
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm4
	movb	%al, 16(%rsp)
	movups	%xmm4, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3066:
	leaq	-256(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-224(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-223(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	cmpq	%rax, -255(%rbp)
	movups	%xmm0, -191(%rbp)
	seta	%al
	movb	$1, -192(%rbp)
	subq	$32, %rsp
	movzbl	%al, %eax
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm3
	movb	%al, 16(%rsp)
	movups	%xmm3, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3055:
	leaq	-256(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-224(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movss	-255(%rbp), %xmm0
	comiss	-223(%rbp), %xmm0
	pxor	%xmm0, %xmm0
	movups	%xmm0, -191(%rbp)
	movq	%r12, %rdi
	movb	$1, -192(%rbp)
	setnb	%al
	subq	$32, %rsp
	movzbl	%al, %eax
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm6
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3056:
	leaq	-256(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-224(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movss	-223(%rbp), %xmm0
	comiss	-255(%rbp), %xmm0
	pxor	%xmm0, %xmm0
	movups	%xmm0, -191(%rbp)
	movq	%r12, %rdi
	movb	$1, -192(%rbp)
	seta	%al
	subq	$32, %rsp
	movzbl	%al, %eax
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm7
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3057:
	leaq	-256(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-224(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movss	-255(%rbp), %xmm0
	comiss	-223(%rbp), %xmm0
	pxor	%xmm0, %xmm0
	movups	%xmm0, -191(%rbp)
	movq	%r12, %rdi
	movb	$1, -192(%rbp)
	seta	%al
	subq	$32, %rsp
	movzbl	%al, %eax
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm5
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3058:
	leaq	-256(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-224(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movss	-255(%rbp), %xmm0
	movl	$1, %edx
	movq	%r12, %rdi
	ucomiss	-223(%rbp), %xmm0
	pxor	%xmm0, %xmm0
	movb	$1, -192(%rbp)
	movups	%xmm0, -191(%rbp)
	setp	%al
	cmovne	%edx, %eax
	subq	$32, %rsp
	movzbl	%al, %eax
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm4
	movb	%al, 16(%rsp)
	movups	%xmm4, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3061:
	leaq	-256(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-224(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-223(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	cmpq	%rax, -255(%rbp)
	movups	%xmm0, -191(%rbp)
	setle	%al
	movb	$1, -192(%rbp)
	subq	$32, %rsp
	movzbl	%al, %eax
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm5
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3062:
	leaq	-256(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-224(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-223(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	cmpq	%rax, -255(%rbp)
	movups	%xmm0, -191(%rbp)
	setnb	%al
	movb	$1, -192(%rbp)
	subq	$32, %rsp
	movzbl	%al, %eax
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm4
	movb	%al, 16(%rsp)
	movups	%xmm4, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3060:
	leaq	-256(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-224(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-223(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	cmpq	%rax, -255(%rbp)
	movups	%xmm0, -191(%rbp)
	setbe	%al
	movb	$1, -192(%rbp)
	subq	$32, %rsp
	movzbl	%al, %eax
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm6
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3026:
	leaq	-256(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-224(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-223(%rbp), %rax
	subq	$32, %rsp
	movq	%r12, %rdi
	addq	-255(%rbp), %rax
	movb	$2, -192(%rbp)
	movq	$0, -183(%rbp)
	movq	%rax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm5
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3027:
	movq	%r12, %rsi
	leaq	-192(%rbp), %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-191(%rbp), %rdi
	call	__popcountdi2@PLT
	movb	$2, -192(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	$0, -183(%rbp)
	cltq
	movq	%rax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm4
	movb	%al, 16(%rsp)
	movups	%xmm4, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3028:
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-191(%rbp), %rdx
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	$0, -183(%rbp)
	rep bsfq	%rdx, %rax
	testq	%rdx, %rdx
	movl	$64, %edx
	movb	$2, -192(%rbp)
	cltq
	cmove	%rdx, %rax
	subq	$32, %rsp
	movq	%rax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm2
	movb	%al, 16(%rsp)
	movups	%xmm2, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3029:
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-191(%rbp), %rax
	movl	$64, %edx
	testq	%rax, %rax
	je	.L3543
	bsrq	%rax, %rax
	xorq	$63, %rax
	movslq	%eax, %rdx
.L3543:
	movb	$2, -192(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	$0, -183(%rbp)
	movzbl	-176(%rbp), %eax
	movq	%rdx, -191(%rbp)
	movdqa	-192(%rbp), %xmm3
	movb	%al, 16(%rsp)
	movups	%xmm3, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3030:
	leaq	-192(%rbp), %r14
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-191(%rbp), %ebx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	pxor	%xmm0, %xmm0
	subq	$32, %rsp
	movq	%r12, %rdi
	movl	-191(%rbp), %eax
	movl	%ebx, %ecx
	movups	%xmm0, -191(%rbp)
	movb	$1, -192(%rbp)
	rorl	%cl, %eax
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm3
	movb	%al, 16(%rsp)
	movups	%xmm3, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3031:
	leaq	-192(%rbp), %r14
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-191(%rbp), %ebx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	pxor	%xmm0, %xmm0
	subq	$32, %rsp
	movq	%r12, %rdi
	movl	-191(%rbp), %eax
	movl	%ebx, %ecx
	movups	%xmm0, -191(%rbp)
	movb	$1, -192(%rbp)
	roll	%cl, %eax
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm2
	movb	%al, 16(%rsp)
	movups	%xmm2, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3032:
	leaq	-192(%rbp), %r14
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-191(%rbp), %ebx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	pxor	%xmm0, %xmm0
	subq	$32, %rsp
	movq	%r12, %rdi
	movl	-191(%rbp), %eax
	movl	%ebx, %ecx
	movups	%xmm0, -191(%rbp)
	movb	$1, -192(%rbp)
	shrl	%cl, %eax
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm5
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3033:
	leaq	-192(%rbp), %r14
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-191(%rbp), %ebx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	pxor	%xmm0, %xmm0
	subq	$32, %rsp
	movq	%r12, %rdi
	movl	-191(%rbp), %eax
	movl	%ebx, %ecx
	movups	%xmm0, -191(%rbp)
	movb	$1, -192(%rbp)
	sarl	%cl, %eax
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm6
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3034:
	leaq	-192(%rbp), %r14
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-191(%rbp), %ebx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	pxor	%xmm0, %xmm0
	subq	$32, %rsp
	movq	%r12, %rdi
	movl	-191(%rbp), %eax
	movl	%ebx, %ecx
	movups	%xmm0, -191(%rbp)
	movb	$1, -192(%rbp)
	sall	%cl, %eax
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm4
	movb	%al, 16(%rsp)
	movups	%xmm4, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3035:
	leaq	-256(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-224(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	pxor	%xmm0, %xmm0
	subq	$32, %rsp
	movq	%r12, %rdi
	movl	-255(%rbp), %eax
	xorl	-223(%rbp), %eax
	movups	%xmm0, -191(%rbp)
	movb	$1, -192(%rbp)
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm4
	movb	%al, 16(%rsp)
	movups	%xmm4, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3036:
	leaq	-256(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-224(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	pxor	%xmm0, %xmm0
	subq	$32, %rsp
	movq	%r12, %rdi
	movl	-255(%rbp), %eax
	orl	-223(%rbp), %eax
	movups	%xmm0, -191(%rbp)
	movb	$1, -192(%rbp)
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm3
	movb	%al, 16(%rsp)
	movups	%xmm3, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3037:
	leaq	-256(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-224(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	pxor	%xmm0, %xmm0
	subq	$32, %rsp
	movq	%r12, %rdi
	movl	-255(%rbp), %eax
	andl	-223(%rbp), %eax
	movups	%xmm0, -191(%rbp)
	movb	$1, -192(%rbp)
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm7
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3038:
	leaq	-192(%rbp), %r14
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-191(%rbp), %ebx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-191(%rbp), %eax
	testl	%ebx, %ebx
	je	.L3531
	xorl	%edx, %edx
	pxor	%xmm0, %xmm0
	movb	$1, -192(%rbp)
	movq	%r12, %rdi
	divl	%ebx
	movups	%xmm0, -191(%rbp)
	movzbl	-176(%rbp), %eax
	subq	$32, %rsp
	movb	%al, 16(%rsp)
	movl	%edx, -191(%rbp)
	movdqa	-192(%rbp), %xmm2
	movups	%xmm2, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3039:
	leaq	-192(%rbp), %r14
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-191(%rbp), %ebx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-191(%rbp), %eax
	testl	%ebx, %ebx
	je	.L3531
	xorl	%edx, %edx
	cmpl	$-1, %ebx
	je	.L3524
	cltd
	idivl	%ebx
.L3524:
	movb	$1, -192(%rbp)
	pxor	%xmm0, %xmm0
	subq	$32, %rsp
	movq	%r12, %rdi
	movups	%xmm0, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movl	%edx, -191(%rbp)
	movdqa	-192(%rbp), %xmm3
	movb	%al, 16(%rsp)
	movups	%xmm3, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3040:
	leaq	-192(%rbp), %r14
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-191(%rbp), %ebx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-191(%rbp), %eax
	testl	%ebx, %ebx
	je	.L3528
	xorl	%edx, %edx
	pxor	%xmm0, %xmm0
	movb	$1, -192(%rbp)
	movq	%r12, %rdi
	divl	%ebx
	movups	%xmm0, -191(%rbp)
	subq	$32, %rsp
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm7
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3041:
	leaq	-192(%rbp), %r14
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-191(%rbp), %ebx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-191(%rbp), %eax
	testl	%ebx, %ebx
	je	.L3686
	cmpl	$-1, %ebx
	jne	.L3720
	cmpl	$-2147483648, %eax
	je	.L3687
.L3720:
	cltd
	pxor	%xmm0, %xmm0
	movb	$1, -192(%rbp)
	subq	$32, %rsp
	idivl	%ebx
	movups	%xmm0, -191(%rbp)
	movq	%r12, %rdi
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm6
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3024:
	leaq	-256(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-224(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-255(%rbp), %rax
	subq	$32, %rsp
	movq	%r12, %rdi
	imulq	-223(%rbp), %rax
	movb	$2, -192(%rbp)
	movq	$0, -183(%rbp)
	movq	%rax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm7
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3025:
	leaq	-256(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-224(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-223(%rbp), %rax
	subq	$32, %rsp
	movq	%r12, %rdi
	subq	-255(%rbp), %rax
	movb	$2, -192(%rbp)
	movq	$0, -183(%rbp)
	movq	%rax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm6
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3109:
	movq	-504(%rbp), %r14
	movq	-512(%rbp), %rbx
	leaq	-416(%rbp), %rdi
	movq	64(%r14), %rsi
	addq	%rbx, %rsi
	call	_ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE0EEC2EPS3_PKhj.constprop.0
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-191(%rbp), %r15d
	movl	%r15d, %eax
	addl	-412(%rbp), %eax
	jc	.L3455
	movq	16(%r12), %rdx
	movq	(%rdx), %rdx
	movq	31(%rdx), %rcx
	cmpq	$7, %rcx
	jbe	.L3455
	movl	%eax, %eax
	subq	$8, %rcx
	cmpq	%rcx, %rax
	ja	.L3455
	andq	39(%rdx), %rax
	addq	23(%rdx), %rax
	je	.L3455
	movq	(%rax), %rax
	movb	$2, -192(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	$0, -183(%rbp)
	movq	%rax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm7
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movl	-408(%rbp), %eax
	addq	$32, %rsp
	addl	-492(%rbp), %eax
	cmpb	$0, _ZN2v88internal22FLAG_trace_wasm_memoryE(%rip)
	movl	%eax, -492(%rbp)
	jne	.L3988
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	cltq
	jmp	.L3137
.L3110:
	movq	-504(%rbp), %r14
	movq	-512(%rbp), %rbx
	leaq	-416(%rbp), %rdi
	movq	64(%r14), %rsi
	addq	%rbx, %rsi
	call	_ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE0EEC2EPS3_PKhj.constprop.1
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-191(%rbp), %r15d
	movl	%r15d, %eax
	addl	-412(%rbp), %eax
	jc	.L3455
	movq	16(%r12), %rdx
	movq	(%rdx), %rdx
	movq	31(%rdx), %rcx
	cmpq	$3, %rcx
	jbe	.L3455
	movl	%eax, %eax
	subq	$4, %rcx
	cmpq	%rcx, %rax
	ja	.L3455
	andq	39(%rdx), %rax
	addq	23(%rdx), %rax
	je	.L3455
	movl	(%rax), %eax
	pxor	%xmm0, %xmm0
	movb	$1, -192(%rbp)
	movq	%r12, %rdi
	movups	%xmm0, -191(%rbp)
	subq	$32, %rsp
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm6
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movl	-408(%rbp), %eax
	addq	$32, %rsp
	addl	-492(%rbp), %eax
	cmpb	$0, _ZN2v88internal22FLAG_trace_wasm_memoryE(%rip)
	movl	%eax, -492(%rbp)
	jne	.L3989
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	cltq
	jmp	.L3137
.L3090:
	movq	-504(%rbp), %r15
	movq	-512(%rbp), %r14
	leaq	-416(%rbp), %rdi
	movq	64(%r15), %rsi
	addq	%r14, %rsi
	call	_ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE0EEC2EPS3_PKhj.constprop.0
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rdi, -536(%rbp)
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-536(%rbp), %rdi
	movq	%r12, %rsi
	movq	-191(%rbp), %rbx
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-191(%rbp), %edx
	movl	%edx, %eax
	addl	-412(%rbp), %eax
	jc	.L3500
	movq	16(%r12), %rcx
	movq	(%rcx), %rcx
	movq	31(%rcx), %rsi
	testq	%rsi, %rsi
	je	.L3500
	movl	%eax, %eax
	subq	$1, %rsi
	cmpq	%rsi, %rax
	ja	.L3500
	andq	39(%rcx), %rax
	addq	23(%rcx), %rax
	je	.L3500
	movb	%bl, (%rax)
	movl	-408(%rbp), %eax
	addl	-492(%rbp), %eax
	cmpb	$0, _ZN2v88internal22FLAG_trace_wasm_memoryE(%rip)
	movl	%eax, -492(%rbp)
	jne	.L3990
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	cltq
	jmp	.L3137
.L2981:
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movss	-191(%rbp), %xmm0
	comiss	.LC12(%rip), %xmm0
	jbe	.L3607
	movss	.LC13(%rip), %xmm1
	comiss	%xmm0, %xmm1
	jbe	.L3607
	cvttss2siq	%xmm0, %rax
	pxor	%xmm1, %xmm1
	subq	$32, %rsp
	movq	%r12, %rdi
	movups	%xmm1, -191(%rbp)
	movb	$1, -192(%rbp)
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm2
	movb	%al, 16(%rsp)
	movups	%xmm2, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L2982:
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movss	-191(%rbp), %xmm0
	comiss	.LC9(%rip), %xmm0
	jb	.L3607
	movss	.LC10(%rip), %xmm1
	comiss	%xmm0, %xmm1
	jbe	.L3607
	cvttss2sil	%xmm0, %eax
	pxor	%xmm1, %xmm1
	subq	$32, %rsp
	movq	%r12, %rdi
	movups	%xmm1, -191(%rbp)
	movb	$1, -192(%rbp)
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm7
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3071:
	leaq	-256(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-224(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-223(%rbp), %eax
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	cmpl	%eax, -255(%rbp)
	movups	%xmm0, -191(%rbp)
	setbe	%al
	movb	$1, -192(%rbp)
	subq	$32, %rsp
	movzbl	%al, %eax
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm5
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3072:
	leaq	-256(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-224(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-223(%rbp), %eax
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	cmpl	%eax, -255(%rbp)
	movups	%xmm0, -191(%rbp)
	setle	%al
	movb	$1, -192(%rbp)
	subq	$32, %rsp
	movzbl	%al, %eax
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm4
	movb	%al, 16(%rsp)
	movups	%xmm4, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3117:
	movq	-504(%rbp), %rdx
	movq	-512(%rbp), %rax
	addq	64(%rdx), %rax
	movb	$0, -412(%rbp)
	movzbl	1(%rax), %ecx
	movl	%ecx, %edx
	andl	$127, %edx
	testb	%cl, %cl
	js	.L3991
	movl	$1, -408(%rbp)
.L3241:
	movq	8(%r12), %r15
	movl	%edx, -416(%rbp)
	movl	%edx, %eax
	addl	$1, 41104(%r15)
	movq	72(%r12), %rcx
	movq	41088(%r15), %r14
	movq	41096(%r15), %rbx
	addq	-8(%rcx), %rax
	movq	%rax, %rdx
	salq	$4, %rdx
	addq	%rax, %rdx
	addq	24(%r12), %rdx
	cmpb	$6, (%rdx)
	je	.L3242
	movzbl	16(%rdx), %eax
	movdqu	(%rdx), %xmm5
	movb	%al, -336(%rbp)
	movdqa	%xmm5, %xmm2
	movaps	%xmm5, -352(%rbp)
.L3243:
	subq	$32, %rsp
	movq	%r12, %rdi
	movb	%al, 16(%rsp)
	movups	%xmm2, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movl	-408(%rbp), %eax
	addq	$32, %rsp
	subl	$1, 41104(%r15)
	movq	%r14, 41088(%r15)
	addl	$1, %eax
	movl	%eax, -492(%rbp)
	cmpq	41096(%r15), %rbx
	je	.L3627
	movq	%rbx, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movl	-492(%rbp), %eax
.L3627:
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	cltq
	jmp	.L3137
.L3118:
	movq	-504(%rbp), %rax
	movq	-512(%rbp), %rsi
	addq	64(%rax), %rsi
	movzbl	1(%rsi), %eax
	movq	%rsi, %r8
	testb	%al, %al
	js	.L3992
	movl	$1, -416(%rbp)
	movl	$2, %eax
	movl	$2, %edx
.L3183:
	movzbl	(%r8,%rdx), %edx
	movl	%eax, -416(%rbp)
	subl	$64, %edx
	cmpb	$63, %dl
	ja	.L3184
	leaq	.L3186(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rdx
	addq	%rcx, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal4wasm10ThreadImpl7ExecuteEPNS1_15InterpreterCodeEmi,"aG",@progbits,_ZN2v88internal4wasm10ThreadImpl7ExecuteEPNS1_15InterpreterCodeEmi,comdat
	.align 4
	.align 4
.L3186:
	.long	.L3187-.L3186
	.long	.L3184-.L3186
	.long	.L3184-.L3186
	.long	.L3184-.L3186
	.long	.L3184-.L3186
	.long	.L3184-.L3186
	.long	.L3184-.L3186
	.long	.L3184-.L3186
	.long	.L3184-.L3186
	.long	.L3184-.L3186
	.long	.L3184-.L3186
	.long	.L3184-.L3186
	.long	.L3184-.L3186
	.long	.L3184-.L3186
	.long	.L3184-.L3186
	.long	.L3184-.L3186
	.long	.L3184-.L3186
	.long	.L3184-.L3186
	.long	.L3184-.L3186
	.long	.L3184-.L3186
	.long	.L3184-.L3186
	.long	.L3184-.L3186
	.long	.L3184-.L3186
	.long	.L3184-.L3186
	.long	.L3184-.L3186
	.long	.L3184-.L3186
	.long	.L3184-.L3186
	.long	.L3184-.L3186
	.long	.L3184-.L3186
	.long	.L3184-.L3186
	.long	.L3184-.L3186
	.long	.L3184-.L3186
	.long	.L3184-.L3186
	.long	.L3184-.L3186
	.long	.L3184-.L3186
	.long	.L3184-.L3186
	.long	.L3184-.L3186
	.long	.L3184-.L3186
	.long	.L3184-.L3186
	.long	.L3184-.L3186
	.long	.L3185-.L3186
	.long	.L3184-.L3186
	.long	.L3184-.L3186
	.long	.L3184-.L3186
	.long	.L3184-.L3186
	.long	.L3184-.L3186
	.long	.L3184-.L3186
	.long	.L3185-.L3186
	.long	.L3185-.L3186
	.long	.L3184-.L3186
	.long	.L3184-.L3186
	.long	.L3184-.L3186
	.long	.L3184-.L3186
	.long	.L3184-.L3186
	.long	.L3184-.L3186
	.long	.L3184-.L3186
	.long	.L3184-.L3186
	.long	.L3184-.L3186
	.long	.L3184-.L3186
	.long	.L3185-.L3186
	.long	.L3185-.L3186
	.long	.L3185-.L3186
	.long	.L3185-.L3186
	.long	.L3185-.L3186
	.section	.text._ZN2v88internal4wasm10ThreadImpl7ExecuteEPNS1_15InterpreterCodeEmi,"axG",@progbits,_ZN2v88internal4wasm10ThreadImpl7ExecuteEPNS1_15InterpreterCodeEmi,comdat
.L3120:
	movq	48(%r12), %rax
	movq	(%rax), %rax
	movq	7(%rax), %rdx
	movq	40(%r12), %rax
	subq	24(%r12), %rax
	movq	%rdx, %rcx
	imull	$-252645135, %eax, %eax
	andq	$-262144, %rcx
	movq	24(%rcx), %rcx
	leal	8(,%rax,8), %eax
	movq	-37496(%rcx), %rcx
	cltq
	movq	%rcx, -1(%rdx,%rax)
	subq	$17, 40(%r12)
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3087:
	movq	16(%r12), %rax
	pxor	%xmm0, %xmm0
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	31(%rax), %rax
	movups	%xmm0, -191(%rbp)
	movb	$1, -192(%rbp)
	shrq	$16, %rax
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm4
	movb	%al, 16(%rsp)
	movups	%xmm4, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movl	$2, -492(%rbp)
	movq	-504(%rbp), %r15
	movl	$2, %eax
	jmp	.L3137
.L3088:
	movq	-504(%rbp), %r15
	movq	-512(%rbp), %r14
	leaq	-416(%rbp), %rdi
	movq	64(%r15), %rsi
	addq	%r14, %rsi
	call	_ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE0EEC2EPS3_PKhj.constprop.0
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rdi, -536(%rbp)
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-536(%rbp), %rdi
	movq	%r12, %rsi
	movq	-191(%rbp), %rbx
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-191(%rbp), %edx
	movl	%edx, %eax
	addl	-412(%rbp), %eax
	jc	.L3500
	movq	16(%r12), %rcx
	movq	(%rcx), %rcx
	movq	31(%rcx), %rsi
	cmpq	$3, %rsi
	jbe	.L3500
	movl	%eax, %eax
	subq	$4, %rsi
	cmpq	%rsi, %rax
	ja	.L3500
	andq	39(%rcx), %rax
	addq	23(%rcx), %rax
	je	.L3500
	movl	%ebx, (%rax)
	movl	-408(%rbp), %eax
	addl	-492(%rbp), %eax
	cmpb	$0, _ZN2v88internal22FLAG_trace_wasm_memoryE(%rip)
	movl	%eax, -492(%rbp)
	jne	.L3993
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	cltq
	jmp	.L3137
.L3089:
	movq	-504(%rbp), %r15
	movq	-512(%rbp), %r14
	leaq	-416(%rbp), %rdi
	movq	64(%r15), %rsi
	addq	%r14, %rsi
	call	_ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE0EEC2EPS3_PKhj.constprop.0
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rdi, -536(%rbp)
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-536(%rbp), %rdi
	movq	%r12, %rsi
	movq	-191(%rbp), %rbx
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-191(%rbp), %edx
	movl	%edx, %eax
	addl	-412(%rbp), %eax
	jc	.L3500
	movq	16(%r12), %rcx
	movq	(%rcx), %rcx
	movq	31(%rcx), %rsi
	cmpq	$1, %rsi
	jbe	.L3500
	movl	%eax, %eax
	subq	$2, %rsi
	cmpq	%rsi, %rax
	ja	.L3500
	andq	39(%rcx), %rax
	addq	23(%rcx), %rax
	je	.L3500
	movw	%bx, (%rax)
	movl	-408(%rbp), %eax
	addl	-492(%rbp), %eax
	cmpb	$0, _ZN2v88internal22FLAG_trace_wasm_memoryE(%rip)
	movl	%eax, -492(%rbp)
	jne	.L3994
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	cltq
	jmp	.L3137
.L2949:
	leaq	-192(%rbp), %r14
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-191(%rbp), %ebx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-191(%rbp), %eax
	testl	%ebx, %ebx
	je	.L3538
	xorl	%edx, %edx
	divl	%ebx
	movl	%eax, %ebx
.L3538:
	pxor	%xmm0, %xmm0
	movb	$1, -192(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movups	%xmm0, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movl	%ebx, -191(%rbp)
	movdqa	-192(%rbp), %xmm5
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3098:
	movq	-504(%rbp), %r14
	movq	-512(%rbp), %rbx
	leaq	-416(%rbp), %rdi
	movq	64(%r14), %rsi
	addq	%rbx, %rsi
	call	_ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE0EEC2EPS3_PKhj.constprop.0
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-191(%rbp), %r15d
	movl	%r15d, %eax
	addl	-412(%rbp), %eax
	jc	.L3455
	movq	16(%r12), %rdx
	movq	(%rdx), %rdx
	movq	31(%rdx), %rcx
	cmpq	$3, %rcx
	jbe	.L3455
	movl	%eax, %eax
	subq	$4, %rcx
	cmpq	%rcx, %rax
	ja	.L3455
	andq	39(%rdx), %rax
	addq	23(%rdx), %rax
	je	.L3455
	movslq	(%rax), %rax
	movb	$2, -192(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	$0, -183(%rbp)
	movq	%rax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm4
	movb	%al, 16(%rsp)
	movups	%xmm4, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movl	-408(%rbp), %eax
	addq	$32, %rsp
	addl	-492(%rbp), %eax
	cmpb	$0, _ZN2v88internal22FLAG_trace_wasm_memoryE(%rip)
	movl	%eax, -492(%rbp)
	jne	.L3995
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	cltq
	jmp	.L3137
.L3099:
	movq	-504(%rbp), %r14
	movq	-512(%rbp), %rbx
	leaq	-416(%rbp), %rdi
	movq	64(%r14), %rsi
	addq	%rbx, %rsi
	call	_ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE0EEC2EPS3_PKhj.constprop.0
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-191(%rbp), %r15d
	movl	%r15d, %eax
	addl	-412(%rbp), %eax
	jc	.L3455
	movq	16(%r12), %rdx
	movq	(%rdx), %rdx
	movq	31(%rdx), %rcx
	cmpq	$1, %rcx
	jbe	.L3455
	movl	%eax, %eax
	subq	$2, %rcx
	cmpq	%rcx, %rax
	ja	.L3455
	andq	39(%rdx), %rax
	addq	23(%rdx), %rax
	je	.L3455
	movzwl	(%rax), %eax
	movb	$2, -192(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	$0, -183(%rbp)
	movq	%rax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm2
	movb	%al, 16(%rsp)
	movups	%xmm2, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movl	-408(%rbp), %eax
	addq	$32, %rsp
	addl	-492(%rbp), %eax
	cmpb	$0, _ZN2v88internal22FLAG_trace_wasm_memoryE(%rip)
	movl	%eax, -492(%rbp)
	jne	.L3996
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	cltq
	jmp	.L3137
.L3100:
	movq	-504(%rbp), %r14
	movq	-512(%rbp), %rbx
	leaq	-416(%rbp), %rdi
	movq	64(%r14), %rsi
	addq	%rbx, %rsi
	call	_ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE0EEC2EPS3_PKhj.constprop.0
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-191(%rbp), %r15d
	movl	%r15d, %eax
	addl	-412(%rbp), %eax
	jc	.L3455
	movq	16(%r12), %rdx
	movq	(%rdx), %rdx
	movq	31(%rdx), %rcx
	cmpq	$1, %rcx
	jbe	.L3455
	movl	%eax, %eax
	subq	$2, %rcx
	cmpq	%rcx, %rax
	ja	.L3455
	andq	39(%rdx), %rax
	addq	23(%rdx), %rax
	je	.L3455
	movswq	(%rax), %rax
	movb	$2, -192(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	$0, -183(%rbp)
	movq	%rax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm3
	movb	%al, 16(%rsp)
	movups	%xmm3, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movl	-408(%rbp), %eax
	addq	$32, %rsp
	addl	-492(%rbp), %eax
	cmpb	$0, _ZN2v88internal22FLAG_trace_wasm_memoryE(%rip)
	movl	%eax, -492(%rbp)
	jne	.L3997
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	cltq
	jmp	.L3137
.L2977:
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-191(%rbp), %eax
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	$0, -183(%rbp)
	movq	%rax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movb	$2, -192(%rbp)
	movdqa	-192(%rbp), %xmm6
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3105:
	movq	-504(%rbp), %r14
	movq	-512(%rbp), %rbx
	leaq	-416(%rbp), %rdi
	movq	64(%r14), %rsi
	addq	%rbx, %rsi
	call	_ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE0EEC2EPS3_PKhj.constprop.1
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-191(%rbp), %r15d
	movl	%r15d, %eax
	addl	-412(%rbp), %eax
	jc	.L3455
	movq	16(%r12), %rdx
	movq	(%rdx), %rdx
	movq	31(%rdx), %rcx
	testq	%rcx, %rcx
	je	.L3455
	movl	%eax, %eax
	subq	$1, %rcx
	cmpq	%rcx, %rax
	ja	.L3455
	andq	39(%rdx), %rax
	addq	23(%rdx), %rax
	je	.L3455
	movzbl	(%rax), %eax
	pxor	%xmm0, %xmm0
	subq	$32, %rsp
	movq	%r12, %rdi
	movups	%xmm0, -191(%rbp)
	movb	$1, -192(%rbp)
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm2
	movb	%al, 16(%rsp)
	movups	%xmm2, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movl	-408(%rbp), %eax
	addq	$32, %rsp
	addl	-492(%rbp), %eax
	cmpb	$0, _ZN2v88internal22FLAG_trace_wasm_memoryE(%rip)
	movl	%eax, -492(%rbp)
	jne	.L3998
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	cltq
	jmp	.L3137
.L3106:
	movq	-504(%rbp), %r14
	movq	-512(%rbp), %rbx
	leaq	-416(%rbp), %rdi
	movq	64(%r14), %rsi
	addq	%rbx, %rsi
	call	_ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE0EEC2EPS3_PKhj.constprop.1
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-191(%rbp), %r15d
	movl	%r15d, %eax
	addl	-412(%rbp), %eax
	jc	.L3455
	movq	16(%r12), %rdx
	movq	(%rdx), %rdx
	movq	31(%rdx), %rcx
	testq	%rcx, %rcx
	je	.L3455
	movl	%eax, %eax
	subq	$1, %rcx
	cmpq	%rcx, %rax
	ja	.L3455
	andq	39(%rdx), %rax
	addq	23(%rdx), %rax
	je	.L3455
	movsbl	(%rax), %eax
	pxor	%xmm0, %xmm0
	subq	$32, %rsp
	movq	%r12, %rdi
	movups	%xmm0, -191(%rbp)
	movb	$1, -192(%rbp)
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm3
	movb	%al, 16(%rsp)
	movups	%xmm3, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movl	-408(%rbp), %eax
	addq	$32, %rsp
	addl	-492(%rbp), %eax
	cmpb	$0, _ZN2v88internal22FLAG_trace_wasm_memoryE(%rip)
	movl	%eax, -492(%rbp)
	jne	.L3999
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	cltq
	jmp	.L3137
.L2945:
	movq	%r12, %rsi
	leaq	-192(%rbp), %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	16(%r12), %rdx
	movl	-191(%rbp), %eax
	xorl	%esi, %esi
	movq	(%rdx), %rdx
	movq	31(%rdx), %rcx
	testq	%rcx, %rcx
	je	.L3504
	subq	$1, %rcx
	cmpq	%rcx, %rax
	ja	.L3504
	andq	39(%rdx), %rax
	addq	23(%rdx), %rax
	je	.L3504
	movzbl	(%rax), %esi
.L3504:
	pxor	%xmm0, %xmm0
	movb	$1, -192(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movups	%xmm0, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movl	%esi, -191(%rbp)
	movdqa	-192(%rbp), %xmm6
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3094:
	movq	-504(%rbp), %r15
	movq	-512(%rbp), %r14
	leaq	-416(%rbp), %rdi
	movq	64(%r15), %rsi
	addq	%r14, %rsi
	call	_ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE0EEC2EPS3_PKhj.constprop.1
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rdi, -536(%rbp)
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-536(%rbp), %rdi
	movq	%r12, %rsi
	movl	-191(%rbp), %ebx
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-191(%rbp), %edx
	movl	%edx, %eax
	addl	-412(%rbp), %eax
	jc	.L3500
	movq	16(%r12), %rcx
	movq	(%rcx), %rcx
	movq	31(%rcx), %rsi
	cmpq	$3, %rsi
	jbe	.L3500
	movl	%eax, %eax
	subq	$4, %rsi
	cmpq	%rsi, %rax
	ja	.L3500
	andq	39(%rcx), %rax
	addq	23(%rcx), %rax
	je	.L3500
	movl	%ebx, (%rax)
	movl	-408(%rbp), %eax
	addl	-492(%rbp), %eax
	cmpb	$0, _ZN2v88internal22FLAG_trace_wasm_memoryE(%rip)
	movl	%eax, -492(%rbp)
	jne	.L4000
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	cltq
	jmp	.L3137
.L3095:
	movq	-504(%rbp), %r15
	movq	-512(%rbp), %r14
	leaq	-416(%rbp), %rdi
	movq	64(%r15), %rsi
	addq	%r14, %rsi
	call	_ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE0EEC2EPS3_PKhj.constprop.0
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rdi, -536(%rbp)
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-536(%rbp), %rdi
	movq	%r12, %rsi
	movq	-191(%rbp), %rbx
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-191(%rbp), %edx
	movl	%edx, %eax
	addl	-412(%rbp), %eax
	jc	.L3500
	movq	16(%r12), %rcx
	movq	(%rcx), %rcx
	movq	31(%rcx), %rsi
	cmpq	$7, %rsi
	jbe	.L3500
	movl	%eax, %eax
	subq	$8, %rsi
	cmpq	%rsi, %rax
	ja	.L3500
	andq	39(%rcx), %rax
	addq	23(%rcx), %rax
	je	.L3500
	movq	%rbx, (%rax)
	movl	-408(%rbp), %eax
	addl	-492(%rbp), %eax
	cmpb	$0, _ZN2v88internal22FLAG_trace_wasm_memoryE(%rip)
	movl	%eax, -492(%rbp)
	jne	.L4001
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	cltq
	jmp	.L3137
.L3096:
	movq	-504(%rbp), %r15
	movq	-512(%rbp), %r14
	leaq	-416(%rbp), %rdi
	movq	64(%r15), %rsi
	addq	%r14, %rsi
	call	_ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE0EEC2EPS3_PKhj.constprop.1
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rdi, -536(%rbp)
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-536(%rbp), %rdi
	movq	%r12, %rsi
	movl	-191(%rbp), %ebx
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-191(%rbp), %edx
	movl	%edx, %eax
	addl	-412(%rbp), %eax
	jc	.L3500
	movq	16(%r12), %rcx
	movq	(%rcx), %rcx
	movq	31(%rcx), %rsi
	cmpq	$3, %rsi
	jbe	.L3500
	movl	%eax, %eax
	subq	$4, %rsi
	cmpq	%rsi, %rax
	ja	.L3500
	andq	39(%rcx), %rax
	addq	23(%rcx), %rax
	je	.L3500
	movl	%ebx, (%rax)
	movl	-408(%rbp), %eax
	addl	-492(%rbp), %eax
	cmpb	$0, _ZN2v88internal22FLAG_trace_wasm_memoryE(%rip)
	movl	%eax, -492(%rbp)
	jne	.L4002
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	cltq
	jmp	.L3137
.L3097:
	movq	-504(%rbp), %r14
	movq	-512(%rbp), %rbx
	leaq	-416(%rbp), %rdi
	movq	64(%r14), %rsi
	addq	%rbx, %rsi
	call	_ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE0EEC2EPS3_PKhj.constprop.0
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-191(%rbp), %r15d
	movl	%r15d, %eax
	addl	-412(%rbp), %eax
	jc	.L3455
	movq	16(%r12), %rdx
	movq	(%rdx), %rdx
	movq	31(%rdx), %rcx
	cmpq	$3, %rcx
	jbe	.L3455
	movl	%eax, %eax
	subq	$4, %rcx
	cmpq	%rcx, %rax
	ja	.L3455
	andq	39(%rdx), %rax
	addq	23(%rdx), %rax
	je	.L3455
	movl	(%rax), %eax
	movb	$2, -192(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	$0, -183(%rbp)
	movq	%rax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm5
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movl	-408(%rbp), %eax
	addq	$32, %rsp
	addl	-492(%rbp), %eax
	cmpb	$0, _ZN2v88internal22FLAG_trace_wasm_memoryE(%rip)
	movl	%eax, -492(%rbp)
	jne	.L4003
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	cltq
	jmp	.L3137
.L3092:
	movq	-504(%rbp), %r15
	movq	-512(%rbp), %r14
	leaq	-416(%rbp), %rdi
	movq	64(%r15), %rsi
	addq	%r14, %rsi
	call	_ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE0EEC2EPS3_PKhj.constprop.1
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rdi, -536(%rbp)
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-536(%rbp), %rdi
	movq	%r12, %rsi
	movl	-191(%rbp), %ebx
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-191(%rbp), %edx
	movl	%edx, %eax
	addl	-412(%rbp), %eax
	jc	.L3500
	movq	16(%r12), %rcx
	movq	(%rcx), %rcx
	movq	31(%rcx), %rsi
	testq	%rsi, %rsi
	je	.L3500
	movl	%eax, %eax
	subq	$1, %rsi
	cmpq	%rsi, %rax
	ja	.L3500
	andq	39(%rcx), %rax
	addq	23(%rcx), %rax
	je	.L3500
	movb	%bl, (%rax)
	movl	-408(%rbp), %eax
	addl	-492(%rbp), %eax
	cmpb	$0, _ZN2v88internal22FLAG_trace_wasm_memoryE(%rip)
	movl	%eax, -492(%rbp)
	jne	.L4004
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	cltq
	jmp	.L3137
.L3091:
	movq	-504(%rbp), %r15
	movq	-512(%rbp), %r14
	leaq	-416(%rbp), %rdi
	movq	64(%r15), %rsi
	addq	%r14, %rsi
	call	_ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE0EEC2EPS3_PKhj.constprop.1
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rdi, -536(%rbp)
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-536(%rbp), %rdi
	movq	%r12, %rsi
	movl	-191(%rbp), %ebx
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-191(%rbp), %edx
	movl	%edx, %eax
	addl	-412(%rbp), %eax
	jc	.L3500
	movq	16(%r12), %rcx
	movq	(%rcx), %rcx
	movq	31(%rcx), %rsi
	cmpq	$1, %rsi
	jbe	.L3500
	movl	%eax, %eax
	subq	$2, %rsi
	cmpq	%rsi, %rax
	ja	.L3500
	andq	39(%rcx), %rax
	addq	23(%rcx), %rax
	je	.L3500
	movw	%bx, (%rax)
	movl	-408(%rbp), %eax
	addl	-492(%rbp), %eax
	cmpb	$0, _ZN2v88internal22FLAG_trace_wasm_memoryE(%rip)
	movl	%eax, -492(%rbp)
	jne	.L4005
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	cltq
	jmp	.L3137
.L3093:
	movq	-504(%rbp), %r15
	movq	-512(%rbp), %r14
	leaq	-416(%rbp), %rdi
	movq	64(%r15), %rsi
	addq	%r14, %rsi
	call	_ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE0EEC2EPS3_PKhj.constprop.0
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rdi, -536(%rbp)
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-536(%rbp), %rdi
	movq	%r12, %rsi
	movq	-191(%rbp), %rbx
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-191(%rbp), %edx
	movl	%edx, %eax
	addl	-412(%rbp), %eax
	jc	.L3500
	movq	16(%r12), %rcx
	movq	(%rcx), %rcx
	movq	31(%rcx), %rsi
	cmpq	$7, %rsi
	jbe	.L3500
	movl	%eax, %eax
	subq	$8, %rsi
	cmpq	%rsi, %rax
	ja	.L3500
	andq	39(%rcx), %rax
	addq	23(%rcx), %rax
	je	.L3500
	movq	%rbx, (%rax)
	movl	-408(%rbp), %eax
	addl	-492(%rbp), %eax
	cmpb	$0, _ZN2v88internal22FLAG_trace_wasm_memoryE(%rip)
	movl	%eax, -492(%rbp)
	jne	.L4006
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	cltq
	jmp	.L3137
.L3102:
	movq	-504(%rbp), %r14
	movq	-512(%rbp), %rbx
	leaq	-416(%rbp), %rdi
	movq	64(%r14), %rsi
	addq	%rbx, %rsi
	call	_ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE0EEC2EPS3_PKhj.constprop.0
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-191(%rbp), %r15d
	movl	%r15d, %eax
	addl	-412(%rbp), %eax
	jc	.L3455
	movq	16(%r12), %rdx
	movq	(%rdx), %rdx
	movq	31(%rdx), %rcx
	testq	%rcx, %rcx
	je	.L3455
	movl	%eax, %eax
	subq	$1, %rcx
	cmpq	%rcx, %rax
	ja	.L3455
	andq	39(%rdx), %rax
	addq	23(%rdx), %rax
	je	.L3455
	movsbq	(%rax), %rax
	movb	$2, -192(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	$0, -183(%rbp)
	movq	%rax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm6
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movl	-408(%rbp), %eax
	addq	$32, %rsp
	addl	-492(%rbp), %eax
	cmpb	$0, _ZN2v88internal22FLAG_trace_wasm_memoryE(%rip)
	movl	%eax, -492(%rbp)
	jne	.L4007
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	cltq
	jmp	.L3137
.L2985:
	leaq	-192(%rbp), %r14
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-191(%rbp), %rbx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rbx, -536(%rbp)
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-191(%rbp), %rcx
	movq	%rcx, %xmm0
	ucomisd	%xmm0, %xmm0
	jp	.L3535
	movsd	-536(%rbp), %xmm1
	ucomisd	%xmm1, %xmm1
	jp	.L3692
	movmskpd	%xmm0, %edx
	movmskpd	%xmm1, %eax
	andl	$1, %edx
	andl	$1, %eax
	cmpb	%al, %dl
	jb	.L3535
	comisd	%xmm1, %xmm0
	maxsd	%xmm1, %xmm0
	cmovbe	%rbx, %rcx
.L3535:
	ucomisd	%xmm0, %xmm0
	movq	%rcx, -191(%rbp)
	movq	%r12, %rdi
	movq	$0, -183(%rbp)
	movb	$4, -192(%rbp)
	movdqa	-192(%rbp), %xmm2
	setp	%al
	orb	%al, 108(%r12)
	movzbl	-176(%rbp), %eax
	subq	$32, %rsp
	movups	%xmm2, (%rsp)
	movb	%al, 16(%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3115:
	movq	-504(%rbp), %rdx
	movq	-512(%rbp), %rax
	addq	64(%rdx), %rax
	movb	$0, -412(%rbp)
	movzbl	1(%rax), %ecx
	movl	%ecx, %edx
	andl	$127, %edx
	testb	%cl, %cl
	js	.L4008
	movl	$1, -408(%rbp)
.L3254:
	movq	8(%r12), %r15
	movl	%edx, -416(%rbp)
	movq	%r12, %rsi
	leaq	-384(%rbp), %rdi
	addl	$1, 41104(%r15)
	movq	41088(%r15), %r14
	movq	41096(%r15), %rbx
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	72(%r12), %rax
	movdqa	-384(%rbp), %xmm0
	movl	-416(%rbp), %edx
	movaps	%xmm0, -288(%rbp)
	movq	-287(%rbp), %rsi
	addq	-8(%rax), %rdx
	movaps	%xmm0, -224(%rbp)
	movq	%rdx, %r8
	movzbl	-224(%rbp), %ecx
	movzbl	-368(%rbp), %edx
	movq	%rsi, -223(%rbp)
	movdqa	-224(%rbp), %xmm5
	movb	%dl, -272(%rbp)
	movb	%dl, -208(%rbp)
	movb	%dl, -240(%rbp)
	movaps	%xmm5, -256(%rbp)
	cmpb	$6, %cl
	je	.L4009
.L3255:
	movq	%r8, %rax
	movb	%cl, -256(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movzbl	-240(%rbp), %ecx
	salq	$4, %rax
	movq	%rsi, -255(%rbp)
	addq	%r8, %rax
	addq	24(%r12), %rax
	movdqa	-256(%rbp), %xmm6
	movb	%cl, 16(%rax)
	movups	%xmm6, (%rax)
	movb	%dl, 16(%rsp)
	movups	%xmm0, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movl	-408(%rbp), %eax
	addq	$32, %rsp
	subl	$1, 41104(%r15)
	movq	%r14, 41088(%r15)
	addl	$1, %eax
	movl	%eax, -492(%rbp)
	cmpq	41096(%r15), %rbx
	je	.L3632
	movq	%rbx, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movl	-492(%rbp), %eax
.L3632:
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	cltq
	jmp	.L3137
.L3116:
	movq	-504(%rbp), %rdx
	movq	-512(%rbp), %rax
	addq	64(%rdx), %rax
	movb	$0, -412(%rbp)
	movzbl	1(%rax), %ecx
	movl	%ecx, %edx
	andl	$127, %edx
	testb	%cl, %cl
	js	.L4010
	movl	$1, -408(%rbp)
.L3248:
	movq	8(%r12), %r15
	movl	%edx, -416(%rbp)
	leaq	-384(%rbp), %rdi
	movq	%r12, %rsi
	addl	$1, 41104(%r15)
	movq	41088(%r15), %r14
	movq	41096(%r15), %rbx
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	72(%r12), %rax
	movl	-416(%rbp), %edx
	movdqa	-384(%rbp), %xmm0
	movq	-8(%rax), %rcx
	movzbl	-368(%rbp), %eax
	movaps	%xmm0, -288(%rbp)
	addq	%rdx, %rcx
	movq	-287(%rbp), %rdx
	movaps	%xmm0, -224(%rbp)
	movzbl	-224(%rbp), %r8d
	movb	%al, -272(%rbp)
	movq	%rdx, -223(%rbp)
	movdqa	-224(%rbp), %xmm7
	movb	%al, -208(%rbp)
	movb	%al, -240(%rbp)
	movaps	%xmm7, -256(%rbp)
	cmpb	$6, %r8b
	je	.L4011
.L3249:
	movq	%rcx, %rax
	movq	%rdx, -255(%rbp)
	movzbl	-240(%rbp), %edx
	movb	%r8b, -256(%rbp)
	salq	$4, %rax
	movdqa	-256(%rbp), %xmm5
	addq	%rcx, %rax
	addq	24(%r12), %rax
	movups	%xmm5, (%rax)
	movb	%dl, 16(%rax)
	movl	-408(%rbp), %eax
	subl	$1, 41104(%r15)
	addl	$1, %eax
	movq	%r14, 41088(%r15)
	movl	%eax, -492(%rbp)
	cmpq	41096(%r15), %rbx
	je	.L3629
	movq	%rbx, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movl	-492(%rbp), %eax
.L3629:
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	cltq
	jmp	.L3137
.L3107:
	movq	-504(%rbp), %r14
	movq	-512(%rbp), %rbx
	leaq	-416(%rbp), %rdi
	movq	64(%r14), %rsi
	addq	%rbx, %rsi
	call	_ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE0EEC2EPS3_PKhj.constprop.0
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-191(%rbp), %r15d
	movl	%r15d, %eax
	addl	-412(%rbp), %eax
	jc	.L3455
	movq	16(%r12), %rdx
	movq	(%rdx), %rdx
	movq	31(%rdx), %rcx
	cmpq	$7, %rcx
	jbe	.L3455
	movl	%eax, %eax
	subq	$8, %rcx
	cmpq	%rcx, %rax
	ja	.L3455
	andq	39(%rdx), %rax
	addq	23(%rdx), %rax
	je	.L3455
	movq	(%rax), %rax
	movb	$4, -192(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	$0, -183(%rbp)
	movq	%rax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm2
	movb	%al, 16(%rsp)
	movups	%xmm2, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movl	-408(%rbp), %eax
	addq	$32, %rsp
	addl	-492(%rbp), %eax
	cmpb	$0, _ZN2v88internal22FLAG_trace_wasm_memoryE(%rip)
	movl	%eax, -492(%rbp)
	jne	.L4012
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	cltq
	jmp	.L3137
.L3108:
	movq	-504(%rbp), %r14
	movq	-512(%rbp), %rbx
	leaq	-416(%rbp), %rdi
	movq	64(%r14), %rsi
	addq	%rbx, %rsi
	call	_ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE0EEC2EPS3_PKhj.constprop.1
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-191(%rbp), %r15d
	movl	%r15d, %eax
	addl	-412(%rbp), %eax
	jc	.L3455
	movq	16(%r12), %rdx
	movq	(%rdx), %rdx
	movq	31(%rdx), %rcx
	cmpq	$3, %rcx
	jbe	.L3455
	movl	%eax, %eax
	subq	$4, %rcx
	cmpq	%rcx, %rax
	ja	.L3455
	andq	39(%rdx), %rax
	addq	23(%rdx), %rax
	je	.L3455
	movl	(%rax), %eax
	pxor	%xmm0, %xmm0
	movb	$3, -192(%rbp)
	movq	%r12, %rdi
	movups	%xmm0, -191(%rbp)
	subq	$32, %rsp
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm3
	movb	%al, 16(%rsp)
	movups	%xmm3, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movl	-408(%rbp), %eax
	addq	$32, %rsp
	addl	-492(%rbp), %eax
	cmpb	$0, _ZN2v88internal22FLAG_trace_wasm_memoryE(%rip)
	movl	%eax, -492(%rbp)
	jne	.L4013
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	cltq
	jmp	.L3137
.L3101:
	movq	-504(%rbp), %r14
	movq	-512(%rbp), %rbx
	leaq	-416(%rbp), %rdi
	movq	64(%r14), %rsi
	addq	%rbx, %rsi
	call	_ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE0EEC2EPS3_PKhj.constprop.0
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-191(%rbp), %r15d
	movl	%r15d, %eax
	addl	-412(%rbp), %eax
	jc	.L3455
	movq	16(%r12), %rdx
	movq	(%rdx), %rdx
	movq	31(%rdx), %rcx
	testq	%rcx, %rcx
	je	.L3455
	movl	%eax, %eax
	subq	$1, %rcx
	cmpq	%rcx, %rax
	ja	.L3455
	andq	39(%rdx), %rax
	addq	23(%rdx), %rax
	je	.L3455
	movzbl	(%rax), %eax
	movb	$2, -192(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	$0, -183(%rbp)
	movq	%rax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm7
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movl	-408(%rbp), %eax
	addq	$32, %rsp
	addl	-492(%rbp), %eax
	cmpb	$0, _ZN2v88internal22FLAG_trace_wasm_memoryE(%rip)
	movl	%eax, -492(%rbp)
	jne	.L4014
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	cltq
	jmp	.L3137
.L2953:
	movq	8(%r12), %rax
	movb	$6, -192(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	$0, -183(%rbp)
	addq	$104, %rax
	movq	%rax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm5
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3104:
	movq	-504(%rbp), %r14
	movq	-512(%rbp), %rbx
	leaq	-416(%rbp), %rdi
	movq	64(%r14), %rsi
	addq	%rbx, %rsi
	call	_ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE0EEC2EPS3_PKhj.constprop.1
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-191(%rbp), %r15d
	movl	%r15d, %eax
	addl	-412(%rbp), %eax
	jc	.L3455
	movq	16(%r12), %rdx
	movq	(%rdx), %rdx
	movq	31(%rdx), %rcx
	cmpq	$1, %rcx
	jbe	.L3455
	movl	%eax, %eax
	subq	$2, %rcx
	cmpq	%rcx, %rax
	ja	.L3455
	andq	39(%rdx), %rax
	addq	23(%rdx), %rax
	je	.L3455
	movswl	(%rax), %eax
	pxor	%xmm0, %xmm0
	subq	$32, %rsp
	movq	%r12, %rdi
	movups	%xmm0, -191(%rbp)
	movb	$1, -192(%rbp)
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm4
	movb	%al, 16(%rsp)
	movups	%xmm4, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movl	-408(%rbp), %eax
	addq	$32, %rsp
	addl	-492(%rbp), %eax
	cmpb	$0, _ZN2v88internal22FLAG_trace_wasm_memoryE(%rip)
	movl	%eax, -492(%rbp)
	jne	.L4015
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	cltq
	jmp	.L3137
.L3009:
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movss	-191(%rbp), %xmm3
	movss	.LC57(%rip), %xmm2
	movss	.LC56(%rip), %xmm4
	movaps	%xmm3, %xmm1
	movaps	%xmm3, %xmm0
	andps	%xmm2, %xmm1
	ucomiss	%xmm1, %xmm4
	jbe	.L3545
	cvttss2sil	%xmm3, %eax
	pxor	%xmm1, %xmm1
	movss	.LC41(%rip), %xmm4
	andnps	%xmm3, %xmm2
	cvtsi2ssl	%eax, %xmm1
	cmpnless	%xmm1, %xmm0
	andps	%xmm4, %xmm0
	addss	%xmm1, %xmm0
	orps	%xmm2, %xmm0
.L3545:
	ucomiss	%xmm0, %xmm0
	pxor	%xmm1, %xmm1
	movb	$3, -192(%rbp)
	movq	%r12, %rdi
	movups	%xmm1, -191(%rbp)
	movd	%xmm0, -191(%rbp)
	movdqa	-192(%rbp), %xmm3
	setp	%al
	orb	%al, 108(%r12)
	movzbl	-176(%rbp), %eax
	subq	$32, %rsp
	movups	%xmm3, (%rsp)
	movb	%al, 16(%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L2975:
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-191(%rbp), %eax
	leaq	-64(%rbp), %rdi
	movl	%eax, -64(%rbp)
	call	_ZN2v88internal4wasm25float32_to_uint64_wrapperEm@PLT
	testl	%eax, %eax
	je	.L3607
	movq	-64(%rbp), %rax
	movb	$2, -192(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	$0, -183(%rbp)
	movq	%rax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm2
	movb	%al, 16(%rsp)
	movups	%xmm2, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L2976:
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-191(%rbp), %eax
	leaq	-64(%rbp), %rdi
	movl	%eax, -64(%rbp)
	call	_ZN2v88internal4wasm24float32_to_int64_wrapperEm@PLT
	testl	%eax, %eax
	je	.L3607
	movq	-64(%rbp), %rax
	movb	$2, -192(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	$0, -183(%rbp)
	movq	%rax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm7
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L2979:
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movsd	-191(%rbp), %xmm0
	comisd	.LC17(%rip), %xmm0
	jbe	.L3607
	movsd	.LC18(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jbe	.L3607
	cvttsd2siq	%xmm0, %rax
	pxor	%xmm1, %xmm1
	subq	$32, %rsp
	movq	%r12, %rdi
	movups	%xmm1, -191(%rbp)
	movb	$1, -192(%rbp)
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm4
	movb	%al, 16(%rsp)
	movups	%xmm4, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L2980:
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movsd	-191(%rbp), %xmm0
	comisd	.LC14(%rip), %xmm0
	jbe	.L3607
	movsd	.LC15(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jbe	.L3607
	cvttsd2sil	%xmm0, %eax
	pxor	%xmm1, %xmm1
	subq	$32, %rsp
	movq	%r12, %rdi
	movups	%xmm1, -191(%rbp)
	movb	$1, -192(%rbp)
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm3
	movb	%al, 16(%rsp)
	movups	%xmm3, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L2978:
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movslq	-191(%rbp), %rax
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	$0, -183(%rbp)
	movq	%rax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movb	$2, -192(%rbp)
	movdqa	-192(%rbp), %xmm5
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L2983:
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	pxor	%xmm0, %xmm0
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	-191(%rbp), %rax
	movb	$1, -192(%rbp)
	movups	%xmm0, -191(%rbp)
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm6
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L2984:
	leaq	-192(%rbp), %r14
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-191(%rbp), %rbx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movb	$4, -192(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movabsq	$-9223372036854775808, %rdx
	movabsq	$9223372036854775807, %rax
	andq	-191(%rbp), %rax
	movq	$0, -183(%rbp)
	andq	%rdx, %rbx
	orq	%rbx, %rax
	movq	%rax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm2
	movb	%al, 16(%rsp)
	movups	%xmm2, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L2987:
	leaq	-256(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-224(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movsd	-223(%rbp), %xmm0
	divsd	-255(%rbp), %xmm0
	ucomisd	%xmm0, %xmm0
	movq	$0, -183(%rbp)
	movq	%r12, %rdi
	movb	$4, -192(%rbp)
	movq	%xmm0, -191(%rbp)
	setp	%al
	subq	$32, %rsp
	movdqa	-192(%rbp), %xmm5
	orb	%al, 108(%r12)
	movzbl	-176(%rbp), %eax
	movups	%xmm5, (%rsp)
	movb	%al, 16(%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L2988:
	leaq	-256(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	leaq	-224(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movsd	-255(%rbp), %xmm0
	mulsd	-223(%rbp), %xmm0
	movq	%r12, %rdi
	movq	$0, -183(%rbp)
	movb	$4, -192(%rbp)
	movq	%xmm0, -191(%rbp)
	ucomisd	%xmm0, %xmm0
	movdqa	-192(%rbp), %xmm3
	setp	%al
	orb	%al, 108(%r12)
	movzbl	-176(%rbp), %eax
	subq	$32, %rsp
	movups	%xmm3, (%rsp)
	movb	%al, 16(%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L2986:
	leaq	-192(%rbp), %r14
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-191(%rbp), %rbx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rbx, -536(%rbp)
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-191(%rbp), %rcx
	movq	%rcx, %xmm1
	ucomisd	%xmm1, %xmm1
	jp	.L3691
	movsd	-536(%rbp), %xmm0
	ucomisd	%xmm0, %xmm0
	jp	.L3534
	movmskpd	%xmm1, %edx
	movmskpd	%xmm0, %eax
	andl	$1, %edx
	andl	$1, %eax
	cmpb	%al, %dl
	jb	.L3534
	comisd	%xmm0, %xmm1
	minsd	%xmm1, %xmm0
	cmovbe	%rcx, %rbx
.L3534:
	ucomisd	%xmm0, %xmm0
	movb	$4, -192(%rbp)
	movq	%r12, %rdi
	movq	$0, -183(%rbp)
	movq	%rbx, -191(%rbp)
	movdqa	-192(%rbp), %xmm3
	setp	%al
	orb	%al, 108(%r12)
	movzbl	-176(%rbp), %eax
	subq	$32, %rsp
	movups	%xmm3, (%rsp)
	movb	%al, 16(%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3111:
	movq	-504(%rbp), %rdx
	movq	-512(%rbp), %rax
	movabsq	$4294967296, %rsi
	addq	64(%rdx), %rax
	movq	%rsi, -448(%rbp)
	movzbl	1(%rax), %ecx
	movl	%ecx, %edx
	andl	$127, %edx
	testb	%cl, %cl
	js	.L4016
.L3381:
	movq	8(%r12), %rax
	movl	%edx, -448(%rbp)
	movq	%rax, -416(%rbp)
	movq	41088(%rax), %rcx
	movq	%rcx, -408(%rbp)
	movq	41096(%rax), %rcx
	movq	%rcx, -400(%rbp)
	addl	$1, 41104(%rax)
	movq	16(%r12), %rax
	movq	8(%r12), %rbx
	movq	(%rax), %rcx
	leal	16(,%rdx,8), %eax
	cltq
	movq	199(%rcx), %rdx
	movq	-1(%rax,%rdx), %r15
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L3382
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r15
	movq	%rax, %r14
.L3383:
	movq	23(%r15), %rax
	leaq	-192(%rbp), %r15
	movq	%r12, %rsi
	movq	%r15, %rdi
	movslq	11(%rax), %rbx
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-191(%rbp), %rcx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rcx, -536(%rbp)
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-191(%rbp), %edx
	movq	-536(%rbp), %rcx
	cmpl	%ebx, %edx
	jnb	.L3939
	movq	8(%r12), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal15WasmTableObject3SetEPNS0_7IsolateENS0_6HandleIS1_EEjNS4_INS0_6ObjectEEE@PLT
	movl	-444(%rbp), %eax
	movq	-416(%rbp), %rdi
	addl	$1, %eax
	movl	%eax, -492(%rbp)
	testq	%rdi, %rdi
	je	.L3386
	movq	-408(%rbp), %rcx
	movq	-400(%rbp), %rdx
	subl	$1, 41104(%rdi)
	movq	%rcx, 41088(%rdi)
	cmpq	41096(%rdi), %rdx
	je	.L3386
	movq	%rdx, 41096(%rdi)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movl	-492(%rbp), %eax
.L3386:
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	cltq
	jmp	.L3137
.L3112:
	movq	-504(%rbp), %rdx
	movq	-512(%rbp), %rax
	movabsq	$4294967296, %rsi
	addq	64(%rdx), %rax
	movq	%rsi, -448(%rbp)
	movzbl	1(%rax), %ecx
	movl	%ecx, %edx
	andl	$127, %edx
	testb	%cl, %cl
	js	.L4017
.L3374:
	movq	8(%r12), %rax
	movl	%edx, -448(%rbp)
	movq	%rax, -416(%rbp)
	movq	41088(%rax), %rcx
	movq	%rcx, -408(%rbp)
	movq	41096(%rax), %rcx
	movq	%rcx, -400(%rbp)
	addl	$1, 41104(%rax)
	movq	16(%r12), %rax
	movq	8(%r12), %rbx
	movq	(%rax), %rcx
	leal	16(,%rdx,8), %eax
	cltq
	movq	199(%rcx), %rdx
	movq	-1(%rax,%rdx), %r14
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L3375
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r14
	movq	%rax, %r15
.L3376:
	movq	23(%r14), %rax
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	movslq	11(%rax), %rbx
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-191(%rbp), %edx
	cmpl	%ebx, %edx
	jnb	.L3939
	movq	8(%r12), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal15WasmTableObject3GetEPNS0_7IsolateENS0_6HandleIS1_EEj@PLT
	movb	$6, -192(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	$0, -183(%rbp)
	movq	%rax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm7
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movl	-444(%rbp), %eax
	movq	-416(%rbp), %rdi
	addq	$32, %rsp
	addl	$1, %eax
	movl	%eax, -492(%rbp)
	testq	%rdi, %rdi
	je	.L3379
	movq	-408(%rbp), %rcx
	movq	-400(%rbp), %rdx
	subl	$1, 41104(%rdi)
	movq	%rcx, 41088(%rdi)
	cmpq	41096(%rdi), %rdx
	je	.L3379
	movq	%rdx, 41096(%rdi)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movl	-492(%rbp), %eax
.L3379:
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	cltq
	jmp	.L3137
.L2947:
	leaq	-192(%rbp), %r14
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-191(%rbp), %ebx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-191(%rbp), %eax
	testl	%ebx, %ebx
	je	.L3540
	xorl	%edx, %edx
	divl	%ebx
	movl	%edx, %ebx
.L3540:
	pxor	%xmm0, %xmm0
	movb	$1, -192(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movups	%xmm0, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movl	%ebx, -191(%rbp)
	movdqa	-192(%rbp), %xmm7
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L2948:
	leaq	-192(%rbp), %r14
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-191(%rbp), %ebx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-191(%rbp), %eax
	xorl	%edx, %edx
	leal	1(%rbx), %ecx
	cmpl	$1, %ecx
	jbe	.L3539
	cltd
	idivl	%ebx
.L3539:
	movb	$1, -192(%rbp)
	pxor	%xmm0, %xmm0
	subq	$32, %rsp
	movq	%r12, %rdi
	movups	%xmm0, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movl	%edx, -191(%rbp)
	movdqa	-192(%rbp), %xmm6
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L2946:
	movq	%r12, %rsi
	leaq	-192(%rbp), %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	16(%r12), %rdx
	movl	-191(%rbp), %eax
	xorl	%esi, %esi
	movq	(%rdx), %rdx
	movq	31(%rdx), %rcx
	testq	%rcx, %rcx
	je	.L3503
	subq	$1, %rcx
	cmpq	%rcx, %rax
	ja	.L3503
	andq	39(%rdx), %rax
	addq	23(%rdx), %rax
	je	.L3503
	movsbl	(%rax), %esi
.L3503:
	pxor	%xmm0, %xmm0
	movb	$1, -192(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movups	%xmm0, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movl	%esi, -191(%rbp)
	movdqa	-192(%rbp), %xmm5
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L2951:
	movq	-504(%rbp), %rdx
	movq	-512(%rbp), %rax
	movabsq	$4294967296, %rsi
	addq	64(%rdx), %rax
	movq	%rsi, -416(%rbp)
	movzbl	1(%rax), %ecx
	movl	%ecx, %edx
	andl	$127, %edx
	testb	%cl, %cl
	js	.L4018
.L3238:
	movq	8(%r12), %r14
	movl	%edx, -416(%rbp)
	addl	$1, 41104(%r14)
	movq	16(%r12), %rsi
	movq	8(%r12), %rdi
	movq	41088(%r14), %r15
	movq	41096(%r14), %rbx
	call	_ZN2v88internal18WasmInstanceObject31GetOrCreateWasmExternalFunctionEPNS0_7IsolateENS0_6HandleIS1_EEi@PLT
	movb	$6, -192(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	$0, -183(%rbp)
	movq	%rax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm6
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movl	-412(%rbp), %eax
	addq	$32, %rsp
	subl	$1, 41104(%r14)
	movq	%r15, 41088(%r14)
	addl	$1, %eax
	movl	%eax, -492(%rbp)
	cmpq	41096(%r14), %rbx
	je	.L3239
	movq	%rbx, 41096(%r14)
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movl	-492(%rbp), %eax
.L3239:
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	cltq
	jmp	.L3137
.L2952:
	movq	8(%r12), %r15
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	addl	$1, 41104(%r15)
	movq	41088(%r15), %r14
	movq	41096(%r15), %rbx
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-191(%rbp), %rax
	xorl	%edx, %edx
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L4019
.L3519:
	pxor	%xmm0, %xmm0
	movb	$1, -192(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movups	%xmm0, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movl	%edx, -191(%rbp)
	movdqa	-192(%rbp), %xmm7
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movq	%r14, 41088(%r15)
	addq	$32, %rsp
	subl	$1, 41104(%r15)
	cmpq	41096(%r15), %rbx
	je	.L3638
	movq	%rbx, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3638:
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	jmp	.L3137
.L2950:
	leaq	-192(%rbp), %r14
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-191(%rbp), %ebx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-191(%rbp), %eax
	xorl	%ecx, %ecx
	testl	%ebx, %ebx
	je	.L3536
	cmpl	$-1, %ebx
	jne	.L3722
	cmpl	$-2147483648, %eax
	je	.L3694
.L3722:
	cltd
	idivl	%ebx
	movl	%eax, %ecx
.L3536:
	pxor	%xmm0, %xmm0
	movb	$1, -192(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	movups	%xmm0, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movl	%ecx, -191(%rbp)
	movdqa	-192(%rbp), %xmm4
	movb	%al, 16(%rsp)
	movups	%xmm4, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L2955:
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movswq	-191(%rbp), %rax
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	$0, -183(%rbp)
	movq	%rax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movb	$2, -192(%rbp)
	movdqa	-192(%rbp), %xmm6
	movb	%al, 16(%rsp)
	movups	%xmm6, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L2956:
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movsbq	-191(%rbp), %rax
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	$0, -183(%rbp)
	movq	%rax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movb	$2, -192(%rbp)
	movdqa	-192(%rbp), %xmm5
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L2954:
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movslq	-191(%rbp), %rax
	subq	$32, %rsp
	movq	%r12, %rdi
	movq	$0, -183(%rbp)
	movq	%rax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movb	$2, -192(%rbp)
	movdqa	-192(%rbp), %xmm7
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3103:
	movq	-504(%rbp), %r14
	movq	-512(%rbp), %rbx
	leaq	-416(%rbp), %rdi
	movq	64(%r14), %rsi
	addq	%rbx, %rsi
	call	_ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE0EEC2EPS3_PKhj.constprop.1
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-191(%rbp), %r15d
	movl	%r15d, %eax
	addl	-412(%rbp), %eax
	jc	.L3455
	movq	16(%r12), %rdx
	movq	(%rdx), %rdx
	movq	31(%rdx), %rcx
	cmpq	$1, %rcx
	jbe	.L3455
	movl	%eax, %eax
	subq	$2, %rcx
	cmpq	%rcx, %rax
	ja	.L3455
	andq	39(%rdx), %rax
	addq	23(%rdx), %rax
	je	.L3455
	movzwl	(%rax), %eax
	pxor	%xmm0, %xmm0
	subq	$32, %rsp
	movq	%r12, %rdi
	movups	%xmm0, -191(%rbp)
	movb	$1, -192(%rbp)
	movl	%eax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm5
	movb	%al, 16(%rsp)
	movups	%xmm5, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movl	-408(%rbp), %eax
	addq	$32, %rsp
	addl	-492(%rbp), %eax
	cmpb	$0, _ZN2v88internal22FLAG_trace_wasm_memoryE(%rip)
	movl	%eax, -492(%rbp)
	jne	.L4020
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	cltq
	jmp	.L3137
.L3352:
	movq	8(%r12), %rax
	movq	%rax, -416(%rbp)
	movq	41088(%rax), %rdx
	movq	%rdx, -408(%rbp)
	movq	41096(%rax), %rdx
	movq	%rdx, -400(%rbp)
	addl	$1, 41104(%rax)
	movq	16(%r12), %rax
	cmpb	$0, 1(%r14)
	movq	8(%r12), %r15
	movq	(%rax), %rax
	je	.L3363
	cmpb	$0, 28(%r14)
	jne	.L4021
.L3363:
	movq	41112(%r15), %rdi
	movq	175(%rax), %rbx
	testq	%rdi, %rdi
	je	.L3368
	movq	%rbx, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rbx
.L3369:
	movl	24(%r14), %r15d
.L3367:
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-191(%rbp), %rax
	movq	(%rax), %r14
	leal	16(,%r15,8), %eax
	cltq
	leaq	-1(%rbx,%rax), %r15
	movq	%r14, (%r15)
	testb	$1, %r14b
	je	.L3634
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -536(%rbp)
	testl	$262144, %eax
	je	.L3372
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-536(%rbp), %rcx
	movq	8(%rcx), %rax
.L3372:
	testb	$24, %al
	je	.L3634
	movq	%rbx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3634
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
.L3634:
	leaq	-416(%rbp), %rdi
	call	_ZN2v88internal11HandleScopeD1Ev
.L3359:
	movl	-432(%rbp), %eax
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	addl	$1, %eax
	movl	%eax, -492(%rbp)
	cltq
	jmp	.L3137
.L3355:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl12GetGlobalPtrEPKNS1_10WasmGlobalE
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rax, %rbx
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movq	-191(%rbp), %rax
	movq	%rax, (%rbx)
	jmp	.L3359
.L3340:
	movq	72(%r12), %rax
	movq	-512(%rbp), %rdx
	movl	$4, 88(%r12)
	movl	$8, 104(%r12)
	movq	%rdx, -16(%rax)
	jmp	.L3947
.L3341:
	movq	72(%r12), %rax
	movq	-512(%rbp), %rdx
	movl	$4, 88(%r12)
	movl	$7, 104(%r12)
	movq	%rdx, -16(%rax)
	jmp	.L3947
.L3300:
	movq	%r9, %rdx
	leaq	-512(%rbp), %rcx
	movq	%r12, %rdi
	movq	%r9, %r15
	leaq	-144(%rbp), %rsi
	leaq	-488(%rbp), %r8
	call	_ZN2v88internal4wasm10ThreadImpl6DoCallEPNS1_7DecoderEPNS1_15InterpreterCodeEPmS7_
	movl	%eax, %edx
	testb	%al, %al
	je	.L3947
.L3943:
	movzbl	109(%r12), %eax
	movq	%r15, -504(%rbp)
	andl	$2, %eax
.L3940:
	movzbl	-513(%rbp), %esi
	movl	$0, %eax
	cmovne	%eax, %r13d
	cmovne	%edx, %esi
	movb	%sil, -513(%rbp)
	jmp	.L3155
.L3297:
	movzbl	109(%r12), %eax
	movzbl	-513(%rbp), %esi
	movl	$1, %edx
	andl	$2, %eax
	movl	$0, %eax
	cmovne	%eax, %r13d
	movl	-536(%rbp), %eax
	cmovne	%edx, %esi
	addl	%ebx, %eax
	movb	%sil, -513(%rbp)
	movl	%eax, -492(%rbp)
.L3303:
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	cltq
	jmp	.L3137
.L3336:
	movq	72(%r12), %rax
	movq	-24(%rax), %rdx
	movq	%rdx, -504(%rbp)
	movq	-16(%rax), %rdx
	movq	%rdx, -512(%rbp)
	movq	-24(%rax), %rcx
	movq	72(%rcx), %rdx
	subq	64(%rcx), %rdx
	movq	%rdx, -488(%rbp)
	movq	-24(%rax), %rax
	movl	$0, -112(%rbp)
	movq	72(%rax), %rdx
	movdqu	64(%rax), %xmm0
	leaq	-168(%rbp), %rax
	movl	$0, -188(%rbp)
	movq	%rax, -184(%rbp)
	movq	%rdx, -120(%rbp)
	movq	-96(%rbp), %rdx
	punpcklqdq	%xmm0, %xmm0
	movl	$0, -192(%rbp)
	movb	$0, -168(%rbp)
	movl	$0, -104(%rbp)
	movq	$0, -88(%rbp)
	movups	%xmm0, -136(%rbp)
	movb	$0, (%rdx)
	movq	-184(%rbp), %rdx
	movq	$0, -176(%rbp)
	movb	$0, (%rdx)
	movq	-184(%rbp), %rdi
	cmpq	%rax, %rdi
	je	.L3335
	call	_ZdlPv@PLT
.L3335:
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3339:
	movq	-504(%rbp), %rax
	addl	%r14d, %ebx
	leaq	-504(%rbp), %rdx
	leaq	-512(%rbp), %rcx
	movl	%ebx, -492(%rbp)
	leaq	-144(%rbp), %rsi
	leaq	-488(%rbp), %r8
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax), %r9
	call	_ZN2v88internal4wasm10ThreadImpl8DoReturnEPNS1_7DecoderEPPNS1_15InterpreterCodeEPmS8_m
	movl	%eax, %edx
	testb	%al, %al
	je	.L3947
	movzbl	109(%r12), %eax
	movzbl	-513(%rbp), %esi
	andl	$2, %eax
	movl	$0, %eax
	cmovne	%edx, %esi
	cmovne	%eax, %r13d
	movb	%sil, -513(%rbp)
	jmp	.L3335
.L3342:
	movq	%r9, %rdx
	leaq	-512(%rbp), %rcx
	movq	%r12, %rdi
	movq	%r9, %r15
	leaq	-144(%rbp), %rsi
	leaq	-488(%rbp), %r8
	call	_ZN2v88internal4wasm10ThreadImpl12DoReturnCallEPNS1_7DecoderEPNS1_15InterpreterCodeEPmS7_
	movl	%eax, %edx
	testb	%al, %al
	je	.L3947
	jmp	.L3943
.L3356:
	movq	16(%r12), %rax
	cmpb	$0, 1(%r14)
	movl	24(%r14), %ebx
	movq	(%rax), %rax
	je	.L3360
	cmpb	$0, 28(%r14)
	jne	.L4022
.L3360:
	addq	103(%rax), %rbx
.L3361:
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movl	-191(%rbp), %eax
	movl	%eax, (%rbx)
	jmp	.L3359
.L3354:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl12GetGlobalPtrEPKNS1_10WasmGlobalE
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rax, %rbx
	call	_ZN2v88internal4wasm10ThreadImpl3PopEv
	movdqu	-191(%rbp), %xmm3
	movups	%xmm3, (%rbx)
	jmp	.L3359
.L3358:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl12GetGlobalPtrEPKNS1_10WasmGlobalE
	movq	%rax, %rbx
	jmp	.L3361
.L3980:
	testl	%eax, %eax
	je	.L3264
.L3278:
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3187:
	movb	$0, -412(%rbp)
	jmp	.L3188
	.p2align 4,,10
	.p2align 3
.L3455:
	movl	$4, 88(%r12)
	movq	72(%r12), %rax
	movl	$1, 104(%r12)
	movq	%rbx, -16(%rax)
	jmp	.L3947
	.p2align 4,,10
	.p2align 3
.L3500:
	movl	$4, 88(%r12)
	movq	72(%r12), %rax
	movl	$1, 104(%r12)
	movq	%r14, -16(%rax)
	jmp	.L3947
	.p2align 4,,10
	.p2align 3
.L3607:
	movq	72(%r12), %rax
	movq	-512(%rbp), %rdx
	movl	$4, 88(%r12)
	movl	$6, 104(%r12)
	movq	%rdx, -16(%rax)
	jmp	.L3947
.L3531:
	movq	72(%r12), %rax
	movq	-512(%rbp), %rdx
	movl	$4, 88(%r12)
	movl	$5, 104(%r12)
	movq	%rdx, -16(%rax)
	jmp	.L3947
.L3216:
	cmpq	%r15, %rcx
	je	.L3214
	cmpq	32(%rcx), %rdx
	cmovnb	%rcx, %r15
.L3214:
	movq	24(%r12), %rdx
	movq	40(%r12), %rax
	movabsq	$-1085102592571150095, %r11
	movl	48(%r15), %r10d
	movl	44(%r15), %ecx
	subq	%rdx, %rax
	imulq	%r11, %rax
	movl	%r10d, -536(%rbp)
	movq	%rax, %r14
	subq	%rcx, %r14
	testq	%r10, %r10
	je	.L3219
	cmpq	%rcx, %r10
	je	.L3219
	subq	%r10, %rax
	movq	%r10, %r9
	movq	%r14, %rdi
	movq	%r10, -552(%rbp)
	movq	%rax, %rsi
	salq	$4, %r9
	movq	%rax, -544(%rbp)
	salq	$4, %rsi
	salq	$4, %rdi
	addq	%r10, %r9
	addq	%rax, %rsi
	addq	%r14, %rdi
	addq	%rdx, %rsi
	addq	%rdx, %rdi
	movq	%r9, %rdx
	call	memmove@PLT
	movq	48(%r12), %rax
	movq	-544(%rbp), %rcx
	leal	16(,%r14,8), %edx
	movslq	%edx, %rdx
	movl	-536(%rbp), %r8d
	movl	$4, %r9d
	movq	(%rax), %rax
	leal	16(,%rcx,8), %ecx
	movslq	%ecx, %rcx
	movq	7(%rax), %rsi
	leaq	-1(%rsi), %rax
	addq	%rax, %rdx
	addq	%rax, %rcx
	movq	8(%r12), %rax
	leaq	37592(%rax), %rdi
	call	_ZN2v88internal4Heap9MoveRangeENS0_10HeapObjectENS0_14FullObjectSlotES3_iNS0_16WriteBarrierModeE@PLT
	movq	24(%r12), %rdx
	movq	40(%r12), %rax
	movabsq	$-1085102592571150095, %r11
	movq	-552(%rbp), %r10
	subq	%rdx, %rax
	imulq	%r11, %rax
.L3219:
	movq	48(%r12), %rcx
	addq	%r14, %r10
	movl	%eax, %edi
	movq	(%rcx), %rcx
	movq	7(%rcx), %rcx
	cmpl	%eax, %r10d
	jge	.L3220
	leal	16(,%r10,8), %edx
	movq	%rcx, %rsi
	movslq	%edx, %rdx
	andq	$-262144, %rsi
	leaq	-1(%rcx,%rdx), %rax
	movl	%r10d, %edx
	addq	$24, %rsi
	notl	%edx
	addl	%edi, %edx
	movslq	%r10d, %rdi
	addq	%rdi, %rdx
	leaq	23(%rcx,%rdx,8), %rcx
	.p2align 4,,10
	.p2align 3
.L3221:
	movq	(%rsi), %rdx
	addq	$8, %rax
	movq	-37496(%rdx), %rdx
	movq	%rdx, -8(%rax)
	cmpq	%rax, %rcx
	jne	.L3221
	movq	24(%r12), %rdx
.L3220:
	movq	%r10, %rax
	movq	-512(%rbp), %rcx
	salq	$4, %rax
	addq	%r10, %rax
	addq	%rdx, %rax
	movq	%rax, 40(%r12)
	movl	40(%r15), %eax
	movq	-504(%rbp), %r15
	addl	%ebx, %eax
	movl	%eax, -492(%rbp)
	cltq
	jmp	.L3137
.L3147:
	cmpq	%rdx, %rsi
	je	.L3145
	cmpq	32(%rdx), %rcx
	cmovb	%rsi, %rdx
.L3145:
	movslq	40(%rdx), %rax
	movl	%eax, -492(%rbp)
	jmp	.L3137
.L3194:
	cmpq	%rcx, %rbx
	je	.L3192
	cmpq	%rdx, 32(%rcx)
	cmovbe	%rcx, %rbx
.L3192:
	movq	24(%r12), %rcx
	movq	40(%r12), %rax
	movabsq	$-1085102592571150095, %r10
	movl	48(%rbx), %r15d
	movl	44(%rbx), %edx
	subq	%rcx, %rax
	imulq	%r10, %rax
	movl	%r15d, -536(%rbp)
	movq	%rax, %r14
	subq	%rdx, %r14
	testq	%r15, %r15
	je	.L3197
	cmpq	%rdx, %r15
	je	.L3197
	subq	%r15, %rax
	movq	%r14, %rdi
	movq	%r15, %rdx
	movq	%rax, %rsi
	salq	$4, %rdi
	movq	%rax, -544(%rbp)
	salq	$4, %rsi
	salq	$4, %rdx
	addq	%r14, %rdi
	addq	%rax, %rsi
	addq	%rcx, %rdi
	addq	%r15, %rdx
	addq	%rcx, %rsi
	call	memmove@PLT
	movq	48(%r12), %rax
	movq	-544(%rbp), %r9
	leal	16(,%r14,8), %edx
	movslq	%edx, %rdx
	movl	-536(%rbp), %r8d
	movq	(%rax), %rax
	leal	16(,%r9,8), %ecx
	movl	$4, %r9d
	movslq	%ecx, %rcx
	movq	7(%rax), %rsi
	leaq	-1(%rsi), %rax
	addq	%rax, %rcx
	addq	%rax, %rdx
	movq	8(%r12), %rax
	leaq	37592(%rax), %rdi
	call	_ZN2v88internal4Heap9MoveRangeENS0_10HeapObjectENS0_14FullObjectSlotES3_iNS0_16WriteBarrierModeE@PLT
	movq	24(%r12), %rcx
	movq	40(%r12), %rax
	movabsq	$-1085102592571150095, %r10
	subq	%rcx, %rax
	imulq	%r10, %rax
.L3197:
	movq	48(%r12), %rdx
	addq	%r15, %r14
	movq	(%rdx), %rdx
	movq	7(%rdx), %rdi
	movl	%eax, %edx
	cmpl	%eax, %r14d
	jge	.L3198
	movl	%r14d, %ecx
	movq	%rdi, %rsi
	leal	16(,%r14,8), %eax
	notl	%ecx
	andq	$-262144, %rsi
	cltq
	addl	%ecx, %edx
	movslq	%r14d, %rcx
	leaq	-1(%rdi,%rax), %rax
	addq	$24, %rsi
	addq	%rcx, %rdx
	leaq	23(%rdi,%rdx,8), %rcx
	.p2align 4,,10
	.p2align 3
.L3199:
	movq	(%rsi), %rdx
	addq	$8, %rax
	movq	-37496(%rdx), %rdx
	movq	%rdx, -8(%rax)
	cmpq	%rax, %rcx
	jne	.L3199
	movq	24(%r12), %rcx
.L3198:
	movq	%r14, %rax
	movq	-504(%rbp), %r15
	salq	$4, %rax
	addq	%r14, %rax
	addq	%rcx, %rax
	movq	-512(%rbp), %rcx
	movq	%rax, 40(%r12)
	movslq	40(%rbx), %rax
	movl	%eax, -492(%rbp)
	jmp	.L3137
.L3721:
	cqto
	movb	$2, -192(%rbp)
	subq	$32, %rsp
	movq	%r12, %rdi
	idivq	%rbx
	movq	$0, -183(%rbp)
	movq	%rax, -191(%rbp)
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm7
	movb	%al, 16(%rsp)
	movups	%xmm7, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	addq	$32, %rsp
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3528:
	movq	72(%r12), %rax
	movq	-512(%rbp), %rdx
	movl	$4, 88(%r12)
	movl	$3, 104(%r12)
	movq	%rdx, -16(%rax)
	jmp	.L3947
.L3686:
	movl	$3, %eax
	jmp	.L3526
.L3264:
	leaq	-512(%rbp), %rcx
	leaq	-144(%rbp), %rsi
	movq	%r15, %rdx
	movq	%r12, %rdi
	leaq	-488(%rbp), %r8
	call	_ZN2v88internal4wasm10ThreadImpl6DoCallEPNS1_7DecoderEPNS1_15InterpreterCodeEPmS7_
	testb	%al, %al
	je	.L3947
	jmp	.L3942
.L3936:
	movsd	.LC62(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jbe	.L3937
	xorl	%edx, %edx
	comisd	.LC63(%rip), %xmm0
	sbbl	%eax, %eax
	notl	%eax
	subl	$8388608, %eax
	jmp	.L3559
.L3310:
	movq	72(%r12), %rax
	movq	-512(%rbp), %rdx
	leaq	-448(%rbp), %rbx
	movq	%rbx, %rdi
	movq	%rdx, -16(%rax)
	movq	(%r15), %rax
	movq	8(%r12), %r14
	movl	8(%rax), %esi
	movq	41088(%r14), %rax
	addl	$1, 41104(%r14)
	movq	8(%r12), %rdx
	movl	%esi, -552(%rbp)
	movq	%rax, -560(%rbp)
	movq	41096(%r14), %rax
	movq	%rdx, -544(%rbp)
	movq	%rax, -536(%rbp)
	movq	16(%r12), %rax
	movl	%esi, -440(%rbp)
	movq	%rax, -448(%rbp)
	call	_ZN2v88internal21ImportedFunctionEntry10object_refEv@PLT
	movq	-544(%rbp), %rdx
	movq	%rax, %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L3313
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L3314:
	movq	%rbx, %rdi
	call	_ZN2v88internal21ImportedFunctionEntry6targetEv@PLT
	movq	8(%r12), %rbx
	movq	%rax, %rsi
	movq	45752(%rbx), %rax
	movq	%rsi, -544(%rbp)
	leaq	280(%rax), %rdi
	call	_ZNK2v88internal4wasm15WasmCodeManager18LookupNativeModuleEm@PLT
	movq	-544(%rbp), %rsi
	movq	504(%rax), %rdx
	movq	%rax, %r8
	movq	%r8, %rdi
	movq	(%rdx), %rax
	cmpq	%rsi, %rax
	ja	.L3316
	addq	8(%rdx), %rax
	cmpq	%rax, %rsi
	jnb	.L3316
	movq	%r8, -544(%rbp)
	call	_ZNK2v88internal4wasm12NativeModule33GetFunctionIndexFromJumpTableSlotEm@PLT
	movq	-544(%rbp), %r8
	movl	%eax, %esi
	movl	%eax, -568(%rbp)
	movq	%r8, %rdi
	call	_ZNK2v88internal4wasm12NativeModule7HasCodeEj@PLT
	movq	-544(%rbp), %r8
	movl	-568(%rbp), %r10d
	testb	%al, %al
	jne	.L3317
	movl	%r10d, %edx
	movq	%r8, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11CompileLazyEPNS0_7IsolateEPNS1_12NativeModuleEi@PLT
	movq	-544(%rbp), %r8
	movl	-568(%rbp), %r10d
	testb	%al, %al
	je	.L4023
.L3317:
	movl	%r10d, %esi
	movq	%r8, %rdi
	call	_ZNK2v88internal4wasm12NativeModule7GetCodeEj@PLT
	movq	%rax, %rcx
.L3319:
	movq	8(%r12), %rsi
	testq	%rcx, %rcx
	je	.L3318
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	8(%rax), %rdx
	movl	-552(%rbp), %eax
	salq	$5, %rax
	addq	136(%rdx), %rax
	movq	%r15, %rdx
	movq	(%rax), %r8
	call	_ZN2v88internal4wasm10ThreadImpl24CallExternalWasmFunctionEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEPKNS1_8WasmCodeEPNS0_9SignatureINS1_9ValueTypeEEE
	movl	%eax, %ebx
.L3322:
	movq	-560(%rbp), %rax
	subl	$1, 41104(%r14)
	movq	%rax, 41088(%r14)
	movq	-536(%rbp), %rax
	cmpq	41096(%r14), %rax
	je	.L3625
	movq	%rax, 41096(%r14)
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3625:
	cmpl	$4, %ebx
	je	.L3947
	ja	.L3324
	cmpl	$3, %ebx
	jne	.L3278
	movl	-400(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -492(%rbp)
.L3327:
	movq	-504(%rbp), %rax
	leaq	-504(%rbp), %rdx
	leaq	-512(%rbp), %rcx
	movq	%r12, %rdi
	leaq	-144(%rbp), %rsi
	leaq	-488(%rbp), %r8
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax), %r9
	call	_ZN2v88internal4wasm10ThreadImpl8DoReturnEPNS1_7DecoderEPPNS1_15InterpreterCodeEPmS8_m
	movl	%eax, %edx
	testb	%al, %al
	je	.L3947
	movzbl	109(%r12), %eax
	movq	-504(%rbp), %r15
	andl	$1, %eax
	jmp	.L3940
.L3175:
	cmpq	%rcx, %r14
	je	.L3173
	cmpq	32(%rcx), %rdx
	cmovnb	%rcx, %r14
.L3173:
	movq	24(%r12), %rsi
	movq	40(%r12), %rax
	movabsq	$-1085102592571150095, %r10
	movl	48(%r14), %ebx
	movl	44(%r14), %edx
	subq	%rsi, %rax
	imulq	%r10, %rax
	movl	%ebx, -536(%rbp)
	movq	%rax, %r11
	subq	%rdx, %r11
	testq	%rbx, %rbx
	je	.L3178
	cmpq	%rdx, %rbx
	je	.L3178
	subq	%rbx, %rax
	movq	%r11, %rdi
	movq	%rbx, %rdx
	movq	%r11, -544(%rbp)
	movq	%rax, %r9
	salq	$4, %rdi
	movq	%rax, -568(%rbp)
	salq	$4, %r9
	addq	%r11, %rdi
	salq	$4, %rdx
	addq	%rax, %r9
	addq	%rsi, %rdi
	addq	%rbx, %rdx
	addq	%rsi, %r9
	movq	%r9, %rsi
	call	memmove@PLT
	movq	48(%r12), %rax
	movq	-544(%rbp), %r11
	movl	$4, %r9d
	movq	-568(%rbp), %rcx
	movl	-536(%rbp), %r8d
	movq	(%rax), %rax
	leal	16(,%r11,8), %edx
	leal	16(,%rcx,8), %ecx
	movslq	%edx, %rdx
	movq	7(%rax), %rsi
	movslq	%ecx, %rcx
	leaq	-1(%rsi), %rax
	addq	%rax, %rdx
	addq	%rax, %rcx
	movq	8(%r12), %rax
	leaq	37592(%rax), %rdi
	call	_ZN2v88internal4Heap9MoveRangeENS0_10HeapObjectENS0_14FullObjectSlotES3_iNS0_16WriteBarrierModeE@PLT
	movq	24(%r12), %rsi
	movq	40(%r12), %rax
	movabsq	$-1085102592571150095, %r10
	movq	-544(%rbp), %r11
	subq	%rsi, %rax
	imulq	%r10, %rax
.L3178:
	movq	48(%r12), %rdx
	addq	%rbx, %r11
	movq	(%rdx), %rdx
	movq	7(%rdx), %rcx
	movl	%eax, %edx
	cmpl	%eax, %r11d
	jge	.L3179
	movl	%r11d, %edi
	movq	%rcx, %rsi
	leal	16(,%r11,8), %eax
	notl	%edi
	andq	$-262144, %rsi
	cltq
	addl	%edi, %edx
	movslq	%r11d, %rdi
	leaq	-1(%rcx,%rax), %rax
	addq	$24, %rsi
	addq	%rdi, %rdx
	leaq	23(%rcx,%rdx,8), %rcx
	.p2align 4,,10
	.p2align 3
.L3180:
	movq	(%rsi), %rdx
	addq	$8, %rax
	movq	-37496(%rdx), %rdx
	movq	%rdx, -8(%rax)
	cmpq	%rcx, %rax
	jne	.L3180
	movq	24(%r12), %rsi
.L3179:
	movq	%r11, %rax
	salq	$4, %rax
	addq	%rax, %r11
	leaq	(%rsi,%r11), %rax
	movq	%rax, 40(%r12)
	movl	40(%r14), %eax
	movl	%eax, -492(%rbp)
.L3181:
	movq	-560(%rbp), %rsi
	subl	$1, 41104(%r15)
	movq	%rsi, 41088(%r15)
	movq	-552(%rbp), %rsi
	cmpq	41096(%r15), %rsi
	je	.L3621
	movq	%rsi, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movl	-492(%rbp), %eax
.L3621:
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	cltq
	jmp	.L3137
.L3276:
	cmpl	$5, %eax
	je	.L3326
	movl	-492(%rbp), %eax
	jmp	.L3283
	.p2align 4,,10
	.p2align 3
.L3171:
	movzbl	-176(%rbp), %eax
	movdqa	-192(%rbp), %xmm4
	subq	$32, %rsp
	movq	%r12, %rdi
	movb	%al, 16(%rsp)
	movups	%xmm4, (%rsp)
	call	_ZN2v88internal4wasm10ThreadImpl4PushENS1_9WasmValueE
	movl	-536(%rbp), %eax
	addq	$32, %rsp
	addl	-544(%rbp), %eax
	movl	%eax, -492(%rbp)
	jmp	.L3181
.L3324:
	cmpl	$5, %ebx
	je	.L3326
	jmp	.L3327
.L3156:
	movq	72(%r12), %rax
	movq	-24(%rax), %rdx
	movq	%rdx, -504(%rbp)
	movq	-16(%rax), %rdx
	movq	%rdx, -512(%rbp)
	movq	-24(%rax), %rcx
	movq	72(%rcx), %rdx
	subq	64(%rcx), %rdx
	movq	%rdx, -488(%rbp)
	movq	-24(%rax), %rax
	movl	$0, -112(%rbp)
	movq	72(%rax), %rdx
	movdqu	64(%rax), %xmm0
	leaq	-168(%rbp), %rax
	movl	$0, -188(%rbp)
	movq	%rax, -184(%rbp)
	movq	%rdx, -120(%rbp)
	movq	-96(%rbp), %rdx
	punpcklqdq	%xmm0, %xmm0
	movl	$0, -192(%rbp)
	movb	$0, -168(%rbp)
	movl	$0, -104(%rbp)
	movq	$0, -88(%rbp)
	movups	%xmm0, -136(%rbp)
	movb	$0, (%rdx)
	movq	-184(%rbp), %rdx
	movq	$0, -176(%rbp)
	movb	$0, (%rdx)
	movq	-184(%rbp), %rdi
	cmpq	%rax, %rdi
	je	.L3158
	call	_ZdlPv@PLT
.L3158:
	subl	$1, 41104(%r15)
	movq	%r14, 41088(%r15)
	cmpq	41096(%r15), %rbx
	je	.L3328
	movq	%rbx, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	jmp	.L3328
.L3515:
	movq	41088(%rcx), %rsi
	cmpq	41096(%rcx), %rsi
	je	.L4024
.L3517:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rcx)
	movq	%r8, (%rsi)
	jmp	.L3516
.L3382:
	movq	41088(%rbx), %r14
	cmpq	%r14, 41096(%rbx)
	je	.L4025
.L3384:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%rbx)
	movq	%r15, (%r14)
	jmp	.L3383
.L3375:
	movq	41088(%rbx), %r15
	cmpq	41096(%rbx), %r15
	je	.L4026
.L3377:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%rbx)
	movq	%r14, (%r15)
	jmp	.L3376
.L3165:
	movq	41088(%rdi), %rax
	cmpq	41096(%rdi), %rax
	je	.L4027
.L3168:
	leaq	8(%rax), %r9
	movq	%r9, 41088(%rdi)
	movq	%rsi, (%rax)
	cmpq	%rcx, %rax
	jne	.L3170
	jmp	.L3167
.L3975:
	cmpl	$-52, %ecx
	jl	.L3584
	movabsq	$4503599627370495, %rax
	movabsq	$4503599627370496, %rcx
	andq	%rdx, %rax
	addq	%rcx, %rax
	movl	$1075, %ecx
	subl	%esi, %ecx
	shrq	%cl, %rax
	jmp	.L3586
.L3973:
	cmpl	$-52, %ecx
	jl	.L3592
	movabsq	$4503599627370495, %rax
	movabsq	$4503599627370496, %rcx
	andq	%rdx, %rax
	addq	%rcx, %rax
	movl	$1075, %ecx
	subl	%esi, %ecx
	shrq	%cl, %rax
	jmp	.L3594
.L3969:
	cmpl	$-52, %ecx
	jl	.L3576
	movabsq	$4503599627370495, %rax
	movabsq	$4503599627370496, %rcx
	andq	%rdx, %rax
	addq	%rcx, %rax
	movl	$1075, %ecx
	subl	%esi, %ecx
	shrq	%cl, %rax
	jmp	.L3578
.L3971:
	cmpl	$-52, %ecx
	jl	.L3568
	movabsq	$4503599627370495, %rax
	movabsq	$4503599627370496, %rcx
	andq	%rdx, %rax
	addq	%rcx, %rax
	movl	$1075, %ecx
	subl	%esi, %ecx
	shrq	%cl, %rax
	jmp	.L3570
.L3268:
	call	_ZNK2v88internal4wasm12NativeModule6LookupEm@PLT
	movq	%rax, %rcx
	jmp	.L3271
.L3316:
	call	_ZNK2v88internal4wasm12NativeModule6LookupEm@PLT
	movq	%rax, %rcx
	jmp	.L3319
.L4019:
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	%rax, -37488(%rdx)
	sete	%dl
	movzbl	%dl, %edx
	jmp	.L3519
.L3960:
	movq	-504(%rbp), %rax
	movq	-512(%rbp), %rdx
	movq	80(%rax), %rax
	leaq	16(%rax), %rbx
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L3203
	movq	%rbx, %rcx
	jmp	.L3204
.L4028:
	movq	%rax, %rcx
	movq	16(%rax), %rax
.L3207:
	testq	%rax, %rax
	je	.L3205
.L3204:
	cmpq	32(%rax), %rdx
	jbe	.L4028
	movq	24(%rax), %rax
	jmp	.L3207
.L3205:
	cmpq	%rcx, %rbx
	je	.L3203
	cmpq	32(%rcx), %rdx
	cmovnb	%rcx, %rbx
.L3203:
	movq	24(%r12), %rsi
	movq	40(%r12), %rax
	movabsq	$-1085102592571150095, %r10
	movl	48(%rbx), %r15d
	movl	44(%rbx), %edx
	subq	%rsi, %rax
	imulq	%r10, %rax
	movl	%r15d, -544(%rbp)
	movq	%rax, %r14
	subq	%rdx, %r14
	testq	%r15, %r15
	je	.L3208
	cmpq	%rdx, %r15
	je	.L3208
	subq	%r15, %rax
	movq	%r14, %rdi
	movq	%r15, %rdx
	movq	%rax, %r9
	salq	$4, %rdi
	movq	%rax, -536(%rbp)
	salq	$4, %r9
	salq	$4, %rdx
	addq	%r14, %rdi
	addq	%rax, %r9
	addq	%rsi, %rdi
	addq	%r15, %rdx
	addq	%rsi, %r9
	movq	%r9, %rsi
	call	memmove@PLT
	movq	48(%r12), %rax
	movq	-536(%rbp), %rcx
	leal	16(,%r14,8), %edx
	movslq	%edx, %rdx
	movl	-544(%rbp), %r8d
	movl	$4, %r9d
	movq	(%rax), %rax
	leal	16(,%rcx,8), %ecx
	movslq	%ecx, %rcx
	movq	7(%rax), %rsi
	leaq	-1(%rsi), %rax
	addq	%rax, %rdx
	addq	%rax, %rcx
	movq	8(%r12), %rax
	leaq	37592(%rax), %rdi
	call	_ZN2v88internal4Heap9MoveRangeENS0_10HeapObjectENS0_14FullObjectSlotES3_iNS0_16WriteBarrierModeE@PLT
	movq	24(%r12), %rsi
	movq	40(%r12), %rax
	movabsq	$-1085102592571150095, %r10
	subq	%rsi, %rax
	imulq	%r10, %rax
.L3208:
	movq	48(%r12), %rdx
	addq	%r15, %r14
	movq	(%rdx), %rdx
	movq	7(%rdx), %rcx
	movl	%eax, %edx
	cmpl	%eax, %r14d
	jge	.L3209
	movl	%r14d, %edi
	movq	%rcx, %rsi
	leal	16(,%r14,8), %eax
	notl	%edi
	andq	$-262144, %rsi
	cltq
	addl	%edi, %edx
	movslq	%r14d, %rdi
	leaq	-1(%rcx,%rax), %rax
	addq	$24, %rsi
	addq	%rdi, %rdx
	leaq	23(%rcx,%rdx,8), %rcx
	.p2align 4,,10
	.p2align 3
.L3210:
	movq	(%rsi), %rdx
	addq	$8, %rax
	movq	-37496(%rdx), %rdx
	movq	%rdx, -8(%rax)
	cmpq	%rax, %rcx
	jne	.L3210
	movq	24(%r12), %rsi
.L3209:
	movq	%r14, %rax
	salq	$4, %rax
	addq	%rax, %r14
	leaq	(%rsi,%r14), %rax
	movq	%rax, 40(%r12)
	movl	40(%rbx), %eax
	movl	%eax, -492(%rbp)
	jmp	.L3211
.L3985:
	cmpq	$0, 64(%r15)
	je	.L3307
	movq	(%rbx), %rdi
	movq	16(%rdi), %r14
	movq	24(%rdi), %rax
	subq	%r14, %rax
	cmpq	$63, %rax
	jbe	.L4029
	leaq	64(%r14), %rax
	movq	%rax, 16(%rdi)
.L3309:
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	movq	%r15, %rcx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm9SideTableC1EPNS0_4ZoneEPKNS1_10WasmModuleEPNS1_15InterpreterCodeE
	movq	%r14, 80(%r15)
	jmp	.L3307
.L3978:
	cmpq	$0, 64(%r15)
	je	.L3261
	movq	(%rbx), %rdi
	movq	16(%rdi), %r14
	movq	24(%rdi), %rax
	subq	%r14, %rax
	cmpq	$63, %rax
	jbe	.L4030
	leaq	64(%r14), %rax
	movq	%rax, 16(%rdi)
.L3263:
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	movq	%r15, %rcx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm9SideTableC1EPNS0_4ZoneEPKNS1_10WasmModuleEPNS1_15InterpreterCodeE
	movq	%r14, 80(%r15)
	jmp	.L3261
.L3138:
	movq	-504(%rbp), %r15
	movq	-512(%rbp), %rcx
	movq	80(%r15), %rax
	leaq	16(%rax), %rsi
	movq	24(%rax), %rax
	movq	%rsi, %rdx
	testq	%rax, %rax
	jne	.L3141
	jmp	.L3140
	.p2align 4,,10
	.p2align 3
.L4031:
	movq	%rax, %rdx
	movq	16(%rax), %rax
.L3144:
	testq	%rax, %rax
	je	.L3142
.L3141:
	cmpq	32(%rax), %rcx
	jbe	.L4031
	movq	24(%rax), %rax
	jmp	.L3144
.L3142:
	cmpq	%rdx, %rsi
	je	.L3140
	cmpq	32(%rdx), %rcx
	cmovb	%rsi, %rdx
.L3140:
	movl	40(%rdx), %edx
	movl	%edx, -492(%rbp)
	jmp	.L3139
.L3966:
	movl	$2, -536(%rbp)
	movzbl	2(%rsi), %ecx
	movl	%ecx, %eax
	sall	$7, %eax
	andl	$16256, %eax
	orl	%eax, %ebx
	testb	%cl, %cl
	jns	.L3164
	movl	$3, -536(%rbp)
	movzbl	3(%rsi), %ecx
	movl	%ecx, %eax
	sall	$14, %eax
	andl	$2080768, %eax
	orl	%eax, %ebx
	testb	%cl, %cl
	jns	.L3164
	movl	$4, -536(%rbp)
	movzbl	4(%rsi), %ecx
	movl	%ecx, %eax
	sall	$21, %eax
	andl	$266338304, %eax
	orl	%eax, %ebx
	testb	%cl, %cl
	jns	.L3164
	movl	$5, -536(%rbp)
	movzbl	5(%rsi), %eax
	sall	$28, %eax
	orl	%eax, %ebx
	jmp	.L3164
	.p2align 4,,10
	.p2align 3
.L3159:
	testb	%cl, %cl
	js	.L3161
	movl	$3, -544(%rbp)
	movzbl	3(%rax), %ecx
	leaq	2(%rax), %rsi
	jmp	.L3160
.L3959:
	leaq	-412(%rbp), %rsi
	leaq	2(%rax), %rdi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE0ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi1EEET_PKhPjPKcS7_.isra.0
	movl	%eax, %edx
	jmp	.L3201
.L4018:
	leaq	-412(%rbp), %rsi
	leaq	2(%rax), %rdi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE0ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi1EEET_PKhPjPKcS7_.isra.0
	movl	%eax, %edx
	jmp	.L3238
.L3987:
	leaq	-400(%rbp), %rsi
	leaq	2(%rax), %rdi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE0ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi1EEET_PKhPjPKcS7_.isra.0
	movl	%eax, %edx
	jmp	.L3348
.L3983:
	movzbl	2(%rsi), %edx
	movl	$2, %r14d
	movl	%edx, %eax
	sall	$7, %eax
	andl	$16256, %eax
	orl	%eax, %r15d
	testb	%dl, %dl
	jns	.L3334
	movzbl	3(%rsi), %edx
	movl	$3, %r14d
	movl	%edx, %eax
	sall	$14, %eax
	andl	$2080768, %eax
	orl	%eax, %r15d
	testb	%dl, %dl
	jns	.L3334
	movzbl	4(%rsi), %edx
	movl	$4, %r14d
	movl	%edx, %eax
	sall	$21, %eax
	andl	$266338304, %eax
	orl	%eax, %r15d
	testb	%dl, %dl
	jns	.L3334
	movzbl	5(%rsi), %eax
	movl	$5, %r14d
	sall	$28, %eax
	orl	%eax, %r15d
	jmp	.L3334
	.p2align 4,,10
	.p2align 3
.L3329:
	movl	%edx, %esi
	sall	$7, %esi
	andl	$16256, %esi
	orl	%esi, %ecx
	testb	%dl, %dl
	js	.L3331
	movzbl	3(%rax), %edx
	leaq	2(%rax), %rsi
	movl	$3, %ebx
	jmp	.L3330
.L3937:
	cvtsd2ss	%xmm0, %xmm0
	ucomiss	%xmm0, %xmm0
	movd	%xmm0, %eax
	setp	%dl
	jmp	.L3559
.L3984:
	leaq	-400(%rbp), %rsi
	leaq	2(%rax), %rdi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE0ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi1EEET_PKhPjPKcS7_.isra.0
	movl	%eax, %edx
	jmp	.L3306
.L3965:
	leaq	-400(%rbp), %r8
	leaq	2(%rsi), %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE0ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi1EEET_PKhPjPKcS7_.isra.0
	movl	%eax, %edx
	movq	-512(%rbp), %rax
	jmp	.L3151
.L3991:
	leaq	-408(%rbp), %rsi
	leaq	2(%rax), %rdi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE0ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi1EEET_PKhPjPKcS7_.isra.0
	movl	%eax, %edx
	jmp	.L3241
.L3986:
	leaq	-432(%rbp), %rsi
	leaq	2(%rax), %rdi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE0ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi1EEET_PKhPjPKcS7_.isra.0
	movl	%eax, %edx
	jmp	.L3351
.L3992:
	movl	%eax, %edx
	leaq	-416(%rbp), %rsi
	leaq	2(%r8), %rdi
	andl	$127, %edx
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE0ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi1EEET_PKhPjPKcS7_.isra.0
	movl	-416(%rbp), %edx
	movq	%rdx, %rax
	addq	$1, %rdx
	addl	$1, %eax
	jmp	.L3183
.L4008:
	leaq	-408(%rbp), %rsi
	leaq	2(%rax), %rdi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE0ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi1EEET_PKhPjPKcS7_.isra.0
	movl	%eax, %edx
	jmp	.L3254
.L4010:
	leaq	-408(%rbp), %rsi
	leaq	2(%rax), %rdi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE0ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi1EEET_PKhPjPKcS7_.isra.0
	movl	%eax, %edx
	jmp	.L3248
.L4016:
	leaq	-444(%rbp), %rsi
	leaq	2(%rax), %rdi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE0ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi1EEET_PKhPjPKcS7_.isra.0
	movl	%eax, %edx
	jmp	.L3381
.L3976:
	movzbl	2(%rcx), %edx
	movl	$2, %ebx
	movl	%edx, %eax
	sall	$7, %eax
	andl	$16256, %eax
	orl	%eax, %r15d
	testb	%dl, %dl
	jns	.L3292
	movzbl	3(%rcx), %edx
	movl	$3, %ebx
	movl	%edx, %eax
	sall	$14, %eax
	andl	$2080768, %eax
	orl	%eax, %r15d
	testb	%dl, %dl
	jns	.L3292
	movzbl	4(%rcx), %edx
	movl	$4, %ebx
	movl	%edx, %eax
	sall	$21, %eax
	andl	$266338304, %eax
	orl	%eax, %r15d
	testb	%dl, %dl
	jns	.L3292
	movzbl	5(%rcx), %eax
	movl	$5, %ebx
	sall	$28, %eax
	orl	%eax, %r15d
	jmp	.L3292
	.p2align 4,,10
	.p2align 3
.L3287:
	movl	%edx, %ecx
	sall	$7, %ecx
	andl	$16256, %ecx
	orl	%ecx, %r14d
	testb	%dl, %dl
	js	.L3289
	movl	$3, -536(%rbp)
	movzbl	3(%rax), %edx
	leaq	2(%rax), %rcx
	jmp	.L3288
.L3981:
	movl	%r14d, %edx
	leaq	-416(%rbp), %rsi
	leaq	2(%rax), %rdi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE0ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi1EEET_PKhPjPKcS7_.isra.0
	movl	%eax, %r14d
	jmp	.L3212
.L4017:
	leaq	-444(%rbp), %rsi
	leaq	2(%rax), %rdi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE0ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi1EEET_PKhPjPKcS7_.isra.0
	movl	%eax, %edx
	jmp	.L3374
.L3977:
	leaq	-464(%rbp), %rsi
	leaq	2(%rax), %rdi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE0ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi1EEET_PKhPjPKcS7_.isra.0
	movl	%eax, %edx
	jmp	.L3260
.L3963:
	movzbl	1(%rcx), %edi
	movq	%rdi, %rax
	salq	$7, %rax
	andl	$16256, %eax
	movq	%rax, %rsi
	movzbl	%dl, %eax
	orq	%rsi, %rax
	testb	%dil, %dil
	js	.L4032
	salq	$50, %rax
	movl	$3, %ebx
	sarq	$50, %rax
	movq	%rax, %rdx
	movl	$3, %eax
	jmp	.L3237
.L3964:
	movzbl	1(%rcx), %edi
	movl	%edi, %eax
	sall	$7, %eax
	andl	$16256, %eax
	movl	%eax, %esi
	movzbl	%dl, %eax
	orl	%esi, %eax
	testb	%dil, %dil
	js	.L4033
	sall	$18, %eax
	movl	$3, %ebx
	sarl	$18, %eax
	movl	%eax, %edx
	movl	$3, %eax
	jmp	.L3227
.L3961:
	leaq	-412(%rbp), %r9
	movl	%r8d, %edx
	leaq	2(%rsi), %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE0ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi1EEET_PKhPjPKcS7_.isra.0
	movq	-512(%rbp), %rdx
	movl	%eax, %r8d
	movq	-504(%rbp), %rax
	jmp	.L3191
.L3313:
	movq	41088(%rdx), %r15
	cmpq	41096(%rdx), %r15
	je	.L4034
.L3315:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%r15)
	jmp	.L3314
.L3265:
	movq	41088(%rdx), %rax
	movq	%rax, -536(%rbp)
	cmpq	41096(%rdx), %rax
	je	.L4035
.L3267:
	movq	-536(%rbp), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%rcx)
	jmp	.L3266
.L4009:
	movq	$0, -183(%rbp)
	movzbl	-176(%rbp), %eax
	movb	$6, -192(%rbp)
	movb	%al, -240(%rbp)
	movq	48(%r12), %rax
	movq	$0, -191(%rbp)
	movq	(%rsi), %rdx
	movq	(%rax), %rax
	movdqa	-192(%rbp), %xmm7
	movq	7(%rax), %rdi
	leal	16(,%r8,8), %eax
	movaps	%xmm7, -256(%rbp)
	cltq
	leaq	-1(%rdi,%rax), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L3633
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	8(%rax), %r9
	movq	%rax, -576(%rbp)
	testl	$262144, %r9d
	je	.L3257
	movb	%cl, -568(%rbp)
	movq	%r8, -560(%rbp)
	movq	%rdx, -552(%rbp)
	movq	%rsi, -544(%rbp)
	movq	%rdi, -536(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-576(%rbp), %rax
	movzbl	-568(%rbp), %ecx
	movq	-560(%rbp), %r8
	movq	-552(%rbp), %rdx
	movq	8(%rax), %r9
	movq	-544(%rbp), %rsi
	movq	-536(%rbp), %rdi
.L3257:
	andl	$24, %r9d
	je	.L3633
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3633
	movb	%cl, -544(%rbp)
	movq	%r8, -536(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movzbl	-544(%rbp), %ecx
	movq	-536(%rbp), %r8
.L3633:
	movdqa	-384(%rbp), %xmm0
	movzbl	-368(%rbp), %edx
	xorl	%esi, %esi
	jmp	.L3255
.L3242:
	movq	48(%r12), %rcx
	movq	8(%r12), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	(%rcx), %rcx
	movq	7(%rcx), %rcx
	movq	-1(%rax,%rcx), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L3244
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L3245:
	movq	$0, -343(%rbp)
	movq	%rax, -351(%rbp)
	movzbl	-336(%rbp), %eax
	movb	$6, -352(%rbp)
	movdqa	-352(%rbp), %xmm2
	jmp	.L3243
.L4011:
	movq	$0, -183(%rbp)
	movzbl	-176(%rbp), %eax
	movb	$6, -192(%rbp)
	movb	%al, -240(%rbp)
	movq	48(%r12), %rax
	movq	$0, -191(%rbp)
	movq	(%rdx), %rdx
	movq	(%rax), %rax
	movdqa	-192(%rbp), %xmm6
	movq	7(%rax), %rdi
	leal	16(,%rcx,8), %eax
	movaps	%xmm6, -256(%rbp)
	cltq
	leaq	-1(%rdi,%rax), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L3630
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	8(%rax), %r9
	movq	%rax, -576(%rbp)
	testl	$262144, %r9d
	je	.L3251
	movb	%r8b, -568(%rbp)
	movq	%rcx, -560(%rbp)
	movq	%rdx, -552(%rbp)
	movq	%rsi, -544(%rbp)
	movq	%rdi, -536(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-576(%rbp), %rax
	movzbl	-568(%rbp), %r8d
	movq	-560(%rbp), %rcx
	movq	-552(%rbp), %rdx
	movq	8(%rax), %r9
	movq	-544(%rbp), %rsi
	movq	-536(%rbp), %rdi
.L3251:
	andl	$24, %r9d
	je	.L3630
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3630
	movb	%r8b, -544(%rbp)
	movq	%rcx, -536(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movzbl	-544(%rbp), %r8d
	movq	-536(%rbp), %rcx
.L3630:
	xorl	%edx, %edx
	jmp	.L3249
.L3988:
	movl	-412(%rbp), %eax
	movl	%ebx, %ecx
	movl	$1, %edi
	leaq	-448(%rbp), %rsi
	addl	%r15d, %eax
	movl	%eax, -448(%rbp)
	movl	$1280, %eax
	movw	%ax, -444(%rbp)
	movq	16(%r12), %rax
	movq	(%r14), %rdx
	movq	(%rax), %rax
	movl	8(%rdx), %edx
	movq	23(%rax), %r8
	call	_ZN2v88internal4wasm20TraceMemoryOperationENS1_13ExecutionTierEPKNS1_17MemoryTracingInfoEiiPh@PLT
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3990:
	movl	-412(%rbp), %eax
	movl	$513, %r9d
	movl	%r14d, %ecx
	leaq	-448(%rbp), %rsi
	movw	%r9w, -444(%rbp)
	movl	$1, %edi
	addl	%edx, %eax
	movl	%eax, -448(%rbp)
	movq	16(%r12), %rax
	movq	(%r15), %rdx
	movq	(%rax), %rax
	movl	8(%rdx), %edx
	movq	23(%rax), %r8
	call	_ZN2v88internal4wasm20TraceMemoryOperationENS1_13ExecutionTierEPKNS1_17MemoryTracingInfoEiiPh@PLT
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3989:
	movl	-412(%rbp), %eax
	movl	%ebx, %ecx
	movl	$1, %edi
	leaq	-448(%rbp), %rsi
	addl	%r15d, %eax
	movl	%eax, -448(%rbp)
	movl	$1024, %eax
	movw	%ax, -444(%rbp)
	movq	16(%r12), %rax
	movq	(%r14), %rdx
	movq	(%rax), %rax
	movl	8(%rdx), %edx
	movq	23(%rax), %r8
	call	_ZN2v88internal4wasm20TraceMemoryOperationENS1_13ExecutionTierEPKNS1_17MemoryTracingInfoEiiPh@PLT
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	jmp	.L3137
.L4004:
	movl	-412(%rbp), %eax
	movl	$513, %r11d
	movl	%r14d, %ecx
	leaq	-448(%rbp), %rsi
	movw	%r11w, -444(%rbp)
	movl	$1, %edi
	addl	%edx, %eax
	movl	%eax, -448(%rbp)
	movq	16(%r12), %rax
	movq	(%r15), %rdx
	movq	(%rax), %rax
	movl	8(%rdx), %edx
	movq	23(%rax), %r8
	call	_ZN2v88internal4wasm20TraceMemoryOperationENS1_13ExecutionTierEPKNS1_17MemoryTracingInfoEiiPh@PLT
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	jmp	.L3137
.L4003:
	movl	-412(%rbp), %eax
	movl	%ebx, %ecx
	movl	$1, %edi
	leaq	-448(%rbp), %rsi
	addl	%r15d, %eax
	movl	%eax, -448(%rbp)
	movl	$1024, %eax
	movw	%ax, -444(%rbp)
	movq	16(%r12), %rax
	movq	(%r14), %rdx
	movq	(%rax), %rax
	movl	8(%rdx), %edx
	movq	23(%rax), %r8
	call	_ZN2v88internal4wasm20TraceMemoryOperationENS1_13ExecutionTierEPKNS1_17MemoryTracingInfoEiiPh@PLT
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	jmp	.L3137
.L4005:
	movl	-412(%rbp), %eax
	movl	$769, %r10d
	movl	%r14d, %ecx
	leaq	-448(%rbp), %rsi
	movw	%r10w, -444(%rbp)
	movl	$1, %edi
	addl	%edx, %eax
	movl	%eax, -448(%rbp)
	movq	16(%r12), %rax
	movq	(%r15), %rdx
	movq	(%rax), %rax
	movl	8(%rdx), %edx
	movq	23(%rax), %r8
	call	_ZN2v88internal4wasm20TraceMemoryOperationENS1_13ExecutionTierEPKNS1_17MemoryTracingInfoEiiPh@PLT
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	jmp	.L3137
.L4001:
	movl	-412(%rbp), %eax
	movl	$1281, %ecx
	movl	$1, %edi
	leaq	-448(%rbp), %rsi
	movw	%cx, -444(%rbp)
	movl	%r14d, %ecx
	addl	%edx, %eax
	movl	%eax, -448(%rbp)
	movq	16(%r12), %rax
	movq	(%r15), %rdx
	movq	(%rax), %rax
	movl	8(%rdx), %edx
	movq	23(%rax), %r8
	call	_ZN2v88internal4wasm20TraceMemoryOperationENS1_13ExecutionTierEPKNS1_17MemoryTracingInfoEiiPh@PLT
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	jmp	.L3137
.L4002:
	movl	-412(%rbp), %eax
	movl	$1025, %esi
	movl	%r14d, %ecx
	movl	$1, %edi
	movw	%si, -444(%rbp)
	leaq	-448(%rbp), %rsi
	addl	%edx, %eax
	movl	%eax, -448(%rbp)
	movq	16(%r12), %rax
	movq	(%r15), %rdx
	movq	(%rax), %rax
	movl	8(%rdx), %edx
	movq	23(%rax), %r8
	call	_ZN2v88internal4wasm20TraceMemoryOperationENS1_13ExecutionTierEPKNS1_17MemoryTracingInfoEiiPh@PLT
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	jmp	.L3137
.L4000:
	movl	-412(%rbp), %eax
	movl	%r14d, %ecx
	movl	$1, %edi
	leaq	-448(%rbp), %rsi
	addl	%edx, %eax
	movl	$3073, %edx
	movl	%eax, -448(%rbp)
	movq	16(%r12), %rax
	movw	%dx, -444(%rbp)
	movq	(%r15), %rdx
	movq	(%rax), %rax
	movl	8(%rdx), %edx
	movq	23(%rax), %r8
	call	_ZN2v88internal4wasm20TraceMemoryOperationENS1_13ExecutionTierEPKNS1_17MemoryTracingInfoEiiPh@PLT
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3994:
	movl	-412(%rbp), %eax
	movl	$769, %r8d
	movl	%r14d, %ecx
	leaq	-448(%rbp), %rsi
	movw	%r8w, -444(%rbp)
	movl	$1, %edi
	addl	%edx, %eax
	movl	%eax, -448(%rbp)
	movq	16(%r12), %rax
	movq	(%r15), %rdx
	movq	(%rax), %rax
	movl	8(%rdx), %edx
	movq	23(%rax), %r8
	call	_ZN2v88internal4wasm20TraceMemoryOperationENS1_13ExecutionTierEPKNS1_17MemoryTracingInfoEiiPh@PLT
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	jmp	.L3137
.L4012:
	movl	-412(%rbp), %eax
	movl	%ebx, %ecx
	movl	$1, %edi
	leaq	-448(%rbp), %rsi
	addl	%r15d, %eax
	movl	$3328, %r15d
	movl	%eax, -448(%rbp)
	movq	16(%r12), %rax
	movw	%r15w, -444(%rbp)
	movq	(%r14), %rdx
	movq	(%rax), %rax
	movl	8(%rdx), %edx
	movq	23(%rax), %r8
	call	_ZN2v88internal4wasm20TraceMemoryOperationENS1_13ExecutionTierEPKNS1_17MemoryTracingInfoEiiPh@PLT
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	jmp	.L3137
.L4015:
	movl	-412(%rbp), %eax
	movl	$768, %r9d
	movl	%ebx, %ecx
	leaq	-448(%rbp), %rsi
	movw	%r9w, -444(%rbp)
	movl	$1, %edi
	addl	%r15d, %eax
	movl	%eax, -448(%rbp)
	movq	16(%r12), %rax
	movq	(%r14), %rdx
	movq	(%rax), %rax
	movl	8(%rdx), %edx
	movq	23(%rax), %r8
	call	_ZN2v88internal4wasm20TraceMemoryOperationENS1_13ExecutionTierEPKNS1_17MemoryTracingInfoEiiPh@PLT
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	jmp	.L3137
.L4006:
	movl	-412(%rbp), %eax
	movl	%r14d, %ecx
	movl	$1, %edi
	leaq	-448(%rbp), %rsi
	addl	%edx, %eax
	movl	%eax, -448(%rbp)
	movl	$3329, %eax
	movw	%ax, -444(%rbp)
	movq	16(%r12), %rax
	movq	(%r15), %rdx
	movq	(%rax), %rax
	movl	8(%rdx), %edx
	movq	23(%rax), %r8
	call	_ZN2v88internal4wasm20TraceMemoryOperationENS1_13ExecutionTierEPKNS1_17MemoryTracingInfoEiiPh@PLT
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	jmp	.L3137
.L4020:
	movl	-412(%rbp), %eax
	movl	$768, %r8d
	movl	%ebx, %ecx
	leaq	-448(%rbp), %rsi
	movw	%r8w, -444(%rbp)
	movl	$1, %edi
	addl	%r15d, %eax
	movl	%eax, -448(%rbp)
	movq	16(%r12), %rax
	movq	(%r14), %rdx
	movq	(%rax), %rax
	movl	8(%rdx), %edx
	movq	23(%rax), %r8
	call	_ZN2v88internal4wasm20TraceMemoryOperationENS1_13ExecutionTierEPKNS1_17MemoryTracingInfoEiiPh@PLT
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3995:
	movl	-412(%rbp), %eax
	movl	%ebx, %ecx
	movl	$1, %edi
	leaq	-448(%rbp), %rsi
	addl	%r15d, %eax
	movl	%eax, -448(%rbp)
	movl	$1024, %eax
	movw	%ax, -444(%rbp)
	movq	16(%r12), %rax
	movq	(%r14), %rdx
	movq	(%rax), %rax
	movl	8(%rdx), %edx
	movq	23(%rax), %r8
	call	_ZN2v88internal4wasm20TraceMemoryOperationENS1_13ExecutionTierEPKNS1_17MemoryTracingInfoEiiPh@PLT
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3998:
	movl	-412(%rbp), %eax
	movl	$512, %r10d
	movl	%ebx, %ecx
	leaq	-448(%rbp), %rsi
	movw	%r10w, -444(%rbp)
	movl	$1, %edi
	addl	%r15d, %eax
	movl	%eax, -448(%rbp)
	movq	16(%r12), %rax
	movq	(%r14), %rdx
	movq	(%rax), %rax
	movl	8(%rdx), %edx
	movq	23(%rax), %r8
	call	_ZN2v88internal4wasm20TraceMemoryOperationENS1_13ExecutionTierEPKNS1_17MemoryTracingInfoEiiPh@PLT
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	jmp	.L3137
.L4014:
	movl	-412(%rbp), %eax
	movl	$768, %esi
	movl	%ebx, %ecx
	movl	$1, %edi
	movw	%si, -444(%rbp)
	leaq	-448(%rbp), %rsi
	addl	%r15d, %eax
	movl	%eax, -448(%rbp)
	movq	16(%r12), %rax
	movq	(%r14), %rdx
	movq	(%rax), %rax
	movl	8(%rdx), %edx
	movq	23(%rax), %r8
	call	_ZN2v88internal4wasm20TraceMemoryOperationENS1_13ExecutionTierEPKNS1_17MemoryTracingInfoEiiPh@PLT
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3996:
	movl	-412(%rbp), %eax
	movl	$768, %edx
	movl	%ebx, %ecx
	leaq	-448(%rbp), %rsi
	movw	%dx, -444(%rbp)
	movl	$1, %edi
	addl	%r15d, %eax
	movl	%eax, -448(%rbp)
	movq	16(%r12), %rax
	movq	(%r14), %rdx
	movq	(%rax), %rax
	movl	8(%rdx), %edx
	movq	23(%rax), %r8
	call	_ZN2v88internal4wasm20TraceMemoryOperationENS1_13ExecutionTierEPKNS1_17MemoryTracingInfoEiiPh@PLT
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	jmp	.L3137
.L4013:
	movl	-412(%rbp), %eax
	movl	%ebx, %ecx
	movl	$1, %edi
	leaq	-448(%rbp), %rsi
	addl	%r15d, %eax
	movl	%eax, -448(%rbp)
	movl	$3072, %eax
	movw	%ax, -444(%rbp)
	movq	16(%r12), %rax
	movq	(%r14), %rdx
	movq	(%rax), %rax
	movl	8(%rdx), %edx
	movq	23(%rax), %r8
	call	_ZN2v88internal4wasm20TraceMemoryOperationENS1_13ExecutionTierEPKNS1_17MemoryTracingInfoEiiPh@PLT
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3993:
	movl	-412(%rbp), %eax
	movl	$1025, %edi
	movl	%r14d, %ecx
	leaq	-448(%rbp), %rsi
	movw	%di, -444(%rbp)
	movl	$1, %edi
	addl	%edx, %eax
	movl	%eax, -448(%rbp)
	movq	16(%r12), %rax
	movq	(%r15), %rdx
	movq	(%rax), %rax
	movl	8(%rdx), %edx
	movq	23(%rax), %r8
	call	_ZN2v88internal4wasm20TraceMemoryOperationENS1_13ExecutionTierEPKNS1_17MemoryTracingInfoEiiPh@PLT
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3997:
	movl	-412(%rbp), %eax
	movl	$768, %ecx
	movl	$1, %edi
	leaq	-448(%rbp), %rsi
	movw	%cx, -444(%rbp)
	movl	%ebx, %ecx
	addl	%r15d, %eax
	movl	%eax, -448(%rbp)
	movq	16(%r12), %rax
	movq	(%r14), %rdx
	movq	(%rax), %rax
	movl	8(%rdx), %edx
	movq	23(%rax), %r8
	call	_ZN2v88internal4wasm20TraceMemoryOperationENS1_13ExecutionTierEPKNS1_17MemoryTracingInfoEiiPh@PLT
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	jmp	.L3137
.L4007:
	movl	-412(%rbp), %eax
	movl	$512, %edi
	movl	%ebx, %ecx
	leaq	-448(%rbp), %rsi
	movw	%di, -444(%rbp)
	movl	$1, %edi
	addl	%r15d, %eax
	movl	%eax, -448(%rbp)
	movq	16(%r12), %rax
	movq	(%r14), %rdx
	movq	(%rax), %rax
	movl	8(%rdx), %edx
	movq	23(%rax), %r8
	call	_ZN2v88internal4wasm20TraceMemoryOperationENS1_13ExecutionTierEPKNS1_17MemoryTracingInfoEiiPh@PLT
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3999:
	movl	-412(%rbp), %eax
	movl	$512, %r11d
	movl	%ebx, %ecx
	leaq	-448(%rbp), %rsi
	movw	%r11w, -444(%rbp)
	movl	$1, %edi
	addl	%r15d, %eax
	movl	%eax, -448(%rbp)
	movq	16(%r12), %rax
	movq	(%r14), %rdx
	movq	(%rax), %rax
	movl	8(%rdx), %edx
	movq	23(%rax), %r8
	call	_ZN2v88internal4wasm20TraceMemoryOperationENS1_13ExecutionTierEPKNS1_17MemoryTracingInfoEiiPh@PLT
	movslq	-492(%rbp), %rax
	movq	-512(%rbp), %rcx
	movq	-504(%rbp), %r15
	jmp	.L3137
.L3979:
	movq	8(%r12), %rsi
.L3270:
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm10ThreadImpl15HandleExceptionEPNS0_7IsolateE
	cmpl	$1, %eax
	setne	%al
	movzbl	%al, %eax
	addl	$4, %eax
	jmp	.L3274
.L3293:
	movl	-492(%rbp), %eax
	jmp	.L3303
.L4023:
	movq	8(%r12), %rsi
.L3318:
	movq	%r12, %rdi
	xorl	%ebx, %ebx
	call	_ZN2v88internal4wasm10ThreadImpl15HandleExceptionEPNS0_7IsolateE
	cmpl	$1, %eax
	setne	%bl
	addl	$4, %ebx
	jmp	.L3322
.L3331:
	movzbl	3(%rax), %esi
	movl	%esi, %edx
	sall	$14, %edx
	andl	$2080768, %edx
	orl	%edx, %ecx
	testb	%sil, %sil
	js	.L3332
	movzbl	4(%rax), %edx
	leaq	3(%rax), %rsi
	movl	$4, %ebx
	jmp	.L3330
.L3161:
	cmpb	$0, 3(%rax)
	js	.L3162
	movl	$4, -544(%rbp)
	movzbl	4(%rax), %ecx
	leaq	3(%rax), %rsi
	jmp	.L3160
.L3289:
	movzbl	3(%rax), %ecx
	movl	%ecx, %edx
	sall	$14, %edx
	andl	$2080768, %edx
	orl	%edx, %r14d
	testb	%cl, %cl
	js	.L3290
	movl	$4, -536(%rbp)
	movzbl	4(%rax), %edx
	leaq	3(%rax), %rcx
	jmp	.L3288
.L4033:
	movzbl	2(%rcx), %esi
	movl	%esi, %edx
	sall	$14, %edx
	andl	$2080768, %edx
	orl	%edx, %eax
	testb	%sil, %sil
	js	.L4036
	sall	$11, %eax
	movl	$4, %ebx
	sarl	$11, %eax
	movl	%eax, %edx
	movl	$4, %eax
	jmp	.L3227
.L4026:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r15
	jmp	.L3377
.L3244:
	movq	41088(%rdx), %rax
	cmpq	41096(%rdx), %rax
	je	.L4037
.L3246:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%rdx)
	movq	%rsi, (%rax)
	jmp	.L3245
.L4024:
	movq	%rcx, %rdi
	movq	%r8, -552(%rbp)
	movl	%edx, -544(%rbp)
	movq	%rcx, -536(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-552(%rbp), %r8
	movl	-544(%rbp), %edx
	movq	-536(%rbp), %rcx
	movq	%rax, %rsi
	jmp	.L3517
.L4027:
	movq	%rcx, -584(%rbp)
	movq	%rsi, -576(%rbp)
	movq	%rdi, -568(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-584(%rbp), %rcx
	movq	-576(%rbp), %rsi
	movq	-568(%rbp), %rdi
	jmp	.L3168
.L4025:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r14
	jmp	.L3384
.L4032:
	movzbl	2(%rcx), %esi
	movq	%rsi, %rdx
	salq	$14, %rdx
	andl	$2080768, %edx
	orq	%rdx, %rax
	testb	%sil, %sil
	js	.L4038
	salq	$43, %rax
	movl	$4, %ebx
	sarq	$43, %rax
	movq	%rax, %rdx
	movl	$4, %eax
	jmp	.L3237
.L3368:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L4039
.L3370:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%rbx, (%rax)
	jmp	.L3369
.L3162:
	cmpb	$0, 4(%rax)
	js	.L4040
	movl	$5, -544(%rbp)
	movzbl	5(%rax), %ecx
	leaq	4(%rax), %rsi
	jmp	.L3160
.L3690:
	movl	%ebx, %eax
	movaps	%xmm1, %xmm0
	jmp	.L3533
.L4035:
	movq	%rdx, %rdi
	movq	%r8, -584(%rbp)
	movq	%rsi, -576(%rbp)
	movq	%rdx, -552(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-584(%rbp), %r8
	movq	-576(%rbp), %rsi
	movq	%rax, -536(%rbp)
	movq	-552(%rbp), %rdx
	jmp	.L3267
.L4036:
	movzbl	3(%rcx), %esi
	movl	%esi, %edx
	sall	$21, %edx
	andl	$266338304, %edx
	orl	%edx, %eax
	testb	%sil, %sil
	js	.L4041
	sall	$4, %eax
	movl	$5, %ebx
	sarl	$4, %eax
	movl	%eax, %edx
	movl	$5, %eax
	jmp	.L3227
.L3972:
	je	.L3592
	jmp	.L3588
	.p2align 4,,10
	.p2align 3
.L3974:
	je	.L3584
	jmp	.L3580
	.p2align 4,,10
	.p2align 3
.L3968:
	je	.L3576
	jmp	.L3572
	.p2align 4,,10
	.p2align 3
.L3970:
	je	.L3568
	jmp	.L3564
	.p2align 4,,10
	.p2align 3
.L3939:
	movq	72(%r12), %rax
	movq	-512(%rbp), %rdx
	movl	$4, 88(%r12)
	leaq	-416(%rbp), %rdi
	movl	$11, 104(%r12)
	movq	%rdx, -16(%rax)
	call	_ZN2v88internal11HandleScopeD1Ev
	jmp	.L3947
.L3691:
	movq	%rcx, %rbx
	movq	%rcx, %xmm0
	jmp	.L3534
.L3332:
	movzbl	4(%rax), %esi
	movl	%esi, %edx
	sall	$21, %edx
	andl	$266338304, %edx
	orl	%edx, %ecx
	testb	%sil, %sil
	js	.L3333
	movzbl	5(%rax), %edx
	leaq	4(%rax), %rsi
	movl	$5, %ebx
	jmp	.L3330
.L3290:
	movzbl	4(%rax), %ecx
	movl	%ecx, %edx
	sall	$21, %edx
	andl	$266338304, %edx
	orl	%edx, %r14d
	testb	%cl, %cl
	js	.L3291
	movl	$5, -536(%rbp)
	movzbl	5(%rax), %edx
	leaq	4(%rax), %rcx
	jmp	.L3288
.L3689:
	movl	%eax, %ebx
	movd	%eax, %xmm0
	jmp	.L3532
.L3692:
	movq	%rbx, %rcx
	movapd	%xmm1, %xmm0
	jmp	.L3535
.L4022:
	movq	111(%rax), %rax
	movq	(%rax,%rbx,8), %rbx
	jmp	.L3361
.L4034:
	movq	%rdx, %rdi
	movq	%rdx, -544(%rbp)
	movq	%rax, -568(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-568(%rbp), %rsi
	movq	-544(%rbp), %rdx
	movq	%rax, %r15
	jmp	.L3315
.L4037:
	movq	%rdx, %rdi
	movq	%rsi, -544(%rbp)
	movq	%rdx, -536(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-544(%rbp), %rsi
	movq	-536(%rbp), %rdx
	jmp	.L3246
.L4029:
	movl	$64, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r14
	jmp	.L3309
.L4040:
	movl	$6, -544(%rbp)
	movzbl	6(%rax), %ecx
	leaq	5(%rax), %rsi
	jmp	.L3160
.L3291:
	movzbl	5(%rax), %edx
	leaq	5(%rax), %rcx
	movl	$6, -536(%rbp)
	sall	$28, %edx
	orl	%edx, %r14d
	movzbl	6(%rax), %edx
	jmp	.L3288
.L3694:
	movl	$-2147483648, %ecx
	jmp	.L3536
.L4038:
	movzbl	3(%rcx), %esi
	movq	%rsi, %rdx
	salq	$21, %rdx
	andl	$266338304, %edx
	orq	%rdx, %rax
	testb	%sil, %sil
	js	.L4042
	salq	$36, %rax
	movl	$5, %ebx
	sarq	$36, %rax
	movq	%rax, %rdx
	movl	$5, %eax
	jmp	.L3237
.L4030:
	movl	$64, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r14
	jmp	.L3263
.L4041:
	movzbl	4(%rcx), %edx
	movl	$6, %ebx
	sall	$28, %edx
	orl	%eax, %edx
	movl	$6, %eax
	jmp	.L3227
.L4021:
	movl	24(%r14), %edx
	movq	183(%rax), %rax
	leal	16(,%rdx,8), %edx
	movslq	%edx, %rdx
	movq	-1(%rdx,%rax), %rbx
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L3364
	movq	%rbx, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L3365:
	movq	16(%r12), %rdx
	movl	24(%r14), %ecx
	movq	(%rax), %rbx
	movq	(%rdx), %rdx
	movq	111(%rdx), %rdx
	movl	(%rdx,%rcx,8), %r15d
	jmp	.L3367
.L3333:
	movzbl	5(%rax), %edx
	leaq	5(%rax), %rsi
	movl	$6, %ebx
	sall	$28, %edx
	orl	%edx, %ecx
	movzbl	6(%rax), %edx
	jmp	.L3330
.L3364:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L4043
.L3366:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%rbx, (%rax)
	jmp	.L3365
.L3710:
	xorl	%eax, %eax
	jmp	.L3584
.L3702:
	xorl	%eax, %eax
	jmp	.L3568
.L3957:
	movsd	%xmm1, -536(%rbp)
	call	sqrt@PLT
	movsd	-536(%rbp), %xmm1
	jmp	.L3597
.L4043:
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L3366
.L3958:
	movss	%xmm1, -536(%rbp)
	call	sqrtf@PLT
	movss	-536(%rbp), %xmm1
	jmp	.L3596
.L4039:
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L3370
.L4042:
	movzbl	4(%rcx), %esi
	movl	$127, %edi
	salq	$28, %rdi
	movq	%rsi, %rdx
	salq	$28, %rdx
	andq	%rdi, %rdx
	orq	%rdx, %rax
	testb	%sil, %sil
	js	.L4044
	salq	$29, %rax
	movl	$6, %ebx
	sarq	$29, %rax
	movq	%rax, %rdx
	movl	$6, %eax
	jmp	.L3237
.L3714:
	xorl	%eax, %eax
	jmp	.L3592
.L3719:
	movq	%rdx, %rax
	jmp	.L3613
.L3953:
	call	__stack_chk_fail@PLT
.L3706:
	xorl	%eax, %eax
	jmp	.L3576
.L4044:
	movzbl	5(%rcx), %esi
	movl	$127, %edi
	salq	$35, %rdi
	movq	%rsi, %rdx
	salq	$35, %rdx
	andq	%rdi, %rdx
	orq	%rdx, %rax
	testb	%sil, %sil
	js	.L4045
	salq	$22, %rax
	movl	$7, %ebx
	sarq	$22, %rax
	movq	%rax, %rdx
	movl	$7, %eax
	jmp	.L3237
.L4045:
	movzbl	6(%rcx), %esi
	movl	$127, %edi
	salq	$42, %rdi
	movq	%rsi, %rdx
	salq	$42, %rdx
	andq	%rdi, %rdx
	orq	%rdx, %rax
	testb	%sil, %sil
	js	.L4046
	salq	$15, %rax
	movl	$8, %ebx
	sarq	$15, %rax
	movq	%rax, %rdx
	movl	$8, %eax
	jmp	.L3237
.L4046:
	movzbl	7(%rcx), %esi
	movl	$127, %edi
	salq	$49, %rdi
	movq	%rsi, %rdx
	salq	$49, %rdx
	andq	%rdi, %rdx
	orq	%rdx, %rax
	testb	%sil, %sil
	js	.L4047
	salq	$8, %rax
	movl	$9, %ebx
	sarq	$8, %rax
	movq	%rax, %rdx
	movl	$9, %eax
	jmp	.L3237
.L4047:
	movzbl	8(%rcx), %esi
	movl	$127, %edi
	salq	$56, %rdi
	movq	%rsi, %rdx
	salq	$56, %rdx
	andq	%rdi, %rdx
	orq	%rax, %rdx
	testb	%sil, %sil
	js	.L4048
	addq	%rdx, %rdx
	movl	$10, %eax
	movl	$10, %ebx
	sarq	%rdx
	jmp	.L3237
.L4048:
	movzbl	9(%rcx), %eax
	movl	$11, %ebx
	salq	$63, %rax
	orq	%rax, %rdx
	movl	$11, %eax
	jmp	.L3237
	.cfi_endproc
.LFE20265:
	.size	_ZN2v88internal4wasm10ThreadImpl7ExecuteEPNS1_15InterpreterCodeEmi, .-_ZN2v88internal4wasm10ThreadImpl7ExecuteEPNS1_15InterpreterCodeEmi
	.section	.text._ZN2v88internal4wasm15WasmInterpreter6Thread3RunEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15WasmInterpreter6Thread3RunEi
	.type	_ZN2v88internal4wasm15WasmInterpreter6Thread3RunEi, @function
_ZN2v88internal4wasm15WasmInterpreter6Thread3RunEi:
.LFB20305:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	72(%rdi), %rax
	movl	$1, 88(%rdi)
	movq	-16(%rax), %rdx
	movq	-24(%rax), %rsi
	call	_ZN2v88internal4wasm10ThreadImpl7ExecuteEPNS1_15InterpreterCodeEmi
	movl	88(%rbx), %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20305:
	.size	_ZN2v88internal4wasm15WasmInterpreter6Thread3RunEi, .-_ZN2v88internal4wasm15WasmInterpreter6Thread3RunEi
	.section	.text._ZN2v88internal4wasm15WasmInterpreter3RunEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15WasmInterpreter3RunEv
	.type	_ZN2v88internal4wasm15WasmInterpreter3RunEv, @function
_ZN2v88internal4wasm15WasmInterpreter3RunEv:
.LFB20346:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rax
	movl	$-1, %ecx
	movq	88(%rax), %rdi
	movq	72(%rdi), %rax
	movl	$1, 88(%rdi)
	movq	-16(%rax), %rdx
	movq	-24(%rax), %rsi
	jmp	_ZN2v88internal4wasm10ThreadImpl7ExecuteEPNS1_15InterpreterCodeEmi
	.cfi_endproc
.LFE20346:
	.size	_ZN2v88internal4wasm15WasmInterpreter3RunEv, .-_ZN2v88internal4wasm15WasmInterpreter3RunEv
	.section	.text._ZN2v88internal4wasm15WasmInterpreter6Thread9InitFrameEPKNS1_12WasmFunctionEPNS1_9WasmValueE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15WasmInterpreter6Thread9InitFrameEPKNS1_12WasmFunctionEPNS1_9WasmValueE
	.type	_ZN2v88internal4wasm15WasmInterpreter6Thread9InitFrameEPKNS1_12WasmFunctionEPNS1_9WasmValueE, @function
_ZN2v88internal4wasm15WasmInterpreter6Thread9InitFrameEPKNS1_12WasmFunctionEPNS1_9WasmValueE:
.LFB20304:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$232, %rsp
	movq	(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	8(%rsi), %eax
	leaq	(%rax,%rax,4), %rdx
	leaq	(%rax,%rdx,2), %rdx
	movq	24(%r13), %rax
	leaq	(%rax,%rdx,8), %r8
	cmpq	$0, 80(%r8)
	je	.L4176
.L4053:
	movq	(%r12), %rax
	movq	32(%rbx), %r13
	movabsq	$-1085102592571150095, %r12
	movq	24(%rbx), %rcx
	movq	8(%rax), %r15
	movq	40(%rbx), %rax
	movq	%r13, %rsi
	movq	%rax, %rdx
	subq	%rax, %rsi
	subq	%rcx, %rdx
	imulq	%r12, %rsi
	imulq	%r12, %rdx
	movq	%rdx, %rdi
	cmpq	%rsi, %r15
	ja	.L4177
.L4057:
	movq	%r15, %r12
	salq	$4, %r12
	leaq	(%r12,%r15), %r10
	leaq	(%r9,%r10), %r12
	cmpq	%r12, %r9
	je	.L4076
	movabsq	$-1085102592571150095, %r15
	jmp	.L4082
	.p2align 4,,10
	.p2align 3
.L4077:
	movq	%rdx, -143(%rbp)
	movzbl	-128(%rbp), %edx
	leaq	17(%rax), %rcx
	addq	$17, %r9
	movb	%r14b, -144(%rbp)
	movdqa	-144(%rbp), %xmm3
	movq	%rcx, 40(%rbx)
	movb	%dl, 16(%rax)
	movups	%xmm3, (%rax)
	movq	40(%rbx), %rax
	movq	24(%rbx), %rcx
	cmpq	%r9, %r12
	je	.L4178
.L4082:
	movdqu	(%r9), %xmm0
	movzbl	16(%r9), %esi
	movaps	%xmm0, -176(%rbp)
	movq	-175(%rbp), %rdx
	movaps	%xmm0, -112(%rbp)
	movzbl	-112(%rbp), %r14d
	movq	%rdx, -111(%rbp)
	movdqa	-112(%rbp), %xmm1
	movb	%sil, -160(%rbp)
	movb	%sil, -96(%rbp)
	movb	%sil, -128(%rbp)
	movaps	%xmm1, -144(%rbp)
	cmpb	$6, %r14b
	jne	.L4077
	movq	$0, -71(%rbp)
	movzbl	-64(%rbp), %esi
	subq	%rcx, %rax
	imulq	%r15, %rax
	movb	$6, -80(%rbp)
	movq	(%rdx), %r13
	movb	%sil, -128(%rbp)
	movq	48(%rbx), %rsi
	movq	$0, -79(%rbp)
	movdqa	-80(%rbp), %xmm2
	movq	(%rsi), %rsi
	leal	16(,%rax,8), %eax
	cltq
	movaps	%xmm2, -144(%rbp)
	movq	7(%rsi), %rdi
	leaq	-1(%rdi,%rax), %rsi
	movq	%r13, (%rsi)
	testb	$1, %r13b
	je	.L4124
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -216(%rbp)
	testl	$262144, %eax
	je	.L4079
	movq	%r13, %rdx
	movq	%r9, -248(%rbp)
	movq	%r8, -240(%rbp)
	movq	%rsi, -232(%rbp)
	movq	%rdi, -224(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-216(%rbp), %rcx
	movq	-248(%rbp), %r9
	movq	-240(%rbp), %r8
	movq	-232(%rbp), %rsi
	movq	8(%rcx), %rax
	movq	-224(%rbp), %rdi
.L4079:
	testb	$24, %al
	je	.L4124
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L4179
.L4124:
	movq	40(%rbx), %rax
	xorl	%edx, %edx
	jmp	.L4077
	.p2align 4,,10
	.p2align 3
.L4178:
	movabsq	$-1085102592571150095, %rdi
	movq	%rax, %rdx
	subq	%rcx, %rdx
	imulq	%rdx, %rdi
.L4076:
	movq	32(%rbx), %r13
	movq	80(%r8), %rsi
	movabsq	$-1085102592571150095, %r12
	movq	32(%r8), %rdx
	subq	24(%r8), %rdx
	movq	%r13, %r11
	movl	56(%rsi), %esi
	subq	%rax, %r11
	movq	%r11, %rax
	addq	%rsi, %rdx
	movq	%rdi, %rsi
	imulq	%r12, %rax
	cmpq	%rax, %rdx
	ja	.L4180
.L4084:
	addq	$1, 112(%rbx)
	movq	(%r8), %rax
	movq	(%rax), %rax
	movq	%r8, -208(%rbp)
	movq	8(%rax), %rax
	movq	$0, -200(%rbp)
	subq	%rax, %rsi
	movq	%rsi, -192(%rbp)
	movq	72(%rbx), %rsi
	cmpq	80(%rbx), %rsi
	je	.L4103
	movdqa	-208(%rbp), %xmm7
	movups	%xmm7, (%rsi)
	movq	-192(%rbp), %rax
	movq	%rax, 16(%rsi)
	movq	72(%rbx), %rax
	leaq	24(%rax), %r12
	movq	%r12, 72(%rbx)
.L4104:
	movq	32(%r8), %r13
	movq	24(%r8), %r15
	leaq	.L4108(%rip), %r14
	cmpq	%r13, %r15
	je	.L4120
	.p2align 4,,10
	.p2align 3
.L4119:
	movzbl	(%r15), %esi
	cmpb	$9, %sil
	ja	.L4106
	movzbl	%sil, %eax
	movslq	(%r14,%rax,4), %rax
	addq	%r14, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmInterpreter6Thread9InitFrameEPKNS1_12WasmFunctionEPNS1_9WasmValueE,"a",@progbits
	.align 4
	.align 4
.L4108:
	.long	.L4106-.L4108
	.long	.L4111-.L4108
	.long	.L4109-.L4108
	.long	.L4111-.L4108
	.long	.L4109-.L4108
	.long	.L4109-.L4108
	.long	.L4107-.L4108
	.long	.L4107-.L4108
	.long	.L4106-.L4108
	.long	.L4107-.L4108
	.section	.text._ZN2v88internal4wasm15WasmInterpreter6Thread9InitFrameEPKNS1_12WasmFunctionEPNS1_9WasmValueE
	.p2align 4,,10
	.p2align 3
.L4111:
	pxor	%xmm0, %xmm0
	movups	%xmm0, -79(%rbp)
	movq	-79(%rbp), %rax
	movq	-71(%rbp), %rdx
.L4114:
	movq	%rdx, -71(%rbp)
	movzbl	-64(%rbp), %edx
	movb	%sil, -80(%rbp)
	movq	40(%rbx), %rdi
	movq	%rax, -79(%rbp)
	movdqa	-80(%rbp), %xmm6
	movb	%dl, -96(%rbp)
	movaps	%xmm6, -112(%rbp)
.L4127:
	movq	%rax, -111(%rbp)
	movzbl	-96(%rbp), %eax
	leaq	17(%rdi), %rdx
	addq	$1, %r15
	movb	%sil, -112(%rbp)
	movdqa	-112(%rbp), %xmm5
	movq	%rdx, 40(%rbx)
	movb	%al, 16(%rdi)
	movups	%xmm5, (%rdi)
	cmpq	%r15, %r13
	jne	.L4119
.L4120:
	movl	8(%r8), %eax
	movq	%rax, -16(%r12)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4181
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4109:
	.cfi_restore_state
	xorl	%eax, %eax
	xorl	%edx, %edx
	jmp	.L4114
	.p2align 4,,10
	.p2align 3
.L4107:
	movq	40(%rbx), %rax
	subq	24(%rbx), %rax
	movq	$0, -71(%rbp)
	movabsq	$-1085102592571150095, %rcx
	movzbl	-64(%rbp), %esi
	imulq	%rcx, %rax
	movq	8(%rbx), %rdx
	movb	$6, -80(%rbp)
	movq	$0, -79(%rbp)
	movdqa	-80(%rbp), %xmm4
	movb	%sil, -96(%rbp)
	movq	48(%rbx), %rsi
	leal	16(,%rax,8), %eax
	movq	104(%rdx), %rdx
	movaps	%xmm4, -112(%rbp)
	movq	(%rsi), %rsi
	cltq
	movq	7(%rsi), %rdi
	leaq	-1(%rdi,%rax), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L4121
	movq	%rdx, %r9
	andq	$-262144, %r9
	movq	8(%r9), %rax
	movq	%r9, -248(%rbp)
	testl	$262144, %eax
	je	.L4117
	movq	%r8, -240(%rbp)
	movq	%rdx, -232(%rbp)
	movq	%rsi, -224(%rbp)
	movq	%rdi, -216(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-248(%rbp), %r9
	movq	-240(%rbp), %r8
	movq	-232(%rbp), %rdx
	movq	-224(%rbp), %rsi
	movq	8(%r9), %rax
	movq	-216(%rbp), %rdi
.L4117:
	testb	$24, %al
	je	.L4121
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4121
	movq	%r8, -216(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-216(%rbp), %r8
	.p2align 4,,10
	.p2align 3
.L4121:
	movq	40(%rbx), %rdi
	xorl	%eax, %eax
	movl	$6, %esi
	jmp	.L4127
	.p2align 4,,10
	.p2align 3
.L4179:
	movq	%r13, %rdx
	movq	%r9, -224(%rbp)
	movq	%r8, -216(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-224(%rbp), %r9
	movq	-216(%rbp), %r8
	xorl	%edx, %edx
	movq	40(%rbx), %rax
	jmp	.L4077
	.p2align 4,,10
	.p2align 3
.L4176:
	cmpq	$0, 64(%r8)
	je	.L4053
	movq	0(%r13), %rdi
	movq	16(%rdi), %r14
	movq	24(%rdi), %rax
	subq	%r14, %rax
	cmpq	$63, %rax
	jbe	.L4182
	leaq	64(%r14), %rax
	movq	%rax, 16(%rdi)
.L4055:
	movq	8(%r13), %rdx
	movq	0(%r13), %rsi
	movq	%r8, %rcx
	movq	%r14, %rdi
	movq	%r9, -224(%rbp)
	movq	%r8, -216(%rbp)
	call	_ZN2v88internal4wasm9SideTableC1EPNS0_4ZoneEPKNS1_10WasmModuleEPNS1_15InterpreterCodeE
	movq	-216(%rbp), %r8
	movq	-224(%rbp), %r9
	movq	%r14, 80(%r8)
	jmp	.L4053
	.p2align 4,,10
	.p2align 3
.L4103:
	leaq	-208(%rbp), %rdx
	leaq	56(%rbx), %rdi
	movq	%r8, -216(%rbp)
	call	_ZNSt6vectorIN2v88internal4wasm10ThreadImpl5FrameENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	movq	72(%rbx), %r12
	movq	-216(%rbp), %r8
	jmp	.L4104
	.p2align 4,,10
	.p2align 3
.L4177:
	subq	%rcx, %r13
	leaq	(%rdx,%r15), %rdi
	movq	%r9, -224(%rbp)
	movl	$8, %r14d
	imulq	%r13, %r12
	movq	%r8, -216(%rbp)
	call	_ZN2v84base4bits21RoundUpToPowerOfTwo64Em@PLT
	movq	-216(%rbp), %r8
	movq	-224(%rbp), %r9
	movq	%rax, %rdx
	leaq	(%r12,%r12), %rax
	cmpq	%rdx, %rax
	cmovb	%rdx, %rax
	movabsq	$542551296285575047, %rdx
	cmpq	$8, %rax
	cmovnb	%rax, %r14
	cmpq	%rdx, %rax
	jbe	.L4058
	movq	%r14, %rax
	movq	$-1, %rdi
	salq	$4, %rax
	addq	%r14, %rax
	movq	%rax, -216(%rbp)
.L4059:
	movq	%r9, -232(%rbp)
	movq	%r8, -224(%rbp)
	call	_Znam@PLT
	movq	%r14, %rdx
	movq	-224(%rbp), %r8
	movq	-232(%rbp), %r9
	subq	$1, %rdx
	movq	%rax, %rcx
	js	.L4063
	.p2align 4,,10
	.p2align 3
.L4064:
	pxor	%xmm0, %xmm0
	subq	$1, %rdx
	movb	$0, (%rax)
	addq	$17, %rax
	movups	%xmm0, -16(%rax)
	cmpq	$-1, %rdx
	jne	.L4064
.L4063:
	movq	24(%rbx), %r11
	testq	%r12, %r12
	jne	.L4183
	movq	40(%rbx), %rax
	movq	%rcx, 24(%rbx)
	addq	%rcx, %rax
	subq	%r11, %rax
	movq	%rax, 40(%rbx)
	testq	%r11, %r11
	jne	.L4065
.L4066:
	movq	-216(%rbp), %rax
	movq	8(%rbx), %r13
	movl	%r14d, %edx
	subl	%r12d, %edx
	addq	%rcx, %rax
	movq	%rax, 32(%rbx)
	movq	41088(%r13), %rax
	addl	$1, 41104(%r13)
	movq	8(%rbx), %rdi
	movq	%rax, -240(%rbp)
	movq	41096(%r13), %rax
	movq	41112(%rdi), %r11
	movq	%rax, -216(%rbp)
	movq	48(%rbx), %rax
	movq	(%rax), %rax
	movq	7(%rax), %r10
	testq	%r11, %r11
	je	.L4067
	movq	%r10, %rsi
	movq	%r11, %rdi
	movq	%r9, -248(%rbp)
	movq	%r8, -232(%rbp)
	movl	%edx, -224(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-224(%rbp), %edx
	movq	-232(%rbp), %r8
	movq	-248(%rbp), %r9
	movq	%rax, %rsi
.L4068:
	movq	8(%rbx), %rdi
	xorl	%ecx, %ecx
	movq	%r9, -232(%rbp)
	movq	%r8, -224(%rbp)
	call	_ZN2v88internal7Factory21CopyFixedArrayAndGrowENS0_6HandleINS0_10FixedArrayEEEiNS0_14AllocationTypeE@PLT
	cmpl	%r14d, %r12d
	movq	-224(%rbp), %r8
	movq	-232(%rbp), %r9
	movq	(%rax), %r10
	jge	.L4070
	movl	%r12d, %edi
	leal	16(,%r12,8), %edx
	movq	%r10, %rsi
	movslq	%r12d, %r12
	notl	%edi
	andq	$-262144, %rsi
	movslq	%edx, %rdx
	addl	%r14d, %edi
	addq	$24, %rsi
	leaq	-1(%r10,%rdx), %rdx
	addq	%rdi, %r12
	leaq	23(%r10,%r12,8), %rdi
	.p2align 4,,10
	.p2align 3
.L4071:
	movq	(%rsi), %rcx
	addq	$8, %rdx
	movq	-37496(%rcx), %rcx
	movq	%rcx, -8(%rdx)
	cmpq	%rdx, %rdi
	jne	.L4071
	movq	(%rax), %r10
.L4070:
	movq	48(%rbx), %rax
	movq	(%rax), %r12
	movq	%r10, 7(%r12)
	leaq	7(%r12), %rsi
	testb	$1, %r10b
	je	.L4075
	movq	%r10, %r14
	andq	$-262144, %r14
	movq	8(%r14), %rax
	testl	$262144, %eax
	jne	.L4184
.L4073:
	testb	$24, %al
	je	.L4075
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4075
	movq	%r10, %rdx
	movq	%r12, %rdi
	movq	%r9, -232(%rbp)
	movq	%r8, -224(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-224(%rbp), %r8
	movq	-232(%rbp), %r9
	.p2align 4,,10
	.p2align 3
.L4075:
	movq	-240(%rbp), %rax
	subl	$1, 41104(%r13)
	movq	%rax, 41088(%r13)
	movq	-216(%rbp), %rax
	cmpq	41096(%r13), %rax
	je	.L4126
	movq	%rax, 41096(%r13)
	movq	%r13, %rdi
	movq	%r9, -232(%rbp)
	movq	%r8, -224(%rbp)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movq	-232(%rbp), %r9
	movq	-224(%rbp), %r8
.L4126:
	movq	40(%rbx), %rax
	movq	24(%rbx), %rcx
	movabsq	$-1085102592571150095, %rdi
	movq	%rax, %rdx
	subq	%rcx, %rdx
	imulq	%rdx, %rdi
	jmp	.L4057
	.p2align 4,,10
	.p2align 3
.L4183:
	movq	%r11, %rsi
	movq	%rcx, %rdi
	movq	%r13, %rdx
	movq	%r9, -240(%rbp)
	movq	%r8, -232(%rbp)
	movq	%r11, -224(%rbp)
	call	memcpy@PLT
	movq	-224(%rbp), %r11
	movq	-232(%rbp), %r8
	movq	%rax, %rcx
	movq	40(%rbx), %rax
	movq	-240(%rbp), %r9
	movq	%rcx, 24(%rbx)
	addq	%rcx, %rax
	subq	%r11, %rax
	movq	%rax, 40(%rbx)
.L4065:
	movq	%r11, %rdi
	movq	%r9, -232(%rbp)
	movq	%r8, -224(%rbp)
	call	_ZdaPv@PLT
	movq	24(%rbx), %rcx
	movq	-232(%rbp), %r9
	movq	-224(%rbp), %r8
	jmp	.L4066
	.p2align 4,,10
	.p2align 3
.L4180:
	subq	%rcx, %r13
	addq	%rdx, %rdi
	movq	%r8, -216(%rbp)
	movl	$8, %r14d
	call	_ZN2v84base4bits21RoundUpToPowerOfTwo64Em@PLT
	imulq	%r13, %r12
	movq	-216(%rbp), %r8
	movq	%rax, %rdx
	leaq	(%r12,%r12), %rax
	cmpq	%rdx, %rax
	cmovb	%rdx, %rax
	movabsq	$542551296285575047, %rdx
	cmpq	$8, %rax
	cmovnb	%rax, %r14
	cmpq	%rdx, %rax
	jbe	.L4085
	movq	%r14, %r15
	movq	$-1, %rdi
	salq	$4, %r15
	addq	%r14, %r15
.L4086:
	movq	%r8, -216(%rbp)
	call	_Znam@PLT
	movq	%r14, %rdx
	movq	-216(%rbp), %r8
	subq	$1, %rdx
	movq	%rax, %rcx
	js	.L4090
	.p2align 4,,10
	.p2align 3
.L4091:
	pxor	%xmm0, %xmm0
	subq	$1, %rdx
	movb	$0, (%rax)
	addq	$17, %rax
	movups	%xmm0, -16(%rax)
	cmpq	$-1, %rdx
	jne	.L4091
.L4090:
	movq	24(%rbx), %r9
	testq	%r12, %r12
	jne	.L4185
	movq	40(%rbx), %rax
	movq	%rcx, 24(%rbx)
	addq	%rcx, %rax
	subq	%r9, %rax
	movq	%rax, 40(%rbx)
	testq	%r9, %r9
	jne	.L4092
.L4093:
	addq	%rcx, %r15
	movl	%r14d, %edx
	movq	%r15, 32(%rbx)
	movq	8(%rbx), %r15
	subl	%r12d, %edx
	movq	41088(%r15), %rax
	addl	$1, 41104(%r15)
	movq	8(%rbx), %rcx
	movq	41096(%r15), %r13
	movq	%rax, -224(%rbp)
	movq	48(%rbx), %rax
	movq	41112(%rcx), %rdi
	movq	(%rax), %rax
	movq	7(%rax), %r10
	testq	%rdi, %rdi
	je	.L4094
	movq	%r10, %rsi
	movl	%edx, -232(%rbp)
	movq	%r8, -216(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-216(%rbp), %r8
	movl	-232(%rbp), %edx
	movq	%rax, %rsi
.L4095:
	movq	8(%rbx), %rdi
	xorl	%ecx, %ecx
	movq	%r8, -216(%rbp)
	call	_ZN2v88internal7Factory21CopyFixedArrayAndGrowENS0_6HandleINS0_10FixedArrayEEEiNS0_14AllocationTypeE@PLT
	cmpl	%r14d, %r12d
	movq	-216(%rbp), %r8
	movq	(%rax), %r10
	jge	.L4097
	movl	%r12d, %ecx
	leal	16(,%r12,8), %edx
	movq	%r10, %rsi
	movslq	%r12d, %r12
	notl	%ecx
	andq	$-262144, %rsi
	movslq	%edx, %rdx
	addl	%r14d, %ecx
	addq	$24, %rsi
	leaq	-1(%r10,%rdx), %rdx
	addq	%rcx, %r12
	leaq	23(%r10,%r12,8), %rdi
	.p2align 4,,10
	.p2align 3
.L4098:
	movq	(%rsi), %rcx
	addq	$8, %rdx
	movq	-37496(%rcx), %rcx
	movq	%rcx, -8(%rdx)
	cmpq	%rdi, %rdx
	jne	.L4098
	movq	(%rax), %r10
.L4097:
	movq	48(%rbx), %rax
	movq	(%rax), %r12
	movq	%r10, 7(%r12)
	leaq	7(%r12), %rsi
	testb	$1, %r10b
	je	.L4102
	movq	%r10, %r14
	andq	$-262144, %r14
	movq	8(%r14), %rax
	testl	$262144, %eax
	jne	.L4186
.L4100:
	testb	$24, %al
	je	.L4102
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4102
	movq	%r10, %rdx
	movq	%r12, %rdi
	movq	%r8, -216(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-216(%rbp), %r8
	.p2align 4,,10
	.p2align 3
.L4102:
	movq	-224(%rbp), %rax
	subl	$1, 41104(%r15)
	movq	%rax, 41088(%r15)
	cmpq	41096(%r15), %r13
	je	.L4123
	movq	%r13, 41096(%r15)
	movq	%r15, %rdi
	movq	%r8, -216(%rbp)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movq	-216(%rbp), %r8
.L4123:
	movabsq	$-1085102592571150095, %rax
	movq	40(%rbx), %rsi
	subq	24(%rbx), %rsi
	imulq	%rax, %rsi
	jmp	.L4084
	.p2align 4,,10
	.p2align 3
.L4185:
	movq	%r9, %rsi
	movq	%rcx, %rdi
	movq	%r13, %rdx
	movq	%r8, -224(%rbp)
	movq	%r9, -216(%rbp)
	call	memcpy@PLT
	movq	-216(%rbp), %r9
	movq	-224(%rbp), %r8
	movq	%rax, %rcx
	movq	40(%rbx), %rax
	movq	%rcx, 24(%rbx)
	addq	%rcx, %rax
	subq	%r9, %rax
	movq	%rax, 40(%rbx)
.L4092:
	movq	%r9, %rdi
	movq	%r8, -216(%rbp)
	call	_ZdaPv@PLT
	movq	24(%rbx), %rcx
	movq	-216(%rbp), %r8
	jmp	.L4093
	.p2align 4,,10
	.p2align 3
.L4106:
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4186:
	movq	%r10, %rdx
	movq	%r12, %rdi
	movq	%r8, -240(%rbp)
	movq	%r10, -232(%rbp)
	movq	%rsi, -216(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r14), %rax
	movq	-240(%rbp), %r8
	movq	-232(%rbp), %r10
	movq	-216(%rbp), %rsi
	jmp	.L4100
	.p2align 4,,10
	.p2align 3
.L4184:
	movq	%r10, %rdx
	movq	%r12, %rdi
	movq	%r9, -256(%rbp)
	movq	%r8, -248(%rbp)
	movq	%r10, -232(%rbp)
	movq	%rsi, -224(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r14), %rax
	movq	-256(%rbp), %r9
	movq	-248(%rbp), %r8
	movq	-232(%rbp), %r10
	movq	-224(%rbp), %rsi
	jmp	.L4073
	.p2align 4,,10
	.p2align 3
.L4085:
	movq	%r14, %rdi
	salq	$4, %rdi
	addq	%r14, %rdi
	movq	%rdi, %r15
	jmp	.L4086
	.p2align 4,,10
	.p2align 3
.L4058:
	movq	%r14, %rdi
	salq	$4, %rdi
	addq	%r14, %rdi
	movq	%rdi, -216(%rbp)
	jmp	.L4059
	.p2align 4,,10
	.p2align 3
.L4067:
	movq	41088(%rdi), %rsi
	cmpq	41096(%rdi), %rsi
	je	.L4187
.L4069:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rdi)
	movq	%r10, (%rsi)
	jmp	.L4068
	.p2align 4,,10
	.p2align 3
.L4094:
	movq	41088(%rcx), %rsi
	cmpq	41096(%rcx), %rsi
	je	.L4188
.L4096:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rcx)
	movq	%r10, (%rsi)
	jmp	.L4095
	.p2align 4,,10
	.p2align 3
.L4182:
	movl	$64, %esi
	movq	%r9, -224(%rbp)
	movq	%r8, -216(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-216(%rbp), %r8
	movq	-224(%rbp), %r9
	movq	%rax, %r14
	jmp	.L4055
.L4188:
	movq	%rcx, %rdi
	movq	%r10, -248(%rbp)
	movl	%edx, -240(%rbp)
	movq	%r8, -232(%rbp)
	movq	%rcx, -216(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-248(%rbp), %r10
	movl	-240(%rbp), %edx
	movq	-232(%rbp), %r8
	movq	-216(%rbp), %rcx
	movq	%rax, %rsi
	jmp	.L4096
.L4187:
	movq	%r9, -264(%rbp)
	movq	%r8, -256(%rbp)
	movq	%r10, -248(%rbp)
	movl	%edx, -232(%rbp)
	movq	%rdi, -224(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-264(%rbp), %r9
	movq	-256(%rbp), %r8
	movq	-248(%rbp), %r10
	movl	-232(%rbp), %edx
	movq	%rax, %rsi
	movq	-224(%rbp), %rdi
	jmp	.L4069
.L4181:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20304:
	.size	_ZN2v88internal4wasm15WasmInterpreter6Thread9InitFrameEPKNS1_12WasmFunctionEPNS1_9WasmValueE, .-_ZN2v88internal4wasm15WasmInterpreter6Thread9InitFrameEPKNS1_12WasmFunctionEPNS1_9WasmValueE
	.section	.text._ZNSt8_Rb_treeImSt4pairIKmN2v88internal4wasm20ControlTransferEntryEESt10_Select1stIS6_ESt4lessImENS3_13ZoneAllocatorIS6_EEE7_M_copyINSD_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS6_EPKSH_PSt18_Rb_tree_node_baseRT_,"axG",@progbits,_ZNSt8_Rb_treeImSt4pairIKmN2v88internal4wasm20ControlTransferEntryEESt10_Select1stIS6_ESt4lessImENS3_13ZoneAllocatorIS6_EEE7_M_copyINSD_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS6_EPKSH_PSt18_Rb_tree_node_baseRT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeImSt4pairIKmN2v88internal4wasm20ControlTransferEntryEESt10_Select1stIS6_ESt4lessImENS3_13ZoneAllocatorIS6_EEE7_M_copyINSD_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS6_EPKSH_PSt18_Rb_tree_node_baseRT_
	.type	_ZNSt8_Rb_treeImSt4pairIKmN2v88internal4wasm20ControlTransferEntryEESt10_Select1stIS6_ESt4lessImENS3_13ZoneAllocatorIS6_EEE7_M_copyINSD_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS6_EPKSH_PSt18_Rb_tree_node_baseRT_, @function
_ZNSt8_Rb_treeImSt4pairIKmN2v88internal4wasm20ControlTransferEntryEESt10_Select1stIS6_ESt4lessImENS3_13ZoneAllocatorIS6_EEE7_M_copyINSD_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS6_EPKSH_PSt18_Rb_tree_node_baseRT_:
.LFB25303:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	(%rcx), %rax
	movq	%rdi, -56(%rbp)
	movq	(%rax), %rdi
	movq	16(%rdi), %r14
	movq	24(%rdi), %rax
	subq	%r14, %rax
	cmpq	$55, %rax
	jbe	.L4212
	leaq	56(%r14), %rax
	movq	%rax, 16(%rdi)
.L4191:
	movdqu	32(%rbx), %xmm1
	movups	%xmm1, 32(%r14)
	movq	48(%rbx), %rax
	movq	%rax, 48(%r14)
	movl	(%rbx), %eax
	movq	$0, 16(%r14)
	movl	%eax, (%r14)
	movq	$0, 24(%r14)
	movq	%rdx, 8(%r14)
	movq	24(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L4192
	movq	-56(%rbp), %rdi
	movq	%r13, %rcx
	movq	%r14, %rdx
	call	_ZNSt8_Rb_treeImSt4pairIKmN2v88internal4wasm20ControlTransferEntryEESt10_Select1stIS6_ESt4lessImENS3_13ZoneAllocatorIS6_EEE7_M_copyINSD_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS6_EPKSH_PSt18_Rb_tree_node_baseRT_
	movq	%rax, 24(%r14)
.L4192:
	movq	16(%rbx), %r12
	testq	%r12, %r12
	je	.L4189
	movq	%r14, %r15
.L4198:
	movq	0(%r13), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$55, %rax
	jbe	.L4213
	leaq	56(%rbx), %rax
	movq	%rax, 16(%rdi)
.L4195:
	movdqu	32(%r12), %xmm0
	movups	%xmm0, 32(%rbx)
	movq	48(%r12), %rax
	movq	%rax, 48(%rbx)
	movl	(%r12), %eax
	movq	$0, 16(%rbx)
	movl	%eax, (%rbx)
	movq	$0, 24(%rbx)
	movq	%rbx, 16(%r15)
	movq	%r15, 8(%rbx)
	movq	24(%r12), %rsi
	testq	%rsi, %rsi
	je	.L4196
	movq	-56(%rbp), %rdi
	movq	%r13, %rcx
	movq	%rbx, %rdx
	call	_ZNSt8_Rb_treeImSt4pairIKmN2v88internal4wasm20ControlTransferEntryEESt10_Select1stIS6_ESt4lessImENS3_13ZoneAllocatorIS6_EEE7_M_copyINSD_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS6_EPKSH_PSt18_Rb_tree_node_baseRT_
	movq	%rax, 24(%rbx)
	movq	16(%r12), %r12
	testq	%r12, %r12
	je	.L4189
.L4197:
	movq	%rbx, %r15
	jmp	.L4198
	.p2align 4,,10
	.p2align 3
.L4196:
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L4197
.L4189:
	addq	$24, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4213:
	.cfi_restore_state
	movl	$56, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rbx
	jmp	.L4195
	.p2align 4,,10
	.p2align 3
.L4212:
	movl	$56, %esi
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-64(%rbp), %rdx
	movq	%rax, %r14
	jmp	.L4191
	.cfi_endproc
.LFE25303:
	.size	_ZNSt8_Rb_treeImSt4pairIKmN2v88internal4wasm20ControlTransferEntryEESt10_Select1stIS6_ESt4lessImENS3_13ZoneAllocatorIS6_EEE7_M_copyINSD_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS6_EPKSH_PSt18_Rb_tree_node_baseRT_, .-_ZNSt8_Rb_treeImSt4pairIKmN2v88internal4wasm20ControlTransferEntryEESt10_Select1stIS6_ESt4lessImENS3_13ZoneAllocatorIS6_EEE7_M_copyINSD_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS6_EPKSH_PSt18_Rb_tree_node_baseRT_
	.section	.text._ZN2v88internal4wasm15WasmInterpreter33ComputeControlTransfersForTestingEPNS0_4ZoneEPKNS1_10WasmModuleEPKhS9_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm15WasmInterpreter33ComputeControlTransfersForTestingEPNS0_4ZoneEPKNS1_10WasmModuleEPKhS9_
	.type	_ZN2v88internal4wasm15WasmInterpreter33ComputeControlTransfersForTestingEPNS0_4ZoneEPKNS1_10WasmModuleEPKhS9_, @function
_ZN2v88internal4wasm15WasmInterpreter33ComputeControlTransfersForTestingEPNS0_4ZoneEPKNS1_10WasmModuleEPKhS9_:
.LFB20355:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-192(%rbp), %r13
	pushq	%r12
	movq	%r13, %rdi
	pushq	%rbx
	subq	$240, %rsp
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	-256(%rbp), %rax
	movq	%rcx, -80(%rbp)
	leaq	-128(%rbp), %rcx
	movq	%rax, -224(%rbp)
	leaq	-224(%rbp), %rax
	movaps	%xmm0, -256(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rax, -128(%rbp)
	movq	%rsi, -112(%rbp)
	movups	%xmm0, -216(%rbp)
	movq	$0, -240(%rbp)
	movq	$0, -200(%rbp)
	movq	$0, -208(%rbp)
	movq	$0, -48(%rbp)
	movl	$0, -120(%rbp)
	movq	$0, -104(%rbp)
	movq	$0, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%r8, -72(%rbp)
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal4wasm9SideTableC1EPNS0_4ZoneEPKNS1_10WasmModuleEPNS1_15InterpreterCodeE
	movq	-192(%rbp), %rax
	movq	-168(%rbp), %rsi
	leaq	16(%r14), %rdx
	movl	$0, 16(%r14)
	movq	%rax, (%r14)
	movq	$0, 24(%r14)
	movq	%rdx, 32(%r14)
	movq	%rdx, 40(%r14)
	movq	$0, 48(%r14)
	testq	%rsi, %rsi
	je	.L4214
	leaq	-264(%rbp), %rcx
	movq	%r14, %rdi
	movq	%r14, -264(%rbp)
	call	_ZNSt8_Rb_treeImSt4pairIKmN2v88internal4wasm20ControlTransferEntryEESt10_Select1stIS6_ESt4lessImENS3_13ZoneAllocatorIS6_EEE7_M_copyINSD_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS6_EPKSH_PSt18_Rb_tree_node_baseRT_
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L4216:
	movq	%rdx, %rcx
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L4216
	movq	%rcx, 32(%r14)
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L4217:
	movq	%rdx, %rcx
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L4217
	movq	-144(%rbp), %rdx
	movq	-168(%rbp), %r12
	movq	%rcx, 40(%r14)
	movq	%rax, 24(%r14)
	movq	%rdx, 48(%r14)
	testq	%r12, %r12
	je	.L4214
.L4220:
	movq	24(%r12), %rbx
	testq	%rbx, %rbx
	je	.L4218
.L4219:
	movq	24(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmN2v88internal4wasm20ControlTransferEntryEESt10_Select1stIS6_ESt4lessImENS3_13ZoneAllocatorIS6_EEE8_M_eraseEPSt13_Rb_tree_nodeIS6_E
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L4219
.L4218:
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L4220
.L4214:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4236
	addq	$240, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4236:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20355:
	.size	_ZN2v88internal4wasm15WasmInterpreter33ComputeControlTransfersForTestingEPNS0_4ZoneEPKNS1_10WasmModuleEPKhS9_, .-_ZN2v88internal4wasm15WasmInterpreter33ComputeControlTransfersForTestingEPNS0_4ZoneEPKNS1_10WasmModuleEPKhS9_
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal4wasm15WasmInterpreter6Thread5stateEv,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal4wasm15WasmInterpreter6Thread5stateEv, @function
_GLOBAL__sub_I__ZN2v88internal4wasm15WasmInterpreter6Thread5stateEv:
.LFB25850:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE25850:
	.size	_GLOBAL__sub_I__ZN2v88internal4wasm15WasmInterpreter6Thread5stateEv, .-_GLOBAL__sub_I__ZN2v88internal4wasm15WasmInterpreter6Thread5stateEv
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal4wasm15WasmInterpreter6Thread5stateEv
	.weak	_ZTVN2v88internal4wasm7DecoderE
	.section	.data.rel.ro.local._ZTVN2v88internal4wasm7DecoderE,"awG",@progbits,_ZTVN2v88internal4wasm7DecoderE,comdat
	.align 8
	.type	_ZTVN2v88internal4wasm7DecoderE, @object
	.size	_ZTVN2v88internal4wasm7DecoderE, 40
_ZTVN2v88internal4wasm7DecoderE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal4wasm7DecoderD1Ev
	.quad	_ZN2v88internal4wasm7DecoderD0Ev
	.quad	_ZN2v88internal4wasm7Decoder12onFirstErrorEv
	.section	.rodata._ZN2v88internal4wasmL16kAllWasmFeaturesE,"a"
	.align 8
	.type	_ZN2v88internal4wasmL16kAllWasmFeaturesE, @object
	.size	_ZN2v88internal4wasmL16kAllWasmFeaturesE, 13
_ZN2v88internal4wasmL16kAllWasmFeaturesE:
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.zero	1
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC9:
	.long	3472883712
	.align 4
.LC10:
	.long	1325400064
	.align 4
.LC12:
	.long	3212836864
	.align 4
.LC13:
	.long	1333788672
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC14:
	.long	2097152
	.long	-1042284544
	.align 8
.LC15:
	.long	0
	.long	1105199104
	.align 8
.LC17:
	.long	0
	.long	-1074790400
	.align 8
.LC18:
	.long	0
	.long	1106247680
	.align 8
.LC20:
	.long	0
	.long	2146959360
	.align 8
.LC21:
	.long	0
	.long	-1048576
	.align 8
.LC22:
	.long	0
	.long	2146435072
	.section	.rodata.cst4
	.align 4
.LC23:
	.long	4286578688
	.align 4
.LC24:
	.long	2139095040
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC25:
	.long	-2147483648
	.long	-2147483648
	.long	-2147483648
	.long	-2147483648
	.align 16
.LC26:
	.long	32767
	.long	32767
	.long	32767
	.long	32767
	.align 16
.LC27:
	.long	-32768
	.long	-32768
	.long	-32768
	.long	-32768
	.align 16
.LC28:
	.value	-32768
	.value	-32768
	.value	-32768
	.value	-32768
	.value	-32768
	.value	-32768
	.value	-32768
	.value	-32768
	.align 16
.LC29:
	.value	32767
	.value	32767
	.value	32767
	.value	32767
	.value	32767
	.value	32767
	.value	32767
	.value	32767
	.align 16
.LC30:
	.long	65535
	.long	65535
	.long	65535
	.long	65535
	.align 16
.LC31:
	.value	255
	.value	255
	.value	255
	.value	255
	.value	255
	.value	255
	.value	255
	.value	255
	.align 16
.LC32:
	.long	127
	.long	127
	.long	127
	.long	127
	.align 16
.LC33:
	.long	-128
	.long	-128
	.long	-128
	.long	-128
	.align 16
.LC34:
	.byte	-128
	.byte	-128
	.byte	-128
	.byte	-128
	.byte	-128
	.byte	-128
	.byte	-128
	.byte	-128
	.byte	-128
	.byte	-128
	.byte	-128
	.byte	-128
	.byte	-128
	.byte	-128
	.byte	-128
	.byte	-128
	.align 16
.LC35:
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.align 16
.LC36:
	.long	255
	.long	255
	.long	255
	.long	255
	.align 16
.LC37:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.align 16
.LC38:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
	.align 16
.LC39:
	.long	2147483647
	.long	2147483647
	.long	2147483647
	.long	2147483647
	.align 16
.LC40:
	.long	2147483648
	.long	2147483648
	.long	2147483648
	.long	2147483648
	.section	.rodata.cst4
	.align 4
.LC41:
	.long	1065353216
	.section	.rodata.cst16
	.align 16
.LC42:
	.long	1
	.long	1
	.long	1
	.long	1
	.align 16
.LC43:
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.align 16
.LC44:
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.align 16
.LC45:
	.long	1199570944
	.long	1199570944
	.long	1199570944
	.long	1199570944
	.align 16
.LC46:
	.long	3472883712
	.long	3472883712
	.long	3472883712
	.long	3472883712
	.align 16
.LC47:
	.long	4290772992
	.long	1105199103
	.long	4290772992
	.long	1105199103
	.align 16
.LC48:
	.long	2147483647
	.long	2147483647
	.long	2147483647
	.long	2147483647
	.align 16
.LC49:
	.long	4292870144
	.long	1106247679
	.long	4292870144
	.long	1106247679
	.align 16
.LC50:
	.long	1325400064
	.long	1325400064
	.long	1325400064
	.long	1325400064
	.section	.rodata.cst4
	.align 4
.LC56:
	.long	1258291200
	.section	.rodata.cst16
	.align 16
.LC57:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC58:
	.long	0
	.long	1127219200
	.align 8
.LC59:
	.long	0
	.long	1072693248
	.align 8
.LC60:
	.long	3758096384
	.long	1206910975
	.align 8
.LC61:
	.long	4026531839
	.long	1206910975
	.align 8
.LC62:
	.long	3758096384
	.long	-940572673
	.align 8
.LC63:
	.long	4026531839
	.long	-940572673
	.align 8
.LC64:
	.long	4294967295
	.long	2146435071
	.align 8
.LC65:
	.long	4290772992
	.long	1105199103
	.align 8
.LC66:
	.long	0
	.long	-1042284544
	.align 8
.LC67:
	.long	4294967295
	.long	2147483647
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
