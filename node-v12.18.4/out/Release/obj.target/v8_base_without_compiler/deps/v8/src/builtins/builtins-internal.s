	.file	"builtins-internal.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB5450:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE5450:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB5451:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5451:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB5453:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5453:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.rodata._ZN2v88internalL26Builtin_Impl_Stats_IllegalEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"disabled-by-default-v8.runtime"
	.section	.rodata._ZN2v88internalL26Builtin_Impl_Stats_IllegalEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC1:
	.string	"V8.Builtin_Illegal"
.LC2:
	.string	"unreachable code"
	.section	.text._ZN2v88internalL26Builtin_Impl_Stats_IllegalEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL26Builtin_Impl_Stats_IllegalEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL26Builtin_Impl_Stats_IllegalEiPmPNS0_7IsolateE.isra.0:
.LFB25334:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$88, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	$0, -64(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L26
.L6:
	movq	_ZZN2v88internalL26Builtin_Impl_Stats_IllegalEiPmPNS0_7IsolateEE27trace_event_unique_atomic14(%rip), %rdx
	movq	%rdx, %r12
	testq	%rdx, %rdx
	je	.L27
.L8:
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L28
.L9:
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L27:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	.LC0(%rip), %rsi
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*16(%rax)
	movq	%rax, _ZZN2v88internalL26Builtin_Impl_Stats_IllegalEiPmPNS0_7IsolateEE27trace_event_unique_atomic14(%rip)
	movq	%rax, %r12
	jmp	.L8
.L28:
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	-48(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%rax, %rdi
	pushq	%rax
	leaq	.LC1(%rip), %rcx
	movl	$88, %esi
	movq	(%rdi), %rax
	pushq	$0
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*24(%rax)
	movq	-40(%rbp), %rdi
	addq	$64, %rsp
	testq	%rdi, %rdi
	je	.L10
	movq	(%rdi), %rax
	call	*8(%rax)
.L10:
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L9
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L9
.L26:
	movq	40960(%rdi), %rdi
	leaq	-88(%rbp), %rsi
	movl	$673, %edx
	addq	$23240, %rdi
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L6
	.cfi_endproc
.LFE25334:
	.size	_ZN2v88internalL26Builtin_Impl_Stats_IllegalEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL26Builtin_Impl_Stats_IllegalEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB8771:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L35
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L38
.L29:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L29
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE8771:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.rodata._ZN2v88internalL37Builtin_Impl_Stats_UnsupportedThrowerEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"V8.Builtin_UnsupportedThrower"
	.section	.text._ZN2v88internalL37Builtin_Impl_Stats_UnsupportedThrowerEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL37Builtin_Impl_Stats_UnsupportedThrowerEiPmPNS0_7IsolateE, @function
_ZN2v88internalL37Builtin_Impl_Stats_UnsupportedThrowerEiPmPNS0_7IsolateE:
.LFB20824:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L69
.L40:
	movq	_ZZN2v88internalL37Builtin_Impl_Stats_UnsupportedThrowerEiPmPNS0_7IsolateEE27trace_event_unique_atomic20(%rip), %rbx
	testq	%rbx, %rbx
	je	.L70
.L42:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L71
.L44:
	addl	$1, 41104(%r12)
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$7, %esi
	movq	%r12, %rdi
	movq	41088(%r12), %r13
	movq	41096(%r12), %rbx
	call	_ZN2v88internal7Factory8NewErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r14
	cmpq	41096(%r12), %rbx
	je	.L48
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L48:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L72
.L39:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L73
	leaq	-32(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L74
.L43:
	movq	%rbx, _ZZN2v88internalL37Builtin_Impl_Stats_UnsupportedThrowerEiPmPNS0_7IsolateEE27trace_event_unique_atomic20(%rip)
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L71:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L75
.L45:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L46
	movq	(%rdi), %rax
	call	*8(%rax)
.L46:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L47
	movq	(%rdi), %rax
	call	*8(%rax)
.L47:
	leaq	.LC3(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L72:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L69:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$675, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L75:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC3(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L74:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L43
.L73:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20824:
	.size	_ZN2v88internalL37Builtin_Impl_Stats_UnsupportedThrowerEiPmPNS0_7IsolateE, .-_ZN2v88internalL37Builtin_Impl_Stats_UnsupportedThrowerEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL42Builtin_Impl_Stats_StrictPoisonPillThrowerEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"V8.Builtin_StrictPoisonPillThrower"
	.section	.text._ZN2v88internalL42Builtin_Impl_Stats_StrictPoisonPillThrowerEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL42Builtin_Impl_Stats_StrictPoisonPillThrowerEiPmPNS0_7IsolateE, @function
_ZN2v88internalL42Builtin_Impl_Stats_StrictPoisonPillThrowerEiPmPNS0_7IsolateE:
.LFB20827:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L106
.L77:
	movq	_ZZN2v88internalL42Builtin_Impl_Stats_StrictPoisonPillThrowerEiPmPNS0_7IsolateEE27trace_event_unique_atomic26(%rip), %rbx
	testq	%rbx, %rbx
	je	.L107
.L79:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L108
.L81:
	addl	$1, 41104(%r12)
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$164, %esi
	movq	%r12, %rdi
	movq	41088(%r12), %r13
	movq	41096(%r12), %rbx
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r14
	cmpq	41096(%r12), %rbx
	je	.L85
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L85:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L109
.L76:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L110
	leaq	-32(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L107:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L111
.L80:
	movq	%rbx, _ZZN2v88internalL42Builtin_Impl_Stats_StrictPoisonPillThrowerEiPmPNS0_7IsolateEE27trace_event_unique_atomic26(%rip)
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L108:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L112
.L82:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L83
	movq	(%rdi), %rax
	call	*8(%rax)
.L83:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L84
	movq	(%rdi), %rax
	call	*8(%rax)
.L84:
	leaq	.LC4(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L109:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L106:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$674, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L112:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC4(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L111:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L80
.L110:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20827:
	.size	_ZN2v88internalL42Builtin_Impl_Stats_StrictPoisonPillThrowerEiPmPNS0_7IsolateE, .-_ZN2v88internalL42Builtin_Impl_Stats_StrictPoisonPillThrowerEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL32Builtin_Impl_Stats_EmptyFunctionEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC5:
	.string	"V8.Builtin_EmptyFunction"
	.section	.text._ZN2v88internalL32Builtin_Impl_Stats_EmptyFunctionEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL32Builtin_Impl_Stats_EmptyFunctionEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL32Builtin_Impl_Stats_EmptyFunctionEiPmPNS0_7IsolateE.isra.0:
.LFB25337:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L142
.L114:
	movq	_ZZN2v88internalL32Builtin_Impl_Stats_EmptyFunctionEiPmPNS0_7IsolateEE27trace_event_unique_atomic18(%rip), %rbx
	testq	%rbx, %rbx
	je	.L143
.L116:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L144
.L118:
	leaq	-144(%rbp), %rdi
	movq	88(%r12), %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L145
.L113:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L146
	leaq	-24(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L143:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L147
.L117:
	movq	%rbx, _ZZN2v88internalL32Builtin_Impl_Stats_EmptyFunctionEiPmPNS0_7IsolateEE27trace_event_unique_atomic18(%rip)
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L144:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L148
.L119:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L120
	movq	(%rdi), %rax
	call	*8(%rax)
.L120:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L121
	movq	(%rdi), %rax
	call	*8(%rax)
.L121:
	leaq	.LC5(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L145:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L142:
	movq	40960(%rdi), %rax
	leaq	-104(%rbp), %rsi
	movl	$672, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L148:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC5(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L147:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L117
.L146:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25337:
	.size	_ZN2v88internalL32Builtin_Impl_Stats_EmptyFunctionEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL32Builtin_Impl_Stats_EmptyFunctionEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal15Builtin_IllegalEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal15Builtin_IllegalEiPmPNS0_7IsolateE
	.type	_ZN2v88internal15Builtin_IllegalEiPmPNS0_7IsolateE, @function
_ZN2v88internal15Builtin_IllegalEiPmPNS0_7IsolateE:
.LFB20819:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testl	%eax, %eax
	jne	.L152
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L152:
	movq	%rdx, %rdi
	call	_ZN2v88internalL26Builtin_Impl_Stats_IllegalEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE20819:
	.size	_ZN2v88internal15Builtin_IllegalEiPmPNS0_7IsolateE, .-_ZN2v88internal15Builtin_IllegalEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal21Builtin_EmptyFunctionEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal21Builtin_EmptyFunctionEiPmPNS0_7IsolateE
	.type	_ZN2v88internal21Builtin_EmptyFunctionEiPmPNS0_7IsolateE, @function
_ZN2v88internal21Builtin_EmptyFunctionEiPmPNS0_7IsolateE:
.LFB20822:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L155
	movq	88(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L155:
	movq	%rdx, %rdi
	jmp	_ZN2v88internalL32Builtin_Impl_Stats_EmptyFunctionEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE20822:
	.size	_ZN2v88internal21Builtin_EmptyFunctionEiPmPNS0_7IsolateE, .-_ZN2v88internal21Builtin_EmptyFunctionEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal26Builtin_UnsupportedThrowerEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal26Builtin_UnsupportedThrowerEiPmPNS0_7IsolateE
	.type	_ZN2v88internal26Builtin_UnsupportedThrowerEiPmPNS0_7IsolateE, @function
_ZN2v88internal26Builtin_UnsupportedThrowerEiPmPNS0_7IsolateE:
.LFB20825:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L160
	addl	$1, 41104(%rdx)
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movl	$7, %esi
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory8NewErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	41096(%r12), %rbx
	je	.L158
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L158:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L160:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL37Builtin_Impl_Stats_UnsupportedThrowerEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE20825:
	.size	_ZN2v88internal26Builtin_UnsupportedThrowerEiPmPNS0_7IsolateE, .-_ZN2v88internal26Builtin_UnsupportedThrowerEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal31Builtin_StrictPoisonPillThrowerEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal31Builtin_StrictPoisonPillThrowerEiPmPNS0_7IsolateE
	.type	_ZN2v88internal31Builtin_StrictPoisonPillThrowerEiPmPNS0_7IsolateE, @function
_ZN2v88internal31Builtin_StrictPoisonPillThrowerEiPmPNS0_7IsolateE:
.LFB20828:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L165
	addl	$1, 41104(%rdx)
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movl	$164, %esi
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	41096(%r12), %rbx
	je	.L163
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L163:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L165:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL42Builtin_Impl_Stats_StrictPoisonPillThrowerEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE20828:
	.size	_ZN2v88internal31Builtin_StrictPoisonPillThrowerEiPmPNS0_7IsolateE, .-_ZN2v88internal31Builtin_StrictPoisonPillThrowerEiPmPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal15Builtin_IllegalEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal15Builtin_IllegalEiPmPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal15Builtin_IllegalEiPmPNS0_7IsolateE:
.LFB25330:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE25330:
	.size	_GLOBAL__sub_I__ZN2v88internal15Builtin_IllegalEiPmPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal15Builtin_IllegalEiPmPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal15Builtin_IllegalEiPmPNS0_7IsolateE
	.section	.bss._ZZN2v88internalL42Builtin_Impl_Stats_StrictPoisonPillThrowerEiPmPNS0_7IsolateEE27trace_event_unique_atomic26,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL42Builtin_Impl_Stats_StrictPoisonPillThrowerEiPmPNS0_7IsolateEE27trace_event_unique_atomic26, @object
	.size	_ZZN2v88internalL42Builtin_Impl_Stats_StrictPoisonPillThrowerEiPmPNS0_7IsolateEE27trace_event_unique_atomic26, 8
_ZZN2v88internalL42Builtin_Impl_Stats_StrictPoisonPillThrowerEiPmPNS0_7IsolateEE27trace_event_unique_atomic26:
	.zero	8
	.section	.bss._ZZN2v88internalL37Builtin_Impl_Stats_UnsupportedThrowerEiPmPNS0_7IsolateEE27trace_event_unique_atomic20,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL37Builtin_Impl_Stats_UnsupportedThrowerEiPmPNS0_7IsolateEE27trace_event_unique_atomic20, @object
	.size	_ZZN2v88internalL37Builtin_Impl_Stats_UnsupportedThrowerEiPmPNS0_7IsolateEE27trace_event_unique_atomic20, 8
_ZZN2v88internalL37Builtin_Impl_Stats_UnsupportedThrowerEiPmPNS0_7IsolateEE27trace_event_unique_atomic20:
	.zero	8
	.section	.bss._ZZN2v88internalL32Builtin_Impl_Stats_EmptyFunctionEiPmPNS0_7IsolateEE27trace_event_unique_atomic18,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL32Builtin_Impl_Stats_EmptyFunctionEiPmPNS0_7IsolateEE27trace_event_unique_atomic18, @object
	.size	_ZZN2v88internalL32Builtin_Impl_Stats_EmptyFunctionEiPmPNS0_7IsolateEE27trace_event_unique_atomic18, 8
_ZZN2v88internalL32Builtin_Impl_Stats_EmptyFunctionEiPmPNS0_7IsolateEE27trace_event_unique_atomic18:
	.zero	8
	.section	.bss._ZZN2v88internalL26Builtin_Impl_Stats_IllegalEiPmPNS0_7IsolateEE27trace_event_unique_atomic14,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL26Builtin_Impl_Stats_IllegalEiPmPNS0_7IsolateEE27trace_event_unique_atomic14, @object
	.size	_ZZN2v88internalL26Builtin_Impl_Stats_IllegalEiPmPNS0_7IsolateEE27trace_event_unique_atomic14, 8
_ZZN2v88internalL26Builtin_Impl_Stats_IllegalEiPmPNS0_7IsolateEE27trace_event_unique_atomic14:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
