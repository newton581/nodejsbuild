	.file	"runtime-module.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB5450:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE5450:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB5451:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5451:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB5453:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5453:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB8771:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L11
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L14
.L5:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L5
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE8771:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.rodata._ZN2v88internalL31Stats_Runtime_DynamicImportCallEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"disabled-by-default-v8.runtime"
	.align 8
.LC1:
	.string	"V8.Runtime_Runtime_DynamicImportCall"
	.section	.rodata._ZN2v88internalL31Stats_Runtime_DynamicImportCallEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"args[0].IsJSFunction()"
.LC3:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internalL31Stats_Runtime_DynamicImportCallEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL31Stats_Runtime_DynamicImportCallEiPmPNS0_7IsolateE, @function
_ZN2v88internalL31Stats_Runtime_DynamicImportCallEiPmPNS0_7IsolateE:
.LFB18345:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L71
.L16:
	movq	_ZZN2v88internalL31Stats_Runtime_DynamicImportCallEiPmPNS0_7IsolateEE27trace_event_unique_atomic15(%rip), %rbx
	testq	%rbx, %rbx
	je	.L72
.L18:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L73
.L20:
	addl	$1, 41104(%r14)
	movq	(%r12), %rax
	movq	41088(%r14), %r13
	movq	41096(%r14), %rbx
	testb	$1, %al
	jne	.L74
.L24:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L72:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L75
.L19:
	movq	%rbx, _ZZN2v88internalL31Stats_Runtime_DynamicImportCallEiPmPNS0_7IsolateEE27trace_event_unique_atomic15(%rip)
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L74:
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L24
	movq	23(%rax), %rax
	subq	$8, %r12
	movq	31(%rax), %r15
	testb	$1, %r15b
	jne	.L70
	.p2align 4,,10
	.p2align 3
.L33:
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L27
.L80:
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r15
	movq	%rax, %rsi
.L28:
	movq	71(%r15), %rax
	testb	$1, %al
	jne	.L76
.L32:
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal7Isolate38RunHostImportModuleDynamicallyCallbackENS0_6HandleINS0_6ScriptEEENS2_INS0_6ObjectEEE@PLT
	testq	%rax, %rax
	je	.L77
	movq	(%rax), %r12
.L40:
	subl	$1, 41104(%r14)
	movq	%r13, 41088(%r14)
	cmpq	41096(%r14), %rbx
	je	.L43
	movq	%rbx, 41096(%r14)
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L43:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L78
.L15:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L79
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$160, 11(%rax)
	jne	.L32
	movq	(%rsi), %rax
	movq	71(%rax), %rax
	movq	31(%rax), %r15
	testb	$1, %r15b
	je	.L33
.L70:
	movq	-1(%r15), %rax
	cmpw	$86, 11(%rax)
	jne	.L33
	movq	41112(%r14), %rdi
	movq	23(%r15), %r15
	testq	%rdi, %rdi
	jne	.L80
	.p2align 4,,10
	.p2align 3
.L27:
	movq	41088(%r14), %rsi
	cmpq	41096(%r14), %rsi
	je	.L81
.L29:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r14)
	movq	%r15, (%rsi)
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L81:
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L73:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L82
.L21:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L22
	movq	(%rdi), %rax
	call	*8(%rax)
.L22:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L23
	movq	(%rdi), %rax
	call	*8(%rax)
.L23:
	leaq	.LC1(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r13, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L77:
	movq	312(%r14), %r12
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L71:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$393, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L78:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L15
.L75:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L19
.L82:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC1(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L21
.L79:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18345:
	.size	_ZN2v88internalL31Stats_Runtime_DynamicImportCallEiPmPNS0_7IsolateE, .-_ZN2v88internalL31Stats_Runtime_DynamicImportCallEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL33Stats_Runtime_GetImportMetaObjectEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"V8.Runtime_Runtime_GetImportMetaObject"
	.section	.text._ZN2v88internalL33Stats_Runtime_GetImportMetaObjectEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL33Stats_Runtime_GetImportMetaObjectEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL33Stats_Runtime_GetImportMetaObjectEiPmPNS0_7IsolateE.isra.0:
.LFB22130:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	addq	$-128, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L117
.L84:
	movq	_ZZN2v88internalL33Stats_Runtime_GetImportMetaObjectEiPmPNS0_7IsolateEE27trace_event_unique_atomic40(%rip), %rbx
	testq	%rbx, %rbx
	je	.L118
.L86:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L119
.L88:
	addl	$1, 41104(%r12)
	movq	12464(%r12), %rax
	leaq	-152(%rbp), %rdi
	movq	41088(%r12), %r13
	movq	41096(%r12), %rbx
	movq	%rax, -152(%rbp)
	call	_ZN2v88internal7Context6moduleEv@PLT
	movq	41112(%r12), %rdi
	movq	%rax, %r14
	testq	%rdi, %rdi
	je	.L92
	movq	%rax, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L93:
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate41RunHostInitializeImportMetaObjectCallbackENS0_6HandleINS0_16SourceTextModuleEEE@PLT
	movq	(%rax), %r14
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L97
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L97:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L120
.L83:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L121
	leaq	-32(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L122
.L94:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L119:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L123
.L89:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L90
	movq	(%rdi), %rax
	call	*8(%rax)
.L90:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L91
	movq	(%rdi), %rax
	call	*8(%rax)
.L91:
	leaq	.LC4(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L118:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L124
.L87:
	movq	%rbx, _ZZN2v88internalL33Stats_Runtime_GetImportMetaObjectEiPmPNS0_7IsolateEE27trace_event_unique_atomic40(%rip)
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L117:
	movq	40960(%rdi), %rax
	leaq	-104(%rbp), %rsi
	movl	$394, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L120:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L122:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L123:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC4(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L124:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L87
.L121:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22130:
	.size	_ZN2v88internalL33Stats_Runtime_GetImportMetaObjectEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL33Stats_Runtime_GetImportMetaObjectEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL32Stats_Runtime_GetModuleNamespaceEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"V8.Runtime_Runtime_GetModuleNamespace"
	.section	.rodata._ZN2v88internalL32Stats_Runtime_GetModuleNamespaceEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC6:
	.string	"args[0].IsSmi()"
	.section	.text._ZN2v88internalL32Stats_Runtime_GetModuleNamespaceEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL32Stats_Runtime_GetModuleNamespaceEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL32Stats_Runtime_GetModuleNamespaceEiPmPNS0_7IsolateE.isra.0:
.LFB22137:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L160
.L126:
	movq	_ZZN2v88internalL32Stats_Runtime_GetModuleNamespaceEiPmPNS0_7IsolateEE27trace_event_unique_atomic32(%rip), %rbx
	testq	%rbx, %rbx
	je	.L161
.L128:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L162
.L130:
	movq	41088(%r12), %r14
	movq	41096(%r12), %r13
	addl	$1, 41104(%r12)
	movq	(%r15), %rbx
	testb	$1, %bl
	jne	.L163
	movq	12464(%r12), %rax
	leaq	-168(%rbp), %rdi
	sarq	$32, %rbx
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal7Context6moduleEv@PLT
	movq	41112(%r12), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L135
	movq	%rax, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L136:
	movl	%ebx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal16SourceTextModule18GetModuleNamespaceEPNS0_7IsolateENS0_6HandleIS1_EEi@PLT
	movq	(%rax), %r15
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r13
	je	.L140
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L140:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L164
.L125:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L165
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L135:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L166
.L137:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r15, (%rsi)
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L162:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L167
.L131:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L132
	movq	(%rdi), %rax
	call	*8(%rax)
.L132:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L133
	movq	(%rdi), %rax
	call	*8(%rax)
.L133:
	leaq	.LC5(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r13, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L161:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L168
.L129:
	movq	%rbx, _ZZN2v88internalL32Stats_Runtime_GetModuleNamespaceEiPmPNS0_7IsolateEE27trace_event_unique_atomic32(%rip)
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L163:
	leaq	.LC6(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L160:
	movq	40960(%rsi), %rax
	movl	$395, %edx
	leaq	-120(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L164:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L166:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L168:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L167:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC5(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L131
.L165:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22137:
	.size	_ZN2v88internalL32Stats_Runtime_GetModuleNamespaceEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL32Stats_Runtime_GetModuleNamespaceEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal25Runtime_DynamicImportCallEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal25Runtime_DynamicImportCallEiPmPNS0_7IsolateE
	.type	_ZN2v88internal25Runtime_DynamicImportCallEiPmPNS0_7IsolateE, @function
_ZN2v88internal25Runtime_DynamicImportCallEiPmPNS0_7IsolateE:
.LFB18346:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L199
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L200
.L171:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L200:
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L171
	movq	23(%rax), %rax
	leaq	-8(%rsi), %r12
	movq	31(%rax), %r14
	testb	$1, %r14b
	jne	.L198
	.p2align 4,,10
	.p2align 3
.L180:
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L174
.L203:
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r14
	movq	%rax, %rsi
.L175:
	movq	71(%r14), %rax
	testb	$1, %al
	jne	.L201
.L179:
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal7Isolate38RunHostImportModuleDynamicallyCallbackENS0_6HandleINS0_6ScriptEEENS2_INS0_6ObjectEEE@PLT
	testq	%rax, %rax
	je	.L202
	movq	(%rax), %r12
.L187:
	subl	$1, 41104(%r15)
	movq	%r13, 41088(%r15)
	cmpq	41096(%r15), %rbx
	je	.L169
	movq	%rbx, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L169:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L201:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$160, 11(%rax)
	jne	.L179
	movq	(%rsi), %rax
	movq	71(%rax), %rax
	movq	31(%rax), %r14
	testb	$1, %r14b
	je	.L180
.L198:
	movq	-1(%r14), %rax
	cmpw	$86, 11(%rax)
	jne	.L180
	movq	41112(%r15), %rdi
	movq	23(%r14), %r14
	testq	%rdi, %rdi
	jne	.L203
	.p2align 4,,10
	.p2align 3
.L174:
	movq	41088(%r15), %rsi
	cmpq	41096(%r15), %rsi
	je	.L204
.L176:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r15)
	movq	%r14, (%rsi)
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L204:
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L202:
	movq	312(%r15), %r12
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L199:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL31Stats_Runtime_DynamicImportCallEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE18346:
	.size	_ZN2v88internal25Runtime_DynamicImportCallEiPmPNS0_7IsolateE, .-_ZN2v88internal25Runtime_DynamicImportCallEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal26Runtime_GetModuleNamespaceEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal26Runtime_GetModuleNamespaceEiPmPNS0_7IsolateE
	.type	_ZN2v88internal26Runtime_GetModuleNamespaceEiPmPNS0_7IsolateE, @function
_ZN2v88internal26Runtime_GetModuleNamespaceEiPmPNS0_7IsolateE:
.LFB18349:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L216
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rbx
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %r13
	testb	$1, %bl
	jne	.L217
	movq	12464(%rdx), %rax
	leaq	-64(%rbp), %rdi
	sarq	$32, %rbx
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal7Context6moduleEv@PLT
	movq	41112(%r12), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L209
	movq	%rax, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L210:
	movl	%ebx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal16SourceTextModule18GetModuleNamespaceEPNS0_7IsolateENS0_6HandleIS1_EEi@PLT
	movq	(%rax), %r15
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r13
	je	.L205
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L205:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L218
	addq	$24, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L209:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L219
.L211:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r15, (%rsi)
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L216:
	movq	%rdx, %rsi
	call	_ZN2v88internalL32Stats_Runtime_GetModuleNamespaceEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r15
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L217:
	leaq	.LC6(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L219:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L211
.L218:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18349:
	.size	_ZN2v88internal26Runtime_GetModuleNamespaceEiPmPNS0_7IsolateE, .-_ZN2v88internal26Runtime_GetModuleNamespaceEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal27Runtime_GetImportMetaObjectEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal27Runtime_GetImportMetaObjectEiPmPNS0_7IsolateE
	.type	_ZN2v88internal27Runtime_GetImportMetaObjectEiPmPNS0_7IsolateE, @function
_ZN2v88internal27Runtime_GetImportMetaObjectEiPmPNS0_7IsolateE:
.LFB18352:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L230
	addl	$1, 41104(%rdx)
	movq	12464(%rdx), %rax
	leaq	-48(%rbp), %rdi
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal7Context6moduleEv@PLT
	movq	41112(%r12), %rdi
	movq	%rax, %r14
	testq	%rdi, %rdi
	je	.L223
	movq	%rax, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L224:
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate41RunHostInitializeImportMetaObjectCallbackENS0_6HandleINS0_16SourceTextModuleEEE@PLT
	movq	(%rax), %r14
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L220
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L220:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L231
	addq	$16, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L223:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L232
.L225:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L230:
	movq	%rdx, %rdi
	call	_ZN2v88internalL33Stats_Runtime_GetImportMetaObjectEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r14
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L232:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L225
.L231:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18352:
	.size	_ZN2v88internal27Runtime_GetImportMetaObjectEiPmPNS0_7IsolateE, .-_ZN2v88internal27Runtime_GetImportMetaObjectEiPmPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal25Runtime_DynamicImportCallEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal25Runtime_DynamicImportCallEiPmPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal25Runtime_DynamicImportCallEiPmPNS0_7IsolateE:
.LFB22109:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE22109:
	.size	_GLOBAL__sub_I__ZN2v88internal25Runtime_DynamicImportCallEiPmPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal25Runtime_DynamicImportCallEiPmPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal25Runtime_DynamicImportCallEiPmPNS0_7IsolateE
	.section	.bss._ZZN2v88internalL33Stats_Runtime_GetImportMetaObjectEiPmPNS0_7IsolateEE27trace_event_unique_atomic40,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL33Stats_Runtime_GetImportMetaObjectEiPmPNS0_7IsolateEE27trace_event_unique_atomic40, @object
	.size	_ZZN2v88internalL33Stats_Runtime_GetImportMetaObjectEiPmPNS0_7IsolateEE27trace_event_unique_atomic40, 8
_ZZN2v88internalL33Stats_Runtime_GetImportMetaObjectEiPmPNS0_7IsolateEE27trace_event_unique_atomic40:
	.zero	8
	.section	.bss._ZZN2v88internalL32Stats_Runtime_GetModuleNamespaceEiPmPNS0_7IsolateEE27trace_event_unique_atomic32,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL32Stats_Runtime_GetModuleNamespaceEiPmPNS0_7IsolateEE27trace_event_unique_atomic32, @object
	.size	_ZZN2v88internalL32Stats_Runtime_GetModuleNamespaceEiPmPNS0_7IsolateEE27trace_event_unique_atomic32, 8
_ZZN2v88internalL32Stats_Runtime_GetModuleNamespaceEiPmPNS0_7IsolateEE27trace_event_unique_atomic32:
	.zero	8
	.section	.bss._ZZN2v88internalL31Stats_Runtime_DynamicImportCallEiPmPNS0_7IsolateEE27trace_event_unique_atomic15,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL31Stats_Runtime_DynamicImportCallEiPmPNS0_7IsolateEE27trace_event_unique_atomic15, @object
	.size	_ZZN2v88internalL31Stats_Runtime_DynamicImportCallEiPmPNS0_7IsolateEE27trace_event_unique_atomic15, 8
_ZZN2v88internalL31Stats_Runtime_DynamicImportCallEiPmPNS0_7IsolateEE27trace_event_unique_atomic15:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
