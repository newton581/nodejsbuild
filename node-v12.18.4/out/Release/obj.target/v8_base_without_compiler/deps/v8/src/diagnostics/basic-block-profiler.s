	.file	"basic-block-profiler.cc"
	.text
	.section	.text._ZNKSt5ctypeIcE8do_widenEc,"axG",@progbits,_ZNKSt5ctypeIcE8do_widenEc,comdat
	.align 2
	.p2align 4
	.weak	_ZNKSt5ctypeIcE8do_widenEc
	.type	_ZNKSt5ctypeIcE8do_widenEc, @function
_ZNKSt5ctypeIcE8do_widenEc:
.LFB2086:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	ret
	.cfi_endproc
.LFE2086:
	.size	_ZNKSt5ctypeIcE8do_widenEc, .-_ZNKSt5ctypeIcE8do_widenEc
	.section	.text._ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPSt4pairIijESt6vectorIS3_SaIS3_EEEElS3_NS0_5__ops15_Iter_comp_iterIZN2v88internallsERSoRKNSC_18BasicBlockProfiler4DataEEUlS3_S3_E_EEEvT_T0_SL_T1_T2_.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPSt4pairIijESt6vectorIS3_SaIS3_EEEElS3_NS0_5__ops15_Iter_comp_iterIZN2v88internallsERSoRKNSC_18BasicBlockProfiler4DataEEUlS3_S3_E_EEEvT_T0_SL_T1_T2_.isra.0, @function
_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPSt4pairIijESt6vectorIS3_SaIS3_EEEElS3_NS0_5__ops15_Iter_comp_iterIZN2v88internallsERSoRKNSC_18BasicBlockProfiler4DataEEUlS3_S3_E_EEEvT_T0_SL_T1_T2_.isra.0:
.LFB5058:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	-1(%rdx), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rax, %r13
	pushq	%r12
	shrq	$63, %r13
	pushq	%rbx
	addq	%rax, %r13
	sarq	%r13
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, -48(%rbp)
	cmpq	%r13, %rsi
	jge	.L4
	movq	%rsi, %rbx
	.p2align 4,,10
	.p2align 3
.L10:
	leaq	1(%rbx), %rax
	leaq	(%rax,%rax), %r8
	salq	$4, %rax
	leaq	-1(%r8), %r9
	addq	%rdi, %rax
	leaq	(%rdi,%r9,8), %rcx
	movl	4(%rax), %r10d
	movl	(%rax), %r15d
	movl	4(%rcx), %r11d
	movl	(%rcx), %r14d
	cmpl	%r10d, %r11d
	setb	%r12b
	je	.L30
.L6:
	testb	%r12b, %r12b
	jne	.L7
	leaq	(%rdi,%rbx,8), %rcx
	movl	%r15d, (%rcx)
	movl	4(%rax), %r9d
	movl	%r9d, 4(%rcx)
	cmpq	%r8, %r13
	jle	.L8
	movq	%r8, %rbx
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L7:
	leaq	(%rdi,%rbx,8), %rax
	movl	%r14d, (%rax)
	movl	4(%rcx), %r8d
	movl	%r8d, 4(%rax)
	cmpq	%r9, %r13
	jle	.L18
	movq	%r9, %rbx
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L30:
	cmpl	%r15d, %r14d
	setg	%r12b
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L18:
	movq	%rcx, %rax
	movq	%r9, %r8
.L8:
	testb	$1, %dl
	je	.L11
.L13:
	leaq	-1(%r8), %r9
	movq	-48(%rbp), %rbx
	movq	%r9, %rdx
	shrq	$63, %rdx
	movl	%ebx, %r10d
	shrq	$32, %rbx
	addq	%r9, %rdx
	movq	%rbx, %rcx
	sarq	%rdx
	cmpq	%rsi, %r8
	jg	.L17
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L16:
	leaq	(%rdi,%r8,8), %rax
	testb	%r11b, %r11b
	je	.L14
.L32:
	movl	%ebx, (%rax)
	movl	4(%r9), %r8d
	movl	%r8d, 4(%rax)
	leaq	-1(%rdx), %r8
	movq	%r8, %rax
	shrq	$63, %rax
	addq	%r8, %rax
	movq	%rdx, %r8
	sarq	%rax
	cmpq	%rdx, %rsi
	jge	.L31
	movq	%rax, %rdx
.L17:
	leaq	(%rdi,%rdx,8), %r9
	movl	4(%r9), %eax
	movl	(%r9), %ebx
	cmpl	%eax, %ecx
	setb	%r11b
	jne	.L16
	cmpl	%ebx, %r10d
	leaq	(%rdi,%r8,8), %rax
	setg	%r11b
	testb	%r11b, %r11b
	jne	.L32
.L14:
	movl	%r10d, (%rax)
	movl	%ecx, 4(%rax)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	.cfi_restore_state
	leaq	(%rdi,%rsi,8), %rax
	testb	$1, %dl
	jne	.L33
	movq	%rsi, %r8
	.p2align 4,,10
	.p2align 3
.L11:
	subq	$2, %rdx
	movq	%rdx, %rcx
	shrq	$63, %rcx
	addq	%rcx, %rdx
	sarq	%rdx
	cmpq	%r8, %rdx
	jne	.L13
	leaq	1(%r8,%r8), %r8
	leaq	(%rdi,%r8,8), %rdx
	movl	(%rdx), %ecx
	movl	%ecx, (%rax)
	movl	4(%rdx), %ecx
	movl	%ecx, 4(%rax)
	movq	%rdx, %rax
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L31:
	movq	%r9, %rax
	jmp	.L14
.L33:
	movq	%rcx, %rsi
	movl	%ecx, %r10d
	shrq	$32, %rsi
	movq	%rsi, %rcx
	jmp	.L14
	.cfi_endproc
.LFE5058:
	.size	_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPSt4pairIijESt6vectorIS3_SaIS3_EEEElS3_NS0_5__ops15_Iter_comp_iterIZN2v88internallsERSoRKNSC_18BasicBlockProfiler4DataEEUlS3_S3_E_EEEvT_T0_SL_T1_T2_.isra.0, .-_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPSt4pairIijESt6vectorIS3_SaIS3_EEEElS3_NS0_5__ops15_Iter_comp_iterIZN2v88internallsERSoRKNSC_18BasicBlockProfiler4DataEEUlS3_S3_E_EEEvT_T0_SL_T1_T2_.isra.0
	.section	.text._ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPSt4pairIijESt6vectorIS3_SaIS3_EEEElNS0_5__ops15_Iter_comp_iterIZN2v88internallsERSoRKNSC_18BasicBlockProfiler4DataEEUlS3_S3_E_EEEvT_SK_T0_T1_,"ax",@progbits
	.p2align 4
	.type	_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPSt4pairIijESt6vectorIS3_SaIS3_EEEElNS0_5__ops15_Iter_comp_iterIZN2v88internallsERSoRKNSC_18BasicBlockProfiler4DataEEUlS3_S3_E_EEEvT_SK_T0_T1_, @function
_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPSt4pairIijESt6vectorIS3_SaIS3_EEEElNS0_5__ops15_Iter_comp_iterIZN2v88internallsERSoRKNSC_18BasicBlockProfiler4DataEEUlS3_S3_E_EEEvT_SK_T0_T1_:
.LFB4709:
	.cfi_startproc
	movq	%rsi, %rax
	subq	%rdi, %rax
	cmpq	$128, %rax
	jle	.L76
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	8(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	testq	%rdx, %rdx
	je	.L81
.L38:
	movq	%r8, %rax
	subq	$1, %r15
	subq	%rbx, %rax
	movq	%rax, %rdx
	shrq	$63, %rax
	sarq	$3, %rdx
	addq	%rdx, %rax
	movl	8(%rbx), %edx
	sarq	%rax
	leaq	(%rbx,%rax,8), %rsi
	movl	12(%rbx), %eax
	movl	4(%rsi), %ecx
	movl	(%rsi), %r10d
	cmpl	%ecx, %eax
	seta	%r9b
	je	.L82
.L44:
	movl	-8(%r8), %r11d
	movl	-4(%r8), %edi
	testb	%r9b, %r9b
	je	.L45
	cmpl	%ecx, %edi
	setb	%r9b
	je	.L83
.L47:
	movl	(%rbx), %ecx
	testb	%r9b, %r9b
	je	.L48
	movl	%r10d, (%rbx)
	movl	%ecx, (%rsi)
.L79:
	movl	4(%rbx), %eax
	movl	4(%rsi), %edx
	movl	%edx, 4(%rbx)
	movl	%eax, 4(%rsi)
	movl	(%rbx), %edx
	movl	4(%rbx), %eax
.L49:
	movq	%r13, %r12
	movq	%r8, %rcx
	.p2align 4,,10
	.p2align 3
.L59:
	movl	4(%r12), %r9d
	movl	(%r12), %edi
	movq	%r12, %r14
	cmpl	%eax, %r9d
	seta	%sil
	je	.L84
.L61:
	testb	%sil, %sil
	jne	.L62
	leaq	-8(%rcx), %rsi
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L64:
	subq	$8, %rsi
	testb	%r10b, %r10b
	je	.L85
.L65:
	movl	4(%rsi), %r11d
	movl	(%rsi), %r9d
	movq	%rsi, %rcx
	cmpl	%eax, %r11d
	setb	%r10b
	jne	.L64
	cmpl	%r9d, %edx
	setl	%r10b
	subq	$8, %rsi
	testb	%r10b, %r10b
	jne	.L65
.L85:
	cmpq	%rcx, %r12
	jnb	.L86
	movl	%r9d, (%r12)
	movl	4(%rcx), %edx
	movl	%edi, (%rcx)
	movl	4(%r12), %eax
	movl	%edx, 4(%r12)
	movl	%eax, 4(%rcx)
	movl	(%rbx), %edx
	movl	4(%rbx), %eax
.L62:
	addq	$8, %r12
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L84:
	cmpl	%edi, %edx
	setg	%sil
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L86:
	movq	%r15, %rdx
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPSt4pairIijESt6vectorIS3_SaIS3_EEEElNS0_5__ops15_Iter_comp_iterIZN2v88internallsERSoRKNSC_18BasicBlockProfiler4DataEEUlS3_S3_E_EEEvT_SK_T0_T1_
	movq	%r12, %rax
	subq	%rbx, %rax
	cmpq	$128, %rax
	jle	.L34
	testq	%r15, %r15
	je	.L36
	movq	%r12, %r8
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L45:
	cmpl	%eax, %edi
	setb	%r9b
	je	.L87
.L54:
	movl	(%rbx), %r12d
	testb	%r9b, %r9b
	je	.L55
	movl	4(%rbx), %ecx
	movl	%edx, (%rbx)
	movl	%r12d, 8(%rbx)
	movl	%eax, 4(%rbx)
	movl	%ecx, 12(%rbx)
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L48:
	cmpl	%eax, %edi
	setb	%sil
	je	.L88
.L51:
	testb	%sil, %sil
	je	.L52
	movl	%r11d, (%rbx)
	movl	%ecx, -8(%r8)
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L55:
	cmpl	%ecx, %edi
	setb	%al
	je	.L89
.L57:
	testb	%al, %al
	je	.L58
	movl	%r11d, (%rbx)
	movl	%r12d, -8(%r8)
.L80:
	movl	4(%rbx), %eax
	movl	-4(%r8), %edx
	movl	%edx, 4(%rbx)
	movl	%eax, -4(%r8)
	movl	(%rbx), %edx
	movl	4(%rbx), %eax
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L81:
	movq	%rsi, %r14
.L36:
	sarq	$3, %rax
	leaq	-2(%rax), %r12
	movq	%rax, %r13
	sarq	%r12
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L39:
	subq	$1, %r12
.L41:
	movq	(%rbx,%r12,8), %rcx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPSt4pairIijESt6vectorIS3_SaIS3_EEEElS3_NS0_5__ops15_Iter_comp_iterIZN2v88internallsERSoRKNSC_18BasicBlockProfiler4DataEEUlS3_S3_E_EEEvT_T0_SL_T1_T2_.isra.0
	testq	%r12, %r12
	jne	.L39
	subq	$8, %r14
	.p2align 4,,10
	.p2align 3
.L40:
	movl	(%rbx), %eax
	movq	(%r14), %rcx
	movq	%r14, %r12
	xorl	%esi, %esi
	subq	%rbx, %r12
	movq	%rbx, %rdi
	subq	$8, %r14
	movl	%eax, 8(%r14)
	movl	4(%rbx), %eax
	movq	%r12, %rdx
	sarq	$3, %rdx
	movl	%eax, 12(%r14)
	call	_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPSt4pairIijESt6vectorIS3_SaIS3_EEEElS3_NS0_5__ops15_Iter_comp_iterIZN2v88internallsERSoRKNSC_18BasicBlockProfiler4DataEEUlS3_S3_E_EEEvT_T0_SL_T1_T2_.isra.0
	cmpq	$8, %r12
	jg	.L40
.L34:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L82:
	.cfi_restore_state
	cmpl	%r10d, %edx
	setl	%r9b
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L58:
	movl	%r10d, (%rbx)
	movl	%r12d, (%rsi)
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L52:
	movl	%ecx, 8(%rbx)
	movl	4(%rbx), %ecx
	movl	%edx, (%rbx)
	movl	%eax, 4(%rbx)
	movl	%ecx, 12(%rbx)
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L87:
	cmpl	%r11d, %edx
	setl	%r9b
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L83:
	cmpl	%r11d, %r10d
	setl	%r9b
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L89:
	cmpl	%r11d, %r10d
	setl	%al
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L88:
	cmpl	%r11d, %edx
	setl	%sil
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L76:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE4709:
	.size	_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPSt4pairIijESt6vectorIS3_SaIS3_EEEElNS0_5__ops15_Iter_comp_iterIZN2v88internallsERSoRKNSC_18BasicBlockProfiler4DataEEUlS3_S3_E_EEEvT_SK_T0_T1_, .-_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPSt4pairIijESt6vectorIS3_SaIS3_EEEElNS0_5__ops15_Iter_comp_iterIZN2v88internallsERSoRKNSC_18BasicBlockProfiler4DataEEUlS3_S3_E_EEEvT_SK_T0_T1_
	.section	.text._ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPSt4pairIijESt6vectorIS3_SaIS3_EEEENS0_5__ops15_Iter_comp_iterIZN2v88internallsERSoRKNSC_18BasicBlockProfiler4DataEEUlS3_S3_E_EEEvT_SK_T0_.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPSt4pairIijESt6vectorIS3_SaIS3_EEEENS0_5__ops15_Iter_comp_iterIZN2v88internallsERSoRKNSC_18BasicBlockProfiler4DataEEUlS3_S3_E_EEEvT_SK_T0_.isra.0, @function
_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPSt4pairIijESt6vectorIS3_SaIS3_EEEENS0_5__ops15_Iter_comp_iterIZN2v88internallsERSoRKNSC_18BasicBlockProfiler4DataEEUlS3_S3_E_EEEvT_SK_T0_.isra.0:
.LFB5095:
	.cfi_startproc
	cmpq	%rsi, %rdi
	je	.L90
	leaq	8(%rdi), %rcx
	cmpq	%rcx, %rsi
	je	.L112
	.p2align 4,,10
	.p2align 3
.L103:
	movl	4(%rcx), %edx
	movl	4(%rdi), %eax
	movl	(%rcx), %r11d
	movl	(%rdi), %r9d
	cmpl	%eax, %edx
	seta	%r8b
	je	.L113
.L94:
	leaq	8(%rcx), %r10
	movq	%rcx, %rax
	testb	%r8b, %r8b
	je	.L102
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L100:
	testb	%r9b, %r9b
	je	.L101
.L115:
	movl	-4(%rax), %ecx
	movl	%r8d, (%rax)
	subq	$8, %rax
	movl	%ecx, 12(%rax)
.L102:
	movl	-4(%rax), %ecx
	movl	-8(%rax), %r8d
	cmpl	%ecx, %edx
	seta	%r9b
	jne	.L100
	cmpl	%r8d, %r11d
	setl	%r9b
	testb	%r9b, %r9b
	jne	.L115
.L101:
	movl	%r11d, (%rax)
	movl	%edx, 4(%rax)
	cmpq	%r10, %rsi
	je	.L90
.L105:
	movq	%r10, %rcx
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L114:
	subq	%rdi, %rcx
	movq	%r10, %rax
	movq	%rcx, %r8
	sarq	$3, %rcx
	testq	%r8, %r8
	jle	.L98
	.p2align 4,,10
	.p2align 3
.L96:
	movl	-16(%rax), %r8d
	subq	$8, %rax
	movl	%r8d, (%rax)
	movl	-4(%rax), %r8d
	movl	%r8d, 4(%rax)
	subq	$1, %rcx
	jne	.L96
.L98:
	movl	%r11d, (%rdi)
	movl	%edx, 4(%rdi)
	cmpq	%r10, %rsi
	jne	.L105
.L90:
	ret
	.p2align 4,,10
	.p2align 3
.L113:
	cmpl	%r9d, %r11d
	setl	%r8b
	jmp	.L94
.L112:
	ret
	.cfi_endproc
.LFE5095:
	.size	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPSt4pairIijESt6vectorIS3_SaIS3_EEEENS0_5__ops15_Iter_comp_iterIZN2v88internallsERSoRKNSC_18BasicBlockProfiler4DataEEUlS3_S3_E_EEEvT_SK_T0_.isra.0, .-_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPSt4pairIijESt6vectorIS3_SaIS3_EEEENS0_5__ops15_Iter_comp_iterIZN2v88internallsERSoRKNSC_18BasicBlockProfiler4DataEEUlS3_S3_E_EEEvT_SK_T0_.isra.0
	.section	.text._ZN2v88internal18BasicBlockProfiler3GetEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18BasicBlockProfiler3GetEv
	.type	_ZN2v88internal18BasicBlockProfiler3GetEv, @function
_ZN2v88internal18BasicBlockProfiler3GetEv:
.LFB3967:
	.cfi_startproc
	endbr64
	movzbl	_ZGVZN2v88internal18BasicBlockProfiler3GetEvE6object(%rip), %eax
	testb	%al, %al
	je	.L127
	leaq	_ZZN2v88internal18BasicBlockProfiler3GetEvE6object(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L127:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZGVZN2v88internal18BasicBlockProfiler3GetEvE6object(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	jne	.L128
	leaq	_ZZN2v88internal18BasicBlockProfiler3GetEvE6object(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L128:
	.cfi_restore_state
	leaq	_ZZN2v88internal18BasicBlockProfiler3GetEvE6object(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	$0, 56+_ZZN2v88internal18BasicBlockProfiler3GetEvE6object(%rip)
	movups	%xmm0, 24+_ZZN2v88internal18BasicBlockProfiler3GetEvE6object(%rip)
	leaq	24(%rax), %rdi
	movups	%xmm0, 40+_ZZN2v88internal18BasicBlockProfiler3GetEvE6object(%rip)
	movq	%rax, %xmm0
	movq	$0, 16+_ZZN2v88internal18BasicBlockProfiler3GetEvE6object(%rip)
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, _ZZN2v88internal18BasicBlockProfiler3GetEvE6object(%rip)
	call	_ZN2v84base5MutexC1Ev@PLT
	leaq	_ZGVZN2v88internal18BasicBlockProfiler3GetEvE6object(%rip), %rdi
	call	__cxa_guard_release@PLT
	leaq	_ZZN2v88internal18BasicBlockProfiler3GetEvE6object(%rip), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3967:
	.size	_ZN2v88internal18BasicBlockProfiler3GetEv, .-_ZN2v88internal18BasicBlockProfiler3GetEv
	.section	.rodata._ZN2v88internal18BasicBlockProfiler4DataC2Em.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"cannot create std::vector larger than max_size()"
	.section	.text._ZN2v88internal18BasicBlockProfiler4DataC2Em,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18BasicBlockProfiler4DataC2Em
	.type	_ZN2v88internal18BasicBlockProfiler4DataC2Em, @function
_ZN2v88internal18BasicBlockProfiler4DataC2Em:
.LFB3969:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movabsq	$2305843009213693951, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, (%rdi)
	cmpq	%r13, %rsi
	ja	.L132
	pxor	%xmm0, %xmm0
	movq	$0, 24(%rdi)
	movq	%rdi, %rbx
	leaq	0(,%rsi,4), %r12
	movups	%xmm0, 8(%rdi)
	testq	%rsi, %rsi
	je	.L131
	movq	%r12, %rdi
	call	_Znwm@PLT
	movq	%r12, %rdx
	xorl	%esi, %esi
	leaq	(%rax,%r12), %r14
	movq	%rax, 8(%rbx)
	movq	%rax, %rdi
	movq	%r14, 24(%rbx)
	call	memset@PLT
	movq	(%rbx), %rax
	movq	%r14, 16(%rbx)
	cmpq	%r13, %rax
	ja	.L132
	pxor	%xmm0, %xmm0
	movq	$0, 48(%rbx)
	leaq	0(,%rax,4), %r12
	movups	%xmm0, 32(%rbx)
	testq	%rax, %rax
	je	.L133
	movq	%r12, %rdi
	call	_Znwm@PLT
	movq	%r12, %rdx
	xorl	%esi, %esi
	leaq	(%rax,%r12), %r13
	movq	%rax, 32(%rbx)
	movq	%rax, %rdi
	movq	%r13, 48(%rbx)
	call	memset@PLT
.L134:
	leaq	72(%rbx), %rax
	movq	%r13, 40(%rbx)
	movq	%rax, 56(%rbx)
	leaq	104(%rbx), %rax
	movq	%rax, 88(%rbx)
	leaq	136(%rbx), %rax
	movq	$0, 64(%rbx)
	movb	$0, 72(%rbx)
	movq	$0, 96(%rbx)
	movb	$0, 104(%rbx)
	movq	%rax, 120(%rbx)
	movq	$0, 128(%rbx)
	movb	$0, 136(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L131:
	.cfi_restore_state
	movq	$0, 16(%rdi)
	movups	%xmm0, 32(%rdi)
.L133:
	movq	$0, 48(%rbx)
	xorl	%r13d, %r13d
	jmp	.L134
.L132:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE3969:
	.size	_ZN2v88internal18BasicBlockProfiler4DataC2Em, .-_ZN2v88internal18BasicBlockProfiler4DataC2Em
	.globl	_ZN2v88internal18BasicBlockProfiler4DataC1Em
	.set	_ZN2v88internal18BasicBlockProfiler4DataC1Em,_ZN2v88internal18BasicBlockProfiler4DataC2Em
	.section	.text._ZN2v88internal18BasicBlockProfiler4Data7SetCodeEPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18BasicBlockProfiler4Data7SetCodeEPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE
	.type	_ZN2v88internal18BasicBlockProfiler4Data7SetCodeEPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE, @function
_ZN2v88internal18BasicBlockProfiler4Data7SetCodeEPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE:
.LFB3973:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	120(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	leaq	-48(%rbp), %rbx
	leaq	-64(%rbp), %rdi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	48(%rsi), %rax
	movq	%rbx, -64(%rbp)
	movq	$0, -56(%rbp)
	movb	$0, -48(%rbp)
	testq	%rax, %rax
	je	.L141
	movq	32(%rsi), %r8
	movq	40(%rsi), %rcx
	cmpq	%r8, %rax
	ja	.L147
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L143:
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %rcx
	movq	%r12, %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	-64(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L140
	call	_ZdlPv@PLT
.L140:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L148
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L147:
	.cfi_restore_state
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L141:
	addq	$80, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L143
.L148:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3973:
	.size	_ZN2v88internal18BasicBlockProfiler4Data7SetCodeEPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE, .-_ZN2v88internal18BasicBlockProfiler4Data7SetCodeEPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE
	.section	.text._ZN2v88internal18BasicBlockProfiler4Data15SetFunctionNameESt10unique_ptrIA_cSt14default_deleteIS4_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18BasicBlockProfiler4Data15SetFunctionNameESt10unique_ptrIA_cSt14default_deleteIS4_EE
	.type	_ZN2v88internal18BasicBlockProfiler4Data15SetFunctionNameESt10unique_ptrIA_cSt14default_deleteIS4_EE, @function
_ZN2v88internal18BasicBlockProfiler4Data15SetFunctionNameESt10unique_ptrIA_cSt14default_deleteIS4_EE:
.LFB3976:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rsi), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	strlen@PLT
	leaq	56(%rbx), %rdi
	movq	%r12, %rcx
	popq	%rbx
	movq	%rax, %r8
	popq	%r12
	xorl	%edx, %edx
	xorl	%esi, %esi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	.cfi_endproc
.LFE3976:
	.size	_ZN2v88internal18BasicBlockProfiler4Data15SetFunctionNameESt10unique_ptrIA_cSt14default_deleteIS4_EE, .-_ZN2v88internal18BasicBlockProfiler4Data15SetFunctionNameESt10unique_ptrIA_cSt14default_deleteIS4_EE
	.section	.text._ZN2v88internal18BasicBlockProfiler4Data11SetScheduleEPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18BasicBlockProfiler4Data11SetScheduleEPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE
	.type	_ZN2v88internal18BasicBlockProfiler4Data11SetScheduleEPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE, @function
_ZN2v88internal18BasicBlockProfiler4Data11SetScheduleEPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE:
.LFB3977:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	88(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	leaq	-48(%rbp), %rbx
	leaq	-64(%rbp), %rdi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	48(%rsi), %rax
	movq	%rbx, -64(%rbp)
	movq	$0, -56(%rbp)
	movb	$0, -48(%rbp)
	testq	%rax, %rax
	je	.L152
	movq	32(%rsi), %r8
	movq	40(%rsi), %rcx
	cmpq	%r8, %rax
	ja	.L158
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L154:
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %rcx
	movq	%r12, %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	-64(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L151
	call	_ZdlPv@PLT
.L151:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L159
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L158:
	.cfi_restore_state
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L152:
	addq	$80, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L154
.L159:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3977:
	.size	_ZN2v88internal18BasicBlockProfiler4Data11SetScheduleEPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE, .-_ZN2v88internal18BasicBlockProfiler4Data11SetScheduleEPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE
	.section	.text._ZN2v88internal18BasicBlockProfiler4Data17SetBlockRpoNumberEmi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18BasicBlockProfiler4Data17SetBlockRpoNumberEmi
	.type	_ZN2v88internal18BasicBlockProfiler4Data17SetBlockRpoNumberEmi, @function
_ZN2v88internal18BasicBlockProfiler4Data17SetBlockRpoNumberEmi:
.LFB3978:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movl	%edx, (%rax,%rsi,4)
	ret
	.cfi_endproc
.LFE3978:
	.size	_ZN2v88internal18BasicBlockProfiler4Data17SetBlockRpoNumberEmi, .-_ZN2v88internal18BasicBlockProfiler4Data17SetBlockRpoNumberEmi
	.section	.text._ZN2v88internal18BasicBlockProfiler4Data17GetCounterAddressEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18BasicBlockProfiler4Data17GetCounterAddressEm
	.type	_ZN2v88internal18BasicBlockProfiler4Data17GetCounterAddressEm, @function
_ZN2v88internal18BasicBlockProfiler4Data17GetCounterAddressEm:
.LFB3979:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	leaq	(%rax,%rsi,4), %rax
	ret
	.cfi_endproc
.LFE3979:
	.size	_ZN2v88internal18BasicBlockProfiler4Data17GetCounterAddressEm, .-_ZN2v88internal18BasicBlockProfiler4Data17GetCounterAddressEm
	.section	.text._ZN2v88internal18BasicBlockProfiler4Data11ResetCountsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18BasicBlockProfiler4Data11ResetCountsEv
	.type	_ZN2v88internal18BasicBlockProfiler4Data11ResetCountsEv, @function
_ZN2v88internal18BasicBlockProfiler4Data11ResetCountsEv:
.LFB3980:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L162
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L164:
	movq	32(%rdi), %rdx
	movl	$0, (%rdx,%rax,4)
	addq	$1, %rax
	cmpq	%rax, (%rdi)
	ja	.L164
.L162:
	ret
	.cfi_endproc
.LFE3980:
	.size	_ZN2v88internal18BasicBlockProfiler4Data11ResetCountsEv, .-_ZN2v88internal18BasicBlockProfiler4Data11ResetCountsEv
	.section	.text._ZN2v88internal18BasicBlockProfiler7NewDataEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18BasicBlockProfiler7NewDataEm
	.type	_ZN2v88internal18BasicBlockProfiler7NewDataEm, @function
_ZN2v88internal18BasicBlockProfiler7NewDataEm:
.LFB3981:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	24(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movl	$152, %edi
	call	_Znwm@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal18BasicBlockProfiler4DataC1Em
	movl	$24, %edi
	call	_Znwm@PLT
	movq	%rbx, %rsi
	movq	%r12, 16(%rax)
	movq	%rax, %rdi
	call	_ZNSt8__detail15_List_node_base7_M_hookEPS0_@PLT
	addq	$1, 16(%rbx)
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3981:
	.size	_ZN2v88internal18BasicBlockProfiler7NewDataEm, .-_ZN2v88internal18BasicBlockProfiler7NewDataEm
	.section	.text._ZN2v88internal18BasicBlockProfilerD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18BasicBlockProfilerD2Ev
	.type	_ZN2v88internal18BasicBlockProfilerD2Ev, @function
_ZN2v88internal18BasicBlockProfilerD2Ev:
.LFB3989:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	(%rdi), %rbx
	cmpq	%rdi, %rbx
	je	.L169
	.p2align 4,,10
	.p2align 3
.L176:
	movq	16(%rbx), %r12
	testq	%r12, %r12
	je	.L170
	movq	120(%r12), %rdi
	leaq	136(%r12), %rax
	cmpq	%rax, %rdi
	je	.L171
	call	_ZdlPv@PLT
.L171:
	movq	88(%r12), %rdi
	leaq	104(%r12), %rax
	cmpq	%rax, %rdi
	je	.L172
	call	_ZdlPv@PLT
.L172:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L173
	call	_ZdlPv@PLT
.L173:
	movq	32(%r12), %rdi
	testq	%rdi, %rdi
	je	.L174
	call	_ZdlPv@PLT
.L174:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L175
	call	_ZdlPv@PLT
.L175:
	movl	$152, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L170:
	movq	(%rbx), %rbx
	cmpq	%r13, %rbx
	jne	.L176
.L169:
	leaq	24(%r13), %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	movq	0(%r13), %r12
	cmpq	%r13, %r12
	je	.L168
	.p2align 4,,10
	.p2align 3
.L178:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	cmpq	%r13, %r12
	jne	.L178
.L168:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3989:
	.size	_ZN2v88internal18BasicBlockProfilerD2Ev, .-_ZN2v88internal18BasicBlockProfilerD2Ev
	.globl	_ZN2v88internal18BasicBlockProfilerD1Ev
	.set	_ZN2v88internal18BasicBlockProfilerD1Ev,_ZN2v88internal18BasicBlockProfilerD2Ev
	.section	.text._ZN2v88internal18BasicBlockProfiler11ResetCountsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18BasicBlockProfiler11ResetCountsEv
	.type	_ZN2v88internal18BasicBlockProfiler11ResetCountsEv, @function
_ZN2v88internal18BasicBlockProfiler11ResetCountsEv:
.LFB3991:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rsi
	cmpq	%rdi, %rsi
	je	.L191
	.p2align 4,,10
	.p2align 3
.L195:
	movq	16(%rsi), %rdx
	cmpq	$0, (%rdx)
	je	.L193
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L194:
	movq	32(%rdx), %rcx
	movl	$0, (%rcx,%rax,4)
	addq	$1, %rax
	cmpq	%rax, (%rdx)
	ja	.L194
.L193:
	movq	(%rsi), %rsi
	cmpq	%rdi, %rsi
	jne	.L195
.L191:
	ret
	.cfi_endproc
.LFE3991:
	.size	_ZN2v88internal18BasicBlockProfiler11ResetCountsEv, .-_ZN2v88internal18BasicBlockProfiler11ResetCountsEv
	.section	.rodata._ZNSt6vectorISt4pairIijESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_.str1.1,"aMS",@progbits,1
.LC1:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorISt4pairIijESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt4pairIijESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt4pairIijESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	.type	_ZNSt6vectorISt4pairIijESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_, @function
_ZNSt6vectorISt4pairIijESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_:
.LFB4704:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movabsq	$1152921504606846975, %rsi
	subq	$40, %rsp
	movq	8(%rdi), %r12
	movq	(%rdi), %r15
	movq	%r12, %rax
	subq	%r15, %rax
	sarq	$3, %rax
	cmpq	%rsi, %rax
	je	.L217
	movq	%rbx, %r9
	movq	%rdi, %r13
	subq	%r15, %r9
	testq	%rax, %rax
	je	.L208
	movabsq	$9223372036854775800, %rcx
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L218
.L200:
	movq	%rcx, %rdi
	movq	%rdx, -72(%rbp)
	movq	%r9, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %r9
	movq	%rax, %r14
	movq	-72(%rbp), %rdx
	leaq	(%rax,%rcx), %rax
	leaq	8(%r14), %r8
.L207:
	movq	(%rdx), %rdx
	movq	%rdx, (%r14,%r9)
	cmpq	%r15, %rbx
	je	.L202
	movq	%r14, %rcx
	movq	%r15, %rdx
	.p2align 4,,10
	.p2align 3
.L203:
	movl	(%rdx), %edi
	movl	4(%rdx), %esi
	addq	$8, %rdx
	addq	$8, %rcx
	movl	%edi, -8(%rcx)
	movl	%esi, -4(%rcx)
	cmpq	%rdx, %rbx
	jne	.L203
	movq	%rbx, %rdx
	subq	%r15, %rdx
	leaq	8(%r14,%rdx), %r8
.L202:
	cmpq	%r12, %rbx
	je	.L204
	movq	%rbx, %rdx
	movq	%r8, %rcx
	.p2align 4,,10
	.p2align 3
.L205:
	movl	4(%rdx), %esi
	movl	(%rdx), %edi
	addq	$8, %rdx
	addq	$8, %rcx
	movl	%edi, -8(%rcx)
	movl	%esi, -4(%rcx)
	cmpq	%r12, %rdx
	jne	.L205
	subq	%rbx, %rdx
	addq	%rdx, %r8
.L204:
	testq	%r15, %r15
	je	.L206
	movq	%r15, %rdi
	movq	%rax, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %rax
	movq	-56(%rbp), %r8
.L206:
	movq	%r14, %xmm0
	movq	%r8, %xmm1
	movq	%rax, 16(%r13)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 0(%r13)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L218:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L201
	movl	$8, %r8d
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L208:
	movl	$8, %ecx
	jmp	.L200
.L201:
	cmpq	%rsi, %rdi
	movq	%rsi, %rax
	cmovbe	%rdi, %rax
	leaq	0(,%rax,8), %rcx
	jmp	.L200
.L217:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE4704:
	.size	_ZNSt6vectorISt4pairIijESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_, .-_ZNSt6vectorISt4pairIijESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	.section	.rodata._ZN2v88internallsERSoRKNS0_18BasicBlockProfiler4DataE.part.0.str1.1,"aMS",@progbits,1
.LC2:
	.string	"unknown function"
.LC3:
	.string	"schedule for "
.LC4:
	.string	" (B0 entered "
.LC5:
	.string	" times)"
.LC6:
	.string	"block counts for "
.LC7:
	.string	":"
.LC8:
	.string	"vector::reserve"
.LC9:
	.string	"block B"
.LC10:
	.string	" : "
	.section	.text._ZN2v88internallsERSoRKNS0_18BasicBlockProfiler4DataE.part.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internallsERSoRKNS0_18BasicBlockProfiler4DataE.part.0, @function
_ZN2v88internallsERSoRKNS0_18BasicBlockProfiler4DataE.part.0:
.LFB5101:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 64(%rsi)
	movq	96(%rsi), %rax
	je	.L220
	movq	56(%rsi), %r12
	testq	%rax, %rax
	jne	.L314
.L221:
	movl	$17, %edx
	leaq	.LC6(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	testq	%r12, %r12
	je	.L315
	movq	%r12, %rdi
	call	strlen@PLT
	movq	%rax, %rdx
.L272:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L233:
	movl	$1, %edx
	leaq	.LC7(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	0(%r13), %rax
	movq	-24(%rax), %rax
	movq	240(%r13,%rax), %r12
	testq	%r12, %r12
	je	.L229
	cmpb	$0, 56(%r12)
	je	.L234
	movsbl	67(%r12), %esi
.L235:
	movq	%r13, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movq	(%rbx), %rcx
	pxor	%xmm0, %xmm0
	movabsq	$1152921504606846975, %rax
	movq	$0, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	cmpq	%rax, %rcx
	ja	.L316
	testq	%rcx, %rcx
	jne	.L317
.L258:
	movq	0(%r13), %rax
	movq	-24(%rax), %rax
	movq	240(%r13,%rax), %r12
	testq	%r12, %r12
	je	.L229
	cmpb	$0, 56(%r12)
	je	.L262
	movsbl	67(%r12), %esi
.L263:
	movq	%r13, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	cmpq	$0, 128(%rbx)
	jne	.L318
.L264:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L219
	call	_ZdlPv@PLT
.L219:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L319
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L220:
	.cfi_restore_state
	testq	%rax, %rax
	jne	.L271
	movl	$17, %edx
	leaq	.LC6(%rip), %rsi
	leaq	.LC2(%rip), %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$16, %edx
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L315:
	movq	0(%r13), %rax
	movq	-24(%rax), %rdi
	addq	%r13, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L234:
	movq	%r12, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r12), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L235
	movq	%r12, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L262:
	movq	%r12, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r12), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L263
	movq	%r12, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L317:
	leaq	0(,%rcx,8), %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	-80(%rbp), %r8
	movq	-72(%rbp), %rdi
	movq	%rax, %r12
	movq	%rax, %rdx
	movq	%r8, %rax
	cmpq	%r8, %rdi
	je	.L241
	.p2align 4,,10
	.p2align 3
.L238:
	movl	(%rax), %esi
	movl	4(%rax), %ecx
	addq	$8, %rax
	addq	$8, %rdx
	movl	%esi, -8(%rdx)
	movl	%ecx, -4(%rdx)
	cmpq	%rax, %rdi
	jne	.L238
.L241:
	testq	%r8, %r8
	je	.L240
	movq	%r8, %rdi
	call	_ZdlPv@PLT
.L240:
	movq	%r12, %xmm0
	leaq	(%r12,%r14), %rcx
	cmpq	$0, (%rbx)
	punpcklqdq	%xmm0, %xmm0
	movq	%rcx, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	je	.L258
	xorl	%r14d, %r14d
	leaq	-88(%rbp), %r15
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L320:
	movl	%edx, (%r12)
	addq	$1, %r14
	movl	%eax, 4(%r12)
	movq	-72(%rbp), %rax
	leaq	8(%rax), %r12
	movq	%r12, -72(%rbp)
	cmpq	(%rbx), %r14
	jnb	.L243
.L244:
	movq	-64(%rbp), %rcx
.L246:
	movq	8(%rbx), %rax
	movl	(%rax,%r14,4), %edx
	movq	32(%rbx), %rax
	movl	(%rax,%r14,4), %eax
	movl	%edx, -88(%rbp)
	movl	%eax, -84(%rbp)
	cmpq	%rcx, %r12
	jne	.L320
	movq	%r12, %rsi
	leaq	-80(%rbp), %rdi
	movq	%r15, %rdx
	addq	$1, %r14
	call	_ZNSt6vectorISt4pairIijESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	movq	-72(%rbp), %r12
	cmpq	%r14, (%rbx)
	ja	.L244
	.p2align 4,,10
	.p2align 3
.L243:
	movq	-80(%rbp), %r14
	cmpq	%r14, %r12
	je	.L258
	movq	%r12, %r15
	movl	$63, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	subq	%r14, %r15
	movq	%r15, %rax
	sarq	$3, %rax
	bsrq	%rax, %rax
	xorq	$63, %rax
	subl	%eax, %edx
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	call	_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPSt4pairIijESt6vectorIS3_SaIS3_EEEElNS0_5__ops15_Iter_comp_iterIZN2v88internallsERSoRKNSC_18BasicBlockProfiler4DataEEUlS3_S3_E_EEEvT_SK_T0_T1_
	cmpq	$128, %r15
	jle	.L247
	leaq	128(%r14), %rsi
	movq	%r14, %rdi
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPSt4pairIijESt6vectorIS3_SaIS3_EEEENS0_5__ops15_Iter_comp_iterIZN2v88internallsERSoRKNSC_18BasicBlockProfiler4DataEEUlS3_S3_E_EEEvT_SK_T0_.isra.0
	movq	%rsi, %r8
	cmpq	%rsi, %r12
	je	.L249
	.p2align 4,,10
	.p2align 3
.L254:
	movl	(%r8), %r9d
	movl	4(%r8), %ecx
	movq	%r8, %rdx
	jmp	.L253
	.p2align 4,,10
	.p2align 3
.L251:
	testb	%dil, %dil
	je	.L252
.L321:
	movl	-4(%rdx), %eax
	movl	%esi, (%rdx)
	subq	$8, %rdx
	movl	%eax, 12(%rdx)
.L253:
	movl	-4(%rdx), %eax
	movl	-8(%rdx), %esi
	cmpl	%eax, %ecx
	seta	%dil
	jne	.L251
	cmpl	%esi, %r9d
	setl	%dil
	testb	%dil, %dil
	jne	.L321
.L252:
	addq	$8, %r8
	movl	%r9d, (%rdx)
	movl	%ecx, 4(%rdx)
	cmpq	%r8, %r12
	jne	.L254
.L249:
	movq	-72(%rbp), %rax
	movq	-80(%rbp), %r12
	movq	%rax, -104(%rbp)
	cmpq	%r12, %rax
	jne	.L261
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L322:
	movsbl	67(%rdi), %esi
.L260:
	movq	%r15, %rdi
	addq	$8, %r12
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	cmpq	%r12, -104(%rbp)
	je	.L258
.L261:
	movl	4(%r12), %r14d
	movl	(%r12), %r15d
	testl	%r14d, %r14d
	je	.L258
	movl	$7, %edx
	leaq	.LC9(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%r15d, %esi
	movq	%r13, %rdi
	call	_ZNSolsEi@PLT
	movl	$3, %edx
	leaq	.LC10(%rip), %rsi
	movq	%rax, %r15
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r15, %rdi
	movl	%r14d, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	%rax, %r15
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%r15,%rax), %rdi
	testq	%rdi, %rdi
	je	.L229
	cmpb	$0, 56(%rdi)
	jne	.L322
	movq	%rdi, -112(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-112(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rcx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rcx, %rax
	je	.L260
	call	*%rax
	movsbl	%al, %esi
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L318:
	movq	120(%rbx), %r12
	testq	%r12, %r12
	je	.L323
	movq	%r12, %rdi
	call	strlen@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L266:
	movq	0(%r13), %rax
	movq	-24(%rax), %rax
	movq	240(%r13,%rax), %r12
	testq	%r12, %r12
	je	.L229
	cmpb	$0, 56(%r12)
	je	.L267
	movsbl	67(%r12), %esi
.L268:
	movq	%r13, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L247:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPSt4pairIijESt6vectorIS3_SaIS3_EEEENS0_5__ops15_Iter_comp_iterIZN2v88internallsERSoRKNSC_18BasicBlockProfiler4DataEEUlS3_S3_E_EEEvT_SK_T0_.isra.0
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L314:
	movl	$13, %edx
	leaq	.LC3(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	testq	%r12, %r12
	jne	.L222
	movq	0(%r13), %rax
	movq	-24(%rax), %rdi
	addq	%r13, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
.L223:
	movl	$13, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	32(%rbx), %rax
	movq	%r13, %rdi
	movl	(%rax), %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$7, %edx
	leaq	.LC5(%rip), %rsi
	movq	%rax, %r14
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rax
	movq	-24(%rax), %rax
	movq	240(%r14,%rax), %r15
	testq	%r15, %r15
	je	.L229
	cmpb	$0, 56(%r15)
	je	.L225
	movsbl	67(%r15), %esi
.L226:
	movq	%r14, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movq	88(%rbx), %r14
	testq	%r14, %r14
	je	.L324
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L228:
	movq	0(%r13), %rax
	movq	-24(%rax), %rax
	movq	240(%r13,%rax), %r14
	testq	%r14, %r14
	je	.L229
	cmpb	$0, 56(%r14)
	je	.L230
	movsbl	67(%r14), %esi
.L231:
	movq	%r13, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L271:
	movl	$13, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	.LC2(%rip), %r12
	movl	$16, %edx
.L270:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L267:
	movq	%r12, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r12), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L268
	movq	%r12, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L230:
	movq	%r14, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r14), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L231
	movq	%r14, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L225:
	movq	%r15, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r15), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L226
	movq	%r15, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L324:
	movq	0(%r13), %rax
	movq	-24(%rax), %rdi
	addq	%r13, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L323:
	movq	0(%r13), %rax
	movq	-24(%rax), %rdi
	addq	%r13, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L266
.L222:
	movq	%r12, %rdi
	call	strlen@PLT
	movq	%rax, %rdx
	jmp	.L270
.L229:
	call	_ZSt16__throw_bad_castv@PLT
.L316:
	leaq	.LC8(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L319:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5101:
	.size	_ZN2v88internallsERSoRKNS0_18BasicBlockProfiler4DataE.part.0, .-_ZN2v88internallsERSoRKNS0_18BasicBlockProfiler4DataE.part.0
	.section	.text._ZN2v88internallsERSoRKNS0_18BasicBlockProfiler4DataE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internallsERSoRKNS0_18BasicBlockProfiler4DataE
	.type	_ZN2v88internallsERSoRKNS0_18BasicBlockProfiler4DataE, @function
_ZN2v88internallsERSoRKNS0_18BasicBlockProfiler4DataE:
.LFB3993:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	40(%rsi), %r8
	movq	32(%rsi), %rdx
	cmpq	%rdx, %r8
	je	.L326
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L327:
	addl	(%rdx), %ecx
	addq	$4, %rdx
	cmpq	%rdx, %r8
	jne	.L327
	testl	%ecx, %ecx
	je	.L326
	movq	%r12, %rdi
	call	_ZN2v88internallsERSoRKNS0_18BasicBlockProfiler4DataE.part.0
.L326:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3993:
	.size	_ZN2v88internallsERSoRKNS0_18BasicBlockProfiler4DataE, .-_ZN2v88internallsERSoRKNS0_18BasicBlockProfiler4DataE
	.section	.rodata._ZN2v88internallsERSoRKNS0_18BasicBlockProfilerE.str1.8,"aMS",@progbits,1
	.align 8
.LC11:
	.string	"---- Start Profiling Data ----"
	.section	.rodata._ZN2v88internallsERSoRKNS0_18BasicBlockProfilerE.str1.1,"aMS",@progbits,1
.LC12:
	.string	"---- End Profiling Data ----"
	.section	.text._ZN2v88internallsERSoRKNS0_18BasicBlockProfilerE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internallsERSoRKNS0_18BasicBlockProfilerE
	.type	_ZN2v88internallsERSoRKNS0_18BasicBlockProfilerE, @function
_ZN2v88internallsERSoRKNS0_18BasicBlockProfilerE:
.LFB3992:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$30, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	leaq	.LC11(%rip), %rsi
	pushq	%rbx
	.cfi_offset 3, -48
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	0(%r13), %rax
	movq	-24(%rax), %rax
	movq	240(%r13,%rax), %r14
	testq	%r14, %r14
	je	.L338
	cmpb	$0, 56(%r14)
	je	.L335
	movsbl	67(%r14), %esi
.L336:
	movq	%r13, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movq	(%r12), %rbx
	cmpq	%r12, %rbx
	je	.L342
	.p2align 4,,10
	.p2align 3
.L337:
	movq	16(%rbx), %rsi
	movq	40(%rsi), %rcx
	movq	32(%rsi), %rax
	cmpq	%rax, %rcx
	je	.L340
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L341:
	addl	(%rax), %edx
	addq	$4, %rax
	cmpq	%rax, %rcx
	jne	.L341
	testl	%edx, %edx
	je	.L340
	movq	%r13, %rdi
	call	_ZN2v88internallsERSoRKNS0_18BasicBlockProfiler4DataE.part.0
.L340:
	movq	(%rbx), %rbx
	cmpq	%r12, %rbx
	jne	.L337
.L342:
	movl	$28, %edx
	leaq	.LC12(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	0(%r13), %rax
	movq	-24(%rax), %rax
	movq	240(%r13,%rax), %r12
	testq	%r12, %r12
	je	.L338
	cmpb	$0, 56(%r12)
	je	.L343
	movsbl	67(%r12), %esi
.L344:
	movq	%r13, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L335:
	.cfi_restore_state
	movq	%r14, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r14), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L336
	movq	%r14, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L343:
	movq	%r12, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r12), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L344
	movq	%r12, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L344
.L338:
	call	_ZSt16__throw_bad_castv@PLT
	.cfi_endproc
.LFE3992:
	.size	_ZN2v88internallsERSoRKNS0_18BasicBlockProfilerE, .-_ZN2v88internallsERSoRKNS0_18BasicBlockProfilerE
	.section	.bss._ZGVZN2v88internal18BasicBlockProfiler3GetEvE6object,"aw",@nobits
	.align 8
	.type	_ZGVZN2v88internal18BasicBlockProfiler3GetEvE6object, @object
	.size	_ZGVZN2v88internal18BasicBlockProfiler3GetEvE6object, 8
_ZGVZN2v88internal18BasicBlockProfiler3GetEvE6object:
	.zero	8
	.section	.bss._ZZN2v88internal18BasicBlockProfiler3GetEvE6object,"aw",@nobits
	.align 32
	.type	_ZZN2v88internal18BasicBlockProfiler3GetEvE6object, @object
	.size	_ZZN2v88internal18BasicBlockProfiler3GetEvE6object, 64
_ZZN2v88internal18BasicBlockProfiler3GetEvE6object:
	.zero	64
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
