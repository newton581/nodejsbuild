	.file	"string-case.cc"
	.text
	.section	.text._ZN2v88internal16FastAsciiConvertILb0EEEiPcPKciPb,"axG",@progbits,_ZN2v88internal16FastAsciiConvertILb0EEEiPcPKciPb,comdat
	.p2align 4
	.weak	_ZN2v88internal16FastAsciiConvertILb0EEEiPcPKciPb
	.type	_ZN2v88internal16FastAsciiConvertILb0EEEiPcPKciPb, @function
_ZN2v88internal16FastAsciiConvertILb0EEEiPcPKciPb:
.LFB5523:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edx, %r10
	addq	%rsi, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testb	$7, %sil
	jne	.L14
	leaq	-8(%r10), %r11
	movq	%rsi, %rax
	movabsq	$-9187201950435737472, %rbx
	movabsq	$-361700864190383366, %r14
	movabsq	$2242545357980376863, %r13
	cmpq	%r11, %rsi
	jbe	.L3
	.p2align 4,,10
	.p2align 3
.L26:
	xorl	%ebx, %ebx
.L2:
	cmpq	%rax, %r10
	jbe	.L4
	movabsq	$-9187201950435737472, %r11
	subq	%rax, %r10
	addq	%rdi, %r10
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L11:
	leal	-97(%r9), %r9d
	cmpb	$25, %r9b
	ja	.L12
	xorl	$32, %r8d
	movl	$1, %ebx
.L12:
	movb	%r8b, (%rdi)
	addq	$1, %rdi
	addq	$1, %rax
	cmpq	%rdi, %r10
	je	.L4
.L13:
	movsbq	(%rax), %r9
	movq	%r9, %r8
	testq	%r11, %r9
	je	.L11
.L27:
	popq	%rbx
	subl	%esi, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	movq	%r14, %r9
	leaq	(%r8,%r13), %r12
	subq	%r8, %r9
	andq	%r12, %r9
	testq	%rbx, %r9
	jne	.L8
	addq	$8, %rax
	movq	%r8, (%rdi)
	addq	$8, %rdi
	cmpq	%r11, %rax
	ja	.L26
.L3:
	movq	(%rax), %r8
	testq	%rbx, %r8
	je	.L6
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L14:
	movq	%rsi, %rax
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L4:
	movb	%bl, (%rcx)
	movl	%edx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	cmpq	%rax, %r11
	jb	.L18
	movabsq	$-361700864190383366, %r14
	movabsq	$2242545357980376863, %r13
	movabsq	$2314885530818453536, %r12
	movabsq	$-9187201950435737472, %rbx
	.p2align 4,,10
	.p2align 3
.L9:
	movq	%r14, %r9
	leaq	(%r8,%r13), %r15
	addq	$8, %rax
	addq	$8, %rdi
	subq	%r8, %r9
	andq	%r15, %r9
	shrq	$2, %r9
	andq	%r12, %r9
	xorq	%r9, %r8
	movq	%r8, -8(%rdi)
	cmpq	%r11, %rax
	ja	.L18
	movq	(%rax), %r8
	testq	%rbx, %r8
	je	.L9
	jmp	.L27
.L18:
	movl	$1, %ebx
	jmp	.L2
	.cfi_endproc
.LFE5523:
	.size	_ZN2v88internal16FastAsciiConvertILb0EEEiPcPKciPb, .-_ZN2v88internal16FastAsciiConvertILb0EEEiPcPKciPb
	.section	.text._ZN2v88internal16FastAsciiConvertILb1EEEiPcPKciPb,"axG",@progbits,_ZN2v88internal16FastAsciiConvertILb1EEEiPcPKciPb,comdat
	.p2align 4
	.weak	_ZN2v88internal16FastAsciiConvertILb1EEEiPcPKciPb
	.type	_ZN2v88internal16FastAsciiConvertILb1EEEiPcPKciPb, @function
_ZN2v88internal16FastAsciiConvertILb1EEEiPcPKciPb:
.LFB5525:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edx, %r10
	addq	%rsi, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testb	$7, %sil
	jne	.L42
	leaq	-8(%r10), %r11
	movq	%rsi, %rax
	movabsq	$-9187201950435737472, %rbx
	movabsq	$-2676586395008836902, %r14
	movabsq	$4557430888798830399, %r13
	cmpq	%r11, %rsi
	jbe	.L31
	.p2align 4,,10
	.p2align 3
.L54:
	xorl	%ebx, %ebx
.L30:
	cmpq	%rax, %r10
	jbe	.L32
	movabsq	$-9187201950435737472, %r11
	subq	%rax, %r10
	addq	%rdi, %r10
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L39:
	leal	-65(%r9), %r9d
	cmpb	$25, %r9b
	ja	.L40
	xorl	$32, %r8d
	movl	$1, %ebx
.L40:
	movb	%r8b, (%rdi)
	addq	$1, %rdi
	addq	$1, %rax
	cmpq	%rdi, %r10
	je	.L32
.L41:
	movsbq	(%rax), %r9
	movq	%r9, %r8
	testq	%r11, %r9
	je	.L39
.L55:
	popq	%rbx
	subl	%esi, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	.cfi_restore_state
	movq	%r14, %r9
	leaq	(%r8,%r13), %r12
	subq	%r8, %r9
	andq	%r12, %r9
	testq	%rbx, %r9
	jne	.L36
	addq	$8, %rax
	movq	%r8, (%rdi)
	addq	$8, %rdi
	cmpq	%r11, %rax
	ja	.L54
.L31:
	movq	(%rax), %r8
	testq	%rbx, %r8
	je	.L34
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L42:
	movq	%rsi, %rax
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L32:
	movb	%bl, (%rcx)
	movl	%edx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	.cfi_restore_state
	cmpq	%rax, %r11
	jb	.L46
	movabsq	$-2676586395008836902, %r14
	movabsq	$4557430888798830399, %r13
	movabsq	$2314885530818453536, %r12
	movabsq	$-9187201950435737472, %rbx
	.p2align 4,,10
	.p2align 3
.L37:
	movq	%r14, %r9
	leaq	(%r8,%r13), %r15
	addq	$8, %rax
	addq	$8, %rdi
	subq	%r8, %r9
	andq	%r15, %r9
	shrq	$2, %r9
	andq	%r12, %r9
	xorq	%r9, %r8
	movq	%r8, -8(%rdi)
	cmpq	%r11, %rax
	ja	.L46
	movq	(%rax), %r8
	testq	%rbx, %r8
	je	.L37
	jmp	.L55
.L46:
	movl	$1, %ebx
	jmp	.L30
	.cfi_endproc
.LFE5525:
	.size	_ZN2v88internal16FastAsciiConvertILb1EEEiPcPKciPb, .-_ZN2v88internal16FastAsciiConvertILb1EEEiPcPKciPb
	.section	.text.startup._GLOBAL__sub_I_string_case.cc,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I_string_case.cc, @function
_GLOBAL__sub_I_string_case.cc:
.LFB5904:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE5904:
	.size	_GLOBAL__sub_I_string_case.cc, .-_GLOBAL__sub_I_string_case.cc
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I_string_case.cc
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
